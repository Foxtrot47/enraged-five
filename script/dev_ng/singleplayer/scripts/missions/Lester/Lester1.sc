 
///
/// 				Author:  Michael Wadelin & Steven Kerrigan					Date: Nov/2011
///    
///    Michael is innstructed by lester to infiltrate the LifeInvader HQ and plant a bomb in the back
///    of a new, to be announced, device that they are working on.
///    Michael must get a disguise to blend in with the staff and then go to the HQ.
///    Once there he is mistaken for an IT guy and is coaxed into helping remove some porn malware from
///    one of the programmers PC's.
///    Once this is done you must plant the bomb then leave without alerting anyone to your presence.

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.
   
/// Includes
 
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
using "commands_streaming.sch"
using "commands_camera.sch"
USING "Commands_clock.sch"
USING "Commands_interiors.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "script_blips.sch"
USING "script_maths.sch"
USING "commands_entity.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "locates_public.sch"
USING "player_ped_public.sch"
USING "CompletionPercentage_public.sch"
USING "shop_public.sch"
USING "shop_private.sch"
USING "building_control_public.sch"
USING "usefulcommands.sch"
USING "asset_management_public.sch"
USING "building_control_private.sch"
USING "ambient_common.sch"
USING "cellphone_public.sch"
USING "clearmissionarea.sch"
USING "rc_threat_public.sch"
USING "tv_control_public.sch"
USING "finance_control_public.sch"
USING "commands_recording.sch"

/*
											 _     _  _______  ______   _  _______  ______   _        _______   ______ 
											(_)   (_)(_______)(_____ \ | |(_______)(____  \ (_)      (_______) / _____)
											 _     _  _______  _____) )| | _______  ____)  ) _        _____   ( (____  
											| |   | ||  ___  ||  __  / | ||  ___  ||  __  ( | |      |  ___)   \____ \ 
											 \ \ / / | |   | || |  \ \ | || |   | || |__)  )| |_____ | |_____  _____) )
											  \___/  |_|   |_||_|   |_||_||_|   |_||______/ |_______)|_______)(______/
*/
 
// Constants
//---------------------------------------------------------------------------------------------------------------------------------------------------

	CONST_INT		STAGE_ENTRY						0
	CONST_INT		STAGE_EXIT						-1
	
	CONST_INT		I_NUM_CUTSCENES					5
	CONST_INT		I_NUM_Z_MENU_ELEMENTS			11 + I_NUM_CUTSCENES
	
	CONST_FLOAT		F_DIST_LI_PED_STOPS				25.0
	
	TEXT_LABEL_23	str_wp_engineer_route			= "lester1a_follow_e"
	
	TEXT_LABEL_23	str_dialogue					= "LS1AAUD"
	
	// ANIM Dictionaries
	
	TEXT_LABEL_23	anim_dict_milk_lady				= "misslester1aig_1"
	TEXT_LABEL_63	anim_dict_coffee				= "amb@world_human_drinking@coffee@male@idle_a"
	
	TEXT_LABEL_23	anim_dict_interview_1st			= "misslester1aig_2main"
	TEXT_LABEL_23	anim_dict_interview_1st_exit	= "misslester1aig_2exit"
	TEXT_LABEL_23	anim_dict_interview_2nd			= "misslester1aig_7main"
	TEXT_LABEL_23	anim_dict_interview_2nd_exit	= "misslester1aig_7exit"
	
	TEXT_LABEL_23	anim_dict_airguitar_1st			= "misslester1aig_3main"
	TEXT_LABEL_23	anim_dict_airguitar_1st_exit	= "misslester1aig_3exit"
	TEXT_LABEL_23	anim_dict_airguitar_2nd			= "misslester1aig_4main"
	TEXT_LABEL_23	anim_dict_airguitar_2nd_exit	= "misslester1aig_4exit"
	
	TEXT_LABEL_23	anim_dict_boardroom_intro		= "misslester1aig_5intro"
	TEXT_LABEL_23	anim_dict_boardroom_main		= "misslester1aig_5main"
	TEXT_LABEL_23	anim_dict_boardroom_exit		= "misslester1aig_5exit"
	
	TEXT_LABEL_23	anim_dict_paper_throw			= "misslester1aig_6"
	
	TEXT_LABEL_23	anim_dict_engineer_airguitar 	= "misslester1aig_9"
		
	// Audio scenes
	TEXT_LABEL_31	aScene_noise_blocker			= "FAKE_INTERIOR_OCCLUSION_SCENE"
	
	
	// Assisted routes
	TEXT_LABEL_15	al_stairs_up					= "life_up"
	TEXT_LABEL_15	al_stairs_down					= "invad_exit"
//	TEXT_LABEL_15	al_door1 						= "life_door1"
//	TEXT_LABEL_15	al_door2 						= "invad_door2"
	
	
	// model names
	MODEL_NAMES 	mod_hipster_main				= IG_LIFEINVAD_01
	MODEL_NAMES	 	mod_hipster_m					= A_M_Y_HIPSTER_01
	MODEL_NAMES 	mod_hipster_f 					= A_F_Y_HIPSTER_01
	MODEL_NAMES 	mod_hipster_f_heel				= A_F_Y_HIPSTER_04
	MODEL_NAMES 	mod_chair 						= Prop_Off_Chair_01
	MODEL_NAMES 	mod_cup							= PROP_CS_PAPER_CUP
	MODEL_NAMES 	mod_milk_carton					= PROP_CS_MILK_01
	
	CONST_FLOAT 	DOORS_KEEP_CURRENT_RATIO 		100.0
	
	// 1B merged constants
	
	//CAMERA CONST
	CONST_INT		MAX_CAM_SHOTS_POS				4
	CONST_INT		MAX_CAM_SHOTS_ROT				4
	
	TEXT_LABEL_15	str_new_overlay 				= "breaking_news"

	TEXT_LABEL_31	ad_ig_1 						= "misslester1b"
	
	CONST_INT 		i_start_speech					5000
	CONST_INT 		i_keynote_duration				175137
	CONST_INT 		i_detonate_window_open			77083
	CONST_INT 		i_detonate_window_close			170000 //150000
	CONST_INT 		i_remove_help					148000
	
	CONST_INT 		i_signal_lost_start				11950
	CONST_INT 		i_signal_lost_duration			7000
	
	VECTOR 			ig_1_coord						= <<-803.9083, 171.8480, 72.8347>>
	VECTOR 			ig_1_rot 						= << 0.000, 0.000, 297.2519 >>
	
	VECTOR 			v_home_living_room 				= << -802.4717, 175.9820, 71.8348 >>
		
	VECTOR			v_sf3d_tv_coord					= <<-800.158, 173.603, 74.38>>
	VECTOR			v_sf3d_tv_rot					= <<0,0,69.130>>
	VECTOR			v_sf3d_tv_scale					= <<1,1,1>>
	VECTOR			v_sf3d_tv_world_size			= <<3.163,1.783,1>>

	//TV CAMERA VECTORS
	VECTOR			vCamPos[MAX_CAM_SHOTS_POS]
	VECTOR			vCamRot[MAX_CAM_SHOTS_ROT]

// Enumerators
//---------------------------------------------------------------------------------------------------------------------------------------------------

	// List of synced scenes
	ENUM synced_scene_flag
		ssf_lead_in,
		ssf_ambient_milk_lady,
		ssf_ambient_interview,
		ssf_ambient_office,
		ssf_ambient_paper_throw,
		ssf_ambient_boardroom_main,
		ssf_ambient_boardroom_additional,
		ssf_engineer_air_guitar,
		ssf_tracey,
		ssf_num_scenes
	ENDENUM

	// List of peds in the mission
	ENUM mission_ped_flag
		mpf_mike,
		mpf_shop,
		
		mpf_lester,
	
		// guy who goes out for a smoke
		mpf_smoker,
		
		mpf_milk_f,
		mpf_coffee_1,
		mpf_coffee_2,
		
		mpf_receptionist_f,
		
		// interview scene
		mpf_hr,
		mpf_gamer,
		mpf_interviewee_m,
		mpf_interviewee_f,
		mpf_interviewer,

		// paper throwers
		mpf_office_paper_m,
		mpf_office_paper_f,

		// office peds
		mpf_office_boss,
		mpf_office_a,
		mpf_office_b,
		mpf_office_c,
		mpf_office_d,
		
		// board room peds
		mpf_boardroom_norris,
		mpf_boardroom_m_a,
		mpf_boardroom_m_b,
		mpf_boardroom_m_e,
		mpf_boardroom_m_f,
		//mpf_boardroom_f_a,
		mpf_boardroom_f_b,
		mpf_boardroom_f_c,
		
		// Family
		mpf_tracey,
				
		mpf_num_peds
	ENDENUM

	// list of mission objects
	ENUM mission_objects_flag
		mof_wheelchair,
		mof_smokers_chair,
		mof_paper_ball,
		mof_hacky_sack,
		mof_prototype_case,
		mof_office_boss_chair,
		mof_office_a_chair,
		mof_office_b_chair,
		mof_office_c_chair,
		mof_office_d_chair,
		mof_office_thrower_m_chair,
		mof_office_thrower_f_chair,
		mof_boardroom_spare_chair_a,
		mof_boardroom_spare_chair_b,
		mof_boardroom_spare_chair_c,
		mof_boardroom_spare_chair_d,
		mof_boardroom_m_a_chair,
		mof_boardroom_m_b_chair,		
		mof_boardroom_m_e_chair,
		mof_boardroom_m_f_chair,
		//mof_boardroom_f_a_chair,
		mof_boardroom_f_b_chair,
		mof_boardroom_f_c_chair,
		mof_jay_norris_chair,
		mof_receptionist_chair,
		mof_milk_carton,
		mof_cup,
		mof_cup_1,
		mof_cup_2,
		mof_cupboard_door,
		mof_mike_rucksack,
		mof_tv_remote,
		mof_phone,
		mof_num_objs
	ENDENUM

	// List of mission events
	ENUM mission_event_flag		
		mef_lester_phone_call,
		mef_lester_phone_call_2,
		mef_manage_bag,
		mef_engineer_follow,
		mef_ambient_milk,
		mef_ambient_interview,
		mef_ambient_office,		// air guitaring as you enter
		mef_ambient_paper_throw,
		mef_ambient_boardroom,	// board room meeting
		mef_ambient_receptionist,
		mef_engineer_air_guitar,
		mef_ambient_dialogue,	// manages the ambient dialogue in the office
		mef_tracey,
		mef_num_events
	ENDENUM

	// List of mission stages
	ENUM mission_stage_flag 
		msf_st0_intro_cutscene = 0,		
		msf_st1_get_clothes,			
		msf_st2_go_to_offices,			
		msf_st3_go_to_computer,			
		msf_st4_mini_game,				
		msf_st5_plant_bomb,				
		msf_st6_leave_building,			
		msf_st7_go_home,
		msf_st8_tv_start,				
		msf_st9_tv_detonate,			
		msf_st10_mission_passed,
		
		msf_num_stages
	ENDENUM
	
	// Describes the current stage management state
	ENUM STAGE_SWITCHSTATE
		STAGESWITCH_IDLE,
		STAGESWITCH_REQUESTED,
		STAGESWITCH_EXITING,
		STAGESWITCH_ENTERING
	ENDENUM
	
	//CAMERA ENUMS
	ENUM CAM_SHOTS
		CAMS_ULTRA = 0,
		CAMS_CLOSE,
		CAMS_ROOM,
		CAMS_SCRIPT
	ENDENUM
	
	//ANIM STATE MACHINE ENUMS
	ENUM MIKE_ANIM_STATE
		MAS_NULL,
		MAS_R_IDLE,
		MAS_R_FIDG,
		MAS_R_PRESS,
		MAS_R_PRESS_HOLD_INTRO,
		MAS_R_PRESS_HOLD_LOOP,
		MAS_R_PRESS_HOLD_OUTRO,
		MAS_P_INTO,
		MAS_P_AWAY,
		MAS_P_IDLE,
		MAS_P_SCROLL,
		MAS_P_PRESS,
		MAS_P_DETO_INTRO,
		MAS_P_DETO_WAIT,
		MAS_P_DETO_REACT,
		MAS_P_OUTRO
	ENDENUM
	
	// List of minigame feedback flags
	ENUM minigame_scaleform_flags
		mgsf_nothing = 0,
		mgsf_pc_window = 40,
		mgsf_av_open = 2,
		mgsf_av_start_clean = 50,
		mgsf_av_exterminate = 51,
		mgsf_av_popup_close = 60
	ENDENUM
	
	// List of all the fail conditions
	ENUM mission_fail_flag
		mff_lester_dead,
		mff_blew_your_cover,
		mff_bomb_plant_failed,
		mff_store_closed,
		mff_insufficient_fund_for_disguise,
		mff_did_not_kill_target,
		mff_tracey_dead,
		mff_tracey_threat,
		mff_wanted_home,
		mff_debug_forced,
		mff_default,	// used when a specific condition isn't specified
		mff_num_fails
	ENDENUM
	
	ENUM mission_door_flags
		mdf_office_board_room_l,
		mdf_office_board_room_r,
		mdf_office_slide_1,
		mdf_office_slide_2,
		mdf_num_doors
	ENDENUM
	
	ENUM ENGINEER_FOLLOW_ENUM
		EFE_START = 1,
		EFE_TO_DESK,	
		EFE_WAITING_ON_ROUTE_START,
		EFE_WAITING_ON_ROUTE, 
		EFE_WAITING_AT_END
	ENDENUM

	ENUM TRIGGER_SOUND_STATES
		TSS_NULL,
		TSS_WAIT_PLAY_REMOTE_RING,
		TSS_REMOTE_RING,
		TSS_WAIT_ANSWER,
		TSS_ANSWER,
		TSS_END
	ENDENUM


// Structs
//---------------------------------------------------------------------------------------------------------------------------------------------------

	// Event struct
	STRUCT MISSION_EVENT_STRUCT
		INT		iTimeStamp
		INT 	iStage
		INT 	iDelay
		BOOL	bTriggered
		BOOL	bComplete
		BOOL 	bPaused
		BOOL	bAnimDictPrestreamed
		BOOL	bSafeAfterSkip
	ENDSTRUCT
	
	STRUCT POPUP_STRUCT
		INT		iType			= -1  	// if this does not get initialised to something other than 1 it is considered not used
		BOOL	bClosed			= TRUE
		FLOAT	fPosX
		FLOAT	fPosY
		BOOL	bSfxPlaying		= FALSE
		INT		iSfxLoop		= -1
	ENDSTRUCT
	
	// struct is used so I can different between props I've added myself and ones that I've grabbed and used from the interior
	STRUCT PROP_STRUCT
		OBJECT_INDEX	id
		BOOL			bCustom
		BOOL			bInSynchedScene
		MODEL_NAMES		model
		VECTOR			v_coord
		BOOL			bDoNotReset
		STRING			adName
		STRING			animName
	ENDSTRUCT
	
	// Struct used to hold info about doors
	STRUCT DOOR_STRUCT
		INT				iHash				= -1
		BOOL			bChangingState
		FLOAT			fDesiredOpenRatio
		FLOAT			fStartOpenRatio
		BOOL			bDesiredLockState
		INT				iInterpTime
		INT				iTimeStamp
	ENDSTRUCT

// Variables
//---------------------------------------------------------------------------------------------------------------------------------------------------

	// locations: vectors & headings
	VECTOR 								v_LestersHouseParkingCoord		= <<1283.7845, -1728.9476, 51.8119>>
	CONST_FLOAT							f_LestersHouseParkingHeading	115.1431
	VECTOR 								v_ShopBlip 						= << 127.6313, -209.8459, 53.5463 >>
	VECTOR								v_bin_coord						= << -1054.357, -240.242, 43.217 >>
	VECTOR								v_bin_rot						= << 0.0, 0.0, 27.5>>
	VECTOR 								v_engineer_monitor 				= <<-1059.61, -244.62, 43.92>>
	VECTOR 								v_coffee_guy_1_coord			= << -1066.5713, -244.5061, 39.7332 >>
	VECTOR 								v_coffee_guy_1_rot				= <<0,0,85.0757>>
	VECTOR 								v_coffee_guy_2_coord			= << -1067.689, -243.949, 39.7332 >>
	VECTOR 								v_coffee_guy_2_rot				= <<0,0,-117.5>>
	VECTOR								v_LifeInvaderCarParkCoord		= <<-1049.6331, -220.7515, 36.9051>>
	VECTOR								v_offices_foot					= << -1044.3571, -229.9086, 38.0141 >>
	FLOAT 								f_LifeInvaderCarParkHeading		= 246.7054
	VECTOR 								v_BagCoord 						= <<-1053.15, -230.89, 43.92>>
	VECTOR 								v_BagRot						= <<90.0, -58.0, 0.0>>
	VECTOR 								vBoardRoomSceneRootCoord 		= << -1044.4253, -235.0814, 43.021 >>
	VECTOR 								vBoardRoomSceneRootRot 			= << -0.000, 0.000, 118.800 >>

	// entity indices
	INTERIOR_INSTANCE_INDEX 			interior_offices
	INTERIOR_INSTANCE_INDEX 			interior_lesters
	INTERIOR_INSTANCE_INDEX 			interior_living_room
	DOOR_STRUCT							sDoors[mdf_num_doors]
	
	// general variables
	INT									iFailTimer 						= -1
	SEQUENCE_INDEX 						seq
	BLIP_INDEX 							blip_Objective
	SCALEFORM_INDEX 					mov
	SCALEFORM_RETURN_INDEX 				minigame_return_index
	SCALEFORM_RETURN_INDEX				last_popup_return_index
	LOCATES_HEADER_DATA 				locates_data
	structPedsForConversation			sConvo
	TEXT_LABEL_23						str_paused_root	
	TEXT_LABEL_23						str_paused_label	
	SCENARIO_BLOCKING_INDEX 			sbi_offices
	INT 								i_road_node_speed_zone
	REL_GROUP_HASH						rel_Friend
	REL_GROUP_HASH						REL_FAMILY

	// Flags
	BOOL 								bObtainedClothing				= FALSE
	BOOL 								bSideEntranceObjective			= FALSE
	BOOL 								bStreamCutScene					= FALSE
	BOOL 								bRequestedPreload 				= FALSE
	BOOL 								bDisableConspicuousBehavior		= FALSE
	BOOL 								bAllowRunning					= FALSE
	BOOL 								bHasChanged						= FALSE
	BOOL 								bLookingAtReceptionist			= FALSE
	INT									iLesterCallTimer				= 0
	BOOL 								bCutsceneExitHasFired			= FALSE
	BOOL 								bCamExited						= FALSE
	BOOL 								bSwappedMonitorToPopups			= FALSE
	BOOL 								bSwappedMonitorToClean			= FALSE
	BOOL 								bLestersDoorLocked				= FALSE
	BOOL								b_IsPlayingConvoIG9 			= FALSE
	INT 								i_EngineerBye					= 0
	BOOL 								bFirstThrow						= FALSE
	BOOL 								bWasBrowsingClothesLastFrame	= FALSE
	INT 								i_ReceptionistTalkCount			= 0
	INT 								i_LIChokePointStage				= 0
	INT 								i_CoughCounter					= 0
	INT 								i_CoughTimer					= 0
	BOOL								b_MiniGameScaleformSetUp		= FALSE
	INT 								i_EngineerCovoTimer				= 0
	BOOL 								bReadyToStartWalkChatConvo		= FALSE
	BOOL 								bStartedCoffeeDrinkingAnims1	= FALSE
	BOOL 								bStartedCoffeeDrinkingAnims2	= FALSE
	STRING 								strMinigameConvo				= ""
	BOOL								bPlayedClosePopupsFirstConvo	= FALSE
	INT 								iTimeSinceLastMinigameConvo		= 0
	BOOL 								bLifeInvaderGuyLastToSpeak		= FALSE
	BOOL 								bJustThrown 					= FALSE
	BOOL								b_CanFailForNotEnoughFunds		= FALSE
	INT									i_min_required_funds			= 0
	INT 								i_most_expensive_gilet			= 0
	INT 								i_most_expensive_shorts			= 0
	BOOL								bBringVehicleToHalt				= FALSE
	BOOL 								bAdditionalIsLooping			= FALSE
	INT									iShopCutsceneTimer				= 0
	INT									iLeaveShopConvo					= 0
	BOOL 								bExitStateCam					= FALSE
	BOOL 								bExitStateMike					= FALSE
	STRING 								strShopHelp
	BOOL								bCanInterruptAnim				= FALSE
	BOOL 								bPlayerIsBlockedForLeadIn		= FALSE
	BOOL								bReplayRecordEventStarted		= FALSE
	
	BLIP_INDEX 							blip_clothes[3]
	PED_COMP_NAME_ENUM					playerTorsoPrev
	PED_COMP_NAME_ENUM					playerLegsPrev

	INT									syncedIDs[ssf_num_scenes]
	PED_INDEX							peds[mpf_num_peds]
	PROP_STRUCT							objects[mof_num_objs]
	MISSION_EVENT_STRUCT				mEvents[mef_num_events]					// events array
	ASSET_MANAGEMENT_DATA				sAssetData
	CUTSCENE_PED_VARIATION 				sCutscenePedVariationRegister[10]
	VEHICLE_INDEX						veh_replay
	VEHICLE_INDEX						veh_Tracey

	// Stage/Skip stuff
	STAGE_SWITCHSTATE 					stageSwitch								// current switching status
	INT 								mission_stage							// current mission stage
	INT 								mission_substage						// current mission substage (switch flag used in stage procs)
	INT 								requestedStage							// the stage requested by a stage switch
	INT									iStageTimer
	INT									iStageManagementTimer
	BOOL 								bDoSkip
	INT									iSkipToStage
	INT									iPassTimer								//	url:bugstar:2099721
	
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 					widget_debug
	MissionStageMenuTextStruct			zMenuNames[I_NUM_Z_MENU_ELEMENTS]
	BOOL								bDisplayEventDebug
	BOOL								b_debug_PrintToConsole
	INT 								i_debug_PopupsOpen
	BOOL								b_debug_window_open
#ENDIF
	
	// Dialogue stuff
	INT 								i_ambient_dialogue_timer
	INT 								i_ambient_dialogue_delay
	BOOL 								b_OfficeSceneChat
	BOOL 								b_dia_new_guy
	
	// Mini game stuff
	POPUP_STRUCT						sPopups[12]
	INT									iIcons[6]
	INT 								iPopupHelp 									= 0
	INT									iReopenTimeStamp							= 0
	INT									iPopupsSFXPlaying							= 0
	INT									iLastPopupClosed							= -1
	BOOL								bIsAVOpen									= FALSE
	INT									iPrevScanBar 								= 0
	BOOL								bScanPaused									= FALSE
	INT									iScanStutter								= 0
	INT 								iSFX_FindVirus								= 0
	INT									iScanTimer									= 0
	INT									iCurrentPopup								= 0
	INT									iExterminateTimer							= 0
	FLOAT								fInputAccum 								= 0.0
	INT									iPopupOpenStage								= 0
	INT 								iUsablePopups
	FLOAT 								f_PCMousePosPrevX							= 0.5
	FLOAT 								f_PCMousePosPrevY							= 0.5
	
	INT									iPaperThrow								// paper throw anim index
	INT		 							i_piano_airguitar_loop_count

	INT									iDetonationTimer 							= -1
	INT									iDetonationCallTimer						= 0

	TRIGGER_SOUND_STATES				eSoundState 								= TSS_WAIT_PLAY_REMOTE_RING
	INT									iSoundID									= -1
	INT									iSoundTimer
	
	BOOL								bExterminated								= FALSE
	
	SCALEFORM_INDEX						sfi_news_overlay
	BOOL								bTVTurnedOn 								= FALSE
	MIKE_ANIM_STATE						eAnimState 									= MAS_NULL
	INT									iAnimDelay 									= -1
	BOOL								bDelaying 									= FALSE
	BOOL								bChangedChannel 							= FALSE
	BOOL 								bDetonated 									= FALSE
	BOOL 								bHouseAssetsRequested 						= FALSE
	BOOL 								bExpireChannelObj 							= FALSE
	BOOL								bTVTurnedOff 								= FALSE
	BOOL								bExpireDetonateConv 						= FALSE

	//TV camera
	CAM_SHOTS					eCameraCurrent = CAMS_SCRIPT
	CAMERA_INDEX 				cam_tv
	
	BOOL								bCursorSnap									= TRUE // Cursor snap toggle - true for PC MOUSE false otherwise.

	
// Grabs the name of the mission stage
FUNC STRING GET_MISSION_STAGE_NAME(mission_stage_flag eStage)

	STRING sResult
	
	SWITCH eStage
		CASE msf_st0_intro_cutscene				sResult = "0. Intro Cutscene"					BREAK
		CASE msf_st1_get_clothes				sResult = "1. Get Programmer Clothes"			BREAK
		CASE msf_st2_go_to_offices				sResult = "2. Goto LifeInvader Offices"			BREAK
		CASE msf_st3_go_to_computer				sResult = "3. Follow Programmer to his PC"		BREAK
		CASE msf_st4_mini_game					sResult = "4. Pop-Up Mini Game"					BREAK
		CASE msf_st5_plant_bomb					sResult = "5. Plant the Bomb"					BREAK
		CASE msf_st6_leave_building				sResult = "6. Exit Building"					BREAK
		CASE msf_st7_go_home					sResult = "7. Go Home"							BREAK
		CASE msf_st8_tv_start					sResult = "8. TV Start"							BREAK
		CASE msf_st9_tv_detonate				sResult = "9. TV Detonate"						BREAK
		CASE msf_st10_mission_passed			sResult = "10. Mission Passed"					BREAK
	ENDSWITCH
	
	RETURN sResult
ENDFUNC

FUNC BOOL IS_ENTITY_TOO_BIG(ENTITY_INDEX entity, VECTOR vMaxAllowableSize)
	IF NOT IS_VECTOR_ZERO(vMaxAllowableSize)	
		IF DOES_ENTITY_EXIST(entity)
			model_names eModel
			eModel = GET_ENTITY_MODEL(entity)
			
			vector vSizeMin,vSizeMax
			GET_MODEL_DIMENSIONS(eModel,vSizeMin,vSizeMax)
			
			//choppers seem to have unreasonably large dimensions. Much larger than their blades.
			If IS_THIS_MODEL_A_HELI(eModel)
				vMaxAllowableSize.x += 3
				vMaxAllowableSize.y += 3
			ENDIF
		
			IF vSizeMax.x - vSizeMin.x > vMaxAllowableSize.x
				CPRINTLN(DEBUG_MIKE, "IS_ENTITY_TOO_BIG - entity X too large")
				RETURN TRUE
			ELIF vSizeMax.y - vSizeMin.y > vMaxAllowableSize.y
				CPRINTLN(DEBUG_MIKE, "IS_ENTITY_TOO_BIG - entity Y too large")
				RETURN TRUE
			ELIF vSizeMax.z - vSizeMin.z > vMaxAllowableSize.z
				CPRINTLN(DEBUG_MIKE, "IS_ENTITY_TOO_BIG - entity Z too large")
				RETURN TRUE
			ELSE
				CPRINTLN(DEBUG_MIKE, "IS_ENTITY_TOO_BIG - entity within size limits")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// DOOR MANAGEMENT

//PURPOSE: Adds a new door to the doors array
PROC Doors_Add_New(INT iDoor, VECTOR vCoord, MODEL_NAMES model, BOOL bResetDoor = FALSE)
	TEXT_LABEL_23 strDoorName = "LESTER1A_DOOR_"
	strDoorName += iDoor
	
	sDoors[iDoor].iHash	= GET_HASH_KEY(strDoorName)
	ADD_DOOR_TO_SYSTEM(sDoors[iDoor].iHash, model, vCoord, FALSE)
	
	IF bResetDoor
		DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[iDoor].iHash, 1.0)
		DOOR_SYSTEM_SET_DOOR_STATE(sDoors[iDoor].iHash, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
		DOOR_SYSTEM_SET_DOOR_STATE(sDoors[iDoor].iHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	ENDIF
ENDPROC


FUNC BOOL Is_Door_LockState_Locked(DOOR_STATE_ENUM eDoorState)
	IF eDoorState = DOORSTATE_LOCKED
	OR eDoorState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
	OR eDoorState = DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//PURPOSE: Changes the state of the door
PROC Doors_Set_State(INT iDoor, BOOL lockState, FLOAT openRatio, INT iTime = 0)
	IF sDoors[iDoor].iHash != 0
		// No update over time, do NOW!
		IF iTime <= 0
			IF openRatio != DOORS_KEEP_CURRENT_RATIO
				DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[iDoor].iHash, openRatio, FALSE, TRUE)
			ENDIF
			
			IF lockState
				DOOR_SYSTEM_SET_DOOR_STATE(sDoors[iDoor].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
			ELSE
				DOOR_SYSTEM_SET_DOOR_STATE(sDoors[iDoor].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
			ENDIF
			
			sDoors[iDoor].iTimeStamp = 0
			sDoors[iDoor].iInterpTime = 0
			
		// Needs updating over time, mark for frame by frame update
		ELIF openRatio != DOORS_KEEP_CURRENT_RATIO 
			sDoors[iDoor].bChangingState = TRUE
			sDoors[iDoor].bDesiredLockState = lockState
			sDoors[iDoor].fDesiredOpenRatio = openRatio
			
			sDoors[iDoor].iTimeStamp = GET_GAME_TIMER()
			sDoors[iDoor].iInterpTime = iTime
			
			sDoors[iDoor].fStartOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(sDoors[iDoor].iHash)
		ELSE
			SCRIPT_ASSERT("DOOR_SYSTEM: CANNOT OPEN DOOR OVER TIME TO KEEP THE SAME DOOR RATIO, POINTLESS!")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Obtains the state of the door
PROC Doors_Get_State(INT iDoor, BOOL &bLockState, FLOAT &fOpenRatio)
	IF sDoors[iDoor].iHash != 0
		fOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(sDoors[iDoor].iHash)
		DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE(sDoors[iDoor].iHash)
		//DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_PENDING_STATE(sDoors[iDoor].iHash)
		
		bLockState = Is_Door_LockState_Locked(eDoorState)
	ENDIF
ENDPROC

//PURPOSE: Resets all the doors streamed in
PROC Doors_Reset_All()
	INT x
	REPEAT COUNT_OF(sDoors) x
		IF sDoors[x].iHash != 0
			DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[x].iHash, 0.0, FALSE, TRUE)
			DOOR_SYSTEM_SET_DOOR_STATE(sDoors[x].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
		ENDIF
		
		sDoors[x].bChangingState 	= FALSE
		sDoors[x].iInterpTime 		= 0
		sDoors[x].iTimeStamp		= 0
		sDoors[x].fDesiredOpenRatio	= 0.0
		sDoors[x].bDesiredLockState = FALSE
		sDoors[x].fStartOpenRatio	= 0.0
	ENDREPEAT
ENDPROC


//PURPOSE: Manage locking and opening of doors
PROC Manage_Doors()
	INT i
	FOR i = 0 TO enum_to_int(mdf_num_doors) -1
		IF sDoors[i].iHash != 0
		AND sDoors[i].bChangingState
		
			BOOL bLockState = FALSE
			FLOAT fOpenRatio = 0.0
			Doors_Get_State(i, bLockState, fOpenRatio)
	
			// If state does not match then update to new state
			IF bLockState != sDoors[i].bDesiredLockState
			OR fOpenRatio != sDoors[i].fDesiredOpenRatio

				IF sDoors[i].iInterpTime > 0
				AND sDoors[i].iTimeStamp > 0

					FLOAT fInterpProg = CLAMP((GET_GAME_TIMER() - sDoors[i].iTimeStamp)/TO_FLOAT(sDoors[i].iInterpTime), 0.0, 1.0)
//					PRINTLN("fInterpProg: ", fInterpProg)
//					SCRIPT_ASSERT("DOOR SYSTEM: DOOR PROG CALC")
					IF fInterpProg != 1.0
						
						// change in progress
						fOpenRatio = sDoors[i].fStartOpenRatio + (fInterpProg * (sDoors[i].fDesiredOpenRatio - sDoors[i].fStartOpenRatio))
						bLockState = TRUE
						
//						PRINTLN("DOOR SYSTEM: CALCULATED OPEN RATIO: ", fOpenRatio)
//						PRINTLN("DOOR SYSTEM: startRatio: ", sDoors[i].fStartOpenRatio)
//						PRINTLN("DOOR SYSTEM: desiredRatio: ", sDoors[i].fDesiredOpenRatio)
//						PRINTLN("DOOR SYSTEM: currentRatio: ", fOpenRatio)
//						SCRIPT_ASSERT("DOOR SYSTEM: PROCESSING DOOR SWING")
					ELSE
//						SCRIPT_ASSERT("DOOR SYSTEM: DOOR FINISHED")
						
						// door finished
						bLockState = sDoors[i].bDesiredLockState
						fOpenRatio = sDoors[i].fDesiredOpenRatio
						sDoors[i].bChangingState = FALSE

						sDoors[i].iInterpTime		= 0
						sDoors[i].iTimeStamp 		= 0
						sDoors[i].bChangingState 	= FALSE
					ENDIF
				ELSE
//					SCRIPT_ASSERT("DOOR SYSTEM: DOOR NOT INTERP TIME SET!")
					// Set to the desired values as no time was present
					IF sDoors[i].bDesiredLockState
						bLockState = TRUE
					ELSE
						bLockState = FALSE
					ENDIF
					fOpenRatio = sDoors[i].fDesiredOpenRatio
					
					sDoors[i].iInterpTime		= 0
					sDoors[i].iTimeStamp 		= 0
					sDoors[i].bChangingState 	= FALSE
				ENDIF
				
				IF fOpenRatio != DOORS_KEEP_CURRENT_RATIO
					DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[i].iHash, fOpenRatio, FALSE, TRUE)
				ENDIF
				
				// Set the door
				IF bLockState
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[i].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
				ELSE
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[i].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
			ELSE
//				PRINTLN("CurrentLockState: ", bLockState)
//				PRINTLN("CurrentOpenRatio: ", fOpenRatio)
//				PRINTLN("DesiredLockState: ", sDoors[i].bDesiredLockState)
//				PRINTLN("DesiredOpenRatio: ", sDoors[i].fDesiredOpenRatio)
//				SCRIPT_ASSERT("DOOR SYSTEM: DOOR ALREADY SET")
				
				DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[i].iHash, fOpenRatio, FALSE, TRUE)
				
				// Set the door
				IF bLockState
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[i].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
				ELSE
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[i].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
			
				// door already in the desired stage
				sDoors[i].iInterpTime		= 0
				sDoors[i].iTimeStamp 		= 0
				sDoors[i].bChangingState 	= FALSE
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///   Checks if an entity is ready, to be animated, drawn, be seen by the player, etc
/// PARAMS:
///    entity - entity you are testing
/// RETURNS:
///    Returns TRUE if the entity is ready
FUNC BOOL IS_ENTITY_READY(ENTITY_INDEX entity)
	
	IF DOES_ENTITY_EXIST(entity)
		SWITCH GET_ENTITY_TYPE(entity)
			CASE ET_PED
				IF NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(entity))
				AND DOES_ENTITY_HAVE_DRAWABLE(entity)
					RETURN TRUE
				ENDIF
			BREAK
			CASE ET_VEHICLE
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity))
				AND DOES_ENTITY_HAVE_DRAWABLE(entity)
					RETURN TRUE
				ENDIF
			BREAK
			CASE ET_OBJECT
				IF DOES_ENTITY_HAVE_DRAWABLE(entity)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Puts the player in default variation of programmer outfit and sets it as acquired
PROC PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
	
	IF g_replay.iReplayInt[0] = -1
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_0, TRUE)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GILET_0)
	ELSE
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, int_to_enum(PED_COMP_NAME_ENUM, g_replay.iReplayInt[0]), TRUE)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, int_to_enum(PED_COMP_NAME_ENUM, g_replay.iReplayInt[0]))
	ENDIF
	
	IF g_replay.iReplayInt[1] = -1
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0, TRUE)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0)
	ELSE
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, int_to_enum(PED_COMP_NAME_ENUM, g_replay.iReplayInt[1]), TRUE)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, int_to_enum(PED_COMP_NAME_ENUM, g_replay.iReplayInt[1]))
	ENDIF
	
	SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
ENDPROC

/// PURPOSE:
///    Sets any porgrammer outfit items as not acquired and puts player in the default outfit
PROC REMOVE_PROGRAMMER_OUTFIT()
	
	// torsos
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_0)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_0, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_1)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_1, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_2)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_2, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_3)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_3, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_4)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_4, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_5)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_5, FALSE)
	ENDIF
	
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_1)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_1, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_2)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_2, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_3)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_3, FALSE)
	ENDIF
	IF NOT IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_4)
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_4, FALSE)
	ENDIF
	
	RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
	
ENDPROC

//PURPOSE: Create blocking areas
PROC Add_Scenario_Blocking()

	// Lesters
	SET_ROADS_IN_ANGLED_AREA(<<1368.168823,-1686.163818,68.780426>>, <<1189.980713,-1764.964233,31.711254>>, 91.5, FALSE, FALSE)
	
	// Store
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<130.635315,-199.948563,55.317688>> - <<12.187500,14.375000,2.125000>>, 
		<<130.635315,-199.948563,55.317688>> + <<12.187500,14.375000,2.125000>>, FALSE)
												
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<130.635315,-199.948563,55.317688>> - <<12.187500,14.375000,2.125000>>, 
		<<130.635315,-199.948563,55.317688>> + <<12.187500,14.375000,2.125000>>)
	CLEAR_AREA_OF_VEHICLES(<<128.87, -205.99, 53.52>>, 30.0)

	// block the whole of the offices
	sbi_offices = ADD_SCENARIO_BLOCKING_AREA(<<-1057.517822,-238.242065,48.295681>> - <<43.000000,29.000000,11.812500>>,
	 	<<-1057.517822,-238.242065,48.295681>> + <<43.000000,29.000000,11.812500>>, TRUE)
	SET_PED_PATHS_IN_AREA(<<-1057.517822,-238.242065,48.295681>> - <<43.000000,29.000000,11.812500>>,
	 	<<-1057.517822,-238.242065,48.295681>> + <<43.000000,29.000000,11.812500>>, FALSE)
	SET_PED_NON_CREATION_AREA(<<-1057.517822,-238.242065,48.295681>> - <<43.000000,29.000000,11.812500>>,
	 	<<-1057.517822,-238.242065,48.295681>> + <<43.000000,29.000000,11.812500>>)
	CLEAR_AREA_OF_PEDS(<<-1057.517822,-238.242065,48.295681>>, 266.250)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1057.517822,-238.242065,48.295681>> - <<43.000000,29.000000,11.812500>>,
	 	<<-1057.517822,-238.242065,48.295681>> + <<43.000000,29.000000,11.812500>>, FALSE)

ENDPROC

PROC Reset_Scenario_Blocking()
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<1368.168823,-1686.163818,68.780426>>, <<1189.980713,-1764.964233,31.711254>>, 91.5)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1057.517822,-238.242065,48.295681>> - <<43.000000,29.000000,11.812500>>,
	 	<<-1057.517822,-238.242065,48.295681>> + <<43.000000,29.000000,11.812500>>, TRUE)
	SET_PED_PATHS_IN_AREA(<<-1057.517822,-238.242065,48.295681>> - <<43.000000,29.000000,11.812500>>,
	 	<<-1057.517822,-238.242065,48.295681>> + <<43.000000,29.000000,11.812500>>, TRUE)
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_offices)
	REMOVE_SCENARIO_BLOCKING_AREAS()
ENDPROC

//PURPOSE: Lines up the peds chairs
FUNC BOOL Lineup_Boardroom_Chairs()

	// SPARE CHAIR[A]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_spare_chair_a].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1043.98, -238.39, 43.02>>,0.5, PROP_OFF_CHAIR_01)
			objects[mof_boardroom_spare_chair_a].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1043.98, -238.39, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_spare_chair_a].id, <<-1043.74, -238.72, 43.02>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_spare_chair_a].id, <<0.00, 0.00, 152.50>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_spare_chair_a].id, TRUE)
		ENDIF
	ENDIF
	
	// SPARE CHAIR[B]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_spare_chair_b].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1046.38, -233.24, 43.02>>,0.5, PROP_OFF_CHAIR_01)
			objects[mof_boardroom_spare_chair_b].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1046.38, -233.24, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_spare_chair_b].id, <<-1046.89, -232.61, 43.02>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_spare_chair_b].id, <<0.00, 0.00, 63.15>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_spare_chair_b].id, TRUE)
		ENDIF
	ENDIF
	
	// SPARE CHAIR[C]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_spare_chair_c].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1044.53, -232.09, 43.02>>,0.5, PROP_OFF_CHAIR_01)
			objects[mof_boardroom_spare_chair_c].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1044.53, -232.09, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_spare_chair_c].id, <<-1044.78, -231.92, 43.02>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_spare_chair_c].id, <<-0.00, 0.00, -27.50>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_spare_chair_c].id, TRUE)
		ENDIF
	ENDIF
	
	// SPARE CHAIR[D]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_spare_chair_d].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1042.13, -237.24, 43.02>>,0.5, PROP_OFF_CHAIR_01)
			objects[mof_boardroom_spare_chair_d].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1042.13, -237.24, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_spare_chair_d].id, <<-1041.90, -237.63, 43.02>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_spare_chair_d].id, <<-0.00, 0.00, -92.50>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_spare_chair_d].id, TRUE)
		ENDIF
	ENDIF

	// MALE[A]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_m_a_chair].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1043.03, -235.22, 43.02>>,0.5, PROP_OFF_CHAIR_01)
		AND NOT IS_PED_INJURED(peds[mpf_boardroom_m_a])
			objects[mof_boardroom_m_a_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1043.03, -235.22, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_m_a_chair].id, <<-1042.8325, -235.5175, 43.0300>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_m_a_chair].id, <<-0.00, -0.04, -72.11>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_m_a_chair].id, TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_boardroom_m_a_chair].id, peds[mpf_boardroom_m_a], FALSE)
		ENDIF
	ENDIF
	
	// MALE[B]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_m_b_chair].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1045.90, -234.22, 43.02>>,0.5, PROP_OFF_CHAIR_01)
		AND NOT IS_PED_INJURED(peds[mpf_boardroom_m_b])
			objects[mof_boardroom_m_b_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1045.90, -234.22, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_m_b_chair].id, <<-1046.3765, -234.1675, 43.0300>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_m_b_chair].id, <<-0.03, -0.03, 88.64>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_m_b_chair].id, TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_boardroom_m_b_chair].id, peds[mpf_boardroom_m_b], FALSE)
		ENDIF
	ENDIF
	
	// MALE[E]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_m_e_chair].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1043.49, -234.33, 43.02>>,0.5, PROP_OFF_CHAIR_01)
		AND NOT IS_PED_INJURED(peds[mpf_boardroom_m_e])
			objects[mof_boardroom_m_e_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1043.49, -234.33, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_m_e_chair].id, <<-1043.3405, -234.2000, 43.0300>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_m_e_chair].id, <<-0.0300, -0.0300, -61.9200>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_m_e_chair].id, TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_boardroom_m_e_chair].id, peds[mpf_boardroom_m_e], FALSE)
		ENDIF
	ENDIF
	
	// MALE[F]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_m_f_chair].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1045.02, -236.15, 43.02>>,0.5, PROP_OFF_CHAIR_01)
		AND NOT IS_PED_INJURED(peds[mpf_boardroom_m_f])
			objects[mof_boardroom_m_f_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1045.02, -236.15, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_m_f_chair].id,<<-1044.9955, -236.9400, 43.0225>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_m_f_chair].id, <<0.0000, 0.0700, 121.7650>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_m_f_chair].id, TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_boardroom_m_f_chair].id, peds[mpf_boardroom_m_f], FALSE)
		ENDIF
	ENDIF
	
//	// FEMALE[A]
//	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_f_a_chair].id)
//		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1044.06, -233.34, 43.02>>,0.5, PROP_OFF_CHAIR_01)
//		AND NOT IS_PED_INJURED(peds[mpf_boardroom_f_a])	
//			objects[mof_boardroom_f_a_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1044.06, -233.34, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
//			SET_ENTITY_COORDS(objects[mof_boardroom_f_a_chair].id, <<-1044.1935, -233.0600, 43.0175>>)
//			SET_ENTITY_ROTATION(objects[mof_boardroom_f_a_chair].id, <<0.0000, 0.0000, -50.7650>>)
//			FREEZE_ENTITY_POSITION(objects[mof_boardroom_f_a_chair].id, TRUE)
//			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_boardroom_f_a_chair].id, peds[mpf_boardroom_f_a], FALSE)
//		ENDIF
//	ENDIF
	
	// FEMALE[B]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_f_b_chair].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1045.48, -235.26, 43.02>>,0.5, PROP_OFF_CHAIR_01)
		AND NOT IS_PED_INJURED(peds[mpf_boardroom_f_b])
			objects[mof_boardroom_f_b_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1045.48, -235.26, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_f_b_chair].id, <<-1045.6705, -235.5025, 43.0300>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_f_b_chair].id, <<0.0200, 0.0200, 120.5900>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_f_b_chair].id, TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_boardroom_f_b_chair].id, peds[mpf_boardroom_f_b], FALSE)
		ENDIF
	ENDIF
	
	// FEMALE[C]
	IF NOT DOES_ENTITY_EXIST(objects[mof_boardroom_f_c_chair].id)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1044.45, -237.14, 43.02>>,0.5, PROP_OFF_CHAIR_01)
		AND NOT IS_PED_INJURED(peds[mpf_boardroom_f_c])
			objects[mof_boardroom_f_c_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1044.45, -237.14, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			SET_ENTITY_COORDS(objects[mof_boardroom_f_c_chair].id, <<-1044.2515, -237.9050, 43.0300>>)
			SET_ENTITY_ROTATION(objects[mof_boardroom_f_c_chair].id, <<0.0000, 0.0000, 129.2400>>)
			FREEZE_ENTITY_POSITION(objects[mof_boardroom_f_c_chair].id, TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_boardroom_f_c_chair].id, peds[mpf_boardroom_f_c], FALSE)
		ENDIF
	ENDIF
	
//	IF DOES_ENTITY_EXIST(objects[mof_boardroom_f_a_chair].id)
	IF DOES_ENTITY_EXIST(objects[mof_boardroom_f_b_chair].id)
	AND DOES_ENTITY_EXIST(objects[mof_boardroom_f_c_chair].id)
	AND DOES_ENTITY_EXIST(objects[mof_boardroom_m_a_chair].id)
	AND DOES_ENTITY_EXIST(objects[mof_boardroom_m_b_chair].id)
	AND DOES_ENTITY_EXIST(objects[mof_boardroom_m_e_chair].id)
	AND DOES_ENTITY_EXIST(objects[mof_boardroom_m_f_chair].id)			
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SAFE_PLAY_SYNCHRONIZED_ENTITY(PROP_STRUCT &prop, INT sceneID, STRING animName, STRING animDict, FLOAT fBlendIn, FLOAT fBlendOut, SYNCED_SCENE_PLAYBACK_FLAGS eFlags = SYNCED_SCENE_NONE)

	prop.v_coord = <<0,0,0>>
	prop.model = DUMMY_MODEL_FOR_SCRIPT
	prop.bInSynchedScene = TRUE
	prop.adName = animDict
	prop.animName = animName
	PLAY_SYNCHRONIZED_ENTITY_ANIM(prop.id, sceneID, animName, animDict, fBlendIn, fBlendOut, enum_to_int(eFlags))
	
ENDPROC

//PURPOSE: Keeps track of a map entity playing a synched scene
PROC SAFE_PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(PROP_STRUCT &prop, VECTOR vCoord, MODEL_NAMES model, INT sceneID, STRING animDict, STRING animName, FLOAT fBlend)
	prop.id = NULL
	prop.bCustom = FALSE
	prop.v_coord = vCoord
	prop.model = model
	prop.bInSynchedScene = TRUE

	PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(vCoord, 1.0, model, sceneId, animName, animDict, fBlend)
ENDPROC

//PURPOSE: Will choose the right way to stop an synched prop
PROC SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(PROP_STRUCT &prop, FLOAT fBlend)
	IF prop.bInSynchedScene
		IF prop.id = NULL
			IF (NOT IS_VECTOR_ZERO(prop.v_coord))
			AND prop.model != DUMMY_MODEL_FOR_SCRIPT
				STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(prop.v_coord, 1.0, prop.model, fBlend)
			ENDIF
		ELSE
			IF NOT IS_STRING_NULL_OR_EMPTY(prop.adName)
			AND NOT IS_STRING_NULL_OR_EMPTY(prop.animName)
				IF IS_ENTITY_PLAYING_ANIM(prop.id, prop.adName, prop.animName)
					STOP_SYNCHRONIZED_ENTITY_ANIM(prop.id, fBlend, TRUE)
				ENDIF
			ENDIF
		ENDIF
		prop.model				= DUMMY_MODEL_FOR_SCRIPT
		prop.adName				= ""
		prop.animName			= ""
		prop.bInSynchedScene 	= FALSE
	ENDIF
ENDPROC

// create a prop using the prop struct
PROC Create_Prop(PROP_STRUCT &prop, MODEL_NAMES model, VECTOR pos, FLOAT fHeading = 0.0)
	prop.id = CREATE_OBJECT(model, pos)
	prop.bCustom = TRUE
	
	IF fHeading != 0
		SET_ENTITY_HEADING(prop.id, fHeading)
	ENDIF
ENDPROC

//PURPOSE: Safely blip a coordindate
PROC SAFE_BLIP_COORD(BLIP_INDEX &blip, VECTOR vec, BOOL bShowRoute = FALSE)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_COORD(vec, bShowRoute)
ENDPROC

//PURPOSE: Safely blip a ped
PROC SAFE_BLIP_PED(BLIP_INDEX &blip, PED_INDEX ped, BOOL bIsEnemy = FALSE)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_PED(ped, bIsEnemy)
ENDPROC

//PURPOSE: Safely blip a vehicle
PROC SAFE_BLIP_VEHICLE(BLIP_INDEX &blip, VEHICLE_INDEX veh, BOOL bIsEnemy = FALSE)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_VEHICLE(veh, bIsEnemy)
ENDPROC

//PURPOSE: Safely blip an object
PROC SAFE_BLIP_OBJECT(BLIP_INDEX &blip, OBJECT_INDEX objID)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_OBJECT(objID)
ENDPROC

//PURPOSE: Counts the number of popups open.
FUNC INT GET_POPUPS_OPEN()
	INT count, x
	FOR x = 0 TO 11
		IF NOT sPopups[x].bClosed 	count++ 	ENDIF
	ENDFOR
	
	RETURN count
ENDFUNC

PROC DISABLE_PLAYERS_WEAPONS_THIS_FRAME()

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_UNARMED)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_MELEE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_HANDGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_SHOTGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_SMG)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_AUTO_RIFLE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_SNIPER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON_SPECIAL)
	
	// force unarmed
	WEAPON_TYPE playerWeap
	IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerWeap)
		IF !(playerWeap = WEAPONTYPE_UNARMED OR playerWeap = WEAPONTYPE_OBJECT)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: To set the correct ped variation for the li
PROC Set_Life_Invader_Office_Ped_Variation(mission_ped_flag mpf)
	IF DOES_ENTITY_EXIST(peds[mpf])
	AND (NOT IS_PED_INJURED(peds[mpf]))
		PED_VARIATION_STRUCT	sPedVar
		
		INT i
		REPEAT NUM_PED_COMPONENTS i
			sPedVar.iDrawableVariation[i] = -1
			sPedVar.iTextureVariation[i] = -1
		ENDREPEAT

		SWITCH mpf
			CASE mpf_boardroom_norris
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
			BREAK
			CASE mpf_interviewer
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 3
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 0
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_interviewee_m
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 1
				sPedVar.iTextureVariation[PED_COMP_LEG] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_interviewee_f
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_HAND] = 0
				sPedVar.iTextureVariation[PED_COMP_HAND] = 0
			BREAK
			CASE mpf_gamer
			FALLTHRU
			CASE mpf_coffee_1
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 2
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 4
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 1
				sPedVar.iTextureVariation[PED_COMP_LEG] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_milk_f
			FALLTHRU
			CASE mpf_hr
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_HAND] = 0
				sPedVar.iTextureVariation[PED_COMP_HAND] = 0
			BREAK
			CASE mpf_coffee_2
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_office_a
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 2
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 0
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_office_b
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 1
				sPedVar.iTextureVariation[PED_COMP_LEG] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_office_c
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 1
				sPedVar.iTextureVariation[PED_COMP_LEG] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_office_d
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_office_boss
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 0
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 2
			BREAK
			CASE mpf_office_paper_f
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 1
				sPedVar.iTextureVariation[PED_COMP_LEG] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAND] = 0
				sPedVar.iTextureVariation[PED_COMP_HAND] = 0
			BREAK
			CASE mpf_office_paper_m
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_boardroom_m_a
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_boardroom_m_b
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 5
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 1
				sPedVar.iTextureVariation[PED_COMP_LEG] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_boardroom_m_e
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 1
				sPedVar.iTextureVariation[PED_COMP_LEG] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
			CASE mpf_boardroom_m_f
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_SPECIAL] = 1
				sPedVar.iTextureVariation[PED_COMP_SPECIAL] = 0
			BREAK
//			CASE mpf_boardroom_f_a
//				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
//				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
//				
//				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
//				sPedVar.iTextureVariation[PED_COMP_HAIR] = 2
//				
//				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
//				sPedVar.iTextureVariation[PED_COMP_TORSO] = 1
//				
//				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
//				sPedVar.iTextureVariation[PED_COMP_LEG] = 0
//				
//				sPedVar.iDrawableVariation[PED_COMP_HAND] = 0
//				sPedVar.iTextureVariation[PED_COMP_HAND] = 0
//			BREAK
			CASE mpf_boardroom_f_b
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 0
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 0
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 0
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 0
				sPedVar.iTextureVariation[PED_COMP_LEG] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_HAND] = 0
				sPedVar.iTextureVariation[PED_COMP_HAND] = 0
			BREAK
			CASE mpf_boardroom_f_c
				sPedVar.iDrawableVariation[PED_COMP_HEAD] = 1
				sPedVar.iTextureVariation[PED_COMP_HEAD] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAIR] = 1
				sPedVar.iTextureVariation[PED_COMP_HAIR] = 1
				
				sPedVar.iDrawableVariation[PED_COMP_TORSO] = 1
				sPedVar.iTextureVariation[PED_COMP_TORSO] = 2
				
				sPedVar.iDrawableVariation[PED_COMP_LEG] = 1
				sPedVar.iTextureVariation[PED_COMP_LEG] = 0
				
				sPedVar.iDrawableVariation[PED_COMP_HAND] = 0
				sPedVar.iTextureVariation[PED_COMP_HAND] = 0
			BREAK
			DEFAULT
				SCRIPT_ASSERT("Not a life invader office ped, possibly a player ped or jaynorris")
			BREAK
		ENDSWITCH

		REPEAT NUM_PED_COMPONENTS i
			IF sPedVar.iDrawableVariation[i] != -1
			OR sPedVar.iTextureVariation[i] != -1
				SET_PED_COMPONENT_VARIATION(peds[mpf], int_to_enum(PED_COMPONENT, i), sPedVar.iDrawableVariation[i], sPedVar.iTextureVariation[i])
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_VISIBLY_TARGETTING_TRACY()

	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND DOES_ENTITY_EXIST(peds[mpf_tracey])
	AND NOT IS_PED_INJURED(peds[mpf_tracey])
		IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), peds[mpf_tracey])
		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), peds[mpf_tracey]) 
		
			// player is targetting ped, set targetting dist based on whether he is using melee or ranged attac		
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.21313, 182.88890, 75.42337>>, <<-796.66968, 186.31412, 71.60547>>, 10.69) // player is in kitchen
				IF CAN_PED_SEE_PLAYER(peds[mpf_tracey]) // player can be seen by other ped
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_PLAYER_THREATENED_TRACY()

	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND DOES_ENTITY_EXIST(peds[mpf_tracey])
	AND NOT IS_PED_INJURED(peds[mpf_tracey])
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_tracey], PLAYER_PED_ID())
			RETURN TRUE
		ENDIF

		IF IS_PLAYER_SHOOTING_NEAR_PED(peds[mpf_tracey], TRUE)
			RETURN TRUE
		ENDIF

		IF IS_PLAYER_VISIBLY_TARGETTING_TRACY()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC


PROC SET_TRACY_VARS()
	SET_PED_COMPONENT_VARIATION(peds[mpf_tracey], PED_COMP_TORSO, 1, 1)
	SET_PED_COMPONENT_VARIATION(peds[mpf_tracey], PED_COMP_LEG, 5, 0)
	SET_PED_COMPONENT_VARIATION(peds[mpf_tracey], PED_COMP_FEET, 2, 0)
	SET_PED_COMPONENT_VARIATION(peds[mpf_tracey], PED_COMP_SPECIAL, 1,0)
ENDPROC

PROC MONITOR_PLAYER_BEING_ARMED()
	IF IS_INTERIOR_SCENE()
	AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_living_room
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)

		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, -1)
		ENDIF
	
		DISABLE_SELECTOR_THIS_FRAME()
	ENDIF
ENDPROC

PROC MONITOR_SOUNDS()
	#IF IS_DEBUG_BUILD
		IF eSoundState > TSS_WAIT_PLAY_REMOTE_RING
			CPRINTLN(DEBUG_MIKE, "GET_GAME_TIMER() - iSoundTimer ==  ", GET_GAME_TIMER() - iSoundTimer)
		ENDIF
	#ENDIF

	SWITCH eSoundState
		CASE TSS_WAIT_PLAY_REMOTE_RING
			IF bDetonated
			
				IF iSoundID = -1
					iSoundID = GET_SOUND_ID()
				ENDIF
			
				IF DOES_ENTITY_EXIST(objects[mof_phone].id)
					PLAY_SOUND_FROM_ENTITY(iSoundID, "Remote_Ring", objects[mof_phone].id, "Phone_SoundSet_Michael")
					iSoundTimer = GET_GAME_TIMER()
					eSoundState = TSS_WAIT_ANSWER
				ENDIF
			ENDIF
		BREAK
		
		CASE TSS_WAIT_ANSWER
		
			IF iSoundID = -1
				iSoundID = GET_SOUND_ID()
			ENDIF

			IF (GET_GAME_TIMER() - iSoundTimer) > 5000
				IF NOT HAS_SOUND_FINISHED(iSoundID)
					STOP_SOUND(iSoundID)
				ENDIF
				PLAY_SOUND_FROM_ENTITY(iSoundID, "Answer_Phone", objects[mof_phone].id, "Lester1B_Sounds")
				iSoundTimer = GET_GAME_TIMER()
				eSoundState = TSS_END
			ENDIF
		BREAK

	ENDSWITCH
ENDPROC

PROC SWITCH_CAMERA(CAM_SHOTS nextCam, INT iInterpTime = 0)
	FLOAT fov = GET_CAM_FOV(cam_tv)
	SET_CAM_PARAMS(cam_tv, vCamPos[nextCam], vCamRot[nextCam], fov, iInterpTime)
	eCameraCurrent = nextCam
ENDPROC

PROC CYCLE_CAMERAS()
      // Cycle through cameras                                                
	IF DOES_CAM_EXIST(cam_tv)
		IF IS_CAM_ACTIVE(cam_tv)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_NEXT_CAMERA)   
				IF IS_XBOX360_VERSION () // Has no CAMERA_ULTRA_CLOSE 
					SWITCH eCameraCurrent
						CASE CAMS_CLOSE
							SWITCH_CAMERA(CAMS_ROOM)
						BREAK
						
						CASE CAMS_ROOM
							SWITCH_CAMERA(CAMS_SCRIPT)
						BREAK

						CASE CAMS_SCRIPT
							SWITCH_CAMERA(CAMS_CLOSE)
						BREAK
					ENDSWITCH
				ELSE
					SWITCH eCameraCurrent
						CASE CAMS_ULTRA
							SWITCH_CAMERA(CAMS_CLOSE)
						BREAK
						
						CASE CAMS_CLOSE
							SWITCH_CAMERA(CAMS_ROOM)
						BREAK
						
						CASE CAMS_ROOM
							SWITCH_CAMERA(CAMS_SCRIPT)
						BREAK
						
						CASE CAMS_SCRIPT
							SWITCH_CAMERA(CAMS_ULTRA)
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_ANIM_STRING(MIKE_ANIM_STATE anim)
	SWITCH anim	

		CASE MAS_R_IDLE
			RETURN "michael_tv_remote_idle"
		BREAK
		
		CASE MAS_R_FIDG
			RETURN "michael_tv_remote_fidget"
		BREAK
		
		CASE MAS_R_PRESS
			RETURN "michael_tv_remote_button_press_faster" //"micheal_tv_remote_button_press"
		FALLTHRU
		
		CASE MAS_R_PRESS_HOLD_INTRO
			RETURN "michael_tv_remote_volume_into"
		BREAK
		
		CASE MAS_R_PRESS_HOLD_LOOP
			RETURN "michael_tv_remote_volume"
		BREAK
		
		CASE MAS_R_PRESS_HOLD_OUTRO
			RETURN "michael_tv_remote_volume_outro"
		BREAK
		
		CASE MAS_P_INTO
			RETURN "michael_phone_into"
		BREAK
		
		CASE MAS_P_AWAY
			RETURN "michael_phone_menu_exit"
		BREAK
		
		CASE MAS_P_IDLE
			RETURN "michael_phone_idle"
		BREAK
		
		CASE MAS_P_SCROLL
			RETURN "michael_phone_single_screen_scroll"
		BREAK
		
		CASE MAS_P_PRESS
			RETURN "michael_phone_single_screen_press"
		BREAK
		
		CASE MAS_P_DETO_INTRO
			RETURN "michael_phone_detonate_to_wait_idle"
		BREAK
		
		CASE MAS_P_DETO_WAIT
			RETURN "michael_wait_idle"
		BREAK
		
		CASE MAS_P_DETO_REACT
			RETURN "michael_explosion_reaction_to_wait_idle"
		BREAK
		
		CASE MAS_P_OUTRO
			RETURN "michael_wait_idle_to_standing_idle_leadout"
		BREAK
	ENDSWITCH
	
	RETURN "NONE"
ENDFUNC

PROC SET_NEW_ANIM_TASK(MIKE_ANIM_STATE anim, FLOAT BlendIn = INSTANT_BLEND_IN, FLOAT BlendOut = NORMAL_BLEND_OUT, ANIMATION_FLAGS flags = AF_DEFAULT, BOOL bForceUpdate = FALSE)
	IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(anim))
		TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(anim), ig_1_coord, ig_1_rot, BlendIn, BlendOut, -1, flags, 0, DEFAULT, AIK_DISABLE_LEG_IK)
		IF bForceUpdate
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		ENDIF
		eAnimState = anim
	ENDIF
ENDPROC

FUNC BOOL HAS_USED_PHONE_NAV()
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_UP)
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_DOWN)
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_LEFT)
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_RIGHT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MONITOR_FOR_ANIM_END()
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_IDLE))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_IDLE)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_FIDG))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_FIDG)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_PRESS))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_PRESS)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_PRESS_HOLD_INTRO))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_PRESS_HOLD_INTRO)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_PRESS_HOLD_LOOP))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_PRESS_HOLD_LOOP)) > 0.9
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_PRESS_HOLD_OUTRO))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_PRESS_HOLD_OUTRO)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_INTO))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_INTO)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_AWAY))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_AWAY)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_IDLE))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_IDLE)) > 0.9
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_SCROLL))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_SCROLL)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_PRESS))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_PRESS)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_INTRO))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_INTRO)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_WAIT))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_WAIT)) > 0.9
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_REACT))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_REACT)) >= 1.0
			RETURN TRUE
		ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO))
		IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO)) > 1.0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_PLAYER_CHANGING_VOLUME()
	INT iAxis, iDeadZone
	iDeadZone = 32

	iAxis = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_Y) - 127	

	IF iAxis > 0+iDeadzone
	OR iAxis < 0-iDeadzone
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD

PROC PRINT_MICHAEL_ANIM_STAGE()

	TEXT_LABEL_63 strDebug
	strDebug = "MichaelAnimStage: "
	
	SWITCH eAnimState
		CASE MAS_NULL					strDebug += "MAS_NULL"							BREAK
		CASE MAS_R_IDLE					strDebug += "MAS_R_IDLE"						BREAK
		CASE MAS_R_FIDG					strDebug += "MAS_R_FIDG"						BREAK
		CASE MAS_R_PRESS				strDebug += "MAS_R_PRESS"						BREAK
		CASE MAS_R_PRESS_HOLD_INTRO		strDebug += "MAS_R_PRESS_INTRO"					BREAK
		CASE MAS_R_PRESS_HOLD_LOOP		strDebug += "MAS_R_PRESS_LOOP"					BREAK
		CASE MAS_R_PRESS_HOLD_OUTRO		strDebug += "MAS_R_PRESS_OUTRO"					BREAK
		CASE MAS_P_INTO					strDebug += "MAS_P_INTO"						BREAK
		CASE MAS_P_AWAY					strDebug += "MAS_P_AWAY"						BREAK
		CASE MAS_P_IDLE					strDebug += "MAS_P_IDLE"						BREAK
		CASE MAS_P_SCROLL				strDebug += "MAS_P_SCROLL"						BREAK
		CASE MAS_P_PRESS				strDebug += "MAS_P_PRESS"						BREAK
		CASE MAS_P_DETO_INTRO			strDebug += "MAS_P_DETO_INTRO"					BREAK
		CASE MAS_P_DETO_WAIT			strDebug += "MAS_P_DETO_WAIT"					BREAK
		CASE MAS_P_DETO_REACT			strDebug += "MAS_P_DETO_REACT"					BREAK
		CASE MAS_P_OUTRO				strDebug += "MAS_P_OUTRO"						BREAK
	ENDSWITCH

	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.5, 0.0>>, 255, 255, 255, 255)
	strDebug = "TV timer: "
	strDebug += GET_GAME_TIMER() - iDetonationTimer
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.525, 0.0>>, 255, 255, 255, 255)
	strDebug = "Detonate timer: "
	strDebug += GET_GAME_TIMER() - iDetonationCallTimer
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.55, 0.0>>, 255, 255, 255, 255)

ENDPROC

#ENDIF

PROC MICHAEL_ANIM_MANAGER(BOOL bCallingEnabled)	
	
	IF eAnimState = MAS_P_DETO_INTRO
	OR eAnimState = MAS_P_DETO_WAIT
	OR eAnimState = MAS_P_DETO_REACT
	OR eAnimState = MAS_P_OUTRO
		RUN_TV_TRANSPORT_CONTROLS(FALSE, FALSE)
	ELSE
		RUN_TV_TRANSPORT_CONTROLS(FALSE)
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
	
	IF bCallingEnabled
	AND (eAnimState = MAS_R_IDLE
	OR eAnimState = MAS_R_FIDG
	OR eAnimState = MAS_P_IDLE
	OR eAnimState = MAS_P_AWAY
	OR eAnimState = MAS_P_DETO_INTRO
	OR eAnimState = MAS_P_DETO_REACT
	OR eAnimState = MAS_P_DETO_WAIT
	OR eAnimState = MAS_P_INTO
	OR eAnimState = MAS_P_OUTRO
	OR eAnimState = MAS_P_PRESS
	OR eAnimState = MAS_P_SCROLL)
	
		IF IS_CELLPHONE_DISABLED()
			DISABLE_CELLPHONE(FALSE)
		ENDIF
	ELSE
		IF NOT IS_CELLPHONE_DISABLED()
			DISABLE_CELLPHONE(TRUE)
		ENDIF
	ENDIF
	
	IF eAnimState = MAS_R_IDLE
	OR eAnimState = MAS_R_FIDG
	
		// Just got the phone out, transition to phone idle
		IF IS_PHONE_ONSCREEN(FALSE)
			SET_NEW_ANIM_TASK(MAS_P_INTO, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			
		// Play change channel anim
		ELIF IS_INPUT_ATTEMPTING_TO_CHANGE_CHANNEL(TRUE)
		AND NOT bChangedChannel
			IF IS_ENTITY_ALIVE(objects[mof_tv_remote].id)
				//PLAY_SOUND_FROM_ENTITY(-1, "MICHAEL_SOFA_TV_CHANGE_CHANNEL_MASTER", objects[mof_tv_remote].id)
				CPRINTLN(DEBUG_MIKE, "PLAYING SOUND: IDLE/FIDGET channel change")
			ENDIF
			SET_NEW_ANIM_TASK(MAS_R_PRESS, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			
		// Go to changing vol loop
		ELIF IS_PLAYER_CHANGING_VOLUME()

			SET_NEW_ANIM_TASK(MAS_R_PRESS_HOLD_INTRO, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
		
		ENDIF
		
	ENDIF
	
	SWITCH eAnimState
	
		CASE MAS_R_IDLE

			// do a fidget idle
			IF bDelaying
				IF (GET_GAME_TIMER() - iAnimDelay) >= 8000
					bDelaying = FALSE
					SET_NEW_ANIM_TASK(MAS_R_FIDG, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
				ENDIF
			ENDIF
			
		BREAK
	
		CASE MAS_R_FIDG
			// back to idle
			IF MONITOR_FOR_ANIM_END()
				bDelaying = TRUE
				iAnimDelay = GET_GAME_TIMER()
				SET_NEW_ANIM_TASK(MAS_R_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_LOOPING|AF_IGNORE_GRAVITY)
			ENDIF
		BREAK

		CASE MAS_R_PRESS
		
			// back to idle
			IF MONITOR_FOR_ANIM_END()
				SET_NEW_ANIM_TASK(MAS_R_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_LOOPING|AF_IGNORE_GRAVITY)
				
			// repeat another press
			ELIF IS_INPUT_ATTEMPTING_TO_CHANGE_CHANNEL(TRUE)
				IF IS_ENTITY_ALIVE(objects[mof_tv_remote].id)
					//PLAY_SOUND_FROM_ENTITY(-1, "MICHAEL_SOFA_CHANGE_CHANNEL_MASTER", objects[mof_tv_remote].id)
					CPRINTLN(DEBUG_MIKE, "PLAYING SOUND: MAS_R_PRESS channel change")
				ENDIF
				SET_NEW_ANIM_TASK(MAS_R_PRESS, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			ENDIF
		BREAK
		
		CASE MAS_R_PRESS_HOLD_INTRO
			// go to the hold loop
			IF MONITOR_FOR_ANIM_END()
				SET_NEW_ANIM_TASK(MAS_R_PRESS_HOLD_LOOP, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_LOOPING)
			ENDIF
		BREAK
		
		CASE MAS_R_PRESS_HOLD_LOOP
			// Stop holding volume, go to volume loop outro
			IF NOT IS_PLAYER_CHANGING_VOLUME()
				SET_NEW_ANIM_TASK(MAS_R_PRESS_HOLD_OUTRO, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			ENDIF
		BREAK
		
		CASE MAS_R_PRESS_HOLD_OUTRO
			// back to idle
			IF MONITOR_FOR_ANIM_END()
				SET_NEW_ANIM_TASK(MAS_R_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_LOOPING|AF_IGNORE_GRAVITY)
			ENDIF
		BREAK
		
		CASE MAS_P_INTO
			// called detonate
			IF HAS_CONTACT_BEEN_SELECTED_IN_CONTACTS_LIST(CHAR_DETONATEPHONE)
			AND bCallingEnabled
				SET_NEW_ANIM_TASK(MAS_P_DETO_INTRO, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			// start the idle anim
			ELIF MONITOR_FOR_ANIM_END()
				SET_NEW_ANIM_TASK(MAS_P_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_LOOPING|AF_IGNORE_GRAVITY)
				g_TheContactInvolvedinCall = NO_CHARACTER
				
			// Manage spawning the phone prop
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_INTO))
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_INTO)) > 0.376
						IF NOT DOES_ENTITY_EXIST(objects[mof_phone].id)
							objects[mof_phone].id = CREATE_OBJECT(Prop_Phone_ING, GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_PH_R_HAND, <<0,0,0>>))
							ATTACH_ENTITY_TO_ENTITY(objects[mof_phone].id, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF

			ENDIF
		BREAK
		
		CASE MAS_P_IDLE
			IF NOT bDetonated
				// called detonate
				IF HAS_CONTACT_BEEN_SELECTED_IN_CONTACTS_LIST(CHAR_DETONATEPHONE)
				AND bCallingEnabled
					SET_NEW_ANIM_TASK(MAS_P_DETO_INTRO, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
				// put the phone away
				ELIF NOT IS_PHONE_ONSCREEN(TRUE)
					CPRINTLN(DEBUG_MIKE, "HAS JUST PUT PHONE AWAY")
					SET_NEW_ANIM_TASK(MAS_P_AWAY, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
				// browsing the phone
				ELIF HAS_USED_PHONE_NAV()
					SET_NEW_ANIM_TASK(MAS_P_SCROLL, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
				// press accept on the phone
				ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT)
				AND NOT IS_CONTACTS_LIST_ON_SCREEN()
					CPRINTLN(DEBUG_MIKE, "INPUT_CELLPHONE_SELECT IS_CONTROL_JUST_PRESSED")
					SET_NEW_ANIM_TASK(MAS_P_PRESS, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
				// pressed cancel on the phone
				ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL)
					CPRINTLN(DEBUG_MIKE, "INPUT_CELLPHONE_CANCEL IS_CONTROL_JUST_PRESSED")
					SET_NEW_ANIM_TASK(MAS_P_PRESS, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
				ENDIF
			ENDIF
		BREAK
		
		CASE MAS_P_AWAY
			// go back to the remote idle
			IF MONITOR_FOR_ANIM_END()
				SET_NEW_ANIM_TASK(MAS_R_IDLE, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_LOOPING|AF_IGNORE_GRAVITY)
			// manage deleting the phone prop
			ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_AWAY))
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_AWAY)) > 0.376
					IF DOES_ENTITY_EXIST(objects[mof_phone].id)
						DELETE_OBJECT(objects[mof_phone].id)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MAS_P_SCROLL
			// called detonate
			IF HAS_CONTACT_BEEN_SELECTED_IN_CONTACTS_LIST(CHAR_DETONATEPHONE)
			AND bCallingEnabled
				SET_NEW_ANIM_TASK(MAS_P_DETO_INTRO, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			// called detonate
			ELIF HAS_CONTACT_BEEN_SELECTED_IN_CONTACTS_LIST(CHAR_DETONATEPHONE)
			AND bCallingEnabled
				SET_NEW_ANIM_TASK(MAS_P_DETO_INTRO, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			// back to phone idle
			ELIF MONITOR_FOR_ANIM_END()
				SET_NEW_ANIM_TASK(MAS_P_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_LOOPING|AF_IGNORE_GRAVITY)
			ENDIF
		BREAK
		
		CASE MAS_P_PRESS
			// called detonate
			IF HAS_CONTACT_BEEN_SELECTED_IN_CONTACTS_LIST(CHAR_DETONATEPHONE)
			AND bCallingEnabled
				SET_NEW_ANIM_TASK(MAS_P_DETO_INTRO, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			// back to phone idle
			ELIF MONITOR_FOR_ANIM_END()
				SET_NEW_ANIM_TASK(MAS_P_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_LOOPING|AF_IGNORE_GRAVITY)
			ENDIF
		BREAK
		
		CASE MAS_P_DETO_INTRO
			IF NOT bDetonated 
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_INTRO))
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_INTRO)) > 0.305
						SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_SCRIPT)
						//PLAY_TV_CHANNEL_WITH_PLAYLIST( TV_LOC_MICHAEL_PROJECTOR, TVCHANNELTYPE_CHANNEL_SCRIPT, TV_PLAYLIST_SPECIAL_INVADER_EXP, TRUE)
						SETTIMERA(0)
						DISABLE_CELLPHONE(TRUE)
						bDetonated = TRUE
						iDetonationCallTimer = GET_GAME_TIMER()
						REPLAY_START_EVENT()
						bReplayRecordEventStarted = TRUE
						CPRINTLN( DEBUG_MIKE, "MICHAEL_ANIM_MANAGER() - MAS_P_DETO_INTRO called REPLAY_START_EVENT()" )
					ELIF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_DETO_INTRO)) > 0.200
						IF NOT bExpireDetonateConv
							bExpireDetonateConv = CREATE_CONVERSATION(sConvo, str_dialogue, "LS1B_TV", CONV_PRIORITY_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MONITOR_FOR_ANIM_END()
					SET_NEW_ANIM_TASK(MAS_P_DETO_WAIT, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_LOOPING|AF_IGNORE_GRAVITY)
				ENDIF
			ENDIF
		BREAK
		
		CASE MAS_P_DETO_WAIT
			IF NOT IS_SAFE_TO_START_CONVERSATION()
				ENABLE_MOVIE_SUBTITLES(FALSE)
				KILL_FACE_TO_FACE_CONVERSATION()
			ELSE
				IF GET_GAME_TIMER() - iDetonationCallTimer > 6500
					IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1B_REACT", CONV_PRIORITY_HIGH)
						SET_NEW_ANIM_TASK(MAS_P_DETO_REACT, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MAS_P_DETO_REACT
			IF MONITOR_FOR_ANIM_END()
				SET_NEW_ANIM_TASK(MAS_P_OUTRO, INSTANT_BLEND_IN, WALK_BLEND_OUT, AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
			ENDIF
		BREAK
		
		CASE MAS_P_OUTRO
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO))
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO)) > 0.321				
					SAFE_DELETE_OBJECT(objects[mof_tv_remote].id)
					SAFE_DELETE_OBJECT(objects[mof_phone].id)
					IF bReplayRecordEventStarted
						REPLAY_STOP_EVENT()
						CPRINTLN( DEBUG_MIKE, "MICHAEL_ANIM_MANAGER() - MAS_P_OUTRO called REPLAY_STOP_EVENT()" )
						bReplayRecordEventStarted = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
	
	#IF IS_DEBUG_BUILD
		IF bDisplayEventDebug
			PRINT_MICHAEL_ANIM_STAGE()
		ENDIF
	#ENDIF	

ENDPROC

// Triggers an event
// returns false if already triggered, true if just triggered
FUNC BOOL Trigger_Event(mission_event_flag event, INT delay = 0)
	
	IF mEvents[event].bTriggered = TRUE
	OR mEvents[event].bComplete = TRUE
		RETURN FALSE // already triggered, do nothing
	ELSE
		mEvents[event].bTriggered = TRUE
		mEvents[event].bComplete = FALSE
		mEvents[event].iTimeStamp = GET_GAME_TIMER()
		mEvents[event].iDelay = delay
		mEvents[event].iStage = 1
		
		RETURN TRUE
	ENDIF
ENDFUNC

// Check if the given event has triggered
FUNC BOOL Is_Event_Triggered(mission_event_flag event)
	RETURN mEvents[ENUM_TO_INT(event)].bTriggered
ENDFUNC

// Checks if the given event is complete
FUNC BOOL Is_Event_Complete(mission_event_flag event)
	RETURN mEvents[ENUM_TO_INT(event)].bComplete
ENDFUNC

// Kill event
PROC Kill_Event(mission_event_flag event)
	IF IS_EVENT_TRIGGERED(event)
	AND (NOT IS_EVENT_COMPLETE(event))
		mEvents[event].bComplete = TRUE
	ENDIF
ENDPROC

// Resets the given event
PROC Reset_Event(mission_event_flag event)
	mEvents[ENUM_TO_INT(event)].bTriggered = FALSE
	mEvents[ENUM_TO_INT(event)].bComplete = FALSE
	mEvents[ENUM_TO_INT(event)].iTimeStamp = 0
	mEvents[ENUM_TO_INT(event)].iDelay = 0
	mEvents[ENUM_TO_INT(event)].iStage= 1
	mEvents[ENUM_TO_INT(event)].bPaused = FALSE
	mEvents[ENUM_TO_INT(event)].bAnimDictPrestreamed = FALSE
	mEvents[ENUM_TO_INT(event)].bSafeAfterSkip = FALSE
ENDPROC

// Resets all the events in the mission
PROC  Reset_All_Events()
	INT i
	FOR i = 0 TO ENUM_TO_INT(mef_num_events)-1
		IF IS_EVENT_TRIGGERED(INT_TO_ENUM(mission_event_flag, i))
			RESET_EVENT(INT_TO_ENUM(mission_event_flag, i))	
		ENDIF
	ENDFOR
ENDPROC

// Used to delay events internally insde their procs
PROC Add_Event_Delay(mission_event_flag event, INT ms)
	mEvents[event].iTimeStamp = GET_GAME_TIMER()
	mEvents[event].iDelay = ms
ENDPROC

// Manages the swap between stages
PROC Mission_Stage_Management()
	SWITCH stageSwitch
		CASE STAGESWITCH_REQUESTED 
			CPRINTLN(DEBUG_MIKE_UTIL, "[StageManagement] Stage switch requested from stage:", mission_stage, " to stage:", requestedStage)
			stageSwitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			CPRINTLN(DEBUG_MIKE_UTIL, "[StageManagement] Exiting stage: ", mission_stage)
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage = STAGE_ENTRY
			mission_stage = requestedStage
			iStageTimer = GET_GAME_TIMER()
		BREAK
		CASE STAGESWITCH_ENTERING
			CPRINTLN(DEBUG_MIKE_UTIL, "[StageManagement] Entered stage: ", mission_stage)
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - iStageManagementTimer) > 2500
				CPRINTLN(DEBUG_MIKE_UTIL, "[StageManagement] Stage: ", mission_stage, " Substage: ", mission_substage)
				iStageManagementTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// Set stage, will only switch while stage switch management is idle
// returns false if not idle
FUNC BOOL Mission_Set_Stage(mission_stage_flag newStage)
	IF stageSwitch = STAGESWITCH_IDLE	
		requestedStage = enum_to_int(newStage)
		stageSwitch = STAGESWITCH_REQUESTED
		return TRUE
	ELSE
		return FALSE 
	ENDIF
ENDFUNC

PROC Mission_Cleanup()
	KILL_ANY_CONVERSATION()
	CLEAR_PRINTS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID())
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(veh_replay)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_replay)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_replay)
		ELSE
			DELETE_VEHICLE(veh_replay)
		ENDIF
	ENDIF

	INT i
	FOR i = 0 TO enum_to_int(mof_num_objs)-1
		IF DOES_ENTITY_EXIST(objects[i].id)
		
			SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[i], INSTANT_BLEND_OUT)
			
			IF IS_ENTITY_ATTACHED(objects[i].id)
				DETACH_ENTITY(objects[i].id)
			ENDIF
			
			VECTOR vPos
			
			IF objects[i].bCustom
				vPos = GET_ENTITY_COORDS(objects[i].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objects[i].id)
				CLEAR_AREA(vPos, 0.5, TRUE, TRUE, FALSE, FALSE)
				objects[i].bCustom = FALSE
			ELSE
				vPos = GET_ENTITY_COORDS(objects[i].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objects[i].id)
				IF NOT objects[i].bDoNotReset
					CLEAR_AREA(vPos, 0.5, TRUE, TRUE, FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = enum_to_int(mpf_shop) + 1 TO enum_to_int(mpf_num_peds)-1
		IF DOES_ENTITY_EXIST(peds[i])
		AND (NOT IS_PED_INJURED(peds[i]))
			IF GET_SCRIPT_TASK_STATUS(peds[i], SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
				CLEAR_PED_TASKS(peds[i])
			ENDIF
			
			SET_PED_AS_NO_LONGER_NEEDED(peds[i])
		ENDIF
	ENDFOR
	
	mission_door_flags x
	REPEAT COUNT_OF(sDoors) x
		IF sDoors[x].iHash != -1
		AND IS_DOOR_REGISTERED_WITH_SYSTEM(sDoors[x].iHash)
			REMOVE_DOOR_FROM_SYSTEM(sDoors[x].iHash)
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(veh_Tracey)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_Tracey)
	ENDIF
	
	RELEASE_AMBIENT_AUDIO_BANK()
	RELEASE_SOUND_ID(iSFX_FindVirus)
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_LOCKED)
	
	SAFE_REMOVE_BLIP(blip_clothes[0])
	SAFE_REMOVE_BLIP(blip_clothes[1])
	SAFE_REMOVE_BLIP(blip_clothes[2])
	
	IF IS_AUDIO_SCENE_ACTIVE(aScene_noise_blocker)
		STOP_AUDIO_SCENE(aScene_noise_blocker)
	ENDIF
	
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
	
	CLEAR_WEATHER_TYPE_PERSIST()
	
	Reset_Scenario_Blocking()
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE)
	
	DISABLE_CELLPHONE(FALSE)
	
	g_bLaptopMissionSuppressed = FALSE
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	
	SET_SHOP_DIALOGUE_IS_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
	
	REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_DETONATEPHONE, MICHAEL_BOOK)
	HANG_UP_AND_PUT_AWAY_PHONE()
	
	SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", TRUE, TRUE)
	ENABLE_MOVIE_SUBTITLES(TRUE)
	
	SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, TRUE)
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_INVADER_OFFICE_INTERIOR, BUILDINGSTATE_NORMAL, TRUE )
	
ENDPROC

PROC Mission_Reset_Everything()

	// set michael back to normal attire
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)
	
	RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	REMOVE_PROGRAMMER_OUTFIT()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID())
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, FALSE)
	ENDIF
	
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE()
		REMOVE_CUTSCENE()
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	ENDIF
	
	RESET_ALL_EVENTS()
	CLEAR_MISSION_LOCATE_STUFF(locates_data)
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	KILL_ANY_CONVERSATION()
	CLEAR_PRINTS()
	CLEAR_HELP()
	DESTROY_ALL_CAMS()
	SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_LOCKED)
	
	REMOVE_PED_FOR_DIALOGUE(sConvo, 0)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 3)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 4)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 5)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 6)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 7)
	
	INT i
	// Delete all props
	FOR i = 0 TO enum_to_int(mof_num_objs)-1
		IF DOES_ENTITY_EXIST(objects[i].id)
			IF objects[i].bCustom
				DELETE_OBJECT(objects[i].id)
				objects[i].bCustom = FALSE
			ELSE
				VECTOR vPos
				vPos = GET_ENTITY_COORDS(objects[i].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objects[i].id)
				CLEAR_AREA(vPos, 1.0, TRUE, TRUE, FALSE, FALSE)
			ENDIF
		ENDIF
		
		objects[i].bInSynchedScene = FALSE
		objects[i].v_coord	= <<0,0,0>>
		objects[i].bCustom	= FALSE
		objects[i].adName = ""
		objects[i].animName = ""
		objects[i].model = DUMMY_MODEL_FOR_SCRIPT
		objects[i].bDoNotReset = FALSE
	ENDFOR
	
	// Delete all peds
	FOR i = enum_to_int(mpf_shop) + 1  TO enum_to_int(mpf_num_peds)-1
		IF DOES_ENTITY_EXIST(peds[i])
			DELETE_PED(peds[i])
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(veh_Tracey)
		DELETE_VEHICLE(veh_Tracey)
	ENDIF
	
	IF DOES_ENTITY_EXIST(veh_replay)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_replay)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
		DELETE_VEHICLE(veh_replay)		
	ENDIF
	
	// remove objective blip
	IF DOES_BLIP_EXIST(blip_Objective)
		REMOVE_BLIP(blip_Objective)
	ENDIF
	
	FOR i = 0 TO 11
		IF sPopups[i].iType != -1
			IF sPopups[i].iSfxLoop != -1
				STOP_SOUND(sPopups[i].iSfxLoop)
			ENDIF
			sPopups[i].bClosed 		= FALSE
			sPopups[i].bSfxPlaying	= FALSE
		ELSE
			sPopups[i].bClosed = TRUE
		ENDIF
	ENDFOR
	
	REPEAT COUNT_OF(syncedIDs) i
		syncedIDs[i] = -1
	ENDREPEAT
	
	REPEAT COUNT_OF(blip_clothes) i
		SAFE_REMOVE_BLIP(blip_clothes[i])
	ENDREPEAT
	
	IF bReplayRecordEventStarted
		REPLAY_CANCEL_EVENT()
	ENDIF
	bReplayRecordEventStarted	= FALSE
	 
	// Reset flags
	iPaperThrow 				= 0
	iScanTimer 					= 0
	iPrevScanBar 				= 0
	iScanStutter				= 0
	bObtainedClothing 			= FALSE
	iPopupHelp					= 0
	bIsAVOpen					= FALSE
	iCurrentPopup				= 0
	iExterminateTimer			= 0
	fInputAccum					= 0.0
	bSideEntranceObjective		= FALSE
	bAllowRunning				= FALSE
	bDisableConspicuousBehavior = FALSE
	bAllowRunning				= FALSE
	b_MiniGameScaleformSetUp	= FALSE
	g_QuickSaveDisabledByScript = FALSE
	bTVTurnedOff				= FALSE
	bTVTurnedOn					= FALSE
	
	Reset_Scenario_Blocking()
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	
	// clear the life invader building of all peds
	CLEAR_AREA_OF_PEDS(<< -1061.6774, -242.1771, 43.0213 >>, 50.0)
	
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("LES1A_TXT1")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("LES1A_TXT2")
	
	CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER(CHAR_MICHAEL)
	REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_DETONATEPHONE, MICHAEL_BOOK)
	
	DISABLE_CELLPHONE(FALSE)
	
	STOP_AUDIO_SCENES()

	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", TRUE, TRUE)
	ENABLE_MOVIE_SUBTITLES(TRUE)
	
	MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_LESTER_1)
	
ENDPROC
 
PROC Mission_Passed()
	CLEAR_PRINTS()
	CLEAR_PLAYER_WANTED_LEVEL( PLAYER_ID() )
		
	Mission_Cleanup()
	Mission_Flow_Mission_Passed()	
	TERMINATE_THIS_THREAD()
ENDPROC

// Fail param handles printing fail text
PROC Mission_Failed(mission_fail_flag fail_condition)
	
	// Force failed - player death, arrested, switch to Multiplayer, etc
	IF fail_condition = mff_default
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		Mission_Cleanup()
		
	// Normal mission failed
	ELSE
		// Process fail msg
		STRING strFailMsg
		SWITCH fail_condition
			CASE mff_blew_your_cover
				strFailMsg = "LES1A_F1"
			BREAK
			CASE mff_bomb_plant_failed
				strFailMsg = "LES1A_F2"
			BREAK
			CASE mff_store_closed
				strFailMsg = "LES1A_F3"
			BREAK
			CASE mff_insufficient_fund_for_disguise
				strFailMsg = "LES1A_F5"
			BREAK
			CASE mff_lester_dead
				strFailMsg = "LES1A_F4"
			BREAK
			CASE mff_did_not_kill_target
				strFailMsg = "F_DETONATE"
			BREAK
			CASE mff_tracey_dead
				strFailMsg = "F_TRACY_DEAD"
			BREAK
			CASE mff_tracey_threat
				strFailMsg = "F_TRACEY_FLED"
			BREAK
			CASE mff_wanted_home
				strFailMsg = "F_WANT_HOME"
			BREAK
			CASE mff_debug_forced
				strFailMsg = "LES1A_FF"
			BREAK
			CASE mff_default
			DEFAULT
				strFailMsg = "LES1A_DF"
			BREAK
		ENDSWITCH
	
		MISSION_FLOW_MISSION_FAILED_WITH_REASON(strFailMsg)
		
		WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
			WAIT(0)
		ENDWHILE

		// check if we need to respawn the player in a different position, 
		// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-1048.4845, -219.1426, 36.8756>>, 234.6454)
			SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(v_LifeInvaderCarParkCoord, f_LifeInvaderCarParkHeading)
			
		ELIF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_lesters
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<1282.1167, -1736.8835, 51.2234>>, 22.2041)
		
		// is michael sitting watching the tv when failing
		ELIF mission_stage = enum_to_int(msf_st8_tv_start)
		OR mission_stage = enum_to_int(msf_st9_tv_detonate)
		OR mission_stage = enum_to_int(msf_st10_mission_passed)
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-802.8600, 172.6933, 71.8447>>, 286.1387)
		ENDIF

		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_M_04_HW, FALSE)
		
		IF fail_condition = mff_debug_forced
			Mission_Reset_Everything()
		ENDIF

		RESTORE_MISSION_START_OUTFIT()
		MISSION_CLEANUP() // must only take 1 frame and terminate the thread
		RESTORE_STANDARD_CHANNELS()
		
		PRINTLN("Lester1A: Failed - MSG:", strFailMsg, ", FailCondition: ", fail_condition)
	ENDIF
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE: Checks for mission fail conditions
PROC Mission_Checks()
	INT i
	IF mission_stage = enum_to_int(msf_st0_intro_cutscene)
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX veh_TempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(veh_TempVeh)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(veh_TempVeh)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(veh_TempVeh)
		ELSE
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(null)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(null)
		ENDIF
		
	ENDIF

	
	IF DOES_ENTITY_EXIST(peds[mpf_lester])
		IF IS_PED_INJURED(peds[mpf_lester])
		OR IS_ENTITY_ON_FIRE(peds[mpf_lester])
			Mission_Failed(mff_lester_dead)
		ENDIF
	ENDIF
		
	// Blow your cover if your use weapons in/around the life invader building, at any point during the mission
	IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1010.514221,-215.746918,36.917801>>, <<-1136.744751,-279.693207,66.870827>>, 88.437500)
	AND IS_PED_SHOOTING(PLAYER_PED_ID()))
	OR IS_BULLET_IN_BOX(<<-1109.98, -263.37, 37.73>>, <<-1008.46, -216.71, 56.92>>, TRUE)
	OR IS_PROJECTILE_IN_AREA(<<-1109.98, -263.37, 37.73>>, <<-1008.46, -216.71, 56.92>>, TRUE)
		Mission_Failed(mff_blew_your_cover)
	ENDIF
	
	// has ped blown cover by driving vehicle into building
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices
		Mission_Failed(mff_blew_your_cover)
	ENDIF
	
	// life invader engineer is injured
	IF DOES_ENTITY_EXIST(peds[mpf_smoker])
	AND IS_PED_INJURED(peds[mpf_smoker])
		Mission_Failed(mff_blew_your_cover)
	ENDIF
	
	// driving a stupid vehicle that draws attention around the life invader offices
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1010.514221,-215.746918,36.917801>>, <<-1136.744751,-279.693207,66.870827>>, 88.437500)
	AND (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
		OR IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
		OR IS_VEHICLE_SIREN_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
	OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) != PPS_INVALID)

		Mission_Failed(mff_blew_your_cover)
		
	// ped has brought a vehicle up to the entrance of the building
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1043.089844,-228.149597,38.013393>>, <<-1047.865967,-230.596863,48.139465>>, 7.875000)
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		Mission_Failed(mff_blew_your_cover)
	ENDIF
	
	// Any vehicles near the doors of the life invader building will blow the cover
	IF IS_ANY_VEHICLE_NEAR_POINT(<<-1045.633789,-230.615540,38.093407>>, 2.0)
	AND IS_ANY_VEHICLE_NEAR_POINT(<<-1082.183594,-259.889313,36.814720>>, 2.0)
		Mission_Failed(mff_blew_your_cover)
	ENDIF
	
	IF mission_stage = enum_to_int(msf_st1_get_clothes)
	AND b_CanFailForNotEnoughFunds
	AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()

		BOOL bHasGillet, bHasShorts
		
		IF IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_0)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_1)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_2)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_3)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_4)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_5)
			bHasGillet = TRUE
		ENDIF
			
		IF IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_1)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_2)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_3)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_4)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_4)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_0)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_1)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_2)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_3)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_4)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_5)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_6)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_7)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_8)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_9)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_10)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_11)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_12)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_13)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_14)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_LONG_SHORTS_15)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_0)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_1)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_2)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_3)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_4)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_5)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_6)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_7)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_8)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_9)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_10)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_11)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_12)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_13)
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_14)	
		OR IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_YOGA_15)	
			bHasShorts	= TRUE
		ENDIF
		
		IF NOT bHasGillet 
		AND NOT bHasShorts
			IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < i_min_required_funds
				Mission_Failed(mff_insufficient_fund_for_disguise)
			ENDIF
		ELIF NOT bHasGillet 
			IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < i_most_expensive_gilet
				Mission_Failed(mff_insufficient_fund_for_disguise)
			ENDIF
		ELIF NOT bHasShorts
			IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < i_most_expensive_shorts
				Mission_Failed(mff_insufficient_fund_for_disguise)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF mission_stage <= enum_to_int(msf_st2_go_to_offices)
		// If shop is closed before you get your disguise then fail
		IF mission_stage > enum_to_int(msf_st0_intro_cutscene)
		AND NOT IS_SHOP_OPEN_FOR_BUSINESS(CLOTHES_SHOP_M_04_HW)
		AND bObtainedClothing = FALSE
			Mission_Failed(mff_store_closed)
		ENDIF
		
		IF mission_substage < 4
		AND mission_substage != STAGE_EXIT
			// Fail if player enteres offices before being told to (i.e. without disguise)
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices
				Mission_Failed(mff_blew_your_cover)
			ENDIF
		ENDIF
	ENDIF
	
	IF mission_stage >= enum_to_int(msf_st2_go_to_offices)
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices
			IF NOT IS_AUDIO_SCENE_ACTIVE(aScene_noise_blocker)
				START_AUDIO_SCENE(aScene_noise_blocker)
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE(aScene_noise_blocker)
				STOP_AUDIO_SCENE(aScene_noise_blocker)
			ENDIF
		ENDIF
	ENDIF
	
	IF mission_stage >= enum_to_int(msf_st3_go_to_computer)
		
		// Check if any of the life invader peds are injured, if so fail
		FOR i = enum_to_int(mpf_smoker) TO enum_to_int(mpf_boardroom_f_c)
			IF DOES_ENTITY_EXIST(peds[i])		
				IF IS_PED_INJURED(peds[i])
					Mission_Failed(mff_blew_your_cover)
				ENDIF
			ENDIF
		ENDFOR
	
		// Check player hasn't left the area
		IF mission_stage <= enum_to_int(msf_st5_plant_bomb)
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != interior_offices

				IF iFailTimer < 0
					iFailTimer = GET_GAME_TIMER()
				ENDIF
				
				// Only fail once player has been out of building for 3 seconds
				IF GET_GAME_TIMER() - iFailTimer > 3000
					IF mission_stage = enum_to_int(msf_st3_go_to_computer)
						// Give enough time for the player to have walked into the building
						IF GET_GAME_TIMER() - iStageTimer > 5000
							Mission_Failed(mff_bomb_plant_failed)
						ENDIF
					ELSE
						Mission_Failed(mff_bomb_plant_failed)
					ENDIF
				ENDIF
			ELSE
				// Reset timer
				iFailTimer = -1
			ENDIF
		ENDIF
		
		IF mission_stage <= enum_to_int(msf_st6_leave_building)
			// start checking for attacking of people once player arrives at the life invader offices
			IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				Mission_Failed(mff_blew_your_cover)
			ENDIF	
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				Mission_Failed(mff_blew_your_cover)
			ENDIF
			IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
				Mission_Failed(mff_blew_your_cover)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF mission_stage >= enum_to_int(msf_st7_go_home)
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_living_room
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				Mission_Failed(mff_wanted_home)
			ENDIF
		ENDIF
	ENDIF
	
	// Player cannot equip a weapon while they have the bag on or their conspicuous behavior is disabled
	IF bDisableConspicuousBehavior
		DISABLE_PLAYERS_WEAPONS_THIS_FRAME()
	ENDIF
	
	IF bDisableConspicuousBehavior
	
		IF NOT bAllowRunning
			// force the player to walk only
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SPRINT)
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_AIM)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, true)
		
		IF NOT IS_CELLPHONE_DISABLED()
			DISABLE_CELLPHONE(TRUE)
		ENDIF
	ELSE
		IF mission_stage < enum_to_int(msf_st8_tv_start)
			IF IS_CELLPHONE_DISABLED()
				DISABLE_CELLPHONE(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	// 2140700 - can see ped creation/deletion during the LI office.
	mission_stage_flag eStage = int_to_enum(mission_stage_flag, mission_stage)
	IF eStage = msf_st3_go_to_computer
	OR eStage = msf_st4_mini_game
	OR eStage = msf_st5_plant_bomb
	OR eStage = msf_st6_leave_building
	// 2061975 - blocking camera movement while watching the TV
	OR eStage = msf_st8_tv_start
	OR eStage = msf_st9_tv_detonate
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	ENDIF
	
ENDPROC


/*
													 _______  _     _  _______  _______  _______  ______ 
													(_______)(_)   (_)(_______)(_______)(_______)/ _____)
													 _____    _     _  _____    _     _     _   ( (____  
													|  ___)  | |   | ||  ___)  | |   | |   | |   \____ \ 
													| |_____  \ \ / / | |_____ | |   | |   | |   _____) )
													|_______)  \___/  |_______)|_|   |_|   |_|  (______/ 
*/

FUNC STRING Get_Event_Name(mission_event_flag mef)	
	STRING result
	SWITCH mef
		CASE		mef_lester_phone_call			result = "Phone Call"			BREAK
		CASE		mef_lester_phone_call_2			result = "Phone Call 2"			BREAK
		CASE 		mef_manage_bag					result = "Manage bag"			BREAK
		CASE 		mef_ambient_boardroom			result = "Boardroom" 			BREAK
		CASE 		mef_ambient_interview			result = "Interview" 			BREAK
		CASE 		mef_ambient_milk				result = "Milk Lady" 			BREAK
		CASE 		mef_ambient_office				result = "Office AirGuitar" 	BREAK
		CASE 		mef_ambient_paper_throw			result = "Paper Throw" 			BREAK
		CASE		mef_ambient_receptionist		result = "Receptionist" 		BREAK
		CASE		mef_engineer_air_guitar			result = "Engineer Airguitar"	BREAK
		CASE		mef_engineer_follow				result = "Engineer Follow"		BREAK
		CASE		mef_ambient_dialogue			result = "DIA: Ambient Office"	BREAK
		CASE		mef_tracey						result = "Manage Tracey"		BREAK
		DEFAULT										result = "UNKNOWN" 				BREAK
	ENDSWITCH
	RETURN result
ENDFUNC

PROC event_lester_phone_call()
	SWITCH mEvents[mef_lester_phone_call].iStage
		CASE 1
			iLesterCallTimer = 0
			mEvents[mef_lester_phone_call].iStage++
		BREAK
		CASE 2
			// got into a vehicle, start the timer for 5 seconds
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				iLesterCallTimer = GET_GAME_TIMER() + 5000
				mEvents[mef_lester_phone_call].iStage = 4
				
			// moved away from the shop, start the timer for 15000 seconds
			ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), << 127.7614, -209.5459, 53.5500 >>) >= 30.0
				iLesterCallTimer = GET_GAME_TIMER() + 15000
				mEvents[mef_lester_phone_call].iStage = 3
			ENDIF
		BREAK
		CASE 3
			// if gotten in a vehicle then reset the timer by jumping back a stage
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				mEvents[mef_lester_phone_call].iStage--
			ENDIF
		FALLTHRU
		CASE 4			
			IF GET_GAME_TIMER() > iLesterCallTimer

				// Player has a wanted level, send them a text instead from lester
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					mEvents[mef_lester_phone_call].iStage = 7
				// phone lester
				ELSE
					REMOVE_PED_FOR_DIALOGUE(sConvo, 8)
					ADD_PED_FOR_DIALOGUE(sConvo, 8, null, "LESTER")
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF PLAYER_CALL_CHAR_CELLPHONE(sConvo, CHAR_LESTER, str_dialogue, "LES_1A_CALL", CONV_PRIORITY_HIGH) 
							mEvents[mef_lester_phone_call].iStage = 5
						ENDIF
					ENDIF		
				ENDIF
			ENDIF
		BREAK
		CASE 5
			// phone call finished
			IF IS_CALLING_ANY_CONTACT()	
				mEvents[mef_lester_phone_call].iStage++
			ENDIF
		BREAK
		CASE 6
			IF NOT IS_CALLING_ANY_CONTACT()
			
				// Phone call was interrupted, delay next stage...
				IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED() 
				OR HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()
				
					Add_Event_Delay(mef_lester_phone_call, 10000)
					mEvents[mef_lester_phone_call].iStage++
				
				ELSE
				
					REMOVE_PED_FOR_DIALOGUE(sConvo, 8)
					Kill_Event(mef_lester_phone_call)
					
				ENDIF
			ENDIF	
		BREAK
		CASE 7
			// .... send text message containing info.
			SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_LESTER, "LES1A_TXT2", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
			Kill_Event(mef_lester_phone_call)
		BREAK
	ENDSWITCH
ENDPROC

PROC event_lester_phone_call_2()

	SWITCH mEvents[mef_lester_phone_call_2].iStage
		CASE 1
			// PLayer is out of the offices and can no longer get back in
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != interior_offices
			AND DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_L)) 	= DOORSTATE_LOCKED AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_L))) <= 0.1
			AND DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_R)) 	= DOORSTATE_LOCKED AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_R))) <= 0.1
			AND DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_L)) 	= DOORSTATE_LOCKED AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LIFE_INVADER_REAR_L))) <= 0.1
			AND DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_R)) 	= DOORSTATE_LOCKED AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LIFE_INVADER_REAR_R))) <= 0.1
				iLesterCallTimer = GET_GAME_TIMER() + 5000
				mEvents[mef_lester_phone_call_2].iStage++
			ENDIF
		BREAK
		CASE 2
			IF GET_GAME_TIMER() > iLesterCallTimer
			OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				REMOVE_PED_FOR_DIALOGUE(sConvo, 8)
				ADD_PED_FOR_DIALOGUE(sConvo, 8, null, "LESTER")
				mEvents[mef_lester_phone_call_2].iStage++
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF PLAYER_CALL_CHAR_CELLPHONE(sConvo, CHAR_LESTER, str_dialogue, "LES1A_01", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT) 
					mEvents[mef_lester_phone_call_2].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_CALLING_ANY_CONTACT()	
				mEvents[mef_lester_phone_call_2].iStage++
			ENDIF
		BREAK
		CASE 5
			// phone call finished
			IF NOT IS_CALLING_ANY_CONTACT()	
				REMOVE_PED_FOR_DIALOGUE(sConvo, 8)
				Kill_Event(mef_lester_phone_call_2)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC event_manage_bag()
	SWITCH mEvents[mef_manage_bag].iStage
		CASE 1
			IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2) = 9
				PRELOAD_PED_COMP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_NONE)
				//SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
				mEvents[mef_manage_bag].iStage = 2
			ELSE
				PRELOAD_PED_COMP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK)
				//SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 9, 0)
				mEvents[mef_manage_bag].iStage = 3
			ENDIF
		BREAK
		CASE 2
			// Remove bag
			IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2) != 9
			OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR ((IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() AND NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CLOTHES_SHOP_M_04_HW)) AND NOT IS_GAMEPLAY_CAM_RENDERING()))
				//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_NONE, FALSE)
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				//SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 9, 0)
				PRELOAD_PED_COMP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK)
				
				IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
					mEvents[mef_manage_bag].iStage = 4
				ELSE
					mEvents[mef_manage_bag].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			// Add bag
			IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2) = 9
			OR (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP())
			
				//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 9, 0)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK, FALSE)
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				//SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
				PRELOAD_PED_COMP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_NONE)				

				mEvents[mef_manage_bag].iStage--
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				mEvents[mef_manage_bag].iStage = 2
			ENDIF
		BREAK
	ENDSWITCH
	
	// Manage clipset
	WEAPON_TYPE eCurrentWeapon 
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
	
	IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2) = 9
	AND eCurrentWeapon = WEAPONTYPE_UNARMED
		IF HAS_ANIM_DICT_LOADED("MOVE_P_M_ZERO_RUCKSACK")
			SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), FALSE)
		ENDIF
	ELSE
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID())
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), TRUE)
	ENDIF
ENDPROC

PROC event_engineer_follow()

	IF DOES_ENTITY_EXIST(peds[mpf_smoker])
	AND NOT IS_PED_INJURED(peds[mpf_smoker])
	
		IF EFE_START = INT_TO_ENUM(ENGINEER_FOLLOW_ENUM, mEvents[mef_engineer_follow].iStage)
		
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_smoker], TRUE)
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_WAYPOINT_RECORDING(null, str_wp_engineer_route, 3, EWAYPOINT_START_TASK_EXACTSTOP)
				TASK_LOOK_AT_ENTITY(null, PLAYER_PED_ID(), -1)
				TASK_ACHIEVE_HEADING(null, 160.0)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(peds[mpf_smoker], seq)
			CLEAR_SEQUENCE_TASK(seq)
			FORCE_PED_MOTION_STATE(peds[mpf_smoker], MS_ON_FOOT_WALK, false, FAUS_CUTSCENE_EXIT)	
			
			Load_Asset_AnimDict(sAssetData, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@base")
			Load_Asset_AnimDict(sAssetData, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@idle_a")
			
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0		str_paused_root = "LS1A_WLKCT2"		BREAK
				CASE 1		str_paused_root = "LS1A_WLKCT3"		BREAK
				//CASE 2		str_paused_root = "LS1A_WLKCT"		BREAK
			ENDSWITCH
			str_paused_label								= ""
			i_LIChokePointStage 							= 0
			bReadyToStartWalkChatConvo						= FALSE
			b_OfficeSceneChat								= FALSE
			mEvents[mef_engineer_follow].bSafeAfterSkip 	= TRUE
			mEvents[mef_engineer_follow].iTimeStamp 		= 0
			mEvents[mef_engineer_follow].iStage				= enum_to_int(EFE_TO_DESK)
			
		ELSE
		
			FLOAT f_dist_between = VDIST2(GET_ENTITY_COORDS(peds[mpf_smoker]), GET_ENTITY_COORDS(PLAYER_PED_ID()))
			
			// Process look at
			IF f_dist_between < 36 // 6^2
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), peds[mpf_smoker], -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
			ELIF f_dist_between >= 100	// 10^2
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			ENDIF
		
			// Enable the door opening search and IK for the LI ped when he nears the door
			IF VDIST2(<<-1063.57, -241.33, 39.91>>, GET_ENTITY_COORDS(peds[mpf_smoker])) < F_DIST_LI_PED_STOPS
				SET_PED_RESET_FLAG(peds[mpf_smoker], PRF_SearchForClosestDoor, TRUE)
				SET_PED_CONFIG_FLAG(peds[mpf_smoker], PCF_OpenDoorArmIK, TRUE)
			ELSE
				SET_PED_CONFIG_FLAG(peds[mpf_smoker], PCF_OpenDoorArmIK, FALSE)
			ENDIF
			
			BOOL bDoChokePointCheck
			
			// Choke points
			SWITCH i_LIChokePointStage
				CASE 0
					IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_smoker], <<-1061.159058,-240.230515,38.733181>>, <<-1066.040039,-242.661133,41.858181>>, 2.937500)
						bDoChokePointCheck = TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_smoker], <<-1066.657715,-247.583450,38.733181>>, <<-1070.308838,-243.851654,43.312668>>, 2.937500)
						bDoChokePointCheck = TRUE
					ENDIF
				BREAK
				CASE 2
					IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_smoker], <<-1074.717285,-242.909637,42.689636>>, <<-1079.151611,-245.258301,46.146278>>, 7.062500)
						bDoChokePointCheck = TRUE
					ENDIF
				BREAK
				CASE 3
					IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_smoker], <<-1075.363159,-248.935425,43.021278>>, <<-1070.266357,-250.371078,46.146278>>, 7.062500)
						bDoChokePointCheck = TRUE
					ENDIF
				BREAK
			ENDSWITCH

			SWITCH INT_TO_ENUM(ENGINEER_FOLLOW_ENUM, mEvents[mef_engineer_follow].iStage)
				CASE EFE_TO_DESK
				
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_smoker], SCRIPT_TASK_PERFORM_SEQUENCE)
						mEvents[mef_engineer_follow].iStage			= enum_to_int(EFE_WAITING_AT_END)
					ELSE
				
						IF bDoChokePointCheck

							IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices
							AND VDIST2(GET_ENTITY_COORDS(peds[mpf_smoker]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < F_DIST_LI_PED_STOPS
								// don't stop ped walking and move on to the next choke point check
								i_LIChokePointStage++
							ELSE
								// Not in range, pause
								WAYPOINT_PLAYBACK_PAUSE(peds[mpf_smoker], TRUE)
								
								// Store current convo
								IF NOT b_OfficeSceneChat
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									 	IF IS_PED_IN_CURRENT_CONVERSATION(peds[mpf_smoker])
											str_paused_root		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
											str_paused_label	= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
										ENDIF
										KILL_FACE_TO_FACE_CONVERSATION()
									ENDIF
								ENDIF
								
								mEvents[mef_engineer_follow].iStage = ENUM_TO_INT(EFE_WAITING_ON_ROUTE_START)
							ENDIF
							
						ENDIF
					
					ENDIF
				
				BREAK
				CASE EFE_WAITING_ON_ROUTE_START 		FALLTHRU
				CASE EFE_WAITING_ON_ROUTE
						
					IF WAYPOINT_PLAYBACK_GET_IS_PAUSED(peds[mpf_smoker])
						IF bDoChokePointCheck
							// Resume playback
							IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices
							AND f_dist_between < F_DIST_LI_PED_STOPS // 5 squared
							
								WAYPOINT_PLAYBACK_RESUME(peds[mpf_smoker], FALSE)	

								IF NOT b_OfficeSceneChat
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										KILL_FACE_TO_FACE_CONVERSATION()
									ENDIF
								ENDIF
								
								mEvents[mef_engineer_follow].iStage = enum_to_int(EFE_TO_DESK)
								i_LIChokePointStage++
								
							ENDIF
						ENDIF
					ENDIF
				
				BREAK
				CASE EFE_WAITING_AT_END
					
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_smoker], SCRIPT_TASK_PERFORM_SEQUENCE) 
					AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_smoker], SCRIPT_TASK_PLAY_ANIM)

						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM(null, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@base", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,  -1)
							TASK_PLAY_ANIM(null, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@idle_a", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM(null, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@base", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,  -1)
							TASK_PLAY_ANIM(null, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@idle_a", "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM(null, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@base", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,  -1)
							TASK_PLAY_ANIM(null, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@idle_a", "idle_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(peds[mpf_smoker], seq)
						CLEAR_SEQUENCE_TASK(seq)
					
					ENDIF
					
				BREAK
			ENDSWITCH
			
			//DIALOGUE
			// start the convo off again if it wasn't finished when interupted
			IF NOT b_OfficeSceneChat
				SWITCH int_to_enum(ENGINEER_FOLLOW_ENUM, mEvents[mef_engineer_follow].iStage)
					CASE EFE_TO_DESK	
					
						IF i_LIChokePointStage = 4
							
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, "LS1AAUD", "LS1A_POINT", CONV_PRIORITY_HIGH)
									str_paused_root 	= ""
									str_paused_label	= ""
									i_LIChokePointStage++
								ENDIF
							ELSE
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
							ENDIF
					
						ELSE
						
							IF bReadyToStartWalkChatConvo
							AND NOT IS_STRING_NULL_OR_EMPTY(str_paused_root)
								IF IS_SAFE_TO_START_CONVERSATION()
									IF IS_STRING_NULL_OR_EMPTY(str_paused_label)
										IF CREATE_CONVERSATION(sConvo, "LS1AAUD", str_paused_root, CONV_PRIORITY_HIGH)
											str_paused_root 	= ""
										ENDIF
									ELSE
										IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sConvo, "LS1AAUD", str_paused_root, str_paused_label, CONV_PRIORITY_HIGH)
											str_paused_root 	= ""
											str_paused_label 	= ""
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF	
					BREAK
					CASE EFE_WAITING_ON_ROUTE_START	FALLTHRU
					CASE EFE_WAITING_ON_ROUTE
						// player is still close enough to get berrated by the engineer for being slow.
						IF f_dist_between <= F_DIST_LI_PED_STOPS // 5 squared
						OR (f_dist_between < 144.0 // 12 squared
						AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices)
						
							IF GET_GAME_TIMER() - mEvents[mef_engineer_follow].iTimeStamp >= 7000	
								IF NOT b_OfficeSceneChat
									IF IS_SAFE_TO_START_CONVERSATION()
									
										IF INT_TO_ENUM(ENGINEER_FOLLOW_ENUM, mEvents[mef_engineer_follow].iStage) = EFE_WAITING_ON_ROUTE_START
											IF CREATE_CONVERSATION(sConvo, "LS1AAUD", "LS1A_LFTB", CONV_PRIORITY_HIGH)
												mEvents[mef_engineer_follow].iTimeStamp 	= GET_GAME_TIMER()
												mEvents[mef_engineer_follow].iStage 		= enum_to_int(EFE_WAITING_ON_ROUTE)
											ENDIF
										ELSE
											IF CREATE_CONVERSATION(sConvo, "LS1AAUD", "LS1A_COMON", CONV_PRIORITY_HIGH)
												mEvents[mef_engineer_follow].iTimeStamp = GET_GAME_TIMER()
											ENDIF
										ENDIF
										
									ENDIF
								ELSE
									mEvents[mef_engineer_follow].iTimeStamp = GET_GAME_TIMER()
								ENDIF
							ENDIF
							
						ELSE
							IF NOT b_OfficeSceneChat
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		
		ENDIF
		
	ELSE
		// Ped is dead stop this event
		Kill_Event(mef_engineer_follow)
	ENDIF	
ENDPROC

PROC event_ambient_milk_lady()
	SWITCH mEvents[mef_ambient_milk].iStage
		CASE 1
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1075.434814,-244.675262,43.021240>>, <<-1076.745728,-242.233261,47.445156>>, 1.500000)
				
				Load_Asset_AnimDict(sAssetData, anim_dict_milk_lady)
				Load_Asset_AnimDict(sAssetData, anim_dict_coffee)
				Load_Asset_Model(sAssetData, mod_cup)
				Load_Asset_Model(sAssetData, mod_milk_carton)
				
				mEvents[mef_ambient_milk].iStage++
			ENDIF
		BREAK
		CASE 2
			IF HAS_MODEL_LOADED(mod_hipster_f)
			AND HAS_MODEL_LOADED(mod_hipster_m)
			AND HAS_MODEL_LOADED(mod_cup)
			AND HAS_ANIM_DICT_LOADED(anim_dict_milk_lady)
			AND HAS_ANIM_DICT_LOADED(anim_dict_coffee)
				peds[mpf_milk_f] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_f, << -1066.2509, -241.2682, 38.7331 >>, 68.4967)
				Set_Life_Invader_Office_Ped_Variation(mpf_milk_f)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_milk_f], TRUE)
				
				peds[mpf_coffee_1] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m,  v_coffee_guy_1_coord, v_coffee_guy_1_rot.z)
				Set_Life_Invader_Office_Ped_Variation(mpf_coffee_1)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_coffee_1], TRUE)
				
				peds[mpf_coffee_2] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, v_coffee_guy_2_coord, v_coffee_guy_2_rot.z)
				Set_Life_Invader_Office_Ped_Variation(mpf_coffee_2)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_coffee_2], TRUE)
				
				// Create the milk
				Create_Prop(objects[mof_milk_carton], mod_milk_carton, GET_PED_BONE_COORDS(peds[mpf_milk_f], BONETAG_PH_R_HAND, <<0,0,0>>))
				Unload_Asset_Model(sAssetData, mod_milk_carton)
				
				// Create the cups
				Create_Prop(objects[mof_cup], mod_cup, << -1066.8055, -241.0743, 39.7336 >> )
				SET_ENTITY_COORDS_NO_OFFSET(objects[mof_cup].id, << -1066.8055, -241.0743, 39.7336 >> )
				FREEZE_ENTITY_POSITION(objects[mof_cup].id, TRUE)
				Create_Prop(objects[mof_cup_1], mod_cup, GET_PED_BONE_COORDS(peds[mpf_coffee_1], BONETAG_PH_R_HAND, <<0,0,0>>))
				Create_Prop(objects[mof_cup_2], mod_cup, GET_PED_BONE_COORDS(peds[mpf_coffee_2], BONETAG_PH_R_HAND, <<0,0,0>>))
				Unload_Asset_Model(sAssetData, mod_cup)			
				
				ATTACH_ENTITY_TO_ENTITY(objects[mof_cup_1].id, peds[mpf_coffee_1], GET_PED_BONE_INDEX(peds[mpf_coffee_1], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
				ATTACH_ENTITY_TO_ENTITY(objects[mof_cup_2].id, peds[mpf_coffee_2], GET_PED_BONE_INDEX(peds[mpf_coffee_2], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
				
				bStartedCoffeeDrinkingAnims1	= FALSE
				bStartedCoffeeDrinkingAnims2	= FALSE
				
				mEvents[mef_ambient_milk].iStage++
			ENDIF
		BREAK
		CASE 3
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1068.971436,-246.413971,38.733139>>, <<-1070.091431,-244.426758,42.951130>>, 4.000000)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				REMOVE_PED_FOR_DIALOGUE(sConvo, 4)
				ADD_PED_FOR_DIALOGUE(sConvo, 4, peds[mpf_milk_f], "Woman")
				mEvents[mef_ambient_milk].iStage++
			ENDIF
		BREAK
		CASE 4
			IF IS_SAFE_TO_START_CONVERSATION()
			AND IS_ENTITY_READY(peds[mpf_milk_f])
			//AND IS_ENTITY_READY(objects[mof_cupboard_door].id)
				IF CREATE_CONVERSATION(sConvo, "LS1AAUD", "LES_1A_IG_1", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
					// Task milk lady
					syncedIDs[ssf_ambient_milk_lady] = CREATE_SYNCHRONIZED_SCENE(<<-1065.19, -240.00, 39.65>>, <<-0.00, 0.00, 27.50>>)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_milk_f], syncedIDs[ssf_ambient_milk_lady], anim_dict_milk_lady, "milkLady_f", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT)
					SAFE_PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(objects[mof_cupboard_door], <<-1065.91, -240.78, 39.17>>, PROP_CUB_DOOR_LIFEBLURB, syncedIDs[ssf_ambient_milk_lady], anim_dict_milk_lady, "milklady_cupboard", INSTANT_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_milk_lady], 0.2)
					
					mEvents[mef_ambient_milk].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF IS_ENTITY_READY(peds[mpf_milk_f])
			
				FLOAT fScenePhase
				IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_ambient_milk_lady])
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_milk_lady])
				ELSE
					fScenePhase = 1.0
				ENDIF
			
				// scene not running because its been interupted or is finished
				IF GET_SCRIPT_TASK_STATUS(peds[mpf_milk_f], SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF DOES_ENTITY_EXIST(objects[mof_cup].id)
						DETACH_ENTITY(objects[mof_cup].id)
					ENDIF
					IF DOES_ENTITY_EXIST(objects[mof_milk_carton].id)
						DETACH_ENTITY(objects[mof_milk_carton].id)
					ENDIF
					OPEN_SEQUENCE_TASK(seq)
						TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID(), 2000)
						TASK_LOOK_AT_ENTITY(null, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(peds[mpf_milk_f], seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					mEvents[mef_ambient_milk].iStage++
				
				// Scene has reached the end
				// OR player has walked off from the scene
				ELIF fScenePhase = 1.0
				OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_milk_f]) > 8.5
				
					syncedIDs[ssf_ambient_milk_lady] = CREATE_SYNCHRONIZED_SCENE(<<-1065.19, -240.00, 39.65>>, <<-0.00, 0.00, 27.50>>)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_milk_f], syncedIDs[ssf_ambient_milk_lady], anim_dict_milk_lady, "milkLady_exitLoop_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT)
					SAFE_PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(objects[mof_cupboard_door], <<-1065.91, -240.78, 39.17>>, PROP_CUB_DOOR_LIFEBLURB, syncedIDs[ssf_ambient_milk_lady], anim_dict_milk_lady, "milklady_exitloop_cupboard", NORMAL_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_milk_lady], TRUE)
					KILL_FACE_TO_FACE_CONVERSATION()
					
					IF DOES_ENTITY_EXIST(objects[mof_milk_carton].id)
						IF IS_ENTITY_ATTACHED(objects[mof_milk_carton].id)
							DETACH_ENTITY(objects[mof_milk_carton].id)
						ENDIF
						DELETE_OBJECT(objects[mof_milk_carton].id)
					ENDIF
					
					IF NOT IS_ENTITY_ATTACHED(objects[mof_cup].id)
						ATTACH_ENTITY_TO_ENTITY(objects[mof_cup].id, peds[mpf_milk_f], GET_PED_BONE_INDEX(peds[mpf_milk_f], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
					ENDIF
					
					mEvents[mef_ambient_milk].iStage++
				
				// Otherwise do anim event checks for attaching and detaching objects
				ELSE
					IF DOES_ENTITY_EXIST(objects[mof_milk_carton].id)
						IF NOT IS_ENTITY_ATTACHED(objects[mof_milk_carton].id)
							IF fScenePhase >= 0.119 
								CPRINTLN(DEBUG_MIKE, "Attaching Milk Carton")
								ATTACH_ENTITY_TO_ENTITY(objects[mof_milk_carton].id, peds[mpf_milk_f], GET_PED_BONE_INDEX(peds[mpf_milk_f], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							ENDIF
						ELSE
							IF fScenePhase >= 0.386
								CPRINTLN(DEBUG_MIKE, "Detaching Milk Carton")
								DETACH_ENTITY(objects[mof_milk_carton].id, FALSE)
								FREEZE_ENTITY_POSITION(objects[mof_milk_carton].id, TRUE)
								DELETE_OBJECT(objects[mof_milk_carton].id)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_ATTACHED(objects[mof_cup].id)
						IF fScenePhase >= 0.755
							FREEZE_ENTITY_POSITION(objects[mof_cup].id, FALSE)
							ATTACH_ENTITY_TO_ENTITY(objects[mof_cup].id, peds[mpf_milk_f], GET_PED_BONE_INDEX(peds[mpf_milk_f], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							VECTOR vCupCoord 
							vCupCoord = GET_PED_BONE_COORDS( peds[mpf_milk_f], BONETAG_PH_R_HAND, <<0,0,0>>)
							CPRINTLN(DEBUG_MIKE, vCupCoord)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF NOT mEvents[mef_ambient_milk].bComplete
		IF mEvents[mef_ambient_milk].iStage > 4
		
			IF IS_ENTITY_READY(peds[mpf_coffee_1])

				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_coffee_1], SCRIPT_TASK_PERFORM_SEQUENCE)
							
					IF NOT bStartedCoffeeDrinkingAnims1
						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_a", v_coffee_guy_1_coord, v_coffee_guy_1_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_b", v_coffee_guy_1_coord, v_coffee_guy_1_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_b", v_coffee_guy_1_coord, v_coffee_guy_1_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_a", v_coffee_guy_1_coord, v_coffee_guy_1_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_b", v_coffee_guy_1_coord, v_coffee_guy_1_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(peds[mpf_coffee_1], seq)
						CLEAR_SEQUENCE_TASK(seq)
						bStartedCoffeeDrinkingAnims1 = TRUE
					ELSE
						IF IS_ENTITY_ATTACHED(objects[mof_cup_1].id)
							DETACH_ENTITY(objects[mof_cup_1].id)
						ENDIF
						
						SET_PED_KEEP_TASK(peds[mpf_coffee_1], TRUE)
						
						OPEN_SEQUENCE_TASK(seq)
							TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
							TASK_STAND_STILL(null, 3000)
							TASK_WANDER_STANDARD(null)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(peds[mpf_coffee_1], seq)
						CLEAR_SEQUENCE_TASK(seq)

						TASK_LOOK_AT_ENTITY(peds[mpf_coffee_1], PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						
						SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_coffee_1])
					ENDIF
						
				ENDIF
			ENDIF
			
			IF IS_ENTITY_READY(peds[mpf_coffee_2])	
				
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_coffee_2], SCRIPT_TASK_PERFORM_SEQUENCE)
					
					IF NOT bStartedCoffeeDrinkingAnims2
						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_b", v_coffee_guy_2_coord, v_coffee_guy_2_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_c", v_coffee_guy_2_coord, v_coffee_guy_2_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_b", v_coffee_guy_2_coord, v_coffee_guy_2_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_a", v_coffee_guy_2_coord, v_coffee_guy_2_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							TASK_PLAY_ANIM_ADVANCED(null, anim_dict_coffee, "idle_b", v_coffee_guy_2_coord, v_coffee_guy_2_rot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(peds[mpf_coffee_2], seq)
						CLEAR_SEQUENCE_TASK(seq)
						bStartedCoffeeDrinkingAnims2 = TRUE
					ELSE
						IF IS_ENTITY_ATTACHED(objects[mof_cup_2].id)
							DETACH_ENTITY(objects[mof_cup_2].id)
						ENDIF
						
						SET_PED_KEEP_TASK(peds[mpf_coffee_2], TRUE)
						
						OPEN_SEQUENCE_TASK(seq)
							TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
							TASK_STAND_STILL(null, 3000)
							TASK_WANDER_STANDARD(null)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(peds[mpf_coffee_2], seq)
						CLEAR_SEQUENCE_TASK(seq)

						TASK_LOOK_AT_ENTITY(peds[mpf_coffee_2], PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						
						SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_coffee_2])
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

BOOL bIntervieweeFemaleGreet

PROC event_ambient_interview()

	VECTOR vInterviewCoord		= <<-1080.48, -244.68, 43.96>> //<< -1080.203, -244.499, 43.96 >>
	VECTOR vInterviewRot		= <<-0.00, 0.00, 27.50>> //<< 0.000, 0.000, 27.440 >>
	
	// track and store scene progress
	float 	fSceneProg
	IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_ambient_interview])
		fSceneProg = GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_interview])
	ELSE
		fSceneProg = 1.0
	ENDIF
	
	SWITCH mEvents[mef_ambient_interview].iStage
		CASE 1
			// delete all existing peds in this scene
			IF DOES_ENTITY_EXIST(peds[mpf_interviewee_f])		DELETE_PED(peds[mpf_interviewee_f])			ENDIF
			IF DOES_ENTITY_EXIST(peds[mpf_interviewee_m]) 		DELETE_PED(peds[mpf_interviewee_m])			ENDIF
			IF DOES_ENTITY_EXIST(peds[mpf_interviewer])			DELETE_PED(peds[mpf_interviewer])			ENDIF
			IF DOES_ENTITY_EXIST(peds[mpf_hr])					DELETE_PED(peds[mpf_hr])					ENDIF
			IF DOES_ENTITY_EXIST(peds[mpf_gamer])				DELETE_PED(peds[mpf_gamer])					ENDIF
			IF DOES_ENTITY_EXIST(objects[mof_hacky_sack].id)	DELETE_OBJECT(objects[mof_hacky_sack].id)	ENDIF
		
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
				CASE msf_st3_go_to_computer
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1070.653687,-243.591553,40.611217>>, <<-1072.526489,-240.037018,44.762821>>, 3.250000)
					AND HAS_MODEL_LOADED(mod_hipster_m)
					AND HAS_MODEL_LOADED(mod_hipster_f_heel)
					AND HAS_ANIM_DICT_LOADED(anim_dict_interview_1st)
						peds[mpf_hr] = 				CREATE_PED(PEDTYPE_MISSION, mod_hipster_f_heel,	<< -1076.32, -246.03, 44.04 >>, 93.97)
						peds[mpf_gamer] = 			CREATE_PED(PEDTYPE_MISSION, mod_hipster_m,	<< -1078.2277, -246.6952, 43.0211 >>, 184.1198)	
						peds[mpf_interviewee_m] = 	CREATE_PED(PEDTYPE_MISSION, mod_hipster_m,	<< -1076.4138, -248.0054, 43.0211 >>, 295.9542)
						Set_Life_Invader_Office_Ped_Variation(mpf_hr)
						Set_Life_Invader_Office_Ped_Variation(mpf_gamer)
						Set_Life_Invader_Office_Ped_Variation(mpf_interviewee_m)
						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
						mEvents[mef_ambient_interview].iStage++
					ELSE
						IF NOT b_OfficeSceneChat
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1068.451660,-243.964478,38.733181>>, <<-1063.518066,-241.417603,42.012131>>, 8.375000)
							KILL_FACE_TO_FACE_CONVERSATION()
							b_OfficeSceneChat = TRUE
						ENDIF
					ENDIF
				BREAK
				CASE msf_st4_mini_game	FALLTHRU
				CASE msf_st5_plant_bomb
					IF HAS_MODEL_LOADED(mod_hipster_m)
					AND HAS_ANIM_DICT_LOADED(anim_dict_interview_1st_exit)
						peds[mpf_gamer] = 			CREATE_PED(PEDTYPE_MISSION, mod_hipster_m,	<< -1078.2277, -246.6952, 43.0211 >>, 184.1198)	
						peds[mpf_interviewee_m] = 	CREATE_PED(PEDTYPE_MISSION, mod_hipster_m,	<< -1076.4138, -248.0054, 43.0211 >>, 295.9542)						
						Set_Life_Invader_Office_Ped_Variation(mpf_gamer)
						Set_Life_Invader_Office_Ped_Variation(mpf_interviewee_m)
							
						mEvents[mef_ambient_interview].iStage = 4
					ENDIF
				BREAK
				CASE msf_st6_leave_building
					IF HAS_MODEL_LOADED(mod_hipster_m)
					AND HAS_MODEL_LOADED(mod_hipster_f_heel)
					AND HAS_MODEL_LOADED(PROP_HACKY_SACK_01)
					AND HAS_ANIM_DICT_LOADED(anim_dict_interview_2nd)
						mEvents[mef_ambient_interview].iStage = 5
					ENDIF
				BREAK
			ENDSWITCH
			
			bIntervieweeFemaleGreet = FALSE
		BREAK
		CASE 2
			IF IS_ENTITY_READY(peds[mpf_gamer])
			AND IS_ENTITY_READY(peds[mpf_hr])
			AND IS_ENTITY_READY(peds[mpf_interviewee_m])
			
				REMOVE_PED_FOR_DIALOGUE(sConvo, 4)
				REMOVE_PED_FOR_DIALOGUE(sConvo, 5)
				ADD_PED_FOR_DIALOGUE(sConvo, 4, peds[mpf_hr], "LIHRLady")
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_interviewee_m], "LIInterviewee")
			
				CLEAR_PED_TASKS(peds[mpf_gamer])
				CLEAR_PED_TASKS(peds[mpf_interviewee_m])
			
				syncedIDs[ssf_ambient_interview] = CREATE_SYNCHRONIZED_SCENE(vInterviewCoord, vInterviewRot)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_gamer], 			syncedIDs[ssf_ambient_interview], 	anim_dict_interview_1st, 	"hr_greet_gamer", 	INSTANT_BLEND_IN,	NORMAL_BLEND_OUT,	SYNCED_SCENE_USE_PHYSICS,	RBF_PLAYER_IMPACT)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_interviewee_m], 	syncedIDs[ssf_ambient_interview],	anim_dict_interview_1st, 	"hr_greet_m", 		INSTANT_BLEND_IN, 	NORMAL_BLEND_OUT,	SYNCED_SCENE_USE_PHYSICS,	RBF_PLAYER_IMPACT)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_hr], 				syncedIDs[ssf_ambient_interview], 	anim_dict_interview_1st, 	"hr_greet_f", 		INSTANT_BLEND_IN, 	WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT,	RBF_PLAYER_IMPACT)
				
				mEvents[mef_ambient_interview].iStage++
			ENDIF
		BREAK
		CASE 3	
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, str_dialogue, "LES_1A_IG_2", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
					mEvents[mef_ambient_interview].iStage++
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
		BREAK
		CASE 4
			// scene is over, play exit loop
			IF fSceneProg = 1.0
				IF HAS_ANIM_DICT_LOADED(anim_dict_interview_1st_exit)
				AND IS_ENTITY_READY(peds[mpf_gamer])
				AND IS_ENTITY_READY(peds[mpf_interviewee_m])

					CLEAR_PED_TASKS(peds[mpf_gamer])
					CLEAR_PED_TASKS(peds[mpf_interviewee_m])

//					syncedIDs[ssf_ambient_interview] = CREATE_SYNCHRONIZED_SCENE(vInterviewCoord, vInterviewRot)
//					TASK_SYNCHRONIZED_SCENE(peds[mpf_gamer], 			syncedIDs[ssf_ambient_interview], 	anim_dict_interview_1st_exit, 	"hr_greet_exitloop_gamer", 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
//					TASK_SYNCHRONIZED_SCENE(peds[mpf_interviewee_m], 	syncedIDs[ssf_ambient_interview], 	anim_dict_interview_1st_exit, 	"hr_greet_exitloop_m", 		NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_LOOP_WITHIN_SCENE, RBF_PLAYER_IMPACT)
//					SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_interview], TRUE)
					TASK_PLAY_ANIM_ADVANCED(peds[mpf_gamer], anim_dict_interview_1st_exit, "hr_greet_exitloop_gamer", vInterviewCoord, vInterviewRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0 , EULER_YXZ, AIK_DISABLE_LEG_IK)
					TASK_PLAY_ANIM_ADVANCED(peds[mpf_interviewee_m], anim_dict_interview_1st_exit, "hr_greet_exitloop_m", vInterviewCoord, vInterviewRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0 , EULER_YXZ, AIK_DISABLE_LEG_IK)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_gamer])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_interviewee_m])
					
					IF NOT IS_PED_INJURED(peds[mpf_hr])
						SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_hr])
					ENDIF
					
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_interview_1st)
					
					mEvents[mef_ambient_interview].bAnimDictPrestreamed = FALSE
					mEvents[mef_ambient_interview].iStage++
				ENDIF
			ELSE
				SET_PED_RESET_FLAG(peds[mpf_interviewee_m], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1072.958252,-250.495453,43.021278>>, <<-1069.429810,-248.660156,45.833778>>, 3.750000)
						IF IS_PED_IN_CURRENT_CONVERSATION(peds[mpf_hr])
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
				ENDIF
			
				// Seamless exit for HR
				IF IS_ENTITY_READY(peds[mpf_hr])
					IF GET_SCRIPT_TASK_STATUS(peds[mpf_hr], SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
					AND HAS_ANIM_EVENT_FIRED(peds[mpf_hr], GET_HASH_KEY("ENDS_IN_RUN")) 
						CPRINTLN(DEBUG_MIKE, "SeamlessExit: HR")
						
						OPEN_SEQUENCE_TASK(seq)
							TASK_FORCE_MOTION_STATE(null, enum_to_int(MS_ON_FOOT_WALK))
							TASK_GO_STRAIGHT_TO_COORD(null, <<-1071.8040, -241.9532, 41.2713>>, PEDMOVE_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(null, << -1065.5614, -241.8892, 38.7332 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING, 37.5678)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(peds[mpf_hr], seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
				
				// Load the exit loop anims
				IF mEvents[mef_ambient_interview].bAnimDictPrestreamed = FALSE
				AND fSceneProg > 0.8
					Load_Asset_AnimDict(sAssetData, anim_dict_interview_1st_exit)
					Load_Asset_Model(sAssetData, PROP_HACKY_SACK_01)
					mEvents[mef_ambient_interview].bAnimDictPrestreamed = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE 5
			// after bomb plant set up new interview scene
			IF mission_stage >= enum_to_int(msf_st6_leave_building)
				Load_Asset_AnimDict(sAssetData, anim_dict_interview_2nd)
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1068.497192,-248.066208,43.021240>>, <<-1069.539673,-248.631180,45.521240>>, 4.000000)
					
					IF DOES_ENTITY_EXIST(peds[mpf_gamer])			DELETE_PED(peds[mpf_gamer])					ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_hr])				DELETE_PED(peds[mpf_hr])					ENDIF
					
					IF DOES_ENTITY_EXIST(peds[mpf_interviewee_m]) 
						IF NOT IS_PED_INJURED(peds[mpf_interviewee_m])
							CLEAR_PED_TASKS(peds[mpf_interviewee_m])	
						ENDIF
					ELSE
						peds[mpf_interviewee_m] = 	CREATE_PED(PEDTYPE_MISSION, mod_hipster_m,	<< -1076.4138, -248.0054, 43.0211 >>, 295.9542)	
						Set_Life_Invader_Office_Ped_Variation(mpf_interviewee_m)
					ENDIF

					IF NOT DOES_ENTITY_EXIST(peds[mpf_interviewee_f])
						peds[mpf_interviewee_f] = 	CREATE_PED(PEDTYPE_MISSION, mod_hipster_f_heel, << -1082.2778, -247.6897, 43.0211 >> , 0.0)
						Set_Life_Invader_Office_Ped_Variation(mpf_interviewee_f)
					ENDIF
					IF NOT DOES_ENTITY_EXIST(peds[mpf_interviewer])
						peds[mpf_interviewer] = 	CREATE_PED(PEDTYPE_MISSION,mod_hipster_m,	<< -1080.5645, -246.6328, 43.0211 >> , 0.0)				
						Set_Life_Invader_Office_Ped_Variation(mpf_interviewer)
					ENDIF
					
					Create_Prop(objects[mof_hacky_sack], PROP_HACKY_SACK_01, << -1081.5645, -246.6328, 43.0211 >>)
					Unload_Asset_Model(sAssetData, PROP_HACKY_SACK_01)
					ATTACH_ENTITY_TO_ENTITY(objects[mof_hacky_sack].id, peds[mpf_interviewer],  get_ped_bone_index(peds[mpf_interviewer], BONETAG_PH_R_HAND), <<0,0,0>>,<<0,0,0>>, TRUE)
					
					//Swap out the dictionaries at this point
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_interview_1st_exit)
					
					mEvents[mef_ambient_interview].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF IS_ENTITY_READY(peds[mpf_interviewer])
			AND IS_ENTITY_READY(peds[mpf_interviewee_f])
			AND IS_ENTITY_READY(peds[mpf_interviewee_m])
			
				CLEAR_PED_TASKS(peds[mpf_interviewer])
				CLEAR_PED_TASKS(peds[mpf_interviewee_m])
				CLEAR_PED_TASKS(peds[mpf_interviewee_f])
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

				ADD_PED_FOR_DIALOGUE(sConvo, 4, peds[mpf_interviewer], "LIInterviewer")
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_interviewee_m], "LIInterviewee")

				mEvents[mef_ambient_interview].iStage++
			ENDIF
		BREAK
		CASE 7
			IF IS_ENTITY_READY(peds[mpf_interviewer])
			AND IS_ENTITY_READY(peds[mpf_interviewee_f])
			AND IS_ENTITY_READY(peds[mpf_interviewee_m])
				IF CREATE_CONVERSATION(sConvo, "LS1AAUD", "LES_1A_IG_7", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
					syncedIDs[ssf_ambient_interview] = CREATE_SYNCHRONIZED_SCENE(vInterviewCoord, vInterviewRot)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_interviewee_f], 	syncedIDs[ssf_ambient_interview], 	anim_dict_interview_2nd, "interview_f", 		INSTANT_BLEND_IN, 	NORMAL_BLEND_OUT, 	SYNCED_SCENE_USE_PHYSICS, 								RBF_PLAYER_IMPACT)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_interviewee_m], 	syncedIDs[ssf_ambient_interview],	anim_dict_interview_2nd, "interview_m", 		INSTANT_BLEND_IN, 	WALK_BLEND_OUT,		SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT, 	RBF_PLAYER_IMPACT)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_interviewer], 		syncedIDs[ssf_ambient_interview], 	anim_dict_interview_2nd, "interview_boss", 		INSTANT_BLEND_IN, 	WALK_BLEND_OUT,		SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT, 	RBF_PLAYER_IMPACT)
					SET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_interview], 0.1)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedIDs[ssf_ambient_interview], FALSE)
				
					Load_Asset_AnimDict(sAssetData, anim_dict_interview_2nd_exit)
					
					mEvents[mef_ambient_interview].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF IS_ENTITY_READY(peds[mpf_interviewer])
			AND IS_ENTITY_READY(peds[mpf_interviewee_f])
			AND IS_ENTITY_READY(peds[mpf_interviewee_m])
				// Detach the hacky sack
				IF DOES_ENTITY_EXIST(objects[mof_hacky_sack].id)
				AND IS_ENTITY_ATTACHED(objects[mof_hacky_sack].id)
				AND IS_ENTITY_IN_ANGLED_AREA(objects[mof_hacky_sack].id, <<-1082.668457,-247.492935,43.021118>>, <<-1082.259766,-247.488403,43.121117>>, 0.350000)
					DETACH_ENTITY(objects[mof_hacky_sack].id)
					SET_OBJECT_AS_NO_LONGER_NEEDED(objects[mof_hacky_sack].id)
				ENDIF

				// seamless exit, interviewer
				IF GET_SCRIPT_TASK_STATUS(peds[mpf_interviewer], SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
					IF HAS_ANIM_EVENT_FIRED(peds[mpf_interviewer], GET_HASH_KEY("ENDS_IN_WALK"))
						
						CPRINTLN(DEBUG_MIKE, "SeamlessExit: Interviewer")
						OPEN_SEQUENCE_TASK(seq)
							TASK_FORCE_MOTION_STATE(null, enum_to_int(MS_ON_FOOT_WALK))
							//TASK_GO_STRAIGHT_TO_COORD(null, << -1073.7821, -250.8327, 43.0213 >>, PEDMOVE_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(null, << -1048.6239, -238.3391, 43.0213 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 288.1765)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(peds[mpf_interviewer], seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
				
				// seamless exit, interviewee
				IF GET_SCRIPT_TASK_STATUS(peds[mpf_interviewee_m], SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
					IF HAS_ANIM_EVENT_FIRED(peds[mpf_interviewee_m], GET_HASH_KEY("ENDS_IN_WALK"))
					
						CPRINTLN(DEBUG_MIKE, "SeamlessExit: Interviewee_m")
						OPEN_SEQUENCE_TASK(seq)
							TASK_FORCE_MOTION_STATE(null, enum_to_int(MS_ON_FOOT_WALK))
							//TASK_GO_STRAIGHT_TO_COORD(null, << -1073.7821, -250.8327, 43.0213 >>, PEDMOVE_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(null, << -1051.1515, -236.2580, 43.0213 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 237.9632)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(peds[mpf_interviewee_m], seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
				
				IF HAS_ANIM_DICT_LOADED(anim_dict_interview_2nd_exit)
					IF fSceneProg = 1.0
						CLEAR_PED_TASKS (peds[mpf_interviewee_f])
						
						syncedIDs[ssf_ambient_interview] = CREATE_SYNCHRONIZED_SCENE(vInterviewCoord, vInterviewRot)
						TASK_SYNCHRONIZED_SCENE (peds[mpf_interviewee_f], syncedIDs[ssf_ambient_interview], anim_dict_interview_2nd_exit, "interview_exitloop_f", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
						SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_interview], true)
						
						Unload_Asset_Anim_Dict(sAssetData, anim_dict_interview_2nd)
						mEvents[mef_ambient_interview].bAnimDictPrestreamed = FALSE
						mEvents[mef_ambient_interview].iStage++
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MIKE, "EXIT LOOP DICTIONARY HAS NOT LOADED!")
				ENDIF	
			ENDIF
		BREAK
		CASE 9
			mEvents[mef_ambient_interview].iStage++
		
//			// Finished, walking to the boardroom
//			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_interviewer], SCRIPT_TASK_PERFORM_SEQUENCE)
//				IF IS_SAFE_TO_START_CONVERSATION()
//				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_interviewer]) < 3.0
//					IF CREATE_CONVERSATION(sConvo, str_dialogue, "LES1A_WATCH", CONV_PRIORITY_HIGH)
//						mEvents[mef_ambient_interview].iStage++
//					ENDIF
//				ENDIF
//			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF DOES_ENTITY_EXIST(peds[mpf_interviewee_f])
	AND NOT IS_PED_INJURED(peds[mpf_interviewee_f])
		IF NOT bIntervieweeFemaleGreet
		AND mEvents[mef_ambient_interview].iStage > 8
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_interviewee_f]) < 3.0
				IF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_interviewee_f])
					PLAY_PED_AMBIENT_SPEECH(peds[mpf_interviewee_f], "GENERIC_HI", SPEECH_PARAMS_FORCE)
					bIntervieweeFemaleGreet = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC

PROC event_ambient_office()
	// track and store scene progress
	FLOAT fSceneProg
	BOOL bCreatePeds
	IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_ambient_office])
		fSceneProg = GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_office])
	ELSE
		fSceneProg = 1.0
	ENDIF

	SWITCH mEvents[mef_ambient_office].iStage
		CASE 1
			// Firstly check all assets are ready for this to begin
			BOOL bAreAssetsReady
			
			IF HAS_MODEL_LOADED(mod_hipster_m)
			
				SWITCH int_to_enum(mission_stage_flag, mission_stage)
					CASE msf_st3_go_to_computer
						IF HAS_ANIM_DICT_LOADED(anim_dict_airguitar_1st)
						AND HAS_ANIM_DICT_LOADED(anim_dict_airguitar_1st_exit)
						AND HAS_MODEL_LOADED(PROP_MONITOR_01D)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1082.850098,-244.797516,42.795040>>, <<-1073.899292,-248.911301,45.521118>>, 1.000000)
								IF NOT bSwappedMonitorToPopups
									CREATE_MODEL_SWAP(v_engineer_monitor, 1.0, PROP_MONITOR_LI, PROP_MONITOR_01D, TRUE)
									Unload_Asset_Model(sAssetData, PROP_MONITOR_01D)
									bSwappedMonitorToPopups = TRUE
								ENDIF
								bCreatePeds = TRUE
								bAreAssetsReady = TRUE
								mEvents[mef_ambient_office].iStage = 101
							ENDIF
						ENDIF
					BREAK
					CASE msf_st4_mini_game
					FALLTHRU
					CASE msf_st5_plant_bomb
						IF HAS_ANIM_DICT_LOADED(anim_dict_airguitar_1st_exit)
							bCreatePeds = TRUE
							bAreAssetsReady = TRUE
							mEvents[mef_ambient_office].iStage = 3
						ENDIF
					BREAK
					CASE msf_st6_leave_building
						IF HAS_ANIM_DICT_LOADED(anim_dict_airguitar_2nd)
							bCreatePeds = TRUE
							bAreAssetsReady = TRUE
							mEvents[mef_ambient_office].iStage = 5
						ENDIF
					BREAK
				ENDSWITCH
				
				IF bAreAssetsReady
				AND bCreatePeds
					IF DOES_ENTITY_EXIST(peds[mpf_office_boss])		DELETE_PED(peds[mpf_office_boss])	ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_office_a]) 		DELETE_PED(peds[mpf_office_a])		ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_office_b])		DELETE_PED(peds[mpf_office_b])		ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_office_c])		DELETE_PED(peds[mpf_office_c])		ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_office_d])		DELETE_PED(peds[mpf_office_d])		ENDIF
					
					peds[mpf_office_boss] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, << -1064.7488, -238.4493, 43.0211 >>, 54.0951)
					peds[mpf_office_a] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, << -1064.7488, -238.4493, 43.0211 >>, 54.0951)
					peds[mpf_office_b] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, << -1064.7488, -238.4493, 43.0211 >>, 54.0951)
					peds[mpf_office_c] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, << -1064.7488, -238.4493, 43.0211 >>, 54.0951)
					peds[mpf_office_d] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, << -1063.5718, -246.7145, 44.0194 >>, -22.918)
					SET_PED_NAME_DEBUG(peds[mpf_office_boss], "OFFICE_boss")
					SET_PED_NAME_DEBUG(peds[mpf_office_a], "OFFICE_a")
					SET_PED_NAME_DEBUG(peds[mpf_office_b], "OFFICE_b")
					SET_PED_NAME_DEBUG(peds[mpf_office_c], "OFFICE_c")
					SET_PED_NAME_DEBUG(peds[mpf_office_d], "OFFICE_d")
					
					Set_Life_Invader_Office_Ped_Variation(mpf_office_boss)
					Set_Life_Invader_Office_Ped_Variation(mpf_office_a)
					Set_Life_Invader_Office_Ped_Variation(mpf_office_b)
					Set_Life_Invader_Office_Ped_Variation(mpf_office_c)
					Set_Life_Invader_Office_Ped_Variation(mpf_office_d)
					
					// set up chairs
					IF NOT DOES_ENTITY_EXIST(objects[mof_office_boss_chair].id)
						objects[mof_office_boss_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1068.0780,-238.9636,43.0229>>, 1.0, mod_chair)
						IF DOES_ENTITY_EXIST(objects[mof_office_boss_chair].id)
							SET_ENTITY_COORDS_NO_OFFSET(objects[mof_office_boss_chair].id, <<-1068.0780,-238.9636,43.0229>>)
							SET_ENTITY_ROTATION(objects[mof_office_boss_chair].id, <<-0.0000, -0.0000, 52.50>>)
							SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_office_boss_chair].id, peds[mpf_office_boss], FALSE)
							FREEZE_ENTITY_POSITION(objects[mof_office_boss_chair].id, TRUE)
						ENDIF
					ENDIF
					IF NOT DOES_ENTITY_EXIST(objects[mof_office_a_chair].id)
						objects[mof_office_a_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1066.25, -238.04, 43.02>>, 1.0, mod_chair)
						IF DOES_ENTITY_EXIST(objects[mof_office_a_chair].id)
							IF (mEvents[mef_ambient_office].iStage = 3)
							OR (mEvents[mef_ambient_office].iStage = 4)
								SET_ENTITY_COORDS_NO_OFFSET(objects[mof_office_a_chair].id, <<-1065.5271, -237.9645, 43.0258>>)
								SET_ENTITY_ROTATION(objects[mof_office_a_chair].id, <<-0.1264, 0.0338, -88.9461>>)
							ELSE
								//ATTACH_ENTITY_TO_ENTITY(objects[mof_office_a_chair].id, peds[mpf_office_a], get_ped_bone_index(peds[mpf_office_a], BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
							ENDIF
							SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_office_a_chair].id, peds[mpf_office_a], FALSE)
						ENDIF
					ENDIF
					IF NOT DOES_ENTITY_EXIST(objects[mof_office_b_chair].id)
						objects[mof_office_b_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1065.51, -239.51, 43.02>>, 1.0, mod_chair)
						IF DOES_ENTITY_EXIST(objects[mof_office_b_chair].id)
							IF (mEvents[mef_ambient_office].iStage = 3)
							OR (mEvents[mef_ambient_office].iStage = 4)
								SET_ENTITY_COORDS_NO_OFFSET(objects[mof_office_b_chair].id, <<-1064.8973, -240.5339, 43.0251>>)
								SET_ENTITY_ROTATION(objects[mof_office_b_chair].id, <<-0.0000, -0.0005, 167.7005>>)
							ELSE
								//ATTACH_ENTITY_TO_ENTITY(objects[mof_office_b_chair].id, peds[mpf_office_b], get_ped_bone_index(peds[mpf_office_b], BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
							ENDIF
							SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_office_b_chair].id, peds[mpf_office_b], FALSE)
						ENDIF
					ENDIF
					IF NOT DOES_ENTITY_EXIST(objects[mof_office_c_chair].id)
						objects[mof_office_c_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1057.86, -243.51, 43.02>>, 1.0, mod_chair)
						IF DOES_ENTITY_EXIST(objects[mof_office_c_chair].id)
							SET_ENTITY_COORDS_NO_OFFSET(objects[mof_office_c_chair].id, <<-1058.1906, -243.0693, 43.0229>>)
							SET_ENTITY_ROTATION(objects[mof_office_c_chair].id, <<-0.0000, -0.0000, -134.5749>>)
							SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_office_c_chair].id, peds[mpf_office_c], FALSE)
							FREEZE_ENTITY_POSITION(objects[mof_office_c_chair].id, TRUE)
						ENDIF
					ENDIF
					IF NOT DOES_ENTITY_EXIST(objects[mof_office_d_chair].id)
						objects[mof_office_d_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1063.61, -246.65, 43.02>>, 1.0, mod_chair)
						IF DOES_ENTITY_EXIST(objects[mof_office_d_chair].id)
							
							IF mission_stage < enum_to_int(msf_st4_mini_game)
								SET_ENTITY_COORDS(objects[mof_office_d_chair].id, <<-1063.832, -246.746, 43.023>>)
								SET_ENTITY_ROTATION(objects[mof_office_d_chair].id, <<-0.04, 0.04, 159.275>>)
								CPRINTLN(DEBUG_MIKE, "[CHAIR D] Moved inside of event_ambient_office()")
							ENDIF
							SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_office_d_chair].id, peds[mpf_office_d], FALSE)
							
							//FREEZE_ENTITY_POSITION(objects[mof_office_d_chair].id, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 101
			IF IS_ENTITY_READY(peds[mpf_office_c])
			AND IS_ENTITY_READY(peds[mpf_office_d])
			AND IS_ENTITY_READY(objects[mof_office_d_chair].id)

				syncedIDs[ssf_ambient_office] = CREATE_SYNCHRONIZED_SCENE(<< -1067.002, -239.245, 43.021 >>, << -0.000, 0.000, 27.750 >>)
				TASK_SYNCHRONIZED_SCENE (peds[mpf_office_c], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st_exit, "air_guitar_01_exitloop_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE (peds[mpf_office_d], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st_exit, "air_guitar_01_exitloop_d", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_d_chair], syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_d", anim_dict_airguitar_1st_exit, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_office], TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_c])
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
				
				mEvents[mef_ambient_office].iStage++
			ENDIF
		BREAK
		CASE 102
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1068.843140,-246.079193,42.771278>>, <<-1067.171875,-249.250885,46.783058>>, 5.750000)
			AND REQUEST_MISSION_AUDIO_BANK("Lester1A_01")
			
				ADD_PED_FOR_DIALOGUE(sConvo, 7, peds[mpf_office_boss], "LIOffice1")
				ADD_PED_FOR_DIALOGUE(sConvo, 8, peds[mpf_office_a], "LIOffice2")
				ADD_PED_FOR_DIALOGUE(sConvo, 6, peds[mpf_office_b], "LIOfficeGroup")
				
				INT i
				FOR i = 0 TO iUsablePopups-1
					IF sPopups[i].iType != -1
					AND sPopups[i].iSfxLoop != -1
					AND sPopups[i].bSfxPlaying = FALSE
						STRING strSFX
						IF iPopupsSFXPlaying < 6
							SWITCH (iPopupsSFXPlaying)
								CASE 0 strSFX = "POPUP_MUSIC_01" BREAK
								CASE 1 strSFX = "POPUP_MUSIC_02" BREAK
								CASE 2 strSFX = "POPUP_MUSIC_03" BREAK
								CASE 3 strSFX = "POPUP_MUSIC_04" BREAK
								CASE 4 strSFX = "POPUP_MUSIC_05" BREAK
								CASE 5 strSFX = "POPUP_MUSIC_06" BREAK
								DEFAULT strSFX = "POPUP_MUSIC_RND"	BREAK
							ENDSWITCH
						ELSE
							strSFX = "POPUP_MUSIC_RND"
						ENDIF
						PLAY_SOUND_FROM_COORD(sPopups[i].iSfxLoop, strSFX, <<-1059.63, -244.61, 43.92>>, "LESTER1A_SOUNDS")
						SET_VARIABLE_ON_SOUND(sPopups[i].iSfxLoop, "State", 0)
						CPRINTLN(DEBUG_MIKE, "INIT_MINIGAME PLAY POPUP LOOP SFX:", i)
						sPopups[i].bSfxPlaying = TRUE
						iPopupsSFXPlaying++
					ENDIF
				ENDFOR
				
				REPEAT COUNT_OF(sPopups) i
					IF sPopups[i].bSfxPlaying
					AND sPopups[i].iSfxLoop != -1
						SET_VARIABLE_ON_SOUND(sPopups[i].iSfxLoop, "TracksPlaying", TO_FLOAT(CLAMP_INT(iPopupsSFXPlaying, 1, 12)))
					ENDIF
				ENDREPEAT
				
				mEvents[mef_ambient_office].iStage = 2
			ENDIF
		BREAK
		CASE 2
			// first phase main animation
			IF IS_ENTITY_READY(peds[mpf_office_a])
			AND IS_ENTITY_READY(peds[mpf_office_b])
			AND IS_ENTITY_READY(peds[mpf_office_c])
			AND IS_ENTITY_READY(peds[mpf_office_d])
			AND IS_ENTITY_READY(peds[mpf_office_boss])
			AND IS_ENTITY_READY(objects[mof_office_a_chair].id)
			AND IS_ENTITY_READY(objects[mof_office_b_chair].id)
			AND IS_ENTITY_READY(objects[mof_office_d_chair].id)
				IF (IS_SAFE_TO_START_CONVERSATION()
				AND CREATE_CONVERSATION(sConvo, str_dialogue, "LES_1A_IG_3", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES))
				OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					syncedIDs[ssf_ambient_office] = CREATE_SYNCHRONIZED_SCENE(<< -1067.002, -239.245, 43.021 >>, << -0.000, 0.000, 27.750 >>)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_office_boss], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st, "air_guitar_01_boss", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_office_a], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st, "air_guitar_01_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_office_b], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st, "air_guitar_01_b", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_office_c], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st, "air_guitar_01_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_office_d], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st, "air_guitar_01_d", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_boss])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_a])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_b])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_c])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
					
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_boss_chair], 	syncedIDs[ssf_ambient_office], "air_guitar_01_chair_boss", anim_dict_airguitar_1st, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_a_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_01_chair_a", anim_dict_airguitar_1st, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_b_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_01_chair_b", anim_dict_airguitar_1st, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_d_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_01_chair_d", anim_dict_airguitar_1st, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				
					mEvents[mef_ambient_office].bSafeAfterSkip = TRUE
					mEvents[mef_ambient_office].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			// first phase exit loop
			IF fSceneProg = 1.0
			OR (IS_CUTSCENE_PLAYING() AND mission_stage = enum_to_int(msf_st4_mini_game) AND mission_substage > 5)
				IF IS_ENTITY_READY(peds[mpf_office_a])
				AND IS_ENTITY_READY(peds[mpf_office_b])
				AND IS_ENTITY_READY(peds[mpf_office_c])
				AND IS_ENTITY_READY(peds[mpf_office_d])
				AND IS_ENTITY_READY(peds[mpf_office_boss])
				AND IS_ENTITY_READY(objects[mof_office_a_chair].id)
				AND IS_ENTITY_READY(objects[mof_office_b_chair].id)
				AND IS_ENTITY_READY(objects[mof_office_d_chair].id)
				
					syncedIDs[ssf_ambient_office] = CREATE_SYNCHRONIZED_SCENE(<< -1067.002, -239.245, 43.021 >>, << -0.000, 0.000, 27.750 >>)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_boss], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st_exit, "air_guitar_01_exitloop_boss", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_a], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st_exit, "air_guitar_01_exitloop_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_b], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st_exit, "air_guitar_01_exitloop_b", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_c], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st_exit, "air_guitar_01_exitloop_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_boss])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_a])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_b])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_c])

					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_boss_chair], 	syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_boss", anim_dict_airguitar_1st_exit, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_a_chair], syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_a", anim_dict_airguitar_1st_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_b_chair], syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_b", anim_dict_airguitar_1st_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					
					IF mission_stage < enum_to_int(msf_st4_mini_game)
						TASK_SYNCHRONIZED_SCENE (peds[mpf_office_d], syncedIDs[ssf_ambient_office], anim_dict_airguitar_1st_exit, "air_guitar_01_exitloop_d", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_d_chair], syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_d", anim_dict_airguitar_1st_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)

						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
					ENDIF
										
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_office], TRUE)

					Unload_Asset_Anim_Dict(sAssetData, anim_dict_airguitar_1st)
					
					mEvents[mef_ambient_office].bSafeAfterSkip = TRUE
					mEvents[mef_ambient_office].bAnimDictPrestreamed = FALSE
					mEvents[mef_ambient_office].iStage++
				ENDIF
			ELSE
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(peds[mpf_office_boss])
						IF IS_PED_IN_CURRENT_CONVERSATION(peds[mpf_office_boss])
						AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1066.78, -239.63, 43.86>>) > 225 // 15^2
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
				ENDIF
	
				// stream in the exit loop
				IF mEvents[mef_ambient_office].bAnimDictPrestreamed = FALSE
				AND fSceneProg > 0.8
					Load_Asset_AnimDict(sAssetData, anim_dict_airguitar_1st_exit)
					mEvents[mef_ambient_office].bAnimDictPrestreamed = TRUE
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_office_boss])
					IF (fSceneProg > 0.745 AND fSceneProg < 0.930)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_boss])
							CPRINTLN(DEBUG_MIKE, "PLAY_PED_AMBIENT_SPEECH: AIRROCKING")
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_boss], "AIRROCKING", "LIENGINEER2", SPEECH_PARAMS_FORCE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_office_d])
					// Process drumming ambient speech
					IF (fSceneProg > 0.745 AND fSceneProg < 0.930)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
							CPRINTLN(DEBUG_MIKE, "PLAY_PED_AMBIENT_SPEECH: Drumming")
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_d], "DRUMMING", "AIRDRUMMER", SPEECH_PARAMS_FORCE)
						ENDIF
					ELSE
						IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_office_d])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF mission_stage >= enum_to_int(msf_st6_leave_building)
				mEvents[mef_ambient_office].iStage++
			ENDIF
		BREAK
		CASE 5
			// second office scene
			IF IS_ENTITY_READY(peds[mpf_office_a])
			AND IS_ENTITY_READY(peds[mpf_office_b])
			AND IS_ENTITY_READY(peds[mpf_office_c])
			AND IS_ENTITY_READY(peds[mpf_office_d])
			AND IS_ENTITY_READY(peds[mpf_office_boss])
			AND IS_ENTITY_READY(objects[mof_office_a_chair].id)
			AND IS_ENTITY_READY(objects[mof_office_b_chair].id)
			AND IS_ENTITY_READY(objects[mof_office_d_chair].id)
			
				CLEAR_PED_TASKS(peds[mpf_office_boss])
				CLEAR_PED_TASKS(peds[mpf_office_a])
				CLEAR_PED_TASKS(peds[mpf_office_b])
				CLEAR_PED_TASKS(peds[mpf_office_c])
				CLEAR_PED_TASKS(peds[mpf_office_d])
				
				SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_a_chair], INSTANT_BLEND_OUT)
				SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_b_chair], INSTANT_BLEND_OUT)		
				SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_d_chair], INSTANT_BLEND_OUT)

				syncedIDs[ssf_ambient_office] = CREATE_SYNCHRONIZED_SCENE(<< -1067.002, -239.245, 43.021 >>, << -0.000, 0.000, 27.750 >>)
				TASK_SYNCHRONIZED_SCENE (peds[mpf_office_boss], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd, "air_guitar_02_boss", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE (peds[mpf_office_a], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd, "air_guitar_02_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE (peds[mpf_office_b], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd, "air_guitar_02_b", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE (peds[mpf_office_c], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd, "air_guitar_02_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE (peds[mpf_office_d], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd, "air_guitar_02_d", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_boss])
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_a])
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_b])
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_c])
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
				
				SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_a_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_02_chair_a", anim_dict_airguitar_2nd, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_b_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_02_chair_b", anim_dict_airguitar_2nd, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_d_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_02_chair_d", anim_dict_airguitar_2nd, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)

				Unload_Asset_Anim_Dict(sAssetData, anim_dict_airguitar_1st_exit)
				
				mEvents[mef_ambient_office].bSafeAfterSkip = TRUE
				mEvents[mef_ambient_office].iStage++
			ENDIF
		BREAK
		CASE 6
			// second phase exit loop
			IF fSceneProg = 1.0
				IF IS_ENTITY_READY(peds[mpf_office_a])
				AND IS_ENTITY_READY(peds[mpf_office_b])
				AND IS_ENTITY_READY(peds[mpf_office_c])
				AND IS_ENTITY_READY(peds[mpf_office_d])
				AND IS_ENTITY_READY(peds[mpf_office_boss])
				AND IS_ENTITY_READY(objects[mof_office_a_chair].id)
				AND IS_ENTITY_READY(objects[mof_office_b_chair].id)
				AND IS_ENTITY_READY(objects[mof_office_d_chair].id)
				
					CLEAR_PED_TASKS(peds[mpf_office_boss])
					CLEAR_PED_TASKS(peds[mpf_office_a])
					CLEAR_PED_TASKS(peds[mpf_office_b])
					CLEAR_PED_TASKS(peds[mpf_office_c])
					CLEAR_PED_TASKS(peds[mpf_office_d])
					
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_a_chair], INSTANT_BLEND_OUT)
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_b_chair], INSTANT_BLEND_OUT)		
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_d_chair], INSTANT_BLEND_OUT)
				
					syncedIDs[ssf_ambient_office] = CREATE_SYNCHRONIZED_SCENE(<< -1067.002, -239.245, 43.021 >>, << -0.000, 0.000, 27.750 >>)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_boss], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_02_exit_boss", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_a], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_02_exit_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_b], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_02_exit_b", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_c], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_02_exit_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_d], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_02_exit_d", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_boss])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_a])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_b])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_c])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
					
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_a_chair], syncedIDs[ssf_ambient_office], "air_guitar_02_exit_chair_a", anim_dict_airguitar_2nd_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_b_chair], syncedIDs[ssf_ambient_office], "air_guitar_02_exit_chair_b", anim_dict_airguitar_2nd_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_d_chair], syncedIDs[ssf_ambient_office], "air_guitar_02_exit_chair_d", anim_dict_airguitar_2nd_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					
					// cut off drumming
					IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_office_d])
					ENDIF
					
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_airguitar_2nd)
					mEvents[mef_ambient_office].iStage++
				ENDIF
			ELSE
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(peds[mpf_office_boss])
						IF IS_PED_IN_CURRENT_CONVERSATION(peds[mpf_office_boss])
						AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1066.78, -239.63, 43.86>>) > 225 // 15^2
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
				ENDIF
			
				// Stream in the exit loops
				IF mEvents[mef_ambient_office].bAnimDictPrestreamed = FALSE
				AND fSceneProg > 0.8
					Load_Asset_AnimDict(sAssetData, anim_dict_airguitar_2nd_exit)
					mEvents[mef_ambient_office].bAnimDictPrestreamed = TRUE
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_office_d])
					IF (fSceneProg > 0.135 AND fSceneProg < 0.216)
					OR (fSceneProg > 0.329 AND fSceneProg < 0.419)
					OR (fSceneProg > 0.824 AND fSceneProg < 0.99)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
							CPRINTLN(DEBUG_MIKE, "PLAY_PED_AMBIENT_SPEECH: Drumming")
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_d], "DRUMMING", "AIRDRUMMER", SPEECH_PARAMS_FORCE)
						ENDIF
					ELSE
						// cut off drumming
						IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_office_d])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF fSceneProg >= 1.0
				IF IS_ENTITY_READY(peds[mpf_office_a])
				AND IS_ENTITY_READY(peds[mpf_office_b])
				AND IS_ENTITY_READY(peds[mpf_office_c])
				AND IS_ENTITY_READY(peds[mpf_office_d])
				AND IS_ENTITY_READY(peds[mpf_office_boss])
				AND IS_ENTITY_READY(objects[mof_office_a_chair].id)
				AND IS_ENTITY_READY(objects[mof_office_b_chair].id)
				AND IS_ENTITY_READY(objects[mof_office_d_chair].id)
				
					CLEAR_PED_TASKS(peds[mpf_office_boss])
					CLEAR_PED_TASKS(peds[mpf_office_a])
					CLEAR_PED_TASKS(peds[mpf_office_b])
					CLEAR_PED_TASKS(peds[mpf_office_c])
					CLEAR_PED_TASKS(peds[mpf_office_d])
					
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_boss_chair], INSTANT_BLEND_OUT)
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_a_chair], INSTANT_BLEND_OUT)
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_b_chair], INSTANT_BLEND_OUT)		
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_d_chair], INSTANT_BLEND_OUT)
				
					syncedIDs[ssf_ambient_office] = CREATE_SYNCHRONIZED_SCENE(<< -1067.002, -239.245, 43.021 >>, << -0.000, 0.000, 27.750 >>)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_boss], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_01_exitloop_boss", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_a], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_01_exitloop_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_b], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_01_exitloop_b", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_c], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_01_exitloop_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_office_d], syncedIDs[ssf_ambient_office], anim_dict_airguitar_2nd_exit, "air_guitar_01_exitloop_d", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_boss])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_a])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_b])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_c])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
					
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_boss_chair], 	syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_boss", anim_dict_airguitar_2nd_exit, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_a_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_a", 	anim_dict_airguitar_2nd_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					//SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_b_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_b", 	anim_dict_airguitar_2nd_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_d_chair], 		syncedIDs[ssf_ambient_office], "air_guitar_01_exitloop_chair_d", 	anim_dict_airguitar_2nd_exit, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_office], TRUE)
					
					// cut off drumming
					IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_office_d])
					ENDIF
					
					mEvents[mef_ambient_office].iStage++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC event_ambient_paper_throw()	

	IF mEvents[mef_ambient_paper_throw].iStage = 1
	
		IF NOT DOES_ENTITY_EXIST(objects[mof_office_thrower_f_chair].id)
		AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1054.1595, -241.7358, 43.6537 >>, 1.0, PROP_OFF_CHAIR_01)
			objects[mof_office_thrower_f_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<< -1054.1595, -241.7358, 43.6537 >>, 1.0, PROP_OFF_CHAIR_01)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objects[mof_office_thrower_m_chair].id)
		AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1053.2346, -245.2269, 43.6272 >>, 1.0, PROP_OFF_CHAIR_01)
			objects[mof_office_thrower_m_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<< -1053.2346, -245.2269, 43.6272 >>, 1.0, PROP_OFF_CHAIR_01)
		ENDIF
	
		IF HAS_MODEL_LOADED(mod_hipster_m)
		AND HAS_MODEL_LOADED(mod_hipster_f)
		AND HAS_MODEL_LOADED(PROP_PAPER_BALL)
		AND HAS_ANIM_DICT_LOADED(anim_dict_paper_throw)
		AND DOES_ENTITY_EXIST(objects[mof_office_thrower_f_chair].id)
		AND DOES_ENTITY_EXIST(objects[mof_office_thrower_m_chair].id)
		AND (NOT IS_CUTSCENE_PLAYING() OR GET_CUTSCENE_TIME() > 3700)
			
			// create the paper throwing peds
			peds[mpf_office_paper_f] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_f, << -1054.9045, -241.1034, 43.0211 >>, 297.0785)
			peds[mpf_office_paper_m] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, << -1053.4689, -243.6800, 43.0211 >>, 118.6611)
			Set_Life_Invader_Office_Ped_Variation(mpf_office_paper_f)
			Set_Life_Invader_Office_Ped_Variation(mpf_office_paper_m)
			
			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_office_thrower_f_chair].id, peds[mpf_office_paper_f], FALSE)
			SET_ENTITY_NO_COLLISION_ENTITY(objects[mof_office_thrower_m_chair].id, peds[mpf_office_paper_m], FALSE)
			
			iPaperThrow = 1
			bFirstThrow = TRUE
			bJustThrown = FALSE
			mEvents[mef_ambient_paper_throw].iStage++
		ENDIF
	
	ENDIF
	
	IF mEvents[mef_ambient_paper_throw].iStage > 1

		SWITCH mEvents[mef_ambient_paper_throw].iStage
			CASE 2
				
				// Do random throw anim once base has finished
				IF IS_ENTITY_READY(peds[mpf_office_paper_f])
				AND IS_ENTITY_READY(peds[mpf_office_paper_m])
				AND IS_ENTITY_READY(objects[mof_office_thrower_f_chair].id)
				AND IS_ENTITY_READY(objects[mof_office_thrower_m_chair].id)
				
					FLOAT fRandProb
					fRandProb = GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
					
					// scene is already running and has reached the end
					// probability test: 60% chance of the paper throw happening
					// peds not already speaking ambient dialogue
					IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_ambient_paper_throw])
					AND GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_paper_throw]) = 1.0
					AND (fRandProb <= 0.60 OR bFirstThrow)
					AND NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_paper_f])
					AND NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_paper_m])
					AND NOT bJustThrown
					
					// its the first throw, iPaperThrow = 1 and the player is walking down the office
					// OR not first throw
					AND ((bFirstThrow AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1060.480835,-240.979706,43.021118>>, <<-1053.232910,-237.168442,45.771118>>, 3.750000))
					OR NOT bFirstThrow)

						STRING paper_anims_f, paper_anims_m, paper_anims_f_chair, paper_anims_m_chair, paper_anims_ball
						SWITCH iPaperThrow
							CASE 0
								paper_anims_f 			= "paper_throw_01_f"
								paper_anims_m 			= "paper_throw_01_m"
								paper_anims_f_chair 	= "paper_throw_01_f_chair"
								paper_anims_m_chair 	= "paper_throw_01_m_chair"
								paper_anims_ball		= "paper_throw_01_paper_ball"
							BREAK
							CASE 1
								paper_anims_f 			= "paper_throw_03_f"
								paper_anims_m 			= "paper_throw_03_m"
								paper_anims_f_chair 	= "paper_throw_03_f_chair"
								paper_anims_m_chair 	= "paper_throw_03_m_chair"
								paper_anims_ball		= "paper_throw_03_paper_ball"
							BREAK
							CASE 2
								paper_anims_f 			= "paper_throw_06_f"
								paper_anims_m 			= "paper_throw_06_m"
								paper_anims_f_chair 	= "paper_throw_06_f_chair"
								paper_anims_m_chair 	= "paper_throw_06_m_chair"
								paper_anims_ball		= "paper_throw_06_paper_ball"
							BREAK
						ENDSWITCH
						
						// create paper ball
						Create_Prop(objects[mof_paper_ball], PROP_PAPER_BALL, << -1052.7799, -245.6611, 43.0211 >>)
						SET_ENTITY_COLLISION(objects[mof_paper_ball].id, FALSE)
						//ATTACH_ENTITY_TO_ENTITY(objects[mof_paper_ball].id, peds[mpf_office_paper_m], get_ped_bone_index(peds[mpf_office_paper_m], BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
						
						syncedIDs[ssf_ambient_paper_throw] = CREATE_SYNCHRONIZED_SCENE(v_bin_coord, v_bin_rot)
						TASK_SYNCHRONIZED_SCENE (peds[mpf_office_paper_f], syncedIDs[ssf_ambient_paper_throw], anim_dict_paper_throw, paper_anims_f, SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)	
						TASK_SYNCHRONIZED_SCENE (peds[mpf_office_paper_m], syncedIDs[ssf_ambient_paper_throw], anim_dict_paper_throw, paper_anims_m, SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_thrower_f_chair], syncedIDs[ssf_ambient_paper_throw], paper_anims_f_chair, anim_dict_paper_throw, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_thrower_m_chair], syncedIDs[ssf_ambient_paper_throw], paper_anims_m_chair, anim_dict_paper_throw, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_paper_ball], syncedIDs[ssf_ambient_paper_throw], paper_anims_ball, anim_dict_paper_throw, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_paper_m])
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_paper_f])
						
						bFirstThrow = FALSE
						bJustThrown = TRUE
						
						iPaperThrow++
						IF iPaperThrow > 2
							iPaperThrow = 0
						ENDIF
						mEvents[mef_ambient_paper_throw].iStage++
						
					// none of the above conditions were met, if anim has finished restart the loop section
					ELIF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_ambient_paper_throw])
					OR GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_paper_throw]) = 1.0
					
						syncedIDs[ssf_ambient_paper_throw] = CREATE_SYNCHRONIZED_SCENE(v_bin_coord, v_bin_rot)				
						TASK_SYNCHRONIZED_SCENE (peds[mpf_office_paper_f], syncedIDs[ssf_ambient_paper_throw], anim_dict_paper_throw, "paper_throw_base_f", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT )	
						TASK_SYNCHRONIZED_SCENE (peds[mpf_office_paper_m], syncedIDs[ssf_ambient_paper_throw], anim_dict_paper_throw, "paper_throw_base_m", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT )
						SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_thrower_f_chair], syncedIDs[ssf_ambient_paper_throw], "paper_throw_base_f_chair", anim_dict_paper_throw, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_office_thrower_m_chair], syncedIDs[ssf_ambient_paper_throw], "paper_throw_base_m_chair", anim_dict_paper_throw, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_paper_m])
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_paper_f])
						bJustThrown = FALSE
						
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1053.111084,-242.029358,43.021278>>) < 24.3838 // 4.938 squared
							IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
							AND NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_paper_f])
								SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
									CASE 0 
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_paper_f], "LES1A_BEAA", "LIFEINVADERF3", SPEECH_PARAMS_FORCE)
										CDEBUG1LN(DEBUG_MIKE, "[Lester1A] PaperThrow F - AMB DIA A")
									BREAK
									CASE 1
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_paper_f], "LES1A_BFAA", "LIFEINVADERF3", SPEECH_PARAMS_FORCE)
										CDEBUG1LN(DEBUG_MIKE, "[Lester1A] PaperThrow F - AMB DIA B")
									BREAK
								ENDSWITCH
								
							ELIF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_paper_m])
								SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
									CASE 0 
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_paper_m], "LES1A_AYAA", "LIFEINVADERM3", SPEECH_PARAMS_FORCE)
										CDEBUG1LN(DEBUG_MIKE, "[Lester1A] PaperThrow M - AMB DIA A")
									BREAK
									CASE 1
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_paper_m], "LES1A_AZAA", "LIFEINVADERM3", SPEECH_PARAMS_FORCE)
										CDEBUG1LN(DEBUG_MIKE, "[Lester1A] PaperThrow M - AMB DIA B")
									BREAK
								ENDSWITCH
								
							ENDIF
						ENDIF
			
					ENDIF
				ENDIF
			BREAK
			CASE 3
				// Check if paper ball has hit the bin locate to detach it from the animation
				IF IS_ENTITY_IN_ANGLED_AREA(objects[mof_paper_ball].id, <<-1054.525757,-240.298752,43.021118>>, <<-1054.168579,-240.197128,43.121117>>, 0.500000)
				OR IS_ENTITY_IN_ANGLED_AREA(objects[mof_paper_ball].id, <<-1053.177979,-241.098190,43.021118>>, <<-1053.063232,-241.393219,43.121117>>, 0.500000)
					IF IS_ENTITY_ATTACHED(objects[mof_paper_ball].id)
						DETACH_ENTITY(objects[mof_paper_ball].id)
					ELSE
						SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_paper_ball], NORMAL_BLEND_OUT)
					ENDIF
					FREEZE_ENTITY_POSITION(objects[mof_paper_ball].id,TRUE)
					SET_OBJECT_AS_NO_LONGER_NEEDED(objects[mof_paper_ball].id)
					mEvents[mef_ambient_paper_throw].iStage = 2
				ENDIF
			BREAK
		ENDSWITCH
	
	ENDIF
ENDPROC

PROC event_ambient_boardroom()
	// track and store scene progress
	float fSceneProg_main
	float fSceneProg_additional
	BOOL bCreatePeds
	IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_ambient_boardroom_main])
		fSceneProg_main 	= GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_boardroom_main])
	ELSE
		fSceneProg_main		= 1.0
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_ambient_boardroom_additional])
		fSceneProg_additional 	= GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_ambient_boardroom_additional])
	ELSE
		fSceneProg_additional 	= 1.0
	ENDIF
	
	// Creation
	IF mEvents[mef_ambient_boardroom].iStage = 1
		IF HAS_MODEL_LOADED(IG_JAY_NORRIS)
		AND HAS_MODEL_LOADED(mod_hipster_m)
		AND HAS_MODEL_LOADED(mod_hipster_f)
		AND (NOT IS_CUTSCENE_PLAYING() OR GET_CUTSCENE_TIME() > 3700)
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
				CASE msf_st3_go_to_computer
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1082.850098,-244.797516,42.795040>>, <<-1073.899292,-248.911301,45.521118>>, 1.000000)
					AND HAS_ANIM_DICT_LOADED(anim_dict_boardroom_main)
					AND HAS_ANIM_DICT_LOADED(anim_dict_boardroom_intro)
						bCreatePeds = TRUE
						mEvents[mef_ambient_boardroom].iStage++
					ENDIF
				BREAK
				CASE msf_st4_mini_game	FALLTHRU
				CASE msf_st5_plant_bomb
					IF HAS_ANIM_DICT_LOADED(anim_dict_boardroom_main)
					AND HAS_ANIM_DICT_LOADED(anim_dict_boardroom_intro)
						bCreatePeds = TRUE
						mEvents[mef_ambient_boardroom].iStage++
					ENDIF
				BREAK
				CASE msf_st6_leave_building
					IF HAS_ANIM_DICT_LOADED(anim_dict_boardroom_main)
					AND HAS_ANIM_DICT_LOADED(anim_dict_boardroom_intro)
						bCreatePeds = TRUE
						mEvents[mef_ambient_boardroom].iStage++
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bCreatePeds
				CPRINTLN(DEBUG_MIKE, "[***LS1***] event_ambient_boardroom() CREATING PEDS")
				
				IF DOES_ENTITY_EXIST(peds[mpf_boardroom_norris])	
					CLEAR_PED_TASKS(peds[mpf_boardroom_norris])		
				ELSE
					peds[mpf_boardroom_norris] = CREATE_PED(PEDTYPE_MISSION, IG_JAY_NORRIS, <<-1045.5, -229.0, 43.41>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_boardroom_norris], "JayNorris")
					Set_Life_Invader_Office_Ped_Variation(mpf_boardroom_norris)
				ENDIF
//				IF DOES_ENTITY_EXIST(peds[mpf_boardroom_f_a])		
//					CLEAR_PED_TASKS(peds[mpf_boardroom_f_a])
//				ELSE
//					peds[mpf_boardroom_f_a] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_f, <<-1045.07, -229.78, 43.41>>, 0)
//					SET_PED_NAME_DEBUG(peds[mpf_boardroom_f_a], "BoardFemaleA")
//					Set_Life_Invader_Office_Ped_Variation(mpf_boardroom_f_a)
//				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_boardroom_f_b])		
					CLEAR_PED_TASKS(peds[mpf_boardroom_f_b])		
				ELSE
					peds[mpf_boardroom_f_b] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_f, <<-1045.07, -229.78, 43.41>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_boardroom_f_b], "BoardFemaleB")
					Set_Life_Invader_Office_Ped_Variation(mpf_boardroom_f_b)
				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_boardroom_f_c])		
					CLEAR_PED_TASKS(peds[mpf_boardroom_f_c])		
				ELSE
					peds[mpf_boardroom_f_c] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_f, <<-1045.07, -229.78, 43.41>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_boardroom_f_c], "BoardFemaleC")
					Set_Life_Invader_Office_Ped_Variation(mpf_boardroom_f_c)
				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_boardroom_m_a])		
					CLEAR_PED_TASKS(peds[mpf_boardroom_m_a])		
				ELSE
					peds[mpf_boardroom_m_a] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, <<-1045.07, -229.78, 43.41>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_boardroom_m_a], "BoardMaleA")
					Set_Life_Invader_Office_Ped_Variation(mpf_boardroom_m_a)
				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_boardroom_m_b])		
					CLEAR_PED_TASKS(peds[mpf_boardroom_m_b])		
				ELSE
					peds[mpf_boardroom_m_b] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, <<-1045.07, -229.78, 43.41>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_boardroom_m_b], "BoardMaleB")
					Set_Life_Invader_Office_Ped_Variation(mpf_boardroom_m_b)
				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_boardroom_m_e])		
					CLEAR_PED_TASKS(peds[mpf_boardroom_m_e])		
				ELSE
					peds[mpf_boardroom_m_e] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, <<-1045.07, -229.78, 43.41>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_boardroom_m_e], "BoardMaleE")
					Set_Life_Invader_Office_Ped_Variation(mpf_boardroom_m_e)
				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_boardroom_m_f])		
					CLEAR_PED_TASKS(peds[mpf_boardroom_m_f])		
				ELSE
					peds[mpf_boardroom_m_f] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_m, <<-1045.07, -229.78, 43.41>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_boardroom_m_f], "BoardMaleF")
					Set_Life_Invader_Office_Ped_Variation(mpf_boardroom_m_f)
				ENDIF

				IF NOT DOES_ENTITY_EXIST(objects[mof_jay_norris_chair].id)	
					Create_Prop(objects[mof_jay_norris_chair], Prop_Off_Chair_01, <<-1045.07, -229.78, 43.41>>)
				ENDIF
				
				IF DOES_ENTITY_EXIST(objects[mof_jay_norris_chair].id)	
					IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objects[mof_jay_norris_chair].id, peds[mpf_boardroom_norris])
						IF IS_ENTITY_ATTACHED(objects[mof_jay_norris_chair].id)
							DETACH_ENTITY(objects[mof_jay_norris_chair].id, false, TRUE)
						ENDIF
						ATTACH_ENTITY_TO_ENTITY(objects[mof_jay_norris_chair].id, peds[mpf_boardroom_norris],get_ped_bone_index(peds[mpf_boardroom_norris], BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF mEvents[mef_ambient_boardroom].iStage > 1
	
		TEXT_LABEL_31 strBoardRoomConvo
		TEXT_LABEL_31 strBoardRoomAnimDictionary[5]
		TEXT_LABEL_31 strBoardRoomAnim[5]
	
		SWITCH mEvents[mef_ambient_boardroom].iStage
			CASE 2
				IF Lineup_Boardroom_Chairs()
					bAdditionalIsLooping = FALSE
					mEvents[mef_ambient_boardroom].iStage++
				ENDIF
			BREAK
			CASE 3
				// intro loop
				IF IS_ENTITY_READY(peds[mpf_boardroom_norris])
				//AND IS_ENTITY_READY(peds[mpf_boardroom_f_a])
				AND IS_ENTITY_READY(peds[mpf_boardroom_f_b])
				AND IS_ENTITY_READY(peds[mpf_boardroom_f_c])
				AND IS_ENTITY_READY(peds[mpf_boardroom_m_a])
				AND IS_ENTITY_READY(peds[mpf_boardroom_m_b])
				AND IS_ENTITY_READY(peds[mpf_boardroom_m_e])
				AND IS_ENTITY_READY(peds[mpf_boardroom_m_f])
				
					syncedIDs[ssf_ambient_boardroom_main] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
					TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_norris], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_intro, "boardroom_intro_jaynorris", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_a], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_intro, "boardroom_intro_m_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_b], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_intro, "boardroom_intro_m_b", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_f], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_intro, "boardroom_intro_m_f", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_boardroom_main], TRUE)

					syncedIDs[ssf_ambient_boardroom_additional] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
//					TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_a], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_intro, "boardroom_intro_f_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_b], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_intro, "boardroom_intro_f_b", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_c], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_intro, "boardroom_intro_f_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_e], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_intro, "boardroom_intro_m_e", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_boardroom_additional], TRUE)

					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_norris])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_a])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_b])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_f])
					
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_f_a])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_f_b])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_f_c])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_e])
					
					Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_main)
					mEvents[mef_ambient_boardroom].iStage++
				ENDIF
			BREAK
			CASE 4 FALLTHRU 
			CASE 6 FALLTHRU
			CASE 8
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1047.747314,-238.057571,43.021240>>, <<-1050.871216,-239.662491,45.921116>>, 10.000000)
					
					IF NOT IS_SAFE_TO_START_CONVERSATION()
						KILL_FACE_TO_FACE_CONVERSATION()
					ELSE
						// main anim phase
						IF HAS_ANIM_DICT_LOADED(anim_dict_boardroom_main)
						AND IS_ENTITY_READY(peds[mpf_boardroom_norris])
//						AND IS_ENTITY_READY(peds[mpf_boardroom_f_a])
						AND IS_ENTITY_READY(peds[mpf_boardroom_f_b])
						AND IS_ENTITY_READY(peds[mpf_boardroom_f_c])
						AND IS_ENTITY_READY(peds[mpf_boardroom_m_a])
						AND IS_ENTITY_READY(peds[mpf_boardroom_m_b])
						AND IS_ENTITY_READY(peds[mpf_boardroom_m_e])
						AND IS_ENTITY_READY(peds[mpf_boardroom_m_f])
						
							SWITCH mEvents[mef_ambient_boardroom].iStage
								CASE 4
									ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_boardroom_norris], "JAYNORRIS")
									ADD_PED_FOR_DIALOGUE(sConvo, 6, peds[mpf_boardroom_m_a], "LIBoard3")
									ADD_PED_FOR_DIALOGUE(sConvo, 7, peds[mpf_boardroom_m_f], "LIBoard1")
								
									strBoardRoomConvo 					= "LES_1A_IG_5a"
									strBoardRoomAnimDictionary[0]		= anim_dict_boardroom_main
									strBoardRoomAnim[0]					= "LES_1A_IG_5a_jayNorris"
									strBoardRoomAnimDictionary[1]		= anim_dict_boardroom_main
									strBoardRoomAnim[1]					= "LES_1A_IG_5a_jNor_chair"
									strBoardRoomAnimDictionary[2]		= anim_dict_boardroom_main
									strBoardRoomAnim[2]					= "LES_1A_IG_5a_m_a"
									strBoardRoomAnimDictionary[3]		= anim_dict_boardroom_main
									strBoardRoomAnim[3]					= "LES_1A_IG_5a_m_b"
									strBoardRoomAnimDictionary[4]		= anim_dict_boardroom_main
									strBoardRoomAnim[4]					= "LES_1A_IG_5a_m_f"
								BREAK
								CASE 6
									ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_boardroom_norris], "JAYNORRIS")
									ADD_PED_FOR_DIALOGUE(sConvo, 8, peds[mpf_boardroom_m_a], "LIBoard2")
								
									strBoardRoomConvo = "LES_1A_IG_5b"
									strBoardRoomAnimDictionary[0]		= anim_dict_boardroom_main
									strBoardRoomAnim[0]					= "LES_1A_IG_5b_jayNorris"
									strBoardRoomAnimDictionary[1]		= anim_dict_boardroom_main
									strBoardRoomAnim[1]					= "LES_1A_IG_5b_jNor_chair"
									strBoardRoomAnimDictionary[2]		= anim_dict_boardroom_main
									strBoardRoomAnim[2]					= "LES_1A_IG_5b_m_a"
									strBoardRoomAnimDictionary[3]		= anim_dict_boardroom_main
									strBoardRoomAnim[3]					= "LES_1A_IG_5b_m_b"
									strBoardRoomAnimDictionary[4]		= anim_dict_boardroom_main
									strBoardRoomAnim[4]					= "LES_1A_IG_5b_m_f"
								BREAK
								CASE 8
									ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_boardroom_norris], "JAYNORRIS")
									ADD_PED_FOR_DIALOGUE(sConvo, 6, peds[mpf_boardroom_m_a], "LIBoard3")
								
									strBoardRoomConvo = "LES_1A_IG_5c"
									strBoardRoomAnimDictionary[0]		= anim_dict_boardroom_main
									strBoardRoomAnim[0]					= "LES_1A_IG_5c_jayNorris"
									strBoardRoomAnimDictionary[1]		= anim_dict_boardroom_main
									strBoardRoomAnim[1]					= "LES_1A_IG_5c_jNor_chair"
									strBoardRoomAnimDictionary[2]		= anim_dict_boardroom_main
									strBoardRoomAnim[2]					= "LES_1A_IG_5c_m_a"
									strBoardRoomAnimDictionary[3]		= anim_dict_boardroom_main
									strBoardRoomAnim[3]					= "LES_1A_IG_5c_m_b"
									strBoardRoomAnimDictionary[4]		= anim_dict_boardroom_main
									strBoardRoomAnim[4]					= "LES_1A_IG_5c_m_f"
								BREAK
							ENDSWITCH

							IF CREATE_CONVERSATION(sConvo, str_dialogue, strBoardRoomConvo, CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)

								syncedIDs[ssf_ambient_boardroom_main] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
								TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_norris], syncedIDs[ssf_ambient_boardroom_main], 	strBoardRoomAnimDictionary[0], strBoardRoomAnim[0], 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
								IF IS_ENTITY_ATTACHED(objects[mof_jay_norris_chair].id)
									DETACH_ENTITY(objects[mof_jay_norris_chair].id)
								ENDIF
								SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_jay_norris_chair], syncedIDs[ssf_ambient_boardroom_main], strBoardRoomAnim[1], strBoardRoomAnimDictionary[1], NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
								TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_a], syncedIDs[ssf_ambient_boardroom_main],		strBoardRoomAnimDictionary[2], strBoardRoomAnim[2],		NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
								TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_b], syncedIDs[ssf_ambient_boardroom_main],		strBoardRoomAnimDictionary[3], strBoardRoomAnim[3], 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
								TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_f], syncedIDs[ssf_ambient_boardroom_main],		strBoardRoomAnimDictionary[4], strBoardRoomAnim[4], 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
								
//								FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_norris])
//								FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_a])
//								FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_b])
//								FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_f])
								
								IF mEvents[mef_ambient_boardroom].iStage = 4
									syncedIDs[ssf_ambient_boardroom_additional] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
									TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_e], syncedIDs[ssf_ambient_boardroom_additional],anim_dict_boardroom_main, "boardroom_react_m_e", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
//									TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_a], syncedIDs[ssf_ambient_boardroom_additional],anim_dict_boardroom_main, "boardroom_react_f_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
									TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_b], syncedIDs[ssf_ambient_boardroom_additional],anim_dict_boardroom_main, "boardroom_react_f_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
									TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_c], syncedIDs[ssf_ambient_boardroom_additional],anim_dict_boardroom_main, "boardroom_react_f_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
//									FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_f_a])
//									FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_f_b])
//									FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_f_c])
//									FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_e])
								ENDIF		
														
								Unload_Asset_Anim_Dict(sAssetData, anim_dict_boardroom_intro)
								
								mEvents[mef_ambient_boardroom].iStage++
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			BREAK
			CASE 5 FALLTHRU
			CASE 7 
				IF fSceneProg_main = 1.0
					IF HAS_ANIM_DICT_LOADED(anim_dict_boardroom_main)
					AND IS_ENTITY_READY(peds[mpf_boardroom_norris])
//					AND IS_ENTITY_READY(peds[mpf_boardroom_f_a])
					AND IS_ENTITY_READY(peds[mpf_boardroom_f_b])
					AND IS_ENTITY_READY(peds[mpf_boardroom_f_c])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_a])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_b])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_e])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_f])
					
						SWITCH mEvents[mef_ambient_boardroom].iStage
							CASE 5
								strBoardRoomAnimDictionary[0]		= anim_dict_boardroom_main
								strBoardRoomAnim[0]					= "idle_01_jayNorris"
								strBoardRoomAnimDictionary[1]		= anim_dict_boardroom_main
								strBoardRoomAnim[1]					= "idle_01_jNor_chair"
								strBoardRoomAnimDictionary[2]		= anim_dict_boardroom_main
								strBoardRoomAnim[2]					= "idle_01_m_a"
								strBoardRoomAnimDictionary[3]		= anim_dict_boardroom_main
								strBoardRoomAnim[3]					= "idle_01_m_b"
								strBoardRoomAnimDictionary[4]		= anim_dict_boardroom_main
								strBoardRoomAnim[4]					= "idle_01_m_f"
							BREAK
							CASE 7
								strBoardRoomAnimDictionary[0]		= anim_dict_boardroom_main
								strBoardRoomAnim[0]					= "idle_02_jayNorris"
								strBoardRoomAnimDictionary[1]		= anim_dict_boardroom_main
								strBoardRoomAnim[1]					= "idle_02_jNor_chair"
								strBoardRoomAnimDictionary[2]		= anim_dict_boardroom_main
								strBoardRoomAnim[2]					= "idle_01_m_a"
								strBoardRoomAnimDictionary[3]		= anim_dict_boardroom_main
								strBoardRoomAnim[3]					= "idle_01_m_b"
								strBoardRoomAnimDictionary[4]		= anim_dict_boardroom_main
								strBoardRoomAnim[4]					= "idle_01_m_f"
							BREAK
						ENDSWITCH
					
						syncedIDs[ssf_ambient_boardroom_main] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_norris], syncedIDs[ssf_ambient_boardroom_main], 	strBoardRoomAnimDictionary[0], strBoardRoomAnim[0], 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						IF IS_ENTITY_ATTACHED(objects[mof_jay_norris_chair].id)
							DETACH_ENTITY(objects[mof_jay_norris_chair].id)
						ENDIF
						SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_jay_norris_chair], syncedIDs[ssf_ambient_boardroom_main], strBoardRoomAnim[1], strBoardRoomAnimDictionary[1], NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_a], syncedIDs[ssf_ambient_boardroom_main],		strBoardRoomAnimDictionary[2], strBoardRoomAnim[2],		NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_b], syncedIDs[ssf_ambient_boardroom_main],		strBoardRoomAnimDictionary[3], strBoardRoomAnim[3], 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_f], syncedIDs[ssf_ambient_boardroom_main],		strBoardRoomAnimDictionary[4], strBoardRoomAnim[4], 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_boardroom_main], TRUE)
						
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_norris])
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_a])
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_b])
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_boardroom_m_f])
						
						Add_Event_Delay(mef_ambient_boardroom, 3500)
						mEvents[mef_ambient_boardroom].iStage++
					ENDIF
				ENDIF
			BREAK
			CASE 9
				// Into exit
				IF fSceneProg_main = 1.0
					IF IS_ENTITY_READY(peds[mpf_boardroom_norris])
//					AND IS_ENTITY_READY(peds[mpf_boardroom_f_a])
					AND IS_ENTITY_READY(peds[mpf_boardroom_f_b])
					AND IS_ENTITY_READY(peds[mpf_boardroom_f_c])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_a])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_b])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_e])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_f])

						syncedIDs[ssf_ambient_boardroom_main] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_norris], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_exit, "boardroom_into_exit_jaynorris", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_a], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_exit, "boardroom_into_exit_m_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_b], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_exit, "boardroom_into_exit_m_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_f], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_exit, "boardroom_into_exit_m_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						
						syncedIDs[ssf_ambient_boardroom_additional] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
//						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_a], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_exit, "boardroom_into_exit_f_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_b], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_exit, "boardroom_into_exit_f_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_c], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_exit, "boardroom_into_exit_f_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_e], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_exit, "boardroom_into_exit_m_e", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						
						Unload_Asset_Anim_Dict(sAssetData, anim_dict_boardroom_main)
						
						mEvents[mef_ambient_boardroom].iStage++
					ENDIF
				// Prestream the outro anims
				ELIF mEvents[mef_ambient_boardroom].bAnimDictPrestreamed = FALSE
				AND fSceneProg_main > 0.8
					Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_exit)
					mEvents[mef_ambient_boardroom].bAnimDictPrestreamed = TRUE
				ENDIF
			BREAK
			CASE 10
				IF fSceneProg_main = 1.0
				AND HAS_ANIM_DICT_LOADED(anim_dict_boardroom_exit)
					IF IS_ENTITY_READY(peds[mpf_boardroom_norris])
//					AND IS_ENTITY_READY(peds[mpf_boardroom_f_a])
					AND IS_ENTITY_READY(peds[mpf_boardroom_f_b])
					AND IS_ENTITY_READY(peds[mpf_boardroom_f_c])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_a])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_b])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_e])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_f])

						syncedIDs[ssf_ambient_boardroom_main] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_norris], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_exit, "boardroom_exitloop_jaynorris", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_a], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_exit, "boardroom_exitloop_m_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_b], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_exit, "boardroom_exitloop_m_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_f], syncedIDs[ssf_ambient_boardroom_main], anim_dict_boardroom_exit, "boardroom_exitloop_m_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_boardroom_main], TRUE)
						
						syncedIDs[ssf_ambient_boardroom_additional] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
//						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_a], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_exit, "boardroom_exitloop_f_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_b], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_exit, "boardroom_exitloop_f_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_c], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_exit, "boardroom_exitloop_f_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_e], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_exit, "boardroom_exitloop_m_e", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_boardroom_additional], TRUE)
						
						mEvents[mef_ambient_boardroom].iStage++
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF mEvents[mef_ambient_boardroom].iStage > 4
			// shortened non essential anims looped bit
			IF NOT bAdditionalIsLooping
				IF fSceneProg_additional = 1.0
					
//					IF IS_ENTITY_READY(peds[mpf_boardroom_f_a])
					IF IS_ENTITY_READY(peds[mpf_boardroom_f_b])
					AND IS_ENTITY_READY(peds[mpf_boardroom_f_c])
					AND IS_ENTITY_READY(peds[mpf_boardroom_m_e])
					
//						CLEAR_PED_TASKS(peds[mpf_boardroom_f_a])
						CLEAR_PED_TASKS(peds[mpf_boardroom_f_b])
						CLEAR_PED_TASKS(peds[mpf_boardroom_f_c])
						CLEAR_PED_TASKS(peds[mpf_boardroom_m_e])
						
						syncedIDs[ssf_ambient_boardroom_additional] = CREATE_SYNCHRONIZED_SCENE(vBoardRoomSceneRootCoord, vBoardRoomSceneRootRot)
//						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_a], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_main, "boardroom_listen_loop_f_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_b], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_main, "boardroom_listen_loop_f_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_f_c], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_main, "boardroom_listen_loop_f_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (peds[mpf_boardroom_m_e], syncedIDs[ssf_ambient_boardroom_additional], anim_dict_boardroom_main, "boardroom_listen_loop_m_e", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
						SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_ambient_boardroom_additional], TRUE)
						
						bAdditionalIsLooping = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC event_engineer_air_guitar

	SWITCH mEvents[mef_engineer_air_guitar].iStage
		CASE 1
			IF NOT IS_PED_INJURED(peds[mpf_smoker])
			AND NOT IS_PED_INJURED(peds[mpf_office_d])
				SWITCH int_to_enum(mission_stage_flag, mission_stage)
					CASE msf_st5_plant_bomb
						IF HAS_ANIM_DICT_LOADED(anim_dict_engineer_airguitar)
						AND DOES_ENTITY_EXIST(objects[mof_office_d_chair].id)
						
							SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_d_chair], INSTANT_BLEND_OUT)

							FREEZE_ENTITY_POSITION(objects[mof_office_d_chair].id, FALSE)
							SET_ENTITY_COORDS(objects[mof_office_d_chair].id, <<-1063.9155, -246.8907, 43.0306>>)
							SET_ENTITY_HEADING(objects[mof_office_d_chair].id, 121.360)
							FREEZE_ENTITY_POSITION(objects[mof_office_d_chair].id, TRUE)
							CPRINTLN(DEBUG_MIKE, "[CHAIR D] Moved inside of event_engineer_air_guitar()")
							i_piano_airguitar_loop_count = 0
							b_IsPlayingConvoIG9 = FALSE
							mEvents[mef_engineer_air_guitar].iStage 		= 2
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_READY(peds[mpf_smoker])
			AND IS_ENTITY_READY(peds[mpf_office_d])
			
				// Michael gets near and mocks the guys
				IF NOT b_IsPlayingConvoIG9
					IF IS_SAFE_TO_START_CONVERSATION()
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1065.365601,-241.407578,43.028111>>, <<-1063.174927,-245.603302,45.292542>>, 3.500000)
						IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_WTF", CONV_PRIORITY_HIGH)
							b_IsPlayingConvoIG9 = TRUE
						ENDIF	
					ENDIF
				ENDIF
			
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_engineer_air_guitar])
				OR GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_engineer_air_guitar]) = 1.0

					IF b_IsPlayingConvoIG9
					OR i_piano_airguitar_loop_count > 10
						
						syncedIDs[ssf_engineer_air_guitar] = CREATE_SYNCHRONIZED_SCENE(<<-1063.542, -245.794, 43.032 >>, <<0.0,0.0,27.800>>)
						TASK_SYNCHRONIZED_SCENE(peds[mpf_smoker], syncedIDs[ssf_engineer_air_guitar], anim_dict_engineer_airguitar, "idle_to_sit_pedb", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
						TASK_SYNCHRONIZED_SCENE(peds[mpf_office_d], syncedIDs[ssf_engineer_air_guitar], anim_dict_engineer_airguitar, "idle_to_sit_peda", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_smoker])
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
						
						IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_office_d])
						ENDIF
						
						IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_smoker])
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_smoker])
						ENDIF
					
						mEvents[mef_engineer_air_guitar].iStage++
					
					// Carry on another loop
					ELSE
						
						syncedIDs[ssf_engineer_air_guitar] = CREATE_SYNCHRONIZED_SCENE(<<-1063.542, -245.794, 43.032 >>, <<0.0,0.0,27.800>>)
						TASK_SYNCHRONIZED_SCENE(peds[mpf_smoker], syncedIDs[ssf_engineer_air_guitar], anim_dict_engineer_airguitar, "idle_standing_pedb", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
						TASK_SYNCHRONIZED_SCENE(peds[mpf_office_d], syncedIDs[ssf_engineer_air_guitar], anim_dict_engineer_airguitar, "idle_standing_peda", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_smoker])
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
						
						i_piano_airguitar_loop_count++

					ENDIF
				ELSE
					
					IF NOT b_IsPlayingConvoIG9
						// making air guitar noises until the loop has finished
						IF NOT IS_PED_INJURED(peds[mpf_smoker])
							IF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_smoker])
								CPRINTLN(DEBUG_MIKE, "PLAY_PED_AMBIENT_SPEECH: LES_1A_AIRG")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_smoker], "GUITAR", "LIEngineer", SPEECH_PARAMS_FORCE)
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(peds[mpf_office_d])
							IF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
								CPRINTLN(DEBUG_MIKE, "PLAY_PED_AMBIENT_SPEECH: Air Piano")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_d], "PIANO", "AIRPIANIST", SPEECH_PARAMS_FORCE)
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			
			ENDIF
		BREAK
		CASE 3	
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedIDs[ssf_engineer_air_guitar])
			OR GET_SYNCHRONIZED_SCENE_PHASE(syncedIDs[ssf_engineer_air_guitar]) = 1.0
				
				IF IS_ENTITY_READY(peds[mpf_office_d])
				AND IS_ENTITY_READY(peds[mpf_smoker])
				
					syncedIDs[ssf_engineer_air_guitar] = CREATE_SYNCHRONIZED_SCENE(<<-1063.542, -245.794, 43.032 >>, <<0.0,0.0,27.800>>)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_smoker], syncedIDs[ssf_engineer_air_guitar], anim_dict_engineer_airguitar, "idle_sitting_pedb", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_office_d], syncedIDs[ssf_engineer_air_guitar], anim_dict_engineer_airguitar, "idle_sitting_peda", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
					
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_engineer_air_guitar], TRUE)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_smoker])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_office_d])
					
					mEvents[mef_engineer_air_guitar].iStage++
				ENDIF
			ENDIF
		FALLTHRU
		CASE 4
			IF b_IsPlayingConvoIG9
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_EXPLAIN", CONV_PRIORITY_HIGH)
					
						IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_office_d])
						ENDIF
						
						IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_smoker])
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_smoker])
						ENDIF
					
						b_IsPlayingConvoIG9 = FALSE
					ENDIF
				ENDIF
			ELSE
				IF mEvents[mef_engineer_air_guitar].iStage = 4
					mEvents[mef_engineer_air_guitar].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				Add_Event_Delay(mef_engineer_air_guitar, 5000)
				mEvents[mef_engineer_air_guitar].iStage++
			ENDIF
		BREAK
		CASE 6
			IF IS_SAFE_TO_START_CONVERSATION()
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1063.45, -245.74, 43.14>>) <= 2.750
					IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_RETURN", CONV_PRIORITY_HIGH)
						mEvents[mef_engineer_air_guitar].iStage--
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC event_ambient_receptionist()

	// While not at the stage where you can go to life invader stream the receptionist in/out and lock the doors
	IF mission_stage < ENUM_TO_INT(msf_st3_go_to_computer)
	
		IF NOT DOES_ENTITY_EXIST(peds[mpf_receptionist_f])
	
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1088.11, -262.09, 36.79>>) < 30.0
				Load_Asset_Model(sAssetData, mod_hipster_f)
				
				IF HAS_MODEL_LOADED(mod_hipster_f)
					CLEAR_AREA_OF_PEDS(<< -1084.3562, -246.5053, 36.7634 >>,5.0)
					
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_LOCKED)
					
					IF DOES_SCENARIO_EXIST_IN_AREA(<<-1083.76, -246.05, 37.41>>, 2.0, FALSE)
					AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1083.87, -246.33, 36.76>>, 0.5, PROP_OFF_CHAIR_01)
					
						IF DOES_ENTITY_EXIST(objects[mof_receptionist_chair].id)
							// Grab and freeze the chair
							objects[mof_receptionist_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1083.87, -246.33, 36.76>>, 0.5, PROP_OFF_CHAIR_01)
							objects[mof_receptionist_chair].bDoNotReset = TRUE
							FREEZE_ENTITY_POSITION(objects[mof_receptionist_chair].id, TRUE)
						ENDIF
					
						// Create receptionist, put in the using computer scenario
						peds[mpf_receptionist_f] = CREATE_PED(PEDTYPE_MISSION,mod_hipster_f, << -1085.3929, -248.0326, 36.7632 >>, 187.3241)
						SET_ENTITY_COLLISION(peds[mpf_receptionist_f], FALSE)
						TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(peds[mpf_receptionist_f], <<-1083.76, -246.05, 37.41>>, 2.0)
						FREEZE_ENTITY_POSITION(peds[mpf_receptionist_f], TRUE)
						SET_PED_KEEP_TASK(peds[mpf_receptionist_f], TRUE)
						
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1083.87, -246.33, 36.76>>) > 35.0
				DELETE_PED(peds[mpf_receptionist_f])
				Unload_Asset_Model(sAssetData, mod_hipster_f)
			ENDIF
			
		ENDIF
		
	// once in lifeinvader at the correct mission stage
	ELSE

		SWITCH mEvents[mef_ambient_receptionist].iStage
			CASE 1
				bLookingAtReceptionist = FALSE
				i_ReceptionistTalkCount = 0
				mEvents[mef_ambient_receptionist].iStage++
			BREAK
			CASE 2
				IF HAS_MODEL_LOADED(mod_hipster_f)
					CLEAR_AREA_OF_PEDS(<< -1084.3562, -246.5053, 36.7634 >>,5.0)
					
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_LOCKED)
					
					IF DOES_SCENARIO_EXIST_IN_AREA(<<-1083.76, -246.05, 37.41>>, 2.0, FALSE)
					AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1083.87, -246.33, 36.76>>, 0.5, PROP_OFF_CHAIR_01)
					
						// Grab and freeze the chair
						IF DOES_ENTITY_EXIST(objects[mof_receptionist_chair].id)
							objects[mof_receptionist_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1083.87, -246.33, 36.76>>, 0.5, PROP_OFF_CHAIR_01)
							objects[mof_receptionist_chair].bDoNotReset = TRUE
							FREEZE_ENTITY_POSITION(objects[mof_receptionist_chair].id, TRUE)
						ENDIF
					
						// Create receptionist, put in the using computer scenario
						peds[mpf_receptionist_f] = CREATE_PED(PEDTYPE_MISSION,mod_hipster_f, << -1085.3929, -248.0326, 36.7632 >>, 187.3241)
						SET_ENTITY_COLLISION(peds[mpf_receptionist_f], FALSE)
						TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(peds[mpf_receptionist_f], <<-1083.76, -246.05, 37.41>>, 2.0)
						FREEZE_ENTITY_POSITION(peds[mpf_receptionist_f], TRUE)
						SET_PED_KEEP_TASK(peds[mpf_receptionist_f], TRUE)
						
						mEvents[mef_ambient_receptionist].iStage++
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF IS_INTERIOR_SCENE()
				
					SWITCH int_to_enum(mission_stage_flag, mission_stage)
						CASE msf_st3_go_to_computer		FALLTHRU
						CASE msf_st4_mini_game			FALLTHRU
						CASE msf_st5_plant_bomb
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1083.435059,-245.203964,36.763268>>) < 34.4569 // 5.875000 squared

								ADD_PED_FOR_DIALOGUE(sConvo, 4, peds[mpf_receptionist_f], "LIReceptionist")
								
								IF NOT IS_SAFE_TO_START_CONVERSATION()
									KILL_FACE_TO_FACE_CONVERSATION()	
								ELSE
									IF i_ReceptionistTalkCount < 3
										IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_REC2", CONV_PRIORITY_HIGH)
											mEvents[mef_ambient_receptionist].iStage = -301
											i_ReceptionistTalkCount++
										ENDIF
									ELIF i_ReceptionistTalkCount < 7
										IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_REC3", CONV_PRIORITY_HIGH)
											mEvents[mef_ambient_receptionist].iStage = -301
											i_ReceptionistTalkCount++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE msf_st6_leave_building
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1084.703857,-253.404266,36.513294>>, <<-1081.726074,-259.221130,39.759724>>, 10.750000)
								ADD_PED_FOR_DIALOGUE(sConvo, 4, peds[mpf_receptionist_f], "LIReceptionist")
								IF IS_SAFE_TO_START_CONVERSATION()
									IF CREATE_CONVERSATION(sConvo, "LS1AAUD", "LS1A_REC", CONV_PRIORITY_VERY_HIGH)
										Add_Event_Delay(mef_ambient_receptionist, 2000)
										mEvents[mef_ambient_receptionist].iStage++
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
			CASE -301
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					mEvents[mef_ambient_receptionist].iStage = 3
					Add_Event_Delay(mef_ambient_receptionist, GET_RANDOM_INT_IN_RANGE(5000, 7500))
				ENDIF
			BREAK
			CASE 4
				// play unlock sound
				PLAY_SOUND_FROM_COORD(-1, "UNLOCK_DOOR", <<-1080.40, -257.84, 37.92>>, "LESTER1A_SOUNDS")

				SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_UNLOCKED)
				
				mEvents[mef_ambient_receptionist].iStage++
			BREAK
			CASE 5
				IF NOT IS_INTERIOR_SCENE()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					mEvents[mef_ambient_receptionist].bComplete = TRUE
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_PED_INJURED(peds[mpf_receptionist_f])
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
				CASE msf_st3_go_to_computer		FALLTHRU
				CASE msf_st4_mini_game			FALLTHRU
				CASE msf_st5_plant_bomb
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1083.435059,-245.203964,36.763268>>) < 34.4569 // 5.875000 squared
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), peds[mpf_receptionist_f], -1)
						TASK_LOOK_AT_ENTITY(peds[mpf_receptionist_f], PLAYER_PED_ID(), -1)
					ELSE
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						TASK_CLEAR_LOOK_AT(peds[mpf_receptionist_f])
					ENDIF
				BREAK
				CASE msf_st6_leave_building
					IF mEvents[mef_ambient_receptionist].iStage > 3
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1088.850708,-254.181381,39.258392>>, <<-1074.995361,-246.906158,36.513264>>, 11.625000)
							IF NOT bLookingAtReceptionist
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), peds[mpf_receptionist_f], -1)
								TASK_LOOK_AT_ENTITY(peds[mpf_receptionist_f], PLAYER_PED_ID(), -1)
								bLookingAtReceptionist = TRUE
							ENDIF
						ELSE
							IF bLookingAtReceptionist
								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
								TASK_CLEAR_LOOK_AT(peds[mpf_receptionist_f])
								bLookingAtReceptionist = FALSE
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC event_ambient_dialogue()
	SWITCH mEvents[mef_ambient_dialogue].iStage
		CASE 1
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1067.389893,-247.876862,43.021278>>, <<-1051.009766,-239.455734,46.958778>>, 10.000000)
			AND GET_GAME_TIMER() - i_ambient_dialogue_timer >= i_ambient_dialogue_delay
			AND NOT IS_CUTSCENE_PLAYING()
			
				// In range of first guy on the end
				IF mission_stage != enum_to_int(msf_st5_plant_bomb)
				AND (NOT Is_Event_Triggered(mef_engineer_air_guitar) OR Is_Event_Complete(mef_engineer_air_guitar))
				AND NOT IS_PED_INJURED(peds[mpf_office_d])
				AND NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_d])
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1063.654053,-246.729340,43.021278>>) < 10.5625 // 3.25 squared
				
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
						CASE 0		
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_d], "LES1A_AUAA", "LIFEINVADERM1", SPEECH_PARAMS_FORCE)
						BREAK
						CASE 1		
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_d], "LES1A_AVAA", "LIFEINVADERM1", SPEECH_PARAMS_FORCE)
						BREAK
					ENDSWITCH
					
					mEvents[mef_ambient_dialogue].iStage++
			
				// Walk pass the guy sitting by the pillar (black guy in the green)
				ELIF NOT IS_PED_INJURED(peds[mpf_office_c])
				AND NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_office_c])
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1058.56, -242.61, 43.02>>) < 10.5625 // 3.25 squared
					
					IF mission_stage >= enum_to_int(msf_st5_plant_bomb)
					AND NOT b_dia_new_guy 
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_c], "LES1A_AXAA", "LIFEINVADERM1", SPEECH_PARAMS_FORCE)
						b_dia_new_guy = TRUE
					ELSE
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_office_c], "LES1A_AWAA", "LIFEINVADERM1", SPEECH_PARAMS_FORCE)
					ENDIF
					
					mEvents[mef_ambient_dialogue].iStage++
					
				ENDIF
			ENDIF
		BREAK
		CASE 2
			i_ambient_dialogue_timer = GET_GAME_TIMER()
			i_ambient_dialogue_delay = GET_RANDOM_INT_IN_RANGE(7000, 12000)
			mEvents[mef_ambient_dialogue].iStage--
		BREAK
	ENDSWITCH
ENDPROC

PROC event_tracey()
	
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), v_home_living_room, 200)
		IF NOT DOES_ENTITY_EXIST(veh_Tracey)	
			CREATE_NPC_VEHICLE(veh_Tracey, CHAR_TRACEY, <<-813.3314, 159.8796, 70.3280>>, 112.3623, TRUE)
		ENDIF
	ELIF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), v_home_living_room, 230)
		IF DOES_ENTITY_EXIST(veh_Tracey)	
			DELETE_VEHICLE(veh_Tracey)
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), v_home_living_room, 100)
		IF NOT bHouseAssetsRequested
			Load_Asset_Interior(sAssetData, interior_living_room)
			Load_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_TRACEY))
			Load_Asset_Model(sAssetData, PROP_CS_REMOTE_01)
			Load_Asset_Model(sAssetData, Prop_Phone_ING)
			Load_Asset_AnimDict(sAssetData, "misslester1b")
			bHouseAssetsRequested = TRUE
		ENDIF
	ELIF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), v_home_living_room, 110)
		IF bHouseAssetsRequested
			Unload_Asset_Interior(sAssetData, interior_living_room)
			Unload_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_TRACEY))
			Unload_Asset_Model(sAssetData, PROP_CS_REMOTE_01)
			Unload_Asset_Model(sAssetData, Prop_Phone_ING)
			Unload_Asset_Anim_Dict(sAssetData, "misslester1b")
			bHouseAssetsRequested = FALSE
		ENDIF
	ENDIF

	IF bHouseAssetsRequested
		// Blip and instruct the player to go to the living room
		IF NOT DOES_ENTITY_EXIST(peds[mpf_tracey])
		AND HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_TRACEY))
		AND HAS_MODEL_LOADED(PROP_CS_REMOTE_01)
		AND HAS_ANIM_DICT_LOADED("misslester1b")
			IF CREATE_NPC_PED_ON_FOOT(peds[mpf_tracey], CHAR_TRACEY, << -803.0255, 172.6011, 71.8348 >>, 129.7437)			
				SET_PED_CAN_BE_TARGETTED(peds[mpf_tracey], FALSE)				
				SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_tracey], REL_FAMILY)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_tracey], TRUE)

				IF NOT DOES_ENTITY_EXIST(objects[mof_tv_remote].id)
					Create_Prop(objects[mof_tv_remote], PROP_CS_REMOTE_01, GET_PED_BONE_COORDS(peds[mpf_tracey], BONETAG_PH_L_HAND, <<0,0,0>>))
					Unload_Asset_Model(sAssetData, PROP_CS_REMOTE_01)
					ATTACH_ENTITY_TO_ENTITY(objects[mof_tv_remote].id, peds[mpf_tracey], GET_PED_BONE_INDEX(peds[mpf_tracey], BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
				ENDIF

				SET_TRACY_VARS()
				syncedIDs[ssf_tracey] = CREATE_SYNCHRONIZED_SCENE(<< -803.510, 171.950, 71.840 >>, << 0.000, 0.000, 120.320 >>)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_tracey], syncedIDs[ssf_tracey], "misslester1b", "watchtv", INSTANT_BLEND_IN, WALK_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_tracey], TRUE)
				SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", FALSE)
				CREATE_MODEL_HIDE(<<-804.44751, 172.79373, 72.34801>>, 0.5, PROP_CS_REMOTE_01, FALSE)
				
				Unload_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_TRACEY))
			ENDIF
		ENDIF
	ELSE
		REMOVE_MODEL_HIDE(<<-804.44751, 172.79373, 72.34801>>, 0.5, PROP_CS_REMOTE_01, FALSE)
		
		IF DOES_ENTITY_EXIST(objects[mof_tv_remote].id)
			DELETE_OBJECT(objects[mof_tv_remote].id)
		ENDIF
		
		IF DOES_ENTITY_EXIST(peds[mpf_tracey])
			DELETE_PED(peds[mpf_tracey])
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Draws event debug information
PROC Display_Event_Debug_Info()
#IF IS_DEBUG_BUILD
	CONST_FLOAT f_left 			0.05 // 0.1
	CONST_FLOAT f_top 			0.05 // 0.1
	CONST_FLOAT f_spacing		0.05
	CONST_FLOAT	f_txt_scale		0.4
	CONST_INT	i_event_offset	3
	
	CONST_FLOAT f_name_offset	0.03
	CONST_FLOAT f_status_offset	0.17
	CONST_FLOAT	f_stage_offset	0.25
	
	INT i_active_count = 0
	BOOL bShowAllEvents
	IF IS_KEYBOARD_KEY_PRESSED(KEY_END)
		bShowAllEvents = TRUE
	ELSE
		bShowAllEvents = FALSE
	ENDIF
	
	INT i
	FOR i = 0 TO enum_to_int(mef_num_events)-1
		IF bShowAllEvents
			i_active_count++
		ELSE
			IF mEvents[i].bComplete
				i_active_count++
			ELIF mEvents[i].bTriggered 
				i_active_count++
			ENDIF
		ENDIF
	ENDFOR
	
	// If none active then there will be one line in there saying nothing is active
	DRAW_RECT_FROM_CORNER(f_left-0.01, f_top-0.01, 0.35, (f_txt_scale * 0.05 * (i_active_count+i_event_offset+1)) + 0.02, 0,0,0, 150)
	
	TEXT_LABEL_23 str_debug_info = "Mission Stage: "
	str_debug_info += (mission_stage)
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, f_top, "STRING", str_debug_info)
	
	str_debug_info = "Mission Substage: "
	str_debug_info += (mission_substage)
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left + 0.12, f_top, "STRING", str_debug_info)
	
	str_debug_info = "Assets: "
	str_debug_info += Get_Num_Assets_Loaded(sAssetData)
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left + 0.27, f_top, "STRING", str_debug_info)
	
	i_active_count = 0
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, 					f_top+((i_event_offset-1)*f_spacing*f_txt_scale), "STRING", "ID")
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_name_offset, 		f_top+((i_event_offset-1)*f_spacing*f_txt_scale), "STRING", "Name")
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_status_offset, 	f_top+((i_event_offset-1)*f_spacing*f_txt_scale), "STRING", "Status")
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_stage_offset, 	f_top+((i_event_offset-1)*f_spacing*f_txt_scale), "STRING", "Stage")
	
	FOR i = 0 TO enum_to_int(mef_num_events)-1
		INT debug_draw_col_r, debug_draw_col_g, debug_draw_col_b
		BOOL bDrawThisEvent
		TEXT_LABEL_23 str_debug_status
		bDrawThisEvent = FALSE

		IF mEvents[i].bComplete
			str_debug_status = "Complete"
			debug_draw_col_r = 68
			debug_draw_col_g = 255
			debug_draw_col_b = 40
			bDrawThisEvent = TRUE
			i_active_count++
		ELIF mEvents[i].bTriggered
			str_debug_status = "Running"
			debug_draw_col_r = 255
			debug_draw_col_g = 191
			debug_draw_col_b = 0
			bDrawThisEvent = TRUE
			i_active_count++
		ELSE
			str_debug_status = "Idle"
			debug_draw_col_r = 77
			debug_draw_col_g = 77
			debug_draw_col_b = 77
			IF bShowAllEvents
				bDrawThisEvent = TRUE
				i_active_count++
			ENDIF
		ENDIF
		
		IF bDrawThisEvent
			// ID
			str_debug_info = ""
			str_debug_info += i
			SET_TEXT_COLOUR(debug_draw_col_r,debug_draw_col_g,debug_draw_col_b, 255)
			SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, f_top+((i_active_count+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_info)
			
			// Name
			str_debug_info = ""
			str_debug_info += Get_Event_Name(int_to_enum(mission_event_flag, i))
			SET_TEXT_COLOUR(debug_draw_col_r,debug_draw_col_g,debug_draw_col_b, 255)
			SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_name_offset, f_top+((i_active_count+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_info)
			
			// Status
			SET_TEXT_COLOUR(debug_draw_col_r,debug_draw_col_g,debug_draw_col_b, 255)
			SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_status_offset, f_top+((i_active_count+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_status)
			
			// Stage
			str_debug_info = ""
			str_debug_info += mEvents[i].iStage
			SET_TEXT_COLOUR(debug_draw_col_r,debug_draw_col_g,debug_draw_col_b, 255)
			SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_stage_offset, f_top+((i_active_count+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_info)
		ENDIF
	ENDFOR
	
	IF i_active_count = 0
		SET_TEXT_COLOUR(163, 163, 163, 255)
		SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
		DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, f_top+(i_event_offset*f_spacing*f_txt_scale), "STRING", "no active mission events")
	ENDIF
	
#ENDIF
ENDPROC

PROC Manage_Mission_Events()
	INT i
	FOR i = 0 TO enum_to_int(mef_num_events)-1
	
		// make sure the event has triggered but is not complete
		IF (mEvents[i].bTriggered = TRUE)
		AND (mEvents[i].bComplete != TRUE)
		
			// calculate time passed since event was triggered
			INT iTimePassed = GET_GAME_TIMER() - mEvents[i].iTimeStamp
			
			// if past the delay enter the event logic otherwise, do nothing
			IF iTimePassed > mEvents[i].iDelay
			
				// Select the correct vent
				SWITCH int_to_enum(mission_event_flag, i)
				
					CASE mef_lester_phone_call			event_lester_phone_call()		BREAK
					CASE mef_lester_phone_call_2		event_lester_phone_call_2()		BREAK
					CASE mef_manage_bag					event_manage_bag()				BREAK
					CASE mef_engineer_follow			event_engineer_follow()			BREAK
					CASE mef_ambient_milk				event_ambient_milk_lady()		BREAK
					CASE mef_ambient_interview  		event_ambient_interview()		BREAK
					CASE mef_ambient_boardroom			event_ambient_boardroom()		BREAK
					CASE mef_ambient_office				event_ambient_office()			BREAK
					CASE mef_ambient_paper_throw		event_ambient_paper_throw()		BREAK
					CASE mef_ambient_receptionist		event_ambient_receptionist()	BREAK
					CASE mef_engineer_air_guitar		event_engineer_air_guitar()		BREAK
					CASE mef_ambient_dialogue			event_ambient_dialogue()		BREAK
					CASE mef_tracey						event_tracey()					BREAK
				
				ENDSWITCH
			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		// Toggle event debug info on/off
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
			bDisplayEventDebug = NOT bDisplayEventDebug
			IF bDisplayEventDebug
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			ELSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			ENDIF
		ENDIF
	
		IF bDisplayEventDebug
			Display_Event_Debug_Info()
		ENDIF
	#ENDIF
ENDPROC
	
/*
								 _______  _   ______  ______  _  _______  _______    _______  _       _______  _  _  _ 
								(_______)| | / _____)/ _____)| |(_______)(_______)  (_______)(_)     (_______)(_)(_)(_)
								 _  _  _ | |( (____ ( (____  | | _     _  _     _    _____    _       _     _  _  _  _ 
								| ||_|| || | \____ \ \____ \ | || |   | || |   | |  |  ___)  | |     | |   | || || || |
								| |   | || | _____) )_____) )| || |___| || |   | |  | |      | |_____| |___| || || || |
								|_|   |_||_|(______/(______/ |_| \_____/ |_|   |_|  |_|      |_______)\_____/  \_____/ 
*/

//PURPOSE: Checks wthether its ok to let the screen fade in yet
PROC IS_SKIP_OK_TO_FADE_IN_NOW(BOOL bUpdateEvents, BOOL bShopOpen)

	BOOL bRunLoop = TRUE
	WHILE bRunLoop
		BOOL bFinished = TRUE
		
		IF bUpdateEvents
			Manage_Mission_Events()
		ENDIF
		
		IF Is_Event_Triggered(mef_ambient_office)
			IF NOT mEvents[mef_ambient_office].bSafeAfterSkip
				bFinished = FALSE
				CPRINTLN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() NOT FINISHED: EVENT mef_ambient_office not ready")
			ENDIF
		ENDIF
		
		IF Is_Event_Triggered(mef_engineer_follow)
			IF NOT mEvents[mef_engineer_follow].bSafeAfterSkip
				bFinished = FALSE
				CPRINTLN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() NOT FINISHED: EVENT mef_engineer_follow not ready")
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				bFinished = FALSE
				CPRINTLN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() NOT FINISHED: Player streaming requests not finished")
			ENDIF
		ENDIF
		
		IF bShopOpen
			IF NOT IS_SHOP_OPEN_FOR_BUSINESS(CLOTHES_SHOP_M_04_HW)
				bFinished = FALSE
				CPRINTLN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() NOT FINISHED: Shop Not Open")
			ENDIF
		ENDIF
		
		IF bFinished
			bRunLoop = FALSE
			CPRINTLN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() FINISHED")
		ELSE
			WAIT(0)
			CPRINTLN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW(BOOL bUpdateEvents:", bUpdateEvents," BOOL bShopOpen:", bShopOpen)
		ENDIF
	ENDWHILE
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		Unload_Asset_NewLoadScene(sAssetData)	
	ENDIF

ENDPROC

PROC GET_SKIP_STAGE_COORD_AND_HEADING(mission_stage_flag eStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH eStage
		CASE msf_st0_intro_cutscene				vCoord = <<1274.3231, -1710.7501, 53.7715>>			fHeading = 0.0			BREAK
		CASE msf_st1_get_clothes				vCoord = <<1279.1504, -1729.9221, 51.5473>> 		fHeading = 202.8531		BREAK
		CASE msf_st2_go_to_offices				vCoord = <<126.6064, -212.3286, 53.5578>>			fHeading = 341.1025		BREAK
		CASE msf_st3_go_to_computer				vCoord = <<-1047.9139, -233.2071, 38.0145>>			fHeading = 124.0412		BREAK
		CASE msf_st4_mini_game					vCoord = <<-1060.9747, -244.5783, 43.0211>>			fHeading = 21.5040		BREAK
		CASE msf_st5_plant_bomb					vCoord = <<-1060.9747, -244.5783, 43.0211>>			fHeading = 21.5040		BREAK
		CASE msf_st6_leave_building				vCoord = <<-1055.0518, -231.8623, 43.0211>>			fHeading = 203.6044		BREAK
		CASE msf_st7_go_home					vCoord = <<-1080.3341, -263.5708, 36.7904>>			fHeading = 286.3264		BREAK
		CASE msf_st8_tv_start					vCoord = <<-803.3376, 172.3380, 71.8447>>			fHeading = 284.5370		BREAK
		CASE msf_st9_tv_detonate				vCoord = <<-803.3376, 172.3380, 71.8447>>			fHeading = 284.5370		BREAK
		CASE msf_st10_mission_passed			vCoord = <<-803.3376, 172.3380, 71.8447>>			fHeading = 284.5370		BREAK		
	ENDSWITCH

ENDPROC

PROC MISSION_STAGE_SKIP()
	VECTOR vPlayerPos
	
	// a skip has been made
	IF bDoSkip = TRUE
		
		// Begin the skip if the switching is idle
		IF stageSwitch = STAGESWITCH_IDLE
			
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				ENDIF
			ELSE
				Mission_Set_Stage(int_to_enum(mission_stage_flag, iSkipToStage))
			ENDIF

		// needs to be carried out before states own entering stage
		ELIF stageSwitch = STAGESWITCH_ENTERING
		
			//MISSION_STAT_SYSTEM_ALERT_TIME_WARP()
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), true)
			
			Mission_Reset_Everything()
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(sConvo, 0, PLAYER_PED_ID(), "MICHAEL")
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			
			Start_Skip_Streaming(sAssetData)
			
			IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
			ENDIF
			
		// Warping
		// -----------------------------------------------
		
			IF NOT IS_REPLAY_BEING_SET_UP()
			
				CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Performing NON-REPLAY skip warping")
				
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(mission_stage_flag, iSkipToStage), vWarpCoord, fWarpHeading)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 50.0)
				
				CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() NON-REPLAY skip warp coord ", vWarpCoord, " warp heading ", fWarpHeading)
				
			ENDIF
			
		// Asset loading
		// -----------------------------------------------
			CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Requesting assets")
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
				CASE msf_st0_intro_cutscene
					REQUEST_CUTSCENE("lester_1_int")
					Register_Ped_Prop_For_Cutscene(sCutscenePedVariationRegister, "Lester", ANCHOR_EYES, 0,0)
					Load_Asset_Model(sAssetData, IG_LESTERCREST)
					Load_Asset_Model(sAssetData, Prop_WheelChair_01_S)
				BREAK
				CASE msf_st1_get_clothes
					Load_Asset_AnimDict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
				BREAK
				CASE msf_st2_go_to_offices
					Load_Asset_AnimDict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
				BREAK
				CASE msf_st3_go_to_computer
					Load_Asset_Model(sAssetData, mod_hipster_main)
					Load_Asset_Model(sAssetData, mod_hipster_m)
					Load_Asset_Model(sAssetData, mod_hipster_f)
					Load_Asset_Model(sAssetData, mod_hipster_f_heel)
					Load_Asset_Waypoint(sAssetData, str_wp_engineer_route)
					Load_Asset_AnimDict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
//					Load_Asset_AssistedLine(sAssetData, al_door1)
//					Load_Asset_AssistedLine(sAssetData, al_door2)
				BREAK
				CASE msf_st4_mini_game					
					Load_Asset_Audiobank(sAssetData, "COMPUTERS")
					Load_Asset_Audiobank(sAssetData, "Lester1A_01")
					Load_Asset_Audiobank(sAssetData, "Lester1A_Qub3d")
					Load_Asset_Model(sAssetData, mod_hipster_main)
					Load_Asset_Scaleform(sAssetData, "desktop_pc", mov)
//					Load_Asset_AssistedLine(sAssetData, al_door1)
//					Load_Asset_AssistedLine(sAssetData, al_door2)
					Load_Asset_AnimDict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
				BREAK
				CASE msf_st5_plant_bomb
					Load_Asset_Model(sAssetData, IG_JAY_NORRIS)
					Load_Asset_Model(sAssetData, mod_hipster_main)
					Load_Asset_Model(sAssetData, mod_hipster_m)
					Load_Asset_Model(sAssetData, mod_hipster_f)
					Load_Asset_Model(sAssetData, mod_hipster_f_heel)
					Load_Asset_Model(sAssetData, PROP_PAPER_BALL)
					Load_Asset_Model(sAssetData, PROP_MONITOR_01C)
					Load_Asset_AnimDict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
					Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_main)
					Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_intro)
					Load_Asset_AnimDict(sAssetData, anim_dict_paper_throw)
					Load_Asset_AnimDict(sAssetData, anim_dict_interview_1st_exit)
					Load_Asset_AnimDict(sAssetData, anim_dict_airguitar_1st_exit)
					Load_Asset_AnimDict(sAssetData, anim_dict_engineer_airguitar)
					Load_Asset_Model(sAssetData, Prop_Off_Chair_01)
//					Load_Asset_AssistedLine(sAssetData, al_door1)
//					Load_Asset_AssistedLine(sAssetData, al_door2)
					Load_Asset_Audiobank(sAssetData, "Lester1A_01")
					Load_Asset_Audiobank(sAssetData, "Lester1A_Qub3d")
				BREAK
				CASE msf_st6_leave_building
					Load_Asset_Model(sAssetData, IG_JAY_NORRIS)
					Load_Asset_Model(sAssetData, Prop_Off_Chair_01)
					Load_Asset_Model(sAssetData, mod_hipster_main)
					Load_Asset_Model(sAssetData, mod_hipster_m)
					Load_Asset_Model(sAssetData, mod_hipster_f)
					Load_Asset_Model(sAssetData, mod_hipster_f_heel)
					Load_Asset_Model(sAssetData, PROP_PAPER_BALL)
					Load_Asset_Model(sAssetData, PROP_HACKY_SACK_01)
					Load_Asset_Model(sAssetData, PROP_MONITOR_01C)
					Load_Asset_Model(sAssetData, P_MICHAEL_BACKPACK_S)
					Load_Asset_AnimDict(sAssetData, anim_dict_paper_throw)
					Load_Asset_AnimDict(sAssetData, anim_dict_interview_2nd)
					Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_main)
					Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_intro)
					Load_Asset_AnimDict(sAssetData, anim_dict_airguitar_2nd)
//					Load_Asset_AssistedLine(sAssetData, al_door1)
//					Load_Asset_AssistedLine(sAssetData, al_door2)
//					Load_Asset_AssistedLine(sAssetData, al_stairs_down)
					Load_Asset_Audiobank(sAssetData, "Lester1A_01")
					Load_Asset_Audiobank(sAssetData, "Lester1A_Qub3d")
					Load_Asset_AnimDict(sAssetData, "amb@prop_human_seat_computer@male@base")
				BREAK
				CASE msf_st7_go_home
					Load_Asset_Model(sAssetData, mod_hipster_f)
				BREAK
				CASE msf_st8_tv_start
					Load_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_TRACEY))
					Load_Asset_Model(sAssetData, PROP_CS_REMOTE_01)
					Load_Asset_Model(sAssetData, Prop_Phone_ING)
					Load_Asset_AnimDict(sAssetData, ad_ig_1)
					Load_Asset_Audiobank(sAssetData, "Lester1B")
					Load_Asset_Audiobank(sAssetData, "SAFEHOUSE_MICHAEL_SIT_SOFA")
				BREAK
				CASE msf_st9_tv_detonate
					Load_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_TRACEY))
					Load_Asset_Model(sAssetData, PROP_CS_REMOTE_01)
					Load_Asset_Model(sAssetData, Prop_Phone_ING)
					Load_Asset_AnimDict(sAssetData, ad_ig_1)
					Load_Asset_Audiobank(sAssetData, "Lester1B")
					Load_Asset_Audiobank(sAssetData, "SAFEHOUSE_MICHAEL_SIT_SOFA")
				BREAK
				CASE msf_st10_mission_passed
					Load_Asset_NewLoadScene_Sphere(sAssetData, <<-803.3376, 172.3380, 71.8447>>, 10.0)
				BREAK
			ENDSWITCH

			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)				
			ENDWHILE
			
			CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Skip streaming complete")
			
			IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
				CDEBUG1LN(DEBUG_MIKE, "MISSION_STAGE_SKIP() replay checkpoint vehicle available, loading now.")
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					WAIT(0)
					CDEBUG1LN(DEBUG_MIKE, "MISSION_STAGE_SKIP() waiting for HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()")
				ENDWHILE
			ENDIF
			
			IF bRequestedPreload
				CDEBUG1LN(DEBUG_MIKE, "Starting: Preload Ped Variation")
				WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
					WAIT(0)
					CDEBUG1LN(DEBUG_MIKE, "Waiting on: Preload Ped Variation")
				ENDWHILE
				bRequestedPreload = TRUE
				CDEBUG1LN(DEBUG_MIKE, "Finished: Preload Ped Variation")
			ENDIF
			
			BOOL bSkipFadeIn
			
			// Set up the mission for that current stage
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
				
				CASE msf_st0_intro_cutscene
				
					g_QuickSaveDisabledByScript = TRUE
				
					g_sTriggerSceneAssets.ped[0] 		= CREATE_PED(PEDTYPE_MISSION, IG_LESTERCREST, <<1275.3646, -1710.7744, 53.7715>>, 333.7887)
					g_sTriggerSceneAssets.object[0] 	= CREATE_OBJECT(Prop_WheelChair_01_S, <<1275.3646, -1710.7744, 53.7715>>)

					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, FALSE)

					Add_Scenario_Blocking()
					
					IS_SKIP_OK_TO_FADE_IN_NOW(FALSE, TRUE)
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					
					bSkipFadeIn = TRUE
				
				BREAK
				
				// Skip to drive to clothes shop
				CASE msf_st1_get_clothes
					
					SET_CLOCK_TIME(7,0,0)
					
					g_replay.iReplayInt[0] = -1
					g_replay.iReplayInt[1] = -1
					g_replay.iReplayInt[2] = -1
					REMOVE_PROGRAMMER_OUTFIT()
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(v_LestersHouseParkingCoord, f_LestersHouseParkingHeading)
					ENDIF

					Trigger_Event(mef_manage_bag)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
					//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL2,9,0,0)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK, FALSE)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), FALSE)
					
					Add_Scenario_Blocking()

					IF IS_REPLAY_BEING_SET_UP()
						IF DOES_ENTITY_EXIST(veh_replay)
							END_REPLAY_SETUP(veh_replay, VS_DRIVER)
						ELSE
							END_REPLAY_SETUP()
						ENDIF
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW(FALSE, TRUE)
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)

					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
				
				// Skip leaving the clothes shop
				CASE msf_st2_go_to_offices
				
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)
					
					CLEAR_AREA(v_ShopBlip, 25.0, TRUE)
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(<< 127.5399, -197.6886, 53.4039 >>, 251.4021)
					ENDIF
					
					// Make him look like a programmer
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL2,9,0,0)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK, FALSE)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), FALSE)
					
					Trigger_Event(mef_ambient_receptionist)
					Trigger_Event(mef_manage_bag)
					
					Add_Scenario_Blocking()
					
					bObtainedClothing = TRUE
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW(FALSE, FALSE)
					
					SET_DOOR_STATE(GET_SHOP_DOOR_NAME(CLOTHES_SHOP_M_04_HW, 0), DOORSTATE_UNLOCKED)
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 3000)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()

				BREAK
				CASE msf_st3_go_to_computer
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)

					WAYPOINT_RECORDING_GET_COORD(str_wp_engineer_route, 1, vPlayerPos)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 124.0412)
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(v_LifeInvaderCarParkCoord, f_LifeInvaderCarParkHeading)
					ENDIF
					
					// Make him look like a programmer
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL2,9,0,0)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK, FALSE)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), FALSE)
					
					// create other ped
					WAYPOINT_RECORDING_GET_COORD(str_wp_engineer_route, 2, vPlayerPos)
					peds[mpf_smoker] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_main, vPlayerPos, 118.9741)
					
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(peds[mpf_smoker], FALSE)
					ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_smoker], "LIEngineer")
					
					Trigger_Event(mef_ambient_receptionist)
					mEvents[mef_engineer_follow].bSafeAfterSkip = FALSE
					Trigger_Event(mef_engineer_follow)
					
					Add_Scenario_Blocking()
					
					bObtainedClothing = TRUE
					bDisableConspicuousBehavior = TRUE
					
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_UNLOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_UNLOCKED)
					
					START_AUDIO_SCENE("LESTER_1A_FOLLOW_PROGRAMMER")
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW(TRUE, FALSE)
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 117.8704, FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()

				BREAK
				CASE msf_st4_mini_game
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(v_LifeInvaderCarParkCoord, f_LifeInvaderCarParkHeading)
					ENDIF
					
					// Make him look like a programmer
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL2,0,0)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK, FALSE)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), FALSE)
					
					// create other ped
					peds[mpf_smoker] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_main, << -1063.4578, -243.7453, 43.0347 >>, 260.8757)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(peds[mpf_smoker], FALSE)
					ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_smoker], "LIEngineer")
										
					Add_Scenario_Blocking()
					
					bObtainedClothing = TRUE
					bDisableConspicuousBehavior = TRUE
					
					SET_DOOR_STATE(DOORNAME_LINVADER_OFFICE_UP, DOORSTATE_UNLOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_UNLOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_UNLOCKED)
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW(FALSE, FALSE)
					
					INT i
					FOR i = 0 TO iUsablePopups-1
						IF sPopups[i].iType != -1
						AND sPopups[i].iSfxLoop != -1
						AND sPopups[i].bSfxPlaying = FALSE
							STRING strSFX
							IF iPopupsSFXPlaying < 6
								SWITCH (iPopupsSFXPlaying)
									CASE 0 strSFX = "POPUP_MUSIC_01" BREAK
									CASE 1 strSFX = "POPUP_MUSIC_02" BREAK
									CASE 2 strSFX = "POPUP_MUSIC_03" BREAK
									CASE 3 strSFX = "POPUP_MUSIC_04" BREAK
									CASE 4 strSFX = "POPUP_MUSIC_05" BREAK
									CASE 5 strSFX = "POPUP_MUSIC_06" BREAK
									DEFAULT strSFX = "POPUP_MUSIC_RND"	BREAK
								ENDSWITCH
							ELSE
								strSFX = "POPUP_MUSIC_RND"
							ENDIF
							PLAY_SOUND_FROM_COORD(sPopups[i].iSfxLoop, strSFX, <<-1059.63, -244.61, 43.92>>, "LESTER1A_SOUNDS")
							SET_VARIABLE_ON_SOUND(sPopups[i].iSfxLoop, "State", 2)
							CPRINTLN(DEBUG_MIKE, "MINI GAME Porn popup sfx started with volume 2 for popup: ", i)
							sPopups[i].bSfxPlaying = TRUE
							iPopupsSFXPlaying++
						ENDIF
					ENDFOR
					
					REPEAT COUNT_OF(sPopups) i
						IF sPopups[i].bSfxPlaying
						AND sPopups[i].iSfxLoop != -1
							SET_VARIABLE_ON_SOUND(sPopups[i].iSfxLoop, "TracksPlaying", TO_FLOAT(CLAMP_INT(iPopupsSFXPlaying, 1, 12)))
						ENDIF
					ENDREPEAT
					
					WHILE NOT DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
						WAIT(0)
						CDEBUG1LN(DEBUG_MIKE, "MISSION_STAGE_SKIP() can't find the programmer's chair")
					ENDWHILE
					objects[mof_smokers_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				BREAK
				
				CASE msf_st5_plant_bomb
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(v_LifeInvaderCarParkCoord, f_LifeInvaderCarParkHeading)
					ENDIF

					// Make him look like a programmer
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL2,9,0,0)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK, FALSE)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), FALSE)
					
					// create other ped
					peds[mpf_smoker] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_main, << -1063.4578, -243.7453, 43.0347 >>, 260.8757)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(peds[mpf_smoker], FALSE)
					ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_smoker], "LIEngineer")
					
					Trigger_Event(mef_ambient_receptionist)
					Trigger_Event(mef_ambient_office)
					Trigger_Event(mef_ambient_interview)
					Trigger_Event(mef_ambient_boardroom)
					Trigger_Event(mef_ambient_paper_throw)
					Trigger_Event(mef_engineer_air_guitar)
					Trigger_Event(mef_ambient_dialogue)
					
					Add_Scenario_Blocking()
					
					bObtainedClothing = TRUE
					bDisableConspicuousBehavior = TRUE
					
					SET_DOOR_STATE(DOORNAME_LINVADER_OFFICE_UP, DOORSTATE_UNLOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_UNLOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_UNLOCKED)
					
					START_AUDIO_SCENE("LESTER_1A_RIG_EXPLOSIVE")
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
										
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW(TRUE, FALSE)
					
					WHILE NOT DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
						WAIT(0)
						CDEBUG1LN(DEBUG_MIKE, "MISSION_STAGE_SKIP() can't find the programmer's chair")
					ENDWHILE
					objects[mof_smokers_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
					
					IF NOT bSwappedMonitorToClean
						IF bSwappedMonitorToPopups
							CREATE_MODEL_SWAP(v_engineer_monitor, 1.0, PROP_MONITOR_01D, PROP_MONITOR_01C, TRUE)
						ELSE
							CREATE_MODEL_SWAP(v_engineer_monitor, 1.0, PROP_MONITOR_LI, PROP_MONITOR_01C, TRUE)
						ENDIF
						Unload_Asset_Model(sAssetData, PROP_MONITOR_01C)
						bSwappedMonitorToClean = TRUE
					ENDIF
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, FALSE, FAUS_CUTSCENE_EXIT)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
				CASE msf_st6_leave_building
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)
					SET_DOOR_STATE(DOORNAME_LINVADER_OFFICE_UP, DOORSTATE_UNLOCKED)
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(v_LifeInvaderCarParkCoord, f_LifeInvaderCarParkHeading)
					ENDIF
				
					// Make him look like a programmer
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					
					peds[mpf_smoker] = CREATE_PED(PEDTYPE_MISSION, mod_hipster_main, << -1062.2268, -243.1470, 43.0212 >>, 293.0184)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(peds[mpf_smoker], FALSE)
					ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_smoker], "LIEngineer")	

					TRIGGER_EVENT(mef_ambient_office)
					TRIGGER_EVENT(mef_ambient_interview)
					TRIGGER_EVENT(mef_ambient_boardroom)
					TRIGGER_EVENT(mef_ambient_paper_throw)
					//Trigger_Event(mef_engineer_air_guitar)
					TRIGGER_EVENT(mef_ambient_receptionist)
					Trigger_Event(mef_ambient_dialogue)
					
					Add_Scenario_Blocking()
					
					bObtainedClothing = TRUE
					bDisableConspicuousBehavior = TRUE
					
					Create_Prop(objects[mof_mike_rucksack], P_MICHAEL_BACKPACK_S, v_BagCoord)
					SET_ENTITY_COLLISION(objects[mof_mike_rucksack].id, FALSE)
					SET_ENTITY_COORDS(objects[mof_mike_rucksack].id, v_BagCoord)
					SET_ENTITY_ROTATION(objects[mof_mike_rucksack].id, v_BagRot)
					FREEZE_ENTITY_POSITION(objects[mof_mike_rucksack].id, TRUE)
					
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_UNLOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_UNLOCKED)
					
					DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_LINVADER_OFFICE_UP), 0.0, TRUE, TRUE)
					
					START_AUDIO_SCENE("LESTER_1A_LEAVE_OFFICE")
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW(TRUE, FALSE)
					
					WHILE NOT DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
						WAIT(0)
						CDEBUG1LN(DEBUG_MIKE, "MISSION_STAGE_SKIP() can't find the programmer's chair")
					ENDWHILE
					objects[mof_smokers_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
					
					IF NOT bSwappedMonitorToClean
						IF bSwappedMonitorToPopups
							CREATE_MODEL_SWAP(v_engineer_monitor, 1.0, PROP_MONITOR_01D, PROP_MONITOR_01C, TRUE)
						ELSE
							CREATE_MODEL_SWAP(v_engineer_monitor, 1.0, PROP_MONITOR_LI, PROP_MONITOR_01C, TRUE)
						ENDIF
						Unload_Asset_Model(sAssetData, PROP_MONITOR_01C)
						bSwappedMonitorToClean = TRUE
					ENDIF
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
				CASE msf_st7_go_home
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					bObtainedClothing = TRUE
					
					SET_DOOR_STATE(DOORNAME_LINVADER_OFFICE_UP, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_LOCKED)
					
					TRIGGER_EVENT(mef_ambient_receptionist)
					Trigger_Event(mef_tracey)
					
					bDisableConspicuousBehavior = FALSE
					bAllowRunning = TRUE
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(v_LifeInvaderCarParkCoord, f_LifeInvaderCarParkHeading)
					ENDIF
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW(FALSE, TRUE)
					
					SET_WANTED_LEVEL_MULTIPLIER(0.1)
					SET_MAX_WANTED_LEVEL(5)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
				CASE msf_st8_tv_start
					
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					bObtainedClothing = TRUE
					
					bDisableConspicuousBehavior = FALSE
					bAllowRunning = TRUE
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-827.0287, 175.9394, 69.8927>>, 331.2491)
					ENDIF
					
					SET_DOOR_STATE(DOORNAME_LINVADER_OFFICE_UP, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_LOCKED)

					Create_Prop(objects[mof_tv_remote], PROP_CS_REMOTE_01, GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_PH_L_HAND, <<0,0,0>>))	
					Unload_Asset_Model(sAssetData, PROP_CS_REMOTE_01)
					ATTACH_ENTITY_TO_ENTITY(objects[mof_tv_remote].id, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)

					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					
					cam_tv = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos[CAMS_SCRIPT], vCamRot[CAMS_SCRIPT], 40.0, TRUE)
					SHAKE_CAM(cam_tv, "HAND_SHAKE", 0.3)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					
					SET_NEW_ANIM_TASK(MAS_R_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_LOOPING|AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME, TRUE)
					
					WHILE NOT IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_MICHAEL_PROJECTOR)
						wait(0)
					ENDWHILE
					
					START_AMBIENT_TV_PLAYBACK(TV_LOC_MICHAEL_PROJECTOR)
					PLAY_TV_CHANNEL_WITH_PLAYLIST(TV_LOC_MICHAEL_PROJECTOR, TVCHANNELTYPE_CHANNEL_2, TV_PLAYLIST_SPECIAL_LEST1_INTRO_FAME_OR_SHAME, TRUE)
					bTVTurnedOn = TRUE

					IS_SKIP_OK_TO_FADE_IN_NOW(FALSE, TRUE)
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				BREAK
				CASE msf_st9_tv_detonate
					
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					bObtainedClothing = TRUE
					
					SET_DOOR_STATE(DOORNAME_LINVADER_OFFICE_UP, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_LOCKED)

					Create_Prop(objects[mof_tv_remote], PROP_CS_REMOTE_01, GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_PH_L_HAND, <<0,0,0>>))	
					Unload_Asset_Model(sAssetData, PROP_CS_REMOTE_01)
					ATTACH_ENTITY_TO_ENTITY(objects[mof_tv_remote].id, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
					
					bDisableConspicuousBehavior = FALSE
					bAllowRunning = TRUE
					bExterminated = TRUE
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-827.0287, 175.9394, 69.8927>>, 331.2491)
					ENDIF
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					cam_tv = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos[CAMS_SCRIPT], vCamRot[CAMS_SCRIPT], 40.0, TRUE)
					SHAKE_CAM(cam_tv, "HAND_SHAKE", 0.3)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					
					SET_NEW_ANIM_TASK(MAS_R_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_LOOPING|AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME, TRUE)
					
					IS_SKIP_OK_TO_FADE_IN_NOW(FALSE, TRUE)
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
				CASE msf_st10_mission_passed
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CLOTHES_SHOP_M_04_HW, TRUE)
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
					PUT_PLAYER_IN_PROGRAMMER_OUTFIT()
					bObtainedClothing = TRUE
					
					SET_DOOR_STATE(DOORNAME_LINVADER_OFFICE_UP, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_LOCKED)
					
					bDisableConspicuousBehavior = FALSE
					bAllowRunning = TRUE
					bExterminated = TRUE
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
							wait(0)
						ENDWHILE
						veh_replay = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-827.0287, 175.9394, 69.8927>>, 331.2491)
					ENDIF

					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW(FALSE, TRUE)
					
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()

				BREAK
				
			ENDSWITCH
			
			CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Skip finished")
			
			IF (IS_SCREEN_FADED_OUT()
			OR NOT IS_SCREEN_FADING_IN())
			AND NOT bSkipFadeIn
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Fading in")
			ENDIF
			
			bDoSkip = FALSE
		ENDIF
#IF IS_DEBUG_BUILD
	// Check is a skip is being asked for
	ELIF IS_SCREEN_FADED_IN()
	AND NOT IS_CUTSCENE_PLAYING()
	
		INT iCurrentStage
		INT iReturnStage
		
		IF TRANSLATE_TO_Z_MENU(zMenuNames, mission_stage, iCurrentStage)
			
			DONT_DO_J_SKIP(locates_data)
			IF LAUNCH_MISSION_STAGE_MENU(zMenuNames, iReturnStage, iCurrentStage, FALSE, "Lester 1", FALSE, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
							
				iSkipToStage = TRANSLATE_FROM_Z_MENU(zMenuNames, iReturnStage)
				
				IF IS_CUTSCENE_ACTIVE()
					STOP_CUTSCENE()
					REMOVE_CUTSCENE()
				ENDIF
				
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				bDoSkip = TRUE
			ENDIF	
			
		ENDIF

#ENDIF
	ENDIF
	
ENDPROC


//PURPOSE: Sets up the mission
PROC Mission_Setup()

	SET_MISSION_FLAG(TRUE)

// Handle replay
//----------------------------------------------------------------------
	IF IS_REPLAY_IN_PROGRESS()	// RETRIES
	OR IS_REPEAT_PLAY_ACTIVE()	// REPLAY VIA START MENU
	
		IF IS_REPEAT_PLAY_ACTIVE()
			SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, FALSE)
		ENDIF
		
		IF IS_REPLAY_IN_PROGRESS()
			
			iSkipToStage = GET_REPLAY_MID_MISSION_STAGE()
			CPRINTLN(DEBUG_MIKE, "LES1A: Get_Replay_Mid_Mission_Stage() = ", iSkipToStage)
			IF g_bShitskipAccepted
				iSkipToStage += 1
				CPRINTLN(DEBUG_MIKE, "LES1A: g_bShitskipAccepted")
			ENDIF
			
		ELIF IS_REPEAT_PLAY_ACTIVE()
		
			g_replay.iReplayInt[0] = -1
			g_replay.iReplayInt[1] = -1
			g_replay.iReplayInt[2] = -1	
			iSkipToStage = 0
		
		ENDIF
		
		CPRINTLN(DEBUG_MIKE, "LES1A: Checkpoint skipping to ", iSkipToStage)
		
		// Translate checkpoint into mission stage
		SWITCH iSkipToStage
			CASE 0						iSkipToStage = enum_to_int(msf_st0_intro_cutscene)						BREAK
			CASE 1						iSkipToStage = enum_to_int(msf_st1_get_clothes)							BREAK
			CASE 2						iSkipToStage = enum_to_int(msf_st2_go_to_offices)						BREAK
			CASE 3						iSkipToStage = enum_to_int(msf_st3_go_to_computer)						BREAK
			CASE 4						iSkipToStage = enum_to_int(msf_st5_plant_bomb)							BREAK
			CASE 5						iSkipToStage = enum_to_int(msf_st6_leave_building)						BREAK
			CASE 6						iSkipToStage = enum_to_int(msf_st7_go_home)								BREAK
			CASE 7						iSkipToStage = enum_to_int(msf_st8_tv_start)							BREAK
			CASE 8						iSkipToStage = enum_to_int(msf_st10_mission_passed)						BREAK
		ENDSWITCH
		
		CPRINTLN(DEBUG_MIKE, "LES1A: stage skipping to ", GET_MISSION_STAGE_NAME(int_to_enum(mission_stage_flag, iSkipToStage)))
		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(mission_stage_flag, iSkipToStage), vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
			CPRINTLN(DEBUG_MIKE, "LES1A: Using replay warp system - Warp Coord ", vSkipToCoord, " Warp Heading ", fSkipToHeading)
		ENDIF
		
		bDoSkip = TRUE
	ELSE
		g_replay.iReplayInt[0] = -1
		g_replay.iReplayInt[1] = -1
		g_replay.iReplayInt[2] = -1	
	ENDIF
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_INVADER_OFFICE_INTERIOR, BUILDINGSTATE_DESTROYED )

// Regular Setup procedure
//----------------------------------------------------------------------
	REQUEST_ADDITIONAL_TEXT("LEST1", MISSION_TEXT_SLOT)

#IF IS_DEBUG_BUILD
	zMenuNames[0].sTxtLabel 	= "CUTSCENE: LES_1A_INT"
	zMenuNames[1].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st1_get_clothes)
	zMenuNames[2].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st2_go_to_offices)
	zMenuNames[3].sTxtLabel 	= "CUTSCENE: LES_1A_MCS_1"
	zMenuNames[3].bSelectable	= FALSE
	zMenuNames[4].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st3_go_to_computer)
	zMenuNames[5].sTxtLabel 	= "CUTSCENE: LES_1A_MCS_2"
	zMenuNames[5].bSelectable	= FALSE
	zMenuNames[6].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st4_mini_game)
	zMenuNames[7].sTxtLabel 	= "CUTSCENE: LES_1A_MCS_3"
	zMenuNames[7].bSelectable	= FALSE
	zMenuNames[8].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st5_plant_bomb)
	zMenuNames[9].sTxtLabel 	= "CUTSCENE: LES_1A_MCS_4"
	zMenuNames[9].bSelectable	= FALSE
	zMenuNames[10].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st6_leave_building)
	zMenuNames[11].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st7_go_home)
	zMenuNames[12].sTxtLabel 	= "CUTSCENE: LES_1B_MCS_1"
	zMenuNames[12].bSelectable	= FALSE
	zMenuNames[13].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st8_tv_start)
	zMenuNames[14].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st9_tv_detonate)
	zMenuNames[15].sTxtLabel 	= GET_MISSION_STAGE_NAME(msf_st10_mission_passed)
	
	widget_debug = START_WIDGET_GROUP("Lester1a_widget")
		ADD_WIDGET_BOOL("Open Window", b_debug_window_open)
		START_WIDGET_GROUP("Minigame Debug")
			ADD_WIDGET_INT_READ_ONLY(	"Popups help index",		iPopupHelp)
			ADD_WIDGET_INT_READ_ONLY(	"Popups currently open",	i_debug_PopupsOpen)
			ADD_WIDGET_INT_READ_ONLY(	"Popups with sfx playing",	iPopupsSFXPlaying)
			ADD_WIDGET_INT_READ_ONLY(	"Last popup closed",		iLastPopupClosed)
			ADD_WIDGET_BOOL(			"PrintToConsole", 			b_debug_PrintToConsole)
			ADD_WIDGET_BOOL(			"IsAVOpen",					bIsAVOpen)
			ADD_WIDGET_INT_READ_ONLY( 	"Usable popups",			iUsablePopups)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Minigame Popups Open")
			ADD_WIDGET_BOOL("Popup 0", sPopups[0].bClosed)
			ADD_WIDGET_BOOL("Popup 1", sPopups[1].bClosed)
			ADD_WIDGET_BOOL("Popup 2", sPopups[2].bClosed)
			ADD_WIDGET_BOOL("Popup 3", sPopups[3].bClosed)
			ADD_WIDGET_BOOL("Popup 4", sPopups[4].bClosed)
			ADD_WIDGET_BOOL("Popup 5", sPopups[5].bClosed)
			ADD_WIDGET_BOOL("Popup 6", sPopups[6].bClosed)
			ADD_WIDGET_BOOL("Popup 7", sPopups[7].bClosed)
			ADD_WIDGET_BOOL("Popup 8", sPopups[8].bClosed)
			ADD_WIDGET_BOOL("Popup 9", sPopups[9].bClosed)
			ADD_WIDGET_BOOL("Popup 10", sPopups[10].bClosed)
			ADD_WIDGET_BOOL("Popup 11", sPopups[11].bClosed)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
#ENDIF
	
	// Clear costume & remove costume, so player has to buy it again
	RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	REMOVE_PROGRAMMER_OUTFIT()

	peds[mpf_mike] = PLAYER_PED_ID()
	
	// Reset the shop and make sure it is open.
	FORCE_SHOP_RESET(CLOTHES_SHOP_M_04_HW)
	SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_M_04_HW, TRUE)
	
// Minigame initialisation
	// Icons
	iIcons[0] = 8
	iIcons[1] = 2
	iIcons[2] = 14
	iIcons[3] = 5
	iIcons[4] = 11
	iIcons[5] = 17
	
	IF GET_IS_WIDESCREEN()
	
		// Set the advert types
		sPopups[0].iType = 2
		sPopups[1].iType = 0
		sPopups[2].iType = 5
		sPopups[3].iType = 7
		sPopups[4].iType = 1
		sPopups[5].iType = 3
		sPopups[6].iType = 4
		sPopups[7].iType = 11
		sPopups[8].iType = 8
		sPopups[9].iType = 9
		sPopups[10].iType = 10
		sPopups[11].iType = 6

		// set the 0 to cover the AV icon
		sPopups[0].fPosX = 0.1      		sPopups[0].fPosY = 0.20833333333333334
	   	sPopups[1].fPosX = 0.13671875     	sPopups[1].fPosY = 0.06944444444444445
	   	sPopups[2].fPosX = 0.078125       	sPopups[2].fPosY = 0.1388888888888889
	  	sPopups[3].fPosX = 0.3125         	sPopups[3].fPosY = 0.05
	   	sPopups[4].fPosX = 0.5       		sPopups[4].fPosY = 0.05
	   	sPopups[5].fPosX = 0.4296875      	sPopups[5].fPosY = 0.4
	   	sPopups[6].fPosX = 0.1171875      	sPopups[6].fPosY = 0.4444444444444444
	   	sPopups[7].fPosX = 0.390625       	sPopups[7].fPosY = 0.0
	   	sPopups[8].fPosX = 0.5078125      	sPopups[8].fPosY = 0.4166666666666667
	   	sPopups[9].fPosX = 0.025     		sPopups[9].fPosY = 0.45
	   	sPopups[10].fPosX = 0.3125        	sPopups[10].fPosY = 0.20833333333333334
	   	sPopups[11].fPosX = 0.3515625     	sPopups[11].fPosY = 0.2777777777777778

	ELSE
	
		// Set the advert types
		sPopups[0].iType = 2
		sPopups[1].iType = 5
		sPopups[2].iType = 3
		sPopups[3].iType = 4
		sPopups[4].iType = 8
		sPopups[5].iType = 9
		sPopups[6].iType = 10
		sPopups[7].iType = 6
	
		// set the 0 to cover the AV icon
		sPopups[0].fPosX = 0.0390625     sPopups[0].fPosY = 0.2083
	   	sPopups[1].fPosX = 0.078125      sPopups[1].fPosY = 0.1388
	   	sPopups[2].fPosX = 0.4296875     sPopups[2].fPosY = 0.5000
	   	sPopups[3].fPosX = 0.1171875     sPopups[3].fPosY = 0.4444
	   	sPopups[4].fPosX = 0.35    		 sPopups[4].fPosY = 0.4166
	   	sPopups[5].fPosX = 0.05859375    sPopups[5].fPosY = 0.4861
	   	sPopups[6].fPosX = 0.3125        sPopups[6].fPosY = 0.2083
	   	sPopups[7].fPosX = 0.3515625     sPopups[7].fPosY = 0.2777
	
	ENDIF
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	// work out how many usable popups there are and get them a sound ID
	iUsablePopups = 0
	INT i
	FOR i = 0 TO 11
		IF sPopups[i].iType != -1
			sPopups[i].bClosed = FALSE
			sPopups[i].iSfxLoop = GET_SOUND_ID()
			sPopups[i].bSfxPlaying = FALSE
			iUsablePopups++
		ELSE
			sPopups[i].bClosed = TRUE
		ENDIF
	ENDFOR

	iSFX_FindVirus 			= GET_SOUND_ID()
	
	interior_lesters 		= GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1273.7292, -1712.0665, 53.7715>>, "v_lesters")				
	interior_offices 		= GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1047.5997, -232.3503, 38.0135 >>, "v_faceoffice")
	interior_living_room 	= GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -803.1297, 171.9331, 71.8348 >>, "v_michael")
	bHasChanged				= GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)

	Doors_Add_New(enum_to_int(mdf_office_board_room_l), <<-1048.28, -236.82, 44.17>>, V_ILEV_FIB_DOOR2)
	Doors_Add_New(enum_to_int(mdf_office_board_room_r), <<-1047.08, -239.12, 44.17>>, V_ILEV_FIB_DOOR2)
	Doors_Add_New(enum_to_int(mdf_office_slide_1), <<-1063.84, -240.65, 43.02>>, V_ILEV_FB_SL_DOOR01)
	Doors_Add_New(enum_to_int(mdf_office_slide_2), <<-1057.77, -237.48, 43.02>>, V_ILEV_FB_SL_DOOR01)
	
	// These doors are locked for the duration of the mission
	Doors_Set_State(enum_to_int(mdf_office_slide_1), TRUE, 0.0)
	Doors_Set_State(enum_to_int(mdf_office_slide_2), TRUE, 0.0)	
	Doors_Set_State(enum_to_int(mdf_office_board_room_l), TRUE, 0.0)
	Doors_Set_State(enum_to_int(mdf_office_board_room_r), TRUE, 0.0)
	
	SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_LOCKED)
	
	Add_Scenario_Blocking()
	
	REPEAT COUNT_OF(syncedIDs) i
		syncedIDs[i] = -1
	ENDREPEAT
	
	//B* 2091976: the data should be set-up by the trigger scene cutscene. This does not happein in a repeat play
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(debug_dan,"Setting player data in cutscene")
		SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE)
	ELSE
		SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE)
	ENDIF
	
	SET_PED_MODEL_IS_SUPPRESSED(mod_hipster_main, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(CS_LIFEINVAD_01, TRUE)
	
	ADD_RELATIONSHIP_GROUP("REL_FREIND", rel_Friend)
	ADD_RELATIONSHIP_GROUP("Family", REL_FAMILY)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, rel_Friend)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, REL_FAMILY)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_Friend, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_Friend, REL_FAMILY)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, REL_FAMILY)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_FAMILY, rel_Friend)
	
	g_bLaptopMissionSuppressed = TRUE
	
	
	PED_COMP_ITEM_DATA_STRUCT sClothesData 
	
	PED_COMP_NAME_ENUM eComp
	FOR eComp = TORSO_P0_GILET_0 TO TORSO_P0_GILET_5
		sClothesData = GET_PED_COMP_DATA_FOR_ITEM_MICHAEL_TORSO(eComp)
		IF sClothesData.iCost > i_most_expensive_gilet
			i_most_expensive_gilet = sClothesData.iCost
		ENDIF
	ENDFOR
	
	FOR eComp = LEGS_P0_CARGO_SHORTS_0 TO LEGS_P0_CARGO_SHORTS_4
		sClothesData = GET_PED_COMP_DATA_FOR_ITEM_MICHAEL_LEGS(eComp)
		IF sClothesData.iCost > i_most_expensive_shorts
			i_most_expensive_shorts = sClothesData.iCost
		ENDIF
	ENDFOR
	
	i_min_required_funds = i_most_expensive_gilet + i_most_expensive_shorts
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	SET_MAX_WANTED_LEVEL(0)
	
	// LESTER 1B merge----------------------------------------------------------------------------
	
	vCamPos[CAMS_ULTRA]		= <<-802.252747,173.037430,74.357079>>
	vCamRot[CAMS_ULTRA]     = <<0.914659,-0.000000,-69.592506>>

	vCamPos[CAMS_CLOSE]   	= <<-804.174622,172.362076,74.324562>>
	vCamRot[CAMS_CLOSE]   	= <<0.914659,-0.000000,-71.189026>>

	vCamPos[CAMS_ROOM]    	= <<-804.157471,170.530487,73.041122>>
	vCamRot[CAMS_ROOM]   	= <<14.629187,0.000000,-54.176620>>
	
	vCamPos[CAMS_SCRIPT]	= <<-805.6417, 171.1509, 73.2577>>
	vCamRot[CAMS_SCRIPT]	= <<2.1993, 0.0000, -68.9021>>
	
	ADD_PED_FOR_DIALOGUE(sConvo, 0, PLAYER_PED_ID(), "MICHAEL")
	
ENDPROC

PED_COMP_NAME_ENUM eHeadProp

PROC ST0_Intro_Cutscene()
	
	VECTOR			v_mike_coord_post_cutscene		= <<1273.7292, -1712.0665, 53.7715>>
	CONST_FLOAT		f_mike_heading_post_cutscene	202.4595

	SWITCH mission_substage
		CASE STAGE_ENTRY
		
			// grab entities from trigger scene
			IF NOT DOES_ENTITY_EXIST(peds[mpf_lester])
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					peds[mpf_lester] 			= g_sTriggerSceneAssets.ped[0]
					SET_ENTITY_AS_MISSION_ENTITY(peds[mpf_lester], TRUE, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_lester], REL_FRIEND)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(objects[mof_wheelchair].id)
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
					objects[mof_wheelchair].id 	= g_sTriggerSceneAssets.object[0]
					SET_ENTITY_AS_MISSION_ENTITY(objects[mof_wheelchair].id, TRUE, TRUE)
				ENDIF
			ENDIF
		
			// once grabbed deal with the players vehicle
			IF DOES_ENTITY_EXIST(peds[mpf_lester])
			AND DOES_ENTITY_EXIST(objects[mof_wheelchair].id)
			AND NOT IS_PED_INJURED(peds[mpf_lester])
				
				SET_ROADS_IN_ANGLED_AREA(<<1368.168823,-1686.163818,68.780426>>, <<1189.980713,-1764.964233,31.711254>>, 91.5, FALSE, FALSE)
				i_road_node_speed_zone = ADD_ROAD_NODE_SPEED_ZONE(<<1279.403320,-1730.470703,51.567390>>, 20.312500, 0.0, FALSE)
				
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", FALSE, TRUE)
			
				Load_Asset_AnimDict(sAssetData, "MissLester1ALeadInOut")
				//MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_LESTER_1)
				
				SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_UNLOCKED)

				eHeadProp = DUMMY_PED_COMP
				mission_substage++
				
			ENDIF
			
			SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, FALSE)
		BREAK
		CASE 1
			IF HAS_CUTSCENE_LOADED()
			AND NOT IS_PED_INJURED(peds[mpf_lester])
			AND DOES_ENTITY_EXIST(objects[mof_wheelchair].id)
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)

				CLEAR_HELP()
				CLEAR_PRINTS()
				
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_lester], "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(objects[mof_wheelchair].id, "Wheelchair_Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				Load_Asset_AnimDict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
				//SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 9, 0)
				PRELOAD_PED_COMP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK)

				eHeadProp = GET_PROP_ITEM_FROM_VARIATIONS( PLAYER_PED_ID(), GET_PED_PROP_INDEX( PLAYER_PED_ID(), ANCHOR_HEAD ), GET_PED_PROP_TEXTURE_INDEX( PLAYER_PED_ID(), ANCHOR_HEAD ), ANCHOR_HEAD )
				CPRINTLN( DEBUG_MIKE, "eHeadProp = ", ENUM_TO_INT( eHeadProp ) )
				IF ENUM_TO_INT(eHeadProp) != 0
					PRELOAD_PED_COMP( PLAYER_PED_ID(), COMP_TYPE_PROPS, eHeadProp )
				ENDIF
				
				
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				bCutsceneExitHasFired = FALSE
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			IF IS_CUTSCENE_PLAYING()
			
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1275.838989,-1719.560547,53.539257>>, <<1270.735229,-1708.342651,56.788410>>, 9.687500, v_LestersHouseParkingCoord, f_LestersHouseParkingHeading)
				SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(v_LestersHouseParkingCoord, f_LestersHouseParkingHeading)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
				
				REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
				
				CLEAR_AREA_OF_PEDS(<<1272.145630,-1716.185547,55.141109>>, 23.750000)
				CLEAR_AREA_OF_PEDS(<<1283.72, -1751.16, 51.03>>, 12.688)
				
				SET_CLOCK_TIME(7,0,0)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 9, 0)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, eHeadProp, FALSE)
				
				SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//AND GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) != CAM_VIEW_MODE_FIRST_PERSON // 2001919
					// set player on the steps
					SET_ENTITY_COORDS(PLAYER_PED_ID(), v_mike_coord_post_cutscene)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), f_mike_heading_post_cutscene)
				ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				REPLAY_STOP_EVENT()
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0)
				bCutsceneExitHasFired = TRUE
			ENDIF
		
			IF bCutsceneExitHasFired
			AND HAS_ANIM_DICT_LOADED("MissLester1ALeadInOut")
			AND HAS_ANIM_DICT_LOADED("MOVE_P_M_ZERO_RUCKSACK")
				
				// set roads back to normal
				SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<1368.168823,-1686.163818,68.780426>>, <<1189.980713,-1764.964233,31.711254>>, 91.5)
				IF i_road_node_speed_zone != -1
					REMOVE_ROAD_NODE_SPEED_ZONE(i_road_node_speed_zone)
					i_road_node_speed_zone = -1
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_lester])
					syncedIDs[ssf_lead_in] = CREATE_SYNCHRONIZED_SCENE(<<1277.661, -1713.688, 54.410>>, <<0,0,-151.560>>)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_lester],					syncedIDs[ssf_lead_in], "MissLester1ALeadInOut", "Lester_1_INT_LeadIn_loop_Lester", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_MELEE | RBF_PLAYER_IMPACT)
					SAFE_PLAY_SYNCHRONIZED_ENTITY(objects[mof_wheelchair], 	syncedIDs[ssf_lead_in], "Lester_1_INT_LeadIn_loop_wChair", "MissLester1ALeadInOut", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedIDs[ssf_lead_in], TRUE)
				ENDIF
				
				PLAY_PAIN(peds[mpf_lester], AUD_DAMAGE_REASON_COUGH)

				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
				bCutsceneExitHasFired = FALSE
				
				SET_WANTED_LEVEL_MULTIPLIER(1.0)
				SET_MAX_WANTED_LEVEL(5)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			// fade screen in if faded out
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			
			Mission_Set_Stage(msf_st1_get_clothes)
		BREAK
		CASE STAGE_EXIT
		
			
		
			// any code to run before the stage exits
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		BREAK
	ENDSWITCH
	
	
ENDPROC	

PROC ST1_Get_Clothing()

	VECTOR 		v_GilletBlipCoord		= <<126.87, -218.96, 53.56>>
	VECTOR		v_ShortsBlipCoord		= <<130.71, -217.81, 53.56>>
	
	// shop cutscene streaming
	IF mission_substage <= 7
	AND mission_substage != STAGE_EXIT
		IF NOT DOES_ENTITY_EXIST(peds[mpf_shop])
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_ShopBlip) < DEFAULT_CUTSCENE_LOAD_DIST
				IF GET_CLOSEST_PED(<<127.11, -224.48, 53.56>>, 2.0, FALSE, TRUE, peds[mpf_shop], FALSE, TRUE)
					ADD_PED_FOR_DIALOGUE(sConvo, 8, peds[mpf_shop], "SHOPASSISTANT")
				ENDIF
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_ShopBlip) > DEFAULT_CUTSCENE_UNLOAD_DIST
				peds[mpf_shop] = NULL
			ENDIF
		ENDIF
	ENDIF
	
	IF mission_substage < 6
	AND mission_substage != STAGE_EXIT
		IF NOT IS_CUTSCENE_ACTIVE()
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_ShopBlip) < DEFAULT_CUTSCENE_LOAD_DIST
			AND DOES_ENTITY_EXIST(peds[mpf_shop])
				REQUEST_CUTSCENE("LES_1A_MCS_0")
				Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, PLAYER_PED_ID(), "Michael")
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Michael", PED_COMP_SPECIAL2, 9, 0)
				Register_Ped_Prop_For_Cutscene(sCutscenePedVariationRegister, "Michael", ANCHOR_HEAD, -1, 0)
				Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, peds[mpf_shop], "Shop_Assistant")
				LOAD_SHOP_INTRO_ASSETS(CLOTHES_SHOP_M_04_HW)
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_ShopBlip) > DEFAULT_CUTSCENE_UNLOAD_DIST
			OR IS_PLAYER_CHANGING_CLOTHES()
				Reset_Cutscene_Ped_Variation_Streaming(sCutscenePedVariationRegister)
				REMOVE_CUTSCENE()
				UNLOAD_SHOP_INTRO_ASSETS(CLOTHES_SHOP_M_04_HW)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF (mission_substage > 3 OR IS_PLAYER_IN_SHOP(CLOTHES_SHOP_M_04_HW) )
	AND mission_substage < 6
	AND mission_substage != STAGE_EXIT
		IF NOT bPlayerIsBlockedForLeadIn
			BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
			bPlayerIsBlockedForLeadIn = TRUE
		ENDIF
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
	ELSE
		IF bPlayerIsBlockedForLeadIn
			CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
			SET_MAX_WANTED_LEVEL(5)
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			bPlayerIsBlockedForLeadIn = FALSE
		ENDIF
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY
			b_CanFailForNotEnoughFunds = FALSE
		
			IF IS_SHOP_OPEN_FOR_BUSINESS(CLOTHES_SHOP_M_04_HW)
				TRIGGER_EVENT(mef_ambient_receptionist)
				bSideEntranceObjective		= FALSE
				bLestersDoorLocked			= FALSE
				
				SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_LOCKED)
				
				SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 120)
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "1. Get clothes", FALSE, FALSE)
				
				SET_SHOP_DIALOGUE_IS_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
				SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
				
				Trigger_Event(mef_manage_bag)
				
				bPlayerIsBlockedForLeadIn = FALSE
				mission_substage++
			ENDIF
			
			SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, FALSE)
		BREAK
		CASE 1		
			IF IS_PLAYER_AT_ANGLED_AREA_ANY_MEANS(locates_data, 
					v_ShopBlip,
					<<128.484558,-207.428040,53.576126>>, <<127.086388,-211.525604,56.738232>>, 4.937500,
					TRUE, "LES1A_1", TRUE, TRUE)
			OR (IS_PLAYER_IN_SHOP(CLOTHES_SHOP_M_04_HW) AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0))
				
				CLEAR_MISSION_LOCATE_STUFF(locates_data)
				
				IF IS_AUDIO_SCENE_ACTIVE("LESTER_1A_DRIVE_TO_STORE")
					STOP_AUDIO_SCENE("LESTER_1A_DRIVE_TO_STORE")
				ENDIF
				START_AUDIO_SCENE("LESTER_1A_BUY_CLOTHES")
				
				// if player does not have enoug cash, send the text and credit their account
				IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < i_min_required_funds
					INT iMoneyToGive
					iMoneyToGive = CLAMP_INT(200 - GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()), 100, 200)
					SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_LESTER, "LES1A_TXT1", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
					CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_LESTER, iMoneyToGive)
				ENDIF
				
				mission_substage++
			
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_AUDIO_SCENE_ACTIVE("LESTER_1A_DRIVE_TO_STORE")
						START_AUDIO_SCENE("LESTER_1A_DRIVE_TO_STORE")
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		CASE 2
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 2.0)
					mission_substage++
				ENDIF
			ELSE
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF IS_PLAYER_AT_LOCATION_ON_FOOT(locates_data, 
				<<126.0279, -222.9916, 53.5578>>, << 8.0, 9.0, LOCATE_SIZE_HEIGHT>>, FALSE,
				"", TRUE, TRUE)
			
				CLEAR_MISSION_LOCATE_STUFF(locates_data)
				CLEAR_PRINTS()
				
				IF NOT IS_GAMEPLAY_HINT_ACTIVE()
				AND DOES_ENTITY_EXIST(peds[mpf_shop])
				AND NOT IS_PED_INJURED(peds[mpf_shop])
					SET_GAMEPLAY_ENTITY_HINT(peds[mpf_shop], <<0,0,0>>, TRUE, -1)
					SET_GAMEPLAY_HINT_FOV(30.0)
					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.7)
					SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.0)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.02)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.1)
				ENDIF
				
				TASK_FOLLOW_NAV_MESH_TO_COORD( PLAYER_PED_ID(), <<125.9931, -221.0858, 53.5578>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT, DEFAULT, 193.8799 )
				 
				mission_substage++
			
			ENDIF
		BREAK
		CASE 4
			IF CREATE_CONVERSATION(sConvo, str_dialogue, "LES1A_MCS0LI", CONV_PRIORITY_HIGH)
				mission_substage++
			ENDIF
		BREAK
		CASE 5
			IF IS_CUTSCENE_ACTIVE()
			AND HAS_CUTSCENE_LOADED()
			AND NOT IS_PED_INJURED(peds[mpf_shop])
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
				REPLAY_RECORD_BACK_FOR_TIME(7.0, 0.0)
			
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), 	"Michael", 			CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_shop], 	"Shop_Assistant", 	CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				START_CUTSCENE()
				SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
				
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
			
				strShopHelp = ""
			
				mission_substage++
			ENDIF
		BREAK
		CASE 6
			IF IS_CUTSCENE_PLAYING()
			
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT(TRUE)
				ENDIF
				
				IF bPlayerIsBlockedForLeadIn
					CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
					SET_MAX_WANTED_LEVEL(5)
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
				ENDIF
				
				IF HAS_CUTSCENE_CUT_THIS_FRAME()
				
					// Help text 1
					IF GET_CUTSCENE_TIME() > 4000 AND GET_CUTSCENE_TIME() < 5000
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						strShopHelp = "LS1_SHOPHLP_1"
						
					ELIF GET_CUTSCENE_TIME() > 7500 AND GET_CUTSCENE_TIME() < 13000
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						strShopHelp = ""
					
					// Help text 2
					ELIF GET_CUTSCENE_TIME() > 13000 AND GET_CUTSCENE_TIME() < 15000
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						strShopHelp = "LS1_SHOPHLP_2"
					// Help text 3
					ELIF GET_CUTSCENE_TIME() > 18000 AND GET_CUTSCENE_TIME() < 20000
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						strShopHelp = "LS1_SHOPHLP_3"
					ELSE
						IF GET_CUTSCENE_TIME() > 22000
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
							strShopHelp = ""
						ENDIF
					ENDIF
				
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(strShopHelp)
					DISPLAY_SHOP_INTRO_MESSAGE(CLOTHES_SHOP_M_04_HW, strShopHelp)
				ELSE
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				PRINT_NOW("LES1A_2", DEFAULT_GOD_TEXT_TIME, 1)
				
				SAFE_BLIP_COORD(blip_clothes[0], v_GilletBlipCoord)
				SAFE_BLIP_COORD(blip_clothes[1], v_ShortsBlipCoord)

				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
				
				UNLOAD_SHOP_INTRO_ASSETS(CLOTHES_SHOP_M_04_HW)
				
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
				
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
				
				IF IS_SCREEN_FADED_OUT()
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
				ENDIF
				
				b_CanFailForNotEnoughFunds = TRUE
				iShopCutsceneTimer = GET_GAME_TIMER() + 3000
				
				strShopHelp = ""
				mission_substage++
			ENDIF
		BREAK
		CASE 7
			IF IS_PLAYER_IN_SHOP(CLOTHES_SHOP_M_04_HW)
			
				IF GET_GAME_TIMER() > iShopCutsceneTimer
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
				ENDIF
				
				BOOL bHasTorso, bHasLegs, bChangedTorso, bChangedLegs
				PED_COMP_NAME_ENUM playerTorso
				PED_COMP_NAME_ENUM playerLegs
				PED_COMP_TYPE_ENUM eCompTypeLastPreviewed
				PED_COMP_NAME_ENUM eCompNameLastPreviewed
				
				IF bWasBrowsingClothesLastFrame
				
					CPRINTLN(DEBUG_MIKE, "**** FINISHED BROWSING IN SHOP ****")
				
					playerTorso 	= GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO)
					playerLegs 		= GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS)
					
					GET_CLOTHING_ITEM_PLAYER_LAST_PREVIEWED(CHAR_MICHAEL, eCompTypeLastPreviewed, eCompNameLastPreviewed)
					
					CPRINTLN(DEBUG_MIKE, "**** TORSO: ", playerTorso, " ****")
					CPRINTLN(DEBUG_MIKE, "**** LEGS: ", playerLegs, " ****")
					
					IF playerTorsoPrev != playerTorso
						bChangedTorso = TRUE
					ENDIF
					
					IF playerLegsPrev != playerLegs
						bChangedLegs = TRUE
					ENDIF
				
					// Check TORSO
					IF playerTorso = TORSO_P0_GILET_0
					OR playerTorso = TORSO_P0_GILET_1
					OR playerTorso = TORSO_P0_GILET_2
					OR playerTorso = TORSO_P0_GILET_3
					OR playerTorso = TORSO_P0_GILET_4
					OR playerTorso = TORSO_P0_GILET_5
						CPRINTLN(DEBUG_MIKE, "**** Removing Torso Blip ****")
						SAFE_REMOVE_BLIP(blip_clothes[0])
						bHasTorso = TRUE
					ELIF NOT DOES_BLIP_EXIST(blip_clothes[0])
						SAFE_BLIP_COORD(blip_clothes[0], v_GilletBlipCoord)
						CPRINTLN(DEBUG_MIKE, "**** Adding Torso Blip ****")
					ENDIF 
					
					// CHECK LEGS
					IF playerLegs = LEGS_P0_CARGO_SHORTS_0
					OR playerLegs = LEGS_P0_CARGO_SHORTS_1
					OR playerLegs = LEGS_P0_CARGO_SHORTS_2
					OR playerLegs = LEGS_P0_CARGO_SHORTS_3
					OR playerLegs = LEGS_P0_CARGO_SHORTS_4
					OR playerLegs = LEGS_P0_LONG_SHORTS_0
					OR playerLegs = LEGS_P0_LONG_SHORTS_1
					OR playerLegs = LEGS_P0_LONG_SHORTS_2
					OR playerLegs = LEGS_P0_LONG_SHORTS_3
					OR playerLegs = LEGS_P0_LONG_SHORTS_4
					OR playerLegs = LEGS_P0_LONG_SHORTS_5
					OR playerLegs = LEGS_P0_LONG_SHORTS_6
					OR playerLegs = LEGS_P0_LONG_SHORTS_7
					OR playerLegs = LEGS_P0_LONG_SHORTS_8
					OR playerLegs = LEGS_P0_LONG_SHORTS_9
					OR playerLegs = LEGS_P0_LONG_SHORTS_10
					OR playerLegs = LEGS_P0_LONG_SHORTS_11
					OR playerLegs = LEGS_P0_LONG_SHORTS_12
					OR playerLegs = LEGS_P0_LONG_SHORTS_13
					OR playerLegs = LEGS_P0_LONG_SHORTS_14
					OR playerLegs = LEGS_P0_LONG_SHORTS_15
					OR playerLegs = LEGS_P0_YOGA_0
					OR playerLegs = LEGS_P0_YOGA_1
					OR playerLegs = LEGS_P0_YOGA_2
					OR playerLegs = LEGS_P0_YOGA_3
					OR playerLegs = LEGS_P0_YOGA_4
					OR playerLegs = LEGS_P0_YOGA_5
					OR playerLegs = LEGS_P0_YOGA_6
					OR playerLegs = LEGS_P0_YOGA_7
					OR playerLegs = LEGS_P0_YOGA_8
					OR playerLegs = LEGS_P0_YOGA_9
					OR playerLegs = LEGS_P0_YOGA_10
					OR playerLegs = LEGS_P0_YOGA_11
					OR playerLegs = LEGS_P0_YOGA_12
					OR playerLegs = LEGS_P0_YOGA_13
					OR playerLegs = LEGS_P0_YOGA_14	
					OR playerLegs = LEGS_P0_YOGA_15	
						CPRINTLN(DEBUG_MIKE, "**** Removing Leg Blip ****")
						SAFE_REMOVE_BLIP(blip_clothes[1])
						bHasLegs = TRUE
					ELIF NOT DOES_BLIP_EXIST(blip_clothes[1])
						SAFE_BLIP_COORD(blip_clothes[1], v_ShortsBlipCoord)
						CPRINTLN(DEBUG_MIKE, "**** Adding Leg Blip ****")
					ENDIF
					
					// Changed something and outfit was completed
					IF NOT DOES_BLIP_EXIST(blip_clothes[0])
					AND NOT DOES_BLIP_EXIST(blip_clothes[1])
					AND ((bChangedTorso AND eCompTypeLastPreviewed = COMP_TYPE_TORSO)
					OR (bChangedLegs AND eCompTypeLastPreviewed = COMP_TYPE_LEGS))
					
						IF IS_STRING_NULL_OR_EMPTY(strShopHelp)
							strShopHelp = "LES1A_FULL"
						ENDIF
					
					// Changed a torso component
					ELIF bChangedTorso AND eCompTypeLastPreviewed = COMP_TYPE_TORSO
						// Incorrect
						IF DOES_BLIP_EXIST(blip_clothes[0])
							IF IS_STRING_NULL_OR_EMPTY(strShopHelp)
								strShopHelp = "LES1A_UP_N"
							ENDIF
						// Correct
						ELSE
							IF IS_STRING_NULL_OR_EMPTY(strShopHelp)
								strShopHelp = "LES1A_UP_Y"
							ENDIF
						ENDIF
					
					// Changed a leg componeent
					ELIF bChangedLegs AND eCompTypeLastPreviewed = COMP_TYPE_LEGS
						// Incorrect
						IF DOES_BLIP_EXIST(blip_clothes[1])
							IF IS_STRING_NULL_OR_EMPTY(strShopHelp)
								strShopHelp = "LES1A_LOW_N"
							ENDIF
						// Correct
						ELSE
							IF IS_STRING_NULL_OR_EMPTY(strShopHelp)
								strShopHelp = "LES1A_LOW_Y"
							ENDIF
						ENDIF
						
					ENDIF
					
					// Changed something and it was wrong
					IF (bChangedTorso AND DOES_BLIP_EXIST(blip_clothes[0]) AND eCompTypeLastPreviewed = COMP_TYPE_TORSO)
					OR (bChangedLegs AND DOES_BLIP_EXIST(blip_clothes[1]) AND eCompTypeLastPreviewed = COMP_TYPE_LEGS)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP("LES1A_H2", 3000)
						ENDIF
					ENDIF
					
				ELIF NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CLOTHES_SHOP_M_04_HW)
				
					// Update legs while not browsing 
					playerTorsoPrev 	= GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO)
					playerLegsPrev 		= GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS)
					//CPRINTLN(DEBUG_MIKE, "**** UPDATE PREV TORSO TO: ", playerTorsoPrev, " ****")
					//CPRINTLN(DEBUG_MIKE, "**** UPDATE PREV LEGS TO: ", playerLegsPrev, " ****")
					
					IF NOT IS_STRING_NULL_OR_EMPTY(strShopHelp)
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, str_dialogue, strShopHelp, CONV_PRIORITY_HIGH)
								strShopHelp = ""
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF

				// Update the browsing last frame var
				IF NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CLOTHES_SHOP_M_04_HW)
					bWasBrowsingClothesLastFrame = FALSE
				ELSE
					bWasBrowsingClothesLastFrame = TRUE
				ENDIF

				// once bought remove the blip and block the player from buying other clothes
				IF NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CLOTHES_SHOP_M_04_HW)
				AND bHasTorso AND bHasLegs
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, TRUE)
					bObtainedClothing = TRUE
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(locates_data)
					
					// Store costume for replays
					g_replay.iReplayInt[0] = enum_to_int(playerTorso)
					g_replay.iReplayInt[1] = enum_to_int(playerLegs)
					
					CLEAR_HELP()
					Mission_Set_Stage(msf_st2_go_to_offices)
				ENDIF
			ENDIF
				
		BREAK
		CASE STAGE_EXIT
		BREAK
	ENDSWITCH
	
	
	IF IS_VALID_INTERIOR(interior_lesters)
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != interior_lesters
			IF NOT bLestersDoorLocked
				IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LESTER_F)) = DOORSTATE_LOCKED
					IF ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LESTER_F))) < 0.1
						g_QuickSaveDisabledByScript = FALSE
						bLestersDoorLocked = TRUE
					ENDIF
				ELSE
					SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_LOCKED)
				ENDIF
			ENDIF
		ELSE
			DISABLE_PLAYERS_WEAPONS_THIS_FRAME()
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_JUMP )
		
			IF NOT IS_PED_INJURED(peds[mpf_lester])
				IF i_CoughCounter < 2
				AND GET_GAME_TIMER() > i_CoughTimer
					ADD_PED_FOR_DIALOGUE(sConvo, 8, peds[mpf_lester], "Lester")
					PLAY_PAIN(peds[mpf_lester], AUD_DAMAGE_REASON_COUGH)
					i_CoughTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 5000)
					i_CoughCounter++
				ELSE
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() > i_CoughTimer
						IF CREATE_CONVERSATION(sConvo, str_dialogue, "LES1A_LEAVE", CONV_PRIORITY_HIGH)
							i_CoughTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 15000)
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
			SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_UNLOCKED)
			bLestersDoorLocked 				= FALSE
			g_QuickSaveDisabledByScript 	= TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(peds[mpf_lester])
	AND NOT IS_PED_INJURED(peds[mpf_lester])
	AND (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(peds[mpf_lester])) > 900 // 30 squared 
	OR bLestersDoorLocked)
		Unload_Asset_Anim_Dict(sAssetData, "MissLester1ALeadInOut")
		IF IS_ENTITY_ATTACHED(objects[mof_wheelchair].id)
			DETACH_ENTITY(objects[mof_wheelchair].id)
		ENDIF
		DELETE_OBJECT(objects[mof_wheelchair].id)
		DELETE_PED(peds[mpf_lester])
		// 1979116
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_LESTER_1)
	ENDIF
	
ENDPROC


PROC ST2_Go_To_Lifeinvader()
	
	VECTOR 	v_offices_roadside 		= << -1057.9395, -224.8114, 37.1567 >> //<< -1053.5070, -215.3025, 36.7002 >>
	
	IF NOT IS_INTERIOR_SCENE()	
	
		IF IS_AUDIO_SCENE_ACTIVE("LESTER_1A_BUY_CLOTHES")
			STOP_AUDIO_SCENE("LESTER_1A_BUY_CLOTHES")
		ENDIF

	ENDIF
	
	SWITCH mission_substage
		CASE STAGE_ENTRY
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "2. Goto Lifeinvader offices", FALSE, FALSE)
			Trigger_Event(mef_lester_phone_call)
			
			bBringVehicleToHalt 	= FALSE
			bExitStateCam 			= FALSE
			bExitStateMike			= FALSE
			iLeaveShopConvo			= 0
			
			SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, TRUE)
			
			mission_substage++
		BREAK
		CASE 1

			SWITCH iLeaveShopConvo
				CASE 0
					IF NOT IS_STRING_NULL_OR_EMPTY(strShopHelp)
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, str_dialogue, strShopHelp, CONV_PRIORITY_HIGH)
								strShopHelp = ""
								iLeaveShopConvo++
							ENDIF
						ENDIF
					ELSE
						iLeaveShopConvo++
					ENDIF
				BREAK
				CASE 1
					IF IS_PLAYER_IN_SHOP(CLOTHES_SHOP_M_04_HW)
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, str_dialogue, "LES1A_LUCK", CONV_PRIORITY_HIGH)
								iLeaveShopConvo++
							ENDIF
						ENDIF
					ELSE
						iLeaveShopConvo = 3
					ENDIF
				BREAK
				CASE 2
					IF IS_PLAYER_IN_SHOP(CLOTHES_SHOP_M_04_HW)
						IF IS_PLAYER_IN_SHOP(CLOTHES_SHOP_M_04_HW)
							IF IS_SAFE_TO_START_CONVERSATION()
							AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
								PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_BYE", SPEECH_PARAMS_FORCE)
								iLeaveShopConvo++
							ENDIF
						ENDIF
					ELSE
						iLeaveShopConvo = 3
					ENDIF
				BREAK
				CASE 3
					IF IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					AND IS_SAFE_TO_DISPLAY_GODTEXT()
						CLEAR_BIT(locates_data.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
						iLeaveShopConvo++
					ENDIF
				BREAK
			ENDSWITCH
			
			TEXT_LABEL_15 strObjective
			IF iLeaveShopConvo > 2
			OR (NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON())
				IF NOT bSideEntranceObjective
					strObjective	= "LES1A_3"
				ELSE
					strObjective	= "LES1A_4"
				ENDIF
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_offices_foot) < DEFAULT_CUTSCENE_LOAD_DIST
					REQUEST_CUTSCENE("LES_1A_MCS_1")
					Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, PLAYER_PED_ID(), "Michael")
					Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Michael", PED_COMP_SPECIAL2, 9, 0)
					
					Load_Asset_Model(sAssetData, mod_hipster_main)
					Load_Asset_Interior(sAssetData, interior_offices)
					Load_Asset_Waypoint(sAssetData, str_wp_engineer_route)
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_offices_foot) > DEFAULT_CUTSCENE_UNLOAD_DIST
					REMOVE_CUTSCENE()
					Unload_Asset_Model(sAssetData, mod_hipster_main)
					Unload_Asset_Interior(sAssetData, interior_offices)
					Unload_Asset_Waypoint(sAssetData, str_wp_engineer_route)
				ENDIF
			ENDIF
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT bSideEntranceObjective
			
				IF IS_PLAYER_AT_ANGLED_AREA_IN_ANY_VEHICLE(locates_data, v_offices_roadside,
					<<-1046.286865,-219.515488,36.808811>>, <<-1068.808472,-231.234207,38.173355>>, 5.312500, TRUE,
					strObjective, "", TRUE)
				
					CLEAR_BIT(locates_data.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
					bSideEntranceObjective = TRUE
					bBringVehicleToHalt = TRUE
					
				ELSE
					IF NOT IS_AUDIO_SCENE_ACTIVE("LESTER_1A_DRIVE_TO_LIFEINVADER")
						START_AUDIO_SCENE("LESTER_1A_DRIVE_TO_LIFEINVADER")
					ENDIF
				ENDIF

			ELSE
			
				IF IS_PLAYER_AT_ANGLED_AREA_ON_FOOT(locates_data, v_offices_foot, 
					<<-1043.331787,-231.809570,37.764164>>, <<-1045.451904,-224.953644,40.952034>>, 4.625000, FALSE,
					strObjective, TRUE, TRUE, RADAR_TRACE_INVALID, FALSE)

					REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)

					mission_substage++
					
				ELSE
					IF NOT bSideEntranceObjective
					AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_offices_foot) <= 900 // 30 squared
						CLEAR_BIT(locates_data.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
						bSideEntranceObjective = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bBringVehicleToHalt
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 4.0, 1, 0.0)
						bBringVehicleToHalt = FALSE
					ENDIF
				ELSE
					bBringVehicleToHalt = FALSE
				ENDIF
			ENDIF
			
			IF bSideEntranceObjective
				IF IS_AUDIO_SCENE_ACTIVE("LESTER_1A_DRIVE_TO_LIFEINVADER")
					STOP_AUDIO_SCENE("LESTER_1A_DRIVE_TO_LIFEINVADER")
				ENDIF
			ENDIF
		BREAK
		CASE 2
			// cut scene has loaded, create ped and start smokers cutscene
			IF HAS_CUTSCENE_LOADED()
			AND GET_IS_WAYPOINT_RECORDING_LOADED(str_wp_engineer_route)
				Kill_Event(mef_lester_phone_call)
				Load_Asset_AnimDict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
				
				REGISTER_ENTITY_FOR_CUTSCENE(null, "Life_invader_Engineer", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, mod_hipster_main)
				START_CUTSCENE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)

				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF IS_CUTSCENE_PLAYING()
			
				VEHICLE_INDEX vehTemp
				vehTemp = GET_PLAYERS_LAST_VEHICLE()
				
				// Use the size limit resolve function with a larger area for vehicles that are too big
				IF DOES_ENTITY_EXIST(vehTemp)
				AND IS_ENTITY_TOO_BIG(vehTemp, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()) 
				
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1048.717651,-212.836868,36.700832>>, <<-1027.840088,-233.548004,39.983681>>, 22.750000, 
						<<-1053.5105, -222.5092, 37.0833>>, 241.3186, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				ELSE
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1043.131470,-221.360596,36.801144>>, <<-1034.720215,-237.289597,42.920845>>, 16.812500,
						<<-1053.5105, -222.5092, 37.0833>>, 241.3186)
				ENDIF
				
				CLEAR_AREA_OF_PROJECTILES(v_offices_foot, 20.0)
				CLEAR_AREA_OF_PEDS(v_offices_foot, 20.0)
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			
			// grab the ped index from the cutscene and set the correct variation and dialogue stuff
			IF NOT DOES_ENTITY_EXIST(peds[mpf_smoker])
				ENTITY_INDEX cutscene_entity
				cutscene_entity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Life_invader_Engineer", mod_hipster_main)
				IF DOES_ENTITY_EXIST(cutscene_entity)
					peds[mpf_smoker] = GET_PED_INDEX_FROM_ENTITY_INDEX(cutscene_entity)
					mission_substage++
				ENDIF
			ENDIF
		BREAK
		CASE 5
		
//			IF IS_CUTSCENE_PLAYING()
//				IF NOT bReplayRecordEventStarted
//					IF GET_CUTSCENE_TIME() >= 50500
//					AND GET_CUTSCENE_TIME() < 57500
//						REPLAY_START_EVENT()
//						bReplayRecordEventStarted = TRUE
//					ENDIF
//				ELSE
//					IF GET_CUTSCENE_TIME() >= 57500
//						REPLAY_STOP_EVENT()
//						bReplayRecordEventStarted = FALSE
//					ENDIF
//				ENDIF
//			ENDIF
		
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices
				IF NOT IS_AUDIO_SCENE_ACTIVE("LESTER_1A_FOLLOW_PROGRAMMER")
					START_AUDIO_SCENE("LESTER_1A_FOLLOW_PROGRAMMER")
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				CPRINTLN(DEBUG_MIKE, "LES1A: CAMERA EXITED @ ", GET_GAME_TIMER())
				bExitStateCam = TRUE
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Life_invader_Engineer")
				VECTOR vWpCoord
				WAYPOINT_RECORDING_GET_COORD(str_wp_engineer_route, 2, vWpCoord)
				SET_ENTITY_COORDS(peds[mpf_smoker], vWpCoord)
				SET_ENTITY_HEADING(peds[mpf_smoker], 113.2956)
			
				ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_smoker], "LIEngineer")
				Trigger_Event(mef_engineer_follow)				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				
				CPRINTLN(DEBUG_MIKE, "LES1A: MICHAEL EXITED @ ", GET_GAME_TIMER())
				SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
				SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), FALSE)
				
				// mikes state
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 4000, 115.2213, FALSE)
				
				IF WAS_CUTSCENE_SKIPPED()
				AND GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
					SET_ENTITY_COORDS( PLAYER_PED_ID(), <<-1046.6028, -232.4546, 38.0140>> )
					SET_ENTITY_HEADING( PLAYER_PED_ID(), 117.5673 )
				ENDIF
				
				REMOVE_PARTICLE_FX_IN_RANGE(<< -1047.5804, -230.8085, 39.8967 >>, 10.0)
				
				bExitStateMike 				= TRUE
				bDisableConspicuousBehavior = TRUE
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
				
			ENDIF
			
			IF bExitStateMike AND NOT bExitStateCam 
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(115.2213 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			IF bExitStateCam AND bExitStateMike
				mission_substage++
			ENDIF
		BREAK
		CASE 6
			IF HAS_CUTSCENE_FINISHED()
				REMOVE_CUTSCENE()
				Mission_Set_Stage(msf_st3_go_to_computer)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC ST3_Walkthrough_Offices()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "3. Follow Programmer", FALSE, FALSE)
		
			// load the sit down at pc cutscene
			REQUEST_CUTSCENE("LES_1A_MCS_2")
			Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, PLAYER_PED_ID(), "Michael")
			
			bStreamCutScene = FALSE
			
			// load ambient models and animations
			Load_Asset_Model(sAssetData, mod_hipster_m)
			Load_Asset_Model(sAssetData, mod_hipster_f)
			Load_Asset_Model(sAssetData, mod_hipster_f_heel)
			Load_Asset_Model(sAssetData, IG_JAY_NORRIS)
			Load_Asset_Model(sAssetData, PROP_PAPER_BALL)
			Load_Asset_Model(sAssetData, Prop_Off_Chair_01)
			Load_Asset_Model(sAssetData, PROP_MONITOR_01D)
			
			Load_Asset_Audiobank(sAssetData, "COMPUTERS")
			Load_Asset_Audiobank(sAssetData, "Lester1A_01")
			
			Load_Asset_AnimDict(sAssetData, anim_dict_paper_throw)
			Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_main)
			Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_intro)	// boardroom with jay norris
			Load_Asset_AnimDict(sAssetData, anim_dict_interview_1st)
			Load_Asset_AnimDict(sAssetData, anim_dict_airguitar_1st)
			Load_Asset_AnimDict(sAssetData, anim_dict_airguitar_1st_exit)
			
			Load_Asset_AssistedLine(sAssetData, al_stairs_up)
			
			Load_Asset_Audiobank(sAssetData, "Lester1A_Qub3d")
			
			Load_Asset_Scaleform(sAssetData, "desktop_pc", mov)
			
		
			Kill_Event(mef_manage_bag)
		
			IF NOT IS_PED_INJURED(peds[mpf_smoker])
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(peds[mpf_smoker], FALSE)
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			ENDIF
			
			SET_DOOR_STATE(DOORNAME_LINVADER_OFFICE_UP, DOORSTATE_UNLOCKED)
			
			PRINT_NOW("LES1A_5",DEFAULT_GOD_TEXT_TIME,1)
			blip_Objective = CREATE_BLIP_FOR_PED(peds[mpf_smoker],FALSE)
			bReadyToStartWalkChatConvo = TRUE

			REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0)

			mission_substage++
		BREAK
		CASE 1	
			// trigger ambient ped scenes
			TRIGGER_EVENT(mef_ambient_interview)
			TRIGGER_EVENT(mef_ambient_office)
			TRIGGER_EVENT(mef_ambient_boardroom)
			TRIGGER_EVENT(mef_ambient_paper_throw)
			Trigger_Event(mef_ambient_dialogue)
			
			IF NOT DOES_ENTITY_EXIST(objects[mof_smokers_chair].id)
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
					objects[mof_smokers_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
				ENDIF
			ENDIF

			IF NOT IS_PED_INJURED(peds[mpf_smoker])
				// As player gets there kick off the cutscene
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1064.362793,-240.885773,43.041534>>, <<-1062.236572,-245.067993,44.896278>>, 1.500000)
					IF NOT IS_PED_INJURED(peds[mpf_smoker])
					AND HAS_CUTSCENE_LOADED()
						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

						REMOVE_BLIP(blip_Objective)				
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						Kill_Event(mef_engineer_follow)
						
						REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_smoker], "Life_invader_Engineer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(objects[mof_smokers_chair].id, "LifeInvad_Chair", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PROP_OFF_CHAIR_01)
						START_CUTSCENE()
						REPLAY_RECORD_BACK_FOR_TIME(6.0, 0.0)
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
								
						IF IS_AUDIO_SCENE_ACTIVE("LESTER_1A_FOLLOW_PROGRAMMER")
							STOP_AUDIO_SCENE("LESTER_1A_FOLLOW_PROGRAMMER")
						ENDIF
						
						INT i
						FOR i = 0 TO iUsablePopups-1
							IF sPopups[i].iType != -1
							AND sPopups[i].iSfxLoop != -1
							AND sPopups[i].bSfxPlaying
								SET_VARIABLE_ON_SOUND(sPopups[i].iSfxLoop, "State", 1)
								CPRINTLN(DEBUG_MIKE, "MINI GAME porn popups sfx volume set to 1 for popup: ", i)
							ENDIF
						ENDFOR
						
						// CLEAN UP OF AMBIENT CRAP WHILE SCALEFORM IS IN USE
						Reset_Event(mef_ambient_interview)
						IF DOES_ENTITY_EXIST(peds[mpf_hr]) 				DELETE_PED(peds[mpf_hr]) ENDIF
						IF DOES_ENTITY_EXIST(peds[mpf_interviewee_m]) 	DELETE_PED(peds[mpf_interviewee_m]) ENDIF
						IF DOES_ENTITY_EXIST(peds[mpf_interviewee_f]) 	DELETE_PED(peds[mpf_interviewee_f]) ENDIF
						IF DOES_ENTITY_EXIST(peds[mpf_gamer]) 			DELETE_PED(peds[mpf_gamer]) ENDIF
						
						// begin unloading assets and stopping synched scenes
						Unload_Asset_Model(sAssetData, IG_JAY_NORRIS)
						Unload_Asset_Model(sAssetData, Prop_Off_Chair_01)
						Unload_Asset_Anim_Dict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
						Unload_Asset_AssistedLine(sAssetData, al_stairs_up)

						Unload_Asset_Anim_Dict(sAssetData, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@base")
						Unload_Asset_Anim_Dict(sAssetData, "AMB@WORLD_HUMAN_STAND_IMPATIENT@MALE@No_Sign@idle_a")
					
						b_MiniGameScaleformSetUp = FALSE
						Mission_Set_Stage(msf_st4_mini_game)
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT bStreamCutScene
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL2,0,0)
			bStreamCutScene = TRUE
		ENDIF
	ENDIF
	
ENDPROC


PROC ST4_Mini_Game()
	INT i
	HIDE_HUD_AND_RADAR_THIS_FRAME() 
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	// Get player input and pass to scaleform
	minigame_scaleform_flags scaleform_return
	
	IF mission_substage >= 1 AND mission_substage < 5
	
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF bCursorSnap = TRUE
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_SNAP_SPEED")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( 0.0 )
				END_SCALEFORM_MOVIE_METHOD()
				bCursorSnap = FALSE
			ENDIF
		ELSE
			IF bCursorSnap = FALSE
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_SNAP_SPEED")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( 500.0 )
				END_SCALEFORM_MOVIE_METHOD()
				bCursorSnap = TRUE
			ENDIF
		ENDIF
		
		IF IS_USING_CURSOR( FRONTEND_CONTROL )
		
			FLOAT f_PCMousePosX, f_PCMousePosY

			// No snap if using mouse on PC.
					
			f_PCMousePosX = GET_CONTROL_UNBOUND_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
			f_PCMousePosY = GET_CONTROL_UNBOUND_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )
			
			IF f_PCMousePosPrevX != f_PCMousePosX
			OR f_PCMousePosPrevY != f_PCMousePosY
			
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_CURSOR")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( f_PCMousePosX )
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( f_PCMousePosY )
				END_SCALEFORM_MOVIE_METHOD()
				
				fInputAccum += TIMESTEP()	// used to trigger the next help text
				
			ENDIF
			
			f_PCMousePosPrevX = f_PCMousePosX
			f_PCMousePosPrevY = f_PCMousePosY
			
					
		ELSE
			
			INT iPadLeftX,iPadLeftY,iPadRightX,iPadRightY
			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iPadLeftX,iPadLeftY,iPadRightX,iPadRightY)
						
			IF iPadLeftX != 0
			OR iPadLeftY != 0
				
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "MOVE_CURSOR")
					// Adjusted input to be framerate dependant for smoothness
	//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( f_CursorChangeX * 0.3)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( TO_FLOAT( iPadLeftX ) * 5 * TIMESTEP())
	//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( f_CursorChangeY * 0.3)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( TO_FLOAT( iPadLeftY ) * 5 * TIMESTEP())
				END_SCALEFORM_MOVIE_METHOD()
				
				fInputAccum += TIMESTEP()	// used to trigger the next help text
			
			ENDIF
		
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//		
//		// Debug functionality to test directly passing in a cursor position to the scaleform
//			
//			FLOAT fCursorPosX, fCursorPosY
//			
//			IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT )
//				
//				fCursorPosX = 0.25
//			
//			ELIF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT )
//			
//				fCursorPosX = 0.75
//				
//			ENDIF
//				
//			IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP )
//			
//				fCursorPosY = 0.25
//				
//			ELIF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN )
//			
//				fCursorPosY = 0.75
//			
//			ENDIF
//			
//			IF fCursorPosX != 0.0
//			OR fCursorPosY != 0.0
//			
//				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_CURSOR")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( fCursorPosX )
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT( fCursorPosY )
//				END_SCALEFORM_MOVIE_METHOD()
//			
//			ENDIF
//			
//		#ENDIF
		
		
		#IF IS_DEBUG_BUILD
			i_debug_PopupsOpen = GET_POPUPS_OPEN()
		#ENDIF
		
		// Process mouse click return
		IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(minigame_return_index)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				CDEBUG1LN(DEBUG_MIKE, "Scaleform: Calling SET_INPUT_EVENT on mouse click")
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_INPUT_EVENT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
				minigame_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MIKE, "Scaleform: Obtaininging mouse click return value")
			scaleform_return = int_to_enum(minigame_scaleform_flags, GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(minigame_return_index))
			CDEBUG1LN(DEBUG_MIKE, "Scaleform: Returned: ", scaleform_return)

			IF iPopupHelp = 0
			AND fInputAccum >= 0.25
				CLEAR_HELP()
				iPopupHelp = 1
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1,"COMPUTERS_MOUSE_CLICK")
			INFORM_MISSION_STATS_OF_INCREMENT(LES1A_MOUSE_CLICKS)
		ENDIF
		
		// Process Last popup return
		IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(last_popup_return_index)
			CDEBUG1LN(DEBUG_MIKE, "Scaleform: Obtaininging last popup retrun value")
			INT iLast = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(last_popup_return_index)
			CDEBUG1LN(DEBUG_MIKE, "Scaleform: Returned: ", iLast)
			
			IF iLast != -1
				iLastPopupClosed = iLast
				sPopups[iLast].bClosed = TRUE
				
				// Stop the sfx if it has one
				IF sPopups[iLast].bSfxPlaying
				AND sPopups[iLast].iSfxLoop != -1
					STOP_SOUND(sPopups[iLast].iSfxLoop)
					CPRINTLN(DEBUG_MIKE, "CLOSE POPUP STOP POPUP LOOP SFX:", iLast)
					sPopups[iLast].bSfxPlaying = FALSE
					iPopupsSFXPlaying--
				ENDIF
				
				// look for if icon has been revealed
				IF iPopupHelp < 2
					IF sPopups[0].bClosed 
						CLEAR_HELP()
						IF GET_POPUPS_OPEN() = 0
							iPopupHelp = 3
						ELSE
							iPopupHelp = 2
						ENDIF
					ENDIF
				ENDIF
				
				PLAY_SOUND_FRONTEND(-1,"CLOSE_WINDOW", "LESTER1A_SOUNDS")
				INFORM_MISSION_STATS_OF_INCREMENT(LES1A_POPUPS_CLOSED)
			ENDIF
		ENDIF
	ENDIF
	
	IF mission_substage >= 1
		IF NOT bSwappedMonitorToClean
		AND HAS_MODEL_LOADED(PROP_MONITOR_01C)
			IF bSwappedMonitorToPopups
				CREATE_MODEL_SWAP(v_engineer_monitor, 1.0, PROP_MONITOR_01D, PROP_MONITOR_01C, TRUE)
			ELSE
				CREATE_MODEL_SWAP(v_engineer_monitor, 1.0, PROP_MONITOR_LI, PROP_MONITOR_01C, TRUE)
			ENDIF
			Unload_Asset_Model(sAssetData, PROP_MONITOR_01C)
			bSwappedMonitorToClean = TRUE
		ENDIF
	ENDIF
	
	SWITCH mission_substage
		CASE STAGE_ENTRY
			IF NOT b_MiniGameScaleformSetUp
			AND HAS_SCALEFORM_MOVIE_LOADED(mov)
			
				// set up icons
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_PROGRAM")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcons[0])
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LES1A_ANTVI")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_PROGRAM")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcons[1])
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LES1A_MYCOM")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_PROGRAM")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcons[2])
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(7)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LES1A_EXT")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_PROGRAM")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcons[3])
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LES1A_FOLD")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_PROGRAM")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcons[4])
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LES1A_PRINT")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_PROGRAM")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcons[5])
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(10)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LES1A_TRASH")
				END_SCALEFORM_MOVIE_METHOD()
				
				// position the popup locations
				FOR i = 0 TO 11
					IF sPopups[i].iType != -1
						BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_DATA_SLOT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sPopups[i].iType)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(sPopups[i].fPosX)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(sPopups[i].fPosY)
						END_SCALEFORM_MOVIE_METHOD()
					ELSE
						sPopups[i].bClosed = TRUE
					ENDIF
				ENDFOR
				
				// Run the virus popups
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "RUN_PROGRAM")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
				END_SCALEFORM_MOVIE_METHOD()
				
				b_MiniGameScaleformSetUp = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LifeInvad_Chair")
			// position the chair
				
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			OR HAS_CUTSCENE_FINISHED()
			
				SET_ENTITY_COORDS(objects[mof_smokers_chair].id, <<-1060.36, -244.97, 43.02>>)
				SET_ENTITY_HEADING(objects[mof_smokers_chair].id, 117.456348)
			
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(PLAYER_PED_ID(), SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
					IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(GET_ENTITY_COORDS(objects[mof_smokers_chair].id), "PROP_HUMAN_SEAT_COMPUTER", 1.5, FALSE)
						TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD_WARP(PLAYER_PED_ID(), GET_ENTITY_COORDS(objects[mof_smokers_chair].id), 1.5, -1)
					ENDIF
				ENDIF
			
				IF HAS_SCALEFORM_MOVIE_LOADED(mov)
					#IF IS_DEBUG_BUILD 
						Start_New_Metrics_Stage("4. Popup Minigame") 
					#ENDIF
				
					//Load_Asset_String(str_wp_to_bomb_room)
					IF IS_SCRIPTED_CONVERSATION_ONGOING()
						KILL_ANY_CONVERSATION()
					ENDIF
					
					strMinigameConvo = "LS1A_LOOK"
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					REMOVE_CUTSCENE()	

					DISPLAY_RADAR(FALSE)	
					DISPLAY_HUD(FALSE) 
					CLEAR_PRINTS()			 
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,0)
					
					//Activate multihead blinders
					SET_MULTIHEAD_SAFE(TRUE,TRUE)
					
					// CLean up remaining
					Reset_Event(mef_ambient_boardroom)
					Reset_Event(mef_ambient_office)
					Reset_Event(mef_ambient_paper_throw)
					
					mission_ped_flag x
					// Remove paper throwing and air guitaring peds
					FOR x = mpf_office_paper_m TO mpf_office_d
						IF DOES_ENTITY_EXIST(peds[x]) 	DELETE_PED(peds[x]) ENDIF
					ENDFOR
					
					// remove boardroom peds
					FOR x = mpf_boardroom_norris TO mpf_boardroom_f_c
						IF DOES_ENTITY_EXIST(peds[x]) 	DELETE_PED(peds[x]) ENDIF
					ENDFOR
					
					
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_a_chair], INSTANT_BLEND_OUT)
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_b_chair], INSTANT_BLEND_OUT)
					SAFE_STOP_SYNCHRONIZED_ENTITY_ANIM(objects[mof_office_d_chair], INSTANT_BLEND_OUT)
					
					UnLoad_Asset_Model(sAssetData, mod_hipster_m)
					Unload_Asset_Model(sAssetData, mod_hipster_f)
					Unload_Asset_Model(sAssetData, mod_hipster_f_heel)
					
					Unload_Asset_Model(sAssetData, PROP_PAPER_BALL)
					
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_boardroom_main)
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_boardroom_intro)
					
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_paper_throw)
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_airguitar_1st)
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_airguitar_1st_exit)
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_airguitar_2nd)
					Unload_Asset_Anim_Dict(sAssetData, anim_dict_airguitar_2nd_exit)
					
					Load_Asset_Model(sAssetData, PROP_MONITOR_01C)
					bSwappedMonitorToClean = FALSE
					
					// Reset the chairs
					mission_objects_flag iChairs
					FOR iChairs = mof_boardroom_m_a_chair TO mof_boardroom_f_c_chair
						IF DOES_ENTITY_EXIST(objects[iChairs].id)
							VECTOR vPos
							vPos = GET_ENTITY_COORDS(objects[iChairs].id)
							SET_OBJECT_AS_NO_LONGER_NEEDED(objects[iChairs].id)
							CLEAR_AREA(vPos, 1.0, TRUE, TRUE, FALSE, FALSE)
						ENDIF
					ENDFOR
					
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(LES1A_CLEAR_POPUPS)
					
					INIT_PC_SCRIPTED_CONTROLS("Lester1A_PopUpMinigame")
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("LESTER_1A_POPUP_MINIGAME")
						START_AUDIO_SCENE("LESTER_1A_POPUP_MINIGAME")
					ENDIF
					
					FOR i = 0 TO iUsablePopups-1
						IF sPopups[i].iType != -1
						AND sPopups[i].iSfxLoop != -1
						AND sPopups[i].bSfxPlaying
							SET_VARIABLE_ON_SOUND(sPopups[i].iSfxLoop, "State", 2)
							CPRINTLN(DEBUG_MIKE, "MINI GAME porn popups sfx volume set to 2 for popup: ", i)
						ENDIF
					ENDFOR
					
					iReopenTimeStamp = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 4000)
					iPopupOpenStage = 0
					
					bPlayedClosePopupsFirstConvo = FALSE
										
					mission_substage++
				ENDIF
			ENDIF
		BREAK
	    CASE 1 //draws the popupminigame page and waits for photo upload
			// load the get up mini cutscene
			IF NOT IS_CUTSCENE_ACTIVE()
				REQUEST_CUTSCENE("LES_1A_MCS_3")
				bStreamCutScene = FALSE
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(objects[mof_smokers_chair].id)
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
					objects[mof_smokers_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<< -1060.1737, -245.2477, 43.6942 >>, 1.0, PROP_OFF_CHAIR_01)
				ENDIF
			ENDIF
			
			// print help text
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF iPopupHelp = 0
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						PRINT_HELP_FOREVER("LES1A_16_KM")		// move the mouse...
					ELSE
						PRINT_HELP_FOREVER("LES1A_16")		// move the mouse...
					ENDIF
				ELIF iPopupHelp = 1
					PRINT_HELP_FOREVER("LES1A_161")		// reveal the shortcut
				ELIF iPopupHelp = 2
					PRINT_HELP_FOREVER("LES1A_162")		// close all the popups....
				ELIF iPopupHelp = 3
					PRINT_HELP_FOREVER("LES1A_17")		// run the AV...
				ELIF iPopupHelp = 4
					PRINT_HELP_FOREVER("LES1A_18")		// click scan...
				ENDIF
			ENDIF
			 
			IF GET_POPUPS_OPEN() = 0
				IF iPopupHelp < 3
					CLEAR_HELP()
					iPopupHelp = 3
					strMinigameConvo = "LS1A_SOFT"
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
			
			IF GET_GAME_TIMER() > iReopenTimeStamp
			
				iReopenTimeStamp = GET_GAME_TIMER()
				IF iScanTimer != 0
					iReopenTimeStamp += GET_RANDOM_INT_IN_RANGE(100, 250)
				ELSE
					SWITCH iPopupOpenStage
						CASE 0
							iReopenTimeStamp += GET_RANDOM_INT_IN_RANGE(3000, 4000)
						BREAK
						CASE 1
							iReopenTimeStamp += GET_RANDOM_INT_IN_RANGE(500, 1000)
						BREAK
						CASE 2	FALLTHRU
						CASE 3
							iReopenTimeStamp += GET_RANDOM_INT_IN_RANGE(6000, 7000)
						BREAK
						DEFAULT
							IF GET_POPUPS_OPEN() = 0
								iReopenTimeStamp += 10000
							ELSE
								iReopenTimeStamp += GET_RANDOM_INT_IN_RANGE(7000, 10000)
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF

				IF GET_POPUPS_OPEN() < iUsablePopups-1
				//AND iScanTimer != 0
				
					iPopupOpenStage++

					/*	Find a random popup to reopen
					 *	The popup cannot be one that is already open
					 *	Nor can it be the last one to be closed	
					 */
					INT nextToReopen
					nextToReopen = -1
					WHILE nextToReopen = -1 
					OR nextToReopen = iLastPopupClosed
					OR NOT sPopups[nextToReopen].bClosed
						nextToReopen 	= GET_RANDOM_INT_IN_RANGE(0, iUsablePopups) 
					ENDWHILE

					// Prompt scaleform to open a popup
					BEGIN_SCALEFORM_MOVIE_METHOD(mov, "OPEN_POPUP")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(nextToReopen)
					END_SCALEFORM_MOVIE_METHOD()

					// Play opening sounds and advert music
					PLAY_SOUND_FRONTEND(-1,"OPEN_WINDOW", "LESTER1A_SOUNDS")
					sPopups[nextToReopen].bClosed = FALSE
					IF sPopups[nextToReopen].bSfxPlaying = FALSE
					AND sPopups[nextToReopen].iSfxLoop != -1
						STRING strSFX
						IF iPopupsSFXPlaying < 6
							SWITCH (iPopupsSFXPlaying)
								CASE 0 strSFX = "POPUP_MUSIC_01" BREAK
								CASE 1 strSFX = "POPUP_MUSIC_02" BREAK
								CASE 2 strSFX = "POPUP_MUSIC_03" BREAK
								CASE 3 strSFX = "POPUP_MUSIC_04" BREAK
								CASE 4 strSFX = "POPUP_MUSIC_05" BREAK
								CASE 5 strSFX = "POPUP_MUSIC_06" BREAK
								DEFAULT strSFX = "POPUP_MUSIC_RND" BREAK
							ENDSWITCH
						ELSE
							strSFX = "POPUP_MUSIC_RND"
						ENDIF
						PLAY_SOUND_FROM_COORD(sPopups[nextToReopen].iSfxLoop, strSFX, <<-1059.63, -244.61, 43.92>>, "LESTER1A_SOUNDS")
						SET_VARIABLE_ON_SOUND(sPopups[nextToReopen].iSfxLoop, "State", 2)
						//PLAY_SOUND_FRONTEND(sPopups[nextToReopen].iSfxLoop, strSFX, "LESTER1A_SOUNDS")
						CPRINTLN(DEBUG_MIKE, "REOPEN POPUP PLAY POPUP LOOP SFX:", nextToReopen)
						sPopups[nextToReopen].bSfxPlaying = TRUE
						iPopupsSFXPlaying++
					ENDIF

				ENDIF
			ENDIF
			
			// Process feedback from scaleform movie
			SWITCH scaleform_return
			
				CASE mgsf_av_open

					IF NOT bIsAVOpen
						IF GET_POPUPS_OPEN() > 0
							CPRINTLN(DEBUG_MIKE, "AV Open: Close popups first!")
							BEGIN_SCALEFORM_MOVIE_METHOD(mov, "OPEN_ANTIVIRUS")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LES1A_AV2")
							END_SCALEFORM_MOVIE_METHOD()
							IF NOT bPlayedClosePopupsFirstConvo
								strMinigameConvo = "LS1A_CLICK"
								bPlayedClosePopupsFirstConvo = TRUE
							ENDIF
							KILL_FACE_TO_FACE_CONVERSATION()
						ELSE
							CPRINTLN(DEBUG_MIKE, "AV Open: Opened")
							BEGIN_SCALEFORM_MOVIE_METHOD(mov, "OPEN_ANTIVIRUS")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LES1A_AV1")
							END_SCALEFORM_MOVIE_METHOD()
							bIsAVOpen = TRUE
							IF iPopupHelp != 4
								CLEAR_HELP()
								iPopupHelp = 4
								strMinigameConvo = "LS1A_SCAN"
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Popup closed
				CASE mgsf_av_popup_close					
					// Get the last popup closed
					IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(last_popup_return_index)
						BEGIN_SCALEFORM_MOVIE_METHOD(mov, "LAST_POPUP_CLOSED")
						last_popup_return_index = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
					ENDIF
				BREAK
				
				// Antivirus clean started
				CASE mgsf_av_start_clean
				
					IF bIsAVOpen
						PLAY_SOUND_FRONTEND(iSFX_FindVirus,"FINDING_VIRUS", "LESTER1A_SOUNDS")
						IF iPopupHelp != 5
							CLEAR_HELP()
							iPopupHelp = 5
							strMinigameConvo = "LS1A_SCAN2"
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
						
						iScanTimer = GET_GAME_TIMER()
						iReopenTimeStamp = iScanTimer + 100
					ENDIF
				BREAK
				
				// Start exterminate
				CASE mgsf_av_exterminate
				
					IF bIsAVOpen
						CLEAR_HELP()
						KILL_FACE_TO_FACE_CONVERSATION()
						
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						iCurrentPopup 		= 0
						strMinigameConvo 	= "LS1A_DALEK"
						mission_substage++
					ENDIF
				BREAK
			ENDSWITCH
			
			// Update scan
			IF iScanTimer != 0
				INT iTimePassed, iScanBar
				iTimePassed = GET_GAME_TIMER() - iScanTimer
				
				SWITCH iScanStutter
					CASE 0
						// Progress up to 85% over 1 seconds
						IF iTimePassed >= 1000 
							STOP_SOUND(iSFX_FindVirus)
							iScanStutter++ 
						ELSE
							iPrevScanBar = iScanBar
							iScanBar = ROUND((TO_FLOAT(iTimePassed)/1000.0) * 85.0)
						ENDIF
					BREAK
					CASE 1
						IF iTimePassed >= 3000
							PLAY_SOUND_FRONTEND(iSFX_FindVirus,"FINDING_VIRUS", "LESTER1A_SOUNDS")
							iScanStutter++
						ELSE
							// Pause for 1 second
							IF NOT bScanPaused
								bScanPaused = TRUE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF iTimePassed >= 3250
							STOP_SOUND(iSFX_FindVirus)
							iScanStutter++ 
						ELSE
							IF bScanPaused
								bScanPaused = FALSE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
							
							iPrevScanBar = iScanBar
							iScanBar = ROUND((TO_FLOAT(iTimePassed-3000)/250.0) * 1.0) + 85
						ENDIF
					BREAK
					CASE 3
						IF iTimePassed >= 3750
							PLAY_SOUND_FRONTEND(iSFX_FindVirus,"FINDING_VIRUS", "LESTER1A_SOUNDS")
							iScanStutter++ 
						ELSE
							// Pause for 1/2 second
							IF NOT bScanPaused
								bScanPaused = TRUE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
						ENDIF
					BREAK
					CASE 4
						IF iTimePassed >= 4500
							STOP_SOUND(iSFX_FindVirus)
							iScanStutter++ 
						ELSE
							IF bScanPaused
								bScanPaused = FALSE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
							
							iPrevScanBar = iScanBar
							iScanBar = ROUND((TO_FLOAT(iTimePassed-3750)/750.0) * 3.0) + 86
						ENDIF
					BREAK
					CASE 5
						IF iTimePassed >= 4650
							PLAY_SOUND_FRONTEND(iSFX_FindVirus,"FINDING_VIRUS", "LESTER1A_SOUNDS")
							iScanStutter++ 
						ELSE
							IF NOT bScanPaused
								bScanPaused = TRUE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
						ENDIF
					BREAK
					CASE 6
						IF iTimePassed >= 5000
							STOP_SOUND(iSFX_FindVirus)
							iScanStutter++ 
						ELSE
							IF bScanPaused
								bScanPaused = FALSE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
							
							iPrevScanBar = iScanBar
							iScanBar = ROUND((TO_FLOAT(iTimePassed-4650)/350.0) * 3.0) + 89
						ENDIF
					BREAK
					CASE 7
						IF iTimePassed >= 6500
							PLAY_SOUND_FRONTEND(iSFX_FindVirus,"FINDING_VIRUS", "LESTER1A_SOUNDS")
							iScanStutter++ 
						ELSE
							IF NOT bScanPaused
								bScanPaused = TRUE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
						ENDIF
					BREAK
					CASE 8
						IF iTimePassed >= 6850
							STOP_SOUND(iSFX_FindVirus)
							iScanStutter++ 
						ELSE
							IF bScanPaused
								bScanPaused = FALSE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
							
							iPrevScanBar = iScanBar
							iScanBar = ROUND((TO_FLOAT(iTimePassed-6500)/350.0) * 2.0) + 92
						ENDIF
					BREAK
					CASE 9
						IF iTimePassed >= 7350
							PLAY_SOUND_FRONTEND(iSFX_FindVirus,"FINDING_VIRUS", "LESTER1A_SOUNDS")
							iScanStutter++ 
						ELSE
							IF NOT bScanPaused
								bScanPaused = TRUE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
						ENDIF
					BREAK
					CASE 10
						IF iTimePassed >= 8600
							STOP_SOUND(iSFX_FindVirus)
							iScanStutter++ 
						ELSE
							IF bScanPaused
								bScanPaused = FALSE
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "PLAY_SCAN_ANIM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
							
							iPrevScanBar = iScanBar
							iScanBar = ROUND((TO_FLOAT(iTimePassed-7350)/1250.0) * 6.0) + 94
						ENDIF
					BREAK
					CASE 11
						STOP_SOUND(iSFX_FindVirus)
						PLAY_SOUND(-1, "Virus_Eradicated", "LESTER1A_SOUNDS")
						iPrevScanBar = iScanBar
						iScanBar = 100
						iScanTimer = 0
					BREAK
				ENDSWITCH
				
				// Udates the scan bar
				IF iPrevScanBar != iScanBar
					BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_SCAN_BAR")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScanBar)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
			ENDIF
			
			// Manage volume of sound fx
			INT j
			REPEAT COUNT_OF(sPopups) j
				IF sPopups[j].bSfxPlaying
				AND sPopups[j].iSfxLoop != -1
					SET_VARIABLE_ON_SOUND(sPopups[j].iSfxLoop, "TracksPlaying", TO_FLOAT(CLAMP_INT(iPopupsSFXPlaying, 1, 12)))
				ENDIF
			ENDREPEAT
			
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,0)
		BREAK
		CASE 2
			BOOL bClosedOne
			
			IF GET_GAME_TIMER() - iExterminateTimer >= 100
				WHILE NOT bClosedOne
					IF iCurrentPopup >= iUsablePopups
						SETTIMERA(0)
						bClosedOne = TRUE	// set to true just to cause the loop to exit
						
						mission_substage++
					ELIF NOT sPopups[iCurrentPopup].bClosed
					AND sPopups[iCurrentPopup].iType != -1
						
						// Close the popup
						BEGIN_SCALEFORM_MOVIE_METHOD(mov, "CLOSE_POPUP")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentPopup)
						END_SCALEFORM_MOVIE_METHOD()
					
						// kill the sfx
						IF sPopups[iCurrentPopup].bSfxPlaying
						AND sPopups[iCurrentPopup].iSfxLoop != -1
							STOP_SOUND(sPopups[iCurrentPopup].iSfxLoop)
							CPRINTLN(DEBUG_MIKE, "EXTERMINATE STOP POPUP LOOP SFX:", iCurrentPopup)
							sPopups[iCurrentPopup].bSfxPlaying = FALSE
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1,"CLOSE_WINDOW", "LESTER1A_SOUNDS")
						
						sPopups[iCurrentPopup].bClosed = TRUE
						iExterminateTimer = GET_GAME_TIMER()
						bClosedOne = TRUE
					ELSE
						bClosedOne = FALSE
					ENDIF
					
					iCurrentPopup++
				ENDWHILE
			ENDIF

			DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,0)
		BREAK
		CASE 3
			strMinigameConvo = "DONTPLAY"
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_PASS", CONV_PRIORITY_HIGH)
					mission_substage++
				ENDIF
			ENDIF
			
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,0)
		BREAK
		CASE 4
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				mission_substage++
			ENDIF
			
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,0)
		BREAK
		CASE 5
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,0)
			
			// do cutscene
			IF HAS_CUTSCENE_LOADED()
				//This should keep the blinders on
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				IF NOT IS_PED_INJURED(peds[mpf_smoker])
					REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_smoker], "Life_invader_Engineer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(objects[mof_smokers_chair].id, "LifeInvad_Chair", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PROP_OFF_CHAIR_01)
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				IF IS_AUDIO_SCENE_ACTIVE("LESTER_1A_POPUP_MINIGAME")
					STOP_AUDIO_SCENE("LESTER_1A_POPUP_MINIGAME")
				ENDIF
				
				// Load all the assets back in
				Load_Asset_Model(sAssetData, IG_JAY_NORRIS)
				Load_Asset_Model(sAssetData, mod_hipster_m)
				Load_Asset_Model(sAssetData, mod_hipster_f)
				Load_Asset_Model(sAssetData, mod_hipster_f_heel)
				
				Load_Asset_Model(sAssetData, PROP_PAPER_BALL)
				Load_Asset_Model(sAssetData, Prop_Off_Chair_01)
				
				Load_Asset_AnimDict(sAssetData, anim_dict_airguitar_1st_exit)
				Load_Asset_AnimDict(sAssetData, anim_dict_interview_1st_exit)
				Load_Asset_AnimDict(sAssetData, anim_dict_paper_throw)
				Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_main)
				Load_Asset_AnimDict(sAssetData, anim_dict_boardroom_intro)	// boardroom with jay norris
				Load_Asset_AnimDict(sAssetData, anim_dict_engineer_airguitar)
				Load_Asset_AnimDict(sAssetData, "MOVE_P_M_ZERO_RUCKSACK")
				
				TRIGGER_EVENT(mef_ambient_receptionist)
				TRIGGER_EVENT(mef_ambient_office)
				TRIGGER_EVENT(mef_ambient_interview)
				TRIGGER_EVENT(mef_ambient_boardroom)
				TRIGGER_EVENT(mef_ambient_paper_throw)
				
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				
				SHUTDOWN_PC_SCRIPTED_CONTROLS()
				
				mission_substage++
			ENDIF
		BREAK
		CASE 6
			Unload_Asset_ScaleForm(sAssetData, mov)
			bExitStateCam = FALSE
			bExitStateMike = FALSE
			mission_substage++
		BREAK
		CASE 7
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LifeInvad_Chair")
				FREEZE_ENTITY_POSITION(objects[mof_smokers_chair].id, TRUE)
				bExitStateCam = TRUE
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL")
				REPLAY_STOP_EVENT()
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0)
				//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 9, 0)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK, FALSE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ZERO_RUCKSACK")
				
				START_AUDIO_SCENE("LESTER_1A_RIG_EXPLOSIVE")
				
				Trigger_Event(mef_engineer_air_guitar)
				bExitStateMike = TRUE
			ENDIF
			
			IF bExitStateMike
			AND bExitStateCam
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				Mission_Set_Stage(msf_st5_plant_bomb)
			ENDIF
			
		BREAK
		CASE STAGE_EXIT
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
		BREAK
	ENDSWITCH
	
	IF mission_substage = 1
	
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF iTimeSinceLastMinigameConvo = -1
				iTimeSinceLastMinigameConvo = GET_GAME_TIMER()
			ENDIF
		ELSE
			iTimeSinceLastMinigameConvo = -1
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strMinigameConvo)
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, str_dialogue, strMinigameConvo, CONV_PRIORITY_HIGH)
					IF ARE_STRINGS_EQUAL(strMinigameConvo, "LS1A_LOOK")
						strMinigameConvo = "LS1A_PCCHT"
					ELSE
						strMinigameConvo 			= ""
					ENDIF
					iTimeSinceLastMinigameConvo = -1
					bLifeInvaderGuyLastToSpeak 	= FALSE
				ENDIF
			ENDIF
		ELIF NOT ARE_STRINGS_EQUAL(strMinigameConvo, "DONTPLAY")
			IF (iTimeSinceLastMinigameConvo != -1 AND GET_GAME_TIMER() - iTimeSinceLastMinigameConvo > 3500)
			OR bLifeInvaderGuyLastToSpeak
				IF IS_SAFE_TO_START_CONVERSATION()
					IF NOT bLifeInvaderGuyLastToSpeak
						IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_ENCOUR", CONV_PRIORITY_HIGH)
							bLifeInvaderGuyLastToSpeak = TRUE
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_DEAL", CONV_PRIORITY_HIGH)
							bLifeInvaderGuyLastToSpeak = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF NOT bStreamCutScene
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
			//SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 9, 0)
			PRELOAD_PED_COMP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_RUCKSACK)
			bStreamCutScene = TRUE
		ENDIF
	ENDIF
ENDPROC


PROC ST5_Plant_Bomb()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1053.66, -231.77, 43.99>>, 0.5, PROP_SECURITY_CASE_01)
				objects[mof_prototype_case].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1053.66, -231.77, 43.99>>, 0.5, PROP_SECURITY_CASE_01)
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "5. Plant the bomb", FALSE, FALSE)
				
				KILL_ANY_CONVERSATION()
				PRINT_NOW("LES1A_6",DEFAULT_GOD_TEXT_TIME,1)
				blip_Objective = CREATE_BLIP_FOR_OBJECT(objects[mof_prototype_case].id)
				SET_BLIP_NAME_FROM_TEXT_FILE(blip_Objective, "PROTOTYPE")
				
				bCamExited = FALSE
				mission_substage++
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_CUTSCENE_ACTIVE()
				REQUEST_CUTSCENE("LES_1A_MCS_4")
				bStreamCutScene = FALSE
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			// play bomb plant cutscene
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1053.825439,-234.700668,43.021164>>, <<-1057.149414,-234.353485,46.205666>>, 1.000000)
			AND HAS_CUTSCENE_LOADED()

				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
				START_CUTSCENE()
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				
				IF IS_AUDIO_SCENE_ACTIVE("LESTER_1A_RIG_EXPLOSIVE")
					STOP_AUDIO_SCENE("LESTER_1A_RIG_EXPLOSIVE")
				ENDIF
												
				Load_Asset_AnimDict(sAssetData, anim_dict_airguitar_2nd)
				Load_Asset_AnimDict(sAssetData, anim_dict_interview_2nd)
				Load_Asset_AnimDict(sAssetData, "amb@prop_human_seat_computer@male@base")
				Load_Asset_Model(sAssetData, P_MICHAEL_BACKPACK_S)
				
				Kill_Event(mef_engineer_air_guitar)
				
				RESET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID())
				SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(), TRUE)
				
				SAFE_REMOVE_BLIP(blip_Objective)
				
				REPLAY_RECORD_BACK_FOR_TIME(6.0, 0.0)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			// Reset the chair incase its been moved
			IF DOES_ENTITY_EXIST(objects[mof_smokers_chair].id)
				VECTOR vPos
				vPos = GET_ENTITY_COORDS(objects[mof_smokers_chair].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objects[mof_smokers_chair].id)
				CLEAR_AREA(vPos, 1.0, TRUE, TRUE, FALSE, FALSE)
			ENDIF
			
			/*
			IF IS_CUTSCENE_PLAYING()
				IF NOT bReplayRecordEventStarted
					IF GET_CUTSCENE_TIME() >= 8000
					AND GET_CUTSCENE_TIME() < 19250
						REPLAY_START_EVENT()
						bReplayRecordEventStarted = TRUE
					ENDIF
				ELSE
					IF GET_CUTSCENE_TIME() >= 19250
						REPLAY_STOP_EVENT()
						bReplayRecordEventStarted = FALSE
					ENDIF
				ENDIF
			ENDIF
			*/
			
			IF NOT bCamExited
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0)
					bCamExited = TRUE
				ENDIF
			ENDIF
			
			IF bCamExited
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael") 
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
				
				IF NOT DOES_ENTITY_EXIST(objects[mof_mike_rucksack].id)
				AND HAS_MODEL_LOADED(P_MICHAEL_BACKPACK_S)
					Create_Prop(objects[mof_mike_rucksack], P_MICHAEL_BACKPACK_S, v_BagCoord)
					SET_ENTITY_COLLISION(objects[mof_mike_rucksack].id, FALSE)
					SET_ENTITY_COORDS(objects[mof_mike_rucksack].id, v_BagCoord)
					SET_ENTITY_ROTATION(objects[mof_mike_rucksack].id, v_BagRot)
					FREEZE_ENTITY_POSITION(objects[mof_mike_rucksack].id, TRUE)
				ENDIF
				
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
				
				START_AUDIO_SCENE("LESTER_1A_LEAVE_OFFICE")

				mission_substage++
			ENDIF
		BREAK
		CASE 4
			IF IS_SCREEN_FADED_OUT()
				IF HAS_CUTSCENE_FINISHED()
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(enum_to_int(DOORHASH_LINVADER_OFFICE_UP))
						DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_LINVADER_OFFICE_UP), 0.0)
						DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LINVADER_OFFICE_UP), DOORSTATE_FORCE_LOCKED_THIS_FRAME, TRUE, TRUE)
						DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LINVADER_OFFICE_UP), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, TRUE, TRUE)
						CDEBUG1LN(DEBUG_MIKE, "LES1A &&&&&& Closed office door after skipping cutscene")
					ELSE
						CDEBUG1LN(DEBUG_MIKE, "LES1A &&&&&& Office door NOT registered with door system")
					ENDIF
					
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ELIF IS_SCREEN_FADED_IN()
				Mission_Set_Stage(msf_st6_leave_building)
			ENDIF
		BREAK
		CASE STAGE_EXIT
		BREAK
	ENDSWITCH
	
	IF NOT bStreamCutScene
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL2,9,0)
			bStreamCutScene = TRUE
		ENDIF
	ENDIF
		
ENDPROC

PROC ST6_Leave_Building()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "6. Leave the building", FALSE, FALSE)
		
			Load_Asset_AnimDict(sAssetData, "AMB@PROP_HUMAN_SEAT_COMPUTER@MALE@IDLE_B")
			Load_Asset_AssistedLine(sAssetData, al_stairs_down)
			TRIGGER_EVENT(mef_ambient_milk)
			
			SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_UNLOCKED)
			
			PRINT_NOW("LES1A_7",DEFAULT_GOD_TEXT_TIME,1)
			
			i_EngineerBye = 0
			mission_substage++
		BREAK
		CASE 1
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != interior_offices
			
				bAllowRunning = TRUE
			
				IF IS_AUDIO_SCENE_ACTIVE("LESTER_1A_LEAVE_OFFICE")
					STOP_AUDIO_SCENE("LESTER_1A_LEAVE_OFFICE")
				ENDIF
				
				Mission_Set_Stage(msf_st7_go_home)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH i_EngineerBye 
		CASE 0
			// stop the engineer animating and stick him in his chair.
			IF NOT IS_PED_INJURED(peds[mpf_smoker])
			AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1060.18, -245.31, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
			AND HAS_ANIM_DICT_LOADED("amb@prop_human_seat_computer@male@base")
			
				IF NOT DOES_ENTITY_EXIST(objects[mof_smokers_chair].id)
					objects[mof_smokers_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<-1060.18, -245.31, 43.02>>, 0.5, PROP_OFF_CHAIR_01)
				ENDIF
				
				// Move and freeze the chair
				SET_ENTITY_COORDS(objects[mof_smokers_chair].id, <<-1060.36, -244.97, 43.02>>)
				SET_ENTITY_HEADING(objects[mof_smokers_chair].id, 117.456348)
				FREEZE_ENTITY_POSITION(objects[mof_smokers_chair].id, TRUE)

				CLEAR_PED_TASKS(peds[mpf_smoker])
				SET_RAGDOLL_BLOCKING_FLAGS(peds[mpf_smoker], RBF_PLAYER_IMPACT)
				
				TASK_PLAY_ANIM_ADVANCED(peds[mpf_smoker], "amb@prop_human_seat_computer@male@base", "base",
						<<-1060.36, -244.96, 43.52>>, <<0,0,-62.4523997>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_OVERRIDE_PHYSICS)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_smoker])
				
				
				IF IS_AMBIENT_SPEECH_PLAYING(peds[mpf_smoker])
					STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_smoker])
				ENDIF
				
				i_EngineerBye++
			ENDIF
		BREAK
		CASE 1
			// walks past the programmer
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1058.017456,-237.587067,43.021278>>, <<-1053.271118,-246.440964,48.722553>>, 3.812500)
				i_EngineerBye++
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_SAFE_TO_START_CONVERSATION()
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_smoker], "LIEngineer")
				
			ELIF HAS_ANIM_DICT_LOADED("AMB@PROP_HUMAN_SEAT_COMPUTER@MALE@IDLE_B")
				IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_BYE", CONV_PRIORITY_HIGH)
					
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0)
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM_ADVANCED(null, "AMB@PROP_HUMAN_SEAT_COMPUTER@MALE@IDLE_B", "IDLE_E",
							<<-1060.36, -244.96, 43.52>>, <<0,0,-62.4523997>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
						TASK_PLAY_ANIM_ADVANCED(null, "amb@prop_human_seat_computer@male@base", "base",
							<<-1060.36, -244.96, 43.52>>, <<0,0,-62.4523997>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_OVERRIDE_PHYSICS)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(peds[mpf_smoker], seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), peds[mpf_smoker], 5000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					TASK_LOOK_AT_ENTITY(peds[mpf_smoker], PLAYER_PED_ID(), 4000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_smoker])
					i_EngineerCovoTimer = GET_GAME_TIMER()
					i_EngineerBye++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_BYE", SPEECH_PARAMS_FORCE)
				i_EngineerBye++
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_PED_INJURED(peds[mpf_smoker])
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(peds[mpf_smoker])) < 7.5625 // 2.75^2
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_EngineerCovoTimer > 8000
					IF CREATE_CONVERSATION(sConvo, str_dialogue, "LS1A_HANG", CONV_PRIORITY_HIGH)
						i_EngineerCovoTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC ST7_Go_Home()
	MONITOR_PLAYER_BEING_ARMED()
	
	// rob - 2161097
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(peds[mpf_receptionist_f])
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), peds[mpf_receptionist_f], <<5,5,5>>)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SPRINT)
		ENDIF
	ENDIF
	
	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_living_room
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE)
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ENDIF
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "7. Go home", FALSE, FALSE)
			Trigger_Event(mef_lester_phone_call_2)
			
			SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
			SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_04_HW, FALSE)
			
			Trigger_Event(mef_tracey)
			
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			
			bTVTurnedOn 		= FALSE
			bTVTurnedOff		= FALSE
			mission_substage++
		BREAK
		CASE 1

			IF IS_PLAYER_AT_ANGLED_AREA_ON_FOOT(locates_data, v_home_living_room, 
					<<-805.352295,177.564972,71.848480>>, <<-801.704163,168.162628,74.974594>>, 9.037500,
					TRUE, "WATCH_KEYNOTE", TRUE)

				IF HAS_CUTSCENE_LOADED()
				AND bTVTurnedOn
				
					IF DOES_ENTITY_EXIST(peds[mpf_receptionist_f])
						DELETE_PED(peds[mpf_receptionist_f])
					ENDIF
					
					IF DOES_ENTITY_EXIST(objects[mof_receptionist_chair].id)
						SET_OBJECT_AS_NO_LONGER_NEEDED(objects[mof_receptionist_chair].id)
					ENDIF
					
					Unload_Asset_Anim_Dict(sAssetData, "AMB@PROP_HUMAN_SEAT_COMPUTER@MALE@IDLE_B")
				
					CLEAR_MISSION_LOCATE_STUFF(locates_data)
				
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					IF NOT IS_PED_INJURED(peds[mpf_tracey])
						REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_tracey], "Tracy", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					Kill_Event(mef_tracey)
					
					INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
					
					Load_Asset_AnimDict(sAssetData, ad_ig_1)
					Load_Asset_Audiobank(sAssetData, "Lester1B")
					Load_Asset_Audiobank(sAssetData, "SAFEHOUSE_MICHAEL_SIT_SOFA")
					
					ENABLE_MOVIE_SUBTITLES(FALSE)
					mission_substage++
				ELSE
					IF NOT HAS_CUTSCENE_LOADED()
						DRAW_DEBUG_TEXT_2D("WAITING FOR: cutscene", <<0.5, 0.5, 0.0>>, 255,0,0,255)
						PRINTLN("[Lester1] WAITING FOR: cutscene")
					ENDIF
					IF NOT bTVTurnedOn
						DRAW_DEBUG_TEXT_2D("WAITING FOR: bTVTurnedOn", <<0.5, 0.55, 0.0>>, 255,0,0,255)
						PRINTLN("[Lester1] WAITING FOR: bTVTurnedOn")
					ENDIF
				ENDIF
			ELSE
				IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_living_room
				AND IS_SAFE_TO_START_CONVERSATION()
					ENABLE_MOVIE_SUBTITLES(TRUE)
				ELSE
					ENABLE_MOVIE_SUBTITLES(FALSE)
				ENDIF
			ENDIF
			
			// cutscene stream
			IF NOT IS_CUTSCENE_ACTIVE()
				IF DOES_ENTITY_EXIST(peds[mpf_tracey])
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_home_living_room) < DEFAULT_CUTSCENE_LOAD_DIST
					REQUEST_CUTSCENE("LES_1B_MCS_1")
					Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, PLAYER_PED_ID(), "Michael")
					Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, peds[mpf_tracey], "Tracy")
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_home_living_room) > DEFAULT_CUTSCENE_UNLOAD_DIST
				OR IS_PLAYER_CHANGING_CLOTHES()
					REMOVE_CUTSCENE()
				ENDIF
			ENDIF
		
			// Enable the living room projector
			IF NOT bTVTurnedOn
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_home_living_room) < DEFAULT_CUTSCENE_LOAD_DIST
					IF IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_MICHAEL_PROJECTOR)
						RESTORE_STANDARD_CHANNELS()
						START_AMBIENT_TV_PLAYBACK(TV_LOC_MICHAEL_PROJECTOR, TVCHANNELTYPE_CHANNEL_2, TV_PLAYLIST_SPECIAL_LEST1_INTRO_FAME_OR_SHAME, FALSE, TRUE)
						bTVTurnedOn = TRUE
					ENDIF
				ENDIF
			ELSE
				IF bTVTurnedOn
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_home_living_room) > DEFAULT_CUTSCENE_UNLOAD_DIST
						IF IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_MICHAEL_PROJECTOR)
							STOP_TV_PLAYBACK(TRUE, TV_LOC_MICHAEL_PROJECTOR)
						ENDIF
						bTVTurnedOn = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			// Lock life invader offices
			IF bDisableConspicuousBehavior
				IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_offices
				
					DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_L), DOORSTATE_UNLOCKED)
					DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_R), DOORSTATE_UNLOCKED)
					
					DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_L), DOORSTATE_UNLOCKED)
					DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_R), DOORSTATE_UNLOCKED)
				
				ELSE
			
			
					BOOL bHasLockedBoth
					bHasLockedBoth = TRUE
					IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_L)) 	!= DOORSTATE_LOCKED
					OR DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_R)) 	!= DOORSTATE_LOCKED
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1081.791260,-260.706299,36.803814>>, <<-1082.800537,-258.813782,39.825764>>, 3.250000)
							DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_L), DOORSTATE_LOCKED)
							DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_R), DOORSTATE_LOCKED)
						ELSE
							bHasLockedBoth = FALSE
						ENDIF
					ENDIF
					
					IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_L)) 	!= DOORSTATE_LOCKED
					OR DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_R)) 	!= DOORSTATE_LOCKED
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1044.870972,-230.183853,38.014164>>, <<-1046.881226,-231.225525,41.074177>>, 3.250000)
							DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_L), DOORSTATE_LOCKED)
							DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_R), DOORSTATE_LOCKED)
						ELSE
							bHasLockedBoth = FALSE
						ENDIF
					ENDIF
				
					IF bHasLockedBoth
						
						IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_L)) 	= DOORSTATE_LOCKED AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_L))) <= 0.1
						AND DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_R)) 	= DOORSTATE_LOCKED AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LIFE_INVADER_FRONT_R))) <= 0.1
						AND DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_L)) 	= DOORSTATE_LOCKED AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LIFE_INVADER_REAR_L))) <= 0.1
						AND DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LIFE_INVADER_REAR_R)) 	= DOORSTATE_LOCKED AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_LIFE_INVADER_REAR_R))) <= 0.1
						
							Kill_Event(mef_ambient_milk)
							Kill_Event(mef_ambient_interview)
							Kill_Event(mef_ambient_office)
							Kill_Event(mef_ambient_boardroom)
							Kill_Event(mef_ambient_paper_throw)
							Kill_Event(mef_ambient_dialogue)
							Kill_Event(mef_engineer_air_guitar)
							
							Unload_Asset_Interior(sAssetData, interior_offices)
							
							Unload_Asset_Model(sAssetData, mod_hipster_f)
							Unload_Asset_Model(sAssetData, mod_hipster_f_heel)
							Unload_Asset_Model(sAssetData, mod_hipster_main)
							Unload_Asset_Model(sAssetData, mod_hipster_m)
							Unload_Asset_Model(sAssetData, mod_chair)
							Unload_Asset_Model(sAssetData, IG_JAY_NORRIS)
							Unload_Asset_Model(sAssetData, PROP_PAPER_BALL)
							Unload_Asset_Model(sAssetData, PROP_HACKY_SACK_01)
							Unload_Asset_Model(sAssetData, P_MICHAEL_BACKPACK_S)
							
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_airguitar_1st)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_airguitar_1st_exit)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_airguitar_2nd)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_airguitar_2nd_exit)
							Unload_Asset_Anim_Dict(sAssetData, 	anim_dict_boardroom_main)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_boardroom_intro)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_boardroom_main)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_boardroom_exit)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_coffee)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_engineer_airguitar)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_interview_1st)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_interview_1st_exit)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_interview_2nd)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_interview_2nd_exit)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_milk_lady)
							Unload_Asset_Anim_Dict(sAssetData,	anim_dict_paper_throw)

							Unload_Asset_Anim_Dict(sAssetData, "amb@prop_human_seat_computer@male@base")
							
							Unload_Asset_Audio_Bank(sAssetData, "Lester1A_Qub3d")
							Unload_Asset_Audio_Bank(sAssetData, "Lester1A")
							Unload_Asset_Audio_Bank(sAssetData, "COMPUTERS")
							
							mission_ped_flag ePed
							FOR ePed = mpf_smoker TO mpf_boardroom_f_c
								IF ePed != mpf_receptionist_f
									IF DOES_ENTITY_EXIST(peds[ePed])
										DELETE_PED(peds[ePed])
									ENDIF
								ENDIF
							ENDFOR
							
							mission_objects_flag eObj
							FOR eObj = mof_smokers_chair TO mof_mike_rucksack
								IF eObj != mof_receptionist_chair
									IF DOES_ENTITY_EXIST(objects[eObj].id)
										SET_OBJECT_AS_NO_LONGER_NEEDED(objects[eObj].id)
									ENDIF
								ENDIF
							ENDFOR
							
							bDisableConspicuousBehavior 	= FALSE
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF

			IF HAS_PLAYER_THREATENED_TRACY()
				IF IS_PED_UNINJURED(peds[mpf_tracey])
					TASK_SMART_FLEE_PED(peds[mpf_tracey], PLAYER_PED_ID(), 200, -1)
					SET_PED_KEEP_TASK(peds[mpf_tracey], TRUE)
					Mission_Failed(mff_tracey_threat)
				ELSE
					Mission_Failed(mff_tracey_dead)
				ENDIF
			ENDIF
			
		BREAK
		CASE 2
			IF IS_CUTSCENE_PLAYING()
				
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-805.352295,177.564972,71.848480>>, <<-801.704163,168.162628,74.974594>>, 9.037500, <<-828.2097, 179.0565, 70.2544>>, 154.3327, TRUE, FALSE)
				REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
			
				SAFE_DELETE_OBJECT(objects[mof_tv_remote].id)
				CLEAR_PRINTS()
				CLEAR_HELP()				
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF IS_CUTSCENE_ACTIVE()
			AND IS_CUTSCENE_PLAYING()
				ENABLE_MOVIE_SUBTITLES(TRUE)
				iDetonationTimer = -1
				Mission_Set_Stage(msf_st8_tv_start)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

BOOL bEnteredAsFirstPerson

PROC ST8_TV_Start()
	MONITOR_PLAYER_BEING_ARMED()
	CYCLE_CAMERAS()
	
	BOOL bWindowOpen
	BOOL bWindowClose
	
	IF iDetonationTimer != -1
		IF GET_GAME_TIMER() - iDetonationTimer > i_detonate_window_open
		#IF IS_DEBUG_BUILD
		OR b_debug_window_open
		#ENDIF
			bWindowOpen = TRUE
		ENDIF
		
		IF GET_GAME_TIMER() - iDetonationTimer > i_detonate_window_close
			bWindowClose = TRUE
		ENDIF
	ENDIF
	
	IF bWindowOpen AND NOT bWindowClose
		MICHAEL_ANIM_MANAGER(TRUE)
	ELSE
		MICHAEL_ANIM_MANAGER(FALSE)
	ENDIF
	MONITOR_SOUNDS()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			OR NOT IS_CUTSCENE_ACTIVE()
				IF NOT DOES_CAM_EXIST(cam_tv)
					cam_tv = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos[CAMS_SCRIPT], vCamRot[CAMS_SCRIPT], 40.0, TRUE)
					SHAKE_CAM(cam_tv, "HAND_SHAKE", 0.3)
				ENDIF
				// 1987687
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
					bEnteredAsFirstPerson = TRUE
					SET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON )
				ENDIF
				ENABLE_MOVIE_SUBTITLES(TRUE)
				SET_CAM_ACTIVE(cam_tv, TRUE)
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tracey")
				SAFE_DELETE_PED(peds[mpf_tracey])
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			OR NOT IS_CUTSCENE_ACTIVE()
				
				REPLAY_STOP_EVENT()
				
				IF DOES_CAM_EXIST(cam_tv)
					IF IS_CAM_ACTIVE(cam_tv)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
				ENDIF
				
				//IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_R_IDLE), ANIM_SCRIPT)
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM)
					SET_NEW_ANIM_TASK(MAS_R_IDLE, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, AF_LOOPING|AF_TURN_OFF_COLLISION|AF_IGNORE_GRAVITY, TRUE)
				ENDIF
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, TRUE)

				IF NOT DOES_ENTITY_EXIST(objects[mof_tv_remote].id)
					Create_Prop(objects[mof_tv_remote], PROP_CS_REMOTE_01, GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_PH_L_HAND, <<0,0,0>>))
					Unload_Asset_Model(sAssetData, PROP_CS_REMOTE_01)
					ATTACH_ENTITY_TO_ENTITY(objects[mof_tv_remote].id, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
				ENDIF

				eAnimState = MAS_R_IDLE
				iAnimDelay = GET_GAME_TIMER()
				bDelaying = TRUE
				
				DISABLE_CELLPHONE(TRUE)
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
			
				//Preload next TV segment. Attempt to fix: 1843791 - waiting for possible code fix.
				//Load_Asset_Scaleform(sAssetData, str_new_overlay, sfi_news_overlay)
			
				ADD_CONTACT_TO_PHONEBOOK(CHAR_DETONATEPHONE, MICHAEL_BOOK, FALSE) 
				SET_CALL_PREVENTED_FOR_CHAR_DETONATEPHONE(TRUE)
				
				SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_SCRIPT, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(TV_PLAYLIST_SPECIAL_INVADER_EXP), FALSE)
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "8. Start TV", TRUE, FALSE)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				SET_WANTED_LEVEL_MULTIPLIER(1.0)
			
				CLEAR_HELP()
				PRINT_HELP_FOREVER("TV_HLP_LEST1B_1")
				INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
				mission_substage++
			ENDIF
		BREAK
		CASE 1
			IF IS_INPUT_ATTEMPTING_TO_CHANGE_CHANNEL(TRUE)
				// add detonate contact to phone
				PLAY_TV_CHANNEL_WITH_PLAYLIST(TV_LOC_MICHAEL_PROJECTOR, TVCHANNELTYPE_CHANNEL_2, TV_PLAYLIST_SPECIAL_INVADER, TRUE)
				CLEAR_HELP()
				IF IS_THIS_PRINT_BEING_DISPLAYED("FIND_NEWS")
					CLEAR_THIS_PRINT("FIND_NEWS")
				ENDIF
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(LES1B_TIME_WATCHING)
				bChangedChannel = TRUE
				iDetonationTimer = GET_GAME_TIMER()
				mission_substage++
			ELSE
				IF IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					IF NOT bExpireChannelObj
						PRINT_NOW("FIND_NEWS", DEFAULT_GOD_TEXT_TIME, 0)
						bExpireChannelObj = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF (GET_GAME_TIMER() - iDetonationTimer) > i_start_speech
				PRINT_HELP("WAIT_TO_DETONATE", 23000)
				mission_substage++ 
			ENDIF
		BREAK
		CASE 3
			IF bWindowOpen
				g_TVStockMarketIntro_CellphoneTimer 	= 10049
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			IF IS_PHONE_ONSCREEN()
				DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
				DISABLE_CELLPHONE_INTERNET_APP_THIS_FRAME_ONLY()
				FORCE_SELECTION_OF_THIS_CONTACT_ONLY(CHAR_DETONATEPHONE)
			ENDIF
		
			// Jump to detonated stage
			IF bDetonated
				// player has called the contact trigger the next scene
				CLEAR_PRINTS()
				CLEAR_HELP()
				Mission_Set_Stage(msf_st9_tv_detonate)
				
			ELSE
				IF bWindowClose
					CLEAR_PRINTS()
					CLEAR_HELP()
					bDetonated = FALSE
					Mission_Set_Stage(msf_st10_mission_passed)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC ST9_TV_Detonate()
//	CPRINTLN(DEBUG_MIKE, "TIMERA() =============== ", TIMERA())
	MONITOR_PLAYER_BEING_ARMED()
	MICHAEL_ANIM_MANAGER(FALSE)
	CYCLE_CAMERAS()
	MONITOR_SOUNDS()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			SETTIMERA(0)
			Load_Asset_Scaleform(sAssetData, str_new_overlay, sfi_news_overlay)
			bExterminated 	= TRUE
			mission_substage++
		BREAK
		CASE 1
			// Show the signal lost scaleform
			IF TIMERA() > i_signal_lost_start
			AND HAS_SCALEFORM_MOVIE_LOADED(sfi_news_overlay)
				RESTORE_STANDARD_CHANNELS()
				SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)
				STOP_AMBIENT_TV_PLAYBACK(TV_LOC_MICHAEL_PROJECTOR)
				BEGIN_SCALEFORM_MOVIE_METHOD(sfi_news_overlay, "SHOW_STATIC")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				END_SCALEFORM_MOVIE_METHOD()
//				PRELOAD_BROWSER()
				SETTIMERA(0)
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			IF TIMERA() <  i_signal_lost_duration
				IF NOT bTVTurnedOff
					RESTORE_STANDARD_CHANNELS()
					SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
					CLEAR_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_SCRIPT) // to clear up the "special" script channel
					CPRINTLN(DEBUG_MIKE, "Turning off Tv under scaleform")
					bTVTurnedOff = TRUE
				ENDIF
				SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", FALSE)
				DRAW_SCALEFORM_MOVIE_3D_SOLID(sfi_news_overlay, v_sf3d_tv_coord, v_sf3d_tv_rot, v_sf3d_tv_scale, v_sf3d_tv_world_size)
			ELSE
				IF eCameraCurrent != CAMS_SCRIPT
					SWITCH_CAMERA(CAMS_SCRIPT, 2000)
				ENDIF
				SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", TRUE)
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				mission_substage++
			ENDIF
		BREAK
		CASE 3
		
			START_AUDIO_SCENE("LESTER_1A_AFTER_EXPLOSION")
		
			SET_NEW_ANIM_TASK(MAS_P_OUTRO, NORMAL_BLEND_IN, WALK_BLEND_OUT, AF_TURN_OFF_COLLISION)
			Mission_Set_Stage(msf_st10_mission_passed)
		BREAK		
	ENDSWITCH
ENDPROC

PROC ST10_Mission_Passed()
	MONITOR_PLAYER_BEING_ARMED()
	MICHAEL_ANIM_MANAGER(FALSE)
	SWITCH mission_substage
		CASE STAGE_ENTRY
			REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_DETONATEPHONE, MICHAEL_BOOK)
			
			IF bExterminated
				IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM)
				OR (IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO))
				AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO)) > 0.64)
				
					CPRINTLN(DEBUG_MIKE, "TRIGGERED BLEND OUT")
					RELEASE_TV_FOR_PLAYER_CONTROL(TV_LOC_MICHAEL_PROJECTOR)
					
					IF bEnteredAsFirstPerson
						SET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_FIRST_PERSON )
					ENDIF
					
					IF DOES_CAM_EXIST(cam_tv)
						DESTROY_CAM(cam_tv)
					ENDIF
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					RENDER_SCRIPT_CAMS(FALSE, TRUE, 3000)
					
					bCanInterruptAnim = FALSE
					iPassTimer = GET_GAME_TIMER()
					mission_substage++
				ENDIF
			ELSE	
				REMOVE_MODEL_HIDE(<<-804.44751, 172.79373, 72.34801>>, 0.5, PROP_CS_REMOTE_01, TRUE)
				Mission_Failed(mff_did_not_kill_target)
			ENDIF
		BREAK
		CASE 1
			IF NOT bCanInterruptAnim 
				IF IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM)
					IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("WalkInterruptible"))
						bCanInterruptAnim = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			BOOL bDoInterrupt
			IF bCanInterruptAnim
				IF GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) != 0
				OR GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) != 0
					bDoInterrupt = TRUE
				ENDIF
			ENDIF
		
			// url:bugstar:2099721
			IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) != CAM_VIEW_MODE_FIRST_PERSON)
			OR (GET_GAME_TIMER() - iPassTimer) >= 4000
				IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM)
				OR (IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO), ANIM_SCRIPT)
				AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO)) >= 0.831)
				
					CPRINTLN( DEBUG_MIKE, "MAS_P_OUTRO blend out started @ ", GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), ad_ig_1, GET_ANIM_STRING(MAS_P_OUTRO)) )
				
					SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", TRUE)
					REMOVE_MODEL_HIDE(<<-804.44751, 172.79373, 72.34801>>, 0.5, PROP_CS_REMOTE_01, TRUE)
			
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
	//				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SkipOnFootIdleIntro, TRUE)
	//				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
					
					Mission_Passed()	
					
				ELIF bDoInterrupt
				
					SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", TRUE)
					REMOVE_MODEL_HIDE(<<-804.44751, 172.79373, 72.34801>>, 0.5, PROP_CS_REMOTE_01, TRUE)
				
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					Mission_Passed()	
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC Mission_Flow()

	mission_stage_flag eStage = int_to_enum(mission_stage_flag, mission_stage)

	SWITCH eStage
		
		CASE msf_st0_intro_cutscene		ST0_Intro_Cutscene()			BREAK
		CASE msf_st1_get_clothes		ST1_Get_Clothing()				BREAK
		CASE msf_st2_go_to_offices		ST2_Go_To_Lifeinvader()			BREAK
		CASE msf_st3_go_to_computer		ST3_Walkthrough_Offices()		BREAK
		CASE msf_st4_mini_game			ST4_Mini_Game()					BREAK
		CASE msf_st5_plant_bomb			ST5_Plant_Bomb()				BREAK
		CASE msf_st6_leave_building		ST6_Leave_Building()			BREAK
		CASE msf_st7_go_home			ST7_Go_Home()					BREAK
		CASE msf_st8_tv_start			ST8_TV_Start()					BREAK
		CASE msf_st9_tv_detonate		ST9_TV_Detonate()				BREAK
		CASE msf_st10_mission_passed	ST10_Mission_Passed()			BREAK
		
	ENDSWITCH
	
	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FriendRequest")
ENDPROC

#IF IS_DEBUG_BUILD 

PROC Process_Debugging()
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)			
		Mission_Passed()
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_f)	
		Mission_Failed(mff_debug_forced)
	ENDIF
	DONT_DO_J_SKIP(locates_data)
ENDPROC

#ENDIF

/*
									  ______  _______  ______   _  ______  _______    _       _______  _______  ______  
									 / _____)(_______)(_____ \ | |(_____ \(_______)  (_)     (_______)(_______)(_____ \ 
									( (____   _        _____) )| | _____) )   _       _       _     _  _     _  _____) )
									 \____ \ | |      |  __  / | ||  ____/   | |     | |     | |   | || |   | ||  ____/ 
									 _____) )| |_____ | |  \ \ | || |        | |     | |_____| |___| || |___| || |      
									(______/  \______)|_|   |_||_||_|        |_|     |_______)\_____/  \_____/ |_|                                                                                         
*/

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Mission_Failed(mff_default)
	ENDIF                              
	
	REQUEST_BAWSAQ_STOCK_PRICE_VALUES() // 1480767 - makes sure player has prices in time for end of mission
	
	Mission_Setup()
	
WHILE TRUE	
	WAIT(0)
	#IF IS_DEBUG_BUILD 
		Process_Debugging()
	#ENDIF
	
	Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
	Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)
	
	Mission_Checks()								// Mission scenario checks; fails, disables running at certain points, etc
	Manage_Mission_Events()							// manage any events triggered
	Mission_Stage_Management()						// manage any stage switching stuff
	Mission_Stage_Skip()							// processes any stage skipping that is required
	Manage_Doors()	
	Mission_Flow()									// process the mission flow
ENDWHILE
	
ENDSCRIPT					
