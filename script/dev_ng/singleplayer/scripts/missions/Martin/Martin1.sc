//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║ 				Author:  Ben Barclay				Date: 03/08/2011		║
//║																				║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║ 																			║
//║ 				Martin 1 - Vinewood Babylon									║
//║																				║
//║ 		Snipe a plane as Michael, chase it as Trevor, then meet at the  	║
//║ 		barn, where Trevor has kidnapped the wife. 							║
//║ 																			║
//║			*	Step															║
//║ 		*	By																║
//║ 		*	Step															║
//║ 		*	Instructions													║
//║ 																			║
//║ 		Gameplay elements introduced: 										║
//║ 		Vehicle Introduced: 						 						║
//║ 		Location: 															║
//║ 		Notes:  															║
//║ 																			║
//╚═════════════════════════════════════════════════════════════════════════════╝

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//═════════════════════════════════╡ COMMAND HEADERS ╞═══════════════════════════════════

using "rage_builtins.sch"
using "globals.sch"

using "script_heist.sch"
using "commands_misc.sch"
using "commands_pad.sch" 
using "commands_script.sch"
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "commands_camera.sch"
using "commands_path.sch"
using "commands_fire.sch"
using "commands_graphics.sch"
using "commands_object.sch"
using "script_player.sch"
using "script_debug.sch"
using "streamed_scripts.sch"
using "selector_public.sch"
using "model_enums.sch"
using "dialogue_public.sch"
using "player_ped_public.sch"
USING "flow_public_core_override.sch"
USING "commands_audio.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_hud.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_interiors.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "script_misc.sch"
USING "remote_sniper_public.sch"
USING "commands_entity.sch"
using "script_blips.sch"
USING "select_mission_stage.sch"
USING "locates_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "script_ped.sch"
USING "CompletionPercentage_public.sch"
USING "flow_public_GAME.sch"
USING "clearMissionArea.sch"
USING "timelapse.sch"
USING "player_ped_scenes.sch"
USING "spline_cam_edit.sch"
USING "achievement_public.sch"
USING "cheat_controller_public.sch" 
USING "push_in_public.sch"
USING "script_misc.sch"

USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//═════════════════════════════════╡ CONSTANTS ╞═══════════════════════════════════

CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS                      175
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS                       15    
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS                    35    

CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK           12  
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK         14                             

CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK            6

USING "traffic.sch"

//═════════╡ VALUES ╞═════════
CONST_INT NUMBER_OF_RECORDINGS 20
CONST_INT NUMBER_OF_DIALOGUE 20
CONST_INT NUMBER_OF_SET_PIECES 20

CONST_INT iRecNo									294

//═════════════════════════════════╡ ENUMS ╞═══════════════════════════════════


//═════════╡ MISSION CONTROL ╞═════════

ENUM MISSION_STAGE
	STAGE_INIT_MISSION			=0,
	STAGE_DRIVE_TO_OBSERVATORY,
	STAGE_SHOOT_PLANE,
	STAGE_CUTSCENE_PLANE_DESCENT,
	STAGE_CHASE_PLANE,
	STAGE_KILL_PASSENGERS,
	STAGE_CUTSCENE_PLANE_CRASH,
	STAGE_DESTROY_SNIPER,
	STAGE_END_CUTSCENE,
	PASSED_MISSION
ENDENUM
MISSION_STAGE eMissionStage 

ENUM MISSION_TEXT
	TEXT_SNIPE_HELP_1,
	TEXT_SNIPE_HELP_2,
	TEXT_LOCATE_TARGETS,
	TEXT_THERMAL_HELP,
	TEXT_DRIVE,
	TEXT_CROSS_HELP,
	TEXT_OFF_ROAD,
	TEXT_JUMP_BUS,
	TEXT_JUMP_TRAIN,
	TEXT_FALL_CLIFF,
	TEXT_FALL1,
	TEXT_FALL2,
	TEXT_FALL3,
	TEXT_FALL4,
	TEXT_FALL5,
	TEXT_END1,
	TEXT_END2,
	TEXT_CRASH,
	TEXT_FMC1,
	TEXT_FMC2,
	TEXT_FMC3,
	TEXT_FMC4,
	TEXT_FMC5
ENDENUM

//═════════╡ COORDINATES AND HEADINGS ╞═════════


//═════════════════════════════════╡ STRUCTS ╞═══════════════════════════════════
SELECTOR_CAM_STRUCT sCamDetails
REMOTE_SNIPER_DATA sSniperData
SELECTOR_PED_STRUCT sSelectorPeds
structPedsForConversation structConvoPeds
//═════════════════════════════════╡ VARIABLES ╞═══════════════════════════════════

structTimelapse sTimelapse

//═════════╡ FLAGS ╞═════════
BOOL bScriptInProgress								= TRUE
BOOL bStageSetup								
BOOL bStartRecordingTool
BOOL bTriggerRecording[NUMBER_OF_RECORDINGS]
BOOL bRightEngineShot
BOOL bDoRightEngineCut
BOOL assetsRequested
BOOL bTriggerFX[10]
BOOl bTextTriggered[100]
BOOL bSetPiece[NUMBER_OF_SET_PIECES]
BOOL bSetPieceUnloaded[NUMBER_OF_SET_PIECES]
BOOL bDoingJumpCutscene							
BOOL bDoneSlowMoJump									
BOOL btimeScaleBackToNormal								
BOOL bHasUsedHintCam								
BOOL PlayerCanDoJump								
BOOL modelsRequested								
BOOL doneGetBackOnBikeText								
BOOL doneChaseAfterPlaneText						
BOOL doneTakeDownPlaneWarning						
BOOL playerDoneTrainJump							
BOOL bCutsceneActive							
BOOL broughtlandingGearOut							
BOOL Bikeplaybackstopped							
BOOL PlaneOnfireSoundStopped							
BOOL bMissionFailed										
BOOL doneChatS1_Jump3
BOOL floatingHelp1Done
BOOL DumpModelRequested
BOOL DumpSetPieceCreated
BOOL DumpPlaybackStarted
BOOL DumpSetpieceSwitchedToAI
BOOL bCutsceneDone
BOOL FellOffBikeStatDone
BOOL screenFadedInFromReplay
BOOL cinematicCamDeactivated
BOOL hasSniperBeenSetToActive
BOOL playerHasTakenTaxi
BOOL RemoteSniperNeedsUpdated
BOOL DamageEnginesTextDone
BOOL interactiveMusicStarted
BOOL interactiveMusicStarted2
BOOL preparedInteractiveMusic2
BOOL IsPedPointingPlayingAnim
BOOL IsPedPointingSetup
BOOL IsPedPointingPlayingAnim2
BOOL IsPedPointingPlayingAnim3
BOOL IsPedPointingSetup2
BOOL IsPedPointingSetup3
BOOL IsParkedCarSetup
BOOL hasIsStreamPlaying
BOOL startInteractiveMusic2
BOOL doneHeadLook[6]
BOOL PlayerOnBike
BOOL EndCutRequested
BOOL bPlaneShotSceneStreamLoaded
BOOL bPlaneShotSceneStreamStarted
BOOL sniperCreated
BOOL PlayerIsNowTrevor
BOOL trainModelsRequested
BOOL busRequested
BOOL HaulerRequested
BOOL PatriotRequested
BOOL BallerRequested
BOOL DilettanteRequested
BOOL TornadoRequested
BOOL Bus2Requested
BOOL bCutsceneStarted	
BOOL S1_CLIFF_CHAT_DONE 
BOOL S1_FMC4_CHAT_DONE 
BOOL S1_FALL5_CHAT_DONE
BOOL S1_JUMP2_CHAT_DONE 
BOOL S1_FALL4_CHAT_DONE
BOOL S1_FMC3_CHAT_DONE 
BOOL S1_FALL3_CHAT_DONE
BOOL S1_FALL2_CHAT_DONE 
BOOL S1_FMC2_CHAT_DONE
BOOL S1_FALL1_CHAT_DONE 
BOOL S1_JUMP1_CHAT_DONE
BOOL S1_OROAD_CHAT_DONE 
BOOL S1_FMC1_CHAT_DONE
BOOL VanMoved
BOOL bMissionFailedToSlow
BOOL DoneHelp[3]
BOOL doneSnipe8Text 		
BOOL doneLoseCopsText 	
BOOL doneSnipe7Text 		
BOOL doneSnipe5Text 
BOOL vehiclesMovedForStart
//BOOL planeFrozen
BOOL safetyTimeScaleBackToNormalSet
BOOL missionCanFail
BOOL doneSnipe8Text2
BOOL IsPedPointingPlayingAnim4	
BOOL IsPedPointingSetup4
BOOL requestedCrashStream
BOOL crashStreamStarted
BOOL DoneHorn1
BOOL DoneHorn2
BOOL DoneHorn3
BOOL CutsceneRequested
BOOL IPLSwapped
BOOL hasNumberPlateBeenSet
BOOL playerStartedAsMichael
BOOL bDoneJumpCutscene
BOOL camsCleanedUp
BOOL doneGameCamShake
BOOL doneChatGun
BOOL doneKillPedGodText
BOOL CombatTaskGiven
BOOL CombatTaskRequired
BOOL DoneChatChange2
BOOL doneChatWreck
BOOL doneWreckHelpText
BOOL HintCamActivated
BOOL trevExitTaskGiven
BOOL MikeExitTaskGiven
BOOL bVanDoorsLocked
//BOOL todStopped
BOOL vanFrozen
//BOOL pourGasTaskGiven
//BOOL weaponSwapped
//BOOL createdGasTrail
BOOL cutsceneSkipped
BOOL timelapseRequired
BOOL driveTaskCancelled
BOOL doneGetOffChat
BOOL camsKilled
BOOL TaskToLeaveCar
BOOL sniperVanBlipRouteSet
BOOL pedHelmetRemoved
//BOOL startedLoadScene
BOOL collisionTurnedOnForPlane
BOOL ringToneStarted
BOOL ringToneStopped 
BOOL HavierMoved
BOOL CheckPointVehOveridden
BOOL bloodAdded
BOOL doneS1_HEAD
BOOL doneS1_ATVAN
BOOL doneS1_ATVAN2 
BOOL doneS1_BIKE
BOOL MikeEarPieceGiven
BOOL doneS1_JUMP4
BOOL doneS1_ATTACK
BOOL S1_FMC5_CHAT_DONE
BOOL S1_TRAILER_CHAT_DONE
BOOL barrelSetUp
BOOL PilotComponentsGiven
BOOL bSniperIsBeingReplayed
BOOL playersCarMoved
BOOL lightsTurnedOn
BOOL vanNeedsToBeExploded
BOOL vehicleFalling
BOOL bStartCoordsGrabbed
BOOL bEndCoordsGrabbed
BOOL doneGetBackInVanText
BOOL blipSetToRed
BOOL nonTempEventsSetup
BOOL RadarBack
BOOL AnimForPed0Skipped
BOOL AnimForPed1Skipped
BOOL bCutsceneBeingSkipped
BOOL TrevLookTaskGiven
BOOL bClearedAreaForCutscene
BOOL bVideoRecordingDone
BOOL bCamNearClipSet
BOOL bPilotDeleted
BOOL bCamViewModeChanged


//═════════╡ VECTORS ╞═════════
VECTOR vSniperStartPos = <<-893.0, -446.2, 173.1>>//<< -880.7526, -447.4006, 150.5476 >>
VECTOR vDriveToPoint = << -411.5472, 1175.3082, 324.5601 >>
VECTOR vBFChaseStartCoords = << -390.1234, 1226.1991, 324.6426 >>
VECTOR vEngineOffset1 = << 1.6631, -3.2729, 0.7426 >>
VECTOR vTitanSlideRot = <<-15.000, -30.000, -20.000>>
VECTOR vTitanCrashCoords = << 2813.8667, 4679.4482, 44.9295 >>
VECTOR vImpact
VECTOR vObservatoryRoadMin = << -526.6754, 1145.1808, 307.5816 >>
VECTOR vObservatoryRoadMax = << -276.3485, 1305.7333, 336.9266 >>
VECTOR vSportsCarRoadMin = << 253.4966, 2206.1792, 34.6447 >>
VECTOR vSportsCarRoadMax = << 709.9940, 2627.1787, 129.6453 >>
VECTOR playersCoords 
VECTOR truckCoords
VECTOR VECTOR_ZERO = <<0.0, 0.0, 0.0>>
VECTOR vplayerCoords
VECTOR vFallingStartCoords
VECTOR vFallingEndCoords
//VECTOR vOffset = <<-0.4, 5.7, 0.05>>
//VECTOR vHavierCoords

//═════════╡ FLOAT ╞═════════
CONST_FLOAT fMissionRestartHeading 220.4202
CONST_FLOAT fBFChaseStartHeading 328.7128
CONST_FLOAT fCarSpawnHeading 18.7639
FLOAT f_sniper_heading = 255.9889 /*166.0019*/
FLOAT fRecordingTime
FLOAT fPlaybackSpeed 
FLOAT fCrossScreenPositionX[2]
Float fCrossScreenPositionY[2]
FLOAT fMainPlaybackSpeed 
FLOAT fTimeScale
FLOAT fGroundz
//FLOAT fpitch
//FLOAT fHavierZ
FLOAT fHavierHeading
//═════════╡ INTS ╞═════════
INT iCounter										= 0
INT iGetOnBikeTextTimer								= 0
INT iLoadingScreenTimer								= 0
INT iShotTimer										= 0
INT iStageSection									= 0
INT iMiniCutStage									= 0
INT icutsceneStage									= 0
INT iSeePlaneChatTimer								= 0
INT PlaneOnFireSoundID
INT PlaneShotSoundID
INT iLeftDoor
INT iRightDoor
INT iHornTimer
INT iMoveStageOnTimer								= 0
INT iNumberOfHits									= 0
INT iEffectTimer
INT iTrevDeletedTimer
INT iMissChatTimer
INT iHelpChatTimer
INT iKillChatTimer
INT iGetOffCarChatTimer
INT iFallingTimer
INT iskipCutsceneStage								= 0
INT iAnimTaskTime

//═════════╡ SYNC SCENES ╞═════════
INT BarrellSyncScene
//INT LeavingJetSyncScene

//═════════╡ TOD ╞═════════
//TIMEOFDAY sTimeHour

//═════════╡ SCALEFORM ╞═════════
SCALEFORM_INDEX sfSniperLoading
SCALEFORM_RETURN_INDEX sfReturnBool

//═════════╡ STRINGS ╞═════════
STRING FailReason

//═════════╡ PEDS ╞═════════
PED_INDEX pedPilot
//PED_INDEX pedTarget
PED_INDEX RandomDriver[8]
PED_INDEX DumpSetPieceDriver
PED_INDEX PointingPed[4]
PED_INDEX CSTrev
PED_INDEX PedPatricia
PED_INDEX endGuy[2]

//═════════╡ ENTITYS ╞═════════
ENTITY_INDEX TrevsCarEntity
ENTITY_INDEX TrevsEntity
ENTITY_INDEX CutPatricia
//ENTITY_INDEX CSMike

//═════════╡ VEHICLES ╞═════════
VEHICLE_INDEX vehTitan
VEHICLE_INDEX vehBike
VEHICLE_INDEX PlayerCar
VEHICLE_INDEX MichaelsCar
VEHICLE_INDEX vehTrain
VEHICLE_INDEX vehBus
VEHICLE_INDEX vehTruck
VEHICLE_INDEX vehVan
VEHICLE_INDEX vehBaller
VEHICLE_INDEX vehDilettante
VEHICLE_INDEX vehBobcat
VEHICLE_INDEX vehTornado
VEHICLE_INDEX vehBus2
VEHICLE_INDEX ParkedCar
VEHICLE_INDEX SniperVan
VEHICLE_INDEX vehBiff 
VEHICLE_INDEX DumpSetPiece
VEHICLE_INDEX PlayerStatCar
VEHICLE_INDEX TrevCutsceneCar
VEHICLE_INDEX ParkedCarAtStart
VEHICLE_INDEX vehCutBike

VEHICLE_SNAPSHOT TrevsStartCar
//VEHICLE_GEN_NAME_ENUM MichaelsCarEnum

//═════════╡ OBJECTS ╞═════════
//OBJECT_INDEX ObjCrashedPlane
OBJECT_INDEX RemoteSniper
OBJECT_INDEX objBriefcase
OBJECT_INDEX ObjTitan
OBJECT_INDEX ObjEndBarrel
OBJECT_INDEX objTrevsPhone

//═════════╡ SCENARIOS ╞═════════
SCENARIO_BLOCKING_INDEX	AirstripBlockingArea
SCENARIO_BLOCKING_INDEX CropFieldBlockingArea1
SCENARIO_BLOCKING_INDEX PoliceBikeBlockingArea
SCENARIO_BLOCKING_INDEX BarnBlockingArea
SCENARIO_BLOCKING_INDEX	SeatNextToVanBlockingArea
SCENARIO_BLOCKING_INDEX MartinHouseMainScenarioBlockingArea
SCENARIO_BLOCKING_INDEX PlaneBlockingArea

//═════════╡ SEQUENCES ╞═════════
SEQUENCE_INDEX seqSequence

//═════════╡ WEAPONS ╞═════════

//═════════╡ PARTICLE ID ╞═════════
PTFX_ID ptfxEngine[11]

//═════════╡ Scene ID ╞═════════

//═════════╡ STRUCTS ╞═════════
BUILDING_DATA_STRUCT PlaneCrashTrench
PUSH_IN_DATA pushInData

//═════════╡ Sound ID ╞═════════

//═════════╡ BLIPS ╞═════════
BLIP_INDEX blipObj
BLIP_INDEX blipLocate
BLIP_INDEX bikeBlip
BLIP_INDEX BlipTrevor
BLIP_INDEX BlipMartinHouse
BLIP_INDEX BlipSniperVan
BLIP_INDEX endGuyBlip[2]

//═════════╡ CAMERAS ╞═════════
CAMERA_INDEX camInit  
CAMERA_INDEX camDest  
CAMERA_INDEX camAnim 
CAMERA_INDEX camStatic
CAMERA_INDEX BulletCam

//═════════╡ GROUPS ╞═════════

//═════════╡ MODEL NAMES ╞═════════


//═════════╡ DEBUG ╞═════════
#IF IS_DEBUG_BUILD
//	WIDGET_GROUP_ID widgetDebug
//	BOOL bUberRecord = FALSE
//	BOOL bGnarly = FALSE
//	INT iGnarlyStyle = 0
//	FLOAT pos0x = 2402.6060
//	FLOAT pos0y = 3963.0063
//	FLOAT pos0z = 35.8270
//	FLOAT lookat0x = 0.0
//	FLOAT lookat0y = 0.0
//	FLOAT lookat0z = 0.0
//	FLOAT fov0 = 45.0
//
//	FLOAT pos1x = 0.0
//	FLOAT pos1y = 0.0
//	FLOAT pos1z = 0.0
//	FLOAT lookat1x = 0.0
//	FLOAT lookat1y = 0.0
//	FLOAT lookat1z = 0.0
//	FLOAT fov1 = 45.0
	
	CONST_INT MAX_SKIP_MENU_LENGTH 						10
//	BOOL doneGetBackOnBikeHelpText	
	BOOL PSkip
	BOOL JSkip
	INT iReturnStage                                      								 // mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      				// struct containing the debug menu 
	STRING sMissionName									= "Martin 1 - Vinewood Babylon"
	INT iTrevorTrackPlaneConvoDelay = 10000
	INT iDGTitanCrashRecording1StartNode = 3
	FLOAT fDGTitanCrashRecording1Time = 1000.0
	FLOAT fDGTitanCrashRecording1Speed = 0.8
//	FLOAT fDirY, fDirZ, fScale, fDirX

#ENDIF


//═════════════════════════════════╡ SAFE WAIT ╞═══════════════════════════════════
//PURPOSE:	Performs a wait and a failcheck so that there are no deathcheck problems
PROC safeWait(INT iWaitTime)
	WAIT(iWaitTime)
ENDPROC



//═════════════════════════════════╡ CUTSCENE ╞═══════════════════════════════════


//═════════╡ FUNCTIONS ╞═════════
//PURPOSE:		Performs actions required to start all cutscenes
PROC doStartCutscene(BOOL bClearPlayerTasks = TRUE, BOOL bAnimatedCam = FALSE)
	IF NOT bCutsceneStarted
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT bAnimatedCam
				camInit	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				camDest	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
				SET_CAM_COORD(camInit, <<1,1,1>>)
			ELSE
				camAnim = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
				SET_CAM_COORD(camAnim, <<1,1,1>>)
			ENDIF
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			SET_WIDESCREEN_BORDERS(TRUE, 500)
			IF bClearPlayerTasks
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_CLEAR_TASKS)
			ELSE 
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
		ENDIF
		SETTIMERB(0)
		bCutsceneStarted = TRUE
	ENDIF
ENDPROC

//PURPOSE:		Performs actions required to end all cutscenes
PROC doEndCutscene(BOOL bClearPlayerTasks = TRUE, BOOL bRenderScriptCams = TRUE)
	IF bCutsceneStarted
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE, 500)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF DOES_CAM_EXIST(camInit)
				SET_CAM_ACTIVE(camInit, FALSE)
				DESTROY_CAM	(camInit)
			ENDIF
			IF DOES_CAM_EXIST(camDest)
				SET_CAM_ACTIVE(camDest, FALSE)
				DESTROY_CAM	(camDest)
			ENDIF
			IF DOES_CAM_EXIST(camAnim)
				SET_CAM_ACTIVE(camAnim, FALSE)
				DESTROY_CAM	(camAnim)
			ENDIF
			IF bRenderScriptCams
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ENDIF
			STOP_GAMEPLAY_CAM_SHAKING(TRUE)
			IF bClearPlayerTasks
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF 
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bCutsceneStarted = FALSE
		ENDIF 
	ENDIF
ENDPROC

//PURPOSE:		Checks if the cutscene is ready to skip
FUNC BOOL ReadyToSkipCutscene()
	IF TIMERB() > 1000
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:		Shakes both cameras at the same time
PROC doCamShake(STRING ShakeName, FLOAT fAmplitudeScalar = 1.0)
	SHAKE_CAM(camInit, ShakeName, fAmplitudeScalar)
	SHAKE_CAM(camDest, ShakeName, fAmplitudeScalar)
ENDPROC

//PURPOSE:		Changes shake amplitude
PROC setCamShakeAmp(FLOAT fAmplitudeScalar = 1.0)
	SET_CAM_SHAKE_AMPLITUDE(camInit, fAmplitudeScalar)
	SET_CAM_SHAKE_AMPLITUDE(camDest, fAmplitudeScalar)
ENDPROC

//PURPOSE:		Stops both cams shaking
PROC stopCamShake(BOOL bStopImmediately = FALSE)
	IF DOES_CAM_EXIST(camInit)
		STOP_CAM_SHAKING(camInit, bStopImmediately)
	ENDIF
	IF DOES_CAM_EXIST(camDest)
		STOP_CAM_SHAKING(camDest, bStopImmediately)
	ENDIF
ENDPROC

//PURPOSE: Moves a camera between Init and Dest, for as long as duration
PROC doTwoPointCam(	VECTOR camInitCoord, VECTOR camInitRot, FLOAT camInitFOV, VECTOR camDestCoord, VECTOR camDestRot, FLOAT camDestFOV, INT duration, CAMERA_GRAPH_TYPE GraphTypePo = GRAPH_TYPE_LINEAR, CAMERA_GRAPH_TYPE GraphTypeRo = GRAPH_TYPE_LINEAR)
					
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
	DETACH_CAM(camInit)
	STOP_CAM_POINTING(camInit)
	SET_CAM_COORD(camInit, camInitCoord)
	SET_CAM_ROT(camInit, camInitRot)
	SET_CAM_FOV(camInit, camInitFOV)
	SET_CAM_ACTIVE(camDest, FALSE)
	SET_CAM_ACTIVE(camInit, TRUE)

	DETACH_CAM(camDest)
	STOP_CAM_POINTING(camDest)
	SET_CAM_COORD(camDest, camDestCoord)
	SET_CAM_ROT(camDest, camDestRot)
	SET_CAM_FOV(camDest, camDestFOV)
	SET_CAM_ACTIVE(camDest, TRUE)
	SET_CAM_ACTIVE(camInit, TRUE)
	
	SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphTypePo, GraphTypeRo)
ENDPROC

//PURPOSE: Moves a camera between Init and Dest, for as long as duration, whilst looking at objLookAt
PROC doTwoPointEntityCam(	ENTITY_INDEX entLookAt, 
							VECTOR camInitCoord, VECTOR camInitOffset, FLOAT camInitFOV, 
							VECTOR camDestCoord, VECTOR camDestOffset, FLOAT camDestFOV,
							INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR)
							
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
	DETACH_CAM(camInit)
	STOP_CAM_POINTING(camInit)
	SET_CAM_COORD(camInit, camInitCoord)
	POINT_CAM_AT_ENTITY(camInit, entLookAt, camInitOffset)
	SET_CAM_FOV(camInit, camInitFOV)
	SET_CAM_ACTIVE(camDest, FALSE)
	SET_CAM_ACTIVE(camInit, TRUE)
	DETACH_CAM(camDest)
	STOP_CAM_POINTING(camDest)
	SET_CAM_COORD(camDest, camDestCoord)
	POINT_CAM_AT_ENTITY(camDest, entLookAt, camDestOffset)
	SET_CAM_FOV(camDest, camDestFOV)
	SET_CAM_ACTIVE(camDest, TRUE)
	SET_CAM_ACTIVE(camInit, TRUE)

	SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphType, GraphType)
ENDPROC

//PURPOSE: Moves a camera between Init and Dest, for as long as duration, looking at LookAt
PROC doTwoPointCoordCam(VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
						VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
						INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR)
						
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
	DETACH_CAM(camInit)
	STOP_CAM_POINTING(camInit)
	SET_CAM_COORD(camInit, camInitCoord)
	POINT_CAM_AT_COORD(camInit, camInitLookAt)
	SET_CAM_FOV(camInit, camInitFOV)
	SET_CAM_ACTIVE(camDest, FALSE)
	SET_CAM_ACTIVE(camInit, TRUE)

	DETACH_CAM(camDest)
	STOP_CAM_POINTING(camDest)
	SET_CAM_COORD(camDest, camDestCoord)
	POINT_CAM_AT_COORD(camDest, camDestLookAt)
	SET_CAM_FOV(camDest, camDestFOV)
	SET_CAM_ACTIVE(camDest, TRUE)
	SET_CAM_ACTIVE(camInit, TRUE)

	SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphType, GraphType)
ENDPROC

//PURPOSE: Moves a camera that's attached to a vehicle between Init and Dest, for as long as duration
PROC doTwoAttachLookAtVehicleCam(	VEHICLE_INDEX vehSubject,
									VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
									VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
									INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR)
	IF NOT IS_ENTITY_DEAD(vehSubject)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
		DETACH_CAM(camInit)
		STOP_CAM_POINTING(camInit)
		ATTACH_CAM_TO_ENTITY(camInit, vehSubject, camInitCoord)
		POINT_CAM_AT_ENTITY(camInit, vehSubject, camInitLookAt)
		SET_CAM_FOV(camInit, camInitFOV)
		SET_CAM_ACTIVE(camDest, FALSE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		DETACH_CAM(camDest)
		STOP_CAM_POINTING(camDest)
		ATTACH_CAM_TO_ENTITY(camDest, vehSubject, camDestCoord)
		POINT_CAM_AT_ENTITY(camDest, vehSubject, camDestLookAt)
		SET_CAM_FOV(camDest, camDestFOV)
		SET_CAM_ACTIVE(camDest, TRUE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphType, GraphType)
	ENDIF
ENDPROC

//PURPOSE: Moves a camera that's attached to a vehicle between Init and Dest, for as long as duration
PROC doTwoAttachLookAtEntityCam(	ENTITY_INDEX entSubject,
									VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
									VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
									INT duration)
	IF NOT IS_ENTITY_DEAD(entSubject)
							
		DETACH_CAM(camInit)
		STOP_CAM_POINTING(camInit)
		ATTACH_CAM_TO_ENTITY(camInit, entSubject, camInitCoord)
		POINT_CAM_AT_ENTITY(camInit, entSubject, camInitLookAt)
		SET_CAM_FOV(camInit, camInitFOV)
		SET_CAM_ACTIVE(camDest, FALSE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		DETACH_CAM(camDest)
		STOP_CAM_POINTING(camDest)
		ATTACH_CAM_TO_ENTITY(camDest, entSubject, camDestCoord)
		POINT_CAM_AT_ENTITY(camDest, entSubject, camDestLookAt)
		SET_CAM_FOV(camDest, camDestFOV)
		SET_CAM_ACTIVE(camDest, TRUE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF
ENDPROC

//PURPOSE: Moves a camera that's attached to a vehicle between Init and Dest, for as long as duration
PROC doTwoAttachEntityLookAtOtherEntityCam(	ENTITY_INDEX entAttach, ENTITY_INDEX entLookAt,
											VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
											VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
											INT duration)
	IF NOT IS_ENTITY_DEAD(entAttach)
	AND NOT IS_ENTITY_DEAD(entLookAt)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
		DETACH_CAM(camInit)
		STOP_CAM_POINTING(camInit)
		ATTACH_CAM_TO_ENTITY(camInit, entAttach, camInitCoord)
		POINT_CAM_AT_ENTITY(camInit, entLookAt, camInitLookAt)
		SET_CAM_FOV(camInit, camInitFOV)
		SET_CAM_ACTIVE(camDest, FALSE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		DETACH_CAM(camDest)
		STOP_CAM_POINTING(camDest)
		ATTACH_CAM_TO_ENTITY(camDest, entAttach, camDestCoord)
		POINT_CAM_AT_ENTITY(camDest, entLookAt, camDestLookAt)
		SET_CAM_FOV(camDest, camDestFOV)
		SET_CAM_ACTIVE(camDest, TRUE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
	ENDIF
ENDPROC

//═════════════════════════════════╡ SAFE FADE ╞═══════════════════════════════════


PROC fadeOutAndWait(INT iTime = DEFAULT_FADE_TIME)
	IF IS_SCREEN_FADED_IN()
	OR IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_OUT(iTime)
	ENDIF
	WHILE IS_SCREEN_FADING_OUT()
		WAIT(0)
		PRINTSTRING("SOLOMON1_WAITING_30") PRINTNL()
	ENDWHILE
ENDPROC

PROC fadeInAndWait(INT iTime = DEFAULT_FADE_TIME)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_IN(iTime)
	ENDIF
	WHILE IS_SCREEN_FADING_IN()
		WAIT(0)
		PRINTSTRING("SOLOMON1_WAITING_31") PRINTNL()
	ENDWHILE
ENDPROC


//                _ 
//               | |
//  ___ _ __   __| |
// / _ \ '_ \ / _` |
//|  __/ | | | (_| |
// \___|_| |_|\__,_|	end common functions for David (POST FAILCHECK)


//═════════════════════════════════╡ LOADING ╞═══════════════════════════════════

//═════════════════════════════════╡ MARTIN SWITCH CAMS ╞═══════════════════════════════════

/*#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID widgetDebug
#ENDIF*/

ENUM MARTIN_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE,
	SWITCH_CAM_START_SPLINE1,
	SWITCH_CAM_PLAYING_SPLINE1,
	SWITCH_CAM_START_SPLINE2,
	SWITCH_CAM_PLAYING_SPLINE2,
	SWITCH_CAM_WAITING_FOR_PLAYER_CONTROL,
	SWITCH_CAM_RETURN_TO_GAMEPLAY
ENDENUM
MARTIN_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsPlaneShotScene
SWITCH_CAM_STRUCT scsPlaneToBike

STRING strAnimDictCamShake = "shake_cam_all@"

BOOL bCamShakeAnimDictRequested = FALSE
BOOL bPlaneShotSceneStarted = FALSE
BOOL bPlaneShotSceneFinished = FALSE
BOOL bPlaneEngineExplosionStarted = FALSE
BOOL bPlaneEnginePreSmokeStarted = FALSE
BOOL bPlaneEnginePreSmokeStopped = FALSE
BOOL bPlanePilotConvoStarted = FALSE
BOOL bMichaelShotPlaneConvoStarted = FALSE
BOOL bTrevorLookAtPlaneStarted = FALSE
BOOL bChaseMusicStarted = FALSE
BOOL bBikeRecordingStopped = FALSE
BOOL hasEffectStarted
BOOL doneHelp1
BOOL doneHelp2
BOOL bEffectTriggered
BOOL subStream1Loaded
BOOL subStream2Loaded
BOOL BulletCamActive
BOOL doneS1_SEE
BOOL doneS1_LEAD
BOOL doneS1_LOOK
BOOL PlaneSlowedDown
BOOL TimeToCreatePlane
BOOL landingGearPutAway

INT iPlaneShotSceneDelay = 250
INT iPlaneEngineExplosionDelay = 1000
INT iPlaneEnginePreSmokeOnDelay = 1500
INT iPlaneEnginePreSmokeOffDelay = 2000
INT iPlaneEngineSmokeDelay = 3000
INT iPlanePilotConvoDelay = 2500
INT iMichaelShotPlaneConvoDelay = 5000

FLOAT fDGTitanEntryRecording1Time = 9000.00
FLOAT fDGTitanEntryRecording1Speed = 0.7

INT iDGTitanCrashRecording2StartNode = 3
FLOAT fDGTitanCrashRecording2Time = 25700.00
FLOAT fDGTitanCrashRecording2Speed = 0.6

FLOAT fDGTitanCrashRecording3Time = 30000.00
FLOAT fDGTitanCrashRecording3Speed = 0.6

FLOAT fBB_SOL_JETRecordingTime = 16500.0
FLOAT fBB_SOL_JETRecordingSpeed = 0.4

BOOL bDGTitanCrashRecording2Started = FALSE

FLOAT fDGBFReadyRecordingTime = 2800.000

INT iTrevorLookAtPlaneDelay = 1750
//INT iTrevorLookAtPlaneDuration = 500
INT iBikeRecordingStoppedDelay = 2000


INT iWooshSoundID = -1

FLOAT fWooshStartPhase = 0.243
FLOAT fWooshStopPhase = 0.756

BOOL bWooshStarted = FALSE
BOOL bWooshStopped = FALSE

VECTOR vPlaneEngineExplosionOffset = <<0.0, 1.5, 0.0>>
VECTOR vPlaneEngineExplosionRot = <<-90.0, 0.0, 0.0>>
FLOAT fPlaneEngineExplosionScale = 4.0

/*FLOAT fPlaneFlyByShakeStartPhase = 0.55
FLOAT fPlaneFlyByShakeStopPhase = 0.6
FLOAT fPlaneFlyByShakeScale = 5.0

BOOL bPlaneFlyByShakeStarted = FALSE
BOOL bPlaneFlyByShakeStopped = FALSE*/

FLOAT fPlanceSceneFX1StartPhase = 0.243
//FLOAT fPlanceSceneFX2StartPhase = 0.573

BOOL bPlaneSceneFX1Started = FALSE
BOOL bPlaneSceneFX2Started = FALSE

PROC SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_SHOT_SCENE(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX &viPlane)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_SHOT_SCENE")

	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<2.8408, 12.3731, 0.1466>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<2.2061, -0.0863, 0.6487>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 25.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.1000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 600
		thisSwitchCam.nodes[1].vNodePos = <<2.7632, 10.8777, 0.2020>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<2.1944, -0.0942, 0.5753>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 25.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 2.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 2500
		thisSwitchCam.nodes[2].vNodePos = <<2.5059, 7.8159, 0.3064>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<1.9644, -0.1365, 0.1968>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 25.0000
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<2.5600, 74.1566, 3.0641>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<-8.3390, 0.0002, -175.0362>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].fNodeFOV = 30.0000
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[4].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[4].iCamEaseType = 2
		thisSwitchCam.nodes[4].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 3000
		thisSwitchCam.nodes[5].vNodePos = <<3.8913, -5.8747, 0.5965>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<-3.5100, -0.1071, -176.1688>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 30.0000
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[5].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[7].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[7].iNodeTime = 0
		thisSwitchCam.nodes[7].vNodePos = <<16.2416, -41.4694, -2.3709>>
		thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[7].vNodeDir = <<3.0180, -0.2268, 9.4804>>
		thisSwitchCam.nodes[7].bPointAtEntity = FALSE
		thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[7].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[7].fNodeFOV = 45.0000
		thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[7].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[7].iCamEaseType = 0
		thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[7].fTimeScale = 1.0000
		thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bFlashEnabled = FALSE
		thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[7].fMinExposure = 0.0000
		thisSwitchCam.nodes[7].fMaxExposure = 0.0000
		thisSwitchCam.nodes[7].iRampUpDuration = 0
		thisSwitchCam.nodes[7].iRampDownDuration = 0
		thisSwitchCam.nodes[7].iHoldDuration = 0
		thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[8].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[8].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[8].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[8].iNodeTime = 3000
		thisSwitchCam.nodes[8].vNodePos = <<12.4154, -42.9466, -6.2632>>
		thisSwitchCam.nodes[8].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[8].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[8].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[8].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[8].vNodeDir = <<0.5307, -0.4107, 6.1259>>
		thisSwitchCam.nodes[8].bPointAtEntity = FALSE
		thisSwitchCam.nodes[8].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[8].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[8].fNodeFOV = 45.0000
		thisSwitchCam.nodes[8].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[8].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[8].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[8].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[8].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[8].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[8].iCamEaseType = 0
		thisSwitchCam.nodes[8].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[8].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[8].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[8].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[8].fTimeScale = 1.0000
		thisSwitchCam.nodes[8].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[8].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[8].bFlashEnabled = FALSE
		thisSwitchCam.nodes[8].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[8].fMinExposure = 0.0000
		thisSwitchCam.nodes[8].fMaxExposure = 0.0000
		thisSwitchCam.nodes[8].iRampUpDuration = 0
		thisSwitchCam.nodes[8].iRampDownDuration = 0
		thisSwitchCam.nodes[8].iHoldDuration = 0
		thisSwitchCam.nodes[8].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[8].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[8].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_EffectStrength = 0.0000


		thisSwitchCam.iNumNodes = 9
		thisSwitchCam.iCamSwitchFocusNode = 0
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Martin1_PlaneShotScene.txt"
		thisSwitchCam.strXMLFileName = "CameraInfo_Martin1_PlaneShotScene.xml"

		thisSwitchCam.bInitialized = TRUE
	ENDIF
		
	thisSwitchCam.viVehicles[0] = viPlane
		
ENDPROC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_TO_BIKE(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX &vi1, VEHICLE_INDEX &vi2)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_TO_BIKE")

	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<9.5194, 42.6350, -0.8512>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<7.8905, -0.0652, 5.4872>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].fNodeFOV = 30.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1000
		thisSwitchCam.nodes[1].vNodePos = <<14.4904, 30.6943, -3.5751>> // <<11.0786, 20.6597, -2.9302>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<2.4470, -0.7788, 2.6008>> // <<6.6681, 18.7737, 5.0256>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].fNodeFOV = 24.5 // 30.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 2.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 0.9000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.8000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 900
		thisSwitchCam.nodes[2].vNodePos = <<16.7361, 31.1127, -5.2246>> // <<10.8295, 19.1224, -5.0878>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<1.5855, -0.5112, 1.9870>> // <<7.4407, 17.9167, 2.6567>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].fNodeFOV = 24.5 // 30.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[2].iCamEaseType = 2
		thisSwitchCam.nodes[2].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 0.3000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[3].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[3].iNodeTime = 600
		thisSwitchCam.nodes[3].vNodePos =  <<24.6776, 34.6530, -18.4241>> // <<24.3269, 32.4904, -40.9953>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[3].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[3].vNodeDir = <<23.3133, 36.4863, 3.5606>> // <<42.9828, 33.9844, -7.5523>>
		thisSwitchCam.nodes[3].bPointAtEntity = TRUE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].fNodeFOV = 24.5 // 30.0000
		thisSwitchCam.nodes[3].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].iNodeToClone = 0
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[3].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[3].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[3].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 50
		thisSwitchCam.nodes[3].iRampDownDuration = 50
		thisSwitchCam.nodes[3].iHoldDuration = 100
		thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[4].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 0
		thisSwitchCam.nodes[5].vNodePos = <<0.9194, 2.7878, 6.4911>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<13.0044, 0.0000, 142.8329>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 40.0 // 50.0000
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 600
		thisSwitchCam.nodes[6].vNodePos = <<2.1044, 3.4443, 0.9265>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<0.1714, 0.3040, 1.6840>>
		thisSwitchCam.nodes[6].bPointAtEntity = TRUE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 40.0 // 50.0000
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[6].fNodeCamShake = 0.75 // 0.0000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = -0.4000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[7].iNodeTime = 700
		thisSwitchCam.nodes[7].vNodePos = <<1.3806, 2.0677, 0.2982>>
		thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[7].vNodeDir = <<-0.1853, 0.2172, 0.6513>>
		thisSwitchCam.nodes[7].bPointAtEntity = TRUE
		thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[7].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[7].fNodeFOV = 40.0 // 50.0000
		thisSwitchCam.nodes[7].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].iNodeToClone = 0
		thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[7].fNodeCamShake = 0.75 // 0.0000
		thisSwitchCam.nodes[7].iCamEaseType = 0
		thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[7].fTimeScale = 1.0000
		thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bFlashEnabled = FALSE
		thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[7].fMinExposure = 0.0000
		thisSwitchCam.nodes[7].fMaxExposure = 0.0000
		thisSwitchCam.nodes[7].iRampUpDuration = 0
		thisSwitchCam.nodes[7].iRampDownDuration = 0
		thisSwitchCam.nodes[7].iHoldDuration = 0
		thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[8].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[8].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[8].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[8].iNodeTime = 300
		thisSwitchCam.nodes[8].vNodePos = <<1.3811, 1.8374, 0.2559>>
		thisSwitchCam.nodes[8].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[8].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[8].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[8].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[8].vNodeDir = <<8.5434, 0.0000, 137.2264>>
		thisSwitchCam.nodes[8].bPointAtEntity = FALSE
		thisSwitchCam.nodes[8].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[8].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[8].fNodeFOV = 40.0 // 50.0000
		thisSwitchCam.nodes[8].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[8].iNodeToClone = 0
		thisSwitchCam.nodes[8].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[8].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[8].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[8].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[8].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[8].fNodeCamShake = 0.75 // 0.0000
		thisSwitchCam.nodes[8].iCamEaseType = 0
		thisSwitchCam.nodes[8].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[8].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[8].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[8].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[8].fTimeScale = 1.0000
		thisSwitchCam.nodes[8].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[8].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[8].bFlashEnabled = FALSE
		thisSwitchCam.nodes[8].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[8].fMinExposure = 0.0000
		thisSwitchCam.nodes[8].fMaxExposure = 0.0000
		thisSwitchCam.nodes[8].iRampUpDuration = 0
		thisSwitchCam.nodes[8].iRampDownDuration = 0
		thisSwitchCam.nodes[8].iHoldDuration = 0
		thisSwitchCam.nodes[8].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[8].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[8].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_EffectStrength = 0.0000



		thisSwitchCam.iNumNodes = 9
		thisSwitchCam.iCamSwitchFocusNode = 4
		thisSwitchCam.fSwitchSoundAudioStartPhase = -1.0000
		thisSwitchCam.fSwitchSoundAudioEndPhase = -1.0000
//		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
//		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
//		thisSwitchCam.bSwitchSoundPlayExitPulse = TRUE
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---



		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Martin1_JetToBike.txt"
		thisSwitchCam.strXMLFileName = "CameraInfo_Martin1_JetToBike.xml"

		thisSwitchCam.bInitialized = TRUE
	ENDIF
		
	thisSwitchCam.viVehicles[0] = vi1
	thisSwitchCam.viVehicles[1] = vi2
		
ENDPROC

#IF IS_DEBUG_BUILD

PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS(WIDGET_GROUP_ID wgidWidget)
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")
	
	SET_CURRENT_WIDGET_GROUP(wgidWidget)
	
	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables -")
		START_WIDGET_GROUP("Plane Shot Scene")
			ADD_WIDGET_INT_SLIDER("Plane Shot Scene Delay (after player has shot)", iPlaneShotSceneDelay, 0, 5000, 100)
			ADD_WIDGET_INT_SLIDER("Plane Engine Explosion Delay", iPlaneEngineExplosionDelay, 0, 30000, 100)
			ADD_WIDGET_INT_SLIDER("Plane Engine Pre Smoke On Delay", iPlaneEnginePreSmokeOnDelay, 0, 30000, 100)
			ADD_WIDGET_INT_SLIDER("Plane Engine Pre Smoke Off Delay", iPlaneEnginePreSmokeOffDelay, 0, 30000, 100)
			ADD_WIDGET_INT_SLIDER("Plane Engine Smoke Delay", iPlaneEngineSmokeDelay, 0, 30000, 100)
			ADD_WIDGET_INT_SLIDER("Plane Pilot Convo Delay", iPlanePilotConvoDelay, 0, 30000, 100)
			ADD_WIDGET_INT_SLIDER("Michael Shot Plane Convo Delay", iMichaelShotPlaneConvoDelay, 0, 30000, 100)
			ADD_WIDGET_INT_SLIDER("Trevor Track Plane Convo Delay", iTrevorTrackPlaneConvoDelay, 0, 30000, 100)
			START_WIDGET_GROUP("DGTitanEntry 1 Recording")
				ADD_WIDGET_FLOAT_SLIDER("Recording Playback Time", fDGTitanEntryRecording1Time, 0.0, 999999.0, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Recording Playback Speed", fDGTitanEntryRecording1Speed, 0.0, 5.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("DGTitanCrash 1 Recording")
				ADD_WIDGET_INT_SLIDER("Recording Start Node", iDGTitanCrashRecording1StartNode, 0, 20, 1)
				ADD_WIDGET_FLOAT_SLIDER("Recording Playback Time", fDGTitanCrashRecording1Time, 0.0, 999999.0, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Recording Playback Speed", fDGTitanCrashRecording1Speed, 0.0, 5.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("DGTitanCrash 2 Recording")
				ADD_WIDGET_INT_SLIDER("Recording Start Node", iDGTitanCrashRecording2StartNode, 0, 20, 1)
				ADD_WIDGET_FLOAT_SLIDER("Recording Playback Time", fDGTitanCrashRecording2Time, 0.0, 999999.0, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Recording Playback Speed", fDGTitanCrashRecording2Speed, 0.0, 5.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("DGTitanCrash 3 Recording")
				ADD_WIDGET_FLOAT_SLIDER("Recording Playback Time", fDGTitanCrashRecording3Time, 0.0, 999999.0, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Recording Playback Speed", fDGTitanCrashRecording3Speed, 0.0, 5.0, 0.1)
			STOP_WIDGET_GROUP()
			/*START_WIDGET_GROUP("Plane Fly By Shake")
				ADD_WIDGET_FLOAT_SLIDER("Start Phase", fPlaneFlyByShakeStartPhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Stop Phase", fPlaneFlyByShakeStopPhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Shake Scale", fPlaneFlyByShakeScale, 0.0, 10.0, 0.01)
			STOP_WIDGET_GROUP()*/
			START_WIDGET_GROUP("Engine Explosion PTFX")
				ADD_WIDGET_VECTOR_SLIDER("Offset", vPlaneEngineExplosionOffset, -10.0, 10.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", vPlaneEngineExplosionRot, -360.0, 360.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Scale", fPlaneEngineExplosionScale, 0.0, 10.0, 0.1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Plane To Bike")
			START_WIDGET_GROUP("Plane Recording")
				ADD_WIDGET_FLOAT_SLIDER("Plane Recording Playback Time", fBB_SOL_JETRecordingTime, 0.0, 999999.0, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Plane Recording Playback Speed", fBB_SOL_JETRecordingSpeed, 0.0, 5.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Bike Recording")
				ADD_WIDGET_FLOAT_SLIDER("Bike Recording Playback Time", fDGBFReadyRecordingTime, 0.0, 999999.0, 100.0)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_INT_SLIDER("Give Player Control Back Delay", iBikeRecordingStoppedDelay, 0, 999999, 100)
			ADD_WIDGET_INT_SLIDER("Trevor Look At Plane Delay", iTrevorLookAtPlaneDelay, 0, 999999, 100)
			//ADD_WIDGET_INT_SLIDER("Trevor Look At Plane Duration", iTrevorLookAtPlaneDuration, 0, 999999, 100)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	CLEAR_CURRENT_WIDGET_GROUP(wgidWidget)
	
ENDPROC

#ENDIF

FUNC BOOL IS_ENTITY_ALIVE(ENTITY_INDEX mEntity)
	IF DOES_ENTITY_EXIST(mEntity)
		IF NOT IS_ENTITY_DEAD(mEntity)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_TRYING_TO_DRIVE()
	//CDEBUG3LN(DEBUG_MISSION, "IS_PLAYER_TRYING_TO_DRIVE")
	
	INT iLX, iLY, iRX, iRY
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iLX, iLY, iRX, iRY)
	IF iLX > 0.5
	OR iLX < -0.5
	OR iLY > 0.5
	OR iLY < -0.5
		RETURN TRUE
	ENDIF
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT iSwitchTime
INT iTransitionTime
BOOL bDonePushIn
FUNC BOOL HANDLE_PLANE_SHOT_SWITCH_CAM(SWITCH_CAM_STRUCT &thisSwitchCam1, SWITCH_CAM_STRUCT &thisSwitchCam2)
	//CDEBUG3LN(DEBUG_MISSION, "HANDLE_PLANE_SHOT_SWITCH_CAM")
	
	INT iCurrentNode
	FLOAT fCamPhase
	
	SWITCH eSwitchCamState	
		
		CASE SWITCH_CAM_IDLE
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_START_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE1")

			SET_GAME_PAUSES_FOR_STREAMING(FALSE)
			
			DESTROY_ALL_CAMS()

			SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_SHOT_SCENE(thisSwitchCam1, vehTitan)
			CREATE_SPLINE_CAM(thisSwitchCam1)

			//recording starts here. Need to pause until recording is started
			SET_CAM_SPLINE_NODE_EXTRA_FLAGS(thisSwitchCam1.ciSpline, iDGTitanCrashRecording2StartNode + 1, CAM_SPLINE_NODE_FORCE_PAUSE)
			
			SET_CAM_ACTIVE(thisSwitchCam1.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)						
			SET_WIDESCREEN_BORDERS(TRUE, 500)	
						
			//Start plane recording "DGTitanEntry"
			IF DOES_ENTITY_EXIST(vehTitan)
				IF NOT IS_ENTITY_DEAD(vehTitan)
					IF IS_VEHICLE_DRIVEABLE(vehTitan)
						CDEBUG3LN(DEBUG_MISSION, "Starting DGTitanEntry 1 Recording...")
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE(vehTitan, 294, "DGTitanEntry")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, fDGTitanEntryRecording1Time)
						SET_PLAYBACK_SPEED(vehTitan, fDGTitanEntryRecording1Speed)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehTitan)
					ENDIF	
				ENDIF
			ENDIF
			
			bDGTitanCrashRecording2Started = FALSE
			bTrevorLookAtPlaneStarted = FALSE
			bPlaneSceneFX1Started = FALSE
			bPlaneSceneFX2Started = FALSE
			bWooshStarted = FALSE
			bWooshStopped = FALSE
			bDonePushIn = FALSE
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
		FALLTHRU
	
		CASE SWITCH_CAM_PLAYING_SPLINE1
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
			
			IF IS_CAM_ACTIVE(thisSwitchCam1.ciSpline)
				iCurrentNode = iCurrentNode
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam1)
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam1.ciSpline)
				
				/*IF NOT bPlaneFlyByShakeStarted
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam1.ciSpline) >= fPlaneFlyByShakeStartPhase
						CDEBUG3LN(DEBUG_MISSION, "Starting Plane Fly By Shake...")
						SHAKE_CAM(thisSwitchCam1.ciSpline, "SKY_DIVING_SHAKE", fPlaneFlyByShakeScale)
						bPlaneFlyByShakeStarted = TRUE
					ENDIF
				ENDIF
				IF NOT bPlaneFlyByShakeStopped
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam1.ciSpline) >= fPlaneFlyByShakeStopPhase
						CDEBUG3LN(DEBUG_MISSION, "Stopped Plane Fly By Shake...")
						STOP_CAM_SHAKING(thisSwitchCam1.ciSpline, TRUE)
						bPlaneFlyByShakeStopped = TRUE
					ENDIF
				ENDIF*/
				
				//Start plane recording "DGTitanCrash"
				IF NOT bDGTitanCrashRecording2Started
					IF IS_CAM_SPLINE_PAUSED(thisSwitchCam1.ciSpline)
						SET_CAM_SPLINE_NODE_EXTRA_FLAGS(thisSwitchCam1.ciSpline, iDGTitanCrashRecording2StartNode + 1, CAM_SPLINE_NODE_FORCE_NONE)
						IF DOES_ENTITY_EXIST(vehTitan)
							IF NOT IS_ENTITY_DEAD(vehTitan)
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
									CDEBUG3LN(DEBUG_MISSION, "Starting DGTitanCrash 2 Recording...")
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
									ENDIF
									START_PLAYBACK_RECORDED_VEHICLE(vehTitan, iRecNo, "DGTitanCrash")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, fDGTitanCrashRecording2Time)
									SET_PLAYBACK_SPEED(vehTitan, fDGTitanCrashRecording2Speed)
									FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehTitan)
								ENDIF	
							ENDIF
						ENDIF
						bDGTitanCrashRecording2Started = TRUE
					ENDIF
				ENDIF
				
				IF fCamPhase >= 1.0
				
					SET_TIME_SCALE(1.0)
					IF DOES_CAM_EXIST(thisSwitchCam1.ciSpline)
						IF IS_CAM_ACTIVE(thisSwitchCam1.ciSpline)
							DESTROY_CAM(thisSwitchCam1.ciSpline)
						ENDIF
					ENDIF
					DESTROY_ALL_CAMS()
					
					eSwitchCamState = SWITCH_CAM_START_SPLINE2
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
		FALLTHRU
		
		CASE SWITCH_CAM_START_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE2")

			RESET_PUSH_IN(pushInData)
			FILL_PUSH_IN_DATA(pushInData, vehBike, CHAR_TREVOR, 1.3, PUSH_IN_INTERP_TIME, PUSH_IN_CUT_TIME, PUSH_IN_POSTFX_TIME, PUSH_IN_SPEED_UP_TIME)
			SET_PUSH_IN_DIRECTION_MODIFIER(pushInData, <<22,0,5>>)

			//Activate a video recording from this point for 7 seconds into the past so it will show players final snipe and start of cutscene.
			REPLAY_RECORD_BACK_FOR_TIME(8.0)
			
			SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_TO_BIKE(thisSwitchCam2, vehTitan, vehBike)
			CREATE_SPLINE_CAM(thisSwitchCam2)

			SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, TRUE)
			
			//Start bike recording
			IF DOES_ENTITY_EXIST(vehBike)
				IF NOT IS_ENTITY_DEAD(vehBike)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike)
					ENDIF
					START_PLAYBACK_RECORDED_VEHICLE(vehBike, iRecNo, "DGBFReady")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike, fDGBFReadyRecordingTime)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike)
				ENDIF
			ENDIF
			
			//Start plane recording "DGTitanCrash"
			IF DOES_ENTITY_EXIST(vehTitan)
				IF NOT IS_ENTITY_DEAD(vehTitan)
					IF IS_VEHICLE_DRIVEABLE(vehTitan)
						CDEBUG3LN(DEBUG_MISSION, "Starting DGTitanCrash 3 Recording...")
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE(vehTitan, iRecNo, "DGTitanCrash")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, fDGTitanCrashRecording3Time)
						SET_PLAYBACK_SPEED(vehTitan, fDGTitanCrashRecording3Speed)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehTitan)
					ENDIF	
				ENDIF
			ENDIF
			
			iTransitionTime = -1
			iSwitchTime = GET_GAME_TIMER()
			SETTIMERA(0)
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2
		FALLTHRU
	
		CASE SWITCH_CAM_PLAYING_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2")
			PRINTSTRING("time = ") PRINTINT(GET_GAME_TIMER() - iSwitchTime) PRINTNL()
			
			IF IS_CAM_ACTIVE(thisSwitchCam2.ciSpline)
			
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam2)
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam2.ciSpline)
				
				IF NOT bWooshStarted
					IF fCamPhase >= fWooshStartPhase
						iWooshSoundID = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(iWooshSoundID, "In", "SHORT_PLAYER_SWITCH_SOUND_SET")
						bWooshStarted = TRUE
					ENDIF
				ENDIF
				
				IF NOT bWooshStopped
					IF fCamPhase >= fWooshStopPhase
						STOP_SOUND(iWooshSoundID)
						bWooshStopped = TRUE
					ENDIF
				ENDIF
				
				// bug 2004541
				IF NOT bPlaneSceneFX2Started
					IF GET_GAME_TIMER() >= iSwitchTime + 1565
						PRINTLN("Plane Scene FX2 Started...")
						ANIMPOSTFX_PLAY("SwitchShortTrevorMid", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "Hit_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET") // added in, switch SFX weren't playing before.
						bPlaneSceneFX2Started = TRUE
					ENDIF
				ENDIF
			
				//Trevor look at the plane
				IF NOT bTrevorLookAtPlaneStarted
					IF TIMERA() > iTrevorLookAtPlaneDelay
						IF bPlaneShotSceneStreamLoaded = TRUE
							STOP_STREAM()
							bPlaneShotSceneStreamLoaded = FALSE
						ENDIF
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	//							CLEAR_PED_TASKS(PLAYER_PED_ID())
								TASK_LOOK_AT_COORD(PLAYER_PED_ID(), (<<-393, 1223, 329>>), 2000, SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH) //iTrevorLookAtPlaneDuration
								PRINTSTRING("TASK_LOOK_AT_COORD being triggered") PRINTNL()
							ENDIF
						ENDIF
						TRIGGER_MUSIC_EVENT("SOL1_FRANKLIN_STARTS")
						PRINTSTRING("SOL1_FRANKLIN_STARTS being triggered") PRINTNL()
						bTrevorLookAtPlaneStarted = TRUE
					ENDIF
				ENDIF
				
				//FX
				IF NOT bPlaneSceneFX1Started
					IF fCamPhase >= fPlanceSceneFX1StartPhase
						PRINTLN("Plane Scene FX1 Started...")
						ANIMPOSTFX_PLAY("SwitchShortMichaelIn", 0, FALSE)
						// url:bugstar:2057569
						PLAY_SOUND_FRONTEND(-1,"Hit_Out","PLAYER_SWITCH_CUSTOM_SOUNDSET")
						iTransitionTime = GET_GAME_TIMER() + 200
						bPlaneSceneFX1Started = TRUE
					ENDIF
				ENDIF
				
				// url:bugstar:2057569
				IF iTransitionTime != -1
				AND GET_GAME_TIMER() >= iTransitionTime
					PRINTLN("Transition SFX triggered...")
					PLAY_SOUND_FRONTEND(-1,"Short_Transition_In","PLAYER_SWITCH_CUSTOM_SOUNDSET")
					iTransitionTime = -1
				ENDIF
				
				// bug 2004541
//				IF NOT bPlaneSceneFX2Started
//					IF fCamPhase >= fPlanceSceneFX2StartPhase
//						CDEBUG3LN(DEBUG_MISSION, "Plane Scene FX2 Started...")
//						ANIMPOSTFX_PLAY("SwitchShortTrevorMid", 0, FALSE)
//						bPlaneSceneFX2Started = TRUE
//					ENDIF
//				ENDIF

				IF fCamPhase >= 1.0
				
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) <> CAM_VIEW_MODE_FIRST_PERSON
						bDonePushIn = TRUE
					ELSE
						bDonePushIn = HANDLE_PUSH_IN(pushInData, TRUE, TRUE, TRUE, FALSE, FALSE)
					ENDIF
					
					IF bDonePushIn
						SET_TIME_SCALE(1.0)
						
						IF DOES_CAM_EXIST(thisSwitchCam2.ciSpline)
							IF IS_CAM_ACTIVE(thisSwitchCam2.ciSpline)
								DESTROY_CAM(thisSwitchCam2.ciSpline)
							ENDIF
						ENDIF
						DESTROY_ALL_CAMS()
					
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						

						
						STOP_SOUND(iWooshSoundID)
						
						//Start plane recording "BB_SOL_JET"
						IF DOES_ENTITY_EXIST(vehTitan)
							IF NOT IS_ENTITY_DEAD(vehTitan)
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
									CDEBUG3LN(DEBUG_MISSION, "Starting BB_SOL_JET Recording...")
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
									ENDIF
									START_PLAYBACK_RECORDED_VEHICLE(vehTitan, 1, "BB_SOL_JET")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, fBB_SOL_JETRecordingTime)
									SET_PLAYBACK_SPEED(vehTitan, fBB_SOL_JETRecordingSpeed)
									//FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehTitan)
									REMOVE_VEHICLE_RECORDING(iRecNo, "DGTitanCrash")
								ENDIF	
							ENDIF
						ENDIF
						
		//				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		//					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		//						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
		//					ENDIF
		//				ENDIF
		
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())
						ENDIF
						
						REMOVE_ANIM_DICT(strAnimDictCamShake)
						
						DISPLAY_HUD(TRUE)
						DISPLAY_RADAR(TRUE)
						SET_WIDESCREEN_BORDERS(FALSE, 500)

						SET_GAME_PAUSES_FOR_STREAMING(TRUE)
						
						eSwitchCamState = SWITCH_CAM_IDLE
						
						RETURN TRUE
					ENDIF
				ENDIF
	
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC






//PURPOSE: Sets values for variables that cannot be set during creation
PROC SetupArrays()					
	iStageSection									= 0
	iMiniCutStage									= 0
	
	#IF IS_DEBUG_BUILD
	SkipMenuStruct[0].sTxtLabel = "Initialise Mission"
	SkipMenuStruct[1].sTxtLabel	= "Stage: Drive to Observatory"
	SkipMenuStruct[2].sTxtLabel = "Stage: Shoot Down Plane"
	SkipMenuStruct[3].sTxtLabel = "Cutscene: Plane Starts Descent"
	SkipMenuStruct[4].sTxtLabel = "Stage: Chase Plane"
	SkipMenuStruct[5].sTxtLabel = "Stage: Kill Passengers"
	SkipMenuStruct[6].sTxtLabel = "CUTSCENE - Plane crash"
	SkipMenuStruct[7].sTxtLabel = "Stage: Destroy sniper"
	SkipMenuStruct[8].sTxtLabel = "CUTSCENE - End"
	SkipMenuStruct[9].sTxtLabel = "Passed Mission"
	#ENDIF
	
ENDPROC

//PURPOSE:		Requests resources to load before checkpoint script can start
PROC doCheckpointRequestLoad

ENDPROC
//PURPOSE:		Returns true if all requested resources have loaded before checkpoint script can start
FUNC BOOL hasCheckpointRequestLoaded
	RETURN TRUE
ENDFUNC

//═════════════════════════════════╡ CUTSCENE ╞═══════════════════════════════════

//PURPOSE: Moves a camera that's attached to a vehicle between Init and Dest, for as long as duration
PROC doChaseCam(	ENTITY_INDEX entTarget, ENTITY_INDEX entChaser,
					VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
					VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
					INT duration)
	IF NOT IS_ENTITY_DEAD(entTarget)
	AND NOT IS_ENTITY_DEAD(entChaser)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
		DETACH_CAM(camInit)
		STOP_CAM_POINTING(camInit)
		ATTACH_CAM_TO_ENTITY(camInit, entTarget, camInitCoord)
		POINT_CAM_AT_ENTITY(camInit, entTarget, camInitLookAt)
		SET_CAM_FOV(camInit, camInitFOV)
		SET_CAM_ACTIVE(camDest, FALSE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		DETACH_CAM(camDest)
		STOP_CAM_POINTING(camDest)
		ATTACH_CAM_TO_ENTITY(camDest, entTarget, camDestCoord)
		POINT_CAM_AT_ENTITY(camDest, entChaser, camDestLookAt)
		SET_CAM_FOV(camDest, camDestFOV)
		SET_CAM_ACTIVE(camDest, TRUE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
	ENDIF
ENDPROC



//═════════════════════════════════╡ TIDY UP ╞═══════════════════════════════════
PROC TidyUpPed(PED_INDEX pedTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(pedTidy)
		IF bImmediately
			/*IF NOT IS_ENTITY_DEAD(pedTidy)
				IF IS_ENTITY_ATTACHED(pedTidy)
					DETACH_ENTITY(pedTidy, FALSE)
				ENDIF
			ENDIF*/
			IF NOT IS_ENTITY_DEAD(pedTidy)
				SET_ENTITY_AS_MISSION_ENTITY(pedTidy)
			ENDIF
			DELETE_PED(pedTidy)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedTidy)
		ENDIF
	ENDIF
ENDPROC

PROC TidyUpVehicle(VEHICLE_INDEX vehTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(vehTidy)
		IF bImmediately
			IF NOT IS_ENTITY_DEAD(vehTidy)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTidy)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehTidy) + <<2,0,0>>)
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(vehTidy)
				IF IS_ENTITY_ATTACHED(vehTidy)
					DETACH_ENTITY(vehTidy, FALSE)
				ENDIF
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(vehTidy)
			DELETE_VEHICLE(vehTidy)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTidy)
		ENDIF
	ENDIF
ENDPROC

PROC TidyUpObject(OBJECT_INDEX objTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(objTidy)
		IF bImmediately
			IF NOT IS_ENTITY_DEAD(objTidy)
				IF IS_ENTITY_ATTACHED(objTidy)
					DETACH_ENTITY(objTidy, FALSE)
				ENDIF
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(objTidy)
			DELETE_OBJECT(objTidy)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(objTidy)
		ENDIF
	ENDIF
ENDPROC


//═════════════════════════════════╡ CLEANUP PROCEDURES ╞═══════════════════════════════════

//Removes all blips
PROC REMOVE_BLIPS()

	IF DOES_BLIP_EXIST(blipObj)
		REMOVE_BLIP(blipObj)
	ENDIF
	IF DOES_BLIP_EXIST(blipLocate)
		REMOVE_BLIP(blipLocate)
	ENDIF
	IF DOES_BLIP_EXIST(bikeBlip)
		REMOVE_BLIP(bikeBlip)
	ENDIF
	IF DOES_BLIP_EXIST(BlipTrevor)
		REMOVE_BLIP(BlipTrevor)
	ENDIF
	IF DOES_BLIP_EXIST(BlipMartinHouse)
		REMOVE_BLIP(BlipMartinHouse)
	ENDIF
	IF DOES_BLIP_EXIST(BlipSniperVan)
		REMOVE_BLIP(BlipSniperVan)
	ENDIF
	IF DOES_BLIP_EXIST(endGuyBlip[0])
		REMOVE_BLIP(endGuyBlip[0])
	ENDIF
	IF DOES_BLIP_EXIST(endGuyBlip[1])
		REMOVE_BLIP(endGuyBlip[1])
	ENDIF
	
ENDPROC

//PURPOSE: Stops dispatch types from spawning.
PROC ALLOW_DISPATCH(DISPATCH_TYPE dispatchType, BOOL bAllow)

      BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(dispatchType, (NOT bAllow))
      ENABLE_DISPATCH_SERVICE(dispatchType, bAllow)

ENDPROC

//PURPOSE:		Deletes or removes all elements of the script
PROC MissionTidyUp()
	
	#IF IS_DEBUG_BUILD
		IF NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE()
			WHILE NOT HAS_CUTSCENE_FINISHED()
				WAIT(0)
				PRINTSTRING("SOLOMON1_WAITING_32") PRINTNL()
			ENDWHILE
		ENDIF
	#ENDIF
	
	SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_RANCH, TRUE)	
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES()
	
	SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", false)
	
	//STOP the audio scene for chasing the plane
	IF PlaneOnfireSoundStopped = FALSE
		STOP_SOUND(PlaneonfireSoundID)
		PlaneOnfireSoundStopped = TRUE
	ENDIF
	
	DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, FALSE)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1367.5, 1122.5, 111.4>>, <<1377.8, 1138.7, 117.5>>, TRUE, TRUE)
	
	SET_MAX_WANTED_LEVEL(5)
	
	//Return vehicle generators to normal
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)	
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())
		SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_DRIVE_TO_OBSERVATORY")
		STOP_AUDIO_SCENE("MARTIN_1_DRIVE_TO_OBSERVATORY")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_SNIPER_CAMERA")
		STOP_AUDIO_SCENE("MARTIN_1_SNIPER_CAMERA")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CHASE_SCENE")
		STOP_AUDIO_SCENE("MARTIN_1_PLANE_CHASE_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_MAIN")
		STOP_AUDIO_SCENE("MARTIN_1_CHASE_MAIN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_01")
		STOP_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_01")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_TRAIN_SCENE")
		STOP_AUDIO_SCENE("MARTIN_1_TRAIN_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_TRAIN")
		STOP_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_TRAIN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_DUMP_TRUCK")
		STOP_AUDIO_SCENE("MARTIN_1_CHASE_DUMP_TRUCK")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CRASH")
		STOP_AUDIO_SCENE("MARTIN_1_PLANE_CRASH")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_DRIVE_BACK")
		STOP_AUDIO_SCENE("MARTIN_1_DRIVE_BACK")
	ENDIF

	IF DOES_BLIP_EXIST(endGuyBlip[0])
		REMOVE_BLIP(endGuyBlip[0])
	ENDIF	
	IF DOES_BLIP_EXIST(endGuyBlip[1])
		REMOVE_BLIP(endGuyBlip[1])
	ENDIF	
	IF DOES_BLIP_EXIST(blipObj)
		REMOVE_BLIP(blipObj)
	ENDIF	
	
	STOP_GAMEPLAY_HINT()
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	SET_TIME_SCALE(1.0)
	
	SET_ROADS_IN_AREA(<<105.7, 1683, 230>>, <<-11.8, 1818.4, 207>>, TRUE)
	SET_ROADS_IN_AREA(<<-9.5, 1758.3, 235.3>>, <<134.5, 1638.3, 222.5>>, TRUE)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<2299.7, 4870.2, 39>>, <<2307.0, 4894.2, 45>>, TRUE)
	
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	SET_STUNT_JUMPS_CAN_TRIGGER(TRUE)
	
	SET_GAME_PAUSES_FOR_STREAMING(TRUE)
	
	REMOVE_BLIPS()
	
	CANCEL_MUSIC_EVENT("SOL1_START")
	CANCEL_MUSIC_EVENT("SOL1_VEH")
	CANCEL_MUSIC_EVENT("SOL1_SNIPER_READY")
	CANCEL_MUSIC_EVENT("SOL1_APP_ACTIVE")
	CANCEL_MUSIC_EVENT("SOL1_ENGINE_HIT")
	CANCEL_MUSIC_EVENT("SOL1_FRANKLIN_STARTS")
	CANCEL_MUSIC_EVENT("SOL1_AIR_TRAFFIC")
	CANCEL_MUSIC_EVENT("SOL1_BUS_JUMP")
	CANCEL_MUSIC_EVENT("SOL1_TRAIN_JUMP")
	CANCEL_MUSIC_EVENT("SOL1_ALMOST_CRASHED")
	CANCEL_MUSIC_EVENT("SOL1_DRIVE_TO_OBS_RT")
	CANCEL_MUSIC_EVENT("SOL1_SHOOT_PLANE_RT")
	CANCEL_MUSIC_EVENT("SOL1_CHASE_PLANE_RT")

	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iLeftDoor)
		REMOVE_DOOR_FROM_SYSTEM(iLeftDoor)	
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iRightDoor)
		REMOVE_DOOR_FROM_SYSTEM(iRightDoor)	
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(AirstripBlockingArea)	
	REMOVE_SCENARIO_BLOCKING_AREA(CropFieldBlockingArea1)
	REMOVE_SCENARIO_BLOCKING_AREA(PoliceBikeBlockingArea)
	REMOVE_SCENARIO_BLOCKING_AREA(BarnBlockingArea)
	REMOVE_SCENARIO_BLOCKING_AREA(SeatNextToVanBlockingArea)
	REMOVE_SCENARIO_BLOCKING_AREA(MartinHouseMainScenarioBlockingArea)
	REMOVE_SCENARIO_BLOCKING_AREA(PlaneBlockingArea)
	
	CLEANUP_REMOTE_SNIPER(sSniperData)
	
	doEndCutscene()
		
	STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	IF IS_PLAYER_PLAYING(PLAYER_ID())	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	//re-enable all the police for the chase section
	ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE, TRUE)
	ALLOW_DISPATCH(DT_POLICE_HELICOPTER, TRUE)
	ALLOW_DISPATCH(DT_POLICE_VEHICLE_REQUEST, TRUE)
	ALLOW_DISPATCH(DT_POLICE_ROAD_BLOCK, TRUE)
	ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
	ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
	ALLOW_DISPATCH(DT_SWAT_AUTOMOBILE, TRUE)
	ALLOW_DISPATCH(DT_SWAT_HELICOPTER, TRUE)		
		
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)	
		
	SET_MAX_WANTED_LEVEL(5)

	SET_RANDOM_TRAINS(TRUE)
	PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_RANDOM_TRAINS(TRUE).")
	
	g_bDisableNoirLensEffect = FALSE

ENDPROC

PROC MissionCleanup()

	MissionTidyUp()
	UNREGISTER_SCRIPT_WITH_AUDIO()

	TERMINATE_THIS_THREAD()	
ENDPROC

//PURPOSE:		Performs necessary actions for player completing the mission, then calls cleanup
PROC MissionPassed()
	fadeInAndWait()
	//B* 1831413: Disable player switch if on a replay
	IF NOT IS_REPEAT_PLAY_ACTIVE()
		Trigger_Specific_Switch_And_Wait(PR_SCENE_F_WALKCHOP_a)
	ENDIF

	AWARD_ACHIEVEMENT_FOR_MISSION(ACH02)  // [AG] - A Fair Day's Pay
	TRIGGER_MUSIC_EVENT("SOL1_END")
	Mission_Flow_Mission_Passed()
	
	//Display generic pass text - "Mission Passed"
//	runGodText(Asset.godPass, TRUE, TRUE, FALSE)
//	Trigger_Specific_Switch_And_Wait(PR_SCENE_F_TAUNT)
	MissionCleanup()
ENDPROC

//PURPOSE:		Performs necessary actions for player failing the mission, then calls cleanup
PROC MissionFailed()	
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
			

	MissionCleanup() // must only take 1 frame and terminate the thread
ENDPROC

/// PURPOSE:
///    Sets bMissionFailed to be True so that we start fading for screen to fade out
///  	Sets the fail reason string and calls the flow mission failed function.
///    This tells the replay controller to start displaying the fail reason + fading
///    (If bMissionFailed is already true, this function doesn't do anything- as we're already waiting for fail fade to finish)
/// PARAMS:
///    sFailReason - text label of the fail reason
PROC SetMissionFailed(String sFailReason)
	IF bMissionFailed = FALSE
		
		//Give pilot task to fly plane if he is still in the air 
		IF DOES_ENTITY_EXIST(vehTitan)
			IF IS_VEHICLE_DRIVEABLE(vehTitan)
				IF DOES_ENTITY_EXIST(pedPilot)
					IF NOT IS_PED_INJURED(pedPilot)
						IF IS_PED_IN_VEHICLE(pedPilot, vehTitan)
							IF IS_ENTITY_IN_AIR(vehTitan)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehTitan, FALSE)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
									STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
								ENDIF
								CLEAR_PED_TASKS(pedPilot)
								TASK_PLANE_MISSION(pedPilot, vehTitan, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, <<0,0,100>>), MISSION_GOTO, 30, 1, GET_ENTITY_HEADING(vehTitan), ROUND(GET_ENTITY_HEIGHT_ABOVE_GROUND(vehTitan)), ROUND(GET_ENTITY_HEIGHT_ABOVE_GROUND(vehTitan)))
								SET_PED_KEEP_TASK(pedPilot, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		FailReason = sFailReason
		
		KILL_ANY_CONVERSATION()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
		CLEAR_PRINTS()
		
		TRIGGER_MUSIC_EVENT("SOL1_FAIL")
		
		MISSION_FLOW_MISSION_FAILED_WITH_REASON(FailReason)
		bMissionFailed = TRUE
	ENDIF
ENDPROC


//═════════════════════════════════╡ FAIL PROCEDURES ╞═══════════════════════════════════
//PURPOSE:		Checks if the user has failed a level stage
PROC FailCheck()

	SWITCH eMissionStage
	
		CASE STAGE_DRIVE_TO_OBSERVATORY
			//Fail if player tries to get in a taxi at this stage in the mission
//			IF iStageSection > 2
//				IF playerHasTakenTaxi = FALSE
//					IF IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
//						SetMissionFailed("BFFLOST") //Trevor lost the plane.
//					ENDIF
//				ENDIF
//			ENDIF
			
			//Fail if Trev dies
			IF DOES_ENTITY_EXIST(CSTrev)
				IF IS_PED_INJURED(CSTrev)
					SetMissionFailed("M1_FAIL3") //~s~Trevor died.
				ENDIF
			ENDIF
			
			//Fail if Trevors car is destroyed.
			IF DOES_ENTITY_EXIST(TrevCutsceneCar)
				IF NOT IS_VEHICLE_DRIVEABLE(TrevCutsceneCar, TRUE)
					SetMissionFailed("M1_FAIL5") //~s~Trevor's car was destroyed.
				ENDIF
			ENDIF
			
			//Fail if the van is destroyed
			IF DOES_ENTITY_EXIST(SniperVan)
				IF IS_ENTITY_ON_FIRE(SniperVan)
					SetMissionFailed("M1_FAIL1") //~s~The remote sniper was destroyed.
				ENDIF
				IF NOT IS_VEHICLE_DRIVEABLE(SniperVan)
					FREEZE_ENTITY_POSITION(SniperVan, FALSE)
					SetMissionFailed("M1_FAIL1") //~s~The remote sniper was destroyed.
				ENDIF
			ENDIF
		BREAK
		
		CASE STAGE_SHOOT_PLANE
			//Fail if player takes too long to snipe the plane
			IF bMissionFailedToSlow = FALSE
				IF bStageSetup = TRUE
					IF DOES_ENTITY_EXIST(vehTitan)	
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF NOT bDoRightEngineCut
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
			//						PRINTSTRING("plane recording is at ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehTitan)) PRINTNL()
									IF GET_TIME_POSITION_IN_RECORDING(vehTitan) > 33269
										bMissionFailedToSlow = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			IF bMissionFailedToSlow = TRUE
				SetMissionFailed("2SLOW") //~r~The plane got too close to the airport.
			ENDIF
		BREAK
		
		CASE STAGE_CHASE_PLANE
			//Fail if player gets too far away from the plane
			IF bStageSetup = TRUE
				IF NOT IS_ENTITY_DEAD(vehTitan)
				AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//					PRINTSTRING("Distance between player and plane is ") PRINTFLOAT(GET_DISTANCE_BETWEEN_ENTITIES(vehTitan, PLAYER_PED_ID())) PRINTNL()
					IF GET_DISTANCE_BETWEEN_ENTITIES(vehTitan, PLAYER_PED_ID()) > 750
						SetMissionFailed("BFFLOST") //~r~You lost the target.
					ENDIF
				ENDIF
				
				//If the van is destroyed 
				IF DOES_ENTITY_EXIST(SniperVan)
					IF NOT IS_VEHICLE_DRIVEABLE(SniperVan)
						FREEZE_ENTITY_POSITION(SniperVan, FALSE)
						SetMissionFailed("M1_FAIL1") //~s~The remote sniper was destroyed.
					ENDIF
				ENDIF
				
				//If michael is killed
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						SetMissionFailed("M1_FAIL4") //~s~Michael died.
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE STAGE_KILL_PASSENGERS
			
			IF bStageSetup = TRUE
				//Fail if player gets too far away from the plane
				IF DOES_ENTITY_EXIST(ObjTitan)
					IF GET_DISTANCE_BETWEEN_ENTITIES(ObjTitan, PLAYER_PED_ID()) > 400
						SetMissionFailed("BFFLOST") //~r~You lost the target.
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		
		CASE STAGE_DESTROY_SNIPER		
			
			//Fail if you get too far from the van
			IF iStageSection > 1
				IF DOES_ENTITY_EXIST(SniperVan)
					IF IS_VEHICLE_DRIVEABLE(SniperVan)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), SniperVan) > 300
								SetMissionFailed("M1_FAIL2") //~s~Michael abandoned the van.
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
	ENDSWITCH
	
	//Should be called every frame to ensure game fades out and fails.
	IF bMissionFailed = TRUE
		IF eMissionStage = STAGE_SHOOT_PLANE
//			IF DOES_ENTITY_EXIST(SniperVan)
//				IF IS_VEHICLE_DRIVEABLE(SniperVan)
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), SniperVan)
//					ENDIF
//				ENDIF
//			ENDIF
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			SET_REMOTE_SNIPER_ACTIVE(sSniperData,FALSE)
//			
//			CLEANUP_REMOTE_SNIPER(sSniperData)
//			//Set the flag to stop drawing the sniper hud to the screen now.
//			RemoteSniperNeedsUpdated = FALSE
		ENDIF
		IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			SET_REMOTE_SNIPER_ACTIVE(sSniperData,FALSE)
			
			CLEANUP_REMOTE_SNIPER(sSniperData)
			//Set the flag to stop drawing the sniper hud to the screen now.
			RemoteSniperNeedsUpdated = FALSE
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF DOES_ENTITY_EXIST(CSTrev)
					DELETE_PED(CSTrev)
				ENDIF
			ENDIF
			MissionFailed()
		ENDIF
	ENDIF
ENDPROC

//═════════════════════════════════╡ INITIALISING ╞═══════════════════════════════════


//═════════╡ GENERAL ╞═════════
PROC ADD_ALL_PEDS_TO_DIALOGUE()

	SWITCH eMissionStage
		
		CASE STAGE_CUTSCENE_PLANE_DESCENT
		
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
			IF DOES_ENTITY_EXIST(pedPilot)
				ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, pedPilot, "Solomon1Pilot")
			ENDIF
			//Null should be added for the Sol1Tower
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 5, NULL, "Sol1Tower")			
		
		BREAK
		
		CASE STAGE_CHASE_PLANE
			//Player should be added for Trev
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
			ENDIF
			//Null should be added for Michael
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, NULL, "MICHAEL")
			//pedPilot should be added for the pilot
			IF DOES_ENTITY_EXIST(pedPilot)
				ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, pedPilot, "Solomon1Pilot")
			ENDIF
			//Null should be added for the Sol1Tower
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 5, NULL, "Sol1Tower")
		BREAK
		
		CASE STAGE_CUTSCENE_PLANE_CRASH

			//Player should be added for Trev
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
			ENDIF			
			//Null should be added for Michael
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, NULL, "MICHAEL")
			
		BREAK
		
		CASE STAGE_DESTROY_SNIPER
		
			//Player should be added for Michael
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
			ELSE
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
					ENDIF
				ENDIF
			ENDIF
			
			//Null should be added for Trevor
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")			
		
		BREAK
	
	ENDSWITCH
	
ENDPROC


//#IF IS_DEBUG_BUILD
//PROC PRINT_VEHICLE_RECORD_TIME(VEHICLE_INDEX veh)
//	IF IS_VEHICLE_DRIVEABLE(veh)
//		IF IS_RECORDING_GOING_ON_FOR_VEHICLE(veh) AND bRecordingStarted = FALSE
//			PRINTNL()PRINTNL()
//			PRINTSTRING("VEH - START TIME:")PRINTFLOAT(fRecordingTime)PRINTNL()
//			PRINTNL()PRINTNL()
//			bRecordingStarted = TRUE
//		ENDIF
//	ENDIF
//ENDPROC
//#ENDIF

PROC RECORDING_TOOL()
	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
		fRecordingTime = GET_TIME_POSITION_IN_RECORDING(vehTitan)
		PRINTFLOAT(fRecordingTime)PRINTNL()
	ENDIF
	
	IF bStartRecordingTool = FALSE
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
			//START_PLAYBACK_RECORDED_VEHICLE(vehTarget,295,"Heli")
			//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTarget,0)
			//SET_PLAYBACK_SPEED(vehTarget,1.0)
		ENDIF
		bStartRecordingTool = TRUE
	ENDIF
	
	IF bTriggerRecording[0] = FALSE
		bTriggerRecording[0] = TRUE
	ENDIF
	
	//PRINT_VEHICLE_RECORD_TIME(vehFrontBodyguard)
	//TRIGGER BACK BODYGUARD
ENDPROC

////DEBUG ONLY
//#IF IS_DEBUG_BUILD
//PROC RESTORE_PLAYER_POSITION()
//	
//	VECTOR vCurrentPosition
//	FLOAT fHeading
//	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		IF IS_VEHICLE_DRIVEABLE(vehBike)
//			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
//				IF doneGetBackOnBikeHelpText = FALSE
//					CLEAR_HELP(TRUE)
//					PRINT_HELP_FOREVER("BFHELPTXT9")
//					//DEBUG ONLY Hold ~INPUT_COVER~ and press ~INPUT_HUD_SPECIAL~ to get back on bike.
//					doneGetBackOnBikeHelpText = TRUE
//				ENDIF
//			ELSE
//				IF doneGetBackOnBikeHelpText = TRUE
//					CLEAR_HELP(TRUE)
//					doneGetBackOnBikeHelpText = FALSE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_COVER)
//		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_HUD_SPECIAL)
//			vCurrentPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
//			fHeading = GET_ENTITY_HEADING(vehTitan)
//			SET_ENTITY_COORDS(vehBike, vCurrentPosition)
//			SET_ENTITY_HEADING(vehBike, fHeading)
//			SET_VEHICLE_ON_GROUND_PROPERLY(vehBike)
//			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBike)			
//		ENDIF
//				
//	ENDIF
//ENDPROC
//#ENDIF

//Infinite clips ftw beacause only noobs reload.
PROC INFINITE_AMMO_NO_RELOAD()
	INT iAmmo
	INT iMaxAmmo
	
	GET_AMMO_IN_CLIP(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,iAmmo)
	iMaxAmmo = GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
	
	IF iAmmo < 2
		SET_AMMO_IN_CLIP(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,iMaxAmmo)
	ENDIF
ENDPROC

FUNC BOOL HAS_TEXT_BEEN_TRIGGERED(MISSION_TEXT e_text)
	IF bTextTriggered[ENUM_TO_INT(e_text)]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MARK_TEXT_AS_TRIGGERED(MISSION_TEXT e_text)
	bTextTriggered[ENUM_TO_INT(e_text)] = TRUE
ENDPROC

PROC DIALOGUE()
	
	IF S1_FMC1_CHAT_DONE = FALSE
		IF fRecordingTime > 13662
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FMC1", CONV_PRIORITY_MEDIUM)
						//We're plugged in to the Air Traffic Control channel. Keep me updated.
						//Okey doke.
						TRIGGER_MUSIC_EVENT("SOL1_AIR_TRAFFIC")
						S1_FMC1_CHAT_DONE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	IF S1_OROAD_CHAT_DONE = FALSE
		IF fRecordingTime > 23659
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-254.383347,1558.426514,327.312714>>, <<-289.239960,1495.801147,342.381317>>, 26.000000)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-253.2, 1556.2, 335.9>>) < 10
							IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_OROAD", CONV_PRIORITY_MEDIUM)
								PREPARE_MUSIC_EVENT("SOL1_BUS_JUMP")
								//Plane ain't following the road - got to go cross-country.
								//Got to take this off-road, if I'm staying with the plane.
								S1_OROAD_CHAT_DONE = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF S1_JUMP1_CHAT_DONE = FALSE
		IF fRecordingTime > 24701
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1.431795,1704.114014,217.402771>>, <<-89.146202,1637.338379,276.547119>>, 26.000000)
							IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_JUMP1", CONV_PRIORITY_MEDIUM)
								//You think I can clear two lanes of traffic?
								//There's a small jump coming up.
								S1_JUMP1_CHAT_DONE = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF S1_FALL1_CHAT_DONE = FALSE
		IF fRecordingTime > 30963
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FALL1", CONV_PRIORITY_MEDIUM)
						//This is flight November Niner Charlie Echo. Our engine number two has blown.
						//Roger November Niner Charlie Echo. Fly heading two four zero. Say your souls on board.
						S1_FALL1_CHAT_DONE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF S1_FMC2_CHAT_DONE = FALSE
		IF fRecordingTime > 37858
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTitan) < 125
								IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FMC2", CONV_PRIORITY_MEDIUM)
									//The guy is fighting it, but he going down.
									//Good. Stay on him. We need the black box.
									S1_FMC2_CHAT_DONE = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

	IF S1_FALL2_CHAT_DONE = FALSE
		IF fRecordingTime > 40402
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FALL2", CONV_PRIORITY_MEDIUM)
						//Flight November Niner Charlie Echo. Say your souls on board?
						//Three souls on board. Engine two totally lost. All hydraulics lost. All hydraulic systems are gone. 
						S1_FALL2_CHAT_DONE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

	IF S1_FALL3_CHAT_DONE = FALSE
		IF fRecordingTime > 46889
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FALL3", CONV_PRIORITY_MEDIUM)
						//Roger November Niner Charlie Echo. Where are you putting her down?
						//We're not going to make the runway. Going to have to ditch this son of a bitch and hope for the best.
						S1_FALL3_CHAT_DONE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

	IF S1_FMC3_CHAT_DONE = FALSE
		IF fRecordingTime > 53000
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTitan) < 125
								IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FMC3", CONV_PRIORITY_MEDIUM)
									//How you doing out there. You still got him?
									//I'm on him. He's passed the airfield. Landing ain't gonna be pretty. 
									S1_FMC3_CHAT_DONE = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF		
	
	IF S1_FALL4_CHAT_DONE = FALSE
		IF fRecordingTime > 60000
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FALL4", CONV_PRIORITY_MEDIUM)
						//Where are you putting it down? Can you make The Alamo Sea Air Field?
						//Can't pull it round. Going to have to put it down in a field somewhere.
						PREPARE_MUSIC_EVENT("SOL1_TRAIN_JUMP")
						S1_FALL4_CHAT_DONE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF		

	IF S1_JUMP2_CHAT_DONE = FALSE
		IF fRecordingTime > 71768
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1496.083618,3259.302246,34.684143>>, <<1526.860107,3337.125000,45.199547>>, 40.000000)
							IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_JUMP2", CONV_PRIORITY_MEDIUM)
								//There's a small matter of having to jump a moving train. 
								//Can't go round the train, gotta go over it.
								IF IS_VEHICLE_DRIVEABLE(vehTrain)
									PLAY_SOUND_FROM_ENTITY(-1,"SUBWAY_TRAIN_HORNS_AIR_HORN",vehTrain)
								ENDIF
								S1_JUMP2_CHAT_DONE = TRUE
							ENDIF
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1538.228516,3252.864746,34.075874>>, <<1526.860107,3337.125000,45.199547>>, 40.000000)
							IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_JUMP2", CONV_PRIORITY_MEDIUM)
								//Fuck! I'm gonna hit this jump!
								IF IS_VEHICLE_DRIVEABLE(vehTrain)
									PLAY_SOUND_FROM_ENTITY(-1,"SUBWAY_TRAIN_HORNS_AIR_HORN",vehTrain)
								ENDIF
								S1_JUMP2_CHAT_DONE = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF
	
	IF S1_FALL5_CHAT_DONE = FALSE
		IF fRecordingTime > 76555
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FALL5", CONV_PRIORITY_MEDIUM)
						//We have no hydraulic systems. No elevator control. Very little aileron control. Serious doubts of making a landing strip. Need to ditch.
						//Roger November Niner Charlie Echo. Have you lost manual flight control systems? Pull your slats.
						S1_FALL5_CHAT_DONE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

	IF S1_TRAILER_CHAT_DONE = FALSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1970, 3815, 34>>) < 60
		AND fRecordingTime > 84000
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTitan) < 125
								IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_TRAILER", CONV_PRIORITY_MEDIUM)
									//Ron, you better be working!
									//How's business, Ronald?
									S1_TRAILER_CHAT_DONE = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF		

	IF S1_FMC4_CHAT_DONE = FALSE
		IF fRecordingTime > 90000
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTitan) < 125
								IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FMC4", CONV_PRIORITY_MEDIUM)
									//Okay, this guy's going down any minute.
									//Good. Let's hope he don't take out some poor farmer while he's at it.
									//Only thing they farm out here is methamphetamine.
									//Trevor Philips country.
									//God, I miss it out here. You got to come stay sometime.
									//You know what - I'm alright. 
									S1_FMC4_CHAT_DONE = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	IF S1_CLIFF_CHAT_DONE = FALSE
		IF fRecordingTime > 103000
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_CLIFF", CONV_PRIORITY_MEDIUM)
						//Losing altitude rapidly. I'm going to have to bring it down in dirt. God help us.
						//Roger November Niner Charlie Echo. What is your status?
						//God help us!
						TRIGGER_MUSIC_EVENT("SOL1_ALMOST_CRASHED")
						IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CRASH")
							START_AUDIO_SCENE("MARTIN_1_PLANE_CRASH")
						ENDIF
						S1_CLIFF_CHAT_DONE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF S1_FMC5_CHAT_DONE = FALSE
		IF fRecordingTime > 117262
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_FMC5", CONV_PRIORITY_MEDIUM)
						//They're in the dirt. 
						//Get the files, and if the cousin made it, take him out.  
						//Sure thing, but seriously bro - coming all the way out here, we gotta get paid. 
						//Just do the job, I'll get us paid down the road. 
						S1_FMC5_CHAT_DONE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: Handles the player looking at the plane at certain points in the chase.
PROC HANDLE_PLAYER_LOOKING_AT_PLANE()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(vehTitan)
			IF doneHeadLook[0] = FALSE
				IF fRecordingTime > 17960
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), vehTitan, 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
					PRINTSTRING("TASK_LOOK_AT_ENTITY CALLED FOR PLAYER TO LOOK AT PLANE") PRINTNL()
					doneHeadLook[0] = TRUE
				ENDIF
			ENDIF
			IF doneHeadLook[1] = FALSE
				IF fRecordingTime > 29010
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), vehTitan, 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
					PRINTSTRING("TASK_LOOK_AT_ENTITY CALLED FOR PLAYER TO LOOK AT PLANE") PRINTNL()
					doneHeadLook[1] = TRUE
				ENDIF
			ENDIF
			IF doneHeadLook[2] = FALSE
				IF fRecordingTime > 55700
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), vehTitan, 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
					PRINTSTRING("TASK_LOOK_AT_ENTITY CALLED FOR PLAYER TO LOOK AT PLANE") PRINTNL()
					doneHeadLook[2] = TRUE
				ENDIF
			ENDIF
			IF doneHeadLook[3] = FALSE
				IF fRecordingTime > 65565
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), vehTitan, 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
					PRINTSTRING("TASK_LOOK_AT_ENTITY CALLED FOR PLAYER TO LOOK AT PLANE") PRINTNL()
					doneHeadLook[3] = TRUE
				ENDIF
			ENDIF
			IF doneHeadLook[4] = FALSE
				IF fRecordingTime > 104000
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), vehTitan, 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
					PRINTSTRING("TASK_LOOK_AT_ENTITY CALLED FOR PLAYER TO LOOK AT PLANE") PRINTNL()
					doneHeadLook[4] = TRUE
				ENDIF
			ENDIF
			IF doneHeadLook[5] = FALSE
				IF fRecordingTime > 110000
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), vehTitan, 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
					PRINTSTRING("TASK_LOOK_AT_ENTITY CALLED FOR PLAYER TO LOOK AT PLANE") PRINTNL()
					doneHeadLook[5] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: Handles all the set piece vehicles during the chase
PROC SET_PIECES()
	
	//Set peice train for jump over train tracks
	IF trainModelsRequested = FALSE
		IF fRecordingTime > 60000
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCAR)
			REQUEST_MODEL(FREIGHTCONT2)
			trainModelsRequested = TRUE
		ENDIF
	ENDIF
	IF trainModelsRequested = TRUE
		IF bSetPiece[0] = FALSE
			IF fRecordingTime > 65173
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCAR)
			REQUEST_MODEL(FREIGHTCONT2)
				IF HAS_MODEL_LOADED(FREIGHT)
				AND HAS_MODEL_LOADED(FREIGHTCAR)
				AND HAS_MODEL_LOADED(FREIGHTCONT2)
					vehTrain = CREATE_MISSION_TRAIN(22, <<1800.49, 3504.19, 37.9>>, TRUE)
					PRINTSTRING("TRAIN TRIGGERED")PRINTNL()
					IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_TRAIN_SCENE")
						START_AUDIO_SCENE("MARTIN_1_TRAIN_SCENE")
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHT)
					SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCAR)
					SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT2)
					bSetPiece[0] = TRUE
				ELSE
					PRINTSTRING("Waiting on models loading for the train") PRINTNL()
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehTrain)
				IF NOT IS_ENTITY_DEAD(vehTrain)
					SET_TRAIN_SPEED(vehTrain, fPlaybackSpeed*33.3)
					IF fRecordingTime > 90000
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTrain) > 250
						AND NOT IS_ENTITY_ON_SCREEN(vehTrain)
							SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(vehTrain)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Set peice truck for bottom of hill
	IF g_bSetPieceCrossoverHasBeenSeen = FALSE
		IF HaulerRequested = FALSE
			IF fRecordingTime > 21000
				REQUEST_MODEL(HAULER)
				REQUEST_MODEL(A_M_Y_BUSINESS_01)
				REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_TRK")
				HaulerRequested = TRUE
			ENDIF
		ENDIF
		IF HaulerRequested = TRUE
			IF bSetPiece[1] = FALSE
				REQUEST_MODEL(HAULER)
				REQUEST_MODEL(A_M_Y_BUSINESS_01)
				REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_TRK")
				IF HAS_MODEL_LOADED(HAULER)
				AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "BB_SOL_TRK")
					IF fRecordingTime > 26640
						vehTruck = CREATE_VEHICLE(HAULER, << 716.3976, 2266.2209, 49.7731 >>, 31.2793)
						START_PLAYBACK_RECORDED_VEHICLE(vehTruck, iRecNo, "BB_SOL_TRK")
						PRINTSTRING("TRUCK TRIGGERED")PRINTNL()
						
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTruck, "MARTIN_1_PASSING_TRUCKS_GROUP")
						IF IS_VEHICLE_DRIVEABLE(vehTruck)
							RandomDriver[0] = CREATE_PED_INSIDE_VEHICLE(vehTruck, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RandomDriver[0], TRUE)
							SET_PED_KEEP_TASK(RandomDriver[0], TRUE)
							SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[0])
						ENDIF
						SET_MODEL_AS_NO_LONGER_NEEDED(HAULER)
						bSetPiece[1] = TRUE
					ENDIF
				ELSE
					PRINTSTRING("Waiting for Hauler model to load") PRINTNL()
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(vehTruck)
					IF IS_VEHICLE_DRIVEABLE(vehTruck)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)
							IF GET_TIME_POSITION_IN_RECORDING(vehTruck) > 15500
								g_bSetPieceCrossoverHasBeenSeen = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehTruck)
		IF IS_VEHICLE_DRIVEABLE(vehTruck)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)
//				PRINTSTRING("Truck playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehTruck)) PRINTNL()			
				SET_PLAYBACK_SPEED(vehTruck, fPlaybackSpeed)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(vehBike)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
							IF IS_ENTITY_TOUCHING_ENTITY(vehTruck, vehBike)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bSetPieceUnloaded[1] = FALSE
					REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_TRK")
					bSetPieceUnloaded[1] = TRUE
				ENDIF
				IF GET_DISTANCE_BETWEEN_ENTITIES(vehTruck, PLAYER_PED_ID()) > 150
				AND NOT IS_ENTITY_ON_SCREEN(vehTruck)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehTruck)
					IF DOES_ENTITY_EXIST(RandomDriver[0])
						DELETE_PED(RandomDriver[0])
					ENDIF
					DELETE_VEHICLE(vehTruck)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	//Set peice van for bottom of hill
	IF PatriotRequested = FALSE
		IF fRecordingTime > 29000
			REQUEST_MODEL(PATRIOT)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_VAN")
			PatriotRequested = TRUE
		ENDIF
	ENDIF
	IF PatriotRequested = TRUE
		IF bSetPiece[2] = FALSE
			REQUEST_MODEL(PATRIOT)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_VAN")
			IF HAS_MODEL_LOADED(PATRIOT)
			AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "BB_SOL_VAN")
				IF fRecordingTime > 34302
					vehVan = CREATE_VEHICLE(PATRIOT,  << 381.9736, 2468.7913, 45.1115 >>, 264.3281)
					START_PLAYBACK_RECORDED_VEHICLE(vehVan, iRecNo, "BB_SOL_VAN")
					
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehVan, "MARTIN_1_PASSING_TRUCKS_GROUP")
					IF IS_VEHICLE_DRIVEABLE(vehVan)
						RandomDriver[1] = CREATE_PED_INSIDE_VEHICLE(vehVan, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RandomDriver[1], TRUE)
						SET_PED_KEEP_TASK(RandomDriver[1], TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[1])
					ENDIF
					PRINTSTRING("VAN TRIGGERED")PRINTNL()
					SET_MODEL_AS_NO_LONGER_NEEDED(PATRIOT)
					bSetPiece[2] = TRUE
				ENDIF
			ELSE
				PRINTSTRING("Waiting for Patriot model to load") PRINTNL()
			ENDIF			
		ELSE
			IF DOES_ENTITY_EXIST(vehVan)
				IF IS_VEHICLE_DRIVEABLE(vehVan)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehVan)
	//						PRINTSTRING("Van playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehVan)) PRINTNL()
						SET_PLAYBACK_SPEED(vehVan, fPlaybackSpeed)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(vehBike)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
									IF IS_ENTITY_TOUCHING_ENTITY(vehVan, vehBike)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehVan)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF bSetPieceUnloaded[2] = FALSE
							REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_VAN")
							bSetPieceUnloaded[2] = TRUE
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(vehVan, PLAYER_PED_ID()) > 150
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehVan)
							IF DOES_ENTITY_EXIST(RandomDriver[1])
								DELETE_PED(RandomDriver[1])
							ENDIF
							DELETE_VEHICLE(vehVan)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Set piece bus for jump over road.
	IF busRequested = FALSE
		IF fRecordingTime > 20000
			REQUEST_MODEL(BUS)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_BUS")
			busRequested = TRUE
		ENDIF
	ENDIF
	IF busRequested = TRUE
		IF bSetPiece[3] = FALSE
			REQUEST_MODEL(BUS)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_BUS")
			IF HAS_MODEL_LOADED(BUS)
			AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "BB_SOL_BUS")
				IF fRecordingTime > 25650
					SET_ROADS_IN_AREA(<<105.7, 1683, 230>>, <<-11.8, 1818.4, 207>>, FALSE)
					vehBus = CREATE_VEHICLE(BUS, << 143.1083, 1650.0073, 227.9803 >>, 32.0527)
					START_PLAYBACK_RECORDED_VEHICLE(vehBus, iRecNo, "BB_SOL_BUS")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBus, 4000)
					IF IS_VEHICLE_DRIVEABLE(vehBus)
						RandomDriver[2] = CREATE_PED_INSIDE_VEHICLE(vehBus, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RandomDriver[2], TRUE)
						SET_PED_KEEP_TASK(RandomDriver[2], TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[2])
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(BUS)
					bSetPiece[3] = TRUE
				ENDIF
			ELSE
				PRINTSTRING("Waiting on bus models loading") PRINTNL()
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehBus)
				IF IS_VEHICLE_DRIVEABLE(vehBus)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBus)
	//					PRINTSTRING("Bus playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehBus)) PRINTNL()
						SET_PLAYBACK_SPEED(vehBus, fPlaybackSpeed*2.5)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(vehBike)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
									IF IS_ENTITY_TOUCHING_ENTITY(vehBus, vehBike)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehBus)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF bSetPieceUnloaded[3] = FALSE
							REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_BUS")
							bSetPieceUnloaded[3] = TRUE
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(vehBus, PLAYER_PED_ID()) > 150
						AND NOT IS_ENTITY_ON_SCREEN(vehBus)
							SET_ROADS_IN_AREA(<<105.7, 1683, 230>>, <<-11.8, 1818.4, 207>>, TRUE)
							IF DOES_ENTITY_EXIST(RandomDriver[2])
								DELETE_PED(RandomDriver[2])
							ENDIF
							DELETE_VEHICLE(vehBus)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Set peice baller just driving along road as traffic
	IF BallerRequested = FALSE
		IF fRecordingTime > 37000
			REQUEST_MODEL(BALLER)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_BLR")
			BallerRequested = TRUE
		ENDIF
	ENDIF
	IF BallerRequested = TRUE
		IF bSetPiece[4] = FALSE
			REQUEST_MODEL(BALLER)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_BLR")
			IF HAS_MODEL_LOADED(BALLER)
			AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "BB_SOL_BLR")
				IF fRecordingTime > 42784
					vehBaller = CREATE_VEHICLE(BALLER,  << 961.7518, 2697.2954, 39.2009 >>, 85.8753)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehBaller, TRUE)
					START_PLAYBACK_RECORDED_VEHICLE(vehBaller, iRecNo, "BB_SOL_BLR")
		//			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBaller, 4000)
					PRINTSTRING("BALLER TRIGGERED")PRINTNL()
					
					IF IS_VEHICLE_DRIVEABLE(vehBaller)
						RandomDriver[3] = CREATE_PED_INSIDE_VEHICLE(vehBaller, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RandomDriver[3], TRUE)
						SET_PED_KEEP_TASK(RandomDriver[3], TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[3])
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(BALLER)
					bSetPiece[4] = TRUE
				ENDIF
			ELSE
				PRINTSTRING("Waiting on bus models loading") PRINTNL()
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehBaller)
				IF IS_VEHICLE_DRIVEABLE(vehBaller)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBaller)
	//					PRINTSTRING("Baller playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehBaller)) PRINTNL()
						SET_PLAYBACK_SPEED(vehBaller, fPlaybackSpeed*1.5)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_DISTANCE_BETWEEN_ENTITIES(vehBaller, PLAYER_PED_ID()) < 80
								CHANGE_PLAYBACK_TO_USE_AI_AND_KEEP_SPEED(vehBaller)
							ENDIF
							IF fRecordingTime > 62784
							AND NOT IS_ENTITY_ON_SCREEN(vehBaller)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehBaller)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBaller)
								SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[3])
								REMOVE_VEHICLE_RECORDING(294, "BB_SOL_BLR")
							ENDIF
						ENDIF
					ELSE
						IF bSetPieceUnloaded[4] = FALSE
							REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_BLR")
							bSetPieceUnloaded[4] = TRUE
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(vehBaller, PLAYER_PED_ID()) > 150
						AND NOT IS_ENTITY_ON_SCREEN(vehBaller)
							IF DOES_ENTITY_EXIST(RandomDriver[3])
								DELETE_PED(RandomDriver[3])
							ENDIF
							DELETE_VEHICLE(vehBaller)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//Set peice Dilettante just driving along road as traffic
	IF DilettanteRequested = FALSE
		IF fRecordingTime > 33000
			REQUEST_MODEL(DILETTANTE)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_DIL")
			DilettanteRequested = TRUE
		ENDIF
	ENDIF
	IF DilettanteRequested = TRUE
		IF bSetPiece[5] = FALSE
			REQUEST_MODEL(DILETTANTE)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_DIL")
			IF HAS_MODEL_LOADED(DILETTANTE)
			AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "BB_SOL_DIL")
				IF fRecordingTime > 38832
					vehDilettante = CREATE_VEHICLE(DILETTANTE,  << 1373.6660, 2691.5361, 36.6655 >>, 101.0761)
					START_PLAYBACK_RECORDED_VEHICLE(vehDilettante, iRecNo, "BB_SOL_DIL")
		//			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehDilettante, 4000)
					PRINTSTRING("DILETTANTE TRIGGERED")PRINTNL()

					IF IS_VEHICLE_DRIVEABLE(vehDilettante)
						RandomDriver[4] = CREATE_PED_INSIDE_VEHICLE(vehDilettante, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RandomDriver[4], TRUE)
						SET_PED_KEEP_TASK(RandomDriver[4], TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[4])
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(DILETTANTE)
					bSetPiece[5] = TRUE
				ENDIF
			ELSE
				PRINTSTRING("Waiting on Dilettante models loading") PRINTNL()
			ENDIF			
		ELSE
			IF DOES_ENTITY_EXIST(vehDilettante)
				IF IS_VEHICLE_DRIVEABLE(vehDilettante)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDilettante)
	//					PRINTSTRING("DILETTANTE playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehDilettante)) PRINTNL()
						SET_PLAYBACK_SPEED(vehDilettante, fPlaybackSpeed)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_DISTANCE_BETWEEN_ENTITIES(vehDilettante, PLAYER_PED_ID()) < 80
								CHANGE_PLAYBACK_TO_USE_AI_AND_KEEP_SPEED(vehDilettante)
							ENDIF
						ENDIF
						IF fRecordingTime > 58832
						AND NOT IS_ENTITY_ON_SCREEN(vehDilettante)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehDilettante)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehDilettante)
							SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[4])
							REMOVE_VEHICLE_RECORDING(294, "BB_SOL_DIL")
						ENDIF
					ELSE
						IF bSetPieceUnloaded[5] = FALSE
							REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_DIL")
							bSetPieceUnloaded[5] = TRUE
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(vehDilettante, PLAYER_PED_ID()) > 150
						AND NOT IS_ENTITY_ON_SCREEN(vehDilettante)
							IF DOES_ENTITY_EXIST(RandomDriver[4])
								DELETE_PED(RandomDriver[4])
							ENDIF
							DELETE_VEHICLE(vehDilettante)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	//Set peice Bobcat just driving along road as traffic
//	IF bSetPiece[6] = FALSE
//		IF fRecordingTime > 34000
//			REQUEST_MODEL(BOBCATXL)
//			REQUEST_MODEL(A_M_Y_BUSINESS_01)
//			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_BOB")
//			
//			IF HAS_MODEL_LOADED(BOBCATXL)
//			AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "BB_SOL_BOB")
//				IF fRecordingTime > 39253
//					vehBobcat = CREATE_VEHICLE(BOBCATXL, << 1407.9412, 2702.5293, 36.5625 >>, 106.5234)
//					START_PLAYBACK_RECORDED_VEHICLE(vehBobcat, iRecNo, "BB_SOL_BOB")
//		//			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBobcat, 4000)
//					PRINTSTRING("BOBCAT TRIGGERED")PRINTNL()
//
//					IF IS_VEHICLE_DRIVEABLE(vehBobcat)
//						RandomDriver[5] = CREATE_PED_INSIDE_VEHICLE(vehBobcat, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RandomDriver[5], TRUE)
//						SET_PED_KEEP_TASK(RandomDriver[5], TRUE)
//						SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[5])
//					ENDIF
//					
//					SET_MODEL_AS_NO_LONGER_NEEDED(BOBCATXL)
//					bSetPiece[6] = TRUE
//				ENDIF
//			ELSE
//				PRINTSTRING("Waiting on Bobcat models loading") PRINTNL()
//			ENDIF			
//		ELSE
//			IF DOES_ENTITY_EXIST(vehBobcat)
//				IF IS_VEHICLE_DRIVEABLE(vehBobcat)
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBobcat)
//	//						PRINTSTRING("BOBCAT playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehBobcat)) PRINTNL()
//						SET_PLAYBACK_SPEED(vehBobcat, fPlaybackSpeed*1.1)
//						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//							IF GET_DISTANCE_BETWEEN_ENTITIES(vehBobcat, PLAYER_PED_ID()) < 80
//								CHANGE_PLAYBACK_TO_USE_AI_AND_KEEP_SPEED(vehBobcat)
//							ENDIF
//						ENDIF
//						IF fRecordingTime > 59253
//						AND NOT IS_ENTITY_ON_SCREEN(vehBobcat)
//							STOP_PLAYBACK_RECORDED_VEHICLE(vehBobcat)
//							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBobcat)
//							SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[5])
//							REMOVE_VEHICLE_RECORDING(294, "BB_SOL_BOB")
//						ENDIF
//					ELSE
//						IF bSetPieceUnloaded[6] = FALSE
//							REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_BOB")
//							bSetPieceUnloaded[6] = TRUE
//						ENDIF
//						IF GET_DISTANCE_BETWEEN_ENTITIES(vehBobcat, PLAYER_PED_ID()) > 150
//							IF DOES_ENTITY_EXIST(RandomDriver[5])
//								DELETE_PED(RandomDriver[5])
//							ENDIF
//							DELETE_VEHICLE(vehBobcat)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	//Set peice Tornado just driving along road as traffic
	IF TornadoRequested = FALSE
		IF fRecordingTime > 77000
			REQUEST_MODEL(TORNADO3)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_TOR")
			TornadoRequested = TRUE
		ENDIF
	ENDIF
	IF TornadoRequested = TRUE
		IF bSetPiece[7] = FALSE
			IF HAS_MODEL_LOADED(TORNADO3)
			AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "BB_SOL_TOR")	
				IF fRecordingTime > 82241
					vehTornado = CREATE_VEHICLE(TORNADO3,  << 1890.7859, 3826.8328, 31.3937 >>, 208.6624)
					START_PLAYBACK_RECORDED_VEHICLE(vehTornado, iRecNo, "BB_SOL_TOR")
					PRINTSTRING("TORNADO TRIGGERED")PRINTNL()

					IF IS_VEHICLE_DRIVEABLE(vehTornado)
						RandomDriver[6] = CREATE_PED_INSIDE_VEHICLE(vehTornado, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RandomDriver[6], TRUE)
						SET_PED_KEEP_TASK(RandomDriver[6], TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[6])
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(TORNADO3)
					bSetPiece[7] = TRUE
				ENDIF
			ELSE
				PRINTSTRING("Waiting on Tornado models loading") PRINTNL()
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehTornado)
				IF IS_VEHICLE_DRIVEABLE(vehTornado)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTornado)
	//						PRINTSTRING("TORNADO playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehTornado)) PRINTNL()
						SET_PLAYBACK_SPEED(vehTornado, fPlaybackSpeed*1.5)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(vehBike)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
									IF IS_ENTITY_TOUCHING_ENTITY(vehTornado, vehBike)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehTornado)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF bSetPieceUnloaded[7] = FALSE
							REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_TOR")
							bSetPieceUnloaded[7] = TRUE
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(vehTornado, PLAYER_PED_ID()) > 150
							IF DOES_ENTITY_EXIST(RandomDriver[6])
								DELETE_PED(RandomDriver[6])
							ENDIF
							DELETE_VEHICLE(vehTornado)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Set peice Bus2 just driving along road as traffic
	IF Bus2Requested = FALSE
		IF fRecordingTime > 89000
			REQUEST_MODEL(BUS)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_BUS2")
			Bus2Requested = TRUE
		ENDIF
	ENDIF
	IF Bus2Requested = TRUE
		IF bSetPiece[8] = FALSE
			REQUEST_MODEL(BUS)
			REQUEST_MODEL(A_M_Y_BUSINESS_01)
			REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_BUS2")
			IF HAS_MODEL_LOADED(BUS)
			AND HAS_MODEL_LOADED(A_M_Y_BUSINESS_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "BB_SOL_BUS2")			
				IF fRecordingTime > 94000
					vehBus2 = CREATE_VEHICLE(BUS,  << 2501.2251, 4119.9131, 37.4645 >>, 153.5773)
					START_PLAYBACK_RECORDED_VEHICLE(vehBus2, iRecNo, "BB_SOL_BUS2")
		//			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBus2, 4000)
					PRINTSTRING("BUS2 TRIGGERED")PRINTNL()

					IF IS_VEHICLE_DRIVEABLE(vehBus2)
						RandomDriver[7] = CREATE_PED_INSIDE_VEHICLE(vehBus2, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RandomDriver[7], TRUE)
						SET_PED_KEEP_TASK(RandomDriver[7], TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[7])
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(BUS)
					SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_BUSINESS_01)
					bSetPiece[8] = TRUE
				ENDIF
			ELSE
				PRINTSTRING("Waiting on Bus2 models loading") PRINTNL()
			ENDIF			
		ELSE
			IF DOES_ENTITY_EXIST(vehBus2)
				IF IS_VEHICLE_DRIVEABLE(vehBus2)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBus2)
	//						PRINTSTRING("BUS2 playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehBus2)) PRINTNL()
						SET_PLAYBACK_SPEED(vehBus2, fPlaybackSpeed*1.2)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_DISTANCE_BETWEEN_ENTITIES(vehBus2, PLAYER_PED_ID()) < 80
								CHANGE_PLAYBACK_TO_USE_AI_AND_KEEP_SPEED(vehBus2)
							ENDIF
						ENDIF
						IF fRecordingTime > 110000
						AND NOT IS_ENTITY_ON_SCREEN(vehBus2)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBus2)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBus2)
							SET_PED_AS_NO_LONGER_NEEDED(RandomDriver[7])
							REMOVE_VEHICLE_RECORDING(294, "BB_SOL_BUS2")
						ENDIF
					ELSE
						IF bSetPieceUnloaded[8] = FALSE
							REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_BUS2")
							bSetPieceUnloaded[8] = TRUE
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(vehBus2, PLAYER_PED_ID()) > 150
							IF DOES_ENTITY_EXIST(RandomDriver[7])
								DELETE_PED(RandomDriver[7])
							ENDIF
							DELETE_VEHICLE(vehBus2)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	//Set piece of DUMP truck at the water.
	IF DumpModelRequested = FALSE
		IF fRecordingTime > 65000
			REQUEST_MODEL(DUMP)
			REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
			REQUEST_VEHICLE_RECORDING(294, "BB_SOL_DMP")
			DumpModelRequested = TRUE
		ENDIF
	ELSE
		IF DumpSetPieceCreated = FALSE
			IF fRecordingTime > 70000
				IF HAS_MODEL_LOADED(DUMP)
				AND HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(294, "BB_SOL_DMP")
					//Create Truck
					DumpSetPiece = CREATE_VEHICLE(DUMP, << 2229.2375, 3861.1123, 32.4406 >>, 66.3172)
					SET_MODEL_AS_NO_LONGER_NEEDED(DUMP)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(DUMP, TRUE)
					//Create truck driver
					DumpSetPieceDriver = CREATE_PED_INSIDE_VEHICLE(DumpSetPiece, PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(DumpSetPieceDriver, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)
					
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(DumpSetPiece, "MARTIN_1_DUMP_TRUCK_GROUP")
					DumpSetPieceCreated = TRUE
				ELSE
					PRINTSTRING("Waiting for DUMP and S_M_Y_CONSTRUCT_01 models and recording loading") PRINTNL()
				ENDIF
			ENDIF
		ELSE
			IF DumpPlaybackStarted = FALSE
				IF fRecordingTime > 79600
					IF IS_VEHICLE_DRIVEABLE(DumpSetPiece)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(DumpSetPiece)
							START_PLAYBACK_RECORDED_VEHICLE(DumpSetPiece, 294, "BB_SOL_DMP")
							DumpPlaybackStarted = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//Start audio scene once truck is in view and stop once player has passed it
				IF DOES_ENTITY_EXIST(DumpSetPiece)
					IF IS_VEHICLE_DRIVEABLE(DumpSetPiece)
						IF fRecordingTime > 90000
							
							playersCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
							truckCoords = GET_ENTITY_COORDS(DumpSetPiece)

							IF playersCoords.x < truckCoords.x
								IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_DUMP_TRUCK")
									START_AUDIO_SCENE("MARTIN_1_CHASE_DUMP_TRUCK")
								ENDIF	
							ELSE
								IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_DUMP_TRUCK")
									STOP_AUDIO_SCENE("MARTIN_1_CHASE_DUMP_TRUCK")
								ENDIF
							ENDIF
							
						ENDIF
					ELSE
						IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_DUMP_TRUCK")
							STOP_AUDIO_SCENE("MARTIN_1_CHASE_DUMP_TRUCK")
						ENDIF
					ENDIF
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_DUMP_TRUCK")
						STOP_AUDIO_SCENE("MARTIN_1_CHASE_DUMP_TRUCK")
					ENDIF
				ENDIF
				
				IF DumpSetpieceSwitchedToAI = FALSE
					IF IS_VEHICLE_DRIVEABLE(DumpSetPiece)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(DumpSetPiece)
							SET_PLAYBACK_SPEED(DumpSetPiece, fPlaybackSpeed*1.2)
//							PRINTSTRING("DUMP SETPIECE RECORDING TIME IS ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(DumpSetPiece)) PRINTNL()
							IF GET_TIME_POSITION_IN_RECORDING(DumpSetPiece) > 52843
								CHANGE_PLAYBACK_TO_USE_AI_AND_KEEP_SPEED(DumpSetPiece)
								DumpSetpieceSwitchedToAI = TRUE
							ENDIF
							IF fRecordingTime > 100000
							AND NOT IS_ENTITY_ON_SCREEN(DumpSetPiece)
								REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(DumpSetPiece)
								STOP_PLAYBACK_RECORDED_VEHICLE(DumpSetPiece)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(DumpSetPiece)
								SET_PED_AS_NO_LONGER_NEEDED(DumpSetPieceDriver)
								REMOVE_VEHICLE_RECORDING(294, "BB_SOL_DMP")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(DumpSetPiece)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(DumpSetPiece)
						REMOVE_VEHICLE_RECORDING(294, "BB_SOL_DMP")
						IF IS_VEHICLE_DRIVEABLE(DumpSetPiece)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), DumpSetPiece) > 150
							AND NOT IS_ENTITY_ON_SCREEN(DumpSetPiece)
								IF DOES_ENTITY_EXIST(DumpSetPieceDriver)
									DELETE_PED(DumpSetPieceDriver)
								ENDIF
								REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(DumpSetPiece)
								DELETE_VEHICLE(DumpSetPiece)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//Set piece of guy pointing at the plane as it passes over head.			
	IF fRecordingTime > 45000
		IF IsParkedCarSetup = FALSE
			REQUEST_MODEL(BOBCATXL)
			IF HAS_MODEL_LOADED(BOBCATXL)
				IF NOT DOES_ENTITY_EXIST(ParkedCar)
					ParkedCar = CREATE_VEHICLE(BOBCATXL, <<754.9153, 2688.4556, 39.1427>>, 301.6810)
					SET_ENTITY_LOAD_COLLISION_FLAG(ParkedCar, TRUE)
					SET_VEHICLE_DOOR_OPEN(ParkedCar, SC_DOOR_FRONT_LEFT)
					SET_MODEL_AS_NO_LONGER_NEEDED(BOBCATXL)
					IsParkedCarSetup = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF IsPedPointingSetup = FALSE
			REQUEST_MODEL(S_M_Y_BARMAN_01)
			REQUEST_ANIM_DICT("missmartin1@pointing_sky1@base")
			IF HAS_MODEL_LOADED(S_M_Y_BARMAN_01)
				IF NOT DOES_ENTITY_EXIST(PointingPed[0])
					PointingPed[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_BARMAN_01, <<755.7743, 2691.3401, 39.0932>>, 260.5786)
					SET_ENTITY_LOAD_COLLISION_FLAG(PointingPed[0], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BARMAN_01)
					IsPedPointingSetup = TRUE
				ENDIF
			ENDIF	
		ENDIF
		IF IsPedPointingPlayingAnim = FALSE	
			IF IsPedPointingSetup = TRUE
				IF HAS_ANIM_DICT_LOADED("missmartin1@pointing_sky1@base")
					IF fRecordingTime > 50000
						IF NOT IS_PED_INJURED(PointingPed[0])
							IF NOT IS_ENTITY_PLAYING_ANIM(PointingPed[0], "missmartin1@pointing_sky1@base", "base")
								TASK_PLAY_ANIM(PointingPed[0], "missmartin1@pointing_sky1@base", "base")
								IsPedPointingPlayingAnim = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(PointingPed[0])
				IF NOT IS_PED_INJURED(PointingPed[0])
					IF NOT IS_ENTITY_PLAYING_ANIM(PointingPed[0], "missmartin1@pointing_sky1@base", "base")
						IF fRecordingTime > 52000	
							IF IS_VEHICLE_DRIVEABLE(ParkedCar)
								TASK_ENTER_VEHICLE(PointingPed[0], ParkedCar)
								SET_PED_KEEP_TASK(PointingPed[0], TRUE)
								SET_PED_AS_NO_LONGER_NEEDED(PointingPed[0])
								SET_VEHICLE_AS_NO_LONGER_NEEDED(ParkedCar)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//DO another set piece of a guy looking and pointing	
	IF fRecordingTime > 75000
		IF IsPedPointingSetup3 = FALSE
			REQUEST_MODEL(A_M_M_TRAMP_01)
			REQUEST_ANIM_DICT("missmartin1@pointing_sky1@base")
			IF HAS_MODEL_LOADED(A_M_M_TRAMP_01)
				IF NOT DOES_ENTITY_EXIST(PointingPed[1])
					PointingPed[1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_TRAMP_01, <<1663.0045, 3602.3420, 34.5846>>, 308.4162)
					SET_ENTITY_LOAD_COLLISION_FLAG(PointingPed[1], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_TRAMP_01)
					IsPedPointingSetup3 = TRUE
				ENDIF
			ENDIF	
		ENDIF
		IF IsPedPointingPlayingAnim3 = FALSE	
			IF IsPedPointingSetup3 = TRUE
				IF HAS_ANIM_DICT_LOADED("missmartin1@pointing_sky1@base")
					IF fRecordingTime > 78000
						IF NOT IS_PED_INJURED(PointingPed[1])
							IF NOT IS_ENTITY_PLAYING_ANIM(PointingPed[1], "missmartin1@pointing_sky1@base", "base")
								TASK_PLAY_ANIM(PointingPed[1], "missmartin1@pointing_sky1@base", "base")
								IsPedPointingPlayingAnim3 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(PointingPed[1])
				IF NOT IS_PED_INJURED(PointingPed[1])
					IF NOT IS_ENTITY_PLAYING_ANIM(PointingPed[1], "missmartin1@pointing_sky1@base", "base")
					OR fRecordingTime > 99000
						SET_PED_AS_NO_LONGER_NEEDED(PointingPed[1])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
		
	//DO another set piece of a guy looking and pointing	
	IF fRecordingTime > 100000
		IF IsPedPointingSetup2 = FALSE
			REQUEST_MODEL(S_M_Y_BARMAN_01)
			REQUEST_ANIM_DICT("missmartin1@pointing_sky1@base")
			IF HAS_MODEL_LOADED(S_M_Y_BARMAN_01)
				IF NOT DOES_ENTITY_EXIST(PointingPed[2])
					PointingPed[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_BARMAN_01, <<2672.2710, 4338.6265, 44.4309>>, 354.9540)
					SET_ENTITY_LOAD_COLLISION_FLAG(PointingPed[2], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BARMAN_01)
					IsPedPointingSetup2 = TRUE
				ENDIF
			ENDIF	
		ENDIF
		IF IsPedPointingPlayingAnim2 = FALSE	
			IF IsPedPointingSetup2 = TRUE
				IF HAS_ANIM_DICT_LOADED("missmartin1@pointing_sky1@base")
					IF fRecordingTime > 105000
						IF NOT IS_PED_INJURED(PointingPed[2])
							IF NOT IS_ENTITY_PLAYING_ANIM(PointingPed[2], "missmartin1@pointing_sky1@base", "base")
								TASK_PLAY_ANIM(PointingPed[2], "missmartin1@pointing_sky1@base", "base")
								IsPedPointingPlayingAnim2 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(PointingPed[2])
				IF NOT IS_PED_INJURED(PointingPed[2])
					IF NOT IS_ENTITY_PLAYING_ANIM(PointingPed[2], "missmartin1@pointing_sky1@base", "base")
						SET_PED_AS_NO_LONGER_NEEDED(PointingPed[2])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	//DO another set piece of a guy looking and pointing	
	IF fRecordingTime > 109000
		IF IsPedPointingSetup4 = FALSE
			REQUEST_MODEL(S_M_M_LatHandy_01)
			REQUEST_ANIM_DICT("missmartin1@pointing_sky1@base")
			IF HAS_MODEL_LOADED(S_M_M_LatHandy_01)
				IF NOT DOES_ENTITY_EXIST(PointingPed[3])
					PointingPed[3] = CREATE_PED(PEDTYPE_MISSION, S_M_M_LatHandy_01, <<2825.1704, 4616.9258, 45.5896>>, 14.0262)
					SET_ENTITY_LOAD_COLLISION_FLAG(PointingPed[3], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_LatHandy_01)
					IsPedPointingSetup4 = TRUE
				ENDIF
			ENDIF	
		ENDIF
		IF IsPedPointingPlayingAnim4 = FALSE	
			IF IsPedPointingSetup4 = TRUE
				IF HAS_ANIM_DICT_LOADED("missmartin1@pointing_sky1@base")
					IF fRecordingTime > 113000
						IF NOT IS_PED_INJURED(PointingPed[3])
							IF NOT IS_ENTITY_PLAYING_ANIM(PointingPed[3], "missmartin1@pointing_sky1@base", "base")
								TASK_PLAY_ANIM(PointingPed[3], "missmartin1@pointing_sky1@base", "base")
								IsPedPointingPlayingAnim4 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(PointingPed[3])
				IF NOT IS_PED_INJURED(PointingPed[3])
					IF NOT IS_ENTITY_PLAYING_ANIM(PointingPed[3], "missmartin1@pointing_sky1@base", "base")
						SET_PED_AS_NO_LONGER_NEEDED(PointingPed[3])
						REMOVE_ANIM_DICT("missmartin1@pointing_sky1@base")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
			
//	IF fRecordingTime > 65173
//		IF bSetPiece[4] = FALSE
//			FOR iCounter = 0 TO 1
//				IF DOES_ENTITY_EXIST(vehFeltzer[iCounter])
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehFeltzer[iCounter])
//				ENDIF
//			ENDFOR
//			IMMEDIATELY_UNLOAD_NEED_LIST_VEHICLE_RECORDING(Asset.recLeftCross, iRecNo)
//			IMMEDIATELY_UNLOAD_NEED_LIST_VEHICLE_RECORDING(Asset.recRightCross, iRecNo)
//			bSetPiece[4] = TRUE
//		ENDIF
//	ENDIF

ENDPROC

//Purpose skip to a specified time in 
PROC SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX veh, FLOAT fDGTime,BOOL bDGPausePlayback = FALSE)
	FLOAT fTemp
	
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
			fTemp = GET_TIME_POSITION_IN_RECORDING(veh)
			fTemp = fTemp * - 1 
//			PRINTSTRING("fTemp is:")PRINTFLOAT(fTemp)PRINTNL()
			fDGTime = fTemp + fDGTime 
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh,fDGTime)
//			PRINTSTRING("Skipping Playback forward to:")PRINTFLOAT(fDGTime)PRINTNL()
			IF bDGPausePlayback = TRUE
				PAUSE_PLAYBACK_RECORDED_VEHICLE(veh)
			ENDIF
		ELSE
			SCRIPT_ASSERT("NO PLAYBACK ON SELECTED VEHICLE- CANNOT SKIP TIME IN RECORDING")
		ENDIF
	ELSE
		SCRIPT_ASSERT("VEHICLE NOT DRIVABLE - CANNOT SKIP TIME IN RECORDING")
	ENDIF
ENDPROC

PROC setPlaneCrashed()
	IF DOES_ENTITY_EXIST(pedPilot)
		IF NOT IS_ENTITY_DEAD(pedPilot)
			IF IS_VEHICLE_DRIVEABLE(vehTitan)
				IF NOT IS_PED_IN_VEHICLE(pedPilot, vehTitan)
					SET_PED_INTO_VEHICLE(pedPilot, vehTitan)
				ENDIF
			ENDIF
			SET_ENTITY_HEALTH(pedPilot, 0)
		ENDIF
	ENDIF
	FREEZE_ENTITY_POSITION(vehTitan, FALSE)
	SET_ENTITY_VELOCITY(vehTitan, <<0, 0, 0>>)
	//SET_VEHICLE_ENGINE_HEALTH(vehTitan, -1000.0)
	//SET_ENTITY_HEALTH(vehTitan, 0)
	SET_VEHICLE_DOORS_LOCKED(vehTitan, VEHICLELOCK_LOCKED)
	SET_ENTITY_COORDS(vehTitan, vTitanCrashCoords)
	SET_ENTITY_ROTATION(vehTitan, vTitanSlideRot)
	SET_VEHICLE_DOOR_OPEN(vehTitan, SC_DOOR_FRONT_LEFT)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehTitan)
	FREEZE_ENTITY_POSITION(vehTitan, TRUE)
	SET_ENTITY_COLLISION(vehTitan, TRUE)
ENDPROC

//═════════════════════════════════╡ DEBUG PROCEDURES ╞═══════════════════════════════════
#IF IS_DEBUG_BUILD
//PURPOSE:		Moniters for any keyboard inputs that would alter the mission(debug pass etc)
PROC Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		eMissionStage = PASSED_MISSION
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		PREPARE_MUSIC_EVENT("SOL1_FAIL")
		SetMissionFailed("")
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		JSkip = TRUE
	ELSE
		JSkip = FALSE
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		PSkip = TRUE
	ELSE
		PSkip = FALSE
	ENDIF
	
ENDPROC

//FLOAT f_debug_thermal_x_modifier = 0.0
//FLOAT f_debug_thermal_y_modifier = 0.0

//PROC CREATE_WIDGETS()
//	widgetDebug = START_WIDGET_GROUP("Remote sniping mission")
//		//ADD_WIDGET_FLOAT_SLIDER("Thermal x modifier", f_debug_thermal_x_modifier, 0.001, 100.0, 0.001)
//		//ADD_WIDGET_FLOAT_SLIDER("Thermal y modifier", f_debug_thermal_y_modifier, 0.001, 100.0, 0.001)
//		START_WIDGET_GROUP("Uber Recording")
//			ADD_WIDGET_BOOL("Commence the uber recorder?", bUberRecord)
//			ADD_WIDGET_BOOL("GNARLY SHAKE CAM!?!", bGnarly)
//			ADD_WIDGET_INT_SLIDER("GNARLY SHAKE CAM!?!", iGnarlyStyle,0,2,1)
//		STOP_WIDGET_GROUP()
//		START_WIDGET_GROUP("Camera 0")
//			ADD_WIDGET_FLOAT_SLIDER("Pos0 X", pos0x, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Pos0 Y", pos0y, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Pos0 Z", pos0z, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Lookat0 X", lookat0x, -1000.0, 1000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Lookat0 Y", lookat0y, -1000.0, 1000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Lookat0 Z", lookat0z, -1000.0, 1000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("FOV0", fov0, 0.0, 1000.0, 0.001)
//		STOP_WIDGET_GROUP()
//		START_WIDGET_GROUP("Camera 1")
//			ADD_WIDGET_FLOAT_SLIDER("Pos1 X", pos1x, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Pos1 Y", pos1y, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Pos1 Z", pos1z, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Lookat1 X", lookat1x, -1000.0, 1000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Lookat1 Y", lookat1y, -1000.0, 1000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Lookat1 Z", lookat1z, -1000.0, 1000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("FOV1", fov1, 0.0, 1000.0, 0.001)
//		STOP_WIDGET_GROUP()
//	STOP_WIDGET_GROUP()
//	SET_LOCATES_HEADER_WIDGET_GROUP(widgetDebug)
//ENDPROC
//
////PURPOSE:		Updates debug widgets
//PROC UPDATE_WIDGETS()
//	IF bUberRecord
//		/*IF eMissionStage <> UBER_RECORD
//			bStageSetup = FALSE
//			eMissionStage = UBER_RECORD
//		ENDIF*/
//		bDoingJumpCutscene = TRUE
//		bDoneJumpCutscene = FALSE
//		iMiniCutStage = 0
//		bUberRecord = FALSE
//	ENDIF
//ENDPROC
//
//PROC DESTROY_WIDGETS()
//	IF DOES_WIDGET_GROUP_EXIST(widgetDebug)
//		DELETE_WIDGET_GROUP(widgetDebug)
//	ENDIF
//ENDPROC

#ENDIF

//PURPOSE: Delete's all entities to be used for skipping
PROC DELETE_EVERYTHING()

//═════════╡ CUTSCENE ╞═════════
	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE_IMMEDIATELY()
	ENDIF
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	SET_MAX_WANTED_LEVEL(5)
	
	REMOVE_CUTSCENE()
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)		
	
	SET_TIME_SCALE(1.0)

	//Move player out of any vehicle before it gets deleted
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
	ENDIF

//═════════╡ AUDIO ╞═════════
	IF PlaneOnfireSoundStopped = FALSE
		STOP_SOUND(PlaneonfireSoundID)
		PlaneOnfireSoundStopped = TRUE
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_DRIVE_TO_OBSERVATORY")
		STOP_AUDIO_SCENE("MARTIN_1_DRIVE_TO_OBSERVATORY")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_SNIPER_CAMERA")
		STOP_AUDIO_SCENE("MARTIN_1_SNIPER_CAMERA")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CHASE_SCENE")
		STOP_AUDIO_SCENE("MARTIN_1_PLANE_CHASE_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_MAIN")
		STOP_AUDIO_SCENE("MARTIN_1_CHASE_MAIN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_01")
		STOP_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_01")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_TRAIN_SCENE")
		STOP_AUDIO_SCENE("MARTIN_1_TRAIN_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_TRAIN")
		STOP_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_TRAIN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_DUMP_TRUCK")
		STOP_AUDIO_SCENE("MARTIN_1_CHASE_DUMP_TRUCK")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CRASH")
		STOP_AUDIO_SCENE("MARTIN_1_PLANE_CRASH")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_DRIVE_BACK")
		STOP_AUDIO_SCENE("MARTIN_1_DRIVE_BACK")
	ENDIF
	CANCEL_MUSIC_EVENT("SOL1_START")
	CANCEL_MUSIC_EVENT("SOL1_VEH")
	CANCEL_MUSIC_EVENT("SOL1_SNIPER_READY")
	CANCEL_MUSIC_EVENT("SOL1_APP_ACTIVE")
	CANCEL_MUSIC_EVENT("SOL1_ENGINE_HIT")
	CANCEL_MUSIC_EVENT("SOL1_FRANKLIN_STARTS")
	CANCEL_MUSIC_EVENT("SOL1_AIR_TRAFFIC")
	CANCEL_MUSIC_EVENT("SOL1_BUS_JUMP")
	CANCEL_MUSIC_EVENT("SOL1_TRAIN_JUMP")
	CANCEL_MUSIC_EVENT("SOL1_ALMOST_CRASHED")
	CANCEL_MUSIC_EVENT("SOL1_DRIVE_TO_OBS_RT")
	CANCEL_MUSIC_EVENT("SOL1_SHOOT_PLANE_RT")
	CANCEL_MUSIC_EVENT("SOL1_CHASE_PLANE_RT")
	
	STOP_STREAM()
	hasIsStreamPlaying 			= FALSE

//═════════╡ PTFX ╞═════════
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[0])
		STOP_PARTICLE_FX_LOOPED(ptfxEngine[0])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[1])
		STOP_PARTICLE_FX_LOOPED(ptfxEngine[1])
	ENDIF	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[2])
		STOP_PARTICLE_FX_LOOPED(ptfxEngine[2])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[3])
		STOP_PARTICLE_FX_LOOPED(ptfxEngine[3])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[4])
		REMOVE_PARTICLE_FX(ptfxEngine[4])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[5])
		REMOVE_PARTICLE_FX(ptfxEngine[5])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[6])
		REMOVE_PARTICLE_FX(ptfxEngine[6])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[7])
		REMOVE_PARTICLE_FX(ptfxEngine[7])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[8])
		REMOVE_PARTICLE_FX(ptfxEngine[8])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[9])
		REMOVE_PARTICLE_FX(ptfxEngine[9])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[10])
		REMOVE_PARTICLE_FX(ptfxEngine[10])
	ENDIF

//═════════╡ OBJECTS ╞═════════
	IF DOES_ENTITY_EXIST(RemoteSniper)
		DELETE_OBJECT(RemoteSniper)
	ENDIF
	IF DOES_ENTITY_EXIST(ObjTitan)
		DELETE_OBJECT(ObjTitan)
	ENDIF
	IF DOES_ENTITY_EXIST(objTrevsPhone)
		DELETE_OBJECT(objTrevsPhone)
	ENDIF
	IF DOES_ENTITY_EXIST(ObjEndBarrel)
		DELETE_OBJECT(ObjEndBarrel)
	ENDIF
	
//═════════╡ PEDS ╞═════════
	IF DOES_ENTITY_EXIST(pedPilot)
		DELETE_PED(pedPilot)
	ENDIF
//	IF DOES_ENTITY_EXIST(pedTarget)
//		DELETE_PED(pedTarget)
//	ENDIF
	IF DOES_ENTITY_EXIST(DumpSetPieceDriver)
		DELETE_PED(DumpSetPieceDriver)
	ENDIF
	IF DOES_ENTITY_EXIST(CSTrev)
		DELETE_PED(CSTrev)
	ENDIF
	IF DOES_ENTITY_EXIST(PedPatricia)
		DELETE_PED(PedPatricia)
	ENDIF
	IF DOES_ENTITY_EXIST(endGuy[0])
		DELETE_PED(endGuy[0])
	ENDIF
	IF DOES_ENTITY_EXIST(endGuy[1])
		DELETE_PED(endGuy[1])
	ENDIF
	FOR iCounter = 0 TO 3
		IF DOES_ENTITY_EXIST(PointingPed[iCounter])
			DELETE_PED(PointingPed[iCounter])
		ENDIF
	ENDFOR
	FOR iCounter = 0 TO 7
		IF DOES_ENTITY_EXIST(RandomDriver[iCounter])
			DELETE_PED(RandomDriver[iCounter])
		ENDIF
	ENDFOR
	
//═════════╡ ENTITYS ╞═════════
	IF DOES_ENTITY_EXIST(TrevsCarEntity)
		DELETE_ENTITY(TrevsCarEntity)
	ENDIF
	IF DOES_ENTITY_EXIST(TrevsEntity)
		DELETE_ENTITY(TrevsEntity)
	ENDIF
	IF DOES_ENTITY_EXIST(CutPatricia)
		DELETE_ENTITY(CutPatricia)
	ENDIF

//═════════╡ VEHICLES ╞═════════
	IF DOES_ENTITY_EXIST(vehTitan)
		DELETE_VEHICLE(vehTitan)
	ENDIF
	IF DOES_ENTITY_EXIST(vehBike)
		DELETE_VEHICLE(vehBike)
	ENDIF
	IF DOES_ENTITY_EXIST(PlayerCar)
		DELETE_VEHICLE(PlayerCar)
	ENDIF
	IF DOES_ENTITY_EXIST(vehTrain)
		DELETE_MISSION_TRAIN(vehTrain)
	ENDIF
	IF DOES_ENTITY_EXIST(vehBus)
		DELETE_VEHICLE(vehBus)
	ENDIF
	IF DOES_ENTITY_EXIST(vehTruck)
		DELETE_VEHICLE(vehTruck)
	ENDIF
	IF DOES_ENTITY_EXIST(vehVan)
		DELETE_VEHICLE(vehVan)
	ENDIF
	IF DOES_ENTITY_EXIST(vehBaller)
		DELETE_VEHICLE(vehBaller)
	ENDIF
	IF DOES_ENTITY_EXIST(vehDilettante)
		DELETE_VEHICLE(vehDilettante)
	ENDIF
	IF DOES_ENTITY_EXIST(vehBobcat)
		DELETE_VEHICLE(vehBobcat)
	ENDIF
	IF DOES_ENTITY_EXIST(vehTornado)
		DELETE_VEHICLE(vehTornado)
	ENDIF
	IF DOES_ENTITY_EXIST(vehBus2)
		DELETE_VEHICLE(vehBus2)
	ENDIF
	IF DOES_ENTITY_EXIST(ParkedCar)
		DELETE_VEHICLE(ParkedCar)
	ENDIF
	IF DOES_ENTITY_EXIST(SniperVan)
		DELETE_VEHICLE(SniperVan)
	ENDIF
	IF DOES_ENTITY_EXIST(vehBiff)
		DELETE_VEHICLE(vehBiff)
	ENDIF
	IF DOES_ENTITY_EXIST(DumpSetPiece)
		DELETE_VEHICLE(DumpSetPiece)
	ENDIF
	IF DOES_ENTITY_EXIST(PlayerStatCar)
		DELETE_VEHICLE(PlayerStatCar)
	ENDIF
	IF DOES_ENTITY_EXIST(TrevCutsceneCar)
		DELETE_VEHICLE(TrevCutsceneCar)
	ENDIF
	IF DOES_ENTITY_EXIST(ParkedCarAtStart)
		DELETE_VEHICLE(ParkedCarAtStart)
	ENDIF	
	IF DOES_ENTITY_EXIST(vehCutBike)
		DELETE_VEHICLE(vehCutBike)
	ENDIF	
	//Return vehicle generators to normal
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	
//═════════╡ VEHICLE RECORDINGS ╞═════════	
	REMOVE_VEHICLE_RECORDING(294, "DGTitanEntry")
	REMOVE_VEHICLE_RECORDING(iRecNo, "DGTitanCrash")
	REMOVE_VEHICLE_RECORDING(iRecNo, "DGBFReady")
	REMOVE_VEHICLE_RECORDING(1, "BB_SOL_JET")
	REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_TRK")
	REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_VAN")
	REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_BUS")
	REMOVE_VEHICLE_RECORDING(294, "BB_SOL_BLR")
	REMOVE_VEHICLE_RECORDING(294, "BB_SOL_DIL")
	REMOVE_VEHICLE_RECORDING(294, "BB_SOL_BOB")
	REMOVE_VEHICLE_RECORDING(iRecNo, "BB_SOL_TOR")
	REMOVE_VEHICLE_RECORDING(294, "BB_SOL_BUS2")
	REMOVE_VEHICLE_RECORDING(294, "BB_SOL_DMP")

//═════════╡ ANIM DICTS ╞═════════	
	REMOVE_ANIM_DICT("missmartin1@pointing_sky1@base")
	REMOVE_ANIM_DICT("missmartin1@exit_plane")
	REMOVE_ANIM_DICT("COMBAT@DAMAGE@WRITHEIDLE_A")
	REMOVE_ANIM_DICT("COMBAT@DAMAGE@WRITHEIDLE_B")

//═════════╡ TEXT ╞═════════	
	FOR iCounter = 0 TO 99
		bTextTriggered[iCounter] = FALSE
	ENDFOR
	
//═════════╡ DOORS ╞═════════		
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iLeftDoor)
		REMOVE_DOOR_FROM_SYSTEM(iLeftDoor)	
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iRightDoor)
		REMOVE_DOOR_FROM_SYSTEM(iRightDoor)	
	ENDIF
	
//═════════╡ CAMS ╞═════════
	IF DOES_CAM_EXIST(camDest)
		DESTROY_CAM(camDest)
	ENDIF
	IF DOES_CAM_EXIST(camInit)
		DESTROY_CAM(camInit)
	ENDIF
	IF DOES_CAM_EXIST(camAnim)
		DESTROY_CAM(camAnim)
	ENDIF
	
//═════════╡ REMOTE SNIPER ╞═════════	
	SET_REMOTE_SNIPER_ACTIVE(sSniperData,FALSE)
	
	CLEANUP_REMOTE_SNIPER(sSniperData)
	//Set the flag to stop drawing the sniper hud to the screen now.
	RemoteSniperNeedsUpdated = FALSE
	
	CLEAR_AREA(<<1388.6, 1150.0, 114>>, 70, TRUE)
	
ENDPROC

//PURPOSE: Requests and loads everything required for the missionstage
PROC REQUEST_AND_LOAD_EVERYTHING()
	
	REQUEST_PTFX_ASSET()
	
	WHILE NOT HAS_PTFX_ASSET_LOADED()
		PRINTSTRING("Waiting for PTFX asset to load") PRINTNL()
		WAIT(0)
	ENDWHILE

	MartinHouseMainScenarioBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<1333, 1095, 137>>, <<1438.9, 1197, 109>>)
	
	SWITCH eMissionStage
	
		CASE STAGE_INIT_MISSION	
			
			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_TREVOR
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
					IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
						REQUEST_MODEL(GET_REPLAY_START_VEHICLE_MODEL())
						WHILE NOT HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
					IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
						REQUEST_MODEL(GET_REPLAY_START_VEHICLE_MODEL())
						WHILE NOT HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
//					IF NOT IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
//					OR NOT IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
//					OR NOT IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
//					OR NOT IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())	
//					OR NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
//						REQUEST_MODEL(BODHI2)
//						WHILE NOT HAS_MODEL_LOADED(BODHI2)
//							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
				ENDIF
			ENDIF
			
			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
				//Check for Michael's car existing from start
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
					IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
						REQUEST_MODEL(GET_REPLAY_START_VEHICLE_MODEL())
						WHILE NOT HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
					IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
						REQUEST_MODEL(GET_REPLAY_START_VEHICLE_MODEL())
						WHILE NOT HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
				
//				//Request trev's car
//				REQUEST_MODEL(BODHI2)
//				WHILE NOT HAS_MODEL_LOADED(BODHI2)
//					PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
//					WAIT(0)
//				ENDWHILE
			ENDIF
			
			//Request and load the model for the parked car at the side of the house
			REQUEST_MODEL(FUSILADE)
			WHILE NOT HAS_MODEL_LOADED(FUSILADE)
				PRINTSTRING("waiting for fusilade model to load") PRINTNL()
				WAIT(0)
			ENDWHILE
		
		BREAK	
	
		CASE STAGE_DRIVE_TO_OBSERVATORY		
			
			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_TREVOR
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
					IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
						REQUEST_MODEL(GET_REPLAY_START_VEHICLE_MODEL())
						WHILE NOT HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
					IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
						REQUEST_MODEL(GET_REPLAY_START_VEHICLE_MODEL())
						WHILE NOT HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
//					IF NOT IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
//					OR NOT IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
//					OR NOT IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
//					OR NOT IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())	
//					OR NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
//						REQUEST_MODEL(BODHI2)
//						WHILE NOT HAS_MODEL_LOADED(BODHI2)
//							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
				ELSE
//					REQUEST_MODEL(BODHI2)
//					WHILE NOT HAS_MODEL_LOADED(BODHI2)
//						PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
//						WAIT(0)
//					ENDWHILE
				ENDIF
			ENDIF
			
			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
				//Check for Michael's car existing from start
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
					IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
						REQUEST_MODEL(GET_REPLAY_START_VEHICLE_MODEL())
						WHILE NOT HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
					IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
						REQUEST_MODEL(GET_REPLAY_START_VEHICLE_MODEL())
						WHILE NOT HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
				
//				//Request trev's car
//				REQUEST_MODEL(BODHI2)
//				WHILE NOT HAS_MODEL_LOADED(BODHI2)
//					PRINTSTRING("Waiting for checkpoint vehicle model loading") PRINTNL()
//					WAIT(0)
//				ENDWHILE
			ENDIF
			
			//Request and load the model for the parked car at the side of the house
			REQUEST_MODEL(FUSILADE)
			WHILE NOT HAS_MODEL_LOADED(FUSILADE)
				PRINTSTRING("waiting for fusilade model to load") PRINTNL()
				WAIT(0)
			ENDWHILE			
		
		BREAK
		
		CASE STAGE_SHOOT_PLANE
			
			IF IS_REPLAY_BEING_SET_UP()
				NEW_LOAD_SCENE_START(<<-892, -446, 173>>, <<0,0,-103>>, 3000)
			ENDIF
		
			REQUEST_REMOTE_SNIPER_ASSETS(sSniperData)
			REQUEST_MODEL(BURRITO3)
			REQUEST_MODEL(P_RCSS_FOLDED)
			REQUEST_VEHICLE_RECORDING(294, "DGTitanEntry")
			
			WHILE NOT HAS_MODEL_LOADED(BURRITO3)
			OR NOT HAS_MODEL_LOADED(P_RCSS_FOLDED)
			OR NOT HAVE_REMOTE_SNIPER_ASSETS_LOADED(sSniperData)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(294, "DGTitanEntry")
				PRINTSTRING("Waiting for models to laod for STAGE_SHOOT_PLANE") PRINTNL()
				WAIT(0)
			ENDWHILE			
		
		BREAK
		
		CASE STAGE_CUTSCENE_PLANE_DESCENT
		
			REQUEST_MODEL(SHAMAL)
			REQUEST_MODEL(BURRITO3)
			REQUEST_MODEL(P_RCSS_FOLDED)
			REQUEST_MODEL(S_M_M_PILOT_01)
			REQUEST_VEHICLE_RECORDING(294, "DGTitanEntry")
			REQUEST_VEHICLE_RECORDING(294, "DGBFReady")
			REQUEST_VEHICLE_RECORDING(1, "BB_SOL_JET")
			REQUEST_VEHICLE_RECORDING(iRecNo, "DGTitanCrash")
			
			WHILE NOT HAS_MODEL_LOADED(SHAMAL)
			OR NOT HAS_MODEL_LOADED(BURRITO3)
			OR NOT HAS_MODEL_LOADED(P_RCSS_FOLDED)
			OR NOT HAS_MODEL_LOADED(S_M_M_PILOT_01)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(294, "DGTitanEntry")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(294, "DGBFReady")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "BB_SOL_JET")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNo, "DGTitanCrash")
				PRINTSTRING("Waiting for models to laod for STAGE_CUTSCENE_PLANE_DESCENT") PRINTNL()
				WAIT(0)
			ENDWHILE	
			
			WHILE NOT LOAD_STREAM("MARTIN_1_LEAR_JET_SHOT_DOWN_MASTER") 
				PRINTSTRING("Waiting for audio stream MARTIN_1_LEAR_JET_SHOT_DOWN_MASTER loading") PRINTNL()
				WAIT(0)
			ENDWHILE
		
		BREAK
		
		CASE STAGE_CHASE_PLANE
		
			REQUEST_MODEL(SANCHEZ)
			REQUEST_MODEL(SHAMAL)
			REQUEST_MODEL(S_M_M_PILOT_01)
			REQUEST_PLAYER_PED_MODEL(CHAR_TREVOR)
			REQUEST_VEHICLE_RECORDING(1, "BB_SOL_JET")
			REQUEST_VEHICLE_RECORDING(294, "DGBFReady")
//			PRELOAD_STORED_PLAYER_PED_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			
			WHILE NOT HAS_MODEL_LOADED(SANCHEZ)
			OR NOT HAS_MODEL_LOADED(SHAMAL)
			OR NOT HAS_MODEL_LOADED(S_M_M_PILOT_01)
			OR NOT HAS_PLAYER_PED_MODEL_LOADED(CHAR_TREVOR)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "BB_SOL_JET")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(294, "DGBFReady")
				PRINTSTRING("Waiting for models to laod for STAGE_CHASE_PLANE") PRINTNL()
				WAIT(0)
			ENDWHILE		
		
		BREAK
		
		CASE STAGE_KILL_PASSENGERS
		
			IF GET_BUILDING_DATA(PlaneCrashTrench, BUILDINGNAME_IPL_PLANE_CRASH_TRENCH) 
				SET_BUILDING_STATE(BUILDINGNAME_IPL_PLANE_CRASH_TRENCH, BUILDINGSTATE_DESTROYED)
			ENDIF		
			
//			REQUEST_MODEL(SHAMAL)
			REQUEST_MODEL(SANCHEZ)
			REQUEST_MODEL(S_M_M_PILOT_01)
			REQUEST_MODEL(G_M_Y_MEXGANG_01)
			REQUEST_MODEL(G_M_Y_MEXGOON_02)
			REQUEST_MODEL(PROP_SHAMAL_CRASH)
			REQUEST_ANIM_DICT("missmartin1@exit_plane")
			REQUEST_ANIM_DICT("missmartin1@leadinoutmartin_1_dead_pilot")
		
//			WHILE NOT HAS_MODEL_LOADED(SHAMAL)
			WHILE NOT HAS_MODEL_LOADED(SANCHEZ)
			OR NOT HAS_MODEL_LOADED(S_M_M_PILOT_01)
			OR NOT HAS_MODEL_LOADED(G_M_Y_MEXGANG_01)
			OR NOT HAS_MODEL_LOADED(G_M_Y_MEXGOON_02)
			OR NOT HAS_MODEL_LOADED(PROP_SHAMAL_CRASH)
			OR NOT HAS_ANIM_DICT_LOADED("missmartin1@exit_plane")
			OR NOT HAS_ANIM_DICT_LOADED("missmartin1@leadinoutmartin_1_dead_pilot")
				PRINTSTRING("Waiting for models to laod for STAGE_CUTSCENE_PLANE_CRASH") PRINTNL()
				WAIT(0)
			ENDWHILE		
			
			//Request and load checkpoint vehicle if on a replay	
			IF IS_REPLAY_IN_PROGRESS()
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					IF NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
					AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
						WHILE NOT  HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
							PRINTSTRING("Waiting for replay checkpoint vehicle loading")
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
			ENDIF			

		BREAK		
		
		CASE STAGE_CUTSCENE_PLANE_CRASH
			
			REQUEST_CUTSCENE("Martin_1_mcs_1")
			REQUEST_MODEL(SANCHEZ)
			REQUEST_MODEL(PROP_SHAMAL_CRASH)
			REQUEST_MODEL(S_M_M_PILOT_01)
			
			WHILE NOT HAS_MODEL_LOADED(SANCHEZ)
			OR NOT HAS_MODEL_LOADED(PROP_SHAMAL_CRASH)
			OR NOT HAS_MODEL_LOADED(S_M_M_PILOT_01)
			OR NOT HAS_CUTSCENE_LOADED()
				PRINTSTRING("Waiting for models to laod for STAGE_CUTSCENE_PLANE_CRASH") PRINTNL()
				WAIT(0)
			ENDWHILE		
			
			IF GET_BUILDING_DATA(PlaneCrashTrench, BUILDINGNAME_IPL_PLANE_CRASH_TRENCH) 
				SET_BUILDING_STATE(BUILDINGNAME_IPL_PLANE_CRASH_TRENCH, BUILDINGSTATE_DESTROYED)
			ENDIF					
		
		BREAK
		
		CASE STAGE_DESTROY_SNIPER
		
			REQUEST_MODEL(BURRITO3)
			REQUEST_MODEL(P_RCSS_FOLDED)
			REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR))
			
			WHILE NOT HAS_MODEL_LOADED(BURRITO3)
			OR NOT HAS_MODEL_LOADED(P_RCSS_FOLDED)
			OR NOT HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR))
				PRINTSTRING("Waiting for vehicle recording and model to load") PRINTNL()
				WAIT(0)
			ENDWHILE		
			
			//Request and load checkpoint vehicle if on a replay	
			IF IS_REPLAY_IN_PROGRESS()
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					IF NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
					AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
						WHILE NOT  HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
							PRINTSTRING("Waiting for replay checkpoint vehicle loading")
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
			ENDIF			
		
		BREAK
		
		CASE STAGE_END_CUTSCENE

			REQUEST_ANIM_DICT("missmartin1@leadinoutmartin_1_ext")
			
			WHILE NOT HAS_ANIM_DICT_LOADED("missmartin1@leadinoutmartin_1_ext")
				PRINTSTRING("Waiting for anim dict to load") PRINTNL()
				WAIT(0)
			ENDWHILE		
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC manageSkip()

	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	DELETE_EVERYTHING()

	REMOVE_BLIPS()
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	REQUEST_AND_LOAD_EVERYTHING()
	
	SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
	
	g_bDisableNoirLensEffect = FALSE
	
	SWITCH eMissionStage
		CASE STAGE_INIT_MISSION
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<1392.3302, 1141.6107, 113.4430>>, 91.8877)
					WAIT(0)
					PRINTSTRING("SOLOMON1_WAITING_42") PRINTNL()
				ENDWHILE
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			ENDIF
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1386.8092, 1142.3643, 113.3340>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 82.7671)			
			ENDIF
			
			TRIGGER_MUSIC_EVENT("SOL1_DRIVE_TO_OBS_RT")
			
			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_TREVOR
				IF NOT DOES_ENTITY_EXIST(TrevCutsceneCar)
					IF IS_REPLAY_START_VEHICLE_AVAILABLE()
						IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
							CREATE_VEHICLE_FOR_REPLAY(TrevCutsceneCar, <<1360.3199, 1166.0765, 112.4975>>, 181.0991, FALSE, FALSE, FALSE, FALSE, FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
						ENDIF
						IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
						OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
						OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
						OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
							CREATE_VEHICLE_FOR_REPLAY(TrevCutsceneCar, <<1371.1974, 1146.6389, 112.7587>>, 177.6714, FALSE, FALSE, FALSE, FALSE, FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
						ENDIF
					ELSE
						WHILE NOT CREATE_PLAYER_VEHICLE(TrevCutsceneCar, CHAR_TREVOR, <<1371.1974, 1146.6389, 112.7587>>, 177.6714)
							PRINTSTRING("waiting on trevs car being created") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF			
			ENDIF
			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
					IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
						CREATE_VEHICLE_FOR_REPLAY(PlayerCar, <<1360.3199, 1166.0765, 112.4975>>, 181.0991, FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
					ENDIF
					IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
						CREATE_VEHICLE_FOR_REPLAY(PlayerCar, <<1372.0696, 1131.5979, 112.8844>>, 19.2193, FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
					ENDIF
				ENDIF
				IF NOT DOES_ENTITY_EXIST(TrevCutsceneCar)
					WHILE NOT CREATE_PLAYER_VEHICLE(TrevCutsceneCar, CHAR_TREVOR, <<1371.1974, 1146.6389, 112.7587>>, 177.6714)
						PRINTSTRING("waiting on trevs car being created 1") PRINTNL()
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
			
			//Create Trevor
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(CSTrev, CHAR_TREVOR, <<1386.1703, 1141.1138, 113.3340>>, 87.7877)
				PRINTSTRING("Creating Trevor") PRINTNL()
				WAIT(0)
			ENDWHILE
			
//			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_TORSO, 0, 4)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_LEG, 18, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_HAND, 0, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_FEET, 16, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_SPECIAL, 0, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_SPECIAL2, 0, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_DECL, 0, 0)	
//			ENDIF
			
			//Task Trev to walk to his car/heli and drive off
			IF DOES_ENTITY_EXIST(TrevCutsceneCar)
				IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
					SET_VEHICLE_DOORS_LOCKED(TrevCutsceneCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					OPEN_SEQUENCE_TASK(seqSequence)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CSTrev, TRUE)
						TASK_ENTER_VEHICLE(NULL, TrevCutsceneCar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
						IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
							TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, NULL, <<1360.3199, 1166.0765, 170>>, MISSION_GOTO, 20, 20, -1, 70, 60, -1)
							TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FLEE, 40, -1, -1, 100, 90)
						ENDIF
						IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
							TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
						ENDIF
					CLOSE_SEQUENCE_TASK(seqSequence)
					TASK_PERFORM_SEQUENCE(CSTrev, seqSequence)
					CLEAR_SEQUENCE_TASK(seqSequence)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(CSTrev)
				ENDIF
			ENDIF
			
			//Parked car at side of house	
			IF NOT DOES_ENTITY_EXIST(ParkedCarAtStart)
				ParkedCarAtStart = CREATE_VEHICLE(FUSILADE, <<1404.8268, 1118.4050, 113.8380>>, 90.5742)
				SET_VEHICLE_COLOURS(ParkedCarAtStart, 112, 112)
				SET_MODEL_AS_NO_LONGER_NEEDED(FUSILADE)
			ENDIF
			
		BREAK
		CASE STAGE_DRIVE_TO_OBSERVATORY

			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<1392.3302, 1141.6107, 113.4430>>, 91.8877)
					WAIT(0)
					PRINTSTRING("SOLOMON1_WAITING_43") PRINTNL()
				ENDWHILE
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE)
			ENDIF
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1386.8092, 1142.3643, 113.3340>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 82.7671)	
			ENDIF
			
			TRIGGER_MUSIC_EVENT("SOL1_DRIVE_TO_OBS_RT")
			
			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_TREVOR
				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					WHILE NOT CREATE_PLAYER_VEHICLE(PlayerCar, CHAR_MICHAEL, <<1372.0696, 1131.5979, 112.8844>>, 19.2193, TRUE, VEHICLE_TYPE_CAR)
						PRINTSTRING("waiting on michaels car being created for replay") PRINTNL()
						WAIT(0)
					ENDWHILE
				ENDIF
				IF NOT DOES_ENTITY_EXIST(TrevCutsceneCar)
					IF IS_REPLAY_START_VEHICLE_AVAILABLE()
						IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
							CREATE_VEHICLE_FOR_REPLAY(TrevCutsceneCar, <<1360.3199, 1166.0765, 112.4975>>, 181.0991, FALSE, FALSE, FALSE, FALSE, FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
						ENDIF
						IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
						OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
						OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
						OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
							CREATE_VEHICLE_FOR_REPLAY(TrevCutsceneCar, <<1371.1974, 1146.6389, 112.7587>>, 177.6714, FALSE, FALSE, FALSE, FALSE, FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
						ENDIF
					ELSE
						WHILE NOT CREATE_PLAYER_VEHICLE(TrevCutsceneCar, CHAR_TREVOR, <<1371.1974, 1146.6389, 112.7587>>, 177.6714)
							PRINTSTRING("waiting on trevs car being created 2") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF			
			ENDIF
			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
					IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
						CREATE_VEHICLE_FOR_REPLAY(PlayerCar, <<1360.3199, 1166.0765, 112.4975>>, 181.0991, FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
					ENDIF
					IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL())
					OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
						CREATE_VEHICLE_FOR_REPLAY(PlayerCar, <<1372.0696, 1131.5979, 112.8844>>, 19.2193, FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
					ENDIF
				ENDIF
				IF NOT DOES_ENTITY_EXIST(TrevCutsceneCar)
					WHILE NOT CREATE_PLAYER_VEHICLE(TrevCutsceneCar, CHAR_TREVOR, <<1371.1974, 1146.6389, 112.7587>>, 177.6714)
						PRINTSTRING("waiting on trevs car being created 3") PRINTNL()
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
			
			//Create Trevor
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(CSTrev, CHAR_TREVOR, <<1386.1703, 1141.1138, 113.3340>>, 87.7877)
				PRINTSTRING("Creating Trevor") PRINTNL()
				WAIT(0)
			ENDWHILE
//			IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_TORSO, 0, 4)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_LEG, 18, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_HAND, 0, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_FEET, 16, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_SPECIAL, 0, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_SPECIAL2, 0, 0)
//				SET_PED_COMPONENT_VARIATION(CSTrev, PED_COMP_DECL, 0, 0)
//			ENDIF
			
			//Task Trev to walk to his car/heli and drive off
			IF DOES_ENTITY_EXIST(TrevCutsceneCar)
				IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
					SET_VEHICLE_DOORS_LOCKED(TrevCutsceneCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					OPEN_SEQUENCE_TASK(seqSequence)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CSTrev, TRUE)
						TASK_ENTER_VEHICLE(NULL, TrevCutsceneCar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
						IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
							TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, NULL, <<1360.3199, 1166.0765, 170>>, MISSION_GOTO, 20, 20, -1, 70, 60, -1)
							TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FLEE, 40, -1, -1, 100, 90)
						ENDIF
						IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
							TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
						ENDIF
					CLOSE_SEQUENCE_TASK(seqSequence)
					TASK_PERFORM_SEQUENCE(CSTrev, seqSequence)
					CLEAR_SEQUENCE_TASK(seqSequence)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(CSTrev)
				ENDIF
			ENDIF	
						
			//Parked car at side of house			
			IF NOT DOES_ENTITY_EXIST(ParkedCarAtStart)
				ParkedCarAtStart = CREATE_VEHICLE(FUSILADE, <<1404.8268, 1118.4050, 113.8380>>, 90.5742)
				SET_VEHICLE_COLOURS(ParkedCarAtStart, 112, 112)
			ENDIF
			
			
		BREAK

		CASE STAGE_SHOOT_PLANE
			
			CLEAR_AREA(vSniperStartPos, 5, TRUE)
			CREATE_REMOTE_SNIPER(sSniperData, vSniperStartPos, f_sniper_heading)	
			LOCK_REMOTE_SNIPER_ROTATION(sSniperData, TRUE, -179, -70)
			LOCK_REMOTE_SNIPER_PITCH(sSniperData, -5, 20)
			SET_REMOTE_SNIPER_ACTIVE(sSniperData, TRUE)
			SET_REMOTE_SNIPER_ALLOW_QUIT(sSniperData, FALSE)
			
			IF NOT DOES_ENTITY_EXIST(SniperVan)
				SniperVan = CREATE_VEHICLE(BURRITO3, <<-363.5190, 1142.1714, 323.0888>>, 11.2577)
				SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_LEFT, FALSE, TRUE)
				SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_RIGHT, FALSE, TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(SniperVan, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(SniperVan)
				SET_VEHICLE_COLOURS(SniperVan, 0,0)
				SET_VEHICLE_DIRT_LEVEL(SniperVan, 10)
				SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO3)
				SET_VEHICLE_DISABLE_TOWING(SniperVan, TRUE)
				RemoteSniper = CREATE_OBJECT(P_RCSS_FOLDED, <<-363.5190, 1142.1714, 323.0888>>)
				ATTACH_ENTITY_TO_ENTITY(RemoteSniper, SniperVan, 0, << 0, -1.2, -0.3 >>, <<0,0,0>>)
				SET_MODEL_AS_NO_LONGER_NEEDED(P_RCSS_FOLDED)
			ENDIF			
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, SniperVan)
					WAIT(0)
					PRINTSTRING("SOLOMON1_WAITING_43") PRINTNL()
				ENDWHILE
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE)
			ENDIF
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				IF DOES_ENTITY_EXIST(SniperVan)
					IF IS_VEHICLE_DRIVEABLE(SniperVan)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), SniperVan)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
			TRIGGER_MUSIC_EVENT("SOL1_SHOOT_PLANE_RT")
			
		BREAK
		CASE STAGE_CUTSCENE_PLANE_DESCENT	
			
			IF NOT bCamShakeAnimDictRequested 
				REQUEST_ANIM_DICT(strAnimDictCamShake)
				bCamShakeAnimDictRequested = TRUE
			ENDIF	
			
			IF NOT DOES_ENTITY_EXIST(SniperVan)
				SniperVan = CREATE_VEHICLE(BURRITO3, <<138.8186, 1389.2057, 253.1271>>, 253.6382)
				SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_LEFT, FALSE, TRUE)
				SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_RIGHT, FALSE, TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(SniperVan, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(SniperVan)
				SET_VEHICLE_COLOURS(SniperVan, 0,0)
				SET_VEHICLE_DIRT_LEVEL(SniperVan, 10)
				SET_VEHICLE_DISABLE_TOWING(SniperVan, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO3)
				RemoteSniper = CREATE_OBJECT(P_RCSS_FOLDED, <<-363.5190, 1142.1714, 323.0888>>)
				ATTACH_ENTITY_TO_ENTITY(RemoteSniper, SniperVan, 0, << 0, -1.2, -0.3 >>, <<0,0,0>>)
				SET_MODEL_AS_NO_LONGER_NEEDED(P_RCSS_FOLDED)
			ENDIF						
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, SniperVan)
					WAIT(0)
					PRINTSTRING("SOLOMON1_WAITING_49") PRINTNL()
				ENDWHILE
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE)
			ENDIF			
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				IF DOES_ENTITY_EXIST(SniperVan)
					IF IS_VEHICLE_DRIVEABLE(SniperVan)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), SniperVan)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF NOT DOES_ENTITY_EXIST(vehTitan)
				vehTitan = CREATE_VEHICLE(SHAMAL, << -1638.6176, -2734.4397, 12.8201 >>, 327.2515)
				CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)
				SET_ENTITY_INVINCIBLE(vehTitan, TRUE)
				SET_VEHICLE_LIVERY(vehTitan, 1)
				SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
				SET_VEHICLE_ENGINE_ON(vehTitan, TRUE, TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTitan)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
					START_PLAYBACK_RECORDED_VEHICLE(vehTitan, 294, "DGTitanEntry")
					SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, 5000)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedPilot)
				pedPilot = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, S_M_M_PILOT_01)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_LEG, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_FEET, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_JBIB, 0, 0)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
			ENDIF

			//Load the camera's for the cutscene
			SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_SHOT_SCENE(scsPlaneShotScene, vehTitan)
			SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_TO_BIKE(scsPlaneToBike, vehTitan, vehBike)
			
			WHILE NOT HAS_ANIM_DICT_LOADED(strAnimDictCamShake)
				PRINTSTRING("waiting for camera's to load") PRINTNL()
				WAIT(0)
			ENDWHILE

//			REQUEST_REMOTE_SNIPER_ASSETS(sSniperData)
//			WHILE NOT HAVE_REMOTE_SNIPER_ASSETS_LOADED(sSniperData)
//				WAIT(0)
//				PRINTSTRING("SOLOMON1_WAITING_50") PRINTNL()
//			ENDWHILE
//			CLEAR_AREA(vSniperStartPos, 5, TRUE)
//			CREATE_REMOTE_SNIPER(sSniperData, vSniperStartPos, f_sniper_heading)
//			
//			SET_REMOTE_SNIPER_ACTIVE(sSniperData, TRUE)
			
		BREAK
		CASE STAGE_CHASE_PLANE
			
			IF NOT DOES_ENTITY_EXIST(vehBike)
				vehBike = CREATE_VEHICLE(SANCHEZ,vBFChaseStartCoords, fBFChaseStartHeading)
			ENDIF		
			SET_VEHICLE_ENGINE_ON(vehBike, TRUE, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(SANCHEZ)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
				WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vehBike)
					WAIT(0)
					PRINTSTRING("SOLOMON1_WAITING_49") PRINTNL()
				ENDWHILE
				
//				IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 0, 4)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 18, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAND, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_FEET, 16, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL2, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_DECL, 0, 0)
//				ENDIF
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
				
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE)
			ENDIF						
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				IF IS_VEHICLE_DRIVEABLE(vehBike)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBike)
					ENDIF
				ENDIF
			ENDIF
				
//			GIVE_PED_HELMET(PLAYER_PED_ID(), FALSE)			

			IF NOT DOES_ENTITY_EXIST(vehTitan)
				vehTitan = CREATE_VEHICLE(SHAMAL, << -1638.6176, -2734.4397, 12.8201 >>, 327.2515)
				CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)
				SET_ENTITY_INVINCIBLE(vehTitan, TRUE)
				SET_VEHICLE_LIVERY(vehTitan, 1)
				START_PLAYBACK_RECORDED_VEHICLE(vehTitan, 1, "BB_SOL_JET")
				SET_PLAYBACK_SPEED(vehTitan,0.4)
				SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, 13662)
				SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
				SET_VEHICLE_ENGINE_ON(vehTitan, TRUE, TRUE)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedPilot)
				pedPilot = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, S_M_M_PILOT_01)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_LEG, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_FEET, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_JBIB, 0, 0)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehBike)
				START_PLAYBACK_RECORDED_VEHICLE(vehBike, iRecNo, "DGBFReady")
				SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike, 18400)
			ENDIF			
			
			TRIGGER_MUSIC_EVENT("SOL1_CHASE_PLANE_RT")
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						
		BREAK
		CASE STAGE_KILL_PASSENGERS
		
			//Should be still on the sanchez on replay commenting this out for bug 1908651
			//Create players car and put him in it.
			/*IF IS_REPLAY_IN_PROGRESS()
				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						IF NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
							PlayerCar = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<2776.9570, 4740.3716, 44.9082>>, 341.7251)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						ELSE
							PlayerCar = CREATE_VEHICLE(SANCHEZ, <<2776.9570, 4740.3716, 44.9082>>, 341.7251)
						ENDIF
					ELSE
						PlayerCar = CREATE_VEHICLE(SANCHEZ, <<2776.9570, 4740.3716, 44.9082>>, 341.7251)
					ENDIF	
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(SANCHEZ)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
				ENDIF
			ELSE*/
				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					PlayerCar = CREATE_VEHICLE(SANCHEZ, <<2776.9570, 4740.3716, 44.9082>>, 341.7251)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(SANCHEZ)
				ENDIF
			//ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
				WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, PlayerCar)
					WAIT(0)
					PRINTSTRING("SOLOMON1_WAITING_61") PRINTNL()
				ENDWHILE
//				IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 0, 4)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 18, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAND, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_FEET, 16, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL2, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_DECL, 0, 0)	
//				ENDIF
				
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE)
			ENDIF
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), PlayerCar)
				ENDIF
			ENDIF
			
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
//				IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(PlayerCar))
//					SET_PED_HELMET(PLAYER_PED_ID(), FALSE)
//				ENDIF
//			ENDIF
			
			SET_VEHICLE_ENGINE_ON(PlayerCar, TRUE, TRUE)
			
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
		
			//Original location
			//ObjTitan = CREATE_OBJECT(PROP_SHAMAL_CRASH, <<2808.3870, 4795.4839, 45.8104>>)
			//Creating object at different location for NG
			ObjTitan = CREATE_OBJECT_NO_OFFSET(PROP_SHAMAL_CRASH, <<2807.691895, 4796.448730, 47.668404>> )
			SET_ENTITY_COLLISION(ObjTitan, FALSE)
			SET_ENTITY_ROTATION(ObjTitan, <<-0.000000, -0.000000, -11.340065>> )
			SET_ENTITY_QUATERNION(ObjTitan, -0.0162, -0.0524, -0.1071, 0.9927)
			SET_ENTITY_INVINCIBLE(ObjTitan, TRUE)
			FREEZE_ENTITY_POSITION(ObjTitan, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SHAMAL_CRASH)
		
			IF NOT DOES_ENTITY_EXIST(pedPilot)
				pedPilot = CREATE_PED(PEDTYPE_MISSION, S_M_M_PILOT_01, <<2809.4, 4801.3, 47.7>>, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_LEG, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_FEET, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_JBIB, 0, 0)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPilot, TRUE)
				SET_ENTITY_COORDS(pedPilot, <<2808.3870, 4795.4839, 45.8104>>)
				ATTACH_ENTITY_TO_ENTITY(pedPilot, ObjTitan, 0, << -0.54, 5.7, 0.03 >>, <<0,0,0>>)
				TASK_PLAY_ANIM(pedPilot, "missmartin1@leadinoutmartin_1_dead_pilot", "dead_pilot_leadin_loop", INSTANT_BLEND_IN, 
					NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
				SET_PED_KEEP_TASK(pedPilot, TRUE)
				SET_ENTITY_COLLISION(pedPilot, FALSE)
				APPLY_PED_BLOOD_SPECIFIC(pedPilot, ENUM_TO_INT(PDZ_HEAD), 0.515, 0.814, 300.781, 0.975, 3, 0.00, "stab") 
				APPLY_PED_BLOOD_SPECIFIC(pedPilot, ENUM_TO_INT(PDZ_HEAD), 0.633, 0.620, 300.617, 0.262, 1, 0.00, "ShotgunLargeMonolithic") 
				SET_PED_CAN_BE_TARGETTED(pedPilot, FALSE)
				SET_FACIAL_IDLE_ANIM_OVERRIDE(pedPilot, "dead_1")
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
			ENDIF		
		
//			vehTitan = CREATE_VEHICLE(SHAMAL, <<2808.3870, 4795.4839, 45.8104>>, 348.6388)
//			SET_VEHICLE_DOOR_OPEN(vehTitan, SC_DOOR_FRONT_LEFT, FALSE, TRUE)
//			SET_ENTITY_ROTATION(vehTitan, <<-1.2048, -6.1718, -12.3839>>)
//			SET_ENTITY_QUATERNION(vehTitan, -0.0162, -0.0524, -0.1071, 0.9927)
//			SET_ENTITY_INVINCIBLE(vehTitan, TRUE)
//			CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)
//			SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
//			SET_VEHICLE_LIVERY(vehTitan, 1)
//			SET_VEHICLE_ENGINE_ON(vehTitan, FALSE, TRUE)
			
//			pedPilot = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, S_M_M_PILOT_01)
//			SET_ENTITY_HEALTH(pedPilot, 0)
//			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
			
			//Create the 2 passengers 
			IF NOT DOES_ENTITY_EXIST(endGuy[0])
				//Pre NG location
				//endGuy[0] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGANG_01, <<2806.1355, 4801.3262, 46.0759>>, 78.4343)
				
				endGuy[0] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGANG_01, <<2807.3120, 4802.3140, 46.3392>>, 78.4343)
				
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_TORSO, 1, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_LEG, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_FEET, 0, 2)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_SPECIAL, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_JBIB, 0, 0)				
				
				STOP_PED_WEAPON_FIRING_WHEN_DROPPED(endGuy[0])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(endGuy[0], TRUE)
				GIVE_WEAPON_TO_PED(endGuy[0], WEAPONTYPE_PISTOL, 100)
				APPLY_PED_BLOOD_BY_ZONE(endGuy[0], ENUM_TO_INT(PDZ_HEAD), 0.360, 0.710, "ShotgunSmall")
				SET_PED_SPHERE_DEFENSIVE_AREA(endGuy[0], GET_ENTITY_COORDS(endGuy[0]), 10)
				SET_PED_COMBAT_ATTRIBUTES(endGuy[0], CA_CAN_CHARGE, TRUE)
				
				//PRE NG ANIM LOCATION
				//TASK_PLAY_ANIM_ADVANCED(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b", <<2808.3870, 4795.4839, 47.4224>>, <<0,0,-0.21>>, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1,AF_EXTRACT_INITIAL_OFFSET)
				
				TASK_PLAY_ANIM_ADVANCED(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b", <<2804.3113, 4798.0303, 46.7273>>, <<0,0,-0.21>>, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET)
				
				iAnimTaskTime = GET_GAME_TIMER()
//				LeavingJetSyncScene = CREATE_SYNCHRONIZED_SCENE(<<2808.3870, 4795.4839, 47.4224>>, <<0,0,-0.21>>)
//				TASK_SYNCHRONIZED_SCENE(endGuy[0], LeavingJetSyncScene, "missmartin1@exit_plane", "exit_plane_guy_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			ENDIF
//			IF NOT DOES_ENTITY_EXIST(endGuy[0])
//				IF IS_VEHICLE_DRIVEABLE(vehTitan)
//					endGuy[0] = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, G_M_Y_MEXGANG_01, VS_FRONT_RIGHT)
//					GIVE_WEAPON_TO_PED(endGuy[0], WEAPONTYPE_PISTOL, 100, TRUE)
//					OPEN_SEQUENCE_TASK(seqSequence)
//						TASK_LEAVE_VEHICLE(NULL, vehTitan, ECF_DONT_CLOSE_DOOR)
////												TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
//					CLOSE_SEQUENCE_TASK(seqSequence)
//					TASK_PERFORM_SEQUENCE(endGuy[0], seqSequence)
//					CLEAR_SEQUENCE_TASK(seqSequence)
//					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(endGuy[0], vehTitan, <<0,0,0>>, 10)
//					SET_PED_COMBAT_ATTRIBUTES(endGuy[0], CA_CAN_CHARGE, TRUE)
//				ENDIF
//			ENDIF
			IF NOT DOES_ENTITY_EXIST(endGuy[1])
				endGuy[1] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGOON_02, <<2805.3694, 4799.1553, 46.0390>>, 82.1640)
				
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_HEAD, 0, 1)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_TORSO, 0, 2)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_LEG, 0, 2)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_FEET, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_SPECIAL, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_JBIB, 0, 0)				
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(endGuy[1], TRUE)
//				LeavingJetSyncScene = CREATE_SYNCHRONIZED_SCENE(<<2808.3870, 4795.4839, 47.4224>>, <<0,0,-0.21>>)
//				TASK_SYNCHRONIZED_SCENE(endGuy[1], LeavingJetSyncScene, "missmartin1@exit_plane", "exit_plane_guy_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)	

				//PRE NG
				//TASK_PLAY_ANIM_ADVANCED(endGuy[1], "missmartin1@exit_plane", "exit_plane_guy_a", <<2808.3870, 4795.4839, 47.4224>>, <<0, 0, -0.21>>, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME|AF_EXTRACT_INITIAL_OFFSET)
			
				TASK_PLAY_ANIM_ADVANCED(endGuy[1], "missmartin1@exit_plane", "exit_plane_guy_a", <<2806.2720, 4799.6250, 46.5135>>, <<0, 0, 40.0>>, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME|AF_EXTRACT_INITIAL_OFFSET|AF_NOT_INTERRUPTABLE)
			
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MEXGANG_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MEXGOON_02)
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_KILL_PASSENGERS")
				START_AUDIO_SCENE("MARTIN_1_KILL_PASSENGERS")
			ENDIF
		
			TRIGGER_MUSIC_EVENT("SOL1_CRASHED_PLANE_RT")
		
		BREAK
		CASE STAGE_CUTSCENE_PLANE_CRASH
			
			vehBike = CREATE_VEHICLE(SANCHEZ,<< 2350.7231, 3925.1118, 34.6029 >>, 298.6736)
			SET_VEHICLE_ENGINE_ON(vehBike, TRUE, TRUE)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR,<< -450.3808, 1065.3434, 326.5121 >>)
					WAIT(0)
					PRINTSTRING("SOLOMON1_WAITING_61") PRINTNL()
				ENDWHILE
//				IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 0, 4)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 18, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAND, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_FEET, 16, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL2, 0, 0)
//					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_DECL, 0, 0)	
//				ENDIF
				
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE)
			ENDIF			
			
//			REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)			
			
			ObjTitan = CREATE_OBJECT_NO_OFFSET(PROP_SHAMAL_CRASH, <<2807.691895, 4796.448730, 47.668404>>)
			SET_ENTITY_ROTATION(ObjTitan, <<-0.000000, -0.000000, -11.340065>>)
			SET_ENTITY_QUATERNION(ObjTitan, -0.0162, -0.0524, -0.1071, 0.9927)
			SET_ENTITY_INVINCIBLE(ObjTitan, TRUE)
			FREEZE_ENTITY_POSITION(ObjTitan, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SHAMAL_CRASH)
			
//			pedPilot = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, S_M_M_PILOT_01)
//			SET_ENTITY_HEALTH(pedPilot, 0)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
			
		BREAK
		CASE STAGE_DESTROY_SNIPER		
			
			REMOVE_PTFX_ASSET()		
			
			//Set up Michael
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<-365.1108, 1146.2261, 323.6802>>, 212.6341)//<<-370.9615, 1151.7002, 324.3864>>, 225.63827)
					PRINTSTRING("Waiting for Michael to be created") PRINTNL()
					WAIT(0)
				ENDWHILE
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE)
			ENDIF
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-365.1108, 1146.2261, 323.6802>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 212.6341)
			ENDIF
			
			//Create players car.
			IF IS_REPLAY_IN_PROGRESS()
				IF NOT DOES_ENTITY_EXIST(MichaelsCar)
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						//These 2 commands should clear any previous cars created from before failing mission for bug 1706193
						DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
						DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<-386.6838, 1202.5878, 324.6417>>, 20, TRUE)
						IF NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
							MichaelsCar = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-386.6838, 1202.5878, 324.6417>>, 100.5474)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						ENDIF
					ENDIF	
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(MichaelsCar, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
				ENDIF
			ENDIF				
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			
			REQUEST_MODEL(PROP_SHAMAL_CRASH)
			WHILE NOT HAS_MODEL_LOADED(PROP_SHAMAL_CRASH)
				WAIT(0)
			ENDWHILE
			ObjTitan = CREATE_OBJECT_NO_OFFSET(PROP_SHAMAL_CRASH, <<2807.691895, 4796.448730, 47.668404>> )
			SET_ENTITY_COLLISION(ObjTitan, FALSE)
			SET_ENTITY_ROTATION(ObjTitan, <<-0.000000, -0.000000, -11.340065>>)
			SET_ENTITY_QUATERNION(ObjTitan, -0.0162, -0.0524, -0.1071, 0.9927)
			SET_ENTITY_INVINCIBLE(ObjTitan, TRUE)
			FREEZE_ENTITY_POSITION(ObjTitan, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SHAMAL_CRASH)
								
			IF GET_BUILDING_DATA(PlaneCrashTrench, BUILDINGNAME_IPL_PLANE_CRASH_TRENCH) 
				SET_BUILDING_STATE(BUILDINGNAME_IPL_PLANE_CRASH_TRENCH, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
		CASE STAGE_END_CUTSCENE			
				
			//Set up a scenario blocking area for the barn area to stop peds spawning in 
			CLEAR_AREA(<<1216.6, 1852.6, 78.8>>, 60, TRUE)
			BarnBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<1288, 1962.8, 128.4>>, <<1139.2, 1800.8, 59.5>>)
				
			IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
				
			//Set up Michael#
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<1204.7, 1859, 78.2>>, 214.5280)
					PRINTSTRING("Waiting for Michael to be created") PRINTNL()
					WAIT(0)
				ENDWHILE
			
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)	
			ENDIF	
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1204.7, 1859, 78.2>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 214.5280)
			ENDIF
			
		BREAK
		CASE PASSED_MISSION
			MissionPassed()
		BREAK
	ENDSWITCH	
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

ENDPROC


//═════════════════════════════════╡ NON-Stage FUNCTIONS AND PROCEDURES ╞═══════════════════════════════════

//═════════╡ GAMEPLAY ╞═════════

//PROC DUMP_CARGO()
//	VECTOR vSpawn
//	INT iRandCrate
//	INT iRandForce
//	OBJECT_INDEX obj
//	
//	IF fRecordingTime < Asset.skipPlaneDumpStop
//		IF bSetPiece[2] = TRUE
//			IF TIMERB() > iRand
//				
//				vSpawn = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan,<<-2,0,0>>)
//				iRandCrate = GET_RANDOM_INT_IN_RANGE(0,6)
//				iRandForce  = GET_RANDOM_INT_IN_RANGE(-3,3)
//				iRand = GET_RANDOM_INT_IN_RANGE(1000,2500)
//				//PRINTINT(iRand)PRINTNL()
//				
//				IF iRandCrate = 0
//					obj = CREATE_OBJECT(Asset.mnCrate1,vSpawn)
//				ENDIF
//				IF iRandCrate = 1
//					obj = CREATE_OBJECT(Asset.mnCrate2,vSpawn)
//				ENDIF
//				IF iRandCrate = 2
//					obj = CREATE_OBJECT(Asset.mnCrate3,vSpawn)
//				ENDIF
//				IF iRandCrate = 3
//					obj = CREATE_OBJECT(Asset.mnCrate4,vSpawn)
//				ENDIF
//				IF iRandCrate = 4
//					obj = CREATE_OBJECT(Asset.mnCrate5,vSpawn)
//				ENDIF
//				IF iRandCrate = 5
//					obj = CREATE_OBJECT(Asset.mnCrate6,vSpawn)
//				ENDIF
//
//				IF DOES_ENTITY_HAVE_PHYSICS(obj)
//					APPLY_FORCE_TO_ENTITY(obj,APPLY_TYPE_FORCE,<<1,iRandForce,2>>,VECTOR_ZERO,0,TRUE,TRUE,TRUE)
//				ENDIF
//				
//				SETTIMERB(0)
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

PROC HIGHLIGHT_PED_ON_REMOTE_SNIPER_HUD_NO_THERMAL(REMOTE_SNIPER_DATA &SniperData, PED_INDEX &ped)
	IF NOT IS_PED_INJURED(ped)
		IF WAS_PED_SKELETON_UPDATED(ped)
			VECTOR vTopPos, vBottomPos, vLeftPos, vRightPos, vBackPos, vFrontPos
			VECTOR vCamPos = GET_CAM_COORD(SniperData.cam_rifle)
			VECTOR vCamRot = GET_ENTITY_ROTATION(SniperData.obj_sniper)
		
			//These modifiers are fudged, and are used to make sure the tags are in the right position at any fov level
			FLOAT fOffsetModifierX = SniperData.f_current_fov_plus_recoil * 1.345
			FLOAT fOffsetModifierY = SniperData.f_current_fov_plus_recoil
		
			//Get a set of coordinates that surround the the current ped
			vLeftPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped, <<0.45, 20.0/*0.0*/, 0.0>>)
			vRightPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped, <<-0.45, 20.0/*0.0*/, 0.0>>)
			vBackPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped, <<0.0, 19.7/*-0.3*/, 0.0>>)
			vFrontPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped, <<0.0, 20.3/*0.3*/, 0.0>>)
			vTopPos = GET_PED_BONE_COORDS(ped, BONETAG_HEAD, <<0.0, 20.0/*0.0*/, 0.0>>) + <<0.0, 0.0, 0.2>>
			
			VECTOR vLeftFootPos = GET_PED_BONE_COORDS(ped, BONETAG_L_FOOT, <<0.0, 0.0, 0.0>>) - <<0.0, 0.0, 0.2>>
			VECTOR vRightFootPos = GET_PED_BONE_COORDS(ped, BONETAG_R_FOOT, <<0.0, 0.0, 0.0>>) - <<0.0, 0.0, 0.2>>
			IF vLeftFootPos.z < vRightFootPos.z
				vBottomPos = vLeftFootPos
			ELSE
				vBottomPos = vRightFootPos
			ENDIF			
			
			//Work out the difference between these coords and the current camera position
			VECTOR vTopPosDiff = vTopPos - vCamPos
			VECTOR vBottomPosDiff = vBottomPos - vCamPos
			VECTOR vLeftPosDiff = vLeftPos - vCamPos
			VECTOR vRightPosDiff = vRightPos - vCamPos
			VECTOR vBackPosDiff = vBackPos - vCamPos
			VECTOR vFrontPosDiff = vFrontPos - vCamPos
			
			//Use the differences to work out the angles the camera 
			//would have to face for these coordinates to be dead centre
			FLOAT fLeftHeading = GET_HEADING_FROM_VECTOR_2D(vLeftPosDiff.x, vLeftPosDiff.y)
			FLOAT fRightHeading = GET_HEADING_FROM_VECTOR_2D(vRightPosDiff.x, vRightPosDiff.y)
			FLOAT fBackHeading = GET_HEADING_FROM_VECTOR_2D(vBackPosDiff.x, vBackPosDiff .y)
			FLOAT fFrontHeading = GET_HEADING_FROM_VECTOR_2D(vFrontPosDiff.x, vFrontPosDiff.y)
			FLOAT fTopPitch = ATAN2(vTopPosDiff.z, DISTANCE_BETWEEN_COORDS(vTopPos, vCamPos, TRUE, TRUE))
			FLOAT fBottomPitch=ATAN2(vBottomPosDiff.z,DISTANCE_BETWEEN_COORDS(vBottomPos, vCamPos,TRUE,TRUE))
			
			//For the box width: need to consider what the heading of the ped is. 
			//If they're facing side on to the camera the front and back offsets should be used
			IF ABSF(fBackHeading - fFrontHeading) > ABSF(fLeftHeading - fRightHeading)
				fLeftHeading = fBackHeading
				fRightHeading = fFrontHeading
			ENDIF
			
			//Convert the differences between the required angles and the current camera angle into screen offsets
			FLOAT fXLeftOffset = (fLeftHeading - vCamRot.z) / fOffsetModifierX 
			FLOAT fXRightOffset = (fRightHeading - vCamRot.z) / fOffsetModifierX 
			FLOAT fTopYOffset = (fTopPitch - vCamRot.x) / fOffsetModifierY
			FLOAT fBottomYOffset = (fBottomPitch - vCamRot.x) / fOffsetModifierY
			
			//These just improve readability of the rectangle calculations
			FLOAT fLeftX = 0.5 - fXLeftOffset
			FLOAT fRightX = 0.5 - fXRightOffset
			FLOAT fTopY = 0.5 - fTopYOffset
			FLOAT fBottomY = 0.5 - fBottomYOffset

			SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)

			//Top and bottom lines
			DRAW_RECT((fLeftX + fRightX) / 2.0, fTopY, ABSF(fLeftX - fRightX), 0.002, 255, 0, 0, 128)
			DRAW_RECT((fLeftX + fRightX) / 2.0, fBottomY, ABSF(fLeftX - fRightX), 0.002, 255, 0, 0, 128)
			
			//Side lines
			DRAW_RECT(fLeftX, (fTopY + fBottomY) / 2.0, 0.002, ABSF(fTopY - fBottomY) + 0.002, 255, 0, 0, 128)
			DRAW_RECT(fRightX, (fTopY + fBottomY) / 2.0, 0.002, ABSF(fTopY - fBottomY) + 0.002, 255, 0, 0, 128)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL drawPlaneHighlight(REMOTE_SNIPER_DATA &SniperData)
	VECTOR vTopPos, vBottomPos, vLeftPos, vRightPos, vBackPos, vFrontPos
	VECTOR vCamPos = GET_CAM_COORD(SniperData.cam_rifle)
	VECTOR vCamRot = GET_ENTITY_ROTATION(SniperData.obj_sniper)

	//These modifiers are fudged, and are used to make sure the tags are in the right position at any fov level
	FLOAT fOffsetModifierX = SniperData.f_current_fov_plus_recoil * 1.345
	FLOAT fOffsetModifierY = SniperData.f_current_fov_plus_recoil

	//Left << -7.2953, 0.0, 0.0 >>
	//Right << 7.3825, 0.0, 0.0 >>
	//Back << 0.0, -10.7886, 0.0 >>
	//Forward << 0.0, 9.0564, 0.0 >>
	//Top << 0.0, 0.0, 3.4162 >>
	//Bottom << 0.0, 0.0, -1.7393 >>

	//Get a set of coordinates that surround the the current ped
	vLeftPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, << -7.2953, 0.0, 0.0 >>)
	vRightPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, << 7.3825, 0.0, 0.0 >>)
	vBackPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, << 0.0, -10.7886, 0.0 >>)
	vFrontPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, << 0.0, 9.0564, 0.0 >>)
	vTopPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, << 0.0, 0.0, 3.4162 >>)
	vBottomPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, << 0.0, 0.0, -1.7393 >>)
	
	//Work out the difference between these coords and the current camera position
	VECTOR vTopPosDiff = vTopPos - vCamPos
	VECTOR vBottomPosDiff = vBottomPos - vCamPos
	VECTOR vLeftPosDiff = vLeftPos - vCamPos
	VECTOR vRightPosDiff = vRightPos - vCamPos
	VECTOR vBackPosDiff = vBackPos - vCamPos
	VECTOR vFrontPosDiff = vFrontPos - vCamPos
	
	//Use the differences to work out the angles the camera 
	//would have to face for these coordinates to be dead centre
	FLOAT fLeftHeading = GET_HEADING_FROM_VECTOR_2D(vLeftPosDiff.x, vLeftPosDiff.y)
	FLOAT fRightHeading = GET_HEADING_FROM_VECTOR_2D(vRightPosDiff.x, vRightPosDiff.y)
	FLOAT fBackHeading = GET_HEADING_FROM_VECTOR_2D(vBackPosDiff.x, vBackPosDiff .y)
	FLOAT fFrontHeading = GET_HEADING_FROM_VECTOR_2D(vFrontPosDiff.x, vFrontPosDiff.y)
	FLOAT fTopPitch = ATAN2(vTopPosDiff.z, DISTANCE_BETWEEN_COORDS(vTopPos, vCamPos, TRUE, TRUE))
	FLOAT fBottomPitch = ATAN2(vBottomPosDiff.z, DISTANCE_BETWEEN_COORDS(vBottomPos, vCamPos, TRUE, TRUE))
	
	//For the box width: need to consider what the heading of the ped is. 
	//If they're facing side on to the camera the front and back offsets should be used
	IF ABSF(fBackHeading - fFrontHeading) > ABSF(fLeftHeading - fRightHeading)
		fLeftHeading = fBackHeading
		fRightHeading = fFrontHeading
	ENDIF
	
	FLOAT fCamRotZ = vCamRot.z
	IF fCamRotZ < 0
		fCamRotZ += 360
	ENDIF
	
	//Convert the differences between the required angles and the current camera angle into screen offsets
	FLOAT fXLeftOffset = (fLeftHeading - fCamRotZ) / fOffsetModifierX
	FLOAT fXRightOffset = (fRightHeading - fCamRotZ) / fOffsetModifierX
	FLOAT fTopYOffset = (fTopPitch - vCamRot.x) / fOffsetModifierY
	FLOAT fBottomYOffset = (fBottomPitch - vCamRot.x) / fOffsetModifierY
	
	//These just improve readability of the rectangle calculations
	FLOAT fLeftX = 0.5 - fXLeftOffset
	FLOAT fRightX = 0.5 - fXRightOffset
	FLOAT fTopY = 0.5 - fTopYOffset
	FLOAT fBottomY = 0.5 - fBottomYOffset
		
	SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)
	
	FLOAT fCentreX = (fLeftX + fRightX) / 2.0
	FLOAT fCentreY = (fTopY + fBottomY) / 2.0

	FLOAT fWidth = ABSF(fLeftX - fRightX)
	FLOAT fHeight = ABSF(fTopY - fBottomY)
	
	FLOAT fArea = fWidth * fHeight
		
	FLOAT fSmallestArea = 0.001

	IF fArea < fSmallestArea
		FLOAT fSquareSecArea = (fSmallestArea/fArea) * (fHeight*fHeight)
		FLOAT fNewHeight = SQRT(fSquareSecArea)
		FLOAT fNewWidth = fSmallestArea/fNewHeight
				
		fWidth = fNewWidth
		fHeight = fNewHeight
		
		fRightX = fCentreX + (fNewWidth/2)
		fLeftX = fCentreX - (fNewWidth/2)
		fTopY = fCentreY + (fNewHeight/2)
		fBottomY = fCentreY - (fNewHeight/2)
	ENDIF
	
	IF fArea > 0.01
		RETURN FALSE
	ELSE	
		DRAW_RECT(fCentreX, fTopY, fWidth, 0.002, 255, 0, 0, 128,TRUE)
		DRAW_RECT(fRightX, fCentreY, 0.002, fHeight, 255, 0, 0, 128,TRUE)
		DRAW_RECT(fCentreX, fBottomY, fWidth, 0.002, 255, 0, 0, 128,TRUE)
		DRAW_RECT(fLeftX, fCentreY, 0.002, fHeight, 255, 0, 0, 128,TRUE)
		RETURN TRUE
	ENDIF
ENDFUNC

PROC drawAimSquare(REMOTE_SNIPER_DATA &SniperData, VECTOR vTargetOffset, FLOAT &fOutputX, FLOAT &fOutputY)
	VECTOR vTopPos, vBottomPos, vLeftPos, vRightPos, vBackPos, vFrontPos
	VECTOR vCamPos = GET_CAM_COORD(SniperData.cam_rifle)
	VECTOR vCamRot = GET_ENTITY_ROTATION(SniperData.obj_sniper)
	
	FLOAT fCrossSize = 1.0
	FLOAT fDistanceInFront = 30.0

	//These modifiers are fudged, and are used to make sure the tags are in the right position at any fov level
	FLOAT fOffsetModifierX = SniperData.f_current_fov_plus_recoil * 1.345
	FLOAT fOffsetModifierY = SniperData.f_current_fov_plus_recoil

	//Get a set of coordinates that surround the the current ped
	vLeftPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, <<fCrossSize, fDistanceInFront, 0.0>> + vTargetOffset)
	vRightPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, <<-fCrossSize, fDistanceInFront, 0.0>> + vTargetOffset)
	vBackPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, <<0.0, fDistanceInFront - fCrossSize, 0.0>> + vTargetOffset)
	vFrontPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, <<0.0, fDistanceInFront + fCrossSize, 0.0>> + vTargetOffset)
	vTopPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, <<0, fDistanceInFront, fCrossSize>> + vTargetOffset)
	vBottomPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTitan, <<0, fDistanceInFront, -fCrossSize>> + vTargetOffset)
	
	//Work out the difference between these coords and the current camera position
	VECTOR vTopPosDiff = vTopPos - vCamPos
	VECTOR vBottomPosDiff = vBottomPos - vCamPos
	VECTOR vLeftPosDiff = vLeftPos - vCamPos
	VECTOR vRightPosDiff = vRightPos - vCamPos
	VECTOR vBackPosDiff = vBackPos - vCamPos
	VECTOR vFrontPosDiff = vFrontPos - vCamPos
	
	//Use the differences to work out the angles the camera 
	//would have to face for these coordinates to be dead centre
	FLOAT fLeftHeading = GET_HEADING_FROM_VECTOR_2D(vLeftPosDiff.x, vLeftPosDiff.y)
	FLOAT fRightHeading = GET_HEADING_FROM_VECTOR_2D(vRightPosDiff.x, vRightPosDiff.y)
	FLOAT fBackHeading = GET_HEADING_FROM_VECTOR_2D(vBackPosDiff.x, vBackPosDiff .y)
	FLOAT fFrontHeading = GET_HEADING_FROM_VECTOR_2D(vFrontPosDiff.x, vFrontPosDiff.y)
	FLOAT fTopPitch = ATAN2(vTopPosDiff.z, DISTANCE_BETWEEN_COORDS(vTopPos, vCamPos, TRUE, TRUE))
	FLOAT fBottomPitch = ATAN2(vBottomPosDiff.z, DISTANCE_BETWEEN_COORDS(vBottomPos, vCamPos, TRUE, TRUE))
	
	//For the box width: need to consider what the heading of the ped is. 
	//If they're facing side on to the camera the front and back offsets should be used
	IF ABSF(fBackHeading - fFrontHeading) > ABSF(fLeftHeading - fRightHeading)
		fLeftHeading = fBackHeading
		fRightHeading = fFrontHeading
	ENDIF
	
	FLOAT fCamRotZ = vCamRot.z
	IF fCamRotZ < 0
		fCamRotZ += 360
	ENDIF
	
	//Convert the differences between the required angles and the current camera angle into screen offsets
	FLOAT fXLeftOffset = (fLeftHeading - fCamRotZ) / fOffsetModifierX 
	FLOAT fXRightOffset = (fRightHeading - fCamRotZ) / fOffsetModifierX 
	FLOAT fTopYOffset = (fTopPitch - vCamRot.x) / fOffsetModifierY
	FLOAT fBottomYOffset = (fBottomPitch - vCamRot.x) / fOffsetModifierY
	
	//These just improve readability of the rectangle calculations
	FLOAT fLeftX = 0.5 - fXLeftOffset
	FLOAT fRightX = 0.5 - fXRightOffset
	FLOAT fTopY = 0.5 - fTopYOffset
	FLOAT fBottomY = 0.5 - fBottomYOffset

	SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	FLOAT fWidth = ABSF(fLeftX - fRightX)
	FLOAT fHeight = ABSF(fTopY - fBottomY)
	
	fOutputX = (fLeftX + fRightX) / 2.0
	fOutputY = (fTopY + fBottomY) / 2.0

	//Lines
	//DRAW_RECT(fOutputX, fOutputY, ABSF(fLeftX - fRightX), 0.002, 255, 0, 0, 128)
	//DRAW_RECT(fOutputX, fOutputY, 0.002, ABSF(fTopY - fBottomY), 255, 0, 0, 128)
	DRAW_RECT(fOutputX, fTopY, fWidth, 0.002, 255, 0, 0, 128,TRUE)
	DRAW_RECT(fRightX, fOutputY, 0.002, fHeight, 255, 0, 0, 128,TRUE)
	DRAW_RECT(fOutputX, fBottomY, fWidth, 0.002, 255, 0, 0, 128,TRUE)
	DRAW_RECT(fLeftX, fOutputY, 0.002, fHeight, 255, 0, 0, 128,TRUE)

ENDPROC

PROC RUBBER_BAND_BETWEEN_TWO_ENTITIES(	ENTITY_INDEX entChaser, ENTITY_INDEX entTarget, FLOAT fOptimalDis,
										FLOAT fMinSpeed = 0.7, FLOAT fMaxSpeed = 1.3)
	IF NOT IS_ENTITY_DEAD(entChaser) AND NOT IS_ENTITY_DEAD(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entTarget))
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entTarget))
			fMainPlaybackSpeed=  (	fOptimalDis / 
									GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(entChaser),
																GET_ENTITY_COORDS(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entTarget))))
			SET_PLAYBACK_SPEED(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entTarget), fMainPlaybackSpeed)
			IF fMainPlaybackSpeed> fMaxSpeed
				fMainPlaybackSpeed= fMaxSpeed
			ENDIF
			IF fMainPlaybackSpeed< fMinSpeed
				fMainPlaybackSpeed= fMinSpeed
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_VEHICLE_ROTATION_LOCAL(VEHICLE_INDEX veh)
	VECTOR v_rot = <<0.0, 0.0, 0.0>>

	IF IS_VEHICLE_DRIVEABLE(veh)
		v_rot = <<GET_ENTITY_PITCH(veh), GET_ENTITY_ROLL(veh), GET_ENTITY_HEADING(veh)>>
	ENDIF
	
	RETURN v_rot
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Converts the current position and rotation of the debug cam into an attach and point offset relative to a given vehicle. 
///    This is useful for quickly creating cameras that will be attached to/pointed at vehicles.
/// PARAMS:
///    veh - The vehicle to attach to and point at. If NULL, the nearest vehicle will be used
///    b_include_source_info - If TRUE, the proc will also output info about the camera and vehicle's actual position and rotation
PROC OUTPUT_DEBUG_CAM_PARAMS_RELATIVE_TO_VEHICLE(VEHICLE_INDEX veh = NULL, BOOL b_include_source_info = FALSE)
	CAMERA_INDEX cam = GET_DEBUG_CAM()
	
	IF DOES_CAM_EXIST(cam)
		IF NOT DOES_ENTITY_EXIST(veh)
			INT i_search_flags = VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES 
			i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER
			i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES
			i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES
			i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED
			i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS
			i_search_flags |= VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK
			veh = GET_CLOSEST_VEHICLE(GET_CAM_COORD(cam), 500.0, DUMMY_MODEL_FOR_SCRIPT, i_search_flags)
		ENDIF
	
		IF IS_VEHICLE_DRIVEABLE(veh)
			VECTOR vCamPos = GET_CAM_COORD(cam)
			VECTOR vCamRot = GET_CAM_ROT(cam)				
			VECTOR v_veh_rot = GET_VEHICLE_ROTATION_LOCAL(veh)
			IF v_veh_rot.z > 180.0
				v_veh_rot.z = -180.0 -(180.0 - v_veh_rot.z)
			ENDIF
			VECTOR v_cam_front_pos = GET_CAM_COORD(cam) + (CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_CAM_ROT(cam)) * 3.0)
		
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("ATTACH_CAM_TO_ENTITY(cam, veh, ")
			SAVE_VECTOR_TO_DEBUG_FILE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh, vCamPos))
			SAVE_STRING_TO_DEBUG_FILE(")")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("POINT_CAM_AT_ENTITY(cam, veh, ")
			SAVE_VECTOR_TO_DEBUG_FILE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh, v_cam_front_pos))
			SAVE_STRING_TO_DEBUG_FILE(")")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("SET_CAM_FOV(cam, ")
			SAVE_FLOAT_TO_DEBUG_FILE(GET_CAM_FOV(cam))
			SAVE_STRING_TO_DEBUG_FILE(")")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF b_include_source_info
				VECTOR v_veh_pos = GET_ENTITY_COORDS(veh)
			
				SAVE_STRING_TO_DEBUG_FILE("Camera world coords: ")
				SAVE_VECTOR_TO_DEBUG_FILE(vCamPos)
				SAVE_STRING_TO_DEBUG_FILE("  Camera rotation: ")
				SAVE_VECTOR_TO_DEBUG_FILE(vCamRot)
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("Vehicle world coords: ")
				SAVE_VECTOR_TO_DEBUG_FILE(v_veh_pos)
				SAVE_STRING_TO_DEBUG_FILE("  Vehicle rotation: ")
				SAVE_VECTOR_TO_DEBUG_FILE(v_veh_rot)

				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDIF	
	ENDIF
ENDPROC
#ENDIF


//═════════════════════════════════╡ Stage FUNCTIONS AND PROCEDURES ╞═══════════════════════════════════


//Sets up the end of the mission for skipping to end
PROC SET_UP_MISSION_PASSED()
	
	IF iStageSection = 0
	
		CLEAR_AREA(<<2830.1670, 4743.4956, 46.4952>>, 20, TRUE)
		
//		REQUEST_MODEL(PROP_DEST_PLANE)	
//		REQUEST_MODEL(CS_SEXTONDANIELS)
		
//		WHILE NOT HAS_MODEL_LOADED(PROP_DEST_PLANE)
//		WHILE NOT HAS_MODEL_LOADED(CS_SEXTONDANIELS)
//			WAIT(0)
//		ENDWHILE
		
		//Create plane
//		IF NOT DOES_ENTITY_EXIST(ObjCrashedPlane)
//			ObjCrashedPlane = CREATE_OBJECT(PROP_DEST_PLANE, <<2835.90, 4739.27, 43.47>>)
//			SET_ENTITY_ROTATION(ObjCrashedPlane, <<0,0,166.34>>)
//		ENDIF			
		
		//Create Sexton
//		IF NOT DOES_ENTITY_EXIST(pedTarget)
//			pedTarget = CREATE_PED(PEDTYPE_MISSION, CS_SEXTONDANIELS, << 2836.3191, 4745.7349, 46.8542 >>, 155.3079)
//			SET_ENTITY_HEALTH(pedTarget, 0)
//		ENDIF
		
		//Move player
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2830.1670, 4743.4956, 46.4952>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 98.2722)
					
		iStageSection ++
		
	ENDIF
	
	IF iStageSection = 1
		
		LOAD_SCENE(<<2830.1670, 4743.4956, 46.4952>>)
		
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		MissionPassed()
	
	ENDIF
	
ENDPROC

//PURPOSE:		Initialise Mission
FUNC BOOL stageInitMission()
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
		
		//Load text slot for mission
		REQUEST_ADDITIONAL_TEXT("BFINJEC", MISSION_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			PRINTSTRING("Waiting for additional text to load ") PRINTNL()
			WAIT(0)
		ENDWHILE
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1367.5, 1122.5, 111.4>>, <<1377.8, 1138.7, 117.5>>, FALSE, FALSE)
		
		SetupArrays()
		
		TOGGLE_STEALTH_RADAR(FALSE)
		
		iStageSection = 0
		bStageSetup = TRUE
		
//═════════╡ UPDATE ╞═════════		
	ELSE
	
		SWITCH iStageSection
			CASE 0
				iStageSection++
			BREAK
		ENDSWITCH
	
//═════════╡ MOVE STAGE ON ╞═════════
		IF iStageSection >= 1
				
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			
			bStageSetup = FALSE	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Handles Trevor after opening cutscene.
PROC HANDLE_CUTSCENE_TREVOR()
	
	//Make Trev head track Michael
	IF iStageSection > 0
		IF DOES_ENTITY_EXIST(CSTrev)
			IF NOT IS_PED_INJURED(CSTrev)
				IF GET_DISTANCE_BETWEEN_ENTITIES(CSTrev, PLAYER_PED_ID()) < 20
					IF TrevLookTaskGiven = FALSE
						TASK_LOOK_AT_ENTITY(CSTrev, PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
						TrevLookTaskGiven = TRUE
					ENDIF
				ELSE
					IF TrevLookTaskGiven = TRUE
						TASK_CLEAR_LOOK_AT(CSTrev)
						TrevLookTaskGiven = FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTSTRING("CSTrev doesn't exist") PRINTNL()
		ENDIF
	ENDIF
	
	//Delete cutscene trev and his car once off screen and far enough away
	IF DOES_ENTITY_EXIST(CSTrev)
		IF NOT IS_PED_INJURED(CSTrev)
			IF GET_DISTANCE_BETWEEN_ENTITIES(CSTrev, PLAYER_PED_ID()) > 100
			AND NOT IS_ENTITY_ON_SCREEN(CSTrev)
				DELETE_PED(CSTrev)
				iTrevDeletedTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ELSE	
		IF DOES_ENTITY_EXIST(TrevCutsceneCar)
			IF GET_DISTANCE_BETWEEN_ENTITIES(TrevCutsceneCar, PLAYER_PED_ID()) > 100
			OR NOT DOES_ENTITY_EXIST(CSTrev)
				IF NOT IS_ENTITY_ON_SCREEN(TrevCutsceneCar)
					DELETE_VEHICLE(TrevCutsceneCar)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Give Trev a task to get another car and flee if his car is destroyed
	IF DOES_ENTITY_EXIST(TrevCutsceneCar)
	AND DOES_ENTITY_EXIST(CSTrev)
		IF NOT IS_PED_INJURED(CSTrev)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), CSTrev)
					PLAY_PED_AMBIENT_SPEECH(CSTrev, "GENERIC_INSULT_MED", SPEECH_PARAMS_FORCE_NORMAL)
				ENDIF
			ENDIF
			IF NOT IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)	
				IF IS_PED_IN_VEHICLE(CSTrev, TrevCutsceneCar)
					TASK_LEAVE_VEHICLE(CSTrev, TrevCutsceneCar)
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(CSTrev, TrevCutsceneCar)
					IF NOT IS_PED_IN_ANY_VEHICLE(CSTrev)
						IF DOES_ENTITY_EXIST(GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(CSTrev), 200, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES))
							PRINTSTRING("There is a vehicle closer than 200metres") PRINTNL()
							IF GET_SCRIPT_TASK_STATUS(CSTrev, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(CSTrev, SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK									
								TASK_ENTER_VEHICLE(CSTrev, GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(CSTrev), 200, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES), -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
							ENDIF
						ELSE
							PRINTSTRING("There is not a vehicle closer than 200metres") PRINTNL()
							IF  GET_SCRIPT_TASK_STATUS(CSTrev, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(CSTrev, SCRIPT_TASK_SMART_FLEE_PED) <> WAITING_TO_START_TASK							
								TASK_SMART_FLEE_PED(CSTrev, PLAYER_PED_ID(), 150, -1)
							ENDIF						
						ENDIF
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(CSTrev)
						IF  GET_SCRIPT_TASK_STATUS(CSTrev, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(CSTrev, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
							TASK_VEHICLE_MISSION_PED_TARGET(CSTrev, GET_VEHICLE_PED_IS_IN(CSTrev), PLAYER_PED_ID(), MISSION_FLEE, 40, DRIVINGMODE_AVOIDCARS_RECKLESS, -1, 1, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//PURPOSE:		Performs the section where Michael drives to the observatory
FUNC BOOL stageDriveToObservatory()

	HANDLE_CUTSCENE_TREVOR()

	IF iStageSection > 4
	AND iStageSection < 7
        IF HAS_SCALEFORM_MOVIE_LOADED(sfSniperLoading)
              HIDE_HUD_AND_RADAR_THIS_FRAME()
              SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
              DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfSniperLoading,255,255,255,0)
        ENDIF 
	ENDIF
	
	//CLean up the start car at side of the house.
	IF bStageSetup
		IF DOES_ENTITY_EXIST(ParkedCarAtStart)
			IF GET_DISTANCE_BETWEEN_ENTITIES(ParkedCarAtStart, PLAYER_PED_ID()) > 100
				SET_VEHICLE_AS_NO_LONGER_NEEDED(ParkedCarAtStart)
			ELIF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), ParkedCarAtStart)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(ParkedCarAtStart)
			ENDIF
		ENDIF
	ENDIF

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
	
		PRINTSTRING("MISSION STAGE DRIVE TO OBSERVATORY") PRINTNL()	
		
		//Flags
		missionCanFail 				= FALSE
		hasSniperBeenSetToActive 	= FALSE
		playerHasTakenTaxi 			= FALSE
		RemoteSniperNeedsUpdated 	= FALSE
		interactiveMusicStarted 	= FALSE
		interactiveMusicStarted2 	= FALSE
		sniperCreated 				= FALSE
		doneSnipe8Text2				= FALSE
		DoneLoseCopsText 			= FALSE
		vanFrozen 					= FALSE
		timelapseRequired 			= FALSE
		driveTaskCancelled 			= FALSE
		doneGetOffChat 				= FALSE
		sniperVanBlipRouteSet 		= FALSE
		doneS1_HEAD 				= FALSE
		doneS1_ATVAN 				= FALSE
		doneS1_ATVAN2 				= FALSE
		doneS1_BIKE					= FALSE
		MikeEarPieceGiven 			= FALSE
		nonTempEventsSetup 			= FALSE
		TrevLookTaskGiven 			= FALSE
		bVanDoorsLocked 			= FALSE
		ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")						
				
		CLEAR_PRINTS()
		
		REMOVE_CUTSCENE()
		
		SET_INTERIOR_CAPPED(INTERIOR_V_RANCH, TRUE)
		
		iStageSection 				= 0
		iMoveStageOnTimer 			= 0
		iTrevDeletedTimer			= 0
		iGetOffCarChatTimer			= 0
		
		SeatNextToVanBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<-358.3, 1130.6, 333.3>>, <<-323.6, 1170.2, 319.6>>)
		
		IF NOT DOES_ENTITY_EXIST(CSTrev)
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				CSTrev = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			ENDIF
		ENDIF
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP()
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		IF DOES_ENTITY_EXIST(PlayerCar)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar)
		ENDIF

		IF DOES_ENTITY_EXIST(TrevCutsceneCar)
			IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
				Store_Vehicle_Snapshot_Only(TrevsStartCar, TrevCutsceneCar, TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(TrevCutsceneCar, FALSE)
			ENDIF
		ENDIF
		
		//Give them both earpieces.
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P1_HEADSET, FALSE)	
			ENDIF
		ENDIF
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET, FALSE)	
		
		missionCanFail = TRUE
		bStageSetup = TRUE
		
//═════════╡ UPDATE ╞═════════
	ELSE
//		PRINTSTRING("CASE = ") PRINTINT(iStageSection) PRINTNL()
		
		//IF Trev set up for blocking of non temp events
		IF nonTempEventsSetup = FALSE
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])	
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
					nonTempEventsSetup = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Create the sniper van once close enough.
		IF iStageSection < 2
			IF NOT DOES_ENTITY_EXIST(SniperVan)			
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDriveToPoint) < 200
					REQUEST_MODEL(BURRITO3)
					REQUEST_MODEL(P_RCSS_FOLDED)
					IF HAS_MODEL_LOADED(BURRITO3)
					AND HAS_MODEL_LOADED(P_RCSS_FOLDED)
						SniperVan = CREATE_VEHICLE(BURRITO3, <<-363.5190, 1142.1714, 323.0888>>, 11.2577)
						SET_ENTITY_LOAD_COLLISION_FLAG(SniperVan, TRUE)
						SET_VEHICLE_ON_GROUND_PROPERLY(SniperVan)
						SET_VEHICLE_UNDRIVEABLE(SniperVan, TRUE)
						SET_VEHICLE_COLOURS(SniperVan, 0,0)
						SET_VEHICLE_DIRT_LEVEL(SniperVan, 10)
						SET_VEHICLE_DISABLE_TOWING(SniperVan, TRUE)
	//					FREEZE_ENTITY_POSITION(SniperVan, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO3)
						SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_LEFT, FALSE, TRUE)
						SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_RIGHT, FALSE, TRUE)
						RemoteSniper = CREATE_OBJECT(P_RCSS_FOLDED, <<-363.5190, 1142.1714, 323.0888>>)
						ATTACH_ENTITY_TO_ENTITY(RemoteSniper, SniperVan, 0, << 0, -1.2, -0.3 >>, <<0,0,0>>)
						SET_MODEL_AS_NO_LONGER_NEEDED(P_RCSS_FOLDED)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(SniperVan)
						SETTIMERB(0)
					ENDIF
				ENDIF
			ELSE
				IF TIMERB() < 2000
					IF IS_VEHICLE_DRIVEABLE(SniperVan)
						SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_LEFT, FALSE, FALSE)
						SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_RIGHT, FALSE, FALSE)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(SniperVan)
					ENDIF
				ENDIF
				IF vanFrozen = FALSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDriveToPoint) < 10
//						FREEZE_ENTITY_POSITION(SniperVan, TRUE)
						vanFrozen = TRUE
					ENDIF
				ENDIF
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDriveToPoint) > 200
					IF DOES_ENTITY_EXIST(RemoteSniper)
						DELETE_OBJECT(RemoteSniper)
					ENDIF
					DELETE_VEHICLE(SniperVan)
				ENDIF
			ENDIF
		ENDIF
			
		//Don't allow Trev to drive off if player is standing in his bodhi 
		IF DOES_ENTITY_EXIST(TrevCutsceneCar)
			IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
//				PRINTSTRING("car offset is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(TrevCutsceneCar, GET_CAM_COORD(GET_DEBUG_CAM()))) PRINTNL()
				IF DOES_ENTITY_EXIST(CSTrev)
					IF NOT IS_PED_INJURED(CSTrev)
						IF IS_PED_IN_VEHICLE(CSTrev, TrevCutsceneCar)
							IF GET_ENTITY_MODEL(TrevCutsceneCar) = BODHI2
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), CSTrev) < 5
								AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									vplayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
									GET_GROUND_Z_FOR_3D_COORD(vplayerCoords, fGroundz)
//									PRINTSTRING("vplayerCoords = ") PRINTVECTOR(vplayerCoords) PRINTNL()
									IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1371.318848,1149.513306,113.365517>>, <<1371.155762,1145.732544,115.321281>>, 1.500000) AND vplayerCoords.z > 114.2 )
									OR (IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), TrevCutsceneCar) AND vplayerCoords.z > (fGroundz + 0.2))
									OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TrevCutsceneCar, <<-0.00188396, -0.300501, 1.24172>>), <<1.5,1.5,2>>)
										IF driveTaskCancelled = FALSE
											CLEAR_PED_TASKS(CSTrev)
											IF NOT IS_VEHICLE_STOPPED(TrevCutsceneCar)
												BRING_VEHICLE_TO_HALT(TrevCutsceneCar, 2, 2)
											ENDIF
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CSTrev, TRUE)
											IF doneGetOffChat = FALSE
												ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, CSTrev, "TREVOR")
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
														IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "M1_GETOFF", CONV_PRIORITY_MEDIUM)
															//Leave it alone, alright?
															//Get your own.
															//Ain't you got a ride, scumbag?
															iGetOffCarChatTimer = GET_GAME_TIMER()
															doneGetOffChat = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											driveTaskCancelled = TRUE
										ENDIF
										IF doneGetOffChat = TRUE
											IF GET_GAME_TIMER() > (iGetOffCarChatTimer + 8000)
												ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, CSTrev, "TREVOR")
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
														IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "M1_GETOFF", CONV_PRIORITY_MEDIUM)
															//Leave it alone, alright?
															//Get your own.
															//Ain't you got a ride, scumbag?
															iGetOffCarChatTimer = GET_GAME_TIMER()
														ENDIF
													ENDIF
												ENDIF	
											ENDIF
										ENDIF
									ELSE
										IF driveTaskCancelled = TRUE
											TASK_VEHICLE_MISSION_COORS_TARGET(CSTrev, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
											driveTaskCancelled = FALSE
										ENDIF										
									ENDIF
								ELSE
									IF driveTaskCancelled = TRUE
										TASK_VEHICLE_MISSION_COORS_TARGET(CSTrev, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
										driveTaskCancelled = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
			
			IF DoneLoseCopsText = FALSE
				CLEAR_PRINTS()
				CLEAR_HELP()
				PRINT_NOW("LOSE_WANTED", DEFAULT_GOD_TEXT_TIME, 1)//Lose the cops.
				DoneLoseCopsText = TRUE
			ENDIF
			
			IF bVanDoorsLocked = FALSE
				IF DOES_ENTITY_EXIST(SniperVan)
					IF IS_VEHICLE_DRIVEABLE(SniperVan)
						SET_VEHICLE_DOORS_LOCKED(SniperVan, VEHICLELOCK_LOCKED)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(SniperVan, FALSE)
						PRINTSTRING("MARTIN 1 : Locking van doors as the player has a wanted level") PRINTNL()
						bVanDoorsLocked = TRUE
					ENDIF
				ENDIF		
			ENDIF
			
			IF DOES_BLIP_EXIST(BlipSniperVan)
				REMOVE_BLIP(BlipSniperVan)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
		
		ELSE

			SWITCH iStageSection
				CASE 0
				
					// Set blip and tell player to drive to observatory
					IF NOT DOES_BLIP_EXIST(blipLocate)
						blipLocate  = ADD_BLIP_FOR_COORD(vDriveToPoint)
					ENDIF
					SET_BLIP_ROUTE(blipLocate, TRUE)
					TRIGGER_MUSIC_EVENT("SOL1_START")
					PRINT_NOW("BFDRIVE", DEFAULT_GOD_TEXT_TIME, 1)//Go to the ~y~observatory.
					iStageSection ++
					
				BREAK
				
				CASE 1
					// Set blip and tell player to drive to observatory
					IF NOT DOES_BLIP_EXIST(blipLocate)
						IF DoneLoseCopsText = TRUE
							CLEAR_PRINTS()
							DoneLoseCopsText = FALSE
						ENDIF
						blipLocate  = ADD_BLIP_FOR_COORD(vDriveToPoint)
						SET_BLIP_ROUTE(blipLocate, TRUE)
					ENDIF
					
					//Trigger interactive music when player gets into a vehicle.
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_DRIVE_TO_OBSERVATORY")
							START_AUDIO_SCENE("MARTIN_1_DRIVE_TO_OBSERVATORY")
						ENDIF
						IF interactiveMusicStarted = FALSE
							TRIGGER_MUSIC_EVENT("SOL1_VEH")
							interactiveMusicStarted = TRUE
						ENDIF
					ENDIF
					
					//Do headset dialogue
					IF doneS1_HEAD = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
								IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_HEAD", CONV_PRIORITY_MEDIUM)
									//Hey, and let's stay in touch on the headsets. 
									//You need anything, get me on the headset.
									doneS1_HEAD = TRUE
								ENDIF
							ENDIF
						ENDIF					
					ENDIF
					
					//Give player his ear piece once he gets into a vehicle
					IF MikeEarPieceGiven = FALSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
							MikeEarPieceGiven = TRUE
						ENDIF
					ENDIF
					
					//Do the dialogue 20 seconds after Trevor has been deleted
					IF NOT DOES_ENTITY_EXIST(CSTrev)
					AND MikeEarPieceGiven = TRUE
					AND GET_GAME_TIMER() > (iTrevDeletedTimer + 30000)
						
						IF doneS1_BIKE = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
									ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
									IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_BIKE", CONV_PRIORITY_MEDIUM)						
										//Mike... Mike, you there? I got some poor bastard's dirt bike. 
										//Okay, well, I ain't at the Observatory yet, start heading that way. 
									
										REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									
										doneS1_BIKE = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF doneS1_ATVAN = FALSE
						AND doneS1_BIKE = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
									ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
									IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_ATVAN", CONV_PRIORITY_MEDIUM)
										//By the way, I know why you took me there, to that guy's house - understood.
										//Understood what? That's he's a terrifying psycho, and you might have that in common? 
										//What is it? We gotta work for him.
										//We don't gotta work for anyone, but I read you, man. Loud and clear. 
										//What're you talking about? 
										//All that Aztec stuff - those weird little artefacts. 
										//What?
										//The little fertility statues or whatever - guy has trouble getting it up, we'll clear two, three million on those easy.
										//We weren't casing the joint. That's not what that was about. 
										//If we didn't agree a fee on this job, so what'd you mean?
										//Okay, the guy lives well. That don't mean we're robbing him, but it does mean he's got the money to pay people who help him. 
										//When the time comes, he'll do the right thing. If he don't let me handle it.  
										//You don't get a wife like that without being rich and an asshole, right?
										//Huh?
										doneS1_ATVAN = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
					
					//Wrap all this in a check to make sure player is not in a taxi
					IF NOT g_bPlayerIsInTaxi
						
						IF DOES_ENTITY_EXIST(SniperVan)
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDriveToPoint, <<100,100,100>>)
								//Save the players car here for STAGE_DESTROY_SNIPER
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
								AND NOT IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
									MichaelsCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									PRINTSTRING("MichaelsCar grabbed") PRINTNL()
								ENDIF
								
								IF IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
									playerHasTakenTaxi = TRUE
								ENDIF
								
								//Request assets for cutscene now so no pause occurs
								REQUEST_MODEL(SHAMAL)
								REQUEST_MODEL(S_M_M_PILOT_01)
								REQUEST_PTFX_ASSET()
								REQUEST_VEHICLE_RECORDING(294, "DGTitanEntry")
								
								REQUEST_REMOTE_SNIPER_ASSETS(sSniperData) 
								iStageSection++
							ENDIF
						ENDIF
					ENDIF
					
				BREAK

				CASE 2
					
//					IF DOES_ENTITY_EXIST(SniperVan)
//					AND DOES_ENTITY_EXIST(RemoteSniper)
//						
//						ATTACH_ENTITY_TO_ENTITY(RemoteSniper, SniperVan, 0, vOffset, vRot)
//						
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
//							vOffset.x = (vOffset.x + 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1)
//							vOffset.x = (vOffset.x - 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2)
//							vOffset.y = (vOffset.y + 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_3)
//							vOffset.y = (vOffset.y - 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_4)
//							vOffset.z = (vOffset.z + 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_5)
//							vOffset.z = (vOffset.z - 0.1)
//						ENDIF
//						
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_6)
//							vRot.x = (vRot.x + 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_7)
//							vRot.x = (vRot.x - 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_8)
//							vRot.y = (vRot.y + 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
//							vRot.y = (vRot.y - 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
//							vRot.z = (vRot.z + 0.1)
//						ENDIF
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
//							vRot.z = (vRot.z - 0.1)
//						ENDIF							
//						
//						PRINTSTRING("vOffset = ") PRINTVECTOR(vOffset) PRINTNL()
//						PRINTSTRING("vRot = ") PRINTVECTOR(vRot) PRINTNL()
//						
//					ENDIF	
						
					IF DOES_ENTITY_EXIST(SniperVan)
						IF IS_VEHICLE_DRIVEABLE(SniperVan)
							SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_LEFT, FALSE, FALSE)
							SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_RIGHT, FALSE, FALSE)
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(SniperVan)
						ENDIF
					ENDIF
						
					//Do some dialogue of Michael arriving at the Van.
					IF doneS1_ATVAN2 = FALSE
						IF DOES_ENTITY_EXIST(SniperVan)
							IF IS_VEHICLE_DRIVEABLE(SniperVan)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), SniperVan) < 30
									KILL_ANY_CONVERSATION()
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
											ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
											IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_ATVAN2", CONV_PRIORITY_MEDIUM)
												//Alright, I see the van with the gun in it. You nearby?
												//Not far out. Shoot the plane down, and I'll be ready. 
												
												REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
												
												doneS1_ATVAN2 = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF						
						
					//Create the remote sniper
					IF sniperCreated = FALSE
						IF HAVE_REMOTE_SNIPER_ASSETS_LOADED(sSniperData)
							CLEAR_AREA(vSniperStartPos, 5, TRUE)
							CREATE_REMOTE_SNIPER(sSniperData, vSniperStartPos, f_sniper_heading)
							LOCK_REMOTE_SNIPER_ROTATION(sSniperData, TRUE, -179, -70)
							LOCK_REMOTE_SNIPER_PITCH(sSniperData, -5, 20)
							SET_REMOTE_SNIPER_ALLOW_QUIT(sSniperData, FALSE)
							sniperCreated = TRUE
						ELSE
							PRINTSTRING("Waiting on sniper assets loading") PRINTNL()
						ENDIF	
					ENDIF
					
					//Set up plane and pilot
					IF NOT DOES_ENTITY_EXIST(vehTitan)
						IF HAS_MODEL_LOADED(SHAMAL)
							vehTitan = CREATE_VEHICLE(SHAMAL, << -1638.6176, -2734.4397, 12.8201 >>, 327.2515)
							SET_ENTITY_INVINCIBLE(vehTitan, TRUE)
							SET_VEHICLE_LIVERY(vehTitan, 1)
							SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
							SET_VEHICLE_ENGINE_ON(vehTitan, TRUE, TRUE)
							CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehTitan)
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF NOT DOES_ENTITY_EXIST(pedPilot)
								IF HAS_MODEL_LOADED(S_M_M_PILOT_01)
									pedPilot = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, S_M_M_PILOT_01)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HEAD, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_BERD, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAIR, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TORSO, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_LEG, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAND, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_FEET, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TEETH, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL2, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_DECL, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_JBIB, 0, 0)
									SET_ENTITY_INVINCIBLE(pedPilot, TRUE)
									SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
								ELSE
//									PRINTSTRING("Model S_M_M_PILOT_01 has not loaded ") PRINTNL()
								ENDIF
							ELSE	
//								PRINTSTRING("pedPilot has been created and exists ") PRINTNL()
							ENDIF
						ENDIF
					ENDIF
					
					//Check flag to see if player took taxi to this stage. If they did then reset flag once they've got out so the flag is reset for fail condition.
					IF playerHasTakenTaxi = TRUE
						IF NOT IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
							playerHasTakenTaxi = FALSE
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(SniperVan)
						IF DOES_BLIP_EXIST(blipLocate)
							REMOVE_BLIP(blipLocate)
						ENDIF
						IF NOT DOES_BLIP_EXIST(BlipSniperVan)
							IF DoneLoseCopsText = TRUE
								CLEAR_PRINTS()
								DoneLoseCopsText = FALSE
							ENDIF
							BlipSniperVan = CREATE_BLIP_FOR_VEHICLE(SniperVan)
							IF IS_VEHICLE_DRIVEABLE(SniperVan)
								SET_VEHICLE_DOORS_LOCKED(SniperVan, VEHICLELOCK_UNLOCKED)
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(SniperVan, TRUE)
								PRINTSTRING("MARTIN 1 : Unlocking van doors as the player does not have a wanted level") PRINTNL()
								bVanDoorsLocked = FALSE
							ENDIF
							IF doneSnipe8Text2 = FALSE
								PRINT_NOW("SNIPE8", DEFAULT_GOD_TEXT_TIME, 1)//Get in the ~b~van.
								doneSnipe8Text2 = TRUE
							ENDIF
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(SniperVan)
							IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
								FREEZE_ENTITY_POSITION(SniperVan, FALSE)
							ELSE
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
										FREEZE_ENTITY_POSITION(SniperVan, TRUE)
									ENDIF
								ENDIF
							ENDIF
							IF sniperVanBlipRouteSet = FALSE
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), SniperVan) > 150
									SET_BLIP_ROUTE(BlipSniperVan, TRUE)
									sniperVanBlipRouteSet = TRUE
								ENDIF
							ENDIF
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
								IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_DRIVE_TO_OBSERVATORY")
									STOP_AUDIO_SCENE("MARTIN_1_DRIVE_TO_OBSERVATORY")
								ENDIF
								sfSniperLoading = REQUEST_SCALEFORM_MOVIE("REMOTE_SNIPER_LOADING")
								//Start interactive music now for sniper being ready
								TRIGGER_MUSIC_EVENT("SOL1_SNIPER_READY")
								PREPARE_MUSIC_EVENT("SOL1_APP_ACTIVE")
								SET_MAX_WANTED_LEVEL(0)
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								iMoveStageOnTimer = GET_GAME_TIMER()
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
								iStageSection++
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE 3
				
					IF DOES_ENTITY_EXIST(SniperVan)
						IF IS_VEHICLE_DRIVEABLE(SniperVan)
							SET_VEHICLE_ENGINE_ON(SniperVan, FALSE, TRUE)
							SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_LEFT, FALSE, FALSE)
							SET_VEHICLE_DOOR_OPEN(SniperVan, SC_DOOR_REAR_RIGHT, FALSE, FALSE)
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(SniperVan)
						ENDIF
					ENDIF
					
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
					
					//Have a slight pause to make sure that the player is completely inside the van.
					IF bMissionFailed = FALSE
						IF GET_GAME_TIMER() > (iMoveStageOnTimer + 1500)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							IF DOES_ENTITY_EXIST(SniperVan)
								IF IS_VEHICLE_DRIVEABLE(SniperVan)
									FREEZE_ENTITY_POSITION(SniperVan, TRUE)
								ENDIF
							ENDIF
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CLEAR_PRINTS()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							iStageSection++
						ENDIF
					ENDIF
				
				BREAK
				
				CASE 4
					
					IF DOES_BLIP_EXIST(BlipSniperVan)
						REMOVE_BLIP(BlipSniperVan)
					ENDIF					
					
					//check here if TOD skip is required or not
//					IF GET_TIMEOFDAY_HOUR(sTimeHour) > 8
//					AND GET_TIMEOFDAY_HOUR(sTimeHour) < 20
					WHILE NOT DO_TIMELAPSE(SP_MISSION_MARTIN_1, sTimelapse, FALSE, FALSE, TRUE, TRUE)
						timelapseRequired = TRUE
						WAIT(0)
					ENDWHILE
					
					IF timelapseRequired = FALSE
						//Wait for the scaleform movie to load for the loading screen for the sniper app
				    	WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(sfSniperLoading)
				        	PRINTSTRING("Waiting for scaleform movie loading") PRINTNL()
							WAIT(0)
				     	ENDWHILE
							
						SETTIMERB(0)
						CLEAR_HELP()
						CLEAR_PRINTS()
	//					SET_FRONTEND_RADIO_ACTIVE(FALSE)
						iStageSection ++
					ELSE
						iStageSection = 7
					ENDIF
				
				BREAK
				
				CASE 5 
					
					//CALL THE METHODS USING 
					IF HAS_SCALEFORM_MOVIE_LOADED(sfSniperLoading)
						IF interactiveMusicStarted2 = FALSE
							TRIGGER_MUSIC_EVENT("SOL1_APP_ACTIVE")
							SETTIMERB(0)
							interactiveMusicStarted2 = TRUE
						ENDIF
						SET_MULTIHEAD_SAFE(TRUE,TRUE)	//Instant blinders
						CALL_SCALEFORM_MOVIE_METHOD(sfSniperLoading,"START_LOADING")
						SETTIMERA(0)
						iLoadingScreenTimer = GET_GAME_TIMER()
						PLAY_SOUND_FRONTEND(-1, "SCOPE_UI_MASTER")
						iStageSection++
					ENDIF
					
				BREAK
				
				CASE 6
					
					PRINTSTRING("timer A = ") PRINTINT(TIMERA()) PRINTNL()
//					INT iLeftX, iLeftY, iRightX, iRightY
//					
//					GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iLeftX, iLeftY, iRightX, iRightY)
//					
//					iLeftX = 0
//					iLeftY = 0
//					iRightX = 0
//					iRightY = 0
					
	                IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(sfReturnBool)
						PRINTSTRING("IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY = FALSE ") PRINTNL()
						BEGIN_SCALEFORM_MOVIE_METHOD(sfSniperLoading, "IS_LOAD_ANIM_FINISHED")
						sfReturnBool = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
					ENDIF
					
					//Use this as back up to make sure stage moves on incase scaleform return values break.
					IF hasSniperBeenSetToActive = FALSE
						IF GET_GAME_TIMER() > iLoadingScreenTimer + 5999	
							SET_REMOTE_SNIPER_ACTIVE(sSniperData, TRUE)
							hasSniperBeenSetToActive = TRUE
						ENDIF
					ENDIF

					IF GET_GAME_TIMER() > iLoadingScreenTimer + 100
						RemoteSniperNeedsUpdated = TRUE
					ENDIF
					IF GET_GAME_TIMER() > iLoadingScreenTimer + 6000
					AND hasSniperBeenSetToActive = TRUE
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfSniperLoading)
						IF DOES_ENTITY_EXIST(MichaelsCar)
							IF IS_VEHICLE_DRIVEABLE(MichaelsCar)
								CLEAR_AREA(<<-386.6838, 1202.5878, 324.6417>>, 5, TRUE)
								SET_ENTITY_COORDS(MichaelsCar, <<-386.6838, 1202.5878, 324.6417>>)
								SET_ENTITY_HEADING(MichaelsCar, 100.5474)	
								SET_MISSION_VEHICLE_GEN_VEHICLE(MichaelsCar, <<-386.6838, 1202.5878, 324.6417>>, 100.5474)
							ENDIF
						ELSE
							PRINTSTRING("MichaelsCar does NOT exist") PRINTNL()
						ENDIF						
						
						iStageSection ++		
					ENDIF

				BREAK
				
			ENDSWITCH
		ENDIF
		
//═════════╡ CLEANUP ╞═════════
		IF iStageSection > 6
		AND bMissionFailed = FALSE
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
			IF DOES_BLIP_EXIST(BlipSniperVan)
				REMOVE_BLIP(BlipSniperVan)
			ENDIF			
			
			RemoteSniperNeedsUpdated = TRUE
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:		Performs the cutscene which introduces the garage and the three characters
//FUNC BOOL stageCutsceneArrive()
//
////═════════╡ SETUP ╞═════════
//	IF NOT bStageSetup	
//		Set_Replay_Mid_Mission_Stage(ENUM_TO_INT(STAGE_CUTSCENE_ARRIVE))
//		RESET_NEED_LIST()
//		SET_ANIM_DICT_AS_NEEDED(Asset.adTorture)
//		SET_MODEL_AS_NEEDED(Asset.mnTREVOR)
//		SET_MODEL_AS_NEEDED(SANCHEZ)
//		SET_MODEL_AS_NEEDED(SHAMAL)
//		REQUEST_MODEL(S_M_M_PILOT_01)	
//		MANAGE_LOADING_NEED_LIST()
//				
//		doStartCutscene()
//		
//		SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 160.4131)
//		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//
//		ADD_ALL_PEDS_TO_DIALOGUE()
//	
//		iStageSection = 0
//		bCameraShotSetup = FALSE
//		bStageSetup = TRUE
//
////═════════╡ UPDATE ╞═════════		
//	ELSE
//		REQUEST_ADDITIONAL_COLLISION_AT_COORD(<< 614.8568, -629.4142, 303.2502 >>)
//		SWITCH iStageSection
//			CASE 0
//				//SETUP
//				IF NOT bCameraShotSetup	
//					TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), Asset.adTorture, Asset.animWalk, << -443.787, 1060.879, 327.676 >>, << 0.000, 0.000, 180.000 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//					doTwoPointCam(	<< -440.9612, 1056.7878, 328.0601 >>, << -1.0701, 0.0595, 41.0614 >>, 17.7497,<< -440.6522, 1056.4385, 328.0631 >>, << -0.8867, 0.0595, 43.4512 >>, 17.0194, 3000)
//					doCamShake(Asset.shakeRoadVib, 0.1)
//					
//					runConversation("S1_RDY", TRUE, TRUE, FALSE)
//						//You got a good view, Michael?
//						//I'd say so. You better be ready for this.
//						//Michael, the plane's just come into range.
//					
//					SETTIMERA(0)	
//					
//					//Set up bike AND TREVOR
//					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, << 768.7714, 1312.5142, 359.0287 >>, 24.1579)
//						SafeWait(0)
//					ENDWHILE
//					
//					SAFE_ADD_PED_FOR_DIALOGUE(GET_SELECT_PED(SEL_TREVOR), 2, "TREVOR")
//					
//					vehBike = CREATE_VEHICLE(SANCHEZ,<< 768.7714, 1312.5142, 359.0287 >>, 24.1579)
//					SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_TREVOR), vehBike)		
//					SET_VEHICLE_ENGINE_ON(vehBike, TRUE, TRUE)
//					
//					//Set up target plane
//					vehTitan = CREATE_VEHICLE(SHAMAL, << -1638.6176, -2734.4397, 12.8201 >>, 327.2515)
//					pedPilot = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, S_M_M_PILOT_01)
//					SET_ENTITY_INVINCIBLE(vehTitan, TRUE)				
//					CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)
//					
//					bCameraShotSetup = TRUE
//
//				//UPDATE
//				ELSE					
//					
//				//CLEANUP
//					IF TIMERA() > 3000
//						iStageSection++
//						bCameraShotSetup = FALSE
//					ENDIF
//				ENDIF
//			BREAK
//			CASE 1
//				//SETUP
//				IF NOT bCameraShotSetup	
//					doTwoPointCam(	<< -444.1410, 1060.5026, 328.1403 >>, << -4.6504, -0.2752, -156.8803 >>, 45.9326, << -444.1410, 1058.6980, 328.1403 >>, << -7.0790, -0.2752, 176.2011 >>, 45.9326, 6000, GRAPH_TYPE_SIN_ACCEL_DECEL)
////					TASK_PLAY_ANIM_ADVANCED(GET_SELECT_PED(SEL_MICHAEL), Asset.adMovement, Asset.animIdle, << -443.272, 1059.564, 327.668 >>, << 0.000, 0.000, 180.000 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_SELECT_PED(SEL_MICHAEL))
//					
//					doCamShake(Asset.shakeRoadVib, 0.1)
//					
//					SETTIMERA(0)
//					bCameraShotSetup = TRUE
//				
//				//UPDATE
//				ELSE
//					
//				//CLEANUP
//					IF TIMERA() > 6000
//						iStageSection++
//						bCameraShotSetup = FALSE
//					ENDIF
//				ENDIF
//			BREAK
//			CASE 2
//				//SETUP
//				IF NOT bCameraShotSetup	
//					//LOAD_SCENE(<< 619.0132, -625.1191, 300.9872 >>)
//										
//					IF IS_VEHICLE_DRIVEABLE(vehTitan)	
//						START_PLAYBACK_RECORDED_VEHICLE(vehTitan, iRecNo, "DGTitanEntry")
//						SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, Asset.skipPlaneEntryStart)
//						SET_PLAYBACK_SPEED(vehTitan, 0.6)
//					
////						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTitan,VS_FRONT_RIGHT)
////						SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
//					ENDIF
//					
//					doTwoPointCam(	<< 614.8568, -629.4142, 303.2502 >>, << -10.7105, -0.0621, 100.9647 >>, 32.1174,<< 486.0085, -587.2617, 309.6030 >>, << -09.2317, 0.03570, 114.8343 >>, 31.5067, 4700)
//								
//					doCamShake(Asset.shakeRoadVib, 0.1)
//					
//					//Request vehicle recording for plane in next stage here to avoid loading times for it		
//					REQUEST_VEHICLE_RECORDING(iRecNo, "DGTitanEntry")		
//					
//					SETTIMERA(0)
//					bCameraShotSetup = TRUE
//				
//				//UPDATE
//				ELSE		
//					//PRINTINT(TIMERA())PRINTNL()
//					
//				//CLEANUP
//					IF TIMERA() > 4700
//												
//						stopCamShake(TRUE)
//
//						iStageSection++
//						bCameraShotSetup = FALSE
//					ENDIF
//				ENDIF
//			BREAK
//		ENDSWITCH
//		
////═════════╡ JSKIP ╞═════════
//		IF JSkip
//		OR (IS_CUTSCENE_SKIP_BUTTON_PRESSED() AND ReadyToSkipCutscene())	
//			doEndCutscene()
//			
//			iStageSection = 3
//		ENDIF
//				
////═════════╡ CLEANUP ╞═════════
//		IF iStageSection >= 3
////			SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), << -443.2711, 1059.5475, 326.6811 >>)
////			SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 188.1957)
////			SET_ENTITY_VISIBLE(GET_SELECT_PED(SEL_MICHAEL),TRUE)
//			
//			stopCamShake(TRUE)
//
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//
//			doEndCutscene()
//			bStageSetup = FALSE
//			bCameraShotSetup = FALSE
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

//PURPOSE:		Performs the section where TREVOR tortures the victim for the first time
FUNC BOOL stageShootPlane()
	
	IF DOES_ENTITY_EXIST(vehTitan)
		IF IS_VEHICLE_DRIVEABLE(vehTitan)
			IF landingGearPutAway = FALSE
				CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)
				landingGearPutAway = TRUE
			ENDIF
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
	//			STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
	//			FREEZE_ENTITY_POSITION(vehTitan, TRUE)
				PRINTSTRING("plane playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehTitan)) PRINTNL()
			ENDIF
		ENDIF
	ENDIF
//	PRINTSTRING("plane offset is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTitan, GET_CAM_COORD(GET_DEBUG_CAM()))) PRINTNL()
	
	//Call this everyframe during this section.
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.1)	
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2164315
	
	// disable replay cam movement 1978527
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	
	//Audio streams
	IF bStageSetup
		IF iNumberOfHits = 0
			IF subStream1Loaded = FALSE
				IF LOAD_STREAM("MARTIN_1_LEAR_JET_HIT_1_MASTER")
					PRINTSTRING("MARTIN_1_LEAR_JET_HIT_1_MASTER is ready")
					subStream1Loaded = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF iNumberOfHits = 1
		AND hasEffectStarted = TRUE
			IF subStream2Loaded = FALSE
				IF LOAD_STREAM("MARTIN_1_LEAR_JET_HIT_2_MASTER")
					PRINTSTRING("MARTIN_1_LEAR_JET_HIT_2_MASTER is ready")
					subStream2Loaded = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	g_bDisableNoirLensEffect = TRUE
	
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_SHOOT_PLANE")
		PRINTSTRING("MISSION STAGE SHOOT PLANE") PRINTNL()
		
		missionCanFail = FALSE
//		RESET_NEED_LIST()
//		SET_MODEL_AS_NEEDED(Asset.mnTREVOR)
//		SET_MODEL_AS_NEEDED(SANCHEZ)
//		SET_MODEL_AS_NEEDED(SHAMAL)
//		REQUEST_MODEL(S_M_M_PILOT_01)	
//		MANAGE_LOADING_NEED_LIST()	
		
//		UPDATE_REMOTE_SNIPER(sSniperData)
		
//		SET_FRONTEND_RADIO_ACTIVE(FALSE)
		
		//Request the vehicle recording needed for the cutscene now
		REQUEST_VEHICLE_RECORDING(iRecNo, "DGTitanCrash")
		
		SET_PAD_CAN_SHAKE_DURING_CUTSCENE(FALSE)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				
//		REQUEST_MODEL(PLAYER_TWO)
//		REQUEST_MODEL(SANCHEZ)				
//				
//		//Set up bike AND TREVOR
//		IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, << 768.7714, 1312.5142, 359.0287 >>, 24.1579)
//				SafeWait(0)
//				UPDATE_REMOTE_SNIPER(sSniperData)
//				PRINTSTRING("SOLOMON1_WAITING_19") PRINTNL()
//			ENDWHILE
//		ELSE
//			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], << 768.7714, 1312.5142, 359.0287 >>)
//				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 24.1579)	
//			ENDIF
//		ENDIF
//		
//		IF NOT DOES_ENTITY_EXIST(vehBike)
//			vehBike = CREATE_VEHICLE(SANCHEZ,<< 768.7714, 1312.5142, 359.0287 >>, 24.1579)
//			SET_VEHICLE_ENGINE_ON(vehBike, TRUE, TRUE)
//		ELSE
//			IF IS_VEHICLE_DRIVEABLE(vehBike)
//				SET_ENTITY_COORDS(vehBike, << 768.7714, 1312.5142, 359.0287 >>)
//				SET_ENTITY_HEADING(vehBike, 24.1579)	
//			ENDIF
//		ENDIF		
//		
//		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//			IF IS_VEHICLE_DRIVEABLE(vehBike)
//				IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehBike)
//					SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehBike)
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		//Give TREVOR headset
//		SET_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//		SET_PED_COMPONENT_ITEM_CURRENT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P1_HEADSET)	
//		
//		SAFE_ADD_PED_FOR_DIALOGUE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 2, "TREVOR")
		
		CLEAR_AREA(vSniperStartPos, 5, TRUE)
		
		//bDoLeftEngineCut = FALSE
		bDoRightEngineCut 			= FALSE
		bRightEngineShot			= FALSE
		assetsRequested 			= FALSE
		doneTakeDownPlaneWarning 	= FALSE
		floatingHelp1Done 			= FALSE
		screenFadedInFromReplay 	= FALSE
		DamageEnginesTextDone 		= FALSE
		bMissionFailedToSlow 		= FALSE
		doneChatGun					= FALSE
		bCamShakeAnimDictRequested	= FALSE
		hasEffectStarted 			= FALSE
		doneHelp1					= FALSE
		doneHelp2					= FALSE
		subStream1Loaded			= FALSE
		subStream2Loaded			= FALSE
		BulletCamActive 			= FALSE
		doneS1_SEE 					= FALSE
		doneS1_LEAD 				= FALSE
		doneS1_LOOK 				= FALSE
		PlaneSlowedDown 			= FALSE
		TimeToCreatePlane 			= FALSE
		landingGearPutAway 			= FALSE
		bCamNearClipSet				= FALSE
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		
		iNumberOfHits 				= 0
		iShotTimer					= 0
		iSeePlaneChatTimer			= 0
		iMissChatTimer = GET_GAME_TIMER()
				
		//Clear area of path for TREVOR on bike		
		CLEAR_AREA(<<-318.28, 1398.18, 351.69>>, 170, TRUE)
		CLEAR_AREA(<<-397.8, 1205.1, 324.8>>, 50, TRUE)
				
		ADD_ALL_PEDS_TO_DIALOGUE()
			
//		PREPARE_MUSIC_EVENT("SOL1_ENGINE_HIT")	
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_SNIPER_CAMERA")
			START_AUDIO_SCENE("MARTIN_1_SNIPER_CAMERA")
		ENDIF		
		
		IF timelapseRequired = TRUE
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)

			//Use this as back up to make sure stage moves on incase scaleform return values break.
			IF hasSniperBeenSetToActive = FALSE
				SET_REMOTE_SNIPER_ACTIVE(sSniperData, TRUE)
				hasSniperBeenSetToActive = TRUE
			ENDIF	

			RemoteSniperNeedsUpdated = TRUE
		ENDIF
		
		RemoteSniperNeedsUpdated = TRUE
		
//		//Move Michael to here so that the area is loaded in for when the plane cutscene is playing.
		IF NOT IS_REPLAY_BEING_SET_UP()
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-455.6177, 1083.2539, 326.6815>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 111.0994)	
		ENDIF
		
		SET_MAX_WANTED_LEVEL(0)
		
		SETTIMERB(0)	
//		ishotsFired = 0		
		iMiniCutStage = 0
		iStageSection = 0
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			IF IS_REPLAY_BEING_SET_UP()
				NEW_LOAD_SCENE_STOP()
			ENDIF
			END_REPLAY_SETUP(SniperVan)
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//B* 2230206: Activate multihead blinders in case of a fail retry
		SET_MULTIHEAD_SAFE(TRUE,TRUE)
		
		missionCanFail = TRUE
		bStageSetup = TRUE
		
//═════════╡ UPDATE ╞═════════		
	ELSE
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
		
		//Load the camera's for the cutscene
		IF NOT bCamShakeAnimDictRequested 
			REQUEST_ANIM_DICT(strAnimDictCamShake)
			bCamShakeAnimDictRequested = TRUE
		ENDIF	
	
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_LB)
		
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 9, 0)
//		PRINTSTRING("ishotsFired = ") PRINTINT(ishotsFired) PRINTNL()
		
		SWITCH iStageSection
		
			CASE 0
								
//				UPDATE_REMOTE_SNIPER(sSniperData)
				
				IF screenFadedInFromReplay = FALSE 	
				AND bMissionFailed = FALSE
					IF TIMERB() > 1000
						IF NOT IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							screenFadedInFromReplay = TRUE
						ENDIF
					ENDIF
				ENDIF
					
				IF GET_PED_LAST_WEAPON_IMPACT_COORD(PLAYER_PED_ID(), vImpact)
//					PRINTSTRING("BULLET IMPACT COORDS: ") PRINTVECTOR(vImpact) PRINTNL()
				ENDIF

				IF NOT bDoRightEngineCut
					
					//start 1st effect	
					IF iNumberOfHits = 1 
						IF hasEffectStarted = FALSE
							
							//Call every frame for these cutscene's so that the plane stays in HD
							IF DOES_ENTITY_EXIST(vehTitan)
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
									REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehTitan) 
								ENDIF
							ENDIF	
							
							IF NOT DOES_CAM_EXIST(BulletCam)
							AND GET_GAME_TIMER() > (iShotTimer + 500)
								IF IS_HELP_MESSAGE_BEING_DISPLAYED()
									CLEAR_HELP()
								ENDIF
								CLEAR_PRINTS()
								BulletCamActive = TRUE
								SET_REMOTE_SNIPER_ACTIVE(sSniperData, FALSE)
								BulletCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
								SET_CAM_FOV(BulletCam, 22.4)
								SHAKE_CAM(BulletCam, "SKY_DIVING_SHAKE", 1.5)
								ATTACH_CAM_TO_ENTITY(BulletCam, vehTitan, << 4.88167, 6.58488, 0.16096 >>)
								POINT_CAM_AT_ENTITY(BulletCam, vehTitan, << 1.2611, -2.12109, 0.750921 >>)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								DISPLAY_RADAR(FALSE)
								DISPLAY_HUD(FALSE)
								IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_BULLET_HITS_PLANE_SCENE")
									
									START_AUDIO_SCENE("MARTIN_1_BULLET_HITS_PLANE_SCENE")
								ENDIF	
								IF subStream1Loaded = TRUE
									PLAY_STREAM_FRONTEND()
								ENDIF
								iEffectTimer = GET_GAME_TIMER()
							ENDIF
							
							IF bEffectTriggered = FALSE
								IF DOES_CAM_EXIST(BulletCam)
								AND GET_GAME_TIMER() > (iEffectTimer + 400)
									ptfxEngine[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_smoke",vehTitan,vEngineOffset1,VECTOR_ZERO)
									SET_PARTICLE_FX_LOOPED_SCALE(ptfxEngine[0], 2.0)
									SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxEngine[0], "Shot1", 1.0)
									iEffectTimer = GET_GAME_TIMER()
									
									REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									
									bEffectTriggered = TRUE
								ENDIF
							ENDIF
							
							IF bEffectTriggered = TRUE
							AND GET_GAME_TIMER() > (iEffectTimer + 2500) 
								BulletCamActive = FALSE
								IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_BULLET_HITS_PLANE_SCENE")
									STOP_AUDIO_SCENE("MARTIN_1_BULLET_HITS_PLANE_SCENE")
								ENDIF	
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								DISPLAY_RADAR(TRUE)
								DISPLAY_HUD(TRUE)
								DESTROY_CAM(BulletCam)
								SET_REMOTE_SNIPER_ACTIVE(sSniperData, TRUE)
								sSniperData.f_current_sniper_fov = 4
								hasEffectStarted = TRUE
								PRINT_HELP("M1_HELP3")//~s~The plane will need multiple shots to be taken down.
							ENDIF
							
						ENDIF
					ENDIF
						
					//start 2nd effect	
					IF iNumberOfHits = 2
						IF hasEffectStarted = FALSE
						
							//Call every frame for these cutscene's so that the plane stays in HD
							IF DOES_ENTITY_EXIST(vehTitan)
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
									REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehTitan) 
								ENDIF
							ENDIF						
						
							IF NOT DOES_CAM_EXIST(BulletCam)
							AND GET_GAME_TIMER() > (iShotTimer + 500)
								IF IS_HELP_MESSAGE_BEING_DISPLAYED()
									CLEAR_HELP()
								ENDIF
								CLEAR_PRINTS()
								BulletCamActive = TRUE
								SET_REMOTE_SNIPER_ACTIVE(sSniperData, FALSE)
								BulletCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
								SET_CAM_FOV(BulletCam, 22.4)
								SHAKE_CAM(BulletCam, "SKY_DIVING_SHAKE", 1.5)
								ATTACH_CAM_TO_ENTITY(BulletCam, vehTitan, << 6.63932, 5.91098, 0.482164 >>)
								POINT_CAM_AT_ENTITY(BulletCam, vehTitan, << 1.87226, -2.71828, 0.671346 >>)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								DISPLAY_RADAR(FALSE)
								DISPLAY_HUD(FALSE)
								IF subStream2Loaded = TRUE
									PLAY_STREAM_FRONTEND()
								ENDIF
								IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_BULLET_HITS_PLANE_SCENE")
								
									START_AUDIO_SCENE("MARTIN_1_BULLET_HITS_PLANE_SCENE")
								ENDIF	
								iEffectTimer = GET_GAME_TIMER()
							ENDIF
							
							IF bEffectTriggered = FALSE
								IF DOES_CAM_EXIST(BulletCam)
								AND GET_GAME_TIMER() > (iEffectTimer + 400)
									IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[0])
										SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxEngine[0], "Shot2", 1.0)
									ENDIF
									iEffectTimer = GET_GAME_TIMER()
									
									REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									
									bEffectTriggered = TRUE
								ENDIF
							ENDIF
							
							IF bEffectTriggered = TRUE
							AND GET_GAME_TIMER() > (iEffectTimer + 2500) 
								BulletCamActive = FALSE
								IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_BULLET_HITS_PLANE_SCENE")
									STOP_AUDIO_SCENE("MARTIN_1_BULLET_HITS_PLANE_SCENE")
								ENDIF
								STOP_STREAM()
								
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								DESTROY_CAM(BulletCam)
								DISPLAY_RADAR(TRUE)
								DISPLAY_HUD(TRUE)
								
								SET_REMOTE_SNIPER_ACTIVE(sSniperData, TRUE)
								sSniperData.f_current_sniper_fov = 4
								hasEffectStarted = TRUE
							ENDIF										
							
						ENDIF														
					ENDIF
						
					//start 3rd effect and move stage on to start cutscene
					IF iNumberOfHits = 3	
						IF hasEffectStarted = FALSE
						
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						
							IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[0])
								SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxEngine[0], "Shot3", 1.0)
							ENDIF
							hasEffectStarted = TRUE
						ENDIF
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						
						bRightEngineShot = TRUE
						bDoRightEngineCut = TRUE
						
						iStageSection = 1
						
						//Stats this stops the timer for checking how long it takes the player to shoot down the plane
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED() 
					ENDIF								
							
					IF NOT sSniperData.b_is_sniping
//						IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_MICHAEL),vehTitan)
//							SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), << -443.2711, 1059.5475, 326.6811 >>)
//							SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 188.1957)
//							SET_ENTITY_VISIBLE(GET_SELECT_PED(SEL_MICHAEL),TRUE)
//						ENDIF
					ELSE
						
						IF assetsRequested = FALSE
							PRINTSTRING("assetsRequested = FALSE") PRINTNL()
							REQUEST_VEHICLE_RECORDING(1, "BB_SOL_JET")
							REQUEST_VEHICLE_RECORDING(294, "DGBFReady")
							assetsRequested = TRUE
						ENDIF

						IF NOT bRightEngineShot
							IF bMissionFailed = FALSE
								//Do some dialogue from Michael commenting on the guns coolness.
								IF doneHelp2 = FALSE
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										PRINT_HELP("M1_HELP2")//~s~Move the rifle with ~INPUTGROUP_LOOK~
										doneHelp2 = TRUE
									ENDIF
								ENDIF
								IF doneHelp2 = TRUE
									IF doneHelp1 = FALSE
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											PRINT_HELP("M1_HELP1") //~s~Zoom in and out with ~INPUT_SNIPER_ZOOM~.
											doneHelp1 = TRUE
										ENDIF
									ENDIF
								ENDIF

								IF doneChatGun = FALSE
								AND bSniperIsBeingReplayed = FALSE
									ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
									ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
									IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_CONTROL", CONV_PRIORITY_MEDIUM)
										//T, if you could see this gun, it might convince you Martin Madrazo ain't someone to mess with. 
										//The fancier the gun, the bigger the pussy who owns it. Now take down the jet. 
										doneChatGun = TRUE
									ENDIF
								ENDIF
								
								//Depending if mission is being replayed or not determine how quick to spawn in the plane/
								IF TimeToCreatePlane = FALSE
									IF bSniperIsBeingReplayed = FALSE
										IF TIMERB() > 12500
											iSeePlaneChatTimer = GET_GAME_TIMER()
											TimeToCreatePlane = TRUE
										ENDIF
									ELSE
										IF TIMERB() > 2500
											iSeePlaneChatTimer = GET_GAME_TIMER()
											TimeToCreatePlane = TRUE
										ENDIF	
									ENDIF
								ENDIF
									
								IF TimeToCreatePlane = TRUE
									//Have some dialogue of Michael spotting the jet once it's been created
									IF GET_GAME_TIMER() > (iSeePlaneChatTimer + 1500)
										IF doneS1_SEE = FALSE
											IF DOES_ENTITY_EXIST(vehTitan)
												IF IS_ENTITY_ON_SCREEN(vehTitan)
												AND iNumberOfHits = 0
													IF NOT IS_MESSAGE_BEING_DISPLAYED()
														IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
															ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
															ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
															IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_SEE", CONV_PRIORITY_MEDIUM)
																//There it is - out past Downtown.
																//I got her - South West of the city. 
																doneS1_SEE = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF doneS1_LEAD = FALSE
									AND doneS1_SEE = TRUE
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
												ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
												ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
												IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_LEAD", CONV_PRIORITY_MEDIUM)
													//The jet's moving. Got to give a lead. 
													//Gotta shoot in front to compensate for the speed of the jet. 
													doneS1_LEAD = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								
									//Set up target plane
									IF NOT DOES_ENTITY_EXIST(vehTitan)
										REQUEST_MODEL(SHAMAL)
										REQUEST_MODEL(S_M_M_PILOT_01)
										IF HAS_MODEL_LOADED(SHAMAL)
										AND HAS_MODEL_LOADED(S_M_M_PILOT_01)
											
											vehTitan = CREATE_VEHICLE(SHAMAL, << -1638.6176, -2734.4397, 12.8201 >>, 327.2515)
											SET_VEHICLE_LIVERY(vehTitan, 1)
											SET_VEHICLE_ENGINE_ON(vehTitan, TRUE, TRUE)
											SET_ENTITY_LOAD_COLLISION_FLAG(vehTitan, TRUE)
											SET_ENTITY_INVINCIBLE(vehTitan, TRUE)
											CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)	
												
											pedPilot = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, S_M_M_PILOT_01)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HEAD, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_BERD, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAIR, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TORSO, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_LEG, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAND, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_FEET, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TEETH, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL2, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_DECL, 0, 0)
											SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_JBIB, 0, 0)											

											CHECK_PED_WITH_REMOTE_SNIPER(sSniperData, pedPilot)	
											
											SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
											SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
										ENDIF
									ENDIF
									
									//Create pilot in plane
									IF DOES_ENTITY_EXIST(vehTitan)	
									
										//Add in a temp blip for the plane here so it can be seen if game is paused on map.Note to be removed once the cutscene starts. 
										IF NOT DOES_BLIP_EXIST(blipObj)
											IF IS_VEHICLE_DRIVEABLE(vehTitan)
												blipObj = ADD_BLIP_FOR_ENTITY(vehTitan)
												SET_BLIP_AS_FRIENDLY(blipObj, FALSE)
											ENDIF
										ENDIF									
									
										IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
											START_PLAYBACK_RECORDED_VEHICLE(vehTitan, 294, "DGTitanEntry")
											SET_PLAYBACK_SPEED(vehTitan, 0.616)
										ENDIF
										
										IF PlaneSlowedDown = FALSE
											IF DOES_ENTITY_EXIST(vehTitan)
												IF IS_VEHICLE_DRIVEABLE(vehTitan)
													IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
														IF GET_TIME_POSITION_IN_RECORDING(vehTitan) > 14136
															SET_PLAYBACK_SPEED(vehTitan, 0.55)
															PlaneSlowedDown = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										//Call this every frame after the plane has been created to draw the red target square. Only draw when the bulletcam is not active
										IF BulletCamActive = FALSE
											drawAimSquare(sSniperData, vEngineOffset1, fCrossScreenPositionX[1], fCrossScreenPositionY[1])
										ENDIF
										
										//Have some help text and god text once the plane has been created.
										IF floatingHelp1Done = FALSE
											PRINT_HELP("BFHELPTXT4")//Aim for the red target calculated to compensate for the distance.
											floatingHelp1Done = TRUE
										ENDIF										
										
										IF DamageEnginesTextDone = FALSE
											IF bSniperIsBeingReplayed = FALSE
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
													PRINT_NOW("BFDAMAGE", DEFAULT_GOD_TEXT_TIME, 1)//Shoot the ~r~plane's engine.
													DamageEnginesTextDone = TRUE
												ENDIF
											ELSE
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
													PRINT_NOW("BFDAMAGE", 4000, 1)//Shoot the ~r~plane's engine.
													DamageEnginesTextDone = TRUE
												ENDIF	
											ENDIF
										ENDIF										
										
									ENDIF
								ELSE
									//Have some dialogue of Michael looking for the jet
									IF bSniperIsBeingReplayed = FALSE
										IF doneS1_LOOK = FALSE
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
													ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
													ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
													IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_LOOK", CONV_PRIORITY_MEDIUM)
														//Where is it?
														//Looking for it.
														//Hold on.
														//Can't see it yet.
														doneS1_LOOK = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Handle where the traffic is spawned when player is using the sniper.
						USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
						
						IF doneTakeDownPlaneWarning = FALSE
							IF IS_VEHICLE_DRIVEABLE(vehTitan)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
									IF GET_TIME_POSITION_IN_RECORDING(vehTitan) > 20000
										PRINT_NOW("SNIPE4", DEFAULT_GOD_TEXT_TIME, 1)//Take the plane out before it's too late.
										doneTakeDownPlaneWarning = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
//						PRINTSTRING("Cam fov is ") PRINTFLOAT(GET_FINAL_RENDERED_CAM_FOV()) PRINTNL()
						
						IF bMissionFailedToSlow = FALSE
							IF TIMERB() > 1000
								//Stats this starts the timer for checking how long it takes the player to shoot down the plane
								INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MAR1_PLANE_HIT_TIME)
								
								IF NOT bRightEngineShot //OR NOT bLeftEngineShot
									PRINTSTRING("bRightEngineShot = FALSE") PRINTNL()
									IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK)
										
										PRINTSTRING("Bullet being fired") PRINTNL()
										
										//Stats
										INFORM_MISSION_STATS_OF_INCREMENT(MAR1_ONE_SHOT) 
										
										//Make sure sniper is zoomed in a decent amount before allowing it to hit.
										IF GET_FINAL_RENDERED_CAM_FOV() <= 16
										AND GET_FINAL_RENDERED_CAM_FOV() > 5
//											IF fCrossScreenPositionX[1] > 0.45 AND fCrossScreenPositionX[1] < 0.55
//											AND fCrossScreenPositionY[1] > 0.45 AND fCrossScreenPositionY[1] < 0.55
											IF fCrossScreenPositionX[1] > 0.49 AND fCrossScreenPositionX[1] < 0.51
											AND fCrossScreenPositionY[1] > 0.49 AND fCrossScreenPositionY[1] < 0.51											
											AND NOT bRightEngineShot
												
												IF GET_GAME_TIMER() > (iShotTimer + 200)
													hasEffectStarted = FALSE
													bEffectTriggered = FALSE
													iShotTimer = GET_GAME_TIMER()
													iNumberOfHits ++
												ENDIF
												
											ELSE
												//Some dialogue if he's missed
												IF GET_GAME_TIMER() > (iMissChatTimer + 5000)
													KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
													ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
													ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
													IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_MISS", CONV_PRIORITY_MEDIUM)
														//Ah, shit.
														//Not good enough.
														//That didn't do it.
														//Gimme a second. 
														//It'll go down next time.
														//Gonna take another shot.
														//Nope. 
														iMissChatTimer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ENDIF
										ENDIF										
										
										IF GET_FINAL_RENDERED_CAM_FOV() <= 5
										AND GET_FINAL_RENDERED_CAM_FOV() > 2
//											IF fCrossScreenPositionX[1] > 0.45 AND fCrossScreenPositionX[1] < 0.55
//											AND fCrossScreenPositionY[1] > 0.45 AND fCrossScreenPositionY[1] < 0.55
											IF DOES_ENTITY_EXIST(vehTitan)
												IF IS_VEHICLE_DRIVEABLE(vehTitan)
													IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
														IF GET_TIME_POSITION_IN_RECORDING(vehTitan) < 18000
															IF fCrossScreenPositionX[1] > 0.485 AND fCrossScreenPositionX[1] < 0.515
															AND fCrossScreenPositionY[1] > 0.485 AND fCrossScreenPositionY[1] < 0.515											
															AND NOT bRightEngineShot
																
																IF GET_GAME_TIMER() > (iShotTimer + 200)
																	hasEffectStarted = FALSE
																	bEffectTriggered = FALSE
																	iShotTimer = GET_GAME_TIMER()
																	iNumberOfHits ++
																ENDIF
																
															ELSE
																//Some dialogue if he's missed
																IF GET_GAME_TIMER() > (iMissChatTimer + 5000)
																	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
																	ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
																	ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
																	IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_MISS", CONV_PRIORITY_MEDIUM)
																		//Ah, shit.
																		//Not good enough.
																		//That didn't do it.
																		//Gimme a second. 
																		//It'll go down next time.
																		//Gonna take another shot.
																		//Nope. 
																		iMissChatTimer = GET_GAME_TIMER()
																	ENDIF
																ENDIF
															ENDIF
														ELSE
															IF fCrossScreenPositionX[1] > 0.45 AND fCrossScreenPositionX[1] < 0.55
															AND fCrossScreenPositionY[1] > 0.45 AND fCrossScreenPositionY[1] < 0.55											
															AND NOT bRightEngineShot
																
																IF GET_GAME_TIMER() > (iShotTimer + 200)
																	hasEffectStarted = FALSE
																	bEffectTriggered = FALSE
																	iShotTimer = GET_GAME_TIMER()
																	iNumberOfHits ++
																ENDIF
																
															ELSE
																//Some dialogue if he's missed
																IF GET_GAME_TIMER() > (iMissChatTimer + 5000)
																	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
																	ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
																	ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
																	IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_MISS", CONV_PRIORITY_MEDIUM)
																		//Ah, shit.
																		//Not good enough.
																		//That didn't do it.
																		//Gimme a second. 
																		//It'll go down next time.
																		//Gonna take another shot.
																		//Nope. 
																		iMissChatTimer = GET_GAME_TIMER()
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										IF GET_FINAL_RENDERED_CAM_FOV() <= 2
										AND GET_FINAL_RENDERED_CAM_FOV() >= 1
//											IF fCrossScreenPositionX[1] > 0.45 AND fCrossScreenPositionX[1] < 0.55
//											AND fCrossScreenPositionY[1] > 0.45 AND fCrossScreenPositionY[1] < 0.55
											IF fCrossScreenPositionX[1] > 0.46 AND fCrossScreenPositionX[1] < 0.54
											AND fCrossScreenPositionY[1] > 0.46 AND fCrossScreenPositionY[1] < 0.54											
											AND NOT bRightEngineShot
												
												IF GET_GAME_TIMER() > (iShotTimer + 200)
													hasEffectStarted = FALSE
													bEffectTriggered = FALSE
													iShotTimer = GET_GAME_TIMER()
													iNumberOfHits ++
												ENDIF
												
											ELSE
												//Some dialogue if he's missed
												IF GET_GAME_TIMER() > (iMissChatTimer + 5000)
													KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
													ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, PLAYER_PED_ID(), "MICHAEL")
													ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, NULL, "TREVOR")
													IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_MISS", CONV_PRIORITY_MEDIUM)
														//Ah, shit.
														//Not good enough.
														//That didn't do it.
														//Gimme a second. 
														//It'll go down next time.
														//Gonna take another shot.
														//Nope. 
														iMissChatTimer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF		
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
									
					
				ENDIF
				
			BREAK
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════

		IF iStageSection >= 1
//				UPDATE_REMOTE_SNIPER(sSniperData)
			SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
			bStageSetup = FALSE
			
			SET_MAX_WANTED_LEVEL(5)
			RETURN TRUE
		ENDIF

	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:		Performs the cutscene which introduces the garage and the three characters
FUNC BOOL stageCutscenePlaneDescent()
	
	//Call this everyframe
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.1)	
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
	//Call every frame for these cutscene's so that the plane stays in HD
	IF DOES_ENTITY_EXIST(vehTitan)
		IF IS_VEHICLE_DRIVEABLE(vehTitan)
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehTitan) 
		ENDIF
	ENDIF	
	
	IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
		REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
	ENDIF	
	
	IF NOT IS_GAMEPLAY_CAM_RENDERING()
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2176962
	ENDIF
	
	//Switch player to Trevor during the cutscene	
	IF PlayerIsNowTrevor = FALSE				
		IF NOT DOES_ENTITY_EXIST(vehBike)
			REQUEST_MODEL(SANCHEZ)
			REQUEST_MODEL(PLAYER_TWO)
			IF HAS_MODEL_LOADED(SANCHEZ)
				vehBike = CREATE_VEHICLE(SANCHEZ,vBFChaseStartCoords, fBFChaseStartHeading)
				SET_VEHICLE_ENGINE_ON(vehBike, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(SANCHEZ)
			ENDIF					
		ELSE
			//Set up bike AND TREVOR
			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF HAS_MODEL_LOADED(PLAYER_TWO)
					IF CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vehBike)
						//Give TREVOR headset
//						SET_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//						GIVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//						IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
//							SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 0, 4)
//							SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 18, 0)
//							SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAND, 0, 0)
//							SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_FEET, 16, 0)
//							SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL, 0, 0)
//							SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL2, 0, 0)
//							SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_DECL, 0, 0)
//						ENDIF
						SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P1_HEADSET)	
						ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
						PRINTSTRING("Trevor created ") PRINTNL()
					ENDIF
				ENDIF
			ELSE
				IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						//Put Michael into the van
						IF DOES_ENTITY_EXIST(SniperVan)
						AND DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF IS_VEHICLE_DRIVEABLE(SniperVan)
								IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SniperVan)
									SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SniperVan)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), "Mood_Angry_1")
						ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
						ADD_PED_FOR_DIALOGUE(structConvoPeds, 0, NULL, "MICHAEL")
						PlayerIsNowTrevor = TRUE
						PRINTSTRING("PlayerIsNowTrevor = TRUE ") PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
//---------� SETUP �---------
	IF NOT bStageSetup
//		Set_Replay_Mid_Mission_Stage(ENUM_TO_INT(STAGE_CUTSCENE_PLANE_DESCENT))
		PRINTSTRING("MISSION STAGE CUTSCENE PLANE DESCENT") PRINTNL()
		
		missionCanFail = FALSE
		//Flags
		PlayerIsNowTrevor 			= FALSE	
		bPlaneShotSceneStreamStarted= FALSE		
		modelsRequested 			= FALSE
		iStageSection 				= 0
		
		//Clear help for cutscene 
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF			
		
		IF DOES_BLIP_EXIST(blipObj)
			REMOVE_BLIP(blipObj)
		ENDIF
		
		//Move Michael to help stream in areas
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-464.7, 1255.5, 317>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 111.0994)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTitan)
			CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)
		ENDIF
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		CLEAR_HELP()
		
		IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_SNIPER_CAMERA")
			STOP_AUDIO_SCENE("MARTIN_1_SNIPER_CAMERA")
		ENDIF			
		
		CLEAR_AREA(<<-401, 1211, 325>>, 70, TRUE)
		SET_ROADS_IN_AREA(vObservatoryRoadMin, vObservatoryRoadMax, FALSE)
		
		IF NOT IS_SCREEN_FADED_IN()
			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF		
		
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//[SP] just fixing compiling issues
		modelsrequested = modelsrequested
		planeshotsoundid = planeshotsoundid
		//[SP] just fixing compiling issues
		
		
		eSwitchCamState = SWITCH_CAM_START_SPLINE1
		
		bPlaneShotSceneStarted = FALSE
		bPlaneShotSceneFinished = FALSE
		
		bPlaneEngineExplosionStarted = FALSE
		bPlaneEnginePreSmokeStarted = FALSE
		bPlaneEnginePreSmokeStopped = FALSE
		bPlanePilotConvoStarted = FALSE
		bMichaelShotPlaneConvoStarted = FALSE
		bTrevorLookAtPlaneStarted = FALSE
		bChaseMusicStarted = FALSE
		bBikeRecordingStopped = FALSE
		
		bDGTitanCrashRecording2Started = FALSE

		/*bPlaneFlyByShakeStarted = FALSE
		bPlaneFlyByShakeStopped = FALSE*/
		
		PREPARE_MUSIC_EVENT("SOL1_FRANKLIN_STARTS")
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[1])
			STOP_PARTICLE_FX_LOOPED(ptfxEngine[1])
		ENDIF
		bTriggerFX[2] = FALSE
				
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
		
		STOP_STREAM()

		SETTIMERB(0)
		missionCanFail 				= TRUE
		bStageSetup 				= TRUE							
//---------� UPDATE �---------		
	ELSE
		
		SWITCH iStageSection
			CASE 0		
				
				DISABLE_ALL_CONTROLS_FOR_PLAYER_THIS_FRAME()
				
				IF NOT bPlaneShotSceneStreamStarted
					IF NOT bPlaneShotSceneStreamLoaded
						bPlaneShotSceneStreamLoaded = LOAD_STREAM("MARTIN_1_LEAR_JET_SHOT_DOWN_MASTER") 
					ENDIF
				ENDIF
				
				IF NOT bPlaneShotSceneStarted
				AND TIMERB() > iPlaneShotSceneDelay
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					CLEAR_PRINTS()
					
					IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_SNIPER_CAMERA")
						STOP_AUDIO_SCENE("MARTIN_1_SNIPER_CAMERA")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CHASE_SCENE")
						START_AUDIO_SCENE("MARTIN_1_PLANE_CHASE_SCENE")
					ENDIF	
					
					SET_REMOTE_SNIPER_ACTIVE(sSniperData, FALSE)
					CLEANUP_REMOTE_SNIPER(sSniperData)
					
					SETTIMERB(0)
					bPlaneShotSceneStarted = TRUE
					g_bDisableNoirLensEffect = FALSE
				ENDIF
				
				IF bPlaneShotSceneStarted
				AND NOT bPlaneShotSceneFinished
				
					IF NOT bPlaneShotSceneStreamStarted
						IF bPlaneShotSceneStreamLoaded
							CDEBUG3LN(DEBUG_MISSION, "Stream is loaded...")
							PLAY_STREAM_FRONTEND()
							bPlaneShotSceneStreamStarted = TRUE
						ELSE
							CDEBUG3LN(DEBUG_MISSION, "Stream is NOT loaded!!!")
						ENDIF
					ENDIF
				
					IF NOT bPlaneEngineExplosionStarted
					AND TIMERB() > iPlaneEngineExplosionDelay
						IF DOES_ENTITY_EXIST(vehTitan)
							IF NOT IS_ENTITY_DEAD(vehTitan)
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
									CDEBUG3LN(DEBUG_MISSION, "Starting Explosion...")
									
									// Play impact sound and trigger music event
									PLAY_SOUND_FROM_ENTITY(PlaneShotSoundID, "SOLOMON_1_JET_SHOT_EXPLODE", vehTitan)
									TRIGGER_MUSIC_EVENT("SOL1_ENGINE_HIT")	
									
									// Impact and Fire
									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sol1_sniper_impact", vehTitan, vEngineOffset1 + vPlaneEngineExplosionOffset, vPlaneEngineExplosionRot, fPlaneEngineExplosionScale)
								ENDIF
							ENDIF
						ENDIF
						bPlaneEngineExplosionStarted = TRUE
					ENDIF
					
					IF NOT bPlaneEnginePreSmokeStarted
					AND TIMERB() > iPlaneEnginePreSmokeOnDelay
						IF DOES_ENTITY_EXIST(vehTitan)
							IF NOT IS_ENTITY_DEAD(vehTitan)
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
								
									// Short burst of smoke
									IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[0])
										STOP_PARTICLE_FX_LOOPED(ptfxEngine[0])
									ENDIF
									ptfxEngine[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_smoke_loop", vehTitan, vEngineOffset1, VECTOR_ZERO)
								ENDIF
							ENDIF
						ENDIF
						bPlaneEnginePreSmokeStarted = TRUE
					ENDIF
					

					IF NOT bPlaneEnginePreSmokeStopped
					AND TIMERB() > iPlaneEnginePreSmokeOffDelay
	
						// Stop short burst of smoke
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[1])
							STOP_PARTICLE_FX_LOOPED(ptfxEngine[1])
						ENDIF
						bPlaneEnginePreSmokeStopped = TRUE
					ENDIF
					
					
					IF NOT bTriggerFX[2]
					AND TIMERB() > iPlaneEngineSmokeDelay
						IF DOES_ENTITY_EXIST(vehTitan)
							IF NOT IS_ENTITY_DEAD(vehTitan)
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
									CDEBUG3LN(DEBUG_MISSION, "Starting Smoke Trail...")
									
									IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[0])
										STOP_PARTICLE_FX_LOOPED(ptfxEngine[0])
									ENDIF
									ptfxEngine[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_smoke_loop", vehTitan, vEngineOffset1, VECTOR_ZERO)
									SET_PARTICLE_FX_LOOPED_SCALE(ptfxEngine[1], 2.0)
									bTriggerFX[2] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT bMichaelShotPlaneConvoStarted
					AND TIMERB() > iMichaelShotPlaneConvoDelay
						CDEBUG3LN(DEBUG_MISSION, "Starting Michael Shot Plane Convo...")
						KILL_ANY_CONVERSATION()
						CLEAR_PRINTS()
						CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_PLANEHIT", CONV_PRIORITY_MEDIUM)
						//Plane's coming down, are you on it?
						//You see it, plane's coming down, can you stay with it? 
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
						
						bMichaelShotPlaneConvoStarted = TRUE
					ENDIF
					
					
					IF NOT bPlanePilotConvoStarted
					AND TIMERB() > iPlanePilotConvoDelay
						CDEBUG3LN(DEBUG_MISSION, "Starting Plane Pilot Convo...")
						KILL_ANY_CONVERSATION()
						CLEAR_PRINTS()
						CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_HIT", CONV_PRIORITY_MEDIUM)
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
						
						//Engine's gone! 
						//Birdstrike!
						bPlanePilotConvoStarted = TRUE
					ENDIF
					
					/*IF NOT bTrevorTrackPlaneConvoStarted = FALSE
					AND TIMERB() > iTrevorTrackPlaneConvoDelay
						CDEBUG3LN(DEBUG_MISSION, "Starting Trevor Track Plane Convo...")
						KILL_ANY_CONVERSATION()
						CLEAR_PRINTS()
						CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_SHOT", CONV_PRIORITY_MEDIUM)
						//[SP]Assuming there will be dialogue from Trevor
						bTrevorTrackPlaneConvoStarted = TRUE
					ENDIF*/
					
					IF HANDLE_PLANE_SHOT_SWITCH_CAM(scsPlaneShotScene, scsPlaneToBike)
	
						SETTIMERA(0)
						
						PRINTSTRING("bPlaneShotSceneFinished = TRUE") PRINTNL()
						bPlaneShotSceneFinished = TRUE
					ELSE
						PRINTSTRING("bPlaneShotSceneFinished = FALSE") PRINTNL()
					ENDIF
					
				ENDIF
				
				IF bPlaneShotSceneFinished
					
					IF NOT bChaseMusicStarted
						IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_MAIN")
							CDEBUG3LN(DEBUG_MISSION, "Starting Chase Music...")
							START_AUDIO_SCENE("MARTIN_1_CHASE_MAIN")
						ENDIF
						bChaseMusicStarted = TRUE
					ENDIF
					
					
					IF NOT bBikeRecordingStopped
						IF IS_PLAYER_TRYING_TO_DRIVE()
						OR TIMERA() > iBikeRecordingStoppedDelay
							IF DOES_ENTITY_EXIST(vehBike)
								IF NOT IS_ENTITY_DEAD(vehBike)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehBike)
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_PLAYER_DEAD(PLAYER_ID())
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							ENDIF
							iStageSection++
							bBikeRecordingStopped = TRUE
						ENDIF
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE 1
				
				iStageSection++
			
			BREAK
		
		ENDSWITCH
		/*IF TIMERB() > 200		
			
			SWITCH iStageSection
				CASE 0
					//SETUP
					IF NOT bCameraShotSetup
						CLEAR_PRINTS()
						SET_REMOTE_SNIPER_ACTIVE(sSniperData,FALSE)
						
						CLEANUP_REMOTE_SNIPER(sSniperData)
						//Set the flag to stop drawing the sniper hud to the screen now.
						RemoteSniperNeedsUpdated = FALSE
						doStartCutscene()
						doTwoAttachLookAtEntityCam(	vehTitan,<< 5.0902, 3.5482, 2.0866 >>, vEngineOffset1, 30.9066,<< 4.9543, 3.6008, 1.4026 >>, vEngineOffset1, 30.9066, 2000)
						doCamShake("ROAD_VIBRATION_SHAKE", 0.1)
						SETTIMERA(0)
						bHasJolted = FALSE
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CHASE_SCENE")
							START_AUDIO_SCENE("MARTIN_1_PLANE_CHASE_SCENE")
						ENDIF						
						
						//Start audio stream now 
						IF bPlaneShotSceneStreamStarted = FALSE
							PLAY_STREAM_FRONTEND()
							bPlaneShotSceneStreamStarted = TRUE
						ENDIF
						
						bCameraShotSetup = TRUE
					ELSE
						IF bHasJolted = FALSE
							doCamShake("JOLT_SHAKE", 2)
							IF IS_VEHICLE_DRIVEABLE(vehTitan)
								PLAY_SOUND_FROM_ENTITY(PlaneShotSoundID, "SOLOMON_1_JET_SHOT_EXPLODE", vehTitan)
								TRIGGER_MUSIC_EVENT("SOL1_ENGINE_HIT")										
								START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(	"scr_sol1_sniper_impact", vehTitan, vEngineOffset1 + <<0,1.5,0>>, <<-90,0,0>>, 4)
							ENDIF
							bHasJolted = TRUE
						ENDIF
						IF TIMERA() > 1000
							IF bTriggerFX[2] = FALSE
								doCamShake("SKY_DIVING_SHAKE", 1.5)
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
									ptfxEngine[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY(	"scr_sol1_plane_smoke_loop", vehTitan,vEngineOffset1,VECTOR_ZERO)
									SET_PARTICLE_FX_LOOPED_SCALE(ptfxEngine[1], 2.0)
									bTriggerFX[2] = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF TIMERA() > 1500
							bCameraShotSetup = FALSE
							iStageSection++
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					IF NOT bCameraShotSetup
						doTwoAttachLookAtEntityCam(	vehTitan, << 1.5504, 14.5097, 0.6268 >>, <<0.0, 0.0, 0.0>>, 22.7820, << 2.0504, 14.5097, 0.6268 >>, <<0.0, 0.0, 0.0>>, 22.7820, 2500)
						doCamShake("SKY_DIVING_SHAKE", 1.5)
						SETTIMERA(0)
						bHasJolted = FALSE
						
						KILL_ANY_CONVERSATION()
						CLEAR_PRINTS()
						CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_HIT", CONV_PRIORITY_MEDIUM)
						//What the fuck was that?
						
						IF NOT IS_PED_INJURED(pedPilot)
							TASK_LOOK_AT_COORD(pedPilot, vEngineOffset1+<<0,15,0>>, 1000)
						ENDIF
						
						bCameraShotSetup = TRUE
					ELSE									
						IF TIMERA() > 2500
							bCameraShotSetup = FALSE
							iStageSection++
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF NOT bCameraShotSetup
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
							ENDIF
							
							REMOVE_VEHICLE_RECORDING(294, "DGTitanEntry")
							START_PLAYBACK_RECORDED_VEHICLE(vehTitan, iRecNo, "DGTitanCrash")
							SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, 8405)
							SET_PLAYBACK_SPEED(vehTitan,0.6)
						ENDIF	
//			
//						//Add particle effects of plane engine on fire for the chase
//						IF bTriggerFX[2] = FALSE
//							START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_smoke_loop",vehTitan,vEngineOffset1,VECTOR_ZERO)
//							bTriggerFX[2] = TRUE
//						ENDIF						
						
						doTwoPointCam(	<< -167.9992, -85.6194, 286.6718 >>, << -11.7987, 13.3209, -179.2416 >>, 45.0000, << -179.0338, 45.5796, 260.4149 >>, << -9.2809, 11.0089, -172.1679 >>, 21.0171,3900)
						doCamShake("SKY_DIVING_SHAKE", 1.5)
									
						KILL_ANY_CONVERSATION()
						CLEAR_PRINTS()
						CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_SHOT", CONV_PRIORITY_MEDIUM)
							//I�m on this motherfucker!
							//I got this!
										
						SETTIMERA(0)					
						bCameraShotSetup = TRUE
						
					//UPDATE
					ELSE
						
					//CLEANUP
						IF TIMERA() > 3900
							iStageSection++
							bCameraShotSetup = FALSE
						ENDIF
					ENDIF
				BREAK
				
				CASE 3
					//SETUP
					IF NOT bCameraShotSetup	
						
						SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, 27552)
						
						doTwoPointCam(	<< -453.9597, 1071.6810, 328.7998 >>, << -2.6154, 0.1738, -174.9520 >>, 30.1916, << -453.7553, 1071.3461, 327.1249 >>, << 26.1871, 0.3835, -179.0974 >>, 35.4347, 4000, GRAPH_TYPE_SIN_ACCEL_DECEL)
						doCamShake("ROAD_VIBRATION_SHAKE", 0.1)
						SETTIMERA(0)
						
						bCameraShotSetup = TRUE
					
					//UPDATE
					ELSE
						
						//Request models for next stage
						IF modelsRequested = FALSE
							PREPARE_MUSIC_EVENT("SOL1_FRANKLIN_STARTS")
							modelsRequested	= TRUE
						ENDIF
						
						IF TIMERA() > 4000
							iStageSection++
							bCameraShotSetup = FALSE
						ENDIF
					ENDIF
				BREAK
				CASE 4
					//SETUP
					IF NOT bCameraShotSetup	
					
						SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, 30516)
						SET_PLAYBACK_SPEED(vehTitan, 0.3)
						
						doTwoPointCam(	<< -447.6947, 1073.7161, 382.1912 >>, << -20.4731, 0.0955, 5.8696 >>, 28.8050, << -454.5002, 1128.1649, 392.7070 >>, << -37.9954, 10.6115, -31.3539 >>, 28.8050, 3047)
						doCamShake("SKY_DIVING_SHAKE", 1.5)
						SETTIMERA(0)
						
						IF IS_VEHICLE_DRIVEABLE(vehBike)
							START_PLAYBACK_RECORDED_VEHICLE(vehBike, iRecNo, "DGBFReady")
							SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike, 11829)
						ENDIF
						
						TRIGGER_MUSIC_EVENT("SOL1_FRANKLIN_STARTS")
						
						bCameraShotSetup = TRUE
					
					ELSE					
						IF TIMERA() > 3000
							iStageSection++
							bCameraShotSetup = FALSE
						ENDIF
					ENDIF
				BREAK
				CASE 5
					//SETUP
					IF NOT bCameraShotSetup	
						DESTROY_ALL_CAMS()
						
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
							ENDIF
							REMOVE_VEHICLE_RECORDING(iRecNo, "DGTitanCrash")
							START_PLAYBACK_RECORDED_VEHICLE(vehTitan, 1, "BB_SOL_JET")
							SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, 13662)
						ENDIF					
						
						camInit = CREATE_CAMERA(CAMTYPE_SPLINE_DEFAULT, FALSE)
						SET_CAM_SPLINE_SMOOTHING_STYLE(camInit, CAM_SPLINE_SLOW_OUT_SMOOTH)
						ADD_CAM_SPLINE_NODE_USING_CAMERA(camInit, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-408.776764,1181.976563,325.094482>>,<<8.912352,-0.000001,-17.225756>>,40.463402, TRUE), 0, CAM_SPLINE_NODE_SMOOTH_ROT)
						ADD_CAM_SPLINE_NODE_USING_CAMERA(camInit, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-402.191925,1186.963501,325.682770>>,<<-0.094461,-0.000001,-17.225756>>,43.463402, TRUE), 1700, CAM_SPLINE_NODE_SMOOTH_ROT)
						ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(camInit, 800, CAM_SPLINE_NODE_SMOOTH_ROT)
						SHAKE_CAM(camInit, "HAND_SHAKE", 0.2)
						
						camDest = CREATE_CAMERA(CAMTYPE_SPLINE_DEFAULT, FALSE)
						SET_CAM_SPLINE_SMOOTHING_STYLE(camInit, CAM_SPLINE_SLOW_OUT_SMOOTH)
						ADD_CAM_SPLINE_NODE(camDest, <<-392.684845,1233.878906,333.615997>>, <<0.0, 0.0, 0.0>>, 0)
						ADD_CAM_SPLINE_NODE(camDest, <<-388.822266,1229.930420,326.045013>>, <<0.0, 0.0, 0.0>>, 2000)
						
						POINT_CAM_AT_CAM(camInit, camDest)
						SET_CAM_ACTIVE(camDest, TRUE)
						SET_CAM_ACTIVE(camInit, TRUE)
					
						SETTIMERA(0)
						bCameraShotSetup = TRUE				
					ELSE		
						IF TIMERA() > 2500
							iStageSection++
							bCameraShotSetup = FALSE
						ENDIF
					ENDIF
				BREAK

				CASE 6
					//SETUP
					IF NOT bCameraShotSetup	
						DESTROY_ALL_CAMS()
						
						IF bPlaneShotSceneStreamStarted = TRUE
							STOP_STREAM()
							bPlaneShotSceneStreamStarted = FALSE
						ENDIF
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

						bCameraShotSetup = TRUE
					ELSE					
						IF TIMERA() > 700
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBike)
							doEndCutscene(TRUE, FALSE)
							iStageSection++
							bCameraShotSetup = FALSE
						ENDIF
					ENDIF
				BREAK

			ENDSWITCH
			
		ELSE
			//Keep the sniper on screen until the camera becomes active
			UPDATE_REMOTE_SNIPER(sSniperData)
		ENDIF*/
		
//		//Check for cutscene being skipped
//		IF TIMERB() > 1500
//			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
//				stopCamShake(TRUE)
//
//				bStageSetup = FALSE
//				bCameraShotSetup = FALSE
//				iStageSection = 7
//				
//				eMissionStage = STAGE_CHASE_PLANE
//				
//				manageSkip()
//			ENDIF
//		ENDIF
		
//---------� CLEANUP �---------
		IF iStageSection >= 2		
			
			stopCamShake(TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)

			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Deals with the chase hint cam.
PROC ChaseHintCam()
	
	//Have audio scene switched on when hint cam is active
	IF IS_GAMEPLAY_HINT_ACTIVE()
		IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_FOCUS_CAM")
			START_AUDIO_SCENE("MARTIN_1_FOCUS_CAM")
		ENDIF	
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_FOCUS_CAM")
			STOP_AUDIO_SCENE("MARTIN_1_FOCUS_CAM")
		ENDIF			
	ENDIF

	IF cinematicCamDeactivated = TRUE	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()//url:bugstar:1997821
			IF fRecordingTime < 117498
				IF IS_VEHICLE_DRIVEABLE(vehTitan)
					CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehTitan)
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(endGuyBlip[0])
				AND NOT DOES_BLIP_EXIST(endGuyBlip[1])
					IF IS_VEHICLE_DRIVEABLE(vehTitan)
						CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehTitan)
					ENDIF
				ELSE
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				ENDIF
			ENDIF
		ELSE
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()//url:bugstar:1997821
		IF bHasUsedHintCam = FALSE
			IF cinematicCamDeactivated = TRUE
				IF DoneHelp[0] = FALSE
					PRINT_HELP("BFHELPTXT2")//Press ~INPUT_VEH_CIN_CAM~ to toggle focus onto the plane.
					SETTIMERB(0)
					DoneHelp[0] = TRUE
				ELSE
					IF DoneHelp[1] = FALSE
						IF TIMERB() > 15000
							PRINT_HELP("BFHELPTXT2")//Press ~INPUT_VEH_CIN_CAM~ to toggle focus onto the plane.
							SETTIMERB(0)
							DoneHelp[1] = TRUE
						ENDIF
					ELSE
						IF DoneHelp[2] = FALSE
							IF TIMERB() > 10000
								PRINT_HELP("BFHELPTXT2")//Press ~INPUT_VEH_CIN_CAM~ to toggle focus onto the plane.
								SETTIMERB(0)
								DoneHelp[2] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_GAMEPLAY_HINT_ACTIVE()
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP(TRUE)
				ENDIF
				bHasUsedHintCam = TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BFHELPTXT2")
			CLEAR_HELP()
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE:		Performs the section where TREVOR tortures the victim for the first time
FUNC BOOL stageChasePlane()
		
//	IF IS_VEHICLE_DRIVEABLE(vehBike)	
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike)
//			PRINTSTRING("Bike's playback position is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehBike))PRINTNL()
//		ENDIF
//	ENDIF
//	FLOAT fOptimalPlaneDistance
//	FLOAT fMinPlaneSpeed
//	FLOAT fMaxPlaneSpeed
	
//	//Remove this once finished making crash scene
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
			missionCanFail = FALSE
			IF DOES_ENTITY_EXIST(vehTitan)
				IF IS_VEHICLE_DRIVEABLE(vehTitan)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
						SKIP_TO_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTitan, 107591)
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(vehBike)
				IF IS_VEHICLE_DRIVEABLE(vehBike)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike)
					ENDIF
				ENDIF
			ENDIF
			SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<2673.5642, 4346.6299, 44.5755>>)
			SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 311.4434)
			LOAD_SCENE(<<2673.5642, 4346.6299, 44.5755>>)
			missionCanFail = TRUE
		ENDIF
	#ENDIF
	
	IF bStageSetup = TRUE
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(vehBike)
				IF IS_VEHICLE_DRIVEABLE(vehBike)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehBike) < 100
						IF NOT DOES_BLIP_EXIST(bikeBlip)
							bikeBlip = CREATE_BLIP_FOR_VEHICLE(vehBike)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(bikeBlip)
							REMOVE_BLIP(bikeBlip)
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(bikeBlip)
						REMOVE_BLIP(bikeBlip)
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(bikeBlip)
					REMOVE_BLIP(bikeBlip)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(bikeBlip)
				REMOVE_BLIP(bikeBlip)
			ENDIF
		ENDIF
	ENDIF
	
	//Handle requesting and unrequesting the mocap scene if player gets close to cutscene trigger point.
	IF CutsceneRequested = FALSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2803.8, 4798.1, 47.7>>) < DEFAULT_CUTSCENE_LOAD_DIST
			REQUEST_CUTSCENE("Martin_1_mcs_1")
			CutsceneRequested = TRUE
		ENDIF
	ENDIF
	IF CutsceneRequested = TRUE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2803.8, 4798.1, 47.7>>) > DEFAULT_CUTSCENE_UNLOAD_DIST
			REMOVE_CUTSCENE()
			CutsceneRequested = FALSE
		ENDIF		
		//Set up Trev's clothes for opening cutscene
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			//Trevor
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
			PRINTSTRING("TRIGGER SCENE - Trev's cutscene components set 1.") PRINTNL()
		ENDIF
	ENDIF
	
	//Handles all the set pieces through out the chase. And cleans them up.
	IF bStageSetup = TRUE
		SET_PIECES()
	ENDIF
	
	IF hasIsStreamPlaying = FALSE
		IF IS_VEHICLE_DRIVEABLE(vehTitan)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
				IF LOAD_STREAM("MARTIN_1_DAMAGED_PLANE_MASTER")
					PLAY_STREAM_FROM_VEHICLE(vehTitan)
					PRINTSTRING("Starting the stream MARTIN_1_DAMAGED_PLANE_MASTER")
					hasIsStreamPlaying = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Call this every frame to keep ped density down until after the jump over bus section.
	IF DOES_ENTITY_EXIST(vehTitan)
		IF IS_VEHICLE_DRIVEABLE(vehTitan)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
				IF GET_TIME_POSITION_IN_RECORDING(vehTitan) < 30000
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.1)	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Stats
	IF PlayerOnBike = FALSE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			IF IS_VEHICLE_DRIVEABLE(vehBike)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
					PlayerOnBike = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF PlayerOnBike = TRUE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			IF IS_VEHICLE_DRIVEABLE(vehBike)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
					INFORM_MISSION_STATS_OF_INCREMENT(MAR1_FELL_OFF_THE_BIKE)
					PlayerOnBike = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Delete Michael and sniper Van once player is far enough away.
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) > 150
		AND NOT IS_ENTITY_ON_SCREEN(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SniperVan)
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), SniperVan) > 150
		AND NOT IS_ENTITY_ON_SCREEN(SniperVan)
			IF DOES_ENTITY_EXIST(RemoteSniper)
				DELETE_OBJECT(RemoteSniper)
			ENDIF
			DELETE_VEHICLE(SniperVan)
		ENDIF
	ENDIF
	
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_CHASE_PLANE")
		
		missionCanFail = FALSE
		PRINTSTRING("MISSION STAGE CHASE PLANE") PRINTNL()
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID()) 
		
		//Create a scenario blocking area to prevent the planes from spawning during the chase.
		AirstripBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<957.9, 3051.7, 76.7>>, <<1898.5, 3195.2, 32.3>>)
		PoliceBikeBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<2782.5, 4496.7, 50.8>>, <<2787.3, 4519.3, 42.5>>)
		
//		SET_FRONTEND_RADIO_ACTIVE(TRUE)
		
		ADD_ALL_PEDS_TO_DIALOGUE()
			
		fPlaybackSpeed = 0.4
		IF DOES_ENTITY_EXIST(vehTitan)
			IF IS_VEHICLE_DRIVEABLE(vehTitan)
//				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTitan, "SOL_1_CHASE_JET_SHAMAL")
				PlaneonfireSoundID = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(PlaneonfireSoundID, "PLANE_ON_FIRE", vehTitan)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
					SET_PLAYBACK_SPEED(vehTitan, fPlaybackSpeed)
				ENDIF
			ENDIF
		ENDIF
		
//		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,100)
		
		/*IF bTriggerFX[1] = FALSE
			START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_smoke_loop",vehTitan,vEngineOffset0,VECTOR_ZERO)
			bTriggerFX[1] = TRUE
		ENDIF*/
											
		//Flags
//		//Debug Flags
//		#IF IS_DEBUG_BUILD
//			doneGetBackOnBikeHelpText 	= FALSE
//		#ENDIF
		
		bDoingJumpCutscene 			= FALSE
		bDoneJumpCutscene 			= FALSE
		bDoneSlowMoJump				= FALSE
		btimeScaleBackToNormal		= FALSE
		bHasUsedHintCam		 		= FALSE
		PlayerCanDoJump				= FALSE
		doneGetBackOnBikeText 		= FALSE
//		playerfellOffbike 			= FALSE
		doneChaseAfterPlaneText 	= FALSE
		playerDoneTrainJump			= FALSE
		broughtlandingGearOut 		= FALSE
		Bikeplaybackstopped 		= FALSE
		PlaneOnfireSoundStopped	 	= FALSE
		doneChatS1_Jump3 			= FALSE
		DumpModelRequested 			= FALSE
		DumpSetPieceCreated 		= FALSE
		DumpPlaybackStarted 		= FALSE
		DumpSetpieceSwitchedToAI 	= FALSE
		FellOffBikeStatDone 		= FALSE
//		GodTextCleared 				= FALSE
		cinematicCamDeactivated 	= FALSE
		preparedInteractiveMusic2 	= FALSE
		IsPedPointingPlayingAnim 	= FALSE
		IsPedPointingSetup			= FALSE
		IsPedPointingSetup2		 	= FALSE
		IsPedPointingPlayingAnim2 	= FALSE
		IsPedPointingSetup3		 	= FALSE
		IsPedPointingPlayingAnim3 	= FALSE
		IsParkedCarSetup 			= FALSE
		startInteractiveMusic2 		= FALSE
		PlayerOnBike 				= FALSE
		trainModelsRequested 		= FALSE
		busRequested 				= FALSE
		HaulerRequested 			= FALSE
		PatriotRequested 			= FALSE
		BallerRequested 			= FALSE
		DilettanteRequested 		= FALSE
		TornadoRequested 			= FALSE
		Bus2Requested 				= FALSE
		S1_CLIFF_CHAT_DONE 			= FALSE
		S1_FMC4_CHAT_DONE 			= FALSE
		S1_FALL5_CHAT_DONE			= FALSE
		S1_JUMP2_CHAT_DONE 			= FALSE
		S1_FALL4_CHAT_DONE			= FALSE
		S1_FMC3_CHAT_DONE 			= FALSE
		S1_FALL3_CHAT_DONE			= FALSE
		S1_FALL2_CHAT_DONE 			= FALSE
		S1_FMC2_CHAT_DONE			= FALSE
		S1_FALL1_CHAT_DONE 			= FALSE
		S1_JUMP1_CHAT_DONE			= FALSE
		S1_OROAD_CHAT_DONE 			= FALSE
		S1_FMC1_CHAT_DONE			= FALSE
//		planeFrozen 				= FALSE
		IsPedPointingPlayingAnim4	= FALSE
		IsPedPointingSetup4			= FALSE
		requestedCrashStream		= FALSE
		crashStreamStarted			= FALSE
		DoneHorn1					= FALSE
		DoneHorn2					= FALSE
		DoneHorn3					= FALSE
		IPLSwapped 					= FALSE
		CutsceneRequested 			= FALSE
		camsCleanedUp 				= FALSE
		doneKillPedGodText 			= FALSE
		doneGameCamShake 			= FALSE
		CombatTaskGiven 			= FALSE
		CombatTaskRequired 			= FALSE
		safetyTimeScaleBackToNormalSet = FALSE
		doneS1_JUMP4 				= FALSE	
		S1_FMC5_CHAT_DONE	 		= FALSE
		S1_TRAILER_CHAT_DONE 		= FALSE
		
		FOR iCounter = 0 TO 19
			bSetPiece[iCounter] 	= FALSE
		ENDFOR
		FOR iCounter = 0 TO 5
			doneHeadLook[iCounter]	= FALSE
		ENDFOR
		FOR iCounter = 0 TO 2
			DoneHelp[iCounter] 		= FALSE
		ENDFOR
		//SET_BLIP_ROUTE(blipObj, TRUE)
			
		SETTIMERB(0)
		
		SET_ROADS_IN_AREA(vSportsCarRoadMin, vSportsCarRoadMax, FALSE)
		SET_ROADS_IN_AREA(<<-9.5, 1758.3, 235.3>>, <<134.5, 1638.3, 222.5>>, FALSE)
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(SANCHEZ, TRUE)
			
		SET_STUNT_JUMPS_CAN_TRIGGER(FALSE)
		
		INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehBike)
		
		//Start the audio scene for chasing the plane
//		START_AUDIO_SCENE("SOL_1_CHASE_JET_SCENE")	
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_MAIN")
			START_AUDIO_SCENE("MARTIN_1_CHASE_MAIN")
		ENDIF			
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
		
		DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
		
		CropFieldBlockingArea1 = ADD_SCENARIO_BLOCKING_AREA(<<2756.5, 4523.2, 72>>, <<2916.2, 4821.8, 36.9>>)
		PlaneBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<2813.7, 4807.9, 51.5>>, <<2803.1, 4795.8, 44.6>>)
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(vehBike)
			
			IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				SET_VEHICLE_FORWARD_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 20)
			ENDIF
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF		
//		fOptimalPlaneDistance = 30
//		fMinPlaneSpeed = 0.7
//		fMaxPlaneSpeed = 1.3			
			
//		bUseRubberBand = TRUE
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[0])
			STOP_PARTICLE_FX_LOOPED(ptfxEngine[0])
		ENDIF
		
		IF bTriggerFX[2] = FALSE
			IF IS_VEHICLE_DRIVEABLE(vehTitan)
				ptfxEngine[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_smoke_loop",vehTitan,vEngineOffset1,VECTOR_ZERO)
				SET_PARTICLE_FX_LOOPED_SCALE(ptfxEngine[1], 2.0)
				bTriggerFX[2] = TRUE
			ENDIF
		ENDIF		
		
		IF NOT DOES_CAM_EXIST(camInit)
			camInit = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<20.821671,1743.010620,221.849548>>,<<11.511129,-0.000000,165.596802>>,34.700695)
		ENDIF
		IF NOT DOES_CAM_EXIST(camDest)
			camDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<28.231022,1742.381470,221.297195>>,<<27.238729,0.000000,144.930542>>,34.700695)
		ENDIF	
		IF NOT DOES_CAM_EXIST(camStatic)
			camStatic = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<26.255728,1737.435059,225.466125>>,<<-14.318232,2.157022,-28.279879>>,50.028435)
		ENDIF		
		
		//Disable all the police for the chase section
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_HELICOPTER, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_VEHICLE_REQUEST, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_ROAD_BLOCK, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, FALSE)
	    ALLOW_DISPATCH(DT_SWAT_AUTOMOBILE, FALSE)
	    ALLOW_DISPATCH(DT_SWAT_HELICOPTER, FALSE)		
		
		SET_MAX_WANTED_LEVEL(0)
		
		CLEAR_PRINTS()
		PRINT_NOW("BFCHASE", DEFAULT_GOD_TEXT_TIME, -1)
		//Follow the plane and get the package.	
		
		iMiniCutStage 			= 0
		iStageSection 			= 0
		fTimeScale				= 1
		missionCanFail 			= TRUE
		bStageSetup 			= TRUE
		
		
//═════════╡ UPDATE ╞═════════		
	ELSE			
		//PRINT_WITH_NUMBER_NOW("~1~", ROUND(fRecordingTime), 1000, 1)
		
		IF DOES_ENTITY_EXIST(vehTitan)
			IF IS_VEHICLE_DRIVEABLE(vehTitan)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
					fRecordingTime = GET_TIME_POSITION_IN_RECORDING(vehTitan)
					PRINTSTRING("Plane's playback position is ") PRINTFLOAT(fRecordingTime)PRINTNL()
				ENDIF
			ENDIF
		ENDIF
			
//		PRINTSTRING("Players heading is ") PRINTFLOAT(GET_ENTITY_HEADING(PLAYER_PED_ID()))PRINTNL()				
//		PRINTSTRING("Players Speed is ") PRINTFLOAT(GET_ENTITY_SPEED(PLAYER_PED_ID()))PRINTNL()						
		SWITCH iStageSection
			CASE 0
				IF bDoingJumpCutscene
					
//					//Allow the player to have control over the speed of the camera
//					IF iMiniCutStage < 3
//					INT i_left_stick_x, i_left_stick_y, i_right_stick_x, i_right_stick_y
//						GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(i_left_stick_x, i_left_stick_y, i_right_stick_x, i_right_stick_y)
//						
//						SET_TIME_SCALE(fTimeScale)
//						
//						//Apply a deadzone to the stick if it's just touched.
//						IF i_right_stick_y > 0
//						AND i_right_stick_y < 10
//							i_right_stick_y = 0
//						ENDIF
//						IF i_right_stick_y < 0
//						AND i_right_stick_y > -10
//							i_right_stick_y = 0
//						ENDIF
//						
//						//Speed up the time scale if pushed up
//						IF i_right_stick_y < -10					
//							
//							fTimeScale = fTimeScale * 1.10
//							
//							IF fTimeScale >= 1
//								fTimeScale = 1
//							ENDIF
//						ENDIF
//						
//						//Slow down the time scale if pulled down
//						IF i_right_stick_y > 10					
//						
//							fTimeScale = fTimeScale / 1.15
//							
//							IF fTimeScale <= 0.15
//								fTimeScale = 0.15
//							ENDIF
//						ENDIF
//					
//					ENDIF
					
					IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 25
					OR GET_ENTITY_HEADING(PLAYER_PED_ID()) < 293
					OR GET_ENTITY_HEADING(PLAYER_PED_ID()) > 354
						SET_TIME_SCALE(1)
						DISPLAY_RADAR(TRUE)
						DISPLAY_HUD(TRUE)
						//Stop the audio scene
						IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_01")
							STOP_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_01")
						ENDIF						
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						SET_WIDESCREEN_BORDERS(FALSE, 500)						
						
						bDoingJumpCutscene = FALSE
						bDoneJumpCutscene = TRUE
						iMiniCutStage = 99
					ENDIF
					
//					PRINTSTRING("Offset from Bike is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehBike, GET_CAM_COORD(GET_DEBUG_CAM()))) PRINTNL()
					PRINTSTRING("Timer A is ") PRINTINT(TIMERA()) PRINTNL()
					
					
					SWITCH iMiniCutStage						
						
						CASE 0
//								KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								
//								DISPLAY_RADAR(FALSE)
//								DISPLAY_HUD(FALSE)
								
//								IF NOT DOES_CAM_EXIST(camInit)
//									camInit = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<20.821671,1743.010620,221.849548>>,<<11.511129,-0.000000,165.596802>>,34.700695, TRUE)
//								ENDIF
//								IF NOT DOES_CAM_EXIST(camDest)
//									camDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<27.699244,1742.676270,221.899628>>,<<22.520906,-0.253692,153.718536>>,34.700695, TRUE)
//								ENDIF			
//								SET_CAM_ACTIVE(camInit, TRUE)
//								SET_CAM_ACTIVE(camDest, TRUE)
//								SET_CAM_ACTIVE(camStatic, FALSE)
//								
//								POINT_CAM_AT_ENTITY(camDest, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<0, 3, 0>>)
//								
//								CLEAR_HELP()
//								
//								SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 7000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								//Start the audio scene for the jump
								IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_01")
									START_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_01")
								ENDIF
							
								SETTIMERA(0)
								
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
								
								TRIGGER_MUSIC_EVENT("SOL1_BUS_JUMP")
								
								iMiniCutStage++
								
						BREAK
						
						CASE 1
							
							IF TIMERA() > 500
							OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
								fTimeScale = fTimeScale / 1.15
								
								SET_TIME_SCALE(fTimeScale)
								
								IF fTimeScale <= 0.25
									fTimeScale = 0.25
									SET_TIME_SCALE(0.25)
									iMiniCutStage++
								ENDIF
							ENDIF							
							
//							IF TIMERA() > 1000					
////								ATTACH_CAM_TO_ENTITY(camStatic, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), << -0.532271, -1.46621, -0.0967791 >>)
////								POINT_CAM_AT_ENTITY(camStatic, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<0, 3, 0>>)
////								SET_CAM_ACTIVE(camStatic, TRUE)
//								iMiniCutStage++
//							ENDIF
						
						BREAK
						
						CASE 2
						
							IF doneChatS1_Jump3 = FALSE
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								CLEAR_PRINTS()
								IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_JUMP3", CONV_PRIORITY_MEDIUM)
									//Whoa!
									doneChatS1_Jump3 = TRUE
								ENDIF
							ENDIF						
						
//							RENDER_SCRIPT_CAMS(FALSE, TRUE, 1500, TRUE)
//							DISPLAY_RADAR(TRUE)
//							DISPLAY_HUD(TRUE)						
//							SET_WIDESCREEN_BORDERS(FALSE, 500)	
							iMiniCutStage++
							
						BREAK
						
						CASE 3
						
							IF TIMERA() > 1000
							OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
								fTimeScale = fTimeScale*1.15
								
								SET_TIME_SCALE(fTimeScale)
								
								IF fTimeScale >= 1
									//Stop the audio scene
									IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_01")
										STOP_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_01")
									ENDIF							
								
									SET_TIME_SCALE(1)					
									iMiniCutStage = 0
									bDoingJumpCutscene = FALSE
									bDoneJumpCutscene = TRUE
									iMiniCutStage++	
								ENDIF
							ENDIF
							
						BREAK
						
					ENDSWITCH
				ELSE
					
//					//DEBUG
//					#IF IS_DEBUG_BUILD
//						RESTORE_PLAYER_POSITION()
//					#ENDIF
					
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						IF doneGetBackOnBikeText = TRUE
						AND GET_GAME_TIMER() > iGetOnBikeTextTimer + 7000
							DIALOGUE()
						ENDIF
						IF doneGetBackOnBikeText = FALSE
							DIALOGUE()
						ENDIF
					ENDIF
					//RECORDING_TOOL()
					
					HANDLE_PLAYER_LOOKING_AT_PLANE()
//					DUMP_CARGO()
					
					IF Bikeplaybackstopped = FALSE
						IF IS_VEHICLE_DRIVEABLE(vehBike)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike)
								IF GET_TIME_POSITION_IN_RECORDING(vehBike) > 21000
									STOP_PLAYBACK_RECORDED_VEHICLE(vehBike)
									REMOVE_VEHICLE_RECORDING(iRecNo, "DGBFReady")
									Bikeplaybackstopped = TRUE
								ENDIF
							ELSE
								REMOVE_VEHICLE_RECORDING(iRecNo, "DGBFReady")
								Bikeplaybackstopped = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					//deactivate the cinematic cam once plane is ahead of player and activate hint cam
					IF cinematicCamDeactivated = FALSE
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
								IF GET_TIME_POSITION_IN_RECORDING(vehTitan) > 18952
									SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
									cinematicCamDeactivated = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF FellOffBikeStatDone = FALSE
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(vehBike)
								IF IS_VEHICLE_DRIVEABLE(vehBike)
									IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike)
										//INFORM_STAT_CAR_SOLOMON_ONE_FELL_OFF_BIKE()
										FellOffBikeStatDone = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF doneChaseAfterPlaneText = FALSE
						PRINT_NOW("BFCHASE", DEFAULT_GOD_TEXT_TIME, 1)//Follow the plane and get the package.
						doneChaseAfterPlaneText = TRUE
					ENDIF
											
					IF NOT IS_PED_INJURED(pedPilot)
						ChaseHintCam()
					ENDIF	
						
					IF bDoneJumpCutscene = FALSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								IF IS_ENTITY_AT_COORD(vehBike, <<13.13, 1718.86, 227>>,<<7.03, 5.52, 3>>)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-5.680077,1710.670776,230.738052>>, <<10.925901,1699.666138,224.228409>>, 5.250000)
//								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<6.388503,1728.948853,225.126419>>, <<22.800776,1718.545898,231.155518>>, 11.500000)
								IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 25
								AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > 293
								AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 354
									fTimeScale = 0.3
									iMiniCutStage = 0
									bDoingJumpCutscene = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF bDoneJumpCutscene = TRUE
						IF camsCleanedUp = FALSE
							IF IS_GAMEPLAY_CAM_RENDERING()
								IF DOES_CAM_EXIST(camInit)
									DESTROY_CAM(camInit)
								ENDIF
								IF DOES_CAM_EXIST(camDest)
									DESTROY_CAM(camDest)
								ENDIF
								IF DOES_CAM_EXIST(camStatic)
									DESTROY_CAM(camStatic)
								ENDIF
								SET_TIME_SCALE(1)	
								camsCleanedUp = TRUE
							ENDIF
						ENDIF
						IF doneGameCamShake = FALSE
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									SET_ROADS_IN_AREA(<<-9.5, 1758.3, 235.3>>, <<134.5, 1638.3, 222.5>>, TRUE)
									SHAKE_GAMEPLAY_CAM("small_explosion_shake", 0.2)
									
									//Add in a video record moment here for the jump.
									REPLAY_RECORD_BACK_FOR_TIME(6.0)
									doneGameCamShake = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTitan)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
							IF GET_TIME_POSITION_IN_RECORDING(vehTitan) < 105199
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehTitan, PLAYER_PED_ID(), fPlaybackSpeed, 0.45, 90, 107, 125, 2, 0, 0.95)
								SET_PLAYBACK_SPEED(vehTitan, fPlaybackSpeed)
							ELSE
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehTitan, PLAYER_PED_ID(), fPlaybackSpeed, 0.55, 95, 105, 125, 2, 0, 1.3)
								SET_PLAYBACK_SPEED(vehTitan, fPlaybackSpeed)
								IF broughtlandingGearOut = FALSE
//										CONTROL_LANDING_GEAR(vehTitan, LGC_DEPLOY)
									broughtlandingGearOut = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
//						PRINTSTRING("Players speed is ") PRINTFLOAT(GET_ENTITY_SPEED(vehBike)) PRINTNL()
					
					IF playerDoneTrainJump = FALSE
						
						//Do some audio of train horns before player does the jump
						IF DOES_ENTITY_EXIST(vehTrain)
							IF fRecordingTime > 72000
							AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTrain) < 200
								
								IF DoneHorn1 = FALSE
									PLAY_SOUND_FRONTEND(-1, "MARTIN1_DISTANT_TRAIN_HORNS_MASTER")
									iHornTimer = GET_GAME_TIMER()
									DoneHorn1 = TRUE
								ELSE
									IF DoneHorn2 = FALSE
										IF GET_GAME_TIMER() > (iHornTimer + 3000)
											PLAY_SOUND_FRONTEND(-1, "MARTIN1_DISTANT_TRAIN_HORNS_MASTER")
											iHornTimer = GET_GAME_TIMER()
											DoneHorn2 = TRUE
										ENDIF
									ELSE
										IF DoneHorn3 = FALSE
											IF GET_GAME_TIMER() > (iHornTimer + 3000)
												PLAY_SOUND_FRONTEND(-1, "MARTIN1_DISTANT_TRAIN_HORNS_MASTER")
												DoneHorn3 = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					
						//Check if the player is going to make the jump over the train track
						IF PlayerCanDoJump = FALSE
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1529, 3349, 37>>, <<6,3,4>>)
										IF GET_ENTITY_HEADING(PLAYER_PED_ID()) < 65
										OR GET_ENTITY_HEADING(PLAYER_PED_ID()) > 270
											IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 15
												PlayerCanDoJump = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
					
						IF PlayerCanDoJump = TRUE
							IF bDoneSlowMoJump = FALSE
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										IF IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
											SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", true)
											SET_TIME_SCALE(0.3)
											fTimeScale = 0.3
											SETTIMERA(0)
											TRIGGER_MUSIC_EVENT("SOL1_TRAIN_JUMP")
											IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_TRAIN")
												START_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_TRAIN")
											ENDIF
											
											REPLAY_RECORD_BACK_FOR_TIME(6.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
											
											bDoneSlowMoJump = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF btimeScaleBackToNormal = FALSE
//									PRINTSTRING("Timer A is ") PRINTINT(TIMERA()) PRINTNL()
									//Put time back to normal if player is not in air or if timer reaches max air time
									IF TIMERA() > 250
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))										
												IF NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
													SET_TIME_SCALE(1.0)
													SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", false)
													playerDoneTrainJump = TRUE
													btimeScaleBackToNormal = TRUE
												ENDIF
											ENDIF
										ELSE
											SET_TIME_SCALE(1.0)
											SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", false)
											playerDoneTrainJump = TRUE
											btimeScaleBackToNormal = TRUE	
										ENDIF
									ENDIF
									IF TIMERA() > 379
										fTimeScale = fTimeScale*1.05
										SET_TIME_SCALE(fTimeScale)
										
										IF fTimeScale >= 1
											SET_TIME_SCALE(1.0)
											SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", false)
											playerDoneTrainJump = TRUE
											btimeScaleBackToNormal = TRUE
										ENDIF
									ENDIF										
								ENDIF
							ENDIF
						ELSE
							IF doneS1_JUMP4 = FALSE
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_JUMP4", CONV_PRIORITY_MEDIUM)
										//There ya go!
										//Ooooop!
										doneS1_JUMP4 = TRUE
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ELSE
						IF safetyTimeScaleBackToNormalSet = FALSE
							SET_TIME_SCALE(1.0)
							
							//Add in a video recording clip of the train jump now.
							REPLAY_RECORD_BACK_FOR_TIME(5.0)
							safetyTimeScaleBackToNormalSet = TRUE
						ENDIF
						IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_TRAIN_SCENE")
							STOP_AUDIO_SCENE("MARTIN_1_TRAIN_SCENE")
						ENDIF
						IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_JUMP_TRAIN")
							STOP_AUDIO_SCENE("MARTIN_1_CHASE_JUMP_TRAIN")
						ENDIF
//							PRINTSTRING("Player has done train jump") PRINTNL()
					ENDIF
					
					IF fRecordingTime > 90000
						SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
					ENDIF
					
					//Handle the audio stream
					IF requestedCrashStream = FALSE
						IF fRecordingTime > 108000
							IF REQUEST_SCRIPT_AUDIO_BANK("MARTIN_1_01")
							AND REQUEST_SCRIPT_AUDIO_BANK("MARTIN_1_02")
								requestedCrashStream = TRUE
							ELSE
								PRINTSTRING("waiting for MARTIN_1_01 and MARTIN_1_02 audio banks to load") PRINTNL()
							ENDIF
						ENDIF
					ELSE
						IF crashStreamStarted = FALSE
							IF fRecordingTime > 117262
								IF IS_VEHICLE_DRIVEABLE(vehTitan)
									STOP_STREAM()
//									PLAY_SOUND_FRONTEND(-1, "MARTIN_1_PLANE_CRASH_MASTER")
//									PLAY_STREAM_FROM_VEHICLE(vehTitan)
									PLAY_SOUND_FROM_ENTITY(-1, "MARTIN_1_PLANE_CRASH_MASTER", vehTitan)
									PRINTSTRING("MARTIN_1_PLANE_CRASH_MASTER being told to play now")
									crashStreamStarted = TRUE
								ENDIF
							ENDIF					
						ENDIF
					ENDIF
							
					//Swap IPL's to create the trench in the ground now
					IF IPLSwapped = FALSE
						IF fRecordingTime > 117262
							IF GET_BUILDING_DATA(PlaneCrashTrench, BUILDINGNAME_IPL_PLANE_CRASH_TRENCH) 
								SET_BUILDING_STATE(BUILDINGNAME_IPL_PLANE_CRASH_TRENCH, BUILDINGSTATE_DESTROYED)
								IPLSwapped = TRUE
							ENDIF
						ENDIF
					ENDIF
								
					IF preparedInteractiveMusic2 = FALSE
						IF fRecordingTime > 108000
							PREPARE_MUSIC_EVENT("SOL1_CRASH")
							REQUEST_MODEL(G_M_Y_MEXGANG_01)
							REQUEST_MODEL(G_M_Y_MEXGOON_02)
							preparedInteractiveMusic2 = TRUE
						ENDIF
					ELSE
						IF startInteractiveMusic2 = FALSE
							//PLANE HITS THE GROUND HERE TO BEGIN CRASH
							IF fRecordingTime > 117262
								IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_CHASE_MAIN")
									STOP_AUDIO_SCENE("MARTIN_1_CHASE_MAIN")
								ENDIF
								IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CRASH")
									STOP_AUDIO_SCENE("MARTIN_1_PLANE_CRASH")
								ENDIF
								IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_KILL_PASSENGERS")
									START_AUDIO_SCENE("MARTIN_1_KILL_PASSENGERS")
								ENDIF
								IF PlaneOnfireSoundStopped = FALSE
									STOP_SOUND(PlaneonfireSoundID)
									PlaneOnfireSoundStopped = TRUE
								ENDIF
								
								REQUEST_MODEL(PROP_SHAMAL_CRASH)
								REQUEST_ANIM_DICT("missmartin1@exit_plane")
								REQUEST_ANIM_DICT("missmartin1@leadinoutmartin_1_dead_pilot")
								TRIGGER_MUSIC_EVENT("SOL1_CRASH")
								startInteractiveMusic2 = TRUE
							ENDIF
						ENDIF
					ENDIF
						
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[2])
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF fRecordingTime > 116262
								ptfxEngine[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_crash_dust", vehTitan,VECTOR_ZERO,VECTOR_ZERO)
							ENDIF
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(vehTitan)
							IF IS_VEHICLE_DRIVEABLE(vehTitan)
								IF fRecordingTime > 119000			
									SET_VEHICLE_DOOR_OPEN(vehTitan, SC_DOOR_FRONT_LEFT, FALSE, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF

					//Start cutscene once player gets to the plane and its stopped playing back its recording
					IF bMissionFailed = FALSE
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[1])	
									STOP_PARTICLE_FX_LOOPED(ptfxEngine[1])
								ENDIF
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[2])
									STOP_PARTICLE_FX_LOOPED(ptfxEngine[2])
								ENDIF								
								
								IF DOES_ENTITY_EXIST(pedPilot)
									IF NOT IS_PED_INJURED(pedPilot)
										SET_ENTITY_COORDS(pedPilot, <<2808.3870, 4795.4839, 45.8104>>)
										SET_ENTITY_COLLISION(pedPilot, FALSE)
									ENDIF
								ENDIF
								DELETE_VEHICLE(vehTitan)
//								IF NOT IS_PED_INJURED(pedPilot)
//									SET_ENTITY_HEALTH(pedPilot, 0)
//								ENDIF
								
								IF HAS_MODEL_LOADED(PROP_SHAMAL_CRASH)
									ObjTitan = CREATE_OBJECT_NO_OFFSET(PROP_SHAMAL_CRASH, <<2807.691895, 4796.448730, 47.668404>> )
									SET_ENTITY_COLLISION(ObjTitan, FALSE)
									SET_ENTITY_ROTATION(ObjTitan, <<-0.000000, -0.000000, -11.340065>>)
									SET_ENTITY_QUATERNION(ObjTitan, -0.0162, -0.0524, -0.1071, 0.9927)
									SET_ENTITY_INVINCIBLE(ObjTitan, TRUE)
									FREEZE_ENTITY_POSITION(ObjTitan, TRUE)
									SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SHAMAL_CRASH)
								ENDIF
								
								IF HAS_ANIM_DICT_LOADED("missmartin1@leadinoutmartin_1_dead_pilot")
									IF DOES_ENTITY_EXIST(pedPilot)
										IF NOT IS_PED_INJURED(pedPilot)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPilot, TRUE)
											SET_ENTITY_COORDS(pedPilot, <<2808.3870, 4795.4839, 45.8104>>)
											ATTACH_ENTITY_TO_ENTITY(pedPilot, ObjTitan, 0, << -0.54, 5.7, 0.03 >>, <<0,0,0>>)
											TASK_PLAY_ANIM(pedPilot, "missmartin1@leadinoutmartin_1_dead_pilot", "dead_pilot_leadin_loop", INSTANT_BLEND_IN, 
												NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
											SET_PED_KEEP_TASK(pedPilot, TRUE)
											SET_ENTITY_COLLISION(pedPilot, FALSE)
											APPLY_PED_BLOOD_SPECIFIC(pedPilot, ENUM_TO_INT(PDZ_HEAD), 0.515, 0.814, 300.781, 0.975, 3, 0.00, "stab") 
											APPLY_PED_BLOOD_SPECIFIC(pedPilot, ENUM_TO_INT(PDZ_HEAD), 0.633, 0.620, 300.617, 0.262, 1, 0.00, "ShotgunLargeMonolithic") 
											SET_PED_CAN_BE_TARGETTED(pedPilot, FALSE)
											SET_FACIAL_IDLE_ANIM_OVERRIDE(pedPilot, "dead_1")
											//PCF_DisableLockonToRandomPeds
											//PCF_CannotBeTargeted
										ENDIF
									ENDIF								
								ENDIF
								
								//Create the end enemies the player should have to kill
								IF HAS_MODEL_LOADED(G_M_Y_MEXGANG_01)
								AND HAS_MODEL_LOADED(G_M_Y_MEXGOON_02)
								AND HAS_ANIM_DICT_LOADED("missmartin1@exit_plane")
									IF NOT DOES_ENTITY_EXIST(endGuy[0])
//										IF IS_VEHICLE_DRIVEABLE(vehTitan)
//											endGuy[0] = CREATE_PED_INSIDE_VEHICLE(vehTitan, PEDTYPE_MISSION, G_M_Y_MEXGANG_01, VS_FRONT_RIGHT)
											//PRE NG LOCATION
											//endGuy[0] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGANG_01, <<2806.1355, 4801.3262, 46.0759>>, 78.4343)
											
											endGuy[0] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGANG_01, <<2807.3120, 4802.3140, 46.3392>>, 78.4343)
											
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HEAD, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_BERD, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HAIR, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_TORSO, 1, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_LEG, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HAND, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_FEET, 0, 2)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_TEETH, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_SPECIAL, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_SPECIAL2, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_DECL, 0, 0)
											SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_JBIB, 0, 0)
											
											STOP_PED_WEAPON_FIRING_WHEN_DROPPED(endGuy[0])
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(endGuy[0], TRUE)
											GIVE_WEAPON_TO_PED(endGuy[0], WEAPONTYPE_PISTOL, 100)
											APPLY_PED_BLOOD_BY_ZONE(endGuy[0], ENUM_TO_INT(PDZ_HEAD), 0.360, 0.710, "ShotgunSmall")
											SET_PED_SPHERE_DEFENSIVE_AREA(endGuy[0], GET_ENTITY_COORDS(endGuy[0]), 10)
//											SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(endGuy[0], vehTitan, <<0,0,0>>, 10)
											SET_PED_COMBAT_ATTRIBUTES(endGuy[0], CA_CAN_CHARGE, TRUE)
											
											//PRE NG ANIM LOCATION
											//TASK_PLAY_ANIM_ADVANCED(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b", <<2808.3870, 4795.4839, 47.4224>>, <<0,0,-0.21>>, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET)
											
											TASK_PLAY_ANIM_ADVANCED(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b", <<2804.3113, 4798.0303, 46.7273>>, <<0,0,-0.21>>, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET)
											
											iAnimTaskTime = GET_GAME_TIMER()
//											LeavingJetSyncScene = CREATE_SYNCHRONIZED_SCENE(<<2808.3870, 4795.4839, 47.4224>>, <<0,0,-0.21>>)
//											TASK_SYNCHRONIZED_SCENE(endGuy[0], LeavingJetSyncScene, "missmartin1@exit_plane", "exit_plane_guy_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
//										ENDIF
									ENDIF
									IF NOT DOES_ENTITY_EXIST(endGuy[1])
										endGuy[1] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGOON_02, <<2805.3694, 4799.1553, 46.0390>>, 82.1640)
										
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_HEAD, 0, 1)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_BERD, 0, 0)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_HAIR, 0, 0)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_TORSO, 0, 2)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_LEG, 0, 2)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_HAND, 0, 0)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_FEET, 0, 0)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_TEETH, 0, 0)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_SPECIAL, 0, 0)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_SPECIAL2, 0, 0)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_DECL, 0, 0)
										SET_PED_COMPONENT_VARIATION(endGuy[1], PED_COMP_JBIB, 0, 0)										
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(endGuy[1], TRUE)
										APPLY_PED_BLOOD_BY_ZONE(endGuy[1], ENUM_TO_INT(PDZ_HEAD), 0.360, 0.710, "ShotgunSmall")
//										TASK_WRITHE(endGuy[1], PLAYER_PED_ID(), 25, SHM_ONGROUND)
										//PRE NG
										//TASK_PLAY_ANIM_ADVANCED(endGuy[1], "missmartin1@exit_plane", "exit_plane_guy_a", <<2808.3870, 4795.4839, 47.4224>>, <<0,0,-0.21>>, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME|AF_EXTRACT_INITIAL_OFFSET)
										
										TASK_PLAY_ANIM_ADVANCED(endGuy[1], "missmartin1@exit_plane", "exit_plane_guy_a", <<2806.2720, 4799.6250, 46.5135>>, <<0,0,40.0>>, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME|AF_EXTRACT_INITIAL_OFFSET|AF_NOT_INTERRUPTABLE)
										
										//<<2809.2, 4798.6, 47.85>>
//										LeavingJetSyncScene = CREATE_SYNCHRONIZED_SCENE(<<2808.3870, 4795.4839, 47.4224>>, <<0,0,-0.21>>)
//										TASK_SYNCHRONIZED_SCENE(endGuy[1], LeavingJetSyncScene, "missmartin1@exit_plane", "exit_plane_guy_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
									ENDIF
									SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MEXGANG_01)
									SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MEXGOON_02)
								ENDIF
								
								IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_PLANE_CHASE_SCENE")
									STOP_AUDIO_SCENE("MARTIN_1_PLANE_CHASE_SCENE")
								ENDIF
						
								REMOVE_VEHICLE_RECORDING(1, "BB_SOL_JET")
//								IF IS_VEHICLE_DRIVEABLE(vehTitan)
								IF DOES_ENTITY_EXIST(ObjTitan)
									ptfxEngine[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_wreck", ObjTitan, VECTOR_ZERO, VECTOR_ZERO)
								ENDIF
								
								//New PTFX for next Gen
								ptfxEngine[4] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_spot", <<2803.5, 4789.3, 46.6>>, <<89.5, 0,0>>, 1.5)
								ptfxEngine[5] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_trail", <<2805.4, 4782.5, 46.5>>, <<89.5, 0, 74>>, 1.5)
								ptfxEngine[6] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_trail", <<2804.1, 4777.3, 46.2>>, <<89.5, 0, 74>>, 1.5)
								ptfxEngine[7] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_drip", <<2808.2, 4791.6, 48.8>>, <<89.5, 0, 169.1>>, 1.5)
								ptfxEngine[8] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_plane_tail_fire", <<2805.8, 4788.5, 49.8>>, <<89.5, -100, -40>>, 0.7)
								ptfxEngine[9] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_plane_engine_fire", <<2808.6, 4793.4, 48.7>>, <<0, 0, -10>>, 0.6)
								ptfxEngine[10] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_plane_elec_crackle", <<2809.1, 4802.4, 48.0>>, <<0,0,0>>, 0.9)
//								SET_VEHICLE_ENGINE_ON(vehTitan, FALSE, TRUE)
								
								SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
								
								//Create a video recording clip of the plane crashing
								REPLAY_RECORD_BACK_FOR_TIME(12.0)
								
								iStageSection ++
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 1

			IF bTriggerFX[2] = TRUE
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[1])
					STOP_PARTICLE_FX_LOOPED(ptfxEngine[1])
					bTriggerFX[2] = FALSE
				ENDIF
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(10.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)

			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)

			bStageSetup = FALSE
			RETURN TRUE
//			iStageSection++
		ENDIF

	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL stageKillPassengers()

//	#IF IS_DEBUG_BUILD
//		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[10])
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1)
//				fDirZ = (fDirX + 10)
//				REMOVE_PARTICLE_FX(ptfxEngine[10])
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2)
//				fDirZ = (fDirX - 10)
//				REMOVE_PARTICLE_FX(ptfxEngine[10])
//			ENDIF	
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_3)
//				fDirZ = (fDirY + 10)
//				REMOVE_PARTICLE_FX(ptfxEngine[10])
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_4)
//				fDirZ = (fDirY - 10)
//				REMOVE_PARTICLE_FX(ptfxEngine[10])
//			ENDIF	
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_5)
//				fDirY = (fDirZ + 10)
//				REMOVE_PARTICLE_FX(ptfxEngine[10])
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_6)
//				fDirY = (fDirZ - 10)
//				REMOVE_PARTICLE_FX(ptfxEngine[10])
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
//				fScale = (fScale + 0.1)
//				REMOVE_PARTICLE_FX(ptfxEngine[10])
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
//				fScale = (fScale - 0.1)
//				REMOVE_PARTICLE_FX(ptfxEngine[10])
//			ENDIF
//		ELSE
//			ptfxEngine[10] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_plane_elec_crackle", <<2809.8, 4801.6, 47.7>>, <<fDirX, fDirY, fDirZ>>, fScale)
//			PRINTSTRING("ptfxEngine[9] being re-created for positioning purposes")PRINTNL()
//			PRINTSTRING("fDirX = ") PRINTFLOAT(fDirX) PRINTNL()
//			PRINTSTRING("fDirY = ") PRINTFLOAT(fDirY) PRINTNL()
//			PRINTSTRING("fDirZ = ") PRINTFLOAT(fDirZ) PRINTNL()
//			PRINTSTRING("fScale = ") PRINTFLOAT(fScale) PRINTNL()
//			//START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_spot", <<2803.5, 4789.3, 46.6>>, <<89.5, fDirY, fDirZ>>, fScale)
//		ENDIF
//	#ENDIF

	//Skip anims forward to allow for planes collision to be turned back on quicker
	IF AnimForPed0Skipped = FALSE
		IF DOES_ENTITY_EXIST(endGuy[0])
			IF NOT IS_PED_INJURED(endGuy[0])
				IF IS_ENTITY_PLAYING_ANIM(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b")
					SET_ENTITY_ANIM_CURRENT_TIME(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b", 0.3)
					AnimForPed0Skipped = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF AnimForPed1Skipped = FALSE
		IF DOES_ENTITY_EXIST(endGuy[1])
			IF NOT IS_PED_INJURED(endGuy[1])
				IF IS_ENTITY_PLAYING_ANIM(endGuy[1], "missmartin1@exit_plane", "exit_plane_guy_a")
					SET_ENTITY_ANIM_CURRENT_TIME(endGuy[1], "missmartin1@exit_plane", "exit_plane_guy_a", 0.3)
					AnimForPed1Skipped = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehCutBike = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF	

	//Handle requesting and unrequesting the mocap scene if player gets close to cutscene trigger point.
	IF CutsceneRequested = FALSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2803.8, 4798.1, 47.7>>) < DEFAULT_CUTSCENE_LOAD_DIST
			REQUEST_CUTSCENE("Martin_1_mcs_1")
			CutsceneRequested = TRUE
		ENDIF
	ENDIF
	IF CutsceneRequested = TRUE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2803.8, 4798.1, 47.7>>) > DEFAULT_CUTSCENE_UNLOAD_DIST
			REMOVE_CUTSCENE()
			PilotComponentsGiven = FALSE
			CutsceneRequested = FALSE
		ENDIF	
		//Set up Trev's clothes for opening cutscene
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			//Trevor
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
			PRINTSTRING("TRIGGER SCENE - Trev's cutscene components set 2.") PRINTNL()
		ENDIF
	ENDIF
	
	//Give the pilots components from pedpilot to the cutscene pilot so they are the same.
	IF CutsceneRequested = TRUE
	AND PilotComponentsGiven = FALSE
		IF DOES_ENTITY_EXIST(pedPilot)
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Pilot_Martin_1", pedPilot)
				PilotComponentsGiven = TRUE
			ENDIF
		ENDIF
	ENDIF
	
//	IF DOES_ENTITY_EXIST(pedPilot)
//		IF NOT IS_PED_INJURED(pedPilot)
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(key_9)
//				vOffset.z = (vOffset.z + 0.02)
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(key_8)
//				vOffset.z = (vOffset.z - 0.02)
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(key_7)
//				vOffset.y = (vOffset.y + 0.02)
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(key_6)
//				vOffset.y = (vOffset.y - 0.02)
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(key_5)
//				vOffset.x = (vOffset.x + 0.02)
//			ENDIF
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(key_4)
//				vOffset.x = (vOffset.x - 0.02)
//			ENDIF	
//			
//			ATTACH_ENTITY_TO_ENTITY(pedPilot, ObjTitan, 0, vOffset, <<0,0,0>>)
//			PRINTSTRING("vOffset = ") PRINTVECTOR(vOffset) PRINTNL()
//			IF NOT IS_ENTITY_PLAYING_ANIM(pedPilot, "missmartin1@leadinoutmartin_1_dead_pilot", "dead_pilot_leadin_loop")
//				TASK_PLAY_ANIM(pedPilot, "missmartin1@leadinoutmartin_1_dead_pilot", "dead_pilot_leadin_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
			IF pedHelmetRemoved = FALSE
				REMOVE_PED_HELMET(PLAYER_PED_ID(), FALSE)
				pedHelmetRemoved = TRUE
			ENDIF
		ENDIF
	ELSE
		IF pedHelmetRemoved = TRUE
			pedHelmetRemoved = FALSE
		ENDIF
	ENDIF
//	
//	IF DOES_ENTITY_EXIST(pedPilot)	
//		IF NOT IS_PED_INJURED(pedPilot)
//			IF DOES_SCENARIO_EXIST_IN_AREA(<<2809.4, 4801.3, 47.7>>, 3, FALSE)
//				IF GET_SCRIPT_TASK_STATUS(pedPilot, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
//				AND GET_SCRIPT_TASK_STATUS(pedPilot, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
//					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedPilot, <<2809.4, 4801.3, 47.7>>, 5, 0)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_KILL_PASSENGERS")
		
		missionCanFail 		= FALSE
//		planefrozen			= FALSE
		doneKillPedGodText 	= FALSE
		CombatTaskGiven 	= FALSE
		CombatTaskRequired 	= FALSE
		pedHelmetRemoved 	= FALSE
		doneS1_ATTACK 		= FALSE
		PilotComponentsGiven = FALSE
		cinematicCamDeactivated = TRUE
		collisionTurnedOnForPlane = FALSE
		AnimForPed0Skipped = FALSE
		AnimForPed1Skipped = FALSE
		PRINTSTRING("MISSION STAGE KILL PASSENGERS") PRINTNL()	
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		//Disable all the police for the chase section
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_HELICOPTER, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_VEHICLE_REQUEST, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_ROAD_BLOCK, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, FALSE)
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, FALSE)
	    ALLOW_DISPATCH(DT_SWAT_AUTOMOBILE, FALSE)
	    ALLOW_DISPATCH(DT_SWAT_HELICOPTER, FALSE)			
		
		REQUEST_ANIM_DICT("COMBAT@DAMAGE@WRITHEIDLE_A")
		REQUEST_ANIM_DICT("COMBAT@DAMAGE@WRITHEIDLE_B")
		
		//Remove bikes blip incase it's on.
		IF DOES_BLIP_EXIST(bikeBlip)
			REMOVE_BLIP(bikeBlip)
		ENDIF		
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			
			
			WHILE IS_NEW_LOAD_SCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			
			SETTIMERB(0)
			NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), REPLAY_LOAD_SCENE_SIZE)
			WAIT(0)
			WHILE IS_NEW_LOAD_SCENE_ACTIVE()
			AND NOT IS_NEW_LOAD_SCENE_LOADED()
			AND TIMERB() < 20000
				PRINTSTRING("doing extra wait for crashed plane ") PRINTINT(TIMERB()) PRINTNL()
				WAIT(0)
			ENDWHILE
			
			END_REPLAY_SETUP(PlayerCar)
			IF IS_VEHICLE_DRIVEABLE(PlayerCar)
				SET_VEHICLE_FORWARD_SPEED(PlayerCar, 20)
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			IF IS_VEHICLE_DRIVEABLE(vehTitan)
			IF DOES_ENTITY_EXIST(ObjTitan)
				ptfxEngine[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sol1_plane_wreck", ObjTitan, VECTOR_ZERO, VECTOR_ZERO)
				ptfxEngine[4] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_spot", <<2803.5, 4789.3, 46.6>>, <<89.5, 0,0>>, 1.5)
				ptfxEngine[5] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_trail", <<2805.4, 4782.5, 46.5>>, <<89.5, 0, 74>>, 1.5)
				ptfxEngine[6] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_trail", <<2804.1, 4777.3, 46.2>>, <<89.5, 0, 74>>, 1.5)
				ptfxEngine[7] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_fire_drip", <<2808.2, 4791.6, 48.8>>, <<89.5, 0, 169.1>>, 1.5)
				ptfxEngine[8] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_plane_tail_fire", <<2805.8, 4788.5, 49.8>>, <<89.5, -100, -40>>, 0.7)
				ptfxEngine[9] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_plane_engine_fire", <<2808.6, 4793.4, 48.7>>, <<0, 0, -10>>, 0.6)
				ptfxEngine[10] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sol1_plane_elec_crackle", <<2809.1, 4802.4, 48.0>>, <<0,0,0>>, 0.9)
			ENDIF
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF		
		
		SETTIMERB(0)
		
		SET_MAX_WANTED_LEVEL(0)
		
		SET_ENTITY_CAN_BE_TARGETED_WITHOUT_LOS(PLAYER_PED_ID(), TRUE)
		
		iMiniCutStage 			= 0
		iStageSection 			= 0
		missionCanFail 			= TRUE
		bStageSetup 			= TRUE	
		iHelpChatTimer 			= GET_GAME_TIMER()
		iKillChatTimer			= GET_GAME_TIMER()
	
	ELSE
	
		SWITCH iStageSection
			CASE 0
				
//				ChaseHintCam()
				BOOL bForceCollisionOnForPlane
				bForceCollisionOnForPlane = FALSE
				
				IF DOES_ENTITY_EXIST(endGuy[0])
					IF NOT IS_PED_INJURED(endGuy[0])
						IF IS_ENTITY_PLAYING_ANIM(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b")
							IF collisionTurnedOnForPlane = FALSE
								IF GET_ENTITY_ANIM_CURRENT_TIME(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b") > 0.427
									SET_ENTITY_COLLISION(ObjTitan, TRUE)
									collisionTurnedOnForPlane = TRUE
								ENDIF
							ENDIF
							IF CombatTaskGiven = FALSE
								IF GET_ENTITY_ANIM_CURRENT_TIME(endGuy[0], "missmartin1@exit_plane", "exit_plane_guy_b") > 0.99
									STOP_ENTITY_ANIM(endGuy[0], "exit_plane_guy_b", "missmartin1@exit_plane", REALLY_SLOW_BLEND_OUT)
									TASK_PLAY_ANIM(endGuy[0], "missmartin1@exit_plane", "loop_guy_b", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
								ENDIF
							ENDIF
						ELSE
							IF GET_GAME_TIMER() >= iAnimTaskTime + 500
								bForceCollisionOnForPlane = TRUE
							ENDIF
						ENDIF
					ELSE
						bForceCollisionOnForPlane = TRUE
					ENDIF
				ENDIF
				
				IF collisionTurnedOnForPlane = FALSE
					IF bForceCollisionOnForPlane
						IF DOES_ENTITY_EXIST(ObjTitan)
							SET_ENTITY_COLLISION(ObjTitan, TRUE)
							collisionTurnedOnForPlane = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Blip the bad guys once player gets close enough to the plane
				IF DOES_ENTITY_EXIST(ObjTitan)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ObjTitan) < 50
						IF DOES_ENTITY_EXIST(endGuy[0])
							IF NOT IS_PED_INJURED(endGuy[0])
								IF NOT DOES_BLIP_EXIST(endGuyBlip[0])
									endGuyBlip[0] = CREATE_BLIP_FOR_ENTITY(endGuy[0], TRUE)
									TASK_LOOK_AT_ENTITY(endGuy[0], PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
								ENDIF
								IF doneKillPedGodText = FALSE
									PRINT_NOW("M1_GOD2", DEFAULT_GOD_TEXT_TIME, -1)//~s~Kill ~r~Javier.
									doneKillPedGodText = TRUE
								ENDIF
								IF CombatTaskGiven = FALSE
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), endGuy[0]) < 100
										
										//Some dialogue from Javier before player starts attacking
										IF GET_GAME_TIMER() > (iHelpChatTimer + 4000)
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()	
													ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, endGuy[0], "JAVIER")
													IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_STUMBLE", CONV_PRIORITY_MEDIUM)
														//You gotta help us!  
														//Hey! We crashed!
														//Call the cops, I'm working with the government!
														//I'm an important guy, you gotta help me out!
														iHelpChatTimer = GET_GAME_TIMER()
													ENDIF	
												ENDIF
											ENDIF
										ENDIF
										
										//Javier should become hostile if ...
										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), endGuy[0]) < 30
											//Player is shooting near him
											IF IS_PED_SHOOTING(PLAYER_PED_ID())
												CombatTaskRequired = TRUE
											ENDIF
											//Player is aiming at him
											IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
												IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
												AND NOT IS_PHONE_ONSCREEN()
													IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), endGuy[0])
														CombatTaskRequired = TRUE
													ENDIF
												ENDIF
											ENDIF
											//Player is in melee combat
											IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
												CombatTaskRequired = TRUE
											ENDIF
										ENDIF

										IF CombatTaskRequired = TRUE
//											SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(endGuy[0], vehTitan, <<0,0,0>>, 10)
											SET_PED_SPHERE_DEFENSIVE_AREA(endGuy[0], GET_ENTITY_COORDS(endGuy[0]), 10)
											SET_PED_COMBAT_ATTRIBUTES(endGuy[0], CA_CAN_CHARGE, TRUE)
											TASK_COMBAT_PED(endGuy[0], PLAYER_PED_ID())
											CombatTaskGiven = TRUE
										ENDIF
									ENDIF
								ELSE
									//Some dialogue from Javier once he becomes hostile
									IF doneS1_ATTACK = FALSE
										CLEAR_PRINTS()
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, endGuy[0], "JAVIER")
										IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_ATTACK", CONV_PRIORITY_MEDIUM)
											//It's a hit?  
											//Who sent you - Martin?
											//Guy musta taken down the jet!
											//It's a green light!
											//He's one of Martin's people!
											//If the crash didn't kill me, you ain't.
											iKillChatTimer = GET_GAME_TIMER()
											doneS1_ATTACK = TRUE
										ENDIF
									ELSE
										IF GET_GAME_TIMER() > (iKillChatTimer + 4000)
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()	
													ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, endGuy[0], "JAVIER")
													IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "S1_ATTACK", CONV_PRIORITY_MEDIUM)
														//It's a hit?  
														//Who sent you - Martin?
														//Guy musta taken down the jet!
														//It's a green light!
														//He's one of Martin's people!
														//If the crash didn't kill me, you ain't.
														iKillChatTimer = GET_GAME_TIMER()
													ENDIF	
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE	
								IF DOES_BLIP_EXIST(endGuyBlip[0])
									REMOVE_BLIP(endGuyBlip[0])
								ENDIF
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(endGuyBlip[0])
								REMOVE_BLIP(endGuyBlip[0])
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(endGuy[1])
							IF NOT IS_ENTITY_DEAD(endGuy[1])
//								IF NOT DOES_BLIP_EXIST(endGuyBlip[1])
//									endGuyBlip[1] = CREATE_BLIP_FOR_ENTITY(endGuy[1], TRUE)
//								ENDIF
//								IF IS_SYNCHRONIZED_SCENE_RUNNING(LeavingJetSyncScene)
								IF IS_ENTITY_PLAYING_ANIM(endGuy[1], "missmartin1@exit_plane", "exit_plane_guy_a")
//									IF GET_SYNCHRONIZED_SCENE_PHASE(LeavingJetSyncScene) > 0.99
									IF GET_ENTITY_ANIM_CURRENT_TIME(endGuy[1], "missmartin1@exit_plane", "exit_plane_guy_a") > 0.587
//										STOP_SYNCHRONIZED_ENTITY_ANIM(endGuy[0], INSTANT_BLEND_OUT, TRUE)
//										STOP_ENTITY_ANIM(endGuy[1], "exit_plane_guy_a", "missmartin1@exit_plane", REALLY_SLOW_BLEND_OUT)
										TASK_WRITHE(endGuy[1], PLAYER_PED_ID(), 25, SHM_ONGROUND)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(endGuy[1], TRUE)
									ENDIF
								ENDIF
//								IF IS_SYNCHRONIZED_SCENE_RUNNING(LeavingJetSyncScene)
//									IF GET_SYNCHRONIZED_SCENE_PHASE(LeavingJetSyncScene) > 0.99
//										STOP_SYNCHRONIZED_ENTITY_ANIM(endGuy[1], INSTANT_BLEND_OUT, TRUE)
////										TASK_PLAY_ANIM(endGuy[1], "missmartin1@exit_plane", "loop_guy_a", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//										TASK_WRITHE(endGuy[1], PLAYER_PED_ID(), 25, SHM_ONGROUND)
//									ENDIF
//								ENDIF
//								IF doneKillPedGodText = FALSE
//									PRINT_NOW("M1_GOD2", DEFAULT_GOD_TEXT_TIME, -1)//~s~Kill ~r~Javier.
//									doneKillPedGodText = TRUE
//								ENDIF
							ELSE	
								IF DOES_BLIP_EXIST(endGuyBlip[1])
									REMOVE_BLIP(endGuyBlip[1])
								ENDIF
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(endGuyBlip[1])
								REMOVE_BLIP(endGuyBlip[1])
							ENDIF
						ENDIF
						
						IF NOT DOES_BLIP_EXIST(endGuyBlip[0])
						AND NOT DOES_BLIP_EXIST(endGuyBlip[1])
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
							SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
							REMOVE_ANIM_DICT("missmartin1@exit_plane")
							REMOVE_ANIM_DICT("COMBAT@DAMAGE@WRITHEIDLE_A")
							REMOVE_ANIM_DICT("COMBAT@DAMAGE@WRITHEIDLE_B")
							iStageSection ++
						ENDIF
						
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 1
				
				//Start the mocap scene once player enters the locate at the door to the plane.
//				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID() , <<2806.580078,4802.417480,45.348091>>, <<2806.095459,4798.916016,48.348846>>, 3.750000)
//				IF DOES_ENTITY_EXIST(vehTitan)
//					IF IS_VEHICLE_DRIVEABLE(vehTitan)
//						//Unfreeze the plane if player is getting into it so the door opens.
//						IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
//							IF planeFrozen = TRUE
//								FREEZE_ENTITY_POSITION(vehTitan, FALSE)
//								planeFrozen = FALSE
//							ENDIF
//						ELSE
//							IF planeFrozen = FALSE
//								FREEZE_ENTITY_POSITION(vehTitan, TRUE)
//								planeFrozen = TRUE
//							ENDIF
//						ENDIF
//						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTitan, TRUE)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2807.429199,4802.980469,45.975845>>, <<2806.673584,4799.813477,48.226929>>, 3.500000)
							IF DOES_BLIP_EXIST(blipObj)
								CLEAR_PRINTS()
								REMOVE_BLIP(blipObj)
							ENDIF
							PRINTSTRING("Player is in vehicle has returned true, cutscene should be starting now")
							REMOVE_ANIM_DICT("missmartin1@exit_plane")
							iStageSection ++
						ELSE
							IF NOT DOES_BLIP_EXIST(blipObj)
								blipObj = ADD_BLIP_FOR_COORD(<<2807.9, 4800.7, 47.1>>)
								SET_BLIP_AS_FRIENDLY(blipObj, TRUE)
								PRINT_NOW("M1_GOD1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Go inside the ~b~plane.
							ENDIF	
						ENDIF
//					ENDIF
//				ENDIF
							
			BREAK

		ENDSWITCH

//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 2		

			REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)

			STOP_STREAM()			
			
//			//Remove Shamal from audio mix group
//			IF DOES_ENTITY_EXIST(vehTitan)
//				IF IS_VEHICLE_DRIVEABLE(vehTitan)
//					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehTitan)
//				ENDIF
//			ENDIF
			
//			SET_VEHICLE_DENSITY_MULTIPLIER(1)
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DISABLE_FIRST_PERSON_CAMERA_DURING_TREVOR_SWITCH_SCENE()
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//		IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
//				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missmartin1@switch", "trevor_switch_trevor")
//					DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//				ENDIF
//			ENDIF
//		ENDIF
		IF bCamViewModeChanged = TRUE
			SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Cutscene where plane crashes into the ground
FUNC BOOL stageCutscenePlaneCrash()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
//		Set_Replay_Mid_Mission_Stage(ENUM_TO_INT(STAGE_CUTSCENE_PLANE_CRASH))
		
		missionCanFail 			= FALSE
		cutsceneSkipped			= FALSE
		ringToneStarted 		= FALSE
		ringToneStopped 		= FALSE
		HavierMoved 			= FALSE
		bloodAdded 				= FALSE
		playersCarMoved 		= FALSE
//		startedLoadScene 		= FALSE
		PRINTSTRING("MISSION STAGE CUTSCENE PLANE CRASH") PRINTNL()
		
		REMOVE_VEHICLE_RECORDING(1,"BB_SOL_JET")
		
		//Request Cutscene for crash
		REQUEST_CUTSCENE("Martin_1_mcs_1")
//		//Request crashed plane model
//		REQUEST_MODEL(PROP_DEST_PLANE)
		//Request the van model for next stage
		REQUEST_MODEL(BURRITO3)
		REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR))
		REQUEST_MODEL(P_RCSS_FOLDED)
		REQUEST_ANIM_DICT("missmartin1@switch")
		REQUEST_PLAYER_PED_MODEL(CHAR_MICHAEL)
		
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		IF DOES_BLIP_EXIST(blipObj)
			REMOVE_BLIP(blipObj)
		ENDIF		

//		//Set pilots health to zero	
//		IF DOES_ENTITY_EXIST(pedPilot)
//			IF NOT IS_PED_INJURED(pedPilot)
//				SET_ENTITY_HEALTH(pedPilot, 0)
//			ENDIF
//		ENDIF
				
//		IF DOES_ENTITY_EXIST(vehTitan)		
//			IF IS_VEHICLE_DRIVEABLE(vehTitan)		
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTitan)
//					STOP_PLAYBACK_RECORDED_VEHICLE(vehTitan)
//				ENDIF
//			ENDIF
//		ENDIF
	
		WHILE NOT HAS_CUTSCENE_LOADED()
//		OR NOT HAS_MODEL_LOADED(PROP_DEST_PLANE)
			PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
			WAIT(0)
		ENDWHILE		
	
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		CLEAR_HELP()	
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(SANCHEZ, TRUE)
		
		IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_KILL_PASSENGERS")
			STOP_AUDIO_SCENE("MARTIN_1_KILL_PASSENGERS")
		ENDIF		
		
		IF IS_SCREEN_FADED_OUT()
			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		
		iStageSection 		= 0
		missionCanFail 		= TRUE
		bStageSetup 		= TRUE
		bPilotDeleted		= FALSE
		bCamViewModeChanged = FALSE

//═════════╡ UPDATE ╞═════════		
	ELSE
	
//		IF ( iStageSection > 0 )
//			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//		ENDIF

		SWITCH iStageSection
			CASE 0

				//Register entities for cutscene
				//Sexton
//				REGISTER_ENTITY_FOR_CUTSCENE(Sexton, "SextonDaniels", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, CS_SEXTONDANIELS)
				
				//plane
//				IF IS_VEHICLE_DRIVEABLE(vehTitan)
				IF DOES_ENTITY_EXIST(ObjTitan)
//					REGISTER_ENTITY_FOR_CUTSCENE(vehTitan, "Crashed_Plane", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PROP_DEST_PLANE)
					REGISTER_ENTITY_FOR_CUTSCENE(ObjTitan, "Crashed_Plane", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PROP_SHAMAL_CRASH)
				ENDIF
				
//				//******REMOVE THIS ONCE BUG 1400198 HAS BEEN FIXED *******//
//				IF DOES_ENTITY_EXIST(ObjTitan)
//					DELETE_OBJECT(ObjTitan)
//				ENDIF
				
				REMOVE_ANIM_DICT("missmartin1@leadinoutmartin_1_dead_pilot")
				
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Pilot_Martin_1", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, S_M_M_PILOT_01)
				
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Briefcase", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, P_ATTACHE_CASE_01_S)
//				
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				//Register the phone for after the cutscene
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevors_phone", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_PHONE_ING_02)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
		      	//Configure script systems into a safe mode.
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)				
//				
//				IF NOT DOES_CAM_EXIST(StaticCam)
//					StaticCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<2792.154541,4795.074219,55.678799>>,<<-28.129641,-0.000000,-69.815933>>,50.000000, TRUE)
//				ELSE
//					SET_CAM_PARAMS(StaticCam, <<2792.154541,4795.074219,55.678799>>,<<-28.129641,-0.000000,-69.815933>>,50.000000)
//					SET_CAM_ACTIVE(StaticCam, TRUE)
//				ENDIF
			
					
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2805.6519, 4802.8950, 46.0406>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 79.9974)
//				ENDIF				
				
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				REQUEST_VEHICLE_RECORDING(iRecNo, "BB_SOL_BIK")			
				
				TRIGGER_MUSIC_EVENT("SOL1_GOT_IT")
				
				CLEAR_PRINTS()
				KILL_ANY_CONVERSATION()
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
//				PRINT_NOW("BFPLHLD2", DEFAULT_GOD_TEXT_TIME, 1)//~s~Placeholder cutscene for Trevor retrieving the briefcase from the plane.
				
				IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
					REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				IF NOT IS_SCREEN_FADED_IN()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				SETTIMERA(0)
				
				iStageSection++

			BREAK
			
			CASE 1
			
				IF IS_CUTSCENE_PLAYING()
					IF bCamViewModeChanged = FALSE
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
							SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Changing cam view mode for first person on foot camera.")
							bCamViewModeChanged = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF playersCarMoved = FALSE
					IF IS_CUTSCENE_PLAYING()
						IF DOES_ENTITY_EXIST(vehCutBike)
							IF IS_VEHICLE_DRIVEABLE(vehCutBike)
								IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehCutBike))
								OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehCutBike))
								OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(vehCutBike))
								OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(vehCutBike))
									SET_ENTITY_COORDS(vehCutBike, <<2795.7239, 4801.2764, 45.5453>>)
									SET_ENTITY_HEADING(vehCutBike, 168.8915)
									SET_VEHICLE_ON_GROUND_PROPERLY(vehCutBike)
									playersCarMoved = TRUE
								ELSE
									SET_ENTITY_AS_MISSION_ENTITY(vehCutBike)
									DELETE_VEHICLE(vehCutBike)
									playersCarMoved = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bPilotDeleted
					IF IS_CUTSCENE_ACTIVE()
						IF DOES_ENTITY_EXIST(pedPilot)
							DELETE_PED(pedPilot)
							ENTITY_INDEX entity
							entity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Pilot_Martin_1")
							IF DOES_ENTITY_EXIST(entity)
								IF IS_ENTITY_A_PED(entity)
									pedPilot = GET_PED_INDEX_FROM_ENTITY_INDEX(entity)
								ENDIF
							ENDIF
							bPilotDeleted = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Kill the ped on the ground to stop him moving at end of cut-scene
				IF DOES_ENTITY_EXIST(endGuy[1])
				AND NOT IS_ENTITY_DEAD(endGuy[1])
					SET_ENTITY_HEALTH(endGuy[1], 0)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(objBriefcase)
					SET_ENTITY_AS_MISSION_ENTITY(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Briefcase"))
					objBriefcase = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Briefcase"))
				ENDIF	
				
				IF NOT DOES_ENTITY_EXIST(objTrevsPhone)
					SET_ENTITY_AS_MISSION_ENTITY(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevors_phone"))
					objTrevsPhone = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevors_phone"))
				ENDIF
				
				//Add blood to pilot
				IF bloodAdded = FALSE
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Pilot_Martin_1"))
						APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Pilot_Martin_1")), ENUM_TO_INT(PDZ_HEAD), 0.515, 0.814, 300.781, 0.975, 3, 0.00, "stab") 
						APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Pilot_Martin_1")), ENUM_TO_INT(PDZ_HEAD), 0.633, 0.620, 300.617, 0.262, 1, 0.00, "ShotgunLargeMonolithic") 
						bloodAdded = TRUE
					ENDIF
				ENDIF
				
				IF IS_CUTSCENE_PLAYING()
					//Move Havier if he is lying in the cutscene path.
					IF HavierMoved = FALSE
						IF DOES_ENTITY_EXIST(endGuy[0])
							IF IS_ENTITY_IN_ANGLED_AREA(endGuy[0] , <<2806.659180,4801.574219,48.026577>>, <<2800.094727,4802.901367,44.988617>>, 2.5)
//								vHavierCoords = GET_ENTITY_COORDS(endGuy[0], FALSE)
								IF NOT IS_PED_INJURED(endGuy[0])
									fHavierHeading = GET_ENTITY_HEADING(endGuy[0])
								ENDIF
								DELETE_PED(endGuy[0])
							ENDIF
						ENDIF
						IF NOT DOES_ENTITY_EXIST(endGuy[0])
							REQUEST_MODEL(G_M_Y_MEXGANG_01)
//							GET_GROUND_Z_FOR_3D_COORD((<<vHavierCoords.x, (vHavierCoords.y + 2), vHavierCoords.z>>), fHavierZ)
							IF HAS_MODEL_LOADED(G_M_Y_MEXGANG_01)
//								endGuy[0] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGANG_01, <<vHavierCoords.x, (vHavierCoords.y + 1.5), fHavierZ>>, fHavierHeading)
								//PRE NG LOCATION
								//endGuy[0] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGANG_01, <<2805.2, 4803.1, 46.2>>, fHavierHeading)
								
								endGuy[0] = CREATE_PED(PEDTYPE_MISSION, G_M_Y_MEXGANG_01, <<2803.2285, 4805.6099, 45.9882>>, fHavierHeading)
						
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HEAD, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_BERD, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HAIR, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_TORSO, 1, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_LEG, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_HAND, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_FEET, 0, 2)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_TEETH, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_SPECIAL, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_SPECIAL2, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_DECL, 0, 0)
								SET_PED_COMPONENT_VARIATION(endGuy[0], PED_COMP_JBIB, 0, 0)								
								
								STOP_PED_WEAPON_FIRING_WHEN_DROPPED(endGuy[0])
								EXPLODE_PED_HEAD(endGuy[0])
								//TASK_WRITHE(endGuy[0], PLAYER_PED_ID(), 1, SHM_ONGROUND)
								SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MEXGANG_01)
								HavierMoved = TRUE
							ENDIF
						ENDIF
					ENDIF
					
//					IF startedLoadScene = FALSE
//						IF NEW_LOAD_SCENE_START(<<2806.3, 4798.7, 47.9>>, <<7.2, 0, 80.6>>, 1000)
//							startedLoadScene = TRUE
//						ENDIF
//					ENDIF
					//Start the ringtone for the phone 
					IF NOT ARE_STRINGS_EQUAL (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, "Silent Ringtone Dummy")
						IF ringToneStarted = FALSE
							IF GET_CUTSCENE_TIME() > 12959
								PLAY_PED_RINGTONE(g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, PLAYER_PED_ID(), false)
								PRINTSTRING("PLAY_PED_RINGTONE NOW") PRINTNL()
								ringToneStarted = TRUE
							ENDIF
						ENDIF
						//Stop the ringtone for the phone 
						IF ringToneStopped = FALSE
							IF IS_PED_RINGTONE_PLAYING(PLAYER_PED_ID())
							AND GET_CUTSCENE_TIME() > 15953
								STOP_PED_RINGTONE(PLAYER_PED_ID())
								PRINTSTRING("STOP_PED_RINGTONE NOW") PRINTNL()
								ringToneStopped = TRUE
							ENDIF
						ENDIF		
					ENDIF
					
					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
						cutsceneSkipped = TRUE
					ENDIF
				ENDIF
			
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Crashed_Plane")
//					IF DOES_ENTITY_EXIST(vehTitan)
//						IF IS_VEHICLE_DRIVEABLE(vehTitan)
//							CONTROL_LANDING_GEAR(vehTitan, LGC_RETRACT_INSTANT)
//							SET_ENTITY_COORDS(vehTitan, <<2807.3306, 4794.7900, 46.6197>>)
//							SET_ENTITY_HEADING(vehTitan, 344.2750)
//							SET_VEHICLE_ON_GROUND_PROPERLY(vehTitan)
						IF DOES_ENTITY_EXIST(ObjTitan)
						
							//PRE NG
							//SET_ENTITY_COORDS(ObjTitan, <<2808.3870, 4795.4839, 45.8104>>)
							//SET_ENTITY_ROTATION(ObjTitan, <<-1.2048, -6.1718, -12.3839>>)
							
							//NG TO MATCH CUTSCENE
							SET_ENTITY_COORDS(ObjTitan, <<2807.691895, 4796.448730, 47.668404>>)
							SET_ENTITY_ROTATION(ObjTitan, <<-0.000000, -0.000000, -11.340065>> )
							
							SET_ENTITY_QUATERNION(ObjTitan, -0.0162, -0.0524, -0.1071, 0.9927)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(ObjTitan, TRUE)
//							SET_VEHICLE_DOORS_LOCKED(vehTitan, VEHICLELOCK_LOCKED)
							FREEZE_ENTITY_POSITION(ObjTitan, TRUE)
						ENDIF
//					ENDIF				
				ENDIF
//				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2801.9619, 4800.4131, 46.0215>>)
					SET_ENTITY_ROTATION(PLAYER_PED_ID(), <<0,0, -34.2>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 61.5688)
					IF HAS_ANIM_DICT_LOADED("missmartin1@switch")
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missmartin1@switch", "trevor_switch_trevor")
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "missmartin1@switch", "trevor_switch_trevor", INSTANT_BLEND_IN)
						ENDIF
					ENDIF
//					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 4000, 0) 
//                    FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				ENDIF
//				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Briefcase")
					IF DOES_ENTITY_EXIST(objBriefcase)
						ATTACH_ENTITY_TO_ENTITY(objBriefcase, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.05,0,0>>, <<0, -90, 0>>) 
						IF HAS_ANIM_DICT_LOADED("missmartin1@switch")
							IF NOT IS_ENTITY_PLAYING_ANIM(objBriefcase, "missmartin1@switch", "trevor_switch_case")
								PLAY_ENTITY_ANIM(objBriefcase, "trevor_switch_case", "missmartin1@switch", INSTANT_BLEND_IN, FALSE, TRUE)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objBriefcase)
							ENDIF
						ENDIF					
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Pilot_Martin_1")
				
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned TRUE for Pilot_Martin_1")
				
					IF DOES_ENTITY_EXIST(pedPilot)
						DELETE_PED(pedPilot)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					
					REPLAY_STOP_EVENT()
				
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(80)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3.97)
					
					//For bug 1945901
					IF cutsceneSkipped = TRUE
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3.97)
						PRINTSTRING("MARTIN 1 -A cut skipped game play cam pitch being set to -3.97")
					ENDIF
				ENDIF				
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_phone", PROP_PHONE_ING_02)
					IF DOES_ENTITY_EXIST(objTrevsPhone)
						ATTACH_ENTITY_TO_ENTITY(objTrevsPhone, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), <<0,0,0>>, <<0, 0, 0>>)
					ENDIF
				ENDIF
				
				IF cutsceneSkipped = TRUE
					IF CAN_SET_EXIT_STATE_FOR_CAMERA()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(80)
						//For bug 1945901
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3.97)
						PRINTSTRING("MARTIN 1 -B cut skipped game play cam pitch being set to -3.97")
					ENDIF
				ENDIF
				
				IF NOT IS_CUTSCENE_PLAYING()
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
				ENDIF
				
				IF NOT IS_CUTSCENE_ACTIVE()
					
//					IF DOES_ENTITY_EXIST(pedPilot)
//						DELETE_PED(pedPilot)
//					ENDIF	
					
//					CANCEL_MUSIC_EVENT("SOL1_CRASH")
					
//					WHILE NOT HAS_MODEL_LOADED(PROP_DEST_PLANE)
//						WAIT(0)
//						PRINTSTRING("SOLOMON1_WAITING_63") PRINTNL()
//					ENDWHILE					
					
//					IF NOT DOES_ENTITY_EXIST(ObjCrashedPlane)
//						ObjCrashedPlane = CREATE_OBJECT(PROP_DEST_PLANE, <<2835.90, 4739.27, 43.47>>)
//						SET_ENTITY_ROTATION(ObjCrashedPlane, <<0,0,166.34>>)
//					ENDIF				
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					
//					fpitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					iStageSection ++
				ENDIF					

			BREAK
			
			CASE 2
				IF DOES_ENTITY_EXIST(pedPilot)
					SET_ENTITY_AS_MISSION_ENTITY(pedPilot)
					DELETE_PED(pedPilot)
				ENDIF
//				IF NOT DOES_ENTITY_EXIST(PropBriefcase)
//					PropBriefcase = CREATE_OBJECT(PROP_SECURITY_CASE_01, <<2802, 4979, 48>>)
//				ENDIF
//				ATTACH_ENTITY_TO_ENTITY(PropBriefcase, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.05,0,0>>, <<0, -90, 0>>) 
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				SET_CAM_ACTIVE(StaticCam, FALSE)
//				DESTROY_CAM(StaticCam)
				
//				PRINTSTRING("Fpitch = ") PRINTFLOAT(fpitch) PRINTNL()
//				
//				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
//					fpitch = fpitch + 2
//				ENDIF
//				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1)
//					fpitch = fpitch - 2
//				ENDIF
				
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3.97)
				
				IF DOES_ENTITY_EXIST(objTrevsPhone)
					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missmartin1@switch", "trevor_switch_trevor")
						PRINTSTRING("GET_ENTITY_ANIM_CURRENT_TIME = ") PRINTFLOAT(GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missmartin1@switch", "trevor_switch_trevor")) PRINTNL()
						IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missmartin1@switch", "trevor_switch_trevor") > 0.08
							DELETE_OBJECT(objTrevsPhone)
						ENDIF
					ENDIF
				ENDIF


				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missmartin1@switch", "trevor_switch_trevor")
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missmartin1@switch", "trevor_switch_trevor") > 0.5
//						IF startedLoadScene = TRUE
//							NEW_LOAD_SCENE_STOP()
//							startedLoadScene = FALSE
//						ENDIF
						iStageSection++	
					ENDIF
				ENDIF
			
			BREAK
					
		ENDSWITCH
		
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 3
			stopCamShake(TRUE)
			
			IF bTriggerFX[2] = TRUE
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[1])
					STOP_PARTICLE_FX_LOOPED(ptfxEngine[1])
					bTriggerFX[2] = FALSE
				ENDIF
			ENDIF
			
			iStageSection 		= 0
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_FALLING(VEHICLE_INDEX fallingVehicle)
	
//	PRINTSTRING("vFallingStartCoords.z = ") PRINTFLOAT(vFallingStartCoords.z) PRINTNL()
//	PRINTSTRING("vFallingEndCoords.z = ") PRINTFLOAT(vFallingEndCoords.z) PRINTNL()
	
	IF bStartCoordsGrabbed = FALSE
		vFallingStartCoords = GET_ENTITY_COORDS(fallingVehicle)
		iFallingTimer = GET_GAME_TIMER()
		bStartCoordsGrabbed = TRUE
	ENDIF
	
	IF bEndCoordsGrabbed = FALSE
	AND bStartCoordsGrabbed = TRUE
		IF GET_GAME_TIMER() > (iFallingTimer + 4000)
			vFallingEndCoords = GET_ENTITY_COORDS(fallingVehicle)
			bEndCoordsGrabbed = TRUE
		ENDIF
	ENDIF
			
	IF bEndCoordsGrabbed = TRUE	
		//If the car has dropped more than 10metres in 4 seconds it's falling
		IF vFallingStartCoords.z > (vFallingEndCoords.z + 5)
			RETURN TRUE
		ELSE
//			PRINTSTRING("Flags being reset as vehicle is not falling ") PRINTNL()
			//Reset flags Vehicle is not falling
			bEndCoordsGrabbed = FALSE
			bStartCoordsGrabbed = FALSE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL stageDestroySniper()
	
//	PRINTSTRING("gameplay cam relative heading should be set to ") PRINTFLOAT(GET_GAMEPLAY_CAM_RELATIVE_HEADING()) PRINTNL()
	
	//Anything to be called every frame during this stage
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)  
	ENDIF
	
// Remove PTFX 
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[4])
		REMOVE_PARTICLE_FX(ptfxEngine[4])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[5])
		REMOVE_PARTICLE_FX(ptfxEngine[5])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[6])
		REMOVE_PARTICLE_FX(ptfxEngine[6])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[7])
		REMOVE_PARTICLE_FX(ptfxEngine[7])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[8])
		REMOVE_PARTICLE_FX(ptfxEngine[8])
	ENDIF	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[9])
		REMOVE_PARTICLE_FX(ptfxEngine[9])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEngine[10])
		REMOVE_PARTICLE_FX(ptfxEngine[10])
	ENDIF
	IF NOT DOES_ENTITY_EXIST(ObjEndBarrel)
		ObjEndBarrel = GET_CLOSEST_OBJECT_OF_TYPE(<<1219.4, 1865.5, 78.3278>>, 1, PROP_BARREL_01A)
	ELSE
		IF barrelSetUp = FALSE
			FREEZE_ENTITY_POSITION(ObjEndBarrel, TRUE)
			SET_ENTITY_INVINCIBLE(ObjEndBarrel, TRUE)
			barrelSetUp = TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(SniperVan)
		IF IS_VEHICLE_DRIVEABLE(SniperVan)
			SET_DISABLE_PETROL_DECALS_RECYCLING_THIS_FRAME()
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
				IF vanNeedsToBeExploded = FALSE
					IF vehicleFalling = FALSE
						IF IS_VEHICLE_FALLING(SniperVan)
							PRINTSTRING("vehicleFalling = TRUE") PRINTNL()
							vehicleFalling = TRUE
						ELSE
//							PRINTSTRING("vehicleFalling = FALSE") PRINTNL()
						ENDIF
					ELSE
						IF IS_ENTITY_IN_AIR(SniperVan)
							vanNeedsToBeExploded = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ENTITY_IN_AIR(SniperVan)
						EXPLODE_VEHICLE(SniperVan)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bStageSetup = TRUE
	AND iStageSection < 6
		//Clean up parked car
		IF iStageSection > 3
			IF DOES_ENTITY_EXIST(MichaelsCar)
				IF IS_VEHICLE_DRIVEABLE(MichaelsCar)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), MichaelsCar)
					OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MichaelsCar) > 150
						SET_VEHICLE_AS_NO_LONGER_NEEDED(MichaelsCar)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF iStageSection = 3
			OR iStageSection = 4
				IF NOT DOES_BLIP_EXIST(BlipMartinHouse)
					IF DoneLoseCopsText = TRUE
						CLEAR_PRINTS()
						DoneLoseCopsText = FALSE
					ENDIF
					IF doneSnipe8Text = TRUE
						CLEAR_PRINTS()
					ENDIF
					BlipMartinHouse = CREATE_BLIP_FOR_COORD(<<1370.4, 1174.3, 113.1>>, TRUE)
					IF doneSnipe5Text = FALSE
						PRINT_NOW("SNIPE5", DEFAULT_GOD_TEXT_TIME, 1)//~s~Go to ~y~Martin's house.
						doneSnipe5Text = TRUE
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(BlipSniperVan)
					REMOVE_BLIP(BlipSniperVan)
				ENDIF
			ENDIF
			IF iStageSection = 5
				IF DOES_BLIP_EXIST(BlipMartinHouse)
					REMOVE_BLIP(BlipMartinHouse)
				ENDIF
				IF DOES_BLIP_EXIST(BlipSniperVan)
					REMOVE_BLIP(BlipSniperVan)
				ENDIF
				IF GET_DISTANCE_BETWEEN_COORDS(<<1216.6, 1868.1, 78.1>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 5
					IF NOT DOES_BLIP_EXIST(BlipTrevor)
						IF DoneLoseCopsText = TRUE
							CLEAR_PRINTS()
							DoneLoseCopsText = FALSE
						ENDIF
						BlipTrevor = CREATE_BLIP_FOR_COORD(<<1216.6, 1868.1, 78.1>>, TRUE)
					ELSE
						IF doneSnipe7Text = FALSE
						AND DoneChatChange2 = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								PRINT_NOW("SNIPE7", DEFAULT_GOD_TEXT_TIME, 1)//~s~Meet Trevor at the ~y~cement works.
								doneSnipe7Text = TRUE
							ENDIF
						ENDIF
						//Draw a locate blip			
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1216.6, 1868.1, 78.1>>, <<2.5,2.5,LOCATE_SIZE_HEIGHT>>, TRUE)		
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(BlipMartinHouse)
				REMOVE_BLIP(BlipMartinHouse)
			ENDIF		
			IF DOES_BLIP_EXIST(BlipTrevor)
				REMOVE_BLIP(BlipTrevor)
			ENDIF
			IF DOES_BLIP_EXIST(BlipSniperVan)
				REMOVE_BLIP(BlipSniperVan)
			ENDIF
			IF doneLoseCopsText = FALSE
				PRINT_NOW("M1_GOD3", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose the cops.
				doneLoseCopsText = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Update the replay checkpoint vehicle to be the players car if the player gets in the vehicle gen one
	IF CheckPointVehOveridden = FALSE
		IF DOES_ENTITY_EXIST(get_vehicle_gen_vehicle_index(vehgen_mission_veh))
			PRINTSTRING("overriding replay checkpoint vehicle with that of the car gen saved from Michaels car earlier") PRINTNL()
			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(get_vehicle_gen_vehicle_index(vehgen_mission_veh))
			CheckPointVehOveridden = TRUE
		ELSE
//			PRINTSTRING("vehgen_mission_veh doesn't exist") PRINTNL()
		ENDIF
	ENDIF
	
	IF bStageSetup = FALSE
		
		PRINTSTRING("MISSION STAGE DESTROY SNIPER") PRINTNL()
		
		missionCanFail 			= FALSE
		REMOVE_PTFX_ASSET()
		//Any flags that need set for this stage
		EndCutRequested 		= FALSE
		doneSnipe8Text 			= FALSE
		doneLoseCopsText 		= FALSE
		doneSnipe7Text 			= FALSE
		doneSnipe5Text 			= FALSE
		DoneChatChange2 		= FALSE
		doneChatWreck 			= FALSE
		doneWreckHelpText 		= FALSE
		HintCamActivated 		= FALSE
//		pourGasTaskGiven 		= FALSE
//		weaponSwapped 			= FALSE
//		createdGasTrail 		= FALSE
		TaskToLeaveCar 			= FALSE
		barrelSetUp 			= FALSE
		vanNeedsToBeExploded 	= FALSE
		vehicleFalling 			= FALSE
		doneGetBackInVanText	= FALSE
		blipSetToRed			= FALSE
		RadarBack 				= FALSE
		
		//Create the parked car for Michaels escape
		IF NOT IS_VEHICLE_GEN_AVAILABLE(VEHGEN_MISSION_VEH)
			IF NOT DOES_ENTITY_EXIST(MichaelsCar)
				IF HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR))
					WHILE NOT CREATE_PLAYER_VEHICLE(MichaelsCar, CHAR_MICHAEL, <<-390.9780, 1189.5386, 324.6420>>, 102.4179, TRUE, VEHICLE_TYPE_CAR)
						PRINTSTRING("waiting for players car to be created") PRINTNL()
						WAIT(0)
					ENDWHILE
					SET_VEHICLE_ON_GROUND_PROPERLY(MichaelsCar)
					SET_ENTITY_LOAD_COLLISION_FLAG(MichaelsCar, TRUE)
	//				SET_MODEL_AS_NO_LONGER_NEEDED(PREMIER)
				ENDIF
			ENDIF
		ELSE
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR))
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(SniperVan)
			REQUEST_MODEL(BURRITO3)
			REQUEST_MODEL(P_RCSS_FOLDED)
			IF HAS_MODEL_LOADED(BURRITO3)
			AND HAS_MODEL_LOADED(P_RCSS_FOLDED)
				SniperVan = CREATE_VEHICLE(BURRITO3, <<-363.5392, 1142.2645, 323.1035>>, 11.1408)
				SET_ENTITY_LOAD_COLLISION_FLAG(SniperVan, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(SniperVan)
//				SET_VEHICLE_UNDRIVEABLE(SniperVan, TRUE)
//				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(SniperVan, FALSE)
				SET_VEHICLE_COLOURS(SniperVan, 0,0)
				SET_VEHICLE_DIRT_LEVEL(SniperVan, 10)
				SET_VEHICLE_DISABLE_TOWING(SniperVan, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO3)
				RemoteSniper = CREATE_OBJECT(P_RCSS_FOLDED, <<-363.5190, 1142.1714, 323.0888>>)
				ATTACH_ENTITY_TO_ENTITY(RemoteSniper, SniperVan, 0, << 0, -1.2, -0.3 >>, <<0,0,0>>)
				SET_MODEL_AS_NO_LONGER_NEEDED(P_RCSS_FOLDED)
				SET_ENTITY_HEALTH(SniperVan, 310)
//				SET_VEHICLE_ENGINE_HEALTH(SniperVan, 310)
//				SET_VEHICLE_PETROL_TANK_HEALTH(SniperVan, 310)
			ENDIF
		ENDIF
		
		//Set up Michael
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF DOES_ENTITY_EXIST(SniperVan)
					WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, SniperVan) //<<-366.2999, 1147.5693, 323.8548>>, 211.2487)
						PRINTSTRING("Waiting for Michael to be created") PRINTNL()
						WAIT(0)
					ENDWHILE
					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_PETROLCAN, 1000, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN, 1000, TRUE, TRUE)
//			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//					RESTORE_PLAYER_PED_VARIATIONS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])	
//				ENDIF
//			ENDIF
//			RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(SniperVan)
			AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), SniperVan)
			ENDIF
		ENDIF
		
		CLEAR_HELP()			
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
		SET_MAX_WANTED_LEVEL(0)
		
		//re-enable all the police for the chase section
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE, TRUE)
	    ALLOW_DISPATCH(DT_POLICE_HELICOPTER, TRUE)
	    ALLOW_DISPATCH(DT_POLICE_VEHICLE_REQUEST, TRUE)
	    ALLOW_DISPATCH(DT_POLICE_ROAD_BLOCK, TRUE)
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
	    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
	    ALLOW_DISPATCH(DT_SWAT_AUTOMOBILE, TRUE)
	    ALLOW_DISPATCH(DT_SWAT_HELICOPTER, TRUE)		
			
		//Set up a scenario blocking area for the barn area to stop peds spawning in 
		BarnBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<1288, 1962.8, 128.4>>, <<1139.2, 1800.8, 59.5>>)
		SeatNextToVanBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<-358.3, 1130.6, 333.3>>, <<-323.6, 1170.2, 319.6>>)	
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		iStageSection 		= 0
		missionCanFail 		= TRUE
		bStageSetup 		= TRUE
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP()
			
			IF DOES_ENTITY_EXIST(SniperVan)
				IF IS_VEHICLE_DRIVEABLE(SniperVan)
					CLEAR_AREA(GET_ENTITY_COORDS(SniperVan), 20, TRUE)
				ENDIF
			ENDIF
//			//Create gas trail
//			START_PETROL_TRAIL_DECALS(1)
//				ADD_PETROL_TRAIL_DECAL_INFO(<<-369.6, 1150.6, 324.3>>, 1)
//				WAIT(0)
//				ADD_PETROL_TRAIL_DECAL_INFO(<<-368.8, 1149.7, 324.2>>, 1)
//				WAIT(0)
//				ADD_PETROL_TRAIL_DECAL_INFO(<<-367.8, 1148.6, 324>>, 1)
//				WAIT(0)
//				ADD_PETROL_TRAIL_DECAL_INFO(<<-366.7, 1147.5, 323.9>>, 1)
//				WAIT(0)
//				ADD_PETROL_TRAIL_DECAL_INFO(<<-365.4, 1146.4, 323.7>>, 1)
//				WAIT(0)
//				ADD_PETROL_TRAIL_DECAL_INFO(<<-364.2, 1145.0, 323.6>>, 1)
//				WAIT(0)
//				ADD_PETROL_TRAIL_DECAL_INFO(<<-363.7, 1143.3, 323.4>>, 2)
//				WAIT(0)
//				ADD_PETROL_TRAIL_DECAL_INFO(<<-363, 1141, 323>>, 4)
//			END_PETROL_TRAIL_DECALS()
		ENDIF	
		
	ELSE
		IF iStageSection = 0
			//Fade the screen in now for skipping
			IF NOT IS_SCREEN_FADED_IN()
			AND bMissionFailed = FALSE
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)			
			ENDIF
//			IF NOT IS_SCREEN_FADED_IN()
//				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//			ENDIF
			iStageSection = 1
		ENDIF
		
		//Switch to Michael once chat is done
		IF iStageSection = 1
		
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							IF NOT sCamDetails.bRun
								IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
									CLEAR_PRINTS()
									TRIGGER_MUSIC_EVENT("SOL1_END")
									//Restore the players to their start variations now for remainder of mission
//									RESTORE_PLAYER_PED_VARIATIONS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])	
//									RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
									sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
									sCamDetails.bRun  = TRUE
								ENDIF
							ELSE
								WHILE RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails, 0, 0, 0)	// Returns FALSE when the camera spline is complete
									
//									SET_DISABLE_PETROL_DECALS_RECYCLING_THIS_FRAME()
									
									PRINTSTRING("SET_GAMEPLAY_CAM_RELATIVE_HEADING = ") PRINTFLOAT(GET_GAMEPLAY_CAM_RELATIVE_HEADING()) PRINTNL()
									PRINTSTRING("SET_GAMEPLAY_CAM_RELATIVE_PITCH = ") PRINTFLOAT(GET_GAMEPLAY_CAM_RELATIVE_PITCH()) PRINTNL()
										
									SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
									SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)																	
																	
									CLEAR_PRINTS()
									IF sCamDetails.bOKToSwitchPed
										IF NOT sCamDetails.bPedSwitched
											IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)						
												
												sCamDetails.bPedSwitched = TRUE
												
											ENDIF
										ENDIF
									ENDIF
									
//									IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
//										PRINTSTRING("GET_PLAYER_SWITCH_JUMP_CUT_INDEX = ") PRINTINT(GET_PLAYER_SWITCH_JUMP_CUT_INDEX()) PRINTNL()
//									ENDIF
									
									//Start the task for pouring petrol
								    IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
								    AND GET_PLAYER_SWITCH_JUMP_CUT_INDEX() = 0
										
//										//Create gas trail
//										IF createdGasTrail = FALSE
//											START_PETROL_TRAIL_DECALS(1)
//												ADD_PETROL_TRAIL_DECAL_INFO(<<-369.6, 1150.6, 324.3>>, 1)
//												WAIT(0)
//												ADD_PETROL_TRAIL_DECAL_INFO(<<-368.8, 1149.7, 324.2>>, 1)
//												WAIT(0)
//												ADD_PETROL_TRAIL_DECAL_INFO(<<-367.8, 1148.6, 324>>, 1)
//												WAIT(0)
//												ADD_PETROL_TRAIL_DECAL_INFO(<<-366.7, 1147.5, 323.9>>, 1)
//												WAIT(0)
//												ADD_PETROL_TRAIL_DECAL_INFO(<<-365.4, 1146.4, 323.7>>, 1)
//												WAIT(0)
//												ADD_PETROL_TRAIL_DECAL_INFO(<<-364.2, 1145.0, 323.6>>, 1)
//												WAIT(0)
//												ADD_PETROL_TRAIL_DECAL_INFO(<<-363.7, 1143.3, 323.4>>, 2)
//												WAIT(0)
//												ADD_PETROL_TRAIL_DECAL_INFO(<<-363, 1141, 323>>, 4)
//											END_PETROL_TRAIL_DECALS()
//											createdGasTrail = TRUE
//										ENDIF
										
//								    	//do what you need
//										IF pourGasTaskGiven = FALSE	
//											IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//												OPEN_SEQUENCE_TASK(seqSequence)
//													TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-370.3, 1151.7, 325.4>>, <<-365.1, 1146.1, 323.8>>, PEDMOVEBLENDRATIO_WALK, TRUE, 0.5, 4, FALSE, ENAV_DEFAULT, TRUE, FIRING_TYPE_CONTINUOUS)
//													TASK_ACHIEVE_HEADING(NULL, 218.6255)
//												CLOSE_SEQUENCE_TASK(seqSequence)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seqSequence)
//												CLEAR_SEQUENCE_TASK(seqSequence)
//												pourGasTaskGiven = TRUE
//											ENDIF
//											IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//												OPEN_SEQUENCE_TASK(seqSequence)
//													TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-370.3, 1151.7, 325.4>>, <<-365.1, 1146.1, 323.8>>, PEDMOVEBLENDRATIO_WALK, TRUE, 0.5, 4, FALSE, ENAV_DEFAULT, TRUE, FIRING_TYPE_CONTINUOUS)
//													TASK_ACHIEVE_HEADING(NULL, 218.6255)
//												CLOSE_SEQUENCE_TASK(seqSequence)
//												TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqSequence)
//												CLEAR_SEQUENCE_TASK(seqSequence)
//												pourGasTaskGiven = TRUE
//											ENDIF												
//										ENDIF
										
//										IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "BB_MAR_VAN")
//											IF IS_VEHICLE_DRIVEABLE(SniperVan)
//												IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SniperVan)
//													START_PLAYBACK_RECORDED_VEHICLE(SniperVan, 1, "BB_MAR_VAN")
//													CLEAR_AREA(GET_ENTITY_COORDS(SniperVan), 20, TRUE)
//								    			ENDIF
//											ENDIF
//										ENDIF
									ENDIF
									
									WAIT(0)
								ENDWHILE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
					
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					FREEZE_ENTITY_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
				ENDIF
				IF bCamViewModeChanged = TRUE
					// url:bugstar:2068528
					IF NOT IS_ALLOWED_INDEPENDENT_CAMERA_MODES()
						SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE, CAM_VIEW_MODE_FIRST_PERSON)
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring cam view mode for first person in vehicle camera.")
					ENDIF
					SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_FIRST_PERSON)
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring cam view mode for first person on foot camera.")
					bCamViewModeChanged = FALSE
				ENDIF
				IF NOT IS_SELECTOR_CAM_ACTIVE()
//					IF IS_VEHICLE_DRIVEABLE(SniperVan)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SniperVan)
//							STOP_PLAYBACK_RECORDED_VEHICLE(SniperVan)
//						ENDIF
//					ENDIF
//					REMOVE_VEHICLE_RECORDING(1, "BB_MAR_VAN")
					IF DOES_ENTITY_EXIST(vehTitan)
						IF IS_VEHICLE_DRIVEABLE(vehTitan)
							FREEZE_ENTITY_POSITION(vehTitan, TRUE)
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(vehBike)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBike)
					ENDIF	
					IF DOES_ENTITY_EXIST(endGuy[0])
						SET_PED_AS_NO_LONGER_NEEDED(endGuy[0])
					ENDIF	
					IF DOES_ENTITY_EXIST(endGuy[1])
						SET_PED_AS_NO_LONGER_NEEDED(endGuy[1])
					ENDIF
					IF DOES_ENTITY_EXIST(objBriefcase)
						IF IS_ENTITY_ATTACHED(objBriefcase)
							DETACH_ENTITY(objBriefcase)
						ENDIF
						DELETE_OBJECT(objBriefcase)
					ENDIF
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					ENDIF
					
//					SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN, 0)
					
//					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_DEST_PLANE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					IF NOT DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_PISTOL)
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 100, FALSE, FALSE)
					ELSE
						IF GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), AMMOTYPE_PISTOL) < 100
							SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 100)
						ENDIF
					ENDIF
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_DESTROY_SNIPER", TRUE)
					IF NOT IS_AUDIO_SCENE_ACTIVE("MARTIN_1_DRIVE_BACK")
						START_AUDIO_SCENE("MARTIN_1_DRIVE_BACK")
					ENDIF
					SETTIMERB(0)
					iStageSection = 2
				ENDIF
			ENDIF
		
		ENDIF
			
		IF iStageSection = 2		
			
//			PRINTSTRING("gameplay cam pitch is ") PRINTFLOAT(GET_GAMEPLAY_CAM_RELATIVE_PITCH()) PRINTNL()
			
//			IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_PETROLCAN
//				IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN) > 0
//					SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN, 0)
//				ENDIF
//				IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN) = 0
//					IF weaponSwapped = FALSE
//						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
//							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), TRUE)
//							weaponSwapped = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			IF RadarBack = FALSE
				IF TIMERB() > 2000
					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					RadarBack = TRUE
				ENDIF
			ENDIF
			
			
			
					
//					IF GET_NUMBER_OF_FIRES_IN_RANGE(<<-363.3, 1141.4, 323>>, 2) > 0
//						FREEZE_ENTITY_POSITION(SniperVan, FALSE)
//						EXPLODE_VEHICLE(SniperVan, TRUE, TRUE)
//					ENDIF
//					IF NOT DOES_BLIP_EXIST(BlipSniperVan)
//						SET_VEHICLE_HANDBRAKE(SniperVan, FALSE)
//						SET_ENTITY_HEALTH(SniperVan, 110)
//						BlipSniperVan = CREATE_BLIP_FOR_VEHICLE(SniperVan, TRUE)
//					ENDIF
			IF DOES_ENTITY_EXIST(SniperVan)	
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(SniperVan, FALSE), <<-428, 1111, 327>>) < 250
					IF IS_VEHICLE_DRIVEABLE(SniperVan)
						blipSetToRed = FALSE
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SniperVan)
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
								PRINT_NOW("M1_GOD5", 200000, -1)//Leave the area
							ENDIF
							IF DOES_BLIP_EXIST(BlipSniperVan)
								CLEAR_PRINTS()
								REMOVE_BLIP(BlipSniperVan)
							ENDIF
						ELSE
							IF NOT DOES_BLIP_EXIST(BlipSniperVan)
								BlipSniperVan = CREATE_BLIP_FOR_VEHICLE(SniperVan)
								CLEAR_PRINTS()
								IF doneGetBackInVanText = FALSE
									PRINT_NOW("SNIPE9", DEFAULT_GOD_TEXT_TIME, -1)//~s~Get back in the ~b~van.
									doneGetBackInVanText = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SetMissionFailed("M1_FAIL6") //~s~Michael destroyed the van too close to the observatory.
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(SniperVan)
						IF blipSetToRed = FALSE
							IF DOES_BLIP_EXIST(BlipSniperVan)
								REMOVE_BLIP(BlipSniperVan)
							ENDIF
							IF NOT DOES_BLIP_EXIST(BlipSniperVan)
								BlipSniperVan = CREATE_BLIP_FOR_VEHICLE(SniperVan, TRUE)
								CLEAR_PRINTS()
								PRINT_NOW("M1_GOD4", 200000, -1)//~s~Destroy the ~r~van.
							ENDIF
							blipSetToRed = TRUE
						ENDIF
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							PRINT_NOW("M1_GOD4", 200000, -1)//~s~Destroy the ~r~van.
						ENDIF
					ENDIF
					IF bMissionFailed = FALSE
						IF IS_ENTITY_DEAD(SniperVan)
							IF DOES_BLIP_EXIST(BlipSniperVan)
								REMOVE_BLIP(BlipSniperVan)
							ENDIF
							SETTIMERA(0)
							CLEAR_PRINTS()
							bVideoRecordingDone = FALSE
							iStageSection = 3
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF iStageSection = 3
			
			//Have a video recording of the van being destroyed
			IF bVideoRecordingDone = FALSE
				IF Timera() > 3000
					REPLAY_RECORD_BACK_FOR_TIME(10.0)
					bVideoRecordingDone = TRUE
				ENDIF
			ENDIF
			
			//Make the phone call if player gets too close to Martins house or if the timer runs out
			IF TIMERA() > 30000
			OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1373, 1147, 113>>) < 1000
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(structConvoPeds, CHAR_TREVOR, "SLM1AUD", "M1_CHANGE", CONV_PRIORITY_VERY_HIGH)
						//What?
						//There's been a change of plans, you don't need to come to the Ranch,
						//meet me at Stoner Cement Works, a little up Senora Road from there. 
						//Trevor, what're you doing?
						//Nothing you wouldn't do. 
						iStageSection = 4
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
		
		IF iStageSection = 4

			IF HAS_CELLPHONE_CALL_FINISHED()

				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				ENDIF
				
//				iLeftDoor = HASH("LeftFarmDoor")
//				iRightDoor = HASH("RightFarmDoor")
//
//				//Register the door
//				ADD_DOOR_TO_SYSTEM(iLeftDoor, prop_gate_farm_03, <<2328.16, 4926.86, 40.95>>)
//				ADD_DOOR_TO_SYSTEM(iRightDoor, prop_gate_farm_03, <<2322.92, 4931.90, 40.71>>)			
//				
//				//set door state
//				DOOR_SYSTEM_SET_OPEN_RATIO(iLeftDoor, 1, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(iLeftDoor, DOORSTATE_LOCKED, FALSE, TRUE)
//				DOOR_SYSTEM_SET_OPEN_RATIO(iRightDoor, 1, FALSE, TRUE)
//				DOOR_SYSTEM_SET_DOOR_STATE(iRightDoor, DOORSTATE_LOCKED, FALSE, TRUE)
				
				SET_MAX_WANTED_LEVEL(5)
				
				SETTIMERB(0)
				iStageSection = 5		
			ENDIF
		
		ENDIF
		
		IF iStageSection = 5

			IF EndCutRequested = FALSE
				IF GET_DISTANCE_BETWEEN_COORDS(<<1216.6, 1868.1, 78.1>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < DEFAULT_CUTSCENE_LOAD_DIST
//					//To stop the fork lift spawning
//					CLEAR_AREA(<<2305.2, 4883.4, 41.8>>, 8, TRUE)
//					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<2299.7, 4870.2, 39>>, <<2307.0, 4894.2, 45>>, FALSE)
					REQUEST_CUTSCENE("Martin_1_ext")
					EndCutRequested = TRUE
				ENDIF
			ENDIF
			IF EndCutRequested = TRUE
				//Set up Trev's clothes for opening cutscene
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					//Trevor
					IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
						SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
//						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_TORSO, 0, 4)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_LEG, 18, 0)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAND, 0, 0)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_FEET, 16, 0)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_DECL, 0, 0)	
						PRINTSTRING("Player was Michael at start so Trev's components being set") PRINTNL()
					ELSE
						SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
						PRINTSTRING("Player was trevor at start so no components being set") PRINTNL()
					ENDIF
					
					//Michael
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					PRINTSTRING("TRIGGER SCENE - Trev's cutscene components set 3.") PRINTNL()
				ENDIF
				IF GET_DISTANCE_BETWEEN_COORDS(<<1204.7, 1859, 78.2>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) > DEFAULT_CUTSCENE_UNLOAD_DIST
					REMOVE_CUTSCENE()
					EndCutRequested = FALSE
				ENDIF	
			ENDIF

			IF DoneChatChange2 = FALSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "M1_CHANGE2", CONV_PRIORITY_MEDIUM)
							//Trevor! Trevor!!!
							//I wouldn't do... somehow I don't believe that!
							DoneChatChange2 = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Do some dialogue and help text and hint cam if Michael comes near the wrecked plane.
			IF DOES_ENTITY_EXIST(vehTitan)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTitan) < 160
					IF doneChatWreck = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "M1_WRECK", CONV_PRIORITY_MEDIUM)
									//Fuck sake, what a mess. How do I get myself involved in this shit.
									doneChatWreck = TRUE
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
					//Help text and activate hint cam
					IF doneWreckHelpText = FALSE
						PRINT_HELP("BFHELPTXT2")//Press ~INPUT_VEH_CIN_CAM~ to toggle focus onto the plane.
						doneWreckHelpText = TRUE
					ENDIF
					IF HintCamActivated = FALSE
						SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
						HintCamActivated = TRUE
					ENDIF
					IF HintCamActivated = TRUE
						CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehTitan)
					ENDIF
					IF IS_GAMEPLAY_HINT_ACTIVE()
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP(TRUE)
						ENDIF
					ENDIF
				ELSE
					IF HintCamActivated = TRUE
						CLEAR_HELP()
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
						HintCamActivated = FALSE
					ENDIF
				ENDIF
			ENDIF
				
			//Make player leave vehicle once close enough
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
//				IF GET_DISTANCE_BETWEEN_COORDS(<<1204.7, 1859, 78.2>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 6
				IF GET_DISTANCE_BETWEEN_COORDS(<<1216.6, 1868.1, 78.1>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 5
					IF DOES_BLIP_EXIST(BlipTrevor)
						REMOVE_BLIP(BlipTrevor)
					ENDIF	
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						PlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(PlayerCar)
						REQUEST_ANIM_DICT("missmartin1@leadinoutmartin_1_ext")
						SETTIMERB(0)
						iStageSection = 6
					ELSE
						SETTIMERB(0)
						REQUEST_ANIM_DICT("missmartin1@leadinoutmartin_1_ext")
						iStageSection = 6
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF iStageSection = 6
			IF DOES_BLIP_EXIST(BlipTrevor)
				REMOVE_BLIP(BlipTrevor)
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF TaskToLeaveCar = FALSE
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						SETTIMERB(0)
						TaskToLeaveCar = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF TIMERB() > 1500
					IF IS_AUDIO_SCENE_ACTIVE("MARTIN_1_DRIVE_BACK")
						STOP_AUDIO_SCENE("MARTIN_1_DRIVE_BACK")
					ENDIF
					iStageSection = 7
				ENDIF
			ENDIF
		ENDIF	
		
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 7
			
			IF DOES_BLIP_EXIST(BlipTrevor)
				REMOVE_BLIP(BlipTrevor)
			ENDIF			
			
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF		
		
	ENDIF

	RETURN FALSE
ENDFUNC

//PURPOSE:End cutscene
FUNC BOOL StageEndCutscene()

	IF bStageSetup = FALSE
		
		SET_MAX_WANTED_LEVEL(0)
		
		PRINTSTRING("MISSION STAGE END CUTSCENE") PRINTNL()
		//Any flags that need set for this stage
		VanMoved 				= FALSE
		missionCanFail 			= FALSE
		hasNumberPlateBeenSet 	= FALSE
		camsKilled 				= FALSE
		
		REQUEST_VEHICLE_RECORDING(0, "END_MARTIN_1")
		
		CLEAR_HELP()
		KILL_ANY_CONVERSATION()
		
		IF DOES_CAM_EXIST(camInit)
			DESTROY_CAM(camInit)
		ENDIF
		IF DOES_CAM_EXIST(camDest)
			DESTROY_CAM(camDest)
		ENDIF
		
		IF NOT DOES_CAM_EXIST(camInit)
			camInit = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1216.503052,1864.997559,79.242203>>,<<1.266527,0.000000,-89.015869>>,45.031136)
			SET_CAM_ACTIVE(camInit, TRUE)
		ENDIF
		IF NOT DOES_CAM_EXIST(camDest)
			camDest = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1217.515137,1867.192505,79.242203>>,<<16.743389,0.000000,-57.137970>>,45.031136)
			SET_CAM_ACTIVE(camDest, TRUE)
		ENDIF		
		
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP()
			REQUEST_CUTSCENE("Martin_1_ext")
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)		
		
		WHILE NOT HAS_CUTSCENE_LOADED()
		OR NOT HAS_ANIM_DICT_LOADED("missmartin1@leadinoutmartin_1_ext")
			//Set up Trevors clothes for cutscene
			
		
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				//Trevor
				IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL
					PRINTSTRING("MARTIN 1 SCRIPT - Trev's cutscene components set.") PRINTNL()
					SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_TORSO, 0, 4, PLAYER_TWO)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_LEG, 18, 0, PLAYER_TWO)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_HAND, 0, 0, PLAYER_TWO)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_FEET, 16, 0, PLAYER_TWO)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_SPECIAL, 0, 0, PLAYER_TWO)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_SPECIAL2, 0, 0, PLAYER_TWO)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_DECL, 0, 0, PLAYER_TWO)	
				ELSE
					SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
				ENDIF
				
				//Michael
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
			ENDIF
			PRINTSTRING("Waiting for end cutscene and anim dict to load") PRINTNL()
			WAIT(0)
		ENDWHILE		
		
		iStageSection = 0
		missionCanFail 		= TRUE
		bStageSetup 		= TRUE
	ELSE
		
		IF iStageSection = 0
			
			//Delete the sniper van incase it's been brough to end cutscene area for bug 1712405
			IF DOES_ENTITY_EXIST(SniperVan)
				DELETE_VEHICLE(SniperVan)
			ENDIF
			IF DOES_ENTITY_EXIST(RemoteSniper)
				DELETE_OBJECT(RemoteSniper)
			ENDIF			
			
			IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
			ENDIF			
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 10000)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SETTIMERA(0)
			
			CLEAR_AREA(<<1215.97400, 1863.09082, 77.88766>>, 14, FALSE)
			CLEAR_AREA_OF_PROJECTILES(<<1219.4, 1865.7, 78.1>>, 15, FALSE)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SETTIMERB(0)
			
			iStageSection = 1
			
		ENDIF
		
		IF iStageSection = 1		
			
			IF TIMERB() < 5000
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<1219.4, 1865.5, 78.3278>>, 3, PROP_BARREL_01A)
					BarrellSyncScene = CREATE_SYNCHRONIZED_SCENE_AT_MAP_OBJECT(<<1219.4, 1865.5, 78.3278>>, 1, PROP_BARREL_01A)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), BarrellSyncScene, "missmartin1@leadinoutmartin_1_ext", "martin_1_ext_leadin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					iStageSection = 2
				ELSE
					PRINTSTRING("Barrel doesn't exist at coords for Sync scene") PRINTNL()
				ENDIF
			ELSE
				iStageSection = 2
			ENDIF
			
		ENDIF
		
		IF iStageSection = 2
		
			//Have a line of dialogue in here from Michael
			IF CREATE_CONVERSATION(structConvoPeds, "SLM1AUD", "M1_EXT_LI", CONV_PRIORITY_MEDIUM)
				//Where is he?
			ENDIF
			iStageSection = 3
			
		ENDIF
		
		IF iStageSection = 3
		
			IF TIMERA() > 9500
				
				CLEAR_AREA(<<1214.0551, 1875.0118, 77.7712>>, 15, FALSE)
			
				//move the players car
				IF DOES_ENTITY_EXIST(PlayerCar)
					IF IS_VEHICLE_DRIVEABLE(PlayerCar)
						IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(PlayerCar))
							SET_ENTITY_COORDS(PlayerCar, <<1211.0487, 1859.6018, 77.9089>>)
							SET_ENTITY_HEADING(PlayerCar, 311.1039)							
						ELSE
							DELETE_VEHICLE(PlayerCar)
						ENDIF
					ENDIF
				ENDIF
				iStageSection = 4
			ENDIF
		
		ENDIF
		
		IF iStageSection = 4

			IF DOES_ENTITY_EXIST(TrevCutsceneCar)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(TrevCutsceneCar)
			ENDIF

			//Register entities
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF

			REGISTER_ENTITY_FOR_CUTSCENE(TrevsEntity, "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_TWO)
			
			REGISTER_ENTITY_FOR_CUTSCENE(CutPatricia, "Patricia", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, CS_Patricia)
			
			REGISTER_ENTITY_FOR_CUTSCENE(TrevsCarEntity, "Madrazos_Car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, BALLER)
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
	      	//Configure script systems into a safe mode.
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
			
			START_CUTSCENE()
			SET_VEHICLE_MODEL_PLAYER_WILL_EXIT_SCENE(BALLER)
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			iStageSection = 5
			
		ENDIF
		
		IF iStageSection = 5
		
			// Let the ambient script know that it should play the carrec when this cutscene ends
			// Carrec is 000, END_MARTIN_1, and will trigger in player_controller.sc
//			g_bHandleEndOfMission_Martin1 = TRUE
		
			
			IF camsKilled = FALSE
				IF IS_CUTSCENE_PLAYING()
					SET_CAM_ACTIVE(camInit, FALSE)
					SET_CAM_ACTIVE(camDest, FALSE)
					DESTROY_CAM(camInit)
					DESTROY_CAM(camDest)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					camsKilled = TRUE
				ENDIF
			ENDIF
			
			IF VanMoved = FALSE
				IF IS_CUTSCENE_PLAYING()
					IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//						SET_ENTITY_COORDS(PlayerCar, <<2303.1599, 4882.6382, 40.8083>>)
//						SET_ENTITY_HEADING(PlayerCar, 315.2404)
						SET_VEHICLE_DOORS_SHUT(PlayerCar)
						VanMoved = TRUE
					ENDIF
				ENDIF
			ENDIF
			//Set Exits states
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				IF DOES_ENTITY_EXIST(TrevCutsceneCar)
					IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), TrevCutsceneCar)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), TrevCutsceneCar, VS_BACK_RIGHT)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
//				SET_BUILDING_STATE(BUILDINGNAME_IPL_MARTIN1_ROAD_NODES, BUILDINGSTATE_DESTROYED) 
				TrevsEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor", PLAYER_TWO)
				CSTrev = GET_PED_INDEX_FROM_ENTITY_INDEX(TrevsEntity)
				IF DOES_ENTITY_EXIST(CSTrev)
					IF NOT IS_PED_INJURED(CSTrev)
						IF DOES_ENTITY_EXIST(TrevCutsceneCar)
							IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
								SET_PED_INTO_VEHICLE(CSTrev, TrevCutsceneCar)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Patricia")
				CutPatricia = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Patricia", CS_Patricia)
				PedPatricia = GET_PED_INDEX_FROM_ENTITY_INDEX(CutPatricia)
				IF DOES_ENTITY_EXIST(TrevCutsceneCar)
					IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)				
						IF NOT IS_PED_IN_VEHICLE(PedPatricia, TrevCutsceneCar)				
							SET_PED_INTO_VEHICLE(PedPatricia, TrevCutsceneCar, VS_FRONT_RIGHT)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedPatricia, TRUE)
							SET_PED_KEEP_TASK(PedPatricia, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Grab Martin's car for setting up number plate and for exit states.
			IF NOT DOES_ENTITY_EXIST(TrevsCarEntity)
				TrevsCarEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Madrazos_Car", BALLER)
			ELSE
				IF NOT DOES_ENTITY_EXIST(TrevCutsceneCar)
					TrevCutsceneCar = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(TrevsCarEntity)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
						IF hasNumberPlateBeenSet = FALSE
							SET_VEHICLE_NUMBER_PLATE_TEXT(TrevCutsceneCar, "MADRAZO")
							hasNumberPlateBeenSet = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Madrazos_Car")
				IF DOES_ENTITY_EXIST(TrevCutsceneCar)
					IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(TrevCutsceneCar, TRUE, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Madrazos_Car")
				IF DOES_ENTITY_EXIST(TrevCutsceneCar)
					IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
						SET_VEHICLE_DOORS_SHUT(TrevCutsceneCar)
//						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(TrevCutsceneCar, TRUE, TRUE, TRUE)
						START_PLAYBACK_RECORDED_VEHICLE(TrevCutsceneCar, 0, "END_MARTIN_1")
						SET_ADDITIONAL_ROTATION_FOR_RECORDED_VEHICLE_PLAYBACK(TrevCutsceneCar, <<-0.189057499, -0.189057499, 0.189057499>>, EULER_XYZ)
						PRINTSTRING("Martin 1 : Martin's car starting playback now") PRINTNL()
					ELSE
						PRINTSTRING("Martin 1 : car isn't driveable") PRINTNL()
					ENDIF
				ELSE
					PRINTSTRING("Martin 1 : Car doesn't exist") PRINTNL()
				ENDIF
			ELSE	
				PRINTSTRING("Martin 1 : Can't set exit state for car") PRINTNL()
			ENDIF
			
//			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			ENDIF			
			
			IF NOT IS_CUTSCENE_ACTIVE()
//				SET_BUILDING_STATE(BUILDINGNAME_IPL_MARTIN1_ROAD_NODES, BUILDINGSTATE_DESTROYED) 
				IF DOES_ENTITY_EXIST(TrevCutsceneCar)
					IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TrevCutsceneCar)
							START_PLAYBACK_RECORDED_VEHICLE(TrevCutsceneCar, 0, "END_MARTIN_1")
							SET_ADDITIONAL_ROTATION_FOR_RECORDED_VEHICLE_PLAYBACK(TrevCutsceneCar, <<-0.189057499, -0.189057499, 0.189057499>>, EULER_XYZ)
						ENDIF
					ENDIF
				ENDIF
				
				REPLAY_STOP_EVENT()
				
				//Return script systems to normal.
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)	
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				
				REMOVE_SCENARIO_BLOCKING_AREA(AirstripBlockingArea)	
				REMOVE_SCENARIO_BLOCKING_AREA(CropFieldBlockingArea1)
				REMOVE_SCENARIO_BLOCKING_AREA(PoliceBikeBlockingArea)
				REMOVE_SCENARIO_BLOCKING_AREA(SeatNextToVanBlockingArea)
				REMOVE_SCENARIO_BLOCKING_AREA(MartinHouseMainScenarioBlockingArea)
				REMOVE_SCENARIO_BLOCKING_AREA(PlaneBlockingArea)				
				
				//Display the mission passed screen now
				IF NOT IS_REPEAT_PLAY_ACTIVE()	//B* 1831413: Only hurry stats if this is not a replay
					TRIGGER_MISSION_STATS_UI()
				ENDIF
				iStageSection = 6
			ENDIF
			
		ENDIF
		
		IF iStageSection = 6
		
			IF DOES_ENTITY_EXIST(TrevCutsceneCar)
				IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), TrevCutsceneCar)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), TrevCutsceneCar, VS_BACK_RIGHT)
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(PedPatricia)
						IF NOT IS_PED_INJURED(PedPatricia)
							IF NOT IS_PED_IN_VEHICLE(PedPatricia, TrevCutsceneCar)
								SET_PED_INTO_VEHICLE(PedPatricia, TrevCutsceneCar, VS_FRONT_RIGHT)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedPatricia, TRUE)
								SET_PED_KEEP_TASK(PedPatricia, TRUE)
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(CSTrev)
						IF NOT IS_PED_INJURED(CSTrev)
							IF NOT IS_PED_IN_VEHICLE(CSTrev, TrevCutsceneCar)
								SET_PED_INTO_VEHICLE(CSTrev, TrevCutsceneCar)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CSTrev, TRUE)
							ENDIF
						ENDIF
					ENDIF
					SET_VEHICLE_DOORS_SHUT(TrevCutsceneCar)
					
					IF NOT IS_PED_INJURED(CSTrev)
						IF IS_PED_IN_VEHICLE(CSTrev, TrevCutsceneCar)
//							OPEN_SEQUENCE_TASK(seqSequence)
//								TASK_VEHICLE_DRIVE_TO_COORD(NULL, TrevCutsceneCar, <<1221.5, 1887.5, 77.8>>, 5, DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL(TrevCutsceneCar), DRIVINGMODE_PLOUGHTHROUGH, 2, 10)
////								TASK_VEHICLE_DRIVE_TO_COORD(NULL, TrevCutsceneCar, <<1235.3, 1902.7, 78.1>>, 7, DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL(TrevCutsceneCar), DRIVINGMODE_PLOUGHTHROUGH, 2, 10)
////								TASK_VEHICLE_DRIVE_TO_COORD(NULL, TrevCutsceneCar, <<1256.6, 1930.1, 78.6>>, 10, DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL(TrevCutsceneCar), DRIVINGMODE_PLOUGHTHROUGH, 2, 10)
//								TASK_VEHICLE_DRIVE_TO_COORD(NULL, TrevCutsceneCar, <<1290.8, 1906.4, 83.6>>, 15, DRIVINGSTYLE_ACCURATE, GET_ENTITY_MODEL(TrevCutsceneCar), DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5, 10)
//								
////								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, <<1216.7, 1881.5, 78.2>>, MISSION_GOTO, 5, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 4, 30)
////								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, <<1235.3, 1902.7, 78.1>>, MISSION_GOTO, 7, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5, 20)
////								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, <<1256.6, 1930.1, 78.6>>, MISSION_GOTO, 10, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5, 20)
////								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, <<1290.8, 1906.4, 83.6>>, MISSION_GOTO, 10, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5, 20)
//							CLOSE_SEQUENCE_TASK(seqSequence)
//							TASK_PERFORM_SEQUENCE(CSTrev, seqSequence)
//							CLEAR_SEQUENCE_TASK(seqSequence)
//							SET_PED_KEEP_TASK(CSTrev, TRUE)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TrevCutsceneCar)
								IF GET_TIME_POSITION_IN_RECORDING(TrevCutsceneCar) > 5000
									iStageSection = 7
								ENDIF
							ELSE
								PRINTSTRING("playback is not going on for trevs car ") PRINTNL()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
		
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 7
			
			IF DOES_BLIP_EXIST(BlipTrevor)
				REMOVE_BLIP(BlipTrevor)
			ENDIF			
			
			//Move stage on once the mission passed screen has finished
			IF g_bResultScreenDisplaying = FALSE
				SET_MAX_WANTED_LEVEL(5)
				
				bStageSetup = FALSE
				RETURN TRUE
			ENDIF
		ENDIF		
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

//═════════════════════════════════╡ MAIN SCRIPT ╞═══════════════════════════════════

VEHICLE_INDEX vehBodhi2

SCRIPT
	SET_MISSION_FLAG(TRUE)

	PRINTSTRING("VINEWOOD BABYLON MISSION FLAG BEING SET TO TRUE") PRINTNL()
	
	//Enbale the interior if on repeat play
	IF IS_REPEAT_PLAY_ACTIVE()
		SET_INTERIOR_CAPPED(INTERIOR_V_RANCH, FALSE)
	ENDIF	
	
	//INFORM_MISSION_STATS_OF_MISSION_START_SOLOMON_ONE()
	REGISTER_SCRIPT_WITH_AUDIO()	
	
	//Call this here to give me full control over peds variations in cutscenes
	SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE)
	
	DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, TRUE)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_HELMET(PLAYER_PED_ID(), FALSE)
		ENDIF
	ENDIF
	
	SET_RANDOM_TRAINS(FALSE)
	PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_RANDOM_TRAINS(FALSE).")
	
	SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 20)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		TRIGGER_MUSIC_EVENT("SOL1_FAIL")
		PRINTSTRING("playing fail music for death arrest") PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		MissionCleanup()
	ENDIF				

/*#IF IS_DEBUG_BUILD
	SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_SHOT_SCENE(scsPlaneShotScene, vehTitan)
	SETUP_SPLINE_CAM_NODE_ARRAY_PLANE_TO_BIKE(scsPlaneToBike, vehTitan, vehBike)
	WIDGET_GROUP_ID widgetDebug = START_WIDGET_GROUP("MISSION DEBUG")
	STOP_WIDGET_GROUP()
	CREATE_SPLINE_CAM_WIDGETS(scsPlaneShotScene, "Plane", "", widgetDebug)
	CREATE_SPLINE_CAM_WIDGETS(scsPlaneToBike, "Jet", "Bike", widgetDebug)
	CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS(widgetDebug)
#ENDIF*/

	WHILE bScriptInProgress
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_CAIDALIBRE") 
		
		SWITCH eMissionStage		
			CASE STAGE_INIT_MISSION		
			
				IF Is_Replay_In_Progress()

					SetupArrays()
					missionCanFail = FALSE
		
					//Check if player chose to shitskip
	       			IF g_bShitskipAccepted = TRUE
						IF Get_Replay_Mid_Mission_Stage() = 0
							START_REPLAY_SETUP(<<-363.5190, 1142.1714, 323.0888>>, 11.2577)
							eMissionStage = STAGE_SHOOT_PLANE							
						ENDIF
						IF Get_Replay_Mid_Mission_Stage() = 1
							START_REPLAY_SETUP(vBFChaseStartCoords, fBFChaseStartHeading)
							eMissionStage = STAGE_CHASE_PLANE
						ENDIF
						IF Get_Replay_Mid_Mission_Stage() = 2
							START_REPLAY_SETUP(<<2776.9570, 4740.3716, 44.9082>>, 341.7251)
							eMissionStage = STAGE_KILL_PASSENGERS
						ENDIF
						IF Get_Replay_Mid_Mission_Stage() = 3
							START_REPLAY_SETUP(<<-370.9615, 1151.7002, 324.3864>>, 225.63827)
							eMissionStage = STAGE_DESTROY_SNIPER
						ENDIF
						IF Get_Replay_Mid_Mission_Stage() = 4
							START_REPLAY_SETUP(<<1204.7, 1859, 78.2>>, 214.5280)
							eMissionStage = STAGE_END_CUTSCENE
						ENDIF
					ELSE
						IF Get_Replay_Mid_Mission_Stage() = 0	
							START_REPLAY_SETUP(<<1386.8092, 1142.3643, 113.3340>>, 82.7671)
							eMissionStage = STAGE_DRIVE_TO_OBSERVATORY
						ENDIF
						IF Get_Replay_Mid_Mission_Stage() = 1
							bSniperIsBeingReplayed = TRUE
							START_REPLAY_SETUP(<<-363.5190, 1142.1714, 323.0888>>, 11.2577)
							eMissionStage = STAGE_SHOOT_PLANE
						ENDIF
						IF Get_Replay_Mid_Mission_Stage() = 2
							START_REPLAY_SETUP(vBFChaseStartCoords, fBFChaseStartHeading)
							eMissionStage = STAGE_CHASE_PLANE
						ENDIF
						IF Get_Replay_Mid_Mission_Stage() = 3
							START_REPLAY_SETUP(<<2776.9570, 4740.3716, 44.9082>>, 341.7251)
							eMissionStage = STAGE_KILL_PASSENGERS
						ENDIF
						IF Get_Replay_Mid_Mission_Stage() = 4
							START_REPLAY_SETUP(<<-370.9615, 1151.7002, 324.3864>>, 225.63827)
							eMissionStage = STAGE_DESTROY_SNIPER
						ENDIF
					ENDIF
					
					manageSkip()
					
					bStageSetup = FALSE
				ELSE
					IF IS_REPEAT_PLAY_ACTIVE()
					
						PRINTSTRING("REPEAT PLAY IS ACTIVE, REQUESTING OPENING CUTSCENE") PRINTNL()
						//Request the opening cutscene
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							REQUEST_CUTSCENE("MARTIN_1_INT")
						ENDIF
//						MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_MARTIN_1, "MARTIN_1_INT", CS_ALL, CS_NONE, CS_3|CS_4|CS_5|CS_6|CS_7|CS_8)
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("MARTIN_1_INT",CS_SECTION_3|CS_SECTION_4|CS_SECTION_5|CS_SECTION_6|CS_SECTION_7|CS_SECTION_8|CS_SECTION_9|CS_SECTION_10|CS_SECTION_11|CS_SECTION_12|CS_SECTION_13|CS_SECTION_14|CS_SECTION_15|CS_SECTION_16|CS_SECTION_17)
						ENDIF
						
						//Create Michael for the mission if you are playing as Trevor
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<1390.7065, 1142.6586, 113.4431>>, 170.5477)
								PRINTSTRING("Waiting for Michael to be created for repeat play") PRINTNL()
								WAIT(0)
							ENDWHILE
						ENDIF
						WHILE NOT CREATE_PLAYER_VEHICLE(playerCar, CHAR_MICHAEL, <<1371.3683, 1132.9742, 112.8401>>, 20.5505, TRUE, VEHICLE_TYPE_CAR)
							PRINTSTRING("Waiting for Michaels car to be created for repeat play") PRINTNL()
							WAIT(0)
						ENDWHILE							
					ENDIF
				
					IF bCutsceneDone = FALSE
						icutsceneStage 			= 0
						bCutsceneActive 		= TRUE
						vehiclesMovedForStart 	= FALSE
						missionCanFail 			= FALSE
						trevExitTaskGiven 		= FALSE
						MikeExitTaskGiven		= FALSE
						lightsTurnedOn 			= FALSE
						bCutsceneBeingSkipped 	= FALSE

//						todStopped 				= FALSE
						
						REQUEST_ADDITIONAL_TEXT("BFINJEC", MISSION_TEXT_SLOT)
						
						WHILE bCutsceneActive
						
							SWITCH icutsceneStage 
								CASE 0																			
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
										IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
											REQUEST_MODEL( GET_PLAYER_VEH_MODEL( CHAR_TREVOR ) )
											SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
//											SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_TORSO, 0, 4, PLAYER_TWO)
//											SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_LEG, 18, 0, PLAYER_TWO)
//											SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_HAND, 0, 0, PLAYER_TWO)
//											SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_FEET, 16, 0, PLAYER_TWO)
//											SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_SPECIAL, 0, 0, PLAYER_TWO)
//											SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_SPECIAL2, 0, 0, PLAYER_TWO)
//											SET_CUTSCENE_PED_COMPONENT_VARIATION("TREVOR", PED_COMP_DECL, 0, 0, PLAYER_TWO)
											
											//Michael
											SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)											
											PRINTSTRING("MARTIN 1 SCRIPT - Trev's opening cutscene components set.") PRINTNL()
										ENDIF
									ENDIF
									
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR

										IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()	
											//Michael
											IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
												IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
													SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", g_sTriggerSceneAssets.ped[0], PLAYER_ZERO)
												ENDIF
											ENDIF
											
											//Trevor
											SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
											
											PRINTSTRING("TRIGGER SCENE - Trev and Mike set up for opening cutscene 1.") PRINTNL()
										ENDIF
										
									ENDIF									
									
									IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
										
	//									
	//									WHILE NOT HAS_CUTSCENE_LOADED()
	//										PRINTSTRING("waiting for cutscene MARTIN_1_INT to load")
	//										WAIT(0)
	//									ENDWHILE
										
										IF NOT IS_PED_INJURED(PLAYER_PED_ID())
											IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
												REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
											ENDIF
										ENDIF					
										
										SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)																
										
										SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
										
										//Register entities
										//Michael
										IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
											playerStartedAsMichael = TRUE
											
											IF NOT IS_PED_INJURED(PLAYER_PED_ID())
												REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
											ENDIF
											//created and handed over to cutscene to stop vehicle variations popping in during cutscene
											CREATE_PLAYER_VEHICLE(vehBodhi2, CHAR_TREVOR, <<1431.2135, 1055.9114, 113.2366>>, 356.6531, TRUE)
											
											//Trev
//											SET_CUTSCENE_PED_PROP_VARIATION("Trevor", ANCHOR_HEAD, 0, 0)
											REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_TWO)
											REGISTER_ENTITY_FOR_CUTSCENE(vehBodhi2, "trevors_car", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, BODHI2)
											
											//Get Michaels car if he came in one from trigger scene here
											IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
												IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
													PRINTSTRING("g_sTriggerSceneAssets.veh[0] exists from trigger scene. This is the car Michael arrived in")
													SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
													playerCar = g_sTriggerSceneAssets.veh[0]
												ELSE
													PRINTSTRING("g_sTriggerSceneAssets.veh[0] isn't driveable")
												ENDIF
											ELSE
												PRINTSTRING("g_sTriggerSceneAssets.veh[0] doesn't exist") 
											ENDIF
										ENDIF
										
										//Trev
										IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
											playerStartedAsMichael = FALSE
											IF NOT IS_PED_INJURED(PLAYER_PED_ID())
												REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
											ENDIF
											//Grab Michael from the lead in scene				
											IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
											    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
												sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = g_sTriggerSceneAssets.ped[0]
												SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
											ENDIF
											
											//Register Michael
											REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
											
											//Get Michaels car that should have been created by lead in scene.
											IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
												SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
												playerCar = g_sTriggerSceneAssets.veh[1]
												SET_VEHICLE_DOORS_LOCKED(playerCar, VEHICLELOCK_UNLOCKED)
											ENDIF										
											
											//Get Trevs car if he came in one from trigger scene here
											IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
												IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
													SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
													TrevCutsceneCar = g_sTriggerSceneAssets.veh[0]
													SET_VEHICLE_DOORS_LOCKED(TrevCutsceneCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
												ENDIF
											ENDIF
										ENDIF
										
										//Grab the parked car at side of the house
										IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
											SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], TRUE, TRUE)
											ParkedCarAtStart = g_sTriggerSceneAssets.veh[2]
										ENDIF
										
										MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_MARTIN_1)
										
										MartinHouseMainScenarioBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<1333, 1095, 137>>, <<1438.9, 1197, 109>>)
										
										IF playerStartedAsMichael
											START_CUTSCENE()
										ELSE
											// Flash blue as switch occurs during cutscene
											START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
										ENDIF
										
										REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
										
										SETTIMERA(0)
										
										IF NOT IS_REPEAT_PLAY_ACTIVE()
											IF NOT IS_SCREEN_FADED_IN()
												DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
											ENDIF
										ENDIF
										
										bClearedAreaForCutscene = FALSE
										iskipCutsceneStage = 0
										icutsceneStage++
									ENDIF	
										
								BREAK
							
								CASE 1			
									
									//Anything that should happen regardless of who the player is at start
									IF IS_CUTSCENE_PLAYING()
										IF GET_CUTSCENE_TIME() > 130000
											DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)	
										ENDIF
										
										IF NOT bClearedAreaForCutscene
											CLEAR_AREA_OF_PROJECTILES(<<1371.1974, 1146.6389, 112.7587>>, 100)
											CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100, TRUE)
											//Fix for B*1954118 - Stopping taxi getting in the way of the cutscene.
											IF DOES_ENTITY_EXIST(g_WaitingTaxi)
												SET_ENTITY_AS_MISSION_ENTITY(g_WaitingTaxi, DEFAULT, TRUE)
												TidyUpVehicle(g_WaitingTaxi, TRUE)
											ENDIF
											IF DOES_ENTITY_EXIST(g_WaitingTaxiDriver)
												SET_ENTITY_AS_MISSION_ENTITY(g_WaitingTaxiDriver, DEFAULT, TRUE)
												TidyUpPed(g_WaitingTaxiDriver, TRUE)
											ENDIF
											bClearedAreaForCutscene = TRUE
										ENDIF
									ENDIF									
									
									IF IS_REPEAT_PLAY_ACTIVE()
									AND IS_CUTSCENE_PLAYING()
										IF NOT IS_SCREEN_FADED_IN()
											DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
										ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(PlayerCar)
										IF NOT IS_VEHICLE_DRIVEABLE(playerCar)
											DELETE_VEHICLE(playerCar)
										ENDIF
									ENDIF
									
									//Handle the cutscene if player started as Michael
									IF NOT WAS_CUTSCENE_SKIPPED()
										IF playerStartedAsMichael = TRUE
										
											//Create Trevs car if starting as Michael and move Michaels car if it exists
											IF IS_CUTSCENE_PLAYING()
												IF lightsTurnedOn = FALSE
													IF GET_CLOCK_HOURS() > 21
													OR GET_CLOCK_HOURS() < 6
														IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Trevors_car"))
															IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Trevors_car"))
																SET_VEHICLE_LIGHTS(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Trevors_car")), SET_VEHICLE_LIGHTS_ON)
																lightsTurnedOn = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											
												IF GET_CUTSCENE_SECTION_PLAYING() > 1
													IF NOT DOES_ENTITY_EXIST(TrevCutsceneCar)
														IF CREATE_PLAYER_VEHICLE(TrevCutsceneCar, CHAR_TREVOR, <<1371.1974, 1146.6389, 112.7587>>, 177.6714, TRUE, VEHICLE_TYPE_CAR)
															SET_VEHICLE_DOORS_LOCKED(TrevCutsceneCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
														ENDIF
													ENDIF
												ENDIF	
												IF vehiclesMovedForStart = FALSE
													IF DOES_ENTITY_EXIST(playerCar)
														IF IS_VEHICLE_DRIVEABLE(playerCar)
															IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(playerCar)) 
																SET_ENTITY_COORDS(playerCar, <<1360.5767, 1181.4652, 111.4876>>)
																SET_ENTITY_HEADING(playerCar, 180.5927)
																SET_VEHICLE_ON_GROUND_PROPERLY(playerCar)
																vehiclesMovedForStart = TRUE
															ENDIF
															IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(playerCar))
															OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(playerCar))
															OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(playerCar))
															OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(playerCar))
																SET_ENTITY_COORDS(playerCar, <<1372.0696, 1131.5979, 112.8844>>)
																SET_ENTITY_HEADING(playerCar, 19.2193)
																SET_VEHICLE_ON_GROUND_PROPERLY(playerCar)
																RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1368.9, 1126.3, 116.6>>, <<1374.7, 1136.7, 111.9>>, 6, <<1372.0696, 1131.5979, 112.8844>>, 19.2193)
																vehiclesMovedForStart = TRUE
															ENDIF
															IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(playerCar)) 
															AND NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(playerCar)) 
															AND NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(playerCar)) 
															AND NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(playerCar)) 
															AND NOT IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(playerCar)) 
																DELETE_VEHICLE(playerCar)
																vehiclesMovedForStart = TRUE
															ENDIF
														ENDIF														
													ENDIF
												ENDIF
											ENDIF
											
											//Set up Michael on his exit state
											IF MikeExitTaskGiven = FALSE
												IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
													SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500) 
		                        					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
													FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK) 	
													MikeExitTaskGiven = TRUE
												ENDIF	
											ENDIF
											
											//Grab Trevor from the cutscene
											IF NOT DOES_ENTITY_EXIST(CSTrev)
												CSTrev = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor", PLAYER_TWO))  
												IF IS_PED_WEARING_HELMET(CSTrev)
													REMOVE_PED_HELMET(CSTrev, TRUE)
												ENDIF
											ENDIF		
											
											//Set up Trevor on his exit state
											IF trevExitTaskGiven = FALSE
												IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
													IF DOES_ENTITY_EXIST(TrevCutsceneCar)
														IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
															IF DOES_ENTITY_EXIST(CSTrev)
																IF NOT IS_PED_INJURED(CSTrev)
																	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CSTrev, TRUE)
																	OPEN_SEQUENCE_TASK(seqSequence)
																		TASK_ENTER_VEHICLE(NULL, TrevCutsceneCar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
																		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																			TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, NULL, <<1360.3199, 1166.0765, 170>>, MISSION_GOTO, 20, 20, -1, 70, 60, -1)
																			TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FLEE, 40, -1, -1, 100, 90)
																		ENDIF
																		IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																			TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
																		ENDIF
																	CLOSE_SEQUENCE_TASK(seqSequence)
																	TASK_PERFORM_SEQUENCE(CSTrev, seqSequence)
																	CLEAR_SEQUENCE_TASK(seqSequence)
																	FORCE_PED_AI_AND_ANIMATION_UPDATE(CSTrev)
																	FORCE_PED_MOTION_STATE(CSTrev, MS_ON_FOOT_WALK) 
																	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(TrevCutsceneCar, FALSE)
																	PRINTSTRING("sequence task given to Trev") PRINTNL()
																	trevExitTaskGiven = TRUE
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF										
											ENDIF
											
										ENDIF
										
										
										//*******************Handle everything if i started the mission as Trevor *******************
										
										IF playerStartedAsMichael = FALSE

											//Move Trev's car or create it if needed if started as Trev.
											IF vehiclesMovedForStart = FALSE
												IF DOES_ENTITY_EXIST(TrevCutsceneCar)
													IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
														IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar)) 
															SET_ENTITY_COORDS(TrevCutsceneCar, <<1360.5767, 1181.4652, 111.4876>>)
															SET_ENTITY_HEADING(TrevCutsceneCar, 180.5927)
															SET_VEHICLE_ON_GROUND_PROPERLY(TrevCutsceneCar)
															vehiclesMovedForStart = TRUE
														ENDIF
														IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(TrevCutsceneCar))
														OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(TrevCutsceneCar))
														OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(TrevCutsceneCar))
														OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(TrevCutsceneCar))
															SET_ENTITY_COORDS(TrevCutsceneCar, <<1371.1974, 1146.6389, 112.7587>>)
															SET_ENTITY_HEADING(TrevCutsceneCar, 177.6714)
															SET_VEHICLE_ON_GROUND_PROPERLY(TrevCutsceneCar)
															RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1368.5, 1141.0, 116.8>>, <<1374.2, 1152.5, 112.6>>, 6, <<1371.1974, 1146.6389, 112.7587>>, 177.6714)
															vehiclesMovedForStart = TRUE
														ENDIF
														IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar)) 
														AND NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(TrevCutsceneCar)) 
														AND NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(TrevCutsceneCar)) 
														AND NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(TrevCutsceneCar)) 
														AND NOT IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(TrevCutsceneCar)) 
															DELETE_VEHICLE(TrevCutsceneCar)
														ENDIF
													ENDIF
												ENDIF
												IF NOT DOES_ENTITY_EXIST(TrevCutsceneCar)
													IF CREATE_PLAYER_VEHICLE(TrevCutsceneCar, CHAR_TREVOR, <<1371.1974, 1146.6389, 112.7587>>, 177.6714)
														SET_VEHICLE_DOORS_LOCKED(TrevCutsceneCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
														vehiclesMovedForStart = TRUE
													ENDIF
												ENDIF
											ENDIF
											
	//										//Switch to Michael at the point when they're not on screen
	//										IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
	//											IF IS_CUTSCENE_PLAYING()
	//												IF GET_CUTSCENE_TIME() > 30809
	//													MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
	//													TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
	//												ENDIF
	//											ENDIF
	//										ENDIF
											
											//Set up Michael on his exit state
											IF MikeExitTaskGiven = FALSE
												IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
													//Switch to Michael now for player control
													//If switching after watching whole cut.
													IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
														IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO))
															FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO)))
															FORCE_PED_MOTION_STATE(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO)), MS_ON_FOOT_WALK) 														
														ENDIF
														MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
														TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
													ENDIF
//													SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500) 
//		                        					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//													FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK) 
													MikeExitTaskGiven = TRUE
												ENDIF	
											ENDIF
											//Handle Trevor on his exit state	
											If trevExitTaskGiven = FALSE
												IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
													IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL											
														IF DOES_ENTITY_EXIST(TrevCutsceneCar)
															IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
																SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
																OPEN_SEQUENCE_TASK(seqSequence)
																	TASK_ENTER_VEHICLE(NULL, TrevCutsceneCar, -1, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
																	IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																		TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, NULL, <<1360.3199, 1166.0765, 170>>, MISSION_GOTO, 20, 20, -1, 70, 60, -1)
																		TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FLEE, 40, -1, -1, 100, 90)
																	ENDIF
																	IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																		TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
																	ENDIF
																CLOSE_SEQUENCE_TASK(seqSequence)
																TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqSequence)
																CLEAR_SEQUENCE_TASK(seqSequence)
																FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
																FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], MS_ON_FOOT_WALK) 
																PRINTSTRING("sequence task given to Trev") PRINTNL()
																trevExitTaskGiven = TRUE
															ENDIF
														ENDIF
													ELSE
														IF DOES_ENTITY_EXIST(TrevCutsceneCar)
															IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
																OPEN_SEQUENCE_TASK(seqSequence)
																	TASK_ENTER_VEHICLE(NULL, TrevCutsceneCar, -1, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
																	IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																		TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, NULL, <<1360.3199, 1166.0765, 170>>, MISSION_GOTO, 20, 20, -1, 70, 60, -1)
																		TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FLEE, 40, -1, -1, 100, 90)
																	ENDIF
																	IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																		TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
																	ENDIF
																CLOSE_SEQUENCE_TASK(seqSequence)
																TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqSequence)
																CLEAR_SEQUENCE_TASK(seqSequence)
																FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
																FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK) 
																PRINTSTRING("sequence task given to Trev") PRINTNL()
																trevExitTaskGiven = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF										
											ENDIF
											
										ENDIF
										
										IF CAN_SET_EXIT_STATE_FOR_CAMERA()	
										
											REPLAY_STOP_EVENT()
											
											REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
										
											IF DOES_ENTITY_EXIST(sSelectorPeds.PedID[SELECTOR_PED_MICHAEL])
												IF NOT IS_PED_INJURED(sSelectorPeds.PedID[SELECTOR_PED_MICHAEL])
													SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.PedID[SELECTOR_PED_MICHAEL])
												ENDIF
											ENDIF
										ENDIF
										
										//Move stage on
										IF NOT IS_CUTSCENE_ACTIVE()
											SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
											icutsceneStage++
										ENDIF								
									ELSE
										
										//Set flag to start the cutscene skip
										bCutsceneBeingSkipped = TRUE
										
									ENDIF
									
									WHILE bCutsceneBeingSkipped = TRUE
										SWITCH iskipCutsceneStage
											CASE 0
												//Fade screen out before making changes
												IF NOT IS_SCREEN_FADED_OUT()
												OR NOT IS_SCREEN_FADING_OUT()
													DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
												ENDIF	
												IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
												AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
													IF IS_SCREEN_FADED_OUT()
														PRINTSTRING("iskipCutsceneStage++ Screen is faded out") PRINTNL()
														iskipCutsceneStage ++
													ENDIF
												ELSE	
													PRINTSTRING("Can't set exit states for Michael and Trevor just now") PRINTNL()
												ENDIF
											BREAK
											CASE 1
												IF IS_SCREEN_FADING_IN()
													DO_SCREEN_FADE_OUT(0)
												ENDIF											
												//Switch players if player started as trevor
												IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
													MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
													TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, FALSE)
													PRINTSTRING("Waiting on Michael being switched too") PRINTNL()
												ENDIF
												IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL 
													CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
													PRINTSTRING("iskipCutsceneStage++ player is Michael") PRINTNL()
													iskipCutsceneStage ++
												ENDIF
											BREAK
											CASE 2
												//Set up Trevs car if cutscene is skipped
												IF NOT DOES_ENTITY_EXIST(TrevCutsceneCar)
													REQUEST_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR)
													WHILE NOT HAS_PLAYER_VEH_MODEL_LOADED(CHAR_TREVOR, VEHICLE_TYPE_CAR)
														PRINTSTRING("waiting for trevs car to load")
														WAIT(0)
													ENDWHILE
													CREATE_PLAYER_VEHICLE(TrevCutsceneCar, CHAR_TREVOR, <<1371.1974, 1146.6389, 112.7587>>, 177.6714, TRUE, VEHICLE_TYPE_CAR)											
													SET_VEHICLE_ON_GROUND_PROPERLY(TrevCutsceneCar)
													SET_VEHICLE_DOORS_LOCKED(TrevCutsceneCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
												ENDIF
											
												//Give Trevor and the player tasks 
												IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
													IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
														IF DOES_ENTITY_EXIST(TrevCutsceneCar)
															IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
																SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
																OPEN_SEQUENCE_TASK(seqSequence)
																	TASK_ENTER_VEHICLE(NULL, TrevCutsceneCar, -1, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
																	IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																		TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, NULL, <<1360.3199, 1166.0765, 170>>, MISSION_GOTO, 20, 20, -1, 70, 60, -1)
																		TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FLEE, 40, -1, -1, 100, 90)
																	ENDIF
																	IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																		TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
																	ENDIF
																CLOSE_SEQUENCE_TASK(seqSequence)
																TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqSequence)
																CLEAR_SEQUENCE_TASK(seqSequence)
																FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
																FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], MS_ON_FOOT_WALK) 
																PRINTSTRING("sequence task given to Trev") PRINTNL()
																trevExitTaskGiven = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												
												//Give Trevor and the player tasks 
												IF DOES_ENTITY_EXIST(CSTrev)
													IF NOT IS_PED_INJURED(CSTrev)
														IF DOES_ENTITY_EXIST(TrevCutsceneCar)
															IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
																SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CSTrev, TRUE)
																OPEN_SEQUENCE_TASK(seqSequence)
																	TASK_ENTER_VEHICLE(NULL, TrevCutsceneCar, -1, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
																	IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																		TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, NULL, <<1360.3199, 1166.0765, 170>>, MISSION_GOTO, 20, 20, -1, 70, 60, -1)
																		TASK_HELI_MISSION(NULL, TrevCutsceneCar, NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FLEE, 40, -1, -1, 100, 90)
																	ENDIF
																	IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(TrevCutsceneCar))
																		TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TrevCutsceneCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1)
																	ENDIF
																CLOSE_SEQUENCE_TASK(seqSequence)
																TASK_PERFORM_SEQUENCE(CSTrev, seqSequence)
																CLEAR_SEQUENCE_TASK(seqSequence)
																FORCE_PED_AI_AND_ANIMATION_UPDATE(CSTrev)
																FORCE_PED_MOTION_STATE(CSTrev, MS_ON_FOOT_WALK) 
																PRINTSTRING("sequence task given to Trev") PRINTNL()
																trevExitTaskGiven = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF	
												
												//Michael
												SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500) 
	                        					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
												FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)	
												SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
												
												SETTIMERA(0)
												PRINTSTRING("iskipCutsceneStage++ tasks given") PRINTNL()
												iskipCutsceneStage++
											BREAK
											CASE 3
												IF TIMERA() > 1000

//													SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
													
													IF NOT IS_SCREEN_FADED_IN()
														DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
													ENDIF
													SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
													PRINTSTRING("icutsceneStage++ end of cutscene screen fading back in now") PRINTNL()
													icutsceneStage++
													iskipCutsceneStage++
													bCutsceneBeingSkipped = FALSE
												ENDIF
											BREAK
										ENDSWITCH										
									
										PRINTSTRING("Waiting for cutscene to finish skipping")
										WAIT(0)
										
									ENDWHILE											
											
								BREAK
								
								CASE 2
									
									CLEAR_PRINTS()
//									SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)			
									SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									
									//Clean up Michael's car if it exists
									IF DOES_ENTITY_EXIST(playerCar)
										IF IS_VEHICLE_DRIVEABLE(playerCar)
											SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(playerCar, FALSE)
											SET_VEHICLE_AS_NO_LONGER_NEEDED(playerCar)
										ENDIF
									ENDIF
									
									//Stop player from entering trevs car
									IF DOES_ENTITY_EXIST(TrevCutsceneCar)
										IF IS_VEHICLE_DRIVEABLE(TrevCutsceneCar)
											SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(TrevCutsceneCar, FALSE)
										ENDIF
									ENDIF
									
									//Make sure CsTrev exists now
									IF NOT DOES_ENTITY_EXIST(CSTrev)
										IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
											IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
												CSTrev = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
												PRINTSTRING("creating CSTrev ") PRINTNL()
											ENDIF
										ENDIF
									ENDIF
									
									REMOVE_CUTSCENE()
									bCutsceneActive = FALSE
									bCutsceneDone = TRUE
									icutsceneStage++
								
								BREAK
							ENDSWITCH			
							
							WAIT(0)
						ENDWHILE
					ENDIF
					
					IF stageInitMission()
						eMissionStage = STAGE_DRIVE_TO_OBSERVATORY
					ENDIF
				ENDIF
			BREAK
			CASE STAGE_DRIVE_TO_OBSERVATORY
				IF stageDriveToObservatory()
					eMissionStage = STAGE_SHOOT_PLANE
				ENDIF
			BREAK
			CASE STAGE_SHOOT_PLANE
				IF stageShootPlane()
					eMissionStage = STAGE_CUTSCENE_PLANE_DESCENT
				ENDIF
			BREAK
			CASE STAGE_CUTSCENE_PLANE_DESCENT
				IF stageCutscenePlaneDescent()
					eMissionStage = STAGE_CHASE_PLANE
				ENDIF
			BREAK
			CASE STAGE_CHASE_PLANE
				IF stageChasePlane()
					eMissionStage = STAGE_KILL_PASSENGERS
				ENDIF
			BREAK
			CASE STAGE_KILL_PASSENGERS
				IF stageKillPassengers()
					eMissionStage = STAGE_CUTSCENE_PLANE_CRASH
				ENDIF
			BREAK
			CASE STAGE_CUTSCENE_PLANE_CRASH
				DISABLE_FIRST_PERSON_CAMERA_DURING_TREVOR_SWITCH_SCENE()
				IF stageCutscenePlaneCrash()
					eMissionStage = STAGE_DESTROY_SNIPER
				ENDIF
			BREAK
			CASE STAGE_DESTROY_SNIPER
				DISABLE_FIRST_PERSON_CAMERA_DURING_TREVOR_SWITCH_SCENE()
				IF stageDestroySniper()
					eMissionStage = STAGE_END_CUTSCENE
				ENDIF			
			BREAK
			CASE STAGE_END_CUTSCENE
				IF StageEndCutscene()
					eMissionStage = PASSED_MISSION
				ENDIF			
			BREAK
			CASE PASSED_MISSION
				MissionPassed()
			BREAK
		ENDSWITCH
		
		//anything that needs to be called every frame the mission is running.
		IF missionCanFail = TRUE
			FailCheck()
		ENDIF
		
		IF eMissionStage = STAGE_DRIVE_TO_OBSERVATORY
		OR eMissionStage = STAGE_SHOOT_PLANE
		OR eMissionStage = STAGE_CUTSCENE_PLANE_DESCENT
			IF RemoteSniperNeedsUpdated = TRUE
				UPDATE_REMOTE_SNIPER(sSniperData)
				IF bCamNearClipSet = FALSE
					IF DOES_CAM_EXIST(sSniperData.cam_rifle) 
						SET_CAM_NEAR_CLIP(sSniperData.cam_rifle, 1.0)
						bCamNearClipSet = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Stats
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
				PlayerStatCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
			IF DOES_ENTITY_EXIST(PlayerStatCar)
				IF IS_VEHICLE_DRIVEABLE(PlayerStatCar)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerStatCar)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PlayerStatCar)
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
		
		#IF IS_DEBUG_BUILD
			Debug_Options()
//			UPDATE_WIDGETS()
			
//			UPDATE_SPLINE_CAM_WIDGETS(scsPlaneShotScene)
//			UPDATE_SPLINE_CAM_WIDGETS(scsPlaneToBike)
			
			sMissionName = "Martin 1"
			
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, 0, FALSE, sMissionName)								
								
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
				
				IF iReturnStage = 0
					eMissionStage = STAGE_INIT_MISSION
				ENDIF
				IF iReturnStage = 1
					eMissionStage = STAGE_DRIVE_TO_OBSERVATORY
				ENDIF	
				IF iReturnStage = 2
					eMissionStage = STAGE_SHOOT_PLANE
				ENDIF
				IF iReturnStage = 3
					eMissionStage = STAGE_CUTSCENE_PLANE_DESCENT
				ENDIF	
				IF iReturnStage = 4
					eMissionStage = STAGE_CHASE_PLANE
				ENDIF
				IF iReturnStage = 5
					eMissionStage = STAGE_KILL_PASSENGERS
				ENDIF
				IF iReturnStage = 6
					eMissionStage = STAGE_CUTSCENE_PLANE_CRASH
				ENDIF	
				IF iReturnStage = 7
					eMissionStage = STAGE_DESTROY_SNIPER
				ENDIF
				IF iReturnStage = 8
					eMissionStage = STAGE_END_CUTSCENE
				ENDIF
				IF iReturnStage = 9
					eMissionStage = PASSED_MISSION
				ENDIF

				manageSkip()
				iStageSection = 0
				bStageSetup = FALSE
				
			ENDIF
			
			IF JSkip
				
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
				
				SWITCH eMissionStage
					CASE STAGE_INIT_MISSION
						PRINTSTRING("JSKIPPING FROM STAGE_INIT_MISSION TO STAGE_DRIVE_TO_OBSERVATORY ") PRINTNL()
						eMissionStage = STAGE_DRIVE_TO_OBSERVATORY
					BREAK
					CASE STAGE_DRIVE_TO_OBSERVATORY
						PRINTSTRING("JSKIPPING FROM STAGE_DRIVE_TO_OBSERVATORY TO STAGE_SHOOT_PLANE ") PRINTNL()
						eMissionStage = STAGE_SHOOT_PLANE
					BREAK
					CASE STAGE_SHOOT_PLANE
						PRINTSTRING("JSKIPPING FROM STAGE_SHOOT_PLANE TO STAGE_CUTSCENE_PLANE_DESCENT ") PRINTNL()
						eMissionStage = STAGE_CUTSCENE_PLANE_DESCENT
					BREAK
					CASE STAGE_CUTSCENE_PLANE_DESCENT
						PRINTSTRING("JSKIPPING FROM STAGE_CUTSCENE_PLANE_DESCENT TO STAGE_CHASE_PLANE ") PRINTNL()
						eMissionStage = STAGE_CHASE_PLANE
					BREAK
					CASE STAGE_CHASE_PLANE
						PRINTSTRING("JSKIPPING FROM STAGE_CHASE_PLANE TO STAGE_KILL_PASSENGERS ") PRINTNL()
						eMissionStage = STAGE_KILL_PASSENGERS
					BREAK
					CASE STAGE_KILL_PASSENGERS
						PRINTSTRING("JSKIPPING FROM STAGE_KILL_PASSENGERS TO STAGE_CUTSCENE_PLANE_CRASH ") PRINTNL()
						eMissionStage = STAGE_CUTSCENE_PLANE_CRASH
					BREAK
					CASE STAGE_CUTSCENE_PLANE_CRASH
						PRINTSTRING("JSKIPPING FROM STAGE_CUTSCENE_PLANE_CRASH TO STAGE_DESTROY_SNIPER ") PRINTNL()
						eMissionStage = STAGE_DESTROY_SNIPER
					BREAK
					CASE STAGE_DESTROY_SNIPER
						PRINTSTRING("JSKIPPING FROM STAGE_DESTROY_SNIPER TO STAGE_END_CUTSCENE ") PRINTNL()
						eMissionStage = STAGE_END_CUTSCENE
					BREAK
					CASE STAGE_END_CUTSCENE
						PRINTSTRING("JSKIPPING FROM STAGE_END_CUTSCENE TO PASSED_MISSION ") PRINTNL()
						eMissionStage = PASSED_MISSION
					BREAK
				ENDSWITCH
				
				manageSkip()
				
				bStageSetup = FALSE
				iStageSection = 0
				
				JSkip = FALSE
			ENDIF			
			
			IF PSkip

				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF			
			
				SWITCH eMissionStage
					CASE STAGE_DRIVE_TO_OBSERVATORY
						eMissionStage = STAGE_DRIVE_TO_OBSERVATORY
					BREAK
					CASE STAGE_SHOOT_PLANE
						eMissionStage = STAGE_DRIVE_TO_OBSERVATORY
					BREAK
					CASE STAGE_CUTSCENE_PLANE_DESCENT
						eMissionStage = STAGE_SHOOT_PLANE
					BREAK
					CASE STAGE_CHASE_PLANE
						eMissionStage = STAGE_CUTSCENE_PLANE_DESCENT
					BREAK
					CASE STAGE_KILL_PASSENGERS
						eMissionStage = STAGE_CHASE_PLANE
					BREAK
					CASE STAGE_CUTSCENE_PLANE_CRASH
						eMissionStage = STAGE_KILL_PASSENGERS
					BREAK
					CASE STAGE_DESTROY_SNIPER
						eMissionStage = STAGE_CUTSCENE_PLANE_CRASH
					BREAK
					CASE STAGE_END_CUTSCENE
						eMissionStage = STAGE_DESTROY_SNIPER
					BREAK
				ENDSWITCH	
				
				manageSkip()
				
				bStageSetup = FALSE
				iStageSection = 0

				PSkip = FALSE
			ENDIF
		#ENDIF
		
		WAIT(0)
		
	ENDWHILE

	MissionCleanup()
ENDSCRIPT