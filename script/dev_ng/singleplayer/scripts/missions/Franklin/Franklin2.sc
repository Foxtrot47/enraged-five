// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	franklin2.sc
//		AUTHOR			:	Kevin Bolt, Ben Wilson, Matthew Booton
//		DESCRIPTION		:	Lamar is in trouble, Franklin goes to the sawmill to rescue
//							him with the help of Michael and Trevor.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_player.sch"
USING "commands_recording.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_itemsets.sch"

USING "building_control_public.sch"
USING "cam_recording_public.sch"
USING "chase_hint_cam.sch"   
USING "dialogue_public.sch"
USING "emergency_call.sch"
USING "locates_public.sch"
USING "flow_public_core_override.sch"
USING "model_enums.sch"
USING "script_ped.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "script_blips.sch"
USING "script_heist.sch"
USING "selector_public.sch"
USING "locates_public.sch"
USING "replay_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "cutscene_public.sch"
USING "vehicle_gen_public.sch"
USING "stripclub_public.sch"
USING "shop_public.sch"
USING "shared_hud_displays.sch"
USING "heist_end_screen.sch"
USING "vehicle_public.sch"
USING "achievement_public.sch"
USING "clearmissionarea.sch"
USING "cheat_controller_public.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

ENUM MISSION_STAGE
	STAGE_INTRO_CUTSCENE = 0,
	STAGE_MEET_THE_CREW,
	STAGE_MEET_CREW_CUTSCENE,
	STAGE_GET_INTO_POSITION,
	STAGE_ATTACK_SAWMILL,
	STAGE_GET_LAMAR_OUT,
	STAGE_DRIVE_HOME,
	STAGE_OUTRO_CUTSCENE
ENDENUM

ENUM FAILED_REASON
	FAILED_GENERIC = 0,
	FAILED_COVER_BLOWN,
	FAILED_LAMAR_DIED,
	FAILED_TREVOR_DIED,
	FAILED_MICHAEL_DIED,
	FAILED_FRANKLIN_DIED,
	FAILED_BULLDOZER_DESTROYED,
	FAILED_ABANDONED_LAMAR,
	FAILED_COPS_LED_TO_SAWMILL,
	FAILED_MICHAELS_CAR_DESTROYED,
	FAILED_TREVORS_CAR_DESTROYED
ENDENUM

ENUM SECTION_STAGE
	SECTION_STAGE_SETUP = 0,
	SECTION_STAGE_RUNNING,
	SECTION_STAGE_CLEANUP,
	SECTION_STAGE_SKIP,
	SECTION_STAGE_JUMPING_TO_STAGE
ENDENUM

ENUM TRIGGERED_TEXT_LABEL
	FRAN2_GOSAW = 0,
	FRAN2_GETP,
	FRAN2_FIND,
	FRAN2_GETLAMAR,
	FRAN2_SWFRANK,
	FRAN2_SIGHELP,
	FRAN2_SIGHELP2,
	FRAN2_SPECHELP,
	FRAN2_POSHELP,
	FRAN2_getCar,
	FRAN2_drvL,
	FRAN2_phnhlp,
	FRAN2_leadL,
	FRAN2_PROT,
	FRAN2_PROTFT,
	FRAN2_switch,
	FRAN2_SWITCH2,
	LM2_ALONE,
	LM2_WHERE,
	LM2_OUT, 
	LM2_LHEARM,
	LM2_WAY,
	LM2_NOWAIT,
	lm2_lgof,
	lm2_lgot,
	LM2_LSEE,
	LM2_SH1,
	LM2_SH2,
	LM2_SH3,
	LM2_SH4,
	LM2_SH5,
	LM2_SH6,
	LM2_SH7,
	LM2_SH8,
	LM2_snipe1,
	lm2_movin,
	LM2_BANT1,
	LM2_BANT2,
	LM2_BANT3,
	LM2_BANT4,
	LM2_BANT5,
	LM2_BANT6,
	LM2_BANT6b,
	LM2_LSIDE,
	LM2_MORE1,
	LM2_MORE2,
	LM2_MORE3,
	LM2_MORE4,
	LM2_MORE5,
	LM2_LCARS,
	LM2_miKill,
	LM2_PICK,
	LM2_FRANPICK,
	LM2_MIKEBANG,
	LM2_MIXT,
	LM2_MIXF,
	LM2_TREVSAME,
	LM2_PREBANG,
	lm2_bang,
	LM2_TREVBANG,
	LM2_MATT,
	LM2_NOCLOSE,
	LM2_SPOTL,
	LM2_MIKEHURT1,
	Lm2_frank,
	Lm2_mut,
	Lm2_flank,
	LM2_mrpg,
	LM2_deal,
	LM2_deal2,
	LM2_MPATH,
	LM2_PHNL,
	FRAN2_RETSAW,
	LM2_TAKE,
	LM2_LBYEM,
	LM2_LBYET,
	LM2_MGOING,
	LM2_tbye,
	LM2_leaveA,
	lm2_leave,
	Lm2_onphn,
	LM2_SNIPER,
	LM2_phn,
	LM2_Snipe3A,
	LM2_Snipe3B,
	LM2_enc1,
	LM2_enc2,
	LM2_move,
	LM2_move2,
	LM2_driveT,
	LM2_ANYSIGN,
	LM2_ANYSIGN2,
	LM2_ANYSIGN3,
	LM2_FOUND,
	LM2_FOUND2,
	LM2_FOUND3,
	LM2_WEED,
	FRAN2_SNIPEHELP
ENDENUM

ENUM SAWMILL_ENTRY_POINT
	ENTRY_POINT_BY_SNIPER_HILL = 0,
	ENTRY_POINT_BY_METAL_BRIDGE,
	ENTRY_POINT_BY_CONVEYOR_BELT,
	ENTRY_POINT_BY_CLIFF,
	ENTRY_POINT_BY_BULLDOZER,
	ENTRY_POINT_BY_WOOD_LOGS,
	ENTRY_POINT_BY_TRAIN_TUNNEL
ENDENUM

ENUM BUDDY_SHOOTOUT_ROUTE
	BUDDY_ROUTE_BULLDOZER = 0,
	BUDDY_ROUTE_BULLDOZER_ON_FOOT,
	BUDDY_ROUTE_SNIPING,
	BUDDY_ROUTE_REAR,
	BUDDY_ROUTE_FRONT_ASSAULT,
	BUDDY_ROUTE_IN_PLAYERS_GROUP
ENDENUM

STRUCT MISSION_VEHICLE
	VEHICLE_INDEX veh
	BLIP_INDEX blip
	INT iEvent
ENDSTRUCT

STRUCT MISSION_PED
	PED_INDEX ped
	BLIP_INDEX blip
	INT iEvent
ENDSTRUCT

STRUCT BUDDY_DATA
	PED_INDEX ped
	BLIP_INDEX blip
	COVERPOINT_INDEX cover
	INT iEvent
	INT iPrevMaxEvent
	INT iTimer
	INT iHealth
	BOOL bRefreshTasks
	BOOL bIsUsingSecondaryCover
	BOOL bSwitchedFromThisPed
	BOOL bSwitchBlocked
	VECTOR vDest
ENDSTRUCT

STRUCT ENEMY_DATA
	PED_INDEX ped
	BLIP_INDEX blip
	COVERPOINT_INDEX cover
	BOOL bIsCreated
	BOOL bSetToCharge
	BOOL bBlockOpenCombat
	BOOL bForcedLowAccuracy
	INT iEvent
	INT iTimer
	INT iRopeEvent
	VECTOR vDest
	AI_BLIP_STRUCT sBlipData
ENDSTRUCT

VECTOR vMeetLocation								= <<-727.5588, 5313.5103, 71.4980>>
VECTOR vMichaelsCarStartPos							= <<-716.7578, 5319.3579, 70.8262>>
VECTOR vTrevorsCarStartPos							= <<-718.4451, 5304.8486, 71.6183>>
VECTOR vMichaelMeetingPos							= <<-715.2372, 5317.7798, 70.6856>>
VECTOR vTrevorMeetingPos							= <<-719.1467, 5308.0078, 71.6904>>
VECTOR vBulldozerStartPos							= <<-540.0828, 5425.9590, 62.3495>>
VECTOR vLamarShootoutStartPos						= <<-523.0940, 5309.0811, 79.2679>>
VECTOR vSawmillExitPos 								= <<-579.0994, 5246.1548, 69.4693>>
VECTOR vFrontRouteStartPos							= <<-654.8719, 5241.6436, 75.2173>>
VECTOR vRearRouteStartPos							= <<-440.6452, 5429.0010, 76.2830>>
VECTOR vBulldozerRouteStartPos						= <<-545.1334, 5428.2881, 61.8625>>
VECTOR vMichaelSnipePos								= <<-555.7971, 5214.8340, 81.4045>>//<<-566.6038, 5214.9829, 82.3980>>
VECTOR vGoHomePos									= <<-71.2345, -1466.6842, 30.9867>>
VECTOR vReplayCarPos								= <<-732.8705, 5317.5469, 71.9049>>
VECTOR vMichaelSwitchAimPos							= <<0.0, 0.0, 0.0>>
VECTOR vChosenRoutePos								= <<0.0, 0.0, 0.0>>
VECTOR vPosOfLastPlayerKill							= <<0.0, 0.0, 0.0>>

BOOL bMissionFailed									= FALSE
BOOL bSkippedMocap									= FALSE
BOOL bHasUsedCheckpoint								= FALSE
BOOL bCreatedBallaStartCars							= FALSE
BOOL bEnemiesHaveBeenAlerted						= FALSE
BOOL bLamarHasBeenRescued							= FALSE
BOOL bHitLocateBallasOnBelt							= FALSE
BOOL bHitLocateBallasTimber1						= FALSE
BOOL bHitLocateBallasTimber2						= FALSE
BOOL bHitLocateBallasBulldozer1						= FALSE
BOOL bHitLocateBallasBulldozer2						= FALSE
BOOL bHitLocateBallasBeforeTimber					= FALSE
BOOL bHitLocateBallasSniper							= FALSE
BOOL bHitLocateBallasRearTimber1					= FALSE
BOOL bHitLocateBallasRearTimber2					= FALSE
BOOL bHitLocateBallasPostRescueTimber				= FALSE
BOOL bHitLocateBallasPostRescueExit					= FALSE
BOOL bHitLocateBallasRocket							= FALSE
BOOL bHitLocateBallasBulldozer3						= FALSE
BOOL bBulldozerReachedDestination					= FALSE
BOOL bBulldozerIsInMixGroup							= FALSE
BOOL bEscapedSawmill								= FALSE
BOOL bTaxiDropoffUpdated							= FALSE
BOOL bPlayerPickedStartCar							= FALSE
BOOL bMichaelHUDFlashingForSniper					= FALSE
BOOL bMichaelHUDFlashingForRocket					= FALSE
BOOL bSwitchHUDFlashingToGetLamar					= FALSE
BOOL bMichaelIsGettingFlanked						= FALSE
BOOL bPlayerGivenLookAtLamarTask					= FALSE
BOOL bMichaelSetToMoveFreely						= FALSE
BOOl bTrevorTriggeredShootoutEarly					= FALSE
BOOL bPlayerIsInShootoutAreaForSwitch				= FALSE
BOOL bBulldozerCoverDisabled						= FALSE
BOOL bReplayEventStarted							= FALSE
BOOL bHasTextLabelTriggered[120]

CONST_INT CHECKPOINT_FRANKLIN2_ATTACK_SAWMILL		1
CONST_INT CHECKPOINT_FRANKLIN2_GET_LAMAR_OUT		2
CONST_INT CHECKPOINT_FRANKLIN2_FLEE_AREA			3
CONST_INT CARREC_BALLA_POST_RESCUE_1				10
CONST_INT CARREC_BALLA_POST_RESCUE_2				11
CONST_INT CARREC_BALLA_POST_RESCUE_3				12
INT iCurrentEvent									= 0
INT iSwitchStage									= 0
INT iHealthBeforeSwitch								= 0
INT iTimeOfLastSwitch								= 0
INT iNavmeshBlockUnderSawmill						= -1
INT iNavmeshBlockByTimber							= -1
INT iEnemyToCheckForAlert							= 0
INT iCombatDialogueWaitTime							= 0
INT iPlayerCombatDialogueTimer						= 0
INT iBuddyCombatDialogueTimer						= 0
INT iBuddyUpdateDialogueTimer						= 0
INT iLamarCombatDialogueTimer						= 0
INT iLocateCheckIndex								= 0
INT iMainDialogueTimer								= 0
INT iTimeLastPedKilledByMichael						= 0
INT iTimeLastPedKilledByFranklin					= 0
INT iTimeLastPedKilledByPlayer						= 0
INT iFranklinInDangerTimer							= 0
INT iTrevorInDangerTimer							= 0
INT iFranklinDangerDialogueTimer					= 0
INT iTrevorDangerDialogueTimer						= 0
INT iNumTimesFiredRocket							= 0
INT iBallaRocketTimer								= 0
INT iBulldozerLiftTimer								= 0
INT iFinalSwitchTimer								= 0
INT iBulldozerJackTimer								= 0
INT iTimeSinceLamarRescued							= 0
INT iTimeSniperStartedAttacking						= 0
INT iRocketDialogueTimer							= 0
INT iSniperDialogueTimer							= 0
INT iTimePlayerGaveSignal							= 0
INT iTimeSincePlayerShotSniperRifle					= 0
INT iTimeSinceSwitchToMichael						= 0
INT iLamarFireTimer									= 0
INT iWantedFailTimer								= 0
INT iTimePlayedGotoMichaelDialogue					= 0
INT iBallasPedToSpeak								= 0
INT iFirstPersonSnipeAimTimer						= 0

FLOAT fMichaelsCarStartHeading						= 261.2129
FLOAT fTrevorsCarStartHeading						= 15.0
FLOAT fMichaelMeetingHeading						= 221.4874
FLOAT fTrevorMeetingHeading							= 283.0
FLOAT fBulldozerStartHeading						= 104.2
FLOAT fLamarShootoutStartHeading					= 163.0
FLOAT fMichaelSnipeHeading							= -14.1
FLOAT fReplayCarHeading								= 261.1234
FLOAT fBulldozerCruiseSpeed							= 0.0

STRING strLamarInjuredWalkAnims						= "move_injured_generic"
STRING strLamarInjuredAnims							= "missfra2"
STRING strFailLabel
STRING strWaypointBulldozerRoute					= "fra2_001"
STRING strWaypointRearRoute							= "fra2_002"
STRING strWaypointFrontRoute						= "fra2_003"
STRING strWaypointBulldozer							= "fratwoWP0"
STRING strCarrec									= "fratwo"
STRING strPickupAnims								= "pickup_object"

BLIP_INDEX blipCurrentObjective
BLIP_INDEX blipBulldozerRoute
BLIP_INDEX blipFrontRoute
BLIP_INDEX blipRearRoute

CAMERA_INDEX camMain

COVERPOINT_INDEX coverShootout[20]

OBJECT_INDEX objMichaelsWeapon
OBJECT_INDEX objShootoutCover[5]
OBJECT_INDEX objExtraWeed[3]
OBJECT_INDEX objFrontDoor

PICKUP_INDEX pickupShootout[3]

SCENARIO_BLOCKING_INDEX sbiShootout[1]

VEHICLE_INDEX vehMichaelsCar
VEHICLE_INDEX vehTrevorsCar
VEHICLE_INDEX vehFinalCutsceneCar
VEHICLE_INDEX vehReplayCar
VEHICLE_INDEX vehBallaStart[6]
VEHICLE_INDEX vehBallaPostRescueTimber
VEHICLE_INDEX vehBallaPostRescueExit[2]

MODEL_NAMES modelBulldozer				= BULLDOZER
MODEL_NAMES modelBalla					= G_M_Y_BallaOrig_01
MODEL_NAMES modelBallaStartCars			= BISON
MODEL_NAMES modelBallaReinforcementCar	= FELON
MODEL_NAMES modelExtraCover1			= PROP_WOODPILE_01B
MODEL_NAMES modelExtraCover2 			= PROP_SKIP_01A
MODEL_NAMES modelWeedPallet				= Prop_Weed_Pallet

MISSION_STAGE eMissionStage 			= STAGE_INTRO_CUTSCENE
SECTION_STAGE eSectionStage 			= SECTION_STAGE_SETUP
SELECTOR_SLOTS_ENUM eCurrentPlayer		= SELECTOR_PED_FRANKLIN
BUDDY_SHOOTOUT_ROUTE eMichaelRoute		= BUDDY_ROUTE_SNIPING
BUDDY_SHOOTOUT_ROUTE eTrevorRoute		= BUDDY_ROUTE_BULLDOZER
BUDDY_SHOOTOUT_ROUTE eFranklinRoute		= BUDDY_ROUTE_BULLDOZER_ON_FOOT
BUDDY_SHOOTOUT_ROUTE ePrevTrevorRoute	= BUDDY_ROUTE_BULLDOZER
BUDDY_SHOOTOUT_ROUTE ePrevFranklinRoute	= BUDDY_ROUTE_BULLDOZER_ON_FOOT
BUDDY_SHOOTOUT_ROUTE ePlayerRoute		= BUDDY_ROUTE_BULLDOZER_ON_FOOT
SAWMILL_ENTRY_POINT ePointOfEnemySpawn	= ENTRY_POINT_BY_METAL_BRIDGE
SWITCH_TYPE eFinalSwitchType			= SWITCH_TYPE_SHORT

SELECTOR_PED_STRUCT sSelectorPeds
LOCATES_HEADER_DATA sLocatesData

BUDDY_DATA sTrevor
BUDDY_DATA sMichael
BUDDY_DATA sFranklin
BUDDY_DATA sLamar

ENEMY_DATA sBallasStart[7]
ENEMY_DATA sBallasAfterAlert[4]
ENEMY_DATA sBallasGuardingLamar[1]
ENEMY_DATA sBallasOnBelt[1]
ENEMY_DATA sBallasBeforeTimber[4]
ENEMY_DATA sBallasTimber1[4]
ENEMY_DATA sBallasTimber2[4]
ENEMY_DATA sBallasAfterLamar[3]
ENEMY_DATA sBallasAfterLamar2[4]
ENEMY_DATA sBallasAfterLamar3[1]
ENEMY_DATA sBallasBulldozer1[3]
ENEMY_DATA sBallasBulldozer2[3]
ENEMY_DATA sBallasBulldozer3[3]
ENEMY_DATA sBallasRocket[1]
ENEMY_DATA sBallasSniper[1]
ENEMY_DATA sBallasFrontExtra[3]
ENEMY_DATA sBallasRearTimber1[4]
ENEMY_DATA sBallasRearTimber2[4]
ENEMY_DATA sBallasPostRescueTimber[3]
ENEMY_DATA sBallasPostRescueExit[6]
ENEMY_DATA sBallasOnPhone[1]

MISSION_VEHICLE sBulldozer

REL_GROUP_HASH relGroupBallas
REL_GROUP_HASH relGroupNeutral

structPedsForConversation sConversationPeds
SELECTOR_CAM_STRUCT sSelectorCam

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetDebug
	
	CONST_INT MAX_SKIP_MENU_LENGTH 			8
	INT iDebugJumpStage						= 0
	INT iDebugMichaelRoute					= 0
	INT iDebugFranklinRoute					= 0
	INT iDebugTrevorRoute					= 0
	INT iDebugPlayerRoute					= 0
	
	MissionStageMenuTextStruct sSkipMenu[MAX_SKIP_MENU_LENGTH]
	
	BOOL bDebugShowDebug					= FALSE
	
	PROC CREATE_WIDGETS()	
		widgetDebug = START_WIDGET_GROUP("Franklin 2")
			ADD_WIDGET_BOOL("Show debug display", bDebugShowDebug)
			
			START_WIDGET_GROUP("Variables")
				ADD_WIDGET_INT_READ_ONLY("iCurrentEvent", iCurrentEvent)
				ADD_WIDGET_INT_READ_ONLY("sLamar.iEvent", sLamar.iEvent)
				ADD_WIDGET_INT_READ_ONLY("sMichael.iEvent", sMichael.iEvent)
				ADD_WIDGET_INT_READ_ONLY("sTrevor.iEvent", sTrevor.iEvent)
				ADD_WIDGET_INT_READ_ONLY("sFranklin.iEvent", sFranklin.iEvent)
				ADD_WIDGET_INT_READ_ONLY("iDebugPlayerRoute", iDebugPlayerRoute)
				ADD_WIDGET_INT_READ_ONLY("iDebugMichaelRoute", iDebugMichaelRoute)
				ADD_WIDGET_INT_READ_ONLY("iDebugFranklinRoute", iDebugFranklinRoute)
				ADD_WIDGET_INT_READ_ONLY("iDebugTrevorRoute", iDebugTrevorRoute)
				ADD_WIDGET_INT_READ_ONLY("iTrevorInDangerTimer", iTrevorInDangerTimer)
				ADD_WIDGET_INT_READ_ONLY("iFranklinInDangerTimer", iFranklinInDangerTimer)
				ADD_WIDGET_FLOAT_READ_ONLY("fBulldozerCruiseSpeed", fBulldozerCruiseSpeed)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		SET_LOCATES_HEADER_WIDGET_GROUP(widgetDebug)
	ENDPROC
	
	PROC DESTROY_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetDebug)
			DELETE_WIDGET_GROUP(widgetDebug)
		ENDIF
	ENDPROC
	
	PROC DRAW_INT_ABOVE_PED(PED_INDEX ped, INT i)
		IF bDebugShowDebug
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
			IF NOT IS_PED_INJURED(ped)
				TEXT_LABEL str = ""
				str += i
				
				DRAW_DEBUG_TEXT(str, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped, <<0.0, 0.0, 1.0>>))
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(bHasTextLabelTriggered)
	INT i = 0
	
	REPEAT iArraySize i
		bHasTextLabelTriggered[i] = FALSE
	ENDREPEAT
ENDPROC

///Gets the ped index for a given player ped character, if the character is the player it returns PLAYER_PED_ID(), otherwise it returns the relevant selector ped.
FUNC PED_INDEX GET_PED_INDEX(enumCharacterList eChar)
	IF GET_PLAYER_PED_MODEL(eChar) = GET_ENTITY_MODEL(PLAYER_PED_ID())
		RETURN PLAYER_PED_ID()
	ELSE
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(eChar)]
	ENDIF
ENDFUNC


PROC DISABLE_COMBAT_CONTROLS_THIS_FRAME(BOOL bAllowEnteringVehicles = FALSE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	IF NOT bAllowEnteringVehicles
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
ENDPROC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

PROC FIX_ALL_VEHICLE_TYRES(VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		SET_VEHICLE_TYRE_FIXED(veh, SC_WHEEL_CAR_FRONT_LEFT)
		SET_VEHICLE_TYRE_FIXED(veh, SC_WHEEL_CAR_FRONT_RIGHT)
		SET_VEHICLE_TYRE_FIXED(veh, SC_WHEEL_CAR_REAR_LEFT)
		SET_VEHICLE_TYRE_FIXED(veh, SC_WHEEL_CAR_REAR_RIGHT)
	ENDIF
ENDPROC

FUNC PED_INDEX CREATE_ENEMY_PED(MODEL_NAMES model, VECTOR vPos, FLOAT fHeading, REL_GROUP_HASH group, 
								INT iHealth = 200, INT iArmour = 0, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, PED_TYPE ePedType = PEDTYPE_MISSION)
	PED_INDEX ped = CREATE_PED(ePedType, model, vPos, fHeading)
	GIVE_WEAPON_TO_PED(ped, weapon, INFINITE_AMMO, TRUE)
	SET_PED_INFINITE_AMMO(ped, TRUE, weapon)
	SET_PED_MAX_HEALTH(ped, iHealth)
	SET_ENTITY_HEALTH(ped, iHealth)
	ADD_ARMOUR_TO_PED(ped, -GET_PED_ARMOUR(ped))
	ADD_ARMOUR_TO_PED(ped, iArmour)
	SET_PED_DIES_WHEN_INJURED(ped, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(ped, group)
	SET_PED_AS_ENEMY(ped, TRUE)
	SET_PED_TARGET_LOSS_RESPONSE(ped, TLR_NEVER_LOSE_TARGET)
	SET_PED_CONFIG_FLAG(ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(ped, PCF_DisableHurt, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_DISABLE_PINNED_DOWN, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
	
	SET_PED_MONEY(ped, 0)
	
	RETURN ped
ENDFUNC

FUNC PED_INDEX CREATE_ENEMY_PED_IN_VEHICLE(MODEL_NAMES model, VEHICLE_INDEX veh, VEHICLE_SEAT eSeat, REL_GROUP_HASH group, 
										   INT iHealth = 200, INT iArmour = 0, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, PED_TYPE ePedType = PEDTYPE_MISSION)	
	PED_INDEX ped
	
	IF IS_VEHICLE_DRIVEABLE(veh)
		ped = CREATE_ENEMY_PED(model, GET_ENTITY_COORDS(veh) + <<0.0, 0.0, 3.0>>, 0.0, group, iHealth, iArmour, weapon, ePedType)
		SET_PED_INTO_VEHICLE(ped, veh, eSeat)
	ENDIF
	
	RETURN ped
ENDFUNC

PROC SET_PED_COMBAT_PARAMS(PED_INDEX &ped, INT iAccuracy, COMBAT_MOVEMENT eMovement, COMBAT_ABILITY_LEVEL eAbility, COMBAT_RANGE eRange, 
						   COMBAT_TARGET_LOSS_RESPONSE eTLR)
	SET_PED_COMBAT_MOVEMENT(ped, eMovement)
	SET_PED_COMBAT_ABILITY(ped, eAbility)
	SET_PED_COMBAT_RANGE(ped, eRange)
	SET_PED_ACCURACY(ped, iAccuracy)
	SET_PED_TARGET_LOSS_RESPONSE(ped, eTLR)
ENDPROC

PROC INITIALISE_ENEMY_PED(ENEMY_DATA &sEnemy, STRING strName, INT iIndex)
	IF NOT IS_PED_INJURED(sEnemy.ped)
		TASK_STAND_STILL(sEnemy.ped, -1)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemy.ped, TRUE)
		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(sEnemy.ped)
		sEnemy.bIsCreated = TRUE
		sEnemy.bSetToCharge = FALSE
		sEnemy.bForcedLowAccuracy = FALSE
		sEnemy.bBlockOpenCombat = FALSE
		sEnemy.iEvent = 0
		sEnemy.iTimer = 0
			
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_31 strDebugName = strName
			strDebugName += iIndex
			SET_PED_NAME_DEBUG(sEnemy.ped, strDebugName)
		#ENDIF
		
		#IF NOT IS_DEBUG_BUILD
			strName = strName	//	to prevent an Unreferenced variable error
			iIndex = iIndex
		#ENDIF
	ENDIF
ENDPROC

PROC INITIALISE_ENEMY_GROUP(ENEMY_DATA &sEnemies[], STRING name)
	INT i = 0

	REPEAT COUNT_OF(sEnemies) i
		INITIALISE_ENEMY_PED(sEnemies[i], name, i)
	ENDREPEAT
ENDPROC

PROC CLEAN_UP_ENEMY_PED(ENEMY_DATA &sEnemy, BOOL bResetVariables = FALSE)
	IF DOES_BLIP_EXIST(sEnemy.blip)
		REMOVE_BLIP(sEnemy.blip)
	ENDIF
	
	CLEANUP_AI_PED_BLIP(sEnemy.sBlipData)
	
	IF NOT IS_PED_INJURED(sEnemy.ped)
		STOP_SYNCHRONIZED_ENTITY_ANIM(sEnemy.ped, NORMAL_BLEND_OUT, TRUE)
	ELSE
		//Increment the kill stat if this ped was killed by the player.
		IF DOES_ENTITY_EXIST(sEnemy.ped)
			ENTITY_INDEX entityDeathSource = GET_PED_SOURCE_OF_DEATH(sEnemy.ped)
		
			IF DOES_ENTITY_EXIST(entityDeathSource)
			AND IS_ENTITY_A_PED(entityDeathSource)
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(entityDeathSource) = PLAYER_PED_ID()
					REL_GROUP_HASH eEnemyGroup = GET_PED_RELATIONSHIP_GROUP_HASH(sEnemy.ped)
				
					IF eEnemyGroup = relGroupBallas
						INFORM_MISSION_STATS_OF_INCREMENT(FRA2_KILLS)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	REMOVE_COVER_POINT(sEnemy.cover)
	SET_PED_AS_NO_LONGER_NEEDED(sEnemy.ped)
	
	IF bResetVariables
		sEnemy.bIsCreated = FALSE
		sEnemy.iEvent = 0
		sEnemy.iTimer = 0
		sEnemy.iRopeEvent = 0
		sEnemy.ped = NULL
	ENDIF
ENDPROC

PROC KILL_ENEMY_GROUP_INSTANTLY(ENEMY_DATA &sEnemies[])
	INT i = 0

	REPEAT COUNT_OF(sEnemies) i
		sEnemies[i].bIsCreated = TRUE
	
		IF NOT IS_PED_INJURED(sEnemies[i].ped)
			SET_ENTITY_HEALTH(sEnemies[i].ped, 0)
		ENDIF
	ENDREPEAT
ENDPROC

///Counts the number of enemies currently alive in the group. If the group hasn't been created yet than all the peds are considered as alive.
FUNC INT GET_NUM_ENEMIES_ALIVE_IN_GROUP(ENEMY_DATA &sEnemies[])
	INT iNumAlive = 0
	INT iNumEnemiesInGroup = COUNT_OF(sEnemies)
	INT i = 0
	
	IF sEnemies[0].bIsCreated
		REPEAT iNumEnemiesInGroup i
			IF NOT IS_PED_INJURED(sEnemies[i].ped)
				iNumAlive++
			ENDIF
		ENDREPEAT
	ELSE
		iNumAlive = iNumEnemiesInGroup
	ENDIF
	
	RETURN iNumAlive
ENDFUNC


/// PURPOSE:
///    Helper function to set up a ped's personal cover point and defensive sphere in one go.
PROC SET_PED_COVER_POINT_AND_SPHERE(PED_INDEX &ped, COVERPOINT_INDEX &cover, VECTOR vPos, FLOAT fHeading, FLOAT fSphereRadius,
									   COVERPOINT_USAGE covUsage, COVERPOINT_HEIGHT covHeight, COVERPOINT_ARC covArc, BOOL bGoToCentreOfSphere = FALSE)
	IF NOT IS_PED_INJURED(ped)
		IF cover != NULL
			REMOVE_COVER_POINT(cover)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(vPos)
			cover = ADD_COVER_POINT(vPos, fHeading, covUsage, covHeight, covArc)
		ENDIF
		
		SET_PED_SPHERE_DEFENSIVE_AREA(ped, vPos, fSphereRadius, bGoToCentreOfSphere)
	ENDIF
ENDPROC

PROC REMOVE_OBJECT(OBJECT_INDEX &obj, BOOL bForceDelete = FALSE)
	IF DOES_ENTITY_EXIST(obj)
		IF IS_ENTITY_ATTACHED(obj)
			DETACH_ENTITY(obj)
		ENDIF

		IF bForceDelete
			DELETE_OBJECT(obj)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL bForceDelete = FALSE)
	IF DOES_ENTITY_EXIST(veh)
		IF NOT IS_ENTITY_DEAD(veh)
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
			
			STOP_SYNCHRONIZED_ENTITY_ANIM(veh, NORMAL_BLEND_OUT, TRUE)
		ENDIF

		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh, FALSE)
			IF bForceDelete
				DELETE_VEHICLE(veh)
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_PED(PED_INDEX &ped, BOOL bForceDelete = FALSE)
	IF DOES_ENTITY_EXIST(ped)
		IF NOT IS_PED_INJURED(ped)
			STOP_SYNCHRONIZED_ENTITY_ANIM(ped, NORMAL_BLEND_OUT, TRUE)
		
			IF NOT IS_PED_IN_ANY_VEHICLE(ped, TRUE)
			AND NOT IS_PED_GETTING_INTO_A_VEHICLE(ped)
				IF IS_ENTITY_ATTACHED_TO_ANY_OBJECT(ped)
				OR IS_ENTITY_ATTACHED_TO_ANY_PED(ped)
				OR IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(ped)
					DETACH_ENTITY(ped)
				ENDIF
				
				FREEZE_ENTITY_POSITION(ped, FALSE)
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
				SET_ENTITY_COLLISION(ped, TRUE)
			ENDIF
			
			IF IS_PED_GROUP_MEMBER(ped, PLAYER_GROUP_ID())
				REMOVE_PED_FROM_GROUP(ped)
			ENDIF
			
			SET_PED_KEEP_TASK(ped, TRUE)
		ENDIF

		IF bForceDelete
			DELETE_PED(ped)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(ped)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ENEMY_GROUP(ENEMY_DATA &peds[], BOOL bForceDelete = FALSE)
	INT i = 0
	REPEAT COUNT_OF(peds) i
		IF DOES_BLIP_EXIST(peds[i].blip)
			REMOVE_BLIP(peds[i].blip)
		ENDIF
		
		CLEANUP_AI_PED_BLIP(peds[i].sBlipData)
	
		REMOVE_COVER_POINT(peds[i].cover)
	
		//Give them combat in most cases
		IF NOT IS_PED_INJURED(peds[i].ped)
			IF NOT IS_PED_IN_ANY_VEHICLE(peds[i].ped)
			AND GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].ped) = relGroupBallas
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_COMBAT_PED(peds[i].ped, PLAYER_PED_ID())
				ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					TASK_COMBAT_PED(peds[i].ped, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					TASK_COMBAT_PED(peds[i].ped, sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					TASK_COMBAT_PED(peds[i].ped, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				ENDIF	
				
				//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[i].ped, TRUE)
				SET_PED_KEEP_TASK(peds[i].ped, TRUE)
			ENDIF
		ENDIF
	
		REMOVE_PED(peds[i].ped, bForceDelete)

		peds[i].bIsCreated = FALSE
		peds[i].iEvent = 0
		peds[i].iRopeEvent = 0
		peds[i].iTimer = 0
	ENDREPEAT
ENDPROC

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &veh, FLOAT fTime)
	IF NOT IS_ENTITY_DEAD(veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh, fTime - GET_TIME_POSITION_IN_RECORDING(veh))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Converges a value towards a given destination, by adding/removing a given amount.
///    If bAdjustForFramerate is true, then fAmountToConverge is scaled by frame time.
PROC CONVERGE_VALUE(FLOAT &val, FLOAT fDesiredVal, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = FALSE)
	IF val != fDesiredVal
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = 0.0 +@ (fAmountToConverge * 30.0)
		ENDIF
	
		IF val - fDesiredVal > fConvergeAmountThisFrame
			val -= fConvergeAmountThisFrame
		ELIF val - fDesiredVal < -fConvergeAmountThisFrame
			val += fConvergeAmountThisFrame
		ELSE
			val = fDesiredVal
		ENDIF
	ENDIF
ENDPROC

PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_OUT()		
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC DO_FADE_IN_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC REMOVE_ALL_BLIPS()	
	IF DOES_BLIP_EXIST(blipCurrentObjective)
		REMOVE_BLIP(blipCurrentObjective)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipBulldozerRoute)
		REMOVE_BLIP(blipBulldozerRoute)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipFrontRoute)
		REMOVE_BLIP(blipFrontRoute)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipRearRoute)
		REMOVE_BLIP(blipRearRoute)
	ENDIF
	
	IF DOES_BLIP_EXIST(sMichael.blip)
		REMOVE_BLIP(sMichael.blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(sTrevor.blip)
		REMOVE_BLIP(sTrevor.blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(sFranklin.blip)
		REMOVE_BLIP(sFranklin.blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(sLamar.blip)
		REMOVE_BLIP(sLamar.blip)
	ENDIF
ENDPROC

PROC REMOVE_ALL_SHOOTOUT_OBJECTS(BOOL bForceDelete = FALSE)
	INT i = 0
	REPEAT COUNT_OF(objShootoutCover) i
		REMOVE_OBJECT(objShootoutCover[i], bForceDelete)
	ENDREPEAT
	
	REPEAT COUNT_OF(objExtraWeed) i
		REMOVE_OBJECT(objExtraWeed[i], bForceDelete)
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_OBJECTS(BOOL bForceDelete = FALSE)
	REMOVE_OBJECT(objMichaelsWeapon, bForceDelete)
	REMOVE_OBJECT(objFrontDoor, FALSE)
	
	REMOVE_ALL_SHOOTOUT_OBJECTS(bForceDelete)
ENDPROC

PROC REMOVE_ALL_SHOOTOUT_PEDS(BOOL bForceDelete = FALSE)
	REMOVE_ENEMY_GROUP(sBallasStart, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasAfterAlert, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasAfterLamar, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasAfterLamar2, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasAfterLamar3, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasGuardingLamar, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasOnBelt, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasBeforeTimber, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasTimber1, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasTimber2, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasBulldozer1, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasBulldozer2, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasRocket, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasSniper, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasFrontExtra, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasRearTimber1, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasRearTimber2, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasPostRescueTimber, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasPostRescueExit, bForceDelete)
	REMOVE_ENEMY_GROUP(sBallasOnPhone, bForceDelete)
ENDPROC

PROC REMOVE_ALL_PEDS(BOOL bForceDelete = FALSE)
	REMOVE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], bForceDelete)
	REMOVE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], bForceDelete)
	REMOVE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], bForceDelete)
	REMOVE_PED(sLamar.ped, bForceDelete)
	
	REMOVE_ALL_SHOOTOUT_PEDS(bForceDelete)
ENDPROC

PROC REMOVE_ALL_SHOOTOUT_VEHICLES(BOOL bForceDelete = FALSE)
	REMOVE_VEHICLE(vehBallaPostRescueTimber, bForceDelete)
	REMOVE_VEHICLE(vehBallaPostRescueExit[0], bForceDelete)
	REMOVE_VEHICLE(vehBallaPostRescueExit[1], bForceDelete)
	REMOVE_VEHICLE(sBulldozer.veh, bForceDelete)
	
	INT i = 0
	REPEAT COUNT_OF(vehBallaStart) i
		REMOVE_VEHICLE(vehBallaStart[i], bForceDelete)
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_VEHICLES(BOOL bForceDelete = FALSE)
	REMOVE_ALL_SHOOTOUT_VEHICLES(bForceDelete)
	REMOVE_VEHICLE(vehFinalCutsceneCar, bForceDelete)
	REMOVE_VEHICLE(vehMichaelsCar, bForceDelete)
	REMOVE_VEHICLE(vehTrevorsCar, bForceDelete)
	REMOVE_VEHICLE(vehReplayCar, bForceDelete)
ENDPROC

PROC REMOVE_ALL_CAMERAS()
	IF DOES_CAM_EXIST(camMain)
		DESTROY_CAM(camMain)
	ENDIF

	DISPLAY_RADAR(TRUE) 
	DISPLAY_HUD(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
ENDPROC

PROC REMOVE_ALL_ANIMS()
	REMOVE_ANIM_DICT(strLamarInjuredWalkAnims)
	REMOVE_ANIM_DICT(strLamarInjuredAnims)
	REMOVE_ANIM_DICT(strPickupAnims)
ENDPROC

PROC REMOVE_ALL_AUDIO()  
  	STOP_AUDIO_SCENES()
	STOP_STREAM()

	IF NOT bMissionFailed //This will have already triggered in that case.
		TRIGGER_MUSIC_EVENT("FH2A_MISSION_FAIL")
	ENDIF
	
	RELEASE_SCRIPT_AUDIO_BANK()
ENDPROC

/// Cancels any music events from this mission that may
PROC CANCEL_ALL_PREPARED_MUSIC_EVENTS()
	CANCEL_MUSIC_EVENT("FRA2_END_VEHICLE")
ENDPROC


/// PURPOSE:
///    Blocks all cops, ambulance and fire department.
PROC BLOCK_EMERGENCY_SERVICES_FOR_SHOOTOUT(BOOL bBlock)
	IF bBlock
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_MAX_WANTED_LEVEL(0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
		SET_CREATE_RANDOM_COPS(FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, FALSE) 
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
    	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
	ELSE
		SET_MAX_WANTED_LEVEL(5)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
		SET_CREATE_RANDOM_COPS(TRUE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE) 
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
    	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)
	ENDIF
ENDPROC

//Blocks random peds and vehicles to help improve performance.
PROC BLOCK_AMBIENT_PEDS_AND_VEHICLES_FOR_SHOOTOUT(BOOL bBlock)
	IF bBlock
		SET_NUMBER_OF_PARKED_VEHICLES(0)
		SET_RANDOM_TRAINS(FALSE)
		SET_VEHICLE_POPULATION_BUDGET(0)
		SET_PED_POPULATION_BUDGET(0)
	ELSE
		SET_NUMBER_OF_PARKED_VEHICLES(-1)
		SET_RANDOM_TRAINS(TRUE)
		SET_VEHICLE_POPULATION_BUDGET(3)
		SET_PED_POPULATION_BUDGET(3)
	ENDIF
ENDPROC

PROC BLOCK_ROADS_AT_SAWMILL(BOOL bBlock)
	IF bBlock
		SET_ROADS_IN_AREA(<<-713.6164, 5242.0498, 0.8567>>, <<-428.4971, 5498.2988, 171.8567>>, FALSE)
	ELSE
		SET_ROADS_BACK_TO_ORIGINAL(<<-713.6164, 5242.0498, 0.8567>>, <<-428.4971, 5498.2988, 171.8567>>)
	ENDIF
ENDPROC

PROC BLOCK_SCENARIOS_AT_SAWMILL(BOOL bBlock)
	IF bBlock
		IF sbiShootout[0] = NULL
			sbiShootout[0] = ADD_SCENARIO_BLOCKING_AREA(<<-970.6, 5015.2, 0.8567>>, <<-228.4971, 5698.2988, 181.8567>>)
		ENDIF
	ELSE
		IF sbiShootout[0] != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(sbiShootout[0])
			sbiShootout[0] = NULL
		ENDIF
	ENDIF
ENDPROC

PROC SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(BOOL bActive)
	INT i

	IF bActive
		IF coverShootout[0] = NULL
			coverShootout[0] = ADD_COVER_POINT(<<-565.6465, 5257.0039, 69.4669>>, 175.0081, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[1] = NULL
			coverShootout[1] = ADD_COVER_POINT(<<-581.6578, 5291.7690, 69.2604>>, 128.3183, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[2] = NULL
			coverShootout[2] = ADD_COVER_POINT(<<-592.0037, 5236.3384, 69.9043>>, 325.1953, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[3] = NULL
			coverShootout[3] = ADD_COVER_POINT(<<-487.6498, 5385.0801, 77.0692>>, 192.7037, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[4] = NULL
			coverShootout[4] = ADD_COVER_POINT(<<-499.8006, 5292.3442, 79.6100>>, 158.0821, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF

		IF coverShootout[5] = NULL
			coverShootout[5] = ADD_COVER_POINT(<<-580.1699, 5262.9121, 69.4438>>, 56.7250, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[6] = NULL
			coverShootout[6] = ADD_COVER_POINT(<<-588.9133, 5318.1499, 69.2144>>, 333.8809, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[7] = NULL
			coverShootout[7] = ADD_COVER_POINT(<<-582.0135, 5334.6250, 69.2144>>, 329.7396, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[8] = NULL
			coverShootout[8] = ADD_COVER_POINT(<<-554.9641, 5364.9629, 69.4369>>, 340.0293, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[9] = NULL
			coverShootout[9] = ADD_COVER_POINT(<<-522.1553, 5250.7095, 78.5550>>, 339.6773, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[10] = NULL
			coverShootout[10] = ADD_COVER_POINT(<<-524.0961, 5250.4805, 78.3368>>, 355.7343, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[11] = NULL
			coverShootout[11] = ADD_COVER_POINT(<<-475.8146, 5321.9814, 79.6100>>, 161.7416, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[12] = NULL
			coverShootout[12] = ADD_COVER_POINT(<<-475.4565, 5334.2856, 81.7084>>, 344.8566, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[13] = NULL
			coverShootout[13] = ADD_COVER_POINT(<<-472.9561, 5340.5737, 81.7094>>, 162.1539, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[14] = NULL
			coverShootout[14] = ADD_COVER_POINT(<<-495.6543, 5299.4863, 79.6100>>, 152.2808, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[15] = NULL
			coverShootout[15] = ADD_COVER_POINT(<<-494.1110, 5298.9985, 79.6100>>, 161.1279, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF coverShootout[16] = NULL
			coverShootout[16] = ADD_COVER_POINT(<<-532.4843, 5260.9775, 73.3015>>, 73.5275, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[17] = NULL
			coverShootout[17] = ADD_COVER_POINT(<<-580.3589, 5263.8354, 69.4457>>, 140.5717, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[18] = NULL
			coverShootout[18] = ADD_COVER_POINT(<<-492.6234, 5316.0278, 79.6067>>, 333.0276, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF coverShootout[19] = NULL
			coverShootout[19] = ADD_COVER_POINT(<<-615.4936, 5267.1724, 71.5453>>, 317.7524, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
		ENDIF
	ELSE
		REPEAT COUNT_OF(coverShootout) i
			IF coverShootout[i] != NULL
				REMOVE_COVER_POINT(coverShootout[i])
				coverShootout[i] = NULL
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC CREATE_SHOOTOUT_PICKUPS()
	VECTOR vPickupPos
	
	INT i = 0
	REPEAT COUNT_OF(pickupShootout) i
		IF DOES_PICKUP_EXIST(pickupShootout[i])
			vPickupPos = GET_PICKUP_COORDS(pickupShootout[i])
			REMOVE_PICKUP(pickupShootout[i])
			CLEAR_AREA(vPickupPos, 2.0, TRUE)
		ENDIF
	ENDREPEAT
		
	
	INT iPlacementFlags = 0
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
	
	pickupShootout[0] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<-515.1648, 5307.2559, 80.2493>>, <<0.0000, 0.0000, -18.3600>>, iPlacementFlags)
	pickupShootout[1] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<-468.3400, 5355.2998, 79.8500>>, <<0.0000, 0.0000, -50.4000>>, iPlacementFlags)
	pickupShootout[2] = CREATE_PICKUP_ROTATE(PICKUP_ARMOUR_STANDARD, <<-523.6100, 5289.8901, 73.2810>>, <<-90.0000, 0.0000, -58.2000>>, iPlacementFlags)
ENDPROC

///Checks the three player chars and makes sure they're assigned the correct speakers in dialogue. This is useful for when switches occur, as these
///make current speaker peds invalid.
PROC REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF sConversationPeds.PedInfo[0].Index != sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
		OR NOT sConversationPeds.PedInfo[0].ActiveInConversation
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 0, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
		ENDIF
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF sConversationPeds.PedInfo[0].Index != PLAYER_PED_ID()
		OR NOT sConversationPeds.PedInfo[0].ActiveInConversation
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 0, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
	ELSE
		IF sConversationPeds.PedInfo[0].Index != NULL
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 0, NULL, "MICHAEL")
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF sConversationPeds.PedInfo[1].Index != sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
		OR NOT sConversationPeds.PedInfo[1].ActiveInConversation
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 1, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
		ENDIF
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF sConversationPeds.PedInfo[1].Index != PLAYER_PED_ID()
		OR NOT sConversationPeds.PedInfo[1].ActiveInConversation
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 1, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF
	ELSE
		IF sConversationPeds.PedInfo[1].Index != NULL
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 1, NULL, "FRANKLIN")
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF sConversationPeds.PedInfo[2].Index != sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
		OR NOT sConversationPeds.PedInfo[2].ActiveInConversation
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
		ENDIF
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF sConversationPeds.PedInfo[2].Index != PLAYER_PED_ID()
		OR NOT sConversationPeds.PedInfo[2].ActiveInConversation
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ELSE
		IF sConversationPeds.PedInfo[2].Index != NULL
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 2, NULL, "TREVOR")
		ENDIF
	ENDIF
ENDPROC

PROC SET_CUTSCENE_PED_COMPONENT_VARIATIONS(STRING strCutsceneName)
	INT iNameHash = GET_HASH_KEY(strCutsceneName)

	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		IF iNameHash = HASH("FRA_2_INT")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
		ELIF iNameHash = HASH("FRA_2_IG_4_ALT1_concat")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			ENDIF
			
			//SET_CUTSCENE_PED_OUTFIT("Michael", GET_PLAYER_PED_MODEL(CHAR_MICHAEL), OUTFIT_P0_LEATHER_AND_JEANS)
			//SET_CUTSCENE_PED_OUTFIT("Trevor", GET_PLAYER_PED_MODEL(CHAR_TREVOR), OUTFIT_P2_TSHIRT_CARGOPANTS_3)
			
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_EARS, 0)
			SET_CUTSCENE_PED_PROP_VARIATION("Trevor", ANCHOR_EARS, 0)			
		ELIF iNameHash = HASH("FRA_2_EXT")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Lamar", sLamar.ped)
			ENDIF
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_BERD, 0, 0)
		ENDIF
	ENDIF
ENDPROC

//Checks if ped1 is in front of ped2
FUNC BOOL IS_PED_BLOCKING_TARGET(PED_INDEX ped1, PED_INDEX ped2, VECTOR vTarget)
	VECTOR vPedPos1 = GET_ENTITY_COORDS(ped1)
	VECTOR vPedPos2 = GET_ENTITY_COORDS(ped2)
	FLOAT fDistBetweenPeds = VDIST2(vPedPos1, vPedPos2)
	FLOAT fHeadingBetweenPedAndTarget = GET_HEADING_FROM_VECTOR_2D(vTarget.x - vPedPos2.x, vTarget.y - vPedPos2.y)
	FLOAT fHeadingBetweenPeds = GET_HEADING_FROM_VECTOR_2D(vPedPos1.x - vPedPos2.x, vPedPos1.y - vPedPos2.y)
	FLOAT fHeadingDiff = ABSF(fHeadingBetweenPeds - fHeadingBetweenPedAndTarget)
	
	IF fHeadingDiff > 180.0
		fHeadingDiff = ABSF(fHeadingDiff - 360.0)
	ENDIF

	IF fDistBetweenPeds < 9.0
		IF fHeadingDiff < 40.0
			RETURN TRUE
		ENDIF
	ELSE
		IF fHeadingDiff < 20.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Fetches the closest entry point to a given coords. Useful for determining suitable locations to spawn enemies.
FUNC SAWMILL_ENTRY_POINT GET_CLOSEST_ENTRY_POINT_TO_COORDS(VECTOR vPos)
	FLOAT fDist[7] 
	FLOAT fClosestDist = -1.0
	SAWMILL_ENTRY_POINT eClosestEntryPoint
	
	fDist[ENTRY_POINT_BY_BULLDOZER] = VDIST2(vPos, <<-557.3316, 5385.4585, 69.6229>>)
	fDist[ENTRY_POINT_BY_CLIFF] = VDIST2(vPos, <<-611.2953, 5334.0156, 71.1438>>)
	fDist[ENTRY_POINT_BY_SNIPER_HILL] = VDIST2(vPos, <<-570.3, 5212.4, 88.4678>>)
	fDist[ENTRY_POINT_BY_METAL_BRIDGE] = VDIST2(vPos, <<-598.2, 5280.5, 71.2>>)
	fDist[ENTRY_POINT_BY_CONVEYOR_BELT] = VDIST2(vPos, <<-574.8340, 5253.5645, 69.4682>>)
	fDist[ENTRY_POINT_BY_TRAIN_TUNNEL] = VDIST2(vPos, <<-503.5374, 5248.1382, 85.5385>>)
	fDist[ENTRY_POINT_BY_WOOD_LOGS] = VDIST2(vPos, <<-476.6512, 5344.6880, 83.9177>>)
	
	INT i = 0
	REPEAT COUNT_OF(fDist) i
		IF fClosestDist = -1.0
		OR fDist[i] < fClosestDist
			fClosestDist = fDist[i]
			eClosestEntryPoint = INT_TO_ENUM(SAWMILL_ENTRY_POINT, i)
		ENDIF
	ENDREPEAT
	
	RETURN eClosestEntryPoint
ENDFUNC

PROC GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
	WEAPON_TYPE e_fail_weapon = Get_Fail_Weapon(ENUM_TO_INT(GET_PLAYER_PED_ENUM(PLAYER_PED_ID())))
	
	IF e_fail_weapon = WEAPONTYPE_INVALID 
	OR e_fail_weapon = WEAPONTYPE_UNARMED
		e_fail_weapon = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
	ENDIF
		
	IF e_fail_weapon != WEAPONTYPE_INVALID 
	AND e_fail_weapon != WEAPONTYPE_UNARMED
		INT i_ammo_per_clip = GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), e_fail_weapon)
	
		IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), e_fail_weapon) < i_ammo_per_clip
			ADD_AMMO_TO_PED(PLAYER_PED_ID(), e_fail_weapon, i_ammo_per_clip * 2)
			SET_AMMO_IN_CLIP(PLAYER_PED_ID(), e_fail_weapon, i_ammo_per_clip)
		ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), e_fail_weapon, TRUE)
	ENDIF	
ENDPROC

PROC REFRESH_BUDDY_PED_AUDIO_MIX_GROUP()
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "FRANKLIN_2_BUDDIES_GROUP")
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN_2_BUDDIES_GROUP")
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "FRANKLIN_2_BUDDIES_GROUP")
	ENDIF
	
	REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
ENDPROC

PROC MISSION_SETUP()	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)
	
	REQUEST_ADDITIONAL_TEXT("FRAN2", MISSION_TEXT_SLOT)

	ADD_PED_FOR_DIALOGUE(sConversationPeds, 1, PLAYER_PED_ID(), "FRANKLIN")
	
	ADD_RELATIONSHIP_GROUP("BALLAS", relGroupBallas)
	ADD_RELATIONSHIP_GROUP("NEUTRAL", relGroupNeutral)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupBallas)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupNeutral)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBallas, relGroupBallas)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupBallas, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBallas, relGroupNeutral)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupNeutral, relGroupNeutral)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupNeutral, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupNeutral, relGroupBallas)

	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)

	DISABLE_TAXI_HAILING(TRUE)
	DISABLE_CABLE_CAR_SCRIPT()
	SUPPRESS_EMERGENCY_CALLS()
	
	BLOCK_ROADS_AT_SAWMILL(TRUE)
	BLOCK_SCENARIOS_AT_SAWMILL(TRUE)
	
	iNavmeshBlockUnderSawmill = ADD_NAVMESH_BLOCKING_OBJECT((<<-551.224, 5323.079, 70.517>>), (<<25.0, 87.3, 3.0>>), DEG_TO_RAD(-18.920))
	iNavmeshBlockByTimber = ADD_NAVMESH_BLOCKING_OBJECT((<<-517.347, 5276.551, 77.5>>), (<<52.27, 4.0, 3.0>>), DEG_TO_RAD(-110.2))

	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())

	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelBulldozer, TRUE)

	g_bFranklin2RequestedBackup = TRUE //The current mission is set up to always have Michael and Trevor arrive as backup.

	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
		
		sSkipMenu[0].sTxtLabel = "INTRO_CUTSCENE (FRA_2_INT)"   
		sSkipMenu[1].sTxtLabel = "MEET_THE_CREW" 
		sSkipMenu[2].sTxtLabel = "MEET_CREW_CUTSCENE (FRA_2_IG_4_ALT1_concat)" 
		sSkipMenu[3].sTxtLabel = "GET_INTO_POSITION" 
		sSkipMenu[4].sTxtLabel = "ATTACK_SAWMILL" 
		sSkipMenu[5].sTxtLabel = "GET_LAMAR_OUT" 
		sSkipMenu[6].sTxtLabel = "DRIVE_HOME"
		sSkipMenu[7].sTxtLabel = "OUTRO_CUTSCENE (FRA_2_EXT)"
	#ENDIF
ENDPROC

PROC MISSION_CLEANUP()
	IF NOT HAS_CUTSCENE_FINISHED()
		STOP_CUTSCENE()
	ENDIF

	REMOVE_CUTSCENE()
	
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE
	ENDIF
	
	//If we started a replay recording then make sure it's cancelled if we're performing cleanup.
	IF bReplayEventStarted
		REPLAY_STOP_EVENT()
		bReplayEventStarted = FALSE
	ENDIF

	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	SET_CREATE_RANDOM_COPS(TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	DISABLE_CELLPHONE(FALSE)
	HIDE_ACTIVE_PHONE(FALSE)
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	SET_STUNT_JUMPS_CAN_TRIGGER(TRUE)
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	SET_AGGRESSIVE_HORNS(FALSE)
	SET_RANDOM_TRAINS(TRUE)
	CLEAR_FOCUS()
	SET_GPS_DISABLED_ZONE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	STOP_GAMEPLAY_CAM_SHAKING(FALSE)
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
				DETACH_ENTITY(PLAYER_PED_ID())
			ENDIF
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
		SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
		SET_POLICE_RADAR_BLIPS(TRUE)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
		SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
	ELSE
		//Have the buddies flee/fight depending on what's happening in the shootout.
		IF eMissionStage = STAGE_ATTACK_SAWMILL
		OR eMissionStage = STAGE_GET_LAMAR_OUT
		OR eMissionStage = STAGE_GET_INTO_POSITION
		OR eMissionStage = STAGE_MEET_THE_CREW
		OR eMissionStage = STAGE_DRIVE_HOME
			PED_INDEX peds[8]
			BOOL bEnemiesNearby = FALSE
		
			IF NOT IS_PED_INJURED(sLamar.ped)
				GET_PED_NEARBY_PEDS(sLamar.ped, peds)
			ENDIF
			
			INT i = 0
			REPEAT COUNT_OF(peds) i
				IF NOT IS_PED_INJURED(peds[i])
					IF GET_PED_RELATIONSHIP_GROUP_HASH(peds[i]) = relGroupBallas
						bEnemiesNearby = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				IF NOT IS_PED_IN_COMBAT(sLamar.ped)
				AND eMissionStage >= STAGE_GET_LAMAR_OUT
					IF bEnemiesNearby
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sLamar.ped, 500.0)
					ELSE
						TASK_SMART_FLEE_COORD(sLamar.ped, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 200.0, -1)
					ENDIF
				ENDIF		
				
				SET_PED_KEEP_TASK(sLamar.ped, TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF NOT IS_PED_IN_COMBAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF bEnemiesNearby
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 500.0)
					ELSE
						TASK_SMART_FLEE_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 200.0, -1)
					ENDIF
					SET_PED_KEEP_TASK(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
				ENDIF		
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF NOT IS_PED_IN_COMBAT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF bEnemiesNearby
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 500.0)
					ELSE
						TASK_SMART_FLEE_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 200.0, -1)
					ENDIF
					SET_PED_KEEP_TASK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
				ENDIF		
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF NOT IS_PED_IN_COMBAT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF bEnemiesNearby
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 500.0)
					ELSE
						TASK_SMART_FLEE_COORD(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 200.0, -1)
					ENDIF
					SET_PED_KEEP_TASK(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
  
	//REMOVE_PTFX_ASSET()
	TRIGGER_MUSIC_EVENT("FRA2_FAIL")
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
  
  	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
 	REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 2)
	REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 4)
	REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 5)
	REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 6)
	REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 7)
	REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 8)

	REMOVE_ALL_AUDIO()
	REMOVE_ALL_BLIPS()
	REMOVE_ALL_CAMERAS()
	
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		REMOVE_ALL_OBJECTS(TRUE)
		REMOVE_ALL_PEDS(TRUE)
		REMOVE_ALL_VEHICLES(TRUE)
	ELSE
		REMOVE_ALL_OBJECTS()
		REMOVE_ALL_PEDS()
		REMOVE_ALL_VEHICLES()
	ENDIF
	
	REMOVE_ALL_ANIMS()
	SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(FALSE)
	
	CLEAR_SELECTOR_PED_PRIORITY(sSelectorPeds)
	SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
	SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
	SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)

	IF eSectionStage != SECTION_STAGE_JUMPING_TO_STAGE
		//Clean up anything that was created in the initial setup (this stuff must not be cleaned up if p-skipping)
		#IF IS_DEBUG_BUILD
			DESTROY_WIDGETS()
		#ENDIF
		
		DISABLE_TAXI_HAILING(FALSE)
		RELEASE_SUPPRESSED_EMERGENCY_CALLS()

		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelBulldozer, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_BIKE), FALSE)

		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		ENABLE_CABLE_CAR_SCRIPT()
		KILL_FACE_TO_FACE_CONVERSATION()

		BLOCK_ROADS_AT_SAWMILL(FALSE)
		BLOCK_SCENARIOS_AT_SAWMILL(FALSE)
		
		IF iNavmeshBlockUnderSawmill != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavmeshBlockUnderSawmill)
			iNavmeshBlockUnderSawmill = -1
		ENDIF
		
		IF iNavmeshBlockByTimber != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavmeshBlockByTimber)
			iNavmeshBlockByTimber = -1
		ENDIF

		REGISTER_SCRIPT_WITH_AUDIO(FALSE)
		TERMINATE_THIS_THREAD()
	ELSE
		CLEAR_TRIGGERED_LABELS()
		CLEAR_PRINTS()
		CLEAR_HELP()
		CLEAR_AREA(<<0.0, 0.0, 0.0>>, 10000.0, TRUE)
		STOP_FIRE_IN_RANGE(<<0.0, 0.0, 0.0>>, 10000.0)
		REMOVE_DECALS_IN_RANGE(<<0.0, 0.0, 0.0>>, 10000.0)
		
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())

		CANCEL_ALL_PREPARED_MUSIC_EVENTS()
		
		SET_MODEL_AS_NO_LONGER_NEEDED(modelBulldozer)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelBalla)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelBallaStartCars)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelBallaReinforcementCar)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelExtraCover1)
		
		REMOVE_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
		REMOVE_WEAPON_ASSET(WEAPONTYPE_SMG)
		REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
		
		REMOVE_WAYPOINT_RECORDING(strWaypointBulldozerRoute)
		REMOVE_WAYPOINT_RECORDING(strWaypointFrontRoute)
		REMOVE_WAYPOINT_RECORDING(strWaypointRearRoute)
		REMOVE_WAYPOINT_RECORDING(strWaypointBulldozer)

		REMOVE_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_1, strCarrec)
		REMOVE_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_2, strCarrec)
		REMOVE_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_3, strCarrec)

		IF eMissionStage < STAGE_ATTACK_SAWMILL
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
			SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)		
		ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		
		INT i = 0
		REPEAT COUNT_OF(pickupShootout) i
			IF DOES_PICKUP_EXIST(pickupShootout[i])
				REMOVE_PICKUP(pickupShootout[i])
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC



PROC JUMP_TO_STAGE(MISSION_STAGE stage, BOOL bIsDebugJump = FALSE)
	DO_FADE_OUT_WITH_WAIT()
	eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
	iCurrentEvent = 0
	eMissionStage = stage 
	MISSION_CLEANUP()
	
	//Update mission checkpoint in case they skipped the stages where it gets set.
	IF bIsDebugJump
		IF eMissionStage >= STAGE_DRIVE_HOME
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_FRANKLIN2_FLEE_AREA, "FLEE_AREA", TRUE)
		ELIF eMissionStage >= STAGE_GET_LAMAR_OUT
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_FRANKLIN2_GET_LAMAR_OUT, "GET_LAMAR_OUT")
		ELIF eMissionStage >= STAGE_GET_INTO_POSITION
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_FRANKLIN2_ATTACK_SAWMILL, "ATTACK_SAWMILL")
		ELIF eMissionStage >= STAGE_MEET_THE_CREW
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "MEET_THE_CREW")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Chooses which stage to jump to if the player restarted the mission from a checkpoint.
/// RETURNS:
///    
FUNC BOOL JUMP_TO_CHECKPOINT()
	IF Is_Replay_In_Progress()
		INT iStage = Get_Replay_Mid_Mission_Stage()
		
		IF g_bShitskipAccepted
			iStage++
		ENDIF
        
		IF iStage = 0
			JUMP_TO_STAGE(STAGE_MEET_THE_CREW)
		ELIF iStage = CHECKPOINT_FRANKLIN2_ATTACK_SAWMILL
			JUMP_TO_STAGE(STAGE_GET_INTO_POSITION)
		ELIF iStage = CHECKPOINT_FRANKLIN2_GET_LAMAR_OUT
			JUMP_TO_STAGE(STAGE_GET_LAMAR_OUT)
		ELIF iStage = CHECKPOINT_FRANKLIN2_FLEE_AREA
			JUMP_TO_STAGE(STAGE_DRIVE_HOME)
		ELIF iStage > CHECKPOINT_FRANKLIN2_FLEE_AREA
			JUMP_TO_STAGE(STAGE_OUTRO_CUTSCENE)
		ENDIF
		
		RETURN TRUE
	ELSE
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "MEET_THE_CREW") 
	ENDIF
	
	RETURN FALSE
ENDFUNC

///Creates a new load scene sphere and waits until it has finished loading or it times out.
PROC NEW_LOAD_SCENE_SPHERE_WITH_WAIT(VECTOR vPos, FLOAT fRadius, NEWLOADSCENE_FLAGS controlFlags = 0, INT iMaxWaitTime = 10000)
	INT iTimeOut = GET_GAME_TIMER()

	WHILE GET_GAME_TIMER() - iTimeOut < iMaxWaitTime
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_START_SPHERE(vPos, fRadius, controlFlags)
		ELIF IS_NEW_LOAD_SCENE_LOADED()
			iTimeOut = 0
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	NEW_LOAD_SCENE_STOP()	
ENDPROC

PROC SETUP_TREVOR_OUTFIT()
	IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_TREVOR))
		//SET_PED_COMP_ITEM_CURRENT_SP(GET_PED_INDEX(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_TSHIRT_CARGOPANTS_3, FALSE) //1572447 - Don't change outfits.
		SET_PED_COMP_ITEM_CURRENT_SP(GET_PED_INDEX(CHAR_TREVOR), COMP_TYPE_PROPS, PROPS_P2_HEADSET, TRUE)
	ENDIF
ENDPROC

PROC SETUP_MICHAEL_OUTFIT()
	IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_MICHAEL))
		//SET_PED_COMP_ITEM_CURRENT_SP(GET_PED_INDEX(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_LEATHER_AND_JEANS, FALSE)
		SET_PED_COMP_ITEM_CURRENT_SP(GET_PED_INDEX(CHAR_MICHAEL), COMP_TYPE_PROPS, PROPS_P0_HEADSET, TRUE)
	ENDIF
ENDPROC

FUNC BOOL SETUP_REQ_FRANKLIN(VECTOR vPos, FLOAT fHeading = 0.0, BOOl bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		REQUEST_PLAYER_PED_MODEL(CHAR_FRANKLIN)
	
		IF HAS_PLAYER_PED_MODEL_LOADED(CHAR_FRANKLIN)
			IF bJustRequestAssets
				RETURN TRUE
			ELIF CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, vPos, fHeading)
				SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
				SET_PED_MAX_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 1800)
				SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 1800)
				SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PCF_DisableHurt, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
				sFranklin.iEvent = 0
				
				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_CARBINERIFLE, 500, TRUE)
				ADD_PED_FOR_DIALOGUE(sConversationPeds, 1, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], RELGROUPHASH_PLAYER)
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_PROPS, PROPS_P1_HEADSET, TRUE)
								
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF sConversationPeds.PedInfo[1].Index != sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 1, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_TREVOR(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		REQUEST_PLAYER_PED_MODEL(CHAR_TREVOR)
		
		IF HAS_PLAYER_PED_MODEL_LOADED(CHAR_TREVOR)
			IF bJustRequestAssets
				RETURN TRUE
			ELIF CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vPos, fHeading)
				SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
				SET_PED_MAX_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 1800)
				SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 1800)
				SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_DisableHurt, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
				sTrevor.iEvent = 0
			
				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_CARBINERIFLE, 500, TRUE)
				ADD_PED_FOR_DIALOGUE(sConversationPeds, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RELGROUPHASH_PLAYER)		
				SETUP_TREVOR_OUTFIT()
			
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF sConversationPeds.PedInfo[2].Index != sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_MICHAEL(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		REQUEST_PLAYER_PED_MODEL(CHAR_MICHAEL)
		
		IF HAS_PLAYER_PED_MODEL_LOADED(CHAR_MICHAEL)
			IF bJustRequestAssets
				RETURN TRUE
			ELIF CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vPos, fHeading)
				SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
				SET_PED_MAX_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 1800)
				SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 1800)
				SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_DisableHurt, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
				sMichael.iEvent = 0
			
				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER, 100, TRUE)
				ADD_PED_FOR_DIALOGUE(sConversationPeds, 0, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RELGROUPHASH_PLAYER)
				SETUP_MICHAEL_OUTFIT()
			
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF sConversationPeds.PedInfo[0].Index != sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 0, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_LAMAR(VECTOR vPos, FLOAT fHeading = 0.0)
	IF NOT DOES_ENTITY_EXIST(sLamar.ped)		
		IF CREATE_NPC_PED_ON_FOOT(sLamar.ped, CHAR_LAMAR, vPos, fHeading)		
			SET_PED_CAN_BE_TARGETTED(sLamar.ped, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(sLamar.ped, RELGROUPHASH_PLAYER)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
			SET_PED_MAX_MOVE_BLEND_RATIO(sLamar.ped, pedmove_run)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(sLamar.ped, "FRANKLIN_2_LAMAR_GROUP")
			
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 3, sLamar.ped, "LAMAR")
			
			IF eMissionStage = STAGE_ATTACK_SAWMILL
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sLamar.ped,TRUE)
				TASK_START_SCENARIO_IN_PLACE(sLamar.ped,"WORLD_HUMAN_STUPOR")
			ENDIF
			
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_HEAD, 0, 1)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_TORSO, 2, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_LEG, 5, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_FEET, 1, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_BERD, 1, 0)
			
			sLamar.iEvent = 0
			sLamar.iTimer = 0
			sLamar.bRefreshTasks = FALSE
			sLamar.bIsUsingSecondaryCover = FALSE
			
			RETURN TRUE
		ENDIF
	ELSE
		IF sConversationPeds.PedInfo[3].Index != sLamar.ped
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 3, sLamar.ped, "LAMAR")
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_MICHAELS_CAR(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(vehMichaelsCar)
		REQUEST_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
		
		IF HAS_PLAYER_VEH_MODEL_LOADED(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
			IF bJustRequestAssets
				RETURN TRUE
			ELIF CREATE_PLAYER_VEHICLE(vehMichaelsCar, CHAR_MICHAEL, vPos, fHeading, TRUE, VEHICLE_TYPE_CAR)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehMichaelsCar, SC_DOOR_FRONT_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehMichaelsCar, SC_DOOR_FRONT_RIGHT, FALSE)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehMichaelsCar, FALSE)
				SET_VEHICLE_STRONG(vehMichaelsCar, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehMichaelsCar, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehMichaelsCar, TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMichaelsCar, FALSE)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_TREVORS_CAR(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(vehTrevorsCar)
		REQUEST_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR)
		
		IF HAS_PLAYER_VEH_MODEL_LOADED(CHAR_TREVOR, VEHICLE_TYPE_CAR)
			IF bJustRequestAssets
				RETURN TRUE
			ELIF CREATE_PLAYER_VEHICLE(vehTrevorsCar, CHAR_TREVOR, vPos, fHeading, TRUE, VEHICLE_TYPE_CAR)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehTrevorsCar, SC_DOOR_FRONT_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehTrevorsCar, SC_DOOR_FRONT_RIGHT, FALSE)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehTrevorsCar, FALSE)
				SET_VEHICLE_STRONG(vehTrevorsCar, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehTrevorsCar, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehTrevorsCar, TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevorsCar, FALSE)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_REPLAY_CAR(VECTOR vPos, FLOAT fHeading = 0.0)
	IF NOT DOES_ENTITY_EXIST(vehReplayCar)
		IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		AND GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() != BULLDOZER
			REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
			
			IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
				vehReplayCar = CREATE_REPLAY_CHECKPOINT_VEHICLE(vPos, fHeading)
				
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehReplayCar, FALSE)
				SET_VEHICLE_INFLUENCES_WANTED_LEVEL(vehReplayCar, FALSE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehReplayCar, TRUE)
				SET_ENTITY_AS_MISSION_ENTITY(vehReplayCar, TRUE, TRUE)
				
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_BULLDOZER(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(sBulldozer.veh)
		REQUEST_MODEL(modelBulldozer)
		
		IF HAS_MODEL_LOADED(modelBulldozer)
			IF bJustRequestAssets
				RETURN TRUE
			ELSE 
				sBulldozer.veh = CREATE_VEHICLE(modelBulldozer, vPos, fHeading)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sBulldozer.veh, SC_DOOR_FRONT_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sBulldozer.veh, SC_DOOR_FRONT_RIGHT, FALSE)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(sBulldozer.veh, FALSE)
				SET_VEHICLE_STRONG(sBulldozer.veh, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(sBulldozer.veh, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sBulldozer.veh, TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sBulldozer.veh, FALSE)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(sBulldozer.veh, "FRANKLIN_2_BULLDOZER_Group")
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sBulldozer.veh, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelBulldozer)
				SET_VEHICLE_BULLDOZER_ARM_POSITION(sBulldozer.veh, 0.1, FALSE)
				SET_ENTITY_ALWAYS_PRERENDER(sBulldozer.veh, TRUE)
				
				sBulldozer.iEvent = 0
				
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_PLAYER_IS_FRANKLIN()
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
		SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
		REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
	ELSE
		REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_PLAYER_IS_MICHAEL()
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
		SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
		REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
	ELSE
		REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_PLAYER_IS_TREVOR()
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
		REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
	ELSE
		REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SETUP_REQ_BUDDIES_START_FIGHT() 
	sMichael.iEvent = 0
	sMichael.iPrevMaxEvent = 0
	sMichael.iTimer = 0
	sMichael.bSwitchBlocked = FALSE
	sMichael.bRefreshTasks = FALSE
	sMichael.bIsUsingSecondaryCover = FALSE
	sMichael.bSwitchedFromThisPed = FALSE
	
	sFranklin.iEvent = 0
	sFranklin.iPrevMaxEvent = 0
	sFranklin.iTimer = 0
	sFranklin.bSwitchBlocked = FALSE
	sFranklin.bRefreshTasks = FALSE
	sFranklin.bIsUsingSecondaryCover = FALSE
	sFranklin.bSwitchedFromThisPed = FALSE
	
	sTrevor.iEvent = 0
	sTrevor.iPrevMaxEvent = 0
	sTrevor.iTimer = 0
	sTrevor.bSwitchBlocked = FALSE
	sTrevor.bRefreshTasks = FALSE
	sTrevor.bIsUsingSecondaryCover = FALSE
	sTrevor.bSwitchedFromThisPed = FALSE

	sLamar.iEvent = 0
	sLamar.iPrevMaxEvent = 0
	sLamar.iTimer = 0
	sLamar.bRefreshTasks = FALSE
	sLamar.bIsUsingSecondaryCover = FALSE
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])		
		IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER)
			GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER, 100, TRUE)
		ENDIF
		
		SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RELGROUPHASH_PLAYER)
		SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 5, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CA_BLIND_FIRE_IN_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)				
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_DisableHurt, TRUE)
		SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE, TRUE, FALSE, FALSE, FALSE)
		SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
		SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER, TRUE)
		SET_PED_MAX_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 1800)
		SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 1800)
		SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
		SET_RAGDOLL_BLOCKING_FLAGS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_FALLING | RBF_IMPACT_OBJECT)
		
		SET_PED_USING_ACTION_MODE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, -1)
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_CARBINERIFLE)
			GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_CARBINERIFLE, 500, TRUE)
		ENDIF
		
		SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], RELGROUPHASH_PLAYER)
		SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 5, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CA_BLIND_FIRE_IN_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PCF_DisableHurt, TRUE)
		SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE, TRUE, FALSE, FALSE, FALSE)
		SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_CARBINERIFLE, TRUE)
		SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
		SET_PED_MAX_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 1800)
		SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 1800)
		SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
		SET_RAGDOLL_BLOCKING_FLAGS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_FALLING | RBF_IMPACT_OBJECT)
		SET_PED_USING_ACTION_MODE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE, -1)
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_CARBINERIFLE)
			GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_CARBINERIFLE, 500, TRUE)
		ENDIF
		
		SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RELGROUPHASH_PLAYER)
		SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CA_BLIND_FIRE_IN_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_DisableHurt, TRUE)
		SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE, TRUE, FALSE, FALSE, FALSE)
		SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_CARBINERIFLE, TRUE)
		SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		SET_PED_MAX_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 1800)
		SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 1800)
		SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		SET_RAGDOLL_BLOCKING_FLAGS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_FALLING | RBF_IMPACT_OBJECT)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_USING_ACTION_MODE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, -1)
	ENDIF

	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SETUP_REQ_SHOOTOUT_COVER_OBJECTS()
	IF NOT DOES_ENTITY_EXIST(objShootoutCover[0])
		REQUEST_MODEL(modelExtraCover1)
		REQUEST_MODEL(modelExtraCover2)
		REQUEST_MODEL(modelWeedPallet)
		
		IF HAS_MODEL_LOADED(modelExtraCover1)	
		AND HAS_MODEL_LOADED(modelExtraCover2)	
		AND HAS_MODEL_LOADED(modelWeedPallet)	
			objShootoutCover[0] = CREATE_OBJECT_NO_OFFSET(modelExtraCover1, <<-614.2900, 5269.0601, 71.3612>>)
			SET_ENTITY_ROTATION(objShootoutCover[0], <<-4.3000, -0.0000, -44.1800>>)
			
			objShootoutCover[1] = CREATE_OBJECT_NO_OFFSET(modelExtraCover2, <<-525.2531, 5241.2412, 78.6260>>)
			SET_ENTITY_ROTATION(objShootoutCover[1], <<3.3465, 4.7541, 172.9586>>)
			
			objShootoutCover[2] = CREATE_OBJECT_NO_OFFSET(modelWeedPallet, <<-600.1290, 5325.8389, 69.4513>>)
			SET_ENTITY_ROTATION(objShootoutCover[2], <<0.0000, 0.0000, -32.0400>>)
			
			objShootoutCover[3] = CREATE_OBJECT_NO_OFFSET(modelWeedPallet, <<-578.5708, 5303.5190, 69.2611>>)
			SET_ENTITY_ROTATION(objShootoutCover[3], <<0.0000, 0.0000, -83.5200>>)
			
			objShootoutCover[4] = CREATE_OBJECT_NO_OFFSET(modelWeedPallet, <<-578.8108, 5305.3989, 69.2611>>)
			SET_ENTITY_ROTATION(objShootoutCover[4], <<0.0000, 0.0000, -97.5600>>)
							
			INT i = 0
			REPEAT COUNT_OF(objShootoutCover) i
				FREEZE_ENTITY_POSITION(objShootoutCover[i], TRUE)
				
				IF GET_ENTITY_MODEL(objShootoutCover[i]) = modelWeedPallet
					SET_ENTITY_LOD_DIST(objShootoutCover[i], 200)
				ENDIF
			ENDREPEAT
			
			SET_MODEL_AS_NO_LONGER_NEEDED(modelExtraCover1)
			SET_MODEL_AS_NO_LONGER_NEEDED(modelExtraCover2)
			SET_MODEL_AS_NO_LONGER_NEEDED(modelWeedPallet)
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_WEED_PALLETS_FOR_CUTSCENE(BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(objExtraWeed[0])
		REQUEST_MODEL(modelWeedPallet)
		
		IF HAS_MODEL_LOADED(modelWeedPallet)	
			IF bJustRequestAssets
				RETURN TRUE
			ELSE
				objExtraWeed[0] = CREATE_OBJECT_NO_OFFSET(modelWeedPallet, <<-597.2990, 5313.8091, 69.2413>>)
				SET_ENTITY_ROTATION(objExtraWeed[0], <<0.0000, 0.0000, 33.4800>>)
				
				objExtraWeed[1] = CREATE_OBJECT_NO_OFFSET(modelWeedPallet, <<-594.8490, 5314.0791, 69.2413>>)
				SET_ENTITY_ROTATION(objExtraWeed[1], <<0.0000, 0.0000, 42.8400>>)
				
				objExtraWeed[2] = CREATE_OBJECT_NO_OFFSET(modelWeedPallet, <<-595.7390, 5312.1489, 69.2413>>)
				SET_ENTITY_ROTATION(objExtraWeed[2], <<0.0000, 0.0000, 5.4000>>)
								
				INT i = 0
				REPEAT COUNT_OF(objExtraWeed) i
					FREEZE_ENTITY_POSITION(objExtraWeed[i], TRUE)
					
					IF GET_ENTITY_MODEL(objExtraWeed[i]) = modelWeedPallet
						SET_ENTITY_LOD_DIST(objExtraWeed[i], 200)
					ENDIF
				ENDREPEAT

				SET_MODEL_AS_NO_LONGER_NEEDED(modelWeedPallet)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///Returns the ped index of the ped that killed the given ped (if their source of death was a ped).
FUNC PED_INDEX GET_PED_THAT_KILLED_PED(PED_INDEX ped)
	ENTITY_INDEX entityDeathSource = GET_PED_SOURCE_OF_DEATH(ped)
	
	IF DOES_ENTITY_EXIST(entityDeathSource)
		IF IS_ENTITY_A_PED(entityDeathSource)
			RETURN GET_PED_INDEX_FROM_ENTITY_INDEX(entityDeathSource)
		ENDIF
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC BOOL ARE_PEDS_IN_ANGLED_AREA(PED_INDEX ped_1, PED_INDEX ped_2, PED_INDEX ped_3, VECTOR v_pos_1, VECTOR v_pos_2, FLOAT f_width)
	IF NOT IS_PED_INJURED(ped_1)
		IF IS_ENTITY_IN_ANGLED_AREA(ped_1, v_pos_1, v_pos_2, f_width, FALSE, TRUE, TM_ANY)
			RETURN TRUE
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(ped_2)
		IF IS_ENTITY_IN_ANGLED_AREA(ped_2, v_pos_1, v_pos_2, f_width, FALSE, TRUE, TM_ANY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(ped_3)
		IF IS_ENTITY_IN_ANGLED_AREA(ped_3, v_pos_1, v_pos_2, f_width, FALSE, TRUE, TM_ANY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the heading to set the gameplay cam so that it faces the given coord to look at.
FUNC FLOAT GET_GAMEPLAY_CAM_RELATIVE_HEADING_FOR_COORD(VECTOR vPlayerPos, VECTOR vLookAtPos) 	
    VECTOR vPosDiff = vLookAtPos - vPlayerPos
    FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(vPosDiff.x, vPosDiff.y) - GET_ENTITY_HEADING(PLAYER_PED_ID())
	
	IF fHeading > 180.0
		fheading -= 180.0
	ELIF fHeading < -180.0
		fHeading += 180.0
	ENDIF
	
    RETURN fHeading      
ENDFUNC 

/// PURPOSE:
///    Returns the pitch to set the gameplay cam so that it faces the given coord to look at.
FUNC FLOAT GET_GAMEPLAY_CAM_RELATIVE_PITCH_FOR_COORD(VECTOR vPlayerPos, VECTOR vLookAtPos)
	VECTOR vPosDiff = vLookAtPos - vPlayerPos
	VECTOR vDir = vPosDiff / VMAG(vPosDiff)

    RETURN ATAN2(vDir.z, VMAG(<<vDir.x, vDir.y, 0.0>>))
ENDFUNC

FUNC BOOL IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(PED_INDEX ped)
	IF NOT IS_PED_RAGDOLL(ped)
	AND NOT IS_PED_GETTING_UP(ped)
	AND NOT IS_PED_IN_COMBAT(ped)
	AND NOT	IS_PED_RELOADING(ped)
	AND IS_PED_WEAPON_READY_TO_SHOOT(ped)
		WEAPON_TYPE eCurrentWeapon
		WEAPON_GROUP eCurrentGroup
	
		GET_CURRENT_PED_WEAPON(ped, eCurrentWeapon)
		eCurrentGroup = GET_WEAPONTYPE_GROUP(eCurrentWeapon)
		
		IF eCurrentGroup != WEAPONGROUP_MELEE
		AND eCurrentGroup != WEAPONGROUP_INVALID
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

///Called after a ped is killed: keep track of who killed the ped and where they were killed.
PROC UPDATE_PLAYER_PED_KILLS_STATS(PED_INDEX pedDead, VECTOR vPlayerPos)
	ENTITY_INDEX entityKiller = GET_PED_SOURCE_OF_DEATH(pedDead)

	IF IS_ENTITY_A_PED(entityKiller)
		PED_INDEX pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(entityKiller)
	
		IF pedKiller = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
			IF VDIST2(GET_ENTITY_COORDS(pedDead, FALSE), vPlayerPos) < 10000.0
				iTimeLastPedKilledByMichael = GET_GAME_TIMER()
			ENDIF
		ELIF pedKiller = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
			iTimeLastPedKilledByFranklin = GET_GAME_TIMER()
		ELIF pedKiller = PLAYER_PED_ID()
			vPosOfLastPlayerKill = GET_ENTITY_COORDS(pedKiller, FALSE)
			iTimeLastPedKilledByPlayer = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

FUNC BUDDY_SHOOTOUT_ROUTE GET_CLOSEST_ROUTE_TO_PED(PED_INDEX ped)
	BUDDY_SHOOTOUT_ROUTE eClosestRoute = BUDDY_ROUTE_IN_PLAYERS_GROUP
	
	IF NOT IS_PED_INJURED(ped)
	AND GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointBulldozerRoute)
	AND GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointFrontRoute)
	AND GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointRearRoute)
		INT iClosestNodeBulldozer, iClosestNodeFront, iClosestNodeRear
		
		IF GET_PLAYER_PED_MODEL(CHAR_MICHAEL) = GET_ENTITY_MODEL(ped)
			eClosestRoute = BUDDY_ROUTE_SNIPING
		ELSE
			VECTOR vBuddyPos = GET_ENTITY_COORDS(ped)
			VECTOR vBulldozerNodePos, vFrontNodePos, vRearNodePos
		
			WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(strWaypointBulldozerRoute, vBuddyPos, iClosestNodeBulldozer)
			WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(strWaypointFrontRoute, vBuddyPos, iClosestNodeFront)
			WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(strWaypointRearRoute, vBuddyPos, iClosestNodeRear)
			
			WAYPOINT_RECORDING_GET_COORD(strWaypointBulldozerRoute, iClosestNodeBulldozer, vBulldozerNodePos)
			WAYPOINT_RECORDING_GET_COORD(strWaypointFrontRoute, iClosestNodeFront, vFrontNodePos)
			WAYPOINT_RECORDING_GET_COORD(strWaypointRearRoute, iClosestNodeRear, vRearNodePos)
			
			FLOAT fDistFromBulldozerRoute = VDIST2(vBuddyPos, vBulldozerNodePos)
			FLOAT fDistFromFrontRoute = VDIST2(vBuddyPos, vFrontNodePos)
			FLOAT fDistFromRearRoute = VDIST2(vBuddyPos, vRearNodePos)
			
			IF fDistFromBulldozerRoute < fDistFromFrontRoute
				IF fDistFromBulldozerRoute < fDistFromRearRoute
					eClosestRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
				ELSE
					eClosestRoute = BUDDY_ROUTE_REAR
				ENDIF
			ELSE
				IF fDistFromFrontRoute < fDistFromRearRoute
					eClosestRoute = BUDDY_ROUTE_FRONT_ASSAULT
				ELSE
					eClosestRoute = BUDDY_ROUTE_REAR
				ENDIF
			ENDIF
			
			//Check if the ped is currently in the bulldozer.
			IF eClosestRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
				IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
				AND IS_PED_SITTING_IN_VEHICLE(ped, sBulldozer.veh)
					eClosestRoute = BUDDY_ROUTE_BULLDOZER
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN eClosestRoute
ENDFUNC

/// PURPOSE:
///    Splits locates triggers over multiple frames to conserve execution time.
PROC CHECK_ENEMY_TRIGGER_LOCATES(INT iMaxNumLocatesPerFrame = 1)
	INT iNumLocatesCheckedThisFrame = 0

	WHILE iNumLocatesCheckedThisFrame < iMaxNumLocatesPerFrame
		IF eMissionStage = STAGE_ATTACK_SAWMILL
			SWITCH iLocateCheckIndex
				CASE 0
					IF NOT bHitLocateBallasOnBelt
						bHitLocateBallasOnBelt = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		 <<-558.177429,5305.119629,67.829933>>, <<-580.605591,5263.624023,88.293259>>, 4.000000)
					ENDIF
				BREAK
				
				CASE 1 //Alternate trigger for conveyor belt ped.
					IF NOT bHitLocateBallasOnBelt
						bHitLocateBallasOnBelt = IS_ENTITY_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), <<-581.077393,5263.983398,61.983421>>, <<-566.598206,5254.707520,74.502625>>, 4.000000)
					ENDIF
				BREAK
				
				CASE 2
					IF NOT bHitLocateBallasTimber1
						bHitLocateBallasTimber1 = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-513.981750,5303.330078,74.589127>>, <<-531.828796,5254.357910,78.010544>>, 1.750000)
					ENDIF
				BREAK
				
				CASE 3
					IF NOT bHitLocateBallasTimber1
						bHitLocateBallasTimber1 = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-531.574036,5253.477539,74.835709>>, <<-541.579407,5225.216309,78.376968>>, 1.750000)
					ENDIF
				BREAK
				
				CASE 4
					IF NOT bHitLocateBallasBulldozer1
						IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_FRANKLIN)) 
						AND IS_ENTITY_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), <<-578.975525,5378.432617,68.781471>>, <<-531.740173,5358.951660,83.436295>>, 10.500000)
							IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
								bHitLocateBallasBulldozer1 = TRUE
							ELSE
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sBulldozer.veh)
									IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
										bHitLocateBallasBulldozer1 = TRUE
									ENDIF
								ELSE
									bHitLocateBallasBulldozer1 = TRUE
								ENDIF
							ENDIF
						ELIF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_TREVOR))
						AND IS_ENTITY_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_TREVOR), <<-578.975525,5378.432617,68.781471>>, <<-531.740173,5358.951660,83.436295>>, 10.500000)
							IF eCurrentPlayer = SELECTOR_PED_TREVOR
								bHitLocateBallasBulldozer1 = TRUE
							ELSE
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sBulldozer.veh)
									IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
										bHitLocateBallasBulldozer1 = TRUE
									ENDIF
								ELSE
									bHitLocateBallasBulldozer1 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 5
					bHitLocateBallasBulldozer2 = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																	     <<-547.798279,5327.897461,83.811836>>, <<-621.139587,5351.126953,65.211105>>, 10.500000)
				
					IF NOT bHitLocateBallasBulldozer2
						IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_FRANKLIN))
						AND IS_ENTITY_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), <<-547.798279,5327.897461,83.811836>>, <<-621.139587,5351.126953,65.211105>>, 10.500000)
							IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
								bHitLocateBallasBulldozer2 = TRUE
							ELSE
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sBulldozer.veh)
									IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
										bHitLocateBallasBulldozer2 = TRUE
									ENDIF
								ELSE
									bHitLocateBallasBulldozer2 = TRUE
								ENDIF
							ENDIF
						ELIF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_TREVOR))
						AND IS_ENTITY_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_TREVOR), <<-547.798279,5327.897461,83.811836>>, <<-621.139587,5351.126953,65.211105>>, 10.500000)
							IF eCurrentPlayer = SELECTOR_PED_TREVOR
								bHitLocateBallasBulldozer2 = TRUE
							ELSE
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sBulldozer.veh)
									IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
										bHitLocateBallasBulldozer2 = TRUE
									ENDIF
								ELSE
									bHitLocateBallasBulldozer2 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 6
					IF NOT bHitLocateBallasRearTimber1
						bHitLocateBallasRearTimber1 = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-421.038300,5325.919922,105.337448>>, <<-516.910034,5335.936523,63.603600>>, 18.500000)
					ENDIF
				BREAK
				
				CASE 7
					IF NOT bHitLocateBallasRearTimber2
						bHitLocateBallasRearTimber2 = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-439.473450,5303.472656,98.535141>>, <<-518.608032,5324.276855,79.133942>>, 9.250000)
					ENDIF
				BREAK
				
				CASE 8
					IF NOT bHitLocateBallasTimber2
						bHitLocateBallasTimber2 = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-538.501160,5288.443359,72.864220>>, <<-462.794434,5238.156738,99.614388>>, 9.250000)
					ENDIF
				BREAK
				
				CASE 9
					IF NOT bHitLocateBallasRocket
						IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
							bHitLocateBallasRocket = IS_ENTITY_IN_ANGLED_AREA(sBulldozer.veh, <<-558.265198,5294.500977,69.138664>>, <<-629.746155,5269.547852,82.583916>>, 4.250000)
						ENDIF
					ENDIF
				BREAK
				
				CASE 10
					IF NOT bHitLocateBallasBulldozer3
						bHitLocateBallasBulldozer3 = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-563.001221,5294.141602,83.103325>>, <<-615.778564,5313.239746,66.259796>>, 7.000000)
					ENDIF
				BREAK
				
				CASE 11
					IF NOT bHitLocateBallasSniper
						bHitLocateBallasSniper = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-512.323181,5297.808105,75.352188>>, <<-528.553284,5122.546875,114.407562>>, 11.000000)
					ENDIF
				BREAK
				
				CASE 12
					IF NOT bHitLocateBallasBeforeTimber
						bHitLocateBallasBeforeTimber = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-563.693848,5225.039551,66.894447>>, <<-556.407288,5290.143066,95.390747>>, 7.250000)
					ENDIF
				BREAK
				
				CASE 13 //Alternate locate for BeforeTimber
					IF NOT bHitLocateBallasBeforeTimber
						bHitLocateBallasBeforeTimber = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  <<-588.445679,5264.123535,56.399567>>, <<-546.951477,5244.379883,85.201309>>, 7.250000)
					ENDIF
				BREAK
			ENDSWITCH
			
			iLocateCheckIndex++
		
			IF iLocateCheckIndex > 13
				iLocateCheckIndex = 0
			ENDIF
		ELIF eMissionStage = STAGE_GET_LAMAR_OUT
			SWITCH iLocateCheckIndex
				CASE 0
					IF NOT bHitLocateBallasPostRescueTimber
						bHitLocateBallasPostRescueTimber = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																		  		   <<-450.690033,5276.055176,94.359428>>, <<-533.646851,5304.580078,72.325470>>, 2.500000)
					ENDIF
				BREAK
				
				CASE 1
					IF NOT bHitLocateBallasPostRescueExit
						bHitLocateBallasPostRescueExit = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-598.337305,5469.928711,56.109970>>, <<-431.000751,5025.955762,204.017235>>, 8.500000)
						
					ENDIF
				BREAK
				
				CASE 2 //Alternate trigger for PostRescueExit: need this to still trigger as Michael, but also safeguard against a buddy ped running the wrong way and triggering it early.
					IF eCurrentPlayer = SELECTOR_PED_MICHAEL
						bHitLocateBallasPostRescueExit = ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_FRANKLIN), GET_PED_INDEX(CHAR_TREVOR), NULL, 
																			  	 <<-566.737305,5383.928711,68.109970>>, <<-488.800751,5165.155762,104.017235>>, 8.500000)
														 AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar) = 0
														 AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar2) = 0
														 AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueTimber) < 3
					ENDIF
				BREAK
			ENDSWITCH
			
			iLocateCheckIndex++
		
			IF iLocateCheckIndex > 2
				iLocateCheckIndex = 0
			ENDIF
		ENDIF
		
		iNumLocatesCheckedThisFrame++
	ENDWHILE
ENDPROC

PROC RESET_SHOOTOUT_VARIABLES()
	bHitLocateBallasBulldozer1 = FALSE
	bHitLocateBallasBulldozer2 = FALSE
	bHitLocateBallasRearTimber1 = FALSE
	bHitLocateBallasRearTimber2 = FALSE
	bHitLocateBallasOnBelt = FALSE
	bHitLocateBallasTimber1 = FALSE
	bBulldozerReachedDestination = FALSE
	bHitLocateBallasPostRescueTimber = FALSE
	bHitLocateBallasPostRescueExit = FALSE
	bCreatedBallaStartCars = FALSE
	bHitLocateBallasTimber2 = FALSE
	bHitLocateBallasRocket = FALSE
	bHitLocateBallasSniper = FALSE
	bHitLocateBallasBeforeTimber = FALSE
	bMichaelIsGettingFlanked = FALSE
	bPlayerGivenLookAtLamarTask = FALSE
	bMichaelHUDFlashingForRocket = FALSE
	bMichaelHUDFlashingForSniper = FALSE
	bSwitchHUDFlashingToGetLamar = FALSE
	bHitLocateBallasBulldozer3 = FALSE
	bBulldozerIsInMixGroup = FALSE
	bMichaelSetToMoveFreely = FALSE
	
	iNumTimesFiredRocket = 0
	iBulldozerLiftTimer = 0
	iBallaRocketTimer = 0
	iTimeSniperStartedAttacking = 0
	iSwitchStage = 0
	
	IF eMissionStage <= STAGE_GET_INTO_POSITION
		bEnemiesHaveBeenAlerted = FALSE
		bLamarHasBeenRescued = FALSE
		bBulldozerReachedDestination = FALSE
	ELIF eMissionStage = STAGE_ATTACK_SAWMILL
		bEnemiesHaveBeenAlerted = TRUE
		bLamarHasBeenRescued = FALSE
		bBulldozerReachedDestination = FALSE
	ELIF eMissionStage >= STAGE_GET_LAMAR_OUT
		bEnemiesHaveBeenAlerted = TRUE
		bLamarHasBeenRescued = TRUE
		bBulldozerReachedDestination = TRUE
		sLamar.iEvent = 50
	ENDIF
ENDPROC

//At this moment we need to do some cleanup for the next section, reposition vehicles so they won't be in the way for the recorded cars.
PROC REPOSITION_VEHICLES_FOR_LAMAR_ESCAPE()
	IF DOES_ENTITY_EXIST(vehBallaStart[2])
		IF WOULD_ENTITY_BE_OCCLUDED(modelBallaStartCars, GET_ENTITY_COORDS(vehBallaStart[2], FALSE), FALSE)
			SET_ENTITY_COORDS(vehBallaStart[2], <<-599.5241, 5287.4238, 69.7612>>, FALSE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehBallaStart[3])
		IF WOULD_ENTITY_BE_OCCLUDED(modelBallaStartCars, GET_ENTITY_COORDS(vehBallaStart[3], FALSE), FALSE)
			SET_ENTITY_COORDS(vehBallaStart[3], <<-551.5220, 5251.0991, 72.2283>>, FALSE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehBallaStart[4])
		IF WOULD_ENTITY_BE_OCCLUDED(modelBallaStartCars, GET_ENTITY_COORDS(vehBallaStart[4], FALSE), FALSE)
			SET_ENTITY_COORDS(vehBallaStart[4], <<-568.6070, 5270.8330, 69.8024>>, FALSE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sBulldozer.veh)
		VECTOR vCurrentBulldozerPos = GET_ENTITY_COORDS(sBulldozer.veh, FALSE)
		
		IF VDIST2(vCurrentBulldozerPos, <<-585.3985, 5283.5503, 69.2604>>) < 2500.0
			SET_ENTITY_COORDS(sBulldozer.veh, <<-585.3985, 5283.5503, 69.2604>>, FALSE)
			SET_ENTITY_ROTATION(sBulldozer.veh, <<0.0, 0.0, 213.3433>>, EULER_YXZ, FALSE)
		ENDIF
		
		IF VDIST2(vCurrentBulldozerPos, <<-509.1606, 5238.8970, 79.3041>>) < 2500.0
			SET_ENTITY_COORDS(sBulldozer.veh, <<-509.1606, 5238.8970, 79.3041>>, FALSE)
			SET_ENTITY_ROTATION(sBulldozer.veh, <<0.0, 0.0, 256.9148>>, EULER_YXZ, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_SHOOTOUT_ENEMIES()
	INT i = 0
	SEQUENCE_INDEX seq
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

	REQUEST_MODEL(modelBalla)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_SMG)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
	REQUEST_CLIP_SET("move_ped_strafing")
	
	SAWMILL_ENTRY_POINT ePlayersClosestEntryPoint = GET_CLOSEST_ENTRY_POINT_TO_COORDS(vPlayerPos)		
	
	//General check for alerting enemies.
	IF NOT bEnemiesHaveBeenAlerted
		VEHICLE_INDEX vehLast = GET_PLAYERS_LAST_VEHICLE()
	
		IF ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_MICHAEL), GET_PED_INDEX(CHAR_TREVOR), GET_PED_INDEX(CHAR_FRANKLIN),
															   <<-582.528870,5331.000977,66.464424>>, <<-477.767700,5292.183105,145.072815>>, 109.500000)
		OR IS_BULLET_IN_ANGLED_AREA(<<-499.700958,5391.089844,166.611298>>, <<-559.496155,5230.064941,63.068489>>, 188.000000, FALSE)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-499.700958,5391.089844,166.611298>>, <<-559.496155,5230.064941,63.068489>>, 188.000000)
		OR DOES_ENTITY_EXIST(vehLast) AND IS_POINT_IN_ANGLED_AREA(GET_ENTITY_COORDS(vehLast, FALSE), <<-499.700958,5391.089844,166.611298>>, <<-559.496155,5230.064941,63.068489>>, 188.000000)
			bEnemiesHaveBeenAlerted = TRUE
		ENDIF
	ENDIF
	
	//First ballas: these peds are there when the player arrives, dotted around key areas to cover all angles of entry.
	IF NOT sBallasStart[0].bIsCreated
		IF HAS_MODEL_LOADED(modelBalla)
		AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
		AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
		AND eMissionStage < STAGE_GET_LAMAR_OUT
			//Stagger creation over multiple frames to avoid spikes.
			IF NOT DOES_ENTITY_EXIST(sBallasStart[0].ped)
				sBallasStart[0].ped = CREATE_ENEMY_PED(modelBalla, <<-600.8010, 5282.0342, 69.9220>>, -123.5317, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
			ELIF NOT DOES_ENTITY_EXIST(sBallasStart[1].ped)
				sBallasStart[1].ped = CREATE_ENEMY_PED(modelBalla, <<-567.2858, 5256.0068, 69.4673>>, 62.9176, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
			ELIF NOT DOES_ENTITY_EXIST(sBallasStart[2].ped)
				sBallasStart[2].ped = CREATE_ENEMY_PED(modelBalla, <<-465.5948, 5348.7334, 83.4476>>, 160.7631, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
			ELIF NOT DOES_ENTITY_EXIST(sBallasStart[3].ped)
				sBallasStart[3].ped = CREATE_ENEMY_PED(modelBalla, <<-556.7905, 5363.8936, 69.2721>>, 74.8856, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
			ELIF NOT DOES_ENTITY_EXIST(sBallasStart[4].ped)
				sBallasStart[4].ped = CREATE_ENEMY_PED(modelBalla, <<-495.7584, 5248.2979, 85.7865>>, 104.5180, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
			ELIF NOT DOES_ENTITY_EXIST(sBallasStart[5].ped)
				sBallasStart[5].ped = CREATE_ENEMY_PED(modelBalla, <<-604.6235, 5295.0073, 69.3041>>, 291.1520, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
			ELIF NOT DOES_ENTITY_EXIST(sBallasStart[6].ped)
				sBallasStart[6].ped = CREATE_ENEMY_PED(modelBalla, <<-552.7806, 5348.8618, 73.7482>>, 291.1520, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
				
				INITIALISE_ENEMY_GROUP(sBallasStart, "Start_")
				SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(TRUE)
				iEnemyToCheckForAlert = 0
				iBallasPedToSpeak = -1
			ENDIF	
		ENDIF
	ELSE
		REPEAT COUNT_OF(sBallasStart) i 
			IF DOES_ENTITY_EXIST(sBallasStart[i].ped)
				IF NOT IS_PED_INJURED(sBallasStart[i].ped)			
					SWITCH sBallasStart[i].iEvent
						CASE 0 //Set up starting behaviour: they'll either be standing or on patrol.
							IF i = 5
								OPEN_SEQUENCE_TASK(seq)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-599.9635, 5350.1118, 69.4697>>, PEDMOVE_WALK, -1, DEFAULT, DEFAULT, 98.0563)
									TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(2000, 4000))
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-604.6235, 5295.0073, 69.3041>>, PEDMOVE_WALK, -1, DEFAULT, DEFAULT, 291.1520)
									TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(2000, 4000))
									SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(sBallasStart[i].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
							ELSE
								TASK_STAND_GUARD(sBallasStart[i].ped, GET_ENTITY_COORDS(sBallasStart[i].ped), GET_ENTITY_HEADING(sBallasStart[i].ped), "WORLD_HUMAN_GUARD_STAND")
							ENDIF
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasStart[i].ped, TRUE)
							sBallasStart[i].iEvent++
						BREAK
						
						CASE 1 //Once everything kicks off send them all to different defensive areas.
							IF NOT bEnemiesHaveBeenAlerted
								//Check if any of the enemies would be able to spot the player, only check one ped a frame to avoid execution spikes.
								IF i = iEnemyToCheckForAlert
									VECTOR vPedPos
									FLOAT fHeightDiff, fDistFromPlayer, fDistFromBuddy
									vPedPos = GET_ENTITY_COORDS(sBallasStart[i].ped)
									fDistFromPlayer = VDIST2(vPlayerPos, vPedPos)
									fDistFromBuddy = 100000.0
									
									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
										fDistFromBuddy = VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]), vPedPos)
									ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
										fDistFromBuddy = VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]), vPedPos)
									ENDIF
									
									fHeightDiff = vPedPos.z - vPlayerPos.z
								
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sBallasStart[i].ped, PLAYER_PED_ID())
									OR (i != 2 AND fDistFromPlayer < 1600.0 AND (fHeightDiff > -5.0 OR fDistFromPlayer < 100.0))
									OR (i != 2 AND fDistFromBuddy < 1600.0)
									OR (i = 2 AND fDistFromPlayer < 900.0)
									OR IS_PROJECTILE_IN_AREA(vPedPos - <<8.0, 8.0, 5.0>>, vPedPos + <<8.0, 8.0, 5.0>>, TRUE)
										iBallasPedToSpeak = i
										bEnemiesHaveBeenAlerted = TRUE
									ENDIF
								ENDIF
							ELSE
								IF i = 0
									sBallasStart[i].vDest = <<-596.4, 5287.0, 69.8>>
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasStart[i].ped, sBallasStart[i].vDest, 6.0, TRUE)
								ELIF i = 1
									sBallasStart[i].vDest = <<-565.5, 5257.3, 69.9>>
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasStart[i].ped, sBallasStart[i].vDest, 2.0, TRUE)
								ELIF i = 2
									sBallasStart[i].vDest = <<-465.8, 5348.8, 84.1>>
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasStart[i].ped, sBallasStart[i].vDest, 3.0, TRUE)
									sBallasStart[i].bBlockOpenCombat = TRUE
								ELIF i = 3
									sBallasStart[i].vDest = <<-561.2, 5377.0, 70.2>>
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasStart[i].ped, sBallasStart[i].vDest, 6.0, TRUE)
								ELIF i = 4
									sBallasStart[i].vDest = <<-513.3, 5257.5, 80.0>>
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasStart[i].ped, sBallasStart[i].vDest, 10.0, TRUE)
								ELIF i = 5
									sBallasStart[i].vDest = <<-581.3, 5292.0, 70.4>>
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasStart[i].ped, sBallasStart[i].vDest, 6.0, TRUE)
								ELIF i = 6
									sBallasStart[i].vDest = <<-554.8165, 5349.4985, 73.7563>>
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasStart[i].ped, sBallasStart[i].vDest, 6.0, TRUE)
								ENDIF
							
								SET_PED_COMBAT_PARAMS(sBallasStart[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasStart[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasStart[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasStart[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasStart[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasStart[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasStart[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasStart[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_PED_ALERTNESS(sBallasStart[i].ped, AS_MUST_GO_TO_COMBAT)
								
								sBallasStart[i].iEvent++
							ENDIF
						BREAK
					ENDSWITCH
					
					IF sBallasStart[i].iEvent != 0
					AND bEnemiesHaveBeenAlerted
						UPDATE_AI_PED_BLIP(sBallasStart[i].ped, sBallasStart[i].sBlipData)
						
						IF NOT sBallasStart[i].bForcedLowAccuracy
							IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
								SET_PED_ACCURACY(sBallasStart[i].ped, 1)
								sBallasStart[i].bForcedLowAccuracy = TRUE
							ENDIF
						ELSE
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								SET_PED_ACCURACY(sBallasStart[i].ped, 5)
								sBallasStart[i].bForcedLowAccuracy = FALSE
							ENDIF
						ENDIF
						
						//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
						IF sBallasStart[i].iEvent > 1
							IF NOT sBallasStart[i].bSetToCharge
								IF sBallasStart[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasStart[i].ped), sBallasStart[i].vDest) < 4.0
										IF (i = 0 AND ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT)
											sBallasStart[i].iTimer = 1
										ELSE
											sBallasStart[i].iTimer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasStart[i].iTimer > 10000
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
										IF (NOT sBallasStart[i].bBlockOpenCombat AND VDIST2(vPlayerPos, GET_ENTITY_COORDS(sBallasStart[i].ped)) < 625.0)
										OR (i = 0 AND ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT)
										OR (sBallasStart[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasStart[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasStart[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasStart[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasStart[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											IF i = 0
												SET_PED_CONFIG_FLAG(sBallasStart[i].ped, PCF_ShouldChargeNow, TRUE)
											ENDIF
											
											sBallasStart[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF (VDIST2(GET_ENTITY_COORDS(sBallasStart[i].ped), vPlayerPos) > 900.0 AND i != 0)
								OR eCurrentPlayer = SELECTOR_PED_MICHAEL
									SET_PED_COMBAT_ATTRIBUTES(sBallasStart[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(sBallasStart[i].ped, CA_CAN_CHARGE, FALSE)
									SET_PED_COMBAT_MOVEMENT(sBallasStart[i].ped, CM_DEFENSIVE)
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasStart[i].ped, sBallasStart[i].vDest, 3.0, TRUE)
									
									sBallasStart[i].bSetToCharge = FALSE
								ENDIF
							ENDIF
							
							IF iBallasPedToSpeak > -1
							AND iBallasPedToSpeak = i
								ADD_PED_FOR_DIALOGUE(sConversationPeds, 5, sBallasStart[i].ped, "fr2_enemy1")
							
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "Lm2_alert", CONV_PRIORITY_MEDIUM)
									iBallasPedToSpeak = -1
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						DRAW_INT_ABOVE_PED(sBallasStart[i].ped, sBallasStart[i].iEvent)
					#ENDIF
				ELSE	
					IF NOT bEnemiesHaveBeenAlerted
						bEnemiesHaveBeenAlerted = TRUE
					ENDIF
				
					UPDATE_PLAYER_PED_KILLS_STATS(sBallasStart[i].ped, vPlayerPos)
					CLEAN_UP_ENEMY_PED(sBallasStart[i])
				ENDIF
			ENDIF
		ENDREPEAT
		
		iEnemyToCheckForAlert++
		
		IF iEnemyToCheckForAlert >= COUNT_OF(sBallasStart)
			iEnemyToCheckForAlert = 0
		ENDIF
	ENDIF
	
	//Parked cars: these are always there at the sawmill.
	IF NOT bCreatedBallaStartCars
		//Only create one car a frame to keep down execution time.
		IF NOT DOES_ENTITY_EXIST(vehBallaStart[0])
			REQUEST_MODEL(modelBallaStartCars)
				
			IF HAS_MODEL_LOADED(modelBallaStartCars)
				vehBallaStart[0] = CREATE_VEHICLE(modelBallaStartCars, <<-583.9711, 5315.6089, 69.2144>>, -68.552)
			ENDIF
		ELIF NOT DOES_ENTITY_EXIST(vehBallaStart[1])
			vehBallaStart[1] = CREATE_VEHICLE(modelBallaStartCars, <<-561.6607, 5378.3745, 69.1896>>, 274.1927)
		ELIF NOT DOES_ENTITY_EXIST(vehBallaStart[2])
			vehBallaStart[2] = CREATE_VEHICLE(modelBallaStartCars, <<-596.3167, 5286.4297, 69.2252>>, 19.9865)
		ELIF NOT DOES_ENTITY_EXIST(vehBallaStart[3])
			vehBallaStart[3] = CREATE_VEHICLE(modelBallaStartCars, <<-545.0247, 5253.0581, 73.3049>>, 124.1131)
		ELIF NOT DOES_ENTITY_EXIST(vehBallaStart[4])
			vehBallaStart[4] = CREATE_VEHICLE(modelBallaStartCars, <<-569.1078, 5268.1108, 69.2658>>, 146.2613)
		ELIF NOT DOES_ENTITY_EXIST(vehBallaStart[5])
			vehBallaStart[5] = CREATE_VEHICLE(modelBallaStartCars, <<-583.5582, 5250.5132, 69.4679>>, 216.5756)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(modelBallaStartCars)
			
			REPEAT COUNT_OF(vehBallaStart) i
				IF NOT IS_ENTITY_DEAD(vehBallaStart[i])
					SET_VEHICLE_COLOURS(vehBallaStart[i], 66, 0)
					SET_VEHICLE_EXTRA_COLOURS(vehBallaStart[i], 66, 0)
				
					SET_VEHICLE_DOORS_LOCKED(vehBallaStart[i], VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehBallaStart[i], TRUE)
					//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBallaStart[i], FALSE)
				ENDIF
			ENDREPEAT
			
			bCreatedBallaStartCars = TRUE
		ENDIF
	ELSE
		IF eMissionStage = STAGE_DRIVE_HOME
		AND IS_SCREEN_FADED_IN()
			IF DOES_ENTITY_EXIST(vehBallaStart[0])
				REPEAT COUNT_OF(vehBallaStart) i
					REMOVE_VEHICLE(vehBallaStart[i])
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
	//Ped on phone: intended to be stealth killed, do not create if the player has already been seen.
	IF NOT sBallasOnPhone[0].bIsCreated
		IF eMissionStage = STAGE_GET_INTO_POSITION
		AND NOT bEnemiesHaveBeenAlerted
			IF HAS_MODEL_LOADED(modelBalla)
				sBallasOnPhone[0].ped = CREATE_ENEMY_PED(modelBalla, <<-474.2554, 5377.4683, 79.3745>>, 208.8788, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
				
				INITIALISE_ENEMY_GROUP(sBallasOnPhone, "Phone_")
				
				SET_CURRENT_PED_WEAPON(sBallasOnPhone[0].ped, WEAPONTYPE_UNARMED, TRUE)
				TASK_USE_MOBILE_PHONE(sBallasOnPhone[0].ped, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasOnPhone[0].ped, TRUE)
			ENDIF
		ENDIF
	ELSE
		REPEAT COUNT_OF(sBallasOnPhone) i 
			IF DOES_ENTITY_EXIST(sBallasOnPhone[i].ped)
				IF NOT IS_PED_INJURED(sBallasOnPhone[i].ped)			
					SWITCH sBallasOnPhone[i].iEvent
						CASE 0 //Aim at Lamar			
							FLOAT fDistFromPlayer
							fDistFromPlayer = VDIST2(GET_ENTITY_COORDS(sBallasOnPhone[i].ped), vPlayerPos)
						
							IF bEnemiesHaveBeenAlerted
							OR (fDistFromPlayer < 2500.0 AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(sBallasOnPhone[i].ped, PLAYER_PED_ID()))
								IF fDistFromPlayer > 5000.0
								AND (IS_ENTITY_OCCLUDED(sBallasOnPhone[i].ped) OR fDistFromPlayer > 10000.0)
									SET_ENTITY_HEALTH(sBallasOnPhone[i].ped, 0)
								ELSE
									SET_CURRENT_PED_WEAPON(sBallasOnPhone[i].ped, WEAPONTYPE_PISTOL, TRUE)
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasOnPhone[i].ped, GET_ENTITY_COORDS(sBallasOnPhone[i].ped), 5.0)
									SET_PED_COMBAT_PARAMS(sBallasOnPhone[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_RELATIONSHIP_GROUP_HASH(sBallasOnPhone[i].ped, relGroupBallas)
									SET_PED_COMBAT_ATTRIBUTES(sBallasOnPhone[i].ped, CA_USE_VEHICLE, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(sBallasOnPhone[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(sBallasOnPhone[i].ped, CA_CAN_CHARGE, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(sBallasOnPhone[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
									SET_COMBAT_FLOAT(sBallasOnPhone[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									SET_COMBAT_FLOAT(sBallasOnPhone[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
									SET_PED_ALERTNESS(sBallasOnPhone[i].ped, AS_MUST_GO_TO_COMBAT)
									
									CLEAR_PED_TASKS(sBallasOnPhone[i].ped)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasOnPhone[i].ped, 200.0)								
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasOnPhone[i].ped, FALSE)

									
									
									PLAY_PED_AMBIENT_SPEECH(sBallasOnPhone[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
									
									sBallasOnPhone[i].iEvent++
								ENDIF
							ELSE
								IF NOT bHasTextLabelTriggered[Lm2_onphn]
									IF fDistFromPlayer < 625.0
									AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										ADD_PED_FOR_DIALOGUE(sConversationPeds, 5, sBallasOnPhone[i].ped, "fr2_enemy1")
									
										IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "Lm2_onphn", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[Lm2_onphn] = TRUE
											ENDIF
										ELSE
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "Lm2_onphn2", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[Lm2_onphn] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
					IF sBallasOnPhone[i].iEvent != 0
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sBallasOnPhone[i].ped, PLAYER_PED_ID())
						UPDATE_AI_PED_BLIP(sBallasOnPhone[i].ped, sBallasOnPhone[i].sBlipData)
						
						TEXT_LABEL strLabel
						strLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						
						IF ARE_STRINGS_EQUAL(strLabel, "Lm2_onphn")
						OR ARE_STRINGS_EQUAL(strLabel, "Lm2_onphn2")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						DRAW_INT_ABOVE_PED(sBallasOnPhone[i].ped, sBallasOnPhone[i].iEvent)
					#ENDIF
				ELSE	
					UPDATE_PLAYER_PED_KILLS_STATS(sBallasOnPhone[i].ped, vPlayerPos)
					CLEAN_UP_ENEMY_PED(sBallasOnPhone[i])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//The following enemies only trigger after the player has alerted the starting enemies.
	IF bEnemiesHaveBeenAlerted
		//This group spawn straight after the player alerts the start Ballas. Spawn point is dependent on where the player currently is.
		IF NOT sBallasAfterAlert[0].bIsCreated
			IF HAS_MODEL_LOADED(modelBalla)
			AND HAS_CLIP_SET_LOADED("move_ped_strafing")
			AND eMissionStage < STAGE_GET_LAMAR_OUT
				//Stagger creation, and create each ped at multiple potential locations depending on where the player was when they alerted the enemies.
				IF ePlayersClosestEntryPoint = ENTRY_POINT_BY_BULLDOZER
					IF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[0].ped)
						sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-554.3489, 5358.4390, 69.6098>>, 339.5240, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[1].ped)
						sBallasAfterAlert[1].ped = CREATE_ENEMY_PED(modelBalla, <<-579.1994, 5344.5347, 69.2308>>, 322.3706, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL) 
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[2].ped)
						sBallasAfterAlert[2].ped = CREATE_ENEMY_PED(modelBalla, <<-576.9061, 5350.8096, 69.2145>>, 349.5044, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[3].ped)
						sBallasAfterAlert[3].ped = CREATE_ENEMY_PED(modelBalla, <<-585.9430, 5345.0, 69.2672>>, 280.4455, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ENDIF
				ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_CLIFF
					IF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[0].ped)
						sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-572.7003, 5346.6313, 69.2309>>, 139.4092, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[1].ped)
						sBallasAfterAlert[1].ped = CREATE_ENEMY_PED(modelBalla, <<-571.7344, 5348.9189, 69.2319>>, 154.9614, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[2].ped)
						sBallasAfterAlert[2].ped = CREATE_ENEMY_PED(modelBalla, <<-571.5287, 5351.3413, 69.2284>>, 154.9614, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[3].ped)
						sBallasAfterAlert[3].ped = CREATE_ENEMY_PED(modelBalla, <<-570.4391, 5353.6860, 69.2298>>, 154.9614, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ENDIF
				ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_SNIPER_HILL
					IF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[0].ped)
						IF VDIST2(vPlayerPos, <<-574.6, 5249.3, 70.9>>) < VDIST2(vPlayerPos, <<-551.1, 5245.5, 73.1>>)
							sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-581.5417, 5264.8799, 69.4481>>, 144.5352, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
						ELSE
							sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-561.1369, 5254.2246, 69.5090>>, 139.8273, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
						ENDIF
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[1].ped)
						sBallasAfterAlert[1].ped = CREATE_ENEMY_PED(modelBalla, <<-556.1404, 5300.7168, 80.6723>>, 155.9017, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[2].ped)
						sBallasAfterAlert[2].ped = CREATE_ENEMY_PED(modelBalla, <<-543.4486, 5247.4453, 73.7604>>, 138.4167, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[3].ped)
						sBallasAfterAlert[3].ped = CREATE_ENEMY_PED(modelBalla, <<-587.5444, 5271.0928, 69.4683>>, 131.0496, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ENDIF
				ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_METAL_BRIDGE
					IF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[0].ped)
						sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-544.0679, 5269.8926, 73.1741>>, 74.9389, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[1].ped)
						sBallasAfterAlert[1].ped = CREATE_ENEMY_PED(modelBalla, <<-563.5525, 5288.2593, 74.9328>>, 152.3262, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(sBallasAfterAlert[1].ped, <<-563.5525, 5288.2593, 74.9328>>, 1000, FALSE, 0.0, FALSE, FALSE, NULL, FALSE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sBallasAfterAlert[1].ped)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[2].ped)
						sBallasAfterAlert[2].ped = CREATE_ENEMY_PED(modelBalla, <<-593.4202, 5320.5791, 69.2144>>, 153.0497, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[3].ped)
						sBallasAfterAlert[3].ped = CREATE_ENEMY_PED(modelBalla, <<-592.1321, 5334.9941, 69.2534>>, 169.7280, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ENDIF
				ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_CONVEYOR_BELT
					IF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[0].ped)
						IF VDIST2(vPlayerPos, <<-574.6, 5249.3, 70.9>>) < VDIST2(vPlayerPos, <<-551.1, 5245.5, 73.1>>)
							sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-581.5417, 5264.8799, 69.4481>>, 144.5352, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
						ELSE
							sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-561.1369, 5254.2246, 69.5090>>, 139.8273, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
						ENDIF
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[1].ped)
						sBallasAfterAlert[1].ped = CREATE_ENEMY_PED(modelBalla, <<-544.4281, 5274.3335, 73.1741>>, 167.7283, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[2].ped)
						sBallasAfterAlert[2].ped = CREATE_ENEMY_PED(modelBalla, <<-543.4486, 5247.4453, 73.7604>>, 138.4167, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[3].ped)
						sBallasAfterAlert[3].ped = CREATE_ENEMY_PED(modelBalla, <<-587.5444, 5271.0928, 69.4683>>, 131.0496, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ENDIF
				ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_TRAIN_TUNNEL
					IF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[0].ped)
						sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-503.1779, 5326.7080, 79.2676>>, 184.6035, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[1].ped)
						sBallasAfterAlert[1].ped = CREATE_ENEMY_PED(modelBalla, <<-498.2337, 5277.7466, 79.6100>>, 150.6502, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[2].ped)
						sBallasAfterAlert[2].ped = CREATE_ENEMY_PED(modelBalla, <<-505.1916, 5286.6523, 79.5654>>, 141.7527, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[3].ped)
						sBallasAfterAlert[3].ped = CREATE_ENEMY_PED(modelBalla, <<-501.1952, 5292.9609, 79.6030>>, 169.5985, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ENDIF
				ELSE
					IF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[0].ped)
						sBallasAfterAlert[0].ped = CREATE_ENEMY_PED(modelBalla, <<-484.6786, 5324.6045, 79.6103>>, 337.5868, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[1].ped)
						sBallasAfterAlert[1].ped = CREATE_ENEMY_PED(modelBalla, <<-480.7613, 5337.8291, 81.6950>>, 329.0870, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[2].ped)
						sBallasAfterAlert[2].ped = CREATE_ENEMY_PED(modelBalla, <<-471.4462, 5315.3018, 82.4486>>, 350.6388, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterAlert[3].ped)
						sBallasAfterAlert[3].ped = CREATE_ENEMY_PED(modelBalla, <<-476.9317, 5324.8955, 79.6100>>, 333.9693, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(sBallasAfterAlert[3].ped)
					INITIALISE_ENEMY_GROUP(sBallasAfterAlert, "Alert_")
					
					ePointOfEnemySpawn = ePlayersClosestEntryPoint
					
					//When these peds are created delete some of the starting peds based on where the player peds are.
					VECTOR vBuddyPos
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						vBuddyPos = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						vBuddyPos = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					ENDIF
					
					REPEAT COUNT_OF(sBallasStart) i 
						IF NOT IS_PED_INJURED(sBallasStart[i].ped)
							FLOAT fDistFromPlayer = VDIST2(GET_ENTITY_COORDS(sBallasStart[i].ped), vPlayerPos)
							FLOAT fDistFromBuddy = VDIST2(GET_ENTITY_COORDS(sBallasStart[i].ped), vBuddyPos)
						
							IF fDistFromPlayer > 5000.0
							AND fDistFromBuddy > 5000.0
							AND (IS_ENTITY_OCCLUDED(sBallasStart[i].ped) OR fDistFromPlayer > 10000.0)
								SET_ENTITY_HEALTH(sBallasStart[i].ped, 0)
							ENDIF
						ENDIF
					ENDREPEAT
					
					//Difficulty update: If the player is in the bulldozer reduce the number of enemies.
					IF ePlayersClosestEntryPoint = ENTRY_POINT_BY_BULLDOZER
						IF NOT IS_ENTITY_DEAD(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
							IF NOT IS_PED_INJURED(sBallasAfterAlert[0].ped)
								SET_ENTITY_HEALTH(sBallasAfterAlert[0].ped, 0)
							ENDIF
							
							IF NOT IS_PED_INJURED(sBallasAfterAlert[2].ped)
								SET_ENTITY_HEALTH(sBallasAfterAlert[2].ped, 0)
							ENDIF
							
							IF NOT IS_PED_INJURED(sBallasAfterAlert[3].ped)
								SET_ENTITY_HEALTH(sBallasAfterAlert[3].ped, 0)
							ENDIF
						ENDIF
					ENDIF
					
					//Difficulty update: As this group accounts for peds in different areas it's easier to just kill the peds we want to remove.
					IF ePlayersClosestEntryPoint = ENTRY_POINT_BY_WOOD_LOGS
						SET_ENTITY_HEALTH(sBallasAfterAlert[0].ped, 0)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasAfterAlert) i 
				IF DOES_ENTITY_EXIST(sBallasAfterAlert[i].ped)
					IF NOT IS_PED_INJURED(sBallasAfterAlert[i].ped)			
						SWITCH sBallasAfterAlert[i].iEvent
							CASE 0 //Put into combat immediately.
								IF ePlayersClosestEntryPoint = ENTRY_POINT_BY_BULLDOZER								
									IF i = 0
										sBallasAfterAlert[i].vDest = <<-562.2291, 5374.9160, 69.1818>>
									ELIF i = 1
										sBallasAfterAlert[i].vDest = <<-559.8881, 5356.9600, 69.2145>>
									ELIF i = 2
										sBallasAfterAlert[i].vDest = <<-572.6667, 5362.7197, 69.2182>>
									ELIF i = 3
										sBallasAfterAlert[i].vDest = <<-577.9835, 5344.3418, 69.2178>>
									ENDIF
								ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_CLIFF								
									IF i = 0
										sBallasAfterAlert[i].vDest = <<-594.3, 5314.7, 69.2309>>
									ELIF i = 1
										sBallasAfterAlert[i].vDest = <<-604.9, 5333.7, 69.2319>>
									ELIF i = 2
										sBallasAfterAlert[i].vDest = <<-605.7, 5340.1, 69.5284>>
									ELIF i = 3
										sBallasAfterAlert[i].vDest = <<-587.1, 5369.4860, 69.7298>>
									ENDIF
								ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_SNIPER_HILL								
									IF i = 0
										sBallasAfterAlert[i].vDest = <<-567.8237, 5270.8550, 69.2604>>
									ELIF i = 1
										sBallasAfterAlert[i].vDest = <<-556.1403, 5283.8877, 76.1836>>
										SET_PED_STRAFE_CLIPSET(sBallasAfterAlert[i].ped, "move_ped_strafing")
										SET_PED_PATH_CAN_USE_CLIMBOVERS(sBallasAfterAlert[i].ped, FALSE)
									ELIF i = 2
										sBallasAfterAlert[i].vDest = <<-571.0912, 5276.4121, 69.2604>>
										
									ELIF i = 3
										sBallasAfterAlert[i].vDest = <<-580.3883, 5263.8218, 69.4455>>
									ENDIF
								ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_METAL_BRIDGE
									IF i = 0
										sBallasAfterAlert[i].vDest = <<-571.9470, 5273.3647, 69.2604>>
									ELIF i = 1
										sBallasAfterAlert[i].vDest = <<-572.3436, 5296.3540, 73.8037>>
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterAlert[i].ped, CA_USE_COVER, FALSE)
										sBallasAfterAlert[i].bBlockOpenCombat = TRUE
									ELIF i = 2
										sBallasAfterAlert[i].vDest = <<-594.5383, 5285.5923, 69.2216>>
									ELIF i = 3
										sBallasAfterAlert[i].vDest = <<-586.1855, 5316.1484, 69.2147>>
									ENDIF
								ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_CONVEYOR_BELT
									IF i = 0
										sBallasAfterAlert[i].vDest = <<-564.5952, 5272.3892, 69.2142>>
									ELIF i = 1
										sBallasAfterAlert[i].vDest = <<-556.1403, 5283.8877, 76.1836>>
										SET_PED_STRAFE_CLIPSET(sBallasAfterAlert[i].ped, "move_ped_strafing")
										SET_PED_PATH_CAN_USE_CLIMBOVERS(sBallasAfterAlert[i].ped, FALSE)
										sBallasAfterAlert[i].bBlockOpenCombat = TRUE
									ELIF i = 2
										sBallasAfterAlert[i].vDest = <<-571.0912, 5276.4121, 69.2604>>
										
									ELIF i = 3
										sBallasAfterAlert[i].vDest = <<-580.3883, 5263.8218, 69.4455>>
									ENDIF
								ELIF ePlayersClosestEntryPoint = ENTRY_POINT_BY_TRAIN_TUNNEL
									IF i = 0
										sBallasAfterAlert[i].vDest = <<-498.4009, 5303.5508, 79.5833>>
									ELIF i = 1
										sBallasAfterAlert[i].vDest = <<-507.3313, 5259.4932, 79.6100>>
									ELIF i = 2
										sBallasAfterAlert[i].vDest = <<-515.3116, 5265.8149, 79.4651>>
									ELIF i = 3
										sBallasAfterAlert[i].vDest = <<-504.5242, 5282.3179, 79.6100>>
									ENDIF
								ELSE
									IF i = 0
										sBallasAfterAlert[i].vDest = <<-464.8362, 5331.2334, 82.4351>>
										sBallasAfterAlert[i].bBlockOpenCombat = TRUE
									ELIF i = 1
										sBallasAfterAlert[i].vDest = <<-474.5163, 5340.7954, 81.7043>>
										sBallasAfterAlert[i].bBlockOpenCombat = TRUE
									ELIF i = 2
										sBallasAfterAlert[i].vDest = <<-462.8243, 5337.7476, 82.4694>>
										sBallasAfterAlert[i].bBlockOpenCombat = TRUE
									ELIF i = 3
										sBallasAfterAlert[i].vDest = <<-472.7236, 5312.4346, 85.6664>>
										SET_PED_STRAFE_CLIPSET(sBallasAfterAlert[i].ped, "move_ped_strafing")
										sBallasAfterAlert[i].bBlockOpenCombat = TRUE
									ENDIF
								ENDIF
								
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterAlert[i].ped, sBallasAfterAlert[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasAfterAlert[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasAfterAlert[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasAfterAlert[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasAfterAlert[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasAfterAlert[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasAfterAlert[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasAfterAlert[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasAfterAlert[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasAfterAlert[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasAfterAlert[i].ped, AS_MUST_GO_TO_COMBAT)
								SET_CURRENT_PED_WEAPON(sBallasAfterAlert[i].ped, WEAPONTYPE_PISTOL, TRUE)
								
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasAfterAlert[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasAfterAlert[i].iTimer = 0
								sBallasAfterAlert[i].iEvent++
							BREAK
							
							CASE 1 
								//Special cases:
								IF ePointOfEnemySpawn = ENTRY_POINT_BY_WOOD_LOGS AND i = 3
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
										IF vPlayerPos.z > 83.0
										AND VDIST2(GET_ENTITY_COORDS(sBallasAfterAlert[i].ped), sBallasAfterAlert[i].vDest) < 4.0
											sBallasAfterAlert[i].vDest = <<-466.5307, 5329.6491, 82.4978>>
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterAlert[i].ped, sBallasAfterAlert[i].vDest, 2.0)
											
											sBallasAfterAlert[i].iEvent++
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
						
						IF sBallasAfterAlert[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasAfterAlert[i].ped, sBallasAfterAlert[i].sBlipData)
							
							IF (i = 0 AND ePointOfEnemySpawn != ENTRY_POINT_BY_WOOD_LOGS)
							OR (i = 1 AND ePointOfEnemySpawn = ENTRY_POINT_BY_WOOD_LOGS)
								IF NOT bHasTextLabelTriggered[Lm2_frank]
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									AND NOT IS_SELECTOR_CAM_ACTIVE()
										BOOL bPlayBulldozerLine = FALSE
										
										IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
											IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
											OR (NOT IS_VEHICLE_SEAT_FREE(sBulldozer.veh) AND VDIST2(vPlayerPos, GET_ENTITY_COORDS(sBulldozer.veh)) < 625.0)
												bPlayBulldozerLine = TRUE
											ENDIF
										ENDIF
									
										IF bPlayBulldozerLine
											ADD_PED_FOR_DIALOGUE(sConversationPeds, 4, sBallasAfterAlert[i].ped, "fr2_enemy2")
											
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "Lm2_dozer", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[Lm2_frank] = TRUE
											ENDIF
										ELIF eCurrentPlayer = SELECTOR_PED_FRANKLIN
											ADD_PED_FOR_DIALOGUE(sConversationPeds, 5, sBallasAfterAlert[i].ped, "fr2_enemy1")
											
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "Lm2_frank", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[Lm2_frank] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//Set to lower accuracy if the player is in the bulldozer.
							IF NOT sBallasAfterAlert[i].bForcedLowAccuracy
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									SET_PED_ACCURACY(sBallasAfterAlert[i].ped, 1)
									sBallasAfterAlert[i].bForcedLowAccuracy = TRUE
								ENDIF
							ELSE
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									SET_PED_ACCURACY(sBallasAfterAlert[i].ped, 5)
									sBallasAfterAlert[i].bForcedLowAccuracy = FALSE
								ENDIF
							ENDIF
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasAfterAlert[i].bSetToCharge
								IF sBallasAfterAlert[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasAfterAlert[i].ped), sBallasAfterAlert[i].vDest) < 4.0
										sBallasAfterAlert[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasAfterAlert[i].iTimer > 10000
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
										IF (NOT sBallasAfterAlert[i].bBlockOpenCombat AND VDIST2(vPlayerPos, GET_ENTITY_COORDS(sBallasAfterAlert[i].ped)) < 900.0)
										OR (sBallasAfterAlert[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasAfterAlert[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasAfterAlert[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasAfterAlert[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasAfterAlert[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasAfterAlert[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF VDIST2(GET_ENTITY_COORDS(sBallasAfterAlert[i].ped), vPlayerPos) > 900.0
								OR eCurrentPlayer = SELECTOR_PED_MICHAEL
									SET_PED_COMBAT_ATTRIBUTES(sBallasAfterAlert[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(sBallasAfterAlert[i].ped, CA_CAN_CHARGE, FALSE)
									SET_PED_COMBAT_MOVEMENT(sBallasAfterAlert[i].ped, CM_DEFENSIVE)
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterAlert[i].ped, sBallasAfterAlert[i].vDest, 3.0, TRUE)
									
									sBallasAfterAlert[i].bSetToCharge = FALSE
								ENDIF
							ENDIF
						ENDIF

						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasAfterAlert[i].ped, sBallasAfterAlert[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasAfterAlert[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasAfterAlert[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//Ped guarding Lamar: once shot at or the player gets close he goes into combat.
		IF NOT sBallasGuardingLamar[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
					IF NOT IS_PED_INJURED(sLamar.ped)
						sBallasGuardingLamar[0].ped = CREATE_ENEMY_PED(modelBalla, <<-521.6014, 5306.6914, 79.2676>>, 39.3728, relGroupBallas, 200, 0, WEAPONTYPE_PISTOL)
						SET_PED_DROPS_WEAPONS_WHEN_DEAD(sBallasGuardingLamar[0].ped, FALSE)
						
						INITIALISE_ENEMY_GROUP(sBallasGuardingLamar, "Guard_")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasGuardingLamar) i 
				IF DOES_ENTITY_EXIST(sBallasGuardingLamar[i].ped)
					IF NOT IS_PED_INJURED(sBallasGuardingLamar[i].ped)			
						SWITCH sBallasGuardingLamar[i].iEvent
							CASE 0 //Aim at Lamar							
								IF NOT IS_PED_INJURED(sLamar.ped)
									TASK_AIM_GUN_AT_ENTITY(sBallasGuardingLamar[i].ped, sLamar.ped, -1)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasGuardingLamar[i].ped, TRUE)
									
									sBallasGuardingLamar[i].iEvent++
								ENDIF
							BREAK
							
							CASE 1 //If the player shoots at the ped then go into combat.
								IF NOT DOES_BLIP_EXIST(sBallasGuardingLamar[i].blip)
									IF DOES_BLIP_EXIST(sLamar.blip)
										sBallasGuardingLamar[i].blip = CREATE_BLIP_FOR_ENTITY(sBallasGuardingLamar[i].ped, TRUE)
									ENDIF
								ENDIF
							
								IF (NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_FRANKLIN)) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sBallasGuardingLamar[i].ped, GET_PED_INDEX(CHAR_FRANKLIN)))
								OR (NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_TREVOR)) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sBallasGuardingLamar[i].ped, GET_PED_INDEX(CHAR_TREVOR)))
								OR VDIST2(GET_ENTITY_COORDS(sBallasGuardingLamar[i].ped), vPlayerPos) < 324.0
								OR (IS_BULLET_IN_AREA(GET_ENTITY_COORDS(sBallasGuardingLamar[i].ped), 8.0) AND NOT IS_ENTITY_OCCLUDED(sBallasGuardingLamar[i].ped))
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasGuardingLamar[i].ped, GET_ENTITY_COORDS(sBallasGuardingLamar[i].ped), 5.0)
									SET_PED_COMBAT_PARAMS(sBallasGuardingLamar[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_RELATIONSHIP_GROUP_HASH(sBallasGuardingLamar[i].ped, relGroupBallas)
									SET_PED_COMBAT_ATTRIBUTES(sBallasGuardingLamar[i].ped, CA_USE_VEHICLE, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasGuardingLamar[i].ped, 200.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasGuardingLamar[i].ped, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(sBallasGuardingLamar[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
									SET_COMBAT_FLOAT(sBallasGuardingLamar[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									SET_COMBAT_FLOAT(sBallasGuardingLamar[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
									SET_PED_ALERTNESS(sBallasGuardingLamar[i].ped, AS_MUST_GO_TO_COMBAT)
									
									IF NOT DOES_BLIP_EXIST(sBallasGuardingLamar[i].blip)
										sBallasGuardingLamar[i].blip = CREATE_BLIP_FOR_ENTITY(sBallasGuardingLamar[i].ped, TRUE)
									ENDIF
									
									sBallasGuardingLamar[i].iEvent++
								ENDIF
							BREAK
						ENDSWITCH
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasGuardingLamar[i].ped, sBallasGuardingLamar[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasGuardingLamar[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasGuardingLamar[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//Bulldozer first wave
		IF NOT sBallasBulldozer1[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
					BOOL bBypassThisGroup = FALSE
				
					IF bHitLocateBallasBulldozer1
						IF NOT IS_ENTITY_DEAD(sBulldozer.veh)							
							//If an AI buddy is in the bulldozer then the player must be on the same route to trigger this.
							IF (NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sBulldozer.veh))
							OR (NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sBulldozer.veh))
								IF ePlayerRoute != BUDDY_ROUTE_BULLDOZER_ON_FOOT
									#IF IS_DEBUG_BUILD
										PRINTLN("Franklin2.sc - Bulldozer1 group not created: AI was in bulldozer, but player wasn't with them.")
									#ENDIF
								
									bBypassThisGroup = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-597.861523,5345.111328,54.719841>>, <<-666.824890,5242.858398,102.606491>>, 112.250000)
							#IF IS_DEBUG_BUILD
								PRINTLN("Franklin2.sc - Bulldozer1 group not created: Player is in a position to see them spawn.")
							#ENDIF
							
							bBypassThisGroup = TRUE
						ENDIF
					
						IF bBypassThisGroup
							REPEAT COUNT_OF(sBallasBulldozer1) i 
								sBallasBulldozer1[i].bIsCreated = TRUE
							ENDREPEAT
						ELSE
							IF NOT DOES_ENTITY_EXIST(sBallasBulldozer1[0].ped)
								sBallasBulldozer1[0].ped = CREATE_ENEMY_PED(modelBalla, <<-565.0798, 5326.8994, 69.2400>>, 71.9622, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
							ELIF NOT DOES_ENTITY_EXIST(sBallasBulldozer1[1].ped)
								sBallasBulldozer1[1].ped = CREATE_ENEMY_PED(modelBalla, <<-592.5015, 5334.4028, 69.2620>>, 252.6935, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELIF NOT DOES_ENTITY_EXIST(sBallasBulldozer1[2].ped)
								sBallasBulldozer1[2].ped = CREATE_ENEMY_PED(modelBalla, <<-594.6821, 5335.3022, 69.3288>>, 350.3948, relGroupBallas, 200, 0, WEAPONTYPE_SMG)

								INITIALISE_ENEMY_GROUP(sBallasBulldozer1, "Bull1_")
								
								//Difficulty update: If the player is in the bulldozer reduce the number of enemies.
								IF NOT IS_ENTITY_DEAD(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									IF NOT IS_PED_INJURED(sBallasBulldozer1[1].ped)
										SET_ENTITY_HEALTH(sBallasBulldozer1[1].ped, 0)
									ENDIF
									
									IF NOT IS_PED_INJURED(sBallasBulldozer1[2].ped)
										SET_ENTITY_HEALTH(sBallasBulldozer1[2].ped, 0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasBulldozer1) i 
				IF DOES_ENTITY_EXIST(sBallasBulldozer1[i].ped)
					IF NOT IS_PED_INJURED(sBallasBulldozer1[i].ped)			
						SWITCH sBallasBulldozer1[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasBulldozer1[i].vDest = <<-571.5660, 5329.7129, 69.2144>>
								ELIF i = 1
									sBallasBulldozer1[i].vDest = <<-578.4724, 5338.5435, 69.2144>>
								ELIF i = 2
									sBallasBulldozer1[i].vDest = <<-566.1284, 5339.8271, 69.2144>>
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasBulldozer1[i].ped, sBallasBulldozer1[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasBulldozer1[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasBulldozer1[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer1[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasBulldozer1[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasBulldozer1[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer1[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasBulldozer1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasBulldozer1[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasBulldozer1[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasBulldozer1[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasBulldozer1[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasBulldozer1[i].iTimer = 0
								sBallasBulldozer1[i].iEvent++
							BREAK
						ENDSWITCH
						
						IF sBallasBulldozer1[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasBulldozer1[i].ped, sBallasBulldozer1[i].sBlipData)
							
							IF NOT sBallasBulldozer1[i].bForcedLowAccuracy
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									SET_PED_ACCURACY(sBallasBulldozer1[i].ped, 1)
									sBallasBulldozer1[i].bForcedLowAccuracy = TRUE
								ENDIF
							ELSE
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									SET_PED_ACCURACY(sBallasBulldozer1[i].ped, 5)
									sBallasBulldozer1[i].bForcedLowAccuracy = FALSE
								ENDIF
							ENDIF
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasBulldozer1[i].bSetToCharge
								IF sBallasBulldozer1[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasBulldozer1[i].ped), sBallasBulldozer1[i].vDest) < 4.0
										sBallasBulldozer1[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasBulldozer1[i].iTimer > 10000
									IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER
									OR ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
										IF (NOT sBallasBulldozer1[i].bBlockOpenCombat)
										OR (sBallasBulldozer1[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasBulldozer1[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer1[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasBulldozer1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasBulldozer1[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF ePlayerRoute != BUDDY_ROUTE_BULLDOZER
								AND ePlayerRoute != BUDDY_ROUTE_BULLDOZER_ON_FOOT
									IF VDIST2(GET_ENTITY_COORDS(sBallasBulldozer1[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer1[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasBulldozer1[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasBulldozer1[i].ped, sBallasBulldozer1[i].vDest, 3.0, TRUE)
										
										sBallasBulldozer1[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasBulldozer1[i].ped, sBallasBulldozer1[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasBulldozer1[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasBulldozer1[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		//Bulldozer second wave
		IF NOT sBallasBulldozer2[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
					BOOL bTriggerBasedOnDeaths = FALSE
					BOOL bBypassThisGroup = FALSE
					
					IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasBulldozer1) <= 1
					AND (ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT OR ePlayerRoute = BUDDY_ROUTE_BULLDOZER)
						bTriggerBasedOnDeaths = TRUE
					ENDIF
				
					//This group is only safe to trigger in particular scenarios, otherwise we risk swamping the bulldozer and/or the other AI ped.
					IF bHitLocateBallasBulldozer2 OR bTriggerBasedOnDeaths
						IF NOT IS_ENTITY_DEAD(sBulldozer.veh)
							//If the player is in the bulldozer or it's empty then just make sure no player/AI peds aren't in front.
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
							OR IS_VEHICLE_SEAT_FREE(sBulldozer.veh, VS_DRIVER)
								IF ARE_PEDS_IN_ANGLED_AREA(GET_PED_INDEX(CHAR_TREVOR), GET_PED_INDEX(CHAR_FRANKLIN), NULL, 
														   <<-503.459473,5236.240723,102.546524>>, <<-618.606689,5292.703613,55.009380>>, 40.500000)
									#IF IS_DEBUG_BUILD
										PRINTLN("Franklin2.sc - Bulldozer2 group not created: a player ped was up ahead.")
									#ENDIF
								
									bBypassThisGroup = TRUE
								ENDIF
							ENDIF
							
							//If an AI buddy is in the bulldozer then the player must be on the same route to trigger this.
							IF (NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sBulldozer.veh))
							OR (NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sBulldozer.veh))
								IF ePlayerRoute != BUDDY_ROUTE_BULLDOZER_ON_FOOT
									#IF IS_DEBUG_BUILD
										PRINTLN("Franklin2.sc - Bulldozer2 group not created: AI was in bulldozer, but player wasn't with them.")
									#ENDIF
								
									bBypassThisGroup = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-601.561523,5333.111328,54.719841>>, <<-666.824890,5242.858398,102.606491>>, 112.250000)
							#IF IS_DEBUG_BUILD
								PRINTLN("Franklin2.sc - Bulldozer2 group not created: Player is in a position to see them spawn.")
							#ENDIF
							
							bBypassThisGroup = TRUE
						ENDIF
			
						IF bBypassThisGroup
							REPEAT COUNT_OF(sBallasBulldozer2) i 
								sBallasBulldozer2[i].bIsCreated = TRUE
							ENDREPEAT
						ELSE
							IF NOT DOES_ENTITY_EXIST(sBallasBulldozer2[0].ped)
								sBallasBulldozer2[0].ped = CREATE_ENEMY_PED(modelBalla, <<-583.9528, 5284.0156, 69.2604>>, 345.9741, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
							ELIF NOT DOES_ENTITY_EXIST(sBallasBulldozer2[1].ped)
								sBallasBulldozer2[1].ped = CREATE_ENEMY_PED(modelBalla, <<-582.0480, 5282.9438, 69.6424>>, 71.8583, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELIF NOT DOES_ENTITY_EXIST(sBallasBulldozer2[2].ped)
								sBallasBulldozer2[2].ped = CREATE_ENEMY_PED(modelBalla, <<-595.5361, 5315.0264, 69.2144>>, 309.6455, relGroupBallas, 200, 0, WEAPONTYPE_SMG)

								INITIALISE_ENEMY_GROUP(sBallasBulldozer2, "Bull2_")
								
								//Difficulty update: If the player is in the bulldozer reduce the number of enemies.
								IF NOT IS_ENTITY_DEAD(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									IF NOT IS_PED_INJURED(sBallasBulldozer2[1].ped)
										SET_ENTITY_HEALTH(sBallasBulldozer2[1].ped, 0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasBulldozer2) i 
				IF DOES_ENTITY_EXIST(sBallasBulldozer2[i].ped)
					IF NOT IS_PED_INJURED(sBallasBulldozer2[i].ped)			
						SWITCH sBallasBulldozer2[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasBulldozer2[i].vDest = <<-584.1713, 5311.3719, 69.2144>>
								ELIF i = 1
									sBallasBulldozer2[i].vDest = <<-573.7343, 5315.2500, 69.1795>>
								ELIF i = 2
									sBallasBulldozer2[i].vDest = <<-583.9662, 5286.5220, 69.2604>>
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasBulldozer2[i].ped, sBallasBulldozer2[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasBulldozer2[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasBulldozer2[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer2[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasBulldozer2[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasBulldozer2[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer2[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasBulldozer2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasBulldozer2[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasBulldozer2[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasBulldozer2[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasBulldozer2[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasBulldozer2[i].iTimer = 0
								sBallasBulldozer2[i].iEvent++
							BREAK
						ENDSWITCH
						
						IF sBallasBulldozer2[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasBulldozer2[i].ped, sBallasBulldozer2[i].sBlipData)
							
							//Set to lower accuracy if the player is in the bulldozer.
							IF NOT sBallasBulldozer2[i].bForcedLowAccuracy
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									SET_PED_ACCURACY(sBallasBulldozer2[i].ped, 1)
									sBallasBulldozer2[i].bForcedLowAccuracy = TRUE
								ENDIF
							ELSE
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									SET_PED_ACCURACY(sBallasBulldozer2[i].ped, 5)
									sBallasBulldozer2[i].bForcedLowAccuracy = FALSE
								ENDIF
							ENDIF
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasBulldozer2[i].bSetToCharge
								IF sBallasBulldozer2[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasBulldozer2[i].ped), sBallasBulldozer2[i].vDest) < 4.0
										sBallasBulldozer2[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasBulldozer2[i].iTimer > 10000
									IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER
									OR ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
										IF (NOT sBallasBulldozer2[i].bBlockOpenCombat)
										OR (sBallasBulldozer2[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasBulldozer2[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer2[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasBulldozer2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasBulldozer2[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF ePlayerRoute != BUDDY_ROUTE_BULLDOZER
								AND ePlayerRoute != BUDDY_ROUTE_BULLDOZER_ON_FOOT
									IF VDIST2(GET_ENTITY_COORDS(sBallasBulldozer2[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer2[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasBulldozer2[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasBulldozer2[i].ped, sBallasBulldozer2[i].vDest, 3.0, TRUE)
										
										sBallasBulldozer2[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasBulldozer2[i].ped, sBallasBulldozer2[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasBulldozer2[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasBulldozer2[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//Bulldozer third wave.
		IF NOT sBallasBulldozer3[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)				
					//This group is only safe to trigger in particular scenarios, otherwise we risk swamping the bulldozer and/or the other AI ped.
					IF bHitLocateBallasBulldozer3 
					OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasBulldozer2) = 0
						IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT OR ePlayerRoute = BUDDY_ROUTE_BULLDOZER
							IF NOT DOES_ENTITY_EXIST(sBallasBulldozer3[0].ped)
								sBallasBulldozer3[0].ped = CREATE_ENEMY_PED(modelBalla, <<-562.3906, 5254.9883, 69.4939>>, 64.2948, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELIF NOT DOES_ENTITY_EXIST(sBallasBulldozer3[1].ped)
								sBallasBulldozer3[1].ped = CREATE_ENEMY_PED(modelBalla, <<-568.1168, 5236.5229, 69.4282>>, 152.2310, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELIF NOT DOES_ENTITY_EXIST(sBallasBulldozer3[2].ped)
								sBallasBulldozer3[2].ped = CREATE_ENEMY_PED(modelBalla, <<-558.9417, 5266.6992, 71.3148>>, 54.0498, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)

								INITIALISE_ENEMY_GROUP(sBallasBulldozer3, "Bull3_")
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("franklin2.sc - Did not create bulldozer3, player route is ", ENUM_TO_INT(ePlayerRoute))
							#ENDIF
						
							REPEAT COUNT_OF(sBallasBulldozer3) i 
								sBallasBulldozer3[i].bIsCreated = TRUE
							ENDREPEAT
						ENDIF
						
						//Difficulty update: If the player is in the bulldozer reduce the number of enemies.
						IF NOT IS_ENTITY_DEAD(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
							IF NOT IS_PED_INJURED(sBallasBulldozer3[2].ped)
								SET_ENTITY_HEALTH(sBallasBulldozer3[2].ped, 0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasBulldozer3) i 
				IF DOES_ENTITY_EXIST(sBallasBulldozer3[i].ped)
					IF NOT IS_PED_INJURED(sBallasBulldozer3[i].ped)			
						SWITCH sBallasBulldozer3[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasBulldozer3[i].vDest = <<-571.5106, 5274.3599, 69.2604>>
								ELIF i = 1
									sBallasBulldozer3[i].vDest = <<-568.8, 5265.7, 69.8>>
								ELIF i = 2
									sBallasBulldozer3[i].vDest = <<-580.0616, 5262.9087, 69.4442>>
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasBulldozer3[i].ped, sBallasBulldozer3[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasBulldozer3[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasBulldozer3[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer3[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasBulldozer3[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasBulldozer3[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer3[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasBulldozer3[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasBulldozer3[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasBulldozer3[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasBulldozer3[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasBulldozer3[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasBulldozer3[i].iTimer = 0
								sBallasBulldozer3[i].iEvent++
							BREAK
						ENDSWITCH
						
						IF sBallasBulldozer3[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasBulldozer3[i].ped, sBallasBulldozer3[i].sBlipData)
							
							//Set to lower accuracy if the player is in the bulldozer.
							IF NOT sBallasBulldozer3[i].bForcedLowAccuracy
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									SET_PED_ACCURACY(sBallasBulldozer3[i].ped, 1)
									sBallasBulldozer3[i].bForcedLowAccuracy = TRUE
								ENDIF
							ELSE
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									SET_PED_ACCURACY(sBallasBulldozer3[i].ped, 5)
									sBallasBulldozer3[i].bForcedLowAccuracy = FALSE
								ENDIF
							ENDIF
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasBulldozer3[i].bSetToCharge
								IF sBallasBulldozer3[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasBulldozer3[i].ped), sBallasBulldozer3[i].vDest) < 4.0
										sBallasBulldozer3[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasBulldozer3[i].iTimer > 10000
									IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER
									OR ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
										IF (NOT sBallasBulldozer3[i].bBlockOpenCombat)
										OR (sBallasBulldozer3[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasBulldozer3[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer3[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer3[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasBulldozer3[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasBulldozer3[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF ePlayerRoute != BUDDY_ROUTE_BULLDOZER
								AND ePlayerRoute != BUDDY_ROUTE_BULLDOZER_ON_FOOT
									IF VDIST2(GET_ENTITY_COORDS(sBallasBulldozer3[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer3[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasBulldozer3[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasBulldozer3[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasBulldozer3[i].ped, sBallasBulldozer3[i].vDest, 3.0, TRUE)
										
										sBallasBulldozer3[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasBulldozer3[i].ped, sBallasBulldozer3[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasBulldozer3[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasBulldozer3[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//Rocket ped: takes position on the roof and shoots at the bulldozer.
		IF NOT sBallasRocket[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_CLIP_SET_LOADED("move_ped_strafing")
					IF bHitLocateBallasRocket
						IF NOT DOES_ENTITY_EXIST(sBallasRocket[0].ped)
							BOOL bSafeToCreate = FALSE
						
							IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									bSafeToCreate = TRUE
								ELSE
									IF (NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sBulldozer.veh))
									OR (NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sBulldozer.veh))
										IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
										OR ePlayerRoute = BUDDY_ROUTE_SNIPING
										OR ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT
											IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber2) > 0 //Don't create if the player already got to Lamar
												bSafeToCreate = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							IF bSafeToCreate
								sBallasRocket[0].ped = CREATE_ENEMY_PED(modelBalla, <<-544.1907, 5296.9399, 87.4445>>, 157.0515, relGroupBallas, 200, 0, WEAPONTYPE_RPG)
								SET_PED_STRAFE_CLIPSET(sBallasRocket[0].ped, "move_ped_strafing")
						
								INITIALISE_ENEMY_GROUP(sBallasRocket, "Rocket_")
							ELSE
								KILL_ENEMY_GROUP_INSTANTLY(sBallasRocket)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			VECTOR vShootPos
			INT iClosestNode
			FLOAT fDistFromTrevor = 0.0
			FLOAT fDistFromFranklin = 0.0

			REPEAT COUNT_OF(sBallasRocket) i 
				IF DOES_ENTITY_EXIST(sBallasRocket[i].ped)
					IF NOT IS_PED_INJURED(sBallasRocket[i].ped)			
						SWITCH sBallasRocket[i].iEvent
							CASE 0 	
								sBallasRocket[i].vDest = <<-546.0917, 5286.8765, 87.3861>>
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasRocket[i].ped, sBallasRocket[i].vDest, 1.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasRocket[i].ped, 10, CM_STATIONARY, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasRocket[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasRocket[i].ped, CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasRocket[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 							
								SET_COMBAT_FLOAT(sBallasRocket[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasRocket[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasRocket[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								
								OPEN_SEQUENCE_TASK(seq)
									IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, sBallasRocket[i].vDest, sBulldozer.veh, PEDMOVE_RUN, FALSE, 0.25, 4.0, FALSE, ENAV_STOP_EXACTLY)
										TASK_AIM_GUN_AT_ENTITY(NULL, sBulldozer.veh, -1)
									ELSE
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, sBallasRocket[i].vDest, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 0.25, 4.0, FALSE, ENAV_STOP_EXACTLY)
										TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
									ENDIF
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(sBallasRocket[i].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								
								PLAY_PED_AMBIENT_SPEECH(sBallasRocket[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								
								sBallasRocket[i].iTimer = 0
								sBallasRocket[i].iEvent++
							BREAK
							
							CASE 1 //Fire rockets based on the bulldozer's position: get closer the more rockets fired.
								IF VDIST2(GET_ENTITY_COORDS(sBallasRocket[i].ped), sBallasRocket[i].vDest) < 4.0
									IF sBallasRocket[i].iTimer = 0
										sBallasRocket[i].iTimer = GET_GAME_TIMER() + 1000
									ELIF GET_GAME_TIMER() - sBallasRocket[i].iTimer > 0
										IF NOT IS_ENTITY_DEAD(sBulldozer.veh)
											IF iNumTimesFiredRocket < 4
												IF GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointBulldozer)
													WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(strWaypointBulldozer, GET_ENTITY_COORDS(sBulldozer.veh), iClosestNode)
													WAYPOINT_RECORDING_GET_COORD(strWaypointBulldozer, iClosestNode + (4 - iNumTimesFiredRocket), vShootPos)
													
													vShootPos.x += GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0)
													vShootPos.y += GET_RANDOM_FLOAT_IN_RANGE(-1.0, 1.0)
													
													//First rocket always takes out the car.
													IF iNumTimesFiredRocket = 0
														vShootPos = <<-570.1, 5266.5, 70.9>>
													ENDIF
													
													SET_PED_SHOOTS_AT_COORD(sBallasRocket[i].ped, vShootPos)
													
													iNumTimesFiredRocket++
													sBallasRocket[i].iTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5000)
												ENDIF
											ELSE
												IF eCurrentPlayer != SELECTOR_PED_MICHAEL
													SET_PED_SHOOTS_AT_COORD(sBallasRocket[i].ped, GET_ENTITY_COORDS(PLAYER_PED_ID()))
												ELSE
													IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
													AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
														fDistFromFranklin = VDIST2(GET_ENTITY_COORDS(sBallasRocket[i].ped), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
														fDistFromTrevor = VDIST2(GET_ENTITY_COORDS(sBallasRocket[i].ped), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))
														
														IF fDistFromFranklin < fDistFromTrevor
															SET_PED_SHOOTS_AT_COORD(sBallasRocket[i].ped, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
														ELSE
															SET_PED_SHOOTS_AT_COORD(sBallasRocket[i].ped, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))
														ENDIF
													ENDIF
												ENDIF
												
												iNumTimesFiredRocket++
												sBallasRocket[i].iTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5000)
											ENDIF
										ELSE
											//Bulldozer already dead, so just go into combat.
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasRocket[i].ped, 300.0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasRocket[i].ped, FALSE)
											
											sBallasRocket[i].iEvent++
										ENDIF
									ENDIF
								ELSE
									IF eMissionStage >= STAGE_GET_LAMAR_OUT
										//Bulldozer already dead, so just go into combat.
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasRocket[i].ped, 300.0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasRocket[i].ped, FALSE)
										
										sBallasRocket[i].iEvent++
									ENDIF
								ENDIF
							BREAK
							
							CASE 2
							
							BREAK
						ENDSWITCH
						
						IF sBallasRocket[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasRocket[i].ped, sBallasRocket[i].sBlipData)
							
							//Kill if they fall off the roof.
							VECTOR vPos = GET_ENTITY_COORDS(sBallasRocket[i].ped)
						
							IF vPos.z < 84.0
								APPLY_DAMAGE_TO_PED(sBallasRocket[i].ped, 300, TRUE)
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasRocket[i].ped, sBallasRocket[i].iEvent)
						#ENDIF
					ELSE	
						IF i = 0
							//Record footage of the player killing the ped.
							REPLAY_RECORD_BACK_FOR_TIME(1.0)
						ENDIF
					
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasRocket[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasRocket[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		//Sniper ped: alternative to the rocket ped.
		IF NOT sBallasSniper[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_CLIP_SET_LOADED("move_ped_strafing")
					IF bHitLocateBallasSniper
						IF NOT DOES_ENTITY_EXIST(sBallasSniper[0].ped)
							IF g_bFranklin2RequestedBackup
								IF ePlayerRoute != BUDDY_ROUTE_REAR						
									sBallasSniper[0].ped = CREATE_ENEMY_PED(modelBalla, <<-503.8309, 5323.5601, 88.0160>>, 158.6523, relGroupBallas, 200, 0, WEAPONTYPE_SNIPERRIFLE)
								ELSE
									sBallasSniper[0].ped = CREATE_ENEMY_PED(modelBalla, <<-510.6272, 5308.1636, 87.9901>>, 158.8536, relGroupBallas, 200, 0, WEAPONTYPE_SNIPERRIFLE)
								ENDIF
								
								SET_PED_STRAFE_CLIPSET(sBallasSniper[0].ped, "move_ped_strafing")
								INITIALISE_ENEMY_GROUP(sBallasSniper, "Sniper_")
							ELSE
								sBallasSniper[0].bIsCreated = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			VECTOR vPos
		
			REPEAT COUNT_OF(sBallasSniper) i 
				IF DOES_ENTITY_EXIST(sBallasSniper[i].ped)
					IF NOT IS_PED_INJURED(sBallasSniper[i].ped)			
						SWITCH sBallasSniper[i].iEvent
							CASE 0 	
								sBallasSniper[i].vDest = <<-512.1504, 5308.8306, 88.0628>>
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasSniper[i].ped, sBallasSniper[i].vDest, 1.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasSniper[i].ped, 40, CM_STATIONARY, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasSniper[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasSniper[i].ped, CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasSniper[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 							
								SET_COMBAT_FLOAT(sBallasSniper[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasSniper[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasSniper[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								
								OPEN_SEQUENCE_TASK(seq)
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, sBallasSniper[i].vDest, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 0.25, 4.0, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 500.0)
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(sBallasSniper[i].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								
								sBallasSniper[i].blip = CREATE_BLIP_FOR_PED(sBallasSniper[i].ped, TRUE)
								iTimeSniperStartedAttacking = GET_GAME_TIMER()
								sBallasSniper[i].iTimer = 0
								sBallasSniper[i].iEvent++
							BREAK
						ENDSWITCH
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasSniper[i].ped, sBallasSniper[i].iEvent)
						#ENDIF
						
						vPos = GET_ENTITY_COORDS(sBallasSniper[i].ped)
						
						IF vPos.z < 85.0
							APPLY_DAMAGE_TO_PED(sBallasSniper[i].ped, 300, TRUE)
						ENDIF
					ELSE	
						IF i = 0
							//Record footage of the player killing the ped
							REPLAY_RECORD_BACK_FOR_TIME(1.0)
						ENDIF
					
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasSniper[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasSniper[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		//Roof ped: only turns up if the player started on the front route.
		IF NOT sBallasFrontExtra[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_CLIP_SET_LOADED("move_ped_strafing")
					IF sBallasAfterAlert[0].bIsCreated
					AND (ePointOfEnemySpawn = ENTRY_POINT_BY_METAL_BRIDGE OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterAlert) < 4)
						IF (ePointOfEnemySpawn = ENTRY_POINT_BY_CONVEYOR_BELT OR ePointOfEnemySpawn = ENTRY_POINT_BY_METAL_BRIDGE OR ePointOfEnemySpawn = ENTRY_POINT_BY_SNIPER_HILL)
						AND (ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT OR ePlayerRoute = BUDDY_ROUTE_SNIPING)
						AND g_bFranklin2RequestedBackup
							IF ePointOfEnemySpawn = ENTRY_POINT_BY_METAL_BRIDGE
								//Note: there used to be enemies here, currently they're gone for difficulty testing.
								sBallasFrontExtra[0].bIsCreated = TRUE
								sBallasFrontExtra[1].bIsCreated = TRUE
								sBallasFrontExtra[2].bIsCreated = TRUE
								
								INITIALISE_ENEMY_GROUP(sBallasFrontExtra, "Extra_")
							ELSE
								IF NOT DOES_ENTITY_EXIST(sBallasFrontExtra[0].ped)			
									sBallasFrontExtra[0].ped = CREATE_ENEMY_PED(modelBalla, <<-537.4317, 5292.5913, 88.1882>>, 162.6303, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
									SET_PED_STRAFE_CLIPSET(sBallasFrontExtra[0].ped, "move_ped_strafing")
								ELSE
									sBallasFrontExtra[1].bIsCreated = TRUE
									sBallasFrontExtra[2].bIsCreated = TRUE
									
									INITIALISE_ENEMY_GROUP(sBallasFrontExtra, "Extra_")
								ENDIF
							ENDIF
						ELSE
							sBallasFrontExtra[0].bIsCreated = TRUE
							sBallasFrontExtra[1].bIsCreated = TRUE
							sBallasFrontExtra[2].bIsCreated = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			VECTOR vPos
		
			REPEAT COUNT_OF(sBallasFrontExtra) i 
				IF DOES_ENTITY_EXIST(sBallasFrontExtra[i].ped)
					IF NOT IS_PED_INJURED(sBallasFrontExtra[i].ped)			
						SWITCH sBallasFrontExtra[i].iEvent
							CASE 0 	
								IF i = 0
									IF ePointOfEnemySpawn = ENTRY_POINT_BY_METAL_BRIDGE
										sBallasFrontExtra[i].vDest = <<-569.2231, 5302.5654, 82.7490>>
									ELSE
										sBallasFrontExtra[i].vDest = <<-539.9885, 5285.0991, 87.4831>>
									ENDIF
								ELIF i = 1
									sBallasFrontExtra[i].vDest = <<-586.1855, 5316.1484, 69.2147>>
								ELIF i = 2
									sBallasFrontExtra[i].vDest = <<-566.7495, 5269.2300, 69.2973>>
								ENDIF
								
								IF i = 0
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasFrontExtra[i].ped, sBallasFrontExtra[i].vDest, 1.0, TRUE)
									SET_PED_COMBAT_PARAMS(sBallasFrontExtra[i].ped, 10, CM_STATIONARY, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								ELSE
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasFrontExtra[i].ped, sBallasFrontExtra[i].vDest, 2.0, TRUE)
									SET_PED_COMBAT_PARAMS(sBallasFrontExtra[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								ENDIF
								
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasFrontExtra[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasFrontExtra[i].ped, CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasFrontExtra[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 							
								SET_COMBAT_FLOAT(sBallasFrontExtra[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasFrontExtra[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasFrontExtra[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								
								IF i = 0
									OPEN_SEQUENCE_TASK(seq)
										TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, sBallasFrontExtra[i].vDest, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 0.25, 4.0, FALSE)
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
										TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(sBallasFrontExtra[i].ped, seq)
									CLEAR_SEQUENCE_TASK(seq)
								ELSE
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasFrontExtra[i].ped, 200.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasFrontExtra[i].ped, FALSE)
								ENDIF
								
								sBallasFrontExtra[i].iTimer = 0
								sBallasFrontExtra[i].iEvent++
							BREAK
						ENDSWITCH
						
						IF sBallasFrontExtra[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasFrontExtra[i].ped, sBallasFrontExtra[i].sBlipData)
							
							//Kill the ped on the high roof if they fall off.
							IF i = 0
							AND ePointOfEnemySpawn != ENTRY_POINT_BY_METAL_BRIDGE
								vPos = GET_ENTITY_COORDS(sBallasFrontExtra[i].ped)
						
								IF vPos.z < 85.0
									APPLY_DAMAGE_TO_PED(sBallasFrontExtra[i].ped, 300, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasFrontExtra[i].ped, sBallasFrontExtra[i].iEvent)
						#ENDIF
						
						
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasFrontExtra[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasFrontExtra[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		//Rear peds first wave
		IF NOT sBallasRearTimber1[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
					IF bHitLocateBallasRearTimber1
						IF NOT sBallasTimber2[0].bIsCreated AND vPlayerPos.y > 5315.2 //The player may be on the rear route but still in view of spawn points, so do another check here.
							IF NOT DOES_ENTITY_EXIST(sBallasRearTimber1[0].ped)
								IF ePlayerRoute != BUDDY_ROUTE_FRONT_ASSAULT AND ePlayerRoute != BUDDY_ROUTE_SNIPING
									sBallasRearTimber1[0].ped = CREATE_ENEMY_PED(modelBalla, <<-510.4515, 5304.9263, 79.2676>>, 341.0726, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
								ELSE
									sBallasRearTimber1[0].ped = CREATE_ENEMY_PED(modelBalla, <<-502.4980, 5298.0488, 79.5761>>, 70.1201, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
								ENDIF
							ELIF NOT DOES_ENTITY_EXIST(sBallasRearTimber1[1].ped)
								IF VDIST2(vPlayerPos, <<-462.7, 5341.5, 83.4>>) < 400.0
								OR ePlayerRoute != BUDDY_ROUTE_REAR
									sBallasRearTimber1[1].ped = CREATE_ENEMY_PED(modelBalla, <<-485.4689, 5324.9492, 79.6100>>, 349.1465, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
								ELSE
									sBallasRearTimber1[1].ped = CREATE_ENEMY_PED(modelBalla, <<-504.8583, 5308.1387, 79.2676>>, 320.3648, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
								ENDIF
							ELIF NOT DOES_ENTITY_EXIST(sBallasRearTimber1[2].ped)
								IF VDIST2(vPlayerPos, <<-462.7, 5341.5, 83.4>>) < 400.0
								OR ePlayerRoute != BUDDY_ROUTE_REAR
									sBallasRearTimber1[2].ped = CREATE_ENEMY_PED(modelBalla, <<-491.6151, 5327.0200, 79.5436>>, 334.4004, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
								ELSE
									sBallasRearTimber1[2].ped = CREATE_ENEMY_PED(modelBalla, <<-505.3082, 5306.3003, 79.2676>>, 325.5638, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
								ENDIF
							ELIF NOT DOES_ENTITY_EXIST(sBallasRearTimber1[3].ped)
								IF ePlayerRoute != BUDDY_ROUTE_FRONT_ASSAULT AND ePlayerRoute != BUDDY_ROUTE_SNIPING
									sBallasRearTimber1[3].ped = CREATE_ENEMY_PED(modelBalla, <<-512.5977, 5305.6699, 79.2676>>, 307.4940, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
								ELSE
									sBallasRearTimber1[3].ped = CREATE_ENEMY_PED(modelBalla, <<-502.8857, 5326.4292, 79.2676>>, 192.3165, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
								ENDIF

								INITIALISE_ENEMY_GROUP(sBallasRearTimber1, "Rear1_")
							ENDIF
						ELSE
							//Player is on top of the spawn points (or they reached the trigger from the front route), so don't spawn these peds.
							sBallasRearTimber1[0].bIsCreated = TRUE
							sBallasRearTimber1[1].bIsCreated = TRUE
							sBallasRearTimber1[2].bIsCreated = TRUE
							sBallasRearTimber1[3].bIsCreated = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasRearTimber1) i 
				IF DOES_ENTITY_EXIST(sBallasRearTimber1[i].ped)
					IF NOT IS_PED_INJURED(sBallasRearTimber1[i].ped)			
						SWITCH sBallasRearTimber1[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasRearTimber1[i].vDest = <<-491.4250, 5314.0537, 79.6100>>
								ELIF i = 1
									sBallasRearTimber1[i].vDest = <<-487.1756, 5317.7554, 79.6100>>
								ELIF i = 2
									sBallasRearTimber1[i].vDest = <<-484.8855, 5314.1694, 79.6100>>
								ELIF i = 3
									sBallasRearTimber1[i].vDest = <<-480.3152, 5303.8145, 79.6100>>
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasRearTimber1[i].ped, sBallasRearTimber1[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasRearTimber1[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasRearTimber1[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber1[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasRearTimber1[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasRearTimber1[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber1[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasRearTimber1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasRearTimber1[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasRearTimber1[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasRearTimber1[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF HAS_PED_GOT_WEAPON(sBallasRearTimber1[i].ped, WEAPONTYPE_SMG)
									SET_CURRENT_PED_WEAPON(sBallasRearTimber1[i].ped, WEAPONTYPE_SMG, TRUE)
								ELIF HAS_PED_GOT_WEAPON(sBallasRearTimber1[i].ped, WEAPONTYPE_ASSAULTRIFLE)
									SET_CURRENT_PED_WEAPON(sBallasRearTimber1[i].ped, WEAPONTYPE_ASSAULTRIFLE, TRUE)
								ENDIF
								
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasRearTimber1[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasRearTimber1[i].iTimer = 0
								sBallasRearTimber1[i].iEvent++
							BREAK
						ENDSWITCH
						
						IF sBallasRearTimber1[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasRearTimber1[i].ped, sBallasRearTimber1[i].sBlipData)
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasRearTimber1[i].bSetToCharge
								IF sBallasRearTimber1[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasRearTimber1[i].ped), sBallasRearTimber1[i].vDest) < 4.0
										sBallasRearTimber1[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasRearTimber1[i].iTimer > 10000
									IF ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT
									OR ePlayerRoute = BUDDY_ROUTE_REAR 
										IF (NOT sBallasRearTimber1[i].bBlockOpenCombat AND VDIST2(vPlayerPos, GET_ENTITY_COORDS(sBallasRearTimber1[i].ped)) < 900.0)
										OR (sBallasRearTimber1[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasRearTimber1[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber1[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasRearTimber1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasRearTimber1[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF ePlayerRoute != BUDDY_ROUTE_FRONT_ASSAULT
								AND ePlayerRoute != BUDDY_ROUTE_REAR 
									IF VDIST2(GET_ENTITY_COORDS(sBallasRearTimber1[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber1[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasRearTimber1[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasRearTimber1[i].ped, sBallasRearTimber1[i].vDest, 3.0, TRUE)
										
										sBallasRearTimber1[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasRearTimber1[i].ped, sBallasRearTimber1[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasRearTimber1[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasRearTimber1[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//One ped runs up onto the gantry when entering from the back timber area.
		IF NOT sBallasRearTimber2[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
			AND ePlayerRoute = BUDDY_ROUTE_REAR
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
					IF bHitLocateBallasRearTimber2
					OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasRearTimber1) <= 1
						IF NOT sBallasTimber2[0].bIsCreated AND vPlayerPos.y > 5315.2 //The player may be on the rear route but still in view of spawn points, so do another check here.
							IF NOT DOES_ENTITY_EXIST(sBallasRearTimber2[0].ped)
								sBallasRearTimber2[0].ped = CREATE_ENEMY_PED(modelBalla, <<-503.3340, 5294.9595, 79.5500>>, 343.6317, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELIF NOT DOES_ENTITY_EXIST(sBallasRearTimber2[1].ped)
								sBallasRearTimber2[1].ped = CREATE_ENEMY_PED(modelBalla, <<-511.1639, 5305.3657, 79.2676>>, 345.3339, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
							ELIF NOT DOES_ENTITY_EXIST(sBallasRearTimber2[2].ped)
								sBallasRearTimber2[2].ped = CREATE_ENEMY_PED(modelBalla, <<-503.0632, 5293.5557, 79.5585>>, 352.8675, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
							ELIF NOT DOES_ENTITY_EXIST(sBallasRearTimber2[3].ped)
								sBallasRearTimber2[3].ped = CREATE_ENEMY_PED(modelBalla, <<-519.3588, 5304.9502, 79.2676>>, 261.0985, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)

								INITIALISE_ENEMY_GROUP(sBallasRearTimber2, "Rear2_")
							ENDIF
						ELSE
							//Player is right on top of the spawn points, just skip this enemy group.
							sBallasRearTimber2[0].bIsCreated = TRUE
							sBallasRearTimber2[1].bIsCreated = TRUE
							sBallasRearTimber2[2].bIsCreated = TRUE
							sBallasRearTimber2[3].bIsCreated = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasRearTimber2) i 
				IF DOES_ENTITY_EXIST(sBallasRearTimber2[i].ped)
					IF NOT IS_PED_INJURED(sBallasRearTimber2[i].ped)			
						SWITCH sBallasRearTimber2[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasRearTimber2[i].vDest = <<-499.1525, 5298.5225, 79.6052>>
								ELIF i = 1
									sBallasRearTimber2[i].vDest = <<-495.8702, 5309.0659, 79.5914>>
								ELIF i = 2
									sBallasRearTimber2[i].vDest = <<-501.6623, 5302.2622, 79.5513>>
								ELSE
									sBallasRearTimber2[i].vDest = <<-488.4678, 5306.7129, 79.6103>>
								ENDIF
								
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasRearTimber2[i].ped, sBallasRearTimber2[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasRearTimber2[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasRearTimber2[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber2[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasRearTimber2[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasRearTimber2[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber2[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasRearTimber2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasRearTimber2[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasRearTimber2[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasRearTimber2[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasRearTimber2[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasRearTimber2[i].iTimer = 0
								sBallasRearTimber2[i].iEvent++
							BREAK
						ENDSWITCH
						
						IF sBallasRearTimber2[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasRearTimber2[i].ped, sBallasRearTimber2[i].sBlipData)
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasRearTimber2[i].bSetToCharge
								IF sBallasRearTimber2[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasRearTimber2[i].ped), sBallasRearTimber2[i].vDest) < 4.0
										sBallasRearTimber2[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasRearTimber2[i].iTimer > 10000
									IF ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT
									OR ePlayerRoute = BUDDY_ROUTE_REAR 
										IF (NOT sBallasRearTimber2[i].bBlockOpenCombat AND VDIST2(vPlayerPos, GET_ENTITY_COORDS(sBallasRearTimber2[i].ped)) < 900.0)
										OR (sBallasRearTimber2[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasRearTimber2[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber2[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasRearTimber2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasRearTimber2[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF ePlayerRoute != BUDDY_ROUTE_FRONT_ASSAULT
								AND ePlayerRoute != BUDDY_ROUTE_REAR 
									IF VDIST2(GET_ENTITY_COORDS(sBallasRearTimber2[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasRearTimber2[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasRearTimber2[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasRearTimber2[i].ped, sBallasRearTimber2[i].vDest, 3.0, TRUE)
										
										sBallasRearTimber2[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasRearTimber2[i].ped, sBallasRearTimber2[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasRearTimber2[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasRearTimber2[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		//Ped guarding Lamar: once shot at or the player gets close he goes into combat.
		IF NOT sBallasOnBelt[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)	
				AND HAS_CLIP_SET_LOADED("move_ped_strafing")
					IF bHitLocateBallasOnBelt
						IF eCurrentPlayer = SELECTOR_PED_MICHAEL //Don't create if we're in sniper cam, or if the player has reached an area where they could be seen to spawn.
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-485.044128,5304.459961,66.859909>>, <<-562.708069,5083.084961,129.198059>>, 91.250000)
							KILL_ENEMY_GROUP_INSTANTLY(sBallasOnBelt)
						ELSE
							sBallasOnBelt[0].ped = CREATE_ENEMY_PED(modelBalla, <<-535.3907, 5288.1235, 83.9712>>, 100.9884, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							SET_PED_STRAFE_CLIPSET(sBallasOnBelt[0].ped, "move_ped_strafing")
							
							INITIALISE_ENEMY_GROUP(sBallasOnBelt, "Belt_")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasOnBelt) i 
				IF DOES_ENTITY_EXIST(sBallasOnBelt[i].ped)
					IF NOT IS_PED_INJURED(sBallasOnBelt[i].ped)			
						SWITCH sBallasOnBelt[i].iEvent
							CASE 0 //Run down conveyor belt.	
								sBallasOnBelt[0].vDest = <<-540.4274, 5272.7402, 80.2341>>
							
								OPEN_SEQUENCE_TASK(seq)
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, sBallasOnBelt[0].vDest, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 0.5, 4.0, FALSE)
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(sBallasOnBelt[i].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasOnBelt[i].ped, sBallasOnBelt[0].vDest, 2.0)
								SET_PED_COMBAT_PARAMS(sBallasOnBelt[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasOnBelt[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasOnBelt[i].ped, CA_USE_VEHICLE, FALSE)
								SET_COMBAT_FLOAT(sBallasOnBelt[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasOnBelt[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasOnBelt[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasOnBelt[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
							
								sBallasOnBelt[i].iEvent++
							BREAK
						ENDSWITCH
						
						IF sBallasOnBelt[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasOnBelt[i].ped, sBallasOnBelt[i].sBlipData)
							
							//Kill the ped if he falls off.
							VECTOR vPos = GET_ENTITY_COORDS(sBallasOnBelt[i].ped)
							IF vPos.z < 78.5
								APPLY_DAMAGE_TO_PED(sBallasOnBelt[i].ped, 200, TRUE)
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasOnBelt[i].ped, sBallasOnBelt[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasOnBelt[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasOnBelt[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//Peds show up on the way to the timber section.
		IF NOT sBallasBeforeTimber[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
					IF bHitLocateBallasBeforeTimber
					OR (ePlayerRoute != BUDDY_ROUTE_REAR AND IS_PED_INJURED(sBallasRocket[0].ped) AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasOnBelt) = 0)
						IF NOT DOES_ENTITY_EXIST(sBallasBeforeTimber[0].ped)
							IF ePlayerRoute != BUDDY_ROUTE_SNIPING
								sBallasBeforeTimber[0].ped = CREATE_ENEMY_PED(modelBalla, <<-502.0386, 5252.9727, 79.6104>>, 74.4253, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELSE
								sBallasBeforeTimber[0].ped = CREATE_ENEMY_PED(modelBalla, <<-503.4162, 5258.2720, 79.6100>>, 79.7836, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ENDIF
						ELIF NOT DOES_ENTITY_EXIST(sBallasBeforeTimber[1].ped)
							IF ePlayerRoute != BUDDY_ROUTE_SNIPING
								sBallasBeforeTimber[1].ped = CREATE_ENEMY_PED(modelBalla, <<-527.3934, 5272.4277, 73.3164>>, 126.3766, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
							ELSE
								sBallasBeforeTimber[1].ped = CREATE_ENEMY_PED(modelBalla, <<-528.6542, 5274.0645, 73.1741>>, 133.3165, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
							ENDIF
						ELIF NOT DOES_ENTITY_EXIST(sBallasBeforeTimber[2].ped)
							IF ePlayerRoute != BUDDY_ROUTE_SNIPING
								sBallasBeforeTimber[2].ped = CREATE_ENEMY_PED(modelBalla, <<-531.4066, 5263.5024, 73.3278>>, 175.8792, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELSE
								sBallasBeforeTimber[2].ped = CREATE_ENEMY_PED(modelBalla, <<-531.7002, 5265.3149, 73.2375>>, 155.8115, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ENDIF
						ELIF NOT DOES_ENTITY_EXIST(sBallasBeforeTimber[3].ped)
							IF ePlayerRoute != BUDDY_ROUTE_SNIPING
								sBallasBeforeTimber[3].ped = CREATE_ENEMY_PED(modelBalla, <<-502.2048, 5252.9600, 79.6104>>, 83.7086, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELSE
								sBallasBeforeTimber[3].ped = CREATE_ENEMY_PED(modelBalla, <<-502.4242, 5256.5176, 79.6100>>, 84.5402, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ENDIF

							INITIALISE_ENEMY_GROUP(sBallasBeforeTimber, "BeforeT_")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasBeforeTimber) i 
				IF DOES_ENTITY_EXIST(sBallasBeforeTimber[i].ped)
					IF NOT IS_PED_INJURED(sBallasBeforeTimber[i].ped)			
						SWITCH sBallasBeforeTimber[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasBeforeTimber[i].vDest = <<-520.9052, 5260.5249, 78.8500>>
								ELIF i = 1
									sBallasBeforeTimber[i].vDest = <<-542.3945, 5253.3306, 73.6131>>
								ELIF i = 2
									sBallasBeforeTimber[i].vDest = <<-545.6429, 5251.0005, 73.1374>>
								ELIF i = 3
									sBallasBeforeTimber[i].vDest = <<-523.8617, 5242.0698, 78.6782>>
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasBeforeTimber[i].ped, sBallasBeforeTimber[i].vDest, 3.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasBeforeTimber[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasBeforeTimber[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasBeforeTimber[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasBeforeTimber[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasBeforeTimber[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasBeforeTimber[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasBeforeTimber[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasBeforeTimber[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasBeforeTimber[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasBeforeTimber[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF i = 2
									PLAY_PED_AMBIENT_SPEECH(sBallasBeforeTimber[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								//Keep these enemies near to their defensive areas.
								IF i = 0 OR i = 3
									sBallasBeforeTimber[i].bBlockOpenCombat = TRUE
								ENDIF
								
								sBallasBeforeTimber[i].iEvent++
							BREAK
						ENDSWITCH
						
						IF sBallasBeforeTimber[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasBeforeTimber[i].ped, sBallasBeforeTimber[i].sBlipData)
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasBeforeTimber[i].bSetToCharge
								IF sBallasBeforeTimber[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasBeforeTimber[i].ped), sBallasBeforeTimber[i].vDest) < 4.0
										sBallasBeforeTimber[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasBeforeTimber[i].iTimer > 15000
									IF (NOT sBallasBeforeTimber[i].bBlockOpenCombat AND ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT)
									OR (sBallasBeforeTimber[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasBeforeTimber[i].ped), vPlayerPos) < 25.0)
										SET_PED_COMBAT_ATTRIBUTES(sBallasBeforeTimber[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasBeforeTimber[i].ped, CA_CAN_CHARGE, TRUE)
										
										sBallasBeforeTimber[i].bSetToCharge = TRUE
									ENDIF
								ENDIF
							ELSE
								IF ePlayerRoute != BUDDY_ROUTE_FRONT_ASSAULT
									IF VDIST2(GET_ENTITY_COORDS(sBallasBeforeTimber[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasBeforeTimber[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasBeforeTimber[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasBeforeTimber[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasBeforeTimber[i].ped, sBallasBeforeTimber[i].vDest, 3.0, TRUE)
										
										sBallasBeforeTimber[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasBeforeTimber[i].ped, sBallasBeforeTimber[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasBeforeTimber[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasBeforeTimber[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//Peds show up and take cover at the timber.
		IF NOT sBallasTimber1[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
					IF bHitLocateBallasTimber1
						IF NOT DOES_ENTITY_EXIST(sBallasTimber1[0].ped)
							sBallasTimber1[0].ped = CREATE_ENEMY_PED(modelBalla, <<-500.7499, 5274.3579, 79.6100>>, 125.7038, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
						ELIF NOT DOES_ENTITY_EXIST(sBallasTimber1[1].ped)
							sBallasTimber1[1].ped = CREATE_ENEMY_PED(modelBalla, <<-503.8717, 5286.3208, 79.5971>>, 174.9208, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
						ELIF NOT DOES_ENTITY_EXIST(sBallasTimber1[2].ped)
						AND NOT sBallasTimber1[2].bIsCreated
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-527.082581,5251.387207,85.503258>>, <<-554.000549,5261.276855,68.925285>>, 20.000000)
								sBallasTimber1[2].ped = CREATE_ENEMY_PED(modelBalla, <<-492.2799, 5259.2686, 85.8358>>, 96.1830, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELSE
								sBallasTimber1[2].bIsCreated = TRUE
							ENDIF
						ELIF NOT DOES_ENTITY_EXIST(sBallasTimber1[3].ped)
							sBallasTimber1[3].ped = CREATE_ENEMY_PED(modelBalla, <<-503.4756, 5262.6255, 79.6103>>, 357.9579, relGroupBallas, 200, 0, WEAPONTYPE_SMG)

							INITIALISE_ENEMY_GROUP(sBallasTimber1, "Timber1_")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasTimber1) i 
				IF DOES_ENTITY_EXIST(sBallasTimber1[i].ped)
					IF NOT IS_PED_INJURED(sBallasTimber1[i].ped)			
						SWITCH sBallasTimber1[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasTimber1[i].vDest = <<-515.1771, 5265.7627, 79.4700>>
								ELIF i = 1
									sBallasTimber1[i].vDest = <<-502.3758, 5270.7603, 79.6100>>
								ELIF i = 2
									sBallasTimber1[i].vDest = <<-492.2799, 5259.2686, 85.8358>>
									sBallasTimber1[i].bBlockOpenCombat = TRUE
								ELIF i = 3
									sBallasTimber1[i].vDest = <<-504.5726, 5265.7915, 79.6103>>
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasTimber1[i].ped, sBallasTimber1[i].vDest, 3.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasTimber1[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasTimber1[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasTimber1[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasTimber1[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasTimber1[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasTimber1[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasTimber1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasTimber1[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasTimber1[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasTimber1[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasTimber1[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasTimber1[i].iEvent++
							BREAK
							
							CASE 1 //Switch combat once the player arrives.
								IF vPlayerPos.z > 79.5
									IF i = 2
										sBallasTimber1[i].vDest = <<-485.1102, 5278.6089, 85.8650>>										
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasTimber1[i].ped, sBallasTimber1[i].vDest, 2.0, TRUE)
										
										sBallasTimber1[i].iEvent++
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
						
						IF sBallasTimber1[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasTimber1[i].ped, sBallasTimber1[i].sBlipData)
							
							IF i != 2
							AND vPlayerPos.z > 79.5
								//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
								IF NOT sBallasTimber1[i].bSetToCharge
									IF sBallasTimber1[i].iTimer = 0
										IF eCurrentPlayer != SELECTOR_PED_MICHAEL
										AND VDIST2(GET_ENTITY_COORDS(sBallasTimber1[i].ped), sBallasTimber1[i].vDest) < 4.0
											sBallasTimber1[i].iTimer = GET_GAME_TIMER()
										ENDIF
									ELIF GET_GAME_TIMER() - sBallasTimber1[i].iTimer > 10000
										IF (NOT sBallasTimber1[i].bBlockOpenCombat AND ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT)
										OR (sBallasTimber1[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasTimber1[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasTimber1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasTimber1[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasTimber1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasTimber1[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ELSE
									IF ePlayerRoute != BUDDY_ROUTE_FRONT_ASSAULT
										IF VDIST2(GET_ENTITY_COORDS(sBallasTimber1[i].ped), vPlayerPos) > 900.0
											SET_PED_COMBAT_ATTRIBUTES(sBallasTimber1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasTimber1[i].ped, CA_CAN_CHARGE, FALSE)
											SET_PED_COMBAT_MOVEMENT(sBallasTimber1[i].ped, CM_DEFENSIVE)
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasTimber1[i].ped, sBallasTimber1[i].vDest, 3.0, TRUE)
											
											sBallasTimber1[i].bSetToCharge = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasTimber1[i].ped, sBallasTimber1[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasTimber1[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasTimber1[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		//More peds show up at the timber section.
		IF NOT sBallasTimber2[0].bIsCreated
			IF eMissionStage < STAGE_GET_LAMAR_OUT
				IF HAS_MODEL_LOADED(modelBalla)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
				AND HAS_CLIP_SET_LOADED("move_ped_strafing")
					IF bHitLocateBallasTimber2
					OR (GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber1) <= 1 AND ePlayerRoute != BUDDY_ROUTE_REAR)
						IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-495.678741,5296.493652,90.666832>>,<<21.250000,14.000000,12.500000>>)
							IF NOT DOES_ENTITY_EXIST(sBallasTimber2[0].ped)
								sBallasTimber2[0].ped = CREATE_ENEMY_PED(modelBalla, <<-501.6830, 5293.6221, 79.5882>>, 164.6348, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
							ELIF NOT DOES_ENTITY_EXIST(sBallasTimber2[1].ped)
								sBallasTimber2[1].ped = CREATE_ENEMY_PED(modelBalla, <<-504.0350, 5286.5898, 79.5914>>, 175.8207, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
							ELIF NOT DOES_ENTITY_EXIST(sBallasTimber2[2].ped)
							AND NOT sBallasTimber2[2].bIsCreated
								IF ePlayerRoute != BUDDY_ROUTE_REAR
									sBallasTimber2[2].ped = CREATE_ENEMY_PED(modelBalla, <<-502.1692, 5320.1548, 86.6316>>, 164.9300, relGroupBallas, 200, 0, WEAPONTYPE_COMBATMG)
									SET_PED_STRAFE_CLIPSET(sBallasTimber2[2].ped, "move_ped_strafing")
								ELSE
									sBallasTimber2[2].bIsCreated = TRUE
								ENDIF
							ELIF NOT DOES_ENTITY_EXIST(sBallasTimber2[3].ped)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-514.717224,5247.317383,77.877007>>, <<-499.806976,5287.700684,85.860344>>, 21.500000)
									sBallasTimber2[3].ped = CREATE_ENEMY_PED(modelBalla, <<-481.4254, 5284.5571, 83.1625>>, 146.3917, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
									SET_PED_STRAFE_CLIPSET(sBallasTimber2[3].ped, "move_ped_strafing")
								ELSE
									sBallasTimber2[3].bIsCreated = TRUE
								ENDIF

								INITIALISE_ENEMY_GROUP(sBallasTimber2, "Timber2_")
							ENDIF
						ELSE
							sBallasTimber2[0].bIsCreated = TRUE
							sBallasTimber2[1].bIsCreated = TRUE
							sBallasTimber2[2].bIsCreated = TRUE
							sBallasTimber2[3].bIsCreated = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasTimber2) i 
				BOOL bMoveToSphere = FALSE
			
				IF DOES_ENTITY_EXIST(sBallasTimber2[i].ped)
					IF NOT IS_PED_INJURED(sBallasTimber2[i].ped)			
						SWITCH sBallasTimber2[i].iEvent
							CASE 0 	
								IF i != 2
									bMoveToSphere = TRUE
								ELSE
									//Delay the roof guy moving until the player's attention is in the area.
									IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber2) < COUNT_OF(sBallasTimber2)
									OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-538.501160,5288.443359,72.864220>>, <<-462.794434,5238.156738,99.614388>>, 9.250000)
									OR ePlayerRoute = BUDDY_ROUTE_REAR 
										bMoveToSphere = TRUE
									ENDIF
								ENDIF
							
								IF bMoveToSphere
									IF i = 0
										sBallasTimber2[i].vDest = <<-501.6830, 5293.6221, 79.5882>>
									ELIF i = 1
										sBallasTimber2[i].vDest = <<-496.6544, 5286.9561, 79.6100>>
									ELIF i = 2
										sBallasTimber2[i].vDest = <<-489.4470, 5304.7974, 86.9991>>
										sBallasTimber2[i].bBlockOpenCombat = TRUE
									ELIF i = 3
										sBallasTimber2[i].vDest = <<-478.7840, 5296.9609, 85.6715>>
										sBallasTimber2[i].bBlockOpenCombat = TRUE
									ENDIF
								
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasTimber2[i].ped, sBallasTimber2[i].vDest, 3.0, TRUE)
									SET_PED_COMBAT_PARAMS(sBallasTimber2[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_RELATIONSHIP_GROUP_HASH(sBallasTimber2[i].ped, relGroupBallas)
									SET_PED_COMBAT_ATTRIBUTES(sBallasTimber2[i].ped, CA_USE_VEHICLE, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasTimber2[i].ped, 200.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasTimber2[i].ped, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(sBallasTimber2[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
									SET_COMBAT_FLOAT(sBallasTimber2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									SET_COMBAT_FLOAT(sBallasTimber2[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
									SET_COMBAT_FLOAT(sBallasTimber2[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
									SET_PED_ALERTNESS(sBallasTimber2[i].ped, AS_MUST_GO_TO_COMBAT)
									
									IF i = 0
										PLAY_PED_AMBIENT_SPEECH(sBallasTimber2[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
									ENDIF
									
									sBallasTimber2[i].iEvent++
								ENDIF
							BREAK
							
							CASE 1 //Switch combat once the player arrives.
								IF i = 2
									VECTOR vPos
									vPos = GET_ENTITY_COORDS(sBallasTimber2[i].ped)
									
									IF vPos.z < 82.0
										APPLY_DAMAGE_TO_PED(sBallasTimber2[i].ped, 200, TRUE)
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
						
						IF sBallasTimber2[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasTimber2[i].ped, sBallasTimber2[i].sBlipData)
							
							IF i != 2
								//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
								IF NOT sBallasTimber2[i].bSetToCharge
									IF sBallasTimber2[i].iTimer = 0
										IF eCurrentPlayer != SELECTOR_PED_MICHAEL
										AND VDIST2(GET_ENTITY_COORDS(sBallasTimber2[i].ped), sBallasTimber2[i].vDest) < 4.0
											sBallasTimber2[i].iTimer = GET_GAME_TIMER()
										ENDIF
									ELIF GET_GAME_TIMER() - sBallasTimber2[i].iTimer > 10000
										IF (NOT sBallasTimber2[i].bBlockOpenCombat AND ePlayerRoute = BUDDY_ROUTE_FRONT_ASSAULT)
										OR (sBallasTimber2[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasTimber2[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasTimber2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasTimber2[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasTimber2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasTimber2[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ELSE
									IF ePlayerRoute != BUDDY_ROUTE_FRONT_ASSAULT
										IF VDIST2(GET_ENTITY_COORDS(sBallasTimber2[i].ped), vPlayerPos) > 900.0
											SET_PED_COMBAT_ATTRIBUTES(sBallasTimber2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasTimber2[i].ped, CA_CAN_CHARGE, FALSE)
											SET_PED_COMBAT_MOVEMENT(sBallasTimber2[i].ped, CM_DEFENSIVE)
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasTimber2[i].ped, sBallasTimber2[i].vDest, 3.0, TRUE)
											
											sBallasTimber2[i].bSetToCharge = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasTimber2[i].ped, sBallasTimber2[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasTimber2[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasTimber2[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	
ENDPROC

PROC UPDATE_SHOOTOUT_ENEMIES_AFTER_RESCUING_LAMAR()
	//The following enemies only trigger after the player has rescued Lamar.
	IF eMissionStage = STAGE_GET_LAMAR_OUT
		INT i = 0
		SEQUENCE_INDEX seq
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
		//First wave: arrive immediately after rescuing Lamar.
		IF NOT sBallasAfterLamar[0].bIsCreated		
			IF HAS_MODEL_LOADED(modelBalla)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
				IF NOT DOES_ENTITY_EXIST(sBallasAfterLamar[0].ped)
					IF GET_GAME_TIMER() - iTimeSinceLamarRescued > 1500
					OR IS_SPHERE_VISIBLE(<<-500.2, 5299.8, 80.8>>, 2.0)
						sBallasAfterLamar[0].ped = CREATE_ENEMY_PED(modelBalla, <<-493.5506, 5253.9517, 85.8353>>, 39.0411, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(sBallasAfterLamar[1].ped)
					sBallasAfterLamar[1].ped = CREATE_ENEMY_PED(modelBalla, <<-489.7464, 5314.8032, 79.6100>>, 33.0662, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
				ELIF NOT DOES_ENTITY_EXIST(sBallasAfterLamar[2].ped)
					sBallasAfterLamar[2].ped = CREATE_ENEMY_PED(modelBalla, <<-482.9134, 5314.7871, 79.6100>>, 83.8990, relGroupBallas, 200, 0, WEAPONTYPE_SMG)

					INITIALISE_ENEMY_GROUP(sBallasAfterLamar, "AfterL_")
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasAfterLamar) i 
				IF DOES_ENTITY_EXIST(sBallasAfterLamar[i].ped)
					IF NOT IS_PED_INJURED(sBallasAfterLamar[i].ped)			
						SWITCH sBallasAfterLamar[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasAfterLamar[i].vDest = <<-484.8358, 5277.8628, 85.8626>>
									sBallasAfterLamar[i].bBlockOpenCombat = TRUE
								ELIF i = 1
									sBallasAfterLamar[i].vDest = <<-497.8528, 5299.3110, 79.6100>>
									sBallasAfterLamar[i].bBlockOpenCombat = TRUE
								ELIF i = 2
									sBallasAfterLamar[i].vDest = <<-498.0019, 5301.9556, 79.6037>>
									sBallasAfterLamar[i].bBlockOpenCombat = TRUE
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterLamar[i].ped, sBallasAfterLamar[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasAfterLamar[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasAfterLamar[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasAfterLamar[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasAfterLamar[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasAfterLamar[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasAfterLamar[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasAfterLamar[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasAfterLamar[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF i = 1
									PLAY_PED_AMBIENT_SPEECH(sBallasAfterLamar[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasAfterLamar[i].iTimer = 0
								sBallasAfterLamar[i].iEvent++
							BREAK
							
							CASE 1 //Switch combat once the player arrives.

							BREAK
						ENDSWITCH
						
						IF sBallasAfterLamar[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasAfterLamar[i].ped, sBallasAfterLamar[i].sBlipData)
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasAfterLamar[i].bSetToCharge
								IF sBallasAfterLamar[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar[i].ped), sBallasAfterLamar[i].vDest) < 4.0
										sBallasAfterLamar[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasAfterLamar[i].iTimer > 10000
									IF (NOT sBallasAfterLamar[i].bBlockOpenCombat AND eCurrentPlayer != SELECTOR_PED_MICHAEL)
									OR (sBallasAfterLamar[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar[i].ped), vPlayerPos) < 25.0)
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar[i].ped, CA_CAN_CHARGE, TRUE)
										SET_COMBAT_FLOAT(sBallasAfterLamar[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
										
										sBallasAfterLamar[i].bSetToCharge = TRUE
									ENDIF
								ENDIF
							ELSE
								IF eCurrentPlayer = SELECTOR_PED_MICHAEL
									IF VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasAfterLamar[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterLamar[i].ped, sBallasAfterLamar[i].vDest, 3.0, TRUE)
										
										sBallasAfterLamar[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasAfterLamar[i].ped, sBallasAfterLamar[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasAfterLamar[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasAfterLamar[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//Second wave: a bit further on in the timber.
		IF NOT sBallasAfterLamar2[0].bIsCreated
			IF HAS_MODEL_LOADED(modelBalla)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SMG)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				IF sBallasAfterLamar[0].bIsCreated
					IF NOT DOES_ENTITY_EXIST(sBallasAfterLamar2[0].ped)
						sBallasAfterLamar2[0].ped = CREATE_ENEMY_PED(modelBalla, <<-500.3767, 5275.7744, 79.6100>>, 350.0594, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterLamar2[1].ped)
						sBallasAfterLamar2[1].ped = CREATE_ENEMY_PED(modelBalla, <<-524.5526, 5250.3794, 78.2809>>, 314.8624, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterLamar2[2].ped)
						sBallasAfterLamar2[2].ped = CREATE_ENEMY_PED(modelBalla, <<-494.5456, 5251.4844, 85.8305>>, 356.7617, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
					ELIF NOT DOES_ENTITY_EXIST(sBallasAfterLamar2[3].ped)
						sBallasAfterLamar2[3].ped = CREATE_ENEMY_PED(modelBalla, <<-505.5895, 5259.6177, 79.6100>>, 347.4243, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)

						INITIALISE_ENEMY_GROUP(sBallasAfterLamar2, "AfterL2_")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasAfterLamar2) i 
				IF DOES_ENTITY_EXIST(sBallasAfterLamar2[i].ped)
					IF NOT IS_PED_INJURED(sBallasAfterLamar2[i].ped)			
						SWITCH sBallasAfterLamar2[i].iEvent
							CASE 0 								
								sBallasAfterLamar2[i].vDest = GET_ENTITY_COORDS(sBallasAfterLamar2[i].ped)
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterLamar2[i].ped, sBallasAfterLamar2[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasAfterLamar2[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasAfterLamar2[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar2[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasAfterLamar2[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasAfterLamar2[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar2[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasAfterLamar2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasAfterLamar2[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasAfterLamar2[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasAfterLamar2[i].ped, AS_MUST_GO_TO_COMBAT)
								
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(sBallasAfterLamar2[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
								ENDIF
								
								sBallasAfterLamar2[i].iTimer = 0
								sBallasAfterLamar2[i].iEvent++
							BREAK
							
							CASE 1 //Once the player gets closer have them charge.
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-451.718506,5289.728516,89.019905>>, <<-536.241882,5292.254395,71.685295>>, 10.250000)
								OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar) = 0
									IF i = 0
										sBallasAfterLamar2[i].vDest = <<-504.8420, 5280.0142, 79.6100>>
									ELIF i = 1
										sBallasAfterLamar2[i].vDest = <<-502.5594, 5272.3975, 79.6100>>
									ELIF i = 2
										sBallasAfterLamar2[i].vDest = <<-490.6916, 5260.4219, 85.9607>>
										sBallasAfterLamar2[i].bBlockOpenCombat = TRUE
									ELIF i = 3
										sBallasAfterLamar2[i].vDest = <<-515.0439, 5269.5923, 79.4698>>
									ENDIF	
									
									SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterLamar2[i].ped, sBallasAfterLamar2[i].vDest, 2.0, TRUE)
								
									IF i = 0
										PLAY_PED_AMBIENT_SPEECH(sBallasAfterLamar[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
									ENDIF
									
									sBallasAfterLamar2[i].iEvent++
								ENDIF
							BREAK
							
							CASE 2 //Set to charge once safe.

							BREAK
						ENDSWITCH
						
						IF sBallasAfterLamar2[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasAfterLamar2[i].ped, sBallasAfterLamar2[i].sBlipData)
							
							IF sBallasAfterLamar2[i].iEvent > 1
								//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
								IF NOT sBallasAfterLamar2[i].bSetToCharge
									IF sBallasAfterLamar2[i].iTimer = 0
										IF eCurrentPlayer != SELECTOR_PED_MICHAEL
										AND VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar2[i].ped), sBallasAfterLamar2[i].vDest) < 4.0
											sBallasAfterLamar2[i].iTimer = GET_GAME_TIMER()
										ENDIF
									ELIF GET_GAME_TIMER() - sBallasAfterLamar2[i].iTimer > 10000
										IF (NOT sBallasAfterLamar2[i].bBlockOpenCombat AND eCurrentPlayer != SELECTOR_PED_MICHAEL)
										OR (sBallasAfterLamar2[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar2[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar2[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(sBallasAfterLamar2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											
											sBallasAfterLamar2[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ELSE
									IF eCurrentPlayer = SELECTOR_PED_MICHAEL
										IF VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar2[i].ped), vPlayerPos) > 900.0
											SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar2[i].ped, CA_CAN_CHARGE, FALSE)
											SET_PED_COMBAT_MOVEMENT(sBallasAfterLamar2[i].ped, CM_DEFENSIVE)
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterLamar2[i].ped, sBallasAfterLamar2[i].vDest, 3.0, TRUE)
											
											sBallasAfterLamar2[i].bSetToCharge = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasAfterLamar2[i].ped, sBallasAfterLamar2[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasAfterLamar2[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasAfterLamar2[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		//Third wave: Hides behind fence down the hill.
		IF NOT sBallasAfterLamar3[0].bIsCreated
			IF HAS_MODEL_LOADED(modelBalla)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				IF bHitLocateBallasPostRescueTimber
					IF NOT DOES_ENTITY_EXIST(sBallasAfterLamar3[0].ped)
						sBallasAfterLamar3[0].ped = CREATE_ENEMY_PED(modelBalla, <<-561.4196, 5254.9111, 69.5070>>, 290.0712, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)

						INITIALISE_ENEMY_GROUP(sBallasAfterLamar3, "AfterL3_")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasAfterLamar3) i 
				IF DOES_ENTITY_EXIST(sBallasAfterLamar3[i].ped)
					IF NOT IS_PED_INJURED(sBallasAfterLamar3[i].ped)			
						SWITCH sBallasAfterLamar3[i].iEvent
							CASE 0 	
								IF i = 0
									sBallasAfterLamar3[i].vDest = <<-561.0891, 5254.9658, 69.5110>>
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterLamar3[i].ped, sBallasAfterLamar3[i].vDest, 2.0, TRUE)
								SET_PED_COMBAT_PARAMS(sBallasAfterLamar3[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								SET_PED_RELATIONSHIP_GROUP_HASH(sBallasAfterLamar3[i].ped, relGroupBallas)
								SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar3[i].ped, CA_USE_VEHICLE, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasAfterLamar3[i].ped, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasAfterLamar3[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar3[i].ped, CA_DISABLE_PINNED_DOWN, TRUE) 
								SET_COMBAT_FLOAT(sBallasAfterLamar3[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(sBallasAfterLamar3[i].ped, CCF_MAX_SHOOTING_DISTANCE, 300.0)
								SET_COMBAT_FLOAT(sBallasAfterLamar3[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_PED_ALERTNESS(sBallasAfterLamar3[i].ped, AS_MUST_GO_TO_COMBAT)
								
								sBallasAfterLamar3[i].iTimer = 0
								sBallasAfterLamar3[i].iEvent++
							BREAK
							
							CASE 1 //Once the player gets closer have them charge.

							BREAK
						ENDSWITCH
						
						IF sBallasAfterLamar3[i].iEvent != 0
							UPDATE_AI_PED_BLIP(sBallasAfterLamar3[i].ped, sBallasAfterLamar3[i].sBlipData)
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasAfterLamar3[i].bSetToCharge
								IF sBallasAfterLamar3[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar3[i].ped), sBallasAfterLamar3[i].vDest) < 4.0
										sBallasAfterLamar3[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar3[i].ped), vPlayerPos) < 900.0
								OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sBallasAfterLamar3[i].ped, PLAYER_PED_ID())
									IF (NOT sBallasAfterLamar3[i].bBlockOpenCombat AND eCurrentPlayer != SELECTOR_PED_MICHAEL)
									OR (sBallasAfterLamar3[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar3[i].ped), vPlayerPos) < 25.0)
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar3[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar3[i].ped, CA_CAN_CHARGE, TRUE)
										SET_COMBAT_FLOAT(sBallasAfterLamar3[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
										
										sBallasAfterLamar3[i].bSetToCharge = TRUE
									ENDIF
								ENDIF
							ELSE
								IF eCurrentPlayer = SELECTOR_PED_MICHAEL
									IF VDIST2(GET_ENTITY_COORDS(sBallasAfterLamar3[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar3[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasAfterLamar3[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasAfterLamar3[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasAfterLamar3[i].ped, sBallasAfterLamar3[i].vDest, 3.0, TRUE)
										
										sBallasAfterLamar3[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasAfterLamar3[i].ped, sBallasAfterLamar3[i].iEvent)
						#ENDIF
					ELSE	
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasAfterLamar3[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasAfterLamar3[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF NOT sBallasPostRescueTimber[0].bIsCreated
			REQUEST_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_1, strCarrec)
			REQUEST_MODEL(modelBallaReinforcementCar)
		
			IF HAS_MODEL_LOADED(modelBalla)
			AND HAS_MODEL_LOADED(modelBallaReinforcementCar)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_BALLA_POST_RESCUE_1, strCarrec)
				IF bHitLocateBallasPostRescueTimber
					vehBallaPostRescueTimber = CREATE_VEHICLE(modelBallaReinforcementCar, <<-590.5018, 5283.3286, 69.9264>>, -107.7629)
					SET_VEHICLE_ENGINE_ON(vehBallaPostRescueTimber, TRUE, TRUE)
					START_PLAYBACK_RECORDED_VEHICLE(vehBallaPostRescueTimber, CARREC_BALLA_POST_RESCUE_1, strCarrec)
					SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBallaPostRescueTimber, 2000.0)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBallaPostRescueTimber)
					SET_PLAYBACK_SPEED(vehBallaPostRescueTimber, 1.1)
					SET_AUDIO_VEHICLE_PRIORITY(vehBallaPostRescueTimber, AUDIO_VEHICLE_PRIORITY_MAX)
					//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBallaPostRescueTimber, FALSE)
					
					sBallasPostRescueTimber[0].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueTimber, VS_DRIVER, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
					sBallasPostRescueTimber[1].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueTimber, VS_FRONT_RIGHT, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
					sBallasPostRescueTimber[2].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueTimber, VS_BACK_LEFT, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
					
					INITIALISE_ENEMY_GROUP(sBallasPostRescueTimber, "TimbPost_")
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(sBallasPostRescueTimber) i 
				IF DOES_ENTITY_EXIST(sBallasPostRescueTimber[i].ped)
					IF NOT IS_PED_INJURED(sBallasPostRescueTimber[i].ped)						
						SWITCH sBallasPostRescueTimber[i].iEvent
							CASE 0 
								BOOL bGetPedsOut
								INT iGetOutTimer
								
								IF IS_VEHICLE_DRIVEABLE(vehBallaPostRescueTimber)
									IF ABSF(GET_ENTITY_SPEED(vehBallaPostRescueTimber)) < 0.5 AND IS_ENTITY_AT_COORD(vehBallaPostRescueTimber, <<-518.2, 5245.4, 80.0>>, <<10.0, 10.0, 5.0>>)
										SET_AUDIO_VEHICLE_PRIORITY(vehBallaPostRescueTimber, AUDIO_VEHICLE_PRIORITY_NORMAL)
									
										bGetPedsOut = TRUE
									ENDIF
								ELSE
									bGetPedsOut = TRUE
								ENDIF
							
								IF bGetPedsOut
									IF i = 0
										iGetOutTimer = 0
									ELIF i = 1
										iGetOutTimer = 1200
									ELIF i = 2
										iGetOutTimer = 650
									ENDIF
										
									IF sBallasPostRescueTimber[i].iTimer = 0
										sBallasPostRescueTimber[i].iTimer = GET_GAME_TIMER()
									ELIF GET_GAME_TIMER() - sBallasPostRescueTimber[i].iTimer > iGetOutTimer
										IF i = 0
											sBallasPostRescueTimber[i].vDest = <<-519.2643, 5255.7915, 79.4995>>
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueTimber[i].ped, sBallasPostRescueTimber[i].vDest, 3.0, TRUE)
										ELIF i = 1
											sBallasPostRescueTimber[i].vDest = GET_ENTITY_COORDS(sBallasPostRescueTimber[i].ped)
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueTimber[i].ped, sBallasPostRescueTimber[i].vDest, 5.0, TRUE)
										ELIF i = 2
											sBallasPostRescueTimber[i].vDest = <<-507.8264, 5254.7817, 79.6530>>
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueTimber[i].ped, sBallasPostRescueTimber[i].vDest, 3.0, TRUE)
										ENDIF
									
										SET_PED_COMBAT_PARAMS(sBallasPostRescueTimber[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
										
										IF HAS_PED_GOT_WEAPON(sBallasPostRescueTimber[i].ped, WEAPONTYPE_SMG)
											SET_CURRENT_PED_WEAPON(sBallasPostRescueTimber[i].ped, WEAPONTYPE_SMG, TRUE)
										ELIF HAS_PED_GOT_WEAPON(sBallasPostRescueTimber[i].ped, WEAPONTYPE_ASSAULTRIFLE)
											SET_CURRENT_PED_WEAPON(sBallasPostRescueTimber[i].ped, WEAPONTYPE_ASSAULTRIFLE, TRUE)
										ENDIF
										SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueTimber[i].ped, CA_USE_VEHICLE, FALSE)
										SET_COMBAT_FLOAT(sBallasPostRescueTimber[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasPostRescueTimber[i].ped, 200.0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasPostRescueTimber[i].ped, FALSE)
										SET_PED_ALERTNESS(sBallasPostRescueTimber[i].ped, AS_MUST_GO_TO_COMBAT)
										
										IF i = 0
											PLAY_PED_AMBIENT_SPEECH(sBallasPostRescueTimber[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
										ENDIF
										
										sBallasPostRescueTimber[i].iTimer = GET_GAME_TIMER()
										sBallasPostRescueTimber[i].iEvent++
									ENDIF
								ENDIF
							BREAK
							
							CASE 1

							BREAK
						ENDSWITCH
						
						IF sBallasPostRescueTimber[i].iEvent > 0
							UPDATE_AI_PED_BLIP(sBallasPostRescueTimber[i].ped, sBallasPostRescueTimber[i].sBlipData)			
							
							//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
							IF NOT sBallasPostRescueTimber[i].bSetToCharge
								IF sBallasPostRescueTimber[i].iTimer = 0
									IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									AND VDIST2(GET_ENTITY_COORDS(sBallasPostRescueTimber[i].ped), sBallasPostRescueTimber[i].vDest) < 4.0
										sBallasPostRescueTimber[i].iTimer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - sBallasPostRescueTimber[i].iTimer > 10000
									IF (NOT sBallasPostRescueTimber[i].bBlockOpenCombat AND eCurrentPlayer != SELECTOR_PED_MICHAEL)
									OR (sBallasPostRescueTimber[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasPostRescueTimber[i].ped), vPlayerPos) < 25.0)
										SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueTimber[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueTimber[i].ped, CA_CAN_CHARGE, TRUE)
										SET_COMBAT_FLOAT(sBallasPostRescueTimber[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
										
										sBallasPostRescueTimber[i].bSetToCharge = TRUE
									ENDIF
								ENDIF
							ELSE
								IF eCurrentPlayer = SELECTOR_PED_MICHAEL
									IF VDIST2(GET_ENTITY_COORDS(sBallasPostRescueTimber[i].ped), vPlayerPos) > 900.0
										SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueTimber[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueTimber[i].ped, CA_CAN_CHARGE, FALSE)
										SET_PED_COMBAT_MOVEMENT(sBallasPostRescueTimber[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueTimber[i].ped, sBallasPostRescueTimber[i].vDest, 3.0, TRUE)
										
										sBallasPostRescueTimber[i].bSetToCharge = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasPostRescueTimber[i].ped, sBallasPostRescueTimber[i].iEvent)
						#ENDIF
					ELSE
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasPostRescueTimber[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasPostRescueTimber[i])
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT IS_ENTITY_DEAD(vehBallaPostRescueTimber) 
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBallaPostRescueTimber)
					REMOVE_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_1, strCarrec)
				ENDIF
				
				/*IF IS_VEHICLE_DRIVEABLE(vehBallaPostRescueTimber)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehBallaPostRescueTimber, PLAYER_PED_ID())
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueTimber) <= 2
						IF GET_ENTITY_HEALTH(vehBallaPostRescueTimber) < 800
						OR GET_VEHICLE_PETROL_TANK_HEALTH(vehBallaPostRescueTimber) < 800
						OR GET_VEHICLE_ENGINE_HEALTH(vehBallaPostRescueTimber) < 800
							SET_VEHICLE_PETROL_TANK_HEALTH(vehBallaPostRescueTimber, -100)
						ENDIF
					ENDIF
				ENDIF*/
			ENDIF
		ENDIF
		
		
		IF NOT sBallasPostRescueExit[0].bIsCreated
			REQUEST_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_2, strCarrec)
			REQUEST_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_3, strCarrec)
			REQUEST_MODEL(modelBallaReinforcementCar)
		
			IF HAS_MODEL_LOADED(modelBalla)
			AND HAS_MODEL_LOADED(modelBallaReinforcementCar)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_BALLA_POST_RESCUE_2, strCarrec)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_BALLA_POST_RESCUE_3, strCarrec)
				IF bHitLocateBallasPostRescueExit
				OR (GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueTimber) = 0 AND VDIST2(vPlayerPos, vSawmillExitPos) < 10000.0)
					IF NOT DOES_ENTITY_EXIST(vehBallaPostRescueExit[0])
						vehBallaPostRescueExit[0] = CREATE_VEHICLE(modelBallaReinforcementCar, <<-701.8870, 5255.8970, 73.3489>>, -104.8644)
						SET_VEHICLE_ENGINE_ON(vehBallaPostRescueExit[0], TRUE, TRUE)
						START_PLAYBACK_RECORDED_VEHICLE(vehBallaPostRescueExit[0], CARREC_BALLA_POST_RESCUE_2, strCarrec)
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBallaPostRescueExit[0], 1500.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBallaPostRescueExit[0])
						SET_PLAYBACK_SPEED(vehBallaPostRescueExit[0], 1.1)
						SET_AUDIO_VEHICLE_PRIORITY(vehBallaPostRescueExit[0], AUDIO_VEHICLE_PRIORITY_MAX)
						//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBallaPostRescueExit[0], FALSE)
						
						vehBallaPostRescueExit[1] = CREATE_VEHICLE(modelBallaReinforcementCar, <<-709.0966, 5259.7446, 72.5166>>, -126.1647)
						SET_VEHICLE_ENGINE_ON(vehBallaPostRescueExit[1], TRUE, TRUE)
						START_PLAYBACK_RECORDED_VEHICLE(vehBallaPostRescueExit[1], CARREC_BALLA_POST_RESCUE_3, strCarrec)
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBallaPostRescueExit[1], 1500.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBallaPostRescueExit[1])
						SET_PLAYBACK_SPEED(vehBallaPostRescueExit[1], 1.1)
						//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBallaPostRescueExit[1], FALSE)
					ELIF NOT DOES_ENTITY_EXIST(sBallasPostRescueExit[0].ped)
						sBallasPostRescueExit[0].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueExit[0], VS_DRIVER, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
					ELIF NOT DOES_ENTITY_EXIST(sBallasPostRescueExit[1].ped)
						sBallasPostRescueExit[1].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueExit[0], VS_FRONT_RIGHT, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
					ELIF NOT DOES_ENTITY_EXIST(sBallasPostRescueExit[2].ped)
						sBallasPostRescueExit[2].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueExit[0], VS_BACK_LEFT, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
					ELIF NOT DOES_ENTITY_EXIST(sBallasPostRescueExit[3].ped)
						sBallasPostRescueExit[3].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueExit[1], VS_DRIVER, relGroupBallas, 200, 0, WEAPONTYPE_SMG)
					ELIF NOT DOES_ENTITY_EXIST(sBallasPostRescueExit[4].ped)
						sBallasPostRescueExit[4].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueExit[1], VS_FRONT_RIGHT, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
					ELIF NOT DOES_ENTITY_EXIST(sBallasPostRescueExit[5].ped)
						sBallasPostRescueExit[5].ped = CREATE_ENEMY_PED_IN_VEHICLE(modelBalla, vehBallaPostRescueExit[1], VS_BACK_LEFT, relGroupBallas, 200, 0, WEAPONTYPE_ASSAULTRIFLE)
						
						INITIALISE_ENEMY_GROUP(sBallasPostRescueExit, "ExitPost_")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			INT iVehIndex = 0
		
			REPEAT COUNT_OF(sBallasPostRescueExit) i 
				IF DOES_ENTITY_EXIST(sBallasPostRescueExit[i].ped)
					IF NOT IS_PED_INJURED(sBallasPostRescueExit[i].ped)						
						SWITCH sBallasPostRescueExit[i].iEvent
							CASE 0 
								IF i < 3
									iVehIndex = 0
								ELSE
									iVehIndex = 1
								ENDIF
							
								BOOL bGetPedsOut
								INT iGetOutTimer
								
								bGetPedsOut = FALSE
								iGetOutTimer = 0
								
								IF IS_VEHICLE_DRIVEABLE(vehBallaPostRescueExit[iVehIndex])								
									IF ABSF(VMAG(GET_ENTITY_VELOCITY(vehBallaPostRescueExit[iVehIndex]))) < 0.5 AND IS_ENTITY_AT_COORD(vehBallaPostRescueExit[iVehIndex], <<-568.1, 5263.7, 71.7>>, <<30.0, 30.0, 10.0>>)
										IF iVehIndex = 0
											SET_AUDIO_VEHICLE_PRIORITY(vehBallaPostRescueExit[iVehIndex], AUDIO_VEHICLE_PRIORITY_MAX)
										ENDIF
										
										bGetPedsOut = TRUE
									ENDIF
								ELSE
									bGetPedsOut = TRUE
								ENDIF
							
								IF bGetPedsOut
									IF i = 0
										iGetOutTimer = 0
									ELIF i = 1
										iGetOutTimer = 950
									ELIF i = 2
										iGetOutTimer = 450
									ELIF i = 3
										iGetOutTimer = 0
									ELIF i = 4
										iGetOutTimer = 1900
									ELIF i = 5
										iGetOutTimer = 1200
									ENDIF
										
									IF sBallasPostRescueExit[i].iTimer = 0
										sBallasPostRescueExit[i].iTimer = GET_GAME_TIMER()
									ELIF GET_GAME_TIMER() - sBallasPostRescueExit[i].iTimer > iGetOutTimer
										IF i = 0
											sBallasPostRescueExit[i].vDest = <<-566.3519, 5256.4736, 69.4670>>
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].vDest, 3.0, TRUE)
										ELIF i = 1
											sBallasPostRescueExit[i].vDest = <<-566.4669, 5270.9419, 69.2532>>
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].vDest, 3.0, TRUE)
										ELIF i = 2
											sBallasPostRescueExit[i].vDest = GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped)
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].vDest, 5.0, TRUE)
										ELIF i = 3
											IF NOT g_bFranklin2RequestedBackup
												sBallasPostRescueExit[i].vDest = <<-573.4135, 5274.2612, 69.2604>>
											ELSE
												sBallasPostRescueExit[i].vDest = <<-542.5646, 5232.9063, 74.4704>>
											ENDIF
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].vDest, 3.0, TRUE)
										ELIF i = 4
											IF NOT g_bFranklin2RequestedBackup
												sBallasPostRescueExit[i].vDest = GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped)
											ELSE
												sBallasPostRescueExit[i].vDest = <<-547.2554, 5231.9375, 73.1780>>
											ENDIF
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].vDest, 5.0, TRUE)
										ELSE
											IF NOT g_bFranklin2RequestedBackup
												sBallasPostRescueExit[i].vDest = GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped)
											ELSE
												sBallasPostRescueExit[i].vDest = <<-541.3654, 5227.2979, 75.7013>>
											ENDIF
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].vDest, 5.0, TRUE)
										ENDIF
									
										SET_PED_COMBAT_PARAMS(sBallasPostRescueExit[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
										
										IF HAS_PED_GOT_WEAPON(sBallasPostRescueExit[i].ped, WEAPONTYPE_SMG)
											SET_CURRENT_PED_WEAPON(sBallasPostRescueExit[i].ped, WEAPONTYPE_SMG, TRUE)
										ELIF HAS_PED_GOT_WEAPON(sBallasPostRescueExit[i].ped, WEAPONTYPE_ASSAULTRIFLE)
											SET_CURRENT_PED_WEAPON(sBallasPostRescueExit[i].ped, WEAPONTYPE_ASSAULTRIFLE, TRUE)
										ENDIF
										SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueExit[i].ped, CA_USE_VEHICLE, FALSE)
										SET_COMBAT_FLOAT(sBallasPostRescueExit[i].ped, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
										SET_COMBAT_FLOAT(sBallasPostRescueExit[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
										
										//The peds from the second car go after Michael.
										IF i < 3
										OR IS_PED_INJURED(GET_PED_INDEX(CHAR_MICHAEL))
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasPostRescueExit[i].ped, 200.0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasPostRescueExit[i].ped, FALSE)
										ELSE
											OPEN_SEQUENCE_TASK(seq)
												IF i = 3
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-570.1, 5236.1, 69.6337>>, GET_PED_INDEX(CHAR_MICHAEL), PEDMOVE_RUN,
																						   FALSE, 2.0, 2.0, TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
												ELIF i = 4
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-571.1, 5235.4, 69.6337>>, GET_PED_INDEX(CHAR_MICHAEL), PEDMOVE_RUN,
																						   FALSE, 2.0, 2.0, TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
												ELIF i = 5
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-573.1, 5235.4, 69.6337>>, GET_PED_INDEX(CHAR_MICHAEL), PEDMOVE_RUN,
																						   FALSE, 2.0, 2.0, TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
												ENDIF
											
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, sBallasPostRescueExit[i].vDest, GET_PED_INDEX(CHAR_MICHAEL), PEDMOVE_RUN,
																					   FALSE, 1.0, 4.0, TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
												TASK_COMBAT_PED(NULL, GET_PED_INDEX(CHAR_MICHAEL), COMBAT_PED_PREVENT_CHANGING_TARGET)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(sBallasPostRescueExit[i].ped, seq)
											CLEAR_SEQUENCE_TASK(seq)
											
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasPostRescueExit[i].ped, TRUE)
										ENDIF
										
										IF i = 0
											PLAY_PED_AMBIENT_SPEECH(sBallasPostRescueExit[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED)
										ENDIF
										
										sBallasPostRescueExit[i].bSetToCharge = FALSE
										sBallasPostRescueExit[i].iTimer = 0
										sBallasPostRescueExit[i].iEvent++
									ENDIF
								ENDIF
							BREAK
							
							CASE 1 //Go further up the hill after some time.
								IF i >= 3
								AND g_bFranklin2RequestedBackup
									INT iThreshold
									VECTOR vNewDest
									
									IF i = 3
										iThreshold = 10000
										vNewDest = <<-562.8715, 5217.4800, 81.9101>>
									ELIF i = 4
										iThreshold = 10700
										vNewDest = <<-578.9219, 5207.4038, 81.6344>>
									ELSE
										iThreshold = 11200
										vNewDest = <<-586.4161, 5210.2407, 80.3978>>
									ENDIF
									
									IF sBallasPostRescueExit[i].iTimer = 0
										IF VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped), sBallasPostRescueExit[i].vDest) < 4.0
											sBallasPostRescueExit[i].iTimer = GET_GAME_TIMER()
										ENDIF
									ELIF GET_GAME_TIMER() - sBallasPostRescueExit[i].iTimer > iThreshold
										sBallasPostRescueExit[i].vDest = vNewDest
										SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].vDest, 3.0, TRUE)
										
										sBallasPostRescueExit[i].iTimer = GET_GAME_TIMER()
										sBallasPostRescueExit[i].iEvent++
									ENDIF
								ENDIF
							BREAK
							
							CASE 2 //1445421 - Kill Michael once they get up the hill.
								IF i >= 3
								AND g_bFranklin2RequestedBackup
									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
										IF VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped), sBallasPostRescueExit[i].vDest) < 4.0
										AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sBallasPostRescueExit[i].ped)
											SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 0)
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
						
						IF sBallasPostRescueExit[i].iEvent > 0
							UPDATE_AI_PED_BLIP(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].sBlipData)	
							
							IF i = 3
								IF NOT bHasTextLabelTriggered[Lm2_flank]
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									AND NOT IS_SELECTOR_CAM_ACTIVE()
										IF VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped), vPlayerPos) < 900.0
											IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
												ADD_PED_FOR_DIALOGUE(sConversationPeds, 6, sBallasPostRescueExit[i].ped, "fr2_enemy3")
												
												IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "Lm2_flank", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[Lm2_flank] = TRUE
												ENDIF
											ENDIF
										ELSE
											bHasTextLabelTriggered[Lm2_flank] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							
							IF i < 3
								//Handle charging behaviour: set to charge once they reach their destinations and the player is nearby. Reset behaviour if the player switches away.
								IF NOT sBallasPostRescueExit[i].bSetToCharge
									IF sBallasPostRescueExit[i].iTimer = 0
										IF eCurrentPlayer != SELECTOR_PED_MICHAEL
										AND VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped), sBallasPostRescueExit[i].vDest) < 4.0
											sBallasPostRescueExit[i].iTimer = GET_GAME_TIMER()
										ENDIF
									ELIF GET_GAME_TIMER() - sBallasPostRescueExit[i].iTimer > 10000
										IF (NOT sBallasPostRescueExit[i].bBlockOpenCombat AND eCurrentPlayer != SELECTOR_PED_MICHAEL)
										OR (sBallasPostRescueExit[i].bBlockOpenCombat AND VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped), vPlayerPos) < 25.0)
											SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueExit[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueExit[i].ped, CA_CAN_CHARGE, TRUE)
											
											sBallasPostRescueExit[i].bSetToCharge = TRUE
										ENDIF
									ENDIF
								ELSE
									IF eCurrentPlayer = SELECTOR_PED_MICHAEL
										IF VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped), vPlayerPos) > 900.0
											SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueExit[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
											SET_PED_COMBAT_ATTRIBUTES(sBallasPostRescueExit[i].ped, CA_CAN_CHARGE, FALSE)
											SET_PED_COMBAT_MOVEMENT(sBallasPostRescueExit[i].ped, CM_DEFENSIVE)
											SET_PED_SPHERE_DEFENSIVE_AREA(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].vDest, 3.0, TRUE)
											
											sBallasPostRescueExit[i].bSetToCharge = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//1445421 - If the player is Michael then make sure the buddies can't kill these peds.
							IF eCurrentPlayer = SELECTOR_PED_MICHAEL
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sBallasPostRescueExit[i].ped, TRUE)
							ELSE
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sBallasPostRescueExit[i].ped, FALSE)
							ENDIF
							
							//These peds are set to go for Michael, if they're damaged by the player then switch them to combat.
							IF i >= 3
								IF eCurrentPlayer != SELECTOR_PED_MICHAEL
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sBallasPostRescueExit[i].ped, PLAYER_PED_ID())
									OR VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[i].ped), vPlayerPos) < 10.0
										IF GET_SCRIPT_TASK_STATUS(sBallasPostRescueExit[i].ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(sBallasPostRescueExit[i].ped, 200.0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBallasPostRescueExit[i].ped, FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							DRAW_INT_ABOVE_PED(sBallasPostRescueExit[i].ped, sBallasPostRescueExit[i].iEvent)
						#ENDIF
					ELSE
						UPDATE_PLAYER_PED_KILLS_STATS(sBallasPostRescueExit[i].ped, vPlayerPos)
						CLEAN_UP_ENEMY_PED(sBallasPostRescueExit[i])
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT IS_ENTITY_DEAD(vehBallaPostRescueExit[0]) AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBallaPostRescueExit[0])
				REMOVE_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_2, strCarrec)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(vehBallaPostRescueExit[1]) AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBallaPostRescueExit[1])
				REMOVE_VEHICLE_RECORDING(CARREC_BALLA_POST_RESCUE_3, strCarrec)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a given ped is currently hanging around where another given ped is trying to task to.
/// PARAMS:
///    pedToCheckForSteal - The ped that we want to check is stealing cover from our AI ped.
///    pedToMove - The AI ped that we want to move to the given destination.
///    vDestination - The destination
///    fMaxDistBeforeCheck - The moving ped must be within this distance of the destination before we start checking what the other ped is doing.
///    fMaxDistConsideredAsStealing - pedToCheckForSteal must be within this distance of the destination to be considered stealing it.
///    bPlayerMustBeInCover - If TRUE then the ped must be in cover for them to be considered in the way.  
FUNC BOOL IS_PED_STEALING_PEDS_DESTINATION(PED_INDEX pedToCheckForSteal, PED_INDEX pedToMove, VECTOR vDestination, VECTOR vStealPedDestination, FLOAT fMaxDistBeforeCheck, FLOAT fMaxDistConsideredAsStealing = 3.0)
	IF NOT IS_PED_INJURED(pedToMove)
	AND NOT IS_PED_INJURED(pedToCheckForSteal)
		IF ARE_VECTORS_ALMOST_EQUAL(vStealPedDestination, vDestination)
			RETURN TRUE
		ELSE
			VECTOR vPed1Pos = GET_ENTITY_COORDS(pedToCheckForSteal)
			VECTOR vPed2Pos = GET_ENTITY_COORDS(pedToMove)

			FLOAT fDistFromPed2ToDest = VDIST2(vPed2Pos, vDestination)
			FLOAT fDistFromPed1ToDest = VDIST2(vPed1Pos, vDestination)
			
			IF fDistFromPed2ToDest < (fMaxDistBeforeCheck * fMaxDistBeforeCheck)
				IF fDistFromPed1ToDest < (fMaxDistConsideredAsStealing * fMaxDistConsideredAsStealing)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if any enemies are hanging around close to the ped's next destination. 
FUNC BOOL ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(PED_INDEX ped, VECTOR vPedDestination, FLOAT fExtraDistance = 20.0)
	CONST_INT NUM_PEDS_TO_GRAB			8

	IF NOT IS_PED_INJURED(ped)
		PED_INDEX peds[NUM_PEDS_TO_GRAB]
		VECTOR vPedPos = GET_ENTITY_COORDS(ped)
		VECTOR vClosestEnemyPos
		FLOAT fEnemyDistFromPed
		FLOAT fPedDistToDestination, fEnemyDistToDestination
		INT i = 0
		
		GET_PED_NEARBY_PEDS(ped, peds)
		
		WHILE i < NUM_PEDS_TO_GRAB 
			IF NOT IS_PED_INJURED(peds[i])
				IF GET_ENTITY_MODEL(peds[i]) = modelBalla
					vClosestEnemyPos = GET_ENTITY_COORDS(peds[i])
					fEnemyDistFromPed = VDIST2(vClosestEnemyPos, vPedPos)
					fEnemyDistToDestination = VDIST2(vClosestEnemyPos, vPedDestination)
					fPedDistToDestination = VDIST2(vPedPos, vPedDestination)
					
					IF fEnemyDistFromPed < fPedDistToDestination + (fExtraDistance * fExtraDistance) //Ped is closer to the enemy than the destination.
						IF fEnemyDistToDestination < fPedDistToDestination //Enemy is closer to the destination than the ped is.
						OR fEnemyDistFromPed < 225.0 //Enemy is close in general to the ped from any direction.
							#IF IS_DEBUG_BUILD
								IF bDebugShowDebug
									SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
									DRAW_DEBUG_SPHERE(vClosestEnemyPos, 1.0, 0, 0, 255, 128)
								ENDIF
							#ENDIF
						
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			i++
		ENDWHILE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if any enemies are hanging around between the two coordinates.
FUNC BOOL ARE_ENEMIES_IN_BETWEEN_COORDS(PED_INDEX ped, VECTOR vStart, VECTOR vEnd, FLOAT fExtraDistance = 20.0)
	CONST_INT NUM_PEDS_TO_GRAB			16

	IF NOT IS_PED_INJURED(ped)
		PED_INDEX peds[NUM_PEDS_TO_GRAB]
		VECTOR vClosestEnemyPos
		FLOAT fEnemyDistFromStart, fEnemyDistFromEnd
		FLOAT fDistFromStartToEnd = VDIST2(vStart, vEnd)
		INT i = 0
		
		GET_PED_NEARBY_PEDS(ped, peds)
		
		WHILE i < NUM_PEDS_TO_GRAB 
			IF NOT IS_PED_INJURED(peds[i])
				IF GET_PED_RELATIONSHIP_GROUP_HASH(peds[i]) = relGroupBallas
					vClosestEnemyPos = GET_ENTITY_COORDS(peds[i])
					fEnemyDistFromStart = VDIST2(vClosestEnemyPos, vStart)
					fEnemyDistFromEnd = VDIST2(vClosestEnemyPos, vEnd)
					
					IF fEnemyDistFromStart < fEnemyDistFromEnd + (fExtraDistance * fExtraDistance) //Start point is closer to the enemy than the end point (the enemy is potentially in the way if you wanted to walk to the end point).
						IF fEnemyDistFromEnd < fDistFromStartToEnd //Enemy is closer to the end than the distance from the start coord to the end coord (the enemy is between the two coords).
						OR fEnemyDistFromStart < 225.0 //Enemy is close in general to the start point
							#IF IS_DEBUG_BUILD
								IF bDebugShowDebug
									SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
									DRAW_DEBUG_SPHERE(vClosestEnemyPos, 1.0, 0, 0, 255, 128)
								ENDIF
							#ENDIF
						
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			i++
		ENDWHILE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_BULLDOZER()
	IF NOT IS_ENTITY_DEAD(sBulldozer.veh)
		IF g_bFranklin2RequestedBackup
			IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
				IF NOT bBulldozerReachedDestination
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
						IF IS_SELECTOR_CAM_ACTIVE()
						AND bEnemiesHaveBeenAlerted
							SET_VEHICLE_FORWARD_SPEED(sBulldozer.veh, 2.0)
						ENDIF
					ELSE
						IF IS_VEHICLE_SEAT_FREE(sBulldozer.veh, VS_DRIVER)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sBulldozer.veh, TRUE)
						ELSE
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sBulldozer.veh, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//Update the bulldozer audio mix group: should only be active if the player isn't driving the bulldozer.
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBullDozer.veh)
			IF NOT bBulldozerIsInMixGroup
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(sBulldozer.veh, "FRANKLIN_2_BULLDOZER_Group")
				bBulldozerIsInMixGroup = TRUE
			ENDIF
		ELSE
			IF bBulldozerIsInMixGroup
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sBulldozer.veh)
				bBulldozerIsInMixGroup = FALSE
			ENDIF
		ENDIF
		
		IF NOT bBulldozerCoverDisabled
			IF GET_ENTITY_SPEED(sBulldozer.veh) > 2.0
				SET_VEHICLE_PROVIDES_COVER(sBulldozer.veh, FALSE)
				bBulldozerCoverDisabled = TRUE
			ENDIF
		ELSE
			IF GET_ENTITY_SPEED(sBulldozer.veh) < 0.5
				SET_VEHICLE_PROVIDES_COVER(sBulldozer.veh, TRUE)
				bBulldozerCoverDisabled = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///2070342 - If the player is in first-person and switches to Michael then they'll already be forced into snipe mode. Display help indicating that
///they still need to press the aim button to zoom.
PROC UPDATE_FIRST_PERSON_SNIPE_HELP_TEXT()
	IF eCurrentPlayer = SELECTOR_PED_MICHAEL
	AND GET_ALLOW_MOVEMENT_WHILE_ZOOMED()
	AND IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
	AND NOT IS_SELECTOR_CAM_ACTIVE()
		IF NOT bHasTextLabelTriggered[FRAN2_SNIPEHELP]
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_SECONDARY)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_SECONDARY)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT)
			OR GET_GAME_TIMER() - iFirstPersonSnipeAimTimer > 2000
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				
					IF IS_PC_VERSION()
						IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							PRINT_HELP("FRAN2_SNIPEHELP", DEFAULT_HELP_TEXT_TIME * 2) // ~s~Use ~INPUTGROUP_LOOK~ to move the reticle. ~n~Use ~INPUTGROUP_SNIPER_ZOOM_SECONDARY~ while holding ~INPUT_AIM~ to zoom in/out. ~n~Press ~INPUT_ATTACK~ to fire the weapon.
						ELSE
							PRINT_HELP("FRAN2_SNIPEHELP_KM", DEFAULT_HELP_TEXT_TIME * 2) // ~s~Use ~INPUTGROUP_LOOK~ to move the reticle. ~n~Use ~INPUTGROUP_SNIPER_ZOOM_SECONDARY~ to zoom in/out. ~n~Press ~INPUT_ATTACK~ to fire the weapon.
						ENDIF
					ELSE
						PRINT_HELP("FRAN2_SNIPEHELP", DEFAULT_HELP_TEXT_TIME * 2)
					ENDIF
					
					bHasTextLabelTriggered[FRAN2_SNIPEHELP] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SNIPEHELP")
		AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
			CLEAR_HELP()
		ENDIF
		IF IS_PC_VERSION()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SNIPEHELP_KM")
			AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
				CLEAR_HELP()
			ENDIF
		ENDIF
		iFirstPersonSnipeAimTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

///Sniping route: ped just needs to stand and snipe. May possibly have some scripted events later where they snipe specific peds.
PROC UPDATE_BUDDY_PED_SNIPE_PATH(PED_INDEX &pedBuddy, BUDDY_DATA &sBuddyData)
	IF NOT IS_PED_INJURED(pedBuddy)
		SEQUENCE_INDEX seq
	
		IF HAS_PED_GOT_WEAPON(pedBuddy, WEAPONTYPE_HEAVYSNIPER)
			WEAPON_TYPE eWeapon
			GET_CURRENT_PED_WEAPON(pedBuddy, eWeapon)
			
			IF eWeapon != WEAPONTYPE_HEAVYSNIPER
				SET_CURRENT_PED_WEAPON(pedBuddy, WEAPONTYPE_HEAVYSNIPER)
			ENDIF
		
			IF GET_AMMO_IN_PED_WEAPON(pedBuddy, WEAPONTYPE_HEAVYSNIPER) < 5
				ADD_AMMO_TO_PED(pedBuddy, WEAPONTYPE_HEAVYSNIPER, 5)
			ENDIF
		ENDIF
		
		sBuddyData.vDest = vMichaelSnipePos
		
		IF sBuddyData.bSwitchedFromThisPed	
			//Warp the player if we're currently switching and they're a long distance away from where they're meant to be.
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fDistToDest = VDIST2(sBuddyData.vDest, vPlayerPos)
			
			IF fDistToDest > 4.0
				IF IS_SELECTOR_CAM_ACTIVE()
					SET_ENTITY_COORDS(pedBuddy, sBuddyData.vDest)
				ENDIF
			ENDIF
			
			sBuddyData.bRefreshTasks = TRUE
			sBuddyData.bSwitchedFromThisPed = FALSE
		ENDIF
		
		IF bEnemiesHaveBeenAlerted
			IF iSwitchStage = 0
				IF (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK AND NOT IS_PED_IN_COMBAT(pedBuddy))
				OR sBuddyData.bRefreshTasks
					SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
					
					OPEN_SEQUENCE_TASK(seq)
						IF VDIST2(sBuddyData.vDest, GET_ENTITY_COORDS(pedBuddy)) > 100.0
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, sBuddyData.vDest, PEDMOVE_RUN, 60000, DEFAULT, ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 1000.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					SET_COMBAT_FLOAT(pedBuddy, CCF_MAX_SHOOTING_DISTANCE, 500.0)
					SET_PED_STEALTH_MOVEMENT(pedBuddy, FALSE)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Bulldozer route: ped drives bulldozer up to the timber area, then gets out and joins the fron assault route.
PROC UPDATE_BUDDY_PED_BULLDOZER_PATH(PED_INDEX &pedBuddy, BUDDY_DATA &sBuddyData)
	BOOL bNeedToChangeRoute = FALSE
	VECTOR vBuddyPos
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())

	IF NOT IS_PED_INJURED(pedBuddy)
		vBuddyPos = GET_ENTITY_COORDS(pedBuddy)
	
		IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
			REQUEST_WAYPOINT_RECORDING(strWaypointBulldozer)

			IF IS_PED_IN_VEHICLE(pedBuddy, sBulldozer.veh)
				IF GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointBulldozer)
					BOOL bPlayerBlockingBulldozer = FALSE
					VECTOR vPlayerOffset
				
					//Stop the bulldozer if the player gets in front.
					IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
						vPlayerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sBulldozer.veh, vPlayerPos)
						
						IF vPlayerOffset.y < 6.0 AND vPlayerOffset.y > 3.0
							IF ABSF(vPlayerOffset.x) < 1.25
								IF GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_VEHICLE_TEMP_ACTION) = FINISHED_TASK
									IF GET_ENTITY_SPEED(sBulldozer.veh) > 1.0
										TASK_VEHICLE_TEMP_ACTION(pedBuddy, sBulldozer.veh, TEMPACT_BRAKE, -1)
									ENDIF
								ENDIF
								
								bPlayerBlockingBulldozer = TRUE
							ENDIF
						ENDIF
					ENDIF
				
					IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(sBulldozer.veh)
						IF NOT bBulldozerReachedDestination
						AND NOT bPlayerBlockingBulldozer
							INT iClosestNode
							WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(strWaypointBulldozer, GET_ENTITY_COORDS(sBulldozer.veh), iClosestNode)
							
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedBuddy, sBulldozer.veh, strWaypointBulldozer, DRIVINGMODE_PLOUGHTHROUGH, iClosestNode + 2, 
																   EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBuddy, TRUE)
							fBulldozerCruiseSpeed = 4.0
						ENDIF
					ELSE
						INT iClosestNode
						WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(strWaypointBulldozer, GET_ENTITY_COORDS(sBulldozer.veh), iClosestNode)
					
						IF GET_GAME_TIMER() - iBulldozerLiftTimer > 2000
						AND GET_GAME_TIMER() - iBulldozerLiftTimer < 5200
							SET_VEHICLE_BULLDOZER_ARM_POSITION(sBulldozer.veh, 0.9, FALSE)
						ELSE
							IF iClosestNode > 10 AND iClosestNode < 25
								SET_VEHICLE_BULLDOZER_ARM_POSITION(sBulldozer.veh, 0.05, FALSE)
							ELSE
								SET_VEHICLE_BULLDOZER_ARM_POSITION(sBulldozer.veh, 0.1, FALSE)
							ENDIF
						ENDIF						
						
						//Rubber band the bulldozer based on the player.
						IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
							IF iClosestNode > 9
								FLOAT fOffset = vPlayerOffset.y
								FLOAT fDesiredSpeed = 4.0
							
								IF iClosestNode < 17
									IF fOffset < -2.0
										fDesiredSpeed = 1.0
									ELSE
										fDesiredSpeed = 4.0
									ENDIF
								ELSE
									IF fOffset < -2.0
										fDesiredSpeed = 5.0
									ELIF 
										fDesiredSpeed = 10.0
									ENDIF
								ENDIF
								
								fBulldozerCruiseSpeed = fBulldozerCruiseSpeed +@ (((fDesiredSpeed - fBulldozerCruiseSpeed) * 0.01) * 30.0)
								SET_DRIVE_TASK_CRUISE_SPEED(pedBuddy, fBulldozerCruiseSpeed)
							ENDIF
						ENDIF
					ENDIF
						
					IF IS_ENTITY_IN_ANGLED_AREA(sBulldozer.veh, <<-512.429565,5258.324707,79.609993>>, <<-515.066040,5217.234375,83.786385>>, 8.0)
						bBulldozerReachedDestination = TRUE
						bNeedToChangeRoute = TRUE
					ENDIF
					
					//Check for nearby peds and vehicles to the bulldozer
					VEHICLE_INDEX vehClosest[2]
					PED_INDEX pedClosest[4]
					GET_PED_NEARBY_VEHICLES(pedBuddy, vehClosest)
					GET_PED_NEARBY_PEDS(pedBuddy, pedClosest)
					
					//Play dialogue if an enemy ped tries to jack Trevor.
					IF GET_GAME_TIMER() - iBulldozerJackTimer > 10000
					AND pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
					AND VDIST2(vPlayerPos, vBuddyPos) < 625.0
						BOOL bEnemyTryingToJack = FALSE
						INT i = 0
						
						REPEAT COUNT_OF(pedClosest) i
							IF NOT IS_PED_INJURED(pedClosest[i])
							AND GET_PED_RELATIONSHIP_GROUP_HASH(pedClosest[i]) = relGroupBallas
								IF VDIST2(GET_ENTITY_COORDS(pedClosest[i]), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sBulldozer.veh, <<-1.9927, -0.0424, 0.8947>>)) < 9.0
									bEnemyTryingToJack = TRUE
								ENDIF
							ENDIF
						ENDREPEAT
						
						IF bEnemyTryingToJack
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_jack", CONV_PRIORITY_MEDIUM)
								iBulldozerJackTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
					
					//Set vehicles on fire when touching them.
					IF NOT IS_ENTITY_DEAD(vehClosest[1])
					AND vehClosest[1] != sBulldozer.veh
						IF VDIST2(GET_ENTITY_COORDS(vehClosest[1]), vBuddyPos) < 100.0
							IF GET_VEHICLE_ENGINE_HEALTH(vehClosest[1]) > 0.0
								IF IS_ENTITY_TOUCHING_ENTITY(sBulldozer.veh, vehClosest[1])
									SET_VEHICLE_ENGINE_HEALTH(vehClosest[1], 0.0)
									
									iBulldozerLiftTimer = GET_GAME_TIMER()
								ENDIF
							ELSE
								//Play some specific dialogue if the buddy is Trevor.
								IF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
									IF VDIST2(vPlayerPos, vBuddyPos) < 625.0
										IF NOT bHasTextLabelTriggered[LM2_move]
											IF VDIST2(vBuddyPos, <<-563.1, 5375.6, 69.8>>) < 400.0
												IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_move", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[LM2_move] = TRUE
												ENDIF
											ENDIF
										ELIF NOT bHasTextLabelTriggered[LM2_move2]
											IF VDIST2(vBuddyPos, <<-584.2, 5312.7, 69.8>>) < 400.0
												IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_move2", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[LM2_move2] = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//If the player is also on the bulldozer route treat the ped in the bulldozer as the player.
					IF ePlayerRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
						SET_PED_CONFIG_FLAG(pedBuddy, PCF_TreatAsPlayerDuringTargeting, TRUE)
					ELSE
						SET_PED_CONFIG_FLAG(pedBuddy, PCF_TreatAsPlayerDuringTargeting, FALSE)
					ENDIF
				
					//Specific events along the bulldozer path.
					SWITCH sBulldozer.iEvent
						CASE 0
							//If the rocket ped spawns then stop the bulldozer.
							IF sBallasRocket[0].bIsCreated
								IF iBallaRocketTimer != 0
									IF GET_GAME_TIMER() - iBallaRocketTimer > 1500
										bNeedToChangeRoute = TRUE
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							
						BREAK
					ENDSWITCH
				ENDIF
			ELSE
				bNeedToChangeRoute = TRUE
			ENDIF
		ELSE
			bNeedToChangeRoute = TRUE
		ENDIF
		
		IF bNeedToChangeRoute
			IF VDIST2(vBuddyPos, <<-512.429565,5258.324707,79.609993>>) < 6400.0
			AND IS_PED_INJURED(sBallasRocket[0].ped)
				IF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
					eFranklinRoute = BUDDY_ROUTE_FRONT_ASSAULT
				ELSE
					eTrevorRoute = BUDDY_ROUTE_FRONT_ASSAULT
				ENDIF
			ELSE
				IF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
					eFranklinRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
				ELSE
					eTrevorRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(sBulldozer.veh)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sBulldozer.veh, FALSE)
			ENDIF
			
			SET_PED_CONFIG_FLAG(pedBuddy, PCF_TreatAsPlayerDuringTargeting, FALSE)
			
			sBuddyData.iEvent = 0
			sBuddyData.bSwitchedFromThisPed = TRUE
			sBuddyData.bRefreshTasks = TRUE
		ENDIF
	ENDIF
ENDPROC

//Bulldozer alternate route: ped takes same route as bulldozer but on foot, eventually meeting up with where the bulldozer would have stopped.
PROC UPDATE_BUDDY_PED_BULLDOZER_ON_FOOT_PATH(PED_INDEX &pedBuddy, BUDDY_DATA &sBuddyData)
	IF NOT IS_PED_INJURED(pedBuddy)
		VECTOR vBuddyPos = GET_ENTITY_COORDS(pedBuddy)
		FLOAT fDistToDestination
		BOOL bPlayerStoleCover = FALSE
		BOOL bKilledFirstWave = TRUE
		BOOL bIsInCombat = (IS_PED_IN_COMBAT(pedBuddy) OR GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK)
		SEQUENCE_INDEX seq
		VECTOR vOtherBuddysCurrentDestination
		
		IF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			vOtherBuddysCurrentDestination = sFranklin.vDest
		ELSE
			vOtherBuddysCurrentDestination = sTrevor.vDest
		ENDIF
		
		IF HAS_PED_GOT_WEAPON(pedBuddy, WEAPONTYPE_CARBINERIFLE)
			IF GET_AMMO_IN_PED_WEAPON(pedBuddy, WEAPONTYPE_CARBINERIFLE) < 50
				ADD_AMMO_TO_PED(pedBuddy, WEAPONTYPE_CARBINERIFLE, 50)
			ENDIF
		ENDIF
		
		VECTOR vDest[5]
		vDest[0] = <<-561.3218, 5379.8799, 69.0322>> //no cover
		vDest[1] = <<-563.3644, 5350.0957, 69.2162>> //156.7, wall to both, low
		vDest[2] = <<-571.0067, 5331.9878, 69.2144>> //156.7, wall to both, low
		vDest[3] = <<-598.5005, 5291.3999, 69.2144>> //no cover
		vDest[4] = <<-573.1577, 5274.5259, 69.2604>> //-126.0, wall to both, low
		
		VECTOR vAltDest[5]
		vAltDest[0] = <<-551.4478, 5375.3696, 69.4518>> //156.7, wall to both, low
		vAltDest[1] = <<-572.2329, 5357.8052, 69.2145>> //156.7, wall to both, low
		vAltDest[2] = <<-579.6328, 5342.0933, 69.2144>> //156.7, wall to right, high
		vAltDest[3] = <<-584.6021, 5285.1162, 69.2604>> //-122.7, wall to left, high
		vAltDest[4] = <<-584.8169, 5285.1431, 69.2604>> //-122.7, wall to left, high
		
		//If the player switched away from this ped then we need to recalculate where they should be on the route.
		IF sBuddyData.bSwitchedFromThisPed
			INT i = 0
			INT iClosestDest = 0
			FLOAT fClosestDist = -1.0
			FLOAT fDist
			
			REPEAT COUNT_OF(vDest) i
				fDist = VDIST2(vBuddyPos, vDest[i])
				
				IF fClosestDist = -1.0
				OR fDist < fClosestDist
					iClosestDest = i
					fClosestDist = fDist
				ENDIF
			ENDREPEAT
			
			//Check if this destination would put the ped into a group of enemies. If so, take the previous destination as the closest.
			IF iClosestDest > 0
				IF ARE_ENEMIES_IN_BETWEEN_COORDS(pedBuddy, vDest[iClosestDest - 1], vDest[iClosestDest])
					iClosestDest = iClosestDest - 1
				ENDIF
			ENDIF
			
			IF iClosestDest = 0
				sBuddyData.iEvent = 0
			ELIF iClosestDest = 1
				sBuddyData.iEvent = 1
			ELIF iClosestDest = 2
				sBuddyData.iEvent = 2
			ELIF iClosestDest = 3
				sBuddyData.iEvent = 3
			ELSE
				sBuddyData.iEvent = 4
			ENDIF
			
			//Warp the player if we're currently switching and they're a long distance away from where they're meant to be.
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fDistToDest = VDIST2(vDest[iClosestDest], vPlayerPos)
			FLOAT fDistToAltDest = VDIST2(vAltDest[iClosestDest], vPlayerPos)
			
			IF fDistToDest < fDistToAltDest
				IF fDistToDest > 400.0
					IF IS_SELECTOR_CAM_ACTIVE()
						SET_ENTITY_COORDS(pedBuddy, vDest[iClosestDest])
					ENDIF
				ENDIF
				
				sBuddyData.vDest = vDest[iClosestDest]
				sBuddyData.bIsUsingSecondaryCover = FALSE
			ELSE
				IF fDistToAltDest > 400.0
					IF IS_SELECTOR_CAM_ACTIVE()
						SET_ENTITY_COORDS(pedBuddy, vAltDest[iClosestDest])
					ENDIF
				ENDIF
				
				sBuddyData.vDest = vAltDest[iClosestDest]
				sBuddyData.bIsUsingSecondaryCover = TRUE
			ENDIF
			
			sBuddyData.iTimer = 0
			sBuddyData.bRefreshTasks = TRUE
			sBuddyData.bSwitchedFromThisPed = FALSE
		ENDIF
		
		IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, <<0.0, 0.0, 0.0>>)
			sBuddyData.vDest = vDest[0]
		ENDIF

		SWITCH sBuddyData.iEvent
			CASE 0 //Fight at the rock outside the entrance until it's safe to enter.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[0]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[0]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))		
					IF NOT ARE_VECTORS_ALMOST_EQUAL(vDest[0], sBuddyData.vDest)
					AND NOT ARE_VECTORS_ALMOST_EQUAL(vAltDest[0], sBuddyData.vDest)
						sBuddyData.vDest = vDest[0]
					ENDIF
					
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[0])
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 156.7, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-590.8, 5276.8, 71.5>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[1])
						bKilledFirstWave = TRUE
						
						IF ePointOfEnemySpawn = ENTRY_POINT_BY_BULLDOZER
							IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterAlert) > 0
								bKilledFirstWave = FALSE
							ENDIF
						ENDIF
					
						IF bKilledFirstWave
							sBuddyData.vDest = vDest[1]
							SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
							sBuddyData.iTimer = 0
							sBuddyData.iEvent++
							sBuddyData.bRefreshTasks = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[1]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[1]							
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[1])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 156.7, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 156.7, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-590.8, 5276.8, 71.5>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[2])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasBulldozer1) = 0
						sBuddyData.vDest = vDest[2]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[2]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[2]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[2])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 156.7, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 156.7, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-590.8, 5276.8, 71.5>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[3])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasBulldozer2) = 0
						sBuddyData.vDest = vDest[3]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[3]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[3]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[3])
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -122.7, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ENDIF				
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-553.1, 5258.8, 73.3>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[4])
						sBuddyData.vDest = vDest[4]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[4]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[4]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[4])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -126.0, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -122.7, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-553.1, 5258.8, 73.3>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				
				//From this point onwards the route joins up with the front assault, so switch the buddy ped over to that route.
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, <<-566.3613, 5256.4595, 69.4670>>)
					AND IS_PED_INJURED(sBallasRocket[0].ped) 
						IF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
							eFranklinRoute = BUDDY_ROUTE_FRONT_ASSAULT
						ELIF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
							eTrevorRoute = BUDDY_ROUTE_FRONT_ASSAULT
						ENDIF
						
						sBuddyData.iEvent = 0
						sBuddyData.bSwitchedFromThisPed = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//Front route: ped goes on foot from the front.
PROC UPDATE_BUDDY_PED_FRONT_PATH(PED_INDEX &pedBuddy, BUDDY_DATA &sBuddyData)
	IF NOT IS_PED_INJURED(pedBuddy)
		VECTOR vBuddyPos = GET_ENTITY_COORDS(pedBuddy)
		FLOAT fDistToDestination
		BOOL bPlayerStoleCover = FALSE
		BOOL bKilledFirstWave = TRUE
		BOOL bIsInCombat = (IS_PED_IN_COMBAT(pedBuddy) OR GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK)
		SEQUENCE_INDEX seq
		VECTOR vOtherBuddysCurrentDestination
		
		IF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			vOtherBuddysCurrentDestination = sFranklin.vDest
		ELSE
			vOtherBuddysCurrentDestination = sTrevor.vDest
		ENDIF
		
		IF HAS_PED_GOT_WEAPON(pedBuddy, WEAPONTYPE_CARBINERIFLE)
			IF GET_AMMO_IN_PED_WEAPON(pedBuddy, WEAPONTYPE_CARBINERIFLE) < 50
				ADD_AMMO_TO_PED(pedBuddy, WEAPONTYPE_CARBINERIFLE, 50)
			ENDIF
		ENDIF
		
		VECTOR vDest[8]
		//There are two potential start positions for this route, depending on where the player started.
		IF VDIST2(vBuddyPos, <<-592.0587, 5236.6323, 69.8622>>) < VDIST2(vBuddyPos, <<-598.3776, 5286.8184, 69.2335>>)
			vDest[0] = <<-592.0587, 5236.6323, 69.8622>> //-31.4, wall to both, low
		ELSE
			vDest[0] = <<-598.3776, 5286.8184, 69.2335>> //no cover
		ENDIF
		vDest[1] = <<-566.3613, 5256.4595, 69.4670>> //-108.3, wall to right, high
		vDest[2] = <<-547.0441, 5270.3545, 73.1741>> //-115.0, wall to left, high
		vDest[3] = <<-522.0834, 5250.7563, 78.5639>> //-23.1, wall to both, low
		vDest[4] = <<-518.9554, 5255.7407, 79.5127>> //-30.7, wall to both, low
		vDest[5] = <<-504.8981, 5280.0112, 79.6100>> //-22.7, wall to both, low
		vDest[6] = <<-497.9232, 5299.3667, 79.6100>> //68.6, wall to both, low
		vDest[7] = <<-504.7159, 5300.7744, 79.5876>> //no cover
		
		VECTOR vAltDest[8]
		vAltDest[0] = <<-583.5048, 5248.0664, 69.4689>> //no cover
		vAltDest[1] = <<-566.3081, 5271.2119, 69.2443>> //-107.8, wall to both, low
		vAltDest[2] = <<-544.6422, 5255.0625, 73.2873>> //231.8, wall to both, low
		vAltDest[3] = <<-508.5136, 5255.0532, 79.6276>> //-14.4, wall to both, low
		vAltDest[4] = <<-508.5136, 5255.0532, 79.6276>> //-14.4, wall to both, low
		vAltDest[5] = <<-501.2620, 5275.9766, 79.6100>> //-21.3, wall to right, high
		vAltDest[6] = <<-497.8619, 5302.2593, 79.6041>> //68.6, wall to both, low
		vAltDest[7] = <<-504.2799, 5304.1685, 79.4737>> //no cover
		
		//If the player switched away from this ped then we need to recalculate where they should be on the route.
		IF sBuddyData.bSwitchedFromThisPed
			INT i = 0
			INT iClosestDest = 0
			FLOAT fClosestDist = -1.0
			FLOAT fDist
			
			//Find the closest destination to the ped.
			REPEAT COUNT_OF(vDest) i
				fDist = VDIST2(vBuddyPos, vDest[i])
				
				IF fClosestDist = -1.0
				OR fDist < fClosestDist
					iClosestDest = i
					fClosestDist = fDist
				ENDIF
			ENDREPEAT
			
			//Check if this destination would put the ped into a group of enemies. If so, take the previous destination as the closest.
			IF iClosestDest > 0
				IF ARE_ENEMIES_IN_BETWEEN_COORDS(pedBuddy, vDest[iClosestDest - 1], vDest[iClosestDest])
					iClosestDest = iClosestDest - 1
				ENDIF
			ENDIF
			
			//Assign the event index that matches the closest destination.
			IF iClosestDest = 0
				sBuddyData.iEvent = 0
			ELIF iClosestDest = 1
				sBuddyData.iEvent = 1
			ELIF iClosestDest = 2
				sBuddyData.iEvent = 2
			ELIF iClosestDest = 3
				sBuddyData.iEvent = 3
			ELIF iClosestDest = 4
				sBuddyData.iEvent = 4
			ELIF iClosestDest = 5
				sBuddyData.iEvent = 5
			ELIF iClosestDest = 6
				sBuddyData.iEvent = 6
			ELSE
				sBuddyData.iEvent = 7
			ENDIF
			
			//Warp the player if we're currently switching and they're a long distance away from where they're meant to be.
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fDistToDest = VDIST2(vDest[iClosestDest], vPlayerPos)
			FLOAT fDistToAltDest = VDIST2(vAltDest[iClosestDest], vPlayerPos)
			
			IF fDistToDest < fDistToAltDest
				IF fDistToDest > 400.0
					IF IS_SELECTOR_CAM_ACTIVE()
						SET_ENTITY_COORDS(pedBuddy, vDest[iClosestDest])
					ENDIF
				ENDIF
				
				sBuddyData.vDest = vDest[iClosestDest]
				sBuddyData.bIsUsingSecondaryCover = FALSE
			ELSE
				IF fDistToAltDest > 400.0
					IF IS_SELECTOR_CAM_ACTIVE()
						SET_ENTITY_COORDS(pedBuddy, vAltDest[iClosestDest])
					ENDIF
				ENDIF
				
				sBuddyData.vDest = vAltDest[iClosestDest]
				sBuddyData.bIsUsingSecondaryCover = TRUE
			ENDIF
			
			sBuddyData.iTimer = 0
			sBuddyData.bRefreshTasks = TRUE
			sBuddyData.bSwitchedFromThisPed = FALSE
		ENDIF
		
		//Failsafe in case something went wrong with determining the next destination.
		IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, <<0.0, 0.0, 0.0>>)
			sBuddyData.vDest = vDest[0]
		ENDIF

		SWITCH sBuddyData.iEvent
			CASE 0 //Fight at the rock outside the entrance until it's safe to enter.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[0]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[0]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))		
					SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							
							IF fDistToDestination > VDIST2(sBuddyData.vDest, <<-664.8, 5242.8, 76.3>>)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-664.8, 5242.8, 76.3>>, <<-570.3, 5267.9, 70.9>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																	ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
							ENDIF
							
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, <<-570.3, 5267.9, 70.9>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																	ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[1])
						bKilledFirstWave = TRUE
						
						IF (ePointOfEnemySpawn = ENTRY_POINT_BY_CONVEYOR_BELT OR ePointOfEnemySpawn = ENTRY_POINT_BY_SNIPER_HILL)
							IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterAlert) > 0
								bKilledFirstWave = FALSE
							ENDIF
						ELIF ePointOfEnemySpawn = ENTRY_POINT_BY_METAL_BRIDGE
							//If the player took the left fork at the front then the buddy usually takes the right fork: only check for alert peds that are closer to the right fork.
							IF NOT IS_PED_INJURED(sBallasAfterAlert[0].ped)
								bKilledFirstWave = FALSE
							ENDIF
						ENDIF
						
						IF bKilledFirstWave
						AND (GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasBulldozer3) = 0 OR NOT sBallasBulldozer3[0].bIsCreated)
							sBuddyData.vDest = vDest[1]
							SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
							
							sBuddyData.iTimer = 0
							sBuddyData.iEvent++
							sBuddyData.bRefreshTasks = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1 //Heading up to the timber
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[1]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[1]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))		
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[1])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -108.3, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -107.8, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, <<-541.5, 5254.5, 75.6>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																	ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[2])
						sBuddyData.vDest = vDest[2]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 2		
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[2]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[2]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))		
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[2])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -115.0, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 231.8, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, <<-541.5, 5254.5, 75.6>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																	ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(pedBuddy)
					IF IS_ENTITY_IN_ANGLED_AREA(pedBuddy, <<-543.377502,5254.161621,76.240410>>, <<-575.465820,5268.107910,67.468452>>, 19.500000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasBeforeTimber) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), pedBuddy, <<-524.1, 5246.2, 80.0>>)
								SET_PED_SHOOTS_AT_COORD(pedBuddy, <<-524.1, 5246.2, 80.0>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[3])
						sBuddyData.vDest = vDest[3]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 3	
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[3]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[3]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[3])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -23.1, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -14.4, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, <<-492.6, 5304.7, 81.6>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																	ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(pedBuddy)
					IF IS_ENTITY_IN_ANGLED_AREA(pedBuddy, <<-509.119965,5255.369141,82.360039>>, <<-515.347961,5237.144531,77.304115>>, 19.500000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber1) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), pedBuddy, <<-492.6, 5304.7, 81.6>>)
								SET_PED_SHOOTS_AT_COORD(pedBuddy, <<-492.6, 5304.7, 81.6>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[4])
						sBuddyData.vDest = vDest[4]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 4	//Kill any remaining enemies before heading up to the timber area.	
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[4]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[4]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[4])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -30.7, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -14.4, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, <<-492.6, 5304.7, 81.6>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																	ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(pedBuddy)
					IF IS_ENTITY_IN_ANGLED_AREA(pedBuddy, <<-511.059113,5255.000488,79.360039>>, <<-498.243927,5290.137207,83.110039>>, 14.250000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber1) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber2) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), pedBuddy, <<-492.6, 5304.7, 81.6>>)
								SET_PED_SHOOTS_AT_COORD(pedBuddy, <<-492.6, 5304.7, 81.6>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[5])
					AND (sBallasTimber1[0].bIsCreated AND IS_PED_INJURED(sBallasTimber1[0].ped) AND IS_PED_INJURED(sBallasTimber1[1].ped))
					AND IS_PED_INJURED(sBallasSniper[0].ped) //This ped may never get created, so we only care that it's alive.
						sBuddyData.vDest = vDest[5]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 5	//Kill enemies in the timber area
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[5]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[5]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[5])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -22.7, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -21.3, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, <<-492.6, 5304.7, 81.6>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																	ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(pedBuddy)
					IF IS_ENTITY_IN_ANGLED_AREA(pedBuddy, <<-511.059113,5255.000488,79.360039>>, <<-498.243927,5290.137207,83.110039>>, 14.250000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber1) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber2) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), pedBuddy, <<-492.6, 5304.7, 81.6>>)
								SET_PED_SHOOTS_AT_COORD(pedBuddy, <<-492.6, 5304.7, 81.6>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[6])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber2) = 0
					AND IS_PED_INJURED(sBallasSniper[0].ped) //This ped may never get created, so we only care that it's alive.
						sBuddyData.vDest = vDest[6]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 6	//near to Lamar: take out remaining enemies.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[6]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[6]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))		
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[6])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 68.6, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 68.6, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, <<-520.9, 5306.8, 80.8>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																	ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
					
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[7])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasGuardingLamar) = 0
						sBuddyData.vDest = vDest[7]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 7	//Go to near where Lamar is, fight any nearby enemies, once they'e dead aim at a coord.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 3.0, 2.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[7]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[7]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
			
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND fDistToDestination > 4.0)	
					VECTOR vAimPos
					
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[7])
						vAimPos = <<-537.1587, 5268.9067, 79.4526>>
					ELSE
						vAimPos = <<-487.8635, 5318.1475, 81.0427>>
					ENDIF
				
					SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, vAimPos, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						TASK_AIM_GUN_AT_COORD(NULL, vAimPos, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
			BREAK
		ENDSWITCH

	ENDIF
ENDPROC

//Rear route: ped goes on-foot from the rear.
PROC UPDATE_BUDDY_PED_REAR_PATH(PED_INDEX &pedBuddy, BUDDY_DATA &sBuddyData)
	IF NOT IS_PED_INJURED(pedBuddy)
		VECTOR vBuddyPos = GET_ENTITY_COORDS(pedBuddy)
		FLOAT fDistToDestination
		BOOL bPlayerStoleCover = FALSE
		BOOL bIsInCombat = (IS_PED_IN_COMBAT(pedBuddy) OR GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK)
		SEQUENCE_INDEX seq
		VECTOR vOtherBuddysCurrentDestination
		
		IF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			vOtherBuddysCurrentDestination = sFranklin.vDest
		ELSE
			vOtherBuddysCurrentDestination = sTrevor.vDest
		ENDIF
		
		IF HAS_PED_GOT_WEAPON(pedBuddy, WEAPONTYPE_CARBINERIFLE)
			IF GET_AMMO_IN_PED_WEAPON(pedBuddy, WEAPONTYPE_CARBINERIFLE) < 50
				ADD_AMMO_TO_PED(pedBuddy, WEAPONTYPE_CARBINERIFLE, 50)
			ENDIF
		ENDIF
		
		VECTOR vDest[6]
		vDest[0] = <<-487.6498, 5385.0801, 77.0692>> //-176.6, wall to both, low
		vDest[1] = <<-462.0061, 5339.4502, 82.4832>> //no cover
		vDest[2] = <<-476.0758, 5322.0405, 79.6100>> //156.4, wall to left, low
		vDest[3] = <<-483.3072, 5315.9570, 79.6100>> //153.5, wall to both, low
		vDest[4] = <<-497.8078, 5302.2251, 79.6052>> //65.6, wall to both, low
		vDest[5] = <<-504.7159, 5300.7744, 79.5876>> //no cover
		
		VECTOR vAltDest[6]
		vAltDest[0] = <<-471.3402, 5380.9253, 78.8354>> //158.9, wall to left, low
		vAltDest[1] = <<-462.9181, 5335.0532, 82.4505>> //150.6, wall to left, low
		vAltDest[2] = <<-483.2917, 5321.4258, 79.6100>> //158.8, wall to both, low
		vAltDest[3] = <<-489.7770, 5318.1147, 79.6100>> //158.4, wall to both, low
		vAltDest[4] = <<-497.9330, 5299.2705, 79.6100>> //65.6, wall to both, low
		vAltDest[5] = <<-504.2799, 5304.1685, 79.4737>> //no cover
		
		//If the player switched away from this ped then we need to recalculate where they should be on the route.
		IF sBuddyData.bSwitchedFromThisPed
			INT i = 0
			INT iClosestDest = 0
			FLOAT fClosestDist = -1.0
			FLOAT fDist
			
			REPEAT COUNT_OF(vDest) i
				fDist = VDIST2(vBuddyPos, vDest[i])
				
				IF fClosestDist = -1.0
				OR fDist < fClosestDist
					iClosestDest = i
					fClosestDist = fDist
				ENDIF
			ENDREPEAT
			
			//Check if this destination would put the ped into a group of enemies. If so, take the previous destination as the closest.
			IF iClosestDest > 0
				IF ARE_ENEMIES_IN_BETWEEN_COORDS(pedBuddy, vDest[iClosestDest - 1], vDest[iClosestDest])
					iClosestDest = iClosestDest - 1
				ENDIF
			ENDIF
			
			IF iClosestDest = 0
				sBuddyData.iEvent = 0
			ELIF iClosestDest = 1
				sBuddyData.iEvent = 1
			ELIF iClosestDest = 2
				sBuddyData.iEvent = 2
			ELIF iClosestDest = 3
				sBuddyData.iEvent = 3
			ELIF iClosestDest = 4
				sBuddyData.iEvent = 4
			ELSE
				sBuddyData.iEvent = 5
			ENDIF
			
			//Warp the player if we're currently switching and they're a long distance away from where they're meant to be.
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fDistToDest = VDIST2(vDest[iClosestDest], vPlayerPos)
			FLOAT fDistToAltDest = VDIST2(vAltDest[iClosestDest], vPlayerPos)
			
			IF fDistToDest < fDistToAltDest
				IF fDistToDest > 400.0
					IF IS_SELECTOR_CAM_ACTIVE()
						SET_ENTITY_COORDS(pedBuddy, vDest[iClosestDest])
					ENDIF
				ENDIF
				
				sBuddyData.vDest = vDest[iClosestDest]
				sBuddyData.bIsUsingSecondaryCover = FALSE
			ELSE
				IF fDistToAltDest > 400.0
					IF IS_SELECTOR_CAM_ACTIVE()
						SET_ENTITY_COORDS(pedBuddy, vAltDest[iClosestDest])
					ENDIF
				ENDIF
				
				sBuddyData.vDest = vAltDest[iClosestDest]
				sBuddyData.bIsUsingSecondaryCover = TRUE
			ENDIF
			
			sBuddyData.iTimer = 0
			sBuddyData.bRefreshTasks = TRUE
			sBuddyData.bSwitchedFromThisPed = FALSE
		ENDIF
		
		IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, <<0.0, 0.0, 0.0>>)
			sBuddyData.vDest = vDest[0]
		ENDIF

		SWITCH sBuddyData.iEvent
			CASE 0 //Fight at the rock outside the entrance until it's safe to enter.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[0]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[0]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))		
					IF NOT ARE_VECTORS_ALMOST_EQUAL(vDest[0], sBuddyData.vDest)
					AND NOT ARE_VECTORS_ALMOST_EQUAL(vAltDest[0], sBuddyData.vDest)
						sBuddyData.vDest = vDest[0]
					ENDIF
					
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[0])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -176.6, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 158.9, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-498.8398, 5292.7832, 81.7003>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[1])
						sBuddyData.vDest = vDest[1]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 1 //Climb up the ladder.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 2.0, 2.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[1]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[1]					
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[1])
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 150.6, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							IF vBuddyPos.z < 83.0
								TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<-463.8806, 5341.8472, 79.7449>>, <<-498.8398, 5292.7832, 81.7003>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  	  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-462.0908, 5340.9727, 82.4936>>, PEDMOVE_RUN, -1, 0.5, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
							ENDIF
							
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-498.8398, 5292.7832, 81.7003>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//Warp the buddy up the ladder if the player isn't looking.
				IF vBuddyPos.z < 83.0
				AND VDIST2(vBuddyPos, <<-463.8806, 5341.8472, 79.7449>>) < 4.0
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBuddyPos) > 100.0
						IF WOULD_ENTITY_BE_OCCLUDED(GET_ENTITY_MODEL(pedBuddy), <<-463.8806, 5341.8472, 79.7449>>)
						AND WOULD_ENTITY_BE_OCCLUDED(GET_ENTITY_MODEL(pedBuddy), <<-462.0908, 5340.9727, 82.4936>>)
							SET_ENTITY_COORDS(pedBuddy, <<-462.0908, 5340.9727, 82.4936>>)
						ENDIF
					ENDIF
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[2])
					AND vBuddyPos.z > 83.0
						sBuddyData.vDest = vDest[2]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 2 //Go down to the timber stacks.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[2]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[2]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[2])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 156.4, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 158.8, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-498.8398, 5292.7832, 81.7003>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[3])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasRearTimber1) = 0
						sBuddyData.vDest = vDest[3]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[3]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[3]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[3])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 153.5, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 158.5, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF	
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-498.8398, 5292.7832, 81.7003>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[4])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasRearTimber2) = 0
						sBuddyData.vDest = vDest[4]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[4]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[4]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[4])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 65.6, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 65.6, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-523.1256, 5307.7715, 80.4874>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//From this point onwards the route joins up with the front assault, so switch the buddy ped over to that route.
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[5])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasGuardingLamar) = 0
						sBuddyData.vDest = vDest[5]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 5	//Go to Lamar
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 3.0, 2.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[5]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[5]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND fDistToDestination > 4.0)	
					VECTOR vAimPos
				
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[5])
						vAimPos = <<-537.1587, 5268.9067, 79.4526>>
					ELSE
						vAimPos = <<-487.8635, 5318.1475, 81.0427>>
					ENDIF
				
					SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, vAimPos, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, 
																ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						TASK_AIM_GUN_AT_COORD(NULL, vAimPos, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


//Route to the exit after the player has rescued Lamar.
PROC UPDATE_BUDDY_PED_AFTER_SAVING_LAMAR(PED_INDEX &pedBuddy, BUDDY_DATA &sBuddyData)
	IF NOT IS_PED_INJURED(pedBuddy)
		VECTOR vBuddyPos = GET_ENTITY_COORDS(pedBuddy)
		FLOAT fDistToDestination
		BOOL bPlayerStoleCover = FALSE
		BOOL bIsInCombat = (IS_PED_IN_COMBAT(pedBuddy) OR GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK)
		SEQUENCE_INDEX seq
		VECTOR vOtherBuddysCurrentDestination
		
		IF pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			vOtherBuddysCurrentDestination = sFranklin.vDest
		ELSE
			vOtherBuddysCurrentDestination = sTrevor.vDest
		ENDIF
		
		IF HAS_PED_GOT_WEAPON(pedBuddy, WEAPONTYPE_CARBINERIFLE)
			IF GET_AMMO_IN_PED_WEAPON(pedBuddy, WEAPONTYPE_CARBINERIFLE) < 50
				ADD_AMMO_TO_PED(pedBuddy, WEAPONTYPE_CARBINERIFLE, 50)
			ENDIF
		ENDIF
		
		SET_PED_RESET_FLAG(pedBuddy, PRF_DisableFriendlyGunReactAudio, TRUE)
		
		VECTOR vDest[5]
		vDest[0] = <<-505.4764, 5305.9390, 79.2676>> //-111.4, wall to left, high
		vDest[1] = <<-502.2627, 5288.6313, 79.6100>> //159.7959, wall to right, high
		vDest[2] = <<-515.8207, 5274.3574, 79.2366>> //154.3, wall to both, low
		vDest[3] = <<-547.1495, 5262.1836, 72.9514>> //70.7, wall to right, high
		vDest[4] = <<-573.9721, 5258.0229, 69.4643>> //no cover
		
		VECTOR vAltDest[5]
		vAltDest[0] = <<-506.0377, 5301.8140, 79.2758>> //-113.9, wall to both, low
		vAltDest[1] = <<-496.8887, 5287.0449, 79.6100>> //158.4, wall to both, low
		vAltDest[2] = <<-502.4147, 5270.4785, 79.6100>> //158.1, wall to both, low
		vAltDest[3] = <<-544.3444, 5269.4688, 73.1745>> //72.4, wall to right, high
		vAltDest[4] = <<-573.9721, 5258.0229, 69.4643>> //Same as regular dest, it's here to avoid complications with the closest node calculation below.
		
		//If the player switched away from this ped then we need to recalculate where they should be on the route.
		IF sBuddyData.bSwitchedFromThisPed
			INT i = 0
			INT iClosestDest = 0
			FLOAT fClosestDist = -1.0
			FLOAT fDist
			
			REPEAT COUNT_OF(vDest) i
				fDist = VDIST2(vBuddyPos, vDest[i])
				
				IF fClosestDist = -1.0
				OR fDist < fClosestDist
					iClosestDest = i
					fClosestDist = fDist
				ENDIF
			ENDREPEAT
			
			//Check if this destination would put the ped into a group of enemies. If so, take the previous destination as the closest.
			IF iClosestDest > 0
				IF ARE_ENEMIES_IN_BETWEEN_COORDS(pedBuddy, vDest[iClosestDest - 1], vDest[iClosestDest])
					iClosestDest = iClosestDest - 1
				ENDIF
			ENDIF
			
			IF iClosestDest = 0
				sBuddyData.iEvent = 0
			ELIF iClosestDest = 1
				sBuddyData.iEvent = 1
			ELIF iClosestDest = 2
				sBuddyData.iEvent = 2
			ELSE
				sBuddyData.iEvent = 3
			ENDIF
			
			//Warp the player if we're currently switching and they're a long distance away from where they're meant to be.
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fDistToDest = VDIST2(vDest[iClosestDest], vPlayerPos)
			FLOAT fDistToAltDest = VDIST2(vAltDest[iClosestDest], vPlayerPos)
			
			IF fDistToDest < fDistToAltDest
				IF fDistToDest > 400.0
					IF IS_SELECTOR_CAM_ACTIVE()
						SET_ENTITY_COORDS(pedBuddy, vDest[iClosestDest])
					ENDIF
				ENDIF
				
				sBuddyData.vDest = vDest[iClosestDest]
				sBuddyData.bIsUsingSecondaryCover = FALSE
			ELSE
				IF fDistToAltDest > 400.0
					IF IS_SELECTOR_CAM_ACTIVE()
						SET_ENTITY_COORDS(pedBuddy, vAltDest[iClosestDest])
					ENDIF
				ENDIF
				
				sBuddyData.vDest = vAltDest[iClosestDest]
				sBuddyData.bIsUsingSecondaryCover = TRUE
			ENDIF
			
			sBuddyData.iTimer = 0
			sBuddyData.bRefreshTasks = TRUE
			sBuddyData.bSwitchedFromThisPed = FALSE
		ENDIF
		
		IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, <<0.0, 0.0, 0.0>>)
			sBuddyData.vDest = vDest[0]
		ENDIF

		SWITCH sBuddyData.iEvent
			CASE 0 //Fight at the rock outside the entrance until it's safe to enter.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[0]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[0]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))		
					IF NOT ARE_VECTORS_ALMOST_EQUAL(vDest[0], sBuddyData.vDest)
					AND NOT ARE_VECTORS_ALMOST_EQUAL(vAltDest[0], sBuddyData.vDest)
						sBuddyData.vDest = vDest[0]
					ENDIF
					
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[0])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -111.4, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, -113.9, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF
				
					OPEN_SEQUENCE_TASK(seq)
						//IF fDistToDestination > 4.0
						//AND NOT bPlayerStoleCover
						//	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						//	TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-493.3602, 5294.6758, 81.5900>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
						//														  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						//ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[1])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar) = 0
						sBuddyData.vDest = vDest[1]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 1 
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 2.0, 2.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[1]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[1]						
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[1])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 158.4, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 158.4, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)							
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, sBuddyData.vDest, <<-516.1574, 5247.1709, 80.3439>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(pedBuddy)
					IF IS_ENTITY_IN_ANGLED_AREA(pedBuddy, <<-511.059113,5255.000488,79.360039>>, <<-496.357666,5296.456543,84.412308>>, 14.250000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar2) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), pedBuddy, <<-516.1574, 5247.1709, 80.3439>>)
								SET_PED_SHOOTS_AT_COORD(pedBuddy, <<-516.1574, 5247.1709, 80.3439>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Play some dialogue for the first run, encouraging the player to go towards Michael.
				IF NOT bHasTextLabelTriggered[LM2_OUT]
				AND eCurrentPlayer != SELECTOR_PED_MICHAEL
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND NOT IS_SELECTOR_CAM_ACTIVE()
						IF VDIST2(vBuddyPos, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 625.0
							IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_OUT2", CONV_PRIORITY_MEDIUM)
									bHasTextLabelTriggered[LM2_OUT] = TRUE
									iTimePlayedGotoMichaelDialogue = GET_GAME_TIMER()
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_OUT", CONV_PRIORITY_MEDIUM)
									bHasTextLabelTriggered[LM2_OUT] = TRUE
									iTimePlayedGotoMichaelDialogue = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[2])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar2) = 0
						sBuddyData.vDest = vDest[2]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 2 
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[2]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[2]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[2])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 154.3, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 158.1, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
					ENDIF
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-516.1574, 5247.1709, 80.3439>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(pedBuddy)
					IF IS_ENTITY_IN_ANGLED_AREA(pedBuddy, <<-511.059113,5255.000488,79.360039>>, <<-496.357666,5296.456543,84.412308>>, 14.250000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueTimber) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), pedBuddy, <<-516.1574, 5247.1709, 80.3439>>)
								SET_PED_SHOOTS_AT_COORD(pedBuddy, <<-516.1574, 5247.1709, 80.3439>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If the player is Michael at this point play a line.
				IF eCurrentPlayer = SELECTOR_PED_MICHAEL
				AND NOT bHasTextLabelTriggered[LM2_Snipe3A]
					IF fDistToDestination > 25.0
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND NOT IS_SELECTOR_CAM_ACTIVE()
					AND GET_GAME_TIMER() - iTimeLastPedKilledByPlayer < 10000
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_Snipe3", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[LM2_Snipe3A] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF fDistToDestination < 25.0
					IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(pedBuddy, vDest[3])
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueTimber) = 0
						sBuddyData.vDest = vDest[3]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 3 //Run down to the conveyor belt area.
				IF NOT sBuddyData.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), pedBuddy, sBuddyData.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sBuddyData.bIsUsingSecondaryCover
							sBuddyData.vDest = vDest[3]
							sBuddyData.bIsUsingSecondaryCover = FALSE
						ELSE
							sBuddyData.vDest = vAltDest[3]
							sBuddyData.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					//Setup the cover point if needed.
					IF ARE_VECTORS_ALMOST_EQUAL(sBuddyData.vDest, vDest[3])
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 70.7, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ELSE
						SET_PED_COVER_POINT_AND_SPHERE(pedBuddy, sBuddyData.cover, sBuddyData.vDest, 72.4, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
					ENDIF
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							
							IF VDIST2(vBuddyPos, sBuddyData.vDest) > VDIST2(<<-519.4547, 5250.6426, 78.7824>>, sBuddyData.vDest)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-519.4547, 5250.6426, 78.7824>>, PEDMOVE_RUN, 60000, 0.5, ENAV_NO_STOPPING)
							ENDIF
							
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-574.0313, 5258.5898, 71.0182>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(pedBuddy)
					IF IS_ENTITY_IN_ANGLED_AREA(pedBuddy, <<-527.860657,5247.066406,79.939369>>, <<-556.417664,5258.927734,69.149582>>, 14.500000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueExit) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), pedBuddy, <<-574.0313, 5258.5898, 71.0182>>)
								SET_PED_SHOOTS_AT_COORD(pedBuddy, <<-574.0313, 5258.5898, 71.0182>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If the player is Michael at this point play a line.
				IF eCurrentPlayer = SELECTOR_PED_MICHAEL
				AND NOT bHasTextLabelTriggered[LM2_Snipe3B]
					IF fDistToDestination > 25.0
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND NOT IS_SELECTOR_CAM_ACTIVE()
					AND NOT bMichaelIsGettingFlanked
					AND GET_GAME_TIMER() - iTimeLastPedKilledByPlayer < 10000
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_Snipe3", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[LM2_Snipe3B] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF fDistToDestination < 25.0
					IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueExit) = 0
						sBuddyData.vDest = vDest[4]
						SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
						sBuddyData.iTimer = 0
						sBuddyData.iEvent++
						sBuddyData.bRefreshTasks = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 4 //Final enemies are dead, run to the exit.
				fDistToDestination = VDIST2(vBuddyPos, sBuddyData.vDest)
				
				IF sBuddyData.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(pedBuddy, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					SET_PED_SPHERE_DEFENSIVE_AREA(pedBuddy, sBuddyData.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sBuddyData.vDest, <<-587.1, 5281.6, 71.0>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBuddy, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sBuddyData.bRefreshTasks = FALSE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//Last resort behaviour: put the buddy with the player and tell them to follow.
PROC UPDATE_BUDDY_PED_IN_PLAYERS_GROUP()

ENDPROC

PROC RESET_BUDDY_ROUTES_FOR_SHOOTOUT_START()
	IF GET_PLAYER_PED_MODEL(CHAR_FRANKLIN) = GET_ENTITY_MODEL(PLAYER_PED_ID())
		eCurrentPlayer = SELECTOR_PED_FRANKLIN
	ELIF GET_PLAYER_PED_MODEL(CHAR_TREVOR) = GET_ENTITY_MODEL(PLAYER_PED_ID())
		eCurrentPlayer = SELECTOR_PED_TREVOR
	ELSE
		eCurrentPlayer = SELECTOR_PED_MICHAEL
	ENDIF
	
	eMichaelRoute = GET_CLOSEST_ROUTE_TO_PED(GET_PED_INDEX(CHAR_MICHAEL))
	eFranklinRoute = GET_CLOSEST_ROUTE_TO_PED(GET_PED_INDEX(CHAR_FRANKLIN))
	eTrevorRoute = GET_CLOSEST_ROUTE_TO_PED(GET_PED_INDEX(CHAR_TREVOR))
	ePrevFranklinRoute = eFranklinRoute
	ePrevTrevorRoute = eTrevorRoute
	iFranklinInDangerTimer = GET_GAME_TIMER()
	iTrevorInDangerTimer = GET_GAME_TIMER()
	iBuddyUpdateDialogueTimer = GET_GAME_TIMER() + 25000
	
	IF eCurrentPlayer != SELECTOR_PED_FRANKLIN
		sFranklin.bSwitchedFromThisPed = TRUE //This will ensure their initial destination is calculated.
	ENDIF
	
	IF eCurrentPlayer != SELECTOR_PED_TREVOR
		sTrevor.bSwitchedFromThisPed = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Kills off buddies if the player launches explosive weapons at them (e.g. grenades). This has to be handled in script due to the
///    high health of the buddies.
PROC HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(PED_INDEX &pedBuddy, INT &iBuddyHealthTracker)
	IF NOT IS_PED_INJURED(pedBuddy)
		BOOL bSafeToGrabHealth = TRUE
	
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBuddy, PLAYER_PED_ID(), TRUE)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedBuddy)
			
			IF IS_PED_RAGDOLL(pedBuddy)
				IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedBuddy), 5.0)
					SET_RAGDOLL_BLOCKING_FLAGS(pedBuddy, RBF_NONE)
					SET_ENTITY_HEALTH(pedBuddy, 0)
					bSafeToGrabHealth = FALSE
				ELIF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					//If the buddy was smashed into by the player's car then kill them.
					IF iBuddyHealthTracker - GET_ENTITY_HEALTH(pedBuddy) > 100
						SET_RAGDOLL_BLOCKING_FLAGS(pedBuddy, RBF_NONE)
						SET_ENTITY_HEALTH(pedBuddy, 0)
						bSafeToGrabHealth = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bSafeToGrabHealth
			iBuddyHealthTracker = GET_ENTITY_HEALTH(pedBuddy)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates buddies during the shootout: keeps them progressing on the closest route.
/// PARAMS:
///    bJustPerformedASwitch - If the player just switched then the routes for each buddy need to be refreshed in case the player moved them elsewhere before switching.
PROC UPDATE_PLAYER_BUDDIES_DURING_SHOOTOUT()
	PED_INDEX pedsNearby[8]
	INT i = 0

	//Michael's route: he should always be sniping but just in case check the other routes.
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		SET_PED_RESET_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PRF_DisableFriendlyGunReactAudio, TRUE)
	
		IF eMichaelRoute = BUDDY_ROUTE_SNIPING
		
		ENDIF
	
		//Only update Michael if the player isn't switching to him, as during the switch to Michael he is given custom aim tasks.
		IF NOT IS_SELECTOR_CAM_ACTIVE()
		OR (IS_SELECTOR_CAM_ACTIVE() AND sSelectorPeds.eNewSelectorPed != SELECTOR_PED_MICHAEL)
			UPDATE_BUDDY_PED_SNIPE_PATH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sMichael)
		ENDIF
		
		//Kill Michael if the player does anything to them.
		IF IS_PED_RAGDOLL(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), 5.0)
				SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 0)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		SET_PED_RESET_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PRF_DisableFriendlyGunReactAudio, TRUE)
	
		IF eMissionStage <= STAGE_ATTACK_SAWMILL
			IF eFranklinRoute = BUDDY_ROUTE_BULLDOZER
				UPDATE_BUDDY_PED_BULLDOZER_PATH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sFranklin)
			ELIF eFranklinRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
				UPDATE_BUDDY_PED_BULLDOZER_ON_FOOT_PATH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sFranklin)
			ELIF eFranklinRoute = BUDDY_ROUTE_FRONT_ASSAULT
				UPDATE_BUDDY_PED_FRONT_PATH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sFranklin)
			ELIF eFranklinRoute = BUDDY_ROUTE_REAR
				UPDATE_BUDDY_PED_REAR_PATH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sFranklin)
			ENDIF
			
			//If Franklin becomes stuck on a route then flash them red for help. Becoming stuck depends on the following:
			// - The ped's event index for this route hasn't increased for a long time.
			// - There are enemies near to the ped.
			VECTOR vFranklinPos = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			
			//If the ped is currently performing a custom task then reset the danger timer.
			IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
			AND NOT IS_PED_IN_COMBAT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				iFranklinInDangerTimer = GET_GAME_TIMER()
			ENDIF
			
			//If the ped is moving to their destination then reset the danger timer.
			IF VDIST2(vFranklinPos, sFranklin.vDest) > 9.0
			AND GET_ENTITY_SPEED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) > 0.3
				iFranklinInDangerTimer = GET_GAME_TIMER()
			ENDIF
			
			//If the player kills an enemy near to the buddy then reset the danger timer.
			IF GET_GAME_TIMER() - iTimeLastPedKilledByPlayer < 500
			AND VDIST2(vFranklinPos, vPosOfLastPlayerKill) < 2500.0
				iFranklinInDangerTimer = GET_GAME_TIMER()
			ENDIF
			
			//If the enemies haven't been triggered yet then reset the danger timer.
			IF NOT bEnemiesHaveBeenAlerted
				iFranklinInDangerTimer = GET_GAME_TIMER()
			ENDIF
			
			//If the buddy changes route then update their previous max event index as it won't be valid for the new route.
			IF ePrevFranklinRoute != eFranklinRoute
				iFranklinInDangerTimer = GET_GAME_TIMER()
				sFranklin.iPrevMaxEvent = 0
				ePrevFranklinRoute = eFranklinRoute
			ENDIF
			
			//If the buddy progresses a stage in their current route then reset the danger timer.
			IF sFranklin.iEvent > sFranklin.iPrevMaxEvent
				iFranklinInDangerTimer = GET_GAME_TIMER()
				sFranklin.iPrevMaxEvent = sFranklin.iEvent
			ENDIF
			
			//If Franklin and Trevor are both on the same route then they're not considered in danger.
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF VDIST2(vFranklinPos, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 400.0
					iFranklinInDangerTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
			
			IF GET_GAME_TIMER() - iFranklinInDangerTimer > 20000
				INT iNumEnemiesNearFranklin = 0
				
				GET_PED_NEARBY_PEDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], pedsNearby)
				
				REPEAT COUNT_OF(pedsNearby) i
					IF NOT IS_PED_INJURED(pedsNearby[i])
					AND GET_PED_RELATIONSHIP_GROUP_HASH(pedsNearby[i]) = relGroupBallas
						IF VDIST2(vFranklinPos, GET_ENTITY_COORDS(pedsNearby[i])) < 2500.0
							iNumEnemiesNearFranklin++
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF iNumEnemiesNearFranklin > 1
					SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_FRANKLIN, TRUE)
					SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_FRANKLIN, SELECTOR_PED_TREVOR, SELECTOR_PED_MICHAEL)
					
					IF GET_GAME_TIMER() - iFranklinDangerDialogueTimer > 0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						AND NOT IS_SELECTOR_CAM_ACTIVE()
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_FRANHURT", CONV_PRIORITY_MEDIUM)
								iFranklinDangerDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000, 10000)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iFranklinInDangerTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
			ENDIF
		ELSE
			UPDATE_BUDDY_PED_AFTER_SAVING_LAMAR(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sFranklin)
		ENDIF
		
		HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sFranklin.iHealth)
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		SET_PED_RESET_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PRF_DisableFriendlyGunReactAudio, TRUE)
	
		IF eMissionStage <= STAGE_ATTACK_SAWMILL
			IF eTrevorRoute = BUDDY_ROUTE_BULLDOZER
				UPDATE_BUDDY_PED_BULLDOZER_PATH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sTrevor)
			ELIF eTrevorRoute = BUDDY_ROUTE_BULLDOZER_ON_FOOT
				UPDATE_BUDDY_PED_BULLDOZER_ON_FOOT_PATH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sTrevor)
			ELIF eTrevorRoute = BUDDY_ROUTE_FRONT_ASSAULT
				UPDATE_BUDDY_PED_FRONT_PATH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sTrevor)
			ELIF eTrevorRoute = BUDDY_ROUTE_REAR
				UPDATE_BUDDY_PED_REAR_PATH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sTrevor)
			ENDIF
			
			//If the ped is currently performing a custom task then reset the danger timer.
			VECTOR vTrevorPos = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			
			IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
			AND NOT IS_PED_IN_COMBAT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				iTrevorInDangerTimer = GET_GAME_TIMER()
			ENDIF
			
			//If the player kills an enemy near to the buddy then reset the danger timer.
			IF GET_GAME_TIMER() - iTimeLastPedKilledByPlayer < 500
			AND VDIST2(vTrevorPos, vPosOfLastPlayerKill) < 2500.0
				iTrevorInDangerTimer = GET_GAME_TIMER()
			ENDIF
			
			//If the ped is moving to their destination then reset the danger timer.
			IF VDIST2(vTrevorPos, sTrevor.vDest) > 9.0
			AND GET_ENTITY_SPEED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) > 0.3
				iTrevorInDangerTimer = GET_GAME_TIMER()
			ENDIF
			
			//If the enemies haven't been alerted yet then reset the danger time.
			IF NOT bEnemiesHaveBeenAlerted
				iTrevorInDangerTimer = GET_GAME_TIMER()
			ENDIF
			
			//If the buddy changes route then update their previous max event index as it won't be valid for the new route.
			IF ePrevTrevorRoute != eTrevorRoute
				iTrevorInDangerTimer = GET_GAME_TIMER()
				sTrevor.iPrevMaxEvent = 0
				ePrevTrevorRoute = eTrevorRoute
			ENDIF
			
			//If the buddy progresses a stage in their current route then reset the danger timer.
			IF sTrevor.iEvent > sTrevor.iPrevMaxEvent
				iTrevorInDangerTimer = GET_GAME_TIMER()
				sTrevor.iPrevMaxEvent = sTrevor.iEvent
			ENDIF
			
			//If Franklin and Trevor are both on the same route then they're not considered in danger.
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF VDIST2(vTrevorPos, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])) < 400.0
					iTrevorInDangerTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
			
			IF GET_GAME_TIMER() - iTrevorInDangerTimer > 20000
				INT iNumEnemiesNearTrevor = 0
				
				GET_PED_NEARBY_PEDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], pedsNearby)
				
				REPEAT COUNT_OF(pedsNearby) i
					IF NOT IS_PED_INJURED(pedsNearby[i])
					AND GET_PED_RELATIONSHIP_GROUP_HASH(pedsNearby[i]) = relGroupBallas
						IF VDIST2(vTrevorPos, GET_ENTITY_COORDS(pedsNearby[i])) < 2500.0
							iNumEnemiesNearTrevor++
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF iNumEnemiesNearTrevor > 1
					SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
					SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_PED_FRANKLIN, SELECTOR_PED_MICHAEL)
					
					IF GET_GAME_TIMER() - iTrevorDangerDialogueTimer > 0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						AND NOT IS_SELECTOR_CAM_ACTIVE()
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_TREVHURT", CONV_PRIORITY_MEDIUM)
								iTrevorDangerDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000, 10000)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iTrevorInDangerTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
			ENDIF
		ELSE
			UPDATE_BUDDY_PED_AFTER_SAVING_LAMAR(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sTrevor)
		ENDIF
		
		HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sTrevor.iHealth)
	ENDIF
ENDPROC

PROC UPDATE_SHOOTOUT_SWITCH()
	VECTOR vPlayerPos
	INT iClosestEnemy = -1
	INT i = 0
	BOOL bAllowSwitch = TRUE
	PED_INDEX peds[16]

	IF eCurrentPlayer = SELECTOR_PED_MICHAEL
		IF NOT bMichaelSetToMoveFreely
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ENDIF
	ENDIF

	SWITCH iSwitchStage 
	    CASE 0
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
			//Don't allow switching if the player is flying.
			IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				bAllowSwitch = FALSE
			ENDIF
			
			//Block switching if it's not safe to switch (e.g. the ped is on fire).
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF NOT sFranklin.bSwitchBlocked
					IF IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_FRANKLIN, TRUE)
						sFranklin.bSwitchBlocked = TRUE
					ENDIF
				ELSE
					IF NOT IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
						sFranklin.bSwitchBlocked = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			//Block switching if it's not safe to switch (e.g. the ped is on fire).
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF NOT sTrevor.bSwitchBlocked
					IF IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
						sTrevor.bSwitchBlocked = TRUE
					ENDIF
				ELSE
					IF NOT IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
						sTrevor.bSwitchBlocked = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			//Block switching if it's not safe to switch (e.g. the ped is on fire). Don't allow switching to Michael if he isn't in position yet.
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF NOT sMichael.bSwitchBlocked
					IF IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					OR VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), vMichaelSnipePos) > 16.0
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
						sMichael.bSwitchBlocked = TRUE
					ENDIF
				ELSE
					IF NOT IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					AND VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), vMichaelSnipePos) < 16.0
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
						sMichael.bSwitchBlocked = FALSE
					ENDIF
				ENDIF
			ENDIF
		
			IF bAllowSwitch
				IF UPDATE_SELECTOR_HUD(sSelectorPeds)
		            CLEAR_HELP()
					DESTROY_ALL_CAMS()

					INFORM_MISSION_STATS_OF_INCREMENT(FRA2_SWITCHES)

					sSelectorCam.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
					iHealthBeforeSwitch = GET_ENTITY_HEALTH(PLAYER_PED_ID())

					IF sSelectorPeds.eNewSelectorPed = SELECTOR_PED_TREVOR
						IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
							START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
						ENDIF
					ELSE
						IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
							STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
						ENDIF
					ENDIF
					
					IF sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MICHAEL
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							GET_PED_NEARBY_PEDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], peds)
							
							REPEAT COUNT_OF(peds) i
								IF iClosestEnemy = -1
									IF NOT IS_PED_INJURED(peds[i])
										IF GET_PED_RELATIONSHIP_GROUP_HASH(peds[i]) = relGroupBallas
											iClosestEnemy = i
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
						
							//Set Michael to aim at the closest enemy ped (or the player if there are no enemies).
							IF bMichaelHUDFlashingForSniper
							AND NOT IS_PED_INJURED(sBallasSniper[0].ped)
								vMichaelSwitchAimPos = GET_ENTITY_COORDS(sBallasSniper[0].ped, FALSE) + <<-1.0, 0.0, 3.0>>
								TASK_AIM_GUN_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vMichaelSwitchAimPos, -1, TRUE)
							ELIF bMichaelHUDFlashingForRocket
							AND NOT IS_PED_INJURED(sBallasRocket[0].ped)
								vMichaelSwitchAimPos = GET_ENTITY_COORDS(sBallasRocket[0].ped, FALSE) + <<1.0, 0.0, -3.0>>
								TASK_AIM_GUN_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vMichaelSwitchAimPos, -1, TRUE)
							ELSE
								IF iClosestEnemy = -1
									vMichaelSwitchAimPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
								ELSE
									vMichaelSwitchAimPos = GET_ENTITY_COORDS(peds[iClosestEnemy], FALSE)
								ENDIF
								
								TASK_AIM_GUN_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vMichaelSwitchAimPos, -1, TRUE)
								
								#IF IS_DEBUG_BUILD
									PRINTLN("Franklin2.sc - Michael set to aim at ", vMichaelSwitchAimPos)
								#ENDIF
							ENDIF
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
						ENDIF
					
						IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
							START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
						ENDIF
					ELSE
						IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
							STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
						ENDIF
					ENDIF
					
					bPlayerIsInShootoutAreaForSwitch = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-489.725586,5408.617676,49.595184>>, <<-570.134766,5210.887207,122.609947>>, 188.750000)
					
					IF bPlayerIsInShootoutAreaForSwitch
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed])	
							IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
								VECTOR vRot
								vRot = <<0.0, 0.0, GET_ENTITY_HEADING(sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed])>>
								NEW_LOAD_SCENE_START(GET_ENTITY_COORDS(sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]), CONVERT_ROTATION_TO_DIRECTION_VECTOR(vRot), 50.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
							ENDIF
						ENDIF
					ENDIF			

					iSwitchStage++
		        ENDIF
			ENDIF
			
			
			
			//Guarantee that the current player's cover info is removed from when they were AI controlled.
			IF GET_GAME_TIMER() - iTimeOfLastSwitch > 1000
				IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
					IF sFranklin.cover != NULL
						IF VDIST2(sFranklin.vDest, vPlayerPos) > 9.0
							REMOVE_COVER_POINT(sFranklin.cover)
							sFranklin.vDest = <<0.0, 0.0, 0.0>>
						ENDIF
					ENDIF
				ELIF eCurrentPlayer = SELECTOR_PED_MICHAEL
					IF sMichael.cover != NULL
						IF VDIST2(sMichael.vDest, vPlayerPos) > 9.0
							REMOVE_COVER_POINT(sMichael.cover)
							sMichael.vDest = <<0.0, 0.0, 0.0>>
						ENDIF
					ENDIF
				ELSE
					IF sTrevor.cover != NULL
						IF VDIST2(sTrevor.vDest, vPlayerPos) > 9.0
							REMOVE_COVER_POINT(sTrevor.cover)
							sTrevor.vDest = <<0.0, 0.0, 0.0>>
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	    BREAK 
	    
	    CASE 1	  			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
		
			IF eCurrentPlayer = SELECTOR_PED_MICHAEL
			AND bMichaelSetToMoveFreely 
				
			ENDIF
		
			IF (bPlayerIsInShootoutAreaForSwitch AND NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sSelectorCam))
			OR (NOT bPlayerIsInShootoutAreaForSwitch AND NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sSelectorCam))
				IF eCurrentPlayer = SELECTOR_PED_MICHAEL
					IF bMichaelSetToMoveFreely
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
						SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
					ELSE
						IF NOT ARE_VECTORS_ALMOST_EQUAL(vMichaelSwitchAimPos, <<0.0, 0.0, 0.0>>)
							FLOAT fPitch
							fPitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH_FOR_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMichaelSwitchAimPos)
						
							SET_FIRST_PERSON_AIM_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(fPitch - 0.5, fPitch + 0.5)
						ENDIF
						
						SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
						SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), FALSE)
					ENDIF
				
					SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), FALSE)
				ENDIF
			
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
			
				iTimeOfLastSwitch = GET_GAME_TIMER()
				iSwitchStage = 0
			ELSE			
				IF sSelectorCam.bOKToSwitchPed
					IF NOT sSelectorCam.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)		
							vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
								IF iHealthBeforeSwitch < 110
									iHealthBeforeSwitch = 110
								ENDIF
							
						        SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)     
						        SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)
						        SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], RELGROUPHASH_PLAYER)
								
								SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 5, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
								SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_BLIND_FIRE_IN_COVER, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_DISABLE_PINNED_DOWN, TRUE)
								SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_UseKinematicModeWhenStationary, TRUE)
								SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_DisableHurt, TRUE)
								SET_ENTITY_PROOFS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE, TRUE, FALSE, FALSE, FALSE)
								SET_ENTITY_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], iHealthBeforeSwitch)
								SET_PED_MAX_HEALTH_WITH_SCALE(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
								SET_PED_USING_ACTION_MODE(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE, -1)
								SET_RAGDOLL_BLOCKING_FLAGS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 
														   RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_FALLING | RBF_MELEE | RBF_IMPACT_OBJECT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_IMPACT)
								SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
								
								IF eMissionStage = STAGE_ATTACK_SAWMILL
								OR eMissionStage = STAGE_GET_LAMAR_OUT
									//Reset the defensive area for each ped, this ensures they update their destination to match the latest state of the buddies.
									IF sSelectorPeds.ePreviousSelectorPed = SELECTOR_PED_MICHAEL
										eMichaelRoute = GET_CLOSEST_ROUTE_TO_PED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
										sMichael.bSwitchedFromThisPed = TRUE
									ELIF sSelectorPeds.ePreviousSelectorPed = SELECTOR_PED_FRANKLIN
										eFranklinRoute = GET_CLOSEST_ROUTE_TO_PED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
										sFranklin.bSwitchedFromThisPed = TRUE
										
										//If Franklin would be considered in danger after switching then reset the danger timer.
										IF GET_GAME_TIMER() - iFranklinInDangerTimer > 17000
											iFranklinInDangerTimer = GET_GAME_TIMER() - 17000
										ENDIF
									ELIF sSelectorPeds.ePreviousSelectorPed = SELECTOR_PED_TREVOR
										eTrevorRoute = GET_CLOSEST_ROUTE_TO_PED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
										sTrevor.bSwitchedFromThisPed = TRUE
										
										//If Trevor would be considered in danger after switching then reset the danger timer.
										IF GET_GAME_TIMER() - iTrevorInDangerTimer > 17000
											iTrevorInDangerTimer = GET_GAME_TIMER() - 17000
										ENDIF
									ENDIF
								ENDIF
								
								
								//Don't allow AI peds to use certain weapons.
								IF sSelectorPeds.ePreviousSelectorPed != SELECTOR_PED_MICHAEL
									//GET_WEAPONTYPE_GROUP
								
									IF HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], WEAPONTYPE_CARBINERIFLE)
										SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], WEAPONTYPE_CARBINERIFLE)
									
										IF GET_AMMO_IN_PED_WEAPON(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], WEAPONTYPE_CARBINERIFLE) < 50
											ADD_AMMO_TO_PED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], WEAPONTYPE_CARBINERIFLE, 50)
										ENDIF
									ENDIF
								ENDIF
						    ENDIF
							
							SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
							SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
							CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_FALLING | RBF_MELEE | RBF_IMPACT_OBJECT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_IMPACT)
							
							REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
							REFRESH_BUDDY_PED_AUDIO_MIX_GROUP()
							
							eCurrentPlayer = sSelectorPeds.eCurrentSelectorPed
							
							IF eCurrentPlayer = SELECTOR_PED_MICHAEL	
								sMichael.vDest = <<0.0, 0.0, 0.0>>
							ELSE
								SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
								SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
							
								//If the new player ped is near their destination then try to get them into cover there.
								VECTOR vAICoverPos
								FLOAT fBuddyDistFromCover, fPlayerDistFromCover
								
								IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
									vAICoverPos = sFranklin.vDest
								ELIF eCurrentPlayer = SELECTOR_PED_TREVOR
									vAICoverPos = sTrevor.vDest
								ENDIF
								
								IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(vAICoverPos)
									fBuddyDistFromCover = VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed]), vAICoverPos)
									fPlayerDistFromCover = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vAICoverPos)
									
									IF fPlayerDistFromCover < fBuddyDistFromCover
									AND fBuddyDistFromCover > 100.0
									AND fPlayerDistFromCover < 4.0
										TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), vAICoverPos, 1500, FALSE, 0.0, FALSE, FALSE, NULL, TRUE)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									ENDIF
								ENDIF
							
								IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
									sFranklin.vDest = <<0.0, 0.0, 0.0>>
								ELIF eCurrentPlayer = SELECTOR_PED_TREVOR
									sTrevor.vDest = <<0.0, 0.0, 0.0>>
								ENDIF
							ENDIF
							
							sSelectorCam.bPedSwitched = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	    BREAK 
	ENDSWITCH 
ENDPROC

PROC UPDATE_BUDDY_BLIPS_DURING_SHOOTOUT()
	//Update buddy blips: remove the blip for whoever is the player, add blips for other buddies.
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF NOT DOES_BLIP_EXIST(sFranklin.blip)
			sFranklin.blip = CREATE_BLIP_FOR_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(sFranklin.blip)
			REMOVE_BLIP(sFranklin.blip)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF NOT DOES_BLIP_EXIST(sMichael.blip)
			sMichael.blip = CREATE_BLIP_FOR_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(sMichael.blip)
			REMOVE_BLIP(sMichael.blip)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF NOT DOES_BLIP_EXIST(sTrevor.blip)
			sTrevor.blip = CREATE_BLIP_FOR_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(sTrevor.blip)
			REMOVE_BLIP(sTrevor.blip)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_LAMAR_DURING_SHOOTOUT()
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	PED_INDEX pedsNearby[5]
	INT i = 0

	IF NOT DOES_ENTITY_EXIST(sLamar.ped)
		IF VDIST2(vPlayerPos, vLamarShootoutStartPos) < 40000.0
		AND sBallasAfterAlert[0].bIsCreated
			SETUP_REQ_LAMAR(vLamarShootoutStartPos, fLamarShootoutStartHeading)
		ENDIF
	ELIF NOT IS_PED_INJURED(sLamar.ped)
		VECTOR vBuddyPos = GET_ENTITY_COORDS(sLamar.ped)
		BOOL bPlayerStoleCover
		BOOL bIsInCombat = (IS_PED_IN_COMBAT(sLamar.ped) OR GET_SCRIPT_TASK_STATUS(sLamar.ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK)
		FLOAT fDistToDestination
		SEQUENCE_INDEX seq
		VECTOR vOtherBuddysCurrentDestination
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			vOtherBuddysCurrentDestination = sFranklin.vDest
		ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			vOtherBuddysCurrentDestination = sTrevor.vDest
		ENDIF
		
		VECTOR vDest[6]
		vDest[0] = <<-523.0930, 5306.6973, 79.2448>>
		vDest[1] = <<-498.3445, 5300.7861, 79.6049>>
		vDest[2] = <<-498.9495, 5281.7505, 79.6100>>
		vDest[3] = <<-516.5259, 5258.8921, 79.5591>>
		vDest[4] = <<-532.4301, 5261.0220, 73.3084>>
		vDest[5] = <<-577.0162, 5256.7354, 69.4628>>
		
		VECTOR vAltDest[6]
		vAltDest[0] = <<-519.9658, 5307.7275, 79.2448>>
		vAltDest[1] = <<-498.4532, 5303.5000, 79.5833>>
		vAltDest[2] = <<-504.1630, 5282.2646, 79.6100>>
		vAltDest[3] = <<-517.9027, 5256.0908, 79.5374>>
		vAltDest[4] = <<-541.1633, 5260.2632, 73.2855>>
		vAltDest[5] = <<-577.0162, 5256.7354, 69.4628>>
		
	
		REQUEST_ANIM_DICT(strLamarInjuredAnims)
	
		SET_PED_RESET_FLAG(sLamar.ped, PRF_DisableFriendlyGunReactAudio, TRUE)
	
		SWITCH sLamar.iEvent
			CASE 0 //Set up Lamar sitting and waiting.
				IF HAS_ANIM_DICT_LOADED(strLamarInjuredAnims)
					SET_PED_CONFIG_FLAG(sLamar.ped, PCF_RunFromFiresAndExplosions, FALSE)
					SET_PED_CONFIG_FLAG(sLamar.ped, PCF_DisableExplosionReactions, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(sLamar.ped, relGroupNeutral)
					SET_PED_CAN_BE_TARGETTED(sLamar.ped, FALSE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sLamar.ped, TRUE)
					SET_PED_SUFFERS_CRITICAL_HITS(sLamar.ped, FALSE)
					SET_CURRENT_PED_WEAPON(sLamar.ped, WEAPONTYPE_UNARMED, TRUE)
					SET_RAGDOLL_BLOCKING_FLAGS(sLamar.ped, RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_FALLING | RBF_IMPACT_OBJECT)
					
					TASK_PLAY_ANIM(sLamar.ped, strLamarInjuredAnims, "lamar_base_idle", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sLamar.ped, TRUE)
					
					sLamar.iEvent++
				ENDIF
			BREAK
			
			CASE 1 //Progress once the player reaches him.
				SET_PED_CAPSULE(sLamar.ped, 0.5)
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-512.474158,5304.624805,78.767578>>, <<-525.518250,5308.746582,83.017578>>, 3.500000)
				AND IS_PED_INJURED(sBallasGuardingLamar[0].ped) 
				AND sBallasGuardingLamar[0].bIsCreated
					TASK_PLAY_ANIM(sLamar.ped, strLamarInjuredAnims, "lamar_base_idle_to_standing_idle", SLOW_BLEND_IN, WALK_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sLamar.ped, TRUE)
				
					//Record footage of the player reaching Lamar.
					REPLAY_RECORD_BACK_FOR_TIME(2.0)
				
					REPOSITION_VEHICLES_FOR_LAMAR_ESCAPE()
				
					sLamar.iEvent++
				ENDIF
			BREAK
			
			CASE 2 //Once Lamar has gotten up progress.
				IF NOT bHasTextLabelTriggered[lm2_lgof]
				AND NOT bHasTextLabelTriggered[lm2_lgot]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "lm2_lgof", CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								
								bHasTextLabelTriggered[lm2_lgof] = TRUE
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "lm2_lgot", CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								
								bHasTextLabelTriggered[lm2_lgot] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bLamarHasBeenRescued = TRUE
				ENDIF
				
				SET_PED_CAPSULE(sLamar.ped, 0.5)
				
				IF (IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarInjuredAnims, "lamar_base_idle_to_standing_idle") 
				AND GET_ENTITY_ANIM_CURRENT_TIME(sLamar.ped, strLamarInjuredAnims, "lamar_base_idle_to_standing_idle") > 0.65)
				OR NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarInjuredAnims, "lamar_base_idle_to_standing_idle")
					CLEAR_PED_TASKS(sLamar.ped)
					
					sLamar.iEvent = 50
				ENDIF
			BREAK
			
			CASE 50 //Set up Lamar for combat.
				IF NOT HAS_PED_GOT_WEAPON(sLamar.ped, WEAPONTYPE_PISTOL)
					GIVE_WEAPON_TO_PED(sLamar.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE, FALSE)
				ENDIF
				
				SET_PED_RELATIONSHIP_GROUP_HASH(sLamar.ped, RELGROUPHASH_PLAYER)
				SET_PED_COMBAT_PARAMS(sLamar.ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
				SET_PED_COMBAT_ATTRIBUTES(sLamar.ped, CA_BLIND_FIRE_IN_COVER, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sLamar.ped, CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sLamar.ped, CA_DISABLE_PINNED_DOWN, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sLamar.ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)				
				SET_PED_CONFIG_FLAG(sLamar.ped, PCF_RunFromFiresAndExplosions, FALSE)
				SET_ENTITY_PROOFS(sLamar.ped, FALSE, TRUE, FALSE, FALSE, FALSE)
				SET_PED_CAN_BE_TARGETTED(sLamar.ped, FALSE)
				SET_PED_MAX_HEALTH(sLamar.ped, 1800)
				SET_ENTITY_HEALTH(sLamar.ped, 1800)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sLamar.ped, FALSE)
				SET_PED_SUFFERS_CRITICAL_HITS(sLamar.ped, FALSE)
				SET_RAGDOLL_BLOCKING_FLAGS(sLamar.ped, RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_FALLING | RBF_IMPACT_OBJECT)
				SET_PED_USING_ACTION_MODE(sLamar.ped, TRUE, -1)
				SET_PED_PATH_AVOID_FIRE(sLamar.ped, TRUE)
				
				
				sLamar.vDest = vDest[0]
				SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
				sLamar.iTimer = 0
				sLamar.iEvent++
				sLamar.bRefreshTasks = TRUE
			BREAK
			
			CASE 51 //Lamar goes to the dead body of the ped near him and picks up the gun.
				VECTOR vDeadBallaPos
				
				GET_PED_NEARBY_PEDS(sLamar.ped, pedsNearby)
					
				REPEAT COUNT_OF(pedsNearby) i
					IF DOES_ENTITY_EXIST(pedsNearby[i])
						IF GET_ENTITY_MODEL(pedsNearby[i]) = modelBalla
							IF VDIST2(vBuddyPos, GET_ENTITY_COORDS(pedsNearby[i], FALSE)) < 16.0
								vDeadBallaPos = GET_ENTITY_COORDS(pedsNearby[i], FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				WEAPON_TYPE eCurrentWeapon
				
				IF HAS_PED_GOT_WEAPON(sLamar.ped, WEAPONTYPE_PISTOL)
					GET_CURRENT_PED_WEAPON(sLamar.ped, eCurrentWeapon)
				ENDIF
				
				IF NOT ARE_VECTORS_EQUAL(vDeadBallaPos, <<0.0, 0.0, 0.0>>)
					REQUEST_ANIM_DICT(strPickupAnims)
				
					IF sLamar.iTimer = 0
						IF HAS_ANIM_DICT_LOADED(strPickupAnims)
							OPEN_SEQUENCE_TASK(seq)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDeadBallaPos, PEDMOVE_RUN, -1, DEFAULT, ENAV_NO_STOPPING)
								TASK_PLAY_ANIM(NULL, strPickupAnims, "pickup_low", WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
							
							TASK_LOOK_AT_COORD(sLamar.ped, vDeadBallaPos, -1, SLF_WHILE_NOT_IN_FOV)
							
							sLamar.iTimer = GET_GAME_TIMER()
						ENDIF
					ELIF GET_GAME_TIMER() - sLamar.iTimer > 3000
					AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strPickupAnims, "pickup_low")
						IF eCurrentWeapon != WEAPONTYPE_PISTOL
							SET_CURRENT_PED_WEAPON(sLamar.ped, WEAPONTYPE_PISTOL, TRUE)
						ENDIF
						
						TASK_CLEAR_LOOK_AT(sLamar.ped)
						REMOVE_ANIM_DICT(strPickupAnims)
						
						sLamar.iTimer = 0
						sLamar.iEvent++
						sLamar.bRefreshTasks = TRUE
					ELSE
						IF eCurrentWeapon != WEAPONTYPE_PISTOL
							IF IS_ENTITY_PLAYING_ANIM(sLamar.ped, strPickupAnims, "pickup_low")
								IF GET_ENTITY_ANIM_CURRENT_TIME(sLamar.ped, strPickupAnims, "pickup_low") > 0.381
									SET_CURRENT_PED_WEAPON(sLamar.ped, WEAPONTYPE_PISTOL, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//Couldn't find the ped so just continue.
					SET_CURRENT_PED_WEAPON(sLamar.ped, WEAPONTYPE_PISTOL, TRUE)

					REMOVE_ANIM_DICT(strPickupAnims)

					sLamar.iTimer = 0
					sLamar.iEvent++
					sLamar.bRefreshTasks = TRUE
				ENDIF
			BREAK
			
			CASE 52 //Leaving the start point and entering combat.
				//Warp Lamar if he falls too far behind the player.
				IF NOT sLamar.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), sLamar.ped, sLamar.vDest, vOtherBuddysCurrentDestination, 3.0, 2.0)
						IF sLamar.bIsUsingSecondaryCover
							sLamar.vDest = vDest[0]
							sLamar.bIsUsingSecondaryCover = FALSE
						ELSE
							sLamar.vDest = vAltDest[0]
							sLamar.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sLamar.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sLamar.vDest)
				
				IF sLamar.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(sLamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sLamar.bRefreshTasks = FALSE
				ENDIF

				//Progress once the player has gone ahead to the next enemy wave.
				IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(sLamar.ped, vDest[1])
				AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar) = 0
				AND (GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar2) < COUNT_OF(sBallasAfterLamar2)
				OR (NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_FRANKLIN)) AND IS_ENTITY_AT_COORD(GET_PED_INDEX(CHAR_FRANKLIN), <<-499.087769,5287.962402,84.859978>>,<<56.750000,9.000000,9.500000>>)))
					sLamar.vDest = vDest[1]
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
					sLamar.iTimer = 0
					sLamar.iEvent++
					sLamar.bRefreshTasks = TRUE
				ENDIF
			BREAK
			
			CASE 53 //Begins making his way across the timber area.
				//Warp Lamar if he falls too far behind the player.
				IF NOT sLamar.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), sLamar.ped, sLamar.vDest, vOtherBuddysCurrentDestination, 3.0, 2.0)
						IF sLamar.bIsUsingSecondaryCover
							sLamar.vDest = vDest[1]
							sLamar.bIsUsingSecondaryCover = FALSE
						ELSE
							sLamar.vDest = vAltDest[1]
							sLamar.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sLamar.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sLamar.vDest)
				
				IF sLamar.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(sLamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sLamar.vDest, <<-516.1574, 5247.1709, 80.3439>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sLamar.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(sLamar.ped)
					IF IS_ENTITY_IN_ANGLED_AREA(sLamar.ped, <<-511.059113,5255.000488,79.360039>>, <<-496.357666,5296.456543,84.412308>>, 14.250000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar2) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), sLamar.ped, <<-516.1574, 5247.1709, 80.3439>>)
								SET_PED_SHOOTS_AT_COORD(sLamar.ped, <<-516.1574, 5247.1709, 80.3439>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(sLamar.ped, vDest[2])
				AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar2) = 0
				AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueTimber) < COUNT_OF(sBallasPostRescueTimber)
					sLamar.vDest = vDest[2]
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
					sLamar.iTimer = 0
					sLamar.iEvent++
					sLamar.bRefreshTasks = TRUE
				ELSE
					//Play some "more guys" dialogue.
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND NOT IS_SELECTOR_CAM_ACTIVE()
						IF NOT bHasTextLabelTriggered[LM2_MORE1]
							IF sBallasAfterLamar2[0].iEvent > 0
								IF VDIST2(vPlayerPos, vBuddyPos) < 900.0
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MORE", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_MORE1] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bHasTextLabelTriggered[LM2_LSIDE]
							IF NOT IS_PED_INJURED(sBallasAfterLamar2[2].ped)
								IF VDIST2(vPlayerPos, vBuddyPos) < 900.0
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_LSIDE", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_LSIDE] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 54 //Begins making his way across the timber area.
				//Warp Lamar if he falls too far behind the player.
				IF NOT sLamar.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), sLamar.ped, sLamar.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sLamar.bIsUsingSecondaryCover
							sLamar.vDest = vDest[2]
							sLamar.bIsUsingSecondaryCover = FALSE
						ELSE
							sLamar.vDest = vAltDest[2]
							sLamar.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sLamar.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sLamar.vDest)
				
				IF sLamar.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(sLamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sLamar.vDest, <<-516.1574, 5247.1709, 80.3439>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sLamar.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(sLamar.ped)
					IF IS_ENTITY_IN_ANGLED_AREA(sLamar.ped, <<-511.059113,5255.000488,79.360039>>, <<-496.357666,5296.456543,84.412308>>, 14.250000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueTimber) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), sLamar.ped, <<-516.1574, 5247.1709, 80.3439>>)
								SET_PED_SHOOTS_AT_COORD(sLamar.ped, <<-516.1574, 5247.1709, 80.3439>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(sLamar.ped, vDest[3])
				AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueTimber) = 0
					sLamar.vDest = vDest[3]
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
					sLamar.iTimer = 0
					sLamar.iEvent++
					sLamar.bRefreshTasks = TRUE
				ELSE
					//Play some "more guys" dialogue.
					IF NOT bHasTextLabelTriggered[LM2_MORE2]
						IF sBallasPostRescueTimber[0].iEvent > 0
						AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF VDIST2(vPlayerPos, vBuddyPos) < 625.0
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MORE", CONV_PRIORITY_MEDIUM)
									bHasTextLabelTriggered[LM2_MORE2] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 55 //Runs to the start of the hill.
				IF NOT sLamar.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), sLamar.ped, sLamar.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sLamar.bIsUsingSecondaryCover
							sLamar.vDest = vDest[3]
							sLamar.bIsUsingSecondaryCover = FALSE
						ELSE
							sLamar.vDest = vAltDest[3]
							sLamar.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sLamar.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sLamar.vDest)
				
				IF sLamar.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(sLamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, sLamar.vDest, PEDMOVE_RUN, -1, 0.5, ENAV_NO_STOPPING)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sLamar.bRefreshTasks = FALSE
				ENDIF

				IF NOT ARE_ENEMIES_IN_THE_WAY_OF_DESTINATION(sLamar.ped, vDest[4])
				AND ((NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_FRANKLIN)) AND IS_ENTITY_AT_COORD(GET_PED_INDEX(CHAR_FRANKLIN), <<-556.707581,5235.427246,83.054245>>,<<19.250000,44.750000,16.750000>>))
				OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueExit) = 0)
					sLamar.vDest = vDest[4]
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
					sLamar.iTimer = 0
					sLamar.iEvent++
					sLamar.bRefreshTasks = TRUE
				ENDIF
			BREAK
			
			CASE 56 //Runs down to the conveyor area
				//Warp Lamar if he falls too far behind the player.
				IF NOT sLamar.bRefreshTasks
					IF IS_PED_STEALING_PEDS_DESTINATION(PLAYER_PED_ID(), sLamar.ped, sLamar.vDest, vOtherBuddysCurrentDestination, 5.0, 3.0)
						IF sLamar.bIsUsingSecondaryCover
							sLamar.vDest = vDest[4]
							sLamar.bIsUsingSecondaryCover = FALSE
						ELSE
							sLamar.vDest = vAltDest[4]
							sLamar.bIsUsingSecondaryCover = TRUE
						ENDIF
						
						bPlayerStoleCover = TRUE
						sLamar.bRefreshTasks = TRUE
					ENDIF
				ENDIF
				
				fDistToDestination = VDIST2(vBuddyPos, sLamar.vDest)
				
				IF sLamar.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(sLamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sLamar.vDest, <<-574.0313, 5258.5898, 71.0182>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sLamar.bRefreshTasks = FALSE
				ENDIF
				
				//Fire bullets during the aim run task if it's safe to do so.
				IF IS_IT_SAFE_TO_FIRE_BULLETS_FROM_PED(sLamar.ped)
					IF IS_ENTITY_IN_ANGLED_AREA(sLamar.ped, <<-527.860657,5247.066406,79.939369>>, <<-556.417664,5258.927734,69.149582>>, 14.500000)
						IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueExit) > 0
							IF NOT IS_PED_BLOCKING_TARGET(PLAYER_PED_ID(), sLamar.ped, <<-574.0313, 5258.5898, 71.0182>>)
								SET_PED_SHOOTS_AT_COORD(sLamar.ped, <<-574.0313, 5258.5898, 71.0182>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueExit) = 0
					sLamar.vDest = vDest[5]
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
					sLamar.iTimer = 0
					sLamar.iEvent++
					sLamar.bRefreshTasks = TRUE
				ELSE
					//Play some "more guys" dialogue.
					IF NOT bHasTextLabelTriggered[LM2_MORE3]
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						AND NOT IS_SELECTOR_CAM_ACTIVE()
							IF sBallasPostRescueExit[0].bIsCreated
								IF VDIST2(vPlayerPos, vBuddyPos) < 900.0
								AND VDIST2(vBuddyPos, sLamar.vDest) < 400.0
									IF (IS_VEHICLE_DRIVEABLE(vehBallaPostRescueExit[0]) AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBallaPostRescueExit[0]))
									OR (IS_VEHICLE_DRIVEABLE(vehBallaPostRescueExit[1]) AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBallaPostRescueExit[1]))
										//If the cars are still driving play a specific line about them pulling up.
										IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_LCARS", CONV_PRIORITY_MEDIUM)
											bHasTextLabelTriggered[LM2_LCARS] = TRUE
										ENDIF
									ELSE
										IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MORE", CONV_PRIORITY_MEDIUM)
											bHasTextLabelTriggered[LM2_MORE3] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 57 //Lamar goes to the exit and waits				
				fDistToDestination = VDIST2(vBuddyPos, sLamar.vDest)
				
				IF sLamar.bRefreshTasks
				OR (GET_SCRIPT_TASK_STATUS(sLamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
				AND (fDistToDestination > 4.0 OR NOT bIsInCombat))	
					SET_PED_SPHERE_DEFENSIVE_AREA(sLamar.ped, sLamar.vDest, 2.0, TRUE)
				
					OPEN_SEQUENCE_TASK(seq)
						IF fDistToDestination > 4.0
						AND NOT bPlayerStoleCover
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, sLamar.vDest, <<-591.5313, 5285.2898, 71.0182>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, 
																				  TRUE, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
						ENDIF
						
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					sLamar.bRefreshTasks = FALSE
				ENDIF
				
				//Area should be safe, Lamar can rejoin group.
				IF VDIST2(vBuddyPos, sLamar.vDest) < 25.0
					IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sLamar.ped, FALSE)
						CLEAR_PED_TASKS(sLamar.ped)
						SET_PED_AS_GROUP_MEMBER(sLamar.ped, PLAYER_GROUP_ID())
						
						SET_PED_NEVER_LEAVES_GROUP(sLamar.ped, TRUE)

						sLamar.iEvent = 100
					ENDIF
				ENDIF
			BREAK
			
			CASE 100 //Lamar is now in the player's group.
				
			BREAK
		ENDSWITCH
		
		//1476917 - If Lamar is set on fire then make sure it isn't prolonged.
		IF sLamar.iEvent > 50
		AND NOT IS_PED_INJURED(sLamar.ped)
			IF IS_ENTITY_ON_FIRE(sLamar.ped)
				IF GET_GAME_TIMER() - iLamarFireTimer > 2000
					STOP_ENTITY_FIRE(sLamar.ped)
				ENDIF
			ELSE
				iLamarFireTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC UPDATE_BUDDY_SHOOTOUT_DIALOGUE()
	PED_INDEX pedBuddy
	PED_INDEX pedNearest[5]
	TEXT_LABEL strBuddyDialogueLabel
	TEXT_LABEL strPlayerDialogueLabel
	BOOL bEnemyPedIsNearby = FALSE
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	INT i = 0
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
		
		IF eCurrentPlayer != SELECTOR_PED_MICHAEL
			IF NOT bHasTextLabelTriggered[FRAN2_GETLAMAR]
			AND eMissionStage < STAGE_GET_LAMAR_OUT
				IF NOT bHasTextLabelTriggered[LM2_driveT]
				AND IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
				AND IS_PED_IN_VEHICLE(pedBuddy, sBulldozer.veh)
					strBuddyDialogueLabel = "LM2_driveT"
				ELSE
					strBuddyDialogueLabel = "LM2_drive"
				ENDIF
				strPlayerDialogueLabel = "LM2_FRANGEN" 
			ENDIF
			
			IF eMissionStage = STAGE_GET_LAMAR_OUT
				strBuddyDialogueLabel = "LM2_TRVATT"
			ENDIF
		ENDIF
	ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]

		IF eCurrentPlayer != SELECTOR_PED_MICHAEL
			IF NOT bHasTextLabelTriggered[FRAN2_GETLAMAR]
			AND eMissionStage < STAGE_GET_LAMAR_OUT
				strBuddyDialogueLabel = "LM2_FRANGEN"
				strPlayerDialogueLabel = "LM2_drive" 
			ENDIF
			
			IF eMissionStage = STAGE_GET_LAMAR_OUT
				strPlayerDialogueLabel = "LM2_TRVATT" 
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_GAME_TIMER() - iCombatDialogueWaitTime > 0
	AND NOT IS_SELECTOR_CAM_ACTIVE()
		//Player dialogue
		IF GET_GAME_TIMER() - iPlayerCombatDialogueTimer > 0
			GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedNearest)
			
			REPEAT COUNT_OF(pedNearest) i
				IF NOT IS_PED_INJURED(pedNearest[i])
					IF GET_ENTITY_MODEL(pedNearest[i]) = modelBalla
						IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(pedNearest[i])) < 10000.0
							bEnemyPedIsNearby = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT

			IF bEnemyPedIsNearby
			AND IS_PED_SHOOTING(PLAYER_PED_ID())
				IF NOT IS_STRING_NULL_OR_EMPTY(strPlayerDialogueLabel)
					IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", strPlayerDialogueLabel, CONV_PRIORITY_MEDIUM)
						iPlayerCombatDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 13000)
						iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
		//Buddy dialogue
		IF GET_GAME_TIMER() - iBuddyCombatDialogueTimer > 0
			IF NOT IS_PED_INJURED(pedBuddy)
				IF (IS_PED_SHOOTING(pedBuddy) OR IS_PED_IN_COMBAT(pedBuddy) OR (IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(pedBuddy, sBulldozer.veh)))
				AND NOT IS_PED_RAGDOLL(pedBuddy)
				AND VDIST2(GET_ENTITY_COORDS(pedBuddy), vPlayerPos) < 625.0
					IF NOT IS_STRING_NULL_OR_EMPTY(strBuddyDialogueLabel)
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", strBuddyDialogueLabel, CONV_PRIORITY_MEDIUM)
							IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(pedBuddy, sBulldozer.veh)
								iBuddyCombatDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(15000, 19000)
							ELSE
								iBuddyCombatDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 13000)
							ENDIF
							
							IF ARE_STRINGS_EQUAL(strBuddyDialogueLabel, "LM2_driveT")
								bHasTextLabelTriggered[LM2_driveT] = TRUE
							ENDIF
							
							iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//If the buddies are on different paths then play update dialogue 
		IF g_bFranklin2RequestedBackup
		AND eMissionStage = STAGE_ATTACK_SAWMILL
		AND eCurrentPlayer != SELECTOR_PED_MICHAEL
		AND NOT DOES_BLIP_EXIST(sLamar.blip)
		AND NOT IS_PED_INJURED(pedBuddy)
			IF GET_GAME_TIMER() - iBuddyUpdateDialogueTimer > 0
				IF VDIST2(GET_ENTITY_COORDS(pedBuddy), vPlayerPos) > 2500.0
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						IF NOT bHasTextLabelTriggered[LM2_ANYSIGN]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_ANYSIGN", CONV_PRIORITY_MEDIUM)
								iBuddyUpdateDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(35000, 40000)
								iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
								bHasTextLabelTriggered[LM2_ANYSIGN] = TRUE
							ENDIF
						ELIF NOT bHasTextLabelTriggered[LM2_ANYSIGN2]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_ANYSIGN2", CONV_PRIORITY_MEDIUM)
								iBuddyUpdateDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(35000, 40000)
								iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
								bHasTextLabelTriggered[LM2_ANYSIGN2] = TRUE
							ENDIF
						ELIF NOT bHasTextLabelTriggered[LM2_ANYSIGN3]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_ANYSIGN3", CONV_PRIORITY_MEDIUM)
								iBuddyUpdateDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(35000, 40000)
								iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
								bHasTextLabelTriggered[LM2_ANYSIGN3] = TRUE
							ENDIF
						ENDIF
					ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF NOT bHasTextLabelTriggered[LM2_FOUND]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_FOUND", CONV_PRIORITY_MEDIUM)
								iBuddyUpdateDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(35000, 40000)
								iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
								bHasTextLabelTriggered[LM2_FOUND] = TRUE
							ENDIF
						ELIF NOT bHasTextLabelTriggered[LM2_FOUND2]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_FOUND2", CONV_PRIORITY_MEDIUM)
								iBuddyUpdateDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(35000, 40000)
								iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
								bHasTextLabelTriggered[LM2_FOUND2] = TRUE
							ENDIF
						ELIF NOT bHasTextLabelTriggered[LM2_FOUND3]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_FOUND3", CONV_PRIORITY_MEDIUM)
								iBuddyUpdateDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(35000, 40000)
								iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
								bHasTextLabelTriggered[LM2_FOUND3] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Play dialogue if one of the buddies passes the weed.
	IF NOT bHasTextLabelTriggered[LM2_WEED]
	AND eMissionStage = STAGE_ATTACK_SAWMILL
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]), <<-579.0, 5304.6, 70.1>>) < 100.0
				IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_WEEDT", CONV_PRIORITY_MEDIUM)
					bHasTextLabelTriggered[LM2_WEED] = TRUE
				ENDIF
			ENDIF
		ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
			IF VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]), <<-579.0, 5304.6, 70.1>>) < 100.0
				IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_WEEDF", CONV_PRIORITY_MEDIUM)
					bHasTextLabelTriggered[LM2_WEED] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Play dialogue if AI Michael just killed a ped.
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF iTimeLastPedKilledByMichael > 0
		AND GET_GAME_TIMER() - iTimeLastPedKilledByMichael < 1500
			IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "lm2_mikill", CONV_PRIORITY_MEDIUM)
				iTimeLastPedKilledByMichael = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
	//Play dialogue if AI Franklin just killed a ped (while playing as Michael).
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF iTimeLastPedKilledByFranklin > 0
		AND GET_GAME_TIMER() - iTimeLastPedKilledByFranklin < 1500
		AND eCurrentPlayer = SELECTOR_PED_MICHAEL
			IF eFranklinRoute = BUDDY_ROUTE_FRONT_ASSAULT
			OR eMissionStage = STAGE_GET_LAMAR_OUT
				IF IS_ENTITY_ON_SCREEN(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "lm2_frkill", CONV_PRIORITY_MEDIUM)
						iTimeLastPedKilledByFranklin = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Play dialogue if the player as Michael hasn't killed anyone.
	IF eCurrentPlayer = SELECTOR_PED_MICHAEL
		IF NOT IS_SELECTOR_CAM_ACTIVE()
		AND NOT bHasTextLabelTriggered[FRAN2_SWFRANK] //This text triggers once the buddies have reached the exit.
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				iTimeSincePlayerShotSniperRifle = GET_GAME_TIMER()
				bHasTextLabelTriggered[LM2_enc1] = FALSE
				bHasTextLabelTriggered[LM2_enc2] = FALSE
			ELSE
				IF eTrevorRoute = BUDDY_ROUTE_FRONT_ASSAULT
				OR eMissionStage = STAGE_GET_LAMAR_OUT
					IF NOT bHasTextLabelTriggered[LM2_enc1]
						IF GET_GAME_TIMER() - iTimeSincePlayerShotSniperRifle > 10000
						AND GET_GAME_TIMER() - iTrevorInDangerTimer > 10000
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_enc1", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[LM2_enc1] = TRUE
							ENDIF
						ENDIF
					ELIF NOT bHasTextLabelTriggered[LM2_enc2]
						IF GET_GAME_TIMER() - iTimeSincePlayerShotSniperRifle > 17000
						AND sSelectorPeds.bDisplayDamage[SELECTOR_PED_TREVOR]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_enc2", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[LM2_enc2] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		iTimeSincePlayerShotSniperRifle = GET_GAME_TIMER()
	ENDIF
ENDPROC

FUNC BOOL CREATE_RANDOM_LAMAR_SHOOTOUT_CONVERSATION()
	INT i = GET_RANDOM_INT_IN_RANGE(0, 8)
	TEXT_LABEL strLabel = ""
	TRIGGERED_TEXT_LABEL eLabel
	
	SWITCH i
		CASE 0 		
			strLabel += "LM2_SH1" 	
			eLabel = LM2_SH1
		BREAK
		
		CASE 1 		
			strLabel += "LM2_SH2" 	
			eLabel = LM2_SH2
		BREAK
		
		CASE 2 		
			strLabel += "LM2_SH3" 		
			eLabel = LM2_SH3
		BREAK
		
		CASE 3 		
			strLabel += "LM2_SH4" 	
			eLabel = LM2_SH4
		BREAK
		
		CASE 4 		
			strLabel += "LM2_SH5" 	
			eLabel = LM2_SH5
		BREAK
		
		CASE 5 		
			strLabel += "LM2_SH6" 	
			eLabel = LM2_SH6
		BREAK
		
		CASE 6 		
			strLabel += "LM2_SH7" 	
			eLabel = LM2_SH7
		BREAK
		
		CASE 7 		
			strLabel += "LM2_SH8" 		
			eLabel = LM2_SH8
		BREAK
	ENDSWITCH
	
	IF NOT bHasTextLabelTriggered[eLabel]
		IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", strLabel, CONV_PRIORITY_MEDIUM)
			bHasTextLabelTriggered[eLabel] = TRUE
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_LAMAR_SHOOTOUT_DIALOGUE()
	IF NOT IS_PED_INJURED(sLamar.ped)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR vLamarPos = GET_ENTITY_COORDS(sLamar.ped)
	
		IF VDIST2(vPlayerPos, vLamarPos) < 400.0
			IF GET_GAME_TIMER() - iCombatDialogueWaitTime > 0
			AND GET_GAME_TIMER() - iLamarCombatDialogueTimer > 0
				IF (bHasTextLabelTriggered[LM2_LSEE] AND (bHasTextLabelTriggered[LM2_LHEARM] OR NOT bHasTextLabelTriggered[LM2_OUT])) //Don't play if we're waiting on responses from Lamar to buddy peds.
				OR NOT g_bFranklin2RequestedBackup
					IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
						IF CREATE_RANDOM_LAMAR_SHOOTOUT_CONVERSATION()
							ilamarCombatDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(14000, 19000)
							iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
						ENDIF	
					ELSE
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_SPEAK", CONV_PRIORITY_MEDIUM)
							ilamarCombatDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(14000, 19000)
							iCombatDialogueWaitTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
			IF bHasTextLabelTriggered[LM2_OUT]
				//If the buddy told the player to go to Michael, then Lamar says a line about Michael being in the bushes.
				IF NOT bHasTextLabelTriggered[LM2_LHEARM]
				AND eCurrentPlayer != SELECTOR_PED_MICHAEL
					IF GET_GAME_TIMER() - iTimePlayedGotoMichaelDialogue < 13000
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_LHEARM", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[LM2_LHEARM] = TRUE
						ENDIF	
					ELSE
						bHasTextLabelTriggered[LM2_LHEARM] = TRUE
					ENDIF
				ENDIF
				
				//Play some dialogue if the player goes in a different direction.
				IF NOT bHasTextLabelTriggered[LM2_WAY]
				AND eCurrentPlayer != SELECTOR_PED_MICHAEL
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-486.696198,5309.810547,86.643921>>,<<22.250000,2.500000,7.500000>>)
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_WAY", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[LM2_WAY] = TRUE
						ENDIF	
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_FOR_MICHAEL_ABILITY_HELP_TEXT()
	IF NOT bHasTextLabelTriggered[FRAN2_SPECHELP]
		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
			bHasTextLabelTriggered[FRAN2_SPECHELP] = TRUE
		ELSE
			IF GET_GAME_TIMER() - iTimeSinceSwitchToMichael > 30000
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					PRINT_HELP("FRAN2_SPECHELP_KM")
				ELSE
					PRINT_HELP("FRAN2_SPECHELP")
				ENDIF
				bHasTextLabelTriggered[FRAN2_SPECHELP] = TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SPECHELP")
				CLEAR_HELP()
			ENDIF
			
			IF IS_PC_VERSION()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SPECHELP_KM")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///Fail cleanup cannot delete peds in the same group/vehicle as the player, so use this to force delete them.
PROC FORCE_DELETE_PED_FOR_FAIL(PED_INDEX ped)
	IF NOT IS_ENTITY_DEAD(ped)
		IF IS_PED_GROUP_MEMBER(ped, PLAYER_GROUP_ID())
		OR ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), ped)
			REMOVE_PED(ped, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC MISSION_PASSED(BOOL bIsDebugPass = FALSE)
	IF bIsDebugPass
		#IF IS_DEBUG_BUILD
			IF NOT HAS_CUTSCENE_FINISHED()
				STOP_CUTSCENE()
			ENDIF
		 	
			REMOVE_CUTSCENE()

			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			
			DO_FADE_IN_WITH_WAIT()
		#ENDIF
	ENDIF

	Mission_Flow_Mission_Passed()
	
	MISSION_CLEANUP()
ENDPROC

PROC MISSION_FAILED(FAILED_REASON reason)
	IF NOT bMissionFailed
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		CLEAR_PRINTS()
		CLEAR_HELP()
		REMOVE_ALL_BLIPS()
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		KILL_ANY_CONVERSATION()

		SWITCH reason
			CASE FAILED_GENERIC
				strFailLabel = ""
			BREAK
			
			CASE FAILED_ABANDONED_LAMAR
				strFailLabel = "FRAN2_LABAN"
			BREAK
			
			CASE FAILED_BULLDOZER_DESTROYED
				strFailLabel = "FRAN2_F03"
			BREAK
			
			CASE FAILED_COVER_BLOWN
				strFailLabel = "FRAN2_FEARLY"
			BREAK
			
			CASE FAILED_FRANKLIN_DIED
				strFailLabel = "CMN_FDIED"
			BREAK
			
			CASE FAILED_LAMAR_DIED
				strFailLabel = "FRAN2_F01"
			BREAK
			
			CASE FAILED_MICHAEL_DIED
				strFailLabel = "CMN_MDIED"
			BREAK
			
			CASE FAILED_TREVOR_DIED
				strFailLabel = "CMN_TDIED"
			BREAK
			
			CASE FAILED_COPS_LED_TO_SAWMILL
				strFailLabel = "FRAN2_FCOPS"
			BREAK
			
			CASE FAILED_MICHAELS_CAR_DESTROYED
				strFailLabel = "CMN_MDEST"
			BREAK
			
			CASE FAILED_TREVORS_CAR_DESTROYED
				strFailLabel = "CMN_TDEST"
			BREAK
		ENDSWITCH
		
		bMissionFailed = TRUE
	ENDIF
		
	TRIGGER_MUSIC_EVENT("FRA2_FAIL")

	MISSION_FLOW_MISSION_FAILED_WITH_REASON(strFailLabel)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		IF eMissionStage >= STAGE_GET_INTO_POSITION
		AND eMissionStage < STAGE_DRIVE_HOME
			UPDATE_SHOOTOUT_ENEMIES()
			UPDATE_LAMAR_DURING_SHOOTOUT()
			
			IF eMissionStage >= STAGE_GET_LAMAR_OUT
				UPDATE_SHOOTOUT_ENEMIES_AFTER_RESCUING_LAMAR()
			ENDIF
		ENDIF
	
		WAIT(0)
	ENDWHILE
	
	RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	
	FORCE_DELETE_PED_FOR_FAIL(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
	FORCE_DELETE_PED_FOR_FAIL(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	FORCE_DELETE_PED_FOR_FAIL(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	FORCE_DELETE_PED_FOR_FAIL(sLamar.ped)

	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
ENDPROC


PROC DO_INTRO_CUTSCENE()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF SETUP_REQ_PLAYER_IS_FRANKLIN()
			IF bHasUsedCheckpoint
				START_REPLAY_SETUP(<<8.2263, 535.3607, 175.0279>>, 2.8314, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<8.2263, 535.3607, 175.0279>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 2.8314)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			SETTIMERA(0)
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		REQUEST_CUTSCENE("FRA_2_INT")
		SET_CUTSCENE_PED_COMPONENT_VARIATIONS("FRA_2_INT")
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
	
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()		
			objFrontDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<7.5184, 539.5269, 176.1776>>, 2.0, v_ilev_fh_frontdoor, TRUE)
			
			IF DOES_ENTITY_EXIST(objFrontDoor)
				CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), "] ", "Successfully grabbed front door.")
				//NOTE: 1024 represents a new flag for flagging a registered door as a map door.
				REGISTER_ENTITY_FOR_CUTSCENE(objFrontDoor, "Franklins_Door", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DEFAULT, INT_TO_ENUM(CUTSCENE_ENTITY_OPTION_FLAGS, 1024)) 
			ENDIF
		
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			START_CUTSCENE()

			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

			iCurrentEvent = 0
			bSkippedMocap = FALSE
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
		
		//JUMP_TO_STAGE(STAGE_MEET_THE_CREW)
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING
		SWITCH iCurrentEvent
			CASE 0 //Do any setup on first frame of cutscene.
				IF IS_CUTSCENE_PLAYING()
					IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(<<0.0, 0.0, 0.0>>, TRUE)
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<3.1501, 543.9788, 173.7280>>, 124.6853)
					ENDIF
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<8.178145,549.365967,169.815567>>, <<18.647863,526.653015,179.864288>>, 29.000000, <<3.1501, 543.9788, 173.7280>>, 124.6853,
																	    GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
					//DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES()
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
					MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FRANKLIN_2)
				
					DO_FADE_IN_WITH_WAIT() //In case this is a mission replay, the game will be faded out at this point
				
					//1910810 - Registering the door for the cutscene causes portal issues, to get around this open the door slightly when the cutscene starts.
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_fh_frontdoor, <<7.5184, 539.5269, 176.1776>>, TRUE, 0.1)
				
					CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), "] ", "Intro cutscene is playing.")
				
					CLEAR_AREA_OF_PEDS(<<0.0, 0.0, 0.0>>, 5000.0)
					CLEAR_AREA_OF_OBJECTS(<<0.0, 0.0, 0.0>>, 5000.0)
					CLEAR_AREA_OF_COPS(<<0.0, 0.0, 0.0>>, 5000.0)
					CLEAR_AREA_OF_PROJECTILES(<<0.0, 0.0, 0.0>>, 5000.0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 1
				IF bSkippedMocap
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_fh_frontdoor, <<7.5184, 539.5269, 176.1776>>, TRUE, 0.0)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_fh_frontdoor, <<7.5184, 539.5269, 176.1776>>, FALSE, 0.0)
					iCurrentEvent++
				ENDIF
			BREAK
		ENDSWITCH
	
		IF NOT IS_CUTSCENE_ACTIVE()
			eSectionStage = SECTION_STAGE_CLEANUP
		ENDIF	
		
		IF WAS_CUTSCENE_SKIPPED()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			eSectionStage = SECTION_STAGE_SKIP
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			REPLAY_STOP_EVENT()
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP		
		IF bSkippedMocap
		
		ENDIF
		
		REMOVE_OBJECT(objFrontDoor, FALSE)
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, FALSE)
	
		eMissionStage = STAGE_MEET_THE_CREW
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		DO_FADE_OUT_WITH_WAIT()
		
		STOP_CUTSCENE()
		bSkippedMocap = TRUE
		
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

PROC MEET_THE_CREW()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF SETUP_REQ_PLAYER_IS_FRANKLIN()
			IF bHasUsedCheckpoint
				START_REPLAY_SETUP(<<8.2263, 535.3607, 175.0279>>, 2.8314, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<8.2263, 535.3607, 175.0279>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 2.8314)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			SETTIMERA(0)
			
			//Stream assets.
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0, TRUE, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		RESET_SHOOTOUT_VARIABLES()
		bTaxiDropoffUpdated = FALSE
	
		IF NOT IS_SCREEN_FADED_IN()
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_BIKE) 
            CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_CAR)
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			WAIT(500)
			
			DO_FADE_IN_WITH_WAIT()
		ENDIF
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)

		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CREATE_SHOOTOUT_PICKUPS()
		
		SETTIMERB(0)
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING	
		#IF IS_DEBUG_BUILD 
			DONT_DO_J_SKIP(sLocatesData) 
		#ENDIF
		
		FLOAT fPlayerDistFromDestination = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeetLocation)
		FLOAT fPlayerDistFromOtherSide = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-484.7, 5276.6, 87.5>>)
		BOOL bLoseWantedLevel = TRUE
		
		//Fail if the player has a wanted level and goes to the sawmill.
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			IF fPlayerDistFromDestination < 2500.0
				bLoseWantedLevel = FALSE
			
				IF GET_GAME_TIMER() - iWantedFailTimer > 1500
					MISSION_FAILED(FAILED_COPS_LED_TO_SAWMILL)
				ENDIF
			ENDIF
		ELSE
			iWantedFailTimer = GET_GAME_TIMER()
		ENDIF
		
		VEHICLE_INDEX vehFinalCar
		
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehFinalCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		
		SWITCH iCurrentEvent
			CASE 0
				IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData, vMeetLocation, <<0.001, 0.001, LOCATE_SIZE_HEIGHT>>, TRUE, "FRAN2_GOSAW", bLoseWantedLevel)
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF (NOT IS_ENTITY_DEAD(vehFinalCar) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-704.742188,5313.340820,74.917175>>, <<-733.021667,5316.371094,69.172340>>, 37.000000) AND NOT IS_ENTITY_IN_AIR(vehFinalCar))
					OR (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-711.735474,5310.284180,75.798866>>, <<-730.608948,5311.804199,69.004204>>, 27.500000))
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						KILL_FACE_TO_FACE_CONVERSATION()
						
						IF IS_MOBILE_PHONE_CALL_ONGOING()
							HANG_UP_AND_PUT_AWAY_PHONE()
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(vehFinalCar)
						AND IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehFinalCar))
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
						ENDIF
						
						iCurrentEvent++
					ENDIF
				ENDIF
				
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT bHasTextLabelTriggered[LM2_PHNL]
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							ADD_PED_FOR_DIALOGUE(sConversationPeds, 8, NULL, "LESTER")
						
							IF PLAYER_CALL_CHAR_CELLPHONE(sConversationPeds, CHAR_LESTER, "LEM2AUD", "LM2_PHNL", CONV_PRIORITY_CELLPHONE)
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								
								bHasTextLabelTriggered[LM2_PHNL] = TRUE
							ENDIF
						ENDIF
					/*ELIF NOT bHasTextLabelTriggered[Lm2_mut]
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						AND NOT IS_MOBILE_PHONE_CALL_ONGOING()
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "Lm2_mut", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[Lm2_mut] = TRUE
								iMainDialogueTimer = 0
							ENDIF
						ENDIF
					ELIF NOT bHasTextLabelTriggered[FRAN2_phnhlp]
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF iMainDialogueTimer = 0
								iMainDialogueTimer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - iMainDialogueTimer > 1800
								PRINT_HELP("FRAN2_phnhlp")
								bHasTextLabelTriggered[FRAN2_phnhlp] = TRUE
							ENDIF
						ENDIF*/
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				BOOL bHasStopped
				
				IF NOT IS_ENTITY_DEAD(vehFinalCar)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
				
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehFinalCar, DEFAULT_VEH_STOPPING_DISTANCE, 1)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
						bHasStopped = TRUE
					ENDIF
				ELSE
					bHasStopped = TRUE
				ENDIF
			
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF

				IF bHasStopped
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
					eSectionStage = SECTION_STAGE_CLEANUP
				ENDIF
			BREAK
		ENDSWITCH
		
		//If the player calls Michael or Trevor then force a specific call through.
		/*IF NOT bHasTextLabelTriggered[LM2_phn]
		AND bHasTextLabelTriggered[LM2_PHNL]
			IF fPlayerDistFromDestination > 90000.0
				IF IS_CALLING_CONTACT(CHAR_MICHAEL)
					IF PLAYER_CALL_CHAR_CELLPHONE(sConversationPeds, CHAR_MICHAEL, "LEM2AUD", "LM2_phnM", CONV_PRIORITY_MEDIUM)
						DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
						bHasTextLabelTriggered[LM2_phn] = TRUE
						g_bFranklin2RequestedBackup = TRUE
					ENDIF
				ELIF IS_CALLING_CONTACT(CHAR_TREVOR)
					IF PLAYER_CALL_CHAR_CELLPHONE(sConversationPeds, CHAR_TREVOR, "LEM2AUD", "LM2_phnT", CONV_PRIORITY_MEDIUM)
						DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
						bHasTextLabelTriggered[LM2_phn] = TRUE
						g_bFranklin2RequestedBackup = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF*/
		
		//If the enemies were alreted early then progress straight away.
		IF bEnemiesHaveBeenAlerted
			eSectionStage = SECTION_STAGE_CLEANUP
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_GET_TO_SAWMILL")
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				START_AUDIO_SCENE("FRANKLIN_2_GET_TO_SAWMILL")
			ENDIF
		ENDIF
		
		IF NOT bPlayerPickedStartCar
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//Track the vehicle speed/damage stat.
				VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF NOT IS_ENTITY_DEAD(veh)
					IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(veh))
						INFORM_MISSION_STATS_OF_INCREMENT(FRA2_VEHICLE_CHOSEN, 0)
					ELIF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(veh))
						INFORM_MISSION_STATS_OF_INCREMENT(FRA2_VEHICLE_CHOSEN, 1)
					ELSE
						INFORM_MISSION_STATS_OF_INCREMENT(FRA2_VEHICLE_CHOSEN, 2) //something else
					ENDIF
				ELSE
					INFORM_MISSION_STATS_OF_INCREMENT(FRA2_VEHICLE_CHOSEN, 2) //no vehicle
				ENDIF
				
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				bPlayerPickedStartCar = TRUE
			ENDIF
		ENDIF
		
		
		IF fPlayerDistFromDestination < 40000.0
		OR fPlayerDistFromOtherSide < 90000.0
			UPDATE_SHOOTOUT_ENEMIES()
			
			IF DOES_ENTITY_EXIST(sBallasStart[0].ped)
				SETUP_REQ_BULLDOZER(vBulldozerStartPos, fBulldozerStartHeading)
			ENDIF
			
			IF DOES_ENTITY_EXIST(sBulldozer.veh)
				SETUP_REQ_SHOOTOUT_COVER_OBJECTS()
			ENDIF
		
			//Pre-stream Michael, Trevor and the cars en-route.
			IF g_bFranklin2RequestedBackup
				SETUP_REQ_MICHAEL(vMichaelMeetingPos, fMichaelMeetingHeading)
				SETUP_REQ_TREVOR(vTrevorMeetingPos, fTrevorMeetingHeading)
				
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					SETUP_REQ_MICHAELS_CAR(vMichaelsCarStartPos, fMichaelsCarStartHeading)
					SETUP_REQ_TREVORS_CAR(vTrevorsCarStartPos, fTrevorsCarStartHeading)
				ENDIF
			ENDIF
		ELIF fPlayerDistFromDestination > 62500.0 
		AND fPlayerDistFromOtherSide > 1600000.0
			IF g_bFranklin2RequestedBackup
				REMOVE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
				REMOVE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
				REMOVE_VEHICLE(vehMichaelsCar, TRUE)
				REMOVE_VEHICLE(vehTrevorsCar, TRUE)
			ENDIF
		ENDIF
		
		IF g_bFranklin2RequestedBackup
			IF fPlayerDistFromDestination < (DEFAULT_CUTSCENE_LOAD_DIST * DEFAULT_CUTSCENE_LOAD_DIST)
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				AND DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					REQUEST_CUTSCENE("FRA_2_IG_4_ALT1_concat")
					SET_CUTSCENE_PED_COMPONENT_VARIATIONS("FRA_2_IG_4_ALT1_concat")
				ENDIF
			ELIF fPlayerDistFromDestination > (150.0 * 150.0) //Used to be default unload dist, but there were issues when following the road.
				REMOVE_CUTSCENE()
			ENDIF
		ENDIF
		
		//Add a custom taxi drop-off.
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			IF NOT bTaxiDropoffUpdated
				SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(sLocatesData.LocationBlip, <<-745.3523, 5320.2935, 72.6167>>, 257.6477)
				bTaxiDropoffUpdated = TRUE
			ENDIF
		ELSE
			bTaxiDropoffUpdated = FALSE
		ENDIF
		
		//Fail for destroying the bulldozer early.
		IF DOES_ENTITY_EXIST(sBulldozer.veh)
			IF NOT IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
				MISSION_FAILED(FAILED_BULLDOZER_DESTROYED)
			ENDIF
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		REMOVE_ALL_BLIPS()
		CLEAR_HELP()
		
		//If the player didn't go there in a car then just set the stat to no vehicle.
		IF NOT bPlayerPickedStartCar
			INFORM_MISSION_STATS_OF_INCREMENT(FRA2_VEHICLE_CHOSEN, 2) 
		ENDIF
		
		REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
		
		IF g_bFranklin2RequestedBackup
		AND NOT bEnemiesHaveBeenAlerted
			eMissionStage = STAGE_MEET_CREW_CUTSCENE
		ELSE
			eMissionStage = STAGE_GET_INTO_POSITION
		ENDIF
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		JUMP_TO_STAGE(STAGE_MEET_CREW_CUTSCENE)
	ENDIF
ENDPROC

PROC MEET_CREW_CUTSCENE()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		#IF IS_DEBUG_BUILD
			IF NOT bHasUsedCheckpoint
				g_bFranklin2RequestedBackup = TRUE
			ENDIF
		#ENDIF	
	
		IF SETUP_REQ_PLAYER_IS_FRANKLIN()
			IF bHasUsedCheckpoint
				START_REPLAY_SETUP(vMeetLocation, 204.9590, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vMeetLocation)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 204.9590)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			SETTIMERA(0)
			
			RESET_SHOOTOUT_VARIABLES()
			
			//Stream assets.
			WHILE NOT DOES_ENTITY_EXIST(sBulldozer.veh)
			OR NOT DOES_ENTITY_EXIST(objShootoutCover[0])
			OR NOT sBallasStart[0].bIsCreated
			OR NOT bCreatedBallaStartCars
				UPDATE_SHOOTOUT_ENEMIES()
				SETUP_REQ_BULLDOZER(vBulldozerStartPos, fBulldozerStartHeading)
				SETUP_REQ_SHOOTOUT_COVER_OBJECTS()
				
				WAIT(0)
			ENDWHILE
			
			IF g_bFranklin2RequestedBackup
				WHILE NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				OR NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				OR NOT DOES_ENTITY_EXIST(vehMichaelsCar)
				OR NOT DOES_ENTITY_EXIST(vehTrevorsCar)
					SETUP_REQ_MICHAEL(vMichaelMeetingPos, fMichaelMeetingHeading)
					SETUP_REQ_TREVOR(vTrevorMeetingPos, fTrevorMeetingHeading)
					SETUP_REQ_MICHAELS_CAR(vMichaelsCarStartPos, fMichaelsCarStartHeading)
					SETUP_REQ_TREVORS_CAR(vTrevorsCarStartPos, fTrevorsCarStartHeading)
					
					WAIT(0)
				ENDWHILE
			ENDIF	
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			CREATE_SHOOTOUT_PICKUPS()
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			bEnemiesHaveBeenAlerted = FALSE
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		REQUEST_CUTSCENE("FRA_2_IG_4_ALT1_concat")
		SET_CUTSCENE_PED_COMPONENT_VARIATIONS("FRA_2_IG_4_ALT1_concat")
	
		RESET_SHOOTOUT_VARIABLES()
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
		DISABLE_PLAYER_VEHICLE_CONTROLS_THIS_FRAME()
		SETUP_REQ_WEED_PALLETS_FOR_CUTSCENE(TRUE)
	
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		AND SETUP_REQ_WEED_PALLETS_FOR_CUTSCENE(TRUE)
		AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				SET_FOCUS_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ENDIF

			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
				IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER)
					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER, 100)
				ENDIF
				
				SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER, TRUE)
				
				objMichaelsWeapon = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER)
				REGISTER_ENTITY_FOR_CUTSCENE(objMichaelsWeapon, "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehMichaelsCar)
				REGISTER_ENTITY_FOR_CUTSCENE(vehMichaelsCar, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ELSE
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Michaels_car", CU_DONT_ANIMATE_ENTITY, GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevorsCar)
				REGISTER_ENTITY_FOR_CUTSCENE(vehTrevorsCar, "Trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ELSE
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevors_car", CU_DONT_ANIMATE_ENTITY, GET_PLAYER_VEH_MODEL(CHAR_TREVOR))
			ENDIF
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			START_CUTSCENE()

			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

			iCurrentEvent = 0
			bSkippedMocap = FALSE
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING
		UPDATE_SHOOTOUT_ENEMIES()
	
		SWITCH iCurrentEvent
			CASE 0 //Do any setup on first frame of cutscene.
				IF IS_CUTSCENE_PLAYING()
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
					//Store the last player's car as the checkpoint car, so it's recreated on a retry.
					VEHICLE_INDEX vehLastCar
					vehLastCar = GET_PLAYERS_LAST_VEHICLE()
					
					IF NOT IS_ENTITY_DEAD(vehLastCar)
						IF VDIST2(GET_ENTITY_COORDS(vehLastCar), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 10000.0
						AND vehLastCar != vehMichaelsCar
						AND vehLastCar != vehTrevorsCar
							IF IS_ENTITY_IN_ANGLED_AREA(vehLastCar, <<-707.737244,5325.163574,74.473297>>, <<-715.099304,5299.242188,68.701591>>, 45.000000)
								SET_ENTITY_HEADING(vehLastCar, 261.1234)
								SET_ENTITY_COORDS(vehLastCar, <<-732.8705, 5317.5469, 71.9049>>)
							ENDIF
						
							SET_VEH_RADIO_STATION(vehLastCar, "OFF")
							SET_VEHICLE_ENGINE_ON(vehLastCar, FALSE, FALSE)
							OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehLastCar)
							vehReplayCar = vehLastCar
							SET_ENTITY_AS_MISSION_ENTITY(vehReplayCar, TRUE, TRUE)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(vehMichaelsCar)
						SET_VEHICLE_ENGINE_HEALTH(vehMichaelsCar, 1000.0)
						SET_VEHICLE_PETROL_TANK_HEALTH(vehMichaelsCar, 1000.0)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(vehTrevorsCar)
						SET_VEHICLE_ENGINE_HEALTH(vehTrevorsCar, 1000.0)
						SET_VEHICLE_PETROL_TANK_HEALTH(vehTrevorsCar, 1000.0)
					ENDIF
					
					//SET_ENTITY_COORDS(vehMichaelsCar, vMichaelsCarStartPos, FALSE)
					//SET_ENTITY_ROTATION(vehMichaelsCar, <<-4.7475, 2.8756, -98.7217>>, DEFAULT, FALSE)
					
					//SET_ENTITY_COORDS(vehTrevorsCar, vTrevorsCarStartPos, FALSE)
					//SET_ENTITY_ROTATION(vehTrevorsCar, <<1.2134, -3.1879, 15.0001>>, DEFAULT, FALSE)
				
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-707.737244,5325.163574,74.473297>>, <<-715.099304,5299.242188,68.701591>>, 45.000000, <<-732.8705, 5317.5469, 71.9049>>, 261.1234,
																	    GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
					
					BLOCK_EMERGENCY_SERVICES_FOR_SHOOTOUT(TRUE)
					BLOCK_AMBIENT_PEDS_AND_VEHICLES_FOR_SHOOTOUT(TRUE)
				
					CLEAR_AREA_OF_PEDS(vMeetLocation, 5000.0)
					CLEAR_AREA_OF_OBJECTS(vMeetLocation, 5000.0)
					CLEAR_AREA_OF_COPS(vMeetLocation, 5000.0)
					CLEAR_AREA_OF_PROJECTILES(vMeetLocation, 5000.0)
					//CLEAR_AREA_OF_VEHICLES(vMeetLocation, 5000.0, TRUE)
				
					STOP_AUDIO_SCENES()
					
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_PROPS, PROPS_P1_HEADSET, FALSE)
					ENDIF
				
					SETUP_REQ_WEED_PALLETS_FOR_CUTSCENE()
				
					DO_FADE_IN_WITH_WAIT() //In case this is a mission replay, the game will be faded out at this point
					
					iCurrentEvent++
				ELSE
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF GET_CUTSCENE_TIME() > 28000
					TRIGGER_MUSIC_EVENT("FRA2_START")
					iCurrentEvent++
				ENDIF
			BREAK
		ENDSWITCH
		
		//Remove the weed pallets created just for the cutscene.
		IF DOES_ENTITY_EXIST(objExtraWeed[0])
			IF IS_CUTSCENE_PLAYING()
			AND GET_CUTSCENE_TIME() > 43000
				INT i = 0
				REPEAT COUNT_OF(objExtraWeed) i
					REMOVE_OBJECT(objExtraWeed[i], TRUE)
				ENDREPEAT
			ENDIF
		ENDIF
	
		IF NOT IS_CUTSCENE_ACTIVE()
			ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
			PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
		
			eSectionStage = SECTION_STAGE_CLEANUP
		ENDIF	
		
		IF IS_CUTSCENE_PLAYING()
		AND GET_CUTSCENE_TIME() > 46000
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
		ENDIF
		
		IF WAS_CUTSCENE_SKIPPED()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			eSectionStage = SECTION_STAGE_SKIP
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], <<-686.4227, 5256.7705, 75.7882>>)
				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 223.4715)
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			CLEAR_FOCUS()
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
		
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 5500, -87.7, FALSE)
			ELSE
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 3500, -87.7, FALSE)
			ENDIF
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
			
			GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car")
			
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_car")

		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_weapon")
			GIVE_WEAPON_OBJECT_TO_PED(objMichaelsWeapon, GET_PED_INDEX(CHAR_MICHAEL))
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			REPLAY_STOP_EVENT()
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		SET_FRONTEND_RADIO_ACTIVE(TRUE)
		bEnemiesHaveBeenAlerted = FALSE
	
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
		INT i = 0
		REPEAT COUNT_OF(objExtraWeed) i
			REMOVE_OBJECT(objExtraWeed[i], TRUE)
		ENDREPEAT
		
		CLEAR_FOCUS()
	
		eMissionStage = STAGE_GET_INTO_POSITION
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		DO_FADE_OUT_WITH_WAIT()
	
		STOP_CUTSCENE()
		bSkippedMocap = TRUE
		
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

PROC GET_INTO_POSITION()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		#IF IS_DEBUG_BUILD
			IF NOT bHasUsedCheckpoint
				g_bFranklin2RequestedBackup = TRUE
			ENDIF
		#ENDIF	
	
		IF (g_bFranklin2RequestedBackup AND SETUP_REQ_PLAYER_IS_TREVOR())
		OR (NOT g_bFranklin2RequestedBackup AND SETUP_REQ_PLAYER_IS_FRANKLIN())
			IF bHasUsedCheckpoint
				START_REPLAY_SETUP(<<-706.1613, 5313.1440, 70.0483>>, 284.2023, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-706.1613, 5313.1440, 70.0483>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 284.2023)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			RESET_SHOOTOUT_VARIABLES()
			
			IF g_bFranklin2RequestedBackup
				WHILE NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				OR NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				OR NOT DOES_ENTITY_EXIST(vehMichaelsCar)
				OR NOT DOES_ENTITY_EXIST(vehTrevorsCar)
					SETUP_REQ_MICHAEL(<<-686.4227, 5256.7705, 75.7882>>, 223.4715)
					SETUP_REQ_FRANKLIN(<<-696.7804, 5312.5044, 69.2695>>, 275.3851)  
					SETUP_REQ_MICHAELS_CAR(vMichaelsCarStartPos, fMichaelsCarStartHeading)
					SETUP_REQ_TREVORS_CAR(vTrevorsCarStartPos, fTrevorsCarStartHeading)
					
					WAIT(0)
				ENDWHILE
				
				SETUP_MICHAEL_OUTFIT()
				SETUP_TREVOR_OUTFIT()
			ENDIF	
			
			//Stream assets.
			WHILE NOT SETUP_REQ_REPLAY_CAR(vReplayCarPos, fReplayCarHeading)
			OR NOT DOES_ENTITY_EXIST(sBulldozer.veh)
			OR NOT DOES_ENTITY_EXIST(objShootoutCover[0])
			OR NOT sBallasStart[0].bIsCreated
			OR NOT bCreatedBallaStartCars
				UPDATE_SHOOTOUT_ENEMIES()
				SETUP_REQ_BULLDOZER(vBulldozerStartPos, fBulldozerStartHeading)
				SETUP_REQ_SHOOTOUT_COVER_OBJECTS()
				
				WAIT(0)
			ENDWHILE
		
			IF g_bFranklin2RequestedBackup
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P2_HEADSET, FALSE)
			ELSE
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET, FALSE)
			ENDIF
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
				
			SETTIMERA(0)
			
			WHILE TIMERA() < 10000
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]))
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))
					SETTIMERA(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
			
			BLOCK_EMERGENCY_SERVICES_FOR_SHOOTOUT(TRUE)
			BLOCK_AMBIENT_PEDS_AND_VEHICLES_FOR_SHOOTOUT(TRUE)
			CREATE_SHOOTOUT_PICKUPS()
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			bEnemiesHaveBeenAlerted = FALSE
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		bLamarHasBeenRescued = FALSE
	
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
	
		TRIGGER_MUSIC_EVENT("FRA2_HEAD_TO_POS")
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_STEALTH")
			START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_STEALTH")
		ENDIF
		
		IF g_bFranklin2RequestedBackup
			SETUP_REQ_BUDDIES_START_FIGHT()
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sBulldozer.veh, FALSE)
		ENDIF
	
		CLEAR_TRIGGERED_LABELS()
		GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_FRANKLIN2_ATTACK_SAWMILL, "ATTACK_SAWMILL")
		REFRESH_BUDDY_PED_AUDIO_MIX_GROUP()
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			IF g_bFranklin2RequestedBackup
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-706.1613, 5313.1440, 70.0483>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 284.2023)
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], <<-707.5012, 5310.9429, 70.3263>>)
					SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 285.2878)
				ENDIF
			ENDIF
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			WAIT(500)
			
			DO_FADE_IN_WITH_WAIT()
		ENDIF

		IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sBulldozer.veh, TRUE)
		ENDIF

		//If the audio scene is still running from the first stage then stop it now.
		IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_GET_TO_SAWMILL")
			STOP_AUDIO_SCENE("FRANKLIN_2_GET_TO_SAWMILL")
		ENDIF

		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		SETTIMERB(0)
		eCurrentPlayer = SELECTOR_PED_TREVOR
		bTrevorTriggeredShootoutEarly = FALSE
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING	
		UPDATE_SHOOTOUT_ENEMIES()
		UPDATE_BULLDOZER()
		
		REQUEST_WAYPOINT_RECORDING(strWaypointBulldozerRoute)
		REQUEST_WAYPOINT_RECORDING(strWaypointFrontRoute)
		REQUEST_WAYPOINT_RECORDING(strWaypointRearRoute)
		
		IF bEnemiesHaveBeenAlerted
			IF g_bFranklin2RequestedBackup
				UPDATE_SHOOTOUT_SWITCH()
			ENDIF	
		
			//Warp Michael if he's not on screen.
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])) > 25.0
					IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), 2.0)
					AND NOT IS_SPHERE_VISIBLE(vMichaelSnipePos, 2.0)
						SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vMichaelSnipePos)
						SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], fMichaelSnipeHeading)
					ENDIF
				ENDIF
			ENDIF
			
			//If the player was Trevor and decided to run in early then play a line.
			IF iCurrentEvent = 0
				bTrevorTriggeredShootoutEarly = TRUE
			ENDIF
			
			//Once the enemies have been alerted then progress the mission.
			eSectionStage = SECTION_STAGE_CLEANUP
		ELSE
			//Get each ped into position
			IF NOT g_bFranklin2RequestedBackup
				IF NOT bHasTextLabelTriggered[LM2_ALONE]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_ALONE", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[LM2_ALONE] = TRUE
						ENDIF
					ENDIF
				ELIF NOT bHasTextLabelTriggered[FRAN2_FIND]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						PRINT_NOW("FRAN2_FIND", DEFAULT_GOD_TEXT_TIME, 0)
						bHasTextLabelTriggered[FRAN2_FIND] = TRUE
					ENDIF
				ENDIF
			ELSE
				BOOL bPlayerReachedStartPoint
			
				SWITCH iCurrentEvent
					CASE 0 //Get Trevor into position
						IF NOT DOES_BLIP_EXIST(blipBulldozerRoute)
							IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
								blipBulldozerRoute = CREATE_BLIP_FOR_ENTITY(sBulldozer.veh)
								SET_BLIP_COLOUR(blipBulldozerRoute, BLIP_COLOUR_YELLOW)
								SET_BLIP_BRIGHT(blipBulldozerRoute, FALSE)
							ELSE
								blipBulldozerRoute = CREATE_BLIP_FOR_COORD(vBulldozerRouteStartPos)
							ENDIF
						ELSE
							BOOL bShowCorona
						
							IF NOT IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
								IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(blipBulldozerRoute), vBulldozerRouteStartPos, 0.1)
									REMOVE_BLIP(blipBulldozerRoute)
								ENDIF
								
								bShowCorona = TRUE
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									DISABLE_PLAYER_VEHICLE_DRIVING_CONTROLS_THIS_FRAME()
								
									SET_BLIP_ALPHA(blipBulldozerRoute, 0)
								ELSE
									SET_BLIP_ALPHA(blipBulldozerRoute, 255)
								ENDIF
							
								bShowCorona = TRUE //FALSE: currently the bulldozer blip is turned off.
							ENDIF
						
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBulldozerRouteStartPos, g_vAnyMeansLocate, bShowCorona)
							OR (IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh))
								vChosenRoutePos = vBulldozerRouteStartPos
								bPlayerReachedStartPoint = TRUE
							ENDIF
						ENDIF
						
						IF NOT DOES_BLIP_EXIST(blipFrontRoute)
							blipFrontRoute = CREATE_BLIP_FOR_COORD(vFrontRouteStartPos)
						ELSE
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vFrontRouteStartPos, g_vAnyMeansLocate, TRUE)
								vChosenRoutePos = vFrontRouteStartPos
								bPlayerReachedStartPoint = TRUE
							ENDIF
						ENDIF
						
						IF NOT DOES_BLIP_EXIST(blipRearRoute)
							blipRearRoute = CREATE_BLIP_FOR_COORD(vRearRouteStartPos)
						ELSE
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRearRouteStartPos, g_vAnyMeansLocate, TRUE)
								vChosenRoutePos = vRearRouteStartPos
								bPlayerReachedStartPoint = TRUE
							ENDIF
						ENDIF
						
						IF bPlayerReachedStartPoint	
						OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
							DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						
							IF NOT bHasTextLabelTriggered[LM2_PICK]
								TEXT_LABEL strLabel
								
								IF ARE_VECTORS_ALMOST_EQUAL(vChosenRoutePos, vRearRouteStartPos)
									strLabel = "LM2_PICK3"
								ELIF ARE_VECTORS_ALMOST_EQUAL(vChosenRoutePos, vFrontRouteStartPos)
									strLabel = "LM2_PICK1"
								ELSE
									strLabel = "LM2_PICK2"
								ENDIF
								
								IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
									IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
										SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
									ELSE
										PRINT_HELP_FOREVER("FRAN2_SIGHELP2")
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", strLabel, CONV_PRIORITY_MEDIUM)
										
										REPLAY_RECORD_BACK_FOR_TIME(5.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
										
										bHasTextLabelTriggered[LM2_PICK] = TRUE
									ELSE
										KILL_FACE_TO_FACE_CONVERSATION()
									ENDIF
								ENDIF
							ELSE
								CLEAR_HELP()
								DESTROY_ALL_CAMS()
								REMOVE_ALL_BLIPS()
									
								INFORM_MISSION_STATS_OF_INCREMENT(FRA2_SWITCHES)

								MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
								sSelectorCam.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
								iHealthBeforeSwitch = GET_ENTITY_HEALTH(PLAYER_PED_ID())				
								
								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed])	
									IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
										VECTOR vRot
										vRot = <<0.0, 0.0, GET_ENTITY_HEADING(sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed])>>
										NEW_LOAD_SCENE_START(GET_ENTITY_COORDS(sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]), CONVERT_ROTATION_TO_DIRECTION_VECTOR(vRot), 50.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
									ENDIF
								ENDIF
								
								iCurrentEvent++
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SIGHELP2")
								CLEAR_HELP()
							ENDIF
						
							IF NOT bHasTextLabelTriggered[LM2_WHERE]
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_WHERE", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_WHERE] = TRUE
									ENDIF
								ENDIF
							ELIF NOT bHasTextLabelTriggered[FRAN2_GETP]
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									PRINT_NOW("FRAN2_GETP", DEFAULT_GOD_TEXT_TIME, 0)
									bHasTextLabelTriggered[FRAN2_GETP] = TRUE
									iMainDialogueTimer = GET_GAME_TIMER()
								ENDIF
							ELIF NOT bHasTextLabelTriggered[LM2_MPATH]
								IF GET_GAME_TIMER() - iMainDialogueTimer > 4000
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MPATH", CONV_PRIORITY_MEDIUM)
											bHasTextLabelTriggered[LM2_MPATH] = TRUE
										ENDIF
									ENDIF
									
									IF NOT bHasTextLabelTriggered[FRAN2_POSHELP]
										PRINT_HELP("FRAN2_POSHELP")
										bHasTextLabelTriggered[FRAN2_POSHELP] = TRUE
									ENDIF
								ENDIF
							ELIF NOT bHasTextLabelTriggered[LM2_NOCLOSE]
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_NOCLOSE", CONV_PRIORITY_MEDIUM)
									bHasTextLabelTriggered[LM2_NOCLOSE] = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sFranklin.iHealth)
					BREAK
					
				    CASE 1 //Perform switch to Franklin			
						IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sSelectorCam, SWITCH_TYPE_SHORT)	
							NEW_LOAD_SCENE_STOP()
							
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							iCurrentEvent++
						ELSE
							IF sSelectorCam.bOKToSwitchPed
								IF NOT sSelectorCam.bPedSwitched
									IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)					
										IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
											IF iHealthBeforeSwitch < 110
												iHealthBeforeSwitch = 110
											ENDIF
										
									        SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)     
									        SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)
									        SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
											SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], RELGROUPHASH_PLAYER)
											
											SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 5, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
											SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_BLIND_FIRE_IN_COVER, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_DISABLE_PINNED_DOWN, TRUE)
											SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_RunFromFiresAndExplosions, FALSE)
											SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_UseKinematicModeWhenStationary, TRUE)
											SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_DisableHurt, TRUE)
											SET_ENTITY_PROOFS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE, TRUE, FALSE, FALSE, FALSE)
											SET_ENTITY_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], iHealthBeforeSwitch)
											SET_PED_MAX_HEALTH_WITH_SCALE(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
											SET_PED_USING_ACTION_MODE(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE, -1)
											SET_RAGDOLL_BLOCKING_FLAGS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 
																	   RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_FALLING | RBF_MELEE | RBF_IMPACT_OBJECT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_IMPACT)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
											SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
											
											//1897010 - Make sure Trevor has a suitable weapon after the switch.
											IF HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], WEAPONTYPE_CARBINERIFLE)
												SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], WEAPONTYPE_CARBINERIFLE, TRUE)
											
												IF GET_AMMO_IN_PED_WEAPON(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], WEAPONTYPE_CARBINERIFLE) < 50
													ADD_AMMO_TO_PED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], WEAPONTYPE_CARBINERIFLE, 50)
												ENDIF
											ENDIF
									    ENDIF
										
										//Now is a good time to reposition Michael
										IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
											SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vMichaelSnipePos, TRUE, TRUE)
											SET_PED_STEALTH_MOVEMENT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
										ENDIF
										
										CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
										
										SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
										SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
										CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_FALLING | RBF_MELEE | RBF_IMPACT_OBJECT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_IMPACT)
										
										REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
										REFRESH_BUDDY_PED_AUDIO_MIX_GROUP()
										
										eCurrentPlayer = sSelectorPeds.eNewSelectorPed
									
										sSelectorCam.bPedSwitched = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF eCurrentPlayer != SELECTOR_PED_FRANKLIN
								//If Trevor is on foot just have him face the right way.
								VECTOR vPos
								FLOAT fHeading, fHeadingFromDest, fHeadingDiff
								vPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) 
								fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
								fHeadingFromDest = GET_HEADING_FROM_VECTOR_2D(vLamarShootoutStartPos.x - vPos.x, vLamarShootoutStartPos.y - vPos.y)
								fHeadingDiff = ABSF(fHeadingFromDest - fHeading)
								
								IF fHeadingDiff > 180.0
									fHeadingDiff = ABSF(fHeadingDiff - 360.0)
								ENDIF
								
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_COORD) != PERFORMING_TASK
								AND fHeadingDiff > 20.0
									TASK_TURN_PED_TO_FACE_COORD(PLAYER_PED_ID(), vLamarShootoutStartPos)
								ENDIF
							ENDIF
						ENDIF
				    BREAK 
					
					CASE 2 //Get Franklin into position
						IF NOT DOES_BLIP_EXIST(blipBulldozerRoute)
							IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
							AND IS_VEHICLE_SEAT_FREE(sBulldozer.veh, VS_DRIVER)
								blipBulldozerRoute = CREATE_BLIP_FOR_ENTITY(sBulldozer.veh)
								SET_BLIP_COLOUR(blipBulldozerRoute, BLIP_COLOUR_YELLOW)
								SET_BLIP_BRIGHT(blipBulldozerRoute, FALSE)
							ELSE
								blipBulldozerRoute = CREATE_BLIP_FOR_COORD(vBulldozerRouteStartPos)
							ENDIF
						ELSE
							BOOL bShowCorona
							
							IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
							AND IS_VEHICLE_SEAT_FREE(sBulldozer.veh, VS_DRIVER)
								SET_BLIP_ALPHA(blipBulldozerRoute, 255)
							
								bShowCorona = TRUE
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh)
									DISABLE_PLAYER_VEHICLE_DRIVING_CONTROLS_THIS_FRAME()
								
									SET_BLIP_ALPHA(blipBulldozerRoute, 0)
								ELSE
									IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(blipBulldozerRoute), vBulldozerRouteStartPos, 0.1)
										REMOVE_BLIP(blipBulldozerRoute)
									ENDIF
								ENDIF
							
								bShowCorona = TRUE //FALSE
							ENDIF
						
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBulldozerRouteStartPos, g_vAnyMeansLocate, bShowCorona)
							OR (IS_VEHICLE_DRIVEABLE(sBulldozer.veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBulldozer.veh))
								bPlayerReachedStartPoint = TRUE
							ENDIF
						ENDIF
						
						IF NOT DOES_BLIP_EXIST(blipFrontRoute)
							blipFrontRoute = CREATE_BLIP_FOR_COORD(vFrontRouteStartPos)
						ELSE
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vFrontRouteStartPos, g_vAnyMeansLocate, TRUE)
								bPlayerReachedStartPoint = TRUE
							ENDIF
						ENDIF
						
						IF NOT DOES_BLIP_EXIST(blipRearRoute)
							blipRearRoute = CREATE_BLIP_FOR_COORD(vRearRouteStartPos)
						ELSE
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRearRouteStartPos, g_vAnyMeansLocate, TRUE)
								bPlayerReachedStartPoint = TRUE
							ENDIF
						ENDIF
						
						IF bPlayerReachedStartPoint
							DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
								CLEAR_HELP()
								REMOVE_ALL_BLIPS()
								
								RESET_BUDDY_ROUTES_FOR_SHOOTOUT_START()						
								
								TRIGGER_MUSIC_EVENT("FRA2_IN_POSITION")
								
								iTimePlayerGaveSignal = GET_GAME_TIMER()
								iCurrentEvent++
							ELSE
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP_FOREVER("FRAN2_SIGHELP")
								ENDIF
								
								IF NOT bHasTextLabelTriggered[LM2_PREBANG]
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_PREBANG", CONV_PRIORITY_MEDIUM)
											bHasTextLabelTriggered[LM2_PREBANG] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SIGHELP")
								CLEAR_HELP()
							ENDIF
						
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT bHasTextLabelTriggered[LM2_FRANPICK]
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_FRANPICK", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_FRANPICK] = TRUE
									ENDIF
								ELIF NOT bHasTextLabelTriggered[LM2_MIKEBANG]
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MIKEBANG", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_MIKEBANG] = TRUE
									ENDIF
								ELIF NOT bHasTextLabelTriggered[LM2_MIXT]
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MIXT", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_MIXT] = TRUE
									ENDIF
								ELIF NOT bHasTextLabelTriggered[LM2_MIXF]
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MIXF", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_MIXF] = TRUE
									ENDIF
								ELIF NOT bHasTextLabelTriggered[LM2_TREVSAME]
									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
										IF VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 400.0
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_TREVSAME", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_TREVSAME] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							//If the player chose the bulldozer route for Trevor then get him to get inside.
							IF ARE_VECTORS_ALMOST_EQUAL(vChosenRoutePos, vBulldozerRouteStartPos)
								IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
									IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sBullDozer.veh)
									AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
										TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sBulldozer.veh, -1, VS_DRIVER, PEDMOVE_RUN)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
										SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sBulldozer.veh, TRUE)
									ENDIF
								ENDIF
							ELSE
								//If Trevor is on foot just have him face the right way.
								VECTOR vPos
								FLOAT fHeading, fHeadingFromDest, fHeadingDiff
								vPos = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
								fHeading = GET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								fHeadingFromDest = GET_HEADING_FROM_VECTOR_2D(vLamarShootoutStartPos.x - vPos.x, vLamarShootoutStartPos.y - vPos.y)
								fHeadingDiff = ABSF(fHeadingFromDest - fHeading)
								
								IF fHeadingDiff > 180.0
									fHeadingDiff = ABSF(fHeadingDiff - 360.0)
								ENDIF
								
								IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_TURN_PED_TO_FACE_COORD) != PERFORMING_TASK
								AND fHeadingDiff > 20.0
									TASK_TURN_PED_TO_FACE_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vLamarShootoutStartPos)
								ENDIF
							ENDIF
						ENDIF
						
						HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sTrevor.iHealth)
					BREAK
					
					CASE 3 //Wait for the player to kick off.
						IF NOT bHasTextLabelTriggered[lm2_bang]
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "lm2_bang", CONV_PRIORITY_MEDIUM)
									iTimePlayerGaveSignal = GET_GAME_TIMER()
									
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									
									bHasTextLabelTriggered[lm2_bang] = TRUE
								ENDIF
							ENDIF
						ELSE
							//Have the buddies kick off here.
							IF g_bFranklin2RequestedBackup
								IF GET_GAME_TIMER() - iTimePlayerGaveSignal > 800
									UPDATE_SHOOTOUT_SWITCH()
									UPDATE_PLAYER_BUDDIES_DURING_SHOOTOUT()
									UPDATE_FIRST_PERSON_SNIPE_HELP_TEXT()
								ENDIF
								UPDATE_BUDDY_BLIPS_DURING_SHOOTOUT()
							ENDIF
						
							IF NOT bHasTextLabelTriggered[LM2_MATT]
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MATT", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_MATT] = TRUE
									ENDIF
								ENDIF
							ELIF NOT bHasTextLabelTriggered[LM2_TREVBANG]
								//If Trevor and Franklin picked the same route then play a different line
								IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN)), GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR))) < 625.0
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "lm2_bul", CONV_PRIORITY_MEDIUM)
											bHasTextLabelTriggered[LM2_TREVBANG] = TRUE
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_TREVBANG", CONV_PRIORITY_MEDIUM)
											bHasTextLabelTriggered[LM2_TREVBANG] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT bHasTextLabelTriggered[FRAN2_FIND]
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
										PRINT_NOW("FRAN2_FIND", DEFAULT_GOD_TEXT_TIME, 0)
										bHasTextLabelTriggered[FRAN2_FIND] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			//Fail if the player tries to leave.
			FLOAT fDistFromLamar = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vLamarShootoutStartPos)				
				
			IF fDistFromlamar > 90000.0
				MISSION_FAILED(FAILED_ABANDONED_LAMAR)
			ELIF fDistFromLamar > 50625.0
				IF NOT bHasTextLabelTriggered[FRAN2_RETSAW]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						PRINT_NOW("FRAN2_RETSAW", DEFAULT_GOD_TEXT_TIME, 0)
						bHasTextLabelTriggered[FRAN2_RETSAW] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Michael gets into position at the hill.
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			sMichael.vDest = vMichaelSnipePos
			
			//1476954 - Michael seems to change weapons after he's been knocked over, so make sure he still has the sniper rifle.
			IF HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER)
				WEAPON_TYPE eWeapon
				GET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], eWeapon)
				
				IF eWeapon != WEAPONTYPE_HEAVYSNIPER
					SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER)
				ENDIF
			
				IF GET_AMMO_IN_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER) < 5
					ADD_AMMO_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_HEAVYSNIPER, 5)
				ENDIF
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			AND VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), vMichaelSnipePos) > 4.0
				SEQUENCE_INDEX seq
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vMichaelSnipePos, PEDMOVE_RUN, -1, 0.5)
					TASK_AIM_GUN_AT_COORD(NULL, <<-573.5, 5257.8, 71.7>>, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
				CLEAR_SEQUENCE_TASK(seq)
				
				SET_PED_STEALTH_MOVEMENT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			ENDIF
			
			HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sMichael.iHealth)
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		REMOVE_ALL_BLIPS()
		CLEAR_HELP()
		NEW_LOAD_SCENE_STOP()
		
		//Warp Michael if he didn't make it to the start point
		If NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF (WOULD_ENTITY_BE_OCCLUDED(GET_ENTITY_MODEL(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), vMichaelSnipePos, FALSE) OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMichaelSnipePos) > 22500.0)
			AND (IS_ENTITY_OCCLUDED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])) > 22500.0)
				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vMichaelSnipePos)
				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], fMichaelSnipeHeading)
			ENDIF
		ENDIF
		
		KILL_FACE_TO_FACE_CONVERSATION()
		
		eMissionStage = STAGE_ATTACK_SAWMILL
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		JUMP_TO_STAGE(STAGE_ATTACK_SAWMILL)
	ENDIF
ENDPROC

PROC ATTACK_SAWMILL()
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		#IF IS_DEBUG_BUILD
			IF NOT bHasUsedCheckpoint
				g_bFranklin2RequestedBackup = TRUE
			ENDIF
		#ENDIF	
	
		IF SETUP_REQ_PLAYER_IS_FRANKLIN()
			IF bHasUsedCheckpoint
				START_REPLAY_SETUP(<<-591.8130, 5236.3486, 69.8739>>, 321.3476, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-591.8130, 5236.3486, 69.8739>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 321.3476)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			RESET_SHOOTOUT_VARIABLES()
			
			IF g_bFranklin2RequestedBackup
				WHILE NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				OR NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				OR NOT DOES_ENTITY_EXIST(vehMichaelsCar)
				OR NOT DOES_ENTITY_EXIST(vehTrevorsCar)
					SETUP_REQ_MICHAEL(vMichaelSnipePos, fMichaelSnipeHeading)
					SETUP_REQ_TREVOR(<<-487.0432, 5385.6133, 77.1299>>, 189.1107)  
					SETUP_REQ_MICHAELS_CAR(vMichaelsCarStartPos, fMichaelsCarStartHeading)
					SETUP_REQ_TREVORS_CAR(vTrevorsCarStartPos, fTrevorsCarStartHeading)
					
					WAIT(0)
				ENDWHILE
				
				SETUP_MICHAEL_OUTFIT()
				SETUP_TREVOR_OUTFIT()
			ENDIF	
			
			//Stream assets.
			WHILE NOT SETUP_REQ_REPLAY_CAR(vReplayCarPos, fReplayCarHeading)
			OR NOT DOES_ENTITY_EXIST(sBulldozer.veh)
			OR NOT DOES_ENTITY_EXIST(objShootoutCover[0])
			OR NOT sBallasStart[0].bIsCreated
			OR NOT bCreatedBallaStartCars
				UPDATE_SHOOTOUT_ENEMIES()
				SETUP_REQ_BULLDOZER(vBulldozerStartPos, fBulldozerStartHeading)
				SETUP_REQ_SHOOTOUT_COVER_OBJECTS()
				
				WAIT(0)
			ENDWHILE
			
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET, FALSE)
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SETTIMERA(0)
			
			WHILE TIMERA() < 5000
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]))
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))
					SETTIMERA(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
			ENDIF
			
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
			
			BLOCK_EMERGENCY_SERVICES_FOR_SHOOTOUT(TRUE)
			BLOCK_AMBIENT_PEDS_AND_VEHICLES_FOR_SHOOTOUT(TRUE)
			SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(TRUE)
			SETUP_REQ_BUDDIES_START_FIGHT()
			REFRESH_BUDDY_PED_AUDIO_MIX_GROUP()
			CREATE_SHOOTOUT_PICKUPS()
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			eCurrentPlayer = SELECTOR_PED_FRANKLIN
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		REQUEST_WAYPOINT_RECORDING(strWaypointBulldozerRoute)
		REQUEST_WAYPOINT_RECORDING(strWaypointFrontRoute)
		REQUEST_WAYPOINT_RECORDING(strWaypointRearRoute)
	
		IF GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointBulldozerRoute)
		AND GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointFrontRoute)
		AND GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointRearRoute)
			RESET_SHOOTOUT_VARIABLES()
		
			BLOCK_EMERGENCY_SERVICES_FOR_SHOOTOUT(TRUE)
			BLOCK_AMBIENT_PEDS_AND_VEHICLES_FOR_SHOOTOUT(TRUE)
			SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
			
			RESET_BUDDY_ROUTES_FOR_SHOOTOUT_START()
			
			IF IS_VEHICLE_DRIVEABLE(sBulldozer.veh)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sBulldozer.veh, TRUE)
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_STEALTH")
				STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_STEALTH")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_ALERT")
				START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_ALERT")
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), <<-591.8130, 5236.3486, 69.8739>>, -1, FALSE, 0, FALSE, FALSE, NULL)				
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
				
				TRIGGER_MUSIC_EVENT("FRA2_ATTACK_RT")
				
				WAIT(500)
				
				DO_FADE_IN_WITH_WAIT()
			ENDIF
			
			//TRIGGER_MUSIC_EVENT("FRA2_BULLDOZER")
			TRIGGER_MUSIC_EVENT("FRA2_ALERTED")

			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			SETTIMERB(0)
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING	
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ePlayerRoute = GET_CLOSEST_ROUTE_TO_PED(PLAYER_PED_ID())
	
		CHECK_ENEMY_TRIGGER_LOCATES(3)
		UPDATE_SHOOTOUT_ENEMIES()
		UPDATE_LAMAR_DURING_SHOOTOUT()
		
		IF g_bFranklin2RequestedBackup
			REQUEST_WAYPOINT_RECORDING(strWaypointBulldozerRoute)
			REQUEST_WAYPOINT_RECORDING(strWaypointFrontRoute)
			REQUEST_WAYPOINT_RECORDING(strWaypointRearRoute)
		
			UPDATE_SHOOTOUT_SWITCH()
			UPDATE_PLAYER_BUDDIES_DURING_SHOOTOUT()
			UPDATE_BUDDY_BLIPS_DURING_SHOOTOUT()
			UPDATE_BULLDOZER()
			UPDATE_FIRST_PERSON_SNIPE_HELP_TEXT()
		ENDIF

		IF bLamarHasBeenRescued
			eSectionStage = SECTION_STAGE_CLEANUP
		ELSE
			IF NOT bHasTextLabelTriggered[LM2_NOWAIT] //Play dialogue from Trevor if he triggered the shootout early.
				IF bTrevorTriggeredShootoutEarly
				AND g_bFranklin2RequestedBackup
				AND eCurrentPlayer = SELECTOR_PED_TREVOR
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND NOT IS_SELECTOR_CAM_ACTIVE()
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_NOWAIT", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[LM2_NOWAIT] = TRUE
						ENDIF
					ENDIF
				ELSE
					bHasTextLabelTriggered[LM2_NOWAIT] = TRUE
				ENDIF
			ELIF NOT bHasTextLabelTriggered[FRAN2_FIND] //Display god text to find lamar.
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT_NOW("FRAN2_FIND", DEFAULT_GOD_TEXT_TIME, 0)
					bHasTextLabelTriggered[FRAN2_FIND] = TRUE
				ENDIF
			ENDIF
			
			IF g_bFranklin2RequestedBackup
				//When the sniper/rocket ped shows up then flash Michael's HUD.
				IF NOT bMichaelHUDFlashingForRocket
					IF NOT IS_PED_INJURED(sBallasRocket[0].ped) 
					AND iNumTimesFiredRocket > 0
						IF iBallaRocketTimer = 0
							iBallaRocketTimer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - iBallaRocketTimer > 1500
							IF eCurrentPlayer != SELECTOR_PED_MICHAEL
								SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
								SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_MICHAEL, SELECTOR_PED_FRANKLIN, SELECTOR_PED_TREVOR)
								bMichaelHUDFlashingForRocket = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(sBallasRocket[0].ped) 
						//If Franklin and Trevor are together and waiting for the rocket ped to die then play a line.
						IF NOT bHasTextLabelTriggered[LM2_mrpg]
							IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR)), GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN))) < 900.0
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								AND NOT IS_SELECTOR_CAM_ACTIVE()
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_mrpg", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_mrpg] = TRUE
										iRocketDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF	
						ELIF NOT bHasTextLabelTriggered[LM2_deal] //If the ped still isn't dead then play another line.
							IF GET_GAME_TIMER() - iRocketDialogueTimer > 11000
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								AND NOT IS_SELECTOR_CAM_ACTIVE()
									IF eCurrentPlayer = SELECTOR_PED_MICHAEL
										IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR)), GET_ENTITY_COORDS(sBallasRocket[0].ped)) < VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN)), GET_ENTITY_COORDS(sBallasRocket[0].ped))
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_TSHOOTM", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_deal] = TRUE
											ENDIF
										ELSE
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_FSHOOTM", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_deal] = TRUE
											ENDIF
										ENDIF
									ELIF eCurrentPlayer = SELECTOR_PED_FRANKLIN
										IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR)), GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN))) < 900.0
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_DEALT", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_deal] = TRUE
											ENDIF
										ENDIF
									ELSE
										IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR)), GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN))) < 900.0
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_DEALF", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_deal] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_PED_INJURED(sBallasSniper[0].ped)
							SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
							bMichaelHUDFlashingForRocket = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				//Do the same for the sniper that turns up, but play different dialogue.
				IF NOT bMichaelHUDFlashingForSniper
					IF NOT IS_PED_INJURED(sBallasSniper[0].ped) 
						IF iTimeSniperStartedAttacking != 0
						AND GET_GAME_TIMER() - iTimeSniperStartedAttacking > 5000
							IF eCurrentPlayer != SELECTOR_PED_MICHAEL
								SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
								SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_MICHAEL, SELECTOR_PED_FRANKLIN, SELECTOR_PED_TREVOR)
								bMichaelHUDFlashingForSniper = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(sBallasSniper[0].ped) 
						//Get which player is closest to the sniper and have them play the line.
						IF NOT bHasTextLabelTriggered[LM2_SNIPER]
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							AND NOT IS_SELECTOR_CAM_ACTIVE()
								IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR)), GET_ENTITY_COORDS(sBallasSniper[0].ped)) < VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN)), GET_ENTITY_COORDS(sBallasSniper[0].ped))
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_SNIPERT", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_SNIPER] = TRUE
										iSniperDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_SNIPERF", CONV_PRIORITY_MEDIUM)
										bHasTextLabelTriggered[LM2_SNIPER] = TRUE
										iSniperDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ELIF NOT bHasTextLabelTriggered[LM2_deal2] //If the ped still isn't dead then play another line.
							IF GET_GAME_TIMER() - iSniperDialogueTimer > 11000
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								AND NOT IS_SELECTOR_CAM_ACTIVE()
									IF eCurrentPlayer = SELECTOR_PED_MICHAEL
										IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR)), GET_ENTITY_COORDS(sBallasSniper[0].ped)) < VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN)), GET_ENTITY_COORDS(sBallasSniper[0].ped))
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_TSHOOTM", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_deal2] = TRUE
											ENDIF
										ELSE
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_FSHOOTM", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_deal2] = TRUE
											ENDIF
										ENDIF
									ELIF eCurrentPlayer = SELECTOR_PED_FRANKLIN
										IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR)), GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN))) < 900.0
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_DEALT", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_deal2] = TRUE
											ENDIF
										ENDIF
									ELSE
										IF VDIST2(GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR)), GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN))) < 900.0
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_DEALF", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_deal2] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_PED_INJURED(sBallasRocket[0].ped)
							SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
							bMichaelHUDFlashingForRocket = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				//If both buddies reached Lamar and the player is Michael then prompt a switch.
				IF eCurrentPlayer = SELECTOR_PED_MICHAEL
				AND NOT IS_SELECTOR_CAM_ACTIVE()
					IF VDIST2(vLamarShootoutStartPos, GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR))) < 900.0
					AND VDIST2(vLamarShootoutStartPos, GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN))) < 900.0
					AND (GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasTimber2) = 0 OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasRearTimber2) = 0)
						IF NOT bHasTextLabelTriggered[FRAN2_SWITCH2]
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								PRINT_NOW("FRAN2_SWITCH2", DEFAULT_GOD_TEXT_TIME, 0)
							ENDIF
						ENDIF
						
						IF NOT bSwitchHUDFlashingToGetLamar
							SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, TRUE)
							SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
							SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_FRANKLIN, SELECTOR_PED_TREVOR, SELECTOR_PED_MICHAEL)
							
							bSwitchHUDFlashingToGetLamar = TRUE
						ENDIF
					ENDIF
										
					//CHECK_FOR_MICHAEL_ABILITY_HELP_TEXT()
				ELSE
					IF IS_THIS_PRINT_BEING_DISPLAYED("FRAN2_SWITCH2")
						CLEAR_PRINTS()
					ENDIF
					
					IF bHasTextLabelTriggered[FRAN2_SPECHELP]
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SPECHELP")
							CLEAR_HELP()
						ENDIF
						
						IF IS_PC_VERSION()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SPECHELP_KM")
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF
					
					IF bSwitchHUDFlashingToGetLamar
						SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
						SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
						
						bSwitchHUDFlashingToGetLamar = FALSE
					ENDIF
					
					iTimeSinceSwitchToMichael = GET_GAME_TIMER()
				ENDIF
			ENDIF
			
			//Fail for abandoning Lamar if leaving the area (not as michael).
			IF eCurrentPlayer != SELECTOR_PED_MICHAEL
				FLOAT fDistFromLamar = VDIST2(vPlayerPos, vLamarShootoutStartPos)				
					
				IF fDistFromlamar > 90000.0
					MISSION_FAILED(FAILED_ABANDONED_LAMAR)
				ELIF fDistFromLamar > 50625.0
					IF NOT bHasTextLabelTriggered[FRAN2_RETSAW]
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							PRINT_NOW("FRAN2_RETSAW", DEFAULT_GOD_TEXT_TIME, 0)
							bHasTextLabelTriggered[FRAN2_RETSAW] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sLamar.ped)
					VECTOR vLamarPos = GET_ENTITY_COORDS(sLamar.ped)
				
					IF VDIST2(vPlayerPos, vLamarPos) < 2500.0
					AND vPlayerPos.z > 79.0
						IF NOT IS_SELECTOR_CAM_ACTIVE()
							IF NOT DOES_BLIP_EXIST(sLamar.blip)
								sLamar.blip = CREATE_BLIP_FOR_ENTITY(sLamar.ped)
								SET_BLIP_SCALE(sLamar.blip, BLIP_SIZE_VEHICLE)
							ENDIF
						
							IF NOT bHasTextLabelTriggered[FRAN2_GETLAMAR]
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									TRIGGER_MUSIC_EVENT("FRA2_GET_TO_LAMAR")
								
									PRINT_NOW("FRAN2_GETLAMAR", DEFAULT_GOD_TEXT_TIME, 0)
									bHasTextLabelTriggered[FRAN2_GETLAMAR] = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						//Play some dialogue when the player sees Lamar.
						IF NOT bHasTextLabelTriggered[LM2_SPOTL]
						AND g_bFranklin2RequestedBackup
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							AND NOT IS_SELECTOR_CAM_ACTIVE()
								IF NOT IS_ENTITY_OCCLUDED(sLamar.ped)
								AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-527.930969,5309.885742,79.267578>>, <<-503.380615,5300.900879,82.347145>>, 3.500000)
									FLOAT fScreenX, fScreenY
									GET_SCREEN_COORD_FROM_WORLD_COORD(vLamarPos, fScreenX, fScreenY)
									
									IF fScreenX > 0.3 AND fScreenX < 0.7
									AND fScreenY > 0.3 AND fScreenY < 0.7
										IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_SPOTL", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_SPOTL] = TRUE
											ENDIF
										ELIF eCurrentPlayer = SELECTOR_PED_TREVOR
											IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_SPOTL2", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[LM2_SPOTL] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Have the player look at Lamar once going near him.
		IF NOT bPlayerGivenLookAtLamarTask
			IF NOT IS_PED_INJURED(sLamar.ped)
				IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sLamar.ped)) < 225.0
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sLamar.ped, 8000, SLF_WHILE_NOT_IN_FOV)
					bPlayerGivenLookAtLamarTask = TRUE
				ENDIF
			ENDIF	
		ELSE
			IF NOT IS_PED_INJURED(sLamar.ped)
				IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sLamar.ped)) > 400.0
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					bPlayerGivenLookAtLamarTask = FALSE
				ENDIF
			ENDIF	
		ENDIF
		
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			UPDATE_BUDDY_SHOOTOUT_DIALOGUE()
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		REMOVE_ALL_BLIPS()
		CLEAR_HELP()
		
		//Kill any of the start enemies that were dotted around originally.
		INT i
		REPEAT COUNT_OF(sBallasStart) i
			IF NOT IS_PED_INJURED(sBallasStart[i].ped)
				IF VDIST2(GET_ENTITY_COORDS(sBallasStart[i].ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 22500.0
				AND NOT IS_ENTITY_ON_SCREEN(sBallasStart[i].ped)
					SET_ENTITY_HEALTH(sBallasStart[i].ped, 0)
				ENDIF
			ENDIF
		ENDREPEAT
		
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
		CLEAR_SELECTOR_PED_PRIORITY(sSelectorPeds)
		
		eMissionStage = STAGE_GET_LAMAR_OUT
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		JUMP_TO_STAGE(STAGE_GET_LAMAR_OUT)
	ENDIF
ENDPROC

PROC GET_LAMAR_OUT()
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		#IF IS_DEBUG_BUILD
			IF NOT bHasUsedCheckpoint
				g_bFranklin2RequestedBackup = TRUE
			ENDIF
		#ENDIF	
	
		IF SETUP_REQ_PLAYER_IS_FRANKLIN()
			IF bHasUsedCheckpoint
				START_REPLAY_SETUP(<<-516.7184, 5305.6040, 79.2676>>, 248.7328, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-516.7184, 5305.6040, 79.2676>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 248.7328)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			RESET_SHOOTOUT_VARIABLES()
			
			IF g_bFranklin2RequestedBackup
				WHILE NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				OR NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				OR NOT DOES_ENTITY_EXIST(vehMichaelsCar)
				OR NOT DOES_ENTITY_EXIST(vehTrevorsCar)
					SETUP_REQ_MICHAEL(vMichaelSnipePos, fMichaelSnipeHeading)
					SETUP_REQ_TREVOR(<<-508.7989, 5305.9316, 79.2676>>, 250.0743)  
					SETUP_REQ_MICHAELS_CAR(vMichaelsCarStartPos, fMichaelsCarStartHeading)
					SETUP_REQ_TREVORS_CAR(vTrevorsCarStartPos, fTrevorsCarStartHeading)
					
					WAIT(0)
				ENDWHILE
				
				SETUP_MICHAEL_OUTFIT()
				SETUP_TREVOR_OUTFIT()
			ENDIF	
			
			//Stream assets.
			WHILE NOT SETUP_REQ_REPLAY_CAR(vReplayCarPos, fReplayCarHeading)
			OR NOT DOES_ENTITY_EXIST(sBulldozer.veh)
			OR NOT DOES_ENTITY_EXIST(sLamar.ped)
			OR NOT DOES_ENTITY_EXIST(objShootoutCover[0])
			OR NOT bCreatedBallaStartCars
				UPDATE_SHOOTOUT_ENEMIES()
				UPDATE_SHOOTOUT_ENEMIES_AFTER_RESCUING_LAMAR()
				SETUP_REQ_LAMAR(<<-521.4622, 5307.6079, 79.2676>>, 252.3208)
				SETUP_REQ_BULLDOZER(vBulldozerStartPos, fBulldozerStartHeading)
				SETUP_REQ_SHOOTOUT_COVER_OBJECTS()
				
				WAIT(0)
			ENDWHILE
			
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET, FALSE)
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SETTIMERA(0)
			
			WHILE TIMERA() < 10000
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]))
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))
					SETTIMERA(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
			ENDIF
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
			
			BLOCK_EMERGENCY_SERVICES_FOR_SHOOTOUT(TRUE)
			BLOCK_AMBIENT_PEDS_AND_VEHICLES_FOR_SHOOTOUT(TRUE)
			SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(TRUE)
			SETUP_REQ_BUDDIES_START_FIGHT()
			REPOSITION_VEHICLES_FOR_LAMAR_ESCAPE()
			REFRESH_BUDDY_PED_AUDIO_MIX_GROUP()
			CREATE_SHOOTOUT_PICKUPS()
			
			bHasTextLabelTriggered[lm2_lgof] = TRUE //Indicates that Franklin rescued Lamar.
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			eCurrentPlayer = SELECTOR_PED_FRANKLIN
			sLamar.iEvent = 50
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_ALERT")
			START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_ALERT")
		ENDIF
	
		IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_PROTECT_LAMAR")
			START_AUDIO_SCENE("FRANKLIN_2_PROTECT_LAMAR")
		ENDIF
	
		IF g_bFranklin2RequestedBackup
			//Warp the buddy closer to where Lamar was if they got stuck on their route.
			PED_INDEX pedBuddy
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
			ELSE
				pedBuddy = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			ENDIF
			
			IF NOT IS_PED_INJURED(pedBuddy)
				IF VDIST2(GET_ENTITY_COORDS(pedBuddy), vLamarShootoutStartPos) > 2500.0
					IF IS_ENTITY_OCCLUDED(pedBuddy)
						IF WOULD_ENTITY_BE_OCCLUDED(GET_ENTITY_MODEL(pedBuddy), <<-507.0433, 5308.2754, 79.2676>>)
							SET_ENTITY_COORDS(pedBuddy, <<-507.0433, 5308.2754, 79.2676>>)
						ELSE
							SET_ENTITY_COORDS(pedBuddy, <<-494.3257, 5324.5132, 79.5372>>)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			sTrevor.iEvent = 0
			sTrevor.bRefreshTasks = TRUE
			sFranklin.iEvent = 0
			sFranklin.bRefreshTasks = TRUE
		ENDIF
		
		IF DOES_BLIP_EXIST(sLamar.blip)
			REMOVE_BLIP(sLamar.blip)
		ENDIF
		
		//1979404 - Don't overwrite the replay checkpoint vehicle when hitting this checkpoint.
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_FRANKLIN2_GET_LAMAR_OUT, "GET_LAMAR_OUT", DEFAULT, DEFAULT, DEFAULT, FALSE)
		SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
		SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
		SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
	
		IF IS_SCREEN_FADED_OUT()
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
			
			TRIGGER_MUSIC_EVENT("FRA2_CUT_LAMAR_RT")
			
			WAIT(500)
			
			DO_FADE_IN_WITH_WAIT()
		ENDIF
		
		TRIGGER_MUSIC_EVENT("FRA2_GOT_LAMAR")

		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		SETTIMERB(0)
		SETTIMERA(0)
		iFinalSwitchTimer = 0
		eFinalSwitchType = SWITCH_TYPE_SHORT
		iTimeSinceLamarRescued = GET_GAME_TIMER()
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING	
		VECTOR vPlayerPos, vLamarPos, vMichaelPos
		
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF NOT IS_PED_INJURED(sLamar.ped)
			vLamarPos = GET_ENTITY_COORDS(sLamar.ped)
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			vMichaelPos = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ENDIF
		
		CHECK_ENEMY_TRIGGER_LOCATES()
		UPDATE_LAMAR_DURING_SHOOTOUT()
		UPDATE_SHOOTOUT_ENEMIES()
		UPDATE_SHOOTOUT_ENEMIES_AFTER_RESCUING_LAMAR()
		UPDATE_BULLDOZER()
		UPDATE_FIRST_PERSON_SNIPE_HELP_TEXT()
		
		IF NOT bHasTextLabelTriggered[LM2_snipe1]
			IF g_bFranklin2RequestedBackup
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_snipe1", CONV_PRIORITY_MEDIUM)
						bHasTextLabelTriggered[LM2_snipe1] = TRUE
					ENDIF
				ENDIF
			ELSE
				bHasTextLabelTriggered[LM2_snipe1] = TRUE
			ENDIF
		ELIF NOT bHasTextLabelTriggered[FRAN2_leadL]
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
				PRINT_NOW("FRAN2_leadL", DEFAULT_GOD_TEXT_TIME, 0)
				bHasTextLabelTriggered[FRAN2_leadL] = TRUE
			ENDIF
		ELIF NOT bHasTextLabelTriggered[LM2_LSEE]
			//Dialogue.
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND NOT IS_SELECTOR_CAM_ACTIVE()
			AND eCurrentPlayer != SELECTOR_PED_MICHAEL
			AND g_bFranklin2RequestedBackup
				//If the player rescued Lamar with buddies then have Lamar comment on the other buddy once they're in range.
				IF TIMERA() < 35000
				AND NOT IS_PED_INJURED(sLamar.ped)
					IF bHasTextLabelTriggered[lm2_lgof]
						IF VDIST2(vLamarPos, GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_TREVOR))) < 400.0
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_LSEET", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[LM2_LSEE] = TRUE
							ENDIF
						ENDIF
					ELIF bHasTextLabelTriggered[lm2_lgot]
						IF VDIST2(vLamarPos, GET_ENTITY_COORDS(GET_PED_INDEX(CHAR_FRANKLIN))) < 400.0
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_LSEEF", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[LM2_LSEE] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(sLamar.blip)
		AND NOT IS_PED_INJURED(sLamar.ped)
			sLamar.blip = CREATE_BLIP_FOR_ENTITY(sLamar.ped, FALSE)
		ENDIF
		
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		AND NOT IS_SELECTOR_CAM_ACTIVE()
			IF bHasTextLabelTriggered[FRAN2_leadL]
				UPDATE_BUDDY_SHOOTOUT_DIALOGUE()
				UPDATE_LAMAR_SHOOTOUT_DIALOGUE()
			ENDIF
		ENDIF
		
		//A group of enemies go up to attack Michael, if they get close flash Michael and play dialogue.
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF (NOT IS_PED_INJURED(sBallasPostRescueExit[3].ped) AND VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[3].ped), vMichaelPos) < 1600.0)
			OR (NOT IS_PED_INJURED(sBallasPostRescueExit[4].ped) AND VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[4].ped), vMichaelPos) < 1600.0)
			OR (NOT IS_PED_INJURED(sBallasPostRescueExit[5].ped) AND VDIST2(GET_ENTITY_COORDS(sBallasPostRescueExit[5].ped), vMichaelPos) < 1600.0)
				bMichaelIsGettingFlanked = TRUE
				bMichaelSetToMoveFreely = TRUE
			
				IF NOT bHasTextLabelTriggered[LM2_MIKEHURT1]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MIKEHURT", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[LM2_MIKEHURT1] = TRUE
							SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_MICHAEL)
							SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_MICHAEL, SELECTOR_PED_FRANKLIN, SELECTOR_PED_TREVOR)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bMichaelIsGettingFlanked
			IF IS_PED_INJURED(sBallasPostRescueExit[3].ped)
			AND IS_PED_INJURED(sBallasPostRescueExit[4].ped)
			AND IS_PED_INJURED(sBallasPostRescueExit[5].ped)
				SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
				
				bMichaelIsGettingFlanked = FALSE
			ENDIF
		ENDIF
		
		/*IF eCurrentPlayer = SELECTOR_PED_MICHAEL
		AND NOT IS_SELECTOR_CAM_ACTIVE()
			//CHECK_FOR_MICHAEL_ABILITY_HELP_TEXT()
		ELSE
			IF bHasTextLabelTriggered[FRAN2_SPECHELP]
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRAN2_SPECHELP")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF*/
		
		SWITCH iCurrentEvent
			CASE 0
				//This locate is used just for the corona.
				IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vSawmillExitPos, g_vAnyMeansLocate, TRUE)				
			
				BOOL bAllEnemiesAreDead
			
				IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasPostRescueExit) = 0 //Must have been created first.
				AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(sBallasAfterLamar3) = 0
				AND IS_PED_INJURED(sBallasRocket[0].ped) //Can be dead or never created.
				AND IS_PED_INJURED(sBallasSniper[0].ped) //Can be dead or never created.
					bAllEnemiesAreDead = TRUE
				ENDIF
			
				IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-565.169250,5272.239746,69.214165>>, <<-584.383545,5234.474121,78.220451>>, 20.000000) OR eCurrentPlayer = SELECTOR_PED_MICHAEL)
				AND bAllEnemiesAreDead
				AND NOT IS_SELECTOR_CAM_ACTIVE()
					IF DOES_BLIP_EXIST(blipCurrentObjective)
						REMOVE_BLIP(blipCurrentObjective)
					ENDIF
				
					IF eCurrentPlayer = SELECTOR_PED_FRANKLIN
						eSectionStage = SECTION_STAGE_CLEANUP
					ELSE
						IF iFinalSwitchTimer = 0
							iFinalSwitchTimer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - iFinalSwitchTimer > 1500
							/*IF NOT bHasTextLabelTriggered[FRAN2_SWFRANK]
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									PRINT_NOW("FRAN2_SWFRANK", DEFAULT_GOD_TEXT_TIME, 0)
									bHasTextLabelTriggered[FRAN2_SWFRANK] = TRUE
								ENDIF
							ENDIF
							
							SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, TRUE)
							SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_FRANKLIN, SELECTOR_PED_TREVOR, SELECTOR_PED_MICHAEL)
							SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
							SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
							
							IF UPDATE_SELECTOR_HUD(sSelectorPeds, FALSE)*/
								CLEAR_PRINTS()
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								KILL_FACE_TO_FACE_CONVERSATION()
							
								INFORM_MISSION_STATS_OF_INCREMENT(FRA2_SWITCHES)
								SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
								MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
								sSelectorCam.pedTo = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
								
								//Make sure Franklin is aiming in a good place for the switch.
								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
									TASK_AIM_GUN_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], <<-587.6, 5230.2, 71.4>>, -1, FALSE)
								ENDIF
								
								IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-489.725586,5408.617676,49.595184>>, <<-570.134766,5210.887207,122.609947>>, 188.750000)
									eFinalSwitchType = SWITCH_TYPE_AUTO
								ENDIF
								
								iCurrentEvent++
							//ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT DOES_BLIP_EXIST(blipCurrentObjective)
						blipCurrentObjective = CREATE_BLIP_FOR_COORD(vSawmillExitPos)
					ENDIF
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("FRAN2_SWFRANK")
						CLEAR_PRINTS()
					ENDIF
				
					IF g_bFranklin2RequestedBackup
						//1979130: Don't allow the player to switch if all the enemies are dead and they're already playing as Franklin.
						IF eCurrentPlayer != SELECTOR_PED_FRANKLIN
						OR NOT bAllEnemiesAreDead 
						OR IS_SELECTOR_CAM_ACTIVE()
							UPDATE_SHOOTOUT_SWITCH()
						ENDIF
						
						UPDATE_BUDDY_BLIPS_DURING_SHOOTOUT()
						UPDATE_PLAYER_BUDDIES_DURING_SHOOTOUT()
						
						
						//Display a different objective if playing as Michael
						IF eCurrentPlayer = SELECTOR_PED_MICHAEL
							IF IS_THIS_PRINT_BEING_DISPLAYED("FRAN2_leadL")
								CLEAR_PRINTS()
							ENDIF
						
							IF NOT bHasTextLabelTriggered[FRAN2_PROT]
							AND NOT bMichaelIsGettingFlanked
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								AND NOT IS_SELECTOR_CAM_ACTIVE()
									PRINT_NOW("FRAN2_PROT", DEFAULT_GOD_TEXT_TIME, 0)
									bHasTextLabelTriggered[FRAN2_PROT] = TRUE
								ENDIF
							ENDIF
						ELSE
							IF bHasTextLabelTriggered[FRAN2_PROT]
								IF IS_THIS_PRINT_BEING_DISPLAYED("FRAN2_PROT")
									CLEAR_PRINTS()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sSelectorCam, eFinalSwitchType)		
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					eSectionStage = SECTION_STAGE_CLEANUP
				ELSE
					IF sSelectorCam.bOKToSwitchPed
						IF NOT sSelectorCam.bPedSwitched
							IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)					
								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
									IF iHealthBeforeSwitch < 110
										iHealthBeforeSwitch = 110
									ENDIF
								
							        SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)     
							        SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)
							        SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
									SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], RELGROUPHASH_PLAYER)
									
									SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 5, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
									SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_BLIND_FIRE_IN_COVER, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_DISABLE_PINNED_DOWN, TRUE)
									SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_RunFromFiresAndExplosions, FALSE)
									SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_UseKinematicModeWhenStationary, TRUE)
									SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], PCF_DisableHurt, TRUE)
									SET_ENTITY_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], iHealthBeforeSwitch)
									SET_PED_MAX_HEALTH_WITH_SCALE(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
									SET_PED_USING_ACTION_MODE(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE, -1)
									SET_RAGDOLL_BLOCKING_FLAGS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 
															   RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_FALLING | RBF_MELEE | RBF_IMPACT_OBJECT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_IMPACT)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
							    ENDIF
								
								//TASK_AIM_GUN_AT_COORD(PLAYER_PED_ID(), <<-587.6, 5230.2, 71.4>>, -1, TRUE)
								
								SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
								SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
								CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_FALLING | RBF_MELEE | RBF_IMPACT_OBJECT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_IMPACT)
								
								REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
								REFRESH_BUDDY_PED_AUDIO_MIX_GROUP()
								
								eCurrentPlayer = sSelectorPeds.eNewSelectorPed
							
								sSelectorCam.bPedSwitched = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF VDIST2(vPlayerPos, vLamarPos) > 22500.0
			MISSION_FAILED(FAILED_ABANDONED_LAMAR)
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		REMOVE_ALL_BLIPS()
		CLEAR_HELP()
		REMOVE_ANIM_DICT(strLamarInjuredAnims)
		
		REMOVE_CLIP_SET("move_ped_strafing")
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ENDIF
		
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
		CLEAR_SELECTOR_PED_PRIORITY(sSelectorPeds)
		
		eMissionStage = STAGE_DRIVE_HOME
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		JUMP_TO_STAGE(STAGE_DRIVE_HOME)
	ENDIF
ENDPROC

PROC DRIVE_HOME()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		#IF IS_DEBUG_BUILD
			IF NOT bHasUsedCheckpoint
				g_bFranklin2RequestedBackup = TRUE
			ENDIF
		#ENDIF	
	
		IF SETUP_REQ_PLAYER_IS_FRANKLIN()
			IF bHasUsedCheckpoint
				START_REPLAY_SETUP(<<-577.2953, 5254.0166, 69.4656>>, 152.9580, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-577.2953, 5254.0166, 69.4656>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 152.9580)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			SETTIMERA(0)
			
			RESET_SHOOTOUT_VARIABLES()
			
			IF g_bFranklin2RequestedBackup
				WHILE NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				OR NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				OR NOT DOES_ENTITY_EXIST(vehMichaelsCar)
				OR NOT DOES_ENTITY_EXIST(vehTrevorsCar)
					SETUP_REQ_MICHAEL(vMichaelSnipePos, fMichaelSnipeHeading)
					SETUP_REQ_TREVOR(<<-568.2169, 5261.1172, 69.5043>>, 178.0029)  
					SETUP_REQ_MICHAELS_CAR(vMichaelsCarStartPos, fMichaelsCarStartHeading)
					SETUP_REQ_TREVORS_CAR(vTrevorsCarStartPos, fTrevorsCarStartHeading)
					
					WAIT(0)
				ENDWHILE
				
				SETUP_MICHAEL_OUTFIT()
				SETUP_TREVOR_OUTFIT()
			ENDIF	
			
			//Stream assets.
			WHILE NOT SETUP_REQ_REPLAY_CAR(vReplayCarPos, fReplayCarHeading)
			OR NOT DOES_ENTITY_EXIST(sLamar.ped)
			OR NOT bCreatedBallaStartCars
			OR NOT DOES_ENTITY_EXIST(objShootoutCover[0])
				UPDATE_SHOOTOUT_ENEMIES()
				SETUP_REQ_LAMAR(<<-573.1397, 5254.3828, 69.4683>>, 145.2989)
				SETUP_REQ_SHOOTOUT_COVER_OBJECTS()
				
				WAIT(0)
			ENDWHILE
			
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET, FALSE)
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SETTIMERA(0)
			
			WHILE TIMERA() < 10000
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]))
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
				AND ((NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) 
				OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))
					SETTIMERA(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE			
		
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			
			BLOCK_EMERGENCY_SERVICES_FOR_SHOOTOUT(TRUE)
			BLOCK_AMBIENT_PEDS_AND_VEHICLES_FOR_SHOOTOUT(TRUE)
			SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(TRUE)
			SETUP_REQ_BUDDIES_START_FIGHT()
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			eCurrentPlayer = SELECTOR_PED_FRANKLIN
			sLamar.iEvent = 50
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		bEscapedSawmill = FALSE
	
		IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_ALERT")
			STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_ALERT")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_PROTECT_LAMAR")
			STOP_AUDIO_SCENE("FRANKLIN_2_PROTECT_LAMAR")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
			STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
			STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sBulldozer.veh)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sBulldozer.veh)
		ENDIF
	
		IF DOES_BLIP_EXIST(sMichael.blip)
			REMOVE_BLIP(sMichael.blip)
		ENDIF
		
		IF DOES_BLIP_EXIST(sTrevor.blip)
			REMOVE_BLIP(sTrevor.blip)
		ENDIF
	
		//Reset Lamar post-shootout.
		IF NOT IS_PED_INJURED(sLamar.ped)
			CLEAR_PED_TASKS(sLamar.ped)
		
			IF NOT IS_PED_GROUP_MEMBER(sLamar.ped, PLAYER_GROUP_ID())
				SET_PED_AS_GROUP_MEMBER(sLamar.ped, PLAYER_GROUP_ID())
			ENDIF
		
			SET_RAGDOLL_BLOCKING_FLAGS(sLamar.ped, RBF_NONE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sLamar.ped, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sLamar.ped, FALSE)
			SET_ENTITY_PROOFS(sLamar.ped, FALSE, FALSE, FALSE, FALSE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(sLamar.ped, CA_DO_DRIVEBYS, TRUE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sLamar.ped, FALSE)
			SET_ENTITY_PROOFS(sLamar.ped, FALSE, FALSE, FALSE, FALSE, FALSE)
			SET_PED_ALLOWED_TO_DUCK(sLamar.ped, TRUE)
			SET_PED_PATH_CAN_USE_CLIMBOVERS(sLamar.ped, TRUE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_UseKinematicModeWhenStationary, FALSE)
			SET_PED_NEVER_LEAVES_GROUP(sLamar.ped, FALSE)
		ENDIF
	
		//1979404 - Don't overwrite the replay checkpoint vehicle when hitting this checkpoint.
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_FRANKLIN2_FLEE_AREA, "FLEE_AREA", TRUE, DEFAULT, DEFAULT, FALSE)
	
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
		//Unlock the player cars 
		//(NOTE: this should only be done if the buddies aren't going to use them. Currently they hang back at the sawmill, but in the past they've used the vehicles to drive off).
		IF IS_VEHICLE_DRIVEABLE(vehMichaelsCar)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMichaelsCar, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehMichaelsCar, VEHICLELOCK_UNLOCKED)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrevorsCar)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevorsCar, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehTrevorsCar, VEHICLELOCK_UNLOCKED)
		ENDIF
		
		//Reset collision flags on buddies as you can no longer switch to them.
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		ENDIF
	
		IF IS_SCREEN_FADED_OUT()
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
		
			IF NOT IS_PED_INJURED(sLamar.ped)
				SET_PED_USING_ACTION_MODE(sLamar.ped, TRUE, -1)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_USING_ACTION_MODE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, -1)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_PED_USING_ACTION_MODE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, -1)
			ENDIF
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			TRIGGER_MUSIC_EVENT("FRA2_FLEE_RT")
			
			GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
			
			WAIT(1000) //Wait a bit to allow action mode to kick in.
			
			DO_FADE_IN_WITH_WAIT()
		ELSE
			TRIGGER_MUSIC_EVENT("FRA2_RETURN_LAMAR")
		ENDIF
		
		BLOCK_ROADS_AT_SAWMILL(FALSE)

		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		SETTIMERB(0)
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING	
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
		#IF IS_DEBUG_BUILD 
			DONT_DO_J_SKIP(sLocatesData) 
		#ENDIF
		
		IF DOES_BLIP_EXIST(sLamar.blip)
			REMOVE_ALL_BLIPS()
		ENDIF
		
		SWITCH iCurrentEvent
			CASE 0
				//If Michael and Trevor are here play dialogue saying that Franklin will take Lamar home.
				IF g_bFranklin2RequestedBackup
					IF NOT bHasTextLabelTriggered[LM2_TAKE] //Franklin says he'll take Lamar.
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_TAKE", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[LM2_TAKE] = TRUE
							ENDIF
						ENDIF
					ELIF NOT bHasTextLabelTriggered[LM2_MGOING] //Michael says he's going.
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_MGOING", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[LM2_MGOING] = TRUE
							ENDIF
						ENDIF
					ELIF NOT bHasTextLabelTriggered[LM2_LBYEM] //Lamar says bye to Michael.
						IF NOT IS_PED_INJURED(sLamar.ped)
							IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sLamar.ped)) < 400.0
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_LBYEM", CONV_PRIORITY_MEDIUM)
										
										REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
										
										bHasTextLabelTriggered[LM2_LBYEM] = TRUE
									ENDIF
								ENDIF
							ELSE
								bHasTextLabelTriggered[LM2_LBYEM] = TRUE
							ENDIF
						ENDIF
					ELIF NOT bHasTextLabelTriggered[LM2_tbye] //Trevor says he's going.
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sLamar.ped)) < 400.0
								IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 625.0
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_tbye", CONV_PRIORITY_MEDIUM)
											
											REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
											
											bHasTextLabelTriggered[LM2_tbye] = TRUE
										ENDIF
									ENDIF
								ELSE
									bHasTextLabelTriggered[LM2_tbye] = TRUE
									bHasTextLabelTriggered[LM2_LBYET] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT bHasTextLabelTriggered[LM2_LBYET] //Lamar says bye to Trevor.
						IF NOT IS_PED_INJURED(sLamar.ped)
							IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sLamar.ped)) < 625.0
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_LBYET", CONV_PRIORITY_MEDIUM)
										
										REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
										
										bHasTextLabelTriggered[LM2_LBYET] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				//If the player is in a car and Lamar isn't then play a line telling him to get in.
				IF NOT bHasTextLabelTriggered[lm2_leave]
				AND NOT bHasTextLabelTriggered[LM2_BANT1]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						AND NOT IS_PED_IN_ANY_VEHICLE(sLamar.ped)
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "lm2_leave", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[lm2_leave] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(sLocatesData, vGoHomePos, g_vAnyMeansLocate, TRUE, sLamar.ped, "FRAN2_getCar", "FRAN2_drvL", FALSE, TRUE)
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					ENDIF
					
					KILL_FACE_TO_FACE_CONVERSATION()
							
					IF IS_MOBILE_PHONE_CALL_ONGOING()
						HANG_UP_AND_PUT_AWAY_PHONE()
					ENDIF
					
					//1984722 - Player is able to get a wanted level after hitting the locate.
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_MAX_WANTED_LEVEL(0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					
					iCurrentEvent++
				ENDIF

				//Dialogue after losing the cops: locates god text takes priority so check this after checking the locate.
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				AND NOT IS_PED_INJURED(sLamar.ped)
				AND ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), sLamar.ped)
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					ENDIF
					
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT bHasTextLabelTriggered[LM2_BANT1]
							IF bEscapedSawmill
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_BANT1", CONV_PRIORITY_MEDIUM)
									SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
									SET_PED_USING_ACTION_MODE(sLamar.ped, FALSE)
								
									bHasTextLabelTriggered[LM2_BANT1] = TRUE
								ENDIF
							ENDIF
						ELIF NOT bHasTextLabelTriggered[LM2_BANT2]
							IF g_bFranklin2RequestedBackup
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_BANT4", CONV_PRIORITY_MEDIUM)
									bHasTextLabelTriggered[LM2_BANT2] = TRUE
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_BANT5", CONV_PRIORITY_MEDIUM)
									bHasTextLabelTriggered[LM2_BANT2] = TRUE
								ENDIF
							ENDIF
						ELIF NOT bHasTextLabelTriggered[LM2_BANT6]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_BANT6", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[LM2_BANT6] = TRUE
							ENDIF
						ELIF NOT bHasTextLabelTriggered[LM2_BANT6b]
							IF CREATE_CONVERSATION(sConversationPeds, "LEM2AUD", "LM2_BANT6b", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[LM2_BANT6b] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF bHasTextLabelTriggered[LM2_BANT1]
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				BOOL bHasStopped
				VEHICLE_INDEX vehFinalCar
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehFinalCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehFinalCar)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
				
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehFinalCar, DEFAULT, 5)
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
						ENDIF
						bHasStopped = TRUE
					ENDIF
				ELSE
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE)
					ENDIF
					bHasStopped = TRUE
				ENDIF

				IF bHasStopped
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
					eSectionStage = SECTION_STAGE_CLEANUP
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_PED_INJURED(sLamar.ped)
			VECTOR vLamarPos = GET_ENTITY_COORDS(sLamar.ped)
		
			//Check if the player escaped the sawmill:
			// - Player must be at least 30m away from the mill.
			// - Player must be at least 100m away from any Balla peds.
			IF NOT bEscapedSawmill
				VECTOR vMillCentre = <<-547.2, 5314.1, 84.7>>
				
				PREPARE_MUSIC_EVENT("FRA2_END_VEHICLE")
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				//Play audio scene if the player is in a car
				IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_ESCAPE_IN_VEHICLE")
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						START_AUDIO_SCENE("FRANKLIN_2_ESCAPE_IN_VEHICLE")
					ENDIF
				ELSE
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						STOP_AUDIO_SCENE("FRANKLIN_2_ESCAPE_IN_VEHICLE")
					ENDIF
				ENDIF
				
				IF VDIST2(vPlayerPos, vSawmillExitPos) > 900.0
				AND VDIST2(vPlayerPos, vMillCentre) > VDIST2(vSawmillExitPos, vMillCentre)
					IF VDIST2(vPlayerPos, vLamarPos) < 900.0
						PED_INDEX pedNearest[5]
						
						GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedNearest)
						
						INT i = 0
						BOOL bEnemyIsNearby = FALSE
						REPEAT COUNT_OF(pedNearest) i
							IF NOT IS_PED_INJURED(pedNearest[i])
								IF GET_ENTITY_MODEL(pedNearest[i]) = modelBalla
									IF VDIST2(GET_ENTITY_COORDS(pedNearest[i]), vPlayerPos) < 10000.0
										bEnemyIsNearby = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						
						IF NOT bEnemyIsNearby
							IF ARE_CHARS_IN_SAME_VEHICLE(sLamar.ped, PLAYER_PED_ID())
								TRIGGER_MUSIC_EVENT("FRA2_END_VEHICLE")
							ELSE
								CANCEL_ALL_PREPARED_MUSIC_EVENTS()
								TRIGGER_MUSIC_EVENT("FRA2_END_ON_FOOT")
							ENDIF
							
							IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_ESCAPE_IN_VEHICLE")
								STOP_AUDIO_SCENE("FRANKLIN_2_ESCAPE_IN_VEHICLE")
							ENDIF
							
							IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_GO_TO_FRANKLINS")
								START_AUDIO_SCENE("FRANKLIN_2_GO_TO_FRANKLINS")
							ENDIF
							
							REMOVE_ALL_SHOOTOUT_PEDS()
							REMOVE_ALL_SHOOTOUT_VEHICLES()
							REMOVE_ALL_SHOOTOUT_OBJECTS()
							
							//Remove all assets from the shootout.
							SET_MODEL_AS_NO_LONGER_NEEDED(modelBalla)
							SET_MODEL_AS_NO_LONGER_NEEDED(modelBallaReinforcementCar)
							SET_MODEL_AS_NO_LONGER_NEEDED(modelBallaStartCars)
							REMOVE_VEHICLE_ASSET(modelBallaReinforcementCar)
							REMOVE_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
							REMOVE_WEAPON_ASSET(WEAPONTYPE_SMG)
							REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
						
							BLOCK_EMERGENCY_SERVICES_FOR_SHOOTOUT(FALSE)
							BLOCK_AMBIENT_PEDS_AND_VEHICLES_FOR_SHOOTOUT(FALSE)
							SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(FALSE)
						
							bEscapedSawmill = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//Remove the other player peds once they're far away.
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)) > 40000.0
						REMOVE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehMichaelsCar)
					IF (VDIST2(vPlayerPos, GET_ENTITY_COORDS(vehMichaelsCar, FALSE)) > 40000.0 AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					OR VDIST2(vPlayerPos, GET_ENTITY_COORDS(vehMichaelsCar, FALSE)) > 160000.0
						REMOVE_VEHICLE(vehMichaelsCar, TRUE)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)) > 40000.0
						REMOVE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehTrevorsCar)
					IF (VDIST2(vPlayerPos, GET_ENTITY_COORDS(vehTrevorsCar, FALSE)) > 40000.0 AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					OR VDIST2(vPlayerPos, GET_ENTITY_COORDS(vehTrevorsCar, FALSE)) > 160000.0
						REMOVE_VEHICLE(vehTrevorsCar, TRUE)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehReplayCar)
					IF (VDIST2(vPlayerPos, GET_ENTITY_COORDS(vehReplayCar, FALSE)) > 40000.0 AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					OR VDIST2(vPlayerPos, GET_ENTITY_COORDS(vehReplayCar, FALSE)) > 160000.0
						REMOVE_VEHICLE(vehReplayCar, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			//Fail for leaving Lamar behind.
			IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF VDIST2(vPlayerPos, vLamarPos) > 22500.0
					MISSION_FAILED(FAILED_ABANDONED_LAMAR)
				ENDIF
			ENDIF
		ENDIF
		
		//Michael behaviour: he leaves in his car.
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			//New behaviour: ped stays at the sawmill (1516070).
			IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
				IF VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), <<-587.9527, 5181.2993, 94.6509>>) > 4.0
				AND NOT IS_AREA_OCCUPIED(<<-587.9527, 5181.2993, 94.6509>> - <<1.0, 1.0, 1.0>>, <<-587.9527, 5181.2993, 94.6509>> + <<1.0, 1.0, 1.0>>, FALSE, TRUE, FALSE, FALSE, FALSE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], <<-587.9527, 5181.2993, 94.6509>>, PEDMOVE_RUN, -1, DEFAULT, DEFAULT, 186.8357)
					TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_UseKinematicModeWhenStationary, TRUE)
				ENDIF
			ENDIF
		
			//Old behaviour: gets into car and drives off.
			/*IF IS_VEHICLE_DRIVEABLE(vehMichaelsCar)
				IF IS_PED_SITTING_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehMichaelsCar)
					IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
						TASK_VEHICLE_MISSION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehMichaelsCar, NULL, MISSION_CRUISE, 20.0, DRIVINGMODE_STOPFORCARS, 2.0, 2.0)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					ENDIF
				ELSE
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehMichaelsCar)
					AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
						TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehMichaelsCar, 120000)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					ENDIF
				ENDIF
			ELSE
				//Alternate behaviour (currently not needed as we fail for destroying the car).
			ENDIF*/
		ENDIF
		
		//Trevor behaviour: he leaves in his car.
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			//New behaviour: ped stays at the sawmill (1516070).
			IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
				IF VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]), <<-575.8129, 5271.7529, 69.2686>>) > 4.0
				AND NOT IS_AREA_OCCUPIED(<<-575.8129, 5271.7529, 69.2686>> - <<0.5, 0.5, 1.0>>, <<-575.8129, 5271.7529, 69.2686>> + <<0.5, 0.5, 1.0>>, FALSE, TRUE, FALSE, FALSE, FALSE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<-575.8129, 5271.7529, 69.2686>>, PEDMOVE_RUN, -1, DEFAULT, DEFAULT, 204.2659)
					TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
					SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_UseKinematicModeWhenStationary, TRUE)
				ENDIF
			ENDIF
		
			//Old behaviour: gets into car and drives off.
			/*IF IS_VEHICLE_DRIVEABLE(vehTrevorsCar)
				IF IS_PED_SITTING_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorsCar)
					IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
						TASK_VEHICLE_MISSION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorsCar, NULL, MISSION_CRUISE, 20.0, DRIVINGMODE_STOPFORCARS, 2.0, 2.0)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
					ENDIF
				ELSE
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorsCar)
					AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
						TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorsCar, 120000)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
					ENDIF
				ENDIF
			ELSE
				//Alternate behaviour (currently not needed as we fail for destroying the car).
			ENDIF*/
		ENDIF
		
		//Request mocap cutscene in advance.
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGoHomePos) < 15625.0
			REQUEST_CUTSCENE("FRA_2_EXT")
			SET_CUTSCENE_PED_COMPONENT_VARIATIONS("FRA_2_EXT")
		ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGoHomePos) > 30625.0
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		REMOVE_ALL_BLIPS()
		CLEAR_PRINTS()
		CLEAR_HELP()
		KILL_FACE_TO_FACE_CONVERSATION()
		
		REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
		
		eMissionStage = STAGE_OUTRO_CUTSCENE
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		JUMP_TO_STAGE(STAGE_OUTRO_CUTSCENE)
	ENDIF
ENDPROC

PROC DO_OUTRO_CUTSCENE()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF SETUP_REQ_PLAYER_IS_FRANKLIN()
			IF bHasUsedCheckpoint
				START_REPLAY_SETUP(vGoHomePos, 0.0, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vGoHomePos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			WHILE NOT DOES_ENTITY_EXIST(sLamar.ped)
				SETUP_REQ_LAMAR(<<-72.3909, -1461.0822, 31.1151>>, 323.4056)
				
				WAIT(0)
			ENDWHILE
			
			SETTIMERA(0)
			
			//Stream assets.			
			WHILE TIMERA() < 10000
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				AND ((NOT IS_PED_INJURED(sLamar.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sLamar.ped)) 
				OR IS_PED_INJURED(sLamar.ped))
					SETTIMERA(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 300.0, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		REQUEST_CUTSCENE("FRA_2_EXT")
		SET_CUTSCENE_PED_COMPONENT_VARIATIONS("FRA_2_EXT")
	
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			START_CUTSCENE()
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

			IF NOT IS_PED_INJURED(sLamar.ped)
				REGISTER_ENTITY_FOR_CUTSCENE(sLamar.ped, "Lamar", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)

			iCurrentEvent = 0
			bSkippedMocap = FALSE
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING
		SWITCH iCurrentEvent
			CASE 0 //Do any setup on first frame of cutscene.
				IF IS_CUTSCENE_PLAYING()
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
					IF IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_GO_TO_FRANKLINS")
						STOP_AUDIO_SCENE("FRANKLIN_2_GO_TO_FRANKLINS")
					ENDIF
					
					//Resolve vehicles
					//Move the last vehicle somewhere if it's in the way.
					vehFinalCutsceneCar = GET_PLAYERS_LAST_VEHICLE()
					
					IF IS_VEHICLE_DRIVEABLE(vehFinalCutsceneCar)
						SET_VEH_RADIO_STATION(vehFinalCutsceneCar, "OFF")
						SET_VEHICLE_ENGINE_ON(vehFinalCutsceneCar, FALSE, FALSE)
					
						IF IS_ENTITY_AT_COORD(vehFinalCutsceneCar, vGoHomePos, <<20.0, 20.0, 10.0>>)
						AND GET_ENTITY_MODEL(vehFinalCutsceneCar) != GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
							PED_INDEX pedDriver 
							IF NOT IS_VEHICLE_SEAT_FREE(vehFinalCutsceneCar)
								pedDriver = GET_PED_IN_VEHICLE_SEAT(vehFinalCutsceneCar)
							ENDIF
							
							//Don't warp if we arrived as a passenger (e.g. in a taxi ride)
							IF pedDriver = NULL OR pedDriver = PLAYER_PED_ID()
								SET_ENTITY_AS_MISSION_ENTITY(vehFinalCutsceneCar)
								SET_ENTITY_HEADING(vehFinalCutsceneCar, 135.3133)
								SET_ENTITY_COORDS(vehFinalCutsceneCar, <<-77.4702, -1472.7953, 31.0564>>)
								SET_VEHICLE_DOORS_SHUT(vehFinalCutsceneCar)
								SET_VEHICLE_ENGINE_ON(vehFinalCutsceneCar, FALSE, FALSE)
								SET_VEHICLE_LIGHTS(vehFinalCutsceneCar, FORCE_VEHICLE_LIGHTS_OFF)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(sLamar.ped)
						SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_BERD, 0, 0)
					ENDIF
					
					REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
					
					CLEAR_AREA(vGoHomePos, 200.0, TRUE)
				
					REMOVE_PED_COMP_ITEM_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
				
					DO_FADE_IN_WITH_WAIT() //In case this is a mission replay, the game will be faded out at this point
					
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 1
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			
				IF NOT IS_CUTSCENE_ACTIVE()
					SETTIMERB(0)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					iCurrentEvent++
				ENDIF	
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				ENDIF
			BREAK
			
			CASE 2
				IF TIMERB() < 500
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
				ENDIF
				
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
			
				IF TIMERB() > 4000
				OR IS_SCREEN_FADED_OUT()
					eSectionStage = SECTION_STAGE_CLEANUP
				ENDIF
			BREAK
		ENDSWITCH
		
		IF WAS_CUTSCENE_SKIPPED()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			eSectionStage = SECTION_STAGE_SKIP
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 0.0)
		ENDIF
		
		IF NOT IS_PED_INJURED(sLamar.ped)
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lamar")
				SET_ENTITY_COORDS(sLamar.ped, <<-72.3909, -1461.0822, 31.1151>>)
				SET_ENTITY_HEADING(sLamar.ped, 323.4056)
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(sLamar.ped, <<-83.6490, -1437.1771, 30.3611>>, PEDMOVE_WALK, -1, DEFAULT, ENAV_NO_STOPPING)
				
				SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_BERD, 1, 0)
			ENDIF
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	
		REMOVE_VEHICLE(vehFinalCutsceneCar, FALSE)
		REMOVE_PED(sLamar.ped, TRUE)
	
		DO_FADE_IN_WITH_WAIT()
	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
		IF GET_ACCOUNT_BALANCE(BANK_ACCOUNT_FRANKLIN) > 50
			DEBIT_BANK_ACCOUNT(CHAR_FRANKLIN,BAAC_UNLOGGED_SMALL_ACTION,50)
		ENDIF
		
		REPLAY_STOP_EVENT()
	
		MISSION_PASSED()
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		STOP_CUTSCENE()
		REMOVE_CUTSCENE()
		
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

///Fail checks that are relevant across the entire mission or over a number of stages.
PROC DO_GLOBAL_FAIL_CHECKS()
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
	AND IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		MISSION_FAILED(FAILED_FRANKLIN_DIED)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	AND IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		MISSION_FAILED(FAILED_MICHAEL_DIED)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	AND IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		MISSION_FAILED(FAILED_TREVOR_DIED)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sLamar.ped)
	AND IS_PED_INJURED(sLamar.ped)
		MISSION_FAILED(FAILED_LAMAR_DIED)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehMichaelsCar)
	AND NOT IS_VEHICLE_DRIVEABLE(vehMichaelsCar)
		MISSION_FAILED(FAILED_MICHAELS_CAR_DESTROYED)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTrevorsCar)
	AND NOT IS_VEHICLE_DRIVEABLE(vehTrevorsCar)
		MISSION_FAILED(FAILED_TREVORS_CAR_DESTROYED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks stats that are applicable across multiple mission stages.
PROC DO_GLOBAL_STAT_CHECKS()	
	IF eSectionStage = SECTION_STAGE_RUNNING
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), FRA2_DAMAGE)
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			//Track the vehicle speed/damage stat.
			VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF NOT IS_ENTITY_DEAD(veh)				
				IF NOT DOES_ENTITY_EXIST(g_MissionStatSingleDamageWatchEntity)
				OR NOT IS_ENTITY_A_VEHICLE(g_MissionStatSingleDamageWatchEntity)
				OR (IS_ENTITY_A_VEHICLE(g_MissionStatSingleDamageWatchEntity) AND GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(g_MissionStatSingleDamageWatchEntity) != veh)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(veh, FRA2_CAR_DAMAGE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(g_MissionStatSingleSpeedWatchEntity)
				OR NOT IS_ENTITY_A_VEHICLE(g_MissionStatSingleSpeedWatchEntity)
				OR (IS_ENTITY_A_VEHICLE(g_MissionStatSingleSpeedWatchEntity) AND GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(g_MissionStatSingleSpeedWatchEntity) != veh)
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(veh)
				ENDIF
			ENDIF
		ELSE
			//Track the player damage stat.
			IF NOT DOES_ENTITY_EXIST(g_MissionStatSingleDamageWatchEntity)
			OR NOT IS_ENTITY_A_PED(g_MissionStatSingleDamageWatchEntity)
			OR (IS_ENTITY_A_PED(g_MissionStatSingleDamageWatchEntity) AND GET_PED_INDEX_FROM_ENTITY_INDEX(g_MissionStatSingleDamageWatchEntity) != PLAYER_PED_ID())
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), FRA2_DAMAGE)
			ENDIF
			
			//Remove any vehicles from the speed tracking stat.
			IF g_MissionStatSingleSpeedWatchEntity != NULL
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD		
	PROC UPDATE_WIDGETS()
		iDebugMichaelRoute = ENUM_TO_INT(eMichaelRoute)
		iDebugFranklinRoute = ENUM_TO_INT(eFranklinRoute)
		iDebugTrevorRoute = ENUM_TO_INT(eTrevorRoute)
		iDebugPlayerRoute = ENUM_TO_INT(ePlayerRoute)
	ENDPROC
	
	PROC DO_DEBUG()
		UPDATE_WIDGETS()
		
		//Reset any skipping from the previous frame
		IF eSectionStage = SECTION_STAGE_SKIP
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_RUNNING
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				PRINTLN("Franklin2.sc - Player S-passed the mission.")
			
				MISSION_PASSED(TRUE)
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				PRINTLN("Franklin2.sc - Player F-failed the mission.")
			
				MISSION_FAILED(FAILED_GENERIC)
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				PRINTLN("Franklin2.sc - Player J-skipped.")
			
				eSectionStage = SECTION_STAGE_SKIP
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				PRINTLN("Franklin2.sc - Player P-skipped.")
			
				//Work out which stage we want to reach based on the current stage
				INT iCurrentStage = ENUM_TO_INT(eMissionStage)
				
				IF iCurrentStage > 0	
					MISSION_STAGE eStage = INT_TO_ENUM(MISSION_STAGE, iCurrentStage - 1)
					
					JUMP_TO_STAGE(eStage, TRUE)
				ELSE
					JUMP_TO_STAGE(STAGE_INTRO_CUTSCENE, TRUE)
				ENDIF
			ENDIF
			
			IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage, 0, FALSE, "", TRUE, FALSE, KEY_Z, KEYBOARD_MODIFIER_NONE, TRUE, FALSE, 0.9)	
				PRINTLN("Franklin2.sc - Player Z-skipped.")
			
				MISSION_STAGE eStage = INT_TO_ENUM(MISSION_STAGE, iDebugJumpStage)
				JUMP_TO_STAGE(eStage, TRUE)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

SCRIPT
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Flow_Mission_Force_Cleanup()
		eSectionStage = SECTION_STAGE_RUNNING
		MISSION_CLEANUP()
	ENDIF

	MISSION_SETUP()
	bHasUsedCheckpoint = JUMP_TO_CHECKPOINT()

	//If the player triggered the mission from the blip then keep them blocked until the cutscene loads.
	IF NOT bHasUsedCheckpoint
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
	ENDIF

	WHILE (TRUE)
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_LamarDown")
		
		IF bHasUsedCheckpoint
			IF eSectionStage = SECTION_STAGE_RUNNING
				bHasUsedCheckpoint = FALSE
			ENDIF
		ENDIF
		
		DO_GLOBAL_FAIL_CHECKS()
		DO_GLOBAL_STAT_CHECKS()		
		
		//Main stages
		SWITCH eMissionStage	
			CASE STAGE_INTRO_CUTSCENE
				DO_INTRO_CUTSCENE()
			BREAK
			
			CASE STAGE_MEET_THE_CREW
				MEET_THE_CREW()
			BREAK
			
			CASE STAGE_MEET_CREW_CUTSCENE
				MEET_CREW_CUTSCENE()
			BREAK
			
			CASE STAGE_GET_INTO_POSITION
				GET_INTO_POSITION()
			BREAK
			
			CASE STAGE_ATTACK_SAWMILL
				ATTACK_SAWMILL()
			BREAK
			
			CASE STAGE_GET_LAMAR_OUT
				GET_LAMAR_OUT()
			BREAK
			
			CASE STAGE_DRIVE_HOME
				DRIVE_HOME()
			BREAK
			
			CASE STAGE_OUTRO_CUTSCENE
				DO_OUTRO_CUTSCENE()
			BREAK
		ENDSWITCH

		#IF IS_DEBUG_BUILD
			DO_DEBUG()
		#ENDIF	
	ENDWHILE
ENDSCRIPT			

