USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
USING "script_ped.sch"
using "commands_streaming.sch"
using "commands_camera.sch"
USING "Commands_clock.sch"
USING "Commands_interiors.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "script_blips.sch"
USING "commands_entity.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "locates_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "seamless_cuts.sch"
USING "CompletionPercentage_public.sch"
using "clearMissionArea.sch"
using "usefulcommands.sch"
using "rgeneral_include.sch"
USING "building_control_public.sch"
USING "vehicle_gen_public.sch"
USING "timelapse.sch"
USING "script_blips.sch"
USING "commands_event.sch"
USING "dialogue_public.sch"
USING "family_public.sch"
USING "player_scene_private.sch"
USING "Franklin2_support.sch"


PROC UPDATE_HANDLE_ATTACK_INCIDENTAL_DIALOGUE()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR IS_MESSAGE_BEING_DISPLAYED()
		EXIT
	ENDIF
	
	// high priority stuff
	
	// limit this so it doesnt happen EVERY time
	// (this works even though it's constantly generating a rand because 
	// HAS_PED_JUST_KILLED_ only works for 1 frame)
	IF GET_RANDOM_INT_IN_RANGE(0, 100) > 90
		IF HAS_PED_JUST_KILLED_ANY_ENEMY(missionPeds[ped_franklin].id)
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_frKill", 1, missionPeds[ped_franklin].id, "FRANKLIN")
		ENDIF
		IF HAS_PED_JUST_KILLED_ANY_ENEMY(missionPeds[ped_trevor].id)
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_trKill", 1, missionPeds[ped_trevor].id, "TREVOR")
		ENDIF
		IF HAS_PED_JUST_KILLED_ANY_ENEMY(missionPeds[ped_michael].id)
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_miKill", 1, missionPeds[ped_michael].id, "MICHAEL")
		ENDIF
	ENDIF
			
	IF GET_TIMER_IN_SECONDS_SAFE(tmrIncidentalDialogueThrottle) > fIncidentalDialogueTime AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		fIncidentalDialogueTime = GET_RANDOM_FLOAT_IN_RANGE(2, 8)
		START_TIMER_NOW_SAFE(tmrIncidentalDialogueThrottle)
		
		INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 100)
		// TREVOR
		IF iRandom < 15
			IF eBulldozerState < BULLDOZER_FINISHED
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_drive", 2, missionPeds[ped_trevor].id, "TREVOR")
			ELIF eAttackSawmillCurrentGoal > MISSION_GOAL_09
				IF GET_NUM_LIVING_ENEMIES() > 1
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_enc1", 2, missionPeds[ped_trevor].id, "TREVOR")
				ENDIF
			ENDIF
		// MICHAEL
		ELIF iRandom < 30
			IF eMichaelState > MICHAEL_SETUP
				IF GET_RANDOM_INT_IN_RANGE(0, 10) < 7
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_snipe2", 0, missionPeds[ped_michael].id, "MICHAEL")
				ELSE
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_snipe3", 0, missionPeds[ped_michael].id, "MICHAEL")
				ENDIF
			ENDIF
		// ENEMIES
		ELIF iRandom < 70
			IF GET_NUM_LIVING_ENEMIES() > 2
				STRING sDialogue, sSpeakerName
				INT iSpeakerNum
				SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
					CASE 0
					CASE 1
						sDialogue = "Lm2_frank"
						sSpeakerName = "fr2_enemy1"
						iSpeakerNum = 5
					BREAK
					CASE 2
						sDialogue = "Lm2_alert2"
						sSpeakerName = "fr2_enemy2"
						iSpeakerNum = 4
					BREAK
					CASE 3
						sDialogue = "Lm2_alert3"
						sSpeakerName = "fr2_enemy3"
						iSpeakerNum = 3
					BREAK
				ENDSWITCH
				PED_INDEX speaker
				speaker = GET_FIRST_LIVING_ENEMY()
				IF IS_ENTITY_OK(speaker)
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA(sDialogue, iSpeakerNum, speaker, sSpeakerName)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_RPG_ATTACK()
	IF NOT IS_ENTITY_OK(enemyPeds[iRPGEnemy].id)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_OK(vehicle[veh_bulldozer].id)
		EXIT
	ENDIF
	
	SWITCH eRPGState
		CASE RPG_STATE_INIT
			TASK_AIM_GUN_AT_ENTITY(enemyPeds[iRPGEnemy].id, vehicle[veh_bulldozer].id, INFINITE_TASK_TIME)
			
			eRPGState = RPG_STATE_AIMING
		BREAK
		CASE RPG_STATE_AIMING
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			OR GET_TIMER_IN_SECONDS_SAFE(tmrMichaelActive) > 25
			OR IS_ENTITY_IN_TRIGGER_BOX(tbBulldozerInRPGRange, vehicle[veh_bulldozer].id)
				eRPGState = RPG_STATE_WARNING_SHOT
			ENDIF
		BREAK
		CASE RPG_STATE_WARNING_SHOT
			CPRINTLN(DEBUG_MISSION, "BSW rpgEnemy firing warning shot")
			SET_PED_COMBAT_ATTRIBUTES(enemyPeds[iRPGEnemy].id, CA_PERFECT_ACCURACY, TRUE)
			
			START_TIMER_NOW_SAFE(tmrRPGScene)
			
			TASK_SHOOT_AT_COORD(enemyPeds[iRPGEnemy].id, <<-580.4778, 5263.3911, 74.7760>>, INFINITE_TASK_TIME, FIRING_TYPE_1_THEN_AIM)
			
			eRPGState = RPG_STATE_WARNING_DIALOGUE
		BREAK
		CASE RPG_STATE_WARNING_DIALOGUE
			IF eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
				IF TIMER_DO_ONCE_WHEN_READY(tmrRPGScene, 2)
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_RPG_SHOT_1
					
					START_TIMER_NOW_SAFE(tmrRPGScene)
					eRPGState = RPG_STATE_KILL_SHOT
				ENDIF
			ENDIF
		BREAK
		CASE RPG_STATE_KILL_SHOT
			IF GET_TIMER_IN_SECONDS_SAFE(tmrRPGScene) > 15 AND IS_ENTITY_IN_TRIGGER_BOX(tbBulldozerInRPGRange, vehicle[veh_bulldozer].id)
				CPRINTLN(DEBUG_MISSION, "rpgEnemy should fire kill shot")
				SET_PED_AMMO(enemyPeds[iRPGEnemy].id, enemySpawnPoints[iRPGEnemy].iWeaponType, 10)
				REFILL_AMMO_INSTANTLY(enemyPeds[iRPGEnemy].id)

				TASK_SHOOT_AT_ENTITY(enemyPeds[iRPGEnemy].id, vehicle[veh_bulldozer].id, 25000, FIRING_TYPE_1_BURST)
				//TASK_SHOOT_AT_COORD(enemyPeds[iRPGEnemy].id, GET_ENTITY_COORDS(vehicle[veh_bulldozer].id), 25000, FIRING_TYPE_1_BURST)
				eRPGState = RPG_STATE_COMPLETE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_BULLDOZER_RECORDING()
	IF IS_VEHICLE_DRIVEABLE(vehicle[veh_bulldozer].id)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_bulldozer].id)
			IF GET_IS_WAYPOINT_RECORDING_LOADED(GET_WAYPOINT_LABEL(FW_BULLDOZER))
				IF GET_NUM_LIVING_ENEMIES() > 20 AND NOT SHOULD_BULLDOZER_RAM_BARRICADE()
					VEHICLE_WAYPOINT_PLAYBACK_PAUSE(vehicle[veh_bulldozer].id)
				ELSE
					IF VEHICLE_WAYPOINT_PLAYBACK_GET_IS_PAUSED(vehicle[veh_bulldozer].id)
						VEHICLE_WAYPOINT_PLAYBACK_RESUME(vehicle[veh_bulldozer].id)
					ENDIF
					
					FLOAT fNewSpeed = GET_NEW_BULLDOZER_PLAYBACK_SPEED()
					VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(vehicle[veh_bulldozer].id, fNewSpeed)
				ENDIF
			ENDIF
			
			SET_VEHICLE_BULLDOZER_ARM_POSITION(vehicle[veh_bulldozer].id, 0.1, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_BULLDOZER_EXPLOSIONS()
	IF NOT IS_ENTITY_OK(vehicle[veh_bulldozer].id)
		EXIT
	ENDIF
	
	INT iter
	FOR iter = veh_blocker1 TO veh_blocker4
		IF IS_ENTITY_OK(vehicle[iter].id)
			IF IS_VEHICLE_DRIVEABLE(vehicle[iter].id)
				IF IS_ENTITY_TOUCHING_ENTITY(vehicle[veh_bulldozer].id, vehicle[iter].id)
					EXPLODE_VEHICLE(vehicle[iter].id)
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_boom", 1, missionPeds[ped_trevor].id, "TREVOR")
					
					CPRINTLN(DEBUG_MISSION, "UPDATE_BULLDOZER_EXPLOSIONS exploding blocker", iter)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC UPDATE_BULLDOZER()
	IF NOT DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
		EXIT
	ENDIF
	
	IF PLAYER_PED_ID() = missionPeds[ped_trevor].id
		SET_VEHICLE_DOORS_LOCKED(vehicle[veh_bulldozer].id, VEHICLELOCK_UNLOCKED)
	ELSE
		SET_VEHICLE_DOORS_LOCKED(vehicle[veh_bulldozer].id, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
	ENDIF
	
	// give the bulldozer some forward momentum
	IF eBulldozerState < BULLDOZER_FINISHED
		IF GET_BULLDOZER_DRIVER() = PLAYER_PED_ID() OR IS_SELECTOR_CAM_ACTIVE()
			IF GET_TIMER_IN_SECONDS_SAFE(tmrBulldozerForceDrive) <= 1
				SET_VEHICLE_FORWARD_SPEED(vehicle[veh_bulldozer].id, SPEED_Slow)
			ENDIF
		ENDIF
	ENDIF

	UPDATE_BULLDOZER_EXPLOSIONS()
	
	
	IF IS_ENTITY_OK(enemyPeds[iRPGEnemy].id) AND eRPGState >= RPG_STATE_INIT
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, GET_ENTITY_COORDS(vehicle[veh_bulldozer].id), 3)
			IF NOT IS_ENTITY_DEAD(vehicle[veh_bulldozer].id)
				EXPLODE_VEHICLE(vehicle[veh_bulldozer].id)
			ENDIF
		ENDIF
	ENDIF

	SWITCH eBulldozerState
		CASE BULLDOZER_SETUP
			iCurrentBarricadeTarget = veh_blocker1

			eBulldozerState = BULLDOZER_STAGE1
		BREAK
		CASE BULLDOZER_STAGE1
			IF NOT IS_BITMASK_AS_ENUM_SET(iMissionDialogueFlags, TREVOR_BarricadeSmash1)
				IF GET_DISTANCE_TO_BARRICADE_1() < 40
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_move", 2, missionPeds[ped_trevor].id, "TREVOR")
					SET_BITMASK_AS_ENUM(iMissionDialogueFlags, TREVOR_BarricadeSmash1)
				ENDIF
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iMissionDialogueFlags, TREVOR_BarricadeSmash2)
				IF GET_DISTANCE_TO_BARRICADE_2() < 20
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_move2", 2, missionPeds[ped_trevor].id, "TREVOR")
					SET_BITMASK_AS_ENUM(iMissionDialogueFlags, TREVOR_BarricadeSmash2)
				ENDIF
			ENDIF
			
			IF IS_BARRICADE_DESTROYED(1)
				iCurrentBarricadeTarget = veh_blocker3
				
				// if the player is driving the bulldozer, update the objective
				IF GET_BULLDOZER_DRIVER() = PLAYER_PED_ID()
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_PUSH_CARS
				ENDIF
				
				eBulldozerState = BULLDOZER_STAGE2
			ENDIF
		BREAK
		CASE BULLDOZER_STAGE2
			IF IS_BARRICADE_DESTROYED(2)
				// if the player is driving the bulldozer, update the objective
				IF GET_BULLDOZER_DRIVER() = PLAYER_PED_ID()
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_BULLDOZER_END
				ENDIF
				
				eBulldozerState = BULLDOZER_STAGE3
			ENDIF		
		BREAK
		CASE BULLDOZER_STAGE3
			IF IS_ENTITY_IN_TRIGGER_BOX(tbBulldozerEnd, vehicle[veh_bulldozer].id)
				CPRINTLN(DEBUG_MISSION, "UPDATE_BULLDOZER bulldozer has reached the end")
				
				TRIGGER_MUSIC_EVENT("FRA2_TREV_OUT")
				
				eBulldozerState = BULLDOZER_FINISHED
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_TREVOR_BULLDOZER_FAIL_CONDITIONS()
	BOOL bShouldTrevorWarnFail = FALSE
	
	// if franklin(player) wanders around the back of the sawmill
	IF NOT IS_TIMER_STARTED(tmrFranklinAbandonTimer)
		IF IS_PLAYER_IN_TRIGGER_BOX(tbSawmillBackSafety)
			bShouldTrevorWarnFail = TRUE
			START_TIMER_NOW_SAFE(tmrFranklinAbandonTimer)
		ENDIF
	ELSE
		IF NOT IS_PLAYER_IN_TRIGGER_BOX(tbSawmillBackSafety)
			CANCEL_TIMER(tmrFranklinAbandonTimer)
		ENDIF
		
		IF GET_TIMER_IN_SECONDS_SAFE(tmrFranklinAbandonTimer) > 4
			MISSION_FAILED(FAIL_TREVOR_KILLED)
		ENDIF
	ENDIF
	
	// if we're more than X away, give a warning and start jacking
	IF GET_DISTANCE_BETWEEN_ENTITIES(vehicle[veh_bulldozer].id, missionPeds[ped_franklin].id) > 45
		bShouldTrevorWarnFail = TRUE
		
		// OMGHAX @BSW this doesn't really belong inside this dialogue throttle
		// but it's an easy way to prevent spamming this task
		PED_INDEX pedJacker
		IF GET_CLOSEST_ENEMY_TO_BULLDOZER(pedJacker) < 25
			IF IS_ENTITY_OK(pedJacker)
				SEQUENCE_INDEX seqJack
				OPEN_SEQUENCE_TASK(seqJack)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_ENTER_VEHICLE(NULL, vehicle[veh_bulldozer].id, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_JACK_ANYONE | ECF_JUST_PULL_PED_OUT)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
				CLOSE_SEQUENCE_TASK(seqJack)
				TASK_PERFORM_SEQUENCE(pedJacker, seqJack)
				CLEAR_SEQUENCE_TASK(seqJack)
				
				CPRINTLN(DEBUG_MISSION, "Tasking ped to jack the bulldozer")
			ENDIF
		ENDIF
	ENDIF
	
	IF bShouldTrevorWarnFail
		IF GET_TIMER_IN_SECONDS_SAFE(tmrTrevorDialogueThrottle) > iTrevorRequestHelpTime
			START_TIMER_NOW_SAFE(tmrTrevorDialogueThrottle)
			iTrevorRequestHelpTime = GET_RANDOM_INT_IN_RANGE(9, 20)
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_cover", 2, missionPeds[ped_trevor].id, "TREVOR")
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_TREVOR_ATTACK()
	IF PLAYER_PED_ID() = missionPeds[ped_trevor].id
		eTrevorState = TREVOR_COMBAT_INIT
		EXIT
	ENDIF
	IF NOT IS_ENTITY_OK(missionPeds[ped_trevor].id)
		EXIT
	ENDIF
	IF NOT IS_ENTITY_OK(missionPeds[ped_franklin].id)
		EXIT
	ENDIF
	
	IF NOT g_bMagDemoActive
		IF IS_PED_BEING_JACKED(missionPeds[ped_trevor].id)
			IF GET_PEDS_JACKER(missionPeds[ped_trevor].id) != missionPeds[ped_franklin].id
				CPRINTLN(DEBUG_MISSION, "BSW trevor is being jacked")
				MISSION_FAILED(FAIL_TREVOR_KILLED)
			ENDIF
		ENDIF
	ENDIF
	
	SEQUENCE_INDEX iSeq
	
	SWITCH eTrevorState
		CASE TREVOR_SETUP
			eTrevorState = TREVOR_COMBAT_INIT
		BREAK
		CASE TREVOR_COMBAT_INIT
			IF eBulldozerState = BULLDOZER_FINISHED
				eTrevorState = TREVOR_START_COMBAT
			ELSE
				eTrevorState = TREVOR_ENTER_BULLDOZER
			ENDIF
		BREAK
		CASE TREVOR_ENTER_BULLDOZER
			IF IS_BULLDOZER_DRIVER_SEAT_OPEN()
				CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_PED tasking trevor into bulldozer")
				TASK_ENTER_VEHICLE(missionPeds[ped_trevor].id, vehicle[veh_bulldozer].id)

				eTrevorState = TREVOR_ENTERING_BULLDOZER
			ELIF GET_BULLDOZER_DRIVER() = missionPeds[ped_trevor].id
				CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_PED trevor is already in dozer, go straight to start playback")
				START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE()
				eTrevorState = TREVOR_DRIVE_BULLDOZER
			ELSE
				eTrevorState = TREVOR_START_COMBAT
			ENDIF
		BREAK
		CASE TREVOR_ENTERING_BULLDOZER
			IF GET_SCRIPT_TASK_STATUS(missionPeds[ped_trevor].id, SCRIPT_TASK_ENTER_VEHICLE) = FINISHED_TASK
			AND GET_BULLDOZER_DRIVER() = missionPeds[ped_trevor].id
				START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE()
				
				WEAPON_TYPE newDrivebyWep
				newDrivebyWep = GET_PED_WEAPONTYPE_IN_SLOT(missionPeds[ped_trevor].id, WEAPONSLOT_SMG)
				IF IS_WEAPON_VALID(newDrivebyWep)
					SET_CURRENT_PED_WEAPON(missionPeds[ped_trevor].id, newDrivebyWep)
				ELSE
					newDrivebyWep = GET_PED_WEAPONTYPE_IN_SLOT(missionPeds[ped_trevor].id, WEAPONSLOT_PISTOL)
					IF IS_WEAPON_VALID(newDrivebyWep)
						SET_CURRENT_PED_WEAPON(missionPeds[ped_trevor].id, newDrivebyWep)
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
					GIVE_WEAPON_TO_PED(missionPeds[ped_trevor].id, WEAPONTYPE_SMG, 200)
				#ENDIF
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_DIA_TREVOR_USES_BULLDOZER
				
				eTrevorState = TREVOR_DRIVE_BULLDOZER
			ENDIF
		BREAK
		CASE TREVOR_DRIVE_BULLDOZER
			IF NOT DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
				EXIT
			ENDIF
			UPDATE_BULLDOZER_RECORDING()
			
			HANDLE_TREVOR_BULLDOZER_FAIL_CONDITIONS()

			// make sure Trevor is animating and looking around
			IF GET_TIMER_IN_SECONDS_SAFE(tmrTrevorLookAtInDozer) > 5
				PED_INDEX lookatTarget
				IF GET_CLOSEST_ENEMY_TO_BULLDOZER(lookatTarget) < 30
					IF IS_ENTITY_OK(lookatTarget)
						TASK_LOOK_AT_ENTITY(GET_BULLDOZER_DRIVER(), lookatTarget, GET_RANDOM_INT_IN_RANGE(1000, 4000))
						CPRINTLN(DEBUG_MISSION, "BSW tasking Trevor to look at an enemy")
					ENDIF
				ENDIF
				START_TIMER_NOW_SAFE(tmrTrevorLookAtInDozer)
			ENDIF
			
			

			IF eBulldozerState = BULLDOZER_FINISHED
				eTrevorState = TREVOR_START_COMBAT
			ENDIF
		BREAK
		CASE TREVOR_START_COMBAT
			CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_ATTACK - tasking Trevor combat!")
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(missionPeds[ped_trevor].id, FALSE)
			REGISTER_ENEMIES_FOR_PED(missionPeds[ped_trevor].id)
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_LEAVE_ANY_VEHICLE(NULL)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200)
			CLOSE_SEQUENCE_TASK(iSeq)
			
			TASK_PERFORM_SEQUENCE(missionPeds[ped_trevor].id, iSeq)
			CLEAR_SEQUENCE_TASK(iSeq)
			
			// this was turned on to make enemies prefer trevor as a target more
			// while he's driving the bulldozer. it should be off by default, so now
			// that he's out, turn it off
			SET_PED_CONFIG_FLAG(missionPeds[ped_trevor].id, PCF_TreatAsPlayerDuringTargeting, FALSE)
			
			eTrevorState = TREVOR_FIGHT_ON_FOOT
		BREAK
		CASE TREVOR_FIGHT_ON_FOOT
			SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(missionPeds[ped_trevor].id, missionPeds[ped_franklin].id, <<7, 7, 7>>, <<-7, -7, -7>>, 8, FALSE)
		BREAK
	ENDSWITCH

ENDPROC

PROC UPDATE_FRANKLIN_ATTACK_SAWMILL()
	IF PLAYER_PED_ID() = missionPeds[ped_franklin].id
		eFranklinState = FRANKLIN_COMBAT_INIT
		EXIT
	ENDIF
	IF NOT IS_ENTITY_OK(missionPeds[ped_franklin].id)
		EXIT
	ENDIF
	IF NOT DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
		EXIT
	ENDIF

	IF eBulldozerState = BULLDOZER_STAGE1 OR eBulldozerState = BULLDOZER_STAGE2
		IF GET_TIMER_IN_SECONDS_SAFE(tmrFranklinDialogueThrottle) > iFranklinRequestHelpTime
			IF GET_DISTANCE_BETWEEN_ENTITIES(vehicle[veh_bulldozer].id, missionPeds[ped_franklin].id) > 40
				START_TIMER_NOW_SAFE(tmrFranklinDialogueThrottle)
				iFranklinRequestHelpTime = GET_RANDOM_INT_IN_RANGE(9, 20)
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_FCover", 0, missionPeds[ped_franklin].id, "FRANKLIN")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_TIMER_STARTED(tmrMichaelKillTimer)
		IF GET_TIMER_IN_SECONDS_SAFE(tmrMichaelKillTimer) > 8
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(missionPeds[ped_franklin].id, FALSE)
			CANCEL_TIMER(tmrMichaelKillTimer)
		ENDIF
	ENDIF
	
	SWITCH eFranklinState
		CASE FRANKLIN_SETUP
			eFranklinState = FRANKLIN_COMBAT_INIT
		BREAK
		CASE FRANKLIN_COMBAT_INIT
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(missionPeds[ped_franklin].id, FALSE)
			REGISTER_ENEMIES_FOR_PED(missionPeds[ped_franklin].id)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(missionPeds[ped_franklin].id, 200)
			eFranklinState = FRANKLIN_COMBAT
		BREAK
		CASE FRANKLIN_COMBAT
			// keep Franklin around the sawmill in case the player decides
			// to drive the bulldozer off into the sunset
			IF IS_ENTITY_IN_TRIGGER_BOX(tbBulldozerEnd, vehicle[veh_bulldozer].id)
				SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(missionPeds[ped_franklin].id, missionPeds[ped_trevor].id, <<7, 0, 7>>, <<-7, 0, -7>>, 7, FALSE)
			ELIF IS_ENTITY_IN_TRIGGER_BOX(tbFirstCombatZone, vehicle[veh_bulldozer].id)
			OR (IS_ENTITY_IN_TRIGGER_BOX(tbSecondCombatZone, vehicle[veh_bulldozer].id) AND IS_BARRICADE_DESTROYED(1))
				SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(missionPeds[ped_franklin].id, vehicle[veh_bulldozer].id, <<0, 0, 0>>, 10.0)
			ELSE
				SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(missionPeds[ped_franklin].id, tbFirstCombatZone)
			ENDIF
		BREAK
		CASE FRANKLIN_GO_TO_LAMAR
			CPRINTLN(DEBUG_MISSION, "UPDATE_FRANKLIN_ATTACK_SAWMILL - FRANKLIN_GO_TO_LAMAR")
			SEQUENCE_INDEX iSeq
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLamarInjuredPosition, PEDMOVE_RUN, DEFAULT, 5)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200)
			CLOSE_SEQUENCE_TASK(iSeq)
			TASK_PERFORM_SEQUENCE(missionPeds[ped_franklin].id, iSeq)
			CLEAR_SEQUENCE_TASK(iSeq)
			eFranklinState = FRANKLIN_COMPLETE
		BREAK
		CASE FRANKLIN_COMPLETE
			SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(missionPeds[ped_franklin].id, missionPeds[ped_lamar].id, <<4, 0, 4>>, <<-4, 0, -4>>, 4, FALSE)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC UPDATE_HANDLE_ATTACK_MESSAGING()
	IF IS_SELECTOR_CAM_ACTIVE()
		EXIT
	ENDIF
	
	SWITCH eAttackSawmillCurrentDialogue
		CASE ATTACK_MESSAGE_DIA_TREVOR_USES_BULLDOZER
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("lm2_bul", 2, missionPeds[ped_trevor].id, "TREVOR")
					CPRINTLN(DEBUG_MISSION, "UPDATE_HANDLE_ATTACK_MESSAGING playing third trevor bulldozer dialogue")
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_ESCORT_BULLDOZER
				ENDIF
			ENDIF
		BREAK
		// ESCORT THE BULLDOZER
		CASE ATTACK_MESSAGE_ESCORT_BULLDOZER
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_MESSAGE_BEING_DISPLAYED()
					CLEAR_PRINTS()
				ENDIF
				PRINT("FRAN2_FOLLOWBD", DEFAULT_GOD_TEXT_TIME ,1)
				
				IF DOES_BLIP_EXIST(missionPeds[ped_trevor].aiBlip.BlipID)
					REMOVE_BLIP(missionPeds[ped_trevor].aiBlip.BlipID)
				ENDIF
				
				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIf
				
				mission_blip = CREATE_BLIP_FOR_VEHICLE(vehicle[veh_bulldozer].id, FALSE, FALSE)
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
			ENDIF
		BREAK
		// PUSH THE CARS OUT OF THE WAY
		CASE ATTACK_MESSAGE_PUSH_CARS
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_MESSAGE_BEING_DISPLAYED()
					CLEAR_PRINTS()
				ENDIF
				PRINT("FRAN2_PUSHCARS", DEFAULT_GOD_TEXT_TIME,1)

				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIf

				IF iCurrentBarricadeTarget > -1
					mission_blip = CREATE_BLIP_FOR_ENTITY(vehicle[iCurrentBarricadeTarget].id, TRUE)
				ENDIF
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_MICHAEL_ARRIVES
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_mrpgwarn", 1, missionPeds[ped_michael].id, "MICHAEL")
					CPRINTLN(DEBUG_MISSION, "BSW ATTACK_MESSAGE_MICHAEL_ARRIVES conversation triggered")
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_RPG_SHOT_1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_mrpg", 2, missionPeds[ped_trevor].id, "TREVOR", 0, missionPeds[ped_franklin].id, "FRANKLIN", 1, missionPeds[ped_michael].id, "MICHAEL")
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_BULLDOZER_END
			PRINT("FRAN2_DOZER_END", DEFAULT_GOD_TEXT_TIME, 1)

			IF DOES_BLIP_EXIST(mission_blip)
				REMOVE_BLIP(mission_blip)
			ENDIf
			mission_blip = CREATE_BLIP_FOR_COORD(vBulldozer2EndGoal)
				
			eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
		BREAK
		// PROTECT FRANKLIN AND TREVOR
		CASE ATTACK_MESSAGE_PROTECT_F_AND_T
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_MESSAGE_BEING_DISPLAYED()
					CLEAR_PRINTS()
				ENDIF
				PRINT("FRAN2_ProtF", DEFAULT_GOD_TEXT_TIME,1)

				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIf
				
				IF NOT DOES_BLIP_EXIST(missionPeds[ped_trevor].aiBlip.BlipID)
					missionPeds[ped_trevor].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_trevor].id)
				ENDIF
				IF NOT DOES_BLIP_EXIST(missionPeds[ped_franklin].aiBlip.BlipID)
					missionPeds[ped_franklin].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_franklin].id)
				ENDIF
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_MICHAEL_SAYS_FIND_LAMAR
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_mgofindl", 0, missionPeds[ped_michael].id, "MICHAEL")
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_FIND_LAMAR
				ENDIF
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_FIND_LAMAR
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_MESSAGE_BEING_DISPLAYED()
					CLEAR_PRINTS()
				ENDIF
				PRINT("FRAN2_GETLAMAR", DEFAULT_GOD_TEXT_TIME, 1)

				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIf
				
				IF NOT DOES_BLIP_EXIST(missionPeds[ped_trevor].aiBlip.BlipID)
					IF PLAYER_PED_ID() != missionPeds[ped_trevor].id
						missionPeds[ped_trevor].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_trevor].id)
					ENDIF
				ENDIF				
				
				mission_blip = CREATE_BLIP_FOR_COORD(vLamarInjuredPosition)
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_TREVOR_REACHES_LAMAR
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("lm2_lgot", 2, missionPeds[ped_trevor].id, "TREVOR", 3, missionPeds[ped_lamar].id, "LAMAR")
					SET_BITMASK_AS_ENUM(iMissionDialogueFlags, PLAYER_SaysHiToLamar)
					IF GET_NUM_ENEMIES_IN_TRIGGER_BOX(tbLamarGuardsClear) = 0
						eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_SWITCH_TO_FRANKLIN
					ELSE
						eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_CLEAR_GUARDS
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_FRANKLIN_REACHES_LAMAR
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("lm2_lgof", 0, missionPeds[ped_franklin].id, "FRANKLIN", 3, missionPeds[ped_lamar].id, "LAMAR")
					SET_BITMASK_AS_ENUM(iMissionDialogueFlags, PLAYER_SaysHiToLamar)
					IF GET_NUM_ENEMIES_IN_TRIGGER_BOX(tbLamarGuardsClear) = 0
						eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
					ELSE
						eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_CLEAR_GUARDS
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_CLEAR_GUARDS
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_MESSAGE_BEING_DISPLAYED()
					CLEAR_PRINTS()
				ENDIF
				PRINT("FRAN2_KILLG", DEFAULT_GOD_TEXT_TIME, 1)
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
			ENDIF
		BREAK
		CASE ATTACK_MESSAGE_SWITCH_TO_FRANKLIN
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT("FRAN2_SWFRANK", DEFAULT_GOD_TEXT_TIME, 1)

				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, TRUE)
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_LAMAR_ATTACK_SAWMILL()
	IF NOT IS_ENTITY_OK(missionPeds[ped_lamar].id)
		EXIT
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iMissionDialogueFlags, PLAYER_SaysHiToLamar)
		IF IS_ENTITY_OK(missionPeds[ped_lamar].id) AND IS_ENTITY_OK(PLAYER_PED_ID())
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), missionPeds[ped_lamar].id) < 5
				CPRINTLN(DEBUG_MISSION, "BSW player greets Lamar")
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_FRANKLIN_REACHES_LAMAR
				ELSE
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_TREVOR_REACHES_LAMAR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH eLamarState
		CASE LAMAR_AT_GUNPOINT
			IF DOES_ENTITY_EXIST(enemyPeds[iLamarAimGuard].id)
				IF IS_ENTITY_DEAD(enemyPeds[iLamarAimGuard].id)
					//SET_PED_MOVEMENT_CLIPSET(missionPeds[ped_lamar].id, "MOVE_INJURED_GENERIC")
					
					//CLEAR_PED_TASKS(missionPeds[ped_lamar].id)
					
					eLamarState = LAMAR_WAITING_IN_COVER
				ELSE
					IF NOT IS_TIMER_STARTED(tmrLamarExecution)
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), enemyPeds[iLamarAimGuard].id) < 14
						OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(enemyPeds[iLamarAimGuard].id), 3)
							CPRINTLN(DEBUG_MISSION, "BSW lamar executor is spooked, starting execution timer")
							START_TIMER_NOW_SAFE(tmrLamarExecution)
						ENDIF
					ELSE
						IF GET_TIMER_IN_SECONDS_SAFE(tmrLamarExecution) > 2
							CPRINTLN(DEBUG_MISSION, "BSW execution time is up, kill lamar")
							TASK_SHOOT_AT_ENTITY(enemyPeds[iLamarAimGuard].id, missionPeds[ped_lamar].id, INFINITE_TASK_TIME, FIRING_TYPE_CONTINUOUS)
							MISSION_FAILED(FAIL_LAMAR_KILLED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE LAMAR_WAITING_IN_COVER
			
		BREAK
		CASE LAMAR_RESCUED
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DO_ATTACK_SAWMILL()
	BOOL bRetVal = FALSE
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
	
	UPDATE_PLAYER_SWITCH()
	UPDATE_TREVOR_ATTACK()
	UPDATE_FRANKLIN_ATTACK_SAWMILL()
	UPDATE_MICHAEL_PED()
	
	UPDATE_LAMAR_ATTACK_SAWMILL()
	
	UPDATE_BULLDOZER()
	UPDATE_RPG_ATTACK()
	
	FAIL(FAIL_LAMAR_KILLED)
	FAIL(FAIL_TREVOR_KILLED)
	FAIL(FAIL_MICHAEL_KILLED)
	FAIL(FAIL_FRANKLIN_KILLED)
	FAIL(FAIL_BULLDOZER_KILLED)
	
	UPDATE_HANDLE_ATTACK_MESSAGING()
	UPDATE_HANDLE_ATTACK_INCIDENTAL_DIALOGUE()
	UPDATE_ENEMY_BLIPS()

	IF g_bMagDemoActive
		IF IS_ENTITY_OK(missionPeds[ped_lamar].id)
			IF IS_PED_IN_ANY_VEHICLE(missionPeds[ped_lamar].id)
				TRIGGER_MUSIC_EVENT("FRA2_END_ON_FOOT")
				MISSION_PASSED()
			ENDIF
		ENDIF
	ENDIF

	SWITCH eAttackSawmillCurrentGoal
		CASE MISSION_GOAL_01			
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
			
			eAttackSawmillCurrentGoal = MISSION_GOAL_02
		BREAK
		
		CASE MISSION_GOAL_02
			CREATE_LAMAR(STAGE_ATTACK_SAWMILL, TRUE)
			
			SPAWN_ENEMY_SQUAD(SQUAD_Wave1)
			SET_UP_ENEMY_PEDS(SQUAD_Wave1, DEFAULT, SIGHTRANGE_BLIND)
			TASK_START_SCENARIO_IN_PLACE(enemyPeds[iCellphoneEnemy].id, "WORLD_HUMAN_STAND_MOBILE")
			
			eTrevorState = TREVOR_SETUP
			eFranklinState = FRANKLIN_SETUP
			eLamarState = LAMAR_AT_GUNPOINT
			
			START_TIMER_NOW_SAFE(tmrTrevorDialogueThrottle)
			START_TIMER_NOW_SAFE(tmrFranklinDialogueThrottle)
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_STEALTH")
				START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_STEALTH")
			ENDIF

			eAttackSawmillCurrentGoal = MISSION_GOAL_03
		BREAK
		
		CASE MISSION_GOAL_03
			IF eTrevorState = TREVOR_DRIVE_BULLDOZER
				TRIGGER_MUSIC_EVENT("FRA2_BULLDOZER")
				eBulldozerState = BULLDOZER_SETUP
				eAttackSawmillCurrentGoal = MISSION_GOAL_04
			ENDIF
		BREAK
		CASE MISSION_GOAL_04
			IF IS_ANY_BUDDY_IN_TRIGGER_BOX(tbTriggerWave1, missionPeds[ped_franklin].id, missionPeds[ped_michael].id, missionPeds[ped_trevor].id)
			OR IS_ANY_SQUAD_MEMBER_INJURED(SQUAD_Wave1)
				STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_STEALTH")
				IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_ALERT")
					START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_ALERT")
				ENDIF

				SET_UP_ENEMY_PEDS(SQUAD_Wave1)
				TASK_SQUAD_DEFEND_IN_PLACE(SQUAD_Wave1)
				
				START_TIMER_NOW_SAFE(tmrTrevorLookAtInDozer)
				
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_spot", 2, missionPeds[ped_trevor].id, "TREVOR")
				
				PED_INDEX pAlertPed
				GET_CLOSEST_ENEMY_TO_BULLDOZER(pAlertPed)
				IF IS_ENTITY_OK(pAlertPed)
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("Lm2_alert", 4, pAlertPed, "fr2_enemy1")
				ENDIF
				
				SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)

				eAttackSawmillCurrentGoal = MISSION_GOAL_05
			ENDIF
		BREAK

		CASE MISSION_GOAL_05
			IF IS_ANY_BUDDY_IN_TRIGGER_BOX(tbTriggerWave2, missionPeds[ped_franklin].id, missionPeds[ped_michael].id, missionPeds[ped_trevor].id)
			OR GET_NUM_LIVING_ENEMIES() < 2
				SPAWN_ENEMY_SQUAD(SQUAD_Wave2)
				SET_UP_ENEMY_PEDS(SQUAD_Wave2)
				
				SPAWN_ENEMY_SQUAD(SQUAD_Barricade1)
				SET_UP_ENEMY_PEDS(SQUAD_Barricade1)
				TASK_SQUAD_DEFEND_VEHICLE(SQUAD_Barricade1, vehicle[veh_blocker1].id)
				
				SPAWN_ENEMY_SQUAD(SQUAD_Barricade2)
				SET_UP_ENEMY_PEDS(SQUAD_Barricade2)
				TASK_SQUAD_DEFEND_VEHICLE(SQUAD_Barricade2, vehicle[veh_blocker4].id)
				
				TASK_SQUAD_FOLLOW_BULLDOZER(SQUAD_Wave1)

				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("Lm2_dozer", 4, enemyPeds[iDiaBulldozerTargeter].id, "fr2_enemy2")
				
				TASK_SQUAD_DEFEND_IN_PLACE(SQUAD_Wave2)

				eAttackSawmillCurrentGoal = MISSION_GOAL_06
			ENDIF
		BREAK
		
		CASE MISSION_GOAL_06
			IF IS_ANY_BUDDY_IN_TRIGGER_BOX(tbTriggerWave3, missionPeds[ped_franklin].id, missionPeds[ped_michael].id, missionPeds[ped_trevor].id)
				TASK_SQUAD_FOLLOW_BULLDOZER(SQUAD_Wave2)
				
				SPAWN_ENEMY_SQUAD(SQUAD_Wave3)
				SET_UP_ENEMY_PEDS(SQUAD_Wave3, CM_DEFENSIVE)
				
				TASK_SQUAD_ATTACK_HATED_TARGETS_AROUND_BULLDOZER(SQUAD_Wave3)

				eAttackSawmillCurrentGoal = MISSION_GOAL_07
			ENDIF
		BREAK
		
		CASE MISSION_GOAL_07
			IF IS_ANY_BUDDY_IN_TRIGGER_BOX(tbMichaelAppears, missionPeds[ped_franklin].id, missionPeds[ped_michael].id, missionPeds[ped_trevor].id)
				SET_UP_ENEMY_PEDS(SQUAD_Wave3)
				TASK_SQUAD_FOLLOW_BULLDOZER(SQUAD_Wave3)
				eAttackSawmillCurrentGoal = MISSION_GOAL_08
			ENDIF
		BREAK
		
		CASE MISSION_GOAL_08
			IF CREATE_MICHAEL(STAGE_ATTACK_SAWMILL, TRUE)
				IF NOT DOES_BLIP_EXIST(missionPeds[ped_michael].aiBlip.BlipID)
					missionPeds[ped_michael].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_michael].id)
				ENDIF
				
				// restart this timer so we dont have random dialogue trying to conflict with michael's intro dialogue
				START_TIMER_NOW_SAFE(tmrIncidentalDialogueThrottle)
				
				SPAWN_ENEMY_SQUAD(SQUAD_RPGSquad)
				SET_UP_SCRIPTED_SQUAD(SQUAD_RPGSquad)
				
				eMichaelState = MICHAEL_SETUP
				
				sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = missionPeds[ped_michael].id
				
				START_TIMER_NOW_SAFE(tmrMichaelActive)
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
				SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_MICHAEL, SELECTOR_PED_FRANKLIN, SELECTOR_PED_TREVOR)
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_MICHAEL_ARRIVES
				
				eAttackSawmillCurrentGoal = MISSION_GOAL_09
			ENDIF
		BREAK
		
		CASE MISSION_GOAL_09
			IF IS_ANY_BUDDY_IN_TRIGGER_BOX(tbTriggerWave4, missionPeds[ped_franklin].id, missionPeds[ped_michael].id, missionPeds[ped_trevor].id)
			OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				SPAWN_ENEMY_SQUAD(SQUAD_Wave4)
				SET_UP_ENEMY_PEDS(SQUAD_Wave4)
				
				SPAWN_ENEMY_SQUAD(SQUAD_SnipeTargets)
				SET_UP_ENEMY_PEDS(SQUAD_SnipeTargets, CM_STATIONARY, 100)
				SQUAD_REGISTER_TARGET(SQUAD_SnipeTargets, missionPeds[ped_trevor].ID)
				TASK_SQUAD_ATTACK_HATED_TARGETS(SQUAD_SnipeTargets)

				eAttackSawmillCurrentGoal = MISSION_GOAL_10
			ENDIF
		BREAK
		
		CASE MISSION_GOAL_10
			IF IS_ANY_BUDDY_IN_TRIGGER_BOX(tbTriggerWave4, missionPeds[ped_franklin].id, missionPeds[ped_michael].id, missionPeds[ped_trevor].id)
				TASK_SQUAD_ATTACK_HATED_TARGETS_AROUND_BULLDOZER(SQUAD_Wave4)

				eAttackSawmillCurrentGoal = MISSION_GOAL_11
			ENDIF
		BREAK

		CASE MISSION_GOAL_11
			IF IS_ANY_BUDDY_IN_TRIGGER_BOX(tbTriggerWave5, missionPeds[ped_franklin].id, missionPeds[ped_michael].id, missionPeds[ped_trevor].id)
			OR GET_NUM_LIVING_ENEMIES() < 7
				TASK_SQUAD_FOLLOW_BULLDOZER(SQUAD_Wave4)
				
				SPAWN_ENEMY_SQUAD(SQUAD_Wave5)
				SET_UP_ENEMY_PEDS(SQUAD_Wave5)
				
				TASK_SQUAD_ATTACK_HATED_TARGETS_AROUND_BULLDOZER(SQUAD_Wave5)
				
				eAttackSawmillCurrentGoal = MISSION_GOAL_12
			ENDIF
		BREAK
		
		CASE MISSION_GOAL_12
			IF IS_ANY_BUDDY_IN_TRIGGER_BOX(tbTriggerWave6, missionPeds[ped_franklin].id, missionPeds[ped_michael].id, missionPeds[ped_trevor].id)
			OR eBulldozerState = BULLDOZER_FINISHED
				// safety
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
					
				TASK_SQUAD_DEFEND_LAMAR(SQUAD_Wave4)
				TASK_SQUAD_DEFEND_LAMAR(SQUAD_Wave5)
				
				SPAWN_ENEMY_SQUAD(SQUAD_Wave6)
				SET_UP_ENEMY_PEDS(SQUAD_Wave6, CM_DEFENSIVE)
				TASK_SQUAD_DEFEND_LAMAR_ENTRANCE(SQUAD_Wave6)
				
				SPAWN_ENEMY_SQUAD(SQUAD_LamarGuards)
				SET_UP_ENEMY_PEDS(SQUAD_LamarGuards, CM_DEFENSIVE)
				TASK_SQUAD_DEFEND_LAMAR(SQUAD_LamarGuards)
				
				IF IS_ENTITY_OK(enemyPeds[iLamarAimGuard].id) AND IS_ENTITY_OK(missionPeds[ped_lamar].id)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemyPeds[iLamarAimGuard].id, TRUE)
					TASK_AIM_GUN_AT_ENTITY(enemyPeds[iLamarAimGuard].id, missionPeds[ped_lamar].id, INFINITE_TASK_TIME)
				ENDIF
				
				// reset these guys so they move around
				SET_UP_ENEMY_PEDS(SQUAD_SnipeTargets)
				
				PRE_STREAM_ASSETS_FOR_STAGE(STAGE_GET_LAMAR_OUT)
				
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_MICHAEL_SAYS_FIND_LAMAR

				SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_FRANKLIN, SELECTOR_PED_TREVOR, SELECTOR_PED_MICHAEL)
				
				eAttackSawmillCurrentGoal = MISSION_GOAL_13
			ENDIF
		BREAK
		
		CASE MISSION_GOAL_13
			IF PLAYER_PED_ID() = missionPeds[ped_michael].id
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, TRUE)
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
			ELSE
				SET_ALL_PED_SELECTOR_HINTS_OFF()
			ENDIF
			
			IF GET_NUM_ENEMIES_IN_TRIGGER_BOX(tbLamarGuardsClear) = 0
				IF IS_ENTITY_OK(missionPeds[ped_lamar].id) AND IS_ENTITY_OK(PLAYER_PED_ID())
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), missionPeds[ped_lamar].id) < 5
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							eLamarState = LAMAR_RESCUED
						ELSE
							// this is hacky and could cause bugs OMGHAX @BSW
							IF eFranklinState < FRANKLIN_GO_TO_LAMAR
								eFranklinState = FRANKLIN_GO_TO_LAMAR
								
								eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_SWITCH_TO_FRANKLIN
								
								eAttackSawmillCurrentGoal = MISSION_GOAL_14
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF eLamarState = LAMAR_RESCUED
				eAttackSawmillCurrentGoal = MISSION_GOAL_15
			ENDIF
		BREAK
		CASE MISSION_GOAL_14
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN AND NOT IS_SELECTOR_CAM_ACTIVE()				
				eAttackSawmillCurrentGoal = MISSION_GOAL_13
			ENDIF
		BREAK
		CASE MISSION_GOAL_15
			bRetVal = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bRetVal
ENDFUNC
