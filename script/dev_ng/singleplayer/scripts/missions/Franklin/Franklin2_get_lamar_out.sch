USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
USING "script_ped.sch"
using "commands_streaming.sch"
using "commands_camera.sch"
USING "Commands_clock.sch"
USING "Commands_interiors.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "script_blips.sch"
USING "commands_entity.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "locates_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "seamless_cuts.sch"
USING "CompletionPercentage_public.sch"
using "clearMissionArea.sch"
using "usefulcommands.sch"
using "rgeneral_include.sch"
USING "building_control_public.sch"
USING "vehicle_gen_public.sch"
USING "timelapse.sch"
USING "script_blips.sch"
USING "commands_event.sch"
USING "dialogue_public.sch"
USING "family_public.sch"
USING "player_scene_private.sch"
USING "Franklin2_support.sch"


PROC UPDATE_HANDLE_GET_OUT_INCIDENTAL_DIALOGUE()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR IS_MESSAGE_BEING_DISPLAYED()
		EXIT
	ENDIF
	
	// high priority stuff
	
	// limit this so it doesnt happen EVERY time
	// (this works even though it's constantly generating a rand because 
	// HAS_PED_JUST_KILLED_ only works for 1 frame)
	IF GET_RANDOM_INT_IN_RANGE(0, 100) > 90
		IF HAS_PED_JUST_KILLED_ANY_ENEMY(missionPeds[ped_franklin].id)
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_frKill", 1, missionPeds[ped_franklin].id, "FRANKLIN")
		ENDIF
		IF HAS_PED_JUST_KILLED_ANY_ENEMY(missionPeds[ped_trevor].id)
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_trKill", 1, missionPeds[ped_trevor].id, "TREVOR")
		ENDIF
		IF HAS_PED_JUST_KILLED_ANY_ENEMY(missionPeds[ped_michael].id)
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_miKill", 1, missionPeds[ped_michael].id, "MICHAEL")
		ENDIF
	ENDIF

	IF GET_TIMER_IN_SECONDS_SAFE(tmrIncidentalDialogueThrottle) > fIncidentalDialogueTime	
		IF GET_NUM_LIVING_ENEMIES() > 1
			INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 100)
			// TREVOR
			IF iRandom < 10
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_enc2", 2, missionPeds[ped_trevor].id, "TREVOR")
			// MICHAEL
			ELIF iRandom < 25
				IF GET_RANDOM_INT_IN_RANGE(0, 10) < 8
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_snipe1", 0, missionPeds[ped_michael].id, "MICHAEL")
				ELSE
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_snipe3", 0, missionPeds[ped_michael].id, "MICHAEL")
				ENDIF
			// LAMAR
			ELIF iRandom < 50
				IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_MORE", 3, missionPeds[ped_lamar].id, "LAMAR")
				ELSE
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_SPEAK", 3, missionPeds[ped_lamar].id, "LAMAR")
				ENDIF
			// ENEMIES
			ELIF iRandom < 80
				IF GET_NUM_LIVING_ENEMIES() > 2
					STRING sDialogue, sSpeakerName
					INT iSpeakerNum
					SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
						CASE 0
							sDialogue = "Lm2_attack1"
							sSpeakerName = "fr2_enemy1"
							iSpeakerNum = 5
						BREAK
						CASE 1
							sDialogue = "Lm2_attack2"
							sSpeakerName = "fr2_enemy2"
							iSpeakerNum = 4
						BREAK
						CASE 2
							sDialogue = "Lm2_attack3"
							sSpeakerName = "fr2_enemy3"
							iSpeakerNum = 3
						BREAK
					ENDSWITCH
					PED_INDEX speaker
					speaker = GET_FIRST_LIVING_ENEMY()
					IF IS_ENTITY_OK(speaker)
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA(sDialogue, iSpeakerNum, speaker, sSpeakerName)
					ENDIF
				ENDIF
			ENDIF
			
			fIncidentalDialogueTime = GET_RANDOM_FLOAT_IN_RANGE(3, 8)
		ELSE
			IF GET_RANDOM_INT_IN_RANGE(0, 10) < 7
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_SPEAK", 3, missionPeds[ped_lamar].id, "LAMAR")
			ENDIF
			
			fIncidentalDialogueTime = GET_RANDOM_FLOAT_IN_RANGE(10, 20)
		ENDIF
		
		
		START_TIMER_NOW_SAFE(tmrIncidentalDialogueThrottle)
	ENDIF
	
ENDPROC

PROC SPAWN_CAR_ARRIVALS()
	vehicle[veh_EnemyArrival1].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_FELON), vEnemyArrival1SpawnPosition, fEnemyArrival1SpawnHeading)	// this is the one that drives up into the sawmill
	vehicle[veh_EnemyArrival2].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BUCCANEER), vEnemyArrival2SpawnPosition, fEnemyArrival2SpawnHeading)
	
	IF IS_VEHICLE_DRIVEABLE(vehicle[veh_EnemyArrival1].id)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_EnemyArrival1].id)
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(0, strVehicleRecordingFile)
				enemyPeds[iCar1Driver].id = CREATE_PED_INSIDE_VEHICLE(vehicle[veh_EnemyArrival1].id, pedtype_mission, GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BALLA), VS_DRIVER)
				enemyPeds[iCar1Passenger].id = CREATE_PED_INSIDE_VEHICLE(vehicle[veh_EnemyArrival1].id, pedtype_mission, GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BALLA), VS_FRONT_RIGHT)
				
				SET_UP_ENEMY_PED(enemyPeds[iCar1Driver])
				SET_UP_ENEMY_PED(enemyPeds[iCar1Passenger])
				
				GIVE_WEAPON_TO_PED(enemyPeds[iCar1Driver].id, WEAPONTYPE_ASSAULTSHOTGUN, 150, TRUE)
				GIVE_WEAPON_TO_PED(enemyPeds[iCar1Passenger].id, WEAPONTYPE_SMG, 150, TRUE)
				
				START_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_EnemyArrival1].id, 0, strVehicleRecordingFile)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehicle[veh_EnemyArrival2].id)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_EnemyArrival2].id)
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, strVehicleRecordingFile)				
				enemyPeds[iCar2Driver].id = CREATE_PED_INSIDE_VEHICLE(vehicle[veh_EnemyArrival2].id, pedtype_mission, GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BALLA), VS_DRIVER)
				enemyPeds[iCar2Passenger].id = CREATE_PED_INSIDE_VEHICLE(vehicle[veh_EnemyArrival2].id, pedtype_mission, GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BALLA), VS_FRONT_RIGHT)
				
				SET_UP_ENEMY_PED(enemyPeds[iCar2Driver])
				SET_UP_ENEMY_PED(enemyPeds[iCar2Passenger])
				
				GIVE_WEAPON_TO_PED(enemyPeds[iCar2Driver].id, WEAPONTYPE_PISTOL, 150, TRUE)
				GIVE_WEAPON_TO_PED(enemyPeds[iCar2Passenger].id, WEAPONTYPE_PISTOL, 150, TRUE)
				
				START_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_EnemyArrival2].id, 1, strVehicleRecordingFile)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_CAR_ARRIVALS()
	IF NOT bCarArrivalsSpawned
		SPAWN_CAR_ARRIVALS()
		
		bCarArrivalsSpawned = TRUE
	ELSE
		IF DOES_ENTITY_EXIST(vehicle[veh_EnemyArrival1].id)
		AND IS_VEHICLE_DRIVEABLE(vehicle[veh_EnemyArrival1].id)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_EnemyArrival1].id)
				IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE(vehicle[veh_EnemyArrival1].id) > 95.0
					STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_EnemyArrival1].id)
					TASK_EXIT_VEHICLE_THEN_DEFEND(enemyPeds[iCar1Driver].id, vehicle[veh_EnemyArrival1].id)
					TASK_EXIT_VEHICLE_THEN_DEFEND(enemyPeds[iCar1Passenger].id, vehicle[veh_EnemyArrival1].id)
				ENDIF				
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehicle[veh_EnemyArrival2].id)
		AND IS_VEHICLE_DRIVEABLE(vehicle[veh_EnemyArrival2].id)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_EnemyArrival2].id)
				IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE(vehicle[veh_EnemyArrival2].id) > 95.0
					STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_EnemyArrival2].id)
					TASK_EXIT_VEHICLE_THEN_DEFEND(enemyPeds[iCar2Driver].id, vehicle[veh_EnemyArrival2].id)
					TASK_EXIT_VEHICLE_THEN_DEFEND(enemyPeds[iCar2Passenger].id, vehicle[veh_EnemyArrival2].id)
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_TREVOR_GET_OUT()
	IF PLAYER_PED_ID() = missionPeds[ped_trevor].id
		eTrevorState = TREVOR_COMBAT_INIT
		EXIT
	ENDIF
	IF NOT IS_ENTITY_OK(missionPeds[ped_trevor].id)
		EXIT
	ENDIF
	IF NOT IS_ENTITY_OK(missionPeds[ped_franklin].id)
		EXIT
	ENDIF

	SEQUENCE_INDEX iSeq
	
	SWITCH eTrevorState
		CASE TREVOR_SETUP
			eTrevorState = TREVOR_COMBAT_INIT
		BREAK
		CASE TREVOR_COMBAT_INIT
			eTrevorState = TREVOR_START_COMBAT
		BREAK
		CASE TREVOR_START_COMBAT
			CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_ATTACK - tasking Trevor combat!")
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(missionPeds[ped_trevor].id, FALSE)
			
			REGISTER_ENEMIES_FOR_PED(missionPeds[ped_trevor].id)
			
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_LEAVE_ANY_VEHICLE(NULL)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200)
			CLOSE_SEQUENCE_TASK(iSeq)
			
			TASK_PERFORM_SEQUENCE(missionPeds[ped_trevor].id, iSeq)
			CLEAR_SEQUENCE_TASK(iSeq)
			
			eTrevorState = TREVOR_FIGHT_ON_FOOT
		BREAK
		CASE TREVOR_FIGHT_ON_FOOT
			SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(missionPeds[ped_trevor].id, missionPeds[ped_franklin].id, <<7, 7, 7>>, <<-7, -7, -7>>, 8, FALSE)
			
			IF NOT IS_PED_IN_COMBAT(missionPeds[ped_trevor].id)
				CPRINTLN(DEBUG_MISSION, "tasking Trevor back into combat")
				eTrevorState = TREVOR_COMBAT_INIT
			ENDIF
		BREAK
		CASE TREVOR_ESCAPE
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTrevorExitCoverPosition, PEDMOVE_RUN, -1)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, vTrevorExitCoverPosition, INFINITE_TASK_TIME, DEFAULT, DEFAULT, DEFAULT, DEFAULT, covTrevorExit, TRUE)
			CLOSE_SEQUENCE_TASK(iSeq)
			TASK_PERFORM_SEQUENCE(missionPeds[ped_trevor].id, iSeq)
			CLEAR_SEQUENCE_TASK(iSeq)
			
			eTrevorState = TREVOR_COMPLETE
		BREAK
		CASE TREVOR_COMPLETE
			
		BREAK
	ENDSWITCH

ENDPROC

PROC UPDATE_LAMAR_GET_OUT()
	SWITCH eLamarState
		CASE LAMAR_ESCAPE_INIT
			IF IS_ENTITY_OK(missionPeds[ped_lamar].id)				
				CLEAR_PED_TASKS(missionPeds[ped_lamar].id)
				
				GIVE_WEAPON_TO_PED(missionPeds[ped_lamar].id, WEAPONTYPE_PISTOL, 300, TRUE)
				
				SET_PED_RELATIONSHIP_GROUP_HASH(missionPeds[ped_lamar].id, RELGROUPHASH_PLAYER)
				
				SET_PED_AS_GROUP_MEMBER(missionPeds[ped_lamar].id, GET_PLAYER_GROUP(PLAYER_ID()))
				SET_PED_NEVER_LEAVES_GROUP(missionPeds[ped_lamar].id, TRUE)
				
				CREATE_CONVERSATION_EXTRA("LM2_give", 3, missionPeds[ped_lamar].id, "LAMAR")				
				
				SET_PED_MOVEMENT_CLIPSET(missionPeds[ped_lamar].id, "MOVE_INJURED_GENERIC")
				
				missionPeds[ped_lamar].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_lamar].id)
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(missionPeds[ped_lamar].id, 100)
				
				eLamarState = LAMAR_ESCAPING
			ENDIF
		BREAK
		CASE LAMAR_ESCAPING
			IF IS_ENTITY_OK(missionPeds[ped_lamar].id) AND IS_ENTITY_OK(missionPeds[ped_franklin].id)
				IF GET_DISTANCE_BETWEEN_ENTITIES(missionPeds[ped_lamar].id, missionPeds[ped_franklin].id) > 15
					TASK_GO_TO_ENTITY(missionPeds[ped_lamar].id, missionPeds[ped_franklin].id)
					eLamarState = LAMAR_CATCHING_UP
				ENDIF
				
				SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(missionPeds[ped_lamar].id, missionPeds[ped_franklin].id, <<3, 0, 3>>, <<-3, 0, -3>>, 3, FALSE)
			ENDIF
		BREAK
		CASE LAMAR_CATCHING_UP
			IF IS_ENTITY_OK(missionPeds[ped_lamar].id) AND IS_ENTITY_OK(missionPeds[ped_franklin].id)
				IF GET_DISTANCE_BETWEEN_ENTITIES(missionPeds[ped_lamar].id, missionPeds[ped_franklin].id) < 10
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(missionPeds[ped_lamar].id, 100)
					eLamarState = LAMAR_ESCAPING
				ENDIF
			ENDIF
		BREAK
		CASE LAMAR_GO_TO_CAR
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_HANDLE_GET_OUT_MESSAGING()
	IF IS_SELECTOR_CAM_ACTIVE()
		EXIT
	ENDIF
	
	SWITCH eGetLamarOutCurrentDialogue
		CASE GET_LAMAR_OUT_MESSAGE_LEAD_LAMAR_OUT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIf
				mission_blip = CREATE_BLIP_FOR_COORD(vLeaveSawmillAreaGoal)
				
				PRINT("FRAN2_leadL", DEFAULT_GOD_TEXT_TIME, 1)
				
				eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_IDLE
			ENDIF
		BREAK
		CASE GET_LAMAR_OUT_MESSAGE_MICHAEL_LEAVES
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_mbye", 0, missionPeds[ped_michael].id, "MICHAEL")
					eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		CASE GET_LAMAR_OUT_MESSAGE_TREVOR_SAYS_BYE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_tbye", 2, missionPeds[ped_trevor].id, "TREVOR")
					SET_BITMASK_AS_ENUM(iMissionDialogueFlags, TREVOR_TrevorSaysBye)
					eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		CASE GET_LAMAR_OUT_MESSAGE_IDLE
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DO_GET_LAMAR_OUT()
	BOOL bRetVal = FALSE
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
	
	UPDATE_TREVOR_GET_OUT()
	UPDATE_MICHAEL_PED()
	
	UPDATE_LAMAR_GET_OUT()
	
	FAIL(FAIL_LAMAR_KILLED)
	FAIL(FAIL_TREVOR_KILLED)
	FAIL(FAIL_MICHAEL_KILLED)
	FAIL(FAIL_FRANKLIN_KILLED)
	FAIL(FAIL_BULLDOZER_KILLED)
	
	UPDATE_CAR_ARRIVALS()
	
	UPDATE_HANDLE_GET_OUT_MESSAGING()
	UPDATE_HANDLE_GET_OUT_INCIDENTAL_DIALOGUE()
	UPDATE_ENEMY_BLIPS()
	
	IF g_bMagDemoActive
		IF IS_ENTITY_OK(missionPeds[ped_lamar].id)
			IF IS_PED_IN_ANY_VEHICLE(missionPeds[ped_lamar].id)
				TRIGGER_MUSIC_EVENT("FRA2_END_ON_FOOT")
				MISSION_PASSED()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_BITMASK_AS_ENUM_SET(iMissionDialogueFlags, TREVOR_TrevorSaysBye)
		eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_TREVOR_SAYS_BYE
		eTrevorState = TREVOR_ESCAPE
	ENDIF

	SWITCH eGetLamarOutCurrentGoal
		CASE MISSION_GOAL_01					
			eLamarState = LAMAR_ESCAPE_INIT
			eMichaelState = MICHAEL_SETUP
			
			IF IS_ENTITY_OK(missionPeds[ped_michael].id)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(missionPeds[ped_michael].id, TRUE)
			ENDIF
			IF IS_ENTITY_OK(missionPeds[ped_trevor].id)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(missionPeds[ped_trevor].id, TRUE)
			ENDIF
			
			START_TIMER_NOW_SAFE(tmrLamarChasersSpawn)
			
			START_TIMER_NOW_SAFE(tmrIncidentalDialogueThrottle)
			
			eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_LEAD_LAMAR_OUT
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_PROTECT_LAMAR")
				START_AUDIO_SCENE("FRANKLIN_2_PROTECT_LAMAR")
			ENDIF
			
			TRIGGER_MUSIC_EVENT("FRA2_GOT_LAMAR")
			
			eGetLamarOutCurrentGoal = MISSION_GOAL_02
		BREAK
		CASE MISSION_GOAL_02
			IF GET_TIMER_IN_SECONDS(tmrLamarChasersSpawn) > 2
				SPAWN_ENEMY_SQUAD(SQUAD_LamarChasers1)
				SET_UP_ENEMY_PEDS(SQUAD_LamarChasers1)
				TASK_SQUAD_ATTACK_FRANKLIN_AROUND_LAMAR(SQUAD_LamarChasers1)
				
				eTrevorState = TREVOR_START_COMBAT

				eGetLamarOutCurrentGoal = MISSION_GOAL_03
			ENDIF
		BREAK
		CASE MISSION_GOAL_03
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(missionPeds[ped_franklin].id, vLeaveSawmillAreaGoal) < 100
				SPAWN_ENEMY_SQUAD(SQUAD_LamarChasers2)
				SET_UP_ENEMY_PEDS(SQUAD_LamarChasers2)
				TASK_SQUAD_ATTACK_FRANKLIN_AROUND_LAMAR(SQUAD_LamarChasers2)
				
				PRE_STREAM_ASSETS_FOR_STAGE(STAGE_DRIVE_HOME)
				
				eGetLamarOutCurrentGoal = MISSION_GOAL_04
			ENDIF
		BREAK
		CASE MISSION_GOAL_04
			IF IS_TIMER_STARTED(tmrAlliesFinished)
				IF TIMER_DO_ONCE_WHEN_READY(tmrAlliesFinished, 2)
					eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_MICHAEL_LEAVES
					eMichaelState = MICHAEL_ESCAPE
					
					eTrevorState = TREVOR_ESCAPE
				ENDIF
			ELSE
				IF GET_NUM_LIVING_ENEMIES() = 0 AND eMichaelState < MICHAEL_ESCAPE
					START_TIMER_NOW_SAFE(tmrAlliesFinished)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_AT_LOCATION_ANY_MEANS(locates_data, vLeaveSawmillAreaGoal, (<<10, 10, LOCATE_SIZE_HEIGHT>>), FALSE, "FRAN2_leadL")
				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIF
				
				// this might be redundant with above if we already hit num_enemies=0
				// but that should be OK
				eTrevorState = TREVOR_ESCAPE
				IF NOT IS_BITMASK_AS_ENUM_SET(iMissionDialogueFlags, TREVOR_TrevorSaysBye)
					eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_TREVOR_SAYS_BYE
				ENDIF
				
				IF NOT IS_BITMASK_AS_ENUM_SET(iMissionDialogueFlags, TREVOR_TrevorSaysBye)
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("LM2_tbye", 2, missionPeds[ped_trevor].id, "TREVOR")
					SET_BITMASK_AS_ENUM(iMissionDialogueFlags, TREVOR_TrevorSaysBye)
				ENDIF
				
				STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_ALERT")
				STOP_AUDIO_SCENE("FRANKLIN_2_PROTECT_LAMAR")
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_GO_TO_FRANKLINS")
					START_AUDIO_SCENE("FRANKLIN_2_GO_TO_FRANKLINS")
				ENDIF
				
				TRIGGER_MUSIC_EVENT("FRA2_RETURN_LAMAR")
				
				// mission end for magdemo
				IF g_bMagDemoActive
					TRIGGER_MUSIC_EVENT("FRA2_MAGDEMO_END")
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "BSW Frank and Lamar are at the exit, advance to the next stage")

				eGetLamarOutCurrentGoal = MISSION_GOAL_05
			ENDIF
		BREAK
		CASE MISSION_GOAL_05
			bRetVal = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bRetVal
ENDFUNC
