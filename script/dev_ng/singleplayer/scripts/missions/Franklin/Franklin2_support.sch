USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "commands_streaming.sch"
using "commands_camera.sch"
USING "Commands_clock.sch"
USING "Commands_interiors.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "script_blips.sch"
USING "commands_entity.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "locates_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "seamless_cuts.sch"
USING "CompletionPercentage_public.sch"
using "clearMissionArea.sch"
using "usefulcommands.sch"
using "rgeneral_include.sch"
USING "building_control_public.sch"
USING "vehicle_gen_public.sch"
USING "timelapse.sch"
USING "script_blips.sch"
USING "commands_event.sch"
USING "dialogue_public.sch"
USING "family_public.sch"
USING "player_scene_private.sch"
USING "trigger_box.sch"


ENUM enumFails
	FAIL_NULL,
	FAIL_COVER_BLOWN,
	FAIL_LAMAR_KILLED,
	FAIL_TREVOR_KILLED,
	FAIL_MICHAEL_KILLED,
	FAIL_FRANKLIN_KILLED,
	FAIL_BULLDOZER_KILLED,
	FAIL_KILL_TANISHA
ENDENUM

ENUM enumMissionStage
	STAGE_INTRO_CUTSCENE,
	STAGE_MEET_THE_CREW,
	STAGE_GET_INTO_POSITION,			
	STAGE_ATTACK_SAWMILL,						
	STAGE_GET_LAMAR_OUT,
	STAGE_DRIVE_HOME,
	STAGE_OUTRO_CUTSCENE				
ENDENUM

ENUM enumEndState
	END_STATE_PASSED,
	END_STATE_FAILED,
	END_STATE_STAGE_SKIP,
	END_STATE_MISSION_ONGOING
ENDENUM


ENUM MISSION_STAGE_GOAL
	MISSION_GOAL_01,
	MISSION_GOAL_02,
	MISSION_GOAL_03,
	MISSION_GOAL_04,
	MISSION_GOAL_05,
	MISSION_GOAL_06,
	MISSION_GOAL_07,
	MISSION_GOAL_08,
	MISSION_GOAL_09,
	MISSION_GOAL_10,
	MISSION_GOAL_11,
	MISSION_GOAL_12,
	MISSION_GOAL_13,
	MISSION_GOAL_14,
	MISSION_GOAL_15,
	MISSION_GOAL_COMPLETE
ENDENUM

ENUM MEET_CREW_MESSAGE
	MEET_CREW_MESSAGE_GO_TO_SAWMILL,
	MEET_CREW_MESSAGE_IDLE
ENDENUM

ENUM GET_INTO_POSITION_MESSAGE
	GET_INTO_POSITION_MESSAGE_DIA_TREVOR_TO_BULLDOZER,
	GET_INTO_POSITION_MESSAGE_GET_IN_POSITION,
	GET_INTO_POSITION_MESSAGE_DIA_TREVOR_SPOTS_BULLDOZER,
	GET_INTO_POSITION_MESSAGE_DIA_TREVOR_PLANS_BULLDOZER,
	GET_INTO_POSITION_MESSAGE_WAIT_FOR_TREVOR,
	GET_INTO_POSITION_MESSAGE_IDLE
ENDENUM

ENUM ATTACK_MESSAGE
	ATTACK_MESSAGE_DIA_TREVOR_USES_BULLDOZER,
	ATTACK_MESSAGE_ESCORT_BULLDOZER,
	ATTACK_MESSAGE_PUSH_CARS,
	ATTACK_MESSAGE_MICHAEL_ARRIVES,
	ATTACK_MESSAGE_RPG_SHOT_1,
	ATTACK_MESSAGE_BULLDOZER_END,
	ATTACK_MESSAGE_PROTECT_F_AND_T,
	ATTACK_MESSAGE_MICHAEL_SAYS_FIND_LAMAR,
	ATTACK_MESSAGE_FIND_LAMAR,
	ATTACK_MESSAGE_TREVOR_REACHES_LAMAR,
	ATTACK_MESSAGE_FRANKLIN_REACHES_LAMAR,
	ATTACK_MESSAGE_CLEAR_GUARDS,
	ATTACK_MESSAGE_SWITCH_TO_FRANKLIN,
	ATTACK_MESSAGE_IDLE
ENDENUM

ENUM GET_LAMAR_OUT_MESSAGE
	GET_LAMAR_OUT_MESSAGE_MICHAEL_LEAVES,
	GET_LAMAR_OUT_MESSAGE_LEAD_LAMAR_OUT,
	GET_LAMAR_OUT_MESSAGE_TREVOR_SAYS_BYE,
	GET_LAMAR_OUT_MESSAGE_IDLE
ENDENUM

ENUM DRIVE_HOME_MESSAGE
	DRIVE_HOME_MESSAGE_BANTER1,
	DRIVE_HOME_MESSAGE_BANTER2,
	DRIVE_HOME_MESSAGE_BANTER3,
	DRIVE_HOME_MESSAGE_BANTER4,
	DRIVE_HOME_MESSAGE_IDLE
ENDENUM

ENUM MISSION_PED_TREVOR
	TREVOR_INVALID,
	TREVOR_SETUP,
	TREVOR_GO_TO_START,
	TREVOR_GOING_TO_START,
	TREVOR_GOING_BULLDOZER,
	TREVOR_COMBAT_INIT,
	TREVOR_ENTER_BULLDOZER,
	TREVOR_ENTERING_BULLDOZER,
	TREVOR_DRIVE_BULLDOZER,
	TREVOR_START_COMBAT,
	TREVOR_FIGHT_ON_FOOT,
	TREVOR_ESCAPE,
	TREVOR_COMPLETE
ENDENUM

ENUM MISSION_PED_FRANKLIN
	FRANKLIN_INVALID,
	FRANKLIN_SETUP,
	FRANKLIN_COMBAT_INIT,
	FRANKLIN_COMBAT,
	FRANKLIN_GO_TO_LAMAR,
	FRANKLIN_COMPLETE
ENDENUM

ENUM MISSION_PED_MICHAEL
	MICHAEL_INVALID,
	MICHAEL_SETUP,
	MICHAEL_COMBAT_INIT,
	MICHAEL_SHOOT,
	MICHAEL_AIM,
	MICHAEL_ESCAPE,
	MICHAEL_COMPLETE
ENDENUM

ENUM MISSION_PED_LAMAR
	LAMAR_INVALID,
	LAMAR_AT_GUNPOINT,
	LAMAR_WAITING_IN_COVER,
	LAMAR_RESCUED,
	LAMAR_ESCAPE_INIT,
	LAMAR_ESCAPING,
	LAMAR_CATCHING_UP,
	LAMAR_GO_TO_CAR,
	LAMAR_COMPLETE
ENDENUM

ENUM MISSION_VEH_BULLDOZER
	BULLDOZER_INVALID,
	BULLDOZER_SETUP,
	BULLDOZER_STAGE1,
	BULLDOZER_STAGE2,
	BULLDOZER_STAGE3,
	BULLDOZER_FINISHED
ENDENUM

ENUM RPG_ATTACK_STATE
	RPG_STATE_INVALID,
	RPG_STATE_INIT,
	RPG_STATE_AIMING,
	RPG_STATE_WARNING_SHOT,
	RPG_STATE_WARNING_DIALOGUE,
	RPG_STATE_KILL_SHOT,
	RPG_STATE_COMPLETE
ENDENUM

ENUM PLAYER_PED_DIALOGUE_FLAGS
	TREVOR_BarricadeSmash1 = BIT0,
	TREVOR_BarricadeSmash2 = BIT1,
	TREVOR_DozerDamage1 = BIT2,
	TREVOR_DozerDamage2 = BIT3,
	PLAYER_SaysHiToLamar = BIT4,
	TREVOR_TrevorSaysBye = BIT5
ENDENUM

ENUM enumSwitchState
	SWITCH_SETUP,
	SWITCH_SELECTING,
	SWITCH_SWAP_PLAYER_PEDS,
	SWITCH_COMPLETE,
	SWITCH_NULL
ENDENUM

ENUM ADVANCE_STAGE_STAGE
	ADVANCE_INITIALISE,
	ADVANCE_STREAM,
	ADVANCE_CLEAN_UP,
	ADVANCE_CREATE_PLAYER_VEHICLES,
	ADVANCE_CREATE_FRANKLIN,
	ADVANCE_CREATE_TREVOR,
	ADVANCE_CREATE_MICHAEL,
	ADVANCE_CREATE_LAMAR,
	ADVANCE_LOAD_ENVIRONMENT,
	ADVANCE_SET_UP,
	ADVANCE_FINISHED
ENDENUM

ENUM FRA2_MODEL_ENUM
	FM_SURGE,
	FM_TANISHA,
	FM_TANISHA_DRIVER,
	FM_LAMAR,
	FM_BULLDOZER,
	FM_BISON,
	FM_BUCCANEER,
	FM_FELON,
	FM_HAULER,
	FM_BALLA,
	FM_BARREL
ENDENUM

ENUM FRA2_WAYPOINTS
	FW_BULLDOZER,
	FW_TREVOR
ENDENUM

ENUM FRA2_WEAPON_ENUM
	FW_WEAPON_PISTOL,
	FW_WEAPON_SMG,
	FW_WEAPON_ASSAULT_SHOTGUN,
	FW_WEAPON_HEAVYSNIPER
ENDENUM

STRUCT StructPed
	PED_INDEX id
	AI_BLIP_STRUCT aiBlip
	INT iSquad
ENDSTRUCT

STRUCT structVehicle
	VEHICLE_INDEX id
ENDSTRUCT

STRUCT SPAWN_POINT
	VECTOR vCoords
	FLOAT fHeading
	INT iSquad
	WEAPON_TYPE iWeaponType
ENDSTRUCT

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgAccuracyControls
#ENDIF

CONST_FLOAT	SPEED_VerySlow		1.0
CONST_FLOAT	SPEED_Slow			2.0
CONST_FLOAT	SPEED_Medium		3.0
CONST_FLOAT	SPEED_Fast			4.0

CONST_INT	MAX_PEDS 5
CONST_INT	MAX_VEHICLES 21

CONST_INT	SQUAD_Wave1			1
CONST_INT	SQUAD_Wave2			2
CONST_INT	SQUAD_Wave3			3
CONST_INT	SQUAD_Wave4			4
CONST_INT	SQUAD_Wave5			5
CONST_INT	SQUAD_Barricade1	6
CONST_INT	SQUAD_Barricade2	7
CONST_INT	SQUAD_Wave6			8
CONST_INT	SQUAD_LamarGuards	9
CONST_INT	SQUAD_SnipeTargets	10
CONST_INT	SQUAD_LamarChasers1	11
CONST_INT	SQUAD_LamarChasers2	12
CONST_INT	SQUAD_CarArrive1	13
CONST_INT	SQUAD_CarArrive2	14
CONST_INT	SQUAD_RPGSquad		15
CONST_INT	SQUAD_PreGuards		16

CONST_INT MAX_SAWMILL_ENEMIES 65

CONST_INT ped_franklin 0
CONST_INT ped_trevor 1
CONST_INT ped_michael 2
CONST_INT ped_lamar 3

CONST_INT NUMBER_RECORDING_REQUEST_SLOTS 2

CONST_INT	SIGHTRANGE_BLIND 5

CONST_INT iBARRICADECHARGEDISTANCE 20
CONST_INT veh_franklin 0
CONST_INT veh_michael 1
CONST_INT veh_Trevor 2
CONST_INT vehTrain 3
CONST_INT veh_bisonFront 4
CONST_INT veh_bisonFront2 5
CONST_INT veh_carblock1 6
CONST_INT veh_carblock2 7
CONST_INT veh_bulldozer 8
CONST_INT veh_drugdeal 9
CONST_INT veh_cover1 10
CONST_INT veh_cover2 11
CONST_INT veh_cover3 12
CONST_INT veh_cover4 13
CONST_INT veh_cover5 14
CONST_INT veh_blocker1 15
CONST_INT veh_blocker2 16
CONST_INT veh_blocker3 17
CONST_INT veh_blocker4 18
CONST_INT veh_EnemyArrival1 20
CONST_INT veh_EnemyArrival2 10

CONST_INT objgas1 0
CONST_INT objgas2 1
CONST_INT objgas3 2
CONST_INT objgas4 3
CONST_INT objgas5 4
CONST_INT objFrontBarrel1 5
CONST_INT objFrontBarrel2 6
CONST_INT objFrontBarrel3 7
CONST_INT objFrontBarrel4 8

REL_GROUP_HASH relgroupIgnore

ADVANCE_STAGE_STAGE eAdvanceStageState

LOCATES_HEADER_DATA locates_data

enumMissionStage eCurrentStage
enumMissionStage eSkipToStage

SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sCamDetails

MISSION_PED_TREVOR eTrevorState = TREVOR_INVALID
MISSION_PED_FRANKLIN eFranklinState = FRANKLIN_INVALID
MISSION_PED_MICHAEL eMichaelState = MICHAEL_INVALID
MISSION_PED_LAMAR eLamarState = LAMAR_INVALID

RPG_ATTACK_STATE eRPGState = RPG_STATE_INIT

MISSION_VEH_BULLDOZER eBulldozerState = BULLDOZER_INVALID

MISSION_STAGE_GOAL eInitialiseCurrentGoal = MISSION_GOAL_01
MISSION_STAGE_GOAL eIntroCutsceneCutsceneGoal = MISSION_GOAL_01
MISSION_STAGE_GOAL eSawmillSceneCurrentGoal = MISSION_GOAL_01
MISSION_STAGE_GOAL eMeetCrewCurrentGoal = MISSION_GOAL_01
MISSION_STAGE_GOAL eGetIntoPositionCurrentGoal = MISSION_GOAL_01
MISSION_STAGE_GOAL eAttackSawmillCurrentGoal = MISSION_GOAL_01
MISSION_STAGE_GOAL eGetLamarOutCurrentGoal = MISSION_GOAL_01
MISSION_STAGE_GOAL eDriveHomeCurrentGoal = MISSION_GOAL_01
MISSION_STAGE_GOAL eOutroCutsceneCurrentGoal = MISSION_GOAL_01

GET_INTO_POSITION_MESSAGE eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_IDLE
MEET_CREW_MESSAGE eMeetCrewCurrentDialogue = MEET_CREW_MESSAGE_IDLE
ATTACK_MESSAGE eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
GET_LAMAR_OUT_MESSAGE eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_IDLE
DRIVE_HOME_MESSAGE eDriveHomeCurrentDialogue = DRIVE_HOME_MESSAGE_IDLE

StructPed missionPeds[MAX_PEDS]
structVehicle vehicle[MAX_VEHICLES]

SPAWN_POINT enemySpawnPoints[MAX_SAWMILL_ENEMIES]
StructPed enemyPeds[MAX_SAWMILL_ENEMIES]

enumSwitchState switchState 

structTimer tmrTrevorLookAtInDozer
structtimer	tmrTrevorDialogueThrottle
structtimer	tmrFranklinDialogueThrottle
structTimer tmrIncidentalDialogueThrottle
structTimer tmrLamarChasersSpawn
structTimer tmrMichaelKillTimer
structTimer tmrLamarExecution
structTimer tmrFranklinAbandonTimer

structTimer tmrBulldozerForceDrive
structTimer tmrMichaelActive
structTimer tmrAlliesFinished
structTimer tmrDriveHomeBanterStart
structTimer tmrRPGScene

TRIGGER_BOX tbTriggerWave1
TRIGGER_BOX tbTriggerWave2
TRIGGER_BOX tbTriggerWave3
TRIGGER_BOX tbTriggerWave4
TRIGGER_BOX tbTriggerWave5
TRIGGER_BOX tbTriggerWave6

TRIGGER_BOX tbBulldozerInRPGRange

TRIGGER_BOX tbMichaelAppears

TRIGGER_BOX tbFirstCombatZone
TRIGGER_BOX tbSecondCombatZone
TRIGGER_BOX tbBulldozerEnd

TRIGGER_BOX tbSawmillBackSafety

TRIGGER_BOX tbLamarGuardsClear
TRIGGER_BOX tbLamarGuardsEntrance
TRIGGER_BOX tbEntireSawmill

PED_INDEX ped_tanisha
PED_INDEX ped_tanisha_drive

VEHICLE_INDEX veh_tanisha
VEHICLE_INDEX stat_player_vehicle

BLIP_INDEX mission_blip

OBJECT_INDEX objMichaelsWeapon

COVERPOINT_INDEX covTrevorExit


STRING strVehicleRecordingFile = "fratwo"
STRING convBlock = "LEM2AUD"

BOOL	bCarArrivalsSpawned = FALSE
BOOL	bWaitForTrevor
BOOL 	bSkipInProgress
BOOL bVehicleRecordingRequestTracker[NUMBER_RECORDING_REQUEST_SLOTS]
BOOL bModelRequestTracker[COUNT_OF(FRA2_MODEL_ENUM)]
BOOL bWaypointRequestTracker[COUNT_OF(FRA2_WAYPOINTS)]
BOOL bWeaponRequestTracker[COUNT_OF(FRA2_WEAPON_ENUM)]

VECTOR vLamarInjuredPosition = <<-523.0540, 5309.2811, 79.2679>>
VECTOR vBulldozer2SpawnPosition = <<-540.0828, 5425.9590, 62.3495>>
VECTOR vBulldozer2EndGoal = <<-511.7181, 5240.3496, 79.3045>>
VECTOR vEnemyArrival1SpawnPosition = <<-578.9232, 5252.3105, 69.4674>>
VECTOR vEnemyArrival2SpawnPosition = <<-846.9134, 5315.1313, 76.7078>>
VECTOR vFranklinMeetCrewGoal = <<-727.5588, 5313.5103, 71.4980>>
VECTOR vTrevorGoToPositionPosition = <<-699.4736, 5311.9136, 68.6419>>
VECTOR vMichaelSnipePosition = <<-558.2831, 5196.5093, 92.9929>>
VECTOR vBarricade1Position = <<-586.9476, 5281.4785, 69.2602>>
VECTOR vBarricade2Position = <<-592.0154, 5282.8984, 69.2247>>
VECTOR vBarricade3Position =  <<-551.9666, 5256.6309, 71.7141>>
VECTOR vBarricade4Position = <<-549.5427, 5260.9229, 72.3861>>
VECTOR vTrevorExitCoverPosition = <<-594.4057, 5238.4316, 69.9196>>

VECTOR vLeaveSawmillAreaGoal = <<-590.0426, 5233.0049, 69.8396>>

INT			iUnderSawmillBlocker

INT			statTrackingFlag
INT			iMissionDialogueFlags

INT			iCurrentEnemyAccuracy

INT			iCellphoneEnemy
INT			iDiaBulldozerTargeter
INT			iCar1Driver
INT			iCar1Passenger
INT			iRPGEnemy
INT			iLamarAimGuard

INT			iCar2Driver
INT			iCar2Passenger


INT			iCurrentBarricadeTarget

INT			iMichaelAimTimer = 0
INT			iMichaelShootDelay

INT 		iDefaultEnemyAccuracy = 10
INT 		iDefaultAllyAccuracy = 25
INT 		iLowEnemyAccuracy = 5
INT 		iLowAllyAccuracy = 10

INT			iTrevorRequestHelpTime
INT			iFranklinRequestHelpTime
FLOAT		fIncidentalDialogueTime
FLOAT		fBarricade4Heading = 37.5
FLOAT		fBarricade3Heading = 156
FLOAT		fBarricade2Heading = 325
FLOAT		fBarricade1Heading = 88
FLOAT		vTrevorGoToPositionHeading = 272.1327
FLOAT		fEnemyArrival2SpawnHeading = 287
FLOAT		fEnemyArrival1SpawnHeading = 336
FLOAT		fBulldozer2SpawnHeading = 104.2
FLOAT		fLamarInjuredHeading = 163




FUNC MODEL_NAMES GET_MODEL_FOR_FRA2_MODEL_ENUM(FRA2_MODEL_ENUM FModel)
	SWITCH FModel
		CASE FM_LAMAR
			RETURN GET_NPC_PED_MODEL(CHAR_LAMAR)
		BREAK
		CASE FM_SURGE
			RETURN SURGE
		BREAK
		CASE FM_TANISHA
			RETURN IG_TANISHA
		BREAK
		CASE FM_TANISHA_DRIVER
			RETURN A_M_Y_BUSINESS_01
		BREAK
		CASE FM_BULLDOZER
			RETURN BULLDOZER
		BREAK
		CASE FM_BISON
			RETURN BISON2
		BREAK
		CASE FM_BUCCANEER
			RETURN BUCCANEER
		CASE FM_FELON
			RETURN FELON
		BREAK
		CASE FM_HAULER
			RETURN HAULER
		BREAK
		CASE FM_BALLA
			RETURN G_M_Y_BallaOrig_01
		BREAK
		CASE FM_BARREL
			RETURN prop_barrel_exp_01c
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC STRING GET_WAYPOINT_LABEL(FRA2_WAYPOINTS thisWaypoint)
	SWITCH thisWaypoint
		CASE FW_BULLDOZER
			RETURN "fratwoWP0"
		BREAK
		CASE FW_TREVOR
			RETURN "fratwoWP1"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC WEAPON_TYPE GET_FRA2_WEAPON_TYPE(FRA2_WEAPON_ENUM weapon)
	SWITCH weapon
		CASE FW_WEAPON_PISTOL
			RETURN WEAPONTYPE_PISTOL
		BREAK
		CASE FW_WEAPON_SMG
			RETURN WEAPONTYPE_SMG
		BREAK
		CASE FW_WEAPON_ASSAULT_SHOTGUN
			RETURN WEAPONTYPE_ASSAULTSHOTGUN
		BREAK
		CASE FW_WEAPON_HEAVYSNIPER
			RETURN WEAPONTYPE_HEAVYSNIPER
		BREAK
	ENDSWITCH

	RETURN WEAPONTYPE_UNARMED
ENDFUNC

// is this model required on this stage?
FUNC BOOL IS_MODEL_REQUIRED_FOR_MISSION_STAGE(FRA2_MODEL_ENUM sagModel, enumMissionStage reqStage)
	SWITCH sagModel
		CASE FM_LAMAR
			IF reqStage >= STAGE_ATTACK_SAWMILL
				RETURN TRUE
			ENDIF
		BREAK
		CASE FM_SURGE
		CASE FM_TANISHA
		CASE FM_TANISHA_DRIVER
			IF reqStage <= STAGE_MEET_THE_CREW
				RETURN TRUE
			ENDIF
		BREAK
		CASE FM_BULLDOZER
		CASE FM_BISON
		CASE FM_BUCCANEER
		CASE FM_FELON
		CASE FM_HAULER
		CASE FM_BALLA
		CASE FM_BARREL
			IF reqStage >= STAGE_MEET_THE_CREW AND reqStage < STAGE_OUTRO_CUTSCENE
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// add a request for a model
PROC ADD_MODEL_REQUEST(INT i)
	MODEL_NAMES mModelToRequest = GET_MODEL_FOR_FRA2_MODEL_ENUM(INT_TO_ENUM(FRA2_MODEL_ENUM, i))
	REQUEST_MODEL(mModelToRequest)
	bModelRequestTracker[i] = TRUE
ENDPROC

// remove request
PROC CLEAR_MODEL_REQUEST(INT i)
	IF bModelRequestTracker[i]
		MODEL_NAMES mModelToRequest = GET_MODEL_FOR_FRA2_MODEL_ENUM(INT_TO_ENUM(FRA2_MODEL_ENUM, i))
		IF HAS_MODEL_LOADED(mModelToRequest)
			SET_MODEL_AS_NO_LONGER_NEEDED(mModelToRequest)
		ENDIF
		bModelRequestTracker[i] = FALSE
	ENDIF
ENDPROC

// have all requests for models succeeded?
FUNC BOOL HAVE_ALL_MODEL_REQUESTS_SUCCEEDED()
	INT i 

	// loop through and check for empty
	REPEAT COUNT_OF(FRA2_MODEL_ENUM) i 
		IF bModelRequestTracker[i]
			IF NOT HAS_MODEL_LOADED(GET_MODEL_FOR_FRA2_MODEL_ENUM(INT_TO_ENUM(FRA2_MODEL_ENUM, i)))
				// a full slot is not loaded, return false
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT	
	
	RETURN TRUE
ENDFUNC

// request the models for a particular stage
FUNC BOOL LOAD_MODELS_FOR_STAGE(enumMissionStage requestStage)

	INT i 
	REPEAT COUNT_OF(FRA2_MODEL_ENUM) i 
		IF IS_MODEL_REQUIRED_FOR_MISSION_STAGE(INT_TO_ENUM(FRA2_MODEL_ENUM, i), requestStage)
			// if we need this model, request it
			ADD_MODEL_REQUEST(i)
		ELSE	
			// otherwise try and get rid of it
			IF NOT IS_MODEL_REQUIRED_FOR_MISSION_STAGE(INT_TO_ENUM(FRA2_MODEL_ENUM, i), eCurrentStage)
				CLEAR_MODEL_REQUEST(i)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF HAVE_ALL_MODEL_REQUESTS_SUCCEEDED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// is this recording required on this stage?
FUNC BOOL IS_RECORDING_REQUIRED_FOR_MISSION_STAGE(INT iRec, enumMissionStage reqStage)
	SWITCH iRec
		CASE 000
		CASE 001 
			IF reqStage = STAGE_GET_LAMAR_OUT
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// add a request for a recording
PROC ADD_RECORDING_REQUEST(INT iRec)
	CPRINTLN(DEBUG_MISSION, "ADD_RECORDING_REQUEST adding request for recording #", iRec)
	bVehicleRecordingRequestTracker[iRec] = TRUE
	REQUEST_VEHICLE_RECORDING(iRec, strVehicleRecordingFile)
ENDPROC

// clear a recording request slot
PROC CLEAR_RECORDING_REQUEST(INT iRec)
	IF bVehicleRecordingRequestTracker[iRec]
		bVehicleRecordingRequestTracker[iRec] = FALSE
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iRec, strVehicleRecordingFile)
			REMOVE_VEHICLE_RECORDING(iRec, strVehicleRecordingFile)
		ENDIF
	ENDIF
ENDPROC

// have all requests for recordings succeeded?
FUNC BOOL HAVE_ALL_RECORDING_REQUESTS_SUCCEEDED()
	INT i 
	
	// loop through and check for empty
	REPEAT NUMBER_RECORDING_REQUEST_SLOTS i 
		IF bVehicleRecordingRequestTracker[i]
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(i, strVehicleRecordingFile)
				// a full slot is not loaded, return false
				RETURN FALSE
			ELSE
				CPRINTLN(DEBUG_MISSION, "HAVE_ALL_RECORDING_REQUESTS_SUCCEEDED recording #", i, " is loaded")
			ENDIF
		ENDIF
	ENDREPEAT	
	
	RETURN TRUE
ENDFUNC

// get recordings for particular stage
FUNC BOOL LOAD_VEHICLE_RECORDINGS_FOR_STAGE(enumMissionStage requestStage)

	INT i 
	REPEAT NUMBER_RECORDING_REQUEST_SLOTS i 
		IF IS_RECORDING_REQUIRED_FOR_MISSION_STAGE(i, requestStage)
			// if we need this recording, request it
			ADD_RECORDING_REQUEST(i)
		ELSE
			IF NOT IS_RECORDING_REQUIRED_FOR_MISSION_STAGE(i, eCurrentStage)
				CLEAR_RECORDING_REQUEST(i)
			ENDIF
		ENDIF
	ENDREPEAT

	IF HAVE_ALL_RECORDING_REQUESTS_SUCCEEDED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// is this waypoint required on this stage?
FUNC BOOL IS_WAYPOINT_REQUIRED_FOR_MISSION_STAGE(FRA2_WAYPOINTS thisWaypoint, enumMissionStage reqStage)
	SWITCH thisWaypoint
		CASE FW_BULLDOZER
			IF reqStage = STAGE_ATTACK_SAWMILL
				RETURN TRUE
			ENDIF
		BREAK
		CASE FW_TREVOR
			IF reqStage = STAGE_GET_INTO_POSITION
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// add a request for a waypoint
PROC ADD_WAYPOINT_REQUEST(FRA2_WAYPOINTS thisWaypoint)
	bWaypointRequestTracker[thisWaypoint] = TRUE
	TEXT_LABEL tWaypoint = GET_WAYPOINT_LABEL(thisWaypoint)
	REQUEST_WAYPOINT_RECORDING(tWaypoint)
ENDPROC

// clear a waypoint request slot
PROC CLEAR_WAYPOINT_REQUEST(FRA2_WAYPOINTS thisWaypoint)
	IF bWaypointRequestTracker[thisWaypoint]
		bWaypointRequestTracker[thisWaypoint] = FALSE
		TEXT_LABEL tWaypoint = GET_WAYPOINT_LABEL(thisWaypoint)
		IF NOT GET_IS_WAYPOINT_RECORDING_LOADED(tWaypoint)
			REMOVE_WAYPOINT_RECORDING(tWaypoint)
		ENDIF
	ENDIF
ENDPROC

// have all requests for waypoints succeeded?
FUNC BOOL HAVE_ALL_WAYPOINT_REQUESTS_SUCCEEDED()
	INT i 
	
	// loop through and check for empty
	REPEAT COUNT_OF(FRA2_WAYPOINTS) i 
		IF bWaypointRequestTracker[i]
			TEXT_LABEL tWaypoint = GET_WAYPOINT_LABEL(INT_TO_ENUM(FRA2_WAYPOINTS, i))
			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED(tWaypoint)
				// a full slot is not loaded, return false
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT	
	
	RETURN TRUE
ENDFUNC

// get recordings for particular stage
FUNC BOOL LOAD_WAYPOINT_RECORDINGS_FOR_STAGE(enumMissionStage requestStage)
	INT i 
	REPEAT COUNT_OF(FRA2_WAYPOINTS) i
		FRA2_WAYPOINTS thisWaypoint = INT_TO_ENUM(FRA2_WAYPOINTS, i)
		IF IS_WAYPOINT_REQUIRED_FOR_MISSION_STAGE(thisWaypoint, requestStage)
			// if we need this waypoint, request it
			ADD_WAYPOINT_REQUEST(thisWaypoint)
		ELSE	
			// otherwise try and get rid of it
			IF NOT IS_WAYPOINT_REQUIRED_FOR_MISSION_STAGE(thisWaypoint, eCurrentStage)
				CLEAR_WAYPOINT_REQUEST(thisWaypoint)
			ENDIF
		ENDIF
	ENDREPEAT

	IF HAVE_ALL_WAYPOINT_REQUESTS_SUCCEEDED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL LOAD_SPECIAL_ASSETS_FOR_STAGE(enumMissionStage requestStage)
	SWITCH requestStage
		CASE STAGE_ATTACK_SAWMILL
		CASE STAGE_GET_LAMAR_OUT
			REQUEST_ANIM_SET("move_injured_generic")
			IF HAS_ANIM_SET_LOADED("move_injured_generic")
				RETURN TRUE
			ENDIF
		BREAK
		DEFAULT
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL LOAD_MISSION_TEXT()
	REQUEST_ADDITIONAL_TEXT("Fran2", MISSION_TEXT_SLOT)
	
	RETURN HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
ENDFUNC

FUNC BOOL IS_WEAPON_REQUIRED_FOR_MISSION_STAGE(FRA2_WEAPON_ENUM weapon, enumMissionStage reqStage)
	SWITCH weapon
		CASE FW_WEAPON_PISTOL
		CASE FW_WEAPON_HEAVYSNIPER
		CASE FW_WEAPON_ASSAULT_SHOTGUN
		CASE FW_WEAPON_SMG
			IF reqStage >= STAGE_MEET_THE_CREW AND reqStage < STAGE_DRIVE_HOME
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// add a request for a weapon
PROC ADD_WEAPON_REQUEST(FRA2_WEAPON_ENUM weapon)
	bWeaponRequestTracker[weapon] = TRUE 
	WEAPON_TYPE weaponType = GET_FRA2_WEAPON_TYPE(weapon)
	REQUEST_WEAPON_ASSET(weaponType)
ENDPROC

// clear a weapon request slot
PROC CLEAR_WEAPON_REQUEST(FRA2_WEAPON_ENUM weapon)	
	IF bWeaponRequestTracker[weapon]
		bWeaponRequestTracker[weapon] = FALSE
		WEAPON_TYPE weaponType = GET_FRA2_WEAPON_TYPE(weapon)
		IF HAS_WEAPON_ASSET_LOADED(weaponType)
			REMOVE_WEAPON_ASSET(weaponType)
		ENDIF
	ENDIF
ENDPROC

// have all requests for weapons succeeded?
FUNC BOOL HAVE_ALL_WEAPON_REQUESTS_SUCCEEDED()
	INT i 
	
	// loop through and check for empty
	REPEAT COUNT_OF(FRA2_WEAPON_ENUM) i 
		IF bWeaponRequestTracker[i]
			IF NOT HAS_WEAPON_ASSET_LOADED(GET_FRA2_WEAPON_TYPE(INT_TO_ENUM(FRA2_WEAPON_ENUM, i)))
				// a full slot is not loaded, return false
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT	
	
	RETURN TRUE
ENDFUNC

// get weapons for particular stage
FUNC BOOL LOAD_WEAPONS_FOR_STAGE(enumMissionStage requestStage)

	INT i 
	REPEAT COUNT_OF(FRA2_WEAPON_ENUM) i 
		FRA2_WEAPON_ENUM thisWeapon = INT_TO_ENUM(FRA2_WEAPON_ENUM, i)
	
		IF IS_WEAPON_REQUIRED_FOR_MISSION_STAGE(thisWeapon, requestStage)
			// if we need this recording, request it
			ADD_WEAPON_REQUEST(thisWeapon)
		ELSE	
			// otherwise try and get rid of it
			IF NOT IS_WEAPON_REQUIRED_FOR_MISSION_STAGE(thisWeapon, eCurrentStage)
				CLEAR_WEAPON_REQUEST(thisWeapon)
			ENDIF
		ENDIF
	ENDREPEAT

	// if wait for load, don't quit out 'til all the recordings are in
	IF HAVE_ALL_WEAPON_REQUESTS_SUCCEEDED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PED_INDEX GET_BULLDOZER_DRIVER()
	IF DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
		IF IS_VEHICLE_DRIVEABLE(vehicle[veh_bulldozer].id)
			RETURN GET_PED_IN_VEHICLE_SEAT(vehicle[veh_bulldozer].id, VS_DRIVER)
		ENDIF
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC BOOL IS_BULLDOZER_DRIVER_SEAT_OPEN()
	IF DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
		IF IS_VEHICLE_DRIVEABLE(vehicle[veh_bulldozer].id)
			IF IS_VEHICLE_SEAT_FREE(vehicle[veh_bulldozer].id)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_ALL_PED_SELECTOR_HINTS_OFF()
	SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
	SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
	SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
ENDPROC

FUNC BOOL DELETE_EVERYTHING(enumEndState endState)
	CPRINTLN(DEBUG_MISSION, "DELETE_EVERYTHING start")
	
	IF IS_CUTSCENE_ACTIVE()	
		IF IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE(TRUE)		 
		ELSE
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
	
	CANCEL_MUSIC_EVENT("FRA2_END_VEHICLE")
	
	IF DOES_ENTITY_EXIST(objMichaelsWeapon)
		DELETE_OBJECT(objMichaelsWeapon)
	ENDIF
	
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iUnderSawmillBlocker)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iUnderSawmillBlocker)
	ENDIF
	
	IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(vTrevorExitCoverPosition)
		REMOVE_COVER_POINT(covTrevorExit)
	ENDIF
	
	REMOVE_RELATIONSHIP_GROUP(relgroupIgnore)
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	KILL_ANY_CONVERSATION()
	
	SET_ALL_PED_SELECTOR_HINTS_OFF()
		
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(player_ped_id(),FALSE)
	ENDIF
	
	STOP_AUDIO_SCENES()
	
	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
	REMOVE_NAVMESH_REQUIRED_REGIONS()
	
	INT i
	REPEAT COUNT_OF(missionPeds) i
		IF endState != END_STATE_PASSED	AND endState != END_STATE_FAILED
			IF missionPeds[i].id != PLAYER_PED_ID()
				IF DOES_ENTITY_EXIST(missionPeds[i].id)
					IF NOT IS_PED_INJURED(missionPeds[i].id)
						IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(missionPeds[i].id)
							DELETE_PED(missionPeds[i].id)	
						ENDIF
					ELSE
						DELETE_PED(missionPeds[i].id)
					ENDIF
				ENDIF
			ENDIF
		ELSE	
			IF NOT IS_PED_INJURED(missionPeds[i].id)								
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(missionPeds[i].id)
				AND missionPeds[i].id != PLAYER_PED_ID()
					IF IS_PED_IN_GROUP(missionPeds[i].id)
						REMOVE_PED_FROM_GROUP(missionPeds[i].id)	
					ENDIF
					SET_PED_KEEP_TASK(missionPeds[i].id,TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(missionPeds[i].id)
				ENDIF			
			ELSE
				IF DOES_ENTITY_EXIST(missionPeds[i].id)
				AND missionPeds[i].id != PLAYER_PED_ID()				
					SET_PED_AS_NO_LONGER_NEEDED(missionPeds[i].id)					
				ENDIF
			ENDIF	
		ENDIF		
	ENDREPEAT
	
	REPEAT COUNT_OF(enemyPeds) i
		IF endState != END_STATE_PASSED	
		AND endState != END_STATE_FAILED
			IF DOES_ENTITY_EXIST(enemyPeds[i].id)
				IF NOT IS_PED_INJURED(enemyPeds[i].id)
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(enemyPeds[i].id)
						DELETE_PED(enemyPeds[i].id)	
					ENDIF
				ELSE
					DELETE_PED(enemyPeds[i].id)
				ENDIF
			ENDIF
		ELSE	
			IF NOT IS_PED_INJURED(enemyPeds[i].id)								
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(enemyPeds[i].id)
					IF IS_PED_IN_GROUP(enemyPeds[i].id)
						REMOVE_PED_FROM_GROUP(enemyPeds[i].id)	
					ENDIF
					SET_PED_KEEP_TASK(enemyPeds[i].id,TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(enemyPeds[i].id)
				ENDIF			
			ELSE
				IF DOES_ENTITY_EXIST(enemyPeds[i].id)			
					SET_PED_AS_NO_LONGER_NEEDED(enemyPeds[i].id)					
				ENDIF
			ENDIF	
		ENDIF		
	ENDREPEAT
	
	REPEAT COUNT_OF(vehicle) i
		IF endState != END_STATE_PASSED	
		AND endState != END_STATE_FAILED
			IF DOES_ENTITY_EXIST(vehicle[i].id)
				IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[i].id)
					CLEAR_PED_TASKS_IMMEDIATELY(player_ped_id())
				ENDIF
				DELETE_VEHICLE(vehicle[i].id)	
			ENDIF
		ELSE			
			IF DOES_ENTITY_EXIST(vehicle[i].id)
				IF IS_ENTITY_A_MISSION_ENTITY(vehicle[i].id)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[i].id)
				ENDIF
			ENDIF	
		ENDIF		
	ENDREPEAT	
	
	REMOVE_SCENARIO_BLOCKING_AREAS()	

	
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(Locates_Data)
	CLEAR_MISSION_LOCATE_STUFF(locates_data,true)
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	CLEAR_HELP()
	CLEAR_PRINTS()
	
	
	DISPLAY_RADAR(TRUE)	
	DISPLAY_HUD(TRUE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
	
	IF DOES_BLIP_EXIST(mission_blip)
		REMOVE_BLIP(mission_blip)
	ENDIF
	
	
	IF endState != END_STATE_STAGE_SKIP
		SET_VEHICLE_CONVERSATIONS_PERSIST(FALSE, FALSE)
	ENDIF
	
	switchState = SWITCH_SETUP
	
	statTrackingFlag = 0
	
	#IF IS_DEBUG_BUILD
		DELETE_WIDGET_GROUP(widgAccuracyControls)
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC MISSION_CLEANUP(enumEndState endState)
	DELETE_EVERYTHING(endState)

	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()	
	g_bMagDemoFRA2Passed = TRUE // So the magdemo2 script knows if the mission should be re-launched or not.
	Mission_Flow_Mission_Passed()
	MISSION_CLEANUP(END_STATE_PASSED)
ENDPROC

PROC MISSION_FAILED(enumFails failReason = FAIL_NULL)
	string sFailReason
	SWITCH failReason
		CASE FAIL_NULL sFailReason = "" BREAK
		CASE FAIL_COVER_BLOWN sFailReason = "FRAN2_FEARLY" BREAK
		CASE FAIL_LAMAR_KILLED sFailReason = "FRAN2_F01" BREAK
		CASE FAIL_MICHAEL_KILLED sFailReason = "CMN_MDIED" BREAK
		CASE FAIL_TREVOR_KILLED sFailReason = "CMN_TDIED" BREAK
		CASE FAIL_FRANKLIN_KILLED sFailReason = "CMN_FDIED" BREAK
		CASE FAIL_KILL_TANISHA sFailReason = "FRAN2_F02" BREAK
		CASE FAIL_BULLDOZER_KILLED sFailReason = "FRAN2_F03" BREAK
		DEFAULT sFailReason = "" BREAK
	ENDSWITCH

	TRIGGER_MUSIC_EVENT("FRA2_FAIL")
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(sfailReason) 
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	MISSION_CLEANUP(END_STATE_FAILED) // must only take 1 frame and terminate the thread

ENDPROc

FUNC BOOL IS_ANY_SQUAD_MEMBER_INJURED(INT iSquad)
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF DOES_ENTITY_EXIST(enemyPeds[i].id)
				IF IS_PED_INJURED(enemyPeds[i].id)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC
PROC FAIL(enumFails thisFailToCheck)

	SWITCH thisFailToCheck
		CASE fail_null
		BREAK	
		CASE FAIL_COVER_BLOWN
			IF IS_ANY_SQUAD_MEMBER_INJURED(SQUAD_PreGuards)
			OR IS_PLAYER_IN_TRIGGER_BOX(tbEntireSawmill)
				MISSION_FAILED(thisFailToCheck)
			ENDIF
		BREAK
		CASE FAIL_LAMAR_KILLED
			IF DOES_ENTITY_EXIST(missionPeds[ped_lamar].id)
				IF IS_PED_INJURED(missionPeds[ped_lamar].id)
					MISSION_FAILED(thisFailToCheck)
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_TREVOR_KILLED
			IF DOES_ENTITY_EXIST(missionPeds[ped_trevor].id)
				IF IS_PED_INJURED(missionPeds[ped_trevor].id)
					MISSION_FAILED(thisFailToCheck)
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_MICHAEL_KILLED
			IF DOES_ENTITY_EXIST(missionPeds[ped_michael].id)
				IF IS_PED_INJURED(missionPeds[ped_michael].id)
					MISSION_FAILED(thisFailToCheck)
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_FRANKLIN_KILLED
			IF DOES_ENTITY_EXIST(missionPeds[ped_franklin].id)
				IF IS_PED_INJURED(missionPeds[ped_franklin].id)
					MISSION_FAILED(thisFailToCheck)
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_BULLDOZER_KILLED
			IF DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle[veh_bulldozer].id)
					MISSION_FAILED(thisFailToCheck)
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_KILL_TANISHA
			IF DOES_ENTITY_EXIST(ped_tanisha)
				IF IS_PED_INJURED(ped_tanisha)
					MISSION_FAILED(thisFailToCheck)
				ENDIF
			ENDIF
		BREAK

		
	ENDSWITCH
	
	
ENDPROC


PROC SET_UP_PLAYER(INT ID)
	
	IF NOT IS_PED_INJURED(missionPeds[ID].id)
		IF missionPeds[ID].id != PLAYER_PED_ID()
			SET_PED_SUFFERS_CRITICAL_HITS(missionPeds[ID].id, FALSE)
			SET_PED_CAN_RAGDOLL(missionPeds[ID].id, FALSE)
			SET_PED_ACCURACY(missionPeds[ID].id, iDefaultAllyAccuracy)
			SET_PED_COMBAT_ATTRIBUTES(missionPeds[ID].id, CA_LEAVE_VEHICLES, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(missionPeds[ID].id, TRUE)
		ENDIF
		
		//weapons
		SWITCH ID
			CASE ped_Franklin
				IF g_bMagDemoActive
					GIVE_WEAPON_TO_PED(missionPeds[ped_franklin].id, WEAPONTYPE_PISTOL, 100)
					GIVE_WEAPON_TO_PED(missionPeds[ped_franklin].id, WEAPONTYPE_ASSAULTSHOTGUN, 50)
					GIVE_WEAPON_TO_PED(missionPeds[ped_franklin].id, WEAPONTYPE_SMG, 200)
					GIVE_WEAPON_TO_PED(missionPeds[ped_franklin].id, WEAPONTYPE_CARBINERIFLE, 100)
				ELSE
					#if is_debug_build
						GIVE_WEAPON_TO_PED(missionPeds[ped_franklin].id, WEAPONTYPE_PISTOL, 200)
					#endif
				ENDIF
			BREAK
			CASE ped_michael
				GIVE_WEAPON_TO_PED(missionPeds[ped_michael].id, WEAPONTYPE_HEAVYSNIPER, 40)
				SET_CURRENT_PED_WEAPON(missionPeds[ped_michael].id, WEAPONTYPE_HEAVYSNIPER)
				SET_PED_SEEING_RANGE(missionPeds[ped_michael].id, 170)
				SET_PED_RELATIONSHIP_GROUP_HASH(missionPeds[ped_michael].id, relgroupIgnore)
				IF g_bMagDemoActive
					SET_PED_COMP_ITEM_CURRENT_SP(missionPeds[ped_michael].id, COMP_TYPE_OUTFIT, OUTFIT_P0_POLOSHIRT_PANTS, FALSE)
				ELSE
					SET_PED_COMP_ITEM_CURRENT_SP(missionPeds[ped_michael].id, COMP_TYPE_OUTFIT, OUTFIT_P0_LEATHER_AND_JEANS, FALSE)
				ENDIF
			BREAK
			CASE ped_trevor
				#IF is_debug_build
					GIVE_WEAPON_TO_PED(missionPeds[ped_trevor].id, GET_FRA2_WEAPON_TYPE(FW_WEAPON_ASSAULT_SHOTGUN), 200, TRUE)			
				#ENDIF
				
				IF g_bMagDemoActive
					SET_PED_COMP_ITEM_CURRENT_SP(missionPeds[ped_trevor].id, COMP_TYPE_OUTFIT, OUTFIT_P2_DENIM, FALSE)
				ELSE
					SET_PED_COMP_ITEM_CURRENT_SP(missionPeds[ped_trevor].id, COMP_TYPE_OUTFIT, OUTFIT_P2_TSHIRT_CARGOPANTS_3, FALSE)
				ENDIF
				
				// this sets enemies to equate trevor for targeting priority purposes
				// which means they won't switch to franklin too early when firing at trevor
				// while he's driving the bulldozer, which just looks weird becaus they are
				// also tethered around the bulldozer. this is turned off in the trevor handler
				// when he gets out of the bulldozer
				SET_PED_CONFIG_FLAG(missionPeds[ped_trevor].id, PCF_TreatAsPlayerDuringTargeting, TRUE)
			BREAK
		ENDSWITCH

	ENDIF
ENDPROC

structPedsForConversation MyLocalPedStruct

BOOL	bLastConvoWithoutSubtitles

STRUCT convStoreStruct
	int speakerNo
	ped_index pedIndex
	string speakerLabel
ENDSTRUCT
convStoreStruct convStore[4]


TEXT_LABEL_23 restartSubtitleLine,restartSubtitleRoot

ped_index pedInSlot[10]

func PED_INDEX ped_in_slot(int slot)
	return pedInSlot[slot]
ENDFUNC

func bool IS_CONV_ROOT_PLAYING(string conv_root_to_check)
	TEXT_LABEL_23 txt
	txt = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

	IF ARE_STRINGS_EQUAL(txt,conv_root_to_check)
		RETURN TRUE		
	ENDIF
	RETURN FALSE
endfunc

PROC ADD_PED_FOR_DIALOGUE_EXTRA(int speakerIndex, ped_index speaker, string speakerLabel)
	int iJ
	repeat count_of(pedInSlot) iJ
		if pedInSlot[iJ] = speaker
			//there should never be more than one situation for this
			REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,iJ)
			pedInSlot[iJ] = null
		endif
	endrepeat

	if pedInSlot[speakerIndex] != null
		REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,speakerIndex)
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,speakerIndex,speaker, speakerLabel)
	pedInSlot[speakerIndex] = speaker

ENDPROC

func bool PLAYER_CALL_CHAR_CELLPHONE_EXTRA(string Label, enumCharacterList charToCall, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null)
	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			if ped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
			ENDIF
			if speakerTwo != -1
				if ped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
				ENDIF
			ENDIF
			if speakerThree != -1
				if ped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
				ENDIF
			ENDIF

			IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct,charToCall,convBlock,Label,CONV_PRIORITY_VERY_HIGH)
				return TRUE
			ENDIF
		ENDIF
	ENDIF
	return FALSE
endfunc

FUNC BOOL CREATE_CONVERSATION_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, int speakerFour=-1, ped_index pedSpeakerFour=null, string speakerFourLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		convStore[0].speakerNo = speakerOne
		convStore[0].pedIndex = pedSpeakerOne
		convStore[0].speakerLabel = speakerOneLabel
		convStore[1].speakerNo = speakerTwo
		convStore[1].pedIndex = pedSpeakerTwo
		convStore[1].speakerLabel = speakerTwoLabel
		convStore[2].speakerNo = speakerThree
		convStore[2].pedIndex = pedSpeakerThree
		convStore[2].speakerLabel = speakerThreeLabel
		convStore[3].speakerNo = speakerFour
		convStore[3].pedIndex = pedSpeakerFour
		convStore[3].speakerLabel = speakerFourLabel
		
		if ped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		ENDIF
		if speakerTwo != -1
			if ped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
			ENDIF
		ENDIF
		if speakerThree != -1
			if ped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
			ENDIF
		ENDIF
		if speakerFour != -1
			if ped_in_slot(speakerFour) != pedSpeakerFour or pedSpeakerFour = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
			ENDIF
		ENDIF		

		IF IS_MESSAGE_BEING_DISPLAYED()
		ANd IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
			if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, Label, convPriority,DO_NOT_DISPLAY_SUBTITLES)
				bLastConvoWithoutSubtitles = TRUE
			
				return true
			ENDIF
		else
			if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, Label, convPriority)
				bLastConvoWithoutSubtitles = FALSE
	
				return true
			ENDIF
		endif
	//ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(string Label, string line, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null,int speakerFour=-1,ped_index pedSpeakerFour=null, string speakerFourLabel = null,enumConversationPriority convPriority = CONV_PRIORITY_HIGH)	
	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
		IF ped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		ENDIF
		IF speakerTwo != -1
			if ped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
			ENDIF
		ENDIF
		IF speakerThree != -1
			if ped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
			ENDIF
		ENDIF
		IF speakerFour != -1
			if ped_in_slot(speakerFour) != pedSpeakerFour or pedSpeakerFour = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
			ENDIF
		ENDIF
		IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct,convBlock,Label,line,convPriority)		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	IF ped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
	ENDIF
	IF speakerTwo != -1
		if ped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
	ENDIF
	IF speakerThree != -1
		if ped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
	ENDIF

	START_TIMER_NOW_SAFE(tmrIncidentalDialogueThrottle)
	
	ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(MyLocalPedStruct, convBlock, Label, convPriority)
ENDPROC

PROC SET_ENEMIES_ACCURACY(INT iAccuracy)
	INT enemyIter
	REPEAT MAX_SAWMILL_ENEMIES enemyIter
		IF IS_ENTITY_OK(enemyPeds[enemyIter].id)
			SET_PED_ACCURACY(enemyPeds[enemyIter].id, iAccuracy)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL do_switch(ped_index &pedFranklin, ped_index &pedTrevor, ped_index &pedMichael)
	SWITCH switchState
		CASE SWITCH_SETUP
			sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = pedFranklin
			sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = pedTrevor
			sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = pedMichael
			
			PRINTSTRING("switchState=SWITCH_SELECTING")PRINTNL()
			switchState=SWITCH_SELECTING
		BREAK
		CASE SWITCH_SELECTING
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF UPDATE_SELECTOR_HUD(sSelectorPeds, IS_PLAYER_CONTROL_ON(PLAYER_ID()))     // Returns TRUE when the player has made a selection
					IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_FRANKLIN)
					OR HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MICHAEL)
					OR HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_TREVOR)
						sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
						sCamDetails.bRun  = TRUE
						
						PRINTSTRING("switchState = SWITCH_SWAP_PLAYER_PEDS")PRINTNL()
						switchState = SWITCH_SWAP_PLAYER_PEDS
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SWITCH_SWAP_PLAYER_PEDS
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails, SWITCH_TYPE_SHORT)
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
							PRINTSTRING("switchState = SWITCH_COMPLETE")PRINTNL()
							switchState = SWITCH_COMPLETE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SWITCH_COMPLETE
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails)
				//switch still in progress
				
				// this will keep getting restarted, thats ok(?)
				IF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_MICHAEL
					// force aim to get the sniper HUD up
					SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
					CPRINTLN(DEBUG_MISSION, "BSW starting michael aim timer")
				ELSE
					SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
				ENDIF
			ELSE
				// update audio scenes
				STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
				STOP_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
				IF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_TREVOR
					IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
						START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_TREVOR")
					ENDIF
				ELIF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_MICHAEL
					IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
						START_AUDIO_SCENE("FRANKLIN_2_SAVE_LAMAR_MICHAEL")
					ENDIF
				ENDIF
				
				// toggle enemy (and Franklin) accuracy based on which player you are
				// ALSO make sure Franklin is properly set to invuln or not
				IF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_FRANKLIN
					iCurrentEnemyAccuracy = iDefaultEnemyAccuracy
					SET_ENEMIES_ACCURACY(iCurrentEnemyAccuracy)
					IF IS_ENTITY_OK(PLAYER_PED_ID())
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
					ENDIF
				ELSE
					// always set AI Frank flame and explosion proof
					IF IS_ENTITY_OK(missionPeds[ped_franklin].id)
						SET_ENTITY_PROOFS(missionPeds[ped_franklin].id, FALSE, TRUE, TRUE, FALSE, FALSE)
					ENDIF
					
					// set all our enemies to low accuracy so they dont kill people
					iCurrentEnemyAccuracy = iLowEnemyAccuracy
					SET_ENEMIES_ACCURACY(iCurrentEnemyAccuracy)
					
					// if we're michael, set franklin to low accuracy as well
					IF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_MICHAEL
						IF IS_ENTITY_OK(missionPeds[ped_franklin].id)
							SET_PED_ACCURACY(missionPeds[ped_franklin].id, iLowAllyAccuracy)
						ENDIF
					// if trevor
					ELSE
						// if we're trevor, set franklin to regular accuracy because trevor can't easily kill people
						IF IS_ENTITY_OK(missionPeds[ped_franklin].id)
							SET_PED_ACCURACY(missionPeds[ped_franklin].id, iDefaultAllyAccuracy)
						ENDIF
						
						// kill this timer so that if franklin should be invuln
						// it doesnt stomp that and make him vulnerable again
						CANCEL_TIMER(tmrMichaelKillTimer)
						
						// finally make Franklin invuln
						IF IS_ENTITY_OK(missionPeds[ped_franklin].id)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(missionPeds[ped_franklin].id, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				// update objectives
				IF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_MICHAEL
					eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_PROTECT_F_AND_T
					
					FLOAT fNewPitch
					IF IS_ENTITY_OK(missionPeds[ped_michael].id) AND IS_ENTITY_OK(vehicle[veh_bulldozer].id)
						fNewPitch = GET_PITCH_FROM_COORDS(GET_ENTITY_COORDS(missionPeds[ped_michael].id), GET_ENTITY_COORDS(vehicle[veh_bulldozer].id))
					ENDIF

					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fNewPitch)
				ELSE
					IF eAttackSawmillCurrentGoal < MISSION_GOAL_13
						IF GET_BULLDOZER_DRIVER() = PLAYER_PED_ID()
							IF eBulldozerState < BULLDOZER_STAGE3
								eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_PUSH_CARS
							ELSE
								eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_BULLDOZER_END
							ENDIF
							
							START_TIMER_NOW_SAFE(tmrBulldozerForceDrive)
							CPRINTLN(DEBUG_MISSION, "BSW starting bulldozer drive timer")
						ELSE
							eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_ESCORT_BULLDOZER
						ENDIF
					ELIF eAttackSawmillCurrentGoal = MISSION_GOAL_13
						eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_FIND_LAMAR
					ENDIF
				ENDIF
				
				pedFranklin = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
				pedTrevor = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
				pedMichael = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
				
			
				IF GET_ENTITY_MODEL(player_ped_id()) = GET_PLAYER_PED_MODEL(CHAR_FRANKLIN) pedFranklin = PLAYER_PED_ID() ENDIF
				IF GET_ENTITY_MODEL(player_ped_id()) = GET_PLAYER_PED_MODEL(CHAR_TREVOR) pedTrevor = PLAYER_PED_ID() ENDIF
				IF GET_ENTITY_MODEL(player_ped_id()) = GET_PLAYER_PED_MODEL(CHAR_MICHAEL) pedMichael = PLAYER_PED_ID() ENDIF
				
				cprintln(debug_Trevor3,"player id = ",native_to_int(player_ped_id()))
				cprintln(debug_Trevor3,"pedFranklin = ",native_to_int(pedFranklin))
				cprintln(debug_Trevor3,"pedTrevor = ",native_to_int(pedTrevor))
				cprintln(debug_Trevor3,"pedMichael = ",native_to_int(pedMichael))
				
				PRINTSTRING("switchState = SWITCH_SETUP")PRINTNL()
				switchState = SWITCH_SETUP
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC


PROC UPDATE_PLAYER_SWITCH()	
	IF do_switch(missionPeds[ped_franklin].id, missionPeds[ped_trevor].id, missionPeds[ped_michael].id)	
		
		IF eBulldozerState = BULLDOZER_FINISHED
			IF NOT DOES_BLIP_EXIST(missionPeds[ped_trevor].aiBlip.BlipID)
				missionPeds[ped_trevor].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_trevor].id)
			ELSE
				IF PLAYER_PED_ID() = missionPeds[ped_trevor].id
					REMOVE_BLIP(missionPeds[ped_trevor].aiBlip.BlipID)
				ENDIF
			ENDIF
			IF NOT DOES_BLIP_EXIST(missionPeds[ped_franklin].aiBlip.BlipID)
				missionPeds[ped_franklin].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_franklin].id)
			ELSE
				IF PLAYER_PED_ID() = missionPeds[ped_franklin].id
					REMOVE_BLIP(missionPeds[ped_franklin].aiBlip.BlipID)
				ENDIF
			ENDIF
			IF NOT DOES_BLIP_EXIST(missionPeds[ped_michael].aiBlip.BlipID)
				missionPeds[ped_michael].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_michael].id)
			ELSE
				IF PLAYER_PED_ID() = missionPeds[ped_michael].id
					REMOVE_BLIP(missionPeds[ped_michael].aiBlip.BlipID)
				ENDIF
			ENDIF
		ENDIF
		
		IF PLAYER_PED_ID() = missionPeds[ped_michael].id
			IF DOES_BLIP_EXIST(mission_blip)
				REMOVE_BLIP(mission_blip)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_AREA_AROUND_SAWMILL()
	CPRINTLN(DEBUG_MISSION, "SETUP_AREA_AROUND_SAWMILL start...")
	CLEAR_AREA(<<-537.743896,5295.065430,87.356049>>, 150.0, FALSE)
	CLEAR_AREA_OF_VEHICLES(<<-537.743896,5295.065430,87.356049>>, 150.0)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	SET_MAX_WANTED_LEVEL(0)
	SET_CREATE_RANDOM_COPS(FALSE)		
	SET_ROADS_IN_ANGLED_AREA(<<-662.546204,5223.201660,28.358589>>, <<-528.598022,5441.020508,116.627426>>, 206.750000, FALSE, FALSE)
	ADD_SCENARIO_BLOCKING_AREA(<<-537.743896,5295.065430,87.356049>>-<<250, 250, 50>>, <<-537.743896,5295.065430,87.356049>>+<<250, 250, 50>>)
	ADD_NAVMESH_REQUIRED_REGION(-542.984802,5344.354980, 110)
	
	iUnderSawmillBlocker =  ADD_NAVMESH_BLOCKING_OBJECT((<<-559.243, 5306.247, 70.517>>), (<<25, 50, 4>>), DEG_TO_RAD(-25.920))
	
	covTrevorExit = ADD_COVER_POINT(vTrevorExitCoverPosition, 288, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
ENDPROC



PROC SET_UP_SPAWN_DATA()
	
	INT i = 0
	
	enemySpawnPoints[i].vCoords = <<-568.2123, 5365.3032, 69.2145>>
	enemySpawnPoints[i].fHeading = 68
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-564.5731, 5348.4878, 69.2195>>
	enemySpawnPoints[i].fHeading = 135
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-595.5546, 5286.5776, 69.2215>>
	enemySpawnPoints[i].fHeading = 160
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-593.7036, 5283.6763, 69.2283>>
	enemySpawnPoints[i].fHeading = 133
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-578.4775, 5259.5327, 69.4571>>
	enemySpawnPoints[i].fHeading = 177
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-575.3481, 5257.3994, 69.4630>>
	enemySpawnPoints[i].fHeading = 157
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-514.4727, 5257.8027, 79.5973>>
	enemySpawnPoints[i].fHeading = 28
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-513.2372, 5258.2646, 79.6101>>
	enemySpawnPoints[i].fHeading = 309
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-496.8114, 5329.0796, 79.2569>>
	enemySpawnPoints[i].fHeading = 314
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-469.1050, 5290.9258, 84.5428>>
	enemySpawnPoints[i].fHeading = 193
	enemySpawnPoints[i].iSquad = SQUAD_PreGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-563.0150, 5350.5938, 69.2153>>
	enemySpawnPoints[i].fHeading = 183
	enemySpawnPoints[i].iSquad = SQUAD_Wave1
	iCellphoneEnemy = i
	i++
	
	enemySpawnPoints[i].vCoords = <<-576.9453, 5350.7593, 69.2145>>
	enemySpawnPoints[i].fHeading = 5
	enemySpawnPoints[i].iSquad = SQUAD_Wave1
	i++
	
	enemySpawnPoints[i].vCoords = <<-578.7988, 5344.6387, 69.2271>>
	enemySpawnPoints[i].fHeading = 278
	enemySpawnPoints[i].iSquad = SQUAD_Wave1
	i++
	
	enemySpawnPoints[i].vCoords = <<-585.5563, 5337.6045, 69.2150>>
	enemySpawnPoints[i].fHeading = 0
	enemySpawnPoints[i].iSquad = SQUAD_Wave2
	iDiaBulldozerTargeter = i
	i++
	
	enemySpawnPoints[i].vCoords = <<-585.2084, 5337.5332, 69.2144>>
	enemySpawnPoints[i].fHeading = 298
	enemySpawnPoints[i].iSquad = SQUAD_Wave2
	i++
	
	enemySpawnPoints[i].vCoords = <<-594.7781, 5314.9937, 69.2150>>
	enemySpawnPoints[i].fHeading = 347
	enemySpawnPoints[i].iSquad = SQUAD_Wave2
	i++
	
	enemySpawnPoints[i].vCoords =  <<-565.1418, 5326.7324, 72.5980>>
	enemySpawnPoints[i].fHeading = 269
	enemySpawnPoints[i].iSquad = SQUAD_Wave2
	i++
	
	enemySpawnPoints[i].vCoords = <<-599.3968, 5326.1245, 69.4395>>
	enemySpawnPoints[i].fHeading = 41
	enemySpawnPoints[i].iSquad = SQUAD_Wave2
	i++
	
	enemySpawnPoints[i].vCoords = <<-583.7757, 5282.8755, 69.2604>>
	enemySpawnPoints[i].fHeading = 344
	enemySpawnPoints[i].iSquad = SQUAD_Wave3
	i++
	
	enemySpawnPoints[i].vCoords = <<-582.9308, 5282.7554, 69.2604>>
	enemySpawnPoints[i].fHeading = 338
	enemySpawnPoints[i].iSquad = SQUAD_Wave3
	i++
	
	enemySpawnPoints[i].vCoords = <<-579.7418, 5275.2979, 69.2670>>
	enemySpawnPoints[i].fHeading = 327
	enemySpawnPoints[i].iSquad = SQUAD_Wave3
	i++
	
	enemySpawnPoints[i].vCoords = <<-570.3315, 5269.4961, 69.2653>>
	enemySpawnPoints[i].fHeading = 327
	enemySpawnPoints[i].iSquad = SQUAD_Wave3
	i++
	
	enemySpawnPoints[i].vCoords = <<-573.8957, 5271.5938, 69.2663>>
	enemySpawnPoints[i].fHeading = 327
	enemySpawnPoints[i].iSquad = SQUAD_Wave3
	i++
	
	enemySpawnPoints[i].vCoords = <<-581.6175, 5260.2944, 69.4516>>
	enemySpawnPoints[i].fHeading = 24
	enemySpawnPoints[i].iSquad = SQUAD_Wave4
	i++
	
	enemySpawnPoints[i].vCoords = <<-566.4334, 5245.6865, 69.4657>>
	enemySpawnPoints[i].fHeading = 40
	enemySpawnPoints[i].iSquad = SQUAD_Wave4
	i++
	
	enemySpawnPoints[i].vCoords = <<-561.0808, 5280.9692, 72.0562>>
	enemySpawnPoints[i].fHeading = 327
	enemySpawnPoints[i].iSquad = SQUAD_Wave4
	i++
	
	enemySpawnPoints[i].vCoords = <<-562.1721, 5255.2261, 69.4955>>
	enemySpawnPoints[i].fHeading = 119
	enemySpawnPoints[i].iSquad = SQUAD_Wave4
	i++
	
	enemySpawnPoints[i].vCoords = <<-542.6985, 5279.6460, 73.1741>>
	enemySpawnPoints[i].fHeading = 179
	enemySpawnPoints[i].iSquad = SQUAD_Wave5
	i++
	
	enemySpawnPoints[i].vCoords = <<-519.4446, 5294.2515, 73.2926>>
	enemySpawnPoints[i].fHeading = 206
	enemySpawnPoints[i].iSquad = SQUAD_Wave5
	i++
	
	enemySpawnPoints[i].vCoords = <<-538.1600, 5288.1733, 74.3689>>
	enemySpawnPoints[i].fHeading = 186
	enemySpawnPoints[i].iSquad = SQUAD_Wave5
	i++

	enemySpawnPoints[i].vCoords = <<-587.8854, 5279.5742, 69.2659>>
	enemySpawnPoints[i].fHeading = 15
	enemySpawnPoints[i].iSquad = SQUAD_Barricade1
	enemySpawnPoints[i].iWeaponType =  WEAPONTYPE_CARBINERIFLE
	i++
	
	enemySpawnPoints[i].vCoords = <<-586.0494, 5280.1616, 69.2663>>
	enemySpawnPoints[i].fHeading = 330
	enemySpawnPoints[i].iSquad = SQUAD_Barricade1
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_SMG
	i++
	
	enemySpawnPoints[i].vCoords = <<-591.7385, 5280.0029, 69.2191>>
	enemySpawnPoints[i].fHeading = 62
	enemySpawnPoints[i].iSquad = SQUAD_Barricade1
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_CARBINERIFLE
	i++
	
	enemySpawnPoints[i].vCoords = <<-593.3607, 5279.5625, 69.2358>>
	enemySpawnPoints[i].fHeading = 39
	enemySpawnPoints[i].iSquad = SQUAD_Barricade1
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_SMG
	i++
	
	enemySpawnPoints[i].vCoords = <<-544.8799, 5256.7974, 73.1512>>
	enemySpawnPoints[i].fHeading = 65
	enemySpawnPoints[i].iSquad = SQUAD_Barricade2
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_CARBINERIFLE
	i++
	
	enemySpawnPoints[i].vCoords = <<-545.1004, 5255.3447, 73.1806>>
	enemySpawnPoints[i].fHeading = 337
	enemySpawnPoints[i].iSquad = SQUAD_Barricade2
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_CARBINERIFLE
	i++
	
	enemySpawnPoints[i].vCoords = <<-546.0182, 5253.8887, 73.0702>>
	enemySpawnPoints[i].fHeading = 30
	enemySpawnPoints[i].iSquad = SQUAD_Barricade2
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_SMG
	i++
	
	enemySpawnPoints[i].vCoords = <<-489.5215, 5302.5742, 79.6101>>
	enemySpawnPoints[i].fHeading = 167
	enemySpawnPoints[i].iSquad = SQUAD_Wave6
	i++
	
	enemySpawnPoints[i].vCoords = <<-491.7450, 5302.5181, 79.6101>>
	enemySpawnPoints[i].fHeading = 73
	enemySpawnPoints[i].iSquad = SQUAD_Wave6
	i++
	
	enemySpawnPoints[i].vCoords = <<-488.2481, 5280.5132, 79.6101>>
	enemySpawnPoints[i].fHeading = 80
	enemySpawnPoints[i].iSquad = SQUAD_Wave6
	i++
	
	enemySpawnPoints[i].vCoords = <<-501.0388, 5292.8853, 79.6070>>
	enemySpawnPoints[i].fHeading = 151
	enemySpawnPoints[i].iSquad = SQUAD_Wave6
	i++
	
	enemySpawnPoints[i].vCoords = <<-842.8779, 5313.4150, 76.5895>>
	enemySpawnPoints[i].fHeading = 235
	enemySpawnPoints[i].iSquad = SQUAD_CarArrive1
	iCar1Driver = i	// i know this is sorta weird
	i++
	
	enemySpawnPoints[i].vCoords = <<-840.7180, 5312.5503, 76.4866>>
	enemySpawnPoints[i].fHeading = 235
	enemySpawnPoints[i].iSquad = SQUAD_CarArrive1
	iCar1Passenger = i
	i++
	
	enemySpawnPoints[i].vCoords = <<-839.2146, 5313.2173, 76.4735>>
	enemySpawnPoints[i].fHeading = 235
	enemySpawnPoints[i].iSquad = SQUAD_CarArrive2
	iCar2Driver = i
	i++
	
	enemySpawnPoints[i].vCoords = <<-837.7225, 5313.5483, 76.4258>>
	enemySpawnPoints[i].fHeading = 235
	enemySpawnPoints[i].iSquad = SQUAD_CarArrive2
	iCar2Passenger = i
	i++
	
	enemySpawnPoints[i].vCoords = <<-546.2625, 5286.2749, 86.9530>>
	enemySpawnPoints[i].fHeading = 87
	enemySpawnPoints[i].iSquad = SQUAD_RPGSquad
	enemySpawnPoints[i].iWeaponType =  WEAPONTYPE_STINGER
	iRPGEnemy = i
	i++
	
	enemySpawnPoints[i].vCoords = <<-520.8571, 5306.1846, 79.2679>>
	enemySpawnPoints[i].fHeading = 94
	enemySpawnPoints[i].iSquad = SQUAD_LamarGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-510.4001, 5304.0142, 79.2679>>
	enemySpawnPoints[i].fHeading = 258
	enemySpawnPoints[i].iSquad = SQUAD_LamarGuards
	i++
	
	enemySpawnPoints[i].vCoords = <<-523.0146, 5306.2637, 79.2683>>
	enemySpawnPoints[i].fHeading = 8
	enemySpawnPoints[i].iSquad = SQUAD_LamarGuards
	iLamarAimGuard = i
	i++
	
	enemySpawnPoints[i].vCoords = <<-473.2388, 5286.7881, 84.5588>>
	enemySpawnPoints[i].fHeading = 167
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers1
	i++
	
	enemySpawnPoints[i].vCoords = <<-476.3661, 5279.9761, 84.7585>>
	enemySpawnPoints[i].fHeading = 153
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers1
	i++
	
	enemySpawnPoints[i].vCoords = <<-480.1133, 5269.6333, 85.0910>>
	enemySpawnPoints[i].fHeading = 108
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers1
	i++
	
	enemySpawnPoints[i].vCoords = <<-491.2911, 5247.2354, 85.2618>>
	enemySpawnPoints[i].fHeading = 126
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers1
	i++
	
	enemySpawnPoints[i].vCoords = <<-492.5721, 5245.7529, 84.9450>>
	enemySpawnPoints[i].fHeading = 150
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers1
	i++
	
	enemySpawnPoints[i].vCoords = <<-493.2359, 5244.0269, 85.1857>>
	enemySpawnPoints[i].fHeading = 151
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers1
	i++
	
	enemySpawnPoints[i].vCoords = <<-479.1326, 5236.2026, 86.3041>>
	enemySpawnPoints[i].fHeading = 158
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers1
	i++
	
	enemySpawnPoints[i].vCoords = <<-569.6173, 5250.5767, 69.4678>>
	enemySpawnPoints[i].fHeading = 332
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers2
	i++
	
	enemySpawnPoints[i].vCoords = <<-570.7142, 5248.9429, 69.4685>>
	enemySpawnPoints[i].fHeading = 313
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers2
	i++
	
	enemySpawnPoints[i].vCoords = <<-558.5370, 5266.5098, 71.3960>>
	enemySpawnPoints[i].fHeading = 41
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers2
	i++
	
	enemySpawnPoints[i].vCoords = <<-555.8079, 5265.3735, 71.9044>>
	enemySpawnPoints[i].fHeading = 307
	enemySpawnPoints[i].iSquad = SQUAD_LamarChasers2
	i++

	enemySpawnPoints[i].vCoords = <<-496.2458, 5235.3403, 86.8624>>
	enemySpawnPoints[i].fHeading = 63
	enemySpawnPoints[i].iSquad = SQUAD_SnipeTargets
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_CARBINERIFLE
	i++
	
	enemySpawnPoints[i].vCoords = <<-494.3994, 5252.8330, 85.8345>>
	enemySpawnPoints[i].fHeading = 98
	enemySpawnPoints[i].iSquad = SQUAD_SnipeTargets
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_CARBINERIFLE
	i++
	
	enemySpawnPoints[i].vCoords = <<-525.7026, 5274.6855, 78.4065>>
	enemySpawnPoints[i].fHeading = 186
	enemySpawnPoints[i].iSquad = SQUAD_SnipeTargets
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_CARBINERIFLE
	i++
	
	enemySpawnPoints[i].vCoords = <<-529.3057, 5275.7119, 78.0837>>
	enemySpawnPoints[i].fHeading = 138
	enemySpawnPoints[i].iSquad = SQUAD_SnipeTargets
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_CARBINERIFLE
	i++
	
	enemySpawnPoints[i].vCoords = <<-524.5403, 5304.7021, 81.3393>>
	enemySpawnPoints[i].fHeading = 156
	enemySpawnPoints[i].iSquad = SQUAD_SnipeTargets
	enemySpawnPoints[i].iWeaponType = WEAPONTYPE_CARBINERIFLE
	i++
	
//	enemySpawnPoints[i].vCoords = 
//	enemySpawnPoints[i].fHeading = 
//	enemySpawnPoints[i].iSquad = 
//	i++

	CPRINTLN(DEBUG_MISSION, "SET_UP_SPAWN_DATA set up ", i, " enemies")

ENDPROC

PROC INITIALISE_ATTACK_SAWMILL_DATA()
	SET_UP_SPAWN_DATA()
	
	iCurrentBarricadeTarget = -1
	
	iCurrentEnemyAccuracy = iDefaultEnemyAccuracy
	
	bCarArrivalsSpawned = FALSE
	bWaitForTrevor = FALSE
	
	tbTriggerWave1 = CREATE_TRIGGER_BOX(<<-542.600525,5373.625000,72.553314>>, <<-568.033813,5385.608887,64.270645>>, 8)
	tbTriggerWave2 = CREATE_TRIGGER_BOX(<<-545.064880,5335.187012,74.261208>>, <<-619.028809,5355.734375,64.037292>>, 5)
	tbTriggerWave3 = CREATE_TRIGGER_BOX(<<-548.951904,5324.590820,73.584953>>, <<-622.946411,5332.164551,65.593849>>, 8)
	tbTriggerWave4 = CREATE_TRIGGER_BOX(<<-564.614258,5295.276855,72.260368>>, <<-613.205505,5290.259277,63.230331>>, 5)
	tbTriggerWave5 = CREATE_TRIGGER_BOX(<<-553.980469,5289.254395,68.467430>>, <<-562.654541,5224.930176,77.657822>>, 5)
	tbTriggerWave6 = CREATE_TRIGGER_BOX(<<-541.968567,5287.351563,71.189102>>, <<-469.863403,5211.107422,95.462677>> , 15)
	
	tbMichaelAppears = CREATE_TRIGGER_BOX(<<-555.737305,5302.450684,74.543816>>, <<-641.466736,5300.998047,58.467258>>, 5)
	
	tbBulldozerInRPGRange = CREATE_TRIGGER_BOX(<<-583.818115,5262.382813,67.741280>>, <<-566.095337,5276.970703,73.236435>>, 18)

	tbFirstCombatZone = CREATE_TRIGGER_BOX(<<-559.057617,5388.774414,67.543236>>, <<-595.060547,5279.380371,74.380745>>, 60)
	tbSecondCombatZone = CREATE_TRIGGER_BOX(<<-586.345215,5282.420410,69.259094>>, <<-496.270050,5241.339844,87.589897>>, 30)
	
	tbBulldozerEnd = CREATE_TRIGGER_BOX(<<-512.429565,5258.324707,79.609993>>, <<-515.066040,5217.234375,83.786385>>, 8)
	
	tbSawmillBackSafety = CREATE_TRIGGER_BOX(<<-510.725555,5311.182617,89.268044>>, <<-448.558197,5422.180664,66.293083>>, 40)
	
	tbLamarGuardsClear = CREATE_TRIGGER_BOX(<<-491.384583,5295.602539,78.610100>>, <<-530.742310,5310.027832,82.268303>>, 14.5)
	tbLamarGuardsEntrance = CREATE_TRIGGER_BOX(<<-505.955688,5310.247070,78.268303>>, <<-490.709625,5298.566895,82.610100>>, 12.25)
	
	tbEntireSawmill = CREATE_TRIGGER_BOX(<<-510.308563,5402.376465,66.948547>>, <<-551.114868,5205.978516,92.807167>>, 183)

ENDPROC

PROC SET_UP_ENEMY_PED(StructPed enemyPed, COMBAT_MOVEMENT thisMovement = CM_WILLADVANCE, FLOAT fSightRange = 60.0)
	IF NOT IS_ENTITY_OK(enemyPed.id)
		EXIT
	ENDIF

	SET_PED_DIES_WHEN_INJURED(enemyPed.id, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(enemyPed.id, RELGROUPHASH_HATES_PLAYER)
	
	SET_PED_ACCURACY(enemyPed.id, iCurrentEnemyAccuracy)

	SET_PED_DIES_WHEN_INJURED(enemyPed.id, TRUE)
	SET_PED_SEEING_RANGE(enemyPed.id, fSightRange)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemyPed.id, FALSE)
	SET_PED_COMBAT_MOVEMENT(enemyPed.id, thisMovement)
	SET_PED_COMBAT_ATTRIBUTES(enemyPed.id, CA_USE_ENEMY_ACCURACY_SCALING, TRUE)
ENDPROC
	
PROC SET_UP_ENEMY_PEDS(INT iSquad, COMBAT_MOVEMENT thisMovement = CM_WILLADVANCE, FLOAT fSightRange = 60.0)
	INT enemyIter
	REPEAT MAX_SAWMILL_ENEMIES enemyIter
		IF enemyPeds[enemyIter].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[enemyIter].id)
				SET_UP_ENEMY_PED(enemyPeds[enemyIter], thisMovement, fSightRange)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_UP_SCRIPTED_SQUAD(INT iSquad)
	INT enemyIter
	REPEAT MAX_SAWMILL_ENEMIES enemyIter
		IF enemyPeds[enemyIter].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[enemyIter].id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemyPeds[enemyIter].id, TRUE)
				SET_PED_COMBAT_MOVEMENT(enemyPeds[enemyIter].id, CM_STATIONARY)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC FLOAT GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE(VEHICLE_INDEX VehicleIndex #IF IS_DEBUG_BUILD, BOOL bPrintRecordingTime = FALSE #ENDIF)

	IF DOES_ENTITY_EXIST(VehicleIndex)
	AND IS_VEHICLE_DRIVEABLE(VehicleIndex)
		    
	    IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(VehicleIndex)
		
          	RECORDING_ID RecordingID = GET_CURRENT_PLAYBACK_FOR_VEHICLE(VehicleIndex)             
                
            #IF IS_DEBUG_BUILD
                  INT iPercentage = ROUND( 100.0 * ( GET_TIME_POSITION_IN_RECORDING(VehicleIndex) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(RecordingID) ) )
				  TEXT_LABEL_63 debugName = GET_VEHICLE_RECORDING_NAME(RecordingID)
				  debugName += ":"
                  debugName += iPercentage
                  SET_VEHICLE_NAME_DEBUG(VehicleIndex, debugName)
				  //PRINTLN(GET_THIS_SCRIPT_NAME(), ": Recording ", GET_VEHICLE_RECORDING_NAME(RecordingID), " playback percentage ", 100.0 * ( GET_TIME_POSITION_IN_RECORDING(VehicleIndex) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(RecordingID) ) )
            #ENDIF
            
			#IF IS_DEBUG_BUILD
				IF ( bPrintRecordingTime = TRUE )
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Recording ", GET_VEHICLE_RECORDING_NAME(RecordingID), " time is ", GET_TIME_POSITION_IN_RECORDING(VehicleIndex), ".")
				ENDIF
			#ENDIF
			
            RETURN ( 100.0 * ( GET_TIME_POSITION_IN_RECORDING(VehicleIndex) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(RecordingID) ) )

	    ELSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS: Playback not going on for vehicle.")
			#ENDIF
			
	    ENDIF 
		
	ELSE
	
		#IF IS_DEBUG_BUILD
	    	PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS: Vehicle does not exist or is not driveable.")
		#ENDIF
		
	ENDIF

	RETURN -1.0
      
ENDFUNC

PROC START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE()
	IF NOT IS_ENTITY_OK(GET_BULLDOZER_DRIVER())
		EXIT
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehicle[veh_bulldozer].id)
		IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_bulldozer].id)
			IF GET_IS_WAYPOINT_RECORDING_LOADED(GET_WAYPOINT_LABEL(FW_BULLDOZER))
				TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(GET_BULLDOZER_DRIVER(), vehicle[veh_bulldozer].id, GET_WAYPOINT_LABEL(FW_BULLDOZER), DRIVINGMODE_PLOUGHTHROUGH | DF_StopForPeds, 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
				CPRINTLN(DEBUG_MISSION, "START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE starting playing")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION, "START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE PLAYBACK ALREADY GOING ON??")
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_CLOSEST_ENEMY_TO_BULLDOZER(PED_INDEX &outPed)
	FLOAT fDistance = 999
	
	INT iter
	REPEAT MAX_SAWMILL_ENEMIES iter
		IF IS_ENTITY_OK(enemyPeds[iter].id) AND IS_ENTITY_OK(vehicle[veh_bulldozer].id)
			IF iter <> iRPGEnemy AND iter <> iLamarAimGuard
				FLOAT fTempDistance = GET_DISTANCE_BETWEEN_ENTITIES(enemyPeds[iter].id, vehicle[veh_bulldozer].id)
				IF fTempDistance < fDistance
					outPed = enemyPeds[iter].id
					fDistance = fTempDistance
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN fDistance
ENDFUNC

FUNC INT GET_NUM_LIVING_ENEMIES()
	INT iCount
	
	INT iter
	REPEAT MAX_SAWMILL_ENEMIES iter
		IF IS_ENTITY_OK(enemyPeds[iter].id)
			iCount++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

FUNC PED_INDEX GET_FIRST_LIVING_ENEMY()	
	INT iter
	REPEAT MAX_SAWMILL_ENEMIES iter
		IF IS_ENTITY_OK(enemyPeds[iter].id)
			RETURN enemyPeds[iter].id
		ENDIF
	ENDREPEAT
	
	RETURN NULL
ENDFUNC

FUNC INT GET_NUM_ENEMIES_IN_TRIGGER_BOX(TRIGGER_BOX thisTrigger)
	INT iCount
	
	INT iter
	REPEAT MAX_SAWMILL_ENEMIES iter
		IF IS_ENTITY_OK(enemyPeds[iter].id)
			IF IS_ENTITY_IN_TRIGGER_BOX(thisTrigger, enemyPeds[iter].id)
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

FUNC BOOL IS_BARRICADE_DESTROYED(INT iBarricadeNumber)

	INT iStart, iEnd
	IF iBarricadeNumber = 1
		iStart = veh_blocker1
		iEnd = veh_blocker2
	ELIF iBarricadeNumber = 2
		iStart = veh_blocker3
		iEnd = veh_blocker4
	ELSE
		CERRORLN(DEBUG_MISSION, "IS_BARRICADE_DESTROYED - Invalid barricade number passed to script")
		RETURN TRUE
	ENDIF
	
	INT iter
	FOR iter = iStart TO iEnd
		IF NOT DOES_ENTITY_EXIST(vehicle[iter].id)
			RETURN TRUE
		ELSE
			IF NOT IS_VEHICLE_DRIVEABLE(vehicle[iter].id)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DISTANCE_TO_BARRICADE_1()
		IF NOT IS_ENTITY_DEAD(vehicle[veh_blocker1].id)
			RETURN GET_DISTANCE_BETWEEN_ENTITIES(vehicle[veh_blocker1].id, vehicle[veh_bulldozer].id)
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehicle[veh_blocker2].id)
			RETURN GET_DISTANCE_BETWEEN_ENTITIES(vehicle[veh_blocker2].id, vehicle[veh_bulldozer].id)
		ENDIF
		
	RETURN -1.0
ENDFUNC

FUNC FLOAT GET_DISTANCE_TO_BARRICADE_2()
		IF NOT IS_ENTITY_DEAD(vehicle[veh_blocker3].id)
			RETURN GET_DISTANCE_BETWEEN_ENTITIES(vehicle[veh_blocker3].id, vehicle[veh_bulldozer].id)
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehicle[veh_blocker4].id)
			RETURN GET_DISTANCE_BETWEEN_ENTITIES(vehicle[veh_blocker4].id, vehicle[veh_bulldozer].id)
		ENDIF
		
	RETURN -1.0
ENDFUNC

FUNC BOOL SHOULD_BULLDOZER_RAM_BARRICADE()
	// if we're near a barricade, GO TO RAMMING SPEED!!
	IF NOT IS_BARRICADE_DESTROYED(1)
		IF GET_DISTANCE_TO_BARRICADE_1() < iBARRICADECHARGEDISTANCE
			RETURN TRUE
		ENDIF 
	ENDIF
	IF NOT IS_BARRICADE_DESTROYED(2)
		IF GET_DISTANCE_TO_BARRICADE_2() < iBARRICADECHARGEDISTANCE
			RETURN TRUE
		ENDIF 
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_NEW_BULLDOZER_PLAYBACK_SPEED()
	FLOAT fRetVal = 6
	
	FLOAT fClosestEnemy = 100

	IF SHOULD_BULLDOZER_RAM_BARRICADE()
		SET_DRIVE_TASK_DRIVING_STYLE(GET_BULLDOZER_DRIVER(), ENUM_TO_INT(DRIVINGMODE_PLOUGHTHROUGH))
		RETURN SPEED_Fast
	ELSE
		SET_DRIVE_TASK_DRIVING_STYLE(GET_BULLDOZER_DRIVER(), ENUM_TO_INT(DRIVINGMODE_PLOUGHTHROUGH) | ENUM_TO_INT(DF_StopForPeds))
	ENDIF
	
	INT iter
	REPEAT MAX_SAWMILL_ENEMIES iter
		IF IS_ENTITY_OK(enemyPeds[iter].id)
			IF enemyPeds[iter].iSquad <> SQUAD_Barricade1
			AND enemyPeds[iter].iSquad <> SQUAD_Barricade2
				FLOAT fThisDistance = GET_DISTANCE_BETWEEN_ENTITIES(enemyPeds[iter].id, vehicle[veh_bulldozer].id)
				IF fThisDistance < fClosestEnemy
					fClosestEnemy = fThisDistance
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF fClosestEnemy < 10
		fRetVal = SPEED_VerySlow
	ELIF fClosestEnemy < 30
		fRetVal = SPEED_Slow
	ELIF fClosestEnemy < 40
		fRetVal = SPEED_Medium
	ELIF fClosestEnemy < 70
		fRetVal = SPEED_Fast
	ENDIF
	
	RETURN fRetVal
ENDFUNC


PROC TASK_TANISHA_DRIVE_OFF()
	IF IS_ENTITY_OK(ped_tanisha)
		CPRINTLN(DEBUG_MISSION, "TASK_TANISHA_DRIVE_OFF start")
		CLEAR_AREA(<<14.7838, 554.4817, 175.7667>>, 3, FALSE)
		veh_Tanisha = CREATE_VEHICLE(SURGE, <<14.7838, 554.4817, 175.7667>>, 315.6160)
		ped_tanisha_drive = CREATE_PED_INSIDE_VEHICLE(veh_Tanisha, PEDTYPE_MISSION, A_M_Y_BUSINESS_01)
		IF IS_VEHICLE_DRIVEABLE(veh_tanisha)
			SET_PED_INTO_VEHICLE(ped_tanisha, veh_tanisha, VS_FRONT_RIGHT)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_tanisha, TRUE)
			SEQUENCE_INDEX iSeq
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_VEHICLE_DRIVE_TO_COORD(null, veh_tanisha, <<212.2176, 632.6493, 186.8713>>, 20, DRIVINGSTYLE_NORMAL, SURGE, DRIVINGMODE_STOPFORCARS, 5, 5)
				IF IS_ENTITY_OK(PLAYER_PED_ID())
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, INFINITE_TASK_TIME, TRUE)
				ENDIF
			CLOSE_SEQUENCE_TASK(iSeq)
			TASK_PERFORM_SEQUENCE(ped_tanisha_drive, iSeq)
			CLEAR_SEQUENCE_TASK(iSeq)
			SET_PED_KEEP_TASK(ped_tanisha_drive, TRUE)
			SET_PED_KEEP_TASK(ped_tanisha, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(ped_tanisha)
			SET_PED_AS_NO_LONGER_NEEDED(ped_tanisha_drive)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_tanisha)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CREATE_FRANKLIN_CAR(enumMissionStage eNewStage)
	CPRINTLN(DEBUG_MISSION, "SET_UP_FRANKLIN_CAR setting up car for stage #", eNewStage)
	
	VECTOR vFranklinCarPosition = <<14.3669, 548.6658, 175.1985>>
	FLOAT fFranklinCarHeading = 61.4909
	SWITCH eNewStage
		CASE STAGE_INTRO_CUTSCENE
		CASE STAGE_MEET_THE_CREW
			vFranklinCarPosition = <<14.3669, 548.6658, 175.1985>>
			fFranklinCarHeading = 61.4909
		BREAK
		CASE STAGE_GET_INTO_POSITION
		CASE STAGE_ATTACK_SAWMILL						
		CASE STAGE_GET_LAMAR_OUT
			vFranklinCarPosition = <<-733.9679, 5315.1636, 71.9083>>
			fFranklinCarHeading = 265.2296
		BREAK
		CASE STAGE_DRIVE_HOME
			vFranklinCarPosition = <<-594.8925, 5230.1704, 70.6033>>
			fFranklinCarHeading = 117
		BREAK
		CASE STAGE_OUTRO_CUTSCENE
		BREAK
	ENDSWITCH
	
	IF eNewStage <= STAGE_MEET_THE_CREW
		SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vFranklinCarPosition, fFranklinCarHeading)
		RETURN TRUE
	ELSE
		RETURN CREATE_PLAYER_VEHICLE(vehicle[veh_franklin].id, CHAR_FRANKLIN, vFranklinCarPosition, fFranklinCarHeading, TRUE, VEHICLE_TYPE_CAR)
	ENDIF
ENDFUNC

FUNC BOOL CREATE_MICHAEL_CAR()
	RETURN CREATE_PLAYER_VEHICLE(vehicle[veh_michael].id, CHAR_MICHAEL, <<-716.7578, 5319.3579, 70.8262>>, 261.2129)	
ENDFUNC

FUNC BOOL CREATE_TREVOR_CAR()
	RETURN CREATE_PLAYER_VEHICLE(vehicle[veh_trevor].id, CHAR_TREVOR, <<-718.4451, 5304.8486, 71.6183>>, 15)
ENDFUNC

FUNC BOOL CREATE_PLAYER_VEHICLES(enumMissionStage eNewStage)
	BOOL bRetVal
	
	SWITCH eNewStage
		CASE STAGE_INTRO_CUTSCENE
		CASE STAGE_OUTRO_CUTSCENE
			bRetVal = CREATE_FRANKLIN_CAR(eNewStage)
		BREAK
		CASE STAGE_MEET_THE_CREW
		CASE STAGE_GET_INTO_POSITION
		CASE STAGE_ATTACK_SAWMILL						
		CASE STAGE_GET_LAMAR_OUT
		CASE STAGE_DRIVE_HOME
			IF CREATE_FRANKLIN_CAR(eNewStage)
			AND CREATE_MICHAEL_CAR()
			AND CREATE_TREVOR_CAR()
				bRetVal = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bRetVal
ENDFUNC

FUNC VECTOR GET_PLAYER_STARTING_POSITION(enumMissionStage eNewStage)
	VECTOR vReturnVec
	SWITCH eNewStage
		CASE STAGE_INTRO_CUTSCENE
			vReturnVec = <<6.6466, 535.2598, 175.0279>>
		BREAK
		CASE STAGE_MEET_THE_CREW
			vReturnVec = <<8.7386, 540.9130, 175.0275>>
		BREAK
		CASE STAGE_GET_INTO_POSITION
			vReturnVec = <<-706.9433, 5311.5801, 70.2264>>
		BREAK
		CASE STAGE_ATTACK_SAWMILL
			vReturnVec = <<-555.5608, 5434.0308, 60.9470>>
		BREAK
		CASE STAGE_GET_LAMAR_OUT
			vReturnVec = <<-523.7297, 5307.5522, 79.2683>>
		BREAK
		CASE STAGE_DRIVE_HOME
			vReturnVec = <<-593.7846, 5228.6670, 70.5013>>
		BREAK
		CASE STAGE_OUTRO_CUTSCENE
			vReturnVec = <<-59.9714, -1456.2513, 31.1158>>
		BREAK
	ENDSWITCH
	
	RETURN vReturnVec
ENDFUNC

FUNC BOOL SET_UP_FRANKLIN(enumMissionStage eNewStage)
	IF NOT IS_ENTITY_OK(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	FLOAT fFranklinHeading
	
	SWITCH eNewStage
		CASE STAGE_INTRO_CUTSCENE
			fFranklinHeading = 180
		BREAK
		CASE STAGE_MEET_THE_CREW
			fFranklinHeading = 343.3962
		BREAK
		CASE STAGE_GET_INTO_POSITION
			fFranklinHeading = 279.1242
		BREAK
		CASE STAGE_ATTACK_SAWMILL
			fFranklinHeading = 218
		BREAK
		CASE STAGE_GET_LAMAR_OUT
			fFranklinHeading = 251
		BREAK
		CASE STAGE_DRIVE_HOME
			fFranklinHeading = 148
		BREAK
		CASE STAGE_OUTRO_CUTSCENE
			fFranklinHeading = 0.0
		BREAK
	ENDSWITCH
	
	IF eNewStage > STAGE_INTRO_CUTSCENE
		SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_PLAYER_STARTING_POSITION(eNewStage))
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinHeading)
	ENDIF
	
	SET_UP_PLAYER(ped_franklin)
	
	RETURN TRUE	
ENDFUNC

FUNC BOOL CREATE_MICHAEL(enumMissionStage eNewStage, BOOL bMidStageCreation = FALSE)
	BOOL bRetVal
	BOOL bCreatePlayer = FALSE
	
	VECTOR vMichaelPosition
	FLOAT fMichaelHeading

	SWITCH eNewStage
		CASE STAGE_MEET_THE_CREW
		CASE STAGE_ATTACK_SAWMILL
		CASE STAGE_GET_LAMAR_OUT
			bCreatePlayer = TRUE
			
			SWITCH eNewStage
				CASE STAGE_MEET_THE_CREW
					vMichaelPosition = <<-715.2372, 5317.7798, 70.6856>>
					fMichaelHeading = 221.4874
					
					bCreatePlayer = bMidStageCreation
				BREAK
				CASE STAGE_ATTACK_SAWMILL
					vMichaelPosition = vMichaelSnipePosition
					fMichaelHeading = 14.8016
					
					bCreatePlayer = bMidStageCreation
				BREAK
				CASE STAGE_GET_LAMAR_OUT
					vMichaelPosition = vMichaelSnipePosition
					fMichaelHeading = 14.8016
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF bCreatePlayer
		IF CREATE_PLAYER_PED_ON_FOOT(missionPeds[ped_michael].id, CHAR_MICHAEL, vMichaelPosition, fMichaelHeading)
			SET_UP_PLAYER(ped_michael)
			
			IF eNewStage = STAGE_MEET_THE_CREW
				TASK_START_SCENARIO_IN_PLACE(missionPeds[ped_michael].id, "WORLD_HUMAN_AA_SMOKE")
			ENDIF
			
			bRetVal = TRUE
		ENDIF
	ELSE
		bRetVal = TRUE
	ENDIF
	
	RETURN bRetVal
ENDFUNC

FUNC BOOL CREATE_TREVOR(enumMissionStage eNewStage, BOOL bMidStageCreation = FALSE)
	BOOL bRetVal
	BOOL bCreatePlayer = FALSE
	
	VECTOR vTrevorPosition
	FLOAT fTrevorHeading

	SWITCH eNewStage
		CASE STAGE_GET_INTO_POSITION
		CASE STAGE_MEET_THE_CREW
		CASE STAGE_ATTACK_SAWMILL						
		CASE STAGE_GET_LAMAR_OUT
			bCreatePlayer = TRUE
			
			SWITCH eNewStage
				CASE STAGE_MEET_THE_CREW
					vTrevorPosition = <<-719.1467, 5308.0078, 71.6904>>
					fTrevorHeading = 283
					bCreatePlayer = bMidStageCreation
				BREAK
				CASE STAGE_GET_INTO_POSITION
					vTrevorPosition = vTrevorGoToPositionPosition
					fTrevorHeading = vTrevorGoToPositionHeading
				BREAK
				CASE STAGE_ATTACK_SAWMILL
					vTrevorPosition = <<-548.6953, 5431.8882, 61.4631>>
					fTrevorHeading = 206
				BREAK
				CASE STAGE_GET_LAMAR_OUT
					vTrevorPosition = <<-513.1269, 5304.6904, 79.2683>>
					fTrevorHeading = 248
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF bCreatePlayer
		IF CREATE_PLAYER_PED_ON_FOOT(missionPeds[ped_trevor].id, CHAR_TREVOR, vTrevorPosition, fTrevorHeading)
			SET_UP_PLAYER(ped_trevor)
			
			IF eNewStage = STAGE_MEET_THE_CREW
				TASK_START_SCENARIO_IN_PLACE(missionPeds[ped_trevor].id, "WORLD_HUMAN_BINOCULARS")
			ENDIF
			
			bRetVal = TRUE
		ENDIF
	ELSE
		bRetVal = TRUE
	ENDIF
	
	RETURN bRetVal
ENDFUNC

FUNC BOOL CREATE_LAMAR(enumMissionStage eNewStage, BOOL bMidStageCreation = FALSE)
	BOOL bRetVal = FALSE
	
	BOOL bCreateLamar = FALSE
	
	VECTOR vLamarPosition
	FLOAT fLamarHeading

	SWITCH eNewStage
		CASE STAGE_ATTACK_SAWMILL						
		CASE STAGE_GET_LAMAR_OUT
		CASE STAGE_DRIVE_HOME			
			SWITCH eNewStage
				CASE STAGE_ATTACK_SAWMILL
					vLamarPosition = vLamarInjuredPosition
					fLamarHeading = fLamarInjuredHeading
					
					bCreateLamar = bMidStageCreation
				BREAK
				CASE STAGE_GET_LAMAR_OUT
					vLamarPosition = vLamarInjuredPosition
					fLamarHeading = fLamarInjuredHeading
					
					bCreateLamar = TRUE
				BREAK
				CASE STAGE_DRIVE_HOME
					vLamarPosition = <<-594.7075, 5232.2505, 70.4603>>
					fLamarHeading = 115
					
					bCreateLamar = TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF bCreateLamar
		IF CREATE_NPC_PED_ON_FOOT(missionPeds[ped_lamar].id, CHAR_LAMAR, vLamarPosition, fLamarHeading)		
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(missionPeds[ped_lamar].id,TRUE)
			
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(missionPeds[ped_lamar].id, TRUE)
			SET_PED_CAN_BE_TARGETTED(missionPeds[ped_lamar].id, FALSE)
			SET_PED_MAX_MOVE_BLEND_RATIO(missionPeds[ped_lamar].id,pedmove_run)
			SET_PED_RELATIONSHIP_GROUP_HASH(missionPeds[ped_lamar].id, relgroupIgnore)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(missionPeds[ped_lamar].id, FALSE)
			
			IF eNewStage = STAGE_ATTACK_SAWMILL
				TASK_START_SCENARIO_IN_PLACE(missionPeds[ped_lamar].id,"WORLD_HUMAN_STUPOR")
			ENDIF
			
			bRetVal = TRUE
		ENDIF
	ELSE
		bRetVal = TRUE
	ENDIF
	
	RETURN bRetVal
ENDFUNC

PROC SQUAD_REGISTER_TARGET(INT iSquad, PED_INDEX targetPed)

	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id) AND IS_ENTITY_OK(targetPed)
				REGISTER_TARGET(enemyPeds[i].id, targetPed)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC REGISTER_ENEMIES_FOR_PED(PED_INDEX aimPed)
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF IS_ENTITY_OK(enemyPeds[i].id) AND IS_ENTITY_OK(aimPed)
			REGISTER_TARGET(aimPed, enemyPeds[i].id)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC DELETE_SQUAD(INT iSquad)
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				DELETE_PED(enemyPeds[i].id)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC TASK_SQUAD_ATTACK_FRANKLIN_AROUND_LAMAR(INT iSquad)
	IF NOT IS_ENTITY_OK(missionPeds[ped_lamar].id)
		EXIT
	ENDIF
	
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(enemyPeds[i].id, missionPeds[ped_lamar].id, (<<0, 0, 0>>), 25)
				TASK_COMBAT_PED(enemyPeds[i].id, missionPeds[ped_franklin].id)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC TASK_SQUAD_ATTACK_HATED_TARGETS(INT iSquad)
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				REMOVE_PED_DEFENSIVE_AREA(enemyPeds[i].id)
				FLOAT fRadius = 200
				REGISTER_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, fRadius)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, fRadius)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC TASK_SQUAD_DEFEND_IN_PLACE(INT iSquad)
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				FLOAT fRadius = 200
				REGISTER_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, fRadius)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, fRadius)
				SET_PED_SPHERE_DEFENSIVE_AREA(enemyPeds[i].id, GET_ENTITY_COORDS(enemyPeds[i].id), 8)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC TASK_SQUAD_ATTACK_HATED_TARGETS_AROUND_BULLDOZER(INT iSquad)
	IF NOT DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
		EXIT
	ENDIF
	
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				TASK_COMBAT_HATED_TARGETS_IN_AREA(enemyPeds[i].id, GET_ENTITY_COORDS(vehicle[veh_bulldozer].id), 80)
				SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(enemyPeds[i].id, vehicle[veh_bulldozer].id, <<0, 0, 0>>, 50)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC TASK_SQUAD_DEFEND_LAMAR(INT iSquad)
	IF NOT DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
		EXIT
	ENDIF
	
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				FLOAT fRadius = 200
				REGISTER_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, fRadius)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, fRadius)
				SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(enemyPeds[i].id, tbLamarGuardsClear)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC TASK_SQUAD_DEFEND_LAMAR_ENTRANCE(INT iSquad)
	IF NOT DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
		EXIT
	ENDIF
	
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				FLOAT fRadius = 200
				REGISTER_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, fRadius)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, fRadius)
				SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(enemyPeds[i].id, tbLamarGuardsEntrance)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC TASK_SQUAD_FOLLOW_BULLDOZER(INT iSquad)
	IF NOT DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
		EXIT
	ENDIF
	
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				TASK_COMBAT_HATED_TARGETS_IN_AREA(enemyPeds[i].id, GET_ENTITY_COORDS(vehicle[veh_bulldozer].id), 80)
				FLOAT fBulldozerYOffst = 15.0
				SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(enemyPeds[i].id, vehicle[veh_bulldozer].id, <<0, -fBulldozerYOffst, 0>>, fBulldozerYOffst + 4.0)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC TASK_SQUAD_DEFEND_VEHICLE(INT iSquad, VEHICLE_INDEX defendVehicle)
	IF NOT DOES_ENTITY_EXIST(defendVehicle)
		EXIT
	ENDIF
	
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF enemyPeds[i].iSquad = iSquad
			IF IS_ENTITY_OK(enemyPeds[i].id)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemyPeds[i].id, 80.0)
				SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(enemyPeds[i].id, defendVehicle, <<0, 0, 0>>, 10.0)
				SET_PED_COMBAT_MOVEMENT(enemyPeds[i].id, CM_DEFENSIVE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC TASK_EXIT_VEHICLE_THEN_DEFEND(PED_INDEX thisPed, VEHICLE_INDEX defendVehicle)
	IF NOT IS_ENTITY_OK(thisPed)
		EXIT
	ENDIF
	IF NOT DOES_ENTITY_EXIST(defendVehicle)
		EXIT
	ENDIF
	
	SEQUENCE_INDEX iSeq
	OPEN_SEQUENCE_TASK(iSeq)
		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
		TASK_LEAVE_ANY_VEHICLE(NULL)
		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
	CLOSE_SEQUENCE_TASK(iSeq)
	TASK_PERFORM_SEQUENCE(thisPed, iSeq)
	CLEAR_SEQUENCE_TASK(iSeq)
	
	SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(thisPed, defendVehicle, <<0, 0, 0>>, 10.0)
ENDPROC


FUNC BOOL HAS_PED_JUST_KILLED_PED(PED_INDEX killerPed, PED_INDEX targetPed)
	IF NOT IS_ENTITY_OK(killerPed)
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(targetPed)
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(targetPed)
		ENTITY_INDEX killerEntity = GET_PED_SOURCE_OF_DEATH(targetPed)
		IF DOES_ENTITY_EXIST(killerEntity)
			IF IS_ENTITY_A_PED(killerEntity)
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(killerEntity) = killerPed
					IF GET_PED_TIME_OF_DEATH(targetPed) = GET_GAME_TIMER()
						CPRINTLN(DEBUG_MISSION, "HAS_PED_JUST_KILLED_ENEMY - dead enemy was killed at: ", GET_PED_TIME_OF_DEATH(targetPed), " , game time is: ", GET_GAME_TIMER())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PED_JUST_KILLED_ANY_ENEMY(PED_INDEX killerPed)
	INT enemyIter
	REPEAT MAX_SAWMILL_ENEMIES enemyIter
		IF HAS_PED_JUST_KILLED_PED(killerPed, enemyPeds[enemyIter].id)
			RETURN TRUE
		ENDIF
//		IF DOES_ENTITY_EXIST(enemyPeds[enemyIter].id)
//			IF IS_ENTITY_DEAD(enemyPeds[enemyIter].id)
//				CPRINTLN(DEBUG_MISSION, "BSWtest ped #", enemyIter, " time of death: ", GET_PED_TIME_OF_DEATH(enemyPeds[enemyIter].id))
//			ENDIF
//		ENDIF
		
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


PROC UPDATE_ENEMY_BLIPS()
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	BOOL bForceBlipOn = FALSE
	
	INT i
	REPEAt MAX_SAWMILL_ENEMIES i
		IF DOES_ENTITY_EXIST(enemyPeds[i].id)
			IF i = iLamarAimGuard OR i = iRPGEnemy
				bForceBlipOn = TRUE
			ENDIF
			UPDATE_AI_PED_BLIP(enemyPeds[i].id, enemyPeds[i].aiBlip, BLIP_GANG_ENEMY, DEFAULT, bForceBlipOn)
		ENDIF
	ENDREPEAT	
ENDPROC

PROC SPAWN_SAWMILL_VEHICLES(enumMissionStage eNewStage)
	CPRINTLN(DEBUG_MISSION, "SPAWN_SAWMILL_VEHICLES start...")

	IF eNewStage < STAGE_GET_LAMAR_OUT
		vehicle[veh_drugdeal].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BISON),<<-597.0967, 5297.9897, 69.2149>>, 257.5257)	// barricade1 SORTA
		vehicle[veh_blocker1].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_FELON), vBarricade1Position, fBarricade1Heading)	// barricade2
		vehicle[veh_blocker2].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BUCCANEER), vBarricade2Position, fBarricade2Heading)	// barricade3
		vehicle[veh_blocker3].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BUCCANEER), vBarricade3Position, fBarricade3Heading)	// barricade4
		vehicle[veh_blocker4].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BISON), vBarricade4Position, fBarricade4Heading)	// barricade5
		
		SET_VEHICLE_DOORS_LOCKED(vehicle[veh_blocker1].id, VEHICLELOCK_LOCKED)
		SET_VEHICLE_DOORS_LOCKED(vehicle[veh_blocker2].id, VEHICLELOCK_LOCKED)
		SET_VEHICLE_DOORS_LOCKED(vehicle[veh_blocker3].id, VEHICLELOCK_LOCKED)
		SET_VEHICLE_DOORS_LOCKED(vehicle[veh_blocker4].id, VEHICLELOCK_LOCKED)
		
		SET_VEHICLE_ON_GROUND_PROPERLY(vehicle[veh_blocker3].id)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehicle[veh_blocker4].id)
	ENDIF

	vehicle[veh_carblock2].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BISON), <<-572.3868, 5235.0562, 69.5029>>, 128)
	vehicle[veh_cover1].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BUCCANEER),<<-555.6071, 5371.3384, 69.2279>>, 341.4286)
	vehicle[veh_cover2].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_FELON), <<-564.8663, 5381.9126, 68.8198>>, 104.1)
	vehicle[veh_cover3].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BUCCANEER), <<-567.8027, 5338.6128, 69.2150>>, 71)
	vehicle[veh_cover4].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_FELON),<<-583.0345, 5372.5757, 69.7851>>, 30.8574)
	vehicle[veh_cover5].id = CREATE_VEHICLE(GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_FELON), <<-584.0190, 5323.9644, 69.2150>>, 339.6)

	VECTOR vLocalBulldozerPos = vBulldozer2SpawnPosition
	FLOAT fLocalBulldozerHeading = fBulldozer2SpawnHeading
	IF eNewStage >= STAGE_GET_LAMAR_OUT
		vLocalBulldozerPos = <<-510.3933, 5244.3394, 79.3040>>
		fLocalBulldozerHeading = 272.4
	ENDIF

	vehicle[veh_bulldozer].id = CREATE_VEHICLE(BULLDOZER, vLocalBulldozerPos, fLocalBulldozerHeading)
	SET_ENTITY_PROOFS(vehicle[veh_bulldozer].id, TRUE, TRUE, TRUE, TRUE, TRUE)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehicle[veh_bulldozer].id, "FRANKLIN_2_BULLDOZER_Group")
	SET_VEHICLE_PROVIDES_COVER(vehicle[veh_bulldozer].id, FALSE)	// we might want to toggle this based on movement, but for now just turn it off		
	SET_VEHICLE_DOORS_LOCKED(vehicle[veh_bulldozer].id, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
	SET_ENEMY_PEDS_FORCE_FALL_OFF_THIS_VEHICLE(vehicle[veh_bulldozer].id, TRUE)
ENDPROC

PROC SPAWN_ENEMY_SQUAD(INT iSquad)
	CPRINTLN(DEBUG_MISSION, "SPAWN_ENEMY_SQUAD spawning squad ", iSquad)

	INT enemyIter
	REPEAT MAX_SAWMILL_ENEMIES enemyIter
		IF enemySpawnPoints[enemyIter].iSquad = iSquad
			enemyPeds[enemyIter].id = CREATE_PED(pedtype_mission, GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_BALLA), enemySpawnPoints[enemyIter].vCoords, enemySpawnPoints[enemyIter].fHeading)
			IF IS_ENTITY_OK(enemyPeds[enemyIter].id)
			
				TEXT_LABEL_15 tempName = "squad"
				tempName += enemySpawnPoints[enemyIter].iSquad
				tempName += "_"
				tempName +=enemyIter
				SET_PED_NAME_DEBUG(enemyPeds[enemyIter].id, tempName)
				
				enemyPeds[enemyIter].iSquad = enemySpawnPoints[enemyIter].iSquad
				
				WEAPON_TYPE myWeapon = WEAPONTYPE_PISTOL
				IF enemySpawnPoints[enemyIter].iWeaponType = WEAPONTYPE_INVALID
					IF enemyIter % 2 = 0
						myWeapon = WEAPONTYPE_SMG
					ELSE
						myWeapon = WEAPONTYPE_PISTOL
					ENDIF
				ELSE
					myWeapon = enemySpawnPoints[enemyIter].iWeaponType
				ENDIF
			
				GIVE_WEAPON_TO_PED(enemyPeds[enemyIter].id, myWeapon, 150, TRUE)
				
				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemyPeds[enemyIter].id)
				
				SET_UP_ENEMY_PED(enemyPeds[enemyIter])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC PED_INDEX GET_TARGET_PED_FOR_MICHAEL()
	IF NOT IS_ENTITY_OK(missionPeds[ped_michael].id)
		RETURN NULL
	ENDIF
	IF NOT IS_ENTITY_OK(missionPeds[ped_franklin].id)
		RETURN NULL
	ENDIF
	
	PED_INDEX targetPed

	INT iter

	FLOAT fCheckDistance
	FLOAT fSmallestDistance = 1000
			
	// now find one that's close to Franklin
	REPEAT MAX_SAWMILL_ENEMIES iter
		IF iter <> iRPGEnemy
			IF IS_ENTITY_OK(enemyPeds[iter].id)
				IF IS_ENTITY_ON_SCREEN(enemyPeds[iter].id)
					fCheckDistance = GET_DISTANCE_BETWEEN_ENTITIES(missionPeds[ped_franklin].id, enemyPeds[iter].id)
					IF fCheckDistance < fSmallestDistance
						fSmallestDistance = fCheckDistance
						targetPed = enemyPeds[iter].id
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN targetPed

ENDFUNC

PROC UPDATE_MICHAEL_PED()
	IF NOT IS_ENTITY_OK(missionPeds[ped_michael].id)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_OK(PLAYER_PED_ID())	
		EXIT
	ENDIF
	
	IF PLAYER_PED_ID() = missionPeds[ped_michael].id
		// OMGHAX if the player, as Michael, has just killed a ped
		// then give Franklin some invulnerability! CRAZINESS
		IF HAS_PED_JUST_KILLED_ANY_ENEMY(missionPeds[ped_michael].id)
			START_TIMER_NOW_SAFE(tmrMichaelKillTimer)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(missionPeds[ped_franklin].id, TRUE)
		ENDIF

		eMichaelState = MICHAEL_COMBAT_INIT
		
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
		
		EXIT
	ENDIF

	VECTOR vMichaelPosition = GET_ENTITY_COORDS(missionPeds[ped_michael].id)
	
	SWITCH eMichaelState
		CASE MICHAEL_SETUP
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				eMichaelState = MICHAEL_COMBAT_INIT
			ENDIF
		BREAK
		CASE MICHAEL_COMBAT_INIT
			SET_PED_SPHERE_DEFENSIVE_AREA(missionPeds[ped_michael].id, vMichaelSnipePosition, 5.0)
			TASK_COMBAT_HATED_TARGETS_IN_AREA(missionPeds[ped_michael].id, vMichaelPosition, 300)
					
			iMichaelShootDelay = 3000
			eMichaelState = MICHAEL_AIM
		BREAK
		CASE MICHAEL_AIM
			IF NOT IS_PED_SHOOTING(missionPeds[ped_michael].id)
				IF IS_ENTITY_OK(enemyPeds[iRPGEnemy].id)
					TASK_AIM_GUN_AT_COORD(missionPeds[ped_michael].id, <<-546.8980, 5286.7007, 85.4466>>, INFINITE_TASK_TIME)
					CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_PED michael is aiming at RPG enemy")
				ELSE
					IF IS_ENTITY_OK(missionPeds[ped_franklin].id)
						TASK_AIM_GUN_AT_ENTITY(missionPeds[ped_michael].id, missionPeds[ped_franklin].id, INFINITE_TASK_TIME)
						CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_PED michael is aiming at franklin")
					ENDIF
				ENDIF
				iMichaelAimTimer = GET_GAME_TIMER()
				eMichaelState = MICHAEL_SHOOT
			ENDIF
		BREAK
		CASE MICHAEL_SHOOT
			IF iMichaelAimTimer + iMichaelShootDelay < GET_GAME_TIMER() 
				VECTOR vTargetPos
				PED_INDEX targetPed
				targetPed = GET_TARGET_PED_FOR_MICHAEL()
				IF IS_ENTITY_OK(targetPed)
					vTargetPos = GET_ENTITY_COORDS(targetPed)
				ENDIF
				
				BOOL bPerfectAccuracy
				IF GET_RANDOM_INT_IN_RANGE(0, 10) > 2
					bPerfectAccuracy = TRUE
				ELSE
					bPerfectAccuracy = FALSE
				ENDIF

				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_ENTITY_COORDS(missionPeds[ped_michael].id), vTargetPos, 200, bPerfectAccuracy, WEAPONTYPE_HEAVYSNIPER)
				
				iMichaelShootDelay = GET_RANDOM_INT_IN_RANGE(5000, 10000)
				eMichaelState = MICHAEL_AIM
			ENDIF
		BREAK
		CASE MICHAEL_ESCAPE
			SEQUENCE_INDEX iSeq
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-534.2451, 5118.3823, 90.0078>>, PEDMOVE_RUN)
				TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 100, INFINITE_TASK_TIME)
			CLOSE_SEQUENCE_TASK(iSeq)
			TASK_PERFORM_SEQUENCE(missionPeds[ped_michael].id, iSeq)
			CLEAR_SEQUENCE_TASK(iSeq)
			
			eMichaelState = MICHAEL_COMPLETE
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SET_UP_RELATIONSHIP_GROUPS()
	ADD_RELATIONSHIP_GROUP("IgnoreGroup", relgroupIgnore)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relgroupIgnore, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relgroupIgnore)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_HATES_PLAYER, relgroupIgnore)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relgroupIgnore, RELGROUPHASH_HATES_PLAYER)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_HATES_PLAYER)
ENDPROC

PROC PRE_STREAM_ASSETS_FOR_STAGE(enumMissionStage eNewStage)
	CPRINTLN(DEBUG_MISSION, "PRE_STREAM_ASSETS_FOR_STAGE for stage #", eNewStage)
	LOAD_MODELS_FOR_STAGE(eNewStage)
	LOAD_WAYPOINT_RECORDINGS_FOR_STAGE(eNewStage)
	LOAD_VEHICLE_RECORDINGS_FOR_STAGE(eNewStage)
	LOAD_SPECIAL_ASSETS_FOR_STAGE(eNewStage)
	LOAD_WEAPONS_FOR_STAGE(eNewStage)
	LOAD_MISSION_TEXT()
ENDPROC

FUNC BOOL STREAM_ASSETS_FOR_STAGE(enumMissionStage eNewStage)
	IF LOAD_MODELS_FOR_STAGE(eNewStage)
	AND LOAD_WAYPOINT_RECORDINGS_FOR_STAGE(eNewStage)
	AND LOAD_VEHICLE_RECORDINGS_FOR_STAGE(eNewStage)
	AND LOAD_SPECIAL_ASSETS_FOR_STAGE(eNewStage)
	AND LOAD_WEAPONS_FOR_STAGE(eNewStage)
	AND LOAD_MISSION_TEXT()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_STAGE_DEBUG_NAME(enumMissionStage eNewStage)
	STRING strRetVal

	SWITCH eNewStage
		CASE STAGE_INTRO_CUTSCENE
			strRetVal = "Intro Cutscene"
		BREAK
		CASE STAGE_MEET_THE_CREW
			strRetVal = "Meet the Crew"
		BREAK
		CASE STAGE_GET_INTO_POSITION
			strRetVal = "Get Into Position"
		BREAK
		CASE STAGE_ATTACK_SAWMILL
			strRetVal = "Attack Sawmill"
		BREAK
		CASE STAGE_GET_LAMAR_OUT
			strRetVal = "Get Lamar Out"
		BREAK
		CASE STAGE_DRIVE_HOME
			strRetVal = "Drive Lamar Home"
		BREAK
		CASE STAGE_OUTRO_CUTSCENE
			strRetVal = "Outru Cutscene"
		BREAK
	ENDSWITCH
	
	RETURN strRetVal
ENDFUNC

FUNC BOOL SET_MISSION_STAGE(enumMissionStage eNewStage, BOOL bJumpingToStage = FALSE)
	BOOL bRetVal
	
	IF g_bMagdemoActive
		IF eNewStage < STAGE_MEET_THE_CREW
			eNewStage = STAGE_MEET_THE_CREW
		ENDIF
	ENDIF

	SWITCH eAdvanceStageState
		CASE ADVANCE_INITIALISE
			IF bJumpingToStage
				eIntroCutsceneCutsceneGoal = MISSION_GOAL_01
				eSawmillSceneCurrentGoal = MISSION_GOAL_01
				eMeetCrewCurrentGoal = MISSION_GOAL_01
				eGetIntoPositionCurrentGoal = MISSION_GOAL_01
				eAttackSawmillCurrentGoal = MISSION_GOAL_01
				eGetLamarOutCurrentGoal = MISSION_GOAL_01
				eDriveHomeCurrentGoal = MISSION_GOAL_01
				eOutroCutsceneCurrentGoal = MISSION_GOAL_01


				eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_IDLE
				eMeetCrewCurrentDialogue = MEET_CREW_MESSAGE_IDLE
				eAttackSawmillCurrentDialogue = ATTACK_MESSAGE_IDLE
				eGetLamarOutCurrentDialogue = GET_LAMAR_OUT_MESSAGE_IDLE
				eDriveHomeCurrentDialogue = DRIVE_HOME_MESSAGE_IDLE
								
				eTrevorState = TREVOR_INVALID
				eFranklinState = FRANKLIN_INVALID
				eMichaelState = MICHAEL_INVALID
				eLamarState = LAMAR_INVALID
				
				eRPGState = RPG_STATE_INIT
			ENDIF
			eAdvanceStageState = ADVANCE_STREAM
		BREAK
		CASE ADVANCE_STREAM
			IF STREAM_ASSETS_FOR_STAGE(eNewStage)
				CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE finished streaming, advancing to clean up for stage #", eNewStage)
				eAdvanceStageState = ADVANCE_CLEAN_UP
			ELSE
				CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE still streaming assets for stage #", eNewStage)
			ENDIF
		BREAK
		CASE ADVANCE_CLEAN_UP
			IF bJumpingToStage
				CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE we jumped, so clean stuff up and then go to create players...")
				DELETE_EVERYTHING(END_STATE_STAGE_SKIP)
				
				TRIGGER_MUSIC_EVENT("FRA2_FAIL")
				
				LOAD_SCENE(GET_PLAYER_STARTING_POSITION(eNewStage))
				
				SET_UP_RELATIONSHIP_GROUPS()
					
				eAdvanceStageState = ADVANCE_CREATE_PLAYER_VEHICLES
			ELSE
				CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE we're in flow, just go straight to set up")
				eAdvanceStageState = ADVANCE_SET_UP
			ENDIF
		BREAK
		CASE ADVANCE_CREATE_PLAYER_VEHICLES
			CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE creating player vehicles...")
			IF CREATE_PLAYER_VEHICLES(eNewStage)
				eAdvanceStageState = ADVANCE_CREATE_FRANKLIN
			ENDIF
		BREAK
		CASE ADVANCE_CREATE_FRANKLIN
			CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE creating Franklin...")
			IF SET_UP_FRANKLIN(eNewStage)
				eAdvanceStageState = ADVANCE_CREATE_TREVOR
			ENDIF
		BREAK
		CASE ADVANCE_CREATE_TREVOR
			CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE creating Trevor...")
			IF CREATE_TREVOR(eNewStage)
				eAdvanceStageState = ADVANCE_CREATE_MICHAEL
			ENDIF
		BREAK
		CASE ADVANCE_CREATE_MICHAEL
			CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE creating Michael...")
			IF CREATE_MICHAEL(eNewStage)
				eAdvanceStageState = ADVANCE_CREATE_LAMAR
			ENDIF
		BREAK
		CASE ADVANCE_CREATE_LAMAR
			CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE creating Lamar...")
			IF CREATE_LAMAR(eNewStage)
				eAdvanceStageState = ADVANCE_SET_UP
			ENDIF
		BREAK
		CASE ADVANCE_SET_UP
			SWITCH eNewStage
				CASE STAGE_INTRO_CUTSCENE
				BREAK
				CASE STAGE_MEET_THE_CREW
					INITIALISE_ATTACK_SAWMILL_DATA()
					
					IF bJumpingToStage
						IF NOT IS_ENTITY_OK(ped_tanisha)
							ped_tanisha = CREATE_PED(pedtype_mission, GET_MODEL_FOR_FRA2_MODEL_ENUM(FM_TANISHA), <<0, 0, 0>>)
						ENDIF
						TASK_TANISHA_DRIVE_OFF()
					ENDIF
				BREAK
				CASE STAGE_GET_INTO_POSITION
					INITIALISE_ATTACK_SAWMILL_DATA()
					
					SPAWN_ENEMY_SQUAD(SQUAD_PreGuards)
					SET_UP_SCRIPTED_SQUAD(SQUAD_PreGuards)
				
					IF bJumpingToStage
						SETUP_AREA_AROUND_SAWMILL()
						SPAWN_SAWMILL_VEHICLES(eNewStage)
					ENDIF
				BREAK
				CASE STAGE_ATTACK_SAWMILL
					INITIALISE_ATTACK_SAWMILL_DATA()
					
					IF bJumpingToStage
						SETUP_AREA_AROUND_SAWMILL()
						SPAWN_SAWMILL_VEHICLES(eNewStage)
						
						TRIGGER_MUSIC_EVENT("FRA2_ATTACK_RT")
					ENDIF
				BREAK
				CASE STAGE_GET_LAMAR_OUT
					IF bJumpingToStage
						INITIALISE_ATTACK_SAWMILL_DATA()
						
						SETUP_AREA_AROUND_SAWMILL()
						TRIGGER_MUSIC_EVENT("FRA2_FLEE_RT")
					ENDIF
				BREAK
				CASE STAGE_DRIVE_HOME
					IF bJumpingToStage
						SETUP_AREA_AROUND_SAWMILL()
						SPAWN_SAWMILL_VEHICLES(eNewStage)
					ENDIF
				BREAK
				CASE STAGE_OUTRO_CUTSCENE
				BREAK
			ENDSWITCH

			CPRINTLN(DEBUG_MISSION, "SET_MISSION_STAGE advancing to finished...")
			eAdvanceStageState = ADVANCE_FINISHED
		BREAK
		CASE ADVANCE_FINISHED
			eCurrentStage = eNewStage
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(eCurrentStage), GET_STAGE_DEBUG_NAME(eCurrentStage))
			
			IF bJumpingToStage
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
			
			DO_SCREEN_FADE_IN(1000)
			
			eAdvanceStageState = ADVANCE_INITIALISE
			
			bRetVal = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bRetVal
ENDFUNC
