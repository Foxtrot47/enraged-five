USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
USING "script_ped.sch"
using "commands_streaming.sch"
using "commands_camera.sch"
USING "Commands_clock.sch"
USING "Commands_interiors.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "script_blips.sch"
USING "commands_entity.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "locates_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "seamless_cuts.sch"
USING "CompletionPercentage_public.sch"
using "clearMissionArea.sch"
using "usefulcommands.sch"
using "rgeneral_include.sch"
USING "building_control_public.sch"
USING "vehicle_gen_public.sch"
USING "timelapse.sch"
USING "script_blips.sch"
USING "commands_event.sch"
USING "dialogue_public.sch"
USING "family_public.sch"
USING "player_scene_private.sch"
USING "Franklin2_support.sch"

PROC UPDATE_TREVOR_GET_INTO_POSITION()
	IF NOT IS_ENTITY_OK(missionPeds[ped_trevor].id)
		EXIT
	ENDIF
	
	SWITCH eTrevorState
		CASE TREVOR_SETUP
			eTrevorState = TREVOR_GO_TO_START
		BREAK
		CASE TREVOR_GO_TO_START
			IF GET_IS_WAYPOINT_RECORDING_LOADED(GET_WAYPOINT_LABEL(FW_TREVOR))
				TASK_FOLLOW_WAYPOINT_RECORDING(missionPeds[ped_trevor].id, GET_WAYPOINT_LABEL(FW_TREVOR), 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
			ENDIF
			
			eTrevorState = TREVOR_GOING_TO_START
		BREAK
		CASE TREVOR_GOING_TO_START
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(missionPeds[ped_trevor].id)
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(missionPeds[ped_trevor].id, PEDMOVE_RUN)
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_ENTITIES(missionPeds[ped_trevor].id, vehicle[veh_bulldozer].id) < 80
				IF GET_DISTANCE_BETWEEN_ENTITIES(missionPeds[ped_franklin].id, vehicle[ped_trevor].id) < 30
					CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_PED should talk about the bulldozer now")
					eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_DIA_TREVOR_SPOTS_BULLDOZER
				ENDIF
				
				eTrevorState = TREVOR_GOING_BULLDOZER
			ENDIF
		BREAK
		CASE TREVOR_GOING_BULLDOZER
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(missionPeds[ped_trevor].id)
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(missionPeds[ped_trevor].id, PEDMOVE_RUN)
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_ENTITIES(missionPeds[ped_trevor].id, vehicle[veh_bulldozer].id) < 40
			AND GET_DISTANCE_BETWEEN_ENTITIES(missionPeds[ped_franklin].id, vehicle[veh_bulldozer].id) < 40
				eTrevorState = TREVOR_COMBAT_INIT
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC UPDATE_HANDLE_GET_INTO_POSITION_MESSAGING()
	SWITCH eGetIntoPositionCurrentDialogue
		CASE GET_INTO_POSITION_MESSAGE_DIA_TREVOR_TO_BULLDOZER
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, missionPeds[ped_trevor].id, "TREVOR")
			IF CREATE_CONVERSATION(MyLocalPedStruct, convBlock, "LM2_follow", CONV_PRIORITY_HIGH)
				eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_GET_IN_POSITION
			ENDIF
		BREAK
		CASE GET_INTO_POSITION_MESSAGE_GET_IN_POSITION
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT("FRAN2_GETP", DEFAULT_GOD_TEXT_TIME, 1)

				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIf
				mission_blip = CREATE_BLIP_FOR_COORD(vBulldozer2SpawnPosition)
				
				eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_IDLE
			ENDIF
		BREAK
		CASE GET_INTO_POSITION_MESSAGE_WAIT_FOR_TREVOR
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIf
				
				PRINT("FRAN2_WAITTREV", DEFAULT_GOD_TEXT_TIME, 1)
				
				eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_IDLE
			ENDIF
		BREAK
		CASE GET_INTO_POSITION_MESSAGE_DIA_TREVOR_SPOTS_BULLDOZER
			IF CREATE_CONVERSATION_EXTRA("LM2_tsees", 2, missionPeds[ped_trevor].id, "TREVOR")
				CPRINTLN(DEBUG_MISSION, "UPDATE_HANDLE_GET_INTO_POSITION_MESSAGING playing first trevor bulldozer dialogue")
				eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_DIA_TREVOR_PLANS_BULLDOZER
			ENDIF
		BREAK
		CASE GET_INTO_POSITION_MESSAGE_DIA_TREVOR_PLANS_BULLDOZER
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_tsees2", 2, missionPeds[ped_trevor].id, "TREVOR")
					CPRINTLN(DEBUG_MISSION, "UPDATE_HANDLE_GET_INTO_POSITION_MESSAGING playing second trevor bulldozer dialogue")
					eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		CASE GET_INTO_POSITION_MESSAGE_IDLE
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DO_GET_INTO_POSITION()
	BOOL bRetVal = FALSE
	
	FAIL(FAIL_COVER_BLOWN)
	FAIL(FAIL_TREVOR_KILLED)
	FAIL(FAIL_BULLDOZER_KILLED)
	
	UPDATE_TREVOR_GET_INTO_POSITION()
	UPDATE_HANDLE_GET_INTO_POSITION_MESSAGING()
	
	SWITCH eGetIntoPositionCurrentGoal
		CASE MISSION_GOAL_01
			TRIGGER_MUSIC_EVENT("FRA2_HEAD_TO_POS")
			
			eTrevorState = TREVOR_SETUP
			eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_DIA_TREVOR_TO_BULLDOZER
			eGetIntoPositionCurrentGoal = MISSION_GOAL_02
		BREAK
		CASE MISSION_GOAL_02
			IF NOT bWaitForTrevor
				IF GET_DISTANCE_BETWEEN_ENTITIES(missionPeds[ped_franklin].id, vehicle[veh_bulldozer].id) < 20 AND eTrevorState <= TREVOR_GOING_BULLDOZER
					eGetIntoPositionCurrentDialogue = GET_INTO_POSITION_MESSAGE_WAIT_FOR_TREVOR
					bWaitForTrevor = TRUE
				ENDIF
			ENDIF
			
			IF eTrevorState = TREVOR_COMBAT_INIT
				PRE_STREAM_ASSETS_FOR_STAGE(STAGE_ATTACK_SAWMILL)
				
				DELETE_SQUAD(SQUAD_PreGuards)

				eGetIntoPositionCurrentGoal = MISSION_GOAL_03
			ENDIF
		BREAK
		CASE MISSION_GOAL_03
			bRetVal = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bRetVal
ENDFUNC
