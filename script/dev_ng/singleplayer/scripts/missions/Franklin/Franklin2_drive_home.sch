USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
USING "script_ped.sch"
using "commands_streaming.sch"
using "commands_camera.sch"
USING "Commands_clock.sch"
USING "Commands_interiors.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "script_blips.sch"
USING "commands_entity.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "locates_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "seamless_cuts.sch"
USING "CompletionPercentage_public.sch"
using "clearMissionArea.sch"
using "usefulcommands.sch"
using "rgeneral_include.sch"
USING "building_control_public.sch"
USING "vehicle_gen_public.sch"
USING "timelapse.sch"
USING "script_blips.sch"
USING "commands_event.sch"
USING "dialogue_public.sch"
USING "family_public.sch"
USING "player_scene_private.sch"
USING "Franklin2_support.sch"

PROC HANDLE_DRIVE_HOME_TEXT()
	SWITCH eDriveHomeCurrentDialogue
		CASE DRIVE_HOME_MESSAGE_BANTER1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_BANT1", 0, missionPeds[ped_franklin].id, "FRANKLIN", 3, missionPeds[ped_lamar].id, "LAMAR")
					eDriveHomeCurrentDialogue = DRIVE_HOME_MESSAGE_BANTER2
				ENDIF
			ENDIF
		BREAK
		CASE DRIVE_HOME_MESSAGE_BANTER2
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_BANT4", 0, missionPeds[ped_franklin].id, "FRANKLIN", 3, missionPeds[ped_lamar].id, "LAMAR")
					eDriveHomeCurrentDialogue = DRIVE_HOME_MESSAGE_BANTER3
				ENDIF
			ENDIF
		BREAK
		CASE DRIVE_HOME_MESSAGE_BANTER3
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_BANT6", 0, missionPeds[ped_franklin].id, "FRANKLIN", 3, missionPeds[ped_lamar].id, "LAMAR")
					eDriveHomeCurrentDialogue = DRIVE_HOME_MESSAGE_BANTER4
				ENDIF
			ENDIF
		BREAK
		CASE DRIVE_HOME_MESSAGE_BANTER4
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_EXTRA("LM2_BANT6b", 0, missionPeds[ped_franklin].id, "FRANKLIN", 3, missionPeds[ped_lamar].id, "LAMAR")
					eDriveHomeCurrentDialogue = DRIVE_HOME_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DO_DRIVE_HOME()
	BOOL bRetVal = FALSE
	
	FAIL(FAIL_LAMAR_KILLED)
	FAIL(FAIL_FRANKLIN_KILLED)
	
	HANDLE_DRIVE_HOME_TEXT()
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF

	SWITCH eDriveHomeCurrentGoal
		CASE MISSION_GOAL_01
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				PREPARE_MUSIC_EVENT("FRA2_END_VEHICLE")
			ENDIF
		
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vLeaveSawmillAreaGoal) > 50
			OR IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(locates_data, <<-71.2345, -1466.6842, 30.9867>>, <<10,10, LOCATE_SIZE_HEIGHT>>, TRUE, missionPeds[ped_lamar].id, null, null, "fran2_getCar", "fran2_drvL", "", "", "")
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TRIGGER_MUSIC_EVENT("FRA2_END_ON_FOOT")
					CANCEL_MUSIC_EVENT("FRA2_END_VEHICLE")
				ELSE
					IF PREPARE_MUSIC_EVENT("FRA2_END_VEHICLE")
						TRIGGER_MUSIC_EVENT("FRA2_END_VEHICLE")
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehicle[veh_bulldozer].id)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle[veh_bulldozer].id)
				ENDIF

				START_TIMER_NOW_SAFE(tmrDriveHomeBanterStart)
				
				eDriveHomeCurrentGoal = MISSION_GOAL_02
			ENDIF

		BREAK
		CASE MISSION_GOAL_02
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
				IF TIMER_DO_ONCE_WHEN_READY(tmrDriveHomeBanterStart, 5)
					eDriveHomeCurrentDialogue = DRIVE_HOME_MESSAGE_BANTER1
				ENDIF
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vLeaveSawmillAreaGoal) > 300
				IF DOES_ENTITY_EXIST(missionPeds[ped_michael].id)
					DELETE_PED(missionPeds[ped_michael].id)
				ENDIF
				
				IF DOES_ENTITY_EXIST(missionPeds[ped_trevor].id)
					DELETE_PED(missionPeds[ped_trevor].id)
				ENDIF
				
				INT i
				REPEAT COUNT_OF(enemyPeds) i
					IF DOES_ENTITY_EXIST(enemyPeds[i].id)
						DELETE_PED(enemyPeds[i].id)
					ENDIF
				ENDREPEAT
				
				REPEAT COUNT_OF(vehicle) i
					IF DOES_ENTITY_EXIST(vehicle[i].id)
						IF IS_ENTITY_A_MISSION_ENTITY(vehicle[i].id)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[i].id)
						ENDIF
					ENDIF
				ENDREPEAT
				
				PRE_STREAM_ASSETS_FOR_STAGE(STAGE_OUTRO_CUTSCENE)
				CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_BALLA))
				CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_BULLDOZER))
				CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_BUCCANEER))
				CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_BISON))
				CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_FELON))
				CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_HAULER))
				CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_BARREL))
				
				eDriveHomeCurrentGoal = MISSION_GOAL_03
			ENDIF
		BREAK
		CASE MISSION_GOAL_03	
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(locates_data, <<-71.2345, -1466.6842, 30.9867>>, <<10,10, LOCATE_SIZE_HEIGHT>>, TRUE, missionPeds[ped_lamar].id, null, null, "fran2_getCar", "fran2_drvL", "", "", "")
				
				KILL_ANY_CONVERSATION()
				
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(Locates_Data, TRUE)
				CLEAR_MISSION_LOCATE_STUFF(locates_data, TRUE)
				
				eDriveHomeCurrentGoal = MISSION_GOAL_04
			ENDIF
		BREAK
		CASE MISSION_GOAL_04
			bRetVal = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bRetVal
ENDFUNC
