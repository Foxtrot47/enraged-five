USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
USING "script_ped.sch"
using "commands_streaming.sch"
using "commands_camera.sch"
USING "Commands_clock.sch"
USING "Commands_interiors.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "script_blips.sch"
USING "commands_entity.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "locates_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "seamless_cuts.sch"
USING "CompletionPercentage_public.sch"
using "clearMissionArea.sch"
using "usefulcommands.sch"
using "rgeneral_include.sch"
USING "building_control_public.sch"
USING "vehicle_gen_public.sch"
USING "timelapse.sch"
USING "script_blips.sch"
USING "commands_event.sch"
USING "dialogue_public.sch"
USING "family_public.sch"
USING "player_scene_private.sch"
USING "Franklin2_support.sch"

PROC UPDATE_HANDLE_MEET_CREW_MESSAGING()

	SWITCH eMeetCrewCurrentDialogue
		CASE MEET_CREW_MESSAGE_GO_TO_SAWMILL
			IF IS_MESSAGE_BEING_DISPLAYED()
				CLEAR_PRINTS()
			ENDIF
			PRINT("FRAN2_GOSAW",DEFAULT_GOD_TEXT_TIME,1)
			mission_blip = CREATE_BLIP_FOR_COORD(vFranklinMeetCrewGoal, TRUE)
			
			eMeetCrewCurrentDialogue = MEET_CREW_MESSAGE_IDLE
		BREAK
		CASE MEET_CREW_MESSAGE_IDLE
		BREAK
	ENDSWITCH
ENDPROC


FUNC BOOL DO_LAMAR_CUTSCENE()
	BOOL bRetVal = FALSE
	
	SWITCH eSawmillSceneCurrentGoal		
		CASE MISSION_GOAL_01
			REQUEST_CUTSCENE("FRA_2_IG_4_ALT1_concat")
			REQUEST_WAYPOINT_RECORDING(GET_WAYPOINT_LABEL(FW_TREVOR))
			IF HAS_CUTSCENE_LOADED()
			AND GET_IS_WAYPOINT_RECORDING_LOADED(GET_WAYPOINT_LABEL(FW_TREVOR))
			
				IF IS_ENTITY_OK(missionPeds[ped_michael].id)
					CPRINTLN(DEBUG_MISSION, "bsw registering michael for fra2 ig4 alt1 p2")
					REGISTER_ENTITY_FOR_CUTSCENE(missionPeds[ped_michael].id, "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", missionPeds[ped_michael].id)
					
					objMichaelsWeapon = CREATE_WEAPON_OBJECT(WEAPONTYPE_HEAVYSNIPER, 100, GET_ENTITY_COORDS(missionPeds[ped_michael].id) - <<0.0, 0.0, 10.0>>, TRUE)
					IF DOES_ENTITY_EXIST(objMichaelsWeapon)
						REGISTER_ENTITY_FOR_CUTSCENE(objMichaelsWeapon, "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT)
					ENDIF
				ENDIF
				IF IS_ENTITY_OK(missionPeds[ped_trevor].id)
					REGISTER_ENTITY_FOR_CUTSCENE(missionPeds[ped_trevor].id, "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", missionPeds[ped_trevor].id)
				ENDIF
				IF IS_ENTITY_OK(missionPeds[ped_franklin].id)
					REGISTER_ENTITY_FOR_CUTSCENE(missionPeds[ped_franklin].id, "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", missionPeds[ped_franklin].id)
				ENDIF
				IF IS_ENTITY_OK(vehicle[veh_michael].id)
					REGISTER_ENTITY_FOR_CUTSCENE(vehicle[veh_michael].id, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT)
				ENDIF
				IF IS_ENTITY_OK(vehicle[veh_Trevor].id)
					REGISTER_ENTITY_FOR_CUTSCENE(vehicle[veh_Trevor].id, "Trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT)
				ENDIF
				
				DELETE_SQUAD(SQUAD_PreGuards)
			
				START_CUTSCENE()
				
				TRIGGER_MUSIC_EVENT("FRA2_START")
					
				eSawmillSceneCurrentGoal = MISSION_GOAL_02
			ENDIF
		BREAK
		CASE MISSION_GOAL_02
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
				CPRINTLN(DEBUG_MISSION, "BSW forcing ped motion state")
				
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 2000)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				IF IS_ENTITY_OK(missionPeds[ped_trevor].id)
					SET_ENTITY_COORDS(missionPeds[ped_trevor].id, vTrevorGoToPositionPosition)
					SET_ENTITY_HEADING(missionPeds[ped_trevor].id, vTrevorGoToPositionHeading)
					SET_CURRENT_PED_WEAPON(missionPeds[ped_trevor].id,GET_BEST_PED_WEAPON(missionPeds[ped_trevor].id))
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(missionPeds[ped_trevor].id, TRUE)
					
					TASK_FOLLOW_WAYPOINT_RECORDING(missionPeds[ped_trevor].id, GET_WAYPOINT_LABEL(FW_TREVOR), 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
				ENDIF
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 46000
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
			ENDIF
							
			IF HAS_CUTSCENE_FINISHED()
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				
				DELETE_OBJECT(objMichaelsWeapon)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				SET_FRONTEND_RADIO_ACTIVE(TRUE)
				
				IF NOT IS_PED_INJURED(missionPeds[ped_trevor].id)
					missionPeds[ped_trevor].aiBlip.BlipID = CREATE_BLIP_FOR_ENTITY(missionPeds[ped_trevor].id)
				ENDIF
				IF NOT IS_PED_INJURED(missionPeds[ped_michael].id)
					DELETE_PED(missionPeds[ped_michael].id)
				ENDIF
				
				eSawmillSceneCurrentGoal = MISSION_GOAL_03
				
				bRetVal = TRUE
			ENDIF
		BREAK
	ENDSWITCH
		
	RETURN bRetVal
ENDFUNC

FUNC BOOL do_meet_crew()
	BOOL bRetVal = FALSE
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	UPDATE_HANDLE_MEET_CREW_MESSAGING()
	
	FAIL(FAIL_KILL_TANISHA)
	FAIL(FAIL_COVER_BLOWN)
	
	SWITCH eMeetCrewCurrentGoal
		CASE MISSION_GOAL_01
			IF g_bMagdemoActive
				SET_UP_PLAYER(ped_franklin)
				eMeetCrewCurrentGoal = MISSION_GOAL_03
			ELSE
				eMeetCrewCurrentGoal = MISSION_GOAL_02
			ENDIF
		BREAK
		CASE MISSION_GOAL_02
			// this is where the tanisha car stuff should go if it isn't
			// handled by the intro cutscene stage
			// if you fail meet_the_crew and restart,
			// tanisha wont be there, but who cares you never see her anyway
			CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_TANISHA))
			CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_TANISHA_DRIVER))
			CLEAR_MODEL_REQUEST(ENUM_TO_INT(FM_SURGE))
			
			eMeetCrewCurrentDialogue = MEET_CREW_MESSAGE_GO_TO_SAWMILL
			eMeetCrewCurrentGoal = MISSION_GOAL_03
		BREAK
		CASE MISSION_GOAL_03
			IF CREATE_MICHAEL(STAGE_MEET_THE_CREW, TRUE)
				eMeetCrewCurrentGoal = MISSION_GOAL_04
			ENDIF
		BREAK
		CASE MISSION_GOAL_04
			IF CREATE_MICHAEL_CAR()
				eMeetCrewCurrentGoal = MISSION_GOAL_05
			ENDIF
		BREAK
		CASE MISSION_GOAL_05
			IF CREATE_TREVOR(STAGE_MEET_THE_CREW, TRUE)
				eMeetCrewCurrentGoal = MISSION_GOAL_06
			ENDIF
		BREAK
		CASE MISSION_GOAL_06
			IF CREATE_TREVOR_CAR()
				eMeetCrewCurrentGoal = MISSION_GOAL_07
			ENDIF
		BREAK
		CASE MISSION_GOAL_07
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vFranklinMeetCrewGoal) < 500	// SAFETY in case some crazy person runs all the way there (omg)
				IF NOT IS_AUDIO_SCENE_ACTIVE("FRANKLIN_2_GET_TO_SAWMILL")
					START_AUDIO_SCENE("FRANKLIN_2_GET_TO_SAWMILL")
				ENDIF
				
				eMeetCrewCurrentGoal = MISSION_GOAL_08
			ENDIF
		BREAK
		CASE MISSION_GOAL_08
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vFranklinMeetCrewGoal) < 500
				SETUP_AREA_AROUND_SAWMILL()
				
				SPAWN_ENEMY_SQUAD(SQUAD_PreGuards)
				SET_UP_SCRIPTED_SQUAD(SQUAD_PreGuards)
				
				SPAWN_SAWMILL_VEHICLES(STAGE_MEET_THE_CREW)
				
				REQUEST_CUTSCENE("FRA_2_IG_4_ALT1_concat")
				
				eMeetCrewCurrentGoal = MISSION_GOAL_09
			ENDIF
		BREAK
		CASE MISSION_GOAL_09
			FAIL(FAIL_TREVOR_KILLED)
			FAIL(FAIL_MICHAEL_KILLED)
			FAIL(FAIL_BULLDOZER_KILLED)
	
			IF g_bMagDemoActive
			AND NOT g_bMagDemoFRA2Ready
			AND NOT IS_PED_INJURED(missionPeds[ped_trevor].id)
			AND NOT IS_PED_INJURED(missionPeds[ped_michael].id)
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(missionPeds[ped_michael].id)
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(missionPeds[ped_trevor].id)
				// Magdemo had to hold the switch until all assets were set up.
				ar_ALLOW_PLAYER_SWITCH_OUTRO()
				g_bMagDemoFRA2Ready = TRUE
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vFranklinMeetCrewGoal, g_vAnyMeansLocate, TRUE)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 5.0, 4)
						IF NOT IS_SELECTOR_CAM_ACTIVE()
							PRE_STREAM_ASSETS_FOR_STAGE(STAGE_GET_INTO_POSITION)
						
							STOP_AUDIO_SCENE("FRANKLIN_2_GET_TO_SAWMILL")
							eMeetCrewCurrentGoal = MISSION_GOAL_10
						ENDIF
					ENDIF
				ELSE
					eMeetCrewCurrentGoal = MISSION_GOAL_10
				ENDIF
			ENDIF
		BREAK
		CASE MISSION_GOAL_10
			IF DO_LAMAR_CUTSCENE()
				IF DOES_BLIP_EXIST(mission_blip)
					REMOVE_BLIP(mission_blip)
				ENDIf
				
				bRetVal = TRUE
				eMeetCrewCurrentGoal = MISSION_GOAL_11
			ENDIF
		BREAK
		CASE MISSION_GOAL_11
			bRetVal = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bRetVal
ENDFUNC
