
//////////////////////////////////////////////////////////////////////
/* DRF1.sc													 		*/
/* Author: DJ Jones, Yomal Perera									*/
/* Michael's 1st session with Dr. Friedlander.						*/
//////////////////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "shrink_office_defs.sch"


FUNC SHRINK_SESSION SHRINK_GET_CURRENT_MISSION_SESSION()
	RETURN SHRINKSESSION_OFFICE_CHAOS
ENDFUNC


USING "shrink_office.sch"
