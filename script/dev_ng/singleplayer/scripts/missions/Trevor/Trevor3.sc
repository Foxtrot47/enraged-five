

// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Kevin Bolt					Date: 			 	         ___
// _________________________________________________________________________________________ 
// ___ 																					 ___
// ___ 							 						 								 ___
// ___ 				                        Trevor 3					                 ___
// ___ 								     												 ___	
// ___										         									 ___
// _________________________________________________________________________________________
// _________________________________________________________________________________________

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// ____________________________________ INCLUDES ___________________________________________

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_path.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
USING "script_ped.sch"
using "commands_streaming.sch"
using "commands_camera.sch"
USING "Commands_clock.sch"
USING "Commands_interiors.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "script_blips.sch"
USING "commands_entity.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "locates_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "seamless_cuts.sch"
USING "CompletionPercentage_public.sch"
using "clearMissionArea.sch"
using "usefulcommands.sch"
using "rgeneral_include.sch"
USING "building_control_public.sch"
USING "vehicle_gen_public.sch"
USING "timelapse.sch"
USING "script_blips.sch"
USING "commands_event.sch"
USING "dialogue_public.sch"
USING "family_public.sch"
USING "scripted_los_checks.sch"
USING "commands_recording.sch"

STRING remlastConv

USING "ped_player_reactions.sch"
USING "taxi_functions.sch"
USING "commands_Water.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	CONST_INT MAX_SKIP_MENU_LENGTH 12	
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      

#ENDIF

CONST_INT MAX_CONDITIONS 34
CONST_INT MAX_DIALOGUES 19
CONST_INT MAX_INSTRUCTIONS 5
CONST_INT MAX_ACTIONS 33

bool bPressedSkip
bool bConvoPausedOutVeh
bool bConvoPausedWanted

STREAMVOL_ID endFrustum
//NEW MISSION CONTROLLER
ENUM enumActions
	ACT_NULL,
	//intro cut
	ACT_LOAD_INTRO_CUT,
	ACT_PLAY_CUTSCENE,
	ACT_RESOLVE_VEHICLES_AT_START,

	ACT_EXTRACT_WADE_FROM_CUTSCENE,		
	
	//drive to trailer park
	ACT_WADE_GETS_IN_BODHI,
	ACT_UPDATE_PLAYER_VEHICLE,
	ACT_MAKE_WADE_AND_PLAYER_ENTER_SPECIFIC_SEAT,
	ACT_MAKE_IT_RAIN,
	ACT_PREP_TRAILER_PARK,
	ACT_RELEASE_TRAILER_PARK,
	ACT_SET_PED_DENSITY_EVERY_FRAME,
	ACT_AUDIO_SCENE_GETTING_IN_CAR,
	ACT_LIGHTNING_FLASH,
	ACT_KILL_BANTER,
	ACT_MECHANIC,
	ACT_AUDIO_FOR_EXPLOSIONS,
	ACT_MAKE_PLAYER_USE_DRIVER_DOOR,
	ACT_TRAILER,
	ACT_BIKERS_DRIVE_OFF,
	ACT_BIKERS_SITTING_CHATTING,
	ACT_BIKER_FIGHT,
	ACT_COFFIN_CRY,
	ACT_COFFIN_RESTART_ANIM,
	ACT_RECORD_CAMERA,
	ACT_REWARD_BOMB_PLANTING_UNOBSERVED,
	ACT_WADE_RUNS_OUT_OF_TRAILER_PARK, 
	ACT_REWARD_BLOW_ALL_AT_ONCE,
	ACT_REWARD_STICKY_BOMB_USAGE,
	ACT_WADE_LOOKS_AT_TREVOR_NEAR_CAR,
	ACT_DISABLE_STICKY_BOMB_TRIGGER,
	ACT_AUDIO_SCENE_PLANT_STICKY_BOMBS,
	ACT_LOAD_AUDIO_STREAM,
	ACT_DELAY_ENEMY_SHOOTING,
	ACT_FRUSTUM_SETUP_FOR_FRIST_CUT,
	ACT_UPDATE_SRL_POSITION,
	ACT_SLOW_DOWN_PLAYER_IN_CHOPPER,
	
	
	//trailer park cutscene
	ACT_CUTSCENE_CAM,
	ACT_DISABLE_PLAYER_CONTROL,
	ACT_CUTSCENE_LIGHTNING_FLASH,
	
	//plant bombs
	ACT_SET_BOMB_PLANTING_STUFF,
	ACT_LOST_ALERTED_RESPONSE,
	ACT_PED_REACTS_TO_STICKY_BOMB,
	ACT_UPDATE_AI_BLIPS,
	ACT_UNFIX_BIKES_FROM_SYNC_SCENES,
	ACT_MECHANIC_ANIM_RESETS_AFTER_INTERUPTION,
	ACT_BEAT_UP_GUY_REACTS_TO_TREVOR,
	ACT_FIND_ANIM_TO_TRIGGER,
	ACT_BEAT_UP_ANIMATION_PLAYS_OUT,
	ACT_BIKERS_DRIVE_OFF_ANIM_PLAYS_OUT,
	ACT_TRAILER_ANIM_PLAYS_OUT,
	ACT_COFFIN_GUY_DROPS_BOTTLE,
	ACT_DETACH_COFFIN,
	ACT_TRAILER_RAYFIRES,
	ACT_REMOVE_TRAILER_BLIPS,
	ACT_ENSURE_ONE_PED_ENGAGING_PLAYER,
	ACT_CONTROL_MUSIC,
	ACT_STOP_TRUCK_FROM_BURNING,
	ACT_FOUGHT_GUY_FELL_AND_NOW_WONT_REACT,
	ACT_STOP_TRAILER_WOMAN_GETTING_CUT_OFF,
	ACT_UPDATE_TRAILER_GUY_BLIP,
	ACT_SET_LOST_ALERTED_STUFF,
	ACT_BIKER_REACTIONS,
	ACT_SEATED_BIKER_REACTIONS,
	ACT_STOP_BOMBS_ON_TRAILER_DOOR_BEING_REMOVED_AT_RANGE,
	
	//explosion cutscene
	ACT_EXPLOSION_CAMS,
	ACT_TIMED_EXPLOSIONS,
	
	//drive to LS
	ACT_CLEAR_WEATHER,
	ACT_CLEAN_UP_TRAILER_PARK,
	ACT_SET_VEHICLE_RADIO,
	ACT_IN_CAR_SHOVE_ANIM,
	ACT_LOAD_CUTSCENE,
	ACT_STOP_VEHICLE,
	ACT_STOP_VEHICLE_PLAYER_EXIT,
	ACT_LS_VIEW_LEADIN,
	ACT_PLAY_LS_CUTSCENE,
	ACT_LOAD_WADE_COUSIN_CUTSCENE,
	ACT_MAKE_PLAYER_AND_BUDDY_WALK,
	ACT_STOP_VEHICLE_DOOR_GETTING_KNOCKED_OFF,
	ACT_PREVENT_WADE_LEAVING_BURNING_VEHICLE,
	ACT_ESTABLISHING_SHOT_AT_WADES,
	ACT_WADE_PLAYS_SHOUT_ANIM,
	ACT_WADE_ENTERS_PLAYERS_GROUP,
	ACT_BLOCK_PLAYER_FOR_LEAD_IN,
	ACT_MUSIC_IN_VEHICLE_GOING_TO_LS,
	ACT_AUDIO_SCENE_DRIVE_TO_LS,
	ACT_AUDIO_SCENE_DRIVE_TO_WADES_COUSIN,
	ACT_PREP_FRUSTUM_AT_WADES_COUSIN,
	ACT_SLOW_DOWN_PLAYER,
	ACT_FLEEING_PEDS,
	
	
	//final cutscne
	ACT_DO_END_CUTSCENE,
	
	ACT_SAFEHOUSE_INTRO_SHOT,
	ACT_KEEP_INTERIOR_IN_MEMORY,
	ACT_BED_SHOT,
	ACT_ACTIVITIES_SHOT,
	ACT_WARDROBE_SHOT,
	ACT_SKIP_CUTS
	
ENDENUM

ENUM enumInstructions
	INS_NULL,
	INS_GET_TO_TRAILER_PARK,	
	
	//ttrailer park
	ins_detonate_bombs,
	ins_stealth_instructions,
	ins_destroy_trailers,
	ins_plant_sticky_bombs,
	ins_get_back_to_lost,
	ins_trigger_sticky_bombs,
	
	//drive to LS
	INS_GET_TO_LOS_SANTOS,
	INS_GO_TO_WADES_COUSIN,
	INS_FOLLOW_WADE
ENDENUM

ENUM enumconditions
	COND_NULL,						//0
	// ===== CUTSCENES ========
	COND_START_CUTSCENE,
	COND_CUTSCENE_STARTED_PLAYING,
	COND_CUTSCENE_FINISHED,
	COND_END_CUTSCENE,
	// === START DRIVE STAGE ===
	COND_START_TRIALER_PARK_DRIVE,
	COND_PLAYER_IN_TRIGGER_RANGE_OF_TRAILER_PARK,
	COND_PLAYER_WITHIN_300M_OF_TRAILER_PARK,
	COND_PLAYER_WITHIN_500M_OF_TRAILER_PARK,
	COND_TRAILER_PARK_ASSETS_LOADED,
	COND_ARRIVE_AT_TRAILER_PARK,
	COND_HIT_TRIGGER_ZONE,
	COND_LIGHTNING_FLASH,
	COND_IN_TAXI_RIDE,
	COND_PLAYER_TOO_CLOSE_TO_TRAILER_PARK,
	COND_TREVOR_ARRIVAL_DIALOGUE_FINISHED,	
	COND_DRIVE_TO_LS_START, //from here is duplicated later on in the mission for LS drive	
	COND_WADE_IN_PLAYER_GROUP,
	COND_PLAYER_AND_WADE_IN_SAFE_CAR,
	COND_WADE_NEARBY,
	COND_WANTED,
	COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE,
	COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM,
	COND_WADE_WANTS_TO_COMMENT_ON_WANTED_LEVEL,
	COND_BANTER_STATUS_SAVED,
	COND_RAINING,
	COND_END_TRIALER_PARK_DRIVE,
	//drive to LS...continued from above	
	COND_PLAYER_LEFT_TRAILER_PARK,
	COND_PLAYER_300M_FROM_TRAILER_PARK,
	COND_BACK_WITH_WADE,
	COND_WADE_AND_PLAYER_IN_VEHICLE,
	COND_PLAYER_300M_FROM_CUTSCENE,
	COND_LS_VIEW_TRIGGER,
	COND_PLAYER_350M_FROM_CUTSCENE,
	COND_CAN_STOP_PLAYER_VEHICLE,
	COND_VEHICLE_STOPPED_OR_ON_FOOT,
	COND_CUTSCENE_PLAYING,
	COND_LS_CUTSCENE_FINISHED,
	COND_NO_INTERRUPTIONS,
	COND_PLAYER_50M_FROM_WADES_COUSIN,
	COND_PLAYER_600M_FROM_WADES_COUSIN,
	COND_AT_WADES_COUSIN,
	COND_ESTABLISHING_SHOT_DONE,
	COND_TREVOR_APPROACHES,
	COND_TREVOR_AT_TOP_OF_FLLOYD_STAIRS,
	COND_STICK_JAMMED_LINE_PLAYED,
	COND_PLAYER_TRIG_IN_CAR,
	COND_LS_LEADIN_OK,
	COND_DRIVE_TO_LS_END,


	//trailer cutscene
	COND_TRAILER_CUT_START,
	COND_ARRIVED_IN_VEHICLE,
	COND_TRAILER_CUT_END,
	
	//plant sticky bombs
	COND_PLANT_BOMBS_START,
	COND_PLAYER_SPOTTED_BY_LOST,
	COND_BEAT_UP_GUY_REACT_CONDITIONS,
	COND_A_PED_IS_REACTING,
	COND_BEAT_UP_GUY_SEES_TREVOR,
	COND_LOST_SPOTTED_STICKY_BOMB,
	COND_BEAT_HIM_UP_DIALOGUE_STARTS,
	COND_FIXER_ANIM_FINISHED,
	COND_TRAILER_ANIM_FINISHED,
	COND_BIKER_DRIVE_ANIM_FINISHED,
	COND_BIKER_CHATTING_ANIM_FINISHED,
	COND_BEATING_UP_ANIM_FINISHED,
	COND_COFFIN_ANIM_FINISHED,
	COND_ENEMIES_STILL_ALIVE,
	COND_THREE_BOMBS_PLANTED,
	COND_ALL_BOMBS_PLANTED,
	COND_ALL_WILL_BLOW_AT_ONCE,
	COND_TRIGGER_BUTTON_PRESSED,
	COND_BOMB_JUST_PLANTED,
	COND_FIRST_BOMB_PLANTED,
	COND_ALL_TRAILERS_DESTROYED,
	COND_A_TRAILER_HAS_BLOWN,
	COND_PLAYER_IN_A_VEHICLE,
	COND_LOS_QUAD_CHECKS,
	COND_FOUGHT_GUY_FELL,
	COND_THREE_SECONDS_SINCE_LAST_BOMB,
	COND_PLAYER_AT_WADE,
	COND_PLAYER_LEAVING_AREA,
	COND_PLAYER_LEFT_AREA,
	COND_RETURNED_TO_WADE,
	COND_PLANT_BOMBS_END
	
	
ENDENUM

enum enumDialogue
	DIA_NULL,
	DIA_INTRO_LINE,
	DIA_BIGGER_CAR,
	DIA_TRAILER_PARK_DRIVE_CHAT,
	DIA_WADE_COMMENTS_ON_PLAYER_LEAVING,
	DIA_WADE_COMMENTS_ON_COPS,
	DIA_WADE_COMMENTS_ON_CAR_DAMAGE,
	DIA_ARRIVE_AT_TRAILER_PARK,
	DIA_FIRST_CAR_COMMENT,

	
	//bomb planting
	DIA_ITS_TREVOR,
	DIA_SEES_STICKY_BOMB,
	DIA_ALERT_ALL_LOST_FOLLOWUP,
	DIA_BEAT_UP_BKIER_SEES_TREVOR,
	DIA_TRAILER_BLOWN_UNSEEN,
	DIA_FIXER_DIALOGUE,
	DIA_TRAILER,
	DIA_BIKER_DRIVE_OFF,
	DIA_BIKERS_CHAT,
	DIA_BEAT_UP_GUY,
	DIA_COFFIN_GUY,
	DIA_TREVOR_SAYS_STAY_HERE,
	DIA_COMBAT_BANTER,
	DIA_PLANTING_BOMBS,
	DIA_TREVOR_GIVEN_GUNS,
	DIA_TRAILER_BLOWN,
	DIA_BACK_AT_WADE_COMBAT,
	DIA_BACK_AT_WADE_COMBAT_IN_CAR,
	DIA_BACK_AT_WADE_IN_CAR_NO_COMBAT,
	DIA_PLAYER_GETS_STEALTH_KILL,
	DIA_BACK_AT_WADE_NO_COMBAT,
	
	//drive to LS
	DIA_LETS_GO,
	DIA_DRIVE_TO_LS_BANTER,
	DIA_DRIVE_TO_WADES_BANTER,
	DIA_WADE_AT_TOP_OF_STAIRS,
	DIA_WADE_GIVES_DIRECTIONS,
	DIA_WADE_CLIMBS_STAIRS,
	DIA_WADE_CALLS_DOWN_TO_TREVOR,
	DIA_OVERLOOKING_LS,
	DIA_TREVOR_STOPS_BANTER
ENDENUM

enum enumFails
	FAIL_NULL,
	fail_lead_cops_to_trailer_park,
	FAIL_WADE_KILLED,
	FAIL_LEFT_WADE_BEHIND,
	FAIL_LOST_ATTACKED,
	FAIL_KILL_LOST,
	FAIL_NO_MORE_STICKY_BOMBS,
	FAIL_NOT_ENOUGH_STICKY_BOMBS,
	FAIL_TRAILER_PARK_ABANDONED
endenum

enum eArriveComment
	AR_NOTSET,
	AR_WAITNIGHT,
	AR_ISNIGHT
endenum

eArriveComment arriveComment
INT iWhooshSound

struct structDialogue
	enumDialogue dial = DIA_NULL
	bool bCompleted
	bool bStarted
	int flag
	int intA,intB
	#if IS_DEBUG_BUILD bool condTrue #endif
endstruct

structDialogue dia[MAX_DIALOGUES]

struct structInstructions
	enumInstructions ins = INS_NULL
	bool bCompleted
	int flag
	int intA
	#if IS_DEBUG_BUILD bool CONDTRUE #endif
endstruct

structInstructions instr[MAX_INSTRUCTIONS]

ENUM enumACTIONplayout
	PLAYOUT_ON_TRIGGER,
	PLAYOUT_ON_COND,
	SKIP_SET_TO_COMPLETE
endenum

STRUCT structactions
	enumActions action = ACT_NULL
	bool active
	bool ongoing
	bool completed
	bool needsCleanup
	bool trackCondition
	int flag
	int intA,intB
	float floatA
ENDSTRUCT

structactions actions[MAX_ACTIONS]

enum andOrEnum
	cFORCEtrue,
	cIGNORE,
	cIF,
	cIFnot,
	cAND,
	cAndNOT,
	cOR,
	cWasTrueNowFalse,
	cORbracket
endenum
bool conditionsTrue
bool bracketOpen

struct structConditions
	enumconditions condition = COND_NULL
	bool active
	bool returns
	bool wasTrue
	int flag
	int intA
endstruct

structConditions conditions[MAX_CONDITIONS]

bool bRemovedPlayerHelmet
bool bShoutingAudioPlaying

//struct for fleeing peds

enum enumPanicAction
	panic_point_at_trailer1,
	panic_point_at_trailer2,
	panic_point_at_trailer3,
	panic_point_at_trailer4,
	panic_flee,
	panic_run_and_cower,
	panic_drive_off_bike,
	panic_drive_off_quad
endenum

struct structfleeingPeds
	ped_index pedID
	vehicle_index vehID
	vector spawnAt
	vector target
	enumPanicAction panicAction
ENDSTRUCT

structfleeingPeds fleeingPed[10]
bool bShowCutscene
// ==================================================== Mission Stages & progress ==================================

ENUM enumMissionStage
	STAGE_STARTUP,						//0
	STAGE_INTRO_CUT,					//1
	STAGE_GET_TO_TRAILER_PARK,			//2
	STAGE_TRAILER_PARK_CUT,				//3
	STAGE_DETROY_TRAILER_PARK,			//4
	STAGE_DESTROYED_CUTSCENE,			//5
	STAGE_GET_BACK_TO_WADE,				//6
	STAGE_DRIVE_TO_LS,					//7
	STAGE_LS_VIEW_CUT,					//8
	STAGE_DRIVE_TO_WADES_COUSIN,		//9
	STAGE_FINAL_CUT,					//10
	STAGE_SAFEHOUSE_SHOTS,				//11
	STAGE_GAME_OVER,					//12
	STAGE_CLEANUP,						//13
	STAGE_NULL							//14
ENDENUM

enumMissionStage missionProgress

enum enumEndState
	END_STATE_PASSED,
	END_STATE_FORCE_CLEANUP,
	END_STATE_FAILED,
	END_STATE_STAGE_SKIP,
	END_STATE_MISSION_ONGOING
endenum

CONST_INT CLEANUP 99
int iMechanicArray

// === directions
float fLastDistToJunc
bool bNewDirections
VEHICLE_PATH_DIRECTIONS directionsLastFrame = DIRECTIONS_UNKNOWN

// =================================================== ENTITIES ===========================================================
CONST_INT MAX_PEDS 11
CONST_INT MAX_VEHICLES 10
CONST_INT MAX_OBJECTS 4

//peds
CONST_INT ped_coffins 0
CONST_INT ped_fixer 1
CONST_INT ped_nookie 2
CONST_INT ped_driveOffA 3
CONST_INT ped_driveOffB 4
CONST_INT ped_fighterA 5
CONST_INT ped_fighterB 6
CONST_INT ped_fought 7
CONST_INT ped_chattingA 8
CONST_INT ped_chattingB 9
CONST_INT ped_wade 10

//vehicles
CONST_INT veh_player 0
CONST_INT veh_coffin 1
CONST_INT veh_driveOffA 2
CONST_INT veh_driveOffB 3
CONST_INT veh_chatA 4
CONST_INT veh_chatB 5
CONST_INT veh_ambientA 6
CONST_INT veh_ambientB 7
CONST_INT veh_broken 8
CONST_INT veh_lastPlayer 9



//objects
CONST_INT obj_coffinA 0
CONST_INT obj_coffinB 1
CONST_INT obj_coffinC 2
CONST_INT obj_beer 3

int iCamZone
object_index prop_pipe
struct StructPed
	ped_index id
	AI_BLIP_STRUCT aiBlip
	bool registeredKill
ENDSTRUCT

struct structVehicle
	vehicle_index id
ENDSTRUCT

struct structObject
	object_index id
ENDSTRUCT

VEHICLE_PATH_DIRECTIONS preloadDirections
vehicle_index vehiclearray[25]
vehicle_index cutsceneChopper
ped_index chopperPed
StructPed ped[MAX_PEDS]
structVehicle vehicle[MAX_VEHICLES]
structObject obj[MAX_OBJECTS]

int iCamSync
//float  fLastRange
//ped reaction stuff
structCheckState pedReactionState[MAX_PEDS]
diaData diAlertAllSeen, diAlertAllUnseen, diSeePlayer, diHearPlayer, diHearPlayerAgain, diLostPlayer, diBullet, diDeadBody, diExplosion, diFoundPlayer
vector vDefAreas[44]

streamvol_id trailer_frustum

REL_GROUP_HASH relPreCombatEnemy[MAX_PEDS]
REL_GROUP_HASH relGroupDislike
REL_GROUP_HASH relGroupEnemy
REL_GROUP_HASH RELGROUPHASH_NEUTRAL_TO_PLAYER
REL_GROUP_HASH relGroupFoughtBiker

FLOAT SEEING_RANGE = 20
FLOAT HEARING_RANGE = 120

// ==== sync scenes
int iTrailerAnim,iCoffinAnim
int iBikersDriveOff, iBikersDriveOffB, iBikersDriveOffc
// ====================================== Trailers Rayfire =======================================
enum enumState
	init,
	resetForSkip,
	PRIMING,
	resetAndGo,
	inactive,
	go,
	trigger,
	playing,
	skip_to_blown,
	endNow
ENDENUM

STRUCT structRayfire
	enumState state = init
	RAYFIRE_INDEX index
	int explosionTime
	string rayfireName
	string audioName
	BUILDING_NAME_ENUM buildingName
	BUILDING_NAME_ENUM occlusionName
	vector vLocates[2]
	float fLength
	PTFX_ID fx
	BLIP_INDEX blip
ENDSTRUCT

bool bTrailerBlown
structRayfire rayfireTrailer[5]


// =================================================== Timelapse ===================================================
structTimelapse sTimelapse

// ========================================================== Conversatins ================================================
structPedsForConversation MyLocalPedStruct
string convBlock = "TRV3AUD"
int iConvoAttempts
int iConvoLastAttmpetTime


struct convStoreStruct
	int speakerNo
	ped_index pedIndex
	string speakerLabel
endstruct
convStoreStruct convStore[4]

bool bLastConvoWithoutSubtitles

TEXT_LABEL_23 restartLine//,restartRoot
TEXT_LABEL_23 restartSubtitleLine,restartSubtitleRoot
int iTrevorCount
int iBiker1Count
int iBiker2Count
int iBiker3Count

//========== cameras and cutscenes ==============
camera_index script_cam

// ===================================================Othjer variables =============================================================
LOCATES_HEADER_DATA locates_data
blip_index mission_blip
int iTemp
int i

vector vSeenStickyBomb

INTERIOR_INSTANCE_INDEX int_wade

int iTRack1,iTRack2,iTRack3
bool bTracksMade


object_index trackObj
vector vStartPan
vector vEndPan

// =============== debug =============

//widget debug
#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID trev3_widget 

	bool bScreenConditions,bScreenActionsA,bScreenActionsB,bscreenDialogue,bScreenInstructions
	bool bSkipJourney,bBlowUpTrailers,bPlantStickyBombs,bMakeBang
	bool bAllowShitskip
	//bool bTempCrashFix
#ENDIF

PROC SAFEWAIT(int waittime=0)
	#if IS_DEBUG_BUILD
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		TERMINATE_THIS_THREAD()
	ENDIF
	#endif
	WAIT(waittime)
ENDPROC

FUNC BOOL IS_VEHICLE_SAFE(vehicle_index aVeh)
	IF DOES_ENTITY_EXIST(aVeh)
		IF IS_VEHICLE_DRIVEABLE(aVeh)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_FUCKED(ped_index thisPed)
	IF IS_PED_INJURED(thisPed)
	OR IS_PED_DEAD_OR_DYING(thisPed)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// ===================================== music ========================================

// music
bool bNewMusicRequest,bRequiresPreload,bPreloadRequest
string sMusicToPlay,sPreloadMusic

proc playMusic(string musicToPlay, bool bPreload, string musicToLoadNext)
	bNewMusicRequest = TRUE
	bRequiresPreload = bPreload
	sMusicToPlay = musictoPlay
	sPreloadMusic = musicToLoadNext
endproc

PROC control_music()

	If bNewMusicRequest
		If bRequiresPreload
			IF PREPARE_MUSIC_EVENT(sMusicToPlay)
				IF TRIGGER_MUSIC_EVENT(sMusicToPlay)
					//cprintln(DEBUG_TREVOR3,"triggered music: ",sMusicToPlay)
					bNewMusicRequest = FALSE
					bRequiresPreload = FALSE
				ENDIF
			ENDIf
		ELSE
			IF TRIGGER_MUSIC_EVENT(sMusicToPlay)
				//cprintln(DEBUG_TREVOR3,"triggered music: ",sMusicToPlay)
				bNewMusicRequest = FALSE
			ENDIF
		ENDIF
	ELSE
		If bPreloadRequest
			IF NOT IS_STRING_NULL_OR_EMPTY(sPreloadMusic)
				IF PREPARE_MUSIC_EVENT(sPreloadMusic)
					bPreloadRequest=FALSE
				ENDIf
			ENDIF
		ENDIF
	ENDIF
endproc




PROC KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(int iBlah=0)
	iBlah=iBlah
	#if is_debug_build
	//cprintln(debug_trevor3,"Kill convo : ",iBlah)
	#endif
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
ENDPROC

// =================================================== other ====================================================
#if is_Debug_build

FUNC STRING GET_DIALOGUE_STRING(enumDialogue thisDia)
		SWITCH thisDia		 
			case	DIA_NULL	RETURN "	DIA_NULL	" BREAK
			case	DIA_INTRO_LINE	RETURN "	DIA_INTRO_LINE	" BREAK
			case	DIA_BIGGER_CAR	RETURN "	DIA_BIGGER_CAR	" BREAK
			case	DIA_TRAILER_PARK_DRIVE_CHAT	RETURN "	DIA_TRAILER_PARK_DRIVE_CHAT	" BREAK
			case	DIA_WADE_COMMENTS_ON_PLAYER_LEAVING	RETURN "	DIA_WADE_COMMENTS_ON_PLAYER_LEAVING	" BREAK
			case	DIA_WADE_COMMENTS_ON_COPS	RETURN "	DIA_WADE_COMMENTS_ON_COPS	" BREAK
			case	DIA_WADE_COMMENTS_ON_CAR_DAMAGE	RETURN "	DIA_WADE_COMMENTS_ON_CAR_DAMAGE	" BREAK
			case	DIA_ARRIVE_AT_TRAILER_PARK	RETURN "	DIA_ARRIVE_AT_TRAILER_PARK	" BREAK
			case	DIA_FIRST_CAR_COMMENT	RETURN "	DIA_FIRST_CAR_COMMENT	" BREAK
			case	DIA_ITS_TREVOR	RETURN "	DIA_ITS_TREVOR	" BREAK
			case	DIA_SEES_STICKY_BOMB	RETURN "	DIA_SEES_STICKY_BOMB	" BREAK
			case	DIA_ALERT_ALL_LOST_FOLLOWUP	RETURN "	DIA_ALERT_ALL_LOST_FOLLOWUP	" BREAK
			case	DIA_BEAT_UP_BKIER_SEES_TREVOR	RETURN "	DIA_BEAT_UP_BKIER_SEES_TREVOR	" BREAK
			case	DIA_TRAILER_BLOWN_UNSEEN	RETURN "	DIA_TRAILER_BLOWN_UNSEEN	" BREAK
			case	DIA_FIXER_DIALOGUE	RETURN "	DIA_FIXER_DIALOGUE	" BREAK
			case	DIA_TRAILER	RETURN "	DIA_TRAILER	" BREAK
			case	DIA_BIKER_DRIVE_OFF	RETURN "	DIA_BIKER_DRIVE_OFF	" BREAK
			case	DIA_BIKERS_CHAT	RETURN "	DIA_BIKERS_CHAT	" BREAK
			case	DIA_BEAT_UP_GUY	RETURN "	DIA_BEAT_UP_GUY	" BREAK
			case	DIA_COFFIN_GUY	RETURN "	DIA_COFFIN_GUY	" BREAK
			case	DIA_TREVOR_SAYS_STAY_HERE	RETURN "	DIA_TREVOR_SAYS_STAY_HERE	" BREAK
			case	DIA_COMBAT_BANTER	RETURN "	DIA_COMBAT_BANTER	" BREAK
			case	DIA_PLANTING_BOMBS	RETURN "	DIA_PLANTING_BOMBS	" BREAK
			case	DIA_TREVOR_GIVEN_GUNS	RETURN "	DIA_TREVOR_GIVEN_GUNS	" BREAK
			case	DIA_TRAILER_BLOWN	RETURN "	DIA_TRAILER_BLOWN	" BREAK
			case	DIA_BACK_AT_WADE_COMBAT	RETURN "	DIA_BACK_AT_WADE_COMBAT	" BREAK
			case	DIA_BACK_AT_WADE_COMBAT_IN_CAR	RETURN "	DIA_BACK_AT_WADE_COMBAT_IN_CAR	" BREAK
			case	DIA_BACK_AT_WADE_IN_CAR_NO_COMBAT	RETURN "	DIA_BACK_AT_WADE_IN_CAR_NO_COMBAT	" BREAK
			case	DIA_PLAYER_GETS_STEALTH_KILL	RETURN "	DIA_PLAYER_GETS_STEALTH_KILL	" BREAK
			case	DIA_BACK_AT_WADE_NO_COMBAT	RETURN "	DIA_BACK_AT_WADE_NO_COMBAT	" BREAK
			case	DIA_LETS_GO	RETURN "	DIA_LETS_GO	" BREAK
			case	DIA_DRIVE_TO_LS_BANTER	RETURN "	DIA_DRIVE_TO_LS_BANTER	" BREAK
			case	DIA_DRIVE_TO_WADES_BANTER	RETURN "	DIA_DRIVE_TO_WADES_BANTER	" BREAK
			case	DIA_WADE_AT_TOP_OF_STAIRS	RETURN "	DIA_WADE_AT_TOP_OF_STAIRS	" BREAK
			case	DIA_WADE_GIVES_DIRECTIONS	RETURN "	DIA_WADE_GIVES_DIRECTIONS	" BREAK
			case	DIA_WADE_CLIMBS_STAIRS	RETURN "	DIA_WADE_CLIMBS_STAIRS	" BREAK
			case	DIA_WADE_CALLS_DOWN_TO_TREVOR	RETURN "	DIA_WADE_CALLS_DOWN_TO_TREVOR	" BREAK
			case	DIA_OVERLOOKING_LS	RETURN "	DIA_OVERLOOKING_LS	" BREAK
			case	DIA_TREVOR_STOPS_BANTER	RETURN "	DIA_TREVOR_STOPS_BANTER	" BREAK



			DEFAULT RETURN " -NOT DEFINED-" BREAK
		ENDSWITCH
		RETURN "UNKNOWN"
ENDFUNC

FUNC STRING GET_CONDITION_STRING(enumconditions thisCond)
	string sCond
		SWITCH thisCond
			case	COND_NULL	sCond="	COND_NULL	" BREAK
			case	COND_START_CUTSCENE	sCond="	COND_START_CUTSCENE	" BREAK
			case	COND_CUTSCENE_STARTED_PLAYING	sCond="	COND_CUTSCENE_STARTED_PLAYING	" BREAK
			case	COND_CUTSCENE_FINISHED	sCond="	COND_CUTSCENE_FINISHED	" BREAK
			case	COND_END_CUTSCENE	sCond="	COND_END_CUTSCENE	" BREAK
			case	COND_START_TRIALER_PARK_DRIVE	sCond="	COND_START_TRIALER_PARK_DRIVE	" BREAK
			case	COND_PLAYER_IN_TRIGGER_RANGE_OF_TRAILER_PARK	sCond="	COND_PLAYER_IN_TRIGGER_RANGE_OF_TRAILER_PARK	" BREAK
			case	COND_PLAYER_WITHIN_300M_OF_TRAILER_PARK	sCond="	COND_PLAYER_WITHIN_300M_OF_TRAILER_PARK	" BREAK
			case	COND_TRAILER_PARK_ASSETS_LOADED	sCond="	COND_TRAILER_PARK_ASSETS_LOADED	" BREAK
			case	COND_ARRIVE_AT_TRAILER_PARK	sCond="	COND_ARRIVE_AT_TRAILER_PARK	" BREAK
			case	COND_HIT_TRIGGER_ZONE	sCond="	COND_HIT_TRIGGER_ZONE	" BREAK
			case	COND_LIGHTNING_FLASH	sCond="	COND_LIGHTNING_FLASH	" BREAK
			case	COND_TREVOR_ARRIVAL_DIALOGUE_FINISHED	sCond="	COND_TREVOR_ARRIVAL_DIALOGUE_FINISHED	" BREAK
			case	COND_DRIVE_TO_LS_START	sCond="	COND_DRIVE_TO_LS_START	" BREAK
			case	COND_WADE_IN_PLAYER_GROUP	sCond="	COND_WADE_IN_PLAYER_GROUP	" BREAK
			case	COND_PLAYER_AND_WADE_IN_SAFE_CAR	sCond="	COND_PLAYER_AND_WADE_IN_SAFE_CAR	" BREAK
			case	COND_WADE_NEARBY	sCond="	COND_WADE_NEARBY	" BREAK
			case	COND_WANTED	sCond="	COND_WANTED	" BREAK
			case	COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE	sCond="	COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE	" BREAK
			case	COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM	sCond="	COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM	" BREAK
			case	COND_WADE_WANTS_TO_COMMENT_ON_WANTED_LEVEL	sCond="	COND_WADE_WANTS_TO_COMMENT_ON_WANTED_LEVEL	" BREAK
			case	COND_BANTER_STATUS_SAVED	sCond="	COND_BANTER_STATUS_SAVED	" BREAK
			case	COND_RAINING	sCond="	COND_RAINING	" BREAK
			case	COND_END_TRIALER_PARK_DRIVE	sCond="	COND_END_TRIALER_PARK_DRIVE	" BREAK
			case	COND_PLAYER_LEFT_TRAILER_PARK	sCond="	COND_PLAYER_LEFT_TRAILER_PARK	" BREAK
			case	COND_PLAYER_300M_FROM_TRAILER_PARK	sCond="	COND_PLAYER_300M_FROM_TRAILER_PARK	" BREAK
			case	COND_BACK_WITH_WADE	sCond="	COND_BACK_WITH_WADE	" BREAK
			case	COND_WADE_AND_PLAYER_IN_VEHICLE	sCond="	COND_WADE_AND_PLAYER_IN_VEHICLE	" BREAK
			case	COND_PLAYER_300M_FROM_CUTSCENE	sCond="	COND_PLAYER_300M_FROM_CUTSCENE	" BREAK
			case	COND_LS_VIEW_TRIGGER	sCond="	COND_LS_VIEW_TRIGGER	" BREAK
			case	COND_PLAYER_350M_FROM_CUTSCENE	sCond="	COND_PLAYER_350M_FROM_CUTSCENE	" BREAK
			case	COND_CAN_STOP_PLAYER_VEHICLE	sCond="	COND_CAN_STOP_PLAYER_VEHICLE	" BREAK
			case	COND_VEHICLE_STOPPED_OR_ON_FOOT	sCond="	COND_VEHICLE_STOPPED_OR_ON_FOOT	" BREAK
			case	COND_CUTSCENE_PLAYING	sCond="	COND_CUTSCENE_PLAYING	" BREAK
			case	COND_LS_CUTSCENE_FINISHED	sCond="	COND_LS_CUTSCENE_FINISHED	" BREAK
			case	COND_NO_INTERRUPTIONS	sCond="	COND_NO_INTERRUPTIONS	" BREAK
			case	COND_PLAYER_50M_FROM_WADES_COUSIN	sCond="	COND_PLAYER_50M_FROM_WADES_COUSIN	" BREAK
			case	COND_PLAYER_600M_FROM_WADES_COUSIN	sCond="	COND_PLAYER_600M_FROM_WADES_COUSIN	" BREAK
			case	COND_AT_WADES_COUSIN	sCond="	COND_AT_WADES_COUSIN	" BREAK
			case	COND_ESTABLISHING_SHOT_DONE	sCond="	COND_ESTABLISHING_SHOT_DONE	" BREAK
			case	COND_TREVOR_APPROACHES	sCond="	COND_TREVOR_APPROACHES	" BREAK
			case	COND_TREVOR_AT_TOP_OF_FLLOYD_STAIRS	sCond="	COND_TREVOR_AT_TOP_OF_FLLOYD_STAIRS	" BREAK
			case	COND_DRIVE_TO_LS_END	sCond="	COND_DRIVE_TO_LS_END	" BREAK
			case	COND_TRAILER_CUT_START	sCond="	COND_TRAILER_CUT_START	" BREAK
			case	COND_ARRIVED_IN_VEHICLE	sCond="	COND_ARRIVED_IN_VEHICLE	" BREAK
			case	COND_TRAILER_CUT_END	sCond="	COND_TRAILER_CUT_END	" BREAK
			case	COND_PLANT_BOMBS_START	sCond="	COND_PLANT_BOMBS_START	" BREAK
			case	COND_PLAYER_SPOTTED_BY_LOST	sCond="	COND_PLAYER_SPOTTED_BY_LOST	" BREAK
			case	COND_BEAT_UP_GUY_REACT_CONDITIONS	sCond="	COND_BEAT_UP_GUY_REACT_CONDITIONS	" BREAK
			case	COND_A_PED_IS_REACTING	sCond="	COND_A_PED_IS_REACTING	" BREAK
			case	COND_BEAT_UP_GUY_SEES_TREVOR	sCond="	COND_BEAT_UP_GUY_SEES_TREVOR	" BREAK
			case	COND_LOST_SPOTTED_STICKY_BOMB	sCond="	COND_LOST_SPOTTED_STICKY_BOMB	" BREAK
			case	COND_BEAT_HIM_UP_DIALOGUE_STARTS	sCond="	COND_BEAT_HIM_UP_DIALOGUE_STARTS	" BREAK
			case	COND_FIXER_ANIM_FINISHED	sCond="	COND_FIXER_ANIM_FINISHED	" BREAK
			case	COND_TRAILER_ANIM_FINISHED	sCond="	COND_TRAILER_ANIM_FINISHED	" BREAK
			case	COND_BIKER_DRIVE_ANIM_FINISHED	sCond="	COND_BIKER_DRIVE_ANIM_FINISHED	" BREAK
			case	COND_BIKER_CHATTING_ANIM_FINISHED	sCond="	COND_BIKER_CHATTING_ANIM_FINISHED	" BREAK
			case	COND_BEATING_UP_ANIM_FINISHED	sCond="	COND_BEATING_UP_ANIM_FINISHED	" BREAK
			case	COND_COFFIN_ANIM_FINISHED	sCond="	COND_COFFIN_ANIM_FINISHED	" BREAK
			case	COND_ENEMIES_STILL_ALIVE	sCond="	COND_ENEMIES_STILL_ALIVE	" BREAK
			case	COND_THREE_BOMBS_PLANTED	sCond="	COND_THREE_BOMBS_PLANTED	" BREAK
			case	COND_ALL_BOMBS_PLANTED	sCond="	COND_ALL_BOMBS_PLANTED	" BREAK
			case	COND_ALL_WILL_BLOW_AT_ONCE	sCond="	COND_ALL_WILL_BLOW_AT_ONCE	" BREAK
			case	COND_TRIGGER_BUTTON_PRESSED	sCond="	COND_TRIGGER_BUTTON_PRESSED	" BREAK
			case	COND_BOMB_JUST_PLANTED	sCond="	COND_BOMB_JUST_PLANTED	" BREAK
			case	COND_FIRST_BOMB_PLANTED	sCond="	COND_FIRST_BOMB_PLANTED	" BREAK
			case	COND_ALL_TRAILERS_DESTROYED	sCond="	COND_ALL_TRAILERS_DESTROYED	" BREAK
			case	COND_A_TRAILER_HAS_BLOWN	sCond="	COND_A_TRAILER_HAS_BLOWN	" BREAK
			case	COND_PLAYER_IN_A_VEHICLE	sCond="	COND_PLAYER_IN_A_VEHICLE	" BREAK
			case	COND_LOS_QUAD_CHECKS	sCond="	COND_LOS_QUAD_CHECKS	" BREAK
			case	COND_FOUGHT_GUY_FELL	sCond="	COND_FOUGHT_GUY_FELL	" BREAK
			case	COND_THREE_SECONDS_SINCE_LAST_BOMB	sCond="	COND_THREE_SECONDS_SINCE_LAST_BOMB	" BREAK
			case	COND_PLANT_BOMBS_END	sCond="	COND_PLANT_BOMBS_END	" BREAK


			DEFAULT sCond = " -NOT DEFINED-" BREAK
		ENDSWITCH
		RETURN sCond
	ENDFUNC
#endif

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

proc SET_WADE_DEFAULTS()
	//cprintln(debug_trevor3,"Set wade defaults")
	IF NOT IS_PED_FUCKED(ped[ped_wade].id)
		//cprintln(debug_trevor3,"defaults set")
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(ped[ped_wade].id,TRUE)
		SET_PED_CAN_BE_TARGETTED(ped[ped_wade].id,FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped[ped_wade].id,RELGROUPHASH_NEUTRAL_TO_PLAYER)
		SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_wade].id,FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_wade].id,true)
		SET_PED_GROUP_MEMBER_PASSENGER_INDEX(ped[ped_wade].id,VS_FRONT_RIGHT)		
		SET_PED_GROUP_MEMBER_PASSENGER_INDEX(ped[ped_wade].id,VS_FRONT_RIGHT)
		
		
		SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_wade].id)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,1), 0, 0, 0) //(berd)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,3), 4, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,4), 3, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,9), 1, 1, 0) //(task)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,10), 3, 0, 0) //(decl)
		SET_PED_COMPONENT_VARIATION(ped[ped_wade].id, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
		
	ENDIF
ENDPROC

FUNC BOOL DOES_LINE_INTERSECT_tri(vector p1, vector p2, vector vCorner1,vector vCorner2,vector vCorner3)
		vector vPointOnPlane

		vector pNormal = NORMALISE_VECTOR(p2-p1)
		pNormal *= <<300,300,300>>
		pNormal += p1
		vector normal = CROSS_PRODUCT(vCorner2 - vCorner1, vCorner3 - vCorner1)
		IF NOT IS_VECTOR_ZERO(normal)
			normal = NORMALISE_VECTOR(normal)
			IF GET_LINE_PLANE_INTERSECT(vPointOnPlane, p1, pNormal, normal, vCorner1)
			//	IF GET_DISTANCE_BETWEEN_COORDS(p1,vPointOnPlane) < GET_DISTANCE_BETWEEN_COORDS(p1,p2)
					IF POINT_IN_TRI(vPointOnPlane,vCorner1,vCorner2,vCorner3)
						RETURN TRUE
					ENDIF
			//	ENDIF
			ENDIF

		ENDIf

		return false
	ENDFUNC



PROC CREATE_BIKER_PED(int iPed, MODEL_NAMES model, WEAPON_TYPE weapon, vector coord, float heading)
	ped[iPed].id = CREATE_PED(pedtype_mission,model,coord,heading)
	IF iPed = ped_fought
		SET_PED_RELATIONSHIP_GROUP_HASH(ped[iPed].id, relGroupDislike)
	ELSE
		
		SET_PED_RELATIONSHIP_GROUP_HASH(ped[iPed].id, relPreCombatEnemy[iped])
		//SET_PED_RELATIONSHIP_GROUP_HASH(ped[iPed].id, relGroupEnemy)	this is commented out so the first reacting ped won't inform all the others. Once he's played his alerted line, all peds are set a rel group
	ENDIF
	SET_PED_AS_ENEMY(ped[iPed].id, TRUE)		
	SET_PED_SEEING_RANGE(ped[iPed].id,SEEING_RANGE)
	SET_PED_HEARING_RANGE(ped[iPed].id,HEARING_RANGE)	
	SET_COMBAT_FLOAT(ped[iPed].id,CCF_BULLET_IMPACT_DETECTION_RANGE,3.0)
	SET_PED_ACCURACY(ped[iPed].id,30)
	STOP_PED_WEAPON_FIRING_WHEN_DROPPED(ped[iPed].id)
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(ped[iPed].id)
	if weapon != WEAPONTYPE_UNARMED
		GIVE_WEAPON_TO_PED(ped[iPed].id, weapon, INFINITE_AMMO, FALSE)
	ENDIF
ENDPROC

FUNC bool get_closest_enemy(int &iClosest)
	int iT
	iClosest = -1
	float fRange
	float fClosestRange = 150
	
	repeat count_of(ped) iT
		IF NOT IS_PED_FUCKED(ped[iT].id)
			IF iT != ped_wade
			AND iT != ped_fought
				fRange = GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[iT].id)
				If fRange < fClosestRange
					fClosestRange = fRange
					iClosest = iT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	If iClosest != -1
	AND iClosest != 0
		RETURN TRUE
	endif
	RETURN FALSE
endfunc

// RESET MISSION ETC

PROC PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle_index thisVeh,int syncSceneID,string animNAme,string animDict,FLOAT blendIn, FLOAT blendOUT)
	
	PLAY_SYNCHRONIZED_ENTITY_ANIM(thisVeh,syncSceneID,animNAme,animDict, blendIn, blendOUT,enum_to_int(SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE))
ENDPROC

// ============================ rayfire ===================

//to cause damage to peds nearby
proc make_fake_explosion_for_trailer(int iTrailer)
	float fPropAlong = GET_RANDOM_FLOAT_IN_RANGE(0.2,0.8)
	vector vPointAlongTrailer = rayfireTrailer[iTrailer].vLocates[0]  + ((rayfireTrailer[iTrailer].vLocates[1] - rayfireTrailer[iTrailer].vLocates[0]) * fPropAlong)
	SWITCH iTRailer
		CASE 0 vPointAlongTrailer.z = 42.78 - 2 BREAK
		CASE 1 vPointAlongTrailer.z =  42.64144 - 2 BREAK
		CASE 2 vPointAlongTrailer.z = 42.99 - 2 BREAK
		CASE 3 vPointAlongTrailer.z = 42.90 - 2 BREAK
		CASE 4 vPointAlongTrailer.z = 42.77 - 2 BREAK
	ENDSWITCH
	vector vExplosionPoint = GET_RANDOM_POINT_IN_SPHERE(vPointAlongTrailer,1.7)
	vExplosionPoint=<<vExplosionPoint.x,vExplosionPoint.y,vPointAlongTrailer.z>>
	//ADD_EXPLOSION(vExplosionPoint+<<0,0,1>>,EXP_TAG_SHIP_DESTROY,4) 
	//cprintln(debug_Trevor3,"explosion at: ",vExplosionPoint)
	ADD_EXPLOSION_WITH_USER_VFX(vExplosionPoint,exp_tag_grenade,get_hash_key("EXP_VFXTAG_TREV3_TRAILER"),0.5)
endproc

FUNC VECTOR GET_TRAILER_MIDPOINT(int iTrailer)
	return (rayfireTrailer[iTrailer].vLocates[0] + rayfireTrailer[iTrailer].vLocates[1])/2.0
ENDFUNC

FUNC BOOL IS_TRAILER_DESTROYED(int iTrailer)
	IF rayfireTrailer[iTrailer].state = playing
	OR rayfireTrailer[iTrailer].state = inactive
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RESET_TRAILER_PARK_RAYFIRES(enumEndState endState = END_STATE_MISSION_ONGOING)

	rayfireTrailer[0].vLocates[0] =   <<61.698372,3746.573242,43.304322>>
	rayfireTrailer[0].vLocates[1] =  <<37.680099,3742.203125,37.686787>>
	rayfireTrailer[0].fLength = 10.000000
	
	rayfireTrailer[1].vLocates[0] = <<111.925751,3722.156982,43.304322>> 
	rayfireTrailer[1].vLocates[1] = <<100.782082,3741.904785,37.485065>>
	rayfireTrailer[1].fLength = 10.000000
	
	rayfireTrailer[2].vLocates[0] = <<65.522331,3685.638428,43.304322>>
	rayfireTrailer[2].vLocates[1] = <<77.480957,3702.876953,37.608200>>
	rayfireTrailer[2].fLength = 10.000000
	
	rayfireTrailer[3].vLocates[0] = <<33.644451,3707.114014,43.304322>>
	rayfireTrailer[3].vLocates[1] = <<52.891590,3697.370850,37.888683>>
	rayfireTrailer[3].fLength = 10.000000
	
	rayfireTrailer[4].vLocates[0] = <<23.337156,3656.120361,43.304322>>
	rayfireTrailer[4].vLocates[1] = <<35.659618,3679.771973,37.697174>>
	rayfireTrailer[4].fLength = 12.500000
		
	rayfireTrailer[0].rayfireName = "des_trailerparka"
	rayfireTrailer[1].rayfireName = "des_trailerparkb"
	rayfireTrailer[2].rayfireName = "des_trailerparkc"
	rayfireTrailer[3].rayfireName = "des_trailerparkd"
	rayfireTrailer[4].rayfireName = "des_trailerparke"
	
	rayfireTrailer[0].audioName = "AZ_BIKER_CAMP_TRAILER_06"
	rayfireTrailer[1].audioName = "AZ_BIKER_CAMP_TRAILER_03"
	rayfireTrailer[2].audioName = "AZ_BIKER_CAMP_TRAILER_14"
	rayfireTrailer[3].audioName = "AZ_BIKER_CAMP_TRAILER_12"
	rayfireTrailer[4].audioName = "AZ_BIKER_CAMP_TRAILER_11"
	
	rayfireTrailer[0].buildingName = BUILDINGNAME_IPL_TRAILERPARK_A
	rayfireTrailer[1].buildingName = BUILDINGNAME_IPL_TRAILERPARK_B
	rayfireTrailer[2].buildingName = BUILDINGNAME_IPL_TRAILERPARK_C
	rayfireTrailer[3].buildingName = BUILDINGNAME_IPL_TRAILERPARK_D
	rayfireTrailer[4].buildingName = BUILDINGNAME_IPL_TRAILERPARK_E
	
	rayfireTrailer[0].occlusionName = BUILDINGNAME_IPL_TRAILERPARK_A_OCCLUSION
	rayfireTrailer[1].occlusionName = BUILDINGNAME_IPL_TRAILERPARK_B_OCCLUSION
	rayfireTrailer[2].occlusionName = BUILDINGNAME_IPL_TRAILERPARK_C_OCCLUSION
	rayfireTrailer[3].occlusionName = BUILDINGNAME_IPL_TRAILERPARK_D_OCCLUSION
	rayfireTrailer[4].occlusionName = BUILDINGNAME_IPL_TRAILERPARK_E_OCCLUSION
	
	//reset trailer park rayfire trailers
	IF endState = END_STATE_STAGE_SKIP
		rayfireTrailer[0].state = resetForSkip
		rayfireTrailer[1].state = resetForSkip
		rayfireTrailer[2].state = resetForSkip
		rayfireTrailer[3].state = resetForSkip
		rayfireTrailer[4].state = resetForSkip		
	endif
	
	REPEAT COUNT_OF (rayfireTrailer) i
		IF DOES_BLIP_EXIST(rayfireTrailer[i].blip)
			REMOVE_BLIP(rayfireTrailer[i].blip)
		ENDIF
	ENDREPEAT
	
	

ENDPROC

// ===================================================== Handling Dialogue ======================================================

// ============================================= Handling dialogue ===========================================






ped_index rpedInSlot[10]

func PED_INDEX Fped_in_slot(int slot)
	return rpedInSlot[slot]
ENDFUNC



PROC ADD_PED_FOR_DIALOGUE_EXTRA(int speakerIndex, ped_index speaker, string speakerLabel)
	int iJ
	repeat count_of(rpedInSlot) iJ
		if rpedInSlot[iJ] = speaker
			//there should never be more than one situation for this
			REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,iJ)
			rpedInSlot[iJ] = null
		endif
	endrepeat

	if rpedInSlot[speakerIndex] != null
		REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,speakerIndex)
	ENDIF
	
	
	
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,speakerIndex,speaker, speakerLabel)
	
	IF are_Strings_equal(speakerLabel,"WomanInTrailer")
		SET_VOICE_LOCATION_FOR_NULL_PED(MyLocalPedStruct, 7, << 70.8631, 3692.8467, 41.6392 >>)
	ENDIF
	
	//cprintln(debug_trevor3,"ADD ped for D index: ",speakerIndex," speaker: ",speakerLabel)
	rpedInSlot[speakerIndex] = speaker

ENDPROC



func bool CREATE_CONVERSATION_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, int speakerFour=-1, ped_index pedSpeakerFour=null, string speakerFourLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)	

	IF DOES_ENTITY_EXIST(pedSpeakerOne) AND IS_PED_FUCKED(pedSpeakerOne) RETURN TRUE ENDIF //abort creating a conversation but let script think it was called.
	IF DOES_ENTITY_EXIST(pedspeakerTwo) AND IS_PED_FUCKED(pedspeakerTwo) RETURN TRUE ENDIF
	IF DOES_ENTITY_EXIST(pedspeakerThree) AND IS_PED_FUCKED(pedspeakerThree) RETURN TRUE ENDIF
	IF DOES_ENTITY_EXIST(pedspeakerFour) AND IS_PED_FUCKED(pedspeakerFour) RETURN TRUE ENDIF

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		convStore[0].speakerNo = speakerOne
		convStore[0].pedIndex = pedSpeakerOne
		convStore[0].speakerLabel = speakerOneLabel
		convStore[1].speakerNo = speakerTwo
		convStore[1].pedIndex = pedSpeakerTwo
		convStore[1].speakerLabel = speakerTwoLabel
		convStore[2].speakerNo = speakerThree
		convStore[2].pedIndex = pedSpeakerThree
		convStore[2].speakerLabel = speakerThreeLabel
		convStore[3].speakerNo = speakerFour
		convStore[3].pedIndex = pedSpeakerFour
		convStore[3].speakerLabel = speakerFourLabel
		
		REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,speakerFour)
		
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		if speakerTwo != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
		if speakerThree != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
		if speakerFour != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
		ENDIF
		
		
	//	//cprintln(debug_Trevor3,GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES))
		IF IS_MESSAGE_BEING_DISPLAYED()		
		ANd IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
		//	//cprintln(debug_Trevor3,"convA")
			
			if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, Label, convPriority,DO_NOT_DISPLAY_SUBTITLES)
			
				bLastConvoWithoutSubtitles = TRUE
				iConvoAttempts = 0
				return true
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		else
		//	//cprintln(debug_Trevor3,"convB")
			if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, Label, convPriority)
				bLastConvoWithoutSubtitles = FALSE
				iConvoAttempts = 0
				return true
			ELSE
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		endif
	ENDIF
	return FALSE
ENDFUNC

func bool CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(string Label, string line, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null,int speakerFour=-1,ped_index pedSpeakerFour=null, string speakerFourLabel = null,enumConversationPriority convPriority = CONV_PRIORITY_HIGH)	

	IF DOES_ENTITY_EXIST(pedSpeakerOne) AND IS_PED_FUCKED(pedSpeakerOne) RETURN TRUE ENDIF //abort creating a conversation but let script think it was called.
	IF DOES_ENTITY_EXIST(pedspeakerTwo) AND IS_PED_FUCKED(pedspeakerTwo) RETURN TRUE ENDIF
	IF DOES_ENTITY_EXIST(pedspeakerThree) AND IS_PED_FUCKED(pedspeakerThree) RETURN TRUE ENDIF
	IF DOES_ENTITY_EXIST(pedspeakerFour) AND IS_PED_FUCKED(pedspeakerFour) RETURN TRUE ENDIF

	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,speakerFour)
	//		if Fped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
	//		ENDIF
			if speakerTwo != -1
	//			if Fped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
	//			ENDIF
			ENDIF
			if speakerThree != -1
	//			if Fped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
	//			ENDIF
			ENDIF
			if speakerFour != -1
	//			if Fped_in_slot(speakerFour) != pedSpeakerFour or pedSpeakerFour = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
	//			ENDIF
			ENDIF
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct,convBlock,Label,line,convPriority)		
			
		//		rpedInSlot[speakerIndex] = speaker
				iConvoAttempts = 0
				RETURN TRUE
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
endfunc

func bool PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(string Label, string line, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)

	IF DOES_ENTITY_EXIST(pedSpeakerOne) AND IS_PED_FUCKED(pedSpeakerOne) RETURN TRUE ENDIF //abort creating a conversation but let script think it was called.
	IF DOES_ENTITY_EXIST(pedspeakerTwo) AND IS_PED_FUCKED(pedspeakerTwo) RETURN TRUE ENDIF
	IF DOES_ENTITY_EXIST(pedspeakerThree) AND IS_PED_FUCKED(pedspeakerThree) RETURN TRUE ENDIF


	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,-1)	
			//if Fped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
			//ENDIF
			if speakerTwo != -1
				//if Fped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
				//ENDIF
			ENDIF
			if speakerThree != -1
				//if Fped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
				//ENDIF
			ENDIF
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct,convBlock,Label,line,convPriority)
				iConvoAttempts = 0
				RETURN TRUE
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	return false
ENDFUNC

func bool PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA_NO_SUBS(string Label, string line, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	
	IF DOES_ENTITY_EXIST(pedSpeakerOne) AND IS_PED_FUCKED(pedSpeakerOne) RETURN TRUE ENDIF //abort creating a conversation but let script think it was called.
	IF DOES_ENTITY_EXIST(pedSpeakertwo) AND IS_PED_FUCKED(pedspeakerTwo) RETURN TRUE ENDIF
	IF DOES_ENTITY_EXIST(pedSpeakerThree) AND IS_PED_FUCKED(pedspeakerThree) RETURN TRUE ENDIF

	
	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			//if Fped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
			REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,-1)
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
			//ENDIF
			if speakerTwo != -1
				//if Fped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
				//ENDIF
			ENDIF
			if speakerThree != -1
				//if Fped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
					ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
				//ENDIF
			ENDIF
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct,convBlock,Label,line,convPriority,DO_NOT_DISPLAY_SUBTITLES)
				iConvoAttempts = 0
				RETURN TRUE
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	return false
ENDFUNC

PROC ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,-1)
	if Fped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
	ENDIF
	if speakerTwo != -1
		if Fped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
	ENDIF
	if speakerThree != -1
		if Fped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
	ENDIF
	ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(MyLocalPedStruct, convBlock, Label, convPriority)
endproc

PROC ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA_NO_SUBS(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,-1)
	if Fped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
	ENDIF
	if speakerTwo != -1
		if Fped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
	ENDIF
	if speakerThree != -1
		if Fped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
	ENDIF
	ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(MyLocalPedStruct, convBlock, Label, convPriority,DO_NOT_DISPLAY_SUBTITLES)
endproc

// ================================================= Control mission ===========================================


PROC SET_DIALOGUE_AS_ENDED(int iEntry,enumDialogue thisDia)	
	//repeat count_of(dia) i
		IF dia[iEntry].dial = thisDia
			dia[iEntry].bCompleted = TRUE
		ELSE
			#if IS_DEBUG_BUILD
			TEXT_LABEL_63 txt
			txt = "Fail: SET_DIA_AS_ENDED() "
			txt+= GET_DIALOGUE_STRING(thisDia)
			SCRIPT_ASSERT(txt)
			#endif
		ENDIF		
	//ENDREPEAT
ENDPROC

PROC FORCE_DIALOGUE_STATE(int iDia, enumDialogue thisDia, bool ended = TRUE, int flag = 0)
	
	dia[iDia].dial = thisDia
	dia[iDia].bCompleted = ended
	dia[iDia].bStarted = TRUE
	dia[iDia].flag = flag
	dia[iDia].intA = 0

ENDPROC

PROC SET_DIALOGUE_FLAG(int iDia, enumDialogue thisDia, int flag = -1, int intA = -1, int intB = -1)
	#if IS_DEBUG_BUILD
	IF dia[iDia].dial != thisDia
		TEXT_LABEL_63 txt
			txt = "Fail: SET_DIA() "
			txt+= GET_DIALOGUE_STRING(thisDia)
			SCRIPT_ASSERT(txt)
		SCRIPT_ASSERT("SET_DIALOGUE_FLAG() has wrong entry value")
	ENDIF
	#endif
	thisDia=thisDia
	IF flag != -1
		dia[iDia].flag = flag
	ENDIF
	IF intA != -1
		dia[iDia].intA = intA
	ENDIF
	IF intB != -1
		dia[iDia].intB = intB
	ENDIF
ENDPROC

FUNC INT GET_DIALOGUE_FLAG(int iDia,enumDialogue thisDia)
	IF dia[iDia].dial = thisDia
		RETURN dia[iDia].flag
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT GET_DIALOGUE_INT(enumDialogue thisDia)
	repeat count_of(dia) i
		IF dia[i].dial = thisDia
			RETURN dia[i].intA
		ENDIF		
	ENDREPEAT
	RETURN 0
ENDFUNC

FUNC BOOL HAS_DIALOGUE_FINISHED(int iEntry,enumDialogue thisDia)
	IF dia[iEntry].dial = thisDia
		IF dia[iEntry].bCompleted
			RETURN TRUE
		ENDIF
	ELSE
		IF dia[iEntry].dial != DIA_NULL
			SCRIPT_ASSERT("HAS_DIALOGUE_FINISHED() has wrong entry value")
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_DIALOGUE_STARTED(int iEntry,enumDialogue thisDia)
	IF dia[iEntry].dial = thisDia
		IF dia[iEntry].bStarted
			RETURN TRUE
		ENDIF
	ELSE
		IF dia[iEntry].dial != DIA_NULL
			SCRIPT_ASSERT("HAS_DIALOGUE_STARTED() has wrong entry value")
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// condiotions

FUNC BOOL IS_CONDITION_TRUE(enumconditions conditiontoCheck)
	
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 
	
	IF iArray < MAX_CONDITIONS
	AND iArray >= 0
	
		IF conditions[iArray].condition = conditiontoCheck
			IF conditions[iArray].active
			AND conditions[iArray].returns
				RETURN TRUE
			ENDIF
		ELSE
		#if is_debug_build
			TEXT_LABEL_63 txt
			txt = ""
			txt += "COND NOT EQUAL TO CHECK:"
			txt += GET_CONDITION_STRING(conditiontoCheck)
			
			SCRIPT_ASSERT(txt)
		#endif
		ENDIF
	ELSE
		#if is_debug_build
		TEXT_LABEL_63 txt
		txt = ""
		txt += "IS_COND_TRUE() fail:"
		txt += GET_CONDITION_STRING(conditiontoCheck)
		
		SCRIPT_ASSERT(txt)#endif
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CONDITION_FALSE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		IF conditions[iArray].active
		AND NOT conditions[iArray].returns
			RETURN TRUE
		ENDIF
	ELSE
		
		cprintln(debug_Trevor3,"FAIL : ",GET_CONDITION_STRING(conditiontoCheck)," array: ",iArray)
		SCRIPT_ASSERT("IS_CONDITION_FALSE fail")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CONDITION_FLAG(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		RETURN conditions[iArray].flag
	ELSE
		
		cprintln(debug_Trevor3,"FAIL : ",GET_CONDITION_STRING(conditiontoCheck)," array: ",iArray)
		SCRIPT_ASSERT("GET_CONDITION_FLAG fail")
	ENDIF
	return 0
ENDFUNC

FUNC INT GET_CONDITION_INT(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		RETURN conditions[iArray].intA
	ELSE
		cprintln(debug_Trevor3,"FAIL : ",GET_CONDITION_STRING(conditiontoCheck)," array: ",iArray)
		SCRIPT_ASSERT("GET_CONDITION_INT fail")
		
	ENDIF
	return 0
ENDFUNC

FUNC BOOL WAS_CONDITION_TRUE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		IF conditions[iArray].active
		AND conditions[iArray].wasTrue
			RETURN TRUE
		ENDIF
	ELSE		
		cprintln(debug_Trevor3,"FAIL : ",GET_CONDITION_STRING(conditiontoCheck)," array: ",iArray)
		SCRIPT_ASSERT("WAS_CONDITION_TRUE fail")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CONDITION_WAS_TRUE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		conditions[iArray].wasTrue = TRUE		
		//conditions[iArray].returns
	ELSE		
		cprintln(debug_Trevor3,"FAIL : ",GET_CONDITION_STRING(conditiontoCheck)," array: ",iArray)
		SCRIPT_ASSERT("SET_CONDITION_WAS_TRUE fail")
	ENDIF
	
ENDPROC

FUNC bool andOrReturns(bool &pconditionsTrue, bool &pbracketOpen, andorEnum andOr, enumconditions cond)

		SWITCH andOr
			CASE cFORCEtrue
				pconditionsTrue = TRUE
			BREAK
			CASE cIGNORE
				RETURN FALSE
			BREAK
			CASE cIF
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cIFnot
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_FALSE(cond)))
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cOR
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cORbracket
				IF pconditionsTrue
					RETURN FALSE
				ELSE
					pbracketOpen = TRUE
					IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))
						pconditionsTrue = TRUE
					ENDIF
				ENDIF
			BREAK
			CASE cAND
				IF (cond != COND_NULL AND IS_CONDITION_FALSE(cond))
					pconditionsTrue = FALSE
				ENDIF
			BREAK
			CASE cANDNOT
				IF (cond != COND_NULL AND IS_CONDITION_TRUE(cond))
					pconditionsTrue = FALSE
				ENDIF
			BREAK
			CASE cWasTrueNowFalse
				IF WAS_CONDITION_TRUE(cond)
					IF IS_CONDITION_FALSE(COND)
						pconditionsTrue = TRUE
					ENDIF
				ELSE
					IF IS_CONDITION_TRUE(cond)
						SET_CONDITION_WAS_TRUE(COND)
						pconditionsTrue = FALSE
					ENDIF
				ENDIF
			BREAK		
		ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC bool checkANDOR(andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL, andorEnum andOr5=cIGNORE, enumconditions cond5 = COND_NULL)
	conditionsTrue = FALSE
	bracketOpen = FALSE
	
	IF andOrReturns(conditionsTrue,bracketOpen,andOr1,cond1)
		IF andOrReturns(conditionsTrue,bracketOpen,andOr2,cond2)
			IF andOrReturns(conditionsTrue,bracketOpen,andOr3,cond3) 
				IF andOrReturns(conditionsTrue,bracketOpen,andOr4,cond4) 
					IF andOrReturns(conditionsTrue,bracketOpen,andOr5,cond5) 
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF conditionsTrue
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL SET_CONDITION_STATE(enumconditions thisCondition, bool setReturns)
	int j

	REPEAT COUNT_OF(conditions) j		
		IF thisCondition = conditions[j].condition
			IF setReturns = TRUE
				conditions[j].returns = TRUE				
			ELSE
				conditions[j].returns = FALSE
			ENDIF
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

//actions
FUNC BOOL GET_THIS_ACTION_ID(enumActions actionToCheck, int &iReturn)
	REPEAT COUNT_OF(actions) i
		IF actions[i].action = actionToCheck
			iReturn = i
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ACTION_COMPLETE(int iEntry, enumActions actionToCheck)
	IF actions[iEntry].action = actionToCheck
		IF actions[iEntry].completed = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ACTION_ONGOING(enumActions actionToCheck)

	REPEAT COUNT_OF(actions) i
		IF actions[i].action = actionToCheck
			IF actions[i].active = TRUE
			AND actions[i].ongoing = TRUE
				IF actions[i].completed = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ACTION_STARTED(enumActions actionToCheck)

	REPEAT COUNT_OF(actions) i
		IF actions[i].action = actionToCheck
			IF actions[i].active = TRUE
			AND actions[i].ongoing = TRUE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC INT GET_ACTION_FLAG(int iAction, enumActions thisAction)
	IF actions[iAction].action = thisAction
		RETURN actions[iAction].flag
	ELSE
		IF actions[iAction].action != ACT_NULL
			SCRIPT_ASSERT("Incorrect value passed in to GET_ACTION_FLAG()")
		ENDIF
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_ACTION_INTA(int iAction, enumActions thisAction)
	IF actions[iAction].action = thisAction
		RETURN actions[iAction].intA
	ELSE
		IF actions[iAction].action != ACT_NULL
			TEXT_LABEL_63 txt
			txt = "Fail: GET_ACTION_INTA() "
			txt+= enum_to_int(thisAction)
			txt += " "
			txt += enum_to_int(actions[iAction].action)
			SCRIPT_ASSERT(txt)
		ENDIF
	ENDIF
	
	RETURN 0
ENDFUNC

PROC SET_ACTION_FLAG(int iAction, enumActions actionToSet, int iNewFlagValue)
	IF actions[iAction].action = actionToSet
		actions[iAction].flag = iNewFlagValue
	ELSE
		IF actions[iAction].action != ACT_NULL
			TEXT_LABEL_63 txt
			txt = "Fail: SET_ACTION_FLAG() "
			txt += enum_to_int(actionToSet)
			txt += " "
			txt += enum_to_int(actions[iAction].action)
			SCRIPT_ASSERT(txt)
		ENDIF
	ENDIF	
endproc

PROC SET_ACTION_NEEDS_CLEANUP(int iClean)
	actions[iClean].needsCleanup = TRUE
ENDPROC

PROC FORCE_ACTION_STATE(int thisI, enumActions thisAction, bool setCompleted=TRUE, bool setOngoing = TRUE)
	
	actions[thisI].action = thisAction
	actions[thisI].active = TRUE
	actions[thisI].ongoing = setOngoing
	actions[thisI].completed = setCompleted		
	actions[thisI].flag = 0
	actions[thisI].needsCleanup = FALSE
	actions[thisI].intA = 0
	actions[thisI].intB = 0
	actions[thisI].floatA = 0
	
ENDPROC

PROC DO_TRAILERPARK_ARRIVAL(structActions &thisAction)
	//cprintln(debug_trevor3,"DO_TRAILERPARK_ARRIVAL")
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()				
		bPressedSkip = TRUE
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF		
	ENDIF
	
	//cutscene skipped
	IF IS_SCREEN_FADED_OUT()
	AND bPressedSkip = TRUE
		
		NEW_LOAD_SCENE_STOP()
		SET_CLOCK_TIME(22,0,0)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA()
		IF IS_CONDITION_TRUE(COND_ARRIVED_IN_VEHICLE)
			IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
				IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehicle[veh_player].id))
				OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehicle[veh_player].id))
					FREEZE_ENTITY_POSITION(vehicle[veh_player].id,FALSE)
					SET_ENTITY_VISIBLE(vehicle[veh_player].id,TRUE)
					SET_VEHICLE_FORWARD_SPEED(vehicle[veh_player].id,thisAction.floatA)
				ELIF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehicle[veh_player].id))
					FREEZE_ENTITY_POSITION(vehicle[veh_player].id,FALSE)
					SET_ENTITY_VISIBLE(vehicle[veh_player].id,TRUE)
				ELSE
					SET_ENTITY_COORDS(vehicle[veh_player].id, <<59.1493, 3605.3535, 38.8528>>)
					SET_ENTITY_HEADING(vehicle[veh_player].id, 358.6649)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehicle[veh_player].id)										
					SET_VEHICLE_LIGHTS(vehicle[veh_player].id,SET_VEHICLE_LIGHTS_OFF)	
				ENDIF	
			ELSE
				SET_ENTITY_COORDS(player_ped_id(), <<59.8307, 3605.6824, 38.8624>>)
				SET_ENTITY_HEADING(player_ped_id(), 348.9076)
				IF NOT IS_PED_FUCKED(ped[ped_wade].id)
					SET_ENTITY_COORDS(ped[ped_wade].id, <<60.9103, 3602.3120, 38.7845>>)
					SET_ENTITY_HEADING(ped[ped_wade].id, 0.8571)
				ENDIF
			ENDIF
		ELSE
			SET_ENTITY_COORDS(player_ped_id(), <<59.8307, 3605.6824, 38.8624>>)
			SET_ENTITY_HEADING(player_ped_id(), 348.9076)
			IF NOT IS_PED_FUCKED(ped[ped_wade].id)
				SET_ENTITY_COORDS(ped[ped_wade].id, <<60.9103, 3602.3120, 38.7845>>)
				SET_ENTITY_HEADING(ped[ped_wade].id, 0.8571)
			ENDIF								
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-20)
		
		thisAction.flag = 6
	ENDIF
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF thisAction.flag = 0
		//BEGIN_SRL()
		
		
		
		CPRINTLN(debug_trevor3,"iCamZone = ",iCamZone)
		
		If iCamZone = 0
		OR iCamZone = -1
			IF IS_VEHICLE_DRIVEABLE(vehicle[veh_player].id)
				MODEL_NAMES thisModel
				thisModel = GET_ENTITY_MODEL(vehicle[veh_player].id)
				IF IS_THIS_MODEL_A_HELI(thisModel)
				OR IS_THIS_MODEL_A_PLANE(thisModel)
				OR IS_THIS_MODEL_A_BOAT(thisModel)
					START_AUDIO_SCENE("TREVOR_3_TRAILER_PARK_OVERVIEW")
					thisAction.flag = 1
				ELSe
					IF IS_PED_ON_FOOT(player_ped_id())
						START_AUDIO_SCENE("TREVOR_3_TRAILER_PARK_OVERVIEW")
						thisAction.flag = 1
					ENDIF
				ENDIF
			ELSE
				START_AUDIO_SCENE("TREVOR_3_TRAILER_PARK_OVERVIEW")
				thisAction.flag = 1
			ENDIF
		ELSE
			START_AUDIO_SCENE("TREVOR_3_TRAILER_PARK_OVERVIEW")
			thisAction.flag = 1
		ENDIF
	ENDIF
	
	SET_SRL_TIME(to_float(timera()))
	CPRINTLN(debug_trevor3,"TIMERA = ",timera())

	
			SWITCH thisAction.flag
				CASE 0			
				
					//choose whether to show car arriving from left or right
					
					bPressedSkip = FALSE
					
					sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
					
					CLEAR_ANGLED_AREA_OF_VEHICLES(<<25.257790,3600.882080,28.990997>>, <<113.032845,3570.979248,52.484802>>, 61.250000)
					
					IF GET_HEADING_FROM_COORDS(<<59.2455, 3609.4213, 40.7427>>,GET_ENTITY_COORDS(player_ped_id())) > 180.0 //from the left	
				
						SETTIMERA(0)
						SET_SRL_TIME(0)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<52.9476, 3616.5872, 39.7652>>, <<7.6593, 0.0986, -144.2271>>,6000)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<56.0832, 3615.2510, 39.9863>>, <<2.4213, 0.0595, -149.8007>>,6000)
						
						SET_CAM_FOV(sTimelapse.splineCamera,30)
						IF IS_VEHICLE_DRIVEABLE(vehicle[veh_player].id)
							vector vWAy
							WAYPOINT_RECORDING_GET_COORD("trev3_trL",0,vWAy)
							SET_ENTITY_COORDS(vehicle[veh_player].id,vWAy)
							SET_ENTITY_HEADING(vehicle[veh_player].id,73)
							SET_VEHICLE_FORWARD_SPEED(vehicle[veh_player].id,12)
							
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(),vehicle[veh_player].id,"trev3_trL",DRIVINGMODE_AVOIDCARS_RECKLESS)
							
						ENDIF
						
						
					ELSE
						SETTIMERA(6000)
						SET_SRL_TIME(6000)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<64.0716, 3612.8428, 41.0038>>, <<-1.3547, 0.0000, 147.7515>>,6000)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<60.8387, 3613.2549, 39.9549>>, <<3.0742, 0.0000, 155.9106>>,6000)
						SET_CAM_FOV(sTimelapse.splineCamera,40)
						IF IS_VEHICLE_DRIVEABLE(vehicle[veh_player].id)
							vector vWAy
							WAYPOINT_RECORDING_GET_COORD("trev3_trR",0,vWAy)
							SET_ENTITY_COORDS(vehicle[veh_player].id,vWAy)
							//SET_ENTITY_COORDS(vehicle[veh_player].id,<<39.4072, 3592.3840, 38.7656>>)
							SET_ENTITY_HEADING(vehicle[veh_player].id,254)
							SET_VEHICLE_FORWARD_SPEED(vehicle[veh_player].id,12)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(),vehicle[veh_player].id,"trev3_trR",DRIVINGMODE_AVOIDCARS_RECKLESS)
						ENDIF
						
					ENDIF
					
			
					
					
					
					
					SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera,CAM_SPLINE_SLOW_OUT_SMOOTH)
					
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(player_ped_id(),true)
					
					SHAKE_CAM(sTimelapse.splineCamera,"Hand_shake",0.3)
					SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					
			
					IF IS_SCREEN_FADED_OUT()
					AND bPressedSkip = FALSE
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					
					SET_FRONTEND_RADIO_ACTIVE(FALSE)		
					CLEAR_PLAYER_WANTED_LEVEL(player_id())
					playMusic("TRV3_START",true,"TRV3_ALERTED")
					START_AUDIO_SCENE("TREVOR_3_TRAILER_PARK_OVERVIEW")
					thisAction.intA = TIMERA() + 6000
					thisAction.flag=1
					
				BREAK

				CASE 1
					CPRINTLN(debug_trevor3,"waiting for REQUEST_SCRIPT_AUDIO_BANK")
			//		SETTIMERA(12000)
			//		SET_SRL_TIME(12000)
					if REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
					AND TIMERA() > thisAction.intA
					
					
					
						DESTROY_ALL_CAMS()
						
						IF NOT DOES_CAM_EXIST(sTimelapse.splineCamera)
							sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
						ENDIF
						
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
							vehicle[veh_player].id = GET_VEHICLE_PED_IS_IN(player_ped_id())
							
							IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehicle[veh_player].id))
							OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehicle[veh_player].id))
							OR IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehicle[veh_player].id))
								thisAction.floatA = GET_ENTITY_SPEED(vehicle[veh_player].id)
								FREEZE_ENTITY_POSITION(vehicle[veh_player].id,TRUE)
								SET_ENTITY_VISIBLE(vehicle[veh_player].id,FALSE)																
							ENDIF
						ENDIF		
						
						IF iCamZone = -1
							iCamZone = 0
						ENDIF
						
						SWITCH iCamZone
							
							CASE 0				
								SETTIMERA(12000)
								SET_SRL_TIME(12000)
								
						
								ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<67.9004, 3636.9590, 43.3989>>, <<-1.8034, 0.0000, 9.4512>>,5000)
								ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<64.8050, 3638.7800, 45.6303>>, <<-3.6109, 0.0000, 12.5594>>,6000)
								SET_CAM_FOV(sTimelapse.splineCamera,47.8)	
								SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
								//cprintln(debug_Trevor3,"check player vehicle")
								IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
									vehicle[veh_player].id = GET_VEHICLE_PED_IS_IN(player_ped_id())
									
									IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehicle[veh_player].id))
									AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehicle[veh_player].id))																			
									AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehicle[veh_player].id))	
										TASK_VEHICLE_DRIVE_TO_COORD(player_ped_id(),vehicle[veh_player].id,<<59.0078, 3604.2839, 38.8316>>,10.0,DRIVINGSTYLE_NORMAL,DUMMY_MODEL_FOR_SCRIPT,DRIVINGMODE_AVOIDCARS,3.0,3.0)
										SET_VEHICLE_LIGHTS(vehicle[veh_player].id,SET_VEHICLE_LIGHTS_OFF)	
										//cprintln(debug_trevor3,"LIGHTS OFF")
										SET_VEHICLE_SIREN(vehicle[veh_player].id,FALSE)
									ENDIF
								ENDIF
								thisAction.intA = TIMERA() + 7000
							BREAK
							CASE 1
								SETTIMERA(19000)
								SET_SRL_TIME(19000)
							
								ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<142.7424, 3728.2502, 44.4219>>, <<-1.2115, -0.0000, 105.4402>>,5000)
								ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<142.0027, 3728.0459, 47.1392>>, <<-1.2115, -0.0000, 107.9020>>,5000)
								SET_CAM_FOV(sTimelapse.splineCamera,50)
								SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
								thisAction.intA = TIMERA() + 5000
							BREAK
							CASE 2
								SETTIMERA(24000)
								SET_SRL_TIME(24000)
							
								ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<83.0859, 3796.3843, 45.6352>>, <<-3.6228, 0.0000, 178.2066>>,5000)
								ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<83.9047, 3795.4473, 49.5695>>, <<-3.6228, 0.0000, 173.1413>>,5000)
						
								SET_CAM_FOV(sTimelapse.splineCamera,50)
								SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
								thisAction.intA = TIMERA() + 5000
						
							BREAK
						ENDSWITCH
						SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						//REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						sTimelapse.sStartTimeOfDay = GET_CURRENT_TIMEOFDAY()
						
						IF arriveComment = AR_WAITNIGHT
						OR (arriveComment = AR_NOTSET AND GET_CLOCK_HOURS() < 21 AND GET_CLOCK_HOURS() > 6)
							arriveComment = AR_WAITNIGHT
							iWhooshSound = GET_SOUND_ID() 
		                	PLAY_SOUND_FRONTEND(iWhooshSound, "TIME_LAPSE_MASTER") 
		                	START_AUDIO_SCENE("TOD_SHIFT_SCENE")
						ENDIF
						SET_ACTION_FLAG(2,ACT_CUTSCENE_LIGHTNING_FLASH,1)
						thisAction.flag=2
				
						 
					ENDIF
				BREAK
				
				CASE 2
					IF thisAction.intA - TIMERA() < 7000
						IF arriveComment = AR_WAITNIGHT
						OR (arriveComment = AR_NOTSET AND GET_CLOCK_HOURS() < 21 AND GET_CLOCK_HOURS() > 6)
							arriveComment = AR_WAITNIGHT
							SKIP_TO_TIME_DURING_SPLINE_CAMERA(22, 00, "","", sTimelapse) 
						ENDIF
					ENDIF
					
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					IF TIMERA() > thisAction.intA
						SETTIMERA(29000)
						SET_SRL_TIME(29000)
							STOP_SOUND(iWhooshSound)
							stop_audio_scene("TOD_SHIFT_SCENE")
							RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")							
							DESTROY_ALL_CAMS()
							sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
							ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<52.5215, 3704.1267, 39.5312>>, <<2.4381, 0.0000, 172.2686>>,5700)
							ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<52.6653, 3703.8884, 39.5404>>, <<3.6794, 0.0000, 172.1825>>,5700)
							SET_CAM_FOV(sTimelapse.splineCamera,29.66)							
							SHAKE_CAM(sTimelapse.splineCamera,"Hand_shake",0.8)
							
						
						//	PRINT("TRV3_11",5000,1)
							//SET_CAM_SPLINE_SMOOTHING_STYLE(script_cam,CAM_SPLINE_SLOW_OUT_SMOOTH)
							thisAction.flag++
				
					ENDIF
				BREAK
				case 3					
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA("TRV3_CHAT","TRV3_CHAT_1",3,ped[ped_driveOffA].id,"TREV3BIKER2",4,ped[ped_driveOffB].id,"TRV3BIKER3")
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iCoffinAnim)
							SET_SYNCHRONIZED_SCENE_PHASE(iCoffinAnim,0.0)
						ENDIF
						thisAction.flag++
					ENDIf
				break
				CASE 4
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					//IF IS_NEW_LOAD_SCENE_LOADED() //possibly never loads
						If GET_CAM_SPLINE_PHASE(sTimelapse.splineCamera) >= 0.93	
						AND NOT IS_CONV_ROOT_PLAYING("TRV3_CHAT")
							SETTIMERA(32295)
							SET_SRL_TIME(32295)
							CLEAR_PRINTS()
							//PRINT("TRV3_11b",5000,1)
							DESTROY_CAM(sTimelapse.splineCamera)
							sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
							ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<99.6278, 3750.4080, 39.9449>>, <<1.9937, -0.0000, 176.8776>>,6500)
							ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<99.6649, 3751.0872, 39.9212>>, <<1.9937, -0.0000, 176.8776>>,6500)
							SET_CAM_FOV(sTimelapse.splineCamera,37.16)	
							SHAKE_CAM(sTimelapse.splineCamera,"Hand_shake",0.8)
						

							
							//SET_CAM_SPLINE_SMOOTHING_STYLE(script_cam,CAM_SPLINE_SLOW_OUT_SMOOTH)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(2)
							thisAction.flag++
						ENDIF
					//ENDIF
					
				BREAK
				case 5
					
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA("TRV3_ST5","TRV3_ST5_9",4,ped[ped_coffins].id,"TRV3_BIKER1")
						thisAction.flag=41
					ENDIf
				break
				case 41
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA("TRV3_ST5","TRV3_ST5_11",4,ped[ped_coffins].id,"TRV3_BIKER1")
						thisAction.flag=42
					ENDIf
				break
				CASE 42
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					If GET_CAM_SPLINE_PHASE(sTimelapse.splineCamera) >= 0.95
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						thisAction.intA = 0
						
						CLEAR_PRINTS()
						
						IF IS_CONDITION_TRUE(COND_ARRIVED_IN_VEHICLE)
							IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
								IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehicle[veh_player].id))
								OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehicle[veh_player].id))
								OR IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehicle[veh_player].id))
								
									FREEZE_ENTITY_POSITION(vehicle[veh_player].id,FALSE)
									SET_ENTITY_VISIBLE(vehicle[veh_player].id,TRUE)
									SET_VEHICLE_FORWARD_SPEED(vehicle[veh_player].id,thisAction.floatA)
								ELSE
									SETTIMERA(37500)
									SET_SRL_TIME(37500)
									DESTROY_CAM(sTimelapse.splineCamera)
									sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
									ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<57.3634, 3603.9126, 42.2868>>, <<3.9263, 0.0000, -3.3426>>,6500)
									ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera,<<57.1100, 3602.3652, 40.8780>>, <<2.4529, -0.0000, -13.2129>>,3500)
									SET_CAM_FOV(sTimelapse.splineCamera,50)	
									SET_CAM_SPLINE_SMOOTHING_STYLE(sTimelapse.splineCamera,CAM_SPLINE_SLOW_OUT_SMOOTH)
									thisAction.intA = TIMERA() + 3500
									SET_ENTITY_COORDS(vehicle[veh_player].id, <<59.1493, 3605.3535, 38.8528>>)
									SET_ENTITY_HEADING(vehicle[veh_player].id, 358.6649)
									SET_VEHICLE_ON_GROUND_PROPERLY(vehicle[veh_player].id)
										thisAction.flag=50
									ENDIF
								ELSE
									SET_ENTITY_COORDS(player_ped_id(), <<59.8307, 3605.6824, 38.8624>>)
									SET_ENTITY_HEADING(player_ped_id(), 348.9076)
									IF NOT IS_PED_FUCKED(ped[ped_wade].id)
										SET_ENTITY_COORDS(ped[ped_wade].id, <<60.9103, 3602.3120, 38.7845>>)
										SET_ENTITY_HEADING(ped[ped_wade].id, 0.8571)
									ENDIF
								ENDIF
							ELSE
								SET_ENTITY_COORDS(player_ped_id(), <<59.8307, 3605.6824, 38.8624>>)
								SET_ENTITY_HEADING(player_ped_id(), 348.9076)
								IF NOT IS_PED_FUCKED(ped[ped_wade].id)
									SET_ENTITY_COORDS(ped[ped_wade].id, <<60.9103, 3602.3120, 38.7845>>)
									SET_ENTITY_HEADING(ped[ped_wade].id, 0.8571)
								ENDIF								
							ENDIF
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-20)
							If thisAction.flag != 50
								thisAction.flag=6
							ENDIF
						ENDIF
					BREAK
					
					case 50
						IF TIMERA() > thisAction.intA
							DESTROY_CAM(sTimelapse.splineCamera)
							
							STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
							SET_PLAYER_CONTROL(player_id(),TRUE)	
							clear_ped_tasks(player_ped_id())
							
							
							IF IS_STREAMVOL_ACTIVE()
								STREAMVOL_DELETE(trailer_frustum)
							endif
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(3)						
							DO_SCREEN_FADE_IN(1000)
							STOP_AUDIO_SCENE("TREVOR_3_TRAILER_PARK_OVERVIEW")
							SET_FRONTEND_RADIO_ACTIVE(TRUE)
							SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE,false,2000,true,false,false,false)
							END_SRL()
							
							REPLAY_STOP_EVENT()
							
							thisAction.completed = TRUE
						ENDIF
					BREAK
					CASE 6
							
							//add flash back to first person
							IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							AND NOT bPressedSkip
								ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
								PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							ENDIF
							
							STOP_SOUND(iWhooshSound)
							stop_audio_scene("TOD_SHIFT_SCENE")
							RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")	
							DESTROY_CAM(sTimelapse.splineCamera)
							RENDER_SCRIPT_CAMS(FALSE,FALSE)
							//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
							SET_PLAYER_CONTROL(player_id(),TRUE)	
							clear_ped_tasks(player_ped_id())
							SET_PED_STEALTH_MOVEMENT(player_ped_id(),true)
												
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(4)						
							DO_SCREEN_FADE_IN(1000)
							STOP_AUDIO_SCENE("TREVOR_3_TRAILER_PARK_OVERVIEW")
							SET_FRONTEND_RADIO_ACTIVE(TRUE)
							SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE,false,2000,true,false,false,false)
							END_SRL()
							
							REPLAY_STOP_EVENT()
							SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
							thisAction.completed = TRUE
				
					BREAK
				ENDSWITCH	
ENDPROC

PROC HANDLE_EXITTING_PEDS_FROM_CUTSCENE()
	
	vector vLastCarCoord
	IF GET_LAST_DRIVEN_VEHICLE() != null
		IF DOES_ENTITY_EXIST(GET_LAST_DRIVEN_VEHICLE())		
			IF IS_VEHICLE_DRIVEABLE(GET_LAST_DRIVEN_VEHICLE())									
				IF IS_VECTOR_ZERO(vLastCarCoord)
			
					vLastCarCoord = GET_ENTITY_COORDS(GET_LAST_DRIVEN_VEHICLE())
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	

	IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Wade"))	
		ped[ped_wade].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Wade"))
		SET_WADE_DEFAULTS()
	ENDIF				
	
			

	if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Wade")	
		IF NOT IS_PED_INJURED(ped[ped_wade].id)
			SET_ENTITY_COORDS(ped[ped_wade].id,<<1981.4721, 3815.6523, 31.3564>>)
			
			IF IS_POINT_IN_ANGLED_AREA(vLastCarCoord,<<1963.927734,3789.891113,30.252787>>, <<2002.270142,3813.016602,36.092949>>, 19.562500)
				SET_ENTITY_HEADING(ped[ped_wade].id,260)
			ELSE
				SET_ENTITY_HEADING(ped[ped_wade].id,303.0639)
			ENDIF
			
			
			FORCE_PED_MOTION_STATE(ped[PED_wade].id,MS_ON_FOOT_WALK,FALSE,FAUS_DEFAULT)																		
			SET_PED_MIN_MOVE_BLEND_RATIO(ped[PED_wade].id,PEDMOVE_WALK)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[PED_wade].id,TRUE)
			SET_WADE_DEFAULTS()
		ENDIF
	ENDIF
	
	if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")			
		cprintln(debug_trevor3,"Hit this exit for Trevor?")
		SET_ENTITY_COORDS(player_ped_id(),<<1982.0884, 3817.2808, 31.2670>>)

		IF IS_POINT_IN_ANGLED_AREA(vLastCarCoord,<<1963.927734,3789.891113,30.252787>>, <<2002.270142,3813.016602,36.092949>>, 19.562500)
			SET_ENTITY_HEADING(PLAYER_PED_ID(),233)
		ELSE
			SET_ENTITY_HEADING(player_ped_id(),308.0639)
		ENDIF
										
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE) 
        SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE) 
		IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
			 FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING, FALSE)
			 SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1200)
		ELSE
			 FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE)
			 SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 600)
		ENDIF
        
        

	ENDIF
ENDPROC

PROC ACTION2(int thisI)			
	
	SWITCH actions[thisI].action
		case ACT_EXPLOSION_CAMS
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				STOP_STREAM()			
				bPressedSkip = TRUE
				actions[thisI].flag = 2
			ENDIF
			
			SWITCH actions[thisI].flag
				CASE 0	
					IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_DETONATE_BOMBS")
						STOP_AUDIO_SCENE("TREVOR_3_DETONATE_BOMBS")
					ENDIF
					IF LOAD_STREAM("TREVOR3_BLOW_UP_TRAILERS_MASTER")
					AND HAS_ANIM_DICT_LOADED("misstrevor3")																												
						bPressedSkip = FALSE
						sTimelapse.splineCamera = CREATE_CAMERA(CAMTYPE_ANIMATED,true)								
						IF IS_STREAMVOL_ACTIVE()
							STREAMVOL_DELETE(trailer_frustum)
						ENDIF
						trailer_frustum = STREAMVOL_CREATE_FRUSTUM(<<63.6975, 3630.1255, 49.8993>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-4.2671, 0.6115, 13.5931>>) ,150,FLAG_MAPDATA,STREAMVOL_LOD_FLAG_HIGH)


						iCamSync = CREATE_SYNCHRONIZED_SCENE(<<68.185,3653.15,45.796>>,<<0,0,6.38>>)
						PLAY_SYNCHRONIZED_CAM_ANIM(sTimelapse.splineCamera,icamsync,"ALL_TRAILERS_EXPLOSION_CAM","misstrevor3")
								//script_cam									
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
						PLAY_STREAM_FRONTEND()
						CLEAR_AREA_OF_PROJECTILES(<<64.2897, 3711.5496, 40.4220>>,100)
						
						/*
						rayfireTrailer[0].state = resetForSkip
						rayfireTrailer[1].state = resetForSkip
						rayfireTrailer[2].state = resetForSkip
						rayfireTrailer[3].state = resetForSkip
						rayfireTrailer[4].state = resetForSkip
						*/
												
						RENDER_SCRIPT_CAMS(true,false)
						
						SET_TIMECYCLE_MODIFIER("trailer_explosion_optimise")

						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						CLEAR_PRINTS()
						CLEAR_HELP()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(7)
						
						IF bAPedIsReacting OR bPlayerSeen
							PLAYMUSIC("TRV3_EXPLOSIONS_ALERTED",FALSE,"TRV3_EXPLOSIONS")
						ELSE
							PLAYMUSIC("TRV3_EXPLOSIONS",TRUE,"TRV3_EXPLOSIONS")
						ENDIF
						
						
						//check if player near any trailers
						int iT
						REPEAT COUNT_OF(rayfireTrailer) iT
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),GET_TRAILER_MIDPOINT(iT)) < 15
								vehicle_index aVeh
								IF IS_PED_IN_ANY_VEHICLE(player_ped_id())	
									aVEh = GET_VEHICLE_PED_IS_IN(player_ped_id())
								ENDIF
								SWITCH iT
									CASE 0
										IF IS_VEHICLE_SAFE(aVEh)
											SET_ENTITY_COORDS(aVeh,<<67.7464, 3738.6018, 38.7134>>)
											SET_ENTITY_HEADING(player_ped_id(), 289.8005)
										ELSE
											SET_ENTITY_COORDS(player_ped_id(),<<48.8548, 3763.2749, 38.6019>>) 
											SET_ENTITY_HEADING(player_ped_id(),171.1065)
										ENDIF
									BREAK

									CASE 1
										IF IS_VEHICLE_SAFE(aVEh)
											SET_ENTITY_COORDS(aVeh,<<100.3786, 3709.5959, 38.7631>>)
											SET_ENTITY_HEADING(player_ped_id(), 150.8005)
										ELSE
											SET_ENTITY_COORDS(player_ped_id(),<<125.3921, 3730.3071, 38.7241>>) 
											SET_ENTITY_HEADING(player_ped_id(),96.0804)
										ENDIF
									BREAK

									CASE 2
										IF IS_VEHICLE_SAFE(aVEh)
											SET_ENTITY_COORDS(aVeh,<<85.5019, 3687.4158, 38.7033>>)
											SET_ENTITY_HEADING(player_ped_id(), 150.8005)
										ELSE
											SET_ENTITY_COORDS(player_ped_id(),<<66.9316, 3673.0535, 38.8189>>) 
											SET_ENTITY_HEADING(player_ped_id(),334.2690)
										ENDIF
									BREAK

									CASE 3	
										IF IS_VEHICLE_SAFE(aVEh)
											SET_ENTITY_COORDS(aVeh,<<31.3841, 3688.9363, 38.6971>>)
											SET_ENTITY_HEADING(player_ped_id(), 203.8005)
										ELSE
											SET_ENTITY_COORDS(player_ped_id(),<<45.2714, 3682.5688, 38.7484>>) 
											SET_ENTITY_HEADING(player_ped_id(),213.2737)
										ENDIF
									BREAK

									CASE 4
										IF IS_VEHICLE_SAFE(aVEh)
											SET_ENTITY_COORDS(aVeh,<<31.3841, 3688.9363, 38.6971>>)
											SET_ENTITY_HEADING(player_ped_id(), 203.8005)
										ELSE
											SET_ENTITY_COORDS(player_ped_id(),<<19.7921, 3646.6829, 39.0559>>) 
											SET_ENTITY_HEADING(player_ped_id(),316.9460)
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDREPEAT
						
						 actions[thisI].flag++
						
					ENDIF
				BREAK
				
				CASE 1
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iCamSync)
						If GET_SYNCHRONIZED_SCENE_PHASE(iCamSync) = 1.0
							actions[thisI].flag++
						ENDIF
					ELSE
						actions[thisI].flag++
					ENDIF
					
				BREAK
			ENDSWITCH
			
			IF actions[thisI].flag=2
			
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				AND NOT bPressedSkip
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				ENDIF
			
				DESTROY_CAM(script_cam)														
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				SET_PLAYER_CONTROL(player_id(),TRUE)	
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-20)
				
				IF IS_STREAMVOL_ACTIVE()
					STREAMVOL_DELETE(trailer_frustum)
				ENDIF
				
				CLEAR_TIMECYCLE_MODIFIER()
				
				rayfireTrailer[0].state = skip_to_blown
				rayfireTrailer[1].state = skip_to_blown
				rayfireTrailer[2].state = skip_to_blown
				rayfireTrailer[3].state = skip_to_blown
				rayfireTrailer[4].state = skip_to_blown	

				actions[thisI].completed = TRUE
				PLAYMUSIC("TRV3_GET_TO_CAR",FALSE,"TRV3_EXPLOSIONS")
			ENDIF
		BREAK
		
		CASE ACT_CLEAR_WEATHER
			SWITCH actions[thisI].flag
				CASE 0
					SET_WEATHER_TYPE_OVERTIME_PERSIST("CLEARING",45.0)
					actions[thisI].flag++
					actions[thisI].intA = GET_GAME_TIMER() + 45000
					cprintln(debug_Trevor3,"WEATHER CHANGE")
				BREAK
				CASE 1		
					
					IF GET_GAME_TIMER() > actions[thisI].intA
						SET_WEATHER_TYPE_OVERTIME_PERSIST("SMOG",50.0)
						cprintln(debug_Trevor3,"FINAL WEATHER CHANGE")
						actions[thisI].completed = TRUE
					ENDIF							
				BREAK
			ENDSWITCH					
		BREAK
		
		CASE ACT_CLEAN_UP_TRAILER_PARK
	
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_02)
			SET_MODEL_AS_NO_LONGER_NEEDED(DLOADER)
			SET_MODEL_AS_NO_LONGER_NEEDED(HEXER)	
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Coffin_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_CS_Beer_Bot_01)
			REMOVE_ANIM_DICT("misstrevor3")
			REMOVE_ANIM_DICT("misstrevor3_beatup")
			
			IF DOES_ENTITY_EXIST(vehicle[veh_ambientA].id)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_ambientA].id)
			ENDIF
			IF DOES_ENTITY_EXIST(vehicle[veh_ambientB].id)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_ambientB].id)
			ENDIF
			
			REPEAT COUNT_OF(ped) i
				IF i != ped_wade
					IF DOES_ENTITY_EXIST(ped[i].id)
						SET_PED_AS_NO_LONGER_NEEDED(ped[i].id)
					ENDIF
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(vehicle) i
				IF i != veh_player
				and i != veh_player
					IF DOES_ENTITY_EXIST(vehicle[i].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[i].id)
					ENDIF
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(obj) i					
				IF DOES_ENTITY_EXIST(obj[i].id)
					SET_OBJECT_AS_NO_LONGER_NEEDED(obj[i].id)
				ENDIF
			ENDREPEAT
			
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,TRUE)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,TRUE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,TRUE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST,TRUE)
			REPEAT COUNT_OF(rayfireTrailer) i
				IF DOES_PARTICLE_FX_LOOPED_EXIST(rayfireTrailer[i].fx)
					STOP_PARTICLE_FX_LOOPED(rayfireTrailer[i].fx)			
				ENDIF	
			ENDREPEAT
			
			FORCE_ACTION_STATE(25,ACT_FLEEING_PEDS,TRUE,TRUE)
		
			REPEAT COUNT_OF(fleeingPed) i
				IF DOES_ENTITY_EXIST(fleeingPed[i].pedID)
					IF NOT IS_PED_INJURED(fleeingPed[i].pedID)
						SET_PED_KEEP_TASK(fleeingPed[i].pedID,true)															
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(fleeingPed[i].pedID)
				ENDIF
				IF DOES_ENTITY_EXIST(fleeingPed[i].vehID)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(fleeingPed[i].vehID)
				ENDIF
			endrepeat
			
			REMOVE_ANIM_DICT("reaction@male_stand@big_variations@idle_c")
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_M_Salton_01)
			REMOVE_ANIM_SET("move_f@flee@a")
			
			actions[thisI].completed = TRUE
		BREAK
		
		CASE ACT_SET_VEHICLE_RADIO
			IF IS_VEHICLE_SAFE(vehicle[veh_player].id)						
				SET_VEHICLE_LIGHTS(vehicle[veh_player].id,NO_VEHICLE_LIGHT_OVERRIDE)						
				SET_VEHICLE_RADIO_ENABLED(vehicle[veh_player].id,TRUE)
				
			ENDIF
			actions[thisI].completed = TRUE
		BREAK
		
		CASE ACT_IN_CAR_SHOVE_ANIM
			BOOL bInInteriorCam
			bInInteriorCam = FALSE
			CAM_VIEW_MODE_CONTEXT activeViewModeContext
			CAM_VIEW_MODE activeViewMode
			activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
			IF activeViewModeContext != CAM_VIEW_MODE_CONTEXT_ON_FOOT
			  	activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)
			  	IF activeViewMode = CAM_VIEW_MODE_FIRST_PERSON
					bInInteriorCam = TRUE
				ENDIF
			ENDIF						
		
			SWITCH actions[thisI].flag
				case 0							
					REQUEST_ANIM_DICT("misstrevor3ig_7")
					actions[thisI].flag++
				BREAK
				CASE 1
					IF HAS_ANIM_DICT_LOADED("misstrevor3ig_7")
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 2
					IF IS_CONV_ROOT_PLAYING("TRV3_DR3")
					OR IS_CONV_ROOT_PLAYING("TRV3_DR4")
						actions[thisI].flag++									
					ENDIF
				BREAK
				case 3
					IF IS_CONDITION_TRUE(COND_WADE_AND_PLAYER_IN_VEHICLE)
						
						IF NOT IS_PED_FUCKED(ped[ped_wade].id)
							IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
								IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[veh_player].id)
									IF GET_PED_VEHICLE_SEAT(ped[ped_wade].id,vehicle[veh_player].id) = VS_FRONT_RIGHT
										IF GET_ENTITY_MODEL(vehicle[veh_player].id) = BODHI2
											IF NOT bInInteriorCam												
												TASK_PLAY_ANIM(ped[ped_wade].id,"misstrevor3ig_7","shove_wade") 
												TASK_PLAY_ANIM(player_ped_id(),"misstrevor3ig_7","shove_trev",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
											ELSE
												actions[thisI].completed = TRUE
											ENDIF
										ELSE
											actions[thisI].completed = TRUE
										ENDIF
									ELSE
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF
							ENDIF
							actions[thisI].flag++					
						ENDIF
					ENDIF
				BREAK
				CASE 4
					IF IS_ENTITY_PLAYING_ANIM(player_ped_id(),"misstrevor3ig_7","shove_trev")
						actions[thisI].flag++
					ENDIF
				break
				case 5
					IF bInInteriorCam
					OR NOT IS_CONDITION_TRUE(COND_WADE_AND_PLAYER_IN_VEHICLE)
						IF NOT IS_PED_FUCKED(ped[ped_wade].id)
							CLEAR_PED_TASKS(ped[ped_wade].id)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
					ENDIF
							
					IF NOT IS_ENTITY_PLAYING_ANIM(player_ped_id(),"misstrevor3ig_7","shove_trev")
						REMOVE_ANIM_DICT("misstrevor3ig_7")
						actions[thisI].completed = TRUE
					ENDIF							
				BREAK
			endswitch
		BREAK
		
		CASE ACT_LOAD_CUTSCENE
			SWITCH actions[thisI].flag
				case 0
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1351.1,727.5,187.5 >>) < DEFAULT_CUTSCENE_LOAD_DIST
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("trv_dri_mcs_concat", CS_SECTION_2)	
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 1
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
	     				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())	
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 2
					IF IS_CUTSCENE_PLAYING()
						actions[thisI].completed = TRUE
					ELSE
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1351.1,727.5,187.5 >>) > DEFAULT_CUTSCENE_LOAD_DIST + 100
							
							REMOVE_CUTSCENE()
							//cprintln(debug_Trevor3,"Remove cutscene d")
							actions[thisI].flag=0
						ENDIF
					ENDIF
				BREAK						
			ENDSWITCH
			
		BREAK
		
		CASE ACT_STOP_VEHICLE
			SWITCH actions[thisI].flag
				case 0
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						vehicle[veh_player].id = GET_VEHICLE_PED_IS_USING(player_ped_id())
						SET_ENTITY_AS_MISSION_ENTITY(vehicle[veh_player].id, TRUE, TRUE)
						actions[thisI].flag++		
					ELSE
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),6.0,2)									
							actions[thisI].completed = TRUE
						ENDIF
					ELSE
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
				CASE 2
					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_STOP_VEHICLE_PLAYER_EXIT
		
				SWITCH actions[thisI].flag
					case 0
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
							vehicle[veh_player].id = GET_VEHICLE_PED_IS_USING(player_ped_id())
							SET_ENTITY_AS_MISSION_ENTITY(vehicle[veh_player].id, TRUE, TRUE)
							actions[thisI].flag++		
						ELSE
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					CASE 1
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),2.0,2)
								TASK_LEAVE_ANY_VEHICLE(player_ped_id())										
								actions[thisI].flag++
							ENDIF
						ELSE
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					CASE 2
						IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
				ENDSWITCH
		
		BREAK
		CASE ACT_LS_VIEW_LEADIN
			SWITCH actions[thisI].flag

				CASE 0
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())														
							actions[thisI].flag = 1000								
						ELSE
							REQUEST_MODEL(PROP_LD_TEST_01)
							actions[thisI].flag++
						ENDIF
				BREAK
				CASE 1
					IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 2
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-1350.1467, 725.3141, 185.3831>>) < 12
						SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),pedmove_walk)
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 3
					SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),pedmove_walk)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-1350.1467, 725.3141, 185.3831>>) > 14								
						actions[thisI].flag = 0
					ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-1350.1467, 725.3141, 185.3831>>) < 6.000							
						actions[thisI].flag++
						actions[thisI].intA = GET_GAME_TIMER() + 1000
						SET_PLAYER_CONTROL(player_id(),FALSE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(player_ped_id(),<<-1350.1467, 725.3141, 185.3831>>,pedmove_walk,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,219)
					ENDIF
				BREAK
				CASE 4
				
						IF GET_CAM_ACTIVE_VIEW_MODE_CONTEXT() = CAM_VIEW_MODE_CONTEXT_ON_FOOT
							IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
								ENDIF
							ENDIF
						ENDIF
				
						float fPlayerHeading
						fPlayerHeading = GET_ENTITY_HEADING(player_ped_id())
						//cprintln(debug_trevor3,"heading: ",fPlayerHeading)
						IF fPlayerHeading > 214 AND fPlayerHeading < 294
							vStartPan = GET_ENTITY_COORDS(player_ped_id()) + <<-1000.0 * sin(250),1000.0 * cos(250),0>>
							vEndPan = GET_ENTITY_COORDS(player_ped_id()) + <<-1000.0 * sin(210),1000.0 * cos(210),0>>
							trackObj = CREATE_OBJECT(PROP_LD_TEST_01,vStartPan)
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.78)
							SET_CONDITION_STATE(COND_LS_LEADIN_OK,TRUE)
						ELIF fPlayerHeading < 214 AND fPlayerHeading > 125					
				
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( 0.15)
							vStartPan = GET_ENTITY_COORDS(player_ped_id()) + <<-1000.0 * sin(184),1000.0 * cos(184),0>>
							vEndPan = GET_ENTITY_COORDS(player_ped_id()) + <<-1000.0 * sin(224),1000.0 * cos(224),0>>
							trackObj = CREATE_OBJECT(PROP_LD_TEST_01,vStartPan)
							SET_CONDITION_STATE(COND_LS_LEADIN_OK,TRUE)
						ELSE
							actions[thisI].flag = 1000	
						ENDIF
						cprintln(debug_Trevor3,"hint cam: ",actions[thisI].flag)
						
						IF actions[thisI].flag != 1000	
							STOP_GAMEPLAY_HINT(true)
							SET_GAMEPLAY_ENTITY_HINT(trackObj,<<0,0,0>>,TRUE, -1, 5500)
				            SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.375)					            				                        
				            SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(-0.113)					                        
				            SET_GAMEPLAY_HINT_FOV(40)
							TASK_LOOK_AT_ENTITY(player_ped_id(),trackObj,-1)
							
							actions[thisI].intA = GET_GAME_TIMER() + 8000
							actions[thisI].floatA = 0.0
							actions[thisI].flag++	
						ENDIF
				
				BREAK
				CASE 5			
						vector vMoveTo
						actions[thisI].floatA += timestep() / 8
						IF actions[thisI].floatA > 1.0
							actions[thisI].floatA = 1.0
						ENDIF
						//cprintln(debug_trevor3,"Phase: ",actions[thisI].floatA," start: ",vStartPan," end: ",vEndPan)
						vMoveTo = vStartPan + ((vEndPan-vStartPan) * actions[thisI].floatA)
					//	DRAW_DEBUG_SPHERE(vMoveTo,15)
						SET_ENTITY_COORDS(trackObj,vMoveTo)						
						
						IF GET_GAME_TIMER() > actions[thisI].intA
							
							SET_PLAYER_CONTROL(player_id(),TRUE)
							delete_object(trackObj)
							actions[thisI].completed = TRUE
						ENDIF							
				BREAK
				
				case 1000 //player entered trigger zone messily
					REMOVE_CUTSCENE()
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("trv_dri_mcs_concat", CS_SECTION_1 | CS_SECTION_2)
					actions[thisI].flag++	
				break
				case 1001
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
	     				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())	
						actions[thisI].flag++
					ENDIF
				BREAK
				case 1002
					IF HAS_CUTSCENE_LOADED()
						actions[thisI].completed = TRUE
					ENDIF
				break
			ENDSWITCH
					
		BREAK
		
		CASE ACT_PLAY_LS_CUTSCENE
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(2.0)
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
			IF NOT DOES_ENTITY_EXIST(cutsceneChopper)
				CPRINTLN(debug_Trevor3,"NO chopper")
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Paper_Car"))					
					CPRINTLN(debug_Trevor3,"Chopper exists B")
					cutsceneChopper = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Paper_Car"))					
					
					chopperPed = CREATE_PED_INSIDE_VEHICLE(cutsceneChopper,PEDTYPE_MISSION,IG_WADE)					
					SET_VEHICLE_ENGINE_ON(cutsceneChopper,true,true)
					SET_VEHICLE_SEARCHLIGHT(cutsceneChopper,true,true)
					TASK_VEHICLE_AIM_AT_COORD(chopperPed,<<-1115,168,60>>)					
					actions[thisI].intB = GET_GAME_TIMER() + 10000
				ENDIF	
			ELSE
				IF actions[thisI].intB != 0
					IF GET_GAME_TIMER() > actions[thisI].intB
						IF NOT IS_PED_INJUREd(chopperPed)
							TASK_VEHICLE_AIM_AT_COORD(chopperPed,<<-815,68,60>>)
						ENDIF
						actions[thisI].intB = 0
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH actions[thisI].flag
				case 0
					IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_DRIVE_TO_LS_DIALOGUE")
						STOP_AUDIO_SCENE("TREVOR_3_DRIVE_TO_LS_DIALOGUE")	
					ENDIF
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()	
						
						REGISTER_ENTITY_FOR_CUTSCENE(cutsceneChopper,"Paper_Car",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,MAVERICK)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(8)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						//restartRoot = ""
						//restartLine = ""
						restartSubtitleLine = ""
						restartSubtitleRoot = ""
				
						bLastConvoWithoutSubtitles = FALSE
						
						REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0)
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						START_CUTSCENE()
						
						
						
						missionProgress = STAGE_LS_VIEW_CUT
						 actions[thisI].flag++
					ENDIF
				BREAK
				
				cASE 1
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[veh_player].id,<<-1349.9064, 740.0016, 183.4498>>) < 25
							
							//B*1988309
							IF GET_ENTITY_MODEL(vehicle[veh_player].id) = TOWTRUCK 
							OR GET_ENTITY_MODEL(vehicle[veh_player].id) = TOWTRUCK2
								IF GET_ENTITY_ATTACHED_TO_TOW_TRUCK(vehicle[veh_player].id) != null
									DETACH_VEHICLE_FROM_TOW_TRUCK(vehicle[veh_player].id, GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(vehicle[veh_player].id)))
									SET_VEHICLE_TOW_TRUCK_ARM_POSITION(vehicle[veh_player].id, 1)
								ENDIF
							ENDIF
							SET_ENTITY_COORDS(vehicle[veh_player].id,<<-1349.9064, 740.0016, 183.4498>>)
							SET_ENTITY_HEADING(vehicle[veh_player].id,101)
							SET_VEHICLE_ENGINE_HEALTH(vehicle[veh_player].id,1000)
						ENDIF
					ENDIF		
					
					//start beam
				//	IF IS_VEHICLE_DRIVEABLE(cutsceneChopper)
				//		CPRINTLN(debug_trevor3,"SPOTLIGHT ON")
				//		SET_VEHICLE_SEARCHLIGHT(cutsceneChopper,true,true)
				//	ENDIF
					
					SET_FRONTEND_RADIO_ACTIVE(false)
					CLEAR_AREA(<<-1349.4404, 738.9042, 183.5858>>,6,TRUE)
					CLEAR_AREA(<<-1345.9774, 729.1130, 184.8326>>,7,TRUE)
					STOP_GAMEPLAY_HINT(true)
				
					REMOVE_PED_HELMET(PLAYER_PED_ID(),TRUE)
					SET_PED_HELMET_FLAG(player_ped_id(),PV_FLAG_NONE)
				
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(Locates_Data)
					CLEAR_MISSION_LOCATE_STUFF(locates_data,true)

					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					actions[thisI].flag++
					actions[thisI].intA = 0
				BREAK
					CASE 2
			
					IF CAN_SET_EXIT_STATE_FOR_CAMERA()
						IF NOT IS_BIT_SET(actions[thisI].intA,0)
							SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)																				
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-5)
							
						
							FORCE_DIALOGUE_STATE(3,DIA_WADE_COMMENTS_ON_PLAYER_LEAVING,false)
							
							REPLAY_STOP_EVENT()
							REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0)
							
							missionProgress = STAGE_DRIVE_TO_WADES_COUSIN
							SET_BIT(actions[thisI].intA,0)
						ENDIF
					ENDIF
				
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor") //OR HAS_CUTSCENE_FINISHED() //for shit skip to this stage									
						IF NOT IS_BIT_SET(actions[thisI].intA,1)
							//IF NOT IS_CUTSCENE_PLAYING()								
							//	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK)
							//ELSE									
							//	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)									
							//ENDIF
							
							SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE) 
		                    SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE) 
							IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
								 FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING, FALSE, FAUS_CUTSCENE_EXIT)
								  SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500)
							ELSE
								 FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
								 SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1500)
							ENDIF	                    												 	                   
							
							
							SET_BIT(actions[thisI].intA,1)
						ENDIF
					ENDIF //commenting out because I can't find the bug that made me make this change, which I did some time between 8/1/13 and 10/1/13. Got bug 968304
					
					cprintln(debug_trevor3,"Check delete chopper")
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Paper_Car")						
						IF NOT IS_BIT_SET(actions[thisI].intA,2)
							cprintln(debug_trevor3,"chopper exits")
							IF IS_VEHICLE_DRIVEABLE(cutsceneChopper)
								cprintln(debug_trevor3,"delete chopper")
								IF DOES_ENTITY_EXIST(chopperPed)
									delete_ped(chopperPed)
								ENDIF
							
								DELETE_VEHICLE(cutsceneChopper)
								
								SET_FRONTEND_RADIO_ACTIVE(TRUE)
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
								actions[thisI].completed = TRUE

							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
			/*	CASE 4
					//cprintln(debug_trevor3,"timer = ",get_game_timer()," end time = ",actions[thisI].intA)
					IF GET_GAME_TIMER() < actions[thisI].intA							
						SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 0500)
					ELSE
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
						actions[thisI].completed = TRUE
					ENDIF
				BREAK*/
			ENDSWITCH
			
			IF NOT IS_PED_FUCKED(ped[ped_wade].id)
				IF IS_ENTITY_ON_FIRE(ped[ped_wade].id)
					STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(ped[ped_wade].id),5.0)													
				ENDIF
				IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
					IF NOT IS_PED_IN_VEHICLE(ped[ped_wade].id,vehicle[veh_player].id)
						IF IS_VEHICLE_SEAT_FREE(vehicle[veh_player].id,VS_DRIVER)
						AND GET_DISTANCE_BETWEEN_ENTITIES(vehicle[veh_player].id,ped[ped_wade].id) < 10.0								
							SET_PED_INTO_VEHICLE(ped[ped_wade].id,vehicle[veh_player].id,VS_FRONT_RIGHT)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE ACT_MAKE_PLAYER_AND_BUDDY_WALK
			
			IF missionProgress = STAGE_GET_TO_TRAILER_PARK		
				
				IF actions[thisI].flag = 0					
					actions[thisI].flag = 1														
				ENDIF	
			ENDIF
			
			IF NOT IS_PED_INJURED(ped[ped_wade].id)						
				SWITCH actions[thisI].flag
					case 0
						IF IS_CUTSCENE_PLAYING()
							actions[thisI].flag++
						ENDIF
					BREAK
					CASE 1
						vector vLastCarCoord
						IF GET_LAST_DRIVEN_VEHICLE() != null
							IF DOES_ENTITY_EXIST(GET_LAST_DRIVEN_VEHICLE())		
								IF IS_VEHICLE_DRIVEABLE(GET_LAST_DRIVEN_VEHICLE())									
									
									vLastCarCoord = GET_ENTITY_COORDS(GET_LAST_DRIVEN_VEHICLE())
								ENDIF
							ENDIF
						ENDIF
						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Wade",GET_ENTITY_MODEL(ped[PED_wade].id))
						OR NOT IS_CUTSCENE_PLAYING()
							CPRINTLN(debug_trevor3,"Wade exit ")
							IF NOT IS_VECTOR_ZERO(vLastCarCoord)
								CPRINTLN(debug_trevor3,"check car in area ")
								IF IS_POINT_IN_ANGLED_AREA(vLastCarCoord,<<1963.927734,3789.891113,30.252787>>, <<2002.270142,3813.016602,36.092949>>, 19.562500)
									CPRINTLN(debug_trevor3,"car in area ")
									SET_ENTITY_HEADING(PLAYER_PED_ID(),260)
								ENDIF
							ENDIF
							FORCE_PED_MOTION_STATE(ped[PED_wade].id,MS_ON_FOOT_WALK,FALSE,FAUS_DEFAULT)																		
							SET_PED_MIN_MOVE_BLEND_RATIO(ped[PED_wade].id,PEDMOVE_WALK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[PED_wade].id,TRUE)
							
							actions[thisI].intA = GET_GAME_TIMER() + 1000														
							actions[thisI].intB++
						ENDIF
						
						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor",GET_ENTITY_MODEL(player_ped_id()))									
						OR NOT IS_CUTSCENE_PLAYING()
							CPRINTLN(debug_trevor3,"Player exit ")
							IF NOT IS_VECTOR_ZERO(vLastCarCoord)
								CPRINTLN(debug_trevor3,"Last car was found")
								IF IS_POINT_IN_ANGLED_AREA(vLastCarCoord,<<1963.927734,3789.891113,30.252787>>, <<2002.270142,3813.016602,36.092949>>, 19.562500)
									CPRINTLN(debug_trevor3,"set wade heading")
									SET_ENTITY_HEADING(player_ped_id(),260)
								ENDIF
							ENDIF
						
                           SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE) 
		                    SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE) 
							IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
								 FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING, FALSE)
							ELSE
								 FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE)
							ENDIF
		                    
		                    SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
							
							cprintln(debug_trevor3,"FORCE WALK ON PLAYER B")
							
							actions[thisI].completed = TRUE
                              
						ENDIF
					BREAK
					
				ENDSWITCH
				
			ENDIF
			
		BREAK

		CASE ACT_LOAD_WADE_COUSIN_CUTSCENE
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1152.7819, -1523.9880, 3.5198 >>) > DEFAULT_CUTSCENE_LOAD_DIST + 100
			AND missionProgress != STAGE_FINAL_CUT
				IF actions[thisI].flag < 5
				AND actions[thisI].flag > 0
					actions[thisI].flag = 5
				ENDIF
			ENDIF
			
			
			
			SWITCH actions[thisI].flag
				case 0
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1152.7819, -1523.9880, 3.5198 >>) < DEFAULT_CUTSCENE_LOAD_DIST
						REQUEST_CUTSCENE("TRV_DRI_EXT")
						//cprintln(debug_Trevor3,"Request cutscene")
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 1
					int_wade = GET_INTERIOR_AT_COORDS(<< -1154.8173, -1518.3027, 9.6345 >>)			
					IF IS_VALID_INTERIOR(int_wade)
						PIN_INTERIOR_IN_MEMORY(int_wade)
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 2
					IF IS_INTERIOR_READY(int_wade)
						SET_INTERIOR_ACTIVE(int_wade, TRUE)		
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 3
					IF HAS_CUTSCENE_LOADED()
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 4
					IF IS_CUTSCENE_PLAYING()
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
				CASE 5
					REMOVE_CUTSCENE()
					
					//cprintln(debug_Trevor3,"Remove cutscene a")
					IF IS_INTERIOR_READY(int_wade)
						UNPIN_INTERIOR(int_wade)
					ENDIF
					actions[thisI].flag=0
				BREAK
				
			ENDSWITCH
			
			IF actions[thisI].flag >= 0
			AND actions[thisI].flag <= 4
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()							
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)		
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Wade",ped[ped_wade].id)									
					ENDIF
				ENDIF
			ENDIF
			
			
		BREAK
		
		CASE ACT_STOP_VEHICLE_DOOR_GETTING_KNOCKED_OFF																									
			IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
				IF GET_VEHICLE_PED_IS_IN(player_ped_id()) != vehicle[veh_player].id
					IF DOES_ENTITY_EXIST(vehicle[veh_player].id)
						//IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicle[veh_player].id)
						SET_ENTITY_AS_MISSION_ENTITY(vehicle[veh_player].id,TRUE,TRUE)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_player].id)
					ENDIF
					vehicle[veh_player].id = GET_VEHICLE_PED_IS_IN(player_ped_id())		
					
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehicle[veh_player].id, SC_DOOR_FRONT_LEFT,FALSE)
				ENDIF
			ENDIF						
		BREAK
		
		CASE ACT_PREVENT_WADE_LEAVING_BURNING_VEHICLE
			IF NOT IS_PED_FUCKED(ped[ped_wade].id)
				SET_PED_CONFIG_FLAG(ped[ped_wade].id,PCF_GetOutBurningVehicle,FALSE)
				SET_ENTITY_PROOFS(ped[ped_wade].id,false,true,false,false,false)
				actions[thisI].completed = TRUE
			ENDIF
		BREAK
		
		CASE ACT_ESTABLISHING_SHOT_AT_WADES
			
			IF NOT IS_PED_FUCKED(ped[ped_wade].id)
				SWITCH actions[thisI].flag
					CASE 0	
						IF IS_ACTION_COMPLETE(10,ACT_STOP_VEHICLE)
						AND NOT IS_CONV_ROOT_PLAYING("TRV3_END")
							//REQUEST_CUTSCENE("TRV_DRI_EXT")
							FORCE_DIALOGUE_STATE(7,DIA_WADE_GIVES_DIRECTIONS)
							REQUEST_ANIM_DICT("misstrevor3leadinout")	
					
							DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					
							CLEAR_AREA_OF_VEHICLES(<<-1134.3741, -1513.4758, 3.4483>>,15)
							SET_ENTITY_COORDS(ped[ped_wade].id,<<-1148.0438, -1520.9515, 4.1386>>)
							SET_ENTITY_HEADING(ped[ped_wade].id,300)
							FORCE_PED_MOTION_STATE(ped[ped_wade].id,MS_ON_FOOT_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(ped[ped_wade].id,<<-1149.8651, -1523.0471, 9.6329>>,pedmove_walk,-1,0.2,ENAV_DEFAULT,41)
							IF DOES_CAM_EXIST(script_cam)
								DESTROY_CAM(script_cam)
							ENDIF
							script_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
							ADD_CAM_SPLINE_NODE(script_cam,<<-1133.8,-1514.9,6.2>>, <<11.4,0,113.5>>,6500)
							ADD_CAM_SPLINE_NODE(script_cam,<<-1133.7,-1514.8,6.9>>, <<11.2,0,114.5>>,8500)
							
							CLEAR_AREA_OF_VEHICLES(<<-1133.8,-1514.9,6.2>>,15)
							SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
													
							SET_CAM_SPLINE_SMOOTHING_STYLE(script_cam,CAM_SPLINE_NO_SMOOTH)

							SET_CAM_FOV(script_cam,40)	
							RENDER_SCRIPT_CAMS(true,false)															
							SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(9)
							SET_FRONTEND_RADIO_ACTIVE(FALSE)	
							actions[thisI].flag++
							actions[thisI].intA = GET_GAME_TIMER() + 6000
							
						ENDIF
					BREAK
					CASE 1		
						DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						IF GET_GAME_TIMER() > actions[thisI].intA
						AND HAS_ANIM_DICT_LOADED("misstrevor3leadinout")
						
							IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
								ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
								PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							ENDIF
							SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_UNARMED,true)
							
							RENDER_SCRIPT_CAMS(FALSE,FALSE)
							
							if DOES_CAM_EXIST(script_cam)
								DESTROY_CAM(script_cam)
							endif
							
							IF NOT IS_PED_FUCKED(ped[ped_wade].id)
								SET_ENTITY_COORDS(ped[ped_wade].id,<<-1149.8651, -1523.0471, 9.6329>>)
								SET_ENTITY_HEADING(ped[ped_wade].id,41)
								TASK_PLAY_ANIM(ped[ped_wade].id,"misstrevor3leadinout","trv_drive_ext_leadin_door_loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
								//SET_PED_HELMET(ped[ped_wade].id,false)
								REMOVE_PED_HELMET(ped[ped_wade].id,true)
							ENDIF
							
							TASK_LEAVE_ANY_VEHICLE(player_ped_id())		
							SET_PLAYER_CONTROL(PLAYER_ID(),true)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					

					
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE ACT_WADE_PLAYS_SHOUT_ANIM	
			IF IS_CONV_ROOT_PLAYING("TRV3_DRI")												
				TASK_PLAY_ANIM(ped[ped_wade].id,"misstrevor3leadinout","trv_drive_ext_leadin_door_shout",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
				actions[thisI].completed = TRUE									
			ENDIF					
		BREAK
		
		CASE ACT_BLOCK_PLAYER_FOR_LEAD_IN
			SWITCH actions[thisI].flag
				CASE 0
					BLOCK_PLAYER_FOR_LEAD_IN(false)
					SET_PLAYER_CONTROL(player_id(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
					TASK_FOLLOW_NAV_MESH_TO_COORD(player_ped_id(),<<-1149.2781, -1522.2864, 9.6331>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_NO_STOPPING)
					//FORCE_PED_MOTION_STATE(player_ped_id(),MS_ON_FOOT_WALK,FALSE,FAUS_DEFAULT,TRUE)
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(player_ped_id(),TRUE)
					actions[thisI].flag++
				BREAK
				CASE 1
					UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(false)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_MUSIC_IN_VEHICLE_GOING_TO_LS
			SWITCH actions[thisI].flag
				CASE 0
					IF NOT IS_PED_INJURED(ped[ped_wade].id)
						IF IS_PED_IN_ANY_VEHICLE(ped[ped_wade].id)
							actions[thisI].flag++
						ELSE	
							IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
								actions[thisI].flag++
							ELSE
								PLAYMUSIC("TRV3_MUSIC_END",FALSE,"")
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF NOT IS_PED_INJURED(ped[ped_wade].id)
						IF NOT IS_PED_IN_ANY_VEHICLE(ped[ped_wade].id)
						AND NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
							PLAYMUSIC("TRV3_MUSIC_END",FALSE,"")
							actions[thisI].completed = TRUE
						ENDIF
						
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())										
							IF DOES_PLAYER_VEH_HAVE_RADIO()
								vehicle_index aVeh
								aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
								IF GET_IS_VEHICLE_ENGINE_RUNNING(aVeh)
									actions[thisI].flag++
									actions[thisI].intA = GET_GAME_TIMER() + 3000
										
								ENDIF
							ELSE
								PLAYMUSIC("TRV3_MUSIC_END",FALSE,"")
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 2
					IF GET_GAME_TIMER() > actions[thisI].intA
						PLAYMUSIC("TRV3_RADIO_TRUCK",TRUE,"")
						actions[thisI].completed = TRUE	
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_AUDIO_SCENE_DRIVE_TO_LS
			SWITCH actions[thisI].flag
				CASE 0
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						STOP_AUDIO_SCENE("TREVOR_3_ESCAPE_TO_CAR")
						START_AUDIO_SCENE("TREVOR_3_DRIVE_TO_LS_DIALOGUE")													
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_AUDIO_SCENE_DRIVE_TO_WADES_COUSIN
			SWITCH actions[thisI].flag
				CASE 0
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						START_AUDIO_SCENE("TREVOR_3_DRIVE_TO_FLOYDS")					
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 1
					IF IS_CONDITION_TRUE(COND_AT_WADES_COUSIN)
						IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_DRIVE_TO_FLOYDS")
							STOP_AUDIO_SCENE("TREVOR_3_DRIVE_TO_FLOYDS")
						ENDIF
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_PREP_FRUSTUM_AT_WADES_COUSIN
			
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1152.7819, -1523.9880, 3.5198 >>) < 80.0
				IF NOT IS_STREAMVOL_ACTIVE()
				
					trailer_frustum = STREAMVOL_CREATE_FRUSTUM(<<-1133.8,-1514.9,6.2>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<11.4,0,113.5>>) ,20,FLAG_MAPDATA,STREAMVOL_LOD_FLAG_HIGH)
				ENDIF
			ELSE	
				IF IS_STREAMVOL_ACTIVE()
					STREAMVOL_DELETE(trailer_frustum)
				ENDIF
			ENDIF
		BREAK
		
		CASE ACT_SLOW_DOWN_PLAYER
			IF IS_ACTION_COMPLETE(8,ACT_PLAY_LS_CUTSCENE)
				actions[thisI].completed = TRUE
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(player_ped_id())		
					vehicle_index aVeh
					aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
					IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(aVeh))
					AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(aVeh))
						float fDist,fMulti,fThisSpeed
						fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(aVEh,<<-1350.1150, 729.2382, 184.7668>>)
						fmulti = (250 - fDist)
						IF fmulti < 0
							fMulti = 0
						endif
						
						fmulti /= 250
						fmulti *= 1000
						
						fThisSpeed = GET_ENTITY_SPEED(aVeh)
						IF fThisSpeed > 22
							APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(aVeh,apply_type_impulse,<<0,-fmulti,0>>,0,true,false)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
							
		CASE ACT_FLEEING_PEDS
			SWITCH actions[thisI].flag
				 CASE 0 
				 	REQUEST_ANIM_DICT("reaction@male_stand@big_variations@idle_c")
					REQUEST_MODEL(A_F_M_Salton_01)		
					REQUEST_MODEL(HEXER)
					REQUEST_ANIM_SET("move_f@flee@a")
					
					fleeingPed[0].spawnAt = <<100.1487, 3668.9001, 38.7549>>
					fleeingPed[0].target = <<71.9131, 3695.3372, 39.6061>>
					fleeingPed[0].panicAction = panic_point_at_trailer2
					
					fleeingPed[1].spawnAt = <<18.9311, 3661.2852, 38.8146>>
					fleeingPed[1].target = <<91.4470, 3590.4426, 38.7550>>
					fleeingPed[1].panicAction = panic_flee
					
					fleeingPed[2].spawnAt = <<65.1302, 3681.3906, 38.8345>>
					fleeingPed[2].target = <<58.3865, 3583.4180, 38.6780>>
					fleeingPed[2].panicAction = panic_flee
					
					fleeingPed[3].spawnAt = <<6.8100, 3701.0938, 38.5752>>
					fleeingPed[3].target = <<39.2396, 3703.7141, 39.5151>>
					fleeingPed[3].panicAction = panic_point_at_trailer3
					
					fleeingPed[4].spawnAt = <<95.5319, 3742.6921, 38.6829>>
					fleeingPed[4].target = <<107.2796, 3519.9431, 38.7924>>
					fleeingPed[4].panicAction = panic_drive_off_bike
					
					fleeingPed[5].spawnAt = <<11.4552, 3702.3096, 38.6405>>
					fleeingPed[5].target = <<-32.4513, 3605.8613, 42.4539>>
					fleeingPed[5].panicAction = panic_drive_off_bike
					
					fleeingPed[6].spawnAt = <<99.4489, 3636.0779, 38.7549>>
					fleeingPed[6].target = <<31.4033, 3667.5022, 39.4457>>
					fleeingPed[6].panicAction = panic_point_at_trailer4
					
					fleeingPed[7].spawnAt = <<28.5668, 3730.8567, 38.6138>>
					fleeingPed[7].target = <<45.4471, 3588.1135, 38.6882>>
					fleeingPed[7].panicAction = panic_flee
					
					fleeingPed[8].spawnAt = <<93.1663, 3726.8423, 38.5199>>
					fleeingPed[8].target = <<85.9274, 3607.3198, 38.8282>>
					fleeingPed[8].panicAction = panic_flee
					
					fleeingPed[9].spawnAt = <<36.5757, 3639.2393, 38.8501>>
					fleeingPed[9].target =  <<46.3355, 3744.5891, 39.0855>>
					fleeingPed[9].panicAction = panic_point_at_trailer1
					

					actions[thisI].flag++
				BREAK
				CASE 1
					IF HAS_ANIM_DICT_LOADED("reaction@male_stand@big_variations@idle_c")
					AND HAS_MODEL_LOADED(A_F_M_Salton_01)
					AND HAS_ANIM_SET_LOADED("move_f@flee@a")
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 2
					int iv
					REPEAT COUNT_OF(fleeingPed) iv
						IF NOT DOES_ENTITY_EXIST(fleeingPed[iv].pedID)
							IF actions[thisI].intA < 20
								IF NOT IS_POINT_VISIBLE(fleeingPed[iv].spawnAt,10)
									fleeingPed[iv].pedID = CREATE_PED(PEDTYPE_MISSION,A_F_M_Salton_01,fleeingPed[iv].spawnAt)
									SET_PED_RELATIONSHIP_GROUP_HASH(fleeingPed[iv].pedID,relGroupFoughtBiker)
									SET_PED_MOVEMENT_CLIPSET(fleeingPed[iv].pedID,"move_f@flee@a")
									SET_PED_HEARING_RANGE(fleeingPed[iv].pedID,20)
									
									//set variations
									SWITCH iv
										CASE 0										
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
										BREAK
										
										CASE 1
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 1, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 1, 1, 0) //(feet)
										BREAK
										
										CASE 2
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
										BREAK
										
										CASE 3
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
										BREAK
										
										CASE 4
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 2, 0, 0) //(feet)
										BREAK
										
										CASE 5
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
										BREAK
										
										CASE 6
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
										BREAK
										
										CASE 7
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
										BREAK
										
										CASE 8
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 2, 0, 0) //(feet)
										BREAK
										
										CASE 9
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 1, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(fleeingPed[iv].pedID, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
										BREAK
									ENDSWITCH
									
									actions[thisI].intA++
									SWITCH fleeingPed[iv].panicAction
										CASE panic_point_at_trailer1
										FALLTHRU
										CASE panic_point_at_trailer2
										FALLTHRU
										CASE panic_point_at_trailer3
										FALLTHRU
										CASE panic_point_at_trailer4
										
											int trailerToLookAt
											SWITCH fleeingPed[iv].panicAction
												CASE panic_point_at_trailer1 trailerToLookAt = 1 BREAK
												CASE panic_point_at_trailer2 trailerToLookAt = 2 BREAK
												CASE panic_point_at_trailer3 trailerToLookAt = 3 BREAK
												CASE panic_point_at_trailer4 trailerToLookAt = 4 BREAK
											ENDSWITCH
											
										
											startSeq()
												
												TASK_FOLLOW_NAV_MESH_TO_COORD(null,fleeingPed[iv].target,pedmove_run,DEFAULT_TIME_NEVER_WARP,7)
												IF NOT DOES_BLIP_EXIST(rayfireTrailer[trailerToLookAt].blip)
													TASK_TURN_PED_TO_FACE_COORD(null,fleeingPed[iv].target,2000)
													TASK_PLAY_ANIM(null,"reaction@male_stand@big_variations@idle_c","react_big_variations_s")													
													TASK_PLAY_ANIM(null,"reaction@male_stand@big_variations@idle_c","react_big_variations_p")
												ENDIF
												TASK_SMART_FLEE_PED(null,player_ped_id(),1500,-1)	
											endseq(fleeingPed[iv].pedID)
											
										BREAK
										CASE panic_flee
											startSeq()
												TASK_FOLLOW_NAV_MESH_TO_COORD(null,fleeingPed[iv].target,pedmove_run,DEFAULT_TIME_NEVER_WARP,15)
												TASK_SMART_FLEE_PED(null,player_ped_id(),200,-1)
												TASK_COWER(null,-1)
											endseq(fleeingPed[iv].pedID)
										BREAK
										CASE panic_run_and_cower
											startSeq()
												TASK_FOLLOW_NAV_MESH_TO_COORD(null,fleeingPed[iv].target,pedmove_run,DEFAULT_TIME_NEVER_WARP,2)
												TASK_COWER(null,-1)
											endseq(fleeingPed[iv].pedID)
										BREAK
										CASE panic_drive_off_bike
											fleeingPed[iv].vehID = CREATE_VEHICLE(HEXER,fleeingPed[iv].spawnAt,180)
											SET_PED_INTO_VEHICLE(fleeingPed[iv].pedID,fleeingPed[iv].vehID)
											startSeq()
												TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
												TASK_VEHICLE_DRIVE_TO_COORD(null,fleeingPed[iv].vehID,fleeingPed[iv].target,15,DRIVINGSTYLE_NORMAL,DUMMY_MODEL_FOR_SCRIPT,DRIVINGMODE_AVOIDCARS_RECKLESS,10,5)
												TASK_SMART_FLEE_PED(null,player_ped_id(),1500,-1)												
											endseq(fleeingPed[iv].pedID)
										BREAK
										CASE panic_drive_off_quad
										BREAK
									ENDSWITCH
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_INJURED(fleeingPed[iv].pedID)
								IF (GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),fleeingPed[iv].pedID) > 120 AND fleeingPed[iv].panicAction != panic_drive_off_bike)
								OR (GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),fleeingPed[iv].pedID) > 200 AND fleeingPed[iv].panicAction = panic_drive_off_bike)
									SET_PED_KEEP_TASK(fleeingPed[iv].pedID,TRUE)
									SET_PED_AS_NO_LONGER_NEEDED(fleeingPed[iv].pedID)
									IF DOES_ENTITY_EXIST(fleeingPed[iv].vehID)
										SET_VEHICLE_AS_NO_LONGER_NEEDED(fleeingPed[iv].vehID)
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAt
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_DO_END_CUTSCENE
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()							
				IF NOT IS_PED_FUCKED(ped[ped_wade].id)		
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Wade",ped[ped_wade].id)									
				ENDIF
			ENDIF
			

		
			SWITCH actions[thisI].flag
				CASE 0	

					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(10)
						KILL_ANY_CONVERSATION()
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						IF NOT IS_PED_FUCKED(ped[ped_wade].id)
							REGISTER_ENTITY_FOR_CUTSCENE(ped[ped_wade].id,"Wade",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						//	SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Wade",ped[ped_wade].id)
						ENDIF
						/*
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
							vehicle[veh_player].id = GET_VEHICLE_PED_IS_USING(player_ped_id())
						ELSe
							vehicle[veh_player].id = null
						ENDIF*/ //not sure purpose of this but the player will never be in a vehicle when the final cut is triggered
						CLEAR_AREA(<< -1155.3761, -1518.0754, 3.5626 >>,10.0,TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						START_CUTSCENE()
						
						IF IS_STREAMVOL_ACTIVE()
							STREAMVOL_DELETE(trailer_frustum)
						ENDIF
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0)
						//set static cam from safehouse shot so there's no frame without action
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						actions[thisI].flag++
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_PLAYING()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(1000)
						ENDIF
						actions[thisI].flag++
					ENDIF
				BREAK						
				CASE 2
					IF HAS_CUTSCENE_FINISHED()  		
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)		
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
							IF NOT IS_THIS_MODEL_A_PLANE( GET_ENTITY_MODEL(vehicle[veh_player].id))
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[veh_player].id,<< -1156.9592, -1521.3325, 3.3315 >>) < 7.0
									SET_ENTITY_COORDS(vehicle[veh_player].id,<< -1156.9592, -1521.3325, 3.3315 >>)
									SET_ENTITY_HEADING(vehicle[veh_player].id,216)
									//SET_VEHICLE_GEN_VEHICLE(VEHGEN_TREVOR_SAVEHOUSE_CITY, vehicle[veh_player].id) //removed as per 1426788
								ENDIF
							ENDIF
						ENDIF
						
						REPLAY_STOP_EVENT()
						
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1158.065186,-1517.147827,2.732938>>, <<-1146.635010,-1532.795044,6.208485>>, 8.750000,<<-1151.1544, -1530.3187, 3.2486>>, 214.5216,<<4.5,20,2.5>>,true,true,true,true)
						script_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
				
						SET_CAM_COORD(script_cam,<<-1157.184937,-1524.298706,4.493604>>)
						SET_CAM_ROT(script_cam,<<8.201474,-0.184709,-78.422516>>)
						SET_CAM_FOV(script_cam,40)			
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			//			SET_CAM_SPLINE_SMOOTHING_STYLE(script_cam,CAM_SPLINE_NO_SMOOTH)
						SET_FRONTEND_RADIO_ACTIVE(TRUE)
						RENDER_SCRIPT_CAMS(true,false)
						actions[thisI].completed = TRUE
					ENDIF
					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_SAFEHOUSE_INTRO_SHOT
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SWITCH actions[thisI].flag
				CASE 0
					IF DOES_CAM_EXIST(script_cam)
						DESTROY_CAM(script_cam)
					ENDIF
					script_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
												
					ADD_CAM_SPLINE_NODE(script_cam,<<-1157.184937,-1524.298706,4.493604>>,<<8.201474,-0.184709,-78.422516>>,7500) 
					ADD_CAM_SPLINE_NODE(script_cam,<<-1157.184937,-1524.298706,4.493604>>,<<-0.144409,-0.184709,-129.169205>>,7500)
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1158.065186,-1517.147827,2.732938>>, <<-1146.635010,-1532.795044,6.208485>>, 8.750000,<<-1151.1544, -1530.3187, 3.2486>>, 214.5216,<<4.5,20,2.5>>,true,true,true,true)
					

					SET_CAM_FOV(script_cam,40)		
					SHAKE_CAM(script_cam,"Hand_shake",0.3)
					SET_CAM_SPLINE_SMOOTHING_STYLE(script_cam,CAM_SPLINE_NO_SMOOTH)
					RENDER_SCRIPT_CAMS(true,false)
												
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					HIDE_HUD_AND_RADAR_THIS_FRAME()							
					SET_FRONTEND_RADIO_ACTIVE(FALSE)	
					actions[thisI].intA = get_Game_timer() + 7500
					PRINT_HELP("TRV3_END1")

					 actions[thisI].flag++
				BREAK
				CASE 1
					HIDE_HUD_AND_RADAR_THIS_FRAME()	
					IF GET_GAME_TIMER() > actions[thisI].intA
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_KEEP_INTERIOR_IN_MEMORY
			IF NEW_LOAD_SCENE_START(<<-1159.6870, -1519.1455, 11.5989>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-15.0004, 0.0000, -161.4299>>) ,8)
			//endFrustum = STREAMVOL_CREATE_FRUSTUM( <<-1159.6870, -1519.1455, 11.5989>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-15.0004, 0.0000, -161.4299>>) ,8,FLAG_MAPDATA,STREAMVOL_LOD_FLAG_HIGH)
				actions[thisI].completed = TRUE
			ENDIF
		BREAK
		
		CASE ACT_BED_SHOT
			SWITCH actions[thisI].flag
				CASE 0
					IF DOES_CAM_EXIST(script_cam)
						DESTROY_CAM(script_cam)
					ENDIF
					script_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
					ADD_CAM_SPLINE_NODE(script_cam,<<-1149.8,-1515.2,11.7>>, <<-19.4,-0.0,-23.2>>,4000)							
					ADD_CAM_SPLINE_NODE(script_cam,<<-1149.9,-1515.3,11.6>>, <<-19.4,-0.0,-23.2>>,7500) 
					SET_CAM_FOV(script_cam,50)							
					SET_CAM_SPLINE_SMOOTHING_STYLE(script_cam,CAM_SPLINE_NO_SMOOTH)
					RENDER_SCRIPT_CAMS(true,false)
												
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					HIDE_HUD_AND_RADAR_THIS_FRAME()							
					SET_FRONTEND_RADIO_ACTIVE(FALSE)	
					actions[thisI].intA = get_Game_timer() + 7500
					PRINT_HELP("TRV3_END2")

					 actions[thisI].flag++
				BREAK
				CASE 1
					HIDE_HUD_AND_RADAR_THIS_FRAME()	
					IF GET_GAME_TIMER() > actions[thisI].intA
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_ACTIVITIES_SHOT
			SWITCH actions[thisI].flag
				CASE 0
					IF DOES_CAM_EXIST(script_cam)
						DESTROY_CAM(script_cam)
					ENDIF
					script_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
					ADD_CAM_SPLINE_NODE(script_cam,<<-1145.5,-1515.9,11.5>>, <<-2.8,0,-52.1>>,4000)							
					ADD_CAM_SPLINE_NODE(script_cam,<<-1145.5,-1516.0,11.1>>, <<-3.3,0,8.5>>,7500) 
					SET_CAM_FOV(script_cam,38)							
					SET_CAM_SPLINE_SMOOTHING_STYLE(script_cam,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
					RENDER_SCRIPT_CAMS(true,false)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_OUTFIT,OUTFIT_P2_TANKTOP_SWEATPANTS_1)
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					HIDE_HUD_AND_RADAR_THIS_FRAME()							
					SET_FRONTEND_RADIO_ACTIVE(FALSE)	
					
					SET_ENTITY_COORDS(player_ped_id(),<<-1150.5624, -1513.6116, 9.6327>>)
					SET_ENTITY_HEADING(player_ped_id(),256)
					
					
					actions[thisI].intA = get_Game_timer() + 7500
					PRINT_HELP("TRV3_END3")

					 actions[thisI].flag++
				BREAK
				CASE 1
					HIDE_HUD_AND_RADAR_THIS_FRAME()	
					IF GET_GAME_TIMER() > actions[thisI].intA																							
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ACT_WARDROBE_SHOT
			SWITCH actions[thisI].flag
				CASE 0
					IF DOES_CAM_EXIST(script_cam)
						DESTROY_CAM(script_cam)
					ENDIF
					script_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
					ADD_CAM_SPLINE_NODE(script_cam,<<-1148.1903, -1512.6702, 11.3973>>, <<-11.8298, -0.0000, 101.1389>>,4000)							
					ADD_CAM_SPLINE_NODE(script_cam,<<-1149.6112, -1511.7377, 11.5468>>, <<-13.2912, -0.0000, 142.1143>>,7500) 
					SET_CAM_FOV(script_cam,50)							
					SET_CAM_SPLINE_SMOOTHING_STYLE(script_cam,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
					RENDER_SCRIPT_CAMS(true,false)
												
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					HIDE_HUD_AND_RADAR_THIS_FRAME()							
					SET_FRONTEND_RADIO_ACTIVE(FALSE)	
					actions[thisI].intA = get_Game_timer() + 7500
					PRINT_HELP("TRV3_END4")

					 actions[thisI].flag++
				BREAK

				CASE 1
					HIDE_HUD_AND_RADAR_THIS_FRAME()	
					IF GET_GAME_TIMER() > actions[thisI].intA
					//	IF DOES_CAM_EXIST(script_cam)
					//		DESTROY_CAM(script_cam)
					//	ENDIF
						actions[thisI].intA = get_Game_timer() + 3000
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-114)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
						RENDER_SCRIPT_CAMS(false,true,DEFAULT_INTERP_TO_FROM_GAME,FALSE)
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						SET_FRONTEND_RADIO_ACTIVE(TRUE)
						TASK_TURN_PED_TO_FACE_COORD(player_ped_id(),<<-1150.3480, -1514.8767,  11.0266>>,2000)
						
						actions[thisI].flag++
					ENDIF
				BREAK
				case 2
					IF GET_GAME_TIMER() > actions[thisI].intA
						actions[thisI].completed = TRUE
					ENDIF
				break
			ENDSWITCH
		BREAK
		
		CASE ACT_SKIP_CUTS
			SWITCH actions[thisI].flag
				CASE 0
					CPRINTLN(debug_Trevor3,"Is streamvol valid")
					IF IS_ACTION_COMPLETE(1,ACT_KEEP_INTERIOR_IN_MEMORY)
						actions[thisI].flag++							
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
				
						//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_OUTFIT,OUTFIT_P2_TANKTOP_SWEATPANTS_1)
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)												
						SET_FRONTEND_RADIO_ACTIVE(TRUE)	
							
					//	SET_ENTITY_COORDS(player_ped_id(),<<-1150.5624, -1513.6116, 9.6327>>)
					//	SET_ENTITY_HEADING(player_ped_id(),189)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(player_ped_id())
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)						
						RENDER_SCRIPT_CAMS(false,false)
						DESTROY_ALL_CAMS()
						NEW_LOAD_SCENE_STOP()
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL ACTION(int thisI, enumActions thisAction, enumACTIONplayout playout=PLAYOUT_ON_TRIGGER, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL)			
	#if IS_DEBUG_BUILD
		IF thisI >= MAX_ACTIONS
			SCRIPT_ASSERT("SCRIPT: INCREASE MAX_ACTIONS")
		ENDIF
	#endif
	IF actions[thisI].action != thisAction
	OR actions[thisI].active = FALSE
		actions[thisI].action = thisAction
		actions[thisI].active = TRUE
		actions[thisI].ongoing = FALSE
		actions[thisI].completed = FALSE		
		actions[thisI].flag = 0
		actions[thisI].needsCleanup = FALSE
		actions[thisI].intA = 0
		actions[thisI].intB = 0
		actions[thisI].floatA = 0
	ENDIF
	
	IF actions[thisI].completed
		RETURN TRUE
	ENDIF
	
	bool actionConditionCheck
	
	IF actions[thisI].ongoing
	AND playout = PLAYOUT_ON_TRIGGER
		actionConditionCheck = TRUE
	ELSE
		actionConditionCheck = checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3)
	ENDIF
	
	IF playout = PLAYOUT_ON_COND
	AND NOT actionConditionCheck
		actions[thisI].ongoing = FALSE
	ENDIF
	
		
	IF actions[thisI].completed = FALSE
	OR actions[thisI].flag = CLEANUP
		IF actionConditionCheck = TRUE
			IF NOT actions[thisI].ongoing
				actions[thisI].ongoing = TRUE
			ENDIF
			
			SWITCH actions[thisI].action	
				CASE ACT_LOAD_INTRO_CUT								
					REQUEST_CUTSCENE("trevor_drive_int")						
					actions[thisI].completed = TRUE
				BREAK
				CASE ACT_PLAY_CUTSCENE
					SET_DOOR_STATE(DOORNAME_T_TRAILER_CS_G,DOORSTATE_FORCE_CLOSED_THIS_FRAME)
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						IF NOT IS_PED_FUCKED(g_sTriggerSceneAssets.ped[0])
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Wade",g_sTriggerSceneAssets.ped[0])
						ENDIF
					ENDIF
				
					switch actions[thisI].flag
						case 0
							
							
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
								MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_TREVOR_3)
								IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
									IF NOT IS_PED_FUCKED(g_sTriggerSceneAssets.ped[0])
										REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.ped[0],"Wade",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,GET_NPC_PED_MODEL(CHAR_WADE))						
										
										SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
										
										ped[ped_wade].id = g_sTriggerSceneAssets.ped[0]
									ENDIF
								ELSE									
									REGISTER_ENTITY_FOR_CUTSCENE(ped[ped_wade].id,"Wade",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,GET_NPC_PED_MODEL(CHAR_WADE))	
										//cprintln(DEBUG_TREVOR3,"CREATE WADE IN CUTSCENE")				
								ENDIF
								REGISTER_ENTITY_FOR_CUTSCENE(player_ped_id(),"Trevor",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								START_CUTSCENE()
								
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
								
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
								actions[thisI].flag++
							ENDIF
						BREAK
						case 1
							IF IS_SCREEN_FADED_OUT()
							AND NOT IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_IN(1000)
								//cprintln(DEBUG_TREVOR3,"Fade in: ",get_game_timer())
							ENDIF
							actions[thisI].flag++
						break
						CASE 2
							
						
							
						
							
							IF CAN_SET_EXIT_STATE_FOR_CAMERA()
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
								SET_CONDITION_STATE(COND_CUTSCENE_FINISHED,TRUE)
								
								REPLAY_STOP_EVENT()
								
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_RESOLVE_VEHICLES_AT_START
					SWITCH actions[thisI].flag
						case 0
							//cprintln(debug_trevor3,"RESOLVE VEHICLES")
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1970.273438,3810.582520,30.559752>>, <<1982.751831,3817.503906,34.618141>>, 6.925000,<<1983.9470, 3823.9656, 31.3549>>,25.7606, <<10,10,9>>)
							//wade jump location
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1970.069214,3813.558105,30.769392>>, <<1971.171143,3811.620361,33.250465>>, 1.000000,<<1965.8198, 3797.8459, 31.2019>>, 123.9023, <<7,20,15>>)
							CLEAR_AREA(<<1970.7434, 3812.1272, 31.3808>>,1.0,TRUE)
							CLEAR_AREA_OF_PEDS(<<1985.750732,3825.215088,31.339054>>,15.8)
							vehicle[veh_player].id = GET_LAST_DRIVEN_VEHICLE()
							IF DOES_ENTITY_EXIST(vehicle[veh_player].id)
								IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[veh_player].id,<<1985.8885, 3813.3721, 31.1577>>) > 30
										vehicle[veh_player].id = null
									endif
								endif
							endif
							
							//create a new vehicle if there are no nearby vehicles
							
							
							
							int iCounter
							icounter = GET_PED_NEARBY_VEHICLES(player_ped_id(),vehiclearray)
							int iTmp
					
									
							For iTmp = 0 to icounter-1
								IF DOES_ENTITY_EXIST(vehiclearray[iTmp])
									IF IS_VEHICLE_SAFE(vehiclearray[iTmp])								
										float fDist
										fdist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehiclearray[iTmp],<<1982.7443, 3821.2297, 31.4748>>)
										IF fdist < 20
											IF GET_ENTITY_SPEED(vehiclearray[iTmp]) < 0.5
												actions[thisI].completed = TRUE
											ENDIF
										ENDIF																									
									ENDIF
								ENDIf
							ENDFOR
							actions[thisI].flag++
						BREAK
						CASE 1 //only gets here if there are no existing nearby vehicles
							REQUEST_MODEL(BLAZER)
							actions[thisI].flag++
						BREAK
						CASE 2
							IF HAS_MODEL_LOADED(BLAZER)
								vehicle_index veHtemp
								veHtemp = CREATE_VEHICLE(BLAZER,<<1987.1693, 3825.6538, 31.4259>>,25)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTemp)
								SET_MODEL_AS_NO_LONGER_NEEDED(blazer)
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH							
				BREAK
								
				CASE ACT_EXTRACT_WADE_FROM_CUTSCENE
					IF NOT DOES_ENTITY_EXIST(ped[ped_wade].id)
						IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Wade"))
							ped[ped_wade].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Wade"))
							SET_WADE_DEFAULTS()	
							//cprintln(DEBUG_TREVOR3,"EXTRACT WADE FROM CUTSCENE: ",get_game_timer())
					
						ENDIF
					ENDIF
					
				BREAK
				
				CASE ACT_WADE_GETS_IN_BODHI
					
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						SWITCH actions[thisI].flag
							CASE 0
								SET_PED_RESET_FLAG(ped[ped_wade].id,PRF_UseTighterAvoidanceSettings,TRUE)
								actions[thisI].flag++										
							BREAK
							CASE 1
								IF IS_PED_IN_ANY_VEHICLE(ped[ped_wade].id)
									SET_PED_RESET_FLAG(ped[ped_wade].id,PRF_UseTighterAvoidanceSettings,FALSE)
									actions[thisI].completed = TRUE
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE ACT_UPDATE_PLAYER_VEHICLE
					IF IS_PED_SITTING_IN_ANY_VEHICLE(player_ped_id())
						
						vehicle_index vehTemp
						vehTemp = GET_VEHICLE_PED_IS_IN(player_ped_id())
						IF GET_PED_VEHICLE_SEAT(player_ped_id(),vehTemp) = VS_DRIVER
							IF vehTemp != vehicle[veh_player].id
							
								IF IS_VEHICLE_SAFE(vehicle[veh_lastPlayer].id)
									IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[veh_lastPlayer].id)								
										SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_lastPlayer].id)
									ENDIF
								ENDIF
							
								vehicle[veh_lastPlayer].id = vehicle[veh_player].id												
								
								vehicle[veh_player].id = GET_VEHICLE_PED_IS_IN(player_ped_id())
								IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[veh_player].id)
									SET_ENTITY_AS_MISSION_ENTITY(vehicle[veh_player].id,TRUE,TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_SAFE(vehicle[veh_lastPlayer].id)
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),vehicle[veh_lastPlayer].id) > 60
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[veh_lastPlayer].id)								
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_lastPlayer].id)
								vehicle[veh_lastPlayer].id = null
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE ACT_MAKE_WADE_AND_PLAYER_ENTER_SPECIFIC_SEAT
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						SWITCH actions[thisI].flag
							CASE 0						
								SET_PED_RESET_FLAG(player_ped_id(),PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly,TRUE) 
								actions[thisI].flag++
							BREAK
							CASE 1
								IF IS_PED_IN_GROUP(ped[ped_wade].id)
									SET_PED_GROUP_MEMBER_PASSENGER_INDEX(ped[ped_wade].id,VS_FRONT_RIGHT)
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 2
								IF NOT IS_PED_IN_GROUP(ped[ped_wade].id)
									actions[thisI].flag = 1
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE ACT_MAKE_IT_RAIN
					SWITCH actions[thisI].flag
						CASE 0
							SET_WEATHER_TYPE_OVERTIME_PERSIST("OVERCAST",30.0)
							actions[thisI].flag++
							actions[thisI].intA = GET_GAME_TIMER() + 30000
						BREAK
						CASE 1															
							IF GET_GAME_TIMER() > actions[thisI].intA
								SET_WEATHER_TYPE_OVERTIME_PERSIST("THUNDER",40.0)
								actions[thisI].completed = TRUE
							ENDIF							
						BREAK
					ENDSWITCH
					
				BREAK
				
				CASE ACT_PREP_TRAILER_PARK
					SWITCH actions[thisI].flag
						CASE 0
							ADD_SCENARIO_BLOCKING_AREA( <<61,3694,38>>-<<130,130,130>>,<<61,3694,38>>+<<130,130,130>>)
							SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_01,TRUE)
							SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_02,TRUE)
							SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_03,TRUE)
							CLEAR_AREA(<<61,3694,38>>,130,TRUE)	

							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 92.6982, 3738.7275, 36.7299 >>,<< 105.6982, 3747.7275, 40.7299 >>,FALSE)
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 43.4604, 3656.9370, 38.8369 >>-<<7,7,5>>,<< 43.4604, 3656.9370, 38.8369 >>+<<7,7,5>>,FALSE)
							
							SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<< 41.2529, 3709.4043, 38.7345 >>,100.0,0)
						
							SET_ROADS_IN_ANGLED_AREA(<<53.943714,3620.474365,28.627594>>, <<64.899071,3764.616943,50.328354>>, 106.500000,FALSE,FALSE)
							
							REQUEST_MODEL(G_M_Y_LOST_01) 
							
							actions[thisI].flag++
						BREAK
						CASE 1
							IF HAS_MODEL_LOADED(G_M_Y_LOST_01)							
								REQUEST_MODEL(G_M_Y_LOST_02)																
								actions[thisI].flag=101
							ENDIF
						BREAK
						CASE 101
							IF HAS_MODEL_LOADED(G_M_Y_LOST_02)
								REQUEST_MODEL(DLOADER)
								actions[thisI].flag=102
							ENDIF
						BREAK
						CASE 102
							IF HAS_MODEL_LOADED(DLOADER)
								REQUEST_MODEL(HEXER)
								actions[thisI].flag=2
							ENDIF
						BREAK
						CASE 2							
							IF HAS_MODEL_LOADED(HEXER)									
								REQUEST_MODEL(Prop_Coffin_01)
								request_model(Prop_CS_Beer_Bot_01)
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 3
							IF HAS_MODEL_LOADED(Prop_Coffin_01)
							AND HAS_MODEL_LOADED(Prop_CS_Beer_Bot_01)
								actions[thisI].flag++
								REQUEST_PTFX_ASSET()
								REQUEST_ANIM_DICT("misstrevor3")
							ENDIF
						BREAK
						CASE 4
							IF HAS_PTFX_ASSET_LOADED()
							AND HAS_ANIM_DICT_LOADED("misstrevor3")
								REQUEST_ANIM_DICT("misschinese2_bank1")
								REQUEST_ANIM_DICT("misstrevor3_beatup")
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 5
							IF HAS_ANIM_DICT_LOADED("misschinese2_bank1")
							AND HAS_ANIM_DICT_LOADED("misstrevor3_beatup")
								actions[thisI].flag++
								REQUEST_ANIM_DICT("reaction@male_stand@big_variations@b")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@left")
							ENDIF
						BREAK
						CASE 6
							IF HAS_ANIM_DICT_LOADED("reaction@male_stand@big_variations@b")
							and HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@left")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@right")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@backward")							
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 7																																														
							IF HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@right")
							and HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@backward")
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 8
							//create ambient bikes
							vehicle[veh_ambientA].id = CREATE_VEHICLE(HEXER,<< 43.4235, 3653.8823, 38.7234 >>, 0.5785)
							vehicle[veh_ambientB].id = CREATE_VEHICLE(HEXER,<<42.7094, 3659.6279, 38.8113>>, 235.4671)
							FORCE_ACTION_STATE(4,ACT_RELEASE_TRAILER_PARK,FALSE,FALSE)
							SET_ACTION_FLAG(4,ACT_RELEASE_TRAILER_PARK,0)
							actions[thisI].completed = TRUE
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_RELEASE_TRAILER_PARK
					
					SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_01)
					SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_02)
					SET_MODEL_AS_NO_LONGER_NEEDED(DLOADER)
					SET_MODEL_AS_NO_LONGER_NEEDED(HEXER)	
					SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Coffin_01)
					SET_MODEL_AS_NO_LONGER_NEEDED(Prop_CS_Beer_Bot_01)
					REMOVE_ANIM_DICT("misstrevor3")
					REMOVE_ANIM_DICT("misschinese2_bank1")
					REMOVE_ANIM_DICT("misstrevor3_beatup")
	
					REMOVE_ANIM_DICT("reaction@male_stand@big_variations@b")
					REMOVE_ANIM_DICT("reaction@male_stand@big_intro@left")
					REMOVE_ANIM_DICT("reaction@male_stand@big_intro@right")
					REMOVE_ANIM_DICT("reaction@male_stand@big_intro@backward")
					REMOVE_PTFX_ASSET()
					IF DOES_ENTITY_EXIST(vehicle[veh_ambientA].id)
						DELETE_VEHICLE(vehicle[veh_ambientA].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehicle[veh_ambientB].id)
						DELETE_VEHICLE(vehicle[veh_ambientB].id)
					ENDIF

													
					FORCE_ACTION_STATE(3,ACT_PREP_TRAILER_PARK,FALSE,FALSE)
					SET_ACTION_FLAG(3,ACT_PREP_TRAILER_PARK,0)	
					actions[thisI].completed = TRUE												
				BREAK
				
				CASE ACT_SET_PED_DENSITY_EVERY_FRAME
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				BREAK
				
				CASE ACT_KILL_BANTER
					SET_DIALOGUE_AS_ENDED(4,DIA_TRAILER_PARK_DRIVE_CHAT)
					SET_DIALOGUE_AS_ENDED(3,DIA_WADE_COMMENTS_ON_CAR_DAMAGE)
					//cprintln(debug_trevor3,"kill convo ACT_KILL_BANTER")
					KILL_FACE_TO_FACE_CONVERSATION()
					actions[thisI].completed = TRUE	
				BREAK
				
				CASE ACT_AUDIO_SCENE_GETTING_IN_CAR
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						START_AUDIO_SCENE("TREVOR_3_DRIVE_TO_TRAILER_PARK")
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
				
				CASE ACT_LIGHTNING_FLASH
					SWITCH actions[thisI].flag
						CASE 0							
							actions[thisI].flag++
							actions[thisI].intA = GET_GAME_TIMER() + 500
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > actions[thisI].intA 
								FORCE_LIGHTNING_FLASH()
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_AUDIO_FOR_EXPLOSIONS					
					//cprintln(debug_Trevor3,"LOAD TREVOR3_FIGHT_COMMOTION_MASTER")
					IF NOT bShoutingAudioPlaying
						
						//check if 3 or more enemies alive
						int iAliveCount, iT
						repeat count_of(ped) iT
							IF NOT IS_PED_FUCKED(ped[iT].id)
								IF iT != ped_wade
								AND iT != ped_fought
									iAliveCount++
								ENDIF
							ENDIF
						ENDREPEAT
						
						IF iAliveCount > 2
							IF LOAD_STREAM("TREVOR3_FIGHT_COMMOTION_MASTER")		
								bShoutingAudioPlaying = TRUE
								//cprintln(debug_Trevor3,"PLAY TREVOR3_FIGHT_COMMOTION_MASTER")
								PLAY_STREAM_FROM_POSITION(<<70.2011, 3712.8398, 40.5056>>)
								actions[thisI].completed = TRUE
							ENDIF	
						ENDIF
					ENDIf
				BREAK
				
				CASE ACT_MAKE_PLAYER_USE_DRIVER_DOOR
					SET_PED_RESET_FLAG(player_ped_id(),PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly,TRUE) 
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_SET_LOST_ALERTED_STUFF
					SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
					actions[thisI].completed = TRUE
				BREAK
				
				
				
				CASE ACT_MECHANIC	
				/*
					cprintln(debug_trevor3,"A!")
					IF IS_VEHICLE_DRIVEABLE(vehicle[veh_broken].id)
						cprintln(debug_trevor3,"B!")
						IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(vehicle[veh_broken].id,SC_DOOR_BONNET)
							//SET_VEHICLE_DOOR_LATCHED(vehicle[veh_broken].id,SC_DOOR_BONNET,false,false,false)
							cprintln(debug_trevor3,"OPEN SESAME!")
							SET_VEHICLE_DOOR_OPEN(vehicle[veh_broken].id,SC_DOOR_BONNET,false,true)																
						ELSE	
							SET_VEHICLE_ENGINE_HEALTH(vehicle[veh_broken].id,0)
							cprintln(debug_trevor3,"The door is fully open!")
						ENDIF
					ELSE
						cprintln(debug_trevor3,"C!")
					ENDIF*/
					IF NOT IS_PED_FUCKED(ped[ped_fixer].id)
					OR NOT DOES_ENTITY_EXIST(ped[ped_fixer].id)
						IF (pedReactionState[ped_fixer].state <= STATE_AMBIENT //do nothing if ped responding
						AND NOT IS_PED_FUCKED(ped[ped_fixer].id)
						AND NOT IS_PED_IN_COMBAT(ped[ped_fixer].id))
						OR NOT DOES_ENTITY_EXIST(ped[ped_fixer].id)
							SWITCH actions[thisI].flag
								CASE 0
									IF NOT DOES_ENTITY_EXIST(ped[ped_fixer].id)
										CREATE_BIKER_PED(ped_fixer,G_M_Y_LOST_02,WEAPONTYPE_PUMPSHOTGUN,<< 59.1969, 3635.6641, 38.7018 >>,105.7270)
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_fixer,ped[ped_fixer].id,TRUE)
										iMechanicArray = GET_REACTION_ARRAY_ENTRY(ped[ped_fixer].id,pedReactionState)
										vehicle[veh_broken].id = CREATE_VEHICLE(DLOADER,<< 58.0830, 3638.1704, 38.4957 >>, 205.5108)
										
										SET_VEHICLE_DOOR_OPEN(vehicle[veh_broken].id,SC_DOOR_BONNET,false,false)
										
										PLAY_SOUND_FROM_ENTITY(-1,"DAMAGED_TRUCK_IDLE",vehicle[veh_broken].id)
										actions[thisI].flag++
									ELSE
										actions[thisI].flag++
									ENDIF
								BREAK

								CASE 1
								FALLTHRU
								CASE 2
									
									IF actions[thisI].flag = 1
										IF IS_VEHICLE_DRIVEABLE(vehicle[veh_broken].id)
											IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle[veh_broken].id,SC_DOOR_BONNET) > 0.9
											
												SET_VEHICLE_ENGINE_HEALTH(vehicle[veh_broken].id,0)
												actions[thisI].flag++
											ELSE	
												SET_VEHICLE_DOOR_OPEN(vehicle[veh_broken].id,SC_DOOR_BONNET,false,false)
											ENDIF
											
										ENDIF
									ENDIF
									
									IF pedReactionState[iMechanicArray].state = STATE_AMBIENT
									OR pedReactionState[iMechanicArray].state = STATE_RESTART_AMBIENT
										pedReactionState[iMechanicArray].state = STATE_AMBIENT
										IF (IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intB) AND GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intB) = 1.0)
										OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA) AND NOT  IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intB))
											actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 58.162, 3638.364, 39.438 >>, << 0.000, 0.000, -157.250 >>)							
											TASK_SYNCHRONIZED_SCENE (ped[ped_fixer].id, actions[thisI].intA, "misstrevor3", "brokendown_longlook", 1, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,rbf_none,INSTANT_BLEND_IN) //url:bugstar:1427928
											SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intA,TRUE)
											actions[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,5000)
										ENDIF
									ENDIF
								BREAK
								DEFAULT
								
								
									IF pedReactionState[iMechanicArray].state = STATE_AMBIENT
									OR pedReactionState[iMechanicArray].state = STATE_RESTART_AMBIENT
										pedReactionState[iMechanicArray].state = STATE_AMBIENT
										IF GET_GAME_TIMER() > actions[thisI].flag
											actions[thisI].intB = CREATE_SYNCHRONIZED_SCENE(<< 58.162, 3638.364, 39.438 >>, << 0.000, 0.000, -157.250 >>)									
											SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(actions[thisI].intB,TRUE)
											IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_fixer].id) > 10									
												IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
													TASK_SYNCHRONIZED_SCENE (ped[ped_fixer].id, actions[thisI].intB, "misstrevor3", "brokendown_wrongwithyou", 1, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,rbf_none,1 )
												ELSE
													TASK_SYNCHRONIZED_SCENE (ped[ped_fixer].id, actions[thisI].intB, "misstrevor3", "brokendown_suchabitch", 1, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,rbf_none,1 )
												ENDIF									
											ELSE
												IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
													TASK_SYNCHRONIZED_SCENE (ped[ped_fixer].id, actions[thisI].intB, "misstrevor3", "brokendown_wrongwithyou", 1, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,rbf_none,1 )
												ELSE
													actions[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,5000)
												ENDIF
											ENDIF
											actions[thisI].flag = 2
										ENDIF
									ENDIF
								BREAK										
							ENDSWITCH
						ENDIF
					ENDIF
				BREAK
				
				CASE ACT_TRAILER
					CREATE_BIKER_PED(ped_nookie,G_M_Y_LOST_01,WEAPONTYPE_ASSAULTRIFLE,<< 64.1751, 3690.5498, 38.7544 >>,285)					
					ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_nookie,ped[ped_nookie].id,TRUE)
					iTrailerAnim = CREATE_SYNCHRONIZED_SCENE(<< 67.817, 3694.449, 38.766 >>, << -0.000, 0.000, -35.190 >>)
					TASK_SYNCHRONIZED_SCENE (ped[ped_nookie].id, iTrailerAnim, "misstrevor3", "horny_biker_loop", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
					SET_SYNCHRONIZED_SCENE_LOOPED(iTrailerAnim,TRUE)
					actions[thisI].completed = TRUE	
				BREAK
				
				CASE ACT_BIKERS_DRIVE_OFF
					
						vehicle[veh_driveOffA].id = CREATE_VEHICLE(HEXER,<< 51.6001, 3701.1587, 38.7542 >>, 331.9564)
						vehicle[veh_driveOffB].id = CREATE_VEHICLE(HEXER,<< 53.2583, 3699.8926, 39.2443 >>, 331.9564)
						CREATE_BIKER_PED(ped_driveOffA,G_M_Y_LOST_01,WEAPONTYPE_ASSAULTRIFLE,<< 49.3678, 3697.2810, 38.7542 >>,277.1980)	
						CREATE_BIKER_PED(ped_driveOffB,G_M_Y_LOST_02,WEAPONTYPE_PUMPSHOTGUN,<< 51.1357, 3696.6035, 38.7542 >>,332.8487)	
						ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_driveOffA,ped[ped_driveOffA].id,false,false,true)
						ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_driveOffB,ped[ped_driveOffB].id,false,false,true)
					
						iBikersDriveOff = CREATE_SYNCHRONIZED_SCENE(<< 51.367, 3697.315, 38.756 >>, << -0.000, 0.000, -28.440 >>)
						SET_SYNCHRONIZED_SCENE_LOOPED(iBikersDriveOff,TRUE)
						
						TASK_SYNCHRONIZED_SCENE (ped[ped_driveOffA].id, iBikersDriveOff, "misstrevor3", "bike_chat_b_loop_1", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE  | SYNCED_SCENE_ON_ABORT_STOP_SCENE) 
						TASK_SYNCHRONIZED_SCENE (ped[ped_driveOffB].id, iBikersDriveOff, "misstrevor3", "bike_chat_b_loop_2", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE  | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
						
						if IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
							PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle[veh_driveOffA].id,iBikersDriveOff, "bike_chat_b_loop_bike_a","misstrevor3", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
						endif
						
						if IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
							PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle[veh_driveOffB].id,iBikersDriveOff, "bike_chat_b_loop_bike_b","misstrevor3", INSTANT_BLEND_IN, SLOW_BLEND_OUT)//,enum_to_int(SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE))
						endif
						
					
						actions[thisI].completed = TRUE
				
				BREAK
				
				CASE ACT_BIKERS_SITTING_CHATTING
				
					vehicle[veh_chatA].id = CREATE_VEHICLE(HEXER,<<88.4630, 3698.4883, 38.6126 >>, 162.4861)
					vehicle[veh_chatB].id = CREATE_VEHICLE(HEXER,<< 91.4370, 3697.3845, 38.7356 >>, 161.4624)
					CREATE_BIKER_PED(ped_chattingA,G_M_Y_LOST_01,WEAPONTYPE_ASSAULTRIFLE,<< 92.7466, 3715.8420, 38.6797 >>,32.1990)	
					CREATE_BIKER_PED(ped_chattingB,G_M_Y_LOST_02,WEAPONTYPE_PUMPSHOTGUN,<< 93.6988, 3717.6482, 38.6765 >>,151.3154)	
					ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_chattingA,ped[ped_chattingA].id,FALSE,FALSE,TRUE)
					ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_chattingB,ped[ped_chattingB].id,FALSE,FALSE,TRUE)
					actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 88.368, 3701.228, 38.535 >>, << -0.000, 0.000, -7.920 >>)
					SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intA,TRUE)
					TASK_SYNCHRONIZED_SCENE (ped[ped_chattingA].id, actions[thisI].intA, "misstrevor3", "bike_chat_a_1", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
					TASK_SYNCHRONIZED_SCENE (ped[ped_chattingB].id, actions[thisI].intA, "misstrevor3", "bike_chat_a_2", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
					
					if IS_VEHICLE_SAFE(vehicle[veh_chatA].id)
						PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle[veh_chatA].id,actions[thisI].intA, "bike_chat_a_bike_b","misstrevor3", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					endif
					
					if IS_VEHICLE_SAFE(vehicle[veh_chatB].id)
						PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle[veh_chatB].id,actions[thisI].intA, "bike_chat_a_bike_a","misstrevor3", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					endif
					actions[thisI].completed = TRUE
				
				BREAK
				
				CASE ACT_BIKER_FIGHT
					CLEAR_AREA(<<65.51, 3744.23, 38.83>>,3,true)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<65.51, 3744.23, 38.83>>-<<2,2,2.562500>>,<<65.51, 3744.23, 38.83>>+<<2,2,2.562500>>,FALSE)

					CREATE_BIKER_PED(ped_fighterA,G_M_Y_LOST_01,WEAPONTYPE_PUMPSHOTGUN,<< 77.6549, 3717.1753, 38.7655 >>,312.9244)	
					CREATE_BIKER_PED(ped_fighterB,G_M_Y_LOST_01,WEAPONTYPE_ASSAULTRIFLE,<< 79.7052, 3719.2146, 38.7656 >>,314.3887)	
					CREATE_BIKER_PED(ped_fought,G_M_Y_LOST_02,WEAPONTYPE_UNARMED,<< 77.3499, 3719.5664, 38.7655 >>,254.4565)
					DISABLE_PED_PAIN_AUDIO(ped[ped_fought].id,true)
					ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_fighterA,ped[ped_fighterA].id,FALSE)
					ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_fighterB,ped[ped_fighterB].id,FALSE)
					actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 67.711, 3743.775, 38.915 >>,<< -0.000, 0.000, -29.880 >>)//<< 61.166, 3729.765, 38.585 >>, << -0.000, 0.000, -27.750 >>)
					TASK_SYNCHRONIZED_SCENE (ped[ped_fought].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_startidle_dockworker", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
					TASK_SYNCHRONIZED_SCENE (ped[ped_fighterA].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_startidle_guard1", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
					TASK_SYNCHRONIZED_SCENE (ped[ped_fighterB].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_startidle_guard2", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )		
					SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intA,true)
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_COFFIN_CRY
					
							CREATE_BIKER_PED(ped_coffins,G_M_Y_LOST_02,WEAPONTYPE_ASSAULTRIFLE,<< 101.0579, 3745.6917, 38.7460 >>,130.9023)
							ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,ped_coffins,ped[ped_coffins].id,TRUE)
							obj[obj_coffinA].id = CREATE_OBJECT_NO_OFFSET (Prop_Coffin_01, <<99.410004,3744.350098,40.00000>>)
							SET_ENTITY_ROTATION (obj[obj_coffinA].id, <<-0.000000,0.000000,-25.210142>>)								
						//	SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(obj[obj_coffinA].id,TRUE)
							
										
							obj[obj_coffinB].id = CREATE_OBJECT_NO_OFFSET(Prop_Coffin_01,<< 98.8646, 3747.87, 39.2 >>)
							SET_ENTITY_ROTATION(obj[obj_coffinB].id,<< -0.65, -0, -0.75 >>)
							SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(obj[obj_coffinB].id,TRUE)
							SET_ENTITY_VELOCITY(obj[obj_coffinB].id,<<0,0,0.1>>)
							
							obj[obj_coffinC].id = CREATE_OBJECT_NO_OFFSET(Prop_Coffin_01,<< 99.5847, 3749.75, 39.2 >>)
							SET_ENTITY_ROTATION(obj[obj_coffinC].id,<< 3.4, -0.1, -104.4 >>)
							SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(obj[obj_coffinC].id,TRUE)
							SET_ENTITY_VELOCITY(obj[obj_coffinC].id,<<0,0,0.1>>)
							obj[obj_beer].id = CREATE_OBJECT_NO_OFFSET(Prop_CS_Beer_Bot_01,<< 99.5847, 3749.75, 41.0 >>)
							iCoffinAnim = CREATE_SYNCHRONIZED_SCENE(<< 98.488, 3742.822, 39.538 >>, << 0.000, 0.000, 153.250 >>)
							TASK_SYNCHRONIZED_SCENE (ped[ped_coffins].id, iCoffinAnim, "misstrevor3", "biker_mourns", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE )
							SET_SYNCHRONIZED_SCENE_LOOPED(iCoffinAnim,true)
							ATTACH_ENTITY_TO_ENTITY(obj[obj_beer].id,ped[ped_coffins].id,get_ped_bone_index(ped[ped_coffins].id,BONETAG_PH_R_HAND),<<0,0.0,0>>,<<0,0,0>>)			
							vehicle[veh_coffin].id = CREATE_VEHICLE(DLOADER,<< 98.6982, 3742.7275, 38.7299 >>, 155.2052)
							SET_VEHICLE_EXTRA(vehicle[veh_coffin].id,2,TRUE)
							SET_VEHICLE_EXTRA(vehicle[veh_coffin].id,3,TRUE)
					
							SET_VEHICLE_ON_GROUND_PROPERLY(vehicle[veh_coffin].id)
							ATTACH_ENTITY_TO_ENTITY(obj[obj_coffinA].id,vehicle[veh_coffin].id,0,<<0,-1.793,0.703>>,<<0,0,0>>,TRUE,TRUE,TRUE)
							
							ACTIVATE_PHYSICS(vehicle[veh_coffin].id)
							ACTIVATE_PHYSICS(obj[obj_coffinA].id)
							actions[thisI].completed = TRUE
						
				BREAK
				
				CASE ACT_COFFIN_RESTART_ANIM
					SWITCH actions[thisI].flag
						CASE 0 
							actions[thisI].intB = GET_REACTION_ARRAY_ENTRY(ped[ped_coffins].id,pedReactionState)
							actions[thisI].flag++
						BREAK
						CASE 1
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iCoffinAnim)
								IF pedReactionState[actions[thisI].intB].state = STATE_RESTART_AMBIENT
									iCoffinAnim = CREATE_SYNCHRONIZED_SCENE(<< 98.488, 3742.822, 39.538 >>, << 0.000, 0.000, 153.250 >>)
									TASK_SYNCHRONIZED_SCENE (ped[ped_coffins].id, iCoffinAnim, "misstrevor3", "biker_mourns", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE,RBF_NONE,SLOW_BLEND_IN)
									SET_SYNCHRONIZED_SCENE_LOOPED(iCoffinAnim,true)
									pedReactionState[actions[thisI].intB].state = STATE_AMBIENT
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
						
				CASE ACT_RECORD_CAMERA
					/*#if IS_DEBUG_BUILD
					vector va1,va2
					va1 = <<0,0,0>>
					va2 = <<0,0,0>>
					
					SWITCH actions[thisI].flag
						CASE 0	
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F3)
								IF IS_CAM_RENDERING(GET_DEBUG_CAM())
									#if IS_DEBUG_BUILD
									va1 = GET_CAM_COORD(GET_DEBUG_CAM())
									va2 = GET_CAM_ROT(GET_DEBUG_CAM())
									//cprintln(DEBUG_TREVOR3,va1,",",va2)
									#endif
									actions[thisI].flag++
									actions[thisI].intA = GET_GAME_TIMER() + 1000
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > actions[thisI].intA
							AND IS_CAM_RENDERING(GET_DEBUG_CAM())
								actions[thisI].intA += 1000	
								#if IS_DEBUG_BUILD
								va1 = GET_CAM_COORD(GET_DEBUG_CAM())
								va2 = GET_CAM_ROT(GET_DEBUG_CAM())
								//cprintln(DEBUG_TREVOR3,va1,",",va2)
								#endif
							ENDIF
						BREAK
					ENDSWITCH
					#endif*/
				BREAK
				
				CASE ACT_REWARD_BOMB_PLANTING_UNOBSERVED
					//INFORM_STAT_TREVOR_THREE_CAUGHT_SETTING_BOMB()
					 
					 INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TRV3_NOT_DETECTED)
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_WADE_RUNS_OUT_OF_TRAILER_PARK
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						IF IS_PED_IN_ANY_VEHICLE(ped[ped_wade].id)
							vehicle_index vehTemp
							vehTemp = GET_VEHICLE_PED_IS_IN(ped[ped_wade].id)
							IF IS_VEHICLE_SAFE(vehTemp)
								IF IS_ENTITY_ON_FIRE(vehTemp)
									startSeq()
									TASK_LEAVE_ANY_VEHICLE(null)
									TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<63.0295, 3603.9556, 38.8210>>,pedmove_run)
									task_cower(null,-1)
									endseq(ped[ped_wade].id)
									actions[thisI].completed = TRUE
								ENDIf
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_wade].id,<<68.210007,3701.656006,38.754890>> ) < 79.5
								startSeq()
								TASK_LEAVE_ANY_VEHICLE(null)
								TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<63.0295, 3603.9556, 38.8210>>,pedmove_run)
								task_cower(null,-1)
								endseq(ped[ped_wade].id)
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ACT_REWARD_BLOW_ALL_AT_ONCE
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TRV3_ALL_TRAILERS_AT_ONCE)
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_REWARD_STICKY_BOMB_USAGE
					SWITCH actions[thisI].flag
						CASE 0
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB)
								actions[thisI].intA = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB)
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB)
								actions[thisI].intB = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB)
								IF actions[thisI].intB < actions[thisI].intA
									actions[thisI].intA = actions[thisI].intB
									INFORM_MISSION_STATS_OF_INCREMENT(TRV3_STICKY_BOMBS_USED)
								ENDIF
							ELSE
								INFORM_MISSION_STATS_OF_INCREMENT(TRV3_STICKY_BOMBS_USED)
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_WADE_LOOKS_AT_TREVOR_NEAR_CAR
					
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
				
						IF GET_SCRIPT_TASK_STATUS(ped[ped_wade].id,SCRIPT_TASK_LOOK_AT_ENTITY) = FINISHED_TASK
					
							IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wade].id) < 20.0						
							
								TASK_LOOK_AT_ENTITY(ped[ped_wade].id,player_ped_id(),3000, SLF_EXTEND_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ACT_DISABLE_STICKY_BOMB_TRIGGER
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_DETONATE)
				BREAK
				
				CASE ACT_AUDIO_SCENE_PLANT_STICKY_BOMBS
					SWITCH actions[thisI].flag
						CASE 0
							IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
								START_AUDIO_SCENE("TREVOR_3_PLANT_BOMBS_STEALTH")
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONDITION_TRUE(COND_PLAYER_SPOTTED_BY_LOST)
								START_AUDIO_SCENE("TREVOR_3_PLANT_BOMBS_SHOOTOUT")
								STOP_AUDIO_SCENE("TREVOR_3_PLANT_BOMBS_STEALTH")
								actions[thisI].flag++
							ENDIF
							IF actions[thisI].flag = 1
								IF IS_CONDITION_TRUE(COND_ALL_BOMBS_PLANTED)
									STOP_AUDIO_SCENE("TREVOR_3_PLANT_BOMBS_STEALTH")
									actions[thisI].completed = TRUE
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF IS_CONDITION_TRUE(COND_ALL_BOMBS_PLANTED)								
								STOP_AUDIO_SCENE("TREVOR_3_PLANT_BOMBS_SHOOTOUT")
								actions[thisI].completed = TRUE
							ENDIF							
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_LOAD_AUDIO_STREAM
					IF LOAD_STREAM("TREVOR3_BLOW_UP_TRAILERS_MASTER")										
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
				
				CASE ACT_DELAY_ENEMY_SHOOTING
					SWITCH actions[thisI].flag
						CASE 0
							REPEAT COUNT_OF(ped) actions[thisI].intA
								IF NOT IS_PED_FUCKED(ped[actions[thisI].intA].id)
									actions[thisI].floatA = GET_COMBAT_FLOAT(ped[actions[thisI].intA].id,CCF_MAX_SHOOTING_DISTANCE)
									SET_COMBAT_FLOAT(ped[actions[thisI].intA].id,CCF_MAX_SHOOTING_DISTANCE,1.0)
								ENDIF
							ENDREPEAT
							actions[thisI].intA = GET_GAME_TIMER() + 3000
							actions[thisI].flag++
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > actions[thisI].intA
								REPEAT COUNT_OF(ped) actions[thisI].intA
									IF NOT IS_PED_FUCKED(ped[actions[thisI].intA].id)
										SET_COMBAT_FLOAT(ped[actions[thisI].intA].id,CCF_MAX_SHOOTING_DISTANCE,actions[thisI].floatA)
									ENDIF
								ENDREPEAT
								actions[thisI].completed = TRUE
							ENDIF							
						BREAK
					ENDSWITCH					
				BREAK
				
				CASE ACT_FRUSTUM_SETUP_FOR_FRIST_CUT
					SWITCH actions[thisI].flag
						CASE 0
						
							cprintln(debug_Trevor3,"PRELOAD SRL")
							PREFETCH_SRL("TREV3_TRAILER_ARRIVAL_CUTSCENE")
							SET_SRL_READAHEAD_TIMES(8,8,8,8)
							SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_ON) 
							REQUEST_WAYPOINT_RECORDING("trev3_trL")
							REQUEST_WAYPOINT_RECORDING("trev3_trR")
							actions[thisI].flag++
						BREAK
						CASE 1
							cprintln(debug_Trevor3,"WAITING FOR SRL TO LOAD")
							BEGIN_SRL()
							//IF IS_SRL_LOADED()
								actions[thisI].completed = TRUE		
							//ENDIF
						BREAK
					ENDSWITCH										
				BREAK
				
				CASE ACT_UPDATE_SRL_POSITION
					IF IS_CONDITION_FALSE(COND_PLAYER_WITHIN_300M_OF_TRAILER_PARK)
						//clean up SRL LOAD
						actions[thisI].ongoing = false
						END_SRL() //stop the SRL streaming in map data as player has gone away from the trigger area.
						SET_ACTION_FLAG(20,ACT_FRUSTUM_SETUP_FOR_FRIST_CUT,0)
						actions[20].completed = FALSE
						actions[thisI].intA = 0
					ELSE
						//update position in SRL depending on where player is
						
						int cutTriggerPoint
						int ipreload_SRL_Time
						
					
						IF IS_VEHICLE_DRIVEABLE(vehicle[veh_player].id)
							MODEL_NAMES thisModel
							thisModel = GET_ENTITY_MODEL(vehicle[veh_player].id)
							IF IS_THIS_MODEL_A_HELI(thisModel)
							OR IS_THIS_MODEL_A_PLANE(thisModel)
							OR IS_THIS_MODEL_A_BOAT(thisModel)
								cutTriggerPoint = 1
							ELSe
								IF IS_PED_ON_FOOT(player_ped_id())
									cutTriggerPoint = 1
								ENDIF
							ENDIF
						ELSE
							cutTriggerPoint = 1
						ENDIF
						
						
						
						
						
						IF cutTriggerPoint = 1
							IF actions[thisI].intA = 0
								actions[thisI].intA = 1
								//calculate iCamZone
								float fHeadingToTrailerPark
								fHeadingToTrailerPark = GET_HEADING_FROM_COORDS(<<59.12711, 3707.53613, 38.75499>>,GET_ENTITY_COORDS(player_ped_id()))
								
								
								
								IF fHeadingToTrailerPark < 97
								OR fHeadingToTrailerPark > 330
									iCamZone = 2
								ELIF fHeadingToTrailerPark >97
								AND fHeadingToTrailerPark < 217
									iCamZone = 0
								ELSE
									iCamZone = 1
								ENDIF
								
								cprintln(debug_trevor3,"fHeadingToTrailerPark = ",fHeadingToTrailerPark," iCamZone = ",iCamZone)
								
								SWITCH iCamZone
									CASE 0
										ipreload_SRL_Time = 12000
										cprintln(debug_Trevor3,"Prep SRL to time: 12000")
									BREAK
									CASE 1
										ipreload_SRL_Time = 19000
										cprintln(debug_Trevor3,"Prep SRL to time: 19000")																
									BREAK
									CASE 2
										ipreload_SRL_Time = 24000
										cprintln(debug_Trevor3,"Prep SRL to time: 24000")
									BREAK
								ENDSWITCH
							ENDIF
						ELSE
							actions[thisI].intA = 0
							iCamZone = -1
							IF GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(player_ped_id()),<<59.2455, 3609.4213, 40.7427>>) > 180.0
								ipreload_SRL_Time = 6000
								cprintln(debug_Trevor3,"Prep SRL to time: 0")
							ELSE
								ipreload_SRL_Time = 0
								
							ENDIF
							
						ENDIF
						
						SETTIMERA(ipreload_SRL_Time)
						SET_SRL_TIME(ipreload_SRL_Time *1.0)
						cprintln(debug_Trevor3,"Prep SRL to time: ",ipreload_SRL_Time)	
					ENDIF
				BREAK
				
				CASE ACT_SLOW_DOWN_PLAYER_IN_CHOPPER
					float fDist,fMulti,fThisSpeed
					fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<59.2455, 3609.4213, 40.7427>>)
					IF fDist < 1100					
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())		
							vehicle_index aVeh
							aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
							IF IS_VEHICLE_DRIVEABLE(aVeh)								
								IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(aVeh))
								OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(aVeh))
																	
									fmulti = (1100 - fDist)
									IF fmulti < 0
										fMulti = 0
									endif
									
									fmulti /= 1100
									fmulti *= 10000
									
									fThisSpeed = GET_ENTITY_SPEED(aVeh)
									
									fmulti *= fThisSpeed/50
									
									IF fThisSpeed > 50
										cprintln(debug_Trevor3,"Slow Down Player. Speed: ",fThisSpeed)
										APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(aVeh,apply_type_impulse,<<0,-fmulti,0>>,0,true,false)
										//showfloatOnScreen(0.1,0.1,fmulti)
										//showfloatOnScreen(0.1,0.15,fThisSpeed)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				//trailer park cutscene
				CASE ACT_DISABLE_PLAYER_CONTROL
					SWITCH actions[thisI].flag
						CASE 0
							actions[thisI].intA = GET_GAME_TIMER() + 1500
							actions[thisI].flag++
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > actions[thisI].intA
								SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_CUTSCENE_CAM
					DO_TRAILERPARK_ARRIVAL(actions[thisI])
																	
				BREAK
				
				CASE ACT_CUTSCENE_LIGHTNING_FLASH 
					IF IS_ACTION_COMPLETE(0,ACT_CUTSCENE_CAM)
						actions[thisI].completed = TRUE
					ELSE
						SWITCH actions[thisI].flag
							CASE 1							
								actions[thisI].intA = GET_GAME_TIMER() + 4000
								actions[thisI].flag++								
							BREAK
							CASE 2
								IF GET_GAME_TIMER() > actions[thisI].intA
									FORCE_LIGHTNING_FLASH()
									actions[thisI].completed = TRUE
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE ACT_SET_BOMB_PLANTING_STUFF
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 20, FALSE)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 20, FALSE)
					
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_PISTOL,WEAPONCOMPONENT_AT_PI_SUPP_02) 
					SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_UNARMED,TRUE)
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,FALSE)
					ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,FALSE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,FALSE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST,FALSE)
					
					
					
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						vehicle[veh_player].id = GET_VEHICLE_PED_IS_IN(player_ped_id())
						SET_VEHICLE_LIGHTS(vehicle[veh_player].id,SET_VEHICLE_LIGHTS_OFF)						
						SET_VEH_RADIO_STATION(vehicle[veh_player].id,"OFF")
						//SET_VEHICLE_RADIO_ENABLED(vehicle[veh_player].id,FALSE)
					ENDIF
					
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_wade].id,TRUE)
					ENDIF
					
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_LOST_ALERTED_RESPONSE
					REPEAT COUNT_OF(ped) actions[thisI].intA
						IF actions[thisI].intA != ped_wade
						AND actions[thisI].intA != ped_fought
							IF NOT IS_PED_FUCKED(ped[actions[thisI].intA].id)
								
								startSeq()
								IF GET_DISTANCE_BETWEEN_ENTITIES(ped[actions[thisI].intA].id,player_ped_id()) < 20.0
									
									float randHeading
									randHeading = GET_HEADING_FROM_ENTITIES(player_ped_id(),ped[actions[thisI].intA].id)
									vector vGoto
									vGoto = GET_ENTITY_COORDS(ped[actions[thisI].intA].id)
									vGoto.x += sin(randHeading) * 20.0
									vGoto.y += cos(randHeading) * 20.0						
									IF GET_SAFE_COORD_FOR_PED(vGoto,FALSE,vGoto)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,vGoto,pedmove_run)									
									ENDIF									
								ENDIF
								
								TASK_COMBAT_PED(null,player_ped_id())
								endseq(ped[actions[thisI].intA].id)
								SET_PED_TARGET_LOSS_RESPONSE(ped[actions[thisI].intA].id,TLR_NEVER_LOSE_TARGET)
								SET_PED_SPHERE_DEFENSIVE_AREA(ped[actions[thisI].intA].id,GET_ENTITY_COORDS(ped[actions[thisI].intA].id),25.0)
								SET_PED_RELATIONSHIP_GROUP_HASH(ped[actions[thisI].intA].id, relGroupEnemy)
								
								SET_PED_SEEING_RANGE(ped[actions[thisI].intA].id,60.0)
							ENDIF
						ELSE
							IF actions[thisI].intA = ped_fought			
								IF NOT IS_PED_FUCKED(ped[actions[thisI].intA].id)
									SET_ENTITY_HEALTH(ped[actions[thisI].intA].id,0)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_driveOffA].id)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_driveOffA].id)
						ENDIf
					ENDIF
					IF IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_driveOffB].id)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_driveOffB].id)
						ENDIf
					ENDIF
					
					actions[thisI].completed = TRUE
				BREAK

				CASE ACT_PED_REACTS_TO_STICKY_BOMB
					actions[thisI].intA = GET_CONDITION_INT(COND_LOST_SPOTTED_STICKY_BOMB)
					IF actions[thisI].intA >= 0
						IF NOT IS_PED_FUCKED(ped[actions[thisI].intA].id)
							IF NOT IS_VECTOR_ZERO(vSeenStickyBomb)
								//cprintln(DEBUG_TREVOR3,"PED DID THIS BIT")
								startSeq()
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
								TASK_AIM_GUN_AT_COORD(null,vSeenStickyBomb,2000)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,false)
								float randHeading
								randHeading = GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(player_ped_id()),vSeenStickyBomb)
								vector vGoto
								vGoto = GET_ENTITY_COORDS(ped[actions[thisI].intA].id)
								vGoto.x += sin(randHeading) * 20.0
								vGoto.y += cos(randHeading) * 20.0
							
								IF GET_SAFE_COORD_FOR_PED(vGoto,FALSE,vGoto)
									TASK_FOLLOW_NAV_MESH_TO_COORD(null,vGoto,pedmove_run)									
								ENDIF	
								TASK_COMBAT_PED(null,player_ped_id())
								
								endseq(ped[actions[thisI].intA].id)
								actions[thisI].completed = TRUE
							ELSE
								actions[thisI].completed = TRUE
							ENDIF
						ELSE
							actions[thisI].completed = TRUE
						ENDIF
					ELSE
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
				
				CASE ACT_UPDATE_AI_BLIPS
					//add death stats here
					REPEAT COUNT_OF(ped) iTemp
						If iTemp != ped_wade
						and iTemp != ped_fought
							IF (missionProgress = STAGE_DETROY_TRAILER_PARK AND IS_CONDITION_TRUE(COND_PLAYER_SPOTTED_BY_LOST))		
							OR (missionProgress = STAGE_DRIVE_TO_LS)	
								UPDATE_AI_PED_BLIP(ped[iTemp].id, ped[iTemp].aiBlip,-1,null,false,false)//),IS_PLAYER_USING_TREVORS_SPECIAL_ABILITY(),FALSE)
							ELSE
								UPDATE_AI_PED_BLIP(ped[iTemp].id, ped[iTemp].aiBlip,-1,null,false,TRUE)
							ENDIF
							
							IF NOT ped[iTemp].registeredKill
								IF DOES_ENTITY_EXIST(ped[iTemp].id)	
									IF IS_PED_FUCKED(ped[iTemp].id)
										ped[iTemp].registeredKill = TRUE
										IF WAS_PED_KILLED_BY_STEALTH(ped[iTemp].id)									
											CPRINTLN(debug_Trevor3,"Stealth kill stat")
											INFORM_MISSION_STATS_OF_INCREMENT(TRV3_STEALTH_KILLS)
										ENDIF
										CPRINTLN(debug_Trevor3,"kill stat")
										INFORM_MISSION_STATS_OF_INCREMENT(TRV3_KILLS)
									ENDIF
								ENDIF
							ENDIF																					
							
						ENDIF
					ENDREPEAT
				BREAK
				
				CASE ACT_BEAT_UP_GUY_REACTS_TO_TREVOR
					//cprintln(DEBUG_TREVOR3,"ACT_BEAT_UP_GUY_REACTS_TO_TREVOR")
					IF NOT IS_PED_FUCKED(ped[ped_fought].id)
						SET_PED_RELATIONSHIP_GROUP_HASH(ped[ped_fought].id,relGroupFoughtBiker)
						IF IS_CONDITION_TRUE(COND_FOUGHT_GUY_FELL)
							//cprintln(DEBUG_TREVOR3,"COND_FOUGHT_GUY_FELL TRUE")
							SET_ENTITY_HEALTH(ped[ped_fought].id,0)
						ELSE
							//cprintln(DEBUG_TREVOR3,"COND_FOUGHT_GUY_FELL FALSE")
							TASK_SMART_FLEE_PED(ped[ped_fought].id,player_ped_id(),1000,9999)
							SET_PED_KEEP_TASK(ped[ped_fought].id,TRUE)
							SET_PED_AS_NO_LONGER_NEEDED(ped[ped_fought].id)
						ENDIF
					ENDIF
					
					
					
					IF NOT IS_PED_FUCKED(ped[ped_fighterA].id)
						IF NOT IS_PED_IN_COMBAT(ped[ped_fighterA].id)
							startSeq()
							TASK_LOOK_AT_ENTITY(null,player_ped_id(),3000)
							TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),2000)
							TASK_COMBAT_PED(null,player_ped_id())
							endseq(ped[ped_fighterA].id)
						ENDIf
					ENDIF
					
					IF NOT IS_PED_FUCKED(ped[ped_fighterB].id)
						IF NOT IS_PED_IN_COMBAT(ped[ped_fighterB].id)						
							startSeq()
							TASK_LOOK_AT_ENTITY(null,player_ped_id(),3000)
							TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),2000)
							TASK_COMBAT_PED(null,player_ped_id())
							endseq(ped[ped_fighterB].id)
						ENDIf
					ENDIF
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_UNFIX_BIKES_FROM_SYNC_SCENES					
					if IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)								
						STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_driveOffA].id,SLOW_BLEND_OUT,true)
					endif
					
					if IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)								
						STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_driveOffB].id,SLOW_BLEND_OUT,true)
					endif
					
					if IS_VEHICLE_SAFE(vehicle[veh_chatA].id)								
						STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_chatA].id,SLOW_BLEND_OUT,true)
					endif
					
					if IS_VEHICLE_SAFE(vehicle[veh_chatB].id)								
						STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_chatB].id,SLOW_BLEND_OUT,true)
					endif
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_MECHANIC_ANIM_RESETS_AFTER_INTERUPTION
					IF NOT IS_PED_FUCKED(ped[ped_fixer].id)	
						SWITCH actions[thisI].flag
							CASE 0
								IF GET_SCRIPT_TASK_STATUS(ped[ped_fixer].id,SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
									actions[thisI].intB = GET_REACTION_ARRAY_ENTRY(ped[ped_fixer].id,pedReactionState)
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
								IF pedReactionState[actions[thisI].intB].state = STATE_RESTART_AMBIENT
									actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 58.162, 3638.364, 39.438 >>, << 0.000, 0.000, -157.250 >>)
									TASK_SYNCHRONIZED_SCENE (ped[ped_fixer].id, actions[thisI].intA, "misstrevor3", "brokendown_longlook", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
									SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intA,TRUE)
									pedReactionState[actions[thisI].intB].state = STATE_AMBIENT
								ENDIF
							BREAK					
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE ACT_FIND_ANIM_TO_TRIGGER
					actions[thisI].intA = -1
					float ClosestAnimRange 
					ClosestAnimRange  = 5000.0
					float animRange
					IF NOT IS_CONDITION_TRUE(COND_FIXER_ANIM_FINISHED)
						animRange =  GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<58.668655,3634.281494,35.984818>> )
						IF animRange < 13.562500
							if animRange < ClosestAnimRange
								ClosestAnimRange = animRange
								actions[thisI].intA = 0
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_CONDITION_TRUE(COND_TRAILER_ANIM_FINISHED)
						animRange =  GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<67.412399,3697.422363,38.765015>> )
						IF animRange < 27.25
							if animRange < ClosestAnimRange
								ClosestAnimRange = animRange
								actions[thisI].intA = 1
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_CONDITION_TRUE(COND_BIKER_DRIVE_ANIM_FINISHED)
						IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<93.893196,3706.537354,38.695930>>, <<69.317497,3670.831299,40.400547>>, 20.125000)
							animRange =  GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<51.407944,3696.901123,38.754974>> )
							IF animRange < 46.062500
								if animRange < ClosestAnimRange
									ClosestAnimRange = animRange
									actions[thisI].intA = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_CONDITION_TRUE(COND_BIKER_CHATTING_ANIM_FINISHED)
						animRange =  GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<93.405701,3692.200439,34.614063>> )
						
						IF not IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<51.151649,3681.861084,34.784412>>, <<87.650787,3735.440918,44.137173>>, 20.812500)
							IF animRange < 25
								if animRange < ClosestAnimRange
									ClosestAnimRange = animRange
									actions[thisI].intA = 3
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_CONDITION_TRUE(COND_BEATING_UP_ANIM_FINISHED)
					
							animRange =  GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 67.602, 3743.562, 38.932 >>)//<<59.083553,3728.930664,38.590771>> )
							IF animRange < 50
								if animRange < ClosestAnimRange
									ClosestAnimRange = animRange
									actions[thisI].intA = 4
								ENDIF
							ENDIF
						
					ENDIF
					
					IF NOT IS_CONDITION_TRUE(COND_COFFIN_ANIM_FINISHED)
						animRange =  GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<100.714699,3745.973145,38.747395>> )
						IF animRange < 31.8
							if animRange < ClosestAnimRange
								ClosestAnimRange = animRange
								actions[thisI].intA = 5
							ENDIF
						ENDIF
					ENDIF
					
					
				BREAK
				
				CASE ACT_TRAILER_ANIM_PLAYS_OUT
					IF NOT IS_PED_INJURED(ped[ped_nookie].id)
						SWITCH actions[thisI].flag
							case 0
								actions[thisI].intB = GET_REACTION_ARRAY_ENTRY(ped[ped_nookie].id,pedReactionState)
								IF actions[thisI].intB != -1
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
								IF pedReactionState[actions[thisI].intB].state != STATE_AMBIENT
									actions[thisI].flag = 3
									actions[thisI].floatA = 0.03
								ELSE
									IF GET_ACTION_INTA(5,ACT_FIND_ANIM_TO_TRIGGER) = 1
										iTrailerAnim = CREATE_SYNCHRONIZED_SCENE(<< 67.817, 3694.449, 38.766 >>, << -0.000, 0.000, -35.190 >>)
										TASK_SYNCHRONIZED_SCENE (ped[ped_nookie].id, iTrailerAnim, "misstrevor3", "horny_biker", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
										SET_SYNCHRONIZED_SCENE_LOOPED(iTrailerAnim,TRUE)
										actions[thisI].flag = 2
									ENDIF
								ENDIF
							BREAK
							CASE 3
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iTrailerAnim)
									actions[thisI].flag = 2
								ENDIF
							BREAK
							DEFAULT
								IF IS_SYNCHRONIZED_SCENE_RUNNING(iTrailerAnim)
									actions[thisI].floatA = GET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim)			
								ENDIF
								
								IF pedReactionState[actions[thisI].intB].state = STATE_REACT
								OR pedReactionState[actions[thisI].intB].state = STATE_RESTART_AMBIENT								
									If pedReactionState[actions[thisI].intB].state = STATE_RESTART_AMBIENT
										pedReactionState[actions[thisI].intB].state = STATE_AMBIENT
										IF actions[thisI].floatA > 0.8 OR actions[thisI].floatA < 0.04
											startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<65.9500, 3694.1870, 38.7549>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.2,ENAV_DONT_ADJUST_TARGET_POSITION)
											TASK_ACHIEVE_HEADING(null,241)
											endseq(ped[ped_nookie].id)
											actions[thisI].flag = 4
										elif actions[thisI].floatA>= 0.04 and actions[thisI].floatA < 0.188
											startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<65.3946, 3691.2192, 38.7496>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.2,ENAV_DONT_ADJUST_TARGET_POSITION)
											TASK_ACHIEVE_HEADING(null,241)
											endseq(ped[ped_nookie].id)
											actions[thisI].flag = 5
										elif actions[thisI].floatA > 0.188 and	actions[thisI].floatA < 0.475
											startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<69.3267, 3698.3433, 38.7549>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.2,ENAV_DONT_ADJUST_TARGET_POSITION)
											TASK_ACHIEVE_HEADING(null,241)
											endseq(ped[ped_nookie].id)
											actions[thisI].flag = 6
										else
											startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<71.0567, 3701.3481, 38.7549>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.2,ENAV_DONT_ADJUST_TARGET_POSITION)
											TASK_ACHIEVE_HEADING(null,241)
											endseq(ped[ped_nookie].id)
											actions[thisI].flag = 7
										endif
										
									ENDIF
								ELIf pedReactionState[actions[thisI].intB].state = STATE_AMBIENT
									IF actions[thisI].flag >= 4 AND actions[thisI].flag <= 7
										IF NOT IS_PED_FUCKED(ped[ped_nookie].id)	
											IF GET_SCRIPT_TASK_STATUS(ped[ped_nookie].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
												iTrailerAnim = CREATE_SYNCHRONIZED_SCENE(<< 67.817, 3694.449, 38.766 >>, << -0.000, 0.000, -35.190 >>)
												TASK_SYNCHRONIZED_SCENE (ped[ped_nookie].id, iTrailerAnim, "misstrevor3", "horny_biker", 0.5, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,rbf_none,slow_blend_in)
												SWITCH actions[thisI].flag
													CASE 4 SET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim,0.04) BREAK
													CASE 5 SET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim,0.188) BREAK 
													CASE 6 SET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim,0.475) BREAK
													CASE 7 SET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim,0.8) BREAK
												ENDSWITCH
												SET_SYNCHRONIZED_SCENE_LOOPED(iTrailerAnim,TRUE)
												actions[thisI].flag = 3
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH		
					ENDIF
				BREAK
				
				CASE ACT_SEATED_BIKER_REACTIONS
					SWITCH actions[thisI].flag
						case 0
							IF pedReactionState[ped_chattingA].state = STATE_REACT
							OR pedReactionState[ped_chattingB].state = STATE_REACT
								actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 88.368, 3701.228, 38.535 >>, << -0.000, 0.000, -7.920 >>)
								actions[thisI].intB = CREATE_SYNCHRONIZED_SCENE(<< 88.368, 3701.228, 38.535 >>, << -0.000, 0.000, -7.920 >>)
								IF NOT IS_PED_INJURED(ped[ped_chattingA].id)													
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_chattingA].id,true)
									TASK_SYNCHRONIZED_SCENE (ped[ped_chattingA].id, actions[thisI].intA, "misstrevor3", "biker_exit_1", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
								ENDIF
								
								IF NOT IS_PED_INJURED(ped[ped_chattingB].id)													
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_chattingB].id,true)
									TASK_SYNCHRONIZED_SCENE (ped[ped_chattingB].id, actions[thisI].intB, "misstrevor3", "biker_exit_2", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
								ENDIF
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intB)
								IF GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intB) > 0.378
									IF NOT IS_PED_INJURED(ped[ped_chattingB].id)							
										CLEAR_PED_TASKS(ped[ped_chattingB].id)																						
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_chattingB].id,FALSE)
										SET_PED_HELMET(ped[ped_chattingB].id,FALSE)
										pedReactionState[ped_chattingB].scriptReactionFirst = FALSE
									ENDIF
								//	if IS_VEHICLE_SAFE(vehicle[veh_chatA].id)
								//		STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_chatA].id,SLOW_BLEND_OUT,true)
								//	ENDIF
									actions[thisI].flag++
								ENDIF
							ELSE
								IF IS_VEHICLE_SAFE(vehicle[veh_chatA].id)
									STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_chatA].id,SLOW_BLEND_OUT,true)
								ENDIF
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
								IF GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intA) > 0.560
									
									IF NOT IS_PED_INJURED(ped[ped_chattingA].id)							
										CLEAR_PED_TASKS(ped[ped_chattingA].id)																						
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_chattingA].id,FALSE)
										SET_PED_HELMET(ped[ped_chattingA].id,FALSE)
										pedReactionState[ped_chattingA].scriptReactionFirst = FALSE
										
									ENDIF
								//	if IS_VEHICLE_SAFE(vehicle[veh_chatB].id)
								//		STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_chatB].id,SLOW_BLEND_OUT,true)
								//	ENDIF
									
									actions[thisI].completed = TRUE
								ENDIF
							ELSE
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_STOP_BOMBS_ON_TRAILER_DOOR_BEING_REMOVED_AT_RANGE
					SWITCH actions[thisI].flag
						case 0
							IF NEW_LOAD_SCENE_START_SPHERE(<<30.9745, 3665.2126, 40.9901>>,5.0)
								CREATE_FORCED_OBJECT(<<32.3311, 3667.6604, 40.4431>>,3.0,prop_cs4_05_tdoor,true)
								CREATE_FORCED_OBJECT(<<29.10, 3661.49, 40.85>>,3.0,prop_magenta_door,true)
								
								CPRINTLN(debug_trevor3,"NEW LOAD SCENE STARTED")
								actions[thisI].flag = 1
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_BIKER_REACTIONS
					
					
						SWITCH actions[thisI].flag
							case 0
								IF pedReactionState[ped_driveOffA].state = STATE_REACT
								OR pedReactionState[ped_driveOffB].state = STATE_REACT
									IF IS_SYNCHRONIZED_SCENE_RUNNING(iBikersDriveOff)
										IF NOT IS_PED_INJURED(ped[ped_driveOffA].id)																					
											IF GET_SYNCHRONIZED_SCENE_PHASE(iBikersDriveOff) < 0.23
											OR GET_SYNCHRONIZED_SCENE_PHASE(iBikersDriveOff) > 0.96
												actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 51.367, 3697.315, 38.756 >>, << -0.000, 0.000, -28.440 >>)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_driveOffA].id,TRUE)
												TASK_SYNCHRONIZED_SCENE (ped[ped_driveOffA].id, actions[thisI].intA, "misstrevor3", "bike_chat_b_loop_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
												SET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intA,0.23)											
												if IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
													PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle[veh_driveOffA].id,actions[thisI].intA, "bike_chat_b_loop_bike_a","misstrevor3", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
												endif																								
											ELSE
												CLEAR_PED_TASKS(ped[ped_driveOffA].id)
												pedReactionState[ped_driveOffA].scriptReactionFirst = FALSE
											ENDIF											
										ENDIF																				
								
									
										IF IS_SYNCHRONIZED_SCENE_RUNNING(iBikersDriveOffB)
											SET_SYNCHRONIZED_SCENE_RATE(iBikersDriveOffB,2.0)
										ENDIF
										
										IF NOT IS_PED_INJURED(ped[ped_driveOffB].id)										
											actions[thisI].intB = CREATE_SYNCHRONIZED_SCENE(<< 51.367, 3697.315, 38.756 >>, << -0.000, 0.000, -28.440 >>)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_driveOffB].id,TRUE)
											TASK_SYNCHRONIZED_SCENE (ped[ped_driveOffB].id, actions[thisI].intB, "misstrevor3", "bike_chat_b_outro_2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT )
											if IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
												PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle[veh_driveOffB].id,actions[thisI].intB,"bike_chat_b_outro_bike_b","misstrevor3", NORMAL_BLEND_IN, SLOW_BLEND_OUT)//,enum_to_int(SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT))
											endif
											SET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intB,0.373)
											SET_SYNCHRONIZED_SCENE_RATE(actions[thisI].intB,2.0)										
										ENDIF
										actions[thisI].flag++
									ELSE
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF
							BREAK
							CASE 1
								IF (IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA) AND GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intA) > 0.28)
									IF NOT IS_PED_INJURED(ped[ped_driveOffA].id)
										//cprintln(debug_Trevor3,"CLEAR PED A1")
										CLEAR_PED_TASKS(ped[ped_driveOffA].id)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_driveOffA].id,FALSE)
										SET_PED_HELMET(ped[ped_driveOffA].id,FALSE)
										pedReactionState[ped_driveOffA].scriptReactionFirst = FALSE
									ENDIF
									if IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
										STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_driveOffA].id,SLOW_BLEND_OUT,true)
									ENDIF
										
								ENDIF
								IF IS_SYNCHRONIZED_SCENE_RUNNING(iBikersDriveOffB) AND GET_SYNCHRONIZED_SCENE_PHASE(iBikersDriveOffB) > 0.41
									IF NOT IS_PED_INJURED(ped[ped_driveOffA].id)
										IF GET_ENTITY_HEALTH(ped[ped_driveOffA].id) >= 200
											CLEAR_PED_TASKS(ped[ped_driveOffA].id)
											IF IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)	
												SET_PED_INTO_VEHICLE(ped[ped_driveOffA].id,vehicle[veh_driveOffA].id)
											ENDIF
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_driveOffA].id,FALSE)
											SET_PED_HELMET(ped[ped_driveOffA].id,FALSE)
										ENDIF
									ENDIF
									
									if IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
										STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_driveOffA].id,SLOW_BLEND_OUT,true) //stop chjecking veh alive cos it wouldn't stop a blown up veh from stopping anim
									ENDIF
	
									pedReactionState[ped_driveOffA].scriptReactionFirst = FALSE
								ENDIF
								IF IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intB)
									IF GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intB) > 0.731
										IF NOT IS_PED_INJURED(ped[ped_driveOffB].id)
											IF GET_ENTITY_HEALTH(ped[ped_driveOffB].id) >= 200
												//cprintln(debug_Trevor3,"CLEAR PED B1")
												CLEAR_PED_TASKS(ped[ped_driveOffB].id)
												
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_driveOffB].id,FALSE)
												SET_PED_HELMET(ped[ped_driveOffB].id,FALSE)
												pedReactionState[ped_driveOffB].scriptReactionFirst = FALSE
												IF IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
													SET_PED_INTO_VEHICLE(ped[ped_driveOffB].id,vehicle[veh_driveOffB].id)
												ENDIF
											ENDIF
										ENDIF
										if IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
											STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_driveOffB].id,SLOW_BLEND_OUT,true)
										ENDIF
									ENDIF
								ENDIF
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
								AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intB)
								AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iBikersDriveOff)
									
									pedReactionState[ped_driveOffB].scriptReactionFirst = FALSE
									pedReactionState[ped_driveOffA].scriptReactionFirst = FALSE
									actions[thisI].completed = TRUE
								ENDIF
							BREAK
						ENDSWITCH
				
				BREAK
				
				CASE ACT_BIKERS_DRIVE_OFF_ANIM_PLAYS_OUT
					IF GET_ACTION_FLAG(29,ACT_BIKER_REACTIONS) > 0
						actions[thisI].completed = TRUE
					ELSE
						if (actions[thisI].flag < 4 AND NOT IS_PED_FUCKED(ped[ped_driveOffA].id) AND NOT IS_PED_FUCKED(ped[ped_driveOffB].id))
						or (actions[thisI].flag > 3  AND NOT IS_PED_FUCKED(ped[ped_driveOffB].id))
							SWITCH actions[thisI].flag
								case 0
									IF IS_SYNCHRONIZED_SCENE_RUNNING(iBikersDriveOff)
								
										IF GET_SYNCHRONIZED_SCENE_PHASE(iBikersDriveOff) < 0.23
										OR GET_SYNCHRONIZED_SCENE_PHASE(iBikersDriveOff) > 0.98
											iBikersDriveOffB = CREATE_SYNCHRONIZED_SCENE(<< 51.367, 3697.315, 38.756 >>, << -0.000, 0.000, -28.440 >>)
											iBikersDriveOffc = CREATE_SYNCHRONIZED_SCENE(<< 51.367, 3697.315, 38.756 >>, << -0.000, 0.000, -28.440 >>)
											TASK_SYNCHRONIZED_SCENE (ped[ped_driveOffA].id, iBikersDriveOffB, "misstrevor3", "bike_chat_b_outro_1", SLOW_BLEND_IN, slow_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE) 
											TASK_SYNCHRONIZED_SCENE (ped[ped_driveOffB].id, iBikersDriveOffc, "misstrevor3", "bike_chat_b_outro_2", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
											
											if IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
												PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle[veh_driveOffA].id,iBikersDriveOffB, "bike_chat_b_outro_bike_a","misstrevor3", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
											endif
											
											if IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
												PLAY_SYNCHRONIZED_ENTITY_ANIM_EXTRA(vehicle[veh_driveOffB].id,iBikersDriveOffc, "bike_chat_b_outro_bike_b","misstrevor3", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
											endif
											
											SET_PED_HELMET(ped[ped_driveOffA].id,false)										
											SET_PED_HELMET(ped[ped_driveOffb].id,false)		
											SET_SYNCHRONIZED_SCENE_PHASE(iBikersDriveOffB,0.0)
											
											REQUEST_VEHICLE_RECORDING(2,"trev3")
											REQUEST_VEHICLE_RECORDING(3,"trev3")
											actions[thisI].flag++
										ENDIF
									ENDIF
								break
								case 1	
									IF IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
										IF IS_SYNCHRONIZED_SCENE_RUNNING(iBikersDriveOffB)
											if GET_SYNCHRONIZED_SCENE_PHASE(iBikersDriveOffB) >= 0.9
												IF HAS_VEHICLE_RECORDING_BEEN_LOADED(2,"trev3")
													IF NOT IS_PED_FUCKED(ped[ped_driveOffA].id)
														STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_driveOffA].id,SLOW_BLEND_OUT,true)
														CLEAR_PED_TASKS(ped[ped_driveOffA].id)
														SET_PED_INTO_VEHICLE(ped[ped_driveOffA].id,vehicle[veh_driveOffA].id)
														SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(ped[ped_driveOffA].id,KNOCKOFFVEHICLE_NEVER)
														FREEZE_ENTITY_POSITION(vehicle[veh_driveOffA].id,FALSE)
														START_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_driveOffA].id,2,"trev3")
														actions[thisI].flag++
													ENDIF
												ENDIF
											endif
										ENDIF
									ENDIF
								BREAK
								case 2
									IF IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
										IF IS_SYNCHRONIZED_SCENE_RUNNING(iBikersDriveOffc)
											if GET_SYNCHRONIZED_SCENE_PHASE(iBikersDriveOffc) >= 1.0
												IF HAS_VEHICLE_RECORDING_BEEN_LOADED(3,"trev3")
													IF NOT IS_PED_FUCKED(ped[ped_driveOffB].id)
														STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[veh_driveOffB].id,SLOW_BLEND_OUT,true)
									
														CLEAR_PED_TASKS(ped[ped_driveOffB].id)
														SET_PED_INTO_VEHICLE(ped[ped_driveOffB].id,vehicle[veh_driveOffB].id)
														SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(ped[ped_driveOffB].id,KNOCKOFFVEHICLE_NEVER)
														FREEZE_ENTITY_POSITION(vehicle[veh_driveOffB].id,FALSE)
														START_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_driveOffB].id,3,"trev3")
														actions[thisI].flag++
													ENDIF
												ENDIF
											endif
										ENDIF
									ENDIF
								break

								case 3
									IF NOT IS_PED_FUCKED(ped[ped_driveOffA].id)
										IF IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
											if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_driveOffA].id)
												//if GET_SCRIPT_TASK_STATUS(ped[ped_driveOffA].id,script_task_any) != PERFORMING_TASK
													startSeq()
													TASK_VEHICLE_DRIVE_TO_COORD(null,vehicle[veh_driveOffA].id,<< 185.8774, 3393.5964, 37.0011 >>,18.0,DRIVINGSTYLE_NORMAL,HEXER,DRIVINGMODE_STOPFORCARS,10.0,10.0)
													TASK_VEHICLE_DRIVE_WANDER(null,vehicle[veh_driveOffA].id,15.0,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
													endseq(ped[ped_driveOffA].id)
													SET_PED_KEEP_TASK(ped[ped_driveOffA].id,true)
													SET_PED_AS_NO_LONGER_NEEDED(ped[ped_driveOffA].id)
													SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_driveOffA].id)
													actions[thisI].flag++
												//ENDIF
											ENDIF					
										ENDIF
									ELSE
										IF IS_VEHICLE_SAFE(vehicle[veh_driveOffA].id)
											if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_driveOffA].id)
												STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_driveOffA].id)
												actions[thisI].flag++
											ENDIF
										ENDIF
									ENDIF
								break
								case 4
									IF NOT IS_PED_FUCKED(ped[ped_driveOffB].id)
										IF IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
											if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_driveOffB].id)
												//if GET_SCRIPT_TASK_STATUS(ped[ped_driveOffB].id,script_task_any) != PERFORMING_TASK
													startSeq()
													TASK_VEHICLE_DRIVE_TO_COORD(null,vehicle[veh_driveOffB].id,<< 185.8774, 3393.5964, 37.0011 >>,18.0,DRIVINGSTYLE_NORMAL,HEXER,DRIVINGMODE_STOPFORCARS,10.0,10.0)
													TASK_VEHICLE_DRIVE_WANDER(null,vehicle[veh_driveOffB].id,15.0,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
													endseq(ped[ped_driveOffB].id)
													SET_PED_KEEP_TASK(ped[ped_driveOffB].id,true)
													SET_PED_AS_NO_LONGER_NEEDED(ped[ped_driveOffB].id)
													SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_driveOffB].id)
													actions[thisI].flag++
												//ENDIF
											ENDIF					
										ENDIF
									ELSE
										IF IS_VEHICLE_SAFE(vehicle[veh_driveOffB].id)
											if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[veh_driveOffB].id)
												STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[veh_driveOffB].id)
												actions[thisI].flag++
											ENDIF
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				BREAK
						
				CASE ACT_BEAT_UP_ANIMATION_PLAYS_OUT
					SWITCH actions[thisI].flag
						case 0											
							if not IS_PED_FUCKED(PED[ped_fighterA].id) and not IS_PED_FUCKED(PED[ped_fighterB].id) and not IS_PED_FUCKED(PED[ped_fought].id)										
								actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 67.711, 3743.775, 38.915 >>,<< -0.000, 0.000, -29.880 >>)
								TASK_SYNCHRONIZED_SCENE (PED[ped_fought].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_mainaction_dockworker", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
								TASK_SYNCHRONIZED_SCENE (PED[ped_fighterA].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_mainaction_guard1", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
								TASK_SYNCHRONIZED_SCENE (PED[ped_fighterB].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_mainaction_guard2", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )			
								
								SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intA,FALSE)
								actions[thisI].flag++
							ENDIF									
						BREAK
				
						case 1																	
							IF IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
								IF GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intA) > 0.877
									SET_CONDITION_STATE(COND_FOUGHT_GUY_FELL,TRUE)
									REMOVE_PED_FROM_REACT_ARRAY(pedReactionState,ped_fought)
								ENDIF
								if GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intA) > 0.99
							//	OR IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(actions[thisI].intA)
									if not IS_PED_FUCKED(PED[ped_fighterA].id) and not IS_PED_FUCKED(PED[ped_fighterB].id) and not IS_PED_FUCKED(PED[ped_fought].id)
										actions[thisI].intB = CREATE_SYNCHRONIZED_SCENE(<< 67.711, 3743.775, 38.915 >>,<< -0.000, 0.000, -29.880 >>)
										TASK_SYNCHRONIZED_SCENE (PED[ped_fought].id, actions[thisI].intB, "misstrevor3_beatup", "guard_beatup_kickidle_dockworker", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
										TASK_SYNCHRONIZED_SCENE (PED[ped_fighterA].id, actions[thisI].intB, "misstrevor3_beatup", "guard_beatup_kickidle_guard1", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
										TASK_SYNCHRONIZED_SCENE (PED[ped_fighterB].id, actions[thisI].intB, "misstrevor3_beatup", "guard_beatup_kickidle_guard2", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )			
										SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intB,true)																					
										
										actions[thisI].flag=GET_GAME_TIMER()+100
										actions[thisI].intA = GET_GAME_TIMER() + 15000
									ENDIF
								ENDIF
							ELSE
								if not IS_PED_FUCKED(PED[ped_fighterA].id) and not IS_PED_FUCKED(PED[ped_fighterB].id) and not IS_PED_FUCKED(PED[ped_fought].id)
									/*
									actions[thisI].intB = CREATE_SYNCHRONIZED_SCENE(<< 67.711, 3743.775, 38.915 >>,<< -0.000, 0.000, -29.880 >>)
									TASK_SYNCHRONIZED_SCENE (PED[ped_fought].id, actions[thisI].intB, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_dockworker", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
									TASK_SYNCHRONIZED_SCENE (PED[ped_fightera].id, actions[thisI].intB, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_guard1", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
									TASK_SYNCHRONIZED_SCENE (PED[ped_fighterB].id, actions[thisI].intB, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_guard2", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )			
									SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intB,true)												
									actions[thisI].flag=GET_GAME_TIMER()+700*/
									actions[thisI].completed=TRUE
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)	
								if GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intA) > 0.97
									if not IS_PED_FUCKED(PED[ped_fighterA].id)
									and not IS_PED_FUCKED(PED[ped_fighterB].id)
									and not IS_PED_FUCKED(PED[ped_fought].id)
										SET_ENTITY_HEALTH(PED[ped_fought].id,0)
										if not IS_PED_FUCKED(PED[ped_fighterA].id)
											startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<71.1857, 3744.8770, 38.8392>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,210)
											//TASK_ACHIEVE_HEADING(null,210)
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_AA_SMOKE",0,TRUE)
											endseq(PED[ped_fighterA].id)
										ENDIF
										if not IS_PED_FUCKED(PED[ped_fighterB].id)
											startSeq()
											TASK_LOOK_AT_ENTITY(null,PED[ped_fighterA].id,2000)
											TASK_PAUSE(null,2000)											
											TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<69.5081, 3744.3945, 38.8998>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,243)
											//TASK_ACHIEVE_HEADING(null,243)
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_SMOKING_POT",0,TRUE)
											endseq(PED[ped_fighterB].id)
										ENDIF
										actions[thisI].completed=TRUE
									endif
								ENDIF
							ENDIF
						BREAK
					
						DEFAULT
							IF IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intB)						
								IF GET_GAME_TIMER() > actions[thisI].flag								
									IF GET_GAME_TIMER() > actions[thisI].intA
										if not IS_PED_FUCKED(PED[ped_fighterA].id)
										and not IS_PED_FUCKED(PED[ped_fighterB].id)
										and not IS_PED_FUCKED(PED[ped_fought].id)
											actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 67.711, 3743.775, 38.915 >>,<< -0.000, 0.000, -29.880 >>)
											TASK_SYNCHRONIZED_SCENE (PED[ped_fought].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_exit_dockworker", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
											TASK_SYNCHRONIZED_SCENE (PED[ped_fighterA].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_exit_guard1", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
											TASK_SYNCHRONIZED_SCENE (PED[ped_fighterB].id, actions[thisI].intA, "misstrevor3_beatup", "guard_beatup_exit_guard2", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )			
										ENDIF
										/*
										if not IS_PED_FUCKED(PED[ped_fighterA].id)
											startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<71.1857, 3744.8770, 38.8392>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,210)
											//TASK_ACHIEVE_HEADING(null,210)
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_AA_SMOKE",0,TRUE)
											endseq(PED[ped_fighterA].id)
										ENDIF
										if not IS_PED_FUCKED(PED[ped_fighterB].id)
											startSeq()
											TASK_LOOK_AT_ENTITY(null,PED[ped_fought].id,2000)
											TASK_PAUSE(null,2000)											
											TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<69.5081, 3744.3945, 38.8998>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,243)
											//TASK_ACHIEVE_HEADING(null,243)
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_SMOKING_POT",0,TRUE)
											endseq(PED[ped_fighterB].id)
										ENDIF
										if not IS_PED_FUCKED(PED[ped_fought].id)
											SET_ENTITY_HEALTH(PED[ped_fought].id,0)
										ENDIF*/
										actions[thisI].flag=2
									ELSE
										TRIGGER_SONAR_BLIP(<< 67.602, 3743.562, 38.932 >>,30,HUD_COLOUR_RED)
										UPDATE_AI_PED_BLIP(PED[ped_fighterA].id,PED[ped_fighterA].aiBlip,-1,null,true)
										UPDATE_AI_PED_BLIP(PED[ped_fighterB].id,PED[ped_fighterA].aiBlip,-1,null,true)
										actions[thisI].flag = GET_GAME_TIMER() + 2000
									ENDIF
								ENDIF
							ELSE
								actions[thisI].completed=TRUE
							ENDIF
						break
					ENDSWITCH
				BREAK
				
				CASE ACT_COFFIN_GUY_DROPS_BOTTLE
					IF DOES_ENTITY_EXIST(ped[ped_coffins].id)
						IF IS_PED_FUCKED(ped[ped_coffins].id)
						OR IS_CONDITION_TRUE(COND_PLAYER_SPOTTED_BY_LOST)
							IF IS_ENTITY_ATTACHED_TO_ANY_PED(obj[obj_beer].id)
								DETACH_ENTITY(obj[obj_beer].id)
								actions[thisI].completed = TRUE
							ENDIF
							
							IF DOES_ENTITY_EXIST(ped[ped_coffins].id)							
								IF DOES_ENTITY_EXIST(obj[obj_coffinA].id)
								//	DETACH_ENTITY(obj[obj_coffinA].id,FALSE,FALSE)
									ACTIVATE_PHYSICS(obj[obj_coffinA].id)
									actions[thisI].completed = TRUE
								ENDIF								
							ENDIF
						
						ENDIF
					ENDIF
				BREAK
				
				CASE ACT_DETACH_COFFIN
					IF IS_VEHICLE_SAFE(vehicle[veh_coffin].id)
						IF NOT IS_ENTITY_DEAD(obj[obj_coffinA].id) 
							IF GET_ENTITY_SPEED(vehicle[veh_coffin].id) > 1
								IF IS_ENTITY_ATTACHED(obj[obj_coffinA].id)
									DETACH_ENTITY(obj[obj_coffinA].id,TRUE,FALSE)
									actions[thisI].completed = TRUE
								ENDIF						
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ACT_TRAILER_RAYFIRES

					REPEAT COUNT_OF(rayfireTrailer) i
						IF rayfireTrailer[i].state = PRIMING
							IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_ROCKET,rayfireTrailer[i].vLocates[0],rayfireTrailer[i].vLocates[1],rayfireTrailer[i].fLength)
							OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_STICKYBOMB,rayfireTrailer[i].vLocates[0],rayfireTrailer[i].vLocates[1],rayfireTrailer[i].fLength)
							OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_SHIP_DESTROY,rayfireTrailer[i].vLocates[0],rayfireTrailer[i].vLocates[1],rayfireTrailer[i].fLength)
							OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_GRENADELAUNCHER,rayfireTrailer[i].vLocates[0],rayfireTrailer[i].vLocates[1],rayfireTrailer[i].fLength)
							OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_GRENADE,rayfireTrailer[i].vLocates[0],rayfireTrailer[i].vLocates[1],rayfireTrailer[i].fLength)
			
						
								rayfireTrailer[i].state = resetAndGo
								bTrailerBlown = TRUE
								IF DOES_BLIP_EXIST(rayfireTrailer[i].blip)
									REMOVE_BLIP(rayfireTrailer[i].blip)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				
					
					repeat COUNT_OF(rayfireTrailer) i		
						IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
							IF rayfireTrailer[i].state = resetAndGo or rayfireTrailer[i].state = go or rayfireTrailer[i].state = trigger
							OR (rayfireTrailer[i].state = playing and  GET_RAYFIRE_MAP_OBJECT_ANIM_PHASE(rayfireTrailer[i].index) < 0.05 and GET_RAYFIRE_MAP_OBJECT_ANIM_PHASE(rayfireTrailer[i].index) != 0.0)
								IF GET_GAME_TIMER() > rayfireTrailer[i].explosionTime				
									make_fake_explosion_for_trailer(i)
									rayfireTrailer[i].explosionTime = get_Game_timer()+ 200
									
								ENDIF
							endif
						endif
						
						SWITCH rayfireTrailer[i].state
							case init
								IF not DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
						
									rayfireTrailer[i].index = GET_RAYFIRE_MAP_OBJECT((rayfireTrailer[i].vLocates[0]+rayfireTrailer[i].vLocates[1])/2.0, 10, rayfireTrailer[i].rayfireName)
									
								ELSE
									
									if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_START	
										IF missionProgress > STAGE_TRAILER_PARK_CUT
											SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_PRIMING)
											SET_BUILDING_STATE(rayfireTrailer[i].occlusionName, BUILDINGSTATE_NORMAL)
											cprintln(debug_Trevor3,"switch occluders normal b")
											rayfireTrailer[i].state = PRIMING
										endif						
									else
										if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_PRIMED
											rayfireTrailer[i].state = PRIMING
										endif
									ENDIF
								ENDIF
							break
							
							case resetForSkip
								IF not DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
									
									rayfireTrailer[i].index = GET_RAYFIRE_MAP_OBJECT((rayfireTrailer[i].vLocates[0]+rayfireTrailer[i].vLocates[1])/2.0, 10, rayfireTrailer[i].rayfireName)
								ELSE		
									
									if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) != RFMO_STATE_START									
									and GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) != RFMO_STATE_STARTING
										SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_STARTING)
										SET_BUILDING_STATE(rayfireTrailer[i].occlusionName, BUILDINGSTATE_NORMAL)
										cprintln(debug_Trevor3,"switch occluders normal a")
										rayfireTrailer[i].explosionTime = 0 //used as a counter
										rayfireTrailer[i].state = INIT
									else
										rayfireTrailer[i].explosionTime = 0 //used as a counter
										rayfireTrailer[i].state = INIT
									endif					
								ENDIF		
								IF DOES_PARTICLE_FX_LOOPED_EXIST(rayfireTrailer[i].fx)
									STOP_PARTICLE_FX_LOOPED(rayfireTrailer[i].fx)			
								ENDIF
							BREAK

							case resetAndGo								
								IF not DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
									rayfireTrailer[i].index = GET_RAYFIRE_MAP_OBJECT((rayfireTrailer[i].vLocates[0]+rayfireTrailer[i].vLocates[1])/2.0, 10, rayfireTrailer[i].rayfireName)
								ELSE					
									if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_PRIMED
										//cprintln(debug_trevor3,"STARTFX")
										SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_START_ANIM)					
										cprintln(debug_Trevor3,"switch occluders c")
										SET_BUILDING_STATE(rayfireTrailer[i].occlusionName, BUILDINGSTATE_DESTROYED)
										rayfireTrailer[i].state = playing
										rayfireTrailer[i].explosionTime = get_Game_timer()+100
										SET_AMBIENT_ZONE_STATE(rayfireTrailer[i].audioName, FALSE, TRUE)
										//START_PARTICLE_FX_NON_LOOPED_AT_COORD("EXP_VFXTAG_TREV3_TRAILER",(rayfireTrailer[i].vLocates[0]+rayfireTrailer[i].vLocates[1])/2.0,<<0,0,0>>) //temp removal at request from Nick Greco
										rayfireTrailer[i].fx = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev3_trailer_plume",(rayfireTrailer[i].vLocates[0]+rayfireTrailer[i].vLocates[1])/2.0,<<0,0,0>>)
										//add fires
									else
										if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_START					
											SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_PRIMING)
										else
											if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) != RFMO_STATE_STARTING
											and GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) != RFMO_STATE_PRIMING
												SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_STARTING)
												cprintln(debug_Trevor3,"switch occluders d")
												SET_BUILDING_STATE(rayfireTrailer[i].occlusionName, BUILDINGSTATE_DESTROYED)
											endif
										ENDIF
									endif
								ENDIF		
								
							BREAK
							case skip_to_blown
							//	SET_BUILDING_STATE(rayfireTrailer[i].buildingName, BUILDINGSTATE_DESTROYED)
								SET_BUILDING_STATE(rayfireTrailer[i].occlusionName, BUILDINGSTATE_DESTROYED)
								cprintln(debug_Trevor3,"switch occluders2")
								IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
									SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_ENDING)
									rayfireTrailer[i].fx = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev3_trailer_plume",(rayfireTrailer[i].vLocates[0]+rayfireTrailer[i].vLocates[1])/2.0,<<0,0,0>>)
									rayfireTrailer[i].state = inactive
								ELSE								
									rayfireTrailer[i].index = GET_RAYFIRE_MAP_OBJECT((rayfireTrailer[i].vLocates[0]+rayfireTrailer[i].vLocates[1])/2.0, 10, rayfireTrailer[i].rayfireName)									
								ENDIF
							BREAK
							case go			
								IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
									if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_START
										SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_PRIMING) //RFMO_STATE_PRIMING)
										rayfireTrailer[i].state = trigger																																						
									endif
								ENDIF
							break
							case trigger				
								IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
									if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_PRIMED
										SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_START_ANIM)
										cprintln(debug_Trevor3,"switch occluders")
										SET_BUILDING_STATE(rayfireTrailer[i].occlusionName, BUILDINGSTATE_DESTROYED)
										rayfireTrailer[i].state = playing
									ENDIF
								ENDIF
							break
							case playing
							
								IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
									if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_END
										rayfireTrailer[i].state = inactive
									ENDIF
								//	if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_SYNC_ENDING
								//		SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_ENDING)
								//	endif
								ENDIF
							BREAK
							case endNow
								IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfireTrailer[i].index)
									if GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index) = RFMO_STATE_ANIMATING
										SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfireTrailer[i].index, RFMO_STATE_END)
										rayfireTrailer[i].state = inactive
									ENDIF
								ENDIF
							break
							case inactive //create fire damage
						
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),(rayfireTrailer[i].vLocates[0]+rayfireTrailer[i].vLocates[1])/2.0) < 2.3
									IF NOT IS_ENTITY_ON_FIRE(player_ped_id())
										START_ENTITY_FIRE(player_ped_id())
									ENDIF
								ENDIF
							break
						ENDSWITCH
					ENDREPEAT
				BREAK
				
				case ACT_REMOVE_TRAILER_BLIPS
					SWITCH actions[thisI].flag
						CASE 0
							actions[thisI].intA = COUNT_OF(rayfireTrailer)
							actions[thisI].flag++
						BREAK
						CASE 1
							REPEAT COUNT_OF(rayfireTrailer) iTemp
								IF DOES_BLIP_EXIST(rayfireTrailer[itemp].blip)
									IF rayfireTrailer[itemp].state = playing							
									OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(rayfireTrailer[itemp].vLocates[0], rayfireTrailer[itemp].vLocates[1],rayfireTrailer[itemp].fLength,WEAPONTYPE_STICKYBOMB,TRUE)							
									
										REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGH)
									
										REMOVE_BLIP(rayfireTrailer[itemp].blip)
										actions[thisI].intA--
									ENDIF
								ELSE
									IF NOT IS_PROJECTILE_TYPE_IN_ANGLED_AREA(rayfireTrailer[itemp].vLocates[0], rayfireTrailer[itemp].vLocates[1],rayfireTrailer[itemp].fLength,WEAPONTYPE_STICKYBOMB,TRUE)	
									AND (rayfireTrailer[itemp].state = PRIMING)
										actions[thisI].intA++
										rayfireTrailer[itemp].blip = CREATE_BLIP_FOR_COORD(GET_TRAILER_MIDPOINT(iTemp))
									ENDIF
								ENDIF
							ENDREPEAT
							
							IF actions[thisI].intA = 0
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				break
				
				case ACT_ENSURE_ONE_PED_ENGAGING_PLAYER
					SWITCH actions[thisI].flag
						CASE 0
							actions[thisI].intA = GET_RANDOM_INT_IN_RANGE(1,COUNT_OF(ped)-1)
							IF NOT IS_PED_FUCKED(ped[actions[thisI].intA].id)
							AND actions[thisI].intA != ped_fought
								REMOVE_PED_DEFENSIVE_AREA(ped[actions[thisI].intA].id)
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_PED_FUCKED(ped[actions[thisI].intA].id)
								actions[thisI].flag = 0
							ENDIF							
						BREAK
					ENDSWITCH
				break
				
				CASE ACT_CONTROL_MUSIC
					SWITCH actions[thisI].flag
						CASE 0
							PLAYMUSIC("TRV3_GAMEPLAY_ST",FALSE,"TRV3_ALERTED")
							actions[thisI].flag++
						BREAK
						CASE 1
							IF IS_CONDITION_TRUE(COND_PLAYER_SPOTTED_BY_LOST)
								PLAYMUSIC("TRV3_ALERTED",TRUE,"TRV3_EXPLOSIONS")
								actions[thisI].flag=3
							ELSE
								IF IS_CONDITION_TRUE(COND_THREE_BOMBS_PLANTED)									
									PLAYMUSIC("TRV3_3_PLANTED",FALSE,"TRV3_EXPLOSIONS")
									actions[thisI].flag++
								ENDIF								
							ENDIF								
						BREAK
						CASE 2
							IF IS_CONDITION_TRUE(COND_PLAYER_SPOTTED_BY_LOST)
								PLAYMUSIC("TRV3_ALERTED",TRUE,"TRV3_EXPLOSIONS")
								actions[thisI].flag=3
							ELIF IS_CONDITION_TRUE(COND_ALL_BOMBS_PLANTED)
								PLAYMUSIC("TRV3_BOMBS_PLANTED",FALSE,"TRV3_EXPLOSIONS")
								actions[thisI].completed = TRUE
							ENDIF
						BREAK	
						CASE 3 //all bombs planted - enemy alerted
							IF IS_CONDITION_TRUE(COND_ALL_BOMBS_PLANTED)
								PLAYMUSIC("TRV3_BOMBS_PLANTED_ALERTED",FALSE,"TRV3_EXPLOSIONS")
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_STOP_TRUCK_FROM_BURNING
					IF IS_VEHICLE_SAFE(vehicle[veh_broken].id)						
						SET_VEHICLE_ENGINE_HEALTH(vehicle[veh_broken].id,200)
					ENDIF
				BREAK
				
				CASE ACT_FOUGHT_GUY_FELL_AND_NOW_WONT_REACT
					IF NOT IS_PED_FUCKED(ped[ped_fought].id)
						SET_PED_RELATIONSHIP_GROUP_HASH(ped[ped_fought].id, relGroupFoughtBiker)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_fought].id,TRUE)
					ENDIF
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_STOP_TRAILER_WOMAN_GETTING_CUT_OFF
					SWITCH actions[thisI].flag
						CASE 0
							IF IS_CONV_ROOT_PLAYING("TRV3_GIRL1")
								SET_AUDIO_FLAG("DisableAbortConversationForDeathAndInjury", true) 
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							//cprintln(debug_trevor3,GET_SPEAKER_INT_FOR_CURRENT_STANDARD_CONVERSATION_LINE())
							IF IS_PED_FUCKED(ped[ped_nookie].id)
								IF GET_SPEAKER_INT_FOR_CURRENT_STANDARD_CONVERSATION_LINE() != 7
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(5)
									//cprintln(debug_trevor3,"kill dont play last line")
									actions[thisI].completed = true
								ELSE
									//cprintln(debug_trevor3,"kill but let last line play")
									//KILL_FACE_TO_FACE_CONVERSATION()
									actions[thisI].flag++
								ENDIF
							ENDIF
							IF NOT IS_CONV_ROOT_PLAYING("TRV3_GIRL1")
								SET_AUDIO_FLAG("DisableAbortConversationForDeathAndInjury", false) 
								actions[thisI].flag = 0
							ENDIF
						BREAK
						case 2
							IF IS_CONV_ROOT_PLAYING("TRV3_GIRL1")
								IF GET_SPEAKER_INT_FOR_CURRENT_STANDARD_CONVERSATION_LINE() != 7
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(6)
									SET_AUDIO_FLAG("DisableAbortConversationForDeathAndInjury", false) 
									actions[thisI].completed = true
								ENDIF
							ENDIF
						break
					ENDSWITCH
				BREAK
				
				CASE ACT_UPDATE_TRAILER_GUY_BLIP
					IF NOT IS_PED_FUCKED(ped[ped_nookie].id)
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_nookie].id) < 40
							UPDATE_AI_PED_BLIP(PED[ped_nookie].id,ped[ped_nookie].aiBlip,-1,null,true)
						ENDIF
					ENDIF
					
				BREAK
				
				CASE ACT_TIMED_EXPLOSIONS
					SWITCH actions[thisI].flag
						CASE 0
							IF GET_ACTION_FLAG(1,ACT_EXPLOSION_CAMS) > 0
								actions[thisI].intA = GET_GAME_TIMER()
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > actions[thisI].intA
								ADD_EXPLOSION((rayfireTrailer[3].vLocates[0] + rayfireTrailer[3].vLocates[1])/2.0,EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[3].vLocates[0],EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[3].vLocates[1],EXP_TAG_SHIP_DESTROY,3,false,true)
								//rayfireTrailer[4].state = resetAndGo
								actions[thisI].intA = GET_GAME_TIMER() + 500
								START_AUDIO_SCENE("TREVOR_3_RAYFIRE")
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF GET_GAME_TIMER() > actions[thisI].intA
								ADD_EXPLOSION((rayfireTrailer[0].vLocates[0] + rayfireTrailer[0].vLocates[1])/2.0,EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[0].vLocates[0],EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[0].vLocates[1],EXP_TAG_SHIP_DESTROY,3,false,true)
								//rayfireTrailer[3].state = resetAndGo
								actions[thisI].intA = GET_GAME_TIMER() + 700
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 3
							IF GET_GAME_TIMER() > actions[thisI].intA
								ADD_EXPLOSION((rayfireTrailer[2].vLocates[0] + rayfireTrailer[2].vLocates[1])/2.0,EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[2].vLocates[0],EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[2].vLocates[1],EXP_TAG_SHIP_DESTROY,3,false,true)
								//rayfireTrailer[2].state = resetAndGo
								actions[thisI].intA = GET_GAME_TIMER() + 600
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 4
							IF GET_GAME_TIMER() > actions[thisI].intA
								ADD_EXPLOSION((rayfireTrailer[4].vLocates[0] + rayfireTrailer[4].vLocates[1])/2.0,EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[4].vLocates[0],EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[4].vLocates[1],EXP_TAG_SHIP_DESTROY,3,false,true)
								//rayfireTrailer[0].state = resetAndGo
								actions[thisI].intA = GET_GAME_TIMER() + 700
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 5
							IF GET_GAME_TIMER() > actions[thisI].intA
								ADD_EXPLOSION((rayfireTrailer[1].vLocates[0] + rayfireTrailer[1].vLocates[1])/2.0,EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[1].vLocates[0],EXP_TAG_SHIP_DESTROY,3,false,true)
							//	ADD_EXPLOSION(rayfireTrailer[1].vLocates[1],EXP_TAG_SHIP_DESTROY,3,false,true)
								//rayfireTrailer[1].state = resetAndGo								
								actions[thisI].flag++
							ENDIF
						BREAK
					ENDSWITCH
					
				BREAK
				
				//Split function into a second due to this one being too long.
				DEFAULT
					ACTION2(thisI)			
				BREAK

			endswitch
			
			
			IF actions[thisI].flag = CLEANUP
				actions[thisI].completed = TRUE
				actions[thisI].flag = 0
				actions[thisI].active = FALSE
			ENDIF				
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNc

PROC ACTION_ON_INSTRUCTION(int thisI, enumActions thisiAction, int insiInst, enumInstructions thisInst)
	IF actions[thisI].action != thisiAction
	OR actions[thisI].active = FALSE
		actions[thisI].action = thisiAction
		actions[thisI].active = TRUE
		actions[thisI].ongoing = FALSE
		actions[thisI].completed = FALSE		
		actions[thisI].flag = 0
		actions[thisI].needsCleanup = FALSE
		actions[thisI].intA = 0
		actions[thisI].intB = 0
		actions[thisI].floatA = 0
	ENDIF
	
	IF actions[thisI].completed = FALSE
		IF instr[insiInst].bCompleted
			thisInst=thisInst
			action(thisI,thisiAction)
		ENDIF
	ENDIF	
ENDPROC

PROC ACTION_ON_ACTION(int thisI, enumActions thisiAction, int insiAction, enumActions thisAction,andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL)

	IF actions[thisI].action != thisiAction
	OR actions[thisI].active = FALSE
		actions[thisI].action = thisiAction
		actions[thisI].active = TRUE
		actions[thisI].ongoing = FALSE
		actions[thisI].completed = FALSE		
		actions[thisI].flag = 0
		actions[thisI].needsCleanup = FALSE
		actions[thisI].intA = 0
		actions[thisI].intB = 0
		actions[thisI].floatA = 0
	ENDIF

	IF actions[thisI].completed = FALSE
		IF actions[insiAction].completed
			thisAction=thisAction
			action(thisI,thisiAction,playout_on_trigger,andOr1,cond1)
		ENDIF
	ENDIF
ENDPROC

PROC ACTION_ON_DIALOGUE(int thisI, enumActions thisAction, int diaEntry, enumDialogue thisDia,andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL)
	
	IF actions[thisI].action != thisAction
	OR actions[thisI].active = FALSE
		actions[thisI].action = thisAction
		actions[thisI].active = TRUE
		actions[thisI].ongoing = FALSE
		actions[thisI].completed = FALSE		
		actions[thisI].flag = 0
		actions[thisI].needsCleanup = FALSE
		actions[thisI].intA = 0
		actions[thisI].intB = 0
		actions[thisI].floatA = 0
	ENDIF
	
	IF actions[thisI].completed = FALSE
		IF dia[diaEntry].bCompleted
			thisDia=thisDia
			action(thisI,thisAction,PLAYOUT_ON_COND,andOr1,cond1)
		ENDIF
	ENDIF
ENDPROC

PROC RESET_CONDITIONS()

	REPEAT COUNT_OF(conditions) i
		conditions[i].condition = COND_NULL
		conditions[i].active = FALSE
		conditions[i].returns = FALSE
		conditions[i].wasTrue = FALSE
		conditions[i].flag = 0
		conditions[i].intA = 0
	ENDREPEAT
ENDPROC

PROC RESET_ACTIONS()

	REPEAT COUNT_OF(actions) i
		//tell action to play its cleanup flag
		
			IF actions[i].needsCleanup
				actions[i].active = TRUE
				actions[i].flag = CLEANUP
				ACTION(i,actions[i].action,PLAYOUT_ON_TRIGGER)
			ENDIF
			
			//reset action
			actions[i].action = ACT_NULL
			actions[i].active = FALSE
			actions[i].ongoing = FALSE
			actions[i].completed = FALSE
			actions[i].flag = 0
			actions[i].needsCleanup = FALSE
			actions[i].trackCondition = FALSE
			actions[i].intA = 0
			actions[i].intB = 0
			actions[i].floatA = 0	
		
	ENDREPEAT
ENDPROC



PROC RESET_INSTRUCTIONS()

	REPEAT COUNT_OF(instr) i
		instr[i].ins = INS_NULL
		instr[i].bCompleted = FALSE
		instr[i].flag = 0
		instr[i].intA = 0
	ENDREPEAT
ENDPROC

PROC RESET_DIALOGUE()

	REPEAT COUNT_OF(dia) i
		dia[i].dial = DIA_NULL
		dia[i].bCompleted = FALSE
		dia[i].flag = 0
		dia[i].intA = 0
		dia[i].bStarted = FALSE
	ENDREPEAT
ENDPROC

// ================================================== Reset and cleanup =====================================================
PROC LOAD_STAGE_ASSETS(enumMissionStage ASSETS_STAGE)
	SWITCH ASSETS_STAGE
		CASE STAGE_STARTUP
			REQUEST_MODEL(IG_WADE)
		BREAK
		CASE STAGE_TRAILER_PARK_CUT
		FALLTHRU
		CASE STAGE_DETROY_TRAILER_PARK
			REQUEST_MODEL(BODHI2)	
			REQUEST_MODEL(G_M_Y_LOST_01) 
			REQUEST_MODEL(G_M_Y_LOST_02)
			REQUEST_MODEL(DLOADER)
			REQUEST_MODEL(HEXER)
			REQUEST_MODEL(Prop_Coffin_01)
			request_model(Prop_CS_Beer_Bot_01)
			REQUEST_PTFX_ASSET()
			REQUEST_ANIM_DICT("misstrevor3")
			REQUEST_ANIM_DICT("misstrevor3_beatup")		
			REQUEST_ANIM_DICT("reaction@male_stand@big_variations@b")
			REQUEST_ANIM_DICT("reaction@male_stand@big_intro@left")
			REQUEST_ANIM_DICT("reaction@male_stand@big_intro@right")
			REQUEST_ANIM_DICT("reaction@male_stand@big_intro@backward")
			
			WHILE NOT HAS_MODEL_LOADED(G_M_Y_LOST_01)
			OR NOT HAS_MODEL_LOADED(G_M_Y_LOST_02)
			OR NOT HAS_MODEL_LOADED(BODHI2)
			OR NOT HAS_MODEL_LOADED(DLOADER)
			OR NOT HAS_MODEL_LOADED(HEXER)	
			OR NOT HAS_MODEL_LOADED(Prop_Coffin_01)
			OR NOT HAS_MODEL_LOADED(Prop_CS_Beer_Bot_01)
			OR NOT HAS_PTFX_ASSET_LOADED()
			OR NOT HAS_ANIM_DICT_LOADED("misstrevor3")
			OR NOT HAS_ANIM_DICT_LOADED("misstrevor3_beatup")	
			OR NOT  HAS_ANIM_DICT_LOADED("reaction@male_stand@big_variations@b")
			OR NOT  HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@left")
			OR NOT  HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@right")
			OR NOT  HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@backward")
				safewait()
			ENDWHILE
		BREAK
		CASE STAGE_DESTROYED_CUTSCENE
			REQUEST_PTFX_ASSET()
			WHILE NOT HAS_PTFX_ASSET_LOADED()
				safewait()
			ENDWHILE
		BREAK
		CASE STAGE_DRIVE_TO_LS
			REQUEST_MODEL(BODHI2)
			REQUEST_PTFX_ASSET()
			
			WHILE NOT HAS_MODEL_LOADED(BODHI2)			
			OR NOT HAS_PTFX_ASSET_LOADED()			
				safewait()
			ENDWHILE
		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_ALL_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(BODHI2)
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(DLOADER)
	SET_MODEL_AS_NO_LONGER_NEEDED(HEXER)	
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Coffin_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_CS_Beer_Bot_01)
	REMOVE_VEHICLE_RECORDING(2,"trev3")
	REMOVE_VEHICLE_RECORDING(3,"trev3")
	REMOVE_ANIM_DICT("misstrevor3")
	REMOVE_ANIM_DICT("misstrevor3_beatup")
	REMOVE_ANIM_DICT("reaction@male_stand@big_variations@b")
	REMOVE_ANIM_DICT("reaction@male_stand@big_intro@left")
	REMOVE_ANIM_DICT("reaction@male_stand@big_intro@right")
	REMOVE_ANIM_DICT("reaction@male_stand@big_intro@backward")
	
	//fleeing peds
	REMOVE_ANIM_DICT("reaction@male_stand@big_variations@idle_c")
	SET_MODEL_AS_NO_LONGER_NEEDED(A_F_M_Salton_01)
	REMOVE_ANIM_SET("move_f@flee@a")
	
	REMOVE_PTFX_ASSET()

ENDPROC

PROC DELETE_EVERYTHING(enumEndState endState)
	
	WHILE IS_CUTSCENE_ACTIVE()	
	  IF IS_CUTSCENE_PLAYING()
	      STOP_CUTSCENE(TRUE)		 
	  ELSE
	      REMOVE_CUTSCENE ()
		  //cprintln(debug_Trevor3,"Remove cutscene b")
	   ENDIF
	   SAFEWAIT(0)
	ENDWHILE
	
	REMOVE_FORCED_OBJECT(<<32.3311, 3667.6604, 40.4431>>,3.0,prop_cs4_05_tdoor)
	REMOVE_FORCED_OBJECT(<<29.10, 3661.49, 40.85>>,3.0,prop_magenta_door)
	
	NEW_LOAD_SCENE_STOP()
	
	IF STREAMVOL_IS_VALID(endFrustum)
		STREAMVOL_DELETE(endFrustum)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_DRIVE_TO_TRAILER_PARK")
		STOP_AUDIO_SCENE("TREVOR_3_DRIVE_TO_TRAILER_PARK")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_TRAILER_PARK_OVERVIEW")
		STOP_AUDIO_SCENE("TREVOR_3_TRAILER_PARK_OVERVIEW")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_PLANT_BOMBS_STEALTH")
		STOP_AUDIO_SCENE("TREVOR_3_PLANT_BOMBS_STEALTH")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_PLANT_BOMBS_SHOOTOUT")
		STOP_AUDIO_SCENE("TREVOR_3_PLANT_BOMBS_SHOOTOUT")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_DETONATE_BOMBS")
		STOP_AUDIO_SCENE("TREVOR_3_DETONATE_BOMBS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_RAYFIRE")
		STOP_AUDIO_SCENE("TREVOR_3_RAYFIRE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_DRIVE_TO_LS_DIALOGUE")
		STOP_AUDIO_SCENE("TREVOR_3_DRIVE_TO_LS_DIALOGUE")
	ENDIF

	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_DRIVE_TO_FLOYDS")
		STOP_AUDIO_SCENE("TREVOR_3_DRIVE_TO_FLOYDS")
	ENDIF

	arriveComment = AR_NOTSET 
	
	DISABLE_TAXI_HAILING(FALSE)
	
	STOP_ANY_PED_MODEL_BEING_SUPPRESSED()
	bNewMusicRequest = FALSE
	bRequiresPreload = FALSE
	sMusicToPlay = ""
	sPreloadMusic = ""
	
	bConvoPausedOutVeh = FALSE
	bConvoPausedWanted = FALSE
	
	bPressedSkip = FALSE
	
	IF NOT IS_PED_INJURED(player_ped_id())
		SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_SPRINT)
	ENDIF
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(11)
	KILL_ANY_CONVERSATION()
	
	IF HAS_CUTSCENE_LOADED()
		REMOVE_CUTSCENE()
		//cprintln(debug_Trevor3,"Remove cutscene c")
	ENDIF
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	IF endState != END_STATE_STAGE_SKIP
	AND endstate != END_STATE_MISSION_ONGOING		
		TERMINATE_REACTIONS()
	ELSE
		RESET_REACT_ARRAY(pedReactionState,MyLocalPedStruct,convBlock,9,<<80.601524,3685.037842,38.712517>>,121)	
	ENDIF
	
	IF NOT IS_PED_FUCKED(player_ped_id())
		SET_PED_USING_ACTION_MODE(player_ped_id(),FALSE)
	ENDIF
	
	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	IF IS_STREAMVOL_ACTIVE()
		STREAMVOL_DELETE(trailer_frustum)
	ENDIF
	
	END_SRL()
	
	//SET_PLAYER_STEALTH_PERCEPTION_MODIFIER(player_id(),1.0)
			
	ENTITY_INDEX delEnt = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Wade",GET_NPC_PED_MODEL(CHAR_WADE))
	IF DOES_ENTITY_EXIST(delEnt)
		IF endState = END_STATE_STAGE_SKIP
			DELETE_ENTITY(delEnt)
		else
			IF NOT IS_ENTITY_DEAD(delEnt)
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(delEnt)				
					SET_ENTITY_AS_NO_LONGER_NEEDED(delEnt)
				ENDIF
			ENDIF
		endif
	ENDIF
	
	SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )	
		
	WAIT(0)
	IF endState = END_STATE_STAGE_SKIP
		if not IS_PED_FUCKED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehicle_index aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
				SET_ENTITY_COORDS(PLAYER_PED_ID(),GET_ENTITY_COORDS(PLAYER_PED_ID()))				
				DELETE_VEHICLE(aVeh)
			ENDIF
		ENDIF	
	ENDIF
	
	//remove track points
	IF bTracksMade
		DESTROY_TRACKED_POINT(iTRack1)
		DESTROY_TRACKED_POINT(iTRack2)
		DESTROY_TRACKED_POINT(iTRack3)
	
		bTracksMade = FALSe
	ENDIF

	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<53.943714,3620.474365,28.627594>>, <<64.899071,3764.616943,50.328354>>, 106.500000)
	END_SRL()
	//delete trailer cut bikers
	REMOVE_ALL_ASSETS()

	repeat count_of(ped) i
		IF endState != END_STATE_PASSED	
		AND endState != END_STATE_FORCE_CLEANUP
	//	AND endState != END_STATE_FAILED
			IF DOES_ENTITY_EXIST(ped[i].id)
				IF NOT IS_PED_FUCKED(ped[i].id)
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(ped[i].id)
						DELETE_PED(ped[i].id)	
					ENDIF
				ELSE
					DELETE_PED(ped[i].id)
				ENDIF
			ENDIF
		ELSE	
			
			IF NOT IS_PED_FUCKED(ped[i].id)
				IF endState = END_STATE_PASSED
				AND i = ped_wade
					DELETE_PED(ped[i].id)
				ELSE
				
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(ped[i].id)
						IF IS_PED_IN_GROUP(ped[i].id)
							REMOVE_PED_FROM_GROUP(ped[i].id)	
						ENDIF
						SET_PED_KEEP_TASK(ped[i].id,TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(ped[i].id)
					ENDIF
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(ped[i].id)
					IF NOT IS_PED_FUCKED(ped[i].id)
						IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(ped[i].id)
							SET_PED_AS_NO_LONGER_NEEDED(ped[i].id)
						ENDIF
					ELSE
						IF WAS_PED_KILLED_BY_STEALTH(ped[i].id)
							INFORM_MISSION_STATS_OF_INCREMENT(TRV3_STEALTH_KILLS)
						ENDIF
						SET_PED_AS_NO_LONGER_NEEDED(ped[i].id)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF		
	endrepeat
	
	REPEAT COUNT_OF(fleeingPed) i
		IF endState != END_STATE_PASSED	
		AND endState != END_STATE_FAILED
		AND endState != END_STATE_FORCE_CLEANUP
			IF DOES_ENTITY_EXIST(fleeingPed[i].pedID)
				DELETE_PED(fleeingPed[i].pedID)
			ENDIF
			
			IF DOES_ENTITY_EXIST(fleeingPed[i].vehID)
				DELETE_VEHICLE(fleeingPed[i].vehID)
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(fleeingPed[i].pedID)
				IF NOT IS_PED_INJURED(fleeingPed[i].pedID)
					SET_PED_KEEP_TASK(fleeingPed[i].pedID,true)															
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(fleeingPed[i].pedID)
			ENDIF
			IF DOES_ENTITY_EXIST(fleeingPed[i].vehID)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(fleeingPed[i].vehID)
			ENDIF
		ENDIF
	endrepeat
	
	repeat count_of(vehicle) i
		IF endState != END_STATE_PASSED	
		AND endState != END_STATE_FAILED
		AND endState != END_STATE_FORCE_CLEANUP
			IF DOES_ENTITY_EXIST(vehicle[i].id)
				IF IS_VEHICLE_DRIVEABLE(vehicle[i].id)
				AND NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[i].id)
					SET_ENTITY_AS_MISSION_ENTITY(vehicle[i].id,TRUE,TRUE) //due to bug 965627
				ENDIF
				DELETE_VEHICLE(vehicle[i].id)	
			ENDIF
		ELSE			
			IF DOES_ENTITY_EXIST(vehicle[i].id)
			AND IS_VEHICLE_DRIVEABLE(vehicle[i].id)
				IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[i].id)
					SET_ENTITY_AS_MISSION_ENTITY(vehicle[i].id,TRUE,TRUE) //due to bug 965627
				ENDIF
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[i].id)
			ENDIF	
		ENDIF		
	endrepeat	
	
	repeat count_of(obj) i
		IF endState != END_STATE_PASSED	
		AND endState != END_STATE_FAILED
		AND endState != END_STATE_FORCE_CLEANUP
			IF DOES_ENTITY_EXIST(obj[i].id)
				DELETE_OBJECT(obj[i].id)	
			ENDIF
			
		ELSE			
			IF DOES_ENTITY_EXIST(obj[i].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(obj[i].id)
			ENDIF	
		ENDIF
	endrepeat
	
	REMOVE_SCENARIO_BLOCKING_AREAS()	
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 92.6982, 3738.7275, 36.7299 >>,<< 105.6982, 3747.7275, 40.7299 >>,TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 43.4604, 3656.9370, 38.8369 >>-<<7,7,5>>,<< 43.4604, 3656.9370, 38.8369 >>+<<7,7,5>>,TRUE)
							
	fLastDistToJunc = 0
	bNewDirections = false
	directionsLastFrame = DIRECTIONS_UNKNOWN
	bShoutingAudioPlaying = false
	
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(Locates_Data)
	CLEAR_MISSION_LOCATE_STUFF(locates_data,true)
	
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	CLEAR_HELP()
	CLEAR_PRINTS()
	
	
	DISPLAY_RADAR(TRUE)	
	DISPLAY_HUD(TRUE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
	
	IF DOES_BLIP_EXIST(mission_blip)
		REMOVE_BLIP(mission_blip)
	ENDIF
	
	IF endState = END_STATE_STAGE_SKIP
		CLEAR_AREA(<< 953.5856, 3654.5046, 44.9018 >>,1000.0,true)
	ENDIF	
	
	IF endState != END_STATE_PASSED
	AND endState != END_STATE_FAILED
	AND endstate != END_STATE_FORCE_CLEANUP
	
		SET_WEATHER_TYPE_NOW("EXTRASUNNY")
	ELSE
		g_bBlockShopRoberies = FALSE 
		//cprintln(debug_trevor3,"WEATHER b")
		SET_WEATHER_TYPE_OVERTIME_PERSIST("SMOG",30)
	ENDIF
	
	IF DOES_ENTITY_EXIST(prop_pipe)
		DELETE_OBJECT(prop_pipe)
	ENDIF
	
	IF endState != END_STATE_STAGE_SKIP
		SET_VEHICLE_CONVERSATIONS_PERSIST(FALSE, FALSE)
	ENDIF
	
	//IF endState != END_STATE_PASSED
		WHILE NOT TRIGGER_MUSIC_EVENT("TRV3_FAIL")
			safewait()
		ENDWHILE
	//endif
	
	IF endState = END_STATE_PASSED
		SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("TRAILER_PARK_RAYFIRE_ZONE_LIST", FALSE, TRUE)
	ENDIF
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST,TRUE)
	
	CLEAR_WEATHER_TYPE_PERSIST()
	SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
	
	RESET_CONDITIONS()
	RESET_ACTIONS()
	RESET_DIALOGUE()
	RESET_INSTRUCTIONS()
	
	bTrailerBlown = FALSE
	vSeenStickyBomb = <<0,0,0>>
//	fLastRange = 0.0
	RESET_TRAILER_PARK_RAYFIRES(endState)
	ACTION(0,ACT_TRAILER_RAYFIRES) //to ensure reset of ratyfires may need to wait a few frames.
	/*
	If missionProgress < STAGE_DESTROYED_CUTSCENE
		WHILE rayfireTrailer[0].state != PRIMING
		OR rayfireTrailer[1].state != PRIMING
		OR rayfireTrailer[2].state != PRIMING
		OR rayfireTrailer[3].state != PRIMING
		OR rayfireTrailer[4].state != PRIMING
			ACTION(0,ACT_TRAILER_RAYFIRES)
			safewait(0)
		ENDWHILE
	ENDIF/*
		WHILE NOT rayfireTrailer[0].state = PRIMING
		OR NOT rayfireTrailer[1].state = PRIMING
		OR NOT rayfireTrailer[2].state = PRIMING
		OR NOT rayfireTrailer[3].state = PRIMING
		OR NOT rayfireTrailer[4].state = PRIMING
			ACTION(0,ACT_TRAILER_RAYFIRES)
			safewait(0)
		ENDWHILE
	ENDIF*/
	
ENDPROC

PROC MISSION_CLEANUP(enumEndState endState)

	DELETE_EVERYTHING(endState)
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()	
	Mission_Flow_Mission_Passed()
	MISSION_CLEANUP(END_STATE_PASSED)
ENDPROC

PROC MISSION_FAILED(enumFails failReason = FAIL_NULL)
	string sFailReason
	SWITCH failReason
		CASE FAIL_NULL sFailReason = "TRV3_F01" BREAK
		case fail_lead_cops_to_trailer_park sFailReason = "TRV3_F03" BREAK
		CASE FAIL_WADE_KILLED  sFailReason = "TRV3_15" BREAK		
		CASE FAIL_LEFT_WADE_BEHIND  sFailReason = "TRV3_F05" BREAK 
		CASE FAIL_NO_MORE_STICKY_BOMBS  sFailReason = "TRV3_16" BREAK 
		CASE FAIL_NOT_ENOUGH_STICKY_BOMBS  sFailReason = "TRV3_F06" BREAK
		CASE FAIL_TRAILER_PARK_ABANDONED sFailReason = "TRV3_F04" BREAK
		CASE FAIL_LOST_ATTACKED sFailReason = "TRV3_F07" BREAK
		
		DEFAULT sFailReason = "TRV3_F01" BREAK
	ENDSWITCH

	MISSION_FLOW_MISSION_FAILED_WITH_REASON(sfailReason) 
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(12)
	WHILE NOT TRIGGER_MUSIC_EVENT("TRV3_FAIL")
		safewait()
	ENDWHILE
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
	
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		safewait()
	ENDWHILE
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here

	MISSION_CLEANUP(END_STATE_FAILED) // must only take 1 frame and terminate the thread

ENDPROC

PROC FAIL(enumFails thisFailToCheck,andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL,andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL)

	IF checkANDOR(andOr1,cond1,andor2,cond2)
		SWITCH thisFailToCheck
			CASE fail_lead_cops_to_trailer_park						
				Mission_Failed(fail_lead_cops_to_trailer_park)
			BREAK	
			CASE FAIL_WADE_KILLED
				IF DOES_ENTITY_EXIST(ped[ped_wade].id)
					IF IS_PED_FUCKED(ped[ped_wade].id)
						Mission_Failed(FAIL_WADE_KILLED)
					ENDIF
				ENDIF
			BREAK
			CASE FAIL_LEFT_WADE_BEHIND
				IF DOES_ENTITY_EXIST(ped[ped_wade].id)
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						IF NOT IS_CUTSCENE_PLAYING()
						AND NOT IS_CUTSCENE_ACTIVE()
							IF NOT IS_PLAYER_IN_ANY_SHOP()
								IF missionProgress = STAGE_GET_TO_TRAILER_PARK
									IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wade].id) > ABANDON_BUDDY_FAIL_RANGE //increasing because mission would fail at trailer park because player is alrady 100m ir mre from Wade
										Mission_Failed(FAIL_LEFT_WADE_BEHIND)
									ENDIF
								ELIF missionProgress >= STAGE_DRIVE_TO_LS
									IF IS_CONDITION_TRUE(COND_BACK_WITH_WADE)
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wade].id) > ABANDON_BUDDY_FAIL_RANGE //increasing because mission would fail at trailer park because player is alrady 100m ir mre from Wade
											Mission_Failed(FAIL_LEFT_WADE_BEHIND)
										ENDIF
									ELSe
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wade].id) > 350 //increasing because mission would fail at trailer park because player is alrady 100m ir mre from Wade
											Mission_Failed(FAIL_LEFT_WADE_BEHIND)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE FAIL_KILL_LOST
				int iR
				REPEAT COUNT_OF(ped) iR
					IF DOES_ENTITY_EXIST(ped[iR].id)
						IF IS_PED_INJURED(ped[ir].id)
							IF ped[ir].id != ped[ped_wade].id
								Mission_Failed(FAIL_LOST_ATTACKED)
							ENDIF
						ELSE
							IF IS_PED_IN_COMBAT(ped[ir].id)
								Mission_Failed(FAIL_LOST_ATTACKED)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE FAIL_NO_MORE_STICKY_BOMBS
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB)
					IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB) = 0
						Mission_Failed(FAIL_NO_MORE_STICKY_BOMBS)
					ENDIF
				ELSE
					Mission_Failed(FAIL_NO_MORE_STICKY_BOMBS)
				ENDIF					
			BREAK
			
			CASE FAIL_NOT_ENOUGH_STICKY_BOMBS
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB)
					int iBombCount
					iBombCount = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB)
					IF iBombCount < GET_ACTION_INTA(11,ACT_REMOVE_TRAILER_BLIPS)
						Mission_Failed(FAIL_NOT_ENOUGH_STICKY_BOMBS)
					ENDIF
				ELSE
					IF GET_ACTION_INTA(11,ACT_REMOVE_TRAILER_BLIPS) > 0
						Mission_Failed(FAIL_NO_MORE_STICKY_BOMBS)
					ENDIF
				ENDIF
			BREAK
			
			CASE FAIL_TRAILER_PARK_ABANDONED
				Mission_Failed(FAIL_TRAILER_PARK_ABANDONED)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

FUNC INT GET_INSTRUCTION_FLAG(int iInstr, enumInstructions eInstruction)
	IF instr[iInstr].ins = eInstruction
		RETURN instr[iInstr].flag
	ELSE
		IF instr[iInstr].ins != INS_NULL
			script_Assert("GET_INSTRUCTION_FLAG(), instruction doesn't match array entry")
		ENDIF
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL IS_INSTRUCTION_COMPLETE(int iEntry, enumInstructions instructionToCheck)
	IF instr[iEntry].ins = instructionToCheck
		IF instr[iEntry].bCompleted = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//conditions
FUNC BOOL SET_CONDITION_FLAG(enumconditions thisCondition, int flagValue)
	int j

	REPEAT COUNT_OF(conditions) j		
		IF thisCondition = conditions[j].condition			
			conditions[j].flag = flagValue							
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC



PROC checkConditions(enumconditions firstCondition, enumconditions lastCondition)
	int thisI
	int iLastArray = enum_to_int(lastCondition) - enum_to_int(firstCondition)
	IF conditions[0].condition != firstCondition
	AND conditions[iLastArray].condition != lastCondition
		int j
		int enumCounter = 0
		REPEAT COUNT_OF(conditions) j
			IF j <= iLastArray
				conditions[j].condition = int_to_enum(enumconditions,enum_to_int(firstCondition) + enumCounter)
				conditions[j].active = TRUE
				conditions[j].returns = FALSE
				conditions[j].wasTrue = FALSE
				enumCounter++
			ELSE
				conditions[j].active = FALSE
			ENDIF
		ENDREPEAT
		//reset which condition goes in to which array.
		
	ENDIF
	
	

	REPEAT COUNT_OF(conditions) thisI
		IF conditions[thisI].active
			SWITCH conditions[thisI].condition
				CASE COND_CUTSCENE_STARTED_PLAYING
					IF HAS_CUTSCENE_LOADED()
						IF IS_CUTSCENE_PLAYING()
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				CASE COND_CUTSCENE_FINISHED
				//set elsewhere
				BREAK
				
				//drive to trailer park
				CASE COND_PLAYER_AND_WADE_IN_SAFE_CAR
					conditions[thisI].returns = FALSE
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						vehicle_index vehTemp
						/*IF IS_PED_IN_ANY_VEHICLE(ped[ped_wade].id)
							vehTemp = GET_VEHICLE_PED_IS_IN(ped[ped_wade].id)
							IF IS_VEHICLE_SAFE(vehTemp)
							AND NOT IS_ENTITY_ON_FIRE(vehTemp)
								conditions[thisI].returns = TRUE
							ENDIF*/
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
							vehTemp = GET_VEHICLE_PED_IS_IN(player_ped_id())
							IF IS_VEHICLE_SAFE(vehTemp)
							AND NOT IS_ENTITY_ON_FIRE(vehTemp)
								//IF IS_VEHICLE_SEAT_FREE(vehTemp,VS_FRONT_RIGHT)
								IF IS_PED_IN_VEHICLE(ped[ped_wade].id,vehTemp,TRUE)
									conditions[thisI].returns = TRUE
								ENDIF
							ENDIF							
						ENDIF												
					ENDIF
				BREAK								
				
				CASE COND_WADE_NEARBY
					conditions[thisI].returns = FALSE
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wade].id) < 12.0
							conditions[thisI].returns = TRUE
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-1155.9855, 929.5417, 197.1024>>)<10.0
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_WANTED
					SWITCH conditions[thisI].flag
						CASE 0
							IF GET_PLAYER_WANTED_LEVEL(player_id()) != 0
								conditions[thisI].returns = TRUE
								conditions[thisI].flag++
							ENDIF				
						BREAK
						CASE 1
							IF GET_PLAYER_WANTED_LEVEL(player_id()) = 0
								conditions[thisI].returns = FALSE
								conditions[thisI].flag = 0
							ENDIF
							
							
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM											
					SWITCH conditions[thisI].flag
						CASE 0
							IF IS_CONDITION_TRUE(COND_WADE_NEARBY)
							AND IS_CONDITION_TRUE(COND_PLAYER_AND_WADE_IN_SAFE_CAR)
								IF bConvoPausedOutVeh
									bConvoPausedOutVeh=FALSE
									IF NOT bConvoPausedWanted
										PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									ENDIF
								ENDIF
								conditions[thisI].returns = FALSE
								conditions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONDITION_FALSE(COND_WADE_NEARBY)
							OR IS_CONDITION_FALSE(COND_PLAYER_AND_WADE_IN_SAFE_CAR)								
								conditions[thisI].flag=0
								conditions[thisI].returns = TRUE
								
							ENDIF
						BREAK
					ENDSWITCH					
				BREAK
				
				CASE COND_WADE_WANTS_TO_COMMENT_ON_WANTED_LEVEL //was commented out but not sure why
				
					SWITCH conditions[thisI].flag
						CASE 0
							IF IS_CONDITION_TRUE(COND_WANTED)
								conditions[thisI].returns = TRUE
								conditions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONDITION_FALSE(COND_WANTED)
								IF bConvoPausedWanted
									bConvoPausedWanted=FALSE
									IF NOT bConvoPausedOutVeh
										PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									ENDIF
								ENDIF
								conditions[thisI].flag=0				
								conditions[thisI].returns = FALSE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE
					IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
					AND NOT IS_ENTITY_ON_FIRE(vehicle[veh_player].id)										
						IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[veh_player].id)
						AND IS_PED_IN_VEHICLE(ped[ped_wade].id,vehicle[veh_player].id)
							IF GET_ENTITY_MODEL(vehicle[veh_player].id) = BODHI2
								SWITCH conditions[thisI].flag
									CASE 0							
										conditions[thisI].intA = GET_ENTITY_HEALTH(vehicle[veh_player].id)
										conditions[thisI].flag++										
									BREAK
									CASE 1									
										IF conditions[thisI].intA - GET_ENTITY_HEALTH(vehicle[veh_player].id) > 250
											conditions[thisI].returns = TRUE
											conditions[thisI].flag++
										ENDIF										
									BREAK
									CASE 2
										IF NOT conditions[thisI].returns
											conditions[thisI].intA = GET_ENTITY_HEALTH(vehicle[veh_player].id)
											conditions[thisI].flag++
										ENDIF
									BREAK
									CASE 3
										IF conditions[thisI].intA - GET_ENTITY_HEALTH(vehicle[veh_player].id) > 250
											conditions[thisI].returns = TRUE
											conditions[thisI].flag++
										ENDIF	
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_BANTER_STATUS_SAVED		
					
				BREAK
				
				CASE COND_PLAYER_IN_TRIGGER_RANGE_OF_TRAILER_PARK
					if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<61.666199,3694.246582,38.764908>>  ) < 155.375000
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_WITHIN_300M_OF_TRAILER_PARK
					conditions[thisI].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<51.80, 3672.10, 38.88>>) < 250
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_WITHIN_500m_OF_trailer_park
					conditions[thisI].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<51.80, 3672.10, 38.88>>,FALSE) < 500
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_HIT_TRIGGER_ZONE
					
					//IF NOT conditions[thisI].returns
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<51.80, 3672.10, 38.88>>) < 170
							IF DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	143.76,	3778.86,	30.0781	>>,<<	108.956,	3797.19,	30.105	>>,<<	72.0823,	3695.32,	42.9907	>>)
							OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	-51.7138,	3748.37,	30.412	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	-71.9234,	3660.4,	42.9586	>>)
							OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	-71.9234,	3660.4,	42.9586	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	-45.1569,	3593.76,	43.059	>>)
							OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	-45.1569,	3593.76,	43.059	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	-23.654,	3575.95,	52.4504	>>)
							OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	-23.654,	3575.95,	52.4504	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	35.1416,	3553.26,	48.4446	>>)
							OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	72.0823,	3695.32,	42.9907	>>,<<	35.1416,	3553.26,	48.4446	>>,<<	88.2876,	3541.29,	44.1914	>>)
							OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	88.2876,	3541.29,	44.1914	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	125.963,	3540.97,	38.0976	>>)
							OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	125.963,	3540.97,	38.0976	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	144.039,	3577.55,	36.6136	>>)
							OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	144.039,	3577.55,	36.6136	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	156.052,	3619.7,	32.0503	>>)
								//iCamZone = 0
								conditions[thisI].returns = TRUE
							ELSE						
								IF DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	156.052,	3619.7,	32.0503	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	168.361,	3681.88,	30.4938	>>)
								OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	168.361,	3681.88,	30.4938	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	143.76,	3778.86,	30.0781	>>)
									//iCamZone = 1
									conditions[thisI].returns = TRUE
								ELSE						
									IF DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	108.956,	3797.19,	30.105	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	38.4375,	3806.55,	30.4227	>>)
									OR DOES_LINE_INTERSECT_tri(get_entity_coords(player_ped_id()) + <<0,0,15>>,get_entity_coords(player_ped_id()) - <<0,0,20>>,<<	38.4375,	3806.55,	30.4227	>>,<<	72.0823,	3695.32,	42.9907	>>,<<	-51.7138,	3748.37,	30.412	>>)
										//iCamZone = 2
										conditions[thisI].returns = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						cprintln(debug_Trevor3,"iCamZone = ",iCamZone)
					//ENDIF
				BREAK
				
				CASE COND_LIGHTNING_FLASH
					IF IS_ACTION_COMPLETE(19,ACT_LIGHTNING_FLASH)
							conditions[thisI].returns = TRUE					
					ENDIF
				BREAK
				
				CASE COND_IN_TAXI_RIDE
					IF NOT IS_PED_INJURED(ped[ped_wade].id)
						SWITCH conditions[thisI].flag
							CASE 0
								IF IS_PLAYER_A_TAXI_PASSENGER()
								AND IS_PED_IN_BACK_OF_TAXI(ped[ped_wade].id)
									conditions[thisI].flag++
									conditions[thisI].returns = TRUE
									SET_DIALOGUE_FLAG(4,DIA_ARRIVE_AT_TRAILER_PARK,2)
								ENDIF
							BREAK
							CASE 1
								IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_IN()
								AND IS_CONDITION_TRUE(COND_ARRIVE_AT_TRAILER_PARK)
									IF conditions[thisI].intA = 0
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(432)
										FORCE_DIALOGUE_STATE(0,DIA_TRAILER_PARK_DRIVE_CHAT)
										FORCE_DIALOGUE_STATE(1,DIA_WADE_COMMENTS_ON_PLAYER_LEAVING)
										FORCE_DIALOGUE_STATE(2,DIA_WADE_COMMENTS_ON_COPS)
										FORCE_DIALOGUE_STATE(5,DIA_FIRST_CAR_COMMENT)
										FORCE_DIALOGUE_STATE(6,DIA_INTRO_LINE)
										conditions[thisI].intA = 1
									ENDIF
								ENDIF
								IF (IS_CONDITION_TRUE(COND_ARRIVE_AT_TRAILER_PARK) AND HAS_DIALOGUE_FINISHED(4,DIA_ARRIVE_AT_TRAILER_PARK))
								OR IS_CONDITION_FALSE(COND_ARRIVE_AT_TRAILER_PARK)
									IF NOT IS_PED_IN_BACK_OF_TAXI(ped[ped_wade].id)
										IF IS_CONDITION_FALSE(COND_ARRIVE_AT_TRAILER_PARK)
											SET_DIALOGUE_FLAG(4,DIA_ARRIVE_AT_TRAILER_PARK,0)
										ENDIF
										conditions[thisI].returns = FALSE
										conditions[thisI].flag=0
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
	
				BREAK
				
				CASE COND_TRAILER_PARK_ASSETS_LOADED
					IF NOT conditions[thisI].returns
						IF IS_ACTION_COMPLETE(3,ACT_PREP_TRAILER_PARK)
							conditions[thisI].returns = TRUE
						ENDIF
					ELSE
						IF IS_ACTION_COMPLETE(4,ACT_RELEASE_TRAILER_PARK)
							conditions[thisI].returns = FALSE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_ARRIVE_AT_TRAILER_PARK
					float rangeToTarget
					rangeToTarget = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<57.512672,3688.248291,42.261204>>)
		
			
					IF rangeToTarget < 117.250 + (GET_ENTITY_SPEED(player_ped_id()) * 10.0)
						IF NOT IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<300.599396,3574.600342,19.148991>>, <<723.437622,3619.606934,58.073341>>, 150.500000)
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
					
				BREAK
				
				
				
				
				
				CASE COND_TREVOR_ARRIVAL_DIALOGUE_FINISHED	
					SWITCH conditions[thisI].flag
						CASE 0
							IF IS_CONV_ROOT_PLAYING("TRV3_attr")
								conditions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_CONV_ROOT_PLAYING("TRV3_attr")
								conditions[thisI].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_WADE_IN_PLAYER_GROUP
					conditions[thisI].returns = FALSE
					IF NOT DOES_BLIP_EXIST(locates_data.BuddyBlipID[0])
						conditions[thisI].returns = TRUE
					ENDIF					
				BREAK
				
				//trailer park cutscene
				
				CASE COND_ARRIVED_IN_VEHICLE
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						vehicle[veh_player].id = GET_VEHICLE_PED_IS_IN(player_ped_id())
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_A_PED_IS_REACTING
					If bAPedIsReacting
						conditions[thisI].returns = TRUE
					ELSe
						conditions[thisI].returns = FALSE
					ENDIF
				BREAK
				
				CASE COND_BEAT_UP_GUY_SEES_TREVOR
					IF NOT conditions[thisI].returns
						IF NOT IS_PED_FUCKED(ped[ped_fought].id)
							IF IS_PED_RESPONDING_TO_EVENT(ped[ped_fought].id,EVENT_ACQUAINTANCE_PED_HATE)
								conditions[thisI].returns = TRUE
								SET_PED_RELATIONSHIP_GROUP_HASH(ped[ped_fought].id,relGroupFoughtBiker)
							ENDIF
						ENDIF
					ENDIf
				BREAK
				//plant bombs
				CASE COND_PLAYER_SPOTTED_BY_LOST
					IF bAlertEveryone
					OR bPlayerSeen
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_THREE_SECONDS_SINCE_LAST_BOMB
					SWITCH conditions[thisI].flag
						CASE 0
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB)
								IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STICKYBOMB) = 0
									conditions[thisI].flag = GET_GAME_TIMER() + 3000
								ENDIF
							ENDIF
						BREAK
						DEFAULT
							IF GET_GAME_TIMER() > conditions[thisI].flag
								conditions[thisI].returns = TRUE
							ENDIF						
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_PLAYER_AT_WADE
					conditions[thisI].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_wade].id)
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wade].id) < 10
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PLAYER_LEAVING_AREA
					conditions[thisI].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<61.61, 3685.70, 38.86>>) > 183
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_LEFT_AREA
					conditions[thisI].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<61.61, 3685.70, 38.86>>) > 250
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_RETURNED_TO_WADE
					SWITCH conditions[thisI].flag
						CASE 0
							IF NOT IS_CONDITION_TRUE(COND_PLAYER_AT_WADE)
								conditions[thisI].flag++
								conditions[thisI].intA = GET_GAME_TIMER() + 15000
							ENDIF
						BREAK
						CASE 1
							IF IS_CONDITION_TRUE(COND_PLAYER_AT_WADE)
								IF GET_GAME_TIMER() > conditions[thisI].intA
								OR IS_CONDITION_TRUE(COND_PLAYER_SPOTTED_BY_LOST)
									conditions[thisI].flag++
									conditions[thisI].returns = TRUE
								ENDIF
							ENDIF
						BREAK
						/*
						CASE 2
							IF NOT IS_CONDITION_TRUE(COND_PLAYER_SPOTTED_BY_LOST)
								conditions[thisI].flag = 1
								conditions[thisI].returns = FALSE
							ENDIF
						BREAK*/
						case 5
							IF NOT IS_CONDITION_TRUE(COND_PLAYER_AT_WADE)
								conditions[thisI].flag = 0
							ELSE
								IF IS_CONDITION_TRUE(COND_PLAYER_IN_A_VEHICLE)
									conditions[thisI].returns = TRUE
								ENDIF
							ENDIF
						break
					ENDSWITCH
				BREAK
				
				
				
				CASE COND_BEAT_UP_GUY_REACT_CONDITIONS
					
					IF NOT conditions[thisI].returns
						IF HAS_DIALOGUE_FINISHED(0,DIA_BEAT_UP_BKIER_SEES_TREVOR)
							conditions[thisI].returns = TRUE
						ENDIF
						
						IF bPlayerSeen
						OR bAlertEveryone
							conditions[thisI].returns = TRUE
						ENDIF
						
						IF DOES_ENTITY_EXIST(ped[ped_fighterA].id)
							IF IS_PED_FUCKED(ped[ped_fighterA].id)
								conditions[thisI].returns = TRUE
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(ped[ped_fighterB].id)
							IF IS_PED_FUCKED(ped[ped_fighterB].id)
								conditions[thisI].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_LOST_SPOTTED_STICKY_BOMB
					IF NOT conditions[thisI].returns				
						
						REPEAT COUNT_OF(ped) iTemp						
							if not IS_PED_FUCKED(ped[iTemp].id)
								IF iTemp != ped_wade	
								AND iTemp != ped_fought
									IF IS_PED_RESPONDING_TO_EVENT(ped[iTemp].id,EVENT_SHOT_FIRED_BULLET_IMPACT)
									OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(ped[iTemp].id)-<<1,1,-1.5>>,GET_ENTITY_COORDS(ped[iTemp].id)+<<1,1,-1.5>>,WEAPONTYPE_STICKYBOMB)
										if IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(ped[iTemp].id)-<<10,10,-1.5>>,GET_ENTITY_COORDS(ped[iTemp].id)+<<10,10,-1.5>>,WEAPONTYPE_STICKYBOMB)
											GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(ped[iTemp].id)-<<10,10,-1.5>>,GET_ENTITY_COORDS(PED[iTemp].id)+<<10,10,-1.5>>,WEAPONTYPE_STICKYBOMB,vSeenStickyBomb)
											conditions[thisI].intA = iTemp
											conditions[thisI].returns = TRUE
										ENDIF										
									ENDIF
								ENDIF
							ENDIF						
						ENDREPEAT
					endif	
				BREAK		
				CASE COND_BEAT_HIM_UP_DIALOGUE_STARTS
					IF NOT conditions[thisI].returns
						IF IS_CONV_ROOT_PLAYING("TRV3_IG5")
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_ENEMIES_STILL_ALIVE
					conditions[thisI].returns = TRUE
					IF conditions[thisI].flag = 0
						IF NOT get_closest_enemy(conditions[thisI].intA)
							conditions[thisI].returns = FALSE
							conditions[thisI].flag++
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_THREE_BOMBS_PLANTED
					switch conditions[thisI].flag
						CASE 0
							IF GET_ACTION_INTA(11,ACT_REMOVE_TRAILER_BLIPS) = 5
								conditions[thisI].flag++
							endif
						break
						case 1
							IF GET_ACTION_INTA(11,ACT_REMOVE_TRAILER_BLIPS) <= 2
								conditions[thisI].returns = TRUE
							ENDIF
						break
					endswitch
				BREAK
				
				CASE COND_ALL_BOMBS_PLANTED
					IF IS_ACTION_COMPLETE(11,ACT_REMOVE_TRAILER_BLIPS)
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
		
				
				CASE COND_ALL_WILL_BLOW_AT_ONCE
					conditions[thisI].returns = FALSE
					IF IS_CONDITION_TRUE(COND_ALL_BOMBS_PLANTED)
					AND IS_CONDITION_FALSE(COND_A_TRAILER_HAS_BLOWN)
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_TRIGGER_BUTTON_PRESSED
					conditions[thisI].returns = FALSE
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_DETONATE)
					and not IS_CONTROL_PRESSED(player_control,INPUT_SELECT_WEAPON)
						IF NOT is_custom_menu_on_Screen()
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				
				
				CASE COND_BOMB_JUST_PLANTED
					SWITCH conditions[thisI].flag
						CASE 0
							conditions[thisI].flag++
							//one frame gap to allow actions time to initiate for this stage.
						BREAK
						CASE 1
							IF GET_ACTION_INTA(11,ACT_REMOVE_TRAILER_BLIPS) = COUNT_OF(rayfireTrailer)
								conditions[thisI].intA = GET_ACTION_INTA(11,ACT_REMOVE_TRAILER_BLIPS)
								conditions[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF GET_ACTION_INTA(11,ACT_REMOVE_TRAILER_BLIPS) < conditions[thisI].intA
								conditions[thisI].intA = GET_ACTION_INTA(11,ACT_REMOVE_TRAILER_BLIPS)
								conditions[thisI].flag = GET_GAME_TIMER() + 2000
								conditions[thisI].returns = TRUE
							ENDIF
						BREAK
						DEFAULT
							IF GET_GAME_TIMER() > conditions[thisI].flag
								conditions[thisI].flag = 2
								conditions[thisI].returns = FALSE
							ENDIF
						BREAK
							
					ENDSWITCH
				BREAK
				
				CASE COND_FIRST_BOMB_PLANTED
					IF NOT conditions[thisI].returns
						IF IS_CONDITION_TRUE(COND_BOMB_JUST_PLANTED)
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_ALL_TRAILERS_DESTROYED
					SWITCH conditions[thisI].flag
						CASE 0
							conditions[thisI].intA = 0
							
							REPEAT COUNT_OF(rayfireTrailer) i
							//	//cprintln(DEBUG_TREVOR3,"tr: ",i," ",ENUM_TO_INT(rayfireTrailer[i].state))
								IF rayfireTrailer[i].state != playing
								AND rayfireTrailer[i].state != inactive
								AND rayfireTrailer[i].state != resetAndGo
									conditions[thisI].intA++
								ENDIF
							ENDREPEAT
							
							IF conditions[thisI].intA = 0
								conditions[thisI].returns = TRUE								
							ENDIF
						BREAK
					ENDSWITCH														
				BREAK
					
			
				
				CASE COND_A_TRAILER_HAS_BLOWN
					IF NOT conditions[thisI].returns
						REPEAT COUNT_OF(rayfireTrailer) i
							IF rayfireTrailer[i].state = playing						
								conditions[thisI].returns = TRUE
							ENDIF
						ENDREPEAT
					ENDIF
				BREAK
							
				
				CASE COND_PLAYER_IN_A_VEHICLE
					conditions[thisI].returns = FALSE
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				
				
				CASE COND_PLAYER_LEFT_TRAILER_PARK
					IF NOT conditions[thisI].returns
						if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<61.666199,3694.246582,38.764908>>  ) > 155.375000
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PLAYER_300M_FROM_TRAILER_PARK
					IF NOT conditions[thisI].returns
						if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<61.666199,3694.246582,38.764908>>  ) > 300
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
												
				CASE COND_BACK_WITH_WADE
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						IF IS_PED_IN_ANY_VEHICLE(ped[ped_wade].id)
						AND IS_PED_IN_ANY_VEHICLE(player_ped_id())
							IF GET_VEHICLE_PED_IS_USING(ped[ped_wade].id) = GET_VEHICLE_PED_IS_USING(player_ped_id())
								conditions[thisI].returns = TRUE
							ELSE
								IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_wade].id,player_ped_id()) < 15.0
									conditions[thisI].returns = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_PED_IN_ANY_VEHICLE(ped[ped_wade].id)
							IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_wade].id,player_ped_id()) < 6.0
								conditions[thisI].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
								
				CASE COND_PLAYER_300M_FROM_CUTSCENE
					conditions[thisI].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1351.1,727.5,187.5 >>) < 300.0
						conditions[thisI].returns = TRUE
					ENDIF					
				BREAK
			
				
				CASE COND_LS_VIEW_TRIGGER
					IF NOT conditions[thisI].returns
						SWITCH conditions[thisI].flag
							CASE 0
								vector vpos
								vpos = GET_ENTITY_COORDS(player_ped_id())
								IF vpos.y < 2055
									
									iTRack2 = CREATE_TRACKED_POINT()
									SET_TRACKED_POINT_INFO(iTRack2, <<-291.3503, -540.2892, 160.9588>>, 25)
									iTRack3 = CREATE_TRACKED_POINT()
									SET_TRACKED_POINT_INFO(iTRack3, <<-1271.1735, -1046.4539, 106.8815>>, 25)
									iTRack1 = CREATE_TRACKED_POINT()
									SET_TRACKED_POINT_INFO(iTRack1, <<-549.4528, -754.2225, 160.5410>>, 25)
									//cprintln(debug_trevor3,"tracks: ",iTRack1," ",iTRack2," ",iTRack3)
									conditions[thisI].flag++
									bTracksMade=TRUE
								ENDIF
							BREAK
							CASE 1
								IF IS_TRACKED_POINT_VISIBLE(iTRack1)	
								OR IS_TRACKED_POINT_VISIBLE(iTRack2)
								OR IS_TRACKED_POINT_VISIBLE(iTRack3)
							
									DESTROY_TRACKED_POINT(iTRack1)
									DESTROY_TRACKED_POINT(iTRack2)
									DESTROY_TRACKED_POINT(iTRack3)
							
									bTracksMade=FALSE
									conditions[thisI].flag++
									conditions[thisI].intA = GET_GAME_TIMER() + 2000
									
								ENDIF
							BREAK
							CASE 2
								IF GET_GAME_TIMER() > conditions[thisI].intA
									conditions[thisI].returns = TRUE
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				
				BREAK
				
				CASE COND_PLAYER_350M_FROM_CUTSCENE
					conditions[thisI].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1351.1,727.5,187.5 >>) < 350.0
						conditions[thisI].returns = TRUE
					ENDIF					
				BREAK
				
				
				
				CASE COND_WADE_AND_PLAYER_IN_VEHICLE
					conditions[thisI].returns = FALSE
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
							IF IS_PED_IN_VEHICLE(ped[ped_wade].id,vehicle[veh_player].id)
							AND IS_PED_IN_VEHICLE(player_ped_id(),vehicle[veh_player].id)
								conditions[thisI].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_CAN_STOP_PLAYER_VEHICLE
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
					AND IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<-1373.543823,728.595947,182.160049>>, <<-1337.614624,738.550415,187.760025>>, 12.125000)
						IF IS_VEHICLE_DRIVEABLE(vehicle[veh_player].id)
							IF NOT IS_ENTITY_IN_AIR(vehicle[veh_player].id)
							AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehicle[veh_player].id))
							AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehicle[veh_player].id))
								conditions[thisI].returns = TRUE
							ENDIF
						ENDIF
						
					ENDIF
				BREAK
				
		
				
				CASE COND_VEHICLE_STOPPED_OR_ON_FOOT
					conditions[thisI].returns = FALSE
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						IF IS_ACTION_COMPLETE(10,ACT_STOP_VEHICLE)
							conditions[thisI].returns = TRUE
						ENDIF
					ELSE
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_CUTSCENE_PLAYING
					conditions[thisI].returns = FALSE
					IF IS_CUTSCENE_PLAYING()
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_LS_CUTSCENE_FINISHED
					SWITCH conditions[thisI].flag
						CASE 0
							IF IS_CUTSCENE_PLAYING()
								conditions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF HAS_CUTSCENE_FINISHED()
								conditions[thisI].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_NO_INTERRUPTIONS
					conditions[thisI].returns = FALSE
					//IF NOT IS_CONDITION_TRUE(COND_WANTED)
					IF NOT IS_CONDITION_TRUE(COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE)
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				CASE COND_PLAYER_50M_FROM_WADES_COUSIN
					conditions[thisI].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1152.7819, -1523.9880, 3.5198 >>) < 50.0
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				
				CASE COND_PLAYER_600M_FROM_WADES_COUSIN
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1152.7819, -1523.9880, 3.5198 >>) < 550.0
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_AT_WADES_COUSIN
					IF IS_INSTRUCTION_COMPLETE(1,INS_GO_TO_WADES_COUSIN)
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_ESTABLISHING_SHOT_DONE
					IF IS_ACTION_COMPLETE(14,ACT_ESTABLISHING_SHOT_AT_WADES)
						conditions[thisI].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_TREVOR_APPROACHES				
					//IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<-1143.910278,-1520.446533,6.121376>>, <<-1149.479980,-1524.206421,16.492622>>, 1.937500)
					//IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-1149.088989,-1522.689453,9.632866>>) < 6.7
					conditions[thisI].returns = FALSE
					IF NOT IS_CONDITION_TRUE(COND_WANTED)
						IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<-1148.793945,-1522.302246,6.086947>>, <<-1143.033569,-1518.108154,15.913942>>, 7.625000)
							conditions[thisI].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_TREVOR_AT_TOP_OF_FLLOYD_STAIRS
					//backup check due to 1127362
					conditions[thisI].returns = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_CONDITION_TRUE(COND_WANTED)
							IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<-1150.029297,-1521.471558,11.882867>>, <<-1148.166870,-1524.169312,9.454223>>, 2.437500)
								conditions[thisI].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		endif
	ENDREPEAT
ENDPROC

//instructions
PROC SET_INSTRUCTION_NOT_COMPLETE(enumInstructions eInstruction)
	int j
	REPEAT COUNT_OF(instr) j
		IF instr[j].ins = eInstruction
			instr[j].bCompleted = FALSE
		ENDIF
	ENDREPEAT
ENDPROC

PROC FORCE_INSTRUCTION_STATE(int iInstr, enumInstructions eInstruction, bool ended = TRUE)
	instr[iInstr].ins = eInstruction
	instr[iInstr].bCompleted = ended
	instr[iInstr].flag = 0
	instr[iInstr].intA = 0
ENDPROC





PROC INSTRUCTIONS(int thisI, enumInstructions eInstruction, andorEnum andOr1=cFORCEtrue, enumconditions cond1=COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2=COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3=COND_NULL)
	//reset if instructino is new
	IF instr[thisI].ins != eInstruction
		instr[thisI].bCompleted = FALSE
		instr[thisI].ins = eInstruction
	ENDIF
	
	#if IS_DEBUG_BUILD instr[thisI].condTrue = FALSE #endif
	IF NOT instr[thisI].bCompleted
		IF checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3)
			#if IS_DEBUG_BUILD instr[thisI].condTrue = TRUE #endif
			SWITCH instr[thisI].ins
				//drive to alley
				CASE INS_GET_TO_TRAILER_PARK	
					SWITCH instr[thisI].flag
						CASE 0
							IF NOT IS_BIT_SET(instr[thisI].intA,0)
								IF IS_VEHICLE_SAFE(vehicle[veh_player].id)									
									SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(locates_data,vehicle[veh_player].id)
								ENDIF
								SET_BIT(instr[thisI].intA,0)
							ENDIF
							IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(locates_data,<<59.1994, 3603.5381, 38.8157>>,g_vAnyMeansLocate,TRUE,ped[ped_wade].id,null,null,"TRV3_1","TRV3_BDY1","","","",FALSE,TRUE,TRUE)
								SET_CONDITION_STATE(COND_PLAYER_TOO_CLOSE_TO_TRAILER_PARK,TRUE)
									//intended that nothing's in here.								
							ENDIF
							IF DOES_BLIP_EXIST(locates_data.LocationBlip)
								SET_BLIP_ALPHA(locates_data.LocationBlip,0)
								SET_BLIP_ROUTE(locates_data.GPSBlipID, FALSE)
							
							ENDIF
							IF DOES_BLIP_EXIST(locates_data.BuddyBlipID[0])
								SET_BLIP_ALPHA(locates_data.BuddyBlipID[0],0)
							ENDIF
							
							IF HAS_DIALOGUE_FINISHED(6,DIA_INTRO_LINE)
								IF DOES_BLIP_EXIST(locates_data.LocationBlip)
									SET_BLIP_ALPHA(locates_data.LocationBlip,255)
									SET_BLIP_ROUTE(locates_data.GPSBlipID, TRUE)
								
								ENDIF
								IF DOES_BLIP_EXIST(locates_data.BuddyBlipID[0])
									SET_BLIP_ALPHA(locates_data.BuddyBlipID[0],255)
								ENDIF
								instr[thisI].flag++
							ENDIF
						BREAK
						CASE 1
						
							
							
							IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(locates_data,<<59.1994, 3603.5381, 38.8157>>,g_vAnyMeansLocate,TRUE,ped[ped_wade].id,null,null,"TRV3_1","TRV3_BDY1","","","",FALSE,TRUE,TRUE)
								
									//intended that nothing's in here.								
							ENDIF
							
						BREAK
					ENDSWITCH
					
				BREAK
				
				case ins_destroy_trailers
					PRINT("TRV3_3",DEFAULT_GOD_TEXT_TIME,1)
					REPEAT COUNT_OF(rayfireTrailer) iTemp
						rayfireTrailer[iTemp].blip = CREATE_BLIP_FOR_COORD(GET_TRAILER_MIDPOINT(iTemp))
					ENDREPEAT
					instr[thisI].bCompleted = TRUE
				break
				
				case ins_detonate_bombs
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//cprintln(debug_trevor3,"kill convo ins_detonate_bombs")
						bLastConvoWithoutSubtitles = TRUE
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						
						PRINT("TRV3_12",DEFAULT_GOD_TEXT_TIME,1)
						instr[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				case ins_stealth_instructions
					SWITCH instr[thisI].flag
						CASE 0
							PRINT_HELP("TRV3_STAT1")
							instr[thisI].flag++
						BREAK
						CASE 1
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV3_STAT1")
								PRINT_HELP("TRV3_SNEAK")
								instr[thisI].flag++
							ENDIF
						BREAK
						DEFAULT										
							weapon_type wep
							wep = weapontype_unarmed					

							REPEAT COUNT_OF(ped) i	
								if i = ped_fixer
							
									IF i != ped_chattingA
									AND i != ped_chattingB
									AND i != ped_driveOffA
									AND i != ped_driveOffB
									AND i != ped_fighterA
									AND i != ped_fighterB
								
										if DOES_ENTITY_EXIST(ped[i].id)
										
											IF NOT IS_PED_FUCKED(ped[i].id)									
												
												if GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),ped[i].id) < 24.0
												
													IF IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(ped[i].id), 1.0)												
														
														IF NOT DOES_LINE_INTERSECT_QUAD(get_entity_coords(player_ped_id()),get_entity_coords(ped[i].id),<< 65.2641, 3679.68, 38.9542 >>,<< 91.683, 3725.06, 38.6181 >>,<< 93.1412, 3724.69, 42.8097 >>,<< 65.3335, 3679.76, 41.6201 >>) //player to bikers
														AND NOT DOES_LINE_INTERSECT_QUAD(get_entity_coords(player_ped_id()),get_entity_coords(ped[i].id),<< 65.2641, 3679.68, 38.9542 >>,<< 91.683, 3725.06, 38.6181 >>,<< 93.1412, 3724.69, 42.8097 >>,<< 65.3335, 3679.76, 41.6201 >>) //player to trailer guy
														AND NOT DOES_LINE_INTERSECT_QUAD(get_entity_coords(player_ped_id()),get_entity_coords(ped[i].id),<< 96.6733, 3757.29, 38.5727 >>,<< 97.1655, 3758.33, 42.81 >>,<< 90.7861, 3740.87, 42.8855 >>,<< 90.6463, 3741.27, 38.5212 >>) //player to trailer guy 2
														AND NOT DOES_LINE_INTERSECT_QUAD(get_entity_coords(player_ped_id()),get_entity_coords(ped[i].id),<< 55.5018, 3699.92, 38.7549 >>,<< 55.9722, 3700.12, 42.8396 >>,<< 57.5317, 3688.18, 42.249 >>,<< 57.6211, 3687.91, 38.7549 >>) //player to coffin guy
															
															SWITCH instr[thisI].flag
																case 2
																
																	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
																									
																		
																		IF GET_CURRENT_PED_WEAPON(player_ped_id(),wep) 
																		
																			IF wep != WEAPONTYPE_UNARMED
																				CLEAR_HELP()
																				PRINT_HELP("TRV3_6a")
																				instr[thisI].flag = 3
																				instr[thisI].intA = GET_GAME_TIMER() + 2500
																			ELSE
																				CLEAR_HELP()
																				PRINT_HELP("TRV3_6")															
																				instr[thisI].flag = 4												
																			ENDIF
																		ELSE
																			CLEAR_HELP()
																			PRINT_HELP("TRV3_6")															
																			instr[thisI].flag = 4
																		ENDIF																																		
																	ENDIF
																BREAK
																CASE 3
																	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV3_6a")
																		IF GET_CURRENT_PED_WEAPON(player_ped_id(),wep) 
																			IF wep = WEAPONTYPE_UNARMED
																				IF GET_GAME_TIMER() > instr[thisI].intA
																					CLEAR_HELP()
																				ENDIF
																			ENDIF
																		ENDIF
																	ENDIF
																	
																	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV3_6a") 
																		PRINT_HELP("TRV3_6")
																		instr[thisI].flag = 4																											
																	ENDIF
																BREAK
															ENDSWITCH
														ENDIF
													ENDIF
												ENDIF
												IF WAS_PED_KILLED_BY_STEALTH(ped[i].id)
													IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV3_6")
													OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV3_6a")
														CLEAR_HELP()										
													ENDIF
													instr[thisI].bCompleted = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIf
								ENDIF
							ENDREPEAT
						BREAK
					ENDSWITCH
					
				break
				
				case ins_plant_sticky_bombs
					REPEAT COUNT_OF(rayfireTrailer) i
						if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),rayfireTrailer[i].vLocates[0]) < 25.0
							WEAPON_TYPE yourWeapon
								yourWeapon = WEAPONTYPE_UNARMED
							SWITCH instr[thisI].flag
								CASE 0				
									IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_SELECT_WEAPON)
										GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),yourWeapon)
										if yourWeapon = WEAPONTYPE_STICKYBOMB
											instr[thisI].flag++
										ELSE
											if not IS_HELP_MESSAGE_BEING_DISPLAYED()
												weapon_type wep
												wep = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(),WEAPONSLOT_STICKYBOMB)
												IF wep != WEAPONTYPE_STICKYBOMB
													PRINT_HELP("AM_H_MULTWEP")
													instr[thisI].flag=100
												ELSE
													PRINT_HELP("TRV3_5a")
													instr[thisI].flag++
													instr[thisI].intA = GET_GAME_TIMER() + 2000
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 100
									weapon_type wep
									wep = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(),WEAPONSLOT_STICKYBOMB)
									IF wep = WEAPONTYPE_STICKYBOMB
										CLEAR_HELP()
										PRINT_HELP("TRV3_5a")
										instr[thisI].flag=1
										instr[thisI].intA = GET_GAME_TIMER() + 2000
									ENDIF
								BREAK
								CASE 1															
									GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),yourWeapon)
									if yourWeapon = WEAPONTYPE_STICKYBOMB
									AND GET_GAME_TIMER() > instr[thisI].intA
										CLEAR_HELP()
										PRINT_HELP("TRV3_5b")
										instr[thisI].flag++
									ENDIF									
								BREAK
								CASE 2
									
									IF IS_CONDITION_TRUE(COND_BOMB_JUST_PLANTED)
										CLEAR_HELP()
									ENDIF
									if not IS_HELP_MESSAGE_BEING_DISPLAYED()
										PRINT_HELP("TRV3_7")
										instr[thisI].bCompleted = TRUE
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					ENDREPEAT
				break
				
				case ins_get_back_to_lost
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()					
						PRINT_HELP("TRV3_21")
						instr[thisI].bCompleted = TRUE
					ENDIF
				break
				
				CASE ins_trigger_sticky_bombs
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()					
						PRINT_HELP("TRV3_4")
						instr[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				case INS_GET_TO_LOS_SANTOS
				
					SWITCH instr[thisI].flag
						CASE 0	
							CLEAR_PRINTS()
							IF NOT IS_CONDITION_TRUE(COND_BACK_WITH_WADE)
								mission_blip = CREATE_BLIP_FOR_ENTITY(ped[ped_wade].id)
								PRINT("TRV3_19",DEFAULT_GOD_TEXT_TIME,1)		
								IF NOT ARE_STRINGS_EQUAL(sMusicToPlay,"TRV3_GET_TO_CAR_RT")
									PLAYMUSIC("TRV3_GET_TO_CAR",FALSE,"TRV3_EXPLOSIONS")
								ENDIF
							ENDIF
							instr[thisI].flag++
						BREAK
						CASE 1
							IF IS_CONDITION_TRUE(COND_BACK_WITH_WADE)	
							//	PLAYMUSIC("TRV3_MICHAEL_CONV",FALSE,"TRV3_EXPLOSIONS")
								CLEAR_PRINTS()								
								instr[thisI].flag++								
							ENDIF
						BREAK
						
						
						case 2
						fallthru
						case 3
						fallthru
						case 4
						fallthru
						case 5
						
						
						//	float fStopRange 
						//	fStopRange = 5.0
						/*	IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
								vehicle_index aVeh
								aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
								IF IS_VEHICLE_SAFE(aVeh)
									
						//			fStopRange = GET_ENTITY_SPEED(aVeh) * 0.5
						//			IF fStopRange < 5.0 fStopRange = 5.0 endif
								endif
							ENDIF*/
							
							
							
							
							IF DOES_BLIP_EXIST(mission_blip)
								REMOVE_BLIP(mission_blip)
							ENDIF
							
							
							//IF IS_PLAYER_AT_LOCATION_ANY_MEANS(locates_data,<<-1350.1467, 725.3141, 185.3831>>,<<3,3,LOCATE_SIZE_HEIGHT>>,TRUE,"",FALSE,TRUE)
							IF IS_ACTION_COMPLETE(7,ACT_STOP_VEHICLE_PLAYER_EXIT)
								IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(locates_data,<<-1350.1467, 725.3141, 185.3831>>,<<4,4,LOCATE_SIZE_HEIGHT>>,TRUE,ped[ped_wade].id,null,null,"TRV3_9","TRV3_BDY1","","","",FALSE,TRUE,TRUE)
								//IF IS_PLAYER_AT_LOCATION_ANY_MEANS(locates_data,<<-1350.1467, 725.3141, 185.3831>>,<<4,4,LOCATE_SIZE_HEIGHT>>,TRUE,"",FALSE,TRUE)
										SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_SPRINT)
										instr[thisI].flag = 8
										instr[thisI].bCompleted = TRUE
									ENDIF
							ELSE
								IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(locates_data,<<-1350.1467, 725.3141, 185.3831>>,<<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>,TRUE,ped[ped_wade].id,null,null,"TRV3_9","TRV3_BDY1","","","",FALSE,TRUE,TRUE)
									
									
									//OR NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
										//CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(Locates_Data,TRUE)
										//CLEAR_MISSION_LOCATE_STUFF(locates_data,true)
										//mission_blip = CREATE_BLIP_FOR_COORD(<<-1350.1467, 725.3141, 185.3831>>)
										//SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),pedmove_walk)
										//instr[thisI].flag=6
										//instr[thisI].intA = GET_GAME_TIMER() + 5000
										instr[thisI].bCompleted = TRUE																		
											
										//instr[thisI].bCompleted = TRUE								
									//ENDIF
								ENDIF
							ENDIF
							
							IF instr[thisI].flag = 2
							
								IF DOES_BLIP_EXIST(locates_data.LocationBlip)
									SET_BLIP_ALPHA(locates_data.LocationBlip, 255) 
									SET_BLIP_ROUTE(locates_data.GPSBlipID, FALSE)
								
									//Build the route starting from the node calculated above.
									CLEAR_GPS_MULTI_ROUTE()
									START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
									
									
									ADD_POINT_TO_GPS_MULTI_ROUTE(<<-1161.3927, 968.5089, 199.3427>>)
									ADD_POINT_TO_GPS_MULTI_ROUTE(<<-1350.1467, 725.3141, 185.3831>>)
									
									SET_GPS_MULTI_ROUTE_RENDER(TRUE)
									instr[thisI].flag = 3		
								ENDIF
								
							
							ELIF instr[thisI].flag = 3
							
								vector vPlayer
								vPlayer = GET_ENTITY_COORDS(player_ped_id())
								IF vPlayer.x<-957 
								AND vPlayer.y > 1939
									SET_GPS_MULTI_ROUTE_RENDER(FALSE)
									SET_BLIP_ROUTE(locates_data.GPSBlipID, TRUE)	
									instr[thisI].flag = 4
								ENDIF
							
								IF NOT DOES_BLIP_EXIST(locates_data.LocationBlip)
									IF instr[thisI].intA = 0
										SET_GPS_MULTI_ROUTE_RENDER(FALSE)
										instr[thisI].intA = 1
										cprintln(debug_Trevor3,"ON")
									ENDIF
								ELSE
									If instr[thisI].intA = 1
										cprintln(debug_Trevor3,"OFF")
										SET_GPS_MULTI_ROUTE_RENDER(TRUE)
										instr[thisI].intA = 0
									ENDIF
								ENDIF
								//If the drop off point is the closest point then we don't need to do the route any more.
							/*	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-1350.1467, 725.3141, 185.3831>>) < 1800
									SET_GPS_MULTI_ROUTE_RENDER(FALSE)
									SET_BLIP_ROUTE(locates_data.GPSBlipID, TRUE)	
									instr[thisI].flag = 4
								ENDIF*/
							ELIF instr[thisI].flag = 4
								IF DOES_BLIP_HAVE_GPS_ROUTE(locates_data.GPSBlipID)
									cprintln(debug_Trevor3,"ARGH")
									CLEAR_GPS_MULTI_ROUTE()
									instr[thisI].flag = 5
								ENDIF																
							ENDIF
							
						break
					
					ENDSWITCH
				break
				
				case INS_GO_TO_WADES_COUSIN
					SWITCH instr[thisI].flag
					/*	CASE 0
							mission_blip = CREATE_BLIP_FOR_COORD(<<-1154.8898, -1521.2936, 3.3201>>)
							PRINT("TRV3_2",DEFAULT_GOD_TEXT_TIME,1)
							instr[thisI].flag++
						BREAK
						CASE 1
							IF NOT IS_CONDITION_TRUE(COND_WADE_NEARBY)
							OR IS_PED_IN_ANY_VEHICLE(player_ped_id())
								instr[thisI].flag++
							ENDIF
						BREAK*/
						CASE 0
						
							float fStopRange 
							fStopRange = 3.0
							//bool bWereHere
							IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
								vehicle_index aVeh
								aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
								IF IS_VEHICLE_SAFE(aVeh)
									
									fStopRange = GET_ENTITY_SPEED(aVeh) * 0.4
									IF fStopRange < 5.0 fStopRange = 5.0 endif
									
									
								/*	IF IS_VEHICLE_SAFE(aVeh)
										IF IS_ENTITY_IN_ANGLED_AREA(aVeh , <<-1157.787109,-1517.217041,2.684684>>, <<-1153.420166,-1522.898315,5.701228>>, 8.687500)
											if not DOES_VEHICLE_OVERLAP_ANGLED_AREA(aVeh,<<-1154.744141,-1513.383667,2.680232>>, <<-1162.256226,-1518.946411,5.658344>>, 2.500000)
											AND NOT DOES_VEHICLE_OVERLAP_ANGLED_AREA(aVeh,<<-1148.599365,-1522.633911,2.652654>>, <<-1156.545288,-1528.543945,5.635026>>, 4.625000)
												bWereHere = TRUE
											endif
										endif
									endif									*/
								endif
							ENDIF
							
				
							/*
							IF GET_DIALOGUE_FLAG(7,DIA_WADE_GIVES_DIRECTIONS) >= 3
								IF DOES_BLIP_EXIST(locates_data.LocationBlip)
									cprintln(debug_Trevor3,"HIDE BLIP")
									SET_BLIP_ROUTE(locates_data.LocationBlip, false)									
								ENDIF
							ELSE
								IF DOES_BLIP_EXIST(locates_data.LocationBlip)
							
								ENDIF
							ENDIF*/
							
							IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(locates_data,<<-1154.8898, -1521.2936, 3.3201>>,<<fStopRange,fStopRange,LOCATE_SIZE_HEIGHT>>,TRUE,ped[ped_wade].id,null,null,"TRV3_2","TRV3_BDY1","","","",FALSE,TRUE,TRUE) //bug 1065182
							//OR bWereHere
							
								IF NOT IS_BIT_SET(instr[thisI].intA,0)
									SET_BIT(instr[thisI].intA,0)
								ENDIF
							
								SET_CONDITION_FLAG(COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM,100)
								SET_CONDITION_STATE(COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM,FALSE)
								
								IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
									SET_CONDITION_STATE(COND_PLAYER_TRIG_IN_CAR,TRUE)
								ENDIF
								instr[thisI].bCompleted = TRUE								
							ENDIF
							IF DOES_BLIP_EXIST(mission_blip)
								REMOVE_BLIP(mission_blip)
							ENDIF						
						BREAK
					ENDSWITCH					
				BREAK
				
				CASE INS_FOLLOW_WADE
					IF DOES_BLIP_EXIST(mission_blip)
						REMOVE_BLIP(mission_blip)
					ENDIF
					mission_blip = CREATE_BLIP_FOR_ENTITY(ped[ped_wade].id)
					PRINT("TRV3_20",DEFAULT_GOD_TEXT_TIME,1)
					instr[thisI].bCompleted = TRUE	
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF			
ENDPROC

// ================================================ Dialogue ============================================





PROC DIALOGUE(int thisI, enumDialogue thisDia, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL, andorEnum andOr5=cIGNORE, enumconditions cond5 = COND_NULL)
	IF dia[thisI].dial != thisDia
		dia[thisI].bCompleted = FALSE
		dia[thisI].dial = thisDia
	ENDIF		
	
	#if IS_DEBUG_BUILD dia[thisI].condTrue = FALSE #endif
	
	IF NOT dia[thisI].bCompleted
		
		IF checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3,andOr4,cond4,andOr5,cond5)
			#if IS_DEBUG_BUILD dia[thisI].condTrue = TRUE #endif
			IF NOT dia[thisI].bStarted dia[thisI].bStarted=TRUE ENDIF
			SWITCH dia[thisI].dial
				
				CASE DIA_NULL
				BREAK
				
				CASE DIA_INTRO_LINE
					switch dia[thisI].flag						
						case 0
							IF CREATE_CONVERSATION_EXTRA("TRV3_INT",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
								dia[thisI].flag++							
								dia[thisI].intA = GET_GAME_TIMER() + 1000
							endif
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("TRV3_INT")
							OR (NOT IS_CONV_ROOT_PLAYING("TRV3_INT") AND GET_GAME_TIMER() > dia[thisI].intA) //fail safe for dialogue not playing
								dia[thisI].flag++	
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("TRV3_INT")
								dia[thisI].bCompleted = TRUE	
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_BIGGER_CAR
					IF IS_THIS_PRINT_BEING_DISPLAYED("MORE_SEATS")
						IF NOT IS_PED_INJURED(ped[ped_wade].id)
							PLAY_PED_AMBIENT_SPEECH(ped[ped_wade].id, "NEED_A_VEHICLE",SPEECH_PARAMS_FORCE)
							
							dia[thisI].bCompleted = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE DIA_TRAILER_PARK_DRIVE_CHAT	
				
						switch dia[thisI].flag						
							case 0					
								IF HAS_DIALOGUE_FINISHED(5,DIA_FIRST_CAR_COMMENT)
									IF CREATE_CONVERSATION_EXTRA("TRV3_DRV",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
									
										REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									
										dia[thisI].flag=3	
									ENDIF
								ENDIF
							break
							/*case 2
							//	If IS_ENTITY_AT_COORD(PLAYER_PED_ID(),v_trail_park_cut_trigger,<<450,450,450>>)
									IF CREATE_CONVERSATION_EXTRA("TRV3_DRV2",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
										
										dia[thisI].flag++															
									ENDIF
							//	ENDIF	
							break*/
							case 3
							
								IF NOT IS_CONV_ROOT_PLAYING("TRV3_DRV")									
									//cprintln(debug_trevor3,"Skip drive line ")
									IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
									
										vehicle_index aVeh
										aVEh = GET_VEHICLE_PED_IS_IN(player_ped_id())
										
										MODEL_NAMES amodel
										amodel = GET_ENTITY_MODEL(aVEh)
										
										IF IS_THIS_MODEL_A_BIKE(amodel)
										OR IS_THIS_MODEL_A_QUADBIKE(amodel)
										OR IS_THIS_MODEL_A_BOAT(amodel)
										
										OR amodel = BODHI2
										OR amodel = dune
										OR amodel = mower
										OR amodel = tractor
										OR amodel = peyote
										OR amodel = tornado4
										OR amodel = voltic
										
										OR (IS_VEHICLE_A_CONVERTIBLE(aVeh) AND GET_CONVERTIBLE_ROOF_STATE(aVeh) = CRS_LOWERED)
											dia[thisI].flag = 4
										ELSE
											dia[thisI].flag = 5
										ENDIF
									else
										dia[thisI].flag = 5
									ENDIF
								ENDIF
							BREAK
							CASE 4 //play getting wet line
								//cprintln(debug_trevor3,"CALL WET LINE A")
								IF CREATE_CONVERSATION_EXTRA("TRV3_WET",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
							
									dia[thisI].flag=6															
								ENDIF
							BREAK
							CASE 5
								//cprintln(debug_trevor3,"CALL WET LINE B")
								IF CREATE_CONVERSATION_EXTRA("TRV3_NWET",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
					
									dia[thisI].flag=6														
								ENDIF
								/*
								IF not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
									dia[thisI].bCompleted = TRUE
								endif
								*/
							break
							case 6
								IF NOT IS_CONV_ROOT_PLAYING("TRV3_WET")
								AND NOT IS_CONV_ROOT_PLAYING("TRV3_NWET")
									IF CREATE_CONVERSATION_EXTRA("TRV3_DRVb",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
										SET_DIALOGUE_FLAG(2,DIA_WADE_COMMENTS_ON_COPS,-1,-1,1)
							
										dia[thisI].flag=7
									ENDIF
								ENDIF
							break
							case 7
								IF not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
									dia[thisI].bCompleted = TRUE
								endif
							break
						ENDSWITCH
				//	endif
				BREAK
				
				CASE DIA_WADE_COMMENTS_ON_PLAYER_LEAVING
						IF NOT bConvoPausedOutVeh
						AND NOT bConvoPausedWanted
							IF dia[thisI].intA < 3
								SWITCH missionProgress
									case STAGE_GET_TO_TRAILER_PARK
											cprintln(debug_Trevor3,"PAUSE INT A")
											INTERRUPT_CONVERSATION(ped[ped_wade].id,"TRV3_DMAA","Wade")
											PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
											bConvoPausedOutVeh = TRUE																
											dia[thisI].intA++
									
	
									BREAK
									CASE STAGE_DRIVE_TO_LS
										INTERRUPT_CONVERSATION(ped[ped_wade].id,"TRV3_DNAA","Wade")
										cprintln(debug_Trevor3,"PAUSE INT B")
										PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
										bConvoPausedOutVeh = TRUE																
										dia[thisI].intA++
																			

									BREAK
									case STAGE_DRIVE_TO_WADES_COUSIN
										INTERRUPT_CONVERSATION(ped[ped_wade].id,"TRV3_DOAA","Wade")
										PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
										bConvoPausedOutVeh = TRUE																
										dia[thisI].intA++
										
									BREAK
								ENDSWITCH			
							ELIF dia[thisI].intA < 7
									cprintln(debug_Trevor3,"PAUSE INT C")
									INTERRUPT_CONVERSATION(ped[ped_wade].id,"TRV3_FUAA","Wade")							
									PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
									bConvoPausedOutVeh = TRUE //if two pause conditions exist, need to check for this
									dia[thisI].intA++
									IF dia[thisI].intA = 7
										dia[thisI].intA = 0
									ENDIF
							ENDIF		
						ENDIF
						
				BREAK
				
				CASE DIA_WADE_COMMENTS_ON_COPS
				
							IF NOT bConvoPausedOutVeh
							AND NOT bConvoPausedWanted
								IF dia[thisI].intB = 1
									cprintln(debug_Trevor3,"WANTED INT A")
									INTERRUPT_CONVERSATION(ped[ped_wade].id,"TRV3_DLAA","Wade")													
								else
									cprintln(debug_Trevor3,"WANTED INT B")
									INTERRUPT_CONVERSATION(ped[ped_wade].id,"TRV3_FVAA","Wade")										
								endif
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								bConvoPausedWanted = TRUE
							ENDIF
																															
						
				BREAK
				
				CASE DIA_WADE_COMMENTS_ON_CAR_DAMAGE
					SWITCH dia[thisI].flag
						CASE 0
							IF dia[thisI].intA < 3
								IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
									vehicle_index aVeh
									aVEh = GET_VEHICLE_PED_IS_IN(player_ped_id())
										
									MODEL_NAMES amodel
									amodel = GET_ENTITY_MODEL(aVEh)
										
									IF amodel = BODHI2
										/*If CREATE_CONVERSATION_EXTRA("TRV3_DAMAGE",3,player_ped_id(),"Trevor",2,ped[ped_wade].id,"Wade")									
											dia[thisI].intA++
											dia[thisI].flag++
										ENDIF*/
										INTERRUPT_CONVERSATION(ped[ped_wade].id,"TRV3_DPAA","Wade")	
										dia[thisI].intA++
										dia[thisI].flag++
									ELSE
										dia[thisI].bCompleted = TRUE
									ENDIF
								ENDIF																
							ELSE
								//SET_CONDITION_STATE(COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE,FALSE)		
								//SET_CONDITION_FLAG(COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE,0)
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
						CASE 1
							IF IS_AMBIENT_SPEECH_PLAYING(ped[ped_wade].id)
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_AMBIENT_SPEECH_PLAYING(ped[ped_wade].id)
								INTERRUPT_CONVERSATION(player_ped_id(),"TRV3_DKAA","Trevor")	
								SET_CONDITION_STATE(COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE,FALSE)
								SET_CONDITION_FLAG(COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE,0)
								dia[thisI].flag=0
							ENDIF
						BREAK
						//	If CREATE_CONVERSATION_EXTRA("TRV3_DAM",2,player_ped_id(),"Trevor",3,ped[ped_wade].id,"Wade")
						//		
						//		dia[thisI].flag=0							
						//	ENDIF
						//BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_ARRIVE_AT_TRAILER_PARK		
					SWITCH dia[thisI].flag
						CASE 0
							//cprintln(debug_trevor3,"kill convo DIA_ARRIVE_AT_TRAILER_PARK")
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								dia[thisI].flag++	
							ELSE
								dia[thisI].flag=2
							ENDIF
							bConvoPausedOutVeh = FALSE
							bConvoPausedWanted = FALSE
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							KILL_FACE_TO_FACE_CONVERSATION()
							SET_DIALOGUE_AS_ENDED(0,DIA_TRAILER_PARK_DRIVE_CHAT)
							SET_DIALOGUE_AS_ENDED(1,DIA_WADE_COMMENTS_ON_PLAYER_LEAVING)
							SET_DIALOGUE_AS_ENDED(2,DIA_WADE_COMMENTS_ON_COPS)
							

							dia[thisI].flag++							
						BREAK
						case 1
							If CREATE_CONVERSATION_EXTRA("TRV3_stpcnv",3,player_ped_id(),"Trevor",2,ped[ped_wade].id,"Wade")
								dia[thisI].flag++
							ENDIF
						break
						CASE 2
							float rangeToTarget
							rangeToTarget = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<57.512672,3688.248291,42.261204>>)

					
							IF rangeToTarget < 117.250 + (GET_ENTITY_SPEED(player_ped_id()) * 5.0)
								IF NOT IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<300.599396,3574.600342,19.148991>>, <<723.437622,3619.606934,58.073341>>, 150.500000)
								OR IS_PLAYER_A_TAXI_PASSENGER()
									IF GET_CLOCK_HOURS() < 21
									AND GET_CLOCK_HOURS() > 6
										arriveComment = AR_WAITNIGHT
									ELSE
										arriveComment = AR_ISNIGHT
									ENDIF
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 3
							IF NOT IS_SCREEN_FADED_OUT()
								IF arriveComment = AR_WAITNIGHT								
									If CREATE_CONVERSATION_EXTRA("TRV3_attr",2,player_ped_id(),"Trevor")
										
										dia[thisI].flag++
										//dia[thisI].bCompleted = TRUE
									ENDIF
								ELSE
									If CREATE_CONVERSATION_EXTRA("TRV3_nowait",2,player_ped_id(),"Trevor")
										//cprintln(debug_Trevor3,"FINISHED")
										dia[thisI].flag++
										//dia[thisI].bCompleted = TRUE
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 4
							IF IS_CONV_ROOT_PLAYING("TRV3_attr")
							OR IS_CONV_ROOT_PLAYING("TRV3_nowait")
								dia[thisI].flag++
							ENDIF
						BREAK
						case 5
							IF NOT IS_CONV_ROOT_PLAYING("TRV3_attr")
							AND NOT IS_CONV_ROOT_PLAYING("TRV3_nowait")
								dia[thisI].bCompleted = TRUE
							ENDIF
							
						break
					ENDSWITCH
				BREAK
				
				CASE DIA_FIRST_CAR_COMMENT
					SWITCH dia[thisI].flag
						CASE 0
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("TRV3_1")
							
								dia[thisI].flag=2
							ENDIF
						BREAK

						CASE 2
							IF IS_PED_IN_ANY_VEHICLE(ped[ped_wade].id)
								//If CREATE_CONVERSATION_EXTRA("TRV3_gocar",3,player_ped_id(),"Trevor",2,ped[ped_wade].id,"Wade")
									dia[thisI].bCompleted = TRUE									
							//ENDIF
							ELSE													
								IF IS_PED_GETTING_INTO_A_VEHICLE(player_ped_id())
									dia[thisI].flag=4									
								ELSE
									IF NOT IS_VEHICLE_SAFE(vehicle[veh_player].id)
										If CREATE_CONVERSATION_EXTRA("TRV3_gocar",3,player_ped_id(),"Trevor",2,ped[ped_wade].id,"Wade")
											dia[thisI].flag++										
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						case 3
							If CREATE_CONVERSATION_EXTRA("TRV3_GONO",3,player_ped_id(),"Trevor",2,ped[ped_wade].id,"Wade")
								dia[thisI].bCompleted = TRUE
							ENDIF
						break
						case 4
							If CREATE_CONVERSATION_EXTRA("TRV3_thisone",2,player_ped_id(),"Trevor",3,ped[ped_wade].id,"Wade")
								dia[thisI].bCompleted = TRUE
							ENDIF
						break
					ENDSWITCH
				BREAK
				
				
				
			
				
				CASE DIA_SEES_STICKY_BOMB
					SWITCH dia[thisI].flag
						CASE 0
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(13)
							dia[thisI].flag++
						BREAK
						CASE 1
							dia[thisI].intA =  GET_CONDITION_INT(COND_LOST_SPOTTED_STICKY_BOMB)
							IF dia[thisI].intA >= 0
								IF NOT IS_PED_FUCKED(ped[dia[thisI].intA].id)
									If CREATE_CONVERSATION_EXTRA("TRV3_BOMB2",4,ped[dia[thisI].intA].id,"TREV3BIKER2")
										dia[thisI].bCompleted = TRUE
									ENDIF
								ELSE
									dia[thisI].bCompleted = TRUE
								ENDIF
							ELSE
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
			
				
				CASE DIA_BEAT_UP_BKIER_SEES_TREVOR
					SWITCH dia[thisI].flag
						CASE 0
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(14)
							dia[thisI].flag++
						BREAK
						CASE 1
							If PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA("TRV3_SEES2","TRV3_SEES2_1",4,ped[ped_fought].id,"TREV3BIKER2")
								dia[thisI].flag++
								dia[thisI].intA = GET_GAME_TIMER() + 1500								
							ENDIF	
						BREAK
						CASE 2
							IF GET_GAME_TIMER() > dia[thisI].intA
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_TRAILER_BLOWN_UNSEEN
					IF int_to_enum(ALERT_MESSAGE,highestPriorityDialogue) >= ALERT_PLAYER_FOUND
						bEnableReactionDialogue = TRUE
						dia[thisI].bCompleted = TRUE
					ELSE
						SWITCH dia[thisI].flag
							CASE 0
								bEnableReactionDialogue = FALSE
								bBlockUnseenReactionDialogue = TRUE
								IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
									IF get_closest_enemy(dia[thisI].intA)
										dia[thisI].flag = 1
									ENDIF
								ELSE
									IF get_closest_enemy(dia[thisI].intA)
										dia[thisI].flag = 2
									ENDIF
								ENDIF
							BREAK
							CASE 1		
								IF NOT IS_PED_FUCKED(ped[dia[thisI].intA].id)
									If CREATE_CONVERSATION_EXTRA("TRV3_RE1",4,ped[dia[thisI].intA].id,"Lost1")
										bEnableReactionDialogue=TRUE
										//cprintln(debug_Trevor3,"PLAY A")
										dia[thisI].flag=3
									ENDIF
								ELSE
									dia[thisI].flag=0
								ENDIF
							BREAK
							CASE 2
								IF NOT IS_PED_FUCKED(ped[dia[thisI].intA].id)
									If CREATE_CONVERSATION_EXTRA("TRV3_RE2",5,ped[dia[thisI].intA].id,"Lost2")
										//cprintln(debug_Trevor3,"PLAY B")
										bEnableReactionDialogue=TRUE
										dia[thisI].flag=3
									ENDIF
								ELSE
									dia[thisI].flag=0
								ENDIF							
							BREAK
							CASE 3
								IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
									IF get_closest_enemy(dia[thisI].intA)
										dia[thisI].flag = 4
									ENDIF
								ELSE
									IF get_closest_enemy(dia[thisI].intA)
										dia[thisI].flag = 5
									ENDIF
								ENDIF
							BREAK
							CASE 4		
								IF NOT IS_PED_FUCKED(ped[dia[thisI].intA].id)
									If CREATE_CONVERSATION_EXTRA("TRV3_RE3",6,ped[dia[thisI].intA].id,"Lost3")
										//cprintln(debug_Trevor3,"PLAY C")
										dia[thisI].flag=6
										
										
									ENDIF
								ELSE
									dia[thisI].flag=3
								ENDIF
							BREAK
							CASE 5
								IF NOT IS_PED_FUCKED(ped[dia[thisI].intA].id)
									If CREATE_CONVERSATION_EXTRA("TRV3_RE4",7,ped[dia[thisI].intA].id,"Lost4")
										//cprintln(debug_Trevor3,"PLAY D")
										
										
										dia[thisI].flag++
									ENDIF
								ELSE
									dia[thisI].flag=3
								ENDIF							
							BREAK
							CASE 6
								IF IS_CONV_ROOT_PLAYING("TRV3_RE3")
								OR IS_CONV_ROOT_PLAYING("TRV3_RE4")
									
									dia[thisI].flag++
								ENDIF
							BREAK
							CASE 7
								IF NOT IS_CONV_ROOT_PLAYING("TRV3_RE4")
									dia[thisI].bCompleted = TRUE
									bBlockUnseenReactionDialogue = FALSE									
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE DIA_BACK_AT_WADE_NO_COMBAT
					IF dia[thisI].intA = 0
						IF CREATE_CONVERSATION_EXTRA("TRV3_TRK",3,ped[ped_wade].id,"WADE", 2, player_ped_id(), "TREVOR")				
							SET_CONDITION_STATE(COND_RETURNED_TO_WADE,FALSE)
							SET_CONDITION_FLAG(COND_RETURNED_TO_WADE,5)						
							dia[thisI].bCompleted = TRUE
						ENDIF
					ENDIF
				BREAK
				//IF CREATE_CONVERSATION_EXTRA("TRV3_FLEE",3,ped[ped_wade].id,"Wade",2,player_ped_id(),"Trevor")	
				CASE DIA_BACK_AT_WADE_COMBAT
					SWITCH dia[thisI].flag
						CASE 0					
							IF 	dia[thisI].intA<4			
																		
								IF CREATE_CONVERSATION_EXTRA("TRV3_WAD",3,ped[ped_wade].id,"Wade")											
									SET_CONDITION_STATE(COND_RETURNED_TO_WADE,FALSE)
									SET_CONDITION_FLAG(COND_RETURNED_TO_WADE,5)												
									dia[thisI].intA++
								ENDIF
							ELSE
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE DIA_BACK_AT_WADE_COMBAT_IN_CAR
					SWITCH dia[thisI].flag
						CASE 0					
										
							IF CREATE_CONVERSATION_EXTRA("TRV3_FLEE",3,ped[ped_wade].id,"Wade",2,player_ped_id(),"Trevor")											
								SET_CONDITION_STATE(COND_RETURNED_TO_WADE,FALSE)
									SET_CONDITION_FLAG(COND_RETURNED_TO_WADE,0)
								dia[thisI].flag++					
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("TRV3_FLEE")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("TRV3_FLEE")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_BACK_AT_WADE_IN_CAR_NO_COMBAT
					SWITCH dia[thisI].flag
						CASE 0					
							IF 	dia[thisI].intA = 0			
								cprintln(debug_Trevor3,"car comment")										
								IF CREATE_CONVERSATION_EXTRA("TRV3_G",3,ped[ped_wade].id,"Wade",2,player_ped_id(),"Trevor")											
									SET_CONDITION_STATE(COND_RETURNED_TO_WADE,FALSE)
									SET_CONDITION_FLAG(COND_RETURNED_TO_WADE,0)												
									dia[thisI].intA++
								ENDIF
							ELIF dia[thisI].intA = 1
								IF CREATE_CONVERSATION_EXTRA("TRV3_GONE",3,ped[ped_wade].id,"Wade",2,player_ped_id(),"Trevor")											
									SET_CONDITION_STATE(COND_RETURNED_TO_WADE,FALSE)
									SET_CONDITION_FLAG(COND_RETURNED_TO_WADE,0)												
									dia[thisI].intA++
								ENDIF
							ELIF dia[thisI].intA < 6
								IF CREATE_CONVERSATION_EXTRA("TRV3_FLEEB",3,ped[ped_wade].id,"Wade")											
									SET_CONDITION_STATE(COND_RETURNED_TO_WADE,FALSE)
									SET_CONDITION_FLAG(COND_RETURNED_TO_WADE,0)												
									dia[thisI].intA++
									dia[thisI].bCompleted = TRUE
								ENDIF								
							ENDIF
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE DIA_PLAYER_GETS_STEALTH_KILL
					SWITCH dia[thisI].flag
						CASE 0	
							IF IS_PED_PERFORMING_STEALTH_KILL(player_ped_id())
								cprintln(debug_Trevor3,"STEALTH KILL")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "TRV3_CHAA", "Trevor", SPEECH_PARAMS_FORCE_FRONTEND)
							//IF CREATE_CONVERSATION_EXTRA("TRV3_KILL",2,player_ped_id(),"Trevor")	
								cprintln(debug_Trevor3,"STEALTH KILL SPOKEN")
								dia[thisI].flag++
							//ENDIF
							
						BREAK
						CASE 2
							IF NOT IS_PED_PERFORMING_STEALTH_KILL(player_ped_id())
								dia[thisI].flag = 0
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_FIXER_DIALOGUE
					IF GET_ACTION_INTA(5,ACT_FIND_ANIM_TO_TRIGGER) = 0
						SWITCH dia[thisI].flag
							CASE 0								
								dia[thisI].flag++
							BREAK
							CASE 1						
								IF CREATE_CONVERSATION_EXTRA("TRV3_BIKE",3,ped[ped_fixer].id,"TRV3_BIKER1")											
									dia[thisI].flag++
									dia[thisI].intA = GET_GAME_TIMER() + 5000
								ENDIF						
							BREAk
							CASE 2					
								IF GET_GAME_TIMER() > dia[thisI].intA 
									IF CREATE_CONVERSATION_EXTRA("TRV3_BIKE",3,ped[ped_fixer].id,"TRV3_BIKER1")											
										dia[thisI].flag++
										dia[thisI].intA = GET_GAME_TIMER() + 5000
									ENDIF
								ENDIF							
							BREAK
							CASE 3									
								IF GET_GAME_TIMER() > dia[thisI].intA
									IF CREATE_CONVERSATION_EXTRA("TRV3_BIKE",3,ped[ped_fixer].id,"TRV3_BIKER1")		
										SET_CONDITION_STATE(COND_FIXER_ANIM_FINISHED,TRUE)
										dia[thisI].bCompleted = TRUE
									ENDIF
								ENDIF								
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE DIA_TRAILER
					IF GET_ACTION_INTA(5,ACT_FIND_ANIM_TO_TRIGGER) = 1
						IF NOT IS_PED_FUCKED(ped[ped_nookie].id)
							SWITCH dia[thisI].flag
								CASE 0
									IF CREATE_CONVERSATION_EXTRA("TRV3_GIRL1",8,ped[ped_nookie].id,"TRV3_BIKER6",7,null,"WomanInTrailer")										
										dia[thisI].flag=100
										dia[thisI].intA = 0
									ENDIF
								BREAK
								CASE 100
									IF IS_CONV_ROOT_PLAYING("TRV3_GIRL1")
										
										cprintln(debug_trevor3,"ADD WOMAN")
										dia[thisI].flag=1
									ENDIF
								BREAK

								case 1																
									IF  IS_BIT_SET(dia[thisI].intA,0)
									AND  IS_BIT_SET(dia[thisI].intA,1)
									AND  IS_BIT_SET(dia[thisI].intA,2)
										SET_CONDITION_STATE(COND_TRAILER_ANIM_FINISHED,TRUE)
										dia[thisI].bCompleted = TRUE
									ENDIF
									
									IF IS_SYNCHRONIZED_SCENE_RUNNING(iTrailerAnim)
										IF NOT IS_BIT_SET(dia[thisI].intA,0)
											if GET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim) > 0.226
											AND GET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim) < 0.26
												dia[thisI].flag = 2
											ENDIF
										ENDIF
										
										IF NOT IS_BIT_SET(dia[thisI].intA,1)
											if GET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim) > 0.487
											AND GET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim) < 0.52
												dia[thisI].flag = 3
											ENDIF
										ENDIF
										
										IF NOT IS_BIT_SET(dia[thisI].intA,2)
											if GET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim) > 0.677
											AND GET_SYNCHRONIZED_SCENE_PHASE(iTrailerAnim) < 0.71
												dia[thisI].flag = 4
											ENDIF
										ENDIF
										
									ENDIF
								BREAK
								
								CASE 2
									IF CREATE_CONVERSATION_EXTRA("TRV3_GIRL2",8,ped[ped_nookie].id,"TRV3_BIKER6",7,null,"WomanInTrailer")
										SET_BIT(dia[thisI].intA,0)
										dia[thisI].flag=1
									ENDIF
								BREAK
								
								CASE 3
									IF CREATE_CONVERSATION_EXTRA("TRV3_GIRL3",8,ped[ped_nookie].id,"TRV3_BIKER6",7,null,"WomanInTrailer")
										SET_BIT(dia[thisI].intA,1)
										dia[thisI].flag=1
									ENDIF
								BREAK
								
								CASE 4
									IF CREATE_CONVERSATION_EXTRA("TRV3_GIRL4",8,ped[ped_nookie].id,"TRV3_BIKER6",7,null,"WomanInTrailer")
										SET_BIT(dia[thisI].intA,2)
										dia[thisI].flag=1										
									ENDIF
								BREAK
						
								
							ENDSWITCH
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF dia[thisI].flag < 100
									dia[thisI].flag = 100
									dia[thisI].intA = GET_GAME_TIMER() + 1500
								ENDIF
								
								IF GET_GAME_TIMER() > dia[thisI].intA
									
									IF CREATE_CONVERSATION_EXTRA("TRV3_WMN",7,null,"WomanInTrailer")
										dia[thisI].bCompleted = TRUE
										SET_CONDITION_STATE(COND_TRAILER_ANIM_FINISHED,TRUE)
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE DIA_BIKER_DRIVE_OFF
					IF GET_ACTION_INTA(5,ACT_FIND_ANIM_TO_TRIGGER) = 2
						SWITCH dia[thisI].flag
							CASE 0
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA("TRV3_CHAT","TRV3_CHAT_3",3,ped[ped_driveOffA].id,"TREV3BIKER2",4,ped[ped_driveOffB].id,"TRV3BIKER3")																			
								//IF CREATE_CONVERSATION_EXTRA("TRV3_CHAT",3,ped[ped_driveOffA].id,"TREV3BIKER2",4,ped[ped_driveOffB].id,"TREV3BIKER3")
									dia[thisI].flag++
								ENDIF
								
							BREAK
							CASE 1												
								IF CREATE_CONVERSATION_EXTRA("TRV3_DV1",6,ped[ped_driveOffA].id,"TRV3BIKER2")										
									SET_CONDITION_STATE(COND_BIKER_DRIVE_ANIM_FINISHED,TRUE)
									
								ENDIF
							BREAK
							case 2
								IF NOT IS_CONV_ROOT_PLAYING("TRV3_DV1")
									SET_CONDITION_STATE(COND_BIKER_DRIVE_ANIM_FINISHED,TRUE)
									dia[thisI].bCompleted = TRUE
								ENDIF
							break
						ENDSWITCH						
					ENDIF
				BREAK
				
								
				
				CASE DIA_BIKERS_CHAT
					IF GET_ACTION_INTA(5,ACT_FIND_ANIM_TO_TRIGGER) = 3					
						IF CREATE_CONVERSATION_EXTRA("TRV3_IG4",3,ped[ped_chattingA].id,"TREV3BIKER3",4,ped[ped_chattingB].id,"TRV3_BIKER4")	
							SET_CONDITION_STATE(COND_BIKER_CHATTING_ANIM_FINISHED,TRUE)
							dia[thisI].bCompleted = TRUE								
						ENDIF							
					ENDIF
				BREAK
				
				CASE DIA_BEAT_UP_GUY
					IF GET_ACTION_INTA(5,ACT_FIND_ANIM_TO_TRIGGER) = 4
						IF NOT IS_PED_FUCKED(ped[ped_fighterA].id)
						AND NOT IS_PED_FUCKED(ped[ped_fighterB].id)
							SWITCH dia[thisI].flag
								CASE 0
									IF CREATE_CONVERSATION_EXTRA("TRV3_IG5", 1, ped[ped_fighterA].id, "TRV3_BIKER1", 2, PED[ped_fighterB].id, "TREV3BIKER2", 3, PED[ped_fought].id, "TREV3BIKER3")									
										dia[thisI].flag++									
									ENDIF
								BREAK
								CASE 1
									IF GET_GAME_TIMER() > dia[thisI].intA
										IF GET_SCRIPT_TASK_STATUS(ped[ped_fighterA].id,SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(ped[ped_fighterB].id,SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
											IF GET_RANDOM_INT_IN_RANGE(0,2) = 0												
												PLAY_PED_AMBIENT_SPEECH_NATIVE(ped[ped_fighterA].id, "CHAT_STATE", "SPEECH_PARAMS_FORCE")
												dia[thisI].flag=2
											ELSE
												PLAY_PED_AMBIENT_SPEECH_NATIVE(ped[ped_fighterB].id, "CHAT_STATE", "SPEECH_PARAMS_FORCE")
												dia[thisI].flag=3
											ENDIF
											
											
										ENDIF
									ENDIF
								BREAK
								CASE 2
									
									IF NOT IS_AMBIENT_SPEECH_PLAYING(ped[ped_fighterA].id)
										PLAY_PED_AMBIENT_SPEECH_NATIVE(ped[ped_fighterB].id, "CHAT_RESP", "SPEECH_PARAMS_FORCE")
										dia[thisI].flag = 4
									ENDIF
								BREAK
								CASE 3
									IF NOT IS_AMBIENT_SPEECH_PLAYING(ped[ped_fighterB].id)
										PLAY_PED_AMBIENT_SPEECH_NATIVE(ped[ped_fighterA].id, "CHAT_RESP", "SPEECH_PARAMS_FORCE")
										dia[thisI].flag = 4
									ENDIF
	
								BREAK
								CASE 4
									IF NOT IS_AMBIENT_SPEECH_PLAYING(ped[ped_fighterA].id)
									AND NOT IS_AMBIENT_SPEECH_PLAYING(ped[ped_fighterB].id)
										dia[thisI].intA = GET_GAME_TIMER() + 9000
										dia[thisI].flag = 1
									ENDIF
									//dia[thisI].bCompleted = TRUE
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				BREAK
				
				CASE DIA_COFFIN_GUY
					IF GET_ACTION_INTA(5,ACT_FIND_ANIM_TO_TRIGGER) = 5
						SWITCH dia[thisI].flag
							CASE 0
								IF CREATE_CONVERSATION_EXTRA("TRV3_ST5", 4, ped[ped_coffins].id, "TRV3_BIKER1")									
									dia[thisI].flag++									
								ENDIF
							BREAK
							CASE 1
								IF IS_CONV_ROOT_PLAYING("TRV3_ST5")
									restartLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								ELSE
									//cprintln(debug_trevor3,"STOP")
									dia[thisI].flag++
								ENDIF
							BREAK
							CASE 2
								IF NOT IS_STRING_NULL_OR_EMPTY(restartLine)														
									IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA("TRV3_ST5",restartLine,4,ped[ped_coffins].id,"TRV3_BIKER1")										
										restartLine = ""
										dia[thisI].flag = 1
									ENDIF
								ELSE
									dia[thisI].bCompleted = TRUE
									SET_CONDITION_STATE(COND_COFFIN_ANIM_FINISHED,TRUE)
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE DIA_TREVOR_SAYS_STAY_HERE
					SWITCH dia[thisI].flag
						CASE 0
							dia[thisI].intA = GET_GAME_TIMER() + 500
							dia[thisI].flag++
						break
						CASE 1
							IF GET_GAME_TIMER() > dia[thisI].intA
								IF IS_MESSAGE_BEING_DISPLAYED()
									IF CREATE_CONVERSATION_EXTRA("TRV3_GUN", 2,ped[ped_wade].id, "WADE", 3, player_ped_id(), "TREVOR")																
										dia[thisI].flag++
										//dia[thisI].bCompleted = TRUE
									ENDIF
									
								ENDIF
							ENDIF
						BREAK
						case 2
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wade].id) < 6
									IF CREATE_CONVERSATION_EXTRA("TRV3_WAI", 2, player_ped_id(), "TREVOR")																	
										dia[thisI].flag++																		
									ENDIF
								ELSE
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
						CASE 3					
							IF CREATE_CONVERSATION_EXTRA("TRV3_WAIW", 3,ped[ped_wade].id, "WADE")																
								dia[thisI].bCompleted = TRUE
							ENDIF							
						BREAK					
					ENDSWITCH
				BREAK
				
				CASE DIA_COMBAT_BANTER
					
					IF missionProgress = STAGE_DETROY_TRAILER_PARK	
					OR missionProgress = STAGE_DRIVE_TO_LS
						
						SWITCH dia[thisI].flag
							CASE 0
								iTrevorCount  = 9
								iBiker1Count = 6
								iBiker2Count = 5
								iBiker3Count = 5
								dia[thisI].flag++
							BREAK
							CASE 1
								IF iTrevorCount + iBiker1Count + iBiker2Count + iBiker3Count > 0
									
									int iConv
									iConv = GET_RANDOM_INT_IN_RANGE(0,iTrevorCount+iBiker1Count+iBiker2Count+iBiker3Count)
									IF iConv < iTrevorCount
										iTrevorCount--
										dia[thisI].flag = 2
									elif iConv < iTrevorCount+iBiker1Count
										iBiker1Count--
										dia[thisI].flag = 3
									elif iConv < iTrevorCount+iBiker1Count+iBiker2Count
										iBiker2Count--
										dia[thisI].flag = 4
									else
										iBiker3Count--
										dia[thisI].flag = 5
									endif
									
									If iTrevorCount < 0
										iTrevorCount = 0
									ENDIF
									
									If iBiker1Count < 0
										iBiker1Count = 0
									ENDIF
									
									If iBiker2Count < 0
										iBiker2Count = 0
									ENDIF
									
									If iBiker3Count < 0
										iBiker3Count = 0
									ENDIF
								endif
							BREAK
							CASE 2
								IF CREATE_CONVERSATION_EXTRA("TRV3_ATT2", 2, player_ped_id(), "TREVOR")									
									dia[thisI].flag = 6
								ENDIF
							BREAK
							CASE 3
								IF get_closest_enemy(dia[thisI].intA)
					
									IF CREATE_CONVERSATION_EXTRA("TRV3_ATTACK", 3, ped[dia[thisI].intA].id, "TREV3BIKER3")									
										dia[thisI].flag = 6
									ENDIF
								ENDIF
							BREAK
							CASE 4
								IF get_closest_enemy(dia[thisI].intA)
									
									IF CREATE_CONVERSATION_EXTRA("TRV3_ATTACK2", 4, ped[dia[thisI].intA].id, "TREV3BIKER2")									
										dia[thisI].flag = 6
									ENDIF
								ENDIF
							BREAK
							CASE 5
								IF get_closest_enemy(dia[thisI].intA)
						
									IF CREATE_CONVERSATION_EXTRA("TRV3_ATTACK3", 5, ped[dia[thisI].intA].id, "TRV3_BIKER1")									
										dia[thisI].flag = 6
									ENDIF
								ENDIF
							BREAK
							case 6
								dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,6000)
								dia[thisI].flag = 7
							break
							case 7
								IF GET_GAME_TIMER() > dia[thisI].intA
									dia[thisI].flag = 1
								ENDIF
							break
						ENDSWITCH
					ENDIF
				BREAK
				CASE DIA_PLANTING_BOMBS
					SWITCH dia[thisI].flag
						CASE 0 IF CREATE_CONVERSATION_EXTRA("TRV3_BM1", 2, player_ped_id(), "TREVOR") dia[thisI].flag++ SET_CONDITION_STATE(COND_BOMB_JUST_PLANTED,FALSE) ENDIF BREAK
						CASE 1 IF CREATE_CONVERSATION_EXTRA("TRV3_BM2", 2, player_ped_id(), "TREVOR") dia[thisI].flag++ SET_CONDITION_STATE(COND_BOMB_JUST_PLANTED,FALSE) ENDIF BREAK
						CASE 2 IF CREATE_CONVERSATION_EXTRA("TRV3_BM3", 2, player_ped_id(), "TREVOR") dia[thisI].flag++ SET_CONDITION_STATE(COND_BOMB_JUST_PLANTED,FALSE) ENDIF BREAK
						CASE 3 IF CREATE_CONVERSATION_EXTRA("TRV3_BM4", 2, player_ped_id(), "TREVOR") dia[thisI].flag++ SET_CONDITION_STATE(COND_BOMB_JUST_PLANTED,FALSE) ENDIF BREAK
						CASE 4 IF CREATE_CONVERSATION_EXTRA("TRV3_BM5", 2, player_ped_id(), "TREVOR") dia[thisI].flag++ SET_CONDITION_STATE(COND_BOMB_JUST_PLANTED,FALSE) ENDIF BREAK
					ENDSWITCH						
				BREAK
				CASE DIA_TREVOR_GIVEN_GUNS
					IF CREATE_CONVERSATION_EXTRA("TRV3_GUN", 3, player_ped_id(), "TREVOR", 2, ped[ped_wade].id,"Wade")									
						dia[thisI].bCompleted = TRUE
						
					ENDIF
				BREAK
			 
				CASE DIA_TRAILER_BLOWN
					IF (missionprogress = STAGE_DRIVE_TO_LS AND IS_CONDITION_TRUE(COND_PLAYER_AND_WADE_IN_SAFE_CAR))
						dia[thisI].bCompleted = TRUE
					ENDIF
								
					IF bTrailerBlown		
						IF missionProgress = STAGE_DETROY_TRAILER_PARK
						OR (missionprogress = STAGE_DRIVE_TO_LS AND bShowCutscene = TRUE)
							IF CREATE_CONVERSATION_EXTRA("TRV3_CNT", 2, player_ped_id(), "TREVOR")																						
								dia[thisI].bCompleted = TRUE
							ENDIF
						ENDIF
					ENDIF
			
				BREAK
				
				CASE DIA_LETS_GO
					SWITCH dia[thisI].flag
						CASE 0
							bEnableReactionDialogue = FALSE
							IF CREATE_CONVERSATION_EXTRA("TRV3_RTN", 2, player_ped_id(), "TREVOR", 3, ped[ped_wade].id,"Wade")																
								
								REPLAY_RECORD_BACK_FOR_TIME(9.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
								
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF CREATE_CONVERSATION_EXTRA("TRV3_DEALT", 2, player_ped_id(), "TREVOR", 3, ped[ped_wade].id,"Wade")									
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
						
					ENDSWITCH
				BREAK
				CASE DIA_DRIVE_TO_LS_BANTER
					IF IS_CONDITION_TRUE(COND_CUTSCENE_PLAYING)
						dia[thisI].bCompleted = TRUE
					ELSE
						
				
							SWITCH dia[thisI].flag
								CASE 0
									IF CREATE_CONVERSATION_EXTRA("TRV3_DR2", 2, player_ped_id(), "TREVOR", 3, ped[ped_wade].id,"Wade")																		
										dia[thisI].flag=102
									ENDIF	
								BREAK
								
								case 102
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										dia[thisI].flag=1										
									ENDIF
								break
								
								CASE 1
									IF IS_CONDITION_TRUE(COND_WADE_AND_PLAYER_IN_VEHICLE)
										//check if player is in a motorbike
										vehicle_index aVeh
										aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
										IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(aVeh))
											IF CREATE_CONVERSATION_EXTRA("TRV3_DR3",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
												
												dia[thisI].flag++
												dia[thisI].intA = GET_GAME_TIMER() + 3000
											endif
										ELSE
											IF CREATE_CONVERSATION_EXTRA("TRV3_DR4",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
											
												dia[thisI].flag++
												dia[thisI].intA = GET_GAME_TIMER() + 3000
											endif
										ENDIF
									ELSE
										dia[thisI].flag++
										dia[thisI].intA = GET_GAME_TIMER() + 3000
									ENDIF
								break
								case 2
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										dia[thisI].flag++
										dia[thisI].intA = GET_GAME_TIMER() + 2000
									ENDIF
								break
								case 3
									IF IS_CONV_ROOT_PLAYING("TRV3_DR3")
									OR IS_CONV_ROOT_PLAYING("TRV3_DR2")
										dia[thisI].flag=2
									ENDIF
									IF GET_GAME_TIMER() > dia[thisI].intA
										IF CREATE_CONVERSATION_EXTRA("TRV3_GET",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
										
											dia[thisI].flag++
										endif
									endif
								break	
								case 4
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION_EXTRA("TRV3_GET2",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
											SET_CONDITION_STATE(COND_STICK_JAMMED_LINE_PLAYED,TRUE)
											dia[thisI].flag++
										endif										
									ENDIF
								break
							ENDSWITCH
					
					ENDIF
				BREAK
				
				CASE DIA_DRIVE_TO_WADES_BANTER
									
						SWITCH dia[thisI].flag
							case 0 //
								IF IS_THIS_PRINT_BEING_DISPLAYED("TRV3_2")
									bConvoPausedOutVeh = FALSE
									bConvoPausedWanted = FALSE
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									KILL_FACE_TO_FACE_CONVERSATION()
									IF missionProgress = STAGE_GET_TO_TRAILER_PARK
										SET_DIALOGUE_AS_ENDED(0,DIA_TRAILER_PARK_DRIVE_CHAT)
									ENDIF
								/*	If missionProgress >= STAGE_DRIVE_TO_LS
										SET_DIALOGUE_AS_ENDED(3,DIA_WADE_COMMENTS_ON_PLAYER_LEAVING)
									else
										SET_DIALOGUE_AS_ENDED(1,DIA_WADE_COMMENTS_ON_PLAYER_LEAVING)
									endif*/
									//SET_DIALOGUE_AS_ENDED(2,DIA_WADE_COMMENTS_ON_COPS)
									dia[thisI].flag = 1000
								ENDIF
							BREAK
							CASE 1000
								IF CREATE_CONVERSATION_EXTRA("TRV3_AR2",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
								
									dia[thisI].flag=1001
								ENDIF								
							break
							case 1001
								IF CREATE_CONVERSATION_EXTRA("TRV3_GO",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")									
									
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
									
									dia[thisI].flag=2
								ENDIF
							break
							
							case 2
								IF IS_CONDITION_FALSE(COND_STICK_JAMMED_LINE_PLAYED)
									IF CREATE_CONVERSATION_EXTRA("TRV3_BAN3c",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
								
										dia[thisI].flag++ //don't complete this dialogue or it won't be able to restart if player leaves vehicle multiple times
									ENDIF
								ELSE
									IF CREATE_CONVERSATION_EXTRA("TRV3_BAN3",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
										
										dia[thisI].flag++ //don't complete this dialogue or it won't be able to restart if player leaves vehicle multiple times
									ENDIF
								ENDIF
							BREAK
							case 3
								IF CREATE_CONVERSATION_EXTRA("TRV3_BAN3b",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")
							
									dia[thisI].flag++ //don't complete this dialogue or it won't be able to restart if player leaves vehicle multiple times
								ENDIF
							break
						ENDSWITCH
					
				BREAK
				
			
				
				CASE DIA_WADE_AT_TOP_OF_STAIRS
					SWITCH dia[thisI].flag
						case 0 
							IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA("TRV3_DRI","TRV3_DRI_2",4,ped[ped_wade].id,"Wade",3,null,"FLOYD")
								
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("TRV3_DRI")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("TRV3_DRI")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_TREVOR_STOPS_BANTER
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						SWITCH dia[thisI].flag
							case 0 
								//cprintln(debug_trevor3,"kill convo DIA_TREVOR_STOPS_BANTER")
								bConvoPausedOutVeh = FALSE
								bConvoPausedWanted = FALSE
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)						
								KILL_FACE_TO_FACE_CONVERSATION()
								SET_DIALOGUE_AS_ENDED(1,DIA_DRIVE_TO_LS_BANTER)
								dia[thisI].flag=2
							BREAK
							CASE 1 //skip shut up wade comment
								IF CREATE_CONVERSATION_EXTRA("TRV3_stpcnv",3,player_ped_id(),"Trevor")								
									dia[thisI].flag++		
								ENDIF
							break
							case 2
								IF NOT IS_PED_FUCKED(ped[ped_wade].id)
								//	IF GET_ENTITY_HEADING(ped[ped_wade].id) > 90 and GET_ENTITY_HEADING(ped[ped_wade].id) < 270
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_wade].id,<<-1157.8527, 934.0715, 197.0163>>) > 150.0
											dia[thisI].flag=3											
										ELSE
											dia[thisI].bCompleted = TRUE
										ENDIF
								//	ENDIF
								ENDIF
							break
							Case 3
								IF CREATE_CONVERSATION_EXTRA("TRV3_here",2,player_ped_id(),"Trevor")								
									dia[thisI].flag=4					
								ENDIF
							break
							Case 4
								IF CREATE_CONVERSATION_EXTRA("TRV3_there",3,ped[ped_wade].id,"Wade")	
									FORCE_DIALOGUE_STATE(3,DIA_WADE_COMMENTS_ON_PLAYER_LEAVING,true)
									
									REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									
									dia[thisI].flag=5						
									dia[thisI].intA = GET_GAME_TIMER() + 5000
								ENDIF
							break
							CASE 5
								IF GET_GAME_TIMER() > dia[thisI].intA
									IF CREATE_CONVERSATION_EXTRA("TRV3_BAN3a",2,PLAYER_PED_ID(),"Trevor",3,ped[ped_wade].id,"Wade")							
										dia[thisI].bCompleted = TRUE
									ENDIF
								ENDIF
							BREAK
						endswitch
					endif
				BREAK
				
				CASE DIA_WADE_CLIMBS_STAIRS
					SWITCH dia[thisI].flag
						case 0 
							IF NOT  IS_CONV_ROOT_PLAYING("TRV3_END")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(0)
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1					
							IF GET_ACTION_FLAG(14,ACT_ESTABLISHING_SHOT_AT_WADES) > 0
								IF CREATE_CONVERSATION_EXTRA("TRV3_runs",3,ped[ped_wade].id,"Wade")								
									dia[thisI].bCompleted = TRUE						
								ENDIF
							ENDIF
						break
					endswitch
				BREAK
				
				CASE DIA_WADE_CALLS_DOWN_TO_TREVOR
					SWITCH dia[thisI].flag
						case 0 
							IF CREATE_CONVERSATION_EXTRA("TRV3_calls",3,ped[ped_wade].id,"Wade")								
								dia[thisI].bCompleted = TRUE					
							ENDIF
						BREAK

						CASE 2
							
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								dia[thisI].intA = GET_GAME_TIMER() + 2000
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 3
							IF GET_GAME_TIMER() > dia[thisI].intA
								IF CREATE_CONVERSATION_EXTRA("TRV3_LI",3,ped[ped_wade].id,"Wade")																
									dia[thisI].intA = GET_GAME_TIMER() + 5000						
									dia[thisI].intB++
									IF dia[thisI].intB >= 3
										dia[thisI].bCompleted = TRUE
									ENDIF
								ENDIF
							ENDIF
						BREAK					
					ENDSWITCH
				BREAK
				
				CASE DIA_OVERLOOKING_LS
					SWITCH dia[thisI].flag
						case 0 
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE_EXTRA(188)
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							dia[thisI].flag++
						BREAK
						CASE 1
							IF CREATE_CONVERSATION_EXTRA("TRV3_DRI_LI",2,player_ped_id(),"Trevor")	
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
						
					ENDSWITCH				
				BREAK
				
				case DIA_WADE_GIVES_DIRECTIONS
					IF NOT IS_PED_FUCKED(ped[ped_wade].id)
						
						
					
						SWITCH dia[thisI].flag
							case 0 
								//cprintln(debug_trevor3,"Kill convo DIA_WADE_GIVES_DIRECTIONS")
								KILL_FACE_TO_FACE_CONVERSATION()
								SET_DIALOGUE_AS_ENDED(5,DIA_DRIVE_TO_WADES_BANTER)
								dia[thisI].flag++
							break
							case 1
								IF CREATE_CONVERSATION_EXTRA("TRV3_nerF",3,ped[ped_wade].id,"Wade",2,player_ped_id(),"Trevor")								
									dia[thisI].flag++
									dia[thisI].intA = 1000
								ENDIF
							break
							CASE 2
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									ADD_PED_FOR_DIALOGUE_EXTRA(2,ped[ped_wade].id,"Wade")		
									dia[thisI].flag++
								ENDIF
							BREAK
							case 3
								IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
									//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_wade].id,<< -1152.7819, -1523.9880, 3.5198 >>) < 40
											KILL_FACE_TO_FACE_CONVERSATION()
											dia[thisI].flag++
										ELSE
											
											int iStreet
											VEHICLE_PATH_DIRECTIONS theseDirections
											float fApproxDistance
											GENERATE_DIRECTIONS_TO_COORD(<< -1152.7819, -1523.9880, 3.5198 >>, 0, theseDirections, iStreet, fApproxDistance)
											fLastDistToJunc = fLastDistToJunc
											bNewDirections = bNewDirections
											//IF theseDirections != directionsLastFrame
												SWITCH theseDirections
													CASE DIRECTIONS_UNKNOWN preloadDirections = DIRECTIONS_UNKNOWN BREAK
													CASE DIRECTIONS_WRONG_WAY IF preloadDirections != DIRECTIONS_WRONG_WAY KILL_FACE_TO_FACE_CONVERSATION() ENDIF IF PRELOAD_CONVERSATION(MyLocalPedStruct, convBlock,"TRV3_dirW",CONV_PRIORITY_MEDIUM) preloadDirections = DIRECTIONS_WRONG_WAY ENDIF BREAK
													CASE DIRECTIONS_KEEP_DRIVING preloadDirections = DIRECTIONS_UNKNOWN BREAK
													CASE DIRECTIONS_LEFT_AT_JUNCTION IF preloadDirections != DIRECTIONS_LEFT_AT_JUNCTION KILL_FACE_TO_FACE_CONVERSATION() ENDIF IF PRELOAD_CONVERSATION(MyLocalPedStruct, convBlock,"TRV3_dirL",CONV_PRIORITY_MEDIUM) preloadDirections = DIRECTIONS_LEFT_AT_JUNCTION ENDIF BREAK
													CASE DIRECTIONS_RIGHT_AT_JUNCTION IF preloadDirections != DIRECTIONS_RIGHT_AT_JUNCTION KILL_FACE_TO_FACE_CONVERSATION() ENDIF IF PRELOAD_CONVERSATION(MyLocalPedStruct, convBlock,"TRV3_dirR",CONV_PRIORITY_MEDIUM) preloadDirections = DIRECTIONS_RIGHT_AT_JUNCTION ENDIF BREAK
													CASE DIRECTIONS_KEEP_LEFT IF preloadDirections != DIRECTIONS_WRONG_WAY KILL_FACE_TO_FACE_CONVERSATION() ENDIF IF PRELOAD_CONVERSATION(MyLocalPedStruct, convBlock,"TRV3_dirW",CONV_PRIORITY_MEDIUM) preloadDirections = DIRECTIONS_WRONG_WAY ENDIF BREAK
													CASE DIRECTIONS_KEEP_RIGHT IF preloadDirections != DIRECTIONS_WRONG_WAY KILL_FACE_TO_FACE_CONVERSATION() ENDIF IF  PRELOAD_CONVERSATION(MyLocalPedStruct, convBlock,"TRV3_dirW",CONV_PRIORITY_MEDIUM) preloadDirections = DIRECTIONS_WRONG_WAY ENDIF BREAK
													CASE DIRECTIONS_UTURN IF preloadDirections != DIRECTIONS_WRONG_WAY KILL_FACE_TO_FACE_CONVERSATION() ENDIF IF PRELOAD_CONVERSATION(MyLocalPedStruct, convBlock,"TRV3_dirW",CONV_PRIORITY_MEDIUM) preloadDirections = DIRECTIONS_WRONG_WAY ENDIF BREAK
													CASE DIRECTIONS_STRAIGHT_THROUGH_JUNCTION IF preloadDirections != DIRECTIONS_STRAIGHT_THROUGH_JUNCTION KILL_FACE_TO_FACE_CONVERSATION() ENDIF IF PRELOAD_CONVERSATION(MyLocalPedStruct, convBlock,"TRV3_dirS",CONV_PRIORITY_MEDIUM) preloadDirections = DIRECTIONS_STRAIGHT_THROUGH_JUNCTION ENDIF BREAK 
												ENDSWITCH
											//ENDIF
											//cprintln(debug_trevor3,"Preload = ",preloadDirections," these dir = ",theseDirections," new dir = ",bNewDirections)
											
											////cprintln(debug_trevor3,"bNewDirections : ",bNewDirections)
											IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
												IF theseDirections != DIRECTIONS_UNKNOWN
													
													////cprintln(debug_trevor3,"fApproxDistance : ",fApproxDistance," fLastDistToJunc: ",fLastDistToJunc," speed: ",get_entity_speed(vehicle[veh_player].id)," change = ",absf(fApproxDistance - fLastDistToJunc))
													IF absf(fApproxDistance - fLastDistToJunc) > absf(get_entity_speed(vehicle[veh_player].id) + 3.0)
													OR theseDirections != directionsLastFrame
													OR theseDirections = DIRECTIONS_WRONG_WAY
													OR theseDirections = DIRECTIONS_UTURN
													OR theseDirections = DIRECTIONS_KEEP_LEFT
													OR theseDirections = DIRECTIONS_KEEP_RIGHT
													//	//cprintln(debug_trevor3,"directinos: ",theseDirections," street: ",istreet," dist: ",fapproxDistance)
														bNewDirections = TRUE
													//	//cprintln(debug_trevor3,"bNewDirections = TRUE")
													ENDIF
													
													IF bNewDirections
														IF ((fApproxDistance < 50
														OR fApproxDistance / GET_ENTITY_SPEED(vehicle[veh_player].id) < 5)
														AND fApproxDistance / GET_ENTITY_SPEED(vehicle[veh_player].id) > 1)													
														OR theseDirections = DIRECTIONS_WRONG_WAY
															SWITCH theseDirections
																CASE DIRECTIONS_UNKNOWN cprintln(debug_trevor3,"DIRECTIONS_UNKNOWN") BREAK
																CASE DIRECTIONS_WRONG_WAY cprintln(debug_trevor3,"DIRECTIONS_WRONG_WAY") BREAK
																CASE DIRECTIONS_KEEP_DRIVING cprintln(debug_trevor3,"DIRECTIONS_KEEP_DRIVING") BREAK
																CASE DIRECTIONS_LEFT_AT_JUNCTION cprintln(debug_trevor3,"DIRECTIONS_LEFT_AT_JUNCTION") BREAK
																CASE DIRECTIONS_RIGHT_AT_JUNCTION cprintln(debug_trevor3,"DIRECTIONS_RIGHT_AT_JUNCTION") BREAK
																CASE DIRECTIONS_KEEP_LEFT cprintln(debug_trevor3,"DIRECTIONS_KEEP_LEFT") BREAK
																CASE DIRECTIONS_KEEP_RIGHT cprintln(debug_trevor3,"DIRECTIONS_KEEP_RIGHT") BREAK
																CASE DIRECTIONS_UTURN cprintln(debug_trevor3,"DIRECTIONS_UTURN") BREAK
																CASE DIRECTIONS_STRAIGHT_THROUGH_JUNCTION cprintln(debug_trevor3,"DIRECTIONS_STRAIGHT_THROUGH_JUNCTION") BREAK 
															ENDSWITCH
															IF theseDirections = DIRECTIONS_WRONG_WAY
															OR theseDirections = DIRECTIONS_UTURN
															OR theseDirections = DIRECTIONS_KEEP_LEFT
															OR theseDirections = DIRECTIONS_KEEP_RIGHT
																IF GET_GAME_TIMER() > dia[thisI].intA
																	IF preloadDirections = DIRECTIONS_WRONG_WAY
																		BEGIN_PRELOADED_CONVERSATION()
																		//cprintln(debug_trevor3,"BEGIN_PRELOADED_CONVERSATION()1")
																	//IF CREATE_CONVERSATION_EXTRA("TRV3_dirW",2,ped[ped_wade].id,"Wade")								
																	//	dia[thisI].intA = get_game_timer() + 8000
																	//	bNewDirections = FALSE
																	//ENDIF	
																		preloadDirections = DIRECTIONS_UNKNOWN
																		bNewDirections = FALSE
																		dia[thisI].flag=11
																		dia[thisI].intA = get_game_timer() + 500
																	ENDIF
																ELSE
																	bNewDirections = FALSE
																ENDIF
															ELSE
																IF preloadDirections != DIRECTIONS_UNKNOWN
															//	IF GET_ENTITY_SPEED(vehicle[veh_player].id) < 1.0
																//OR fApproxDistance / GET_ENTITY_SPEED(vehicle[veh_player].id) > 3
																	IF preloadDirections = theseDirections
																		BEGIN_PRELOADED_CONVERSATION()
																		//cprintln(debug_trevor3,"BEGIN_PRELOADED_CONVERSATION()2")
																		preloadDirections = DIRECTIONS_UNKNOWN
																		bNewDirections = FALSE
																		dia[thisI].flag=10
																		dia[thisI].intA = GET_GAME_TIMER() + 500
																		/*
																			SWITCH theseDirections
																				CASE DIRECTIONS_LEFT_AT_JUNCTION																				
																					IF CREATE_CONVERSATION_EXTRA("TRV3_dirL",2,ped[ped_wade].id,"Wade")	
																						bNewDirections = FALSE
																					ENDIF
																				BREAK
																				CASE DIRECTIONS_RIGHT_AT_JUNCTION
																					IF CREATE_CONVERSATION_EXTRA("TRV3_dirR",2,ped[ped_wade].id,"Wade")	
																						bNewDirections = FALSE
																					ENDIF
																				BREAK
																				CASE DIRECTIONS_STRAIGHT_THROUGH_JUNCTION
																					IF CREATE_CONVERSATION_EXTRA("TRV3_dirS",2,ped[ped_wade].id,"Wade")		
																						bNewDirections = FALSE
																					ENDIF
																				BREAK
																				DEFAULT
																				BREAK
																			ENDSWITCH*/
																	ENDIF
																ENDIF
																									
															endif		
														endif
													endif
													fLastDistToJunc = fApproxDistance
													directionsLastFrame = theseDirections
												ENDIF
											ENDIF
											
											
										endif
									//ENDIF
								ENDIF
							break
							case 10 IF GET_GAME_TIMER() > dia[thisI].intA dia[thisI].flag=3 ENDIF BREAK
							CASE 11 IF GET_GAME_TIMER() > dia[thisI].intA dia[thisI].intA = GET_GAME_TIMER() + 7500 dia[thisI].flag=3 ENDIF BREAK 
								
								
							break
							case 4
								IF CREATE_CONVERSATION_EXTRA("TRV3_end",3,ped[ped_wade].id,"Wade")								
									
									REPLAY_RECORD_BACK_FOR_TIME(5.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
									
									dia[thisI].bCompleted = TRUE
								ENDIF
							break
						endswitch
					endif
				break
			ENDSWITCH
		ENDIF
	ENDIF
	
	
ENDPROC

PROC DIALOGUE_ON_DIALOGUE(int thisI, enumDialogue thisDia, int diaEntry, enumDialogue checkDia, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL)
	IF NOT dia[thisI].bCompleted
		IF HAS_DIALOGUE_FINISHED(diaEntry,checkDia)
			DIALOGUE(thisI,thisDia,andOr1,cond1,andOr2,cond2,andOr3,cond3,andOr4,cond4)
		ENDIF
	ENDIF
ENDPROC

PROC DIALOGUE_ON_ACTION(int thisI, enumDialogue thisDia, int actEntry, enumActions checkAction, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL)
	IF dia[thisI].dial != thisDia
		dia[thisI].bCompleted = FALSE
		dia[thisI].dial = thisDia
	ENDIF
	
	IF actions[actEntry].action = checkAction
		IF NOT dia[thisI].bCompleted
			IF actions[actEntry].completed			
				DIALOGUE(thisI,thisDia,andOr1,cond1)				
			ENDIF
		ENDIF
	ELSE
		IF actions[actEntry].action != ACT_NULL
			TEXT_LABEL_63 txt
			txt = ""
			txt += "DIALOGUE_ON_ACTION():"
			txt += actEntry		
			SCRIPT_ASSERT(txt)
		ENDIF
	ENDIF
ENDPROC



// ================================================= replay stages ===============================================

int iStoredLastReplay
PROC SET_REPLAY_STAGE()
	switch iStoredLastReplay
		case 0
			IF missionProgress >= STAGE_DETROY_TRAILER_PARK
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Destroy trailers")
				iStoredLastReplay++
			ENDIF
		BREAK
		CASE 1
			IF missionProgress > STAGE_DESTROYED_CUTSCENE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"TRailers Destroyed")
				int ij 
				g_numEnemyAlive = 0
				repeat count_of(ped) ij
					IF NOT IS_PED_FUCKED(ped[ij].id)
						IF GET_PED_RELATIONSHIP_GROUP_HASH(ped[ij].id) != relGroupFoughtBiker
							g_numEnemyAlive++
						ENDIF
					endif
				ENDREPEAT
				 
				iStoredLastReplay++
			ENDIF
		BREAK
		CASE 2
			IF missionProgress > STAGE_DRIVE_TO_LS			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Drive to Los Santos")			
				iStoredLastReplay++
			ENDIF
			
			IF iStoredLastReplay = 2 //prevent advancing two stage if already advanced above
				IF missionProgress = STAGE_DRIVE_TO_LS	
					IF NOT IS_PED_INJURED(ped[ped_Wade].id)
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wade].id) < 5
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Drive to Los Santos")			
							iStoredLastReplay++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF missionProgress >= STAGE_DRIVE_TO_WADES_COUSIN	
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Drive to Wade's cousin",TRUE)
				iStoredLastReplay++
			ENDIF
		BREAK		
	ENDSWITCH	
ENDPROC

// ==================================================== Mission Skips ================================

PROC SET_REPLAY_STATE(bool firstMissionStage, bool bPlayerInCar, bool bWadeInCar,vector vRespawnPlayer, float fPlayerHeading, vector vRespawnWade, float fWadeHeading, vector vRespawnCar, float fCarHeading, bool bZSkipped, bool bGetPlayersReplayPosition=FALSE, bool bIsReplay=FALSE)
	fCarHeading=fCarHeading
	IF DOES_ENTITY_EXIST(vehicle[VEH_PLAYER].id)
		IF NOT bIsReplay
			SET_ENTITY_COORDS(player_ped_id(),GET_ENTITY_COORDS(player_ped_id()))
		ENDIF
		DELETE_VEHICLE(vehicle[VEH_PLAYER].id)
	ENDIf
	
	IF firstMissionStage
		IF IS_REPLAY_START_VEHICLE_AVAILABLE()	
			REQUEST_REPLAY_START_VEHICLE_MODEL()
			WHILE NOT HAS_REPLAY_START_VEHICLE_LOADED()
				safewait()
			ENDWHILE
			CLEAR_AREA_OF_VEHICLES(vRespawnCar,4)
			
			vehicle[VEH_PLAYER].id = CREATE_REPLAY_START_VEHICLE(<<0,0,0>>,0)
			SET_ENTITY_HEALTH(vehicle[VEH_PLAYER].id,1800)
			
			

		ENDIF					
		
		IF NOT IS_PLAYER_VEHICLE_IN_AREA(CHAR_TREVOR,VEHICLE_TYPE_CAR,<<1981.3942, 3808.1733, 31.1383>>,15.0)
			DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_TREVOR)
			IF NOT DOES_ENTITY_EXIST(vehicle[VEH_PLAYER].id) //player already had a replay vehicle set up.
				IF NOT IS_AREA_OCCUPIED(<<1981.3942, 3808.1733, 31.1383>>-<<10.0,10.0,10.0>>,<<1981.3942, 3808.1733, 31.1383>>+<<10.0,10.0,10.0>>,false,true,false,false,false)
					WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[VEH_PLAYER].id,CHAR_TREVOR,<<1981.3942, 3808.1733, 31.1383>>,117.1702)
						safewait()
					ENDWHILE
				ELSE
					IF NOT IS_AREA_OCCUPIED(<<1967.9039, 3799.5142, 31.1684>>-<<10.0,10.0,10.0>>,<<1967.9039, 3799.5142, 31.1684>>+<<10.0,10.0,10.0>>,false,true,false,false,false)
						WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[VEH_PLAYER].id,CHAR_TREVOR,<<1967.9039, 3799.5142, 31.1684>>,117.1702)
							safewait()
						ENDWHILE
					ENDIF
				ENDIF
			ELSE
				vehicle_index aVeh
				IF IS_VEHICLE_DRIVEABLE(vehicle[VEH_PLAYER].id) 
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[VEH_PLAYER].id,<<1981.3942, 3808.1733, 31.1383>>) > 10.0
					//try to create Bodhi at correct location
					
					WHILE NOT CREATE_PLAYER_VEHICLE(aVeh,CHAR_TREVOR,<<1981.3942, 3808.1733, 31.1383>>,117.1702)
						safewait()
					ENDWHILE
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehicle[VEH_PLAYER].id) 
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[VEH_PLAYER].id,<<1967.9039, 3799.5142, 31.1684>>) > 10.0
						//try to create Bodhi at backup location or give up.
						WHILE NOT CREATE_PLAYER_VEHICLE(aVeh,CHAR_TREVOR,<<1967.9039, 3799.5142, 31.1684>>,117.1702)
							safewait()
						ENDWHILE
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(aVeh)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(aVeh) //we don't want to retain the Bodhi in memory as the player arrived in their own vehicle.				
				ENDIF
			ENDIF
		ENDIF		
	ELSE
		//cprintln(debug_Trevor3,"IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()")
		IF GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() != DUMMY_MODEL_FOR_SCRIPT
			IF GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()) > 1
			
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()						
				AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
				AND NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
				AND NOT IS_THIS_MODEL_A_BOAT(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
					REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
					WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
						safewait()
					ENDWHILE
					CLEAR_AREA_OF_VEHICLES(vRespawnCar,4)
					IF g_bShitskipAccepted
						vehicle[VEH_PLAYER].id = CREATE_REPLAY_CHECKPOINT_VEHICLE(vRespawnCar,fCarHeading)
					ELSE
						vehicle[VEH_PLAYER].id = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<0,0,0>>,0)
					ENDIF
					
					vector vCar,vthisVehicleSizeMin,vthisVehicleSizeMax
					vCar = GET_ENTITY_COORDS(vehicle[VEH_PLAYER].id)
					
					IF IS_POINT_IN_ANGLED_AREA(vCar,<<-1157.712769,-1517.213989,2.749372>>, <<-1147.219360,-1532.730347,12.125776>>, 10.687500)			
						SET_ENTITY_COORDS(vehicle[VEH_PLAYER].id,<<vCar.x,vCar.y,3.3483>>)
						//move vehicle if it's too big for garage.						
						
						GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehicle[VEH_PLAYER].id),vthisVehicleSizeMin,vthisVehicleSizeMax)
						
						vthisVehicleSizeMax = vthisVehicleSizeMax - vthisVehicleSizeMin
						
						If IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehicle[VEH_PLAYER].id))
							//choppers seem to have unreasonably large dimensions. Much larger than their blades.
							vthisVehicleSizeMax.x -= 3
							vthisVehicleSizeMax.y -= 3
						ENDIF
											
						IF vthisVehicleSizeMax.x > 4.5
						OR vthisVehicleSizeMax.y > 20
						OR vthisVehicleSizeMax.z > 2.5
							//vehicle does not fit in garage. Move it outside
							SET_ENTITY_COORDS(vehicle[VEH_PLAYER].id,<<-1161.3878, -1507.9048, 3.4094>>)
							SET_ENTITY_HEADING(vehicle[VEH_PLAYER].id,304)
						ENDIF
						
					ENDIF
					
					SET_ENTITY_HEALTH(vehicle[VEH_PLAYER].id,1800)
				ELSE
					//Create the bodhi if the vehicle isn't valid
					REQUEST_MODEL(BODHI2)
					WHILE NOT HAS_MODEL_LOADED(BODHI2)
						SAFEWAIT()
					ENDWHILE
					
					vehicle[VEH_PLAYER].id = CREATE_VEHICLE(BODHI2, vRespawnCar, fCarHeading)
					SET_ENTITY_HEALTH(vehicle[VEH_PLAYER].id,1800)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bZSkipped
		//cprintln(debug_Trevor3,"bZSkipped")
		WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[VEH_PLAYER].id,CHAR_TREVOR,vRespawnCar,fCarHeading)		
			safewait()
		ENDWHILE
			
	ENDIF
			
	
	IF (IS_VEHICLE_SAFE(vehicle[veh_player].id)
	AND GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehicle[veh_player].id) > 0)
	AND bWadeInCar
		WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(ped[ped_wade].id, CHAR_WADE, vehicle[veh_player].id, VS_FRONT_RIGHT)
			SAFEWAIT()
		ENDWHILE
		
	ELSE
		WHILE NOT CREATE_NPC_PED_ON_FOOT(ped[ped_wade].id, CHAR_WADE,vRespawnWade,fWadeHeading)
			SAFEWAIT()
		ENDWHILE
	ENDIF
	
	
		IF (bPlayerInCar OR bZSkipped)
		AND IS_VEHICLE_SAFE(vehicle[veh_player].id)	
			//cprintln(debug_trevor3,"put player in driveable veh")
			SET_PED_INTO_VEHICLE(player_ped_id(),vehicle[veh_player].id,VS_DRIVER)			
		ELSE
			IF NOT bIsReplay
				IF bGetPlayersReplayPosition
					//cprintln(debug_trevor3,"set player to replay pos")
					SET_ENTITY_COORDS(player_ped_id(),GET_REPLAY_CHECKPOINT_PLAYER_POSITION())
					SET_ENTITY_HEADING(player_ped_id(),GET_REPLAY_CHECKPOINT_PLAYER_HEADING())
				ELSE
					//cprintln(debug_trevor3,"set player to given respawn pos")
					SET_ENTITY_COORDS(player_ped_id(),vRespawnPlayer)
					SET_ENTITY_HEADING(player_ped_id(),fPlayerHeading)
				ENDIF
			eNDIF
		ENDIF

	
	IF NOT bIsReplay
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-4)
		LOAD_SCENE(vRespawnPlayer)
	ENDIF
	SAFEWAIT(2)

ENDPROC

PROC do_skip(enumMissionStage skipToStage, bool zSkipped=false, bool bIsReplay=FALSE)
	DELETE_EVERYTHING(END_STATE_STAGE_SKIP)
	missionProgress = skipToStage
	//cprintln(debug_trevor3,"skip to ",enum_to_int(skipToStage))

	REMOVE_PED_HELMET(PLAYER_PED_ID(),TRUE)
	SET_PED_HELMET_FLAG(player_ped_id(),PV_FLAG_NONE)
	bRemovedPlayerHelmet = TRUE
	
	DISABLE_TAXI_HAILING(TRUE)
	
	SWITCH skipToStage
		CASE STAGE_INTRO_CUT
			SET_CLOCK_TIME(20,00,00)
			REQUEST_MODEL(ig_wade)
			LOAD_STAGE_ASSETS(STAGE_STARTUP)
				
			
		BREAK
		CASE STAGE_GET_TO_TRAILER_PARK
			IF bIsReplay
				START_REPLAY_SETUP(<<1981.5444, 3816.7107, 31.3087>>,308.0639)
			ELSE
				SET_CLOCK_TIME(20,00,00)
			ENDIF
			SET_REPLAY_STATE(true,false,false,<<1981.5444, 3816.7107, 31.3087>>,308.0639,<<1980.6555, 3818.6411, 31.4477>>,197.5902,<<1993.6110, 3813.4626, 31.1612>>, 115.7467,FALSE,false,bIsReplay)	
			SET_WADE_DEFAULTS()
			FORCE_ACTION_STATE(0,ACT_WADE_GETS_IN_BODHI,TRUE)
			FORCE_ACTION_STATE(5,ACT_MAKE_PLAYER_AND_BUDDY_WALK,TRUE)
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		BREAK
		CASE STAGE_TRAILER_PARK_CUT
			IF bIsReplay
				START_REPLAY_SETUP(<<59.9711, 3605.2766, 38.8392>>, 351.7204)
			ELSE
				SET_CLOCK_TIME(22,00,00)
			ENDIF
			LOAD_STAGE_ASSETS(STAGE_DETROY_TRAILER_PARK)
			REQUEST_WAYPOINT_RECORDING("trev3_trL")
			REQUEST_WAYPOINT_RECORDING("trev3_trR")
			PREFETCH_SRL("TREV3_TRAILER_ARRIVAL_CUTSCENE")
			WHILE NOT action(0,ACT_PREP_TRAILER_PARK)
			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("trev3_trL")
			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("trev3_trR")
			OR NOT IS_SRL_LOADED()
				SAFEWAIT()
			ENDWHILE
			
			BEGIN_SRL()
			SETTIMERA(12000)
			SET_SRL_TIME(12000)
			SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_ON)
			
			iCamZone = 0
			
			action(1,ACT_MECHANIC)
			action(2,ACT_TRAILER)
			action(3,ACT_BIKERS_DRIVE_OFF)
			ACTION(4,ACT_BIKERS_SITTING_CHATTING)
			action(5,ACT_BIKER_FIGHT)
			action(6,ACT_COFFIN_CRY)
			
			/*
			IF IS_REPLAY_IN_PROGRESS()
				CREATE_VEHICLE_FOR_REPLAY(vehicle[VEH_PLAYER].id, <<59.1493, 3605.3535, 38.8528>>,358.6649,FALSE,FALSE,FALSE,FALSE,TRUE) //set this to true because otherwise no vehicle was being created.
			ELSE
				CREATE_VEHICLE_FOR_REPLAY(vehicle[VEH_PLAYER].id, <<59.1493, 3605.3535, 38.8528>>,358.6649,FALSE,FALSE,FALSE,FALSE,TRUE)
			ENDIF			
			*/
			
			SET_REPLAY_STATE(false,true,true,<<59.9711, 3605.2766, 38.8392>>, 351.7204,<<60.5616, 3605.4238, 38.8620>>,4,<<59.7026, 3602.7822, 38.8023>>, 3.2394,zSkipped,false,bIsReplay)	
			
			
			SET_WADE_DEFAULTS()
			IF DOES_ENTITY_EXIST(vehicle[veh_player].id) AND NOT IS_ENTITY_DEAD(vehicle[veh_player].id)
			SET_PED_INTO_VEHICLE(player_ped_id(),vehicle[veh_player].id)
			ENDIF
			SET_WEATHER_TYPE_NOW_PERSIST("THUNDER")
			
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		BREAK
		CASE STAGE_DETROY_TRAILER_PARK
			cprintln(debug_trevor3,"Start skip : ",get_Game_timer())
			
			
			
			IF bIsReplay
				START_REPLAY_SETUP(<<59.9711, 3605.2766, 38.8392>>, 351.7204)
			ELSE
				SET_CLOCK_TIME(23,00,00)
			ENDIF
			
			clear_Area(<<59.9711, 3605.2766, 38.8392>>,200,TRUE) //to ensure all mission peds are cleaned up on a death retry
			cprintln(debug_trevor3,"Load assets : ",get_Game_timer())
			LOAD_STAGE_ASSETS(STAGE_DETROY_TRAILER_PARK)
			WHILE NOT action(0,ACT_PREP_TRAILER_PARK)
				SAFEWAIT()
			ENDWHILE
			cprintln(debug_trevor3,"Assets loaded : ",get_Game_timer())
		
			action(1,ACT_MECHANIC)
			action(2,ACT_TRAILER)
			action(3,ACT_BIKERS_DRIVE_OFF)
			ACTION(4,ACT_BIKERS_SITTING_CHATTING)
			action(5,ACT_BIKER_FIGHT)
			action(6,ACT_COFFIN_CRY)
			/*
			IF IS_REPLAY_IN_PROGRESS()
				CREATE_VEHICLE_FOR_REPLAY(vehicle[VEH_PLAYER].id, <<59.1493, 3605.3535, 38.8528>>,358.6649,FALSE,FALSE,FALSE,FALSE,TRUE) //set this to true because otherwise no vehicle was being created.
			ELSE
				CREATE_VEHICLE_FOR_REPLAY(vehicle[VEH_PLAYER].id, <<59.1493, 3605.3535, 38.8528>>,358.6649,FALSE,FALSE,FALSE,FALSE,TRUE)
			ENDIF
			*/
			
			SET_REPLAY_STATE(false,true,true,<<59.9711, 3605.2766, 38.8392>>, 351.7204,<<60.5616, 3605.4238, 38.8620>>,4,<<59.7026, 3602.7822, 38.8023>>, 3.2394,zSkipped,false,bIsReplay)	
			cprintln(debug_trevor3,"set replay state : ",get_Game_timer())
		
			SET_WADE_DEFAULTS()
			
			
			
			
			SET_WEATHER_TYPE_NOW_PERSIST("THUNDER")
			RESET_ACTIONS()
			PLAYMUSIC("TRV3_PLANTING_RT",FALSE,"TRV3_ALERTED")
			FORCE_ACTION_STATE(16,ACT_CONTROL_MUSIC,FALSE,TRUE)
			SET_ACTION_FLAG(16,ACT_CONTROL_MUSIC,1)
			cprintln(debug_trevor3,"end replay load : ",get_Game_timer())
		//	SET_PED_HELMET(player_ped_id,FALSE)
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
			cprintln(debug_trevor3,"skip ended : ",get_Game_timer())
		BREAK
		
		CASE STAGE_DESTROYED_CUTSCENE
			IF bIsReplay
				START_REPLAY_SETUP(<<54.6849, 3700.9927, 38.7550>>,325)
			ELSE
				SET_CLOCK_TIME(3,00,00)
			ENDIF
			REQUEST_PTFX_ASSET()
	 	
			/*
			IF IS_REPLAY_IN_PROGRESS()
				CREATE_VEHICLE_FOR_REPLAY(vehicle[VEH_PLAYER].id, <<59.1493, 3605.3535, 38.8528>>,358.6649,FALSE,FALSE,FALSE,FALSE,TRUE) //set this to true because otherwise no vehicle was being created.
			ELSE
				CREATE_VEHICLE_FOR_REPLAY(vehicle[VEH_PLAYER].id, <<59.1493, 3605.3535, 38.8528>>,358.6649,FALSE,FALSE,FALSE,FALSE,TRUE)
			ENDIF
			*/
			
			SET_REPLAY_STATE(false,false,true,<<54.6849, 3700.9927, 38.7550>>,325,<<60.5616, 3605.4238, 38.8620>>,4,<<59.7026, 3602.7822, 38.8023>>, 3.2394,false,false,bIsReplay)	
			
		
	 
			
			SET_WADE_DEFAULTS()		
			

			rayfireTrailer[0].state = resetForSkip
			rayfireTrailer[1].state = resetForSkip
			rayfireTrailer[2].state = resetForSkip
			rayfireTrailer[3].state = resetForSkip
			rayfireTrailer[4].state = resetForSkip	
			
			
			WHILE rayfireTrailer[0].state != PRIMING
			OR rayfireTrailer[1].state != PRIMING 
			OR rayfireTrailer[2].state != PRIMING 
			OR rayfireTrailer[3].state != PRIMING 
			OR rayfireTrailer[4].state != PRIMING 
				action(0,ACT_TRAILER_RAYFIRES)		
				safewait()
			ENDWHILE
			
			WHILE NOT HAS_PTFX_ASSET_LOADED()
				safewait()
			ENDWHILE
			PLAYMUSIC("TRV3_PLANTING_RT",FALSE,"TRV3_ALERTED")
		//	rayfireTrailer[0].state = resetAndGo
		//	rayfireTrailer[1].state = resetAndGo
		//	rayfireTrailer[2].state = resetAndGo
		//	rayfireTrailer[3].state = resetAndGo
		//	rayfireTrailer[4].state = resetAndGo	
			IF NOT bIsReplay
				SET_ENTITY_COORDS(player_ped_id(),<< 58.9388, 3617.5540, 38.6921 >>)
			ENDIF
			SET_WEATHER_TYPE_NOW_PERSIST("THUNDER")	
			
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		BREAK
		
		CASE STAGE_GET_BACK_TO_WADE
			IF bIsReplay
				START_REPLAY_SETUP(<<74.9264, 3640.3555, 38.6059>>,189)
			ELSE
				SET_CLOCK_TIME(3,00,00)
			ENDIF
			LOAD_STAGE_ASSETS(STAGE_DRIVE_TO_LS)
			REQUEST_MODEL(G_M_Y_LOST_01) 
			REQUEST_MODEL(G_M_Y_LOST_02)
			
			ADD_SCENARIO_BLOCKING_AREA( <<61,3694,38>>-<<300,300,300>>,<<61,3694,38>>+<<300,300,300>>)
			CLEAR_AREA(<<61,3694,38>>,130,TRUE)	

			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 92.6982, 3738.7275, 36.7299 >>,<< 105.6982, 3747.7275, 40.7299 >>,FALSE)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 43.4604, 3656.9370, 38.8369 >>-<<7,7,5>>,<< 43.4604, 3656.9370, 38.8369 >>+<<7,7,5>>,FALSE)
							
			SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<< 41.2529, 3709.4043, 38.7345 >>,100.0,0)
			
			SET_REPLAY_STATE(false,false,true,<<74.9264, 3640.3555, 38.6059>>,189,<<60.5616, 3605.4238, 38.8620>>,4,<<59.7026, 3602.7822, 38.8023>>, 3.2394,false,false,bIsReplay)
			
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,FALSE)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,FALSE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,FALSE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST,FALSE)
			
			SET_WADE_DEFAULTS()
			
			IF g_bShitskipAccepted
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 5, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 10, FALSE)
			ENDIF
			
			SET_WEATHER_TYPE_NOW_PERSIST("THUNDER")		
			
			rayfireTrailer[0].state = skip_to_blown
			rayfireTrailer[1].state = skip_to_blown
			rayfireTrailer[2].state = skip_to_blown
			rayfireTrailer[3].state = skip_to_blown
			rayfireTrailer[4].state = skip_to_blown	
			
			while rayfireTrailer[0].state != inactive
			or rayfireTrailer[1].state != inactive
			or rayfireTrailer[2].state != inactive
			or rayfireTrailer[3].state != inactive
			or rayfireTrailer[4].state != inactive
				action(0,ACT_TRAILER_RAYFIRES)
				SAFEWAIT()
			ENDWHILE
			
			WHILE NOT HAS_MODEL_LOADED(G_M_Y_LOST_01)
			OR NOT HAS_MODEL_LOADED(G_M_Y_LOST_02)
				safewait()
			endwhile
			//create enemy peds
			int iJ
			int iBit
			int iR
			iJ=0
			iBit=0
			iR=0
			vector vPos
			vPos=<<0,0,0>>
			
			
			IF g_bShitskipAccepted
				g_numEnemyAlive = 6
			ENDIF
			int attempts
			For ij = 0 to g_numEnemyAlive - 1
				iR = GET_RANDOM_INT_IN_RANGE(0,14)
				WHILE IS_BIT_SET(iBit,iR)
					iR++
					If iR > 13
						iR = 0
					ENDIF
				ENDWHILE
				set_bit(iBit,iR)
				
				SWITCH iR
					CASE 0 vPos = <<66.2449, 3651.4382, 38.4262>> BREAK
					CASE 1 vPos = <<46.1153, 3656.8059, 38.7626>> BREAK
					CASE 2 vPos = <<49.4054, 3679.3884, 38.7261>> BREAK
					CASE 3 vPos = <<63.3289, 3680.8069, 38.8364>> BREAK
					CASE 4 vPos = <<77.0489, 3683.1292, 38.6059>> BREAK
					CASE 5 vPos = <<92.5827, 3686.6082, 38.5058>> BREAK
					CASE 6 vPos = <<97.4204, 3714.3210, 38.5429>> BREAK
					CASE 7 vPos = <<104.7229, 3722.6169, 38.7102>> BREAK
					CASE 8 vPos = <<84.3981, 3731.7881, 38.5611>>BREAK
					CASE 9 vPos = <<79.5456, 3745.7966, 38.5850>> BREAK
					CASE 10 vPos = <<61.7966, 3746.0532, 38.7113>> BREAK
					CASE 11 vPos = <<59.7046, 3727.3372, 38.6582>> BREAK
					CASE 12 vPos = <<73.2938, 3714.7551, 38.7549>> BREAK
					CASE 13 vPos = <<27.7165, 3720.0862, 38.6902>> BREAK
					CASE 14 vPos = <<25.3776, 3685.6108, 38.5597>> BREAK
				ENDSWITCH
				
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vPos) > 32
				AND ij+1 != ped_wade
					IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
						IF GET_RANDOM_INT_IN_RANGE(0,3) = 0
							CREATE_BIKER_PED(iJ+1,G_M_Y_LOST_01,WEAPONTYPE_PUMPSHOTGUN,vPos,267)
							TASK_COMBAT_PED(ped[iJ+1].id,player_ped_id())
						ELSE
							CREATE_BIKER_PED(iJ+1,G_M_Y_LOST_01,WEAPONTYPE_ASSAULTRIFLE,vPos,267)
							TASK_COMBAT_PED(ped[iJ+1].id,player_ped_id())
						ENDIF
					ELSe
						IF GET_RANDOM_INT_IN_RANGE(0,3) = 0
							CREATE_BIKER_PED(iJ+1,G_M_Y_LOST_02,WEAPONTYPE_PUMPSHOTGUN,vPos,267)
							TASK_COMBAT_PED(ped[iJ+1].id,player_ped_id())
						ELSE
							CREATE_BIKER_PED(iJ+1,G_M_Y_LOST_02,WEAPONTYPE_ASSAULTRIFLE,vPos,267)
							TASK_COMBAT_PED(ped[iJ+1].id,player_ped_id())
						ENDIF
					ENDIF
				ELIF attempts < 3
					attempts++
					ij--
				ENDIF
			ENDFOR
			
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_01) 
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_02)
		//	ACTION(0,ACT_LOST_ALERTED_RESPONSE)
		//	RESET_ACTIONS()
			
			
			PLAYMUSIC("TRV3_GET_TO_CAR_RT",FALSE,"TRV3_ALERTED")
			missionProgress = STAGE_DRIVE_TO_LS
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		BREAK
		
		CASE STAGE_DRIVE_TO_LS
			IF bIsReplay
				START_REPLAY_SETUP(<<59.5340, 3603.4272, 38.8120>>,232)
			ELSE
				SET_CLOCK_TIME(3,00,00)
			ENDIF
			LOAD_STAGE_ASSETS(STAGE_DRIVE_TO_LS)
			
			SET_REPLAY_STATE(false,true,true,<<59.5340, 3603.4272, 38.8120>>,232,<<61.8752, 3603.8555, 38.7897>>, 122.5206,<<59.7026, 3602.7822, 38.8023>>, 3.2394,zSkipped,false,bIsReplay)
			
	
			SET_WADE_DEFAULTS()
			DISABLE_TAXI_HAILING(TRUE)
			
			SET_WEATHER_TYPE_NOW_PERSIST("THUNDER")		
			
			rayfireTrailer[0].state = skip_to_blown
			rayfireTrailer[1].state = skip_to_blown
			rayfireTrailer[2].state = skip_to_blown
			rayfireTrailer[3].state = skip_to_blown
			rayfireTrailer[4].state = skip_to_blown	
			
			while rayfireTrailer[0].state != inactive
			or rayfireTrailer[1].state != inactive
			or rayfireTrailer[2].state != inactive
			or rayfireTrailer[3].state != inactive
			or rayfireTrailer[4].state != inactive
				action(0,ACT_TRAILER_RAYFIRES)
				SAFEWAIT()
			ENDWHILE
			
			IF bIsReplay
				END_REPLAY_SETUP(vehicle[veh_player].id)
			ENDIF
		BREAK
		
		CASE STAGE_LS_VIEW_CUT
		
			IF bIsReplay
				START_REPLAY_SETUP(<<-1343.5785, 737.7505, 184.1437>>,184)
			ELSE
				SET_CLOCK_TIME(7,00,00) //blah
			ENDIF
			
			SET_REPLAY_STATE(false,true,true,<<-1343.5785, 737.7505, 184.1437>>, 48.3414, <<-1341.7830, 740.1107, 184.1154>>,96,<<-1344.5879, 742.2039, 183.7260>>, 112.1451,zSkipped,false,bIsReplay)
		
		
			SET_WADE_DEFAULTS()
			DISABLE_TAXI_HAILING(TRUE)
			
			FORCE_ACTION_STATE(0,ACT_UPDATE_AI_BLIPS,TRUE)
			FORCE_ACTION_STATE(1,ACT_TRAILER_RAYFIRES,TRUE)
			FORCE_ACTION_STATE(2,ACT_CLEAR_WEATHER,TRUE)
			FORCE_ACTION_STATE(3,ACT_CLEAN_UP_TRAILER_PARK,TRUE)
			FORCE_ACTION_STATE(4,ACT_SET_VEHICLE_RADIO,TRUE)
			FORCE_ACTION_STATE(5,ACT_IN_CAR_SHOVE_ANIM)
			FORCE_ACTION_STATE(6,ACT_LOAD_CUTSCENE)
			FORCE_ACTION_STATE(7,ACT_STOP_VEHICLE_PLAYER_EXIT)
			FORCE_ACTION_STATE(27,ACT_LS_VIEW_LEADIN)
			FORCE_ACTION_STATE(25,ACT_FLEEING_PEDS)
		
			FORCE_DIALOGUE_STATE(0,DIA_LETS_GO)
			FORCE_DIALOGUE_STATE(1,DIA_DRIVE_TO_LS_BANTER)
			FORCE_DIALOGUE_STATE(8,DIA_TREVOR_STOPS_BANTER)
			
			FORCE_INSTRUCTION_STATE(0,INS_GET_TO_LOS_SANTOS)
			
			checkConditions(COND_DRIVE_TO_LS_START,COND_DRIVE_TO_LS_END)
			SET_CONDITION_WAS_TRUE(COND_LS_CUTSCENE_FINISHED)
			
			//cprintln(debug_trevor3,"Request_cutscene()")
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("trv_dri_mcs_concat", CS_SECTION_1 | CS_SECTION_2)	
			WHILE NOT CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				safewait()
			endwhile
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())	
							
			WHILE NOT HAS_CUTSCENE_LOADED()
				cprintln(debug_trevor3,"HAS_CUTSCENE_LOADED()")
				safewait()
			ENDWHILE
			
			cprintln(debug_trevor3,"CUTSCENE_LOADED()")
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		BREAK
		CASE STAGE_DRIVE_TO_WADES_COUSIN
			IF bIsReplay
				START_REPLAY_SETUP(<<-1343.5785, 737.7505, 184.1437>>, 124.3414)
			ELSE
				SET_CLOCK_TIME(7,00,00)
				IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
					SET_ENTITY_COORDS(player_ped_id(),<<-1343.5785, 737.7505, 184.1437>>)
					SET_ENTITY_HEADING(player_ped_id(),48.8313)
				ENDIF
			ENDIF
			
			SET_REPLAY_STATE(false,true,true,<<-1343.5785, 737.7505, 184.1437>>, 48.3414, <<-1341.7830, 740.1107, 184.1154>>,96,<<-1344.5879, 742.2039, 183.7260>>, 112.1451,zSkipped,false,bIsReplay)
			
			SET_WADE_DEFAULTS()
			DISABLE_TAXI_HAILING(TRUE)
			
	

			
			FORCE_ACTION_STATE(0,ACT_UPDATE_AI_BLIPS,TRUE)
			FORCE_ACTION_STATE(1,ACT_TRAILER_RAYFIRES,TRUE)
			FORCE_ACTION_STATE(2,ACT_CLEAR_WEATHER,TRUE)
			FORCE_ACTION_STATE(3,ACT_CLEAN_UP_TRAILER_PARK,TRUE)
			FORCE_ACTION_STATE(4,ACT_SET_VEHICLE_RADIO,TRUE)
			FORCE_ACTION_STATE(5,ACT_IN_CAR_SHOVE_ANIM)
			FORCE_ACTION_STATE(6,ACT_LOAD_CUTSCENE)
			FORCE_ACTION_STATE(7,ACT_STOP_VEHICLE_PLAYER_EXIT)
			FORCE_ACTION_STATE(8,ACT_PLAY_LS_CUTSCENE)
			FORCE_ACTION_STATE(25,ACT_FLEEING_PEDS)
			FORCE_ACTION_STATE(27,ACT_LS_VIEW_LEADIN)
			
			FORCE_DIALOGUE_STATE(0,DIA_LETS_GO)
			FORCE_DIALOGUE_STATE(1,DIA_DRIVE_TO_LS_BANTER)
			FORCE_DIALOGUE_STATE(8,DIA_TREVOR_STOPS_BANTER)
			
			FORCE_INSTRUCTION_STATE(0,INS_GET_TO_LOS_SANTOS)
			
			checkConditions(COND_DRIVE_TO_LS_START,COND_DRIVE_TO_LS_END)
			SET_CONDITION_STATE(COND_LS_CUTSCENE_FINISHED,TRUE)
			SET_CONDITION_STATE(COND_STICK_JAMMED_LINE_PLAYED,TRUE)
		//	WHILE GET_DIALOGUE_FLAG(DIA_DRIVE_TO_WADES_BANTER) = 0
		//		dialogue(5,DIA_DRIVE_TO_WADES_BANTER)
		//		safewait(0)
		//	endwhile	
		//	
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
			IF bIsReplay
				END_REPLAY_SETUP(vehicle[veh_player].id)
			ENDIF
		BREAK
		CASE STAGE_FINAL_CUT
			IF bIsReplay
				START_REPLAY_SETUP(<<-1156.1613, -1521.3652, 3.3237>>,227)
			ENDIF
			//cprintln(debug_Trevor3,"Final cutscene")
			SET_REPLAY_STATE(false,false,false,<<-1157.7205, -1520.5835, 9.6327>>, 295.1327,<<-1153.7759, -1521.1268, 3.3193>>, 241.2621,<<-1154.5667, -1519.4229, 3.3617>>, 218.8170,zSkipped,false,bIsReplay)					
		
			SET_WADE_DEFAULTS()
							
			ACTION(0,ACT_LOAD_WADE_COUSIN_CUTSCENE)
			
			//cprintln(debug_Trevor3,"ACT_LOAD_WADE_COUSIN_CUTSCENE")
			while GET_ACTION_FLAG(0,ACT_LOAD_WADE_COUSIN_CUTSCENE) < 4
				REQUEST_CUTSCENE("TRV_DRI_EXT")
				//cprintln(debug_Trevor3,"doing ACT_LOAD_WADE_COUSIN_CUTSCENE: ",GET_ACTION_FLAG(0,ACT_LOAD_WADE_COUSIN_CUTSCENE))
				ACTION(0,ACT_LOAD_WADE_COUSIN_CUTSCENE)
				safewait(0)
			endwhile
		
			//cprintln(debug_Trevor3,"ACT_LOAD_WADE_COUSIN_CUTSCENE complete")
			RESET_ACTIONS()		
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		BREAK
		CASE STAGE_SAFEHOUSE_SHOTS
			//cprintln(debug_Trevor3,"Final cutscene")
			SET_REPLAY_STATE(false,false,false,<<-1157.7205, -1520.5835, 9.6327>>, 295.1327,<<-1153.7759, -1521.1268, 3.3193>>, 241.2621,<<-1154.5667, -1519.4229, 3.3617>>, 218.8170,zSkipped,false,bIsReplay)					
			LOAD_SCENE(<<-1157.7205, -1520.5835, 9.6327>>)
		
			if does_entity_exist(ped[ped_wade].id)
				delete_ped(ped[ped_wade].id)
			ENDIF
			
			int_wade = GET_INTERIOR_AT_COORDS(<< -1154.8173, -1518.3027, 9.6345 >>)
			
			WHILE NOT IS_VALID_INTERIOR(int_wade)
				int_wade = GET_INTERIOR_AT_COORDS(<< -1154.8173, -1518.3027, 9.6345 >>)
				safewait()
			endwhile
			
			PIN_INTERIOR_IN_MEMORY(int_wade)
			
			 while not IS_INTERIOR_READY(int_wade)
			 	safewait()
			endwhile
			
			SET_INTERIOR_ACTIVE(int_wade, TRUE)		
			
			RESET_ACTIONS()			
		BREAK
	ENDSWITCH
	
ENDPROC

// ================================================== debug =====================================================
#if IS_DEBUG_BUILD


	FUNC BOOL IS_SKIP_REQUESTED(int &iReturnStage)
		
		bool bDoSkip
		
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct,iReturnStage,iReturnStage)	
			PRINTLN("STAGE : ",iReturnStage)
			bDoSkip = true
		ELSE			
			If IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				iReturnStage = enum_to_int(missionProgress)
				IF missionProgress = STAGE_DRIVE_TO_LS
					IF NOT IS_CONDITION_TRUE(COND_BACK_WITH_WADE)
						iReturnStage--
					ENDIF
				ENDIF
				bDoSkip = true
				iReturnStage--
				
			ENDIF
			
			If IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)			
				iReturnStage = enum_to_int(missionProgress)
				bDoSkip = true
				iReturnStage++
			ENDIF
		ENDIF
		
		
		If bDoSkip = true
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDFUNC

	

	PROC PRINT_CONDITION_DATA(int iCon)
		string sCond
		enumConditions thisCond = conditions[iCon].condition
		sCond = GET_CONDITION_STRING(thisCond)
		
		
			
		SET_TEXT_SCALE(0.25,0.25)
		IF conditions[iCon].returns		
			SET_TEXT_COLOUR(0,0,255,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iCon*0.02),"STRING",sCond)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.25,0.25)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iCon*0.02),"STRING","TRUE")
		ELSE
			
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iCon*0.02),"STRING",sCond)
			SET_TEXT_COLOUR(255,0,0,255)
			SET_TEXT_SCALE(0.25,0.25)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iCon*0.02),"STRING","FALSE")
		ENDIF

		SET_TEXT_COLOUR(255,0,0,255)
		SET_TEXT_SCALE(0.25,0.25)
		showIntOnScreen(0.5,0.1+(iCon*0.02),conditions[iCon].flag,"",0.35)

	ENDPROC

	

	PROC PRINT_DIALOGUE_DATA(int iDia)
		string sDia
		enumDialogue thisDia = dia[iDia].dial
		
		sDia = GET_DIALOGUE_STRING(thisDia)
		SET_TEXT_SCALE(0.35,0.35)
		IF dia[iDia].bCompleted		
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iDia*0.03),"STRING",sDia)
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iDia*0.03),"STRING","TRUE")
		ELSE
			
			SET_TEXT_COLOUR(0,0,255,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iDia*0.03),"STRING",sDia)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iDia*0.03),"STRING","FALSE")
		ENDIF
		
		SET_TEXT_SCALE(0.35,0.35)
		IF dia[iDia].bStarted		
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iDia*0.03),"STRING","TRUE")
		ELSE
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iDia*0.03),"STRING","FALSE")
		ENDIF
		
		SET_TEXT_COLOUR(255,0,0,255)
		showIntOnScreen(0.5,0.1+(iDia*0.03),dia[iDia].flag,"",0.35)
		
		SET_TEXT_SCALE(0.35,0.35)
		IF dia[iDia].condTrue		
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.56,0.1+(iDia*0.03),"STRING","TRUE")
		ELSE
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.56,0.1+(iDia*0.03),"STRING","FALSE")
		ENDIF
		
	ENDPROC

	PROC PRINT_INSTRUCTION_DATA(int iData)
		string sInst
		enumInstructions thisInst = instr[iData].ins
		SWITCH thisInst		 
			CASE INS_NULL sInst = "INS_NULL" BREAK
			CASE INS_GET_TO_TRAILER_PARK sInst = "INS_GET_TO_TRAILER_PARK" BREAK 
			DEFAULT sInst = " -NOT DEFINED-" BREAK
		ENDSWITCH
		
		
		IF instr[iData].bCompleted
			SET_TEXT_COLOUR(255,0,0,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03),"STRING",sInst)
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.39,0.1+(iData*0.03),"STRING","TRUE")		
		ELSE
			
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03),"STRING",sInst)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.39,0.1+(iData*0.03),"STRING","FALSE")
		ENDIF
		
		SET_TEXT_SCALE(0.35,0.35)
		IF instr[iData].condTrue		
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iData*0.03),"STRING","TRUE")
		ELSE
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iData*0.03),"STRING","FALSE")
		ENDIF
			
		showIntOnScreen(0.5,0.1+(iData*0.03),instr[iData].flag,"",0.35)
		showIntOnScreen(0.55,0.1+(iData*0.03),instr[iData].intA,"",0.35)
		
	ENDPROC

	FUNC STRING GET_ACTION_STRING(enumActions thisAct)
	
	SWITCH thisAct
		CASE	ACT_NULL	RETURN "	ACT_NULL	" BREAK
CASE	ACT_LOAD_INTRO_CUT	RETURN "	ACT_LOAD_INTRO_CUT	" BREAK
CASE	ACT_PLAY_CUTSCENE	RETURN "	ACT_PLAY_CUTSCENE	" BREAK
CASE	ACT_RESOLVE_VEHICLES_AT_START	RETURN "	ACT_RESOLVE_VEHICLES_AT_START	" BREAK
CASE	ACT_EXTRACT_WADE_FROM_CUTSCENE	RETURN "	ACT_EXTRACT_WADE_FROM_CUTSCENE	" BREAK
CASE	ACT_WADE_GETS_IN_BODHI	RETURN "	ACT_WADE_GETS_IN_BODHI	" BREAK
CASE	ACT_UPDATE_PLAYER_VEHICLE	RETURN "	ACT_UPDATE_PLAYER_VEHICLE	" BREAK
CASE	ACT_MAKE_WADE_AND_PLAYER_ENTER_SPECIFIC_SEAT	RETURN "	ACT_MAKE_WADE_AND_PLAYER_ENTER_SPECIFIC_SEAT	" BREAK
CASE	ACT_MAKE_IT_RAIN	RETURN "	ACT_MAKE_IT_RAIN	" BREAK
CASE	ACT_PREP_TRAILER_PARK	RETURN "	ACT_PREP_TRAILER_PARK	" BREAK
CASE	ACT_RELEASE_TRAILER_PARK	RETURN "	ACT_RELEASE_TRAILER_PARK	" BREAK
CASE	ACT_SET_PED_DENSITY_EVERY_FRAME	RETURN "	ACT_SET_PED_DENSITY_EVERY_FRAME	" BREAK
CASE	ACT_AUDIO_SCENE_GETTING_IN_CAR	RETURN "	ACT_AUDIO_SCENE_GETTING_IN_CAR	" BREAK
CASE	ACT_LIGHTNING_FLASH	RETURN "	ACT_LIGHTNING_FLASH	" BREAK
CASE	ACT_KILL_BANTER	RETURN "	ACT_KILL_BANTER	" BREAK
CASE	ACT_MECHANIC	RETURN "	ACT_MECHANIC	" BREAK
CASE	ACT_AUDIO_FOR_EXPLOSIONS	RETURN "	ACT_AUDIO_FOR_EXPLOSIONS	" BREAK
CASE	ACT_TRAILER	RETURN "	ACT_TRAILER	" BREAK
CASE	ACT_BIKERS_DRIVE_OFF	RETURN "	ACT_BIKERS_DRIVE_OFF	" BREAK
CASE	ACT_BIKERS_SITTING_CHATTING	RETURN "	ACT_BIKERS_SITTING_CHATTING	" BREAK
CASE	ACT_BIKER_FIGHT	RETURN "	ACT_BIKER_FIGHT	" BREAK
CASE	ACT_COFFIN_CRY	RETURN "	ACT_COFFIN_CRY	" BREAK
CASE	ACT_COFFIN_RESTART_ANIM	RETURN "	ACT_COFFIN_RESTART_ANIM	" BREAK
CASE	ACT_RECORD_CAMERA	RETURN "	ACT_RECORD_CAMERA	" BREAK
CASE	ACT_REWARD_BOMB_PLANTING_UNOBSERVED	RETURN "	ACT_REWARD_BOMB_PLANTING_UNOBSERVED	" BREAK
CASE	ACT_WADE_RUNS_OUT_OF_TRAILER_PARK 	RETURN "	ACT_WADE_RUNS_OUT_OF_TRAILER_PARK 	" BREAK
CASE	ACT_REWARD_BLOW_ALL_AT_ONCE	RETURN "	ACT_REWARD_BLOW_ALL_AT_ONCE	" BREAK
CASE	ACT_REWARD_STICKY_BOMB_USAGE	RETURN "	ACT_REWARD_STICKY_BOMB_USAGE	" BREAK
CASE	ACT_WADE_LOOKS_AT_TREVOR_NEAR_CAR	RETURN "	ACT_WADE_LOOKS_AT_TREVOR_NEAR_CAR	" BREAK
CASE	ACT_DISABLE_STICKY_BOMB_TRIGGER	RETURN "	ACT_DISABLE_STICKY_BOMB_TRIGGER	" BREAK
CASE	ACT_AUDIO_SCENE_PLANT_STICKY_BOMBS	RETURN "	ACT_AUDIO_SCENE_PLANT_STICKY_BOMBS	" BREAK
CASE	ACT_LOAD_AUDIO_STREAM	RETURN "	ACT_LOAD_AUDIO_STREAM	" BREAK
CASE	ACT_DELAY_ENEMY_SHOOTING	RETURN "	ACT_DELAY_ENEMY_SHOOTING	" BREAK
CASE	ACT_FRUSTUM_SETUP_FOR_FRIST_CUT	RETURN "	ACT_FRUSTUM_SETUP_FOR_FRIST_CUT	" BREAK
CASE	ACT_CUTSCENE_CAM	RETURN "	ACT_CUTSCENE_CAM	" BREAK
CASE	ACT_DISABLE_PLAYER_CONTROL	RETURN "	ACT_DISABLE_PLAYER_CONTROL	" BREAK
CASE	ACT_SET_BOMB_PLANTING_STUFF	RETURN "	ACT_SET_BOMB_PLANTING_STUFF	" BREAK
CASE	ACT_LOST_ALERTED_RESPONSE	RETURN "	ACT_LOST_ALERTED_RESPONSE	" BREAK
CASE	ACT_PED_REACTS_TO_STICKY_BOMB	RETURN "	ACT_PED_REACTS_TO_STICKY_BOMB	" BREAK
CASE	ACT_UPDATE_AI_BLIPS	RETURN "	ACT_UPDATE_AI_BLIPS	" BREAK
CASE	ACT_UNFIX_BIKES_FROM_SYNC_SCENES	RETURN "	ACT_UNFIX_BIKES_FROM_SYNC_SCENES	" BREAK
CASE	ACT_MECHANIC_ANIM_RESETS_AFTER_INTERUPTION	RETURN "	ACT_MECHANIC_ANIM_RESETS_AFTER_INTERUPTION	" BREAK
CASE	ACT_BEAT_UP_GUY_REACTS_TO_TREVOR	RETURN "	ACT_BEAT_UP_GUY_REACTS_TO_TREVOR	" BREAK
CASE	ACT_FIND_ANIM_TO_TRIGGER	RETURN "	ACT_FIND_ANIM_TO_TRIGGER	" BREAK
CASE	ACT_BEAT_UP_ANIMATION_PLAYS_OUT	RETURN "	ACT_BEAT_UP_ANIMATION_PLAYS_OUT	" BREAK
CASE	ACT_BIKERS_DRIVE_OFF_ANIM_PLAYS_OUT	RETURN "	ACT_BIKERS_DRIVE_OFF_ANIM_PLAYS_OUT	" BREAK
CASE	ACT_TRAILER_ANIM_PLAYS_OUT	RETURN "	ACT_TRAILER_ANIM_PLAYS_OUT	" BREAK
CASE	ACT_COFFIN_GUY_DROPS_BOTTLE	RETURN "	ACT_COFFIN_GUY_DROPS_BOTTLE	" BREAK
CASE	ACT_DETACH_COFFIN	RETURN "	ACT_DETACH_COFFIN	" BREAK
CASE	ACT_TRAILER_RAYFIRES	RETURN "	ACT_TRAILER_RAYFIRES	" BREAK
CASE	ACT_REMOVE_TRAILER_BLIPS	RETURN "	ACT_REMOVE_TRAILER_BLIPS	" BREAK
CASE	ACT_ENSURE_ONE_PED_ENGAGING_PLAYER	RETURN "	ACT_ENSURE_ONE_PED_ENGAGING_PLAYER	" BREAK
CASE	ACT_CONTROL_MUSIC	RETURN "	ACT_CONTROL_MUSIC	" BREAK
CASE	ACT_STOP_TRUCK_FROM_BURNING	RETURN "	ACT_STOP_TRUCK_FROM_BURNING	" BREAK
CASE	ACT_FOUGHT_GUY_FELL_AND_NOW_WONT_REACT	RETURN "	ACT_FOUGHT_GUY_FELL_AND_NOW_WONT_REACT	" BREAK
CASE	ACT_STOP_TRAILER_WOMAN_GETTING_CUT_OFF	RETURN "	ACT_STOP_TRAILER_WOMAN_GETTING_CUT_OFF	" BREAK
CASE	ACT_UPDATE_TRAILER_GUY_BLIP	RETURN "	ACT_UPDATE_TRAILER_GUY_BLIP	" BREAK
CASE	ACT_SET_LOST_ALERTED_STUFF	RETURN "	ACT_SET_LOST_ALERTED_STUFF	" BREAK
CASE	ACT_BIKER_REACTIONS	RETURN "	ACT_BIKER_REACTIONS	" BREAK
CASE	ACT_EXPLOSION_CAMS	RETURN "	ACT_EXPLOSION_CAMS	" BREAK
CASE	ACT_TIMED_EXPLOSIONS	RETURN "	ACT_TIMED_EXPLOSIONS	" BREAK
CASE	ACT_CLEAR_WEATHER	RETURN "	ACT_CLEAR_WEATHER	" BREAK
CASE	ACT_CLEAN_UP_TRAILER_PARK	RETURN "	ACT_CLEAN_UP_TRAILER_PARK	" BREAK
CASE	ACT_SET_VEHICLE_RADIO	RETURN "	ACT_SET_VEHICLE_RADIO	" BREAK
CASE	ACT_IN_CAR_SHOVE_ANIM	RETURN "	ACT_IN_CAR_SHOVE_ANIM	" BREAK
CASE	ACT_LOAD_CUTSCENE	RETURN "	ACT_LOAD_CUTSCENE	" BREAK
CASE	ACT_STOP_VEHICLE	RETURN "	ACT_STOP_VEHICLE	" BREAK
CASE	ACT_STOP_VEHICLE_PLAYER_EXIT	RETURN "	ACT_STOP_VEHICLE_PLAYER_EXIT	" BREAK
CASE	ACT_LS_VIEW_LEADIN	RETURN "	ACT_LS_VIEW_LEADIN	" BREAK
CASE	ACT_PLAY_LS_CUTSCENE	RETURN "	ACT_PLAY_LS_CUTSCENE	" BREAK
CASE	ACT_LOAD_WADE_COUSIN_CUTSCENE	RETURN "	ACT_LOAD_WADE_COUSIN_CUTSCENE	" BREAK
CASE	ACT_STOP_VEHICLE_DOOR_GETTING_KNOCKED_OFF	RETURN "	ACT_STOP_VEHICLE_DOOR_GETTING_KNOCKED_OFF	" BREAK
CASE	ACT_PREVENT_WADE_LEAVING_BURNING_VEHICLE	RETURN "	ACT_PREVENT_WADE_LEAVING_BURNING_VEHICLE	" BREAK
CASE	ACT_ESTABLISHING_SHOT_AT_WADES	RETURN "	ACT_ESTABLISHING_SHOT_AT_WADES	" BREAK
CASE	ACT_WADE_PLAYS_SHOUT_ANIM	RETURN "	ACT_WADE_PLAYS_SHOUT_ANIM	" BREAK
CASE	ACT_WADE_ENTERS_PLAYERS_GROUP	RETURN "	ACT_WADE_ENTERS_PLAYERS_GROUP	" BREAK
CASE	ACT_BLOCK_PLAYER_FOR_LEAD_IN	RETURN "	ACT_BLOCK_PLAYER_FOR_LEAD_IN	" BREAK
CASE	ACT_MUSIC_IN_VEHICLE_GOING_TO_LS	RETURN "	ACT_MUSIC_IN_VEHICLE_GOING_TO_LS	" BREAK
CASE	ACT_AUDIO_SCENE_DRIVE_TO_LS	RETURN "	ACT_AUDIO_SCENE_DRIVE_TO_LS	" BREAK
CASE	ACT_AUDIO_SCENE_DRIVE_TO_WADES_COUSIN	RETURN "	ACT_AUDIO_SCENE_DRIVE_TO_WADES_COUSIN	" BREAK
CASE	ACT_PREP_FRUSTUM_AT_WADES_COUSIN	RETURN "	ACT_PREP_FRUSTUM_AT_WADES_COUSIN	" BREAK
CASE	ACT_DO_END_CUTSCENE	RETURN "	ACT_DO_END_CUTSCENE	" BREAK
CASE	ACT_SAFEHOUSE_INTRO_SHOT	RETURN "	ACT_SAFEHOUSE_INTRO_SHOT	" BREAK
CASE	ACT_BED_SHOT	RETURN "	ACT_BED_SHOT	" BREAK
CASE	ACT_ACTIVITIES_SHOT	RETURN "	ACT_ACTIVITIES_SHOT	" BREAK
CASE	ACT_WARDROBE_SHOT	RETURN "	ACT_WARDROBE_SHOT	" BREAK
CASE	ACT_SKIP_CUTS	RETURN "	ACT_SKIP_CUTS	" BREAK
ENDSWITCH
RETURN "UNKNWN"
	ENDFUNC

	PROC PRINT_ACTION_DATA(int iData,bool shiftUp=FALSE)
		TEXT_LABEL_63 txtLbl
			
	
		string sAct
		enumActions thisAct = actions[iData].action
		sACt = GET_ACTION_STRING(thisAct)
		txtLbl = ""
		txtlbl += iData
		txtlbl += " "
		txtLbl += sAct
		float shiftY
		IF shiftUp
			shiftY = -0.36
		ENDIF
		
		
		SET_TEXT_SCALE(0.35,0.35)
		
		IF actions[iData].ongoing
			txtLbl += " TRUE "
		ELSE
			txtLbl += " FALSE "		
		ENDIF
					
		IF actions[iData].completed
			txtLbl += " TRUE "
		ELSE
			txtLbl += " FALSE "		
		ENDIF
			
		txtLbl += actions[iData].flag
		txtlbl += " "
		txtlbl += actions[iData].intA
		
		IF actions[iData].completed
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",txtlbl)
		ELSE
			IF actions[iData].ongoing
				SET_TEXT_COLOUR(0,255,0,255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",txtlbl)
			ELSE
				SET_TEXT_COLOUR(0,0,255,255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",txtlbl)
			ENDIF
		ENDIF
		
	ENDPROc


	proc SHOW_DEBUG_SCRIPT_DATA()
		int iR

		IF bAllowShitskip
			g_savedGlobals.sFlow.missionSavedData[ENUM_TO_INT(SP_MISSION_TREVOR_3)].missionFailsNoProgress = 3
			bAllowShitskip = FALSE
		ENDIF
		
		IF bScreenConditions
			bscreenDialogue = FALSE
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.05,"STRING","Returns")
			
			REPEAT COUNT_OF(conditions) iR
				IF conditions[iR].active
					PRINT_CONDITION_DATA(iR)
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF bscreenDialogue
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.42,0.05,"STRING","Complete")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.47,0.05,"STRING","started")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.56,0.05,"STRING","COND TRUE")
			REPEAT COUNT_OF(dia) iR
				PRINT_DIALOGUE_DATA(iR)
			ENDREPEAT
		ENDIF
		
		IF bscreenInstructions
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.38,0.05,"STRING","Complete")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.44,0.05,"STRING","Cond")
			REPEAT COUNT_OF(instr) iR
				PRINT_INSTRUCTION_DATA(iR)
			ENDREPEAT
		ENDIF
		
		IF bScreenActionsA
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.05,"STRING","Ongoing")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.05,"STRING","Complete")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.57,0.05,"STRING","Flag")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.62,0.05,"STRING","IntA")
			FOR iR = 0 to 12
				IF actions[iR].active	
					PRINT_ACTION_DATA(iR)
				ENDIF
			ENDFOR
		ENDIF
		
		IF bScreenActionsB
			
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.05,"STRING","Ongoing")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.05,"STRING","Complete")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.57,0.05,"STRING","Flag")
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.62,0.05,"STRING","IntA")
		
			FOR iR = 13 to COUNT_OF(actions)-1
				IF actions[iR].active	
					PRINT_ACTION_DATA(iR,TRUE)
				ENDIF
			ENDFOR
		ENDIF
		
		If bSkipJourney
			bSkipJourney = FALSE
			IF missionProgress = STAGE_GET_TO_TRAILER_PARK
				IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
				AND NOT IS_PED_FUCKED(ped[ped_wade].id)
					SET_PED_INTO_VEHICLE(player_ped_id(),vehicle[veh_player].id,VS_DRIVER)
					SET_PED_INTO_VEHICLE(ped[ped_wade].id,vehicle[veh_player].id,VS_FRONT_RIGHT)
					SET_ENTITY_COORDS(vehicle[veh_player].id,<< 318.8481, 3439.1360, 35.4362 >>)
					SET_ENTITY_HEADING(vehicle[veh_player].id,115)
				ENDIF
			ENDIF
			
			IF missionProgress = STAGE_DRIVE_TO_LS
				IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
				AND NOT IS_PED_FUCKED(ped[ped_wade].id)
					SET_PED_INTO_VEHICLE(player_ped_id(),vehicle[veh_player].id,VS_DRIVER)
					SET_PED_INTO_VEHICLE(ped[ped_wade].id,vehicle[veh_player].id,VS_FRONT_RIGHT)
					SET_ENTITY_COORDS(vehicle[veh_player].id,<<-1259.3911, 881.3350, 190.3703>>)
					SET_ENTITY_HEADING(vehicle[veh_player].id,143)
				ENDIF
			ENDIF
			
			IF missionProgress = STAGE_DRIVE_TO_WADES_COUSIN
				IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
				AND NOT IS_PED_FUCKED(ped[ped_wade].id)
					SET_PED_INTO_VEHICLE(player_ped_id(),vehicle[veh_player].id,VS_DRIVER)
					SET_PED_INTO_VEHICLE(ped[ped_wade].id,vehicle[veh_player].id,VS_FRONT_RIGHT)
					SET_ENTITY_COORDS(vehicle[veh_player].id,<< -1209.6541, -1409.3705, 3.1587 >>)
					SET_ENTITY_HEADING(vehicle[veh_player].id,214)
				ENDIF
			ENDIF
		ENDIF
		
		IF bBlowUpTrailers
		//	bBlowUpTrailers = FALSE
		/*	REPEAT COUNT_OF(rayfireTrailer) i
				ADD_EXPLOSION((rayfireTrailer[i].vLocates[0] + rayfireTrailer[i].vLocates[1])/2.0,EXP_TAG_GRENADE)
			ENDREPEAT*/
		ENDIF
		
		IF bMakeBang
			bMakeBang = FALSE
			ADD_EXPLOSION_WITH_USER_VFX(GET_ENTITY_COORDS(player_ped_id())+<<0,0,2>>,exp_tag_grenade,get_hash_key("EXP_VFXTAG_TREV3_TRAILER"),0.5)
		ENDIF
		
		If bPlantStickyBombs
			SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_STICKYBOMB,TRUE)
			SET_ENTITY_COORDS(player_ped_id(),<<28.1357, 3664.6802, 42.7987>>)
		//	TASK_SHOOT_AT_COORD(player_ped_id(),<<28.1357, 3666.6802, 42.7987>>,5500,FIRING_TYPE_CONTINUOUS)
			
			WHILE NOT IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(player_ped_id()),WEAPONTYPE_STICKYBOMB,5,true)
				safeWAIT(0)
			ENDWHILE
			
			
			SET_ENTITY_COORDS(player_ped_id(),<<39.1422, 3702.8313, 42.8996>>)
			TASK_SHOOT_AT_COORD(player_ped_id(),<<39.1422, 3702.8313, 42.8996>>,500,FIRING_TYPE_1_THEN_AIM)
			
			WHILE NOT IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(player_ped_id()),WEAPONTYPE_STICKYBOMB,5,true)
				safeWAIT(0)
			ENDWHILE
			
			SET_ENTITY_COORDS(player_ped_id(),<<69.6369, 3687.8879, 42.9993>>)
			TASK_SHOOT_AT_COORD(player_ped_id(),<<69.6369, 3687.8879, 42.9993>>,500,FIRING_TYPE_1_THEN_AIM)
			
			WHILE NOT IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(player_ped_id()),WEAPONTYPE_STICKYBOMB,5,true)
				safeWAIT(0)
			ENDWHILE
			
			SET_ENTITY_COORDS(player_ped_id(),<<110.8766, 3728.1765, 42.6414>>)
			TASK_SHOOT_AT_COORD(player_ped_id(),<<110.8766, 3728.1765, 42.6414>>,500,FIRING_TYPE_1_THEN_AIM)
			
			WHILE NOT IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(player_ped_id()),WEAPONTYPE_STICKYBOMB,5,true)
				safeWAIT(0)
			ENDWHILE
			
			SET_ENTITY_COORDS(player_ped_id(),<<46.1249, 3745.2603, 42.6737>>)
			TASK_SHOOT_AT_COORD(player_ped_id(),<<46.1249, 3745.2603, 42.6737>>,500,FIRING_TYPE_1_THEN_AIM)
			
			WHILE NOT IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(player_ped_id()),WEAPONTYPE_STICKYBOMB,5,true)
				safeWAIT(0)
			ENDWHILE
			
			SET_ENTITY_COORDS(player_ped_id(),<<98.3366, 3671.0212, 38.7549>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(),90)
			bPlantStickyBombs=FALSE
		ENDIF
		
	endproc 

	int iProgress
	proc debug()
		SHOW_DEBUG_SCRIPT_DATA()
		bLastConvoWithoutSubtitles=bLastConvoWithoutSubtitles
		dia[0] = dia[0]
		instr[0] = instr[0]
		IF IS_KEYBOARD_KEY_PRESSED(KEY_S)			
			MISSION_PASSED()
		ENDIF
		IF IS_KEYBOARD_KEY_PRESSED(KEY_f)				
			Mission_Failed()
		ENDIF
		int skiptoStage
		IF IS_SKIP_REQUESTED(skiptoStage)		
			DO_SKIP(int_to_enum(enumMissionStage,skiptoStage),TRUE)
		ENDIF
		
		iProgress = enum_to_int(missionProgress)
		if not DOES_WIDGET_GROUP_EXIST(trev3_widget)			
			
			trev3_widget = START_WIDGET_GROUP("Trevor 3")
			ADD_WIDGET_BOOL("Allow Shit skip",bAllowShitskip)
			ADD_WIDGET_BOOL("Show Conditions",bScreenConditions)
			ADD_WIDGET_BOOL("Show Actions 0-12",bScreenActionsA)
			ADD_WIDGET_BOOL("Show Actions 12+",bScreenActionsB)
			ADD_WIDGET_BOOL("Show Dialogue",bscreenDialogue)
			ADD_WIDGET_BOOL("Show Instructions",bScreenInstructions)
			ADD_WIDGET_INT_READ_ONLY("Mission Progress",iProgress)	
			ADD_WIDGET_BOOL("Skip Journey",bSkipJourney)
			ADD_WIDGET_BOOL("Blow up trailersy",bBlowUpTrailers)
			ADD_WIDGET_BOOL("Plant bombs",bPlantStickyBombs)
			ADD_WIDGET_BOOL("bLastConvoWithoutSubtitles",bLastConvoWithoutSubtitles)	
			ADD_WIDGET_BOOL("Make explosion",bMakeBang)
			STOP_WIDGET_GROUP()
			
			SET_LOCATES_HEADER_WIDGET_GROUP(trev3_widget)
		ENDIF
		
	endproc
#endif

// ======================================================= Missions Stages ===========================================

FUNC BOOL do_initialise()

	DISABLE_TAXI_HAILING(TRUE)
	//cprintln(debug_Trevor3,"init started: ",get_game_timer())
	#if IS_DEBUG_BUILD
		SkipMenuStruct[0].sTxtLabel = "initialise"  
		SkipMenuStruct[1].sTxtLabel = "Intro Cut (trevor_drive_int)"
		SkipMenuStruct[2].sTxtLabel = "get to trailer park"
		SkipMenuStruct[3].sTxtLabel = "Trailer Park Cut"
		SkipMenuStruct[4].sTxtLabel = "plant bombs" 
		SkipMenuStruct[5].sTxtLabel = "trailer explosion cut" 
		SkipMenuStruct[6].sTxtLabel = "Get back to Wade in car" 
		SkipMenuStruct[7].sTxtLabel = "Drive to LS" 
		SkipMenuStruct[8].sTxtLabel = "LS cutscene (TRV_DRI_MCS_concat)" 
		SkipMenuStruct[9].sTxtLabel = "Drive to Wade's cousin"
		SkipMenuStruct[10].sTxtLabel = "Final cutscene (TRV_DRI_EXT)"
		SkipMenuStruct[11].sTxtLabel = "Show safehouse shots"
	#endif
	g_bBlockShopRoberies = TRUE 
	SET_VEHICLE_MODEL_IS_SUPPRESSED(bodhi2,true)
	SET_VEHICLE_CONVERSATIONS_PERSIST(TRUE, TRUE)
	REQUEST_ADDITIONAL_TEXT("TREV3", MISSION_TEXT_SLOT)
	
	ADD_RELATIONSHIP_GROUP("relGroupDislike",relGroupDislike)
	ADD_RELATIONSHIP_GROUP("Gangb", relGroupEnemy)
	ADD_RELATIONSHIP_GROUP("Wade", RELGROUPHASH_NEUTRAL_TO_PLAYER)
	ADD_RELATIONSHIP_GROUP("fought ped", relGroupFoughtBiker)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_NEUTRAL_TO_PLAYER,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,RELGROUPHASH_NEUTRAL_TO_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,relGroupEnemy,RELGROUPHASH_NEUTRAL_TO_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,RELGROUPHASH_NEUTRAL_TO_PLAYER,relGroupEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relGroupEnemy,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relGroupDislike,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT,relGroupEnemy,relGroupFoughtBiker)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT,relGroupFoughtBiker,relGroupEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,relGroupFoughtBiker,RELGROUPHASH_PLAYER)
	
	int iR,iS
	TEXT_LABEL_7 txt
	REPEAT COUNT_OF(relPreCombatEnemy) iR
		txt = "nGang"
		txt+= iR
		ADD_RELATIONSHIP_GROUP(txt, relPreCombatEnemy[iR])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPreCombatEnemy[iR],RELGROUPHASH_PLAYER)		
		REPEAT COUNT_OF(relPreCombatEnemy) iS
			IF iS < iR
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,relPreCombatEnemy[iR],relPreCombatEnemy[iS])
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,relPreCombatEnemy[iS],relPreCombatEnemy[iR])
			ENDIF
		ENDREPEAT
	ENDREPEAT

//	SET_PLAYER_NOISE_MULTIPLIER(player_id(),0.7)
//	SET_PLAYER_SNEAKING_NOISE_MULTIPLIER(player_id(),0.1)

	//SET_PLAYER_STEALTH_PERCEPTION_MODIFIER(player_id(),0.1)
	
	RESET_REACT_ARRAY(pedReactionState,MyLocalPedStruct,convBlock,9,<<80.601524,3685.037842,38.712517>>,121)		
	
	vDefAreas[0] = <<52.652012,3635.905518,38.700470>> 
	vDefAreas[1] = <<37.014786,3621.633057,39.000427>> 
	vDefAreas[2] = <<24.249683,3624.625000,39.115479>> 
	vDefAreas[3] = <<29.514772,3637.202393,38.772064>> 
	vDefAreas[4] = <<31.043964,3647.692627,38.755215>> 
	vDefAreas[5] = <<15.603708,3650.389404,42.120281>> 
	vDefAreas[6] = <<7.077183,3664.040039,41.797867>> 
	vDefAreas[7] = <<24.638447,3663.974365,42.407444>> 
	vDefAreas[8] = <<9.000495,3687.043945,39.331165>> 
	vDefAreas[9] = <<18.983698,3704.913330,38.769413>> 
	vDefAreas[10] = <<-3.218173,3715.225586,42.921997>> 
	vDefAreas[11] = <<21.879295,3721.244141,38.569950>> 
	vDefAreas[12] = <<13.994083,3734.201416,38.674572>> 
	vDefAreas[13] = <<35.858707,3733.545410,38.669376>> 
	vDefAreas[14] = <<24.373564,3744.352539,38.665215>> 
	vDefAreas[15] = <<42.977978,3745.603516,38.665215>> 
	vDefAreas[16] = <<57.979008,3743.661377,38.679287>> 
	vDefAreas[17] = <<70.274704,3762.172852,38.740314>> 
	vDefAreas[18] = <<92.825897,3743.277588,38.633743>> 
	vDefAreas[19] = <<106.780144,3737.334717,38.730511>> 
	vDefAreas[20] = <<114.095940,3724.494385,38.727863>> 
	vDefAreas[21] = <<121.971077,3712.555908,38.754875>> 
	vDefAreas[22] = <<114.718063,3701.483887,38.754875>> 
	vDefAreas[23] = <<108.037216,3686.295654,38.754875>> 
	vDefAreas[24] = <<91.711632,3679.015137,38.666607>> 
	vDefAreas[25] = <<96.877739,3656.804443,38.754875>> 
	vDefAreas[26] = <<95.055305,3641.014160,38.754875>> 
	vDefAreas[27] = <<74.325081,3609.529053,38.621632>> 
	vDefAreas[28] = <<95.682220,3609.175293,38.795425>> 
	vDefAreas[29] = <<69.808815,3646.932617,44.825924>> 
	vDefAreas[30] = <<62.494877,3670.041992,41.572426>> 
	vDefAreas[31] = <<51.632488,3680.543701,38.738880>> 
	vDefAreas[32] = <<62.593601,3684.663330,38.834270>> 
	vDefAreas[33] = <<49.683342,3694.642578,38.755215>> 
	vDefAreas[34] = <<34.955578,3704.213623,38.622513>> 
	vDefAreas[35] = <<45.199760,3718.186768,38.711952>> 
	vDefAreas[36] = <<62.713112,3726.785156,38.708153>> 
	vDefAreas[37] = <<71.145996,3699.509521,38.754875>> 
	vDefAreas[38] = <<66.005035,3707.480957,38.754875>> 
	vDefAreas[39] = <<75.551201,3717.824707,38.754875>> 
	vDefAreas[40] = <<72.457161,3734.936523,38.585670>> 
	vDefAreas[41] = <<88.490791,3723.996094,38.718147>> 
	vDefAreas[42] = <<89.733513,3707.400879,38.614914>> 
	vDefAreas[43] = <<78.294769,3685.643066,38.529331>> 
	
	setReactDialogue(diAlertAllSeen,"TRV3_ATTACK",3,"TREV3BIKER3")
	setReactDialogue(diAlertAllUnSeen,"TRV3_reac3",5,"TRV3BIKER2")
	setReactDialogue(diLostPlayer,"TRV3_giveup",5,"TRV3BIKER2")
	setReactDialogue(diSeePlayer,"TRV3_reac2",5,"TRV3BIKER2")
	setReactDialogue(diHearPlayer,"TRV3_reac4",5,"TRV3BIKER2")
	setReactDialogue(diHearPlayerAgain,"TRV3_reac5",5,"TRV3BIKER2")
	setReactDialogue(diBullet,"TRV3_reac1",5,"TRV3BIKER2")
	setReactDialogue(diDeadBody,"TRV3_reac3",5,"TRV3BIKER2")
	setReactDialogue(diExplosion,"TRV3_reac1",5,"TRV3BIKER2")
	setReactDialogue(diFoundPlayer,"TRV3_ATTACK2",4,"TREV3BIKER2")
	
	RESET_TRAILER_PARK_RAYFIRES(END_STATE_STAGE_SKIP)
	ACTION(0,ACT_TRAILER_RAYFIRES)
	
	
	
	IF (Is_Replay_In_Progress())
		
		int myStage = Get_Replay_Mid_Mission_Stage()
		IF g_bShitskipAccepted = TRUE
        	myStage++
        ENDIF
		
		switch myStage
			case 0					
				do_skip(STAGE_GET_TO_TRAILER_PARK,g_bShitskipAccepted,true)
				DO_SCREEN_FADE_IN(1000)
				return false
			break
			case 1
				IF g_bShitskipAccepted = TRUE
					do_skip(STAGE_TRAILER_PARK_CUT,g_bShitskipAccepted,false)
					
				ELSE					
					do_skip(STAGE_DETROY_TRAILER_PARK,g_bShitskipAccepted,true)
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				
				return false
			break
			case 2
				IF g_bShitskipAccepted
					do_skip(STAGE_GET_BACK_TO_WADE,g_bShitskipAccepted,FALSE)
				else
					do_skip(STAGE_GET_BACK_TO_WADE,g_bShitskipAccepted,true)
				endif
				
				DO_SCREEN_FADE_IN(1000)
				return false
			break
			case 3
				IF g_bShitskipAccepted
					do_skip(STAGE_DRIVE_TO_LS,g_bShitskipAccepted,FALSE)
				else
					do_skip(STAGE_DRIVE_TO_LS,g_bShitskipAccepted,true)
				endif
				DO_SCREEN_FADE_IN(1000)
				return false
			break
			case 4
				IF g_bShitskipAccepted = TRUE
					do_skip(STAGE_LS_VIEW_CUT,g_bShitskipAccepted,false)
				ELSE
					do_skip(STAGE_DRIVE_TO_WADES_COUSIN,g_bShitskipAccepted,true)
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				
				
				return false
			break	
			case 5
				IF g_bShitskipAccepted = TRUE
					do_skip(STAGE_FINAL_CUT,g_bShitskipAccepted,false)
				else
					do_skip(STAGE_FINAL_CUT,g_bShitskipAccepted,TRUE)
				endif
				
				return false
			break	
		ENDSWITCH
  		 // Your mission is being replayed             
 		SET_VEHICLE_MODEL_IS_SUPPRESSED(bodhi2, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(),RELGROUPHASH_PLAYER)
		SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 1981.1765, 3817.0603, 31.3805 >>  )
		SET_ENTITY_HEADING(PLAYER_PED_ID(),329.2307)	
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
		DO_SCREEN_FADE_IN(1000)
	elIF IS_REPEAT_PLAY_ACTIVE()
		do_skip(STAGE_INTRO_CUT,false,FALSE)
		
		return false
	ELSE
//		todCam=todCam
//		iTodTracker=iTodTracker
//		Trev3timeOfDay=Trev3timeOfDay
	//	WHILE NOT DO_TIMELAPSE(TODS_MISSION_TREVOR3,Trev3timeOfDay,iTodTracker,todCam)
	//		safewait()
	//	ENDWHILE
	ENDIF	
	//cprintln(debug_Trevor3,"init ended: ",get_game_timer())
	return true
ENDFUNC

FUNC BOOL do_intro_cut()
	
	checkConditions(COND_START_CUTSCENE,COND_END_CUTSCENE)



	action(0,ACT_LOAD_INTRO_CUT)
	action(1,ACT_PLAY_CUTSCENE)
	action(2,ACT_RESOLVE_VEHICLES_AT_START,PLAYOUT_ON_TRIGGER,cIF,COND_CUTSCENE_STARTED_PLAYING)
	action(3,ACT_EXTRACT_WADE_FROM_CUTSCENE,PLAYOUT_ON_TRIGGER,cIF,COND_CUTSCENE_STARTED_PLAYING)
	//action(4,ACT_MAKE_PLAYER_AND_BUDDY_WALK,PLAYOUT_ON_TRIGGER,cIF,COND_CUTSCENE_STARTED_PLAYING)
	
	HANDLE_EXITTING_PEDS_FROM_CUTSCENE()
	
	IF IS_CONDITION_TRUE(COND_CUTSCENE_FINISHED)
	AND IS_CONDITION_TRUE(COND_CUTSCENE_STARTED_PLAYING)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)					
		SET_PLAYER_CONTROL(player_id(),true)
		cprintln(debug_trevor3,"cut ended")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL do_trailer_park_drive()

	HANDLE_EXITTING_PEDS_FROM_CUTSCENE()

	checkConditions(COND_START_TRIALER_PARK_DRIVE,COND_END_TRIALER_PARK_DRIVE)
	
	
	INSTRUCTIONS(0,INS_GET_TO_TRAILER_PARK)
	
	dialogue(0,DIA_TRAILER_PARK_DRIVE_CHAT,cIF,COND_PLAYER_AND_WADE_IN_SAFE_CAR,cAND,COND_WADE_NEARBY)//,cANDNOT,COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE,cANDNOT,COND_WADE_WANTS_TO_COMMENT_ON_WANTED_LEVEL,cANDNOT,COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM)
	dialogue(1,DIA_WADE_COMMENTS_ON_PLAYER_LEAVING,cIF,COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM)
	dialogue(2,DIA_WADE_COMMENTS_ON_COPS,cIF,COND_WADE_WANTS_TO_COMMENT_ON_WANTED_LEVEL)
	dialogue(3,DIA_WADE_COMMENTS_ON_CAR_DAMAGE,cIF,COND_WADE_WANTS_TO_COMMENT_ON_VEHICLE_DAMAGE)
	dialogue(4,DIA_ARRIVE_AT_TRAILER_PARK,cIF,COND_ARRIVE_AT_TRAILER_PARK,cAND,COND_WADE_IN_PLAYER_GROUP)	
	DIALOGUE_ON_DIALOGUE(5,DIA_FIRST_CAR_COMMENT,6,DIA_INTRO_LINE)
	dialogue(6,DIA_INTRO_LINE)
	dialogue(7,DIA_BIGGER_CAR)
	
	action(0,ACT_WADE_GETS_IN_BODHI) //commented out due to bug 993936 //replacing for bug 1051032
	action(1,ACT_MAKE_WADE_AND_PLAYER_ENTER_SPECIFIC_SEAT)
	action(2,ACT_MAKE_IT_RAIN)
	action(3,ACT_PREP_TRAILER_PARK,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_WITHIN_500m_OF_trailer_park,cANDNOT,COND_TRAILER_PARK_ASSETS_LOADED)
	action(4,ACT_RELEASE_TRAILER_PARK,PLAYOUT_ON_TRIGGER,cIFNOT,COND_PLAYER_WITHIN_500m_OF_trailer_park,cAND,COND_TRAILER_PARK_ASSETS_LOADED)
	//action(5,ACT_MAKE_PLAYER_AND_BUDDY_WALK)
	action(6,ACT_MECHANIC,PLAYOUT_ON_TRIGGER,cIF,COND_TRAILER_PARK_ASSETS_LOADED)
	action(7,ACT_TRAILER,PLAYOUT_ON_TRIGGER,cIF,COND_TRAILER_PARK_ASSETS_LOADED)
	action(8,ACT_BIKERS_DRIVE_OFF,PLAYOUT_ON_TRIGGER,cIF,COND_TRAILER_PARK_ASSETS_LOADED)
	ACTION(9,ACT_BIKERS_SITTING_CHATTING,PLAYOUT_ON_TRIGGER,cIF,COND_TRAILER_PARK_ASSETS_LOADED)
	action(10,ACT_BIKER_FIGHT,PLAYOUT_ON_TRIGGER,cIF,COND_TRAILER_PARK_ASSETS_LOADED)
	action(11,ACT_COFFIN_CRY,PLAYOUT_ON_TRIGGER,cIF,COND_TRAILER_PARK_ASSETS_LOADED)
	action(12,ACT_RECORD_CAMERA,PLAYOUT_ON_TRIGGER)
	action(13,ACT_STOP_VEHICLE_DOOR_GETTING_KNOCKED_OFF)
	action(14,ACT_PREVENT_WADE_LEAVING_BURNING_VEHICLE)
	ACTION(15,ACT_TRAILER_RAYFIRES)
	ACTION(16,ACT_UPDATE_PLAYER_VEHICLE)
	ACTION(17,ACT_SET_PED_DENSITY_EVERY_FRAME,PLAYOUT_ON_COND,cIF,COND_PLAYER_WITHIN_300m_OF_trailer_park)
	ACTION(18,ACT_AUDIO_SCENE_GETTING_IN_CAR)
	ACTION(19,ACT_LIGHTNING_FLASH,PLAYOUT_ON_TRIGGER,cIF,COND_HIT_TRIGGER_ZONE)
	ACTION(20,ACT_FRUSTUM_SETUP_FOR_FRIST_CUT,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_WITHIN_300m_OF_trailer_park)
	ACTION_ON_ACTION(21,ACT_UPDATE_SRL_POSITION,20,ACT_FRUSTUM_SETUP_FOR_FRIST_CUT)
	ACTION(22,ACT_SLOW_DOWN_PLAYER_IN_CHOPPER)

	
	//FAIL(FAIL_LEAD_COPS_TO_TRAILER_PARK,cIF,COND_PLAYER_IN_TRIGGER_RANGE_OF_TRAILER_PARK,cAND,COND_WANTED)
	FAIL(FAIL_WADE_KILLED)
	FAIL(FAIL_LEFT_WADE_BEHIND)
	FAIL(FAIL_KILL_LOST)
	
	IF IS_CONDITION_TRUE(COND_TRAILER_PARK_ASSETS_LOADED)
	//AND IS_CONDITION_TRUE(COND_TREVOR_ARRIVAL_DIALOGUE_FINISHED)
	AND IS_CONDITION_TRUE(COND_WADE_IN_PLAYER_GROUP)
	AND IS_CONDITION_TRUE(COND_LIGHTNING_FLASH)
	AND IS_CONDITION_FALSE(COND_IN_TAXI_RIDE)
	AND IS_ACTION_COMPLETE(20,ACT_FRUSTUM_SETUP_FOR_FRIST_CUT)
		FORCE_LIGHTNING_FLASH()
		CLEAR_MISSION_LOCATE_STUFF(locates_data,true)
		IF DOES_BLIP_EXIST(mission_blip)
			remove_blip(mission_blip)
		endif
		IF IS_VEHICLE_SAFE(vehicle[veh_player].id)
			SET_ENTITY_HEALTH(vehicle[veh_player].id,1000)
			SET_VEHICLE_ENGINE_HEALTH(vehicle[veh_player].id,1000)
			IF NOT IS_PED_FUCKED(ped[ped_wade].id)
				IF IS_ENTITY_ON_FIRE(ped[ped_wade].id)
					STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(ped[ped_wade].id),5.0)													
				ENDIF
			ENDIF
		ENDIf
		STOP_AUDIO_SCENE("TREVOR_3_DRIVE_TO_TRAILER_PARK")
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL do_trailer_arrive_cutscene()
	checkConditions(COND_TRAILER_CUT_START,COND_TRAILER_CUT_END)
		
	action(0,ACT_CUTSCENE_CAM,PLAYOUT_ON_TRIGGER)
	action(1,ACT_DISABLE_PLAYER_CONTROL)
	action(2,ACT_CUTSCENE_LIGHTNING_FLASH)
	

	IF IS_ACTION_COMPLETE(0,ACT_CUTSCENE_CAM)
	//	restartRoot = ""
		restartLine = ""
		restartSubtitleLine = ""
		restartSubtitleRoot = ""
		bLastConvoWithoutSubtitles = FALSE
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(), TRV3_UNMARKED)
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL do_plant_sticky_bombs(bool &bShowCutsceneRet)
	checkConditions(COND_PLANT_BOMBS_START,COND_PLANT_BOMBS_END)
	
	INSTRUCTIONS(0,ins_destroy_trailers)
	INSTRUCTIONS(1,ins_detonate_bombs,cIF,COND_ALL_BOMBS_PLANTED)
	//instructions(2,ins_stealth_instructions,cIF,COND_ENEMIES_STILL_ALIVE,cANDNOT,COND_PLAYER_SPOTTED_BY_LOST,cANDNOT,COND_PLAYER_IN_A_VEHICLE)
	instructions(2,ins_get_back_to_lost,cIF,COND_PLAYER_LEAVING_AREA)
	instructions(3,ins_plant_sticky_bombs,cIFNOT,COND_PLAYER_IN_A_VEHICLE)
	instructions(4,ins_trigger_sticky_bombs,cIF,COND_ALL_BOMBS_PLANTED)

	
	action(0,ACT_SET_BOMB_PLANTING_STUFF)

	//action(1,ACT_LOST_ALERTED_RESPONSE,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_SPOTTED_BY_LOST)
	//action(2,ACT_PED_REACTS_TO_STICKY_BOMB,PLAYOUT_ON_TRIGGER,cIF,COND_LOST_SPOTTED_STICKY_BOMB)
	action(1,ACT_UNFIX_BIKES_FROM_SYNC_SCENES,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_SPOTTED_BY_LOST)

	action(2,ACT_UPDATE_TRAILER_GUY_BLIP)
	action(3,ACT_UPDATE_AI_BLIPS)
	//action(4,ACT_BEAT_UP_GUY_REACTS_TO_TREVOR,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_SPOTTED_BY_LOST,cOR,COND_BEAT_UP_GUY_SEES_TREVOR)
	ACTION(4,ACT_BEAT_UP_GUY_REACTS_TO_TREVOR,PLAYOUT_ON_TRIGGER,cIF,COND_BEAT_UP_GUY_REACT_CONDITIONS)
	action(5,ACT_FIND_ANIM_TO_TRIGGER)
	action(6,ACT_TRAILER_ANIM_PLAYS_OUT,PLAYOUT_ON_TRIGGER,cIFNOT,COND_PLAYER_SPOTTED_BY_LOST)
	action(7,ACT_BIKERS_DRIVE_OFF_ANIM_PLAYS_OUT,PLAYOUT_ON_COND,cIF,COND_BIKER_DRIVE_ANIM_FINISHED,cANDNOT,COND_PLAYER_SPOTTED_BY_LOST)
	action(8,ACT_BEAT_UP_ANIMATION_PLAYS_OUT,PLAYOUT_ON_TRIGGER,cIF,COND_BEAT_HIM_UP_DIALOGUE_STARTS,cANDNOT,COND_PLAYER_SPOTTED_BY_LOST,cANDNOT,COND_BEAT_UP_GUY_REACT_CONDITIONS)
	action(9,ACT_COFFIN_GUY_DROPS_BOTTLE)
	action(10,ACT_TRAILER_RAYFIRES)
	action(11,ACT_REMOVE_TRAILER_BLIPS)
	action(12,ACT_COFFIN_RESTART_ANIM)
//	action(12,ACT_ENSURE_ONE_PED_ENGAGING_PLAYER,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_SPOTTED_BY_LOST)
	action(13,ACT_REWARD_BOMB_PLANTING_UNOBSERVED,PLAYOUT_ON_TRIGGER,cIF,COND_ALL_BOMBS_PLANTED,cANDNOT,COND_PLAYER_SPOTTED_BY_LOST)
	action(14,ACT_WADE_RUNS_OUT_OF_TRAILER_PARK)
	//action(14,ACT_REWARD_BLOW_ALL_AT_ONCE,PLAYOUT_ON_TRIGGER,cIF,COND_A_TRAILER_HAS_BLOWN)
	action(15,ACT_STOP_VEHICLE_DOOR_GETTING_KNOCKED_OFF)
	action(16,ACT_CONTROL_MUSIC)
	action(17,ACT_REWARD_STICKY_BOMB_USAGE)
	action(18,ACT_WADE_LOOKS_AT_TREVOR_NEAR_CAR)
	action(19,ACT_DISABLE_STICKY_BOMB_TRIGGER,PLAYOUT_ON_TRIGGER,cIF,COND_ALL_WILL_BLOW_AT_ONCE)
	action(20,ACT_AUDIO_SCENE_PLANT_STICKY_BOMBS)
	action(21,ACT_LOAD_AUDIO_STREAM,PLAYOUT_ON_TRIGGER,cIF,COND_THREE_BOMBS_PLANTED)
	action(22,ACT_STOP_TRUCK_FROM_BURNING)
	action(23,ACT_FOUGHT_GUY_FELL_AND_NOW_WONT_REACT,PLAYOUT_ON_TRIGGER,cIF,COND_FOUGHT_GUY_FELL)		
	action(24,ACT_STOP_TRAILER_WOMAN_GETTING_CUT_OFF)
	action(25,ACT_DETACH_COFFIN)
	action(26,ACT_MECHANIC)
	action(27,ACT_AUDIO_FOR_EXPLOSIONS,PLAYOUT_ON_TRIGGER,cIF,COND_A_TRAILER_HAS_BLOWN)
	action(28,ACT_SET_LOST_ALERTED_STUFF,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_SPOTTED_BY_LOST)
	action(29,ACT_BIKER_REACTIONS)
	action(30,ACT_FLEEING_PEDS,PLAYOUT_ON_TRIGGER,cIF,COND_A_TRAILER_HAS_BLOWN)
	action(31,ACT_SEATED_BIKER_REACTIONS)
	action(32,ACT_STOP_BOMBS_ON_TRAILER_DOOR_BEING_REMOVED_AT_RANGE)
	
	dialogue(0,DIA_BEAT_UP_BKIER_SEES_TREVOR,cIFNOT,COND_A_PED_IS_REACTING,cAND,COND_BEAT_UP_GUY_SEES_TREVOR)
	dialogue(1,DIA_TRAILER_BLOWN_UNSEEN,cIFNOT,COND_PLAYER_SPOTTED_BY_LOST,cAND,COND_A_TRAILER_HAS_BLOWN)
	dialogue(3,DIA_FIXER_DIALOGUE,cIFnot,COND_PLAYER_SPOTTED_BY_LOST,cANDNOT,COND_A_PED_IS_REACTING)
	dialogue(4,DIA_TRAILER,cIFnot,COND_PLAYER_SPOTTED_BY_LOST,cANDNOT,COND_A_PED_IS_REACTING)
	dialogue(5,DIA_BIKER_DRIVE_OFF,cIFnot,COND_PLAYER_SPOTTED_BY_LOST,cANDNOT,COND_A_PED_IS_REACTING)
	dialogue(6,DIA_BIKERS_CHAT,cIFnot,COND_PLAYER_SPOTTED_BY_LOST,cANDNOT,COND_A_PED_IS_REACTING)
	dialogue(7,DIA_BEAT_UP_GUY,cIFnot,COND_PLAYER_SPOTTED_BY_LOST,cANDNOT,COND_A_PED_IS_REACTING)
	dialogue(8,DIA_COFFIN_GUY,cIFnot,COND_PLAYER_SPOTTED_BY_LOST,cANDNOT,COND_A_PED_IS_REACTING)
	dialogue(9,DIA_TREVOR_SAYS_STAY_HERE)
	dialogue(10,DIA_COMBAT_BANTER,cIF,COND_PLAYER_SPOTTED_BY_LOST,cAND,COND_ENEMIES_STILL_ALIVE)
	dialogue(11,DIA_PLANTING_BOMBS,cIF,COND_BOMB_JUST_PLANTED,cANDNOT,COND_PLAYER_SPOTTED_BY_LOST)
	//dialogue_on_dialogue(12,DIA_TREVOR_GIVEN_GUNS,9,DIA_TREVOR_SAYS_STAY_HERE)
	dialogue(12,DIA_BACK_AT_WADE_COMBAT,cIF,COND_RETURNED_TO_WADE,cAND,COND_PLAYER_SPOTTED_BY_LOST)
	dialogue(13,DIA_TRAILER_BLOWN)
	dialogue(14,DIA_BACK_AT_WADE_NO_COMBAT,cIF,COND_RETURNED_TO_WADE,cANDNOT,COND_PLAYER_SPOTTED_BY_LOST,cORbracket,COND_RETURNED_TO_WADE,cANDNOT,COND_ENEMIES_STILL_ALIVE)
	dialogue(15,DIA_BACK_AT_WADE_COMBAT_IN_CAR,cIF,COND_RETURNED_TO_WADE,cAND,COND_PLAYER_IN_A_VEHICLE,cAND,COND_PLAYER_SPOTTED_BY_LOST,cAND,COND_ENEMIES_STILL_ALIVE)
	dialogue(17,DIA_BACK_AT_WADE_IN_CAR_NO_COMBAT,cIF,COND_RETURNED_TO_WADE,cAND,COND_PLAYER_IN_A_VEHICLE,cANDNOT,COND_PLAYER_SPOTTED_BY_LOST)
	dialogue(16,DIA_PLAYER_GETS_STEALTH_KILL)
	
	
	FAIL(FAIL_WADE_KILLED)
	FAIL(FAIL_NO_MORE_STICKY_BOMBS,cIFNOT,COND_ALL_BOMBS_PLANTED,cAND,COND_THREE_SECONDS_SINCE_LAST_BOMB)
	FAIL(FAIL_NOT_ENOUGH_STICKY_BOMBS)
	FAIL(FAIL_TRAILER_PARK_ABANDONED,cIF,COND_PLAYER_LEFT_AREA)
	
	IF (IS_CONDITION_TRUE(COND_ALL_WILL_BLOW_AT_ONCE)
	AND IS_CONDITION_TRUE(COND_TRIGGER_BUTTON_PRESSED))
	#if IS_DEBUG_BUILD OR bBlowUpTrailers #endif
		START_AUDIO_SCENE("TREVOR_3_ESCAPE_TO_CAR")
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		REMOVE_FORCED_OBJECT(<<32.3311, 3667.6604, 40.4431>>,3.0,prop_cs4_05_tdoor)
		REMOVE_FORCED_OBJECT(<<29.10, 3661.49, 40.85>>,3.0,prop_magenta_door)
		bShowCutsceneRet = TRUE
		RETURN TRUE
	ELSE	
		IF IS_CONDITION_TRUE(COND_ALL_TRAILERS_DESTROYED)	
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
			REMOVE_FORCED_OBJECT(<<32.3311, 3667.6604, 40.4431>>,3.0,prop_cs4_05_tdoor)
			REMOVE_FORCED_OBJECT(<<29.10, 3661.49, 40.85>>,3.0,prop_magenta_door)
			CLEAR_PRINTS()
			START_AUDIO_SCENE("TREVOR_3_ESCAPE_TO_CAR")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL do_explosion_cut()
	
	action(0,ACT_TRAILER_RAYFIRES)
	action(1,ACT_EXPLOSION_CAMS)
	action(2,ACT_TIMED_EXPLOSIONS)
	//action(3,ACT_LOST_ALERTED_RESPONSE)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF IS_ACTION_COMPLETE(1,ACT_EXPLOSION_CAMS)
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_3_RAYFIRE")
			STOP_AUDIO_SCENE("TREVOR_3_RAYFIRE")
		ENDIF
		
		DISABLE_TAXI_HAILING(TRUE)
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TRV3_ALL_TRAILERS_AT_ONCE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL do_drive_to_los_santos()



	checkConditions(COND_DRIVE_TO_LS_START,COND_DRIVE_TO_LS_END)
	INSTRUCTIONS(0,INS_GET_TO_LOS_SANTOS)
	INSTRUCTIONS(1,INS_GO_TO_WADES_COUSIN,cIF,COND_LS_CUTSCENE_FINISHED)
	INSTRUCTIONS(2,INS_FOLLOW_WADE,cIF,COND_AT_WADES_COUSIN,cAND,COND_ESTABLISHING_SHOT_DONE)
	
	action(0,ACT_UPDATE_AI_BLIPS)
	action(1,ACT_TRAILER_RAYFIRES)
	action(2,ACT_CLEAR_WEATHER,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_LEFT_TRAILER_PARK)
	action(3,ACT_CLEAN_UP_TRAILER_PARK,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_300M_FROM_TRAILER_PARK)
	action(4,ACT_SET_VEHICLE_RADIO)
	action(5,ACT_IN_CAR_SHOVE_ANIM)
	action(6,ACT_LOAD_CUTSCENE)
	action(7,ACT_STOP_VEHICLE_PLAYER_EXIT,PLAYOUT_ON_TRIGGER,cIF,COND_CAN_STOP_PLAYER_VEHICLE,cANDNOT,COND_LS_CUTSCENE_FINISHED)
	ACTION_ON_INSTRUCTION(27,ACT_LS_VIEW_LEADIN,0,INS_GET_TO_LOS_SANTOS)
	ACTION_ON_ACTION(8,ACT_PLAY_LS_CUTSCENE,27,ACT_LS_VIEW_LEADIN)
	action(9,ACT_LOAD_WADE_COUSIN_CUTSCENE,PLAYOUT_ON_TRIGGER,cIF,COND_AT_WADES_COUSIN,cAND,COND_VEHICLE_STOPPED_OR_ON_FOOT)
	
	
	
	IF IS_ACTION_COMPLETE(8,ACT_PLAY_LS_CUTSCENE)
		action(10,ACT_STOP_VEHICLE,PLAYOUT_ON_TRIGGER,cIF,COND_AT_WADES_COUSIN)
		ACTION(14,ACT_ESTABLISHING_SHOT_AT_WADES,PLAYOUT_ON_TRIGGER,cIF,COND_AT_WADES_COUSIN,cAND,COND_VEHICLE_STOPPED_OR_ON_FOOT)
		ACTION(15,ACT_WADE_PLAYS_SHOUT_ANIM,PLAYOUT_ON_TRIGGER,cIF,COND_ESTABLISHING_SHOT_DONE)
		ACTION(19,ACT_BLOCK_PLAYER_FOR_LEAD_IN,PLAYOUT_ON_TRIGGER,cIF,COND_TREVOR_APPROACHES,cAND,COND_ESTABLISHING_SHOT_DONE)
		ACTION(23,ACT_PREP_FRUSTUM_AT_WADES_COUSIN)
	ENDIF
		
	action(11,ACT_STOP_VEHICLE_DOOR_GETTING_KNOCKED_OFF)
	action(12,ACT_PREVENT_WADE_LEAVING_BURNING_VEHICLE)
	ACTION(13,ACT_UPDATE_PLAYER_VEHICLE)	
	
	
	ACTION(16,ACT_WADE_LOOKS_AT_TREVOR_NEAR_CAR,PLAYOUT_ON_COND,cIF,COND_VEHICLE_STOPPED_OR_ON_FOOT,cANDNOT,COND_AT_WADES_COUSIN)
	ACTION(17,ACT_DELAY_ENEMY_SHOOTING)
	action(18,ACT_WADE_ENTERS_PLAYERS_GROUP,PLAYOUT_ON_TRIGGER,cIF,COND_BACK_WITH_WADE)
	
	ACTION(20,ACT_MUSIC_IN_VEHICLE_GOING_TO_LS,PLAYOUT_ON_TRIGGER,cIF,COND_BACK_WITH_WADE)
	ACTION(21,ACT_AUDIO_SCENE_DRIVE_TO_LS)
	ACTION(22,ACT_AUDIO_SCENE_DRIVE_TO_WADES_COUSIN,PLAYOUT_ON_TRIGGER,cIF,COND_LS_CUTSCENE_FINISHED)
	
//	ACTION(24,ACT_SLOW_DOWN_PLAYER,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_300M_FROM_CUTSCENE)
	ACTION(25,ACT_FLEEING_PEDS)
	action(26,ACT_AUDIO_FOR_EXPLOSIONS)
	action(28,ACT_MAKE_PLAYER_USE_DRIVER_DOOR)
	//27 taken above
	
	
	DIALOGUE(0,DIA_LETS_GO,cIF,COND_BACK_WITH_WADE)
	DIALOGUE_ON_DIALOGUE(1,DIA_DRIVE_TO_LS_BANTER,0,DIA_LETS_GO,cIF,COND_PLAYER_AND_WADE_IN_SAFE_CAR,cANDNOT,COND_PLAYER_300M_FROM_CUTSCENE)	//cAND,COND_BACK_WITH_WADE
	dialogue(3,DIA_WADE_COMMENTS_ON_PLAYER_LEAVING,cIF,COND_WADE_WANTS_TO_COMMENT_ON_PLAYER_LEAVING_HIM,cANDNOT,COND_CUTSCENE_PLAYING,cANDNOT,COND_PLAYER_50M_FROM_WADES_COUSIN)
	dialogue(2,DIA_WADE_COMMENTS_ON_COPS,cIF,COND_WADE_WANTS_TO_COMMENT_ON_WANTED_LEVEL,cANDNOT,COND_CUTSCENE_PLAYING,cANDNOT,COND_AT_WADES_COUSIN)
	dialogue(4,DIA_OVERLOOKING_LS,cIF,COND_LS_LEADIN_OK)
	dialogue(5,DIA_DRIVE_TO_WADES_BANTER,cIF,COND_LS_CUTSCENE_FINISHED,cAND,COND_PLAYER_AND_WADE_IN_SAFE_CAR,cAND,COND_WADE_NEARBY,cANDNOT,COND_AT_WADES_COUSIN)
	dialogue(6,DIA_WADE_AT_TOP_OF_STAIRS,cIF,COND_TREVOR_APPROACHES,cAND,COND_ESTABLISHING_SHOT_DONE)
	dialogue(7,DIA_WADE_GIVES_DIRECTIONS,cIF,COND_PLAYER_600M_FROM_WADES_COUSIN,cANDNOT,COND_WANTED)	
	dialogue(8,DIA_TREVOR_STOPS_BANTER,CIF,COND_PLAYER_300M_FROM_CUTSCENE,cOR,COND_LS_VIEW_TRIGGER)//,cAND,COND_NO_INTERRUPTIONS)
	dialogue(9,DIA_WADE_CLIMBS_STAIRS,cIF,COND_AT_WADES_COUSIN,cAND,COND_VEHICLE_STOPPED_OR_ON_FOOT,cAndNOT,COND_WANTED)	
	DIALOGUE_ON_ACTION(10,DIA_WADE_CALLS_DOWN_TO_TREVOR,14,ACT_ESTABLISHING_SHOT_AT_WADES,cIFNOT,COND_TREVOR_APPROACHES)	
	dialogue(11,DIA_COMBAT_BANTER,cIFNOT,COND_PLAYER_300M_FROM_TRAILER_PARK,cANDNOT,COND_BACK_WITH_WADE)			
	dialogue(12,DIA_TRAILER_BLOWN)
	
	IF IS_ACTION_COMPLETE(14,ACT_ESTABLISHING_SHOT_AT_WADES)
		//1943808 - Disable interior cam to prevent pop when cutting back from the establishing shot.
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
			DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		ENDIF
	
		IF HAS_DIALOGUE_FINISHED(6,DIA_WADE_AT_TOP_OF_STAIRS)
		OR IS_CONDITION_TRUE(COND_TREVOR_AT_TOP_OF_FLLOYD_STAIRS)
			IF DOES_BLIP_EXIST(mission_blip)
				remove_blip(mission_blip)
			ENDIF
			CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
			RETURN TRUE
		ENDIF
	ENDIF
	
	FAIL(FAIL_WADE_KILLED)
	FAIL(FAIL_LEFT_WADE_BEHIND)
	/*
	IF IS_CONDITION_TRUE(COND_AT_WADES_COUSIN)
	AND IS_ACTION_COMPLETE(10,ACT_STOP_VEHICLE)
		RETURN TRUE
	ENDIF
	*/
	RETURN FALSE
ENDFUNC



FUNC BOOL do_end_cutscene()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	action(0,ACT_DO_END_CUTSCENE)
	IF IS_ACTION_COMPLETE(0,ACT_DO_END_CUTSCENE)
		IF DOES_ENTITY_EXIST(ped[ped_wade].id)
			DELETE_PED(ped[ped_wade].id)
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL do_safehouse_cuts()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	ACTION(0,ACT_SAFEHOUSE_INTRO_SHOT)
	action(1,ACT_KEEP_INTERIOR_IN_MEMORY)
//	ACTION_ON_ACTION(1,ACT_BED_SHOT,0,ACT_SAFEHOUSE_INTRO_SHOT)
//	ACTION_ON_ACTION(2,ACT_ACTIVITIES_SHOT,1,ACT_BED_SHOT)
//	ACTION_ON_ACTION(3,ACT_WARDROBE_SHOT,2,ACT_ACTIVITIES_SHOT)
	ACTION(4,ACT_SKIP_CUTS)
	IF IS_ACTION_COMPLETE(0,ACT_SAFEHOUSE_INTRO_SHOT)
	OR IS_ACTION_COMPLETE(4,ACT_SKIP_CUTS)
		NEW_LOAD_SCENE_STOP()
		Mission_Passed()
	ENDIF
	RETURN FALSE
ENDFUNc

PROC advance_stage(enumMissionStage skiptoStage=stage_null)

	RESET_CONDITIONS()
	RESET_ACTIONS()
	RESET_INSTRUCTIONS()
	RESET_DIALOGUE()

	CLEAR_HELP()

	
	int iNextStage = ENUM_TO_INT(missionProgress) + 1

	IF skiptoStage = stage_null
		missionProgress = INT_TO_ENUM(enumMissionStage,iNextStage)
	ELSE
		missionProgress = skiptoStage
	ENDIF
	
	
	
ENDPROC

PROC missionFlow()
	SWITCH missionProgress
		CASE STAGE_STARTUP
			if do_initialise()
				advance_stage()
			endif
		BREAK
		CASE STAGE_INTRO_CUT
			if do_intro_cut()
				advance_stage()
			endif
		BREAK
		CASE STAGE_GET_TO_TRAILER_PARK
			IF do_trailer_park_drive()
				advance_stage()
			ENDIF
		BREAK
		CASE STAGE_TRAILER_PARK_CUT
			IF do_trailer_arrive_cutscene()
				advance_stage()
			ENDIF
		BREAK
		CASE STAGE_DETROY_TRAILER_PARK
			
			IF do_plant_sticky_bombs(bShowCutscene)
				IF bShowCutscene
					advance_stage()
				ELSE
					advance_stage(STAGE_DRIVE_TO_LS)
				ENDIF
			ENDIF
		BREAK
		CASE STAGE_DESTROYED_CUTSCENE
			IF do_explosion_cut()
				advance_stage(STAGE_DRIVE_TO_LS)
			ENDIF
		BREAK
		CASE STAGE_DRIVE_TO_LS
			IF do_drive_to_los_santos()
				advance_stage(STAGE_FINAL_CUT)
			ENDIF
		BREAK
		
		CASE STAGE_LS_VIEW_CUT
			IF do_drive_to_los_santos()
				advance_stage(STAGE_FINAL_CUT)
			ENDIF
		BREAK
		
		CASE STAGE_DRIVE_TO_WADES_COUSIN
			IF do_drive_to_los_santos()
				advance_stage(STAGE_FINAL_CUT)
			ENDIF
		BREAK
		
		CASE STAGE_FINAL_CUT
			IF do_end_cutscene()
				advance_stage()
			ENDIF
		BREAK
		
		CASE STAGE_SAFEHOUSE_SHOTS
			IF do_safehouse_cuts()
				advance_stage()
			ENDIF
		BREAK
		
		CASE STAGE_GAME_OVER
			Mission_Passed()
		BREAK
		
	ENDSWITCH
ENDPROC


PROC CONTROL_CONVERSATION_SUBTITLES()
					 
	If bLastConvoWithoutSubtitles
		IF IS_STRING_NULL_OR_EMPTY(restartSubtitleRoot)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_SCRIPTED_CONVERSATION_ONGOING()
					IF NOT IS_MESSAGE_BEING_DISPLAYED()															
						restartSubtitleLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						restartSubtitleRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						restartSubtitleRoot = ""
						
						if not IS_STRING_NULL_OR_EMPTY(restartSubtitleLine)
						AND NOT ARE_STRINGS_EQUAL(restartSubtitleLine,"NULL")
							//cprintln(DEBUG_TREVOR3,"B: ",restartSubtitleRoot," ",restartSubtitleLine)
							restartSubtitleRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							KILL_FACE_TO_FACE_CONVERSATION()
						ELSE
							bLastConvoWithoutSubtitles = FALSE
							restartSubtitleRoot = ""
							restartSubtitleLine = ""
						ENDIF														
					ENDIF
				ENDIF
			ELSE
				bLastConvoWithoutSubtitles = FALSE
				restartSubtitleRoot = ""
				restartSubtitleLine = ""
			ENDIF
		ELSE		
			IF IS_SCRIPTED_CONVERSATION_ONGOING()
				IF NOT IS_CONV_ROOT_PLAYING(restartSubtitleRoot)
					bLastConvoWithoutSubtitles = FALSE
					restartSubtitleRoot = ""
					restartSubtitleLine = ""
				ENDIF
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(restartSubtitleRoot) 
	
					IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(restartSubtitleRoot,restartSubtitleLine,convStore[0].speakerNo,convStore[0].pedIndex,convStore[0].speakerLabel,
						convStore[1].speakerNo,convStore[1].pedIndex,convStore[1].speakerLabel,
						convStore[2].speakerNo,convStore[2].pedIndex,convStore[2].speakerLabel,
						convStore[3].speakerNo,convStore[3].pedIndex,convStore[3].speakerLabel)
					
						restartSubtitleRoot = ""
						restartSubtitleLine = ""
						bLastConvoWithoutSubtitles = FALSE
					ENDIF
			
			ENDIF
		ENDIF
	ENDIF

ENDPROC

SCRIPT
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Mission_Flow_Mission_Force_Cleanup()
		
		MISSION_CLEANUP(END_STATE_FORCE_CLEANUP)
	ENDIF
	
	cprintln(debug_trigger,"KEV GO")

	
	WHILE TRUE	
		#if IS_DEBUG_BUILD
			debug()
		#endif
		
		CONTROL_CONVERSATION_SUBTITLES()
		CONTROL_MUSIC()
		missionFlow()
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("m_trevor3")
		
		SWITCH missionProgress
			CASE STAGE_DETROY_TRAILER_PARK
			FALLTHRU
			CASE STAGE_DESTROYED_CUTSCENE
				handlePedReactions(pedReactionState,vDefAreas,diHearPlayer,diHearPlayerAgain,diLostPlayer,diBullet,diSeePlayer,diFoundPlayer,diAlertAllSeen,diAlertAllUnSeen,diDeadBody,diExplosion)
			BREAK			
			CASE STAGE_DRIVE_TO_LS
				If NOT IS_ACTION_COMPLETE(3,ACT_CLEAN_UP_TRAILER_PARK)
					handlePedReactions(pedReactionState,vDefAreas,diHearPlayer,diHearPlayerAgain,diLostPlayer,diBullet,diSeePlayer,diFoundPlayer,diAlertAllSeen,diAlertAllUnSeen,diDeadBody,diExplosion)
				ENDIF
			BREAK
		ENDSWITCH
		
		
		IF bRemovedPlayerHelmet = TRUE
			IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
				bRemovedPlayerHelmet = FALSE
				SET_PED_HELMET_FLAG(player_ped_id(),PV_FLAG_DEFAULT_HELMET)
			ENDIF
		ENDIF
		
		#if is_Debug_build  pedReactionsDebug(pedReactionState,trev3_widget) #endif
		
		SET_REPLAY_STAGE()
		WAIT(0)	
	ENDWHILE

ENDSCRIPT
