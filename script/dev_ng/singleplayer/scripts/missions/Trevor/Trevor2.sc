// _________________________________________________________________________________________
// ___                                                                                   ___
// ___              Author: Adam Westwood & Rowan Cockcroft                Date: 19/10/10___
// _________________________________________________________________________________________
// ___                                                                                   ___
// ___               Trevor 2:  snipe/fly tutorial                                       ___
// _________________________________________________________________________________________

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// ************************************* INCLUDE FILES *************************************

/*

//╒═════════════════════════════════════════════════════════════════════════════╕

- Re-record - Vehicle 17

Created 2 variants for Ron getting shot at by the player/ Trevor. These are stored in: /MISS/TREVOR2/RON_BASIC_MOVES/

//depot/gta5/art/anim/export_mb/COMBAT@/COMBAT_REACTIONS@/

SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_TREVOR, WEAPONTYPE_SNIPERRIFLE, TRUE

Ped Population/Blocking Areas/Spawn Blocking Areas
Ped Population/Blocking Areas/Scenario Blocking Areas
Ped Population/Blocking Areas/Non-Creation Area
*/


USING "rage_builtins.sch"
USING "globals.sch"
USING "script_blips.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "commands_camera.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_streaming.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_path.sch"
USING "commands_audio.sch"
USING "flow_public_core_override.sch"
USING "locates_private.sch"
USING "script_player.sch"
USING "script_MISC.sch"
USING "script_debug.sch"
USING "script_maths.sch"
USING "cellphone_public.sch"
USING "player_ped_public.sch"
USING "commands_entity.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "mission_stat_public.sch"
USING "locates_public.sch"
USING "cutscene_public.sch"
USING "CompletionPercentage_public.sch"
USING "clearmissionarea.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "RC_helper_functions.sch" 
USING "taxi_functions.sch"
USING "shop_public.sch"
USING "commands_recording.sch"

//For the Text 
CONST_INT MAX_QUEUED_TEXT 10
CONST_INT MAX_QUEUED_HELP_AL 3
CONST_INT MAX_TEXT_HASHES 50
CONST_INT MAX_NUM_CARGO 5

//TIMEOFDAY sTimeOfDay
structTimelapse sTimelapse

structPedsForConversation sSpeech

USING "text_queue.sch"

//Debug things
#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//---------------------------------:CUTSCENE VARS:------------------------------------------
BOOL b_skipped_mocap
INT i_current_event
BOOL b_is_jumping_directly_to_stage
BOOL bAddedRelGroups = FALSE
BOOL bCanSwitchWeapons = FALSE

ENUM SECTION_STAGE
	SECTION_STAGE_SETUP = 0,
	SECTION_STAGE_RUNNING,
	SECTION_STAGE_CLEANUP,
	SECTION_STAGE_SKIP,
	SECTION_STAGE_JUMPING_TO,
	SECTION_DEBUG
ENDENUM

SECTION_STAGE e_section_stage 		= SECTION_STAGE_SETUP

ENUM MISSION_REQUIREMENT
	REQ_RON,
	REQ_RON_QUAD,
	REQ_RON_QUAD_START,
	REQ_RON_QUAD_SNIPE,
	REQ_VEHICLE_TEST,
	REQ_TREVOR_QUAD,
	REQ_RON_PLANE,
	REQ_RON_PLANE_WITH_PILOT,
	REQ_TREVOR_PLANE,
	REQ_TRAFFICKING_BOATS,
	REQ_BUGGY,
	REQ_HAULER_AND_TANKER,
	REQ_OPEN_GUN_SHOP,	
 	REQ_GUN_SHOP_IN_MEMORY
ENDENUM

ENUM MISSION_VEHICLE
	MV_RON_PLANE = 1,
	MV_TREVOR_PLANE = 2,
	MV_HAULER = 4,
	MV_TANKER = 5,
	MV_MAVERICK = 10,
	MV_RON_QUAD = 11,
	MV_TREVOR_QUAD = 12,
	MV_BIKER_VAN_SNIPING_SEQUENCE
ENDENUM

CONST_INT CHECKPOINT_START					1
CONST_INT CHECKPOINT_SNIPE          		2
CONST_INT CHECKPOINT_PLANE_WING      		3
CONST_INT CHECKPOINT_FLIGHT		            4
CONST_INT CHECKPOINT_FLIGHT_RACE			5
CONST_INT MAX_ENEMY_PROPS                   5
CONST_INT MAX_RUNWAY_PROPS                  13
CONST_INT MAX_CARGO                         6
CONST_INT MAX_PICKUPS                       6

// ********************************* VARIABLES AND CONSTANTS *******************************

//BLIP_INDEX
BLIP_INDEX blipDropOff

STREAMVOL_ID iStreamVol

//VEHICLE_INDEX
VEHICLE_INDEX MissionTrain
VEHICLE_INDEX vehBuggy

//SEQUENCE_INDEX

//CAMERA_INDEX
CAMERA_INDEX cutscene_cam

PTFX_ID PTFXfire[3]
INTERIOR_INSTANCE_INDEX interior_gun_shop
PED_INDEX pedBattleEnemies[10]

PICKUP_INDEX Pickup[MAX_PICKUPS]

STREAMVOL_ID SnipeVol

ENUM MISSION_FAIL_ENUM //different mission fail conditions
	GENERIC,
	BUDDY_DEAD,
	BUDDY_LEFT,
	BUDDY_LOST,
	BUDDY_ABADONED,
	BUDDY_SPOOKED,
	MISSION_ABADONED,
	VEHICLE_RONS_QUAD_DESTROYED,
	VEHICLE_RONS_QUAD_STUCK,
	VEHICLE_TREVORS_QUAD_DESTROYED,
	VEHICLE_TREVORS_QUAD_STUCK,
	VEHICLE_CUBAN_ABADONED,
	VEHICLE_CUBAN_UNDRIVABLE,
	VEHICLE_BOATS_UNDRIVABLE,
	VEHICLE_TRAFFICKING_DEAD,
	BANNED_FROM_SHOP,
	ENEMY_SPOOKED,
	TOO_LATE,
	VEHICLE_CUBAN_LANDING_GEAR_DMG,
	ENEMY_ALERTED,
	NO_WEAPON,
	MAX_MISSION_FAILS
ENDENUM
MISSION_FAIL_ENUM reason_for_fail = GENERIC

//
// NEW VARIABLES FOR SNIPING SECTION
//
//

HASH_ENUM TOWER_DOORS
	TOWER_DOOR_TOP							//0
ENDENUM

/// PURPOSE: Specifies ped behaviour states.
ENUM PED_STATES
	PED_STATE_IDLE							= 0,
	PED_STATE_COMBAT,
	PED_STATE_ALERTED,
	PED_STATE_UNALERTED,
	PED_STATE_INVESTIGATING_RON,
	PED_STATE_INVESTIGATING_DEAD_BODY,
	PED_STATE_DRIVE_TO_POSITION,
	PED_STATE_SNEAKING_INTRO,
	PED_STATE_SNEAKING,
	PED_STATE_PLEADING
ENDENUM

/// PURPOSE: Specifies ped postion and heading
STRUCT LOCATION_STRUCT		
	VECTOR 					vPosition				
	FLOAT 					fHeading
ENDSTRUCT

/// PURPOSE: Specifies details of gate props used to block player's exit from the graveyard area.
STRUCT DOOR_STRUCT
	TOWER_DOORS				eHashEnum					//Door hash enum
	VECTOR					vPosition					//Door position
	MODEL_NAMES				ModelName					//Door model
	BOOL					bClosed						//Specifies if door is closed (open ratio is 0.0)
ENDSTRUCT

/// PURPOSE: Specifies details of mission objects
STRUCT OBJECT_SNIPE_STRUCT
	OBJECT_INDEX			ObjectIndex					//Object index
	VECTOR					vPosition					//Object position
	VECTOR					vRotation					//Object rotation
	MODEL_NAMES				ModelName					//Object model
	BOOL					bBroken
	BLIP_INDEX				BlipIndex
ENDSTRUCT

/// PURPOSE: Specifies details of mission vehicles.
STRUCT VEH_SNIPE_STRUCT 
	VEHICLE_INDEX			VehicleIndex				//Vehicle index
	BLIP_INDEX				BlipIndex					//Vehicle blip
	VECTOR					vPosition					//Vehicle postion
	FLOAT					fHeading					//Vehicle heading
	MODEL_NAMES				ModelName					//Vehicle model
	INT 					iThisVehProgress
	INT						iThisVehTimer
	VECTOR					vOffset
	VECTOR					vTargetPosition
	FLOAT					fTargetHeading
	BOOL					bImmobilized
	BOOL					bAutoDriveActive
	BOOL					bLineOfSightToTarget
	INT						iRecordingNumber
ENDSTRUCT

/// PURPOSE: Specifies details of mission peds.
STRUCT PED_SNIPE_STRUCT
	PED_INDEX				PedIndex					//Ped Index
	BLIP_INDEX				BlipIndex					//Ped's Blip Index
	AI_BLIP_STRUCT			EnemyBlipData			//Enemy Blip Data
	VECTOR					vPosition					//Ped's start location
	FLOAT					fHeading					//Ped's start heading
	MODEL_NAMES				ModelName					//Ped's model
	INT						iConversationTime
	INT						iConversationTimer			//Ped's conversation timer
	INT						iConversationCounter
	INT						iThisPedProgress
	INT						iThisPedTimer						//Ped's timer
	INT 					actionFlag
	INT 					iLookAt
	BOOL					bForceBlipOn
	BOOL					bCanSeeTargetPed
	BOOL					bHasTask
	PED_STATES				eState
	FLOAT					fDistanceToTarget
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31		sDebugName
	#ENDIF
ENDSTRUCT

DOOR_STRUCT					TowerDoor

PED_SNIPE_STRUCT			psEnemies[8]
PED_SNIPE_STRUCT			psAlertedEnemies[4]
PED_SNIPE_STRUCT			psMaverickEnemies[2]

VEH_SNIPE_STRUCT			vsEnemyVans[1]
VEH_SNIPE_STRUCT			vsEnemyBikes[1]

PED_SNIPE_STRUCT psRon

OBJECT_SNIPE_STRUCT			osPropPhone
OBJECT_SNIPE_STRUCT			osPropStickyBomb

OBJECT_SNIPE_STRUCT			osPropGasTanks[2]
OBJECT_SNIPE_STRUCT			osPropWallLights[3]

VECTOR						vWaterTowerTopPosition				= << 1574.84, 3363.84, 49.43 >>

BOOL                        bCutsceneSkipped
BOOL						bPedBlipsHidden
BOOL                        bFirstSnipingEnemyAllowed
BOOL                        bSecondSnipingEnemyAllowed
BOOL 						bSecondSnipingEnemyInvestigating
BOOL						bFourthSnipingEnemyAnimating
BOOL 						bSixthSnipingEnemyAllowed
BOOL						bEighthSnipingEnemyAllowed
BOOL                        bFifthEnemySpawnedInTower
BOOL                        bBikersAlerted
BOOL                        bAlertedEnemiesSpawned
BOOL                        bHitGround = FALSE

BOOL 						bQuadPresent

BOOL						FailFlags[MAX_MISSION_FAILS]

FLOAT                       fMaverickPlaybackTime
FLOAT						fSecondEnemyPlaybackTime
FLOAT 						fEighthEnemyPlaybackTime
TWEAK_FLOAT	                fMaverickPilotHeadShotRadius		0.25

STRING						sVehicleRecordingsFile				= "RCSJC"

INT                         iBrokenTargetLightsCount
INT                         iBrokenLightsCount
INT                         iMaverickShotsCount
INT                         iMaverickAlertTimer
INT                         iMaverickReleaseTimer
INT	                        iEnemyMaverickProgress
INT 						iAlertedEnemiesProgress
INT                         iBikersAlertTimer
INT							iAlarmFarSoundID = GET_SOUND_ID()
INT							iAlarmCloseSoundID = GET_SOUND_ID()
INT 						iFirstSnipingEnemyProgress
INT							iSecondSnipingEnemyProgress
INT							iThirdSnipingEnemyProgress
INT							iFourthSnipingEnemyProgress
INT 						iFifthSnipingEnemyProgress
INT 						iSixthSnipingEnemyProgress
INT 						iSeventhSnipingEnemyProgress
INT							iEighthSnipingEnemyProgress
INT							iRonDeadFailTimer
INT							iStaggerRonShootoutLines

INT iTimeTillBlip = 5000
INT iTimeTillBlipTimer

INT iBrokenLightsRonCount 		
INT iBrokenTargetLightsRonCount 

INT 						iVanExitSceneID = -1

OBJECT_INDEX                LostSniperWeapon

BLIP_INDEX					DestinationBlipIndex

#IF IS_DEBUG_BUILD
	BOOL	bDrawDebugStates
#ENDIF

//
// END NEW VARIABLES FOR SNIPING SECTION
//

ENUM MISSION_PICKUP
	MPICK_BODY_ARMOUR,
	MPICK_SHOTGUN,
	MPICK_HEALTHPACK,
	MPICK_HEALTHPACK2,
	MPICK_GRENADE,
	MPICK_AK
ENDENUM

STRUCT PLANE_CARGO
	OBJECT_INDEX cargo
	OBJECT_INDEX cargoPara
	INT iCargoTimer
	BOOL bCargoHasLanded
ENDSTRUCT

PLANE_CARGO s_cargo[MAX_CARGO]

//OBJECT_INDEX
OBJECT_INDEX crate[15]
OBJECT_INDEX objDead
OBJECT_INDEX oFlareProp
OBJECT_INDEX bikerProp[MAX_ENEMY_PROPS]
OBJECT_INDEX objGib

PTFX_ID PTFXsmugglersFlare

//BOOL 
BOOL bRunFailChecks
BOOL bCleanup 
BOOL bMissionStageLoaded
BOOL bcutsceneplaying
BOOL bpeddieplane[4]
BOOL bobjectivetoggle[2]
BOOL bplaybackstarted[4]
BOOL bdistancewarning
BOOL bwinrace
BOOL bagro
BOOL bDialogue = FALSE
BOOL bBridge[8]
BOOL bGodText = FALSE
BOOL bRonTasks = FALSE
BOOL bMissionPassed = FALSE
BOOL bEnemyTasks[5]
BOOL bRetractLandingGear = FALSE
BOOL bGoToSpeech = FALSE
BOOL bCommentOnPlaneBeingDamage
//BOOl bShowHelp = FALSE
BOOL bTraffickingActivated = FALSE
BOOL bSkipped = FALSE
BOOL bFlightDialogue = FALSE
BOOL bDropCargo = FALSE
BOOL bShowHatchOpen = FALSE
BOOL bRaceOutCome = FALSE
BOOL bClearCutscenArea = FALSE
BOOL bCustomGPSActive = FALSE
BOOL bJumpedOffPlaneWing = FALSE
BOOL bCreatePickUps = FALSE

//BOOL bEnemiesBlipped = FALSE
#IF IS_DEBUG_BUILD
	BOOL bSkipping	
    INT ieMissionStageTemp
	CONST_INT MAX_SKIP_MENU_LENGTH 11	//number of stages in mission(length of menu)
	INT iReturnStage					//mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]//struct containing the debug menu 
	WIDGET_GROUP_ID wgThisMissionsWidget
	FLOAT fXWidget = 0.23
	FLOAT fYWidget = 0.47
#ENDIF			

//INT
INT iSetupProgress
INT iReplayStage
INT iProgress
INT ienemyAIprog
INT icutsceneprog
INT iCount
INT itimer
INT irandomexptimelimit
INT iexptimer
INT irepeater
INT idriverrepeater
INT iPreStreamStage
INT iagrotimer
INT iRonAI
INT iFlightHelp 
INT iFlightTimer
INT iShootOut
INT iRunWayDialogue
INT iNukeDelay
INT iNuke
INT iRunWayTimer
INT iStagetimer
INT sceneIDPlayerJumpOnToWing
INT sceneIdBikerActions
INT iAmbientDialogueTimer
INT DandG
INT iDangGTimer
INT iRonTimer
INT iFallOfftimer
INT sceneIDStumble = -1
INT sceneIDJumpOnToWing = - 1
INT sceneIDHangingOnWing = -1
INT iDropOffHelpTimer
INT iTimeInCombat
INT iTowerReminderDialogueTimer

FLOAT fBike1CutsceneSkipStartTime = 10000
FLOAT fBike2CutsceneSkipStartTime = 11000
FLOAT fBike3CutsceneSkipStartTime = 9000
FLOAT fBike4CutsceneSkipStartTime = 10000


INT iRonDriveToTasks

//FLOAT
FLOAT fstartheading

FLOAT fRonPlayback = 1.0
FLOAT fWingPlayback

//VECTOR
VECTOR vStageStart  

//GUY HANGING ON
//	FLOAT fRotationAccel_average
//	FLOAT fRotationAccel_total
FLOAT fRotation, fRotationPrev
FLOAT fRotationAccel
//	FLOAT fForwardSpeed
//	FLOAT fFowardAccel 
//	FLOAT fForwardSpeedPrev
FLOAT ssPlayback = 1.45
FLOAT heading_timer
//	INT headings_captured
INT iLeanStage

FLOAT xTrailerForce = 0
FLOAT yTrailerForce = 10.0
FLOAT zTrailerForce = -10

WEAPON_TYPE wtCutscene

ENUM BALANCE_STATE
	STATE_INTRO,
	STATE_BALANCE,
	STATE_FALL_LEFT,
	STATE_FALL_RIGHT,
	STATE_FALL_OFF
ENDENUM

BALANCE_STATE eBalanceState = STATE_INTRO
// MISSION_ENUMS stages that make up each section of the mission
ENUM MISSION_STAGE_ENUM
    MISSION_STAGE_SETUP,
	MISSION_STAGE_OPENING_CUTSCENE,
	MISSION_STAGE_DRIVE,
	MISSION_STAGE_SNIPE,
	MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE,
	MISSION_STAGE_STEAL_PLANE,
	MISSION_STAGE_PLANE_WING,
	MISSION_STAGE_FLIGHT,
	MISSION_STAGE_TO_AIRSTRIP,
	MISSION_STAGE_CLOSING_CUTSCENE,
	MISSION_STAGE_TEST_STAGE,
	MISSION_STAGE_PASSED,
	MISSION_STAGE_FAIL
ENDENUM
MISSION_STAGE_ENUM eMissionStage = MISSION_STAGE_SETUP

LOCATES_HEADER_DATA sLocatesData

/////////////   DEBUG STUFF ***************************************************************************************************
//debug menu

#IF IS_DEBUG_BUILD
	
	FUNC STRING GET_PED_STATE_NAME_FOR_DEBUG(PED_STATES eState)
	
		SWITCH eState
		
			CASE PED_STATE_IDLE
				RETURN "PED_STATE_IDLE"
			BREAK
			CASE PED_STATE_COMBAT
				RETURN "PED_STATE_COMBAT"
			BREAK
			CASE PED_STATE_ALERTED
				RETURN "PED_STATE_ALERTED"
			BREAK
			CASE PED_STATE_UNALERTED
				RETURN "PED_STATE_UNALERTED"
			BREAK
			CASE PED_STATE_INVESTIGATING_RON
				RETURN "PED_STATE_INVESTIGATING_RON"
			BREAK
			CASE PED_STATE_INVESTIGATING_DEAD_BODY
				RETURN "PED_STATE_INVESTIGATING_DEAD_BODY"
			BREAK
			CASE PED_STATE_DRIVE_TO_POSITION
				RETURN "PED_STATE_DRIVE_TO_POSITION"
			BREAK
			CASE PED_STATE_SNEAKING_INTRO
				RETURN "PED_STATE_SNEAKING_INTRO"
			BREAK
			CASE PED_STATE_SNEAKING
				RETURN "PED_STATE_SNEAKING"
			BREAK
			CASE PED_STATE_PLEADING
				RETURN "PED_STATE_PLEADING"
			BREAK
			
		ENDSWITCH
		
		RETURN "INVALID_PED_STATE"
	
	ENDFUNC

	// PRINT_PROGRESS ************************************************************************************************************
	INT iMissionStageDB
	INT iSetupProgressDB
	INT iProgressDB
	INT iSwapProgDB
	INT ienemyAIprogDB
	INT icutsceneprogDB
	INT iPreStreamStageDB
	
	//Prints thigs when they change value
	PROC PRINT_PROGRESS()

		IF iMissionStageDB <> ENUM_TO_INT(eMissionStage)
			iMissionStageDB = ENUM_TO_INT(eMissionStage)
			PRINTSTRING("MISSION_STAGE_ENUM = ")PRINTINT(iMissionStageDB)PRINTNL()
		ENDIF  
		IF iProgressDB <> iProgress
			iProgressDB = iProgress
			PRINTSTRING("iprogress = ")PRINTINT(iprogress)PRINTNL()
		ENDIF
		IF iSetupProgressDB <> iSetupProgress
			iSetupProgressDB = iSetupProgress
			PRINTSTRING("iSetupProgress = ")PRINTINT(iSetupProgress)PRINTNL()
		ENDIF
		IF iPreStreamStageDB <> iPreStreamStage
			iPreStreamStageDB = iPreStreamStage
			PRINTSTRING("iPreStreamStage = ")PRINTINT(iPreStreamStage)PRINTNL()
		ENDIF
		IF iSwapProgDB <> iSwapProg
			iSwapProgDB = iSwapProg
			PRINTSTRING("iSwapProg = ")PRINTINT(iSwapProg)PRINTNL()
		ENDIF
		IF icutsceneprogDB <> icutsceneprog
			icutsceneprogDB = icutsceneprog
			PRINTSTRING("icutsceneprog = ")PRINTINT(icutsceneprog)PRINTNL()
		ENDIF
		IF ienemyAIprogDB <> ienemyAIprog
			ienemyAIprogDB = ienemyAIprog
			PRINTSTRING("ienemyAIprog = ")PRINTINT(ienemyAIprog)PRINTNL()
		ENDIF
		
	ENDPROC

#ENDIF

/// PURPOSE:
///    Resets all mission flags and progress counters to their default values.


// ******************************* PROCEDURES AND FUNCTIONS ********************************

/// PURPOSE:
///    Resets all mission flags and progress counters to their default values.
PROC RESET_MISSION_FLAGS()

	//reset setup progress to 1, so that iSetupProgress = 0 only runs once, on initial mission load.
	
	bCutsceneSkipped					= FALSE
	bPedBlipsHidden						= FALSE
	
	iBikersAlertTimer					= 0
	iRonDeadFailTimer					= 0
	iMaverickAlertTimer					= 0
	iMaverickReleaseTimer				= 0
	iMaverickShotsCount					= 0
	
	iBrokenLightsCount					= 0
	iBrokenLightsRonCount				= 0
	iBrokenTargetLightsCount			= 0
	iBrokenTargetLightsRonCount			= 0
	
	iEnemyMaverickProgress				= 0
	iFirstSnipingEnemyProgress			= 0
	iSecondSnipingEnemyProgress			= 0
	iThirdSnipingEnemyProgress			= 0
	iFourthSnipingEnemyProgress			= 0
	iFifthSnipingEnemyProgress			= 0
	iSixthSnipingEnemyProgress			= 0
	iSeventhSnipingEnemyProgress		= 0
	iEighthSnipingEnemyProgress			= 0
	iAlertedEnemiesProgress				= 0
	
	fSecondEnemyPlaybackTime			= 0.0
	fEighthEnemyPlaybackTime			= 0.0
	fMaverickPlaybackTime				= 0.0
	
	bFirstSnipingEnemyAllowed			= FALSE
	bSecondSnipingEnemyAllowed			= FALSE
	bSixthSnipingEnemyAllowed			= FALSE
	bEighthSnipingEnemyAllowed			= FALSE
	
	bSecondSnipingEnemyInvestigating	= FALSE
	bFourthSnipingEnemyAnimating		= FALSE
	bFifthEnemySpawnedInTower			= FALSE
	
	bBikersAlerted						= FALSE
	bAlertedEnemiesSpawned				= FALSE
	
	iVanExitSceneID 					= -1
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission flags and progress counters are reset.")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets all fail flags to specified boolean value.
/// PARAMS:
///    bNewBool - Boolean value to set all flags to TRUE or FALSE.
PROC SET_FAIL_FLAGS(BOOL bValue)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(FailFlags) - 1 )
		FailFlags[i] = bValue
	ENDFOR

ENDPROC

//PURPOSE: resets the mission stage ints
PROC RESET_MISSION_STAGE_VARIABLES()
	
	iProgress			= 0
	iPreStreamStage		= 0	
	icutsceneprog 		= 0
	ienemyAIprog 		= 0
	iSetupProgress      = 0
	FOR iCount = 0 TO 3
		bpeddieplane[iCount] = FALSE
	ENDFOR
	FOR iCount = 0 TO 1
		bplaybackstarted[iCount] = FALSE
	ENDFOR
	#IF IS_DEBUG_BUILD
		iProgressDB = 0
		iPreStreamStageDB = 0
		icutsceneprogDB = 0
		ienemyAIprogDB = 0
		iMissionStageDB = 0
	#ENDIF	
ENDPROC
//PURPOSE: resets the mission stage ints if you skipp
PROC RESET_MISSION_STAGE_INTS_SKIP()
	RESET_MISSION_STAGE_VARIABLES()
	bmissionstageloaded = TRUE
	brunfailchecks = TRUE
	#IF IS_DEBUG_BUILD
		bskipping = FALSE
	#ENDIF
ENDPROC

PROC ADVANCE_MISSION_STAGE()

	eMissionStage = INT_TO_ENUM(MISSION_STAGE_ENUM, (ENUM_TO_INT(eMissionStage) + 1))
	RESET_MISSION_STAGE_VARIABLES()

ENDPROC

PROC MANAGE_AMBIENT_ANIMS()
	
	WEAPON_TYPE wtWEP
	WEAPON_TYPE wtWEPb
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtWEP)
		IF wtWEP = WEAPONTYPE_UNARMED
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(),TRUE)
		ELSE
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(),TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(psRon.pedIndex)
		GET_CURRENT_PED_WEAPON(psRon.pedIndex,wtWEPb)
		IF wtWEPb = WEAPONTYPE_UNARMED
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(psRon.pedIndex,TRUE)
		ELSE
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(psRon.pedIndex,TRUE)
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_ALL_ENEMY_BLIPS()

	FOR iCount = 0 TO (MAX_ENEMY_PED-1)
		IF DOES_BLIP_EXIST(enemy[iCount].blip)
			REMOVE_BLIP(enemy[iCount].blip)
		ENDIF
	ENDFOR
	FOR iCount = 0 TO (MAX_DRIVER_PED-1)
		IF DOES_BLIP_EXIST(driver[iCount].blip)
			REMOVE_BLIP(driver[iCount].blip)
		ENDIF
	ENDFOR

ENDPROC

PROC SETUP_BUDDY()
	REQUEST_MODEL(IG_NERVOUSRON)
	CREATE_NPC_PED_ON_FOOT(psRon.pedIndex,CHAR_RON,psRon.vPosition ,psRon.fHeading)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psRon.pedIndex, TRUE)
	GIVE_WEAPON_TO_PED(psRon.pedIndex, WEAPONTYPE_UNARMED , 2000, TRUE)
	SET_PED_CAN_BE_TARGETTED(psRon.pedIndex, FALSE)
	SET_PED_RELATIONSHIP_GROUP_HASH(psRon.pedIndex ,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
	SET_PED_AS_ENEMY(psRon.pedIndex , FALSE)
	SET_ENTITY_AS_MISSION_ENTITY(psRon.pedIndex)
	SET_PED_DEFAULT_COMPONENT_VARIATION(psRon.pedIndex)
	SET_PED_ACCURACY(psRon.pedIndex ,100)
	SET_PED_COMBAT_ABILITY(psRon.pedIndex ,CAL_PROFESSIONAL)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(psRon.pedIndex,FALSE)
	SET_ENTITY_HEALTH(psRon.pedIndex,300)
	SET_PED_SUFFERS_CRITICAL_HITS(psRon.pedIndex,TRUE)
	PRINTSTRING("buddy set up")
	PRINTNL()		
ENDPROC

PROC CLEAN_UP_AUDIO_SCENES()
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_DRIVE_ATV")
		STOP_AUDIO_SCENE("TREVOR_2_DRIVE_ATV")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_PROPERTY_INTRO_SCENE")
		STOP_AUDIO_SCENE("TREVOR_2_PROPERTY_INTRO_SCENE") 
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FOCUS_CAM_RON")
		STOP_AUDIO_SCENE("TREVOR_2_FOCUS_CAM_RON")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_TRAINS_VOLUME")
		STOP_AUDIO_SCENE("TREVOR_2_TRAINS_VOLUME")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SNIPE_MAIN")
		STOP_AUDIO_SCENE("TREVOR_2_SNIPE_MAIN")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SNIPER_SIGHTS")
		STOP_AUDIO_SCENE("TREVOR_2_SNIPER_SIGHTS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SNIPE_FOCUS")
		STOP_AUDIO_SCENE("TREVOR_2_SNIPE_FOCUS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_HELI_SILENT")
		STOP_AUDIO_SCENE("TREVOR_2_HELI_SILENT")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_HELI_APPROACH")
		STOP_AUDIO_SCENE("TREVOR_2_HELI_APPROACH")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_HELI_CRASH")
		STOP_AUDIO_SCENE("TREVOR_2_HELI_CRASH")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SHOOTOUT")
		STOP_AUDIO_SCENE("TREVOR_2_SHOOTOUT")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_GET_TO_PLANE")
		STOP_AUDIO_SCENE("TREVOR_2_GET_TO_PLANE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SHOOTOUT_ON_WING")
		STOP_AUDIO_SCENE("TREVOR_2_SHOOTOUT_ON_WING")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_RAYFIRE")
		STOP_AUDIO_SCENE("TREVOR_2_RAYFIRE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLIGHT_START")
		STOP_AUDIO_SCENE("TREVOR_2_FLIGHT_START")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_CARGO_DOORS")
		STOP_AUDIO_SCENE("TREVOR_2_CARGO_DOORS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_DROP_CARGO")
		STOP_AUDIO_SCENE("TREVOR_2_DROP_CARGO")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLY_BACK")
		STOP_AUDIO_SCENE("TREVOR_2_FLY_BACK")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_LAND_THE_PLANE")
		STOP_AUDIO_SCENE("TREVOR_2_LAND_THE_PLANE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FOCUS_CAM_RON")
		STOP_AUDIO_SCENE("TREVOR_2_FOCUS_CAM_RON")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FOCUS_CAM_RON")
		STOP_AUDIO_SCENE("TREVOR_2_FOCUS_CAM_RON")
	ENDIF
	

ENDPROC

PROC CLEANUP_PED(PED_SNIPE_STRUCT &ped, BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	IF ( bDelete = FALSE )
	
		IF DOES_ENTITY_EXIST(ped.PedIndex)
			
			IF ( bDeathOnly = TRUE )
		
				IF IS_PED_INJURED(ped.PedIndex)

					IF IS_ENTITY_DEAD(ped.PedIndex)
						REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
					ENDIF
					
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF

				ENDIF
				
			ELIF ( bDeathOnly = FALSE )

				IF NOT IS_PED_INJURED(ped.PedIndex)
				
					SET_PED_KEEP_TASK(ped.PedIndex, bKeepTask)
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF
					
				ENDIF

			ENDIF
		
		ENDIF
	
	ELIF ( bDelete = TRUE )

		IF DOES_ENTITY_EXIST(ped.PedIndex)

			IF IS_PED_INJURED(ped.PedIndex)
			OR NOT IS_PED_INJURED(ped.PedIndex)

				IF IS_ENTITY_DEAD(ped.PedIndex)
				OR NOT IS_ENTITY_DEAD(ped.PedIndex)
					REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
				ENDIF
				
				DELETE_PED(ped.PedIndex)
				
				IF DOES_BLIP_EXIST(ped.BlipIndex)
					REMOVE_BLIP(ped.BlipIndex)
				ENDIF
				
			ENDIF

		ENDIF
				
		ped.iThisPedTimer					= 0
		ped.iThisPedProgress 				= 0
		ped.iConversationTime				= 0
		ped.iConversationTimer				= 0
		ped.iConversationCounter			= 0
		ped.bHasTask						= FALSE
		ped.bForceBlipOn					= FALSE
		ped.bCanSeeTargetPed				= FALSE
		ped.eState							= PED_STATE_IDLE
		
		CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
		
	ENDIF

ENDPROC

PROC CLEANUP_PED_GROUP(PED_SNIPE_STRUCT &array[], BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		CLEANUP_PED(array[i], bDelete, bDeathOnly, bKeepTask)
	ENDFOR

ENDPROC

PROC CLEANUP_VEHICLE(VEH_SNIPE_STRUCT &vehicle, BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE)

	IF ( bDelete = FALSE )
	
		IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)
			
			IF ( bDeathOnly = TRUE )
			
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)	
				OR IS_ENTITY_DEAD(vehicle.VehicleIndex)	
					REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle.VehicleIndex)
				ENDIF
				
				IF DOES_BLIP_EXIST(vehicle.BlipIndex)
					REMOVE_BLIP(vehicle.BlipIndex)
				ENDIF
				
			ELIF ( bDeathOnly = FALSE )
			
				IF NOT IS_ENTITY_DEAD(vehicle.VehicleIndex)
					REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle.VehicleIndex)
				ENDIF
				
				IF DOES_BLIP_EXIST(vehicle.BlipIndex)
					REMOVE_BLIP(vehicle.BlipIndex)
				ENDIF
			
			ENDIF
		
		ENDIF
	
	ELIF ( bDelete = TRUE )

		IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)

			IF IS_ENTITY_A_MISSION_ENTITY(vehicle.VehicleIndex)
				IF IS_ENTITY_DEAD(vehicle.VehicleIndex)
				OR NOT IS_ENTITY_DEAD(vehicle.VehicleIndex)
					REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
					DELETE_VEHICLE(vehicle.VehicleIndex)
				ENDIF
			ENDIF
			
		ENDIF
		
		IF DOES_BLIP_EXIST(vehicle.BlipIndex)
			REMOVE_BLIP(vehicle.BlipIndex)
		ENDIF
		
		vehicle.iThisVehProgress 				= 0
		vehicle.bImmobilized			= FALSE
		vehicle.bAutoDriveActive		= FALSE
		vehicle.bLineOfSightToTarget	= FALSE
		
	ENDIF

ENDPROC

PROC CLEANUP_VEHICLE_GROUP(VEH_SNIPE_STRUCT &vsArray[], BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE)
	
	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(vsArray) - 1 )
		CLEANUP_VEHICLE(vsArray[i], bDelete, bDeathOnly)
	ENDFOR
	
ENDPROC

PROC CLEANUP_OBJECT(OBJECT_INDEX &ObjectIndex, BOOL bDelete = FALSE)

	IF DOES_ENTITY_EXIST(ObjectIndex)
		IF IS_ENTITY_ATTACHED(ObjectIndex)
			DETACH_ENTITY(ObjectIndex)
		ENDIF

		FREEZE_ENTITY_POSITION(ObjectIndex, FALSE)

		IF ( bDelete = TRUE )
			DELETE_OBJECT(ObjectIndex)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
		ENDIF
		
	ENDIF
	
ENDPROC

//PURPOSE: cleans up all resources used in the mission
PROC MISSION_CLEANUP(BOOL bfinal)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(),RBF_IMPACT_OBJECT)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(),RBF_VEHICLE_IMPACT)
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)
	RELEASE_AMBIENT_AUDIO_BANK()
	DISABLE_TAXI_HAILING(FALSE)
	NEW_LOAD_SCENE_STOP()
	SET_CUTSCENE_FADE_VALUES(FALSE,TRUE,FALSE,TRUE)
	SET_FLASH_LIGHT_FADE_DISTANCE(-1)
	SET_DEFAULT_GUNSHOP_WEAPON(WEAPONTYPE_INVALID)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_02_SS, FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BLAZER,FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BLAZER2,FALSE)

	//SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1000)
	
	IF STREAMVOL_IS_VALID(SnipeVol)
		STREAMVOL_DELETE(SnipeVol)
	ENDIF
	
	TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_MISSION_FAIL")
		IF TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
			PRINTSTRING("MUSIC FAIL")PRINTNL()
			SET_LABEL_AS_TRIGGERED("TRV2_MISSION_FAIL",TRUE)
		ENDIF
	ENDIF
	
	RESET_MISSION_FLAGS()
	
	CLEANUP_PED(psron, FALSE, FALSE)
	CLEANUP_PED_GROUP(psEnemies, FALSE, FALSE)
	CLEANUP_PED_GROUP(psAlertedEnemies, FALSE, FALSE)
	CLEANUP_PED_GROUP(psMaverickEnemies, FALSE, FALSE)
	
	CLEANUP_VEHICLE_GROUP(vsEnemyVans, FALSE, FALSE)
	CLEANUP_VEHICLE_GROUP(vsEnemyBikes, FALSE, FALSE)
	
	CLEANUP_OBJECT(osPropPhone.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osPropStickyBomb.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osPropGasTanks[0].ObjectIndex, FALSE)
//	CLEANUP_OBJECT(osPropGasTanks[1].ObjectIndex, FALSE)
	
	IF iAlarmFarSoundID != -1
		IF NOT HAS_SOUND_FINISHED(iAlarmFarSoundID)
			STOP_SOUND(iAlarmFarSoundID)
		ENDIF
	ENDIF
	
	IF iAlarmCloseSoundID != -1
		IF NOT HAS_SOUND_FINISHED(iAlarmCloseSoundID)
			STOP_SOUND(iAlarmCloseSoundID)
		ENDIF
	ENDIF
	
	IF DOES_SCENARIO_GROUP_EXIST("GRAPESEED_PLANES")
		SET_SCENARIO_GROUP_ENABLED("GRAPESEED_PLANES",TRUE)
	ENDIF
	
	IF DOES_SCENARIO_GROUP_EXIST("SANDY_PLANES")
		SET_SCENARIO_GROUP_ENABLED("SANDY_PLANES",TRUE)
	ENDIF
	
	IF STREAMVOL_IS_VALID(iStreamVol)
		STREAMVOL_DELETE(iStreamVol)
	ENDIF
	
	IF DOES_BLIP_EXIST(psRon.blipIndex)
		REMOVE_BLIP(psRon.blipIndex)
	ENDIF

	SET_FAIL_FLAGS(FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(A_M_M_HILLBILLY_01,FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(A_M_M_HILLBILLY_02,FALSE)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(),TRUE)
	ENDIF
	
	SET_RENDER_HD_ONLY(FALSE)
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_WAYPOINT_RECORDING("trv2QuadGun")
	REMOVE_WAYPOINT_RECORDING("trv2QuadGtoA")
	
	IF IS_CUTSCENE_ACTIVE()
	AND IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE
	ENDIF
	
	CLEAN_UP_AUDIO_SCENES()

	IF bCustomGPSActive
		SET_GPS_CUSTOM_ROUTE_RENDER(FALSE, 18, 30)
		CLEAR_GPS_CUSTOM_ROUTE()
		bCustomGPSActive = FALSE
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(250)
	
	INT i
	
	FOR i = 0 TO MAX_ENEMY_PED - 1
		CLEANUP_AI_PED_BLIP(enemy[i].EnemyBlipData)
	ENDFOR
	
	FOR i = 0 TO MAX_PICKUPS- 1
		IF DOES_PICKUP_EXIST(Pickup[i])
			REMOVE_PICKUP(Pickup[i])
		ENDIF
	ENDFOR
	
	IF IS_SCREEN_FADED_OUT()
		IF DOES_ENTITY_EXIST(vehBuggy)
			SAFE_DELETE_VEHICLE(vehBuggy)
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(vehBuggy)
			SAFE_RELEASE_VEHICLE(vehBuggy)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
		CLOSE_BOMB_BAY_DOORS(mission_veh[2].veh)
	ENDIF
	
	RESET_MISSION_STATS_ENTITY_WATCH()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForcedAim,FALSE)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),FALSE,-1)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFXsmugglersFlare)
		STOP_PARTICLE_FX_LOOPED(PTFXsmugglersFlare)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oFlareProp)									
		DELETE_OBJECT(oFlareProp)
	ENDIF
	
	REMOVE_PED_COMP_ITEM_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, FALSE)
	ENDIF
	
	CLEAR_MISSION_LOCATION_BLIP(sLocatesData)
	CLEANUP_LOADED_MODEL_ARRAY()
	CLEANUP_LOADED_CARREC_ARRAY("RCSJC")
	CLEAR_PED_NON_CREATION_AREA()
	iNuke = 0
	
	IF ARE_ALL_NAVMESH_REGIONS_LOADED()
		REMOVE_NAVMESH_REQUIRED_REGIONS()
	ENDIF
	
	IF DOES_ENTITY_EXIST(objDead)
		DELETE_OBJECT(objDead)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
			DETACH_ENTITY(PLAYER_PED_ID())
		ENDIF
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		SET_PED_DUCKING(PLAYER_PED_ID(),FALSE)
	ENDIF
	
	PRINTSTRING("Cleanup run")PRINTNL()

	IF NOT bFinal
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		WASH_DECALS_IN_RANGE(<< 1701.4246, 3303.7739, 40.1640 >>,1000,1)
		REMOVE_DECALS_IN_RANGE(<< 1701.4246, 3303.7739, 40.1640 >>,1000) 
		REMOVE_ALL_PICKUPS_OF_TYPE(PICKUP_WEAPON_ASSAULTRIFLE)
		CLEAR_AREA(<< 1701.4246, 3303.7739, 40.1640 >>,1000,TRUE,FALSE)
		CLEAR_AREA(<< 1995.7250, 3812.2937, 31.2603 >>,1000,TRUE,FALSE)
	ENDIF
		
	IF bMissionPassed = FALSE
		KILL_ANY_CONVERSATION()
	ENDIF
	
	REMOVE_ALL_ENEMY_BLIPS()
	
	DISABLE_CELLPHONE(FALSE)
	
	IF DOES_BLIP_EXIST(dest_blip)
		REMOVE_BLIP(dest_blip)
	ENDIF
	
	FOR iCount = 0 TO (MAX_ENEMY_PED-1)
		IF DOES_ENTITY_EXIST(enemy[iCount].ped)
			IF bfinal
				IF NOT IS_PED_INJURED(enemy[iCount].ped)
					SET_PED_KEEP_TASK(enemy[iCount].ped,TRUE)
				ENDIF
				enemy[iCount].actionFlag = 0
				enemy[iCount].bCharge = FALSE
				SET_PED_AS_NO_LONGER_NEEDED(enemy[iCount].ped)
			ELSE
				IF IS_SCREEN_FADED_OUT()
					enemy[iCount].actionFlag = 0
					enemy[iCount].bCharge = FALSE
					DELETE_PED(enemy[iCount].ped)
				ELSE
					IF NOT IS_PED_INJURED(enemy[iCount].ped)
						SET_PED_KEEP_TASK(enemy[iCount].ped,TRUE)
					ENDIF
					enemy[iCount].bCharge = FALSE
					enemy[iCount].actionFlag = 0
					SET_PED_AS_NO_LONGER_NEEDED(enemy[iCount].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iCount = 0 TO (MAX_DRIVER_PED-1)
		IF DOES_ENTITY_EXIST(driver[iCount].ped)
			IF bfinal
				IF NOT IS_PED_INJURED(driver[iCount].ped)
					SET_PED_KEEP_TASK(driver[iCount].ped,TRUE)
				ENDIF
				
				driver[iCount].bCharge = FALSE
				driver[iCount].actionFlag = 0
				SET_PED_AS_NO_LONGER_NEEDED(driver[iCount].ped)
			ELSE
				IF IS_SCREEN_FADED_OUT()
					driver[iCount].bCharge = FALSE
					driver[iCount].actionFlag = 0
					DELETE_PED(driver[iCount].ped)
				ELSE
					IF NOT IS_PED_INJURED(driver[iCount].ped)
						SET_PED_KEEP_TASK(driver[iCount].ped,TRUE)
					ENDIF
					driver[iCount].bCharge = FALSE
					driver[iCount].actionFlag = 0
					SET_PED_AS_NO_LONGER_NEEDED(enemy[iCount].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iCount = 0 TO (MAX_ENEMY_PROPS -1)
		IF IS_SCREEN_FADED_OUT()
			IF DOES_ENTITY_EXIST(bikerProp[iCount])
				DELETE_OBJECT(bikerProp[iCount])
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(bikerProp[iCount])
				SET_OBJECT_AS_NO_LONGER_NEEDED(bikerProp[iCount])
			ENDIF
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(objGib)
		DELETE_OBJECT(objGib)
	ENDIF
	
	FOR iCount = 0 TO (MAX_MISSION_VEH-1)
		IF DOES_ENTITY_EXIST(mission_veh[iCount].veh)
			IF bfinal
				SAFE_RELEASE_VEHICLE(mission_veh[iCount].veh)
			ELSE
				//IF IS_SCREEN_FADED_OUT()
					DELETE_VEHICLE(mission_veh[iCount].veh)
					//SAFE_DELETE_VEHICLE(mission_veh[iCount].veh)
				//ELSE
					//SAFE_RELEASE_VEHICLE(mission_veh[iCount].veh)
				//ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(psRon.pedIndex)
		IF DOES_BLIP_EXIST(psRon.blipIndex)
			REMOVE_BLIP(psRon.blipIndex)
		ENDIF
		IF bfinal
			SET_PED_AS_NO_LONGER_NEEDED(psRon.pedIndex)
		ELSE
			DELETE_PED(psRon.pedIndex)
		ENDIF
	ENDIF
	
	FOR iCount = 0 TO 14
		IF DOES_ENTITY_EXIST(crate[iCount])
			IF bfinal
				SET_OBJECT_AS_NO_LONGER_NEEDED(crate[iCount])
			ELSE
				DELETE_OBJECT(crate[iCount])
			ENDIF
		ENDIF
	ENDFOR
	
	
	//cleanup locates header data
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	
	//cleanup animation dictionaries
	REMOVE_ANIM_SET("move_ped_crouched")
	REMOVE_ANIM_SET("move_ped_crouched_strafing")
	
	REMOVE_ANIM_DICT("guard_reactions")
	REMOVE_ANIM_DICT("misstrevor2ig_2")
	REMOVE_ANIM_DICT("misstrevor2ig_3")
	REMOVE_ANIM_DICT("misstrevor2ig_4")
	REMOVE_ANIM_DICT("misstrevor2ig_4b")
	REMOVE_ANIM_DICT("misstrevor2ig_5")
	REMOVE_ANIM_DICT("misstrevor2ig_5b_p2")
	REMOVE_ANIM_DICT("misstrevor2ig_5c")
	REMOVE_ANIM_DICT("misstrevor2ig_6")
	REMOVE_ANIM_DICT("missbigscore2big_2")
	REMOVE_ANIM_DICT("misstrevor2ig_7")
	REMOVE_ANIM_DICT("misstrevor2mcs_3_b")
	REQUEST_ANIM_DICT("misstrevor2ron_basic_moves")
	REMOVE_ANIM_DICT("reaction@male_stand@big_variations@d")
	REMOVE_ANIM_DICT("missexile2reactions_to_gun_fire@standing@idle_a")

	RELEASE_SCRIPT_AUDIO_BANK()
	
	bagro = FALSE
	bCreatePickUps = FALSE
	
	CLEANUP_ALL_REQUEST_ARRAYS("RCSJC")
	CLEAN_OBJECTIVE_BLIP_DISPLAY()
	
	SET_WIDESCREEN_BORDERS(FALSE,0)
	RENDER_SCRIPT_CAMS(FALSE, FALSE) 
	DESTROY_ALL_CAMS()
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFXfire[0])
		STOP_PARTICLE_FX_LOOPED(PTFXfire[0])
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFXfire[1])
		STOP_PARTICLE_FX_LOOPED(PTFXfire[1])
	ENDIF
   
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	
	eBalanceState = STATE_INTRO
	iLeanStage = 0
	bHitGround = FALSE
	
	IF NOT bfinal
		IF IS_SCREEN_FADED_OUT()
			CLEAR_AREA(<< 1764.9365, 3270.3894, 40.3731 >>,1000,TRUE,TRUE)
		ENDIF
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
		RESET_MISSION_STAGE_VARIABLES()
		CLEAR_PRINTS()
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, FALSE)
		ENDIF

		SET_ALL_VEHICLE_GENERATORS_ACTIVE()
		enable_dispatch_services()
		SET_MAX_WANTED_LEVEL(6)
		TERMINATE_THIS_THREAD()
	ENDIF

ENDPROC

PROC STREAMING_FIX() 
	SET_VEHICLE_POPULATION_BUDGET(1)
	SET_PED_POPULATION_BUDGET(1)
ENDPROC

//PURPOSE: controls the sucessfull completion of the mission
PROC MISSION_PASSED()
	Mission_Flow_Mission_Passed()
	CONTROL_FADE_IN(500)
    MISSION_CLEANUP(TRUE)
	
ENDPROC

PROC DISTANCE_FAIL_CHECKS(VECTOR vfailcheckcoords, FLOAT warningdist, FLOAT faildist,STRING sdistwarning,STRING scurrentobjective,MISSION_FAIL_ENUM eFail, BOOL bDistanceDialogue, STRING sLine)
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) > faildist
		TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
		reason_for_fail = eFail
		eMissionStage = MISSION_STAGE_FAIL
	ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) > warningdist
		IF NOT bdistancewarning
			IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				CLEAR_PRINTS()
			ENDIF
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
				IF bDistanceDialogue
					IF NOT IS_PED_INJURED(psRon.PedIndex)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex,sLine,"NervousRon",SPEECH_PARAMS_FORCE_FRONTEND)
					ENDIF
				ENDIF
				PRINT_GOD_TEXT(sdistwarning)
				bdistancewarning = TRUE
			ENDIF
		ENDIF
	ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) < (warningdist - 100)
		IF bdistancewarning
			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			ENDIF
			PRINT_GOD_TEXT(scurrentobjective)
			bdistancewarning = FALSE
		ENDIF
	ENDIF

ENDPROC

PROC LEAVE_MISSION_CHECKS(VECTOR vfailcheckcoords, FLOAT warningdist, FLOAT faildist,STRING sdistwarning,STRING scurrentobjective,MISSION_FAIL_ENUM eFail)
		
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) > faildist
		reason_for_fail = eFail
		eMissionStage = MISSION_STAGE_FAIL
	ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) > warningdist
		IF NOT bdistancewarning
			PRINT_GOD_TEXT(sdistwarning)
			bdistancewarning = TRUE
		ENDIF
	ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) < (warningdist - 25)
		IF bdistancewarning
			PRINT_GOD_TEXT(scurrentobjective)
			bdistancewarning = FALSE
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_PLANE_PERMANENTLY_STUCK(VEHICLE_INDEX &veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, 5000)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_JAMMED, 20000)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, 40000)
			RETURN TRUE
		ENDIF 
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_VEHICLE_FLY(VEHICLE_INDEX veh)
	IF NOT IS_VEHICLE_DRIVEABLE(veh)
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_PLANE_PERMANENTLY_STUCK(veh)
			RETURN FALSE
		ELSE
			IF NOT ARE_PLANE_PROPELLERS_INTACT(veh)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL HAS_TIME_PASSED(INT iTimeAmount, INT iThisTimer)

	INT iCurrentTime
	INT iTimeDifference
	
	iCurrentTime = GET_GAME_TIMER()
	
	iTimeDifference = iCurrentTime - iTimeAmount
	
	IF iTimeDifference > iThisTimer
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

//PURPOSE: Tests for various fail conditions
PROC FAIL_CHECKS()

//		PRINTLN("@@@@@@@@ FailFlags[ENEMY_ALERTED] = ", FailFlags[ENEMY_ALERTED], 
//		" bBikersAlerted = ", bBikersAlerted,
//		" bAlertedEnemiesSpawned = ", bAlertedEnemiesSpawned,
//		" HAS_TIME_PASSED(2000, iRonDeadFailTimer): ", GET_GAME_TIMER() - iRonDeadFailTimer,
//		" HAS_TIME_PASSED(7500, iBikersAlertTimer): ", GET_GAME_TIMER() - iBikersAlertTimer,
//		" @@@@@@@@")
		IF eMissionStage = MISSION_STAGE_SNIPE
			IF NOT IS_CUTSCENE_PLAYING()
				IF ( FailFlags[BUDDY_DEAD] = TRUE )
					IF DOES_ENTITY_EXIST(psRon.PedIndex)
						IF IS_PED_INJURED(psRon.PedIndex)
						OR IS_ENTITY_DEAD(psRon.PedIndex)
							IF ( iRonDeadFailTimer = 0 )		
								iRonDeadFailTimer = GET_GAME_TIMER()
							ELSE
								IF HAS_TIME_PASSED(2000, iRonDeadFailTimer)
//									PRINTLN("@@@@@@@@ reason_for_fail = BUDDY_DEAD @@@@@@@@@")
									reason_for_fail = BUDDY_DEAD
									eMissionStage = MISSION_STAGE_FAIL
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// b* 2358117
//				IF ( FailFlags[ENEMY_ALERTED] = TRUE )
					IF ( bBikersAlerted = TRUE )
						IF ( bAlertedEnemiesSpawned = TRUE )
							IF HAS_TIME_PASSED(7500, iBikersAlertTimer)
//								PRINTLN("@@@@@@@@ reason_for_fail = ENEMY_ALERTED @@@@@@@@@")
								reason_for_fail = ENEMY_ALERTED
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ENDIF
					ENDIF
//				ENDIF
				
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(psRon.pedIndex)
			IF IS_PED_INJURED(psRon.pedIndex)
				TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
				reason_for_fail = BUDDY_DEAD
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("buddy dead fail")
				PRINTNL()
			ENDIF
		ENDIF
		
		IF eMissionStage = MISSION_STAGE_DRIVE
			IF iProgress < 5
				IF DOES_ENTITY_EXIST(mission_veh[12].veh)
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[12].veh)
						TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
						reason_for_fail = VEHICLE_TREVORS_QUAD_DESTROYED
						eMissionStage = MISSION_STAGE_FAIL
						PRINTSTRING("veh 12 fail")
						PRINTNL()
					ELSE
						IF IS_VEHICLE_PERMANENTLY_STUCK(mission_veh[12].veh)
							TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
							reason_for_fail = VEHICLE_TREVORS_QUAD_STUCK
							eMissionStage = MISSION_STAGE_FAIL
							PRINTSTRING("veh 12 STUCK")
							PRINTNL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			

			IF DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
				IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh) 
					TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
					reason_for_fail = VEHICLE_RONS_QUAD_DESTROYED
					eMissionStage = MISSION_STAGE_FAIL
					PRINTSTRING("RONS ATV DESTROYED")
					PRINTNL()
				ELSE
					IF IS_VEHICLE_TYRE_BURST(mission_veh[MV_RON_QUAD].veh,SC_WHEEL_CAR_FRONT_LEFT)
					OR IS_VEHICLE_TYRE_BURST(mission_veh[MV_RON_QUAD].veh,SC_WHEEL_CAR_FRONT_RIGHT)
					OR IS_VEHICLE_TYRE_BURST(mission_veh[MV_RON_QUAD].veh,SC_WHEEL_CAR_REAR_LEFT)
					OR IS_VEHICLE_TYRE_BURST(mission_veh[MV_RON_QUAD].veh,SC_WHEEL_CAR_REAR_RIGHT)
						TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
						reason_for_fail = VEHICLE_RONS_QUAD_DESTROYED
						eMissionStage = MISSION_STAGE_FAIL
						PRINTSTRING("RONS ATV TYRES BURST")
						PRINTNL()
					ELSE
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
							IF IS_VEHICLE_PERMANENTLY_STUCK(mission_veh[MV_RON_QUAD].veh)
								TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
								reason_for_fail = VEHICLE_RONS_QUAD_STUCK
								eMissionStage = MISSION_STAGE_FAIL
								PRINTSTRING("RONS ATV STUCK")
								PRINTNL()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF eMissionStage = MISSION_STAGE_SNIPE
			IF GET_CLOCK_HOURS() > 4
				TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
				reason_for_fail = TOO_LATE
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("TOO LATE")
				PRINTNL()
			ENDIF
			IF NOT HAS_LABEL_BEEN_TRIGGERED("QUAD NO LONGER NEEDED")
				IF DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh) 
						TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
						reason_for_fail = VEHICLE_RONS_QUAD_DESTROYED
						eMissionStage = MISSION_STAGE_FAIL
						PRINTSTRING("veh 11 fail")
						PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF eMissionStage < MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
			IF NOT IS_CUTSCENE_PLAYING()
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1778.176636,3334.225830,39.189270>>, <<1690.478149,3246.944336,58.621895>>, 125.000000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1719.216431,3253.996094,37.894600>>, <<1483.964355,3196.577637,58.164097>>, 125.000000)
					TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
					reason_for_fail = ENEMY_SPOOKED
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
		ENDIF	
		
		IF eMissionStage = MISSION_STAGE_FLIGHT
		OR eMissionStage = MISSION_STAGE_TO_AIRSTRIP
			IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_PLANE].veh)
				IF NOT CAN_VEHICLE_FLY(mission_veh[MV_TREVOR_PLANE].veh)
					IF NOT IS_PED_INJURED(enemy[27].ped)
						SET_ENTITY_COLLISION(enemy[27].ped,TRUE)
						PROCESS_ENTITY_ATTACHMENTS(enemy[27].ped)
						SET_HIGH_FALL_TASK(enemy[27].ped,2000,10000)
					ENDIF
					KILL_ANY_CONVERSATION()
					TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
					reason_for_fail = VEHICLE_CUBAN_UNDRIVABLE
					eMissionStage = MISSION_STAGE_FAIL
					PRINTSTRING("veh 2 fucked")
				ELSE
					IF NOT IS_PLANE_LANDING_GEAR_INTACT(mission_veh[MV_TREVOR_PLANE].veh)
						KILL_FACE_TO_FACE_CONVERSATION()
						TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
						reason_for_fail = VEHICLE_CUBAN_LANDING_GEAR_DMG
						eMissionStage = MISSION_STAGE_FAIL
						PRINTSTRING("veh 2 landing gear fucked")
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[1].veh)
				IF NOT CAN_VEHICLE_FLY(mission_veh[1].veh)
					IF NOT IS_PED_INJURED(enemy[27].ped)
						SET_ENTITY_COLLISION(enemy[27].ped,TRUE)
						PROCESS_ENTITY_ATTACHMENTS(enemy[27].ped)
						SET_HIGH_FALL_TASK(enemy[27].ped,2000,10000)
					ENDIF
					KILL_ANY_CONVERSATION()
					TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
					reason_for_fail = VEHICLE_CUBAN_UNDRIVABLE
					eMissionStage = MISSION_STAGE_FAIL
					PRINTSTRING("veh 1 fucked")
				ENDIF
			ENDIF
		ELSE
			IF eMissionStage != MISSION_STAGE_CLOSING_CUTSCENE
				IF DOES_ENTITY_EXIST(mission_veh[1].veh)
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						IF NOT IS_PED_INJURED(enemy[27].ped)
							SET_ENTITY_COLLISION(enemy[27].ped,TRUE)
							PROCESS_ENTITY_ATTACHMENTS(enemy[27].ped)
							SET_HIGH_FALL_TASK(enemy[27].ped,2000,10000)
						ENDIF
						KILL_FACE_TO_FACE_CONVERSATION()
						TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
						reason_for_fail = VEHICLE_CUBAN_UNDRIVABLE
						eMissionStage = MISSION_STAGE_FAIL
						PRINTSTRING("veh 1 fail")
						PRINTNL()
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[2].veh)
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						//IF NOT ARE_PLANE_CONTROL_PANELS_INTACT(mission_veh[2].veh)
							IF NOT IS_PED_INJURED(enemy[27].ped)
								SET_ENTITY_COLLISION(enemy[27].ped,TRUE)
								PROCESS_ENTITY_ATTACHMENTS(enemy[27].ped)
								SET_HIGH_FALL_TASK(enemy[27].ped,2000,10000)
							ENDIF
							KILL_FACE_TO_FACE_CONVERSATION()
							TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
							reason_for_fail = VEHICLE_CUBAN_UNDRIVABLE
							eMissionStage = MISSION_STAGE_FAIL
							PRINTSTRING("veh 2 fail")
							PRINTNL()
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF


		//FAIL FOR DESTROYING THE BOATS
		IF DOES_ENTITY_EXIST(mission_veh[19].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[19].veh)
				TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
				reason_for_fail = VEHICLE_BOATS_UNDRIVABLE
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("veh 19 fail")
				PRINTNL()
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(driver[19].ped)
			IF IS_PED_INJURED(driver[19].ped)
				TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
				reason_for_fail = VEHICLE_TRAFFICKING_DEAD
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("driver 19 fail")
				PRINTNL()
			ENDIF	
		ENDIF
		
		IF DOES_ENTITY_EXIST(driver[20].ped)
			IF IS_PED_INJURED(driver[20].ped)
				TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
				reason_for_fail = VEHICLE_TRAFFICKING_DEAD
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("driver 20 fail")
				PRINTNL()
			ENDIF	
		ENDIF
		
		IF DOES_ENTITY_EXIST(driver[21].ped)
			IF IS_PED_INJURED(driver[21].ped)
				TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
				reason_for_fail = VEHICLE_TRAFFICKING_DEAD
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("driver 21 fail")
				PRINTNL()
			ENDIF	
		ENDIF
		
		IF DOES_ENTITY_EXIST(mission_veh[20].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[20].veh)
				TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
				reason_for_fail = VEHICLE_BOATS_UNDRIVABLE
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("veh 20 fail")
				PRINTNL()
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(mission_veh[21].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[21].veh)
				TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
				reason_for_fail = VEHICLE_BOATS_UNDRIVABLE
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("veh 21 fail")
				PRINTNL()
			ENDIF
		ENDIF
		
		IF eMissionStage = MISSION_STAGE_DRIVE
			IF iProgress < 4
				IF IS_PLAYER_KICKING_OFF_IN_SHOP(GUN_SHOP_02_SS)
					TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
					reason_for_fail = BANNED_FROM_SHOP
					eMissionStage = MISSION_STAGE_FAIL
					PRINTSTRING("shop fail")
					PRINTNL()
				ENDIF
			ENDIF
		ENDIF
		
		IF eMissionStage > MISSION_STAGE_OPENING_CUTSCENE
		AND eMissionStage < MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1751.958496,3391.118652,35.988674>>, <<1516.483643,3299.361328,46.035328>>, 118.250000)
				IF GET_PLAYER_WANTED_LEVEL(player_id()) > 0
					TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
					reason_for_fail = ENEMY_SPOOKED
					eMissionStage = MISSION_STAGE_FAIL
					PRINTSTRING("cops alerted")
					PRINTNL()
				ENDIF
			ENDIF
		ENDIF
		
//		IF eMissionStage = MISSION_STAGE_DRIVE
//			IF DOES_ENTITY_EXIST(psRon.pedIndex)
//				IF NOT IS_PED_INJURED(psRon.pedIndex)
//					IF IS_PED_ACTIVE_IN_SCENARIO(psRon.pedIndex)
//						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psRon.pedIndex,PLAYER_PED_ID())
//						reason_for_fail = BUDDY_SPOOKED
//						eMissionStage = MISSION_STAGE_FAIL
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		//DISTANCE FAILS
		
		IF DOES_ENTITY_EXIST(psRon.pedIndex)
			IF NOT IS_PED_INJURED(psRon.pedIndex)
				IF eMissionStage = MISSION_STAGE_DRIVE
					IF iProgress > 3
						DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(psRon.pedIndex), 500, 750,"SJC_DISTWARN","",BUDDY_LEFT,FALSE,"")
					ENDIF
					//DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(psRon.pedIndex), 800, 900,"SJC_SNPP","SJC_SNPP",BUDDY_LEFT,FALSE,"")
				ELIF eMissionStage = MISSION_STAGE_SNIPE
					LEAVE_MISSION_CHECKS(<< 1575.0476, 3363.5938, 47.6350 >>,10,40,"SJC_RETRW","SJC_DISTWARN1",MISSION_ABADONED)
//					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 1575.0476, 3363.5938, 47.6350 >>) > 10
//						IF DOES_BLIP_EXIST(psRon.blipIndex)
//							MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,NULL,<< 1575.9034, 3362.8730, 47.6349 >>)
//							REMOVE_BLIP(psRon.blipIndex)
//							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
//							ENDIF
//						ENDIF
//					ELSE
//						IF NOT DOES_BLIP_EXIST(psRon.blipIndex)
//							IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_RETRW")
//								CLEAR_PRINTS()
//							ENDIF
//							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
//								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
//							ENDIF
//							CLEAN_OBJECTIVE_BLIP_DISPLAY()
//							psRon.blipIndex = CREATE_BLIP_FOR_ENTITY(psRon.pedIndex,FALSE)
//						ENDIF
//					ENDIF
				ELIF eMissionStage = MISSION_STAGE_STEAL_PLANE
					IF iProgress < 5 
					AND iProgress > 0 
						DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(psRon.pedIndex), 200, 250,"SJC_DISTWARN","",BUDDY_LEFT,FALSE,"")
					ENDIF
					IF iProgress = 5 
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(psRon.pedIndex), 150, 200,"SJC_LOSINGG","SJC_GETPLNG2",BUDDY_LEFT,FALSE,"")
							ELSE
								DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(psRon.pedIndex), 150, 200,"SJC_DISTWARN","SJC_ENEM2G",BUDDY_LEFT,FALSE,"")
							ENDIF
						ENDIF
					ENDIF
				ELIF eMissionStage = MISSION_STAGE_PLANE_WING
					IF iProgress > 5
						DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(psRon.pedIndex),300, 350,"SJC_DISTWARN","SJC_FLWPLNG",BUDDY_LOST,FALSE,"")
					ENDIF
				ELIF eMissionStage = MISSION_STAGE_FLIGHT
					IF iProgress = 4
						IF IS_VEHICLE_DRIVEABLE(mission_veh[19].veh)
							DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(mission_veh[19].veh), 1200, 2000,"","SJC_DROPG1",BUDDY_LEFT,TRUE,"TR2_DBAA")
						ENDIF
					ELIF iProgress > 4

					ELSE 
						DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(psRon.pedIndex),500, 700,"SJC_DISTWARN","SJC_FLWPLNG",BUDDY_LEFT,TRUE,"TR2_DBAA")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	//ENDIF
	
ENDPROC
//PURPOSE: controls the failure of the mission
PROC MISSION_FAILED()
	
	TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")

	STRING sFailString

	SWITCH reason_for_fail
	
		CASE GENERIC
			//FAIL_GENERIC
			CLEAR_PRINTS()
			sFailString = "SJC_FAIL"
		BREAK	
		
		CASE BUDDY_DEAD
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "SJC_F_RON_K"
		BREAK	
		
		CASE BUDDY_LEFT
			//left buddy
			CLEAR_PRINTS()
			sFailString = "SJC_F_RON_L"
		BREAK	
		
		CASE BUDDY_LOST
			//left buddy
			CLEAR_PRINTS()
			sFailString = "SJC_F_RON_LO"
		BREAK	
		
		CASE BUDDY_ABADONED
			//left buddy
			CLEAR_PRINTS()
			sFailString = "SJC_F_RON_A"
		BREAK
		
		CASE BUDDY_SPOOKED
			//left buddy
			CLEAR_PRINTS()
			sFailString = "SJC_F_RON_S"
		BREAK
		
		CASE VEHICLE_RONS_QUAD_DESTROYED
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_R_QUAD"
		BREAK
		
		CASE VEHICLE_RONS_QUAD_STUCK
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_R_QUAD_S"
		BREAK

		CASE VEHICLE_TREVORS_QUAD_DESTROYED
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_T_QUAD"
		BREAK
		
		CASE VEHICLE_TREVORS_QUAD_STUCK
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_T_QUAD_S"
		BREAK
	
		CASE VEHICLE_CUBAN_UNDRIVABLE
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_F_CARGO"
		BREAK
		
		CASE VEHICLE_CUBAN_ABADONED
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_F_CUBA"
		BREAK
		
		CASE VEHICLE_CUBAN_LANDING_GEAR_DMG
			//landing gear 
			CLEAR_PRINTS()
			sFailString = "SJC_F_CUBALA"
		BREAK

		CASE VEHICLE_BOATS_UNDRIVABLE
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_F_BOATS"
		BREAK
		
		CASE VEHICLE_TRAFFICKING_DEAD
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_F_T_DEAD"
		BREAK
		
		CASE BANNED_FROM_SHOP
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "SJC_FAILSHOP2"
		BREAK
		
		CASE ENEMY_ALERTED
			CLEAR_PRINTS()
			sFailString = "SJC_FAIL1"
		BREAK
		
		CASE ENEMY_SPOOKED
			CLEAR_PRINTS()
			sFailString = "SJC_SPOOK"
		BREAK
		
		CASE TOO_LATE
			CLEAR_PRINTS()
			sFailString = "SJC_F_TOOLATE"
		BREAK
		
		CASE NO_WEAPON
			CLEAR_PRINTS()
			sFailString = "SJC_FAILSHOP3"
		BREAK

		CASE MISSION_ABADONED
			CLEAR_PRINTS()
			sFailString = "SJC_F_A"
		BREAK

	ENDSWITCH

	MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailString)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE

	// If the mission was responsible for activating trafficking, we just failed, turn it off.
	IF bTraffickingActivated
//		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 	FALSE)
//		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND),	FALSE)
	ENDIF
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES, 0, TRUE)
	REMOVE_IPL("airfield") //Only here, not during death/arrest.
	MISSION_CLEANUP(TRUE) // must only take 1 frame and terminate the thread
ENDPROC


PROC SET_PLAYER_START_POSITION(MISSION_STAGE_ENUM eMissionStagePassed)
	IF (eMissionStagePassed = MISSION_STAGE_SETUP
	OR eMissionStagePassed = MISSION_STAGE_DRIVE)
		vStageStart = << 1987.7053, 3813.7341, 31.2125 >>
		fstartheading = 180.5414
	ELIF eMissionStagePassed = MISSION_STAGE_SNIPE
		vStageStart = << 1574.7804, 3363.0977, 47.6349 >>
		fstartheading = 245.3244
	ELIF eMissionStagePassed = MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
		vStageStart = << 1574.7804, 3363.0977, 47.6349 >>
		fstartheading = 245.3244
	ELIF eMissionStagePassed = MISSION_STAGE_STEAL_PLANE
		vStageStart = << 1583.0302, 3361.1277, 36.6399 >>
		fstartheading = 245.3244
	ELIF eMissionStagePassed = MISSION_STAGE_PLANE_WING
		vStageStart = << 1729.6842, 3316.8794, 40.2261 >>
		fstartheading = 332.4664 
	ELIF eMissionStagePassed = MISSION_STAGE_FLIGHT
		vStageStart = <<842.7066, 3217.6802, 37.4018>>
		fstartheading = 65.2580 
	ENDIF
	WARP_PLAYER(vStageStart, fstartheading)
	
ENDPROC

PROC manage_explosions()
	IF (iProgress > 13 AND  eMissionStage = MISSION_STAGE_STEAL_PLANE)
		IF (GET_GAME_TIMER()-iexptimer) > irandomexptimelimit
			int irand
			irand = GET_RANDOM_INT_IN_RANGE(0,3)
			IF irand = 0
				IF IS_VEHICLE_DRIVEABLE(mission_veh[5].veh)
					ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[5].veh),EXP_TAG_DIR_GAS_CANISTER,1.0,TRUE,FALSE,1.5)
				ELSE
					ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[5].veh,FALSE),EXP_TAG_PLANE,1.0,TRUE,FALSE,1.5)
				ENDIF	
			ELIF irand = 1
				IF IS_VEHICLE_DRIVEABLE(mission_veh[4].veh)
					ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[4].veh),EXP_TAG_PLANE,1.0,TRUE,FALSE,1.5)
				ELSE
					ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[4].veh,FALSE),EXP_TAG_PLANE,1.0,TRUE,FALSE,1.5)
				ENDIF	
			ELIF irand = 2
				IF DOES_ENTITY_EXIST(crate[1])
					ADD_EXPLOSION(GET_ENTITY_COORDS(crate[1]),EXP_TAG_PLANE,1.0,TRUE,FALSE,1.5)
				ENDIF
			ENDIF
			iexptimer = GET_GAME_TIMER()
			irandomexptimelimit =GET_RANDOM_INT_IN_RANGE(5000,9000)
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_AGRO(INT ifirstped,INT isecondped,INT iagrodelay)
	
	IF NOT bagro
		FOR iCount = ifirstped TO isecondped
			IF IS_PED_INJURED(enemy[iCount].ped)
				bagro = TRUE
				iagrotimer = GET_GAME_TIMER()
			ELSE
				IF IS_PED_SHOOTING(enemy[iCount].ped)
					bagro = TRUE
					iagrotimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		IF (GET_GAME_TIMER() - iagrotimer) > iagrodelay
			FOR iCount = ifirstped TO isecondped
				IF NOT IS_PED_INJURED(enemy[iCount].ped)
					IF GET_SCRIPT_TASK_STATUS(enemy[iCount].ped,SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)<> PERFORMING_TASK
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemy[iCount].ped,200)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF

ENDPROC

//PURPOSE:Mark enemies as no longer needed and remove blips upon death.
PROC MANAGE_ENEMIES()

	FOR irepeater = 0 TO (MAX_ENEMY_PED-1)
		IF irepeater <> 3
			IF DOES_ENTITY_EXIST(enemy[irepeater].ped)
				IF IS_PED_INJURED(enemy[irepeater].ped)
					IF DOES_BLIP_EXIST(enemy[irepeater].blip)
						REMOVE_BLIP(enemy[irepeater].blip)
					ENDIF		
					//SET_PED_AS_NO_LONGER_NEEDED(enemy[irepeater].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR irepeater = 0 to (MAX_DRIVER_PED - 1)
		IF DOES_ENTITY_EXIST(driver[irepeater].ped)
			IF IS_PED_INJURED(driver[irepeater].ped)
				IF DOES_BLIP_EXIST(driver[irepeater].blip)
					REMOVE_BLIP(driver[irepeater].blip)
				ENDIF		
				//SET_PED_AS_NO_LONGER_NEEDED(driver[irepeater].ped)
			ENDIF
		ENDIF
	ENDFOR
	
	SWITCH ienemyAIprog
	
		CASE 0 // idle
		
		BREAK
		
		CASE 1

			FOR irepeater = 13 to 20
				IF enemy[irepeater].bTriggerTask = FALSE
					IF NOT IS_PED_INJURED(enemy[irepeater].ped)
						IF GET_SCRIPT_TASK_STATUS(enemy[irepeater].ped,SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)<> PERFORMING_TASK
							SET_PED_COMBAT_MOVEMENT(enemy[irepeater].ped,CM_STATIONARY)
							TASK_COMBAT_HATED_TARGETS_IN_AREA(enemy[irepeater].ped,GET_ENTITY_COORDS(enemy[irepeater].ped), 100)
							IF NOT DOES_BLIP_EXIST(enemy[irepeater].blip)
								//enemy[irepeater].blip=CREATE_BLIP_FOR_PED(enemy[irepeater].ped,TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			FOR idriverrepeater = 0 to MAX_DRIVER_PED-1
				IF NOT IS_PED_INJURED(driver[idriverrepeater].ped)
//					IF enemy[irepeater].bTriggerTask = FALSE
//						IF NOT IS_PED_INJURED(enemy[irepeater].ped)
//							IF GET_SCRIPT_TASK_STATUS(enemy[irepeater].ped,SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)<> PERFORMING_TASK
//								SET_PED_COMBAT_MOVEMENT(enemy[irepeater].ped,CM_STATIONARY)
//								TASK_COMBAT_HATED_TARGETS_IN_AREA(enemy[irepeater].ped,GET_ENTITY_COORDS(enemy[irepeater].ped), 100)
//								IF NOT DOES_BLIP_EXIST(enemy[irepeater].blip)
//									//enemy[irepeater].blip=CREATE_BLIP_FOR_PED(enemy[irepeater].ped,TRUE)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
				ENDIF
			ENDFOR

		BREAK
		
		

	ENDSWITCH


ENDPROC

PROC MANAGE_PLAYER_WEAPONS()
	
	//TREVOR
	IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
		IF eMissionStage < MISSION_STAGE_STEAL_PLANE
		AND eMissionStage > MISSION_STAGE_DRIVE
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,40,FALSE,FALSE)
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
				SET_SEETHROUGH(FALSE)
			ELSE
				IF NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				ENDIF
				
				IF NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
	AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
	AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,120,FALSE,FALSE)
	ELSE
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
			ADD_AMMO_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,IMAX(120,GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)))
		ENDIF
	ENDIF

ENDPROC

PROC SETUP_PEDS_FOR_DIALOGUE()
	IF NOT IS_PED_INJURED(psRon.pedIndex)
		ADD_PED_FOR_DIALOGUE(sSpeech, 3, psRon.pedIndex, "NervousRon")
	ENDIF
	ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
ENDPROC

PROC INITIALISE_ARRAYS_FOR_AIRFIELD()

	//enemy peds
	psEnemies[0].vPosition 					= << 1695.79, 3293.90, 40.15 >>
	psEnemies[0].fHeading 					= 307.0567
	psEnemies[0].ModelName 					= G_M_Y_LOST_01
	#IF IS_DEBUG_BUILD
		psEnemies[0].sDebugName 			= "Enemy 0"
	#ENDIF
	
	psEnemies[1].vPosition 					= << 1695.79, 3293.90, 40.15 >>
	psEnemies[1].fHeading 					= 307.0567
	psEnemies[1].ModelName 					= G_M_Y_LOST_01
	#IF IS_DEBUG_BUILD
		psEnemies[1].sDebugName 			= "Enemy 1"
	#ENDIF
	
	psEnemies[2].vPosition 					= <<1700.5061, 3293.0640, 47.9264>>	//<< 1701.90, 3287.50, 47.23 >>
	psEnemies[2].fHeading 					= 110.6841							//119.5233
	psEnemies[2].ModelName 					= G_M_Y_LOST_01
	#IF IS_DEBUG_BUILD
		psEnemies[2].sDebugName 			= "Enemy 2"
	#ENDIF
	
	psEnemies[3].vPosition 					= << 1726.12, 3291.04, 40.15 >>
	psEnemies[3].fHeading 					= 97.0908
	psEnemies[3].ModelName 					= G_M_Y_LOST_01
	#IF IS_DEBUG_BUILD
		psEnemies[3].sDebugName 			= "Enemy 3"
	#ENDIF
	
	psEnemies[4].vPosition 					= <<1700.9025, 3292.3318, 47.9264>> //<< 1702.10, 3287.89, 48.02 >> 
	psEnemies[4].fHeading 					= 93.9332							//128.34
	psEnemies[4].ModelName 					= G_M_Y_LOST_01
	#IF IS_DEBUG_BUILD
		psEnemies[4].sDebugName 			= "Enemy 4"
	#ENDIF
	
	psEnemies[5].vPosition 					= << 1683.58, 3285.26, 40.95 >>
	psEnemies[5].fHeading 					= 82.84
	psEnemies[5].ModelName 					= G_M_Y_LOST_01
	#IF IS_DEBUG_BUILD
		psEnemies[5].sDebugName 			= "Enemy 5"
	#ENDIF
	
	psEnemies[6].vPosition 					= << 1685.60, 3286.90, 40.14 >>
	psEnemies[6].fHeading 					= 123.9396
	psEnemies[6].ModelName 					= G_M_Y_LOST_01
	#IF IS_DEBUG_BUILD
		psEnemies[6].sDebugName 			= "Enemy 6"
	#ENDIF
	
	psEnemies[7].vPosition 					= << 1683.33, 3285.52, 40.14 >>
	psEnemies[7].fHeading 					= 128.5609
	psEnemies[7].ModelName 					= G_M_Y_LOST_01
	#IF IS_DEBUG_BUILD
		psEnemies[7].sDebugName 			= "Enemy 7"
	#ENDIF
	
	psAlertedEnemies[0].vPosition			= <<1741.2635, 3334.5293, 40.1464>>
	psAlertedEnemies[0].fHeading			= 345.8902
	psAlertedEnemies[0].ModelName			= G_M_Y_LOST_01
	
	psAlertedEnemies[1].vPosition			= <<1716.5404, 3303.2026, 40.2142>>
	psAlertedEnemies[1].fHeading			= 129.2187
	psAlertedEnemies[1].ModelName			= G_M_Y_LOST_01
	
	psAlertedEnemies[2].vPosition			= <<1716.1570, 3293.4749, 40.1894>>
	psAlertedEnemies[2].fHeading			= 109.6988
	psAlertedEnemies[2].ModelName			= G_M_Y_LOST_01
	
	psAlertedEnemies[3].vPosition			= <<1685.3408, 3286.7395, 40.1467>>
	psAlertedEnemies[3].fHeading			= 127.0312
	psAlertedEnemies[3].ModelName			= G_M_Y_LOST_01
	
	psMaverickEnemies[0].ModelName 			= G_M_Y_LOST_01
	psMaverickEnemies[1].ModelName 			= G_M_Y_LOST_01
	
	//enemy vehicles
	vsEnemyVans[0].vPosition 				= << 1887.1278, 3236.9063, 44.2666 >>
	vsEnemyVans[0].fHeading					= 117.7584
	vsEnemyVans[0].ModelName				= GBURRITO
	vsEnemyVans[0].iRecordingNumber			= 31
	
	vsEnemyBikes[0].vPosition				= << 1700.1692, 3276.5105, 40.1465 >>
	vsEnemyBikes[0].fHeading				= 127.9391
	vsEnemyBikes[0].ModelName				= HEXER
	vsEnemyBikes[0].iRecordingNumber		= 39
	
	mission_veh[MV_MAVERICK].Model         = MAVERICK
	mission_veh[MV_MAVERICK].loc		   = << 1706.7020, 3322.2422, 40.1486 >>
	mission_veh[MV_MAVERICK].head		   = 311.7382
	mission_veh[MV_MAVERICK].iRecordingNumber			= 6
	
//	vsMaverick.vPosition					= << 1706.7020, 3322.2422, 40.1486 >>
//	vsMaverick.fHeading						= 311.7382
//	vsMaverick.ModelName					= MAVERICK
//	vsMaverick.iRecordingNumber				= 6

	//airfield props
	osPropGasTanks[0].vPosition				= << 1683.27, 3278.70, 39.93 >>
	osPropGasTanks[0].vRotation				= <<0.00, 0.00, 109.96>>
	osPropGasTanks[0].ModelName				= PROP_GAS_TANK_02A
	
//	osPropGasTanks[1].vPosition				= << 1677.74, 3228.8654, 39.5427 >>
//	osPropGasTanks[1].vRotation				= << 0.0, 0.0, 120.0 >>
//	osPropGasTanks[1].ModelName				= PROP_GAS_TANK_02A
	
	osPropWallLights[0].vPosition			= << 1700.91, 3295.05, 45.89 >>
	osPropWallLights[0].ModelName			= PROP_WALL_LIGHT_02A
	
	osPropWallLights[1].vPosition			= << 1697.82, 3293.07, 47.24 >>
	osPropWallLights[1].ModelName			= PROP_WALL_LIGHT_02A
	
	osPropWallLights[2].vPosition			= << 1699.36, 3289.41, 51.26 >>
	osPropWallLights[2].ModelName			= PROP_WALL_LIGHT_02A
	
	osPropPhone.vPosition					= << 1729.5226, 3299.3979, 40.2263 >>
	osPropPhone.vRotation					= << 0.0, 0.0, 0.0 >>
	osPropPhone.ModelName					= PROP_PHONE_ING
	
	osPropStickyBomb.vPosition				= << 1686.30, 3279.27, 40.10 >>
	osPropStickyBomb.vRotation				= << 0.0, 0.0, 0.0 >>
	osPropStickyBomb.ModelName				= GET_WEAPONTYPE_MODEL(WEAPONTYPE_STICKYBOMB)
	
	TowerDoor.eHashEnum						= TOWER_DOOR_TOP
	TowerDoor.vPosition						= << 1697.8550, 3292.4968, 49.0851 >>
	TowerDoor.ModelName						= PROP_CS4_05_TDOOR
	
ENDPROC


//PURPOSE: defines up the individual ped details
PROC INITALISE_ARRAYS() // defines up the individual ped details

	//buddies
	psRon.vPosition = 	 << 1738.3199, 3345.3433, 40.2626 >>  
	psRon.fHeading =  101.9829 
	psRon.ModelName = CS_NERVOUSRON
	
	//enemies
	enemy[0].loc = << 1695.7944, 3293.9023, 40.1491 >>
	enemy[0].head =  307.0567
	enemy[0].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[0].model =G_M_Y_LOST_01
					
	enemy[1].loc =<< 1726.52, 3291.54, 41.20 >> 
	enemy[1].head = 147.5838 
	enemy[1].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[1].model = G_M_Y_LOST_01

	enemy[2].loc =<< 1726.1248, 3291.0435, 40.1531 >>  
	enemy[2].head =  97.0908
	enemy[2].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[2].model = G_M_Y_LOST_01
	enemy[2].vcov =  << 1697.1594, 3313.7678, 40.1481 >>  

	enemy[3].loc =<< 1701.9045, 3287.5015, 47.2312 >>
	enemy[3].head =  119.5233
	enemy[3].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[3].model = G_M_Y_LOST_01
	enemy[3].vcov = << 1763.3706, 3287.4458, 40.1720 >> 
	
	enemy[4].loc =<< 1684.7559, 3287.3040, 40.1491 >>   
	enemy[4].head =  41.1540
	enemy[4].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[4].model = G_M_Y_LOST_01

	enemy[5].loc =<< 1712.4543, 3293.5645, 40.1649 >>
	enemy[5].head =  62.9918
	enemy[5].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[5].model = G_M_Y_LOST_01

	enemy[6].loc = << 1693.9274, 3292.3901, 40.1503 >>
	enemy[6].head = 325.6591 
	enemy[6].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[6].model = G_M_Y_LOST_01
	enemy[6].vcov = << 1708.8109, 3296.2327, 40.1512 >> 
	
	// chaps after snipe
	enemy[7].loc = << 1693.6914, 3292.3579, 40.1491 >>
	enemy[7].head =217.1215 
	enemy[7].wpn = WEAPONTYPE_PISTOL
	enemy[7].model = G_M_Y_LOST_01
	
	enemy[8].loc =<< 1701.1838, 3287.7649, 40.1502 >> 
	enemy[8].head = 7.6591
	enemy[8].wpn = WEAPONTYPE_PISTOL
	enemy[8].model = G_M_Y_LOST_01  
	
	//Behind crate by bus
	enemy[9].loc = << 1724.58, 3275.33, 41.13 >> 
	enemy[9].head = 61.88  
	enemy[9].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[9].model = G_M_Y_LOST_01 
	
	enemy[10].loc =<< 1748.68, 3275.61, 40.12 >> 
	enemy[10].head = 42.4657
	enemy[10].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[10].model = G_M_Y_LOST_01

	enemy[11].loc =<< 1748.42, 3272.33, 40.17 >> 
	enemy[11].head = 34.3434
	enemy[11].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[11].model = G_M_Y_LOST_01
	
	enemy[12].loc = <<1723.0385, 3290.3284, 40.2005>> 
	enemy[12].head = 30.1818 
	enemy[12].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[12].model =G_M_Y_LOST_01

	enemy[13].loc =<< 1719.85, 3233.0181, 40.5404 >>
	enemy[13].head =  12.9925
	enemy[13].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[13].model = G_M_Y_LOST_01

	enemy[14].loc = << 1679.7498, 3264.4829, 39.7197 >> 
	enemy[14].head =  218.2584 
	enemy[14].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[14].model = G_M_Y_LOST_01
	
	enemy[15].loc =  << 1580.0577, 3219.1492, 39.4144 >> 
	enemy[15].head = 254.6210 
	enemy[15].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[15].model = G_M_Y_LOST_01
	
	enemy[16].loc = << 1691.4519, 3271.1589, 39.9328 >>
	enemy[16].head =    240.1006
	enemy[16].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[16].model = G_M_Y_LOST_01
	
	enemy[17].loc = << 1683.58, 3285.26, 40.95 >>
	enemy[17].head = 82.84 
	enemy[17].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[17].model = G_M_Y_LOST_01
	
	enemy[18].loc =<< 1691.8838, 3274.2786, 40.0108 >> 
	enemy[18].head =  263.0063
	enemy[18].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[18].model = G_M_Y_LOST_01
	
	//Behind crate close - by ammo
	enemy[19].loc = <<1713.3010, 3274.1382, 40.1518>> 
	enemy[19].head =  1.8871 
	enemy[19].wpn = WEAPONTYPE_SAWNOFFSHOTGUN
	enemy[19].model = G_M_Y_LOST_01
	
	enemy[20].loc = << 1708.3717, 3285.8560, 47.9020 >>
	enemy[20].head = 294.3297 
	enemy[20].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[20].model = G_M_Y_LOST_01

	enemy[21].loc =<< 1701.5347, 3292.0940, 48.4063 >>
	enemy[21].head = 11.6120
	enemy[21].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[21].model = G_M_Y_LOST_01
	
	enemy[22].loc =<< 1730.5468, 3308.7539, 40.1517 >>  
	enemy[22].head = 183.2464 
	enemy[22].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[22].model = G_M_Y_LOST_01
	enemy[22].vcov = << 1729.5747, 3288.0334, 40.1538 >> 
	
	//behind crate closer by missiles
	enemy[23].loc = <<1731.6002, 3278.6584, 40.1009>>   
	enemy[23].head = 32.1404
	enemy[23].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[23].model = G_M_Y_LOST_01
	
	enemy[24].loc =<< 1741.5631, 3273.5815, 40.1562 >>
	enemy[24].head =246.8833 
	enemy[24].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[24].model = G_M_Y_LOST_01

	enemy[25].loc =<< 1755.2274, 3299.5830, 40.1551 >> 
	enemy[25].head =  190.3749  
	enemy[25].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[25].model = G_M_Y_LOST_01
	
	enemy[26].loc =<< 1757.9540, 3296.8313, 40.1557 >> 
	enemy[26].head = 72.9738  
	enemy[26].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[26].model = G_M_Y_LOST_01
	
	enemy[27].loc =<< 1590.1178, 3223.7263, 39.4141 >>
	enemy[27].head = 103.4968 
	enemy[27].wpn = WEAPONTYPE_UNARMED
	enemy[27].model = G_M_Y_LOST_01
	
	//new shoot out guys
	
	//behind van
	enemy[28].loc =<<1746.8656, 3274.4014, 40.1354>>
	enemy[28].head = 76.9593
	enemy[28].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[28].model = G_M_Y_LOST_01
	
	//by crate by caravan
	enemy[29].loc =<< 1751.12, 3289.48, 41.11 >>
	enemy[29].head = 91.02
	enemy[29].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[29].model = G_M_Y_LOST_01
	
	//by the two crates 
	enemy[30].loc =<<1729.1697, 3276.9617, 40.1146>>
	enemy[30].head = 40.8308
	enemy[30].wpn = WEAPONTYPE_PISTOL
	enemy[30].model = G_M_Y_LOST_01
	
	enemy[31].model = G_M_Y_LOST_01
	
	//in plane
	enemy[32].loc =<< 1731.9271, 3297.8848, 40.2261 >>
	enemy[32].head = 195.9035
	enemy[32].wpn = WEAPONTYPE_CARBINERIFLE
	enemy[32].model = G_M_Y_LOST_01
	
	//Just used to
	enemy[33].loc = <<1731.0662, 3419.6572, 36.9629>>
	enemy[33].head = 126.9720
	enemy[33].wpn = WEAPONTYPE_UNARMED
	enemy[33].model = G_M_Y_LOST_01
	
	driver[1].wpn = WEAPONTYPE_CARBINERIFLE
	driver[1].model = G_M_Y_LOST_01
	driver[1].vcov = << 1744.26,3256.7,40.9 >> 

	driver[2].wpn = WEAPONTYPE_CARBINERIFLE
	driver[2].model = G_M_Y_LOST_01
	driver[2].vcov = << 1733.74, 3239.8, 40.6416 >> 
	
	driver[6].wpn = WEAPONTYPE_CARBINERIFLE
	driver[6].model = G_M_Y_LOST_01
	driver[6].vcov = << 1684.6627, 3242.6697, 39.5164 >> 
	
	driver[8].wpn = WEAPONTYPE_CARBINERIFLE
	driver[8].model = G_M_Y_LOST_01
	driver[8].vcov = << 1744.3192, 3276.4531, 40.1562 >>
	
	driver[9].wpn = WEAPONTYPE_CARBINERIFLE
	driver[9].model = G_M_Y_LOST_01
	driver[9].vcov = << 1762.5542, 3266.3733, 40.9775 >>
	
	driver[10].wpn = WEAPONTYPE_CARBINERIFLE
	driver[10].model = G_M_Y_LOST_01
	driver[10].vcov = << 1685.1526, 3278.7534, 40.0407 >>
	
		//for the skip menu
	#IF IS_DEBUG_BUILD
		SkipMenuStruct[0].sTxtLabel		= "SETUP"
		SkipMenuStruct[0].bSelectable	= FALSE
		SkipMenuStruct[1].sTxtLabel		= "Opening Cutscene | CUT: TREVOR_2_INT"
		SkipMenuStruct[1].bSelectable	= FALSE
		SkipMenuStruct[2].sTxtLabel		= "Follow Ron to the airstrip"
		SkipMenuStruct[3].sTxtLabel	 	= "Snipe Enemies | CUT: TRV_2_MCS_4_CONCAT"
		SkipMenuStruct[4].sTxtLabel		= "CUT HELI - TRV_2_MCS_4_CONCAT"
		SkipMenuStruct[5].sTxtLabel	 	= "Steal the plane"
		SkipMenuStruct[6].sTxtLabel	 	= "On plane wing | CUT: TRV_2_MSC_6"
		SkipMenuStruct[7].sTxtLabel	 	= "Introduction to flight"
		SkipMenuStruct[8].sTxtLabel	 	= "Race To Airstrip"
		SkipMenuStruct[9].sTxtLabel	    = "Arrive at Hanger | CUT:TRV2_MCS_8"
		SkipMenuStruct[10].bSelectable	= FALSE
		SkipMenuStruct[10].sTxtLabel	    = "Debug Only: Test Stage"
	#ENDIF	
	
	PRINTSTRING("Data array init")
	PRINTNL()	
	
ENDPROC

PROC create_crates()

	IF NOT DOES_ENTITY_EXIST(crate[1])
		crate[1] =CREATE_OBJECT(PROP_GAS_TANK_02A,<<1683.27, 3278.70, 39.93>>)
		SET_ENTITY_ROTATION(crate[1],<<0.00, 0.00, 109.96>>)
		SET_ENTITY_LOD_DIST(crate[1],1000)
		SET_ENTITY_HEALTH(crate[1],1000)
		FREEZE_ENTITY_POSITION(crate[1],TRUE)
		SET_ENTITY_PROOFS(crate[1],TRUE,TRUE,TRUE,TRUE,TRUE)
	ENDIF
	
//	IF NOT DOES_ENTITY_EXIST(crate[2])
//		crate[2] =CREATE_OBJECT(PROP_GAS_TANK_02A, << 1677.74, 3228.8654, 39.5427 >>  )
//		SET_ENTITY_LOD_DIST(crate[2],1000)
//		SET_ENTITY_HEADING(crate[2],120.0)
//		SET_ENTITY_HEALTH(crate[2],1000)
//	ENDIF
	
	FOR iCount = 0 TO 14
		IF DOES_ENTITY_EXIST(crate[iCount])
			FREEZE_ENTITY_POSITION(crate[iCount],TRUE)
		ENDIF
	ENDFOR																				
ENDPROC
																					
// ╚══════════════════════════╡	 	Pause Speech Functions	╞════════════════════════════╝   
//PURPOSE: Pauses the speech if it should be

// ╚═════════════════════════════════════════════════════════════════════════════════╝
// ╚══════════════════════════╡	 	Controls  		╞════════════════════════════╝    
// ╚═════════════════════════════════════════════════════════════════════════════════╝

PROC HANDLE_CHANGE_IN_fRotation()
    IF NOT IS_ENTITY_DEAD(mission_veh[2].veh)
        fRotationPrev = fRotation
        fRotation = GET_ENTITY_HEADING(mission_veh[2].veh)
        fRotationAccel = fRotation - fRotationPrev
        
        IF fRotationAccel > 180.0
            fRotationAccel -= 360.0
        ELIF fRotationAccel < -180.0
            fRotationAccel += 360.0
        ENDIF
        
        IF heading_timer >= 0.1
            heading_timer = 0.0                       
//            fRotationAccel_average = (fRotationAccel_total / headings_captured) * 10

//            fRotationAccel_total = 0.0 
//            headings_captured = 0
        ELSE
//            fRotationAccel_total += fRotationAccel
            heading_timer = heading_timer +@ 1.0
//            headings_captured++
        ENDIF        
    ENDIF
ENDPROC

INT iKillTimer
INT iPlaneWingShouts

PROC MANAGE_BIKER_ON_WING()
	REQUEST_ANIM_DICT("misstrevor2ig_11")
	IF HAS_ANIM_DICT_LOADED("misstrevor2ig_11")
		//PRINTSTRING("INSIDE MANAGE BIKER ON WING")PRINTNL()
		IF NOT IS_ENTITY_DEAD(mission_veh[2].veh)
		AND NOT IS_PED_INJURED(enemy[27].ped)

			HANDLE_CHANGE_IN_fRotation()
//			fForwardSpeed = GET_ENTITY_SPEED(mission_veh[2].veh)
//			fFowardAccel = (fForwardSpeed - fForwardSpeedPrev)
//			fForwardSpeedPrev = fForwardSpeed
			
			PRINTSTRING("Plane Roll")PRINTFLOAT(GET_ENTITY_ROLL(mission_veh[2].veh))PRINTNL()PRINTNL()
			
			IF IS_SCREEN_FADED_IN()
				IF eBalanceState > STATE_INTRO
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						IF MANAGE_MY_TIMER(iFallOfftimer,18000)
						OR IS_ENTITY_UPSIDEDOWN(PLAYER_PED_ID()) 
						OR GET_ENTITY_ROLL(mission_veh[2].veh) > 160
						OR GET_ENTITY_ROLL(mission_veh[2].veh) < -160
							IF NOT IS_PED_INJURED(enemy[27].ped)
								IF HAS_LABEL_BEEN_TRIGGERED("IN THE AIR")
									PRINTSTRING("setting FALL_OFF") PRINTNL()
									IF eBalanceState != STATE_FALL_OFF
										KILL_ANY_CONVERSATION()
										IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(psRon.pedIndex,FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE)) < 500
											//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(enemy[27].ped,"SUPER_HIGH_FALL","WAVELOAD_PAIN_MALE","SPEECH_PARAMS_FORCE_FRONTEND")
											STOP_CURRENT_PLAYING_AMBIENT_SPEECH(enemy[27].ped)
											DISABLE_PED_PAIN_AUDIO(enemy[27].ped,TRUE)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(enemy[27].ped,"TR2_ZAAA","LOST1",SPEECH_PARAMS_FORCE)
											iLeanStage = 0
											SET_ENTITY_COLLISION(enemy[27].ped,TRUE)
											IF NOT IS_PED_INJURED(enemy[27].ped)
												PROCESS_ENTITY_ATTACHMENTS(enemy[27].ped)
												SET_HIGH_FALL_TASK(enemy[27].ped,2000,10000)
											ENDIF
											eBalanceState = STATE_FALL_OFF
										ELSE
											STOP_CURRENT_PLAYING_AMBIENT_SPEECH(enemy[27].ped)
											SET_ENTITY_COLLISION(enemy[27].ped,TRUE)
											IF NOT IS_PED_INJURED(enemy[27].ped)
												PROCESS_ENTITY_ATTACHMENTS(enemy[27].ped)
												SET_HIGH_FALL_TASK(enemy[27].ped,2000,10000)
											ENDIF
											DISABLE_PED_PAIN_AUDIO(enemy[27].ped,TRUE)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(enemy[27].ped,"TR2_ZAAA","LOST1",SPEECH_PARAMS_FORCE)
											iLeanStage = 0
											eBalanceState = STATE_FALL_OFF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF eBalanceState != STATE_FALL_OFF
								IF eBalanceState = STATE_BALANCE
									IF GET_ENTITY_ROLL(mission_veh[2].veh) <= -20
										PRINTSTRING("setting STATE STATE_FALL_LEFT") PRINTNL()
										//SCRIPT_ASSERT("GOING TO FALL LEFT")
										IF eBalanceState != STATE_FALL_RIGHT
										OR eBalanceState != STATE_FALL_LEFT
											iLeanStage = 0
											eBalanceState = STATE_FALL_RIGHT
										ENDIF
									ELIF GET_ENTITY_ROLL(mission_veh[2].veh) >= 20
										PRINTSTRING("setting STATE STATE_FALL_RIGHT") PRINTNL()
										//SCRIPT_ASSERT("GOING TO FALL RIGHT")
										IF eBalanceState != STATE_FALL_LEFT
										OR eBalanceState != STATE_FALL_RIGHT
											iLeanStage = 0
											eBalanceState = STATE_FALL_LEFT
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("IN THE AIR")
				IF eBalanceState > STATE_INTRO
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						IF IS_ENTITY_IN_AIR(mission_veh[2].veh)
							iFallOfftimer = GET_GAME_TIMER()
							SET_LABEL_AS_TRIGGERED("IN THE AIR",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF eBalanceState != STATE_FALL_OFF
				IF NOT HAS_LABEL_BEEN_TRIGGERED("BIKER ON WING SHOUT")
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(enemy[27].ped,"TR2_EOAA","LOST6",SPEECH_PARAMS_FORCE)
					SET_LABEL_AS_TRIGGERED("BIKER ON WING SHOUT",TRUE)
					iPlaneWingShouts = GET_GAME_TIMER()
				ELSE
					IF MANAGE_MY_TIMER(iPlaneWingShouts,GET_RANDOM_INT_IN_RANGE(7000,9000))
						SET_LABEL_AS_TRIGGERED("BIKER ON WING SHOUT",FALSE)
					ENDIF
				ENDIF
			ENDIF

			SWITCH eBalanceState
			
				CASE STATE_INTRO
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDJumpOnToWing)
						sceneIDJumpOnToWing = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDJumpOnToWing, "misstrevor2ig_11", "idle_intro", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDJumpOnToWing, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
						//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<0.0, 0.0, 3.0>>), 1.6, 0, 255, 0)
						PRINTSTRING("setting STATE STATE_BALANCE") PRINTNL()
						iLeanStage = 0
						//iFallOfftimer = GET_GAME_TIMER()
						DISABLE_PLANE_AILERON(mission_veh[2].veh,FALSE,TRUE)
						SET_ENTITY_COLLISION(enemy[27].ped, FALSE)
						eBalanceState = STATE_BALANCE
					ENDIF
				BREAK

				CASE STATE_BALANCE
					
					//iLeanStage = 0
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDJumpOnToWing)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDJumpOnToWing) >= 1.0
							sceneIDHangingOnWing = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDHangingOnWing, "misstrevor2ig_11", "idle", SLOW_BLEND_IN, SLOW_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDHangingOnWing, TRUE)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDHangingOnWing, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
						ENDIF
					ENDIF
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 1.0
								sceneIDHangingOnWing = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDHangingOnWing, "misstrevor2ig_11", "idle", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDHangingOnWing, TRUE)
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDHangingOnWing, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
							ENDIF
						ENDIF
					ELSE
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDHangingOnWing)
							//Do Left and Right Fall anims
							IF HAS_LABEL_BEEN_TRIGGERED("IN THE AIR")
								IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
									IF MANAGE_MY_TIMER(iFallOfftimer,25000)
									OR IS_ENTITY_UPSIDEDOWN(PLAYER_PED_ID()) 
									OR GET_ENTITY_ROLL(mission_veh[2].veh) > 180
									OR GET_ENTITY_ROLL(mission_veh[2].veh) < -180
										IF eBalanceState != STATE_FALL_OFF
											IF NOT IS_PED_INJURED(enemy[27].ped)
												IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(psRon.pedIndex,FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE)) < 500
													STOP_CURRENT_PLAYING_AMBIENT_SPEECH(enemy[27].ped)
													DISABLE_PED_PAIN_AUDIO(enemy[27].ped,TRUE)
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(enemy[27].ped,"TR2_ZAAA","LOST1",SPEECH_PARAMS_FORCE)
													PRINTSTRING("setting FALL_OFF") PRINTNL()
													IF eBalanceState != STATE_FALL_OFF
														eBalanceState = STATE_FALL_OFF
													ENDIF
												ELSE
													STOP_CURRENT_PLAYING_AMBIENT_SPEECH(enemy[27].ped)
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(enemy[27].ped,"TR2_ZAAA","LOST1",SPEECH_PARAMS_FORCE)
													PRINTSTRING("setting FALL_OFF") PRINTNL()
													IF eBalanceState != STATE_FALL_OFF
														eBalanceState = STATE_FALL_OFF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
									
				BREAK
				
				CASE STATE_FALL_LEFT
					//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<-4.0, 0.0, 0.0>>), 0.6)
					
					SWITCH iLeanStage
					
						CASE 0
							//IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDHangingOnWing)
								sceneIDStumble = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDStumble, "misstrevor2ig_11", "turn_left_intro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDStumble, FALSE)
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDStumble, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
								iLeanStage++
							//ENDIF
						BREAK
						
						CASE 1
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 1.0
									sceneIDStumble = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
									TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDStumble, "misstrevor2ig_11", "turn_left_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDStumble, TRUE)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDStumble, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
									iLeanStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
						
							IF GET_ENTITY_ROLL(mission_veh[2].veh) > -10
								PRINTSTRING("going to left outro") PRINTNL()
								iLeanStage++
							ENDIF
							
						BREAK
						
						CASE 3
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
								IF IS_ENTITY_PLAYING_ANIM(enemy[27].ped,"misstrevor2ig_11", "turn_left_loop")
									sceneIDStumble = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
									TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDStumble, "misstrevor2ig_11", "turn_left_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDStumble, FALSE)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDStumble, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
									
									iLeanStage++
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 4
						
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 1.0
									sceneIDHangingOnWing = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
									TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDHangingOnWing, "misstrevor2ig_11", "idle", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDHangingOnWing, TRUE)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDHangingOnWing, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
									eBalanceState = STATE_BALANCE
									iLeanStage = 0
								ENDIF
							ENDIF
							
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE STATE_FALL_RIGHT
					//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<4.0, 0.0, 0.0>>), 0.6)
					
					SWITCH iLeanStage
					
						CASE 0
							//IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDHangingOnWing)
								sceneIDStumble = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDStumble, "misstrevor2ig_11", "turn_right_intro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDStumble, FALSE)
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDStumble, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
								iLeanStage++
							//ENDIF
						BREAK
						
						CASE 1
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 1.0
									sceneIDStumble = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
									TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDStumble, "misstrevor2ig_11", "turn_right_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDStumble, TRUE)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDStumble, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
									iLeanStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							// Returned to center (ish)
//							PRINTSTRING("fRotationAccel_average > ") PRINTFLOAT(fRotationAccel_average)
//							PRINTNL()
//							PRINTSTRING("fLeftThreshold") PRINTFLOAT(fLeftThreshold)
//							PRINTNL()
							
							IF GET_ENTITY_ROLL(mission_veh[2].veh) < 10
								PRINTSTRING("going to right outro") PRINTNL()
								iLeanStage++
							ENDIF
							
						BREAK
						
						CASE 3
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
								IF IS_ENTITY_PLAYING_ANIM(enemy[27].ped,"misstrevor2ig_11", "turn_right_loop")
									sceneIDStumble = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
									TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDStumble, "misstrevor2ig_11", "turn_right_outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDStumble, FALSE)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDStumble, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
									iLeanStage++
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 4
						
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 1.0
									sceneIDHangingOnWing = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
									TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDHangingOnWing, "misstrevor2ig_11", "idle", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDHangingOnWing, TRUE)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDHangingOnWing, mission_veh[2].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[2].veh, "chassis"))
									eBalanceState = STATE_BALANCE
									iLeanStage = 0
								ENDIF
							ENDIF
							
						BREAK
						
					ENDSWITCH
				BREAK
				
				CASE STATE_FALL_OFF
					SWITCH iLeanStage
						CASE 0

							SET_ENTITY_COLLISION(enemy[27].ped,TRUE)
							
							IF NOT IS_PED_INJURED(enemy[27].ped)
								PROCESS_ENTITY_ATTACHMENTS(enemy[27].ped)
								SET_HIGH_FALL_TASK(enemy[27].ped,2000,10000)
							ENDIF
							
							DISABLE_PLANE_AILERON(mission_veh[2].veh,FALSE,FALSE)
							REMOVE_ANIM_DICT("misstrevor2ig_11")
							IF DOES_BLIP_EXIST(enemy[27].blip)
								REMOVE_BLIP(enemy[27].blip)
							ENDIF
							PRINTSTRING("SCREAM!!!!!!!!!!!!!!! ")PRINTNL()
							REPLAY_RECORD_BACK_FOR_TIME(1)
							iKillTimer = GET_GAME_TIMER()
							iLeanStage++
						BREAK
						
						CASE 1
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_bi03")
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_bi03", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("TR_bi03",TRUE)
								ENDIF
							ELSE
								IF MANAGE_MY_TIMER(iKillTimer,3000)
									IF NOT IS_PED_INJURED(enemy[27].ped)
										SET_ENTITY_HEALTH(enemy[27].ped,0)
										iLeanStage++
									ENDIF
								ENDIF
							ENDIF
							
						BREAK
					ENDSWITCH
				BREAK
				
				
				
			ENDSWITCH
			
			
			PRINTSTRING("Rotation Speed: ") PRINTFLOAT(fRotation) PRINTNL()
//			PRINTSTRING("Rotation accel: ") PRINTFLOAT(fRotationAccel_average) PRINTNL()
			
				
//			PRINTSTRING("fForwardSpeed: ") PRINTFLOAT(fForwardSpeed) PRINTNL()
//			PRINTSTRING("fFowardAccel: ") PRINTFLOAT(fFowardAccel) PRINTNL()

		ENDIF
	ENDIF
		
ENDPROC

BOOL bPerformCheck = FALSE
INT iCheckTimer

PROC RUBBER_BAND_BETWEEN_TWO_VEHICLES(VEHICLE_INDEX vehChaser, VEHICLE_INDEX vehTarget, VECTOR vDestination, INT irecording, STRING StrRecording, FLOAT fOptimalDis,FLOAT fMinSpeed = 0.5, FLOAT fMaxSpeed = 1.2, BOOL bcheckahead =FALSE)
   REQUEST_VEHICLE_RECORDING(irecording,StrRecording)
   IF HAS_VEHICLE_RECORDING_BEEN_LOADED(irecording,StrRecording)
		VECTOR vStartCalculationPos 
		IF IS_VEHICLE_DRIVEABLE(vehChaser)
	   		vStartCalculationPos = GET_ENTITY_COORDS(vehChaser,FALSE)
		ENDIF
		VECTOR vPointPos
		FLOAT fCheckRecordingTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(irecording, StrRecording) / 2
		FLOAT fSearchUpDownTime = fCheckRecordingTime / 2
		FLOAT fUpTime
		FLOAT fDownTime
		FLOAT fUpDistanceFromPos
		FLOAT fDownDistanceFromPos
		FLOAT mainPlaybackSpeed 
		//INT iChecksPerformed = 0
		
		FLOAT fTargetDist
		FLOAT fChaserDist
		
		BOOL bAhead
		
	    IF bcheckahead 
			IF NOT IS_VECTOR_ZERO(vDestination)
				IF IS_VEHICLE_DRIVEABLE(vehChaser)
					fChaserDist = GET_DISTANCE_BETWEEN_COORDS(vDestination,GET_ENTITY_COORDS(vehChaser),FALSE)
					//PRINTSTRING("fChaserDist:")PRINTFLOAT(fChaserDist)PRINTNL()
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTarget)
					fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vDestination,GET_ENTITY_COORDS(vehTarget),FALSE)
					//PRINTSTRING("fTargetDist:")PRINTFLOAT(fTargetDist)PRINTNL()
				ENDIF
				
				IF fTargetDist > fChaserDist
					//PRINTSTRING("Ahead = TRUE")PRINTNL()
					bAhead = TRUE	
				ELSE
					//MODIFY_VEHICLE_TOP_SPEED(vehChaser,80)
					//PRINTSTRING("Ahead = FALSE")PRINTNL()
					bAhead = FALSE	
				ENDIF
			ENDIF
			
			IF bAhead = FALSE
				//WHILE iChecksPerformed <  20
					// check up distance
					IF bPerformCheck = FALSE
						fUpTime = fCheckRecordingTime + fSearchUpDownTime
						vPointPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(irecording, fUpTime, StrRecording)
						fUpDistanceFromPos = GET_DISTANCE_BETWEEN_COORDS(vStartCalculationPos, vPointPos)
						
						// check down distance
						fDownTime = fCheckRecordingTime - fSearchUpDownTime
						vPointPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(irecording, fDownTime, StrRecording)
						fDownDistanceFromPos = GET_DISTANCE_BETWEEN_COORDS(vStartCalculationPos, vPointPos)

						// work out if we should be going up or down
						IF fUpDistanceFromPos < fDownDistanceFromPos
							fCheckRecordingTime = fUpTime
						ELSE
							fCheckRecordingTime = fDownTime
						ENDIF
						
						fSearchUpDownTime = fSearchUpDownTime / 2
						iCheckTimer = GET_GAME_TIMER()
						bPerformCheck = TRUE
					ELSE
						IF MANAGE_MY_TIMER(iCheckTimer,20)
							bPerformCheck = FALSE
						ENDIF
					ENDIF
					//WAIT(0)
					//iChecksPerformed++
				//ENDWHILE
			ENDIF
		ENDIF
		
	    IF IS_VEHICLE_DRIVEABLE(vehChaser) AND IS_VEHICLE_DRIVEABLE(vehTarget)
	        IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTarget)
	            IF NOT IS_VECTOR_ZERO(vDestination)
					IF bAhead = FALSE
						mainPlaybackSpeed =  (fOptimalDis / GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehChaser),GET_ENTITY_COORDS(vehTarget)))
			            IF mainPlaybackSpeed > fMaxSpeed
			                mainPlaybackSpeed = fMaxSpeed
			            ENDIF
			            IF mainPlaybackSpeed < fMinSpeed
			                mainPlaybackSpeed = fMinSpeed
			            ENDIF
						IF bcheckahead  
							IF fCheckRecordingTime > GET_TIME_POSITION_IN_RECORDING(vehTarget)
								IF mainPlaybackSpeed > fMaxSpeed
			                		mainPlaybackSpeed = fMaxSpeed
			           			ENDIF
								//PRINTSTRING("player in front")
								//PRINTNL()	
							ENDIF
						ENDIF
					ELSE
						//PRINTSTRING("MAX SPEED")PRINTNL()
						mainPlaybackSpeed = fMaxSpeed
					ENDIF
				ELSE
					mainPlaybackSpeed =  (fOptimalDis / GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehChaser),GET_ENTITY_COORDS(vehTarget)))
				ENDIF
				//PRINTSTRING("PLAYBACK SPEED:")PRINTFLOAT(mainPlaybackSpeed)PRINTNL()
				SET_PLAYBACK_SPEED(vehTarget,mainPlaybackSpeed)
	        ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehChaser)
			IF vehChaser =  mission_veh[15].veh
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaser)
					SET_PLAYBACK_SPEED(vehChaser,(0.85-(mainPlaybackSpeed - 1)))
				ENDIF
			ENDIF
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehChaser)
			IF vehChaser = mission_veh[17].veh
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaser)
					SET_PLAYBACK_SPEED(vehChaser,(1-(mainPlaybackSpeed- 1)))
				ENDIF
			ENDIF	
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehChaser)
			IF vehChaser = mission_veh[18].veh
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaser)
					SET_PLAYBACK_SPEED(vehChaser,(1-(mainPlaybackSpeed- 1)))
				ENDIF
			ENDIF	
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehChaser)
			IF vehChaser = mission_veh[19].veh
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaser)
					SET_PLAYBACK_SPEED(vehChaser,(1-(mainPlaybackSpeed- 1)))
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		REQUEST_VEHICLE_RECORDING(irecording,StrRecording)
	ENDIF
ENDPROC

PROC MANAGE_PED_OUTFIT()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	ENDIF
	
	IF NOT IS_PED_INJURED(psRon.pedIndex)
		SET_PED_COMP_ITEM_CURRENT_SP(psRon.pedIndex, COMP_TYPE_PROPS, PROPS_P2_HEADSET,FALSE)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TORSO, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_LEG, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_JBIB, 0, 0)
		SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_HEAD, 0)
		SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_EYES, 0)
	ENDIF
ENDPROC

PROC SET_MISSION_PED_PROPERTIES(PED_INDEX PedIndex, REL_GROUP_HASH relGroupHash, BOOL bCanFlyThroughWindscreen, BOOL bKeepRelGroupOnCleanup,
								BOOL bCanBeTargetted, BOOL bIsEnemy)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_PED_INJURED(PedIndex)
		
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_WillFlyThroughWindscreen, bCanFlyThroughWindscreen)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, bKeepRelGroupOnCleanup)
			SET_PED_CAN_BE_TARGETTED(PedIndex, bCanBeTargetted)
			SET_PED_AS_ENEMY(PedIndex, bIsEnemy)
			SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, relGroupHash)
		
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates a ped or player ped to be used by player and returns TRUE if such pad was created successfully.
/// PARAMS:
///    psPed - PED_STRUCT containg ped details, like PED_INDEX, blip, coordinates, heading, model etc.
///    bPlayerPed - Boolean indicating if a player ped or NPC ped should be created.
///    relGroupHash - Relationship group hash for the created ped.
///    bCreateBlip - Boolean indicating if a blip for this ped should be created.
///    eCharacter - Enum specifying character from story characters list. Use NO_CHARACTER for characters not stored in that list and provide a MODEL_NAMES in PED_STRUCT.
///    bCanFlyThroughWindscreen - Sets if ped can fly through windscreen when car crashes.
///    bCanBeTargetted - Sets if ped can be targetted by player.
///    bIsEnemy - Sets if ped is considered an enemy.
///    VehicleIndex - Specify a vehicle index if ped should be created inside a vehicle. IF not use NULL.
///    eVehicleSeat - Vehicle seat enum for peds created in vehicles.
/// RETURNS:
///    TRUE if ped was created successfully, FALSE if otherwise.
FUNC BOOL HAS_MISSION_PED_BEEN_CREATED(PED_SNIPE_STRUCT &psPed, BOOL bPlayerPed, REL_GROUP_HASH relGroupHash, BOOL bCreateBlip, enumCharacterList eCharacter,
									   BOOL bCanFlyThroughWindscreen = FALSE, BOOL bCanBeTargetted = FALSE, BOOL bIsEnemy = FALSE, VEHICLE_INDEX VehicleIndex = NULL,
									   VEHICLE_SEAT eVehicleSeat = VS_DRIVER, BOOL bKeepRelGroupOnCleanup = TRUE #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	IF ( bPlayerPed = FALSE )	//create non player ped
		
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
		
			REQUEST_MODEL(psPed.ModelName)
			
			IF HAS_MODEL_LOADED(psPed.ModelName)
		
				IF ( VehicleIndex = NULL )	//create ped outside of vehicle
				
					IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT if no character was specified
					
						psPed.PedIndex = CREATE_PED(PEDTYPE_MISSION, psPed.ModelName, psPed.vPosition, psPed.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
					
					ELSE								//use character name to define the ped MODEL_NAMES
					
						IF CREATE_NPC_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
						ENDIF
					
					ENDIF
					
					IF NOT IS_PED_INJURED(psPed.PedIndex)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
												   
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
					
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF
					#ENDIF
					
				ELIF ( VehicleIndex != NULL )	//create ped in a vehicle
				
					IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
						IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT
					
							psPed.PedIndex = CREATE_PED_INSIDE_VEHICLE(VehicleIndex, PEDTYPE_MISSION, psPed.ModelName, eVehicleSeat)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							
						ELSE								//use character name to define the ped MODEL_NAMES
						
							IF CREATE_NPC_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat)
								SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							ENDIF
						
						ENDIF
						
						IF NOT IS_PED_INJURED(psPed.PedIndex)
						
							SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
													   bCanBeTargetted, bIsEnemy)
							
							IF ( bCreateBlip = TRUE )
								psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
							ENDIF
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ENDIF
						#ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ELSE
		
			//if the ped already exists return true
			RETURN TRUE
		
		ENDIF
		
		
	ELIF ( bPlayerPed = TRUE )	//create player ped, for example a ped that player can hotswap to and take control of
	
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
	
			IF ( VehicleIndex = NULL )	//create player ped outside of vehicle
	
				IF CREATE_PLAYER_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading, TRUE)
				
					SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
					
					IF ( bCreateBlip = TRUE )
						psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF	
					#ENDIF
					
					RETURN TRUE
					
				ENDIF
				
			ELIF ( VehicleIndex != NULL )	//create player ped in a vehicle
			
				IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
					IF CREATE_PLAYER_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat, TRUE)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
						
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
						
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped  in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ENDIF
						#ENDIF
						
						RETURN TRUE
					
					ENDIF
				
				ENDIF
			
			ENDIF
		
		ELSE
		
			//if the ped already exists, return true
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SETUP_MISSION_REQUIREMENT(MISSION_REQUIREMENT eRequirement, VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)

	IF NOT IS_VECTOR_ZERO(vPos)
	
	ENDIF
	
	IF fHeading !=0
	
	ENDIF

	SWITCH eRequirement

		CASE REQ_RON
			IF NOT DOES_ENTITY_EXIST(psRon.pedIndex)
				IF HAS_MISSION_PED_BEEN_CREATED(psRon, FALSE, RELGROUPHASH_PLAYER, TRUE, CHAR_RON, FALSE, FALSE, FALSE, NULL, VS_DRIVER)	
				//IF CREATE_NPC_PED_ON_FOOT(psRon.pedIndex, CHAR_RON, vPos,fHeading)
					SET_ENTITY_COORDS(psRon.pedIndex,vPos)
					SET_ENTITY_HEADING(psRon.pedIndex,fHeading)
					SET_PED_CAN_BE_TARGETTED(psRon.pedIndex, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(psRon.pedIndex, RELGROUPHASH_PLAYER)
					ADD_PED_FOR_DIALOGUE(sSpeech,3,psRon.pedIndex,"NervousRon")
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(psRon.pedIndex,KNOCKOFFVEHICLE_NEVER)
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psRon.pedIndex, TRUE)
					GIVE_WEAPON_TO_PED(psRon.pedIndex, WEAPONTYPE_UNARMED , 2000, TRUE)
					SET_PED_CAN_BE_TARGETTED(psRon.pedIndex, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(psRon.pedIndex ,rel_group_buddies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,rel_group_buddies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
					SET_PED_CONFIG_FLAG(psRon.pedIndex,PCF_RunFromFiresAndExplosions,FALSE)
					SET_PED_AS_ENEMY(psRon.pedIndex , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(psRon.pedIndex)
					SET_PED_DEFAULT_COMPONENT_VARIATION(psRon.pedIndex)
					SET_PED_ACCURACY(psRon.pedIndex ,100)
					SET_PED_COMBAT_ABILITY(psRon.pedIndex ,CAL_PROFESSIONAL)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(psRon.pedIndex,FALSE)
					SET_ENTITY_HEALTH(psRon.pedIndex,300)
					SET_PED_SUFFERS_CRITICAL_HITS(psRon.pedIndex,TRUE)
					
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						SET_PED_COMP_ITEM_CURRENT_SP(psRon.pedIndex, COMP_TYPE_PROPS, PROPS_P2_HEADSET,FALSE)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_BERD, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAND, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_FEET, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TEETH, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_DECL, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_JBIB, 0, 0)
						SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_HEAD, 0)
						SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_EYES, 0)
					ENDIF
					//PRINTSTRING("MADE RON")PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF sSpeech.PedInfo[3].Index != psRon.pedIndex
					ADD_PED_FOR_DIALOGUE(sSpeech, 3, psRon.pedIndex, "NervousRon")
				ENDIF
				IF NOT IS_PED_INJURED(psRon.pedIndex)
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(psRon.pedIndex,KNOCKOFFVEHICLE_NEVER)
				ENDIF
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psRon.pedIndex, TRUE)
				GIVE_WEAPON_TO_PED(psRon.pedIndex, WEAPONTYPE_UNARMED , 2000, TRUE)
				SET_PED_CAN_BE_TARGETTED(psRon.pedIndex, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(psRon.pedIndex ,rel_group_buddies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,rel_group_buddies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
				SET_PED_AS_ENEMY(psRon.pedIndex , FALSE)
				SET_ENTITY_AS_MISSION_ENTITY(psRon.pedIndex)
				SET_PED_DEFAULT_COMPONENT_VARIATION(psRon.pedIndex)
				SET_PED_ACCURACY(psRon.pedIndex ,100)
				SET_PED_COMBAT_ABILITY(psRon.pedIndex ,CAL_PROFESSIONAL)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(psRon.pedIndex,FALSE)
				SET_ENTITY_HEALTH(psRon.pedIndex,300)
				SET_PED_SUFFERS_CRITICAL_HITS(psRon.pedIndex,TRUE)
				
				IF NOT IS_PED_INJURED(psRon.pedIndex)
					SET_PED_COMP_ITEM_CURRENT_SP(psRon.pedIndex, COMP_TYPE_PROPS, PROPS_P2_HEADSET,FALSE)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_BERD, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TORSO, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAND, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_FEET, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TEETH, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_DECL, 0, 0)
					SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_JBIB, 0, 0)
					SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_HEAD, 0)
					SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_EYES, 0)
				ENDIF
				
				//PRINTSTRING("MADE RON")PRINTNL()
			
				RETURN TRUE
			ENDIF
			
			IF bJustRequestAssets
				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_RON))
			ENDIF
			
		BREAK
	
		CASE REQ_TREVOR_QUAD
			IF NOT DOES_ENTITY_EXIST(mission_veh[12].veh)
				REQUEST_MODEL(BLAZER)
				REQUEST_VEHICLE_ASSET(BLAZER)
				IF HAS_MODEL_LOADED(BLAZER)
				AND HAS_VEHICLE_ASSET_LOADED(BLAZER)
					mission_veh[12].veh = CREATE_VEHICLE(BLAZER,vPos,fHeading)
					SET_VEHICLE_COLOURS(mission_veh[12].veh,35,35)
					SET_VEHICLE_NUMBER_PLATE_TEXT(mission_veh[12].veh,"TPI1000")
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[12].veh,TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[12].veh)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(mission_veh[12].veh, TRV2_ATV_DAMAGE)
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(mission_veh[12].veh, TRV2_ATV_MAX_SPEED) 
					SET_VEHICLE_HAS_STRONG_AXLES(mission_veh[12].veh,TRUE)
					//SET_VEHICLE_DOORS_LOCKED(mission_veh[12].veh,VEHICLELOCK_UNLOCKED)
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[12].veh)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[12].veh,TRUE)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_RON_QUAD
			IF NOT DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
				REQUEST_MODEL(BLAZER)
				REQUEST_VEHICLE_ASSET(BLAZER)
				IF HAS_MODEL_LOADED(BLAZER)
				AND HAS_VEHICLE_ASSET_LOADED(BLAZER)
					mission_veh[MV_RON_QUAD].veh = CREATE_VEHICLE(BLAZER,vPos,fHeading)
					SET_VEHICLE_COLOURS(mission_veh[MV_RON_QUAD].veh,62,62)
					SET_VEHICLE_NUMBER_PLATE_TEXT(mission_veh[MV_RON_QUAD].veh,"B3L13V3")
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_RON_QUAD].veh,FALSE)
					ENDIF
					SET_VEHICLE_TYRES_CAN_BURST(mission_veh[MV_RON_QUAD].veh,FALSE)
					SET_HORN_ENABLED(mission_veh[MV_RON_QUAD].veh,FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[MV_RON_QUAD].veh)
					SET_VEHICLE_HAS_STRONG_AXLES(mission_veh[MV_RON_QUAD].veh,TRUE)
					//PRINTSTRING("MADE RONS QUAD")PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)

					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_RON_QUAD].veh,FALSE)
					SET_HORN_ENABLED(mission_veh[MV_RON_QUAD].veh,FALSE)
					//PRINTSTRING("MADE RONS QUAD")PRINTNL()
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_RON_QUAD_START
			IF NOT DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
				REQUEST_MODEL(BLAZER)
				REQUEST_VEHICLE_ASSET(BLAZER)
				IF HAS_MODEL_LOADED(BLAZER)
				AND HAS_VEHICLE_ASSET_LOADED(BLAZER)
					mission_veh[MV_RON_QUAD].veh = CREATE_VEHICLE(BLAZER,vPos,fHeading)
					SET_VEHICLE_COLOURS(mission_veh[MV_RON_QUAD].veh,62,62)
					SET_VEHICLE_NUMBER_PLATE_TEXT(mission_veh[MV_RON_QUAD].veh,"B3L13V3")
					SET_HORN_ENABLED(mission_veh[MV_RON_QUAD].veh,FALSE)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_RON_QUAD].veh,FALSE)
					ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				REQUEST_VEHICLE_ASSET(BLAZER)
				IF HAS_VEHICLE_ASSET_LOADED(BLAZER)
					//PRINTSTRING("MADE RONS QUAD")PRINTNL()
					RETURN TRUE
				ENDIF
				
			ENDIF
		BREAK
		
		CASE REQ_VEHICLE_TEST
			IF NOT DOES_ENTITY_EXIST(mission_veh[20].veh)
				REQUEST_MODEL(TAILGATER)
				REQUEST_VEHICLE_ASSET(TAILGATER)
				IF HAS_MODEL_LOADED(TAILGATER)
				AND HAS_VEHICLE_ASSET_LOADED(TAILGATER)
					mission_veh[20].veh = CREATE_VEHICLE(TAILGATER,<<1971.8951, 3796.5391, 31.2872>>,152.6909)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[20].veh,6)
					SET_VEHICLE_NUMBER_PLATE_TEXT(mission_veh[20].veh,"D1RTY 10")
					SET_HORN_ENABLED(mission_veh[20].veh,FALSE)
					//SET_VEHICLE_DIRT_LEVEL(mission_veh[20].veh,10)
					//SET_VEHICLE_DIRT_LEVEL(mission_veh[20].veh,12)
					SET_VEHICLE_DIRT_LEVEL(mission_veh[20].veh,10)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[20].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[20].veh,FALSE)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK

		CASE REQ_RON_QUAD_SNIPE
			IF NOT DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
				REQUEST_MODEL(BLAZER)
				REQUEST_VEHICLE_RECORDING(29,"RCSJC")
				REQUEST_VEHICLE_ASSET(BLAZER)
				IF HAS_MODEL_LOADED(BLAZER)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(29,"RCSJC")
					mission_veh[MV_RON_QUAD].veh = CREATE_VEHICLE(BLAZER,vPos,fHeading)
					SET_VEHICLE_COLOURS(mission_veh[MV_RON_QUAD].veh,62,62)
					SET_VEHICLE_NUMBER_PLATE_TEXT(mission_veh[MV_RON_QUAD].veh,"B3L13V3")
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_RON_QUAD].veh,FALSE)
					ENDIF
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
						START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh,29,"RCSJC")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh,(8000-GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_QUAD].veh)))
					ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				REQUEST_VEHICLE_ASSET(BLAZER)
				REQUEST_VEHICLE_RECORDING(29,"RCSJC")
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(29,"RCSJC")
				AND HAS_VEHICLE_ASSET_LOADED(BLAZER)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
						SET_ENTITY_COORDS(mission_veh[MV_RON_QUAD].veh,vPos)
						SET_ENTITY_HEADING(mission_veh[MV_RON_QUAD].veh,fHeading)
						SET_VEHICLE_COLOURS(mission_veh[MV_RON_QUAD].veh,62,62)
						SET_VEHICLE_NUMBER_PLATE_TEXT(mission_veh[MV_RON_QUAD].veh,"B3L13V3")
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_RON_QUAD].veh,FALSE)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh,29,"RCSJC")
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh,(8000-GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_QUAD].veh)))
						ENDIF
						//PRINTSTRING("MADE RONS QUAD")PRINTNL()
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_RON_PLANE_WITH_PILOT
			IF NOT DOES_ENTITY_EXIST(mission_veh[1].veh)
				REQUEST_MODEL(CUBAN800)
				REQUEST_MODEL(G_M_Y_LOST_01)
				REQUEST_VEHICLE_ASSET(CUBAN800)
				IF HAS_MODEL_LOADED(CUBAN800)
				AND HAS_VEHICLE_ASSET_LOADED(CUBAN800)
					CREATE_ENEMY_VEHICLE(1,CUBAN800, vPos, fHeading ,G_M_Y_LOST_01)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[1].veh,FALSE)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[1].veh,FALSE)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[1].veh,0)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[1].veh,TRUE)
					SET_VEHICLE_DOORS_LOCKED(mission_veh[1].veh,VEHICLELOCK_UNLOCKED)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[1].veh,SC_DOOR_FRONT_LEFT,FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[1].veh,SC_DOOR_FRONT_RIGHT,FALSE)
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[1].veh,FALSE)
					SET_ENTITY_COORDS(mission_veh[1].veh,vPos)
					SET_ENTITY_HEADING(mission_veh[1].veh,fHeading)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[1].veh,0)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[1].veh,TRUE)
					SET_VEHICLE_DOORS_LOCKED(mission_veh[1].veh,VEHICLELOCK_UNLOCKED)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[1].veh,SC_DOOR_FRONT_LEFT,FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[1].veh,SC_DOOR_FRONT_RIGHT,FALSE)
					//PRINTSTRING("MADE TERVORS QUAD")PRINTNL()
				ENDIF
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE REQ_RON_PLANE
			IF NOT DOES_ENTITY_EXIST(mission_veh[1].veh)
				REQUEST_MODEL(CUBAN800)
				REQUEST_VEHICLE_ASSET(CUBAN800)
				IF HAS_MODEL_LOADED(CUBAN800)
				AND HAS_VEHICLE_ASSET_LOADED(CUBAN800)
					mission_veh[1].veh = CREATE_VEHICLE(CUBAN800,vPos,fHeading)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[1].veh,0)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[1].veh,FALSE)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[1].veh,FALSE)
					SET_VEHICLE_DOORS_LOCKED(mission_veh[1].veh,VEHICLELOCK_UNLOCKED)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[1].veh,SC_DOOR_FRONT_LEFT,FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[1].veh,SC_DOOR_FRONT_RIGHT,FALSE)
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[1].veh,FALSE)
					SET_ENTITY_COORDS(mission_veh[1].veh,vPos)
					SET_ENTITY_HEADING(mission_veh[1].veh,fHeading)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[1].veh,0)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[1].veh,FALSE)
					SET_VEHICLE_DOORS_LOCKED(mission_veh[1].veh,VEHICLELOCK_UNLOCKED)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[1].veh,SC_DOOR_FRONT_LEFT,FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[1].veh,SC_DOOR_FRONT_RIGHT,FALSE)
					//PRINTSTRING("MADE TERVORS QUAD")PRINTNL()
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_TREVOR_PLANE
			IF NOT DOES_ENTITY_EXIST(mission_veh[2].veh)
				REQUEST_MODEL(CUBAN800)
				REQUEST_VEHICLE_ASSET(CUBAN800)
				IF HAS_MODEL_LOADED(CUBAN800)
				AND HAS_VEHICLE_ASSET_LOADED(CUBAN800)
					mission_veh[2].veh = CREATE_VEHICLE(CUBAN800,vPos,fHeading)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[2].veh,FALSE)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[2].veh,2)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[2].veh,TRUE)
					SET_VEHICLE_DOORS_LOCKED(mission_veh[2].veh,VEHICLELOCK_UNLOCKED)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[2].veh,SC_DOOR_FRONT_LEFT,FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[2].veh,SC_DOOR_FRONT_RIGHT,FALSE)
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(mission_veh[2].veh, TRV2_PLANE_MAX_SPEED)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(mission_veh[2].veh,TRV2_PLANE_DAMAGE)
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[2].veh,FALSE)
					SET_ENTITY_COORDS(mission_veh[2].veh,vPos)
					SET_ENTITY_HEADING(mission_veh[2].veh,fHeading)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[2].veh,2)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[2].veh,TRUE)
					SET_VEHICLE_DOORS_LOCKED(mission_veh[2].veh,VEHICLELOCK_UNLOCKED)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[2].veh,SC_DOOR_FRONT_LEFT,FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mission_veh[2].veh,SC_DOOR_FRONT_RIGHT,FALSE)
					//PRINTSTRING("MADE TERVORS QUAD")PRINTNL()
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_BUGGY
			IF NOT DOES_ENTITY_EXIST(vehBuggy)
				REQUEST_MODEL(DUNE)
				IF HAS_MODEL_LOADED(DUNE)
					vehBuggy = CREATE_VEHICLE(DUNE,vPos,fHeading)
					SET_VEHICLE_COLOUR_COMBINATION(vehBuggy,1)
					SET_VEHICLE_NUMBER_PLATE_TEXT(vehBuggy,"TH3MUL3")
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBuggy,TRUE)
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehBuggy)
					SET_ENTITY_COORDS(vehBuggy,vPos)
					SET_ENTITY_HEADING(vehBuggy,fHeading)
					SET_VEHICLE_NUMBER_PLATE_TEXT(vehBuggy,"TH3MUL3")
					SET_VEHICLE_COLOUR_COMBINATION(vehBuggy,1)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBuggy,TRUE)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_HAULER_AND_TANKER
			IF NOT DOES_ENTITY_EXIST(mission_veh[MV_HAULER].veh)
				REQUEST_MODEL(HAULER)
				IF HAS_MODEL_LOADED(HAULER)
					CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_HAULER),HAULER, << 1681.1349, 3267.7612, 39.7872 >>,   121.1609, G_M_Y_LOST_01)
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(mission_veh[MV_TANKER].veh)
					REQUEST_MODEL(TANKER)
					IF HAS_MODEL_LOADED(TANKER)
						CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_TANKER),TANKER, << 1675.4041, 3284.4958, 39.8680 >> , 196.2680,G_M_Y_LOST_01)
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_HAULER].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TANKER].veh)
							ATTACH_VEHICLE_TO_TRAILER(mission_veh[MV_HAULER].veh,mission_veh[MV_TANKER].veh)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_HAULER].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_HAULER].veh,FALSE)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_HAULER].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_HAULER].veh,FALSE)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_OPEN_GUN_SHOP
			SET_SHOP_IS_AVAILABLE(GUN_SHOP_01_DT, TRUE)
			
			RETURN TRUE
		BREAK
		
		CASE REQ_GUN_SHOP_IN_MEMORY
			IF interior_gun_shop = NULL
				interior_gun_shop = GET_INTERIOR_AT_COORDS(<<1693.3518, 3759.2205, 33.7053>>)
			ENDIF
		
			IF IS_INTERIOR_READY(interior_gun_shop)
				RETURN TRUE
			ELSE
				PIN_INTERIOR_IN_MEMORY(interior_gun_shop)
			ENDIF
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


//PURPOSE:  PreLoads the next stage when the player get near to it.
FUNC BOOL PRE_STREAM_MISSION_STAGE()
	
	MISSION_STAGE_ENUM eStageToLoad = INT_TO_ENUM(MISSION_STAGE_ENUM, (ENUM_TO_INT(eMissionStage) + 1))
		
	SWITCH iPreStreamStage
	
		CASE 0
		
			IF eStageToLoad = MISSION_STAGE_SNIPE
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 1580.0, 3364.2393, 37.5 >>, <<100.0, 100.0, 100.0>>, FALSE )
					CLEANUP_LOADED_MODEL_ARRAY()
					ADD_MODEL_REQUEST_TO_ARRAY(CUBAN800)
					ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
					ADD_MODEL_REQUEST_TO_ARRAY(TANKER)
					ADD_MODEL_REQUEST_TO_ARRAY(GBURRITO)
					ADD_MODEL_REQUEST_TO_ARRAY(G_M_Y_LOST_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_RON))
					ADD_MODEL_REQUEST_TO_ARRAY(PROP_GAS_TANK_02A)
					ADD_MODEL_REQUEST_TO_ARRAY(BLAZER)
					//REQUEST_PTFX_ASSET()
					iPreStreamStage++
				ENDIF
			ENDIF

		BREAK
		
		CASE 1
			IF eStageToLoad <> MISSION_STAGE_DRIVE OR eStageToLoad <> MISSION_STAGE_SETUP
				IF ARE_REQUESTED_MODELS_LOADED()
					iPreStreamStage++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
		
			IF eStageToLoad = MISSION_STAGE_SNIPE
				IF iProgress > 1
				AND iProgress < 7
				
					WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON_PLANE,<< 1733.3765, 3320.3232, 41.7312 >>, 183.4859)
					OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<< 1573.97, 3221.91, 41.08 >>  ,106.07)
						WAIT(0)
					ENDWHILE

					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[2].veh,FALSE)
					ENDIF
					
//					CREATE_ENEMY_VEHICLE(6,GBURRITO, << 1763.4292, 3302.2083, 40.1561 >> ,165.8189 ,G_M_Y_LOST_01)
//					CREATE_ENEMY_VEHICLE(8,GBURRITO, << 1734.25, 3286.34, 40.97 >>  , 80.91 ,G_M_Y_LOST_01)
					
					CREATE_CRATES()
					CLEANUP_LOADED_MODEL_ARRAY()
					iPreStreamStage++
				ENDIF
			ENDIF

		BREAK
		
		CASE 3
			IF eStageToLoad = MISSION_STAGE_SNIPE
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_HAULER_AND_TANKER,<< 0,0,0 >> ,0)
					WAIT(0)
				ENDWHILE
				RETURN TRUE
			ELSE
				RETURN TRUE
			ENDIF

			
		BREAK
		
	ENDSWITCH

	
	RETURN FALSE
	
ENDFUNC



/// PURPOSE:
///    Creates an object and returns TRUE if such object was created successfully.
/// PARAMS:
///    osObject - OBJECT_STRUCT containing model, position and rotation of the object.
///    bFreezeObject - Specify if this object's position should be frozen.
/// RETURNS:
///    TRUE if object was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_OBJECT_BEEN_CREATED(OBJECT_SNIPE_STRUCT &osObject, BOOL bFreezeObject = FALSE)

	IF NOT DOES_ENTITY_EXIST(osObject.ObjectIndex)
		
		REQUEST_MODEL(osObject.ModelName)
		
		IF HAS_MODEL_LOADED(osObject.ModelName)

			osObject.ObjectIndex = CREATE_OBJECT(osObject.ModelName, osObject.vPosition)
			SET_ENTITY_COORDS_NO_OFFSET(osObject.ObjectIndex, osObject.vPosition)
			SET_ENTITY_ROTATION(osObject.ObjectIndex, osObject.vRotation)
			FREEZE_ENTITY_POSITION(osObject.ObjectIndex, bFreezeObject)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(osObject.ModelName)
		
		ENDIF
	
	ENDIF
	
	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object with model ", GET_MODEL_NAME_FOR_DEBUG(osObject.ModelName), " at coordinates ", osObject.vPosition, ".")
		#ENDIF
		
		RETURN TRUE
	
	ENDIF

	RETURN FALSE

ENDFUNC

//PURPOSE: this is used to load a specific mission stage, for both restart and j/p skips.
PROC LOAD_MISSION_STAGE(MISSION_STAGE_ENUM eStageToLoad) 

	TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
	
	IF NOT bcleanup //first cleans up all existing mission assets
		IF eStageToLoad <> MISSION_STAGE_OPENING_CUTSCENE
			CONTROL_FADE_OUT(500)
			MISSION_CLEANUP(FALSE)
		ENDIF
		bcleanup = TRUE
	ENDIF
	
	IF DOES_BLIP_EXIST(veh_blip)
		REMOVE_BLIP(veh_blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(dest_blip)
		REMOVE_BLIP(dest_blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(ped_blip)
		REMOVE_BLIP(ped_blip)
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_MISSION_FAIL")
		IF TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
			SET_LABEL_AS_TRIGGERED("TRV2_MISSION_FAIL",TRUE)
			PRINTSTRING("KILL THE MUSIC")PRINTNL()
		ENDIF
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		WASH_DECALS_IN_RANGE(<< 1701.4246, 3303.7739, 40.1640 >>,1000,1)
		REMOVE_DECALS_IN_RANGE(<< 1701.4246, 3303.7739, 40.1640 >>,1000) 
		REMOVE_ALL_PICKUPS_OF_TYPE(PICKUP_WEAPON_ASSAULTRIFLE)
		CLEAR_AREA(<<1734.2261, 3315.7761, 40.2237>>,1000,TRUE,FALSE)
		CLEAR_AREA(<< 1701.4246, 3303.7739, 40.1640 >>,1000,TRUE,FALSE)
		CLEAR_AREA(<< 1575.8,3362.9,49.2 >>,1000,TRUE,FALSE)
		CLEAR_AREA(<< 1995.7250, 3812.2937, 31.2603 >>,1000,TRUE,FALSE)
		CLEAR_AREA(<<1694.6978, 3315.1265, 40.2384>>,1000,TRUE,FALSE)
	ENDIF

	SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	CLEAR_MISSION_LOCATION_BLIP(sLocatesData)
	CLEAR_PED_WETNESS(PLAYER_PED_ID())
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	MANAGE_PLAYER_WEAPONS()
	CLEAR_TRIGGERED_LABELS()
	RESET_MISSION_FLAGS()
	
	IF eStageToLoad > MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
	ENDIF
	
	iNuke = 0
	
	SWITCH iSetupProgress // loads the particular requested stages resources and sets up the stage

		CASE 0
			IF eStageToLoad > MISSION_STAGE_DRIVE
			AND eStageToLoad < MISSION_STAGE_FLIGHT
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
			ENDIF
			
			WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				WAIT(0)
			ENDWHILE
			
			iAlarmFarSoundID 	= GET_SOUND_ID()
			iAlarmCloseSoundID 	= GET_SOUND_ID()
			
			MANAGE_PED_OUTFIT()
			
			CLEANUP_PED(psron, TRUE, FALSE)
			CLEANUP_PED_GROUP(psEnemies,  TRUE, FALSE)
			CLEANUP_PED_GROUP(psAlertedEnemies,  TRUE, FALSE)
			CLEANUP_PED_GROUP(psMaverickEnemies,  TRUE, FALSE)
			
			CLEANUP_VEHICLE_GROUP(vsEnemyVans,  TRUE, FALSE)
			CLEANUP_VEHICLE_GROUP(vsEnemyBikes,  TRUE, FALSE)
			
			CLEANUP_OBJECT(osPropPhone.ObjectIndex,  TRUE)
			CLEANUP_OBJECT(osPropStickyBomb.ObjectIndex,  TRUE)
			CLEANUP_OBJECT(osPropGasTanks[0].ObjectIndex,  TRUE)
//			CLEANUP_OBJECT(osPropGasTanks[1].ObjectIndex,  TRUE)
			
			IF DOES_SCENARIO_GROUP_EXIST("GRAPESEED_PLANES")
				SET_SCENARIO_GROUP_ENABLED("GRAPESEED_PLANES",FALSE)
			ENDIF
			
			IF DOES_SCENARIO_GROUP_EXIST("SANDY_PLANES")
				SET_SCENARIO_GROUP_ENABLED("SANDY_PLANES",FALSE)
			ENDIF

			b_is_jumping_directly_to_stage = TRUE
			INITALISE_ARRAYS()
			IF eStageToLoad = MISSION_STAGE_OPENING_CUTSCENE
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
					REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
				ENDIF
				IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
					REMOVE_WEAPON_COMPONENT_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				ENDIF
				
				IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
					REMOVE_WEAPON_COMPONENT_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
				ENDIF
				REQUEST_CUTSCENE("trevor_2_int")
				iSetupProgress++
			ELIF eStageToLoad = MISSION_STAGE_DRIVE
				
				FORCE_SHOP_RESET(GUN_SHOP_02_SS)
				
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
					REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
				ENDIF
				IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
					REMOVE_WEAPON_COMPONENT_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				ENDIF
				
				IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
					REMOVE_WEAPON_COMPONENT_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
				ENDIF
				
				psRon.vPosition =  <<1980.27, 3815.93, 31.3560>>  
				psRon.fHeading =   -94.68
				CLEANUP_LOADED_MODEL_ARRAY()
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON, <<1980.27, 3815.93, 31.3560>>,-94.68)
					WAIT(0)
				ENDWHILE
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON_QUAD_START, <<1990.40, 3810.90, 31.73>>, 112.13)
					WAIT(0)
				ENDWHILE
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_QUAD, <<1987.89, 3811.75, 31.63>>, 116.00)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[12].veh)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[12].veh)
							//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[12].veh)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
							//SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1981.90, 3816.16, 31.3560 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 241.4918)
				ENDIF
				iSetupProgress++
			ELIF eStageToLoad = MISSION_STAGE_SNIPE
				INITIALISE_ARRAYS_FOR_AIRFIELD()
				
				iAlarmFarSoundID 	= GET_SOUND_ID()
				iAlarmCloseSoundID 	= GET_SOUND_ID()
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON_QUAD, <<1668.9028, 3390.7905, 36.8178>>, 256.2998)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_RON, <<1665.9028, 3390.7905, 36.8178>>, 256.2998)
					WAIT(0)
				ENDWHILE
				
				CREATE_CRATES()
					
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 40, TRUE, TRUE)
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_SCOPE_MAX)
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_AR_SUPP_02)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
							SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
						ENDIF
					ENDIF
				ENDIF
				
				FailFlags[BUDDY_DEAD] 		= TRUE
				FailFlags[ENEMY_ALERTED] 	= TRUE
				
				SET_WEATHER_TYPE_NOW_PERSIST("OVERCAST")
				//SET_TIMECYCLE_MODIFIER("nervousRON_fog")
				SET_CLOCK_TIME(0,00,00)
				iSetupProgress++
			ELIF eStageToLoad = MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
				INITIALISE_ARRAYS_FOR_AIRFIELD()
				psRon.vPosition = 	 << 1682.53, 3298.15, 41.10 >>  
				psRon.fHeading =   -31.24
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,  << 1682.53, 3298.15, 41.10 >> , -31.24)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_QUAD, <<1579.5701, 3358.9519, 36.5113>>, 235.9300)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_RON_QUAD, <<1725.9973, 3367.5498, 39.0636>>, 237.5522)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_HAULER_AND_TANKER, <<0,0,0>>,0)
					WAIT(0)
				ENDWHILE
				IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osPropGasTanks[0], TRUE)
//				AND HAS_MISSION_OBJECT_BEEN_CREATED(osPropGasTanks[1], TRUE)
					REQUEST_MODEL(MAVERICK)
					IF HAS_MODEL_LOADED(MAVERICK)
						IF NOT DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
							mission_veh[MV_MAVERICK].veh = CREATE_VEHICLE(MAVERICK, <<1708.6650, 3349.9604, 39.7627>>, 93.9915)
						ELSE
							IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
								FREEZE_ENTITY_POSITION(mission_veh[MV_MAVERICK].veh,TRUE)
							ENDIF
							SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
							iSetupProgress++
						ENDIF
					ENDIF
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_STEAL_PLANE OR eStageToLoad = MISSION_STAGE_PLANE_WING

				CLEANUP_LOADED_MODEL_ARRAY()
				ADD_MODEL_REQUEST_TO_ARRAY(CUBAN800)
				ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
				ADD_MODEL_REQUEST_TO_ARRAY(TANKER)
				ADD_MODEL_REQUEST_TO_ARRAY(GBURRITO)
				ADD_MODEL_REQUEST_TO_ARRAY(HEXER)
				ADD_MODEL_REQUEST_TO_ARRAY(G_M_Y_LOST_01)
				ADD_MODEL_REQUEST_TO_ARRAY(PROP_GAS_TANK_02A)
				IF eStageToLoad = MISSION_STAGE_STEAL_PLANE
					ADD_MODEL_REQUEST_TO_ARRAY(BLAZER)
				ENDIF
				CLEAR_AREA(<<1734.2261, 3315.7761, 40.2237>>,1000,TRUE,FALSE)
				CLEAR_AREA(<<1556.0183, 3223.9409, 39.4842>>,300,TRUE)
				IF eStageToLoad = MISSION_STAGE_STEAL_PLANE OR eStageToLoad = MISSION_STAGE_PLANE_WING
					IF eStageToLoad = MISSION_STAGE_PLANE_WING
						IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_QUAD].veh)
							DELETE_VEHICLE(mission_veh[MV_TREVOR_QUAD].veh)
						ENDIF
						REQUEST_VEHICLE_RECORDING(2,"RCSJC")
						CLEAR_AREA(<<1556.0183, 3223.9409, 39.4842>>,300,TRUE)
						REQUEST_ANIM_DICT("misstrevor2ig_9b")
						REQUEST_PTFX_ASSET()
						REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE,ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS),WEAPON_COMPONENT_FLASH)
					ENDIF
					ADD_MODEL_REQUEST_TO_ARRAY(MAVERICK)
				ENDIF
				REQUEST_PTFX_ASSET()	
				iSetupProgress++
			ELIF eStageToLoad = MISSION_STAGE_FLIGHT
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					REQUEST_ANIM_DICT("misstrevor2ig_11")
					CLEANUP_LOADED_MODEL_ARRAY()
					CLEAR_AREA(<<1556.0183, 3223.9409, 39.4842>>,300,TRUE)
					ADD_MODEL_REQUEST_TO_ARRAY(CUBAN800)
					ADD_MODEL_REQUEST_TO_ARRAY(G_M_Y_LOST_01)
					ADD_CARREC_REQUEST_TO_ARRAY(3,"RCSJC")
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_TO_AIRSTRIP
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					CLEANUP_LOADED_MODEL_ARRAY()
					CLEAR_AREA(<<1556.0183, 3223.9409, 39.4842>>,300,TRUE)

					ADD_MODEL_REQUEST_TO_ARRAY(CUBAN800)
					ADD_MODEL_REQUEST_TO_ARRAY(G_M_Y_LOST_01)
					ADD_CARREC_REQUEST_TO_ARRAY(3,"RCSJC")
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					iSetupProgress++
	
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_CLOSING_CUTSCENE
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					CLEANUP_LOADED_MODEL_ARRAY()
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<2136.9070, 4782.5830, 39.9702>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),80.2803)
					ADD_MODEL_REQUEST_TO_ARRAY(CUBAN800)
					PRINTSTRING("JUMPING TO FINAL CUTSCENE")
					PRINTNL()
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_TEST_STAGE
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					CLEANUP_LOADED_MODEL_ARRAY()
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<1752.5504, 3290.1565, 40.1121>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),109.6693)
					ADD_MODEL_REQUEST_TO_ARRAY(CUBAN800)
					PRINTSTRING("JUMPING TO FINAL CUTSCENE")
					PRINTNL()
					iSetupProgress++
				ENDIF
			ENDIF
			
		BREAK


		CASE 1  // checks resources loaded
			IF eStageToLoad = MISSION_STAGE_OPENING_CUTSCENE
				iSetupProgress++
			ENDIF

			IF eStageToLoad = MISSION_STAGE_SETUP OR eStageToLoad = MISSION_STAGE_DRIVE
				IF ARE_REQUESTED_CARRECS_LOADED("RCSJC") 
					IF ARE_REQUESTED_MODELS_LOADED()
						IF eStageToLoad = MISSION_STAGE_DRIVE 
							iSetupProgress++
						ELSE
							iSetupProgress++
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF ARE_REQUESTED_CARRECS_LOADED("RCSJC") 
					IF ARE_REQUESTED_MODELS_LOADED()
						IF eStageToLoad = MISSION_STAGE_DRIVE 
							iSetupProgress++
						ELIF eStageToLoad = MISSION_STAGE_FLIGHT
							iSetupProgress++
						ELIF eStageToLoad = MISSION_STAGE_SNIPE 
							iSetupProgress++
						ELIF eStageToLoad = MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
							iSetupProgress++
						ELIF eStageToLoad = MISSION_STAGE_STEAL_PLANE
							IF HAS_PTFX_ASSET_LOADED()
								IF IS_VEHICLE_DRIVEABLE(mission_veh[10].veh)
									SET_ENTITY_INVINCIBLE(mission_veh[10].veh,FALSE)
									SET_ENTITY_HEALTH(mission_veh[10].veh,0)
									ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[10].veh),EXP_TAG_PETROL_PUMP)
									EXPLODE_VEHICLE(mission_veh[10].veh)
								ENDIF
								iSetupProgress++
							ENDIF
						ELIF eStageToLoad = MISSION_STAGE_PLANE_WING
							IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
							AND HAS_ANIM_DICT_LOADED("misstrevor2ig_9b")
							AND HAS_PTFX_ASSET_LOADED()
								iSetupProgress++
							ENDIF
						ELIF eStageToLoad = MISSION_STAGE_TO_AIRSTRIP
							iSetupProgress++
						ELIF eStageToLoad = MISSION_STAGE_CLOSING_CUTSCENE
							iSetupProgress++
						ELIF eStageToLoad = MISSION_STAGE_TEST_STAGE
							iSetupProgress++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK

		CASE 2  // creates assets
			IF eStageToLoad = MISSION_STAGE_OPENING_CUTSCENE
				eMissionStage = MISSION_STAGE_OPENING_CUTSCENE
			ELIF eStageToLoad = MISSION_STAGE_DRIVE

				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ELSE
					LOAD_ALL_OBJECTS_NOW()
					WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
						WAIT(0)
					ENDWHILE
					LOAD_SCENE(<<1985.1227, 3814.1492, 31.2009>>)	
				ENDIF
				
				eMissionStage = MISSION_STAGE_DRIVE
				
			ELIF eStageToLoad = MISSION_STAGE_SNIPE 
					
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON_QUAD_SNIPE, <<1668.9028, 3390.7905, 36.8178>>, 256.2998)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_QUAD, << 1582.0737, 3365.7207, 36.4635 >>, 247.3211)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<< 1582.5380, 3360.7068, 36.6835 >>, 37.5630)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_HAULER_AND_TANKER, <<0,0,0>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<< 1573.97, 3221.91, 41.08 >>  ,106.07)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[2].veh,FALSE)
				ENDIF
				
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,iMAX(25, GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)),TRUE)
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				ELSE
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,TRUE)
				ENDIF
				
				MANAGE_PED_OUTFIT()
				CREATE_CRATES()
				
				IF NOT IS_REPLAY_BEING_SET_UP()
					LOAD_ALL_OBJECTS_NOW()
					WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
						WAIT(0)
					ENDWHILE
					LOAD_SCENE(<<1575.0496, 3363.2942, 47.6350>>)
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 1575.9034, 3362.8730, 47.6349 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),245.1024)
				ENDIF

				//MUSIC
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_SNIPE_RESTART")
					IF TRIGGER_MUSIC_EVENT("TRV2_SNIPE_RESTART")
						SET_LABEL_AS_TRIGGERED("TRV2_SNIPE_RESTART",TRUE)
					ENDIF
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ELSE
					
				ENDIF
				eMissionStage = MISSION_STAGE_SNIPE	
			ELIF eStageToLoad = MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_TOWER_RESTART")
					IF TRIGGER_MUSIC_EVENT("TRV2_TOWER_RESTART")
						SET_LABEL_AS_TRIGGERED("TRV2_TOWER_RESTART",TRUE)
					ENDIF
				ENDIF
				psRon.vPosition = 	 << 1682.53, 3298.15, 41.10 >>  
				psRon.fHeading =   -31.24
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<< 1682.53, 3298.15, 41.10 >>  ,-31.24)
					WAIT(0)
				ENDWHILE
				CREATE_CRATES()
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ELSE
				
				ENDIF
				END_REPLAY_SETUP()
				eMissionStage = MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
			ELIF eStageToLoad = MISSION_STAGE_STEAL_PLANE OR eStageToLoad = MISSION_STAGE_PLANE_WING
				psRon.vPosition = 	 << 1682.53, 3298.15, 41.10 >>  
				psRon.fHeading =   -31.24
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON_PLANE,<< 1733.3765, 3320.3232, 41.7312 >>, 183.4859)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<< 2474.0779, 3477.0808, 48.1862 >>  ,191.3157)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<< 1682.53, 3298.15, 41.10 >> ,-31.24)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_HAULER_AND_TANKER, <<0,0,0>>,0)
					WAIT(0)
				ENDWHILE
			
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[1].veh,FALSE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[2].veh,FALSE)
				ENDIF
			
				CREATE_ENEMY_VEHICLE(7,HEXER,<< 1744.5172, 3271.1912, 40.1741 >>, 82.2751 ,G_M_Y_LOST_01)
				
				IF DOES_ENTITY_EXIST(mission_veh[8].veh)
					DELETE_VEHICLE(mission_veh[8].veh)
				ENDIF
				
				CLEAR_AREA(<<1694.6978, 3315.1265, 40.2384>>,100,TRUE,FALSE)
				IF NOT DOES_ENTITY_EXIST(mission_veh[8].veh)
					CREATE_ENEMY_VEHICLE(8,GBURRITO, << 1698.52, 3313.73, 40.97 >>  ,21.63 ,G_M_Y_LOST_01)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[8].veh, TRUE)
				ENDIF
				CREATE_ENEMY_VEHICLE(6,GBURRITO, << 1745.71, 3275.38, 40.94 >>  , 133.23 ,G_M_Y_LOST_01)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[6].veh, TRUE)
				CREATE_ENEMY_VEHICLE(10,MAVERICK,<< 1706.7020, 3322.2422, 40.1486 >>, 311.7382,G_M_Y_LOST_01,TRUE,10)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[10].veh,TRUE)
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					SET_ENTITY_COORDS(mission_veh[2].veh,<< 1564.5811, 3225.4727, 39.4794 >> )
					SET_ENTITY_HEADING(mission_veh[2].veh, 99.5075    )
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[2].veh,FALSE)
				ENDIF
				
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),<< 1583.8245, 3357.3999, 36.6356 >>  )
				SET_ENTITY_HEADING(PLAYER_PED_ID(),232.1282 )

				MANAGE_PED_OUTFIT()
				CREATE_CRATES()
				IF DOES_ENTITY_EXIST(crate[1])
					SET_ENTITY_INVINCIBLE(crate[1], FALSE)
					SET_ENTITY_PROOFS(crate[1], FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
				
				IF eStageToLoad = MISSION_STAGE_STEAL_PLANE 
					WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_QUAD, << 1601.9093, 3351.9688, 37.3035 >>, 249.6865)
						WAIT(0)
					ENDWHILE
				
					IF IS_VEHICLE_DRIVEABLE(mission_veh[12].veh)
						SET_ENTITY_COORDS(mission_veh[12].veh,<< 1601.9093, 3351.9688, 37.3035 >>)
						SET_ENTITY_HEADING(mission_veh[12].veh, 249.6865 )
					ENDIF
					
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						SET_PED_DUCKING(psRon.pedIndex,TRUE)
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_STEAL_PLANE_RESTART")
						IF TRIGGER_MUSIC_EVENT("TRV2_STEAL_PLANE_RESTART")
							SET_LABEL_AS_TRIGGERED("TRV2_STEAL_PLANE_RESTART",TRUE)
						ENDIF
					ENDIF
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(mission_veh[12].veh)
					ELSE
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[12].veh)
							IF IS_VEHICLE_SEAT_FREE(mission_veh[12].veh)
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[12].veh,TRUE)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[12].veh)
							ENDIF
						ENDIF
						LOAD_ALL_OBJECTS_NOW()
						WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
							WAIT(0)
						ENDWHILE
						LOAD_SCENE(<< 1601.9093, 3351.9688, 37.3035 >>)
					ENDIF
					IF NOT IS_ENTITY_DEAD(mission_veh[10].veh)
						SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(mission_veh[10].veh,FALSE)
						SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[10].veh)
						PTFXfire[1] =START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev2_heli_wreck",GET_ENTITY_COORDS(mission_veh[10].veh,FALSE),<<0,0,0>>)
						SET_ENTITY_HEALTH(mission_veh[10].veh,0)
						ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[10].veh),EXP_TAG_PETROL_PUMP)
						EXPLODE_VEHICLE(mission_veh[10].veh)
					ENDIF
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_TOWER_RESTART")
						IF TRIGGER_MUSIC_EVENT("TRV2_TOWER_RESTART")
							SET_LABEL_AS_TRIGGERED("TRV2_TOWER_RESTART",TRUE)
						ENDIF
					ENDIF
					eMissionStage = MISSION_STAGE_STEAL_PLANE	
				ELIF eStageToLoad = MISSION_STAGE_PLANE_WING
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(2,"RCSJC")
						WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<< 1678.8859, 3285.7861, 39.9321 >>,41.6770)
							WAIT(0)
						ENDWHILE
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) AND NOT IS_PED_INJURED(psRon.pedIndex)
							IF IS_VEHICLE_SEAT_FREE(mission_veh[1].veh)
								SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
							ENDIF
						ENDIF

						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_WING_RESTART")
							IF TRIGGER_MUSIC_EVENT("TRV2_WING_RESTART")
								SET_LABEL_AS_TRIGGERED("TRV2_WING_RESTART",TRUE)
							ENDIF
						ENDIF
						iNuke = 0
						IF IS_REPLAY_BEING_SET_UP()
							END_REPLAY_SETUP()
						ELSE
							LOAD_ALL_OBJECTS_NOW()
							WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
								WAIT(0)
							ENDWHILE
							
							LOAD_SCENE(<<1732.3949, 3319.7141, 40.2235>>)
						ENDIF
						IF NOT IS_ENTITY_DEAD(mission_veh[10].veh)
							SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(mission_veh[10].veh,FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[10].veh)
							PTFXfire[1] =START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev2_heli_wreck",GET_ENTITY_COORDS(mission_veh[10].veh,FALSE),<<0,0,0>>)
							SET_ENTITY_HEALTH(mission_veh[10].veh,0)
							ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[10].veh),EXP_TAG_PETROL_PUMP)
							EXPLODE_VEHICLE(mission_veh[10].veh)
						ENDIF
						
						eMissionStage = MISSION_STAGE_PLANE_WING
					ENDIF
				ENDIF

			ELIF eStageToLoad = MISSION_STAGE_FLIGHT
			
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<< 1678.8859, 3285.7861, 39.9321 >>,41.6770)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_RON_PLANE,<< 1677.0358, 3245.1191, 41.0376 >> , 101.6620)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<<842.7066, 3217.6802, 37.4018>>, 65.2580)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					FREEZE_ENTITY_POSITION(mission_veh[2].veh,TRUE)
				ENDIF
				
				MANAGE_PED_OUTFIT()
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
						START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[1].veh,3,"RCSJC",ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY))
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(mission_veh[MV_RON_PLANE].veh,TRUE)
					ENDIF
					//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,13218.307)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,23118.307)
					CONTROL_LANDING_GEAR(mission_veh[1].veh,LGC_RETRACT_INSTANT)
					SET_VEHICLE_ENGINE_ON(mission_veh[1].veh,TRUE,TRUE)
					SET_HELI_BLADES_FULL_SPEED(mission_veh[1].veh)
					IF DOES_ENTITY_EXIST(driver[1].ped)
						DELETE_PED(driver[1].ped)
					ENDIF
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_SEAT_FREE(mission_veh[1].veh)
							SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
						ENDIF
					ENDIF
				ENDIF
	
				CREATE_ENEMIES(27,27,FALSE)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_HEAD, 		0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_BERD, 		1, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_HAIR, 		0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_TORSO, 		0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_LEG, 		0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_HAND, 		0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_FEET, 		0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_TEETH, 		0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_SPECIAL, 	0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_SPECIAL2, 	0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_DECL, 		0, 0)
				SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_JBIB, 		0, 0)
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					FREEZE_ENTITY_POSITION(mission_veh[2].veh,FALSE)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[2].veh)
						START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[2].veh,3,"RCSJC",ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY))
					ENDIF
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[2].veh,18218.307)
					//FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(mission_veh[2].veh)
					IF DOES_ENTITY_EXIST(driver[2].ped)
						DELETE_PED(driver[2].ped)
					ENDIF
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_VEHICLE_ENGINE_ON(mission_veh[2].veh,TRUE,TRUE)
						SET_HELI_BLADES_FULL_SPEED(mission_veh[2].veh)
						CONTROL_LANDING_GEAR(mission_veh[2].veh,LGC_RETRACT_INSTANT)
					ENDIF
				ENDIF
				
				//MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[1].veh,<<0,0,0>>)
				iTimer = GET_GAME_TIMER()
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_FLY_RESTART")
					IF TRIGGER_MUSIC_EVENT("TRV2_FLY_RESTART")
						SET_LABEL_AS_TRIGGERED("TRV2_FLY_RESTART",TRUE)
					ENDIF
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[2].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
						ENDIF
					ENDIF
					LOAD_SCENE(<<842.7066, 3217.6802, 37.4018>>)
					//NEW_LOAD_SCENE_START(<<842.7066, 3217.6802, 37.40185>>,NORMALISE_VECTOR(<<-209.9863, 3786.8999, 144.9487>>- <<842.7066, 3217.6802, 37.4018>>),100)
				ENDIF

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				eMissionStage = MISSION_STAGE_FLIGHT 
			ELIF eStageToLoad = MISSION_STAGE_TO_AIRSTRIP
			
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<< 1678.8859, 3285.7861, 39.9321 >>,41.6770)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_RON_PLANE,<< 1677.0358, 3245.1191, 41.0376 >> , 101.6620)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<<-3353.6501, 2931.2129, 37.9603>>, 177.9270)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					FREEZE_ENTITY_POSITION(mission_veh[2].veh,TRUE)
				ENDIF
				
				MANAGE_PED_OUTFIT()
				
				//MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[1].veh,<<0,0,0>>)
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV2_RACE_RESTART")
					IF TRIGGER_MUSIC_EVENT("TRV2_RACE_RESTART")
						SET_LABEL_AS_TRIGGERED("TRV2_RACE_RESTART",TRUE)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
						START_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,3,"RCSJC", FALSE)
					ENDIF
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,129981)
					CONTROL_LANDING_GEAR(mission_veh[1].veh,LGC_RETRACT_INSTANT)
					IF DOES_ENTITY_EXIST(driver[1].ped)
						DELETE_PED(driver[1].ped)
					ENDIF
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_SEAT_FREE(mission_veh[1].veh)
							SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					FREEZE_ENTITY_POSITION(mission_veh[2].veh,FALSE)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[2].veh)
						START_PLAYBACK_RECORDED_VEHICLE(mission_veh[2].veh,3,"RCSJC", FALSE)
					ENDIF
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[2].veh,126981)
					//FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(mission_veh[2].veh)
					IF DOES_ENTITY_EXIST(driver[2].ped)
						DELETE_PED(driver[2].ped)
					ENDIF
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_VEHICLE_ENGINE_ON(mission_veh[2].veh,TRUE,TRUE)
						SET_HELI_BLADES_FULL_SPEED(mission_veh[2].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
						
						CONTROL_LANDING_GEAR(mission_veh[2].veh,LGC_RETRACT_INSTANT)
					ENDIF
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[2].veh)
				ELSE
					IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
					ENDIF
					LOAD_SCENE(<<-3353.6501, 2931.2129, -89.9603>>)
					NEW_LOAD_SCENE_START(<<-3353.6501, 2931.2129, -89.9603>>,NORMALISE_VECTOR(<<-1500.0160, 2630.5645, 133.0806>>- <<-3353.6501, 2931.2129, -89.9603>>),100)
				ENDIF
				
				PRINTSTRING("SCRIPTS COMPILING")PRINTNL()
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				eMissionStage = MISSION_STAGE_TO_AIRSTRIP
			ELIF eStageToLoad = MISSION_STAGE_CLOSING_CUTSCENE
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<< 1678.8859, 3285.7861, 39.9321 >>,41.6770)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_RON_PLANE,<< 1677.0358, 3245.1191, 41.0376 >> , 101.6620)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<<2116.5193, 4798.7222, 40.0507>>  ,191.3157)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					CONTROL_LANDING_GEAR(mission_veh[MV_TREVOR_PLANE].veh,LGC_DEPLOY_INSTANT)
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[2].veh)
					SET_ENTITY_INVINCIBLE(mission_veh[2].veh,TRUE)
				ENDIF
				
				PRINTSTRING("SKIPPING TO CLOSING CUTSCENE")PRINTNL()

				MANAGE_PED_OUTFIT()
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TRV2_MCS_8",CS_SECTION_1| CS_SECTION_2)
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ELSE
					LOAD_SCENE(<<2136.9070, 4782.5830, 39.9702>>)
				ENDIF
				eMissionStage = MISSION_STAGE_CLOSING_CUTSCENE
			ELIF eStageToLoad = MISSION_STAGE_TEST_STAGE
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON_PLANE, <<1987.050171,3810.564209,32.100273>>,-166.157623)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<< 2474.0779, 3477.0808, 48.1862 >>  ,191.3157)
					WAIT(0)
				ENDWHILE
					
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<< 1678.8859, 3285.7861, 39.9321 >>,41.6770)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) AND NOT IS_PED_INJURED(psRon.pedIndex)
					IF IS_VEHICLE_SEAT_FREE(mission_veh[1].veh)
						SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
					ENDIF
				ENDIF
				
				LOAD_ALL_OBJECTS_NOW()
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					WAIT(0)
				ENDWHILE
				
				LOAD_SCENE(<<1732.3949, 3319.7141, 40.2235>>)
				
				eMissionStage = MISSION_STAGE_TEST_STAGE	
			ENDIF
			
			IF NOT IS_PED_INJURED(psRon.pedIndex)
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, psRon.pedIndex, "NervousRon")
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
			//CLEANUP_LOADED_MODEL_ARRAY()
			CLEAR_PED_WETNESS(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
			iprogress = 0
			bmissionstageloaded = TRUE
			brunfailchecks = TRUE
			iSetupProgress = 0
			bSkipped = TRUE
			#IF IS_DEBUG_BUILD
				bskipping = FALSE
			#ENDIF	

		BREAK

	ENDSWITCH

ENDPROC

INT iSkipCutsceneTimer 

PROC OPENING_CUTSCENE()

	bcutsceneplaying = TRUE
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0,0)
	
	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
	#ENDIF
	
	IF e_section_stage = SECTION_STAGE_SETUP
		PRINTSTRING("!!!e_section_stage = SECTION_STAGE_SETUP")
		IF b_is_jumping_directly_to_stage
			b_is_jumping_directly_to_stage = FALSE
		ELSE
			IF i_current_event = 0
				bcutsceneplaying = TRUE
				bClearCutscenArea = FALSE
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_DESTROYED)
				REQUEST_CUTSCENE("trevor_2_int")
				SETTIMERA(0)
				
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0],TRUE,TRUE)
						psRon.pedIndex = g_sTriggerSceneAssets.ped[0]
						SET_PED_CAN_BE_TARGETTED(psRon.pedIndex, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(psRon.pedIndex, RELGROUPHASH_PLAYER)
						ADD_PED_FOR_DIALOGUE(sSpeech,3,psRon.pedIndex,"NervousRon")
						IF NOT IS_PED_INJURED(psRon.pedIndex)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(psRon.pedIndex,KNOCKOFFVEHICLE_NEVER)
						ENDIF
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psRon.pedIndex, TRUE)
						GIVE_WEAPON_TO_PED(psRon.pedIndex, WEAPONTYPE_UNARMED , 2000, TRUE)
						SET_PED_CAN_BE_TARGETTED(psRon.pedIndex, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(psRon.pedIndex ,rel_group_buddies)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,rel_group_buddies)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
						SET_PED_CONFIG_FLAG(psRon.pedIndex,PCF_RunFromFiresAndExplosions,FALSE)
						SET_PED_AS_ENEMY(psRon.pedIndex , FALSE)
						SET_ENTITY_AS_MISSION_ENTITY(psRon.pedIndex)
						SET_PED_DEFAULT_COMPONENT_VARIATION(psRon.pedIndex)
						SET_PED_ACCURACY(psRon.pedIndex ,100)
						SET_PED_COMBAT_ABILITY(psRon.pedIndex ,CAL_PROFESSIONAL)
						SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(psRon.pedIndex,FALSE)
						SET_ENTITY_HEALTH(psRon.pedIndex,300)
						SET_PED_SUFFERS_CRITICAL_HITS(psRon.pedIndex,TRUE)
						
						IF NOT IS_PED_INJURED(psRon.pedIndex)
							SET_PED_COMP_ITEM_CURRENT_SP(psRon.pedIndex, COMP_TYPE_PROPS, PROPS_P2_HEADSET,FALSE)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HEAD, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_BERD, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAIR, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TORSO, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_LEG, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAND, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_FEET, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TEETH, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL2, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_DECL, 0, 0)
							SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_JBIB, 0, 0)
							SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_HEAD, 0)
							SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_EYES, 0)
						ENDIF
					ENDIF
				ELSE
					IF NOT DOES_ENTITY_EXIST(psRon.pedIndex)
						WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON, <<1987.050171,3810.564209,32.100273>>,-166.157623)
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
				
				SET_CUTSCENE_FADE_VALUES(FALSE,FALSE,FALSE,FALSE)
				
				SET_PED_POPULATION_BUDGET(0)		
				SET_VEHICLE_POPULATION_BUDGET(0)
				b_skipped_mocap = FALSE
				i_current_event++
			ELIF i_current_event = 1
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(psRon.pedIndex, "Ron", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					//Wait a couple of frames before clearing/warping stuff, so the cutscene is active first.
					//WAIT(0)

					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					IF IS_REPEAT_PLAY_ACTIVE()
						CONTROL_FADE_IN(500)
					ENDIF
					b_skipped_mocap = FALSE
					i_current_event = 0
					e_section_stage = SECTION_STAGE_RUNNING
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
//		IF IS_CUTSCENE_PLAYING()
//			IF bClearCutscenArea = TRUE
//				IF MANAGE_MY_TIMER(iSkipCutsceneTimer,2000)
//				AND GET_CUTSCENE_TIME() < 87047
//					IF NOT b_skipped_mocap
//						IF DOES_ENTITY_EXIST(psRon.pedIndex)
//							IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
//								CONTROL_FADE_OUT(500)
//								STOP_CUTSCENE()
//								WHILE IS_CUTSCENE_ACTIVE()
//									WAIT(0)
//								ENDWHILE
//								b_skipped_mocap = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF IS_CUTSCENE_PLAYING()
			IF bClearCutscenArea = FALSE
				CLEAR_PED_PARACHUTE_PACK_VARIATION(PLAYER_PED_ID())
				//RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<1992.2, 3834, 32.3>>, 207.5433,FALSE)
				//DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<1982.3833, 3807.1899, 31.1531>>,10)
				//RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<1981.9288, 3829.5393, 31.3889>>, 296.4925,FALSE)
				
//				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1974.452881,3808.721680,35.687969>>, <<1989.275757,3816.212158,31.042301>>, 14.250000,<<1980.3944, 3828.6455, 31.4097>>, 295.3727,FALSE)
//				CLEAR_AREA(<< 1986.1279, 3814.4370, 31.2913 >>, 200.0, TRUE,FALSE)
				
				//Vehicle Gen
				VEHICLE_INDEX vehTemp
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	                vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	                PRINTLN("PLAYER IS IN VEHICLE")
				ELSE
	                vehTemp = GET_LAST_DRIVEN_VEHICLE()
	                PRINTLN("PLAYER IS NOT IN VEHICLE")
				ENDIF

				IF DOES_ENTITY_EXIST(vehTemp)
				AND IS_VEHICLE_DRIVEABLE(vehTemp)
	                IF NOT IS_VEHICLE_MODEL(vehTemp, TAXI)
						MODEL_NAMES model = GET_ENTITY_MODEL(vehTemp)
						VECTOR vMin
						VECTOR vMax
						GET_MODEL_DIMENSIONS(model, vMin, vMax)
						FLOAT fHeight = -vMin.z + vMax.z
						
						IF fHeight < 2.6
                        AND VDIST(GET_ENTITY_COORDS(vehTemp), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 50
                            PRINTLN("PLAYER VEHICLE IS NEARBY")
                            SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
							IF IS_ENTITY_AT_COORD(vehTemp,<<1967.1051, 3821.7605, 31.3842>>,<<3,3,3>>)
								SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<1967.1051, 3821.2605, 31.3842>>, 120.7252  )
                                SET_ENTITY_COORDS(vehTemp, <<1967.1051, 3821.2605, 31.3842>>)
                                SET_ENTITY_HEADING(vehTemp, 120.7252)
                                SET_VEHICLE_DOORS_SHUT(vehTemp, TRUE)
                                PRINTLN("SETTING PLAYER VEH AS veh gen In Garage")							
							ELIF NOT IS_POSITION_OCCUPIED( <<1978.1051, 3827.7605, 31.3842>>,5,FALSE,TRUE,FALSE,FALSE,FALSE)
							OR IS_ENTITY_AT_COORD(vehTemp,<<1978.1051, 3827.7605, 31.3842>>,<<5,5,5>>)
								SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<1978.1051, 3827.7605, 31.3842>>, 301.7252  )
                                SET_ENTITY_COORDS(vehTemp, <<1978.1051, 3827.7605, 31.3842>>)
                                SET_ENTITY_HEADING(vehTemp, 301.7252)
                                SET_VEHICLE_DOORS_SHUT(vehTemp, TRUE)
                                PRINTLN("SETTING PLAYER VEH AS veh gen In Driveway")
							ELSE
								SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<-1191.0647, -1504.3447, 3.3697>>, 303.3213)
                                SET_ENTITY_COORDS(vehTemp, <<-1191.0647, -1504.3447, 3.3697>>)
                                SET_ENTITY_HEADING(vehTemp, 303.3213)
                                SET_VEHICLE_DOORS_SHUT(vehTemp, TRUE)
                                PRINTLN("SETTING PLAYER VEH AS veh gen")
							ENDIF
                        ENDIF
	                ENDIF
				ENDIF
				
				CLEAR_AREA(<<-1191.0647, -1504.3447, 3.3697>>,200,TRUE)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
				
				
				iSkipCutsceneTimer = GET_GAME_TIMER()
				STOP_GAMEPLAY_HINT(TRUE)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
					REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
				ENDIF
				IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
					REMOVE_WEAPON_COMPONENT_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				ENDIF
				
				IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
					REMOVE_WEAPON_COMPONENT_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
				ENDIF
				bClearCutscenArea = TRUE
			ENDIF
		ENDIF
		
		IF GET_CUTSCENE_TIME() > 63944
			SETUP_MISSION_REQUIREMENT(REQ_RON_QUAD_START, <<1990.43, 3810.89, 31.79>>, 112.15)
			SETUP_MISSION_REQUIREMENT(REQ_TREVOR_QUAD, <<1987.90, 3811.76, 31.62>>, 116.12)
		ENDIF
	
		IF NOT DOES_ENTITY_EXIST(psRon.pedIndex)
			ENTITY_INDEX entity_ron = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ron")
			IF DOES_ENTITY_EXIST(entity_ron)
				psRon.pedIndex = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_ron)
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		ENDIF

		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
			ENDIF

			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500, 241.5321, FALSE)
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ron")
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
				IF NOT IS_PED_INJURED(psRon.pedIndex)
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						SET_PED_COMP_ITEM_CURRENT_SP(psRon.pedIndex, COMP_TYPE_PROPS, PROPS_P2_HEADSET,FALSE)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_BERD, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_HAND, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_FEET, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_TEETH, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_DECL, 0, 0)
						SET_PED_COMPONENT_VARIATION(psRon.pedIndex, PED_COMP_JBIB, 0, 0)
						SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_HEAD, 0)
						SET_PED_PROP_INDEX(psRon.pedIndex, ANCHOR_EYES, 0)
					ENDIF
					TASK_ENTER_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_WALK)
					FORCE_PED_MOTION_STATE(psRon.pedIndex, MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_CUTSCENE_ACTIVE()
			IF WAS_CUTSCENE_SKIPPED()
				b_skipped_mocap = TRUE
			ENDIF
		ENDIF
		
//		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
//		ENDIF

		IF HAS_CUTSCENE_FINISHED()
		AND bClearCutscenArea = TRUE
			SET_PED_POPULATION_BUDGET(3)		
			SET_VEHICLE_POPULATION_BUDGET(3)
			//PRINTSTRING("!!!HAS_CUTSCENE_FINISHED()e_section_stage = SECTION_STAGE_CLEANUP")
			e_section_stage = SECTION_STAGE_CLEANUP
		ENDIF
	ENDIF

	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TREV2")

	IF e_section_stage = SECTION_STAGE_CLEANUP
		PRINTSTRING("!!!e_section_stage = SECTION_STAGE_CLEANUP")
		//SET_GAMEPLAY_CAM_WORLD_HEADING(-65)

		IF b_skipped_mocap
			PRINTSTRING("!!!MOCAP SKIPPED")PRINTNL()
			//Make sure necessary assets get made.
			IF NOT IS_SCREEN_FADING_OUT()
			OR NOT IS_SCREEN_FADED_OUT()
				CONTROL_FADE_OUT(500)
			ENDIF
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			STOP_CUTSCENE()
			
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON, <<1987.050171,3810.564209,32.100273>>,-166.157623)
				WAIT(0)
			ENDWHILE
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON_QUAD_START, <<1990.43, 3810.89, 31.79>>, 112.15)
				WAIT(0)
			ENDWHILE
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR_QUAD, <<1987.90, 3811.76, 31.62>>, 116.12)
				WAIT(0)
			ENDWHILE
			
			//If the player skipped the mocap the peds need to be warped.
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			
			IF bAddedRelGroups = FALSE
				ADD_RELATIONSHIP_GROUP("BUDDIES", rel_group_buddies)
				ADD_RELATIONSHIP_GROUP("ENEMIES", rel_group_enemies)
				bAddedRelGroups = TRUE
			ENDIF

		ENDIF
		
		REPLAY_STOP_EVENT()
		
		//Setup buddy relationship groups etc.
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_TREVOR_2)
		REMOVE_ANIM_DICT("misstrevor2leadinouttrv_2_int")
		i_current_event = 0
		e_section_stage = SECTION_STAGE_SETUP
		eMissionStage = MISSION_STAGE_DRIVE
		bcutsceneplaying = FALSE
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		STOP_CUTSCENE()
		b_skipped_mocap = TRUE
		
		e_section_stage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

PROC CLEANUP_RUNWAY_VEHICLE_RECORDINGS()
	REMOVE_VEHICLE_RECORDING(1,"RCSJC")
	REMOVE_VEHICLE_RECORDING(2,"RCSJC")
	REMOVE_VEHICLE_RECORDING(7,"RCSJC")
	REMOVE_VEHICLE_RECORDING(9,"RCSJC")
	REMOVE_VEHICLE_RECORDING(13,"RCSJC")
	REMOVE_VEHICLE_RECORDING(23,"RCSJC")
	REMOVE_VEHICLE_RECORDING(24,"RCSJC")
	REMOVE_VEHICLE_RECORDING(25,"RCSJC")
	REMOVE_VEHICLE_RECORDING(14,"RCSJC")
	REMOVE_VEHICLE_RECORDING(15,"RCSJC")
	REMOVE_VEHICLE_RECORDING(16,"RCSJC")
	REMOVE_VEHICLE_RECORDING(17,"RCSJC")
	REMOVE_VEHICLE_RECORDING(32,"RCSJC")
	REMOVE_VEHICLE_RECORDING(33,"RCSJC")
	REMOVE_VEHICLE_RECORDING(34,"RCSJC")
	REMOVE_VEHICLE_RECORDING(35,"RCSJC")
	REMOVE_VEHICLE_RECORDING(36,"RCSJC")
	REMOVE_VEHICLE_RECORDING(37,"RCSJC")
	REMOVE_VEHICLE_RECORDING(38,"RCSJC")
ENDPROC

INT iTimeSinceTrainCreation

PROC CLEANUP_BIKER_ASSETS()
	
	CLEANUP_RUNWAY_VEHICLE_RECORDINGS()
	
	CLEANUP_PED_GROUP(psEnemies,  TRUE, FALSE)
	CLEANUP_PED_GROUP(psAlertedEnemies,  TRUE, FALSE)
	CLEANUP_PED_GROUP(psMaverickEnemies,  TRUE, FALSE)

	CLEANUP_VEHICLE_GROUP(vsEnemyVans,  TRUE, FALSE)
	CLEANUP_VEHICLE_GROUP(vsEnemyBikes,  TRUE, FALSE)

	CLEANUP_OBJECT(osPropPhone.ObjectIndex,  TRUE)
	CLEANUP_OBJECT(osPropStickyBomb.ObjectIndex,  TRUE)
	CLEANUP_OBJECT(osPropGasTanks[0].ObjectIndex,  TRUE)
//	CLEANUP_OBJECT(osPropGasTanks[1].ObjectIndex,  TRUE)
	
	IF DOES_ENTITY_EXIST(LostSniperWeapon)
		DELETE_OBJECT(LostSniperWeapon)
	ENDIF
	
	IF DOES_ENTITY_EXIST(crate[1])
		IF NOT IS_ENTITY_ON_SCREEN(crate[1])
			DELETE_OBJECT(crate[1])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(crate[0])
		IF NOT IS_ENTITY_ON_SCREEN(crate[0])
			DELETE_OBJECT(crate[0])
		ENDIF
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE))
	SET_MODEL_AS_NO_LONGER_NEEDED(GBURRITO)
	SET_MODEL_AS_NO_LONGER_NEEDED(HEXER)
	SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_WALL_LIGHT_02A)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_SNIPERRIFLE))
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_STICKYBOMB))
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT1)
	SET_MODEL_AS_NO_LONGER_NEEDED(TANKERCAR)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCAR)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHT)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_GAS_TANK_02A)
	REMOVE_ANIM_DICT("misstrevor2ron_basic_moves")
	REMOVE_ANIM_DICT("misstrevor2hangar_death")

	IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
		IF NOT IS_ENTITY_ON_SCREEN(vsEnemyVans[0].VehicleIndex)
			DELETE_VEHICLE(vsEnemyVans[0].VehicleIndex)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vsEnemyBikes[0].VehicleIndex)
		IF NOT IS_ENTITY_ON_SCREEN(vsEnemyBikes[0].VehicleIndex)
			DELETE_VEHICLE(vsEnemyBikes[0].VehicleIndex)
		ENDIF
	ENDIF
	
	FOR iCount = 0 TO (MAX_ENEMY_PED-1)
		IF iCount != 27
			IF DOES_ENTITY_EXIST(enemy[iCount].ped)
				IF NOT IS_ENTITY_ON_SCREEN(enemy[iCount].ped)
					DELETE_PED(enemy[iCount].ped)
					SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_01)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(enemy[27].ped)
		IF eBalanceState = STATE_FALL_OFF
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(enemy[27].ped,FALSE)) > 100
				SET_PED_AS_NO_LONGER_NEEDED(enemy[27].ped)
				SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_01)
				REMOVE_ANIM_DICT("misstrevor2ig_11")
			ENDIF
		ENDIF
	ENDIF
	
	FOR iCount = 0 TO (MAX_DRIVER_PED-1)
		IF iCount != 15
		OR iCount != 16
		OR iCount != 17
		OR iCount != 18
			IF DOES_ENTITY_EXIST(driver[iCount].ped)
				IF NOT IS_PED_MODEL(driver[iCount].ped,G_M_Y_MEXGOON_01)
					IF NOT IS_ENTITY_ON_SCREEN(driver[iCount].ped)
						SET_PED_AS_NO_LONGER_NEEDED(driver[iCount].ped)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(objGib)
		DELETE_OBJECT(objGib)
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_RON_QUAD].veh)
			DELETE_VEHICLE(mission_veh[MV_RON_QUAD].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_QUAD].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_TREVOR_QUAD].veh)
			DELETE_VEHICLE(mission_veh[MV_TREVOR_QUAD].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_HAULER].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_HAULER].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_HAULER].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_TANKER].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_TANKER].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_TANKER].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MAVERICK].veh)
			DELETE_VEHICLE(mission_veh[MV_MAVERICK].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[3].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[3].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[3].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[4].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[4].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[4].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[5].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[5].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[5].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[6].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[6].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[6].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[8].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[8].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[8].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[9].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[9].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[9].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[10].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[10].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[10].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[11].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[11].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[11].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[12].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[12].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[12].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[13].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[13].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[13].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[14].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[14].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[14].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[15].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[15].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[15].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[16].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[16].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[16].veh)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[17].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[17].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[17].veh)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[18].veh)
		IF NOT IS_ENTITY_ON_SCREEN(mission_veh[18].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[18].veh)
		ENDIF	
	ENDIF
	
	REMOVE_VEHICLE_RECORDING(1,"RCSJC")
	REMOVE_VEHICLE_RECORDING(2,"RCSJC")
	REMOVE_VEHICLE_RECORDING(7,"RCSJC")
	REMOVE_VEHICLE_RECORDING(9,"RCSJC")
	REMOVE_VEHICLE_RECORDING(13,"RCSJC")
	REMOVE_VEHICLE_RECORDING(23,"RCSJC")
	REMOVE_VEHICLE_RECORDING(24,"RCSJC")
	REMOVE_VEHICLE_RECORDING(25,"RCSJC")
	REMOVE_VEHICLE_RECORDING(14,"RCSJC")
	REMOVE_VEHICLE_RECORDING(15,"RCSJC")
	REMOVE_VEHICLE_RECORDING(16,"RCSJC")
	REMOVE_VEHICLE_RECORDING(17,"RCSJC")
	REMOVE_VEHICLE_RECORDING(32,"RCSJC")
	REMOVE_VEHICLE_RECORDING(33,"RCSJC")
	REMOVE_VEHICLE_RECORDING(34,"RCSJC")
	REMOVE_VEHICLE_RECORDING(35,"RCSJC")
	REMOVE_VEHICLE_RECORDING(36,"RCSJC")
	REMOVE_VEHICLE_RECORDING(37,"RCSJC")
	REMOVE_VEHICLE_RECORDING(38,"RCSJC")
	
	IF HAS_LABEL_BEEN_TRIGGERED("BRIDGETRAIN")
		IF DOES_ENTITY_EXIST(MissionTrain)
			IF MANAGE_MY_TIMER(iTimeSinceTrainCreation,30000)
				IF GET_DISTANCE_BETWEEN_ENTITIES(MissionTrain,PLAYER_PED_ID()) > 300
					IF NOT IS_ENTITY_ON_SCREEN(MissionTrain)
						DELETE_MISSION_TRAIN(MissionTrain)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// *********************** MISSION STAGES AND CUTSCENES AS PROCEDURES **********************

//PURPOSE: Requests and creates any assets etc that are initally required for the mission stage then holds in the load mission stage loop until complete
PROC MISSION_SETUP()

    // Loads the mission text
	REQUEST_ADDITIONAL_TEXT("SJC", MISSION_TEXT_SLOT)

	IF DOES_SCENARIO_GROUP_EXIST("GRAPESEED_PLANES")
		SET_SCENARIO_GROUP_ENABLED("GRAPESEED_PLANES",FALSE)
	ENDIF
	
	IF DOES_SCENARIO_GROUP_EXIST("SANDY_PLANES")
		SET_SCENARIO_GROUP_ENABLED("SANDY_PLANES",FALSE)
	ENDIF
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	//SET_WANTED_LEVEL_MULTIPLIER(0.2)
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	REMOVE_RELATIONSHIP_GROUP(rel_group_enemies)
	REMOVE_RELATIONSHIP_GROUP(rel_group_buddies)

	IF bAddedRelGroups = FALSE
		ADD_RELATIONSHIP_GROUP("BUDDIES", rel_group_buddies)
		ADD_RELATIONSHIP_GROUP("ENEMIES", rel_group_enemies)
		bAddedRelGroups = TRUE
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BLAZER,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BLAZER2,TRUE)

	//SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID() ,rel_group_buddies)
	
	ADD_COVER_BLOCKING_AREA(<<1719.117676 +0.75,3285.014404  +0.75,41.284805  +2>>,<<1719.117676 -0.75,3285.014404 -0.75,41.284805 -2>>,TRUE,TRUE,TRUE)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_buddies,rel_group_enemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_enemies,rel_group_enemies)
	SET_WIND(0)
	bCreatePickUps = FALSE
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,FALSE)
	SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(),TRV2_DAMAGE)
	
	DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<1982.3833, 3807.1899, 31.1531>>,10)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 1735.5898, 3222.9558, 47.8356 >>,<< 1770.1437, 3264.6560, 28.1638 >>,FALSE)
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
	disable_dispatch_services()
	DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
	INITALISE_ARRAYS()
	
	//1994556 - Disable taxis on this mission.
	DISABLE_TAXI_HAILING(TRUE)

	IF Is_Replay_In_Progress()
		//Load appropriate mission stage dependent on checkpoint
		IF Get_Replay_Mid_Mission_Stage() >= ENUM_TO_INT(MISSION_STAGE_SETUP) AND Get_Replay_Mid_Mission_Stage()  < ENUM_TO_INT(MISSION_STAGE_PASSED)
			iReplayStage = Get_Replay_Mid_Mission_Stage()	
			IF g_bShitskipAccepted = TRUE
				//iReplayStage = iReplayStage + 1
				IF iReplayStage < ENUM_TO_INT(MISSION_STAGE_PASSED)
					IF iReplayStage = 0
						START_REPLAY_SETUP(<< 1574.7804, 3363.0977, 47.6349 >>,245.3244)
						eMissionStage = MISSION_STAGE_SNIPE
						PRINTSTRING("******* iReplayStage = 1- MISSION_STAGE_SNIPE")PRINTNL()
					ELIF iReplayStage = 1
						START_REPLAY_SETUP(<< 1574.7804, 3363.0977, 47.6349 >>,245.3244)
						eMissionStage = MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
						PRINTSTRING("******* iReplayStage = 2")PRINTNL()
					ELIF iReplayStage = 2
						START_REPLAY_SETUP(<< 1583.0302, 3361.1277, 36.6399 >>,245.3244)
						eMissionStage = MISSION_STAGE_STEAL_PLANE
						PRINTSTRING("******* iReplayStage = 2")PRINTNL()
					ELIF iReplayStage = 3
						START_REPLAY_SETUP(<< 1729.6842, 3316.8794, 40.2261 >>,332.4664)
						eMissionStage = MISSION_STAGE_PLANE_WING
						PRINTSTRING("******* iReplayStage = 3")PRINTNL()
					ELIF iReplayStage = 4
						START_REPLAY_SETUP(<<842.7066, 3217.6802, 37.4018>>, 65.2580)
						eMissionStage = MISSION_STAGE_FLIGHT
						PRINTSTRING("******* iReplayStage = 4")PRINTNL()
					ELIF iReplayStage = 5
						START_REPLAY_SETUP(<<-3353.6501, 2931.2129, -89.9603>>, 177.9270)
						eMissionStage = MISSION_STAGE_TO_AIRSTRIP
						PRINTSTRING("******* iReplayStage = 5")PRINTNL()
					ELIF iReplayStage = 6
						START_REPLAY_SETUP(<<2136.9070, 4782.5830, 39.9702>>,105.1115)
						eMissionStage = MISSION_STAGE_CLOSING_CUTSCENE
						PRINTSTRING("******* iReplayStage = 6")PRINTNL()
					ELIF iReplayStage = 7
						START_REPLAY_SETUP(<<2136.9070, 4782.5830, 39.9702>>,105.1115)
						eMissionStage = MISSION_STAGE_CLOSING_CUTSCENE
						PRINTSTRING("******* iReplayStage = 7")PRINTNL()
					ELSE
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
						eMissionStage =  MISSION_STAGE_PASSED
						//SCRIPT_ASSERT("Trevor2: Trying to replay a mission stage that doesn't exist!")
					ENDIF
				ELSE
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					eMissionStage =  MISSION_STAGE_PASSED
				ENDIF
			ELSE
				//If the player chose to start from a checkpoint, jump to the last checkpoint reached.
				//eMissionStage = INT_TO_ENUM(MISSION_STAGE_ENUM, (Get_Replay_Mid_Mission_Stage()+1))
				IF iReplayStage = 0
					START_REPLAY_SETUP(<< 1981.90, 3816.16, 31.3560 >>,241.4918)
					eMissionStage = MISSION_STAGE_DRIVE
					PRINTSTRING("******* iReplayStage = 1 - MISSION_STAGE_DRIVE")PRINTNL()
				ELIF iReplayStage = 1
					START_REPLAY_SETUP(<< 1574.7804, 3363.0977, 47.6349 >>,245.3244)
					eMissionStage = MISSION_STAGE_SNIPE
					PRINTSTRING("******* iReplayStage = 2")PRINTNL()
				ELIF iReplayStage = 2
					START_REPLAY_SETUP(<< 1574.7804, 3363.0977, 47.6349 >>,245.3244)
					eMissionStage = MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE
					PRINTSTRING("******* iReplayStage = 2")PRINTNL()
				ELIF iReplayStage = 3
					START_REPLAY_SETUP(<< 1583.0302, 3361.1277, 36.6399 >>,245.3244)
					eMissionStage = MISSION_STAGE_STEAL_PLANE
					PRINTSTRING("******* iReplayStage = 3")PRINTNL()
				ELIF iReplayStage = 4
					START_REPLAY_SETUP(<< 1729.6842, 3316.8794, 40.2261 >>,332.4664)
					eMissionStage = MISSION_STAGE_PLANE_WING
					PRINTSTRING("******* iReplayStage = 4")PRINTNL()
				ELIF iReplayStage = 5
					START_REPLAY_SETUP(<<842.7066, 3217.6802, 37.4018>>, 65.2580)
					eMissionStage = MISSION_STAGE_FLIGHT
					PRINTSTRING("******* iReplayStage = 5")PRINTNL()
				ELIF iReplayStage = 6
					START_REPLAY_SETUP(<<-3353.6501, 2931.2129, -89.9603>>, 177.9270)
					eMissionStage = MISSION_STAGE_TO_AIRSTRIP
					PRINTSTRING("******* iReplayStage = 6")PRINTNL()
				ELIF iReplayStage = 7
					START_REPLAY_SETUP(<<2136.9070, 4782.5830, 39.9702>>,105.1115)
					eMissionStage = MISSION_STAGE_TO_AIRSTRIP
					PRINTSTRING("******* iReplayStage = 6")PRINTNL()
				ELSE
					SCRIPT_ASSERT("Trevor2: Trying to replay a mission stage that doesn't exist!")
				ENDIF
			ENDIF
			
		ELSE
			IF iReplayStage = 0
				IF g_bShitskipAccepted = TRUE
					eMissionStage = MISSION_STAGE_SNIPE
					PRINTSTRING("******* iReplayStage = 1 - MISSION_STAGE_SNIPE")PRINTNL()
				ELSE
					PRINTSTRING("******* iReplayStage = 1 - MISSION_STAGE_DRIV3")PRINTNL()
					eMissionStage = MISSION_STAGE_DRIVE
				ENDIF
			ELSE
				WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
					WAIT(0)
				ENDWHILE
				eMissionStage = MISSION_STAGE_OPENING_CUTSCENE
			ENDIF
		ENDIF
		IF NOT Is_Replay_In_Progress()
			PRINTSTRING("******* Warping player!!")PRINTNL()
			SET_PLAYER_START_POSITION(eMissionStage)
		ENDIF
	ELSE
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
		eMissionStage = MISSION_STAGE_OPENING_CUTSCENE
	ENDIF

	//Use iReplayStage to restart mission at the correct stage
	
	IF ENUM_TO_INT(eMissionStage) < ENUM_TO_INT(MISSION_STAGE_SETUP)
	OR ENUM_TO_INT(eMissionStage) >  ENUM_TO_INT(MISSION_STAGE_PASSED)
		PRINTSTRING("Invalid Mission Stage set somehow - Stage is set to the following - mission will probably not run:")PRINTINT( ENUM_TO_INT(eMissionStage))
	ENDIF
	
	IF eMissionStage != MISSION_STAGE_OPENING_CUTSCENE
		WHILE NOT bmissionstageloaded // continues requesting asserts/setup for particular stage until all are loaded
			WAIT(0)
			LOAD_MISSION_STAGE(eMissionStage)	
		ENDWHILE
	ENDIF

ENDPROC



PROC FLIGHT_CAM_CUTS()
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		
		
		IF iFlightHelp > 8
			IF iProgress < 3
				IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FOCUS_CAM_RUNWAY")
					START_AUDIO_SCENE("TREVOR_2_FOCUS_CAM_RUNWAY")
				ENDIF
			ELSE
				IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FOCUS_CAM_RON")
					START_AUDIO_SCENE("TREVOR_2_FOCUS_CAM_RON")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FOCUS_CAM_RON")
				START_AUDIO_SCENE("TREVOR_2_FOCUS_CAM_RON")
			ENDIF
		ENDIF

	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FOCUS_CAM_RON")
			STOP_AUDIO_SCENE("TREVOR_2_FOCUS_CAM_RON")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FOCUS_CAM_RUNWAY")
			STOP_AUDIO_SCENE("TREVOR_2_FOCUS_CAM_RUNWAY")
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
		IF eMissionStage = MISSION_STAGE_FLIGHT
			IF iProgress = 4
				IF bShowHatchOpen = FALSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[19].veh)
					AND IS_VEHICLE_DRIVEABLE(mission_veh[19].veh) 
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[19].veh)
							CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mission_veh[19].veh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF iProgress < 4
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
						CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mission_veh[1].veh)
					ELSE
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
					ENDIF
				ELSE
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
				ENDIF
			ENDIF
		ELSE
			IF iFlightHelp > 8
				IF iProgress < 3
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					AND IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) 
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
							CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, <<2127.4897, 4803.1445, 40.0305>>)
						ELSE
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
						ENDIF
					ENDIF
				ELSE
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
							CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mission_veh[1].veh)
						ELSE
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
						ENDIF
					ELSE
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()
//		PRINTLN("@@@@@@@@@@@ SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() @@@@@@@@@@@@@@")
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	ENDIF

ENDPROC


// PURPOSE: If player is in a bike/car, remove player control 'til the car is stopped 
PROC STOP_PLAYER_CAR(BOOL bAndLeave = FALSE)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
			FLOAT playerCarSpeed
			VEHICLE_INDEX playerCar
			playerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(playerCar)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				playerCarSpeed = GET_ENTITY_SPEED(playerCar)
				WHILE playerCarSpeed > 0.2
					WAIT(0)
					IF NOT IS_ENTITY_DEAD(playerCar)
						playerCarSpeed = GET_ENTITY_SPEED(playerCar)
					ENDIF
				ENDWHILE
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			ENDIF
			
		ENDIF
	ENDIF
	
	//FINISH
	IF bAndLeave
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
ENDPROC

PROC BUILD_WATERTOWER_CUSTOM_GPS_ROUTE()

	INT i = 0
	INT iClosestNode = -1
	FLOAT fClosestDist = 0.0
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vIntermediatePos[3]
	
	vIntermediatePos[0] = <<2079.8843, 3685.5381, 34.2074>>
	vIntermediatePos[1] = <<1715.8511, 3480.3096, 36.7092>>
	vIntermediatePos[2] = << 1580.0, 3364.2393, 37.5 >>

	//Get the closest node to the player
	REPEAT (COUNT_OF(vIntermediatePos) - 1) i
		FLOAT fDist = VDIST2(vPlayerPos, vIntermediatePos[i])
		IF iClosestNode = -1 OR fDist < fClosestDist
			iClosestNode = i
			fClosestDist = fDist
		ENDIF
	ENDREPEAT
	
	//If the closest node is closer to the next node than the player then it's the first node in the route, otherwise it's the next node.
	IF VDIST2(vPlayerPos, vIntermediatePos[iClosestNode+1]) < VDIST2(vIntermediatePos[iClosestNode], vIntermediatePos[iClosestNode+1])
		iClosestNode = iClosestNode + 1
	ENDIF

	//Build the route starting from the node calculated above.
	//uncomment this
	//START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
	
	i = iClosestNode
	WHILE i < COUNT_OF(vIntermediatePos) - 1
		ADD_POINT_TO_GPS_MULTI_ROUTE(vIntermediatePos[i])
		i++
	ENDWHILE

	ADD_POINT_TO_GPS_MULTI_ROUTE(<< 1580.0, 3364.2393, 37.5 >>)
	
	SET_GPS_MULTI_ROUTE_RENDER(TRUE)
ENDPROC

INT iTimeSinceDrivingOff

PROC MANAGE_RONS_BEHAVIOR()

	SWITCH iRonDriveToTasks
		
		CASE 0
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
					IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
						TASK_ENTER_VEHICLE(NULL,mission_veh[MV_RON_QUAD].veh)
					ENDIF
					TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
				CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
				TASK_PERFORM_SEQUENCE(psRon.pedIndex, TASK_SEQUENCE)
				REQUEST_WAYPOINT_RECORDING("trv2QuadGtoA")
				iRonDriveToTasks ++
			ENDIF
		BREAK
		
		CASE 1
			IF GET_IS_WAYPOINT_RECORDING_LOADED("trv2QuadGtoA")
				IF NOT IS_PED_INJURED(psRon.pedIndex)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
						IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
							OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
								IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
									TASK_ENTER_VEHICLE(NULL,mission_veh[MV_RON_QUAD].veh)
								ENDIF
								TASK_VEHICLE_TEMP_ACTION(NULL,mission_veh[MV_RON_QUAD].veh,TEMPACT_REVERSE,800)
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL, mission_veh[MV_RON_QUAD].veh, "trv2QuadGtoA", DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT,-1,-1,FALSE,2)
							CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_PERFORM_SEQUENCE(psRon.pedIndex, TASK_SEQUENCE)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(psRon.pedIndex,KNOCKOFFVEHICLE_NEVER)
							iTimeSinceDrivingOff = GET_GAME_TIMER()
							IF DOES_BLIP_EXIST(psRon.blipIndex)
								REMOVE_BLIP(psRon.blipIndex)
							ENDIF
							iRonDriveToTasks ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			
			IF HAS_LABEL_BEEN_TRIGGERED("TR2_GOSCOT")
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_QIAA")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.pedIndex,"TR2_QIAA","NervousRon",SPEECH_PARAMS_FORCE_FRONTEND)
						SET_LABEL_AS_TRIGGERED("TR2_QIAA",TRUE)
					ENDIF
				ENDIF
			ENDIF
			REQUEST_VEHICLE_RECORDING(29,"RCSJC")
			IF MANAGE_MY_TIMER(iTimeSinceDrivingOff,3000)
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(29,"RCSJC")
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
								IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
								OR MANAGE_MY_TIMER(iTimeSinceDrivingOff,90000) AND NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_RON_QUAD].veh)
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh,29,"RCSJC")
									PAUSE_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh)
									SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(psRon.pedIndex,KNOCKOFFVEHICLE_NEVER)
									iRonDriveToTasks ++
								ENDIF
							ELSE
								SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_QUAD].veh)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC

INT iRonDriveToTower

PROC MANAGE_RONS_DRIVE_TO_TOWER()
	
	SEQUENCE_INDEX SequenceIndex

	
	IF IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS)
		IF NOT HAS_LABEL_BEEN_TRIGGERED("WARP RON")
			IF NOT IS_PED_INJURED(psRon.PedIndex)
				CLEAR_PED_TASKS_IMMEDIATELY(psRon.PedIndex)
				SET_ENTITY_COORDS(psRon.PedIndex,<<1576.75, 3360.88, 42.13>>)
				SET_ENTITY_HEADING(psRon.PedIndex,-135.27)
				TASK_START_SCENARIO_IN_PLACE(psRon.PedIndex,"WORLD_HUMAN_BINOCULARS",0)
				SET_ENTITY_HEALTH(psRon.PedIndex,120)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
				SET_VEHICLE_ENGINE_ON(mission_veh[MV_RON_QUAD].veh,FALSE,TRUE)
				SET_ENTITY_COORDS(mission_veh[MV_RON_QUAD].veh,<<1573.862671,3353.939941,37.044361>>)
				SET_ENTITY_HEADING(mission_veh[MV_RON_QUAD].veh,-53.571102)
			ENDIF
			SET_LABEL_AS_TRIGGERED("WARP RON",TRUE)
		ENDIF
	ENDIF

	SWITCH iRonDriveToTower
		
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
				IF NOT IS_PED_INJURED(psRon.PedIndex)
					IF NOT IS_PED_SITTING_IN_THIS_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
						IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_RON_QUAD].veh)
							IF GET_SCRIPT_TASK_STATUS (psRon.PedIndex, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								TASK_ENTER_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_WALK)
							ENDIF
						ENDIF
					ELSE
						SET_PED_STEERS_AROUND_OBJECTS(psRon.PedIndex,TRUE) 
						REQUEST_WAYPOINT_RECORDING("trv2QuadGtoA")
						iRonDriveToTower ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			REQUEST_WAYPOINT_RECORDING("trv2QuadGtoA")
			IF GET_IS_WAYPOINT_RECORDING_LOADED("trv2QuadGtoA")
				IF NOT IS_PED_INJURED(psRon.PedIndex)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
						IF IS_PED_SITTING_IN_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
							SET_PED_CONFIG_FLAG(psRon.pedIndex,PCF_Avoidance_Ignore_WeirdPedBuffer,FALSE)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(psRon.PedIndex, mission_veh[MV_RON_QUAD].veh, "trv2QuadGtoA", DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
							CLEAR_AREA(<<1703.014282,3751.739746,32.222816>>,30,TRUE)
							iRonDriveToTower ++
						ELSE
							iRonDriveToTower --
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(psRon.PedIndex)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
					IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
						BRING_VEHICLE_TO_HALT(mission_veh[MV_RON_QUAD].veh,1,2000)
						SET_VEHICLE_ENGINE_ON(mission_veh[MV_RON_QUAD].veh,FALSE,FALSE)
						iRonDriveToTower ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_PED_INJURED(psRon.PedIndex)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
					IF GET_ENTITY_SPEED(mission_veh[MV_RON_QUAD].veh) < 1.0
						SET_RAGDOLL_BLOCKING_FLAGS(psRon.PedIndex,RBF_PLAYER_IMPACT | RBF_MELEE)
						
						SET_PED_PATH_CAN_USE_LADDERS(psRon.PedIndex,TRUE)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							IF IS_PED_IN_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
								TASK_LEAVE_VEHICLE(NULL,mission_veh[MV_RON_QUAD].veh)
							ENDIF
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1576.75, 3360.88, 42.13>>,1.0,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,-135.27)
							TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_BINOCULARS",0)
							TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(psRon.PedIndex, SequenceIndex)
						iRonDriveToTower ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_GUN_SHOP_INTERIOR_STREAMING()
	IF GET_DISTANCE_BETWEEN_COORDS(<<1700.02893, 3752.93066, 33.71693>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 80
		SETUP_MISSION_REQUIREMENT(REQ_GUN_SHOP_IN_MEMORY,<<0,0,0>>)
	ELSE
		IF GET_DISTANCE_BETWEEN_COORDS(<<1700.02893, 3752.93066, 33.71693>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
			IF NOT IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS)
				IF interior_gun_shop != NULL
					UNPIN_INTERIOR(interior_gun_shop)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

BOOL bPositionPedsDuringToD = FALSE
BOOL bShopGodText = FALSE
INT  iTellTrevorToMeetHimAtTowerTimer
INT  iTellTrevorToMeetHimAtTowerFirstTimer
PED_INDEX pedMelvin

PROC DRIVE()

	IF iProgress < 5
		MANAGE_RONS_DRIVE_TO_TOWER()
	ENDIF
	
	PRE_STREAM_MISSION_STAGE()
	
	MANAGE_GUN_SHOP_INTERIOR_STREAMING()
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("BUDDIES IN VEHICLE")
		IF NOT IS_PED_INJURED(psRon.PedIndex)
			SET_PED_MAX_MOVE_BLEND_RATIO(psRon.PedIndex, PEDMOVE_WALK)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
				IF IS_PED_IN_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
					SET_LABEL_AS_TRIGGERED("BUDDIES IN VEHICLE",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1574.341431,3361.411865,40.626335>>, <<1577.552124,3361.419922,43.376335>>, 3.750000)
		IF NOT IS_PED_INJURED(psRon.PedIndex)
			IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),psRon.PedIndex) < 6
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MELEE_ATTACK1)
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MELEE_ATTACK2)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_SHOP_COORDS(GUN_SHOP_02_SS)) < 250.0 * 250.0
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2170555
	ENDIF
	
	SWITCH iProgress
		
		//SET UP STAGE PLAY INITIAL DIALOGUE
		CASE 0
			IF IS_SCREEN_FADED_OUT()
				MANAGE_PED_OUTFIT()
			ENDIF
			
			SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_TREVOR, WEAPONTYPE_SNIPERRIFLE, TRUE)
			SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_02_SS, TRUE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_AllowPlayerToInterruptVehicleEntryExit,TRUE)
			CREATE_FORCED_OBJECT(<<1681.0197, 3282.8428, 39.9353>>,50,PROP_GAS_TANK_02A,TRUE)
			
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			SETUP_PEDS_FOR_DIALOGUE()
			
			REQUEST_IPL("airfield")
			REQUEST_VEHICLE_ASSET(BLAZER)
			
			IF NOT IS_PED_INJURED(psRon.PedIndex)
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(psRon.PedIndex,KNOCKOFFVEHICLE_NEVER)
				SET_PED_STEERS_AROUND_OBJECTS(psRon.PedIndex,FALSE) 
			ENDIF

			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_AMN")
				IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_AMN", CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("TR2_AMN",TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(psRon.pedIndex)
				SET_PED_CONFIG_FLAG(psRon.pedIndex,PCF_Avoidance_Ignore_WeirdPedBuffer,TRUE)
			ENDIF
	
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 8.0)
	
			SET_LABEL_AS_TRIGGERED("START RON GOTO GUNSHOP",FALSE)
			SET_LABEL_AS_TRIGGERED("RON ARRIVES AT GUNSHOP",FALSE)
			SET_LABEL_AS_TRIGGERED("GET BACK IN QUAD",FALSE)
			SET_LABEL_AS_TRIGGERED("START RON GOTO TOWER",FALSE)
			CLEAR_TRIGGERED_LABELS()
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_TREVOR_2)
			iRonDriveToTower = 0
			SET_WEATHER_TYPE_PERSIST("OVERCAST")
			
			SET_CUTSCENE_FADE_VALUES(FALSE,TRUE,FALSE,TRUE)
			START_AUDIO_SCENE("TREVOR_2_DRIVE_ATV")
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_RON_QUAD].veh,"TREVOR_2_RONS_ATV")
			SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SNIPING ) 
			iProgress++
			CONTROL_FADE_IN(500)
			
		BREAK
		
		CASE 1
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<1698.3430, 3753.3125, 33.7053>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,FALSE,mission_veh[MV_TREVOR_QUAD].veh,"SJC_GOTOSHOP","SJC_GETONQ","SJC_GETONQ1",TRUE)
				
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				OR IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1672.158691,3752.785889,32.550236>>, <<1721.808472,3781.364502,43.237503>>, 53.000000)
						KILL_FACE_TO_FACE_CONVERSATION()
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						iProgress++
					ELSE
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF bGoToSpeech = FALSE
								IF NOT IS_PED_INJURED(psRon.PedIndex)
									IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
										IF IS_PED_IN_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
											IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_TRAINS_VOLUME")
												START_AUDIO_SCENE("TREVOR_2_TRAINS_VOLUME")
											ENDIF
											IF GET_DISTANCE_BETWEEN_ENTITIES(psRon.PedIndex,PLAYER_PED_ID()) > 10
												IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
													IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_AMND", CONV_PRIORITY_MEDIUM)
														bGoToSpeech = TRUE
													ENDIF
												ELSE
													IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_AMND", CONV_PRIORITY_MEDIUM)
														bGoToSpeech = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
//					IF NOT DOES_BLIP_EXIST(psRon.BlipIndex)
//						psRon.BlipIndex = CREATE_BLIP_FOR_PED(psRon.PedIndex,FALSE)
//					ENDIF	
				ELSE
					IF DOES_BLIP_EXIST(psRon.BlipIndex)
						REMOVE_BLIP(psRon.BlipIndex)
					ENDIF
				ENDIF
				
				IF bGoToSpeech = TRUE
					//MANAGE PAUSE DIALOGUE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							CLEAR_PRINTS()
						ENDIF
					//Play conversation
					ELSE
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("LOSE_WANTED")
								CLEAR_PRINTS()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//HAS THE PLAYER ENTERED THE SHOP
		CASE 2
			//STOP THE PLAYER IF THEY TRY AND DRIVE THROUGH DOOR
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_QUAD].veh)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1698.875732,3750.414795,32.376537>>, <<1700.868774,3752.533691,36.859776>>, 3.250000)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_STOP_DRIVE")
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(mission_veh[MV_TREVOR_QUAD].veh,2)
								TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_QUAD].veh)
								SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
								SET_LABEL_AS_TRIGGERED("TR2_STOP_DRIVE",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ENDIF
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			
			//STOP THE PLAYER ON GETTING CLOSE TO THE SHOP
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_QUAD].veh)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1700.228516,3745.691406,32.503475>>, <<1710.552002,3756.382568,35.849823>>, 4.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1703.237427,3766.650146,32.922688>>, <<1712.972046,3757.286621,35.860573>>, 4.500000)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_STOP_DRIVE_BY_SHOP")
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(mission_veh[MV_TREVOR_QUAD].veh,3)
								TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_QUAD].veh)
								SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
								SET_LABEL_AS_TRIGGERED("TR2_STOP_DRIVE_BY_SHOP",TRUE)
							ENDIF
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						ENDIF
					ENDIF
				ELSE
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ENDIF
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			
			IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData,<<1698.3430, 3753.3125, 33.7053>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,FALSE,"")
				
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1699.789307,3758.147949,33.455318>>, <<1695.031006,3752.867432,36.205318>>, 5.750000)
					OR IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS)
						
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 5.0)
						
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							REMOVE_BLIP(sLocatesData.LocationBlip)
						ENDIF
						IF NOT IS_PED_INJURED(psRon.PedIndex)
							IF IS_PED_IN_GROUP(psRon.PedIndex)
								REMOVE_PED_FROM_GROUP(psRon.PedIndex)
							ENDIF
						ENDIF
						//KILL_FACE_TO_FACE_CONVERSATION()
						GET_CLOSEST_PED(<<1692.2778, 3760.9111, 33.7053>>,20,true,true,pedMelvin,false,true) 
						ADD_PED_FOR_DIALOGUE(sSpeech,8,pedMelvin,"MELVIN")
						IF NOT IS_PED_INJURED(pedMelvin)
							TASK_LOOK_AT_ENTITY(pedMelvin,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
						ENDIF
						bGoToSpeech = FALSE
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						SET_DEFAULT_GUNSHOP_WEAPON(WEAPONTYPE_SNIPERRIFLE)
						bShopGodText = FALSE
						iProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//HAS PLAYER GOT WEAPON
		CASE 3
			
			IF HAS_LABEL_BEEN_TRIGGERED("TR2_RZAA")
				IF GET_DISTANCE_BETWEEN_COORDS(<<1699.37622, 3752.17920, 33.71643>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 20
					TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
					reason_for_fail = NO_WEAPON
					eMissionStage = MISSION_STAGE_FAIL
					PRINTSTRING("shop fail")
					PRINTNL()
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("UNPAUSE")
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					SET_LABEL_AS_TRIGGERED("UNPAUSE",TRUE)
				ENDIF
			ENDIF
						
			IF IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RZAA")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_IAMN",CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("TR2_RZAA",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
				AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
				
				ELSE
					IF HAS_LABEL_BEEN_TRIGGERED("TR2_IAMN")
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_02_SS)	
				IF HAS_LABEL_BEEN_TRIGGERED("TR2_RZAA")
					IF NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_SCOPE_MAX)
					AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_BUYATTACH")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								PRINT_GOD_TEXT("SJC_BUYATTACH")
								SET_LABEL_AS_TRIGGERED("SJC_BUYATTACH",TRUE)
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_BUYSCOP")
							IF NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
							AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									PRINT_GOD_TEXT("SJC_BUYSCOP")
									SET_LABEL_AS_TRIGGERED("SJC_BUYSCOP",TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_BUYSIL")
							IF NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									PRINT_GOD_TEXT("SJC_BUYSIL")
									SET_LABEL_AS_TRIGGERED("SJC_BUYSIL",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
				AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_QLAA")
						IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_QLAA","TREVOR",SPEECH_PARAMS_FORCE)
							SET_LABEL_AS_TRIGGERED("TR2_QLAA",TRUE)
						ENDIF
					ENDIF
				ELSE
					IF 	IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_02_SS)
					AND g_sShopSettings.eSelectedGunshopWeapon = WEAPONTYPE_SNIPERRIFLE //check if we're looking at sniper rifle
						IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_QJAA SNIPE")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_DRAA","TREVOR",SPEECH_PARAMS_FORCE)
									SET_LABEL_AS_TRIGGERED("TR2_QJAA SNIPE",TRUE)
								ENDIF
							ENDIF
						ELSE
							IF HAS_LABEL_BEEN_TRIGGERED("TR2_QJAA")
								IF NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
								AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DSAA")
										IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_DSAA","TREVOR",SPEECH_PARAMS_FORCE)
											SET_LABEL_AS_TRIGGERED("TR2_DSAA",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF HAS_LABEL_BEEN_TRIGGERED("TR2_DSAA")
								IF HAS_LABEL_BEEN_TRIGGERED("TR2_QKAA")
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DUAA")
										IF NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
										AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DUAA")
												IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_DUAA","TREVOR",SPEECH_PARAMS_FORCE)
													SET_LABEL_AS_TRIGGERED("TR2_DUAA",TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF HAS_LABEL_BEEN_TRIGGERED("TR2_DSAA")
								IF HAS_LABEL_BEEN_TRIGGERED("TR2_XYAA")
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_EBAA")
										IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
										AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_EBAA")
												IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_EBAA","TREVOR",SPEECH_PARAMS_FORCE)
													SET_LABEL_AS_TRIGGERED("TR2_EBAA",TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_QJAA")
							IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_QJAA","TREVOR",SPEECH_PARAMS_FORCE)
								SET_LABEL_AS_TRIGGERED("TR2_QJAA",TRUE)
							ENDIF
						ENDIF
					ENDIF
				
					IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_QKAA")
							IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_QKAA","TREVOR",SPEECH_PARAMS_FORCE)
								SET_LABEL_AS_TRIGGERED("TR2_QKAA",TRUE)
							ENDIF
						ENDIF
					ENDIF
				
					IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_XYAA")
							IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_XYAA","TREVOR",SPEECH_PARAMS_FORCE)
								SET_LABEL_AS_TRIGGERED("TR2_XYAA",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF

			ENDIF
			
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
			AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
			AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
				IF NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_02_SS)
					CLEAR_PRINTS()
					IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							REMOVE_BLIP(sLocatesData.LocationBlip)
						ENDIF
						SET_SEETHROUGH(FALSE)
						SET_LABEL_AS_TRIGGERED("TR2_RNSC",TRUE)
						SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_02_SS, FALSE)
						iProgress++
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("LEFT THE SHOP")
					IF NOT IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS)
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						SET_LABEL_AS_TRIGGERED("LEFT THE SHOP",TRUE)
					ENDIF
				ELSE
					
					IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData,<<1698.3430, 3753.3125, 33.7053>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,FALSE,"",TRUE)
			
					ELSE
						IF NOT bShopGodText
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
								AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
								AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
									PRINTSTRING("Assigning sPurchaseObject to SJC_BACKSHOP")PRINTNL()
									bShopGodText = TRUE
									PRINT_GOD_TEXT("SJC_BACKSHOP")
								ENDIF
								
								IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
								AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
								AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
									PRINTSTRING("Assigning sPurchaseObject to SJC_BACKSHOPBO")PRINTNL()
									bShopGodText = TRUE
									PRINT_GOD_TEXT("SJC_BACKSHOPBO")
								ENDIF
								
								IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
								AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
								AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
									PRINTSTRING("Assigning sPurchaseObject to SJC_BACKSHOPSC")PRINTNL()
									bShopGodText = TRUE
									PRINT_GOD_TEXT("SJC_BACKSHOPSC")
								ENDIF
								
								IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE)
								AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
								AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
									PRINTSTRING("Assigning sPurchaseObject to SJC_BACKSHOPSI")PRINTNL()
									bShopGodText = TRUE
									PRINT_GOD_TEXT("SJC_BACKSHOPSI")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS)
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							REMOVE_BLIP(sLocatesData.LocationBlip)
						ENDIF
						IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_BACKSHOPSI")
						OR IS_THIS_PRINT_BEING_DISPLAYED("SJC_BACKSHOPSC")
						OR IS_THIS_PRINT_BEING_DISPLAYED("SJC_BACKSHOPBO")
						OR IS_THIS_PRINT_BEING_DISPLAYED("SJC_BACKSHOP")
							PRINTSTRING("CLEARING GOD TEXT PRINTS")PRINTNL()
							CLEAR_PRINTS()
						ENDIF
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						SET_LABEL_AS_TRIGGERED("LEFT THE SHOP",FALSE)
						bShopGodText = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//DRIVEN TO OR GOT TO THE TOWER ON FOOT
		CASE 4
			
			IF NOT IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS)
				IF interior_gun_shop != NULL
					UNPIN_INTERIOR(interior_gun_shop)
				ENDIF
			ENDIF
			
			IF bGoToSpeech = TRUE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MAIN CONVO FINISHED")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						SET_LABEL_AS_TRIGGERED("MAIN CONVO FINISHED",TRUE)
						iTellTrevorToMeetHimAtTowerFirstTimer = GET_GAME_TIMER()
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_dr01a")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF MANAGE_MY_TIMER(iTellTrevorToMeetHimAtTowerFirstTimer,GET_RANDOM_INT_IN_RANGE(14000,22000))
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_dr01a", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED( "TR_dr01a",TRUE)
									iTellTrevorToMeetHimAtTowerTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF MANAGE_MY_TIMER(iTellTrevorToMeetHimAtTowerTimer,GET_RANDOM_INT_IN_RANGE(12000,40000))
							SET_LABEL_AS_TRIGGERED("TR_dr01a",FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 1580.0, 3364.2393, 37.5 >>,<<30,30,30>>)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl5op")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl5op", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("TR_pl5op",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
				IF NOT IS_PED_INJURED(psRon.PedIndex)
					//IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<1584.1542, 3359.5991, 36.6475>>,<<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>,TRUE,mission_veh[MV_TREVOR_QUAD].veh,"SJC_GETWA","SJC_GETONQ","SJC_GETONQ1",TRUE)
					IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<1576.64, 3360.53, 42.13>>,<<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>,FALSE,mission_veh[MV_TREVOR_QUAD].veh,"SJC_GETWAR","SJC_GETONQ","SJC_GETONQ1",TRUE)	
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(mission_veh[MV_TREVOR_QUAD].veh,DEFAULT_VEH_STOPPING_DISTANCE)
							IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
								REMOVE_BLIP(sLocatesData.vehicleBlip)
							ENDIF
							STOP_AUDIO_SCENE("TREVOR_2_DRIVE_ATV")

							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							KILL_FACE_TO_FACE_CONVERSATION()
							bGoToSpeech = FALSE
							iProgress++
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							SET_BLIP_COLOUR(sLocatesData.LocationBlip,BLIP_COLOUR_BLUE)
							SET_BLIP_ROUTE_COLOUR(sLocatesData.LocationBlip,BLIP_COLOUR_BLUE)
							SET_BLIP_NAME_FROM_TEXT_FILE(sLocatesData.LocationBlip,"CMN_FRBLIP")
						ENDIF

						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 1580.0, 3364.2393, 37.5 >>,<<6,6,6>>)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_QUAD].veh)
								IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(mission_veh[MV_TREVOR_QUAD].veh,DEFAULT_VEH_STOPPING_DISTANCE)
									IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_GETONQ1")
										CLEAR_PRINTS()
									ENDIF
									STOP_AUDIO_SCENE("TREVOR_2_DRIVE_ATV")
									CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
									KILL_FACE_TO_FACE_CONVERSATION()
									bGoToSpeech = FALSE
									iProgress++
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 1580.0, 3364.2393, 37.5 >>,<<30,30,30>>)
						AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_QUAD].veh)
							IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_GETONQ1")
								CLEAR_PRINTS()
							ENDIF
							STOP_AUDIO_SCENE("TREVOR_2_DRIVE_ATV")
							KILL_FACE_TO_FACE_CONVERSATION()
							bGoToSpeech = FALSE
							iProgress++
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF bGoToSpeech = FALSE
										IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
											IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_COME", CONV_PRIORITY_MEDIUM)
												bGoToSpeech = TRUE
											ENDIF
										ELSE
											IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_COME", CONV_PRIORITY_MEDIUM)
												bGoToSpeech = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF bGoToSpeech = TRUE
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_GOTKIT")
											IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_GOTKIT", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR2_GOTKIT",TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							//MANAGE PAUSE DIALOGUE
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									CLEAR_PRINTS()
								ENDIF
							//Play conversation
							ELSE
								IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
									CLEAR_PRINTS()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK

		//CLIMBED THE TOWER
		CASE 5
			IF HAS_LABEL_BEEN_TRIGGERED("STREAM AIRSTRIP")
				IF GET_DISTANCE_BETWEEN_COORDS(<<1574.8378, 3363.7754, 47.6348>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20
					SnipeVol = STREAMVOL_CREATE_SPHERE(<<1700.36,3293.01,48.86>>, 10.0,FLAG_MAPDATA,STREAMVOL_LOD_FLAG_HIGH)
					SET_LABEL_AS_TRIGGERED("STREAM AIRSTRIP",TRUE)
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(<<1574.8378, 3363.7754, 47.6348>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 30
					IF STREAMVOL_IS_VALID(SnipeVol)
						STREAMVOL_DELETE(SnipeVol)
						SET_LABEL_AS_TRIGGERED("STREAM AIRSTRIP",FALSE)
					ENDIF
				ENDIF
			ENDIF

			IF psRon.iLookAt !=99
				TASK_LOOK_AT_ENTITY(psRon.PedIndex,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
				PRINTSTRING("TELL RON TO LOOK AT YOU")PRINTNL()
				psRon.iLookAt = 99
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl5op")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl5op", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("TR_pl5op",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//Doesn't climb first bit
				IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1572.096313,3363.188232,40.876335>>, <<1577.557861,3363.068604,43.126335>>, 7.500000)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RNSC")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RNSC", CONV_PRIORITY_MEDIUM)
										iTowerReminderDialogueTimer = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("TR2_RNSC",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF MANAGE_MY_TIMER(iTowerReminderDialogueTimer,GET_RANDOM_INT_IN_RANGE(26000,30000))
								SET_LABEL_AS_TRIGGERED("TR2_RNSC",FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Doesn't climb second bit
				IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1572.096313,3363.188232,40.876335>>, <<1577.557861,3363.068604,43.126335>>, 7.500000)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RNLC")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RNLC", CONV_PRIORITY_MEDIUM)
										iTowerReminderDialogueTimer = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("TR2_RNLC",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF MANAGE_MY_TIMER(iTowerReminderDialogueTimer,GET_RANDOM_INT_IN_RANGE(16000,22000))
								SET_LABEL_AS_TRIGGERED("TR2_RNLC",FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("LOAD FOR TOD")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1579.698608,3363.894287,35.419746>>, <<1569.983521,3363.613281,49.305035>>, 10.250000)
						NEW_LOAD_SCENE_START_SPHERE(<<1708.6008, 3328.9648, 40.1454>>, 20.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
						SET_LABEL_AS_TRIGGERED("LOAD FOR TOD",TRUE)
					ENDIF
				ELSE
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1579.698608,3363.894287,35.419746>>, <<1569.983521,3363.613281,49.305035>>, 10.250000)
						NEW_LOAD_SCENE_STOP()
						SET_LABEL_AS_TRIGGERED("LOAD FOR TOD",FALSE)
					ENDIF
				ENDIF
				
				//IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData,<<1574.8439, 3362.6108, 47.6348>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,"SJC_TOWERG")
				IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData,<<1574.8378, 3363.7754, 47.6348>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,"SJC_TOWERG")
					IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(psRon.PedIndex)
							TASK_CLEAR_LOOK_AT(psRon.PedIndex)
						ENDIF
						
						//NEW_LOAD_SCENE_START_SPHERE(<<1699.9397, 3298.1697, 40.1478>>, 2.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
						
						KILL_FACE_TO_FACE_CONVERSATION()
						
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
												
						//REQUEST_VEHICLE_RECORDING(12,"RCSJC")
						REQUEST_VEHICLE_RECORDING(29,"RCSJC")
						
						CLEAR_PRINTS()
						CLEAR_HELP()
						bPositionPedsDuringToD = FALSE
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						TRIGGER_MUSIC_EVENT("TRV2_MISSION_START")
						STOP_AUDIO_SCENE("TREVOR_2_DRIVE_ATV")
						iProgress++
					ENDIF
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1576.692139,3363.694824,47.534836>>, <<1572.984253,3363.694336,50.384834>>, 4.500000)
						IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(psRon.PedIndex)
								TASK_CLEAR_LOOK_AT(psRon.PedIndex)
							ENDIF
							
							//NEW_LOAD_SCENE_START_SPHERE(<<1699.9397, 3298.1697, 40.1478>>, 2.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
							
							KILL_FACE_TO_FACE_CONVERSATION()
							
							//REQUEST_VEHICLE_RECORDING(12,"RCSJC")
							REQUEST_VEHICLE_RECORDING(29,"RCSJC")
							
							CLEAR_PRINTS()
							CLEAR_HELP()
							bPositionPedsDuringToD = FALSE
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							TRIGGER_MUSIC_EVENT("TRV2_MISSION_START")
							STOP_AUDIO_SCENE("TREVOR_2_DRIVE_ATV")
							iProgress++
						ENDIF
					ELSE
						IF IS_PLAYER_CLIMBING(PLAYER_ID())
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_CALL2")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_CALL2", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR_CALL2",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF

					ENDIF
				ENDIF
			ENDIF
					
		BREAK
		
		CASE 6
			//ToD
//			SET_WEATHER_TYPE_NOW_PERSIST("OVERCAST")
//			SET_CLOCK_TIME(23,00,00)

			sTimelapse.currentTimeOfDay = GET_CURRENT_TIMEOFDAY()
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			IF DO_TIMELAPSE(SP_MISSION_TREVOR_2, sTimelapse,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, 2000, FALSE, TRUE,TRUE)
				NEW_LOAD_SCENE_STOP()
				IF STREAMVOL_IS_VALID(SnipeVol)
					STREAMVOL_DELETE(SnipeVol)
				ENDIF
				
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
				
				iProgress++
			ELSE
				IF bPositionPedsDuringToD = FALSE
					IF DOES_CAM_EXIST(sTimelapse.splineCamera)
						IF IS_CAM_ACTIVE(sTimelapse.splineCamera)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TIMECYCLE SHIFT")
								IF GET_CAM_SPLINE_PHASE(sTimelapse.splineCamera) > 0.2
									//SET_TRANSITION_TIMECYCLE_MODIFIER("nervousRON_fog",5000)
									SET_LABEL_AS_TRIGGERED("TIMECYCLE SHIFT",TRUE)
								ENDIF
							ENDIF
								
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(29,"RCSJC")
								SET_ENTITY_COORDS(PLAYER_PED_ID(),<<1576.1097, 3363.3091, 47.6348>>)
								SET_ENTITY_HEADING(PLAYER_PED_ID(),252.6176)
								//SET_ENTITY_HEADING(PLAYER_PED_ID(),240.5856)
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,TRUE)
								
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
									SET_ENTITY_COORDS(mission_veh[MV_TREVOR_QUAD].veh,<<1578.81, 3358.61, 37.03>>)
									SET_ENTITY_HEADING(mission_veh[MV_TREVOR_QUAD].veh,250.73)
								ENDIF
								
								IF NOT IS_PED_INJURED(psRon.PedIndex)
									IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
										IF IS_PED_IN_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
											IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
												START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh,29,"RCSJC")
												PAUSE_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh)
												SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(psRon.PedIndex,KNOCKOFFVEHICLE_NEVER)
												SET_VEHICLE_LIGHTS(mission_veh[MV_RON_QUAD].veh, FORCE_VEHICLE_LIGHTS_OFF)
												bPositionPedsDuringToD = TRUE
											ENDIF
										ELSE
											IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_RON_QUAD].veh)
												SET_PED_INTO_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			
			//GOT TO THE TOWER TELL RON TO GO
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_CALL3")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_CALL3", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("TR_CALL3",TRUE)
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl0b")
					IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl0b", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("TR_pl0b",TRUE)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl0a", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("TR_pl0b",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(psRon.PedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
							IF IS_PED_IN_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
									UNPAUSE_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh)
									SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(psRon.PedIndex,KNOCKOFFVEHICLE_NEVER)
									SET_ENTITY_HEALTH(psRon.PedIndex,300)
									iProgress++
								ELSE
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh,29,"RCSJC")
								ENDIF
							ELSE
								IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_RON_QUAD].veh)
									SET_PED_INTO_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_QUAD].veh)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
		
		CASE 8
			IF NOT IS_PED_INJURED(psRon.PedIndex)
				CLEAR_RAGDOLL_BLOCKING_FLAGS(psRon.PedIndex,RBF_MELEE)
			ENDIF
			REMOVE_WAYPOINT_RECORDING("trv2QuadGun")
			REMOVE_WAYPOINT_RECORDING("trv2QuadGtoA")
			SET_DEFAULT_GUNSHOP_WEAPON(WEAPONTYPE_INVALID)
			INITIALISE_ARRAYS_FOR_AIRFIELD()
			REQUEST_IPL("airfield")
			ADVANCE_MISSION_STAGE()
		BREAK
		
	ENDSWITCH


ENDPROC


PROC PRINT_GOD_TEXT_ADVANCED(STRING sLabel, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)
		PRINT(sLabel, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Printing god text ", sLabel, ".")
		#ENDIF
	ENDIF
ENDPROC

FUNC PED_INDEX CREATE_ENEMY_PED(MODEL_NAMES modelName, VECTOR vPosition, FLOAT fHeading, REL_GROUP_HASH group, WEAPON_TYPE eWeaponType = WEAPONTYPE_UNARMED,
								WEAPONCOMPONENT_TYPE eWeaponComponent = WEAPONCOMPONENT_INVALID, INT iHealth = 200)


	PED_INDEX PedIndex = CREATE_PED(PEDTYPE_MISSION, modelName, vPosition, fHeading)
	
	SET_PED_COMPONENT_VARIATION(PedIndex, PED_COMP_HAIR, 1, 0)

	//enemy health and armor
	SET_PED_MAX_HEALTH(PedIndex, iHealth)
	SET_ENTITY_HEALTH(PedIndex, iHealth)
	SET_PED_DIES_WHEN_INJURED(PedIndex, TRUE)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, group)
	SET_PED_AS_ENEMY(PedIndex, TRUE)
	SET_PED_TARGET_LOSS_RESPONSE(PedIndex, TLR_NEVER_LOSE_TARGET)
	SET_PED_COMBAT_ATTRIBUTES(PedIndex, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(PedIndex, CA_ENABLE_TACTICAL_POINTS_WHEN_DEFENSIVE, TRUE)
		
	//enemy weapons
	GIVE_WEAPON_TO_PED(PedIndex, eWeaponType, INFINITE_AMMO, TRUE)
	SET_CURRENT_PED_WEAPON(PedIndex, eWeaponType, TRUE)
	SET_PED_INFINITE_AMMO(PedIndex, TRUE, eWeaponType)
	SET_PED_ACCURACY(PedIndex,100)
	
	IF ( eWeaponComponent <> WEAPONCOMPONENT_INVALID )
		GIVE_WEAPON_COMPONENT_TO_PED(PedIndex, eWeaponType, eWeaponComponent)
	ENDIF
	
	SET_PED_CONFIG_FLAG(PedIndex, PCF_DisableHurt, TRUE)
	SET_PED_CONFIG_FLAG(PedIndex, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	
	STOP_PED_WEAPON_FIRING_WHEN_DROPPED(PedIndex)
	
	SET_COMBAT_FLOAT(PedIndex, CCF_TIME_TO_INVALIDATE_INJURED_TARGET, 30.0)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy ped with model ", GET_MODEL_NAME_FOR_DEBUG(modelName), " at coordinates ", vPosition, ".")
	#ENDIF
		
	RETURN PedIndex

ENDFUNC

FUNC PED_INDEX CREATE_ENEMY_PED_IN_VEHICLE(VEHICLE_INDEX VehicleIndex, MODEL_NAMES ModelName, REL_GROUP_HASH RelHashGroup,
										   VEHICLE_SEAT eVehicleSeat,  WEAPON_TYPE eWeaponType = WEAPONTYPE_UNARMED,
										   WEAPONCOMPONENT_TYPE eWeaponComponent = WEAPONCOMPONENT_INVALID, INT iHealth = 200
										   #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)
										   
	IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
	
		PED_INDEX PedIndex = CREATE_PED_INSIDE_VEHICLE(VehicleIndex, PEDTYPE_MISSION, ModelName, eVehicleSeat)
		
		SET_PED_MAX_HEALTH(PedIndex, iHealth)
		SET_ENTITY_HEALTH(PedIndex, iHealth)
		SET_PED_DIES_WHEN_INJURED(PedIndex, TRUE)
		
		GIVE_WEAPON_TO_PED(PedIndex, eWeaponType, INFINITE_AMMO, FALSE)
		SET_PED_INFINITE_AMMO(PedIndex, TRUE, eWeaponType)
		
		IF ( eWeaponComponent <> WEAPONCOMPONENT_INVALID )
			GIVE_WEAPON_COMPONENT_TO_PED(PedIndex, eWeaponType, eWeaponComponent)
		ENDIF
		
		SET_PED_AS_ENEMY(PedIndex, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, RelHashGroup)
		
		SET_PED_TARGET_LOSS_RESPONSE(PedIndex, TLR_NEVER_LOSE_TARGET)
		
		SET_PED_CONFIG_FLAG(PedIndex, PCF_DisableHurt, TRUE)
		SET_PED_CONFIG_FLAG(PedIndex, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		SET_PED_CONFIG_FLAG(PedIndex, PCF_AllowToBeTargetedInAVehicle, TRUE)
		
		STOP_PED_WEAPON_FIRING_WHEN_DROPPED(PedIndex)
	
		SET_COMBAT_FLOAT(PedIndex, CCF_TIME_TO_INVALIDATE_INJURED_TARGET, 30.0)
		
		SET_ENTITY_IS_TARGET_PRIORITY(PedIndex, TRUE)

		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			
		#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sDebugName)
				SET_PED_NAME_DEBUG(PedIndex, sDebugName)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy ped in vehicle ", sDebugName, ".")
			ELSE
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy ped in vehicle.")
			ENDIF
		#ENDIF
		
		RETURN PedIndex

	ENDIF

	RETURN NULL

ENDFUNC

/// PURPOSE:
///    Warps specified ped to new coordinates and sets specified ped's new heading.
/// PARAMS:
///    ped - PED_INDEX to warp.
///    vNewPosition - VECTOR coordinates specifying new ped's position.
///    fNewHeading - FLOAT specifying new ped's heading.
///    bKeepVehicle - Specifies if ped should keep vehicle they are currently in.
///    bResetGameplayCamera - Specifies if gameplay camera should be positioned behind player's back. Works only for PLAYER_PED_ID().
///    bLoadScene - Specifies if scene at new provided coordinates should be loaded. Works only for PLAYER_PED_ID().
PROC WARP_PED(PED_INDEX ped, VECTOR vNewPosition, FLOAT fNewHeading, BOOL bKeepVehicle, BOOL bResetGameplayCamera, BOOL bLoadScene)

	IF NOT IS_PED_INJURED(ped)
	
		IF ( bKeepVehicle = TRUE )
			SET_PED_COORDS_KEEP_VEHICLE(ped, vNewPosition)
		ELIF ( bKeepVehicle = FALSE ) 
			SET_ENTITY_COORDS(ped, vNewPosition)
		ENDIF
		
		SET_ENTITY_HEADING(ped, fNewHeading)
		
		IF ( ped = PLAYER_PED_ID())
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
		
			IF ( bResetGameplayCamera = TRUE)
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
			ENDIF
			
			IF ( bLoadScene = TRUE )
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", vNewPosition, ". This might take a while to load.")
				#ENDIF
			
				LOAD_SCENE(vNewPosition)
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

//Checks if the player is at the water tower
FUNC BOOL IS_PED_AT_WATER_TOWER(PED_INDEX PedIndex)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
			IF IS_ENTITY_AT_COORD(PedIndex, vWaterTowerTopPosition, << 1.8, 1.8, 1.8 >>)
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_THE_SNIPER_RIFLE_EQUIPED()
	
	WEAPON_TYPE CurrentPlayerWeapon
	IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentPlayerWeapon)
		IF ( CurrentPlayerWeapon = WEAPONTYPE_SNIPERRIFLE OR CurrentPlayerWeapon = WEAPONTYPE_HEAVYSNIPER )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Checks if a specified ped is in the sniper scope of the player
FUNC BOOL IS_PED_IN_PLAYER_SNIPER_SCOPE(PED_INDEX PedIndex, FLOAT fGameplayCamFOV = 12.0, FLOAT fXTolerance = 0.23, FLOAT fYTolerance = 0.47)

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
				WEAPON_TYPE CurrentPlayerWeapon

				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentPlayerWeapon)
					IF ( CurrentPlayerWeapon = WEAPONTYPE_SNIPERRIFLE OR CurrentPlayerWeapon = WEAPONTYPE_HEAVYSNIPER )

						IF DOES_ENTITY_EXIST(PedIndex)
							IF NOT IS_ENTITY_DEAD(PedIndex)
								
								IF IS_ENTITY_ON_SCREEN(PedIndex)
									IF IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(PedIndex), 0.001)
										IF GET_GAMEPLAY_CAM_FOV() < fGameplayCamFOV
										
											FLOAT fXPosition, fYPosition
										
											GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(PedIndex), fXPosition, fYPosition)
																				
											IF 	(fXPosition > 0.5 - fXTolerance) AND (fXPosition < 0.5 + fXTolerance)
											AND (fYPosition > 0.5 - fYTolerance) AND (fYPosition < 0.5 + fYTolerance)

												RETURN TRUE
												
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF

	RETURN FALSE

ENDFUNC

PROC GO_TO_PED_STATE(PED_SNIPE_STRUCT &ped, PED_STATES eState)

	ped.bHasTask 	= FALSE
	ped.eState 		= eState
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(ped.sDebugName)
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Sending ped with debug name ", ped.sDebugName, " to state ", GET_PED_STATE_NAME_FOR_DEBUG(eState), ".")
		ENDIF
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    This procedure manages the first enemy in the sniping section. This is the enemy standing under the control tower.
/// PARAMS:
///    ped - 
///    iEnemyProgress - 
PROC MANAGE_FIRST_SNIPING_ENEMY(PED_SNIPE_STRUCT &ped, INT &iEnemyProgress)

	SWITCH iEnemyProgress
	
		CASE 0

			iEnemyProgress++
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(ped.ModelName)
			
			IF HAS_MODEL_LOADED(ped.ModelName)
			
				IF ( bFirstSnipingEnemyAllowed = TRUE )
			
					ped.PedIndex = CREATE_ENEMY_PED(ped.ModelName, ped.vPosition, ped.fHeading, rel_group_enemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH, 135)
					ADD_DEADPOOL_TRIGGER(ped.PedIndex, TRV2_KILLS)
					
					IF DOES_ENTITY_EXIST(ped.PedIndex)
						SET_ENTITY_HEALTH(ped.PedIndex, 150)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_HEAD, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_BERD, 		1, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_HAIR, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_TORSO, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_LEG, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_HAND, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_FEET, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_TEETH, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_SPECIAL, 	0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_SPECIAL2, 	0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_DECL, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_JBIB, 		0, 0)
					ENDIF
					
					SET_PED_LOD_MULTIPLIER(ped.PedIndex,1.3)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(ped.ModelName)
					
					GO_TO_PED_STATE(ped, PED_STATE_UNALERTED)
								 
					iEnemyProgress++
					
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 2	//update enemy behaviour
		
			IF DOES_ENTITY_EXIST(ped.PedIndex)
				IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, ped.sDebugName, 1.75)
						ENDIF
					#ENDIF
					
					IF ( ped.eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
						ENDIF
					ENDIF
				
					SWITCH ped.eState
					
						CASE PED_STATE_UNALERTED
						
							IF ( ped.bHasTask = FALSE )
								
								CLEAR_PED_TASKS(ped.PedIndex)
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 25.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 25.0)
								
								SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
								SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)
								
								SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(ped.PedIndex)
								
								SEQUENCE_INDEX SequenceIndex
								
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1701.37, 3296.68, 41.15 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 48.13)
									TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_DRINKING",0,TRUE)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								
								//TASK_START_SCENARIO_AT_POSITION(psEnemies[0].PedIndex, "WORLD_HUMAN_DRINKING", << 1701.37, 3296.68, 41.15 >>, 48.13)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
							REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE,ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS),WEAPON_COMPONENT_FLASH)
							REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE))
							REQUEST_ANIM_DICT("missexile2reactions_to_gun_fire@standing@idle_a")
							
							IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
							OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
								GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
							ENDIF

						BREAK
						
						CASE PED_STATE_ALERTED
						
							IF ( ped.bHasTask = FALSE )
								IF HAS_ANIM_DICT_LOADED("missexile2reactions_to_gun_fire@standing@idle_a")
									GIVE_WEAPON_TO_PED(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE, TRUE)
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(ped.PedIndex)
									TASK_PLAY_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									ped.bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE PED_STATE_INVESTIGATING_RON
						
							IF ( ped.bHasTask = FALSE )
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 60.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 60.0)
								
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex,0,0)
								SET_PED_ACCURACY(ped.PedIndex, 100)
								SET_PED_SHOOT_RATE(ped.PedIndex, 125)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 6.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 6.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.PedIndex, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
								ped.bHasTask = TRUE
							ENDIF

						BREAK
						
						CASE PED_STATE_COMBAT
							
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 1)
								SET_PED_SHOOT_RATE(ped.PedIndex, 175)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex, 0.9, 0.9)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_PED(ped.PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
						BREAK
					
					ENDSWITCH
				
				ELSE	//enemy ped is dead
				
					ped.vPosition = GET_ENTITY_COORDS(ped.PedIndex, FALSE)
				
				ENDIF
			ENDIF
			
			IF ( ped.bForceBlipOn = FALSE )
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
				OR HAS_LABEL_BEEN_TRIGGERED("SJC_ENEM1G_0")
					ped.bForceBlipOn = TRUE
				ENDIF
			ENDIF
			
			IF ( bPedBlipsHidden = FALSE )
				UPDATE_AI_PED_BLIP(ped.PedIndex, ped.EnemyBlipData, -1, NULL, ped.bForceBlipOn, FALSE, 500.0)
			ELSE
				CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
			ENDIF
		
		BREAK

	ENDSWITCH
			
ENDPROC


/// PURPOSE:
///    This procedure manages the second enemy in the sniping section. This is the enemy driving into the area in a gburrito van.
/// PARAMS:
///    ped - 
///    iEnemyProgress - 
PROC MANAGE_SECOND_SNIPING_ENEMY(PED_SNIPE_STRUCT &ped, INT &iEnemyProgress)

	IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
		SET_VEH_FORCED_RADIO_THIS_FRAME(vsEnemyVans[0].VehicleIndex)
	ENDIF
	
	IF IS_PED_INJURED(ped.PedIndex)
		IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
			IF GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex,vsEnemyVans[0].VehicleIndex) > 2
                SET_PED_RESET_FLAG(ped.PedIndex, PRF_ForceEnableFlashLightForAI, TRUE)
                SET_PED_RESET_FLAG(ped.PedIndex, PRF_DisableFlashLight, FALSE)
  			ELSE
                SET_PED_RESET_FLAG(ped.PedIndex, PRF_ForceEnableFlashLightForAI, FALSE)
                SET_PED_RESET_FLAG(ped.PedIndex, PRF_DisableFlashLight, TRUE)
			ENDIF
      	ENDIF
  	ENDIF

	
//	IF NOT IS_ENTITY_DEAD(ped.PedIndex)
//		IF ped.iThisPedProgress > 2
//			SET_PED_RESET_FLAG(ped.PedIndex,PRF_ForceEnableFlashLightForAI,TRUE)
//		ELSE
//			SET_PED_RESET_FLAG(ped.PedIndex,PRF_ForceEnableFlashLightForAI,FALSE)
//		ENDIF
//	ENDIF

	SWITCH iEnemyProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(psEnemies[0].PedIndex)		//when first sniping enemy is killed
				IF IS_ENTITY_DEAD(psEnemies[0].PedIndex)
				OR IS_PED_INJURED(psEnemies[0].PedIndex)

					IF ( bBikersAlerted = FALSE )
						REQUEST_AMBIENT_AUDIO_BANK("TREVOR_2_VAN_REV")
						iEnemyProgress++
						
					ENDIF
					
				ENDIF			
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(ped.ModelName)
			REQUEST_MODEL(vsEnemyVans[0].ModelName)
			REQUEST_VEHICLE_RECORDING(vsEnemyVans[0].iRecordingNumber, sVehicleRecordingsFile)
			
			IF 	HAS_MODEL_LOADED(ped.ModelName)
			AND HAS_MODEL_LOADED(vsEnemyVans[0].ModelName)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(vsEnemyVans[0].iRecordingNumber, sVehicleRecordingsFile)
			
			
				IF ( bSecondSnipingEnemyAllowed = TRUE )
			
					vsEnemyVans[0].VehicleIndex = CREATE_VEHICLE(vsEnemyVans[0].ModelName, vsEnemyVans[0].vPosition, vsEnemyVans[0].fHeading)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vsEnemyVans[0].VehicleIndex, TRUE)
					
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vsEnemyVans[0].VehicleIndex ,"TREVOR_2_BIKER_VAN_GROUP")
				
					ped.PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(vsEnemyVans[0].VehicleIndex, ped.ModelName, rel_group_enemies, VS_DRIVER,
															   WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH, 135)
															   
					ADD_DEADPOOL_TRIGGER(ped.PedIndex, TRV2_KILLS)
					
					IF DOES_ENTITY_EXIST(ped.PedIndex)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_HEAD, 		0, 1)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_BERD, 		1, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_HAIR, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_TORSO, 		1, 1)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_LEG, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_HAND, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_FEET, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_TEETH, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_SPECIAL, 	0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_SPECIAL2, 	0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_DECL, 		0, 0)
						SET_PED_COMPONENT_VARIATION(ped.PedIndex, PED_COMP_JBIB, 		0, 0)
					ENDIF
					
					
					
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vsEnemyVans[0].VehicleIndex,TRUE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(ped.ModelName)
					SET_MODEL_AS_NO_LONGER_NEEDED(vsEnemyVans[0].ModelName)
					
					GO_TO_PED_STATE(ped, PED_STATE_UNALERTED)
					
					fSecondEnemyPlaybackTime = 0.0
						 
					iEnemyProgress++
			
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
				IF IS_HORN_ACTIVE(vsEnemyVans[0].VehicleIndex)
					bBikersAlerted = TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
				IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[0].VehicleIndex)
						fSecondEnemyPlaybackTime = GET_TIME_POSITION_IN_RECORDING(vsEnemyVans[0].VehicleIndex)
					ENDIF
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(ped.PedIndex)
				IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				
					ped.fDistanceToTarget 	= GET_DISTANCE_BETWEEN_PEDS(ped.PedIndex, psEnemies[0].PedIndex)		//get distance to coprse of first enemy
					ped.bCanSeeTargetPed 	= HAS_ENTITY_CLEAR_LOS_TO_ENTITY(ped.PedIndex, psEnemies[0].PedIndex)	//get line of sight to corpse of first enemy
					
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_VEHICLE_RECORDING_INFO(vsEnemyVans[0].VehicleIndex, 1.0)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, ped.sDebugName, 1.75)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_FLOAT(ped.fDistanceToTarget), 2.0)
							IF ( ped.bCanSeeTargetPed )
								DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, "HAS LOS", 2.5)
//								DRAW_DEBUG_LINE(GET_ENTITY_COORDS(ped.PedIndex, FALSE), GET_ENTITY_COORDS(psEnemies[0].PedIndex, FALSE))
							ENDIF
						ENDIF
					#ENDIF
					
					IF ( ped.eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							IF NOT IS_PED_IN_ANY_VEHICLE(ped.PedIndex)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
							ENDIF
						ENDIF
					ENDIF
					
					SWITCH ped.eState
					
						CASE PED_STATE_UNALERTED
						
							SWITCH ped.iThisPedProgress
						
								CASE 0
						
									IF ( ped.bHasTask = FALSE )
										IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
											IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[0].VehicleIndex)
												REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vsEnemyVans[0].VehicleIndex)
												
												SET_VEHICLE_ENGINE_ON(vsEnemyVans[0].VehicleIndex, TRUE, TRUE)
												SET_VEHICLE_LIGHTS(vsEnemyVans[0].VehicleIndex, FORCE_VEHICLE_LIGHTS_ON)
												
												SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vsEnemyVans[0].VehicleIndex, TRUE)
												START_PLAYBACK_RECORDED_VEHICLE(vsEnemyVans[0].VehicleIndex, vsEnemyVans[0].iRecordingNumber, sVehicleRecordingsFile)
												SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vsEnemyVans[0].VehicleIndex, 1000.0)
												SET_PLAYBACK_SPEED(vsEnemyVans[0].VehicleIndex, 0.45)
												FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vsEnemyVans[0].VehicleIndex)
	
												SET_VEH_RADIO_STATION(vsEnemyVans[0].VehicleIndex, "RADIO_04_PUNK")
												SET_VEHICLE_RADIO_LOUD(vsEnemyVans[0].VehicleIndex,TRUE)
												
												SET_PED_SEEING_RANGE(ped.PedIndex, 15.0)
												SET_PED_HEARING_RANGE(ped.PedIndex, 15.0)
										
												SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
												SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)
												
												SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(ped.PedIndex, TRUE)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
												PLAY_SOUND_FROM_ENTITY(-1,"Trevor_2_van_rev",vsEnemyVans[0].VehicleIndex,"TREVOR_2_SOUNDS")
												
											ELSE
												ped.bHasTask = TRUE
											ENDIF
										ENDIF
									ENDIF
									
									REQUEST_AMBIENT_AUDIO_BANK("TREVOR_2_BIKER_RINGTONE")
									REQUEST_ANIM_DICT("misstrevor2ig_4b")
									REQUEST_MODEL(osPropPhone.ModelName)					
									
									IF ( ped.bHasTask = TRUE )
										IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
											IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[0].VehicleIndex)
												REMOVE_VEHICLE_RECORDING(vsEnemyVans[0].iRecordingNumber, sVehicleRecordingsFile)
												ped.bHasTask = FALSE
												ped.iThisPedProgress++
											ENDIF									
										ENDIF
									ENDIF

									IF ( ped.bCanSeeTargetPed )
										IF ( ped.fDistanceToTarget < 5.0 )
											IF ( osPropWallLights[0].bBroken = FALSE )
											OR ( osPropWallLights[1].bBroken = FALSE )
												GO_TO_PED_STATE(ped, PED_STATE_ALERTED)	
											ENDIF
										ENDIF
									ENDIF

									IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 2.0) 
									OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_USING(ped.PedIndex), PLAYER_PED_ID())
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)									
									ENDIF
									
									IF IS_AREA_OCCUPIED(<< 1701.96 - 6.0, 3308.27 - 8.0, 41.68 - 2.0 >>,
														<< 1701.96 + 6.0, 3308.27 + 8.0, 41.68 + 2.0 >>,
														FALSE, FALSE, TRUE, FALSE, FALSE, ped.PedIndex)
										
										IF ( ped.bCanSeeTargetPed = TRUE )
											IF ( ped.fDistanceToTarget < 5.0 )
												ped.iThisPedProgress = 0
												bSecondSnipingEnemyInvestigating = TRUE
												GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_DEAD_BODY)
											ENDIF
										ENDIF		
									ENDIF
									
								BREAK
								
								CASE 1
								
									IF ( ped.bHasTask = FALSE )
										IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
										
											SET_VEHICLE_ENGINE_ON(vsEnemyVans[0].VehicleIndex, FALSE, FALSE)
											//SET_VEHICLE_LIGHTS(vsEnemyVans[0].VehicleIndex, FORCE_VEHICLE_LIGHTS_OFF)

											SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)	//might need attachment to his hand

											TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_4b", "biker_on_phone", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY, 0, FALSE, AIK_DISABLE_LEG_IK)
					
											FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
											
											ped.bHasTask = TRUE
										ENDIF
									ENDIF
								
									IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_4b", "biker_on_phone")
									
										SET_ENTITY_ANIM_SPEED(ped.PedIndex, "misstrevor2ig_4b", "biker_on_phone", 1.25)
									
										//create and attach phone
										IF NOT DOES_ENTITY_EXIST(osPropPhone.ObjectIndex)
											IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_4b", "biker_on_phone") >= 0.035
												IF HAS_MISSION_OBJECT_BEEN_CREATED(osPropPhone)
													IF DOES_ENTITY_EXIST(osPropPhone.ObjectIndex)
														IF NOT IS_ENTITY_DEAD(osPropPhone.ObjectIndex)
															IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(osPropPhone.ObjectIndex)
																ATTACH_ENTITY_TO_ENTITY(osPropPhone.ObjectIndex, ped.PedIndex,
																						GET_PED_BONE_INDEX(ped.PedIndex, BONETAG_PH_L_HAND),
																						<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, -180.0 >>, TRUE, TRUE, FALSE)
																
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										//open van doors
										IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
											IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(vsEnemyVans[0].VehicleIndex, SC_DOOR_FRONT_LEFT)
												IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_4b", "biker_on_phone") >= 0.210
													SET_VEHICLE_DOOR_OPEN(vsEnemyVans[0].VehicleIndex, SC_DOOR_FRONT_LEFT)
												ENDIF
											ENDIF
										ENDIF

										//change tasks when the ped is about to exit vehicle
										IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iVanExitSceneID)
											IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_4b", "biker_on_phone") >= 0.225
											
												FLOAT fStartPhase
												
												fStartPhase = GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_4b", "biker_on_phone")
											
												#IF IS_DEBUG_BUILD
													PRINTLN(GET_THIS_SCRIPT_NAME(), ": Switching animations tasks at phase fStartPhase: ", fStartPhase, ".")
												#ENDIF
											
												iVanExitSceneID = CREATE_SYNCHRONIZED_SCENE(<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
												ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iVanExitSceneID, vsEnemyVans[0].VehicleIndex, -1)
												SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iVanExitSceneID, TRUE)
			
												CLEAR_PED_SECONDARY_TASK(ped.PedIndex)
												SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
												
												TASK_SYNCHRONIZED_SCENE(ped.PedIndex, iVanExitSceneID, "misstrevor2ig_4b", "biker_on_phone",
																		INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN)
												FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
												
												SET_SYNCHRONIZED_SCENE_PHASE(iVanExitSceneID, fStartPhase)
												SET_SYNCHRONIZED_SCENE_RATE(iVanExitSceneID, 1.15)
												
												PLAY_SOUND_FROM_COORD(-1, "Biker_Ring_Tone", psEnemies[0].vPosition, "TREVOR_2_SOUNDS")
												
												SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)	//might need attachment to his hand
												
												ped.iThisPedProgress++
												ped.bHasTask = FALSE
											
											ENDIF
										ENDIF
									
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)									
									ENDIF

								BREAK
								
								CASE 2
								
									IF ( ped.bHasTask = FALSE )
										IF IS_SYNCHRONIZED_SCENE_RUNNING(iVanExitSceneID)
											ped.bHasTask = TRUE
										ENDIF
									ENDIF
								
									IF DOES_ENTITY_EXIST(osPropPhone.ObjectIndex)
										IF IS_SYNCHRONIZED_SCENE_RUNNING(iVanExitSceneID)
											IF GET_SYNCHRONIZED_SCENE_PHASE(iVanExitSceneID) >= 0.815
												CLEANUP_OBJECT(osPropPhone.ObjectIndex, TRUE)
											ENDIF
										ENDIF
									ENDIF
									
									IF IS_SYNCHRONIZED_SCENE_RUNNING(iVanExitSceneID)
										IF GET_SYNCHRONIZED_SCENE_PHASE(iVanExitSceneID) >= 0.95
											GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
										ENDIF
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)									
									ENDIF
								
								BREAK
								
							ENDSWITCH
						
						BREAK
						
						CASE PED_STATE_ALERTED

							IF ( ped.bHasTask = FALSE )
								
								IF DOES_ENTITY_EXIST(osPropPhone.ObjectIndex)
									CLEANUP_OBJECT(osPropPhone.ObjectIndex)
								ENDIF
								
								IF IS_PED_IN_ANY_VEHICLE(ped.PedIndex)
									IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
										IF IS_PED_IN_VEHICLE(ped.PedIndex, vsEnemyVans[0].VehicleIndex)
										
											IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[0].VehicleIndex)
												STOP_PLAYBACK_RECORDED_VEHICLE(vsEnemyVans[0].VehicleIndex)
												REMOVE_VEHICLE_RECORDING(vsEnemyVans[0].iRecordingNumber, sVehicleRecordingsFile)
											ENDIF
											
											SET_HORN_PERMANENTLY_ON_TIME(vsEnemyVans[0].VehicleIndex, 7000)
											CLEAR_PED_TASKS(ped.PedIndex)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
											ped.iThisPedTimer = 0
											ped.bHasTask = TRUE
										ENDIF
									ENDIF
								ELSE
									IF HAS_ANIM_DICT_LOADED("missexile2reactions_to_gun_fire@standing@idle_a")
										SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
										TASK_PLAY_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
										ped.bHasTask = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_PED_SITTING_IN_ANY_VEHICLE(ped.PedIndex)
									BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_USING(ped.PedIndex), 1.0, 1)
									IF IS_PED_STOPPED(ped.PedIndex)
										IF IS_HORN_ACTIVE(GET_VEHICLE_PED_IS_USING(ped.PedIndex))
											IF ( ped.iThisPedTimer = 0 )
												ped.iThisPedTimer = GET_GAME_TIMER()
											ELSE
												IF HAS_TIME_PASSED(3500, ped.iThisPedTimer)
													GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE PED_STATE_INVESTIGATING_DEAD_BODY
							
							SWITCH ped.iThisPedProgress
							
								CASE 0
								
									IF ( ped.bHasTask = FALSE )
							
										IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
											IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
												IF IS_PED_IN_VEHICLE(ped.PedIndex, vsEnemyVans[0].VehicleIndex)
													IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[0].VehicleIndex)
														STOP_PLAYBACK_RECORDED_VEHICLE(vsEnemyVans[0].VehicleIndex)
														REMOVE_VEHICLE_RECORDING(vsEnemyVans[0].iRecordingNumber, sVehicleRecordingsFile)
													ENDIF
													CLEAR_PED_TASKS(ped.PedIndex)
													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
													ped.bHasTask = TRUE
												ENDIF
											ENDIF
										ENDIF
										
									ENDIF
									
									IF ( ped.bHasTask = TRUE )
										IF IS_PED_SITTING_IN_ANY_VEHICLE(ped.PedIndex)
											BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_USING(ped.PedIndex), 0.7, 1)
											IF IS_PED_STOPPED(ped.PedIndex)
												ped.iThisPedProgress++
												ped.bHasTask = FALSE
											ENDIF
										ENDIF
									ENDIF
								
								BREAK
								
								CASE 1
								
									IF ( ped.bHasTask = FALSE )
										
										SEQUENCE_INDEX SequenceIndex
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_LEAVE_ANY_VEHICLE(NULL)
											TASK_LOOK_AT_COORD(NULL, psEnemies[0].vPosition, 2000, SLF_WHILE_NOT_IN_FOV)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, psEnemies[0].vPosition, psEnemies[0].vPosition, 0.5, FALSE, 2.0)
											TASK_AIM_GUN_AT_COORD(NULL, psEnemies[0].vPosition, 5000, FALSE, TRUE)
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										
										FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
										
										SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
										
										ped.bHasTask = TRUE
									ENDIF
									
									IF ( ped.bHasTask = TRUE )
										IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
											GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
										ENDIF
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)									
									ENDIF
								
								BREAK
							
							ENDSWITCH

						BREAK
						
						CASE PED_STATE_INVESTIGATING_RON
						
							IF ( ped.bHasTask = FALSE )
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 60.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 60.0)
								SET_PED_ACCURACY(ped.PedIndex, 100)
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex,0,0)
								
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 3.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									IF NOT IS_PED_INJURED(psRon.PedIndex)
										TASK_LOOK_AT_COORD(NULL, GET_ENTITY_COORDS(psRon.PedIndex), 2000, SLF_WHILE_NOT_IN_FOV)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, GET_ENTITY_COORDS(psRon.PedIndex), GET_ENTITY_COORDS(psRon.PedIndex), 0.5, FALSE, 3.0)
									ENDIF
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
							
								//TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.PedIndex, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
							
								ped.bHasTask = TRUE
							ENDIF
						
						BREAK
						
						CASE PED_STATE_COMBAT
							
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 1)
								SET_PED_SHOOT_RATE(ped.PedIndex, 175)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex, 0.9, 0.9)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_PED(ped.PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
						BREAK
					
					ENDSWITCH
				
				ELSE	//enemy ped is dead
				
					IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[0].VehicleIndex)
							STOP_PLAYBACK_RECORDED_VEHICLE(vsEnemyVans[0].VehicleIndex)
							REMOVE_VEHICLE_RECORDING(vsEnemyVans[0].iRecordingNumber, sVehicleRecordingsFile)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(osPropPhone.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osPropPhone.ObjectIndex)
							IF IS_ENTITY_ATTACHED(osPropPhone.ObjectIndex)
								DETACH_ENTITY(osPropPhone.ObjectIndex, FALSE, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF ( ped.iThisPedProgress = 1 )
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_4b", "biker_on_phone")
							STOP_ANIM_PLAYBACK(ped.PedIndex, AF_PRIORITY_LOW, TRUE)
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
			
			IF ( ped.bForceBlipOn = FALSE )
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
				//OR objective tells to take him out
					ped.bForceBlipOn = TRUE
				ENDIF
			ENDIF

			
			IF ( bPedBlipsHidden = FALSE )
				IF ( ped.bForceBlipOn = TRUE )
					UPDATE_AI_PED_BLIP(ped.PedIndex, ped.EnemyBlipData, -1, NULL, ped.bForceBlipOn, FALSE, 500.0)
				ENDIF
			ELSE
				CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
			ENDIF
			
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    This procedure manages the third enemy in the sniping section. This is the first enemy that comes out of the control tower.
/// PARAMS:
///    ped - 
///    iEnemyProgress - 
PROC MANAGE_THIRD_SNIPING_ENEMY(PED_SNIPE_STRUCT &ped, INT &iEnemyProgress)

	SWITCH iEnemyProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(psEnemies[1].PedIndex)
				IF IS_ENTITY_DEAD(psEnemies[1].PedIndex)
				OR IS_PED_INJURED(psEnemies[1].PedIndex)
					
					IF ( bBikersAlerted = FALSE )
						CLEANUP_OBJECT(osPropPhone.ObjectIndex,  TRUE)
						iEnemyProgress++
					ENDIF
					
				ENDIF			
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(ped.ModelName)
			
			IF 	HAS_MODEL_LOADED(ped.ModelName)
			
				ped.PedIndex = CREATE_ENEMY_PED(ped.ModelName, ped.vPosition, ped.fHeading, rel_group_enemies, WEAPONTYPE_ASSAULTRIFLE,
												WEAPONCOMPONENT_AT_AR_FLSH, 135)
												
				ADD_DEADPOOL_TRIGGER(ped.PedIndex, TRV2_KILLS)
				
				//open tower door, maybe animate for better looking door opening, for now just snap them open and lock in open position
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(TowerDoor.eHashEnum))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(TowerDoor.eHashEnum), 1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(TowerDoor.eHashEnum), DOORSTATE_LOCKED, FALSE, TRUE)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Locking door with int ", ENUM_TO_INT(TowerDoor.eHashEnum), " with model ",
								GET_MODEL_NAME_FOR_DEBUG(TowerDoor.ModelName), " at coordinates ", TowerDoor.vPosition, ".")
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Door with int ", ENUM_TO_INT(TowerDoor.eHashEnum), " with model ",
								GET_MODEL_NAME_FOR_DEBUG(TowerDoor.ModelName), " at coordinates ", TowerDoor.vPosition, " is not registered with door system.")
					#ENDIF
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(ped.ModelName)
				
				GO_TO_PED_STATE(ped, PED_STATE_UNALERTED)
							 
				iEnemyProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF DOES_ENTITY_EXIST(ped.PedIndex)
				IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, ped.sDebugName, 1.75)
						ENDIF
					#ENDIF
					
					IF ( ped.eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
						ENDIF
					ENDIF
				
					SWITCH ped.eState
					
						CASE PED_STATE_UNALERTED
						
							SWITCH ped.iThisPedProgress
							
								CASE 0
						
									IF ( ped.bHasTask = FALSE )
										
										SET_PED_SEEING_RANGE(ped.PedIndex, 25.0)
										SET_PED_HEARING_RANGE(ped.PedIndex, 25.0)
										
										SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
										SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)
										
										SEQUENCE_INDEX SequenceIndex
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_GO_STRAIGHT_TO_COORD(NULL, << 1697.77, 3291.67, 47.92 >>, 0.6)
//											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1701.68, 3296.94, 47.86 >>, 0.6, DEFAULT_TIME_BEFORE_WARP,
//																		  DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 354.0147)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1699.62, 3295.12, 47.92 >>, 0.6, DEFAULT_TIME_BEFORE_WARP,
																		  DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, -6.00)
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
										
										ped.bHasTask = TRUE
										
									ENDIF
									
									REQUEST_ANIM_DICT("reaction@male_stand@big_variations@d")
									
									IF ( ped.bHasTask = TRUE )
										IF IS_ENTITY_AT_COORD(ped.PedIndex, << 1699.62, 3295.12, 47.92 >>, << 2.0, 2.0, 2.0 >>)
											IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
												ped.bHasTask = FALSE
												ped.iThisPedProgress++
											ENDIF
										ENDIF
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										REMOVE_ANIM_DICT("reaction@male_stand@big_variations@d")
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
									ENDIF
									
								BREAK
								
								CASE 1
								
									IF ( ped.bHasTask = FALSE )
									
										IF HAS_ANIM_DICT_LOADED("reaction@male_stand@big_variations@d")
										
											SEQUENCE_INDEX SequenceIndex
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											OPEN_SEQUENCE_TASK(SequenceIndex)
												TASK_PLAY_ANIM(NULL, "reaction@male_stand@big_variations@d", "react_big_variations_k")
												TASK_PLAY_ANIM(NULL, "reaction@male_stand@big_variations@d", "react_big_variations_o")
											CLOSE_SEQUENCE_TASK(SequenceIndex)
											TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											
											ped.bHasTask = TRUE
											
										ENDIF

									ENDIF
									
									IF ( ped.bHasTask = TRUE )
										IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
											REMOVE_ANIM_DICT("reaction@male_stand@big_variations@d")
											ped.bHasTask = FALSE
											ped.iThisPedProgress++
										ENDIF
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										REMOVE_ANIM_DICT("reaction@male_stand@big_variations@d")
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
									ENDIF
								
								BREAK
								
								CASE 2
								
									IF ( ped.bHasTask = FALSE )
										TASK_FOLLOW_NAV_MESH_TO_COORD(ped.PedIndex, << 1705.28, 3290.59, 44.40 >>, PEDMOVE_WALK)
										ped.bHasTask = TRUE
									ENDIF
									
									IF ( ped.bHasTask = TRUE )
										IF IS_ENTITY_AT_COORD(ped.PedIndex, << 1705.28, 3290.59, 45.40 >>, << 1.0, 1.0, 1.0 >>)
											IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
												GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
											ENDIF
										ENDIF
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
									ENDIF
								
								BREAK

							ENDSWITCH 
						
						BREAK
						
						CASE PED_STATE_ALERTED
						
							IF ( ped.bHasTask = FALSE )
								IF HAS_ANIM_DICT_LOADED("missexile2reactions_to_gun_fire@standing@idle_a")
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									TASK_PLAY_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									ped.bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE PED_STATE_INVESTIGATING_RON
						
							IF ( ped.bHasTask = FALSE )
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 60.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 60.0)
								SET_PED_ACCURACY(ped.PedIndex, 100)
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex,0,0)
								
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.PedIndex, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
								ped.bHasTask = TRUE
							ENDIF

						BREAK
						
						CASE PED_STATE_COMBAT
							
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 1)
								SET_PED_SHOOT_RATE(ped.PedIndex, 175)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex, 0.9, 0.9)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_PED(ped.PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
						BREAK
						
					ENDSWITCH
				
				ENDIF
				
				IF IS_ENTITY_DEAD(ped.PedIndex)
					ped.vPosition =  GET_ENTITY_COORDS(ped.PedIndex, FALSE)
				ENDIF
				
			ENDIF
			
			IF ( ped.bForceBlipOn = FALSE )
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
				OR IS_ENTITY_PLAYING_ANIM(ped.PedIndex,"reaction@male_stand@big_variations@d", "react_big_variations_k") //1335272
					ped.bForceBlipOn = TRUE
				ENDIF
			ENDIF
			
			IF ( bPedBlipsHidden = FALSE )
				UPDATE_AI_PED_BLIP(ped.PedIndex, ped.EnemyBlipData, -1, NULL, ped.bForceBlipOn, FALSE, 500.0)
			ELSE
				CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    This procedure manages the fourth enemy in the sniping section. This is the enemy that comes walks to the bottom of the control tower.
/// PARAMS:
///    ped - 
///    iEnemyProgress - 
PROC MANAGE_FOURTH_SNIPING_ENEMY(PED_SNIPE_STRUCT &ped, INT &iEnemyProgress)
	

	SWITCH iEnemyProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(psEnemies[2].PedIndex)
				IF IS_ENTITY_DEAD(psEnemies[2].PedIndex)
				OR IS_PED_INJURED(psEnemies[2].PedIndex)

					IF ( bBikersAlerted = FALSE )
						
						iEnemyProgress++
					
					ENDIF
					
				ENDIF			
			ENDIF
		
		BREAK
		
		
		CASE 1
		
			REQUEST_MODEL(ped.ModelName)
			REQUEST_ANIM_DICT("misstrevor2ig_5b_p2")
			
			IF 	HAS_MODEL_LOADED(ped.ModelName)
			AND	HAS_ANIM_DICT_LOADED("misstrevor2ig_5b_p2")
			
				ped.PedIndex = CREATE_ENEMY_PED(ped.ModelName, ped.vPosition, ped.fHeading, rel_group_enemies, WEAPONTYPE_ASSAULTRIFLE,
												WEAPONCOMPONENT_AT_AR_FLSH, 135)
				
				ADD_DEADPOOL_TRIGGER(ped.PedIndex, TRV2_KILLS)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(ped.ModelName)

				GO_TO_PED_STATE(ped, PED_STATE_UNALERTED)
				RELEASE_AMBIENT_AUDIO_BANK()
							 
				iEnemyProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF DOES_ENTITY_EXIST(ped.PedIndex)
				IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, ped.sDebugName, 1.75)
						ENDIF
					#ENDIF
					
					IF ( ped.eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
						ENDIF
					ENDIF
				
					SWITCH ped.eState
					
						CASE PED_STATE_UNALERTED
						
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_SEEING_RANGE(ped.PedIndex, 25.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 25.0)
								
								SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
								SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)
								
								VECTOR 	vScenePosition, vSceneRotation
								
								vScenePosition 					= << 1714.915, 3293.133, 41.160 >> 
								vScenerotation					= << 0.0, 0.0, 72.12 >>
								bFourthSnipingEnemyAnimating 	= TRUE
								
								IF ( bSecondSnipingEnemyInvestigating = TRUE )
								
									IF NOT IS_AREA_OCCUPIED(<< 1701.31 - 5.0, 3306.22 - 5.0, 41.66 - 2.0 >>,
															<< 1701.31 + 5.0, 3306.22 + 5.0, 41.66 + 2.0 >>,
															FALSE, TRUE, FALSE, FALSE, FALSE, ped.PedIndex)
										
										vScenePosition 					= << 1717.115, 3302.883, 41.160 >>
										vScenerotation					= << 0.0, 0.0, 105.0 >>
										bFourthSnipingEnemyAnimating 	= TRUE
										
									ELSE
									
										bFourthSnipingEnemyAnimating = FALSE
												
									ENDIF
								ENDIF

								IF ( bFourthSnipingEnemyAnimating = TRUE )
								
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misstrevor2ig_5b_p2", "biker_find_body",
															vScenePosition, vSceneRotation, NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
															-1, AF_EXTRACT_INITIAL_OFFSET | AF_HOLD_LAST_FRAME)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									
								ELSE
								
									WARP_PED(ped.PedIndex, <<1701.2786, 3286.0637, 40.1481>>, 347.2772, FALSE, FALSE, FALSE)
									
									FORCE_PED_MOTION_STATE(ped.PedIndex, MS_ON_FOOT_WALK)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									SET_PED_CONFIG_FLAG(ped.PedIndex, PCF_DisablePedAvoidance, TRUE)
									
									SEQUENCE_INDEX SequenceIndex
									CLEAR_SEQUENCE_TASK(SequenceIndex)
									OPEN_SEQUENCE_TASK(SequenceIndex)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1702.51, 3289.76, 40.15 >>, 0.5, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1701.01, 3293.01, 40.15 >>, 0.5, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, psEnemies[0].vPosition, psEnemies[0].vPosition, 0.5, FALSE, 2.0)
										TASK_AIM_GUN_AT_COORD(NULL, psEnemies[0].vPosition, 5000)
									CLOSE_SEQUENCE_TASK(SequenceIndex)
									TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
									CLEAR_SEQUENCE_TASK(SequenceIndex)
									
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								
								ENDIF
								
								iTimeTillBlipTimer = GET_GAME_TIMER()
									
								ped.bHasTask = TRUE
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF ( bFourthSnipingEnemyAnimating = TRUE )
									IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_5b_p2", "biker_find_body")
										IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_5b_p2", "biker_find_body") >= 0.98
											bBikersAlerted = TRUE
											REMOVE_ANIM_DICT("misstrevor2ig_5b_p2")
											GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
										ENDIF
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
										REMOVE_ANIM_DICT("misstrevor2ig_5b_p2")
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
							OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
								REMOVE_ANIM_DICT("misstrevor2ig_5b_p2")
								GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
							ENDIF
						
						BREAK
						
						CASE PED_STATE_ALERTED
						
							IF ( ped.bHasTask = FALSE )
								IF HAS_ANIM_DICT_LOADED("missexile2reactions_to_gun_fire@standing@idle_a")
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									TASK_PLAY_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									ped.bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE PED_STATE_INVESTIGATING_RON
						
							IF ( ped.bHasTask = FALSE )
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 60.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 60.0)
								SET_PED_ACCURACY(ped.PedIndex, 100)
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex,0,0)
								
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.PedIndex, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
								ped.bHasTask = TRUE
							ENDIF

						BREAK
						
						CASE PED_STATE_COMBAT
							
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 1)
								SET_PED_SHOOT_RATE(ped.PedIndex, 175)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex, 0.9, 0.9)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_PED(ped.PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
						BREAK
						
					ENDSWITCH

				ELSE
				
					REMOVE_ANIM_DICT("misstrevor2ig_5b_p2")

				ENDIF
			ENDIF
			
			IF ( ped.bForceBlipOn = FALSE )
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
				OR (MANAGE_MY_TIMER(iTimeTillBlipTimer,iTimeTillBlip) AND ped.bHasTask)
				//or objective printed
					ped.bForceBlipOn = TRUE
				ENDIF
			ENDIF
		
			IF ( bPedBlipsHidden = FALSE )
				UPDATE_AI_PED_BLIP(ped.PedIndex, ped.EnemyBlipData, -1, NULL, ped.bForceBlipOn, FALSE, 500.0)
			ELSE
				CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    This procedure manages the fifth enemy in the sniping section. This is the second enemy coming out of the control tower.
/// PARAMS:
///    ped - 
///    iEnemyProgress - 
PROC MANAGE_FIFTH_SNIPING_ENEMY(PED_SNIPE_STRUCT &ped, INT &iEnemyProgress)

	SWITCH iEnemyProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(psEnemies[3].PedIndex)
				IF IS_ENTITY_DEAD(psEnemies[3].PedIndex)
				OR IS_PED_INJURED(psEnemies[3].PedIndex)
					
					IF ( bBikersAlerted = FALSE )
						
						ped.vPosition 				= << 1700.90, 3292.33, 47.92 >> 			//by default spawn the ped inside control tower
						ped.fHeading 				= 93.9332
						bFifthEnemySpawnedInTower 	= TRUE
						
						IF IS_AREA_OCCUPIED(<< 1696.90 - 2.0, 3291.38 - 2.0, 49.42 - 2.0 >>,	//if door area is oocupied by corpse
											<< 1696.90 + 2.0, 3291.38 + 2.0, 49.42 + 2.0 >>,	//of a dead ped then spawn this ped
											FALSE, FALSE, TRUE, FALSE, FALSE, ped.PedIndex)		//behind the wall of the control tower
																								//so it looks like he is coming from
							ped.vPosition 				= << 1705.29, 3291.67, 47.90 >>			//the other side of the tower
							ped.fHeading 				= 37.8532
							bFifthEnemySpawnedInTower 	= FALSE
								
						ENDIF
						
						iEnemyProgress++
						
					ENDIF
					
				ENDIF			
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(ped.ModelName)
			REQUEST_ANIM_DICT("guard_reactions")
			
			IF 	HAS_MODEL_LOADED(ped.ModelName)
			AND	HAS_ANIM_DICT_LOADED("guard_reactions")
			
				ped.PedIndex = CREATE_ENEMY_PED(ped.ModelName, ped.vPosition, ped.fHeading, rel_group_enemies, WEAPONTYPE_ASSAULTRIFLE,
												WEAPONCOMPONENT_AT_AR_FLSH, 135)
				
				ADD_DEADPOOL_TRIGGER(ped.PedIndex, TRV2_KILLS)
												
				SET_PED_CAPSULE(ped.PedIndex,0.4)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(ped.ModelName)

				GO_TO_PED_STATE(ped, PED_STATE_UNALERTED)
							 
				iEnemyProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF DOES_ENTITY_EXIST(ped.PedIndex)
				IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, ped.sDebugName, 1.75)
						ENDIF
					#ENDIF
					
					IF ( ped.eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
						ENDIF
					ENDIF
				
					SWITCH ped.eState
					
						CASE PED_STATE_UNALERTED
						
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_SEEING_RANGE(ped.PedIndex, 25.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 25.0)
								
								SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
								SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)
							
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									IF ( bFifthEnemySpawnedInTower = TRUE ) 
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 1697.77, 3291.67, 47.92 >>, PEDMOVE_WALK - 0.6)
									ENDIF
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, psEnemies[2].vPosition, PEDMOVE_WALK - 0.6, DEFAULT_TIME_BEFORE_WARP, 2.0, ENAV_SUPPRESS_EXACT_STOP)
									TASK_AIM_GUN_AT_COORD(NULL, psEnemies[2].vPosition, 2000)
									TASK_PLAY_ANIM(NULL,"guard_reactions", "med_down")
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								iTimeTillBlipTimer = GET_GAME_TIMER()
								ped.bHasTask = TRUE
							ENDIF
							
							//1334993
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "guard_reactions", "med_down")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "guard_reactions", "med_down") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
							OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
								GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
							ENDIF
						
						BREAK
						
						CASE PED_STATE_ALERTED
						
							IF ( ped.bHasTask = FALSE )
								IF HAS_ANIM_DICT_LOADED("missexile2reactions_to_gun_fire@standing@idle_a")
									GIVE_WEAPON_TO_PED(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE, TRUE)
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(ped.PedIndex)
									TASK_PLAY_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									ped.bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE PED_STATE_INVESTIGATING_RON
						
							IF ( ped.bHasTask = FALSE )
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 60.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 60.0)
								SET_PED_ACCURACY(ped.PedIndex, 100)
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex,0,0)
								
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 50)
								SET_PED_SHOOT_RATE(ped.PedIndex, 125)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 6.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 6.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.PedIndex, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
								ped.bHasTask = TRUE
							ENDIF

						BREAK
						
						CASE PED_STATE_COMBAT
							
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 1)
								SET_PED_SHOOT_RATE(ped.PedIndex, 175)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex, 0.9, 0.9)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_PED(ped.PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
						BREAK
						
					ENDSWITCH
				
				ENDIF
			ENDIF
			
			IF ( ped.bForceBlipOn = FALSE )
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
				OR (MANAGE_MY_TIMER(iTimeTillBlipTimer,iTimeTillBlip) AND ped.bHasTask)
				//or objective printed
					ped.bForceBlipOn = TRUE
				ENDIF
			ENDIF
			
			IF ( bPedBlipsHidden = FALSE )
				UPDATE_AI_PED_BLIP(ped.PedIndex, ped.EnemyBlipData, -1, NULL, ped.bForceBlipOn, FALSE, 500.0)
			ELSE
				CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    This procedure manages the sixth enemy in the sniping section. This is the first enemy coming out from a builing next to the gas tank.
/// PARAMS:
///    ped - 
///    iEnemyProgress - 
PROC MANAGE_SIXTH_SNIPING_ENEMY(PED_SNIPE_STRUCT &ped, INT &iEnemyProgress)

	SWITCH iEnemyProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(psEnemies[4].PedIndex)
				IF IS_ENTITY_DEAD(psEnemies[4].PedIndex)
				OR IS_PED_INJURED(psEnemies[4].PedIndex)

					IF ( bBikersAlerted = FALSE )

						iEnemyProgress++
						
					ENDIF
					
				ENDIF			
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(ped.ModelName)
			REQUEST_ANIM_DICT("misstrevor2ig_6")
			
			IF 	HAS_MODEL_LOADED(ped.ModelName)
			AND HAS_ANIM_DICT_LOADED("misstrevor2ig_6")
			
				IF ( bSixthSnipingEnemyAllowed = TRUE )
			
					ped.PedIndex = CREATE_ENEMY_PED(ped.ModelName, ped.vPosition, ped.fHeading, rel_group_enemies, WEAPONTYPE_ASSAULTRIFLE,
													WEAPONCOMPONENT_AT_AR_FLSH, 135)
					
					ADD_DEADPOOL_TRIGGER(ped.PedIndex, TRV2_KILLS)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(ped.ModelName)
					
					GO_TO_PED_STATE(ped, PED_STATE_UNALERTED)
								 
					iEnemyProgress++
				
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF DOES_ENTITY_EXIST(ped.PedIndex)
				IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, ped.sDebugName, 1.75)
						ENDIF
					#ENDIF
					
					IF ( ped.eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
						ENDIF
					ENDIF
				
					SWITCH ped.eState
					
						CASE PED_STATE_UNALERTED
						
							SWITCH ped.iThisPedProgress
							
								CASE 0
								
									IF ( ped.bHasTask = FALSE )
									
										SET_PED_SEEING_RANGE(ped.PedIndex, 25.0)
										SET_PED_HEARING_RANGE(ped.PedIndex, 25.0)
										
										SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
										SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)
									
										SEQUENCE_INDEX SequenceIndex
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_PLAY_ANIM(NULL, "misstrevor2ig_6", "call_biker_biker", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT, 0.32)
											TASK_PLAY_ANIM(NULL, "misstrevor2ig_6", "idle_f", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT)
											TASK_PLAY_ANIM(NULL, "misstrevor2ig_6", "idle_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, << 1683.85, 3299.42, 40.09 >>, << 1683.85, 3299.42, 41.0 >>, PEDMOVE_WALK, FALSE)
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
										iTimeTillBlipTimer = GET_GAME_TIMER()
										ped.bHasTask = TRUE
									ENDIF
									
									IF ( ped.bHasTask = TRUE )
										IF IS_ENTITY_AT_COORD(ped.PedIndex, << 1685.54, 3299.13, 42.61 >>, <<5.5, 5.5, 3.0 >>)
											IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
												GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
											ENDIF
										ENDIF
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
									ENDIF
								
								BREAK
							
							ENDSWITCH
						
						BREAK
						
						CASE PED_STATE_ALERTED
						
							IF ( ped.bHasTask = FALSE )
								IF HAS_ANIM_DICT_LOADED("missexile2reactions_to_gun_fire@standing@idle_a")
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									TASK_PLAY_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									ped.bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE PED_STATE_INVESTIGATING_RON
						
							IF ( ped.bHasTask = FALSE )
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 60.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 60.0)
								SET_PED_ACCURACY(ped.PedIndex, 100)
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex,0,0)
								
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.PedIndex, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
								ped.bHasTask = TRUE
							ENDIF

						BREAK
						
						CASE PED_STATE_COMBAT
							
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 1)
								SET_PED_SHOOT_RATE(ped.PedIndex, 175)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex, 0.9, 0.9)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_PED(ped.PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
						BREAK
						
					ENDSWITCH

				ENDIF
				
				IF IS_ENTITY_DEAD(ped.PedIndex)
					ped.vPosition = GET_ENTITY_COORDS(ped.PedIndex, FALSE)
				ENDIF
				
			ENDIF
			
			IF ( ped.bForceBlipOn = FALSE )
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
				OR (MANAGE_MY_TIMER(iTimeTillBlipTimer,iTimeTillBlip) AND ped.bHasTask)
				//or objective printed
					ped.bForceBlipOn = TRUE
				ENDIF
			ENDIF
		
			IF ( bPedBlipsHidden = FALSE )
				UPDATE_AI_PED_BLIP(ped.PedIndex, ped.EnemyBlipData, -1, NULL, ped.bForceBlipOn, FALSE, 500.0)
			ELSE
				CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
			ENDIF
			
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    This procedure manages the seventh enemy in the sniping section. This is the second enemy coming out from a builing next to the gas tank.
/// PARAMS:
///    ped - 
///    iEnemyProgress - 
PROC MANAGE_SEVENTH_SNIPING_ENEMY(PED_SNIPE_STRUCT &ped, INT &iEnemyProgress)

	SWITCH iEnemyProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(psEnemies[5].PedIndex)
				IF IS_ENTITY_DEAD(psEnemies[5].PedIndex)
				OR IS_PED_INJURED(psEnemies[5].PedIndex)
					
					IF ( bBikersAlerted = FALSE )
					
						iEnemyProgress++
						
					ENDIF
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(ped.ModelName)
			
			IF 	HAS_MODEL_LOADED(ped.ModelName)
			
				ped.PedIndex = CREATE_ENEMY_PED(ped.ModelName, ped.vPosition, ped.fHeading, rel_group_enemies, WEAPONTYPE_ASSAULTRIFLE,
												WEAPONCOMPONENT_AT_AR_FLSH, 135)
				
				ADD_DEADPOOL_TRIGGER(ped.PedIndex, TRV2_KILLS)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(ped.ModelName)

				GO_TO_PED_STATE(ped, PED_STATE_UNALERTED)
							 
				iEnemyProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF DOES_ENTITY_EXIST(ped.PedIndex)
				IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, ped.sDebugName, 1.75)
						ENDIF
					#ENDIF
					
					IF ( ped.eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
						ENDIF
					ENDIF
				
					SWITCH ped.eState
				
						CASE PED_STATE_UNALERTED
						
							SWITCH ped.iThisPedProgress
							
								CASE 0
								
									IF ( ped.bHasTask = FALSE )
									
										SET_PED_SEEING_RANGE(ped.PedIndex, 25.0)
										SET_PED_HEARING_RANGE(ped.PedIndex, 25.0)
										
										SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
										SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)
									
										SEQUENCE_INDEX SequenceIndex
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1682.43, 3284.91, 40.10 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, psEnemies[5].vPosition, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 3.0, ENAV_SUPPRESS_EXACT_STOP)
											TASK_AIM_GUN_AT_COORD(NULL, psEnemies[5].vPosition, 3000)
											TASK_PLAY_ANIM(NULL,"guard_reactions", "med_down")
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)

										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
										iTimeTillBlipTimer = GET_GAME_TIMER()
										ped.bHasTask = TRUE
									ENDIF
									
									IF ( ped.bHasTask = TRUE )
										IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
											GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
										ENDIF
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
									ENDIF
								
								BREAK
							
							ENDSWITCH
						
						BREAK
						
						CASE PED_STATE_ALERTED
						
							IF ( ped.bHasTask = FALSE )
								IF HAS_ANIM_DICT_LOADED("missexile2reactions_to_gun_fire@standing@idle_a")
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									TASK_PLAY_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									ped.bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE PED_STATE_INVESTIGATING_RON
						
							IF ( ped.bHasTask = FALSE )
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 60.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 60.0)
								SET_PED_ACCURACY(ped.PedIndex, 100)
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex,0,0)
								
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.PedIndex, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
								ped.bHasTask = TRUE
							ENDIF

						BREAK
						
						CASE PED_STATE_COMBAT
							
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 1)
								SET_PED_SHOOT_RATE(ped.PedIndex, 175)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex, 0.9, 0.9)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_PED(ped.PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
						BREAK
					
					ENDSWITCH
					
					
				ENDIF
			ENDIF
			
			IF ( ped.bForceBlipOn = FALSE )
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
				OR (MANAGE_MY_TIMER(iTimeTillBlipTimer,iTimeTillBlip) AND ped.bHasTask)
				//or objective printed
					ped.bForceBlipOn = TRUE
				ENDIF
			ENDIF
			
			IF ( bPedBlipsHidden = FALSE )
				UPDATE_AI_PED_BLIP(ped.PedIndex, ped.EnemyBlipData, -1, NULL, ped.bForceBlipOn, FALSE, 500.0)
			ELSE
				CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    This procedure manages the eight enemy in the sniping section. This is the enemy driving into the area on hexer motorbike.
/// PARAMS:
///    ped - 
///    iEnemyProgress - 
PROC MANAGE_EIGHTH_SNIPING_ENEMY(PED_SNIPE_STRUCT &ped, INT &iEnemyProgress)

	SWITCH iEnemyProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(psEnemies[6].PedIndex)
				IF IS_ENTITY_DEAD(psEnemies[6].PedIndex)
				OR IS_PED_INJURED(psEnemies[6].PedIndex)
					
					IF ( bBikersAlerted = FALSE )
						REMOVE_ANIM_DICT("guard_reactions")
						iEnemyProgress++
						
					ENDIF
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(ped.ModelName)
			REQUEST_MODEL(vsEnemyBikes[0].ModelName)
			REQUEST_VEHICLE_RECORDING(vsEnemyBikes[0].iRecordingNumber, sVehicleRecordingsFile)
			
			IF 	HAS_MODEL_LOADED(ped.ModelName)
			AND HAS_MODEL_LOADED(vsEnemyBikes[0].ModelName)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(vsEnemyBikes[0].iRecordingNumber, sVehicleRecordingsFile)
			
				IF ( bEighthSnipingEnemyAllowed = TRUE )
			
					vsEnemyBikes[0].VehicleIndex = CREATE_VEHICLE(vsEnemyBikes[0].ModelName, vsEnemyBikes[0].vPosition, vsEnemyBikes[0].fHeading)
				
					ped.PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(vsEnemyBikes[0].VehicleIndex, ped.ModelName, rel_group_enemies, VS_DRIVER,
															   WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH, 135)
					
					ADD_DEADPOOL_TRIGGER(ped.PedIndex, TRV2_KILLS)										   
															   
					SET_VEHICLE_ON_GROUND_PROPERLY(vsEnemyBikes[0].VehicleIndex)
					SET_PED_HELMET(ped.PedIndex, false)
					SET_MODEL_AS_NO_LONGER_NEEDED(ped.ModelName)
					SET_MODEL_AS_NO_LONGER_NEEDED(vsEnemyBikes[0].ModelName)

					GO_TO_PED_STATE(ped, PED_STATE_UNALERTED)
								 
					iEnemyProgress++
					
				ENDIF
																	
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF IS_VEHICLE_DRIVEABLE(vsEnemyBikes[0].VehicleIndex)
				IF IS_HORN_ACTIVE(vsEnemyBikes[0].VehicleIndex)
					bBikersAlerted = TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vsEnemyBikes[0].VehicleIndex)
				IF NOT IS_ENTITY_DEAD(vsEnemyBikes[0].VehicleIndex)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyBikes[0].VehicleIndex)
						fEighthEnemyPlaybackTime = GET_TIME_POSITION_IN_RECORDING(vsEnemyBikes[0].VehicleIndex)
					ENDIF
				ENDIF
			ENDIF
			
			PRINTFLOAT(fEighthEnemyPlaybackTime)PRINTNL()
		
			IF DOES_ENTITY_EXIST(ped.PedIndex)
				IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_VEHICLE_RECORDING_INFO(vsEnemyBikes[0].VehicleIndex, 1.0)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, ped.sDebugName, 1.75)
						ENDIF
					#ENDIF
				
					IF ( ped.eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							IF NOT IS_PED_IN_ANY_VEHICLE(ped.PedIndex)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT)
							ENDIF
						ENDIF
					ENDIF
				
					SWITCH ped.eState
					
						CASE PED_STATE_UNALERTED
						
							SWITCH ped.iThisPedProgress
						
								CASE 0
						
									IF ( ped.bHasTask = FALSE )
										IF IS_VEHICLE_DRIVEABLE(vsEnemyBikes[0].VehicleIndex)
											IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyBikes[0].VehicleIndex) 
												
												SET_VEHICLE_ENGINE_ON(vsEnemyBikes[0].VehicleIndex, TRUE, TRUE)
												SET_VEHICLE_LIGHTS(vsEnemyBikes[0].VehicleIndex, FORCE_VEHICLE_LIGHTS_ON)
												
												SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vsEnemyBikes[0].VehicleIndex, TRUE)
												START_PLAYBACK_RECORDED_VEHICLE(vsEnemyBikes[0].VehicleIndex, vsEnemyBikes[0].iRecordingNumber, sVehicleRecordingsFile)
												SET_PLAYBACK_SPEED(vsEnemyBikes[0].VehicleIndex, 0.7)
												
												SET_PED_SEEING_RANGE(ped.PedIndex, 15.0)
												SET_PED_HEARING_RANGE(ped.PedIndex, 15.0)
										
												SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
												SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)

												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
												iTimeTillBlipTimer = GET_GAME_TIMER()
												
											ELSE
												ped.bHasTask = TRUE
											ENDIF
										ENDIF
									ENDIF									
									
									IF ( ped.bHasTask = TRUE )
										IF IS_VEHICLE_DRIVEABLE(vsEnemyBikes[0].VehicleIndex)
											IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyBikes[0].VehicleIndex)
											OR fEighthEnemyPlaybackTime > 8280.525391
												IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyBikes[0].VehicleIndex)
													STOP_PLAYBACK_RECORDED_VEHICLE(vsEnemyBikes[0].VehicleIndex)
												ENDIF
												REMOVE_VEHICLE_RECORDING(vsEnemyBikes[0].iRecordingNumber, sVehicleRecordingsFile)
												ped.bHasTask = FALSE
												ped.iThisPedProgress++
												FREEZE_ENTITY_POSITION(vsEnemyBikes[0].VehicleIndex,TRUE)
											ENDIF									
										ENDIF
									ENDIF
									
									IF IS_VEHICLE_DRIVEABLE(vsEnemyBikes[0].VehicleIndex)
										IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 2.0) 
										OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_USING(ped.PedIndex), PLAYER_PED_ID())
										OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vsEnemyBikes[0].VehicleIndex,PLAYER_PED_ID())
											GO_TO_PED_STATE(ped, PED_STATE_ALERTED)									
										ENDIF
									ENDIF

								BREAK
								
								CASE 1
								
									IF ( ped.bHasTask = FALSE )
										
										SEQUENCE_INDEX SequenceIndex
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_LEAVE_ANY_VEHICLE(NULL)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1671.96, 3260.97, 40.60 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
										
										ped.bHasTask = TRUE
									ENDIF
									
									IF ( ped.bHasTask = TRUE )
										IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
										OR IS_ENTITY_AT_COORD(ped.PedIndex,<< 1671.96, 3260.97, 40.60 >>,<<2,2,2>>)
											GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
										ENDIF
									ENDIF
									
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 5.0)
									OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 10.0) 
										GO_TO_PED_STATE(ped, PED_STATE_ALERTED)
									ENDIF
								
								BREAK
								
							ENDSWITCH
							
						BREAK
						
						CASE PED_STATE_ALERTED
							
							IF ( ped.bHasTask = FALSE )
								REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE,ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS),WEAPON_COMPONENT_FLASH)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyBikes[0].VehicleIndex)
									STOP_PLAYBACK_RECORDED_VEHICLE(vsEnemyBikes[0].VehicleIndex)
									REMOVE_VEHICLE_RECORDING(vsEnemyBikes[0].iRecordingNumber, sVehicleRecordingsFile)
								ENDIF
								REQUEST_ANIM_DICT("missexile2reactions_to_gun_fire@standing@idle_a")
								IF HAS_ANIM_DICT_LOADED("missexile2reactions_to_gun_fire@standing@idle_a")
								AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
								
									GIVE_WEAPON_TO_PED(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE, TRUE)
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(ped.PedIndex)
									
									SEQUENCE_INDEX SequenceIndex
								
									CLEAR_SEQUENCE_TASK(SequenceIndex)
									OPEN_SEQUENCE_TASK(SequenceIndex)
										TASK_LEAVE_ANY_VEHICLE(NULL)
										TASK_PLAY_ANIM(NULL, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									CLOSE_SEQUENCE_TASK(SequenceIndex)
									TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
									CLEAR_SEQUENCE_TASK(SequenceIndex)
									
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									ped.bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missexile2reactions_to_gun_fire@standing@idle_a", "standing_idle_a") >= 0.98
										GO_TO_PED_STATE(ped, PED_STATE_INVESTIGATING_RON)
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE PED_STATE_INVESTIGATING_RON
						
							IF ( ped.bHasTask = FALSE )
								
								SET_PED_SEEING_RANGE(ped.PedIndex, 60.0)
								SET_PED_HEARING_RANGE(ped.PedIndex, 60.0)
								SET_PED_ACCURACY(ped.PedIndex, 100)
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex,0,0)
								
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 5.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.PedIndex, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
								ped.bHasTask = TRUE
							ENDIF

						BREAK
						
						CASE PED_STATE_COMBAT
							
							IF ( ped.bHasTask = FALSE )
							
								SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
								SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(ped.PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
								
								SET_PED_ACCURACY(ped.PedIndex, 1)
								SET_PED_SHOOT_RATE(ped.PedIndex, 175)
								
								SET_PED_CHANCE_OF_FIRING_BLANKS(ped.PedIndex, 0.9, 0.9)
								SET_PED_FIRING_PATTERN(ped.PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
								SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
								
								TASK_COMBAT_PED(ped.PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								
								ped.bHasTask = TRUE
								
							ENDIF
							
						BREAK
						
					ENDSWITCH
				
				ENDIF
			ENDIF
			
			IF ( ped.bForceBlipOn = FALSE )
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
				OR (MANAGE_MY_TIMER(iTimeTillBlipTimer,iTimeTillBlip) AND ped.bHasTask)
				//or objective printed
					ped.bForceBlipOn = TRUE
				ENDIF
			ENDIF
			
			IF ( bPedBlipsHidden = FALSE )
				IF ( ped.bForceBlipOn = TRUE )
					UPDATE_AI_PED_BLIP(ped.PedIndex, ped.EnemyBlipData, -1, NULL, ped.bForceBlipOn, FALSE, 500.0)
				ENDIF
			ELSE
				CLEANUP_AI_PED_BLIP(ped.EnemyBlipData)
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC MANAGE_ENEMY_MAVERICK(INT &iThisProgress)
	
	LEAVE_MISSION_CHECKS(<< 1575.0476, 3363.5938, 47.6350 >>,10,15,"SJC_RETRW","SJC_DISTWARN1",MISSION_ABADONED)

	SWITCH iThisProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(psEnemies[7].PedIndex)
				IF IS_ENTITY_DEAD(psEnemies[7].PedIndex)
				OR IS_PED_INJURED(psEnemies[7].PedIndex)

					IF ( bBikersAlerted = FALSE )
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_HELI_SILENT")
							START_AUDIO_SCENE("TREVOR_2_HELI_SILENT")
						ENDIF

						iThisProgress++
						
					ENDIF
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(mission_veh[MV_MAVERICK].Model)
			REQUEST_MODEL(psMaverickEnemies[0].ModelName)
			REQUEST_MODEL(psMaverickEnemies[1].ModelName)
			
			REQUEST_ANIM_DICT("misstrevor2mcs_3_b")
			
			REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE))
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE,ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS),WEAPON_COMPONENT_FLASH)
			REQUEST_VEHICLE_RECORDING(mission_veh[MV_MAVERICK].iRecordingNumber, sVehicleRecordingsFile)
			
			IF 	HAS_MODEL_LOADED(mission_veh[MV_MAVERICK].Model)
			AND HAS_MODEL_LOADED(psMaverickEnemies[0].ModelName)
			AND	HAS_MODEL_LOADED(psMaverickEnemies[1].ModelName)
			AND	HAS_MODEL_LOADED(psMaverickEnemies[1].ModelName)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
			AND HAS_ANIM_DICT_LOADED("misstrevor2mcs_3_b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(mission_veh[MV_MAVERICK].iRecordingNumber, sVehicleRecordingsFile)
			
				mission_veh[MV_MAVERICK].veh = CREATE_VEHICLE(mission_veh[MV_MAVERICK].Model, mission_veh[MV_MAVERICK].loc, mission_veh[MV_MAVERICK].head)
				
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MAVERICK].veh,"TREVOR_2_Helicopter")
			
				psMaverickEnemies[0].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mission_veh[MV_MAVERICK].veh, psMaverickEnemies[0].ModelName, rel_group_enemies,
																			VS_DRIVER, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 135)
				
				ADD_DEADPOOL_TRIGGER(psMaverickEnemies[0].PedIndex, TRV2_KILLS)																			
																			
				psMaverickEnemies[1].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mission_veh[MV_MAVERICK].veh, psMaverickEnemies[1].ModelName, rel_group_enemies,
																			VS_BACK_RIGHT, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH, 135)
																			
				ADD_DEADPOOL_TRIGGER(psMaverickEnemies[1].PedIndex, TRV2_KILLS)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(mission_veh[MV_MAVERICK].model)
				SET_MODEL_AS_NO_LONGER_NEEDED(psMaverickEnemies[0].ModelName)
				SET_MODEL_AS_NO_LONGER_NEEDED(psMaverickEnemies[1].ModelName)
				
				#IF IS_DEBUG_BUILD
					psMaverickEnemies[0].sDebugName = "Maverick 0"
					psMaverickEnemies[0].sDebugName = "Maverick 1"
				#ENDIF
								
				GO_TO_PED_STATE(psMaverickEnemies[0], PED_STATE_UNALERTED)
				GO_TO_PED_STATE(psMaverickEnemies[1], PED_STATE_UNALERTED)
							 
				iThisProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			//do railgun checks, railgun seems to have explosion issues when impacting far away so explode the heli upon detecting impact, see B*1986699
			
			BOOL bRailgunImpactDetected
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
				IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(mission_veh[MV_MAVERICK].veh, WEAPONTYPE_DLC_RAILGUN)
						bRailgunImpactDetected = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": WEAPONTYPE_DLC_RAILGUN has damaged enemy helicopter.")
						#ENDIF
					ENDIF
				ELSE
					IF GET_VEHICLE_CAUSE_OF_DESTRUCTION(mission_veh[MV_MAVERICK].veh) = WEAPONTYPE_DLC_RAILGUN
						bRailgunImpactDetected = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": WEAPONTYPE_DLC_RAILGUN is cause of destruction of enemy helicopter.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
				IF NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(psMaverickEnemies[0].PedIndex, WEAPONTYPE_DLC_RAILGUN)
						bRailgunImpactDetected = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": WEAPONTYPE_DLC_RAILGUN has damaged enemy 0 in helicopter.")
						#ENDIF
					ENDIF
				ELSE
					IF GET_PED_CAUSE_OF_DEATH(psMaverickEnemies[0].PedIndex) = WEAPONTYPE_DLC_RAILGUN
						bRailgunImpactDetected = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": WEAPONTYPE_DLC_RAILGUN is cause of death of enemy 0 in helicopter.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psMaverickEnemies[1].PedIndex)
				IF NOT IS_ENTITY_DEAD(psMaverickEnemies[1].PedIndex)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(psMaverickEnemies[1].PedIndex, WEAPONTYPE_DLC_RAILGUN)
						bRailgunImpactDetected = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": WEAPONTYPE_DLC_RAILGUN has damaged enemy 1 in helicopter.")
						#ENDIF
					ENDIF
				ELSE
					IF GET_PED_CAUSE_OF_DEATH(psMaverickEnemies[1].PedIndex) = WEAPONTYPE_DLC_RAILGUN
						bRailgunImpactDetected = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": WEAPONTYPE_DLC_RAILGUN is cause of death of enemy 1 in helicopter.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF ( bRailgunImpactDetected = TRUE )
				IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
					//IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
						bBikersAlerted = TRUE
						EXPLODE_VEHICLE(mission_veh[MV_MAVERICK].veh, TRUE)
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling EXPLODE_VEHICLE() for the enemy helicopter due to WEAPONTYPE_DLC_RAILGUN impact.")
						#ENDIF
					//ENDIF
				ENDIF
			ENDIF
			
			//end railgun checks
		
			IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
				IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
				
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MAVERICK].veh)
						fMaverickPlaybackTime = GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_MAVERICK].veh)
					ENDIF
					
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MAVERICK].veh, PLAYER_PED_ID(), FALSE)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(mission_veh[MV_MAVERICK].veh, WEAPONTYPE_SNIPERRIFLE)
						OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(mission_veh[MV_MAVERICK].veh, WEAPONTYPE_HEAVYSNIPER)
							iMaverickShotsCount++
							INFORM_MISSION_STATS_OF_INCREMENT(TRV2_SHOTS_TO_KILL_HELI)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mission_veh[MV_MAVERICK].veh)
						ENDIF
					ENDIF
					
					IF ( iMaverickShotsCount >= 4 )
						IF DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
							IF NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
								SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(psMaverickEnemies[0].PedIndex, <<0.0, 1.0, 0.0>>), GET_PED_BONE_COORDS(psMaverickEnemies[0].PedIndex, BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 20, TRUE)
								APPLY_DAMAGE_TO_PED(psMaverickEnemies[0].PedIndex, 100, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			IF ( iMaverickAlertTimer != 0 )
				IF HAS_TIME_PASSED(12500, iMaverickAlertTimer)
					bBikersAlerted = TRUE
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
				IF NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_VEHICLE_RECORDING_INFO(mission_veh[MV_MAVERICK].veh, 1.0)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(mission_veh[MV_MAVERICK].veh, GET_STRING_FROM_INT(iMaverickShotsCount), 0.0)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(psMaverickEnemies[0].PedIndex, GET_STRING_FROM_INT(psMaverickEnemies[0].iThisPedProgress), 1.75)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(psMaverickEnemies[0].PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(psMaverickEnemies[0].eState), 2.0)
							DRAW_DEBUG_SPHERE(GET_PED_BONE_COORDS(psMaverickEnemies[0].PedIndex, BONETAG_HEAD, << 0.0, 0.0, 0.0 >>), fMaverickPilotHeadShotRadius, 0, 255, 0, 64)
						ENDIF
					#ENDIF
					
					IF ( psMaverickEnemies[0].eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							GO_TO_PED_STATE(psMaverickEnemies[0], PED_STATE_COMBAT)
						ENDIF
					ENDIF
					
					SWITCH psMaverickEnemies[0].eState
					
						CASE PED_STATE_UNALERTED
						
							SWITCH psMaverickEnemies[0].iThisPedProgress
						
								CASE 0
						
									IF ( psMaverickEnemies[0].bHasTask = FALSE )
										IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MAVERICK].veh)
											IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MAVERICK].veh)
												
												SET_VEHICLE_ENGINE_ON(mission_veh[MV_MAVERICK].veh, TRUE, TRUE)
												SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_MAVERICK].veh)
												SET_VEHICLE_LIGHTS(mission_veh[MV_MAVERICK].veh, FORCE_VEHICLE_LIGHTS_ON)
												
												SET_VEHICLE_ACTIVE_DURING_PLAYBACK(mission_veh[MV_MAVERICK].veh, TRUE)
												START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_MAVERICK].veh, mission_veh[MV_MAVERICK].iRecordingNumber, sVehicleRecordingsFile)
												SET_PLAYBACK_SPEED(mission_veh[MV_MAVERICK].veh, 0.6)
												
												SET_PED_SEEING_RANGE(psMaverickEnemies[0].PedIndex, 15.0)
												SET_PED_HEARING_RANGE(psMaverickEnemies[0].PedIndex, 15.0)
										
												SET_ENTITY_LOD_DIST(psMaverickEnemies[0].PedIndex, 500)
												SET_ENTITY_LOAD_COLLISION_FLAG(psMaverickEnemies[0].PedIndex, TRUE)

												TASK_PLAY_ANIM(psMaverickEnemies[0].PedIndex, "misstrevor2mcs_3_b", "idle_peda", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psMaverickEnemies[0].PedIndex, TRUE)
												
											ELSE
												psMaverickEnemies[0].bHasTask = TRUE
											ENDIF
										ENDIF
									ENDIF
									
									IF ( psMaverickEnemies[0].bHasTask = TRUE )
									
										IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(psMaverickEnemies[0].PedIndex), 2.5)
										OR ( DOES_ENTITY_EXIST(psMaverickEnemies[1].PedIndex) AND IS_ENTITY_DEAD(psMaverickEnemies[1].PedIndex) )
										OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_USING(psMaverickEnemies[0].PedIndex), PLAYER_PED_ID())
											iMaverickReleaseTimer = GET_GAME_TIMER()	
										ENDIF
										
										IF ( iMaverickReleaseTimer != 0 )
											IF HAS_TIME_PASSED(3000, iMaverickReleaseTimer)
												iMaverickAlertTimer = GET_GAME_TIMER()
												GO_TO_PED_STATE(psMaverickEnemies[0], PED_STATE_COMBAT)
											ENDIF
										ENDIF
																				
									ENDIF
									
								BREAK
								
							ENDSWITCH
							
						BREAK
						
						CASE PED_STATE_COMBAT
						
							IF ( psMaverickEnemies[0].bHasTask = FALSE )
							
								IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
									IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
									
										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MAVERICK].veh)
											STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_MAVERICK].veh)
											REMOVE_VEHICLE_RECORDING(mission_veh[MV_MAVERICK].iRecordingNumber, sVehicleRecordingsFile)
										ENDIF
										
										TASK_HELI_MISSION(psMaverickEnemies[0].PedIndex, mission_veh[MV_MAVERICK].veh, NULL,
														  PLAYER_PED_ID(), << 0.0, 0.0, 0.0 >>, MISSION_ATTACK, 18, 2.0, -1,
														  CEIL(GET_ENTITY_HEIGHT_ABOVE_GROUND(mission_veh[MV_MAVERICK].veh)) + 10,
														  CEIL(GET_ENTITY_HEIGHT_ABOVE_GROUND(mission_veh[MV_MAVERICK].veh)) - 10)		
									
									ENDIF
								ENDIF
								
								psMaverickEnemies[0].bHasTask = TRUE	
							ENDIF
						
						BREAK
						
					ENDSWITCH

					IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(psMaverickEnemies[0].PedIndex, BONETAG_HEAD, << 0.0, 0.0, 0.0 >>), fMaverickPilotHeadShotRadius)
					OR HAS_BULLET_IMPACTED_IN_AREA(GET_PED_BONE_COORDS(psMaverickEnemies[0].PedIndex, BONETAG_HEAD, << 0.0, 0.0, 0.0 >>), fMaverickPilotHeadShotRadius)
						APPLY_DAMAGE_TO_PED(psMaverickEnemies[0].PedIndex, 100, TRUE)
					ENDIF
				
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psMaverickEnemies[1].PedIndex)
				IF NOT IS_ENTITY_DEAD(psMaverickEnemies[1].PedIndex)
				
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugStates = TRUE )
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(psMaverickEnemies[1].PedIndex, GET_STRING_FROM_INT(psMaverickEnemies[1].iThisPedProgress), 1.25)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(psMaverickEnemies[1].PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(psMaverickEnemies[1].eState), 1.5)
						ENDIF
					#ENDIF
					
					IF ( psMaverickEnemies[1].eState != PED_STATE_COMBAT )
						IF ( bBikersAlerted = TRUE )
							GO_TO_PED_STATE(psMaverickEnemies[1], PED_STATE_COMBAT)
						ENDIF
					ENDIF
					
					SWITCH psMaverickEnemies[1].eState
					
						CASE PED_STATE_UNALERTED
						
							SWITCH psMaverickEnemies[1].iThisPedProgress
						
								CASE 0
						
									IF ( psMaverickEnemies[1].bHasTask = FALSE )
								
										SET_ENTITY_LOD_DIST(psMaverickEnemies[1].PedIndex, 500)
										SET_ENTITY_LOAD_COLLISION_FLAG(psMaverickEnemies[1].PedIndex, TRUE)
										
										GIVE_WEAPON_TO_PED(psMaverickEnemies[1].PedIndex, WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE, TRUE)
										SET_CURRENT_PED_WEAPON(psMaverickEnemies[1].PedIndex, WEAPONTYPE_ASSAULTRIFLE, TRUE)
									
										//attach weapon object to his hand
										LostSniperWeapon = //GET_WEAPON_OBJECT_FROM_PED(psMaverickEnemies[1].PedIndex)
										CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(psMaverickEnemies[1].PedIndex, WEAPONTYPE_ASSAULTRIFLE)
										ATTACH_ENTITY_TO_ENTITY(LostSniperWeapon, psMaverickEnemies[1].PedIndex,
																GET_PED_BONE_INDEX(psMaverickEnemies[1].PedIndex, BONETAG_PH_R_HAND),
																<< 0.12, 0.05, -0.01 >>, << -90.0, -10.8, 0.0 >>, TRUE, TRUE, FALSE)
									
										TASK_PLAY_ANIM(psMaverickEnemies[1].PedIndex, "misstrevor2mcs_3_b", "idle_pedb", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psMaverickEnemies[1].PedIndex, TRUE)
									
										psMaverickEnemies[1].bHasTask = TRUE
									ENDIF
									
									IF ( psMaverickEnemies[1].bHasTask = TRUE )
									
										IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(psMaverickEnemies[1].PedIndex), 2.5)
										OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_USING(psMaverickEnemies[1].PedIndex), PLAYER_PED_ID())
											iMaverickReleaseTimer = GET_GAME_TIMER()								
										ENDIF
										
										IF ( iMaverickReleaseTimer != 0 )
											IF HAS_TIME_PASSED(3000, iMaverickReleaseTimer)
												iMaverickAlertTimer = GET_GAME_TIMER()
												GO_TO_PED_STATE(psMaverickEnemies[1], PED_STATE_COMBAT)
											ENDIF
										ENDIF
										
										IF ( fMaverickPlaybackTime > 67500.0 )
											bBikersAlerted = TRUE
											GO_TO_PED_STATE(psMaverickEnemies[1], PED_STATE_COMBAT)		
										ENDIF
										
									ENDIF
									
								BREAK
								
							ENDSWITCH
							
						BREAK
						
						CASE PED_STATE_COMBAT
						
							IF ( psMaverickEnemies[1].bHasTask = FALSE )
								
								SET_PED_ACCURACY(psMaverickEnemies[1].PedIndex, 3)
								SET_PED_SHOOT_RATE(psMaverickEnemies[1].PedIndex, 150)
								
								SET_PED_SEEING_RANGE(psMaverickEnemies[1].PedIndex, 100)
								SET_PED_HEARING_RANGE(psMaverickEnemies[1].PedIndex, 100)
								
								SET_PED_FIRING_PATTERN(psMaverickEnemies[1].PedIndex, FIRING_PATTERN_FULL_AUTO)
								
								SET_COMBAT_FLOAT(psMaverickEnemies[1].PedIndex, CCF_MAX_SHOOTING_DISTANCE, 100.0)
								SET_COMBAT_FLOAT(psMaverickEnemies[1].PedIndex, CCF_MIN_DISTANCE_TO_TARGET, 100.0)
								
								SET_PED_COMBAT_ATTRIBUTES(psMaverickEnemies[1].PedIndex, CA_ALWAYS_FIGHT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(psMaverickEnemies[1].PedIndex, CA_USE_VEHICLE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(psMaverickEnemies[1].PedIndex, CA_LEAVE_VEHICLES, FALSE)
								
								SET_PED_COMBAT_ATTRIBUTES(psMaverickEnemies[1].PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(psMaverickEnemies[1].PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(psMaverickEnemies[1].PedIndex, 250.0)
								IF NOT IS_PED_INJURED(psRon.PedIndex)
									TASK_DRIVE_BY(psMaverickEnemies[1].PedIndex,psRon.PedIndex,NULL,<<0,0,0>>,-1,60)
								ENDIF
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psMaverickEnemies[1].PedIndex, FALSE)
							
								psMaverickEnemies[1].bHasTask = TRUE
							ENDIF
						
						BREAK
						
					ENDSWITCH
					
				
				ENDIF
			ENDIF
			
			UPDATE_AI_PED_BLIP(psMaverickEnemies[0].PedIndex, psMaverickEnemies[0].EnemyBlipData, -1, NULL, TRUE, FALSE, 500.0)
			UPDATE_AI_PED_BLIP(psMaverickEnemies[1].PedIndex, psMaverickEnemies[1].EnemyBlipData, -1, NULL, TRUE, FALSE, 500.0)
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_LIGHT_PROP_OBJECTS(OBJECT_SNIPE_STRUCT &props[])
	
	//1335140
//	PRINTSTRING("osPropWallLights[0].bBroken:")PRINTBOOL(osPropWallLights[0].bBroken)PRINTNL()
//	PRINTSTRING("osPropWallLights[1].bBroken:")PRINTBOOL(osPropWallLights[1].bBroken)PRINTNL()
//	
//	PRINTSTRING("fSecondEnemyPlaybackTime:")PRINTFLOAT(fSecondEnemyPlaybackTime)PRINTNL()
	
	IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
		IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[0].VehicleIndex)
				IF fSecondEnemyPlaybackTime > 9148.754883
					IF ( osPropWallLights[0].bBroken = FALSE AND osPropWallLights[1].bBroken = FALSE )
						IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_LIGHTSBL")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF osPropWallLights[0].bBroken = FALSE
									IF NOT DOES_BLIP_EXIST(osPropWallLights[0].BlipIndex)
										osPropWallLights[0].BlipIndex = ADD_BLIP_FOR_COORD(<<1700.94, 3295.07, 45.95>>)
										SET_BLIP_COLOUR(osPropWallLights[0].BlipIndex,BLIP_COLOUR_GREEN)
										SET_BLIP_SCALE(osPropWallLights[0].BlipIndex,0.5)
										SET_BLIP_NAME_FROM_TEXT_FILE(osPropWallLights[0].BlipIndex,"SJC_LIGHTSBLI") 
									ENDIF
								ENDIF
								IF osPropWallLights[1].bBroken = FALSE
									IF NOT DOES_BLIP_EXIST(osPropWallLights[1].BlipIndex)
										osPropWallLights[1].BlipIndex = ADD_BLIP_FOR_COORD(<<1697.88, 3293.11, 47.24>>)
										SET_BLIP_COLOUR(osPropWallLights[1].BlipIndex,BLIP_COLOUR_GREEN)
										SET_BLIP_SCALE(osPropWallLights[1].BlipIndex,0.5)
										SET_BLIP_NAME_FROM_TEXT_FILE(osPropWallLights[1].BlipIndex,"SJC_LIGHTSBLI") 
									ENDIF
								ENDIF
								PRINT_GOD_TEXT("SJC_LIGHTSBL")
								SET_LABEL_AS_TRIGGERED("SJC_LIGHTSBL",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF osPropWallLights[0].bBroken
		IF DOES_BLIP_EXIST(osPropWallLights[0].BlipIndex)
			REMOVE_BLIP(osPropWallLights[0].BlipIndex)
		ENDIF
	ENDIF
	
	IF osPropWallLights[1].bBroken
		IF DOES_BLIP_EXIST(osPropWallLights[1].BlipIndex)
			REMOVE_BLIP(osPropWallLights[1].BlipIndex)
		ENDIF
	ENDIF
	
	IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_LIGHTSBL")
		IF osPropWallLights[0].bBroken
		AND osPropWallLights[1].bBroken
			CLEAR_PRINTS()
		ENDIF
	ENDIF

	INT i

	REPEAT COUNT_OF(props) i
	
		#IF IS_DEBUG_BUILD
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_COORDS(props[i].vPosition, GET_STRING_FROM_INT(i), 0.5)
				IF ( props[i].bBroken = FALSE )
					DRAW_DEBUG_TEXT_ABOVE_COORDS(props[i].vPosition, "ON", 1.0)
				ELSE
					DRAW_DEBUG_TEXT_ABOVE_COORDS(props[i].vPosition, "OFF", 1.0)
				ENDIF
			ENDIF
		#ENDIF
	
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(props[i].vPosition, 1.0, props[i].ModelName)
			
			IF ( props[i].bBroken = FALSE )
				IF HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_BROKEN(props[i].vPosition, 1.0, props[i].ModelName, SEARCH_LOCATION_EXTERIORS)

					SWITCH i
						CASE 0
						CASE 1
							iBrokenTargetLightsCount++
						BREAK
					ENDSWITCH
					
					iBrokenLightsCount++
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Prop object at ", props[i].vPosition, " with model ", GET_MODEL_NAME_FOR_DEBUG(props[i].ModelName), " has been broken.")
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": iBrokenLightsCount: ", iBrokenLightsCount, ".")
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": iBrokenTargetLightsCount: ", iBrokenTargetLightsCount, ".")
					#ENDIF
					
					props[i].bBroken = TRUE
					
				ENDIF
			ENDIF
			
			//SET_ENTITY_LIGHTS
			//maybe make this a script object do model hide etc
			//experiment a bit with that
			
		ENDIF
	
	ENDREPEAT

ENDPROC

PROC MANAGE_GAS_TANK_PROP_OBJECTS(OBJECT_SNIPE_STRUCT &props[],BOOL bAllowTankToBlow = FALSE)

	INT i

	REPEAT COUNT_OF(props) i
	
		IF DOES_ENTITY_EXIST(props[i].ObjectIndex)
			IF NOT IS_ENTITY_DEAD(props[i].ObjectIndex)
				#IF IS_DEBUG_BUILD
					IF ( bDrawDebugStates = TRUE )
						DRAW_DEBUG_TEXT_ABOVE_COORDS(props[i].vPosition, GET_STRING_FROM_INT(GET_ENTITY_HEALTH(props[i].ObjectIndex)), 0.0)
					ENDIF
				#ENDIF
				IF bAllowTankToBlow
					SET_ENTITY_INVINCIBLE(props[i].ObjectIndex, FALSE)
					SET_ENTITY_PROOFS(props[i].ObjectIndex, FALSE, FALSE, FALSE, FALSE, FALSE)
					
					//SET_ENTITY_CAN_BE_DAMAGED(props[i].ObjectIndex, TRUE)
					
					SET_ENTITY_DYNAMIC(props[i].ObjectIndex, FALSE)
					
					SET_DISABLE_BREAKING(props[i].ObjectIndex, FALSE)
				ELSE
//					SET_ENTITY_INVINCIBLE(props[i].ObjectIndex, TRUE)
//					SET_ENTITY_PROOFS(props[i].ObjectIndex, TRUE, TRUE, TRUE, TRUE, TRUE)
//					
//					//SET_ENTITY_CAN_BE_DAMAGED(props[i].ObjectIndex, FALSE)
//					
//					SET_ENTITY_DYNAMIC(props[i].ObjectIndex, FALSE)
//					
//					SET_DISABLE_BREAKING(props[i].ObjectIndex, TRUE)
				ENDIF
				
			ENDIF
		ENDIF
		
	ENDREPEAT

ENDPROC

INT iPlantAnim

PROC MANAGE_RON_DURING_SNIPING(PED_SNIPE_STRUCT &ped)

	//SET_PED_CAN_TORSO_VEHICLE_IK

	IF DOES_ENTITY_EXIST(ped.PedIndex)
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
		
			#IF IS_DEBUG_BUILD
				IF ( bDrawDebugStates = TRUE )
					DRAW_DEBUG_VEHICLE_RECORDING_INFO(mission_veh[MV_RON_QUAD].veh, 1.0)
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_STRING_FROM_INT(ped.iThisPedProgress), 1.25)
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.5)
				ENDIF
				ped.sDebugName = "Ron"
			#ENDIF
			
			IF ( bPedBlipsHidden = FALSE )
				IF NOT DOES_BLIP_EXIST(ped.BlipIndex)
					ped.BlipIndex = CREATE_BLIP_FOR_PED(ped.PedIndex, FALSE)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(ped.BlipIndex)
					REMOVE_BLIP(ped.BlipIndex)
				ENDIF
			ENDIF
		
			SWITCH ped.eState
			
				CASE PED_STATE_IDLE										//initial Ron setup for the stage
				
					SET_ENTITY_HEALTH(ped.PedIndex, 120)
					
					SET_ENTITY_LOD_DIST(ped.PedIndex, 500)
					SET_ENTITY_LOAD_COLLISION_FLAG(ped.PedIndex, TRUE)

					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
						SET_ENTITY_LOD_DIST(mission_veh[MV_RON_QUAD].veh, 500)
					ENDIF

					GO_TO_PED_STATE(ped, PED_STATE_DRIVE_TO_POSITION)
					
				BREAK
				
				CASE PED_STATE_DRIVE_TO_POSITION						//Ron drives blazer quad to position
				
					IF ( ped.bHasTask = FALSE )
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
							
								IF NOT IS_PED_IN_VEHICLE(ped.PedIndex, mission_veh[MV_RON_QUAD].veh)
									SET_PED_INTO_VEHICLE(ped.PedIndex, mission_veh[MV_RON_QUAD].veh, VS_DRIVER)
								ENDIF
							
								SET_VEHICLE_ACTIVE_DURING_PLAYBACK(mission_veh[MV_RON_QUAD].veh, TRUE)
								SET_VEHICLE_ENGINE_ON(mission_veh[MV_RON_QUAD].veh,TRUE,FALSE)
								START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh, 29, sVehicleRecordingsFile)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_QUAD].veh, 9250.0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(mission_veh[MV_RON_QUAD].veh)
								SET_VEHICLE_LIGHTS(mission_veh[MV_RON_QUAD].veh, FORCE_VEHICLE_LIGHTS_OFF)
								
							ELSE
								SET_PED_CAN_TORSO_VEHICLE_IK(ped.PedIndex,TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
							
								ped.bHasTask = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					REQUEST_ANIM_DICT("misstrevor2ig_2")
					REQUEST_ANIM_DICT("misstrevor2ron_basic_moves")
					
					REQUEST_ANIM_SET("move_ped_crouched")
					REQUEST_ANIM_SET("move_ped_crouched_strafing")
					
					IF ( ped.bHasTask = TRUE )
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
							IF IS_PED_IN_VEHICLE(ped.PedIndex, mission_veh[MV_RON_QUAD].veh)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
									IF	HAS_ANIM_DICT_LOADED("misstrevor2ig_2")
									AND HAS_ANIM_SET_LOADED("move_ped_crouched")
									AND HAS_ANIM_SET_LOADED("move_ped_crouched_strafing")
										REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(mission_veh[MV_RON_QUAD].veh)
										GO_TO_PED_STATE(ped, PED_STATE_SNEAKING_INTRO)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF

				BREAK
				
				CASE PED_STATE_SNEAKING_INTRO							//Ron gets off the blazer quad and waits until player aims at him
				
					IF ( ped.bHasTask = FALSE )
						IF 	HAS_ANIM_DICT_LOADED("misstrevor2ig_2")
						AND HAS_ANIM_SET_LOADED("move_ped_crouched")
						AND HAS_ANIM_SET_LOADED("move_ped_crouched_strafing")
						
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
						
								SET_VEHICLE_ENGINE_ON(mission_veh[MV_RON_QUAD].veh, FALSE, TRUE)
								SET_VEHICLE_LIGHTS(mission_veh[MV_RON_QUAD].veh, FORCE_VEHICLE_LIGHTS_OFF)
						
								SET_PED_STEALTH_MOVEMENT(ped.PedIndex, FALSE)
								SET_PED_MOVEMENT_CLIPSET(ped.PedIndex, "move_ped_crouched")
								SET_PED_STRAFE_CLIPSET(ped.PedIndex, "move_ped_crouched_strafing")
								
								SEQUENCE_INDEX SequenceIndex
								
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									CLEAR_PED_TASKS_IMMEDIATELY(ped.PedIndex)
									TASK_PLAY_ANIM_ADVANCED(NULL, "misstrevor2ig_2", "intro", GET_ENTITY_COORDS(mission_veh[MV_RON_QUAD].veh),
															GET_ENTITY_ROTATION(mission_veh[MV_RON_QUAD].veh), INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
															-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
									TASK_PLAY_ANIM_ADVANCED(NULL, "misstrevor2ig_2", "idle", GET_ENTITY_COORDS(mission_veh[MV_RON_QUAD].veh),
															GET_ENTITY_ROTATION(mission_veh[MV_RON_QUAD].veh), NORMAL_BLEND_IN, SLOW_BLEND_OUT,
															-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_LOOPING)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								
								FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
								SET_ENTITY_NO_COLLISION_ENTITY(ped.PedIndex, mission_veh[MV_RON_QUAD].veh, TRUE)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								
								SET_LABEL_AS_TRIGGERED("QUAD NO LONGER NEEDED",TRUE)
								SET_PED_CAN_TORSO_VEHICLE_IK(ped.PedIndex,FALSE)
								
								ped.bHasTask = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF ( ped.bHasTask = TRUE )
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_2", "idle")
							IF IS_PED_AT_WATER_TOWER(PLAYER_PED_ID())
								IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
									ped.iThisPedTimer					= 0
									ped.iThisPedProgress				= 0
									ped.iConversationTimer		= 0
									ped.iConversationCounter	= 0
									bFirstSnipingEnemyAllowed	= TRUE
									GO_TO_PED_STATE(ped, PED_STATE_SNEAKING)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				BREAK
				
				CASE PED_STATE_PLEADING									//Ron plays pleading animation if discovered by enemy
				
					IF ( ped.bHasTask = FALSE )
					
						IF 	NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_5c", "intro")
						AND NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_5c", "plead_loop")
						
							CLEAR_PED_TASKS(ped.PedIndex)
						
							SEQUENCE_INDEX SequenceIndex
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							OPEN_SEQUENCE_TASK(SequenceIndex)
								//SO RON ORIENTATES TO THE FINAL ENEMY
								IF NOT IS_PED_INJURED(psRon.PedIndex)
									IF DOES_ENTITY_EXIST(psEnemies[7].PedIndex)
										IF NOT IS_PED_INJURED(psEnemies[7].PedIndex)
											IF GET_DISTANCE_BETWEEN_PEDS(psRon.PedIndex, psEnemies[7].PedIndex) < 12.0
												TASK_TURN_PED_TO_FACE_ENTITY(NULL, psEnemies[7].PedIndex)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								TASK_PLAY_ANIM(NULL, "misstrevor2ig_5c", "intro", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
								TASK_PLAY_ANIM(NULL, "misstrevor2ig_5c", "plead_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
							ped.bHasTask = TRUE
							
						ELSE						
							ped.bHasTask = TRUE
						ENDIF
					ENDIF
					
					IF ( ped.bHasTask = TRUE )

						IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_5c", "outro_live")
						
							IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_5c", "plead_loop")
								PED_INDEX ClosestEnemyPedIndex
								ClosestEnemyPedIndex = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(ped.PedIndex, rel_group_enemies)
								
								IF NOT DOES_ENTITY_EXIST(ClosestEnemyPedIndex)
								OR ( DOES_ENTITY_EXIST(ClosestEnemyPedIndex) AND IS_ENTITY_DEAD(ClosestEnemyPedIndex) )
								
								//this is buggy
								OR ( DOES_ENTITY_EXIST(ClosestEnemyPedIndex) AND NOT IS_ENTITY_DEAD(ClosestEnemyPedIndex) AND GET_DISTANCE_BETWEEN_PEDS(ped.PedIndex, ClosestEnemyPedIndex) > 12.5)
									CLEAR_PED_TASKS(ped.PedIndex)
									TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_5c", "outro_live", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
								ENDIF
								
							ENDIF
						
						ELSE
						
							IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_5c", "outro_live")									
								IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_5c", "outro_live") >= 0.95
									GO_TO_PED_STATE(ped, PED_STATE_SNEAKING)
								ENDIF
							ENDIF

						ENDIF
						
					ENDIF

				BREAK
				
				CASE PED_STATE_SNEAKING									//Ron is sneaking through bikers turf

					SWITCH ped.iThisPedProgress
					
						CASE 0											//Ron sneaks from blazer to the first enemy position
						
							IF ( ped.bHasTask = FALSE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_2", "idle")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_2", "idle") > 0.65
										
										SEQUENCE_INDEX SequenceIndex
										
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_PLAY_ANIM(NULL, "misstrevor2ig_2", "outro", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1722.83, 3343.36, 40.58 >>, PEDMOVE_RUN,  DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP | ENAV_NO_STOPPING)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1698.65, 3334.90, 40.50 >>, PEDMOVE_RUN,  DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP | ENAV_NO_STOPPING)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1695.43, 3333.25, 41.49 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, -170.30)
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)
									
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK								
										ped.bHasTask = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							REQUEST_ANIM_DICT("misstrevor2ig_3")	//Ron's pointing animation
							REQUEST_ANIM_DICT("misstrevor2ig_4")	//Ron's hide animation
							REQUEST_ANIM_DICT("misstrevor2ig_5c")	//Ron's pleading animations
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_AT_COORD(ped.PedIndex, << 1695.18, 3332.86, 40.49 >>, << 3.0, 3.0, 3.0 >>)
								
									SWITCH GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
									
										CASE PERFORMING_TASK										//if the player takes down the first enemy
											IF DOES_ENTITY_EXIST(psEnemies[0].PedIndex)				//while Ron is still moving to first spot
												IF IS_ENTITY_DEAD(psEnemies[0].PedIndex)			//make Ron move to second spot
												OR IS_PED_INJURED(psEnemies[0].PedIndex)
													IF ( GET_SEQUENCE_PROGRESS(ped.PedIndex) = 3 )
														REMOVE_ANIM_DICT("misstrevor2ig_2")
														ped.bHasTask = FALSE
														ped.iThisPedProgress++
													ENDIF
												ENDIF
											ENDIF
										BREAK
									
										CASE FINISHED_TASK
											REMOVE_ANIM_DICT("misstrevor2ig_2")
											ped.bHasTask = FALSE
											ped.iThisPedProgress++
										BREAK
									
									ENDSWITCH
							
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 1											//Ron spots the first enemy and moves on when that enemy is killed
						
							IF ( ped.bHasTask = FALSE )
							
								IF DOES_ENTITY_EXIST(psEnemies[0].PedIndex)
									IF NOT IS_ENTITY_DEAD(psEnemies[0].PedIndex)
										IF HAS_ANIM_DICT_LOADED("misstrevor2ig_3")
											TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_3", "point",  SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
										ENDIF
									ENDIF
								ENDIF
							
								ped.bHasTask = TRUE
							ENDIF
							
							IF DOES_ENTITY_EXIST(psEnemies[0].PedIndex)
								IF IS_ENTITY_DEAD(psEnemies[0].PedIndex)
								OR IS_PED_INJURED(psEnemies[0].PedIndex)
									ped.bHasTask = FALSE
									ped.iThisPedProgress++
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 2											//Ron sneaks closer to the tower stopping by the front of blue truck
						
							IF ( ped.bHasTask = FALSE )
					
								CLEAR_PED_TASKS(ped.PedIndex)
								
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1692.163, 3328.627, 41.472 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 170.92)
									//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vScenePosition, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 170.92)//vSceneRotation.Z)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<1692.16, 3328.63, 41.47>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, -180.00)
									TASK_PLAY_ANIM(NULL, "misstrevor2ron_basic_moves", "idle", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								
								ped.bHasTask = TRUE
							ENDIF

							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_AT_COORD(ped.PedIndex, <<1692.16, 3328.63, 41.47>>, << 0.3, 0.3, 2.0 >>)
									IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
										bSecondSnipingEnemyAllowed = TRUE
										ped.bHasTask = FALSE
										ped.iThisPedProgress++
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 3											//Ron hides from the second enemy coming in the van
						
							IF ( ped.bHasTask = FALSE )
								IF DOES_ENTITY_EXIST(psEnemies[1].PedIndex)
									IF NOT IS_ENTITY_DEAD(psEnemies[1].PedIndex)
										IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
											IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
												IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[0].VehicleIndex)
													IF GET_TIME_POSITION_IN_RECORDING(vsEnemyVans[0].VehicleIndex) > 2000.0
													
														SEQUENCE_INDEX SequenceIndex
														CLEAR_SEQUENCE_TASK(SequenceIndex)
														OPEN_SEQUENCE_TASK(SequenceIndex)
															TASK_PLAY_ANIM_ADVANCED(NULL, "misstrevor2ig_4", "hide", GET_ENTITY_COORDS(ped.PedIndex),
																					GET_ENTITY_ROTATION(ped.PedIndex) , SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET)
															//TASK_PLAY_ANIM_ADVANCED(NULL, "misstrevor2ig_4", "hide", << 1692.163, 3328.627, 41.472 >>, << 0.0, 0.0, 180.0 >>, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET)
														
															//esperiment with turning to face the truck
															TASK_TURN_PED_TO_FACE_COORD(NULL, << 1697.71, 3314.78, 40.15 >>)
															TASK_PLAY_ANIM(NULL, "misstrevor2ig_3", "point", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
														CLOSE_SEQUENCE_TASK(SequenceIndex)
														TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
														CLEAR_SEQUENCE_TASK(SequenceIndex)
														
														FORCE_PED_AI_AND_ANIMATION_UPDATE(ped.PedIndex)
														
														ped.bHasTask = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_4", "hide")
									IF DOES_ENTITY_EXIST(psEnemies[1].PedIndex)
										IF IS_ENTITY_DEAD(psEnemies[1].PedIndex)
										OR IS_PED_INJURED(psEnemies[1].PedIndex)
											REMOVE_ANIM_DICT("misstrevor2ig_4")
											ped.bHasTask = FALSE
											ped.iThisPedProgress++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 4											//Ron sneaks closer to the tower stopping by the front of blue truck	
						
							IF ( ped.bHasTask = FALSE )
								CLEAR_PED_TASKS(ped.PedIndex)
								TASK_FOLLOW_NAV_MESH_TO_COORD(ped.PedIndex, << 1692.33, 3328.83, 40.47 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 168.17)
								ped.bHasTask = TRUE
							ENDIF
						
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_AT_COORD(ped.PedIndex, << 1692.163, 3328.627, 41.472 >>, << 0.5, 0.5, 2.0 >>)
									IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
										ped.bHasTask = FALSE
										ped.iThisPedProgress++
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 5											//Ron spots the third enemy on the tower and points to him
						
							IF ( ped.bHasTask = FALSE )
							
								IF DOES_ENTITY_EXIST(psEnemies[2].PedIndex)
									IF NOT IS_ENTITY_DEAD(psEnemies[2].PedIndex)
										REQUEST_ANIM_DICT("misstrevor2ig_5")
										IF HAS_ANIM_DICT_LOADED("misstrevor2ig_5")
											TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_5", "point_at_sniper",  SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
											ped.bHasTask = TRUE
										ENDIF
									ELSE
										ped.bHasTask = TRUE										
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(psEnemies[2].PedIndex)
								IF IS_ENTITY_DEAD(psEnemies[2].PedIndex)
								OR IS_PED_INJURED(psEnemies[2].PedIndex)
									ped.bHasTask = FALSE
									ped.iThisPedProgress++
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 6
						
							IF ( ped.bHasTask = FALSE )
								IF DOES_ENTITY_EXIST(psEnemies[3].PedIndex)
									IF NOT IS_ENTITY_DEAD(psEnemies[3].PedIndex)
										IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_5", "point_at_sniper")
											REQUEST_ANIM_DICT("misstrevor2ig_5")
											IF HAS_ANIM_DICT_LOADED("misstrevor2ig_5")
												TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_5", "point_at_sniper",  SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
												ped.bHasTask = TRUE
											ENDIF
										ELSE
											ped.bHasTask = TRUE										
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(psEnemies[3].PedIndex)
								IF IS_ENTITY_DEAD(psEnemies[3].PedIndex)
								OR IS_PED_INJURED(psEnemies[3].PedIndex)
									ped.bHasTask = FALSE
									ped.iThisPedProgress++
								ENDIF
							ENDIF

						BREAK
						
						CASE 7
						
							IF ( ped.bHasTask = FALSE )
								IF DOES_ENTITY_EXIST(psEnemies[4].PedIndex)
									IF NOT IS_ENTITY_DEAD(psEnemies[4].PedIndex)
										IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_3", "point")
											REQUEST_ANIM_DICT("misstrevor2ig_3")
											IF HAS_ANIM_DICT_LOADED("misstrevor2ig_3")
												TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_3", "point",  SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
												ped.bHasTask = TRUE
											ENDIF
										ELSE
											ped.bHasTask = TRUE										
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(psEnemies[4].PedIndex)
								IF IS_ENTITY_DEAD(psEnemies[4].PedIndex)
								OR IS_PED_INJURED(psEnemies[4].PedIndex)
									ped.bHasTask = FALSE
									ped.iThisPedProgress++
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 8
						
							IF ( ped.bHasTask = FALSE )
								IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_2", "idle")
									REQUEST_ANIM_DICT("misstrevor2ig_2")
									IF HAS_ANIM_DICT_LOADED("misstrevor2ig_2")
										
										SEQUENCE_INDEX SequenceIndex
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 0)
											TASK_PLAY_ANIM(NULL, "misstrevor2ig_2", "idle",  SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										
										ped.bHasTask = TRUE
									ENDIF									
								ENDIF
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_2", "idle")
									IF IS_PED_AT_WATER_TOWER(PLAYER_PED_ID())
										IF IS_PED_IN_PLAYER_SNIPER_SCOPE(ped.PedIndex)
											bSixthSnipingEnemyAllowed = TRUE
											ped.bHasTask = FALSE
											ped.iThisPedProgress++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 9
						
							IF ( ped.bHasTask = FALSE )
							
								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_2", "idle")
									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_2", "idle") > 0.65
						
										SEQUENCE_INDEX SequenceIndex
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_PLAY_ANIM(NULL, "misstrevor2ig_2", "outro", SLOW_BLEND_IN, SLOW_BLEND_OUT)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1687.19, 3297.24, 40.13 >>, PEDMOVE_RUN, -1, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP) 
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										
										ped.bHasTask = TRUE
									ENDIF
								ENDIF
								
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_AT_COORD(ped.PedIndex, << 1685.40, 3296.70, 41.09 >>, << 3.0, 3.0, 1.5 >>)
									IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
										ped.bHasTask = FALSE
										ped.iThisPedProgress++
									ENDIF
								ENDIF									
							ENDIF	
						
						BREAK
						
						CASE 10
						
							IF ( ped.bHasTask = FALSE )
							
								IF DOES_ENTITY_EXIST(psEnemies[5].PedIndex)
									IF NOT IS_ENTITY_DEAD(psEnemies[5].PedIndex)
										IF HAS_ANIM_DICT_LOADED("misstrevor2ig_3")
											TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_3", "point",  SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
										ENDIF
									ENDIF
								ENDIF
							
								ped.bHasTask = TRUE
							ENDIF
							
							IF DOES_ENTITY_EXIST(psEnemies[5].PedIndex)
								IF IS_ENTITY_DEAD(psEnemies[5].PedIndex)
								OR IS_PED_INJURED(psEnemies[5].PedIndex)
									ped.bHasTask = FALSE
									ped.iThisPedProgress++
								ENDIF
							ENDIF

						BREAK
						
						CASE 11

							IF ( ped.bHasTask = FALSE )
								IF DOES_ENTITY_EXIST(psEnemies[6].PedIndex)
									IF NOT IS_ENTITY_DEAD(psEnemies[6].PedIndex)
										IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_3", "point")
											REQUEST_ANIM_DICT("misstrevor2ig_3")
											IF HAS_ANIM_DICT_LOADED("misstrevor2ig_3")
												TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_3", "point",  SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
												ped.bHasTask = TRUE
											ENDIF
										ELSE
											ped.bHasTask = TRUE										
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(psEnemies[6].PedIndex)
								IF IS_ENTITY_DEAD(psEnemies[6].PedIndex)
								OR IS_PED_INJURED(psEnemies[6].PedIndex)
									ped.bHasTask = FALSE
									ped.iThisPedProgress++
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 12
						
							IF ( ped.bHasTask = FALSE )
								TASK_FOLLOW_NAV_MESH_TO_COORD(ped.PedIndex, << 1680.91, 3280.30, 40.94 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, -160.04)
								ped.bHasTask = TRUE										
							ENDIF
							
							REQUEST_ANIM_DICT("misstrevor2ig_7")
							REQUEST_MODEL(osPropStickyBomb.ModelName)
						
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_AT_COORD(ped.PedIndex, << 1680.91, 3280.30, 40.94 >>, << 0.7, 0.7, 2.0 >>)
									IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
										ped.bHasTask = FALSE
										ped.iThisPedProgress++
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 13
						
							IF ( ped.bHasTask = FALSE )
								REQUEST_ANIM_DICT("misstrevor2ig_7")
								IF HAS_ANIM_DICT_LOADED("misstrevor2ig_7")
								
									//GIVE_WEAPON_TO_PED(ped.PedIndex, WEAPONTYPE_STICKYBOMB, INFINITE_AMMO, TRUE, TRUE)
									//SET_CURRENT_PED_WEAPON(ped.PedIndex, WEAPONTYPE_STICKYBOMB, TRUE)
										
//									TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misstrevor2ig_7", "plant_bomb", GET_ENTITY_COORDS(ped.PedIndex),
//															<< 0.0, 0.0, -134.4 >>, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_HOLD_LAST_FRAME)
									
									iPlantAnim = CREATE_SYNCHRONIZED_SCENE(<< 0.000, 0.000, 0.000 >>, << 0.000, 0.000, 180.000 >>)
									IF DOES_ENTITY_EXIST(crate[1])		
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iPlantAnim,crate[1],-1)
									ENDIF
									TASK_SYNCHRONIZED_SCENE (ped.PedIndex, iPlantAnim, "misstrevor2ig_7", "plant_bomb", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE,RBF_NONE,4 )
															
									bEighthSnipingEnemyAllowed = TRUE
									ped.bHasTask = TRUE
								ENDIF
							ENDIF

							IF ( ped.bHasTask = TRUE )
//								IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misstrevor2ig_7", "plant_bomb")
//								
//									SET_ENTITY_ANIM_SPEED(ped.PedIndex, "misstrevor2ig_7", "plant_bomb", 1.25)
//									
//									//this handles the sticky bomb detachment from Ron and attachment onto the gas tank
//									IF NOT DOES_ENTITY_EXIST(osPropStickyBomb.ObjectIndex)
//										IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_7", "plant_bomb") >= 0.385
//											IF HAS_MISSION_OBJECT_BEEN_CREATED(osPropStickyBomb)
//												IF NOT IS_ENTITY_ATTACHED(osPropStickyBomb.ObjectIndex)
//													ATTACH_ENTITY_TO_ENTITY(osPropStickyBomb.ObjectIndex, ped.PedIndex,
//																			GET_PED_BONE_INDEX(ped.PedIndex, BONETAG_PH_R_HAND),
//																			<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, FALSE, FALSE, FALSE)
//												ENDIF
//											ENDIF
//										ENDIF
//									ELSE
//										IF NOT IS_ENTITY_DEAD(osPropStickyBomb.ObjectIndex)
//											IF IS_ENTITY_ATTACHED_TO_ENTITY(osPropStickyBomb.ObjectIndex, ped.PedIndex)
//												IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_7", "plant_bomb") >= 0.705
//													DETACH_ENTITY(osPropStickyBomb.ObjectIndex, FALSE)
//													SET_ENTITY_COORDS_NO_OFFSET(osPropStickyBomb.ObjectIndex, << 1682.2469, 3279.75, 40.5851 >>)
//													SET_ENTITY_ROTATION(osPropStickyBomb.ObjectIndex, << 0.0, -140.0, 18.36 >>)
//													FREEZE_ENTITY_POSITION(osPropStickyBomb.ObjectIndex, TRUE)
//												ENDIF
//											ENDIF
//										ENDIF									
//									ENDIF
//									
//									IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misstrevor2ig_7", "plant_bomb") >= 0.99
//										ped.bHasTask = FALSE
//										ped.iThisPedProgress++
//									ENDIF
//									
//								ENDIF
								
								IF IS_SYNCHRONIZED_SCENE_RUNNING(iPlantAnim)	
									SET_SYNCHRONIZED_SCENE_RATE(iPlantAnim,1.25)
									
									IF NOT DOES_ENTITY_EXIST(osPropStickyBomb.ObjectIndex)
										IF GET_SYNCHRONIZED_SCENE_PHASE(iPlantAnim) >= 0.385
											IF HAS_MISSION_OBJECT_BEEN_CREATED(osPropStickyBomb)
												IF NOT IS_ENTITY_ATTACHED(osPropStickyBomb.ObjectIndex)
													ATTACH_ENTITY_TO_ENTITY(osPropStickyBomb.ObjectIndex, ped.PedIndex,
																			GET_PED_BONE_INDEX(ped.PedIndex, BONETAG_PH_R_HAND),
																			<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, FALSE, FALSE, FALSE)
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF NOT IS_ENTITY_DEAD(osPropStickyBomb.ObjectIndex)
											IF IS_ENTITY_ATTACHED_TO_ENTITY(osPropStickyBomb.ObjectIndex, ped.PedIndex)
												IF GET_SYNCHRONIZED_SCENE_PHASE(iPlantAnim) >= 0.705
													DETACH_ENTITY(osPropStickyBomb.ObjectIndex, FALSE)
//													SET_ENTITY_COORDS_NO_OFFSET(osPropStickyBomb.ObjectIndex, << 1682.2469, 3279.75, 40.5851 >>)
//													SET_ENTITY_ROTATION(osPropStickyBomb.ObjectIndex, << 0.0, -140.0, 18.36 >>)
													SET_ENTITY_COORDS_NO_OFFSET(osPropStickyBomb.ObjectIndex, << 1681.77, 3280.74, 40.66 >>)
													SET_ENTITY_ROTATION(osPropStickyBomb.ObjectIndex, << 176.66, -51.67, -156.23 >>)
													FREEZE_ENTITY_POSITION(osPropStickyBomb.ObjectIndex, TRUE)
												ENDIF
											ENDIF
										ENDIF									
									ENDIF
									
									IF GET_SYNCHRONIZED_SCENE_PHASE(iPlantAnim) >= 0.99
										ped.bHasTask = FALSE
										ped.iThisPedProgress++
									ENDIF
									
								ENDIF
								
							ENDIF
						
						BREAK
						
						CASE 14
						
							IF ( ped.bHasTask = FALSE )
						
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									IF NOT IS_PED_INJURED(psEnemies[7].PedIndex)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1678.1774, 3290.1453, 39.9616 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 201.6975)
									ELSE
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 1678.1774, 3290.1453, 39.9616 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 300.0)
									ENDIF
									TASK_PLAY_ANIM(NULL, "misstrevor2ron_basic_moves", "idle", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								
								ped.bHasTask = TRUE
							ENDIF
							
							IF ( ped.bHasTask = TRUE )
								IF IS_ENTITY_AT_COORD(ped.PedIndex, << 1678.45, 3290.73, 41.46 >>, << 2.0, 2.0, 2.0 >>)
									IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
										ped.bHasTask = FALSE
										ped.iThisPedProgress++
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 15	//make Ron point at the helicopter
						
							IF ( ped.bHasTask = FALSE )
								IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
									IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MAVERICK].veh)
											IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_MAVERICK].veh) > 12000.0
												TASK_PLAY_ANIM(ped.PedIndex, "misstrevor2ig_5", "point_at_sniper", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
												ped.bHasTask = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF		
							ENDIF
						
						
						BREAK
						
					ENDSWITCH
					
					
					PED_INDEX ClosestEnemyPedIndex
					
					ClosestEnemyPedIndex = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(ped.PedIndex, rel_group_enemies)
					
					IF DOES_ENTITY_EXIST(ClosestEnemyPedIndex)
						IF NOT IS_ENTITY_DEAD(ClosestEnemyPedIndex)
							IF GET_DISTANCE_BETWEEN_PEDS(ped.PedIndex, ClosestEnemyPedIndex) < 10.0
								GO_TO_PED_STATE(ped, PED_STATE_PLEADING)
							ENDIF
						ENDIF
					ENDIF
			
				BREAK
			
			ENDSWITCH
		
		ENDIF
	ENDIF

ENDPROC

INT iTakeOutTheChopperTimer
//INT iEnemyOffBike
INT iDelayDialogueTimer

PROC MANAGE_CONVERSATIONS_DURING_SNIPING()
	
	

	IF DOES_ENTITY_EXIST(psRon.PedIndex)
		IF NOT IS_ENTITY_DEAD(psRon.PedIndex)

			IF ( bBikersAlerted = TRUE )		//bikers have been alerted

				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_ALERT")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_ALERT", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("TR2_ALERT", TRUE)
							ENDIF
						ENDIF
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF

			ELSE								//bikers are not alerted
			
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl0")				//play conversation for Trevor telling Ron to park up the ATV
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF DOES_ENTITY_EXIST(psRon.PedIndex)
								IF NOT IS_ENTITY_DEAD(psRon.PedIndex)
									IF ( psRon.eState = PED_STATE_DRIVE_TO_POSITION )
										IF DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
											IF NOT IS_ENTITY_DEAD(mission_veh[MV_RON_QUAD].veh)
												IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_QUAD].veh)
													IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_QUAD].veh) > 13000.0
														IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl0", CONV_PRIORITY_MEDIUM)
															SET_LABEL_AS_TRIGGERED("TR_pl0", TRUE)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_ronwav")
					IF DOES_ENTITY_EXIST(psRon.PedIndex)
						IF NOT IS_ENTITY_DEAD(psRon.PedIndex)
							IF ( psRon.eState = PED_STATE_SNEAKING_INTRO )
								IF ( psRon.iConversationTimer = 0 )
									psRon.iConversationTime 	= 1000
									psRon.iConversationTimer 	= GET_GAME_TIMER()
								ELSE
									IF HAS_TIME_PASSED(psRon.iConversationTime, psRon.iConversationTimer)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_ronwav", CONV_PRIORITY_MEDIUM)
													psRon.iConversationTime 	= 10000
													psRon.iConversationTimer 	= GET_GAME_TIMER()
												ENDIF
											ELSE
												IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_QMAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
													psRon.iConversationTime 	= 10000
													psRon.iConversationTimer 	= GET_GAME_TIMER()
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_sf01")	//play conversation for Ron reacting to bullets fired by the player
					IF IS_PED_WALKING(psRon.PedIndex)
					OR IS_PED_RUNNING(psRon.PedIndex)
						IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(psRon.PedIndex), 2.5)
						OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(psRon.PedIndex), 5.0)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_sf01", CONV_PRIORITY_MEDIUM)
									ENDIF
								ELSE
									IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_ADAG", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//TR_RONCOW
				
				IF ( psRon.eState = PED_STATE_PLEADING )
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_RONCOW")				//play conversation for Ron entering pleading state
						IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_5c", "intro")
						OR IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_5c", "plead_loop")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_RONCOW", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("TR_RONCOW", TRUE)
									ENDIF
								ELSE
									IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_ACAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
										SET_LABEL_AS_TRIGGERED("TR_RONCOW", TRUE)
									ENDIF
								ENDIF
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
						ENDIF
					ENDIF
					
				ELSE	//if ron is not pleading then if pleading labels were triggered set them as not triggered

					IF HAS_LABEL_BEEN_TRIGGERED("TR_RONCOW")
						SET_LABEL_AS_TRIGGERED("TR_RONCOW", FALSE)
					ENDIF

				ENDIF

				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl1n")
				
					IF DOES_ENTITY_EXIST(psRon.PedIndex)
						IF NOT IS_ENTITY_DEAD(psRon.PedIndex)
							//weird line being played?
//							IF ( psRon.eState = PED_STATE_SNEAKING_INTRO )
								IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex,"misstrevor2ig_2", "idle")
								OR (IS_ENTITY_PLAYING_ANIM(psRon.PedIndex,"misstrevor2ig_2", "intro") AND GET_ENTITY_ANIM_CURRENT_TIME(psRon.PedIndex,"misstrevor2ig_2", "intro") > 0.7)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF IS_PED_AT_WATER_TOWER(PLAYER_PED_ID())
												IF IS_PED_IN_PLAYER_SNIPER_SCOPE(psRon.PedIndex)
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl1n", CONV_PRIORITY_MEDIUM)
														
														REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
														
														SET_LABEL_AS_TRIGGERED("TR_pl1n", TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
//							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					IF ( psRon.eState != PED_STATE_PLEADING )
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_sn01t")
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl1ng")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF IS_PED_IN_PLAYER_SNIPER_SCOPE(psRon.PedIndex)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl1ng", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR_pl1ng", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//alt line if the player shot the guy early
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl1ng")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF IS_PED_IN_PLAYER_SNIPER_SCOPE(psRon.PedIndex)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_MOVE2", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR_pl1ng", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF

				//play sniping enemies conversations

				IF DOES_ENTITY_EXIST(psEnemies[0].PedIndex)				//play covnersations for first sniping enemy, under the tower
				
					IF NOT IS_ENTITY_DEAD(psEnemies[0].PedIndex)
										
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_sn01p")
						
							IF ( psRon.eState = PED_STATE_SNEAKING )
								IF ( psRon.iThisPedProgress = 1 )
									IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_3", "point")
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF psEnemies[0].eState = PED_STATE_UNALERTED
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_sn01p", CONV_PRIORITY_MEDIUM)
														
														REPLAY_RECORD_BACK_FOR_TIME(1.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
														
														SET_LABEL_AS_TRIGGERED("TR_sn01p", TRUE)
													ENDIF
												ELSE
													SET_LABEL_AS_TRIGGERED("TR_sn01p", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						ELSE
						
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_TREVMISS")
								IF ( psEnemies[0].eState = PED_STATE_ALERTED )
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_TREVMISS", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR2_TREVMISS", TRUE)
											ENDIF
										ENDIF
									ELSE
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
								ENDIF
							ENDIF
						
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl2")
								IF HAS_LABEL_BEEN_TRIGGERED("SJC_ENEM1G_0")
									IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_3", "point")
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl2", CONV_PRIORITY_MEDIUM)
													psRon.iConversationTimer = GET_GAME_TIMER()
													SET_LABEL_AS_TRIGGERED("TR_pl2", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ELSE
							
								//play random reminder
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_POINT")
									IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_3", "point")
										IF HAS_TIME_PASSED(8000, psRon.iConversationTimer)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_POINT", CONV_PRIORITY_MEDIUM)
														psRon.iConversationTimer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						
						ENDIF
						
					ENDIF
				
					IF IS_ENTITY_DEAD(psEnemies[0].PedIndex)			//play conversation when enemy is killed
				
						//SWITCH psEnemies[0].eState
						
						//	CASE PED_STATE_UNALERTED
							
						//	BREAK
						
						//ENDSWITCH
				
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl1b")				//Ron praises Trevor for the kill
							IF HAS_LABEL_BEEN_TRIGGERED("TR_sn01p")				//after the instruction to kill the guy was given
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl1b", CONV_PRIORITY_MEDIUM)
											
											REPLAY_RECORD_BACK_FOR_TIME(1.5, 3.0, REPLAY_IMPORTANCE_HIGHEST)
											
											SET_LABEL_AS_TRIGGERED("TR_pl1b", TRUE)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_AVAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
											SET_LABEL_AS_TRIGGERED("TR_pl1b", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
						
//						IF NOT DOES_ENTITY_EXIST(psEnemies[2].PedIndex)	
//							//use modified label for each enemy that could case ron to plead
//							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RONPL2")			//Ron praises the player for killing enemy when ron was pleading
//								IF psRon.eState = PED_STATE_PLEADING
//									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_RONPL2", CONV_PRIORITY_MEDIUM)
//												SET_LABEL_AS_TRIGGERED("TR2_RONPL2", TRUE)
//											ENDIF
//										ELSE
//											IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
//												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_BJAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
//												SET_LABEL_AS_TRIGGERED("TR2_RONPL2", TRUE)
//											ENDIF
//										ENDIF
//									ELSE
//										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
						
								

						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_sn01t")					//Trevor says he killed the guy early
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_sn01p")				//before the instruction was given
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_sn01t", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR_sn01t", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_MOVE")
							IF HAS_LABEL_BEEN_TRIGGERED("TR_pl1b")
//							OR HAS_LABEL_BEEN_TRIGGERED("TR2_RONPL2")
							OR HAS_LABEL_BEEN_TRIGGERED("TR_sn01t")
								IF 	NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND	NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)	//timing issue here due to god text being cleared when this enemy ped is cleared
								AND	NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_MOVE")
											IF IS_PED_IN_PLAYER_SNIPER_SCOPE(psRon.PedIndex)
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_MOVE2", CONV_PRIORITY_MEDIUM)
													SET_LABEL_AS_TRIGGERED("TR_MOVE", TRUE)
												ENDIF
											ELSE
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_MOVE", CONV_PRIORITY_MEDIUM)
													SET_LABEL_AS_TRIGGERED("TR_MOVE", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF

					ELSE
						IF psRon.eState = PED_STATE_PLEADING
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CAUGHT_0")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CAUGHT", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_0", TRUE)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_EZAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_0", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
								
				ENDIF
			
				IF DOES_ENTITY_EXIST(psEnemies[1].PedIndex)				//play conversations for second sniping enemy, ped who arrives in a van

					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DRIVEIN")		//play conversation for ron saying the van is coming from a distance
					
						IF ( psRon.eState = PED_STATE_SNEAKING )
							IF NOT IS_ENTITY_DEAD(psEnemies[1].PedIndex)
								IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_4", "hide")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_DRIVEIN", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR2_DRIVEIN", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
					ELSE
						
						IF 	NOT HAS_LABEL_BEEN_TRIGGERED("TR2_SHOOTL")	//play conversation for Ron giving instruction to shoot the lights
						AND NOT HAS_LABEL_BEEN_TRIGGERED("TR2_SHOOTLE")	//play conversation for Trevor saying he already shot the lights
						
							IF ( osPropWallLights[0].bBroken = FALSE OR osPropWallLights[1].bBroken = FALSE)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_SHOOTL", CONV_PRIORITY_MEDIUM)
											iBrokenLightsRonCount 		= iBrokenLightsCount
											iBrokenTargetLightsRonCount = iBrokenTargetLightsCount
											psRon.iConversationTimer	= GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("TR2_SHOOTL", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_SHOOTLE", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_SHOOTLE", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						ELSE
						
							IF HAS_LABEL_BEEN_TRIGGERED("TR2_SHOOTL")				//if Ron's instruction conversation was played
							
								IF ( iBrokenLightsRonCount != iBrokenLightsCount )	//player has shot another light
									
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_LIGHTS")
										
										IF ( osPropWallLights[0].bBroken = TRUE AND osPropWallLights[1].bBroken = TRUE )
										
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_BREAK2")
												IF HAS_LABEL_BEEN_TRIGGERED("TR2_BREAK")
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
															IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_BREAK2", CONV_PRIORITY_MEDIUM)
																SET_LABEL_AS_TRIGGERED("TR2_BREAK2", TRUE)
																SET_LABEL_AS_TRIGGERED("TR2_WRONG", TRUE)
																
																REPLAY_RECORD_BACK_FOR_TIME(5.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
																
															ENDIF
														ELSE
															PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_QWAA","TREVOR",SPEECH_PARAMS_FORCE)
															SET_LABEL_AS_TRIGGERED("TR2_BREAK2", TRUE)
															SET_LABEL_AS_TRIGGERED("TR2_WRONG", TRUE)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_LIGHTS", CONV_PRIORITY_MEDIUM)
														iBrokenLightsRonCount 		= iBrokenLightsCount
														iBrokenTargetLightsRonCount	= iBrokenTargetLightsCount
														SET_LABEL_AS_TRIGGERED("TR2_LIGHTS", TRUE)
														SET_LABEL_AS_TRIGGERED("TR2_BREAK", TRUE)
														SET_LABEL_AS_TRIGGERED("TR2_BREAK2", TRUE)
														SET_LABEL_AS_TRIGGERED("TR2_WRONG", TRUE)
													ENDIF
												ENDIF
											ENDIF
										
										ENDIF
										
									ENDIF
									
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_BREAK")
										IF ( osPropWallLights[0].bBroken = TRUE OR osPropWallLights[1].bBroken = TRUE )
											IF ( iBrokenTargetLightsRonCount != iBrokenTargetLightsCount)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
														IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_BREAK", CONV_PRIORITY_MEDIUM)
															iBrokenLightsRonCount 		= iBrokenLightsCount
															iBrokenTargetLightsRonCount	= iBrokenTargetLightsCount
															psRon.iConversationTimer	= GET_GAME_TIMER()
															SET_LABEL_AS_TRIGGERED("TR2_BREAK", TRUE)
														ENDIF
													ELSE
														PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_QVAA","TREVOR",SPEECH_PARAMS_FORCE)
														iBrokenLightsRonCount 		= iBrokenLightsCount
														iBrokenTargetLightsRonCount	= iBrokenTargetLightsCount
														psRon.iConversationTimer	= GET_GAME_TIMER()
														SET_LABEL_AS_TRIGGERED("TR2_BREAK", TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_WRONG")
										IF ( osPropWallLights[2].bBroken = TRUE )
											IF ( iBrokenTargetLightsRonCount = iBrokenTargetLightsCount )
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
														IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_WRONG", CONV_PRIORITY_MEDIUM)
															iBrokenLightsRonCount 		= iBrokenLightsCount
															iBrokenTargetLightsRonCount	= iBrokenTargetLightsCount
															psRon.iConversationTimer	= GET_GAME_TIMER()
															SET_LABEL_AS_TRIGGERED("TR2_WRONG", TRUE)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								ENDIF
								
								//play reminder here
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_LITOUT")
									IF ( osPropWallLights[0].bBroken = FALSE OR osPropWallLights[1].bBroken = FALSE )
										IF HAS_TIME_PASSED(6000, psRon.iConversationTimer)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_LITOUT", CONV_PRIORITY_MEDIUM)
														psRon.iConversationTimer = GET_GAME_TIMER()
														//SET_LABEL_AS_TRIGGERED("TR2_LITOUT", TRUE)
														//might need to set it as triggere after random variations run out
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF

							ENDIF
						
							IF HAS_LABEL_BEEN_TRIGGERED("TR2_SHOOTLE")
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_LIGHTSE")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_LIGHTSE", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR2_LIGHTSE", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(psEnemies[1].PedIndex)
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl2ap")
								IF IS_ENTITY_PLAYING_ANIM(psEnemies[1].PedIndex, "misstrevor2ig_4b", "biker_on_phone")
								OR ( fSecondEnemyPlaybackTime >= 18000.0)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl2ap", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR_pl2ap", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//DIALOGUE FOR SEEING THE DEAD BODY
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_BODY 1")
								IF IS_ENTITY_PLAYING_ANIM(psEnemies[1].PedIndex, "guard_reactions", "med_down")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_BODY", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR_BODY 1", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iVanExitSceneID)
						
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl2bi")
								
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl2bi", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR_pl2bi", TRUE)
											ENDIF
										ENDIF
									ENDIF
									
								ELSE
								
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_OUTVAN")
									
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_OUTVAN", CONV_PRIORITY_MEDIUM)
													SET_LABEL_AS_TRIGGERED("TR2_OUTVAN", TRUE)
												ENDIF
											ENDIF
										ENDIF
										
									ELSE
									
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl2b")
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl2b", CONV_PRIORITY_MEDIUM)
														SET_LABEL_AS_TRIGGERED("TR_pl2b", TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_ENTITY_DEAD(psEnemies[1].PedIndex)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("Delay")
								psRon.iConversationTimer = GET_GAME_TIMER()
								SET_LABEL_AS_TRIGGERED("Delay", TRUE)
							ELSE
								IF MANAGE_MY_TIMER(psRon.iConversationTimer,1000)
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl1c")
										IF NOT IS_PED_IN_ANY_VEHICLE(psEnemies[1].PedIndex)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl1c", CONV_PRIORITY_MEDIUM)
													
														REPLAY_RECORD_BACK_FOR_TIME(2.0, 2.0, REPLAY_IMPORTANCE_HIGHEST)
													
														SET_LABEL_AS_TRIGGERED("TR_pl1c", TRUE)
													ENDIF
												ENDIF
											ELSE
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF psRon.eState = PED_STATE_PLEADING
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CAUGHT_1")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CAUGHT", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_1", TRUE)
											ENDIF
										ELSE
											IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_EZAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
												SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_1", TRUE)
											ENDIF
										ENDIF
									ELSE
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF
					
				ENDIF
				
				IF DOES_ENTITY_EXIST(psEnemies[2].PedIndex)
				
					IF NOT IS_ENTITY_DEAD(psEnemies[2].PedIndex)
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_guytow")
							IF HAS_LABEL_BEEN_TRIGGERED("TR_pl1c")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_guytow", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR_guytow", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ELSE
						
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3")
								
								//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									//IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3", CONV_PRIORITY_MEDIUM)
										psRon.iConversationTimer = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("TR_pl3", TRUE)
									//ENDIF
								//ENDIF
								
							ELSE
							
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_look2")
									IF HAS_TIME_PASSED(6000, psRon.iConversationTimer)
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_look2", CONV_PRIORITY_MEDIUM)
												psRon.iConversationTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							
							ENDIF
							
						ENDIF
						
					ENDIF
					
					IF IS_ENTITY_DEAD(psEnemies[2].PedIndex)
					
						IF HAS_LABEL_BEEN_TRIGGERED("TR_guytow")			//if Ron's instruction conversation was played
						
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3aa_1")	//Ron praises Trevor for the kill 
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3aa", CONV_PRIORITY_MEDIUM)
										
											REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
											
											SET_LABEL_AS_TRIGGERED("TR_pl3aa_1", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF						
						ENDIF
					
					ELSE
						IF psRon.eState = PED_STATE_PLEADING
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CAUGHT_2")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CAUGHT", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_2", TRUE)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_EZAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_2", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
				
				IF DOES_ENTITY_EXIST(psEnemies[3].PedIndex)
				
					IF NOT IS_ENTITY_DEAD(psEnemies[3].PedIndex)
						

						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3ap")
						
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3ap", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("TR_pl3ap", TRUE)
									ENDIF
								ENDIF
							ENDIF
							
						ELSE

							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_TBOTTOM")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_TBOTTOM", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_TBOTTOM", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF

						ENDIF

					ENDIF
				
					IF IS_ENTITY_DEAD(psEnemies[3].PedIndex)
					
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl1d")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl1d", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("TR_pl1d", TRUE)
									ENDIF
								ENDIF
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF

					ELSE
						IF psRon.eState = PED_STATE_PLEADING
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CAUGHT_3")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CAUGHT", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_3", TRUE)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_EZAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_3", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
				
				IF DOES_ENTITY_EXIST(psEnemies[4].PedIndex)
				
					IF NOT IS_ENTITY_DEAD(psEnemies[4].PedIndex)
						
						//DIALOGUE FOR SEEING THE DEAD BODY
						IF HAS_LABEL_BEEN_TRIGGERED("TR_guytow2b")
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_BODY 4")
								IF IS_ENTITY_PLAYING_ANIM(psEnemies[4].PedIndex, "guard_reactions", "med_down")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_BODY", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR_BODY 4", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_guytow2b")
						
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_guytow2b", CONV_PRIORITY_MEDIUM)
										psRon.iConversationTimer = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("TR_guytow2b", TRUE)
									ENDIF
								ENDIF
							ENDIF
							
						ELSE
						
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_look2")
								IF HAS_TIME_PASSED(6000, psRon.iConversationTimer)
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_look2", CONV_PRIORITY_MEDIUM)
											psRon.iConversationTimer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						ENDIF
					
					ENDIF
					
					
					
					IF IS_ENTITY_DEAD(psEnemies[4].PedIndex)
					
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3aa_2")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3aa", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("TR_pl3aa_2", TRUE)
									ENDIF
								ENDIF
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
					
					ELSE
						IF psRon.eState = PED_STATE_PLEADING
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CAUGHT_4")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CAUGHT", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_4", TRUE)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_EZAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_4", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				IF DOES_ENTITY_EXIST(psEnemies[5].PedIndex)
				
					IF NOT IS_ENTITY_DEAD(psEnemies[5].PedIndex)
						
						//CONFIRMATION LINE FROM RON
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3a1")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3a1", CONV_PRIORITY_MEDIUM)
									
										REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
										
										SET_LABEL_AS_TRIGGERED("TR_pl3a1", TRUE)
									ENDIF
								ENDIF
							ENDIF
						
						ELSE
						
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3a4")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3a4", CONV_PRIORITY_MEDIUM)
											iDelayDialogueTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("TR_pl3a4", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_OUT")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF MANAGE_MY_TIMER(iDelayDialogueTimer,12000)
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_OUT", CONV_PRIORITY_MEDIUM)
													SET_LABEL_AS_TRIGGERED("TR_OUT", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF

					ENDIF
					
					IF IS_ENTITY_DEAD(psEnemies[5].PedIndex)
					
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl1e")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl1b", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("TR_pl1e", TRUE)
									ENDIF
								ENDIF
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
					
					ELSE
						IF psRon.eState = PED_STATE_PLEADING
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CAUGHT_5")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CAUGHT", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_5", TRUE)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_EZAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_5", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
				
				IF DOES_ENTITY_EXIST(psEnemies[6].PedIndex)
				
					IF NOT IS_ENTITY_DEAD(psEnemies[6].PedIndex)
						//DIALOGUE FOR SEEING THE DEAD BODY
						IF HAS_LABEL_BEEN_TRIGGERED("TR_SECGUY")
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_BODY 6")
								IF IS_ENTITY_PLAYING_ANIM(psEnemies[6].PedIndex, "guard_reactions", "med_down")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_BODY", CONV_PRIORITY_MEDIUM)
											
												REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
												
												SET_LABEL_AS_TRIGGERED("TR_BODY 6", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_SECGUY")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_SECGUY", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("TR_SECGUY", TRUE)
									ENDIF
								ENDIF
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
					ENDIF
					
					//GO HOOK UP THE EXPLOSIVE
					IF IS_ENTITY_DEAD(psEnemies[6].PedIndex)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3c")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									//IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3c", CONV_PRIORITY_MEDIUM)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl1b", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("TR_pl3c", TRUE)
									ENDIF
								ENDIF
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
					ELSE
						IF psRon.eState = PED_STATE_PLEADING
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CAUGHT_6")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CAUGHT", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_6", TRUE)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_EZAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_6", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
				
				IF DOES_ENTITY_EXIST(psEnemies[7].PedIndex)
				
					IF NOT IS_ENTITY_DEAD(psEnemies[7].PedIndex)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_SOME")
							IF ( fEighthEnemyPlaybackTime > 4000.0)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_SOME", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR_SOME", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RONSPOT18")
								IF IS_VEHICLE_DRIVEABLE(vsEnemyBikes[0].VehicleIndex)
									IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyBikes[0].VehicleIndex)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_RONSPOT1", CONV_PRIORITY_MEDIUM)
//													iEnemyOffBike = GET_GAME_TIMER()
													SET_LABEL_AS_TRIGGERED("TR2_RONSPOT18", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
//								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RONSPOT28")
//									IF MANAGE_MY_TIMER(iEnemyOffBike,5000)
//										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_RONSPOT2", CONV_PRIORITY_MEDIUM)
//													SET_LABEL_AS_TRIGGERED("TR2_RONSPOT28", TRUE)
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//NEW
					IF IS_ENTITY_DEAD(psEnemies[7].PedIndex)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl1f")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl1f", CONV_PRIORITY_MEDIUM)
										
										REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
									
										SET_LABEL_AS_TRIGGERED("TR_pl1f", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF psRon.eState = PED_STATE_PLEADING
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CAUGHT_7")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CAUGHT", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_7", TRUE)
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(psRon.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psRon.PedIndex, "TR2_EZAA", "NERVOUSRON", SPEECH_PARAMS_FORCE_FRONTEND)
											SET_LABEL_AS_TRIGGERED("TR2_CAUGHT_7", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3a2")		//play conversations after killing first 5 enemies and Ron has to move on
				
					IF 	DOES_ENTITY_EXIST(psEnemies[0].PedIndex) AND IS_ENTITY_DEAD(psEnemies[0].PedIndex)
					AND DOES_ENTITY_EXIST(psEnemies[1].PedIndex) AND IS_ENTITY_DEAD(psEnemies[1].PedIndex)
					AND DOES_ENTITY_EXIST(psEnemies[2].PedIndex) AND IS_ENTITY_DEAD(psEnemies[2].PedIndex)
					AND DOES_ENTITY_EXIST(psEnemies[3].PedIndex) AND IS_ENTITY_DEAD(psEnemies[3].PedIndex)
					AND DOES_ENTITY_EXIST(psEnemies[4].PedIndex) AND IS_ENTITY_DEAD(psEnemies[4].PedIndex)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3a2", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("TR_pl3a2", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3a3")
					
						IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_2", "idle")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3a3", CONV_PRIORITY_MEDIUM)
										psRon.iConversationTimer = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("TR_pl3a3", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ELSE
					
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl3a3")
							IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_2", "idle")
								IF HAS_TIME_PASSED(6000, psRon.iConversationTimer)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl3a3", CONV_PRIORITY_MEDIUM)
												psRon.iConversationTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
											
					ENDIF
					
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_LOOK")
					IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_7", "plant_bomb")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_LOOK", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("TR_LOOK", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_TANK")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_TANK", CONV_PRIORITY_MEDIUM)
									
									REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
									
									SET_LABEL_AS_TRIGGERED("TR_TANK", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(psEnemies[7].PedIndex)
							IF IS_ENTITY_DEAD(psEnemies[7].PedIndex)
								IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_HELI_SILENT")
									STOP_AUDIO_SCENE("TREVOR_2_HELI_SILENT")
								ENDIF
								IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_HELI_APPROACH")
									START_AUDIO_SCENE("TREVOR_2_HELI_APPROACH")
								ENDIF
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl4p")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl4p", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR_pl4p", TRUE)
												SET_LABEL_AS_TRIGGERED("TR2_HELSE", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF HAS_LABEL_BEEN_TRIGGERED("SJC_KPILOT")
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_HELSE")
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_HELSE", CONV_PRIORITY_MEDIUM)
														iTakeOutTheChopperTimer = GET_GAME_TIMER()
														SET_LABEL_AS_TRIGGERED("TR2_HELSE", TRUE)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF MANAGE_MY_TIMER(iTakeOutTheChopperTimer,GET_RANDOM_INT_IN_RANGE(8000,12000))
												SET_LABEL_AS_TRIGGERED("TR2_HELSE", FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF HAS_LABEL_BEEN_TRIGGERED("TR_SOME")
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RONSPOT1")
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_RONSPOT1", CONV_PRIORITY_MEDIUM)
													SET_LABEL_AS_TRIGGERED("TR2_RONSPOT1", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_ALERTED_ENEMIES(INT &iThisProgress)

	REQUEST_SCRIPT_AUDIO_BANK("ALARM_KLAXON_03")

	SWITCH iThisProgress
	
		CASE 0
		
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, << 1717.39, 3300.94, 40.21 >>, 100.0)
				bBikersAlerted = TRUE
			ENDIF
			
			IF ( bBikersAlerted = TRUE )

				IF ( iBikersAlertTimer = 0 )
				
					iBikersAlertTimer = GET_GAME_TIMER()
					
				ELSE
					IF HAS_TIME_PASSED(2000, iBikersAlertTimer)
					
						PLAY_SOUND_FROM_COORD(iAlarmFarSoundID, "ALARMS_KLAXON_03_FAR", << 1701.05, 3292.24, 53.29 >>)
						PLAY_SOUND_FROM_COORD(iAlarmCloseSoundID, "ALARMS_KLAXON_03_CLOSE", << 1701.05, 3292.24, 53.29 >>)
					
						iThisProgress++
						
					ENDIF
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(G_M_Y_LOST_01)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE,ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS),WEAPON_COMPONENT_FLASH)
			REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE))
			
			IF 	HAS_MODEL_LOADED(G_M_Y_LOST_01)
			AND	HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
			AND HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE))
			
				INT i
			
				REPEAT 4 i
				
					psAlertedEnemies[i].PedIndex = CREATE_ENEMY_PED(G_M_Y_LOST_01, psAlertedEnemies[i].vPosition, psAlertedEnemies[i].fHeading,
																	rel_group_enemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH)
																	
					ADD_DEADPOOL_TRIGGER(psAlertedEnemies[i].PedIndex, TRV2_KILLS)
					
					SET_ENTITY_LOD_DIST(psAlertedEnemies[i].PedIndex, 500)
					SET_ENTITY_LOAD_COLLISION_FLAG(psAlertedEnemies[i].PedIndex, TRUE)
													
					SET_PED_COMBAT_RANGE(psAlertedEnemies[i].PedIndex, CR_NEAR)
					SET_PED_COMBAT_MOVEMENT(psAlertedEnemies[i].PedIndex, CM_DEFENSIVE)
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(psAlertedEnemies[i].PedIndex, psRon.PedIndex, << 0.0, 0.0, 0.0 >>, 2.0)
					
					SET_PED_ACCURACY(psAlertedEnemies[i].PedIndex, 1)
					SET_PED_SHOOT_RATE(psAlertedEnemies[i].PedIndex, 175)
					
					SET_PED_CHANCE_OF_FIRING_BLANKS(psAlertedEnemies[i].PedIndex, 0.9, 0.9)
					SET_PED_FIRING_PATTERN(psAlertedEnemies[i].PedIndex, FIRING_PATTERN_FULL_AUTO)
					
					SET_COMBAT_FLOAT(psAlertedEnemies[i].PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
					SET_COMBAT_FLOAT(psAlertedEnemies[i].PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
					
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_USE_COVER, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_CAN_FLANK, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_CAN_CHARGE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_ALWAYS_FIGHT, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED, TRUE)
					
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(psAlertedEnemies[i].PedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
					
					TASK_COMBAT_PED(psAlertedEnemies[i].PedIndex, psRon.PedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psAlertedEnemies[i].PedIndex, FALSE)
				
				ENDREPEAT
				
				bAlertedEnemiesSpawned = TRUE
				
				iBikersAlertTimer = GET_GAME_TIMER()
				
				iThisProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2

			iThisProgress++
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_PLAYER_AMMO()
	
	WEAPON_TYPE CurrentPlayerWeapon
	
	IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentPlayerWeapon)
		IF (CurrentPlayerWeapon = WEAPONTYPE_SNIPERRIFLE)
			IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE) < 1
				ADD_AMMO_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,20)
			ENDIF
		ENDIF
		IF (CurrentPlayerWeapon = WEAPONTYPE_HEAVYSNIPER)
			IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_HEAVYSNIPER) < 1
				ADD_AMMO_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_HEAVYSNIPER,20)
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

BOOL bAllowToBlow = FALSE
INT iGasTankShotsCount 
BOOL bSetTankInitialProofs

PROC MANAGE_GAS_TANK()
	IF DOES_ENTITY_EXIST(osPropGasTanks[0].ObjectIndex)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(osPropGasTanks[0].ObjectIndex, PLAYER_PED_ID(), FALSE)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(osPropGasTanks[0].ObjectIndex, WEAPONTYPE_SNIPERRIFLE)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(osPropGasTanks[0].ObjectIndex, WEAPONTYPE_HEAVYSNIPER)
				iGasTankShotsCount++
				PRINTSTRING("Gas tank shot")PRINTNL()
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(osPropGasTanks[0].ObjectIndex)
			ENDIF
		ENDIF
	ENDIF
	
	IF ( iGasTankShotsCount >= 3 )
		IF DOES_ENTITY_EXIST(osPropGasTanks[0].ObjectIndex)
			IF NOT IS_ENTITY_DEAD(osPropGasTanks[0].ObjectIndex)
				IF NOT bAllowToBlow
					bAllowToBlow = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(crate[1], PLAYER_PED_ID(), FALSE)
		SET_ENTITY_PROOFS(crate[1],FALSE,FALSE,FALSE,FALSE,FALSE)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(crate[1], WEAPONTYPE_SNIPERRIFLE)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(crate[1], WEAPONTYPE_HEAVYSNIPER)
			iGasTankShotsCount++
			PRINTSTRING("Gas tank shot")PRINTNL()
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(crate[1])
		ENDIF
	ENDIF
	
	IF NOT bSetTankInitialProofs
		IF DOES_ENTITY_EXIST(crate[1])
			SET_ENTITY_INVINCIBLE(crate[1],TRUE)
			bSetTankInitialProofs = TRUE
		ENDIF
	ENDIF
	
	IF ( iGasTankShotsCount >= 3 )
		IF DOES_ENTITY_EXIST(crate[1])
			IF NOT bAllowToBlow
				SET_ENTITY_INVINCIBLE(crate[1], FALSE)
				SET_ENTITY_PROOFS(crate[1], FALSE, FALSE, FALSE, FALSE, FALSE)
				
				SET_ENTITY_CAN_BE_DAMAGED(crate[1], TRUE)
				
				SET_ENTITY_DYNAMIC(crate[1], FALSE)
				
				SET_DISABLE_BREAKING(crate[1], FALSE)
				bAllowToBlow = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	
ENDPROC

PROC SNIPE()
	
	//REMAP_LODSCALE_RANGE_THIS_FRAME( oldMin, oldMax, newMin, newMax )
	MANAGE_GAS_TANK()
	MANAGE_PLAYER_AMMO()
	MANAGE_RON_DURING_SNIPING(psRon)
	MANAGE_LIGHT_PROP_OBJECTS(osPropWallLights)
	MANAGE_GAS_TANK_PROP_OBJECTS(osPropGasTanks,bAllowToBlow)
	MANAGE_ENEMY_MAVERICK(iEnemyMaverickProgress)
	MANAGE_ALERTED_ENEMIES(iAlertedEnemiesProgress)
	MANAGE_FIRST_SNIPING_ENEMY(psEnemies[0], iFirstSnipingEnemyProgress)
	MANAGE_SECOND_SNIPING_ENEMY(psEnemies[1], iSecondSnipingEnemyProgress)
	MANAGE_THIRD_SNIPING_ENEMY(psEnemies[2], iThirdSnipingEnemyProgress)
	MANAGE_FOURTH_SNIPING_ENEMY(psEnemies[3], iFourthSnipingEnemyProgress)
	MANAGE_FIFTH_SNIPING_ENEMY(psEnemies[4], iFifthSnipingEnemyProgress)
	MANAGE_SIXTH_SNIPING_ENEMY(psEnemies[5], iSixthSnipingEnemyProgress)
	MANAGE_SEVENTH_SNIPING_ENEMY(psEnemies[6], iSeventhSnipingEnemyProgress)
	MANAGE_EIGHTH_SNIPING_ENEMY(psEnemies[7], iEighthSnipingEnemyProgress)

	MANAGE_CONVERSATIONS_DURING_SNIPING()
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("SNIPE REPLAY SET")
		//NEW_LOAD_SCENE_START_SPHERE(<<1699.9397, 3298.1697, 40.1478>>, 2.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
		//SnipeVol = STREAMVOL_CREATE_SPHERE(<<1707.1062, 3294.0757, 40.1483>>, 10.0,FLAG_MAPDATA,STREAMVOL_LOD_FLAG_HIGH)
		//Fix for missing nav mesh arond the trees
		ADD_NAVMESH_BLOCKING_OBJECT(<<1681.287,3289.904,40.413>>,<<2.900,2.200,1.400>>,-5.260)
		ADD_NAVMESH_BLOCKING_OBJECT(<<1682.755,3293.533,40.568>>,<<1.2,1.2,1.2>>,-2.584)
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_QUAD].veh)
			SET_VEHICLE_LIGHTS(mission_veh[MV_RON_QUAD].veh, FORCE_VEHICLE_LIGHTS_OFF)
		ENDIF

		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_MAX_WANTED_LEVEL(0)
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Cover Ron with Sniper Rifle")
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_TRAINS_VOLUME")
			START_AUDIO_SCENE("TREVOR_2_TRAINS_VOLUME")
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SNIPE_MAIN")
			START_AUDIO_SCENE("TREVOR_2_SNIPE_MAIN")
		ENDIF
		
		bAllowToBlow = FALSE
		bQuadPresent = FALSE
		iGasTankShotsCount = 0
		
		SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SNIPING ) 
		SET_FLASH_LIGHT_FADE_DISTANCE(400)
		
		SET_LABEL_AS_TRIGGERED("SNIPE REPLAY SET",TRUE)
	ENDIF
	
	SWITCH iProgress
	
		CASE 0

			//HELP TEXT
			IF DOES_PLAYER_HAVE_THE_SNIPER_RIFLE_EQUIPED()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_EQUIP")
					CLEAR_HELP()
				ENDIF
				IF GET_PROFILE_SETTING(PROFILE_CONTROLLER_SNIPER_ZOOM) = 1
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_SNIPEH1")
						IF DOES_ENTITY_EXIST(psRon.PedIndex)
							IF NOT IS_ENTITY_DEAD(psRon.PedIndex)
								PRINT_HELP("SJC_SNIPEH1")
								SET_LABEL_AS_TRIGGERED("SJC_SNIPEH1",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_SNIPEH2")
						IF DOES_ENTITY_EXIST(psRon.PedIndex)
							IF NOT IS_ENTITY_DEAD(psRon.PedIndex)
								PRINT_HELP("SJC_SNIPEH2")
								SET_LABEL_AS_TRIGGERED("SJC_SNIPEH2",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_EQUIP")
					IF DOES_ENTITY_EXIST(psRon.PedIndex)
						IF NOT IS_ENTITY_DEAD(psRon.PedIndex)
							PRINT_HELP("SJC_EQUIP")
							SET_LABEL_AS_TRIGGERED("SJC_EQUIP",TRUE)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("H_SNIPESTAT")
				IF DOES_ENTITY_EXIST(psEnemies[0].PedIndex)
					IF IS_PED_INJURED(psEnemies[0].PedIndex)
						PRINT_HELP("H_SNIPESTAT")
						SET_LABEL_AS_TRIGGERED("H_SNIPESTAT",TRUE)
					ENDIF
				ENDIF
			ENDIF	
		
			IF NOT DOES_BLIP_EXIST(DestinationBlipIndex)
				IF NOT IS_PED_AT_WATER_TOWER(PLAYER_PED_ID())
					bPedBlipsHidden = TRUE
					DestinationBlipIndex = ADD_BLIP_FOR_COORD(vWaterTowerTopPosition)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_RETRW")
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
						IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
							CLEAR_PRINTS()
						ENDIF
						PRINT_GOD_TEXT_ADVANCED("SJC_RETRW", DEFAULT_GOD_TEXT_TIME, TRUE)
					ENDIF					
				ENDIF
			ELSE
				IF IS_PED_AT_WATER_TOWER(PLAYER_PED_ID())
				AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())
					IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_RETRW")
						CLEAR_PRINTS()
					ENDIF
					REMOVE_BLIP(DestinationBlipIndex)
					bPedBlipsHidden = FALSE
				ENDIF
			ENDIF
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_SNIPEG1")
				IF IS_PED_AT_WATER_TOWER(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(psRon.PedIndex)
						IF NOT IS_ENTITY_DEAD(psRon.PedIndex)
							IF ( psRon.eState = PED_STATE_SNEAKING_INTRO )
								IF IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_2", "idle")
								OR IS_ENTITY_PLAYING_ANIM(psRon.PedIndex, "misstrevor2ig_2", "intro") //1335270
									IF NOT IS_PED_IN_PLAYER_SNIPER_SCOPE(psRon.PedIndex)
										PRINT_GOD_TEXT_ADVANCED("SJC_SNIPEG1", DEFAULT_GOD_TEXT_TIME, TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_IN_PLAYER_SNIPER_SCOPE(psRon.PedIndex)
					IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_SNIPEG1")
						CLEAR_PRINTS()
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_ENEM1G_0")
				IF DOES_ENTITY_EXIST(psEnemies[0].PedIndex)
					IF NOT IS_ENTITY_DEAD(psEnemies[0].PedIndex)
						IF HAS_LABEL_BEEN_TRIGGERED("TR_sn01p")
							IF 	NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND	NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								IF IS_PED_AT_WATER_TOWER(PLAYER_PED_ID())
									PRINT_GOD_TEXT_ADVANCED("SJC_ENEM1G", DEFAULT_GOD_TEXT_TIME, FALSE)
									SET_LABEL_AS_TRIGGERED("SJC_ENEM1G_0", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_DEAD(psEnemies[0].PedIndex)
					IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_ENEM1G")
						CLEAR_PRINTS()
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_LIGHTS")
				IF HAS_LABEL_BEEN_TRIGGERED("TR2_SHOOTL")
					PRINT_HELP("SJC_LIGHTS", DEFAULT_HELP_TEXT_TIME)
					SET_LABEL_AS_TRIGGERED("SJC_LIGHTS", TRUE)
				ENDIF
			ELSE
				IF HAS_LABEL_BEEN_TRIGGERED("TR2_LIGHTS")
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_LIGHTS")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
				IF HAS_LABEL_BEEN_TRIGGERED("TR_pl4p")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND	NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_KPILOT")
							
							REPLAY_RECORD_BACK_FOR_TIME(8.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
							
							//only print god text if the pilot is still alive
							IF DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex) AND NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
								PRINT_GOD_TEXT("SJC_KPILOT")
							ENDIF
							
							SET_LABEL_AS_TRIGGERED("SJC_KPILOT", TRUE)
						ENDIF
					ENDIF
				ELSE
					IF HAS_LABEL_BEEN_TRIGGERED("SJC_PINDWN2H")
						PRINT_HELP("SJC_PINDWN2H")
						SET_LABEL_AS_TRIGGERED("SJC_PINDWN2H", TRUE)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_QUAD].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
						bQuadPresent = TRUE
					ELSE
						bQuadPresent = FALSE
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
						IF IS_ENTITY_IN_ANGLED_AREA(mission_veh[MV_TREVOR_QUAD].veh, <<1580.223877,3352.752441,34.924610>>, <<1560.872314,3386.379150,40.720592>>, 19.500000)
							bQuadPresent = TRUE
						ELSE
							bQuadPresent = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF bQuadPresent = TRUE
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("trv_2_mcs_4_concat", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4)
				ELSE
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("trv_2_mcs_4_concat", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_5)
				ENDIF
				
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
					ENDIF
					
					IF 	DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
					AND	NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Pilot", psMaverickEnemies[0].PedIndex, G_M_Y_LOST_01)
					ELSE
						//call all the variations this ped should have in the cutscene
					ENDIF
					
					IF 	DOES_ENTITY_EXIST(psMaverickEnemies[1].PedIndex)
					AND	NOT IS_ENTITY_DEAD(psMaverickEnemies[1].PedIndex)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lost_sniper", psMaverickEnemies[1].PedIndex, G_M_Y_LOST_01)
					ELSE
						//call all the variations this ped should have in the cutscene
					ENDIF
					
				ENDIF
				
			ENDIF
			
			IF DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
				IF IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
		
					IF ( bBikersAlerted = FALSE )
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"TR2_DXAA","TREVOR",SPEECH_PARAMS_FORCE)
						STOP_AUDIO_SCENE("TREVOR_2_HELI_APPROACH")
						NEW_LOAD_SCENE_STOP()
						CLEAR_HELP()
						IF STREAMVOL_IS_VALID(SnipeVol)
							STREAMVOL_DELETE(SnipeVol)
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
						
						ADVANCE_MISSION_STAGE()
						CLEAR_PRINTS()
					ENDIF
					
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH


ENDPROC


FUNC BOOL HAS_REQUESTED_CUTSCENE_LOADED(STRING sSceneName, CUTSCENE_SECTION CutsceneSection = CS_SECTION_1)

	//check if the specified cutscene has loaded first
	IF HAS_THIS_CUTSCENE_LOADED(sSceneName)
	
		RETURN TRUE
	
	ELSE
		
		//if the expected reuqested cutscene has not loaded, but there already is a loaded cutscene
		//then remove it and keep requesting specified mocap cutscene
		IF HAS_CUTSCENE_LOADED()
			
			REMOVE_CUTSCENE()
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": HAS_REQUESTED_CUTSCENE_LOADED() Removing cutscene from memory, because cutscene ", sSceneName, " was expected to be loaded.")
			#ENDIF
			
		ENDIF
	
		IF ( CutsceneSection = CS_SECTION_1) 
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading.")
			#ENDIF
			
			//if the cutscene is not loaded, keep requesting it
			REQUEST_CUTSCENE(sSceneName)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, ".")
			#ENDIF
		
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading with playback list.")
			#ENDIF
			
			//if the cutscene is not loaded, keep requesting it
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sSceneName, CutsceneSection)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, " cutscene with playback list.")
			#ENDIF
		
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC

INT iExplosionTimer
VECTOR vGetHeliPos

PROC MANAGE_HELI_EXPLOSION()

	REQUEST_PTFX_ASSET()
	REQUEST_COLLISION_FOR_MODEL(MAVERICK)
	REQUEST_COLLISION_AT_COORD(<<1693.43, 3317.96, 40.46>>)
	IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
		SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MAVERICK].veh,TRUE)
	ENDIF
	IF HAS_PTFX_ASSET_LOADED()
	AND REQUEST_AMBIENT_AUDIO_BANK("TREVOR_2_CHOPPER_EXPLOSION_TAIL")
		IF bHitGround = FALSE
			IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
				vGetHeliPos = GET_ENTITY_COORDS(mission_veh[MV_MAVERICK].veh)
				PRINTSTRING("vGetHeliPos")PRINTVECTOR(vGetHeliPos)PRINTNL()
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MAVERICK].veh,TRUE)
				IF vGetHeliPos.Z < 43.0
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(mission_veh[MV_MAVERICK].veh)
					OR vGetHeliPos.Z < 41.3
						SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(mission_veh[MV_MAVERICK].veh,FALSE)
						PTFXfire[1] =START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev2_heli_wreck",GET_ENTITY_COORDS(mission_veh[MV_MAVERICK].veh,FALSE),<<0,0,0>>)
						bHitGround = TRUE
						PRINTSTRING("BOOM!!! - PTFX")PRINTNL()
						iExplosionTimer = GET_GAME_TIMER()
						PLAY_SOUND_FROM_ENTITY(-1,"Trevor_2_chopper_explode",mission_veh[MV_MAVERICK].veh,"TREVOR_2_SOUNDS")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MANAGE_MY_TIMER(iExplosionTimer,50)
				IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_trev2_heli_exp", << 1704, 3320, 40 >>, << -3.7, 3.4, -81.5 >>)
//					ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[MV_MAVERICK].veh),EXP_TAG_PLANE)
					PRINTSTRING("BOOM!!! - EXPLODE")PRINTNL()
					EXPLODE_VEHICLE(mission_veh[MV_MAVERICK].veh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_WEAPON_TYPE_MODEL_LOADED( WEAPON_TYPE eWeaponType, BOOL bUnload = FALSE )
	MODEL_NAMES eMdl = GET_WEAPONTYPE_MODEL( eWeaponType )
	IF( NOT bUnload )
		REQUEST_MODEL( eMdl )
		RETURN HAS_MODEL_LOADED( eMdl )
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED( eMdl )
		RETURN FALSE
	ENDIF
ENDFUNC

BOOL bJumpTime
OBJECT_INDEX TrevorWeapon

PROC RUN_SHOOT_DOWN_HELI_CUTSCENE()

	IF IS_CUTSCENE_PLAYING()
		MANAGE_HELI_EXPLOSION()
	ENDIF
	
	SWITCH iprogress
	
		CASE 0
		
			//4 - set into vehicle
			//5 - run
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
				bQuadPresent = TRUE
			ELSE
				bQuadPresent = FALSE
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(mission_veh[MV_TREVOR_QUAD].veh, <<1580.223877,3352.752441,34.924610>>, <<1560.872314,3386.379150,40.720592>>, 19.500000)
					bQuadPresent = TRUE
				ELSE
					bQuadPresent = FALSE
				ENDIF
			ENDIF
			
			IF bQuadPresent = TRUE
				IF HAS_REQUESTED_CUTSCENE_LOADED("trv_2_mcs_4_concat", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4)
				AND HAS_WEAPON_TYPE_MODEL_LOADED(WEAPONTYPE_SNIPERRIFLE)
					
					TrevorWeapon =CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE)
					HAS_WEAPON_TYPE_MODEL_LOADED(WEAPONTYPE_SNIPERRIFLE, TRUE)
					IF DOES_ENTITY_EXIST(TrevorWeapon)
						REGISTER_ENTITY_FOR_CUTSCENE(TrevorWeapon,"Trevors_weapon", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
				
					IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_QUAD].veh)
						IF NOT IS_ENTITY_DEAD(mission_veh[MV_TREVOR_QUAD].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[MV_TREVOR_QUAD].veh, "Trevors_Quad", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							
							SET_VEHICLE_ENGINE_ON(mission_veh[MV_TREVOR_QUAD].veh, TRUE, TRUE)
							SET_VEHICLE_LIGHTS(mission_veh[MV_TREVOR_QUAD].veh, FORCE_VEHICLE_LIGHTS_ON)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
						IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[MV_MAVERICK].veh, "Lost_heli", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
						IF NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
							REGISTER_ENTITY_FOR_CUTSCENE(psMaverickEnemies[0].PedIndex, "Pilot", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ELSE
							DELETE_PED(psMaverickEnemies[0].PedIndex)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(psMaverickEnemies[1].PedIndex)
						IF NOT IS_ENTITY_DEAD(psMaverickEnemies[1].PedIndex)
						
							REGISTER_ENTITY_FOR_CUTSCENE(psMaverickEnemies[1].PedIndex, "Lost_sniper", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
							
							IF NOT DOES_ENTITY_EXIST(LostSniperWeapon)
								LostSniperWeapon = GET_WEAPON_OBJECT_FROM_PED(psMaverickEnemies[1].PedIndex)
							ENDIF
							
							IF DOES_ENTITY_EXIST(LostSniperWeapon)
								IF NOT IS_ENTITY_DEAD(LostSniperWeapon)
									IF IS_ENTITY_ATTACHED(LostSniperWeapon)
										DETACH_ENTITY(LostSniperWeapon)
									ENDIF
									REGISTER_ENTITY_FOR_CUTSCENE(LostSniperWeapon, "HC_Driver_Rifle", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, (GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE)))
								ENDIF
							ENDIF

						ELSE
						
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Lost_sniper", CU_DONT_ANIMATE_ENTITY, G_M_Y_LOST_01)
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "HC_Driver_Rifle", CU_DONT_ANIMATE_ENTITY, (GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE)))
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Lost_Mag", CU_DONT_ANIMATE_ENTITY, GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_01))
							
						ENDIF
					ENDIF

					CLEAR_HELP()
					CLEAR_PRINTS()
					
					CLEANUP_AI_PED_BLIP(psMaverickEnemies[0].EnemyBlipData)
					CLEANUP_AI_PED_BLIP(psMaverickEnemies[1].EnemyBlipData)
						
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					STOP_AUDIO_SCENE("TREVOR_2_SNIPE_MAIN")
					START_AUDIO_SCENE("TREVOR_2_HELI_CRASH")
					
					REQUEST_PTFX_ASSET()
					REQUEST_COLLISION_FOR_MODEL(MAVERICK)
					REQUEST_AMBIENT_AUDIO_BANK("TREVOR_2_CHOPPER_EXPLOSION_TAIL")

					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

					START_CUTSCENE()
					REQUEST_PTFX_ASSET()
					REQUEST_IPL("airfield")
					bJumpTime = FALSE
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Shoot down heli cutscene") 
					iprogress++
				
				ELSE
				
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
						ENDIF
						
						IF 	DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
						AND	NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Pilot", psMaverickEnemies[0].PedIndex, G_M_Y_LOST_01)
						ELSE
							//call all the variations this ped should have in the cutscene
						ENDIF
						
						IF 	DOES_ENTITY_EXIST(psMaverickEnemies[1].PedIndex)
						AND	NOT IS_ENTITY_DEAD(psMaverickEnemies[1].PedIndex)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lost_sniper", psMaverickEnemies[1].PedIndex, G_M_Y_LOST_01)
						ELSE
							//call all the variations this ped should have in the cutscene
						ENDIF
						
					ENDIF
				
				ENDIF
			ELSE
				IF HAS_REQUESTED_CUTSCENE_LOADED("trv_2_mcs_4_concat", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_5)
				AND HAS_WEAPON_TYPE_MODEL_LOADED(WEAPONTYPE_SNIPERRIFLE)
					
					TrevorWeapon =CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE)
					HAS_WEAPON_TYPE_MODEL_LOADED(WEAPONTYPE_SNIPERRIFLE, TRUE)
					IF DOES_ENTITY_EXIST(TrevorWeapon)
						REGISTER_ENTITY_FOR_CUTSCENE(TrevorWeapon,"Trevors_weapon", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
				
					IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_QUAD].veh)
						IF NOT IS_ENTITY_DEAD(mission_veh[MV_TREVOR_QUAD].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[MV_TREVOR_QUAD].veh, "Trevors_Quad", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							
							SET_VEHICLE_ENGINE_ON(mission_veh[MV_TREVOR_QUAD].veh, TRUE, TRUE)
							SET_VEHICLE_LIGHTS(mission_veh[MV_TREVOR_QUAD].veh, FORCE_VEHICLE_LIGHTS_ON)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
						IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[MV_MAVERICK].veh, "Lost_heli", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
						IF NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
							REGISTER_ENTITY_FOR_CUTSCENE(psMaverickEnemies[0].PedIndex, "Pilot", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ELSE
							DELETE_PED(psMaverickEnemies[0].PedIndex)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(psMaverickEnemies[1].PedIndex)
						IF NOT IS_ENTITY_DEAD(psMaverickEnemies[1].PedIndex)
						
							REGISTER_ENTITY_FOR_CUTSCENE(psMaverickEnemies[1].PedIndex, "Lost_sniper", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
							
							IF NOT DOES_ENTITY_EXIST(LostSniperWeapon)
								LostSniperWeapon = GET_WEAPON_OBJECT_FROM_PED(psMaverickEnemies[1].PedIndex)
							ENDIF
							
							IF DOES_ENTITY_EXIST(LostSniperWeapon)
								IF NOT IS_ENTITY_DEAD(LostSniperWeapon)
									IF IS_ENTITY_ATTACHED(LostSniperWeapon)
										DETACH_ENTITY(LostSniperWeapon)
									ENDIF
									REGISTER_ENTITY_FOR_CUTSCENE(LostSniperWeapon, "HC_Driver_Rifle", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, (GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE)))
								ENDIF
							ENDIF

						ELSE
						
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Lost_sniper", CU_DONT_ANIMATE_ENTITY, G_M_Y_LOST_01)
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "HC_Driver_Rifle", CU_DONT_ANIMATE_ENTITY, (GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE)))
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Lost_Mag", CU_DONT_ANIMATE_ENTITY, GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_01))
							
						ENDIF
					ENDIF

					CLEAR_HELP()
					CLEAR_PRINTS()
					
					CLEANUP_AI_PED_BLIP(psMaverickEnemies[0].EnemyBlipData)
					CLEANUP_AI_PED_BLIP(psMaverickEnemies[1].EnemyBlipData)
						
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					STOP_AUDIO_SCENE("TREVOR_2_SNIPE_MAIN")
					START_AUDIO_SCENE("TREVOR_2_HELI_CRASH")
					
					REQUEST_PTFX_ASSET()
					REQUEST_COLLISION_FOR_MODEL(MAVERICK)
					REQUEST_AMBIENT_AUDIO_BANK("TREVOR_2_CHOPPER_EXPLOSION_TAIL")

					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

					START_CUTSCENE()
					REQUEST_PTFX_ASSET()
					REQUEST_IPL("airfield")
					bJumpTime = FALSE
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Shoot down heli cutscene") 
					iprogress++
				
				ELSE
				
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
						ENDIF
						
						IF 	DOES_ENTITY_EXIST(psMaverickEnemies[0].PedIndex)
						AND	NOT IS_ENTITY_DEAD(psMaverickEnemies[0].PedIndex)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Pilot", psMaverickEnemies[0].PedIndex, G_M_Y_LOST_01)
						ELSE
							//call all the variations this ped should have in the cutscene
						ENDIF
						
						IF 	DOES_ENTITY_EXIST(psMaverickEnemies[1].PedIndex)
						AND	NOT IS_ENTITY_DEAD(psMaverickEnemies[1].PedIndex)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lost_sniper", psMaverickEnemies[1].PedIndex, G_M_Y_LOST_01)
						ELSE
							//call all the variations this ped should have in the cutscene
						ENDIF
						
					ENDIF
				
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
				
				IF DOES_ENTITY_EXIST(vsEnemyBikes[0].VehicleIndex)
					DELETE_VEHICLE(vsEnemyBikes[0].VehicleIndex)
				ENDIF
				
				IF bJumpTime = FALSE
					IF GET_CLOCK_HOURS() < 4
						ADD_TO_CLOCK_TIME(2,00,0)
					ELSE
						IF GET_CLOCK_HOURS() < 5
							ADD_TO_CLOCK_TIME(0,15,0)
						ENDIF
					ENDIF
					SET_ENTITY_COORDS(psRon.PedIndex,<<1681.34, 3297.01, 41.03>>)
					SET_ENTITY_HEADING(psRon.PedIndex,-22.71)
					
					bJumpTime = TRUE
				ENDIF
				
				//clear the area
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF

				iprogress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lost_heli")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Lost_heli.")
				#ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
					IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
					
						SET_ENTITY_HEALTH(mission_veh[MV_MAVERICK].veh, 50)
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[MV_MAVERICK].veh, 50.0)
						SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_MAVERICK].veh)
						
						SET_VEHICLE_OUT_OF_CONTROL(mission_veh[MV_MAVERICK].veh, FALSE, TRUE)
						
						//start playing back a recording of it crashing here?
					ENDIF
				ENDIF
			
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Trevor.")
				#ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_QUAD].veh)
					IF NOT IS_ENTITY_DEAD(mission_veh[MV_TREVOR_QUAD].veh)
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(mission_veh[MV_TREVOR_QUAD].veh, TRUE, FALSE)
					ENDIF
				ENDIF
				
				IF bQuadPresent = TRUE
					IF ( bCutsceneSkipped = FALSE )
					OR WAS_CUTSCENE_SKIPPED()
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
							IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_QUAD].veh)
								IF NOT IS_ENTITY_DEAD(mission_veh[MV_TREVOR_QUAD].veh)
									SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mission_veh[MV_TREVOR_QUAD].veh, VS_DRIVER)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF ( bCutsceneSkipped = FALSE )
					OR WAS_CUTSCENE_SKIPPED()
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 4000, 237.4839, FALSE)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
					ENDIF
				
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_CAMERA() returned true.")
				#ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				
			ENDIF
		
			IF HAS_CUTSCENE_FINISHED()
			
				IF DOES_ENTITY_EXIST(mission_veh[MV_MAVERICK].veh)
					IF NOT IS_ENTITY_DEAD(mission_veh[MV_MAVERICK].veh)
					
						SET_ENTITY_HEALTH(mission_veh[MV_MAVERICK].veh, 50)
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[MV_MAVERICK].veh, 50.0)
						SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_MAVERICK].veh)
						
						SET_VEHICLE_OUT_OF_CONTROL(mission_veh[MV_MAVERICK].veh, FALSE, TRUE)
						
						REQUEST_COLLISION_FOR_MODEL(MAVERICK)
						REQUEST_COLLISION_AT_COORD(<<1693.43, 3317.96, 40.46>>)
						SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MAVERICK].veh,TRUE)
						
						//start playing back a recording of it crashing here?
					ENDIF
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				//cleanup animation dictionaries
				REMOVE_ANIM_SET("move_ped_crouched")
				REMOVE_ANIM_SET("move_ped_crouched_strafing")
				
				REMOVE_ANIM_DICT("guard_reactions")
				REMOVE_ANIM_DICT("misstrevor2ig_2")
				REMOVE_ANIM_DICT("misstrevor2ig_3")
				REMOVE_ANIM_DICT("misstrevor2ig_4")
				REMOVE_ANIM_DICT("misstrevor2ig_4b")
				REMOVE_ANIM_DICT("misstrevor2ig_5")
				REMOVE_ANIM_DICT("misstrevor2ig_5b_p2")
				REMOVE_ANIM_DICT("misstrevor2ig_5c")
				REMOVE_ANIM_DICT("misstrevor2ig_6")
				REMOVE_ANIM_DICT("missbigscore2big_2")
				REMOVE_ANIM_DICT("misstrevor2ig_7")
				REMOVE_ANIM_DICT("misstrevor2mcs_3_b")
				REQUEST_ANIM_DICT("misstrevor2ron_basic_moves")
				REMOVE_ANIM_DICT("reaction@male_stand@big_variations@d")
				REMOVE_ANIM_DICT("missexile2reactions_to_gun_fire@standing@idle_a")
	
				IF DOES_ENTITY_EXIST(LostSniperWeapon)
					DELETE_OBJECT(LostSniperWeapon)
				ENDIF
				
				IF DOES_ENTITY_EXIST(TrevorWeapon)
					DELETE_OBJECT(TrevorWeapon)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_SNIPERRIFLE))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE))
				
				SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(),0.5,TRUE)
				
				REPLAY_STOP_EVENT()
				
				ADVANCE_MISSION_STAGE()
				STOP_AUDIO_SCENE("TREVOR_2_HELI_CRASH")
			ELSE
				
				//if cutscene is skipped, fade out and remain cutscene on faded out screen
				IF ( bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						bCutsceneSkipped = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mocap cutscene was skipped by the player.")
						#ENDIF
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
		
	ENDSWITCH


ENDPROC

//APPROACH FROM THE LHS
FUNC BOOL SHOOTOUT_HAS_PLAYER_APROACHED_FROM_LHS()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1683.482178,3290.230713,39.360733>>, <<1673.402832,3287.756104,41.902275>>, 1.000000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1669.300049,3300.260986,39.344719>>, <<1673.402832,3287.756104,41.902275>>, 1.000000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1704.615967,3310.631592,39.422333>>, <<1674.182739,3295.227051,41.939190>>, 9.750000) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//APPROACH FROM THE RHS
FUNC BOOL SHOOTOUT_HAS_PLAYER_APROACHED_FROM_RHS()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1686.064697,3279.782227,39.369827>>, <<1676.806274,3273.052979,42.083561>>, 2.500000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1685.854370,3278.801025,39.313618>>, <<1693.877930,3265.621338,41.916233>>, 2.500000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1699.373657,3278.888184,39.639809>>, <<1687.910767,3273.757324,42.419823>>, 13.500000) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//APPROACH FROM THE BUILDING
FUNC BOOL SHOOTOUT_HAS_PLAYER_APROACHED_FROM_BUILDING()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1682.020386,3285.587891,39.608017>>, <<1683.000122,3284.415039,42.612045>>, 1.000000)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//APPROACH MAIN SHOOTOUT AREA
FUNC BOOL SHOOTOUT_HAS_PLAYER_APROACHED_MAIN_SHOOTOUT_AREA()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1704.915161,3269.346680,39.652409>>, <<1701.384888,3317.346924,42.648117>>, 34.250000)
	AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1771.344238,3333.743164,38.850021>>, <<1686.254517,3269.190186,50.581741>>, 44.250000)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(enemy[28].ped)
	AND NOT IS_PED_INJURED(enemy[9].ped)
	AND NOT IS_PED_INJURED(enemy[19].ped)
	AND NOT IS_PED_INJURED(enemy[23].ped)
	AND NOT IS_PED_INJURED(enemy[29].ped)
	AND NOT IS_PED_INJURED(enemy[30].ped)
	AND NOT IS_PED_INJURED(driver[0].ped)
		IF IS_ENTITY_ON_SCREEN(enemy[28].ped)
		AND IS_ENTITY_ON_SCREEN(enemy[9].ped)
		AND IS_ENTITY_ON_SCREEN(enemy[19].ped)
		AND IS_ENTITY_ON_SCREEN(enemy[23].ped)
		AND IS_ENTITY_ON_SCREEN(enemy[29].ped)
		AND IS_ENTITY_ON_SCREEN(enemy[30].ped)
		AND IS_ENTITY_ON_SCREEN(driver[0].ped)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_THERE_ENEMIES_IN_THIS_AREA(VECTOR vLoc,FLOAT fSize,MODEL_NAMES enemyModel)
	
	PED_INDEX tPED
	
	GET_CLOSEST_PED(vLoc,fSize,FALSE,TRUE,tPED)
	IF DOES_ENTITY_EXIST(tPED)
		IF GET_ENTITY_MODEL(tPED) = enemyModel
			RETURN TRUE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_AI_PLAYER_BEHAVIOR()
		
		
		
		IF bRonTasks
			IF HAS_LABEL_BEEN_TRIGGERED("TR_pl5p")
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl04")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF MANAGE_MY_TIMER(iStaggerRonShootoutLines,GET_RANDOM_INT_IN_RANGE(15000,20000))
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl04", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("TR_pl04",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF NOT IS_PED_INJURED(psRon.pedIndex)
				SWITCH iRonAI
					CASE 0
						//Set up Ron
						IF NOT IS_PED_INJURED(psRon.pedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psRon.pedIndex,TRUE)
							SET_ENTITY_HEALTH(psRon.pedIndex,2000)
							SET_PED_SUFFERS_CRITICAL_HITS(psRon.pedIndex,FALSE)
							SET_ENTITY_INVINCIBLE(psRon.pedIndex,FALSE)
							SET_PED_ACCURACY(psRon.pedIndex,100)
							GIVE_WEAPON_TO_PED(psRon.pedIndex,WEAPONTYPE_PISTOL,200,TRUE)
							SET_CURRENT_PED_WEAPON(psRon.pedIndex,WEAPONTYPE_PISTOL,TRUE)
							SET_PED_CAN_RAGDOLL(psRon.pedIndex,FALSE)
							SET_PED_PATH_CAN_USE_CLIMBOVERS(psRon.pedIndex,TRUE)
							REQUEST_WAYPOINT_RECORDING("Trevor2RonPlane")
							iRonTimer = GET_GAME_TIMER()
							SET_LABEL_AS_TRIGGERED("TR2_RGOTA",TRUE)
							iRonAI ++
						ENDIF
						
					BREAK
					
					CASE 1
						//SNEAK INTO BUILDING
						IF GET_IS_WAYPOINT_RECORDING_LOADED("Trevor2RonPlane")
							IF NOT IS_PED_INJURED(psRon.pedIndex)
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),psRon.pedIndex,<<36,36,20>>)
								OR MANAGE_MY_TIMER(iRonTimer,35000)
									SET_PED_RELATIONSHIP_GROUP_HASH(psRon.pedIndex ,rel_group_buddies)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,rel_group_buddies)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,RELGROUPHASH_PLAYER)
									SET_PED_AS_ENEMY(psRon.pedIndex , FALSE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psRon.pedIndex,TRUE)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(psRon.pedIndex)
									IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
										OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
											//TASK_FOLLOW_WAYPOINT_RECORDING(NULL,"Trevor2RonPlane",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1714.5170, 3316.7190, 40.2176>>,2.0,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,294.2834)
											TASK_CLIMB(NULL,TRUE)
											TASK_ENTER_VEHICLE(NULL,mission_veh[1].veh)
										CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
										TASK_PERFORM_SEQUENCE(psRon.pedIndex,TASK_SEQUENCE)
										CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
									ENDIF
									
									SET_LABEL_AS_TRIGGERED("TR2_RGOTA",FALSE)
									
									iRonAI ++
								ENDIF
							ENDIF
						ENDIF
							
					BREAK
				
					CASE 2
						
						//modify playback speed
						IF NOT IS_PED_INJURED(psRon.pedIndex)
							IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(psRon.pedIndex)
								WAYPOINT_PLAYBACK_OVERRIDE_SPEED(psRon.pedIndex,1.8)
								iRonAI ++
							ENDIF
							iRonAI ++
						ENDIF
					BREAK
					
					CASE 3
						IF NOT IS_PED_INJURED(psRon.pedIndex)
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),psRon.pedIndex,<<1.5,1.5,1.5>>)
								CLEAR_PED_TASKS(psRon.pedIndex)
								IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
									IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
										OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1714.5170, 3316.7190, 40.2176>>,2.0,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,294.2834)
											TASK_CLIMB(NULL,TRUE)
											//TASK_FOLLOW_WAYPOINT_RECORDING(NULL,"Trevor2RonPlane",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
											TASK_ENTER_VEHICLE(NULL,mission_veh[1].veh)
										CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
										TASK_PERFORM_SEQUENCE(psRon.pedIndex,TASK_SEQUENCE)
										CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
									ENDIF
								ENDIF
								iRonAI ++
							ENDIF
						ENDIF
					
					BREAK

				ENDSWITCH
			ENDIF
		ENDIF
		

ENDPROC

INT iVanDelay

PROC MANAGE_SHOOT_OUT()

	//guy on tower separate AI
	IF NOT IS_PED_INJURED(enemy[7].ped)
		IF IS_ENTITY_AT_COORD(enemy[7].ped,<<1705.8997, 3290.3420, 44.3995>>,<<2,2,2>>)
			IF (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), enemy[7].ped) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), enemy[7].ped))
			OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),enemy[7].ped) < 15
				IF enemy[7].bCharge = FALSE
					SET_PED_SPHERE_DEFENSIVE_AREA(enemy[7].ped,GET_ENTITY_COORDS(PLAYER_PED_ID()),20,TRUE)
					SET_PED_COMBAT_MOVEMENT(enemy[7].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(enemy[7].ped,CA_CAN_CHARGE,TRUE)
					SET_PED_COMBAT_RANGE(enemy[7].ped,CR_NEAR)
					SET_PED_COMBAT_ATTRIBUTES(enemy[7].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					PRINTSTRING("enemy 7 charging")PRINTNL()
					enemy[7].bCharge = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_LABEL_BEEN_TRIGGERED("TRIGGER SHOOTOUT BIKER")
		IF NOT IS_PED_INJURED(driver[0].ped)
			IF NOT IS_PED_IN_ANY_VEHICLE(driver[0].ped)
				IF GET_SCRIPT_TASK_STATUS(psRon.pedIndex,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
					TASK_COMBAT_PED(driver[0].ped,PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(driver[0].ped)
		REQUEST_VEHICLE_RECORDING(20,"RCSJC")
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(20,"RCSJC")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER SHOOTOUT BIKER")
				IF DOES_ENTITY_EXIST(mission_veh[0].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						IF IS_ENTITY_ON_SCREEN(mission_veh[0].veh) AND NOT IS_ENTITY_OCCLUDED(mission_veh[0].veh)
						//OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1718.252930,3302.718262,39.222672>>, <<1674.364258,3274.434570,44.790176>>, 17.500000)
							IF NOT IS_PED_IN_VEHICLE(driver[0].ped,mission_veh[0].veh)
								//SET_PED_INTO_VEHICLE(driver[0].ped, mission_veh[0].veh)
							ENDIF
							START_PLAYBACK_RECORDED_VEHICLE_USING_AI(mission_veh[0].veh,20,"RCSJC",6,DRIVINGMODE_AVOIDCARS)
							SET_PLAYBACK_SPEED(mission_veh[0].veh,0.7)
							GIVE_WEAPON_TO_PED(driver[0].ped,WEAPONTYPE_PISTOL,INFINITE_AMMO,TRUE,TRUE)
							SET_PED_ACCURACY(driver[0].ped,2)
							IF IS_PED_IN_ANY_VEHICLE(driver[0].ped)
								IF NOT IS_PED_INJURED(psRon.pedIndex)
									TASK_DRIVE_BY(driver[0].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,100,80,TRUE)
								ENDIF
							ELSE
								TASK_COMBAT_PED(driver[0].ped,PLAYER_PED_ID())
							ENDIF
							SET_LABEL_AS_TRIGGERED("TRIGGER SHOOTOUT BIKER",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SWITCH iShootOut
	
		//CHECK APPROACH DIRECTION
		CASE 0
			iShootOut ++
		BREAK
		
		//SHOOTOUT_HAS_PLAYER_APROACHED_FROM_LHS()
		CASE 1
			//BEHIND BUS
			IF NOT DOES_ENTITY_EXIST(enemy[8].ped)
				CREATE_ENEMY_AT_COORD(8,<<1693.4961, 3291.8953, 40.1465>>, 301.5915,FALSE,TRUE)
				ADD_DEADPOOL_TRIGGER(enemy[8].ped, TRV2_KILLS)
			ENDIF
		
			//TOWER GUY - UP STAIRS
			IF NOT DOES_ENTITY_EXIST(enemy[7].ped)
				CREATE_ENEMY_AT_COORD(7,<<1698.9329, 3292.5127, 47.9264>>, 128.2312,FALSE,TRUE)
				ADD_DEADPOOL_TRIGGER(enemy[7].ped, TRV2_KILLS)
			ENDIF
			
			//BEHIND INITIAL CARAVAN OR IN HANGAR
			IF NOT DOES_ENTITY_EXIST(enemy[9].ped)
				IF WOULD_ENTITY_BE_OCCLUDED(G_M_Y_LOST_01,<<1721.59, 3296.98, 41.22>>)
					CREATE_ENEMY_AT_COORD(9,<<1721.59, 3296.98, 41.22>>,-103.08,FALSE,TRUE)
					ADD_DEADPOOL_TRIGGER(enemy[9].ped, TRV2_KILLS)
				ELSE
					CREATE_ENEMY_AT_COORD(9,<<1743.40, 3301.30, 41.22>>, 173.27,FALSE,TRUE)
					ADD_DEADPOOL_TRIGGER(enemy[9].ped, TRV2_KILLS)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(enemy[7].ped)
			AND DOES_ENTITY_EXIST(enemy[8].ped)
			AND DOES_ENTITY_EXIST(enemy[9].ped)
				iShootOut ++
			ENDIF
		BREAK
		
		CASE 2
			//BEHIND VAN ABOUT TO GO UP STAIRS ON APPROACH
			IF NOT IS_PED_INJURED(enemy[7].ped)
				OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 1705.9340, 3290.4158, 44.3994 >>,0.75)
					TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
				TASK_PERFORM_SEQUENCE(enemy[7].ped,TASK_SEQUENCE)
				CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
			ENDIF
			
			//INSIDE BUILDING
			IF NOT IS_PED_INJURED(enemy[8].ped)
				OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
					TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
				TASK_PERFORM_SEQUENCE(enemy[8].ped,TASK_SEQUENCE)
				CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
				SET_PED_COMBAT_MOVEMENT(enemy[8].ped,CM_DEFENSIVE)
				SET_PED_CHANCE_OF_FIRING_BLANKS(enemy[8].ped,30,60)
				SET_PED_SPHERE_DEFENSIVE_AREA(enemy[8].ped,<<1701.3646, 3297.6819, 40.1478>>,30,TRUE)
			ENDIF
			
			//BEHIND INITIAL CARAVAN OR IN HANGAR
			IF NOT IS_PED_INJURED(enemy[9].ped)
				OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 1726.99, 3294.21, 41.22 >>,0.75)
					TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
				TASK_PERFORM_SEQUENCE(enemy[9].ped,TASK_SEQUENCE)
				CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
				
				SET_PED_COMBAT_ATTRIBUTES(enemy[9].ped,CA_CAN_CHARGE,TRUE)
			ENDIF
			
			iTimeInCombat = GET_GAME_TIMER()
			iShootOut ++
		BREAK
		
		
		CASE 3
			IF DOES_ENTITY_EXIST(mission_veh[26].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[26].veh)
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[26].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[26].veh,FALSE)
					ENDIF
					
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<1776.7924, 3331.9929, 40.2483>>) < 20
						PRINTSTRING("THIS CONDITION -1")PRINTNL()
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1720.459595,3292.012207,39.717606>>, <<1719.003174,3269.001709,45.155197>>, 5.750000)
						PRINTSTRING("THIS CONDITION -2")PRINTNL()
					ENDIF
					
					IF IS_PED_INJURED(enemy[8].ped) AND IS_PED_INJURED(enemy[7].ped) AND IS_PED_INJURED(driver[0].ped)
						PRINTSTRING("THIS CONDITION -3")PRINTNL()
					ENDIF
					
					IF IS_PED_INJURED(enemy[19].ped) AND IS_PED_INJURED(driver[0].ped) AND IS_PED_INJURED(enemy[8].ped)
						PRINTSTRING("THIS CONDITION -4")PRINTNL()
					ENDIF
					
					IF IS_ENTITY_ON_SCREEN(mission_veh[26].veh) AND NOT IS_ENTITY_OCCLUDED(mission_veh[26].veh)
						PRINTSTRING("THIS CONDITION -5")PRINTNL()
					ENDIF
					
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mission_veh[26].veh)) < 20
						PRINTSTRING("THIS CONDITION -6")PRINTNL()
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGERED VAN EARLY")
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1785.576538,3325.364990,39.638405>>, <<1766.687134,3360.914795,48.246090>>, 22.250000)
						IF MANAGE_MY_TIMER(iTimeInCombat,22000)
							IF IS_VEHICLE_DRIVEABLE(mission_veh[26].veh)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<1776.7924, 3331.9929, 40.2483>>) < 20
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1720.459595,3292.012207,39.717606>>, <<1719.003174,3269.001709,45.155197>>, 5.750000)
								OR IS_PED_INJURED(enemy[8].ped) AND IS_PED_INJURED(enemy[7].ped) AND IS_PED_INJURED(driver[0].ped) AND IS_PED_INJURED(enemy[19].ped)
								OR IS_PED_INJURED(enemy[19].ped) AND IS_PED_INJURED(driver[0].ped) AND IS_PED_INJURED(enemy[8].ped) AND IS_PED_INJURED(enemy[28].ped)
								OR IS_ENTITY_ON_SCREEN(mission_veh[26].veh) AND NOT IS_ENTITY_OCCLUDED(mission_veh[26].veh)
								OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mission_veh[26].veh)) < 20
									REQUEST_VEHICLE_RECORDING(26,"RCSJC")
									IF HAS_VEHICLE_RECORDING_BEEN_LOADED(26,"RCSJC")
										IF NOT IS_PED_INJURED(driver[29].ped)
											IF DOES_ENTITY_EXIST(mission_veh[26].veh)
												IF IS_VEHICLE_DRIVEABLE(mission_veh[26].veh)
													iShootOut ++
												ENDIF
											ENDIF
										ELSE
											driver[29].bTriggerTask = FALSE	
											driver[30].bTriggerTask = FALSE	
											driver[31].bTriggerTask = FALSE	
											iVanDelay = GET_GAME_TIMER()
											iShootOut = 5
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_PED_INJURED(driver[29].ped)
								driver[29].bTriggerTask = FALSE	
								driver[30].bTriggerTask = FALSE	
								driver[31].bTriggerTask = FALSE	
								iVanDelay = GET_GAME_TIMER()
								iShootOut = 5
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_ON_SCREEN(mission_veh[26].veh) AND NOT IS_ENTITY_OCCLUDED(mission_veh[26].veh)
							driver[29].bTriggerTask = FALSE	
							driver[30].bTriggerTask = FALSE	
							driver[31].bTriggerTask = FALSE	
							iShootOut = 5
						ENDIF
					ENDIF
				ELSE
					iShootOut = 6
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 4
			IF IS_VEHICLE_DRIVEABLE(mission_veh[26].veh)
				IF NOT IS_PED_INJURED(driver[29].ped)
					IF NOT WOULD_ENTITY_BE_OCCLUDED(BURRITO,<<1753.92, 3294.63, 40.94>>,FALSE)
					OR MANAGE_MY_TIMER(iVanDelay,10000)
					OR IS_ENTITY_ON_SCREEN(mission_veh[26].veh) AND NOT IS_ENTITY_OCCLUDED(mission_veh[26].veh)
						UNPAUSE_PLAYBACK_RECORDED_VEHICLE(mission_veh[26].veh)
						driver[29].bTriggerTask = FALSE	
						driver[30].bTriggerTask = FALSE	
						driver[31].bTriggerTask = FALSE
						iVanDelay = GET_GAME_TIMER()
						iShootOut ++
					ENDIF
				ELSE
					driver[29].bTriggerTask = FALSE	
					driver[30].bTriggerTask = FALSE	
					driver[31].bTriggerTask = FALSE
					iVanDelay = GET_GAME_TIMER()
					iShootOut ++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 5

			IF driver[29].bTriggerTask = FALSE
				IF NOT IS_PED_INJURED(driver[29].ped)
					IF IS_PED_IN_ANY_VEHICLE(driver[29].ped)
						SET_PED_COMBAT_RANGE(driver[29].ped, CR_MEDIUM)
						SET_PED_COMBAT_MOVEMENT(driver[29].ped,CM_DEFENSIVE)
						EXIT_VEHICLE_TO_FIGHT(mission_veh[26].veh)
					ELSE
						IF NOT DOES_BLIP_EXIST(driver[29].blip)
							driver[29].bTriggerTask = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF driver[30].bTriggerTask = FALSE
				IF NOT IS_PED_INJURED(driver[30].ped)
					IF IS_PED_IN_ANY_VEHICLE(driver[30].ped)
						SET_PED_COMBAT_RANGE(driver[30].ped, CR_MEDIUM)
						SET_PED_COMBAT_MOVEMENT(driver[30].ped,CM_DEFENSIVE)
						EXIT_VEHICLE_TO_FIGHT(mission_veh[26].veh)
					ELSE
						IF NOT DOES_BLIP_EXIST(driver[30].blip)
							driver[30].bTriggerTask = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF driver[31].bTriggerTask = FALSE
				IF NOT IS_PED_INJURED(driver[31].ped)
					IF IS_PED_IN_ANY_VEHICLE(driver[31].ped)
						SET_PED_COMBAT_RANGE(driver[31].ped, CR_MEDIUM)
						SET_PED_COMBAT_MOVEMENT(driver[31].ped,CM_DEFENSIVE)
						EXIT_VEHICLE_TO_FIGHT(mission_veh[26].veh)
					ELSE
						IF NOT DOES_BLIP_EXIST(driver[31].blip)
							driver[31].bTriggerTask = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	

ENDPROC


PROC MANAGE_BIKER_BATTLE_DIALOGUE()

	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(),pedBattleEnemies)

	INT i
	
	FOR i = 0 TO 9
	
		IF DOES_ENTITY_EXIST(pedBattleEnemies[i])
			IF NOT IS_PED_INJURED(pedBattleEnemies[i])
				IF IS_PED_MODEL(pedBattleEnemies[i],G_M_Y_LOST_01)
					IF MANAGE_MY_TIMER(iAmbientDialogueTimer, 2500)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedBattleEnemies[i])) < 15
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBattleEnemies[i],"TR2_DLAA","LOST4",SPEECH_PARAMS_FORCE)
							iAmbientDialogueTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

INT iClearTheRunwayDialogue

PROC MANAGE_DIALOGUE_AND_GOD_TEXT()

	//PRINTSTRING("*****DangG")PRINTINT(DandG)PRINTNL()

	SWITCH DandG
		
		CASE 0
			 bDialogue = FALSE
			 DandG++
		BREAK
		
		CASE 1
		
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl5p", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("TR_pl5p",TRUE)
						iStaggerRonShootoutLines = GET_GAME_TIMER()
						DandG++
					ENDIF
				ENDIF
			ENDIF
		BREAK
			
		CASE 2
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					DandG++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			DandG++
			
		BREAK
		
		CASE 4
			IF iProgress > 4
				IF MANAGE_MY_TIMER(iDangGTimer, 4000)
					KILL_ANY_CONVERSATION()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						PRINT_GOD_TEXT("SJC_GETPLNG1")
						IF DOES_BLIP_EXIST(psRon.blipIndex)
							REMOVE_BLIP(psRon.blipIndex)
						ENDIF
						MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[1].veh,<<0,0,0>>)
						iDangGTimer = GET_GAME_TIMER()
						DandG++
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_INJURED(enemy[23].ped)
				AND IS_PED_INJURED(driver[0].ped)
				AND IS_PED_INJURED(driver[29].ped)
				AND IS_PED_INJURED(driver[30].ped)
				AND IS_PED_INJURED(driver[31].ped)
				AND IS_PED_INJURED(enemy[19].ped)
				AND IS_PED_INJURED(enemy[23].ped)
				AND IS_PED_INJURED(enemy[28].ped)
				AND IS_PED_INJURED(enemy[29].ped)
				AND IS_PED_INJURED(enemy[30].ped)
				AND IS_PED_INJURED(enemy[8].ped)
				AND IS_PED_INJURED(enemy[7].ped)
				
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1716.055176,3312.888428,39.223660>>, <<1743.667480,3321.382813,44.702236>>, 23.750000)
							
						IF NOT HAS_LABEL_BEEN_TRIGGERED("KILL CURRENT CONVO")
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION()
								SET_LABEL_AS_TRIGGERED("KILL CURRENT CONVO",TRUE)
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CLR")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CLR", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("TR2_CLR",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CLRb")
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_CLRb", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR2_CLRb",TRUE)
												iClearTheRunwayDialogue = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF HAS_LABEL_BEEN_TRIGGERED("TR2_CLRb")
										IF MANAGE_MY_TIMER(iClearTheRunwayDialogue,15000)
											SET_LABEL_AS_TRIGGERED("KILL CURRENT CONVO",TRUE)
											SET_LABEL_AS_TRIGGERED("TR2_CLR",TRUE)
											SET_LABEL_AS_TRIGGERED("TR2_CLRb",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
	
						ENDIF
						
					ELSE
						IF HAS_LABEL_BEEN_TRIGGERED("TR2_CLRb")
							IF MANAGE_MY_TIMER(iClearTheRunwayDialogue,15000)
								SET_LABEL_AS_TRIGGERED("KILL CURRENT CONVO",TRUE)
								SET_LABEL_AS_TRIGGERED("TR2_CLR",TRUE)
								SET_LABEL_AS_TRIGGERED("TR2_CLRb",TRUE)
							ENDIF
						ENDIF
						IF NOT HAS_LABEL_BEEN_TRIGGERED("GET_TIMER")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								iDangGTimer = GET_GAME_TIMER()
								SET_LABEL_AS_TRIGGERED("GET_TIMER",TRUE)
							ENDIF
						ELSE
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_SHT2")
										IF MANAGE_MY_TIMER(iDangGTimer,GET_RANDOM_INT_IN_RANGE(12000,19000))
											IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_SHT2", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("TR2_SHT2",TRUE)
												iDangGTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ELSE
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl6a1")
											IF MANAGE_MY_TIMER(iDangGTimer,GET_RANDOM_INT_IN_RANGE(12000,19000))
												IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl6a", CONV_PRIORITY_MEDIUM)
													SET_LABEL_AS_TRIGGERED("TR_pl6a1",TRUE)
													iDangGTimer = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ELSE
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_SHT")
												IF MANAGE_MY_TIMER(iDangGTimer,GET_RANDOM_INT_IN_RANGE(17000,19000))
												AND IS_PED_SHOOTING(PLAYER_PED_ID())
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_SHT", CONV_PRIORITY_MEDIUM)
														SET_LABEL_AS_TRIGGERED("TR2_SHT",TRUE)
														iDangGTimer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ELSE
												IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl6a2")
													IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl6a", CONV_PRIORITY_MEDIUM)
														SET_LABEL_AS_TRIGGERED("TR_pl6a2",TRUE)
														iDangGTimer = GET_GAME_TIMER()
													ENDIF
												ELSE
													IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_SHT3")
														IF MANAGE_MY_TIMER(iDangGTimer,GET_RANDOM_INT_IN_RANGE(15000,16000))
															IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_SHT3", CONV_PRIORITY_MEDIUM)
																SET_LABEL_AS_TRIGGERED("TR2_SHT3",TRUE)
																iDangGTimer = GET_GAME_TIMER()
															ENDIF
														ENDIF
													ELSE
														IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_SHT4")
															IF MANAGE_MY_TIMER(iDangGTimer,GET_RANDOM_INT_IN_RANGE(12000,15000))
																IF NOT IS_PED_INJURED(psRon.pedIndex)
																	IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
																		IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
																			IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_SHT4", CONV_PRIORITY_MEDIUM)
																				SET_LABEL_AS_TRIGGERED("TR2_SHT4",TRUE)
																				iDangGTimer = GET_GAME_TIMER()
																			ENDIF
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ELSE
															IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl6a3")
																IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl6a", CONV_PRIORITY_MEDIUM)
																	SET_LABEL_AS_TRIGGERED("TR_pl6a3",TRUE)
																	iDangGTimer = GET_GAME_TIMER()
																ENDIF
															ELSE
																IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_SHT5")
																	IF MANAGE_MY_TIMER(iDangGTimer,GET_RANDOM_INT_IN_RANGE(12000,15000))
																		IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_SHT5", CONV_PRIORITY_MEDIUM)
																			SET_LABEL_AS_TRIGGERED("TR2_SHT5",TRUE)
																			iDangGTimer = GET_GAME_TIMER()
																		ENDIF
																	ENDIF
																ELSE
																	IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl6a")
																		IF MANAGE_MY_TIMER(iDangGTimer,GET_RANDOM_INT_IN_RANGE(12000,15000))
																			IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_pl6a", CONV_PRIORITY_MEDIUM)
																				SET_LABEL_AS_TRIGGERED("TR_pl6a",TRUE)
																				iDangGTimer = GET_GAME_TIMER()
																			ENDIF
																		ENDIF
																	ENDIF
																	IF HAS_LABEL_BEEN_TRIGGERED("TR_pl6a")
																		IF MANAGE_MY_TIMER(iDangGTimer, GET_RANDOM_INT_IN_RANGE(12000,15000))
																			SET_LABEL_AS_TRIGGERED("TR_pl6a",FALSE)
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 5
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[1].veh),GET_ENTITY_COORDS(PLAYER_PED_ID())) > 3
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_OVER")
								IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR2_OVER", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("TR2_OVER",TRUE)
									iDangGTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("TR2_OVER")
				IF MANAGE_MY_TIMER(iDangGTimer, 12000)
					SET_LABEL_AS_TRIGGERED("TR2_OVER",FALSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
		
		BREAK
		
		
	ENDSWITCH

ENDPROC
INT iPlaneDamageLineTimer

PROC MANAGE_PLAYER_DAMAGE_PLANE()
	IF bCommentOnPlaneBeingDamage = FALSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[1].veh,PLAYER_PED_ID(),TRUE)
				IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl6c", CONV_PRIORITY_MEDIUM )
					bCommentOnPlaneBeingDamage = TRUE
					iPlaneDamageLineTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF MANAGE_MY_TIMER(iPlaneDamageLineTimer,GET_RANDOM_INT_IN_RANGE(12000,15000))
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mission_veh[1].veh)
			bCommentOnPlaneBeingDamage = FALSE
		ENDIF
	ENDIF
ENDPROC

BOOL bGetTimerForLerp = FALSE
INT iLerpTimer

FUNC BOOL INTERP_ENTITY_BETWEEN_COORDS_OVER_TIME(ENTITY_INDEX entity, VECTOR vStartPos,VECTOR vEndPos, INT iTime, FLOAT fDiv )
	
	IF NOT bGetTimerForLerp
		iLerpTimer = GET_GAME_TIMER()
	ELSE
		IF DOES_ENTITY_EXIST(entity)
			IF MANAGE_MY_TIMER(iLerpTimer,iTime)
				RETURN TRUE
			ELSE
				SET_ENTITY_COORDS(entity,LERP_VECTOR(vStartPos,vEndPos,iLerpTimer/fDiv))
				PRINTSTRING("ENTITY POS") PRINTVECTOR(GET_ENTITY_COORDS(entity))PRINTNL()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

VECTOR vAnimPos

PROC CUTSCENE_JUMP_ONTO_PLANE()

	SETTIMERA(0)
	bcutsceneplaying = TRUE
	
	IF icutsceneprog > 0
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		PRINTINT(TIMERB())PRINTNL()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TREV2")
		
		#IF IS_DEBUG_BUILD // allows j/p skip to work even in cutscene
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				SETTIMERB(20000)
				CONTROL_FADE_OUT(500)
				icutsceneprog = 3
			ENDIF
		#ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_bp01")
			IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_bp01", CONV_PRIORITY_MEDIUM )
				SET_LABEL_AS_TRIGGERED("TR_bp01",TRUE)
			ENDIF
		ENDIF
		
		
		
		SWITCH icutsceneprog
			
			CASE 0
				REQUEST_ANIM_DICT("misstrevor2ig_9a")
				IF HAS_ANIM_DICT_LOADED("misstrevor2ig_9a")
					IF NOT DOES_CAM_EXIST(cutscene_cam)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							
							REQUEST_VEHICLE_RECORDING(1,"RCSJC")
							REQUEST_VEHICLE_RECORDING(2,"RCSJC")
							REQUEST_VEHICLE_RECORDING(7,"RCSJC")
							REQUEST_VEHICLE_RECORDING(9,"RCSJC")
							REQUEST_VEHICLE_RECORDING(13,"RCSJC")
							
							//NEW
							REQUEST_VEHICLE_RECORDING(23,"RCSJC")
							REQUEST_VEHICLE_RECORDING(24,"RCSJC")
							REQUEST_VEHICLE_RECORDING(25,"RCSJC")
							
							REQUEST_MODEL(G_M_Y_LOST_01)
							REQUEST_MODEL(HEXER)
							REQUEST_MODEL(HAULER)
							REQUEST_MODEL(TANKER)
							REQUEST_MODEL(GBURRITO)
							REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
							REQUEST_ANIM_DICT("misstrevor2ig_9b")
							REQUEST_ANIM_DICT("misstrevor2hangar_death")
							
							//brunfailchecks = FALSE
							SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
							
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtCutscene)
							
							IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
							OR NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
							OR NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
								GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,120,TRUE,TRUE)
							ELSE
								IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
									IF wtCutscene != WEAPONTYPE_ASSAULTRIFLE
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,TRUE)
									ENDIF
								ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
									IF wtCutscene != WEAPONTYPE_CARBINERIFLE
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE,TRUE)
									ENDIF
								ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
									IF wtCutscene != WEAPONTYPE_ADVANCEDRIFLE
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE,TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							KILL_ANY_CONVERSATION()
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForcedAim,TRUE)
							bJumpTime = FALSE
							bGetTimerForLerp = FALSE
				
							vAnimPos = <<0,0,0.0>>
							
							IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
								DETACH_ENTITY(PLAYER_PED_ID())
							ENDIF
							
							CLEAR_AREA(<< 1735.6924, 3292.5146, 40.1576 >>,100,TRUE)
							CLEAR_AREA_OF_VEHICLES(<< 1735.6924, 3292.5146, 40.1576 >>,100,FALSE,FALSE,TRUE,TRUE)
							
							VEHICLE_INDEX vehPlayersLastVehicle
							IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
								vehPlayersLastVehicle = GET_PLAYERS_LAST_VEHICLE()
								DELETE_VEHICLE(vehPlayersLastVehicle)
							ENDIF
							
							IF DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
								DELETE_VEHICLE(mission_veh[MV_RON_QUAD].veh)
								SET_MODEL_AS_NO_LONGER_NEEDED(BLAZER)
							ENDIF
							
							IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_PLANE].veh)
								DELETE_VEHICLE(mission_veh[MV_TREVOR_PLANE].veh)
								SET_MODEL_AS_NO_LONGER_NEEDED(BLAZER)
							ENDIF
							
							GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtCutscene)
							
							IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
							AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
							AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
								GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,120,TRUE,TRUE)
							ELSE
								IF wtCutscene != WEAPONTYPE_ASSAULTRIFLE
								AND wtCutscene != WEAPONTYPE_ADVANCEDRIFLE
								AND wtCutscene != WEAPONTYPE_CARBINERIFLE
									IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,TRUE)
									ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE,TRUE)
									ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE,TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							
							IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
								sceneIDPlayerJumpOnToWing = CREATE_SYNCHRONIZED_SCENE(vAnimPos, <<0.0, 0.0, 0.0>>)
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneIDPlayerJumpOnToWing, "misstrevor2ig_9a", "get_on_plane_ped", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDPlayerJumpOnToWing, mission_veh[1].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[1].veh, "chassis"))
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
							
							//Bug fix 1866464 cleared everything before the camera cuts now on same frame.
							cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",FALSE) 
							PLAY_CAM_ANIM(cutscene_cam,  "get_on_plane_cam","misstrevor2ig_9a", GET_WORLD_POSITION_OF_ENTITY_BONE(mission_veh[1].veh,0), <<0,0,GET_ENTITY_HEADING(mission_veh[1].veh)>>)
							SET_CAM_ACTIVE(cutscene_cam,TRUE)
							RENDER_SCRIPT_CAMS(TRUE,FALSE)

							SETTIMERB(0)
							
//							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
							
							icutsceneprog++
						ELSE
							icutsceneprog++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_PED_INJURED(psRon.pedIndex)
					TASK_LOOK_AT_ENTITY(psRon.pedIndex, PLAYER_PED_ID(),6000,SLF_WHILE_NOT_IN_FOV)
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[MV_RON_QUAD].veh)
					DELETE_VEHICLE(mission_veh[MV_RON_QUAD].veh)
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_QUAD].veh)
					DELETE_VEHICLE(mission_veh[MV_TREVOR_QUAD].veh)
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[12].veh)
					DELETE_VEHICLE(mission_veh[12].veh)
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[0].veh)
					DELETE_VEHICLE(mission_veh[0].veh)
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[8].veh)
					DELETE_VEHICLE(mission_veh[8].veh)
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[26].veh)
					DELETE_VEHICLE(mission_veh[26].veh)
				ENDIF
				
				icutsceneprog++
			BREAK
			
			CASE 2
				IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) = 1
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						PRINTSTRING("OFFSET:")PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(mission_veh[1].veh,GET_ENTITY_COORDS(PLAYER_PED_ID())))PRINTNL()
					ENDIF
					ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), mission_veh[1].veh, 0, << 3.84078, 0.831592, 0.753682 >>, <<0,0,0>>, TRUE)
					TASK_AIM_GUN_SCRIPTED(PLAYER_PED_ID(),SCRIPTED_GUN_TASK_PLANE_WING)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					icutsceneprog++
				ENDIF
			BREAK
			
			CASE 3
				DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
				IF DOES_CAM_EXIST(cutscene_cam)
					SET_CAM_ACTIVE(cutscene_cam,FALSE)
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					RENDER_SCRIPT_CAMS(FALSE,TRUE,1500)
				ENDIF
				
//				REPLAY_STOP_EVENT()
				
				REPLAY_RECORD_BACK_FOR_TIME(15.0, 4.0)
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					SET_VEHICLE_ENGINE_ON(mission_veh[1].veh,TRUE,FALSE)
				ENDIF

				SET_WIDESCREEN_BORDERS(FALSE,0)
				
				CONTROL_FADE_IN(500)
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				
				IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[8].veh)
					DELETE_VEHICLE(mission_veh[8].veh)
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[26].veh)
					DELETE_VEHICLE(mission_veh[26].veh)
				ENDIF
				
				CLEAR_PRINTS()
				PRINTNL()
				icutsceneprog = 0
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				KILL_ANY_CONVERSATION()
				brunfailchecks = TRUE
				bcutsceneplaying = FALSE
			BREAK

		ENDSWITCH
		
	ENDWHILE

ENDPROC

PROC MANAGE_PLANE_DAMAGE()
	IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
		IF NOT IS_ENTITY_ON_FIRE(mission_veh[1].veh)
			SET_ENTITY_PROOFS(mission_veh[1].veh,TRUE,TRUE,TRUE,TRUE,TRUE)
		ELSE
			SET_ENTITY_PROOFS(mission_veh[1].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(PED_STRUCT &array[])

	INT i
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		IF i = 14
			UPDATE_AI_PED_BLIP(array[i].ped, array[i].EnemyBlipData,-1,NULL,FALSE,FALSE,150)
		ELSE
			UPDATE_AI_PED_BLIP(array[i].ped, array[i].EnemyBlipData)
		ENDIF	
	ENDFOR

ENDPROC

//PURPOSE: Checks how many enemies are left alive
FUNC INT GET_NUMBER_OF_ENEMY_PEDS_STILL_ALIVE()
	INT i, iThisCount
	REPEAT COUNT_OF(enemy) i
		IF NOT IS_PED_INJURED(enemy[i].ped)
			iThisCount++
		ENDIF
	ENDREPEAT
	
	RETURN iThisCount
ENDFUNC

//PURPOSE: Checks how many enemies are left alive
FUNC INT GET_NUMBER_OF_DRIVER_PEDS_STILL_ALIVE()
	INT i, iThisCount
	REPEAT COUNT_OF(driver) i
		IF NOT IS_PED_INJURED(driver[i].ped)
			iThisCount++
		ENDIF
	ENDREPEAT
	
	RETURN iThisCount
ENDFUNC

//PURPOSE: Checks how many bikers are left alive
FUNC BOOL ARE_ONLY_X_BIKER_PEDS_LEFT(INT iNum)
	
	IF GET_NUMBER_OF_ENEMY_PEDS_STILL_ALIVE() + GET_NUMBER_OF_DRIVER_PEDS_STILL_ALIVE() < iNum + 1
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

INT iSwitchDefensiveAreaOFF = 12000
INT iTimeTillCombatRon = 240000

PROC MANAGE_ENEMY_AGGRESSION()
	
	PRINTSTRING("iTimeInCombat: ")PRINTINT(GET_GAME_TIMER() -iTimeInCombat)PRINTNL()
	
	IF enemy[7].bChangeTarget = TRUE
	OR enemy[8].bChangeTarget = TRUE
	OR enemy[9].bChangeTarget = TRUE
	OR driver[0].bChangeTarget = TRUE
	OR enemy[19].bChangeTarget = TRUE
	OR enemy[28].bChangeTarget = TRUE
	OR enemy[29].bChangeTarget = TRUE
	OR enemy[30].bChangeTarget = TRUE
	OR driver[31].bChangeTarget = TRUE
	OR driver[32].bChangeTarget = TRUE
	OR driver[33].bChangeTarget = TRUE
//		IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_P15")
//			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					IF CREATE_CONVERSATION(sSpeech,"T2AUD", "T1M1_P15", CONV_PRIORITY_MEDIUM)
//						SET_LABEL_AS_TRIGGERED("T1M1_P15",TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(enemy[7].ped)
		IF MANAGE_MY_TIMER(iTimeInCombat,30000)
		OR MANAGE_MY_TIMER(iTimeInCombat,iSwitchDefensiveAreaOFF)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1745.110107,3315.022705,39.223515>>, <<1717.157837,3308.499512,46.473660>>, 30.750000)
		OR (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), enemy[7].ped) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), enemy[7].ped))
			IF enemy[7].bCharge = FALSE
				//SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[7].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 25.0 ,FALSE)
				SET_PED_COMBAT_MOVEMENT(enemy[7].ped,CM_WILLADVANCE)
				SET_PED_COMBAT_ATTRIBUTES(enemy[7].ped,CA_CAN_CHARGE,TRUE)
				SET_PED_COMBAT_RANGE(enemy[7].ped,CR_NEAR)
				PRINTSTRING("enemy 7 charging")PRINTNL()
				enemy[7].bCharge = TRUE
			ENDIF
		ENDIF
		IF iShootOut > 4
			IF MANAGE_MY_TIMER(iTimeInCombat,80000)
				IF enemy[7].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_SPHERE_DEFENSIVE_AREA(enemy[7].ped,GET_ENTITY_COORDS(mission_veh[1].veh),20,TRUE)
								SET_PED_COMBAT_MOVEMENT(enemy[7].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(enemy[7].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_COMBAT_RANGE(enemy[7].ped,CR_NEAR)
								TASK_COMBAT_PED(enemy[7].ped,psRon.pedIndex)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[7].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 25.0 ,FALSE)
								PRINTSTRING("enemy 7 changed Target")PRINTNL()
								enemy[7].bCharge = TRUE
								enemy[7].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(enemy[8].ped)
		IF MANAGE_MY_TIMER(iTimeInCombat,25000)
		OR MANAGE_MY_TIMER(iTimeInCombat,iSwitchDefensiveAreaOFF)
		OR GET_DISTANCE_BETWEEN_ENTITIES(enemy[8].ped,PLAYER_PED_ID(),FALSE) < 13
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1745.110107,3315.022705,39.223515>>, <<1717.157837,3308.499512,46.473660>>, 30.750000)
			IF enemy[8].bCharge = FALSE
				SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[8].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 15.0,FALSE)
				SET_PED_COMBAT_MOVEMENT(enemy[8].ped,CM_WILLADVANCE)
				SET_PED_COMBAT_ATTRIBUTES(enemy[8].ped,CA_CAN_CHARGE,TRUE)
				SET_PED_COMBAT_ATTRIBUTES(enemy[8].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
				SET_PED_COMBAT_RANGE(enemy[8].ped,CR_NEAR)
				PRINTSTRING("enemy 8 charging")PRINTNL()
				enemy[8].bCharge = TRUE
			ENDIF
		ENDIF
		IF iShootOut > 4
			IF MANAGE_MY_TIMER(iTimeInCombat,iTimeTillCombatRon)
				IF enemy[8].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_SPHERE_DEFENSIVE_AREA(enemy[8].ped,GET_ENTITY_COORDS(mission_veh[1].veh),10,TRUE)
								SET_PED_COMBAT_MOVEMENT(enemy[8].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(enemy[8].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[8].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 10.0,FALSE)
								TASK_COMBAT_PED(enemy[8].ped,psRon.pedIndex)
								PRINTSTRING("enemy 8 changed Target")PRINTNL()
								enemy[8].bCharge = TRUE
								enemy[8].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(driver[0].ped)
		IF MANAGE_MY_TIMER(iTimeInCombat,36000)
		OR MANAGE_MY_TIMER(iTimeInCombat,iSwitchDefensiveAreaOFF)
		OR GET_DISTANCE_BETWEEN_ENTITIES(enemy[8].ped,PLAYER_PED_ID(),FALSE) < 15
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1745.110107,3315.022705,39.223515>>, <<1717.157837,3308.499512,46.473660>>, 30.750000)
			IF driver[0].bCharge = FALSE
				SET_PED_SPHERE_DEFENSIVE_AREA(driver[0].ped,<<1728.38, 3285.12, 40.07>>,20)
				SET_PED_COMBAT_MOVEMENT(driver[0].ped,CM_WILLADVANCE)
				SET_PED_COMBAT_ATTRIBUTES(driver[0].ped,CA_CAN_CHARGE,TRUE)
				//SET_PED_COMBAT_ATTRIBUTES(driver[0].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
				SET_PED_COMBAT_RANGE(driver[0].ped,CR_NEAR)
				PRINTSTRING("driver 0 charging")PRINTNL()
				driver[0].bCharge = TRUE
			ENDIF
		ENDIF
		IF iShootOut > 4
			IF MANAGE_MY_TIMER(iTimeInCombat,iTimeTillCombatRon)
				IF driver[0].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_SPHERE_DEFENSIVE_AREA(driver[0].ped,GET_ENTITY_COORDS(mission_veh[1].veh),15,TRUE)
								SET_PED_COMBAT_MOVEMENT(driver[0].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(driver[0].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(driver[0].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 10.0,FALSE)
								TASK_COMBAT_PED(driver[0].ped,psRon.pedIndex)
								PRINTSTRING("driver[0] changed Target")PRINTNL()
								driver[0].bCharge = TRUE
								driver[0].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(enemy[19].ped)
		IF (MANAGE_MY_TIMER(iTimeInCombat,14000) AND IS_PED_INJURED(enemy[8].ped) AND IS_PED_INJURED(enemy[7].ped))
		OR MANAGE_MY_TIMER(iTimeInCombat,iSwitchDefensiveAreaOFF)
		OR GET_DISTANCE_BETWEEN_ENTITIES(enemy[19].ped,PLAYER_PED_ID(),FALSE) < 12
		OR (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), enemy[19].ped) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), enemy[19].ped))
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1745.110107,3315.022705,39.223515>>, <<1717.157837,3308.499512,46.473660>>, 30.750000)
		OR MANAGE_MY_TIMER(iTimeInCombat,60000)
			IF enemy[19].bCharge = FALSE
				//SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[19].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 30.0, TRUE)
				SET_PED_COMBAT_MOVEMENT(enemy[19].ped,CM_WILLADVANCE)
				SET_PED_COMBAT_ATTRIBUTES(enemy[19].ped,CA_CAN_CHARGE,TRUE)
				SET_PED_COMBAT_ATTRIBUTES(enemy[19].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
				SET_PED_COMBAT_RANGE(enemy[19].ped,CR_NEAR)
				PRINTSTRING("enemy 19 charging")PRINTNL()
				enemy[19].bCharge = TRUE
			ENDIF
		ENDIF
		IF iShootOut > 4
			IF MANAGE_MY_TIMER(iTimeInCombat,iTimeTillCombatRon)
				IF enemy[19].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_COMBAT_MOVEMENT(enemy[19].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(enemy[19].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[19].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 15.0,FALSE)
								TASK_COMBAT_PED(enemy[19].ped,psRon.pedIndex)
								PRINTSTRING("enemy 19 changed Target")PRINTNL()
								enemy[19].bCharge = TRUE
								enemy[19].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iShootOut > 4
		IF NOT IS_PED_INJURED(enemy[28].ped)
			IF GET_DISTANCE_BETWEEN_ENTITIES(enemy[19].ped,PLAYER_PED_ID(),FALSE) < 23
			OR MANAGE_MY_TIMER(iTimeInCombat,iSwitchDefensiveAreaOFF)
			OR IS_PED_IN_COVER(PLAYER_PED_ID()) AND MANAGE_MY_TIMER(iTimeInCombat,45000) AND GET_DISTANCE_BETWEEN_ENTITIES(enemy[19].ped,PLAYER_PED_ID(),FALSE) < 30
			OR MANAGE_MY_TIMER(iTimeInCombat,60000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1745.110107,3315.022705,39.223515>>, <<1717.157837,3308.499512,46.473660>>, 30.750000)
				IF enemy[28].bCharge = FALSE
					//SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[28].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 30.0,FALSE)
					SET_PED_COMBAT_MOVEMENT(enemy[28].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(enemy[28].ped,CA_CAN_CHARGE,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(enemy[28].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					SET_PED_COMBAT_RANGE(enemy[28].ped,CR_NEAR)
					PRINTSTRING("enemy 28 charging")PRINTNL()
					enemy[28].bCharge = TRUE
				ENDIF
			ENDIF
			IF MANAGE_MY_TIMER(iTimeInCombat,iTimeTillCombatRon)
				IF enemy[28].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_SPHERE_DEFENSIVE_AREA(enemy[28].ped,GET_ENTITY_COORDS(mission_veh[1].veh),15,TRUE)
								SET_PED_COMBAT_MOVEMENT(enemy[28].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(enemy[28].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[28].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 10.0,FALSE)
								TASK_COMBAT_PED(enemy[28].ped,psRon.pedIndex)
								PRINTSTRING("enemy 28 changed Target")PRINTNL()
								enemy[28].bCharge = TRUE
								enemy[28].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		IF NOT IS_PED_INJURED(enemy[29].ped)
			IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),enemy[29].ped) < 18
			OR MANAGE_MY_TIMER(iTimeInCombat,iSwitchDefensiveAreaOFF)
			OR MANAGE_MY_TIMER(iVanDelay,40000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1745.110107,3315.022705,39.223515>>, <<1717.157837,3308.499512,46.473660>>, 30.750000)
				IF enemy[29].bCharge = FALSE
					//SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[29].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 30.0,FALSE)
					SET_PED_COMBAT_MOVEMENT(enemy[29].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(enemy[29].ped,CA_CAN_CHARGE,TRUE)
					//SET_PED_COMBAT_RANGE(enemy[29].ped,CR_NEAR)
					//SET_PED_COMBAT_ATTRIBUTES(enemy[29].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					PRINTSTRING("enemy 29 charging")PRINTNL()
					enemy[29].bCharge = TRUE
				ENDIF
			ENDIF
			IF MANAGE_MY_TIMER(iTimeInCombat,iTimeTillCombatRon)
				IF enemy[29].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_SPHERE_DEFENSIVE_AREA(enemy[29].ped,GET_ENTITY_COORDS(mission_veh[1].veh),15,TRUE)
								SET_PED_COMBAT_MOVEMENT(enemy[29].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(enemy[29].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[29].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 10.0,FALSE)
								SET_PED_COMBAT_ATTRIBUTES(enemy[29].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
								TASK_COMBAT_PED(enemy[29].ped,psRon.pedIndex)
								PRINTSTRING("enemy 29 changed Target")PRINTNL()
								enemy[29].bCharge = TRUE
								enemy[29].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF NOT IS_PED_INJURED(enemy[30].ped)
			IF (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),enemy[30].ped) < 28 AND MANAGE_MY_TIMER(iVanDelay,15000))
			OR MANAGE_MY_TIMER(iTimeInCombat,iSwitchDefensiveAreaOFF)
			OR MANAGE_MY_TIMER(iVanDelay,40000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1745.110107,3315.022705,39.223515>>, <<1717.157837,3308.499512,46.473660>>, 30.750000)
				IF enemy[30].bCharge = FALSE
					SET_PED_SPHERE_DEFENSIVE_AREA(enemy[30].ped,<<1751.04, 3289.58, 41.11>>,20.0,TRUE)
					SET_PED_COMBAT_MOVEMENT(enemy[30].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(enemy[30].ped,CA_CAN_CHARGE,TRUE)
					//SET_PED_COMBAT_RANGE(enemy[30].ped,CR_NEAR)
					//SET_PED_COMBAT_ATTRIBUTES(enemy[30].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					PRINTSTRING("enemy 30 charging")PRINTNL()
					enemy[30].bCharge = TRUE
				ENDIF
			ENDIF
			IF MANAGE_MY_TIMER(iTimeInCombat,iTimeTillCombatRon)
				IF enemy[30].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_SPHERE_DEFENSIVE_AREA(enemy[30].ped,GET_ENTITY_COORDS(mission_veh[1].veh),15,TRUE)
								SET_PED_COMBAT_MOVEMENT(enemy[30].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(enemy[30].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(enemy[30].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 15.0,FALSE)
								TASK_COMBAT_PED(enemy[30].ped,psRon.pedIndex)
								PRINTSTRING("enemy 30 changed Target")PRINTNL()
								enemy[30].bCharge = TRUE
								enemy[30].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF iShootOut > 4
		IF NOT IS_PED_INJURED(driver[29].ped)
			IF MANAGE_MY_TIMER(iVanDelay,12000)
				IF driver[29].bCharge = FALSE
					SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(driver[29].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 30.0,FALSE)
					SET_PED_COMBAT_MOVEMENT(driver[29].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(driver[29].ped,CA_CAN_CHARGE,TRUE)
					//SET_PED_COMBAT_RANGE(driver[29].ped,CR_NEAR)
					//SET_PED_COMBAT_ATTRIBUTES(driver[29].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					PRINTSTRING("driver 29 charging")PRINTNL()
					driver[29].bCharge = TRUE
				ENDIF
			ENDIF
			IF MANAGE_MY_TIMER(iVanDelay,iTimeTillCombatRon)
				IF driver[29].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_COMBAT_MOVEMENT(driver[29].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(driver[29].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(driver[29].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 20.0,FALSE)
								TASK_COMBAT_PED(driver[29].ped,psRon.pedIndex)
								PRINTSTRING("driver 29 changed Target")PRINTNL()
								driver[29].bCharge = TRUE
								driver[29].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(driver[30].ped)
			IF MANAGE_MY_TIMER(iVanDelay,14000)
				IF driver[30].bCharge = FALSE
					//SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(driver[30].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 30.0,FALSE)
					SET_PED_COMBAT_MOVEMENT(driver[30].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(driver[30].ped,CA_CAN_CHARGE,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(driver[30].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					SET_PED_COMBAT_RANGE(driver[30].ped,CR_NEAR)
					PRINTSTRING("driver 38 charging")PRINTNL()
					driver[30].bCharge = TRUE
				ENDIF
			ENDIF
			IF MANAGE_MY_TIMER(iVanDelay,iTimeTillCombatRon)
				IF driver[30].bChangeTarget = FALSE
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								SET_PED_SPHERE_DEFENSIVE_AREA(driver[30].ped,GET_ENTITY_COORDS(mission_veh[1].veh),20,TRUE)
								SET_PED_COMBAT_MOVEMENT(driver[30].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(driver[30].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(driver[30].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 15.0,FALSE)
								TASK_COMBAT_PED(driver[30].ped,psRon.pedIndex)
								PRINTSTRING("driver 30 changed Target")PRINTNL()
								driver[30].bCharge = TRUE
								driver[30].bChangeTarget = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(driver[31].ped)
			IF MANAGE_MY_TIMER(iVanDelay,16000)
				IF driver[31].bCharge = FALSE
					//SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(driver[30].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 30.0,FALSE)
					SET_PED_COMBAT_MOVEMENT(driver[31].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(driver[31].ped,CA_CAN_CHARGE,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(driver[31].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					SET_PED_COMBAT_RANGE(driver[31].ped,CR_NEAR)
					PRINTSTRING("driver 31 charging")PRINTNL()
					driver[31].bCharge = TRUE
				ENDIF
				IF MANAGE_MY_TIMER(iVanDelay,iTimeTillCombatRon)
					IF driver[31].bChangeTarget = FALSE
						IF NOT IS_PED_INJURED(psRon.pedIndex)
							IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
								IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
									SET_PED_SPHERE_DEFENSIVE_AREA(driver[31].ped,GET_ENTITY_COORDS(mission_veh[1].veh),20,TRUE)
									SET_PED_COMBAT_MOVEMENT(driver[31].ped,CM_WILLADVANCE)
									SET_PED_COMBAT_ATTRIBUTES(driver[31].ped,CA_CAN_CHARGE,TRUE)
									SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(driver[31].ped, psRon.pedIndex, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 15.0,FALSE)
									TASK_COMBAT_PED(driver[31].ped,psRon.pedIndex)
									PRINTSTRING("driver 31 changed Target")PRINTNL()
									driver[31].bCharge = TRUE
									driver[31].bChangeTarget = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ARE_ONLY_X_BIKER_PEDS_LEFT(2)
			INT iDriver
			REPEAT COUNT_OF(driver) iDriver
				IF driver[iDriver].bGoAggressive = FALSE
					IF NOT IS_PED_INJURED(driver[iDriver].ped)
						SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(driver[iDriver].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 30.0,FALSE)
						SET_PED_COMBAT_MOVEMENT(driver[iDriver].ped,CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(driver[iDriver].ped,CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(driver[iDriver].ped,CA_AGGRESSIVE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(driver[iDriver].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
						SET_PED_COMBAT_RANGE(driver[iDriver].ped,CR_NEAR)
						TASK_COMBAT_PED(driver[iDriver].ped,PLAYER_PED_ID())
						PRINTSTRING("driver ")PRINTINT(iDriver)PRINTSTRING("aggressive")PRINTNL()
						driver[iDriver].bCharge = TRUE
						driver[iDriver].bChangeTarget = TRUE
						driver[iDriver].bGoAggressive = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			
			INT iEnemy 
			REPEAT COUNT_OF(enemy) iEnemy
				IF enemy[iDriver].bGoAggressive = FALSE
					IF NOT IS_PED_INJURED(Enemy[iEnemy].ped)
						SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(Enemy[iEnemy].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 30.0,FALSE)
						SET_PED_COMBAT_MOVEMENT(Enemy[iEnemy].ped,CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(Enemy[iEnemy].ped,CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(Enemy[iEnemy].ped,CA_AGGRESSIVE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(enemy[iEnemy].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
						SET_PED_COMBAT_RANGE(enemy[iEnemy].ped,CR_NEAR)
						TASK_COMBAT_PED(Enemy[iEnemy].ped,PLAYER_PED_ID())
						PRINTSTRING("enemy ")PRINTINT(iEnemy)PRINTSTRING("aggressive")PRINTNL()
						enemy[iEnemy].bCharge = TRUE
						enemy[iEnemy].bChangeTarget = TRUE
						enemy[iEnemy].bGoAggressive = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF

	ENDIF
	
ENDPROC





INT iRonPlaneAudio
INT iPlaneEngineStartTimer
FLOAT fBladeSpeed

PROC MANAGE_RON_PLANE_AUDIO()
	
	SWITCH iRonPlaneAudio
		
		CASE 0
			IF REQUEST_AMBIENT_AUDIO_BANK("TREVOR_2_PLANE_START")
				fBladeSpeed = 0
				iRonPlaneAudio ++
			ENDIF
		BREAK
		
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh) 
				IF NOT IS_PED_INJURED(psRon.PedIndex)
					IF IS_PED_IN_VEHICLE(psRon.PedIndex,mission_veh[MV_RON_PLANE].veh)
						PLAY_SOUND_FROM_ENTITY(-1,"TREVOR_2_PLANE_START",mission_veh[MV_RON_PLANE].veh,"TREVOR_2_SOUNDS")
						iPlaneEngineStartTimer = GET_GAME_TIMER()
						iRonPlaneAudio ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
		CASE 2
			IF MANAGE_MY_TIMER(iPlaneEngineStartTimer,1600)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh) 
					IF fBladeSpeed > 0.0
						fBladeSpeed = fBladeSpeed - 0.001
						PRINTSTRING("WINDING DOWN")PRINTNL()
						SET_HELI_BLADES_SPEED(mission_veh[MV_RON_PLANE].veh,fBladeSpeed)
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh) 
					IF fBladeSpeed < 0.6
						fBladeSpeed = fBladeSpeed + 0.001
						PRINTSTRING("WINDING UP")PRINTNL()
						SET_HELI_BLADES_SPEED(mission_veh[MV_RON_PLANE].veh,fBladeSpeed)
					ENDIF
				ENDIF
			ENDIF
				
			IF fBladeSpeed <= 0.0
				IF MANAGE_MY_TIMER(iPlaneEngineStartTimer,GET_RANDOM_INT_IN_RANGE(4000,12000))
					IF NOT ARE_ONLY_X_BIKER_PEDS_LEFT(1)
						iRonPlaneAudio --
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_VAN_TRIGGER()
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGERED VAN EARLY")
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1785.576538,3325.364990,39.638405>>, <<1766.687134,3360.914795,48.246090>>, 22.250000)
			IF DOES_ENTITY_EXIST(mission_veh[26].veh)
				IF IS_ENTITY_ON_SCREEN(mission_veh[26].veh)
					IF NOT IS_PED_INJURED(driver[29].ped)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[29].ped,FALSE)
						OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
						TASK_LEAVE_ANY_VEHICLE(NULL,0)
						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 1778.06, 3328.92, 40.29 >>,20)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
						TASK_PERFORM_SEQUENCE(driver[29].ped,TASK_SEQUENCE)
						CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
					ENDIF
					
					IF NOT IS_PED_INJURED(driver[30].ped)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[30].ped,FALSE)
						OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
						TASK_LEAVE_ANY_VEHICLE(NULL,0)
						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 1778.06, 3328.92, 40.29 >>,20)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
						TASK_PERFORM_SEQUENCE(driver[30].ped,TASK_SEQUENCE)
						CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
					ENDIF
					
					IF NOT IS_PED_INJURED(driver[31].ped)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[31].ped,FALSE)
						OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
						TASK_LEAVE_ANY_VEHICLE(NULL,0)
						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 1778.06, 3328.92, 40.29 >>,20)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
						TASK_PERFORM_SEQUENCE(driver[31].ped,TASK_SEQUENCE)
						CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
					ENDIF
					SET_LABEL_AS_TRIGGERED("TRIGGERED VAN EARLY",TRUE)
					PRINTSTRING("MANAGE_VAN_TRIGGER() - 4")PRINTNL()
				ELSE
					PRINTSTRING("MANAGE_VAN_TRIGGER() - 3")PRINTNL()
				ENDIF
			ELSE
				PRINTSTRING("MANAGE_VAN_TRIGGER() - 2")PRINTNL()
			ENDIF
		ELSE
			PRINTSTRING("MANAGE_VAN_TRIGGER() - 1")PRINTNL()
		ENDIF
	ENDIF
ENDPROC

//INT iVehicleBlowUpTimer 
BOOL bStartAggressionManagement
INT iSpecialAbilityHelpTimer
//BOOL bBreakLight[2]
//OBJECT_INDEX objBreakLight[2]
		
PROC STEAL_PLANE()

	IF iProgress > 0
		IF HAS_LABEL_BEEN_TRIGGERED("SJC_SPECIAL_H")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_COMBAT_ROLL")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1699.596558,3267.314941,38.879612>>, <<1765.330688,3283.361816,46.513935>>, 26.500000)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("SJC_COMBAT_ROLL")
						SET_LABEL_AS_TRIGGERED("SJC_COMBAT_ROLL",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	MANAGE_VAN_TRIGGER()
	MANAGE_HELI_EXPLOSION()
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(enemy)
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(driver)
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
	
	IF NOT DOES_ENTITY_EXIST(mission_veh[MV_RON_PLANE].veh)
		SETUP_MISSION_REQUIREMENT(REQ_RON_PLANE,<< 1733.3765, 3320.3232, 41.7312 >>, 183.4859)
	ELSE
	
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(mission_veh[MV_TREVOR_PLANE].veh)
		SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<< 1573.97, 3221.91, 41.08 >>  ,106.07)
	ELSE
		IF NOT HAS_LABEL_BEEN_TRIGGERED("VEHICLE BLOCKED")
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_TREVOR_PLANE].veh,FALSE)	
				SET_LABEL_AS_TRIGGERED("VEHICLE BLOCKED",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	
	IF iProgress > 3
		IF bStartAggressionManagement
			MANAGE_ENEMY_AGGRESSION()
		ENDIF
	ENDIF
	
	IF iProgress > 0
		IF DOES_BLIP_EXIST(dest_blip)
			REMOVE_BLIP(dest_blip)
		ENDIF
		MANAGE_BIKER_BATTLE_DIALOGUE()
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_2_SHOOTOUT")
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SNIPE_MAIN")
			STOP_AUDIO_SCENE("TREVOR_2_SNIPE_MAIN")
		ELSE
			START_AUDIO_SCENE("TREVOR_2_SHOOTOUT")
			SET_LABEL_AS_TRIGGERED("TREVOR_2_SHOOTOUT",TRUE)
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_SPECIAL_H")
		IF iProgress > 0
			IF MANAGE_MY_TIMER(iSpecialAbilityHelpTimer,5000)
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				PRINT_HELP("CMN_TABIL_RET")
			ELSE
				PRINT_HELP("CMN_TABIL_RET_KM")
			ENDIF
				SET_LABEL_AS_TRIGGERED("SJC_SPECIAL_H",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF bRonTasks
		MANAGE_AI_PLAYER_BEHAVIOR()
	ENDIF
	
	IF iProgress > 2
		MANAGE_SHOOT_OUT()
		MANAGE_DIALOGUE_AND_GOD_TEXT()
	ENDIF
	
	IF iProgress > 4
		MANAGE_PLAYER_DAMAGE_PLANE()
	ENDIF
	
	IF iProgress < 5
		MANAGE_PLANE_DAMAGE()
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(mission_veh[6].veh)
		REQUEST_MODEL(GBURRITO)
		IF HAS_MODEL_LOADED(GBURRITO)
			CREATE_ENEMY_VEHICLE(6,GBURRITO, << 1745.71, 3275.38, 40.94 >>  , 133.23 ,G_M_Y_LOST_01)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[6].veh, TRUE)
		ENDIF
	ENDIF
	
	IF iProgress > 1
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
				START_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,2,"RCSJC")
				PRINTLN("@@@@@@@@@@@ SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK 2 @@@@@@@@@@")
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(mission_veh[1].veh, <<0,0,0.13>>)
				PAUSE_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh)
			ENDIF
		ENDIF
	ENDIF
	//SMASH ON RETRY
//	IF NOT bBreakLight[0]
//		IF NOT DOES_ENTITY_EXIST(osPropWallLights[0].ObjectIndex)
//			IF NOT DOES_ENTITY_EXIST(objBreakLight[0])
//				objBreakLight[0] = GET_CLOSEST_OBJECT_OF_TYPE(<<1700.87, 3295.02, 45.95>>,1.0,PROP_WALL_LIGHT_02A)
//			ELSE
//				SET_ENTITY_LIGHTS(objBreakLight[0],FALSE)
//				DAMAGE_OBJECT_FRAGMENT_CHILD(objBreakLight[0],0)
//				bBreakLight[0] = TRUE
//			ENDIF
//		ELSE
//			SET_ENTITY_LIGHTS(osPropWallLights[0].ObjectIndex,FALSE)
//			DAMAGE_OBJECT_FRAGMENT_CHILD(osPropWallLights[0].ObjectIndex,0)
//			bBreakLight[0] = TRUE
//		ENDIF
//	ENDIF
//	
//	IF NOT bBreakLight[1]
//		IF NOT DOES_ENTITY_EXIST(osPropWallLights[1].ObjectIndex)
//			IF NOT DOES_ENTITY_EXIST(objBreakLight[1])
//				objBreakLight[1] = GET_CLOSEST_OBJECT_OF_TYPE(<<1700.87, 3295.02, 45.95>>,1.0,PROP_WALL_LIGHT_02A)
//			ELSE
//				SET_ENTITY_LIGHTS(objBreakLight[1],FALSE)
//				DAMAGE_OBJECT_FRAGMENT_CHILD(objBreakLight[1],0)
//				bBreakLight[1] = TRUE
//			ENDIF
//		ELSE
//			SET_ENTITY_LIGHTS(osPropWallLights[1].ObjectIndex,FALSE)
//			DAMAGE_OBJECT_FRAGMENT_CHILD(osPropWallLights[1].ObjectIndex,0)
//			bBreakLight[1] = TRUE
//		ENDIF
//	ENDIF

	SWITCH iProgress
	
		CASE 0

			IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
				SET_ENTITY_COORDS(mission_veh[2].veh,<< 1573.9843, 3221.8896, 39.4199 >>)
				SET_ENTITY_HEADING(mission_veh[2].veh,106.0713)
				FREEZE_ENTITY_POSITION(mission_veh[2].veh,TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[2].veh)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_QUAD].veh)
				SET_VEHICLE_LIGHTS(mission_veh[MV_TREVOR_QUAD].veh,NO_VEHICLE_LIGHT_OVERRIDE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				SET_ENTITY_HEALTH(mission_veh[1].veh,2000)
				SET_ENTITY_PROOFS(mission_veh[1].veh,FALSE,FALSE,FALSE,TRUE,TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[12].veh)
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[12].veh)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[5].veh)
				SET_VEHICLE_STRONG(mission_veh[5].veh,TRUE)
				SET_ENTITY_PROOFS(mission_veh[5].veh,TRUE,TRUE,TRUE,TRUE,TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[6].veh)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[6].veh,FALSE)
			ENDIF
			
			IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(crate[1])
				SET_ENTITY_HEALTH(crate[1],120)
			ENDIF
			
			IF DOES_ENTITY_EXIST(enemy[32].ped)
				IF NOT IS_PED_INJURED(enemy[32].ped)
					DELETE_PED(enemy[32].ped)
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
			
			IF DOES_ENTITY_EXIST(psRon.pedIndex)
				IF NOT IS_PED_INJURED(psRon.pedIndex)
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						SET_ENTITY_HEALTH(psRon.pedIndex,1000)
						SET_PED_SUFFERS_CRITICAL_HITS(psRon.pedIndex,FALSE)
						SET_ENTITY_INVINCIBLE(psRon.pedIndex,FALSE)
						RESET_PED_MOVEMENT_CLIPSET(psRon.pedIndex)
						RESET_PED_STRAFE_CLIPSET(psRon.pedIndex)
						SET_PED_STEALTH_MOVEMENT(psRon.pedIndex,TRUE)
						SET_ENTITY_LOAD_COLLISION_FLAG(psRon.pedIndex,TRUE)
						
						OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1681.2122, 3300.5271, 40.0814>>,1.3)
						CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
						TASK_PERFORM_SEQUENCE(psRon.pedIndex,TASK_SEQUENCE)
						CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
					ENDIF
				ENDIF
			ENDIF
			//IN HANGER
			ADD_NAVMESH_BLOCKING_OBJECT(<<1732.060,3318.049,40.612>>,<<20.000,20.000,4.100>>,17.307)
			
			//IN FENCED AREA
			ADD_NAVMESH_BLOCKING_OBJECT(<<1712.584,3304.912,41.960>>,<<13.000,13.600,3.700>>,15.578)

			ADD_COVER_BLOCKING_AREA(<<1719.117676 +0.75,3285.014404  +0.75,41.284805  +0.75>>,<<1719.117676 -0.75,3285.014404 -0.75,41.284805 -0.75>>,TRUE,TRUE,TRUE)
			
			
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			CONTROL_FADE_IN(500)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Steal the plane")
			PRINT_GOD_TEXT("SJC_ENEM2G")
			
			bRonTasks = FALSE
			bStartAggressionManagement = FALSE
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE,ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS),WEAPON_COMPONENT_FLASH)
			REQUEST_PTFX_ASSET()
			REMOVE_ANIM_DICT("misstrevor2mcs_3_b")
	
			ADD_MODEL_REQUEST_TO_ARRAY(G_M_Y_LOST_01)
			ADD_MODEL_REQUEST_TO_ARRAY(HEXER)
			REMOVE_MODEL_FROM_ARRAY(MAVERICK)
			SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
			CLEAR_TRIGGERED_LABELS()

			iRonAI = 0
			iRonPlaneAudio = 0
			iShootOut = 0
			DandG = 0
			//bBreakLight[0] = FALSE
			//bBreakLight[1] = FALSE
			bSkipped = FALSE
			SETUP_PEDS_FOR_DIALOGUE()
			STREAMING_FIX()
			
			IF ARE_ALL_NAVMESH_REGIONS_LOADED()
				REMOVE_NAVMESH_REQUIRED_REGIONS()
			ENDIF
			
			REQUEST_VEHICLE_RECORDING(20,"RCSJC")
			REQUEST_VEHICLE_RECORDING(19,"RCSJC")
			REQUEST_VEHICLE_RECORDING(26,"RCSJC")
			REQUEST_VEHICLE_RECORDING(2,"RCSJC")

			IF DOES_ENTITY_EXIST(crate[1])
				SET_ENTITY_PROOFS(crate[1],FALSE,FALSE,FALSE,FALSE,FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(crate[2])
				SET_ENTITY_PROOFS(crate[2],FALSE,FALSE,FALSE,FALSE,FALSE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[1].veh,FALSE)
			ENDIF
			
			CLEAR_GPS_CUSTOM_ROUTE()
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				SET_BLIP_ROUTE(sLocatesData.LocationBlip,FALSE)
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(psRon.blipIndex)
				psRon.blipIndex = CREATE_BLIP_FOR_PED(psRon.pedIndex,FALSE)
			ENDIF
			
			IF STREAMVOL_IS_VALID(iStreamVol)
				STREAMVOL_DELETE(iStreamVol)
			ENDIF
			
			IF DOES_ENTITY_EXIST(osPropGasTanks[0].ObjectIndex)
			AND DOES_ENTITY_EXIST(mission_veh[MV_TANKER].veh)
				SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(osPropGasTanks[0].ObjectIndex,FALSE,rel_group_enemies)
				SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(mission_veh[MV_TANKER].veh,FALSE,rel_group_enemies)
			ENDIF

			CLEANUP_AI_PED_BLIP(psMaverickEnemies[0].EnemyBlipData)
			CLEANUP_AI_PED_BLIP(psMaverickEnemies[1].EnemyBlipData)
			
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			SET_PED_NON_CREATION_AREA(<<1556.037476,3205.534424,128.161575>> - <<250.000000,130.750000,93.750000>>, <<1556.037476,3205.534424,128.161575>> + <<250.000000,130.750000,93.750000>>)
			SET_PED_NON_CREATION_AREA(<<2034.216064,4763.091309,150.810684>> - <<250.000000,168.750000,129.750000>>, <<2034.216064,4763.091309,150.810684>> + <<250.000000,168.750000,129.750000>>)
			
			SET_PED_MODEL_IS_SUPPRESSED(A_M_M_HILLBILLY_01,TRUE)
			SET_PED_MODEL_IS_SUPPRESSED(A_M_M_HILLBILLY_02,TRUE)
			
			SET_WEATHER_TYPE_PERSIST("EXTRASUNNY")
			
			TRIGGER_MUSIC_EVENT("TRV2_GO_TO_RON")
			
			REQUEST_IPL("airfield")
			iSpecialAbilityHelpTimer = GET_GAME_TIMER()
			
			iprogress++
		BREAK
		
		CASE 1
			IF ARE_REQUESTED_MODELS_LOADED()
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(26,"RCSJC")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2,"RCSJC")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(19,"RCSJC")
					IF NOT DOES_ENTITY_EXIST(mission_veh[0].veh)
						CREATE_ENEMY_VEHICLE(0,HEXER,<< 1776.9507, 3284.2617, 40.4616 >>  , 184.9892  ,G_M_Y_LOST_01,TRUE,0)
						ADD_DEADPOOL_TRIGGER(driver[0].ped, TRV2_KILLS)
					ENDIF
					
					CREATE_ENEMIES(19,19,FALSE,FALSE,FALSE,TRUE,TRUE)
					CREATE_ENEMIES(28,30,FALSE,FALSE,FALSE,TRUE,TRUE)
					iStagetimer = GET_GAME_TIMER()
					
					IF NOT DOES_ENTITY_EXIST(enemy[19].ped)
						ADD_DEADPOOL_TRIGGER(enemy[19].ped, TRV2_KILLS)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(enemy[28].ped)
						ADD_DEADPOOL_TRIGGER(enemy[28].ped, TRV2_KILLS)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(enemy[29].ped)
						ADD_DEADPOOL_TRIGGER(enemy[29].ped, TRV2_KILLS)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(enemy[30].ped)
						ADD_DEADPOOL_TRIGGER(enemy[30].ped, TRV2_KILLS)
					ENDIF

					IF IS_VEHICLE_DRIVEABLE(mission_veh[8].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[8].veh,TRUE)
						IF GET_VEHICLE_DOOR_LOCK_STATUS(mission_veh[8].veh) != VEHICLELOCK_UNLOCKED
							SET_VEHICLE_DOORS_LOCKED(mission_veh[8].veh,VEHICLELOCK_UNLOCKED)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[7].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[7].veh,TRUE)
						IF GET_VEHICLE_DOOR_LOCK_STATUS(mission_veh[7].veh) != VEHICLELOCK_UNLOCKED
							SET_VEHICLE_DOORS_LOCKED(mission_veh[7].veh,VEHICLELOCK_UNLOCKED)
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(mission_veh[26].veh)
						CREATE_ENEMY_VEHICLE(26,GBURRITO,<<1776.7924, 3331.9929, 40.2483>>, 213.6601,G_M_Y_LOST_01,TRUE,29,2)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[26].veh, TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[26].veh,FALSE)
						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(26,"RCSJC")
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[26].veh)
								START_PLAYBACK_RECORDED_VEHICLE(mission_veh[26].veh,26,"RCSJC")
								PAUSE_PLAYBACK_RECORDED_VEHICLE(mission_veh[26].veh)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(driver[29].ped)
						ADD_DEADPOOL_TRIGGER(driver[29].ped, TRV2_KILLS)
					ENDIF
					IF NOT DOES_ENTITY_EXIST(driver[30].ped)
						ADD_DEADPOOL_TRIGGER(driver[30].ped, TRV2_KILLS)
					ENDIF
					IF NOT DOES_ENTITY_EXIST(driver[31].ped)
						ADD_DEADPOOL_TRIGGER(driver[31].ped, TRV2_KILLS)
					ENDIF
					REMOVE_MODEL_FROM_ARRAY(MAVERICK)
					bRonTasks = TRUE
					
					iprogress++
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
			REQUEST_VEHICLE_RECORDING(20,"RCSJC")
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(20,"RCSJC")
				iprogress++
			ENDIF
		BREAK
		
		CASE 3
			IF SHOOTOUT_HAS_PLAYER_APROACHED_MAIN_SHOOTOUT_AREA()
				IF ARE_REQUESTED_CARRECS_LOADED("RCSJC")
					IF NOT IS_PED_INJURED(psRon.pedIndex)	
						//CRATE BY AMMO
						IF NOT IS_PED_INJURED(enemy[19].ped)
						AND NOT IS_ENTITY_DEAD(enemy[19].ped)
							SET_PED_SPHERE_DEFENSIVE_AREA(enemy[19].ped,<<1710.2625, 3280.2556, 40.1216>>,20)
							SET_PED_COMBAT_MOVEMENT(enemy[19].ped,CM_DEFENSIVE)
							SET_PED_CHANCE_OF_FIRING_BLANKS(enemy[19].ped,0.3,0.6)
							OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_STAND_STILL(NULL,1000)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_PERFORM_SEQUENCE(enemy[19].ped,TASK_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
							
						ENDIF
						
						//CRATE BY MISSILES
						IF NOT IS_PED_INJURED(enemy[23].ped)
						AND NOT IS_ENTITY_DEAD(enemy[23].ped)
							SET_PED_SPHERE_DEFENSIVE_AREA(enemy[23].ped,<< 1730.30, 3282.01, 41.08 >>,2)
							SET_PED_COMBAT_MOVEMENT(enemy[23].ped,CM_DEFENSIVE)
							SET_PED_CHANCE_OF_FIRING_BLANKS(enemy[23].ped,0.3,0.6)
							OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_PERFORM_SEQUENCE(enemy[23].ped,TASK_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
						ENDIF
						
						//BY VAN
						IF NOT IS_PED_INJURED(enemy[28].ped)
						AND NOT IS_ENTITY_DEAD(enemy[28].ped)
							SET_PED_SPHERE_DEFENSIVE_AREA(enemy[28].ped,<< 1745.79, 3273.51, 41.16 >>,20.0)
							SET_PED_COMBAT_MOVEMENT(enemy[28].ped,CM_DEFENSIVE)
							SET_PED_CHANCE_OF_FIRING_BLANKS(enemy[28].ped,0.3,0.6)
							
							OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_STAND_STILL(NULL,2000)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_PERFORM_SEQUENCE(enemy[28].ped,TASK_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
						ENDIF
						
						//BY CARAVAN CRATES
						IF NOT IS_PED_INJURED(enemy[29].ped)
						AND NOT IS_ENTITY_DEAD(enemy[29].ped)
							SET_PED_SPHERE_DEFENSIVE_AREA(enemy[29].ped,<< 1751.19, 3289.60, 41.11 >>,15.0)
							SET_PED_COMBAT_MOVEMENT(enemy[29].ped,CM_DEFENSIVE)
							SET_PED_CHANCE_OF_FIRING_BLANKS(enemy[29].ped,0.3,0.6)
							OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_PERFORM_SEQUENCE(enemy[29].ped,TASK_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
						ENDIF
						
						//BEHIND CARAVAN
						IF NOT IS_PED_INJURED(enemy[30].ped)
						AND NOT IS_ENTITY_DEAD(enemy[30].ped)
							SET_PED_SPHERE_DEFENSIVE_AREA(enemy[30].ped,<<1729.1941, 3276.9832, 40.1145>>,15.0,TRUE)
							SET_PED_COMBAT_MOVEMENT(enemy[30].ped,CM_DEFENSIVE)
							SET_PED_CHANCE_OF_FIRING_BLANKS(enemy[30].ped,0.3,0.4)
							OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_PERFORM_SEQUENCE(enemy[30].ped,TASK_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
						ENDIF
						
						IF NOT IS_PED_INJURED(driver[0].ped)
							SET_PED_COMBAT_MOVEMENT(driver[0].ped,CM_DEFENSIVE)
							SET_PED_CHANCE_OF_FIRING_BLANKS(driver[0].ped,0.3,0.6)
							SET_PED_SPHERE_DEFENSIVE_AREA(driver[0].ped,<< 1724.34, 3274.78, 40.14 >> ,15,TRUE)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							SET_ENTITY_PROOFS(mission_veh[1].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
						ENDIF
						
//						//QUICK FIX.
//						IF IS_VEHICLE_DRIVEABLE(mission_veh[10].veh)
//							DELETE_VEHICLE(mission_veh[10].veh)
//							SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
//						ENDIF
						
						itimer = GET_GAME_TIMER()
						PRINTSTRING("!!!!ASSIGNED MAIN SHOOTOUT TASKS")PRINTNL()
						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
						iProgress++
					ENDIF
				ENDIF
			ENDIF
				
		BREAK
		
		CASE 4
			
			IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PISTOL)
			OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
			OR IS_PED_SHOOTING(PLAYER_PED_ID()) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1699.917969,3325.750000,38.895042>>, <<1721.699219,3236.393799,56.984840>>, 76.250000)
				IF NOT bStartAggressionManagement
					IF TRIGGER_MUSIC_EVENT("TRV2_FIGHT_START")
						bStartAggressionManagement = TRUE
						iTimeInCombat = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("TRIGGER SHOOTOUT BIKER")
				EXIT_VEHICLE_TO_FIGHT(mission_veh[0].veh)
				IF NOT IS_PED_INJURED(driver[0].ped)
					SET_PED_COMBAT_MOVEMENT(driver[0].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(driver[0].ped,CA_CAN_CHARGE,TRUE)
					SET_PED_CHANCE_OF_FIRING_BLANKS(driver[0].ped,0.3,0.4)
					SET_PED_SPHERE_DEFENSIVE_AREA(driver[0].ped,<< 1719.92, 3278.49, 40.11 >> ,25,TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(driver[0].ped)
				IF IS_PED_IN_ANY_VEHICLE(driver[0].ped)
					IF DOES_BLIP_EXIST(driver[0].blip)
						SET_BLIP_SCALE(driver[0].blip,0.5)
					ENDIF
				ENDIF
			ENDIF
			
			IF ARE_ONLY_X_BIKER_PEDS_LEFT(3)
				MANAGE_RON_PLANE_AUDIO()
			ENDIF

			IF (GET_GAME_TIMER()-itimer) > 5000
				IF ARE_ONLY_X_BIKER_PEDS_LEFT(1)
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							SET_PED_STEALTH_MOVEMENT(psRon.pedIndex,FALSE)
							IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
								OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
										TASK_ENTER_VEHICLE(NULL,mission_veh[1].veh,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_SPRINT) 
								CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_PERFORM_SEQUENCE(psRon.pedIndex,TASK_SEQUENCE)
								CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_ON_SCREEN(psRon.pedIndex)
						IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
							SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
						ENDIF
					ENDIF
					
					TRIGGER_MUSIC_EVENT("TRV2_TO_PLANE")
					
					REQUEST_VEHICLE_RECORDING(1,"RCSJC")
					REQUEST_VEHICLE_RECORDING(2,"RCSJC")
					REQUEST_VEHICLE_RECORDING(7,"RCSJC")
					REQUEST_VEHICLE_RECORDING(9,"RCSJC")
					REQUEST_VEHICLE_RECORDING(13,"RCSJC")
					
					//NEW
					REQUEST_VEHICLE_RECORDING(23,"RCSJC")
					REQUEST_VEHICLE_RECORDING(24,"RCSJC")
					REQUEST_VEHICLE_RECORDING(25,"RCSJC")
					
					REQUEST_MODEL(G_M_Y_LOST_01)
					REQUEST_MODEL(HEXER)
					REQUEST_MODEL(HAULER)
					REQUEST_MODEL(TANKER)
					REQUEST_MODEL(GBURRITO)
					REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE,ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS),WEAPON_COMPONENT_FLASH)
					
					REQUEST_ANIM_DICT("move_action@p_m_zero@armed@2h_short@idle@high_energy@a")
					REQUEST_ANIM_DICT("misstrevor2ig_9a")
					bGoToSpeech = FALSE
					enemy[0].actionFlag = 0
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						SET_ENTITY_PROOFS(mission_veh[1].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
					ENDIF
					IF DOES_CAM_EXIST(cutscene_cam)
						DESTROY_CAM(cutscene_cam)
					ENDIF
					iprogress++
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 5
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
					IF NOT IS_ENTITY_ON_SCREEN(psRon.pedIndex)
					AND NOT IS_ENTITY_ON_SCREEN(mission_veh[1].veh)
						SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
					ENDIF
					
					IF NOT IS_PED_SITTING_IN_THIS_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
						IF GET_SCRIPT_TASK_STATUS(psRon.pedIndex,SCRIPT_TASK_ENTER_VEHICLE)<> PERFORMING_TASK
							TASK_ENTER_VEHICLE(psRon.pedIndex,mission_veh[1].veh,45000,VS_DRIVER,PEDMOVEBLENDRATIO_SPRINT)
							//FORCE_PED_AI_AND_ANIMATION_UPDATE(psRon.pedIndex)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MANAGE_RON_PLANE_AUDIO()
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_2_GET_TO_PLANE")
				IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SHOOTOUT")
					STOP_AUDIO_SCENE("TREVOR_2_SHOOTOUT")
				ELSE
					START_AUDIO_SCENE("TREVOR_2_GET_TO_PLANE")
					SET_LABEL_AS_TRIGGERED("TREVOR_2_GET_TO_PLANE",TRUE)
				ENDIF
			ENDIF

			IF NOT IS_PED_INJURED(psRon.pedIndex)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) 
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[1].veh),GET_ENTITY_COORDS(PLAYER_PED_ID())) < 10
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_JUMP)
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
					ELSE
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, FALSE)
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1726.224243,3320.126709,39.726074>>, <<1740.298218,3321.192871,43.226074>>, 14.000000)
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[1].veh),GET_ENTITY_COORDS(PLAYER_PED_ID())) < 5
						IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(psRon.pedIndex)
								IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
									REQUEST_ANIM_DICT("misstrevor2ig_9a")
									IF HAS_ANIM_DICT_LOADED("misstrevor2ig_9a")
									AND HAS_ANIM_DICT_LOADED("move_action@p_m_zero@armed@2h_short@idle@high_energy@a")
									AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
										IF DOES_ENTITY_EXIST(mission_veh[10].veh)
											DELETE_VEHICLE(mission_veh[10].veh)
											SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
										ENDIF
										SET_PED_CAN_RAGDOLL(psRon.pedIndex,FALSE)
										CLEAN_OBJECTIVE_BLIP_DISPLAY()
										CLEANUP_LOADED_MODEL_ARRAY()
										
										IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_GETPLNG1")
											CLEAR_PRINTS()
										ENDIF
										
										CLEANUP_LOADED_CARREC_ARRAY("RCSJC")
										bDialogue = FALSE
										
										START_AUDIO_SCENE("TREVOR_2_SHOOTOUT_ON_WING")
										
										//PLAY MINI CUTSCENE
										CREATE_ENEMY_AT_COORD(11, enemy[11].loc, enemy[11].head, FALSE) //[MF] Early spawning this guy so we don't see him spawn on screen if van is destroyed B*1763431
										CUTSCENE_JUMP_ONTO_PLANE()

										IF NOT IS_PED_INJURED(psRon.pedIndex)
											SET_PED_CAN_RAGDOLL(psRon.pedIndex,TRUE)
										ENDIF
										iprogress++
									ENDIF
								ELSE
									IF bRonTasks = FALSE
										bRonTasks = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()),2)
								TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
								SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
							ENDIF
						ENDIF
					ELSE
						IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
							IF NOT DOES_BLIP_EXIST(veh_blip)
								
								CLEAN_OBJECTIVE_BLIP_DISPLAY()
								veh_blip=CREATE_BLIP_FOR_VEHICLE(mission_veh[1].veh,FALSE)
								SET_BLIP_ROUTE(veh_blip, TRUE)
								IF DOES_BLIP_EXIST(psRon.blipIndex)
									REMOVE_BLIP(psRon.blipIndex)
								ENDIF
							
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 6
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_TREVOR_PLANE].veh,TRUE)
			ENDIF
			RELEASE_AMBIENT_AUDIO_BANK()
			REMOVE_ANIM_DICT("misstrevor2ig_9a")
			ADVANCE_MISSION_STAGE()
		BREAK
		
	ENDSWITCH

ENDPROC

PROC MANAGE_VEHICLE_AGGRO()

	IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) AND NOT IS_PED_INJURED(psRon.pedIndex)
			
			//GBURRITO
			IF bplaybackstarted[3]
				IF IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
					//IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[22].veh)
					IF IS_ENTITY_AT_COORD(mission_veh[25].veh,<< 1665.4100, 3232.8628, 39.6297 >>,<<5,5,5>>)
						IF bEnemyTasks[2] = FALSE
							IF NOT IS_PED_INJURED(driver[25].ped)
								SET_PED_COMBAT_MOVEMENT(driver[25].ped,CM_STATIONARY)
								SET_PED_COMBAT_ATTRIBUTES(driver[25].ped,CA_USE_VEHICLE,FALSE)
								CLEAR_PED_TASKS(driver[25].ped)
								OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
									//TASK_LEAVE_ANY_VEHICLE(NULL,100,ECF_DONT_CLOSE_DOOR|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
									TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
									TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL,mission_veh[1].veh,psRon.pedIndex,2,TRUE,20)
									TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_PERFORM_SEQUENCE(driver[25].ped,TASK_SEQUENCE)
								CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
							ENDIF
							IF NOT IS_PED_INJURED(driver[26].ped)
								SET_PED_COMBAT_MOVEMENT(driver[26].ped,CM_STATIONARY)
								SET_PED_COMBAT_ATTRIBUTES(driver[26].ped,CA_USE_VEHICLE,FALSE)
								CLEAR_PED_TASKS(driver[26].ped)
								OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
									//TASK_LEAVE_ANY_VEHICLE(NULL,80,ECF_DONT_CLOSE_DOOR|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
									TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL,mission_veh[1].veh,psRon.pedIndex,2,TRUE,20)
									TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_PERFORM_SEQUENCE(driver[26].ped,TASK_SEQUENCE)
								CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
							ENDIF
							bEnemyTasks[2] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bEnemyTasks[3] = FALSE
				REQUEST_ANIM_DICT("misstrevor2ig_10")
				IF IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
					IF HAS_ANIM_DICT_LOADED("misstrevor2ig_10")
						IF IS_ENTITY_AT_COORD(mission_veh[25].veh,<< 1665.4100, 3232.8628, 39.6297 >>,<<5,5,5>>)
							IF IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
								sceneIdBikerActions = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIdBikerActions,mission_veh[25].veh,-1)
								SET_VEHICLE_DOOR_OPEN(mission_veh[25].veh,SC_DOOR_REAR_LEFT)
								SET_VEHICLE_DOOR_OPEN(mission_veh[25].veh,SC_DOOR_REAR_RIGHT)
								IF NOT IS_PED_INJURED(driver[27].ped)
									SET_PED_COMBAT_MOVEMENT(driver[27].ped,CM_WILLADVANCE)
									GIVE_WEAPON_TO_PED(driver[27].ped,WEAPONTYPE_PISTOL,100,TRUE,TRUE)
									SET_CURRENT_PED_WEAPON(driver[27].ped,WEAPONTYPE_PISTOL,TRUE)
									SET_PED_CURRENT_WEAPON_VISIBLE(driver[27].ped,TRUE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[27].ped,TRUE)
									CLEAR_PED_TASKS_IMMEDIATELY(driver[27].ped)
									TASK_SYNCHRONIZED_SCENE (driver[27].ped, sceneIdBikerActions, "misstrevor2ig_10", "jump_out_of_van_ds", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
									FORCE_PED_AI_AND_ANIMATION_UPDATE(driver[27].ped)
								ENDIF
								
								IF NOT IS_PED_INJURED(driver[28].ped)
									CLEAR_PED_TASKS(driver[28].ped)
									SET_PED_COMBAT_MOVEMENT(driver[28].ped,CM_WILLADVANCE)
									GIVE_WEAPON_TO_PED(driver[28].ped,WEAPONTYPE_ASSAULTRIFLE,100,TRUE,TRUE)
									SET_CURRENT_PED_WEAPON(driver[28].ped,WEAPONTYPE_ASSAULTRIFLE,TRUE)
									SET_PED_CURRENT_WEAPON_VISIBLE(driver[28].ped,TRUE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[28].ped,TRUE)
									CLEAR_PED_TASKS_IMMEDIATELY(driver[28].ped)
									TASK_SYNCHRONIZED_SCENE (driver[28].ped, sceneIdBikerActions, "misstrevor2ig_10", "jump_out_of_van_ps", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
									FORCE_PED_AI_AND_ANIMATION_UPDATE(driver[28].ped)
								ENDIF
								PRINTSTRING("EXITING VAN SS")PRINTNL()
								bEnemyTasks[3] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF bEnemyTasks[3] = TRUE
				IF bEnemyTasks[4] = FALSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdBikerActions)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdBikerActions) = 1
							WEAPON_TYPE wtEnemy1
							WEAPON_TYPE wtEnemy2
							IF NOT IS_PED_INJURED(driver[27].ped)
								GET_CURRENT_PED_WEAPON(driver[27].ped,wtEnemy1)
								SET_PED_COMBAT_MOVEMENT(driver[27].ped,CM_WILLADVANCE)
								
								IF wtEnemy1 != WEAPONTYPE_PISTOL
									GIVE_WEAPON_TO_PED(driver[27].ped,WEAPONTYPE_PISTOL,100,TRUE)
									SET_CURRENT_PED_WEAPON(driver[27].ped,WEAPONTYPE_PISTOL,TRUE)
								ENDIF
								TASK_COMBAT_PED(driver[27].ped,PLAYER_PED_ID())
								FORCE_PED_AI_AND_ANIMATION_UPDATE(driver[27].ped)
							ENDIF
							
							IF NOT IS_PED_INJURED(driver[28].ped)
								GET_CURRENT_PED_WEAPON(driver[28].ped,wtEnemy2)
								SET_PED_COMBAT_MOVEMENT(driver[28].ped,CM_WILLADVANCE)
								IF wtEnemy2 != WEAPONTYPE_PISTOL
									GIVE_WEAPON_TO_PED(driver[28].ped,WEAPONTYPE_PISTOL,100,TRUE)
									SET_CURRENT_PED_WEAPON(driver[28].ped,WEAPONTYPE_PISTOL,TRUE)
								ENDIF
								TASK_COMBAT_PED(driver[28].ped,PLAYER_PED_ID())
								FORCE_PED_AI_AND_ANIMATION_UPDATE(driver[28].ped)
							ENDIF
							
							
							PRINTSTRING("COMBATING PLAYER")PRINTNL()
							bEnemyTasks[4] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			

			//HEXER 2
			IF IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[24].veh)
					IF bEnemyTasks[1] = FALSE
						IF NOT IS_PED_INJURED(driver[24].ped)
							SET_PED_COMBAT_MOVEMENT(driver[24].ped,CM_STATIONARY)
							SET_PED_COMBAT_ATTRIBUTES(driver[24].ped,CA_USE_VEHICLE,FALSE)
							OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
								//TASK_LEAVE_ANY_VEHICLE(NULL)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
								TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL,mission_veh[1].veh,psRon.pedIndex,2,TRUE)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_PERFORM_SEQUENCE(driver[24].ped,TASK_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
						ENDIF
						bEnemyTasks[1] = TRUE
					ENDIF
				ELSE
					IF IS_PED_INJURED(driver[24].ped)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[24].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[24].veh)
							APPLY_FORCE_TO_ENTITY(mission_veh[24].veh,APPLY_TYPE_IMPULSE,<<2,2,0>>,<<0,0,0>>,0,TRUE,FALSE,TRUE)
						ENDIF
					ELSE
						IF NOT IS_PED_IN_VEHICLE(driver[24].ped,mission_veh[24].veh)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[24].veh)
								STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[24].veh)
								APPLY_FORCE_TO_ENTITY(mission_veh[24].veh,APPLY_TYPE_IMPULSE,<<2,2,0>>,<<0,0,0>>,0,TRUE,FALSE,TRUE)	
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_SEAT_FREE(mission_veh[24].veh)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[24].veh)
								STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[24].veh)
								APPLY_FORCE_TO_ENTITY(mission_veh[24].veh,APPLY_TYPE_IMPULSE,<<2,2,0>>,<<0,0,0>>,0,TRUE,FALSE,TRUE)	
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_PED_ACTIONS()

	INT i

	IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
		FOR i = 0 TO MAX_ENEMY_PED -1
			IF DOES_ENTITY_EXIST(enemy[i].ped)
				IF NOT IS_PED_INJURED(enemy[i].ped)
					IF IS_ENTITY_TOUCHING_ENTITY(enemy[i].ped,mission_veh[1].veh)
						IF NOT IS_PED_RUNNING_RAGDOLL_TASK(enemy[i].ped)
							IF DOES_ENTITY_EXIST(enemy[i].ped)
								IF NOT IS_PED_INJURED(enemy[i].ped)
									SET_PED_CAN_RAGDOLL(enemy[i].ped,TRUE)
									IF NOT IS_PED_INJURED(enemy[i].ped)
										APPLY_FORCE_TO_ENTITY(enemy[i].ped,APPLY_TYPE_EXTERNAL_IMPULSE,<<0,4,0.5>>,<<0,0,0>>,GET_PED_BONE_INDEX(enemy[i].ped,BONETAG_PELVIS),TRUE,TRUE,TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

INT iNukeAudioTime

PROC MANAGE_PLAYER_NUKE()
	
	IF iNuke > 2
		IF MANAGE_MY_TIMER(iNukeAudioTime,2500)
			IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_RAYFIRE")
				STOP_AUDIO_SCENE("TREVOR_2_RAYFIRE")
			ENDIF
		ENDIF	
	ENDIF
	
	IF iNuke > 1
		SET_ENTITY_PROOFS(crate[1],FALSE,FALSE,FALSE,FALSE,FALSE)
	ENDIF
	
	IF NOT IS_PED_INJURED(enemy[14].ped)
		IF GET_SCRIPT_TASK_STATUS(enemy[14].ped,SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)<> PERFORMING_TASK
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemy[14].ped,200)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(enemy[16].ped)
		IF GET_SCRIPT_TASK_STATUS(enemy[16].ped,SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)<> PERFORMING_TASK
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemy[16].ped,200)
		ENDIF
	ENDIF
	
//	IF iNuke < 2
		IF DOES_ENTITY_EXIST(mission_veh[5].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[5].veh)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("APPLY FORCE")
					APPLY_FORCE_TO_ENTITY(mission_veh[5].veh,APPLY_TYPE_EXTERNAL_IMPULSE,<<xTrailerForce+10,yTrailerForce,zTrailerForce-5>>,<<0,0,0>>,0,TRUE,TRUE,TRUE)
					SET_LABEL_AS_TRIGGERED("APPLY FORCE",TRUE)
				ENDIF
			ENDIF
		ENDIF
//	ENDIF

	SWITCH iNuke
	
		CASE 0
			SET_LABEL_AS_TRIGGERED("TR2_USEB",TRUE)
			IF NOT IS_PED_INJURED(enemy[14].ped)
				SET_PED_SPHERE_DEFENSIVE_AREA(enemy[14].ped,<<1687.45, 3262.98, 40.87>>,0.75,TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(enemy[16].ped)
				SET_PED_SPHERE_DEFENSIVE_AREA(enemy[16].ped,<<1697.17, 3265.93, 41.03>>,0.75,TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(crate[1])
				IF NOT IS_ENTITY_DEAD(crate[1])
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(crate[1],PLAYER_PED_ID())
						iNuke ++
					ENDIF
				ENDIF
			ENDIF
			HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(crate[1],PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(mission_veh[5].veh)
				IF IS_ENTITY_AT_COORD(mission_veh[1].veh,<< 1714.8469, 3253.5327, 40.9562 >>,<<22,22,22>>)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl6d")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl6d", CONV_PRIORITY_HIGH )
								SET_LABEL_AS_TRIGGERED("TR_pl6d",TRUE)
								SET_LABEL_AS_TRIGGERED("TR2_USEB",TRUE)
								iNuke ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				iNuke = 2
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_BLOW")
				IF NOT IS_ENTITY_DEAD(crate[1])
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						PRINT_HELP("SJC_BLOW")
						SET_LABEL_AS_TRIGGERED("SJC_BLOW",TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(crate[1])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(crate[1],PLAYER_PED_ID())
					START_AUDIO_SCENE("TREVOR_2_RAYFIRE")
					
					iNukeAudioTime = GET_GAME_TIMER()
					SET_ENTITY_HEALTH(crate[1],105)
					FREEZE_ENTITY_POSITION(crate[1],FALSE)
					ADD_EXPLOSION(GET_ENTITY_COORDS(crate[1]),EXP_TAG_PLANE)
					iNukeDelay = GET_GAME_TIMER()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_BLOW")
						CLEAR_HELP(TRUE)
					ENDIF
					KILL_ANY_CONVERSATION()
					iNuke ++
				ENDIF
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("SJC_BLOW")
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_DETONATE)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(crate[1],PLAYER_PED_ID())
				OR NOT IS_VEHICLE_DRIVEABLE(mission_veh[5].veh)
					IF DOES_ENTITY_EXIST(crate[1])
						START_AUDIO_SCENE("TREVOR_2_RAYFIRE")
						iNukeAudioTime = GET_GAME_TIMER()
						SET_ENTITY_HEALTH(crate[1],105)
						FREEZE_ENTITY_POSITION(crate[1],FALSE)
						
						SET_ENTITY_INVINCIBLE(crate[1], FALSE)
						SET_ENTITY_PROOFS(crate[1], FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(crate[1], TRUE)
						SET_ENTITY_DYNAMIC(crate[1], FALSE)
						SET_DISABLE_BREAKING(crate[1], FALSE)							
			
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.5)
			
						ADD_EXPLOSION(GET_ENTITY_COORDS(crate[1]),EXP_TAG_PLANE)
						iNukeDelay = GET_GAME_TIMER()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_BLOW")
							CLEAR_HELP(TRUE)
						ENDIF
						KILL_ANY_CONVERSATION()
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[5].veh)
							FREEZE_ENTITY_POSITION(mission_veh[5].veh,TRUE)
						ENDIF
						iNuke ++
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_USEB")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_USEB", CONV_PRIORITY_HIGH )
								SET_LABEL_AS_TRIGGERED("TR2_USEB",TRUE)
								iNukeDelay = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ELSE
						IF MANAGE_MY_TIMER(iNukeDelay,8000)
							SET_LABEL_AS_TRIGGERED("TR2_USEB",FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pl6e")
				IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl6e", CONV_PRIORITY_MEDIUM )
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					SET_LABEL_AS_TRIGGERED("TR_pl6e",TRUE)
				ENDIF
			ENDIF
			
			IF MANAGE_MY_TIMER(iNukeDelay,500)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[5].veh)
					FREEZE_ENTITY_POSITION(mission_veh[5].veh,FALSE)
					APPLY_FORCE_TO_ENTITY(mission_veh[5].veh,APPLY_TYPE_EXTERNAL_IMPULSE,<<xTrailerForce,yTrailerForce,zTrailerForce>>,<<0,0,0>>,0,TRUE,TRUE,TRUE)
					ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[5].veh),EXP_TAG_HI_OCTANE,1,TRUE,FALSE,1.2)
				ENDIF
				IF DOES_ENTITY_EXIST(crate[1])
					IF NOT IS_ENTITY_DEAD(crate[1])
						ADD_EXPLOSION(GET_ENTITY_COORDS(crate[1],FALSE),EXP_TAG_PLANE)
					ENDIF
				ENDIF
			
				IF IS_VEHICLE_DRIVEABLE(mission_veh[4].veh)
					DETACH_VEHICLE_FROM_TRAILER(mission_veh[4].veh)
					ADD_EXPLOSION(GET_ENTITY_COORDS(mission_veh[4].veh),EXP_TAG_PLANE)
				ENDIF
				PLAY_SOUND_FROM_COORD(-1,"Trevor_2_chopper_explode",GET_ENTITY_COORDS(mission_veh[5].veh,FALSE),"TREVOR_2_SOUNDS")
				SHAKE_GAMEPLAY_CAM("LARGE_EXPLOSION_SHAKE",1.0)
				SET_CONTROL_SHAKE(PLAYER_CONTROL,1000,200)
				FOR iCount = 14 TO 18
					IF iCount <> 15
						IF DOES_ENTITY_EXIST(enemy[iCount].ped)
							IF NOT IS_PED_INJURED(enemy[iCount].ped)
								APPLY_FORCE_TO_ENTITY(enemy[iCount].ped,APPLY_TYPE_EXTERNAL_IMPULSE,<<0,4,3>>,<<0,0,0>>,0,TRUE,TRUE,TRUE)
								SET_ENTITY_HEALTH(enemy[iCount].ped,0)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				iNuke ++
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

FLOAT fGroundHeight
FLOAT fHeightAboveGround

// get the plane height above ground
FUNC FLOAT GET_HEIGHT_ABOVE_GROUND(VEHICLE_INDEX planeVehicle, FLOAT &fThisGroundHeight)
      
    VECTOR vPlayerCoords = GET_ENTITY_COORDS(planeVehicle)
    GET_GROUND_Z_FOR_3D_COORD(vPlayerCoords, fThisGroundHeight)
    IF fThisGroundHeight < 0
    	fThisGroundHeight = 0
    ENDIF
      
    fHeightAboveGround = vPlayerCoords.z - fThisGroundHeight  
    IF fHeightAboveGround < 0
    	fHeightAboveGround *= -1
    ENDIF 
      
    RETURN fHeightAboveGround
	
ENDFUNC

PROC MANAGE_FLIGHT_HELP()

	IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DAMAGE")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
			IF GET_VEHICLE_ENGINE_HEALTH(mission_veh[MV_TREVOR_PLANE].veh) < 300
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_DAMAGE", CONV_PRIORITY_MEDIUM )
							SET_LABEL_AS_TRIGGERED("TR2_DAMAGE",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iFlightHelp > 7
		IF iFlightHelp !=99
		OR iFlightHelp !=97
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
				IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_PLANE].veh)
					fHeightAboveGround = GET_HEIGHT_ABOVE_GROUND(mission_veh[MV_TREVOR_PLANE].veh, fGroundHeight)                      
					SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(fGroundHeight + 46)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iFlightHelp 
		
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
			AND IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 12.0
				OR IS_ENTITY_IN_ANGLED_AREA(mission_veh[2].veh, <<1308.810303,3144.014160,37.414104>>, <<1406.186523,3170.944336,59.411552>>, 51.750000)
				OR IS_ENTITY_IN_AIR(mission_veh[1].veh)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_FLYH1")
							CLEAR_HELP(TRUE)
							iFlightTimer = GET_GAME_TIMER()
							iFlightHelp ++ 
						ELSE
							iFlightTimer = GET_GAME_TIMER()
							iFlightHelp ++ 
						ENDIF
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						IF NOT IS_ENTITY_IN_AIR(mission_veh[1].veh)
							PRINTSTRING("DISABLING Y")
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_FRONTEND_AXIS_Y)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_FLYH1")
						PRINT_HELP("SJC_FLYH3",DEFAULT_HELP_TEXT_TIME + 2000)
						iFlightTimer = GET_GAME_TIMER()
						iFlightHelp ++ 
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_FLYH3")
						CLEAR_HELP(TRUE)
					ENDIF
				ENDIF
			ELSE
				CLEAR_HELP(TRUE)
				iFlightHelp ++ 
			ENDIF
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(iFlightTimer,DEFAULT_HELP_TEXT_TIME + 2000)
			OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_FLYH3")
					CLEAR_HELP(TRUE)
					PRINT_HELP("SJC_FLYH2",DEFAULT_HELP_TEXT_TIME + 2000)
					iFlightTimer = GET_GAME_TIMER()
					iFlightHelp ++ 
				ELSE
					PRINT_HELP("SJC_FLYH2",DEFAULT_HELP_TEXT_TIME + 2000)
					iFlightTimer = GET_GAME_TIMER()
					iFlightHelp ++ 
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			
			IF MANAGE_MY_TIMER(iFlightTimer,DEFAULT_HELP_TEXT_TIME + 2000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					IF GET_LANDING_GEAR_STATE(mission_veh[2].veh) = LGS_DEPLOYING
					OR GET_LANDING_GEAR_STATE(mission_veh[2].veh) = LGS_LOCKED_DOWN
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_FLYH2")
							PRINT_HELP("SJC_FLYH4",DEFAULT_HELP_TEXT_TIME + 2000)
							iFlightTimer = GET_GAME_TIMER()
							iFlightHelp ++ 
						ENDIF
					ELSE
						CLEAR_HELP(TRUE)
						iFlightHelp ++ 
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
				IF MANAGE_MY_TIMER(iFlightTimer,7000)
				OR GET_LANDING_GEAR_STATE(mission_veh[2].veh) = LGS_RETRACTING
				OR GET_LANDING_GEAR_STATE(mission_veh[2].veh) = LGS_LOCKED_UP
					IF NOT IS_PED_INJURED(enemy[27].ped)
						IF IS_ENTITY_ATTACHED(enemy[27].ped)
							CLEAR_HELP(TRUE)
							//PRINT_HELP("SJC_ROLL")
							iFlightTimer = GET_GAME_TIMER()
							iFlightHelp ++ 
						ELSE
							CLEAR_HELP(TRUE)
							iFlightHelp ++ 
						ENDIF
					ELSE
						CLEAR_HELP(TRUE)
						iFlightHelp ++ 
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			//IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_ROLL")
			IF DOES_ENTITY_EXIST(enemy[27].ped)
				IF NOT IS_PED_INJURED(enemy[27].ped)
					IF NOT IS_ENTITY_ATTACHED(enemy[27].ped)
						CLEAR_HELP()
						iFlightTimer = GET_GAME_TIMER()
						iFlightHelp ++ 
					ENDIF
				ENDIF
			ELSE
				CLEAR_HELP()
				iFlightTimer = GET_GAME_TIMER()
				iFlightHelp ++ 
			ENDIF
//			ELSE
//				iFlightTimer = GET_GAME_TIMER()
//				iFlightHelp ++ 
//			ENDIF
		BREAK
		
		CASE 6
			IF MANAGE_MY_TIMER(iFlightTimer,1400)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("SJC_PILOTHS1")
					iFlightTimer = GET_GAME_TIMER()
					iFlightHelp ++ 
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF MANAGE_MY_TIMER(iFlightTimer,7000)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("SJC_PILOTHS2")
					iFlightTimer = GET_GAME_TIMER()
					iFlightHelp = 99
				ENDIF
			ENDIF
		BREAK
		
		CASE 99
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< -2819.09131, 3326.99341, 31.81007 >>) < 1000
				IF MANAGE_MY_TIMER(iFlightTimer,7000)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("SJC_BASHLP")
						iFlightTimer = GET_GAME_TIMER()
						iFlightHelp = 97
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 97
			IF eMissionStage =  MISSION_STAGE_FLIGHT
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_MILB")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_MILB", CONV_PRIORITY_MEDIUM )
								SET_LABEL_AS_TRIGGERED("TR2_MILB",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MANAGE_MY_TIMER(iFlightTimer,7000)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP("SJC_LOWHLP")
							iFlightTimer = GET_GAME_TIMER()
							iFlightHelp = 8
						ENDIF
					ENDIF
				ENDIF
			ELSE
				iFlightHelp = 8
			ENDIF
		BREAK
		
		CASE 8
			IF eMissionStage = MISSION_STAGE_TO_AIRSTRIP
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_FLYH5")
					IF MANAGE_MY_TIMER(iFlightTimer,7000)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							//PRINT_HELP("SJC_FLYH5")
							SET_LABEL_AS_TRIGGERED("SJC_FLYH5",TRUE)
						ENDIF
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 2121.5569, 4803.9385, 40.1959 >>) < 1300
						IF SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()
							CLEAR_HELP()
							PRINT_HELP("SJC_CAMLAND")
						ENDIF
						IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLY_BACK")
							STOP_AUDIO_SCENE("TREVOR_2_FLY_BACK")
						ENDIF
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLY_BACK")
							START_AUDIO_SCENE("TREVOR_2_LAND_THE_PLANE")
						ENDIF
						
						iFlightTimer = GET_GAME_TIMER()
						iFlightHelp ++ 
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 9
			IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_CAMLAND")
//				PRINTLN("@@@@@@@@@@@ CLEAR_HELP() @@@@@@@@@@@@")
				CLEAR_HELP()
			ENDIF
			IF eMissionStage = MISSION_STAGE_TO_AIRSTRIP
				IF MANAGE_MY_TIMER(iFlightTimer,7000)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 2121.5569, 4803.9385, 40.1959 >>) < 800
						IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
							IF GET_LANDING_GEAR_STATE(mission_veh[2].veh) = LGS_LOCKED_UP
							OR GET_LANDING_GEAR_STATE(mission_veh[2].veh) = LGS_RETRACTING
								CLEAR_HELP()
								PRINT_HELP("SJC_DEPLOY")
								iFlightHelp ++ 
							ELSE
								iFlightHelp ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			IF eMissionStage = MISSION_STAGE_TO_AIRSTRIP
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					IF GET_LANDING_GEAR_STATE(mission_veh[2].veh) = LGS_DEPLOYING
					OR GET_LANDING_GEAR_STATE(mission_veh[2].veh) = LGS_LOCKED_DOWN
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_DEPLOY")
							CLEAR_HELP()
						ENDIF
						iFlightTimer = GET_GAME_TIMER()
						iFlightHelp ++ 
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 11
			PRINT_HELP("SJC_FLYH2",DEFAULT_HELP_TEXT_TIME)
			iFlightHelp ++ 
		BREAK
		
	ENDSWITCH

ENDPROC


/// PURPOSE:
/// Play the land plane cutscene
PROC RUN_JUMP_OFF_WING_CUTSCENE()
	
	PED_INDEX ped
	e_section_stage = SECTION_STAGE_SETUP
	bcutsceneplaying = TRUE
	b_skipped_mocap = FALSE
	
	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				b_skipped_mocap = TRUE
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ENDIF
	#ENDIF

	WHILE bcutsceneplaying
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TREV2")

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					bcutsceneplaying = TRUE
					REQUEST_CUTSCENE("TRV_2_MCS_6")
					REQUEST_VEHICLE_RECORDING(3,"RCSJC")
					bClearCutscenArea = FALSE
					b_skipped_mocap = FALSE
					bJumpedOffPlaneWing = FALSE
					PRINTSTRING("CUTSCNE -here - 1")
					fRonPlayback = 0.4
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					CREATE_ENEMIES(27,27,FALSE,FALSE,FALSE,FALSE)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_HEAD, 		0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_BERD, 		1, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_HAIR, 		0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_TORSO, 		0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_LEG, 		0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_HAND, 		0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_FEET, 		0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_TEETH, 		0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_SPECIAL, 	0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_SPECIAL2, 	0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_DECL, 		0, 0)
					SET_PED_COMPONENT_VARIATION(enemy[27].ped, PED_COMP_JBIB, 		0, 0)
					KILL_ANY_CONVERSATION()
					i_current_event++
				ELIF i_current_event = 1
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
							WAIT(0)
						ENDIF
						
						GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtCutscene)
						
						TrevorWeapon =CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), wtCutscene)
						IF DOES_ENTITY_EXIST(TrevorWeapon)
							REGISTER_ENTITY_FOR_CUTSCENE(TrevorWeapon,"Trevors_weapon", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[1].veh,"Rons_Plane", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[2].veh, "Trevors_Plane", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(enemy[27].ped)
							REGISTER_ENTITY_FOR_CUTSCENE(enemy[27].ped, "Lost_biker", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(enemy[27].ped)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lost_biker", enemy[27].ped, G_M_Y_LOST_01)
						ENDIF
						
						KILL_ANY_CONVERSATION()
						KILL_FACE_TO_FACE_CONVERSATION()
						
						IF IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
							STOP_PED_SPEAKING(PLAYER_PED_ID(),TRUE)
						ENDIF
						
						DETACH_ENTITY(PLAYER_PED_ID())

						START_CUTSCENE()

						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

						PRINTSTRING("CUTSCNE -here - 2")
						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						bJumpTime = FALSE

						e_section_stage = SECTION_STAGE_RUNNING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF NOT b_skipped_mocap
				IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
					b_skipped_mocap = TRUE
					
					//IF we skip the cutscene stop any bike vehicle recording playbacks
					IF IS_VEHICLE_DRIVEABLE(mission_veh[15].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[15].veh)
							PRINTLN("Starting recording of: mission_veh[15]")
							STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[15].veh)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[16].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[16].veh)
							PRINTLN("Starting recording of: mission_veh[16]")
							STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[16].veh)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[17].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[17].veh)
							PRINTLN("Starting recording of: mission_veh[17]")
							STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[17].veh)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[18].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[18].veh)
							PRINTLN("Starting recording of: mission_veh[18]")
							STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[18].veh)
						ENDIF
					ENDIF
					
					WAIT(0)
					
					REQUEST_VEHICLE_RECORDING(14,"RCSJC")
					REQUEST_VEHICLE_RECORDING(15,"RCSJC")
					REQUEST_VEHICLE_RECORDING(16,"RCSJC")
					REQUEST_VEHICLE_RECORDING(17,"RCSJC")
				
				ENDIF
			ENDIF
			
			IF bClearCutscenArea = FALSE
				IF IS_CUTSCENE_PLAYING()
					IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_SHOOTOUT_ON_WING")
						STOP_AUDIO_SCENE("TREVOR_2_SHOOTOUT_ON_WING")
					ENDIF
					
					IF bJumpTime = FALSE
						IF GET_CLOCK_HOURS() < 5
							IF GET_CLOCK_HOURS() < 4
								ADD_TO_CLOCK_TIME(0,30,0)
							ELSE
								ADD_TO_CLOCK_TIME(0,5,0)
							ENDIF
						ENDIF
						bJumpTime = TRUE
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
						SET_ENTITY_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
						SET_VEHICLE_FIXED(mission_veh[MV_RON_PLANE].veh)
						CLEAR_AREA(GET_ENTITY_COORDS(mission_veh[MV_RON_PLANE].veh),100,TRUE)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
						SET_ENTITY_HEALTH(mission_veh[MV_TREVOR_PLANE].veh,1000)
						CLEAR_AREA(GET_ENTITY_COORDS(mission_veh[MV_TREVOR_PLANE].veh),100,TRUE)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						IF NOT IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_DRIVER)
							ped = GET_PED_IN_VEHICLE_SEAT(mission_veh[2].veh,VS_DRIVER)
							DELETE_PED(ped)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(driver[1].ped)
						DELETE_PED(driver[1].ped)
					ENDIF	
					
					DETACH_ENTITY(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					PRINTSTRING("CUTSCNE -here - 3")
					ACTIVATE_ENEMY(27)
					
					bClearCutscenArea = TRUE
				ENDIF
			ENDIF			
			
//			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
//			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Rons_Plane")
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
					SET_VEHICLE_DOOR_SHUT(mission_veh[MV_RON_PLANE].veh,SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_SHUT(mission_veh[MV_RON_PLANE].veh,SC_DOOR_FRONT_RIGHT)
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_PLANE].veh)
							IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_RON_PLANE].veh)
								SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[MV_RON_PLANE].veh)
							ENDIF
						ELSE
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(3,"RCSJC")
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
									STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_PLANE].veh)
								ENDIF
								IF NOT HAS_LABEL_BEEN_TRIGGERED("START_RON_PLAYBACK")
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_PLANE].veh,3,"RCSJC")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_RON_PLANE].veh,1000)
									SET_ENTITY_PROOFS(mission_veh[MV_RON_PLANE].veh,FALSE,FALSE,FALSE,TRUE,TRUE)
									SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[MV_RON_PLANE].veh,FALSE)
									SET_VEHICLE_ENGINE_ON(mission_veh[MV_RON_PLANE].veh,TRUE,TRUE)
									SET_PLAYBACK_SPEED(mission_veh[MV_RON_PLANE].veh,fRonPlayback)
									SET_VEHICLE_FIXED(mission_veh[MV_RON_PLANE].veh)
									SET_VEHICLE_ACTIVE_DURING_PLAYBACK(mission_veh[MV_RON_PLANE].veh,TRUE)
									SET_LABEL_AS_TRIGGERED("START_RON_PLAYBACK",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_Plane")
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
					SET_VEHICLE_DOOR_SHUT(mission_veh[MV_TREVOR_PLANE].veh,SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_SHUT(mission_veh[MV_TREVOR_PLANE].veh,SC_DOOR_FRONT_RIGHT)
					SET_ENTITY_COORDS(mission_veh[MV_TREVOR_PLANE].veh,<<1562.26, 3225.03, 41.12>>)
					SET_ENTITY_HEADING(mission_veh[MV_TREVOR_PLANE].veh,108.91)
					FREEZE_ENTITY_POSITION(mission_veh[MV_TREVOR_PLANE].veh,FALSE)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_TREVOR_PLANE].veh,TRUE,TRUE)
					SET_HELI_BLADES_SPEED(mission_veh[MV_TREVOR_PLANE].veh,0.5)
					SET_ENTITY_PROOFS(mission_veh[MV_TREVOR_PLANE].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
					SET_VEHICLE_FORWARD_SPEED(mission_veh[MV_TREVOR_PLANE].veh,2)
					SET_VEHICLE_FIXED(mission_veh[MV_TREVOR_PLANE].veh)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lost_biker")
				IF NOT IS_PED_INJURED(enemy[27].ped)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDJumpOnToWing)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
							sceneIDJumpOnToWing = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							TASK_SYNCHRONIZED_SCENE(enemy[27].ped, sceneIDJumpOnToWing, "misstrevor2ig_11", "idle_intro", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDJumpOnToWing, mission_veh[MV_TREVOR_PLANE].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[MV_TREVOR_PLANE].veh, "chassis"))
							PRINTSTRING("setting STATE STATE_BALANCE") PRINTNL()
							iLeanStage = 0
							DISABLE_PLANE_AILERON(mission_veh[MV_TREVOR_PLANE].veh,FALSE,TRUE)
							eBalanceState = STATE_BALANCE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
				
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_PLANE].veh)
						IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_TREVOR_PLANE].veh)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_PLANE].veh)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA() 
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 13000
				IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLIGHT_START")	
					START_AUDIO_SCENE("TREVOR_2_FLIGHT_START")
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("START_THE BIKERS")
				REQUEST_VEHICLE_RECORDING(14,"RCSJC")
				REQUEST_VEHICLE_RECORDING(15,"RCSJC")
				REQUEST_VEHICLE_RECORDING(16,"RCSJC")
				REQUEST_VEHICLE_RECORDING(17,"RCSJC")
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(14,"RCSJC")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(15,"RCSJC")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(16,"RCSJC")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(17,"RCSJC")
					IF GET_CUTSCENE_TIME() > 1616
						//IF NOT DOES_ENTITY_EXIST
						CREATE_ENEMY_VEHICLE(15,HEXER,<<1737.5721, 3308.0176, 40.2237>>,143.6793 ,G_M_Y_LOST_01,TRUE,15)
						CREATE_ENEMY_VEHICLE(16,HEXER,<<1731.9744, 3307.7664, 40.2237>> , 272.7215,G_M_Y_LOST_01,TRUE,16)
						CREATE_ENEMY_VEHICLE(17,HEXER,<<1728.3912, 3308.8350, 40.2237>> , 272.7215,G_M_Y_LOST_01,TRUE,17)
						CREATE_ENEMY_VEHICLE(18,HEXER,<<1724.5388, 3309.1536, 40.2237>> , 272.7215,G_M_Y_LOST_01,TRUE,18)
					
						IF IS_VEHICLE_DRIVEABLE(mission_veh[15].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[15].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[15].veh,14,"RCSJC",4,0,DRIVINGMODE_AVOIDCARS)
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[15].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
								IF NOT b_skipped_mocap
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[15].veh,2000)
								ELSE
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[15].veh, fBike1CutsceneSkipStartTime)
								ENDIF
								SET_PLAYBACK_SPEED(mission_veh[15].veh,0.7)
							ENDIF
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[16].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[16].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[16].veh,15,"RCSJC",4,0,DRIVINGMODE_AVOIDCARS)
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[16].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
								IF NOT b_skipped_mocap
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[16].veh,3000)
								ELSE
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[16].veh, fBike2CutsceneSkipStartTime)
								ENDIF
								SET_PLAYBACK_SPEED(mission_veh[16].veh,0.7)
							ENDIF
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[17].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[17].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[17].veh,16,"RCSJC",4,0,DRIVINGMODE_STOPFORCARS_IGNORELIGHTS)
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[17].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
								IF NOT b_skipped_mocap
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[17].veh,1000)
								ELSE
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[17].veh, fBike3CutsceneSkipStartTime)
								ENDIF
								SET_PLAYBACK_SPEED(mission_veh[17].veh,0.6)
							ENDIF
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[18].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[18].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[18].veh,17,"RCSJC",4,0,DRIVINGMODE_STOPFORCARS_IGNORELIGHTS)
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[18].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
								IF NOT b_skipped_mocap
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[18].veh,2000)
								ELSE
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[18].veh, fBike4CutsceneSkipStartTime)
								ENDIF
								SET_PLAYBACK_SPEED(mission_veh[18].veh,0.7)
							ENDIF
						ENDIF
						PRINTSTRING("BIKER MIKE FROM MARS!")PRINTNL()
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[18].veh)
						AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[15].veh)
						AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[15].veh)
						AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[15].veh)
							SET_LABEL_AS_TRIGGERED("START_THE BIKERS",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			AND bClearCutscenArea = TRUE
				PRINTSTRING("CUTSCNE -here - 4")
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			IF b_skipped_mocap
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
				
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(14,"RCSJC")
					IF IS_VEHICLE_DRIVEABLE(mission_veh[15].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[15].veh)
							PRINTLN("START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[15].veh,14,")
							START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[15].veh,14,"RCSJC",4,0,DRIVINGMODE_AVOIDCARS)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[15].veh, fBike1CutsceneSkipStartTime)
							SET_PLAYBACK_SPEED(mission_veh[15].veh,0.7)
						ENDIF
					ENDIF
				ENDIF

				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(15,"RCSJC")
					IF IS_VEHICLE_DRIVEABLE(mission_veh[16].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[16].veh)
						PRINTLN("START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[16].veh,15,")
							START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[16].veh,15,"RCSJC",4,0,DRIVINGMODE_AVOIDCARS)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[16].veh,fBike2CutsceneSkipStartTime)
							SET_PLAYBACK_SPEED(mission_veh[16].veh,0.7)
						ENDIF
					ENDIF
				ENDIF
					
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(16,"RCSJC")
					IF IS_VEHICLE_DRIVEABLE(mission_veh[17].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[17].veh)
						PRINTLN("START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[17].veh,16,")
							START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[17].veh,16,"RCSJC",4,0,DRIVINGMODE_STOPFORCARS_IGNORELIGHTS)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[17].veh,fBike3CutsceneSkipStartTime)
							SET_PLAYBACK_SPEED(mission_veh[17].veh,0.6)
						ENDIF
					ENDIF
				ENDIF
					
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(17,"RCSJC")
					IF IS_VEHICLE_DRIVEABLE(mission_veh[18].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[18].veh)
							PRINTLN("START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[18].veh,17,")
							START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[18].veh,17,"RCSJC",4,0,DRIVINGMODE_STOPFORCARS_IGNORELIGHTS)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[18].veh,fBike4CutsceneSkipStartTime)
							SET_PLAYBACK_SPEED(mission_veh[18].veh,0.7)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		
			REPLAY_STOP_EVENT()
		
			//Setup buddy relationship groups etc.
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			REMOVE_CUTSCENE()
			i_current_event = 0
			STOP_PED_SPEAKING(PLAYER_PED_ID(),FALSE)
			bcutsceneplaying = FALSE
			IF bJumpedOffPlaneWing = FALSE
				bJumpedOffPlaneWing = TRUE
			ENDIF
			PRINTSTRING("CUTSCNE -here - 5")
			e_section_stage = SECTION_STAGE_SETUP
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDWHILE
ENDPROC

PROC MANAGE_BIKER_RUNWAY_DIALOGUE()

	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(),pedBattleEnemies)

	INT i
	
	FOR i = 0 TO 9
		IF DOES_ENTITY_EXIST(pedBattleEnemies[i])
			IF NOT IS_PED_INJURED(pedBattleEnemies[i])
				IF IS_PED_MODEL(pedBattleEnemies[i],G_M_Y_LOST_01)
					IF MANAGE_MY_TIMER(iAmbientDialogueTimer, 9000)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedBattleEnemies[i])) < 25
							IF GET_RANDOM_BOOL()
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBattleEnemies[i],"TR2_DMAA","LOST5",SPEECH_PARAMS_INTERRUPT_SHOUTED)
							ELSE
								IF GET_RANDOM_BOOL()
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBattleEnemies[i],"TR2_DNAA","LOST6",SPEECH_PARAMS_INTERRUPT_SHOUTED)
								ELSE
									IF GET_RANDOM_BOOL()
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBattleEnemies[i],"TR2_EFAA","LOST7",SPEECH_PARAMS_INTERRUPT_SHOUTED)
									ELSE
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBattleEnemies[i],"TR2_EEAA","LOST7",SPEECH_PARAMS_INTERRUPT_SHOUTED)
									ENDIF
								ENDIF
							ENDIF
							iAmbientDialogueTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC MANAGE_RUNWAY_DIALOGUE()
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
			//PRINTSTRING("iRunWayDialogue:")PRINTINT(iRunWayDialogue)PRINTNL()
			//PRINTSTRING("fTime in Recording:")PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(mission_veh[1].veh))PRINTNL()
			fWingPlayback = GET_TIME_POSITION_IN_RECORDING(mission_veh[1].veh)
		ENDIF
	ENDIF

	SWITCH iRunWayDialogue
	
		CASE 0
		
			IF iNuke = 0 
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_bp02")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_bp02", CONV_PRIORITY_HIGH )
							SET_LABEL_AS_TRIGGERED("TR_bp02",TRUE)
							iRunWayTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ELSE
					IF MANAGE_MY_TIMER(iRunWayTimer,7000)
						SET_LABEL_AS_TRIGGERED("TR_bp02",FALSE)
					ENDIF
				ENDIF
			ELSE
				IF iNuke > 2
					iRunWayDialogue ++
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 1
		
			//GUYS ON THE LEFT BY THE CAR
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RLSb1")
				IF fWingPlayback > 6701.976074
				AND fWingPlayback < 9652.748047
					IF NOT IS_PED_INJURED(enemy[10].ped)
					OR NOT IS_PED_INJURED(enemy[11].ped)
					OR NOT IS_PED_INJURED(driver[13].ped)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RLSb", CONV_PRIORITY_HIGH )
								SET_LABEL_AS_TRIGGERED("TR2_RLSb1",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//GUYS BY THE TANKER
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RLSb2")
				IF fWingPlayback > 13078.115234
					IF NOT IS_PED_INJURED(enemy[18].ped)
					OR NOT IS_PED_INJURED(enemy[16].ped)
					OR NOT IS_PED_INJURED(driver[14].ped)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RLS", CONV_PRIORITY_HIGH )
								SET_LABEL_AS_TRIGGERED("TR2_RLSb2",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//GUYS ON THE AHEAD COMING IN
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RLSb3")
				IF fWingPlayback > 19323.644531
					IF NOT IS_PED_INJURED(enemy[6].ped)
					OR NOT IS_PED_INJURED(enemy[7].ped)
					OR NOT IS_PED_INJURED(enemy[8].ped)
					OR NOT IS_PED_INJURED(enemy[9].ped)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RLSc", CONV_PRIORITY_HIGH )
								SET_LABEL_AS_TRIGGERED("TR2_RLSb3",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//GUYS ON THE LEFT BY THE CAR
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RLSb4")
				IF fWingPlayback > 21421.925781
					IF NOT IS_PED_INJURED(driver[28].ped)
					OR NOT IS_PED_INJURED(driver[27].ped)
					OR NOT IS_PED_INJURED(driver[25].ped)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RLSb", CONV_PRIORITY_HIGH )
								SET_LABEL_AS_TRIGGERED("TR2_RLSb4",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//GUYS ON THE RIGHT BY THE CAR - VAN
			IF HAS_LABEL_BEEN_TRIGGERED("TR2_RLSb4")
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RLSb5")
					IF fWingPlayback > 21375.679688
						IF NOT IS_PED_INJURED(driver[6].ped)
						OR NOT IS_PED_INJURED(driver[7].ped)
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RLS", CONV_PRIORITY_HIGH )
									SET_LABEL_AS_TRIGGERED("TR2_RLSb5",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//GUYS UP AHEAD
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RLSb6")
				IF fWingPlayback > 24887.062500
					IF NOT IS_PED_INJURED(driver[24].ped)
					OR NOT IS_PED_INJURED(enemy[15].ped)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							//IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RLSc", CONV_PRIORITY_HIGH )
								SET_LABEL_AS_TRIGGERED("TR2_RLSb6",TRUE)
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_bp02")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_bp02", CONV_PRIORITY_MEDIUM )
						SET_LABEL_AS_TRIGGERED("TR_bp02",TRUE)
						iRunWayTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ELSE
				IF MANAGE_MY_TIMER(iRunWayTimer,9000)
					SET_LABEL_AS_TRIGGERED("TR_bp02",FALSE)
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH
				


ENDPROC

PROC MANAGE_VEHICLE_COMBAT_RESPONSE_FINAL()
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER FINAL LINE OF ENEMIES")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[8].veh)
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[8].veh)
				IF NOT IS_PED_INJURED(driver[6].ped)
					SET_PED_COMBAT_ATTRIBUTES(driver[6].ped,CA_USE_VEHICLE,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(driver[6].ped,CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(driver[6].ped,CA_DISABLE_SECONDARY_TARGET,FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(driver[6].ped,<<1628.71, 3242.08, 40.49>>,0.75,TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[6].ped,TRUE)
					SET_PED_ACCURACY(driver[6].ped,5)
					SET_PED_CHANCE_OF_FIRING_BLANKS(driver[6].ped,0.3,0.7)
					TASK_LOOK_AT_ENTITY(driver[6].ped,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
					
					//SET_PED_COMBAT_MOVEMENT(driver[6].ped,CM_STATIONARY)
					
					OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
						IF IS_PED_IN_ANY_VEHICLE(driver[6].ped)
							TASK_LEAVE_ANY_VEHICLE(NULL,GET_RANDOM_INT_IN_RANGE(0,800),ECF_DONT_CLOSE_DOOR)
						ENDIF
						
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID(),COMBAT_PED_PREVENT_CHANGING_TARGET)
					CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
					TASK_PERFORM_SEQUENCE(driver[6].ped,TASK_SEQUENCE)	
					CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
				ENDIF
				
				IF NOT IS_PED_INJURED(driver[7].ped)
					SET_PED_COMBAT_ATTRIBUTES(driver[7].ped,CA_USE_VEHICLE,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(driver[7].ped,CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(driver[7].ped,CA_DISABLE_SECONDARY_TARGET,FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(driver[7].ped,<<1627.43, 3239.20, 40.46>>,0.75,TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[7].ped,TRUE)
					TASK_LOOK_AT_ENTITY(driver[7].ped,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
					SET_PED_ACCURACY(driver[7].ped,5)
						SET_PED_CHANCE_OF_FIRING_BLANKS(driver[7].ped,0.3,0.7)
					
					//SET_PED_COMBAT_MOVEMENT(driver[7].ped,CM_STATIONARY)
					OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
						IF IS_PED_IN_ANY_VEHICLE(driver[7].ped)
							TASK_LEAVE_ANY_VEHICLE(NULL,GET_RANDOM_INT_IN_RANGE(0,800),ECF_DONT_CLOSE_DOOR)
						ENDIF
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID(),COMBAT_PED_PREVENT_CHANGING_TARGET)
					CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
					TASK_PERFORM_SEQUENCE(driver[7].ped,TASK_SEQUENCE)	
					CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
				ENDIF
				
				IF NOT IS_PED_INJURED(driver[8].ped)
					SET_PED_COMBAT_ATTRIBUTES(driver[8].ped,CA_USE_VEHICLE,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(driver[8].ped,CA_DISABLE_SECONDARY_TARGET,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(driver[8].ped,CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS,FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(driver[8].ped,<<1626.23, 3245.75, 40.55>>,0.75,TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[8].ped,TRUE)
					TASK_LOOK_AT_ENTITY(driver[8].ped,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
					SET_PED_ACCURACY(driver[8].ped,5)
						SET_PED_CHANCE_OF_FIRING_BLANKS(driver[8].ped,0.3,0.7)
					//SET_PED_COMBAT_MOVEMENT(driver[8].ped,CM_STATIONARY)
					OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
						IF IS_PED_IN_ANY_VEHICLE(driver[8].ped)
							TASK_LEAVE_ANY_VEHICLE(NULL,GET_RANDOM_INT_IN_RANGE(0,500),ECF_DONT_CLOSE_DOOR)
						ENDIF
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID(),COMBAT_PED_PREVENT_CHANGING_TARGET)
					CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
					TASK_PERFORM_SEQUENCE(driver[8].ped,TASK_SEQUENCE)	
					CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
				ENDIF
				
				IF NOT IS_PED_INJURED(driver[9].ped)
					SET_PED_COMBAT_ATTRIBUTES(driver[9].ped,CA_USE_VEHICLE,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(driver[9].ped,CA_DISABLE_SECONDARY_TARGET,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(driver[9].ped,CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS,FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(driver[9].ped,<<1628.35, 3244.15, 40.52>>,0.75,TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[9].ped,TRUE)
					TASK_LOOK_AT_ENTITY(driver[9].ped,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
					SET_PED_ACCURACY(driver[9].ped,5)
					SET_PED_CHANCE_OF_FIRING_BLANKS(driver[9].ped,0.3,0.7)
					//SET_PED_COMBAT_MOVEMENT(driver[9].ped,CM_STATIONARY)
					OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
						IF IS_PED_IN_ANY_VEHICLE(driver[9].ped)
							TASK_LEAVE_ANY_VEHICLE(NULL,GET_RANDOM_INT_IN_RANGE(0,1000),ECF_DONT_CLOSE_DOOR)
						ENDIF
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID(),COMBAT_PED_PREVENT_CHANGING_TARGET)
					CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
					TASK_PERFORM_SEQUENCE(driver[9].ped,TASK_SEQUENCE)	
					CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
				ENDIF
				driver[6].actionFlag = 0
				driver[7].actionFlag = 0
				driver[8].actionFlag = 0
				driver[9].actionFlag = 0
				SET_LABEL_AS_TRIGGERED("TRIGGER FINAL LINE OF ENEMIES",TRUE)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(driver[6].ped)
			IF driver[6].actionFlag != 99
				IF IS_ENTITY_AT_COORD(driver[6].ped,<<1628.71, 3242.08, 40.49>>,<<0.75,0.75,0.75>>)
					SET_PED_COMBAT_MOVEMENT(driver[6].ped,CM_STATIONARY)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[6].ped,TRUE)
					driver[6].actionFlag = 99
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(driver[7].ped)
			IF driver[7].actionFlag != 99
				IF IS_ENTITY_AT_COORD(driver[7].ped,<<1627.43, 3239.20, 40.46>>,<<0.75,0.75,0.75>>)
					SET_PED_COMBAT_MOVEMENT(driver[7].ped,CM_STATIONARY)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[7].ped,TRUE)
					driver[7].actionFlag = 99
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(driver[8].ped)
			IF driver[8].actionFlag != 99
				IF IS_ENTITY_AT_COORD(driver[8].ped,<<1626.23, 3245.75, 40.55>>,<<0.75,0.75,0.75>>)
					SET_PED_COMBAT_MOVEMENT(driver[8].ped,CM_STATIONARY)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[8].ped,TRUE)
					driver[8].actionFlag = 99
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(driver[9].ped)
			IF driver[9].actionFlag != 99
				IF IS_ENTITY_AT_COORD(driver[9].ped,<<1628.35, 3244.15, 40.52>>,<<0.75,0.75,0.75>>)
					SET_PED_COMBAT_MOVEMENT(driver[9].ped,CM_STATIONARY)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[9].ped,TRUE)
					driver[9].actionFlag = 99
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF


ENDPROC

TEXT_LABEL_23 nameLabel = "Enemy "
BOOL bSplat = FALSE

// "move_action@p_m_zero@armed@2h_short@idle@high_energy@a"

PROC MANAGE_RUNWAY_ENEMIES()
	
	//ENEMY KILLED BY PROP
	IF bpeddieplane[0]
		REQUEST_SCRIPT_AUDIO_BANK("TREVOR_2_BIKER_GRIND")
		REQUEST_ANIM_DICT("misstrevor2hangar_death")
		REQUEST_PTFX_ASSET()
		IF REQUEST_SCRIPT_AUDIO_BANK("TREVOR_2_BIKER_GRIND")
		AND HAS_ANIM_DICT_LOADED("misstrevor2hangar_death")
		AND HAS_PTFX_ASSET_LOADED()
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF DOES_ENTITY_EXIST(enemy[12].ped)
					IF NOT IS_PED_INJURED(enemy[12].ped)
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdBikerActions)
							sceneIdBikerActions = CREATE_SYNCHRONIZED_SCENE(<< 1728.345, 3288.885, 41.175 >>, << 0.000, 0.000, -75.520 >>)
							TASK_SYNCHRONIZED_SCENE (enemy[12].ped, sceneIdBikerActions, "misstrevor2hangar_death", "hangar_death", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdBikerActions,FALSE)
							SET_PED_CURRENT_WEAPON_VISIBLE(enemy[12].ped,TRUE)
							SET_SYNCHRONIZED_SCENE_RATE(sceneIdBikerActions,ssPlayback)
							SET_ENTITY_HEALTH(enemy[12].ped,120)
							SET_PED_SUFFERS_CRITICAL_HITS(enemy[12].ped,FALSE)
							SET_ENTITY_IS_TARGET_PRIORITY(enemy[12].ped,FALSE)
							PRINTSTRING("START SS")PRINTNL()
							bSplat = FALSE
						ELSE
							IF HAS_PTFX_ASSET_LOADED()
								IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdBikerActions)
									IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdBikerActions) > 0.965
									OR IS_ENTITY_TOUCHING_ENTITY(enemy[12].ped,mission_veh[1].veh)
										IF bSplat = FALSE
											START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_rotor_blade_blood", enemy[12].ped, <<0,0,0>>, <<0,0,0>>, BONETAG_HEAD,4)
											PLAY_SOUND_FROM_ENTITY(-1, "BIKER_PROP_GRIND", enemy[12].ped,"TREVOR_2_SOUNDS") 
											EXPLODE_PED_HEAD(enemy[12].ped)
											PRINTSTRING("SCREEEEAM")PRINTNL()
											bSplat = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								REQUEST_PTFX_ASSET()
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_DEAD(enemy[12].ped)
							IF IS_ENTITY_TOUCHING_ENTITY(enemy[12].ped,mission_veh[1].veh)
								IF bSplat = FALSE
									START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_rotor_blade_blood", enemy[12].ped, <<0,0,0>>, <<0,0,0>>, BONETAG_HEAD,4)
									PLAY_SOUND_FROM_ENTITY(-1, "BIKER_PROP_GRIND", enemy[12].ped,"TREVOR_2_SOUNDS") 
									EXPLODE_PED_HEAD(enemy[12].ped)
									PRINTSTRING("SCREEEEAM")PRINTNL()
									bSplat = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CREATE_ENEMIES(12,12,FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("APPLY FORCE TO ROTOR PED")
		IF NOT IS_PED_INJURED(enemy[12].ped)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(enemy[12].ped,PLAYER_PED_ID())
				OR IS_ENTITY_TOUCHING_ENTITY(enemy[12].ped,mission_veh[1].veh)
					APPLY_FORCE_TO_ENTITY(enemy[12].ped,APPLY_TYPE_IMPULSE,<<0,3,0>>,<<0,0,0>>,0,FALSE,FALSE,TRUE)
					SET_ENTITY_HEALTH(enemy[12].ped,0)
					SET_LABEL_AS_TRIGGERED("APPLY FORCE TO ROTOR PED",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//HEXER SPAWNED THAT COMES FROM BEHIND -VEHICLE: BIKE
	IF NOT HAS_LABEL_BEEN_TRIGGERED("HEXER BEHIND TRIGGER")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[7].veh)
			IF IS_ENTITY_AT_COORD(mission_veh[1].veh,<< 1680.0447, 3244.7361, 39.4909 >> ,<<25,25,25>>)
				IF NOT IS_SPHERE_VISIBLE(<<1742.47, 3270.15, 40.64>>,1.0)
					IF IS_VEHICLE_SEAT_FREE(mission_veh[7].veh,VS_DRIVER)
						driver[11].ped = CREATE_PED_INSIDE_VEHICLE(mission_veh[7].veh,PEDTYPE_MISSION,G_M_Y_LOST_01)
						ADD_DEADPOOL_TRIGGER(driver[11].ped, TRV2_KILLS)
					ENDIF
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[7].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[7].veh)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[7].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE(mission_veh[7].veh,9,"RCSJC") 
						SET_PLAYBACK_SPEED(mission_veh[7].veh,0.7)
					ENDIF
					IF NOT IS_PED_INJURED(driver[11].ped)
						SET_PED_RELATIONSHIP_GROUP_HASH(driver[11].ped ,rel_group_enemies)
						GIVE_WEAPON_TO_PED(driver[11].ped , WEAPONTYPE_SAWNOFFSHOTGUN , 999, TRUE)  
						SET_CURRENT_PED_WEAPON(driver[11].ped,WEAPONTYPE_SAWNOFFSHOTGUN,TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[11].ped,TRUE)
						SET_ENTITY_HEALTH(driver[11].ped ,110)
						IF IS_PED_IN_ANY_VEHICLE(driver[11].ped)
							IF NOT IS_PED_INJURED(psRon.pedIndex)
								TASK_DRIVE_BY(driver[11].ped,psRon.pedIndex,NULL,<<0,0,0>>,100,20)
								SET_PED_ACCURACY(driver[11].ped,1)
							ENDIF
						ENDIF
					ENDIF
					PRINTSTRING("HEXER BEHIND TRIGGER")PRINTNL()
					SET_LABEL_AS_TRIGGERED("HEXER BEHIND TRIGGER",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_INJURED(driver[11].ped)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[7].veh)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[7].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[7].veh)
					APPLY_FORCE_TO_ENTITY(mission_veh[7].veh,APPLY_TYPE_EXTERNAL_IMPULSE,<<2,2,0>>,<<0,0,0>>,0,TRUE,TRUE,TRUE)
					PRINTNL()	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//HEXER -COMING RIGHT AT THE PLANE AT THE START -VEHICLE: BIKE
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER BIKER BY BOX")
		 IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[14].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[14].veh)
						IF DOES_ENTITY_EXIST(driver[14].ped)
							IF NOT IS_PED_INJURED(driver[14].ped)
								IF IS_PED_IN_ANY_VEHICLE(driver[14].ped)
									REQUEST_VEHICLE_RECORDING(38,"RCSJC")
									IF HAS_VEHICLE_RECORDING_BEEN_LOADED(38,"RCSJC")
										ADD_DEADPOOL_TRIGGER(driver[14].ped, TRV2_KILLS)
										SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(driver[14].ped,KNOCKOFFVEHICLE_DEFAULT)
										SET_PED_ACCURACY(driver[14].ped,5)
										SET_ENTITY_HEALTH(driver[14].ped ,110)
										GIVE_WEAPON_TO_PED(driver[14].ped , WEAPONTYPE_PISTOL , 999, TRUE)
										SET_CURRENT_PED_WEAPON(driver[14].ped, WEAPONTYPE_PISTOL,TRUE)
										TASK_DRIVE_BY(driver[14].ped,psRon.pedIndex,NULL,<<0,0,0>>,400,80,TRUE)
										SET_PED_RELATIONSHIP_GROUP_HASH(driver[14].ped ,rel_group_enemies)
										START_PLAYBACK_RECORDED_VEHICLE(mission_veh[14].veh,38,"RCSJC")
										SET_PLAYBACK_SPEED(mission_veh[14].veh,1.0)
										IF IS_VEHICLE_DRIVEABLE(mission_veh[14].veh)
											ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[14].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
										ENDIF
										PRINTSTRING("TRIGGER BIKER BY BOX")PRINTNL()
										SET_LABEL_AS_TRIGGERED("TRIGGER BIKER BY BOX",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//HEX FROM BEHIND - VEHICLE: HEX
	IF NOT HAS_LABEL_BEEN_TRIGGERED("FROM BEHIND BIKER")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			REQUEST_VEHICLE_RECORDING(36,"RCSJC")
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(36,"RCSJC")
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
					IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 16153.051758
						IF IS_VEHICLE_DRIVEABLE(mission_veh[13].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[13].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[13].veh,36,"RCSJC",ENUM_TO_INT(TURN_ON_ENGINE_INCLUDING_START),0,DRIVINGMODE_AVOIDCARS)
							ELSE
								ADD_DEADPOOL_TRIGGER(driver[13].ped, TRV2_KILLS)
								SET_ENTITY_PROOFS(mission_veh[13].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
								SET_ENTITY_HEALTH(driver[13].ped ,110)
								SET_PLAYBACK_SPEED(mission_veh[13].veh,1.0)
								PRINTSTRING("FROM BEHIND BIKER")PRINTNL()
								IF IS_PED_IN_ANY_VEHICLE(driver[13].ped)
									IF NOT IS_PED_INJURED(driver[13].ped)
										TASK_DRIVE_BY(driver[13].ped,psRon.pedIndex,NULL,<<0,0,0>>,400,80,TRUE)
									ENDIF
								ENDIF
								SET_LABEL_AS_TRIGGERED("FROM BEHIND BIKER",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//HEX FROM THE RUNWAY - VEHICLE: HEX
	IF NOT HAS_LABEL_BEEN_TRIGGERED("RUNWAY BIKER")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 4283.404785
					IF IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
						IF NOT IS_PED_INJURED(driver[24].ped)
							IF NOT IS_PED_INJURED(psRon.pedIndex)
								ADD_DEADPOOL_TRIGGER(driver[24].ped, TRV2_KILLS)
								SET_ENTITY_HEALTH(driver[24].ped ,110)
								SET_PED_ACCURACY(driver[24].ped,5)
								IF IS_PED_IN_ANY_VEHICLE(driver[24].ped)
									TASK_DRIVE_BY(driver[24].ped,psRon.pedIndex,NULL,<<0,0,0>>,400,80,TRUE)
								ENDIF
								SET_PED_RELATIONSHIP_GROUP_HASH(driver[24].ped ,rel_group_enemies)
								GIVE_WEAPON_TO_PED(driver[24].ped , WEAPONTYPE_PISTOL , 999, TRUE)  
								SET_CURRENT_PED_WEAPON(driver[24].ped, WEAPONTYPE_PISTOL,TRUE)
							ENDIF
						ENDIF
						
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[24].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[24].veh,34,"RCSJC")
							IF IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[24].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
							ENDIF
						ELSE
							IF NOT IS_PED_INJURED(driver[24].ped)
								IF IS_PED_IN_ANY_VEHICLE(driver[24].ped)
									TASK_DRIVE_BY(driver[24].ped,psRon.pedIndex,NULL,<<0,0,0>>,400,30,TRUE)
								ENDIF
							ENDIF
							SET_ENTITY_PROOFS(mission_veh[24].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
							SET_PLAYBACK_SPEED(mission_veh[24].veh,1.0)
							PRINTSTRING("RUNWAY BIKER")PRINTNL()
							SET_LABEL_AS_TRIGGERED("RUNWAY BIKER",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
			IF NOT IS_PED_INJURED(driver[24].ped)
				IF IS_PED_IN_VEHICLE(driver[24].ped,mission_veh[24].veh)
					IF IS_ENTITY_AT_COORD(driver[24].ped,<<1612.90, 3231.55, 39.91>>,<<2,2,2>>)
						IF GET_ENTITY_SPEED(mission_veh[24].veh) < 1
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER CERTAIN DEATH")
								IF NOT IS_PED_INJURED(driver[24].ped)
									SET_PED_ACCURACY(driver[24].ped,100)
									IF IS_PED_IN_ANY_VEHICLE(driver[24].ped)
										TASK_DRIVE_BY(driver[24].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,400,100,TRUE)
									ENDIF
									SET_LABEL_AS_TRIGGERED("TRIGGER CERTAIN DEATH",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//LEFT HAND SIDE -VEHICLE: VAN
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER LEFT HAND SIDE VAN FIRST")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 8412.053223 //7912.053223
					IF IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[25].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[25].veh,33,"RCSJC")
						ELSE
							IF NOT IS_PED_INJURED(driver[25].ped)
								ADD_DEADPOOL_TRIGGER(driver[25].ped, TRV2_KILLS)
								SET_PED_RELATIONSHIP_GROUP_HASH(driver[25].ped ,rel_group_enemies)
								GIVE_WEAPON_TO_PED(driver[25].ped , WEAPONTYPE_CARBINERIFLE, 999, TRUE)  
								SET_CURRENT_PED_WEAPON(driver[25].ped,WEAPONTYPE_CARBINERIFLE,TRUE)
								SET_PED_ACCURACY(driver[25].ped,1)
							ENDIF
							IF NOT IS_PED_INJURED(driver[26].ped)
								IF NOT IS_PED_INJURED(psRon.pedIndex)
									IF IS_PED_IN_ANY_VEHICLE(driver[26].ped)
										TASK_DRIVE_BY(driver[26].ped,psRon.pedIndex,NULL,<<0,0,0>>,100,80,TRUE)
									ENDIF
									ADD_DEADPOOL_TRIGGER(driver[26].ped, TRV2_KILLS)
									SET_PED_RELATIONSHIP_GROUP_HASH(driver[26].ped ,rel_group_enemies)
									GIVE_WEAPON_TO_PED(driver[26].ped , WEAPONTYPE_CARBINERIFLE, 999, TRUE)  
									SET_CURRENT_PED_WEAPON(driver[26].ped,WEAPONTYPE_CARBINERIFLE,TRUE)
									SET_PED_ACCURACY(driver[26].ped,1)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[27].ped)
								IF NOT IS_PED_INJURED(psRon.pedIndex)
									IF IS_PED_IN_ANY_VEHICLE(driver[27].ped)
										TASK_DRIVE_BY(driver[27].ped,psRon.pedIndex,NULL,<<0,0,0>>,100,80,TRUE)
									ENDIF
									ADD_DEADPOOL_TRIGGER(driver[27].ped, TRV2_KILLS)
									SET_PED_RELATIONSHIP_GROUP_HASH(driver[27].ped ,rel_group_enemies)
									GIVE_WEAPON_TO_PED(driver[27].ped , WEAPONTYPE_CARBINERIFLE, 999, TRUE)  
									SET_CURRENT_PED_WEAPON(driver[27].ped,WEAPONTYPE_CARBINERIFLE,TRUE)
									SET_PED_ACCURACY(driver[27].ped,1)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[28].ped)
								IF NOT IS_PED_INJURED(psRon.pedIndex)
									IF IS_PED_IN_ANY_VEHICLE(driver[28].ped)
										TASK_DRIVE_BY(driver[28].ped,psRon.pedIndex,NULL,<<0,0,0>>,100,80,TRUE)
									ENDIF
									ADD_DEADPOOL_TRIGGER(driver[28].ped, TRV2_KILLS)
									SET_PED_RELATIONSHIP_GROUP_HASH(driver[28].ped ,rel_group_enemies)
									GIVE_WEAPON_TO_PED(driver[28].ped , WEAPONTYPE_CARBINERIFLE, 999, TRUE)  
									SET_CURRENT_PED_WEAPON(driver[28].ped,WEAPONTYPE_CARBINERIFLE,TRUE)
									SET_PED_ACCURACY(driver[28].ped,1)
								ENDIF
							ENDIF

							SET_ENTITY_PROOFS(mission_veh[25].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
							SET_PLAYBACK_SPEED(mission_veh[25].veh,1.0)
							PRINTSTRING("TRIGGER LEFT HAND SIDE VAN FIRST")PRINTNL()
							SET_LABEL_AS_TRIGGERED("TRIGGER LEFT HAND SIDE VAN FIRST",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//FINAL VAN: VAN
//	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER FINAL VAN")
//		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
//				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 15237.837891
//					IF IS_VEHICLE_DRIVEABLE(mission_veh[27].veh)
//						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[27].veh)
//							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[27].veh,35,"RCSJC")
//						ELSE
//							SET_ENTITY_PROOFS(mission_veh[27].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
//							SET_PLAYBACK_SPEED(mission_veh[27].veh,1.0)
//							PRINTSTRING("TRIGGER FINAL VAN")PRINTNL()
//							SET_LABEL_AS_TRIGGERED("TRIGGER LEFT HAND SIDE VAN FIRST",TRUE)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF

	//RIGHT HAND SIDE -VEHICLE: VAN
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER RIGHT HAND SIDE VAN FIRST")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 4818.600098
					IF IS_VEHICLE_DRIVEABLE(mission_veh[8].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[8].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[8].veh,32,"RCSJC")
						ELSE
							IF IS_VEHICLE_SEAT_FREE(mission_veh[8].veh,VS_DRIVER)
								driver[6].ped = CREATE_PED_INSIDE_VEHICLE(mission_veh[8].veh,PEDTYPE_MISSION,G_M_Y_LOST_01)
								ADD_DEADPOOL_TRIGGER(driver[6].ped, TRV2_KILLS)
								nameLabel = "enemey 6"
								SET_PED_NAME_DEBUG(driver[6].ped ,nameLabel)
							ENDIF
							IF NOT IS_PED_INJURED(psRon.pedIndex)
								SET_ENTITY_HEALTH(driver[6].ped,135)
								SET_PED_RELATIONSHIP_GROUP_HASH(driver[6].ped ,rel_group_enemies)
								GIVE_WEAPON_TO_PED(driver[6].ped , WEAPONTYPE_CARBINERIFLE, 999, TRUE)  
								SET_CURRENT_PED_WEAPON(driver[6].ped,WEAPONTYPE_CARBINERIFLE,TRUE)
							ENDIF
							IF IS_VEHICLE_SEAT_FREE(mission_veh[8].veh,VS_FRONT_RIGHT)
								driver[7].ped = CREATE_PED_INSIDE_VEHICLE(mission_veh[8].veh,PEDTYPE_MISSION,G_M_Y_LOST_01,VS_FRONT_RIGHT)
								ADD_DEADPOOL_TRIGGER(driver[7].ped, TRV2_KILLS)
								nameLabel = "enemey 7"
								SET_PED_NAME_DEBUG(driver[7].ped ,nameLabel)
							ENDIF
							IF NOT IS_PED_INJURED(driver[7].ped)
								SET_ENTITY_HEALTH(driver[7].ped,135)
								SET_PED_RELATIONSHIP_GROUP_HASH(driver[7].ped ,rel_group_enemies)
								GIVE_WEAPON_TO_PED(driver[7].ped , WEAPONTYPE_CARBINERIFLE, 999, TRUE)
								SET_PED_ACCURACY(driver[7].ped,1)
								SET_CURRENT_PED_WEAPON(driver[7].ped,WEAPONTYPE_CARBINERIFLE,TRUE)
							ENDIF
							
							IF IS_VEHICLE_SEAT_FREE(mission_veh[8].veh,VS_BACK_LEFT)
								driver[8].ped = CREATE_PED_INSIDE_VEHICLE(mission_veh[8].veh,PEDTYPE_MISSION,G_M_Y_LOST_01,VS_BACK_LEFT)
								ADD_DEADPOOL_TRIGGER(driver[8].ped, TRV2_KILLS)
								nameLabel = "enemey 8"
								SET_PED_NAME_DEBUG(driver[8].ped ,nameLabel)
							ENDIF
							IF NOT IS_PED_INJURED(driver[8].ped)
								SET_ENTITY_HEALTH(driver[8].ped,135)
								SET_PED_RELATIONSHIP_GROUP_HASH(driver[8].ped ,rel_group_enemies)
								GIVE_WEAPON_TO_PED(driver[8].ped , WEAPONTYPE_CARBINERIFLE, 999, TRUE)
								SET_PED_ACCURACY(driver[8].ped,1)
								SET_CURRENT_PED_WEAPON(driver[8].ped,WEAPONTYPE_CARBINERIFLE,TRUE)
							ENDIF
							
							IF IS_VEHICLE_SEAT_FREE(mission_veh[8].veh,VS_BACK_RIGHT)
								driver[9].ped = CREATE_PED_INSIDE_VEHICLE(mission_veh[8].veh,PEDTYPE_MISSION,G_M_Y_LOST_01,VS_BACK_RIGHT)
								ADD_DEADPOOL_TRIGGER(driver[9].ped, TRV2_KILLS)
								nameLabel = "enemey 9"
								SET_PED_NAME_DEBUG(driver[9].ped ,nameLabel)
							ENDIF
							IF NOT IS_PED_INJURED(driver[9].ped)
								SET_ENTITY_HEALTH(driver[9].ped,135)
								SET_PED_RELATIONSHIP_GROUP_HASH(driver[9].ped ,rel_group_enemies)
								GIVE_WEAPON_TO_PED(driver[9].ped , WEAPONTYPE_CARBINERIFLE, 999, TRUE)
								SET_PED_ACCURACY(driver[9].ped,1)
								SET_CURRENT_PED_WEAPON(driver[9].ped,WEAPONTYPE_CARBINERIFLE,TRUE)
							ENDIF
							
							SET_ENTITY_PROOFS(mission_veh[8].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
							SET_PLAYBACK_SPEED(mission_veh[8].veh,1.0)
							PRINTSTRING("TRIGGER RIGHT HAND SIDE VAN FIRST")PRINTNL()
							SET_LABEL_AS_TRIGGERED("TRIGGER RIGHT HAND SIDE VAN FIRST",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//ON FOOT ENEMIES
	IF eMissionStage =  MISSION_STAGE_PLANE_WING
		IF NOT HAS_LABEL_BEEN_TRIGGERED("HIT GUY BY BOX TRIGGER")
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF IS_ENTITY_AT_COORD(mission_veh[1].veh,<< 1731.26, 3265.86, 41.78 >>,<<25,25,25>>)
					SET_LABEL_AS_TRIGGERED("HIT GUY BY BOX TRIGGER",TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bpeddieplane[1]
			IF NOT IS_PED_INJURED(enemy[13].ped)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF HAS_LABEL_BEEN_TRIGGERED("HIT GUY BY BOX TRIGGER")
						IF IS_ENTITY_ON_SCREEN(enemy[13].ped)
							SET_PED_COMBAT_MOVEMENT(enemy[13].ped,CM_WILLADVANCE)
							SET_PED_TARGET_LOSS_RESPONSE(enemy[13].ped,TLR_NEVER_LOSE_TARGET)
							OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,<< 1718.51, 3252.45, 40.14 >>,PLAYER_PED_ID(),PEDMOVE_SPRINT,TRUE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,200)
							CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
							TASK_PERFORM_SEQUENCE(enemy[13].ped, TASK_SEQUENCE)
							ADD_DEADPOOL_TRIGGER(enemy[13].ped, TRV2_KILLS)
							SET_PED_SPHERE_DEFENSIVE_AREA(enemy[13].ped,<<1718.51, 3252.45, 40.14>>,0.75,TRUE)
							enemy[13].bTriggerTask = TRUE
							bpeddieplane[1] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF enemy[13].actionFlag != 99
				IF NOT IS_PED_INJURED(enemy[13].ped)
					IF NOT IS_PED_INJURED(psRon.pedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemy[13].ped,FALSE)
						SET_PED_COMBAT_MOVEMENT(enemy[13].ped,CM_DEFENSIVE)
						TASK_COMBAT_PED(enemy[13].ped,psRon.pedIndex)
						enemy[13].actionFlag = 99
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF NOT HAS_LABEL_BEEN_TRIGGERED("HIT TANKER GUYS TRIGGER")
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF IS_ENTITY_AT_COORD(mission_veh[1].veh,<< 1721.33, 3256.41, 41.87 >>,<<15,15,15>>)
					SET_LABEL_AS_TRIGGERED("HIT TANKER GUYS TRIGGER",TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER GUYS BY TANKER")
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF DOES_ENTITY_EXIST(enemy[14].ped)
				AND DOES_ENTITY_EXIST(enemy[16].ped)
					IF HAS_LABEL_BEEN_TRIGGERED("HIT TANKER GUYS TRIGGER")
						IF IS_ENTITY_ON_SCREEN(enemy[14].ped)
							IF NOT IS_PED_INJURED(enemy[14].ped)
								ADD_DEADPOOL_TRIGGER(enemy[14].ped, TRV2_KILLS)
								SET_PED_COMBAT_MOVEMENT(enemy[14].ped,CM_WILLADVANCE)
								SET_PED_TARGET_LOSS_RESPONSE(enemy[14].ped,TLR_NEVER_LOSE_TARGET)
								OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_AIM_GUN_AT_ENTITY(NULL,mission_veh[1].veh,1200)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,<< 1687.45, 3262.98, 40.87 >>,PLAYER_PED_ID(),PEDMOVE_RUN,TRUE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,200)
								CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_PERFORM_SEQUENCE(enemy[14].ped, TASK_SEQUENCE)
								SET_PED_SPHERE_DEFENSIVE_AREA(enemy[14].ped,<<1687.45, 3262.98, 40.87>>,0.75,TRUE)
								enemy[14].bTriggerTask = TRUE
							ENDIF
							
							IF NOT IS_PED_INJURED(enemy[16].ped)
								ADD_DEADPOOL_TRIGGER(enemy[16].ped, TRV2_KILLS)
								SET_PED_COMBAT_MOVEMENT(enemy[16].ped,CM_WILLADVANCE)
								SET_PED_TARGET_LOSS_RESPONSE(enemy[16].ped,TLR_NEVER_LOSE_TARGET)
								OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,<< 1697.17, 3265.93, 41.03 >>,PLAYER_PED_ID(),PEDMOVE_RUN,TRUE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,200)
								CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
								TASK_PERFORM_SEQUENCE(enemy[16].ped, TASK_SEQUENCE)
								
								SET_PED_SPHERE_DEFENSIVE_AREA(enemy[16].ped,<<1697.17, 3265.93, 41.03>>,0.75,TRUE)
								enemy[16].bTriggerTask = TRUE
							ENDIF
							SET_LABEL_AS_TRIGGERED("TRIGGER GUYS BY TANKER",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

	IF HAS_LABEL_BEEN_TRIGGERED("FROM BEHIND BIKER")
		EXIT_VEHICLE_TO_FIGHT(mission_veh[13].veh)
	ENDIF
	
	IF HAS_LABEL_BEEN_TRIGGERED("TRIGGER BIKER BY BOX")
		EXIT_VEHICLE_TO_FIGHT(mission_veh[14].veh)
	ENDIF
	
	IF HAS_LABEL_BEEN_TRIGGERED("RUNWAY BIKER")
		EXIT_VEHICLE_TO_FIGHT(mission_veh[24].veh)
	ENDIF
	
	IF HAS_LABEL_BEEN_TRIGGERED("TRIGGER RIGHT HAND SIDE VAN FIRST")
		//EXIT_VEHICLE_TO_FIGHT(mission_veh[8].veh)
		MANAGE_VEHICLE_COMBAT_RESPONSE_FINAL()
	ENDIF
	
	IF HAS_LABEL_BEEN_TRIGGERED("TRIGGER LEFT HAND SIDE VAN FIRST")
		EXIT_VEHICLE_TO_FIGHT(mission_veh[25].veh)
	ENDIF
	
	IF HAS_LABEL_BEEN_TRIGGERED("TRIGGER FINAL VAN")
		EXIT_VEHICLE_TO_FIGHT(mission_veh[27].veh)
	ENDIF

ENDPROC

INT iVehicleBlowUpTimer8
INT iVehicleBlowUpSetPiece8

PROC MANAGE_BLOW_UP_VEHICLE8()
	
	SWITCH iVehicleBlowUpSetPiece8
		
		CASE 0
			iVehicleBlowUpSetPiece8 ++
		BREAK

		CASE 1
			IF NOT IS_ENTITY_DEAD(mission_veh[8].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[8].veh,PLAYER_PED_ID())
					IF GET_ENTITY_HEALTH(mission_veh[8].veh) < 1000
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[8].veh,500)
						SET_ENTITY_HEALTH(mission_veh[8].veh,500)
						iVehicleBlowUpSetPiece8 ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_ENTITY_DEAD(mission_veh[8].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[8].veh,PLAYER_PED_ID())
					IF GET_ENTITY_HEALTH(mission_veh[8].veh) < 500
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[8].veh,400)
						SET_ENTITY_HEALTH(mission_veh[8].veh,400)
						iVehicleBlowUpSetPiece8 ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_ENTITY_DEAD(mission_veh[8].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[8].veh,PLAYER_PED_ID())
					IF GET_ENTITY_HEALTH(mission_veh[8].veh) < 400
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[8].veh,300)
						SET_ENTITY_HEALTH(mission_veh[8].veh,300)
						iVehicleBlowUpSetPiece8 ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_ENTITY_DEAD(mission_veh[8].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[8].veh,PLAYER_PED_ID())
					IF GET_ENTITY_HEALTH(mission_veh[8].veh) < 300
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[8].veh,200)
						SET_ENTITY_HEALTH(mission_veh[8].veh,200)
						iVehicleBlowUpSetPiece8 ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_ENTITY_DEAD(mission_veh[8].veh)
				IF GET_ENTITY_HEALTH(mission_veh[8].veh) < 200
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[8].veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[8].veh)
					ENDIF
					SET_VEHICLE_PETROL_TANK_HEALTH(mission_veh[8].veh,-100)
					SET_VEHICLE_ENGINE_HEALTH(mission_veh[8].veh,-500)
					SET_HORN_PERMANENTLY_ON_TIME(mission_veh[8].veh,GET_RANDOM_FLOAT_IN_RANGE(2000,6000))
					iVehicleBlowUpTimer8 = GET_GAME_TIMER()
					iVehicleBlowUpSetPiece8 ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_ENTITY_DEAD(mission_veh[8].veh)
				IF NOT MANAGE_MY_TIMER(iVehicleBlowUpTimer8,600)
					APPLY_FORCE_TO_ENTITY(mission_veh[8].veh, APPLY_TYPE_FORCE, <<GET_RANDOM_FLOAT_IN_RANGE(33.0,48.0), 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
					iVehicleBlowUpTimer8 = GET_GAME_TIMER()
					iVehicleBlowUpSetPiece8 ++
				ENDIF
			ENDIF

		BREAK
		
		CASE 7
			IF MANAGE_MY_TIMER(iVehicleBlowUpTimer8,GET_RANDOM_INT_IN_RANGE(600,2000))
				IF NOT IS_ENTITY_DEAD(mission_veh[8].veh)
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(mission_veh[8].veh)
						IF GET_RANDOM_BOOL()
							EXPLODE_VEHICLE(mission_veh[8].veh)
							iVehicleBlowUpSetPiece8 ++
						ELSE
							iVehicleBlowUpSetPiece8 ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

INT iVehicleBlowUpSetPiece25
INT iVehicleBlowUpTimer25

PROC MANAGE_BLOW_UP_VEHICLE25()
	
	SWITCH iVehicleBlowUpSetPiece25
		
		CASE 0
			iVehicleBlowUpSetPiece25 ++
		BREAK

		CASE 1
			IF NOT IS_ENTITY_DEAD(mission_veh[25].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[25].veh,PLAYER_PED_ID())
					IF GET_ENTITY_HEALTH(mission_veh[25].veh) < 1000
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[25].veh,800)
						SET_ENTITY_HEALTH(mission_veh[25].veh,800)
						iVehicleBlowUpSetPiece25 ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_ENTITY_DEAD(mission_veh[25].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[25].veh,PLAYER_PED_ID())
					IF GET_ENTITY_HEALTH(mission_veh[25].veh) < 800
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[25].veh,400)
						SET_ENTITY_HEALTH(mission_veh[25].veh,400)
						iVehicleBlowUpSetPiece25 ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_ENTITY_DEAD(mission_veh[25].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[25].veh,PLAYER_PED_ID())
					IF GET_ENTITY_HEALTH(mission_veh[25].veh) < 400
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[25].veh,300)
						SET_ENTITY_HEALTH(mission_veh[25].veh,300)
						iVehicleBlowUpSetPiece25 ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_ENTITY_DEAD(mission_veh[25].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[25].veh,PLAYER_PED_ID())
					IF GET_ENTITY_HEALTH(mission_veh[25].veh) < 300
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[25].veh,200)
						SET_ENTITY_HEALTH(mission_veh[25].veh,200)
						iVehicleBlowUpSetPiece25 ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_ENTITY_DEAD(mission_veh[25].veh)
				IF GET_ENTITY_HEALTH(mission_veh[25].veh) < 200
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[25].veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[25].veh)
					ENDIF
					SET_VEHICLE_PETROL_TANK_HEALTH(mission_veh[25].veh,-100)
					SET_VEHICLE_ENGINE_HEALTH(mission_veh[25].veh,-500)
					SET_HORN_PERMANENTLY_ON_TIME(mission_veh[25].veh,GET_RANDOM_FLOAT_IN_RANGE(2000,6000))
					iVehicleBlowUpTimer25 = GET_GAME_TIMER()
					iVehicleBlowUpSetPiece25 ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_ENTITY_DEAD(mission_veh[25].veh)
				IF NOT MANAGE_MY_TIMER(iVehicleBlowUpTimer25,600)
					APPLY_FORCE_TO_ENTITY(mission_veh[25].veh, APPLY_TYPE_FORCE, <<GET_RANDOM_FLOAT_IN_RANGE(28.0,34.0), 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
					iVehicleBlowUpTimer25 = GET_GAME_TIMER()
					iVehicleBlowUpSetPiece25 ++
				ENDIF
			ENDIF

		BREAK
		
		CASE 7
			IF MANAGE_MY_TIMER(iVehicleBlowUpTimer25,GET_RANDOM_INT_IN_RANGE(600,2000))
				IF NOT IS_ENTITY_DEAD(mission_veh[25].veh)
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(mission_veh[25].veh)
						IF GET_RANDOM_BOOL()
							EXPLODE_VEHICLE(mission_veh[25].veh)
							iVehicleBlowUpSetPiece25 ++
						ELSE
							iVehicleBlowUpSetPiece25 ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

//INT iTimeOnRunway

PROC MANAGE_ENEMY_AGGRESSION_ON_RUNWAY()
	IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
		IF IS_PED_INJURED(driver[6].ped) AND IS_PED_INJURED(driver[7].ped)
		OR IS_PED_INJURED(driver[7].ped) AND IS_PED_INJURED(driver[8].ped)
		OR IS_PED_INJURED(driver[8].ped) AND IS_PED_INJURED(driver[9].ped)
		OR IS_PED_INJURED(driver[6].ped) AND IS_PED_INJURED(driver[9].ped)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
					IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 27007.259766
						IF NOT IS_PED_INJURED(driver[6].ped)
							IF driver[6].bCharge = FALSE
								SET_PED_SPHERE_DEFENSIVE_AREA(driver[6].ped,<<1618.61, 3239.81, 40.50>>,20,TRUE)
								SET_PED_COMBAT_MOVEMENT(driver[6].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(driver[6].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(driver[6].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
								PRINTSTRING("driver 6 charging")PRINTNL()
								driver[6].bCharge = TRUE
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(driver[7].ped)
							IF driver[7].bCharge = FALSE
								SET_PED_SPHERE_DEFENSIVE_AREA(driver[7].ped,<<1617.02, 3237.31, 40.48>>,20,TRUE)
								SET_PED_COMBAT_MOVEMENT(driver[7].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(driver[7].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(driver[7].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
								PRINTSTRING("driver 7 charging")PRINTNL()
								driver[7].bCharge = TRUE
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(driver[8].ped)
							IF driver[8].bCharge = FALSE
								SET_PED_SPHERE_DEFENSIVE_AREA(driver[8].ped,<<1625.80, 3242.82, 40.52>>,20,TRUE)
								SET_PED_COMBAT_MOVEMENT(driver[8].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(driver[8].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(driver[8].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
								PRINTSTRING("driver 8 charging")PRINTNL()
								driver[8].bCharge = TRUE
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(driver[9].ped)
							IF driver[9].bCharge = FALSE
								SET_PED_SPHERE_DEFENSIVE_AREA(driver[9].ped,<<1624.33, 3240.26, 40.49>>,20,TRUE)
								SET_PED_COMBAT_MOVEMENT(driver[9].ped,CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(driver[9].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(driver[9].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
								PRINTSTRING("driver 9 charging")PRINTNL()
								driver[9].bCharge = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_DEATH_ON_A_WING_ACHIEVEMENT()
	IF IS_PED_INJURED(enemy[12].ped)
	AND IS_PED_INJURED(enemy[11].ped)
	AND IS_PED_INJURED(enemy[13].ped)
	AND IS_PED_INJURED(enemy[14].ped)
	AND IS_PED_INJURED(enemy[16].ped)
	AND IS_PED_INJURED(driver[11].ped)
	AND IS_PED_INJURED(driver[13].ped)
	AND IS_PED_INJURED(driver[14].ped)
	AND IS_PED_INJURED(driver[25].ped)
	AND IS_PED_INJURED(driver[26].ped)
	AND IS_PED_INJURED(driver[27].ped)
	AND IS_PED_INJURED(driver[28].ped)
	AND IS_PED_INJURED(driver[6].ped)
	AND IS_PED_INJURED(driver[7].ped)
	AND IS_PED_INJURED(driver[8].ped)
	AND IS_PED_INJURED(driver[9].ped)
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TRV2_ALL_BIKERS_KILLED_ON_WING)
	ENDIF
ENDPROC

PROC PLANE_WING()
	
	IF iProgress > 0
		MANAGE_BLOW_UP_VEHICLE25()
		MANAGE_BLOW_UP_VEHICLE8()
		MANAGE_RUNWAY_ENEMIES()
		MANAGE_ENEMY_AGGRESSION_ON_RUNWAY()
		CLEANUP_OBJECT(osPropStickyBomb.ObjectIndex,  TRUE)
		
		IF DOES_ENTITY_EXIST(driver[14].ped)
			IF NOT IS_PED_INJURED(driver[14].ped)
				IF NOT IS_PED_IN_ANY_VEHICLE(driver[14].ped)
					IF GET_SCRIPT_TASK_STATUS(psRon.pedIndex,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(driver[14].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF iprogress > 6
		IF iprogress != 98
		AND iprogress != 99
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVOR_PLANE].veh)
					reason_for_fail = VEHICLE_CUBAN_ABADONED
					eMissionStage = MISSION_STAGE_FAIL
					PRINTSTRING("CUBAN LEFT")
					PRINTNL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[10].veh)
		DELETE_VEHICLE(mission_veh[10].veh)
		SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
	ENDIF

	IF DOES_ENTITY_EXIST(enemy[12].ped)
		IF IS_PED_INJURED(enemy[12].ped)
			REMOVE_ANIM_DICT("misstrevor2hangar_death")
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(mission_veh[2].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
			SETUP_MISSION_REQUIREMENT(REQ_TREVOR_PLANE,<< 2474.0779, 3477.0808, 48.1862 >>  ,191.3157)
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
				PRINTSTRING("POSITION IN RECORDING:")PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(mission_veh[1].veh))PRINTNL()
			ENDIF
		ENDIF
	#ENDIF

	IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
//		SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	ENDIF
	
	IF bpeddieplane[2]
		manage_explosions()	
	ENDIF
	
	IF iProgress > 0
	AND iProgress < 5
		MANAGE_BIKER_RUNWAY_DIALOGUE()
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			REQUEST_COLLISION_AT_COORD(GET_ENTITY_COORDS(mission_veh[1].veh))
		ENDIF
	ENDIF
	
	IF bplaybackstarted[0] = TRUE
		MANAGE_VEHICLE_AGGRO()
		MANAGE_PED_ACTIONS()
	ENDIF
	
	IF iProgress > 1
		IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_WINGH")
			PRINT_HELP("SJC_WINGH")
			SET_LABEL_AS_TRIGGERED("SJC_WINGH",TRUE)
		ENDIF
	ENDIF
	
	IF iprogress > 7
		IF iprogress != 98
		AND iprogress != 99
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RUNW")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RUNW", CONV_PRIORITY_HIGH )
						SET_LABEL_AS_TRIGGERED("TR2_RUNW",TRUE)
						iRunWayTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ELSE
				IF MANAGE_MY_TIMER(iRunWayTimer,7000)
					SET_LABEL_AS_TRIGGERED("TR2_RUNW",FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iprogress > 6
		IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
			MODIFY_VEHICLE_TOP_SPEED(mission_veh[2].veh,60)
		ENDIF
	ENDIF
	
	IF iprogress > 0
	AND iprogress < 5
		DISPLAY_AMMO_THIS_FRAME(FALSE)
		IF NOT bcutsceneplaying
			MANAGE_RUNWAY_DIALOGUE()
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF DOES_ENTITY_EXIST(enemy[12].ped)
				IF IS_PED_INJURED(enemy[12].ped)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
						IF GET_TIME_POSITION_IN_RECORDING(mission_veh[1].veh) > 4818.600098
							PRINTSTRING("SETTING PLAYBACK TO 0.45")PRINTNL()
							SET_PLAYBACK_SPEED(mission_veh[1].veh,0.45)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bCanSwitchWeapons
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_DisableActionMode, TRUE)
	ENDIF
	
	IF iProgress > 0
		MANAGE_PLAYER_NUKE()
	ENDIF
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(enemy)
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(driver)

	MANAGE_PLAYER_DAMAGE_PLANE()

	SWITCH iProgress
	
		CASE 0
			ADD_CARREC_REQUEST_TO_ARRAY(1,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(2,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(7,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(9,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(13,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(23,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(24,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(25,"RCSJC")
			
			ADD_CARREC_REQUEST_TO_ARRAY(32,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(33,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(34,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(35,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(36,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(37,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(38,"RCSJC")
			
			ADD_MODEL_REQUEST_TO_ARRAY(G_M_Y_LOST_01)
			ADD_MODEL_REQUEST_TO_ARRAY(HEXER)
			ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
			ADD_MODEL_REQUEST_TO_ARRAY(TANKER)
			ADD_MODEL_REQUEST_TO_ARRAY(GBURRITO)
			
			REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
			REQUEST_VEHICLE_ASSET(HEXER)
			REMOVE_MODEL_FROM_ARRAY(MAVERICK)
			REQUEST_ANIM_DICT("misstrevor2ig_9b")
			REQUEST_ANIM_DICT("misstrevor2ig_10")
			
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_WALL_LIGHT_02A)
			
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Plane wing")
			bCommentOnPlaneBeingDamage = FALSE
			
			IF DOES_ENTITY_EXIST(crate[1])
				SET_ENTITY_HEALTH(crate[1],120)
			ENDIF
			
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
				DELETE_VEHICLE(mission_veh[2].veh)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
				SET_VEHICLE_STRONG(mission_veh[MV_TREVOR_PLANE].veh,TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(psRon.pedIndex)
				SET_ENTITY_HEALTH(psRon.pedIndex,1000)
				SET_PED_SUFFERS_CRITICAL_HITS(psRon.pedIndex,FALSE)
				SET_ENTITY_INVINCIBLE(psRon.pedIndex,FALSE)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(mission_veh[5].veh)
				SET_ENTITY_CAN_BE_DAMAGED(mission_veh[5].veh,TRUE)
				SET_VEHICLE_STRONG(mission_veh[5].veh,FALSE)
				SET_ENTITY_PROOFS(mission_veh[5].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_GET_TO_PLANE")
				STOP_AUDIO_SCENE("TREVOR_2_GET_TO_PLANE")
			ELSE
				START_AUDIO_SCENE("TREVOR_2_SHOOTOUT_ON_WING")
			ENDIF
			
			SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(),RBF_IMPACT_OBJECT)
			SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(),RBF_VEHICLE_IMPACT)

			IF IS_SCREEN_FADED_OUT()
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtCutscene)
							
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
				OR NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
				OR NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,120,TRUE,TRUE)
				ELSE
					IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
						IF wtCutscene != WEAPONTYPE_ASSAULTRIFLE
//						AND wtCutscene != WEAPONTYPE_ADVANCEDRIFLE
//						AND wtCutscene != WEAPONTYPE_CARBINERIFLE
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,TRUE)
						ENDIF
					ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
						IF wtCutscene != WEAPONTYPE_CARBINERIFLE
//						AND wtCutscene != WEAPONTYPE_ASSAULTRIFLE
//						AND wtCutscene != WEAPONTYPE_CARBINERIFLE
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE,TRUE)
						ENDIF
					ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
						IF wtCutscene != WEAPONTYPE_ADVANCEDRIFLE
//						AND wtCutscene != WEAPONTYPE_ADVANCEDRIFLE
//						AND wtCutscene != WEAPONTYPE_ASSAULTRIFLE
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE,TRUE)
						ENDIF
					ENDIF
				ENDIF
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			IF NOT IS_PED_INJURED(psRon.pedIndex)
				SET_PED_RELATIONSHIP_GROUP_HASH(psRon.pedIndex ,rel_group_buddies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,rel_group_buddies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_buddies,rel_group_enemies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,rel_group_buddies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,RELGROUPHASH_PLAYER)
			ENDIF
			
			IF DOES_ENTITY_EXIST(crate[1])
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(crate[1])
			ENDIF
			
			SETUP_PEDS_FOR_DIALOGUE()
			STREAMING_FIX()
			
			bSkipped = FALSE
			bCanSwitchWeapons = FALSE
			IF DOES_ENTITY_EXIST(crate[1])
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(crate[1])
			ENDIF
			iNuke = 0
			
			IF DOES_ENTITY_EXIST(crate[14])
				FREEZE_ENTITY_POSITION(crate[14],FALSE)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, FALSE)
			ENDIF
		
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
				IF wtCutscene != WEAPONTYPE_ASSAULTRIFLE
				AND wtCutscene != WEAPONTYPE_ADVANCEDRIFLE
				AND wtCutscene != WEAPONTYPE_CARBINERIFLE
					ADD_AMMO_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,iMAX(250, GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)))
				ENDIF
			ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
				IF wtCutscene != WEAPONTYPE_ASSAULTRIFLE
				AND wtCutscene != WEAPONTYPE_ADVANCEDRIFLE
				AND wtCutscene != WEAPONTYPE_CARBINERIFLE
					ADD_AMMO_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE,iMAX(250, GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)))
				ENDIF
			ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
				IF wtCutscene != WEAPONTYPE_ASSAULTRIFLE
				AND wtCutscene != WEAPONTYPE_ADVANCEDRIFLE
				AND wtCutscene != WEAPONTYPE_CARBINERIFLE
					ADD_AMMO_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE,iMAX(250, GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)))
				ENDIF
			ENDIF
			
			IF bSkipped = FALSE
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
					START_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,2,"RCSJC")
					PRINTLN("@@@@@@@@@@@ START_PLAYBACK_RECORDED_VEHICLE 1 @@@@@@@@@@")
					//0.55
					SET_PLAYBACK_SPEED(mission_veh[1].veh,0.6)
				ELSE
					//0.55
					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh)
					PRINTLN("@@@@@@@@@@@ START_PLAYBACK_RECORDED_VEHICLE 2 @@@@@@@@@@")
					SET_PLAYBACK_SPEED(mission_veh[1].veh,0.6)
				ENDIF
			ENDIF
			
			SET_PED_INFINITE_AMMO_CLIP(PLAYER_PED_ID(),TRUE)
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtCutscene)

			REQUEST_SCRIPT_AUDIO_BANK("TREVOR_2_BIKER_GRIND")
			REQUEST_ANIM_DICT("misstrevor2hangar_death")
			REQUEST_PTFX_ASSET()
			
			iRunWayTimer = GET_GAME_TIMER()
			bEnemyTasks[0] = FALSE
			bEnemyTasks[1] = FALSE
			bEnemyTasks[2] = FALSE
			bEnemyTasks[3] = FALSE
			iVehicleBlowUpSetPiece25 = 0
			iVehicleBlowUpSetPiece8 = 0
			brunfailchecks = TRUE
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),FALSE,-1)
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			IF IS_SCREEN_FADED_OUT()
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForcedAim,TRUE)
				CLEAR_AREA(<<1656.5217, 3246.6509, 39.5928>>,100,TRUE)
			ENDIF
			//iTimeOnRunway = GET_GAME_TIMER()
			iProgress = 98					
		BREAK
		
		CASE 98
			IF ARE_REQUESTED_CARRECS_LOADED("RCSJC")
				IF ARE_REQUESTED_MODELS_LOADED()
					IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_CARBINERIFLE)
						//FROM BEHIND
						CREATE_ENEMY_VEHICLE(13,HEXER,<< 1582.0737, 3365.7207, 36.4635 >>,90.0,G_M_Y_LOST_01,TRUE,13,0,FALSE,-1,WEAPONTYPE_PISTOL)
					
						//BY BOX
						CREATE_ENEMY_VEHICLE(14,HEXER,<< 1748.88, 3045.37, 61.45 >>,262.61,G_M_Y_LOST_01,TRUE,14,0,FALSE,-1,WEAPONTYPE_PISTOL)
						
						//FROM THE RUNWAY
						CREATE_ENEMY_VEHICLE(24,HEXER, <<1330.4536, 3159.9248, 39.4563>>,278.9820,G_M_Y_LOST_01,TRUE,24,0,FALSE,-1,WEAPONTYPE_SAWNOFFSHOTGUN)
						
						//VAN FROM LEFT BACK
						CREATE_ENEMY_VEHICLE(8,GBURRITO,<<1088.4746, 3145.3286, 39.4609>>,296.7920,G_M_Y_LOST_01,FALSE,0,0,FALSE,-1,WEAPONTYPE_PISTOL)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[8].veh, TRUE)
						//RIGHT HAND SIDE
						CREATE_ENEMY_VEHICLE(25,GBURRITO,<< 1417.43, 3006.30, 40.33 >>,283.3621,G_M_Y_LOST_01,TRUE,25,3)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[25].veh, TRUE)
						//ENEMIES COME OUT AND GO INFRONT OF PLANE
						//CREATE_ENEMY_VEHICLE(27,GBURRITO,<< 1548.6367, 3140.0676, 39.5341 >>,317.6045,G_M_Y_LOST_01,TRUE,25,3)
						
						//CREATE_ENEMIES(11,18,FALSE)
						IF NOT DOES_ENTITY_EXIST(enemy[11].ped)
							CREATE_ENEMIES(11,17,FALSE)
						ELSE
							CREATE_ENEMIES(12,17,FALSE) //[MF] Skipping enemy at index 11 if he was spawned during the plane steal cutscen B* 1763431
						ENDIF
						enemy[12].actionFlag = 0
						bpeddieplane[0] = TRUE
						IF NOT IS_PED_INJURED(driver[26].ped)
							DELETE_PED(driver[26].ped)
						ENDIF
						
						IF NOT IS_PED_INJURED(enemy[15].ped)
							DELETE_PED(enemy[15].ped)
						ENDIF
						
						iProgress = 99
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 99
			IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				IF TRIGGER_MUSIC_EVENT("TRV2_WING_PLANE")
					REQUEST_VEHICLE_RECORDING(2,"RCSJC")
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(2,"RCSJC")
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							HANG_UP_AND_PUT_AWAY_PHONE(TRUE)

							PRINTSTRING("ATTACH PLAYER TO WING")PRINTNL()
							ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(),mission_veh[1].veh,0, << 3.84078, 0.831592, 0.753682 >>,<<0,0,0>>,TRUE)
							      
							IF IS_SCREEN_FADED_OUT()
					            FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					            SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					            SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							ENDIF
							
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_SCRIPTED) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_SCRIPTED) != WAITING_TO_START_TASK
						     	IF IS_SCREEN_FADED_OUT()
						           TASK_AIM_GUN_SCRIPTED(PLAYER_PED_ID(),SCRIPTED_GUN_TASK_PLANE_WING,TRUE,TRUE)
						           PRINTSTRING("Forcing AI Update on skip")PRINTNL()
						      	ELSE
						        	TASK_AIM_GUN_SCRIPTED(PLAYER_PED_ID(),SCRIPTED_GUN_TASK_PLANE_WING)
						      	ENDIF
							ENDIF

							SET_VEHICLE_STRONG(mission_veh[1].veh,TRUE)
							SET_ENTITY_HEALTH(mission_veh[1].veh,2000)
							
							IF bSkipped = TRUE
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,2,"RCSJC")
									PRINTLN("@@@@@@@@@@@ START_PLAYBACK_RECORDED_VEHICLE 3 @@@@@@@@@@")
									//0.55
									SET_PLAYBACK_SPEED(mission_veh[1].veh,0.6)
								ELSE
									//0.55
									UNPAUSE_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh)
									PRINTLN("@@@@@@@@@@@ START_PLAYBACK_RECORDED_VEHICLE 4 @@@@@@@@@@")
									SET_PLAYBACK_SPEED(mission_veh[1].veh,0.6)
								ENDIF
							ENDIF
							
							REQUEST_VEHICLE_ASSET(HEXER)
							iProgress = 1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
		
			IF HAS_ANIM_DICT_LOADED("misstrevor2ig_9b")
			AND HAS_ANIM_DICT_LOADED("misstrevor2ig_10")
				IF ARE_REQUESTED_CARRECS_LOADED("RCSJC")
					IF ARE_REQUESTED_MODELS_LOADED()
						IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_CARBINERIFLE)
							IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
								IF NOT IS_PED_INJURED(psRon.pedIndex)
									IF NOT IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
										IF IS_VEHICLE_SEAT_FREE(mission_veh[1].veh)
											SET_PED_INTO_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
										ENDIF
									ELSE
										IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[1].veh) OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											IF IS_SCREEN_FADED_OUT()
												SET_GAMEPLAY_CAM_RELATIVE_HEADING()
											ENDIF
											
											ienemyAIprog = 1
											CLEANUP_LOADED_MODEL_ARRAY()
											itimer = GET_GAME_TIMER()
											CLEAN_OBJECTIVE_BLIP_DISPLAY()
											
											IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
												FREEZE_ENTITY_POSITION(mission_veh[2].veh,TRUE)
												SET_ENTITY_PROOFS(mission_veh[2].veh,FALSE,FALSE,FALSE,TRUE,TRUE)
												SET_VEHICLE_STRONG(mission_veh[2].veh,TRUE)
												SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[2].veh,TRUE)
											ENDIF
											
											IF NOT IS_PED_INJURED(enemy[11].ped)
												TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemy[11].ped,TRUE)
												SET_PED_CHANCE_OF_FIRING_BLANKS(enemy[11].ped, 0.5, 0.8)
												SET_PED_ACCURACY(enemy[11].ped,2)
												OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,<< 1742.7723, 3268.8376, 40.1982 >>,PLAYER_PED_ID(),PEDMOVE_RUN,TRUE,0.2,2)
													TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
												CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
												TASK_PERFORM_SEQUENCE(enemy[11].ped,TASK_SEQUENCE)
												CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
											ENDIF

											IF NOT IS_PED_INJURED(enemy[32].ped)
												SET_PED_CAN_BE_TARGETTED(enemy[32].ped,TRUE)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemy[32].ped,FALSE)
												SET_PED_COMBAT_MOVEMENT(enemy[32].ped,CM_DEFENSIVE)
												OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
													TASK_LEAVE_ANY_VEHICLE(NULL,2000)
													TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 1728.9, 3293, 40.9562 >>,30)
												CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
												TASK_PERFORM_SEQUENCE(enemy[32].ped,TASK_SEQUENCE)
												CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
											ENDIF
											iProgress++
										ELSE
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
	
		CASE 2
		
			IF MANAGE_MY_TIMER(itimer,1500)
				PRINTSTRING("Fading In")PRINTNL()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					WHILE IS_SCREEN_FADING_IN()
						DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
						WAIT(0)
						DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
					ENDWHILE
				ENDIF
			ENDIF
		
			IF (GET_GAME_TIMER() - itimer) > 5000

				bplaybackstarted[0] = TRUE
				bDialogue = FALSE
				iProgress++
			ENDIF
			
		BREAK
		
		CASE 3
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF IS_ENTITY_AT_COORD(mission_veh[1].veh,<< 1658.6034, 3243.4521, 40.8961 >>,<<15,15,15>>)
					PRINTSTRING("STEAL_PLANE - GOAL 5")
					PRINTNL()	
					REMOVE_CUTSCENE()
					REQUEST_CUTSCENE("TRV_2_MCS_6")
					REQUEST_VEHICLE_RECORDING(3,"RCSJC")
					iprogress++
				ENDIF
			ENDIF

		BREAK
		
		CASE 4
		
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<1594.4098, 3220.2776, 39.4116>>,<<15,15,15>>)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
						SET_PLAYBACK_SPEED(mission_veh[1].veh,0.6)
					ENDIF
					CLEANUP_LOADED_CARREC_ARRAY("RCSJC")
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[2].veh,TRUE)
						//MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[2].veh,<<0,0,0>>)
					ENDIF
					
					SETTIMERA(0)
					
					ADD_MODEL_REQUEST_TO_ARRAY(GBURRITO)
					ADD_MODEL_REQUEST_TO_ARRAY(HEXER)
					ADD_MODEL_REQUEST_TO_ARRAY(G_M_Y_LOST_01)
					ADD_CARREC_REQUEST_TO_ARRAY(3,"RCSJC")
					ADD_CARREC_REQUEST_TO_ARRAY(8,"RCSJC")
					ADD_CARREC_REQUEST_TO_ARRAY(14,"RCSJC")
					ADD_CARREC_REQUEST_TO_ARRAY(15,"RCSJC")
					ADD_CARREC_REQUEST_TO_ARRAY(16,"RCSJC")
					ADD_CARREC_REQUEST_TO_ARRAY(17,"RCSJC")
					
					MANAGE_DEATH_ON_A_WING_ACHIEVEMENT()
					
					e_section_stage = SECTION_STAGE_SETUP
					i_current_event = 0

					iprogress = 5
				ELSE
					IF MANAGE_MY_TIMER(iRunWayTimer,7000)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF IS_IT_SAFE_TO_PLAY_DIALOG()
								IF CREATE_CONVERSATION(sSpeech, "T2AUD", "TR_bp02", CONV_PRIORITY_LOW)
									iRunWayTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 5
			PRINTSTRING("RUNNING CUTSCENE")PRINTNL()
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForcedAim,FALSE)
			RUN_JUMP_OFF_WING_CUTSCENE()
			IF NOT IS_CUTSCENE_ACTIVE()
				IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLIGHT_START")	
					START_AUDIO_SCENE("TREVOR_2_FLIGHT_START")
				ENDIF
				SET_PED_INFINITE_AMMO_CLIP(PLAYER_PED_ID(),FALSE)
				SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(),RBF_NONE)
				SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(),TRUE)
				bCanSwitchWeapons = TRUE
				iprogress = 6
			ENDIF
		BREAK
		
		
		CASE 6

			IF ARE_REQUESTED_MODELS_LOADED()
			AND ARE_REQUESTED_CARRECS_LOADED("RCSJC")
			
				IF NOT HAS_LABEL_BEEN_TRIGGERED("START_THE BIKERS")
					REQUEST_VEHICLE_RECORDING(14,"RCSJC")
					REQUEST_VEHICLE_RECORDING(15,"RCSJC")
					REQUEST_VEHICLE_RECORDING(16,"RCSJC")
					REQUEST_VEHICLE_RECORDING(17,"RCSJC")
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(14,"RCSJC")
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(15,"RCSJC")
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(16,"RCSJC")
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(17,"RCSJC")
						
						CREATE_ENEMY_VEHICLE(15,HEXER,<< 1769.6014, 3306.7300, 40.1565 >>,143.6793 ,G_M_Y_LOST_01,TRUE,15)
						CREATE_ENEMY_VEHICLE(16,HEXER,<< 1032.6766, 3080.1802, 41.0803 >> , 272.7215,G_M_Y_LOST_01,TRUE,16)
						CREATE_ENEMY_VEHICLE(17,HEXER,<< 1032.6766, 3080.1802, 41.0803 >> , 272.7215,G_M_Y_LOST_01,TRUE,17)
						CREATE_ENEMY_VEHICLE(18,HEXER,<< 1685.2866, 3231.1609, 39.5343 >> , 272.7215,G_M_Y_LOST_01,TRUE,18)
					
						IF IS_VEHICLE_DRIVEABLE(mission_veh[15].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[15].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[15].veh,14,"RCSJC",4,0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[15].veh, fBike1CutsceneSkipStartTime)
							ENDIF
							SET_PED_COMBAT_ATTRIBUTES(driver[15].ped,CA_DISABLE_PULL_ALONGSIDE_DURING_VEHICLE_CHASE,TRUE)
							SET_PED_ACCURACY(driver[15].ped,1)
							
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[15].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
							SET_PLAYBACK_SPEED(mission_veh[15].veh,1)
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[16].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[16].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[16].veh,15,"RCSJC",4,0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[16].veh, fBike2CutsceneSkipStartTime)
							ENDIF
							SET_PED_COMBAT_ATTRIBUTES(driver[16].ped,CA_DISABLE_PULL_ALONGSIDE_DURING_VEHICLE_CHASE,TRUE)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[16].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
							SET_PLAYBACK_SPEED(mission_veh[16].veh,1)
							SET_PED_ACCURACY(driver[16].ped,1)
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[17].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[17].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[17].veh,16,"RCSJC",4,0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[17].veh, fBike3CutsceneSkipStartTime)
							ENDIF
							SET_PED_COMBAT_ATTRIBUTES(driver[17].ped,CA_DISABLE_PULL_ALONGSIDE_DURING_VEHICLE_CHASE,TRUE)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[17].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
							SET_PLAYBACK_SPEED(mission_veh[17].veh,1)
							SET_PED_ACCURACY(driver[17].ped,1)
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[18].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[18].veh)
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[18].veh,17,"RCSJC",4,0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[18].veh, fBike4CutsceneSkipStartTime)
							ENDIF
							SET_PED_COMBAT_ATTRIBUTES(driver[18].ped,CA_DISABLE_PULL_ALONGSIDE_DURING_VEHICLE_CHASE,TRUE)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[18].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
							SET_PLAYBACK_SPEED(mission_veh[18].veh,1)
							SET_PED_ACCURACY(driver[18].ped,1)
						ENDIF
						SET_LABEL_AS_TRIGGERED("START_THE BIKERS",TRUE)
					ENDIF
				ELSE
					//FINAL VAN
					CREATE_ENEMY_VEHICLE(27,GBURRITO,<< 1548.6367, 3140.0676, 39.5341 >>,317.6045,G_M_Y_LOST_01,TRUE,25,3)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[27].veh, TRUE)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[22].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[22].veh)
							START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(mission_veh[22].veh,17,"RCSJC")
							SET_PLAYBACK_SPEED(mission_veh[22].veh,1.0)
						ENDIF
					ENDIF
					
					//FINAL VAN: VAN
					IF IS_VEHICLE_DRIVEABLE(mission_veh[27].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[27].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[27].veh,35,"RCSJC")
							SET_PLAYBACK_SPEED(mission_veh[27].veh,1.0)
							PRINTSTRING("TRIGGER FINAL VAN")PRINTNL()
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[13].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[13].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[13].veh)
						ELSE
							IF NOT IS_PED_INJURED(driver[13].ped)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(driver[13].ped,200)
							ENDIF
						ENDIF
					ENDIF
						
					
					REMOVE_ANIM_DICT("misstrevor2ig_10")
					REMOVE_ANIM_DICT("misstrevor2ig_9b")
					
					iprogress = 8
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
					FREEZE_ENTITY_POSITION(mission_veh[2].veh,FALSE)
					SET_VEHICLE_ENGINE_ON(mission_veh[2].veh,TRUE,TRUE)
					SET_HELI_BLADES_FULL_SPEED(mission_veh[2].veh)
					SET_ENTITY_PROOFS(mission_veh[2].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
					SETTIMERA(0)
					REMOVE_BLIP(veh_blip)
					IF DOES_BLIP_EXIST(psRon.blipIndex)
						REMOVE_BLIP(psRon.blipIndex)
					ENDIF
					CLEAR_PRINTS()
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iprogress++
				ELSE
					IF NOT DOES_BLIP_EXIST(veh_blip)
						veh_blip = CREATE_BLIP_FOR_VEHICLE(mission_veh[2].veh)
					ENDIF
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_GETPLNG2")
						PRINT_GOD_TEXT("SJC_GETPLNG2")
						SET_LABEL_AS_TRIGGERED("SJC_GETPLNG2",TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF NOT IS_ENTITY_IN_AIR(mission_veh[1].veh)
					PRINTSTRING("DISABLING Y")
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_FRONTEND_AXIS_Y)
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF TRIGGER_MUSIC_EVENT("TRV2_FLY")
					CLEAR_PRINTS()
					IF bGodText = FALSE
						PRINT_GOD_TEXT("SJC_FLWPLNG")
						 bGodText = TRUE
					ENDIF
					PRINT_HELP("SJC_FLYH1")
					
					IF NOT DOES_BLIP_EXIST(psRon.blipIndex)
						psRon.blipIndex = CREATE_BLIP_FOR_VEHICLE(mission_veh[1].veh)
					ENDIF
					fRonPlayback = 0.6
					CLEANUP_RUNWAY_VEHICLE_RECORDINGS()
					iprogress++
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF NOT IS_ENTITY_IN_AIR(mission_veh[1].veh)
					PRINTSTRING("DISABLING Y")
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_FRONTEND_AXIS_Y)
				ENDIF
			ENDIF
		BREAK

		
		CASE 9
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF fRonPlayback < 1.3
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
						fRonPlayback = fRonPlayback + 0.008
						SET_PLAYBACK_SPEED(mission_veh[1].veh,fRonPlayback)
					ENDIF
				ENDIF
				IF fRonPlayback > 1.29
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) AND IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						itimer =  GET_GAME_TIMER()
						SET_WANTED_LEVEL_MULTIPLIER(0.2)
						ADVANCE_MISSION_STAGE()
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_IN_AIR(mission_veh[1].veh)
					PRINTSTRING("DISABLING Y")
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_FRONTEND_AXIS_Y)
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(veh_blip)
					veh_blip = CREATE_BLIP_FOR_VEHICLE(mission_veh[2].veh)
				ENDIF
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_GETPLNG2")
					PRINT_GOD_TEXT("SJC_GETPLNG2")
					SET_LABEL_AS_TRIGGERED("SJC_GETPLNG2",TRUE)
				ENDIF
			ENDIF
			
		
		BREAK

	ENDSWITCH

ENDPROC

PROC MANAGE_CARGO()
	
	INT i

	FOR i = 0 TO MAX_CARGO - 1
		IF DOES_ENTITY_EXIST(s_cargo[i].cargo)
		AND DOES_ENTITY_EXIST(s_cargo[i].cargoPara)
			IF NOT IS_ENTITY_IN_WATER(s_cargo[i].cargo)
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(s_cargo[i].cargo, TRUE)
				APPLY_FORCE_TO_ENTITY(s_cargo[i].cargo, APPLY_TYPE_IMPULSE, <<0,0,0>>, <<0,0,-0.2>>, 0, TRUE, TRUE, TRUE)
				//PRINTSTRING("APPLYING FORCE TO PACKAGE - ")PRINTINT(i)PRINTNL()
				s_cargo[i].bCargoHasLanded = FALSE
			ELSE
				
				IF s_cargo[i].bCargoHasLanded = FALSE
					PLAY_ENTITY_ANIM(s_cargo[i].cargoPara, "p_cargo_chute_s_crumple", "p_cargo_chute_s", 1.0, FALSE, TRUE)
					s_cargo[i].iCargoTimer = GET_GAME_TIMER()
					s_cargo[i].bCargoHasLanded = TRUE
				ELSE
					IF MANAGE_MY_TIMER(s_cargo[i].iCargoTimer,2000)
						IF DOES_ENTITY_EXIST(s_cargo[i].cargoPara)
							IF NOT IS_ENTITY_ON_SCREEN(s_cargo[i].cargoPara)
								DELETE_OBJECT(s_cargo[i].cargoPara)
								SET_MODEL_AS_NO_LONGER_NEEDED(P_CARGO_CHUTE_S)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC DO_APPROACH_DROP_OFF_CUTSCENE()

	SETTIMERA(0)
	SET_USE_HI_DOF()
	//HIDE_HUD_AND_RADAR_THIS_FRAME()
	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_2_CARGO_DOORS")
			START_AUDIO_SCENE("TREVOR_2_CARGO_DOORS")
			SET_LABEL_AS_TRIGGERED("TREVOR_2_CARGO_DOORS",TRUE)
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TREV2")
		
		SWITCH icutsceneprog
			
			CASE 0
				IF REQUEST_SCRIPT_AUDIO_BANK("TREVOR_2_PLANE_CARGO_BAY")
					IF NOT DOES_CAM_EXIST(cutscene_cam)
						cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE) 
						SET_CAM_ACTIVE(cutscene_cam,TRUE)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
							ATTACH_CAM_TO_ENTITY(cutscene_cam, mission_veh[2].veh, <<0.0204, -1.0638, -0.8675>>)
							POINT_CAM_AT_ENTITY(cutscene_cam, mission_veh[2].veh, <<-0.0038, 1.9361, -0.8524>>)
							SET_CAM_FOV(cutscene_cam, 49.4163)
						ENDIF
						
						SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.3)
						SET_TIME_SCALE(0.7)
						RENDER_SCRIPT_CAMS(TRUE,FALSE)
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SETTIMERB(0)
						//TASK_VEHICLE_PLAY_ANIM(mission_veh[2].veh,"va_cuban800","drophatch")
						IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
							OPEN_BOMB_BAY_DOORS(mission_veh[2].veh)
							PLAY_SOUND_FROM_ENTITY(-1,"Trevor_2_cargo_bay_open",mission_veh[2].veh,"TREVOR_2_SOUNDS")
							//PLAY_SOUND_FROM_ENTITY(-1,"DRUG_TRAFFIC_AIR_BAY_DOOR_OPEN_MASTER",mission_veh[2].veh)
						ENDIF
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
						icutsceneprog++
					ENDIF
				ENDIF
			
			BREAK

			CASE 1
			
				IF TIMERB() > 1500	
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_2_DROP_CARGO")
						IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_CARGO_DOORS")
							STOP_AUDIO_SCENE("TREVOR_2_CARGO_DOORS")
						ELSE
							START_AUDIO_SCENE("TREVOR_2_DROP_CARGO")
							SET_LABEL_AS_TRIGGERED("TREVOR_2_DROP_CARGO",TRUE)
						ENDIF
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					SET_CAM_ACTIVE(cutscene_cam,FALSE)
					DESTROY_CAM(cutscene_cam)
					SET_TIME_SCALE(1)
					bcutsceneplaying = FALSE
					bShowHatchOpen = TRUE
					icutsceneprog = 0
				ENDIF
				
			BREAK

		ENDSWITCH
		
	ENDWHILE

ENDPROC


PROC DO_DROP_OFF_CUTSCENE()

	SETTIMERA(0)
	SET_USE_HI_DOF()
	//HIDE_HUD_AND_RADAR_THIS_FRAME()
	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TREV2")
		
		SWITCH icutsceneprog
			
			CASE 0
			
				IF NOT DOES_CAM_EXIST(cutscene_cam)			
					cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE) 
					SET_CAM_ACTIVE(cutscene_cam,TRUE)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						ATTACH_CAM_TO_ENTITY(cutscene_cam, mission_veh[2].veh,  <<0.1509, -0.1200, -0.0369>>)
						POINT_CAM_AT_ENTITY(cutscene_cam, mission_veh[2].veh, <<-0.4904, 0.8529, -2.8013>>)
						SET_CAM_FOV(cutscene_cam, 51.5610)
					ENDIF
					
					SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.3)
					SET_TIME_SCALE(0.5)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					SETTIMERB(0)
					icutsceneprog++
				ENDIF
			
			BREAK

			CASE 1
			
				IF TIMERB() > 1500	
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					SET_CAM_ACTIVE(cutscene_cam,FALSE)
					DESTROY_CAM(cutscene_cam)
					SET_TIME_SCALE(1)
					bcutsceneplaying = FALSE
					bDropCargo = TRUE
					icutsceneprog = 0
				ENDIF
				
			BREAK

		ENDSWITCH
		
	ENDWHILE

ENDPROC

PROC UPDATE_AI_PED_BLIPS_FOR_ENEMIES()

	INT i
	
	FOR i = 0 TO ( MAX_ENEMY_PED - 1 )		
		IF NOT IS_PED_INJURED(enemy[i].ped)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(enemy[i].ped)) < 100
				UPDATE_AI_PED_BLIP(enemy[i].ped, enemy[i].EnemyBlipData,-1,NULL,TRUE)
			ELSE
				CLEANUP_AI_PED_BLIP(enemy[i].EnemyBlipData)
			ENDIF
		ELSE
			CLEANUP_AI_PED_BLIP(enemy[i].EnemyBlipData)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO ( MAX_DRIVER_PED - 1 )		
		IF NOT IS_PED_INJURED(driver[i].ped)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(driver[i].ped)) < 100
				IF i != 19
				OR i != 20
				OR i != 21
					UPDATE_AI_PED_BLIP(driver[i].ped, driver[i].EnemyBlipData,-1,NULL,TRUE)
				ELSE
					CLEANUP_AI_PED_BLIP(driver[i].EnemyBlipData)
				ENDIF
			ELSE
				CLEANUP_AI_PED_BLIP(driver[i].EnemyBlipData)
			ENDIF
		ELSE
			CLEANUP_AI_PED_BLIP(driver[i].EnemyBlipData)
		ENDIF
	ENDFOR
		

ENDPROC

INT iBikerOnWingTimer
INT iCargoCount


PROC FLIGHT()
	IF iprogress < 3
		UPDATE_AI_PED_BLIPS_FOR_ENEMIES()
	ENDIF
	
	IF iprogress > 0
		IF GET_DISTANCE_BETWEEN_COORDS(<<1058.1727, 3075.0239, 40.3747>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
			CLEANUP_BIKER_ASSETS()
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
			reason_for_fail = VEHICLE_CUBAN_ABADONED
			eMissionStage = MISSION_STAGE_FAIL
			PRINTSTRING("CUBAN LEFT")
			PRINTNL()
		ENDIF
	ENDIF
	
	IF iprogress > 1
		IF GET_DISTANCE_BETWEEN_COORDS(<<1058.1727, 3075.0239, 40.3747>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 150
			
			IF DOES_ENTITY_EXIST(mission_veh[15].veh)
				SAFE_CLEANUP_VEHICLE(mission_veh[15].veh,HEXER)
			ENDIF
			IF DOES_ENTITY_EXIST(enemy[15].ped)
				SAFE_CLEANUP_PED(enemy[15].ped,G_M_Y_LOST_01)
			ENDIF
			IF DOES_ENTITY_EXIST(mission_veh[16].veh)
				SAFE_CLEANUP_VEHICLE(mission_veh[16].veh,HEXER)
			ENDIF
			IF DOES_ENTITY_EXIST(enemy[16].ped)
				SAFE_CLEANUP_PED(enemy[16].ped,G_M_Y_LOST_01)
			ENDIF
			IF DOES_ENTITY_EXIST(mission_veh[17].veh)
				SAFE_CLEANUP_VEHICLE(mission_veh[17].veh,HEXER)
			ENDIF
			IF DOES_ENTITY_EXIST(enemy[17].ped)
				SAFE_CLEANUP_PED(enemy[17].ped,G_M_Y_LOST_01)
			ENDIF
			IF DOES_ENTITY_EXIST(mission_veh[18].veh)
				SAFE_CLEANUP_VEHICLE(mission_veh[18].veh,HEXER)
			ENDIF
			IF DOES_ENTITY_EXIST(enemy[18].ped)
				SAFE_CLEANUP_PED(enemy[18].ped,G_M_Y_LOST_01)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(<< -3345.1748, 3001.2778, -0.0897 >>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 1000
		REQUEST_MODEL(DINGHY)
		REQUEST_MODEL(G_M_Y_MEXGOON_01)
		IF HAS_MODEL_LOADED(DINGHY)
		AND HAS_MODEL_LOADED(G_M_Y_MEXGOON_01)
			IF NOT DOES_ENTITY_EXIST(mission_veh[19].veh)
				CREATE_ENEMY_VEHICLE(19,DINGHY,<< -3345.1748, 3001.2778, -0.0897 >>, 359.2722, G_M_Y_MEXGOON_01,TRUE,19)
				IF NOT IS_PED_INJURED(driver[19].ped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[19].ped,TRUE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[19].veh)
					SET_ENTITY_LOD_DIST(mission_veh[19].veh,1000)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(mission_veh[20].veh)
				CREATE_ENEMY_VEHICLE(20,DINGHY, << -3359.0313, 3022.2456, -0.0774 >>, 145.8453, G_M_Y_MEXGOON_01,TRUE,20)
				IF NOT IS_PED_INJURED(driver[20].ped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[20].ped,TRUE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[20].veh)
					SET_ENTITY_LOD_DIST(mission_veh[20].veh,1000)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(mission_veh[21].veh)
				CREATE_ENEMY_VEHICLE(21,DINGHY,<< -3308.2231, 3013.0593, -0.1333 >>, 310.5993 ,G_M_Y_MEXGOON_01,TRUE,21)
				IF NOT IS_PED_INJURED(driver[21].ped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[21].ped,TRUE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[21].veh)
					SET_ENTITY_LOD_DIST(mission_veh[21].veh,1000)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//VECTOR vDropOff = << -3345.1748, 3001.2778, -0.0897 >>
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_2_FLIGHT_START")
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_RAYFIRE")
			STOP_AUDIO_SCENE("TREVOR_2_RAYFIRE")
		ENDIF
		START_AUDIO_SCENE("TREVOR_2_FLIGHT_START")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_RON_PLANE].veh,"TREVOR_2_RONS_PLANE")
		ENDIF
		SET_LABEL_AS_TRIGGERED("TREVOR_2_FLIGHT_START",TRUE)
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
		
	ENDIF

	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_5)
			OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(mission_veh[2].veh)
		ENDIF
			
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_8)
			OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(mission_veh[2].veh)
		ENDIF
	#ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
		IF IS_ENTITY_IN_AIR(mission_veh[2].veh)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("SET FLIGHT CHECKPOINT")
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "Fly to drop off point") 
				SET_LABEL_AS_TRIGGERED("SET FLIGHT CHECKPOINT",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[1].veh) > 84668.429688
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_SIG")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_SIG", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("TR2_SIG",TRUE)
									
									REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RES")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RES", CONV_PRIORITY_MEDIUM)
									iDropOffHelpTimer = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("TR2_RES",TRUE)
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
										//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-3405.108887,2967.501221,-94.399391>>, <<-3189.308105,3065.976807,208.932220>>, 250.000000)
										IF bShowHatchOpen = TRUE
											IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),GET_ENTITY_COORDS(mission_veh[19].veh),<<175,175,800>>)
												IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_DROPH")
													iFlightHelp = 8
													PRINT_HELP("SJC_DROPH")
													SET_LABEL_AS_TRIGGERED("SJC_DROPH",TRUE)
													iDropOffHelpTimer = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ELSE
											IF HAS_LABEL_BEEN_TRIGGERED("SJC_DROPH")
												IF MANAGE_MY_TIMER(iDropOffHelpTimer,1000)
													IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SJC_DROPH")
														CLEAR_HELP()
													ENDIF
													SET_LABEL_AS_TRIGGERED("SJC_DROPH",FALSE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	SWITCH iProgress
	
		CASE 0
			IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLIGHT_START")
				START_AUDIO_SCENE("TREVOR_2_FLIGHT_START")
			ENDIF
			//IF IS_NEW_LOAD_SCENE_LOADED()
				NEW_LOAD_SCENE_STOP()
				IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				
				FOR iCount = 0 TO (MAX_ENEMY_PED-1)
					IF DOES_ENTITY_EXIST(enemy[iCount].ped)
						IF iCount != 27
							IF NOT IS_PED_INJURED(enemy[iCount].ped)
								TASK_COMBAT_PED(enemy[iCount].ped,PLAYER_PED_ID())
								SET_PED_KEEP_TASK(enemy[iCount].ped,TRUE)
							ENDIF
							SET_PED_AS_NO_LONGER_NEEDED(enemy[iCount].ped)
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(enemy[iCount].blip)
						REMOVE_BLIP(enemy[iCount].blip)
					ENDIF
				ENDFOR
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
					SET_VEHICLE_STRONG(mission_veh[MV_TREVOR_PLANE].veh,FALSE)
				ENDIF
				
				FOR iCount = 3 TO (MAX_MISSION_VEH-1)
					IF iCount != 15
					OR iCount != 16
					OR iCount != 17
					OR iCount != 18
						IF DOES_ENTITY_EXIST(mission_veh[iCount].veh)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[iCount].veh)
						ENDIF
					ENDIF
				ENDFOR
				
				FOR iCount = 0 TO (MAX_DRIVER_PED-1)
					IF DOES_ENTITY_EXIST(driver[iCount].ped)
						IF iCount != 15
						OR iCount != 16
						OR iCount != 17
						OR iCount != 18
							IF NOT IS_PED_INJURED(driver[iCount].ped)
								TASK_COMBAT_PED(driver[iCount].ped,PLAYER_PED_ID())
								SET_PED_KEEP_TASK(driver[iCount].ped,TRUE)
							ENDIF
						
							SET_PED_AS_NO_LONGER_NEEDED(driver[iCount].ped)
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(driver[iCount].blip)
						REMOVE_BLIP(driver[iCount].blip)
					ENDIF
				ENDFOR

				FOR iCount = 0 TO 14
					IF DOES_ENTITY_EXIST(crate[iCount])
						SET_OBJECT_AS_NO_LONGER_NEEDED(crate[iCount])
					ENDIF
				ENDFOR
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFXfire[0])
					STOP_PARTICLE_FX_LOOPED(PTFXfire[0])
				ENDIF
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(PTFXfire[1])
					STOP_PARTICLE_FX_LOOPED(PTFXfire[1])
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					iFlightTimer = GET_GAME_TIMER()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					iFlightHelp = 2
				ENDIF
				
				CONTROL_FADE_IN(500)

				IF NOT DOES_BLIP_EXIST(psRon.blipIndex)
					psRon.blipIndex = CREATE_BLIP_FOR_VEHICLE(mission_veh[1].veh)
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[2].veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[2].veh)
					ENDIF
					SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[2].veh, TRUE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					SET_ENTITY_PROOFS(mission_veh[1].veh,TRUE,TRUE,TRUE,TRUE,TRUE)
					CONTROL_LANDING_GEAR(mission_veh[1].veh,LGC_RETRACT)
				ENDIF

				
				
				REQUEST_PTFX_ASSET()
				
				CLEAN_OBJECTIVE_BLIP_DISPLAY()
				//MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[1].veh,<<0,0,0>>)
				
				IF bGodText = FALSE
					PRINT_GOD_TEXT("SJC_FLWPLNG")
					bGodText = TRUE
				ENDIF
				
				REQUEST_MODEL(Prop_Flare_01)
				REMOVE_MODEL_FROM_ARRAY(MAVERICK)
				
				SETTIMERA(0)
				bDialogue = FALSE
				SETUP_PEDS_FOR_DIALOGUE()
				STREAMING_FIX()
				//bSkipped = FALSE
				brunfailchecks = TRUE
				bBridge[0] = FALSE
				bBridge[1] = FALSE
				bBridge[2] = FALSE
				bBridge[3] = FALSE
				bBridge[4] = FALSE
				bBridge[5] = FALSE
				bBridge[6] = FALSE
				bBridge[7] = FALSE
				bFlightDialogue = FALSE
				bShowHatchOpen = FALSE
				PRINTSTRING("!!!iProgress = 1")PRINTNL()
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),TRUE,-1)
				SET_VEHICLE_POPULATION_BUDGET(1)
				SET_PED_POPULATION_BUDGET(1)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_MAX_WANTED_LEVEL(0)
				DISABLE_TAXI_HAILING(TRUE)
				fRonPlayback = 1.0
				iprogress++
			//ENDIF
		
		BREAK
		
		CASE 1
			IF IS_SCREEN_FADED_OUT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			IF bDialogue = FALSE
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pl01", CONV_PRIORITY_MEDIUM )
						bDialogue = TRUE
					ENDIF
				ENDIF
			ELSE
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(psRon.pedIndex,FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE)) < 500
					IF eBalanceState != STATE_FALL_OFF
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_bi01")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_bi01", CONV_PRIORITY_MEDIUM )
									SET_LABEL_AS_TRIGGERED("TR_bi01",TRUE)
								ENDIF
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_bi02")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_bi02", CONV_PRIORITY_MEDIUM )
										SET_LABEL_AS_TRIGGERED("TR_bi02",TRUE)
										iBikerOnWingTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ELSE
								IF MANAGE_MY_TIMER(iBikerOnWingTimer,GET_RANDOM_INT_IN_RANGE(6000,9000))
									SET_LABEL_AS_TRIGGERED("TR_bi02",FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDialogue = TRUE
				IF eBalanceState = STATE_FALL_OFF
					IF bFlightDialogue = FALSE
						IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_FLY", CONV_PRIORITY_MEDIUM )
									bFlightDialogue = TRUE
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_FLYb", CONV_PRIORITY_MEDIUM )
									bFlightDialogue = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF fRonPlayback < 1.6
				IF DOES_ENTITY_EXIST(mission_veh[1].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
						fRonPlayback = fRonPlayback + 0.01
						SET_PLAYBACK_SPEED(mission_veh[1].veh,fRonPlayback)
					ENDIF
				ENDIF
			ELSE
				RUBBER_BAND_BETWEEN_TWO_VEHICLES(mission_veh[2].veh,mission_veh[1].veh,<<-3345.5474, 3001.3638, 2.0393>>,3,"RCSJC",180,0.5,1.8,TRUE)
			ENDIF

			IF HAS_MODEL_LOADED(PROP_FLARE_01)
				IF HAS_PTFX_ASSET_LOADED()
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
								IF bobjectivetoggle[0]
									CLEAR_PRINTS()
									PRINT_GOD_TEXT("SJC_FLWPLNG")
									CLEAN_OBJECTIVE_BLIP_DISPLAY()
									//MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[1].veh,<<0,0,0 >> )
									bobjectivetoggle[0] = FALSE
								ENDIF
								IF IS_ENTITY_AT_COORD(mission_veh[1].veh, << -534.3039, 4574.4072, 89.3176 >>,<<500,500,500>> )
									
									IF GET_LANDING_GEAR_STATE(mission_veh[1].veh) != LGS_RETRACTING
									AND GET_LANDING_GEAR_STATE(mission_veh[1].veh) != LGS_LOCKED_UP
										CONTROL_LANDING_GEAR(mission_veh[1].veh,LGC_RETRACT)
									ENDIF
									
									PTFXsmugglersFlare = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev2_flare_L", <<-3345.26, 3003.77, 0.68>>, <<0,0,0>>, 1.0)
									SET_PARTICLE_FX_LOOPED_COLOUR(PTFXsmugglersFlare, 1.0, 0.84, 0.0)
									PRINTLN("STARTING PARTICLE EFFECT")
												
									//SNAP_3D_COORD_TO_GROUND(vCurDestObjective[0])
									oFlareProp = CREATE_OBJECT(Prop_Flare_01, <<-3345.26, 3003.77, 0.68>>)
									SET_ENTITY_ROTATION(oFlareProp, <<0,90,0>>)
									
									iCount= 0
									bobjectivetoggle[0] = FALSE
									REQUEST_ANIM_DICT("P_cargo_chute_S")
									bDialogue = FALSE
									PRINTSTRING("!!!iProgress = 2")PRINTNL()
									iprogress++
									
								ELSE
									PRINTSTRING("Buddy not at coord")PRINTNL()
								ENDIF
							ELSE
								IF NOT bobjectivetoggle[0]
									PRINT_GOD_TEXT("SJC_GETPLNG2")
									CLEAN_OBJECTIVE_BLIP_DISPLAY()
									MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[2].veh,<<0,0,0 >> )
									bobjectivetoggle[0] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTSTRING("Train not created")PRINTNL()
			ENDIF
			
		BREAK
		
		CASE 2

			IF NOT HAS_LABEL_BEEN_TRIGGERED("BRIDGETRAIN")
				REQUEST_MODEL(FREIGHTCONT1)
				REQUEST_MODEL(TANKERCAR)
				REQUEST_MODEL(FREIGHTCAR)
				REQUEST_MODEL(FREIGHT)
				IF HAS_MODEL_LOADED(FREIGHTCONT1)
				AND HAS_MODEL_LOADED(TANKERCAR)
				AND HAS_MODEL_LOADED(FREIGHT)
				AND HAS_MODEL_LOADED(FREIGHTCAR)
					IF NOT DOES_ENTITY_EXIST(MissionTrain)
						MissionTrain = CREATE_MISSION_TRAIN(6, << -534.3039, 4574.4072, 89.3176 >>, FALSE)
						SET_TRAIN_CRUISE_SPEED(MissionTrain,15)
						SET_TRAIN_SPEED(MissionTrain,15)
						SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT1)
						SET_MODEL_AS_NO_LONGER_NEEDED(TANKERCAR)
						SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCAR)
						SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHT)
						iTimeSinceTrainCreation = GET_GAME_TIMER()
						SET_LABEL_AS_TRIGGERED("BRIDGETRAIN",TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF eBalanceState =  STATE_FALL_OFF
				IF bFlightDialogue = FALSE
					IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_FLY", CONV_PRIORITY_MEDIUM )
								bFlightDialogue = TRUE
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_FLYb", CONV_PRIORITY_MEDIUM )
								bFlightDialogue = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF fRonPlayback < 1.8
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
					fRonPlayback = fRonPlayback + 0.01
					SET_PLAYBACK_SPEED(mission_veh[1].veh,fRonPlayback)
				ENDIF
			ELSE
				RUBBER_BAND_BETWEEN_TWO_VEHICLES(mission_veh[2].veh,mission_veh[1].veh,<<-3345.5474, 3001.3638, 2.0393>>,3,"RCSJC",220,0.5,2.6,TRUE)
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_COORDS(<< -3345.1748, 3001.2778, -0.0897 >>,GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) < 1000
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					MODIFY_VEHICLE_TOP_SPEED(mission_veh[2].veh,70)
					PRINTLN("MODIFYING TOP SPEED")PRINTNL()
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					MODIFY_VEHICLE_TOP_SPEED(mission_veh[2].veh,100)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_AT_COORD(mission_veh[1].veh, << -3345.1748, 3001.2778, -0.0897 >>,<<600,600,600>> )
				PRINTSTRING("!!!iProgress = 3")PRINTNL()
				REQUEST_MODEL(PROP_DRUG_PACKAGE)
				REQUEST_MODEL(P_CARGO_CHUTE_S)	
				REQUEST_SCRIPT_AUDIO_BANK("TREVOR_2_PLANE_CARGO_BAY")
				REMOVE_MODEL_FROM_ARRAY(GBURRITO)
				iCargoCount = 0
				iprogress++
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
						IF bobjectivetoggle[0]
							PRINT_GOD_TEXT("SJC_FLWPLNG")
							CLEAN_OBJECTIVE_BLIP_DISPLAY()
							//MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[1].veh,<<0,0,0 >> )
							bobjectivetoggle[0] = FALSE
						ENDIF
					ELSE
						IF NOT bobjectivetoggle[0]
							PRINT_GOD_TEXT("SJC_GETPLNG2")
							CLEAN_OBJECTIVE_BLIP_DISPLAY()
							MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[2].veh,<<0,0,0 >> )
							bobjectivetoggle[0] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		CASE 3
//			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CONT")
//					IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_CONT", CONV_PRIORITY_MEDIUM ) 
//						SET_LABEL_AS_TRIGGERED("TR2_CONT",TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
			
			RUBBER_BAND_BETWEEN_TWO_VEHICLES(mission_veh[2].veh,mission_veh[1].veh,<<-3345.5474, 3001.3638, 2.0393>>,3,"RCSJC",300,0.5,2.8,TRUE)
		
			IF GET_DISTANCE_BETWEEN_COORDS(<< -3345.1748, 3001.2778, -0.0897 >>,GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) < 800
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					MODIFY_VEHICLE_TOP_SPEED(mission_veh[2].veh,70)
				ENDIF
			ELSE
				MODIFY_VEHICLE_TOP_SPEED(mission_veh[2].veh,100)
			ENDIF
			
			REQUEST_MODEL(PROP_DRUG_PACKAGE)
			REQUEST_MODEL(P_CARGO_CHUTE_S)	
			REQUEST_SCRIPT_AUDIO_BANK("TREVOR_2_PLANE_CARGO_BAY")
			REQUEST_ANIM_DICT("P_cargo_chute_S")
			
			IF HAS_ANIM_DICT_LOADED("P_cargo_chute_S")
				IF HAS_MODEL_LOADED(PROP_DRUG_PACKAGE)
				AND HAS_MODEL_LOADED(P_CARGO_CHUTE_S)
					IF iCargoCount < 3
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) AND IS_VEHICLE_DRIVEABLE(mission_veh[19].veh)
							//IF IS_ENTITY_AT_COORD(mission_veh[1].veh, << -3345.1748, 3001.2778, -0.0897 >>, <<350,350,350>>)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("RON NEAR BOATS")
								IF IS_ENTITY_AT_COORD(mission_veh[1].veh,GET_ENTITY_COORDS(mission_veh[19].veh),<<200,200,800>>)
									SET_LABEL_AS_TRIGGERED("RON NEAR BOATS",TRUE)
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_rLoad")
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech,"T2AUD", "TR_pl02","TR2_ANAA",CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("TR_rLoad",TRUE)
									ENDIF
								ENDIF

								REQUEST_MODEL(P_CARGO_CHUTE_S)
								REQUEST_MODEL(PROP_DRUG_PACKAGE)
								IF HAS_MODEL_LOADED(P_CARGO_CHUTE_S)
								AND HAS_MODEL_LOADED(PROP_DRUG_PACKAGE)
									IF (GET_GAME_TIMER() - itimer) > 700
										IF NOT DOES_ENTITY_EXIST(s_cargo[iCargoCount].cargo)
											IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
												IF NOT DOES_ENTITY_EXIST(s_cargo[iCargoCount].cargoPara)
													s_cargo[iCargoCount].cargoPara = CREATE_OBJECT(P_CARGO_CHUTE_S,(GET_ENTITY_COORDS(mission_veh[1].veh)-<<0,0,3>>))
												ENDIF
												s_cargo[iCargoCount].cargo = CREATE_OBJECT(PROP_DRUG_PACKAGE,(GET_ENTITY_COORDS(mission_veh[1].veh)-<<0,0,4>>))
											ENDIF
										ELSE
											IF DOES_ENTITY_EXIST(s_cargo[iCargoCount].cargoPara)
												ATTACH_ENTITY_TO_ENTITY(s_cargo[iCargoCount].cargoPara,s_cargo[iCargoCount].cargo,0,<<0,0,0.18>>,<<0,0,0>>)
												
												APPLY_FORCE_TO_ENTITY(s_cargo[iCargoCount].cargo, APPLY_TYPE_IMPULSE, <<0,0,0>>, <<0,0,-0.5>>, 0, TRUE, TRUE, TRUE)
												SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(s_cargo[iCargoCount].cargo, TRUE)
												
												PLAY_ENTITY_ANIM(s_cargo[iCargoCount].cargoPara, "p_cargo_chute_s_deploy", "p_cargo_chute_s", 1.0, FALSE, TRUE)
												SET_ENTITY_LOAD_COLLISION_FLAG(s_cargo[iCargoCount].cargo, TRUE)
												SET_ENTITY_LOD_DIST(s_cargo[iCargoCount].cargoPara, 500)
												SET_ENTITY_LOD_DIST(s_cargo[iCargoCount].cargo, 500)
												PRINTSTRING("CARGO!!")PRINTNL()
												iCargoCount++
												itimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						bobjectivetoggle[0] = FALSE
						bobjectivetoggle[1] = FALSE
						bGodText = FALSE
						bDropCargo = FALSE
						PRINTSTRING("!!!iProgress = 4")PRINTNL()
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
							PRINTSTRING("Ron goes back to playback speed 1")PRINTNL()
							SET_PLAYBACK_SPEED(mission_veh[1].veh,1.0)
						ENDIF
						
						IF DOES_CAM_EXIST(cutscene_cam)
							DESTROY_CAM(cutscene_cam)
						ENDIF
						iprogress++	
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4

			IF GET_DISTANCE_BETWEEN_COORDS(<< -3345.1748, 3001.2778, -0.0897 >>,GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) < 600
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					MODIFY_VEHICLE_TOP_SPEED(mission_veh[2].veh,50)
				ENDIF
			ELSE
				MODIFY_VEHICLE_TOP_SPEED(mission_veh[2].veh,100)
			ENDIF
		
			//IF ARE_REQUESTED_CARRECS_LOADED("RCSJC")
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
						IF NOT IS_ENTITY_IN_WATER(mission_veh[2].veh)
							IF IS_ENTITY_AT_COORD(mission_veh[2].veh, << -3345.1748, 3001.2778, -0.0897 >>, <<450,450,450>>)
								IF bShowHatchOpen = FALSE
									DO_APPROACH_DROP_OFF_CUTSCENE()
								ENDIF
								
								IF NOT bobjectivetoggle[0]
									IF IS_THIS_PRINT_BEING_DISPLAYED("SJC_DROPG2")
										CLEAR_PRINTS()
									ENDIF
									CLEAN_OBJECTIVE_BLIP_DISPLAY()
									IF IS_VEHICLE_DRIVEABLE(mission_veh[19].veh)
										IF NOT DOES_BLIP_EXIST(blipDropOff)
											blipDropOff = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(mission_veh[19].veh),150)
											SET_BLIP_COLOUR(blipDropOff,BLIP_COLOUR_BLUE)
											PRINTSTRING("ADDING BLIP FOR DROP OFF - 1")PRINTNL()
										ENDIF
									ENDIF
									
									bobjectivetoggle[0] = TRUE
									DISABLE_CELLPHONE(TRUE)
								ENDIF
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF bGodText = FALSE
										PRINT_GOD_TEXT("SJC_DROPG1")
										bGodText = TRUE
									ENDIF
								ENDIF
								//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-3333.716553,3070.866943,216.570862>>, <<-3269.911133,2949.003906,-1.488411>>, 102.750000)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),GET_ENTITY_COORDS(mission_veh[19].veh),<<175,175,800>>)
									IF bobjectivetoggle[0] = TRUE
										IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_VEH_FLY_ATTACK)
										AND NOT IS_ENTITY_UPSIDEDOWN(PLAYER_PED_ID())
											KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
											PLAY_SOUND_FROM_ENTITY(-1,"Trevor_2_cargo_drop",mission_veh[2].veh,"TREVOR_2_SOUNDS")
											SET_CONTROL_SHAKE(PLAYER_CONTROL,200,256)
											CLEAR_HELP()
											iStageTimer = GET_GAME_TIMER()
											iprogress++
										ELSE
											DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_CIN_CAM)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF IS_ENTITY_AT_COORD(mission_veh[2].veh, << -3345.1748, 3001.2778, -0.0897 >>, <<400,400,400>>)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_CIN_CAM)
								ENDIF
								IF NOT bobjectivetoggle[1]
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
											IF bShowHatchOpen = TRUE
												IF NOT IS_ENTITY_IN_WATER(mission_veh[2].veh)
													PRINT_GOD_TEXT("SJC_DROPG2")
												ENDIF
											ENDIF
										ENDIF
										IF DOES_BLIP_EXIST(veh_blip)
											REMOVE_BLIP(veh_blip)
										ENDIF
										CLEAN_OBJECTIVE_BLIP_DISPLAY()
										IF IS_VEHICLE_DRIVEABLE(mission_veh[19].veh)
											IF NOT DOES_BLIP_EXIST(blipDropOff)
												//blipDropOff = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(mission_veh[19].veh),100)
												blipDropOff = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(mission_veh[19].veh),150)
												SET_BLIP_COLOUR(blipDropOff,BLIP_COLOUR_BLUE)
											ENDIF
										ENDIF
										bobjectivetoggle[1] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bobjectivetoggle[0]
							KILL_FACE_TO_FACE_CONVERSATION()
							PRINT_GOD_TEXT("SJC_GETPLNG2")
							CLEAN_OBJECTIVE_BLIP_DISPLAY()
							MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,mission_veh[2].veh,<<0,0,0 >> )
							bobjectivetoggle[0] = TRUE
						ENDIF
					ENDIF	
				ENDIF
			//ENDIF
			
		BREAK
		
		CASE 5

			IF NOT DOES_ENTITY_EXIST(s_cargo[4].cargo)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					IF NOT DOES_ENTITY_EXIST(s_cargo[4].cargoPara)
						s_cargo[4].cargoPara = CREATE_OBJECT(P_CARGO_CHUTE_S,(GET_ENTITY_COORDS(mission_veh[2].veh)-<<0,0,3>>))
					ENDIF
					s_cargo[4].cargo = CREATE_OBJECT(PROP_DRUG_PACKAGE,(GET_ENTITY_COORDS(mission_veh[2].veh)-<<0,0,4>>))
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(s_cargo[4].cargoPara)
					IF NOT IS_ENTITY_ATTACHED(s_cargo[4].cargoPara)
						ATTACH_ENTITY_TO_ENTITY(s_cargo[4].cargoPara,s_cargo[4].cargo,0,<<0,0,0.18>>,<<0,0,0>>)
						APPLY_FORCE_TO_ENTITY(s_cargo[4].cargo, APPLY_TYPE_IMPULSE, <<0,0,0>>, <<0,0,-0.1>>, 0, TRUE, TRUE, FALSE)
						FREEZE_ENTITY_POSITION(s_cargo[4].cargoPara, FALSE)
						FREEZE_ENTITY_POSITION(s_cargo[4].cargo, FALSE)
						SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(s_cargo[4].cargoPara, TRUE)
						SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(s_cargo[4].cargo, TRUE)
						
						PLAY_ENTITY_ANIM(s_cargo[4].cargoPara, "p_cargo_chute_s_deploy", "p_cargo_chute_s", 1.0, FALSE, TRUE)
						SET_ENTITY_LOAD_COLLISION_FLAG(s_cargo[4].cargo, TRUE)
						SET_ENTITY_LOD_DIST(s_cargo[4].cargoPara, 500)
						SET_ENTITY_LOD_DIST(s_cargo[4].cargo, 500)
						PRINTSTRING("PARA SET UP")PRINTNL()
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(s_cargo[4].cargo)
				IF bDropCargo = FALSE
					//DO_DROP_OFF_CUTSCENE()
				ENDIF
				IF MANAGE_MY_TIMER(iStageTimer,1500)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_RONGUNS")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_RONGUNS", CONV_PRIORITY_MEDIUM )
								
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								
								SET_LABEL_AS_TRIGGERED("TR_RONGUNS",TRUE)
							ENDIF
						ENDIF
					ENDIF
					SET_TIME_SCALE(1.0)
				ENDIF
				IF MANAGE_MY_TIMER(iStageTimer,2000)
					IF HAS_LABEL_BEEN_TRIGGERED("TR_RONGUNS")
						KILL_FACE_TO_FACE_CONVERSATION()
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF DOES_BLIP_EXIST(blipDropOff)
								REMOVE_BLIP(blipDropOff)
							ENDIF
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pload", CONV_PRIORITY_MEDIUM )
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
									SET_PLAYBACK_SPEED(mission_veh[1].veh,1)
								ENDIF
								PRINTSTRING("STAGE 6")PRINTNL()
								SET_LABEL_AS_TRIGGERED("TR_pload",TRUE)
								iprogress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			
			CLEAN_OBJECTIVE_BLIP_DISPLAY()
			//MANAGE_OBJECTIVE_BLIP_DISPLAY(NULL,NULL,<< 2121.5569, 4803.9385, 40.1959 >>)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF NOT DOES_BLIP_EXIST(psRon.blipIndex)
					psRon.blipIndex = CREATE_BLIP_FOR_VEHICLE(mission_veh[1].veh)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
				MODIFY_VEHICLE_TOP_SPEED(mission_veh[2].veh,100)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[2].veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[2].veh)
					ENDIF
				ENDIF
			ENDIF
			
			PRINTNL()	
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			CLEAR_PRINTS()
			itimer = GET_GAME_TIMER()
			bobjectivetoggle[0] = TRUE
			bRetractLandingGear = FALSE
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				SET_ENTITY_PROOFS(mission_veh[1].veh,TRUE,TRUE,TRUE,TRUE,TRUE)
			ENDIF
			ADVANCE_MISSION_STAGE()
		BREAK

	ENDSWITCH
ENDPROC

INT iFailSafeTimer
/// PURPOSE:
/// Play the land plane cutscene
PROC RUN_LAND_PLANE_CUTSCENE()
	
	e_section_stage = SECTION_STAGE_SETUP
	i_current_event = 0
	bcutsceneplaying = TRUE
	b_skipped_mocap = FALSE

	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				b_skipped_mocap = TRUE
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ENDIF
	#ENDIF

	WHILE bcutsceneplaying
	
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TREV2")
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					bcutsceneplaying = TRUE
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TRV2_MCS_8",CS_SECTION_1| CS_SECTION_2)
					bClearCutscenArea = FALSE
					i_current_event++
				ELIF i_current_event = 1
					IF HAS_CUTSCENE_LOADED()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							WAIT(0)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
							CONTROL_LANDING_GEAR(mission_veh[MV_RON_PLANE].veh,LGC_DEPLOY_INSTANT)
							SET_VEHICLE_FIXED(mission_veh[MV_RON_PLANE].veh)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_RON_PLANE].veh, FALSE)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[1].veh, "Rons_plane", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[2].veh, "Trevors_plane", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(psRon.pedIndex)
							REGISTER_ENTITY_FOR_CUTSCENE(psRon.pedIndex, "Ron", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_PRESERVE_BODY_BLOOD_DAMAGE)
						ENDIF
						
						IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_LAND_THE_PLANE")
							STOP_AUDIO_SCENE("TREVOR_2_LAND_THE_PLANE")
						ENDIF
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						CLEAR_HELP()
						
						START_CUTSCENE()
	
						//Wait a couple of frames before clearing/warping stuff, so the cutscene is active first.
						WAIT(0)

						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						b_skipped_mocap = FALSE
						i_current_event = 0
						e_section_stage = SECTION_STAGE_RUNNING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			
			IF bClearCutscenArea = FALSE
				IF IS_CUTSCENE_PLAYING()
					REMOVE_PED_HELMET(PLAYER_PED_ID(),TRUE)
					CONTROL_FADE_IN(500)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						CLEAR_AREA(GET_ENTITY_COORDS(mission_veh[2].veh),100,TRUE)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
						CONTROL_LANDING_GEAR(mission_veh[MV_RON_PLANE].veh,LGC_DEPLOY_INSTANT)
					ENDIF
					iSkipCutsceneTimer = GET_GAME_TIMER()
					bClearCutscenArea = TRUE
					
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						//CINEMA STREAMING REQUESTS
						SET_GAME_PAUSES_FOR_STREAMING(FALSE)
						NEW_LOAD_SCENE_START_SPHERE(<< 396.71, -709.68, 30.7 >>, 20.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT b_skipped_mocap
			AND bClearCutscenArea = TRUE
				IF MANAGE_MY_TIMER(iSkipCutsceneTimer,2000)
					IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
						iFailSafeTimer = GET_GAME_TIMER()
						b_skipped_mocap = TRUE
					ENDIF
				ENDIF
			ENDIF
			
//			IF NOT IS_REPEAT_PLAY_ACTIVE()
//				IF GET_CUTSCENE_TIME() > 59691
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_PROPH1")
//						ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
//						PRINT_HELP_FOREVER("SJC_PROPH1")
//						SET_LABEL_AS_TRIGGERED("SJC_PROPH1",TRUE)
//					ENDIF
//				ENDIF
//				
//				IF GET_CUTSCENE_TIME() > 66691
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_PROPH2")
//						ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
//						PRINT_HELP_FOREVER("SJC_PROPH2")
//						SET_LABEL_AS_TRIGGERED("SJC_PROPH2",TRUE)
//					ENDIF
//				ENDIF
//			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				
					//HANGAR SHOT
				IF NOT IS_REPEAT_PLAY_ACTIVE()	
					IF NOT DOES_CAM_EXIST(cutscene_cam)	
						CLEAR_HELP()
						cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE) 
						SET_CAM_PARAMS(cutscene_cam,<<2130.666504,4814.803223,42.075703>>,<<6.014336,-0.729572,-175.598892>>,32.777882)
						SET_CAM_PARAMS(cutscene_cam,<<2130.519775,4812.049316,42.351028>>,<<7.652661,-0.093014,-177.040253>>,32.780319,12000,GRAPH_TYPE_DECEL)

						SET_CAM_FAR_CLIP(cutscene_cam,55)
						SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.3)
						RENDER_SCRIPT_CAMS(TRUE,FALSE)
					ENDIF
				ELSE
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(180)
				ENDIF
				
			ENDIF
						
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				
			ENDIF
						
			//RON NOT YET HERE
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ron")
				IF NOT IS_PED_INJURED(psRon.pedIndex)
					DELETE_PED(psRon.pedIndex)
				ENDIF
			ENDIF
						
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_plane")
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVOR_PLANE].veh)
					IF NOT IS_PLANE_LANDING_GEAR_INTACT(mission_veh[MV_TREVOR_PLANE].veh)
						SET_ENTITY_HEALTH(mission_veh[MV_TREVOR_PLANE].veh,1000)
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[MV_TREVOR_PLANE].veh,1000)
						SET_VEHICLE_PETROL_TANK_HEALTH(mission_veh[MV_TREVOR_PLANE].veh,1000)
					ENDIF
					CONTROL_LANDING_GEAR(mission_veh[MV_TREVOR_PLANE].veh,LGC_DEPLOY_INSTANT)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[MV_TREVOR_PLANE].veh)
					//SET_ENTITY_COORDS(mission_veh[MV_TREVOR_PLANE].veh,<<2135.65, 4780.53, 41.62>>)
					//SET_ENTITY_HEADING(mission_veh[MV_TREVOR_PLANE].veh, 15.99)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[MV_TREVOR_PLANE].veh)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("cuban800")
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
					IF NOT IS_PLANE_LANDING_GEAR_INTACT(mission_veh[MV_RON_PLANE].veh)
						SET_ENTITY_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
						SET_VEHICLE_PETROL_TANK_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
					ENDIF
					CONTROL_LANDING_GEAR(mission_veh[MV_RON_PLANE].veh,LGC_DEPLOY_INSTANT)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[MV_RON_PLANE].veh)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_RON_PLANE].veh, TRUE)
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
			AND bClearCutscenArea = TRUE
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					SET_MULTIHEAD_SAFE(TRUE, TRUE)
					PRINTLN("@@@@@@@@@@ SET_MULTIHEAD_SAFE(TRUE, TRUE) @@@@@@@@@@")
				ENDIF
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			IF b_skipped_mocap
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					SET_MULTIHEAD_SAFE(TRUE, TRUE)
					PRINTLN("@@@@@@@@@@ SET_MULTIHEAD_SAFE(TRUE, TRUE) @@@@@@@@@@")
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
				IF NOT IS_PLANE_LANDING_GEAR_INTACT(mission_veh[MV_RON_PLANE].veh)
					SET_ENTITY_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
					SET_VEHICLE_ENGINE_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
					SET_VEHICLE_PETROL_TANK_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
				ENDIF
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_RON_PLANE].veh,TRUE)
				SET_VEHICLE_DOORS_LOCKED(mission_veh[MV_RON_PLANE].veh,VEHICLELOCK_UNLOCKED)
				CONTROL_LANDING_GEAR(mission_veh[MV_RON_PLANE].veh,LGC_DEPLOY_INSTANT)
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[MV_RON_PLANE].veh)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_RON_PLANE].veh, TRUE)
			ENDIF
			
			IF IS_REPEAT_PLAY_ACTIVE()
				//Setup buddy relationship groups etc.
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			
			REPLAY_STOP_EVENT()
			
			REMOVE_CUTSCENE()
			i_current_event = 0
			bcutsceneplaying = FALSE
			e_section_stage = SECTION_STAGE_SETUP
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			IF NOT MANAGE_MY_TIMER(iFailSafeTimer,8000)
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDWHILE
ENDPROC


PROC TO_AIRSTRIP()

	IF GET_DISTANCE_BETWEEN_COORDS(<< -3345.1748, 3001.2778, -0.0897 >>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 500
		IF DOES_ENTITY_EXIST(mission_veh[19].veh)
			IF NOT IS_ENTITY_ON_SCREEN(mission_veh[19].veh)
				DELETE_VEHICLE(mission_veh[19].veh)
				IF DOES_ENTITY_EXIST(driver[19].ped)
					DELETE_PED(driver[19].ped)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY)
				SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MEXGOON_01)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(mission_veh[20].veh)
			IF NOT IS_ENTITY_ON_SCREEN(mission_veh[20].veh)
				DELETE_VEHICLE(mission_veh[20].veh)
				IF DOES_ENTITY_EXIST(driver[20].ped)
					DELETE_PED(driver[20].ped)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY)
				SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MEXGOON_01)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(mission_veh[21].veh)
			IF NOT IS_ENTITY_ON_SCREEN(mission_veh[21].veh)
				DELETE_VEHICLE(mission_veh[21].veh)
				IF DOES_ENTITY_EXIST(driver[21].ped)
					DELETE_PED(driver[21].ped)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY)
				SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MEXGOON_01)
			ENDIF
		ENDIF
		
	ENDIF

	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_DROP_CARGO")
		STOP_AUDIO_SCENE("TREVOR_2_DROP_CARGO")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehBuggy)
		SETUP_MISSION_REQUIREMENT(REQ_BUGGY, <<2149.5493, 4797.8740, 40.1225>>,73.9328)
	ENDIF

	//RUBBER_BAND_BETWEEN_TWO_VEHICLES(mission_veh[2].veh,mission_veh[1].veh,3,"RCSJC",75,0.6,1.0,TRUE)
	IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mission_veh[2].veh)) > 30
				reason_for_fail = VEHICLE_CUBAN_ABADONED
				eMissionStage = MISSION_STAGE_FAIL
				PRINTSTRING("CUBAN LEFT")
				PRINTNL()
			ENDIF
		ENDIF
	ENDIF
	
	//PAUSE RON ONCE HE GETS ON THE RUNWAY
	IF psRon.actionFlag != 99
		IF NOT IS_PED_INJURED(psRon.pedIndex)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF IS_PED_IN_VEHICLE(psRon.pedIndex,mission_veh[1].veh)
					IF GET_ENTITY_HEIGHT_ABOVE_GROUND(mission_veh[1].veh) < 2
						IF IS_ENTITY_IN_ANGLED_AREA(mission_veh[1].veh, <<2153.167725,4817.752441,39.222706>>, <<2094.842529,4791.237305,50.060505>>, 26.000000)
							BRING_VEHICLE_TO_HALT(mission_veh[1].veh,15,-1)
							CLEAR_PED_TASKS(psRon.pedIndex)
							psRon.actionFlag = 99
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bRetractLandingGear = TRUE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1916.274780,4707.327148,39.835453>>, <<2005.069580,4746.969238,45.818737>>, 32.500000)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TELL RON TO CIRCLE")
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[1].veh),<<1933.8745, 4714.8691, 40.1090>>) < 300
					TASK_PLANE_MISSION(psRon.pedIndex,mission_veh[1].veh,NULL,NULL,<<2284.7820, 4882.6450, 195.5418>>,MISSION_CIRCLE,30,50,-1,100,40)
					SET_LABEL_AS_TRIGGERED("TELL RON TO CIRCLE",TRUE)
				ENDIF
			ENDIF
		ELSE
			IF HAS_LABEL_BEEN_TRIGGERED("TELL RON TO CIRCLE")
				TASK_PLANE_LAND(psRon.pedIndex,mission_veh[1].veh,<<1917.36804, 4707.80420, 40.19184>>,<<2001.85132, 4746.58057, 40.06049>>)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[1].veh,TRUE)
				REQUEST_COLLISION_AT_COORD(<<1944.87964, 4720.52832, 40.06041>>)
				SET_LABEL_AS_TRIGGERED("TELL RON TO CIRCLE",FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF bRetractLandingGear = FALSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[1].veh),<<1933.8745, 4714.8691, 40.1090>>) < 300
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF NOT IS_PED_INJURED(psRon.pedIndex)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[1].veh,FALSE)
						STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh)
						TASK_PLANE_LAND(psRon.pedIndex,mission_veh[1].veh,<<1917.36804, 4707.80420, 40.19184>>,<<2001.85132, 4746.58057, 40.06049>>)
						SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[1].veh,TRUE)
						REQUEST_COLLISION_AT_COORD(<<1944.87964, 4720.52832, 40.06041>>)
						SET_ENTITY_PROOFS(mission_veh[1].veh,FALSE,FALSE,FALSE,TRUE,FALSE)
						//TASK_PLANE_MISSION(psRon.pedIndex,mission_veh[1].veh,NULL,NULL,<<2137.4839, 4810.9971, 40.1959>>,MISSION_LAND_AND_WAIT,30,5,294,40,0)
						PRINTSTRING("RON VEHICLE RETASKED")PRINTNL()
						psRon.actionFlag = 0
					ENDIF
				ENDIF
			ENDIF
			IF GET_LANDING_GEAR_STATE(mission_veh[1].veh) != LGS_DEPLOYING
			OR GET_LANDING_GEAR_STATE(mission_veh[1].veh) != LGS_LOCKED_DOWN
				CONTROL_LANDING_GEAR(mission_veh[1].veh,LGC_DEPLOY)
			ELSE
				bRetractLandingGear = TRUE
			ENDIF
		ELSE
			IF mission_veh[1].head !=0
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[1].veh,TRUE)
					REQUEST_COLLISION_AT_COORD(<<1933.8745, 4714.8691, 40.1090>>)
					REQUEST_COLLISION_AT_COORD(<<2147.4683, 4814.8496, 40.2417>>)
					mission_veh[1].head = 0
				ENDIF
			ENDIF
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
				RUBBER_BAND_BETWEEN_TWO_VEHICLES(mission_veh[2].veh,mission_veh[1].veh,<<2116.5193, 4798.7222, 40.0507>>,3,"RCSJC",40,0.7,1.2,FALSE)
			ENDIF
		ENDIF
	ELSE
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TURN OFF ENGINE")
			IF GET_SCRIPT_TASK_STATUS(enemy[iCount].ped,SCRIPT_TASK_PLANE_LAND)<> PERFORMING_TASK
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					SET_VEHICLE_ENGINE_ON(mission_veh[1].veh,FALSE,FALSE)
					SET_LABEL_AS_TRIGGERED("TURN OFF ENGINE",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SWITCH iProgress

		CASE 0
			IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLIGHT_START")
				START_AUDIO_SCENE("TREVOR_2_FLIGHT_START")
			ENDIF
			//IF IS_NEW_LOAD_SCENE_LOADED()
				NEW_LOAD_SCENE_STOP()
				IF IS_SCREEN_FADED_OUT()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				
				CLEAR_AREA(<<2129.7385, 4790.8228, 40.0188>>,100,FALSE)
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Fly to airstrip",TRUE) 
				
				// ADAM: I added this here as a way to trigger the vehicles and the launcher to come up at the mission end. It's a bit
				// early, but it does work. I also added a disable if this has been set... Let me know what you think!
				// - Ryan Paradis (3/27/12)
	//			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 	TRUE)
	//			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), TRUE)
									
				bobjectivetoggle[0] = TRUE
				//Temp Proofs
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					SET_ENTITY_PROOFS(mission_veh[1].veh,TRUE,TRUE,TRUE,TRUE,TRUE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[2].veh, TRUE)
				ENDIF
				
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_MAX_WANTED_LEVEL(0)
				bSkipped = FALSE
				iFlightHelp = 8
				brunfailchecks = TRUE
				bDialogue = FALSE
				bRetractLandingGear = FALSE
				bwinrace = FALSE
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(TRV2_RACE_TIME) 
				CONTROL_FADE_IN(500)
				iprogress++
			//ENDIF
		BREAK

		CASE 1
			IF IS_SCREEN_FADED_OUT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			IF TRIGGER_MUSIC_EVENT("TRV2_RACE")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_SCREEN_FADED_OUT()
						CONTROL_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[2].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[2].veh)
							SET_VEHICLE_FORWARD_SPEED(mission_veh[2].veh,40)
						ENDIF
						CLOSE_BOMB_BAY_DOORS(mission_veh[2].veh)
					ENDIF
					IF IS_REPLAY_IN_PROGRESS()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR_pload")
							IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR_pload", CONV_PRIORITY_MEDIUM )
								SET_LABEL_AS_TRIGGERED("TR_pload",TRUE)
							ENDIF
						ENDIF
					ENDIF
					bGoToSpeech = FALSE
					iprogress++
				ENDIF
			ENDIF
			
		BREAK
			
		CASE 2
			
			//INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TRV2_RACEBACK_WON)
			
			//TRAFICKING FLOW
			IF IS_ENTITY_IN_ANGLED_AREA(mission_veh[1].veh, <<2052.786377,4754.016113,39.823788>>, <<2039.938599,4781.490723,47.856483>>, 250.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(mission_veh[2].veh, <<2052.786377,4754.016113,39.823788>>, <<2039.938599,4781.490723,47.856483>>, 250.000000)
				IF NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR))
					bTraffickingActivated = TRUE
				ENDIF
			ENDIF
			
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<2116.5193, 4798.7222, 40.0507>>,<<0.01,0.01,0.01>>,FALSE,mission_veh[2].veh,"SJC_RACE1","SJC_GETPLNG2","SJC_GETPLNG3",TRUE)
				iprogress++
			ELSE
			
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF bGoToSpeech = FALSE
								
								IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_FLII", CONV_PRIORITY_MEDIUM)
											bGoToSpeech = TRUE
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_FLI", CONV_PRIORITY_MEDIUM)
											bGoToSpeech = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF bGoToSpeech = TRUE
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DAMAGE")
						IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
							IF GET_ENTITY_HEALTH(mission_veh[2].veh) <= 500
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_DAMAGE", CONV_PRIORITY_MEDIUM )
											SET_LABEL_AS_TRIGGERED("TR2_DAMAGE",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("TR2_DAMAGE",TRUE)
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_KEEPLOW")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_KEEPLOW", CONV_PRIORITY_MEDIUM )
									SET_LABEL_AS_TRIGGERED("TR2_KEEPLOW",TRUE)
								ENDIF
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_CONT")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_CONT", CONV_PRIORITY_MEDIUM )
										SET_LABEL_AS_TRIGGERED("TR2_CONT",TRUE)
									ENDIF
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_PAYMENT")
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_PAYMENT", CONV_PRIORITY_MEDIUM )
											SET_LABEL_AS_TRIGGERED("TR2_PAYMENT",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//MANAGE PAUSE DIALOGUE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							//CLEAR_PRINTS()
						ENDIF
					//Play conversation
					ELSE
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							//CLEAR_PRINTS()
						ENDIF
					ENDIF
				ENDIF
					
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					SET_BLIP_ROUTE(sLocatesData.LocationBlip,FALSE)
				ENDIF
				
				IF NOT DOES_BLIP_EXIST(psRon.blipIndex)
					psRon.blipIndex = CREATE_BLIP_FOR_VEHICLE(mission_veh[1].veh)
				ENDIF
				
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) AND IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
					IF IS_ENTITY_IN_ANGLED_AREA(mission_veh[2].veh, <<2052.786377,4754.016113,39.823788>>, <<2039.938599,4781.490723,47.856483>>, 250.000000)
						IF GET_ENTITY_HEIGHT_ABOVE_GROUND(mission_veh[2].veh) < 2 
							IF TRIGGER_MUSIC_EVENT("TRV2_MISSION_END")
								SET_WANTED_LEVEL_MULTIPLIER(1.0)
								REMOVE_CUTSCENE()
								REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TRV2_MCS_8",CS_SECTION_1| CS_SECTION_2)
								IF DOES_BLIP_EXIST(dest_blip)
									REMOVE_BLIP(dest_blip)
								ENDIF
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									REMOVE_BLIP(sLocatesData.LocationBlip)
								ENDIF
								IF bRaceOutCome = FALSE
									bwinrace = TRUE
									bRaceOutCome = TRUE
								ENDIF

								KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								INCREMENT_PLAYER_PED_STAT(CHAR_TREVOR, PS_FLYING_ABILITY,5 ) 
								iprogress++
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(mission_veh[1].veh, <<2052.786377,4754.016113,39.823788>>, <<2039.938599,4781.490723,47.856483>>, 250.000000)
						IF GET_ENTITY_HEIGHT_ABOVE_GROUND(mission_veh[1].veh) < 2
							IF bRaceOutCome = FALSE
								bwinrace = FALSE
								bRaceOutCome = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 3
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<2133.26, 4777.97, 39.97>>,<<0.01,0.01,0.01>>,FALSE,mission_veh[2].veh,"","SJC_GETPLNG2","SJC_GETPLNG3",TRUE)
				iprogress++
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF bwinrace = TRUE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_TBEAT")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_TBEAT", CONV_PRIORITY_MEDIUM )
										
										REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
										
										SET_LABEL_AS_TRIGGERED("TR2_TBEAT",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_RBEAT")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_RBEAT", CONV_PRIORITY_MEDIUM )
										
										REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
										
										SET_LABEL_AS_TRIGGERED("TR2_RBEAT",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF HAS_LABEL_BEEN_TRIGGERED("TR2_TBEAT")
					OR HAS_LABEL_BEEN_TRIGGERED("TR2_RBEAT")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_TAXIP")
									PRINT_GOD_TEXT("SJC_SJC_TAXIP")
									PRINT_HELP("SJC_TAXIH")
									SET_LABEL_AS_TRIGGERED("SJC_TAXIP",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2121.525635,4776.739258,46.008667>>, <<2143.957275,4786.680176,39.497566>>, 17.500000)
						IF bwinrace = TRUE
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TRV2_RACEBACK_WON)
						ENDIF
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						iprogress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			ADVANCE_MISSION_STAGE()
		BREAK
		
	ENDSWITCH

ENDPROC

INT iSceneLoadedTime
PROC PROPERTY_CUTSCENE()

	SETTIMERA(0)
	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		
		IF icutsceneprog < 7
			SET_RENDER_HD_ONLY(TRUE)
		ENDIF
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		
		SWITCH icutsceneprog
			
			CASE 0
				SET_MULTIHEAD_SAFE(TRUE, TRUE)
				PRINTLN("@@@@@@@@@@ SET_MULTIHEAD_SAFE(TRUE, TRUE) @@@@@@@@@@")
				//HANGAR SHOT
				IF NOT DOES_CAM_EXIST(cutscene_cam)		
					cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE) 
					SET_CAM_PARAMS(cutscene_cam,<<2130.668701,4814.802734,42.077812>>,<<7.432641,-1.062589,-174.085388>>,32.771206)
					SET_CAM_PARAMS(cutscene_cam,<<2130.519775,4812.049316,42.351028>>,<<7.652661,-0.093014,-177.040253>>,32.780319,12000,GRAPH_TYPE_DECEL)

					SET_CAM_FAR_CLIP(cutscene_cam,55)
					SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.05)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
				ENDIF

				IF NOT IS_PED_INJURED(psRon.pedIndex)
					DELETE_PED(psRon.pedIndex)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
					IF NOT IS_PLANE_LANDING_GEAR_INTACT(mission_veh[MV_RON_PLANE].veh)
						SET_ENTITY_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
						SET_VEHICLE_ENGINE_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
						SET_VEHICLE_PETROL_TANK_HEALTH(mission_veh[MV_RON_PLANE].veh,1000)
					ENDIF
					CONTROL_LANDING_GEAR(mission_veh[MV_RON_PLANE].veh,LGC_DEPLOY_INSTANT)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[MV_RON_PLANE].veh)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_RON_PLANE].veh, TRUE)
				ENDIF
				
				SET_GAME_PAUSES_FOR_STREAMING(FALSE)
				NEW_LOAD_SCENE_START_SPHERE(<<-68.64695, -1819.64319, 25.94197>>, 30.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
				
				START_AUDIO_SCENE("TREVOR_2_PROPERTY_INTRO_SCENE")
				SETTIMERB(0)
				icutsceneprog++
				
			BREAK
			
			CASE 1
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_PROPH1")
					ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
					TRIGGER_MUSIC_EVENT("PROP_INTRO_START")
					PRINT_HELP_FOREVER("SJC_PROPH1")
					SET_LABEL_AS_TRIGGERED("SJC_PROPH1",TRUE)
				ENDIF
				
				IF TIMERB() > 6000
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SJC_PROPH2")
						ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
						PRINT_HELP_FOREVER("SJC_PROPH2")
						SET_LABEL_AS_TRIGGERED("SJC_PROPH2",TRUE)
					ENDIF
				ENDIF

				IF (TIMERB() > 12000 AND IS_NEW_LOAD_SCENE_LOADED())
					////////////
					//GARAGE
					IF DOES_CAM_EXIST(cutscene_cam)			
						SET_CAM_PARAMS(cutscene_cam,<<-46.3, -1828.6, 28.2>>, <<5.1,0,74.6>>,29)
						SET_CAM_PARAMS(cutscene_cam,<<-49.0, -1828.9, 28.4>>, <<5.3,0,71.7>>,29,14000,GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)
						SET_CAM_FAR_CLIP(cutscene_cam,55)
						SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.09)
						RENDER_SCRIPT_CAMS(TRUE,FALSE)
					ELSE
						cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE) 
						SET_CAM_PARAMS(cutscene_cam,<<-46.3, -1828.6, 28.2>>, <<5.1,0,74.6>>,29)
						SET_CAM_PARAMS(cutscene_cam,<<-49.0, -1828.9, 28.4>>, <<5.3,0,71.7>>,29,14000,GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)
						SET_CAM_FAR_CLIP(cutscene_cam,55)
						SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.09)
						RENDER_SCRIPT_CAMS(TRUE,FALSE)
					ENDIF

					CONTROL_FADE_IN(500)
					ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
					PRINT_HELP_FOREVER("SJC_PROPH4B")

					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					//SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.09)
					
					SET_FOCUS_POS_AND_VEL(<<-68.64695, -1819.64319, 25.94197>>,<<0,0,0>>)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(),TRUE)
					SETTIMERB(0)
					icutsceneprog++
				ENDIF
			BREAK
			
			CASE 2
				NEW_LOAD_SCENE_STOP()
				SET_GAME_PAUSES_FOR_STREAMING(FALSE)
				
				//NEW_LOAD_SCENE_START_SPHERE(<<-1177.8, -1582.7, 4.1>>,  8.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
				NEW_LOAD_SCENE_START_SPHERE(<< 396.71, -709.68, 30.7 >>, 20.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
				iSceneLoadedTime = -1
				icutsceneprog++
			BREAK
			
			CASE 3
				//CINEMA
				IF iSceneLoadedTime < 0
					IF IS_NEW_LOAD_SCENE_LOADED()
						iSceneLoadedTime = GET_GAME_TIMER()
					ENDIF
				ELSE
					IF NOT IS_NEW_LOAD_SCENE_LOADED() 
						iSceneLoadedTime = -1
					ENDIF
				ENDIF
				
				IF (TIMERB() > 7000 AND iSceneLoadedTime >=0 AND GET_GAME_TIMER() >= iSceneLoadedTime + 1000)
				OR TIMERB() > 25000
				
					SET_FOCUS_POS_AND_VEL(<<403.6144, -703.1547, 28.2820>>,<<0,0,0>>)
					
					SET_CAM_PARAMS(cutscene_cam,<< 409.7, -705.3, 29.2 >>, << 9.9,-0.0,113.9 >>,39.7)
					SET_CAM_PARAMS(cutscene_cam,<< 409.6, -705.0, 29.2 >>, << 9.9,-0.0,113.9 >>,39.7,20010,GRAPH_TYPE_SIN_ACCEL_DECEL,GRAPH_TYPE_SIN_ACCEL_DECEL)
					SET_CAM_ACTIVE(cutscene_cam,TRUE)
					SET_CAM_FAR_CLIP(cutscene_cam,55)
					SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.09)
					
					ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
					PRINT_HELP_FOREVER("SJC_PROPH3")
					SETTIMERB(0)

					//NEW_LOAD_SCENE_START(<<2116.6,4794.5,45.3>>, NORMALISE_VECTOR( <<2138.5862, 4770.5527, 42.2982>> - <<2116.6,4794.5,45.3>>), 55.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
					
					//SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
					//REQUEST_COLLISION_AT_COORD(<<2120.1,4793.0,40.2>>)
					//SET_ENTITY_COORDS(PLAYER_PED_ID(),<<2120.1,4793.0,40.2>>)
					//SET_ENTITY_HEADING(PLAYER_PED_ID(),238.3386)
					//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
					
					icutsceneprog++
				ENDIF
			BREAK			
			
			CASE 4
				NEW_LOAD_SCENE_STOP()
				SET_GAME_PAUSES_FOR_STREAMING(FALSE)
				
				//NEW_LOAD_SCENE_START_SPHERE(<<-1177.8, -1582.7, 4.1>>,  8.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
				NEW_LOAD_SCENE_START(<<-1175.83, -1581.48, 5.30 >>, <<0.46,0.87,-0.16>>, 15.0, NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR)
				iSceneLoadedTime = -1
				icutsceneprog++
			BREAK
			
			CASE 5
				//WEED SHOP
				IF iSceneLoadedTime < 0
					IF IS_NEW_LOAD_SCENE_LOADED()
						iSceneLoadedTime = GET_GAME_TIMER()
					ENDIF
				ELSE
					IF NOT IS_NEW_LOAD_SCENE_LOADED()
						iSceneLoadedTime = -1
					ENDIF					
				ENDIF				
				
				IF (TIMERB() > 7000 AND iSceneLoadedTime >=0 AND GET_GAME_TIMER() >= iSceneLoadedTime + 1000)
				OR TIMERB() > 25000
				
					PRINTLN("@trevor2 TIMERB() = ",TIMERB()," @trevor2")
					IF IS_NEW_LOAD_SCENE_LOADED()
						PRINTLN("@trevor2 IS_NEW_LOAD_SCENE_LOADED() = TRUE @trevor2")
					ELSE
						PRINTLN("@trevor2 IS_NEW_LOAD_SCENE_LOADED() = FALSE @trevor2")
					ENDIF
				
					SET_FOCUS_POS_AND_VEL(<<-1177.7815, -1582.7000, 3.2610>>,<<0,0,0>>)
					
					SET_CAM_PARAMS(cutscene_cam,<<-1177.8, -1582.7, 4.1>>, <<6.7, 0.0, -31.6>>, 34)
					SET_CAM_PARAMS(cutscene_cam,<<-1177.6, -1582.8, 4.1>>, <<6.7, 0.0, -31.6>>, 34,10010,GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)
					SET_CAM_ACTIVE(cutscene_cam,TRUE)
					SET_CAM_FAR_CLIP(cutscene_cam,55)
					SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.05)
					
					ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
					PRINT_HELP_FOREVER("SJC_PROPH4")
					SETTIMERB(0)

					//NEW_LOAD_SCENE_START(<<2116.6,4794.5,45.3>>, NORMALISE_VECTOR( <<2138.5862, 4770.5527, 42.2982>> - <<2116.6,4794.5,45.3>>), 55.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
					
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
					REQUEST_COLLISION_AT_COORD(<<2120.1,4793.0,40.2>>)
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<2120.1,4793.0,40.2>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),238.3386)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
					
					icutsceneprog++
				ENDIF
			BREAK
			
			CASE 6
				NEW_LOAD_SCENE_STOP()
				SET_GAME_PAUSES_FOR_STREAMING(FALSE)
				NEW_LOAD_SCENE_START_SPHERE(<<2132.19,4782.30,41.93>>, 30, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				
				icutsceneprog++
			BREAK
			
			CASE 7
				//HANGAR
				IF (TIMERB() > 7000 AND IS_NEW_LOAD_SCENE_LOADED())
				OR TIMERB() > 25000
				//AND HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
					
					CLEAR_FOCUS()
					SET_GAME_PAUSES_FOR_STREAMING(TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					SET_RENDER_HD_ONLY(FALSE)

					SET_CAM_PARAMS(cutscene_cam,<<2116.1074, 4794.3887, 44.9460>>, <<-4.6059, -0.0000, -125.4516>>, 50)
					SET_CAM_PARAMS(cutscene_cam,<<2115.9136, 4794.5278, 41.9860>>, <<-4.6059, -0.0000, -125.4516>>, 50,7100,GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)
					SET_CAM_FAR_CLIP(cutscene_cam,100)
					SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.05)
					
					//NEW_LOAD_SCENE_START_SPHERE(<<2132.19,4782.30,41.93>>, 120, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
					
					ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
					TRIGGER_MUSIC_EVENT("PROP_INTRO_STOP")
					PRINT_HELP_FOREVER("SJC_PROPH5")
					SETTIMERB(0)
					icutsceneprog++
				ELSE
					SET_RENDER_HD_ONLY(TRUE)
				ENDIF
			BREAK
			
			CASE 8
				NEW_LOAD_SCENE_STOP()
				SET_GAME_PAUSES_FOR_STREAMING(FALSE)
				NEW_LOAD_SCENE_START_SPHERE(<<2132.19,4782.30,41.93>>, 60, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				icutsceneprog++
			BREAK
			
			CASE 9
				IF TIMERB() > 3000
					NEW_LOAD_SCENE_STOP()
					NEW_LOAD_SCENE_START_SPHERE( <<1527.0111, 5342.4849, 269.4803>>, 100, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
				ENDIF
				
				IF TIMERB() > 7000
					IF DOES_CAM_EXIST(cutscene_cam)
						//SET_CAM_ACTIVE(cutscene_cam,FALSE)
						STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					SET_WIDESCREEN_BORDERS(FALSE,0)
					CLEAR_HELP()
					CLEAR_PRINTS()
					SETTIMERB(0)
					SET_RENDER_HD_ONLY(FALSE)
					STOP_AUDIO_SCENE("TREVOR_2_PROPERTY_INTRO_SCENE") 
					icutsceneprog++
				ENDIF
			BREAK
			
			CASE 10
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVEBLENDRATIO_WALK)
				IF NOT IS_CAM_INTERPOLATING(cutscene_cam)
				OR TIMERB() > 3000
					SET_MULTIHEAD_SAFE(FALSE)
					PRINTLN("@@@@@@@@@@ SET_MULTIHEAD_SAFE(FALSE) @@@@@@@@@@")
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					NEW_LOAD_SCENE_STOP()
					CLEAR_HELP()
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					SET_GAME_PAUSES_FOR_STREAMING(TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					//DESTROY_CAM(cutscene_cam)
					icutsceneprog = 0
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					bcutsceneplaying = FALSE
				ENDIF
			BREAK

		ENDSWITCH
		
	ENDWHILE

ENDPROC

PROC CLOSING_CUTSCENE()
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISPLAY_HUD(FALSE)
	DISPLAY_RADAR(FALSE)
	
	SWITCH iProgress
	
		CASE 0
			PRINTSTRING("CLOSING CUTSCENE")PRINTNL()
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TRV2_MCS_8",CS_SECTION_1| CS_SECTION_2)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			bSkipped = FALSE
			CLEAR_HELP()
			KILL_FACE_TO_FACE_CONVERSATION()
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),FALSE,-1)
			iprogress++
		BREAK
		
		CASE 1
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(mission_veh[MV_RON_PLANE].veh)
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("TREVOR_2_FLIGHT_START")
				STOP_AUDIO_SCENE("TREVOR_2_FLIGHT_START")
			ENDIF
			RUN_LAND_PLANE_CUTSCENE()
			IF NOT IS_CUTSCENE_ACTIVE()
				IF DOES_ENTITY_EXIST(mission_veh[2].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						SET_ENTITY_INVINCIBLE(mission_veh[2].veh,FALSE)
					ENDIF
				ENDIF
				bMissionPassed = TRUE
				iprogress++
			ENDIF
		BREAK
		
		CASE 2
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				PROPERTY_CUTSCENE()
			ENDIF
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
			iprogress++
		BREAK
		
		CASE 3
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				CONTROL_FADE_IN(500)
			ENDIF
			eMissionStage = MISSION_STAGE_PASSED
		BREAK
		
	ENDSWITCH

ENDPROC

PROC MANAGE_RUNWAY_ENEMIES_TEST()
	
	//HEXER -COMING RIGHT AT THE PLANE AT THE START -VEHICLE: BIKE
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER BIKER BY BOX")
		 IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[14].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[14].veh)
						IF DOES_ENTITY_EXIST(driver[14].ped)
							IF NOT IS_PED_INJURED(driver[14].ped)
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(driver[14].ped,KNOCKOFFVEHICLE_HARD)
								SET_PED_ACCURACY(driver[14].ped,10)
								GIVE_WEAPON_TO_PED(driver[14].ped , WEAPONTYPE_PISTOL , 999, TRUE)
								SET_CURRENT_PED_WEAPON(driver[14].ped, WEAPONTYPE_PISTOL,TRUE)
								IF IS_PED_IN_ANY_VEHICLE(driver[14].ped)
									TASK_DRIVE_BY(driver[14].ped,psRon.pedIndex,NULL,<<0,0,0>>,400,80,TRUE)
								ENDIF
								SET_PED_RELATIONSHIP_GROUP_HASH(driver[14].ped ,rel_group_enemies)
								//START_PLAYBACK_RECORDED_VEHICLE(mission_veh[14].veh,31,"RCSJC")
								SET_PLAYBACK_SPEED(mission_veh[14].veh,1.0)
								IF IS_VEHICLE_DRIVEABLE(mission_veh[14].veh)
									ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[14].veh,"TREVOR_2_PLANE_WING_BIKERS_GROUP")
								ENDIF
								PRINTSTRING("TRIGGER BIKER BY BOX")PRINTNL()
								SET_LABEL_AS_TRIGGERED("TRIGGER BIKER BY BOX",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//HEX FROM BEHIND - VEHICLE: HEX
	IF NOT HAS_LABEL_BEEN_TRIGGERED("FROM BEHIND BIKER")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			REQUEST_VEHICLE_RECORDING(36,"RCSJC")
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(36,"RCSJC")
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
					IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 16153.051758
						IF IS_VEHICLE_DRIVEABLE(mission_veh[13].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[13].veh)
								START_PLAYBACK_RECORDED_VEHICLE(mission_veh[13].veh,36,"RCSJC")
							ELSE
								SET_ENTITY_PROOFS(mission_veh[13].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
								SET_PLAYBACK_SPEED(mission_veh[13].veh,1.0)
								PRINTSTRING("FROM BEHIND BIKER")PRINTNL()
								SET_LABEL_AS_TRIGGERED("FROM BEHIND BIKER",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//HEX FROM THE RUNWAY - VEHICLE: HEX
	IF NOT HAS_LABEL_BEEN_TRIGGERED("RUNWAY BIKER")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 4283.404785
					IF IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[24].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[24].veh,34,"RCSJC")
						ELSE
							SET_ENTITY_PROOFS(mission_veh[24].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
							SET_PLAYBACK_SPEED(mission_veh[24].veh,1.0)
							PRINTSTRING("RUNWAY BIKER")PRINTNL()
							SET_LABEL_AS_TRIGGERED("RUNWAY BIKER",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//RIGHT HAND SIDE -VEHICLE: VAN
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER RIGHT HAND SIDE VAN FIRST")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 4818.600098
					IF IS_VEHICLE_DRIVEABLE(mission_veh[8].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[8].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[8].veh,32,"RCSJC")
						ELSE
							SET_ENTITY_PROOFS(mission_veh[8].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
							SET_PLAYBACK_SPEED(mission_veh[8].veh,1.0)
							PRINTSTRING("TRIGGER RIGHT HAND SIDE VAN FIRST")PRINTNL()
							SET_LABEL_AS_TRIGGERED("TRIGGER RIGHT HAND SIDE VAN FIRST",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//RIGHT HAND SIDE -VEHICLE: VAN
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER LEFT HAND SIDE VAN FIRST")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 14592.761719
					IF IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[25].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[25].veh,33,"RCSJC")
						ELSE
							SET_ENTITY_PROOFS(mission_veh[25].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
							SET_PLAYBACK_SPEED(mission_veh[25].veh,1.0)
							PRINTSTRING("TRIGGER LEFT HAND SIDE VAN FIRST")PRINTNL()
							SET_LABEL_AS_TRIGGERED("TRIGGER LEFT HAND SIDE VAN FIRST",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//FINAL VAN: VAN
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER FINAL VAN")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_RON_PLANE].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_RON_PLANE].veh)
				IF GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_RON_PLANE].veh) > 15237.837891
					IF IS_VEHICLE_DRIVEABLE(mission_veh[27].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[27].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[27].veh,35,"RCSJC")
						ELSE
							SET_ENTITY_PROOFS(mission_veh[27].veh,FALSE,FALSE,TRUE,TRUE,TRUE)
							SET_PLAYBACK_SPEED(mission_veh[27].veh,1.0)
							PRINTSTRING("TRIGGER FINAL VAN")PRINTNL()
							SET_LABEL_AS_TRIGGERED("TRIGGER LEFT HAND SIDE VAN FIRST",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//15237.837891


ENDPROC

PROC TEST_STAGE()
	
	create_crates()
	
	IF iProgress > 1
		MANAGE_RUNWAY_ENEMIES_TEST()
		//MANAGE_RUNWAY_ENEMIES()
	ENDIF
	
	IF iProgress > 2
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
				PRINTSTRING("POSITION IN RECORDING:")PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(mission_veh[1].veh))PRINTNL()
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_5)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
					START_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,2,"RCSJC")
					SET_ENTITY_INVINCIBLE(mission_veh[1].veh,TRUE)
					SET_PLAYBACK_SPEED(mission_veh[1].veh,0.6)
					SET_LABEL_AS_TRIGGERED("TRIGGER BIKER BY BOX",FALSE)
					SET_LABEL_AS_TRIGGERED("TRIGGER LEFT HAND SIDE VAN FIRST",FALSE)
					SET_LABEL_AS_TRIGGERED("TRIGGER RIGHT HAND SIDE VAN FIRST",FALSE)
					SET_LABEL_AS_TRIGGERED("RUNWAY BIKER",FALSE)
				ENDIF
			ENDIF
		ENDIF
		#ENDIF
	ENDIF
	
	SWITCH iProgress
	
		CASE 0
			ADD_CARREC_REQUEST_TO_ARRAY(1,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(2,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(7,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(9,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(13,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(23,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(24,"RCSJC")
			ADD_CARREC_REQUEST_TO_ARRAY(25,"RCSJC")
			
			//FROM HILL COME OUT DOWN BY BOX - HEXER
			ADD_CARREC_REQUEST_TO_ARRAY(31,"RCSJC")
			
			//FIRST VAN BACK RIGHT
			ADD_CARREC_REQUEST_TO_ARRAY(32,"RCSJC")
			
			//FIRST VAN BACK LEFT
			ADD_CARREC_REQUEST_TO_ARRAY(33,"RCSJC")
			
			//FROM RUNWAY - FRONT HEXER
			ADD_CARREC_REQUEST_TO_ARRAY(34,"RCSJC")
			
			//LAST CHANCE VAN
			ADD_CARREC_REQUEST_TO_ARRAY(35,"RCSJC")
			
			ADD_MODEL_REQUEST_TO_ARRAY(G_M_Y_LOST_01)
			ADD_MODEL_REQUEST_TO_ARRAY(HEXER)
			ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
			ADD_MODEL_REQUEST_TO_ARRAY(TANKER)
			ADD_MODEL_REQUEST_TO_ARRAY(GBURRITO)
			
			IF NOT IS_PED_INJURED(psRon.pedIndex)
				SET_ENTITY_HEALTH(psRon.pedIndex,1000)
				SET_PED_SUFFERS_CRITICAL_HITS(psRon.pedIndex,FALSE)
				SET_ENTITY_INVINCIBLE(psRon.pedIndex,FALSE)
			ENDIF
			
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			CONTROL_FADE_IN(500)
			iProgress ++					
		BREAK
		
		CASE 1
			IF ARE_REQUESTED_CARRECS_LOADED("RCSJC")
				IF ARE_REQUESTED_MODELS_LOADED()
					//COME FROM BEHIND DRIVE AT PLAYER
					CREATE_ENEMY_VEHICLE(13,HEXER,<< 1582.0737, 3365.7207, 36.4635 >>,90.0,G_M_Y_LOST_01,TRUE,13)
					
					//BY BOX
					CREATE_ENEMY_VEHICLE(14,HEXER,<< 1748.88, 3045.37, 61.45 >>,262.61,G_M_Y_LOST_01,TRUE,14,0,FALSE,-1,WEAPONTYPE_PISTOL)
					
					//FROM THE RUNWAY
					CREATE_ENEMY_VEHICLE(24,HEXER, <<1330.4536, 3159.9248, 39.4563>>,278.9820,G_M_Y_LOST_01,TRUE,24,0,FALSE,-1,WEAPONTYPE_PISTOL)
					
					//VAN FROM LEFT BACK
					CREATE_ENEMY_VEHICLE(8,GBURRITO,<<1088.4746, 3145.3286, 39.4609>>,296.7920,G_M_Y_LOST_01,FALSE,0,0,FALSE,-1,WEAPONTYPE_PISTOL)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[8].veh, TRUE)
					//RIGHT HAND SIDE
					CREATE_ENEMY_VEHICLE(25,GBURRITO,<< 1417.43, 3006.30, 40.33 >>,283.3621,G_M_Y_LOST_01,TRUE,25,2)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[25].veh, TRUE)
					//ENEMIES COME OUT AND GO INFRONT OF PLANE
					CREATE_ENEMY_VEHICLE(27,GBURRITO,<< 1548.6367, 3140.0676, 39.5341 >>,317.6045,G_M_Y_LOST_01,TRUE,25,3)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[27].veh, TRUE)
					PRINTSTRING("MAKING MISSION VEHICLES")PRINTNL()
					iProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)			
				SET_VEHICLE_STRONG(mission_veh[1].veh,TRUE)
				SET_ENTITY_HEALTH(mission_veh[1].veh,2000)
				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
					START_PLAYBACK_RECORDED_VEHICLE(mission_veh[1].veh,2,"RCSJC")
					SET_PLAYBACK_SPEED(mission_veh[1].veh,0.6)
				ENDIF

				iProgress++
			ENDIF
		BREAK
		
		CASE 3
		
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[1].veh)
					IF GET_TIME_POSITION_IN_RECORDING(mission_veh[1].veh) > 4818.600098
						SET_PLAYBACK_SPEED(mission_veh[1].veh,0.45)
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_BRIDGES_ACHIEVEMENT()
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
		IF NOT bBridge[0]
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-136.458481,4276.760742,29.562626>>, <<-197.244232,4222.989258,43.770493>>, 9.000000)
					INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES) 
					PRINTSTRING("BRIDGE 1")PRINTNL()
					bBridge[0] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bBridge[1]
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-509.279968,4377.605957,26.793785>>, <<-523.092590,4479.797852,88.048004>>, 8.500000)
					INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES) 
					PRINTSTRING("BRIDGE 2")PRINTNL()
					bBridge[1] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bBridge[2]
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1807.457397,4712.740723,-0.492477>>, <<-1996.500854,4523.394531,56.047935>>, 19.750000)
					INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES) 
					PRINTSTRING("BRIDGE 3")PRINTNL()
					bBridge[2] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bBridge[3]
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-2701.280029,2368.255615,-0.621469>>, <<-2611.711182,2953.843262,12.483693>>, 16.250000)
					INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES) 
					PRINTSTRING("BRIDGE 4")PRINTNL()
					bBridge[3] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bBridge[4]
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1505.683838,2710.375732,-1.330758>>, <<-1378.119873,2608.957275,15.502466>>, 16.250000)
					INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES) 
					PRINTSTRING("BRIDGE 5")PRINTNL()
					bBridge[4] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bBridge[5]
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-439.501434,2967.736816,5.946320>>, <<-384.548676,2955.278809,24.554129>>, 7.250000)
					INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES) 
					PRINTSTRING("BRIDGE 6")PRINTNL()
					bBridge[5] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bBridge[6]
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<150.586868,3350.778564,25.926353>>, <<97.216026,3382.602539,47.223709>>, 5.500000)
					INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES) 
					PRINTSTRING("BRIDGE 7")PRINTNL()
					bBridge[6] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bBridge[7]
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<127.506866,3419.873535,17.409079>>, <<149.386429,3410.060791,38.730850>>, 15.000000)
					INFORM_MISSION_STATS_OF_INCREMENT(TRV2_UNDERBRIDGES) 
					PRINTSTRING("BRIDGE 8")PRINTNL()
					bBridge[7] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_BIKER_WHILE_IN_PLANE_DIALOGUE()

	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(),pedBattleEnemies)
	
	INT i
	
	FOR i = 0 TO 9
		IF DOES_ENTITY_EXIST(pedBattleEnemies[i])
			IF NOT IS_PED_INJURED(pedBattleEnemies[i])
				IF IS_PED_MODEL(pedBattleEnemies[i],G_M_Y_LOST_01)
					IF MANAGE_MY_TIMER(iAmbientDialogueTimer, 1500)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedBattleEnemies[i])) < 25
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBattleEnemies[i],"TR2_DNAA","LOST6",SPEECH_PARAMS_FORCE)
							iAmbientDialogueTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC MANAGE_RESTRICTED_AIRSTRIP_DIALOGUE()
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_MILB")
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2861.891357,3415.216553,13.777815>>, <<-1654.712402,2763.592285,467.486481>>, 325.000000)
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"T2AUD", "TR2_MILB", CONV_PRIORITY_HIGH)
						SET_LABEL_AS_TRIGGERED("TR2_MILB",TRUE)
					ENDIF
				ELSE
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_PICKUP()

	IF bCreatePickUps = FALSE
		IF NOT DOES_PICKUP_EXIST(Pickup[ENUM_TO_INT(MPICK_HEALTHPACK)])
			Pickup[ENUM_TO_INT(MPICK_HEALTHPACK)] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<1697.46, 3290.95, 40.98>>, <<-1.02, -0.35, 157.86>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(Pickup[ENUM_TO_INT(MPICK_HEALTHPACK)])
			Pickup[ENUM_TO_INT(MPICK_HEALTHPACK2)] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<1687.15, 3286.16, 41.22>>, <<-0.31, -0.27, 69.39>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(Pickup[ENUM_TO_INT(MPICK_SHOTGUN)])
			Pickup[ENUM_TO_INT(MPICK_SHOTGUN)] = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_PUMPSHOTGUN, <<1688.75, 3292.56, 40.90>>, <<-89.87, 0.35, -13.93>>, enum_to_int(PLACEMENT_FLAG_MAP),50)
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(Pickup[ENUM_TO_INT(MPICK_BODY_ARMOUR)])
			Pickup[ENUM_TO_INT(MPICK_BODY_ARMOUR)] = CREATE_PICKUP_ROTATE(PICKUP_ARMOUR_STANDARD, <<1750.3174, 3294.2048, 40.1137>>, <<-89.43, 0.29, 5.86>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF 
		
		IF NOT DOES_PICKUP_EXIST(Pickup[ENUM_TO_INT(MPICK_GRENADE)])
			Pickup[ENUM_TO_INT(MPICK_GRENADE)] = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_GRENADE, <<1727.83, 3277.28, 40.15>>, <<90.52, 30.58, -162.35>>, enum_to_int(PLACEMENT_FLAG_MAP),4)
		ENDIF

		bCreatePickUps = TRUE
	ENDIF

ENDPROC


// ********************************** START OF MAIN SCRIPT *********************************

SCRIPT

    SET_MISSION_FLAG (TRUE)
    
    IF HAS_FORCE_CLEANUP_OCCURRED() // death arrest check
		TRIGGER_MUSIC_EVENT("TRV2_MISSION_FAIL")
//		WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
//			//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
//			WAIT(0)
//		ENDWHILE
		Mission_Flow_Mission_Force_Cleanup()
		
		//Cleans up the "airfield" IPL after death/arrest. Fix for #886675
		//Search for RUN_RCI_TREVOR2() to modify what runs when this triggers. -BenR
		SET_CLEANUP_TO_RUN_ON_RESPAWN(RCI_TREVOR2)
		
        MISSION_CLEANUP(TRUE)
    ENDIF
	
	#IF IS_DEBUG_BUILD 
		//SET_CURRENT_WIDGET_GROUP(wgThisMissionsWidget)
		wgThisMissionsWidget = START_WIDGET_GROUP("Trevor 2")
			// Widgets for the Main Mission Controlers
			START_WIDGET_GROUP(" Main Mission Controlers")
				ADD_WIDGET_INT_SLIDER("eMissionStage", ieMissionStageTemp, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iSetupProgress", iSetupProgress, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iProgress", iProgress, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iFlightHelp",iFlightHelp,0,10,1)
				ADD_WIDGET_FLOAT_SLIDER("XTol",fXWidget,0,1,0.01)
				ADD_WIDGET_FLOAT_SLIDER("YTol",fYWidget,0,1,0.01)
				ADD_WIDGET_FLOAT_SLIDER("ssPlayback",ssPlayback,0,5,0.001)
				ADD_WIDGET_FLOAT_SLIDER("xTrailerForce",xTrailerForce,-10,10,0.001)
				ADD_WIDGET_FLOAT_SLIDER("yTrailerForce",yTrailerForce,-10,10,0.001)
				ADD_WIDGET_FLOAT_SLIDER("zTrailerForce",zTrailerForce,-10,10,0.001)
				

				//ADD_WIDGET_FLOAT_SLIDER("fSSRot",fSSRot,-360,360,0.01)
			STOP_WIDGET_GROUP()			
			START_NEW_WIDGET_COMBO()				
				ADD_TO_WIDGET_COMBO("SETUP")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_1_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_2_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_3_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_4_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_5_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_6_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_7_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_8_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_9_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_10_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_11_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_12_PROC")
				ADD_TO_WIDGET_COMBO("END_CUT")
				ADD_TO_WIDGET_COMBO("PASSED")
				ADD_TO_WIDGET_COMBO("FAIL")
			ADD_WIDGET_BOOL("bmissionstageloaded",bmissionstageloaded)		
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(wgThisMissionsWidget)
	#ENDIF

    // Mission Loop: Loops forever until mission is passed or failed
    WHILE TRUE
		
		//TRACE_NATIVE_COMMAND("CLEAR_AREA")

        WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TREV2")
		
		IF brunfailchecks // controls fail checks
			
		ENDIF
		
		IF eMissionStage >  MISSION_STAGE_OPENING_CUTSCENE
			IF eMissionStage != MISSION_STAGE_TEST_STAGE
				FAIL_CHECKS()
			ENDIF
			MANAGE_ENEMIES() // controls enemy behaviour
			MANAGE_AMBIENT_ANIMS() //ambient anims
			IF eMissionStage >=  MISSION_STAGE_FLIGHT
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
			ELIF eMissionStage = MISSION_STAGE_DRIVE 
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)
			ELIF eMissionStage = MISSION_STAGE_SNIPE
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
			ELIF eMissionStage = MISSION_STAGE_STEAL_PLANE
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
			ELIF eMissionStage = MISSION_STAGE_PLANE_WING
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
			ENDIF
		ENDIF
		
		IF eMissionStage >  MISSION_STAGE_PLANE_WING
			MANAGE_RESTRICTED_AIRSTRIP_DIALOGUE()
			MANAGE_CARGO()
		ENDIF
		
		IF eMissionStage = MISSION_STAGE_STEAL_PLANE
			MANAGE_PICKUP()
		ENDIF
		
		IF eMissionStage =  MISSION_STAGE_PLANE_WING
			IF iProgress > 7
				MANAGE_FLIGHT_HELP()
			ENDIF
		ELSE
			IF eMissionStage >  MISSION_STAGE_PLANE_WING
				IF eMissionStage != MISSION_STAGE_CLOSING_CUTSCENE
					MANAGE_FLIGHT_HELP()
				ENDIF
			ENDIF
		ENDIF
		
		//BIKER ON WING
		IF eMissionStage =  MISSION_STAGE_PLANE_WING
			IF iProgress != 99
				IF iProgress > 7
					MANAGE_BIKER_ON_WING()
					MANAGE_BIKER_WHILE_IN_PLANE_DIALOGUE()
				ENDIF
			ENDIF
		ELSE
			IF eMissionStage =  MISSION_STAGE_FLIGHT
				MANAGE_BIKER_ON_WING()
				IF iProgress < 2
					//MANAGE_BIKER_WHILE_IN_PLANE_DIALOGUE()
				ENDIF
			ENDIF
		ENDIF

		IF eMissionStage >  MISSION_STAGE_STEAL_PLANE
			FLIGHT_CAM_CUTS()
			MANAGE_BRIDGES_ACHIEVEMENT()
		ENDIF

		#IF IS_DEBUG_BUILD 
			IF eMissionStage =  MISSION_STAGE_DRIVE
				DONT_DO_J_SKIP(sLocatesData)
			ENDIF
			IF eMissionStage =  MISSION_STAGE_TO_AIRSTRIP
				DONT_DO_J_SKIP(sLocatesData)
			ENDIF
		#ENDIF
		
		SWITCH eMissionStage // main mission switch

			CASE MISSION_STAGE_SETUP //controls the loading of assets needed for the appropriate stage
				MISSION_SETUP()
			BREAK
			
			CASE MISSION_STAGE_OPENING_CUTSCENE //plays the opening cutscene
				OPENING_CUTSCENE()
			BREAK
			
			CASE MISSION_STAGE_DRIVE  // drive to ammunation and get a sniper rifle then drive to the water tower
				DRIVE()
			BREAK
			
			CASE MISSION_STAGE_SNIPE  // snipe the enemies and the heli
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
				SNIPE()
			BREAK
			
			CASE MISSION_STAGE_SHOOT_DOWN_HELI_CUTSCENE //cutscene showing heli being crashed
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
				RUN_SHOOT_DOWN_HELI_CUTSCENE()
			BREAK
			
			CASE MISSION_STAGE_STEAL_PLANE  // shootout at the airstrip , secure the plane
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
				STEAL_PLANE()
			BREAK
			
			CASE MISSION_STAGE_PLANE_WING // shootout while on the plane wing
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2166128
				PLANE_WING()
			BREAK
			
			CASE MISSION_STAGE_FLIGHT  // flight to drop off the cargo
				FLIGHT()
			BREAK
			
			CASE MISSION_STAGE_TO_AIRSTRIP  // flight to the airstrip
				TO_AIRSTRIP()
			BREAK
			
			CASE MISSION_STAGE_CLOSING_CUTSCENE  // end cutscene with property scene
				CLOSING_CUTSCENE()
			BREAK
			
			CASE MISSION_STAGE_TEST_STAGE  // stage for getting recordings for plane wing section.
				TEST_STAGE()
			BREAK
			
			CASE MISSION_STAGE_PASSED //mission completed
				MISSION_PASSED()
			BREAK
			
			CASE MISSION_STAGE_FAIL  //mission failed
				MISSION_FAILED()
			BREAK

		ENDSWITCH
		
		#IF IS_DEBUG_BUILD // debug controls 

			PRINT_PROGRESS()
			
			//makes the mission debug menu
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(eMissionStage), FALSE, "",TRUE,TRUE)
//				SWITCH  iReturnStage
//				CASE 2	iReturnStage = 2	BREAK
//				CASE 3	iReturnStage = 3	BREAK
//				CASE 4	iReturnStage = 4	BREAK
//				CASE 5	iReturnStage = 5	BREAK
//				CASE 6	iReturnStage = 6	BREAK
//				CASE 7	iReturnStage = 7	BREAK
//				ENDSWITCH
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-90)
				bcleanup = FALSE
				iprogress = 99
				bskipping = TRUE
				
				IF iReturnStage < 1
					iReturnStage = 1
				ENDIF
				
				IF iReturnStage != ENUM_TO_INT(MISSION_STAGE_TEST_STAGE)
					IF iReturnStage > ENUM_TO_INT(MISSION_STAGE_CLOSING_CUTSCENE) OR iReturnStage > ENUM_TO_INT(MISSION_STAGE_PASSED) 
						CONTROL_FADE_OUT(500)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
							CONTROL_LANDING_GEAR(mission_veh[2].veh,LGC_DEPLOY_INSTANT)
						ENDIF
						WARP_PLAYER(<< 2121.5569, 4803.9385, 40.1959 >>,300.1279,TRUE)
						PRINTSTRING("Jumping to finish")PRINTNL()
						eMissionStage = MISSION_STAGE_PASSED
					ELSE
						eMissionStage = INT_TO_ENUM(MISSION_STAGE_ENUM, iReturnStage)
					ENDIF
				ELSE
					eMissionStage = INT_TO_ENUM(MISSION_STAGE_ENUM, iReturnStage)
				ENDIF
			ENDIF
			
            WHILE bskipping = TRUE  //Load new stage if skipping
				IF iReturnStage != ENUM_TO_INT(MISSION_STAGE_TEST_STAGE)
					IF iReturnStage > ENUM_TO_INT(MISSION_STAGE_CLOSING_CUTSCENE)
						CONTROL_FADE_IN(500)
						bskipping = FALSE
					ELSE
						WAIT(0)
						LOAD_MISSION_STAGE(eMissionStage)
					ENDIF
				ELSE
					WAIT(0)
					LOAD_MISSION_STAGE(eMissionStage)
				ENDIF
			ENDWHILE
			
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_5)
					OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(mission_veh[2].veh)
				ENDIF
			#ENDIF
            
            IF IS_KEYBOARD_KEY_PRESSED (KEY_S)// Checks if the keyboard key S has been pressed and if sets the current mission state to complete
				eMissionStage = MISSION_STAGE_PASSED
            ENDIF
            
            IF IS_KEYBOARD_KEY_PRESSED (KEY_F)// Checks if the keyboard key F has been pressed and if so sets the current mission state to failed
				eMissionStage = MISSION_STAGE_FAIL
            ENDIF
			
		#ENDIF
        
    ENDWHILE
    
ENDSCRIPT
