// MISSION NAME : Trevor4.sc
// AUTHOR : Kev Edwards
// DESCRIPTION : Drive to the strip club with Wade

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "dialogue_public.sch"
USING "RC_Helper_Functions.sch"
USING "building_control_public.sch"
USING "script_ped.sch"
USING "CompletionPercentage_public.sch"
USING "player_ped_public.sch"
USING "CompletionPercentage_public.sch"
USING "comms_control_public.sch"
#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF
USING "context_control_public.sch" // For B*1732328 - prevent being able to use context stuff in Michael's house
USING "commands_recording.sch"
USING "cellphone_public.sch"

CONST_INT CP_DRIVE_TO_CLUB 0 // Checkpoints

CONST_INT Z_SKIP_INTRO 0 // Z Skips
CONST_INT Z_SKIP_DRIVE_TO_CLUB 1
CONST_INT Z_SKIP_OUTRO 2

ENUM TREVOR4_MISSION_STATE
	stateNULL,
	stateIntro,
	stateDriveToClub,
	stateOutro,
	stateMissionPassed,
	stateMissionFailed,
	NUM_MISSION_STATES
ENDENUM
TREVOR4_MISSION_STATE missionState = stateIntro // Stores the current TREVOR4_MISSION_STATE
TREVOR4_MISSION_STATE missionStateSkip = stateNULL // Used if needing to change to a TREVOR4_MISSION_STATE out of sequence

ENUM TREVOR4_STATE_MACHINE
	TREVOR4_STATE_MACHINE_SETUP,
	TREVOR4_STATE_MACHINE_LOOP,
	TREVOR4_STATE_MACHINE_CLEANUP
ENDENUM
TREVOR4_STATE_MACHINE missionStateMachine = TREVOR4_STATE_MACHINE_SETUP

ENUM FAILED_REASON
	FAILED_DEFAULT = 0,
	FAILED_WADE_DIED,
	FAILED_WADE_ABANDONED
ENDENUM
FAILED_REASON failReason
STRING failString

BOOL bZSkipping // Changes to TRUE if z skipping to prevent subsequent mission state changes etc

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 3
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
	BOOL bDebug_PrintToTTY = TRUE
	INT iDebugSkipTime // Prevents skipping instantaneously
	WIDGET_GROUP_ID Trev4_Widget 
#ENDIF

INT iCutsceneStage = 0

VECTOR vStripClub = <<122.50, -1323.80, 28.36>> // B*1194005 Park near bins. Previously was in front of the strip club at << 133.65984, -1307.58923, 28.03814 >>
BOOL bArrivedAtStripClub
INTERIOR_INSTANCE_INDEX interiorStripClub
INTERIOR_INSTANCE_INDEX interiorMichaelHouse

PED_INDEX pedFloyd
PED_INDEX pedWade
VECTOR vWadeStart = <<-1158.35583, -1519.05969, 3.36521>> // B*1812644 Ensure Wade starts offscreen
CONST_FLOAT fWadeStart 42.5893//29.9242 this is slightly further away from Trevor's truck
SEQUENCE_INDEX sequenceBuddy
SEQUENCE_INDEX sequencePlayer

VEHICLE_INDEX vehTrevorTruck
VECTOR vTrevorCarStart = <<-1160.15, -1514.20, 3.3>>
CONST_FLOAT fTrevorCarStart 304.66
VECTOR vTrevorStart = <<-1154.9677, -1516.5564, 3.3697>>
CONST_FLOAT fTrevorStart 28.4989

BOOL bCreatedBlood
BOOL bSkipped

structPedsForConversation sConversation
TEXT_LABEL_23 tSavedConversationRoot
TEXT_LABEL_23 tSavedConversationLabel
BOOL bDoneWhereWeGoingConversation
BOOL bDoneDriveToClubConversation
BOOL bDoneAlmostThereConversation
INT iConversationStartTimer
BOOL bWindowSmashed = FALSE

LOCATES_HEADER_DATA s_locates_data

BOOL bHaveResetForceDirectEntryFlag
BOOL bHaveSetGroupTeleport

BOOL bPlayerVehicleStoppedAtLocate
INT iHelpTextTimer

VEHICLE_INDEX vehForReplay

BOOL bIsOutroLoaded

CONST_INT NUM_BLOOD_DECALS 2
DECAL_ID decalBlood[NUM_BLOOD_DECALS]
INT iBloodDecalsCreated

CAMERA_INDEX camDoor
CAMERA_INDEX camToMichael
SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sSelectorCam
INT iSynchedScene
OBJECT_INDEX oCellPhoneMichael
VECTOR vSynchScenePlayerPos = <<-802.5565, 171.1168, 72.30>>
VECTOR vSynchScenePlayerRot = <<0, 0, 5.7971>>

VECTOR vOutroCamPos = <<102.799896,-1298.765137,29.251413>> // B*1520232
VECTOR vOutroCamRot = <<0.567974,-0.260371,31.441088>>

//OBJECT_INDEX oDoor

/// PURPOSE:
///    Used in every RCM_STATE_MACHINE_SETUP. Advances state machine to RCM_STATE_MACHINE_LOOP.
PROC TREVOR4_STATE_SETUP(#IF IS_DEBUG_BUILD STRING s_text #ENDIF)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Initialising ", s_text) ENDIF #ENDIF
	bZSkipping = FALSE
	#IF IS_DEBUG_BUILD iDebugSkipTime = GET_GAME_TIMER() #ENDIF
	missionStateMachine = TREVOR4_STATE_MACHINE_LOOP
ENDPROC

/// PURPOSE:
///    Used in every RCM_STATE_MACHINE_CLEANUP. Advances state machine to RCM_STATE_MACHINE_SETUP.
PROC TREVOR4_STATE_CLEANUP(#IF IS_DEBUG_BUILD STRING s_text #ENDIF)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Cleaning up ", s_text) ENDIF #ENDIF
	CLEAR_PRINTS()
	IF missionStateSkip = stateNULL
		missionState = INT_TO_ENUM(TREVOR4_MISSION_STATE, ENUM_TO_INT(missionState) + 1)
	ELSE
		missionState = missionStateSkip
	ENDIF
	missionStateSkip = stateNULL
	missionStateMachine = TREVOR4_STATE_MACHINE_SETUP
ENDPROC

/// PURPOSE:
///    Safely delete or release all peds in this mission.
PROC TREVOR4_REMOVE_ALL_PEDS(BOOL b_delete = FALSE)
	IF b_delete = TRUE
		SAFE_DELETE_PED(pedWade)
	ELSE
		SAFE_RELEASE_PED(pedWade)
	ENDIF
	SAFE_DELETE_PED(pedFloyd)
ENDPROC

/// PURPOSE:
///    Safely delete or release all vehicles in this mission.
PROC TREVOR4_REMOVE_ALL_VEHICLES(BOOL b_delete = FALSE)
	IF b_delete = TRUE
		SAFE_DELETE_VEHICLE(vehTrevorTruck)
		SAFE_DELETE_VEHICLE(vehForReplay)
	ELSE
		SAFE_RELEASE_VEHICLE(vehTrevorTruck)
		SAFE_RELEASE_VEHICLE(vehForReplay)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds or remove peds for conversations.
///  PARAMS:
///    b_add_peds - whether to add or remove the peds.
PROC TREVOR4_SETUP_CONVERSATION_PEDS(BOOL b_add_peds)
	IF b_add_peds = TRUE
		ADD_PED_FOR_DIALOGUE(sConversation, 1, PLAYER_PED_ID(), "TREVOR")
		IF IS_ENTITY_ALIVE(pedWade)
			ADD_PED_FOR_DIALOGUE(sConversation, 3, pedWade, "Wade")
		ENDIF
	ELSE
		REMOVE_PED_FOR_DIALOGUE(sConversation, 1)
		REMOVE_PED_FOR_DIALOGUE(sConversation, 3)
		REMOVE_PED_FOR_DIALOGUE(sConversation, 4)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if a conversation can be played.
///  PARAMS:
///    b_check_for_interrupted_conversations - Whether to additionally check if a conversation has been paused and needs to be resumed before we allow any other conversations to play.
FUNC BOOL TREVOR4_ALLOW_CONVERSATION_TO_PLAY(BOOL b_check_for_interrupted_conversations = TRUE, BOOL b_check_for_god_text = TRUE)
	IF bZSkipping = TRUE
		RETURN FALSE
	ENDIF
	IF b_check_for_god_text = TRUE // If FALSE the conversation is set to not display subtitles ever, so play conversation regardless of whether there is god text onscreen or not
	AND IS_MESSAGE_BEING_DISPLAYED()
	AND GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) > 0 // B*665755 Support playing conversations over god text when subtitles are turned off
		RETURN FALSE
	ENDIF
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN FALSE
	ENDIF
	IF b_check_for_interrupted_conversations = TRUE
	AND NOT ARE_STRINGS_EQUAL(tSavedConversationRoot, "") // A conversation has been interrupted and still needs to be restored
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Makes Wade do something appropriate when ending the mission.
PROC TREVOR4_SET_WADE_TASKS_IN_CLEANUP()
	IF IS_ENTITY_ALIVE(pedWade)
		SET_PED_KEEP_TASK(pedWade, TRUE)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			OPEN_SEQUENCE_TASK(sequenceBuddy)
				IF IS_PED_IN_ANY_VEHICLE(pedWade)
					BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(pedWade), 5, 1)
					TASK_LEAVE_VEHICLE(NULL, GET_VEHICLE_PED_IS_IN(pedWade), ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
				ENDIF
				TASK_WANDER_STANDARD(NULL)
			CLOSE_SEQUENCE_TASK(sequenceBuddy)
			TASK_PERFORM_SEQUENCE(pedWade, sequenceBuddy)
			CLEAR_SEQUENCE_TASK(sequenceBuddy)
		ELSE
			TASK_SMART_FLEE_PED(pedWade, PLAYER_PED_ID(), 100, -1)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Player is dead but Wade isn't, so making Wade run off in terror") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean up everything conceivably setup in this mission.
///  PARAMS:
///    b_terminate_mission - if the mission is about to terminate (not a z skip or retry) do some extra cleanup.
///    b_delete_entities - whether to delete entities or just release them.
PROC TREVOR4_MISSION_CLEANUP(BOOL b_delete_entities = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Cleaning up mission...") ENDIF #ENDIF
	CLEAR_PRINTS()
	CLEAR_HELP()
	IF missionState != stateMissionPassed
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	ENDIF
	SAFE_DELETE_OBJECT(oCellPhoneMichael)
	//SAFE_RELEASE_OBJECT(oDoor)
	TREVOR4_SET_WADE_TASKS_IN_CLEANUP()
	TREVOR4_SETUP_CONVERSATION_PEDS(FALSE)
	TREVOR4_REMOVE_ALL_PEDS(b_delete_entities)
	TREVOR4_REMOVE_ALL_VEHICLES(b_delete_entities)
	CLEAR_MISSION_LOCATE_STUFF(s_locates_data)
	REMOVE_VEHICLE_ASSET(BODHI2)
	INT i = 0
	REPEAT NUM_BLOOD_DECALS i
		REMOVE_DECAL(decalBlood[i]) // Remove blood decals outside the apartment
	ENDREPEAT
	SET_BUILDING_STATE( BUILDINGNAME_IPL_FLOYDS_APPARTMENT_BLOODY_WINDOW, BUILDINGSTATE_NORMAL ) // Remove Blood on window
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, FALSE) // B*1015243 - Prevent player trying to get into the same door as Wade - reset flag here in case z skipping
	ENDIF
	SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_TREVOR)
	SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_WADE)
	REMOVE_ANIM_DICT("SWITCH@MICHAEL@BENCH")
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Phone_ING)
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_GUARD_STAND", TRUE)
	DESTROY_ALL_CAMS()
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Cleaned up mission") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Mission failed function.
PROC TREVOR4_MISSION_FAILED(FAILED_REASON reason = FAILED_DEFAULT)
	IF bZSkipping = FALSE
		failReason = reason
		missionStateMachine = TREVOR4_STATE_MACHINE_CLEANUP
		missionStateSkip = stateMissionFailed
	ENDIF
ENDPROC

/// PURPOSE:
///    Death checks for any mission critical entities.
PROC TREVOR4_DEATH_CHECKS()
	IF ENUM_TO_INT(missionState) < (ENUM_TO_INT(NUM_MISSION_STATES)-1) // Don't check during stateMissionFailed
		IF DOES_ENTITY_EXIST(pedWade)
		AND IS_ENTITY_DEAD(pedWade)
			TREVOR4_MISSION_FAILED(FAILED_WADE_DIED)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates Trevor's buddy Wade.
PROC TREVOR4_SETUP_WADE()
	SAFE_DELETE_PED(pedWade)
	WHILE NOT CREATE_NPC_PED_ON_FOOT(pedWade, CHAR_WADE, vWadeStart, fWadeStart)
		WAIT(0)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Creating Wade...") ENDIF #ENDIF
	ENDWHILE
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Created Wade") ENDIF #ENDIF
	IF IS_ENTITY_ALIVE(pedWade)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedWade, RELGROUPHASH_PLAYER)
		SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedWade, VS_FRONT_RIGHT) // For B*1734008 - Ensure Wade gets into the front passenger seat
		SET_PED_CONFIG_FLAG(pedWade, PCF_ForcedToUseSpecificGroupSeatIndex, TRUE) // For B*1734008 - Ensure Wade gets into the front passenger seat
		SET_PED_CONFIG_FLAG(pedWade, PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(pedWade, PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
		SET_PED_CONFIG_FLAG(pedWade, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		SET_PED_CONFIG_FLAG(pedWade, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
		SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HAIR, 0, 0)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedWade)
		IF IS_ENTITY_ALIVE(vehTrevorTruck)
			SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(s_locates_data, vehTrevorTruck)
			TASK_ENTER_VEHICLE(pedWade, vehTrevorTruck, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
		ENDIF
	ENDIF
	IF IS_SCREEN_FADED_OUT()
		BOOL b_preload_done = FALSE
		INT i_failsafe_timer = GET_GAME_TIMER()
		WHILE NOT b_preload_done
			IF IS_ENTITY_ALIVE(pedWade)
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWade)
				OR (GET_GAME_TIMER() - i_failsafe_timer) > 5000
					b_preload_done = TRUE
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Waiting for pedWade streaming requests to be completed...") ENDIF #ENDIF
				ENDIF
			ENDIF
			WAIT(0)
		ENDWHILE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: pedWade streaming requests are now completed") ENDIF #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Screen wasn't faded out so not waiting for pedWade streaming requests") ENDIF #ENDIF
	ENDIF
	SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_WADE)
ENDPROC

/// PURPOSE:
///    Creates Trevor's truck (or grab ownership of truck created by mission's trigger scene).
PROC TREVOR4_CREATE_TREVORS_TRUCK()
	Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_VEH_T_UNLOCK_RASP_JAM, TRUE) // See B*1140958 Ensure Mr Raspberry Jam is unlocked
	IF NOT DOES_ENTITY_EXIST(vehTrevorTruck) // First, try getting the vehicle spawned by the car gen
	AND IS_ANY_VEHICLE_NEAR_POINT(vTrevorCarStart, 5)
		vehTrevorTruck = GET_CLOSEST_VEHICLE(vTrevorCarStart, 5, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
		IF DOES_ENTITY_EXIST(vehTrevorTruck)
			SET_ENTITY_AS_MISSION_ENTITY(vehTrevorTruck, TRUE, TRUE)
			IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehTrevorTruck) > 0
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Grabbed car using closest vehicle and it has sufficient seats") ENDIF #ENDIF
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Grabbed car using closest vehicle but it doesn't have sufficient seats so released it") ENDIF #ENDIF
				SAFE_RELEASE_VEHICLE(vehTrevorTruck)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: A car is near the car gen vector but couldn't grab it") ENDIF #ENDIF
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(vehTrevorTruck) // Second, try getting the vehicle spawned by the launcher
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		vehTrevorTruck = g_sTriggerSceneAssets.veh[0]
		SET_ENTITY_AS_MISSION_ENTITY(vehTrevorTruck, TRUE, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Grabbed car from launcher vehicle") ENDIF #ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(vehTrevorTruck) // Third, just spawn a new vehicle
		VECTOR v_car_spawn
		IF NOT IS_ANY_VEHICLE_NEAR_POINT(vTrevorCarStart, 5)
			v_car_spawn = vTrevorCarStart
		ELSE
			v_car_spawn = <<-1168.8463, -1509.1633, 3.1955>>
		ENDIF
		DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES()
		WHILE NOT CREATE_PLAYER_VEHICLE(vehTrevorTruck, CHAR_TREVOR, v_car_spawn, fTrevorCarStart) // B*1502630 - Create across the street
			WAIT(0)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Creating Trevor's car...") ENDIF #ENDIF
		ENDWHILE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Created Trevor's car anew") ENDIF #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Didn't create Trevor's car - must have been created during intro") ENDIF #ENDIF
	ENDIF
	MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_TREVOR_4) // Tell trigger scene to release entity from memory.
	SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_TREVOR)
ENDPROC

/// PURPOSE:
///    Sets up Trevor's truck.
PROC TREVOR4_SETUP_TREVORS_TRUCK()
	TREVOR4_CREATE_TREVORS_TRUCK()
	IF IS_ENTITY_ALIVE(vehTrevorTruck)
		IF IS_VEHICLE_MODEL(vehTrevorTruck, BODHI2)
			SET_VEHICLE_EXTRA(vehTrevorTruck, 5, FALSE) // Ensure Mr Raspberry Jam is set
		ENDIF
		IF IS_SCREEN_FADED_OUT() // B*656362
			SET_ENTITY_COORDS(vehTrevorTruck, vTrevorCarStart)
			SET_ENTITY_HEADING(vehTrevorTruck, fTrevorCarStart)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevorTruck)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Set truck on ground.") ENDIF #ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: NOT Set truck on ground.") ENDIF #ENDIF
		ENDIF
		SET_VEHICLE_DOORS_LOCKED(vehTrevorTruck, VEHICLELOCK_UNLOCKED) // In case grabbed from launcher vehicle
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Setup Trevor's car") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set up mission prior to going into any mission states.
PROC TREVOR4_MISSION_SETUP()
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Starting mission setup...") ENDIF #ENDIF
	failReason = FAILED_DEFAULT
	REQUEST_ADDITIONAL_TEXT("TRV5", MISSION_TEXT_SLOT)

	// Don't wait for it to load or it'll add about 1000ms onto the delay before the intro mocap starts
	//WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		//WAIT(0)
	//ENDWHILE

	REGISTER_SCRIPT_WITH_AUDIO()
	REQUEST_VEHICLE_ASSET(BODHI2)
	#IF IS_DEBUG_BUILD
		mSkipMenu[Z_SKIP_INTRO].sTxtLabel = "Intro - trv_5_int"
		mSkipMenu[Z_SKIP_DRIVE_TO_CLUB].sTxtLabel = "Drive to strip club"
		mSkipMenu[Z_SKIP_OUTRO].sTxtLabel = "Outro - trv_5_ext"
		IF NOT DOES_WIDGET_GROUP_EXIST(Trev4_Widget)			
			Trev4_Widget = START_WIDGET_GROUP("Trevor 4")	
			STOP_WIDGET_GROUP()
			SET_LOCATES_HEADER_WIDGET_GROUP(Trev4_Widget)
		ENDIF
	#ENDIF
	tSavedConversationRoot = ""
	DISABLE_SELECTOR() // Prevent being able to switch
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Finished mission setup") ENDIF #ENDIF
	
ENDPROC

/// PURPOSE:
///    Create blood decals outside the apartment, see B*1141089.
PROC TREVOR4_CREATE_BLOOD_DECALS()
	IF bCreatedBlood = TRUE
		SWITCH iBloodDecalsCreated
			CASE 0
				decalBlood[0] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<-1150.07, -1521.87, 9.63>>, <<0.0, 0.0, -1.0>>, normalise_vector(<<0.0, 1.0, 0.0>>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
				iBloodDecalsCreated++
			BREAK
			CASE 1
				decalBlood[1] = ADD_DECAL(DECAL_RSID_BLOOD_DIRECTIONAL, <<-1149.45, -1522.60, 9.63>>, <<0.0, 0.0, -1.0>>, normalise_vector(<<0.0, 1.0, 0.0>>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
				iBloodDecalsCreated++
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC TREVOR4_HANDLE_BROKEN_WINDOW( BOOL bSmashNow )
	
	IF bWindowSmashed = TRUE
		//CPRINTLN(DEBUG_MISSION, "TREVOR4: WINDOW ALREADY SWAPPED")
		EXIT
	ENDIF
	
	IF bSmashnow = FALSE 
		IF GET_CUTSCENE_TIME() > 16000
			CPRINTLN(DEBUG_MISSION, "TREVOR4: CUTSCENE TIME OK FOR CREATING SMASHED WINDOW") 
			bSmashnow = TRUE 
		ENDIF
	ENDIF
	
	IF bSmashnow = TRUE
	
		SET_BUILDING_STATE( BUILDINGNAME_IPL_FLOYDS_APPARTMENT_BLOODY_WINDOW, BUILDINGSTATE_DESTROYED)
		bWindowSmashed = TRUE
		
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Handles creating blood decals on Trevor during the intro mocap after he's shot Floyd and Debra.
PROC TREVOR4_MANAGE_BLOOD_ON_TREVOR(BOOL b_create_blood_now)
	//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, GET_GAME_TIMER() - iCreateBloodTimer) ENDIF #ENDIF
	IF bCreatedBlood = FALSE
		IF b_create_blood_now = FALSE
		AND GET_CUTSCENE_TIME() > 129300
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: GET_CUTSCENE_TIME returns true so create blood and preload models") ENDIF #ENDIF
			TREVOR4_CREATE_TREVORS_TRUCK() // B*1304001 Ensure truck is visible during end of intro
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Created Trevor's car during intro") ENDIF #ENDIF
			IF bSkipped = FALSE
				SAFE_FADE_SCREEN_OUT_TO_BLACK(0, TRUE)
				WAIT(2000)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Doing fade for mocap") ENDIF #ENDIF
				SAFE_FADE_SCREEN_IN_FROM_BLACK(4000, FALSE)
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Cutscene was skipped, so not doing fade for mocap") ENDIF #ENDIF
			ENDIF
			b_create_blood_now = TRUE
		ENDIF
		IF b_create_blood_now = TRUE
			bCreatedBlood = TRUE
			// Upper torso...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.828, 0.615, 160, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.689, 0.648, 290, 0.95, 1, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.639, 0.607, 30, 0.9, 2, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.723, 0.576, 200, 0.85, 3, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.712, 0.525, 260, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			// Lower torso...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.575, 0.451, 10, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.607, 0.361, 109, 0.95, 1, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.451, 0.385, 203, 0.9, 2, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.754, 0.320, 248, 0.85, 3, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.836, 0.369, 330, 0.8, 0, 0.0, "Scripted_Ped_Splash_Back")
			// Right leg...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 5, 0.164, 0.762, 18, 1, 1, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 5, 0.352, 0.590, 120, 0.95, 2, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 5, 0.492, 0.852, 204, 0.9, 3, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 5, 0.322, 0.452, 320, 0.85, 0, 0.0, "Scripted_Ped_Splash_Back")
			// Left leg...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 4, 0.607, 0.852, 10, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 4, 0.754, 0.738, 103, 0.95, 1, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 4, 0.697, 0.615, 210, 0.9, 2, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 4, 0.582, 0.680, 306, 0.85, 3, 0.0, "Scripted_Ped_Splash_Back")
			// Left foot...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 4, 0.797, 0.299, 50, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 4, 0.655, 0.203, 203, 0.95, 1, 0.0, "Scripted_Ped_Splash_Back")
			// Left arm...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 2, 0.701, 0.582, 100, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 2, 0.650, 0.362, 250, 0.95, 1, 0.0, "Scripted_Ped_Splash_Back")
			// Right arm...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 3, 0.254, 0.514, 30, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 3, 0.418, 0.610, 100, 0.95, 1, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 3, 0.441, 0.373, 200, 0.9, 2, 0.0, "Scripted_Ped_Splash_Back")
			// These match...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.574, 0.230, 33, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 5, 0.377, 0.985, 212, 0.95, 0, 0.0, "Scripted_Ped_Splash_Back")
			// These match...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.811, 0.230, 57, 1, 1, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 4, 0.713, 0.985, 323, 0.95, 1, 0.0, "Scripted_Ped_Splash_Back")
			// Head...
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 1, 0.644, 0.712, 10, 1, 0, 0.0, "Scripted_Ped_Splash_Back")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 1, 0.486, 0.588, 100, 0.95, 1, 0.0, "Scripted_Ped_Splash_Back")
			iBloodDecalsCreated = 0
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Common stuff setup after the intro or when retrying the mission from the post-intro checkpoint.
PROC TREVOR4_COMMON_POST_INTRO_SETUP()
	CLEAR_AREA(vTrevorCarStart, 100.0, TRUE)
	IF NOT IS_REPLAY_BEING_SET_UP()
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vTrevorStart, fTrevorStart)
	ENDIF
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	TREVOR4_HANDLE_BROKEN_WINDOW(TRUE)
	TREVOR4_MANAGE_BLOOD_ON_TREVOR(TRUE)
	TREVOR4_SETUP_TREVORS_TRUCK()
	TREVOR4_SETUP_WADE()
	TREVOR4_SETUP_CONVERSATION_PEDS(TRUE)
ENDPROC

/// PURPOSE:
///    Handles debug skipping.
///  PARAMS:
///    i_new_state - The state to debug skip to.
PROC TREVOR4_DEBUG_SKIP_STATE(INT i_new_state)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Starting debug skip to ", i_new_state, "...") ENDIF #ENDIF
	RC_START_Z_SKIP()
	bZSkipping = TRUE
	tSavedConversationRoot = ""
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vTrevorStart) // Ensure player isn't in the truck
	ENDIF
	TREVOR4_MISSION_CLEANUP(TRUE)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
	ENDIF
	REQUEST_ADDITIONAL_TEXT("TRV5", MISSION_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT) // Didn't wait for the mission text to load in the mission setup so ensure it's loaded
		WAIT(0)
	ENDWHILE
	SWITCH i_new_state
		CASE Z_SKIP_INTRO
			missionStateSkip = stateIntro
		BREAK
		CASE Z_SKIP_DRIVE_TO_CLUB
			//CREATE_VEHICLE_FOR_REPLAY(vehForReplay, vTrevorCarStart, fTrevorCarStart, FALSE, FALSE, FALSE, FALSE, FALSE) this was causing B*1932017
			TREVOR4_COMMON_POST_INTRO_SETUP()
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, TRUE) // B*1015243 - Prevent player trying to get into the same door as Wade
				bHaveResetForceDirectEntryFlag = FALSE
			ENDIF
			missionStateSkip = stateDriveToClub
		BREAK
		CASE Z_SKIP_OUTRO
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vStripClub)
			ENDIF
			missionStateSkip = stateOutro
		BREAK
	ENDSWITCH
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID())) // B*551971 - User could be z skipping across map so wait for the streaming to catch up
	ENDIF
	END_REPLAY_SETUP()
	IF i_new_state = Z_SKIP_DRIVE_TO_CLUB
		SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 0.0, TRUE, TRUE)
		FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT) 
		RC_END_Z_SKIP()
	ELSE
		RC_END_Z_SKIP(TRUE, FALSE)
	ENDIF
	missionStateMachine = TREVOR4_STATE_MACHINE_CLEANUP
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Finished debug skip to ", i_new_state) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Returns true if Trevor and Wade are in the same vehicle. See B*1464354.
FUNC BOOL TREVOR4_IS_PLAYER_AND_WADE_IN_SAME_VEHICLE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(pedWade)
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(pedWade)
		VEHICLE_INDEX veh_trevor = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		VEHICLE_INDEX veh_wade = GET_VEHICLE_PED_IS_IN(pedWade)
		IF veh_trevor = veh_wade
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays the conversation immediately after the intro.
PROC TREVOR4_MANAGE_DRIVE_TO_CLUB_CONVERSATIONS()
	IF bDoneWhereWeGoingConversation = FALSE
		IF (GET_GAME_TIMER() - iConversationStartTimer) > 250
		AND TREVOR4_ALLOW_CONVERSATION_TO_PLAY()
		AND CREATE_CONVERSATION(sConversation, "TRV5AUD", "TRV5_where", CONV_PRIORITY_HIGH)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Started TRV5_where conversation") ENDIF #ENDIF
			bDoneWhereWeGoingConversation = TRUE
		ENDIF
	ELIF bDoneDriveToClubConversation = FALSE
		IF TREVOR4_ALLOW_CONVERSATION_TO_PLAY()
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		AND TREVOR4_IS_PLAYER_AND_WADE_IN_SAME_VEHICLE()
		AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedWade, 10)
		AND CREATE_CONVERSATION(sConversation, "TRV5AUD", "TRV5_Start", CONV_PRIORITY_HIGH)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Started TRV5_Start conversation") ENDIF #ENDIF
			bDoneDriveToClubConversation = TRUE
		ENDIF
	ELIF bArrivedAtStripClub = FALSE
		IF ARE_STRINGS_EQUAL(tSavedConversationRoot, "")
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_ENTITY_ALIVE(pedWade)
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedWade, 10)
				OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				OR NOT TREVOR4_IS_PLAYER_AND_WADE_IN_SAME_VEHICLE()
					tSavedConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					tSavedConversationLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Stopping and storing conversation ", tSavedConversationRoot) ENDIF #ENDIF
					KILL_ANY_CONVERSATION()
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						CLEAR_PRINTS() // Clear prints so "Lose the cops" can be displayed
					ENDIF
				ENDIF
			ENDIF
		ELSE // A conversation has been stored so we need to check for conditions to restore it
			IF IS_ENTITY_ALIVE(pedWade)
			AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedWade, 10)
			AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			AND TREVOR4_IS_PLAYER_AND_WADE_IN_SAME_VEHICLE()
			AND TREVOR4_ALLOW_CONVERSATION_TO_PLAY(FALSE)
			AND CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sConversation, "TRV5AUD", tSavedConversationRoot, tSavedConversationLabel, CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Restoring conversation ", tSavedConversationRoot) ENDIF #ENDIF
				tSavedConversationRoot = ""
			ENDIF
		ENDIF
	ENDIF
	IF bDoneAlmostThereConversation = FALSE
	AND TREVOR4_ALLOW_CONVERSATION_TO_PLAY()
	AND IS_ENTITY_ALIVE(pedWade)
	AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedWade, vStripClub) < 40.0
	AND CREATE_CONVERSATION(sConversation, "TRV5AUD", "TRV5_arrive", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Started TRV5_arrive conversation") ENDIF #ENDIF
		bDoneAlmostThereConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the player going to the strip club.

BOOL bVehicleGrabbedForStats = FALSE //adding a simple flag to check for vehicle player is driving benig grabbed for stat watching

PROC TREVOR4_MANAGE_DRIVE_TO_CLUB()
	#IF IS_DEBUG_BUILD DONT_DO_J_SKIP(s_locates_data) #ENDIF
	IF bArrivedAtStripClub = FALSE
		IF IS_ENTITY_ALIVE(pedWade)
			IF bDoneDriveToClubConversation = TRUE // Hack for B*1512406
			OR (bDoneWhereWeGoingConversation = TRUE AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()) // Hack for B*1512406
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(s_locates_data, vStripClub, g_vAnyMeansLocate, TRUE, pedWade, "TRV5_CLUB", "TRV5_WADE", FALSE, TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					KILL_ANY_CONVERSATION()
					bArrivedAtStripClub = TRUE
					bPlayerVehicleStoppedAtLocate = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Player and Wade arrived at strip club") ENDIF #ENDIF
				ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedWade) > 250.0
					TREVOR4_MISSION_FAILED(FAILED_WADE_ABANDONED)
				ENDIF
			ENDIF
			IF bHaveResetForceDirectEntryFlag = FALSE
			AND IS_PED_SITTING_IN_ANY_VEHICLE(pedWade)
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				bHaveResetForceDirectEntryFlag = TRUE
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, FALSE) // B*1015243 - Prevent player trying to get into the same door as Wade - turn back to normal here
			ENDIF
			IF bHaveSetGroupTeleport = FALSE
			AND IS_PED_IN_GROUP(pedWade)
				SET_PED_CAN_TELEPORT_TO_GROUP_LEADER(pedWade, GET_PED_GROUP_INDEX(PLAYER_PED_ID()), TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: SET_PED_CAN_TELEPORT_TO_GROUP_LEADER") ENDIF #ENDIF
				bHaveSetGroupTeleport = TRUE
			ENDIF
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT bVehicleGrabbedForStats
						INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) // B*1539373 Stat watching
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
						bVehicleGrabbedForStats = TRUE
					ENDIF
				ELSE
					IF bVehicleGrabbedForStats = TRUE
						INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL) 
						bVehicleGrabbedForStats = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Player at locate with Wade and not in vehicle") ENDIF #ENDIF
				missionStateMachine = TREVOR4_STATE_MACHINE_CLEANUP
			ENDIF
		ELSE
			IF bPlayerVehicleStoppedAtLocate = FALSE
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),10.0)
					
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
					
					bPlayerVehicleStoppedAtLocate = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Player vehicle now stopped at locate") ENDIF #ENDIF
				ENDIF
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AM_H_TRCLUB")
					PRINT_HELP_FOREVER("AM_H_TRCLUB")
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Printing help AM_H_TRCLUB") ENDIF #ENDIF
				ENDIF
			ELSE
				IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
					OPEN_SEQUENCE_TASK(sequencePlayer)
						TASK_LEAVE_ANY_VEHICLE(NULL, 1000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<131,-1302,29.2>>, PEDMOVEBLENDRATIO_WALK)
					CLOSE_SEQUENCE_TASK(sequencePlayer)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), sequencePlayer)
					CLEAR_SEQUENCE_TASK(sequencePlayer)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Player tasked to leave vehicle") ENDIF #ENDIF
				ENDIF
				IF IS_ENTITY_ALIVE(pedWade)
				AND NOT IsPedPerformingTask(pedWade, SCRIPT_TASK_PERFORM_SEQUENCE)
					OPEN_SEQUENCE_TASK(sequenceBuddy)
						TASK_LEAVE_ANY_VEHICLE(NULL, 1250)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<131,-1302,29.2>>, PEDMOVEBLENDRATIO_WALK)
					CLOSE_SEQUENCE_TASK(sequenceBuddy)
					TASK_PERFORM_SEQUENCE(pedWade, sequenceBuddy)
					CLEAR_SEQUENCE_TASK(sequenceBuddy)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: pedWade tasked to leave vehicle") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Load/unload outro cutscene depending on distance.
PROC TREVOR4_MANAGE_OUTRO_CUTSCENE_LOADING()
	IF IS_ENTITY_ALIVE(pedWade)
		IF bIsOutroLoaded = FALSE
			IF IS_ENTITY_IN_RANGE_COORDS(pedWade, vStripClub, DEFAULT_CUTSCENE_LOAD_DIST)
				REQUEST_CUTSCENE("TRV_5_EXT")
				bIsOutroLoaded = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Loaded TRV_5_EXT into memory") ENDIF #ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_RANGE_COORDS(pedWade, vStripClub, DEFAULT_CUTSCENE_UNLOAD_DIST)
			AND HAS_CUTSCENE_LOADED()
				REMOVE_CUTSCENE()
				bIsOutroLoaded = FALSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Unloaded TRV_5_EXT from memory") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Standard mission cleanup function.
PROC TREVOR4_Script_Cleanup()
	TREVOR4_MISSION_CLEANUP()
	ENABLE_SELECTOR()
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Mission passed function.
PROC Script_Passed()
	// set strip club property owned
	g_savedGlobals.sPropertyData.propertyOwnershipData[PROPERTY_STRIP_CLUB].charOwner = CHAR_TREVOR
	SET_BITMASK_AS_ENUM(g_savedGlobals.sPropertyData.propertyOwnershipData[PROPERTY_STRIP_CLUB].iBit, PROPERTY_BIT_SHOULD_RECEIVE_PAYMENT_THIS_WEEK)
	
	Mission_Flow_Mission_Passed()
	
	IF IS_MINIGAME_ACTIVE(MINIGAME_BASEJUMPING)
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_BASEJUMP_RUNAWAY_TRAIN, TRUE)
	ENDIF
	
	TREVOR4_Script_Cleanup()
ENDPROC

/// PURPOSE:
///    Main state for handling the intro cutscene.
PROC MS_INTRO()
	SWITCH missionStateMachine
		CASE TREVOR4_STATE_MACHINE_SETUP
			TREVOR4_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_INTRO" #ENDIF)
			RC_REQUEST_CUTSCENE("TRV_5_INT")
			iCutsceneStage = 0
			bCreatedBlood = FALSE
			bSkipped = FALSE
		BREAK
		CASE TREVOR4_STATE_MACHINE_LOOP
			IF bCreatedBlood = FALSE // Stop ambient peds spawning outside while the interior mocap is ongoing
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Stopping ambient and scenario peds spawning") ENDIF #ENDIF
			ENDIF
			SWITCH iCutsceneStage
				CASE 0
					IF RC_IS_CUTSCENE_OK_TO_START()
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(PLAYER_PED_ID()), CEO_PRESERVE_BODY_BLOOD_DAMAGE|CEO_PRESERVE_FACE_BLOOD_DAMAGE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: CU_ANIMATE_EXISTING_SCRIPT_ENTITY Trevor") ENDIF #ENDIF
						ENDIF
						IF IS_ENTITY_ALIVE(g_sTriggerSceneAssets.ped[0])
							pedFloyd = g_sTriggerSceneAssets.ped[0]
							SET_ENTITY_AS_MISSION_ENTITY(pedFloyd, TRUE, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(pedFloyd, "Floyd", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_FLOYD))
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: CU_ANIMATE_EXISTING_SCRIPT_ENTITY Floyd") ENDIF #ENDIF
						ENDIF
						// Might not be required - door seems to be working now - previously was creating an extra door
						/*IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_T_APARTMENT_VB))
							ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_T_APARTMENT_VB), v_ilev_trev_doorfront, <<-1149.71, -1521.09, 10.78>>)
						ENDIF
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1149.71, -1521.09, 10.78>>, 2.0, v_ilev_trev_doorfront)	
							oDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<-1149.71, -1521.09, 10.78>>, 2.0, v_ilev_trev_doorfront)
							REGISTER_ENTITY_FOR_CUTSCENE(oDoor, "V_ILev_Trev_DoorFront", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, v_ilev_trev_doorfront)
							#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "CU_ANIMATE_EXISTING_SCRIPT_ENTITY oDoor") #ENDIF
						ENDIF*/
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_TREVOR_4)
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: REPLAY_START_EVENT for intro") ENDIF #ENDIF
						START_CUTSCENE()
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_PLAYING()
						VEHICLE_INDEX vehTemp
						vehTemp = GET_LAST_DRIVEN_VEHICLE()
						IF IS_ENTITY_ALIVE(vehTemp)
							IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehTemp) > 0
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: last player vehicle has sufficient seats so trying to set it as a car gen") ENDIF #ENDIF
								RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1166.471558,-1504.612793,2.379442>>, <<-1150.952637,-1527.267090,6.269411>>, 10.000000, vTrevorCarStart, fTrevorCarStart, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()) // B*1932017
								SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vTrevorCarStart, fTrevorCarStart)
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: last player vehicle doesn't have sufficient seats so setting it as a car gen at Trevor's safehouse") ENDIF #ENDIF
								SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0, 0, 0>>, 0, TRUE, CHAR_TREVOR)
							ENDIF
						ENDIF
						RC_START_CUTSCENE_MODE(vTrevorCarStart)
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100) // Clear ambient peds outside while the interior mocap is ongoing
						SAFE_RELEASE_PED(pedFloyd)
						SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_FLOYD)
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, TRUE) // B*1015243 - Prevent player trying to get into the same door as Wade
							bHaveResetForceDirectEntryFlag = FALSE
						ENDIF
						REQUEST_PLAYER_VEH_MODEL(CHAR_TREVOR) // Ensure model is loaded for use immediately after intro
						REQUEST_NPC_PED_MODEL(CHAR_WADE) // Ensure model is loaded for use immediately after intro
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 2
					IF bSkipped = FALSE
					AND WAS_CUTSCENE_SKIPPED()
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Cutscene was skipped") ENDIF #ENDIF
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						bSkipped = TRUE
					ENDIF
					IF IS_CUTSCENE_PLAYING()
						TREVOR4_HANDLE_BROKEN_WINDOW(FALSE)
						TREVOR4_MANAGE_BLOOD_ON_TREVOR(FALSE)
						TREVOR4_CREATE_BLOOD_DECALS()
						IF bSkipped = FALSE
						AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor", GET_PLAYER_PED_MODEL(CHAR_TREVOR))
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY returned true, doing TREVOR4_COMMON_POST_INTRO_SETUP") ENDIF #ENDIF
							TREVOR4_COMMON_POST_INTRO_SETUP()
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 0.0, TRUE, TRUE)
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT) 
						ENDIF
					ELSE
						REPLAY_STOP_EVENT()
						
						REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: REPLAY_STOP_EVENT for intro") ENDIF #ENDIF
						REQUEST_ADDITIONAL_TEXT("TRV5", MISSION_TEXT_SLOT) // Didn't wait for the mission text to load in the mission setup so ensure it's loaded
						WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
							WAIT(0)
						ENDWHILE
						IF bSkipped = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: bSkipped = TRUE, doing TREVOR4_COMMON_POST_INTRO_SETUP") ENDIF #ENDIF
							TREVOR4_COMMON_POST_INTRO_SETUP()
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 0.0, TRUE, TRUE)
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT) 
						ENDIF
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
						RC_END_CUTSCENE_MODE()
						//SAFE_RELEASE_OBJECT(oDoor)
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: IS_CUTSCENE_PLAYING returned false") ENDIF #ENDIF
						missionStateMachine = TREVOR4_STATE_MACHINE_CLEANUP
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE TREVOR4_STATE_MACHINE_CLEANUP
			TREVOR4_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_INTRO" #ENDIF)
			SAFE_DELETE_PED(pedFloyd)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling driving to the strip club.
PROC MS_DRIVE_TO_CLUB()
	SWITCH missionStateMachine
		CASE TREVOR4_STATE_MACHINE_SETUP
			TREVOR4_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_DRIVE_TO_CLUB" #ENDIF)
			bDoneWhereWeGoingConversation = FALSE
			bDoneDriveToClubConversation = FALSE
			bDoneAlmostThereConversation = FALSE
			iConversationStartTimer = GET_GAME_TIMER()
			bArrivedAtStripClub = FALSE
			bIsOutroLoaded = FALSE
			bHaveSetGroupTeleport = FALSE
			SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_GUARD_STAND", FALSE)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_trev_doorfront, <<-1149.71, -1521.09, 10.78>>, TRUE, 0.0) // See B*1812652 - this was previously in the trigger scene but was causing a pop in the frame prior to the cutscene, so now locking the door here
		BREAK
		CASE TREVOR4_STATE_MACHINE_LOOP
			TREVOR4_MANAGE_DRIVE_TO_CLUB()
			TREVOR4_MANAGE_DRIVE_TO_CLUB_CONVERSATIONS()
			TREVOR4_MANAGE_OUTRO_CUTSCENE_LOADING()
			TREVOR4_CREATE_BLOOD_DECALS()
		BREAK
		CASE TREVOR4_STATE_MACHINE_CLEANUP
			TREVOR4_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_DRIVE_TO_CLUB" #ENDIF)
			KILL_FACE_TO_FACE_CONVERSATION()
			REMOVE_VEHICLE_ASSET(BODHI2)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the outro cutscene.
PROC MS_OUTRO()

	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_HangTen")

	SWITCH missionStateMachine
		CASE TREVOR4_STATE_MACHINE_SETUP
			TREVOR4_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_OUTRO" #ENDIF)
			RC_REQUEST_CUTSCENE("TRV_5_EXT")
			interiorStripClub = GET_INTERIOR_AT_COORDS(<<130.2632, -1295.0345, 28.2695>>)
			PIN_INTERIOR_IN_MEMORY(interiorStripClub)
			WHILE NOT IS_INTERIOR_READY(interiorStripClub)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Loading strip club interior...") ENDIF #ENDIF
				WAIT(0)
			ENDWHILE
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Loaded strip club interior") ENDIF #ENDIF
			iCutsceneStage = 0
		BREAK
		CASE TREVOR4_STATE_MACHINE_LOOP
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SWITCH iCutsceneStage
					CASE 0
						IF RC_IS_CUTSCENE_OK_TO_START()
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL(PLAYER_PED_ID()), CEO_PRESERVE_BODY_BLOOD_DAMAGE|CEO_PRESERVE_FACE_BLOOD_DAMAGE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: CU_ANIMATE_EXISTING_SCRIPT_ENTITY Trevor") ENDIF #ENDIF
							REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_HangTen")
							REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: REPLAY_START_EVENT for outro") ENDIF #ENDIF
							START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
							SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 1
						IF IS_CUTSCENE_PLAYING()
							SAFE_DELETE_PED(pedWade)
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<128.731430,-1298.473999,27.232737>>, <<133.044540,-1305.980469,32.157188>>, 5.000000, <<124.4367, -1322.2213, 28.2880>>, 304.2106)
							SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<124.4367, -1322.2213, 28.2880>>, 304.2106)
							RC_START_CUTSCENE_MODE(vStripClub, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
							SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
							iHelpTextTimer = GET_GAME_TIMER()
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 2
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						IF IS_CUTSCENE_PLAYING()
							IF (GET_GAME_TIMER() - iHelpTextTimer) > 3000
							AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AM_H_TRCLUB")
								CLEAR_HELP()
							ENDIF
							IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor", GET_PLAYER_PED_MODEL(CHAR_TREVOR))
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Trevor returned true") ENDIF #ENDIF
								SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<98.5821, -1292.2389, 28.2688>>, 101.8921)
							ENDIF
							IF NOT DOES_CAM_EXIST(camDoor)
								IF CAN_SET_EXIT_STATE_FOR_CAMERA()
								AND NOT WAS_CUTSCENE_SKIPPED()
									camDoor = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
									SET_CAM_PARAMS(camDoor, vOutroCamPos, vOutroCamRot, 23.112051, 0, GRAPH_TYPE_ACCEL, GRAPH_TYPE_ACCEL)
									SET_CAM_PARAMS(camDoor, <<103.1056, -1299.2651, 29.2456>>, <<0.5680, -0.2604, 31.4411>>, 23.112051, 15000, GRAPH_TYPE_ACCEL, GRAPH_TYPE_ACCEL) // B*1520239 Slight pull back from door
									SHAKE_CAM(camDoor, "HAND_SHAKE", 0.25)
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Created camDoor") ENDIF #ENDIF
								ENDIF
							ENDIF
						ELSE
							REPLAY_STOP_EVENT()
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: REPLAY_STOP_EVENT for outro") ENDIF #ENDIF
							CLEAR_HELP() // Ensure AM_H_TRCLUB is cleared
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							UNPIN_INTERIOR(interiorStripClub)
							interiorMichaelHouse = GET_INTERIOR_AT_COORDS(vSynchScenePlayerPos)
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 3
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						PIN_INTERIOR_IN_MEMORY(interiorMichaelHouse)
						IF IS_INTERIOR_READY(interiorMichaelHouse)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Loaded Michael house interior") ENDIF #ENDIF
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 4 // Create Michael for switch
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						REQUEST_ANIM_DICT("SWITCH@MICHAEL@BENCH")
						REQUEST_MODEL(Prop_Phone_ING)
						IF HAS_ANIM_DICT_LOADED("SWITCH@MICHAEL@BENCH")
						AND HAS_MODEL_LOADED(Prop_Phone_ING)
						AND CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vSynchScenePlayerPos, vSynchScenePlayerRot.z)
							PRELOAD_STORED_PLAYER_PED_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							sSelectorPeds.bInitialised = TRUE
							sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MICHAEL
							sSelectorCam.pedTo = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
							STORE_PLAYER_PED_VARIATIONS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							RELEASE_PED_PRELOAD_VARIATION_DATA(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Created Michael") ENDIF #ENDIF
							IF NOT DOES_CAM_EXIST(camToMichael)
								camToMichael = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
								SET_CAM_PARAMS(camToMichael, <<-802.0038, 169.6107, 73.5191>>, <<-6.5365, 0.0000, 6.2441>>, 50, 0)
							ENDIF
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 5 // Do switch scene
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						IF RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM(sSelectorCam, camToMichael, SWITCH_TYPE_AUTO, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO), DEFAULT, DEFAULT, DEFAULT, DEFAULT, "v_michael")
							SWITCH_STATE swState
							swState = GET_PLAYER_SWITCH_STATE()
							IF swState >= SWITCH_STATE_JUMPCUT_ASCENT
								SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
							ENDIF
							IF sSelectorCam.bOKToSwitchPed = TRUE
							AND sSelectorCam.bPedSwitched = FALSE
							AND TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE, FALSE)
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								iSynchedScene = CREATE_SYNCHRONIZED_SCENE(vSynchScenePlayerPos, vSynchScenePlayerRot)
								SET_SYNCHRONIZED_SCENE_LOOPED(iSynchedScene, TRUE)
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSynchedScene, "SWITCH@MICHAEL@BENCH", "bench_on_phone_idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
								oCellPhoneMichael = CREATE_OBJECT(Prop_Phone_ING, GET_ENTITY_COORDS(PLAYER_PED_ID()))
								SET_OBJECT_TINT_INDEX(oCellPhoneMichael, GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER(CHAR_MICHAEL)) 
								ATTACH_ENTITY_TO_ENTITY(oCellPhoneMichael, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
								SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Phone_ING)
								iCutsceneStage++
							ENDIF
						ENDIF
					BREAK
					CASE 6 // Wait for switch to finish
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() // For B*1732328 - prevent being able to use context stuff in Michael's house
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableSecondaryAnimationTasks, TRUE) // B*1571751 Stop any other secondary anim tasks
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sSelectorCam)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Switch camera finished") ENDIF #ENDIF
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 7
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() // For B*1732328 - prevent being able to use context stuff in Michael's house
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableSecondaryAnimationTasks, TRUE) // B*1571751 Stop any other secondary anim tasks
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Changed player control to Michael") ENDIF #ENDIF
							sSelectorCam.bPedSwitched = TRUE
							RC_END_CUTSCENE_MODE()
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							DESTROY_ALL_CAMS()
							UNPIN_INTERIOR(interiorMichaelHouse)
							ADD_PED_FOR_DIALOGUE(sConversation, 0, PLAYER_PED_ID(), "MICHAEL")
							ADD_PED_FOR_DIALOGUE(sConversation, 3, NULL, "LESTER")
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 8 // Lester calls Michael
						SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() // For B*1732328 - prevent being able to use context stuff in Michael's house
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableSecondaryAnimationTasks, TRUE) // B*1571751 Stop any other secondary anim tasks
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
						IF CREATE_CONVERSATION(sConversation, "TRV5AUD", "TRV5_Miccall", CONV_PRIORITY_HIGH)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Started TRV5_Miccall phone call") ENDIF #ENDIF
							iSynchedScene = CREATE_SYNCHRONIZED_SCENE(vSynchScenePlayerPos, vSynchScenePlayerRot)
							SET_SYNCHRONIZED_SCENE_LOOPED(iSynchedScene, FALSE)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSynchedScene, "SWITCH@MICHAEL@BENCH", "exit_forward", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_TAG_SYNC_OUT)
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 9 // Wait for Michael to finish standing up
						SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() // For B*1732328 - prevent being able to use context stuff in Michael's house
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableSecondaryAnimationTasks, TRUE) // B*1571751 Stop any other secondary anim tasks
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSynchedScene)
						AND GET_SYNCHRONIZED_SCENE_PHASE(iSynchedScene) > 0.99
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Michael finished standing up") ENDIF #ENDIF
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "SWITCH@MICHAEL@BENCH", "CELLPHONE_CALL_LISTEN_BASE", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_LOOPING|AF_UPPERBODY) 
							//SET_ANIM_FILTER(PLAYER_PED_ID(), "BONEMASK_HEAD_NECK_AND_R_ARM") // Causes asserts
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							RC_END_CUTSCENE_MODE()
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 10 // Wait for Lester's call to Michael to finish
						SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() // For B*1732328 - prevent being able to use context stuff in Michael's house
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableSecondaryAnimationTasks, TRUE) // B*1571751 Stop any other secondary anim tasks
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Finished TRV5_Miccall phone call") ENDIF #ENDIF
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "SWITCH@MICHAEL@BENCH", "CELLPHONE_CALL_OUT", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY)
							//SET_ANIM_FILTER(PLAYER_PED_ID(), "BONEMASK_HEAD_NECK_AND_R_ARM") // Causes asserts
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 11 // Wait for appropriate point in anim to delete the phone object
						SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() // For B*1732328 - prevent being able to use context stuff in Michael's house
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableSecondaryAnimationTasks, TRUE) // B*1571751 Stop any other secondary anim tasks
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
						IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "SWITCH@MICHAEL@BENCH", "CELLPHONE_CALL_OUT")
							IF DOES_ENTITY_EXIST(oCellPhoneMichael)
							AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "SWITCH@MICHAEL@BENCH", "CELLPHONE_CALL_OUT") > 0.3
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Deleted phone object") ENDIF #ENDIF
								SAFE_DELETE_OBJECT(oCellPhoneMichael)
							ENDIF
						ELSE
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
								REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
							ENDIF
							REMOVE_ANIM_DICT("SWITCH@MICHAEL@BENCH")
							missionStateMachine = TREVOR4_STATE_MACHINE_CLEANUP
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE TREVOR4_STATE_MACHINE_CLEANUP
			TREVOR4_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_OUTRO" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling mission failed.
PROC MS_MISSION_FAILED()
	SWITCH missionStateMachine
		CASE TREVOR4_STATE_MACHINE_SETUP
			TREVOR4_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_MISSION_FAILED" #ENDIF)
			CLEAR_PRINTS()
			CLEAR_HELP()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			SWITCH failReason
				CASE FAILED_DEFAULT
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: MISSION_FAILED reason=FAILED_DEFAULT") ENDIF #ENDIF
					failString = "M_FAIL"
				BREAK
				CASE FAILED_WADE_DIED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: MISSION_FAILED reason=FAILED_WADE_DIED") ENDIF #ENDIF
					failString = "TRV5_FWADE" // ~s~Wade died.
				BREAK
				CASE FAILED_WADE_ABANDONED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: MISSION_FAILED reason=FAILED_WADE_ABANDONED") ENDIF #ENDIF
					failString = "TRV5_ABAN" // ~s~Wade was abandoned.
				BREAK
			ENDSWITCH
			IF failReason = FAILED_DEFAULT
				MISSION_FLOW_MISSION_FAILED()
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(failString)
			ENDIF
		BREAK
		CASE TREVOR4_STATE_MACHINE_LOOP
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				TREVOR4_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_MISSION_FAILED" #ENDIF)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID()) // B*1464519 Ensure blood is removed if not retrying
				ENDIF
																
				LOAD_SCENE(<<-1150.0391, -1521.7610, 9.6331>>)
				TREVOR4_REMOVE_ALL_PEDS(TRUE)
				TREVOR4_REMOVE_ALL_VEHICLES(TRUE)
				TREVOR4_Script_Cleanup()
			ELSE
				// not finished fading out: handle dialogue etc here
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
		IF bZSkipping = FALSE
		AND (GET_GAME_TIMER() - iDebugSkipTime) > 1000
		AND ENUM_TO_INT(missionState) < (ENUM_TO_INT(NUM_MISSION_STATES)-1) // Don't check during stateFailed
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: S key pressed so passing mission") ENDIF #ENDIF
				WAIT_FOR_CUTSCENE_TO_STOP()
				Script_Passed()
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: F key pressed so failing mission") ENDIF #ENDIF
				WAIT_FOR_CUTSCENE_TO_STOP()
				TREVOR4_MISSION_FAILED()
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: J key pressed so doing z skip forwards") ENDIF #ENDIF
				SWITCH missionState
					CASE stateIntro
					CASE stateOutro
						IF IS_CUTSCENE_ACTIVE()
						     STOP_CUTSCENE()
						ENDIF
					BREAK
					CASE stateDriveToClub
						TREVOR4_DEBUG_SKIP_STATE(Z_SKIP_OUTRO)
					BREAK
				ENDSWITCH
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: P key pressed so doing z skip backwards") ENDIF #ENDIF
				SWITCH missionState
					CASE stateDriveToClub
						TREVOR4_DEBUG_SKIP_STATE(Z_SKIP_INTRO)
					BREAK
					CASE stateOutro
						TREVOR4_DEBUG_SKIP_STATE(Z_SKIP_DRIVE_TO_CLUB)
					BREAK
				ENDSWITCH
			ENDIF
			INT i_new_state
			IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, i_new_state)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "TREVOR4: Z skip menu used so doing z skip") ENDIF #ENDIF
				TREVOR4_DEBUG_SKIP_STATE(i_new_state)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Flow_Mission_Force_Cleanup()
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		failReason = FAILED_DEFAULT
		TREVOR4_Script_Cleanup()
	ENDIF
	SET_MISSION_FLAG(TRUE)
	
	TREVOR4_MISSION_SETUP()
	
	IF IS_REPLAY_IN_PROGRESS()
		INT istage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			istage++
		ENDIF
		SWITCH istage
			CASE CP_DRIVE_TO_CLUB
				START_REPLAY_SETUP(vTrevorStart, fTrevorStart)
				TREVOR4_DEBUG_SKIP_STATE(Z_SKIP_DRIVE_TO_CLUB)
			BREAK
		ENDSWITCH
	ENDIF

	WHILE (TRUE)
		WAIT(0)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_HangTen")
		TREVOR4_DEATH_CHECKS()
		SWITCH missionState 
			CASE stateIntro
				MS_INTRO()
			BREAK
			CASE stateDriveToClub
				MS_DRIVE_TO_CLUB()
			BREAK
			CASE stateOutro
				MS_OUTRO()
			BREAK
			CASE stateMissionPassed
				Script_Passed()
			BREAK
			CASE stateMissionFailed
				MS_MISSION_FAILED()
			BREAK
		ENDSWITCH
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
ENDSCRIPT
