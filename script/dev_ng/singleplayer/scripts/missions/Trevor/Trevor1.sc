//+-----------------------------------------------------------------------------+
//¦																				¦
//¦ 				Author: Adam Westwood  				Date: 03/11/10			¦
//¦			Prev:	David Watson, David Gentles, Robert Wright, Imran Sarwar	¦
//¦																				¦
//¦-----------------------------------------------------------------------------¦
//¦ 																			¦
//¦ 				Trevor 1 - Two Part Kill									¦
//¦																				¦
//¦ 		Trevor tries to find out a location from some trailer trash			¦
//¦ 																			¦
//¦ 		*	Trevor and Ron drive to a location								¦
//¦ 		*	Chase trailer trash who are driving trucks and bikes			¦
//¦ 		*	Shootout at the trailer park									¦
//¦ 		*	Drive through the caravan to get the target						¦
//¦ 		*	Interrogate target for information								¦
//¦ 																			¦
//¦ 		Gameplay elements introduced: Trevor's special ability				¦
//¦ 		Vehicle Introduced: 						 						¦
//¦ 		Location: 															¦
//¦ 		Notes:  															¦
//¦ 																			¦
//+-----------------------------------------------------------------------------+

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

/*
Crack whore anims
I have created the anims for this they can be found here://depot/gta5/art/anim/export_mb/MISS/TREVOR1/IG_3/

the synched scene should be aligned to the box shown in the screenshot attached.
First play the start_loop.
When you want the action withthe audio to start play the 'action.anim' audio will play with this anim.
This action can go back to the start_loop if trevor doesn't ever walk over.
If trevor walks over then play run_from_trevor.anim. this will have audio and the tweekers run to the corner of the shack.
The tweekers can then play exit_loop.anim indefinately.

I have created the anims for this they can be found here://depot/gta5/art/anim/export_mb/MISS/TREVOR1/IG_3/tweek_lying_base.anim
There is a base loop and three idles. These can be placed wherever u need.
Mat

i have created the anims for this they can be found here:
//depot/gta5/art/anim/export_mb/MISS/TREVOR1/IG_3/tweek_huddle_base.anim
There is a base loop and three idles. 
These can be placed wherever u need.


//EIGHTBIKERS

//T1M1_BIKERAG

T1M1_CONV
T1M1_NR3
T1M1_FALLW
T1M1_GR7
T1M1_WGUARD
T1M1_RGUARD

Edited roots -
T1M1_WADEGBI
T1M1_WADEGBU
T1M1_PH5
T1M1_PH6
T1M1_CH3
T1M1_MUTTER
T1M1_LETSG

//1496038



Props:  Prop_Mug_04

The PROP_MUG_04 has been added to ORTEGA by parenting the mug's ROOT to the ORTEGA'S PH_R_HAND joint.

A Visible_To_Script tag has been added to the clip Ortega_02_Trevor_Arrival_Reaction with the following flag name: REMOVE_MUG to highlight the point where to hide/remove the mug prop.

*/

///    
///    
//---------------------------------¦ COMMAND HEADERS ¦-----------------------------------

//For the car recordings
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS 			22
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS 				11
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK 	1
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS 				1
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 	5
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK 5

using "rage_builtins.sch"
using "globals.sch"
using "script_heist.sch"
using "commands_misc.sch"
using "commands_pad.sch" 
using "commands_script.sch"
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "commands_camera.sch"
using "commands_path.sch"
using "commands_fire.sch"
using "commands_graphics.sch"
using "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_physics.sch"
USING "commands_ped.sch"
USING "commands_audio.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
using "script_player.sch"
using "script_debug.sch"
USING "script_buttons.sch"
USING "script_maths.sch"
USING "script_blips.sch"
using "streamed_scripts.sch"
using "selector_public.sch"
using "model_enums.sch"
using "dialogue_public.sch"
using "player_ped_public.sch"
USING "flow_public_core_override.sch"
USING "locates_public.sch"
USING "cellphone_public.sch"
USING "replay_public.sch"
USING "building_control_public.sch"
USING "traffic.sch"
USING "mission_stat_public.sch"
USING "script_ped.sch"
USING "cutscene_public.sch"
USING "help_at_location.sch"
USING "CompletionPercentage_public.sch"
USING "cam_recording_public.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "flow_public_game.sch"
USING "taxi_functions.sch"
USING "shop_public.sch"
USING "respawn_location_private.sch"
USING "family_public.sch"
USING "clearmissionarea.sch"
USING "RC_helper_functions.sch"
USING "commands_recording.sch"


//USING "text_queue.sch"
//Debug things
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "rerecord_recording.sch"
#ENDIF

//USING "cam_recording_public.sch"
//---------------------------------¦ ENUMS ¦-----------------------------------

ENUM MISSION_REQUIREMENT
	REQ_WADE,
	REQ_RON,
	REQ_ASHLEY,
	REQ_JOHNNY,
	REQ_TREVORS_TRUCK,
	REQ_CLAY,
	REQ_TERRY,
	REQ_CLAY_BIKE,
	REQ_TERRY_BIKE,
	REQ_BIKE,
	REQ_GANG_VAN,
	REQ_ORTEGA,
	REQ_ORTEGAS_TRAILER,
	REQ_PLEE_ANIMS,
	REQ_CHASE_RECORDINGS,
	REQ_TRAILER_OIL_DRUM,
	REQ_TRAILER_INTERIOR,
	REQ_WADES_BIKE,
	REQ_JERRY_CAN,
	REQ_TREVOR,
	REQ_CRACK_WHORE
ENDENUM

//---------¦ MISSION CONTROL ¦---------
//different mission fail conditions
ENUM MISSION_FAIL 
	FAIL_GENERIC,
	FAIL_WADE_DEAD,
	FAIL_WADE_LEFT,
	FAIL_RON_DEAD,
	FAIL_RON_LEFT,
	FAIL_RON_AND_WADE_LEFT,
	FAIL_RON_AND_WADE_DEAD,
	FAIL_VEHICLE_UNDRIVABLE,
	FAIL_VEHICLE_DEAD,
	FAIL_CHASE_LOST_DEAD_DRIVERS,
	FAIL_CHASE_LOST,
	FAIL_CHASE_LOST_VAN,
	FAIL_CHASE_LOST_DEAD_CARS,
	FAIL_SPOOKED_BIKERS,
	FAIL_LEFT_TRAILER_PARK,
	FAIL_LEFT_ORTEGA,
	FAIL_ORTEGA_KILLED_TOO_SOON,
	FAIL_LEFT_CARAVAN_AREA
ENDENUM

// MISSION_ENUMS stages that make up each section of the mission
ENUM MISSION_STAGE
    STAGE_INIT_MISSION,						//0
	STAGE_GET_TO_CHASE,						//1
	STAGE_CUTSCENE_CHASE_START,				//2
	STAGE_CHASE,							//3
	STAGE_CARAVAN_PARK_FIGHT,               //4
	STAGE_DRIVE_TO_CARAVAN,					//5
	STAGE_CARAVAN_SMASH,					//6
	STAGE_KILL_ORTEGA,					//7
	STAGE_DROP_OFF_RON,						//8
	STAGE_CUTSCENE_DROP_OFF_RON,			//9
	PASSED_MISSION,							//10
	STAGE_TEST,
	
	UBER_RECORD
ENDENUM

//Trailer fight enum list of moves- can be linked together to form a ped's fight AI
ENUM FIGHT_MOVES
	FIGHTMOVE_NONE,
	FIGHTMOVE_WAIT_FOR_TRIGGER,
	FIGHTMOVE_RUN,
	FIGHTMOVE_RUN_STRAIGHT,
	FIGHTMOVE_RUN_STRAIGHT_TO_COVER_POINT,
	FIGHTMOVE_RUN_AT_PLAYER,
	FIGHTMOVE_RUN_AT_PLAYER_WHILE_AIMING,
	FIGHTMOVE_JUMP,
	FIGHTMOVE_COVER,
	FIGHTMOVE_COVER_POINT,
	FIGHTMOVE_SHOOT,
	FIGHTMOVE_STAND_AND_SHOOT,
	FIGHTMOVE_ENGINE_AI,
	FIGHTMOVE_HEATWALK,
	FIGHTMOVE_HEATWALK_AT_PLAYER,
	FIGHTMOVE_HEATWALK_STRAIGHT,
	FIGHTMOVE_PLAY_ANIM,
	FIGHTMOVE_LEAVE_VEHICLE,
	FIGHTMOVE_TELEPORT
ENDENUM

//CONST_INT MAX_TRAILER_PED 			16

//---------------------------------¦ TRIP SKIP CONSTANTS ¦-----------------------------------
CONST_INT iDes4TripSkip								1
//---------------------------------:CUTSCENE VARS:------------------------------------------
BOOL b_skipped_mocap
INT i_current_event
INT iBattleDialogue
BOOL b_is_jumping_directly_to_stage
VEHICLE_INDEX veh_pre_mission_car
BOOL bAddedRelGroups = FALSE

BOOL bDoFails = FALSE
BOOL bSetupBuddies = FALSE
BOOL bStreamCutscene = FALSE

FLOAT fThisValue = 360
BOOL bClearCutscenArea
BOOL bTrevorSHoutsAtFleeingBikers
BOOL bBeenToTrailerPark = FALSE

BOOL bBlowTruckUpDialogue = FALSE
BOOL bTriggerBikerSetPiece[5]
BOOL bForceAWaveOfBikersToCome = FALSE

BOOL bTrevorCarGrabbedForStats = FALSE

BOOL ARRIVAL_REACTION = FALSE

WEAPON_TYPE eTrevorsFailWeapon

CAMERA_INDEX camCutscene
CAMERA_INDEX camInterp

//---------------------------------¦ CONSTANTS ¦-----------------------------------

//---------¦ VALUES ¦---------
CONST_INT MAX_CHASE_PED				4
CONST_INT MAX_TRAILER_PARK_VEH		26
CONST_INT MAX_ANIMS 				2
CONST_INT MAX_CAMS 					2
CONST_INT MAX_SHOOT_ZONES			4
CONST_INT MAX_CARS_TO_BLOW_UP 		5
CONST_INT MAX_DRIVER_PED 			5
CONST_INT MAX_SETP_VEH 				2
CONST_INT MAX_ENEMY_EAST_WAVE_PED 	5
CONST_INT MAX_ENEMY_WEST_WAVE_PED 	5
CONST_INT MAX_MISSION_VEH_EAST_WAVE 3
CONST_INT MAX_MISSION_VEH_WEST_WAVE 5
//CONST_INT MAX_FALL_OFF_PIPES		15
CONST_INT MAX_NUM_OF_CAM_LOCATIONS  30
CONST_INT MAX_GANG					50
CONST_INT MAX_DOGS					3
CONST_INT MAX_RUNNER				30
CONST_INT MAX_ENEMY_PROPS           1
CONST_INT MAX_NUMBER_FLEEING        5

INT i_triggered_text_hashes[80]

CONST_FLOAT PRELOAD_DISTANCE		300.0

VECTOR 	vRearLeftWheelOffset	= << -0.875, -1.75, -0.475 >>
VECTOR 	vRearRightWheelOffset	= << 0.875, -1.75, -0.475 >>
VECTOR	vPTFXRotation			= << 0.0, 0.0, 0.0 >>

COVERPOINT_INDEX CoverPlayer
INTERIOR_INSTANCE_INDEX interior_trev_trailer
INT iSkipTimer

BOOL bKilledBikersDuringChase = FALSE
BOOL bCreatePickUps = FALSE
BOOL bGivePedWeaponOnExit = FALSE
BOOl bGetBackInTruckGod 
BOOL bDoFailLine = FALSE
BOOl bikerLeadin = FALSE

BOOL bOrtegaSpared

BOOL bNeedToResetEffects = FALSE

INT iRamTimer 
INT iLighting
INT iRageTimer
INT iWadeGrumbleTimer
INT iJohnnyAndAshley
INT iChaseDialogueTimer 
INT iPetrolStage

INT iWadeBackSeat
INT ssOrtegaTrailerAnims

INT  iSkipCutsceneTimer

PICKUP_INDEX PickupHealth1
PICKUP_INDEX PickupHealth2
PICKUP_INDEX PickupHealth3
PICKUP_INDEX PickupHealth4
PICKUP_INDEX PickupHealth5
PICKUP_INDEX PickupWeapon1
PICKUP_INDEX PickupWeapon2
PICKUP_INDEX PickupWallet
PICKUP_INDEX PickupMoney1
PICKUP_INDEX PickupMoney2
PICKUP_INDEX PickupBat
PICKUP_INDEX PickupAK
PICKUP_INDEX PickupJerryCan

OBJECT_INDEX bikerProp[MAX_ENEMY_PROPS]

//CAM_RECORDING_DATA cam_data
INT iTrevorPissing
VECTOR vTrevorPissPos = << 1972.020, 3817.854,  33.424 >>
VECTOR vTrevorPissRot = << 0.000, 0.000, -152.750 >>

//---------¦ COORDINATES AND HEADINGS ¦---------

//AI_BLIP_STRUCT sBlipEnemies[MAX_GANG]

//---------------------------------¦ STRUCTS ¦-----------------------------------
// contains details for all mission peds that fight
STRUCT COMBAT_PED_STRUCT 
	PED_INDEX				ped				//Ped Index
	BLIP_INDEX				blip			//Blip Index
	VECTOR					startLoc		//Ped start location
	FLOAT					startHead		//Ped start heading
	AI_BLIP_STRUCT			EnemyBlipData   //Enemy Blip
	WEAPON_TYPE      	   	wpn				//Ped Weapon type
	WEAPON_TYPE      	   	wpn2			//Ped Weapon type
	MODEL_NAMES				model			//Ped model
	COMBAT_MOVEMENT			move			//Ped movement type
	COMBAT_RANGE			range			//Ped combat range
	COMBAT_ABILITY_LEVEL	ability			//Ped combat ability
	FLOAT					infmR1ge =	50.0//Ped inform friends range
	INT						accuracy =	3	//Ped inform friends range
	COVERPOINT_INDEX    	cov				//Ped coverpoint index
	BOOL					covExist
	INT						iEnemyProgress
	INT						iTimeSpent
	INT						iTimeLastFrame
	INT 					iHealthBeforeRage
	FLOAT					fUnBlockRange = 12.0
	FLOAT 					fDead
	FIGHT_MOVES				fmMove[6]
	STRING					animDict
	STRING					animName
	BOOL					eastTriggered
	VECTOR              	eastCovLoc			//Ped coverpoint vector
	FLOAT					eastCovHead
	COVERPOINT_USAGE		eastCovUse
	COVERPOINT_HEIGHT		eastCovHeight
	COVERPOINT_ARC			eastCovArc
	VECTOR					eastMoveLoc
	BOOL					westTriggered
	VECTOR              	westCovLoc			//Ped coverpoint vector
	FLOAT					westCovHead
	COVERPOINT_USAGE		westCovUse
	COVERPOINT_HEIGHT		westCovHeight
	COVERPOINT_ARC			westCovArc
	VECTOR					westMoveLoc
	BOOL					westSameAsEast
	BOOL					reactToProximity
	INT						enemyGroup
	BOOL                    bReturnToAI
ENDSTRUCT
//COMBAT_PED_STRUCT	cmbtEnemy[MAX_TRAILER_PED]
COMBAT_PED_STRUCT	cmbtChase[MAX_CHASE_PED]
COMBAT_PED_STRUCT 	cmbtEnemyEastWave[MAX_ENEMY_EAST_WAVE_PED]
COMBAT_PED_STRUCT 	cmbtEnemyWestWave[MAX_ENEMY_WEST_WAVE_PED]

STRUCT decal
	DECAL_ID the_decal_id
	DECAL_RENDERSETTING_ID decal_texture_id
	vector pos
	vector direction
	vector side
	float width
	float height
	float fAlpha
	float life
	float wash_amount
	blip_index blip
	bool decal_removed
ENDSTRUCT

decal blood[3]

ENUM PED_T
	PED_RUNNER_LHS1,
	PED_RUNNER_LHS2,
	PED_RUNNER_RHS_CLOSE1,
	PED_FIGHT_MIDDLE_RUNNER,
	PED_FIGHT_LHS_CLOSE1,
	PED_FIGHT_RHS_CLOSE1,
	PED_FIGHT_RHS_FAR1,
	PED_FIGHT_INSIDE_TRAILER,
	PED_FIGHT_INSIDE_TRAILER_RHS,
	PED_FIGHT_PICKUP_SETPIECE1,
	PED_FIGHT_REINF_PICKUP_SETPIECE1,
	PED_FIGHT_BIKE_CONV_SETPIECE1,
	PED_FIGHT_RHS_BIKE_CONV_SETPIECE1,
	PED_FIGHT_RHS_BIKE_CONV_SETPIECE2,
	PED_FIGHT_APPROACH_CENTRE_RHS_BIKES,
	PED_FIGHT_APPROACH_CENTRE_LHS_BIKES,
	PED_FIGHT_THROUGH_MIDDLE_BIKES_RHS,
	PED_FIGHT_THROUGH_MIDDLE_BIKES_LHS,
	PED_FIGHT_BACK_LEFT_FORKLIFT,
	PED_FIGHT_RHS_VAN_REVERSE,
	PED_FIGHT_MOVE_REAR_TO_CENTRE,
	PED_GRENADE_BOSS,
	PED_FIGHT_FAKE
ENDENUM
ENUM ATTACK_MOVE_T
	MOVE_FOLLOW_NAVMESH,
	MOVE_FOLLOW_NAVMESH_DUCK,
	MOVE_FOLLOW_NAVMESH_2POINTS,
	MOVE_USE_WAYPOINT,
	MOVE_AIM_WHILE_GOTO,
	MOVE_AIM_AT_COORD_WHILE_GOTO,
	MOVE_SHOOT_WHILE_GOTO,
	MOVE_USE_SPEC_COVERPOINT,
	MOVE_AGGRESSIVE_CHARGE,
	MOVE_ROLL_LEFT
ENDENUM

ENUM DEFENSIVE_T                                             
	DA_AT_GO_TO,
	DA_AT_GO_TO_SMALL,
	DA_AT_GO_TO_LARGE,
	DA_AT_GOTO_V_LARGE
ENDENUM

STRUCT GANG_MOVE_ATTACK_T
	PED_INDEX ped
	BOOL bTriggered				// When TRUE, the ped will take part in the gunfight
	BOOL bHasSequence			// Set when ped is given follow_navmesh task
	BOOL bJustAttack			// If we just want the ped to attack where they are, this is set
	BOOL bAttackMove			// Set once the ped has got to there destination and has beem told to attack
	BOOL bJustMove				// Sometimes we just want a ped to go from A to B, and not attack. 
	BOOL bMovedToNewPos
	BOOL bSpecial
	INT iAttackDelay
	INT iPedTimer
	AI_BLIP_STRUCT			EnemyBlipData   //Enemy Blip
  	ATTACK_MOVE_T moveAttackStyle
	COVERPOINT_INDEX coverGangUse
	VECTOR vGotoCoords			// Where the ped goes to before they start fighting
	VECTOR vOtherGotoCoords
   	DEFENSIVE_T gang_da			// Defensive area for the ped
	COMBAT_MOVEMENT	gangCombatMovement	
	BOOL bFriendly
	BOOL bFlee
	BOOL bReturnToAI
	BOOL bInitialAttack
	BOOL bHasPlayerGotNear

	BLIP_INDEX blipPed
ENDSTRUCT

GANG_MOVE_ATTACK_T gangEnemy[MAX_GANG]
GANG_MOVE_ATTACK_T gangFlee[MAX_NUMBER_FLEEING]
PED_INDEX statPeds[MAX_NUMBER_FLEEING]
GANG_MOVE_ATTACK_T gangDog[MAX_DOGS]

STRUCT GANG_RUNNER_T
	PED_INDEX ped
	BOOL bTriggered				
	BOOL bHasSequence
	INT iAttackDelay
	INT iPedTimer
	AI_BLIP_STRUCT			EnemyBlipData   //Enemy Blip
	ATTACK_MOVE_T moveAttackStyle
	VECTOR vGotoCoords
	BLIP_INDEX blipPed
ENDSTRUCT

GANG_RUNNER_T gangRunner[MAX_RUNNER]

ENUM SECTION_STAGE
	SECTION_STAGE_SETUP = 0,
	SECTION_STAGE_RUNNING,
	SECTION_STAGE_CLEANUP,
	SECTION_STAGE_SKIP,
	SECTION_STAGE_JUMPING_TO,
	SECTION_DEBUG
ENDENUM

SECTION_STAGE e_section_stage 		= SECTION_STAGE_SETUP

ENUM SHOOTOUT_ZONE_T
	ZONE_INTRO,
	ZONE_CENTRE,
	ZONE_LHS,
	ZONE_RHS,
	ZONE_NONE
ENDENUM

//SHOOTOUT_ZONE_T shootoutZone[4]

//Trailer fight ped names
ENUM GROUP_NAMES
	GROUP_ENTRANCE = 0,
	GROUP_NORTH_WEST,
	GROUP_CENTRE,
	GROUP_NORTH_EAST,
	GROUP_EAST,
	
	GROUP_NONE
ENDENUM

CONST_INT NUMBER_OF_ENEMIES_IN_GROUP 4
CONST_INT NUMBER_OF_GROUPS 5
STRUCT sTrailerPedGroup
	COMBAT_PED_STRUCT enemy[NUMBER_OF_ENEMIES_IN_GROUP]
	BLIP_INDEX blip
	BOOL triggered, triggeredEast, triggeredWest, dialoguePlayed, cleared
	STRING name
	VECTOR vCentre, vEastTriggerLoc, vEastTriggerDim, vWestTriggerLoc, vWestTriggerDim
ENDSTRUCT
sTrailerPedGroup sGroup[NUMBER_OF_GROUPS]

// contains details for your budRon
STRUCT BUDDY_PED_STRUCT // contains details for all mission peds
	PED_INDEX				ped				//Ped Index
	BLIP_INDEX				blip			//Blip Index
	VECTOR					startLoc				//Ped start location
	FLOAT					startHead			//Ped start heading
	WEAPON_TYPE      	   	wpn				//Ped Weapon type
	WEAPON_TYPE      	   	wpn2			//Ped Weapon type
	MODEL_NAMES				model			//Ped model
	INT						iAction
	//COMBAT_MOVEMENT			move			//Ped movement type
	//COMBAT_RANGE			range			//Ped combat range
	//COMBAT_ABILITY_LEVEL	ability			//Ped combat ability
ENDSTRUCT
BUDDY_PED_STRUCT	budRon
BUDDY_PED_STRUCT	budWade

 // contains details for non combat peds
STRUCT PED_STRUCT
	PED_INDEX				ped				//Ped Index
	BLIP_INDEX				blip			//Blip Index
	VECTOR					startLoc				//Ped start location
	FLOAT					startHead			//Ped start heading
	MODEL_NAMES				model			//Ped model
	BOOL 					bFriendly
	INT 					iPedTimer
ENDSTRUCT
PED_STRUCT	npcDriver[MAX_DRIVER_PED]
PED_STRUCT	npcOrtega

// contains details for all mission vehicles
STRUCT VEH_STRUCT 
	VEHICLE_INDEX			veh
	BLIP_INDEX				blip
	VECTOR					startLoc
	FLOAT					startHead
	MODEL_NAMES				model
	INT						recNumber
	BOOL bCrash
	BOOL bDoCrash
	INT 					colour1
	INT 					colour2
ENDSTRUCT
VEH_STRUCT vehTrailerPark[MAX_TRAILER_PARK_VEH]
VEH_STRUCT vehTrevor
VEH_STRUCT vehTruck
VEH_STRUCT vehBike[2]
VEH_STRUCT vehSetP[MAX_SETP_VEH]

// contains details for the toilet
STRUCT OBJECT_STRUCT 
	OBJECT_INDEX			obj	
	VECTOR					startLoc
	VECTOR					rot
	MODEL_NAMES				model
ENDSTRUCT

//For Swapping out the trailer.
CONST_INT MAX_TRAILER_PARTS 	8
CONST_INT TRAILER_BASE_MAP  	0
CONST_INT TRAILER_BASE_SWAP 	1
CONST_INT TRAILER_FRONT 		2
CONST_INT TRAILER_BACK			3
CONST_INT TRAILER_KITCHEN0		4
CONST_INT TRAILER_KITCHEN1		5
CONST_INT TRAILER_BATHROOM		6
CONST_INT TRAILER_FRIDGE		7

LOCATES_HEADER_DATA sLocatesData
//CAM_RECORDING_DATA cam_data
//-------------------------------¦ VARIABLES ¦-----------------------------------

//---------¦ ENUMS ¦---------
MISSION_STAGE eMissionStage = STAGE_INIT_MISSION

RAYFIRE_INDEX trailerRayfire

PED_INDEX pedWhore[4]
BOOL bTriggerWhore[3]

//BOOL bUseNormalChaseCam

INT iGetDriveByTimer
INT iDriveByHelp
INT iFuelTarget
INT iSmashTimer 
INT icutsceneprog
INT SceneBikers1
INT SceneBikers2
INT SceneBikers3
INT SceneCutscene
INT iBikerAnims
INT SceneAandJ
INT SceneAandJFlee
INT iTimeSinceLastHorn
INT iClosestVehicleCheckTimer
INT iAmbientDialogueTimer
INT iAttackSide
INT iTrailerSounds 
INT iBattleDialogueTimer
INT iBikerFleeHelp
INT iDriveConvo
INT iChaseDialogue
INT iRoofGuy
INT iOrtegaDialogue
INT iWrongWayDialogue

INT iBikesDialogue
INT iVanDialogue

INT iTimer

CONST_INT NUMBER_OF_EVENT_FLAGS 10
INT iCamRecs[16]

VEHICLE_INDEX vehCameraTest
STRING strCarrec = "t1actcam"
CAMERA_INDEX camChase

INT  iTrailerRayfireStage
INT iExplosionTime
FLOAT fSlomoRamp

BOOL bTriggerLoopedEndAnim = FALSE
BOOL bSkipped = FALSE

PTFX_ID ptfx_spark1
PTFX_ID ptfx_spark2
PTFX_ID ptfx_spark3
PTFX_ID ptfx_spark4
PTFX_ID ptfx_crash_dust

//---------¦ FLAGS ¦---------
BOOL bScriptInProgress								= TRUE
BOOL bStageSetup									= FALSE
INT iReplayStage
//BOOL JSkip											= FALSE
#IF IS_DEBUG_BUILD
//BOOL PSkip											= FALSE
#ENDIF

BOOL bBuddyDeleted
BOOL bDisabledHotSwap
BOOL bBrakeing
BOOL bGoToSpeach
BOOL bcutsceneplaying
BOOL bDoneCentreFight
BOOL bDoNewShootout = TRUE
BOOL bSetUpStageData = FALSE
BOOL bSetUpPedAnims
BOOL bTriggerPTFX
BOOL bPTFXStarted
BOOL bAshFleeTask
BOOl bSetJohnnyComponent = FALSE
BOOL bClearUpTrailerPark = FALSE
BOOL bReminderPrint = FALSE
BOOL bImpactDialogue = FALSE
BOOL bDoShoutDialogue = FALSE
BOOL bJustArrived = FALSE
BOOL bExitState = FALSE
BOOL bLoad = FALSE
BOOL bGetBackInTruckTask = FALSE
BOOL bTriggerWadeLeaving = FALSE
BOOL bBlockFirstPerson = FALSE
BOOL bCachedCamMode = FALSE
BOOL bPushInToFirstPerson
CAM_VIEW_MODE cachedCamMode
	
BOOL bTrevorSplashBackEffectOn

BOOL bDirectionsDialogue[5]
BOOL bWatchTruckDialogue[2]
//BOOL bRiderDialogue[2]

PTFX_ID ptfxRearLeftWheel
PTFX_ID ptfxRearRightWheel

INT iFuelDropFromVehTime[MAX_TRAILER_PARK_VEH]
BOOL bFuelCanDropFromVeh[MAX_TRAILER_PARK_VEH]

BOOL bHasPlayerEnteredShootZOne[MAX_SHOOT_ZONES]
BOOL bSHouldRhsVanSetpieceTrigger
//-- Uber rec stuff
STRING sAwUberRec = "Trev1ChaseDw"
VEHICLE_INDEX vehBikeForUber
VEHICLE_INDEX vehUberTruck[2]

VEHICLE_INDEX vehWadesBike
FLOAT fTimeForUberTrailerAndTruck 
FLOAT fUberSpeed
INT iTruckAndTrailerSetpieceProg

BOOL bHaveAllTrailerPedsBeenKilled
BOOL bGotFlashTimer
BOOl bSetupCaravan
BOOL bDoModelSwap = FALSE
BOOL bStartForce
BOOL bPTX_Triggered
BOOL bAssignWadeTask
BOOL bFrag1
BOOL bFrag2
BOOL bFrag3
BOOL bDust
BOOL bSplash
BOOL bDisableControls

//---------¦ VECTORS ¦---------
VECTOR vStageStart    = << 1981.1176, 3817.1990, 31.3807 >> 
VECTOR vStageEnd	  = << 80.2901, 3619.4456, 38.6997 >> 

OBJECT_INDEX oFuelCan[20]
OBJECT_INDEX oOilDrum
VECTOR vFuelCanLoc[20]
VECTOR vForce = <<0,10,5>>
VECTOR vTrailerPark = << 64.9021, 3695.3811, 38.7647 >>

//---------¦ INTS ¦---------
INT iEnemyEastWave
INT iEnemyWestWave
INT iEnemyAIProg
INT iPreLoadStage
INT iBuddyProgress
INT iWadeProgress
INT iShotsTaken[MAX_CARS_TO_BLOW_UP]
INT iEnemyCreationProc
INT iControlChaseSetP
INT iNumberOfPedsAliveTrailer
INT iNumberOfPedsAliveEast
INT iNumberOfPedsAliveWest
INT iDoPedFleeOnBikeSetPieceProg
INT iRage = 0
INT iWhore
INT iFailTimer
INT ThreatenSceneMain
INT iVehicleBlowUpSetPiece

//---------¦ FLOAT ¦---------
FLOAT fVehicleSpeed

FLOAT fPlayBackSpeed = 1.0
FLOAT fStartHeading  = 323.8188

//---------¦ COUNTERS ¦---------
INT iCounter										= 0
INT iGroupCount										= 0
INT iEnemyCount										= 0

//---------¦ Stages ¦---------
INT iStageSection									= 0

//---------¦ Selector ¦---------
//SELECTOR_CAM_STRUCT sCamDetails

//---------¦ PEDS ¦---------

PED_INDEX pedJohnny
PED_INDEX pedAshley

//---------¦ VEHICLES ¦---------

VEHICLE_INDEX vehTrailer

//---------¦ OBJECTS ¦---------

//---------¦ INTERIORS ¦---------

//---------¦ SEQUENCES ¦---------
SEQUENCE_INDEX seqSequence

//---------¦ WEAPONS ¦---------

//---------¦ PARTICLE ID ¦---------

PTFX_ID ptfx_explosion

//---------¦ Scene ID ¦---------

//---------¦ Sound ID ¦---------

//---------¦ BLIPS ¦---------
BLIP_INDEX blipDest
BLIP_INDEX blipVeh
BLIP_INDEX blipPed

//---------¦ CAMERAS ¦---------
CAMERA_INDEX cutscene_cam

//---------¦ GROUPS ¦---------

//---------¦ RELATIONSHIP GROUPS ¦---------
REL_GROUP_HASH grpBuddies
REL_GROUP_HASH grpEnemies

//---------¦ STRINGS ¦---------
STRING sCarRec = "TrevOne"
STRING sTrevor1Aud = "T1M1AUD"
STRING sTrevor1Message = "TREV1"

//---------¦ SCALEFORM ¦---------
//SCALEFORM_INDEX sfRageBar
//INT iCurrentCamSetPiece = 2
FLOAT fPlaybackTimeT1

//---------¦ DEBUG ¦---------
#IF IS_DEBUG_BUILD
	BOOL bMakeSmashPed
	BOOL bOutPut
	BOOL bReset = TRUE
	BOOL bRecStarted[4]
	BOOL bSkipToCarRec
	BOOL bControlCarRec
	BOOL bPutPedinCar
	BOOL bActive
	BOOL bRunDebugProcs
	BOOL bSkipToPoint
	BOOL bDoJandP
	BOOL bDebugText
	BOOL bRePos
	//INT
	INT iCamProgressDB
	INT iMissionStageDB
	INT iProgressDB
	INT iPreLoadStageDB
	INT iEnemyCreationProcDB
	INT iBuddyProgressDB
	INT iEnemyAIProgDB
	INT iEnemyEastWaveDB
	INT iEnemyWestWaveDB
	INT ieMissionStageTemp
	//INT iPipeNumber
	BOOl bSetupCaravanDB

	//mission stage to jump to
	//FLOAT
	FLOAT fRecSkipTime
	FLOAT fXCoord = 0.60
	FLOAT fYCoord = 0.855
	FLOAT fSkipAmount

	WIDGET_GROUP_ID widgetDebug
	BOOL bUberRecord 									= FALSE
	
	CONST_INT MAX_SKIP_MENU_LENGTH 						11
	
	INT iReturnStage                                      								 // mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      				// struct containing the debug menu 
	STRING sMissionName									= "Trevor 1 - Mr Phillips"
	
#ENDIF

//  ___ ___  _ __ ___  _ __ __    ___  _ __		common functions for David G (PRE FAILCHECK)
// / __/ _ \| '_ ` _ \| '_ ` _ \ / _ \| '_ \ 
//| (_| (_) | | | | | | | | | | | (_) | | | |
// \___\___/|_| |_| |_|_| |_| |_|\___/|_| |_|

//---------------------------------¦ FAIL ¦-----------------------------------
BOOL bMissionFailedCleanup = FALSE

//PURPOSE:	Does all the checks needed to see if a vehicle has rolled over
FUNC BOOL hasVehicleRolledOver(VEHICLE_INDEX thisVehicle)
	IF NOT IS_ENTITY_DEAD(thisVehicle)
		IF IS_VEHICLE_STUCK_TIMER_UP(thisVehicle, VEH_STUCK_ON_ROOF, ROOF_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(thisVehicle, VEH_STUCK_JAMMED, ROOF_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(thisVehicle, VEH_STUCK_HUNG_UP, ROOF_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(thisVehicle, VEH_STUCK_ON_SIDE, ROOF_TIME)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//---------------------------------¦ PED CONTROL ¦-----------------------------------
ENUM SELECT_PEDS
	SEL_MICHAEL									= 0,
	SEL_FRANKLIN,
	SEL_TREVOR	
ENDENUM

SELECTOR_PED_STRUCT sSelectorPeds
structPedsForConversation structConvoPeds

FUNC SELECT_PEDS GET_CURRENT_SELECT_PED()
	SWITCH sSelectorPeds.eCurrentSelectorPed
		CASE SELECTOR_PED_MICHAEL
			RETURN SEL_MICHAEL
		BREAK
		CASE SELECTOR_PED_FRANKLIN
			RETURN SEL_FRANKLIN
		BREAK
		CASE SELECTOR_PED_TREVOR
			RETURN SEL_TREVOR
		BREAK
	ENDSWITCH
	RETURN SEL_MICHAEL
ENDFUNC

//PURPOSE:		Gets whether or not a select ped exists
FUNC BOOL DOES_SELECT_PED_EXIST(SELECT_PEDS PedToGet)
	SWITCH PedToGet
		CASE SEL_MICHAEL
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		CASE SEL_FRANKLIN
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		CASE SEL_TREVOR
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//PURPOSE:		If the ped is controlled by the player, return player. Else, return the ped.
FUNC PED_INDEX GET_SELECT_PED(SELECT_PEDS PedToGet)
	SWITCH PedToGet
		CASE SEL_MICHAEL
			IF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_MICHAEL
				RETURN PLAYER_PED_ID()
			ELSE
				RETURN sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
			ENDIF
		BREAK
		CASE SEL_FRANKLIN
			IF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_FRANKLIN
				RETURN PLAYER_PED_ID()
			ELSE
				RETURN sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
			ENDIF
		BREAK
		CASE SEL_TREVOR
			IF sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_TREVOR
				RETURN PLAYER_PED_ID()
			ELSE
				RETURN sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			ENDIF
		BREAK
	ENDSWITCH
	RETURN PLAYER_PED_ID()
ENDFUNC


//---------------------------------¦ PRINT ¦-----------------------------------
PROC printLine(STRING sTextLine)
	PRINTSTRING(sTextLine)
	PRINTNL()
ENDPROC

//                _ 
//               | |
//  ___ _ __   __| |
// / _ \ '_ \ / _` |
//|  __/ | | | (_| |
// \___|_| |_|\__,_|	end common functions for David (PRE FAILCHECK)

//---------------------------------¦ FAIL PROCEDURES ¦-----------------------------------

FUNC BOOL MANAGE_MY_TIMER(INT&start_time, INT time) 
	INT current_time 
	current_time = GET_GAME_TIMER()
	
	IF ((current_time - start_time) > time) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT iStuckNow
BOOL bFlipTimerSet
VECTOR vFlipVec

//PURPOSE:checks to see if the vehicle is stuck or undrivable
FUNC BOOL MISSION_VEHICLE_FAIL_CHECKS(VEHICLE_INDEX VEHICLE)
	
	
	IF DOES_ENTITY_EXIST(VEHICLE)
		IF IS_VEHICLE_DRIVEABLE(VEHICLE)
			
			IF IS_VEHICLE_MODEL(VEHICLE,BODHI2)
				IF bFlipTimerSet = FALSE
					vFlipVec = NORMALISE_VECTOR(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VEHICLE, <<0,0,1>>) - GET_ENTITY_COORDS(VEHICLE, FALSE))
			        IF ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vFlipVec.x, vFlipVec.z, 0, 1)) > 80
			        OR ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vFlipVec.y, vFlipVec.z, 0, 1)) > 80
					OR IS_ENTITY_UPSIDEDOWN(VEHICLE)
						PRINTSTRING("!!!!!!!!!!!!!!!!!!!STUCK")PRINTNL()
						iStuckNow = GET_GAME_TIMER()
					ENDIF
				ELSE
					vFlipVec = NORMALISE_VECTOR(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VEHICLE, <<0,0,1>>) - GET_ENTITY_COORDS(VEHICLE, FALSE))
			        IF ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vFlipVec.x, vFlipVec.z, 0, 1)) > 80
			        OR ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vFlipVec.y, vFlipVec.z, 0, 1)) > 80
					OR IS_ENTITY_UPSIDEDOWN(VEHICLE)
						IF MANAGE_MY_TIMER(iStuckNow,1000)
							PRINTSTRING("!!!!!!!!!!!!!!!!!!!FUCKED")PRINTNL()
							RETURN TRUE
						ENDIF
					ELSE
						PRINTSTRING("!!!!!!!!!!!!!!!!!!!UNSTUCK")PRINTNL()
						bFlipTimerSet = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_JAMMED, JAMMED_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_SIDE, SIDE_TIME)
				RETURN TRUE
			ENDIF 
		ELSE
			RETURN TRUE
		ENDIF 
	ENDIF 
	RETURN FALSE
ENDFUNC 


//---------------------------------¦ CLEANUP PROCEDURES ¦-----------------------------------

//---------------------------------¦ NEED LIST ¦-----------------------------------


//  ___ ___  _ __ ___  _ __ __    ___  _ __		common functions for David (POST FAILCHECK)
// / __/ _ \| '_ ` _ \| '_ ` _ \ / _ \| '_ \ 
//| (_| (_) | | | | | | | | | | | (_) | | | |
// \___\___/|_| |_| |_|_| |_| |_|\___/|_| |_|
//---------------------------------¦ SAFE WAIT ¦-----------------------------------


//---------------------------------¦ MODELS ¦-----------------------------------
CONST_INT NUMBER_OF_MODELS							50
INT iNextEmptyModelEntry							= 0
STRUCT sModelInfo
	MODEL_NAMES modelname
	BOOL bNeeded
	BOOL bLoaded
ENDSTRUCT
sModelInfo ModelList[NUMBER_OF_MODELS]


PROC BLOCK_FIRST_PERSON()
	IF bBlockFirstPerson
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			PRINTLN("@@@@@@@@@@@@@@ DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() @@@@@@@@@@@@@@")
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		//	printstring("block 1st") printnl()
//		ENDIF
	ENDIF	
ENDPROC

PROC safeWait(INT iWaitTime)	

	WAIT(iWaitTime)
	BLOCK_FIRST_PERSON()
	//FailCheck()
ENDPROC

//PURPOSE: Requests the model, then waits until it loads
PROC SAFE_REQUEST_MODEL(MODEL_NAMES thisModel, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_MODEL_LOADED(thisModel)
		REQUEST_MODEL(thisModel)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT HAS_MODEL_LOADED(thisModel)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Assigns a model an entry in the need list
PROC ADD_MODEL_TO_NEED_LIST(MODEL_NAMES thisModelName)
	If iNextEmptyModelEntry < NUMBER_OF_MODELS
		ModelList[iNextEmptyModelEntry].modelname = thisModelName
		ModelList[iNextEmptyModelEntry].bNeeded = FALSE
		ModelList[iNextEmptyModelEntry].bLoaded = FALSE
		iNextEmptyModelEntry++
	ELSE
		SCRIPT_ASSERT("Too many models added to list")
	ENDIF
ENDPROC

//PURPOSE: Finds the model entry in the need list
FUNC INT FIND_NEED_LIST_MODEL_ENTRY(MODEL_NAMES thisModelName)
	INT iCorrectModelEntry = -1
	BOOL bSearchDone = FALSE
	
	INT iLoadCounter = 0
	WHILE NOT bSearchDone
		IF ModelList[iLoadCounter].modelname = thisModelName
			bSearchDone = TRUE
			iCorrectModelEntry = iLoadCounter
		ELSE
			iLoadCounter++
			IF iLoadCounter >= iNextEmptyModelEntry
				bSearchDone = TRUE
				//SCRIPT_ASSERT("Model not found")
			ENDIF			
		ENDIF
	ENDWHILE
	
	RETURN iCorrectModelEntry
ENDFUNC

//PURPOSE: Sets the model's need list entry as needed; will be loaded with next call of "MANAGE_LOADING_NEEDED_LIST"
PROC SET_MODEL_AS_NEEDED(MODEL_NAMES thisModelName)
	INT iThisEntry = FIND_NEED_LIST_MODEL_ENTRY(thisModelName)
	IF iThisEntry <> -1
		ModelList[iThisEntry].bNeeded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately loads a model in the need list, setting it as loaded but not needed
PROC IMMEDIATELY_LOAD_NEED_LIST_MODEL(MODEL_NAMES thisModelName)
	INT iThisEntry = FIND_NEED_LIST_MODEL_ENTRY(thisModelName)
	IF iThisEntry <> -1
		SAFE_REQUEST_MODEL(ModelList[iThisEntry].modelname, TRUE, TRUE)
		ModelList[iThisEntry].bNeeded = FALSE
		ModelList[iThisEntry].bLoaded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately unloads a model in the need list, setting it as not loaded and not needed
PROC IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(MODEL_NAMES thisModelName)
	INT iThisEntry = FIND_NEED_LIST_MODEL_ENTRY(thisModelName)
	IF iThisEntry <> -1
		SET_MODEL_AS_NO_LONGER_NEEDED(ModelList[iThisEntry].modelname)
		ModelList[iThisEntry].bNeeded = FALSE
		ModelList[iThisEntry].bLoaded = FALSE
	ENDIF
ENDPROC

//---------------------------------¦ VEHICLE RECORDINGS ¦-----------------------------------
CONST_INT NUMBER_OF_VEHICLE_RECORDINGS				50
INT iNextEmptyVehRecEntry							= 0
STRUCT sVehRecInfo
	STRING vehrec
	INT vehrecno
	BOOL bNeeded
	BOOL bLoaded
ENDSTRUCT
sVehRecInfo VehRecList[NUMBER_OF_VEHICLE_RECORDINGS]

//PURPOSE: Requests the vehicle recording, then waits until it loads
PROC SAFE_REQUEST_VEHICLE_RECORDING(INT thisRecNumber, STRING thisRecName, BOOL bWait, BOOL bSafely)	
	//IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(thisRecNumber, thisRecName)
		REQUEST_VEHICLE_RECORDING(thisRecNumber, thisRecName)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(thisRecNumber, thisRecName)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Assigns a vehicle recording an entry in the need list
PROC ADD_VEHICLE_RECORDING_TO_NEED_LIST(STRING thisRec, INT thisRecNo)
	If iNextEmptyVehRecEntry < NUMBER_OF_VEHICLE_RECORDINGS
		VehRecList[iNextEmptyVehRecEntry].vehrec = thisRec
		VehRecList[iNextEmptyVehRecEntry].vehrecno = thisRecNo
		VehRecList[iNextEmptyVehRecEntry].bNeeded = FALSE
		VehRecList[iNextEmptyVehRecEntry].bLoaded = FALSE
		iNextEmptyVehRecEntry++
	ELSE
		SCRIPT_ASSERT("Too many anim dicts added to list")
	ENDIF
ENDPROC

//PURPOSE: Finds the vehicle recording entry in the need list
FUNC INT FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(STRING thisRec, INT thisRecNo)
	INT iCorrectVehRecEntry = -1
	BOOL bSearchDone = FALSE
	
	INT iLoadCounter = 0
	WHILE NOT bSearchDone
		IF ARE_STRINGS_EQUAL(VehRecList[iLoadCounter].vehrec, thisRec)
		AND VehRecList[iLoadCounter].vehrecno = thisRecNo
			bSearchDone = TRUE
			iCorrectVehRecEntry = iLoadCounter
		ELSE
			iLoadCounter++
			IF iLoadCounter >= iNextEmptyVehRecEntry
				bSearchDone = TRUE
				SCRIPT_ASSERT("Vehicle Recording not found")
			ENDIF			
		ENDIF
	ENDWHILE
	
	RETURN iCorrectVehRecEntry
ENDFUNC

//PURPOSE: Sets the vehicle recording's need list entry as needed; will be loaded with next call of "MANAGE_LOADING_NEEDED_LIST"
PROC SET_VEHICLE_RECORDING_AS_NEEDED(STRING thisRec, INT thisRecNo)
	INT iThisEntry = FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(thisRec, thisRecNo)
	IF iThisEntry <> -1
		VehRecList[iThisEntry].bNeeded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately loads a vehicle recording in the need list, setting it as loaded but not needed
PROC IMMEDIATELY_LOAD_NEED_LIST_VEHICLE_RECORDING(STRING thisRec, INT thisRecNo)
	INT iThisEntry = FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(thisRec, thisRecNo)
	IF iThisEntry <> -1
		SAFE_REQUEST_VEHICLE_RECORDING(VehRecList[iThisEntry].vehrecno, VehRecList[iThisEntry].vehrec, TRUE, TRUE)
		VehRecList[iThisEntry].bNeeded = FALSE
		VehRecList[iThisEntry].bLoaded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately unloads a vehicle recording in the need list, setting it as not loaded and not needed
PROC IMMEDIATELY_UNLOAD_NEED_LIST_VEHICLE_RECORDING(STRING thisRec, INT thisRecNo)
	INT iThisEntry = FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(thisRec, thisRecNo)
	IF iThisEntry <> -1
		REMOVE_VEHICLE_RECORDING(VehRecList[iThisEntry].vehrecno, VehRecList[iThisEntry].vehrec)
		VehRecList[iThisEntry].bNeeded = FALSE
		VehRecList[iThisEntry].bLoaded = FALSE
	ENDIF
ENDPROC


//---------------------------------¦ NEED LIST CONTROLS ¦-----------------------------------
//PURPOSE: Resets/clears out the model list, replacing all models as DUMMY_MODEL_FOR_SCRIPT
PROC CLEAR_NEED_LIST()
	INT iLoadCounter
	FOR iLoadCounter = 0 to NUMBER_OF_MODELS-1
		ModelList[iLoadCounter].modelname = DUMMY_MODEL_FOR_SCRIPT
		ModelList[iLoadCounter].bNeeded = FALSE
		ModelList[iLoadCounter].bLoaded = FALSE
	ENDFOR
	iNextEmptyModelEntry = 0
	FOR iLoadCounter = 0 to NUMBER_OF_VEHICLE_RECORDINGS-1
		VehRecList[iLoadCounter].vehrec = "missing"
		VehRecList[iLoadCounter].vehrecno = -1
		VehRecList[iLoadCounter].bNeeded = FALSE
		VehRecList[iLoadCounter].bLoaded = FALSE
	ENDFOR
	iNextEmptyVehRecEntry = 0
ENDPROC

//PURPOSE: Resets all the entries in the model list to not needed
PROC RESET_NEED_LIST()
	INT iLoadCounter
	FOR iLoadCounter = 0 to iNextEmptyModelEntry-1
		ModelList[iLoadCounter].bNeeded = FALSE
	ENDFOR
	FOR iLoadCounter = 0 to iNextEmptyVehRecEntry-1
		VehRecList[iLoadCounter].bNeeded = FALSE
	ENDFOR
ENDPROC

//PURPOSE: Checks needed of each entry- if needed but not loaded, loads. If loaded but but not needed, unloads.
PROC MANAGE_LOADING_NEED_LIST()
	INT iLoadCounter
	FOR iLoadCounter = 0 to iNextEmptyModelEntry-1
		IF ModelList[iLoadCounter].modelname <> DUMMY_MODEL_FOR_SCRIPT
			IF ModelList[iLoadCounter].bNeeded 
				IF NOT ModelList[iLoadCounter].bLoaded
					SAFE_REQUEST_MODEL(ModelList[iLoadCounter].modelname, TRUE, TRUE)
					ModelList[iLoadCounter].bLoaded = TRUE
				ENDIF
			ELSE
				IF ModelList[iLoadCounter].bLoaded
					SET_MODEL_AS_NO_LONGER_NEEDED(ModelList[iLoadCounter].modelname)
					ModelList[iLoadCounter].bLoaded = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	FOR iLoadCounter = 0 to iNextEmptyVehRecEntry-1
		IF NOT ARE_STRINGS_EQUAL(VehRecList[iLoadCounter].vehrec, "missing")
		AND VehRecList[iLoadCounter].vehrecno <> -1
			IF VehRecList[iLoadCounter].bNeeded 
				IF NOT VehRecList[iLoadCounter].bLoaded
					SAFE_REQUEST_VEHICLE_RECORDING(VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec, TRUE, TRUE)
					VehRecList[iLoadCounter].bLoaded = TRUE
				ENDIF
			ELSE
				IF VehRecList[iLoadCounter].bLoaded
					REMOVE_VEHICLE_RECORDING(VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec)
					VehRecList[iLoadCounter].bLoaded = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


//---------------------------------¦ SAFE REQUESTS ¦-----------------------------------
//PURPOSE: Requests the ptfx asset, then waits until it loads
PROC SAFE_REQUEST_PTFX_ASSET(STRING thisPTFXAsset, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_PTFX_ASSET_LOADED(thisPTFXAsset)
		REQUEST_PTFX_ASSET(thisPTFXAsset)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT HAS_PTFX_ASSET_LOADED(thisPTFXAsset)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF	
ENDPROC

//PURPOSE: Requests the heist crew models, then waits until it loads
PROC SAFE_REQUEST_MODELS_FOR_HEIST_CREW(INT thisHeistIndex, BOOL bWait, BOOL bSafely)
	//IF NOT HAVE_MODELS_LOADED_FOR_HEIST_CREW(thisHeistIndex)
		REQUEST_MODELS_FOR_HEIST_CREW(thisHeistIndex)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT HAVE_MODELS_LOADED_FOR_HEIST_CREW(thisHeistIndex)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the heist crew member models, then waits until it loads
PROC SAFE_REQUEST_MODEL_FOR_HEIST_CREW_MEMBER(INT thisHeistIndex, INT thisSlotIndex, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(thisHeistIndex, thisSlotIndex))
		REQUEST_MODEL_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(thisHeistIndex, thisSlotIndex))
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(thisHeistIndex, thisSlotIndex))
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC
		
//PURPOSE: Requests the additional text, then waits until it loads
PROC SAFE_REQUEST_ADDITIONAL_TEXT(STRING thisTextBlockName, TEXT_BLOCK_SLOTS thisSlotNumber, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(thisTextBlockName, thisSlotNumber)
		REQUEST_ADDITIONAL_TEXT(thisTextBlockName, thisSlotNumber)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(thisTextBlockName, thisSlotNumber)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the interior at coords, then waits until it loads
PROC SAFE_REQUEST_INTERIOR_AT_COORDS(INTERIOR_INSTANCE_INDEX &storeInteriorIndex, VECTOR theseCoords, BOOL bWait, BOOL bSafely)
	storeInteriorIndex = GET_INTERIOR_AT_COORDS(theseCoords)
		
	//IF NOT IS_INTERIOR_READY(storeInteriorIndex)
		PIN_INTERIOR_IN_MEMORY(storeInteriorIndex)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT IS_INTERIOR_READY(storeInteriorIndex)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the interior at coords, then waits until it loads
PROC SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(	INTERIOR_INSTANCE_INDEX &storeInteriorIndex, VECTOR theseCoords, STRING thisName,
												BOOL bWait, BOOL bSafely)
	storeInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(theseCoords, thisName)
		
	//IF NOT IS_INTERIOR_READY(storeInteriorIndex)
		PIN_INTERIOR_IN_MEMORY(storeInteriorIndex)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT IS_INTERIOR_READY(storeInteriorIndex)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the ambient audio bank, then waits until it loads
PROC SAFE_REQUEST_AMBIENT_AUDIO_BANK(STRING thisBankName, BOOL bWait, BOOL bSafely)
	//IF NOT REQUEST_AMBIENT_AUDIO_BANK(thisBankName)
		REQUEST_AMBIENT_AUDIO_BANK(thisBankName) 
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT REQUEST_AMBIENT_AUDIO_BANK(thisBankName) 
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the ambient audio bank, then waits until it loads
PROC SAFE_REQUEST_MOCAP_CUTSCENE(STRING thisMocap, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_CUTSCENE_LOADED()
		REQUEST_CUTSCENE(thisMocap)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT HAS_CUTSCENE_LOADED()
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the scaleform, then waits until it loads
PROC SAFE_REQUEST_SCALEFORM_MOVIE(SCALEFORM_INDEX &storeScaleform, STRING thisScaleform, BOOL bWait, BOOL bSafely)
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(storeScaleform)
		storeScaleform = REQUEST_SCALEFORM_MOVIE(thisScaleform)
		IF bWait
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(storeScaleform)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	ENDIF
ENDPROC


//---------------------------------¦ CUTSCENE ¦-----------------------------------
//---------¦ CAMERAS ¦---------
CAMERA_INDEX camInit, camDest, camAnim
BOOL bCutsceneStarted								= FALSE

//---------¦ FUNCTIONS ¦---------
//PURPOSE:		Performs actions required to start all cutscenes
PROC doStartCutscene(BOOL bClearPlayerTasks = TRUE, BOOL bAnimatedCam = FALSE)
	IF NOT bCutsceneStarted
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT bAnimatedCam
				camInit	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				camDest	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
				SET_CAM_COORD(camInit, <<1,1,1>>)
			ELSE
				camAnim = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
				SET_CAM_COORD(camAnim, <<1,1,1>>)
			ENDIF
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			SET_WIDESCREEN_BORDERS(TRUE, 500)
			IF bClearPlayerTasks
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_CLEAR_TASKS)
			ELSE 
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
		ENDIF
		SETTIMERB(0)
		bCutsceneStarted = TRUE
	ENDIF
ENDPROC

PROC START_SCRIPTED_CUTSCENE(BOOL bClearPlayerTasks = TRUE, BOOL bAnimatedCam = FALSE)
	IF NOT bCutsceneStarted
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT bAnimatedCam
				cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
			ELSE
//				camAnim = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//				SET_CAM_COORD(camAnim, <<1,1,1>>)
				cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
				SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
			ENDIF
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			SET_WIDESCREEN_BORDERS(TRUE, 500)
			IF bClearPlayerTasks
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_CLEAR_TASKS)
			ELSE 
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, TRUE, FALSE)
		ENDIF
		SETTIMERB(0)
		bCutsceneStarted = TRUE
	ENDIF
ENDPROC


//PURPOSE:		Checks if the cutscene is ready to skip
FUNC BOOL ReadyToSkipCutscene()
	IF TIMERB() > 1000
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:		Shakes both cameras at the same time
PROC doCamShake(STRING ShakeName, FLOAT fAmplitudeScalar = 1.0)
	SHAKE_CAM(camInit, ShakeName, fAmplitudeScalar)
	SHAKE_CAM(camDest, ShakeName, fAmplitudeScalar)
ENDPROC

//PURPOSE:		Changes shake amplitude
PROC setCamShakeAmp(FLOAT fAmplitudeScalar = 1.0)
	SET_CAM_SHAKE_AMPLITUDE(camInit, fAmplitudeScalar)
	SET_CAM_SHAKE_AMPLITUDE(camDest, fAmplitudeScalar)
ENDPROC

//PURPOSE:		Stops both cams shaking
PROC stopCamShake(BOOL bStopImmediately = FALSE)
	IF DOES_CAM_EXIST(camInit)
		STOP_CAM_SHAKING(camInit, bStopImmediately)
	ENDIF
	IF DOES_CAM_EXIST(camDest)
		STOP_CAM_SHAKING(camDest, bStopImmediately)
	ENDIF
ENDPROC

//PURPOSE: Moves a camera between Init and Dest, for as long as duration
PROC doTwoPointCam(	VECTOR camInitCoord, VECTOR camInitRot, FLOAT camInitFOV, 
					VECTOR camDestCoord, VECTOR camDestRot, FLOAT camDestFOV,
					INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR)
					
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
	DETACH_CAM(camInit)
	STOP_CAM_POINTING(camInit)
	SET_CAM_COORD(camInit, camInitCoord)
	SET_CAM_ROT(camInit, camInitRot)
	SET_CAM_FOV(camInit, camInitFOV)
	SET_CAM_ACTIVE(camDest, FALSE)
	SET_CAM_ACTIVE(camInit, TRUE)

	DETACH_CAM(camDest)
	STOP_CAM_POINTING(camDest)
	SET_CAM_COORD(camDest, camDestCoord)
	SET_CAM_ROT(camDest, camDestRot)
	SET_CAM_FOV(camDest, camDestFOV)
	SET_CAM_ACTIVE(camDest, TRUE)
	SET_CAM_ACTIVE(camInit, TRUE)
	
	SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphType, GraphType)
ENDPROC

//PURPOSE: Moves a camera between Init and Dest, for as long as duration, whilst looking at objLookAt
PROC doTwoPointEntityCam(	ENTITY_INDEX entLookAt, 
							VECTOR camInitCoord, VECTOR camInitOffset, FLOAT camInitFOV, 
							VECTOR camDestCoord, VECTOR camDestOffset, FLOAT camDestFOV,
							INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR)
							
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
	DETACH_CAM(camInit)
	STOP_CAM_POINTING(camInit)
	SET_CAM_COORD(camInit, camInitCoord)
	POINT_CAM_AT_ENTITY(camInit, entLookAt, camInitOffset)
	SET_CAM_FOV(camInit, camInitFOV)
	SET_CAM_ACTIVE(camDest, FALSE)
	SET_CAM_ACTIVE(camInit, TRUE)
	DETACH_CAM(camDest)
	STOP_CAM_POINTING(camDest)
	SET_CAM_COORD(camDest, camDestCoord)
	POINT_CAM_AT_ENTITY(camDest, entLookAt, camDestOffset)
	SET_CAM_FOV(camDest, camDestFOV)
	SET_CAM_ACTIVE(camDest, TRUE)
	SET_CAM_ACTIVE(camInit, TRUE)

	SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphType, GraphType)
ENDPROC

//PURPOSE: Moves a camera between Init and Dest, for as long as duration, looking at LookAt
PROC doTwoPointCoordCam(VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
						VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
						INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR)
						
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
	DETACH_CAM(camInit)
	STOP_CAM_POINTING(camInit)
	SET_CAM_COORD(camInit, camInitCoord)
	POINT_CAM_AT_COORD(camInit, camInitLookAt)
	SET_CAM_FOV(camInit, camInitFOV)
	SET_CAM_ACTIVE(camDest, FALSE)
	SET_CAM_ACTIVE(camInit, TRUE)

	DETACH_CAM(camDest)
	STOP_CAM_POINTING(camDest)
	SET_CAM_COORD(camDest, camDestCoord)
	POINT_CAM_AT_COORD(camDest, camDestLookAt)
	SET_CAM_FOV(camDest, camDestFOV)
	SET_CAM_ACTIVE(camDest, TRUE)
	SET_CAM_ACTIVE(camInit, TRUE)

	SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphType, GraphType)
ENDPROC

//PURPOSE: Moves a camera that's attached to a vehicle between Init and Dest, for as long as duration
PROC doTwoAttachLookAtVehicleCam(	VEHICLE_INDEX vehSubject,
									VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
									VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
									INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR)
	IF NOT IS_ENTITY_DEAD(vehSubject)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
		DETACH_CAM(camInit)
		STOP_CAM_POINTING(camInit)
		ATTACH_CAM_TO_ENTITY(camInit, vehSubject, camInitCoord)
		POINT_CAM_AT_ENTITY(camInit, vehSubject, camInitLookAt)
		SET_CAM_FOV(camInit, camInitFOV)
		SET_CAM_ACTIVE(camDest, FALSE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		DETACH_CAM(camDest)
		STOP_CAM_POINTING(camDest)
		ATTACH_CAM_TO_ENTITY(camDest, vehSubject, camDestCoord)
		POINT_CAM_AT_ENTITY(camDest, vehSubject, camDestLookAt)
		SET_CAM_FOV(camDest, camDestFOV)
		SET_CAM_ACTIVE(camDest, TRUE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphType, GraphType)
	ENDIF
ENDPROC

//PURPOSE: Moves a camera that's attached to a vehicle between Init and Dest, for as long as duration
PROC doTwoAttachLookAtEntityCam(	ENTITY_INDEX entSubject,
									VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
									VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
									INT duration)
	IF NOT IS_ENTITY_DEAD(entSubject)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
		DETACH_CAM(camInit)
		STOP_CAM_POINTING(camInit)
		ATTACH_CAM_TO_ENTITY(camInit, entSubject, camInitCoord)
		POINT_CAM_AT_ENTITY(camInit, entSubject, camInitLookAt)
		SET_CAM_FOV(camInit, camInitFOV)
		SET_CAM_ACTIVE(camDest, FALSE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		DETACH_CAM(camDest)
		STOP_CAM_POINTING(camDest)
		ATTACH_CAM_TO_ENTITY(camDest, entSubject, camDestCoord)
		POINT_CAM_AT_ENTITY(camDest, entSubject, camDestLookAt)
		SET_CAM_FOV(camDest, camDestFOV)
		SET_CAM_ACTIVE(camDest, TRUE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
	ENDIF
ENDPROC

//PURPOSE: Moves a camera that's attached to a vehicle between Init and Dest, for as long as duration
PROC doTwoAttachEntityLookAtOtherEntityCam(	ENTITY_INDEX entAttach, ENTITY_INDEX entLookAt,
											VECTOR camInitCoord, VECTOR camInitLookAt, FLOAT camInitFOV, 
											VECTOR camDestCoord, VECTOR camDestLookAt, FLOAT camDestFOV,
											INT duration)
	IF NOT IS_ENTITY_DEAD(entAttach)
	AND NOT IS_ENTITY_DEAD(entLookAt)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
		DETACH_CAM(camInit)
		STOP_CAM_POINTING(camInit)
		ATTACH_CAM_TO_ENTITY(camInit, entAttach, camInitCoord)
		POINT_CAM_AT_ENTITY(camInit, entLookAt, camInitLookAt)
		SET_CAM_FOV(camInit, camInitFOV)
		SET_CAM_ACTIVE(camDest, FALSE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		DETACH_CAM(camDest)
		STOP_CAM_POINTING(camDest)
		ATTACH_CAM_TO_ENTITY(camDest, entAttach, camDestCoord)
		POINT_CAM_AT_ENTITY(camDest, entLookAt, camDestLookAt)
		SET_CAM_FOV(camDest, camDestFOV)
		SET_CAM_ACTIVE(camDest, TRUE)
		SET_CAM_ACTIVE(camInit, TRUE)
	
		SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
	ENDIF
ENDPROC

//---------------------------------¦ SAFE FADE ¦-----------------------------------

//PURPOSE:		Only fades out if the screen is faded in
PROC safeFadeOut(INT iTime = 500)
	IF IS_SCREEN_FADED_IN()
	OR IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_OUT(iTime)
	ENDIF
ENDPROC

//PURPOSE:		Only fades in if the screen is faded out
PROC safeFadeIn(INT iTime = 500)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_IN(iTime)
	ENDIF
ENDPROC

PROC safeFadeOutAndWait(INT iTime = 500)
	IF IS_SCREEN_FADED_IN()
	OR IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_OUT(iTime)
	ENDIF
	WHILE IS_SCREEN_FADING_OUT()
		safeWait(0)
	ENDWHILE
ENDPROC

PROC safeFadeInAndWait(INT iTime = 500)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_IN(iTime)
	ENDIF
	WHILE IS_SCREEN_FADING_IN()
		safeWait(0)
	ENDWHILE
ENDPROC

PROC fadeOutAndWait(INT iTime = 500)
	IF IS_SCREEN_FADED_IN()
	OR IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_OUT(iTime)
	ENDIF
	WHILE IS_SCREEN_FADING_OUT()
		WAIT(0)
	ENDWHILE
ENDPROC

PROC fadeInAndWait(INT iTime = 500)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_IN(iTime)
	ENDIF
	WHILE IS_SCREEN_FADING_IN()
		WAIT(0)
	ENDWHILE
ENDPROC

//---------------------------------¦ TIDY UP ¦-----------------------------------

PROC TidyUpVehicle(VEHICLE_INDEX vehicleTidy, BOOL bImmediately = FALSE)
	IF bImmediately OR IS_SCREEN_FADED_OUT()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE) = vehicleTidy
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehicleTidy,FALSE) + <<2,0,0>>)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(vehicleTidy)
			IF IS_VEHICLE_DRIVEABLE(vehicleTidy)
				IF IS_ENTITY_ATTACHED(vehicleTidy)
					DETACH_ENTITY(vehicleTidy, FALSE)
				ENDIF
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(vehicleTidy)
			SAFE_DELETE_VEHICLE(vehicleTidy)
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(vehicleTidy)
			SAFE_RELEASE_VEHICLE(vehicleTidy)
		ENDIF
	ENDIF
ENDPROC

PROC TidyUpPed(PED_INDEX &pedTidy, BOOL bImmediately = FALSE)
	IF bImmediately
		IF NOT IS_ENTITY_DEAD(pedTidy)
			IF IS_ENTITY_ATTACHED(pedTidy)
				IF NOT IS_PED_IN_ANY_VEHICLE(pedTidy)
					DETACH_ENTITY(pedTidy, FALSE)
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedTidy)
			//SET_ENTITY_AS_MISSION_ENTITY(pedTidy)
			SET_PED_AS_NO_LONGER_NEEDED(pedTidy)
			DELETE_PED(pedTidy)
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(pedTidy)
			IF NOT IS_PED_INJURED(pedTidy)
				SET_PED_CONFIG_FLAG(pedTidy,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)	
				SET_PED_KEEP_TASK(pedTidy,TRUE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedTidy)
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Unloads all the models in the model list
PROC TIDY_UP_NEED_LIST()
	INT iNeedCounter
	FOR iNeedCounter = 0 to iNextEmptyModelEntry-1
		SET_MODEL_AS_NO_LONGER_NEEDED(ModelList[iNeedCounter].modelname)
		ModelList[iNeedCounter].bNeeded = FALSE
		ModelList[iNeedCounter].bLoaded = FALSE
	ENDFOR
	FOR iNeedCounter = 0 to iNextEmptyVehRecEntry-1
		REMOVE_VEHICLE_RECORDING(VehRecList[iNeedCounter].vehrecno, VehRecList[iNeedCounter].vehrec)
		VehRecList[iNeedCounter].bNeeded = FALSE
		VehRecList[iNeedCounter].bLoaded = FALSE
	ENDFOR
ENDPROC

PROC TidyUpBlip(BLIP_INDEX thisBlip)
	IF DOES_BLIP_EXIST(thisBlip)
		REMOVE_BLIP(thisBlip)
	ENDIF
ENDPROC

PROC TidyUpEnemy(COMBAT_PED_STRUCT &thisEnemy, BOOL bImmediately)
	TidyUpPed(thisEnemy.ped, bImmediately)
	TidyUpBlip(thisEnemy.blip)
	IF thisEnemy.covExist
		REMOVE_COVER_POINT(thisEnemy.cov)
		thisEnemy.covExist = FALSE
	ENDIF
	thisEnemy.iEnemyProgress = 0
ENDPROC


PROC CLEAR_TRIGGERED_LABELS()
	INT i = COUNT_OF(i_triggered_text_hashes) - 1
	
	WHILE i > -1
		i_triggered_text_hashes[i] = 0
		
		i--
	ENDWHILE
ENDPROC

//PURPOSE: re-initializes the struct controlling the enemy ai
PROC RESET_GANG_ATTACK(GANG_MOVE_ATTACK_T& struct_to_reset, BOOL bClearTask = TRUE)
	#IF IS_DEBUG_BUILD
		//DW_PRINTS("Reset gang attack called")
	#ENDIF
	struct_to_reset.bReturnToAI = 		FALSE
	struct_to_reset.bTriggered = 		FALSE
	struct_to_reset.bHasSequence = 		FALSE
	struct_to_reset.bJustAttack = 		FALSE
	struct_to_reset.bAttackMove = 		FALSE
	struct_to_reset.bJustMove = 		FALSE
	struct_to_reset.moveAttackStyle =	MOVE_FOLLOW_NAVMESH
	struct_to_reset.gang_da = 			DA_AT_GO_TO
	struct_to_reset.iPedTimer =			0
	struct_to_reset.bMovedToNewPos = 	FALSE
	IF NOT IS_ENTITY_DEAD(struct_to_reset.ped)
		IF bClearTask
			IF NOT IS_PED_INJURED(struct_to_reset.ped)
				CLEAR_PED_TASKS(struct_to_reset.ped)
			ENDIF
		ENDIF
		REMOVE_PED_DEFENSIVE_AREA (struct_to_reset.ped)
	ENDIF
ENDPROC

PROC END_SCRIPTED_CUTSCENE(BOOL bClearPlayerTasks = TRUE,BOOL bInterpToGame = FALSE, INT iTime = DEFAULT_INTERP_IN_TIME,BOOL bReturnPlayerControl = TRUE, BOOL bKillCams = TRUE)
	IF bCutsceneStarted
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE, 500)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF bKillCams
				IF DOES_CAM_EXIST(cutscene_cam)
					SET_CAM_ACTIVE(cutscene_cam, FALSE)
					DESTROY_CAM	(cutscene_cam)
				ENDIF
				RENDER_SCRIPT_CAMS(FALSE, bInterpToGame,iTime)
				STOP_GAMEPLAY_CAM_SHAKING(bKillCams)
			ENDIF
			IF bClearPlayerTasks
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF 
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
			IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), bReturnPlayerControl)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, TRUE, FALSE)
			bCutsceneStarted = FALSE
		ENDIF 
	ENDIF
ENDPROC

//PURPOSE:		Performs actions required to end all cutscenes
PROC doEndCutscene(BOOL bClearPlayerTasks = TRUE,BOOL bInterpToGame = FALSE, INT iTime = DEFAULT_INTERP_IN_TIME)
	IF bCutsceneStarted
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE, 500)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF DOES_CAM_EXIST(camInit)
				SET_CAM_ACTIVE(camInit, FALSE)
				DESTROY_CAM	(camInit)
			ENDIF
			IF DOES_CAM_EXIST(camDest)
				SET_CAM_ACTIVE(camDest, FALSE)
				DESTROY_CAM	(camDest)
			ENDIF
			IF DOES_CAM_EXIST(camAnim)
				SET_CAM_ACTIVE(camAnim, FALSE)
				DESTROY_CAM	(camAnim)
			ENDIF
			RENDER_SCRIPT_CAMS(FALSE, bInterpToGame,iTime)
			STOP_GAMEPLAY_CAM_SHAKING(TRUE)
			IF bClearPlayerTasks
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF 
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bCutsceneStarted = FALSE
		ENDIF 
	ENDIF
ENDPROC

//PURPOSE:		Deletes or removes all elements of the script
PROC MissionTidyUp(BOOL bImmediately = FALSE, BOOL bPassed = FALSE)
	//Activate player control
	
	//#IF DEBUG
	
	IF NOT bPassed
		SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_NORMAL)
	ELSE
		SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_CLEANUP)
	ENDIF
	
	IF IS_CUTSCENE_ACTIVE()
	AND IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE(TRUE)
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<< 68.4629, 3625.6042, 38.6427 >>)
		REMOVE_COVER_POINT(CoverPlayer)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupWeapon1)
		REMOVE_PICKUP(PickupWeapon1)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupWeapon2)
		REMOVE_PICKUP(PickupWeapon2)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupHealth1)
		REMOVE_PICKUP(PickupHealth1)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupHealth2)
		REMOVE_PICKUP(PickupHealth2)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupHealth3)
		REMOVE_PICKUP(PickupHealth3)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupHealth4)
		REMOVE_PICKUP(PickupHealth4)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupHealth5)
		REMOVE_PICKUP(PickupHealth5)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupWallet)
		REMOVE_PICKUP(PickupWallet)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupMoney1)
		REMOVE_PICKUP(PickupMoney1)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupMoney2)
		REMOVE_PICKUP(PickupMoney2)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupBat)
		REMOVE_PICKUP(PickupBat)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupAK)
		REMOVE_PICKUP(PickupAK)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PickupJerryCan)
		REMOVE_PICKUP(PickupJerryCan)
	ENDIF
	
	IF DOES_CAM_EXIST(cutscene_cam)
		DESTROY_CAM(cutscene_cam)
	ENDIF
	
	REMOVE_IPL("TRV1_Trail_start")
	REQUEST_IPL("TRV1_Trail_end")
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),FALSE,-1)
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		REMOVE_DECALS_IN_RANGE(<<2017.3402, 3828.2925, 31.4734>>, 100.00)	
	ENDIF
	
	SET_DOOR_STATE(DOORNAME_HENCHMAN_TRAILER_L, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_HENCHMAN_TRAILER_R, DOORSTATE_LOCKED)
	
	REMOVE_CAM_RECORDING(iCamRecs[0], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[1], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[2], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[3], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[4], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[5], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[6], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[7], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[8], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[9], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[11], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[12], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[13], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[14], strCarrec)
	REMOVE_CAM_RECORDING(iCamRecs[15], strCarrec)
	
	CLEANUP_UBER_PLAYBACK()
	bDoFailLine = FALSE
	
	INT i
	
	FOR i = 0 TO MAX_GANG - 1
		CLEANUP_AI_PED_BLIP(gangEnemy[i].EnemyBlipData)
	ENDFOR
	
	FOR i = 0 TO (MAX_ENEMY_PROPS -1)
		IF DOES_ENTITY_EXIST(bikerProp[i])
			DELETE_OBJECT(bikerProp[i])
		ENDIF
	ENDFOR
	
	DESTROY_ALL_CAMS()
	
	i_Current_Event = 0
	iWadeBackSeat = 0
	
	CLEAR_TRIGGERED_LABELS()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	ENDIF
	
	bAssignWadeTask = FALSE
	
	//REMOVE_RELATIONSHIP_GROUP(grpBuddies)
	//REMOVE_RELATIONSHIP_GROUP(grpEnemies)
	
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	
	SET_MAX_WANTED_LEVEL(6)
	SET_RADAR_ZOOM(0)
	
	IF DOES_ENTITY_EXIST(vehBikeForUber)
		SAFE_DELETE_VEHICLE(vehBikeForUber)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTrailer)
		IF IS_SCREEN_FADED_OUT()
			SAFE_DELETE_VEHICLE(vehTrailer)
		ELSE
			SAFE_RELEASE_VEHICLE(vehTrailer)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehCameraTest)
		SAFE_DELETE_VEHICLE(vehCameraTest)
	ENDIF
	
	IF DOES_CAM_EXIST(camChase)
		DESTROY_CAM(camChase)
	ENDIF
	
	//remove particle fx attached to player's truck
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearLeftWheel)
		REMOVE_PARTICLE_FX(ptfxRearLeftWheel)
	ENDIF

	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearRightWheel)
		REMOVE_PARTICLE_FX(ptfxRearRightWheel)
	ENDIF
	
	RESET_MISSION_STATS_ENTITY_WATCH() 

	REMOVE_PTFX_ASSET()

//---------¦ INTERIOR ¦---------

//---------¦ ENEMIES ¦---------
	

	REPEAT COUNT_OF(gangEnemy) iCounter
		RESET_GANG_ATTACK(gangEnemy[iCounter],FALSE)
		IF DOES_ENTITY_EXIST(gangEnemy[iCounter].ped)
			IF bImmediately OR IS_SCREEN_FADED_OUT()
				IF IS_ENTITY_A_MISSION_ENTITY(gangEnemy[iCounter].ped)
					DELETE_PED(gangEnemy[iCounter].ped)
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(gangEnemy[iCounter].ped)
					SET_PED_CONFIG_FLAG(gangEnemy[iCounter].ped,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)	
					SET_PED_KEEP_TASK(gangEnemy[iCounter].ped,TRUE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(gangEnemy[iCounter].ped)
			ENDIF
		ENDIF
		IF DOES_BLIP_EXIST(gangEnemy[iCounter].blipPed)
			REMOVE_BLIP(gangEnemy[iCounter].blipPed)
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF (gangDog) iCounter
		gangDog[iCounter].bTriggered = FALSE
		gangDog[iCounter].bHasSequence = FALSE
		IF DOES_ENTITY_EXIST(gangDog[iCounter].ped)
			IF bImmediately OR IS_SCREEN_FADED_OUT()
				DELETE_PED(gangDog[iCounter].ped)
			ELSE
				IF NOT IS_PED_INJURED(gangDog[iCounter].ped)
					SET_PED_CONFIG_FLAG(gangDog[iCounter].ped,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)	
					SET_PED_KEEP_TASK(gangDog[iCounter].ped,TRUE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(gangDog[iCounter].ped)
			ENDIF
		ENDIF
		IF DOES_BLIP_EXIST(gangDog[iCounter].blipPed)
			REMOVE_BLIP(gangDog[iCounter].blipPed)
		ENDIF
	ENDREPEAT
	
	//DELETE THE FUEL CANS
	REPEAT COUNT_OF (oFuelCan) iCounter
		IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
			IF bImmediately OR IS_SCREEN_FADED_OUT()
				DELETE_OBJECT(oFuelCan[iCounter])
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(oFuelCan[iCounter])
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(budRon.ped)
		IF bImmediately OR IS_SCREEN_FADED_OUT()
//			IF IS_SCREEN_FADED_OUT()
				DELETE_PED(budRon.ped)
//			ENDIF
		ELSE
			IF bPassed = FALSE
//				IF DOES_ENTITY_EXIST(budRon.ped)
					IF NOT IS_PED_INJURED(budRon.ped)
						SET_PED_CONFIG_FLAG(budRon.ped,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)	
						SET_PED_KEEP_TASK(budRon.ped,TRUE)
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(budRon.ped)
//				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(budWade.ped)
		IF bImmediately OR IS_SCREEN_FADED_OUT()
			DELETE_PED(budWade.ped)
		ELSE
			IF NOT IS_PED_INJURED(budWade.ped)
				SET_PED_CONFIG_FLAG(budWade.ped,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)	
				SET_PED_KEEP_TASK(budWade.ped,TRUE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(budWade.ped)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedJohnny)
		IF bImmediately OR IS_SCREEN_FADED_OUT()
			IF NOT IS_PED_INJURED(pedJohnny)
				CLEAR_PED_TASKS_IMMEDIATELY(pedJohnny)
			ENDIF
			DELETE_PED(pedJohnny)
		ELSE

			IF NOT IS_PED_DEAD_OR_DYING(pedJohnny)
				SET_ENTITY_HEALTH(pedJohnny,0)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedJohnny)
				CLEAR_PED_TASKS_IMMEDIATELY(pedJohnny)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedJohnny)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedAshley)
		IF bImmediately OR IS_SCREEN_FADED_OUT()
			IF NOT IS_PED_INJURED(pedAshley)
				CLEAR_PED_TASKS_IMMEDIATELY(pedAshley)
			ENDIF
			DELETE_PED(pedAshley)
		ELSE
			
			IF NOT IS_PED_INJURED(pedAshley)
				IF NOT IS_ENTITY_ON_SCREEN(pedAshley)
					CLEAR_PED_TASKS_IMMEDIATELY(pedAshley)
				ELSE
					TASK_SMART_FLEE_COORD(pedAshley,GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),200,-1)
				ENDIF
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedAshley)
		ENDIF
	ENDIF
	
	FOR iCounter =  0 to (MAX_CHASE_PED-1)
		TidyUpEnemy(cmbtChase[iCounter], bImmediately)
	ENDFOR
	
	FOR iGroupCount = 0 TO NUMBER_OF_GROUPS-1
		FOR iEnemyCount = 0 TO NUMBER_OF_ENEMIES_IN_GROUP-1
			TidyUpEnemy(sGroup[iGroupCount].enemy[iEnemyCount], bImmediately)
		ENDFOR
	ENDFOR
	
	e_section_stage = SECTION_STAGE_SETUP
	
//---------¦ BLIPS ¦---------	
	TidyUpBlip(blipVeh)
	TidyUpBlip(blipDest)
	TidyUpBlip(blipPed)
	FOR iGroupCount = 0 TO NUMBER_OF_GROUPS-1
		TidyUpBlip(sGroup[iGroupCount].blip)
	ENDFOR

	FOR iCounter =  0 to (MAX_DRIVER_PED-1)
		TidyUpBlip(npcDriver[iCounter].blip)
	ENDFOR
	TidyUpBlip(budRon.blip)
	TidyUpBlip(budWade.blip)
	
//---------¦ OBJECTS ¦---------
	/*FOR iCounter = 0 To (COUNT_OF(pipeFallOff) - 1)
		TidyUpEntity(pipeFallOff[iCounter].obj, bImmediately)
	ENDFOR */
	
//---------¦ VEHICLES ¦--------
	IF DOES_ENTITY_EXIST(vehTrevor.veh)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				TidyUpVehicle(vehTrevor.veh, bImmediately)
			ELSE
				IF bImmediately = TRUE
					PRINTSTRING("CLEARING THE PLAYERS TASK IMMEDIATELY")PRINTNL()
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					TidyUpVehicle(vehTrevor.veh, bImmediately)
				ELSE
					IF IS_SCREEN_FADED_OUT()
					OR IS_SCREEN_FADING_IN()
						IF bPassed = FALSE
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						ENDIF
					ENDIF
					TidyUpVehicle(vehTrevor.veh, bImmediately)
				ENDIF
			ENDIF
		ELSE
			TidyUpVehicle(vehTrevor.veh, bImmediately)
		ENDIF
	ENDIF
	
	TidyUpVehicle(vehTruck.veh, bImmediately)
	FOR iCounter =  0 to 1
		TidyUpVehicle(vehBike[iCounter].veh, bImmediately)
	ENDFOR
	FOR iCounter =  0 to (MAX_TRAILER_PARK_VEH-1)
		TidyUpVehicle(vehTrailerPark[iCounter].veh, bImmediately)
	ENDFOR
	FOR iCounter =  0 to (MAX_SETP_VEH-1)
		TidyUpVehicle(vehSetP[iCounter].veh, bImmediately)
	ENDFOR
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_spark1)
		REMOVE_PARTICLE_FX(ptfx_spark1)
		bFrag1 = FALSE
		bFrag2 = FALSE
		bFrag3 = FALSE
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_spark2)
		REMOVE_PARTICLE_FX(ptfx_spark2)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_spark3)
		REMOVE_PARTICLE_FX(ptfx_spark3)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_spark4)
		REMOVE_PARTICLE_FX(ptfx_spark4)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_crash_dust)
		REMOVE_PARTICLE_FX(ptfx_crash_dust)
	ENDIF
	
	
//---------¦ PEDS ¦---------
	TidyUpPed(budWade.ped, bImmediately)
	TidyUpPed(budRon.ped, bImmediately)
	TidyUpPed(npcOrtega.ped, bImmediately)
	FOR iCounter =  0 to (MAX_DRIVER_PED-1)
		TidyUpPed(npcDriver[iCounter].ped, bImmediately)
	ENDFOR
			
//---------¦ MODELS + ANIMS + RECORDINGS ¦---------
	TIDY_UP_NEED_LIST()
	
//---------¦ ASSISTED MOVEMENT ¦---------	
	
//---------¦ SEQUENCES ¦---------
	CLEAR_SEQUENCE_TASK(seqSequence)
	
//---------¦ CAMS ¦---------
	DESTROY_ALL_CAMS()
	
//---------¦ TEXT LABELS¦---------
	CLEAR_TRIGGERED_LABELS()
	
//---------¦ SCALEFORM ¦---------	
	
	doEndCutscene()
	IF bDisabledHotSwap = TRUE
		ENABLE_SELECTOR()
	ENDIF
	STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	SET_ROADS_BACK_TO_ORIGINAL(<< -110.4747, 4277.0381, 35.7174 >>,<< -214.5608, 4205.5488, 49.4689 >>)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<   5.9328, 3682.4102,0.0 >>,<<110.0308, 3801.6506, 100.0>>, TRUE)
	//CLEAR_TIMECYCLE_MODIFIER()
	SET_TIME_SCALE(1.0)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	IF IS_PLAYER_PLAYING(PLAYER_ID())	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	//TERMINATE_THIS_THREAD()	
//---------¦ GROUPS ¦---------

ENDPROC


//PURPOSE: All the text labels are stored as unique hash keys. Can use this to track which text labels have been displayed
FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING str_label)
	INT i_hash = GET_HASH_KEY(str_label)
	
	INT i = COUNT_OF(i_triggered_text_hashes) - 1
	WHILE i > -1
		IF i_triggered_text_hashes[i] = i_hash
			RETURN TRUE
		ENDIF
		
		i--
	ENDWHILE
		
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING str_label, BOOL b_is_triggered)
	INT i_hash = GET_HASH_KEY(str_label)
	INT i = COUNT_OF(i_triggered_text_hashes) - 1
	
	IF b_is_triggered
		BOOL b_added_label = FALSE //Stops the loop from filling all empty elements with the label hash
	
		WHILE i > -1 AND NOT b_added_label
			IF i_triggered_text_hashes[i] = 0
				i_triggered_text_hashes[i] = i_hash
				b_added_label = TRUE
			ENDIF
			
			i--
		ENDWHILE
	ELSE
		WHILE i > -1
			IF i_triggered_text_hashes[i] = i_hash
				i_triggered_text_hashes[i] = 0
			ENDIF
			
			i--
		ENDWHILE
	ENDIF
ENDPROC
PROC CLEAN_UP_AUDIO_SCENES()
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_DRIVE_TO_BIKERS")
		STOP_AUDIO_SCENE("TREVOR_1_DRIVE_TO_BIKERS")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_FOCUS_CAM_SCENE")
		STOP_AUDIO_SCENE("TREVOR_1_FOCUS_CAM_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_CHASE_FOLLOW_VAN")
		STOP_AUDIO_SCENE("TREVOR_1_CHASE_FOLLOW_VAN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_CHASE_KILL_BIKERS")
		STOP_AUDIO_SCENE("TREVOR_1_CHASE_KILL_BIKERS")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_SHOOTOUT_START")
		STOP_AUDIO_SCENE("TREVOR_1_SHOOTOUT_START")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_RAYFIRE")
		STOP_AUDIO_SCENE("TREVOR_1_RAYFIRE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_SHOOTOUT_BIKERS_FLEE")
		STOP_AUDIO_SCENE("TREVOR_1_SHOOTOUT_BIKERS_FLEE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_DRIVE_TO_TRAILER")
		STOP_AUDIO_SCENE("TREVOR_1_DRIVE_TO_TRAILER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_ARRIVE_AT_TRAILER")
		STOP_AUDIO_SCENE("TREVOR_1_ARRIVE_AT_TRAILER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_PUSH_THE_TRAILER")
		STOP_AUDIO_SCENE("TREVOR_1_PUSH_THE_TRAILER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_THREATEN_ORTEGA")
		STOP_AUDIO_SCENE("TREVOR_1_THREATEN_ORTEGA")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_ORTEGA_KILLED")
		STOP_AUDIO_SCENE("TREVOR_1_ORTEGA_KILLED")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_DRIVE_HOME")
		STOP_AUDIO_SCENE("TREVOR_1_DRIVE_HOME")
	ENDIF
	
ENDPROC

PROC MissionCleanup(BOOL bDontFadeIn = FALSE,BOOL bPassed = FALSE)
	
	REQUEST_IPL("cs4_04_trash")
	
	SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(FALSE)
	
	SET_PED_POPULATION_BUDGET(3)		
	SET_VEHICLE_POPULATION_BUDGET(3)
	ROPE_UNLOAD_TEXTURES()
	CLEAN_UP_AUDIO_SCENES()
	DISPLAY_HUD(TRUE)
	CLEAR_FOCUS()
	SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
	PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
	//1600455
	KILL_ANY_CONVERSATION()
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
			
	SET_PARTICLE_FX_BLOOD_SCALE(1.0)
	SET_PARTICLE_FX_BULLET_IMPACT_SCALE(1.0)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_SCREEN_FADED_OUT()
			CLEAR_PED_WETNESS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
		IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
			SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
		ENDIF
	ENDIF
	MissionTidyUp(FALSE,bPassed)
//	SET_FOCUS_ENTITY(PLAYER_PED_ID())
	STOP_STREAM()
	UNLOCK_MINIMAP_ANGLE()
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>,TRUE)
	REMOVE_SCENARIO_BLOCKING_AREAS()
	SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_01,FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_02,FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_03,FALSE)
	
	bTrevorSplashBackEffectOn = FALSE
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
	DISABLE_TAXI_HAILING(FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	UNREGISTER_SCRIPT_WITH_AUDIO()
	IF bDontFadeIn
		IF IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(500)
		ENDIF
	ENDIF
	
	TERMINATE_THIS_THREAD()	
ENDPROC


//PURPOSE:		Performs necessary actions for player failing the mission, then calls cleanup
PROC MissionFailed(MISSION_FAIL eMissionFail)	
	
	TRIGGER_MUSIC_EVENT("TRV1_FAIL")
	STORE_FAIL_WEAPON(PLAYER_PED_ID(), ENUM_TO_INT(SELECTOR_PED_TREVOR))
	
	STRING sFailString
	
	SWITCH eMissionFail
		CASE FAIL_GENERIC
			sFailString = "M_FAIL"
		BREAK
		
		CASE FAIL_VEHICLE_DEAD
			sFailString = "TRV_FAIL4"
		BREAK
		
		CASE FAIL_VEHICLE_UNDRIVABLE
			sFailString = "TRV_FAIL4"
		BREAK
		
		CASE FAIL_RON_DEAD
			sFailString = "TRV_FAIL2"
		BREAK
		
		CASE FAIL_RON_LEFT
			sFailString = "TRV_FAIL3"
		BREAK
		
		CASE FAIL_WADE_DEAD
			sFailString = "TRV_FAIL10"
		BREAK
		
		CASE FAIL_WADE_LEFT
			sFailString = "TRV_FAIL11"
		BREAK
		
		CASE FAIL_RON_AND_WADE_LEFT
			sFailString = "TRV_BOTH_LEFT"
		BREAK
		
		CASE FAIL_RON_AND_WADE_DEAD
			sFailString = "TRV_BOTH_LEFT"
		BREAK
		
		CASE FAIL_CHASE_LOST
			sFailString = "TRV_FAIL6"
		BREAK
		
		CASE FAIL_CHASE_LOST_DEAD_CARS
			sFailString = "TRV_FAIL7"
		BREAK
		
		CASE FAIL_CHASE_LOST_DEAD_DRIVERS
			sFailString = "TRV_FAIL9"
		BREAK
		
		CASE FAIL_LEFT_TRAILER_PARK
			sFailString = "TRV_TPFAIL"
		BREAK
		
		CASE FAIL_SPOOKED_BIKERS
			sFailString = "TRV_FAIL12"
		BREAK
		
		CASE FAIL_CHASE_LOST_VAN
			sFailString = "TRV_FAIL13"
		BREAK
		
		CASE FAIL_LEFT_ORTEGA
			sFailString = "TRV_ORTL"
		BREAK
		
		CASE FAIL_ORTEGA_KILLED_TOO_SOON
			sFailString = "TRV_FAIL14"
		BREAK
		
		CASE FAIL_LEFT_CARAVAN_AREA
			sFailString = "TRV_ORFF"
		BREAK
		
		DEFAULT
			PRINT_NOW("M_FAIL", DEFAULT_GOD_TEXT_TIME, 0)
		BREAK
	ENDSWITCH
	
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
		IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
			SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
		ENDIF
	ENDIF

	MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailString)
	
	VEHICLE_INDEX vehPlayer
	VECTOR vPlayer
	FLOAT fHeading
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlayer)
		IF IS_VEHICLE_DRIVEABLE(vehPlayer)
			IF IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
			    IF NOT IS_ENTITY_DEAD(vehPlayer) 
					IF GET_ENTITY_MODEL(vehPlayer) = GET_PLAYER_VEH_MODEL(CHAR_TREVOR)
						SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(vPlayer,fHeading)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlayer)
		IF IS_VEHICLE_DRIVEABLE(vehPlayer)
			IF IS_VEHICLE_MODEL(vehPlayer, GET_PLAYER_VEH_MODEL(CHAR_TREVOR))
				IF NOT IS_ENTITY_DEAD(vehPlayer) 
					IF IS_VEHICLE_SEAT_FREE(vehPlayer,VS_DRIVER)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE

	
	//SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_02_SS, FALSE)
	Set_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS, FALSE)	
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	//MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< 1980.9722, 3816.9128, 31.3409 >>, 295.9918)
	//SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<< 1983.1688, 3829.7612, 31.3666 >>, 294.4425)

	// must only take 1 frame and terminate the thread
	
	MissionCleanup(FALSE)
	bMissionFailedCleanup = TRUE

ENDPROC

//PURPOSE:		Checks if the user has failed a level stage
PROC FailCheck()
	IF eMissionStage <> UBER_RECORD
	AND eMissionStage <> STAGE_TEST	
		IF bDoFails
			IF bMissionFailedCleanup = FALSE
				IF MISSION_VEHICLE_FAIL_CHECKS(vehTrevor.veh)
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
						IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
							SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
						ENDIF
					ENDIF
					PRINTSTRING("@@@@@@@@@@VEHICLE FAIL CHECKS")PRINTNL()
					MissionFailed(FAIL_VEHICLE_UNDRIVABLE)
				ENDIF
			ENDIF
			
			//CHECK FOR SPOOKING BIKERS
			IF eMissionStage < STAGE_CUTSCENE_CHASE_START
				IF DOES_ENTITY_EXIST(vehBike[0].veh)
				AND DOES_ENTITY_EXIST(vehBike[1].veh)
				AND DOES_ENTITY_EXIST(cmbtChase[0].ped)
				AND DOES_ENTITY_EXIST(cmbtChase[1].ped)
				AND DOES_ENTITY_EXIST(cmbtChase[2].ped)
				AND DOES_ENTITY_EXIST(cmbtChase[3].ped)
				AND DOES_ENTITY_EXIST(vehTruck.veh)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehBike[0].veh,PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehBike[1].veh,PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(cmbtChase[0].ped,PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(cmbtChase[1].ped,PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(cmbtChase[2].ped,PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(cmbtChase[3].ped,PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTruck.veh,PLAYER_PED_ID())
						IF cmbtChase[0].iEnemyProgress != 99
							IF NOT IS_PED_INJURED(cmbtChase[0].ped)
								DETACH_ENTITY(cmbtChase[0].ped)
								TASK_COMBAT_PED(cmbtChase[0].ped,PLAYER_PED_ID())
								cmbtChase[0].iEnemyProgress = 99
							ENDIF
						ENDIF
						
						IF cmbtChase[1].iEnemyProgress != 99
							IF NOT IS_PED_INJURED(cmbtChase[1].ped)
								DETACH_ENTITY(cmbtChase[1].ped)
								TASK_COMBAT_PED(cmbtChase[1].ped,PLAYER_PED_ID())
								cmbtChase[1].iEnemyProgress = 99
							ENDIF
						ENDIF
						
						IF cmbtChase[2].iEnemyProgress != 99
							IF NOT IS_PED_INJURED(cmbtChase[2].ped)
								DETACH_ENTITY(cmbtChase[2].ped)
								TASK_COMBAT_PED(cmbtChase[2].ped,PLAYER_PED_ID())
								cmbtChase[2].iEnemyProgress = 99
							ENDIF
						ENDIF
						
						IF cmbtChase[3].iEnemyProgress != 99
							IF NOT IS_PED_INJURED(cmbtChase[3].ped)
								DETACH_ENTITY(cmbtChase[3].ped)
								TASK_COMBAT_PED(cmbtChase[3].ped,PLAYER_PED_ID())
								cmbtChase[3].iEnemyProgress = 99
							ENDIF
						ENDIF
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
							IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
								SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
							ENDIF
						ENDIF
						
						MissionFailed(FAIL_SPOOKED_BIKERS)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
				AND IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
				AND NOT IS_PED_INJURED(cmbtChase[0].ped)
				AND NOT IS_PED_INJURED(cmbtChase[1].ped)
				AND NOT IS_PED_INJURED(cmbtChase[2].ped)
				AND NOT IS_PED_INJURED(cmbtChase[3].ped)
				AND IS_VEHICLE_DRIVEABLE(vehTruck.veh)
					IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),vehBike[0].veh)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),vehBike[1].veh)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),cmbtChase[0].ped)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),cmbtChase[1].ped)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),cmbtChase[2].ped)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),cmbtChase[3].ped)
						IF cmbtChase[0].iEnemyProgress != 99
							IF NOT IS_PED_INJURED(cmbtChase[0].ped)
								DETACH_ENTITY(cmbtChase[0].ped)
								TASK_COMBAT_PED(cmbtChase[0].ped,PLAYER_PED_ID())
								cmbtChase[0].iEnemyProgress = 99
							ENDIF
						ENDIF
						
						IF cmbtChase[1].iEnemyProgress != 99
							IF NOT IS_PED_INJURED(cmbtChase[1].ped)
								DETACH_ENTITY(cmbtChase[1].ped)
								TASK_COMBAT_PED(cmbtChase[1].ped,PLAYER_PED_ID())
								cmbtChase[1].iEnemyProgress = 99
							ENDIF
						ENDIF
						
						IF cmbtChase[2].iEnemyProgress != 99
							IF NOT IS_PED_INJURED(cmbtChase[2].ped)
								DETACH_ENTITY(cmbtChase[2].ped)
								TASK_COMBAT_PED(cmbtChase[2].ped,PLAYER_PED_ID())
								cmbtChase[2].iEnemyProgress = 99
							ENDIF
						ENDIF
						
						IF cmbtChase[3].iEnemyProgress != 99
							IF NOT IS_PED_INJURED(cmbtChase[3].ped)
								DETACH_ENTITY(cmbtChase[3].ped)
								TASK_COMBAT_PED(cmbtChase[3].ped,PLAYER_PED_ID())
								cmbtChase[3].iEnemyProgress = 99
							ENDIF
						ENDIF
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
							IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
								SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
							ENDIF
						ENDIF
						MissionFailed(FAIL_SPOOKED_BIKERS)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTruck.veh,TRUE)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
							IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
								SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
							ENDIF
						ENDIF
						MissionFailed(FAIL_SPOOKED_BIKERS)
					ENDIF
				ENDIF
				
				//CHECK FOR SMOKE GRENADES AND SHIT IN THIS AREA
				IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<1983.109619,4658.884766,38.242203>>, <<1935.872559,4626.833496,56.169044>>, 59.500000,WEAPONTYPE_GRENADE)
				OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<1983.109619,4658.884766,38.242203>>, <<1935.872559,4626.833496,56.169044>>, 59.500000,WEAPONTYPE_SMOKEGRENADE)
				OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<1983.109619,4658.884766,38.242203>>, <<1935.872559,4626.833496,56.169044>>, 59.500000,WEAPONTYPE_STICKYBOMB)
				IF cmbtChase[0].iEnemyProgress != 99
					IF NOT IS_PED_INJURED(cmbtChase[0].ped)
						DETACH_ENTITY(cmbtChase[0].ped)
						TASK_COMBAT_PED(cmbtChase[0].ped,PLAYER_PED_ID())
						cmbtChase[0].iEnemyProgress = 99
					ENDIF
				ENDIF
				
				IF cmbtChase[1].iEnemyProgress != 99
					IF NOT IS_PED_INJURED(cmbtChase[1].ped)
						DETACH_ENTITY(cmbtChase[1].ped)
						TASK_COMBAT_PED(cmbtChase[1].ped,PLAYER_PED_ID())
						cmbtChase[1].iEnemyProgress = 99
					ENDIF
				ENDIF
				
				IF cmbtChase[2].iEnemyProgress != 99
					IF NOT IS_PED_INJURED(cmbtChase[2].ped)
						DETACH_ENTITY(cmbtChase[2].ped)
						TASK_COMBAT_PED(cmbtChase[2].ped,PLAYER_PED_ID())
						cmbtChase[2].iEnemyProgress = 99
					ENDIF
				ENDIF
				
				IF cmbtChase[3].iEnemyProgress != 99
					IF NOT IS_PED_INJURED(cmbtChase[3].ped)
						DETACH_ENTITY(cmbtChase[3].ped)
						TASK_COMBAT_PED(cmbtChase[3].ped,PLAYER_PED_ID())
						cmbtChase[3].iEnemyProgress = 99
					ENDIF
				ENDIF
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
						IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
							SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
						ENDIF
					ENDIF
					MissionFailed(FAIL_SPOOKED_BIKERS)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehBike[0].veh)
					IF DOES_ENTITY_EXIST(vehBike[1].veh)
						IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
							IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
								IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
									IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehBike[0].veh) < 10
										OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehBike[1].veh) < 10
											IF cmbtChase[0].iEnemyProgress != 99
												IF NOT IS_PED_INJURED(cmbtChase[0].ped)
													DETACH_ENTITY(cmbtChase[0].ped)
													TASK_COMBAT_PED(cmbtChase[0].ped,PLAYER_PED_ID())
													cmbtChase[0].iEnemyProgress = 99
												ENDIF
											ENDIF
											
											IF cmbtChase[1].iEnemyProgress != 99
												IF NOT IS_PED_INJURED(cmbtChase[1].ped)
													DETACH_ENTITY(cmbtChase[1].ped)
													TASK_COMBAT_PED(cmbtChase[1].ped,PLAYER_PED_ID())
													cmbtChase[1].iEnemyProgress = 99
												ENDIF
											ENDIF
											
											IF cmbtChase[2].iEnemyProgress != 99
												IF NOT IS_PED_INJURED(cmbtChase[2].ped)
													DETACH_ENTITY(cmbtChase[2].ped)
													TASK_COMBAT_PED(cmbtChase[2].ped,PLAYER_PED_ID())
													cmbtChase[2].iEnemyProgress = 99
												ENDIF
											ENDIF
											
											IF cmbtChase[3].iEnemyProgress != 99
												IF NOT IS_PED_INJURED(cmbtChase[3].ped)
													DETACH_ENTITY(cmbtChase[3].ped)
													TASK_COMBAT_PED(cmbtChase[3].ped,PLAYER_PED_ID())
													cmbtChase[3].iEnemyProgress = 99
												ENDIF
											ENDIF
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
												IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
													SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
												ENDIF
											ENDIF
											MissionFailed(FAIL_SPOOKED_BIKERS)
										ENDIF
									ENDIF
								ENDIF
								
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehBike[0].veh) < 30
									IF IS_PED_SHOOTING(PLAYER_PED_ID())
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
											IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
												SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
											ENDIF
										ENDIF
										MissionFailed(FAIL_SPOOKED_BIKERS)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF eMissionStage > STAGE_INIT_MISSION	
				IF bMissionFailedCleanup = FALSE
					IF DOES_ENTITY_EXIST(budRon.ped)
						IF NOT IS_PED_INJURED(budRon.ped)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped, FALSE))>250.0
								PRINTSTRING("@@@@@@@@@@TOO FAR FROM RON")PRINTNL()
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
									IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
										SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
									ENDIF
								ENDIF
								MissionFailed(FAIL_RON_LEFT)
							ENDIF
						ELIF bBuddyDeleted = FALSE AND bMissionFailedCleanup = FALSE
							PRINTSTRING("@@@@@@@@@@RON KILLED")PRINTNL()
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
								IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
									SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
								ENDIF
							ENDIF
							MissionFailed(FAIL_RON_DEAD)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF eMissionStage < STAGE_CARAVAN_SMASH
				IF bMissionFailedCleanup = FALSE
					IF DOES_ENTITY_EXIST(budWade.ped)
					AND DOES_ENTITY_EXIST(budRon.ped)
						IF IS_PED_INJURED(budWade.ped)
						AND IS_PED_INJURED(budRon.ped)
							IF bBuddyDeleted = FALSE AND bMissionFailedCleanup = FALSE
								PRINTSTRING("@@@@@@@@@@WADE AND RON KILLED")PRINTNL()
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
									IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
										SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
									ENDIF
								ENDIF
								MissionFailed(FAIL_RON_AND_WADE_DEAD)
							ENDIF
						ELSE 
							IF eMissionStage != STAGE_CARAVAN_SMASH
								IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budWade.ped,FALSE))>220.0
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped,FALSE))>200.0)
								OR (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budWade.ped,FALSE))>200.0
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped,FALSE))>220.0)
								AND NOT HAS_LABEL_BEEN_TRIGGERED("WADE LEAVE")
									PRINTSTRING("@@@@@@@@@@TOO FAR FROM RON AND WADE")PRINTNL()
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
										IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
											SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
										ENDIF
									ENDIF
									MissionFailed(FAIL_RON_AND_WADE_LEFT)
								ELSE
									IF NOT IS_PED_INJURED(budRon.ped)
										IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped, FALSE))>250.0
											PRINTSTRING("@@@@@@@@@@TOO FAR FROM RON")PRINTNL()
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
												IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
													SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
												ENDIF
											ENDIF
											MissionFailed(FAIL_RON_LEFT)
										ENDIF
									ELSE
										PRINTSTRING("@@@@@@@@@@RON KILLED")PRINTNL()
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
											IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
												SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
											ENDIF
										ENDIF
										MissionFailed(FAIL_RON_DEAD)
									ENDIF
									IF NOT IS_PED_INJURED(budWade.ped)
										IF NOT HAS_LABEL_BEEN_TRIGGERED("WADE LEAVE")
											IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budWade.ped, FALSE))>250.0
												PRINTSTRING("@@@@@@@@@@TOO FAR FROM WADE")PRINTNL()
												IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
													IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
														SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
													ENDIF
												ENDIF
												MissionFailed(FAIL_WADE_LEFT)
											ENDIF
										ENDIF
									ELSE
										PRINTSTRING("@@@@@@@@@@WADE KILLED")PRINTNL()
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
											IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
												SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
											ENDIF
										ENDIF
										MissionFailed(FAIL_WADE_DEAD)
									ENDIF
								ENDIF
							ELSE
								IF iStageSection > 6
									IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budWade.ped,FALSE))>220.0
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped,FALSE))>200.0)
									OR (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budWade.ped,FALSE))>200.0
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped,FALSE))>220.0)
									AND NOT HAS_LABEL_BEEN_TRIGGERED("WADE LEAVE")
										PRINTSTRING("@@@@@@@@@@TOO FAR FROM RON AND WADE")PRINTNL()
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
											IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
												SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
											ENDIF
										ENDIF
										MissionFailed(FAIL_RON_AND_WADE_LEFT)
									ELSE
										IF NOT IS_PED_INJURED(budRon.ped)
											IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped, FALSE))>250.0
												PRINTSTRING("@@@@@@@@@@TOO FAR FROM RON")PRINTNL()
												IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
													IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
														SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
													ENDIF
												ENDIF
												MissionFailed(FAIL_RON_LEFT)
											ENDIF
										ELSE
											PRINTSTRING("@@@@@@@@@@RON KILLED")PRINTNL()
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
												IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
													SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
												ENDIF
											ENDIF
											MissionFailed(FAIL_RON_DEAD)
										ENDIF
										IF NOT IS_PED_INJURED(budWade.ped)
											IF NOT HAS_LABEL_BEEN_TRIGGERED("WADE LEAVE")
												IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budWade.ped, FALSE))>250.0
													PRINTSTRING("@@@@@@@@@@TOO FAR FROM WADE")PRINTNL()
													IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
														IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
															SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
														ENDIF
													ENDIF
													MissionFailed(FAIL_WADE_LEFT)
												ENDIF
											ENDIF
										ELSE
											PRINTSTRING("@@@@@@@@@@WADE KILLED")PRINTNL()
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
												IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
													SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
												ENDIF
											ENDIF
											MissionFailed(FAIL_WADE_DEAD)
										ENDIF
									ENDIF
								ELSE
									IF IS_PED_INJURED(budRon.ped)
										PRINTSTRING("@@@@@@@@@@RON KILLED")PRINTNL()
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
											IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
												SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
											ENDIF
										ENDIF
										MissionFailed(FAIL_RON_DEAD)
									ENDIF
									
									IF IS_PED_INJURED(budWade.ped)
										PRINTSTRING("@@@@@@@@@@WADE KILLED")PRINTNL()
										IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
											IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
												SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
											ENDIF
										ENDIF
										MissionFailed(FAIL_WADE_DEAD)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(budRon.ped)
							IF NOT IS_PED_INJURED(budRon.ped)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped, FALSE))>250.0
									PRINTSTRING("@@@@@@@@@@TOO FAR FROM RON")PRINTNL()
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
										IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
											SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
										ENDIF
									ENDIF
									MissionFailed(FAIL_RON_LEFT)
								ENDIF
							ELIF bBuddyDeleted = FALSE AND bMissionFailedCleanup = FALSE
								PRINTSTRING("@@@@@@@@@@RON KILLED")PRINTNL()
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
									IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
										SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
									ENDIF
								ENDIF
								MissionFailed(FAIL_RON_DEAD)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				IF DOES_ENTITY_EXIST(budRon.ped)
					IF NOT IS_PED_INJURED(budRon.ped)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(budRon.ped, FALSE))>250.0
							PRINTSTRING("@@@@@@@@@@TOO FAR FROM RON")PRINTNL()
							IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
								IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
									SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
								ENDIF
							ENDIF
							MissionFailed(FAIL_RON_LEFT)
						ENDIF
					ELIF bBuddyDeleted = FALSE AND bMissionFailedCleanup = FALSE
						PRINTSTRING("@@@@@@@@@@RON KILLED")PRINTNL()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
							IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
								SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
							ENDIF
						ENDIF
						MissionFailed(FAIL_RON_DEAD)
					ENDIF
				ENDIF
			ENDIF
			
			IF eMissionStage = STAGE_CHASE
				//fail if both cars get too far away
				IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(vehTruck.veh, FALSE)) > 260.0
						//IF NOT IS_ENTITY_ON_SCREEN(vehTruck.veh)
							IF NOT IS_ENTITY_AT_COORD(vehTruck.veh, <<-57.911987,3656.892822,65.658127>>, 
																	<<146.750000,65.250000,27.250000>>)
								IF DOES_BLIP_EXIST(vehTruck.blip)
									REMOVE_BLIP(vehTruck.blip)
								ENDIF
								IF bDoFailLine = FALSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_CFAIL", CONV_PRIORITY_MEDIUM)
										bDoFailLine = TRUE
										iFailTimer = GET_GAME_TIMER()
									ENDIF
								ELSE
									IF MANAGE_MY_TIMER(iFailTimer,2000)
										TRIGGER_MUSIC_EVENT("TRV1_FAIL")	
										MissionFailed(FAIL_CHASE_LOST_VAN)
									ENDIF
								ENDIF
							ENDIF
						//ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_INJURED(cmbtChase[0].ped)
				AND IS_PED_INJURED(cmbtChase[2].ped)
				AND IS_PED_INJURED(cmbtChase[3].ped)
//				AND iStageSection > 1
					IF bDoFailLine = FALSE
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_CFAIL", CONV_PRIORITY_MEDIUM)
							bDoFailLine = TRUE
							iFailTimer = GET_GAME_TIMER()
						ENDIF
					ELSE
						IF MANAGE_MY_TIMER(iFailTimer,2000)
							TRIGGER_MUSIC_EVENT("TRV1_FAIL")	
							MissionFailed(FAIL_CHASE_LOST_DEAD_DRIVERS)
						ENDIF
					ENDIF
				ENDIF
				
				//fail if both cars are killed
				IF((NOT IS_VEHICLE_DRIVEABLE(vehTruck.veh))
				AND (NOT IS_VEHICLE_DRIVEABLE(vehBike[0].veh))
				AND (NOT IS_VEHICLE_DRIVEABLE(vehBike[1].veh)))
//				AND iStageSection > 1
					IF bDoFailLine = FALSE
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_CFAIL", CONV_PRIORITY_MEDIUM)
							bDoFailLine = TRUE
							iFailTimer = GET_GAME_TIMER()
						ENDIF
					ELSE
						IF MANAGE_MY_TIMER(iFailTimer,2000)
							TRIGGER_MUSIC_EVENT("TRV1_FAIL")
							MissionFailed(FAIL_CHASE_LOST_DEAD_CARS)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF eMissionStage = STAGE_CARAVAN_SMASH
				IF iStageSection = 10
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<4.003344,2990.983887,61.920204>>, <<-63.758190,3131.932373,17.698046>>, 39.250000)	
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
							IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
								SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
							ENDIF
						ENDIF
						MissionFailed(FAIL_LEFT_CARAVAN_AREA)		
					ENDIF
				ENDIF
			ENDIF
			
			IF eMissionStage = STAGE_CARAVAN_SMASH
				IF iStageSection = 3
					IF DOES_ENTITY_EXIST(vehTrailer)
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							IF NOT IS_ENTITY_IN_ANGLED_AREA(vehTrailer, <<-17.117577,3015.451172,48.264011>>, <<-55.769604,3129.687988,23.960649>>, 30.500000)
								MissionFailed(FAIL_LEFT_CARAVAN_AREA)		
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			//PRF_ForceEnableFlashLightForAI
			
		ENDIF
		
	ENDIF
ENDPROC

//---------------------------------¦ GAMEPLAY ¦-----------------------------------
FUNC BOOL IS_TASK_ONGOING(PED_INDEX thisPed, SCRIPT_TASK_NAME thisTask)
	IF GET_SCRIPT_TASK_STATUS(thisPed, thisTask) = PERFORMING_TASK
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC FLOAT GET_DISTANCE_BETWEEN_LINE_AND_POINT_2D(VECTOR vLineCoordA, VECTOR vLineCoordB, VECTOR vPoint)
	FLOAT fA = vLineCoordA.y - vLineCoordB.y
	FLOAT fB = vLineCoordB.x - vLineCoordA.x
	FLOAT fC = (vLineCoordA.x * vLineCoordB.y) - (vLineCoordB.x * vLineCoordA.y)

	FLOAT fAnswer = ((fA*vPoint.x) + (fB * vPoint.y) + fC) / SQRT((fA*fA)+(fB*fB))
	
	RETURN SQRT(fAnswer*fAnswer)
ENDFUNC

//                _ 
//               | |
//  ___ _ __   __| |
// / _ \ '_ \ / _` |
//|  __/ | | | (_| |
// \___|_| |_|\__,_|	end common functions for David (POST FAILCHECK)

//-----------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------¦ ASSET AND CONTROL ASSET LISTS ¦------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------


STRUCT AssetList
	MODEL_NAMES mnBike
	MODEL_NAMES mnTruck
	MODEL_NAMES mnTractor
	MODEL_NAMES mnLogTrailer
	MODEL_NAMES mnGangVan
	MODEL_NAMES mnGangTruck
	MODEL_NAMES mnTowTruck
	
	MODEL_NAMES mnLost1
	MODEL_NAMES mnLost2
	MODEL_NAMES mnMechanic
	
	MODEL_NAMES mnTrailerBaseStatic
	MODEL_NAMES mnTrailerBase
	MODEL_NAMES mnTrailerFrontside
	MODEL_NAMES mnTrailerBackside
	MODEL_NAMES mnTrailerKitchen0
	MODEL_NAMES mnTrailerKitchen1
	MODEL_NAMES mnTrailerBathroom
	MODEL_NAMES mnTrailerFridge
	MODEL_NAMES mnToilet
	MODEL_NAMES mnTrailerVeh
	MODEL_NAMES mnClay
	MODEL_NAMES mnTerry
	//MODEL_NAMES mnPipe
		
	STRING adBusted
		STRING animHandsUp
	STRING adPreach
		STRING animPreach
	STRING adSmoke
		STRING animSmoke0
		STRING animSmoke1
	STRING adCelebration
		STRING animCelebMale
		STRING animCelebFemale
		STRING animPlee
	
	STRING adNoDict
		STRING animNoAnim		
	
	STRING shakeRoadVib
	STRING shakeJolt
	STRING shakeHand
	STRING shakeSkyDive
	
	
	//STRING recBikeFlee
	
	INT recNoTrevorChase
	INT recNoTruckChase
	INT recNoBikeChase0
	INT recNoBikeChase1
	INT recNoCampervan
	INT recnoEastWave0
	INT recnoEastWave1
	INT recnoEastWave2
	INT recnoEastWave3
	INT recnoEastWave4
	INT recnoEastWave5
	INT recnoWestWave0
	INT recnoWestWave1
	INT recnoWestWave2
	INT recnoWestWave3
	INT recnoWestWave4
	INT recnoWestWave5
	
	INT skipChaseStart
	
	//STRING assistRooftop
	
	STRING convDriveTo
	STRING convFollowThem
	STRING convFirstDirtTrack
	STRING convTakeFirstDirtTrack
	STRING convLeftJump
	STRING convTakeLeftTrack
	STRING convRightTrack
	STRING convChaseEnd
	STRING convMotherfuckers
	STRING convAngry
	STRING convGoToRiverside
	STRING convSmashHisStuff
	STRING convInterrogate
	STRING convSqueal
	STRING convOnWayHome
	STRING convOnWayToCaravan
	STRING convGrenades
	STRING convWaveBike
	STRING convCover
	STRING convPussy
	STRING convBloodyBikers
	STRING convNotSoTough
	STRING convSecondWave
	STRING convWhatTheHell
	STRING convScriptCut1
	STRING convScriptCut2
	STRING convScriptCut3
	STRING convScriptCut4
	STRING convScriptCut5
	STRING convScriptCut6
	STRING convScriptCut7
	STRING convScriptCut8
	STRING convScriptCut9
	STRING convScriptCut10
	STRING convFightStart
	STRING convFightEntrance
	STRING convFightNorthWest
	STRING convFightNWAfterNE
	STRING convFightCentre
	STRING convFightNorthEast
	STRING convFightNEAfterNW
	STRING convFightEast
	STRING convFightClear1
	STRING convFightClear2
	STRING convFightClear3
	STRING convFightClear4
	STRING convFightClear5
	
	STRING godPass
	STRING godFail
	STRING godFollowThem
	STRING godFightStart
	STRING godCover
	STRING godWaitForRon
	STRING godDestroy
	STRING godGetBackToCar
	STRING godDriveToRons
	STRING godLeaveVehicle
	STRING godThreaten
	STRING godGotoSpotted
	STRING godDriveToRiverside
	STRING godLeftRonBehind
	STRING godGetInTrevorCar
	
	STRING hlpCover
	STRING hlpRage
	STRING hlpUseRage
	STRING hlpRageUsed
	STRING hlpGrenade
	STRING hlpKillHimBlue
	STRING hlpKillHimRed
	
ENDSTRUCT

AssetList Asset
PROC CreateAssetList()
	Asset.mnBike = HEXER
	Asset.mnTruck = BIFF
	Asset.mnTractor = TRACTOR2
	Asset.mnGangVan = GBURRITO
	Asset.mnGangTruck = REBEL
	
	Asset.mnTowTruck = TOWTRUCK
	
	Asset.mnLost1 = G_M_Y_LOST_01
	//ONLY ONE TYPE OF BIKER FOR NOW
	Asset.mnLost2 = G_M_Y_LOST_02
	Asset.mnTrailerVeh = PROPTRAILER
	Asset.mnClay = IG_CLAY
	Asset.mnTerry = IG_TERRY

	Asset.animPlee = "misstrevor1"

	Asset.adNoDict = "animdictnotrequired"
	Asset.animNoAnim = "animnotrequired"
		
	Asset.shakeRoadVib = "ROAD_VIBRATION_SHAKE"
	Asset.shakeJolt = "JOLT_SHAKE"
	Asset.shakeHand = "HAND_SHAKE"
	Asset.shakeSkyDive = "SKY_DIVING_SHAKE"
	
	Asset.recNoTrevorChase									= 1
	Asset.recNoTruckChase									= 2
	Asset.recNoBikeChase0									= 3
	Asset.recNoBikeChase1									= 4
	Asset.recNoCampervan									= 14
	Asset.recnoEastWave0									= 5
	Asset.recnoEastWave1									= 6
	Asset.recnoEastWave2									= 7
	Asset.recnoEastWave3									= 13
	Asset.recnoEastWave4									= 12
	Asset.recnoEastWave5									= 11
	Asset.recnoWestWave0									= 15
	Asset.recnoWestWave1									= 16
	Asset.recnoWestWave2									= 17
	Asset.recnoWestWave3									= 18
	Asset.recnoWestWave4									= 19
	Asset.recnoWestWave5									= 20
	
	Asset.skipChaseStart									= 0
	
	Asset.convDriveTo = "T1M1_PH1"
	Asset.convFollowThem = "T1M1_PH2"
	Asset.convFirstDirtTrack = "T1M1_PH3"
	Asset.convTakeFirstDirtTrack = "T1M1_PH4"
	Asset.convLeftJump = "T1M1_PH5"
	Asset.convTakeLeftTrack = "T1M1_PH6"
	Asset.convRightTrack = "T1M1_PH7"
	Asset.convChaseEnd = "T1M1_PH8"
	Asset.convMotherfuckers = "T1M1_PH9"
	Asset.convAngry = "T1M1_P10"
	Asset.convGoToRiverside = "T1M1_P11"
	Asset.convSmashHisStuff = "T1M1_P12"
	Asset.convInterrogate = "T1M1_P13"
	Asset.convSqueal = "T1M1_P14"
	Asset.convOnWayHome = "T1M1_P15"
	Asset.convOnWayToCaravan = "T1M1_P16"
	Asset.convGrenades = "T1M1_P17"
	Asset.convWaveBike = "T1M1_P18"
	Asset.convCover = "T1M1_P19"
	Asset.convPussy = "T1M1_P20"
	Asset.convBloodyBikers = "T1M1_P21"
	Asset.convNotSoTough = "T1M1_P22"
	Asset.convSecondWave = "T1M1_P23"
	Asset.convWhatTheHell = "T1M1_P24"
	Asset.convScriptCut1 = "T1M1_C1"
	Asset.convScriptCut2 = "T1M1_C2"
	Asset.convScriptCut3 = "T1M1_C3"
	Asset.convScriptCut4 = "T1M1_C4"
	Asset.convScriptCut5 = "T1M1_C5"
	Asset.convScriptCut6 = "T1M1_C6"
	Asset.convScriptCut7 = "T1M1_C7"
	Asset.convScriptCut8 = "T1M1_C8"
	Asset.convScriptCut9 = "T1M1_C9"
	Asset.convScriptCut10 = "T1M1_C10"
	Asset.convFightStart = "T1M1_F1"
	Asset.convFightEntrance = "T1M1_F2"
	Asset.convFightNorthWest = "T1M1_F3"
	Asset.convFightNWAfterNE = "T1M1_F4"
	Asset.convFightCentre = "T1M1_F5"
	Asset.convFightNorthEast = "T1M1_F6"
	Asset.convFightNEAfterNW = "T1M1_F7"
	Asset.convFightEast = "T1M1_F8"
	Asset.convFightClear1 = "T1M1_F9"
	Asset.convFightClear2 = "T1M1_F10"
	Asset.convFightClear3 = "T1M1_F11"
	Asset.convFightClear4 = "T1M1_F12"
	Asset.convFightClear5 = "T1M1_F13"
	
	Asset.godPass = "M_PASS"
	Asset.godFail = "M_FAIL"
	Asset.godFollowThem = "TRV_GT2"
	Asset.godFightStart = "TRV_GT3"
	Asset.godCover = "TRV_COVER1"
	Asset.godWaitForRon = "TRV_BDY2"
	Asset.godDestroy = "TRV_GT5"
	Asset.godGetBackToCar = "TRV_GETIN2"
	Asset.godDriveToRons = "TRV_GT7"
	Asset.godLeaveVehicle = "TRV_GT9"
	Asset.godThreaten = "TRV_GT10"
	Asset.godGotoSpotted = "TRV_GT1"
	Asset.godDriveToRiverside = "TRV_GT4"
	Asset.godLeftRonBehind = "TRV_BDY1"
	Asset.godGetInTrevorCar = "TRV_GETIN1"
	
	Asset.hlpCover = "TRV_COVER2"
	Asset.hlpRage = "TRV_HULKSMASH0"
	Asset.hlpUseRage = "TRV_HULKSMASH"
	Asset.hlpRageUsed = "TRV_HULKSMASH1"
	Asset.hlpGrenade = "TRV_GRENADE"
	Asset.hlpKillHimBlue = "TRV_GT6_3"
	Asset.hlpKillHimRed = "TRV_GT6_4"
	
ENDPROC

#IF IS_DEBUG_BUILD
	//-- Dave W functions
	BOOL bDwUseDebug = FALSE
	//PURPOSE: Debug string print
	PROC DW_PRINTS(STRING S)
		IF bDwUseDebug
			PRINTSTRING(S)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	//PURPOSE: Debug print int
	PROC DW_PRINTI(INT i, STRING S)
		IF bDwUseDebug
			PRINTSTRING(S)
			PRINTINT(i)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	//PURPOSE: Debug print float
	PROC DW_PRINTF(FLOAT f, STRING S)
		IF bDwUseDebug
			PRINTSTRING(S)
			PRINTFLOAT(f)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	//PURPOSE: Debug print Vector
	PROC DW_PRINTV(VECTOR vector_to_print, STRING vector_string)
		IF bDwUseDebug
			PRINTSTRING(vector_string)
			PRINTSTRING(" ")
			PRINTSTRING("<<")
			PRINTFLOAT(vector_to_print.x)
			PRINTSTRING(", ")
			PRINTFLOAT(vector_to_print.y)
			PRINTSTRING(", ")
			PRINTFLOAT(vector_to_print.z)
			PRINTSTRING(">>")
			PRINTNL()
		ENDIF
	ENDPROC

	// PURPOSE: Prints a vector to the file temp_debug.txt
	PROC PRINT_DW_VECTOR_TO_TEMP_FILE_WITH_STRING(VECTOR vPrint, STRING sPrint)
	   	
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(sPrint)
		SAVE_STRING_TO_DEBUG_FILE(": <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vPrint.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vPrint.y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vPrint.z)
		SAVE_STRING_TO_DEBUG_FILE(" >> ")
		
	ENDPROC
#ENDIF


//PURPOSE: Slows down A vehicle
PROC SLOW_DOWN_VEHICLE(VEHICLE_INDEX &Vehicle)
	IF IS_VEHICLE_DRIVEABLE(Vehicle)
		fVehicleSpeed = GET_ENTITY_SPEED(Vehicle)
		IF fVehicleSpeed > 7.0
			IF bBrakeing = FALSE
				TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(),Vehicle, TEMPACT_HANDBRAKESTRAIGHT, 8000)
				TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(),Vehicle, TEMPACT_BRAKE, 8000)
				bBrakeing = TRUE
			ENDIF
		ELIF  fVehicleSpeed > 0.1
		 	IF bBrakeing = TRUE
				TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(),Vehicle, TEMPACT_HANDBRAKETURNLEFT, 8000)
				bBrakeing = FALSE
			ENDIF
		ELSE
			IF bBrakeing = FALSE
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				bBrakeing = TRUE 
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STOP_PLAYER_CAR(BOOL bTurnControlBackOn = TRUE,FLOAT carSpeed = 0.2, BOOL bImmediateStop = TRUE)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		FLOAT playerCarSpeed
		VEHICLE_INDEX playerCar
		
		playerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF IS_VEHICLE_DRIVEABLE(playerCar)	
			IF bImmediateStop = TRUE
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, TRUE, FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_BRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ACCELERATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MOVE_LEFT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MOVE_RIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
			ENDIF
			
			playerCarSpeed = GET_ENTITY_SPEED(playerCar)
			
			WHILE playerCarSpeed > carSpeed
				BLOCK_FIRST_PERSON()				
			
				WAIT(0)
				
				BLOCK_FIRST_PERSON()				
				
				IF NOT IS_ENTITY_DEAD(playerCar)
					playerCarSpeed = GET_ENTITY_SPEED(playerCar)
					SLOW_DOWN_VEHICLE(playerCar)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_BRAKE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ACCELERATE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MOVE_LEFT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MOVE_RIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
				ENDIF
			ENDWHILE
			SET_PLAYER_CONTROL(PLAYER_ID(), bTurnControlBackOn)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, TRUE, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC SHOOTOUT_ZONE_T GET_ZONE_CONTAINING_PLAYER()
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<34.391068,3649.478271,38.026222>>, <<110.601883,3682.793213,41.254082>>, 28.000000)
	OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<47.669071,3613.861084,36.754425>>, <<110.732727,3641.735840,42.004292>>, 48.500000)
		RETURN ZONE_INTRO
	ELIF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<40.028873,3666.858398,37.271870>>, <<3.502873,3738.676514,43.312599>>, 48.000000)
		RETURN ZONE_LHS
	ELIF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<75.489082,3766.716309,37.440598>>, <<107.098579,3696.767822,41.240311>>, 33.500000)
		RETURN ZONE_RHS
	ELIF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<65.113228,3708.477051,33.265129>>, <<37.230984,3766.059082,41.778973>>, 39.500000)
	OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(),<<78.027145,3703.876709,34.608215>>, <<59.605186,3716.406006,44.753613>>, 41.000000)
		RETURN ZONE_CENTRE
	ENDIF
	
	RETURN ZONE_NONE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_SHOOTOUT_ZONE(SHOOTOUT_ZONE_T shootZone)
	SWITCH shootZone
		CASE ZONE_INTRO
			RETURN (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<34.391068,3649.478271,38.026222>>, <<110.601883,3682.793213,41.254082>>, 28.000000)
					OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<47.669071,3613.861084,36.754425>>, <<110.732727,3641.735840,42.004292>>, 48.500000))
		BREAK
		
		CASE ZONE_LHS
			RETURN (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<40.028873,3666.858398,37.271870>>, <<3.502873,3738.676514,43.312599>>, 48.000000))
		BREAK
		
		CASE ZONE_RHS
			RETURN (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<75.489082,3766.716309,37.440598>>, <<107.098579,3696.767822,41.240311>>, 33.500000))
		BREAK
		
		CASE ZONE_CENTRE
			RETURN (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<65.113228,3708.477051,33.265129>>, <<37.230984,3766.059082,41.778973>>, 39.500000)
					OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(),<<78.027145,3703.876709,34.608215>>, <<59.605186,3716.406006,44.753613>>, 41.000000))
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL SHOULD_RHS_VAN_SETPIECE_TRIGGER()
	FLOAT fPlayerSpeed
	fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
	IF NOT bSHouldRhsVanSetpieceTrigger
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<70.166718,3673.619629,38.335999>>, <<94.236130,3675.330322,41.011780>>, 5.000000)
		OR (fPlayerSpeed > 4.0 AND IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<70.166718,3673.619629,38.335999>>, <<94.236130,3675.330322,41.011780>>, 15.000000))
			bSHouldRhsVanSetpieceTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bSHouldRhsVanSetpieceTrigger
ENDFUNC

BOOL bPlayerApproachedCentreFromRhs
FUNC BOOL HAS_PLAYER_APPROACHED_CENTRE_FROM_RHS()
	IF NOT bPlayerApproachedCentreFromRhs
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<70.833870,3733.274902,38.048367>>, <<64.491493,3769.234619,41.597546>>, 4.500000)
		
			bPlayerApproachedCentreFromRhs = TRUE
		ENDIF
	ENDIF
	
	RETURN bPlayerApproachedCentreFromRhs
ENDFUNC

BOOL bPlayerApproachedCentreFromLhs
FUNC BOOL HAS_PLAYER_APPROACHED_CENTRE_FROM_LHS()
	IF NOT bPlayerApproachedCentreFromLhs
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<37.152649,3706.874512,37.909710>>, <<4.259892,3739.113770,42.779907>>, 4.500000)
		OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<53.157814,3696.172852,36.254219>>, <<49.159348,3698.804199,42.216061>>, 4.500000)
		OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<35.961582,3703.884277,43.003876>>, <<-31.536324,3739.534424,34.287323>>, 4.500000)
			bPlayerApproachedCentreFromLhs = TRUE
		ENDIF
	ENDIF
	
	RETURN bPlayerApproachedCentreFromLhs
ENDFUNC

BOOL bPlayerWentThroughLhsGap
FUNC BOOL HAS_PLAYER_ENTERED_CENTRE_FROM_GAP_LHS()
	IF NOT bPlayerWentThroughLhsGap
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<53.157814,3696.172852,36.254219>>, <<49.159348,3698.804199,42.216061>>, 4.500000)
			bPlayerWentThroughLhsGap = TRUE
		ENDIF
	ENDIF
	
	RETURN bPlayerWentThroughLhsGap
ENDFUNC	


BOOL bPlayerApproachedCentreFromMiddle
FUNC BOOL HAS_PLAYER_APPROACHED_CENTRE_FROM_MIDDLE()
	IF NOT bPlayerApproachedCentreFromMiddle
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<57.348606,3687.819336,37.315075>>, <<62.918701,3683.527100,42.590298>>, 4.500000)
			bPlayerApproachedCentreFromMiddle = TRUE
		ENDIF
	ENDIF
	
	RETURN bPlayerApproachedCentreFromMiddle
ENDFUNC


BOOL bPlayerApproachedMethTrailer
FUNC BOOL HAS_PLAYER_APPROACHED_METH_TRAILER
	IF NOT bPlayerApproachedMethTrailer
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<18.027880,3694.493896,38.197628>>, <<48.181362,3696.424072,42.922462>>,  5.500000) //was 4.5
			bPlayerApproachedMethTrailer = TRUE
		ENDIF
	ENDIF
	
	RETURN bPlayerApproachedMethTrailer
ENDFUNC

FUNC BOOL HAS_PLAYER_APPROACHED_CENTRE()
	RETURN (HAS_PLAYER_APPROACHED_CENTRE_FROM_MIDDLE() OR  HAS_PLAYER_APPROACHED_CENTRE_FROM_LHS() OR HAS_PLAYER_APPROACHED_CENTRE_FROM_RHS())
ENDFUNC

FUNC BOOL SHOULD_MIDDLE_RUNNERS_TRIGGER_FROM_LHS()
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<54.444710,3703.152588,38.254219>>, <<36.614941,3672.933350,41.048473>>, 7.500000)
	OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<32.205639,3684.380371,38.200718>>, <<43.326218,3675.422607,41.227501>>, 7.500000)
	OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<27.501648,3679.269531,38.132034>>, <<42.311897,3671.000244,41.206665>>, 7.500000)
		IF IS_POINT_VISIBLE_TO_PLAYER(<< 66.5306, 3729.2708, 41.0291 >> , 1.0, 200.0)
		OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 66.5306, 3729.2708, 41.0291 >>) < 10
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//PURPOSE: Returns TRUE if this_num_peds_killed out of this_num_peds have been killed 
FUNC BOOL HAVE_THESE_PEDS_BEEN_KILLED(GANG_MOVE_ATTACK_T& these_peds[], INT iTotalPedsToCheck, INT this_num_peds_killed, BOOL injured_check = TRUE, BOOL triggered_check = TRUE)
	INT i, dead_count
  	FOR i = 0 TO iTotalPedsToCheck - 1
	
		IF these_peds[i].bTriggered 
		OR NOT triggered_check
			IF DOES_ENTITY_EXIST(these_peds[i].ped)
				IF IS_ENTITY_DEAD(these_peds[i].ped)
				OR IS_PED_FATALLY_INJURED(these_peds[i].ped)
					dead_count++
				ELSE
					IF injured_check
						IF IS_PED_INJURED(these_peds[i].ped)
							dead_count++
						ELSE
							#IF IS_DEBUG_BUILD
								DW_PRINTI(i, "THis ped not injured....")
							#ENDIF
						ENDIF
					ENDIF 
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					DW_PRINTI(i, "This ped doesn't exist....")
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF dead_count >= this_num_peds_killed
   
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUMBER_OF_TRAILER_PEDS_STILL_ALIVE()
	INT i, iCount
	REPEAT COUNT_OF(gangEnemy) i
		IF NOT IS_PED_INJURED(gangEnemy[i].ped)
			iCount++
		ENDIF
	ENDREPEAT
	
	//PRINTSTRING("Enemies Left Alive: ")PRINTINT(iCount)PRINTNL()
	RETURN iCount
	
ENDFUNC
FUNC BOOL HAVE_ALL_TRAILER_PEDS_BEEN_KILLED()
	IF NOT bHaveAllTrailerPedsBeenKilled
		GANG_MOVE_ATTACK_T gangTemp[3]
		//-- Peds 4, 9 and 35 have to be killed
		gangTemp[0] = gangEnemy[40]
		gangTemp[1] = gangEnemy[9]
		gangTemp[2] = gangEnemy[35]
		IF HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 3, 3)
			INT i
			//-- See if anyone else is still alive
			REPEAT COUNT_OF(gangEnemy) i
				IF NOT IS_PED_INJURED(gangEnemy[i].ped)
					RETURN FALSE
				ENDIF
			ENDREPEAT
			bHaveAllTrailerPedsBeenKilled = TRUE
		ENDIF
	ENDIF
	
	RETURN bHaveAllTrailerPedsBeenKilled
ENDFUNC

//PURPOSE: Want the last X bikers to flee the shootout. Works out when there's only X left
FUNC BOOL ARE_ONLY_X_TRAILER_PEDS_LEFT(INT iFlee)
	IF GET_NUMBER_OF_TRAILER_PEDS_STILL_ALIVE() < iFlee + 1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: If a biker is riding a bike playing a care recording, stops the bike if the rider is knocked off the bike
PROC HANDLE_RIDER_ON_CAR_REC_BIKE(INT iRiderIndex, INT iBikeIndex)
	IF NOT IS_ENTITY_DEAD(vehTrailerPark[iBikeIndex].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[iBikeIndex].veh)
			IF IS_PED_INJURED(gangEnemy[iRiderIndex].ped)
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGH)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[iBikeIndex].veh)
			ELSE
				IF NOT IS_PED_SITTING_IN_VEHICLE(gangEnemy[iRiderIndex].ped, vehTrailerPark[iBikeIndex].veh)
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGH)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[iBikeIndex].veh)	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BURST_ALL_TYRES_ON_VEHICLE(VEHICLE_INDEX vehBurstTyres, MODEL_NAMES mBurstTyres)
	IF IS_THIS_MODEL_A_BIKE(mBurstTyres)
		IF NOT IS_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_BIKE_FRONT)
			SET_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_BIKE_FRONT)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_BIKE_REAR)
			SET_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_BIKE_REAR)
		ENDIF
	ELSE
		IF NOT IS_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_CAR_FRONT_LEFT)
			SET_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_CAR_FRONT_LEFT)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_CAR_FRONT_RIGHT)
			SET_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_CAR_FRONT_RIGHT)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_CAR_REAR_LEFT)
			SET_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_CAR_REAR_LEFT)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_CAR_REAR_RIGHT)
			SET_VEHICLE_TYRE_BURST(vehBurstTyres, SC_WHEEL_CAR_REAR_RIGHT)
		ENDIF
	ENDIF
ENDPROC
//PURPOSE: Determine if the ped passed has finished their sequence if CHECK_FOR_FINISHED, or if they have got as far as this_prg otherwise
FUNC BOOL HAS_PED_PROCESSED_SEQ(PED_INDEX this_ped, BOOL CHECK_FOR_FINISHED = TRUE, INT this_prg = 0)
	SCRIPTTASKSTATUS task_status
	INT seq_prog
	IF NOT IS_ENTITY_DEAD(this_ped)
		task_status = GET_SCRIPT_TASK_STATUS (this_ped, SCRIPT_TASK_PERFORM_SEQUENCE)
		IF CHECK_FOR_FINISHED
			IF task_status = FINISHED_TASK
				RETURN TRUE
			ELSE
			//	DW_PRINTI(ENUM_TO_INT(task_status), "task_status....") 
				RETURN FALSE
			ENDIF
		ELSE
			IF task_status = PERFORMING_TASK
				seq_prog = GET_SEQUENCE_PROGRESS(this_ped)

				IF seq_prog > this_prg
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// PURPOSE: Returns True if the specified time has passed 
FUNC BOOL HAS_DW_TIME_PASSED(INT iStartTimer, INT iTimeToCheck)

	// INPUT PARAMS:	iStartTimer			This is considered time = 0
	//					iTimeToCheck	 	The time since iStartTimer being tested for
	// RETURN VALUE		BOOL				TRUE if time iTimeToCheck  has passed since time iStartTimer 
	
	// NOTE: Requires iStartTimer to have been set before the fn is called
	
	INT iTempTime
	IF iStartTimer > 0	
		iTempTime = GET_GAME_TIMER()
		IF iTempTime - iStartTimer > iTimeToCheck
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// PURPOSE: Returns True if the specified car recording time has passed.
FUNC BOOL HAS_DW_CAR_REC_TIME_PASSED(VEHICLE_INDEX vehToCheck, FLOAT fTimeToCheck)
	// INPUT PARAMS:	vehToCheck			The vehicle playing the car recording
	//					fTimeToCheck		The time in the car recording to check for
	// RETURN VALUE		BOOL				TRUE car recoring on the car vehToCheck has been playing for at least fTimeToCheck milliseconds 

	FLOAT fTemp
	IF NOT IS_ENTITY_DEAD(vehToCheck)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToCheck)
			fTemp = GET_TIME_POSITION_IN_RECORDING(vehToCheck) 	
			IF fTemp > fTimeToCheck
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC	


PROC PRINT_GOD_TEXT(STRING strMessage, INT iTimeToDisplay = DEFAULT_GOD_TEXT_TIME)
	IF NOT IS_STRING_NULL(strMessage)
		IF NOT ARE_STRINGS_EQUAL(strMessage, "")
			PRINT_NOW(strMessage, iTimeToDisplay, 1)
		ENDIF
	ENDIF

ENDPROC



#IF IS_DEBUG_BUILD
PROC GIVE_NAME_TO_PED(PED_INDEX ped, INT index, STRING sName)
	TEXT_LABEL t1

	IF NOT IS_ENTITY_DEAD(ped)
		t1 = ""
		t1 += sName
		t1+= " "
		t1 += index
		SET_PED_NAME_DEBUG(ped, t1)
	ENDIF
	
ENDPROC

PROC SET_PED_FLOAT_NAME_DEBUG(PED_INDEX ped, FLOAT f)
	STRING s = GET_STRING_FROM_FLOAT(f)
	IF NOT IS_ENTITY_DEAD(ped)	
		SET_PED_NAME_DEBUG(ped, s)
	ENDIF

ENDPROC

PROC SHOW_PED_DISTANCE_FROM_PLAYER(PED_INDEX ped)
	IF NOT IS_ENTITY_DEAD(ped)
		VECTOR v1 = GET_ENTITY_COORDS(ped, FALSE)
		VECTOR v2 = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		FLOAT f = GET_DISTANCE_BETWEEN_COORDS(v1, v2)
		SET_PED_FLOAT_NAME_DEBUG(ped, f)
	ENDIF
	
	
ENDPROC

PROC SHOW_PED_CAR_REC_TIME_DEBUG(PED_INDEX ped)
	VEHICLE_INDEX veh
	FLOAT f
	IF NOT IS_ENTITY_DEAD(ped)
		IF IS_PED_IN_ANY_VEHICLE(ped) 	
			veh = GET_VEHICLE_PED_IS_IN(ped)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
				f = GET_TIME_POSITION_IN_RECORDING(veh)
				SET_PED_FLOAT_NAME_DEBUG(ped, f)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF	//	IS_DEBUG_BUILD

FUNC INT GET_CLOSEST_GANG_MEMBER_TO_PLAYER(FLOAT fMaxDistance = 100.0)
	INT i
	INT iCurrentClosest = -1
	

	FLOAT fCLosest = fMaxDistance * fMaxDistance
	FLOAT fCurDistSq
	
	VECTOR vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	REPEAT COUNT_OF(gangEnemy) i
		IF gangEnemy[i].bTriggered
			IF NOT IS_PED_INJURED(gangEnemy[i].ped)
				fCurDistSq = VDIST2(vPlayerLoc, GET_ENTITY_COORDS(gangEnemy[i].ped, FALSE))
				IF fCurDistSq < fCLosest
					fCLosest =  fCurDistSq
					iCurrentClosest = i
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iCurrentClosest
ENDFUNC

FUNC INT GET_NEXT_PED_TO_ATTACK(INT &gangList[], BOOL &bMovedList[], INT iCount)
	INT i, j
	FOR i = 0 TO iCount - 1
		IF NOT bMovedList[i]
			j = gangList[i]
			IF NOT IS_PED_INJURED(gangEnemy[j].ped)
				SET_ENTITY_VISIBLE(gangEnemy[j].ped, TRUE)
				RESET_GANG_ATTACK(gangEnemy[j])
				bMovedList[i] = TRUE
				RETURN j
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC
///Purpose: returns tru if a vehicle has passed the PED
FUNC BOOL IS_VEHICLE_IN_FRONT_OF_PED(VEHICLE_INDEX inCar, PED_INDEX inPed)

	VECTOR posA		// car1 position
	VECTOR posB 	// car2 position
	VECTOR posC
	VECTOR vecAB 	// vector from car1 to car2
	VECTOR vecCar2	// unit vector pointing in the direction the car2 is facing
	FLOAT fTemp
	
	// get positions
	IF NOT IS_PED_INJURED(inPed)
		posA = GET_ENTITY_COORDS(inPed, FALSE)
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(inCar)
		posB = GET_ENTITY_COORDS(inCar, FALSE)
	ENDIF

	// get vec from car1 to car2
	vecAB = posB - posA
	
	// get unit vector pointing forwards from car2
	IF IS_VEHICLE_DRIVEABLE(inCar)
		posC = get_offset_from_entity_in_world_coords(inCar, <<0.0, 5.0, 0.0>>)
		vecCar2 = posC - posB
	ENDIF

	// take the z out
	vecAB.z = 0.0
	vecCar2.z = 0.0

	// calculate dot product of vecAB and vecCar2
	fTemp = DOT_PRODUCT(vecAB,vecCar2)
	IF (fTemp >= 0.0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)

ENDFUNC	 


#IF IS_DEBUG_BUILD
PROC NAME_GANG_PEDS()
	INT i
	REPEAT COUNT_OF(gangEnemy) i
		IF DOES_ENTITY_EXIST(gangEnemy[i].ped)
			GIVE_NAME_TO_PED(gangEnemy[i].ped, i, "Gang ")
		ENDIF
	ENDREPEAT
ENDPROC

PROC NAME_RUNNER_PEDS()
	INT i
	REPEAT COUNT_OF(gangRunner) i
		IF DOES_ENTITY_EXIST(gangRunner[i].ped)
			GIVE_NAME_TO_PED(gangRunner[i].ped, i, "Run ")
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF	//	IS_DEBUG_BUILD

FUNC PED_INDEX CREATE_GANG_PED(MODEL_NAMES mGangPedModel, REL_GROUP_HASH new_ped_group, VECTOR vGangPedCoord, FLOAT fGangPedHead, WEAPON_TYPE wepGang , BOOL bInInterior = FALSE, BOOL bOnlyDamagedByPlayer = TRUE, BOOL bSetWeaponAsCurrent = TRUE)
	PED_INDEX pedGang
	INTERIOR_INSTANCE_INDEX interior
	
	pedGang = CREATE_PED (PEDTYPE_MISSION, mGangPedModel, vGangPedCoord, fGangPedHead  )
	ADD_DEADPOOL_TRIGGER(pedGang, TRV1_KILLS)
	
	GIVE_WEAPON_TO_PED(pedGang, wepGang, INFINITE_AMMO, bSetWeaponAsCurrent)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(pedGang, new_ped_group)
	SET_PED_RANDOM_COMPONENT_VARIATION ( pedGang)
	SET_PED_DIES_WHEN_INJURED(pedGang,TRUE)
	SET_PED_ALLOW_HURT_COMBAT_FOR_ALL_MISSION_PEDS(FALSE)
	SET_PED_CONFIG_FLAG(pedGang,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)	
	SET_PED_KEEP_TASK(pedGang,TRUE)
	
	IF GET_RANDOM_BOOL()
		IF GET_RANDOM_BOOL()
			SET_PED_DIES_WHEN_INJURED(pedGang,FALSE)
		ENDIF
	ENDIF
	
	IF bInInterior
		interior = GET_INTERIOR_AT_COORDS(vGangPedCoord)
		IF interior <> NULL
			RETAIN_ENTITY_IN_INTERIOR(pedGang, interior)
		ENDIF
	ENDIF
	
	SET_ENTITY_HEALTH(pedGang,135)
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(pedGang,TRUE)
	

	SET_ped_AS_ENEMY(pedGang, TRUE)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedGang, bOnlyDamagedByPlayer)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGang, TRUE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(pedGang,FALSE)
	SET_PED_USING_ACTION_MODE(pedGang, TRUE, -1, "DEFAULT_ACTION")
	
	SET_PED_COMBAT_ATTRIBUTES(pedGang,CA_USE_ENEMY_ACCURACY_SCALING,TRUE)
	
	//MISSION STATS
	RETURN pedGang
ENDFUNC

FUNC PED_INDEX CREATE_GANG_PED_INSIDE_VEHICLE(VEHICLE_INDEX vehGang, MODEL_NAMES mGangPedModel, REL_GROUP_HASH new_ped_group, WEAPON_TYPE wepGang, VEHICLE_SEAT seatGang = VS_DRIVER, BOOL bOnlyDamagedByPlayer = FALSE, BOOL bSetWeaponAsCurrent = TRUE)
	PED_INDEX pedGang
	IF NOT IS_ENTITY_DEAD(vehGang)
		pedGang = CREATE_PED_INSIDE_VEHICLE(vehGang, PEDTYPE_MISSION, mGangPedModel, seatGang)
		ADD_DEADPOOL_TRIGGER(pedGang, TRV1_KILLS)
	ENDIF
	GIVE_WEAPON_TO_PED(pedGang, wepGang, INFINITE_AMMO, bSetWeaponAsCurrent)
	SET_CURRENT_PED_WEAPON(pedGang, wepGang, bSetWeaponAsCurrent)

	SET_PED_RELATIONSHIP_GROUP_HASH(pedGang, new_ped_group)
	SET_PED_RANDOM_COMPONENT_VARIATION ( pedGang)

	SET_ped_AS_ENEMY(pedGang, TRUE)
	SET_PED_DIES_WHEN_INJURED (pedGang, TRUE)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedGang, bOnlyDamagedByPlayer)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(pedGang,FALSE)
	
	SET_ENTITY_HEALTH(pedGang,125)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGang, TRUE)
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(pedGang,TRUE)
	SET_PED_CONFIG_FLAG(pedGang,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)	
	SET_PED_KEEP_TASK(pedGang,TRUE)
	
	//OPEN COMBAT
	SET_PED_COMBAT_ATTRIBUTES(pedGang,CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedGang,CA_USE_ENEMY_ACCURACY_SCALING,TRUE)
	SET_PED_USING_ACTION_MODE(pedGang, TRUE, -1, "DEFAULT_ACTION")
	
	RETURN pedGang
ENDFUNC

PROC RSEET_RUNNER(INT index)
	gangRunner[index].bHasSequence = FALSE
	gangRunner[index].bTriggered = FALSE
	gangRunner[index].iPedTimer = 0
	IF DOES_BLIP_EXIST(gangRunner[index].blipPed)
		REMOVE_BLIP(gangRunner[index].blipPed)
	ENDIF
ENDPROC

//PURPOSE: For uber chase, need to do truck & trailer as seperate set piece, as uber recorder doesn't have functionality for truck & trailer setpiece recording
PROC MANAGE_TRUCK_AND_TRAILER_UBER_SETPIECE(FLOAT fSpeed = 1.0)
	IF NOT IS_ENTITY_DEAD(vehTruck.veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
			IF iTruckAndTrailerSetpieceProg = 0	
				IF HAS_DW_CAR_REC_TIME_PASSED(vehTruck.veh, 117000)
					REQUEST_VEHICLE_RECORDING(202, sAwUberRec) //-- Truck
					REQUEST_VEHICLE_RECORDING(203, sAwUberRec) //-- Trailer for truck
					//REQUEST_MODEL(HAULER)
					REQUEST_MODEL(TRFLAT)
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(202, sAwUberRec)
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(203, sAwUberRec)
					//AND HAS_MODEL_LOADED(HAULER)
					AND HAS_MODEL_LOADED(TRFLAT)
						IF DOES_ENTITY_EXIST(vehUberTruck[0])
							DELETE_VEHICLE(vehUberTruck[0])
						ENDIF
						IF DOES_ENTITY_EXIST(vehUberTruck[1])
							DELETE_VEHICLE(vehUberTruck[1])
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehUberTruck[0])
							vehUberTruck[1] = CREATE_VEHICLE(TRFLAT, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehUberTruck[0], <<0.0, -10.0, 0.0>>), 328.8440)
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehUberTruck[1]) AND IS_VEHICLE_DRIVEABLE(vehUberTruck[0])
							ATTACH_VEHICLE_TO_TRAILER(vehUberTruck[0], vehUberTruck[1])
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehUberTruck[0])
							START_PLAYBACK_RECORDED_VEHICLE(vehUberTruck[0], 202, sAwUberRec)
							PAUSE_PLAYBACK_RECORDED_VEHICLE(vehUberTruck[0])
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehUberTruck[1])
							START_PLAYBACK_RECORDED_VEHICLE(vehUberTruck[1], 203, sAwUberRec)
							PAUSE_PLAYBACK_RECORDED_VEHICLE(vehUberTruck[1])
						ENDIF
						iTruckAndTrailerSetpieceProg++
					ENDIF
				ENDIF
			ENDIF
			
			IF iTruckAndTrailerSetpieceProg = 1
				IF HAS_DW_CAR_REC_TIME_PASSED(vehTruck.veh, fTimeForUberTrailerAndTruck)
					IF NOT IS_ENTITY_DEAD(vehUberTruck[0])
						IF NOT IS_ENTITY_DEAD(vehUberTruck[1])
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehUberTruck[0])
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehUberTruck[1])
							iTruckAndTrailerSetpieceProg++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF iTruckAndTrailerSetpieceProg = 2
				IF NOT IS_ENTITY_DEAD(vehUberTruck[0])
					IF NOT IS_ENTITY_DEAD(vehUberTruck[1])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehUberTruck[0])
							SET_PLAYBACK_SPEED(vehUberTruck[0], fSPeed)
						ENDIF
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehUberTruck[1])
							SET_PLAYBACK_SPEED(vehUberTruck[1], fSPeed)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Set up the group of peds specified by the PED_T enum.
PROC SETUP_THESE_PEDS(PED_T pedsToSetup)
	SWITCH pedsToSetup
		CASE PED_RUNNER_LHS1
		
			IF NOT DOES_ENTITY_EXIST(gangRunner[0].ped)
				gangRunner[0].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 19.1347, 3684.3474, 38.7443 >>, 286.0925 , WEAPONTYPE_PISTOL)
				gangRunner[0].vGotoCoords = << 47.1648, 3694.5469, 38.7542 >>
				RSEET_RUNNER(0)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(gangRunner[5].ped)
				gangRunner[5].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 49.8133, 3680.6958, 38.7198 >>, 106.7312 , WEAPONTYPE_PISTOL)
				gangRunner[5].vGotoCoords = << 22.7020, 3675.8499, 38.7505 >>
				gangRunner[5].moveAttackStyle = MOVE_FOLLOW_NAVMESH_DUCK
				RSEET_RUNNER(5)
			ENDIF
			
			#IF IS_DEBUG_BUILD	
				NAME_RUNNER_PEDS()
			#ENDIF
		BREAK
		
		
		CASE PED_RUNNER_LHS2
			IF NOT DOES_ENTITY_EXIST(gangRunner[6].ped)
				gangRunner[6].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 16.2309, 3705.1865, 38.6814 >>, 224.6131 , WEAPONTYPE_PISTOL)
				gangRunner[6].vGotoCoords = << 48.7237, 3686.0332, 38.7423 >>
				RSEET_RUNNER(6)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(gangRunner[7].ped)
				gangRunner[7].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 34.1578, 3708.8159, 38.5332 >>, 162.9502 , WEAPONTYPE_PISTOL)
				gangRunner[7].vGotoCoords =<< 24.8406, 3683.7915, 38.6336 >>
				gangRunner[7].moveAttackStyle = MOVE_FOLLOW_NAVMESH_DUCK
				RSEET_RUNNER(7)
			ENDIF
			
			#IF IS_DEBUG_BUILD	
				NAME_RUNNER_PEDS()
			#ENDIF
		BREAK
		
		CASE PED_RUNNER_RHS_CLOSE1
			IF NOT DOES_ENTITY_EXIST(gangRunner[1].ped)
				gangRunner[1].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 96.1861, 3661.2688, 38.7541 >>, 286.0925 , WEAPONTYPE_PISTOL)
				gangRunner[1].vGotoCoords = << 68.3437, 3677.7722, 38.8119 >>
				RSEET_RUNNER(1)
			ENDIF	
			
			IF NOT DOES_ENTITY_EXIST(gangRunner[2].ped)
				gangRunner[2].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 77.7008, 3700.2021, 38.7543 >>, 230.1184 , WEAPONTYPE_PISTOL)
				gangRunner[2].vGotoCoords =  << 96.6802, 3687.2793, 38.6327 >>
				gangRunner[2].moveAttackStyle  =MOVE_FOLLOW_NAVMESH_DUCK
				RSEET_RUNNER(2)
			ENDIF
				
			IF NOT DOES_ENTITY_EXIST(gangRunner[3].ped)
				gangRunner[3].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 69.0141, 3676.4492, 38.7310 >>, 257.5091 , WEAPONTYPE_PISTOL)
				gangRunner[3].vGotoCoords =  << 109.1527, 3678.1072, 38.7541 >>
				gangRunner[3].moveAttackStyle  =MOVE_FOLLOW_NAVMESH_DUCK
				RSEET_RUNNER(3)
			ENDIF
			
			#IF IS_DEBUG_BUILD	
				NAME_RUNNER_PEDS()
			#ENDIF
			
		BREAK
		
		CASE PED_FIGHT_MIDDLE_RUNNER
			gangEnemy[29].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 71.5719, 3721.4912, 38.7652 >>, 87.2615 , WEAPONTYPE_PISTOL)
			gangEnemy[29].vGotoCoords = << 51.6792, 3723.3562, 38.7029 >>
			gangEnemy[29].gangCombatMovement = CM_DEFENSIVE
			gangEnemy[29].moveAttackStyle = MOVE_AIM_WHILE_GOTO
			gangEnemy[29].gang_da = DA_AT_GO_TO_LARGE
			
			//EIGHTBIKERS
			
			gangEnemy[30].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 64.8816, 3708.0137, 38.7651 >>, 59.2328 , WEAPONTYPE_PISTOL)
			gangEnemy[30].vGotoCoords = << 46.8150, 3715.0669, 38.7372 >>
			gangEnemy[30].gangCombatMovement = CM_DEFENSIVE
			gangEnemy[30].moveAttackStyle = MOVE_AIM_WHILE_GOTO
			gangEnemy[30].gang_da = DA_AT_GO_TO_LARGE
		BREAK
		
		CASE PED_GRENADE_BOSS
			IF NOT DOES_ENTITY_EXIST(gangEnemy[31].ped)
				gangEnemy[31].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<79.16, 3710.42, 40.70>>, 119.71 , WEAPONTYPE_ASSAULTRIFLE)
				gangEnemy[31].vGotoCoords = <<75.36, 3717.01, 39.75>>
				gangEnemy[31].gangCombatMovement = CM_WILLADVANCE
				gangEnemy[31].moveAttackStyle = MOVE_AIM_WHILE_GOTO
				gangEnemy[31].gang_da = DA_AT_GOTO_V_LARGE
			ENDIF
		BREAK
		
		CASE PED_FIGHT_LHS_CLOSE1
			//gangEnemy[40].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 50.8563, 3650.8015, 38.5769 >>, 187.0387 , WEAPONTYPE_PUMPSHOTGUN)
			gangEnemy[40].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<63.6262, 3659.5288, 38.5275>>, 178.7849 , WEAPONTYPE_PISTOL)
			SET_PED_TO_LOAD_COVER(gangEnemy[40].ped,TRUE)
			SET_PED_COMBAT_MOVEMENT(gangEnemy[40].ped,CM_DEFENSIVE)
		BREAK
		
		CASE PED_FIGHT_RHS_CLOSE1
			//PLAYTHROUGH CENTRAL PED REMOVED
			gangEnemy[9].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 95.0368, 3689.2026, 38.5154 >>,  174.3295, WEAPONTYPE_PISTOL)
			gangEnemy[9].vGotoCoords =  << 95.0368, 3689.2026, 38.5154 >>
			gangEnemy[9].gang_da = DA_AT_GO_TO
			gangEnemy[9].gangCombatMovement = CM_DEFENSIVE
			SET_PED_SEEING_RANGE(gangEnemy[9].ped,35)
			SET_PED_HEARING_RANGE(gangEnemy[9].ped,35)
			SET_PED_COMBAT_RANGE(gangEnemy[9].ped,CR_NEAR)
		BREAK
		
		
		CASE PED_FIGHT_INSIDE_TRAILER
			
			//gangEnemy[6].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 27.7162, 3667.6687, 39.7258 >>, 268.8322 , WEAPONTYPE_PUMPSHOTGUN)
			gangEnemy[6].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 30.87, 3667.35, 40.67 >>, -91.16 , WEAPONTYPE_PUMPSHOTGUN)
			
			gangEnemy[6].moveAttackStyle = MOVE_AIM_WHILE_GOTO
			gangEnemy[6].vGotoCoords =  <<33.8241, 3666.7546, 38.7235>>
			gangEnemy[6].gangCombatMovement = CM_DEFENSIVE
			gangEnemy[6].gang_da = DA_AT_GO_TO_SMALL
			
			SET_PED_COMBAT_ATTRIBUTES(gangEnemy[6].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(gangEnemy[6].ped,CA_CAN_CHARGE,TRUE)
			SET_ENTITY_HEALTH(gangEnemy[6].ped,120)
			SET_PED_ACCURACY(gangEnemy[6].ped,1)
			
			//gangDog[0].ped = CREATE_GANG_PED(A_C_ROTTWEILER,grpEnemies, <<29.4509, 3668.7168, 39.6770>>, 239.4872  , WEAPONTYPE_UNARMED)
			//SET_ENTITY_HEALTH(gangDog[0].ped,120)
			
			//gangDog[2].ped = CREATE_GANG_PED(A_C_ROTTWEILER,grpEnemies,  << 22.8984, 3709.6724, 38.6486 >>, 175.6694  , WEAPONTYPE_UNARMED)
			//SET_ENTITY_HEALTH(gangDog[2].ped,120)
		
			//--Guy flees on bike
			gangEnemy[7].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 28.5441, 3663.8804, 39.7258 >>, 205.9603, WEAPONTYPE_PISTOL)
			SET_PED_ACCURACY(gangEnemy[7].ped,1)
		BREAK
		
		CASE PED_FIGHT_PICKUP_SETPIECE1
			IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
				IF NOT DOES_ENTITY_EXIST(gangEnemy[17].ped)
					gangEnemy[17].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[12].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
				ENDIF
			ENDIF	
		BREAK
		
		CASE PED_FIGHT_REINF_PICKUP_SETPIECE1
			gangEnemy[8].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 49.4862, 3691.0225, 38.7542 >>, 95.4359, WEAPONTYPE_PISTOL)
			gangEnemy[8].vGotoCoords =  << 41.5072, 3686.1379, 38.5641 >>
			gangEnemy[8].gang_da = DA_AT_GO_TO_LARGE
			gangEnemy[8].gangCombatMovement = CM_DEFENSIVE
			gangEnemy[8].moveAttackStyle = MOVE_AIM_WHILE_GOTO
		
			gangEnemy[9].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 95.0368, 3689.2026, 38.5154 >>, 174.3295, WEAPONTYPE_PISTOL)
			gangEnemy[9].vGotoCoords =  << 95.0368, 3689.2026, 38.5154 >>
			gangEnemy[9].gang_da = DA_AT_GO_TO
			gangEnemy[9].gangCombatMovement = CM_DEFENSIVE
			
			gangEnemy[11].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 15.9229, 3705.1104, 38.6844 >>, 274.0622 , WEAPONTYPE_PISTOL)
			gangEnemy[11].vGotoCoords =   << 35.1732, 3704.9419, 38.6235 >>
			gangEnemy[11].gang_da = DA_AT_GO_TO	
			gangEnemy[11].gangCombatMovement = CM_DEFENSIVE
		BREAK
		
		//PLAYTHROUGH
		//531276
		CASE PED_FIGHT_BIKE_CONV_SETPIECE1
			IF NOT gangEnemy[34].bTriggered
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(3,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(4,"t1b")
	//				vehTrailerPark[15].veh = CREATE_VEHICLE(Asset.mnBike, vehTrailerPark[15].startLoc, vehTrailerPark[15].startHead)
					vehTrailerPark[15].veh = CREATE_VEHICLE(Asset.mnBike, <<56.8454, 3690.1519, 38.9216>>, 334.0780)
					
					#IF IS_DEBUG_BUILD
						SET_VEHICLE_NAME_DEBUG(vehTrailerPark[15].veh, "veh 15")
					#ENDIF
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTrailerPark[15].veh, TRUE)
					//BIKES CIRCLE
					START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[15].veh,4, "t1b",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
					SET_PLAYBACK_SPEED(vehTrailerPark[15].veh,1.1)
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[15].veh)
					gangEnemy[12].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[15].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_SAWNOFFSHOTGUN)
					
					IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[15].veh) AND NOT IS_PED_INJURED(gangEnemy[12].ped)
						TASK_DRIVE_BY(gangEnemy[12].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,0,30,TRUE)
						SET_PED_ACCURACY(gangEnemy[12].ped,1)
						SET_ENTITY_HEALTH(gangEnemy[12].ped,120)
					ENDIF
					
					//vehTrailerPark[16].veh = CREATE_VEHICLE(Asset.mnBike, vehTrailerPark[16].startLoc, vehTrailerPark[16].startHead)
					vehTrailerPark[16].veh = CREATE_VEHICLE(Asset.mnBike, <<58.6292, 3692.5254, 38.9216>>, 243.7529)
					#IF IS_DEBUG_BUILD
						SET_VEHICLE_NAME_DEBUG(vehTrailerPark[16].veh, "veh 16")
					#ENDIF
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTrailerPark[16].veh, TRUE)
					START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[16].veh,3, "t1b",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
					SET_PLAYBACK_SPEED(vehTrailerPark[16].veh,1.1)
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[16].veh)
					gangEnemy[13].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[16].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_SAWNOFFSHOTGUN)
					IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[16].veh) AND NOT IS_PED_INJURED(gangEnemy[13].ped)
						TASK_DRIVE_BY(gangEnemy[13].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,0,30,TRUE)
						SET_PED_ACCURACY(gangEnemy[13].ped,1)
						SET_ENTITY_HEALTH(gangEnemy[13].ped,120)
					ENDIF
				ELSE
					REQUEST_VEHICLE_RECORDING(3, "t1be")
					REQUEST_VEHICLE_RECORDING(4, "t1b")
				ENDIF
			ENDIF
		BREAK
		
		//PLAYTHROUGH
		//531276
		CASE PED_FIGHT_RHS_BIKE_CONV_SETPIECE1

		BREAK
		
		
		CASE PED_FIGHT_RHS_BIKE_CONV_SETPIECE2
			vehTrailerPark[24].veh = CREATE_VEHICLE(Asset.mnBike, vehTrailerPark[24].startLoc, vehTrailerPark[24].startHead)
			gangEnemy[24].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[24].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
			START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[24].veh,5, "t1b",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
			IF NOT IS_PED_INJURED(gangEnemy[24].ped)
				TASK_DRIVE_BY(gangEnemy[24].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,0,20,TRUE)
			ENDIF
			
			vehTrailerPark[25].veh = CREATE_VEHICLE(Asset.mnBike, vehTrailerPark[25].startLoc, vehTrailerPark[25].startHead)
			gangEnemy[25].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[25].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
			START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[25].veh,6, "t1b",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT))
			IF NOT IS_PED_INJURED(gangEnemy[25].ped)
				TASK_DRIVE_BY(gangEnemy[25].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,0,20,TRUE)
			ENDIF
		BREAK
		
		CASE PED_FIGHT_APPROACH_CENTRE_RHS_BIKES
			vehTrailerPark[20].veh = CREATE_VEHICLE(Asset.mnBike, << 86.6435, 3736.6387, 38.6777 >>, 65.1543)
			gangEnemy[32].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[20].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTrailerPark[20].veh, TRUE)
			START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[20].veh,13, "trevor1dw",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[20].veh)
			
			vehTrailerPark[22].veh = CREATE_VEHICLE(Asset.mnBike,<<88.9330, 3739.4028, 39.1818 >> , 69.0480)
			gangEnemy[34].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[22].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTrailerPark[22].veh, TRUE)
			START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[22].veh,15, "trevor1dw",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[22].veh) 
		BREAK
		
		CASE PED_FIGHT_APPROACH_CENTRE_LHS_BIKES
			IF NOT gangEnemy[34].bTriggered
				//vehTrailerPark[15].veh = CREATE_VEHICLE(Asset.mnBike, << 27.4327, 3697.9949, 39.2132 >>, 29.06)
				vehTrailerPark[15].veh = CREATE_VEHICLE(Asset.mnBike, <<56.8454, 3690.1519, 38.9216>>, 334.0780)
				gangEnemy[12].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[15].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)

				//vehTrailerPark[16].veh = CREATE_VEHICLE(Asset.mnBike, <<26.7395, 3695.0552, 39.2112 >> , 29.06)
				vehTrailerPark[16].veh = CREATE_VEHICLE(Asset.mnBike, <<58.6292, 3692.5254, 38.9216>>, 243.7529)
				gangEnemy[13].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[16].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_SAWNOFFSHOTGUN)
			ENDIF
		BREAK
		
		CASE PED_FIGHT_THROUGH_MIDDLE_BIKES_RHS
			vehTrailerPark[20].veh = CREATE_VEHICLE(Asset.mnBike, << 88.5008, 3736.9338, 38.6946 >>, 57.4576)
			gangEnemy[32].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[20].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTrailerPark[20].veh, TRUE)
			START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[20].veh,19, "trevor1dw",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
			
			vehTrailerPark[22].veh = CREATE_VEHICLE(Asset.mnBike, << 91.7644, 3737.0618, 38.6493 >>, 57.2141)
			gangEnemy[34].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[22].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTrailerPark[22].veh, TRUE)
			START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[22].veh,21, "trevor1dw",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
		BREAK
		
		CASE PED_FIGHT_THROUGH_MIDDLE_BIKES_LHS
			vehTrailerPark[15].veh = CREATE_VEHICLE(Asset.mnBike,<<5.78, 3660.17, 39.64>>, 55.61)
			gangEnemy[12].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[15].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
			START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[15].veh,22, "trevor1dw",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
	
			vehTrailerPark[16].veh = CREATE_VEHICLE(Asset.mnBike, <<4.62, 3658.89, 39.69>>, 55.61)
			gangEnemy[13].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[16].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
			START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailerPark[16].veh,23, "trevor1dw",ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
		BREAK
		
		CASE PED_FIGHT_INSIDE_TRAILER_RHS
			//886940
			gangEnemy[27].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 92.0689, 3751.1042, 39.7797 >>, 234.5336 , WEAPONTYPE_PUMPSHOTGUN)
			SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[27].ped,<< 62.1389, 3695.9063, 40.2992 >>,2.0)
			gangEnemy[27].vGotoCoords =  << 62.1389, 3695.9063, 40.2992 >>
			gangEnemy[27].gang_da = DA_AT_GO_TO_LARGE
			gangEnemy[27].gangCombatMovement = CM_DEFENSIVE
			gangEnemy[27].moveAttackStyle = MOVE_AIM_AT_COORD_WHILE_GOTO
			
			//METH LAB GUYS
			gangEnemy[38].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<31.7563, 3734.1489, 39.6287>>, 160.6930   , WEAPONTYPE_PISTOL)
			gangEnemy[38].moveAttackStyle = MOVE_AIM_AT_COORD_WHILE_GOTO
			gangEnemy[38].gang_da = DA_AT_GO_TO_LARGE
			gangEnemy[38].gangCombatMovement = CM_WILLADVANCE
			gangEnemy[38].vGotoCoords = <<37.5261, 3714.5693, 38.6235>>
			
			gangEnemy[39].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies,  << 31.03, 3735.61, 40.63 >>, 173.79   , WEAPONTYPE_PISTOL)
			gangEnemy[39].moveAttackStyle = MOVE_FOLLOW_NAVMESH
			gangEnemy[39].gang_da = DA_AT_GO_TO_LARGE
			gangEnemy[39].gangCombatMovement = CM_DEFENSIVE
			gangEnemy[39].vGotoCoords = <<31.03, 3735.39, 40.63>>
			
			//!!!TEMP REMOVE
//			gangDog[1].ped = CREATE_GANG_PED(A_C_ROTTWEILER,grpEnemies,  <<91.3676, 3745.0969, 39.7747>>, 253.6504  , WEAPONTYPE_UNARMED)
//			SET_ENTITY_HEALTH(gangDog[1].ped,120)

//			gangEnemy[28].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies,<< 104.9236, 3749.3547, 38.7508 >>, 144.1503, WEAPONTYPE_PISTOL)
//			gangEnemy[28].vGotoCoords =  << 94.2594, 3722.7959, 38.5839 >>
//			gangEnemy[28].gang_da = DA_AT_GO_TO
//			gangEnemy[28].gangCombatMovement = CM_WILLADVANCE
//			gangEnemy[28].moveAttackStyle = MOVE_AIM_WHILE_GOTO
//			SET_ENTITY_HEALTH(gangEnemy[28].ped,150)
		//	TASK_TOGGLE_DUCK(gangEnemy[27].ped, TOGGLE_DUCK_ON) 
		BREAK
		
		//NEW ENGLAND
		CASE PED_FIGHT_BACK_LEFT_FORKLIFT 
			gangEnemy[15].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<50.7065, 3698.1155, 38.7549>>, 150.7802 , WEAPONTYPE_PISTOL)
			gangEnemy[15].gang_da = DA_AT_GO_TO_LARGE
			gangEnemy[15].gangCombatMovement = CM_DEFENSIVE
			gangEnemy[15].moveAttackStyle = MOVE_AIM_WHILE_GOTO
			gangEnemy[15].vGotoCoords = <<37.20, 3667.34, 39.61>>
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[15].ped,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(gangEnemy[15].ped,CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED,TRUE)
			SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[15].ped,<<37.20, 3667.34, 39.61>>,2.0,TRUE)
			SET_PED_ACCURACY(gangEnemy[15].ped,1)
			
			gangEnemy[16].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<50.99, 3695.97, 39.75>>, -152.64 , WEAPONTYPE_PUMPSHOTGUN)
			gangEnemy[16].gang_da = DA_AT_GOTO_V_LARGE
			gangEnemy[16].gangCombatMovement = CM_WILLADVANCE
			gangEnemy[16].moveAttackStyle = MOVE_AGGRESSIVE_CHARGE
			gangEnemy[16].vGotoCoords = <<27.30, 3684.03, 39.51>>
			SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[16].ped,<<27.30, 3684.03, 39.51>>,10,TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[16].ped,FALSE)
		BREAK
		
		CASE PED_FIGHT_RHS_VAN_REVERSE
			IF NOT IS_ENTITY_DEAD(vehTrailerPark[3].veh)
//				gangEnemy[20].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[3].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
//				START_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[3].veh,9, "trevor1dw")
//				PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[3].veh)
			ENDIF
		BREAK
		
		CASE PED_FIGHT_RHS_FAR1
			gangEnemy[21].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 91.8644, 3717.2861, 38.7288 >>, 158.26 , WEAPONTYPE_PISTOL)
			gangEnemy[21].vGotoCoords =  << 91.8644, 3717.2861, 38.7288 >>
			gangEnemy[21].moveAttackStyle = MOVE_FOLLOW_NAVMESH
			gangEnemy[21].gang_da = DA_AT_GO_TO
			
			//PLAYTHROUGH CENTRAL GUY 
			gangEnemy[22].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 90.3136, 3704.2324, 38.5071 >>, 103.4727 , WEAPONTYPE_PISTOL)
			gangEnemy[22].vGotoCoords =  << 90.3136, 3704.2324, 38.5071 >>
			gangEnemy[22].moveAttackStyle = MOVE_FOLLOW_NAVMESH
			gangEnemy[22].gang_da = DA_AT_GO_TO
		BREAK
		
		CASE PED_FIGHT_MOVE_REAR_TO_CENTRE
			//DLMAY
		BREAK
		
		CASE PED_FIGHT_FAKE

		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NAME_GANG_PEDS()
	#ENDIF
ENDPROC




//PURPOSE: Sets the defensive area of the specified ped. 
PROC SET_PED_DA(GANG_MOVE_ATTACK_T& da_ped, DEFENSIVE_T area_to_set)
	SWITCH area_to_set

		CASE DA_AT_GO_TO_SMALL
			SET_PED_SPHERE_DEFENSIVE_AREA(da_ped.ped, da_ped.vGotoCoords, 2.0)
		BREAK
		
		CASE DA_AT_GO_TO_LARGE
			SET_PED_SPHERE_DEFENSIVE_AREA(da_ped.ped, da_ped.vGotoCoords, 5.0)
		BREAK
		
		CASE DA_AT_GOTO_V_LARGE
			SET_PED_SPHERE_DEFENSIVE_AREA(da_ped.ped, da_ped.vGotoCoords, 15.0)
		BREAK
		
		CASE DA_AT_GO_TO
			SET_PED_SPHERE_DEFENSIVE_AREA(da_ped.ped, da_ped.vGotoCoords, 1.5,TRUE)
		BREAK
		
	ENDSWITCH
ENDPROC

PROC GET_PED_TO_DRIVEBY_PLAYER(PED_INDEX this_ped, VEHICLE_INDEX this_car)
	SEQUENCE_INDEX temp_seq
	IF NOT IS_PED_INJURED(this_ped)
		IF NOT IS_ENTITY_DEAD(this_car)
			IF IS_PED_IN_VEHICLE(this_ped, this_car)
				IF GET_SCRIPT_TASK_STATUS(this_ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					OPEN_SEQUENCE_TASK(temp_seq)
						TASK_DRIVE_BY(NULL, PLAYER_PED_ID(),NULL, <<0.0, 0.0, 0.0>>, 100.0, 60)
					CLOSE_SEQUENCE_TASK(temp_seq)
					TASK_PERFORM_SEQUENCE(this_ped, temp_seq) 
					CLEAR_SEQUENCE_TASK(temp_seq)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_VECTOR_ALMOST_ZERO(VECTOR v_this_vector)

	IF v_this_vector.x < 1.0 AND v_this_vector.x > - 1.0
		RETURN TRUE
	ENDIF
	IF v_this_vector.y < 1.0 AND v_this_vector.y > - 1.0
		RETURN TRUE
	ENDIF
	IF v_this_vector.z < 1.0 AND v_this_vector.z > - 1.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the AI of the enemy gang
PROC MANAGE_gang_attack(GANG_MOVE_ATTACK_T &gangBank[])
		/*
		Every ped attacking the player either attacks where they are, or moves to a point and then attacks.
		The ped moves to the point specified by gangBank[].vGotoCoords
		How the ped moves is determined by gangBank[].moveAttackStyle. E.g. if MOVE_AIM_AT_COORD_WHILE_GOTO is set,
		the enemy will aim at the player until they reach their specified coordinate
		
		While the ped is doing the move stage of the process, SET_BLOCKING_OF_NON_TEMPORARY_EVENTS is set to TRUE 
	*/
	INT i

	SEQUENCE_INDEX temp_sequence
	REPEAT COUNT_OF(gangBank) i
		IF i !=23
			IF gangBank[i].bTriggered
				IF NOT IS_ENTITY_DEAD(gangBank[i].ped)
					IF NOT gangBank[i].bHasSequence
					AND NOT gangBank[i].bJustAttack
					 	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS ( gangBank[i].ped, TRUE )
						SET_ENTITY_VISIBLE(gangBank[i].ped, TRUE)
						SET_PED_CAN_BE_TARGETTED(gangBank[i].ped, TRUE)
						
						IF (gangBank[i].moveAttackStyle = MOVE_FOLLOW_NAVMESH OR gangBank[i].moveAttackStyle = MOVE_AIM_WHILE_GOTO 
						OR gangBank[i].moveAttackStyle = MOVE_SHOOT_WHILE_GOTO OR gangBank[i].moveAttackStyle = MOVE_AIM_AT_COORD_WHILE_GOTO)
						AND (IS_ENTITY_AT_COORD(gangBank[i].ped, gangBank[i].vGotoCoords, << 1.0, 1.0, 1.5 >>)) 
						AND (NOT IS_PED_SITTING_IN_ANY_VEHICLE(gangBank[i].ped))
							gangBank[i].bHasSequence = TRUE
						ELSE
						
							SWITCH gangBank[i].moveAttackStyle
								CASE MOVE_FOLLOW_NAVMESH
									IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
										OPEN_SEQUENCE_TASK(temp_sequence)
											IF IS_PED_IN_ANY_VEHICLE(gangBank[i].ped)
												TASK_LEAVE_ANY_VEHICLE(NULL)
											ENDIF
											IF IS_PED_DUCKING(gangBank[i].ped)
												TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_OFF)
											ENDIF
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, gangBank[i].vGotoCoords, PEDMOVE_RUN, -1, 0.5)						
											TASK_PAUSE(NULL, gangBank[i].iAttackDelay)
										CLOSE_SEQUENCE_TASK(temp_sequence)
									ENDIF
								BREAK
								
								CASE MOVE_AGGRESSIVE_CHARGE
									SET_PED_COMBAT_MOVEMENT(gangBank[i].ped,CM_WILLADVANCE)
									SET_PED_COMBAT_ATTRIBUTES(gangBank[i].ped,CA_CAN_CHARGE,TRUE)
									IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
										OPEN_SEQUENCE_TASK(temp_sequence)
											TASK_PAUSE(NULL, gangBank[i].iAttackDelay)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,gangBank[i].vGotoCoords, PEDMOVE_RUN, -1, 0.5)
										CLOSE_SEQUENCE_TASK(temp_sequence)
									ENDIF
								BREAK
								
								CASE MOVE_FOLLOW_NAVMESH_DUCK
									IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
										OPEN_SEQUENCE_TASK(temp_sequence)
											TASK_PAUSE(NULL, gangBank[i].iAttackDelay)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,gangBank[i].vGotoCoords, PEDMOVE_RUN, -1, 0.5)
										CLOSE_SEQUENCE_TASK(temp_sequence)
									ENDIF
								BREAK

								CASE MOVE_AIM_WHILE_GOTO
									IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
										OPEN_SEQUENCE_TASK(temp_sequence)	
											IF IS_PED_IN_ANY_VEHICLE(gangBank[i].ped)
												TASK_LEAVE_ANY_VEHICLE(NULL)
											ENDIF
											IF IS_PED_DUCKING(gangBank[i].ped)
												TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_OFF)
											ENDIF
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, gangBank[i].vGotoCoords, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE,0.5,4,TRUE,ENAV_DEFAULT,FALSE,DEFAULT,DEFAULT_TIME_NEVER_WARP)
										CLOSE_SEQUENCE_TASK(temp_sequence)
									ENDIF
								BREAK

								CASE MOVE_SHOOT_WHILE_GOTO
									IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
										OPEN_SEQUENCE_TASK(temp_sequence)					
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, gangBank[i].vGotoCoords, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE,0.5,4,TRUE,ENAV_DEFAULT,FALSE,DEFAULT,DEFAULT_TIME_NEVER_WARP)
										CLOSE_SEQUENCE_TASK(temp_sequence)
									ENDIF
								BREAK

								CASE MOVE_AIM_AT_COORD_WHILE_GOTO
									IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
										IF IS_VECTOR_ALMOST_ZERO(gangBank[i].vGotoCoords)
											OPEN_SEQUENCE_TASK(temp_sequence)					
												TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, gangBank[i].vGotoCoords, gangBank[i].vOtherGotoCoords, PEDMOVE_RUN, TRUE)
											CLOSE_SEQUENCE_TASK(temp_sequence)	
										ELSE
											OPEN_SEQUENCE_TASK(temp_sequence)					
												TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
											CLOSE_SEQUENCE_TASK(temp_sequence)
										ENDIF
									ENDIF
								BREAK

								
								CASE MOVE_USE_SPEC_COVERPOINT
									
									IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
									//  	ADD_COVER_POINT( gangBank[i].vGotoCoords.x, gangBank[i].vGotoCoords.y, gangBank[i].vGotoCoords.z, gangBank[i].CoverInfoForPed.coverUsageType, gangBank[i].CoverInfoForPed.fCoverHead, gangBank[i].CoverInfoForPed.coverHeight, COVARC_180, gangBank[i].CoverInfoForPed.coverId)	
										OPEN_SEQUENCE_TASK(temp_sequence)					
											TASK_SEEK_COVER_TO_COVER_POINT( NULL, gangBank[i].coverGangUse, gangBank[i].vOtherGotoCoords, 50)
										CLOSE_SEQUENCE_TASK(temp_sequence)		
									ENDIF
								BREAK

								CASE MOVE_ROLL_LEFT
								/*	OPEN_SEQUENCE_TASK(temp_sequence)					
										TASK_TOGGLE_DUCK (NULL, TOGGLE_DUCK_ON)
										TASK_COMBAT_ROLL (NULL, RD_LEFT)	
										TASK_SHOOT_AT_ENTITY( NULL, PLAYER_PED_ID(), 2000, FIRING_TYPE_RANDOM_BURSTS ) 
									CLOSE_SEQUENCE_TASK(temp_sequence) */
								BREAK
							/*	
								CASE MOVE_ROLL_RIGHT
									OPEN_SEQUENCE_TASK(temp_sequence)					
										TASK_TOGGLE_DUCK (NULL, TOGGLE_DUCK_ON)
										TASK_COMBAT_ROLL (NULL, RD_RIGHT)	
										TASK_SHOOT_AT_ENTITY( NULL, PLAYER_PED_ID(), 2000, FIRING_TYPE_RANDOM_BURSTS ) 
									CLOSE_SEQUENCE_TASK(temp_sequence) 
								BREAK*/
							ENDSWITCH
							IF i !=23
							OR i !=38
							OR i !=39
								IF gangBank[i].bHasSequence = FALSE	
									IF NOT IS_PED_INJURED(gangBank[i].ped)
										IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
											TASK_PERFORM_SEQUENCE(gangBank[i].ped, temp_sequence)
										ENDIF
									ENDIF
									CLEAR_SEQUENCE_TASK(temp_sequence)
									gangBank[i].bHasSequence = TRUE
								ENDIF
							ENDIF
						ENDIF

						
					ELSE
						IF NOT gangBank[i].bAttackMove
						OR gangBank[i].bJustAttack
							IF NOT gangBank[i].bJustMove
							OR gangBank[i].bSpecial = FALSE
								IF HAS_PED_PROCESSED_SEQ(gangBank[i].ped, TRUE, 0)
								OR ((gangBank[i].moveAttackStyle = MOVE_USE_SPEC_COVERPOINT) AND IS_ENTITY_AT_COORD(gangBank[i].ped, gangBank[i].vGotoCoords, <<0.5, 0.5, 2.0>>,FALSE))  
								OR gangBank[i].bJustAttack
								 	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS ( gangBank[i].ped, FALSE )
							 	  	//SET_PED_COMBAT_MOVEMENT (gangBank[i].ped, gangBank[i].gangCombatMovement )
									SET_PED_TARGET_LOSS_RESPONSE (gangBank[i].ped, TLR_NEVER_LOSE_TARGET)
									IF i !=23
										IF i !=4
											SET_PED_DA( gangBank[i], gangBank[i].gang_da) 
										ENDIF
									
										OPEN_SEQUENCE_TASK(temp_sequence)
								  			TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(temp_sequence)

										IF NOT IS_PED_INJURED(gangBank[i].ped)
											IF GET_SCRIPT_TASK_STATUS(gangBank[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
												TASK_PERFORM_SEQUENCE(gangBank[i].ped, temp_sequence)
											ENDIF
										ENDIF
										CLEAR_SEQUENCE_TASK(temp_sequence)
										gangBank[i].bAttackMove = TRUE
										gangBank[i].bJustAttack = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Fudge the dogs chasing the player. Just ge them to go to the player's current position until we get proper dog ai
PROC MANAGE_DOGS()
	INT i
	SEQUENCE_INDEX seq

	REPEAT COUNT_OF(gangDog) i
		IF NOT IS_PED_INJURED(gangDog[i].ped)
			IF gangDog[i].bTriggered
				IF NOT gangDog[i].bHasSequence
					gangDog[i].vGotoCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					IF GET_SCRIPT_TASK_STATUS(gangDog[i].ped,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(Seq)
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, gangDog[i].vGotoCoords, PEDMOVE_SPRINT, -1, 0.5)
							TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(gangDog[i].ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
					gangDog[i].iPedTimer = GET_GAME_TIMER()
					gangDog[i].bHasSequence = TRUE
				ELSE
					IF HAS_PED_PROCESSED_SEQ(gangDog[i].ped)
						gangDog[i].bHasSequence = FALSE
					ELSE
						IF HAS_DW_TIME_PASSED(gangDog[i].iPedTimer, 2000)
							IF GET_DISTANCE_BETWEEN_ENTITIES(gangDog[i].ped, PLAYER_PED_ID()) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(gangDog[i].ped, FALSE), gangDog[i].vGotoCoords)
								gangDog[i].bHasSequence = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
//flashes blips
FUNC BOOL flashBlipForKillDecision(BLIP_INDEX &thisBlip, INT &iControlTimer, BOOL &bFriendly)
	IF (GET_GAME_TIMER() - iControlTimer) > 500
		IF bFriendly
			SET_BLIP_AS_FRIENDLY(thisBlip, TRUE)
			SET_BLIP_COLOUR(thisBlip, BLIP_COLOUR_BLUE)
			iControlTimer = GET_GAME_TIMER()
			//SCRIPT_ASSERT("BLIP SET TO BLUE")
			bFriendly = FALSE
			RETURN TRUE
		ELSE
			SET_BLIP_COLOUR(thisBlip, BLIP_COLOUR_RED)
			SET_BLIP_AS_FRIENDLY(thisBlip, FALSE)
			iControlTimer = GET_GAME_TIMER()
			//SCRIPT_ASSERT("BLIP SET TO RED")
			bFriendly = TRUE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


FUNC BOOL flashTextForKillDecision(STRING sText1, STRING sText2, INT &iControlTimer, BOOL &bFriendly)
	IF (GET_GAME_TIMER() - iControlTimer) > 500
		IF bFriendly
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR IS_THIS_PRINT_BEING_DISPLAYED(sText1)
					PRINT_NOW(sText2, DEFAULT_GOD_TEXT_TIME, 0)
					iControlTimer = GET_GAME_TIMER()
					bFriendly = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
			RETURN TRUE
		ELSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR IS_THIS_PRINT_BEING_DISPLAYED(sText2)
					PRINT_NOW(sText1, DEFAULT_GOD_TEXT_TIME, 0)
					iControlTimer = GET_GAME_TIMER()
					bFriendly = TRUE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL flashHelpTextForKillDecision(STRING sText1, STRING sText2, INT &iControlTimer, BOOL &bFriendly)
	IF (GET_GAME_TIMER() - iControlTimer) > 500
		IF bFriendly
			IF NOT IS_HELP_MESSAGE_ON_SCREEN()
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sText2)
				PRINT_HELP(sText1)
				bFriendly = FALSE
				iControlTimer = GET_GAME_TIMER()
				RETURN TRUE
			ENDIF
		ELSE
			IF NOT IS_HELP_MESSAGE_ON_SCREEN()
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sText1)
				PRINT_HELP(sText2)
				bFriendly = TRUE
				iControlTimer = GET_GAME_TIMER()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Last two peds flee the trailer park. Kill them / delete them if they get far enough away
PROC MANAGE_FLEEING_PEDS()
//	SEQUENCE_INDEX seq
	IF NOT gangFlee[0].bTriggered
		IF NOT IS_PED_INJURED(gangFlee[0].ped)
			RESET_GANG_ATTACK(gangFlee[0])
			IF NOT IS_ENTITY_VISIBLE(gangFlee[0].ped)
				SET_ENTITY_VISIBLE(gangFlee[0].ped,TRUE)
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangFlee[0].ped, TRUE)
			SET_RELEASED_DRIVERS_TO_EXIT_VEHICLES_ON_FLEE(TRUE)
			TASK_SMART_FLEE_PED(gangFlee[0].ped, PLAYER_PED_ID(), 150.0, -1)
			gangFlee[0].bTriggered = TRUE
		//	SCRIPT_ASSERT("Hit")
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(gangFlee[0].ped)
			SET_PED_MAX_MOVE_BLEND_RATIO(gangFlee[0].ped,2.0)
		ENDIF
		IF DOES_ENTITY_EXIST(gangFlee[0].ped)
			IF IS_PED_INJURED(gangFlee[0].ped)
				IF DOES_BLIP_EXIST(gangFlee[0].blipPed)
					REMOVE_BLIP(gangFlee[0].blipPed)
				ENDIF
			ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), gangFlee[0].ped) > 150
				REMOVE_BLIP(gangFlee[0].blipPed)
				IF IS_ENTITY_A_MISSION_ENTITY(gangFlee[0].ped)
					DELETE_PED(gangFlee[0].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT gangFlee[1].bTriggered
		IF NOT IS_PED_INJURED(gangFlee[1].ped)
			RESET_GANG_ATTACK(gangFlee[1])
			IF NOT IS_ENTITY_VISIBLE(gangFlee[1].ped)
				SET_ENTITY_VISIBLE(gangFlee[1].ped,TRUE)
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangFlee[1].ped, TRUE)
			SET_RELEASED_DRIVERS_TO_EXIT_VEHICLES_ON_FLEE(TRUE)
			TASK_SMART_FLEE_PED(gangFlee[1].ped, PLAYER_PED_ID(), 150.0, -1)
			gangFlee[1].bTriggered = TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(gangFlee[1].ped)
			SET_PED_MAX_MOVE_BLEND_RATIO(gangFlee[1].ped,2.0)
		ENDIF
		IF DOES_ENTITY_EXIST(gangFlee[1].ped)
			IF IS_PED_INJURED(gangFlee[1].ped)
				IF DOES_BLIP_EXIST(gangFlee[1].blipPed)
					REMOVE_BLIP(gangFlee[1].blipPed)
				ENDIF
			ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), gangFlee[1].ped) > 150
				REMOVE_BLIP(gangFlee[1].blipPed)
				IF IS_ENTITY_A_MISSION_ENTITY(gangFlee[1].ped)
					DELETE_PED(gangFlee[1].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT gangFlee[2].bTriggered
		IF NOT IS_PED_INJURED(gangFlee[2].ped)
			RESET_GANG_ATTACK(gangFlee[2])
			IF NOT IS_ENTITY_VISIBLE(gangFlee[2].ped)
				SET_ENTITY_VISIBLE(gangFlee[2].ped,TRUE)
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangFlee[2].ped, TRUE)
			SET_RELEASED_DRIVERS_TO_EXIT_VEHICLES_ON_FLEE(TRUE)
			TASK_SMART_FLEE_PED(gangFlee[2].ped, PLAYER_PED_ID(), 150.0, -1)
			gangFlee[2].bTriggered = TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(gangFlee[2].ped)
			SET_PED_MAX_MOVE_BLEND_RATIO(gangFlee[2].ped,2.0)
		ENDIF
		IF DOES_ENTITY_EXIST(gangFlee[2].ped)
			IF IS_PED_INJURED(gangFlee[2].ped)
				IF DOES_BLIP_EXIST(gangFlee[2].blipPed)
					REMOVE_BLIP(gangFlee[2].blipPed)
				ENDIF
			ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), gangFlee[2].ped) > 150
				REMOVE_BLIP(gangFlee[2].blipPed)
				IF IS_ENTITY_A_MISSION_ENTITY(gangFlee[2].ped)
					DELETE_PED(gangFlee[2].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT gangFlee[3].bTriggered
		IF NOT IS_PED_INJURED(gangFlee[3].ped)
			RESET_GANG_ATTACK(gangFlee[3])
			IF NOT IS_ENTITY_VISIBLE(gangFlee[3].ped)
				SET_ENTITY_VISIBLE(gangFlee[3].ped,TRUE)
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangFlee[3].ped, TRUE)
			SET_RELEASED_DRIVERS_TO_EXIT_VEHICLES_ON_FLEE(TRUE)
			TASK_SMART_FLEE_PED(gangFlee[3].ped, PLAYER_PED_ID(), 150.0, -1)
			gangFlee[3].bTriggered = TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(gangFlee[3].ped)
			SET_PED_MAX_MOVE_BLEND_RATIO(gangFlee[3].ped,2.0)
		ENDIF
		IF DOES_ENTITY_EXIST(gangFlee[3].ped)
			IF IS_PED_INJURED(gangFlee[3].ped)
				IF DOES_BLIP_EXIST(gangFlee[3].blipPed)
					REMOVE_BLIP(gangFlee[3].blipPed)
				ENDIF
			ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), gangFlee[3].ped) > 150
				REMOVE_BLIP(gangFlee[3].blipPed)
				IF IS_ENTITY_A_MISSION_ENTITY(gangFlee[3].ped)
					DELETE_PED(gangFlee[3].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT gangFlee[4].bTriggered
		IF NOT IS_PED_INJURED(gangFlee[4].ped)
			RESET_GANG_ATTACK(gangFlee[4])
			IF NOT IS_ENTITY_VISIBLE(gangFlee[4].ped)
				SET_ENTITY_VISIBLE(gangFlee[4].ped,TRUE)
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangFlee[4].ped, TRUE)
			SET_RELEASED_DRIVERS_TO_EXIT_VEHICLES_ON_FLEE(TRUE)
			TASK_SMART_FLEE_PED(gangFlee[4].ped, PLAYER_PED_ID(), 150.0, -1)
			gangFlee[4].bTriggered = TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(gangFlee[4].ped)
			SET_PED_MAX_MOVE_BLEND_RATIO(gangFlee[4].ped,2.0)
		ENDIF
		IF DOES_ENTITY_EXIST(gangFlee[4].ped)
			IF IS_PED_INJURED(gangFlee[4].ped)
				IF DOES_BLIP_EXIST(gangFlee[4].blipPed)
					REMOVE_BLIP(gangFlee[4].blipPed)
				ENDIF
			ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), gangFlee[4].ped) > 150
				REMOVE_BLIP(gangFlee[4].blipPed)
				IF IS_ENTITY_A_MISSION_ENTITY(gangFlee[4].ped)
					DELETE_PED(gangFlee[4].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF gangFlee[0].bFlee = FALSE
		IF NOT IS_PED_INJURED(gangFlee[0].ped)
			PLAY_PED_AMBIENT_SPEECH(gangFlee[0].ped,"FALL_BACK",SPEECH_PARAMS_INTERRUPT_SHOUTED)
			gangFlee[0].iPedTimer = GET_GAME_TIMER()
			gangFlee[0].bFlee = TRUE
		ENDIF
	ELSE
		IF MANAGE_MY_TIMER(gangFlee[0].iPedTimer,GET_RANDOM_INT_IN_RANGE(6000,14000))
			gangFlee[0].bFlee = FALSE
		ENDIF
	ENDIF
	
	IF gangFlee[1].bFlee = FALSE
		IF NOT IS_PED_INJURED(gangFlee[1].ped)
			PLAY_PED_AMBIENT_SPEECH(gangFlee[1].ped,"FALL_BACK",SPEECH_PARAMS_INTERRUPT_SHOUTED)
			gangFlee[1].iPedTimer = GET_GAME_TIMER()
			SET_LABEL_AS_TRIGGERED("GANG 1 FALL_BACK", TRUE)
			gangFlee[1].bFlee = TRUE
		ENDIF
	ELSE
		IF MANAGE_MY_TIMER(gangFlee[1].iPedTimer,GET_RANDOM_INT_IN_RANGE(6000,14000))
			gangFlee[1].bFlee = FALSE
		ENDIF
	ENDIF
	
	IF gangFlee[2].bFlee = FALSE
		IF NOT IS_PED_INJURED(gangFlee[2].ped)
			PLAY_PED_AMBIENT_SPEECH(gangFlee[2].ped,"FALL_BACK",SPEECH_PARAMS_INTERRUPT_SHOUTED)
			gangFlee[2].iPedTimer = GET_GAME_TIMER()
			SET_LABEL_AS_TRIGGERED("GANG 2 FALL_BACK", TRUE)
			gangFlee[2].bFlee = TRUE
		ENDIF
	ELSE
		IF MANAGE_MY_TIMER(gangFlee[2].iPedTimer,GET_RANDOM_INT_IN_RANGE(6000,14000))
			gangFlee[2].bFlee = FALSE
		ENDIF
	ENDIF
	
	IF gangFlee[3].bFlee = FALSE
		IF NOT IS_PED_INJURED(gangFlee[3].ped)
			PLAY_PED_AMBIENT_SPEECH(gangFlee[3].ped,"FALL_BACK",SPEECH_PARAMS_INTERRUPT_SHOUTED)
			gangFlee[3].iPedTimer = GET_GAME_TIMER()
			SET_LABEL_AS_TRIGGERED("GANG 3 FALL_BACK", TRUE)
			gangFlee[3].bFlee = TRUE
		ENDIF
	ELSE
		IF MANAGE_MY_TIMER(gangFlee[3].iPedTimer,GET_RANDOM_INT_IN_RANGE(6000,14000))
			gangFlee[3].bFlee = FALSE
		ENDIF
	ENDIF
	
	INT iStatCheck
	REPEAT MAX_NUMBER_FLEEING iStatCheck
		IF DOES_ENTITY_EXIST(gangFlee[iStatCheck].ped)
		AND NOT DOES_ENTITY_EXIST(statPeds[iStatCheck])
			statPeds[iStatCheck] = gangFlee[iStatCheck].ped
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(statPeds[0])
	AND DOES_ENTITY_EXIST(statPeds[1])
	AND DOES_ENTITY_EXIST(statPeds[2])
	AND DOES_ENTITY_EXIST(statPeds[3])
		IF IS_PED_INJURED(statPeds[0])
		AND IS_PED_INJURED(statPeds[1])
		AND IS_PED_INJURED(statPeds[2])
		AND IS_PED_INJURED(statPeds[3])
			PRINTSTRING("@@NO SURVIVORS")PRINTNL()
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TRV1_NO_SURVIVORS)
		ENDIF
	ENDIF

	IF NOT DOES_BLIP_EXIST(gangFlee[0].blipPed)
	AND NOT DOES_BLIP_EXIST(gangFlee[1].blipPed)
	AND NOT DOES_BLIP_EXIST(gangFlee[2].blipPed)
	AND NOT DOES_BLIP_EXIST(gangFlee[3].blipPed)
	AND NOT DOES_BLIP_EXIST(gangFlee[4].blipPed)
		
	ENDIF

ENDPROC

//PURPOSE: The enemy ped "pedReinf" will take up the position of ped" pedToReinf" if "pedToReinf" is killed
PROC SET_PED_REINF_PED( GANG_MOVE_ATTACK_T& pedReinf, GANG_MOVE_ATTACK_T& pedToReinf, BOOL bReinfShootWhileGoTo = TRUE)
	GANG_MOVE_ATTACK_T temp_gang[2]
	IF NOT pedReinf.bMovedToNewPos
		IF NOT ARE_VECTORS_EQUAL (pedToReinf.vGotoCoords, pedReinf.vGotoCoords)
			temp_gang[0] = pedToReinf
			IF HAVE_THESE_PEDS_BEEN_KILLED(temp_gang, 1, 1)
				RESET_GANG_ATTACK(pedReinf)
				IF bReinfShootWhileGoTo
					pedReinf.moveAttackStyle = MOVE_SHOOT_WHILE_GOTO
				ELSE
					pedReinf.moveAttackStyle = MOVE_AIM_WHILE_GOTO
				ENDIF
				pedReinf.gang_da = pedToReinf.gang_da
				pedReinf.vGotoCoords = pedToReinf.vGotoCoords
				pedReinf.bMovedToNewPos = TRUE
				pedReinf.bTriggered = TRUE
				pedReinf.gangCombatMovement = CM_DEFENSIVE
			ENDIF
		ENDIF
	ENDIF		
ENDPROC

PROC SET_GANG_MEMBER_ATTACKS_AT_CURRENT_LOC(INT index, DEFENSIVE_T defensiveArea = DA_AT_GO_TO, COMBAT_MOVEMENT cmGang = CM_DEFENSIVE, BOOL bSetAsTriggered = TRUE)
	IF NOT gangEnemy[index].bTriggered
		IF NOT IS_PED_INJURED(gangEnemy[index].ped)
			gangEnemy[index].vGotoCoords = GET_ENTITY_COORDS(gangEnemy[index].ped, FALSE)
			gangEnemy[index].gang_da = defensiveArea
			gangEnemy[index].gangCombatMovement = cmGang
			gangEnemy[index].bTriggered = bSetAsTriggered
			gangEnemy[index].moveAttackStyle = MOVE_FOLLOW_NAVMESH
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: The idea is that when you are one part of the trailer park, you'll be able to see bad guys running between locations in the distance, 
// to make it look busy. Each runner belongs to a group. Each group corresponds to a quadrant (roughly) of the trailer park
FUNC INT GET_NUMBER_OF_RUNNERS_STILL_REMAINING_IN_PED_GROUP(PED_T peds)
	INT iNumLeft = 0
	SWITCH peds
		CASE PED_RUNNER_LHS1
			#IF IS_DEBUG_BUILD
				DW_PRINTS("GET_NUMBER_OF_RUNNERS_STILL_REMAINING_IN_PED_GROUP...PED_RUNNER_LHS1")
			#ENDIF
			IF NOT IS_PED_INJURED(gangRunner[0].ped)
				iNumLeft++
			ENDIF
			IF NOT IS_PED_INJURED(gangRunner[5].ped)
				iNumLeft++
			ENDIF
		BREAK
		
		CASE PED_RUNNER_LHS2
			#IF IS_DEBUG_BUILD
				DW_PRINTS("GET_NUMBER_OF_RUNNERS_STILL_REMAINING_IN_PED_GROUP...PED_RUNNER_LHS2")
			#ENDIF
			IF NOT IS_PED_INJURED(gangRunner[6].ped)
				iNumLeft++
			ENDIF
			IF NOT IS_PED_INJURED(gangRunner[7].ped)
				iNumLeft++
			ENDIF
		BREAK
		
		CASE PED_RUNNER_RHS_CLOSE1
			#IF IS_DEBUG_BUILD
				DW_PRINTS("GET_NUMBER_OF_RUNNERS_STILL_REMAINING_IN_PED_GROUP...PED_RUNNER_RHS_CLOSE1")
			#ENDIF
			IF NOT IS_PED_INJURED(gangRunner[1].ped)
				iNumLeft++
			ENDIF
			IF NOT IS_PED_INJURED(gangRunner[2].ped)
				iNumLeft++
			ENDIF
			IF NOT IS_PED_INJURED(gangRunner[3].ped)
				iNumLeft++
			ENDIF
			IF NOT IS_PED_INJURED(gangRunner[4].ped)
				iNumLeft++
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		DW_PRINTI(iNumLeft, "this many left...")
	#ENDIF
	RETURN iNumLeft 
ENDFUNC

//---------------------------------¦ LOADING ¦-----------------------------------

//---------¦ CONVERSATION ¦---------

//PURPOSE: checks if it is ok to play dialog.
FUNC BOOL IS_IT_SAFE_TO_PLAY_DIALOG() 
	IF NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED() //AND NOT IS_SPEECH_CURRENTLY_PLAYING(myScriptedSpeech) AND NOT IS_SPEECH_PAUSED(myScriptedSpeech)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//---------¦ MESSAGES ¦---------
//PURPOSE: Counts the number of people in all waves
FUNC INT ENEMYS_LEFT()	
	RETURN (iNumberOfPedsAliveTrailer + iNumberOfPedsAliveEast + iNumberOfPedsAliveWest)
ENDFUNC

//PURPOSE: resets the mission stage ints
PROC RESET_MISSION_STAGE_INTS()
	DUMMY_REFERENCE_INT(iEnemyWestWave)
	DUMMY_REFERENCE_INT(iEnemyEastWave)
	DUMMY_REFERENCE_INT(iControlChaseSetP)
	DUMMY_REFERENCE_FLOAT(fPlayBackSpeed)
	DUMMY_REFERENCE_INT(iAttackSide)
	iStageSection		= 0 
	iPreLoadStage		= 0
	iBuddyProgress		= 0
	iWadeProgress       = 0
	iEnemyCreationProc	= 0
	iEnemyAIProg 		= 0
	iEnemyEastWave 		= 0
	iEnemyWestWave 		= 0
	//iLureGuy = 0
	//iCentreGuy = 0
	iRage = 0
	iWhore = 0
	iVehicleBlowUpSetPiece = 0
	iVehicleBlowUpSetPiece = 0
	bSetUpStageData = FALSE
	//iChaseProgress		= 0
	iControlChaseSetP	= 0
	//bMakeCam[0]		 	= FALSE
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	#IF IS_DEBUG_BUILD
		iProgressDB 			= 0
		iPreLoadStageDB 		= 0
		iBuddyProgressDB 		= 0
		iEnemyCreationProcDB 	= 0
		iEnemyAIProgDB 			= 0
		iMissionStageDB	 		= 0
		iEnemyEastWaveDB 			= 0
		iEnemyWestWaveDB 			= 0
	#ENDIF	
ENDPROC

//PURPOSE: resets the mission stage ints if you skipp
PROC RESET_MISSION_STAGE_INTS_SKIP()
	RESET_MISSION_STAGE_INTS()
	//reset the eneemy enter ints.
	FOR iCounter = 0 TO  (MAX_ENEMY_EAST_WAVE_PED-1)
		cmbtEnemyEastWave[iCounter].iEnemyProgress = 0
	ENDFOR
	FOR iCounter = 0 TO  (MAX_ENEMY_WEST_WAVE_PED-1)
		cmbtEnemyWestWave[iCounter].iEnemyProgress = 0
	ENDFOR
	bBrakeing		= FALSE
	bSetupCaravan = FALSE
	bSetUpStageData = FALSE
	#IF IS_DEBUG_BUILD
		//bskipping 	= FALSE
	#ENDIF
ENDPROC

//PURPOSE: Initilises the Mission Stage Start and End Vectors. 
PROC SET_START_AND_END_VECTORS()
	IF(eMissionStage   = STAGE_INIT_MISSION
	OR eMissionStage   = STAGE_GET_TO_CHASE)
		vStageStart    = << 1981.1176, 3817.1990, 31.3807 >> 
		vStageEnd	   = << 2016.4196, 4642.4399, 40.1808 >> 
		fStartHeading  = 323.8188
	ELIF eMissionStage = STAGE_CUTSCENE_CHASE_START
		vStageStart    = << 2016.4196, 4642.4399, 40.1808 >>
		vStageEnd	   = << 1969.6027, 4647.3115, 39.9437 >>
		fStartHeading  = 143.064
	ELIF eMissionStage = STAGE_CHASE
		vStageStart    = << 1969.6027, 4647.3115, 39.9437 >>
		vStageEnd	   = << 80.2901, 3619.4456, 38.6997 >> 
		fStartHeading  = 118.0796
	ELIF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
		vStageStart    = << 80.2901, 3619.4456, 38.6997 >>
		vStageEnd 	   = << 80.2901, 3619.4456, 38.6997 >>
		fStartHeading  = 16.2939
	ELIF eMissionStage = STAGE_DRIVE_TO_CARAVAN
		vStageStart    = << 64.4564, 3073.8577, 40.2777 >> 
		vStageEnd 	   = << 80.2901, 3619.4456, 38.6997 >>
		fStartHeading  =  132.7970 
	ELIF eMissionStage = STAGE_CARAVAN_SMASH
		vStageStart    = << 81.2743, 3609.2981, 38.7300 >> 
		vStageEnd 	   = << 81.2743, 3609.2981, 38.7300 >>
		fStartHeading  =  180.4163
	ELIF eMissionStage = STAGE_KILL_ORTEGA
		vStageStart    = << -43.9504, 3022.5447, 39.5891 >>
		vStageEnd 	   = << -43.9504, 3022.5447, 39.5891 >>
		fStartHeading  = 115.181
	ELIF eMissionStage = STAGE_DROP_OFF_RON
		vStageStart    = << -6.8119, 3039.8992, 39.6770 >> 
		vStageEnd 	   = << 1681.5554, 3725.4580, 32.9490 >>
		fStartHeading  = 287.4757 
	ELIF eMissionStage = STAGE_CUTSCENE_DROP_OFF_RON
		vStageStart    = <<1952.2297, 3787.3960, 31.2922>>
		vStageEnd 	   = <<1952.2297, 3787.3960, 31.2922>>
		fStartHeading  = 27.2205
	ENDIF	
ENDPROC

PROC setupEnemyGroup(	sTrailerPedGroup &thisGroup, STRING sName, VECTOR vCentre, VECTOR vEastTriggerLoc, VECTOR vEastTriggerDim,
						VECTOR vWestTriggerLoc, VECTOR vWestTriggerDim)
	thisGroup.name = sName
	thisGroup.vCentre = vCentre
	thisGroup.vEastTriggerLoc = vEastTriggerLoc
	thisGroup.vEastTriggerDim = vEastTriggerDim
	thisGroup.vWestTriggerLoc = vWestTriggerLoc
	thisGroup.vWestTriggerDim = vWestTriggerDim
	
ENDPROC

PROC UNREF()

IF sGroup[0].triggered = FALSE

ENDIF

IF sGroup[0].triggeredEast = FALSE

ENDIF

IF sGroup[0].triggeredWest = FALSE

ENDIF

IF sGroup[0].dialoguePlayed = FALSE

ENDIF

IF sGroup[0].cleared = FALSE

ENDIF

IF ARE_VECTORS_EQUAL(sGroup[0].vCentre)

ENDIF


IF ARE_VECTORS_EQUAL(sGroup[0].vEastTriggerLoc)

ENDIF

IF ARE_VECTORS_EQUAL(sGroup[0].VEASTTRIGGERDIM)

ENDIF

IF ARE_VECTORS_EQUAL(sGroup[0].VWESTTRIGGERLOC)

ENDIF

IF ARE_VECTORS_EQUAL(sGroup[0].vWestTriggerDim)

ENDIF


IF cmbtChase[0].wpn = WEAPONTYPE_ASSAULTRIFLE

ENDIF

IF cmbtChase[0].wpn2 = WEAPONTYPE_ASSAULTRIFLE

ENDIF

IF cmbtChase[0].ITIMESPENT = 0

ENDIF

IF cmbtChase[0].ITIMELASTFRAME = 0

ENDIF

IF cmbtChase[0].iHealthBeforeRage = 0

ENDIF
	
IF cmbtChase[0].fUnBlockRange < 0.0

ENDIF

IF cmbtChase[0].FDEAD < 0.0

ENDIF

IF cmbtChase[0].fmMove[0] <> FIGHTMOVE_NONE
		
ENDIF

REQUEST_ANIM_DICT(cmbtChase[0].animDict)
IF HAS_ANIM_DICT_LOADED(cmbtChase[0].animDict)

ENDIF

REQUEST_ANIM_DICT(cmbtChase[0].ANIMNAME)
IF HAS_ANIM_DICT_LOADED(cmbtChase[0].ANIMNAME)

ENDIF

IF cmbtChase[0].eastTriggered

ENDIF

IF ARE_VECTORS_EQUAL(cmbtChase[0].eastCovLoc)

ENDIF

IF cmbtChase[0].eastCovHead < 0.0

ENDIF

IF cmbtChase[0].eastCovUse = COVUSE_WALLTOBOTH

ENDIF

IF cmbtChase[0].eastCovHeight = COVHEIGHT_HIGH

ENDIF

IF ARE_VECTORS_EQUAL(cmbtChase[0].eastMoveLoc)

ENDIF


IF cmbtChase[0].westTriggered

ENDIF

IF ARE_VECTORS_EQUAL(cmbtChase[0].westCovLoc)

ENDIF

IF cmbtChase[0].westCovHead < 0.0

ENDIF

IF cmbtChase[0].westCovUse = COVUSE_WALLTOBOTH

ENDIF

IF cmbtChase[0].westCovHeight = COVHEIGHT_HIGH

ENDIF

IF ARE_VECTORS_EQUAL(cmbtChase[0].westMoveLoc)

ENDIF

IF cmbtChase[0].reactToProximity = FALSE

ENDIF

IF cmbtChase[0].enemyGroup = 0

ENDIF

ENDPROC


//PURPOSE: Sets the enemies varaibles
PROC setupEnemy(COMBAT_PED_STRUCT &thisEnemy, VECTOR vStartLoc, FLOAT fStartHead, WEAPON_TYPE weapontype, MODEL_NAMES modelname, 
				COMBAT_MOVEMENT move, COMBAT_RANGE range, COMBAT_ABILITY_LEVEL ability,
				FIGHT_MOVES move0, FIGHT_MOVES move1, FIGHT_MOVES move2, FIGHT_MOVES move3, FIGHT_MOVES move4,
				STRING animDict, STRING animName,
				VECTOR vEastMoveLoc, VECTOR vEastCoverLoc, FLOAT fEastCoverHead,
				COVERPOINT_USAGE cuEastCovUse, COVERPOINT_HEIGHT chEastCovHeight, COVERPOINT_ARC caEastCovArc,
				VECTOR vWestMoveLoc, VECTOR vWestCoverLoc, FLOAT fWestCoverHead,
				COVERPOINT_USAGE cuWestCovUse, COVERPOINT_HEIGHT chWestCovHeight, COVERPOINT_ARC caWestCovArc,
				BOOL bWestSameAsEast, BOOL bReactToProximity, GROUP_NAMES thisGroup)
	thisEnemy.startLoc =			vStartLoc
	thisEnemy.startHead = 			fStartHead
	thisEnemy.wpn = 				weapontype
	thisEnemy.model =				modelname
	thisEnemy.move = 				move
	thisEnemy.range	= 				range
	thisEnemy.ability = 			ability
	thisEnemy.fmMove[0] =			move0
	thisEnemy.fmMove[1] =			move1
	thisEnemy.fmMove[2] =			move2
	thisEnemy.fmMove[3] =			move3
	thisEnemy.fmMove[4] =			move4
	thisEnemy.animDict =			animDict
	thisEnemy.animName =			animName
	thisEnemy.eastMoveLoc =			vEastMoveLoc
	thisEnemy.eastCovLoc = 			vEastCoverLoc
	thisEnemy.eastCovHead =			fEastCoverHead
	thisEnemy.eastCovUse = 			cuEastCovUse
	thisEnemy.eastCovHeight = 		chEastCovHeight
	thisEnemy.eastCovArc = 			caEastCovArc
	thisEnemy.westSameAsEast =		bWestSameAsEast
	IF NOT bWestSameAsEast
		thisEnemy.westMoveLoc =		vWestMoveLoc
		thisEnemy.westCovLoc = 		vWestCoverLoc
		thisEnemy.westCovHead =		fWestCoverHead
		thisEnemy.westCovUse = 		cuWestCovUse
		thisEnemy.westCovHeight = 	chWestCovHeight
		thisEnemy.westCovArc = 		caWestCovArc
	ENDIF
	thisEnemy.reactToProximity =	bReactToProximity
	thisEnemy.enemyGroup = ENUM_TO_INT(thisGroup)
	
	thisEnemy.covExist =	FALSE
	thisEnemy.eastTriggered = FALSE
	thisEnemy.westTriggered = FALSE
	thisEnemy.fmMove[5] = 	FIGHTMOVE_NONE
ENDPROC

PROC setupEnemyValues(MISSION_STAGE eMissionStagePassed)
	IF (eMissionStagePassed = STAGE_INIT_MISSION
	OR eMissionStagePassed = STAGE_GET_TO_CHASE)
	OR eMissionStagePassed = STAGE_CHASE
	OR eMissionStagePassed = STAGE_CARAVAN_PARK_FIGHT
		//Chase enemies (only created if skipped in caravan fight)
		//Van1
		setupEnemy(	cmbtChase[0],
					<< 1952.8842, 4634.7134, 39.5990 >>, 49.8737, WEAPONTYPE_PISTOL, Asset.mnLost1, 
					CM_DEFENSIVE, CR_FAR, CAL_AVERAGE,
					FIGHTMOVE_RUN_AT_PLAYER, FIGHTMOVE_STAND_AND_SHOOT, FIGHTMOVE_NONE, FIGHTMOVE_NONE, FIGHTMOVE_NONE,
					Asset.adNoDict, Asset.animNoAnim,
					/*EAST*//*moveloc*/<< 0,0,0 >>, /*coverloc*/<< 0,0,0 >>, /*coverhead*/ 0.0,
					COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_180,
					/*WEST*//*moveloc*/<< 0,0,0 >>, /*coverloc*/<< 0,0,0 >>, /*coverhead*/0.0,
					COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_0TO45,
					/*WestSameAsEast?*/TRUE, TRUE, GROUP_NONE)
									
		setupEnemy(	cmbtChase[1],
					<< 1955.6431, 4637.1948, 39.6610 >>, 109.1915, WEAPONTYPE_PUMPSHOTGUN, Asset.mnLost1, 
					CM_DEFENSIVE, CR_FAR, CAL_AVERAGE,
					FIGHTMOVE_RUN_AT_PLAYER_WHILE_AIMING, FIGHTMOVE_STAND_AND_SHOOT, FIGHTMOVE_NONE, FIGHTMOVE_NONE, FIGHTMOVE_NONE,
					Asset.adNoDict, Asset.animNoAnim,
					/*EAST*//*moveloc*/<< 0,0,0 >>, /*coverloc*/<< 0,0,0 >>, /*coverhead*/0.0,
					COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_0TO45,
					/*WEST*//*moveloc*/<< 0,0,0 >>, /*coverloc*/<< 0,0,0 >>, /*coverhead*/0.0,
					COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_0TO45,
					/*WestSameAsEast?*/TRUE, TRUE, GROUP_NONE)
		
		//Bike1
		setupEnemy(	cmbtChase[2],
					<< 1957.1071, 4636.9858, 39.6963 >>, 112.9889, WEAPONTYPE_SMG, Asset.mnLost2, 
					CM_DEFENSIVE, CR_FAR, CAL_AVERAGE,
					FIGHTMOVE_HEATWALK_AT_PLAYER, FIGHTMOVE_STAND_AND_SHOOT, FIGHTMOVE_NONE, FIGHTMOVE_NONE, FIGHTMOVE_NONE,
					Asset.adNoDict, Asset.animNoAnim,
					/*EAST*//*moveloc*/<< 0,0,0 >>, /*coverloc*/<< 0,0,0 >>, /*coverhead*/0.0,
					COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_0TO45,
					/*WEST*//*moveloc*/<< 0,0,0 >>, /*coverloc*/<< 0,0,0 >>, /*coverhead*/0.0,
					COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_0TO45,
					/*WestSameAsEast?*/TRUE, TRUE, GROUP_NONE)
		
		//Bike2
		setupEnemy(	cmbtChase[3],
					<< 1956.0724, 4633.9956, 39.6426 >>, 90.1262, WEAPONTYPE_CARBINERIFLE, Asset.mnLost2, 
					CM_DEFENSIVE, CR_FAR, CAL_AVERAGE,
					FIGHTMOVE_RUN_AT_PLAYER_WHILE_AIMING, FIGHTMOVE_STAND_AND_SHOOT, FIGHTMOVE_NONE, FIGHTMOVE_NONE, FIGHTMOVE_NONE,
					Asset.adNoDict, Asset.animNoAnim,
					/*EAST*//*moveloc*/<< 0,0,0 >>, /*coverloc*/<< 0,0,0 >>, /*coverhead*/0.0,
					COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_0TO45,
					/*WEST*//*moveloc*/<< 0,0,0 >>, /*coverloc*/<< 0,0,0 >>, /*coverhead*/0.0,
					COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_0TO45,
					/*WestSameAsEast?*/TRUE, TRUE, GROUP_NONE)
	ENDIF
	
	IF eMissionStagePassed = STAGE_CARAVAN_PARK_FIGHT
		//Trailer enemies

	ENDIF
ENDPROC

//PURPOSE: defines up the individual ped details
PROC INITALISE_ARRAYS(MISSION_STAGE eMissionStagePassed) // defines up the individual ped details

	setupEnemyValues(eMissionStagePassed) //Setup enemy values based on current stage

	IF (eMissionStagePassed = STAGE_INIT_MISSION
	OR eMissionStagePassed = STAGE_GET_TO_CHASE)
	OR eMissionStagePassed = STAGE_CHASE
		//buddies
		//Ron
		budRon.startLoc = 	<< 1983.88, 3828.55, 31.3972 >>
		budRon.startHead = 	28.7057
		budRon.wpn = 		WEAPONTYPE_PISTOL
		budRon.wpn2 = 	    WEAPONTYPE_UNARMED
		//Wade
		budWade.startLoc = 	<< 1981.66, 3831.05, 31.3972 >>
		budWade.startHead = 206.1794
		budWade.wpn = 		WEAPONTYPE_SAWNOFFSHOTGUN
		budWade.wpn2 = 	    WEAPONTYPE_UNARMED
		//vehicles
		vehTrevor.startLoc=<< 1983.17, 3829.76, 32.40 >>
		vehTrevor.startHead=294.44
		vehTrevor.recNumber= Asset.recNoTrevorChase
		vehTruck.startLoc=<<1948.87, 4638.04, 40.39>>
		vehTruck.startHead= 66.68

		vehTruck.model= Asset.mnGangVan//Asset.mnTruck
		vehTruck.recNumber= Asset.recNoTruckChase	
		vehBike[0].startLoc=<< 1962.8153, 4635.3926, 40.2410 >>
		vehBike[0].startHead=140.0220
		vehBike[0].model= Asset.mnBike
		vehBike[0].recNumber= Asset.recNoBikeChase0
		vehBike[1].startLoc=<< 1960.5868, 4632.4321, 40.1918 >>
		vehBike[1].startHead=58.5726
		vehBike[1].model= Asset.mnBike
		vehBike[1].recNumber= Asset.recNoBikeChase1
		
		cmbtChase[1].startLoc = <<1966.7565, 4634.7251, 40.7667>>
		cmbtChase[1].startHead = 23.7752
		
		cmbtChase[2].startLoc = <<1966.0510, 4638.4458, 40.7751>>
		cmbtChase[2].startHead = 153.9032
		cmbtChase[2].model = IG_TERRY
		
		cmbtChase[3].startLoc = <<1963.5370, 4636.0410, 40.8039>>
		cmbtChase[3].startHead = 32.1947
		cmbtChase[3].model = IG_CLAY
		
		cmbtChase[0].startLoc = <<1951.3340, 4636.0430, 40.6186>>
		cmbtChase[0].startHead = -105.8400

		vehBike[0].startLoc = <<1965.54, 4637.74, 40.25>>
		vehBike[0].startHead = 92.63
		
		vehBike[1].startLoc = <<1963.45, 4636.52, 40.24>>
		vehBike[1].startHead = 59.72
		
		//For the set peice stuff	
		//In the large yard
		npcDriver[0].startLoc =		<< 360.3275, 4444.9155, 61.9223 >>
		npcDriver[0].startHead = 	126.5533
		npcDriver[0].model =	Asset.mnMechanic
		npcDriver[1].startLoc =		<< 358.3185, 4442.6445, 61.8962 >>
		npcDriver[1].startHead = 	20.1394
		npcDriver[1].model =	Asset.mnMechanic
		vehSetP[0].startLoc=	<< 355.3197, 4442.6392, 61.9571 >>
		vehSetP[0].startHead=	6.6736
		vehSetP[0].model= 	Asset.mnTractor
		vehSetP[1].startLoc=	<< 377.0306, 4405.7993, 61.3879 >>
		vehSetP[1].startHead=	31.7967
		vehSetP[1].model= 	Asset.mnTruck  //digger			
		
	ELIF eMissionStagePassed = STAGE_CARAVAN_PARK_FIGHT//(eMissionStagePassed = STAGE_CHASE		
	//ENEMY VEHICLES	
		//Around the park
		vehTrailerPark[0].startLoc = << 13.6181, 3702.1545, 38.6766 >>	
		vehTrailerPark[0].startHead =286.8863
		vehTrailerPark[0].model= Asset.mnGangVan
		vehTrailerPark[0].recNumber= Asset.recNoCampervan
		vehTrailerPark[1].startLoc = << 64.3812, 3665.0451, 38.7237 >>
		vehTrailerPark[1].startHead =289.1604  
		vehTrailerPark[1].model= Asset.mnGangVan
		vehTrailerPark[2].startLoc =  << 101.7281, 3742.2266, 38.7385 >> 
		vehTrailerPark[2].startHead = 126.7107
		vehTrailerPark[2].model= Asset.mnBike
		vehTrailerPark[3].startLoc = << 96.8450, 3687.7852, 38.6517 >>
		vehTrailerPark[3].startHead = 273.4825
		vehTrailerPark[3].model= Asset.mnGangVan
		//Place bikes and things in the trailer park
		vehTrailerPark[4].startLoc=<< 31.7653, 3660.3279, 38.7930 >>
		vehTrailerPark[4].startHead=281.1249
		vehTrailerPark[4].model= Asset.mnBike
		vehTrailerPark[5].startLoc=<< 12.51, 3686.04, 39.15 >>
		vehTrailerPark[5].startHead=299.13
		vehTrailerPark[5].model= Asset.mnBike
		vehTrailerPark[6].startLoc=<< 58.7611, 3724.3909, 38.7233 >>
		vehTrailerPark[6].startHead=52.6504
		vehTrailerPark[6].model= Asset.mnBike
		vehTrailerPark[7].startLoc = << 60.6066, 3724.9722, 38.7202 >>
		vehTrailerPark[7].startHead =50.1891
		vehTrailerPark[7].model= Asset.mnBike
		vehTrailerPark[8].startLoc = << 60.1503, 3665.4148, 38.7929 >>
		vehTrailerPark[8].startHead =82.5423
		vehTrailerPark[8].model= Asset.mnBike	
		vehTrailerPark[9].startLoc =<< 59.5704, 3663.1687, 38.6055 >>
		vehTrailerPark[9].startHead = 97.7429
		vehTrailerPark[9].model= Asset.mnBike
		vehTrailerPark[10].startLoc =<< 66.2237, 3625.1873, 38.6523 >>
		vehTrailerPark[10].startHead = 312.2219
		vehTrailerPark[10].model= Asset.mnGangTruck 
		vehTrailerPark[11].startLoc =<< 86.8213, 3637.7795, 38.7683 >>
		vehTrailerPark[11].startHead =   357.0385
		
		vehTrailerPark[11].model= Asset.mnGangTruck
		vehTrailerPark[12].startLoc =<< 34.3147, 3719.1348, 38.6821 >>
		vehTrailerPark[12].startHead = 153.6358
		//XXXX
		vehTrailerPark[12].model= Asset.mnGangTruck //BODHI
		vehTrailerPark[13].startLoc =<< 42.9015, 3683.5884, 38.6273 >>
		vehTrailerPark[13].startHead =38.8820 + 180.0
		vehTrailerPark[13].model= Asset.mnGangTruck
		vehTrailerPark[14].startLoc =<< 26.0817, 3681.3962, 38.6321 >>
		vehTrailerPark[14].startHead =114.3899  
		vehTrailerPark[14].model= Asset.mnGangVan   
		vehTrailerPark[15].startLoc = << 39.2248, 3728.5081, 39.1479 >>
		vehTrailerPark[15].startHead = 105.9004
		vehTrailerPark[15].model= Asset.mnBike
		vehTrailerPark[19].startLoc =  << 78.0597, 3684.4834, 38.5120 >>
		vehTrailerPark[16].startHead =105.9004
		vehTrailerPark[16].model= Asset.mnBike
		vehTrailerPark[17].startLoc = <<40.5676, 3730.7749, 39.1645 >>
		vehTrailerPark[17].startHead = 113.6521
		vehTrailerPark[17].model= Asset.mnBike
		vehTrailerPark[18].startLoc = << 125.9750, 3720.1838, 38.7542 >>
		vehTrailerPark[18].startHead = 282.1393 
		vehTrailerPark[18].model= Asset.mnGangVan
		vehTrailerPark[19].startLoc =  << 78.0597, 3684.4834, 38.5120 >>
		
		vehTrailerPark[19].startHead = 337.7484
		vehTrailerPark[19].model= Asset.mnGangTruck
		vehTrailerPark[20].startLoc =  << 103.8368, 3700.4424, 38.8312 >>
		vehTrailerPark[20].startHead = 113.2645
		vehTrailerPark[20].model= Asset.mnBike
		vehTrailerPark[21].startLoc =  <<106.9271, 3699.7463, 39.2054 >>
		vehTrailerPark[21].startHead = 115.34
		vehTrailerPark[21].model= Asset.mnBike
		vehTrailerPark[22].startLoc =  <<107.3253, 3704.3235, 39.2635 >> 
		vehTrailerPark[22].startHead = 99.51
		vehTrailerPark[22].model= Asset.mnBike
		vehTrailerPark[23].startLoc =  << 88.3355, 3704.1931, 38.5818 >>
		vehTrailerPark[23].startHead =  53.3843
		vehTrailerPark[23].model= Asset.mnGangTruck
		vehTrailerPark[24].startLoc =  << 75.7059, 3740.9680, 39.1979 >>
		vehTrailerPark[24].startHead =  229.0400
		vehTrailerPark[24].model= Asset.mnBike
		vehTrailerPark[25].startLoc =   <<71.3323, 3742.7761, 39.2591 >> 
		vehTrailerPark[25].startHead =  271.1641
		vehTrailerPark[25].model= Asset.mnBike
	 
		//only made if stage is skipped to
//		vehTruck.startLoc=<< 51.5098, 3663.9077, 38.6906 >> 
//		vehTruck.startHead=16.9637
//		vehTruck.startLoc=<< 51.74, 3662.95, 39.52 >> 
//		vehTruck.startHead= 16.84
		
		vehTruck.model= Asset.mnGangVan //Asset.mnTruck
		vehBike[0].startLoc= << 71.6588, 3648.1626, 38.5417 >>
		vehBike[0].startHead=240.6028
		vehBike[0].model= Asset.mnBike
		vehBike[1].startLoc=<< 74.1199, 3653.3162, 38.8485 >>
		vehBike[1].startHead= 249.5172
		vehBike[1].model= Asset.mnBike

	ELIF (eMissionStagePassed = STAGE_CARAVAN_SMASH
	OR eMissionStagePassed = STAGE_KILL_ORTEGA)
		vehTruck.startLoc=<< -20.1293, 3039.4690, 40.0023 >>
		vehTruck.startHead=292.0927
		vehTruck.model= Asset.mnTowTruck
		npcOrtega.startLoc =	 << -21.5156, 3036.3579, 40.2379 >>
		npcOrtega.startHead = 	287.3651
	ELIF eMissionStagePassed = STAGE_CARAVAN_PARK_FIGHT
	OR eMissionStagePassed = STAGE_KILL_ORTEGA
		vehTruck.startLoc=<< -20.1293, 3039.4690, 40.0023 >>
		vehTruck.startHead=292.0927
		vehTruck.model= Asset.mnTowTruck
		npcOrtega.startLoc =	 << -21.5156, 3036.3579, 40.2379 >>
		npcOrtega.startHead = 	287.3651
	ELIF eMissionStagePassed = STAGE_DRIVE_TO_CARAVAN
		vehTruck.startLoc=<< -20.1293, 3039.4690, 40.0023 >>
		vehTruck.startHead=292.0927
		vehTruck.model= Asset.mnTowTruck
		npcOrtega.startLoc =	 << -21.5156, 3036.3579, 40.2379 >>
		npcOrtega.startHead = 	287.3651
	ENDIF
ENDPROC

//PURPOSE: Sets up the uber array
PROC INITALISE_UBER_ARRAYS()
	
ENDPROC

PROC INIT_UBER_DATA()

	SetPieceCarPos[1] = <<362.3041, 4431.9624, 62.5045>>
	SetPieceCarQuatX[1] = 0
	SetPieceCarQuatY[1] = 0
	SetPieceCarQuatZ[1] = 0
	SetPieceCarQuatW[1] =  1
	SetPieceCarRecording[1] = 1
	SetPieceCarStartime[1] = 10087.1
	SetPieceCarRecordingSpeed[1] = 0.9
	SetPieceCarModel[1] = BULLDOZER
	
//	SetPieceCarPos[2] = <<1429.4023, 4440.5718, 49.3761>>
//	SetPieceCarQuatX[2] = 0.0614
//	SetPieceCarQuatY[2] = -0.0066
//	SetPieceCarQuatZ[2] = -0.1807
//	SetPieceCarQuatW[2] = 0.9816
//	SetPieceCarRecording[2] = 2
//	SetPieceCarStartime[2] = 20000.0000
//	SetPieceCarRecordingSpeed[2] = 1.0000
//	SetPieceCarModel[2] = BIFF
	
	SetPieceCarPos[2] = <<1429.4023, 4440.5718, 49.3761>>
	SetPieceCarQuatX[2] = 0.0614
	SetPieceCarQuatY[2] = -0.0066
	SetPieceCarQuatZ[2] = -0.1807
	SetPieceCarQuatW[2] = 0.9816
	SetPieceCarRecording[2] = 2
	SetPieceCarStartime[2] = 20000.0000
	SetPieceCarRecordingSpeed[2] = 1.1000
	SetPieceCarModel[2] = BISON2
	
	SetPieceCarPos[3] = <<-60.2279, 4374.5630, 53.8577>>
	SetPieceCarQuatX[3] = 0.0385
	SetPieceCarQuatY[3] = 0.0081
	SetPieceCarQuatZ[3] = -0.0796
	SetPieceCarQuatW[3] = 0.9960
	SetPieceCarRecording[3] = 3
	SetPieceCarStartime[3] = 115000.0000
	SetPieceCarRecordingSpeed[3] = 1.0000
	SetPieceCarModel[3] = EMPEROR2

	SetPieceCarPos[4] = <<-203.6284, 3797.0049, 39.4701>>
	SetPieceCarQuatX[4] = -0.0002
	SetPieceCarQuatY[4] = 0.0336
	SetPieceCarQuatZ[4] =  0.1906
	SetPieceCarQuatW[4] =  0.9811
	SetPieceCarRecording[4] = 4
	//SetPieceCarStartime[4] = 145000.0000
	SetPieceCarStartime[4] = 142000.0000
	SetPieceCarRecordingSpeed[4] = 0.8000
	SetPieceCarModel[4] = DLOADER
	
	SetPieceCarPos[5] = <<865.3905, 4273.8311, 50.9768>>
	SetPieceCarQuatX[5] = 0.0095
	SetPieceCarQuatY[5] = 0.0174
	SetPieceCarQuatZ[5] = 0.0895
	SetPieceCarQuatW[5] = 0.9958
	SetPieceCarRecording[5] = 5
	SetPieceCarStartime[5] = 50000.0000
	SetPieceCarRecordingSpeed[5] = 1.0000
	SetPieceCarModel[5] = SCRAP
	
	SetPieceCarPos[11] = <<-217.3212, 4159.6401, 41.9787>>
	SetPieceCarQuatX[11] = 0
	SetPieceCarQuatY[11] = 0
	SetPieceCarQuatZ[11] = 0
	SetPieceCarQuatW[11] = 1
	SetPieceCarRecording[11] = 11
	//SetPieceCarStartime[11] = 133573.828125
	SetPieceCarStartime[11] = 134573.828125
	SetPieceCarRecordingSpeed[11] = 1.0000
	//SetPieceCarRecordingSpeed[11] = 0.9000
	SetPieceCarModel[11] = TOWTRUCK2

ENDPROC


//PURPOSE: Sets values for variables that cannot be set during creation
PROC SetupArrays()					
	iStageSection									= 0
	
	#IF IS_DEBUG_BUILD
	SkipMenuStruct[ENUM_TO_INT(STAGE_INIT_MISSION)].sTxtLabel 				= "Initialise Mission"
	SkipMenuStruct[ENUM_TO_INT(STAGE_GET_TO_CHASE)].sTxtLabel 				= "Stage: Get to Chase |CUT: TRV_1_MCS_1_P1"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_CHASE_START)].sTxtLabel 		= "Stage: Chase start"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CHASE)].sTxtLabel 						= "Stage: Chase the bikers"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CARAVAN_PARK_FIGHT)].sTxtLabel 		= "Stage: Caravan Park Fight"
	SkipMenuStruct[ENUM_TO_INT(STAGE_DRIVE_TO_CARAVAN)].sTxtLabel 			= "Stage: Drive to trailer"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CARAVAN_SMASH)].sTxtLabel 				= "Stage: Push the trailer | CUT: TRV_1_MCS_3_CONCAT"
	SkipMenuStruct[ENUM_TO_INT(STAGE_KILL_ORTEGA)].sTxtLabel 			= "Stage: Kill / Threaten Ortega"
	SkipMenuStruct[ENUM_TO_INT(STAGE_DROP_OFF_RON)].sTxtLabel 				= "Stage: Drop Off Ron"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_DROP_OFF_RON)].sTxtLabel 		= "CUT: TRV_1_MCS_4"
	SkipMenuStruct[ENUM_TO_INT(PASSED_MISSION)].sTxtLabel 					= "Passed Mission"
	SkipMenuStruct[ENUM_TO_INT(PASSED_MISSION)].bSelectable                 = FALSE
	#ENDIF	
ENDPROC

//PURPOSE:		Requests resources to load before checkpoint script can start
PROC doCheckpointRequestLoad

ENDPROC
//PURPOSE:		Returns true if all requested resources have loaded before checkpoint script can start
FUNC BOOL hasCheckpointRequestLoaded
	RETURN TRUE
ENDFUNC



//PURPOSE:		Performs necessary actions for player completing the mission, then calls cleanup
PROC MissionPassed()	
	Mission_Flow_Mission_Passed()
	//Display generic pass text - "Mission Passed"
	//	QUEUE_GOD_TEXT_SP(Asset.godPass, 0, TRUE, DEFAULT_GOD_TEXT_TIME, TEXT_QUEUE_PRIORITY_NOW, TRUE)
	//PRINT_NOW(Asset.godPass, DEFAULT_GOD_TEXT_TIME, 0)
	CLEAR_PLAYER_WANTED_LEVEL( PLAYER_ID() )
	TRIGGER_MUSIC_EVENT("TRV1_FAIL")
	MissionCleanup(FALSE,TRUE)
ENDPROC


//---------------------------------¦ INITIALISING ¦-----------------------------------

//---------¦ REQUEST AND WAITS ¦---------
//PURPOSE: Adds all the models in the asset list to the model list
PROC CREATE_NEED_LIST()
	CLEAR_NEED_LIST()
	ADD_MODEL_TO_NEED_LIST(Asset.mnBike)
	ADD_MODEL_TO_NEED_LIST(Asset.mnTruck)
	ADD_MODEL_TO_NEED_LIST(Asset.mnTractor)
	ADD_MODEL_TO_NEED_LIST(Asset.mnGangVan)
	ADD_MODEL_TO_NEED_LIST(Asset.mnGangTruck)
	ADD_MODEL_TO_NEED_LIST(Asset.mnTowTruck)
	ADD_MODEL_TO_NEED_LIST(Asset.mnLost1)
	ADD_MODEL_TO_NEED_LIST(Asset.mnLost2)
						
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recNoTrevorChase)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recNoTruckChase)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recNoBikeChase0)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recNoBikeChase1)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recNoCampervan)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoEastWave0)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoEastWave1)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoEastWave2)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoEastWave3)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoEastWave4)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoEastWave5)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoWestWave0)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoWestWave1)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoWestWave2)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoWestWave3)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoWestWave4)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(sCarRec, Asset.recnoWestWave5)
	
ENDPROC

PROC SET_UP_BUDDY(BUDDY_PED_STRUCT buddy,VEHICLE_SEAT vsSeat = VS_FRONT_RIGHT, BOOL bSetPassengerIndex = FALSE)
	IF DOES_ENTITY_EXIST(buddy.ped)
		IF NOT IS_PED_INJURED(buddy.ped)
			GIVE_WEAPON_TO_PED(buddy.ped, buddy.wpn, INFINITE_AMMO, FALSE)
			GIVE_WEAPON_TO_PED(buddy.ped, buddy.wpn2, 2000, FALSE)
			SET_PED_CAN_BE_TARGETTED(buddy.ped, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(buddy.ped ,grpBuddies)
			SET_PED_CAN_BE_DRAGGED_OUT(buddy.ped,FALSE)
			//SET_PED_SUFFERS_CRITICAL_HITS(buddy.ped ,FALSE)
			IF bSetPassengerIndex
				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(buddy.ped, vsSeat)
			ENDIF
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(buddy.ped ,FALSE)
			SET_PED_AS_ENEMY(buddy.ped , FALSE)
			SET_ENTITY_AS_MISSION_ENTITY(buddy.ped)
			//SET_PED_DEFAULT_COMPONENT_VARIATION(buddy.ped)
			SET_ENTITY_HEALTH(buddy.ped, 200)
			SET_PED_CONFIG_FLAG(buddy.ped,PCF_WillFlyThroughWindscreen,FALSE)
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("SET UP BUDDY CALLED ON A PED WHO DOESN'T EXIST - BUG ADAM W")
		#ENDIF
	ENDIF
ENDPROC
PROC CREATE_BUDDIES(BUDDY_PED_STRUCT buddy,VEHICLE_SEAT vsSeat = VS_FRONT_RIGHT, BOOL bSetPassengerIndex = FALSE)
	REQUEST_MODEL(buddy.model)
	IF HAS_MODEL_LOADED(buddy.model)
		buddy.ped = CREATE_PED(PEDTYPE_MISSION, buddy.model, buddy.startLoc, buddy.startHead)
		//TURNING OFF EVENT BLOCKING
		//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(buddy.ped, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(buddy.ped, FALSE)
		GIVE_WEAPON_TO_PED(buddy.ped, buddy.wpn, 2000, TRUE)
		SET_ENTITY_PROOFS(buddy.ped, FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PED_CAN_BE_TARGETTED(buddy.ped, FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(buddy.ped ,grpBuddies)
		//SET_PED_SUFFERS_CRITICAL_HITS(buddy.ped ,FALSE)
		IF bSetPassengerIndex
			SET_PED_GROUP_MEMBER_PASSENGER_INDEX(buddy.ped, vsSeat)
		ENDIF
		SET_PED_AS_ENEMY(buddy.ped , FALSE)
		SET_ENTITY_AS_MISSION_ENTITY(buddy.ped)
		//SET_PED_DEFAULT_COMPONENT_VARIATION(buddy.ped)
		SET_PED_CONFIG_FLAG(buddy.ped,PCF_WillFlyThroughWindscreen,FALSE)
		PRINTSTRING("buddy spawned")
		PRINTNL()	
	ENDIF
ENDPROC
//PURPOSE: Sets Up a ped
PROC SETUP_ENEMY_AI_VALUES(COMBAT_PED_STRUCT &thisEnemy)
	IF NOT IS_PED_INJURED(thisEnemy.ped)
		SET_ENTITY_AS_MISSION_ENTITY(thisEnemy.ped)
		//SET_PED_COMBAT_MOVEMENT(thisEnemy.ped, thisEnemy.move)
		SET_PED_COMBAT_ABILITY(thisEnemy.ped, thisEnemy.ability)
		SET_PED_TO_INFORM_RESPECTED_FRIENDS(thisEnemy.ped, thisEnemy.infmR1ge, 20) 
		SET_PED_COMBAT_RANGE(thisEnemy.ped, thisEnemy.range)
		SET_PED_ACCURACY(thisEnemy.ped, thisEnemy.accuracy)
		SET_PED_HEARING_RANGE(thisEnemy.ped, 100.0)
		SET_PED_SEEING_RANGE(thisEnemy.ped, 100.0)
		thisEnemy.iTimeSpent = 0
		thisEnemy.iTimeLastFrame = GET_GAME_TIMER()
		thisEnemy.iEnemyProgress = 0
		IF thisEnemy.covExist
			REMOVE_COVER_POINT(thisEnemy.cov)
			thisEnemy.covExist = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Creates an enemy ped
PROC CREATE_ENEMY(COMBAT_PED_STRUCT &thisEnemy, BOOL setupAI)
	REQUEST_MODEL(thisEnemy.model)
	IF HAS_MODEL_LOADED(thisEnemy.model)
		thisEnemy.ped = CREATE_PED(PEDTYPE_MISSION, thisEnemy.model, thisEnemy.startLoc, thisEnemy.startHead, TRUE)
		ADD_DEADPOOL_TRIGGER(thisEnemy.ped, TRV1_KILLS)
		GIVE_WEAPON_TO_PED(thisEnemy.ped, thisEnemy.wpn, INFINITE_AMMO, TRUE)
		REMOVE_RELATIONSHIP_GROUP(grpEnemies)
		ADD_RELATIONSHIP_GROUP("ENEMIES", grpEnemies)
		SET_PED_RELATIONSHIP_GROUP_HASH(thisEnemy.ped, grpEnemies)			
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisEnemy.ped, TRUE)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(thisEnemy.ped, FALSE)
		SET_PED_AS_ENEMY(thisEnemy.ped, TRUE)
		IF setupAI
			SETUP_ENEMY_AI_VALUES(thisEnemy)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Creates an enemy group
PROC CREATE_ENEMY_GROUP(sTrailerPedGroup &thisGroup)
	FOR iEnemyCount = 0 TO NUMBER_OF_ENEMIES_IN_GROUP-1
		CREATE_ENEMY(thisGroup.enemy[iEnemyCount], TRUE)
		
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_23 nameLabel = thisGroup.name
			nameLabel += " "
			nameLabel += iEnemyCount
			SET_PED_NAME_DEBUG(thisGroup.enemy[iEnemyCount].ped, nameLabel)
			PRINTSTRING("Enemy spawned : ")
			PRINTSTRING(nameLabel)
			PRINTNL()
		#ENDIF
	ENDFOR
	
	thisGroup.triggered = FALSE
	thisGroup.triggeredEast = FALSE
	thisGroup.triggeredWest = FALSE
	thisGroup.dialoguePlayed = FALSE
	thisGroup.cleared = FALSE
ENDPROC

//PURPOSE: creates and sets up the specified enemies
PROC CREATE_TRAILER_ENEMIES()
	FOR iGroupCount = 0 TO NUMBER_OF_GROUPS-1
		CREATE_ENEMY_GROUP(sGroup[iGroupCount])
	ENDFOR
ENDPROC

//PURPOSE: creates and sets up the specified enemies
PROC CREATE_CHASE_ENEMIES(BOOL bWithBikers = TRUE)
	IF bWithBikers
		FOR iCounter = 0 TO MAX_CHASE_PED-1
			REQUEST_MODEL(cmbtChase[iCounter].model)
			IF HAS_MODEL_LOADED(cmbtChase[iCounter].model)
				IF NOT DOES_ENTITY_EXIST(cmbtChase[iCounter].ped)
					CREATE_ENEMY(cmbtChase[iCounter], FALSE)
					ADD_DEADPOOL_TRIGGER(cmbtChase[iCounter].ped, TRV1_KILLS)
				ENDIF
				IF iCounter > 1
					IF NOT IS_PED_INJURED(cmbtChase[iCounter].ped)
						SET_PED_SUFFERS_CRITICAL_HITS(cmbtChase[iCounter].ped, TRUE)
						SET_ENTITY_HEALTH(cmbtChase[iCounter].ped, 150)
						SET_PED_MAX_HEALTH(cmbtChase[iCounter].ped, 150)
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
					IF NOT IS_PED_INJURED(cmbtChase[iCounter].ped)
						TEXT_LABEL_23 nameLabel = "Chase "
						nameLabel += iCounter
						SET_PED_NAME_DEBUG(cmbtChase[iCounter].ped, nameLabel)
						PRINTSTRING("cmbtChase spawned : ")
						PRINTSTRING(nameLabel)
						PRINTNL()
					ENDIF
				#ENDIF
			ELSE
				WHILE NOT HAS_MODEL_LOADED(cmbtChase[iCounter].model)
					REQUEST_MODEL(cmbtChase[iCounter].model)
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDFOR
	ELSE
		FOR iCounter = 0 TO 1
			REQUEST_MODEL(cmbtChase[iCounter].model)
			IF HAS_MODEL_LOADED(cmbtChase[iCounter].model)
				IF NOT DOES_ENTITY_EXIST(cmbtChase[iCounter].ped)
					CREATE_ENEMY(cmbtChase[iCounter], FALSE)
				ENDIF
				IF iCounter > 1
					IF NOT IS_PED_INJURED(cmbtChase[iCounter].ped)
						SET_PED_SUFFERS_CRITICAL_HITS(cmbtChase[iCounter].ped, TRUE)
						SET_ENTITY_HEALTH(cmbtChase[iCounter].ped, 150)
						SET_PED_MAX_HEALTH(cmbtChase[iCounter].ped, 150)
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
					IF NOT IS_PED_INJURED(cmbtChase[iCounter].ped)
						TEXT_LABEL_23 nameLabel = "Chase "
						nameLabel += iCounter
						SET_PED_NAME_DEBUG(cmbtChase[iCounter].ped, nameLabel)
						PRINTSTRING("cmbtChase spawned : ")
						PRINTSTRING(nameLabel)
						PRINTNL()
					ENDIF
				#ENDIF
			ELSE
				WHILE NOT HAS_MODEL_LOADED(cmbtChase[iCounter].model)
					REQUEST_MODEL(cmbtChase[iCounter].model)
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

//PURPOSE: Makes a vehicle without a npcDriver
FUNC BOOL CREATE_VEHICLE_STRUCT(VEH_STRUCT &thisVehStruct)
	REQUEST_MODEL(thisVehStruct.model)
	IF HAS_MODEL_LOADED(thisVehStruct.model)
		IF NOT DOES_ENTITY_EXIST(thisVehStruct.veh)
			thisVehStruct.veh=CREATE_VEHICLE(thisVehStruct.model, thisVehStruct.startLoc, thisVehStruct.startHead)
			thisVehStruct.bCrash = FALSE
			thisVehStruct.bDoCrash = FALSE
		ELSE
			PRINTSTRING("vehicle made")
			PRINTNL()
			IF IS_VEHICLE_DRIVEABLE(thisVehStruct.veh)
				SET_VEHICLE_DIRT_LEVEL(thisVehStruct.veh,15.0)
				RETURN TRUE
			ENDIF
		ENDIF
		PRINTSTRING("CREATE_VEHICLE_STRUCT - FALSE")
		PRINTNL()	
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Makes a vehicle without a npcDriver
FUNC BOOL CREATE_SET_PIECE_VEHICLE(INT index,MODEL_NAMES vehmod,VECTOR vVehloc,FLOAT vehhead)//,MODEL_NAMES drivermod)
	PRINTINT(index)
	PRINTNL()
	IF NOT DOES_ENTITY_EXIST(vehSetP[index].veh)
		vehSetP[index].veh=CREATE_VEHICLE(vehmod,vVehloc,vehhead)
	ELSE
		PRINTSTRING("vehicle made")
		PRINTNL()
		IF IS_VEHICLE_DRIVEABLE(vehSetP[index].veh)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_23 nameLabel = "VEHICLE "
				nameLabel += index
				SET_VEHICLE_NAME_DEBUG(vehSetP[index].veh ,nameLabel)
			#ENDIF
			SET_VEHICLE_DIRT_LEVEL(vehSetP[index].veh,15.0)
			PRINTSTRING("TRUE")
			PRINTNL()	
			RETURN TRUE
		ENDIF
	ENDIF
	PRINTSTRING("CREATE_SET_PIECE_VEHICLE - FALSE")
	PRINTNL()	
	RETURN FALSE
ENDFUNC
//Makes a vehicle without a npcDriver
FUNC BOOL CREATE_SET_PIECE_PED(INT index,VECTOR vPEDloc,FLOAT PEDhead)//,MODEL_NAMES drivermod)
	PRINTINT(index)
	PRINTNL()
	IF NOT DOES_ENTITY_EXIST(npcDriver[index].ped)
		npcDriver[index].ped = CREATE_PED(PEDTYPE_MISSION, npcDriver[index].model, vPEDloc,PEDhead)  
		ADD_DEADPOOL_TRIGGER(npcDriver[index].ped, TRV1_KILLS)
		SET_ENTITY_AS_MISSION_ENTITY(npcDriver[index].ped)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(npcDriver[index].ped,TRUE)
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_23 nameLabel = "Driver "
			nameLabel += index
			SET_PED_NAME_DEBUG(npcDriver[index].ped ,nameLabel)
		#ENDIF
		PRINTSTRING("ped not exist")
		PRINTNL()
	ELSE
		PRINTSTRING("TRUE")
		PRINTNL()	
		RETURN TRUE
	ENDIF
	PRINTSTRING("CREATE_SET_PIECE_PED - FALSE")
	PRINTNL()	
	RETURN FALSE
ENDFUNC

//sets up the truck
PROC SetUpTruck()
	IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
		SET_ENTITY_QUATERNION(vehTruck.veh, 0.0109, 0.0036, 0.6236, 0.7816)
		SET_ENTITY_HEALTH(vehTruck.veh, 4000)
		SET_VEHICLE_EXTRA(vehTruck.veh, 1, TRUE)
		SET_VEHICLE_EXTRA(vehTruck.veh, 2, TRUE)
		SET_VEHICLE_EXTRA(vehTruck.veh, 3, TRUE)
		SET_VEHICLE_EXTRA(vehTruck.veh, 4, TRUE)
		SET_VEHICLE_EXTRA(vehTruck.veh, 5, TRUE)
		SET_VEHICLE_EXTRA(vehTruck.veh, 6, TRUE)
		SET_VEHICLE_EXTRA(vehTruck.veh, 7, TRUE)
		SET_VEHICLE_EXTRA(vehTruck.veh, 8, TRUE)
		SET_VEHICLE_EXTRA(vehTruck.veh, 9, TRUE)
		SET_VEHICLE_TYRES_CAN_BURST(vehTruck.veh,TRUE)
		SET_VEHICLE_DIRT_LEVEL(vehTruck.veh,15.0)
		SET_ENTITY_PROOFS(vehTruck.veh, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF 
ENDPROC

//---------¦ GENERAL ¦---------
//PURPOSE:		Defines values required when the player is starting from the checkpoint
PROC PerformCheckpoint()
	//IF (g_Des_Checkpoint[Des_Sabotage_CP]	= iDes4TripSkip)
	
		doCheckpointRequestLoad()
		
		WHILE NOT hasCheckpointRequestLoaded()
			WAIT(0)
		ENDWHILE
			
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
		bStageSetup = FALSE

		eMissionStage = STAGE_INIT_MISSION
	//ENDIF
ENDPROC


//---------------------------------¦ DEBUG PROCEDURES ¦-----------------------------------
#IF IS_DEBUG_BUILD
//PURPOSE:		Moniters for any keyboard inputs that would alter the mission(debug pass etc)
PROC Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		KILL_ANY_CONVERSATION()
		TRIGGER_MUSIC_EVENT("TRV1_FAIL")
		CLEAR_PRINTS()
		eMissionStage = PASSED_MISSION
	ENDIF
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		KILL_ANY_CONVERSATION()
		TRIGGER_MUSIC_EVENT("TRV1_FAIL")
		CLEAR_PRINTS()
		MissionFailed(FAIL_GENERIC)
	ENDIF
ENDPROC

//PURPOSE:		Creates debug widgets
PROC CREATE_WIDGETS() //Called on mission setup		
	widgetDebug = START_WIDGET_GROUP(" Trevor1.SC")
		// Widgets for the Main Mission Controlers
		START_WIDGET_GROUP("Uber Recording")
			ADD_WIDGET_BOOL("Commence the uber recorder?", bUberRecord)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP(" Main Mission Controlers")
			
			ADD_WIDGET_INT_SLIDER("eMissionStage", ieMissionStageTemp, 0, 99, 1)
			ADD_WIDGET_VECTOR_SLIDER("vForce",vForce,0,1000,0.1)
			ADD_WIDGET_FLOAT_SLIDER("fThisValue",fThisValue,-1000,1000,0.1)
			ADD_WIDGET_STRING("Main Mission Controlers")
			ADD_WIDGET_INT_SLIDER("eMissionStage", ieMissionStageTemp, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("iStageSection", iStageSection, 0, 99, 1)
			ADD_WIDGET_STRING("BUDDY  Controlers")				
			ADD_WIDGET_INT_SLIDER("iRonProgress", iBuddyProgress, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("iWadeProgress", iWadeProgress, 0, 99, 1)
			ADD_WIDGET_STRING("Mission Stage Loaders Controlers")
			ADD_WIDGET_INT_SLIDER("iEnemyCreationProc", iEnemyCreationProc, 0, 99, 1)		
			ADD_WIDGET_INT_SLIDER("iPreLoadStage", iPreLoadStage, 0, 99, 1)
			ADD_WIDGET_STRING("ENEMY Controlers")				
			ADD_WIDGET_INT_SLIDER("iEnemyAIProg", iEnemyAIProg, 0, 99, 1)				
			ADD_WIDGET_INT_SLIDER("iEnemyEastWave", iEnemyEastWave, 0, 99, 1)				
			ADD_WIDGET_INT_SLIDER("iEnemyWestWave", iEnemyWestWave, 0, 99, 1)				
			ADD_WIDGET_INT_READ_ONLY("iAttackSide", iAttackSide)
			ADD_WIDGET_INT_SLIDER("iNumberOfPedsAliveTrailer", iNumberOfPedsAliveTrailer, 0, 99, 1)				
			ADD_WIDGET_INT_SLIDER("iNumberOfPedsAliveEast", iNumberOfPedsAliveEast, 0, 99, 1)				
			ADD_WIDGET_INT_SLIDER("iNumberOfPedsAliveWest", iNumberOfPedsAliveWest, 0, 99, 1)
			TEXT_LABEL_63 PedName
			START_WIDGET_GROUP("sGroup[iGroupCount].enemy[iEnemyCount].iEnemyProgress")
				FOR iGroupCount = 0 TO (NUMBER_OF_GROUPS-1)
					FOR iEnemyCount = 0 TO (NUMBER_OF_ENEMIES_IN_GROUP-1)
						PedName = sGroup[iGroupCount].name
						PedName += " "
						PedName += iEnemyCount
						PedName += " progress"
						ADD_WIDGET_INT_SLIDER(PedName, sGroup[iGroupCount].enemy[iEnemyCount].iEnemyProgress, 0, 99, 1)		
					ENDFOR
				ENDFOR
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP(" cmbtEnemyEastWave[iCounter].iEnemyProgress")
				FOR iCounter = 0 To (MAX_ENEMY_EAST_WAVE_PED -1)
					PedName = "cmbtEnemyEastWave "
					PedName +=iCounter
					PedName += " progress"
					ADD_WIDGET_INT_SLIDER(PedName, cmbtEnemyEastWave[iCounter].iEnemyProgress, 0, 99, 1)		
				ENDFOR
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP(" cmbtEnemyWestWave[iCounter].iEnemyProgress")
				FOR iCounter = 0 To (MAX_ENEMY_WEST_WAVE_PED -1)
					PedName = "cmbtEnemyWestWave "
					PedName +=iCounter
					PedName += " progress"
					ADD_WIDGET_INT_SLIDER(PedName, cmbtEnemyWestWave[iCounter].iEnemyProgress, 0, 99, 1)		
				ENDFOR
			STOP_WIDGET_GROUP()
			ADD_WIDGET_STRING("Misc Controlers")				
			ADD_WIDGET_INT_SLIDER("iControlChaseSetP", iControlChaseSetP, 0, 99, 1)						
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Camera Stuf")
			ADD_WIDGET_INT_SLIDER("iCamProgressDB", iCamProgressDB, 0, 99, 1)
			//ADD_WIDGET_BOOL("bUseNormalChaseCam",bUseNormalChaseCam)
			//ADD_WIDGET_BOOL("bResetCams",bResetCams)
			//ADD_WIDGET_BOOL("bUseNormalChaseCam",bUseNormalChaseCam)
			//ADD_WIDGET_BOOL("bUseNormalChaseCam",bUseNormalChaseCam)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Caravan Smash Stuff")
			ADD_WIDGET_BOOL("Reset up the caravan",bSetupCaravanDB)
			//ADD_WIDGET_BOOL("bApplyForce",bApplyForce)
			FOR iCounter = 2 To (MAX_TRAILER_PARTS -1)
				IF iCounter = 2
					PedName = "Front side"
				ELIF iCounter = 3
					PedName = "Back side"
				ELIF iCounter = 4
					PedName = "Kitchen 1"
				ELIF iCounter = 5
					PedName = "Kitchen 2"
				ELIF iCounter = 6
					PedName = "Bathroom"
				ELIF iCounter = 7
					PedName = "Fridge"
				ENDIF
				PedName += " is Active"
				//ADD_WIDGET_BOOL(PedName ,SmashTrailer[iCounter].bPartActive)
			ENDFOR
			START_WIDGET_GROUP("Caravan Parts Locations")
				FOR iCounter = 2 To (MAX_TRAILER_PARTS -1)
					IF iCounter = 2
						PedName = "Front side"
					ELIF iCounter = 3
						PedName = "Back side"
					ELIF iCounter = 4
						PedName = "Kitchen 1"
					ELIF iCounter = 5
						PedName = "Kitchen 2"
					ELIF iCounter = 6
						PedName = "Bathroom"
					ELIF iCounter = 7
						PedName = "Fridge"
					ENDIF
					PedName += " Pos"
					//ADD_WIDGET_VECTOR_SLIDER(PedName, SmashTrailer[iCounter].vPos, -9999.99, 9999.99, 0.01)	
					IF iCounter = 2
						PedName = "Front side"
					ELIF iCounter = 3
						PedName = "Back side"
					ELIF iCounter = 4
						PedName = "Kitchen 1"
					ELIF iCounter = 5
						PedName = "Kitchen 2"
					ELIF iCounter = 6
						PedName = "Bathroom"
					ELIF iCounter = 7
						PedName = "Fridge"
					ENDIF
					PedName += " Rot"
					//ADD_WIDGET_VECTOR_SLIDER(PedName, SmashTrailer[iCounter].vRot, -400.0, 400.0, 0.01)		
				ENDFOR
			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("Repostion objects",bRePos)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("PTFX")
			ADD_WIDGET_VECTOR_SLIDER("Rear Left Wheel Offset", vRearLeftWheelOffset, -10.0, 10.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("Rear Right Wheel Offset", vRearRightWheelOffset, -10.0, 10.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("PTFX Rotation", vPTFXRotation, -360.0, 360.0, 1.0)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Chase Stuff")
		
				ADD_WIDGET_FLOAT_SLIDER("fSkipAmount", fSkipAmount, 0.0, 200000.0, 0.1)					
				ADD_WIDGET_FLOAT_SLIDER("fPlayBackSpeed", fPlayBackSpeed, -10.0, 10.0, 0.001)					
				//ADD_WIDGET_BOOL("bStopPlayback",bStopPlayback)
				ADD_WIDGET_BOOL("bPausePlayback",bPausePlayback)
				ADD_WIDGET_BOOL("Reset the car to start position",bReset)
				ADD_WIDGET_BOOL("Start All Recs",bRecStarted[0])
				ADD_WIDGET_BOOL("Include van",bRecStarted[1])
				ADD_WIDGET_BOOL("include bike 1",bRecStarted[2])
				ADD_WIDGET_BOOL("include bike 2",bRecStarted[3])
				ADD_WIDGET_BOOL("Go to debug mode",bSkipToCarRec)
				ADD_WIDGET_BOOL("Dont control car rec speed",bControlCarRec)
				ADD_WIDGET_BOOL("bDebugText",bDebugText)
				ADD_WIDGET_BOOL("bPutPedinCar",bPutPedinCar)
				ADD_WIDGET_BOOL("bPausePlayback",bPausePlayback)
				ADD_WIDGET_BOOL("bSkipToPoint",bSkipToPoint)
				ADD_WIDGET_FLOAT_SLIDER("fRecSkipTime", fRecSkipTime, -600000.0, 600000.0, 0.001)					
				ADD_WIDGET_FLOAT_SLIDER("fXCoord", fXCoord, 0.0, 1.0, 0.001)					
				ADD_WIDGET_FLOAT_SLIDER("fYCoord", fYCoord, 0.0, 1.0, 0.001)					
			STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Debug Stuff")
			/*START_WIDGET_GROUP(" Trevor Rage Controlers")
				ADD_WIDGET_FLOAT_SLIDER("fNumberKilledByTrevor", fNumberKilledByTrevor, 0.0, 100.0, 1.0)					
				ADD_WIDGET_FLOAT_SLIDER("fRageValue", fRageValue, 0.0, 100.0, 0.1)					
				//ADD_WIDGET_BOOL("bToggleRage",bToggleRage)	
			STOP_WIDGET_GROUP()*/
			
			START_WIDGET_GROUP("Vehicle slow Down")
				ADD_WIDGET_BOOL("bActive",bActive)
				ADD_WIDGET_FLOAT_SLIDER("fVehicleSpeed", fVehicleSpeed, 0.0, 100.0, 1.0)						
			STOP_WIDGET_GROUP()			
			ADD_WIDGET_BOOL("bRunDebugProcs",bRunDebugProcs)
			ADD_WIDGET_BOOL("bMakeSmashPed",bMakeSmashPed)
			ADD_WIDGET_BOOL("bOutPut",bOutPut)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(widgetDebug)
	SET_UBER_PARENT_WIDGET_GROUP(widgetDebug)
	//SET_CAM_RECORDING_WIDGET_GROUP(cam_data, widgetDebug)
	//SET_CAM_RECORDING_WIDGET_GROUP(cam_data, widgetDebug)
	
ENDPROC

INT iWdGenProg
BOOL bWdShowPlayerSpeed
FLOAT fWdPlayerSpeed
PROC DO_DW_GENERAL_WIDGET()
	SWITCH iWdGenProg
		CASE 0
			SET_CURRENT_WIDGET_GROUP(widgetDebug)	
				START_WIDGET_GROUP("General widget")
					ADD_WIDGET_BOOL("Use debug", bDwUseDebug)
					ADD_WIDGET_FLOAT_SLIDER("Speed", fWdPlayerSpeed, 0.0, 1000.0, 0.1)
					ADD_WIDGET_BOOL("Show player speed", bWdShowPlayerSpeed)
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			iWdGenProg++
		BREAK
		
		CASE 1
			IF bWdShowPlayerSpeed
				fWdPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iWdExplosProg
BOOL bWdPlotBlastRad
PROC DO_DW_EXPLOSION_WIDGET()
	INT i, j, k
	VECTOR vTempFUelCanLoc[10]
	SWITCH iWdExplosProg
		CASE 0
			SET_CURRENT_WIDGET_GROUP(widgetDebug)	
				START_WIDGET_GROUP("Explosion widget")
					ADD_WIDGET_BOOL("Plot gas can blast rad", bWdPlotBlastRad)				
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			iWdExplosProg++
		BREAK
		
		CASE 1
			IF bWdPlotBlastRad
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				REPEAT COUNT_OF(oFuelCan) i
					IF DOES_ENTITY_EXIST(oFuelCan[i])
						
						FOR k = 1 TO 10
							IF k % 2 = 0
								FOR j = 0 TO 12
									vTempFUelCanLoc[i] = vFuelCanLoc[i]
									vTempFUelCanLoc[i].x += ( k * (COS(j/12.0* 360.0)))
									vTempFUelCanLoc[i].y += ( k * (SIN(j/12.0* 360.0)))
									DRAW_DEBUG_SPHERE(vTempFUelCanLoc[i], 0.25)
								ENDFOR
							ENDIF
						ENDFOR
					ENDIF
				ENDREPEAT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
INT iWdRunnerProg
BOOL bWdRunnerCreateAll, bWDRunnerDrawRoutes
BOOL bWdTriggerAllRunners, bWdBlipAllAliveRunners
PROC DO_DW_RUNNER_WIDGET()
	INT i
	SWITCH iWdRunnerProg
		CASE 0
			SET_CURRENT_WIDGET_GROUP(widgetDebug)	
				START_WIDGET_GROUP("Runner widget")
					ADD_WIDGET_BOOL("Create all runners", bWdRunnerCreateAll)
					ADD_WIDGET_BOOL("Draw runner routes", bWDRunnerDrawRoutes)
					ADD_WIDGET_BOOL("Trigger all runners", bWdTriggerAllRunners)
					ADD_WIDGET_BOOL("Blip all alive runners", bWdBlipAllAliveRunners)
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			iWdRunnerProg++
		BREAK
		
		CASE 1
			IF bWDRunnerDrawRoutes
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				REPEAT COUNT_OF(gangRunner) i
					IF NOT IS_ENTITY_DEAD(gangRunner[i].ped)
						IF NOT ARE_VECTORS_EQUAL(gangRunner[i].vGotoCoords, <<0.0, 0.0, 0.0>>)
							DRAW_DEBUG_LINE(GET_ENTITY_COORDS(gangRunner[i].ped, FALSE), gangRunner[i].vGotoCoords)
							DRAW_DEBUG_SPHERE(gangRunner[i].vGotoCoords, 0.25)
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bWdRunnerCreateAll
				bWdRunnerCreateAll = FALSE
				REPEAT COUNT_OF(gangRunner) i
					gangRunner[i].bTriggered = FALSE
					gangRunner[i].bHasSequence = FALSE
					IF DOES_ENTITY_EXIST(gangRunner[i].ped)
						DELETE_PED(gangRunner[i].ped)
					ENDIF
				ENDREPEAT
				SETUP_THESE_PEDS(PED_RUNNER_LHS1)
				SETUP_THESE_PEDS(PED_RUNNER_LHS2)
				SETUP_THESE_PEDS(PED_RUNNER_RHS_CLOSE1)
			ENDIF
			
			IF bWdTriggerAllRunners
				bWdTriggerAllRunners = FALSE
				REPEAT COUNT_OF(gangRunner) i
					IF NOT IS_PED_INJURED(gangRunner[i].ped)
						gangRunner[i].bTriggered = TRUE
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bWdBlipAllAliveRunners
				bWdBlipAllAliveRunners= FALSE
				REPEAT COUNT_OF(gangRunner) i
					IF NOT IS_ENTITY_DEAD(gangRunner[i].ped)
						gangRunner[i].blipPed = CREATE_BLIP_FOR_PED(gangRunner[i].ped)
					ENDIF
				ENDREPEAT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iWdGangProg
BOOL bWdGangSHowDistFromPlayer
BOOL bWdGangShowId
BOOL bWdSHowZOnes
BOOL bWdLeaveTwoLeft
BOOL bWdTestMachineGunGuy
BOOL bWdCreateMachineGunGuy
BOOL bWdMachineGunGuyShoot
BOOL bWdMachineRestoresStage

FLOAT fWdmachineMaxDistToPlayer
INT iWdMachineGunGuyShootAtSpotTime
PROC DO_DW_GANG_WIDGET()
	INT i
	SEQUENCE_INDEX seq
	VECTOR vSHootAt[4]
	SWITCH iWdGangProg
		CASE 0
			SET_CURRENT_WIDGET_GROUP(widgetDebug)	
				START_WIDGET_GROUP("Gang widget")
					ADD_WIDGET_BOOL("Show ID", bWdGangShowId)
					ADD_WIDGET_BOOL("Show dist from player", bWdGangSHowDistFromPlayer)
					ADD_WIDGET_BOOL("Show zones", bWdSHowZOnes)
					ADD_WIDGET_BOOL("leave 2 left", bWdLeaveTwoLeft)
					START_WIDGET_GROUP("Machine gun guy")
						ADD_WIDGET_BOOL("Test machine gun guy", bWdTestMachineGunGuy)
						ADD_WIDGET_BOOL("Create machine gun guy", bWdCreateMachineGunGuy)
						ADD_WIDGET_INT_SLIDER("Time to shoot at spot", iWdMachineGunGuyShootAtSpotTime, 0, 10000, 1)
						ADD_WIDGET_FLOAT_SLIDER("Max dist multiplier", fWdmachineMaxDistToPlayer, 0.0, 1.0, 0.05)
						ADD_WIDGET_BOOL("Shoot at player", bWdMachineGunGuyShoot)
						ADD_WIDGET_BOOL("Restore mission stage", bWdMachineRestoresStage)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			iWdMachineGunGuyShootAtSpotTime = 500
			fWdmachineMaxDistToPlayer = 0.85
			iWdGangProg++
		BREAK
		
		CASE 1
			IF bWdGangShowId
				NAME_GANG_PEDS()
				bWdGangShowId = FALSE
			ENDIF
			
			IF bWdGangSHowDistFromPlayer
				REPEAT COUNT_OF(gangEnemy) i
					SHOW_PED_DISTANCE_FROM_PLAYER(gangEnemy[i].ped)
				ENDREPEAT
			ENDIF
			
			IF bWdTestMachineGunGuy
				eMissionStage = STAGE_TEST
				IF bWdCreateMachineGunGuy
					bWdCreateMachineGunGuy = FALSE
					IF DOES_ENTITY_EXIST(gangEnemy[0].ped)
						DELETE_PED(gangEnemy[0].ped)
					ENDIF
					gangEnemy[0].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 79.3519, 3706.4968, 40.1082 >>, 69.7469 , WEAPONTYPE_CARBINERIFLE)
				ENDIF
				
				IF bWdMachineGunGuyShoot
					bWdMachineGunGuyShoot = FALSE
					VECTOR vTempPos
					VECTOR vMachineGunPos
					
					IF NOT IS_PED_INJURED(gangEnemy[0].ped)
						vMachineGunPos = GET_ENTITY_COORDS(gangEnemy[0].ped, FALSE)
						vTempPos = (GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) - vMachineGunPos)
						vSHootAt[0] = vMachineGunPos +((fWdmachineMaxDistToPlayer - 0.15)* vTempPos)
						GET_GROUND_Z_FOR_3D_COORD(vSHootAt[0], vSHootAt[0].z)
						vSHootAt[1] = vMachineGunPos + ((fWdmachineMaxDistToPlayer - 0.1) * vTempPos)
						GET_GROUND_Z_FOR_3D_COORD(vSHootAt[1], vSHootAt[1].z)
						vSHootAt[2] = vMachineGunPos + ((fWdmachineMaxDistToPlayer - 0.05 )* vTempPos)
						GET_GROUND_Z_FOR_3D_COORD(vSHootAt[2], vSHootAt[2].z)
						vSHootAt[3] = vMachineGunPos + (fWdmachineMaxDistToPlayer * vTempPos)
						GET_GROUND_Z_FOR_3D_COORD(vSHootAt[3], vSHootAt[3].z)
						OPEN_SEQUENCE_TASK(seq)
							TASK_SHOOT_AT_COORD(NULL,vSHootAt[0],iWdMachineGunGuyShootAtSpotTime, FIRING_TYPE_CONTINUOUS)
							TASK_SHOOT_AT_COORD(NULL,vSHootAt[1],iWdMachineGunGuyShootAtSpotTime, FIRING_TYPE_CONTINUOUS)
							TASK_SHOOT_AT_COORD(NULL,vSHootAt[2],iWdMachineGunGuyShootAtSpotTime, FIRING_TYPE_CONTINUOUS)
							TASK_SHOOT_AT_COORD(NULL,vSHootAt[3],iWdMachineGunGuyShootAtSpotTime, FIRING_TYPE_CONTINUOUS)
						CLOSE_SEQUENCE_TASK(seq)
						
						TASK_PERFORM_SEQUENCE(gangEnemy[0].ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
				
				IF bWdMachineRestoresStage
					bWdMachineRestoresStage = FALSE
					bWdTestMachineGunGuy = FALSE
					eMissionStage = STAGE_CARAVAN_PARK_FIGHT
				ENDIF
			ENDIF
			
			 
			
			IF bWdSHowZOnes
				//eMissionStage = STAGE_TEST
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<34.391068,3649.478271,38.026222>>, <<110.601883,3682.793213,41.254082>>, 28.000000)
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<40.028873,3666.858398,37.271870>>, <<3.502873,3738.676514,43.312599>>, 48.000000)
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<75.489082,3766.716309,37.440598>>, <<107.098579,3696.767822,41.240311>>, 33.500000)
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<47.669071,3613.861084,36.754425>>, <<110.732727,3641.735840,42.004292>>, 48.500000)
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<65.113228,3708.477051,33.265129>>, <<37.230984,3766.059082,41.778973>>, 39.500000)
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(),<<78.027145,3703.876709,34.608215>>, <<59.605186,3716.406006,44.753613>>, 41.000000)
				ENDIF
			ENDIF
			
			IF bWdLeaveTwoLeft
				bWdLeaveTwoLeft = FALSE
				INT iFirst
				INT iSecond
				iFirst = -1
				iSecond = -1
				REPEAT COUNT_OF(gangEnemy) i
					IF NOT IS_PED_INJURED(gangEnemy[i].ped)
						IF iFirst = -1
							iFirst = i
						ELSE
							IF iSecond = -1
								iSecond = i
							ELSE
								SET_ENTITY_HEALTH(gangEnemy[i].ped, 5)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
	
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

INT iWdBikeConvProg
INT iWdBikeLead
BOOL bWdResetBikes
BOOL bWdRecordNewBike
BOOL bWdStopRecNewBike
BOOL bWdOutputStartPos
BOOL bWdUseSqStartStop
BOOL bWdCreateBikeForPlayer
VECTOR vWdBikeStart
FLOAT fWdBikeHead
VEHICLE_INDEX vehWdNewBike
PROC DO_DW_BIKE_CONVOY_WIDGET(VEHICLE_INDEX vehLead, INT iLeadRecNo)
	STRING sBikeRec = "Trevor1DW"
	INT i
	SWITCH iWdBikeConvProg
		CASE 0
			SET_CURRENT_WIDGET_GROUP(widgetDebug)	
				START_WIDGET_GROUP("Bike widget ")
					ADD_WIDGET_INT_SLIDER("Lead rec no.", iWdBikeLead,0, 100, 1)
					ADD_WIDGET_BOOL("Create bike for player", bWdCreateBikeForPlayer)
					ADD_WIDGET_BOOL("Reset bikes", bWdResetBikes)
					ADD_WIDGET_BOOL("Record new bike", bWdRecordNewBike)
					ADD_WIDGET_BOOL("Stop rec new bikes", bWdStopRecNewBike)
					ADD_WIDGET_BOOL("USe squares start/stop", bWdUseSqStartStop)
					ADD_WIDGET_BOOL("Output start pos", bWdOutputStartPos)
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			iWdBikeLead = iLeadRecNo
			iWdBikeConvProg++
		BREAK
		CASE 1
			IF bWdCreateBikeForPlayer
				eMissionStage = STAGE_TEST
				bWdCreateBikeForPlayer = FALSE
				REQUEST_MODEL(HEXER)
				WHILE NOT HAS_MODEL_LOADED(HEXER)
					WAIT(0)
				ENDWHILE
				VEHICLE_INDEX vehTemp
				vehTemp = CREATE_VEHICLE(HEXER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 1.0, 0.0>>), 0.0)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTemp)
				
				REPEAT COUNT_OF(gangEnemy) i
					IF i <> 12
						IF NOT IS_PED_INJURED(gangEnemy[i].ped)
							SET_ENTITY_HEALTH(gangEnemy[i].ped, 5)
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			IF bWdResetBikes
				eMissionStage = STAGE_TEST
				IF NOT IS_ENTITY_DEAD(vehLead)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLead)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehLead)
					ENDIF
					REQUEST_VEHICLE_RECORDING(iWdBikeLead, sBikeRec)
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iWdBikeLead, sBikeRec)
						WAIT(0)
					ENDWHILE
					IF NOT IS_ENTITY_DEAD(vehLead)
						START_PLAYBACK_RECORDED_VEHICLE(vehLead, iWdBikeLead, sBikeRec)
						WAIT(0)
						IF NOT IS_ENTITY_DEAD(vehLead)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehLead)
						ENDIF
					ENDIF
					
					IF NOT ARE_VECTORS_EQUAL(vWdBikeStart, <<0.0, 0.0, 0.0>>)
						IF NOT IS_ENTITY_DEAD(vehWdNewBike)
							SET_ENTITY_COORDS(vehWdNewBike, vWdBikeStart)
							SET_ENTITY_HEADING(vehWdNewBike, fWdBikeHead)
						ENDIF
					ENDIF
					bWdResetBikes = FALSE
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(vehWdNewBike)
				IF bWdStopRecNewBike
					bWdStopRecNewBike = FALSE
					PRINT_NOW("CRECSTOP5", 8000, 1)
					STOP_RECORDING_VEHICLE(vehWdNewBike)
				ENDIF
				
				
			ENDIF
			
			IF bWdUseSqStartStop	
				eMissionStage = STAGE_TEST
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehWdNewBike = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehWdNewBike)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehWdNewBike)
						IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
							STOP_RECORDING_VEHICLE(vehWdNewBike)
							PRINT_NOW("CRECSTOP5", 8000, 1)
						ENDIF
					ELSE
						IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
							bWdRecordNewBike = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehLead)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLead)
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(vehLead, RDM_WHOLELINE)
					ENDIF
				ENDIF

			ENDIF
			
			IF bWdRecordNewBike
				eMissionStage = STAGE_TEST
				//-- Move the pickup out the way
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[12].veh)
						SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh)
					ENDIF
				ENDIF
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehWdNewBike = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					vWdBikeStart = GET_ENTITY_COORDS(vehWdNewBike, FALSE)
					fWdBikeHead = GET_ENTITY_HEADING(vehWdNewBike)
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehLead)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLead)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehLead)
					ENDIF
					REQUEST_VEHICLE_RECORDING(iWdBikeLead, sBikeRec)
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iWdBikeLead, sBikeRec)
						WAIT(0)
					ENDWHILE
					IF NOT IS_ENTITY_DEAD(vehLead)
						START_PLAYBACK_RECORDED_VEHICLE(vehLead, iWdBikeLead, sBikeRec)
					
					ENDIF
					IF NOT IS_ENTITY_DEAD(vehWdNewBike)
						START_RECORDING_VEHICLE(vehWdNewBike,0, sBikeRec,TRUE)
						CLEAR_PRINTS()
						KILL_ANY_CONVERSATION()
						PRINT_NOW("CRECSTART5", 300000, 1)
					ENDIF
					bWdRecordNewBike = FALSE
				ENDIF
			ENDIF
			
			
			
			IF bWdOutputStartPos
				bWdOutputStartPos = FALSE
				IF NOT ARE_VECTORS_EQUAL(vWdBikeStart, <<0.0, 0.0, 0.0>>)
					PRINT_DW_VECTOR_TO_TEMP_FILE_WITH_STRING(vWdBikeStart, "Bike start....")
					VECTOR vTempBike
					vTempBike.z = fWdBikeHead
					PRINT_DW_VECTOR_TO_TEMP_FILE_WITH_STRING(vTempBike, "Bike rot....")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iWdRopeProg
BOOL bWdInitRope
BOOL bWdCreateRope
BOOL bWdRopeAttachPedToObject
BOOL bWdRopeSetToRagdoll
BOOL bWdResetRagdoll
BOOL bWdRopePlayAnim
BOOL bWdRopeMovePed
OBJECT_INDEX oWdRopeTemp
VECTOR vWdRopeOff
VECTOR vWdRopPedRot, vWdRopCurRot
VECTOR vWdRopePedPos, vWdRopeCurPos
ROPE_INDEX ropeWd
INT iWdRopeAttachProg
INT iWdPoseAnimProg
PROC DO_DW_ROPE_ATTACH_WIDGET()
	SWITCH iWdRopeProg
		CASE 0
			SET_CURRENT_WIDGET_GROUP(widgetDebug)
				START_WIDGET_GROUP("Rope widget")
					ADD_WIDGET_BOOL("Init", bWdInitRope)
					ADD_WIDGET_VECTOR_SLIDER("Rope off", vWdRopeOff,-100.0, 100.0, 0.1) 
					ADD_WIDGET_BOOL("Create rope", bWdCreateRope)
					
					START_WIDGET_GROUP("ped")
						ADD_WIDGET_BOOL("Play Anim", bWdRopePlayAnim)
						ADD_WIDGET_BOOL("Attach ped", bWdRopeAttachPedToObject)
						ADD_WIDGET_BOOL("Set ragdoll", bWdRopeSetToRagdoll)
						ADD_WIDGET_VECTOR_SLIDER("Ped rot", vWdRopPedRot, -360.0, 360.0, 1.0)
						ADD_WIDGET_VECTOR_SLIDER("Ped pos", vWdRopePedPos, -4000.0, 4000.0, 0.1) 
						ADD_WIDGET_BOOL("Move ped", bWdRopeMovePed)
					STOP_WIDGET_GROUP()
					
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			vWdRopeOff = << 0.0, -2.8, 0.0>>
			iWdRopeProg++
		BREAK
		
		CASE 1
			IF bWdInitRope
				REQUEST_MODEL(PROP_POOL_BALL_01)
				REQUEST_MODEL(G_M_Y_LOST_01)
				REQUEST_ANIM_DICT("misswiphanging")
				IF HAS_MODEL_LOADED(PROP_POOL_BALL_01)
				AND HAS_MODEL_LOADED(G_M_Y_LOST_01)
				AND HAS_ANIM_DICT_LOADED("misswiphanging")
					IF DOES_ENTITY_EXIST(gangEnemy[0].ped)
						DELETE_PED(gangEnemy[0].ped)
					ENDIF
					IF DOES_ROPE_EXIST(ropeWd)
					     DELETE_ROPE(ropeWd)
					ENDIF
					
					IF DOES_ENTITY_EXIST(oWdRopeTemp)
						DELETE_OBJECT(oWdRopeTemp)
					ENDIF

					gangEnemy[0].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 1988.2029, 3842.2546, 31.3330 >>, 212.4112 , WEAPONTYPE_UNARMED)
					vWdRopePedPos = GET_ENTITY_COORDS(gangEnemy[0].ped, FALSE)
					vWdRopeCurPos = vWdRopePedPos
					vWdRopPedRot = GET_ENTITY_ROTATION(gangEnemy[0].ped)
					vWdRopCurRot = vWdRopPedRot
					FREEZE_ENTITY_POSITION(gangEnemy[0].ped, TRUE)
					IF NOT IS_ENTITY_DEAD(vehTrevor.veh)
						SET_ENTITY_HEADING(oWdRopeTemp, GET_ENTITY_HEADING(vehTrevor.veh))
						SET_OBJECT_PHYSICS_PARAMS(oWdRopeTemp,400.0, -1.0,<<-1.0,-1.0, -1.0>>, <<-1.0,-1.0, -1.0>>)	
					//	APPLY_FORCE_TO_ENTITY(oWdRopeTemp, APPLY_TYPE_IMPULSE, <<0,0,-2>>, <<0,0,0>>,0, TRUE, TRUE, FALSE)
					ENDIF
					bWdRopeAttachPedToObject = FALSE
					iWdRopeAttachProg = 0
					bWdInitRope = FALSE
					bWdResetRagdoll = FALSE
				ENDIF
			ENDIF
			
			IF bWdCreateRope
				bWdCreateRope = FALSE
				IF DOES_ROPE_EXIST(ropeWd)
					DELETE_ROPE(ropeWd)
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehTrevor.veh)
					IF NOT IS_ENTITY_DEAD(gangEnemy[0].ped)
						VECTOR vWdRopeCreate
						vWdRopeCreate = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrevor.veh, vWdRopeOff)
						ropeWd = ADD_ROPE(vWdRopeCreate, <<0,0,0>>, 10.0, PHYSICS_ROPE_DEFAULT)
						//ATTACH_ENTITIES_TO_ROPE(ropeWd,  vehTrevor.veh, oWdRopeTemp, vWdRopeCreate, GET_ENTITY_COORDS(oWdRopeTemp), 10.0, 0, 0)
						ATTACH_ENTITIES_TO_ROPE(ropeWd,  vehTrevor.veh, gangEnemy[0].ped, vWdRopeCreate, GET_ENTITY_COORDS(gangEnemy[0].ped, FALSE), 10.0, 0, 0)
					//	SET_PED_TO_RAGDOLL(gangEnemy[0].ped, 10000, 10000, TASK_RELAX) 
					ENDIF
				ENDIF
				
				
			ENDIF
			
			IF bWdRopePlayAnim
				IF NOT IS_ENTITY_DEAD(gangEnemy[0].ped)
					SWITCH iWdPoseAnimProg
						CASE 0
							TASK_PLAY_ANIM(gangEnemy[0].ped, "misswiphanging", "idle",INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED )
							iWdPoseAnimProg++
						BREAK
						
						CASE 1
							IF IS_ENTITY_PLAYING_ANIM(gangEnemy[0].ped, "misswiphanging", "idle" )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(gangEnemy[0].ped)
								iWdPoseAnimProg++
							ENDIF
						BREAK
						
						CASE 2
							IF NOT IS_ENTITY_DEAD(vehTrevor.veh)
								VECTOR vTempPedPos
								vTempPedPos = GET_ENTITY_COORDS(gangEnemy[0].ped, FALSE)
								vTempPedPos.z = 30.433
								SET_ENTITY_COORDS(gangEnemy[0].ped, vTempPedPos)
								SET_ENTITY_ROTATION(gangEnemy[0].ped, <<-90.0, 0.0, (GET_ENTITY_HEADING(vehTrevor.veh) + 180.0)>>)
								//FREEZE_ENTITY_POSITION(gangEnemy[0].ped, FALSE)
								//SET_PED_TO_RAGDOLL(gangEnemy[0].ped, 1500, 2000, TASK_RELAX) 
								bWdResetRagdoll = TRUE
								bWdRopePlayAnim = FALSE
								iWdPoseAnimProg++
							ENDIF
						BREAK
					ENDSWITCH
	
				ENDIF
			ENDIF
			
			IF bWdRopeMovePed
				IF NOT IS_ENTITY_DEAD(gangEnemy[0].ped)
					IF NOT ARE_VECTORS_EQUAL(vWdRopPedRot, vWdRopCurRot)
					OR NOT ARE_VECTORS_EQUAL(vWdRopePedPos, vWdRopeCurPos)
						SET_ENTITY_COORDS(gangEnemy[0].ped, vWdRopePedPos)
						SET_ENTITY_ROTATION(gangEnemy[0].ped, vWdRopPedRot)
						vWdRopeCurPos = vWdRopePedPos
						vWdRopCurRot = vWdRopPedRot
					ENDIF
				ENDIF
			ENDIF
			
			IF bWdRopeAttachPedToObject
				IF iWdRopeAttachProg = 0
				ENDIF
				IF NOT IS_ENTITY_DEAD(gangEnemy[0].ped)
					/*SWITCH iWdRopeAttachProg
						CASE 0
							
						//	ATTACH_ENTITY_TO_ENTITY(gangEnemy[0].ped, oWdRopeTemp, 0, <<0.0, -0.5, 0.0>>, <<0.0, 0.0, 0.0>>)
							TASK_PLAY_ANIM(pedToSetup, "misswiphanging", "idle",INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED )
							iWdRopeAttachProg++
							
						BREAK
						
						CASE 1
							DETACH_ENTITY(gangEnemy[0].ped)
							SET_PED_TO_RAGDOLL(gangEnemy[0].ped, 1500, 2000, TASK_RELAX) 
						//	ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(gangEnemy[0].ped, oWdRopeTemp, GET_PED_BONE_INDEX(gangEnemy[0].ped,BONETAG_R_HAND) , -1, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( oWdRopeTemp, GET_PED_BONE_COORDS(gangEnemy[0].ped,BONETAG_R_HAND,<<0.0,0.0,0.0>>)),<<0.0,0.0,0.0>>,<<0.0,0.0,0.0>>, -1.0, TRUE, FALSE)                                            
							ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(gangEnemy[0].ped, oWdRopeTemp, GET_PED_BONE_INDEX(gangEnemy[0].ped,BONETAG_ROOT) , -1, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( oWdRopeTemp, GET_PED_BONE_COORDS(gangEnemy[0].ped,BONETAG_ROOT,<<0.0,0.0,0.0>>)),<<0.0,0.0,0.0>>,<<0.0,0.0,0.0>>, -1.0, FALSE, FALSE)
							iWdRopeAttachProg++
						BREAK
						
						CASE 2
							IF IS_PED_RAGDOLL(gangEnemy[0].ped)
								IF IS_PED_RUNNING_RAGDOLL_TASK(gangEnemy[0].ped)
									RESET_PED_RAGDOLL_TIMER(gangEnemy[0].ped)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH*/
					
					
				ENDIF
				
			ENDIF
			
			IF bWdRopeSetToRagdoll
				IF NOT IS_ENTITY_DEAD(gangEnemy[0].ped)
					SET_PED_TO_RAGDOLL(gangEnemy[0].ped, 1500, 2000, TASK_RELAX) 
					bWdResetRagdoll = TRUE
				ENDIF
			ENDIF
			
			IF bWdResetRagdoll
				IF IS_PED_RAGDOLL(gangEnemy[0].ped)
					IF IS_PED_RUNNING_RAGDOLL_TASK(gangEnemy[0].ped)
						RESET_PED_RAGDOLL_TIMER(gangEnemy[0].ped)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
ENDPROC

VEHICLE_INDEX vehRecord
PROC RECORD_CHASE_TRIGGER_AND_TRAFFIC()

	IF e_section_stage = SECTION_STAGE_SETUP
		MODEL_NAMES model_trigger_car = HEXER
		REQUEST_MODEL(model_trigger_car)
		VECTOR v_gang_car_start_pos = << 1956.3414, 4634.0684, 39.6488 >>
		IF HAS_MODEL_LOADED(model_trigger_car)
			vehRecord = CREATE_VEHICLE(model_trigger_car, v_gang_car_start_pos,  67.5848)
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRecord)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRecord)
			ENDIF
			
			INIT_UBER_RECORDING("Trev1ChaseDw")
			
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
	
	IF e_section_stage = SECTION_STAGE_RUNNING
		UPDATE_UBER_RECORDING()
	ENDIF
ENDPROC	

INT iWdChaseProg
INT iWdPLaybackUberProg
BOOL bWdInitUberPlayBack
BOOL bWdRunUberPlayback
BOOL bWdStopPlaybackUber
BOOL bWdRecordVan
BOOL bWdChaseUseSq
BOOL bWdChaseShowDebugLines
BOOL bWdChaseRestoreStage
BOOL bWdCreateTruckAndTrailer
BOOL bWdRecordTruckAndTrailer
BOOL bWdStopRecordTruckAndTrailer
BOOL bWdPlaybackTruckTrailer
BOOL bWdDoNewUberRec
PROC DW_DW_CHASE_WIDGET()
	SWITCH iWdChaseProg

		CASE 0
			SET_CURRENT_WIDGET_GROUP(widgetDebug)
				START_WIDGET_GROUP("Chase widget")
					ADD_WIDGET_BOOL("Init", bWdInitUberPlayBack)
					ADD_WIDGET_BOOL("Playback uber", bWdRunUberPlayback)
					ADD_WIDGET_BOOL("Stop playback", bWdStopPlaybackUber)
					ADD_WIDGET_FLOAT_SLIDER("Trailer trigger time", fTimeForUberTrailerAndTruck, 0.0, 1000000.0, 1.0)
					ADD_WIDGET_BOOL("Show debug lines", bWdChaseShowDebugLines)
					ADD_WIDGET_BOOL("Do new uber rec", bWdDoNewUberRec)
					START_WIDGET_GROUP("Van")
						ADD_WIDGET_BOOL("Record van", bWdRecordVan)
						ADD_WIDGET_BOOL("Use square to rec van", bWdChaseUseSq)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Truck and trailer")
						ADD_WIDGET_BOOL("Create ", bWdCreateTruckAndTrailer)
						ADD_WIDGET_BOOL("Record ", bWdRecordTruckAndTrailer)
						ADD_WIDGET_BOOL("Stop record", bWdStopRecordTruckAndTrailer) 
						ADD_WIDGET_BOOL("Playback", bWdPlaybackTruckTrailer)
					STOP_WIDGET_GROUP()
					ADD_WIDGET_BOOL("Create bike for player", bWdCreateBikeForPlayer)
					ADD_WIDGET_BOOL("Restore mission stage", bWdChaseRestoreStage)
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			
			fTimeForUberTrailerAndTruck = 127000.0
			iWdChaseProg++
		BREAK
		
		CASE 1
			IF bWdInitUberPlayBack
				eMissionStage = STAGE_TEST
				REQUEST_MODEL(HEXER)
				REQUEST_MODEL(GBURRITO)
				//REQUEST_MODEL(HAULER)
				REQUEST_MODEL(TRFLAT)
				IF HAS_MODEL_LOADED(GBURRITO)
				AND HAS_MODEL_LOADED(HEXER)
				//AND HAS_MODEL_LOADED(HAULER)
				AND HAS_MODEL_LOADED(TRFLAT)
					LOAD_SCENE(<< 1956.3361, 4634.0703, 39.6486 >>)
					IF NOT IS_ENTITY_DEAD(vehTrevor.veh)
						SET_ENTITY_COORDS(vehTrevor.veh, << 1969.5635, 4647.2998, 39.9306 >>)
						SET_ENTITY_HEADING(vehTrevor.veh, 118.0528)
						IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehTrevor.veh)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTrevor.veh)
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(vehBikeForUber)
						DELETE_VEHICLE(vehBikeForUber)
					ENDIF
					IF DOES_ENTITY_EXIST(vehTruck.veh)
						DELETE_VEHICLE(vehTruck.veh)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehBike[0].veh)
						DELETE_VEHICLE(vehBike[0].veh)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehBike[1].veh)
						DELETE_VEHICLE(vehBike[1].veh)
					ENDIF
					
					vehTruck.veh = CREATE_VEHICLE(GBURRITO,<< 1949.0426, 4638.0034, 39.5484 >>, 66.6823) 
					vehBike[0].veh =  CREATE_VEHICLE(HEXER, << 1962.8153, 4635.3926, 40.2410 >>, 140.0220)
					vehBike[1].veh =  CREATE_VEHICLE(HEXER, << 1960.5868, 4632.4321, 40.1918 >>, 58.5726)
					
					bWdInitUberPlayBack = FALSE
					iWdPLaybackUberProg = 0
				ENDIF
			ENDIF
			
			IF bWdCreateTruckAndTrailer
				bWdCreateTruckAndTrailer = FALSE
				LOAD_SCENE( << -198.5713, 4210.7661, 43.7708 >>)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehUberTruck[0])
					DELETE_VEHICLE(vehUberTruck[0])
				ENDIF
				IF DOES_ENTITY_EXIST(vehUberTruck[1])
					DELETE_VEHICLE(vehUberTruck[1])
				ENDIF
				vehUberTruck[0] = CREATE_VEHICLE(HAULER, << -198.5713, 4210.7661, 43.7708 >>, 328.8440)
				vehUberTruck[1] = CREATE_VEHICLE(TRFLAT, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehUberTruck[0], <<0.0, -10.0, 0.0>>), 328.8440)
				ATTACH_VEHICLE_TO_TRAILER(vehUberTruck[0], vehUberTruck[1])
				IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehUberTruck[0])
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehUberTruck[0])
				ENDIF
			ENDIF
			
			IF bWdRecordTruckAndTrailer
				IF NOT IS_ENTITY_DEAD(vehUberTruck[0])
					IF NOT IS_ENTITY_DEAD(vehUberTruck[1])
						START_RECORDING_VEHICLE(vehUberTruck[0],0,sAwUberRec, TRUE)
						START_RECORDING_VEHICLE(vehUberTruck[1],999,sAwUberRec, TRUE)
						PRINT_NOW("CRECSTART5", 300000, 1)
						bWdRecordTruckAndTrailer = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF bWdStopRecordTruckAndTrailer
				STOP_RECORDING_ALL_VEHICLES()
				CLEAR_PRINTS()
				PRINT_NOW("CRECSTOP5", 8000, 1)
				bWdStopRecordTruckAndTrailer = FALSE
			ENDIF
			
			IF bWdPlaybackTruckTrailer
				REQUEST_VEHICLE_RECORDING(202, sAwUberRec) //-- Truck
				REQUEST_VEHICLE_RECORDING(203, sAwUberRec) //-- Trailer for truck
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(202, sAwUberRec)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(203, sAwUberRec)
					IF NOT IS_ENTITY_DEAD(vehUberTruck[0])
						IF NOT IS_ENTITY_DEAD(vehUberTruck[1])
							START_PLAYBACK_RECORDED_VEHICLE(vehUberTruck[0], 202, sAwUberRec)
							START_PLAYBACK_RECORDED_VEHICLE(vehUberTruck[1], 203, sAwUberRec)
							bWdPlaybackTruckTrailer = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bWdRunUberPlayback
				IF iWdPLaybackUberProg = 0
					IF NOT IS_ENTITY_DEAD(vehTruck.veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh)
						ENDIF
						IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
							ENDIF
						ENDIF
						IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh)
							ENDIF
						ENDIF
						REQUEST_VEHICLE_RECORDING(200, sAwUberRec)
						REQUEST_VEHICLE_RECORDING(201, sAwUberRec)
						REQUEST_VEHICLE_RECORDING(204, sAwUberRec)
						REQUEST_VEHICLE_RECORDING(205, sAwUberRec)
						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(200, sAwUberRec)
						AND HAS_VEHICLE_RECORDING_BEEN_LOADED(201, sAwUberRec)
						AND HAS_VEHICLE_RECORDING_BEEN_LOADED(204, sAwUberRec)
						AND HAS_VEHICLE_RECORDING_BEEN_LOADED(205, sAwUberRec)
							CLEANUP_UBER_PLAYBACK()
							INITIALISE_UBER_PLAYBACK(sAwUberRec, 200)
							INIT_UBER_DATA()
							iTruckAndTrailerSetpieceProg = 0
							START_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh, 200, sAwUberRec)
							
							IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
								START_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh, 204, sAwUberRec)
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
								START_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh, 205, sAwUberRec)
							ENDIF
							iWdPLaybackUberProg++
						ENDIF
					ENDIF
				ELIF iWdPLaybackUberProg = 1
					IF NOT IS_ENTITY_DEAD(vehTruck.veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
							CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehTruck.veh, PLAYER_PED_ID(), fUberSpeed)
							fUberSpeed = 1.0
							UPDATE_UBER_PLAYBACK(vehTruck.veh, fUberSpeed)
						ELSE 
							iWdPLaybackUberProg = 99
							bWdRunUberPlayback = FALSE
						ENDIF
						
						MANAGE_TRUCK_AND_TRAILER_UBER_SETPIECE(fUberSpeed)
					ENDIF
				ENDIF
				
				FLOAT f
				IF NOT IS_ENTITY_DEAD(vehTruck.veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
						IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
								f = GET_TIME_POSITION_IN_RECORDING(vehTruck.veh)
								
								
								IF bPausePlayback
									STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
									START_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh, 204, sAwUberRec)
									
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh, f)
									PAUSE_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
								ELSE
									UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
								ENDIF
								
								
								IF NOT IS_ENTITY_DEAD(vehUberTruck[0])
									IF bPausePlayback
										IF f < fTimeForUberTrailerAndTruck
											iTruckAndTrailerSetpieceProg = 0
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bWdStopPlaybackUber
				bWdStopPlaybackUber = FALSE
				iWdPLaybackUberProg = 99
				iTruckAndTrailerSetpieceProg = 0
				bWdRunUberPlayback = FALSE
				IF NOT IS_ENTITY_DEAD(vehBikeForUber)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehBikeForUber)
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehTruck.veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh)
				ENDIF
				IF DOES_ENTITY_EXIST(vehUberTruck[0])
					DELETE_VEHICLE(vehUberTruck[0])
				ENDIF
				IF DOES_ENTITY_EXIST(vehUberTruck[1])
					DELETE_VEHICLE(vehUberTruck[1])
				ENDIF
				CLEANUP_UBER_PLAYBACK()
			ENDIF
			
			IF bWdChaseUseSq
				IF NOT IS_ENTITY_DEAD(vehTruck.veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
						IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X))
							STOP_RECORDING_VEHICLE(vehTruck.veh)
							PRINT_NOW("CRECSTOP5", 8000, 1)
						ENDIF
					ELSE
						IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X))
							bWdRecordVan = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bWdRecordVan
				
			ENDIF
			
			IF bWdChaseShowDebugLines
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				IF NOT IS_ENTITY_DEAD(vehTruck.veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
						VECTOR vBikePos
						VECTOR vBikeOffPos
						vBikePos = GET_ENTITY_COORDS(vehTruck.veh, FALSE)
						vBikeOffPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck.veh, <<0.0, 20.0, 0.0>>)
						DRAW_DEBUG_LINE(vBikePos, vBikeOffPos)
						DRAW_DEBUG_SPHERE(vBikeOffPos, 0.5)
					ENDIF
				ENDIF
			ENDIF
			
			IF bWdCreateBikeForPlayer
				
				bWdCreateBikeForPlayer = FALSE
				REQUEST_MODEL(HEXER)
				WHILE NOT HAS_MODEL_LOADED(HEXER)
					WAIT(0)
				ENDWHILE
				VEHICLE_INDEX vehTemp
				vehTemp = CREATE_VEHICLE(HEXER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 1.0, 0.0>>), 0.0)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTemp)
			ENDIF
			
			IF bWdRunUberPlayback 
				IF NOT IS_ENTITY_DEAD(vehTruck.veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh, RDM_WHOLELINE)
					ENDIF
				ENDIF
			ENDIF
			
			IF bWdChaseRestoreStage
				bWdRunUberPlayback = FALSE
				bWdChaseRestoreStage = FALSE
				eMissionStage = STAGE_GET_TO_CHASE
			ENDIF
			
			IF bWdDoNewUberRec
				RECORD_CHASE_TRIGGER_AND_TRAFFIC()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iWdDogProg
BOOL bWdCreateDog
BOOL bWdSetDOgHatePlayer
PED_INDEX dogFight[5]
PROC DO_DW_DOG_WIDGET()
	SWITCH iWdDogProg
		CASE 0
			SET_CURRENT_WIDGET_GROUP(widgetDebug)
				START_WIDGET_GROUP("Dog widget")
					ADD_WIDGET_BOOL("Create dog", bWdCreateDog)
					ADD_WIDGET_BOOL("Set hate player", bWdSetDOgHatePlayer)
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			iWdDogProg++
		BREAK
		
		CASE 1
			IF bWdCreateDog
				REQUEST_MODEL(A_C_ROTTWEILER)
				IF HAS_MODEL_LOADED(A_C_ROTTWEILER)
					bWdCreateDog = FALSE
					IF DOES_ENTITY_EXIST(dogFight[0])
						DELETE_PED(dogFight[0])
					ENDIF
					dogFight[0] = CREATE_PED (PEDTYPE_MISSION, A_C_ROTTWEILER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 3.0, 0.0>>), 0.0)
					ADD_DEADPOOL_TRIGGER(dogFight[0], TRV1_KILLS)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(dogFight[0], TRUE)
				ENDIF
			ENDIF
			
			IF bWdSetDOgHatePlayer
				bWdSetDOgHatePlayer = FALSE
				IF NOT IS_ENTITY_DEAD(dogFight[0])
					SET_PED_RELATIONSHIP_GROUP_HASH(dogFight[0], grpEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
					TASK_COMBAT_PED(dogFight[0], PLAYER_PED_ID())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

TEXT_WIDGET_ID wdObjName
BOOL bWdObjCreate, bWdObjUpdatePos, bWdObjOutputPos
VECTOR vWdObjPos, vWdObjRot, vWdObjPosCur, vWdObjRotCur
INT iWdObjProg
FLOAT fWdObjHeadCur, fWdObjHead
OBJECT_INDEX oWdObj
PROC DO_DW_OBJECT_WIDGET()
	SWITCH iWdObjProg
		CASE 0 
			SET_CURRENT_WIDGET_GROUP(widgetDebug)
				START_WIDGET_GROUP("Object widget")
					wdObjName = ADD_TEXT_WIDGET("Object ID")
					ADD_WIDGET_BOOL("Create ", bWdObjCreate)
					ADD_WIDGET_VECTOR_SLIDER("Pos", vWdObjPos, -4000.0, 4000.0, 0.05)	
					ADD_WIDGET_VECTOR_SLIDER("Rot", vWdObjRot, -360.0, 360.0, 0.5)
				//	ADD_WIDGET_FLOAT_SLIDER("Heading ", fWdObjHead, -360.0, 360.0, 0.5)			
					ADD_WIDGET_BOOL("Update pos ", bWdObjUpdatePos)
					ADD_WIDGET_BOOL("Output pos ", bWdObjOutputPos)
				STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
			
			SET_CONTENTS_OF_TEXT_WIDGET(wdObjName, "PROP_GASCYL_01A")// 
			fWdObjHeadCur = 0.0
			vWdObjPosCur = << 0.0, 0.0, 0.0>>
			iWdObjProg++
		BREAK
		
		CASE 1
			VECTOR v
			MODEL_NAMES mWdObj
			IF bWdObjCreate
				bWdObjCreate = FALSE
				mWdObj = INT_TO_ENUM(model_names, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(wdObjName)))
				IF IS_MODEL_IN_CDIMAGE(mWdObj)
					REQUEST_MODEL(mWdObj)
					IF NOT HAS_MODEL_LOADED(mWdObj)
						REQUEST_MODEL(mWdObj)
						WHILE NOT HAS_MODEL_LOADED(mWdObj)
							WAIT(0)
						ENDWHILE
					ENDIF
					v = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 2.0, 0.0>>)
					FLOAT z
					GET_GROUND_Z_FOR_3D_COORD( v, z)
					v.z = z
					oWdObj = CREATE_OBJECT_NO_OFFSET(mWdObj, v)
					vWdObjPos = GET_ENTITY_COORDS(oWdObj, FALSE)
					fWdObjHead = GET_ENTITY_HEADING(oWdObj)
					vWdObjRot = GET_ENTITY_ROTATION(oWdObj)
					vWdObjPosCur = vWdObjPos
					fWdObjHeadCur = fWdObjHead
					vWdObjRotCur = vWdObjRot
				ENDIF
			ENDIF
			
			IF bWdObjUpdatePos
				IF (NOT ARE_VECTORS_EQUAL(vWdObjPos, vWdObjPosCur))
				OR (NOT ARE_VECTORS_EQUAL(vWdObjRot, vWdObjRotCur))
				OR (fWdObjHeadCur <> fWdObjHead)
					DW_PRINTS("Moving")
					SET_ENTITY_COORDS_NO_OFFSET(oWdObj, vWdObjPos)
					SET_ENTITY_HEADING(oWdObj, fWdObjHead)
					SET_ENTITY_ROTATION(oWdObj, vWdObjRot) 
					vWdObjPosCur = vWdObjPos
					fWdObjHeadCur = fWdObjHead
					vWdObjRotCur = vWdObjRot
				ENDIF	
			ENDIF
			
			IF bWdObjOutputPos
				bWdObjOutputPos = FALSE	
				PRINT_DW_VECTOR_TO_TEMP_FILE_WITH_STRING(vWdObjPos, "Pos...")
				PRINT_DW_VECTOR_TO_TEMP_FILE_WITH_STRING(vWdObjRot, "Rot...")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE:		Updates debug widgets
PROC UPDATE_WIDGETS()
	/*IF bUberRecord
		IF eMissionStage <> UBER_RECORD
			bStageSetup = FALSE
			eMissionStage = UBER_RECORD
		ENDIF
	ENDIF */
	DO_DW_DOG_WIDGET()
	DO_DW_OBJECT_WIDGET()
	DO_DW_RUNNER_WIDGET()
	DO_DW_GANG_WIDGET()
	DO_DW_GENERAL_WIDGET()
	DO_DW_EXPLOSION_WIDGET()
	DO_DW_BIKE_CONVOY_WIDGET(vehTrailerPark[15].veh,22)
	DW_DW_CHASE_WIDGET()
//	DO_DW_ROPE_ATTACH_WIDGET()
   	DO_RE_RECORDING_WIDGET(widgetDebug)
ENDPROC

#ENDIF

//PURPOS: Sets the gameplay camera'heading relative to the world coordinate system (not relative to the players local heading)
PROC SET_GAMEPLAY_CAM_WORLD_HEADING(FLOAT fHeading = 0.0)
       SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading - GET_ENTITY_HEADING(PLAYER_PED_ID()))
ENDPROC


//PURPOSE: moves the player and their car to a new location
PROC WARP_PLAYER_AND_CAR(VECTOR Location, FLOAT Heading)
	//IF IS_SCREEN_FADED_OUT()
		LOAD_SCENE(Location)
//	ENDIF
	PRINTSTRING("Warp Player and PED")PRINTNL()
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
			ELSE
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(budRon.ped)		
			SET_ENTITY_INVINCIBLE(budRon.ped, TRUE)
			CLEAR_PED_TASKS(budRon.ped)
			IF NOT IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
				IF NOT IS_PED_IN_ANY_VEHICLE(budRon.ped)
					SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh, VS_FRONT_RIGHT)
				ELSE
					SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		IF eMissionStage < STAGE_CARAVAN_SMASH
			IF NOT IS_PED_INJURED(budWade.ped)		
				SET_ENTITY_INVINCIBLE(budWade.ped, TRUE)
				CLEAR_PED_TASKS(budRon.ped)
				IF NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					IF NOT IS_PED_IN_ANY_VEHICLE(budWade.ped)
						SET_PED_INTO_VEHICLE(budWade.ped,vehTrevor.veh, VS_BACK_LEFT)
					ELSE
						SET_PED_INTO_VEHICLE(budWade.ped,vehTrevor.veh, VS_BACK_LEFT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),Location)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), Heading)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
		//FIX FOR CAM WHEN IN COVER
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		CLEAR_AREA(Location, 300.0, TRUE)
		IF NOT IS_PED_INJURED(budRon.ped)		
			SET_ENTITY_INVINCIBLE(budRon.ped, FALSE)		
		ENDIF
		IF NOT IS_PED_INJURED(budWade.ped)		
			SET_ENTITY_INVINCIBLE(budWade.ped, FALSE)		
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: put people in their cars
PROC WARP_CHASE_PEDS()
	IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
		IF NOT IS_PED_INJURED(cmbtChase[0].ped)
			IF NOT IS_PED_IN_VEHICLE(cmbtChase[0].ped,vehTruck.veh)
				CLEAR_PED_TASKS(cmbtChase[0].ped)
				SET_PED_INTO_VEHICLE(cmbtChase[0].ped, vehTruck.veh, VS_DRIVER)
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(cmbtChase[1].ped)	
			DELETE_PED(cmbtChase[1].ped)
//			IF NOT IS_PED_IN_VEHICLE(cmbtChase[1].ped,vehTruck.veh)
//				CLEAR_PED_TASKS(cmbtChase[1].ped)
//				SET_PED_INTO_VEHICLE(cmbtChase[1].ped, vehTruck.veh, VS_FRONT_RIGHT)
//			ENDIF
		ENDIF
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
		AND eMissionStage <> STAGE_CARAVAN_PARK_FIGHT
		AND eMissionStage <> STAGE_CHASE
			START_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh,vehTruck.recNumber, sCarRec )
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh, 300.0)
		ENDIF
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
		IF NOT IS_PED_INJURED(cmbtChase[2].ped)
			IF NOT IS_PED_IN_VEHICLE(cmbtChase[2].ped,vehBike[0].veh)
				CLEAR_PED_TASKS(cmbtChase[2].ped)
				SET_PED_INTO_VEHICLE(cmbtChase[2].ped, vehBike[0].veh, VS_DRIVER)
			ENDIF
		ENDIF
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
		AND eMissionStage <> STAGE_CARAVAN_PARK_FIGHT
		AND eMissionStage <> STAGE_CHASE
			START_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh,vehBike[0].recNumber, sCarRec )
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh, 300.0)
		ENDIF
	ENDIF	
	IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
		
		IF NOT IS_PED_INJURED(cmbtChase[3].ped)
			IF NOT IS_PED_IN_VEHICLE(cmbtChase[3].ped,vehBike[1].veh)
				CLEAR_PED_TASKS(cmbtChase[3].ped)
				SET_PED_INTO_VEHICLE(cmbtChase[3].ped, vehBike[1].veh, VS_DRIVER)
			ENDIF
		ENDIF
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
		AND eMissionStage <> STAGE_CARAVAN_PARK_FIGHT
		AND eMissionStage <> STAGE_CHASE
			START_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh,vehBike[1].recNumber, sCarRec )
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh, 300.0)
		ENDIF
	ENDIF
ENDPROC

//Function that calculates the heading to point to a given coordinate
FUNC FLOAT fPointAtObjectHeading(VECTOR vCurrentPos,VECTOR vTargetPos)
	//calculate the heading to set the helicopter at
	IF vCurrentPos.X - vTargetPos.X > 0
		IF vCurrentPos.Y - vTargetPos.Y <= 0
			RETURN  (ATAN((vTargetPos.Y - vCurrentPos.Y)/(vTargetPos.X - vCurrentPos.X))+90)
		ELIF vCurrentPos.Y - vTargetPos.Y > 0
			RETURN  (ATAN((vTargetPos.Y - vCurrentPos.Y)/(vTargetPos.X - vCurrentPos.X))+90)
		ENDIF
	ELIF vCurrentPos.X - vTargetPos.X <= 0
		IF vCurrentPos.Y - vTargetPos.Y > 0
			RETURN  (ATAN((vTargetPos.Y - vCurrentPos.Y)/(vTargetPos.X - vCurrentPos.X))+270)
		ELIF vCurrentPos.Y - vTargetPos.Y <= 0
			RETURN  (ATAN((vTargetPos.Y - vCurrentPos.Y)/(vTargetPos.X - vCurrentPos.X))+270)
		ENDIF
	ENDIF
RETURN 0.0
ENDFUNC

//converts heading rotation into a directional vector
FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR v_rot)
      RETURN <<-SIN(v_rot.z) * COS(v_rot.x), COS(v_rot.z) * COS(v_rot.x), SIN(v_rot.x)>>
ENDFUNC

//Gets a perpinduclaur vector
FUNC VECTOR Vector2D_CrossProduct(VECTOR v)
	v = <<v.y, -v.x, 0>>
    RETURN v
ENDFUNC

//gets an offset a small distance away from the car, Ignoring rotation and grounds the z Coordinate
FUNC  VECTOR GET_OFFSET_FROM_TREVORS_CAR(BOOL side)
	VECTOR vReturn	 
	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//		FLOAT iHeading = GET_ENTITY_HEADING(vehTrevor.veh)
		VECTOR vPos = GET_ENTITY_COORDS(vehTrevor.veh, FALSE)
		vPos.z += 2.0 //put the z up a bit so you don't go through the map
		VECTOR vRot = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_ENTITY_ROTATION(vehTrevor.veh))
		IF side = TRUE//right
			vReturn = vPos + (vRot+vRot+vRot) + (Vector2D_CrossProduct(vRot) + Vector2D_CrossProduct(vRot) + Vector2D_CrossProduct(vRot) )//+ Vector2D_CrossProduct(vRot))
		ELSE//left
			vReturn = vPos + (vRot+vRot+vRot+vRot) - (Vector2D_CrossProduct(vRot) + Vector2D_CrossProduct(vRot))
		ENDIF
	ENDIF
	GET_GROUND_Z_FOR_3D_COORD(vReturn, vReturn.z)
	RETURN vReturn
ENDFUNC

PROC SETUP_TRAILER_DAMAGE(BOOL bDamaged = FALSE)
	IF bDamaged 
		IF NOT IS_ENTITY_DEAD(vehTrailer)
			SET_VEHICLE_EXTRA(vehTrailer,6,FALSE)
			SET_VEHICLE_EXTRA(vehTrailer,1,TRUE)
			PRINTSTRING("Trailer set as damaged")PRINTNL()	
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailer)
			SET_VEHICLE_EXTRA(vehTrailer,1,FALSE)
			SET_VEHICLE_EXTRA(vehTrailer,2,FALSE)
			SET_VEHICLE_EXTRA(vehTrailer,3,FALSE)
			SET_VEHICLE_EXTRA(vehTrailer,4,FALSE)
			SET_VEHICLE_EXTRA(vehTrailer,5,FALSE)
			SET_VEHICLE_EXTRA(vehTrailer,6,TRUE)
			PRINTSTRING("Trailer set as undamaged")PRINTNL()
		ENDIF
	ENDIF
ENDPROC

PROC SET_UP_WADE_COMPONENTS()
	IF DOES_ENTITY_EXIST(budWade.ped)
		IF NOT IS_PED_INJURED(budWade.ped)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_HAIR, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_LEG, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(budWade.ped, PED_COMP_JBIB, 0, 0)
		ENDIF
	ENDIF
ENDPROC

PROC SET_UP_RON_COMPONENTS()
	IF DOES_ENTITY_EXIST(budRon.ped)
		IF NOT IS_PED_INJURED(budRon.ped)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_HAIR, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_LEG, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(budRon.ped, PED_COMP_JBIB, 0, 0)
			SET_PED_PROP_INDEX(budRon.ped, ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(budRon.ped, ANCHOR_EYES, 0)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SETUP_MISSION_REQUIREMENT(MISSION_REQUIREMENT eRequirement, VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)

	IF NOT IS_VECTOR_ZERO(vPos)
	
	ENDIF
	
	IF fHeading !=0
	
	ENDIF

	SWITCH eRequirement

		CASE REQ_TREVORS_TRUCK
			REQUEST_MODEL(BODHI2)
			IF HAS_MODEL_LOADED(BODHI2)
				IF NOT DOES_ENTITY_EXIST(vehTrevor.veh)
					REQUEST_VEHICLE_ASSET(BODHI2)
					CREATE_PLAYER_VEHICLE(vehTrevor.veh, CHAR_TREVOR ,vehTrevor.startLoc, vehTrevor.startHead, TRUE)
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
						SET_VEHICLE_WHEELS_CAN_BREAK(vehTrevor.veh,FALSE)
						//SET_VEHICLE_TYRES_CAN_BURST(vehTrevor.veh,FALSE)
					ENDIF
				ELSE
					REQUEST_VEHICLE_ASSET(BODHI2)
					IF HAS_VEHICLE_ASSET_LOADED(BODHI2)
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							SET_VEHICLE_WHEELS_CAN_BREAK(vehTrevor.veh,FALSE)
							//SET_VEHICLE_TYRES_CAN_BURST(vehTrevor.veh,FALSE)
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehTrevor.veh,FALSE)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehTrevor.veh,SC_DOOR_FRONT_LEFT,FALSE)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehTrevor.veh,SC_DOOR_FRONT_RIGHT,FALSE)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehTrevor.veh,TRUE)
							SET_VEHICLE_HAS_STRONG_AXLES(vehTrevor.veh,TRUE)
//							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehTrevor.veh,TRV1_CAR_DAMAGE)
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehTrevor.veh)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_TREVOR
			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],CHAR_TREVOR,vPos,fHeading)
				#IF IS_DEBUG_BUILD 
					PRINTSTRING("TREVOR CREATED")PRINTLN()
				#ENDIF
			ELSE
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],FALSE)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRV1_DAMAGE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK

		CASE REQ_RON
			IF NOT DOES_ENTITY_EXIST(budRon.ped)
				IF CREATE_NPC_PED_ON_FOOT(budRon.ped, CHAR_RON, budRon.startLoc, budRon.startHead)
					IF NOT IS_PED_INJURED(budRon.ped)
						SET_PED_CAN_BE_TARGETTED(budRon.ped, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(budRon.ped, RELGROUPHASH_PLAYER)
						ADD_PED_FOR_DIALOGUE(structConvoPeds,3,budRon.ped,"NervousRon")
						SET_PED_CAN_BE_DRAGGED_OUT(budRon.ped,FALSE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(budRon.ped,TRUE)
						SET_UP_RON_COMPONENTS()
						#IF IS_DEBUG_BUILD 
							PRINTLN("@@@@@@@@@ RON CREATED 1 @@@@@@@@@")PRINTLN()
						#ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF structConvoPeds.PedInfo[3].Index != budRon.ped
					SET_UP_BUDDY(budRon,VS_FRONT_RIGHT)
					ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
				ENDIF
				#IF IS_DEBUG_BUILD 
					PRINTLN("@@@@@@@@@@@ RON CREATED 2 @@@@@@@@@")PRINTLN()
				#ENDIF
				RETURN TRUE
			ENDIF
			
			IF bJustRequestAssets
				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_RON))
			ENDIF
			
		BREAK
		
		CASE REQ_WADE
			IF NOT DOES_ENTITY_EXIST(budWade.ped)
				IF CREATE_NPC_PED_ON_FOOT(budWade.ped, CHAR_WADE, budWade.startLoc, budWade.startHead)
					IF NOT IS_PED_INJURED(budWade.ped)
						SET_PED_CAN_BE_TARGETTED(budWade.ped, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(budWade.ped, RELGROUPHASH_PLAYER)
						ADD_PED_FOR_DIALOGUE(structConvoPeds,4,budWade.ped,"Wade")
						SET_PED_CAN_BE_DRAGGED_OUT(budWade.ped,FALSE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(budWade.ped,TRUE)
						SET_RAGDOLL_BLOCKING_FLAGS(budWade.ped,RBF_BULLET_IMPACT)
						SET_ENTITY_LOD_DIST(budWade.ped,50)
						SET_UP_BUDDY(budWade,VS_BACK_LEFT)
						#IF IS_DEBUG_BUILD 
							PRINTSTRING("WADE CREATED")PRINTLN()
						#ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF structConvoPeds.PedInfo[4].Index != budWade.ped
					SET_UP_BUDDY(budWade,VS_BACK_LEFT)
					ADD_PED_FOR_DIALOGUE(structConvoPeds, 4,budWade.ped,"Wade")
				ENDIF
				#IF IS_DEBUG_BUILD 
					PRINTSTRING("WADE CREATED")PRINTLN()
				#ENDIF
				IF NOT IS_PED_INJURED(budWade.ped)
					SET_RAGDOLL_BLOCKING_FLAGS(budWade.ped,RBF_BULLET_IMPACT)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_ASHLEY
			IF NOT DOES_ENTITY_EXIST(pedAshley)
				IF CREATE_NPC_PED_ON_FOOT(pedAshley, CHAR_ASHLEY, <<2017.58, 3827.92, 31.78>>,-86.69)
					SET_PED_CAN_BE_TARGETTED(pedAshley, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedAshley, RELGROUPHASH_PLAYER)
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedAshley)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedAshley, RELGROUPHASH_PLAYER)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_JOHNNY
			IF NOT DOES_ENTITY_EXIST(pedJohnny)
				REQUEST_MODEL(IG_JOHNNYKLEBITZ)
				IF HAS_MODEL_LOADED(IG_JOHNNYKLEBITZ)
					pedJohnny = CREATE_PED(PEDTYPE_MISSION,IG_JOHNNYKLEBITZ,<<2017.58, 3827.92, 31.78>>,-86.69)
					ADD_DEADPOOL_TRIGGER(pedJohnny, TRV1_KILLS)
					SET_PED_CAN_BE_TARGETTED(pedJohnny, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJohnny,TRUE)
					//SET_PED_COMPONENT_VARIATION(pedJohnny,PED_COMP_HEAD,1,0)
					//SCRIPT_ASSERT("JOHNNY GOT CREATED")
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedJohnny)
					//SCRIPT_ASSERT("JOHNNY GOT CREATED")
					//SET_PED_COMPONENT_VARIATION(pedJohnny,PED_COMP_HEAD,1,0)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_TERRY
			IF NOT DOES_ENTITY_EXIST(cmbtChase[2].ped)
				REQUEST_MODEL(IG_TERRY)
				IF HAS_MODEL_LOADED(IG_TERRY)
					cmbtChase[2].ped = CREATE_PED(PEDTYPE_MISSION,IG_TERRY,cmbtChase[2].startLoc,cmbtChase[2].startHead)
					ADD_DEADPOOL_TRIGGER(cmbtChase[2].ped, TRV1_KILLS)
					SET_PED_CAN_BE_DRAGGED_OUT(cmbtChase[2].ped,FALSE)
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_CLAY
			IF NOT DOES_ENTITY_EXIST(cmbtChase[3].ped)
				REQUEST_MODEL(IG_CLAY)
				IF HAS_MODEL_LOADED(IG_CLAY)
					cmbtChase[3].ped = CREATE_PED(PEDTYPE_MISSION,IG_CLAY,cmbtChase[3].startLoc,cmbtChase[3].startHead)
					ADD_DEADPOOL_TRIGGER(cmbtChase[3].ped, TRV1_KILLS)
					SET_PED_CAN_BE_DRAGGED_OUT(cmbtChase[3].ped,FALSE)
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_CLAY_BIKE
			IF NOT DOES_ENTITY_EXIST(vehBike[0].veh)
				REQUEST_MODEL(HEXER)
				IF HAS_MODEL_LOADED(HEXER)
					vehBike[0].veh=CREATE_VEHICLE(vehBike[0].model, vehBike[0].startLoc, vehBike[0].startHead)
					SET_VEHICLE_COLOURS(vehBike[0].veh,0,0)
					SET_VEHICLE_DIRT_LEVEL(vehBike[0].veh,15.0) 
					SET_HORN_ENABLED(vehBike[0].veh,FALSE)
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_TERRY_BIKE
			IF NOT DOES_ENTITY_EXIST(vehBike[1].veh)
				REQUEST_MODEL(HEXER)
				IF HAS_MODEL_LOADED(HEXER)
					vehBike[1].veh=CREATE_VEHICLE(vehBike[1].model, vehBike[1].startLoc, vehBike[1].startHead)
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[1].veh,7)
					SET_VEHICLE_DIRT_LEVEL(vehBike[1].veh,15.0) 
					SET_HORN_ENABLED(vehBike[1].veh,FALSE)
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_ORTEGA
			IF NOT DOES_ENTITY_EXIST(npcOrtega.ped)
				IF CREATE_NPC_PED_ON_FOOT(npcOrtega.ped, CHAR_ORTEGA, npcOrtega.startLoc,npcOrtega.startHead)
					REMOVE_PED_FOR_DIALOGUE(structConvoPeds,7)
					ADD_PED_FOR_DIALOGUE(structConvoPeds,7,npcOrtega.ped,"Ortega")
					FREEZE_ENTITY_POSITION(npcOrtega.ped,TRUE)
					SET_PED_CONFIG_FLAG(npcOrtega.ped,PCF_DisableExplosionReactions,TRUE)
					ADD_DEADPOOL_TRIGGER(npcOrtega.ped, TRV1_KILLS)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(npcOrtega.ped,TRUE)
					SET_PED_COMPONENT_VARIATION(npcOrtega.ped, PED_COMP_SPECIAL, 1, 0, 0) //(accs)
					PRINTSTRING("MADE REQ_ORTEGA")PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(npcOrtega.ped)
					SET_PED_CONFIG_FLAG(npcOrtega.ped,PCF_DisableExplosionReactions,TRUE)
					FREEZE_ENTITY_POSITION(npcOrtega.ped,TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(npcOrtega.ped,TRUE)
					ADD_PED_FOR_DIALOGUE(structConvoPeds,7,npcOrtega.ped,"Ortega")
					PRINTSTRING("MADE REQ_ORTEGA")PRINTNL()
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_PLEE_ANIMS
			REQUEST_ANIM_DICT("misstrevor1")
			IF HAS_ANIM_DICT_LOADED("misstrevor1")
				PRINTSTRING("MADE REQ_PLEE_ANIMS")PRINTNL()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_ORTEGAS_TRAILER
			IF NOT DOES_ENTITY_EXIST(vehTrailer)
				REQUEST_MODEL(PROPTRAILER)
				IF HAS_MODEL_LOADED(PROPTRAILER)
					vehTrailer = CREATE_VEHICLE(PROPTRAILER,<< -51.8943, 3110.9661, 23.2366 >>,44.8533)
					SET_ENTITY_COORDS(vehTrailer,<< -51.8943, 3110.9661, 23.2366 >>)
					SET_ENTITY_HEADING(vehTrailer,44.8533)
					SET_ENTITY_VISIBLE(vehTrailer,TRUE)
					SETUP_TRAILER_DAMAGE(TRUE)
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehTrailer)
					SET_ENTITY_COORDS(vehTrailer,<< -51.8943, 3110.9661, 23.2366 >>)
					SET_ENTITY_HEADING(vehTrailer,44.8533)
					SET_ENTITY_VISIBLE(vehTrailer,TRUE)
					PRINTSTRING("MADE ORTEGAS TRAILER")PRINTNL()
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_TRAILER_OIL_DRUM
			IF NOT DOES_ENTITY_EXIST(oOilDrum)
				REQUEST_MODEL(prop_watercrate_01)
				IF HAS_MODEL_LOADED(prop_watercrate_01)
					oOilDrum = CREATE_OBJECT(prop_watercrate_01,<<37.52, 3731.39, 38.62>>)
					SET_ENTITY_COORDS(oOilDrum,<<37.52, 3731.39, 38.62>>)
					SET_ENTITY_ROTATION(oOilDrum,<<-0.37, -5.54, -43.64>>)
					SET_ENTITY_VISIBLE(oOilDrum,TRUE)
					PRINTSTRING("MADE OIL DRUM")PRINTLN()
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_TRAILER_INTERIOR
			IF interior_trev_trailer = NULL
				interior_trev_trailer = GET_INTERIOR_AT_COORDS(<<1974.4364, 3819.3320, 32.4363>>)
				bLoad = FALSE
			ENDIF

			IF IS_INTERIOR_READY(interior_trev_trailer)
				IF bLoad = FALSE
					//LOAD_SCENE(<<1974.4364, 3819.3320, 32.4363>>)
					bLoad = TRUE
				ELSE
					RETURN TRUE
				ENDIF
			ELSE
				PIN_INTERIOR_IN_MEMORY(interior_trev_trailer)
			ENDIF
		BREAK
		
		CASE REQ_WADES_BIKE
			IF NOT DOES_ENTITY_EXIST(vehWadesBike)
				REQUEST_MODEL(HEXER)
				IF HAS_MODEL_LOADED(HEXER)
					IF WOULD_ENTITY_BE_OCCLUDED(HEXER, <<28.8999, 3616.1060, 38.9847>>)
						vehWadesBike = CREATE_VEHICLE(HEXER,<<28.8999, 3616.1060, 38.9847>>,92.0400)
						SET_ENTITY_COORDS(vehWadesBike,<<28.8999, 3616.1060, 38.9847>>)
						SET_ENTITY_HEADING(vehWadesBike,92.0400)
						SET_ENTITY_VISIBLE(vehWadesBike,TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehWadesBike,FALSE)
						SET_LABEL_AS_TRIGGERED("WADE BIKE RIGHT",TRUE)
						RETURN TRUE
					ELSE
						vehWadesBike = CREATE_VEHICLE(HEXER,<<104.0991, 3592.6980, 38.7392>>,103.0751)
						SET_ENTITY_COORDS(vehWadesBike,<<104.0991, 3592.6980, 38.7392>>)
						SET_ENTITY_HEADING(vehWadesBike,103.0751)
						SET_ENTITY_VISIBLE(vehWadesBike,TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehWadesBike,FALSE)
						SET_LABEL_AS_TRIGGERED("WADE BIKE LEFT",TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_JERRY_CAN
			IF NOT DOES_PICKUP_EXIST(PickupJerryCan)
				PickupJerryCan = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_PETROLCAN, <<-51.76, 3088.96, 26.98>>, <<-101.64, -0.08, -22.76>>, enum_to_int(PLACEMENT_FLAG_MAP))
				RETURN TRUE
			ENDIF
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC



PROC SETUP_TRAILER()
	IF bSetupCaravan = TRUE
		
		IF bDoModelSwap = FALSE
			PRINTSTRING("Swapping Model")PRINTNL()
			REMOVE_IPL("TRV1_Trail_end")
			REQUEST_IPL("TRV1_Trail_start")
			CREATE_MODEL_HIDE(<< -24.722, 3032.973, 40.336 >> , 20, Prop_Trailr_Base_static, TRUE)
			
			IF DOES_ENTITY_EXIST(vehTrailer)
				DELETE_VEHICLE(vehTrailer)
			ENDIF
			SETTIMERA(0)
			bDoModelSwap = TRUE
		ENDIF

		
		IF HAS_MODEL_LOADED(PROPTRAILER)
			IF bDoModelSwap = TRUE
				//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -24.722, 3032.973, 40.336 >>,<<20,20,6>> )
					IF NOT DOES_ENTITY_EXIST(vehTrailer)
						PRINTSTRING("Making trailer")PRINTNL()
						//away from site
						vehTrailer = CREATE_VEHICLE(PROPTRAILER,<< -24.780, 3032.923, 40.376 >>,25.000 ,FALSE,FALSE)
						//SET_ENTITY_COORDS_NO_OFFSET(vehTrailer,<< -24.780, 3032.923, 40.376 >>)
						SET_ENTITY_COORDS_NO_OFFSET(vehTrailer, <<-24.7800, 3032.9231, 40.3760>>)
						SET_ENTITY_ROTATION(vehTrailer, <<0.0073, 0.0009, 25.0000>>)
						SET_ENTITY_QUATERNION(vehTrailer, 0.0001, 0.0000, 0.2164, 0.9763)
						SET_ENTITY_INVINCIBLE(vehTrailer,TRUE)
						SET_ENTITY_COLLISION(vehTrailer,TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrailer,FALSE)
						//SET_VEHICLE_ON_GROUND_PROPERLY(vehTrailer)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehTrailer,TRV1_TRAILER_DAMAGE)
						FREEZE_ENTITY_POSITION(vehTrailer,TRUE)
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							SET_VEHICLE_STRONG(vehTrailer,TRUE)
						ENDIF
						
						SETUP_TRAILER_DAMAGE(FALSE)
						// bSetupCaravan = FALSE
					ENDIF
				//ENDIF
			ENDIF
		ELSE
			REQUEST_MODEL(PROPTRAILER)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -24.722, 3032.973, 40.336 >>,<<30,30,6>> )
				OR eMissionStage = STAGE_CARAVAN_SMASH
					IF DOES_ENTITY_EXIST(vehTrailer)
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							PRINTSTRING("Setting trailer collision")PRINTNL()
							SET_ENTITY_COLLISION(vehTrailer,TRUE)
							FREEZE_ENTITY_POSITION(vehTrailer,FALSE)
							bSetupCaravan = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF

ENDPROC

//PURPOSE: moves the player and budRon in to the correct postion for the kill

PROC manageSkip()	
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_FAIL")
		IF TRIGGER_MUSIC_EVENT("TRV1_FAIL")
			SET_LABEL_AS_TRIGGERED("TRV1_FAIL",TRUE)
		ENDIF
	ENDIF
	
	IF eMissionStage > STAGE_CUTSCENE_CHASE_START
		IF NOT HAS_LABEL_BEEN_TRIGGERED("BLOOD")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.657,0.566,126.360,0.07,3,0,"cs_trev1_blood")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.589,0.266,0,0.01,2,0,"cs_trev1_blood")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.612,0.269,126.360,0.22,2,0,"cs_trev1_blood")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.709,0.396,126.360,0.00,1,0,"cs_trev1_blood")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.582,0.383,200.360,0.1,2,0,"cs_trev1_blood")
			APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.582,0.383,111.360,0.05,2,0,"cs_trev1_blood")
			SET_LABEL_AS_TRIGGERED("BLOOD",TRUE)
		ENDIF
	ENDIF
	
	CLEAR_FOCUS()
	CLEAN_UP_AUDIO_SCENES()
	REMOVE_SCENARIO_BLOCKING_AREAS()
	UNLOCK_MINIMAP_ANGLE()
	
	RENDER_SCRIPT_CAMS(FALSE,FALSE)
	IF DOES_CAM_EXIST(cutscene_cam)
		DESTROY_CAM(cutscene_cam)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),FALSE,-1)
	ENDIF
	bDoFails = FALSE
	bDoFailLine = FALSE

	TOGGLE_STEALTH_RADAR(FALSE)
	IF eMissionStage < STAGE_DRIVE_TO_CARAVAN
		SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_NORMAL)
	ELSE
		SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_CLEANUP)
	ENDIF
	SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_A, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_B, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_C, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_D, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_E, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA, BUILDINGSTATE_NORMAL)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(HEXER,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GBURRITO,TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_01,TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_02,TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_03,TRUE)
	
	ADD_SCENARIO_BLOCKING_AREA(vTrailerPark + <<100,100,100>>, vTrailerPark - <<100,100,100>>)
	SET_NUMBER_OF_PARKED_VEHICLES(0)
	CLEAR_PED_WETNESS(PLAYER_PED_ID())
	CLEAR_PRINTS()
	iBuddyProgress = 0
	iWadeProgress = 0
	bTrevorSHoutsAtFleeingBikers = FALSE
	KILL_ANY_CONVERSATION()
	Set_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS, TRUE)
	DISABLE_TAXI_HAILING(TRUE)
	
	WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK,<<0,0,0>>)
		WAIT(0)
	ENDWHILE
	
	PRINTLN("@@@@@@@@@@@@@ SETUP_MISSION_REQUIREMENT 1 @@@@@@@@@@@@@@")
	WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<<0,0,0>>)
		WAIT(0)
	ENDWHILE
	
	IF eMissionStage < STAGE_DRIVE_TO_CARAVAN
		WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_WADE,<<0,0,0>>)
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF eMissionStage = STAGE_KILL_ORTEGA
		WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_ORTEGA,<<0,0,0>>)
		OR NOT SETUP_MISSION_REQUIREMENT(REQ_ORTEGAS_TRAILER,<<0,0,0>>)
		OR NOT SETUP_MISSION_REQUIREMENT(REQ_PLEE_ANIMS,<<0,0,0>>)
			WAIT(0)
		ENDWHILE
	ENDIF
	
//	IF eMissionStage = STAGE_DROP_OFF_RON
//		WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_ORTEGAS_TRAILER,<<0,0,0>>)
//			WAIT(0)
//		ENDWHILE
//	ENDIF
	
	IF eMissionStage = STAGE_CARAVAN_SMASH
		WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_ORTEGA,<< -40.6843, 3103.7644, 24.8673 >>, 160.9775)
			WAIT(0)
		ENDWHILE
	ENDIF
	
//	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//		MODIFY_VEHICLE_TOP_SPEED(vehTrevor.veh,100)
//	ENDIF
	
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
	
//	SCRIPT_ASSERT("Doing skip")
	//Set up the budRon if he is not dead.
	IF NOT IS_PED_INJURED(budRon.ped)
		 SET_UP_BUDDY(budRon)
		 ADD_PED_FOR_DIALOGUE(structConvoPeds,3,budRon.ped,"NervousRon")
	ENDIF
	
	IF NOT IS_PED_INJURED(budWade.ped)
		 SET_UP_BUDDY(budWade,VS_BACK_LEFT)
		 ADD_PED_FOR_DIALOGUE(structConvoPeds,4,budWade.ped,"Wade")
	ENDIF
	
	iWadeBackSeat = 0
	bSetUpStageData = FALSE
	
	SWITCH eMissionStage
		CASE STAGE_INIT_MISSION
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST()
		BREAK
		CASE STAGE_GET_TO_CHASE
			iJohnnyAndAshley = 0
			bAssignWadeTask = FALSE
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_ASHLEY, <<0,0,0>>)
			OR NOT SETUP_MISSION_REQUIREMENT(REQ_JOHNNY, <<0,0,0>>)
				WAIT(0)
			ENDWHILE
			PRINTSTRING("*** **** *Loading STAGE_GET_TO_CHASE ")PRINTNL()
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST()
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_GET_TO_CHASE)
			CLEAR_AREA(<<1983.4471, 3826.6763, 31.3778>>,50,TRUE)
			
			IF NOT IS_ENTITY_DEAD(budRon.ped)
				SET_ENTITY_COORDS(budRon.ped, <<2000.46, 3831.18, 31.29>>)
				SET_ENTITY_HEADING(budRon.ped, 90.19)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(budRon.ped,TRUE)
				SET_UP_RON_COMPONENTS()
			ENDIF

			IF NOT IS_ENTITY_DEAD(budWade.ped)
				SET_ENTITY_COORDS(budWade.ped, <<1999.89, 3829.58, 31.30>>)
				SET_ENTITY_HEADING(budWade.ped, 91.42)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(budWade.ped,TRUE)
				SET_UP_WADE_COMPONENTS()
			ENDIF
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP()
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<1995.97, 3829.84, 32.27>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 100.81)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(),TRUE)
				ENDIF				
			ENDIF

			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_NORMAL)
		BREAK
		CASE STAGE_CUTSCENE_CHASE_START
			REQUEST_CUTSCENE("TRV_1_MCS_1_P1")
			PRINTSTRING("*** **** *Loading STAGE_CUTSCENE_CHASE_START ")PRINTNL()
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnGangVan)
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			SET_MODEL_AS_NEEDED(Asset.mnLost1)
			SET_MODEL_AS_NEEDED(Asset.mnTerry)
			SET_MODEL_AS_NEEDED(Asset.mnClay)
			MANAGE_LOADING_NEED_LIST()
			
			REQUEST_MODEL(Asset.mnTerry)
			REQUEST_MODEL(Asset.mnClay)
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_CUTSCENE_CHASE_START)
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_COORDS(vehTrevor.veh,vStageStart)
				SET_ENTITY_HEADING(vehTrevor.veh,fStartHeading)
			ENDIF
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP()
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					WARP_PLAYER_AND_CAR(vStageStart,fStartHeading )
				ENDIF				
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_PED_INTO_VEHICLE(budWade.ped,vehTrevor.veh,VS_BACK_LEFT)
			ENDIF
			
			//Turn the car engine on so player can drive quickly if the stage has been skipped to.
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_ENGINE_ON(vehTrevor.veh, TRUE, TRUE)
			ENDIF
			SET_UP_RON_COMPONENTS()
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_NORMAL)
		BREAK
		CASE STAGE_CHASE
			
			PRINTSTRING("*** **** *Loading STAGE_CHASE ")PRINTNL()
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnGangVan)
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			SET_MODEL_AS_NEEDED(Asset.mnLost1)
			SET_MODEL_AS_NEEDED(Asset.mnTerry)
			SET_MODEL_AS_NEEDED(Asset.mnClay)
			PRINTLN("@@@@@@@@@@@@@ SETUP_MISSION_REQUIREMENT 2 @@@@@@@@@@@@@@")
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,<<0,0,0>>)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_WADE,<<0,0,0>>)
				WAIT(0)
			ENDWHILE
			
			SET_UP_RON_COMPONENTS()
			
			MANAGE_LOADING_NEED_LIST()
			REQUEST_MODEL(Asset.mnTerry)
			REQUEST_MODEL(Asset.mnClay)

			IF bDoNewShootout
				REQUEST_MODEL(REBEL)
				
				REQUEST_VEHICLE_RECORDING(200, sAwUberRec) // Trigger car
				REQUEST_VEHICLE_RECORDING(201, sAwUberRec) // Van
						
				REQUEST_VEHICLE_RECORDING(204, sAwUberRec) //-- Bike 0
				REQUEST_VEHICLE_RECORDING(205, sAwUberRec) //-- Bike 1
				REQUEST_VEHICLE_RECORDING(1, "Trevor1dw")
				
				WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(200, sAwUberRec)
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(201, sAwUberRec)
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(204, sAwUberRec)
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(205, sAwUberRec)
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Trevor1dw")
					WAIT(0)
				ENDWHILE
			ENDIF
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_CHASE)
		
			WHILE NOT CREATE_VEHICLE_STRUCT(vehTruck)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_CLAY_BIKE, <<0,0,0>>)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TERRY_BIKE, <<0,0,0>>)
				WAIT(0)
			ENDWHILE
			
			IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
				SET_VEHICLE_DIRT_LEVEL(vehTruck.veh,15)
			ENDIF

			SetUpTruck()
			
			//IF bKilledBikersDuringChase = FALSE
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TERRY,<<0,0,0>>)
					WAIT(0)
				ENDWHILE
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_CLAY,<<0,0,0>>)
					WAIT(0)		
				ENDWHILE
			//ENDIF

			IF bKilledBikersDuringChase = FALSE
				IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
					SET_ENTITY_HEALTH(vehBike[0].veh, 4000)
					SET_VEHICLE_TYRES_CAN_BURST(vehBike[0].veh,TRUE)
					SET_ENTITY_QUATERNION(vehBike[0].veh, 0.0118, 0.0096, 0.5264, 0.8501)
					SET_ENTITY_PROOFS(vehBike[0].veh, FALSE, FALSE, FALSE, TRUE, FALSE)
				ENDIF				
				IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
					SET_ENTITY_HEALTH(vehBike[1].veh, 4000)
					SET_VEHICLE_TYRES_CAN_BURST(vehBike[1].veh,TRUE)
					SET_ENTITY_QUATERNION(vehBike[1].veh, 0.0099, 0.0088, 0.5793, 0.8150)
					SET_ENTITY_PROOFS(vehBike[1].veh, FALSE, FALSE, FALSE, TRUE, FALSE)
				ENDIF
			ENDIF
				
			//Set up Peds
			IF bKilledBikersDuringChase = FALSE
				CREATE_CHASE_ENEMIES()
			ELSE
				FOR iCounter = 0 TO 2
					REQUEST_MODEL(cmbtChase[iCounter].model)
					IF HAS_MODEL_LOADED(cmbtChase[iCounter].model)
						IF NOT DOES_ENTITY_EXIST(cmbtChase[iCounter].ped)
							CREATE_ENEMY(cmbtChase[iCounter], FALSE)
						ENDIF
						IF iCounter > 1
							IF NOT IS_PED_INJURED(cmbtChase[iCounter].ped)
								SET_PED_SUFFERS_CRITICAL_HITS(cmbtChase[iCounter].ped, TRUE)
								SET_ENTITY_HEALTH(cmbtChase[iCounter].ped, 150)
								SET_PED_MAX_HEALTH(cmbtChase[iCounter].ped, 150)
							ENDIF
						ENDIF
					ELSE
						WHILE NOT HAS_MODEL_LOADED(cmbtChase[iCounter].model)
							REQUEST_MODEL(cmbtChase[iCounter].model)
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDFOR
			ENDIF
			
			FOR iCounter = 0 TO MAX_CHASE_PED-1
				IF NOT IS_PED_INJURED(cmbtChase[iCounter].ped)
					IF (iCounter = 0 OR iCounter = 1)
						SET_PED_SUFFERS_CRITICAL_HITS(cmbtChase[iCounter].ped ,FALSE)
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cmbtChase[iCounter].ped ,TRUE)
				ENDIF
			ENDFOR
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_COORDS(vehTrevor.veh,<<1965.3540, 4646.6006, 39.8213>>)
				SET_ENTITY_HEADING(vehTrevor.veh,99.27)
			ENDIF
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehTrevor.veh)
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					WARP_PLAYER_AND_CAR(<<1965.3540, 4646.6006, 39.8213>>,99.27 )
				ENDIF				
			ENDIF

			IF NOT IS_PED_INJURED(budRon.ped)
				CLEAR_PED_TASKS_IMMEDIATELY(budRon.ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_IN_ANY_VEHICLE(budRon.ped)
						SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh, VS_FRONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF	
			
			IF NOT IS_PED_INJURED(budWade.ped)
				CLEAR_PED_TASKS_IMMEDIATELY(budWade.ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_IN_ANY_VEHICLE(budWade.ped)
						SET_PED_INTO_VEHICLE(budWade.ped,vehTrevor.veh, VS_BACK_LEFT)
					ENDIF
				ENDIF
			ENDIF

			//Turn the car engine on so player can drive quickly if the stage has been skipped to.
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_ENGINE_ON(vehTrevor.veh, TRUE, TRUE)
				SET_VEHICLE_FORWARD_SPEED(vehTrevor.veh,1)
			ENDIF
			
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_NORMAL)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, FALSE)
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_CHASE_BIKERS_RT")
				IF TRIGGER_MUSIC_EVENT("TRV1_CHASE_BIKERS_RT")
					SET_LABEL_AS_TRIGGERED("TRV1_CHASE_BIKERS_RT",TRUE)
				ENDIF
			ENDIF
			
			bSkipped = TRUE
		BREAK
		
		CASE STAGE_CARAVAN_PARK_FIGHT
			PRINTSTRING("*** **** *Loading STAGE_CARAVAN_PARK_FIGHT ")PRINTNL()
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnLost1)
			SET_MODEL_AS_NEEDED(Asset.mnLost2)
			SET_MODEL_AS_NEEDED(Asset.mnGangVan)
			SET_MODEL_AS_NEEDED(Asset.mnTruck)
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			MANAGE_LOADING_NEED_LIST()
			REQUEST_VEHICLE_ASSET(HEXER)
			
			IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<< 58.4031, 3630.9597, 38.7482 >>)
				REMOVE_COVER_POINT(CoverPlayer)
			ENDIF

			IF bDoNewShootout
				REQUEST_MODEL(REBEL)
				REQUEST_MODEL(PROP_GASCYL_01A)
				REQUEST_VEHICLE_RECORDING(1, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(9, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(10, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(11, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(12, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(13, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(14, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(15, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(16, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(17, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(18, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(19, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(20, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(21, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(22, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(23, "Trevor1dw")
				
				WHILE NOT HAS_MODEL_LOADED(REBEL)
				OR NOT HAS_MODEL_LOADED(PROP_GASCYL_01A)
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(9, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(10, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(11, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(12, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(13, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(14, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(15, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(16, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(17, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(18, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(19, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(20, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(21, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(22, "Trevor1dw")
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(23, "Trevor1dw")
				OR NOT HAS_VEHICLE_ASSET_LOADED(HEXER)
					WAIT(0)
				ENDWHILE
			ENDIF
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_CARAVAN_PARK_FIGHT)
			
			WHILE NOT  CREATE_VEHICLE_STRUCT(vehTruck)
				WAIT(0)
			ENDWHILE
			
			IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
				SET_VEHICLE_DIRT_LEVEL(vehTruck.veh,15.0)
			ENDIF

			CREATE_CHASE_ENEMIES(FALSE)
			
			WARP_CHASE_PEDS()
			SetUpTruck()
			
			CLEANUP_UBER_PLAYBACK()
			
			FOR iCounter = 0 To MAX_CHASE_PED-1 //make them get out of the truck
				IF NOT IS_ENTITY_DEAD(cmbtChase[iCounter].ped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cmbtChase[iCounter].ped, TRUE)
				ENDIF
			ENDFOR
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_COORDS(vehTrevor.veh,vStageStart)
				SET_ENTITY_HEADING(vehTrevor.veh,fStartHeading)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
				SET_ENTITY_COORDS(vehTruck.veh,<< 51.74, 3662.95, 39.52 >> )
				SET_ENTITY_HEADING(vehTruck.veh,16.84)
			ENDIF
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP()
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					WARP_PLAYER_AND_CAR(<< 71.4078, 3619.4897, 38.7163 >>, 338.4296)
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 58.4031, 3630.9597, 38.7482 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),20.8123)
				ENDIF				
			ENDIF

			IF NOT IS_PED_INJURED(budRon.ped)
				CLEAR_PED_TASKS_IMMEDIATELY(budRon.ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_IN_ANY_VEHICLE(budRon.ped)
						SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh, VS_FRONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF	
			
			IF NOT IS_PED_INJURED(budWade.ped)
				CLEAR_PED_TASKS_IMMEDIATELY(budWade.ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_IN_ANY_VEHICLE(budWade.ped)
						SET_PED_INTO_VEHICLE(budWade.ped,vehTrevor.veh, VS_BACK_LEFT)
					ENDIF
				ENDIF
			ENDIF

			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_NORMAL)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, FALSE)
			bSkipped = TRUE
			SET_LABEL_AS_TRIGGERED("TRV1_AT_CARAVAN",TRUE)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_CARAVAN_RT")
				IF TRIGGER_MUSIC_EVENT("TRV1_CARAVAN_RT")
					SET_LABEL_AS_TRIGGERED("TRV1_CARAVAN_RT",TRUE)
				ENDIF
			ENDIF
			SET_UP_RON_COMPONENTS()
			iRoofGuy = 0
			iPetrolStage = 0
			bJustArrived = FALSE	
			bStageSetup = FALSE
		BREAK
		
		CASE STAGE_DRIVE_TO_CARAVAN

			REQUEST_MODEL(PROPTRAILER)
			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_ORTEGA))
			IF NOT HAS_MODEL_LOADED(PROPTRAILER)
				WAIT(0)
			ENDIF
			
			REMOVE_CUTSCENE()
			PRINTSTRING("*** **** *Loading STAGE_CARAVAN_SMASH ")PRINTNL()
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST()
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-17.546822,3042.975342,46.947666>> - <<20.000000,20.000000,10.000000>>, <<-17.546822,3042.975342,46.947666>> + <<20.000000,20.000000,10.000000>>,FALSE)
			CLEAR_AREA(<<-17.546822,3042.975342,46.947666>>,40.0, TRUE)
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_CARAVAN_SMASH)
			SETUP_TRAILER()
			
			IF DOES_ENTITY_EXIST(budWade.ped)
				DELETE_PED(budWade.ped)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_COORDS(vehTrevor.veh,<< 80.4212, 3607.0464, 38.6962 >>)
				SET_ENTITY_HEADING(vehTrevor.veh,189.0543)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
					SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
				ENDIF
			ENDIF
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehTrevor.veh)
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					WARP_PLAYER_AND_CAR(<< 80.4212, 3607.0464, 38.6962 >>, 189.0543)
				ENDIF				
			ENDIF

			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>)
			CLEAR_AREA(<<-11.4394, 3007.7793, 39.6286 >>, 40.0, TRUE)
			//WARP_PLAYER_AND_CAR(vStageStart,fStartHeading)
			
			//Turn the car engine on so player can drive quickly if the stage has been skipped to.
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_ENGINE_ON(vehTrevor.veh, TRUE, TRUE)
			ENDIF
			bSkipped = TRUE
			SET_UP_RON_COMPONENTS()
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_DRIVE_TRAILER_RT")
				IF TRIGGER_MUSIC_EVENT("TRV1_DRIVE_TRAILER_RT")
					SET_LABEL_AS_TRIGGERED("TRV1_DRIVE_TRAILER_RT",TRUE)
				ENDIF
			ENDIF
			
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_NORMAL)
		BREAK
		CASE STAGE_CARAVAN_SMASH
			
			REQUEST_MODEL(PROPTRAILER)
			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_ORTEGA))
			REQUEST_ANIM_DICT("misstrevor1")
			REQUEST_ANIM_DICT("misstrevor1ig_7")
			
			IF NOT HAS_MODEL_LOADED(PROPTRAILER)
			OR NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_ORTEGA))
				WAIT(0)
			ENDIF
			
//			IF NOT HAS_LABEL_BEEN_TRIGGERED("REQ_JERRY_CAN")
//				IF SETUP_MISSION_REQUIREMENT(REQ_JERRY_CAN,<<0,0,0>>)
//					SET_LABEL_AS_TRIGGERED("REQ_JERRY_CAN",TRUE)
//				ENDIF
//			ENDIF
			
			bSetupCaravan = TRUE	
			bDoModelSwap = FALSE
			
			REMOVE_CUTSCENE()
			PRINTSTRING("*** **** *Loading STAGE_CARAVAN_SMASH ")PRINTNL()
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST()
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_CARAVAN_SMASH)
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-17.546822,3042.975342,46.947666>> - <<20.000000,20.000000,10.000000>>, <<-17.546822,3042.975342,46.947666>> + <<20.000000,20.000000,10.000000>>,FALSE)
			CLEAR_AREA(<<-17.546822,3042.975342,46.947666>>,40.0, TRUE)

			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>)
			CLEAR_AREA(<<-20.1293, 3039.4690, 40.0023 >>, 40.0, TRUE)
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_COORDS_NO_OFFSET(vehTrevor.veh, <<-11.4373, 3007.7844, 40.6016>>)
				SET_ENTITY_ROTATION(vehTrevor.veh, <<-2.0516, -0.7819, 27.5497>>)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
					SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
				ENDIF
			ENDIF

			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehTrevor.veh)
			ELSE
				WARP_PLAYER_AND_CAR(<<-11.4373, 3007.7844, 40.6016>>, 180.4163)
				IF NOT IS_ENTITY_DEAD(vehTrevor.veh)
					SET_ENTITY_COORDS_NO_OFFSET(vehTrevor.veh, <<-11.4373, 3007.7844, 40.6016>>)
					SET_ENTITY_ROTATION(vehTrevor.veh, <<-2.0516, -0.7819, 27.5497>>)
					SET_VEH_RADIO_STATION(vehTrevor.veh, "OFF") 
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					ENDIF
				ENDIF			
			ENDIF
			
			SETUP_TRAILER()
			
			IF NOT DOES_ENTITY_EXIST(vehTrailer)
			AND bSetupCaravan = TRUE
				PRINTSTRING("*** **** *Hold script while setting up the trailer ")PRINTNL()
				WAIT(0)
			ENDIF

			//Turn the car engine on so player can drive quickly if the stage has been skipped to.
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_ENGINE_ON(vehTrevor.veh, TRUE, TRUE)
			ENDIF
			bStageSetup = FALSE
			bSkipped = TRUE
			SET_UP_RON_COMPONENTS()
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_PUSH_TRAILER_RT")
				IF TRIGGER_MUSIC_EVENT("TRV1_PUSH_TRAILER_RT")
					SET_LABEL_AS_TRIGGERED("TRV1_PUSH_TRAILER_RT",TRUE)
				ENDIF
			ENDIF
			
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_NORMAL)
		BREAK
		CASE STAGE_KILL_ORTEGA
			
			bOrtegaSpared = FALSE
			REMOVE_CUTSCENE()
			PRINTSTRING("*** **** *Loading STAGE_KILL_ORTEGA ")PRINTNL()
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST()
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_KILL_ORTEGA)

			REQUEST_COLLISION_AT_COORD(<< -44.7763, 3090.8545, 26.7873 >>)
			REQUEST_COLLISION_AT_COORD(<< -39.71, 3081.90, 30.35 >>)
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>)
			CLEAR_AREA(<<-11.4394, 3007.7793, 39.6286 >>, 40.0, TRUE)

			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_COORDS(vehTrevor.veh,<<-39.71, 3081.90, 30.35>>)
				SET_ENTITY_HEADING(vehTrevor.veh,341.54)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
				SET_ENTITY_VISIBLE(vehTrevor.veh,TRUE)
			ENDIF
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP()
			ELSE
				IF NOT IS_ENTITY_DEAD(vehTrevor.veh)
					WARP_PLAYER_AND_CAR(vStageStart,fStartHeading)
				ENDIF
				SET_ENTITY_COORDS(vehTrevor.veh,<<-39.71, 3081.90, 30.35>>)
				SET_ENTITY_HEADING(vehTrevor.veh,341.54)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-45.7971, 3094.8979, 25.9671>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 23.9393)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrailer)	
				SET_ENTITY_COORDS(vehTrailer,<< -51.8943, 3110.9661, 23.2366 >>)
				SET_ENTITY_HEADING(vehTrailer,44.8533)
				SETUP_TRAILER_DAMAGE(TRUE)
			ENDIF

			IF DOES_ENTITY_EXIST(budRon.ped)
				IF NOT IS_PED_INJURED(budRon.ped)
					SET_ENTITY_COORDS(budRon.ped,<< -44.7763, 3090.8545, 26.7873 >>)
					SET_ENTITY_COORDS (budRon.ped, <<-42.336231,3088.566650,28.480368>>)
					SET_ENTITY_HEADING(budRon.ped,34.380215)
				ENDIF
			ELSE
				SCRIPT_ASSERT("RON DOESN'T EXIST - YA DINGUS")
			ENDIF
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-17.546822,3042.975342,46.947666>> - <<20.000000,20.000000,10.000000>>, <<-17.546822,3042.975342,46.947666>> + <<20.000000,20.000000,10.000000>>,FALSE)
			CLEAR_AREA(<<-17.546822,3042.975342,46.947666>>,40.0, TRUE)
			CLEAR_AREA(<<-20.1293, 3039.4690, 40.0023 >>, 40.0, TRUE)
				
			IF NOT IS_PED_INJURED(npcOrtega.ped)
				SET_ENTITY_INVINCIBLE(npcOrtega.ped, FALSE)	
				CLEAR_PED_TASKS_IMMEDIATELY(npcOrtega.ped)
			ENDIF
			
			//SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(),TRUE)
			
			
			//SAFE_WARP_PLAYER_TO_COORDINATE( << -45.1059, 3094.8477, 25.9720 >>, 36.5137)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, FALSE)
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_ORTEGA_RT")
				IF TRIGGER_MUSIC_EVENT("TRV1_ORTEGA_RT")
					SET_LABEL_AS_TRIGGERED("TRV1_ORTEGA_RT",TRUE)
				ENDIF
			ENDIF
			SET_UP_RON_COMPONENTS()
			bSkipped = TRUE
		BREAK
		CASE STAGE_DROP_OFF_RON
			
			REMOVE_IPL("TRV1_Trail_start")
			REQUEST_IPL("TRV1_Trail_end")
			
			REMOVE_CUTSCENE()
			PRINTSTRING("*** **** *Loading  STAGE_DROP_OFF_RON ")PRINTNL()
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST()
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_DROP_OFF_RON)
			
			REQUEST_COLLISION_AT_COORD(<< -39.71, 3081.90, 30.35 >>)
			
			//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>, FALSE)
			//REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<  -30.8428, 3028.2161, 29.8939 >>,<<-10.1785, 3051.7859, 43.0208 >>)
			CLEAR_AREA(<<-11.4394, 3007.7793, 39.6286 >>, 40.0, TRUE)
			
			

			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_COORDS(vehTrevor.veh,<<-39.71, 3081.90, 30.35>>)
				SET_ENTITY_HEADING(vehTrevor.veh,342.3264)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
				SET_ENTITY_VISIBLE(vehTrevor.veh,TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
					SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
				ENDIF
			ENDIF
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehTrevor.veh)
			ELSE
				WARP_PLAYER_AND_CAR(vStageStart,fStartHeading )
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					SET_ENTITY_COORDS(vehTrevor.veh,<<-39.71, 3081.90, 30.35>>)
					SET_ENTITY_HEADING(vehTrevor.veh,341.54)
					SET_ENTITY_VISIBLE(vehTrevor.veh,TRUE)
				ENDIF
				LOAD_SCENE(<<-39.5698, 3081.8079, 29.3679>>)
			ENDIF

			IPL_GROUP_SWAP_START("CS3_05_water_grp1","CS3_05_water_grp2")

			//Turn the car engine on so player can drive quickly if the stage has been skipped to.
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_ENGINE_ON(vehTrevor.veh, TRUE, TRUE)
			ENDIF
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_DESTROYED)
			SET_UP_RON_COMPONENTS()
			//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-17.546822,3042.975342,46.947666>> - <<20.000000,20.000000,10.000000>>, <<-17.546822,3042.975342,46.947666>> + <<20.000000,20.000000,10.000000>>,FALSE)
			
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREV1_SMASHED_TRAILER,True)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREV1_SMASHED_TRAILER) 
			bSkipped = TRUE
		BREAK
		CASE STAGE_CUTSCENE_DROP_OFF_RON
			
			PRINTSTRING("*** **** *Loading STAGE_CUTSCENE_CHASE_START ")PRINTNL()
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST()
			
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_CUTSCENE_DROP_OFF_RON)
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_COORDS(vehTrevor.veh,vStageStart)
				SET_ENTITY_HEADING(vehTrevor.veh,fStartHeading)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
					SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
				ENDIF
			ENDIF
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehTrevor.veh)
			ELSE
				WARP_PLAYER_AND_CAR(vStageStart,fStartHeading )
			ENDIF
			
			//Turn the car engine on so player can drive quickly if the stage has been skipped to.
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_ENGINE_ON(vehTrevor.veh, TRUE, TRUE)
			ENDIF
			SET_UP_RON_COMPONENTS()
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_DESTROYED)
			bSkipped = TRUE
		BREAK
		CASE PASSED_MISSION
			MissionPassed()
		BREAK
	ENDSWITCH
	
	RESET_MISSION_STAGE_INTS_SKIP()
		
	#IF IS_DEBUG_BUILD
	//	CREATE_WIDGETS()
	#ENDIF
	
	IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
		PRINTSTRING("CUSTOM HEADING CAM")
		SET_GAMEPLAY_CAM_WORLD_HEADING(29.8107)
	ELIF eMissionStage = STAGE_GET_TO_CHASE
		SET_GAMEPLAY_CAM_WORLD_HEADING(-65)
	ELSE
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	ENDIF
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	
ENDPROC

//---------------------------------¦ NON-Stage FUNCTIONS AND PROCEDURES ¦-----------------------------------

//---------¦ GAMEPLAY ¦---------
//PURPOSE: Counts the number of people in wave 1
FUNC INT NUMBER_OF_ENEMIES_ALIVE_TRAILER()
	INT iEnemiesAlive = 0
	FOR iGroupCount = 0 TO NUMBER_OF_GROUPS-1
		FOR iEnemyCount = 0 TO NUMBER_OF_ENEMIES_IN_GROUP-1
			IF DOES_ENTITY_EXIST(sGroup[iGroupCount].enemy[iEnemyCount].ped)
				IF NOT IS_PED_INJURED(sGroup[iGroupCount].enemy[iEnemyCount].ped)
					iEnemiesAlive++
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	RETURN iEnemiesAlive
ENDFUNC
//PURPOSE: Counts the number of people in wave 1
FUNC INT NUMBER_OF_ENEMIES_ALIVE_EAST_WAVE()
	INT iEnemiesAlive = 0
	FOR iCounter = 0 TO MAX_ENEMY_EAST_WAVE_PED-1
		IF DOES_ENTITY_EXIST(cmbtEnemyEastWave[iCounter].ped)
			IF NOT IS_PED_INJURED(cmbtEnemyEastWave[iCounter].ped)
				iEnemiesAlive++
			ENDIF
		ENDIF
	ENDFOR
	RETURN iEnemiesAlive
ENDFUNC
//PURPOSE: Counts the number of people in wave 2
FUNC INT NUMBER_OF_ENEMIES_ALIVE_WEST_WAVE()
	INT iEnemiesAlive = 0
	FOR iCounter = 0 TO MAX_ENEMY_WEST_WAVE_PED-1
		IF DOES_ENTITY_EXIST(cmbtEnemyWestWave[iCounter].ped)
			IF NOT IS_PED_INJURED(cmbtEnemyWestWave[iCounter].ped)
				iEnemiesAlive++
			ENDIF
		ENDIF
	ENDFOR
	RETURN iEnemiesAlive
ENDFUNC


//Checks to see If a vehicle has been hit
FUNC BOOL HAVE_VEHICLES_COLLIDED(VEHICLE_INDEX VehPassed)
	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
		IF IS_VEHICLE_DRIVEABLE(VehPassed)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(VehPassed,vehTrevor.veh)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTrevor.veh, VehPassed)
			OR IS_ENTITY_TOUCHING_ENTITY(vehTrevor.veh, VehPassed)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: says when it is fine to start the chase
FUNC BOOL bStartChase()
	BOOL bStart[3]
	IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
			bStart[0] = TRUE
		ENDIF
	ELSE 
		bStart[0] = TRUE
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
			bStart[1] = TRUE
		ENDIF
	ELSE 
		bStart[1] = TRUE
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
			bStart[2] = TRUE
		ENDIF
	ELSE 
		bStart[2] = TRUE
	ENDIF
	IF bStart[0] = TRUE
	AND bStart[1] = TRUE
	AND bStart[2] = TRUE
		RETURN TRUE	
	ENDIF
	RETURN FALSE
ENDFUNC
//PURPOSE: Counts the number of times that a vehicle has been shot by the player.
FUNC INT NumberOfTimesShot(VEHICLE_INDEX aCar, INT index)
	IF IS_VEHICLE_DRIVEABLE(aCar)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(aCar, WEAPONTYPE_PUMPSHOTGUN)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(aCar, PLAYER_PED_ID())
			iShotsTaken[index]++
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(aCar)
			CLEAR_ENTITY_LAST_WEAPON_DAMAGE(aCar)
		ENDIF
	ENDIF
	RETURN iShotsTaken[index]
ENDFUNC
//PURPOSE: Forces a Ped to use it's beft weapon
PROC FORCE_BEST_WEAPON(PED_INDEX PedPassed)
	SET_PED_CURRENT_WEAPON_VISIBLE(PedPassed, TRUE)
	IF HAS_PED_GOT_WEAPON(PedPassed,WEAPONTYPE_PUMPSHOTGUN)
		SET_CURRENT_PED_WEAPON(PedPassed,WEAPONTYPE_PUMPSHOTGUN, TRUE)
	ELIF HAS_PED_GOT_WEAPON(PedPassed,WEAPONTYPE_ASSAULTRIFLE)
		SET_CURRENT_PED_WEAPON(PedPassed,WEAPONTYPE_ASSAULTRIFLE, TRUE)
	ELIF HAS_PED_GOT_WEAPON(PedPassed,WEAPONTYPE_SMG)
		SET_CURRENT_PED_WEAPON(PedPassed,WEAPONTYPE_SMG, TRUE)
	ELIF HAS_PED_GOT_WEAPON(PedPassed,WEAPONTYPE_GRENADELAUNCHER)
		SET_CURRENT_PED_WEAPON(PedPassed,WEAPONTYPE_GRENADELAUNCHER, TRUE)
	ELIF HAS_PED_GOT_WEAPON(PedPassed,WEAPONTYPE_HEAVYSNIPER)
		SET_CURRENT_PED_WEAPON(PedPassed,WEAPONTYPE_HEAVYSNIPER, TRUE)
	ELIF HAS_PED_GOT_WEAPON(PedPassed,WEAPONTYPE_COMBATPISTOL)
		SET_CURRENT_PED_WEAPON(PedPassed,WEAPONTYPE_COMBATPISTOL, TRUE)
	ELIF HAS_PED_GOT_WEAPON(PedPassed,WEAPONTYPE_PISTOL)
		SET_CURRENT_PED_WEAPON(PedPassed,WEAPONTYPE_PISTOL, TRUE)
	ENDIF
ENDPROC
//proc for making and postioning pipes on the back of the truck.
//#IF IS_DEBUG_BUILD
//PROC SAVE_VECTOR_TO_DEBUG_FILE_B(VECTOR vVector_to_save)
//    SAVE_STRING_TO_DEBUG_FILE("<<")
//    SAVE_FLOAT_TO_DEBUG_FILE(vVector_to_save.x)
//    SAVE_STRING_TO_DEBUG_FILE(", ")
//    SAVE_FLOAT_TO_DEBUG_FILE(vVector_to_save.y)
//    SAVE_STRING_TO_DEBUG_FILE(", ")
//    SAVE_FLOAT_TO_DEBUG_FILE(vVector_to_save.z)
//    SAVE_STRING_TO_DEBUG_FILE(">>")
//ENDPROC
//BOOL iMakePipe
//BOOL bPosPipe
//BOOL bOutPutToScript
//BOOL bDeletePipes
//INT iMakPipes
//INT iPipe
//VECTOR vMakePos
//VECTOR vMakeRot
//PROC MakePipes()
//	SWITCH iMakPipes
//		CASE 0
//			SET_CURRENT_WIDGET_GROUP(widgetDebug)
//			START_WIDGET_GROUP("MakePipes")
//				ADD_WIDGET_STRING("Make pipes")
//				ADD_WIDGET_BOOL("iMakePipe",iMakePipe)
//				ADD_WIDGET_BOOL("bPosPipe",bPosPipe)
//				ADD_WIDGET_BOOL("bDeletePipes",bDeletePipes)
//				ADD_WIDGET_INT_SLIDER("iPipe", iPipe, 0, 99, 1)
//				ADD_WIDGET_INT_SLIDER("iMakPipes", iMakPipes, 0, (MAX_FALL_OFF_PIPES-1), 1)
//				ADD_WIDGET_FLOAT_SLIDER("vMakePos.x", vMakePos.x, -9999.99, 9999.99, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("vMakePos.y", vMakePos.y, -9999.99, 9999.99, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("vMakePos.z", vMakePos.z, -9999.99, 9999.99, 0.01) 
//				ADD_WIDGET_FLOAT_SLIDER("vMakeRot.x", vMakeRot.x, -9999.99, 9999.99, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("vMakeRot.y", vMakeRot.y, -9999.99, 9999.99, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("vMakeRot.z", vMakeRot.z, -9999.99, 9999.99, 0.01)  
//				ADD_WIDGET_BOOL("bOutPutToScript",bOutPutToScript)
//			STOP_WIDGET_GROUP()
//			CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
//			iMakPipes++
//		BREAK
//		CASE 1 
//			IF iMakePipe=TRUE
//				FOR iCounter = 0 TO (MAX_FALL_OFF_PIPES-1)
//					IF NOT DOES_ENTITY_EXIST(pipeFallOff[iCounter].obj)
//						pipeFallOff[iCounter].obj = create_object(Asset.mnPipe, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck.veh, vMakePos))
//						ATTACH_ENTITY_TO_ENTITY(pipeFallOff[iCounter].obj, vehTruck.veh, -1, vMakePos,vMakeRot)
//						iPipe = iCounter
//						iMakePipe=FALSE
//						BREAK
//					ENDIF
//				ENDFOR
//			ENDIF
//			IF bPosPipe = TRUE
//				IF DOES_ENTITY_EXIST(pipeFallOff[iPipe].obj)
//					IF IS_ENTITY_ATTACHED(pipeFallOff[iPipe].obj)
//						DETACH_ENTITY(pipeFallOff[iPipe].obj,FALSE, FALSE)
//					ENDIF
//					ATTACH_ENTITY_TO_ENTITY(pipeFallOff[iPipe].obj, vehTruck.veh, -1, vMakePos,vMakeRot)
//				ENDIF
//			ENDIF
//			IF bOutPutToScript = TRUE
//				OPEN_DEBUG_FILE()
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("pipeFallOff[")SAVE_INT_TO_DEBUG_FILE(iPipe)SAVE_STRING_TO_DEBUG_FILE("].offset = ")SAVE_VECTOR_TO_DEBUG_FILE_B(vMakePos)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("pipeFallOff[")SAVE_INT_TO_DEBUG_FILE(iPipe)SAVE_STRING_TO_DEBUG_FILE("].rot = ")SAVE_VECTOR_TO_DEBUG_FILE_B(vMakeRot)SAVE_NEWLINE_TO_DEBUG_FILE()
//				CLOSE_DEBUG_FILE()		
//				bOutPutToScript = FALSE
//			ENDIF
//			IF bDeletePipes = TRUE
//				//FOR iCounter = 0 To (COUNT_OF(pipeFallOff) - 1)
//					IF DOES_ENTITY_EXIST(pipeFallOff[iPipe].obj) 
//						DELETE_ENTITY(pipeFallOff[iPipe].obj)
//					ENDIF
//				//ENDFOR 
//				bDeletePipes = FALSE
//			ENDIF
//		BREAK
//	ENDSWITCH
//ENDPROC
#IF IS_DEBUG_BUILD

//PURPOSE: does a script assert
PROC ScriptAssert(STRING errorPassed)
	PRINTSTRING("------- Trevor 1 Script Assert --------")PRINTNL()	
	PRINTSTRING("MISSION_STAGE = ")PRINTINT(ENUM_TO_INT(eMissionStage))PRINTNL()
	PRINTSTRING("iStageSection = ")PRINTINT(iStageSection)PRINTNL()
	PRINTSTRING("iPreLoadStage = ")PRINTINT(iPreLoadStage)PRINTNL()
	PRINTSTRING("iBuddyProgress = ")PRINTINT(iBuddyProgress)PRINTNL()
	PRINTSTRING("iEnemyCreationProc = ")PRINTINT(iEnemyCreationProc)PRINTNL()
	PRINTSTRING("iEnemyAIProg = ")PRINTINT(iEnemyAIProg)PRINTNL()
	PRINTSTRING("iEnemyEastWave = ")PRINTINT(iEnemyEastWave)PRINTNL()
	PRINTSTRING("iEnemyWestWave = ")PRINTINT(iEnemyWestWave)PRINTNL()
	SCRIPT_ASSERT(errorPassed)
ENDPROC
//PURPOSE: Prints a line so it's easy to find my db output in logs
PROC DBOutPut()
		PRINTSTRING("**** *Trevor 1 DB output    -  ")PRINTNL()
ENDPROC
//PURPOSE: Prints thigs when they change value
PROC Where_are_we()
	IF iMissionStageDB <> ENUM_TO_INT(eMissionStage)
		iMissionStageDB = ENUM_TO_INT(eMissionStage)
		DBOutPut()
		PRINTSTRING("MISSION_STAGE = ")PRINTINT(iMissionStageDB)PRINTNL()
	ENDIF	
	IF iProgressDB <> iStageSection
		iProgressDB = iStageSection
		DBOutPut()
		PRINTSTRING("iStageSection = ")PRINTINT(iStageSection)PRINTNL()
	ENDIF
	IF iPreLoadStageDB <> iPreLoadStage
		iPreLoadStageDB = iPreLoadStage
		DBOutPut()
		PRINTSTRING("iStageSection = ")PRINTINT(iPreLoadStage)PRINTNL()
	ENDIF
	IF iBuddyProgressDB <> iBuddyProgress
		iBuddyProgressDB = iBuddyProgress
		DBOutPut()
		PRINTSTRING("iBuddyProgress = ")PRINTINT(iBuddyProgress)PRINTNL()
	ENDIF
	IF iEnemyCreationProcDB <> iEnemyCreationProc
		iEnemyCreationProcDB = iEnemyCreationProc
		DBOutPut()
		PRINTSTRING("iEnemyCreationProc = ")PRINTINT(iEnemyCreationProc)PRINTNL()
	ENDIF
	IF iEnemyAIProgDB <> iEnemyAIProg
		iEnemyAIProgDB = iEnemyAIProg
		DBOutPut()
		PRINTSTRING("iEnemyAIProg = ")PRINTINT(iEnemyAIProg)PRINTNL()
	ENDIF
	IF iEnemyEastWaveDB <> iEnemyEastWave
		iEnemyEastWaveDB = iEnemyEastWave
		DBOutPut()
		PRINTSTRING("iEnemyAIProg = ")PRINTINT(iEnemyEastWave)PRINTNL()
	ENDIF
	IF iEnemyWestWaveDB <> iEnemyWestWave
		iEnemyWestWaveDB = iEnemyWestWave
		DBOutPut()
		PRINTSTRING("iEnemyAIProg = ")PRINTINT(iEnemyWestWave)PRINTNL()
	ENDIF
ENDPROC
#ENDIF	


//PURPOSE: Controls the J and P skips and the stage selector menu
#IF IS_DEBUG_BUILD
	PROC CONTROL_SKIPS()
		// Control the Jskips during the gun fight
        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			//only deal with the J and P skips if the caravan park fight is ongoing
			IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
				bDoJandP = FALSE 
				FOR iGroupCount = 0 TO NUMBER_OF_GROUPS-1
					FOR iEnemyCount = 0 TO NUMBER_OF_ENEMIES_IN_GROUP-1
						IF DOES_BLIP_EXIST(sGroup[iGroupCount].enemy[iEnemyCount].blip)
							REMOVE_BLIP(sGroup[iGroupCount].enemy[iEnemyCount].blip)
						ENDIF
						IF DOES_ENTITY_EXIST(sGroup[iGroupCount].enemy[iEnemyCount].ped)
							DELETE_PED(sGroup[iGroupCount].enemy[iEnemyCount].ped)
						ENDIF
					ENDFOR
				ENDFOR
				IF iEnemyEastWave > 3
					FOR iCounter = 0 TO (MAX_ENEMY_EAST_WAVE_PED-1)
						IF DOES_BLIP_EXIST(cmbtEnemyEastWave[iCounter].blip)
							REMOVE_BLIP(cmbtEnemyEastWave[iCounter].blip)
						ENDIF
						IF DOES_ENTITY_EXIST(cmbtEnemyEastWave[iCounter].ped)
							DELETE_PED(cmbtEnemyEastWave[iCounter].ped)
						ENDIF
					ENDFOR
				ELSE 
					WARP_PLAYER_AND_CAR( << 27.1567, 3718.9138, 38.6810 >>, 0.0)
				ENDIF
				IF iEnemyWestWave > 3
					FOR iCounter = 0 TO (MAX_ENEMY_WEST_WAVE_PED-1)
						IF DOES_BLIP_EXIST(cmbtEnemyWestWave[iCounter].blip)
							REMOVE_BLIP(cmbtEnemyWestWave[iCounter].blip)
						ENDIF
						IF DOES_ENTITY_EXIST(cmbtEnemyWestWave[iCounter].ped)
							DELETE_PED(cmbtEnemyWestWave[iCounter].ped)
						ENDIF
					ENDFOR	
				ENDIF
			ELSE 
				bDoJandP = TRUE
			ENDIF
        ENDIF
        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			bDoJandP = TRUE
			IF bDoJandP //Fudge to get to compile for now.
			ENDIF
		ENDIF
		// Checks if the keyboard key S has been pressed and if sets the current mission state to complete
        IF IS_KEYBOARD_KEY_PRESSED (KEY_S)
			//eMissionStage = PASSED_MISSION
			PRINT_NOW(Asset.godPass, DEFAULT_GOD_TEXT_TIME, 0)
			MissionPassed()
        ENDIF
        // Checks if the keyboard key F has been pressed and if so sets the current mission state to failed
        IF IS_KEYBOARD_KEY_PRESSED (KEY_F)
			MissionFailed(FAIL_GENERIC)
        ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_T)
			eMissionStage = STAGE_TEST
		ENDIF
	ENDPROC
#ENDIF
//PURPOSE: Returns a unit vector
FUNC VECTOR GET_UNIT_VECTOR(VECTOR v_start,VECTOR v_end, INT iMag = 1)
	VECTOR v_unit
	INT i
	v_unit = v_end - v_start / VMAG(v_end - v_start)
	FOR i = 1 To iMag
		v_unit.x += v_unit.x
		v_unit.y += v_unit.y
		v_unit.z += v_unit.z
	ENDFOR
	RETURN v_unit
ENDFUNC


//PURPOSE: Returns true when the car in the recording has go on longer than the time passed. 
FUNC FLOAT GET_TIME_IN_PLAYBACK()
	IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
			RETURN(GET_TIME_POSITION_IN_RECORDING(vehTruck.veh))
		ENDIF
	ENDIF
	RETURN(0.0)
ENDFUNC
//PURPOSE: Returns true when the car in the recording has go on longer than the time passed. 
FUNC BOOL IS_TIME_IN_PLAYBACK_MORE_THAN(FLOAT fTimePassed)
	IF fTimePassed > 0.0
		IF (GET_TIME_IN_PLAYBACK() >= fTimePassed)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC
FUNC FLOAT GET_INTERP_POINT_FLOAT(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
      RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC
FUNC VECTOR GET_INTERP_POINT_VECTOR(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
      RETURN <<GET_INTERP_POINT_FLOAT(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC
	
//PURPOSE: Checks to see if the player has passed a car in the car recording
FUNC BOOL PLAYER_PASSED_CAR (VEHICLE_INDEX VehPassed)
	VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	VECTOR vVehPassed
	IF IS_VEHICLE_DRIVEABLE(VehPassed)
		vVehPassed = GET_ENTITY_COORDS(VehPassed, FALSE)
	ELSE 
		RETURN FALSE	
	ENDIF
	//if the player has passed the chase cars and has not rounded the cornor of the lake/loch
	IF vPlayer.x < vVehPassed.x
	AND vPlayer.y > 4080.0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
//PURPOSE: Controls a vehicle during a chase
PROC CONTROL_CAR_DURING_CHASE(VEHICLE_INDEX &VehiclePassed, BLIP_INDEX &BlipPassed, FLOAT fPlayBackSpeedPassed)
	IF IS_VEHICLE_DRIVEABLE(VehiclePassed)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(VehiclePassed)
			IF NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(VehiclePassed))
				SET_PLAYBACK_SPEED(VehiclePassed,fPlayBackSpeedPassed)
			ELSE
				STOP_PLAYBACK_RECORDED_VEHICLE(VehiclePassed)
				IF DOES_BLIP_EXIST(BlipPassed)
					REMOVE_BLIP(BlipPassed)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
//PURPOSE: Removes a Ped From a vehicle
PROC REMOVE_PED_FROM_VEHICLE_STRUCT(VEH_STRUCT thisVehicleStruct)
	PED_INDEX Temp_PED
	IF thisVehicleStruct.bCrash = FALSE
		IF IS_VEHICLE_DRIVEABLE(thisVehicleStruct.veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisVehicleStruct.veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(thisVehicleStruct.veh)
			ENDIF
			IF NOT IS_VEHICLE_SEAT_FREE(thisVehicleStruct.veh)
				Temp_PED = GET_PED_IN_VEHICLE_SEAT(thisVehicleStruct.veh)
				IF NOT IS_PED_INJURED(Temp_PED)
					CLEAR_PED_TASKS_IMMEDIATELY(Temp_PED, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:PreLoads the next stage when the player get near to it.
FUNC BOOL PRE_LOAD_MISSION_STAGE()

	MISSION_STAGE eStageToLoad = INT_TO_ENUM(MISSION_STAGE, (ENUM_TO_INT(eMissionStage) + 1))
	
	SWITCH iPreLoadStage
		CASE 0
			IF eStageToLoad = STAGE_GET_TO_CHASE
			OR eStageToLoad = STAGE_CUTSCENE_CHASE_START
			OR eStageToLoad = STAGE_CHASE
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 2016.4196, 4642.4399, 40.1808 >> , <<PRELOAD_DISTANCE,PRELOAD_DISTANCE,PRELOAD_DISTANCE>>, FALSE )
					iPreLoadStage++
				ENDIF
			ELIF  eStageToLoad = STAGE_CARAVAN_PARK_FIGHT
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 80.2901, 3619.4456, 38.6997 >>, <<PRELOAD_DISTANCE,PRELOAD_DISTANCE,PRELOAD_DISTANCE>>, FALSE )
					iPreLoadStage++
				ENDIF
			ELIF eStageToLoad = STAGE_CARAVAN_SMASH
			OR eStageToLoad = STAGE_KILL_ORTEGA
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -30.5141, 3025.3369, 39.6005 >> , <<PRELOAD_DISTANCE,PRELOAD_DISTANCE,PRELOAD_DISTANCE>>, FALSE )
					iPreLoadStage++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF (eStageToLoad = STAGE_GET_TO_CHASE
			OR eStageToLoad = STAGE_CHASE
			OR eStageToLoad = STAGE_CUTSCENE_CHASE_START)
				iPreLoadStage++
				PRINTSTRING("PRE_LOAD_MISSION_STAGE TRUE")PRINTNL()
			ELIF (eStageToLoad = STAGE_KILL_ORTEGA
			OR eStageToLoad = STAGE_KILL_ORTEGA)
				REQUEST_ANIM_DICT(Asset.adBusted)
				IF HAS_ANIM_DICT_LOADED(Asset.adBusted)
					PRINTSTRING("PRE_LOAD_MISSION_STAGE TRUE")PRINTNL()
					iPreLoadStage++
				ENDIF
			ELSE
				PRINTSTRING("PRE_LOAD_MISSION_STAGE TRUE")PRINTNL()
				iPreLoadStage++
			ENDIF
		BREAK
		CASE 2
			RETURN (TRUE)
		BREAK
	ENDSWITCH
	
	RETURN (FALSE)
	
ENDFUNC
//PURPOSE:Makes the enemies.
FUNC BOOL CONTROL_ENEMY_CREATION()
	IF eMissionStage = STAGE_GET_TO_CHASE
		SWITCH iEnemyCreationProc
			CASE 0
				IF PRE_LOAD_MISSION_STAGE() = TRUE
					iEnemyCreationProc++						
				ENDIF
			BREAK
			CASE 1
				RETURN (TRUE)
			BREAK
		ENDSWITCH
	ELIF eMissionStage = STAGE_CHASE
		SWITCH iEnemyCreationProc
			CASE 0
				IF PRE_LOAD_MISSION_STAGE() = TRUE
					iEnemyCreationProc++
				ENDIF
			BREAK
			CASE 1
				RETURN TRUE
			BREAK
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC


//PURPOSE:Mark enemies as no longer needed and remove blips upon death.
PROC CONTROL_ENEMIES()
	//IF eMissionStage = MISSION_STAGE_GETTO_PARK	
	IF eMissionStage = STAGE_CHASE
		SWITCH iEnemyAIProg
			CASE 0  // Make every body angru with the player
				FOR iCounter = 0 TO 3
					IF NOT IS_PED_INJURED(cmbtChase[iCounter].ped)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cmbtChase[iCounter].ped, TRUE)
					ENDIF
				ENDFOR
				iEnemyAIProg++
			BREAK
		ENDSWITCH
		
		//Controll the vehicle blips		
		IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
				IF iCounter = 1
					IF NOT DOES_BLIP_EXIST(vehTruck.blip)
						vehTruck.blip = CREATE_BLIP_FOR_VEHICLE(vehTruck.veh, FALSE) 
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(vehTruck.blip)
				REMOVE_BLIP(vehTruck.blip)
			ENDIF
			IF NOT IS_PED_INJURED(cmbtChase[0].ped)
				SET_ENTITY_HEALTH(cmbtChase[0].ped, 0)
			ENDIF
			IF NOT IS_PED_INJURED(cmbtChase[1].ped)
				SET_ENTITY_HEALTH(cmbtChase[1].ped, 0)
			ENDIF
		ENDIF
		
		IF NOT IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
			IF DOES_BLIP_EXIST(vehBike[0].blip)
				REMOVE_BLIP(vehBike[0].blip)
			ENDIF
			IF NOT IS_PED_INJURED(cmbtChase[2].ped)
				SET_ENTITY_HEALTH(cmbtChase[2].ped, 0)
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(cmbtChase[2].ped)
				IF NOT IS_PED_SITTING_IN_VEHICLE(cmbtChase[2].ped,vehBike[0].veh)
					IF DOES_BLIP_EXIST(vehBike[0].blip)
						SET_ENTITY_HEALTH(cmbtChase[2].ped, 0)
						REMOVE_BLIP(vehBike[0].blip)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
			IF DOES_BLIP_EXIST(vehBike[1].blip)
				REMOVE_BLIP(vehBike[1].blip)
			ENDIF
			IF NOT IS_PED_INJURED(cmbtChase[3].ped)
				SET_ENTITY_HEALTH(cmbtChase[3].ped, 0)
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(cmbtChase[3].ped)
				IF NOT IS_PED_SITTING_IN_VEHICLE(cmbtChase[3].ped,vehBike[1].veh)
					IF DOES_BLIP_EXIST(vehBike[1].blip)
						SET_ENTITY_HEALTH(cmbtChase[3].ped, 0)
						REMOVE_BLIP(vehBike[1].blip)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC
//PURPOSE:Controls your mate
PROC CONTROL_BUDDY()
	IF NOT IS_PED_INJURED(budRon.ped)
		IF eMissionStage = STAGE_GET_TO_CHASE
			SWITCH iBuddyProgress
				CASE 0
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
					iBuddyProgress++
				BREAK
				
				CASE 1 
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF NOT IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
							//CLEAR_PED_TASKS_IMMEDIATELY(budRon.ped)
//							OPEN_SEQUENCE_TASK(seqSequence)
//								//make the ped run at the vehicle so then the locates heasder makes him get in. 
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrevor.veh, <<5.0, 0.0, 0.0>>), PEDMOVE_RUN)
//								TASK_ENTER_VEHICLE(NULL, vehTrevor.veh,DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
//							CLOSE_SEQUENCE_TASK(seqSequence)
//							TASK_PERFORM_SEQUENCE(budRon.ped,seqSequence)	
//							CLEAR_SEQUENCE_TASK(seqSequence)
							//SET_PED_COMBAT_ATTRIBUTES(budRon.ped, CA_LEAVE_VEHICLES, FALSE)
							SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(budRon.ped, TRUE)
						ENDIF
					ENDIF
					iBuddyProgress++
				BREAK
				
				CASE 2
//					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//						IF IS_PED_IN_VEHICLE(budRon.ped ,vehTrevor.veh)			
//						AND NOT IS_PED_GETTING_INTO_A_VEHICLE(budRon.ped)
//							iBuddyProgress++
//						ELIF  NOT IS_TASK_ONGOING(budRon.ped, SCRIPT_TASK_PERFORM_SEQUENCE)						
//							iBuddyProgress--
//						ENDIF
//					ENDIF
				BREAK
				CASE 3
				BREAK
			ENDSWITCH
		ELIF eMissionStage = STAGE_CHASE
			SWITCH iBuddyProgress
				CASE 0
					IF NOT IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
						SET_PED_AS_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
					ENDIF
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
					iBuddyProgress++
				BREAK
				CASE 1
				BREAK
			ENDSWITCH
		ELIF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
			SWITCH iBuddyProgress
				CASE 0  // idle case no actions assigned
					IF DOES_BLIP_EXIST(budRon.blip)
						REMOVE_BLIP(budRon.blip)
						//budRon.blip = CREATE_BLIP_FOR_PED(budRon.ped, FALSE)
					ENDIF
					/*IF NOT IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
						SET_PED_AS_GROUP_MEMBER(budRon.ped,PLAYER_GROUP_ID())
					ENDIF*/
					IF IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
						REMOVE_PED_FROM_GROUP(budRon.ped)
					ENDIF
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
					SET_PED_ACCURACY(budRon.ped, 100)
					SET_PED_COMBAT_ATTRIBUTES(budRon.ped, CA_LEAVE_VEHICLES, FALSE)
					iBuddyProgress++
				BREAK
				//REMOVED TO KEEP THE GUYS IN THE CAR
				CASE 1  // tell the budRon to get in the car or stand gaurd around it
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						FORCE_BEST_WEAPON(budRon.ped)
						IF IS_PED_IN_VEHICLE(budRon.ped, vehTrevor.veh)
							IF GET_ENTITY_SPEED(vehTrevor.veh) < 2.0
							
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(budRon.ped, CA_LEAVE_VEHICLES, TRUE)
								SET_ENTITY_LOAD_COLLISION_FLAG(budRon.ped,TRUE)
								SET_PED_TO_LOAD_COVER(budRon.ped,TRUE)
								REMOVE_PED_FROM_GROUP(budRon.ped)
								SET_PED_ACCURACY(budRon.ped,10)
								SET_ENTITY_HEALTH(budRon.ped,800)
								SET_PED_SUFFERS_CRITICAL_HITS(budRon.ped,FALSE)
								SET_PED_COMBAT_ATTRIBUTES(budRon.ped,CA_USE_COVER,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(budRon.ped,CA_JUST_SEEK_COVER,TRUE)
								SET_PED_SPHERE_DEFENSIVE_AREA(budRon.ped,GET_ENTITY_COORDS(vehTrevor.veh, FALSE),6.0)
								SET_PED_RELATIONSHIP_GROUP_HASH(budRon.ped, grpBuddies)
								SET_PED_CAN_BE_TARGETTED(budRon.ped,FALSE)
								//1005626
								SET_PED_CONFIG_FLAG(budRon.ped,PCF_RunFromFiresAndExplosions,TRUE)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
						
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,grpBuddies)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,RELGROUPHASH_PLAYER)
								SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(budRon.ped,FALSE,grpEnemies)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, FALSE)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpBuddies,grpEnemies)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,grpEnemies,grpBuddies)
								SET_PED_COMBAT_ATTRIBUTES(budRon.ped,CA_DISABLE_PIN_DOWN_OTHERS,TRUE) 
								
								//TASK_LEAVE_ANY_VEHICLE(budRon.ped,1000)
								
								OPEN_SEQUENCE_TASK(seqSequence)
									TASK_LEAVE_ANY_VEHICLE(NULL,500,ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,100)	
								CLOSE_SEQUENCE_TASK(seqSequence)
								TASK_PERFORM_SEQUENCE(budRon.ped,seqSequence)
								CLEAR_SEQUENCE_TASK(seqSequence)
								iBuddyProgress++
							ENDIF
						ELSE
							iBuddyProgress++
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(budRon.ped, CA_LEAVE_VEHICLES, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(budRon.ped, CA_BLIND_FIRE_IN_COVER, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(budRon.ped, CA_USE_COVER, TRUE)
						SET_PED_COMBAT_MOVEMENT(budRon.ped,CM_DEFENSIVE)
						SET_PED_ACCURACY(budRon.ped,10)
						
						IF NOT IS_PED_IN_ANY_VEHICLE(budRon.ped)
							FORCE_BEST_WEAPON(budRon.ped)
							
							IF NOT DOES_BLIP_EXIST(budRon.blip)
								budRon.blip = ADD_BLIP_FOR_ENTITY(budRon.ped)
								SET_BLIP_AS_FRIENDLY(budRon.blip,TRUE)
								SET_BLIP_SCALE(budRon.blip,0.5)
							ENDIF
							
//								
//							OPEN_SEQUENCE_TASK(seqSequence)
//								TASK_SEEK_COVER_FROM_POS(NULL,<<64.1259, 3651.5776, 38.4862>>,-1,TRUE)
//								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,30)
//							CLOSE_SEQUENCE_TASK(seqSequence)
//							TASK_PERFORM_SEQUENCE(budRon.ped,seqSequence)
//							CLEAR_SEQUENCE_TASK(seqSequence)
							iBuddyProgress++
						ENDIF
					ENDIF	
				BREAK
				
			ENDSWITCH
		ELIF eMissionStage = STAGE_CARAVAN_SMASH
			SWITCH iBuddyProgress
				CASE 0  // idle case no actions assigned
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF NOT IS_PED_IN_VEHICLE(budRon.ped, vehTrevor.veh)
							IF NOT IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
								SET_PED_AS_GROUP_MEMBER(budRon.ped,PLAYER_GROUP_ID())
							ENDIF
							OPEN_SEQUENCE_TASK(seqSequence)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrevor.veh, <<8.0, 0.0, 0.0>>), PEDMOVE_RUN)
							CLOSE_SEQUENCE_TASK(seqSequence)
							TASK_PERFORM_SEQUENCE(budRon.ped,seqSequence)
							CLEAR_SEQUENCE_TASK(seqSequence)
						ENDIF
					ENDIF
					iBuddyProgress++
				BREAK
				CASE 1  // tell the budRon to get in the car or stand gaurd around it
				BREAK
			ENDSWITCH
		ELIF eMissionStage = STAGE_KILL_ORTEGA
			SWITCH iBuddyProgress
				CASE 0  // idle case no actions assigned
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTrevor.veh)
						OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
							IF NOT IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
								SET_PED_AS_GROUP_MEMBER(budRon.ped,PLAYER_GROUP_ID())
							ENDIF
							iBuddyProgress++
						ENDIF
					ENDIF
				BREAK
				CASE 1  // tell the budRon to get in the car or stand gaurd around it
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	
	//WADE
	
	IF NOT IS_PED_INJURED(budWade.ped)
		IF eMissionStage = STAGE_GET_TO_CHASE
			SWITCH iWadeProgress
				CASE 0
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
					iWadeProgress++
				BREAK
				CASE 1 
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								//CLEAR_PED_TASKS_IMMEDIATELY(budWade.ped)
//								OPEN_SEQUENCE_TASK(seqSequence)
//									//make the ped run at the vehicle so then the locates heasder makes him get in. 
//									//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrevor.veh, <<5.0, 0.0, 0.0>>), PEDMOVE_RUN)
//									//TASK_ENTER_VEHICLE(NULL, vehTrevor.veh,DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
//								CLOSE_SEQUENCE_TASK(seqSequence)
//								TASK_PERFORM_SEQUENCE(budWade.ped,seqSequence)	
//								CLEAR_SEQUENCE_TASK(seqSequence)
								//SET_PED_COMBAT_ATTRIBUTES(budWade.ped, CA_LEAVE_VEHICLES, FALSE)
								SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(budWade.ped, TRUE)
							ENDIF
						ENDIF
					ENDIF
					iWadeProgress++
				BREAK
				CASE 2
//					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//						IF IS_PED_IN_VEHICLE(budWade.ped ,vehTrevor.veh)			
//						AND NOT IS_PED_GETTING_INTO_A_VEHICLE(budWade.ped)
//							iBuddyProgress++
//						ELIF  NOT IS_TASK_ONGOING(budWade.ped, SCRIPT_TASK_PERFORM_SEQUENCE)						
//							iBuddyProgress--
//						ENDIF
//					ENDIF
				BREAK
				CASE 3
				BREAK
			ENDSWITCH
		ELIF eMissionStage = STAGE_CHASE
			SWITCH iWadeProgress
				CASE 0
					IF NOT IS_PED_GROUP_MEMBER(budWade.ped, PLAYER_GROUP_ID())
						SET_PED_AS_GROUP_MEMBER(budWade.ped, PLAYER_GROUP_ID())
					ENDIF
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
					iWadeProgress++
				BREAK
				CASE 1
				BREAK
			ENDSWITCH
		ELIF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
			SWITCH iWadeProgress
				CASE 0  // idle case no actions assigned
					IF DOES_BLIP_EXIST(budWade.blip)
						REMOVE_BLIP(budWade.blip)
					ENDIF
					/*IF NOT IS_PED_GROUP_MEMBER(budWade.ped, PLAYER_GROUP_ID())
						SET_PED_AS_GROUP_MEMBER(budWade.ped,PLAYER_GROUP_ID())
					ENDIF*/
					IF IS_PED_GROUP_MEMBER(budWade.ped, PLAYER_GROUP_ID())
						REMOVE_PED_FROM_GROUP(budWade.ped)
					ENDIF
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
					SET_PED_ACCURACY(budWade.ped, 100)
					SET_PED_COMBAT_ATTRIBUTES(budWade.ped, CA_LEAVE_VEHICLES, FALSE)
					CLEAR_PED_TASKS(budWade.ped)
					iWadeProgress++
				BREAK
				CASE 1  // tell the budWade to get in the car or stand gaurd around it
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						FORCE_BEST_WEAPON(budWade.ped)
						REMOVE_PED_FROM_GROUP(budWade.ped)
						IF IS_PED_IN_VEHICLE(budWade.ped, vehTrevor.veh)
							IF GET_ENTITY_SPEED(vehTrevor.veh) < 2.0
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(budWade.ped, CA_LEAVE_VEHICLES, TRUE)
								
								SET_ENTITY_LOAD_COLLISION_FLAG(budWade.ped,TRUE)
								SET_PED_TO_LOAD_COVER(budWade.ped,TRUE)
								//TASK_LEAVE_ANY_VEHICLE(budRon.ped,1000)
								
								REMOVE_PED_FROM_GROUP(budWade.ped)
								SET_PED_ACCURACY(budWade.ped,1)
								SET_ENTITY_HEALTH(budWade.ped,800)
								SET_PED_SUFFERS_CRITICAL_HITS(budWade.ped,FALSE)
								SET_PED_CAN_BE_TARGETTED(budWade.ped,FALSE)
								SET_PED_RELATIONSHIP_GROUP_HASH(budWade.ped, grpBuddies)
								SET_PED_COMBAT_ATTRIBUTES(budWade.ped,CA_USE_COVER,TRUE)
								//1005626
								SET_PED_CONFIG_FLAG(budWade.ped,PCF_RunFromFiresAndExplosions,TRUE)
								SET_PED_SPHERE_DEFENSIVE_AREA(budWade.ped,GET_ENTITY_COORDS(vehTrevor.veh, FALSE),8.0)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
								
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,grpBuddies)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,RELGROUPHASH_PLAYER)
								
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpBuddies,grpEnemies)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,grpEnemies,grpBuddies)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, FALSE)
								
								SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(budWade.ped,FALSE,grpEnemies)
								SET_PED_COMBAT_ATTRIBUTES(budWade.ped,CA_DISABLE_PIN_DOWN_OTHERS,TRUE)
								
								OPEN_SEQUENCE_TASK(seqSequence)
									TASK_LEAVE_ANY_VEHICLE(NULL,1000,ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
									//TASK_SEEK_COVER_FROM_POS(NULL,<<64.1259, 3651.5776, 38.4862>>,-1,TRUE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,30)	
								CLOSE_SEQUENCE_TASK(seqSequence)
								TASK_PERFORM_SEQUENCE(budWade.ped,seqSequence)
								CLEAR_SEQUENCE_TASK(seqSequence)
								
								iWadeProgress++
							ENDIF
						ELSE
							iWadeProgress++				
						ENDIF
					ENDIF
				BREAK
				CASE 2
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(budWade.ped, CA_LEAVE_VEHICLES, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(budWade.ped, CA_AGGRESSIVE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(budWade.ped, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(budWade.ped, CA_USE_COVER, TRUE)
						SET_PED_ACCURACY(budWade.ped,25)
						
						IF NOT IS_PED_IN_ANY_VEHICLE(budWade.ped)
							FORCE_BEST_WEAPON(budWade.ped)
							
							IF NOT IS_PED_INJURED(budWade.ped)
								IF NOT DOES_BLIP_EXIST(budWade.blip)
									budWade.blip = CREATE_BLIP_FOR_PED(budWade.ped,FALSE,CHAR_WADE)
									SET_BLIP_AS_FRIENDLY(budWade.blip,TRUE)
								ENDIF
							ENDIF
						
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,RELGROUPHASH_PLAYER)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,grpBuddies)
							
							SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(vehTrevor.veh,FALSE,grpBuddies)
							SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(vehTrevor.veh,FALSE,grpEnemies)
							SET_VEHICLE_STRONG(vehTrevor.veh,TRUE)

//							OPEN_SEQUENCE_TASK(seqSequence)
//								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,30)
//								TASK_SEEK_COVER_FROM_POS(NULL,<<64.1259, 3651.5776, 38.4862>>,-1,TRUE)
//							CLOSE_SEQUENCE_TASK(seqSequence)
//							TASK_PERFORM_SEQUENCE(budWade.ped,seqSequence)
//							CLEAR_SEQUENCE_TASK(seqSequence)
							iWadeProgress++
						ENDIF
					ENDIF	
				BREAK
			ENDSWITCH
		ELIF eMissionStage = STAGE_CARAVAN_SMASH
			SWITCH iWadeProgress
				CASE 0  // idle case no actions assigned
//					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//						IF NOT IS_PED_IN_VEHICLE(budWade.ped, vehTrevor.veh)
//							IF NOT IS_PED_GROUP_MEMBER(budWade.ped, PLAYER_GROUP_ID())
//								SET_PED_AS_GROUP_MEMBER(budWade.ped,PLAYER_GROUP_ID())
//							ENDIF
//							OPEN_SEQUENCE_TASK(seqSequence)
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrevor.veh, <<8.0, 0.0, 0.0>>), PEDMOVE_RUN)
//							CLOSE_SEQUENCE_TASK(seqSequence)
//							TASK_PERFORM_SEQUENCE(budWade.ped,seqSequence)
//							CLEAR_SEQUENCE_TASK(seqSequence)
//						ENDIF
//					ENDIF
//					iBuddyProgress++
				BREAK
				CASE 1  // tell the budWade to get in the car or stand gaurd around it
				BREAK
			ENDSWITCH
		ELIF eMissionStage = STAGE_KILL_ORTEGA
			SWITCH iWadeProgress
				CASE 0  // idle case no actions assigned
//					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTrevor.veh)
//						OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
//							IF NOT IS_PED_GROUP_MEMBER(budWade.ped, PLAYER_GROUP_ID())
//								SET_PED_AS_GROUP_MEMBER(budWade.ped,PLAYER_GROUP_ID())
//							ENDIF
//							iBuddyProgress++
//						ENDIF
//					ENDIF
				BREAK
				CASE 1  // tell the budWade to get in the car or stand gaurd around it
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
ENDPROC


// ------------------------------------------------------------------
// PURPOSE: managed a timed fade out
PROC CONTROL_FADE_IN(INT iFadeTime)

	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(iFadeTime)
		WHILE IS_SCREEN_FADING_IN()
			WAIT(0)
		ENDWHILE
	ENDIF

ENDPROC
// PURPOSE: managed a timed fade in
PROC CONTROL_FADE_OUT(INT iFadeTime)
	
	IF IS_SCREEN_FADED_IN()
		DO_SCREEN_FADE_OUT(iFadeTime)
		WHILE IS_SCREEN_FADING_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if player is Trevor, if he isn't, calls SET_CURRENT_SELECTOR_PED until he is.
PROC SET_PLAYER_AS_TREVOR()
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		CPRINTLN(DEBUG_MISSION, "Trevor1: Waiting for player to be set to Trevor.")
		WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
			WAIT(0)
 		ENDWHILE
		CPRINTLN(DEBUG_MISSION, "Trevor1: Player now set to be Trevor.")
	ENDIF
ENDPROC


/// PURPOSE:
///    Add blood decals to Trevor's shirt, right foot and lower pantleg.
PROC APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
	
	//Shirt
	APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 0, 0.675, 0.251, 10, 0.85, 0, 0.0, "Scripted_Ped_Splash_Back")

	//Pants and boot
	APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 5, 0.397, 0.280, 320, 0.65, 0, 0.0, "Scripted_Ped_Splash_Back")
	APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(), 5, 0.297, 0.080, 320, 0.65, 0, 0.0, "Scripted_Ped_Splash_Back")

ENDPROC

//---------------------------------¦ Stage FUNCTIONS AND PROCEDURES ¦-----------------------------------

//PURPOSE:		Initialise Mission
PROC stageInitMission()
//---------¦ SETUP ¦---------
	//IF NOT bStageSetup
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
		SET_PLAYER_AS_TREVOR()
		
		IF Is_Replay_In_Progress()
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				//SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1982.7225, 3831.4949, 31.3972 >>)
				//SET_ENTITY_HEADING(PLAYER_PED_ID(), 206.1794)
			ENDIF
		ENDIF
		
		PRINTSTRING("GOING THROUGH HERE")PRINTNL()
		
		SET_DOOR_STATE(DOORNAME_HENCHMAN_TRAILER_L, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_HENCHMAN_TRAILER_R, DOORSTATE_UNLOCKED)

		SET_VEHICLE_MODEL_IS_SUPPRESSED(HEXER,TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GBURRITO,TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_01,TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_02,TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_03,TRUE)
		
		CreateAssetList()
		CREATE_NEED_LIST()
		SetupArrays()

		TOGGLE_STEALTH_RADAR(FALSE)
		
		PRINTSTRING("MISSION_SETUP")
		// Loads the mission text		
		SAFE_REQUEST_ADDITIONAL_TEXT(sTrevor1Message, MISSION_TEXT_SLOT, TRUE, FALSE)
		SAFE_REQUEST_ADDITIONAL_TEXT(sTrevor1Aud, MISSION_DIALOGUE_TEXT_SLOT, TRUE, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_AllowPlayerToInterruptVehicleEntryExit,TRUE)
		
		//make and set up the realationship groups
		//REMOVE_RELATIONSHIP_GROUP(grpBuddies)
		//REMOVE_RELATIONSHIP_GROUP(grpEnemies)
		IF bAddedRelGroups = FALSE
			IF ADD_RELATIONSHIP_GROUP("BUDDIES", grpBuddies)
			AND ADD_RELATIONSHIP_GROUP("ENEMIES", grpEnemies)
				bAddedRelGroups = TRUE
			ENDIF
		ENDIF
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,grpBuddies)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpBuddies,grpEnemies)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,grpBuddies)
		
		//set up player ped for dialouge
		
		//set the start and end vectors
		SET_START_AND_END_VECTORS()
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,FALSE)
		
		sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_TREVOR
		SET_WANTED_LEVEL_MULTIPLIER(0.1)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 65, FALSE)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL,65,FALSE)
		
		bDisabledHotSwap = TRUE
		INITALISE_ARRAYS(STAGE_INIT_MISSION)
		
		IF NOT DOES_ENTITY_EXIST(vehTrevor.veh)		
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK,vehTrevor.startLoc, vehTrevor.startHead)
				WAIT(0)
			ENDWHILE
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(budRon.ped)		
			//WHILE NOT CREATE_NPC_PED_ON_FOOT(budRon.ped, CHAR_RON, budRon.startLoc, budRon.startHead, TRUE)
			PRINTLN("@@@@@@@@@@@@@ SETUP_MISSION_REQUIREMENT 3 @@@@@@@@@@@@@@")
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON,budRon.startLoc, budRon.startHead)
				WAIT(0)
			ENDWHILE
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(budWade.ped)		
			//WHILE NOT CREATE_NPC_PED_ON_FOOT(budWade.ped, CHAR_WADE, budWade.startLoc, budWade.startHead, TRUE)
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_WADE,budWade.startLoc, budWade.startHead)
				WAIT(0)
			ENDWHILE
		ENDIF
		
		SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_NORMAL)
		//Set up the budRon if he is not dead.
		
		IF NOT IS_PED_INJURED(budRon.ped)
			 SET_UP_BUDDY(budRon,VS_FRONT_RIGHT)
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			SET_UP_BUDDY(budWade,VS_BACK_LEFT)
		ENDIF
		
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, budWade.ped, "Wade")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
		
		IF eMissionStage < STAGE_CARAVAN_SMASH
			SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_NORMAL)
		ELSE
			SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_CLEANUP)
		ENDIF
		SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_A, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_B, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_C, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_D, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_E, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA, BUILDINGSTATE_NORMAL)
		//REQUEST_IPL("des_methtrailer")
		
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(),TRV1_DAMAGE)
		//SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_02_SS, TRUE)
		Set_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS, TRUE)	
		
		DISABLE_TAXI_HAILING(TRUE)
		
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
		
		iStageSection = 0
		bSetUpStageData = FALSE
		bStageSetup = FALSE
//		IF IS_SCREEN_FADED_OUT()
//			DO_SCREEN_FADE_IN(500)
//		ENDIF
		eMissionStage = STAGE_GET_TO_CHASE		
	
ENDPROC

PROC SETUP_PEDS_FOR_DIALOGUE()
	
	IF eMissionStage = STAGE_INIT_MISSION

		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, budWade.ped, "Wade")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
		
	ENDIF
	
	IF eMissionStage = STAGE_GET_TO_CHASE
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, budWade.ped, "Wade")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ENDIF
	
	IF eMissionStage = STAGE_CUTSCENE_CHASE_START
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
		
		IF NOT IS_PED_INJURED(cmbtChase[2].ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds,5,cmbtChase[2].ped,"TERRY",TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(cmbtChase[3].ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds,6,cmbtChase[3].ped,"CLAY",TRUE)
		ENDIF
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, budWade.ped, "Wade")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ENDIF
	
	IF eMissionStage = STAGE_CHASE
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, budWade.ped, "Wade")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ENDIF
	
	IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, budWade.ped, "Wade")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ENDIF
	
	IF eMissionStage = STAGE_CARAVAN_SMASH
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, budWade.ped, "Wade")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ENDIF
	
	IF eMissionStage = STAGE_KILL_ORTEGA
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
		
		IF NOT IS_PED_INJURED(npcOrtega.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 7, npcOrtega.ped, "ORTEGA")
		ENDIF
		
	ENDIF
	
	IF eMissionStage = STAGE_DROP_OFF_RON
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,2)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,3)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,7)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ENDIF
	
	IF eMissionStage = STAGE_CUTSCENE_DROP_OFF_RON
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds,7)
			
		IF NOT IS_PED_INJURED(budRon.ped)
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ENDIF
	

ENDPROC
PROC MANAGE_BIKER_ANIMS()

//SceneBikers = CREATE_SYNCHRONIZED_SCENE(<< 1967.647, 4635.184, 40.156 >>, << 0.000, 0.000, 110.000 >>)

SWITCH iBikerAnims

	CASE 0
		IF IS_SYNCHRONIZED_SCENE_RUNNING(SceneBikers1)
			IF GET_SYNCHRONIZED_SCENE_PHASE(SceneBikers1) = 1
				SceneBikers2 = CREATE_SYNCHRONIZED_SCENE(<< 1967.647, 4635.184, 40.156 >>, << 0.000, 0.000, 110.000 >>)
				IF NOT IS_PED_INJURED(cmbtChase[1].ped)
					TASK_SYNCHRONIZED_SCENE (cmbtChase[1].ped, SceneBikers2, "misstrevor1", "gang_chatting_idle02_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT)
				ENDIF
				IF NOT IS_PED_INJURED(cmbtChase[3].ped)
					TASK_SYNCHRONIZED_SCENE (cmbtChase[3].ped, SceneBikers2, "misstrevor1", "gang_chatting_idle02_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT )
				ENDIF
				iBikerAnims ++
			ENDIF
		ENDIF
	BREAK
	
	CASE 1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(SceneBikers2)
			IF GET_SYNCHRONIZED_SCENE_PHASE(SceneBikers2) = 1
				SceneBikers3 = CREATE_SYNCHRONIZED_SCENE(<< 1967.647, 4635.184, 40.156 >>, << 0.000, 0.000, 110.000 >>)
				IF NOT IS_PED_INJURED(cmbtChase[1].ped)
					TASK_SYNCHRONIZED_SCENE (cmbtChase[1].ped, SceneBikers3, "misstrevor1", "gang_chatting_idle03_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT)
				ENDIF
				IF NOT IS_PED_INJURED(cmbtChase[3].ped)
					TASK_SYNCHRONIZED_SCENE (cmbtChase[3].ped, SceneBikers3, "misstrevor1", "gang_chatting_idle03_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT )
				ENDIF
				iBikerAnims ++
			ENDIF
		ENDIF
	BREAK
	
	CASE 2
		IF IS_SYNCHRONIZED_SCENE_RUNNING(SceneBikers3)
			IF GET_SYNCHRONIZED_SCENE_PHASE(SceneBikers3) = 1
				SceneBikers1 = CREATE_SYNCHRONIZED_SCENE(<< 1967.647, 4635.184, 40.156 >>, << 0.000, 0.000, 110.000 >>)
				IF NOT IS_PED_INJURED(cmbtChase[1].ped)
					TASK_SYNCHRONIZED_SCENE (cmbtChase[1].ped, SceneBikers1, "misstrevor1", "gang_chatting_idle01_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT)
				ENDIF
				IF NOT IS_PED_INJURED(cmbtChase[3].ped)
					TASK_SYNCHRONIZED_SCENE (cmbtChase[3].ped, SceneBikers1, "misstrevor1", "gang_chatting_idle01_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT )
				ENDIF
				iBikerAnims = 0
			ENDIF
		ENDIF
	BREAK

ENDSWITCH

ENDPROC

PROC SETUP_JOHNNY_AND_ASHLEY()

	SWITCH iJohnnyAndAshley
		CASE 0
			REQUEST_ANIM_DICT("misstrevor1")
			IF HAS_ANIM_DICT_LOADED("misstrevor1")
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(SceneAandJ)
					IF NOT IS_PED_INJURED(pedJohnny)
					AND NOT IS_PED_INJURED(pedAshley)
						SceneAandJ = CREATE_SYNCHRONIZED_SCENE(<< 2017.841, 3828.193, 31.445 >>, << -1.000, -4.000, -4.000 >>)
						IF NOT IS_PED_INJURED(pedJohnny)
							TASK_SYNCHRONIZED_SCENE(pedJohnny, SceneAandJ, "misstrevor1","mr_philips_idle_johnny", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS)//,RBF_PLAYER_IMPACT)
							TASK_PLAY_ANIM(pedJohnny, "FACIALS@GEN_MALE@BASE", "dead_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY|AF_LOOPING, 0.0)
							SET_ENTITY_HEALTH(pedJohnny,101)
							DISABLE_PED_PAIN_AUDIO(pedJohnny,TRUE)
							STOP_PED_SPEAKING(pedJohnny,TRUE)
							SET_PED_CAN_EVASIVE_DIVE(pedJohnny, FALSE)
							SET_PED_DIES_WHEN_INJURED(pedJohnny,TRUE)
							SET_PED_COMPONENT_VARIATION(pedJohnny,PED_COMP_HEAD,1,0)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJohnny,TRUE)
							SET_PED_CONFIG_FLAG(pedJohnny, PCF_RunFromFiresAndExplosions, FALSE)
							SET_PED_CONFIG_FLAG(pedJohnny, PCF_DisableExplosionReactions, TRUE)
							TASK_PLAY_ANIM(pedJohnny, "FACIALS@GEN_MALE@BASE", "dead_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING, 0.0)
							
							blood[0].decal_texture_id = DECAL_RSID_BLOOD_SPLATTER
							blood[0].pos = <<2018.58, 3828.41, 31.61>> //47.43
							blood[0].direction = <<0.0, 0.0, -1.0>>
							blood[0].side = normalise_vector(<<0.0, 1.0, 0.0>>) //heading 
							blood[0].width = 0.8
							blood[0].height = 0.8
							blood[0].fAlpha = 1.0
							blood[0].life = -1
							blood[0].wash_amount = 0.3
							blood[0].decal_removed = false
							
							blood[1].decal_texture_id = DECAL_RSID_BLOOD_DIRECTIONAL
							blood[1].pos = <<2018.69, 3827.87, 31.63>> //47.43
							blood[1].direction = <<0.0, 0.0, -1.0>>
							blood[1].side = normalise_vector(<<0.0, 1.0, 0.0>>) //heading 
							blood[1].width = 0.9
							blood[1].height = 0.8
							blood[1].fAlpha = 1.0
							blood[1].life = -1
							blood[1].wash_amount = 0.3
							blood[1].decal_removed = false
							
							blood[2].decal_texture_id = DECAL_RSID_BLOOD_DIRECTIONAL
							blood[2].pos = <<2017.87, 3828.09, 31.52>> //47.43
							blood[2].direction = <<0.0, 0.0, -1.0>>
							blood[2].side = normalise_vector(<<0.0, 1.0, 0.0>>) //heading 
							blood[2].width = 2.0
							blood[2].height = 2.0
							blood[2].fAlpha = 1.0
							blood[2].life = -1
							blood[2].wash_amount = 0.3
							blood[2].decal_removed = false
							
							blood[0].the_decal_id = add_decal(blood[0].decal_texture_id, blood[0].pos, blood[0].direction, blood[0].side, blood[0].width, blood[0].height, 0.196, 0, 0, blood[0].fAlpha, blood[0].life)
							
						ENDIF
						IF NOT IS_PED_INJURED(pedJohnny)
							SET_RAGDOLL_BLOCKING_FLAGS(pedJohnny,RBF_PLAYER_IMPACT)
						ENDIF
						IF NOT IS_PED_INJURED(pedAshley)		
							SET_PED_RELATIONSHIP_GROUP_HASH(pedAshley, RELGROUPHASH_PLAYER)
							SET_PED_CAN_EVASIVE_DIVE(pedAshley, FALSE)
							SET_ENTITY_HEALTH(pedAshley,101)
							SET_PED_CONFIG_FLAG(pedAshley, PCF_DisableExplosionReactions, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(pedAshley,CA_ALWAYS_FLEE,TRUE)
							TASK_SYNCHRONIZED_SCENE (pedAshley, SceneAandJ, "misstrevor1", "mr_philips_idle_ashley", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS)//,RBF_PLAYER_IMPACT )
						ENDIF
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAshley,TRUE)
						SET_SYNCHRONIZED_SCENE_LOOPED(SceneAandJ,TRUE)
						SET_ENTITY_NO_COLLISION_ENTITY(pedJohnny, pedAshley, TRUE)
						iJohnnyAndAshley ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			blood[1].the_decal_id = add_decal(blood[1].decal_texture_id, blood[1].pos, blood[1].direction, blood[1].side, blood[1].width, blood[1].height, 0.196, 0, 0, blood[1].fAlpha, blood[1].life)				
			iJohnnyAndAshley ++
		BREAK
		
		CASE 2
			blood[2].the_decal_id = add_decal(blood[2].decal_texture_id, blood[2].pos, blood[2].direction, blood[2].side, blood[2].width, blood[2].height, 0.196, 0, 0, blood[2].fAlpha, blood[2].life)
			iJohnnyAndAshley ++
		BREAK
	ENDSWITCH


	
ENDPROC

INT iAshleyWhimperTimer

PROC MANAGE_JOHNNY_AND_ASHLEY()
	
	IF bAshFleeTask = FALSE
		IF NOT IS_PED_INJURED(pedAshley)
			IF NOT IS_AMBIENT_SPEECH_PLAYING(pedAshley)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("WIMPER")
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedAshley, "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
					iAshleyWhimperTimer = GET_GAME_TIMER()
					SET_LABEL_AS_TRIGGERED("WIMPER",TRUE)
				ELSE
					IF MANAGE_MY_TIMER(iAshleyWhimperTimer,GET_RANDOM_INT_IN_RANGE(0,100))
						SET_LABEL_AS_TRIGGERED("WIMPER",FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedAshley)
		SET_PED_CAPSULE(pedAshley,0.5)
		SET_PED_RESET_FLAG(pedAshley, PRF_DisablePotentialBlastReactions,TRUE) 
	ENDIF
	
	IF NOT IS_PED_INJURED(pedJohnny)
		IF NOT IS_PED_INJURED(pedAshley)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedJohnny)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(pedJohnny)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(pedJohnny)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(pedAshley)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(pedAshley)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(pedAshley)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<2017.885986,3830.869873,30.865017>>, <<2017.531250,3824.715820,34.666862>>, 6.250000,WEAPONTYPE_GRENADE)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<2017.885986,3830.869873,30.865017>>, <<2017.531250,3824.715820,34.666862>>, 6.250000,WEAPONTYPE_STICKYBOMB)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<2017.885986,3830.869873,30.865017>>, <<2017.531250,3824.715820,34.666862>>, 6.250000,WEAPONTYPE_SMOKEGRENADE)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),pedAshley,<<2,2,3>>)
			OR IS_PED_BEING_STUNNED(pedJohnny)
				IF bAshFleeTask = FALSE
					IF NOT IS_PED_RAGDOLL(pedAshley)
						IF NOT IS_PED_FLEEING(pedAshley)
							SceneAandJFlee = CREATE_SYNCHRONIZED_SCENE(<< 2017.841, 3828.193, 31.520 >>, << 0.000, 0.000, -4.000 >>)
							TASK_SYNCHRONIZED_SCENE (pedAshley, SceneAandJFlee, "misstrevor1", "stand_ashley", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS)
							SET_SYNCHRONIZED_SCENE_LOOPED(SceneAandJFlee,FALSE)
							SET_ENTITY_HEALTH(pedJohnny,0)
							bAshFleeTask = TRUE
						ENDIF
					ENDIF
				ENDIF
				SET_ENTITY_HEALTH(pedJohnny,0)
			ENDIF
		ELSE
			SET_ENTITY_HEALTH(pedJohnny,0)
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(pedAshley)
			IF NOT IS_PED_RAGDOLL(pedAshley)
				IF bAshFleeTask = FALSE
					SceneAandJFlee = CREATE_SYNCHRONIZED_SCENE(<< 2017.841, 3828.193, 31.520 >>, << 0.000, 0.000, -4.000 >>)
					TASK_SYNCHRONIZED_SCENE (pedAshley, SceneAandJFlee, "misstrevor1", "stand_ashley", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_LOOPED(SceneAandJFlee,FALSE)
					bAshFleeTask = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bAshFleeTask = TRUE
		IF NOT IS_PED_INJURED(pedAshley)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(SceneAandJFlee)
				IF GET_SYNCHRONIZED_SCENE_PHASE(SceneAandJFlee) = 1.0
					TASK_SMART_FLEE_PED(pedAshley,PLAYER_PED_ID(),150,-1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iStageSection > 1
		IF DOES_ENTITY_EXIST(pedAshley)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedAshley,FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 125
				IF NOT IS_ENTITY_ON_SCREEN(pedAshley)
					DELETE_PED(pedAshley)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_ASHLEY)
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedJohnny)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedJohnny,FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 125
				IF NOT IS_ENTITY_ON_SCREEN(pedJohnny)
					DELETE_PED(pedJohnny)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOHNNYKLEBITZ)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

BOOL bAshleyKilled 

PROC MANAGE_ASHLEY_DIALOGUE()

	//PRINTSTRING("iDriveConvo:")PRINTINT(iDriveConvo)PRINTNL()
	
	IF DOES_ENTITY_EXIST(pedAshley)
		IF IS_PED_INJURED(pedAshley)
			bAshleyKilled = TRUE
		ENDIF
	ENDIF

	SWITCH iDriveConvo
		
		CASE 0
			IF HAS_LABEL_BEEN_TRIGGERED("T1M1_AL1")
				iDriveConvo ++ 
			ENDIF
		BREAK
		
		CASE 1
			//PRINTSTRING("GET CURRENT SCRIPTED LINE:")PRINTINT(GET_CURRENT_SCRIPTED_CONVERSATION_LINE())PRINTNkL()
			IF bAshleyKilled = TRUE
				IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > 9
					KILL_FACE_TO_FACE_CONVERSATION()
					iDriveConvo ++ 
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_KA")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_KA", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("T1M1_KA",TRUE)
						SET_LABEL_AS_TRIGGERED("T1M1_AL1",FALSE)
						iDriveConvo ++ 
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_AL1")
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(structConvoPeds, "T1M1AUD", "T1M1_AL1","T1M1_CFAL",CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("T1M1_AL1",TRUE)
					iDriveConvo ++ 
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
		
		BREAK
		
	ENDSWITCH


ENDPROC


FLOAT fTruckHealth
STRING sAssignedAnim
INT iRandAnim
INT iWadeYellTimer

PROC MANAGE_WADE_IN_BACK_SEAT()

	//PRINTSTRING("iWadeBackSeat: ")PRINTINT(iWadeBackSeat)PRINTNL()
	
	IF iWadeBackSeat > 0
		IF iWadeBackSeat !=1
			IF NOT IS_PED_INJURED(budWade.ped)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(budWade.ped,vehTrevor.veh,VS_BACK_LEFT)
						iWadeBackSeat = 1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

	SWITCH iWadeBackSeat
	
		CASE 0
			REQUEST_ANIM_DICT("misstrevor1ig_2")
			IF HAS_ANIM_DICT_LOADED("misstrevor1ig_2")
				iWadeBackSeat ++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(budWade.ped)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					AND IS_PED_SITTING_IN_VEHICLE_SEAT(budWade.ped,vehTrevor.veh,VS_BACK_LEFT)
//						PRINTSTRING("fTruckHeath: ")PRINTFLOAT(fTruckHealth)PRINTNL()
//						PRINTSTRING("fTruckSpeed: ")PRINTFLOAT(GET_ENTITY_SPEED(vehTrevor.veh))PRINTNL()
						IF GET_ENTITY_SPEED(vehTrevor.veh) > 22.0 AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrevor.veh)
							IF fTruckHealth > GET_ENTITY_HEALTH(vehTrevor.veh)+GET_VEHICLE_ENGINE_HEALTH(vehTrevor.veh)
								SET_LABEL_AS_TRIGGERED("WADE NOISE REQUIRED",TRUE)
								iWadeBackSeat = 2
							ENDIF
						ELIF GET_ENTITY_SPEED(vehTrevor.veh) > 6.0 AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrevor.veh)
							IF fTruckHealth > GET_ENTITY_HEALTH(vehTrevor.veh)+GET_VEHICLE_ENGINE_HEALTH(vehTrevor.veh)
								SET_LABEL_AS_TRIGGERED("WADE NOISE REQUIRED",TRUE)
								iWadeBackSeat = 3
							ENDIF				
						ELIF GET_ENTITY_SPEED(vehTrevor.veh) > 25.0 AND NOT IS_VEHICLE_ON_ALL_WHEELS(vehTrevor.veh)
							SET_LABEL_AS_TRIGGERED("WADE NOISE REQUIRED",TRUE)
							iWadeBackSeat = 4
						ELIF GET_ENTITY_SPEED(vehTrevor.veh) > 10.0 AND NOT IS_VEHICLE_ON_ALL_WHEELS(vehTrevor.veh)
							iWadeBackSeat = 5
						ELIF GET_ENTITY_SPEED(vehTrevor.veh) > 27
							SET_LABEL_AS_TRIGGERED("WADE NOISE REQUIRED",TRUE)
							IF eMissionStage =  STAGE_CHASE
								iWadeBackSeat = 4
							ENDIF
						ELIF GET_ENTITY_SPEED(vehTrevor.veh) > 10.0
							IF eMissionStage =  STAGE_CHASE
								iWadeBackSeat = 5
							ENDIF
						ELIF GET_ENTITY_SPEED(vehTrevor.veh) < 1.0
							IF NOT IS_STRING_NULL(sAssignedAnim)
								IF IS_ENTITY_PLAYING_ANIM(budWade.ped,"misstrevor1ig_2",sAssignedAnim,ANIM_SCRIPT)
									STOP_ANIM_TASK(budWade.ped,"misstrevor1ig_2",sAssignedAnim)
								ENDIF
							ENDIF
						ENDIF
						fTruckHealth = GET_ENTITY_HEALTH(vehTrevor.veh)+GET_VEHICLE_ENGINE_HEALTH(vehTrevor.veh)
					ELSE
						IF NOT IS_STRING_NULL(sAssignedAnim)
							IF IS_ENTITY_PLAYING_ANIM(budWade.ped,"misstrevor1ig_2",sAssignedAnim,ANIM_SCRIPT)
								STOP_ANIM_TASK(budWade.ped,"misstrevor1ig_2",sAssignedAnim)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//IMPACT LARGE
		CASE 2
		
			IF NOT IS_PED_INJURED(budWade.ped)
			AND IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					iRandAnim = GET_RANDOM_INT_IN_RANGE(0,3)	
					IF iRandAnim = 0 
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_large_01_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY|AF_NOT_INTERRUPTABLE)
						sAssignedAnim = "impact_large_01_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 1
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_large_02_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY|AF_NOT_INTERRUPTABLE)
						sAssignedAnim = "impact_large_02_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 2 
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_large_03_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY|AF_NOT_INTERRUPTABLE)
						sAssignedAnim = "impact_large_03_wade"
						iWadeBackSeat = 99
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//IMPACT SMALL
		CASE 3
		
			IF NOT IS_PED_INJURED(budWade.ped)
			AND IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					iRandAnim = GET_RANDOM_INT_IN_RANGE(0,3)	
					IF iRandAnim = 0 
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_small_01_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "impact_small_01_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 1
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_small_02_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "impact_small_02_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 2 
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_small_03_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "impact_small_03_wade"
						iWadeBackSeat = 99
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//BUMPS LARGE
		CASE 4
		
			IF NOT IS_PED_INJURED(budWade.ped)
			AND IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					iRandAnim = GET_RANDOM_INT_IN_RANGE(0,10)					
					IF iRandAnim = 0 
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","bumps_loop_large_01_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "bumps_loop_large_01_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 1
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","bumps_loop_large_02_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "bumps_loop_large_02_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 2
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_small_01_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "impact_small_01_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 3
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_small_02_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "impact_small_02_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 4
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","bumps_loop_large_02_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "bumps_loop_large_02_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 5
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","bumps_loop_large_01_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "bumps_loop_large_01_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 6
						IF NOT HAS_LABEL_BEEN_TRIGGERED("iRandAnim6")
							TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_large_01_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY|AF_NOT_INTERRUPTABLE)
							sAssignedAnim = "impact_large_01_wade"
							SET_LABEL_AS_TRIGGERED("iRandAnim6",TRUE)
							iWadeBackSeat = 99
						ENDIF
					ENDIF
					
					IF iRandAnim = 7
						IF NOT HAS_LABEL_BEEN_TRIGGERED("iRandAnim7")
							TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_large_02_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY|AF_NOT_INTERRUPTABLE)
							sAssignedAnim = "impact_large_02_wade"
							SET_LABEL_AS_TRIGGERED("iRandAnim7",TRUE)
							iWadeBackSeat = 99
						ENDIF
					ENDIF
					
					IF iRandAnim = 8
						IF NOT HAS_LABEL_BEEN_TRIGGERED("iRandAnim8")
							TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_large_03_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY|AF_NOT_INTERRUPTABLE)
							sAssignedAnim = "impact_large_03_wade"
							SET_LABEL_AS_TRIGGERED("iRandAnim8",TRUE)
							iWadeBackSeat = 9
						ENDIF
					ENDIF
					
					IF iRandAnim = 9
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","impact_small_03_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "impact_small_03_wade"
						iWadeBackSeat = 99
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		
		//BUMPS SMALL
		CASE 5
		
			IF NOT IS_PED_INJURED(budWade.ped)
			AND IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					iRandAnim = GET_RANDOM_INT_IN_RANGE(0,2)	
					IF iRandAnim = 0 
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","bumps_loop_small_01_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "bumps_loop_small_01_wade"
						iWadeBackSeat = 99
					ENDIF
					
					IF iRandAnim = 1
						TASK_PLAY_ANIM(budWade.ped,"misstrevor1ig_2","bumps_loop_small_02_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						sAssignedAnim = "bumps_loop_small_02_wade"
						iWadeBackSeat = 99
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 99
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF HAS_LABEL_BEEN_TRIGGERED("WADE NOISE REQUIRED")
					IF NOT HAS_LABEL_BEEN_TRIGGERED("WADE NOISE")
						IF NOT IS_PED_INJURED(budWade.ped)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(budWade.ped,"T1M1_FMAA","WADE",SPEECH_PARAMS_FORCE)
							SET_LABEL_AS_TRIGGERED("WADE NOISE",TRUE)
							iWadeYellTimer = GET_GAME_TIMER()
						ENDIF
					ELSE
						IF MANAGE_MY_TIMER(iWadeYellTimer,GET_RANDOM_INT_IN_RANGE(5000,8000))
							SET_LABEL_AS_TRIGGERED("WADE NOISE",FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF NOT IS_PED_INJURED(budWade.ped)
			AND IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					IF NOT IS_STRING_NULL(sAssignedAnim)
						IF NOT IS_ENTITY_PLAYING_ANIM(budWade.ped,"misstrevor1ig_2",sAssignedAnim,ANIM_SCRIPT)
							SET_LABEL_AS_TRIGGERED("WADE NOISE REQUIRED",FALSE)
							iWadeBackSeat = 1
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_STRING_NULL(sAssignedAnim)
						IF IS_ENTITY_PLAYING_ANIM(budWade.ped,"misstrevor1ig_2",sAssignedAnim,ANIM_SCRIPT)
							STOP_ANIM_TASK(budWade.ped,"misstrevor1ig_2",sAssignedAnim)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	
	ENDSWITCH

ENDPROC

INT iWadeGetBackIn
INT iStoppingDistance
BOOL T1M1_WADEGBI = FALSE
BOOL bTogglePushHintCam = FALSE


//PURPOSE:	Checks if the player has arrived at the bikers	
FUNC BOOL stageGetToChase()
	
	IF DOES_ENTITY_EXIST(vehTrevor.veh)
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1978.168945,4656.044922,37.969585>>, <<1948.442993,4626.579102,49.553745>>, 24.500000)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY)
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 10.0)
					IF bTogglePushHintCam = FALSE
						IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_GAMEPLAY_COORD_HINT(<<1965.2100, 4635.4678, 41.0095>>,-1,1500)
							SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.2)
							bTogglePushHintCam = TRUE
						ENDIF
					ELSE
						IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
							SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE) 
							STOP_GAMEPLAY_HINT()
							bTogglePushHintCam = FALSE
						ENDIF
					ENDIF
				ELSE
					IF bTogglePushHintCam = TRUE
						SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE) 
						STOP_GAMEPLAY_HINT()
						bTogglePushHintCam = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_1_DRIVE_TO_BIKERS")
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				START_AUDIO_SCENE("TREVOR_1_DRIVE_TO_BIKERS")
				SET_LABEL_AS_TRIGGERED("TREVOR_1_DRIVE_TO_BIKERS",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)

	MANAGE_WADE_IN_BACK_SEAT()

	IF bSetUpPedAnims = TRUE
		MANAGE_BIKER_ANIMS()
	ENDIF
	
	IF iStageSection > 0
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly,TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF iStageSection > 1
		MANAGE_ASHLEY_DIALOGUE()
	ENDIF
	
	SETUP_JOHNNY_AND_ASHLEY()
	MANAGE_JOHNNY_AND_ASHLEY()

	IF bSetUpStageData = FALSE
		SETUP_PEDS_FOR_DIALOGUE()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0,"Get to Chase")
		SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		bSetUpStageData = TRUE
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("BUDDIES IN VEHICLE")
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF NOT IS_PED_INJURED(budWade.ped)
			AND NOT IS_PED_INJURED(budRon.ped)
				SET_PED_RESET_FLAG(budWade.ped,PRF_UseTighterAvoidanceSettings,TRUE)
				//SET_PED_RESET_FLAG(budWade.ped,PRF_AvoidPedsEnteringVehicles,TRUE)
				IF IS_PED_RUNNING(PLAYER_PED_ID())
				OR IS_PED_SPRINTING(PLAYER_PED_ID())
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					SET_PED_MAX_MOVE_BLEND_RATIO(budWade.ped, PEDMOVE_RUN)
					SET_PED_MAX_MOVE_BLEND_RATIO(budRon.ped, PEDMOVE_RUN)
				ELSE
					SET_PED_MAX_MOVE_BLEND_RATIO(budWade.ped, PEDMOVE_WALK)
					SET_PED_MAX_MOVE_BLEND_RATIO(budRon.ped, PEDMOVE_WALK)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					AND IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
						SET_LABEL_AS_TRIGGERED("BUDDIES IN VEHICLE",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//---------¦ SETUP ¦---------
	IF NOT bStageSetup	
		
		
		IF IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_IN()
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DO LOAD SCENE")
				LOAD_SCENE(<< 1982.7225, 3831.4949, 31.3972 >>)
			ENDIF
			
			SET_CUTSCENE_FADE_VALUES(FALSE,TRUE,FALSE,TRUE)
			
			IF NOT IS_PED_INJURED(budRon.ped)
			AND NOT IS_PED_INJURED(budWade.ped)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_ENTITY_DEAD(budRon.ped)
						SET_ENTITY_COORDS(budRon.ped, <<2000.46, 3831.18, 31.29>>)
						SET_ENTITY_HEADING(budRon.ped, 90.19)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(budRon.ped,TRUE)
						SET_UP_RON_COMPONENTS()
					ENDIF

					IF NOT IS_ENTITY_DEAD(budWade.ped)
						SET_ENTITY_COORDS(budWade.ped, <<1999.89, 3829.58, 31.30>>)
						SET_ENTITY_HEADING(budWade.ped, 91.42)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(budWade.ped,TRUE)
						SET_UP_WADE_COMPONENTS()
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<1995.97, 3829.84, 31.2735>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 100.81)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(),TRUE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		RESET_NEED_LIST()
		MANAGE_LOADING_NEED_LIST()								
		iStageSection =0
		bTogglePushHintCam = FALSE
		bikerLeadin = FALSE
		bSetUpPedAnims = FALSE
		bDoFails = TRUE
		bGoToSpeach = FALSE
		
		IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
			safeFadeInAndWait()
			bStageSetup = TRUE
		ENDIF
		
		IF NOT bTrevorSplashBackEffectOn
			APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
			bTrevorSplashBackEffectOn = TRUE
		ENDIF	
		
//---------¦ UPDATE ¦---------		
	ELSE	
		SWITCH iStageSection
				
			CASE 0 
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_INJURED(budRon.ped)
						REMOVE_PED_FROM_GROUP(budRon.ped)
					ENDIF
					
					IF NOT IS_PED_INJURED(budWade.ped)
						REMOVE_PED_FROM_GROUP(budWade.ped)
						SET_RAGDOLL_BLOCKING_FLAGS(budWade.ped,RBF_BULLET_IMPACT)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("BLOOD")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.657,0.566,126.360,0.07,3,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.589,0.266,0,0.01,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.612,0.269,126.360,0.22,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.709,0.396,126.360,0.00,1,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.582,0.383,200.360,0.1,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.582,0.383,111.360,0.05,2,0,"cs_trev1_blood")
					SET_LABEL_AS_TRIGGERED("BLOOD",TRUE)
				ENDIF
				
				IF interior_trev_trailer != NULL
					UNPIN_INTERIOR(interior_trev_trailer)
				ENDIF
				
				bAshleyKilled = FALSE
				bStreamCutscene = FALSE
				bDoFails = TRUE
				bAshFleeTask = FALSE
				budRon.iAction = 0
				cmbtChase[0].wpn = WEAPONTYPE_ASSAULTRIFLE
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),FALSE)
				T1M1_WADEGBI = FALSE
				iStageSection++
			BREAK
			
			CASE 1
				IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_S1", CONV_PRIORITY_MEDIUM)
					iStageSection++
				ENDIF
			BREAK
			
			CASE 2
				CONTROL_ENEMY_CREATION()
			
				#IF IS_DEBUG_BUILD				
					DONT_DO_J_SKIP(sLocatesData)
				#ENDIF
				
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,<<1962.4629, 4646.9336, 39.7530>>, <<0.001,0.001,LOCATE_SIZE_HEIGHT>>,TRUE,budRon.ped,budWade.ped,NULL,vehTrevor.veh,"TRV_GT1",Asset.godLeftRonBehind,"TRV_BDY3","","TRV_BDY5", "TRV_GTTT",Asset.godGetBackToCar,FALSE,TRUE)
				
				ELSE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF bGoToSpeach = FALSE
									IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_AL1", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("T1M1_AL1",TRUE)
											IF NOT IS_PED_INJURED(budWade.ped)
												TASK_LOOK_AT_ENTITY(budWade.ped,PLAYER_PED_ID(),-1)
											ENDIF
											bGoToSpeach = TRUE
										ENDIF
									ELSE
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PH1", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("T1M1_PH1",TRUE)
											bGoToSpeach = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1934.793823,4634.039063,38.488747>>, <<1966.492676,4647.708008,42.865807>>, 15.500000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1949.083496,4634.543457,38.653030>>, <<1923.866089,4645.178711,42.060905>>, 15.500000)
							IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
								IF NOT bikerLeadin
									ADD_PED_FOR_DIALOGUE(structConvoPeds,0,cmbtChase[1].ped,"BIKER1")
									ADD_PED_FOR_DIALOGUE(structConvoPeds,1,cmbtChase[2].ped,"BIKER2")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_MCS1_LI", CONV_PRIORITY_MEDIUM)
											IF NOT IS_PED_INJURED(cmbtChase[1].ped)
											AND NOT IS_PED_INJURED(cmbtChase[2].ped)
											AND NOT IS_PED_INJURED(cmbtChase[3].ped)
												IF NOT IS_PED_INJURED(cmbtChase[2].ped)
													TASK_PLAY_ANIM(cmbtChase[2].ped,"misstrevor1","gang_chatting_leadin_b",REALLY_SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)
												ENDIF
												SceneBikers1 = CREATE_SYNCHRONIZED_SCENE(<< 1967.647, 4635.184, 40.156 >>, << 0.000, 0.000, 110.000 >>)
												TASK_SYNCHRONIZED_SCENE (cmbtChase[1].ped, SceneBikers1, "misstrevor1", "gang_chatting_leadin_a", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT)
												TASK_SYNCHRONIZED_SCENE (cmbtChase[3].ped, SceneBikers1, "misstrevor1", "gang_chatting_leadin_c", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT )
												bikerLeadin = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF GET_ENTITY_SPEED(vehTrevor.veh) < 10
									iStoppingDistance = 15
								ELSE
									iStoppingDistance = 10
								ENDIF
								
								IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehTrevor.veh,TO_FLOAT(iStoppingDistance))
									HANG_UP_AND_PUT_AWAY_PHONE()
									KILL_FACE_TO_FACE_CONVERSATION()
									SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
									
									IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(),vehTrevor.veh,VS_DRIVER)
										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
											OPEN_SEQUENCE_TASK(seqSequence)
												TASK_PAUSE(NULL,1000)
												TASK_LEAVE_VEHICLE(NULL,vehTrevor.veh,ECF_DONT_CLOSE_DOOR)
											CLOSE_SEQUENCE_TASK(seqSequence)
											TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqSequence)
											CLEAR_SEQUENCE_TASK(seqSequence)
										ENDIF
									ENDIF
									
									IF NOT IS_PED_INJURED(budRon.ped)
										REMOVE_PED_FROM_GROUP(budRon.ped)
									ENDIF
									
									IF NOT IS_PED_INJURED(budWade.ped)
										REMOVE_PED_FROM_GROUP(budWade.ped)
									ENDIF
									
									budRon.iAction = 0
									iTimer = GET_GAME_TIMER()
									REMOVE_BLIP(sLocatesData.LocationBlip)
									//SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
									IF NOT IS_PED_INJURED(budWade.ped)
										TASK_CLEAR_LOOK_AT(budWade.ped)
									ENDIF
									
									REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
									
									iStageSection ++
								ENDIF
							ENDIF
						ENDIF
						
						IF MANAGE_MY_TIMER(iWadeGetBackIn,2000)
							IF T1M1_WADEGBI = TRUE
								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
								T1M1_WADEGBI = FALSE
							ENDIF
						ENDIF
						
					ELSE	
						
					ENDIF
					IF iStageSection = 2
						IF bGoToSpeach = TRUE
							//MANAGE PAUSE DIALOGUE
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								IF NOT IS_PED_INJURED(budWade.ped)
								AND NOT IS_PED_INJURED(budRon.ped)
									IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									AND IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
									AND IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
										IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
											CLEAR_PRINTS()
											PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
										ENDIF
									//Play conversation
									ELSE
										IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
											IF IS_THIS_PRINT_BEING_DISPLAYED(Asset.godGetBackToCar)
												CLEAR_PRINTS()
											ENDIF
									    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
										ELSE
											IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
												IF NOT IS_PED_INJURED(budWade.ped)
													IF NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh,TRUE)
														IF NOT T1M1_WADEGBI
															TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),budWade.ped,20000,SLF_WHILE_NOT_IN_FOV)
															PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"T1M1_GSAA","TREVOR",SPEECH_PARAMS_FORCE_FRONTEND)
															PRINTSTRING("WADE GET BACK IN REGULAR")PRINTNL()
															T1M1_WADEGBI = TRUE
															iWadeGetBackIn = GET_GAME_TIMER()
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF

					//Speech upon getting close by - RIGHT
					IF budRon.iAction = 0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2013.300415,4635.465332,48.703773>>, <<2083.669678,4704.756836,28.958324>>, 30.000000)
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF IS_IT_SAFE_TO_PLAY_DIALOG()
									IF budRon.iAction = 0 
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_NR1", CONV_PRIORITY_MEDIUM)
											budRon.iAction = 1
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//- LEFT
					IF budRon.iAction = 0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2000.089478,4623.684082,39.670765>>, <<1928.133057,4592.134766,43.342770>>, 25.250000)
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF IS_IT_SAFE_TO_PLAY_DIALOG()
									IF budRon.iAction = 0 
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_NR2", CONV_PRIORITY_MEDIUM)
											budRon.iAction = 1
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//TOGGLE CUTSCENE STREAMING
					IF GET_DISTANCE_BETWEEN_COORDS(<<1959.78027, 4645.95654, 39.74654>>,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 70
						IF bStreamCutscene = FALSE
							bStreamCutscene = TRUE
							REQUEST_CUTSCENE("TRV_1_MCS_1_P1")
							SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_COMPLETELY_OFF)
							PRINTSTRING("SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_COMPLETELY_OFF)")
							PRINTNL()
						ENDIF
					ELSE
						IF bStreamCutscene = TRUE
							IF GET_DISTANCE_BETWEEN_COORDS(<<1959.78027, 4645.95654, 39.74654>>,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 95
								REMOVE_CUTSCENE()
								SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
								PRINTSTRING("SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)")
								PRINTNL()
								bStreamCutscene = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//STREAMING ASSETS
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 1961.6035, 4645.7407, 39.7129 >>,<<300,300,150>>)
						IF NOT DOES_ENTITY_EXIST(vehTruck.veh)
						OR NOT DOES_ENTITY_EXIST(vehBike[0].veh)
						OR NOT DOES_ENTITY_EXIST(vehBike[1].veh)
						OR NOT DOES_ENTITY_EXIST(cmbtChase[0].ped)
						OR NOT DOES_ENTITY_EXIST(cmbtChase[1].ped)
						OR NOT DOES_ENTITY_EXIST(cmbtChase[2].ped)
						OR NOT DOES_ENTITY_EXIST(cmbtChase[3].ped)
							PRINTSTRING("STRAMINGASSETS")PRINTNL()
							REQUEST_MODEL(Asset.mnGangVan)
							REQUEST_MODEL(Asset.mnBike)
							REQUEST_MODEL(Asset.mnLost1)
							IF HAS_MODEL_LOADED(Asset.mnGangVan)
							AND HAS_MODEL_LOADED(Asset.mnGangVan)
							AND HAS_MODEL_LOADED(Asset.mnGangVan)
								REQUEST_MODEL(IG_CLAY)
								REQUEST_MODEL(IG_TERRY)
								REQUEST_MODEL(prop_cs_beer_bot_01)
								IF HAS_MODEL_LOADED(Asset.mnLost1)
								AND HAS_MODEL_LOADED(IG_CLAY)
								AND HAS_MODEL_LOADED(IG_TERRY)
									IF HAS_MODEL_LOADED(Asset.mnLost1)
									AND HAS_MODEL_LOADED(IG_CLAY)
									AND HAS_MODEL_LOADED(IG_TERRY)
										CREATE_CHASE_ENEMIES()
									ENDIF
									
									SET_PED_POPULATION_BUDGET(2)
									SET_VEHICLE_POPULATION_BUDGET(2)
									
									IF HAS_MODEL_LOADED(Asset.mnGangVan)
									AND HAS_MODEL_LOADED(Asset.mnBike)
										IF NOT DOES_ENTITY_EXIST(vehTruck.veh)
											vehTruck.veh=CREATE_VEHICLE(vehTruck.model, vehTruck.startLoc, vehTruck.startHead)
											SET_VEHICLE_DIRT_LEVEL(vehTruck.veh,15.0)
										ELSE
											SetUpTruck()
										ENDIF
										IF NOT DOES_ENTITY_EXIST(vehBike[0].veh)
											SETUP_MISSION_REQUIREMENT(REQ_CLAY_BIKE, <<0,0,0>>)
										ENDIF
										IF NOT DOES_ENTITY_EXIST(vehBike[1].veh)
											SETUP_MISSION_REQUIREMENT(REQ_TERRY_BIKE, <<0,0,0>>)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF bSetUpPedAnims = FALSE
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 2015.9991, 4641.4448, 41.1899 >>,<<50,50,50>>)
									IF DOES_ENTITY_EXIST(vehTruck.veh)
										IF NOT IS_PED_INJURED(cmbtChase[1].ped)
										AND NOT IS_PED_INJURED(cmbtChase[2].ped)
										AND NOT IS_PED_INJURED(cmbtChase[3].ped)
											REQUEST_ANIM_DICT("misstrevor1")
											IF HAS_MODEL_LOADED(prop_cs_beer_bot_01)
												IF NOT DOES_ENTITY_EXIST(bikerProp[0])
													bikerProp[0] = CREATE_OBJECT(prop_cs_beer_bot_01,<< 1967.647, 4635.184, 40.156 >>)
												ELSE
													IF NOT IS_ENTITY_ATTACHED(bikerProp[0])
														ATTACH_ENTITY_TO_ENTITY(bikerProp[0],cmbtChase[1].ped,GET_PED_BONE_INDEX(cmbtChase[1].ped,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE,FALSE)
														SET_ENTITY_VISIBLE(bikerProp[0],TRUE)
													ELSE
														IF HAS_ANIM_DICT_LOADED("misstrevor1")
															SET_PED_CURRENT_WEAPON_VISIBLE(cmbtChase[1].ped,FALSE)
															SET_PED_CURRENT_WEAPON_VISIBLE(cmbtChase[2].ped,FALSE)
															SET_PED_CURRENT_WEAPON_VISIBLE(cmbtChase[3].ped,FALSE)
															
															IF NOT IS_PED_INJURED(cmbtChase[2].ped)
																IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
																	ATTACH_ENTITY_TO_ENTITY(cmbtChase[2].ped,vehBike[0].veh,-1,<<0,0,0>>,<<0,0,0>>)
																ENDIF
															ENDIF
															
															IF NOT IS_PED_INJURED(cmbtChase[3].ped)
																SET_PED_PROP_INDEX(cmbtChase[3].ped,ANCHOR_HEAD,0,0)
															ENDIF
															
															IF NOT IS_PED_INJURED(cmbtChase[1].ped)
																SET_PED_COMPONENT_VARIATION(cmbtChase[1].ped, PED_COMP_HEAD, 0, 0)
																SET_PED_COMPONENT_VARIATION(cmbtChase[1].ped, PED_COMP_TORSO, 0, 0)
																SET_PED_COMPONENT_VARIATION(cmbtChase[1].ped, PED_COMP_LEG, 0, 0)
																SET_PED_COMPONENT_VARIATION(cmbtChase[1].ped, PED_COMP_DECL, 0, 0)
															ENDIF
															
															IF NOT IS_PED_INJURED(cmbtChase[2].ped)
																OPEN_SEQUENCE_TASK(seqSequence)
																	TASK_PLAY_ANIM(NULL,"misstrevor1","gang_chatting_idle01_b",NORMAL_BLEND_IN,SLOW_BLEND_OUT)
																	TASK_PLAY_ANIM(NULL,"misstrevor1","gang_chatting_idle02_b",NORMAL_BLEND_IN,SLOW_BLEND_OUT)
																	TASK_PLAY_ANIM(NULL,"misstrevor1","gang_chatting_idle03_b",NORMAL_BLEND_IN,SLOW_BLEND_OUT)
																	TASK_PLAY_ANIM(NULL,"misstrevor1","gang_chatting_idle03_b",NORMAL_BLEND_IN,SLOW_BLEND_OUT)
																	TASK_PLAY_ANIM(NULL,"misstrevor1","gang_chatting_idle01_b",NORMAL_BLEND_IN,SLOW_BLEND_OUT)
																	TASK_PLAY_ANIM(NULL,"misstrevor1","gang_chatting_idle02_b",NORMAL_BLEND_IN,SLOW_BLEND_OUT)
																	TASK_PLAY_ANIM(NULL,"misstrevor1","gang_chatting_idle01_b",NORMAL_BLEND_IN,SLOW_BLEND_OUT)
																	TASK_PLAY_ANIM(NULL,"misstrevor1","gang_chatting_idle02_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
																CLOSE_SEQUENCE_TASK(seqSequence)
																TASK_PERFORM_SEQUENCE(cmbtChase[2].ped, seqSequence)
																CLEAR_SEQUENCE_TASK(seqSequence)
															ENDIF
															
															IF NOT IS_PED_INJURED(cmbtChase[0].ped)
																SET_CURRENT_PED_WEAPON(cmbtChase[0].ped,WEAPONTYPE_UNARMED,TRUE)
																TASK_START_SCENARIO_IN_PLACE(cmbtChase[0].ped,"WORLD_HUMAN_STAND_IMPATIENT")
															ENDIF

															SceneBikers1 = CREATE_SYNCHRONIZED_SCENE(<< 1967.647, 4635.184, 40.156 >>, << 0.000, 0.000, 110.000 >>)
															TASK_SYNCHRONIZED_SCENE (cmbtChase[1].ped, SceneBikers1, "misstrevor1", "gang_chatting_idle01_a", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT)
															TASK_SYNCHRONIZED_SCENE (cmbtChase[3].ped, SceneBikers1, "misstrevor1", "gang_chatting_idle01_c", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT )
															iBikerAnims = 0
															bSetUpPedAnims = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT bikerLeadin
					ADD_PED_FOR_DIALOGUE(structConvoPeds,0,cmbtChase[1].ped,"BIKER1")
					ADD_PED_FOR_DIALOGUE(structConvoPeds,1,cmbtChase[2].ped,"BIKER2")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_MCS1_LI", CONV_PRIORITY_MEDIUM)
							IF NOT IS_PED_INJURED(cmbtChase[1].ped)
							AND NOT IS_PED_INJURED(cmbtChase[2].ped)
							AND NOT IS_PED_INJURED(cmbtChase[3].ped)
								IF NOT IS_PED_INJURED(cmbtChase[2].ped)
									TASK_PLAY_ANIM(cmbtChase[2].ped,"misstrevor1","gang_chatting_leadin_b",REALLY_SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)
								ENDIF
								SceneBikers1 = CREATE_SYNCHRONIZED_SCENE(<< 1967.647, 4635.184, 40.156 >>, << 0.000, 0.000, 110.000 >>)
								TASK_SYNCHRONIZED_SCENE (cmbtChase[1].ped, SceneBikers1, "misstrevor1", "gang_chatting_leadin_a", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT)
								TASK_SYNCHRONIZED_SCENE (cmbtChase[3].ped, SceneBikers1, "misstrevor1", "gang_chatting_leadin_c", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT )
								bikerLeadin = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					OR MANAGE_MY_TIMER(iTimer,3000)
						//IF MANAGE_MY_TIMER(iTimer,200)
							iStageSection ++
						//ENDIF
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF iStageSection >= 3
			IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
			AND GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			ENDIF
		ENDIF
		
//---------¦ JSKIP ¦---------
		/*IF JSkip
			iStageSection = 1
		ENDIF*/
			
//---------¦ CLEANUP ¦---------
		IF iStageSection >= 4
			IF DOES_BLIP_EXIST(blipVeh)
				REMOVE_BLIP(blipVeh)
			ENDIF
			IF DOES_BLIP_EXIST(blipDest)
				REMOVE_BLIP(blipDest)
			ENDIF
			IF DOES_BLIP_EXIST(blipPed)
				REMOVE_BLIP(blipPed)
			ENDIF
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_LIGHTING_FOR_MCS1()
	
	SWITCH iLighting
		
		CASE 0
			IF GET_CUTSCENE_TIME() > 0
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.434937,4636.586914,40.412811,2.516263)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1968.302002,4629.885742,42.857506,7.414639)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1965.873291,4633.479492,40.888203,13.429683)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 1
			IF GET_CUTSCENE_TIME() > 5461
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.434937,4636.586914,40.412811,2.516263)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1962.875488,4644.254395,40.680481,1.814476)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1965.410889,4637.772949,40.399704,11.230053)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 2
			IF GET_CUTSCENE_TIME() > 10805
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1962.746582,4642.685547,40.678364,0.881858)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1963.935791,4644.579102,40.662670,2.335937)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1965.410889,4637.772949,40.399704,11.230053)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 3
			IF GET_CUTSCENE_TIME() > 13227
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1962.746582,4642.685547,40.678364,0.881858)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1965.411377,4637.781250,40.400429,1.796215)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1963.435425,4636.587891,40.412991,2.062566)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 4
			IF GET_CUTSCENE_TIME() > 16715
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.435425,4636.587891,40.412968,0.914129)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1963.330566,4641.718750,40.659653,1.039871)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1962.602051,4646.024902,40.842731,4.550869)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 5
			IF GET_CUTSCENE_TIME() > 32704
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.435425,4636.587891,40.412968,1.742254)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1963.330566,4641.718750,40.659653,1.039871)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1962.602051,4646.024902,40.842731,4.550869)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 6
			IF GET_CUTSCENE_TIME() > 35094
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.250366,4640.795898,40.491692,1.271309)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1963.736450,4642.139160,40.697403,0.953900)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1962.602051,4646.024902,40.842731,4.550869)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 7
			IF GET_CUTSCENE_TIME() > 42528
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.453125,4639.561523,40.646774,0.491341)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1964.152100,4641.173340,40.695503,0.687045)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1964.151367,4641.171875,40.694950,6.765635)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 8
			IF GET_CUTSCENE_TIME() > 48512
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.435059,4636.586914,40.412827,1.492826)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1965.410400,4637.767578,40.399101,1.803624)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1964.151367,4641.171875,40.694950,6.765635)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 9
			IF GET_CUTSCENE_TIME() > 50997
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.435059,4636.586914,40.412827,1.492826)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1965.410400,4637.767578,40.399101,1.803624)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1964.151367,4641.171875,40.694950,6.765635)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 10
			IF GET_CUTSCENE_TIME() > 52864
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.250488,4638.857422,40.643677,0.447461)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1961.856323,4639.996094,40.670654,1.407288)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1962.602051,4646.024414,40.842731,4.020690)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
		
		CASE 11
			IF GET_CUTSCENE_TIME() > 54864
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0,TRUE,1963.497559,4636.665039,40.415661,2.288949)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1,TRUE,1965.410889,4637.773438,40.399681,4.558084)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(2,TRUE,1962.602051,4646.024414,40.842731,4.020690)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS(3,FALSE,0.000000,0.000000,0.000000,0.000000)
				iLighting ++
			ENDIF
		BREAK
	
	ENDSWITCH
	
	
ENDPROC

BOOL bVanExitState = FALSE

PROC RUN_MEET_BIKERS_CUTSCENE()

	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying

		WAIT(0)
		
		IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
		AND GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_MrPhilips")
		
		#IF IS_DEBUG_BUILD
			IF IS_CUTSCENE_ACTIVE()
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
					b_skipped_mocap = TRUE
					PRINTSTRING("e_section_stage = SECTION_STAGE_SKIP")PRINTNL()
					e_section_stage = SECTION_STAGE_SKIP
				ENDIF
			ENDIF
		#ENDIF

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					PRINTSTRING("i_current_event = 0")
					PRINTNL()
					IF HAS_LABEL_BEEN_TRIGGERED("TREVOR_1_DRIVE_TO_BIKERS")
						STOP_AUDIO_SCENE("TREVOR_1_DRIVE_TO_BIKERS")
					ENDIF
					bcutsceneplaying = TRUE
					SET_START_AND_END_VECTORS()
					REQUEST_ANIM_DICT("misstrevor1leadinout")
					REQUEST_CUTSCENE("TRV_1_MCS_1_P1")
					bClearCutscenArea = FALSE
					b_skipped_mocap = FALSE
					bVanExitState = FALSE
					i_current_event++
				ELIF i_current_event = 1
					PRINTSTRING("i_current_event = 1 waiting on cutscene loading with failsafe")
					PRINTNL()
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						PRINTSTRING("i_current_event = 1 HAS_ANIM_DICT_LOADED misstrevor1leadinout")
						PRINTNL()
						IF HAS_ANIM_DICT_LOADED("misstrevor1leadinout")
							KILL_ANY_CONVERSATION()
							INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF 
							
							IF NOT IS_PED_INJURED(budRon.ped)
								REGISTER_ENTITY_FOR_CUTSCENE(budRon.ped, "Ron", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF 
							
							IF NOT IS_PED_INJURED(budWade.ped)
								REGISTER_ENTITY_FOR_CUTSCENE(budWade.ped, "Wade", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								REGISTER_ENTITY_FOR_CUTSCENE(vehTrevor.veh, "Trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
								REGISTER_ENTITY_FOR_CUTSCENE(vehTruck.veh, "Lost_Van", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
								REGISTER_ENTITY_FOR_CUTSCENE(vehBike[0].veh, "Clays_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
								REGISTER_ENTITY_FOR_CUTSCENE(vehBike[1].veh, "Terrys_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							IF NOT IS_PED_INJURED(cmbtChase[0].ped)
								REGISTER_ENTITY_FOR_CUTSCENE(cmbtChase[0].ped, "Other_Lost_biker", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							IF NOT IS_PED_INJURED(cmbtChase[1].ped)
								REGISTER_ENTITY_FOR_CUTSCENE(cmbtChase[1].ped, "Lost_biker", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							IF NOT IS_PED_INJURED(cmbtChase[2].ped)
								REGISTER_ENTITY_FOR_CUTSCENE(cmbtChase[2].ped, "Terry", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							IF NOT IS_PED_INJURED(cmbtChase[3].ped)
								REGISTER_ENTITY_FOR_CUTSCENE(cmbtChase[3].ped, "Clay", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
							SET_VEHICLE_ENGINE_ON(vehTrevor.veh,FALSE,FALSE)
							
							IF NOT IS_PED_INJURED(cmbtChase[2].ped)
								IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
									IF IS_ENTITY_ATTACHED_TO_ENTITY(cmbtChase[2].ped,vehBike[0].veh)
										DETACH_ENTITY(cmbtChase[2].ped,FALSE)
										DETACH_ENTITY(vehBike[0].veh,FALSE)
									ENDIF
								ENDIF
							ENDIF
							
					
							
							START_CUTSCENE()
							SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
							PRINTSTRING("SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)")
							PRINTNL()
							IF IS_SCREEN_FADED_OUT()
								DO_SCREEN_FADE_IN(500)
							ELSE
								WAIT(0)
							ENDIF
							
							e_section_stage = SECTION_STAGE_RUNNING
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
			AND GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			ENDIF
			IF IS_CUTSCENE_PLAYING()
				IF bClearCutscenArea = FALSE
				
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
					ADD_SCENARIO_BLOCKING_AREA(<< vStageStart.x -100.0,vStageStart.y -100.0,vStageStart.z -100.0>>,<< vStageStart.x +100.0,vStageStart.y +100.0,vStageStart.z +100.0>>)
					CLEAR_AREA_OF_PEDS(vStageStart, 100.0)
					CLEAR_AREA_OF_VEHICLES(vStageStart, 200.0)

					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEHICLE_TYRE_FIXED(vehTrevor.veh,SC_WHEEL_CAR_FRONT_LEFT)
						SET_VEHICLE_TYRE_FIXED(vehTrevor.veh,SC_WHEEL_CAR_FRONT_RIGHT)
						SET_VEHICLE_TYRE_FIXED(vehTrevor.veh,SC_WHEEL_CAR_REAR_LEFT)
						SET_VEHICLE_TYRE_FIXED(vehTrevor.veh,SC_WHEEL_CAR_REAR_RIGHT)
						IF IS_ENTITY_ON_FIRE(vehTrevor.veh)
							STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(vehTrevor.veh, FALSE),20)
							SET_VEHICLE_ENGINE_HEALTH(vehTrevor.veh,1000)
							SET_VEHICLE_PETROL_TANK_HEALTH(vehTrevor.veh,1000)
						ENDIF
					ENDIF
					
					CLEAR_AREA(<<1963.4191, 4644.1030, 39.7482>>,100,TRUE)
					IF DOES_ENTITY_EXIST(bikerProp[0])
						DELETE_OBJECT(bikerProp[0])
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(prop_cs_beer_bot_01)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
					REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
					
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEHICLE_DOOR_CONTROL(vehTrevor.veh,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,0)
						SET_VEHICLE_DOOR_CONTROL(vehTrevor.veh,SC_DOOR_FRONT_RIGHT,DT_DOOR_INTACT,0)
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedJohnny)
						IF NOT IS_PED_INJURED(pedJohnny)
							CLEAR_PED_TASKS_IMMEDIATELY(pedJohnny)
						ENDIF
						DELETE_PED(pedJohnny)
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOHNNYKLEBITZ)
					ELSE
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOHNNYKLEBITZ)
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedAshley)
						IF NOT IS_PED_INJURED(pedAshley)
							CLEAR_PED_TASKS_IMMEDIATELY(pedAshley)
						ENDIF
						DELETE_PED(pedAshley)
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_ASHLEY)
					ELSE
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_ASHLEY)
					ENDIF

					IF NOT IS_PED_INJURED(budRon.ped)
						REMOVE_PED_FROM_GROUP(budRon.ped)
					ENDIF
					
					IF NOT IS_PED_INJURED(budWade.ped)
						REMOVE_PED_FROM_GROUP(budWade.ped)
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_ASHLEY)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOHNNYKLEBITZ)

					ADD_SCENARIO_BLOCKING_AREA(vTrailerPark + <<100,100,100>>, vTrailerPark - <<100,100,100>>)
					SET_NUMBER_OF_PARKED_VEHICLES(0)
					CLEAR_AREA(vTrailerPark,500,TRUE,FALSE)
					
					REQUEST_VEHICLE_RECORDING(vehTrevor.recNumber,sCarRec)
					REQUEST_VEHICLE_RECORDING(vehTruck.recNumber,sCarRec)
					REQUEST_VEHICLE_RECORDING(vehBike[0].recNumber,sCarRec)
					REQUEST_VEHICLE_RECORDING(vehBike[1].recNumber,sCarRec)
					
					REQUEST_VEHICLE_RECORDING(200, sAwUberRec) // Trigger car
					REQUEST_VEHICLE_RECORDING(201, sAwUberRec) // Van
					REQUEST_VEHICLE_RECORDING(204, sAwUberRec) //-- Bike 0
					REQUEST_VEHICLE_RECORDING(205, sAwUberRec) //-- Bike 1
					
					STOP_GAMEPLAY_HINT()
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE) 
					
					bClearCutscenArea = TRUE
				ELSE
					IF IS_CUTSCENE_PLAYING()
						IF GET_CUTSCENE_TIME() < 49954
							IF NOT b_skipped_mocap
								IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
									CONTROL_FADE_OUT(500)
									e_section_stage = SECTION_STAGE_SKIP
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF GET_CUTSCENE_TIME() > 22500
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_START")
					IF TRIGGER_MUSIC_EVENT("TRV1_START")
						SET_LABEL_AS_TRIGGERED("TRV1_START",TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 41200
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_CHASE_STARTS")
					IF TRIGGER_MUSIC_EVENT("TRV1_CHASE_STARTS")
						SET_LABEL_AS_TRIGGERED("TRV1_CHASE_STARTS",TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			//MANAGE_LIGHTING_FOR_MCS1()
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				PRINTSTRING("CAM EXIT")PRINTNL()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Other_Lost_Biker")
				PRINTSTRING("Exit State - Other_Lost_Biker")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
					IF NOT IS_PED_INJURED(cmbtChase[0].ped)
					AND IS_VEHICLE_DRIVEABLE(vehTruck.veh)
						IF NOT IS_PED_IN_VEHICLE(cmbtChase[0].ped,vehTruck.veh)
							SET_VEHICLE_ENGINE_ON(vehTruck.veh,TRUE,TRUE)
							SET_PED_INTO_VEHICLE(cmbtChase[0].ped, vehTruck.veh, VS_DRIVER)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				PRINTSTRING("Exit State - Trevor")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
						IF IS_VEHICLE_SEAT_FREE(vehTrevor.veh,VS_DRIVER)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Wade")
				PRINTSTRING("Exit State - Wade")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_INJURED(budWade.ped)
						IF IS_VEHICLE_SEAT_FREE(vehTrevor.veh,VS_BACK_LEFT)
							IF DOES_ENTITY_EXIST(cmbtChase[2].ped)
								SET_PED_INTO_VEHICLE(budWade.ped,vehTrevor.veh,VS_BACK_LEFT)
								IF NOT IS_SCREEN_FADED_OUT()
									TASK_PLAY_ANIM(budWade.ped, "misstrevor1leadinout", "leadout_action_wade", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
								ENDIF
								TASK_LOOK_AT_ENTITY(budWade.ped,cmbtChase[2].ped,-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_HIGH)
								REMOVE_PED_FROM_GROUP(budWade.ped)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ron")
				PRINTSTRING("Exit State - Ron")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_INJURED(budRon.ped)
						IF IS_VEHICLE_SEAT_FREE(vehTrevor.veh,VS_FRONT_RIGHT)
							SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
							REMOVE_PED_FROM_GROUP(budRon.ped)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//WARP PLAYER AND BUDDIES
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_car")
				PRINTSTRING("Exit State - Trevors_car")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					//SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
					SET_VEHICLE_DOOR_SHUT(vehTrevor.veh,SC_DOOR_FRONT_LEFT)
					SET_VEHICLE_DOOR_SHUT(vehTrevor.veh,SC_DOOR_FRONT_RIGHT)
					SET_VEHICLE_ENGINE_ON(vehTrevor.veh,TRUE,TRUE)
					PLAY_SOUND_FROM_ENTITY(-1,"TREVOR_1_LEAD_OUT_CR",vehTrevor.veh)
					SET_VEHICLE_FORWARD_SPEED(vehTrevor.veh,7)
					bVanExitState = TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF bVanExitState = FALSE
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehTrevor.veh)
					ENDIF
				ENDIF
			ENDIF

			//WARP VAN
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lost_Van")
				PRINTSTRING("Exit State - Lost_Van")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
					SET_VEHICLE_DOORS_SHUT(vehTruck.veh)
					START_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh, 200, sAwUberRec)
					//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh, 2400.0)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh, 3000.0)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehTruck.veh)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lost_biker")
				PRINTSTRING("Exit State - Lost_biker")PRINTNL()
				IF NOT IS_PED_INJURED(cmbtChase[1].ped)	
					SET_ENTITY_VISIBLE(cmbtChase[1].ped,FALSE)
				ENDIF
			ENDIF
				
			//WARP BIKERS
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Clay")
				PRINTSTRING("Exit State - Clay")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
					IF NOT IS_PED_INJURED(cmbtChase[2].ped)	
						IF IS_ENTITY_ATTACHED_TO_ENTITY(cmbtChase[2].ped,vehBike[0].veh)
							DETACH_ENTITY(cmbtChase[2].ped)
							DETACH_ENTITY(vehBike[0].veh)
						ELSE
							IF NOT IS_PED_INJURED(cmbtChase[2].ped)
								IF NOT IS_PED_IN_VEHICLE(cmbtChase[2].ped,vehBike[0].veh)
									FREEZE_ENTITY_POSITION(cmbtChase[2].ped,FALSE)
									SET_PED_INTO_VEHICLE(cmbtChase[2].ped, vehBike[0].veh, VS_DRIVER)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Clays_bike")
				PRINTSTRING("Exit State - Clays_bike")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
						START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehBike[0].veh, 204, sAwUberRec,1,0,DRIVINGMODE_AVOIDCARS_RECKLESS)
						SET_VEHICLE_ENGINE_ON(vehBike[0].veh,TRUE,TRUE)
						//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh, 2400.0)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh, 3000.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[0].veh)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Terry")
				PRINTSTRING("Exit State - Terry")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
					IF NOT IS_PED_INJURED(cmbtChase[3].ped)
						IF NOT IS_PED_IN_VEHICLE(cmbtChase[3].ped,vehBike[1].veh)
							FREEZE_ENTITY_POSITION(cmbtChase[3].ped,FALSE)
							SET_PED_INTO_VEHICLE(cmbtChase[3].ped, vehBike[1].veh, VS_DRIVER)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Terrys_bike")
				PRINTSTRING("Exit State - Terrys_bike")PRINTNL()
				IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
						START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehBike[1].veh, 205, sAwUberRec,1,0,DRIVINGMODE_AVOIDCARS_RECKLESS)
						SET_VEHICLE_ENGINE_ON(vehBike[1].veh,TRUE,TRUE)
						//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh, 2400.0)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh, 3000.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[1].veh)
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			AND bClearCutscenArea = TRUE
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF IS_CUTSCENE_ACTIVE()
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						STOP_CUTSCENE()
						REMOVE_CUTSCENE()
						WHILE IS_CUTSCENE_ACTIVE()
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
			#ENDIF
		ENDIF
			

		IF e_section_stage = SECTION_STAGE_CLEANUP
			IF b_skipped_mocap
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
				LOAD_SCENE(<<1963.4191, 4644.1030, 39.7482>>)
			ENDIF
			//Setup buddy relationship groups etc.
			
			REPLAY_STOP_EVENT()
			
			INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
			TRIGGER_MUSIC_EVENT("TRV1_CHASE_CS_SKIP")
			REMOVE_CUTSCENE()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			i_current_event = 0
			e_section_stage = SECTION_STAGE_SETUP
			bcutsceneplaying = FALSE
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDWHILE
ENDPROC


//BOOL bCanSkipChaseStartScene
//BOOL bWasChaseStartSceneSkipped
//INT iChaseCutDiagProg
//PURPOSE:		Performs the cutscene which introduces the garage and the three characters
FUNC BOOL stageCutsceneChaseStart()
	
	MANAGE_WADE_IN_BACK_SEAT()
	
	IF iStageSection < 3
		IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
		AND GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		ENDIF
	ENDIF
	
	IF iStageSection < 1
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1976.329956,4652.762207,39.183220>>, <<1952.472656,4637.663574,45.604504>>, 21.750000)
			IF bTogglePushHintCam = FALSE
				SET_GAMEPLAY_COORD_HINT(<<1965.2100, 4635.4678, 41.0095>>,-1,1500)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
				bTogglePushHintCam = TRUE
			ENDIF
		ELSE
			IF bTogglePushHintCam = TRUE
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE) 
				STOP_GAMEPLAY_HINT()
				bTogglePushHintCam = FALSE
			ENDIF
		ENDIF
	ENDIF

//---------¦ SETUP ¦---------
	IF NOT bStageSetup
		IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
		AND GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		ENDIF
		//RESET_NEED_LIST()
		IF NOT DOES_ENTITY_EXIST(vehTruck.veh)
			vehTruck.model= Asset.mnGangVan
			SET_MODEL_AS_NEEDED(Asset.mnGangVan)
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			SET_MODEL_AS_NEEDED(Asset.mnLost1)
			SET_MODEL_AS_NEEDED(Asset.mnClay)
			SET_MODEL_AS_NEEDED(Asset.mnTerry)
			SET_VEHICLE_RECORDING_AS_NEEDED(sCarRec, vehTrevor.recNumber)
			SET_VEHICLE_RECORDING_AS_NEEDED(sCarRec, vehTruck.recNumber)
			SET_VEHICLE_RECORDING_AS_NEEDED(sCarRec, vehBike[0].recNumber)
			SET_VEHICLE_RECORDING_AS_NEEDED(sCarRec, vehBike[1].recNumber)
			MANAGE_LOADING_NEED_LIST()
			
			REQUEST_MODEL(Asset.mnTerry)
			REQUEST_MODEL(Asset.mnClay)
			
			REQUEST_VEHICLE_RECORDING(200, sAwUberRec) // Trigger car
			REQUEST_VEHICLE_RECORDING(201, sAwUberRec) // Van
			REQUEST_VEHICLE_RECORDING(204, sAwUberRec) //-- Bike 0
			REQUEST_VEHICLE_RECORDING(205, sAwUberRec) //-- Bike 1
			
			IF IS_SCREEN_FADED_OUT()
				LOAD_ALL_OBJECTS_NOW()
			ENDIF
			
			WHILE NOT CREATE_VEHICLE_STRUCT(vehTruck)
				safeWait(0)
			ENDWHILE
			
			WHILE CREATE_VEHICLE_STRUCT(vehBike[0])
				safeWait(0)
			ENDWHILE
			
			WHILE CREATE_VEHICLE_STRUCT(vehBike[1])
				safeWait(0)
			ENDWHILE
			
			IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
				SET_VEHICLE_DIRT_LEVEL(vehTruck.veh,15.0)
			ENDIF
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
			
			ADD_SCENARIO_BLOCKING_AREA(vTrailerPark + <<100,100,100>>, vTrailerPark - <<100,100,100>>)
			CLEAR_AREA(vTrailerPark,500,TRUE,FALSE)
			
			CREATE_CHASE_ENEMIES()
			bDoFails = TRUE
			SetUpTruck()
		ENDIF
		
		SET_START_AND_END_VECTORS()
		INITALISE_ARRAYS(STAGE_CHASE)
		
		//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
		IF NOT bTrevorSplashBackEffectOn
			APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
			bTrevorSplashBackEffectOn = TRUE
		ENDIF		
		
		FOR iCounter = 0 TO MAX_CHASE_PED-1
			IF NOT IS_PED_INJURED(cmbtChase[iCounter].ped)
				IF (iCounter = 0 OR iCounter = 1)
					SET_PED_SUFFERS_CRITICAL_HITS(cmbtChase[iCounter].ped ,FALSE)
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cmbtChase[iCounter].ped ,TRUE)
				SET_ENTITY_INVINCIBLE(cmbtChase[iCounter].ped ,TRUE)
			ENDIF
		ENDFOR
		
		IF NOT IS_PED_INJURED(budRon.ped)
			//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
		ENDIF
	
		SETTIMERB(0)
		iStageSection = 0
		bStageSetup = TRUE
	
		SETUP_PEDS_FOR_DIALOGUE()
		
		

//---------¦ UPDATE ¦---------		
	ELSE		
		SWITCH iStageSection
			
			
			CASE 0
				//SETUP
				PRINTSTRING("stageCutsceneChaseStart -iStageSection = 0")
				PRINTNL()
				SETUP_PEDS_FOR_DIALOGUE()
				REQUEST_VEHICLE_RECORDING(200, sAwUberRec) // Trigger car
				REQUEST_VEHICLE_RECORDING(201, sAwUberRec) // Van
				REQUEST_VEHICLE_RECORDING(204, sAwUberRec) //-- Bike 0
				REQUEST_VEHICLE_RECORDING(205, sAwUberRec) //-- Bike 1
				REQUEST_CUTSCENE("TRV_1_MCS_1_P1")
				
				SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				KILL_FACE_TO_FACE_CONVERSATION()
				SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
				ADD_PED_FOR_DIALOGUE(structConvoPeds,0,cmbtChase[1].ped,"BIKER1")
				ADD_PED_FOR_DIALOGUE(structConvoPeds,1,cmbtChase[2].ped,"BIKER2")
				iLighting = 0
				i_current_event = 0
				e_section_stage = SECTION_STAGE_SETUP
				iStageSection ++
			BREAK
			
			CASE 1
				PRINTSTRING("stageCutsceneChaseStart -iStageSection = 1")
				PRINTNL()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					REMOVE_PED_FOR_DIALOGUE(structConvoPeds,0)
					REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
					ADD_PED_FOR_DIALOGUE(structConvoPeds,1,cmbtChase[2].ped,"BIKER2")
					REQUEST_VEHICLE_RECORDING(200, sAwUberRec) // Trigger car
					REQUEST_VEHICLE_RECORDING(201, sAwUberRec) // Van
					REQUEST_VEHICLE_RECORDING(204, sAwUberRec) //-- Bike 0
					REQUEST_VEHICLE_RECORDING(205, sAwUberRec) //-- Bike 1
					REMOVE_ANIM_DICT("misstrevor1")
					iStageSection ++
				ENDIF
			BREAK
			
			CASE 2
				RUN_MEET_BIKERS_CUTSCENE()
				IF NOT IS_CUTSCENE_ACTIVE()
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					REMOVE_PED_FOR_DIALOGUE(structConvoPeds,0)
					REMOVE_PED_FOR_DIALOGUE(structConvoPeds,1)
					iStageSection ++
				ENDIF
			BREAK
			
		ENDSWITCH
				
				
//---------¦ CLEANUP ¦---------
		IF iStageSection > 2		
			
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(200, sAwUberRec)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(201, sAwUberRec)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(204, sAwUberRec)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(205, sAwUberRec) 
				INITIALISE_UBER_PLAYBACK(sAwUberRec, 200)
				INIT_UBER_DATA()
				
				IF NOT IS_PED_INJURED(cmbtChase[2].ped)
					SET_ENTITY_HEALTH(cmbtChase[2].ped, 175)
				ENDIF
				
				IF NOT IS_PED_INJURED(cmbtChase[3].ped)
					SET_ENTITY_HEALTH(cmbtChase[3].ped, 175)
				ENDIF
				
				IF NOT IS_PED_INJURED(budRon.ped)
					REMOVE_PED_FROM_GROUP(budRon.ped)
				ENDIF
				
				IF NOT IS_PED_INJURED(budWade.ped)
					REMOVE_PED_FROM_GROUP(budWade.ped)
				ENDIF
				
				IF NOT IS_PED_INJURED(budRon.ped)
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(budWade.ped)
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
				ENDIF
				
				bStageSetup = FALSE
				bSetUpStageData = FALSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_PRELOAD_MODELS_FOR_SHOOTOUT(VEHICLE_INDEX vehPlayback, FLOAT fPreload = 153000.0, FLOAT fCreate = 163000.0)
	INT i
	MODEL_NAMES mFuelCan = PROP_GASCYL_01A
	IF NOT DOES_ENTITY_EXIST(vehTrailerPark[4].veh)
		IF NOT IS_ENTITY_DEAD(vehPlayback)
			IF HAS_DW_CAR_REC_TIME_PASSED(vehPlayback, fPreload)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<24.5298, 3590.7327, 0.0>>, <<147.1179, 3772.4255,100>>, FALSE)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  5.9328, 3682.4102,0.0 >>,<<110.0308, 3801.6506, 100.0>>, FALSE)
				REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<24.5298, 3590.7327, 0.0>>, <<147.1179, 3772.4255,100>>)
				REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<  5.9328, 3682.4102,0.0 >>,<<110.0308, 3801.6506, 100.0>>)
				REQUEST_MODEL(Prop_GasCyl_02A)
				REQUEST_MODEL(HEXER)
				REQUEST_MODEL(G_M_Y_LOST_01)
				REQUEST_MODEL(G_M_Y_LOST_02)
				REQUEST_MODEL(REBEL)
				//REQUEST_MODEL(A_C_ROTTWEILER)
				REQUEST_MODEL(mFuelCan)
				REQUEST_VEHICLE_RECORDING(1, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(9, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(11, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(12, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(13, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(14, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(15, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(16, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(17, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(18, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(19, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(20, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(21, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(22, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(23, "Trevor1dw")
				REQUEST_VEHICLE_RECORDING(2, "t1b")
				REQUEST_VEHICLE_RECORDING(3, "t1b")
				REQUEST_VEHICLE_RECORDING(4, "t1b")
				REQUEST_VEHICLE_RECORDING(5, "t1b")
				REQUEST_VEHICLE_RECORDING(6, "t1b")
				REQUEST_VEHICLE_RECORDING(7, "t1b")
				REQUEST_VEHICLE_RECORDING(8, "t1b")
				REQUEST_VEHICLE_RECORDING(9, "t1b")
				REQUEST_VEHICLE_RECORDING(10, "t1b")
				IF HAS_MODEL_LOADED(HEXER)
				AND HAS_MODEL_LOADED(G_M_Y_LOST_01)
				AND HAS_MODEL_LOADED(G_M_Y_LOST_02)
				AND HAS_MODEL_LOADED(REBEL)
				AND HAS_MODEL_LOADED(mFuelCan)
				AND HAS_MODEL_LOADED(Prop_GasCyl_02A)
				//AND HAS_MODEL_LOADED(A_C_ROTTWEILER)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(9, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(11, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(12, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(13, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(14, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(15, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(16, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(17, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(18, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(19, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(20, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(21, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(22, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(23, "Trevor1dw")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(3,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(4,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(5,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(6,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(7,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(8,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(9,"t1b")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(10,"t1b")
					IF HAS_DW_CAR_REC_TIME_PASSED(vehPlayback, fCreate)
						INITALISE_ARRAYS(STAGE_CARAVAN_PARK_FIGHT)
//						SCRIPT_ASSERT("Hit")
						REPEAT COUNT_OF(vehTrailerPark) i
							IF i != 10
							AND i != 11
								IF NOT ARE_VECTORS_EQUAL(vehTrailerPark[i].startLoc, <<0.0, 0.0, 0.0>>)
									CREATE_VEHICLE_STRUCT(vehTrailerPark[i])
									IF i = 10
										SET_VEHICLE_COLOURS(vehTrailerPark[i].veh,0,0)
									ENDIF
									
									IF i = 11
										SET_VEHICLE_COLOURS(vehTrailerPark[i].veh,62,62)
									ENDIF
									
									IF i = 12
										SET_VEHICLE_COLOURS(vehTrailerPark[i].veh,10,10)
									ENDIF
									
									IF i = 13
										SET_VEHICLE_COLOURS(vehTrailerPark[i].veh,4,4)
									ENDIF
									
									IF i = 23
										SET_VEHICLE_COLOURS(vehTrailerPark[i].veh,13,13)
									ENDIF
									
									IF i = 19
										SET_VEHICLE_COLOURS(vehTrailerPark[i].veh,13,13)
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[6].veh)
							//-- In front of stage
							DELETE_VEHICLE(vehTrailerPark[6].veh)
							
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[7].veh)
							//-- In front of stage
							DELETE_VEHICLE(vehTrailerPark[7].veh)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[15].veh)
							//--Convoy of lhs bikes
							DELETE_VEHICLE(vehTrailerPark[15].veh)
						//	gangEnemy[12].ped = CREATE_GANG_PED_INSIDE_VEHICLE(vehTrailerPark[15].veh, G_M_Y_LOST_01,grpEnemies, WEAPONTYPE_PISTOL)
						//	START_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[15].veh, 3,  "trevor1dw")
						//	PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[15].veh)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[16].veh)
							//--Convoy of lhs bikes
							DELETE_VEHICLE(vehTrailerPark[16].veh)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[17].veh)
							//--Convoy of lhs bikes
							DELETE_VEHICLE(vehTrailerPark[17].veh)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[20].veh)
							//--Convoy of rhs bikes
							DELETE_VEHICLE(vehTrailerPark[20].veh)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[21].veh)
							//--Convoy of rhs bikes
							DELETE_VEHICLE(vehTrailerPark[21].veh)
							
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[22].veh)
							//--Convoy of rhs bikes
							DELETE_VEHICLE(vehTrailerPark[22].veh)
							
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[24].veh)
							//--Convoy 2 of rhs bikes
							DELETE_VEHICLE(vehTrailerPark[24].veh)
							
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[25].veh)
							//--Convoy 2 of rhs bikes
							DELETE_VEHICLE(vehTrailerPark[25].veh)
							
						ENDIF

					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC 

PROC MANAGE_CHASE_HINT_CAM()
	
	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
		IF IS_VEHICLE_DRIVEABLE(vehTruck.veh) 
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(vehTruck.veh)
			ELSE
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
			ENDIF
		ELSE
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
		ENDIF
	ENDIF

ENDPROC

WEAPON_TYPE wtCurrentDriveby

PROC MANAGE_HELP_FOR_DRIVEBY()
	
	//PRINTSTRING("iDrive:")PRINTINT(iDriveByHelp)PRINTNL()

	SWITCH iDriveByHelp
		
		CASE 0 
			iGetDriveByTimer = GET_GAME_TIMER()
			iDriveByHelp ++
		BREAK
		
		CASE 1
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_VEH_AIM)
			OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_VEH_FLY_ATTACK)
				iDriveByHelp  = 99
			ELSE
				IF MANAGE_MY_TIMER(iGetDriveByTimer,35000)
					iDriveByHelp ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(cmbtChase[2].ped)
			OR NOT IS_PED_INJURED(cmbtChase[3].ped)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtCurrentDriveby,FALSE)
					IF wtCurrentDriveby = WEAPONTYPE_UNARMED
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV_DRIVEH1")
							PRINT_HELP("TRV_DRIVEH1")
						ENDIF
					ELSE
						CLEAR_HELP()
						IF NOT GET_IS_USING_ALTERNATE_DRIVEBY()
						AND NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							PRINT_HELP("TRV_DRIVEH2")
						ELSE
							PRINT_HELP("TRV_DRIVEH3")
						ENDIF
						iDriveByHelp ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_VEH_ATTACK)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV_DRIVEH2")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV_DRIVEH3")
					CLEAR_HELP()
					iDriveByHelp  = 99
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC


PROC DO_CHASE_HORNS(VEHICLE_INDEX vehClosest)
	//General horns for all vehicles: get the nearest vehicle and play horn
	IF GET_GAME_TIMER() - iTimeSinceLastHorn > 1800
		IF vehClosest != vehTruck.veh
		AND vehClosest != vehBike[0].veh
		AND vehClosest != vehBike[1].veh
			IF IS_VEHICLE_DRIVEABLE(vehClosest)
				PED_INDEX pedClosest = GET_PED_IN_VEHICLE_SEAT(vehClosest)
				
				IF NOT IS_PED_INJURED(pedClosest)
					START_VEHICLE_HORN(vehClosest, 2000)
								
					iTimeSinceLastHorn = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_TRAFFIC_VEHICLE_SEARCH_FLAGS()
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED)
ENDFUNC

PROC DO_AMBIENT_CHASE_DIALOGUE(VEHICLE_INDEX vehClosest)
	BOOL bForceDialogue = FALSE
	PED_INDEX pedCurrentAngryDriver
	
	IF vehClosest = SetPieceCarID[1]
	OR vehClosest = SetPieceCarID[2]
	OR vehClosest = SetPieceCarID[3]
	OR vehClosest = SetPieceCarID[4]
	OR vehClosest = SetPieceCarID[5]
		bForceDialogue = TRUE
	ENDIF
	
	IF GET_GAME_TIMER() - iAmbientDialogueTimer > 3000
	OR (bForceDialogue AND GET_GAME_TIMER() - iAmbientDialogueTimer > 1000)
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(vehClosest) AND vehClosest != vehTruck.veh
				AND vehClosest != vehBike[0].veh
				AND vehClosest != vehBike[1].veh
					BOOL bCrashed = FALSE
					BOOL bNearMiss = FALSE
				
					IF bForceDialogue
						bNearMiss = TRUE
					ELIF IS_ENTITY_TOUCHING_ENTITY(vehClosest, vehTrevor.veh)
						bCrashed = TRUE
					ELIF ABSF(GET_ENTITY_SPEED(vehClosest)) > 2.0
						IF ABSF(GET_ENTITY_HEADING(vehClosest) - GET_ENTITY_HEADING(vehTrevor.veh)) > 60.0 
							bNearMiss = TRUE
						ENDIF
					ENDIF

					IF bCrashed OR bNearMiss
						pedCurrentAngryDriver = GET_PED_IN_VEHICLE_SEAT(vehClosest)
						
						IF NOT IS_PED_INJURED(pedCurrentAngryDriver)											
							IF bCrashed
								PLAY_PED_AMBIENT_SPEECH(pedCurrentAngryDriver, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_NO_REPEAT_FRONTEND)
							ELSE
								PLAY_PED_AMBIENT_SPEECH(pedCurrentAngryDriver, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_NO_REPEAT_FRONTEND)
							ENDIF
							
							iAmbientDialogueTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iBikeAbuseTimer
INT iWaitTillResume
BOOL bChaseAbuse
INT iWarningTimer
BOOL bChaseDialogue[10]

BOOL T1M1_PLAN1
BOOL T1M1_LO1
BOOL T1M1_TR1

BOOL T1M1_CHAT1
BOOL T1M1_GRD
BOOL T1M1_GR2
BOOL T1M1_GR3
BOOL T1M1_BK1b
BOOL T1M1_PH6c
BOOL T1M1_GR7
BOOL T1M1_GR6
BOOL VANNERS
BOOL VANNERSPLAYED
BOOL T1M1_BK1a
BOOL T1M1_GR5
BOOL T1M1_GR4
BOOL T1M1_BIK

PROC MANAGE_DIALOGUE_FOR_CHASE()
	
PRINTSTRING("iChaseDialogue:")PRINTINT(iChaseDialogue)PRINTNL()
PRINTSTRING("fTime in Recording:")PRINTFLOAT(fPlaybackTimeT1)PRINTNL()
	IF iChaseDialogue > 0
		IF NOT bChaseDialogue[0]
			IF IS_PED_INJURED(cmbtChase[2].ped) OR IS_PED_INJURED(cmbtChase[3].ped)
				IF NOT IS_PED_INJURED(budRon.ped)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
						//IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
							PRINTSTRING("shot the first biker - reaction line")PRINTNL()
							INTERRUPT_CONVERSATION(budRon.ped,"T1M1_CSAA","NervousRon") 
							//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(budRon.ped,"T1M1_CSAA","NervousRon",SPEECH_PARAMS_INTERRUPT_SHOUTED)
							iWaitTillResume = GET_GAME_TIMER()
							bChaseDialogue[0] = TRUE
						ENDIF
					ELSE
						IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
							PRINTSTRING("shot the first biker - reaction line")PRINTNL()
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BK1", CONV_PRIORITY_MEDIUM)
								//INTERRUPT_CONVERSATION(budRon.ped,"T1M1_CSAA","FLOYD")
								iWaitTillResume = GET_GAME_TIMER()
								bChaseDialogue[0] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT bChaseDialogue[1]
				IF MANAGE_MY_TIMER(iWaitTillResume,1200)
					IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PRINTSTRING("resuming convo")PRINTNL()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							bChaseDialogue[1] = TRUE
						ELSE
							bChaseDialogue[1] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bChaseDialogue[0]
			IF NOT bChaseDialogue[2]
				IF IS_PED_INJURED(cmbtChase[2].ped) AND IS_PED_INJURED(cmbtChase[3].ped)
					IF NOT IS_PED_INJURED(budRon.ped)
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
								PRINTSTRING("shot the second biker - reaction line")PRINTNL()
								//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(budRon.ped,"T1M1_CRAA","NervousRon",SPEECH_PARAMS_SHOUTED_CRITICAL)
								INTERRUPT_CONVERSATION(budRon.ped,"T1M1_CRAA","NervousRon") 
								IF NOT IS_ENTITY_DEAD(vehTruck.veh)
									IF NOT DOES_BLIP_EXIST(vehTruck.blip)
										vehTruck.blip = CREATE_BLIP_FOR_VEHICLE(vehTruck.veh)
									ENDIF
								ENDIF
								bKilledBikersDuringChase = TRUE
								PRINTSTRING("HERE:1b")PRINTNL()
								bChaseDialogue[2] = TRUE
							ENDIF
						ELSE
							IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
								PRINTSTRING("shot the second biker - reaction line")PRINTNL()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BK1a", CONV_PRIORITY_MEDIUM)
									T1M1_BK1a = TRUE
									//INTERRUPT_CONVERSATION(budRon.ped,"T1M1_CSAA","FLOYD")
									iWaitTillResume = GET_GAME_TIMER()
									bChaseDialogue[2] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTSTRING("HERE:1")PRINTNL()
				IF NOT bChaseDialogue[3]
					IF NOT IS_PED_INJURED(budRon.ped)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_CV1", CONV_PRIORITY_MEDIUM)
									PRINTSTRING("follow back line Trevor")PRINTNL()
									//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"T1M1_EUAA","Trevor",SPEECH_PARAMS_SHOUTED_CRITICAL)
									iWaitTillResume = GET_GAME_TIMER()
									bChaseDialogue[3] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bChaseDialogue[4]
						IF MANAGE_MY_TIMER(iWaitTillResume,1200)
							IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
							AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
								IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									bChaseDialogue[4] = TRUE
								ELSE
									bChaseDialogue[4] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SWITCH iChaseDialogue
		
		//START
		CASE 0
			iTimer = GET_GAME_TIMER()
			bChaseDialogue[0] = FALSE
			bChaseDialogue[1] = FALSE
			bChaseDialogue[2] = FALSE
			bChaseDialogue[3] = FALSE
			bChaseDialogue[4] = FALSE
			T1M1_PLAN1 = FALSE
			T1M1_LO1 = FALSE
			T1M1_TR1 = FALSE
		 	T1M1_CHAT1 = FALSE
			T1M1_GRD = FALSE
			T1M1_GR2 = FALSE
			T1M1_GR3 = FALSE
			T1M1_BK1b = FALSE
			T1M1_PH6c = FALSE
			T1M1_GR7 = FALSE
			T1M1_GR6 = FALSE
			VANNERS = FALSE
			VANNERSPLAYED = FALSE
			T1M1_BK1a = FALSE
			T1M1_GR5 = FALSE
			T1M1_GR4 = FALSE
			T1M1_BIK = FALSE
			iChaseDialogue ++
		BREAK
		
		//INTRO DIALOGUE
		CASE 1
			IF MANAGE_MY_TIMER(iTimer,200)
				IF NOT T1M1_PLAN1
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PLAN1", CONV_PRIORITY_MEDIUM) // {Trevor tells them the plan} bikers
							IF NOT DOES_BLIP_EXIST(vehTruck.blip)
								vehTruck.blip = CREATE_BLIP_FOR_VEHICLE(vehTruck.veh)
							ENDIF
							T1M1_PLAN1 = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV_GT2")
							PRINT_GOD_TEXT("TRV_GT2") //Follow the van.
							SET_LABEL_AS_TRIGGERED("TRV_GT2", TRUE)
							REMOVE_ANIM_DICT("misstrevor1leadinout")
							iChaseDialogue ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//SECOND LINE OF THE PLAN
		CASE 2
			IF NOT IS_PED_INJURED(cmbtChase[2].ped)
			OR NOT IS_PED_INJURED(cmbtChase[3].ped)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_PLAN2")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PLAN2", CONV_PRIORITY_MEDIUM) // {Trevor tells them the plan} - 2
							SET_LABEL_AS_TRIGGERED("T1M1_PLAN2", TRUE)
							iChaseDialogue = 99
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_LABEL_AS_TRIGGERED("T1M1_PLAN2", TRUE)
				iChaseDialogue = 99
			ENDIF
		BREAK
		
		//KILLED A RIDER - HIGHEST PRIORITY
		CASE 3
			//NOW AMBIENT SPEECH

		BREAK
		
		//TRUCK COMING - MEDIUM PRIORITY
		CASE 4
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_PH6a")
				IF bWatchTruckDialogue[0] = TRUE
					IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
					AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[2])
							IF NOT IS_PED_INJURED(budWade.ped)
								INTERRUPT_CONVERSATION(budWade.ped,"T1M1_CVAA","Wade") 
								PRINTSTRING("ASSIGN WADE LOOK AT TASK")PRINTNL()
								TASK_LOOK_AT_ENTITY(budWade.ped,SetPieceCarID[2],3000)
								SET_LABEL_AS_TRIGGERED("T1M1_PH6a", TRUE)
								iChaseDialogue = 99
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_PH6b")
				IF bWatchTruckDialogue[1] = TRUE
					IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
					AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[3])
							IF NOT IS_PED_INJURED(budWade.ped)
								INTERRUPT_CONVERSATION(budWade.ped,"T1M1_CVAA","Wade") 
								PRINTSTRING("ASSIGN WADE LOOK AT TASK")PRINTNL()
								TASK_LOOK_AT_ENTITY(budWade.ped,SetPieceCarID[3],4000)
								SET_LABEL_AS_TRIGGERED("T1M1_PH6b", TRUE)
								iChaseDialogue = 99
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//FALLING BEHIND - HIGHEST PRIORITY
		CASE 5
			
			IF fPlaybackTimeT1 > 2000
				IF NOT T1M1_LO1
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("TRV_GT2")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_LO1", CONV_PRIORITY_MEDIUM)
								T1M1_LO1 = TRUE
								T1M1_TR1 = FALSE
								PRINTSTRING("@@@@@@@@@@@@@@@@@@@@HERE1")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT T1M1_TR1
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_TR1", CONV_PRIORITY_MEDIUM)
								T1M1_TR1 = TRUE
								iFailTimer = GET_GAME_TIMER()
								PRINTSTRING("@@@@@@@@@@@@@@@@@@@@HERE2")
								iChaseDialogue = 99
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		//DIRECTIONS - HIGHEST PRIORITY
		CASE 6
			IF bDirectionsDialogue[0] = TRUE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_PH6a")
					IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
					AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PH6a", CONV_PRIORITY_HIGH) // Stay on the high road here.
								SET_LABEL_AS_TRIGGERED("T1M1_PH6a", TRUE)
								iChaseDialogue = 99
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDirectionsDialogue[1] = TRUE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_PH7")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1554.187134,4565.204590,40.332378>>, <<1602.851074,4569.142578,61.203342>>, 32.500000)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
						AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PH7", CONV_PRIORITY_HIGH) // Shortcut right
									SET_LABEL_AS_TRIGGERED("T1M1_PH7", TRUE)
									iChaseDialogue = 99
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SET_LABEL_AS_TRIGGERED("T1M1_PH7", TRUE)
						iChaseDialogue = 99
					ENDIF
				ENDIF
			ENDIF
			
			//INTERRUPT_CONVERSATION(budRon.ped,"T1M1_CSAA","NervousRon") 
			
			IF bDirectionsDialogue[2] = TRUE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_CH3")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<851.478516,4493.242676,66.635124>>, <<927.061279,4454.146973,48.634125>>, 42.250000)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
						AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_CH3", CONV_PRIORITY_HIGH) // Sharp left 1
									SET_LABEL_AS_TRIGGERED("T1M1_CH3", TRUE)
									iChaseDialogue = 99
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SET_LABEL_AS_TRIGGERED("T1M1_CH3", TRUE)
						iChaseDialogue = 99
					ENDIF
				ENDIF
			ENDIF
			
			IF bDirectionsDialogue[3] = TRUE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_WRONG")
					IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
					AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_WRONG", CONV_PRIORITY_HIGH) // Wrong way
								iChaseDialogue = 99
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDirectionsDialogue[4] = TRUE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_PH5")
					IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
					AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<484.566223,4313.895996,63.555454>>, <<588.797363,4230.479492,41.473400>>, 68.500000)
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PH5", CONV_PRIORITY_HIGH) // Jump
									iChaseDialogue = 99
								ENDIF
							ENDIF
						ELSE
							iChaseDialogue = 99
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		//SHOT AT 
		CASE 7
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_SH1")
				IF NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
				AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_SH1", CONV_PRIORITY_MEDIUM) // I thought you said not to kill 'em.
							SET_LABEL_AS_TRIGGERED("T1M1_SH1", TRUE)
							IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehTruck.veh)
							ENDIF
							iWarningTimer = GET_GAME_TIMER()
							iChaseDialogue = 99
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//GENERAL CHAT - LOW PRIORITY
		CASE 99
			
			//FALLING BEHIND
			
			IF NOT T1M1_LO1
				IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
					IF GET_DISTANCE_BETWEEN_ENTITIES(vehTruck.veh, PLAYER_PED_ID()) > 120.0
						KILL_FACE_TO_FACE_CONVERSATION()
						iChaseDialogue = 5
					ENDIF
				ENDIF
			ENDIF

			IF GET_DISTANCE_BETWEEN_ENTITIES(vehTruck.veh, PLAYER_PED_ID()) < 120.0
								
				//SHOT AT THE VAN
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_SH1")
					IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vehTruck.veh,WEAPONTYPE_SAWNOFFSHOTGUN)
						OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vehTruck.veh,WEAPONTYPE_PISTOL)
							iChaseDialogue = 7
						ENDIF
					ENDIF
				ELSE
					IF MANAGE_MY_TIMER(iWarningTimer,12000)
						SET_LABEL_AS_TRIGGERED("T1M1_SH1",FALSE)
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("T1M1_SH1")
					IF MANAGE_MY_TIMER(iTimer,14000)
						IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
							SET_LABEL_AS_TRIGGERED("T1M1_SH1", FALSE)
							CLEAR_ENTITY_LAST_WEAPON_DAMAGE(vehTruck.veh)
						ENDIF
					ENDIF
				ENDIF
				
				//WATCH THAT TRUCK
				IF bWatchTruckDialogue[0] = FALSE
					IF fPlaybackTimeT1 > 11000 AND fPlaybackTimeT1 < 13500
						IF NOT IS_ENTITY_DEAD(SetPieceCarID[2])
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								IF GET_DISTANCE_BETWEEN_ENTITIES(SetPieceCarID[2], PLAYER_PED_ID()) < 100.0
								AND IS_ENTITY_IN_ANGLED_AREA(vehTrevor.veh, <<1522.839844,4546.914551,49.740723>>, <<1462.441528,4495.471191,55.657627>>, 23.750000)
									KILL_FACE_TO_FACE_CONVERSATION()
									bWatchTruckDialogue[0] = TRUE
									iChaseDialogue = 4
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//WATCH THAT TRUCK
				IF bWatchTruckDialogue[1] = FALSE
					IF fPlaybackTimeT1 > 27032.726563 AND fPlaybackTimeT1  < 32271.675781
						IF NOT IS_ENTITY_DEAD(SetPieceCarID[3])
							IF GET_DISTANCE_BETWEEN_ENTITIES(SetPieceCarID[3], PLAYER_PED_ID()) < 100.0
								KILL_FACE_TO_FACE_CONVERSATION()
								bWatchTruckDialogue[1] = TRUE
								iChaseDialogue = 4
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// SHORTCUT RIGHT
				IF bDirectionsDialogue[1] = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1540.914795,4560.515137,60.550842>>, <<1608.807495,4570.183594,44.297668>>, 23.500000)
						KILL_FACE_TO_FACE_CONVERSATION()
						bDirectionsDialogue[1] = TRUE
						iChaseDialogue = 6
					ENDIF
				ENDIF
				
				//SHARP LEFT
				IF bDirectionsDialogue[2] = FALSE
					//IF fPlaybackTimeT1 > 57967.902344
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<851.478516,4493.242676,66.635124>>, <<927.061279,4454.146973,48.634125>>, 42.250000)
									KILL_FACE_TO_FACE_CONVERSATION()
									bDirectionsDialogue[2] = TRUE
									iChaseDialogue = 6
								ENDIF
							ENDIF
						ENDIF
					//ENDIF
				ENDIF
				
				//WRONG WAY
				IF bDirectionsDialogue[3] = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1693.080078,4644.204102,38.856388>>, <<1740.775391,4590.881348,45.142933>>, 16.250000)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1442.973389,4475.325195,45.255730>>, <<1424.131958,4421.521973,57.023331>>, 16.250000)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<803.660156,4214.710938,36.835331>>, <<724.126343,4179.515625,53.708752>>, 16.250000)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<153.144226,4434.268066,65.043114>>, <<106.765038,4484.393066,99.222374>>, 16.250000)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-215.058716,4222.676270,32.509438>>, <<-278.967010,4226.645996,56.414246>>, 16.250000)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-267.876709,3942.875732,30.224384>>, <<-322.664917,4011.222412,58.215500>>, 16.250000)
								KILL_FACE_TO_FACE_CONVERSATION()
								bDirectionsDialogue[3] = TRUE
								iWrongWayDialogue = GET_GAME_TIMER()
								iChaseDialogue = 6
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MANAGE_MY_TIMER(iWrongWayDialogue,9000)
						bDirectionsDialogue[3] = FALSE
					ENDIF
				ENDIF
				
				//JUMP COMING UP
				IF bDirectionsDialogue[4] = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<484.566223,4313.895996,63.555454>>, <<588.797363,4230.479492,41.473400>>, 68.500000)
								KILL_FACE_TO_FACE_CONVERSATION()
								bDirectionsDialogue[4] = TRUE
								iChaseDialogue = 6
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					

				//RESET FALLING BEHIND DIALOGUE
				IF T1M1_TR1
					IF MANAGE_MY_TIMER(iFailTimer,15000)
						T1M1_LO1 = FALSE
						T1M1_TR1 = FALSE
					ENDIF
				ENDIF

				
				//GENERAL BANTER
				IF fPlaybackTimeT1 > 13500
					IF NOT T1M1_CHAT1
						IF HAS_LABEL_BEEN_TRIGGERED("T1M1_PH7")
						OR fPlaybackTimeT1 > 31917.000000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									//We did alright back there, didn't we?
									IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_CHAT2", CONV_PRIORITY_MEDIUM) // CHAT
										T1M1_CHAT1 = TRUE
										iChaseDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT T1M1_GRD
							IF MANAGE_MY_TIMER(iChaseDialogue,1000)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//SEATBELTS
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_GRD", CONV_PRIORITY_MEDIUM) // CHAT
											iChaseDialogueTimer = GET_GAME_TIMER()
											T1M1_GRD = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT T1M1_GR2
								//IF fPlaybackTimeT1 > 32271.675781
								
								IF HAS_LABEL_BEEN_TRIGGERED("T1M1_CH3")
								OR MANAGE_MY_TIMER(iChaseDialogueTimer,20000)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										//I CANT SEE US TAKING OUT ALL THE BIKERS THIS GO
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_GR2", CONV_PRIORITY_MEDIUM) // CHAT
												iChaseDialogueTimer = GET_GAME_TIMER()
												T1M1_GR2 = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								//ENDIF
							ELSE
								IF NOT T1M1_GR3
									IF HAS_LABEL_BEEN_TRIGGERED("T1M1_CH3")
									OR MANAGE_MY_TIMER(iChaseDialogueTimer,15000)
										IF fPlaybackTimeT1 > 101481.953125
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												//THIS ROAD IS KINDA DANGEROUS
												//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													//IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_GR3", CONV_PRIORITY_MEDIUM) // CHAT
														iChaseDialogueTimer = GET_GAME_TIMER()
														T1M1_GR3 = TRUE
													//ENDIF
												//ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT T1M1_BK1b
										IF MANAGE_MY_TIMER(iChaseDialogueTimer,6000)
											IF fPlaybackTimeT1 > 101481.953125
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													//IT'S AWFUL DUSTY OUT HERE
													//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
														//IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BK1b", CONV_PRIORITY_MEDIUM) // CHAT
														T1M1_BK1b = TRUE
														//ENDIF
													//ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF NOT T1M1_PH6c
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												//You want me to mount you on the grill so you can act as a lookout?
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													//IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PH6c", CONV_PRIORITY_MEDIUM) // CHAT
														T1M1_PH6c = TRUE
														iChaseDialogueTimer = GET_GAME_TIMER()
													//ENDIF
												ENDIF
											ENDIF
										ELSE
											IF NOT T1M1_GR7
											//Do we know where they're going yet?
												IF MANAGE_MY_TIMER(iChaseDialogueTimer,6000)
													IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
														IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
															IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
																IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_GR7", CONV_PRIORITY_MEDIUM) // CHAT
																	T1M1_GR7 = TRUE
																ENDIF
															ENDIF
														ENDIF
													ELSE
														//WHICH OF YOU TWEAKED OUT IMBECILES WANTS TO KILL SOME BIKERS?
														IF NOT T1M1_GR6
															IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
																IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
																	IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_GR6", CONV_PRIORITY_MEDIUM) // CHAT
																		T1M1_GR6 = TRUE
																		T1M1_GR7 = TRUE
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ELSE
												IF NOT VANNERS
													//SHOULDN'T THESE BIKERS BE ON BIKES?
													IF MANAGE_MY_TIMER(iChaseDialogueTimer,6000)
														IF T1M1_BK1a
															IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
																IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
																	IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_GR4", CONV_PRIORITY_MEDIUM) // CHAT
																		SET_LABEL_AS_TRIGGERED("VANNERSPLAYED", TRUE)
																		VANNERSPLAYED = TRUE
																		VANNERS = TRUE
																		iChaseDialogueTimer = GET_GAME_TIMER()
																	ENDIF
																ENDIF
															ENDIF
														ELSE
															VANNERS = TRUE
														ENDIF
													ENDIF
												ELSE
													IF NOT T1M1_GR5
														//JOHNNY TOLD ME HE ONLY BEEN SMOKING A COUPLE OF YEARS
														IF MANAGE_MY_TIMER(iChaseDialogueTimer,6000)
															IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
																IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
																	//IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_GR5", CONV_PRIORITY_MEDIUM) // CHAT
																		T1M1_GR5 = TRUE
																	//ENDIF
																ENDIF
															ENDIF
														ENDIF
													ELSE
														IF NOT T1M1_GR4
															//SHOULDN'T THESE BIKERS BE ON BIKES?
															IF NOT VANNERSPLAYED
																IF T1M1_BK1a
																	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
																		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
																			IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_GR4", CONV_PRIORITY_MEDIUM) // CHAT
																				T1M1_GR4 = TRUE
																			ENDIF
																		ENDIF
																	ENDIF
																ELSE
																	T1M1_GR4 = TRUE
																ENDIF
															ELSE
																T1M1_GR4 = TRUE
															ENDIF
														ELSE
															IF NOT bChaseAbuse
																IF fPlaybackTimeT1 < 166729.828125
																	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
																		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
																			IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_CHASE", CONV_PRIORITY_MEDIUM) // {General Trevor chase lines}
																				bChaseAbuse = TRUE
																				iBikeAbuseTimer = GET_GAME_TIMER()
																			ENDIF
																		ENDIF
																	ENDIF
																ENDIF
															ELSE
																IF MANAGE_MY_TIMER(iBikeAbuseTimer,GET_RANDOM_INT_IN_RANGE(7000,15000))
																	 bChaseAbuse = FALSE
																ENDIF
															ENDIF
															IF fPlaybackTimeT1 > 166729.828125
																IF NOT T1M1_BIK
																	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
																		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
																			IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BIK", CONV_PRIORITY_MEDIUM) // SLOWING DOWN
																				T1M1_BIK = TRUE
																			ENDIF
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC
PROC DO_HINT_CAM()
	
	IF e_section_stage != SECTION_STAGE_CLEANUP	
		IF fPlaybackTimeT1 > 5500.0
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			AND IS_VEHICLE_DRIVEABLE(vehTruck.veh)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehTruck.veh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_GAMEPLAY_HINT_ACTIVE()
		IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_1_FOCUS_CAM_SCENE")
			START_AUDIO_SCENE("TREVOR_1_FOCUS_CAM_SCENE")
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_FOCUS_CAM_SCENE")
			STOP_AUDIO_SCENE("TREVOR_1_FOCUS_CAM_SCENE")
		ENDIF
	ENDIF
	
ENDPROC

PROC RUBBER_BAND_TEST(FLOAT &fCurrentSpeed, VEHICLE_INDEX vehPlayer, VEHICLE_INDEX vehTrigger)
	
	//63 - Ideal
	//34 - Min
	//80 - Max
	//100 - Losing
	
	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehTruck.veh, PLAYER_PED_ID(), fUberSpeed, 63.0, 34.0,80.0, 0.6,1.4)
	
	IF IS_VEHICLE_DRIVEABLE(vehTrigger)
	AND IS_VEHICLE_DRIVEABLE(vehPlayer)
		FLOAT fDesiredPlaybackSpeed = 1.0
		FLOAT fAccel = 0.01
		
		//Default values for any adjustments to the player's speed and the playback accel.
		
		//Special cases: at these points we want the buddy to be further from or nearer to the player than normal
		IF fPlaybackTimeT1 < 7500.0 
			//At start: keep them further apart to avoid U-turn issues.
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 20, 63, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 23000 AND fPlaybackTimeT1 < 36000
			//Just as you come up to the short cut - slow them down slightly due to the winding roads
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 15, 53, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 36000 AND fPlaybackTimeT1 < 55800
			//Coming out of the winding section
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 20, 63, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 55800 AND fPlaybackTimeT1 < 63000
			//Speed up a bit around the bend so we don't hit them
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 22, 73, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 63000 AND fPlaybackTimeT1 < 72480
			//Coming up on the straight get closer
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 18, 55, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 72480 AND fPlaybackTimeT1 < 84493
			//Just on the bend let them get ahead a bit
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 20, 63, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 84493 AND fPlaybackTimeT1 < 94784
			//Bit of straight after the bend - regular speed
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 20, 63, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 94784 AND fPlaybackTimeT1 < 101074
			//Shortcut stunt jump- let them go a bit infront
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 22, 73, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 101074 AND fPlaybackTimeT1 < 110000
			//Passed the jump section - you sometimes get a bit too close here
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 20, 66, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fPlaybackTimeT1 > 110000 AND fPlaybackTimeT1 < 110000
			//Passed the jump section - you sometimes get a bit too close here
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 20, 63, 80.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ENDIF
			
		//Special cases where desired speed is forced to a specific value.
		IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			fDesiredPlaybackSpeed = 1.0
		ENDIF
				
		//Don't do any slowdown after going past the cutscene trigger.
		IF fPlaybackTimeT1 > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(200, sAwUberRec) - 4000.0
			IF fDesiredPlaybackSpeed < 1.0
				fDesiredPlaybackSpeed = 1.0
			ENDIF
		ENDIF	
		
		UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(fCurrentSpeed, fDesiredPlaybackSpeed, fAccel)
	ENDIF
ENDPROC

BOOL bGiveWadeLookAtTask = FALSE

PROC MANAGE_WADE_LOOK_AT()
	IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)	
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF NOT IS_PED_INJURED(budWade.ped)
				IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
					IF NOT bGiveWadeLookAtTask
						TASK_LOOK_AT_ENTITY(budWade.ped,vehTruck.veh,-1,SLF_WHILE_NOT_IN_FOV)
						bGiveWadeLookAtTask = TRUE
					ENDIF
				ENDIF
			ELSE
				IF bGiveWadeLookAtTask
					IF NOT IS_PED_INJURED(budWade.ped)
						TASK_CLEAR_LOOK_AT(budWade.ped)
					ENDIF
					bGiveWadeLookAtTask = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

BOOL bVehTruckMix = FALSE
BOOL bVehBike0Mix = FALSE
BOOL bVehBike1Mix = FALSE
BOOL T1M1_WADEGBU = FALSE

//BOOL bNeedToDoRidersKilledText, bDoneRidersKilledText
FUNC BOOL stageChaseV2()

	SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_DRIVING)
	
	#IF IS_DEBUG_BUILD 
		DONT_DO_J_SKIP(sLocatesData)
	#ENDIF
	
	//ADD_PED_FOR_DIALOGUE(structConvoPeds,7,cmbtChase[3].ped,"CLAY")
	//ADD_PED_FOR_DIALOGUE(structConvoPeds,6,cmbtChase[2].ped,"TERRY")
	
	IF DOES_ENTITY_EXIST(cmbtChase[2].ped)
		IF IS_PED_INJURED(cmbtChase[2].ped)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TERRY DEATH")
				REMOVE_PED_FOR_DIALOGUE(structConvoPeds,6)
				IF NOT IS_PED_INJURED(cmbtChase[3].ped)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(cmbtChase[3].ped,"T1M1_GBAA","CLAY",SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
				ENDIF
				SET_LABEL_AS_TRIGGERED("TERRY DEATH",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(cmbtChase[3].ped)
		IF IS_PED_INJURED(cmbtChase[3].ped)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("CLAY DEATH")
				//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(cmbtChase[3].ped,"T1M1_GBAA","CLAY",SPEECH_PARAMS_FORCE)
				REMOVE_PED_FOR_DIALOGUE(structConvoPeds,7)
				IF NOT IS_PED_INJURED(cmbtChase[2].ped)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(cmbtChase[2].ped,"T1M1_GCAA","TERRY",SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
				ENDIF
				SET_LABEL_AS_TRIGGERED("CLAY DEATH",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(cmbtChase[2].ped)
		IF IS_PED_INJURED(cmbtChase[2].ped)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("CLAY DEATH")
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(cmbtChase[2].ped,FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 40
					IF NOT IS_ENTITY_ON_SCREEN(cmbtChase[2].ped)
						DELETE_PED(cmbtChase[2].ped)
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_TERRY)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(cmbtChase[3].ped)
		IF IS_PED_INJURED(cmbtChase[3].ped)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(cmbtChase[3].ped,FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 40
				IF NOT IS_ENTITY_ON_SCREEN(cmbtChase[3].ped)
					DELETE_PED(cmbtChase[3].ped)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_CLAY)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(cmbtChase[3].ped)
		SET_ENTITY_HEALTH(cmbtChase[3].ped, 175)
	ENDIF

	//AUDIO SCENE
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_1_CHASE_KILL_BIKERS")
		START_AUDIO_SCENE("TREVOR_1_CHASE_KILL_BIKERS")
		SET_LABEL_AS_TRIGGERED("TREVOR_1_CHASE_KILL_BIKERS",TRUE)
	ELSE
		IF IS_PED_INJURED(cmbtChase[2].ped)
		AND IS_PED_INJURED(cmbtChase[3].ped)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("STOP_TREVOR_1_CHASE_KILL_BIKERS")
				IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_CHASE_KILL_BIKERS")
					STOP_AUDIO_SCENE("TREVOR_1_CHASE_KILL_BIKERS")
					SET_LABEL_AS_TRIGGERED("STOP_TREVOR_1_CHASE_KILL_BIKERS",TRUE)
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_1_CHASE_FOLLOW_VAN")
					START_AUDIO_SCENE("TREVOR_1_CHASE_FOLLOW_VAN")
					SET_LABEL_AS_TRIGGERED("TREVOR_1_CHASE_FOLLOW_VAN",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//MIX GROUPS
	IF bSetUpStageData = TRUE
		IF NOT bVehTruckMix
			IF DOES_ENTITY_EXIST(vehTruck.veh)
				IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTruck.veh,"TREVOR_1_CHASE_VAN_Group")
					bVehTruckMix = TRUE
				ENDIF
			ENDIF
		ENDIF

		IF NOT bVehBike0Mix
			IF DOES_ENTITY_EXIST(vehBike[0].veh)
				IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehBike[0].veh,"TREVOR_1_BIKES_GROUP")
					bVehBike0Mix = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bVehBike1Mix
			IF DOES_ENTITY_EXIST(vehBike[1].veh)
				IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehBike[1].veh,"TREVOR_1_BIKES_GROUP")
					bVehBike1Mix = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_INJURED(cmbtChase[2].ped)
	AND IS_PED_INJURED(cmbtChase[3].ped)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehBike[0].veh)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehBike[1].veh)
	ENDIF

	IF GET_DISTANCE_BETWEEN_COORDS(<< 58.4031, 3630.9597, 38.7482 >>,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 100
		MANAGE_WADE_IN_BACK_SEAT()
	ENDIF

	//INFORM_MISSION_STATS_OF_INCREMENT(TRV1_SHORTCUTS)
	//FLOAT fCUrPlayabackTime
	//DUMMY_REFERENCE_FLOAT(fUberSpeed)
	
	//Get the closest vehicle to the player periodically: this is used to check dialogue and horn triggers elsewhere.
	VEHICLE_INDEX vehClosest
	IF GET_GAME_TIMER() - iClosestVehicleCheckTimer > 200
		vehClosest = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 10.0, DUMMY_MODEL_FOR_SCRIPT, GET_TRAFFIC_VEHICLE_SEARCH_FLAGS())
		iClosestVehicleCheckTimer = GET_GAME_TIMER()
	ENDIF
	
	IF bClearUpTrailerPark = FALSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<48.109467,3642.522217,38.334419>>) < 100
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
			
			ADD_SCENARIO_BLOCKING_AREA(vTrailerPark + <<100,100,100>>, vTrailerPark - <<100,100,100>>)
			CLEAR_AREA(vTrailerPark,500,TRUE,FALSE)
			
			bClearUpTrailerPark = TRUE
		ENDIF
	ENDIF

	DO_HINT_CAM()
	MANAGE_WADE_LOOK_AT()
	MANAGE_HELP_FOR_DRIVEBY()
	
	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
		IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
			DO_CHASE_HORNS(vehClosest)
			DO_AMBIENT_CHASE_DIALOGUE(vehClosest)
		ENDIF
	ENDIF
	
	IF e_section_stage 	= SECTION_STAGE_SETUP
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Chase the bikers")
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
		
		ADD_SCENARIO_BLOCKING_AREA(vTrailerPark + <<100,100,100>>, vTrailerPark - <<100,100,100>>)
		CLEAR_AREA(vTrailerPark,500,TRUE,FALSE)
		
		IF NOT IS_ENTITY_DEAD(cmbtChase[2].ped)
			SET_ENTITY_INVINCIBLE(cmbtChase[2].ped, FALSE)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(cmbtChase[3].ped)
			SET_ENTITY_INVINCIBLE(cmbtChase[3].ped, FALSE)
		ENDIF
		
		fTimeForUberTrailerAndTruck = 127000.0

		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		IF NOT IS_ENTITY_DEAD(vehTruck.veh)
			IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
				IF NOT IS_ENTITY_DEAD(vehBike[1].veh) 
					IF NOT DOES_BLIP_EXIST(cmbtChase[2].blip)
						cmbtChase[2].blip = CREATE_BLIP_FOR_PED(cmbtChase[2].ped, TRUE)
					ENDIF

					IF NOT DOES_BLIP_EXIST(cmbtChase[3].blip)
						cmbtChase[3].blip = CREATE_BLIP_FOR_PED(cmbtChase[3].ped,TRUE)
					ENDIF

					IF NOT IS_PED_INJURED(cmbtChase[2].ped)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cmbtChase[2].ped,TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(cmbtChase[2].ped, grpEnemies)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,grpEnemies)
						SET_PED_CAN_BE_DRAGGED_OUT(cmbtChase[2].ped,FALSE)
					ENDIF
					
					IF NOT IS_PED_INJURED(cmbtChase[3].ped)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cmbtChase[3].ped,TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(cmbtChase[3].ped, grpEnemies)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,grpEnemies)
						SET_PED_CAN_BE_DRAGGED_OUT(cmbtChase[3].ped,FALSE)
					ENDIF
					
					//WARP BIKERS
					IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
						IF NOT IS_PED_INJURED(cmbtChase[2].ped)
							IF NOT IS_PED_IN_VEHICLE(cmbtChase[2].ped,vehBike[0].veh)
								SET_PED_INTO_VEHICLE(cmbtChase[2].ped, vehBike[0].veh, VS_DRIVER)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
						IF NOT IS_PED_INJURED(cmbtChase[3].ped)
							IF NOT IS_PED_IN_VEHICLE(cmbtChase[3].ped,vehBike[1].veh)
								SET_PED_INTO_VEHICLE(cmbtChase[3].ped, vehBike[1].veh, VS_DRIVER)
							ENDIF
						ENDIF
					ENDIF

					//-- WARP_CHASE_PEDS() is starting a playback
					REQUEST_VEHICLE_RECORDING(202, sAwUberRec) //-- Truck
					REQUEST_VEHICLE_RECORDING(203, sAwUberRec) //-- Trailer for truck
					
					REQUEST_MODEL(TRFLAT)
					REQUEST_MODEL(SCRAP)
					ROPE_LOAD_TEXTURES()
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
						REQUEST_VEHICLE_RECORDING(200, sAwUberRec) // Trigger car
						REQUEST_VEHICLE_RECORDING(204, sAwUberRec) //-- Bike 0
						REQUEST_VEHICLE_RECORDING(205, sAwUberRec) //-- Bike 1

						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(200, sAwUberRec)
						AND HAS_VEHICLE_RECORDING_BEEN_LOADED(204, sAwUberRec)
						AND HAS_VEHICLE_RECORDING_BEEN_LOADED(205, sAwUberRec)
						AND ROPE_ARE_TEXTURES_LOADED()
				
							CLEANUP_UBER_PLAYBACK()
							INITIALISE_UBER_PLAYBACK(sAwUberRec, 200)
							INIT_UBER_DATA()
							
							STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh)
							
							START_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh, 200, sAwUberRec)
							IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(vehTruck.veh)
								SET_VEHICLE_ENGINE_ON(vehTruck.veh,TRUE,TRUE)
							ENDIF
							
							IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(vehBike[0].veh)
								SET_VEHICLE_ENGINE_ON(vehBike[0].veh,TRUE,TRUE)
							ENDIF
							
							IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(vehBike[1].veh)
								SET_VEHICLE_ENGINE_ON(vehBike[1].veh,TRUE,TRUE)
							ENDIF
							
							START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehBike[0].veh, 204, sAwUberRec,1,0,DRIVINGMODE_AVOIDCARS_RECKLESS)
							START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehBike[1].veh, 205, sAwUberRec,1,0,DRIVINGMODE_AVOIDCARS_RECKLESS)
							
							IF bSkipped = TRUE
								PRINTSTRING("WESTWOOD - SKIPPING RECORDING")PRINTNL()
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh, 2400.0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[0].veh)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh, 2400.0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[1].veh)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck.veh, 2400.0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehTruck.veh)
								
								SET_VEHICLE_FORWARD_SPEED(vehTrevor.veh, 15)
								
								IF DOES_ENTITY_EXIST(cmbtChase[0].ped)
									IF NOT IS_PED_INJURED(cmbtChase[0].ped)
										SET_PED_INTO_VEHICLE(cmbtChase[0].ped,vehTruck.veh)
									ENDIF
								ENDIF
								
							ENDIF
							
							PRINTSTRING("WESTWOOD - SETUP CALLED")PRINTNL()
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,FALSE)
							
							IF NOT IS_PED_INJURED(budRon.ped)
								SET_PED_CONFIG_FLAG(budRon.ped,PCF_WillFlyThroughWindscreen,FALSE)
								REMOVE_PED_FROM_GROUP(budRon.ped)
							ENDIF	
							
							IF NOT IS_PED_INJURED(budWade.ped)
								SET_PED_CONFIG_FLAG(budWade.ped,PCF_WillFlyThroughWindscreen,FALSE)
								
								REMOVE_PED_FROM_GROUP(budWade.ped)
							ENDIF	
						ENDIF
					ELSE
					
						iTimer = GET_GAME_TIMER()
						iTruckAndTrailerSetpieceProg = 0
						bGiveWadeLookAtTask = FALSE
						safeFadeInAndWait()
						bDoFails = TRUE
						bDoFailLine = FALSE
						SET_VEHICLE_MODEL_IS_SUPPRESSED(HEXER,TRUE)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(GBURRITO,TRUE)
						SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_01,TRUE)
						SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_02,TRUE)
						SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_03,TRUE)
						e_section_stage = SECTION_STAGE_RUNNING//SECTION_DEBUG //SECTION_STAGE_RUNNING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
		IF NOT bTrevorSplashBackEffectOn
			APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
			bTrevorSplashBackEffectOn = TRUE
		ENDIF	

	ENDIF

	IF e_section_stage = SECTION_STAGE_RUNNING	
		//fCUrPlayabackTime = 0

		IF bSetUpStageData = FALSE
			bVehTruckMix = FALSE
			bVehBike0Mix = FALSE
			bVehBike1Mix = FALSE
			bBeenToTrailerPark = FALSE
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_CHASING")
				IF TRIGGER_MUSIC_EVENT("TRV1_CHASING")
					SET_LABEL_AS_TRIGGERED("TRV1_CHASING",TRUE)
				ENDIF
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(structConvoPeds,7,cmbtChase[3].ped,"CLAY")
			ADD_PED_FOR_DIALOGUE(structConvoPeds,6,cmbtChase[2].ped,"TERRY")
			
			SETUP_PEDS_FOR_DIALOGUE()
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Chase the bikers")
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(),RELGROUPHASH_PLAYER)
			
			IF NOT IS_PED_INJURED(cmbtChase[2].ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cmbtChase[2].ped,TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(cmbtChase[2].ped, grpEnemies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,grpEnemies)
				SET_PED_DIES_WHEN_INJURED(cmbtChase[2].ped,TRUE)
				SET_PED_CONFIG_FLAG(cmbtChase[2].ped, PCF_DisableHurt, TRUE)
				SET_ENTITY_LOD_DIST(cmbtChase[2].ped,250)
				SET_PED_CAN_BE_DRAGGED_OUT(cmbtChase[2].ped,FALSE)
				IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
					SET_VEHICLE_TYRES_CAN_BURST(vehBike[0].veh,FALSE)
					SET_ENTITY_LOD_DIST(vehBike[0].veh,250)
				ENDIF
			ENDIF

			IF NOT IS_PED_INJURED(cmbtChase[3].ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(cmbtChase[3].ped,TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(cmbtChase[3].ped, grpEnemies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,grpEnemies)
				SET_PED_DIES_WHEN_INJURED(cmbtChase[3].ped,TRUE)
				SET_PED_CONFIG_FLAG(cmbtChase[3].ped, PCF_DisableHurt, TRUE)
				SET_ENTITY_LOD_DIST(cmbtChase[3].ped,250)
				SET_PED_CAN_BE_DRAGGED_OUT(cmbtChase[3].ped,FALSE)
				IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
					SET_VEHICLE_TYRES_CAN_BURST(vehBike[1].veh,FALSE)
					SET_ENTITY_LOD_DIST(vehBike[1].veh,250)
				ENDIF
			ENDIF
			
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,FALSE)
			
			IF NOT IS_PED_INJURED(budRon.ped)
				SET_PED_CONFIG_FLAG(budRon.ped,PCF_WillFlyThroughWindscreen,FALSE)
				REMOVE_PED_FROM_GROUP(budRon.ped)
			ENDIF	
			
			IF NOT IS_PED_INJURED(budWade.ped)
				SET_PED_CONFIG_FLAG(budWade.ped,PCF_WillFlyThroughWindscreen,FALSE)
				REMOVE_PED_FROM_GROUP(budWade.ped)
			ENDIF	
			
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PISTOL)
				IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PISTOL) < 65
					ADD_AMMO_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_PISTOL,  65 - GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PISTOL))		
				ENDIF
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PISTOL)
			ELSE
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 65, TRUE)
			ENDIF
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<24.5298, 3590.7327, 0.0>>, <<147.1179, 3772.4255,100>>, FALSE)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  5.9328, 3682.4102,0.0 >>,<<110.0308, 3801.6506, 100.0>>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<24.5298, 3590.7327, 0.0>>, <<147.1179, 3772.4255,100>>)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<  5.9328, 3682.4102,0.0 >>,<<110.0308, 3801.6506, 100.0>>)
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
			
			ADD_SCENARIO_BLOCKING_AREA(vTrailerPark + <<100,100,100>>, vTrailerPark - <<100,100,100>>)
			CLEAR_AREA(vTrailerPark,500,TRUE,FALSE)
			
			IF NOT IS_PED_INJURED(budWade.ped)
				TASK_LOOK_AT_ENTITY(budWade.ped,cmbtChase[2].ped,4000)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedJohnny)
				IF NOT IS_PED_INJURED(pedJohnny)
					CLEAR_PED_TASKS_IMMEDIATELY(pedJohnny)
				ENDIF
				DELETE_PED(pedJohnny)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOHNNYKLEBITZ)
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOHNNYKLEBITZ)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedAshley)
				IF NOT IS_PED_INJURED(pedAshley)
					CLEAR_PED_TASKS_IMMEDIATELY(pedAshley)
				ENDIF
				DELETE_PED(pedAshley)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_ASHLEY)
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_ASHLEY)
			ENDIF
			
			bDirectionsDialogue[0] = FALSE
			bDirectionsDialogue[1] = FALSE
			bDirectionsDialogue[2] = FALSE
			bDirectionsDialogue[3] = FALSE
			bDirectionsDialogue[4] = FALSE
			//bRiderDialogue[0] = FALSE
			//bRiderDialogue[1] = FALSE
			bWatchTruckDialogue[0] = FALSE
			bWatchTruckDialogue[1] = FALSE
			iChaseDialogue = 0
			
			iDriveByHelp = 0
			fUberPlaybackDensitySwitchOffRange = 200
			allow_veh_to_stop_on_any_veh_impact = TRUE
			SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			cmbtChase[2].iEnemyProgress = 0 
			bSetUpStageData = TRUE
			bClearUpTrailerPark = FALSE
			bKilledBikersDuringChase = FALSE
			T1M1_WADEGBU = FALSE
			
		ENDIF
			
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF NOT IS_PED_INJURED(budWade.ped)
			AND NOT IS_PED_INJURED(budRon.ped)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				AND IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
				AND IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
					//WOOO
					IF NOT HAS_LABEL_BEEN_TRIGGERED("VEHICLE_JUMP")
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							IF IS_ENTITY_IN_ANGLED_AREA(vehTrevor.veh, <<470.980804,4323.728516,58.776157>>, <<450.019409,4341.243164,83.475525>>, 20.000000)
								IF NOT IS_PED_INJURED(budWade.ped)
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										KILL_ANY_CONVERSATION()
									ELSE
										IF NOT IS_VEHICLE_ON_ALL_WHEELS(vehTrevor.veh)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 6.0, REPLAY_IMPORTANCE_NORMAL)
											
											PLAY_PED_AMBIENT_SPEECH(budWade.ped,"VEHICLE_JUMP",SPEECH_PARAMS_FORCE_FRONTEND)
											PRINTSTRING("WOOOOOOOOOOOOOOOOOOO")
											SET_LABEL_AS_TRIGGERED("VEHICLE_JUMP", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
						IF NOT DOES_BLIP_EXIST(vehTruck.blip)
							vehTruck.blip = CREATE_BLIP_FOR_VEHICLE(vehTruck.veh,TRUE)
						ELSE
							UPDATE_CHASE_BLIP(vehTruck.blip,vehTruck.veh,260.0)
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(cmbtChase[2].ped)
						IF NOT DOES_BLIP_EXIST(cmbtChase[2].blip)
							cmbtChase[2].blip = CREATE_BLIP_FOR_PED(cmbtChase[2].ped, TRUE)
						ELSE
							UPDATE_CHASE_BLIP(cmbtChase[2].blip,cmbtChase[2].ped,260.0)
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(cmbtChase[3].ped)
						IF NOT DOES_BLIP_EXIST(cmbtChase[3].blip)
							cmbtChase[3].blip = CREATE_BLIP_FOR_PED(cmbtChase[3].ped,TRUE)
						ELSE
							UPDATE_CHASE_BLIP(cmbtChase[3].blip,cmbtChase[3].ped,260.0)
						ENDIF
					ENDIF
					
					//CLEAN UP OUT OF THE TRUCK STUFF
					IF DOES_BLIP_EXIST(vehTrevor.blip)
						REMOVE_BLIP(vehTrevor.blip)
					ENDIF
					IF DOES_BLIP_EXIST(budWade.blip)
						REMOVE_BLIP(budWade.blip)
					ENDIF
					IF DOES_BLIP_EXIST(budRon.blip)
						REMOVE_BLIP(budRon.blip)
					ENDIF

					IF T1M1_WADEGBU
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						T1M1_WADEGBU = FALSE
					ENDIF
					
					IF HAS_LABEL_BEEN_TRIGGERED("KILL ON GET OUT")
						SET_LABEL_AS_TRIGGERED("KILL ON GET OUT",FALSE)
					ENDIF
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_GETIN2")
					OR IS_THIS_PRINT_BEING_DISPLAYED("TRV_BDY4")
					OR IS_THIS_PRINT_BEING_DISPLAYED("TRV_BDY2")
						CLEAR_PRINTS()
					ENDIF
					
					MANAGE_DIALOGUE_FOR_CHASE()
				ELSE
					PRINTSTRING("outside truck logic -")PRINTNL()
					//Logic if anyone is out of the truck
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("KILL ON GET OUT")
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ELSE
						SET_LABEL_AS_TRIGGERED("KILL ON GET OUT",TRUE)
					ENDIF
					//Remove enemy blips
					IF DOES_BLIP_EXIST(vehTruck.blip)
						REMOVE_BLIP(vehTruck.blip)
					ENDIF
					
					IF DOES_BLIP_EXIST(cmbtChase[2].blip)
						REMOVE_BLIP(cmbtChase[2].blip)
					ENDIF
					IF DOES_BLIP_EXIST(cmbtChase[3].blip)
						REMOVE_BLIP(cmbtChase[3].blip)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
							IF NOT DOES_BLIP_EXIST(vehTrevor.blip)
								vehTrevor.blip = CREATE_BLIP_FOR_ENTITY(vehTrevor.veh)
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_GET_BACK_IN")
									PRINT_GOD_TEXT("TRV_GETIN2")
									SET_LABEL_AS_TRIGGERED("T1M1_GET_BACK_IN",TRUE)
								ENDIF
							ENDIF
						ELSE
							IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_GETIN2")
								CLEAR_PRINTS()
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF NOT IS_PED_INJURED(budWade.ped)
							IF NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh,TRUE)
								IF NOT T1M1_WADEGBU
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),budWade.ped,20000,SLF_WHILE_NOT_IN_FOV)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"T1M1_GTAA","TREVOR",SPEECH_PARAMS_FORCE_FRONTEND)
										T1M1_WADEGBU = TRUE
										PRINTSTRING("WADE GET BACK IN URGENT")
									ENDIF
								ENDIF
								IF NOT DOES_BLIP_EXIST(budWade.blip)
									budWade.blip = CREATE_BLIP_FOR_PED(budWade.ped,FALSE,CHAR_WADE)
									TASK_ENTER_VEHICLE(budWade.ped,vehTrevor.veh,DEFAULT_TIME_NEVER_WARP,VS_BACK_LEFT)
								ELSE
									IF GET_SCRIPT_TASK_STATUS(budWade.ped,SCRIPT_TASK_ENTER_VEHICLE)<> PERFORMING_TASK
										TASK_ENTER_VEHICLE(budWade.ped,vehTrevor.veh,DEFAULT_TIME_NEVER_WARP,VS_BACK_LEFT)
									ENDIF
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV_BDY4")
										REPLAY_RECORD_BACK_FOR_TIME(6.0, 3.0)
										PRINT_GOD_TEXT("TRV_BDY4")
										SET_LABEL_AS_TRIGGERED("TRV_BDY4",TRUE)
									ENDIF
								ENDIF
							ELSE
								IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_BDY4")
									CLEAR_PRINTS()
								ENDIF
								IF DOES_BLIP_EXIST(budWade.blip)
									REMOVE_BLIP(budWade.blip)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF NOT IS_PED_INJURED(budRon.ped)
							IF NOT IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh,TRUE)
								IF NOT DOES_BLIP_EXIST(budRon.blip)
									budRon.blip = CREATE_BLIP_FOR_PED(budRon.ped,FALSE,CHAR_RON)
									TASK_ENTER_VEHICLE(budRon.ped,vehTrevor.veh,DEFAULT_TIME_NEVER_WARP,VS_FRONT_RIGHT)
								ELSE
									IF GET_SCRIPT_TASK_STATUS(budRon.ped,SCRIPT_TASK_ENTER_VEHICLE)<> PERFORMING_TASK
										TASK_ENTER_VEHICLE(budRon.ped,vehTrevor.veh,DEFAULT_TIME_NEVER_WARP,VS_FRONT_RIGHT)
									ENDIF
									IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV_BDY2")
										PRINT_GOD_TEXT("TRV_BDY2")
										SET_LABEL_AS_TRIGGERED("TRV_BDY2",TRUE)
									ENDIF
								ENDIF
							ELSE
								IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_BDY2")
									CLEAR_PRINTS()
								ENDIF
								IF DOES_BLIP_EXIST(budRon.blip)
									REMOVE_BLIP(budRon.blip)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ELSE
			
		ENDIF

		IF NOT IS_ENTITY_DEAD(vehTruck.veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
				fPlaybackTimeT1 = GET_TIME_POSITION_IN_RECORDING(vehTruck.veh)
				PRINTSTRING("fPlaybackTimeT1:")PRINTFLOAT(fPlaybackTimeT1)PRINTNL()
				
				//RUBBER_BAND_TEST(fUberSpeed,vehTrevor.veh,vehTruck.veh)
				CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehTruck.veh, PLAYER_PED_ID(), fUberSpeed, 63.0, 34.0,80.0, 0.6,1.4)
				UPDATE_UBER_PLAYBACK(vehTruck.veh, fUberSpeed)
				
				IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
						SET_PLAYBACK_SPEED(vehBike[0].veh, fUberSpeed)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
						SET_PLAYBACK_SPEED(vehBike[1].veh, fUberSpeed)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<72.506767,3597.105713,58.682758>>, <<75.939247,3772.750000,25.818333>>, 170.000000)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<67.07, 3703.85, 39.56>>) > 180
						IF NOT DOES_BLIP_EXIST(blipDest)
							KILL_FACE_TO_FACE_CONVERSATION()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								CLEAR_PRINTS()
								IF DOES_BLIP_EXIST(vehTruck.blip)
									REMOVE_BLIP(vehTruck.blip)
								ENDIF
								PRINT_GOD_TEXT("TRV_GETTP")
								blipDest = CREATE_BLIP_FOR_COORD(<<67.07, 3703.85, 39.56>>)
							ENDIF
						ENDIF
						IF bBeenToTrailerPark = FALSE
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<66.109856,3599.583008,27.971104>>, <<77.204147,3772.014160,46.437660>>, 163.250000)
								bBeenToTrailerPark = TRUE
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<67.07, 3703.85, 39.56>>) > 220
								IF IS_PED_INJURED(cmbtChase[2].ped)
								AND NOT IS_PED_INJURED(cmbtChase[3].ped)
									IF NOT IS_ENTITY_ON_SCREEN(vehTruck.veh)
									AND NOT IS_ENTITY_ON_SCREEN(cmbtChase[3].ped)
										MissionFailed(FAIL_LEFT_TRAILER_PARK)
									ENDIF
								ENDIF
								IF IS_PED_INJURED(cmbtChase[3].ped)
								AND NOT IS_PED_INJURED(cmbtChase[2].ped)
									IF NOT IS_ENTITY_ON_SCREEN(vehTruck.veh)
									AND NOT IS_ENTITY_ON_SCREEN(cmbtChase[2].ped)
										MissionFailed(FAIL_LEFT_TRAILER_PARK)
									ENDIF
								ENDIF
								IF IS_PED_INJURED(cmbtChase[3].ped)
								AND IS_PED_INJURED(cmbtChase[2].ped)
									IF NOT IS_ENTITY_ON_SCREEN(vehTruck.veh)
										MissionFailed(FAIL_LEFT_TRAILER_PARK)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipDest)
						REMOVE_BLIP(blipDest)
					ENDIF
					IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_RETTP")
						CLEAR_PRINTS()
					ENDIF
				ENDIF
				CLEANUP_UBER_PLAYBACK()
			ENDIF
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTrevor.veh)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<109.909546,3633.051025,38.254272>>, <<46.707645,3628.708984,41.254379>>, 35.500000)//made locate bigger - bug 145596 //25 previously
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<48.109467,3642.522217,38.334419>>, <<-0.472603,3714.274170,45.373894>>, 35.500000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<105.521057,3622.349854,38.254272>>, <<133.018921,3721.928467,40.977505>>, 35.500000)
					SET_MODEL_AS_NO_LONGER_NEEDED(SCRAP)
					SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
					SET_MODEL_AS_NO_LONGER_NEEDED(TRFLAT)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOHNNYKLEBITZ)

					REQUEST_VEHICLE_ASSET(HEXER)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_PH8")
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PH8", CONV_PRIORITY_MEDIUM) // Pulling into park
							SET_LABEL_AS_TRIGGERED("T1M1_PH8",TRUE)
						ENDIF
					ENDIF
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehTrevor.veh,10)
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_AllowPlayerToInterruptVehicleEntryExit,TRUE)
							SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),TRUE,-1)
							IF IS_PHONE_ONSCREEN()
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							ENDIF
							
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN)
								IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN) > 0
									REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN,TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_BLIP_EXIST(blipDest)
							REMOVE_BLIP(blipDest)
						ENDIF
						
						IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_RETTP")
							CLEAR_PRINTS()
						ENDIF
						bGivePedWeaponOnExit = FALSE
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
						
						SET_LABEL_AS_TRIGGERED("DID CHASE",TRUE)
					
						e_section_stage = SECTION_STAGE_CLEANUP	
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<109.909546,3633.051025,38.254272>>, <<46.707645,3628.708984,41.254379>>, 35.500000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<48.109467,3642.522217,38.334419>>, <<-0.472603,3714.274170,45.373894>>, 35.500000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<105.521057,3622.349854,38.254272>>, <<133.018921,3721.928467,40.977505>>, 35.500000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<72.506767,3597.105713,58.682758>>, <<75.939247,3772.750000,25.818333>>, 170.000000)
					SET_MODEL_AS_NO_LONGER_NEEDED(SCRAP)
					SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
					SET_MODEL_AS_NO_LONGER_NEEDED(TRFLAT)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOHNNYKLEBITZ)
					IF DOES_BLIP_EXIST(blipDest)
						REMOVE_BLIP(blipDest)
					ENDIF
					IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_RETTP")
						CLEAR_PRINTS()
					ENDIF
					bGivePedWeaponOnExit = TRUE
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
					
					SET_LABEL_AS_TRIGGERED("DID CHASE",TRUE)
					
					e_section_stage = SECTION_STAGE_CLEANUP	
				ENDIF
			ENDIF
		ENDIF
		
		MANAGE_PRELOAD_MODELS_FOR_SHOOTOUT(vehTruck.veh)
		
		//--Rider drive by 
		
		IF cmbtChase[2].iEnemyProgress = 0 
			IF NOT IS_PED_INJURED(cmbtChase[2].ped)
				IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
					IF IS_PED_SITTING_IN_VEHICLE(cmbtChase[2].ped, vehBike[0].veh)
						SET_CURRENT_PED_VEHICLE_WEAPON(cmbtChase[2].ped,WEAPONTYPE_UNARMED)
						TASK_DRIVE_BY(cmbtChase[2].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,30,100,TRUE)
						cmbtChase[2].iTimeSpent = GET_GAME_TIMER()
						cmbtChase[2].iEnemyProgress = 1
					ENDIF	
				ENDIF
			ENDIF
		ELSE
			IF cmbtChase[2].iEnemyProgress = 1
				IF MANAGE_MY_TIMER(cmbtChase[2].iTimeSpent,3000)
					IF NOT IS_PED_INJURED(cmbtChase[2].ped)
						CLEAR_PED_TASKS(cmbtChase[2].ped)
						cmbtChase[2].iEnemyProgress = 2
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//--Rider blips
		
		IF DOES_BLIP_EXIST(cmbtChase[2].blip)
			IF NOT IS_PED_INJURED(cmbtChase[2].ped)
				IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
					IF NOT IS_PED_SITTING_IN_VEHICLE(cmbtChase[2].ped, vehBike[0].veh)
						SET_ENTITY_HEALTH(cmbtChase[2].ped, 0)
						INFORM_MISSION_STATS_OF_INCREMENT(TRV1_KILLS)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
							APPLY_FORCE_TO_ENTITY(vehBike[0].veh, APPLY_TYPE_FORCE, <<8.0, 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
						ENDIF
					ENDIF
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(cmbtChase[2].ped, PLAYER_PED_ID())
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(cmbtChase[2].ped)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
						APPLY_FORCE_TO_ENTITY(vehBike[0].veh, APPLY_TYPE_FORCE, <<8.0, 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
					IF DOES_BLIP_EXIST(cmbtChase[2].blip)
						REMOVE_BLIP(cmbtChase[2].blip)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(cmbtChase[2].ped)
				IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
					IF NOT IS_PED_SITTING_IN_VEHICLE(cmbtChase[2].ped, vehBike[0].veh)
						SET_ENTITY_HEALTH(cmbtChase[2].ped, 0)
						INFORM_MISSION_STATS_OF_INCREMENT(TRV1_KILLS)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							APPLY_FORCE_TO_ENTITY(vehBike[0].veh, APPLY_TYPE_FORCE, <<8.0, 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
						ENDIF
					ENDIF
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(cmbtChase[2].ped, PLAYER_PED_ID())
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(cmbtChase[2].ped)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						APPLY_FORCE_TO_ENTITY(vehBike[0].veh, APPLY_TYPE_FORCE, <<8.0, 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(cmbtChase[2].blip)
					REMOVE_BLIP(cmbtChase[2].blip)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(cmbtChase[3].blip)
			IF NOT IS_PED_INJURED(cmbtChase[3].ped)
				IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
					IF NOT IS_PED_SITTING_IN_VEHICLE(cmbtChase[3].ped, vehBike[1].veh)
						SET_ENTITY_HEALTH(cmbtChase[3].ped, 0)
						INFORM_MISSION_STATS_OF_INCREMENT(TRV1_KILLS)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						APPLY_FORCE_TO_ENTITY(vehBike[1].veh, APPLY_TYPE_FORCE, <<8.0, 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh)
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(cmbtChase[3].blip)
					REMOVE_BLIP(cmbtChase[3].blip)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(cmbtChase[3].ped)
				IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
					IF NOT IS_PED_SITTING_IN_VEHICLE(cmbtChase[3].ped, vehBike[1].veh)
						SET_ENTITY_HEALTH(cmbtChase[3].ped, 0)
						INFORM_MISSION_STATS_OF_INCREMENT(TRV1_KILLS)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							APPLY_FORCE_TO_ENTITY(vehBike[1].veh, APPLY_TYPE_FORCE, <<8.0, 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh)
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(cmbtChase[3].blip)
					REMOVE_BLIP(cmbtChase[3].blip)
				ENDIF

			ENDIF
		ENDIF
		
		
		IF IS_PED_INJURED(cmbtChase[2].ped) AND IS_PED_INJURED(cmbtChase[3].ped)
			SET_PED_AS_NO_LONGER_NEEDED(cmbtChase[3].ped)
			SET_PED_AS_NO_LONGER_NEEDED(cmbtChase[2].ped)
			SET_MODEL_AS_NO_LONGER_NEEDED(cmbtChase[3].model)
			SET_MODEL_AS_NO_LONGER_NEEDED(cmbtChase[2].model)
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TRV1_BIKERS_KILLED_BEFORE_LOCATION)
		ENDIF

	ENDIF
	
	IF e_section_stage = SECTION_STAGE_CLEANUP
	
		REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
	
		ROPE_UNLOAD_TEXTURES()
		REQUEST_MODEL(G_M_Y_LOST_02)
		REQUEST_MODEL(G_M_Y_LOST_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(DLOADER)
		SET_MODEL_AS_NO_LONGER_NEEDED(BIFF)
		IF DOES_BLIP_EXIST(vehTrevor.blip)
			REMOVE_BLIP(vehTrevor.blip)
		ENDIF
		IF DOES_BLIP_EXIST(budWade.blip)
			REMOVE_BLIP(budWade.blip)
		ENDIF
		IF DOES_BLIP_EXIST(budRon.blip)
			REMOVE_BLIP(budRon.blip)
		ENDIF
		iBuddyProgress = 0
		iWadeProgress = 0
		bSetUpStageData = FALSE
		
		SET_LABEL_AS_TRIGGERED("DID CHASE",TRUE)
		
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehTruck.veh)
		IF DOES_ENTITY_EXIST(vehBike[1].veh)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehBike[1].veh)
		ENDIF
		IF DOES_ENTITY_EXIST(vehBike[0].veh)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehBike[0].veh)
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_FOCUS_CAM_SCENE")
			STOP_AUDIO_SCENE("TREVOR_1_FOCUS_CAM_SCENE")
		ENDIF
		
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE:		Performs the section where Trevor tortures the victim for the first time
FUNC BOOL stageCaravanParkFight()
	
	RETURN FALSE
ENDFUNC

INT iVanReverseRhsProg
//PURPOSE: Short setpiece of a van reversing out and blocking your path
PROC DO_VAN_REVERSE_RHS_SETPIECE()
	
	SWITCH iVanReverseRhsProg
		CASE 0
			IF NOT bDoneCentreFight
				IF SHOULD_RHS_VAN_SETPIECE_TRIGGER()
				OR (gangEnemy[23].bTriggered AND IS_PED_INJURED(gangEnemy[23].ped))
					iVanReverseRhsProg++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF IS_POINT_VISIBLE_TO_PLAYER( << 108.7403, 3718.4480, 39.7577 >>, 1.0, 200.0)
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[3].veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[3].veh)
						UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[3].veh)
//						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_FI1",CONV_PRIORITY_MEDIUM)
//						ENDIF		
						iVanReverseRhsProg++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT gangEnemy[20].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[3].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[3].veh)
						gangEnemy[20].vGotoCoords =  << 87.8127, 3689.4924, 38.7288 >>
						gangEnemy[20].gang_da = DA_AT_GO_TO
						gangEnemy[20].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[20].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[20].bTriggered = TRUE
						iVanReverseRhsProg++
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iDoFirstBikeConvoySetPieceProg
//PURPOSE: Setpiece of 3 bikers appraoching you and attacking if you proceed clockwise around the trailer park
PROC DO_FIRST_LHS_BIKE_CONVOY_SETPIECE()
	GANG_MOVE_ATTACK_T gangTemp[4]
	SEQUENCE_INDEX seq
	SWITCH iDoFirstBikeConvoySetPieceProg
		CASE 0
			gangTemp[0] = gangEnemy[17]
			gangTemp[1] = gangEnemy[18]
			gangTemp[2] = gangEnemy[11]
			
			IF NOT gangEnemy[13].bTriggered
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-4.104918,3693.259277,37.892815>>, <<29.597843,3712.840088,41.891338>>, 34.250000)
				OR HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 3 ,3)
				OR (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<23.309280,3706.247803,38.455639>>, <<34.667908,3684.702148,43.454075>>, 25.500000)
				AND HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 2 ,2))
					SETUP_THESE_PEDS(PED_FIGHT_BIKE_CONV_SETPIECE1)
					
					VEHICLE_INDEX veh
					
					veh = GET_CLOSEST_VEHICLE(<< 52.4589, 3664.1252, 38.6962 >>,5,GBURRITO,VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
					IF DOES_ENTITY_EXIST(veh)
//						DELETE_VEHICLE(veh) //Fix for B* 2057108
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(vehTrailerPark[15].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[15].veh)
							SET_VEHICLE_ENGINE_ON(vehTrailerPark[15].veh,TRUE,TRUE)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[15].veh)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(vehTrailerPark[16].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[16].veh)
							SET_VEHICLE_ENGINE_ON(vehTrailerPark[16].veh,TRUE,TRUE)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[16].veh)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(vehTrailerPark[17].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[17].veh)
							SET_VEHICLE_ENGINE_ON(vehTrailerPark[17].veh,TRUE,TRUE)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[17].veh)
						ENDIF
					ENDIF
					
					START_AUDIO_SCENE("TREVOR_1_BIKES")
					IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[17].veh)
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[17].veh,"TREVOR_1_BIKES_GROUP")
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[16].veh)
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[16].veh,"TREVOR_1_BIKES_GROUP")
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[15].veh)
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[15].veh,"TREVOR_1_BIKES_GROUP")
					ENDIF
					
					bTriggerBikerSetPiece[0] = TRUE
					iDoFirstBikeConvoySetPieceProg++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			//-- Outtermost biker
			IF NOT gangEnemy[14].bTriggered
				IF NOT IS_PED_INJURED(gangEnemy[14].ped)
					IF NOT gangEnemy[14].bHasSequence
						IF NOT IS_ENTITY_DEAD(vehTrailerPark[17].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[17].veh)
								OPEN_SEQUENCE_TASK(seq)
									TASK_LEAVE_VEHICLE(NULL, vehTrailerPark[17].veh, ECF_DONT_CLOSE_DOOR)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(gangEnemy[14].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								gangEnemy[14].bHasSequence = TRUE
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_IN_ANY_VEHICLE(gangEnemy[14].ped)
							gangEnemy[14].bHasSequence = FALSE
							gangEnemy[14].gangCombatMovement = CM_DEFENSIVE
							gangEnemy[14].moveAttackStyle = MOVE_AIM_WHILE_GOTO
							gangEnemy[14].gang_da = DA_AT_GO_TO_LARGE
							gangEnemy[14].vGotoCoords =  << 27.0110, 3701.7512, 38.6947 >> //<< 30.4541, 3717.3591, 38.7019 >>
							gangEnemy[14].bTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//-- Middle biker
			IF NOT gangEnemy[12].bTriggered
				IF NOT IS_PED_INJURED(gangEnemy[12].ped)
					IF NOT gangEnemy[12].bHasSequence
						IF NOT IS_ENTITY_DEAD(vehTrailerPark[15].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[15].veh)
								OPEN_SEQUENCE_TASK(seq)
									TASK_LEAVE_VEHICLE(NULL, vehTrailerPark[15].veh, ECF_DONT_CLOSE_DOOR)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(gangEnemy[12].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								gangEnemy[12].bHasSequence = TRUE
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_IN_ANY_VEHICLE(gangEnemy[12].ped)
							gangEnemy[12].bHasSequence = FALSE
							gangEnemy[12].gangCombatMovement = CM_DEFENSIVE
							gangEnemy[12].moveAttackStyle = MOVE_AIM_WHILE_GOTO
							gangEnemy[12].gang_da = DA_AT_GO_TO_LARGE
							gangEnemy[12].vGotoCoords =  << 27.0110, 3701.7512, 38.6947 >> //<< 30.4541, 3717.3591, 38.7019 >>
							gangEnemy[12].bTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//--Innermost biker
			IF NOT gangEnemy[13].bTriggered
				IF NOT IS_PED_INJURED(gangEnemy[13].ped)
					IF NOT gangEnemy[13].bHasSequence
						IF NOT IS_ENTITY_DEAD(vehTrailerPark[16].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[16].veh)
								OPEN_SEQUENCE_TASK(seq)
									TASK_PAUSE(NULL, 300)
									TASK_LEAVE_VEHICLE(NULL, vehTrailerPark[16].veh, ECF_DONT_CLOSE_DOOR)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(gangEnemy[13].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								gangEnemy[13].bHasSequence = TRUE
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_IN_ANY_VEHICLE(gangEnemy[13].ped)
							gangEnemy[13].bHasSequence = FALSE
							gangEnemy[13].gangCombatMovement = CM_DEFENSIVE
							gangEnemy[13].moveAttackStyle = MOVE_AIM_WHILE_GOTO
							gangEnemy[13].gang_da = DA_AT_GO_TO
							gangEnemy[13].vGotoCoords = << 34.1710, 3704.2898, 38.5311 >>
							gangEnemy[13].bTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
		BREAK
	ENDSWITCH
ENDPROC

INT iTimeSinceRHSConveyTriggered 
INT iFirstRhsBikeSetpieceProg
//PURPOSE: Setpiece of 3 bikers appraoching you and attacking if you proceed ant-clockwise around the trailer park
PROC DO_FIRST_RHS_BIKE_CONVOY_SETPIECE()
	/*
		[Gang][Bike] [34][22]			[33][21]
							  [32][20]
	*/
	SWITCH iFirstRhsBikeSetpieceProg
		CASE 0
			IF NOT gangEnemy[32].bTriggered
				IF gangEnemy[9].bTriggered
					IF NOT bHasPlayerEnteredShootZOne[ZONE_CENTRE]
						IF IS_PLAYER_IN_SHOOTOUT_ZONE(ZONE_INTRO)
							IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<81.039383,3683.039795,37.952450>>, <<100.188187,3642.277832,43.717731>>, 39.500000)
								IF NOT IS_SPHERE_VISIBLE(vehTrailerPark[20].startLoc, 2.5)
									SETUP_THESE_PEDS(PED_FIGHT_RHS_BIKE_CONV_SETPIECE1)
									IF DOES_ENTITY_EXIST(vehTrailerPark[20].veh)
										//UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[20].veh)
									ENDIF
									IF DOES_ENTITY_EXIST(vehTrailerPark[21].veh)
										SET_VEHICLE_ENGINE_ON(vehTrailerPark[21].veh,TRUE,FALSE)
										UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[21].veh)
										SET_VEHICLE_CAN_LEAK_PETROL(vehTrailerPark[21].veh, TRUE)
										SET_VEHICLE_PETROL_TANK_HEALTH(vehTrailerPark[21].veh, 50)
									ENDIF
									IF DOES_ENTITY_EXIST(vehTrailerPark[22].veh)
										SET_VEHICLE_ENGINE_ON(vehTrailerPark[22].veh,TRUE,FALSE)
										UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[22].veh)
									ENDIF
									
									START_AUDIO_SCENE("TREVOR_1_BIKES")
									IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[21].veh)
										ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[21].veh,"TREVOR_1_BIKES_GROUP")
									ENDIF
									IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[22].veh)
										ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[22].veh,"TREVOR_1_BIKES_GROUP")
									ENDIF
									bTriggerBikerSetPiece[1] = TRUE
									iTimeSinceRHSConveyTriggered = GET_GAME_TIMER()
									iFirstRhsBikeSetpieceProg++
								ENDIF
							ENDIF
						ENDIF
					ELSE
						iFirstRhsBikeSetpieceProg = 100
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT gangEnemy[32].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[20].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[20].veh)
						gangEnemy[32].vGotoCoords =  << 80.4992, 3674.0144, 38.7078 >>
						gangEnemy[32].gang_da = DA_AT_GO_TO_LARGE
						gangEnemy[32].gangCombatMovement = CM_WILLADVANCE
						gangEnemy[32].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[32].bTriggered = TRUE
					ENDIF
				ENDIF
			ELSE
				IF gangEnemy[32].bReturnToAI = FALSE
					IF MANAGE_MY_TIMER(iTimeSinceRHSConveyTriggered, GET_RANDOM_INT_IN_RANGE(15000,20000))
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[32].ped,FALSE)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[32].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[32].ped,CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_RANGE(gangEnemy[32].ped,CR_NEAR)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[32].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
						gangEnemy[32].bReturnToAI = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT gangEnemy[33].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[21].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[21].veh)
						gangEnemy[33].vGotoCoords =  << 91.1677, 3666.9314, 38.7030 >>
						gangEnemy[33].gang_da = DA_AT_GO_TO
						gangEnemy[33].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[33].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[33].bTriggered = TRUE
					ENDIF
				ENDIF
			ELSE
				IF gangEnemy[33].bReturnToAI = FALSE
					IF MANAGE_MY_TIMER(iTimeSinceRHSConveyTriggered, GET_RANDOM_INT_IN_RANGE(15000,20000))
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[33].ped,FALSE)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[33].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[33].ped,CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_RANGE(gangEnemy[33].ped,CR_NEAR)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[33].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
						gangEnemy[33].bReturnToAI = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[34].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[22].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[22].veh)
						gangEnemy[34].vGotoCoords =  << 78.0473, 3686.2131, 38.5502 >>
						gangEnemy[34].gang_da = DA_AT_GO_TO
						gangEnemy[34].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[34].moveAttackStyle =  MOVE_AIM_WHILE_GOTO
						gangEnemy[34].bTriggered = TRUE
					ENDIF
				ENDIF
			ELSE
				SET_PED_REINF_PED(gangEnemy[34], gangEnemy[32])
				IF gangEnemy[34].bReturnToAI = FALSE
					IF MANAGE_MY_TIMER(iTimeSinceRHSConveyTriggered, GET_RANDOM_INT_IN_RANGE(15000,20000))
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[34].ped,FALSE)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[34].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[34].ped,CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_RANGE(gangEnemy[34].ped,CR_NEAR)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[34].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
						gangEnemy[34].bReturnToAI = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC

INT iDoSecRhsBikeProg
//PURPOSE: Setpiece of 3 bikers appraoching you and attacking if you  after proceeding anti-clockwise around the trailer park
PROC DO_SECOND_RHS_BIKE_CONVOY_SETPIECE()
	GANG_MOVE_ATTACK_T gangTemp[3]
	SWITCH iDoSecRhsBikeProg
		CASE 0
			gangTemp[0] = gangEnemy[22]
			gangTemp[1] = gangEnemy[21]
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<81.837639,3707.141846,37.457558>>, <<117.798973,3685.550293,41.427235>>, 8.250000)
			OR HAVE_THESE_PEDS_BEEN_KILLED(gangTemp,2,2)
				IF NOT IS_POINT_VISIBLE_TO_PLAYER(vehTrailerPark[24].startLoc,0.1)
					SETUP_THESE_PEDS(PED_FIGHT_RHS_BIKE_CONV_SETPIECE2)
					bTriggerBikerSetPiece[2] = TRUE
					iDoSecRhsBikeProg++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT gangEnemy[24].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[24].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[24].veh)
						gangEnemy[24].vGotoCoords =  << 96.8904, 3713.2527, 38.5539 >>
						gangEnemy[24].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[24].gangCombatMovement = CM_WILLADVANCE
						gangEnemy[24].gang_da  = DA_AT_GOTO_V_LARGE
						gangEnemy[24].bTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[25].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[25].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[25].veh)
						gangEnemy[25].vGotoCoords =   << 104.9613, 3713.5818, 38.7133 >>
						gangEnemy[25].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[25].gangCombatMovement = CM_WILLADVANCE
						gangEnemy[25].gang_da  = DA_AT_GOTO_V_LARGE
						gangEnemy[25].bTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iTimeSinceRHSCentreTriggered
INT iBkesApproachCentreRhsProg
//PURPOSE: Setpiece of 3 bikers appraoching you and attacking if you approach the rear of the trailer park after proceeding anti-clockwise around the trailer park
PROC DO_BIKES_APPROACH_CENTRE_RHS_SETPIECE()
	SWITCH iBkesApproachCentreRhsProg
		CASE 0
			IF NOT bForceAWaveOfBikersToCome 
				IF NOT bDoneCentreFight
					IF HAS_PLAYER_APPROACHED_CENTRE_FROM_LHS()
						SETUP_THESE_PEDS(PED_FIGHT_APPROACH_CENTRE_RHS_BIKES)
						IF DOES_ENTITY_EXIST(vehTrailerPark[20].veh)
							SET_VEHICLE_ENGINE_ON(vehTrailerPark[20].veh,TRUE,FALSE)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[20].veh)
						ENDIF
						IF DOES_ENTITY_EXIST(vehTrailerPark[21].veh)
							SET_VEHICLE_ENGINE_ON(vehTrailerPark[21].veh,TRUE,FALSE)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[21].veh)
						ENDIF
						IF DOES_ENTITY_EXIST(vehTrailerPark[22].veh)
							SET_VEHICLE_ENGINE_ON(vehTrailerPark[22].veh,TRUE,FALSE)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[22].veh)
						ENDIF
						
						START_AUDIO_SCENE("TREVOR_1_BIKES")
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[20].veh)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[20].veh,"TREVOR_1_BIKES_GROUP")
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[21].veh)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[21].veh,"TREVOR_1_BIKES_GROUP")
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[22].veh)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[22].veh,"TREVOR_1_BIKES_GROUP")
						ENDIF
						iTimeSinceRHSCentreTriggered = GET_GAME_TIMER()
						bTriggerBikerSetPiece[3] = TRUE
						iBkesApproachCentreRhsProg++
					ENDIF
				ENDIF
			ELSE
				SETUP_THESE_PEDS(PED_FIGHT_APPROACH_CENTRE_RHS_BIKES)
				IF DOES_ENTITY_EXIST(vehTrailerPark[20].veh)
					SET_VEHICLE_ENGINE_ON(vehTrailerPark[20].veh,TRUE,FALSE)
					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[20].veh)
				ENDIF
				IF DOES_ENTITY_EXIST(vehTrailerPark[21].veh)
					SET_VEHICLE_ENGINE_ON(vehTrailerPark[21].veh,TRUE,FALSE)
					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[21].veh)
				ENDIF
				IF DOES_ENTITY_EXIST(vehTrailerPark[22].veh)
					SET_VEHICLE_ENGINE_ON(vehTrailerPark[22].veh,TRUE,FALSE)
					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[22].veh)
				ENDIF
				
				START_AUDIO_SCENE("TREVOR_1_BIKES")
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[20].veh)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[20].veh,"TREVOR_1_BIKES_GROUP")
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[21].veh)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[21].veh,"TREVOR_1_BIKES_GROUP")
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[22].veh)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[22].veh,"TREVOR_1_BIKES_GROUP")
				ENDIF
				iTimeSinceRHSCentreTriggered = GET_GAME_TIMER()
				iBkesApproachCentreRhsProg++
			ENDIF
		BREAK
		
		CASE 1
			IF MANAGE_MY_TIMER(iTimeSinceRHSCentreTriggered,6000)
				bTriggerBikerSetPiece[3] = TRUE
			ENDIF
			IF NOT gangEnemy[32].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[20].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[20].veh)
						gangEnemy[32].vGotoCoords = << 42.1645, 3721.8428, 38.5633 >>
						gangEnemy[32].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[32].gang_da = DA_AT_GO_TO_LARGE
						gangEnemy[32].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[32].bTriggered = TRUE
					ENDIF
				ENDIF
			ELSE
				IF gangEnemy[32].bReturnToAI = FALSE
					IF NOT IS_PED_INJURED(gangEnemy[32].ped)
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[20].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[20].veh)
								IF MANAGE_MY_TIMER(iTimeSinceRHSCentreTriggered, GET_RANDOM_INT_IN_RANGE(15000,20000))
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[32].ped,FALSE)
									SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[32].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20,TRUE)
									SET_PED_COMBAT_ATTRIBUTES(gangEnemy[32].ped,CA_CAN_CHARGE,TRUE)
									SET_PED_COMBAT_RANGE(gangEnemy[32].ped,CR_NEAR)
									SET_PED_COMBAT_ATTRIBUTES(gangEnemy[32].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
									gangEnemy[32].bReturnToAI = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[33].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[21].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[21].veh)
						gangEnemy[33].vGotoCoords = << 49.8538, 3723.2258, 38.6739 >>
						gangEnemy[33].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[33].gang_da = DA_AT_GO_TO_LARGE
						gangEnemy[33].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[33].bTriggered = TRUE
					ENDIF
				ENDIF
			ELSE
				IF gangEnemy[33].bReturnToAI = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[21].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[21].veh)
							IF NOT IS_PED_INJURED(gangEnemy[33].ped)
								IF MANAGE_MY_TIMER(iTimeSinceRHSCentreTriggered, GET_RANDOM_INT_IN_RANGE(15000,20000))
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[33].ped,FALSE)
									SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[33].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20,TRUE)
									SET_PED_COMBAT_ATTRIBUTES(gangEnemy[33].ped,CA_CAN_CHARGE,TRUE)
									SET_PED_COMBAT_RANGE(gangEnemy[33].ped,CR_NEAR)
									SET_PED_COMBAT_ATTRIBUTES(gangEnemy[33].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
									gangEnemy[33].bReturnToAI = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[34].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[22].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[22].veh)
						gangEnemy[34].vGotoCoords = << 42.9824, 3736.3972, 38.5268 >>
						gangEnemy[34].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[34].gang_da = DA_AT_GO_TO_LARGE
						gangEnemy[34].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[34].bTriggered = TRUE
					ENDIF
				ENDIF
			ELSE
				SET_PED_REINF_PED(gangEnemy[34], gangEnemy[32])
				IF gangEnemy[34].bReturnToAI = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[22].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[22].veh)
							IF NOT IS_PED_INJURED(gangEnemy[34].ped)
								IF MANAGE_MY_TIMER(iTimeSinceRHSCentreTriggered, GET_RANDOM_INT_IN_RANGE(15000,20000))
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[34].ped,FALSE)
									SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[34].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20,TRUE)
									SET_PED_COMBAT_ATTRIBUTES(gangEnemy[34].ped,CA_CAN_CHARGE,TRUE)
									SET_PED_COMBAT_RANGE(gangEnemy[34].ped,CR_NEAR)
									SET_PED_COMBAT_ATTRIBUTES(gangEnemy[34].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
									gangEnemy[34].bReturnToAI = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC


INT iBkesApproachCentreLhsProg
//PURPOSE: Setpiece of 3 bikers appraoching you and attacking if you approach the rear of the trailer park after proceeding clockwise around the trailer park
PROC DO_BIKES_APPROACH_CENTRE_LHS_SETPIECE()
	SWITCH iBkesApproachCentreLhsProg
		CASE 0
			IF NOT bDoneCentreFight
				IF NOT gangEnemy[13].bTriggered
					IF HAS_PLAYER_APPROACHED_CENTRE_FROM_RHS()
						SETUP_THESE_PEDS(PED_FIGHT_APPROACH_CENTRE_LHS_BIKES)
						START_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[15].veh, 16, "trevor1Dw")
						SET_VEHICLE_ENGINE_ON(vehTrailerPark[15].veh,TRUE,FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[16].veh, 17, "trevor1Dw")
						SET_VEHICLE_ENGINE_ON(vehTrailerPark[16].veh,TRUE,FALSE)
						START_AUDIO_SCENE("TREVOR_1_BIKES")
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[15].veh)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[15].veh,"TREVOR_1_BIKES_GROUP")
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[16].veh)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[16].veh,"TREVOR_1_BIKES_GROUP")
						ENDIF
						bTriggerBikerSetPiece[4] = TRUE
						iBkesApproachCentreLhsProg++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT gangEnemy[12].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[15].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[15].veh)
						gangEnemy[12].vGotoCoords = << 46.1710, 3723.0205, 38.6841 >>
						gangEnemy[12].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[12].gang_da = DA_AT_GO_TO
						gangEnemy[12].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[12].bTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[13].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[16].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[16].veh)
						gangEnemy[13].vGotoCoords = << 49.2689, 3736.7710, 38.5828 >>
						gangEnemy[13].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[13].gang_da = DA_AT_GO_TO
						gangEnemy[13].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[13].bTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF iBkesApproachCentreLhsProg > 0
		HANDLE_RIDER_ON_CAR_REC_BIKE(12, 15)
		HANDLE_RIDER_ON_CAR_REC_BIKE(13, 16)
	ENDIF
ENDPROC


INT iLureGuy
//PURPOSE: NEW GUY
PROC DO_GUY_TO_LURE_YOU_FORWARDS_SETPIECE()
	SEQUENCE_INDEX seq
	SWITCH iLureGuy
	
		CASE 0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<53.843185,3684.475098,38.254253>>, <<32.564510,3676.024170,40.032860>>, 8.750000)
				IF NOT DOES_ENTITY_EXIST(gangEnemy[10].ped)
					gangEnemy[10].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 17.7291, 3702.8911, 38.7281 >>, 224.5845, WEAPONTYPE_SAWNOFFSHOTGUN)
					SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[10].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20.0)
					SET_PED_COMBAT_MOVEMENT(gangEnemy[10].ped,CM_WILLADVANCE)
					iLureGuy++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(gangEnemy[10].ped)
				OPEN_SEQUENCE_TASK(seq)
					TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(gangEnemy[10].ped,seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[10].ped,FALSE)
				iLureGuy++
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

INT iRifleGuy
INT iRifleGuyTimer
//PURPOSE: NEW GUY
PROC DO_THE_RIFLE_GUY()
	SEQUENCE_INDEX seq
	SWITCH iRifleGuy
	
		CASE 0
			IF NOT DOES_ENTITY_EXIST(gangEnemy[42].ped)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<73.325653,3728.407959,37.968533>>, <<49.222183,3694.146973,43.255215>>, 38.750000)
				AND NOT IS_SPHERE_VISIBLE(<<84.6956, 3722.6765, 38.7396>>,1.0)
					IF NOT DOES_ENTITY_EXIST(gangEnemy[42].ped)
						gangEnemy[42].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<84.6956, 3722.6765, 38.7396>>, 127.6510, WEAPONTYPE_SAWNOFFSHOTGUN)
						iRifleGuy++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(gangEnemy[42].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(gangEnemy[42].ped, FALSE)) < 60
				OR IS_ENTITY_ON_SCREEN(gangEnemy[42].ped)
					SET_PED_ACCURACY(gangEnemy[42].ped,1)
					SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[42].ped,<<81.9903, 3721.3938, 38.7452>>,1.0)
					OPEN_SEQUENCE_TASK(seq)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gangEnemy[42].ped,seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[42].ped,FALSE)
					iRifleGuyTimer = GET_GAME_TIMER()
					iRifleGuy++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(gangEnemy[42].ped)
				IF MANAGE_MY_TIMER(iRifleGuyTimer,12000)
					IF IS_ENTITY_ON_SCREEN(gangEnemy[42].ped)
						SET_PED_ACCURACY(gangEnemy[42].ped,1)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[42].ped,<<57.0861, 3724.4236, 38.7258>>,20.0)
						SET_PED_COMBAT_MOVEMENT(gangEnemy[42].ped,CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[42].ped,CA_CAN_CHARGE,TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(gangEnemy[42].ped,seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[42].ped,FALSE)
						iRifleGuy++
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

INT iGrenadeBoss

PROC DO_GRENADE_GUY()

	SWITCH iGrenadeBoss
		
		CASE 0
			IF NOT IS_PED_INJURED(gangEnemy[31].ped)
				SET_ENTITY_HEALTH(gangEnemy[31].ped,200)
				SET_PED_ACCURACY(gangEnemy[31].ped,5)
				SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[31].ped,<<78.1990, 3706.4666, 40.0819>>,20.0)
			ENDIF
			iGrenadeBoss ++
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(gangEnemy[31].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(gangEnemy[31].ped,PLAYER_PED_ID()) < 100
					IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(gangEnemy[31].ped,PLAYER_PED_ID())
						IF IS_ENTITY_ON_SCREEN(gangEnemy[31].ped)
							PLAY_PED_AMBIENT_SPEECH(gangEnemy[31].ped,"GENERIC_WAR_CRY",SPEECH_PARAMS_INTERRUPT_SHOUTED)
							iGrenadeBoss ++
						ENDIF
					ELSE
						PLAY_PED_AMBIENT_SPEECH(gangEnemy[31].ped,"GENERIC_FUCK_YOU",SPEECH_PARAMS_INTERRUPT_SHOUTED)
						iGrenadeBoss ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF GET_DISTANCE_BETWEEN_ENTITIES(gangEnemy[31].ped,PLAYER_PED_ID()) < 45
				IF NOT IS_PED_INJURED(gangEnemy[31].ped)
					IF IS_ENTITY_ON_SCREEN(gangEnemy[31].ped)
						SET_PED_COMBAT_RANGE(gangEnemy[31].ped,CR_NEAR)
						PLAY_PED_AMBIENT_SPEECH(gangEnemy[31].ped,"GENERIC_WAR_CRY",SPEECH_PARAMS_INTERRUPT_SHOUTED)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[31].ped,CA_USE_ENEMY_ACCURACY_SCALING,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[31].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[31].ped,CA_AGGRESSIVE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[31].ped,CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_MOVEMENT(gangEnemy[31].ped,CM_WILLADVANCE)
						SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(gangEnemy[31].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 15.0,FALSE)
						TASK_COMBAT_PED(gangEnemy[31].ped,PLAYER_PED_ID())
						iGrenadeBoss ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	
	ENDSWITCH

ENDPROC

//PURPOSE: NEW GUY

PROC DO_THE_ROOF_GUY()
	
	//PRINTSTRING("iRoofGuy:")PRINTINT(iRoofGuy)PRINTNL()
	
	SEQUENCE_INDEX seq
	SWITCH iRoofGuy
	
		CASE 0
			IF NOT DOES_ENTITY_EXIST(gangEnemy[43].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 14.7984, 3725.8701, 42.4778 >>) < 60	
					IF WOULD_ENTITY_BE_OCCLUDED(G_M_Y_LOST_01,<< 11.88, 3728.81, 43.45 >>)
						gangEnemy[43].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 11.88, 3728.81, 43.45 >>, -152.39, WEAPONTYPE_ASSAULTRIFLE)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(gangEnemy[43].ped)
					SET_PED_COMBAT_ATTRIBUTES(gangEnemy[43].ped,CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED,FALSE)
					SET_PED_FLEE_ATTRIBUTES(gangEnemy[43].ped,FA_DISABLE_COWER,FALSE)
				ENDIF
				iRoofGuy ++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(gangEnemy[43].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 14.7984, 3725.8701, 42.4778 >>) < 100
				OR NOT WOULD_ENTITY_BE_OCCLUDED(G_M_Y_LOST_01,<< 12.3359, 3728.3882, 42.4859 >>)
					SET_PED_ACCURACY(gangEnemy[43].ped,0)
					SET_PED_CHANCE_OF_FIRING_BLANKS(gangEnemy[43].ped,20,80)
					SET_PED_SEEING_RANGE(gangEnemy[43].ped,200)
					SET_ENTITY_HEALTH(gangEnemy[43].ped,110)
					SET_PED_COMBAT_RANGE(gangEnemy[43].ped,CR_FAR)
					SET_PED_COMBAT_MOVEMENT(gangEnemy[43].ped,CM_DEFENSIVE)
					//SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[43].ped,<< 12.3359, 3728.3882, 42.4859 >>,0.75)
					SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[43].ped,<< 14.45, 3726.33, 43.45 >>,0.75,TRUE)
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gangEnemy[43].ped,seq)
					CLEAR_SEQUENCE_TASK(seq)
					iRoofGuy++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(gangEnemy[43].ped)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(gangEnemy[43].ped,PLAYER_PED_ID())
					SET_ENTITY_HEALTH(gangEnemy[43].ped,0)
					iRoofGuy++
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(gangEnemy[43].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 14.7984, 3725.8701, 42.4778 >>) < 40
					SET_PED_COMBAT_ATTRIBUTES(gangEnemy[43].ped,CA_CAN_SHOOT_WITHOUT_LOS,TRUE)
					SET_PED_ACCURACY(gangEnemy[43].ped,1)
					iRoofGuy++
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC


INT iCentreGuy
//PURPOSE: NEW GUY
PROC DO_GUY_IN_CENTRE_UPON_CUTTING_THROUGH()
	SEQUENCE_INDEX seq
	SWITCH iCentreGuy
	
		CASE 0
			IF NOT DOES_ENTITY_EXIST(gangEnemy[41].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<67.31, 3713.33, 39.75>>) < 80
				AND NOT IS_SPHERE_VISIBLE(<<67.31, 3713.33, 39.75>>,1.0)
					IF NOT DOES_ENTITY_EXIST(gangEnemy[41].ped)
						gangEnemy[41].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<67.31, 3713.33, 39.75>>, 25.56, WEAPONTYPE_SAWNOFFSHOTGUN)
						iCentreGuy++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(gangEnemy[41].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(gangEnemy[41].ped, FALSE)) < 60
				OR IS_ENTITY_ON_SCREEN(gangEnemy[41].ped)
					SET_PED_ACCURACY(gangEnemy[41].ped,1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[41].ped,FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[41].ped,<<67.31, 3713.33, 39.75>>,15.0)
					SET_PED_COMBAT_MOVEMENT(gangEnemy[41].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(gangEnemy[41].ped,CA_CAN_CHARGE,TRUE)
					OPEN_SEQUENCE_TASK(seq)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gangEnemy[41].ped,seq)
					CLEAR_SEQUENCE_TASK(seq)
					iCentreGuy++
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

INT iAgressiveCentreGuys

PROC DO_AGGRESSIVE_GUYS_IN_THE_CENTRE()
	SEQUENCE_INDEX seq
	
	SWITCH iAgressiveCentreGuys
		CASE 0
			IF NOT DOES_ENTITY_EXIST(gangEnemy[44].ped)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<75.330368,3727.139893,38.226501>>, <<53.211632,3696.097656,50.754875>>, 34.000000)
				AND NOT IS_SPHERE_VISIBLE(<<84.4422, 3723.2402, 38.7385>>,1.0)
					IF NOT DOES_ENTITY_EXIST(gangEnemy[44].ped)
						gangEnemy[44].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<84.4422, 3723.2402, 38.7385>>, 165.5727  , WEAPONTYPE_PISTOL)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[44].ped,FALSE)
						SET_PED_ACCURACY(gangEnemy[44].ped,10)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[44].ped,FALSE)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[44].ped,<<69.9965, 3717.5894, 38.7549>>,15.0,TRUE)
						SET_PED_COMBAT_MOVEMENT(gangEnemy[44].ped,CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[44].ped,CA_CAN_CHARGE,TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(gangEnemy[44].ped,seq)
						CLEAR_SEQUENCE_TASK(seq)
						iAgressiveCentreGuys++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
//			IF NOT DOES_ENTITY_EXIST(gangEnemy[45].ped)
//				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<75.330368,3727.139893,38.226501>>, <<53.211632,3696.097656,50.754875>>, 34.000000)
//				AND NOT IS_SPHERE_VISIBLE(<<59.6659, 3691.9597, 38.9211>>,1.0)
//					IF NOT DOES_ENTITY_EXIST(gangEnemy[45].ped)
//						gangEnemy[45].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, <<59.6659, 3691.9597, 38.9211>>, 241.6056  , WEAPONTYPE_PISTOL)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[45].ped,FALSE)
//						SET_PED_ACCURACY(gangEnemy[45].ped,10)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[45].ped,FALSE)
//						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[45].ped,<<63.4484, 3706.4148, 38.7549>>,15.0,TRUE)
//						SET_PED_COMBAT_MOVEMENT(gangEnemy[45].ped,CM_WILLADVANCE)
//						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[45].ped,CA_CAN_CHARGE,TRUE)
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(gangEnemy[45].ped,seq)
//						CLEAR_SEQUENCE_TASK(seq)
//						iAgressiveCentreGuys++
//					ENDIF
//				ENDIF
//			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

INT iCentreShotgunGuy
//PURPOSE: NEW GUY
PROC DO_GUY_IN_THE_CENTRE_WITH_SHOTGUN()
	SEQUENCE_INDEX seq
	SWITCH iCentreShotgunGuy
	
		CASE 0
			IF NOT DOES_ENTITY_EXIST(gangEnemy[23].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 63.8115, 3682.9834, 38.8342 >>) < 80
				AND NOT IS_SPHERE_VISIBLE(<< 63.8115, 3682.9834, 38.8342 >>,1.0)
					IF NOT DOES_ENTITY_EXIST(gangEnemy[23].ped)
						gangEnemy[23].ped = CREATE_GANG_PED(G_M_Y_LOST_01,grpEnemies, << 63.8115, 3682.9834, 38.8342 >>, 165.5727  , WEAPONTYPE_MICROSMG)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[23].ped,<<59.8701, 3697.1667, 38.7549>>,2.0,TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[23].ped,FALSE)
						iCentreShotgunGuy++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 1
			IF NOT IS_PED_INJURED(gangEnemy[23].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(gangEnemy[23].ped, FALSE)) < 35
				AND IS_ENTITY_ON_SCREEN(gangEnemy[23].ped)
				OR (IS_PED_INJURED(gangEnemy[27].ped) AND gangEnemy[27].bTriggered = TRUE)
					SET_PED_ACCURACY(gangEnemy[23].ped,1)
					SET_PED_COMBAT_MOVEMENT(gangEnemy[23].ped,CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(gangEnemy[23].ped,CA_CAN_CHARGE,TRUE)
					SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(gangEnemy[23].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 15.0,FALSE)
					OPEN_SEQUENCE_TASK(seq)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gangEnemy[23].ped,seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[23].ped,FALSE)
					iCentreShotgunGuy++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(gangEnemy[23].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(gangEnemy[23].ped, FALSE)) > 100
					SET_PED_ACCURACY(gangEnemy[23].ped,1)
					SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[23].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),10.0,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(gangEnemy[23].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					OPEN_SEQUENCE_TASK(seq)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gangEnemy[23].ped,seq)
					CLEAR_SEQUENCE_TASK(seq)
					iCentreShotgunGuy++
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC



//PURPOSE: A biker exits one of the trailer#s and then tries to make a run for it on his bike
PROC DO_PED_FLEE_ON_BIKE_SETPIECE(BOOL bPlayerUsingShotgun)
	SEQUENCE_INDEX seq
	GANG_MOVE_ATTACK_T gangTemp[2]
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[4].veh)
		IF NOT IS_PED_INJURED(gangEnemy[7].ped)
			SWITCH iDoPedFleeOnBikeSetPieceProg
				CASE 0
					gangTemp[0] = gangEnemy[6]
					gangTemp[1] = gangEnemy[40]
					IF (HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 1, 1) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), gangEnemy[7].ped) < 23.0)
					OR (HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 2, 2) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), gangEnemy[7].ped) < 35.0)
					OR (HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 2, 2) AND NOT WOULD_ENTITY_BE_OCCLUDED(G_M_Y_LOST_01,<<29.60, 3661.92, 40.63>>))
					OR IS_PED_INJURED(gangEnemy[6].ped)
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<29.58, 3661.88, 39.63>>) < 4
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<32.45, 3667.45, 39.56>>) < 1
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<54.949059,3672.497559,38.230812>>, <<39.546333,3657.829590,43.000359>>, 2.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<41.484673,3655.522949,37.778347>>, <<20.273994,3649.470459,43.498501>>, 2.500000)
						IF NOT IS_PED_INJURED(gangEnemy[7].ped)
							IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[4].veh)
								SET_VEHICLE_DOORS_LOCKED(vehTrailerPark[4].veh,VEHICLELOCK_UNLOCKED)
								OPEN_SEQUENCE_TASK(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrailerPark[4].veh, <<-1.0, 0.0, 0.0>>), PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 1.0, 1.0, TRUE,ENAV_DEFAULT,FALSE,DEFAULT,DEFAULT_TIME_NEVER_WARP)	
									TASK_ENTER_VEHICLE(NULL, vehTrailerPark[4].veh)
									TASK_DRIVE_BY(NULL,PLAYER_PED_ID(),NULL,<<0,0,0>>,0,20,TRUE)
								CLOSE_SEQUENCE_TASK(seq)
								SET_ENTITY_VISIBLE(gangEnemy[7].ped, TRUE)
								TASK_PERFORM_SEQUENCE(gangEnemy[7].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								PLAY_PED_AMBIENT_SPEECH(gangEnemy[7].ped,"FALL_BACK",SPEECH_PARAMS_INTERRUPT_SHOUTED)
								iDoPedFleeOnBikeSetPieceProg++
							ELSE
								OPEN_SEQUENCE_TASK(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 1.0, 1.0, TRUE,ENAV_DEFAULT,FALSE,DEFAULT,DEFAULT_TIME_NEVER_WARP)
									TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(seq)
								SET_ENTITY_VISIBLE(gangEnemy[7].ped, TRUE)
								TASK_PERFORM_SEQUENCE(gangEnemy[7].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								PLAY_PED_AMBIENT_SPEECH(gangEnemy[7].ped,"FALL_BACK",SPEECH_PARAMS_INTERRUPT_SHOUTED)
								iDoPedFleeOnBikeSetPieceProg++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					IF NOT IS_PED_INJURED(gangEnemy[7].ped)
						IF IS_PED_SITTING_IN_VEHICLE(gangEnemy[7].ped, vehTrailerPark[4].veh)
							IF IS_ENTITY_ON_SCREEN(gangEnemy[7].ped)
								UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[4].veh)
								PLAY_PED_AMBIENT_SPEECH(gangEnemy[7].ped,"FALL_BACK",SPEECH_PARAMS_INTERRUPT_SHOUTED)
								iDoPedFleeOnBikeSetPieceProg++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF NOT IS_PED_SITTING_IN_VEHICLE(gangEnemy[7].ped, vehTrailerPark[4].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[4].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[4].veh)
						ENDIF
						gangEnemy[7].vGotoCoords = GET_ENTITY_COORDS(gangEnemy[7].ped, FALSE)
						gangEnemy[7].gang_da = DA_AT_GO_TO_LARGE
						gangEnemy[7].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[7].bTriggered = TRUE
					ELSE 
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[4].veh)
							iDoPedFleeOnBikeSetPieceProg++
						ENDIF
					ENDIF
					IF bPlayerUsingShotgun
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTrailerPark[4].veh, PLAYER_PED_ID())
							IF GET_DISTANCE_BETWEEN_ENTITIES(vehTrailerPark[4].veh, PLAYER_PED_ID()) < 25.0
								STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[4].veh)
								EXPLODE_VEHICLE(vehTrailerPark[4].veh)
								SET_VEHICLE_CAN_LEAK_PETROL(vehTrailerPark[4].veh, TRUE)
								SET_VEHICLE_PETROL_TANK_HEALTH(vehTrailerPark[4].veh, 100)
								iDoPedFleeOnBikeSetPieceProg++
								//bFuelCanDropFromVeh[4] = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 3
					IF NOT IS_ENTITY_DEAD(vehTrailerPark[4].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[4].veh)
							SET_PED_COMBAT_MOVEMENT(gangEnemy[7].ped,CM_DEFENSIVE)
							SET_PED_ACCURACY(gangEnemy[7].ped,1)
							
							OPEN_SEQUENCE_TASK(seqSequence)
								TASK_LEAVE_VEHICLE(NULL,vehTrailerPark[4].veh,ECF_DONT_CLOSE_DOOR)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),PLAYER_PED_ID(),1.0,TRUE, 1.0, 1.0, TRUE,ENAV_DEFAULT,FALSE,DEFAULT,DEFAULT_TIME_NEVER_WARP)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(seqSequence)
							TASK_PERFORM_SEQUENCE(gangEnemy[7].ped,seqSequence)	
							CLEAR_SEQUENCE_TASK(seqSequence)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[7].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),3,TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[7].ped,FALSE)
							iDoPedFleeOnBikeSetPieceProg++
						ENDIF
					ELSE
						SET_PED_COMBAT_MOVEMENT(gangEnemy[7].ped,CM_DEFENSIVE)
						SET_PED_ACCURACY(gangEnemy[7].ped,1)
						
						OPEN_SEQUENCE_TASK(seqSequence)
							TASK_LEAVE_VEHICLE(NULL,vehTrailerPark[4].veh,ECF_DONT_CLOSE_DOOR)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),PLAYER_PED_ID(),1.0,TRUE, 1.0, 1.0, TRUE,ENAV_DEFAULT,FALSE,DEFAULT,DEFAULT_TIME_NEVER_WARP)
							TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seqSequence)
						TASK_PERFORM_SEQUENCE(gangEnemy[7].ped,seqSequence)	
						CLEAR_SEQUENCE_TASK(seqSequence)
						
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[7].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),3,TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[7].ped,FALSE)
						iDoPedFleeOnBikeSetPieceProg++
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[4].veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[4].veh)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[4].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[4].veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[4].veh)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		SHOW_PED_CAR_REC_TIME_DEBUG(gangEnemy[7].ped)
	#ENDIF
ENDPROC

INT iDoFleeOnBikeRhsProg
//PURPOSE: A biker exits one of the trailer#s and then tries to make a run for it on his bike
PROC DO_PED_FLEE_ON_BIKE_RHS_TRAILER_SETPIECE()
	SEQUENCE_INDEX seq
	SWITCH iDoFleeOnBikeRhsProg
		CASE 0
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<94.536766,3723.668700,38.373657>>, <<114.679863,3747.909912,41.590752>>, 7.750000)
				IF NOT IS_PED_INJURED(gangEnemy[28].ped)
					IF NOT IS_ENTITY_DEAD(vehTrailerPark[2].veh)
						START_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[2].veh, 12, "trevor1dw")
						PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[2].veh)
					
						OPEN_SEQUENCE_TASK(seq)
							TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_OFF)
						//	TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,  << 99.0061, 3742.8284, 38.7320 >>, PEDMOVE_RUN, -1, 0.5)	
							TASK_ENTER_VEHICLE(NULL, vehTrailerPark[2].veh)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(gangEnemy[28].ped, seq) 
						iDoFleeOnBikeRhsProg++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(gangEnemy[28].ped)
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[2].veh)
					IF IS_PED_SITTING_IN_VEHICLE(gangEnemy[28].ped, vehTrailerPark[2].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[2].veh)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[2].veh)
							bFuelCanDropFromVeh[2] = TRUE
							iDoFleeOnBikeRhsProg++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT gangEnemy[28].bTriggered
				IF NOT IS_ENTITY_DEAD(gangEnemy[28].ped)
					IF NOT IS_ENTITY_DEAD(vehTrailerPark[2].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[2].veh)
							SET_GANG_MEMBER_ATTACKS_AT_CURRENT_LOC(28, DA_AT_GO_TO_LARGE) 
							gangEnemy[28].bTriggered = TRUE
						ELSE
							IF NOT IS_PED_SITTING_IN_VEHICLE(gangEnemy[28].ped, vehTrailerPark[2].veh)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[2].veh)
								SET_GANG_MEMBER_ATTACKS_AT_CURRENT_LOC(28, DA_AT_GO_TO_LARGE) 
								gangEnemy[28].bTriggered = TRUE
							ENDIF
						ENDIF
					ELSE
						SET_GANG_MEMBER_ATTACKS_AT_CURRENT_LOC(28, DA_AT_GO_TO_LARGE) 
						gangEnemy[28].bTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iBikesForPlayerThroughCentreSetPiece
//PURPOSE: Setpiece of a bunch of bikers turning up and attacking the playewr for if the player goes straight for the centre of the trailer park,
// rather than proceeding clockwise or anti-clockwise around it.
PROC DO_BIKES_FOR_PLAYER_THROUGH_CENTRE_SETPIECE()
	SWITCH iBikesForPlayerThroughCentreSetPiece
		CASE 0
			IF HAS_PLAYER_APPROACHED_CENTRE_FROM_MIDDLE()
				IF NOT bDoneCentreFight
					SETUP_THESE_PEDS(PED_FIGHT_THROUGH_MIDDLE_BIKES_LHS)	
					
					SETUP_THESE_PEDS(PED_FIGHT_THROUGH_MIDDLE_BIKES_RHS)
					iBikesForPlayerThroughCentreSetPiece++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT gangEnemy[13].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[16].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[16].veh)
						gangEnemy[13].vGotoCoords = << 47.0720, 3724.4338, 38.8413 >>
						gangEnemy[13].gang_da = DA_AT_GO_TO
						gangEnemy[13].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[13].bTriggered = TRUE
					ENDIF
				ELSE
					gangEnemy[13].bTriggered = TRUE
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[12].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[15].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[15].veh)
						gangEnemy[12].vGotoCoords =  << 54.1816, 3721.0938, 39.1488 >>
						gangEnemy[12].gang_da = DA_AT_GOTO_V_LARGE
						gangEnemy[12].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[12].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[12].bTriggered = TRUE
						
					ENDIF
				ELSE
					gangEnemy[12].bTriggered = TRUE
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[32].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[20].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[20].veh)
						gangEnemy[32].vGotoCoords =   << 70.2943, 3731.3589, 38.9058 >>
						gangEnemy[32].gang_da = DA_AT_GO_TO
						gangEnemy[32].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[32].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[32].bTriggered = TRUE
					ENDIF
				ELSE
					gangEnemy[32].bTriggered = TRUE
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[33].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[21].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[21].veh)
						gangEnemy[33].vGotoCoords =   << 67.1090, 3728.9067, 38.7357 >>
						gangEnemy[33].gang_da = DA_AT_GO_TO
						gangEnemy[33].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[33].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[33].bTriggered = TRUE
					ENDIF
				ELSE
					gangEnemy[33].bTriggered = TRUE
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[34].bTriggered
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[22].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[22].veh)
						gangEnemy[34].vGotoCoords =   << 59.8464, 3723.7600, 38.8078 >>
						gangEnemy[34].gang_da = DA_AT_GOTO_V_LARGE
						gangEnemy[34].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[34].moveAttackStyle = MOVE_AIM_WHILE_GOTO
						gangEnemy[34].bTriggered = TRUE
					ENDIF
				ENDIF
			ELSE
				gangEnemy[34].bTriggered = TRUE
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC
PROC MANAGE_GANG_BLIPS()
	INT i
	INT iDog
	
	REPEAT COUNT_OF(gangEnemy) i
		IF DOES_ENTITY_EXIST(gangEnemy[i].ped)
			IF NOT IS_PED_INJURED(gangEnemy[i].ped)
				IF NOT DOES_BLIP_EXIST(gangEnemy[i].blipPed)
					//PRINTSTRING("ADDING GANG BLIP FOR:")PRINTINT(i)PRINTNL()
					//gangEnemy[i].blipPed = CREATE_BLIP_FOR_PED(gangEnemy[i].ped, TRUE)
					//SET_BLIP_SCALE(gangEnemy[i].blipPed,0.5)
					//SET_BLIP_FLASHES(gangEnemy[i].blipPed, TRUE)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(gangEnemy[i].blipPed)
					PRINTSTRING("REMOVING BLIP FOR:")PRINTINT(i)PRINTNL()
					REMOVE_BLIP(gangEnemy[i].blipPed)
				ENDIF
			ENDIF	
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(gangDog) iDog
		IF DOES_ENTITY_EXIST(gangDog[iDog].ped)
			IF NOT IS_PED_INJURED(gangDog[iDog].ped)
				IF NOT DOES_BLIP_EXIST(gangDog[iDog].blipPed)
					//PRINTSTRING("ADDING DOG BLIP FOR:")PRINTINT(iDog)PRINTNL()
					//gangDog[iDog].blipPed = CREATE_BLIP_FOR_PED(gangDog[iDog].ped, TRUE)
					//SET_BLIP_SCALE(gangDog[iDog].blipPed,0.5)
					//SET_BLIP_FLASHES(gangEnemy[i].blipPed, TRUE)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(gangDog[iDog].blipPed)
					PRINTSTRING("REMOVING DOG BLIP FOR:")PRINTINT(iDog)PRINTNL()
					REMOVE_BLIP(gangDog[iDog].blipPed)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC	

PROC MANAGE_FUEL_CANNISTER_TARGETTING()

	INT iTempCounter
	
	//iCounter = 0
	IF MANAGE_MY_TIMER(iFuelTarget,1000)
		FOR iCounter = 0 TO 14
			IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),oFuelCan[iCounter]) < 6
					IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
						SET_OBJECT_TARGETTABLE(oFuelCan[iCounter], FALSE)
						SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[iCounter],FALSE, 10.0)
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),oFuelCan[iCounter]) < 22
						FOR iTempCounter = 0 TO MAX_GANG -1
							IF NOT IS_PED_INJURED(gangEnemy[iTempCounter].ped)	
								IF IS_ENTITY_AT_ENTITY(gangEnemy[iTempCounter].ped,oFuelCan[iCounter],<<6,6,6>>)
									IF iCounter != 14
									OR iCounter != 7
										IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
											SET_OBJECT_TARGETTABLE(oFuelCan[iCounter], TRUE)
											SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[iCounter],TRUE, 10.0)
											//PRINTSTRING("SETTING PRIORITY ON:")PRINTINT(iCounter)PRINTNL()
											iTempCounter = MAX_GANG
										ENDIF
									ENDIF
								ELSE
									IF iCounter != 14
									OR iCounter != 7
										IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
											SET_OBJECT_TARGETTABLE(oFuelCan[iCounter], FALSE)
											SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[iCounter],FALSE, 10.0)
											//PRINTSTRING("SETTING PRIORITY OFF:")PRINTINT(iCounter)PRINTNL()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							iFuelTarget = GET_GAME_TIMER()
						ENDFOR
					ELSE
						IF iCounter != 14
						OR iCounter != 7
							IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
								SET_OBJECT_TARGETTABLE(oFuelCan[iCounter], FALSE)
								SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[iCounter],FALSE, 10.0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC MANAGE_BIKER_BEHAVIOR()
	
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[15].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[15].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[12].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[15].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[12].ped,vehTrailerPark[15].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[15].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[12].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[12].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(gangEnemy[12].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[15].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[15].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[15].veh)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[16].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[16].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[13].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[16].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[13].ped,vehTrailerPark[16].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[16].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[13].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[13].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(gangEnemy[13].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[16].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[16].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[16].veh)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[17].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[17].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[14].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[17].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[14].ped,vehTrailerPark[17].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[17].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[14].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[14].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(gangEnemy[14].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[14].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[14].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[14].veh)
			ENDIF
		ENDIF
	ENDIF
	
	//SECOND WAVE - FROM BEHIND ON LEFT ROUTE
	
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[24].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[24].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[24].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[24].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[24].ped,vehTrailerPark[24].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[24].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[24].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[24].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(gangEnemy[24].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[24].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[24].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[24].veh)
			ENDIF
		ENDIF
	ENDIF

	
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[25].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[25].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[25].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[25].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[25].ped,vehTrailerPark[25].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[25].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[25].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[25].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(gangEnemy[25].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[25].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[25].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[25].veh)
			ENDIF
		ENDIF
	ENDIF
	
	//RIGHT ROUTE
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[20].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[20].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[32].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[20].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[32].ped,vehTrailerPark[20].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[20].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[32].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[32].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(gangEnemy[32].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[20].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[20].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[20].veh)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[21].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[21].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[33].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[21].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[33].ped,vehTrailerPark[21].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[21].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[33].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[33].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(gangEnemy[33].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[21].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[21].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[21].veh)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[22].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[22].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[34].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[22].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[34].ped,vehTrailerPark[22].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[22].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[34].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[34].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						TASK_COMBAT_PED(gangEnemy[34].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[22].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[22].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[22].veh)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[2].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[2].veh) 
			//IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[17].veh) OR IS_PED_INJURED(gangEnemy[14].ped)
			IF IS_PED_INJURED(gangEnemy[28].ped)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[2].veh)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(gangEnemy[28].ped,vehTrailerPark[2].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[2].veh)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(gangEnemy[28].ped)
				IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
					IF GET_SCRIPT_TASK_STATUS(gangEnemy[28].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
						//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[28].ped,TRUE)
						TASK_COMBAT_PED(gangEnemy[28].ped,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehTrailerPark[2].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[2].veh) 
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[2].veh)
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

INT iMethDialogue
INT iExplosionDialogue

BOOL T1M1_BLOW
BOOL T1M1_FIG2

PROC MANAGE_SET_PIECE_DIALOGUE()
		//Someone tries to run Trevor down
		IF NOT T1M1_FIG2
			IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehTrailerPark[12].veh) < 35
				OR iVehicleBlowUpSetPiece > 3 AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTrailerPark[12].veh,PLAYER_PED_ID())
					IF NOT IS_ENTITY_OCCLUDED(vehTrailerPark[12].veh)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_FIG2",CONV_PRIORITY_HIGH)
								T1M1_FIG2 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Trevor sees bikers in a van
		IF HAS_LABEL_BEEN_TRIGGERED("T1M1_FIG2")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_DVA")
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehTrailerPark[12].veh) < 25
						IF NOT IS_VEHICLE_SEAT_FREE(vehTrailerPark[12].veh)
							IF NOT IS_ENTITY_OCCLUDED(vehTrailerPark[12].veh)
								IF NOT IS_PED_INJURED(gangEnemy[17].ped)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DVA",CONV_PRIORITY_HIGH)
											iVanDialogue = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("T1M1_DVA",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MANAGE_MY_TIMER(iVanDialogue,12000)
					SET_LABEL_AS_TRIGGERED("T1M1_DVA",FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		//blown up truck
		IF bBlowTruckUpDialogue = FALSE
			IF DOES_ENTITY_EXIST(vehTrailerPark[12].veh)
				IF NOT IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BLOW3",CONV_PRIORITY_HIGH)
							bBlowTruckUpDialogue = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Trevor see sees the meth lab
		
		IF iTrailerRayfireStage < 4
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_METH")
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 36.9364, 3730.8999, 38.6028 >>) < 45
						IF DOES_ENTITY_EXIST(oFuelCan[14])
							IF IS_ENTITY_ON_SCREEN(oFuelCan[14])
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_METH",CONV_PRIORITY_HIGH)
										iMethDialogue = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("T1M1_METH",TRUE)
										SET_LABEL_AS_TRIGGERED("T1M1_METH_ONCE",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(oFuelCan[7])
							IF IS_ENTITY_ON_SCREEN(oFuelCan[7])
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_METH",CONV_PRIORITY_HIGH)
										iMethDialogue = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("T1M1_METH",TRUE)
										SET_LABEL_AS_TRIGGERED("T1M1_METH_ONCE",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MANAGE_MY_TIMER(iMethDialogue,16000)
					SET_LABEL_AS_TRIGGERED("T1M1_METH",FALSE)
				ENDIF
			ENDIF
		ENDIF
	
		//Blows up meth lab

		IF iTrailerRayfireStage > 3
			IF NOT T1M1_BLOW
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BLOW",CONV_PRIORITY_HIGH)
						T1M1_BLOW = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//has trevor blown something up
		IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_DVA")
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),12)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BLOW2",CONV_PRIORITY_HIGH)
						iExplosionDialogue = GET_GAME_TIMER()
						SET_LABEL_AS_TRIGGERED("T1M1_BLOW2",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MANAGE_MY_TIMER(iExplosionDialogue,12000)
				SET_LABEL_AS_TRIGGERED("T1M1_BLOW2",FALSE)
			ENDIF
		ENDIF

		//Bikers
		IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_DUA")
			IF NOT IS_PED_INJURED(gangEnemy[12].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[12].ped) < 45
					IF IS_PED_IN_ANY_VEHICLE(gangEnemy[12].ped)
						IF NOT IS_ENTITY_OCCLUDED(gangEnemy[12].ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DUA",CONV_PRIORITY_HIGH)
									iBikesDialogue = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("T1M1_DUA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(gangEnemy[13].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[13].ped) < 45
					IF IS_PED_IN_ANY_VEHICLE(gangEnemy[13].ped)
						IF NOT IS_ENTITY_OCCLUDED(gangEnemy[13].ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DUA",CONV_PRIORITY_HIGH)
									iBikesDialogue = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("T1M1_DUA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(gangEnemy[32].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[32].ped) < 45
					IF IS_PED_IN_ANY_VEHICLE(gangEnemy[32].ped)
						IF NOT IS_ENTITY_OCCLUDED(gangEnemy[32].ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DUA",CONV_PRIORITY_HIGH)
									iBikesDialogue = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("T1M1_DUA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(gangEnemy[33].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[33].ped) < 45
					IF IS_PED_IN_ANY_VEHICLE(gangEnemy[33].ped)
						IF NOT IS_ENTITY_OCCLUDED(gangEnemy[33].ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DUA",CONV_PRIORITY_HIGH)
									iBikesDialogue = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("T1M1_DUA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(gangEnemy[34].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[34].ped) < 45
					IF IS_PED_IN_ANY_VEHICLE(gangEnemy[34].ped)
						IF NOT IS_ENTITY_OCCLUDED(gangEnemy[34].ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DUA",CONV_PRIORITY_HIGH)
									iBikesDialogue = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("T1M1_DUA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(gangEnemy[24].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[24].ped) < 45
					IF IS_PED_IN_ANY_VEHICLE(gangEnemy[24].ped)
						IF NOT IS_ENTITY_OCCLUDED(gangEnemy[24].ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DUA",CONV_PRIORITY_HIGH)
									iBikesDialogue = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("T1M1_DUA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(gangEnemy[25].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[25].ped) < 45
					IF IS_PED_IN_ANY_VEHICLE(gangEnemy[25].ped)
						IF NOT IS_ENTITY_OCCLUDED(gangEnemy[25].ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DUA",CONV_PRIORITY_HIGH)
									iBikesDialogue = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("T1M1_DUA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(gangEnemy[7].ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[7].ped) < 45
					IF IS_PED_IN_ANY_VEHICLE(gangEnemy[7].ped)
						IF NOT IS_ENTITY_OCCLUDED(gangEnemy[7].ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DUA",CONV_PRIORITY_HIGH)
									iBikesDialogue = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("T1M1_DUA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MANAGE_MY_TIMER(iBikesDialogue,12000)
				SET_LABEL_AS_TRIGGERED("T1M1_DUA",FALSE)
			ENDIF
		ENDIF

ENDPROC

PROC BATTLE_DIALOGUE()
	
	IF iBattleDialogue > 1
		MANAGE_SET_PIECE_DIALOGUE()
	ENDIF

	INT iRand
		
	SWITCH iBattleDialogue
		
		CASE 0
			ADD_PED_FOR_DIALOGUE(structConvoPeds,2,PLAYER_PED_ID(),"TREVOR")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_PH8")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PH8", CONV_PRIORITY_MEDIUM) // Pulling into park
						iBattleDialogue++
					ENDIF
				ENDIF
			ELSE
				iBattleDialogue++
			ENDIF
		BREAK
	
		CASE 1
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV_GT3")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT_GOD_TEXT("TRV_GT3")
					SET_LABEL_AS_TRIGGERED("TRV_GT3",TRUE)
				ENDIF
			ELSE
				iBattleDialogueTimer = GET_GAME_TIMER()
				SET_LABEL_AS_TRIGGERED("TRV_GT3",FALSE)
				iBattleDialogue = 99
			ENDIF
		
		BREAK
		
		CASE 99
			iBattleDialogue = 2
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(18000,20000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_P21",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(18000,20000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_P20",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 4
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(14000,20000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_P18",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(18000,22000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_P10",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(15000,20000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_P23",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(15000,23000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_PH9",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(14000,22000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_P22",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 9
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(18000,22000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_P19",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(18000,24000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DXA",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 11
			IF MANAGE_MY_TIMER(iBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(18000,20000))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DYA",CONV_PRIORITY_MEDIUM)
							iBattleDialogueTimer = GET_GAME_TIMER()
							iRand = GET_RANDOM_INT_IN_RANGE(2,12)
							IF iBattleDialogue = iRand
								iBattleDialogue ++
							ELSE
								iBattleDialogue =iRand
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK		
		
	ENDSWITCH
	
ENDPROC

BOOL bDoRageDialogue
INT iRageDialogueTimer

PROC MANAGE_TREVOR_RAGE()

	IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF bDoRageDialogue = FALSE
					IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BIKERAG",CONV_PRIORITY_MEDIUM)
						iRageDialogueTimer = GET_GAME_TIMER()
						bDoRageDialogue = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bDoRageDialogue = TRUE
			IF MANAGE_MY_TIMER(iRageDialogueTimer,8000)
				bDoRageDialogue = FALSE
			ENDIF
		ENDIF
	ENDIF

			

	SWITCH iRage
		
		CASE 0
			iRageTimer = GET_GAME_TIMER()
			REQUEST_VEHICLE_ASSET(HEXER)
			iRage++
		BREAK
		
		CASE 1
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF HAS_VEHICLE_ASSET_LOADED(HEXER)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
						IF MANAGE_MY_TIMER(iRageTimer,3000)
							REQUEST_VEHICLE_ASSET(HEXER)
							SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
							SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(),0.8,TRUE)
							
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								PRINT_HELP("TRV_NRAGEH1_KM",DEFAULT_HELP_TEXT_TIME)
							ELSE
								PRINT_HELP("TRV_NRAGEH1",DEFAULT_HELP_TEXT_TIME)
							ENDIF
							
							//936905
							FLASH_ABILITY_BAR(DEFAULT_HELP_TEXT_TIME)
							iRageTimer = GET_GAME_TIMER()
							iRage++
						ENDIF
					ELSE
						IF MANAGE_MY_TIMER(iRageTimer,30000)
							REQUEST_VEHICLE_ASSET(HEXER)
							SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
							SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(),0.8,TRUE)
							
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								PRINT_HELP("TRV_NRAGEH1_KM",DEFAULT_HELP_TEXT_TIME)
							ELSE
								PRINT_HELP("TRV_NRAGEH1",DEFAULT_HELP_TEXT_TIME)
							ENDIF
							
							//936905
							FLASH_ABILITY_BAR(DEFAULT_HELP_TEXT_TIME)
							iRageTimer = GET_GAME_TIMER()
							iRage++
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		
		CASE 2
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
			OR MANAGE_MY_TIMER(iRageTimer,DEFAULT_HELP_TEXT_TIME + 10000)
				PRINT_HELP("TRV_NRAGEH2",DEFAULT_HELP_TEXT_TIME)
				iRageTimer = GET_GAME_TIMER()
				iRage++
			ENDIF
		BREAK
		
		CASE 3
			//HOW TO GAIN RAGE
			IF MANAGE_MY_TIMER(iRageTimer,DEFAULT_HELP_TEXT_TIME)
				CLEAR_HELP()
				PRINT_HELP("TRV_NRAGEH3",-1)
				iRageTimer = GET_GAME_TIMER()
				iRage++
			ENDIF
		BREAK 
		
		CASE 4
			IF MANAGE_MY_TIMER(iRageTimer,DEFAULT_HELP_TEXT_TIME)
				CLEAR_HELP()
				iRageTimer = GET_GAME_TIMER()
				iRage++	
			ENDIF
		BREAK
		
		CASE 5
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV_GT3")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT_GOD_TEXT("TRV_GT3")
					SET_LABEL_AS_TRIGGERED("TRV_GT3",TRUE)
					iRageTimer = GET_GAME_TIMER()
					iRage++	
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF MANAGE_MY_TIMER(iRageTimer,7000)
				PRINT_HELP("TRV_LOCK_BEHIND",DEFAULT_HELP_TEXT_TIME)
				iRageTimer = GET_GAME_TIMER()
				iRage++	
			ENDIF
		BREAK
		
		CASE 7
			IF MANAGE_MY_TIMER(iRageTimer,9000)
				IF IS_PED_IN_COVER(PLAYER_PED_ID())
					PRINT_HELP("TRV_GO_ROUND",9000)
					iRageTimer = GET_GAME_TIMER()
					iRage++	
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF MANAGE_MY_TIMER(iRageTimer,9000)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<57.366703,3699.954834,37.754868>>, <<76.109993,3725.176025,44.237411>>, 22.750000)
					PRINT_HELP("TRV_JUMP_OVER",9000)
					iRage++	
				ENDIF
			ENDIF
		BREAK

	ENDSWITCH

ENDPROC

PROC MANAGE_CRACK_WHORES()

	SEQUENCE_INDEX seq

	//vars


	SWITCH iWhore
	
	CASE 0
		REQUEST_MODEL(S_F_Y_HOOKER_02)
		bTriggerWhore[0] = FALSE
		bTriggerWhore[1] = FALSE
		bTriggerWhore[2] = FALSE
		
		IF DOES_ENTITY_EXIST(pedWhore[0])
			DELETE_PED(pedWhore[0])
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedWhore[1])
			DELETE_PED(pedWhore[1])
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedWhore[2])
			DELETE_PED(pedWhore[2])
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedWhore[3])
			DELETE_PED(pedWhore[3])
		ENDIF
		
		iWhore ++
	BREAK
	
	CASE 1
		IF HAS_MODEL_LOADED(S_F_Y_HOOKER_02)
		
			IF NOT DOES_ENTITY_EXIST(pedWhore[0])
				pedWhore[0] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_HOOKER_02, << 57.3807, 3689.1533, 39.1483 >>, 338.8795)
				ADD_DEADPOOL_TRIGGER(pedWhore[0], TRV1_KILLS)
			ENDIF
			IF NOT IS_PED_INJURED(pedWhore[0])

				//SET_PED_COMPONENT_VARIATION(pedWhore[0], PED_COMP_HEAD, 0, 0) //(head)
				//SET_PED_COMPONENT_VARIATION(pedWhore[0], PED_COMP_HAIR, 0, 0, 0) //(hair)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWhore[0],TRUE)

			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedWhore[1])
				pedWhore[1] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_HOOKER_02, << 56.0525, 3691.1255, 39.1616 >>, 335.4988)
				ADD_DEADPOOL_TRIGGER(pedWhore[1], TRV1_KILLS)
			ENDIF
			IF NOT IS_PED_INJURED(pedWhore[1])
				//SET_PED_COMPONENT_VARIATION(pedWhore[1], PED_COMP_HEAD, 0, 0) //(head)
				//SET_PED_COMPONENT_VARIATION(pedWhore[1], PED_COMP_HAIR, 2, 0, 0) //(hair)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWhore[1],TRUE)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedWhore[2])
				pedWhore[2] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_HOOKER_02, << 68.8716, 3685.7202, 38.8602 >>, 114.3456)
				ADD_DEADPOOL_TRIGGER(pedWhore[2], TRV1_KILLS)
			ENDIF
			IF NOT IS_PED_INJURED(pedWhore[2])
				SET_PED_COMPONENT_VARIATION(pedWhore[2], PED_COMP_HEAD, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedWhore[2], PED_COMP_HAIR, 1, 0, 0) //(hair)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWhore[2],TRUE)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedWhore[3])
				pedWhore[3] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_HOOKER_02, << 62.1334, 3671.5449, 38.8040 >>, 4.7041)
				ADD_DEADPOOL_TRIGGER(pedWhore[3], TRV1_KILLS)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedWhore[3])
				//SET_PED_COMPONENT_VARIATION(pedWhore[3], PED_COMP_HEAD, 0, 0) //(head)
				//SET_PED_COMPONENT_VARIATION(pedWhore[3], PED_COMP_HAIR, 0, 0, 0) //(hair)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWhore[3],TRUE)
			ENDIF
			
			iWhore ++
		ELSE
			REQUEST_MODEL(S_F_Y_HOOKER_02)
		ENDIF
	BREAK
	
	CASE 2
		
			IF NOT IS_PED_INJURED(pedWhore[0])
				TASK_COWER(pedWhore[0],-1)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedWhore[1])
				TASK_COWER(pedWhore[1],-1)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedWhore[2])
				TASK_COWER(pedWhore[2],-1)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedWhore[3])
				TASK_COWER(pedWhore[3],-1)
			ENDIF
			
			
			iWhore ++
		
	BREAK
	
	CASE 3
		IF bTriggerWhore[0] = FALSE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 60.3862, 3691.0547, 38.8603 >>,<<5,5,5>>)
				IF NOT IS_PED_INJURED(pedWhore[0])
					CLEAR_PED_TASKS(pedWhore[0])
					OPEN_SEQUENCE_TASK(seq)
						TASK_COWER(NULL,2000)
						TASK_STAND_STILL(NULL,200)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150.0, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedWhore[0], seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
			
				IF NOT IS_PED_INJURED(pedWhore[1])
					CLEAR_PED_TASKS(pedWhore[1])
					OPEN_SEQUENCE_TASK(seq)
						TASK_COWER(NULL,200)
						TASK_STAND_STILL(NULL,600)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150.0, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedWhore[1], seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				bTriggerWhore[0] = TRUE
			ENDIF
		ENDIF
		
		IF bTriggerWhore[1] = FALSE
			IF NOT IS_PED_INJURED(pedWhore[2])
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),pedWhore[2],<<5,5,5>>)
					IF NOT IS_PED_INJURED(pedWhore[2])
						CLEAR_PED_TASKS(pedWhore[2])
						OPEN_SEQUENCE_TASK(seq)
							TASK_HANDS_UP(NULL,3000)
							TASK_COWER(NULL,8000)
							TASK_STAND_STILL(NULL,200)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150.0, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedWhore[2], seq)
						CLEAR_SEQUENCE_TASK(seq)
						bTriggerWhore[1] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bTriggerWhore[2] = FALSE
			IF NOT IS_PED_INJURED(pedWhore[3])
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),pedWhore[3],<<5,5,5>>)
					IF NOT IS_PED_INJURED(pedWhore[3])
						CLEAR_PED_TASKS(pedWhore[3])
						OPEN_SEQUENCE_TASK(seq)
							TASK_HANDS_UP(NULL,2000)
							TASK_COWER(NULL,6000)
							TASK_STAND_STILL(NULL,200)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150.0, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedWhore[3], seq)
						CLEAR_SEQUENCE_TASK(seq)
						bTriggerWhore[2] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	BREAK
	
	ENDSWITCH

ENDPROC

WEAPON_TYPE wtCurrentPlayer

FUNC BOOL IS_PLAYER_USING_A_SHOGUN()
	
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtCurrentPlayer,FALSE)
	
	IF wtCurrentPlayer =WEAPONTYPE_SAWNOFFSHOTGUN
	OR wtCurrentPlayer =WEAPONTYPE_PUMPSHOTGUN
		RETURN TRUE		
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

INT iVehicleBlowUpTimer
VEHICLE_INDEX vehPlayerTemp

PROC MANAGE_BLOW_UP_VEHICLE()

	//FLOAT fEngineHealth 
	
	//PRINTSTRING("iVehicleBlowUpSetPiece:")PRINTINT(iVehicleBlowUpSetPiece)PRINTNL()
	
	
	
	IF iVehicleBlowUpSetPiece > 0
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehPlayerTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTrailerPark[12].veh,vehPlayerTemp)
				OR IS_ENTITY_TOUCHING_ENTITY(vehTrailerPark[12].veh,vehPlayerTemp)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[12].veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh)	
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
			IF IS_PED_INJURED(gangEnemy[17].ped)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[12].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh)	
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
			IF IS_ENTITY_IN_ANGLED_AREA(vehTrailerPark[12].veh, <<75.252258,3649.630371,37.520111>>, <<68.415886,3646.147461,41.060265>>, 5.000000)
			OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[12].veh)
				IF NOT gangEnemy[17].bTriggered
					IF NOT gangEnemy[17].bHasSequence
						IF NOT IS_PED_INJURED(gangEnemy[17].ped)
							IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
								IF IS_PED_SITTING_IN_VEHICLE(gangEnemy[17].ped, vehTrailerPark[12].veh)
									OPEN_SEQUENCE_TASK(seqSequence)
										TASK_LEAVE_VEHICLE(NULL, vehTrailerPark[12].veh, ECF_DONT_CLOSE_DOOR)
										TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(seqSequence)
									TASK_PERFORM_SEQUENCE(gangEnemy[17].ped, seqSequence)
									CLEAR_SEQUENCE_TASK(seqSequence)
									gangEnemy[17].bHasSequence = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF HAS_PED_PROCESSED_SEQ(gangEnemy[17].ped)
							SET_GANG_MEMBER_ATTACKS_AT_CURRENT_LOC(17, DA_AT_GOTO_V_LARGE)
							gangEnemy[17].bTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT gangEnemy[17].bTriggered
				IF NOT gangEnemy[17].bHasSequence
					IF NOT IS_PED_INJURED(gangEnemy[17].ped)
						OPEN_SEQUENCE_TASK(seqSequence)
							TASK_LEAVE_ANY_VEHICLE(NULL)
							TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seqSequence)
						TASK_PERFORM_SEQUENCE(gangEnemy[17].ped, seqSequence)
						CLEAR_SEQUENCE_TASK(seqSequence)
						gangEnemy[17].bHasSequence = TRUE
					ENDIF
				ELSE
					IF HAS_PED_PROCESSED_SEQ(gangEnemy[17].ped)
						PLAY_PED_AMBIENT_SPEECH(gangEnemy[17].ped,"GENERIC_CURSE_HIGH",SPEECH_PARAMS_INTERRUPT_SHOUTED)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[17].ped,FALSE)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[17].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[17].ped,CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_RANGE(gangEnemy[17].ped,CR_NEAR)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[17].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
						gangEnemy[17].bTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

	SWITCH iVehicleBlowUpSetPiece
	
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<29.072308,3683.838867,38.082333>>, <<38.745857,3665.138916,42.134918>>, 48.250000)
				OR (iDoPedFleeOnBikeSetPieceProg > 0 AND IS_PED_INJURED(gangEnemy[7].ped))
					IF IS_POINT_VISIBLE_TO_PLAYER(<< 27.5448, 3716.1765, 39.0345 >>, 1.0, 200)
						IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<25.347603,3659.570800,37.758526>>, <<33.682796,3675.039307,43.513779>>, 5.000000)
							SETUP_THESE_PEDS(PED_FIGHT_PICKUP_SETPIECE1)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrailerPark[12].veh,"TREVOR_1_ONCOMING_TRUCK_Group")
							GET_PED_TO_DRIVEBY_PLAYER(gangEnemy[17].ped,  vehTrailerPark[12].veh)
							SET_PLAYBACK_SPEED(vehTrailerPark[12].veh,0.55)
							bBlowTruckUpDialogue = FALSE
							iVehicleBlowUpSetPiece ++
						ENDIF
					ENDIF
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<116.251343,3682.786133,38.005123>>, <<71.231522,3689.022461,42.005123>>, 12.500000)
						IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
							IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(vehTrailerPark[12].veh, FALSE),1.0)
								IF DOES_ENTITY_EXIST(vehTrailerPark[12].veh)
									DELETE_VEHICLE(vehTrailerPark[12].veh)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					IF IS_PLAYER_USING_A_SHOGUN()
						IF GET_ENTITY_HEALTH(vehTrailerPark[12].veh) < 1000
							SET_VEHICLE_ENGINE_HEALTH(vehTrailerPark[12].veh,500)
							SET_ENTITY_HEALTH(vehTrailerPark[12].veh,500)
							//PRINT_GOD_TEXT("BALLS_1")
							iVehicleBlowUpSetPiece = 3
						ENDIF
					ENDIF
				ELSE
				
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					IF GET_ENTITY_HEALTH(vehTrailerPark[12].veh) < 500
						IF IS_PLAYER_USING_A_SHOGUN()
							SET_VEHICLE_ENGINE_HEALTH(vehTrailerPark[12].veh,400)
							SET_ENTITY_HEALTH(vehTrailerPark[12].veh,400)
							//PRINT_GOD_TEXT("BALLS_2")
							iVehicleBlowUpSetPiece ++
						ENDIF
					ENDIF
				ELSE
					iVehicleBlowUpSetPiece ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					IF GET_ENTITY_HEALTH(vehTrailerPark[12].veh) < 400
						IF IS_PLAYER_USING_A_SHOGUN()
							SET_VEHICLE_ENGINE_HEALTH(vehTrailerPark[12].veh,300)
							SET_ENTITY_HEALTH(vehTrailerPark[12].veh,300)
							//PRINT_GOD_TEXT("BALLS_2")
							iVehicleBlowUpSetPiece ++
						ENDIF
					ENDIF
				ELSE
					iVehicleBlowUpSetPiece ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					IF GET_ENTITY_HEALTH(vehTrailerPark[12].veh) < 400
						IF IS_PLAYER_USING_A_SHOGUN()
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[12].veh)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh)
							ENDIF
							SET_VEHICLE_PETROL_TANK_HEALTH(vehTrailerPark[12].veh,-100)
							SET_VEHICLE_ENGINE_HEALTH(vehTrailerPark[12].veh,-500)
							SET_HORN_PERMANENTLY_ON_TIME(vehTrailerPark[12].veh,4000)
							//PRINT_GOD_TEXT("BALLS_3")
							iVehicleBlowUpTimer = GET_GAME_TIMER()
							iVehicleBlowUpSetPiece ++
						ENDIF
					ENDIF
				ELSE
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[12].veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh)
					ENDIF
					SET_HORN_PERMANENTLY_ON_TIME(vehTrailerPark[12].veh,4000)
					iVehicleBlowUpSetPiece ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					IF NOT MANAGE_MY_TIMER(iVehicleBlowUpTimer,600)
						APPLY_FORCE_TO_ENTITY(vehTrailerPark[12].veh, APPLY_TYPE_FORCE, <<28.0, 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
						iVehicleBlowUpTimer = GET_GAME_TIMER()
						iVehicleBlowUpSetPiece ++
					ENDIF
					
					IF GET_ENTITY_HEALTH(vehTrailerPark[12].veh) < 150
						//EXPLODE_VEHICLE(vehTrailerPark[12].veh)
						iVehicleBlowUpSetPiece ++
					ENDIF
				ELSE
					APPLY_FORCE_TO_ENTITY(vehTrailerPark[12].veh, APPLY_TYPE_FORCE, <<6.0, 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
					iVehicleBlowUpSetPiece ++
				ENDIF
			ENDIF

		BREAK
		
		CASE 6
			IF MANAGE_MY_TIMER(iVehicleBlowUpTimer,600)
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[12].veh)
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehTrailerPark[12].veh)
						EXPLODE_VEHICLE(vehTrailerPark[12].veh)
						iVehicleBlowUpSetPiece ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC
BOOL TREVOR_1_RAYFIRE

// RAYFIRE FOR YOUR FACE
PROC MANAGE_RAYFIRE_EXPLOSION()

	IF iTrailerRayfireStage > 4
	AND iTrailerRayfireStage < 11
		DISABLE_SELECTOR_THIS_FRAME()
	ENDIF

	//PRINTSTRING("iTrailerRayfireStage:")PRINTINT(iTrailerRayfireStage)PRINTNL()
		
	IF iTrailerRayfireStage > 6
		IF DOES_RAYFIRE_MAP_OBJECT_EXIST(trailerRayfire)
			IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(trailerRayfire) = RFMO_STATE_ENDING
			OR GET_STATE_OF_RAYFIRE_MAP_OBJECT(trailerRayfire) = RFMO_STATE_END
				IF HAS_PTFX_ASSET_LOADED()
					PRINTSTRING("PLAYING END PTFX")PRINTNL()
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("ent_ray_meth_dust_settle", <<40.8358, 3729.0200, 38.6786>>,<<0, 0.0000, -63.8749>>)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_DESTROYED)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER_OCCLUSION, BUILDINGSTATE_DESTROYED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iTrailerRayfireStage > 4
		IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
			IF NOT TREVOR_1_RAYFIRE
				IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_SHOOTOUT_START")
					STOP_AUDIO_SCENE("TREVOR_1_SHOOTOUT_START")
				ELSE
					IF NOT IS_AUDIO_SCENE_ACTIVE("SLOWMO_T1_RAYFIRE_EXPLOSION")
						ACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_T1_RAYFIRE_EXPLOSION") 
						START_AUDIO_SCENE("TREVOR_1_RAYFIRE")
						TREVOR_1_RAYFIRE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iTrailerRayfireStage < 4
		//AUDIO SCENE
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("GANG ENEMY 38 TRIGGERED")
			IF NOT IS_PED_INJURED(gangEnemy[38].ped)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 36.9364, 3730.8999, 38.6028 >>) < 58
					IF IS_ENTITY_ON_SCREEN(gangEnemy[38].ped)
						IF NOT WOULD_ENTITY_BE_OCCLUDED(G_M_Y_LOST_01,GET_ENTITY_COORDS(gangEnemy[38].ped, FALSE))
							IF NOT IS_PED_INJURED(gangEnemy[38].ped)
								SET_PED_CAN_BE_TARGETTED(gangEnemy[38].ped,TRUE)
							ENDIF
							SET_PED_ACCURACY(gangEnemy[38].ped,1)
							OPEN_SEQUENCE_TASK(seqSequence)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL,<<34.9035, 3730.6326, 38.5661>>,PLAYER_PED_ID(),1.0,TRUE, 1.0, 1.0, TRUE,ENAV_DEFAULT,FALSE,DEFAULT,DEFAULT_TIME_NEVER_WARP)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(seqSequence)
							TASK_PERFORM_SEQUENCE(gangEnemy[38].ped,seqSequence)	
							CLEAR_SEQUENCE_TASK(seqSequence)
							SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[38].ped,<<34.9035, 3730.6326, 38.5661>>,20.0,TRUE)
							SET_PED_COMBAT_ATTRIBUTES(gangEnemy[38].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[38].ped,FALSE)
							SET_ENTITY_LOAD_COLLISION_FLAG(gangEnemy[38].ped,TRUE)
							PLAY_PED_AMBIENT_SPEECH(gangEnemy[38].ped,"GENERIC_WAR_CRY",SPEECH_PARAMS_INTERRUPT_SHOUTED)
							SET_LABEL_AS_TRIGGERED("GANG ENEMY 38 TRIGGERED",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
		IF iTrailerRayfireStage > 2
		AND iTrailerRayfireStage < 4
			IF NOT WOULD_ENTITY_BE_OCCLUDED(Prop_GasCyl_02A,<<35.614193,3731.956543,38.527878>>,FALSE)
				IF DOES_ENTITY_EXIST(oFuelCan[14])
					SET_OBJECT_TARGETTABLE(oFuelCan[14],TRUE)
					SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[14],TRUE,0.5)
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(oFuelCan[14])
					SET_OBJECT_TARGETTABLE(oFuelCan[14],FALSE)
					SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[14],FALSE,0.5)
				ENDIF
			ENDIF
			
			IF NOT WOULD_ENTITY_BE_OCCLUDED(Prop_GasCyl_02A,<<39.845531,3735.912354,38.527878>>,FALSE)
				IF DOES_ENTITY_EXIST(oFuelCan[7])
					SET_OBJECT_TARGETTABLE(oFuelCan[7],TRUE)
					SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[7],TRUE,0.5)
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(oFuelCan[7])
					SET_OBJECT_TARGETTABLE(oFuelCan[7],FALSE)
					SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[7],FALSE,0.5)
				ENDIF
			ENDIF
		ENDIF
	
      SWITCH iTrailerRayfireStage
	  
	  		CASE 0
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 36.9364, 3730.8999, 38.6028 >>) < 90
					//REQUEST_IPL("des_methtrailer")
					IF NOT IS_PED_INJURED(gangEnemy[39].ped)
						SET_PED_CAN_BE_TARGETTED(gangEnemy[39].ped,TRUE)
						SET_PED_COMBAT_MOVEMENT(gangEnemy[39].ped,CM_STATIONARY)
						SET_PED_ACCURACY(gangEnemy[39].ped,1)
						TASK_COMBAT_PED(gangEnemy[39].ped,PLAYER_PED_ID())
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[39].ped,TRUE)
						SET_ENTITY_LOAD_COLLISION_FLAG(gangEnemy[39].ped,TRUE)
						PRINTSTRING("39 ASSIGNED")PRINTNL()
					ENDIF
					TREVOR_1_RAYFIRE = FALSE
					 
					iTrailerRayfireStage++
				ENDIF
            BREAK
			
			CASE 1
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 36.9364, 3730.8999, 38.6028 >>) < 85
					IF LOAD_STREAM("TREVOR_1_TRAILER_EXPLODE_MASTER")
						trailerRayfire = GET_RAYFIRE_MAP_OBJECT(<< 36.9364, 3730.8999, 38.6028 >>, 10, "DES_methtrailer")
						IF DOES_RAYFIRE_MAP_OBJECT_EXIST(trailerRayfire)
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(trailerRayfire, RFMO_STATE_PRIMING)
							iTrailerRayfireStage++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
	  		CASE 2
				
				//vFuelCanLoc[7] = <<39.845531,3735.912354,38.527878>>
				//vFuelCanLoc[14] = <<35.614193,3731.956543,38.527878>>
				IF DOES_ENTITY_EXIST(oFuelCan[14])
				OR DOES_ENTITY_EXIST(oFuelCan[7])
					IF NOT WOULD_ENTITY_BE_OCCLUDED(Prop_GasCyl_02A,<<35.614193,3731.956543,38.527878>>,FALSE)
						IF DOES_ENTITY_EXIST(oFuelCan[14])
							SET_OBJECT_TARGETTABLE(oFuelCan[14],TRUE)
							SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[14],TRUE,0.5)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(oFuelCan[14])
							CLEAR_ENTITY_LAST_WEAPON_DAMAGE(oFuelCan[14])
							iTrailerRayfireStage++
						ENDIF
					ELSE
						IF NOT WOULD_ENTITY_BE_OCCLUDED(Prop_GasCyl_02A,<<39.845531,3735.912354,38.527878>>,FALSE)
							IF DOES_ENTITY_EXIST(oFuelCan[7])
								SET_OBJECT_TARGETTABLE(oFuelCan[7],TRUE)
								SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[7],TRUE,0.5)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(oFuelCan[7])
								CLEAR_ENTITY_LAST_WEAPON_DAMAGE(oFuelCan[7])
								iTrailerRayfireStage++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK 
			
			CASE 3
				IF DOES_ENTITY_EXIST(oFuelCan[14])
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(oFuelCan[14],PLAYER_PED_ID())
						SET_CONTROL_SHAKE(PLAYER_CONTROL,1500,255)
						FREEZE_ENTITY_POSITION(oFuelCan[14],FALSE)
						SET_ENTITY_HEALTH(oFuelCan[14],0)
						
						IF DOES_ENTITY_EXIST(oFuelCan[7])
							SET_OBJECT_TARGETTABLE(oFuelCan[7],FALSE)
							SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[7],FALSE,0)
							FREEZE_ENTITY_POSITION(oFuelCan[7],FALSE)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(oFuelCan[7], FALSE)
							SET_ENTITY_CAN_BE_DAMAGED(oFuelCan[7],TRUE)
							ADD_EXPLOSION(GET_ENTITY_COORDS(oFuelCan[14], FALSE),EXP_TAG_PLANE, 2.0,FALSE,FALSE,0)
							SET_ENTITY_HEALTH(oFuelCan[7],0)
						ENDIF

						IF NOT IS_PED_INJURED(gangEnemy[38].ped)
							SET_PED_CAN_BE_TARGETTED(gangEnemy[38].ped,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(gangEnemy[39].ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[39].ped,TRUE)
							SET_PED_CAN_BE_TARGETTED(gangEnemy[39].ped,TRUE)
						ENDIF
						
						SET_OBJECT_TARGETTABLE(oFuelCan[14],FALSE)
						SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[14],FALSE,0)
						IF DOES_ENTITY_EXIST(oFuelCan[7])
							SET_OBJECT_TARGETTABLE(oFuelCan[7],FALSE)
							SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[7],FALSE,0)
						ENDIF
						
						
						REQUEST_PTFX_ASSET()
						PREPARE_MUSIC_EVENT("TRV1_EXPLODE")
						iTrailerRayfireStage = 4
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(oFuelCan[7])
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(oFuelCan[7],PLAYER_PED_ID())
						SET_CONTROL_SHAKE(PLAYER_CONTROL,1500,255)
						FREEZE_ENTITY_POSITION(oFuelCan[7],FALSE)
						SET_ENTITY_HEALTH(oFuelCan[7],0)
						
						IF DOES_ENTITY_EXIST(oFuelCan[14])
							FREEZE_ENTITY_POSITION(oFuelCan[14],FALSE)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(oFuelCan[14], FALSE)
							SET_ENTITY_CAN_BE_DAMAGED(oFuelCan[14],TRUE)
							ADD_EXPLOSION(GET_ENTITY_COORDS(oFuelCan[7], FALSE),EXP_TAG_PLANE, 2.0,FALSE,FALSE,0)
							SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[14],FALSE,0.5)
							SET_OBJECT_TARGETTABLE(oFuelCan[14],FALSE)
						ENDIF
						
						IF NOT IS_PED_INJURED(gangEnemy[38].ped)
							SET_PED_CAN_BE_TARGETTED(gangEnemy[38].ped,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(gangEnemy[39].ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[39].ped,TRUE)
							SET_PED_CAN_BE_TARGETTED(gangEnemy[39].ped,TRUE)
						ENDIF
						
						SET_OBJECT_TARGETTABLE(oFuelCan[7],FALSE)
						SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[7],FALSE,0)
						IF DOES_ENTITY_EXIST(oFuelCan[14])
							SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[14],FALSE,0.5)
							SET_OBJECT_TARGETTABLE(oFuelCan[14],FALSE)
						ENDIF
						REQUEST_PTFX_ASSET()
						//CANCEL_MUSIC_EVENT() 
						PREPARE_MUSIC_EVENT("TRV1_EXPLODE")
						iTrailerRayfireStage = 4
						//PROP_CONTAINER_03B
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 4
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(trailerRayfire)
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(trailerRayfire, RFMO_STATE_PRIMING)
					REQUEST_PTFX_ASSET()
					iTrailerRayfireStage++
				ELSE
					trailerRayfire = GET_RAYFIRE_MAP_OBJECT(<< 36.9364, 3730.8999, 38.6028 >>, 10, "DES_methtrailer")
				ENDIF
			BREAK
			
			CASE 5     
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(trailerRayfire)
					RAY_FIRE_MAP_OBJECT_STATE rayfireState
					rayfireState = GET_STATE_OF_RAYFIRE_MAP_OBJECT(trailerRayfire)
					IF rayfireState = RFMO_STATE_PRIMED
						PREPARE_MUSIC_EVENT("TRV1_EXPLODE")
						IF TRIGGER_MUSIC_EVENT("TRV1_EXPLODE")
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(trailerRayfire, RFMO_STATE_START_ANIM)
							//IF NOT IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
								PLAY_STREAM_FROM_POSITION(<<30.9258, 3735.7358, 39.6296>>)
							//ENDIF
							iExplosionTime = GET_GAME_TIMER()
							//IF NOT IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
								SET_TIME_SCALE(0.2)
							// ENDIF
							fSlomoRamp = 0.2
							iTrailerRayfireStage++ 
						ENDIF
					ELIF rayfireState <> RFMO_STATE_PRIMING
						iTrailerRayfireStage = 0
					ENDIF
				ELSE
					iTrailerRayfireStage = 0
				ENDIF
			BREAK
			
			CASE 6
				IF MANAGE_MY_TIMER(iExplosionTime,120)
					ADD_EXPLOSION(<<37.73, 3734.61, 39.77>>,EXP_TAG_PLANE, 2.0,FALSE,FALSE,0)
					ADD_EXPLOSION(<< 31.0599, 3736.0564, 39.6304 >>,EXP_TAG_BARREL, 2.0,FALSE,FALSE,0)
					REPLAY_RECORD_BACK_FOR_TIME(2)
					IF NOT IS_ENTITY_DEAD(gangEnemy[38].ped)
						PRINTSTRING("BOOM!1")
						//CLEAR_PED_TASKS_IMMEDIATELY(gangEnemy[38].ped)
						SET_PED_TO_RAGDOLL(gangEnemy[38].ped, 5000, 5000, TASK_NM_BALANCE, FALSE, FALSE)
						APPLY_FORCE_TO_ENTITY(gangEnemy[38].ped, APPLY_TYPE_IMPULSE, <<GET_RANDOM_FLOAT_IN_RANGE(-3, 3), GET_RANDOM_FLOAT_IN_RANGE(2,3), 6>>, <<0,0,0>>, 0, TRUE, TRUE, TRUE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(gangEnemy[39].ped)
						PRINTSTRING("BOOM!2")
						//CLEAR_PED_TASKS_IMMEDIATELY(gangEnemy[39].ped)
						SET_PED_TO_RAGDOLL(gangEnemy[39].ped, 5000, 5000, TASK_NM_BALANCE, FALSE, FALSE)
						APPLY_FORCE_TO_ENTITY(gangEnemy[39].ped, APPLY_TYPE_IMPULSE, <<GET_RANDOM_FLOAT_IN_RANGE(-3, 3), GET_RANDOM_FLOAT_IN_RANGE(2,3), 5>>, <<0,0,0>>, 0, TRUE, TRUE, TRUE)
					ENDIF

					iTrailerRayfireStage++ 
					SPECIAL_ABILITY_CHARGE_MEDIUM(PLAYER_ID(),TRUE,TRUE)
					SET_CONTROL_SHAKE(PLAYER_CONTROL,1500,255)
				ENDIF
			BREAK
			
			CASE 7
				
				IF MANAGE_MY_TIMER(iExplosionTime,300)
				OR NOT IS_POINT_VISIBLE_TO_PLAYER(<< 30.8884, 3733.9258, 39.6283 >>,1)
					IF NOT IS_ENTITY_DEAD(gangEnemy[38].ped)
						SET_ENTITY_HEALTH(gangEnemy[38].ped,1)
					ENDIF
					IF NOT IS_ENTITY_DEAD(gangEnemy[39].ped)
						SET_ENTITY_HEALTH(gangEnemy[39].ped,1)
					ENDIF
				ENDIF
				
				IF MANAGE_MY_TIMER(iExplosionTime,1000)
				OR NOT IS_POINT_VISIBLE_TO_PLAYER(<< 30.8884, 3733.9258, 39.6283 >>,1)
					
					iTrailerRayfireStage++ 
				ENDIF
			BREAK
			
			CASE 8
				IF fSlomoRamp < 1.0
				OR NOT IS_POINT_VISIBLE_TO_PLAYER(<< 30.8884, 3733.9258, 39.6283 >>,1)
					//fSlomoRamp += 0.15
					//SET_TIME_SCALE(fSlomoRamp)
					SET_TIME_SCALE(1.0)
					iTrailerRayfireStage++ 
				ENDIF
			BREAK
			
			CASE 9
				DEACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_T1_RAYFIRE_EXPLOSION")
				IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_RAYFIRE")
					STOP_AUDIO_SCENE("TREVOR_1_RAYFIRE")
				ELSE
					IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_1_SHOOTOUT_START")
						START_AUDIO_SCENE("TREVOR_1_SHOOTOUT_START")
					ELSE
						iTrailerRayfireStage++ 
					ENDIF
				ENDIF
			BREAK
			
			CASE 10
				IF HAS_PTFX_ASSET_LOADED()
					//START_PARTICLE_FX_NON_LOOPED_AT_COORD("ent_ray_meth_dust_settle",<<31.6185, 3739.0620, 42.6984>>,<<217, 0, 0>>)
					SET_TIME_SCALE(1.0)
					iTrailerRayfireStage++ 
				ENDIF
			
			BREAK
      ENDSWITCH
ENDPROC

FUNC PED_INDEX GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PED_INDEX PedIndex, REL_GROUP_HASH rgHash, INT iProximity = 0, BOOL bOnScreen = FALSE) 

  PED_INDEX pedArray[16]
  
  INT i = 0
        
  IF NOT IS_PED_INJURED(PedIndex)
  
    GET_PED_NEARBY_PEDS(PedIndex, pedArray)
    FOR i = 0 TO (COUNT_OF(pedArray) - 1)
    	IF DOES_ENTITY_EXIST(pedArray[i])
        	IF NOT IS_PED_INJURED(pedArray[i])
            	IF (GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i]) = rgHash)
                    IF (iProximity <= 0)
                        IF ( bOnScreen = TRUE )
                            IF IS_ENTITY_ON_SCREEN(pedArray[i])
                           		RETURN pedArray[i]
                            ENDIF
                        ELSE
                      		RETURN pedArray[i]
                        ENDIF
                    ELSE
                    	IF (i + iProximity <= COUNT_OF(pedArray) - 1 )
                 			IF DOES_ENTITY_EXIST(pedArray[i + iProximity])
                                IF NOT IS_PED_INJURED(pedArray[i + iProximity])
                                    IF (GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i + iProximity]) = rgHash )
                                        IF (bOnScreen = TRUE)
                                            IF IS_ENTITY_ON_SCREEN(pedArray[i + iProximity])
                                            	RETURN pedArray[i + iProximity]
                                            ENDIF
       										RETURN pedArray[i + iProximity]       
                                        ENDIF                                               
                                    ENDIF
                                ENDIF
							ENDIF
                        ENDIF
                    ENDIF                                    
              	ENDIF
        	ENDIF
  		ENDIF
	ENDFOR
    
  ENDIF
  
  RETURN NULL

ENDFUNC


PROC MANAGE_BIKER_BATTLE_DIALOGUE()
	
	PED_INDEX pedBikerSpeak = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(),grpEnemies,GET_RANDOM_INT_IN_RANGE(0,3))
	
	INT iRand
	
	IF DOES_ENTITY_EXIST(pedBikerSpeak)
		IF NOT IS_PED_INJURED(pedBikerSpeak)
			IF IS_PED_MODEL(pedBikerSpeak,G_M_Y_LOST_01)
				IF MANAGE_MY_TIMER(iAmbientDialogueTimer, 9000)
				OR bJustArrived = FALSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),GET_ENTITY_COORDS(pedBikerSpeak, FALSE)) < 20
					OR bJustArrived = FALSE	
						iRand = GET_RANDOM_INT_IN_RANGE(0,2)
						IF iRand = 0
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBikerSpeak,"T1M1_EJAA","LOST1",SPEECH_PARAMS_FORCE_SHOUTED)
							iAmbientDialogueTimer = GET_GAME_TIMER()
							bJustArrived = TRUE							
						ENDIF
						IF iRand = 1
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBikerSpeak,"T1M1_EKAA","LOST2",SPEECH_PARAMS_FORCE_SHOUTED)
							iAmbientDialogueTimer = GET_GAME_TIMER()
							bJustArrived = TRUE		
						ENDIF
						IF iRand = 2
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBikerSpeak,"T1M1_ELAA","LOST3",SPEECH_PARAMS_FORCE_SHOUTED)
							iAmbientDialogueTimer = GET_GAME_TIMER()
							bJustArrived = TRUE		
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF


ENDPROC

PROC UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(GANG_MOVE_ATTACK_T &array[],BOOL bForceOn = FALSE)

	INT i
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		UPDATE_AI_PED_BLIP(array[i].ped, array[i].EnemyBlipData,-1,NULL,bForceOn)
	ENDFOR

ENDPROC

PROC MANAGE_PICKUP()

	IF bCreatePickUps = FALSE
		IF NOT DOES_PICKUP_EXIST(PickupHealth1)
			PickupHealth1 = CREATE_PICKUP_ROTATE(PICKUP_ARMOUR_STANDARD, <<30.45, 3669.81, 40.31>>, <<90.41, 0.56, -52.25>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupHealth2)
			PickupHealth2 = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<66.33, 3711.64, 39.45>>, <<-0.22, 0.33, -58.33>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupHealth3)
			PickupHealth3 = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD,  <<14.63, 3719.88, 39.53>>, <<1.52, 2.15, 60.76>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupHealth4)
			PickupHealth4 = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD,  <<46.52, 3683.28, 39.89>>, <<90.70, 0.05, 48.06>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupHealth5)
			PickupHealth5 = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD,  <<66.98, 3686.43, 39.12>>, <<178.74, -0.08, 10.16>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupWeapon1)
			PickupWeapon1 = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_ASSAULTRIFLE, <<30.81, 3669.59, 40.26>>, <<94.06, -0.32, 79.76>>, enum_to_int(PLACEMENT_FLAG_MAP),100)
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupWeapon2)
			PickupWeapon2 = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_ASSAULTRIFLE, <<56.63, 3690.69, 38.95>>, <<-85.78, 0.05, 90.26>>, enum_to_int(PLACEMENT_FLAG_MAP),100)
		ENDIF
		
		//GRENADE
		IF NOT DOES_PICKUP_EXIST(PickupWallet)
			PickupWallet = CREATE_PICKUP_ROTATE(PICKUP_MONEY_WALLET, <<57.78, 3690.51, 38.94>>, <<-92.04, 1.40, -6.62>>, enum_to_int(PLACEMENT_FLAG_MAP),3)
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupMoney1)
			PickupMoney1 = CREATE_PICKUP_ROTATE(PICKUP_MONEY_WALLET, <<31.08, 3670.03, 40.25>>, <<-92.28, -1.83, -100.58>>, enum_to_int(PLACEMENT_FLAG_MAP),42)
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupMoney2)
			PickupMoney2 = CREATE_PICKUP_ROTATE(PICKUP_MONEY_PAPER_BAG, <<10.93, 3735.69, 39.66>>, <<1.37, 0.19, 0.28>>, enum_to_int(PLACEMENT_FLAG_MAP),314)
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupBat)
			PickupBat = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_BAT, <<72.28, 3703.08, 39.73>>, <<90.25, 2.47, 155.49>>, enum_to_int(PLACEMENT_FLAG_MAP))
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(PickupAK)
			PickupAK = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_ASSAULTRIFLE, <<63.90, 3682.91, 38.86>>, <<-89.24, 0.48, -1.06>>, enum_to_int(PLACEMENT_FLAG_MAP),50)
		ENDIF
		bCreatePickUps = TRUE
	ENDIF

ENDPROC

VECTOR vPetrolCentre = <<52.16, 3661.33, 38.77>>
VECTOR vTrailCentre =  <<52.16, 3661.33, 38.77>>
//VECTOR vFire

PROC MANAGE_PETROL()
	SWITCH iPetrolStage
		CASE 0
//			SET_VFX_IGNORE_CAM_DIST_CHECK(TRUE)
			IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
				SET_ENTITY_PROOFS(vehTruck.veh, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_ENTITY_CAN_BE_DAMAGED(vehTruck.veh,TRUE)
				SET_VEHICLE_PETROL_TANK_HEALTH(vehTruck.veh,499)
				SET_VEHICLE_CAN_LEAK_PETROL(vehTruck.veh,TRUE)
				SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(vehTruck.veh,FALSE)
			ENDIF
			ADD_PETROL_DECAL(vPetrolCentre + <<0,0,0>> ,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 1
			ADD_PETROL_DECAL(vPetrolCentre + <<0,0.5,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 2
			ADD_PETROL_DECAL(vPetrolCentre + <<0.5,0.5,0>> ,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 3
			ADD_PETROL_DECAL(vPetrolCentre + <<0.5,0.0,0>> ,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 4
			ADD_PETROL_DECAL(vPetrolCentre + <<0.0,-0.5,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 5
			ADD_PETROL_DECAL(vPetrolCentre + <<-0.5,-0.5,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 6
			ADD_PETROL_DECAL(vPetrolCentre + <<-0.5,0.0,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 7
			ADD_PETROL_DECAL(vPetrolCentre + <<-0.5,0.5,0>>,1.5,2,1)
			iPetrolStage ++
		BREAK
		
		CASE 8
			START_PETROL_TRAIL_DECALS(1)
			ADD_PETROL_TRAIL_DECAL_INFO(vTrailCentre ,1)
			iPetrolStage ++
		BREAK
		
		CASE 9
			ADD_PETROL_TRAIL_DECAL_INFO(<<52.47, 3660.36, 38.81>> ,0.5)
			iPetrolStage ++
		BREAK
		
		CASE 10
			ADD_PETROL_TRAIL_DECAL_INFO(<<52.80, 3659.22, 38.72>> ,0.5)
			iPetrolStage ++
		BREAK
		
		CASE 11
			ADD_PETROL_TRAIL_DECAL_INFO(<<53.16, 3658.02, 38.70>> ,0.5)
			iPetrolStage ++
		BREAK
		
		CASE 12
			ADD_PETROL_TRAIL_DECAL_INFO(<<53.62, 3656.85, 38.69>> ,0.5)
			iPetrolStage ++
		BREAK
		
		
		CASE 13
			ADD_PETROL_TRAIL_DECAL_INFO(<<54.16, 3655.95, 38.69>> ,0.5)
			iPetrolStage ++
		BREAK
		
		CASE 14
			ADD_PETROL_TRAIL_DECAL_INFO(<<54.76, 3655.08, 38.70>> ,0.5)
			iPetrolStage ++
		BREAK
		
		CASE 15
			ADD_PETROL_TRAIL_DECAL_INFO(<<55.44, 3654.26, 38.69>> ,0.5)
			iPetrolStage ++
		BREAK
		
		CASE 16
			ADD_PETROL_TRAIL_DECAL_INFO(<<56.21, 3653.11, 38.69>> ,0.5)
			iPetrolStage ++
		BREAK
		
		CASE 17
			ADD_PETROL_TRAIL_DECAL_INFO(<<57.01, 3651.88, 38.70>> ,0.5)
			iPetrolStage ++
		BREAK
		
		CASE 18
			ADD_PETROL_TRAIL_DECAL_INFO(<<57.9966, 3650.1963, 38.6798>> ,0.5)
//			SET_VFX_IGNORE_CAM_DIST_CHECK(FALSE)
			END_PETROL_TRAIL_DECALS()
			PRINTSTRING("Petrol Trail Created")PRINTNL()
			iPetrolStage ++
		BREAK
		
		CASE 19
			
			IF DOES_ENTITY_EXIST(vehTruck.veh)
				IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
					IF GET_DISTANCE_BETWEEN_ENTITIES(vehTruck.veh,PLAYER_PED_ID()) > 12
						IF IS_ENTITY_ON_FIRE(vehTruck.veh)
							PRINTSTRING("vehTruck BOOM!")PRINTNL()
							EXPLODE_VEHICLE(vehTruck.veh)
							iPetrolStage ++
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTSTRING("vehTruck DOESNT EXIST")PRINTNL()
			ENDIF
		BREAK
		
		
	ENDSWITCH
ENDPROC

PROC MANAGE_ENEMY_BEHAVIOR()


	IF NOT IS_PED_INJURED(gangEnemy[7].ped)
		IF gangEnemy[7].bTriggered = TRUE
			IF gangEnemy[7].bReturnToAI = FALSE
				IF IS_ENTITY_VISIBLE(gangEnemy[7].ped)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(gangEnemy[7].ped)
					OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[7].ped) > 120
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[7].ped,FALSE)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[7].ped,GET_ENTITY_COORDS(gangEnemy[7].ped, FALSE),20)
						gangEnemy[7].bReturnToAI = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(gangEnemy[10].ped)
		IF gangEnemy[10].bTriggered = TRUE
			IF gangEnemy[10].bReturnToAI = FALSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(gangEnemy[10].ped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[10].ped,FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[10].ped,GET_ENTITY_COORDS(gangEnemy[10].ped, FALSE),20)
					gangEnemy[10].bReturnToAI = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

PROC FORCE_BUDDY_TASKS()
	IF bSetupBuddies
		IF NOT IS_PED_INJURED(budRon.ped)
			IF NOT IS_PED_IN_ANY_VEHICLE(budRon.ped)
				IF GET_SCRIPT_TASK_STATUS(budRon.ped,SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)<> PERFORMING_TASK
					TASK_COMBAT_HATED_TARGETS_IN_AREA(budRon.ped,GET_ENTITY_COORDS(budRon.ped, FALSE),100)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(budWade.ped)
			IF NOT IS_PED_IN_ANY_VEHICLE(budWade.ped)
				IF GET_SCRIPT_TASK_STATUS(budWade.ped,SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)<> PERFORMING_TASK
					TASK_COMBAT_HATED_TARGETS_IN_AREA(budWade.ped,GET_ENTITY_COORDS(budWade.ped, FALSE),100)
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

PROC MANAGE_TREVORS_ABILITY_EFFECTS()
	
	IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
		PRINTSTRING("Ability On")PRINTNL()
		IF bNeedToResetEffects = FALSE
			SET_PARTICLE_FX_BLOOD_SCALE(3.0)
			SET_PARTICLE_FX_BULLET_IMPACT_SCALE(3.0)
			PRINTSTRING("Effects on")PRINTNL()
			bNeedToResetEffects = TRUE
		ENDIF
	ELSE
		IF bNeedToResetEffects = TRUE
			SET_PARTICLE_FX_BLOOD_SCALE(1.0)
			SET_PARTICLE_FX_BULLET_IMPACT_SCALE(1.0)
			PRINTSTRING("Effects off")PRINTNL()
			bNeedToResetEffects = FALSE
		ENDIF
	ENDIF

ENDPROC

BOOl T1M1_GYAA
INT iBuddyBattleDialogueTimer

PROC MANAGE_BUDDY_DIALOGUE()
	
	IF T1M1_GYAA = FALSE
		IF GET_RANDOM_BOOL()
			IF NOT IS_PED_INJURED(budRon.ped)
				IF NOT IS_PED_INJURED(budWade.ped)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_AMBIENT_SPEECH_PLAYING(budRon.ped)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(budWade.ped,"T1M1_GYAA","WADE",SPEECH_PARAMS_SHOUTED)
						iBuddyBattleDialogueTimer = GET_GAME_TIMER()
						T1M1_GYAA = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(budWade.ped)
				IF NOT IS_PED_INJURED(budRon.ped)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_AMBIENT_SPEECH_PLAYING(budWade.ped)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(budRon.ped,"T1M1_GZAA","RON",SPEECH_PARAMS_SHOUTED)
						iBuddyBattleDialogueTimer = GET_GAME_TIMER()
						T1M1_GYAA = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF MANAGE_MY_TIMER(iBuddyBattleDialogueTimer,GET_RANDOM_INT_IN_RANGE(8000,18000))
			T1M1_GYAA = FALSE
		ENDIF
	ENDIF

ENDPROC

INT iStartFightTimer
BOOL bAllowBuddiesToShoot = FALSE

//PURPOSE: Controls the big fight at the trailer park
FUNC BOOL stageCaravanParkFightV2()

	IF bAllowBuddiesToShoot = FALSE
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(budRon.ped)
				SET_PED_CONFIG_FLAG(budRon.ped,PCF_ForcedToStayInCover,FALSE)
			ENDIF
			IF NOT IS_PED_INJURED(budWade.ped)
				SET_PED_CONFIG_FLAG(budWade.ped,PCF_ForcedToStayInCover,FALSE)
			ENDIF
			bAllowBuddiesToShoot = TRUE
		ENDIF
	ENDIF
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING) 
	
	//AUDIO SCENES
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_CHASE_KILL_BIKERS")
		STOP_AUDIO_SCENE("TREVOR_1_CHASE_KILL_BIKERS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_CHASE_FOLLOW_VAN")
		STOP_AUDIO_SCENE("TREVOR_1_CHASE_FOLLOW_VAN")
	ENDIF
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_1_CHASE_KILL_BIKERS")
	AND NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_1_CHASE_FOLLOW_VAN")
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_1_SHOOTOUT_START")
			START_AUDIO_SCENE("TREVOR_1_SHOOTOUT_START")
			SET_LABEL_AS_TRIGGERED("TREVOR_1_SHOOTOUT_START",TRUE)
		ENDIF
	ENDIF

	//SETUP_MISSION_REQUIREMENT(REQ_TRAILER_OIL_DRUM,<<0,0,0>>)
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_AT_CARAVAN")
		IF NOT IS_PED_INJURED(budRon.ped)
		AND NOT IS_PED_INJURED(budWade.ped)
			IF NOT IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh,TRUE)
			AND NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh,TRUE)
				PREPARE_MUSIC_EVENT("TRV1_AT_CARAVAN")
				IF TRIGGER_MUSIC_EVENT("TRV1_AT_CARAVAN")
					SET_LABEL_AS_TRIGGERED("TRV1_AT_CARAVAN",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// DISTANCE FAILS
	IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<72.506767,3597.105713,58.682758>>, <<75.939247,3772.750000,25.818333>>, 170.000000)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<67.07, 3703.85, 39.56>>) > 125
			IF NOT DOES_BLIP_EXIST(blipDest)
				KILL_FACE_TO_FACE_CONVERSATION()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					CLEAR_PRINTS()
					PRINT_GOD_TEXT("TRV_RETTP")
					blipDest = CREATE_BLIP_FOR_COORD(<<67.07, 3703.85, 39.56>>)
				ENDIF
			ENDIF
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<67.07, 3703.85, 39.56>>) > 150
				MissionFailed(FAIL_LEFT_TRAILER_PARK)			
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipDest)
			REMOVE_BLIP(blipDest)
		ENDIF
		IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_RETTP")
			CLEAR_PRINTS()
		ENDIF
	ENDIF

	IF IS_PED_INJURED(cmbtChase[3].ped)
		IF DOES_BLIP_EXIST(cmbtChase[3].blip)
			REMOVE_BLIP(cmbtChase[3].blip)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(cmbtChase[3].ped)
		SET_MODEL_AS_NO_LONGER_NEEDED(cmbtChase[3].model)
	ENDIF
	
	IF IS_PED_INJURED(cmbtChase[2].ped)
		IF DOES_BLIP_EXIST(cmbtChase[2].blip)
			REMOVE_BLIP(cmbtChase[2].blip)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(cmbtChase[2].ped)
		SET_MODEL_AS_NO_LONGER_NEEDED(cmbtChase[2].model)
	ENDIF
	
	//Grab the closest ped - to shout abuse
	PED_INDEX pedClosest
	IF GET_GAME_TIMER() - iClosestVehicleCheckTimer > 200
		GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 10.0, FALSE,TRUE,pedClosest)
		iClosestVehicleCheckTimer = GET_GAME_TIMER()
	ENDIF

	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(gangEnemy)
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(gangDog)

	MANAGE_TREVOR_RAGE()
	MANAGE_RAYFIRE_EXPLOSION()
	MANAGE_BLOW_UP_VEHICLE()
	MANAGE_BIKER_BEHAVIOR()
	MANAGE_BIKER_BATTLE_DIALOGUE()
	BATTLE_DIALOGUE()
	MANAGE_PICKUP()
	IF bStageSetup
		MANAGE_PETROL()
	ENDIF
	FORCE_BUDDY_TASKS()
	//MANAGE_TREVORS_ABILITY_EFFECTS()
	MANAGE_BUDDY_DIALOGUE()
	
	IF bSetupBuddies = TRUE
		IF MANAGE_MY_TIMER(iStartFightTimer,60000)
			MANAGE_ENEMY_BEHAVIOR()
		ENDIF
	ENDIF
	
	//MANAGE_CRACK_WHORES()
	
	IF bSetUpStageData = FALSE
		SETUP_PEDS_FOR_DIALOGUE()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Caravan Park Fight")
		SET_VEHICLE_MODEL_IS_SUPPRESSED(HEXER,TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GBURRITO,TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_01,TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_02,TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_03,TRUE)
		T1M1_FIG2 = FALSE
		bSetUpStageData = TRUE
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
	INT i
	SEQUENCE_INDEX seq
	MODEL_NAMES mFuelCan = PROP_GASCYL_01A
	GANG_MOVE_ATTACK_T gangTemp[10]
	//BOOL bMovedTempGang[10]
	//INT iListOfGangIndexes[10]
	
	BOOL bVanFinshed = FALSE
	IF NOT IS_ENTITY_DEAD(vehTruck.veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
			UPDATE_UBER_PLAYBACK(vehTruck.veh, 1.0)
		ELSE
			bVanFinshed = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
			IF bVanFinshed
				SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0].veh)
			ELSE
				SET_PLAYBACK_SPEED(vehBike[0].veh, 1.0)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
			IF bVanFinshed
				SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1].veh)
			ELSE
				SET_PLAYBACK_SPEED(vehBike[1].veh, 1.0)
			ENDIF
		ENDIF
	ENDIF
//---------¦ SETUP ¦---------
	IF NOT bStageSetup	
		REQUEST_MODEL(mFuelCan)
		REQUEST_MODEL(Prop_GasCyl_02A)
		REQUEST_MODEL(Asset.mnLost1)
		REQUEST_MODEL(Asset.mnLost2)
		IF HAS_MODEL_LOADED(mFuelCan)
		AND HAS_MODEL_LOADED(Prop_GasCyl_02A)
		AND HAS_MODEL_LOADED(Asset.mnLost1)
		AND HAS_MODEL_LOADED(Asset.mnLost2)
			REQUEST_MODEL(Asset.mnGangVan)
			REQUEST_MODEL(Asset.mnBike)
			REQUEST_MODEL(Asset.mnGangTruck)
			REQUEST_VEHICLE_RECORDING(1, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(9, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(11, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(12, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(13, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(14, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(15, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(16, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(17, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(18, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(19, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(20, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(21, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(22, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(23, "Trevor1dw")
			REQUEST_VEHICLE_RECORDING(2, "t1b")
			REQUEST_VEHICLE_RECORDING(3, "t1b")
			REQUEST_VEHICLE_RECORDING(4, "t1b")
			REQUEST_VEHICLE_RECORDING(5, "t1b")
			REQUEST_VEHICLE_RECORDING(6, "t1b")
			REQUEST_VEHICLE_RECORDING(7, "t1b")
			REQUEST_VEHICLE_RECORDING(8, "t1b")
			REQUEST_VEHICLE_RECORDING(9, "t1b")
			REQUEST_VEHICLE_RECORDING(10, "t1b")
			IF HAS_MODEL_LOADED(Asset.mnGangVan)
			AND HAS_MODEL_LOADED(Asset.mnBike)
			AND HAS_MODEL_LOADED(Asset.mnGangTruck)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(9, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(11, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(12, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(13, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(14, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(15, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(16, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(17, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(18, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(19, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(20, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(21, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(22, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(23, "Trevor1dw")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2,"t1b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(3,"t1b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(4,"t1b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(5,"t1b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(6,"t1b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(7,"t1b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(8,"t1b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(9,"t1b")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(10,"t1b")
				KILL_FACE_TO_FACE_CONVERSATION()
				//CLEAR_PRINTS()
				
				RESET_NEED_LIST()
				SET_MODEL_AS_NEEDED(Asset.mnLost1)
				SET_MODEL_AS_NEEDED(Asset.mnLost2)
				SET_MODEL_AS_NEEDED(Asset.mnGangVan)
				SET_MODEL_AS_NEEDED(Asset.mnBike)
				SET_MODEL_AS_NEEDED(Asset.mnGangTruck)
			
				//MANAGE_LOADING_NEED_LIST()
				
				//For the Rage Bar. Load the scale form thing
				//SAFE_REQUEST_SCALEFORM_MOVIE(sfRageBar, "vertical_bar", TRUE, TRUE)
				
				SET_START_AND_END_VECTORS()
				INITALISE_ARRAYS(STAGE_CARAVAN_PARK_FIGHT)

				iCounter = 0
				IF NOT DOES_ENTITY_EXIST(vehTrailerPark[4].veh)
					WHILE iCounter < MAX_TRAILER_PARK_VEH
						//AND iCounter != 11
							IF CREATE_VEHICLE_STRUCT(vehTrailerPark[iCounter])
							//	SET_VEHICLE_DOORS_LOCKED(vehTrailerPark[iCounter].veh,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
								#IF IS_DEBUG_BUILD
									TEXT_LABEL tVeh = "Veh "
									tVeh += iCounter
									SET_VEHICLE_NAME_DEBUG(vehTrailerPark[iCounter].veh, tVeh)
								#ENDIF
								IF iCounter = 10
									DELETE_VEHICLE(vehTrailerPark[iCounter].veh)
									//SET_VEHICLE_COLOURS(vehTrailerPark[iCounter].veh,0,0)
								ENDIF
								
								IF iCounter = 11
									DELETE_VEHICLE(vehTrailerPark[iCounter].veh)
									//SET_VEHICLE_COLOURS(vehTrailerPark[iCounter].veh,62,62)
								ENDIF
								
								IF iCounter = 12
									SET_VEHICLE_COLOURS(vehTrailerPark[iCounter].veh,10,10)
								ENDIF
								
								IF iCounter = 13
									SET_VEHICLE_COLOURS(vehTrailerPark[iCounter].veh,4,4)
								ENDIF
								
								IF iCounter = 23
									SET_VEHICLE_COLOURS(vehTrailerPark[iCounter].veh,13,13)
								ENDIF
								
								IF iCounter = 19
									SET_VEHICLE_COLOURS(vehTrailerPark[iCounter].veh,13,13)
								ENDIF
								
								iCounter++
							ELSE
								safeWait(0)
							ENDIF
					ENDWHILE
					
					STOP_PLAYER_CAR(FALSE)
				ENDIF
				iCounter = 0
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[4].veh)
					//--Bike that flees near trailer with interior
					//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrailerPark[4].veh,FALSE)
					//SET_VEHICLE_DOORS_LOCKED(vehTrailerPark[4].veh,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTrailerPark[4].veh, TRUE)
					START_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[4].veh,1, "trevor1dw")
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[4].veh)
				ENDIF
				
				//PLAYTHROUGH XXXX
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<51.19, 3735.08, 39.24>>) > 20
						//--Pickup that drives towards player
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrailerPark[12].veh,FALSE)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTrailerPark[12].veh, TRUE)
						//START_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh,2, "trevor1dw")
						START_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh,10, "t1b")
						PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTrailerPark[12].veh)
						SET_ENTITY_HEALTH(vehTrailerPark[12].veh, 300)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[5].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[5].veh)
						//-- Bike by the trailer
						DELETE_VEHICLE(vehTrailerPark[5].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[6].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[6].veh)
						//-- In front of stage
						DELETE_VEHICLE(vehTrailerPark[6].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[7].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[7].veh)
						//-- In front of stage
						DELETE_VEHICLE(vehTrailerPark[7].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[15].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[15].veh)
						//--Convoy of lhs bikes
						DELETE_VEHICLE(vehTrailerPark[15].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[16].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[16].veh)
						//--Convoy of lhs bikes
						DELETE_VEHICLE(vehTrailerPark[16].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[17].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[17].veh)
						//--Convoy of lhs bikes
						DELETE_VEHICLE(vehTrailerPark[17].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[20].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[20].veh)
						//--Convoy of rhs bikes
						DELETE_VEHICLE(vehTrailerPark[20].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[21].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[21].veh)
						//--Convoy of rhs bikes
						DELETE_VEHICLE(vehTrailerPark[21].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[22].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[22].veh)
						//--Convoy of rhs bikes
						DELETE_VEHICLE(vehTrailerPark[22].veh)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[24].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[24].veh)
						//--Convoy 2 of rhs bikes
						DELETE_VEHICLE(vehTrailerPark[24].veh)
					ENDIF
					
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[25].veh)
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[25].veh)
						//--Convoy 2 of rhs bikes
						DELETE_VEHICLE(vehTrailerPark[25].veh)
					ENDIF
				ENDIF
				
				REPEAT COUNT_OF (oFuelCan) iCounter
					IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
						DELETE_OBJECT(oFuelCan[iCounter])
					ENDIF
				ENDREPEAT

				IF DOES_BLIP_EXIST(vehTruck.blip)
					REMOVE_BLIP(vehTruck.blip)
				ENDIF
				IF DOES_BLIP_EXIST(vehBike[0].blip)
					REMOVE_BLIP(vehBike[0].blip)
				ENDIF
				IF DOES_BLIP_EXIST(vehBike[1].blip)
					REMOVE_BLIP(vehBike[1].blip)
				ENDIF
				
			//	CREATE_TRAILER_ENEMIES()
				
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				
				ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
				//bHasSpawnedWave = FALSE
				iEnemyCreationProc = 0
			//	QUEUE_GOD_TEXT_SP(Asset.godFightStart)
			///	QUEUE_SPEECH_SP(Asset.convFightStart)
				
				vFuelCanLoc[0] = << 61.00, 3669.80, 39.16 >>
				vFuelCanLoc[1] = << 33.31, 3668.32, 39.11 >>
				vFuelCanLoc[2] =   << 39.02, 3688.35, 38.94 >> 
				vFuelCanLoc[3] =  << 63.43, 3685.22, 39.22 >>
				vFuelCanLoc[4] =   << 93.32, 3717.06, 39.01 >>
				vFuelCanLoc[5] = << 104.20, 3730.56, 38.93 >> 
				vFuelCanLoc[6] = << 91.72, 3684.61, 38.94 >>  

				vFuelCanLoc[8] = << 80.07, 3689.77, 39.00 >> 
				vFuelCanLoc[9] = << 79.87, 3690.17, 38.89 >>
				vFuelCanLoc[10] = << 66.32, 3657.64, 39.09 >>
				vFuelCanLoc[11] = << 71.49, 3746.65, 39.13 >>
				vFuelCanLoc[12] =  << 66.98, 3691.36, 39.17 >>
				vFuelCanLoc[13] = << 95.60, 3745.61, 39.17 >>
				//By trailer
				vFuelCanLoc[7] = << 39.845531,3735.912354,38.527878 >>
				vFuelCanLoc[14] = << 35.614193,3731.956543,38.527878 >>
				
				FLOAT fZ
				iCounter = 0
				REPEAT COUNT_OF(vFuelCanLoc) iCounter
					IF NOT ARE_VECTORS_EQUAL(vFuelCanLoc[iCounter], <<0.0, 0.0, 0.0>>)
						IF iCounter = 14 OR iCounter = 7
							oFuelCan[iCounter] = CREATE_OBJECT(Prop_GasCyl_02A,vFuelCanLoc[iCounter])
							IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
								SET_OBJECT_TARGETTABLE(oFuelCan[iCounter], TRUE)
								SET_ENTITY_IS_TARGET_PRIORITY(oFuelCan[iCounter],TRUE,40)
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(oFuelCan[iCounter], TRUE)
							ENDIF
						ELSE
							GET_GROUND_Z_FOR_3D_COORD(vFuelCanLoc[iCounter],fZ)
							fZ = fZ + 0.4
							vFuelCanLoc[iCounter].z = fZ
							oFuelCan[iCounter] = CREATE_OBJECT_NO_OFFSET(mFuelCan,vFuelCanLoc[iCounter])
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(oFuelCan[iCounter], TRUE)
						ENDIF
					ENDIF
				ENDREPEAT
				
				iCounter = 0
				REPEAT COUNT_OF(iFuelDropFromVehTime) iCounter
					iFuelDropFromVehTime[iCounter] = 0
					bFuelCanDropFromVeh[iCounter] = FALSE
				ENDREPEAT
				
				bFuelCanDropFromVeh[12] = TRUE
				bFuelCanDropFromVeh[13] = TRUE
				bFuelCanDropFromVeh[18] = TRUE
				bFuelCanDropFromVeh[19] = TRUE
			//	bFuelCanDropFromVeh[23] = TRUE
				
				SET_RADAR_ZOOM(1)
				REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
				
				IF NOT IS_ENTITY_DEAD(cmbtChase[0].ped)
					//--Van driver
					SET_ENTITY_INVINCIBLE(cmbtChase[0].ped, FALSE)
					gangEnemy[2].ped = cmbtChase[0].ped
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(cmbtChase[1].ped)
					//--Van passenger
					SET_ENTITY_INVINCIBLE(cmbtChase[1].ped, FALSE)
					gangEnemy[3].ped = cmbtChase[1].ped
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(cmbtChase[2].ped)
					//--LHS biker
					gangEnemy[0].ped = cmbtChase[2].ped
					IF DOES_BLIP_EXIST(cmbtChase[2].blip)
						REMOVE_BLIP(cmbtChase[2].blip)
					ENDIF
				ELSE
				//	SCRIPT_ASSERT("LHS Biker is dead!")
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(cmbtChase[3].ped)
					//--LHS biker
					gangEnemy[1].ped = cmbtChase[3].ped
					IF DOES_BLIP_EXIST(cmbtChase[3].blip)
						REMOVE_BLIP(cmbtChase[3].blip)
					ENDIF
				ELSE
				//	SCRIPT_ASSERT("RHS Biker is dead!")
				ENDIF

				IF eMissionStage < STAGE_CARAVAN_SMASH
					SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_NORMAL)
				ELSE
					SET_BUILDING_STATE(BUILDINGNAME_IPL_METH_TRAILER, BUILDINGSTATE_CLEANUP)
				ENDIF
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_A, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_B, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_C, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_D, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_E, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_TRAILERPARK_BLOCKING_AREA, BUILDINGSTATE_NORMAL)
				
				IF IS_SCREEN_FADED_OUT()
					ADD_SCENARIO_BLOCKING_AREA(vTrailerPark + <<100,100,100>>, vTrailerPark - <<100,100,100>>)
					CLEAR_AREA(vTrailerPark,500,TRUE,FALSE)
				ENDIF
				
				IF NOT IS_PED_INJURED(budRon.ped)
					ADD_PED_FOR_DIALOGUE(structConvoPeds, 3, budRon.ped, "NervousRon")
				ENDIF
				
				IF NOT IS_PED_INJURED(budWade.ped)
					ADD_PED_FOR_DIALOGUE(structConvoPeds, 4, budWade.ped, "Wade")
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					ADD_PED_FOR_DIALOGUE(structConvoPeds, 2, PLAYER_PED_ID(), "TREVOR")
				ENDIF
				
				//REMOVE_PTFX_ASSET()
				IF bPTX_Triggered = TRUE
					REMOVE_PARTICLE_FX(ptfx_explosion)
				ENDIF
				
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(trailerRayfire)
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(trailerRayfire, RFMO_STATE_RESET)
				ENDIF
				
				ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,FALSE)
				ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,FALSE)
				
				SETUP_THESE_PEDS(PED_GRENADE_BOSS)
				SETUP_THESE_PEDS(PED_FIGHT_LHS_CLOSE1)
				SETUP_THESE_PEDS(PED_FIGHT_INSIDE_TRAILER)
				SETUP_THESE_PEDS(PED_FIGHT_FAKE)	
				SETUP_THESE_PEDS(PED_FIGHT_BACK_LEFT_FORKLIFT)
				SETUP_THESE_PEDS(PED_FIGHT_RHS_CLOSE1)
				SETUP_THESE_PEDS(PED_FIGHT_RHS_FAR1)
				SETUP_THESE_PEDS(PED_FIGHT_RHS_VAN_REVERSE)
				SETUP_THESE_PEDS(PED_FIGHT_INSIDE_TRAILER_RHS)
				SETUP_THESE_PEDS(PED_FIGHT_MOVE_REAR_TO_CENTRE)
				
				IF bSkipped = FALSE
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
							IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PUMPSHOTGUN)
								IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
									GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 65)
								ENDIF
								SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
							ENDIF
						ELSE
							IF bGivePedWeaponOnExit = FALSE
								REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
								IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PUMPSHOTGUN)
									IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
										bGivePedWeaponOnExit = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bSkipped = TRUE
					IF IS_SCREEN_FADED_OUT()
						coverPlayer = ADD_COVER_POINT(<< 58.4031, 3630.9597, 38.7482 >>, 24.4402, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60, TRUE)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),<< 58.4031, 3630.9597, 38.7482 >>,-1,TRUE,0,TRUE,FALSE,CoverPlayer)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_AllowPlayerToInterruptVehicleEntryExit,TRUE)
						
						eTrevorsFailWeapon = GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR))
						
						IF 	( eTrevorsFailWeapon <> WEAPONTYPE_INVALID )
						AND ( eTrevorsFailWeapon <> WEAPONTYPE_UNARMED )
							REQUEST_WEAPON_ASSET(eTrevorsFailWeapon)
						ENDIF
						
						IF GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR)) != WEAPONTYPE_UNARMED AND GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR)) != WEAPONTYPE_INVALID
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR)), TRUE)
							ENDIF
						ELSE
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), TRUE)
							ENDIF
						ENDIF
						
						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>, FALSE)
						REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vTrailerPark + <<50,50,50>>, vTrailerPark - <<50,50,50>>)
						PRINTSTRING("RUNNING SKIPPED EXCLUSIVE SETUP")

						SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),TRUE,-1)
					ENDIF
				ENDIF

				REMOVE_PED_FOR_DIALOGUE(structConvoPeds,4)
				REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
			//	SETUP_THESE_PEDS(PED_FIGHT_THROUGH_MIDDLE_BIKES_LHS)
				iStageSection = 0
				iDoPedFleeOnBikeSetPieceProg = 0
				iDoFirstBikeConvoySetPieceProg = 0
				iFirstRhsBikeSetpieceProg = 0
				iVanReverseRhsProg = 0
				iDoSecRhsBikeProg = 0
				iTrailerRayfireStage = 0
				iLureGuy = 0
				iRifleGuy = 0
				iGrenadeBoss = 0
				iPetrolStage = 0
				iAgressiveCentreGuys = 0
				iBkesApproachCentreRhsProg = 0
				bSHouldRhsVanSetpieceTrigger = FALSE
				bPlayerApproachedCentreFromRhs = FALSE
				bPlayerApproachedCentreFromLhs = FALSE
				bPlayerApproachedCentreFromMiddle = FALSE
				bHaveAllTrailerPedsBeenKilled = FALSE
				bPlayerWentThroughLhsGap = FALSE
				bDoneCentreFight = FALSE
				bStageSetup = TRUE
				T1M1_BLOW = FALSE
				gangEnemy[40].bInitialAttack = FALSE
				gangEnemy[40].bReturnToAI = FALSE
				gangEnemy[2].bInitialAttack = FALSE
				gangEnemy[2].bReturnToAI = FALSE
				gangEnemy[16].bReturnToAI = FALSE
				gangEnemy[16].bHasPlayerGotNear = FALSE
				gangEnemy[15].bReturnToAI = FALSE
				bTriggerBikerSetPiece[0] = FALSE
				bTriggerBikerSetPiece[1] = FALSE
				bTriggerBikerSetPiece[2] = FALSE
				bTriggerBikerSetPiece[3] = FALSE
				bTriggerBikerSetPiece[4] = FALSE
				bForceAWaveOfBikersToCome = FALSE
				iBkesApproachCentreRhsProg = 0
				bAllowBuddiesToShoot = FALSE
				bTriggerWadeLeaving = FALSE

				SET_DOOR_STATE(DOORNAME_HENCHMAN_TRAILER_L, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_HENCHMAN_TRAILER_R, DOORSTATE_UNLOCKED)
				
				iSkipTimer = GET_GAME_TIMER()
				bSetupBuddies = FALSE
				bCreatePickUps = FALSE
				bDoFails = TRUE
				
				//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
				IF NOT bTrevorSplashBackEffectOn
					APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
					bTrevorSplashBackEffectOn = TRUE
				ENDIF	
			ENDIF
		ENDIF
//---------¦ UPDATE ¦---------		
	ELSE
		BOOL bPlayerUsingShotgun
		WEAPON_TYPE wPlayer
		
		//--Check if player is using sawn-off
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wPlayer)
			IF wPlayer = WEAPONTYPE_SAWNOFFSHOTGUN
			OR wPlayer = WEAPONTYPE_PUMPSHOTGUN
				bPlayerUsingShotgun = TRUE
			ELSE
				bPlayerUsingShotgun = FALSE
			ENDIF
		ELSE
			bPlayerUsingShotgun = FALSE
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			IF MANAGE_MY_TIMER(iSkipTimer,800)
				safefadeInAndWait()
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("SET UP TRUCK FOR SHOOTOUT")
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_STRONG(vehTrevor.veh,TRUE)
				SET_LABEL_AS_TRIGGERED("SET UP TRUCK FOR SHOOTOUT",TRUE)
			ENDIF
		ENDIF

		//--Deal with fuel leaking from vehicles
		IF bPlayerUsingShotgun
			iCounter = 0
			REPEAT COUNT_OF(vehTrailerPark) iCounter
				IF NOT IS_ENTITY_DEAD(vehTrailerPark[iCounter].veh)
					IF bFuelCanDropFromVeh[iCounter]
						IF iFuelDropFromVehTime[iCounter] = 0
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTrailerPark[iCounter].veh, PLAYER_PED_ID())
								SET_VEHICLE_PETROL_TANK_HEALTH(vehTrailerPark[iCounter].veh, 100)
								SET_VEHICLE_CAN_LEAK_PETROL(vehTrailerPark[iCounter].veh, TRUE)
								iFuelDropFromVehTime[iCounter]= GET_GAME_TIMER()
							ENDIF
						ELSE
							IF HAS_DW_TIME_PASSED(iFuelDropFromVehTime[iCounter], 5000)
								SET_VEHICLE_CAN_LEAK_PETROL(vehTrailerPark[iCounter].veh, FALSE)
								bFuelCanDropFromVeh[iCounter] = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//-- Deal with popping tyres
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTrailerPark[iCounter].veh, PLAYER_PED_ID())
						BURST_ALL_TYRES_ON_VEHICLE(vehTrailerPark[iCounter].veh, vehTrailerPark[iCounter].model)
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT IS_ENTITY_DEAD(vehTruck.veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTruck.veh, PLAYER_PED_ID())
					BURST_ALL_TYRES_ON_VEHICLE(vehTruck.veh, vehTruck.model)
				ENDIF
			ENDIF
		ENDIF
		
		//--Check for player making there way around the trailer park
		iCounter = 0
		REPEAT COUNT_OF(bHasPlayerEnteredShootZOne) iCounter
			IF NOT bHasPlayerEnteredShootZOne[iCounter]
				IF IS_PLAYER_IN_SHOOTOUT_ZONE(INT_TO_ENUM(SHOOTOUT_ZONE_T, iCounter))
					bHasPlayerEnteredShootZOne[iCounter] = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bSetupBuddies = FALSE
			//SETUP THE BUDDIES
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF NOT IS_PED_INJURED(budRon.ped)
					REMOVE_PED_FROM_GROUP(budRon.ped)
					SET_PED_ACCURACY(budRon.ped,1)
					SET_PED_CHANCE_OF_FIRING_BLANKS(budRon.ped,80,90)
					SET_ENTITY_HEALTH(budRon.ped,800)
					SET_PED_SUFFERS_CRITICAL_HITS(budRon.ped,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(budRon.ped,CA_USE_COVER,TRUE)
					//1561091
					SET_PED_COMBAT_ATTRIBUTES(budRon.ped,CA_JUST_SEEK_COVER,TRUE)
					SET_PED_SPHERE_DEFENSIVE_AREA(budRon.ped,GET_ENTITY_COORDS(vehTrevor.veh, FALSE),4.0)
					SET_PED_RELATIONSHIP_GROUP_HASH(budRon.ped, grpBuddies)
					SET_PED_CAN_BE_TARGETTED(budRon.ped,FALSE)
					SET_PED_CONFIG_FLAG(budRon.ped,PCF_RunFromFiresAndExplosions,FALSE)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,grpBuddies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,RELGROUPHASH_PLAYER)
					SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(budRon.ped,FALSE,grpEnemies)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, FALSE)
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpBuddies,grpEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,grpEnemies,grpBuddies)
					
					SET_PED_COMBAT_ATTRIBUTES(budRon.ped,CA_DISABLE_PIN_DOWN_OTHERS,TRUE)
					
					SET_PED_SEEING_RANGE(budRon.ped,500)
					SET_PED_TARGET_LOSS_RESPONSE(budRon.ped,TLR_NEVER_LOSE_TARGET)
					SET_PED_CONFIG_FLAG(budRon.ped,PCF_ForcedToStayInCover,TRUE)
					
					
					IF NOT DOES_BLIP_EXIST(budRon.blip)
						budRon.blip = CREATE_BLIP_FOR_PED(budRon.ped,FALSE,CHAR_RON)
						SET_BLIP_AS_FRIENDLY(budRon.blip,TRUE)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(budWade.ped)
					REMOVE_PED_FROM_GROUP(budWade.ped)
					SET_PED_ACCURACY(budWade.ped,1)
					SET_PED_CHANCE_OF_FIRING_BLANKS(budWade.ped,80,90)
					SET_ENTITY_HEALTH(budWade.ped,800)
					SET_PED_SUFFERS_CRITICAL_HITS(budWade.ped,FALSE)
					SET_PED_CAN_BE_TARGETTED(budWade.ped,FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(budWade.ped, grpBuddies)
					SET_PED_COMBAT_ATTRIBUTES(budWade.ped,CA_USE_COVER,TRUE)
					SET_PED_CONFIG_FLAG(budWade.ped,PCF_RunFromFiresAndExplosions,FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(budWade.ped,GET_ENTITY_COORDS(vehTrevor.veh, FALSE),6.0)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,grpBuddies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,RELGROUPHASH_PLAYER)
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpBuddies,grpEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,grpEnemies,grpBuddies)
					
					SET_PED_COMBAT_ATTRIBUTES(budWade.ped,CA_DISABLE_PIN_DOWN_OTHERS,TRUE) 
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, FALSE)
					
					SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(budWade.ped,FALSE,grpEnemies)
					
					SET_PED_SEEING_RANGE(budWade.ped,500)
					SET_PED_TARGET_LOSS_RESPONSE(budWade.ped,TLR_NEVER_LOSE_TARGET)
					SET_PED_CONFIG_FLAG(budWade.ped,PCF_ForcedToStayInCover,TRUE)
					
					IF NOT DOES_BLIP_EXIST(budWade.blip)
						budWade.blip = CREATE_BLIP_FOR_PED(budWade.ped,FALSE,CHAR_WADE)
						SET_BLIP_AS_FRIENDLY(budWade.blip,TRUE)
					ENDIF
				ENDIF
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,grpBuddies)

				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpBuddies,grpEnemies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,grpEnemies,grpBuddies)
				
				SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(vehTrevor.veh,FALSE,grpBuddies)
				SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(vehTrevor.veh,TRUE,grpEnemies)
				SET_VEHICLE_STRONG(vehTrevor.veh,TRUE)
				iStartFightTimer = GET_GAME_TIMER()
				bSetupBuddies = TRUE
			ENDIF
		ENDIF

		//--Deal with the bikers  / van at the end of the uber chase
		IF NOT gangEnemy[0].bHasSequence
			IF NOT IS_ENTITY_DEAD(vehBike[0].veh)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0].veh)
					//-- Send lhs biker to trailer with interior
					IF DOES_ENTITY_EXIST(gangEnemy[0].ped)
						IF NOT IS_PED_INJURED(gangEnemy[0].ped)
							SET_PED_RELATIONSHIP_GROUP_HASH(gangEnemy[0].ped, grpEnemies)
							SET_PED_COMBAT_ATTRIBUTES(gangEnemy[0].ped,CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED,TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[0].ped, FALSE)
							SET_PED_ACCURACY(gangEnemy[0].ped,0)
							GIVE_WEAPON_TO_PED(gangEnemy[0].ped,WEAPONTYPE_PISTOL,200,TRUE)
							SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[0].ped, << 55.7750, 3672.7393, 38.8194 >>,2.0)
						
							OPEN_SEQUENCE_TASK(seq)
								TASK_LEAVE_ANY_VEHICLE(NULL,0,ECF_DONT_CLOSE_DOOR)
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(seq)
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(gangEnemy[0].ped)
						TASK_PERFORM_SEQUENCE(gangEnemy[0].ped, seq)
					ENDIF
					CLEAR_SEQUENCE_TASK(seq)
					gangEnemy[0].bHasSequence = TRUE
				ENDIF
			ENDIF
		ELSE
			
			IF HAS_PED_PROCESSED_SEQ(gangEnemy[0].ped)
				IF DOES_ENTITY_EXIST(gangEnemy[0].ped)
					IF NOT IS_ENTITY_ON_SCREEN(gangEnemy[0].ped)
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[0].ped) > 20
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(gangEnemy[0].ped))
							DELETE_PED(gangEnemy[0].ped)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT gangEnemy[1].bHasSequence
			IF NOT IS_ENTITY_DEAD(vehBike[1].veh)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1].veh)
					//-- Send lhs biker to trailer with interior
					//-- Send rhs biker to trailer with interior
					IF NOT IS_PED_INJURED(gangEnemy[1].ped)
						SET_PED_RELATIONSHIP_GROUP_HASH(gangEnemy[1].ped, grpEnemies)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[1].ped,CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED,TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[1].ped, FALSE)
						SET_PED_ACCURACY(gangEnemy[1].ped,0)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[1].ped, <<63.7196, 3658.6013, 38.4564>>,2.0)
						GIVE_WEAPON_TO_PED(gangEnemy[1].ped,WEAPONTYPE_PISTOL,INFINITE_AMMO,TRUE)
					ENDIF
					IF NOT IS_PED_INJURED(gangEnemy[1].ped)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL,0,ECF_DONT_CLOSE_DOOR)
							TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
					
						TASK_PERFORM_SEQUENCE(gangEnemy[1].ped, seq)
					ENDIF
					CLEAR_SEQUENCE_TASK(seq)
					gangEnemy[1].bHasSequence = TRUE
				ENDIF
			ENDIF
		ELSE
			IF HAS_PED_PROCESSED_SEQ(gangEnemy[1].ped)
				IF DOES_ENTITY_EXIST(gangEnemy[1].ped)
					IF NOT IS_ENTITY_ON_SCREEN(gangEnemy[1].ped)
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[1].ped) > 20
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(gangEnemy[1].ped))
							DELETE_PED(gangEnemy[1].ped)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT gangEnemy[2].bTriggered
			IF NOT IS_ENTITY_DEAD(vehTruck.veh)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
					IF NOT IS_PED_INJURED(gangEnemy[2].ped)
						SET_ENTITY_HEALTH(gangEnemy[2].ped,120)
						SET_PED_RELATIONSHIP_GROUP_HASH(gangEnemy[2].ped, grpEnemies)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[2].ped, FALSE)
						SET_PED_ACCURACY(gangEnemy[2].ped,1)
						GIVE_WEAPON_TO_PED(gangEnemy[2].ped,WEAPONTYPE_PISTOL,INFINITE_AMMO,TRUE)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[2].ped,<<50.2847, 3665.5410, 38.7129>>,8)
						SET_PED_COMBAT_MOVEMENT(gangEnemy[2].ped,CM_DEFENSIVE)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[2].ped,CA_DISABLE_PINNED_DOWN,TRUE)
						SET_PED_TO_LOAD_COVER(gangEnemy[2].ped,TRUE)
		
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL,0,ECF_DONT_CLOSE_DOOR)
							TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(gangEnemy[2].ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						PLAY_PED_AMBIENT_SPEECH(gangEnemy[2].ped,"GENERIC_WAR_CRY",SPEECH_PARAMS_INTERRUPT_SHOUTED)
						gangEnemy[2].bHasSequence = TRUE
						gangEnemy[2].bTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF gangEnemy[2].bTriggered = TRUE
				IF gangEnemy[2].bReturnToAI = FALSE
					IF NOT IS_PED_INJURED(gangEnemy[2].ped)
						IF HAS_LABEL_BEEN_TRIGGERED("DID CHASE")
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(gangEnemy[2].ped,PLAYER_PED_ID())
							OR MANAGE_MY_TIMER(iStartFightTimer,25000)
								PLAY_PED_AMBIENT_SPEECH(gangEnemy[2].ped,"GENERIC_CURSE_HIGH",SPEECH_PARAMS_INTERRUPT_SHOUTED)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[2].ped,FALSE)
								SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[2].ped,GET_ENTITY_COORDS(gangEnemy[2].ped, FALSE),20)
								SET_PED_COMBAT_ATTRIBUTES(gangEnemy[2].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_COMBAT_RANGE(gangEnemy[2].ped,CR_NEAR)
								SET_PED_COMBAT_ATTRIBUTES(gangEnemy[2].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
								PRINTSTRING("gangEnemy 2 triggered")PRINTNL()
								gangEnemy[2].bReturnToAI = TRUE
							ENDIF
						ELSE
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(gangEnemy[2].ped,PLAYER_PED_ID())
							OR MANAGE_MY_TIMER(iStartFightTimer,8000)
								PLAY_PED_AMBIENT_SPEECH(gangEnemy[2].ped,"GENERIC_CURSE_HIGH",SPEECH_PARAMS_INTERRUPT_SHOUTED)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[2].ped,FALSE)
								SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[2].ped,GET_ENTITY_COORDS(gangEnemy[2].ped, FALSE),20)
								SET_PED_COMBAT_ATTRIBUTES(gangEnemy[2].ped,CA_CAN_CHARGE,TRUE)
								SET_PED_COMBAT_RANGE(gangEnemy[2].ped,CR_NEAR)
								SET_PED_COMBAT_ATTRIBUTES(gangEnemy[2].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
								PRINTSTRING("gangEnemy 2 triggered")PRINTNL()
								gangEnemy[2].bReturnToAI = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT gangEnemy[3].bHasSequence
			IF NOT IS_ENTITY_DEAD(vehTruck.veh)	
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck.veh)
					IF NOT IS_PED_INJURED(gangEnemy[3].ped)
						SET_PED_RELATIONSHIP_GROUP_HASH(gangEnemy[3].ped, grpEnemies)
						SET_PED_COMBAT_ATTRIBUTES(gangEnemy[3].ped,CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED,TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[3].ped, FALSE)
						SET_PED_ACCURACY(gangEnemy[3].ped,1)
						GIVE_WEAPON_TO_PED(gangEnemy[3].ped,WEAPONTYPE_PISTOL,INFINITE_AMMO,TRUE)
						SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[3].ped, << 55.3607, 3672.6648, 38.8215 >>,2.0)
					ENDIF

					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(NULL,0,ECF_DONT_CLOSE_DOOR)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					IF NOT IS_PED_INJURED(gangEnemy[3].ped)
						TASK_PERFORM_SEQUENCE(gangEnemy[3].ped, seq)
					ENDIF
					CLEAR_SEQUENCE_TASK(seq)
					gangEnemy[3].bHasSequence = TRUE
				ENDIF
			ENDIF
		ELSE
			IF HAS_PED_PROCESSED_SEQ(gangEnemy[3].ped)
				IF DOES_ENTITY_EXIST(gangEnemy[3].ped)
					IF NOT IS_ENTITY_ON_SCREEN(gangEnemy[3].ped)
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[3].ped) > 20
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(gangEnemy[3].ped))
							DELETE_PED(gangEnemy[3].ped)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		

		//--Setpieces. Only do them if there's > 2 peds remaining.
		IF NOT ARE_ONLY_X_TRAILER_PEDS_LEFT(4) //HAVE_ALL_TRAILER_PEDS_BEEN_KILLED()
			//--Guy in trailer runs out and tries to flee on bike
			DO_PED_FLEE_ON_BIKE_SETPIECE(bPlayerUsingShotgun)
			
			//--Bikes lhs
			DO_FIRST_LHS_BIKE_CONVOY_SETPIECE()
			
			//--Bikes Rhs
			DO_FIRST_RHS_BIKE_CONVOY_SETPIECE()
			
			//-- Bike rhs, back corner
			DO_SECOND_RHS_BIKE_CONVOY_SETPIECE()
			
			//--Bikes approach centre, RHS
			DO_BIKES_APPROACH_CENTRE_RHS_SETPIECE()
			
			//--Bikes approach centre, LHS
			DO_BIKES_APPROACH_CENTRE_LHS_SETPIECE()
			
			//new -single ped
			DO_GUY_TO_LURE_YOU_FORWARDS_SETPIECE()
			
			//new -
			DO_GUY_IN_CENTRE_UPON_CUTTING_THROUGH()
			
			//new-
			DO_GUY_IN_THE_CENTRE_WITH_SHOTGUN()
			
			//new
			DO_AGGRESSIVE_GUYS_IN_THE_CENTRE()
			
			//new -
			//!!! TEMP REMOVE
			DO_THE_RIFLE_GUY()
			
			//new
			DO_GRENADE_GUY()
			
			//new -
			DO_THE_ROOF_GUY()
		ENDIF
		
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF NOT gangEnemy[40].bInitialAttack
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR IS_ENTITY_ON_SCREEN(gangEnemy[40].ped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[40].ped,TRUE)
					TASK_COMBAT_PED(gangEnemy[40].ped,PLAYER_PED_ID())
					REMOVE_COVER_POINT(gangEnemy[40].coverGangUse)
					gangEnemy[40].coverGangUse = ADD_COVER_POINT(<<53.0982, 3645.8618, 38.6581>>, 185.3102, COVUSE_WALLTORIGHT, COVHEIGHT_HIGH,  COVARC_120)
					SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[40].ped,<<53.31, 3646.12, 39.66>>,4,TRUE)
					SET_PED_COMBAT_MOVEMENT(gangEnemy[40].ped,CM_DEFENSIVE)
					SET_PED_COMBAT_ATTRIBUTES(gangEnemy[40].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(gangEnemy[40].ped,CA_CAN_CHARGE,TRUE)
					PRINTSTRING("gangEnemy 40 triggered")PRINTNL()
					PLAY_PED_AMBIENT_SPEECH(gangEnemy[40].ped,"GENERIC_WAR_CRY",SPEECH_PARAMS_INTERRUPT_SHOUTED)
					gangEnemy[40].bInitialAttack = TRUE
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(gangEnemy[40].ped)
					IF gangEnemy[40].bReturnToAI = FALSE
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(gangEnemy[40].ped,PLAYER_PED_ID())
						OR MANAGE_MY_TIMER(iStartFightTimer,10000)
						OR (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), gangEnemy[40].ped) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), gangEnemy[40].ped) AND MANAGE_MY_TIMER(iStartFightTimer,4000))
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[40].ped,FALSE)
							SET_PED_COMBAT_ATTRIBUTES(gangEnemy[40].ped,CA_CAN_CHARGE,TRUE)
							SET_PED_COMBAT_RANGE(gangEnemy[40].ped,CR_NEAR)
							SET_PED_COMBAT_ATTRIBUTES(gangEnemy[40].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
							SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(gangEnemy[40].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 25.0 ,FALSE)
							PRINTSTRING("gangEnemy 40 returned to AI")PRINTNL()
							PLAY_PED_AMBIENT_SPEECH(gangEnemy[40].ped,"GENERIC_WAR_CRY",SPEECH_PARAMS_INTERRUPT_SHOUTED)
							//SCRIPT_ASSERT("TEST")
							//PRINTLN(123456)
							gangEnemy[40].bReturnToAI = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF gangEnemy[16].bTriggered
				IF NOT IS_PED_INJURED(gangEnemy[16].ped)
					IF gangEnemy[16].bReturnToAI = FALSE
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(gangEnemy[16].ped,PLAYER_PED_ID())
						OR (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), gangEnemy[16].ped) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), gangEnemy[16].ped))
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<23.035633,3659.842285,37.530746>>, <<40.428093,3684.749512,45.737465>>, 12.250000)
						OR (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),gangEnemy[16].ped) < 22 AND NOT IS_VEHICLE_DRIVEABLE(vehTrailerPark[12].veh))
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[16].ped,FALSE)
							SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(gangEnemy[16].ped, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 25.0 ,FALSE)
							SET_PED_COMBAT_ATTRIBUTES(gangEnemy[16].ped,CA_CAN_CHARGE,TRUE)
							SET_PED_COMBAT_RANGE(gangEnemy[16].ped,CR_NEAR)
							SET_PED_COMBAT_ATTRIBUTES(gangEnemy[16].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
							gangEnemy[16].bReturnToAI = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF gangEnemy[15].bTriggered
				IF NOT IS_PED_INJURED(gangEnemy[15].ped)
					IF gangEnemy[15].bReturnToAI = FALSE
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(gangEnemy[15].ped,PLAYER_PED_ID())
						OR GET_DISTANCE_BETWEEN_ENTITIES(gangEnemy[15].ped,PLAYER_PED_ID()) < 20
						OR IS_PED_INJURED(gangEnemy[6].ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gangEnemy[15].ped,FALSE)
							SET_PED_SPHERE_DEFENSIVE_AREA(gangEnemy[15].ped,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),20,TRUE)
							SET_PED_COMBAT_ATTRIBUTES(gangEnemy[15].ped,CA_CAN_CHARGE,TRUE)
							SET_PED_COMBAT_RANGE(gangEnemy[15].ped,CR_NEAR)
							SET_PED_COMBAT_ATTRIBUTES(gangEnemy[15].ped,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
							gangEnemy[15].bReturnToAI = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[9].bTriggered
				//-- CLosest on rhs
				gangEnemy[9].bTriggered = TRUE
			ELSE
				IF NOT bHasPlayerEnteredShootZOne[ZONE_CENTRE]
					IF IS_PLAYER_IN_SHOOTOUT_ZONE(ZONE_INTRO)
						IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<81.039383,3683.039795,37.952450>>, <<100.188187,3642.277832,43.717731>>, 39.500000)
							
						ENDIF
					ENDIF
				ENDIF
				
				//FIX FOR IDLE CROUCHED PEDS
				//--Trigger when rhs van reverses out
				IF NOT gangEnemy[21].bTriggered
					IF SHOULD_RHS_VAN_SETPIECE_TRIGGER()
						IF gangEnemy[21].bTriggered = FALSE
							IF IS_POINT_VISIBLE_TO_PLAYER( << 108.7403, 3718.4480, 39.7577 >>, 1.0, 200.0)
							OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 108.7403, 3718.4480, 39.7577 >>) < 30
								gangEnemy[21].bTriggered = TRUE
								gangEnemy[22].bTriggered = TRUE
								//gangEnemy[23].bTriggered = TRUE
							ENDIF
						ENDIF
					ELSE
						IF gangEnemy[21].bTriggered = FALSE
							IF IS_POINT_VISIBLE_TO_PLAYER( << 108.7403, 3718.4480, 39.7577 >>, 1.0, 200.0)
							OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<< 108.7403, 3718.4480, 39.7577 >>) < 30
								gangEnemy[21].bTriggered = TRUE
								gangEnemy[22].bTriggered = TRUE
								//gangEnemy[23].bTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF NOT gangEnemy[29].bTriggered
				//-- Have some guys run through the middle section if the player can see in 
				IF SHOULD_MIDDLE_RUNNERS_TRIGGER_FROM_LHS()
					SETUP_THESE_PEDS(PED_FIGHT_MIDDLE_RUNNER)
					gangEnemy[29].bTriggered = TRUE
					gangEnemy[30].bTriggered = TRUE
					//gangEnemy[31].bTriggered = TRUE
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(gangEnemy[29].ped)
					IF HAS_PED_PROCESSED_SEQ(gangEnemy[29].ped)
						IF NOT HAS_PLAYER_APPROACHED_CENTRE()
							DELETE_PED(gangEnemy[29].ped)
						ELSE
							SET_GANG_MEMBER_ATTACKS_AT_CURRENT_LOC(29)
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(gangEnemy[30].ped)
					IF HAS_PED_PROCESSED_SEQ(gangEnemy[30].ped)
						IF NOT HAS_PLAYER_APPROACHED_CENTRE()
						
							DELETE_PED(gangEnemy[30].ped)
						ELSE
							SET_GANG_MEMBER_ATTACKS_AT_CURRENT_LOC(30)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(gangEnemy[15].ped)
				IF NOT gangEnemy[6].bTriggered
					IF bSetupBuddies = TRUE
						//-- Guys in the trailer, runs to van
						gangTemp[0] = gangEnemy[40]
						gangTemp[1] = gangEnemy[2]
						//-- WIll fight when one of the first two peds have been killed
						IF HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 2 ,2)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<47.683052,3660.819824,38.261711>>, <<55.504646,3671.674561,43.234177>>, 2.250000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<41.593533,3653.652588,37.460617>>, <<20.178158,3651.211426,44.440399>>, 4.500000)
						OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<32.48455, 3667.24927, 39.54953>>) < 26
						OR MANAGE_MY_TIMER(iStartFightTimer,30000)
						OR IS_PED_INJURED(gangEnemy[15].ped)
							IF NOT IS_PED_INJURED(gangEnemy[6].ped)
								PLAY_PED_AMBIENT_SPEECH(gangEnemy[6].ped, "GENERIC_FUCK_YOU")
							ENDIF
							gangEnemy[6].bTriggered = TRUE
							gangDog[0].bTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//--Guys near back left
			IF NOT gangEnemy[15].bTriggered
				gangTemp[0] = gangEnemy[9]
				gangTemp[1] = gangEnemy[11]
				gangTemp[2] = gangEnemy[17]
				IF HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 3 ,3)
				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-4.104918,3693.259277,37.892815>>, <<29.597843,3712.840088,41.891338>>, 34.250000)
					gangEnemy[15].bTriggered = TRUE
				ENDIF
				
				IF NOT IS_PED_INJURED(gangEnemy[15].ped)
					IF iVehicleBlowUpSetPiece > 0
						IF IS_ENTITY_ON_SCREEN(gangEnemy[15].ped)
							gangEnemy[15].bTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT gangEnemy[16].bTriggered
				IF NOT IS_PED_INJURED(gangEnemy[16].ped)
					IF GET_DISTANCE_BETWEEN_ENTITIES(gangEnemy[16].ped,PLAYER_PED_ID()) < 25
						IF GET_DISTANCE_BETWEEN_ENTITIES(gangEnemy[16].ped,PLAYER_PED_ID()) < 15
						OR NOT IS_ENTITY_OCCLUDED(gangEnemy[16].ped)
							gangEnemy[16].bTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bSetupBuddies = TRUE
				IF NOT gangEnemy[16].bTriggered
					IF NOT IS_PED_INJURED(gangEnemy[16].ped)
						IF GET_DISTANCE_BETWEEN_ENTITIES(gangEnemy[16].ped,PLAYER_PED_ID()) < 65
						OR MANAGE_MY_TIMER(iStartFightTimer,20000)
							gangEnemy[16].bTriggered = TRUE
							gangEnemy[15].bTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF NOT gangEnemy[37].bTriggered
				IF HAS_PLAYER_APPROACHED_CENTRE_FROM_LHS()
					IF iBkesApproachCentreRhsProg > 0
						IF NOT IS_ENTITY_DEAD(vehTrailerPark[20].veh)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailerPark[20].veh)
								gangEnemy[36].bTriggered = TRUE
								gangEnemy[37].bTriggered = TRUE
							ENDIF
						ENDIF
						
					ENDIF
				ELIF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<73.803818,3736.187988,38.350574>>, <<72.868080,3746.608154,41.445698>>, 2.250000)
					//-- Approach frm RHS, slightly before rhs centre trigger
					gangEnemy[35].bTriggered = TRUE
					gangEnemy[36].vGotoCoords =<< 49.4762, 3719.5313, 38.7279 >> 
					gangEnemy[36].moveAttackStyle = MOVE_AGGRESSIVE_CHARGE
					gangEnemy[36].bTriggered = TRUE
					
					gangEnemy[37].vGotoCoords = << 55.5819, 3713.4048, 38.8812 >>
					gangEnemy[37].iAttackDelay = 150
					gangEnemy[37].bTriggered = TRUE
				ENDIF
			ENDIF
		
				//--Guys in RHS Trailer
				IF NOT gangEnemy[27].bTriggered
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<95.180412,3739.180176,36.013111>>, <<129.413666,3713.149658,40.914532>>, 25.250000)
					OR IS_ENTITY_ON_SCREEN(gangEnemy[27].ped) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(gangEnemy[27].ped,FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE)) < 20
					OR (bDoneCentreFight )
						gangEnemy[26].bTriggered = TRUE
						gangEnemy[27].bTriggered = TRUE
						
						gangDog[1].bTriggered = TRUE
					ELSE
						IF iDoSecRhsBikeProg > 0
							IF gangEnemy[24].bTriggered
								IF gangEnemy[25].bTriggered
									gangTemp[0] = gangEnemy[24]
									gangTemp[1] = gangEnemy[25]
									IF HAVE_THESE_PEDS_BEEN_KILLED(gangTemp, 2 ,2)
										gangEnemy[26].bTriggered = TRUE
										//SCRIPT_ASSERT("Trig2")
										gangEnemy[27].bTriggered = TRUE
										gangDog[1].bTriggered = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//PLAYTHROUGH
				IF HAS_PLAYER_APPROACHED_METH_TRAILER()
					//-- Guys at back, centre, behind white fence
					IF NOT gangEnemy[35].bTriggered
						gangEnemy[35].vGotoCoords =  << 24.4197, 3726.0955, 38.6186 >>
						gangEnemy[35].moveAttackStyle = MOVE_FOLLOW_NAVMESH_DUCK
						gangEnemy[35].gang_da = DA_AT_GO_TO
						gangEnemy[35].gangCombatMovement = CM_DEFENSIVE
						gangEnemy[35].bTriggered = TRUE
					ENDIF
				ENDIF
				
				//--For player approaching through centre
				//DO_BIKES_FOR_PLAYER_THROUGH_CENTRE_SETPIECE()
				
				IF NOT bDoneCentreFight
					DO_PED_FLEE_ON_BIKE_RHS_TRAILER_SETPIECE()
					
					IF HAS_PLAYER_APPROACHED_CENTRE_FROM_MIDDLE()
						//-- Guys at back, centre, behind white fence
						IF NOT gangEnemy[35].bTriggered
							gangEnemy[35].vGotoCoords = << 40.5141, 3720.7407, 38.9402 >>
							gangEnemy[35].moveAttackStyle = MOVE_AGGRESSIVE_CHARGE
							gangEnemy[35].gang_da = DA_AT_GO_TO
							gangEnemy[35].gangCombatMovement = CM_DEFENSIVE
							gangEnemy[35].bTriggered = TRUE
						ENDIF
						
						IF NOT gangEnemy[36].bTriggered
							gangEnemy[36].vGotoCoords = << 34.6777, 3711.9556, 39.1706 >>
							gangEnemy[36].moveAttackStyle = MOVE_AGGRESSIVE_CHARGE
							gangEnemy[36].gang_da = DA_AT_GO_TO
							gangEnemy[36].gangCombatMovement = CM_DEFENSIVE
							gangEnemy[36].bTriggered = TRUE
						ENDIF
						
						bDoneCentreFight = TRUE
					ENDIF
				ELSE
					IF NOT gangEnemy[28].bTriggered
						//-- Guy for BIKE_RHS_TRAILER_SETPIECE
						gangEnemy[28].vGotoCoords = << 106.4858, 3741.2505, 38.7412 >>
						gangEnemy[28].gang_da = DA_AT_GO_TO
						gangEnemy[28].gangCombatMovement = CM_WILLADVANCE
						gangEnemy[28].bTriggered = TRUE
					ENDIF

					//-- Guy in van that reverses
					IF NOT gangEnemy[20].bTriggered
						IF NOT IS_PED_INJURED(gangEnemy[20].ped)
							CLEAR_PED_TASKS_IMMEDIATELY(gangEnemy[20].ped)
							SET_ENTITY_COORDS(gangEnemy[20].ped, << 99.7073, 3688.6150, 38.7123 >>)
							gangEnemy[20].vGotoCoords =  << 99.7073, 3688.6150, 38.7123 >>
							gangEnemy[20].gang_da = DA_AT_GO_TO_LARGE
							gangEnemy[20].gangCombatMovement = CM_DEFENSIVE
							gangEnemy[20].bTriggered = TRUE
						ENDIF
					ENDIF
					
					
					//-- Guys near forklift
					IF NOT gangEnemy[15].bTriggered
						gangEnemy[15].bTriggered = TRUE
					ENDIF
					IF NOT gangEnemy[16].bTriggered
						gangEnemy[16].bTriggered = TRUE
					ENDIF
				ENDIF
		//	ENDIF

		ENDIF
		
		MANAGE_gang_attack(gangEnemy)
		
		MANAGE_GANG_BLIPS()
		
		MANAGE_DOGS()
		
		//mission passed logic
		IF bTriggerBikerSetPiece[0] = TRUE
		OR bTriggerBikerSetPiece[1] = TRUE
		OR bTriggerBikerSetPiece[2] = TRUE
		OR bTriggerBikerSetPiece[3] = TRUE
		OR bTriggerBikerSetPiece[4] = TRUE
			IF ARE_ONLY_X_TRAILER_PEDS_LEFT(4) // HAVE_ALL_TRAILER_PEDS_BEEN_KILLED()
				CANCEL_MUSIC_EVENT("TRV1_EXPLODE")
				PREPARE_MUSIC_EVENT("TRV1_BIKERS_FLEE")
				IF TRIGGER_MUSIC_EVENT("TRV1_BIKERS_FLEE")
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,grpBuddies,grpEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,grpEnemies,grpBuddies)
					
					IF NOT IS_PED_INJURED(gangEnemy[31].ped)
						SET_ENTITY_HEALTH(gangEnemy[31].ped,120)
					ENDIF
					
					FOR i = 0 TO MAX_GANG - 1
						CLEANUP_AI_PED_BLIP(gangEnemy[i].EnemyBlipData)
					ENDFOR
					
					FOR i = 0 TO MAX_DOGS - 1
						CLEANUP_AI_PED_BLIP(gangDog[i].EnemyBlipData)
					ENDFOR

					iCounter = 0
					REPEAT COUNT_OF(gangDog) i	
						IF NOT IS_PED_INJURED(gangDog[i].ped)
							IF gangDog[i].bFlee = FALSE
								TASK_SMART_FLEE_PED(gangDog[i].ped,PLAYER_PED_ID(),150,-1)
							ENDIF
						ENDIF
					ENDREPEAT
						
					IF NOT DOES_ENTITY_EXIST(gangFlee[0].ped)	
						iCounter = 0
						REPEAT COUNT_OF(gangEnemy) i	
							IF NOT IS_PED_INJURED(gangEnemy[i].ped)
								gangFlee[0] = gangEnemy[i]
								RESET_GANG_ATTACK(gangFlee[0])
								gangFlee[0].iPedTimer = GET_GAME_TIMER()
							ENDIF
						ENDREPEAT
					ENDIF

					IF NOT DOES_ENTITY_EXIST(gangFlee[1].ped)
						iCounter = 0
						REPEAT COUNT_OF(gangEnemy) i	
							IF NOT IS_PED_INJURED(gangEnemy[i].ped)
								IF gangFlee[0].ped <> gangEnemy[i].ped
								AND gangFlee[2].ped <> gangEnemy[i].ped
									gangFlee[1] = gangEnemy[i]
									RESET_GANG_ATTACK(gangFlee[1])
									gangFlee[1].iPedTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(gangFlee[2].ped)
						iCounter = 0
						REPEAT COUNT_OF(gangEnemy) i	
							IF NOT IS_PED_INJURED(gangEnemy[i].ped)
								IF gangFlee[0].ped <> gangEnemy[i].ped
								AND gangFlee[1].ped <> gangEnemy[i].ped
									gangFlee[2] = gangEnemy[i]
									RESET_GANG_ATTACK(gangFlee[2])
									gangFlee[2].iPedTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(gangFlee[3].ped)
						iCounter = 0
						REPEAT COUNT_OF(gangEnemy) i	
							IF NOT IS_PED_INJURED(gangEnemy[i].ped)
								IF gangFlee[0].ped <> gangEnemy[i].ped
								AND gangFlee[1].ped <> gangEnemy[i].ped
								AND gangFlee[2].ped <> gangEnemy[i].ped
									gangFlee[3] = gangEnemy[i]
									RESET_GANG_ATTACK(gangFlee[3])
									gangFlee[3].iPedTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
		//			
		//			IF NOT DOES_ENTITY_EXIST(gangFlee[4].ped)
		//				iCounter = 0
		//				REPEAT COUNT_OF(gangEnemy) i	
		//					IF NOT IS_PED_INJURED(gangEnemy[i].ped)
		//						IF gangFlee[0].ped <> gangEnemy[i].ped
		//						AND gangFlee[1].ped <> gangEnemy[i].ped
		//						AND gangFlee[2].ped <> gangEnemy[i].ped
		//						AND gangFlee[3].ped <> gangEnemy[i].ped
		//							gangFlee[4] = gangEnemy[i]
		//							RESET_GANG_ATTACK(gangFlee[4])
		//							gangFlee[4].iPedTimer = GET_GAME_TIMER()
		//						ENDIF
		//					ENDIF
		//				ENDREPEAT
		//			ENDIF
					
					SET_TIME_SCALE(1.0)
					MANAGE_FLEEING_PEDS()
					bTrevorSHoutsAtFleeingBikers = FALSE
					bSetUpStageData = FALSE
					bStageSetup = FALSE
					bSkipped = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF ARE_ONLY_X_TRAILER_PEDS_LEFT(4)
				bForceAWaveOfBikersToCome = TRUE
				//--Bikes lhs
				//DO_FIRST_LHS_BIKE_CONVOY_SETPIECE()
				
				//--Bikes Rhs
				//DO_FIRST_RHS_BIKE_CONVOY_SETPIECE()
				
				//-- Bike rhs, back corner
				//DO_SECOND_RHS_BIKE_CONVOY_SETPIECE()
				
				//--Bikes approach centre, RHS
				DO_BIKES_APPROACH_CENTRE_RHS_SETPIECE()
				
				//--Bikes approach centre, LHS
				//DO_BIKES_APPROACH_CENTRE_LHS_SETPIECE()
			ENDIF
		ENDIF
	ENDIF 
	RETURN FALSE
ENDFUNC

PROC MANAGE_TRAILER_FORCES()

	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTrevor.veh)
			IF IS_VEHICLE_DRIVEABLE(vehTrailer)
			
				IF IS_VECTOR_ZERO(vForce)
				
				ENDIF
				
				IF bDust = TRUE
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_crash_dust)
						SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_crash_dust, "Speed",GET_ENTITY_SPEED(vehTrailer)/20)
					ENDIF
				ENDIF
				
				//PRINTSTRING("PLAYER SPEED:")PRINTFLOAT(GET_ENTITY_SPEED(vehTrevor.veh))PRINTNL() 
				
				IF IS_ENTITY_TOUCHING_ENTITY(vehTrevor.veh,vehTrailer)
					IF bStartForce
						REQUEST_PTFX_ASSET()
						IF HAS_PTFX_ASSET_LOADED()
							PRINTSTRING("ApplyingStartForce")PRINTNL()
							//APPLY_FORCE_TO_ENTITY(vehTrailer,APPLY_TYPE_IMPULSE,<<0,0.8 * GET_ENTITY_SPEED(vehTrevor.veh)+ 0.5,0.5>>,<<0,0,0>>,0,FALSE,FALSE,TRUE)
							APPLY_FORCE_TO_ENTITY(vehTrailer,APPLY_TYPE_IMPULSE,<<0,0.9 * GET_ENTITY_SPEED(vehTrevor.veh)+ 0.7,0.5>>,<<0,0,0>>,0,FALSE,FALSE,TRUE)
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev1_trailer_boosh",vehTrailer,<<1.6,1.6,3.2>>,<<0,0,0>>)
							REQUEST_PTFX_ASSET()
							PLAY_SOUND_FROM_ENTITY(-1, "TREVOR_1_TRAILER_IMPACT_MASTER_A", vehTrailer)
							PRINTSTRING("TREVOR_1_TRAILER_IMPACT_MASTER_A")PRINTNL()
							bDust = FALSE
							bSplash = FALSE
							bStartForce = FALSE
						ENDIF
					ELSE
						
						IF bDust = FALSE
							ptfx_crash_dust = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev1_crash_dust",vehTrailer,<<2.0,2.0,0.2>>,<<0,0,0>>)
							bDust = TRUE
						ENDIF
						

						//BIG DISH
						IF bFrag1 = FALSE
							REQUEST_PTFX_ASSET()
							IF HAS_PTFX_ASSET_LOADED()
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehTrailer, FALSE),<< -49.0106, 3098.9553, 24.7669 >>) < 50
									SET_ENTITY_PROOFS(vehTrailer,FALSE,FALSE,FALSE,FALSE,FALSE)
									SET_ENTITY_INVINCIBLE(vehTrailer,FALSE)
									SET_VEHICLE_DAMAGE(vehTrailer, GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,9),2000,200,TRUE)
									SET_VEHICLE_DAMAGE(vehTrailer, GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,0),80,20,TRUE)
									ptfx_spark1 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev1_trailer_wires",vehTrailer,<<1.6,1.6,3.2>>,<<0,0,0>>)
									ptfx_spark2 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev1_trailer_wires",vehTrailer,<<-1.6,1.6,3.2>>,<<0,0,0>>)
									//ADD_EXPLOSION(GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,9),EXP_TAG_GRENADE,0.5,FALSE,TRUE,0)
									PLAY_SOUND_FROM_ENTITY(-1, "TREVOR_1_TRAILER_IMPACT_MASTER_C", vehTrailer)
									//SET_ENTITY_INVINCIBLE(vehTrailer,TRUE)
									SET_ENTITY_PROOFS(vehTrailer,TRUE,TRUE,TRUE,FALSE,TRUE)
									PRINTSTRING("TREVOR_1_TRAILER_IMPACT_MASTER_C")PRINTNL()
									bFrag1 = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						//AERIAL
						IF bFrag2 = FALSE
							REQUEST_PTFX_ASSET()
							IF HAS_PTFX_ASSET_LOADED()
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehTrailer, FALSE),<< -49.0106, 3098.9553, 24.7669 >>) < 18
									SET_ENTITY_PROOFS(vehTrailer,FALSE,FALSE,FALSE,FALSE,FALSE)
									SET_ENTITY_INVINCIBLE(vehTrailer,FALSE)
									SET_VEHICLE_DAMAGE(vehTrailer, GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,7),2000,200,TRUE)
									SET_VEHICLE_DAMAGE(vehTrailer, GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,0),60,40,TRUE)
									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev1_trailer_boosh",vehTrailer,<<-1.6,5.4,3.2>>,<<0,0,0>>)
									ptfx_spark3 =START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev1_trailer_wires",vehTrailer,<<-1.6,5.4,3.2>>,<<0,0,0>>)
									//ADD_EXPLOSION(GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,7),EXP_TAG_GRENADE,0.5,FALSE,TRUE,0)
									PLAY_SOUND_FROM_ENTITY(-1, "TREVOR_1_TRAILER_IMPACT_MASTER_C", vehTrailer)
									//SET_ENTITY_INVINCIBLE(vehTrailer,TRUE)
									SET_ENTITY_PROOFS(vehTrailer,TRUE,TRUE,TRUE,FALSE,TRUE)
									PRINTSTRING("TREVOR_1_TRAILER_IMPACT_MASTER_C")PRINTNL()
									bFrag2 = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						//SMALL DISH
						IF bFrag3 = FALSE
							REQUEST_PTFX_ASSET()
							IF HAS_PTFX_ASSET_LOADED()
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehTrailer, FALSE),<< -49.0106, 3098.9553, 24.7669 >>) < 10
									SET_ENTITY_PROOFS(vehTrailer,FALSE,FALSE,FALSE,FALSE,FALSE)
									SET_ENTITY_INVINCIBLE(vehTrailer,FALSE)
									SET_VEHICLE_DAMAGE(vehTrailer, GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,13),2000,200,TRUE)
									SET_VEHICLE_DAMAGE(vehTrailer, GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,0),120,20,TRUE)
									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev1_trailer_boosh",vehTrailer,<<1.6,5.6,3.2>>,<<0,0,0>>)
									ptfx_spark4 =START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev1_trailer_wires",vehTrailer,<<1.6,5.6,3.2>>,<<0,0,0>>)
									//ADD_EXPLOSION(GET_WORLD_POSITION_OF_ENTITY_BONE(vehTrailer,7),EXP_TAG_GRENADE,0.5,FALSE,TRUE,13)
									PLAY_SOUND_FROM_ENTITY(-1, "TREVOR_1_TRAILER_IMPACT_MASTER_C", vehTrailer)
									//SET_ENTITY_INVINCIBLE(vehTrailer,TRUE)
									SET_ENTITY_PROOFS(vehTrailer,TRUE,TRUE,TRUE,FALSE,TRUE)
									PRINTSTRING("TREVOR_1_TRAILER_IMPACT_MASTER_C")PRINTNL()
									bFrag3 = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						
						IF NOT IS_ENTITY_IN_ANGLED_AREA(vehTrailer,<<-101.270760,3104.847168,22.087103>>,<<-16.644316,3122.307129,26.642069>>,28.250000)
							APPLY_FORCE_TO_ENTITY(vehTrailer,APPLY_TYPE_FORCE,<<0,1.8 * GET_ENTITY_SPEED(vehTrevor.veh) + 3,-1>>,<<0,0,0>>,0,FALSE,FALSE,TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

ENTITY_INDEX entityTempCutsceneIndex
OBJECT_INDEX objWeaponTrevor
PED_INDEX pedTempCutsceneIndex
BOOL bGetOrtegaWet

/// PURPOSE:
/// Play the trailer push cutscene
PROC RUN_TRAILER_IN_WATER_CUTSCENE()

	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying
//		PRINTLN("@@@@@@@@@@@@@ GET_FOLLOW_PED_CAM_VIEW_MODE(): ", GET_FOLLOW_PED_CAM_VIEW_MODE(), " @@@@@@@@@@@@@@@@")
		
		PRINTSTRING("i_current_event:")PRINTINT(i_current_event)PRINTNL()
		
		BLOCK_FIRST_PERSON()

		WAIT(0)
		
		BLOCK_FIRST_PERSON()			

		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_MrPhilips")

		#IF IS_DEBUG_BUILD
			IF IS_CUTSCENE_ACTIVE()
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
					b_skipped_mocap = TRUE
					e_section_stage = SECTION_STAGE_SKIP
				ENDIF
			ENDIF
		#ENDIF

		IF e_section_stage = SECTION_STAGE_SETUP
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						SET_VEHICLE_FRICTION_OVERRIDE(vehTrailer,0.3)
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_PUSH_THE_TRAILER")
						STOP_AUDIO_SCENE("TREVOR_1_PUSH_THE_TRAILER")
					ENDIF
					bcutsceneplaying = TRUE
					bClearCutscenArea = FALSE
					SET_START_AND_END_VECTORS()
					INITALISE_ARRAYS(STAGE_INIT_MISSION)
					
					REQUEST_CUTSCENE("TRV_1_MCS_3_CONCAT")
					REQUEST_ANIM_DICT("misstrevor1")
					i_current_event++
				ELIF i_current_event = 1
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						SET_VEHICLE_FRICTION_OVERRIDE(vehTrailer,0.3)
					ENDIF
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					AND HAS_ANIM_DICT_LOADED("misstrevor1")
					AND IPL_GROUP_SWAP_IS_READY()
						BLOCK_FIRST_PERSON()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							WAIT(0)
						ENDIF
						
						BLOCK_FIRST_PERSON()
						
						objWeaponTrevor = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
						IF DOES_ENTITY_EXIST(objWeaponTrevor)
							REGISTER_ENTITY_FOR_CUTSCENE(objWeaponTrevor, "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							IF NOT IS_PED_INJURED(budRon.ped)
								REGISTER_ENTITY_FOR_CUTSCENE(budRon.ped, "Ron", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								REMOVE_PED_FROM_GROUP(budRon.ped)
								//SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
							ENDIF 
						ENDIF
						
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							REGISTER_ENTITY_FOR_CUTSCENE(npcOrtega.ped, "Ortega", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ADD_PED_FOR_DIALOGUE(structConvoPeds, 7, npcOrtega.ped, "ORTEGA")
						ENDIF
						
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							FREEZE_ENTITY_POSITION(npcOrtega.ped,FALSE)
							CLEAR_PED_TASKS_IMMEDIATELY(npcOrtega.ped)
							SET_ENTITY_INVINCIBLE(npcOrtega.ped,FALSE)
						ENDIF
						
						IF IS_ENTITY_ATTACHED(npcOrtega.ped)
							DETACH_ENTITY(npcOrtega.ped,FALSE)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							REGISTER_ENTITY_FOR_CUTSCENE(vehTrevor.veh, "Trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							SET_ENTITY_VISIBLE(vehTrevor.veh,TRUE)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							REGISTER_ENTITY_FOR_CUTSCENE(vehTrailer, "Ortega_Trailer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							SET_ENTITY_VISIBLE(vehTrailer,TRUE)
						ENDIF
						
						IF NOT IS_ENTITY_VISIBLE(npcOrtega.ped)
							SET_ENTITY_VISIBLE(npcOrtega.ped,TRUE)
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
							STOP_SYNCHRONIZED_ENTITY_ANIM(npcOrtega.ped,INSTANT_BLEND_OUT,TRUE)
						ENDIF
						
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						SET_MULTIHEAD_SAFE(TRUE)
						PRINTLN("@@@@@@@@@@@@ SET_MULTIHEAD_SAFE(TRUE) @@@@@@@@@@@")
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						//Wait a couple of frames before clearing/warping stuff, so the cutscene is active first.
						//WAIT(0)
						
						INFORM_MISSION_STATS_OF_FINANCIAL_DAMAGE(5000)
						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, TRUE, FALSE)
						bExitState = FALSE
						bGetOrtegaWet = FALSE
						b_skipped_mocap = FALSE
						i_current_event = 0
						e_section_stage = SECTION_STAGE_RUNNING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			
			IF NOT bGetOrtegaWet
				IF GET_CUTSCENE_TIME() > 1941
					entityTempCutsceneIndex = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Ortega")
					IF DOES_ENTITY_EXIST(entityTempCutsceneIndex)
						pedTempCutsceneIndex =GET_PED_INDEX_FROM_ENTITY_INDEX(entityTempCutsceneIndex) 
						IF NOT IS_PED_INJURED(pedTempCutsceneIndex)
							SET_PED_WETNESS_HEIGHT(pedTempCutsceneIndex,2.0)
							bGetOrtegaWet = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF bClearCutscenArea = FALSE
				bBlockFirstPerson = FALSE
				IF IS_VEHICLE_DRIVEABLE(vehTrailer)
					SET_VEHICLE_FRICTION_OVERRIDE(vehTrailer,0.3)
				ENDIF
				IF IS_CUTSCENE_PLAYING()
					IPL_GROUP_SWAP_FINISH()
					SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER, BUILDINGSTATE_DESTROYED)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_ORTEGA_TRAILER_WATER, BUILDINGSTATE_DESTROYED)
					IF NOT IS_PED_INJURED(npcOrtega.ped)
						SET_ENTITY_VISIBLE(npcOrtega.ped,TRUE)
						SET_PED_WETNESS_HEIGHT(npcOrtega.ped,2.0)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehTrailer)
					ENDIF
				
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEHICLE_DOOR_CONTROL(vehTrevor.veh,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,0)
						SET_VEHICLE_DOOR_CONTROL(vehTrevor.veh,SC_DOOR_FRONT_RIGHT,DT_DOOR_INTACT,0)
					ENDIF
					CLEAR_AREA(<< -46.236, 3096.727, 25.713 >>, 100.0, TRUE)
					iSkipCutsceneTimer = GET_GAME_TIMER()
					bClearCutscenArea = TRUE
				ENDIF
			ELSE
				IF IS_CUTSCENE_PLAYING()
					IF MANAGE_MY_TIMER(iSkipCutsceneTimer,2000)
						IF NOT b_skipped_mocap
							IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
								CONTROL_FADE_OUT(500)
							    e_section_stage = SECTION_STAGE_SKIP
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
//			IF NOT HAS_LABEL_BEEN_TRIGGERED("REQ_JERRY_CAN")
//				IF SETUP_MISSION_REQUIREMENT(REQ_JERRY_CAN,<<0,0,0>>)
//					SET_LABEL_AS_TRIGGERED("REQ_JERRY_CAN",TRUE)
//				ENDIF
//			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF bCachedCamMode
//					PRINTLN("@@@@@@@@@@@@@ SET_FOLLOW_PED_CAM_VIEW_MODE(cachedCamMode): ", cachedCamMode, " @@@@@@@@@@@")
					SET_FOLLOW_PED_CAM_VIEW_MODE(cachedCamMode)
					IF NOT bPushInToFirstPerson
					AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					AND GET_CUTSCENE_TIME() > GET_CUTSCENE_TOTAL_DURATION() - 300
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bPushInToFirstPerson = TRUE
					ENDIF
				ENDIF
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_THREATEN")
					IF TRIGGER_MUSIC_EVENT("TRV1_THREATEN")
						SET_LABEL_AS_TRIGGERED("TRV1_THREATEN", TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(25.5704)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-8.7712)
				IF IS_VEHICLE_DRIVEABLE(vehTrailer)	
					SET_ENTITY_COLLISION(vehTrailer,FALSE,FALSE)
					//SET_ENTITY_COORDS (vehTrailer, <<-51.98, 3110.83, 24.89>>)
					//SET_ENTITY_HEADING(vehTrailer,46.79)
					FREEZE_ENTITY_POSITION(vehTrailer,TRUE)
				ENDIF
				PRINTSTRING("Cam exit")PRINTNL()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
				GIVE_WEAPON_OBJECT_TO_PED(objWeaponTrevor, PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN,TRUE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				SET_ENTITY_COORDS (PLAYER_PED_ID(), <<-45.7700, 3094.9102, 25.9681>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),23.8494)
				
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN)
					WEAPON_TYPE wpn = WEAPONTYPE_PUMPSHOTGUN
					IF NOT GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wpn)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN,TRUE)
					ENDIF
				ENDIF
				
				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
				
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				IF bExitState = FALSE
					//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					bExitState = TRUE
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ron")
				IF NOT IS_PED_INJURED(budRon.ped)
					SET_ENTITY_COORDS (budRon.ped, <<-42.336231,3088.566650,28.480368>>)
					SET_ENTITY_HEADING(budRon.ped,34.380215)
//					TASK_START_SCENARIO_IN_PLACE(budRon.ped,"WORLD_HUMAN_STAND_IMPATIENT")
					PRINTSTRING("Ron exit")PRINTNL()
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ortega_Trailer")
				IF IS_VEHICLE_DRIVEABLE(vehTrailer)	
					SET_ENTITY_COORDS (vehTrailer, <<-51.98, 3110.83, 24.89>>)
					SET_ENTITY_HEADING(vehTrailer,46.79)
					FREEZE_ENTITY_POSITION(vehTrailer,TRUE)
					PRINTSTRING("Trailer exit")PRINTNL()
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_car")
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
					PRINTSTRING("Vehicle exit")PRINTNL()
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ortega")
				IF NOT IS_PED_INJURED(npcOrtega.ped)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("ThreatenSceneMain")
						ThreatenSceneMain = CREATE_SYNCHRONIZED_SCENE(<< -46.236, 3096.727, 25.513 >>, << 3.000, 0.000, -162.720 >>)
						DETACH_ENTITY(npcOrtega.ped)
						SET_ENTITY_PROOFS(npcOrtega.ped,FALSE,FALSE,FALSE,FALSE,FALSE)
						TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ThreatenSceneMain, "misstrevor1", "ortega_stand_loop_ort", INSTANT_BLEND_IN, INSTANT_BLEND_OUT ,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
						SET_SYNCHRONIZED_SCENE_LOOPED(ThreatenSceneMain,TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(npcOrtega.ped)
						PRINTSTRING("Ortega exit")PRINTNL()
						SET_LABEL_AS_TRIGGERED("ThreatenSceneMain",TRUE)
					ENDIF
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
			AND bClearCutscenArea = TRUE
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			REMOVE_IPL("TRV1_Trail_start")
			REQUEST_IPL("TRV1_Trail_end")
			
			IF b_skipped_mocap
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
		
			REPLAY_STOP_EVENT()
		
			//Setup buddy relationship groups etc.
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, TRUE, FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(FALSE)
			PRINTLN("@@@@@@@@@@@@ SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(FALSE) @@@@@@@@@@@")
			SET_MULTIHEAD_SAFE(FALSE)
			
			i_current_event = 0
			e_section_stage = SECTION_STAGE_SETUP
			bcutsceneplaying = FALSE
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDWHILE
ENDPROC

FUNC BOOL RUN_ACTION_CUT()

	SETTIMERA(0)
	bcutsceneplaying = TRUE
	DISABLE_SELECTOR_THIS_FRAME()

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete
		
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_MrPhilips")
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_BRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MOVE_LEFT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MOVE_RIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_LEFT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_RIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_BEHIND)
		
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_ARRIVE_AT_TRAILER")
			STOP_AUDIO_SCENE("TREVOR_1_ARRIVE_AT_TRAILER")
		ELSE
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_1_PUSH_THE_TRAILER")
				START_AUDIO_SCENE("TREVOR_1_PUSH_THE_TRAILER")
				SET_LABEL_AS_TRIGGERED("TREVOR_1_PUSH_THE_TRAILER",TRUE)
			ENDIF
		ENDIF
		
		IF icutsceneprog > 0
			IF bImpactDialogue = FALSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EEB",CONV_PRIORITY_MEDIUM)
						bImpactDialogue = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_EGA")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EGA",CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("T1M1_EGA",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor.veh)
					SET_PLAYBACK_SPEED(vehTrevor.veh,1.0)
				ENDIF
			ELSE
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor.veh)
					SET_PLAYBACK_SPEED(vehTrevor.veh,0.5)
				ENDIF
			ENDIF
		ENDIF

		SWITCH icutsceneprog
		
			CASE 0  
				// creates inital cam and interp
				IF TRIGGER_MUSIC_EVENT("TRV1_TRAILER_SMASHED")
					CLEAR_PRINTS()
					CLEAR_HELP()
					
					PRINTSTRING("ACTION CUT CASE 0- CAM 1 SET")PRINTNL()
					
					SET_WIDESCREEN_BORDERS(TRUE,0)			
					DESTROY_ALL_CAMS()
					camCutscene = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					camInterp = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					
					ATTACH_CAM_TO_ENTITY(camCutscene, vehTrevor.veh, <<3.5569, -1.6759, 0.2972>>)
					POINT_CAM_AT_ENTITY(camCutscene, vehTrevor.veh, <<1.7662, 0.6973, 0.6989>>)
					SET_CAM_FOV(camCutscene, 27.2793)
					SHAKE_CAM(camCutscene,"ROAD_VIBRATION_SHAKE",2.5)
					SET_CAM_MOTION_BLUR_STRENGTH(camCutscene, 0.2)
					
					ATTACH_CAM_TO_ENTITY(camInterp, vehTrevor.veh, <<0.0629, -3.1695, 1.5953>>)
					POINT_CAM_AT_ENTITY(camInterp, vehTrevor.veh, <<0.0554, -0.1993, 1.1738>>)
					SET_CAM_FOV(camInterp, 34.4262)
					SHAKE_CAM(camInterp,"ROAD_VIBRATION_SHAKE",2.5)
					SET_CAM_MOTION_BLUR_STRENGTH(camInterp, 0.2)

					SET_CAM_ACTIVE_WITH_INTERP(camInterp, camCutscene, 1500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
					SHAKE_CAM(camCutscene,"ROAD_VIBRATION_SHAKE",2.5)
					SHAKE_CAM(camInterp,"ROAD_VIBRATION_SHAKE",2.5)
					REQUEST_VEHICLE_RECORDING(1,"pushtrailer")
					
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"pushtrailer")
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor.veh)
								START_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh,1,"pushtrailer")
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh,719)
							ENDIF
						ENDIF
					ENDIF
					
					SETUP_TRAILER_DAMAGE(TRUE)
					
					SET_TIME_SCALE(0.5)
					ACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_T1_TRAILER_SMASH")
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					UNLOCK_MINIMAP_ANGLE()

					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF

					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
								ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
								TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_04_1st_Hit_Reaction", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
								SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					SETTIMERB(0)
					KILL_ANY_CONVERSATION()
					icutsceneprog++
				ENDIF

			BREAK
			
			CASE 1
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("PANIC IDLE")
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
								IF GET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims) > 0.99
									ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
									TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_05_Panic_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
									SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,TRUE)
									SET_LABEL_AS_TRIGGERED("PANIC IDLE",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF TIMERB() > 3000
					RENDER_SCRIPT_CAMS(FALSE, TRUE,1000)
					SET_WIDESCREEN_BORDERS(FALSE,0)
					SET_TIME_SCALE(1)
					DEACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_T1_TRAILER_SMASH")
					DESTROY_ALL_CAMS()
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh)
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					PRINTSTRING("MINI CUT COMPLETE")
					PRINTNL()	
					CONTROL_FADE_IN(500)
					icutsceneprog=0
					bDisableControls = FALSE
					bcutsceneplaying = FALSE
					
					IF NOT IS_PED_INJURED(npcOrtega.ped)
						IF IS_ENTITY_ATTACHED(npcOrtega.ped)
							SET_ENTITY_PROOFS(npcOrtega.ped,TRUE,TRUE,TRUE,TRUE,TRUE)
						ENDIF
					ENDIF
					
					RETURN TRUE
				ENDIF
				
			BREAK

		ENDSWITCH	

	ENDWHILE
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
/// Play the return to truck cutscene
PROC RUN_RETURN_TO_TRUCK_CUTSCENE()
	
	e_section_stage = SECTION_STAGE_SETUP
	i_current_event = 0
	bcutsceneplaying = TRUE
	b_skipped_mocap = FALSE

	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				b_skipped_mocap = TRUE
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ENDIF
	#ENDIF

	WHILE bcutsceneplaying
	
		WAIT(0)

		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_MrPhilips")

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					bcutsceneplaying = TRUE
					SET_START_AND_END_VECTORS()
					INITALISE_ARRAYS(STAGE_INIT_MISSION)
					REQUEST_CUTSCENE("TRV_1_MCS_2")
					i_current_event++
				ELIF i_current_event = 1
					IF HAS_CUTSCENE_LOADED()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							WAIT(0)
						ENDIF
						
						IF NOT IS_PED_INJURED(budRon.ped)
							REGISTER_ENTITY_FOR_CUTSCENE(budRon.ped, "Ron", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF 
						
						IF NOT IS_PED_INJURED(budWade.ped)
							REGISTER_ENTITY_FOR_CUTSCENE(budWade.ped, "Wade", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							REGISTER_ENTITY_FOR_CUTSCENE(vehTrevor.veh, "Trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						START_CUTSCENE()
	
						//Wait a couple of frames before clearing/warping stuff, so the cutscene is active first.
						WAIT(0)
						
						VEHICLE_INDEX vehPlayer
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
							vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							DELETE_VEHICLE(vehPlayer)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							SET_VEHICLE_DOOR_CONTROL(vehTrevor.veh,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,0)
							SET_VEHICLE_DOOR_CONTROL(vehTrevor.veh,SC_DOOR_FRONT_RIGHT,DT_DOOR_INTACT,0)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							CLEAR_AREA(GET_ENTITY_COORDS(vehTrevor.veh, FALSE),10,TRUE)
						ENDIF
						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						b_skipped_mocap = FALSE
						i_current_event = 0
						e_section_stage = SECTION_STAGE_RUNNING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF NOT b_skipped_mocap
				IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
					b_skipped_mocap = TRUE
				ENDIF
			ENDIF

			IF DOES_ENTITY_EXIST(vehTrevor.veh)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ron")
							IF NOT IS_PED_INJURED(budRon.ped)
								IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
									SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
								ENDIF
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_car")
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
			AND NOT IS_CUTSCENE_ACTIVE()
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			
			IF b_skipped_mocap
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
		
			//Setup buddy relationship groups etc.
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			REMOVE_CUTSCENE()
			i_current_event = 0
			bcutsceneplaying = FALSE
			e_section_stage = SECTION_STAGE_SETUP
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDWHILE
ENDPROC


PROC MANAGE_PTFX()

	FLOAT fCameraShakeAmplitude
	FLOAT fWheelspinValue
	
	//update camera shake values and ptfx evolution
	IF bTriggerPTFX = TRUE OR iStageSection = 9
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
			bTriggerPTFX = TRUE
			fCameraShakeAmplitude =  CLAMP(fCameraShakeAmplitude +@0.2, 0.0, 0.3)
			fWheelspinValue = CLAMP(fWheelspinValue +@0.25, 0.0, 1.0)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 256)
		ELSE
			SET_CONTROL_SHAKE(PLAYER_CONTROL,0,0)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearLeftWheel)
				STOP_PARTICLE_FX_LOOPED(ptfxRearLeftWheel)
			ENDIF
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearRightWheel)
				STOP_PARTICLE_FX_LOOPED(ptfxRearRightWheel)
			ENDIF
			bTriggerPTFX = FALSE
			bPTFXStarted = FALSE
			fCameraShakeAmplitude =  CLAMP(fCameraShakeAmplitude -@0.4, 0.0, 0.3)
			fWheelspinValue = 0.0
		ENDIF
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearLeftWheel)
			STOP_PARTICLE_FX_LOOPED(ptfxRearLeftWheel)
		ENDIF
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearRightWheel)
			STOP_PARTICLE_FX_LOOPED(ptfxRearRightWheel)
		ENDIF	
	ENDIF
	
	IF DOES_CAM_EXIST(cutscene_cam)
		SHAKE_CAM(cutscene_cam,"HAND_SHAKE",fCameraShakeAmplitude)
	ENDIF
	
	IF bTriggerPTFX
		IF bPTFXStarted = FALSE
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				
				IF HAS_PTFX_ASSET_LOADED()
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearLeftWheel)
					AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearRightWheel)
						ptfxRearLeftWheel = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev1_wheelspin_dirt", vehTrevor.veh, vRearLeftWheelOffset, vPTFXRotation)				
						ptfxRearRightWheel = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev1_wheelspin_dirt", vehTrevor.veh, vRearRightWheelOffset, vPTFXRotation)
						bPTFXStarted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearLeftWheel)
			AND DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearRightWheel)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxRearLeftWheel, "wheelspin", fWheelspinValue)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxRearRightWheel, "wheelspin", fWheelspinValue)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MANAGE_TRAILER_SOUNDS()
	
	SWITCH iTrailerSounds 
	
		CASE 0
			IF REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACTS")
			AND REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACTS_1")
			AND REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACTS_2")
				iTrailerSounds ++
			ENDIF
		BREAK
		
		CASE 1
//			IF REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACT_MASTER_A")
//			AND REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACT_MASTER_B")
//			AND REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACT_MASTER_C")
			iTrailerSounds ++			
//			ENDIF
		BREAK
		
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(vehTrailer)
				IF IS_ENTITY_AT_COORD(vehTrailer,<<-35.57, 3050.70, 39.65>>,<<4,4,4>>)
					PLAY_SOUND_FROM_ENTITY(-1, "TREVOR_1_TRAILER_IMPACT_MASTER_B", vehTrailer)
					PRINTSTRING("TREVOR_1_TRAILER_IMPACT_MASTER_B")PRINTNL()
					iTrailerSounds ++			
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC DELETE_DOGS()

IF DOES_ENTITY_EXIST(gangDog[0].ped)
	IF NOT IS_PED_INJURED(gangDog[0].ped)
		IF NOT IS_ENTITY_ON_SCREEN(gangDog[0].ped)
			DELETE_PED(gangDog[0].ped)
		ENDIF
	ENDIF
ENDIF

IF DOES_ENTITY_EXIST(gangDog[1].ped)
	IF NOT IS_PED_INJURED(gangDog[1].ped)
		IF NOT IS_ENTITY_ON_SCREEN(gangDog[1].ped)
			DELETE_PED(gangDog[1].ped)
		ENDIF
	ENDIF
ENDIF

IF DOES_ENTITY_EXIST(gangDog[2].ped)
	IF NOT IS_PED_INJURED(gangDog[2].ped)
		IF NOT IS_ENTITY_ON_SCREEN(gangDog[2].ped)
			DELETE_PED(gangDog[2].ped)
		ENDIF
	ENDIF
ENDIF


ENDPROC

PROC MANAGE_BIKER_FLEE_HELP()
	
	IF NOT IS_PED_INJURED(gangFlee[0].ped)
	OR NOT IS_PED_INJURED(gangFlee[1].ped)
	OR NOT IS_PED_INJURED(gangFlee[2].ped)
	OR NOT IS_PED_INJURED(gangFlee[3].ped)
	OR NOT IS_PED_INJURED(gangFlee[4].ped)
		IF MANAGE_MY_TIMER(iBikerFleeHelp,500)
			IF gangFlee[0].bFriendly
				IF NOT IS_HELP_MESSAGE_ON_SCREEN()
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV_RUN2")
					gangFlee[0].bFriendly = FALSE
					iBikerFleeHelp = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF NOT IS_HELP_MESSAGE_ON_SCREEN()
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV_RUN1")

					gangFlee[0].bFriendly = TRUE
					iBikerFleeHelp = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		
	ENDIF

ENDPROC

PROC UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP_FLEE()

	INT i
	
	FOR i = 0 TO ( COUNT_OF(gangEnemy) - 1 )
		
		IF NOT IS_PED_INJURED(gangEnemy[i].ped)
			IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),gangEnemy[i].ped) < 100
				UPDATE_AI_PED_BLIP_FLASHING(gangEnemy[i].ped, gangEnemy[i].EnemyBlipData)
				//UPDATE_AI_PED_BLIP(gangEnemy[i].ped, gangEnemy[i].EnemyBlipData,-1,NULL,TRUE)
			ELSE
				CLEANUP_AI_PED_BLIP(gangEnemy[i].EnemyBlipData)
			ENDIF
		ELSE
			CLEANUP_AI_PED_BLIP(gangEnemy[i].EnemyBlipData)
		ENDIF
	ENDFOR

ENDPROC

PROC MANAGE_BIKER_FLEE_DIALOGUE()
	IF NOT IS_PED_INJURED(pedAshley)
		IF NOT IS_AMBIENT_SPEECH_PLAYING(pedAshley)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("WIMPER")
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedAshley, "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
				iAshleyWhimperTimer = GET_GAME_TIMER()
				SET_LABEL_AS_TRIGGERED("WIMPER",TRUE)
			ELSE
				IF MANAGE_MY_TIMER(iAshleyWhimperTimer,GET_RANDOM_INT_IN_RANGE(0,100))
					SET_LABEL_AS_TRIGGERED("WIMPER",FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_WADE_LEAVING()
	IF DOES_ENTITY_EXIST(budWade.ped)
		REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(1834.35645, 1442.53589,2189.51514, 4781.18555)
	ENDIF
	
	IF bTriggerWadeLeaving = TRUE
		IF DOES_ENTITY_EXIST(budWade.ped)
			IF NOT IS_PED_INJURED(budWade.ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),budWade.ped) > 100
					IF NOT IS_ENTITY_ON_SCREEN(budWade.ped)
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),budWade.ped) > 200
							IF NOT IS_ENTITY_ON_SCREEN(budWade.ped)
								IF DOES_ENTITY_EXIST(vehWadesBike)
									DELETE_VEHICLE(vehWadesBike)
									SET_MODEL_AS_NO_LONGER_NEEDED(HEXER)
								ENDIF
								DELETE_PED(budWade.ped)
								SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bSkipped = FALSE
		IF NOT DOES_ENTITY_EXIST(vehWadesBike)
			SETUP_MISSION_REQUIREMENT(REQ_WADES_BIKE,<<0,0,0>>)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF NOT IS_PED_INJURED(budWade.ped)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(SceneCutscene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(SceneCutscene) < 0.95
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							//FREEZE_ENTITY_POSITION(vehTrevor.veh,TRUE)
						ELSE
							FREEZE_ENTITY_POSITION(vehTrevor.veh,FALSE)
						ENDIF
					ELSE
						STOP_SYNCHRONIZED_ENTITY_ANIM(budWade.ped,SLOW_BLEND_OUT,TRUE)
						DETACH_ENTITY(budWade.ped,TRUE)
						FREEZE_ENTITY_POSITION(vehTrevor.veh,FALSE)
					ENDIF
				ELSE
					FREEZE_ENTITY_POSITION(vehTrevor.veh,FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF NOT IS_PED_INJURED(budWade.ped)
				IF iStageSection > 5
					IF bTriggerWadeLeaving = FALSE
						IF ARE_NODES_LOADED_FOR_AREA(1834.35645, 1442.53589,2189.51514, 4781.18555)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(SceneCutscene)
								IF GET_SYNCHRONIZED_SCENE_PHASE(SceneCutscene) > 0.95
									STOP_SYNCHRONIZED_ENTITY_ANIM(budWade.ped,SLOW_BLEND_OUT,TRUE)
									DETACH_ENTITY(budWade.ped,TRUE)
									OPEN_SEQUENCE_TASK(seqSequence)
									IF HAS_LABEL_BEEN_TRIGGERED("WADE BIKE RIGHT")
										IF IS_VEHICLE_DRIVEABLE(vehWadesBike)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<41.2664, 3615.5132, 38.6250>>,1.0,DEFAULT_TIME_NEVER_WARP)
											TASK_ENTER_VEHICLE(NULL,vehWadesBike,DEFAULT_TIME_NEVER_WARP)
											TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehWadesBike, << 1982.7225, 3831.4949, 31.3972 >>,30,DRIVINGSTYLE_NORMAL,HEXER, DRIVINGMODE_AVOIDCARS|DF_UseWanderFallbackInsteadOfStraightLine,20,1)
											
											TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
										ELSE
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-6.8398, 3611.4167, 40.6130>>,1.0,DEFAULT_TIME_NEVER_WARP)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-229.3429, 3884.1604, 36.4223>>,2.0,DEFAULT_TIME_NEVER_WARP)
											TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
										ENDIF
									ELIF HAS_LABEL_BEEN_TRIGGERED("WADE BIKE LEFT")
										IF IS_VEHICLE_DRIVEABLE(vehWadesBike)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<104.0991, 3592.6980, 38.7392>>,1.0,DEFAULT_TIME_NEVER_WARP)
											TASK_ENTER_VEHICLE(NULL,vehWadesBike,DEFAULT_TIME_NEVER_WARP)
											TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehWadesBike, << 1982.7225, 3831.4949, 31.3972 >>,30,DRIVINGSTYLE_NORMAL,HEXER, DRIVINGMODE_AVOIDCARS|DF_UseWanderFallbackInsteadOfStraightLine,20,1)
											TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
										ELSE
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-6.8398, 3611.4167, 40.6130>>,1.0,DEFAULT_TIME_NEVER_WARP)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-229.3429, 3884.1604, 36.4223>>,2.0,DEFAULT_TIME_NEVER_WARP)
											TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
										ENDIF
									ELSE
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-6.8398, 3611.4167, 40.6130>>,1.0,DEFAULT_TIME_NEVER_WARP)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-229.3429, 3884.1604, 36.4223>>,2.0,DEFAULT_TIME_NEVER_WARP)
										TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
									ENDIF
									CLOSE_SEQUENCE_TASK(seqSequence)
									TASK_PERFORM_SEQUENCE(budWade.ped, seqSequence)
									CLEAR_SEQUENCE_TASK(seqSequence)
									SET_PED_KEEP_TASK(budWade.ped, TRUE)
									SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
									bTriggerWadeLeaving = TRUE
								ENDIF
							ELIF NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
								OPEN_SEQUENCE_TASK(seqSequence)
								IF HAS_LABEL_BEEN_TRIGGERED("WADE BIKE RIGHT")
									IF IS_VEHICLE_DRIVEABLE(vehWadesBike)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<41.2664, 3615.5132, 38.6250>>,1.0,DEFAULT_TIME_NEVER_WARP)
										TASK_ENTER_VEHICLE(NULL,vehWadesBike,DEFAULT_TIME_NEVER_WARP)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehWadesBike, << 1982.7225, 3831.4949, 31.3972 >>,30,DRIVINGSTYLE_NORMAL,HEXER,DF_UseWanderFallbackInsteadOfStraightLine,20,1)
										
										TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
									ELSE
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-6.8398, 3611.4167, 40.6130>>,1.0,DEFAULT_TIME_NEVER_WARP)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-229.3429, 3884.1604, 36.4223>>,2.0,DEFAULT_TIME_NEVER_WARP)
										TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
									ENDIF
								ELIF HAS_LABEL_BEEN_TRIGGERED("WADE BIKE LEFT")
									IF IS_VEHICLE_DRIVEABLE(vehWadesBike)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<104.0991, 3592.6980, 38.7392>>,1.0,DEFAULT_TIME_NEVER_WARP)
										TASK_ENTER_VEHICLE(NULL,vehWadesBike,DEFAULT_TIME_NEVER_WARP)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehWadesBike, << 1982.7225, 3831.4949, 31.3972 >>,30,DRIVINGSTYLE_NORMAL,HEXER,DF_UseWanderFallbackInsteadOfStraightLine,20,1)
										TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
									ELSE
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-6.8398, 3611.4167, 40.6130>>,1.0,DEFAULT_TIME_NEVER_WARP)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-229.3429, 3884.1604, 36.4223>>,2.0,DEFAULT_TIME_NEVER_WARP)
										TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
									ENDIF
								ELSE
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-6.8398, 3611.4167, 40.6130>>,1.0,DEFAULT_TIME_NEVER_WARP)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-229.3429, 3884.1604, 36.4223>>,2.0,DEFAULT_TIME_NEVER_WARP)
									TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
								ENDIF
								CLOSE_SEQUENCE_TASK(seqSequence)
								TASK_PERFORM_SEQUENCE(budWade.ped, seqSequence)
								CLEAR_SEQUENCE_TASK(seqSequence)
								SET_PED_KEEP_TASK(budWade.ped, TRUE)
								SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
								bTriggerWadeLeaving = TRUE
							ENDIF
						ENDIF
					ELSE
						
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(budWade.ped,PLAYER_PED_ID(),TRUE)
							IF GET_SCRIPT_TASK_STATUS(budWade.ped,SCRIPT_TASK_SMART_FLEE_PED)<> PERFORMING_TASK
								//1600455 
								//Stop Wade getting stuck in his Synch Scene 
								STOP_SYNCHRONIZED_ENTITY_ANIM(budWade.ped,SLOW_BLEND_OUT,TRUE)
								DETACH_ENTITY(budWade.ped,TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped,FALSE)
								TASK_SMART_FLEE_PED(budWade.ped,PLAYER_PED_ID(), 2000, -1)
							ENDIF
						ENDIF
						IF NOT HAS_LABEL_BEEN_TRIGGERED("WADE MUMBLE")
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(budWade.ped,"T1M1_FNAA","LOST1",SPEECH_PARAMS_FORCE)
							SET_LABEL_AS_TRIGGERED("WADE MUMBLE",TRUE)
							iWadeGrumbleTimer = GET_GAME_TIMER()
						ELSE
							IF MANAGE_MY_TIMER(iWadeGrumbleTimer,8000)
								SET_LABEL_AS_TRIGGERED("WADE MUMBLE",FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

BOOL bCLeanUpShootOut= FALSE

PROC CLEANUP_SHOOTOUT()
	
	IF bCleanUpShootOut = FALSE
		SET_MODEL_AS_NO_LONGER_NEEDED(Prop_GasCyl_02A)
		SET_MODEL_AS_NO_LONGER_NEEDED(Prop_GasCyl_02A)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_C_ROTTWEILER)
		SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnLost1)
		SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnLost2)
		SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnGangVan)
		SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnBike)
		SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnGangTruck)
		
		REMOVE_VEHICLE_RECORDING(200, "trev1chasedw")
		REMOVE_VEHICLE_RECORDING(201, "trev1chasedw")
		REMOVE_VEHICLE_RECORDING(202, "trev1chasedw")
		REMOVE_VEHICLE_RECORDING(203, "trev1chasedw")
		REMOVE_VEHICLE_RECORDING(204, "trev1chasedw")
		REMOVE_VEHICLE_RECORDING(205, "trev1chasedw")
		
		REMOVE_VEHICLE_RECORDING(1, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(9, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(11, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(12, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(13, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(14, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(15, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(16, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(17, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(18, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(19, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(20, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(21, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(22, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(23, "Trevor1dw")
		REMOVE_VEHICLE_RECORDING(2, "t1b")
		REMOVE_VEHICLE_RECORDING(3, "t1b")
		REMOVE_VEHICLE_RECORDING(4, "t1b")
		REMOVE_VEHICLE_RECORDING(5, "t1b")
		REMOVE_VEHICLE_RECORDING(6, "t1b")
		REMOVE_VEHICLE_RECORDING(7, "t1b")
		REMOVE_VEHICLE_RECORDING(8, "t1b")
		REMOVE_VEHICLE_RECORDING(9, "t1b")
		REMOVE_VEHICLE_RECORDING(10, "t1b")
		
		REMOVE_VEHICLE_ASSET(HEXER)
		SET_MODEL_AS_NO_LONGER_NEEDED(GBURRITO)
		SET_MODEL_AS_NO_LONGER_NEEDED(HEXER)
		SET_MODEL_AS_NO_LONGER_NEEDED(DLOADER)
		SET_MODEL_AS_NO_LONGER_NEEDED(REBEL)
		SET_MODEL_AS_NO_LONGER_NEEDED(BIFF)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_GASCYL_01A)
		
		bCLeanUpShootOut = TRUE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(<<66.0912, 3632.1165, 38.6100>>,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 120
		
		REPEAT COUNT_OF(gangEnemy) iCounter
			IF DOES_ENTITY_EXIST(gangEnemy[iCounter].ped)
				IF NOT IS_ENTITY_ON_SCREEN(gangEnemy[iCounter].ped)
					SET_PED_AS_NO_LONGER_NEEDED(gangEnemy[iCounter].ped)
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(gangDog) iCounter
			IF DOES_ENTITY_EXIST(gangDog[iCounter].ped)
				IF NOT IS_ENTITY_ON_SCREEN(gangDog[iCounter].ped)
					SET_PED_AS_NO_LONGER_NEEDED(gangDog[iCounter].ped)
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(vehTrailerPark) iCounter
			IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[iCounter].veh)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehTrailerPark[iCounter].veh, FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 120
					IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[iCounter].veh)
						DELETE_VEHICLE(vehTrailerPark[iCounter].veh)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF IS_VEHICLE_DRIVEABLE(vehTruck.veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTruck.veh)
				DELETE_VEHICLE(vehTruck.veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehBike[0].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehBike[0].veh)
				DELETE_VEHICLE(vehBike[0].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehBike[1].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehBike[1].veh)
				DELETE_VEHICLE(vehBike[1].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[5].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[5].veh)
				//-- Bike by the trailer
				DELETE_VEHICLE(vehTrailerPark[5].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[6].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[6].veh)
				//-- In front of stage
				DELETE_VEHICLE(vehTrailerPark[6].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[7].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[7].veh)
				//-- In front of stage
				DELETE_VEHICLE(vehTrailerPark[7].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[15].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[15].veh)
				//--Convoy of lhs bikes
				DELETE_VEHICLE(vehTrailerPark[15].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[16].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[16].veh)
				//--Convoy of lhs bikes
				DELETE_VEHICLE(vehTrailerPark[16].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[17].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[17].veh)
				//--Convoy of lhs bikes
				DELETE_VEHICLE(vehTrailerPark[17].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[20].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[20].veh)
				//--Convoy of rhs bikes
				DELETE_VEHICLE(vehTrailerPark[20].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[21].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[21].veh)
				//--Convoy of rhs bikes
				DELETE_VEHICLE(vehTrailerPark[21].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[22].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[22].veh)
				//--Convoy of rhs bikes
				DELETE_VEHICLE(vehTrailerPark[22].veh)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[24].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[24].veh)
				//--Convoy 2 of rhs bikes
				DELETE_VEHICLE(vehTrailerPark[24].veh)
			ENDIF
			
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailerPark[25].veh)
			IF NOT IS_ENTITY_ON_SCREEN(vehTrailerPark[25].veh)
				//--Convoy 2 of rhs bikes
				DELETE_VEHICLE(vehTrailerPark[25].veh)
			ENDIF
		ENDIF
		
		REPEAT COUNT_OF (oFuelCan) iCounter
			IF DOES_ENTITY_EXIST(oFuelCan[iCounter])
				DELETE_OBJECT(oFuelCan[iCounter])
			ENDIF
		ENDREPEAT
	ENDIF
	
	
ENDPROC

TEXT_LABEL_15 sConvo

CAMERA_INDEX TempCam

BOOL bTREVOR_1_DRIVE_TO_TRAILER
BOOL bTREVOR_1_SHOOTOUT_BIKERS_FLEE

FUNC BOOL stageDriveToCaravan()
	
	IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_RAYFIRE")
		STOP_AUDIO_SCENE("TREVOR_1_RAYFIRE")
	ENDIF
	
	PRINTSTRING("iStageSection:")PRINTINT(iStageSection)PRINTNL()
	
	IF bSkipped = FALSE
		IF bStageSetup	
			MANAGE_WADE_LEAVING()
		ENDIF
	ENDIF
	
	//MANAGE_RAYFIRE_EXPLOSION()

	IF bSkipped = FALSE
		IF NOT bTREVOR_1_SHOOTOUT_BIKERS_FLEE
			IF IS_AUDIO_SCENE_ACTIVE("Trevor_1_Shootout_Start")
				STOP_AUDIO_SCENE("Trevor_1_Shootout_Start")
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					START_AUDIO_SCENE("TREVOR_1_SHOOTOUT_BIKERS_FLEE")
					bTREVOR_1_SHOOTOUT_BIKERS_FLEE = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iStageSection > 6
		IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_SHOOTOUT_BIKERS_FLEE")
			STOP_AUDIO_SCENE("TREVOR_1_SHOOTOUT_BIKERS_FLEE")
		ELSE
			IF bTREVOR_1_DRIVE_TO_TRAILER = FALSE
				START_AUDIO_SCENE("TREVOR_1_DRIVE_TO_TRAILER")
				bTREVOR_1_DRIVE_TO_TRAILER = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-25.121183,3033.677734,47.191833>>,<<150,150,150>>)
		SETUP_TRAILER()
	ENDIF
		
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP_FLEE()

	IF iStageSection > 10
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(fThisValue)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(fThisValue)
	ENDIF
	
	IF iStageSection < 6
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),vehTrevor.veh,<<25,25,25>>)
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.8)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
			IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_GT11")
				CLEAR_PRINTS()
			ENDIF
		ENDIF
	ENDIF
	
	CLEANUP_SHOOTOUT()
	DELETE_DOGS()
	MANAGE_FLEEING_PEDS()
	MANAGE_BIKER_BEHAVIOR()
	
	IF HAS_LABEL_BEEN_TRIGGERED("T1M1_CUT1")
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			sConvo = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT() 
			IF ARE_STRINGS_EQUAL(sConvo, "T1M1_CUT1")
				IF NOT IS_PED_INJURED(budWade.ped)
					IF GET_DISTANCE_BETWEEN_ENTITIES(budWade.ped,PLAYER_PED_ID()) > 40.0
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_LABEL_BEEN_TRIGGERED("T1M1_CUT1")
		IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_WADEBO")
			IF NOT IS_PED_INJURED(budWade.ped)
				IF GET_DISTANCE_BETWEEN_ENTITIES(budWade.ped,PLAYER_PED_ID()) < 30.0
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_WADEBO",CONV_PRIORITY_HIGH)
								REPLAY_RECORD_BACK_FOR_TIME(8.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
								SET_LABEL_AS_TRIGGERED("T1M1_WADEBO",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bStageSetup	
		
		REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Drive to the trailer")
		//--Stop checking for headshots.
		RESET_MISSION_STATS_ENTITY_WATCH()
		RESET_NEED_LIST()
		MANAGE_LOADING_NEED_LIST()
	
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,FALSE)
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(HEXER,TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GBURRITO,TRUE)
		
		SET_START_AND_END_VECTORS()
		CLEAR_TRIGGERED_LABELS()
		INITALISE_ARRAYS(STAGE_CARAVAN_SMASH)
		
		IF DOES_ENTITY_EXIST(vehTrailer)
			DELETE_VEHICLE(vehTrailer)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			MODIFY_VEHICLE_TOP_SPEED(vehTrevor.veh,0)
		ENDIF
		
		REMOVE_IPL("TRV1_Trail_end")
		REQUEST_IPL("TRV1_Trail_start")

		REQUEST_MODEL(PROPTRAILER)
		REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_ORTEGA))
		REQUEST_VEHICLE_RECORDING(1,"trev1")
		IF NOT IS_PED_INJURED(budWade.ped)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(budWade.ped)
		ENDIF
		bSetupCaravan = TRUE	
		iStageSection = 0

		bDoModelSwap = FALSE
		bStageSetup = TRUE
		bStartForce = TRUE
		bCLeanUpShootOut= FALSE
		bTREVOR_1_DRIVE_TO_TRAILER = FALSE
		bTREVOR_1_SHOOTOUT_BIKERS_FLEE = FALSE
		SET_WANTED_LEVEL_MULTIPLIER(0.1)
		
		bFrag1 = FALSE
		bFrag2 = FALSE
		bFrag3 = FALSE
		bDoFails = TRUE
		bGetBackInTruckGod = FALSE
		bTrevorSHoutsAtFleeingBikers = FALSE
		
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			SET_VEHICLE_STRONG(vehTrevor.veh,FALSE)
		ENDIF
		
		//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
		IF NOT bTrevorSplashBackEffectOn
			APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
			bTrevorSplashBackEffectOn = TRUE
		ENDIF	
		
		safefadeInAndWait()	
		
//---------¦ UPDATE ¦---------		
	ELSE	
		SWITCH iStageSection
				
			CASE 0
				
				SET_VEHICLE_MODEL_IS_SUPPRESSED(HEXER,TRUE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(GBURRITO,TRUE)
				SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_01,TRUE)
				SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_02,TRUE)
				SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_03,TRUE)
				
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-17.546822,3042.975342,46.947666>> - <<20.000000,20.000000,10.000000>>, <<-17.546822,3042.975342,46.947666>> + <<20.000000,20.000000,10.000000>>,FALSE)
				CLEAR_AREA(<<-17.546822,3042.975342,46.947666>>,40.0, TRUE)
				
				IF DOES_BLIP_EXIST(budWade.blip)
					REMOVE_BLIP(budWade.blip)
				ENDIF
				
				IF DOES_BLIP_EXIST(budRon.blip)
					REMOVE_BLIP(budRon.blip)
				ENDIF
				
				REMOVE_VEHICLE_RECORDING(1, "trevone")
				REMOVE_VEHICLE_RECORDING(2, "trevone")
				REMOVE_VEHICLE_RECORDING(3, "trevone")
				REMOVE_VEHICLE_RECORDING(4, "trevone")
				REMOVE_VEHICLE_RECORDING(200, "trev1chasedw")
				REMOVE_VEHICLE_RECORDING(201, "trev1chasedw")
				REMOVE_VEHICLE_RECORDING(202, "trev1chasedw")
				REMOVE_VEHICLE_RECORDING(203, "trev1chasedw")
				REMOVE_VEHICLE_RECORDING(204, "trev1chasedw")
				REMOVE_VEHICLE_RECORDING(205, "trev1chasedw")
				
				IF bSkipped = FALSE
					#IF IS_DEBUG_BUILD
						DONT_DO_J_SKIP(sLocatesData)
					#ENDIF
					
					IF NOT IS_PED_INJURED(budWade.ped)
					AND NOT IS_PED_INJURED(budRon.ped)
						//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped,TRUE)
						//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped,TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped,FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped,FALSE)
					ENDIF

					IF bGetBackInTruckGod
						REMOVE_IPL("TRV1_Trail_end")
						REQUEST_IPL("TRV1_Trail_start")
						SET_TIME_SCALE(1.0)
						i_current_event = 0
						SET_RADAR_ZOOM(0)

						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
						ENDIF
						
						SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),FALSE,-1)
						
						SET_TIME_SCALE(1.0)
						iTrailerSounds = 0
						REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_ORTEGA))
						REMOVE_CUTSCENE()
						REQUEST_ANIM_DICT("misstrevor1trv_1_mcs_2")
						//REQUEST_CUTSCENE("TRV_1_MCS_2")
						
						INT iDog
						
						REPEAT COUNT_OF(gangDog) iDog
							IF DOES_BLIP_EXIST(gangDog[iDog].blipPed)
								PRINTSTRING("REMOVING DOG BLIP FOR:")PRINTINT(iDog)PRINTNL()
								REMOVE_BLIP(gangDog[iDog].blipPed)
							ENDIF
						ENDREPEAT
						
						PRINTSTRING("JUMPING TO NEXT STAGE")PRINTNL()
						bGetBackInTruckTask = FALSE
						iStageSection++
					ELSE
						IF NOT bTrevorSHoutsAtFleeingBikers
							//1600455
							KILL_FACE_TO_FACE_CONVERSATION()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DZA",CONV_PRIORITY_HIGH)
									bTrevorSHoutsAtFleeingBikers = TRUE
								ENDIF
							ENDIF
						ELSE
							IF NOT bGetBackInTruckGod 
								IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
									IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
											PRINT_GOD_TEXT("TRV_GT11")
											IF NOT DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
												sLocatesData.vehicleBlip = CREATE_BLIP_FOR_VEHICLE(vehTrevor.veh,FALSE)
											ENDIF
											bGetBackInTruckGod = TRUE
											PRINTSTRING("TRV_GT11 PRINTED")PRINTNL()
										ENDIF
									ELSE
										bGetBackInTruckGod = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(budWade.ped)
						SET_ENTITY_LOD_DIST(budWade.ped,80)
					ENDIF
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_DRIVE_TRAILER_RT")
						IF TRIGGER_MUSIC_EVENT("TRV1_DRIVE_TRAILER_RT")
							SET_LABEL_AS_TRIGGERED("TRV1_DRIVE_TRAILER_RT",TRUE)
						ENDIF
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_RADIO_TO_STATION_NAME("OFF")
					ENDIF
					SET_VEHICLE_MODEL_IS_SUPPRESSED(HEXER,TRUE)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(GBURRITO,TRUE)
					SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_01,TRUE)
					SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_02,TRUE)
					SET_PED_MODEL_IS_SUPPRESSED(G_M_Y_LOST_03,TRUE)
					safefadeInAndWait()	
					SET_RADAR_ZOOM(0)
					bGoToSpeach = FALSE
					SET_LABEL_AS_TRIGGERED("WADE LEAVE",TRUE)
					iStageSection = 6
				ENDIF
			BREAK
			
			CASE 1
				REQUEST_ANIM_DICT("misstrevor1trv_1_mcs_2")
				IF HAS_ANIM_DICT_LOADED("misstrevor1trv_1_mcs_2")
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF NOT IS_PED_INJURED(budWade.ped)
						AND NOT IS_PED_INJURED(budRon.ped)
							IF IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
							AND IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
								IF NOT IS_PED_INJURED(budWade.ped)
									REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_ORTEGA))
									SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_01)
									SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_LOST_02)
									SET_MODEL_AS_NO_LONGER_NEEDED(HEXER)
									SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
									SET_MODEL_AS_NO_LONGER_NEEDED(GBURRITO)
									SET_MODEL_AS_NO_LONGER_NEEDED(REBEL)
									
									IF DOES_BLIP_EXIST(budWade.blip)
										REMOVE_BLIP(budWade.blip)
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
								ENDIF
								
								REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_ORTEGA))
								
								iStageSection++
							ELSE
								
								IF NOT bGetBackInTruckTask
									IF DOES_ENTITY_EXIST(budWade.ped)
									AND IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehTrevor.veh) < 50
											IF NOT IS_ENTITY_OCCLUDED(vehTrevor.veh)
												IF NOT IS_PED_INJURED(budWade.ped)
												AND NOT IS_PED_INJURED(budRon.ped)
													IF IS_ENTITY_ON_SCREEN(budWade.ped)
													OR IS_ENTITY_ON_SCREEN(budRon.ped)
													OR IS_ENTITY_ON_SCREEN(vehTrevor.veh)
														KILL_FACE_TO_FACE_CONVERSATION()
														CLEAR_PRINTS()
														IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
															IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EEA",CONV_PRIORITY_MEDIUM)
																IF NOT IS_PED_INJURED(budWade.ped)
																	IF NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
																		//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
																		
																		OPEN_SEQUENCE_TASK(seqSequence)
																			IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(budWade.ped, vehTrevor.veh, VS_BACK_LEFT)
																				TASK_ENTER_VEHICLE(NULL,vehTrevor.veh,240000,VS_BACK_LEFT)
																			ELSE
																				TASK_ENTER_VEHICLE(NULL,vehTrevor.veh,240000,VS_BACK_RIGHT)
																			ENDIF
																		CLOSE_SEQUENCE_TASK(seqSequence)
																		TASK_PERFORM_SEQUENCE(budWade.ped, seqSequence)
																		CLEAR_SEQUENCE_TASK(seqSequence)
																		REMOVE_PED_FROM_GROUP(budWade.ped)
																	ENDIF
																ENDIF
																IF NOT IS_PED_INJURED(budRon.ped)
																	IF NOT IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
																		//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
																		OPEN_SEQUENCE_TASK(seqSequence)
																			TASK_PAUSE(NULL,800)
																			TASK_ENTER_VEHICLE(NULL,vehTrevor.veh,240000,VS_FRONT_RIGHT)
																		CLOSE_SEQUENCE_TASK(seqSequence)
																		TASK_PERFORM_SEQUENCE(budRon.ped, seqSequence)
																		CLEAR_SEQUENCE_TASK(seqSequence)
																		
																		REMOVE_PED_FROM_GROUP(budRon.ped)
																	ENDIF
																ENDIF
																bGetBackInTruckTask = TRUE
															ENDIF
														ENDIF
													ELSE
														IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
															KILL_FACE_TO_FACE_CONVERSATION()
															CLEAR_PRINTS()
															IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
																IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EEA",CONV_PRIORITY_MEDIUM)
																	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehTrevor.veh) > 5
																		IF NOT IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
																			SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
																		ENDIF
																		REMOVE_PED_FROM_GROUP(budRon.ped)
																		
																		IF NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
																			SET_PED_INTO_VEHICLE(budWade.ped,vehTrevor.veh,VS_BACK_LEFT)
																		ENDIF
																		REMOVE_PED_FROM_GROUP(budWade.ped)
																		bGetBackInTruckTask = TRUE
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF		
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
			BREAK
			
			CASE 2
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_INJURED(budWade.ped)
					AND NOT IS_PED_INJURED(budRon.ped)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
						REMOVE_PED_FROM_GROUP(budRon.ped)
						REMOVE_PED_FROM_GROUP(budWade.ped)
						TASK_LOOK_AT_ENTITY(budRon.ped,PLAYER_PED_ID(),8000,SLF_WHILE_NOT_IN_FOV)
//						IF GET_PED_IN_VEHICLE_SEAT(vehTrevor.veh, VS_BACK_LEFT) = budWade.ped
						IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(budWade.ped, vehTrevor.veh, VS_BACK_LEFT)
							CLEAR_PED_TASKS_IMMEDIATELY(budWade.ped)
	//						CLEAR_PED_TASKS_IMMEDIATELY(budRon.ped)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(budWade.ped)
	//						FORCE_PED_AI_AND_ANIMATION_UPDATE(budRon.ped)
							SceneCutscene = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(SceneCutscene,vehTrevor.veh,-1)
							SET_ENTITY_NO_COLLISION_ENTITY(budWade.ped,vehTrevor.veh,FALSE)
							TASK_SYNCHRONIZED_SCENE (budWade.ped, SceneCutscene, "misstrevor1trv_1_mcs_2", "wait_enter_wade", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
							SET_SYNCHRONIZED_SCENE_LOOPED(SceneCutscene,FALSE)
						ENDIF
						bDoShoutDialogue = TRUE
						iStageSection++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),vehTrevor.veh,<<30,30,30>>)
						//IF GET_SYNCHRONIZED_SCENE_PHASE(SceneCutscene) > 0.99
						CLEAR_PRINTS()
						iStageSection++
						//ENDIF
					ELSE
						IF bDoShoutDialogue = TRUE
							
						ELSE
							IF bDoShoutDialogue = FALSE
								IF MANAGE_MY_TIMER(iTimer,16000)
									bDoShoutDialogue = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_INJURED(budWade.ped)
					AND NOT IS_PED_INJURED(budRon.ped)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
						IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(budWade.ped, vehTrevor.veh, VS_BACK_LEFT)
						AND IS_VEHICLE_ON_ALL_WHEELS(vehTrevor.veh)
							SceneCutscene = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(SceneCutscene,vehTrevor.veh,-1)
							SET_ENTITY_NO_COLLISION_ENTITY(budWade.ped,vehTrevor.veh,FALSE)
							TASK_SYNCHRONIZED_SCENE (budWade.ped, SceneCutscene, "misstrevor1trv_1_mcs_2", "jump_out_wade", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT )
						ELSE
							TASK_LEAVE_ANY_VEHICLE(budWade.ped, 0, ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
						ENDIF
						SET_LABEL_AS_TRIGGERED("WADE LEAVE",TRUE)
						iStageSection++
					ENDIF
				ENDIF
			BREAK

			CASE 5
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV1_TRUCK")
					IF TRIGGER_MUSIC_EVENT("TRV1_TRUCK")
						SET_LABEL_AS_TRIGGERED("TRV1_TRUCK", TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_CUT1")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						ADD_PED_FOR_DIALOGUE(structConvoPeds,4,budWade.ped,"WADE")
						ADD_PED_FOR_DIALOGUE(structConvoPeds,2,PLAYER_PED_ID(),"TREVOR")
						ADD_PED_FOR_DIALOGUE(structConvoPeds,3,budRon.ped,"NervousRon")
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_CUT1",CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("T1M1_CUT1",TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_INJURED(budWade.ped)
						IF NOT IS_PED_INJURED(budWade.ped)
						AND NOT IS_PED_INJURED(budRon.ped)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(SceneCutscene)
								IF GET_SYNCHRONIZED_SCENE_PHASE(SceneCutscene) > 0.84
								OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
									IF NOT IS_PED_INJURED(budRon.ped)
										IF NOT IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
											SET_PED_AS_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
										ENDIF
									ENDIF
									
									IF NOT IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
										CLEAR_PED_TASKS_IMMEDIATELY(budRon.ped)
										SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
										REMOVE_PED_FROM_GROUP(budRon.ped)
									ENDIF	
									
									REMOVE_PED_FROM_GROUP(budRon.ped)
									TASK_CLEAR_LOOK_AT(budRon.ped)
									
									IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
										SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
										SET_ENTITY_NO_COLLISION_ENTITY(budWade.ped,vehTrevor.veh,TRUE)
										iStageSection++
									ENDIF
								ENDIF
							ELIF NOT IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
								IF NOT IS_PED_INJURED(budRon.ped)
									IF NOT IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
										SET_PED_AS_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
									ENDIF
								ENDIF
								
								IF NOT IS_PED_IN_VEHICLE(budRon.ped,vehTrevor.veh)
									CLEAR_PED_TASKS_IMMEDIATELY(budRon.ped)
									SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
									REMOVE_PED_FROM_GROUP(budRon.ped)
								ENDIF	
								
								REMOVE_PED_FROM_GROUP(budRon.ped)
								TASK_CLEAR_LOOK_AT(budRon.ped)
								
								IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
									iStageSection++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 6
				IF IS_SYNCHRONIZED_SCENE_RUNNING(SceneCutscene)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
						IF GET_SYNCHRONIZED_SCENE_PHASE(SceneCutscene) < 0.84
						OR IS_PED_IN_VEHICLE(budWade.ped,vehTrevor.veh)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh,TRUE)
						IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
							REMOVE_BLIP(sLocatesData.vehicleBlip)
						ENDIF
					ELSE
						IF NOT DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
							sLocatesData.vehicleBlip = CREATE_BLIP_FOR_VEHICLE(vehTrevor.veh,FALSE)
						ENDIF
					ENDIF
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
						IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
							REMOVE_BLIP(sLocatesData.vehicleBlip)
						ENDIF
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						bGoToSpeach = FALSE
						iStageSection++
					ENDIF
					IF NOT IS_PED_INJURED(budRon.ped)
						SET_PED_CONFIG_FLAG(budRon.ped,PCF_RunFromFiresAndExplosions,TRUE)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 7
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -14.7649, 2998.7571, 40.0025 >>,<<100.000000,100.000000,20.0>>)
					IF SETUP_MISSION_REQUIREMENT(REQ_ORTEGA,<< -40.6843, 3103.7644, 24.8673 >>, 160.9775)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("SETUP ORTEGA")
							IF NOT IS_PED_INJURED(budRon.ped)
								SET_PED_CONFIG_FLAG(budRon.ped,PCF_RunFromFiresAndExplosions,TRUE)
							ENDIF
							IF NOT IS_PED_INJURED(npcOrtega.ped)
								ADD_PED_FOR_DIALOGUE(structConvoPeds, 7, npcOrtega.ped, "ORTEGA")
								SET_ENTITY_INVINCIBLE(npcOrtega.ped,TRUE)
								SET_ENTITY_VISIBLE(npcOrtega.ped,FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(npcOrtega.ped, TRUE)
								REQUEST_ANIM_DICT("misstrevor1")
								SET_LABEL_AS_TRIGGERED("SETUP ORTEGA",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF HAS_LABEL_BEEN_TRIGGERED("SETUP ORTEGA")
						IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -14.7649, 2998.7571, 40.0025 >>,<<150.000000,150.000000,20.0>>)
							DELETE_PED(npcOrtega.ped)
							SET_MODEL_AS_NO_LONGER_NEEDED(IG_ORTEGA)
							REMOVE_PED_FOR_DIALOGUE(structConvoPeds, 7)
							SET_LABEL_AS_TRIGGERED("SETUP ORTEGA",FALSE)
						ENDIF
					ENDIF
				ENDIF
					
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(	sLocatesData,<<-8.5212, 3010.0645, 39.6299>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>, TRUE, budRon.ped, vehTrevor.veh, "TRV_GT4","", "", Asset.godGetBackToCar,FALSE,TRUE)
					
				ELSE
					bSkipped = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
					ENDIF
					
					IF DOES_BLIP_EXIST(blipDest)
						REMOVE_BLIP(blipDest)
					ENDIF
					
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -14.7649, 2998.7571, 40.0025 >>,<<45.000000,45.000000,6.687500>>)
							IF DOES_BLIP_EXIST(blipDest)
								REMOVE_BLIP(blipDest)
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(vehTrailer)
								FREEZE_ENTITY_POSITION(vehTrailer,TRUE)
							ENDIF
							iStageSection++
							REQUEST_VEHICLE_RECORDING(1,"trev1")
						ENDIF
					
						
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
									IF bGoToSpeach = FALSE
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD",  "T1M1_ORTCHAT", CONV_PRIORITY_MEDIUM)
											bGoToSpeach = TRUE
										ENDIF
									ENDIF
								ELSE
									IF bGoToSpeach = FALSE
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD",  "T1M1_ORTCHA2", CONV_PRIORITY_MEDIUM)
											bGoToSpeach = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ELSE
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							FREEZE_ENTITY_POSITION(vehTrailer,TRUE)
						ENDIF
					ENDIF
					
					IF bGoToSpeach = TRUE
						//MANAGE PAUSE DIALOGUE
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								CLEAR_PRINTS()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							ENDIF
						//Play conversation
						ELSE
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								IF IS_THIS_PRINT_BEING_DISPLAYED(Asset.godGetBackToCar)
									CLEAR_PRINTS()
								ENDIF
						    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			BREAK
			
			CASE 8
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(	sLocatesData,<<-8.5212, 3010.0645, 39.6299>>,
																<<0.1,0.1,LOCATE_SIZE_HEIGHT>>, TRUE, budRon.ped, 
																vehTrevor.veh, "","", Asset.godGetInTrevorCar, Asset.godGetBackToCar)
				ELSE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh) AND IS_VEHICLE_DRIVEABLE(vehTrailer)
							IF IS_ENTITY_IN_ANGLED_AREA(vehTrevor.veh, <<7.509832,3010.017822,39.366291>>, <<-12.365745,3050.341797,44.155788>>, 11.750000)
							OR IS_ENTITY_IN_ANGLED_AREA(vehTrevor.veh, <<-48.968388,3035.861084,38.892326>>, <<-12.365745,3050.341797,44.155788>>, 11.750000)
							OR IS_ENTITY_IN_ANGLED_AREA(vehTrevor.veh, <<-33.545883,2974.621094,38.427723>>, <<-46.360538,3038.441895,44.538128>>,11.750000)
							OR IS_ENTITY_IN_ANGLED_AREA(vehTrevor.veh, <<-48.858669,2977.304443,40.542831>>, <<15.350081,3010.138672,50.752701>>, 11.750000)
							OR IS_ENTITY_IN_ANGLED_AREA(vehTrevor.veh, <<-17.563643,3013.614746,52.538559>>, <<-48.496132,3098.484619,15.145500>>, 47.000000)
							OR IS_ENTITY_AT_COORD(vehTrevor.veh,<< -14.7649, 2998.7571, 40.0025 >>,<<10,10,10>>)
							OR GET_DISTANCE_BETWEEN_ENTITIES(vehTrailer,vehTrevor.veh) < 20
							OR GET_DISTANCE_BETWEEN_COORDS(<<0.75, 3023.01, 39.73>>,GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 4
								IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
									REQUEST_ANIM_DICT("misstrevor1")
									REQUEST_ANIM_DICT("misstrevor1ig_7")
									IF HAS_ANIM_DICT_LOADED("misstrevor1")
									AND HAS_ANIM_DICT_LOADED("misstrevor1ig_7")
									
										REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
									
										IF DOES_BLIP_EXIST(blipDest)
											REMOVE_BLIP(blipDest)
										ENDIF
										
										IF DOES_BLIP_EXIST(blipVeh)
											REMOVE_BLIP(blipVeh)
										ENDIF
										
										IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
											REMOVE_BLIP(sLocatesData.LocationBlip)
										ENDIF
										
										IF IS_VEHICLE_DRIVEABLE(vehTrailer)
											FREEZE_ENTITY_POSITION(vehTrailer,FALSE)
										ENDIF
										
										SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrailer,FALSE)

										SET_RADAR_ZOOM(200)
									
										REQUEST_PTFX_ASSET()
										SET_ENTITY_PROOFS(vehTrevor.veh,FALSE,FALSE,TRUE,TRUE,TRUE)
										
										KILL_FACE_TO_FACE_CONVERSATION()
										ADD_PED_FOR_DIALOGUE(structConvoPeds, 7, npcOrtega.ped, "ORTEGA")
										PRINTSTRING("JUMPING TO STAGE 9")PRINTNL()	
										REQUEST_ANIM_DICT("misstrevor1")
										iStageSection++
									ELSE
										REQUEST_ANIM_DICT("misstrevor1")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							FREEZE_ENTITY_POSITION(vehTrailer,TRUE)
						ENDIF
					ENDIF
					
					IF bGoToSpeach = TRUE
						//MANAGE PAUSE DIALOGUE
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								CLEAR_PRINTS()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							ENDIF
						//Play conversation
						ELSE
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								IF IS_THIS_PRINT_BEING_DISPLAYED(Asset.godGetBackToCar)
									CLEAR_PRINTS()
								ENDIF
						    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			BREAK
			
			//QUICK CUT FOR PARK UP
			CASE 9
				SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(TRUE)
				PRINTLN("@@@@@@@@@@@@ SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(TRUE) @@@@@@@@@@@")
				REQUEST_ANIM_DICT("misstrevor1")
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"trev1")
					AND HAS_ANIM_DICT_LOADED("misstrevor1")
					
						IF TRIGGER_MUSIC_EVENT("TRV1_TRAILER")
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor.veh)
								START_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh,1,"trev1")
							ELSE
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh,5900)
								
								IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
									SET_VEHICLE_TYRE_FIXED(vehTrevor.veh,SC_WHEEL_CAR_FRONT_LEFT)
									SET_VEHICLE_TYRE_FIXED(vehTrevor.veh,SC_WHEEL_CAR_FRONT_RIGHT)
									SET_VEHICLE_TYRE_FIXED(vehTrevor.veh,SC_WHEEL_CAR_REAR_LEFT)
									SET_VEHICLE_TYRE_FIXED(vehTrevor.veh,SC_WHEEL_CAR_REAR_RIGHT)
									IF IS_ENTITY_ON_FIRE(vehTrevor.veh)
										STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(vehTrevor.veh, FALSE),20)
										SET_VEHICLE_ENGINE_HEALTH(vehTrevor.veh,1000)
										SET_VEHICLE_PETROL_TANK_HEALTH(vehTrevor.veh,1000)
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehTrailer)
									CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(vehTrailer, FALSE),100,FALSE,FALSE,TRUE,TRUE,TRUE)
								ENDIF
								
								IF NOT IS_PED_INJURED(budRon.ped)
									SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(budRon.ped,FALSE)
								ENDIF
								
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(PLAYER_PED_ID(),FALSE)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehTrailer)
									FREEZE_ENTITY_POSITION(vehTrailer,FALSE)
								ENDIF
								
								LOCK_MINIMAP_ANGLE(29)
								
								CLEAR_AREA(<<-16.19, 3016.65, 40.51>>,100,TRUE,FALSE,FALSE)
								
								SET_MULTIHEAD_SAFE(TRUE)
								PRINTLN("@@@@@@@@@@@@ SET_MULTIHEAD_SAFE(TRUE) @@@@@@@@@@@")
								START_SCRIPTED_CUTSCENE(TRUE,TRUE)

								INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
								SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, TRUE, FALSE)
								//INITIAL ROAD SHOT
								PLAY_CAM_ANIM(cutscene_cam,  "ort_trailer_cam01","misstrevor1",<< -30.163, 3024.143, 39.913 >>,<< 0.000, 0.000, -133.500 >>)
								SET_CAM_ACTIVE(cutscene_cam,TRUE)
								RENDER_SCRIPT_CAMS(TRUE,FALSE)
								
								//MAKE TEMP CAM
								TempCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
								
								IF IS_VEHICLE_DRIVEABLE(vehTrailer)
									IF NOT IS_PED_INJURED(npcOrtega.ped)
										SET_ENTITY_VISIBLE(npcOrtega.ped,TRUE)
										FREEZE_ENTITY_POSITION(npcOrtega.ped,FALSE)
										CLEAR_PED_TASKS_IMMEDIATELY(npcOrtega.ped)
										ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
										TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "ortega_01_drinking_idle_intro", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
										SET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims,0.9)
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
									ENDIF
								ENDIF
								
								SETTIMERB(0)
								
								bGoToSpeach = FALSE
								PRINTSTRING("JUMPING TO STAGE 10")PRINTNL()	
								//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								KILL_FACE_TO_FACE_CONVERSATION()
								STOP_AUDIO_SCENE("TREVOR_1_DRIVE_TO_TRAILER")
								START_AUDIO_SCENE("TREVOR_1_ARRIVE_AT_TRAILER")
								ARRIVAL_REACTION = FALSE
								iStageSection++
							ENDIF
						ENDIF
					ELSE
						REQUEST_VEHICLE_RECORDING(1,"trev1")
					ENDIF
				ENDIF

			
			BREAK
			
			CASE 10
				IF NOT ARRIVAL_REACTION
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
								IF GET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims) > 0.99
									ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
									TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_02_Trevor_Arrival_Reaction", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
									ARRIVAL_REACTION = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_LINE")
					IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_LINE",CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("T1M1_LINE",TRUE)
					ELSE
						PRINTSTRING("TRYING TO START CONVERSATION")PRINTNL()
					ENDIF
				ENDIF
				
				//GRAB CURRENT ANIMATED CAM DATA
				SET_CAM_PARAMS(TempCam, GET_CAM_COORD(cutscene_cam),GET_CAM_ROT(cutscene_cam),GET_CAM_FOV(cutscene_cam))
				SHAKE_CAM(TempCam,"HAND_SHAKE",0.6)
				
				IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) > 0.99
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor.veh)
//								STOP_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh)
//							ENDIF
						ENDIF
						
						//SET_ENTITY_COORDS_NO_OFFSET(vehTrevor.veh, <<-11.4373, 3007.7844, 40.6016>>)
						//SET_ENTITY_ROTATION(vehTrevor.veh, <<-2.0516, -0.7819, 27.5497>>)
						
						IF NOT IS_PED_INJURED(budRon.ped)
							//CLEAR_PED_TASKS_IMMEDIATELY(budRon.ped)
							IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
								SET_PED_INTO_VEHICLE(budRon.ped,vehTrevor.veh,VS_FRONT_RIGHT)
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(),vehTrevor.veh,VS_DRIVER)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
							ENDIF
						ENDIF
					ENDIF

					camInterp = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
					
					ATTACH_CAM_TO_ENTITY(camInterp, vehTrevor.veh, <<0.5784, -8.4060, 0.9975>>)
					POINT_CAM_AT_ENTITY(camInterp, vehTrevor.veh, <<0.2264, -5.4394, 0.7215>>)
					SET_CAM_FOV(camInterp, 27.3860)
					SET_CAM_MOTION_BLUR_STRENGTH(camInterp, 0.0)
					SET_CAM_ACTIVE(camInterp,TRUE)
					SHAKE_CAM(camInterp,"HAND_SHAKE",0.6)
					
					SET_CAM_ACTIVE_WITH_INTERP(camInterp, TempCam, 1500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
					
					iSmashTimer = GET_GAME_TIMER()
					
					PRINTSTRING("JUMPING TO STAGE 11")PRINTNL()
					iStageSection++
				ENDIF
				
			BREAK
			
			CASE 11
				IF NOT ARRIVAL_REACTION
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
								IF GET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims) > 0.99
									ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
									TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_02_Trevor_Arrival_Reaction", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
									ARRIVAL_REACTION = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SHOUT IDLE")
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							IF NOT IS_PED_INJURED(npcOrtega.ped)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
									IF GET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims) > 0.99
										ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
										TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_03_Shouting_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
										SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,TRUE)
										SET_LABEL_AS_TRIGGERED("SHOUT IDLE",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				//IF MANAGE_MY_TIMER(iSmashTimer,2500)
				//ADDED
				IF MANAGE_MY_TIMER(iSmashTimer,2500)
					END_SCRIPTED_CUTSCENE(FALSE,FALSE,0,FALSE,FALSE)
					INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
					PRINTSTRING("KILLED CAMS MOVING TO NEXT STAGE")PRINTNL()
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEHICLE_BURNOUT(vehTrevor.veh,TRUE)
					ENDIF
					
					//SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, TRUE, FALSE)
					DISABLE_CELLPHONE(TRUE)

					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					
					bDisableControls = TRUE
					iSmashTimer = GET_GAME_TIMER()
					//ADDED
					bGoToSpeach = FALSE
					bTriggerPTFX = FALSE
					bPTFXStarted = FALSE
					bSkipped = FALSE
					iStageSection++
				ENDIF
			BREAK
			
		ENDSWITCH


//---------¦ JSKIP ¦---------
		/*IF JSkip
			iStageSection = 1
		ENDIF*/
			
//---------¦ CLEANUP ¦---------
		IF iStageSection >= 12
		
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 2.0, REPLAY_IMPORTANCE_HIGHEST)
		
			IF DOES_BLIP_EXIST(blipVeh)
				REMOVE_BLIP(blipVeh)
			ENDIF
			IF DOES_BLIP_EXIST(blipDest)
				REMOVE_BLIP(blipDest)
			ENDIF
			IF DOES_BLIP_EXIST(blipPed)
				REMOVE_BLIP(blipPed)
			ENDIF
			bStageSetup = FALSE
			iStageSection = 0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_IDEAL_CAR_CAM_POSITION()
      IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
      		RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrevor.veh, <<0.5784, -8.4060, 0.9975>>)
      ENDIF
      
      RETURN <<-7.6414, 3002.2417, 41.5575>>
ENDFUNC 

                              
VECTOR vIdealCamCoord
VECTOR vActualCamCoord

INT iTimesShouted
INT iRevSound = GET_SOUND_ID()
INT iOrtegaTrailerDialogue
BOOL bT1M1_EFA
BOOL bRequestAudio
BOOL bAnimsSetupOnReplay
BOOL bLeaveTrailerAnim = FALSE
BOOL bFallBack

INT iSafetyTimerCaravanSmash


OBJECT_INDEX objFence
BOOL bBreakFence

//PURPOSE:		Performs the section where Trevor tortures the victim for the first time
FUNC BOOL stageCaravanSmash()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	
	IF DOES_ENTITY_EXIST(npcOrtega.ped)
		IF IS_PED_INJURED(npcOrtega.ped)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(npcOrtega.ped, PLAYER_PED_ID())
			TRIGGER_MUSIC_EVENT("TRV1_FAIL")
			MissionFailed(FAIL_ORTEGA_KILLED_TOO_SOON)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehTrailer)
		IF IS_ENTITY_IN_ANGLED_AREA(vehTrailer, <<-30.081879,3046.692871,48.064957>>, <<-40.076206,3069.230225,31.243526>>, 11.250000)
			SET_VEHICLE_FRICTION_OVERRIDE(vehTrailer,0.35)
		ELSE
			IF IS_ENTITY_IN_ANGLED_AREA(vehTrailer,<<-435.020416,-1242.033691,48.434071>>, <<-448.880402,-1342.775635,30.265724>>, 24.000000)
				SET_VEHICLE_FRICTION_OVERRIDE(vehTrailer,0.30)
			ELSE
				SET_VEHICLE_FRICTION_OVERRIDE(vehTrailer,0.65)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(npcOrtega.ped)
		SET_PED_RESET_FLAG(npcOrtega.ped, PRF_DisablePotentialBlastReactions,TRUE) 
	ENDIF
	
	
	IF DOES_ENTITY_EXIST(objFence)
		IF DOES_ENTITY_EXIST(objFence)
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				IF NOT bBreakFence
					IF IS_ENTITY_TOUCHING_ENTITY(vehTrevor.veh,objFence)
						SET_ENTITY_LIGHTS(objFence,FALSE)
						BREAK_OBJECT_FRAGMENT_CHILD(objFence,-1,FALSE)
						bBreakFence = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT DOES_ENTITY_EXIST(objFence)
			objFence = GET_CLOSEST_OBJECT_OF_TYPE(<<1700.87, 3295.02, 45.95>>,1.0,PROP_WALL_LIGHT_02A)
		ENDIF
	ENDIF

	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_SELECTOR_THIS_FRAME() 
	
	IF iStageSection < 2
		IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_ORT")
			IF NOT IS_PED_INJURED(npcOrtega.ped)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(npcOrtega.ped,"T1M1_GDAA","ORTEGA",SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
				iOrtegaTrailerDialogue = GET_GAME_TIMER()
				SET_LABEL_AS_TRIGGERED("T1M1_ORT",TRUE)
			ENDIF
		ELSE
			IF MANAGE_MY_TIMER(iOrtegaTrailerDialogue,GET_RANDOM_INT_IN_RANGE(8000,12000))
				SET_LABEL_AS_TRIGGERED("T1M1_ORT",FALSE)
			ENDIF
		ENDIF
	ELSE
		IF iStageSection < 4
			
			VECTOR vOrtegaVelocity =  GET_ENTITY_VELOCITY(vehTrailer)
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_ORT2")
				IF vOrtegaVelocity.y > 0.0 //[MF] Only have Ortega swear if the trailer is actually moving B* 1762765
					IF NOT IS_PED_INJURED(npcOrtega.ped)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(npcOrtega.ped,"T1M1_GEAA","ORTEGA",SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
						iOrtegaTrailerDialogue = GET_GAME_TIMER()
						SET_LABEL_AS_TRIGGERED("T1M1_ORT2",TRUE)
					ENDIF
				ENDIF
			ELSE
				IF MANAGE_MY_TIMER(iOrtegaTrailerDialogue,GET_RANDOM_INT_IN_RANGE(15000,18000))
					SET_LABEL_AS_TRIGGERED("T1M1_ORT2",FALSE)
				ENDIF
			ENDIF
		ELSE
			IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_ORT3")
				IF NOT IS_PED_INJURED(npcOrtega.ped)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(npcOrtega.ped,"T1M1_GFAA","ORTEGA",SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
					iOrtegaTrailerDialogue = GET_GAME_TIMER()
					SET_LABEL_AS_TRIGGERED("T1M1_ORT3",TRUE)
				ENDIF
			ELSE
				IF MANAGE_MY_TIMER(iOrtegaTrailerDialogue,GET_RANDOM_INT_IN_RANGE(15000,18000))
					SET_LABEL_AS_TRIGGERED("T1M1_ORT3",FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iStageSection > 0 AND iStageSection < 4
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_VEH_ACCELERATE)
					IF HAS_SOUND_FINISHED(iRevSound)
						PLAY_SOUND_FROM_ENTITY(iRevSound,"TREVOR_1_RAM_TRAILER_REVS",vehTrevor.veh)
					ENDIF
				ELSE
					STOP_SOUND(iRevSound)
				ENDIF
			ELSE
				STOP_SOUND(iRevSound)
			ENDIF
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_5)
			OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(vehTrevor.veh)
		ENDIF
			
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_8)
			OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(vehTrailer)
		ENDIF
	#ENDIF
	
	MANAGE_PTFX()
	
	WEAPON_TYPE wpn = WEAPONTYPE_PUMPSHOTGUN
	
	IF iStageSection < 3
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(fThisValue)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(fThisValue)
	ENDIF
	
	IF NOT bRequestAudio
		IF REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACTS")
		AND REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACTS_1")
		AND REQUEST_SCRIPT_AUDIO_BANK("TREVOR_1_TRAILER_IMPACTS_2")
			bRequestAudio = TRUE
		ENDIF
	ENDIF
	
	IF iStageSection > 0 AND iStageSection < 4
		IF bAnimsSetupOnReplay = FALSE
			IF NOT ARRIVAL_REACTION 
				IF IS_VEHICLE_DRIVEABLE(vehTrailer)
					IF NOT IS_PED_INJURED(npcOrtega.ped)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
							IF GET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims) > 0.99
								ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
								TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_02_Trevor_Arrival_Reaction", SLOW_BLEND_IN, NORMAL_BLEND_OUT )
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
								ARRIVAL_REACTION = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SHOUT IDLE")
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
								IF GET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims) > 0.99
									ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
									TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_03_Shouting_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
									SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,TRUE)
									SET_LABEL_AS_TRIGGERED("SHOUT IDLE",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bSetUpStageData = FALSE
		SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		SETUP_PEDS_FOR_DIALOGUE()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Push the trailer")
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		cachedCamMode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() 
//		PRINTLN("@@@@@@@@@@@@@ cachedCamMode: ", cachedCamMode, " = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() @@@@@@@@@@@@@@@@")
		bCachedCamMode = TRUE
		SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
		bBlockFirstPerson = TRUE
		bSetUpStageData = TRUE
	ENDIF

	MANAGE_TRAILER_FORCES()
	MANAGE_BIKER_BEHAVIOR()

//---------¦ SETUP ¦---------
	IF NOT bStageSetup	
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Push the trailer")
		//--Stop checking for headshots.
		RESET_MISSION_STATS_ENTITY_WATCH()
		RESET_NEED_LIST()
		MANAGE_LOADING_NEED_LIST()
		
		REQUEST_MODEL(PROPTRAILER)
		REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_ORTEGA))
		
		IF IS_SCREEN_FADED_OUT()
			LOAD_SCENE(<< -19.4662, 3023.3350, 39.9864 >>)
			SETUP_TRAILER()
			WHILE bSetupCaravan = TRUE
				WAIT(0)
			ENDWHILE
		ENDIF
		
		IF bSkipped = FALSE
			
		ENDIF
		
		//iRage = 0
	
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,FALSE)
		SET_START_AND_END_VECTORS()
		INITALISE_ARRAYS(STAGE_CARAVAN_SMASH)
		
		REMOVE_IPL("TRV1_Trail_end")
		REQUEST_IPL("TRV1_Trail_start")

		REQUEST_VEHICLE_RECORDING(1,"trev1")
		
		bStartForce = TRUE
		bStageSetup = TRUE
		bRequestAudio = FALSE
		bFallBack = FALSE
		
		bFrag1 = FALSE
		bFrag2 = FALSE
		bFrag3 = FALSE
		bDoFails = TRUE
		iStageSection = 0
		
		//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
		IF NOT bTrevorSplashBackEffectOn
			APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
			bTrevorSplashBackEffectOn = TRUE
		ENDIF	
		
//---------¦ UPDATE ¦---------		
	ELSE		

		SWITCH iStageSection
		
			CASE 0
				IF bSkipped = TRUE
					REQUEST_ANIM_DICT("misstrevor1ig_7")
					IF HAS_ANIM_DICT_LOADED("misstrevor1ig_7")
						camInterp = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
						
//						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//							ATTACH_CAM_TO_ENTITY(camInterp, vehTrevor.veh, <<0.5784, -8.4060, 0.9975>>)
//							POINT_CAM_AT_ENTITY(camInterp,vehTrevor.veh, <<0.2264, -5.4394, 0.7215>>)
//						ENDIF
						
						SET_CAM_PARAMS(camInterp, <<-7.0672, 3000.6140, 41.8584>>, <<-6.9699, -0.1718, 33.9582>>,27.3860)
						
						SET_CAM_FOV(camInterp, 27.3860)
						SET_CAM_MOTION_BLUR_STRENGTH(camInterp, 0.0)
						SET_CAM_ACTIVE(camInterp,TRUE)
						SHAKE_CAM(camInterp,"HAND_SHAKE",0.6)
						RENDER_SCRIPT_CAMS(TRUE,FALSE)
						LOCK_MINIMAP_ANGLE(29)

						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							IF NOT IS_PED_INJURED(npcOrtega.ped)
								ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
								TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_03_Shouting_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
								SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,TRUE)
							ENDIF
						ENDIF
						
						REQUEST_PTFX_ASSET()
						REQUEST_ANIM_DICT("misstrevor1")
						bSkipped = FALSE
						safefadeInAndWait()	
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							SET_VEHICLE_BURNOUT(vehTrevor.veh,TRUE)
						ENDIF
						iSmashTimer = GET_GAME_TIMER()
						SET_MULTIHEAD_SAFE(FALSE)
						PRINTLN("@@@@@@@@@@ CASE 1 - SET_MULTIHEAD_SAFE(FALSE) @@@@@@@@@@")
						
						bAnimsSetupOnReplay = TRUE
						iStageSection++
					ENDIF
				ELSE
					REQUEST_ANIM_DICT("misstrevor1ig_7")
					IF HAS_ANIM_DICT_LOADED("misstrevor1ig_7")
						REQUEST_ANIM_DICT("misstrevor1")
						REQUEST_PTFX_ASSET()
						SET_MULTIHEAD_SAFE(FALSE)
						PRINTLN("@@@@@@@@@@ CASE 0 - SET_MULTIHEAD_SAFE(FALSE) @@@@@@@@@@")
						
						bAnimsSetupOnReplay = FALSE
						iSafetyTimerCaravanSmash = GET_GAME_TIMER()
						iStageSection++
					ENDIF
				ENDIF
			BREAK
			
			
			CASE 1
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
				
				//IF HAS_LABEL_BEEN_TRIGGERED("T1M1_PT1")
				IF DOES_BLIP_EXIST(blipDest)
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_VEH_ACCELERATE)
					OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_VEH_ACCELERATE)
						REQUEST_ANIM_DICT("misstrevor1")
						IF HAS_ANIM_DICT_LOADED("misstrevor1")
							IF NOT IS_PED_INJURED(budRon.ped)
								SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(budRon.ped,TRUE)
							ENDIF
							
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(PLAYER_PED_ID(),TRUE)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								SET_ENTITY_INVINCIBLE(vehTrevor.veh,TRUE)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(vehTrailer)
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrailer,FALSE)
								SET_VEHICLE_FRICTION_OVERRIDE(vehTrailer,0.4)
							ENDIF

							REQUEST_VEHICLE_RECORDING(1, "pushtrailer")
							
							camCutscene = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
							DETACH_CAM(camCutscene)
							DETACH_CAM(camInterp)
							
							SET_CAM_PARAMS(camInterp, GET_CAM_COORD(camInterp),GET_CAM_ROT(camInterp),GET_CAM_FOV(camInterp))
							SHAKE_CAM(camInterp, "ROAD_VIBRATION_SHAKE", 0.4)
							
	//						ATTACH_CAM_TO_ENTITY(camCutscene, vehTrevor.veh,  <<1.3158, -7.2702, 0.8591>>)
	//						POINT_CAM_AT_ENTITY(camCutscene, vehTrevor.veh, <<1.0090, -4.2944, 0.6352>>)

							vActualCamCoord = GET_IDEAL_CAR_CAM_POSITION()
							vActualCamCoord.z = 41.9
							SET_CAM_COORD(camCutscene, vActualCamCoord)
							SET_CAM_ROT(camCutscene, <<-6.8007, 0.0945, 34.2821>>)
							
							vIdealCamCoord = GET_IDEAL_CAR_CAM_POSITION()
							vActualCamCoord += (vIdealCamCoord - vActualCamCoord) * (7.5 * GET_FRAME_TIME()) // 7.5 CAN BE TUNED, CHANGES SPEED
							vActualCamCoord.z = 41.9 // SETTING THE Z TO BE THE SAME EACH FRAME OR IT BOUNCES UP AND DOWN AS IF ATTACHED
							
							SET_CAM_COORD(camCutscene, vActualCamCoord)
							PRINTSTRING("vActualCamCoord")PRINTVECTOR(vActualCamCoord)PRINTNL()
							
							SET_CAM_FOV(camCutscene, 27.3860)
							SHAKE_CAM(camCutscene, "ROAD_VIBRATION_SHAKE", 0.2)
							SET_CAM_MOTION_BLUR_STRENGTH(camCutscene, 0.0)
							
							SET_CAM_ACTIVE_WITH_INTERP(camCutscene,camInterp,2000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
							
							iSmashTimer = GET_GAME_TIMER()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV_ACCEL")
								CLEAR_HELP(TRUE)
							ENDIF
							bTriggerPTFX = TRUE
							REMOVE_CUTSCENE()
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, TRUE, FALSE)
							PREPARE_MUSIC_EVENT("TRV1_RAM_TRAILER")
							SET_LABEL_AS_TRIGGERED("T1M1_PT1",TRUE)
							KILL_FACE_TO_FACE_CONVERSATION()
							
							IF IS_VEHICLE_DRIVEABLE(vehTrailer)
								IF NOT IS_PED_INJURED(npcOrtega.ped)
									SET_ENTITY_NO_COLLISION_ENTITY(npcOrtega.ped,vehTrailer,FALSE)
								ENDIF
							ENDIF
							

							iStageSection++
						ENDIF
					ENDIF
				ELSE
					//1600455
					IF MANAGE_MY_TIMER(iSafetyTimerCaravanSmash,6000)
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						PRINT_HELP("TRV_ACCEL")
						PRINT_GOD_TEXT("TRV_PUSH")
						blipDest = CREATE_BLIP_FOR_COORD(<< -49.0106, 3098.9553, 24.7669 >>)
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				//T1M1_PT1
					
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
				
				vIdealCamCoord = GET_IDEAL_CAR_CAM_POSITION()
				//vActualCamCoord += (vIdealCamCoord - vActualCamCoord) * (7.5 * GET_FRAME_TIME()) // 7.5 CAN BE TUNED, CHANGES SPEED
				vActualCamCoord = (vIdealCamCoord) // 7.5 CAN BE TUNED, CHANGES SPEED
				vActualCamCoord.z = 41.9 // SETTING THE Z TO BE THE SAME EACH FRAME OR IT BOUNCES UP AND DOWN AS IF ATTACHED
				
				//SET_CAM_COORD(camCutscene, vIdealCamCoord)
				SHAKE_CAM(camCutscene, "ROAD_VIBRATION_SHAKE", 0.2)
				PRINTSTRING("vvIdealCamCoord")PRINTVECTOR(vIdealCamCoord)PRINTNL()
				
				
				IF MANAGE_MY_TIMER(iSmashTimer,2000)
					PREPARE_MUSIC_EVENT("TRV1_RAM_TRAILER")
					IF TRIGGER_MUSIC_EVENT("TRV1_RAM_TRAILER")
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							SET_VEHICLE_BURNOUT(vehTrevor.veh,FALSE)
							IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearLeftWheel)
							AND DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRearRightWheel)
								STOP_PARTICLE_FX_LOOPED(ptfxRearLeftWheel)
								STOP_PARTICLE_FX_LOOPED(ptfxRearRightWheel)
								bPTFXStarted = FALSE
							ENDIF
							bTriggerPTFX = FALSE
						ENDIF
						REMOVE_CUTSCENE()
						REQUEST_CUTSCENE("TRV_1_MCS_3_CONCAT")
//						RENDER_SCRIPT_CAMS(FALSE,TRUE,1500)
//						DESTROY_CAM(cutscene_cam)
						
						KILL_FACE_TO_FACE_CONVERSATION()
						bDisableControls = TRUE
						bImpactDialogue = FALSE
						icutsceneprog=0
						iTimesShouted = 0
						bT1M1_EFA = FALSE
						IPL_GROUP_SWAP_START("CS3_05_water_grp1","CS3_05_water_grp2")
						iStageSection++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
//				PRINTLN("@@@@@@@@@@@@@@ DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE() @@@@@@@@@@@@@")
				DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_EDA")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EDA",CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("T1M1_EDA",TRUE)
							ENDIF
						ENDIF
					ELSE
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"T1M1_EDAA","TREVOR",SPEECH_PARAMS_SHOUTED)
						SET_LABEL_AS_TRIGGERED("T1M1_EDA",TRUE)
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_SPECIAL")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_SPECIAL",CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("T1M1_SPECIAL",TRUE)
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(),"T1M1_GKAA","TREVOR",SPEECH_PARAMS_SHOUTED)
								SET_LABEL_AS_TRIGGERED("T1M1_EDA",TRUE)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				MANAGE_TRAILER_SOUNDS()
				
//				vIdealCamCoord = GET_IDEAL_CAR_CAM_POSITION()
//				vActualCamCoord += (vIdealCamCoord - vActualCamCoord) * (7.5 * GET_FRAME_TIME()) // 7.5 CAN BE TUNED, CHANGES SPEED
//				vActualCamCoord.z = 41.8564 // SETTING THE Z TO BE THE SAME EACH FRAME OR IT BOUNCES UP AND DOWN AS IF ATTACHED
//				SET_CAM_COORD(camInterp, vActualCamCoord)
//				PRINTSTRING("vActualCamCoord")PRINTVECTOR(vActualCamCoord)PRINTNL()
				
				vIdealCamCoord = GET_IDEAL_CAR_CAM_POSITION()
				//vActualCamCoord += (vIdealCamCoord - vActualCamCoord) * (7.5 * GET_FRAME_TIME()) // 7.5 CAN BE TUNED, CHANGES SPEED
				vActualCamCoord = (vIdealCamCoord) // 7.5 CAN BE TUNED, CHANGES SPEED
				vActualCamCoord.z = 41.9// SETTING THE Z TO BE THE SAME EACH FRAME OR IT BOUNCES UP AND DOWN AS IF ATTACHED
				
				IF DOES_CAM_EXIST(camCutscene)
					IF NOT IS_CAM_INTERPOLATING(camCutscene)
					AND NOT IS_CAM_INTERPOLATING(camInterp)
						SET_CAM_COORD(camCutscene, vActualCamCoord)
						SHAKE_CAM(camCutscene, "ROAD_VIBRATION_SHAKE", 0.3)
					ENDIF
				ENDIF
				
				PRINTSTRING("vvIdealCamCoord")PRINTVECTOR(vIdealCamCoord)PRINTNL()

				IF bDisableControls = TRUE
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF IS_ENTITY_IN_ANGLED_AREA(vehTrevor.veh, <<-20.866846,3021.173096,39.500191>>, <<-17.276619,3023.548340,43.281097>>, 7.000000)
							IF GET_ENTITY_SPEED(vehTrevor.veh) < 3
								SET_VEHICLE_FORWARD_SPEED(vehTrevor.veh,3)
							ENDIF
						ENDIF
					ENDIF
					IF NOT bFallBack//HAS_LABEL_BEEN_TRIGGERED("FALL BACK")
						IF bStartForce = FALSE
							PRINTSTRING("HIT TRAILER")PRINTNL()
							RUN_ACTION_CUT()
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_ENTITY_INVINCIBLE(vehTrevor.veh,FALSE)
						SET_ENTITY_PROOFS(vehTrevor.veh,FALSE,FALSE,FALSE,TRUE,FALSE)
					ENDIF
					PRINTSTRING("!!!!!!!!!!")
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
					//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-12.517030,3010.379639,39.033924>>, <<-9.976560,3005.556152,42.433891>>, 4.000000)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					//ENDIF
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
				ELSE
					IF bImpactDialogue = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EEB",CONV_PRIORITY_MEDIUM)
								bImpactDialogue = TRUE
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("T1M1_EGA")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EGA",CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("T1M1_EGA",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bDisableControls = FALSE
					IF bcutsceneplaying = FALSE
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							IF NOT IS_PED_INJURED(npcOrtega.ped)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
									IF NOT bFallBack//HAS_LABEL_BEEN_TRIGGERED("FALL BACK")
										ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
										TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_06_Fall_Back_Into_Trailer", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
										SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,FALSE)
										bFallBack = TRUE//SET_LABEL_AS_TRIGGERED("FALL BACK",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bFallBack//HAS_LABEL_BEEN_TRIGGERED("FALL BACK")
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
								IF GET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims) > 0.99
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FALL BACK LOOP")
										ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
										TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_07_Holding_Onto_Side_Wall_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
										SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,TRUE)
										SET_LABEL_AS_TRIGGERED("FALL BACK LOOP",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(vehTrailer,<<-101.270760,3104.847168,22.087103>>,<<-16.644316,3122.307129,26.642069>>,28.250000)
					IF bSplash = FALSE
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev1_trailer_splash",vehTrailer,<<0,0,0>>,<<0,0,0>>)
							bSplash = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//IS THE TRAILER IN THE RIVER
				IF DOES_ENTITY_EXIST(vehTrailer)
					IF IS_ENTITY_IN_ANGLED_AREA(vehTrailer,<<-101.270760,3104.847168,22.087103>>,<<-16.644316,3122.307129,26.642069>>,28.250000)
					OR IS_ENTITY_IN_ANGLED_AREA(vehTrailer, <<-45.057804,3085.645264,24.610682>>, <<-46.814102,3118.266113,33.369240>>, 23.250000)
					OR IS_ENTITY_IN_ANGLED_AREA(vehTrailer, <<-59.437050,3101.389160,23.710390>>, <<-13.134295,3108.064453,35.350128>>, 35.250000)
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							SET_ENTITY_PROOFS(vehTrailer,FALSE,FALSE,FALSE,FALSE,FALSE)
							SET_ENTITY_INVINCIBLE(vehTrailer,FALSE)
						ENDIF
						
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 2000, 256)
						
						SETTIMERB(0)
						IF DOES_BLIP_EXIST(blipDest)
							REMOVE_BLIP(blipDest)
						ENDIF
						IF DOES_BLIP_EXIST(blipVeh)
							REMOVE_BLIP(blipVeh)
						ENDIF
						SETTIMERB(0)
						DISABLE_CELLPHONE(FALSE)
						i_current_event = 0
						KILL_ANY_CONVERSATION()
						REQUEST_CUTSCENE("TRV_1_MCS_3_CONCAT")
						
						bLeaveTrailerAnim = FALSE
	
						iStageSection++
					ELSE
						
						IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData,<< -49.0106, 3098.9553, 24.7669 >>,<<0.1,0.1,0.1>>,FALSE,budRon.ped,vehTrevor.veh,"","TRV_BDY2","",Asset.godGetBackToCar)
						
						ELSE		
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF NOT IS_ENTITY_DEAD(vehTrailer)
									FREEZE_ENTITY_POSITION(vehTrailer,FALSE)
								ENDIF
								IF DOES_BLIP_EXIST(blipDest)
									SET_BLIP_ALPHA(blipDest,255)
								ENDIF
								SET_BLIP_ALPHA(sLocatesData.LocationBlip,0)
								SET_BLIP_ROUTE(sLocatesData.LocationBlip,FALSE)
								
								IF NOT DOES_BLIP_EXIST(blipDest)
									blipDest = CREATE_BLIP_FOR_COORD(<< -49.0106, 3098.9553, 24.7669 >>)
									PRINT_HELP("TRV_ACCEL")
									PRINT_GOD_TEXT("TRV_PUSH")
								ELSE
									IF bDisableControls = FALSE
										IF NOT bT1M1_EFA
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EFA",CONV_PRIORITY_MEDIUM)
														bT1M1_EFA = TRUE
														iRamTimer = GET_GAME_TIMER()
														iTimesShouted ++
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF iTimesShouted < 3
												IF NOT MANAGE_MY_TIMER(iRamTimer,GET_RANDOM_INT_IN_RANGE(2000,7000))
													bT1M1_EFA = FALSE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE	
								IF DOES_BLIP_EXIST(blipDest)
									SET_BLIP_ALPHA(blipDest,0)
								ENDIF
								IF NOT IS_ENTITY_DEAD(vehTrailer)
									FREEZE_ENTITY_POSITION(vehTrailer,TRUE)
								ENDIF
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(vehTrailer)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehTrailer) > 60
									MissionFailed(FAIL_LEFT_ORTEGA)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF

			BREAK
			
			CASE 4
				IF IS_ENTITY_IN_ANGLED_AREA(vehTrailer,<<-101.270760,3104.847168,22.087103>>,<<-16.644316,3122.307129,26.642069>>,28.250000)
					IF bSplash = FALSE
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev1_trailer_splash",vehTrailer,<<0,0,0>>,<<0,0,0>>)
							bSplash = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF bFallBack//HAS_LABEL_BEEN_TRIGGERED("FALL BACK")
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
								IF GET_SYNCHRONIZED_SCENE_PHASE(ssOrtegaTrailerAnims) > 0.99
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FALL BACK LOOP")
										ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
										TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_07_Holding_Onto_Side_Wall_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
										SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,TRUE)
										SET_LABEL_AS_TRIGGERED("FALL BACK LOOP",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("FALL BACK LOOP")
					IF bLeaveTrailerAnim = FALSE
						IF IS_VEHICLE_DRIVEABLE(vehTrailer)
							IF NOT IS_PED_INJURED(npcOrtega.ped)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(ssOrtegaTrailerAnims)
									ssOrtegaTrailerAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.450, -3.200, 0.350 >>, << 0.000, 0.000, 0.000 >>)
									TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ssOrtegaTrailerAnims, "misstrevor1ig_7", "Ortega_08_Run_To_Door_Outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssOrtegaTrailerAnims,vehTrailer,-1)
									SET_SYNCHRONIZED_SCENE_LOOPED(ssOrtegaTrailerAnims,FALSE)
									bLeaveTrailerAnim = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
				IF TIMERB() > 500
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_crash_dust)
						REMOVE_PARTICLE_FX(ptfx_crash_dust)
					ENDIF
				ENDIF
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				IF TIMERB() > 20
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_crash_dust)
						REMOVE_PARTICLE_FX(ptfx_crash_dust)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						SET_VEHICLE_STRONG(vehTrailer,FALSE)
					ENDIF
					STOP_PLAYER_CAR(FALSE)
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF IS_VEHICLE_ON_ALL_WHEELS(vehTrevor.veh)
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED | ECF_BLOCK_SEAT_SHUFFLING | ECF_WAIT_FOR_ENTRY_POINT_TO_BE_CLEAR)
						ENDIF
						
						IF NOT IS_PED_INJURED(budRon.ped)
							TASK_LEAVE_ANY_VEHICLE(budRon.ped, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED | ECF_BLOCK_SEAT_SHUFFLING | ECF_WAIT_FOR_ENTRY_POINT_TO_BE_CLEAR)
						ENDIF
					ENDIF
					e_section_stage = SECTION_STAGE_SETUP
					bExitState = FALSE
					i_current_event = 0
					STOP_SOUND(iRevSound)
					iStageSection++
				ELSE
					SET_VEHICLE_FRICTION_OVERRIDE(vehTrailer,0.3)
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						APPLY_FORCE_TO_ENTITY(vehTrailer,APPLY_TYPE_IMPULSE,<<0,1.0,-1>>,<<0,0,0>>,0,FALSE,FALSE,TRUE)
					ENDIF
				ENDIF

			BREAK
			
			CASE 5
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				RUN_TRAILER_IN_WATER_CUTSCENE()
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					SET_ENTITY_VISIBLE(vehTrevor.veh,TRUE)
				ENDIF
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN)
					IF NOT GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wpn)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN,TRUE)
					ENDIF
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					SET_ENTITY_PROOFS(vehTrevor.veh,FALSE,FALSE,FALSE,FALSE,FALSE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrailer)
					IF NOT IS_PED_INJURED(npcOrtega.ped)
						SET_ENTITY_NO_COLLISION_ENTITY(npcOrtega.ped,vehTrailer,TRUE)
					ENDIF
				ENDIF
				iStageSection++
			
			BREAK
			
			
			CASE 6
				IF NOT IS_PED_INJURED(npcOrtega.ped)
				AND NOT IS_PED_INJURED(budRon.ped)
					IF IS_VEHICLE_DRIVEABLE(vehTrailer)
						SET_ENTITY_COLLISION(vehTrailer,TRUE,TRUE)
					ENDIF
					IF IS_ENTITY_ATTACHED(npcOrtega.ped)
						DETACH_ENTITY(npcOrtega.ped)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_ENTITY_PROOFS(vehTrevor.veh,FALSE,FALSE,FALSE,FALSE,FALSE)
					ENDIF
					SET_WANTED_LEVEL_MULTIPLIER(0.3)
					iStageSection++
				ENDIF
				
			BREAK
		ENDSWITCH
		
		
//---------¦ JSKIP ¦---------
		/*IF JSkip
			iStageSection = 5
		ENDIF*/
	
//---------¦ CLEANUP ¦---------
		IF iStageSection >= 7
			//reset the mission stage ints and move on to the next stage
			RESET_MISSION_STAGE_INTS()
			bSetUpStageData = FALSE
			bTriggerLoopedEndAnim = FALSE
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_KILL_DECISION()

	IF DOES_BLIP_EXIST(npcOrtega.blip)
		flashBlipForKillDecision(npcOrtega.blip, npcOrtega.iPedTimer,npcOrtega.bFriendly)
	ENDIF
	
	IF bGotFlashTimer= FALSE
		bGotFlashTimer = TRUE
	ENDIF
	
	IF NOT DOES_BLIP_EXIST( npcOrtega.blip)
		
	ENDIF
ENDPROC

PROC MANAGE_ORTEGA_IF_SPARED()
	IF HAS_LABEL_BEEN_TRIGGERED("ORTEGA SPARED")
		IF NOT IS_PED_INJURED(npcOrtega.ped)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER ORT EXIT")
				IF IS_SYNCHRONIZED_SCENE_RUNNING(ThreatenSceneMain)
					IF GET_SYNCHRONIZED_SCENE_PHASE(ThreatenSceneMain) = 1
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER ORT EXIT")
							ThreatenSceneMain = CREATE_SYNCHRONIZED_SCENE(<< -46.236, 3096.727, 25.513 >>, << 3.000, 0.000, -162.720 >>)
							TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ThreatenSceneMain, "misstrevor1", "ortega_outro_loop_ort", NORMAL_BLEND_IN, NORMAL_BLEND_IN,SYNCED_SCENE_USE_PHYSICS)
							SET_SYNCHRONIZED_SCENE_LOOPED(ThreatenSceneMain,TRUE)
							SET_LABEL_AS_TRIGGERED("TRIGGER ORT EXIT",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FLEE ORTEGA")
					IF NOT IS_ENTITY_ON_SCREEN(npcOrtega.ped)
					OR IS_PED_RAGDOLL(npcOrtega.ped)
						OPEN_SEQUENCE_TASK(seqSequence)
							TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2000, -1)
						CLOSE_SEQUENCE_TASK(seqSequence)
						TASK_PERFORM_SEQUENCE(npcOrtega.ped, seqSequence)
						CLEAR_SEQUENCE_TASK(seqSequence)
						SET_PED_KEEP_TASK(npcOrtega.ped, TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(npcOrtega.ped)
						SET_LABEL_AS_TRIGGERED("FLEE ORTEGA",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iOrtegaFlinches
INT iRandomFlinch
INT iFinchDelayTimer

PROC MANAGE_ORTEGA_FLINCHES()
	
	SWITCH iOrtegaFlinches
		
		CASE 0
			REQUEST_ANIM_DICT("MISSCOMMON@HANDS_UP_FLINCH")
			IF HAS_ANIM_DICT_LOADED("MISSCOMMON@HANDS_UP_FLINCH")
				iOrtegaFlinches ++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(npcOrtega.ped)
				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(npcOrtega.ped, FALSE), 5.0)
				OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(npcOrtega.ped, FALSE), 10.0)
					iOrtegaFlinches ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(npcOrtega.ped)
				iRandomFlinch = GET_RANDOM_INT_IN_RANGE(0,3)
				IF iRandomFlinch = 0
					TASK_PLAY_ANIM(npcOrtega.ped,"MISSCOMMON@HANDS_UP_FLINCH","FLINCH_ADDITIVE_A",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_ADDITIVE| AF_SECONDARY)
				ENDIF
				IF iRandomFlinch = 1
					TASK_PLAY_ANIM(npcOrtega.ped,"MISSCOMMON@HANDS_UP_FLINCH","FLINCH_ADDITIVE_B",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_ADDITIVE| AF_SECONDARY)
				ENDIF
				IF iRandomFlinch = 2
					TASK_PLAY_ANIM(npcOrtega.ped,"MISSCOMMON@HANDS_UP_FLINCH","FLINCH_ADDITIVE_C",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_ADDITIVE| AF_SECONDARY)
				ENDIF
				iOrtegaFlinches ++
				iFinchDelayTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
		
		CASE 3
			IF MANAGE_MY_TIMER(iFinchDelayTimer,1800)
				iOrtegaFlinches = 1
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

INT iOrtegaFlee
INT iTimerBeforeFlee

PROC MANAGE_ORTEGA_FLEE_RESPONSE()
	
	SWITCH iOrtegaFlee
		
		CASE 0
			IF NOT IS_PED_INJURED(npcOrtega.ped)
				IF bOrtegaSpared = TRUE
					IF MANAGE_MY_TIMER(iTimerBeforeFlee,3000)
						ThreatenSceneMain = CREATE_SYNCHRONIZED_SCENE(<< -46.236, 3096.727, 25.513 >>, << 3.000, 0.000, -162.720 >>)
						TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ThreatenSceneMain, "misstrevor1", "ortega_kneel_to_stand_ort", NORMAL_BLEND_IN, NORMAL_BLEND_OUT ,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON,RBF_PLAYER_IMPACT)
						iOrtegaFlee ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(npcOrtega.ped)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(ThreatenSceneMain)
					IF GET_SYNCHRONIZED_SCENE_PHASE(ThreatenSceneMain) > 0.99
						TASK_SMART_FLEE_PED(npcOrtega.ped,PLAYER_PED_ID(),200,-1)
						iOrtegaFlee ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

BOOL T1M1_P13 = FALSE
BOOL T1M1_IG4A = FALSE
BOOL T1M1_BEG = FALSE
BOOL T1M1_SPECIAL = FALSE
STRING bKillString
BOOL T1M1_SHOOT = FALSE



//PURPOSE:		Performs the section where Trevor tortures the victim for the first time
FUNC BOOL stageKillSmashMan()

	MANAGE_ORTEGA_FLINCHES()
	MANAGE_ORTEGA_FLEE_RESPONSE()
	
	IF DOES_ENTITY_EXIST(npcOrtega.ped)
		IF IS_PED_INJURED(npcOrtega.ped)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, TRUE)
			STAT_SET_BOOL(SP_KILLED_ORTEGA, TRUE)
		ELSE
			SET_PED_CAPSULE(npcOrtega.ped,0.5)
		ENDIF
	ENDIF

	//MANAGE_TRAILER_DAMAGE()
	
//	IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
//		bKillString = "T1M1_SHOOT"
//	ELSE
		bKillString = "T1M1_RAGEKIL"
//	ENDIF
	
	IF NOT IS_PED_INJURED(npcOrtega.ped)
		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF NOT T1M1_SPECIAL
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_SPECIAL", CONV_PRIORITY_MEDIUM)
						T1M1_SPECIAL = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iStageSection = 1
		IF NOT IS_PED_INJURED(npcOrtega.ped)
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),npcOrtega.ped) > 8
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),npcOrtega.ped) > 25
					MissionFailed(FAIL_LEFT_ORTEGA)
				ELSE
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV_ORTRET")
							IF bReminderPrint = FALSE
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								bReminderPrint = TRUE
								PRINT_GOD_TEXT("TRV_ORTRET")
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("TRV_ORTRET")
							PRINT_GOD_TEXT("TRV_ORTRET")
							SET_LABEL_AS_TRIGGERED("TRV_ORTRET",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_PRINT_BEING_DISPLAYED("TRV_ORTRET")
					CLEAR_PRINTS()
				ENDIF
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			ENDIF
		ELSE
			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF iStageSection > 1
		IF NOT IS_PED_INJURED(npcOrtega.ped)
			OVERRIDE_TREVOR_RAGE("T1M1_FLAA")
			MANAGE_KILL_DECISION()
		ELSE
			RESET_TREVOR_RAGE()
			REMOVE_PED_FOR_DIALOGUE(structConvoPeds,7)
			IF DOES_BLIP_EXIST(npcOrtega.blip)
				REMOVE_BLIP(npcOrtega.blip)
			ENDIF
		ENDIF
	ENDIF
	
	IF iStageSection > 1
		IF bTriggerLoopedEndAnim = FALSE
			IF NOT IS_PED_INJURED(npcOrtega.ped)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(ThreatenSceneMain)
					IF GET_SYNCHRONIZED_SCENE_PHASE(ThreatenSceneMain) = 1
						ThreatenSceneMain = CREATE_SYNCHRONIZED_SCENE(<< -46.236, 3096.727, 25.513 >>, << 3.000, 0.000, -162.720 >>)
						TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ThreatenSceneMain, "misstrevor1", "ortega_kneel_loop_ort", INSTANT_BLEND_IN, INSTANT_BLEND_OUT ,SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
						SET_SYNCHRONIZED_SCENE_LOOPED(ThreatenSceneMain,TRUE)
						bTriggerLoopedEndAnim = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bSetUpStageData = FALSE
		SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		SETUP_PEDS_FOR_DIALOGUE()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5,"Kill / Threaten Ortega")
		bGotFlashTimer= FALSE
		bSetUpStageData = TRUE
	ENDIF	
	
//---------¦ SETUP ¦---------
	IF NOT bStageSetup	
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER STAGE START")
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5,"Kill / Threaten Ortega")
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnTruck)
			MANAGE_LOADING_NEED_LIST()
		
			SET_START_AND_END_VECTORS()
			INITALISE_ARRAYS(STAGE_CARAVAN_SMASH)
			
			IF IS_SCREEN_FADED_OUT()
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF
			
			SETTIMERA(0)

			IF HAS_ANIM_DICT_LOADED("misstrevor1")
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(ThreatenSceneMain)
					IF NOT IS_PED_INJURED(npcOrtega.ped)
					AND NOT IS_PED_INJURED(budRon.ped)
						ThreatenSceneMain = CREATE_SYNCHRONIZED_SCENE(<< -46.236, 3096.727, 25.513 >>, << 3.000, 0.000, -162.720 >>)
						TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ThreatenSceneMain, "misstrevor1", "ortega_stand_loop_ort", INSTANT_BLEND_IN, INSTANT_BLEND_OUT ,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT)
						SET_SYNCHRONIZED_SCENE_LOOPED(ThreatenSceneMain,TRUE)
//						TASK_START_SCENARIO_IN_PLACE(budRon.ped,"WORLD_HUMAN_STAND_IMPATIENT")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_ENTITY_PROOFS(vehTrevor.veh,FALSE,FALSE,FALSE,FALSE,FALSE)
			ENDIF
			
			REMOVE_IPL("TRV1_Trail_start")
			REQUEST_IPL("TRV1_Trail_end")
			
			IF IS_SCREEN_FADED_OUT()
				SETUP_TRAILER_DAMAGE(TRUE)
			ENDIF
			
			SET_MAX_WANTED_LEVEL(0)
			bTriggerLoopedEndAnim = FALSE		
			iStageSection = 0
			iOrtegaFlinches = 0
			T1M1_SPECIAL = FALSE
			iOrtegaFlee = 0
			bOrtegaSpared = FALSE

			bDoFails = TRUE
			iTimer = GET_GAME_TIMER()
			SET_LABEL_AS_TRIGGERED("TRIGGER STAGE START",TRUE)
		ENDIF
		
		//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
		IF NOT bTrevorSplashBackEffectOn
			APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
			bTrevorSplashBackEffectOn = TRUE
		ENDIF			
		
		IF IS_SCREEN_FADED_OUT()
			IF MANAGE_MY_TIMER(iTimer,1800)
				safefadeInAndWait()	
				bStageSetup = TRUE
			ENDIF
		ELSE
			bStageSetup = TRUE
		ENDIF
		
		
		
////---------¦ UPDATE ¦---------		
	ELSE	
		IF IS_PED_INJURED(npcOrtega.ped)
			IF iStageSection > 0
			AND iStageSection < 3
				iStageSection = 3
			ENDIF
		ENDIF
		
		IF  iStageSection > 0 
		AND iStageSection < 2
			IF NOT IS_CELLPHONE_DISABLED()
				DISABLE_CELLPHONE(TRUE)
			ENDIF
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_FRONTEND_UP)
		ELSE
			IF IS_CELLPHONE_DISABLED()
				DISABLE_CELLPHONE(FALSE)
			ENDIF
		ENDIF
		
		
		SWITCH iStageSection
			CASE 0 
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT DOES_BLIP_EXIST(npcOrtega.blip)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							TASK_LOOK_AT_ENTITY(npcOrtega.ped,PLAYER_PED_ID(),-1)
							npcOrtega.blip = CREATE_BLIP_FOR_ENTITY(npcOrtega.ped, TRUE)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,FALSE)
					ENDIF
					
					IF NOT IS_PED_INJURED(npcOrtega.ped)
						SET_ENTITY_PROOFS(npcOrtega.ped,FALSE,FALSE,FALSE,FALSE,FALSE)
						SET_PED_CONFIG_FLAG(npcOrtega.ped,PCF_UseKinematicModeWhenStationary,TRUE)
						SET_PED_CONFIG_FLAG(npcOrtega.ped,PCF_RunFromFiresAndExplosions,FALSE)
						FREEZE_ENTITY_POSITION(npcOrtega.ped,FALSE)
						SET_ENTITY_INVINCIBLE(npcOrtega.ped,FALSE)
						SET_PED_CAN_RAGDOLL(npcOrtega.ped,TRUE)
						SET_ENTITY_HEALTH(npcOrtega.ped,110)
					ENDIF
					
					IF NOT IS_PED_INJURED(budRon.ped)
						TASK_LOOK_AT_ENTITY(budRon.ped,PLAYER_PED_ID(),-1)
						STOP_PED_SPEAKING(budRon.ped,TRUE)
					ENDIF
					
					IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN) < 2
						ADD_AMMO_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN,10)
					ENDIF
					IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, TRUE)
					ENDIF
					IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),  WEAPONTYPE_PUMPSHOTGUN, TRUE)
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

					CLEAR_PRINTS()
					SET_TIME_SCALE(1.0)
					iOrtegaFlee = 0
					bReminderPrint = FALSE
					T1M1_P13 = FALSE
					T1M1_IG4A = FALSE
					T1M1_BEG = FALSE
					T1M1_SPECIAL = FALSE
					bTriggerLoopedEndAnim = FALSE
					bSkipped = FALSE
					PRINT_GOD_TEXT(Asset.godThreaten)

					IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_1_THREATEN_ORTEGA")
						START_AUDIO_SCENE("TREVOR_1_THREATEN_ORTEGA")
					ENDIF
					IF NOT IS_PED_INJURED(npcOrtega.ped)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(npcOrtega.ped,"T1M1_GGAA","ORTEGA",SPEECH_PARAMS_FORCE)
					ENDIF
					
					CONTROL_FADE_IN(500)
					iStageSection++
				ENDIF
				
			BREAK
			
			CASE 1
				
				IF NOT IS_PED_INJURED(npcOrtega.ped)
					IF (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), npcOrtega.ped) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), npcOrtega.ped))
					AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(npcOrtega.ped,PLAYER_PED_ID())
					OR GET_IS_PETROL_DECAL_IN_RANGE(GET_ENTITY_COORDS(npcOrtega.ped, FALSE),0.5)
						IF DOES_BLIP_EXIST(npcOrtega.blip)
							REMOVE_BLIP(npcOrtega.blip)
						ENDIF
						IF IS_THIS_PRINT_BEING_DISPLAYED(Asset.godThreaten)
							CLEAR_PRINTS()
						ENDIF
						IF IS_SYNCHRONIZED_SCENE_RUNNING(ThreatenSceneMain)
							ThreatenSceneMain = CREATE_SYNCHRONIZED_SCENE(<< -46.236, 3096.727, 25.513 >>, << 3.000, 0.000, -162.720 >>)
							TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ThreatenSceneMain, "misstrevor1", "ortega_stand_to_kneel_ort", NORMAL_BLEND_IN, NORMAL_BLEND_OUT ,SYNCED_SCENE_USE_PHYSICS| SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT)
							iStageSection++
						ENDIF
					ELSE
						IF NOT T1M1_IG4A
							IF NOT IS_AMBIENT_SPEECH_PLAYING(npcOrtega.ped)
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_IG4A", CONV_PRIORITY_MEDIUM)
										T1M1_IG4A = TRUE
										iOrtegaDialogue = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF MANAGE_MY_TIMER(iOrtegaDialogue,GET_RANDOM_INT_IN_RANGE(10000,12000))
								T1M1_IG4A = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			//if the informant has told trevor what is happening then move on
			CASE 2
			
				IF NOT IS_PED_INJURED(npcOrtega.ped)
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF GET_IS_PETROL_DECAL_IN_RANGE(GET_ENTITY_COORDS(npcOrtega.ped, FALSE),0.5)
							IF NOT IS_AMBIENT_SPEECH_PLAYING(npcOrtega.ped)
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_ORTGAS", CONV_PRIORITY_MEDIUM)
									SETTIMERA(0)
									npcOrtega.blip = CREATE_BLIP_FOR_ENTITY(npcOrtega.ped, TRUE)
									bGotFlashTimer= FALSE
									bGoToSpeach = FALSE
									SETUP_PEDS_FOR_DIALOGUE()
									iStageSection++
								ENDIF
							ENDIF
						ELSE
							SETTIMERA(0)
							npcOrtega.blip = CREATE_BLIP_FOR_ENTITY(npcOrtega.ped, TRUE)
							bGotFlashTimer= FALSE
							bGoToSpeach = FALSE
							SETUP_PEDS_FOR_DIALOGUE()
							iStageSection++
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(npcOrtega.ped)
						IF NOT IS_PED_INJURED(npcOrtega.ped)
							APPLY_FORCE_TO_ENTITY(npcOrtega.ped,APPLY_TYPE_IMPULSE,<<0,4,3>>,<<0,0,0>>,0,FALSE,FALSE,TRUE)
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(npcOrtega.blip)
						REMOVE_BLIP(npcOrtega.blip)
					ENDIF
					CLEAR_PRINTS()
					CLEAR_HELP()
					SET_RADAR_ZOOM(0)
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
					ENDIF
					iStageSection++
				ENDIF 
				
			BREAK
			
			
			CASE 3
				
				IF NOT IS_PED_INJURED(npcOrtega.ped)
					IF NOT T1M1_P13
						IF NOT IS_AMBIENT_SPEECH_PLAYING(npcOrtega.ped)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_THREAT", CONV_PRIORITY_MEDIUM)
										T1M1_P13 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//once the ped is dead or the player gets more than 7 away then move on to the next stage.
				IF IS_PED_INJURED(npcOrtega.ped)
					IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_THREATEN_ORTEGA")
						STOP_AUDIO_SCENE("TREVOR_1_THREATEN_ORTEGA")
						CLEAR_PRINTS()
						CLEAR_HELP()
					ELSE
						IF NOT IS_AUDIO_SCENE_ACTIVE("TREVOR_1_ORTEGA_KILLED")
							START_AUDIO_SCENE("TREVOR_1_ORTEGA_KILLED")
						ENDIF
					ENDIF

					IF T1M1_SHOOT = FALSE
						KILL_ANY_CONVERSATION()
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", bKillString, CONV_PRIORITY_MEDIUM)
							//IF TRIGGER_MUSIC_EVENT("TRV1_END")
								IF DOES_ENTITY_EXIST(npcOrtega.ped)
									IF NOT IS_PED_INJURED(npcOrtega.ped)
										APPLY_FORCE_TO_ENTITY(npcOrtega.ped,APPLY_TYPE_IMPULSE,<<0,4,3>>,<<0,0,0>>,0,FALSE,FALSE,TRUE)
									ENDIF
								ENDIF
								IF DOES_BLIP_EXIST(npcOrtega.blip)
									REMOVE_BLIP(npcOrtega.blip)
								ENDIF
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
								
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, TRUE)
								T1M1_SHOOT = TRUE
							//ENDIF
						ENDIF
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_RONORTD", CONV_PRIORITY_MEDIUM)
									IF NOT IS_PED_INJURED(budRon.ped)
										CLEAR_PED_TASKS(budRon.ped)
									ENDIF
									iStageSection++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					
					IF NOT IS_PED_INJURED(npcOrtega.ped)
						IF IS_PED_RAGDOLL(npcOrtega.ped)
							SET_ENTITY_HEALTH(npcOrtega.ped,0)
						ENDIF
					ENDIF
					
					//check to see if the players ran away.
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(npcOrtega.ped, FALSE)) > 7
						IF DOES_BLIP_EXIST(npcOrtega.blip)
							REMOVE_BLIP(npcOrtega.blip)
						ENDIF
						KILL_FACE_TO_FACE_CONVERSATION()
						IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_NOSH", CONV_PRIORITY_MEDIUM)
							IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(npcOrtega.ped)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(ThreatenSceneMain)
									//IF GET_SYNCHRONIZED_SCENE_PHASE(ThreatenSceneMain) > 0.99
										ThreatenSceneMain = CREATE_SYNCHRONIZED_SCENE(<< -46.236, 3096.727, 25.513 >>, << 3.000, 0.000, -162.720 >>)
										TASK_SYNCHRONIZED_SCENE (npcOrtega.ped, ThreatenSceneMain, "misstrevor1", "ortega_kneel_outro_ort", NORMAL_BLEND_IN, NORMAL_BLEND_OUT ,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON,RBF_PLAYER_IMPACT)
										SET_LABEL_AS_TRIGGERED("ORTEGA SPARED",TRUE)
										IF NOT IS_PED_INJURED(budRon.ped)
											CLEAR_PED_TASKS(budRon.ped)
										ENDIF
										IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
											SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
										ENDIF
										iTimerBeforeFlee = GET_GAME_TIMER()
										bOrtegaSpared = TRUE
										iStageSection++
									//ENDIF
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(budRon.ped)
									CLEAR_PED_TASKS(budRon.ped)
								ENDIF
								TASK_SMART_FLEE_PED(npcOrtega.ped,PLAYER_PED_ID(),200,-1)
								SET_LABEL_AS_TRIGGERED("ORTEGA SPARED",TRUE)
								IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
								ENDIF
								iStageSection++
							ENDIF
						ENDIF
					ELSE
						IF T1M1_P13 = TRUE
							IF NOT T1M1_BEG
								IF GET_IS_PETROL_DECAL_IN_RANGE(GET_ENTITY_COORDS(npcOrtega.ped, FALSE),0.5)
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_ORTGAS", CONV_PRIORITY_MEDIUM)
												T1M1_BEG = TRUE
												iOrtegaDialogue = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_BEG", CONV_PRIORITY_MEDIUM)
												T1M1_BEG = TRUE
												iOrtegaDialogue = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF MANAGE_MY_TIMER(iOrtegaDialogue,GET_RANDOM_INT_IN_RANGE(4000,8000))
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_COWER", CONV_PRIORITY_MEDIUM)
												T1M1_BEG = FALSE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK	
		ENDSWITCH

//---------¦ JSKIP ¦---------
		/*IF JSkip
			iStageSection = 4
		ENDIF*/
	
//---------¦ CLEANUP ¦---------
		IF iStageSection >= 4
			PRINTSTRING("NEXT STAGE")PRINTNL()
			IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_THREATEN_ORTEGA")
				STOP_AUDIO_SCENE("TREVOR_1_THREATEN_ORTEGA")
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor.veh,TRUE)
			ENDIF
			REMOVE_ANIM_DICT("MISSCOMMON@HANDS_UP_FLINCH")

			SET_MAX_WANTED_LEVEL(6)
			SET_RADAR_ZOOM(0)
			
			REMOVE_PED_FOR_DIALOGUE(structConvoPeds,5)
			IF NOT IS_PED_INJURED(budRon.ped)
				STOP_PED_SPEAKING(budRon.ped,FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped,FALSE)
				IF NOT IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
					CLEAR_PED_TASKS(budRon.ped)
					SET_PED_AS_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(npcOrtega.blip)
				REMOVE_BLIP(npcOrtega.blip)
			ENDIF
			RESET_MISSION_STAGE_INTS()
			RESET_TREVOR_RAGE()
			bStageSetup = FALSE
			bSetUpStageData = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
/// Play the trailer push cutscene
PROC RUN_DROP_OFF_RON_CUTSCENE()

	bcutsceneplaying = TRUE
	
//	IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
//		SET_VEH_RADIO_STATION(vehTrevor.veh,"OFF")
//	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
				b_skipped_mocap = TRUE
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ENDIF
	#ENDIF
	
	WHILE bcutsceneplaying
	
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_MrPhilips")

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					bcutsceneplaying = TRUE
					SET_START_AND_END_VECTORS()
					INITALISE_ARRAYS(STAGE_INIT_MISSION)
					REQUEST_CUTSCENE("TRV_1_MCS_4")
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						REQUEST_ANIM_DICT("misstrevor1bathroom")
						REQUEST_ANIM_DICT("misstrevor1")
						REQUEST_VEHICLE_RECORDING(1,"T1Safehouse")
					ENDIF
					
					i_current_event++
				ELIF i_current_event = 1
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					AND (HAS_ANIM_DICT_LOADED("misstrevor1bathroom") OR IS_REPEAT_PLAY_ACTIVE())
					AND (HAS_ANIM_DICT_LOADED("misstrevor1") OR IS_REPEAT_PLAY_ACTIVE())
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							WAIT(0)
						ENDIF
						
						IF NOT IS_PED_INJURED(budRon.ped)
							REGISTER_ENTITY_FOR_CUTSCENE(budRon.ped, "Ron", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF 
						
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							REGISTER_ENTITY_FOR_CUTSCENE(vehTrevor.veh, "Trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						//Wait a couple of frames before clearing/warping stuff, so the cutscene is active first.
						WAIT(0)
					
						CLEAR_AREA(vStageEnd,100,TRUE)
						DISPLAY_HUD(FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, TRUE, FALSE)
						
						b_skipped_mocap = FALSE
						i_current_event = 0
						e_section_stage = SECTION_STAGE_RUNNING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			SETUP_MISSION_REQUIREMENT(REQ_TRAILER_INTERIOR, <<0,0,0>>)
			
			IF IS_CUTSCENE_PLAYING()
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(TRUE)
					PRINTLN("@@@@@@@@@@ SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(TRUE) @@@@@@@@@@")
				ENDIF
				IF GET_CUTSCENE_TIME() > 10000
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_SKIP_CUTSCENE)
				ENDIF
				IF bClearCutscenArea = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						SET_VEH_RADIO_STATION(vehTrevor.veh,"OFF")
					ENDIF
					SET_FRONTEND_RADIO_ACTIVE(FALSE)
					CLEAR_AREA(<< 1986.1279, 3814.4370, 31.2913 >>, 200.0, TRUE)
					CONTROL_FADE_IN(500)
					bClearCutscenArea = TRUE
				ELSE
					IF NOT b_skipped_mocap
						IF WAS_CUTSCENE_SKIPPED()
							CONTROL_FADE_OUT(500)
							e_section_stage = SECTION_STAGE_SKIP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_car")
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"T1Safehouse")
						IF DOES_ENTITY_EXIST(vehTrevor.veh)
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								SET_FRONTEND_RADIO_ACTIVE(FALSE)
								SET_VEHICLE_LIGHTS(vehTrevor.veh,SET_VEHICLE_LIGHTS_OFF)
//								SET_ENTITY_COORDS(vehTrevor.veh,<<1980.51, 3828.44, 32.37>>)
//								SET_ENTITY_HEADING(vehTrevor.veh,293.26)
//								SET_VEHICLE_ENGINE_ON(vehTrevor.veh,FALSE,TRUE)
//								SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
//								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor.veh)
//									SET_VEHICLE_ENGINE_ON(vehTrevor.veh,TRUE,TRUE)
//									START_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh,1,"T1Safehouse")
//									
//									SET_PLAYBACK_SPEED(vehTrevor.veh,-1)
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(),vehTrevor.veh,VS_DRIVER)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("START LOAD SCENE SAFE")
					IF GET_CUTSCENE_TIME() > 4178
						NEW_LOAD_SCENE_START_SPHERE(<<1973.8455, 3818.4473, 32.4363>>,  15.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
						SET_LABEL_AS_TRIGGERED("START LOAD SCENE SAFE",TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					
					DISPLAY_HUD(FALSE)
					DESTROY_CAM(cutscene_cam)

					SET_STATIC_EMITTER_ENABLED("SE_TREVOR_TRAILER_RADIO_01",TRUE)
					SET_EMITTER_RADIO_STATION("SE_TREVOR_TRAILER_RADIO_01","RADIO_06_COUNTRY")
					
					IF NOT DOES_CAM_EXIST(cutscene_cam)			
						cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
						SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
					ENDIF

					PLAY_CAM_ANIM(cutscene_cam,  "trv_trailer_tut_ext_cam","misstrevor1",<< 1986.238, 3815.098, 32.793 >>,<< 0.000, 0.000, -154.000 >>)
					SET_CAM_ACTIVE(cutscene_cam,TRUE)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					
					CLEAR_AREA(<<1973.8455, 3818.4473, 32.4363>>,30.0,TRUE)
					
					IF DOES_ENTITY_EXIST(budWade.ped)
						DELETE_PED(budWade.ped)
					ENDIF
					
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
				ELSE
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
			AND bClearCutscenArea = TRUE
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP

			IF b_skipped_mocap
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_spark1)
				REMOVE_PARTICLE_FX(ptfx_spark1)
			ENDIF
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_spark2)
				REMOVE_PARTICLE_FX(ptfx_spark2)
			ENDIF
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_spark3)
				REMOVE_PARTICLE_FX(ptfx_spark3)
			ENDIF
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_spark4)
				REMOVE_PARTICLE_FX(ptfx_spark4)
			ENDIF
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_crash_dust)
				REMOVE_PARTICLE_FX(ptfx_crash_dust)
			ENDIF

			DISPLAY_RADAR(FALSE)
			
			//Setup buddy relationship groups etc.
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, TRUE, FALSE)
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				DISPLAY_HUD(FALSE)
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			//SET_GAMEPLAY_CAM_WORLD_HEADING(11.7851)
			
			REMOVE_CUTSCENE()
			
			IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
				SET_VEHICLE_DOOR_SHUT(vehTrevor.veh,SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_SHUT(vehTrevor.veh,SC_DOOR_FRONT_RIGHT)
			ENDIF
			
			REPLAY_STOP_EVENT()
			
			i_current_event = 0
			e_section_stage = SECTION_STAGE_SETUP
			bcutsceneplaying = FALSE
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDWHILE
ENDPROC


//PURPOSE:		Performs the section where Trevor tortures the victim for the first time
BOOL bGetInTruck
BOOL bTriggerEndMusic = FALSE

FUNC BOOL stageDropOffRon()
	
	MANAGE_ORTEGA_FLEE_RESPONSE()
	
	IF DOES_ENTITY_EXIST(npcOrtega.ped)
		IF IS_PED_INJURED(npcOrtega.ped)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED, TRUE)
			STAT_SET_BOOL(SP_KILLED_ORTEGA, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(npcOrtega.ped)
		IF NOT IS_PED_INJURED(npcOrtega.ped)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(npcOrtega.ped, FALSE)) > 100
				IF NOT IS_ENTITY_ON_SCREEN(npcOrtega.ped)
					DELETE_PED(npcOrtega.ped)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_ORTEGA)
				ENDIF
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(npcOrtega.ped,FALSE)) > 100
				IF NOT IS_ENTITY_ON_SCREEN(npcOrtega.ped)	
					DELETE_PED(npcOrtega.ped)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_ORTEGA)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF NOT HAS_LABEL_BEEN_TRIGGERED("TREVOR_1_DRIVE_HOME")
		IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
				START_AUDIO_SCENE("TREVOR_1_DRIVE_HOME")
				SET_LABEL_AS_TRIGGERED("TREVOR_1_DRIVE_HOME",TRUE)
			ENDIF
		ENDIF
	ENDIF

	IF bSetUpStageData = FALSE
		SETUP_PEDS_FOR_DIALOGUE()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Drop Off Ron",TRUE)
		bSetUpStageData = TRUE
		bGoToSpeach = FALSE
		SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		IF DOES_BLIP_EXIST(npcOrtega.blip)
			REMOVE_BLIP(npcOrtega.blip)
		ENDIF
	ENDIF	
	
//---------¦ SETUP ¦---------
	IF NOT bStageSetup	
		
		IF bSkipped
			IF IPL_GROUP_SWAP_IS_READY()
				IPL_GROUP_SWAP_FINISH()
				IF IS_IPL_ACTIVE("TRV1_Trail_end")
				AND IS_IPL_ACTIVE("CS3_05_water_grp2")
					PRINTLN("@@@@@@@@@@@@@@ IS_IPL_ACTIVE @@@@@@@@@@@@@@@")
					bSkipped = FALSE
				ELSE
					WAIT(0)
				ENDIF
			ELSE
				WAIT(0)
			ENDIF
		ELSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Drop Off Ron",TRUE)
			SET_PED_POPULATION_BUDGET(3)		
			SET_VEHICLE_POPULATION_BUDGET(3)
			SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST()
			SET_RADAR_ZOOM(0)
			SET_START_AND_END_VECTORS()
			//bStopTimer = FALSE													
			iStageSection = 0
			bDoFails = TRUE
			bGetInTruck = FALSE
			bTriggerEndMusic = FALSE
			REMOVE_IPL("TRV1_Trail_start")
			REQUEST_IPL("TRV1_Trail_end")

			//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
			IF NOT bTrevorSplashBackEffectOn
				APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
				bTrevorSplashBackEffectOn = TRUE
			ENDIF	
			
			bStageSetup = TRUE
		ENDIF

		
//---------¦ UPDATE ¦---------		
	ELSE	
		SWITCH iStageSection
			CASE 0
				PRINTSTRING("IN HERE")PRINTNL()
				safefadeInAndWait()				
				#IF IS_DEBUG_BUILD
					DONT_DO_J_SKIP(sLocatesData)
				#ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					MODIFY_VEHICLE_TOP_SPEED(vehTrevor.veh,0)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("BAJS")
						IF NOT IS_PED_INJURED(budRon.ped)
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTrevor.veh, TRUE)
							IF NOT IS_PED_IN_VEHICLE(budRon.ped, vehTrevor.veh, TRUE)
								IF GET_SCRIPT_TASK_STATUS(budRon.ped, SCRIPT_TASK_ENTER_VEHICLE) = FINISHED_TASK
									//CLEAR_PED_TASKS_IMMEDIATELY(budRon.ped)
									IF NOT IS_PED_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
										SET_PED_AS_GROUP_MEMBER(budRon.ped, PLAYER_GROUP_ID())
									ENDIF
									TASK_ENTER_VEHICLE(budRon.ped,vehTrevor.veh,DEFAULT_TIME_NEVER_WARP,VS_FRONT_RIGHT)
									SET_LABEL_AS_TRIGGERED("BAJS", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<1952.2297, 3787.3960, 31.2922>>) < DEFAULT_CUTSCENE_LOAD_DIST
				AND NOT IS_PLAYER_CHANGING_CLOTHES()
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						REQUEST_ANIM_DICT("misstrevor1bathroom")
						REQUEST_ANIM_DICT("misstrevor1")
						REQUEST_VEHICLE_RECORDING(1,"T1Safehouse")
					ENDIF
					REQUEST_CUTSCENE("TRV_1_MCS_4")
				ELSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<1952.2297, 3787.3960, 31.2922>>) > DEFAULT_CUTSCENE_UNLOAD_DIST
					OR IS_PLAYER_CHANGING_CLOTHES()
						REMOVE_CUTSCENE()
					ENDIF
				ENDIF
				
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(	sLocatesData,<<1952.2297, 3787.3960, 31.2922>> , <<7,7,LOCATE_SIZE_HEIGHT>>, TRUE, budRon.ped, 
																vehTrevor.veh, Asset.godDriveToRons, Asset.godWaitForRon, Asset.godGetInTrevorCar, Asset.godGetBackToCar,FALSE,TRUE)
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehTrevor.veh,DEFAULT_VEH_STOPPING_DISTANCE)
						KILL_FACE_TO_FACE_CONVERSATION()
						RESET_MISSION_STAGE_INTS()
						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
						
						iStageSection++
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
							IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_ORTEGA_KILLED")
								STOP_AUDIO_SCENE("TREVOR_1_ORTEGA_KILLED")
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CLEAR LOOK AT")
							IF NOT IS_PED_INJURED(budRon.ped)
								RELEASE_SCRIPT_AUDIO_BANK()
								TASK_CLEAR_LOOK_AT(budRon.ped)
								SET_LABEL_AS_TRIGGERED("CLEAR LOOK AT",TRUE)
							ENDIF
						ENDIF
						IF bGoToSpeach = TRUE
							IF bTriggerEndMusic = FALSE
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									PREPARE_MUSIC_EVENT("TRV1_END_TRUCK")
									IF TRIGGER_MUSIC_EVENT("TRV1_END_TRUCK")
										bTriggerEndMusic = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF IS_IT_SAFE_TO_PLAY_DIALOG()
							IF bGoToSpeach = FALSE
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_DR1", CONV_PRIORITY_MEDIUM)
											REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGH)
											SET_RADAR_ZOOM(0)
											bGoToSpeach = TRUE
										ENDIF
									ELSE
										IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_EIA", CONV_PRIORITY_MEDIUM)
											REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGH)
											SET_RADAR_ZOOM(0)
											bGoToSpeach = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("HASGOTINVEHICLE")
							IF bGetInTruck = FALSE
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_LETSG", CONV_PRIORITY_MEDIUM)
										IF NOT IS_PED_INJURED(budRon.ped)
											TASK_LOOK_AT_ENTITY(budRon.ped,PLAYER_PED_ID(),6000)
										ENDIF
										bGetInTruck = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("HASGOTINVEHICLE")
						IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
								SET_LABEL_AS_TRIGGERED("HASGOTINVEHICLE",TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF iStageSection = 0 
						IF bGoToSpeach = TRUE
							//MANAGE PAUSE DIALOGUE
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									CLEAR_PRINTS()
								ENDIF
							//Play conversation
							ELSE
								IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
									CLEAR_PRINTS()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTrevor.veh)
					IF NOT DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
						sLocatesData.vehicleBlip = ADD_BLIP_FOR_ENTITY(vehTrevor.veh)
						SET_BLIP_COLOUR(sLocatesData.vehicleBlip,BLIP_COLOUR_BLUE)
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_AUDIO_SCENE_ACTIVE("TREVOR_1_DRIVE_HOME")
						STOP_AUDIO_SCENE("TREVOR_1_DRIVE_HOME")
					ENDIF
					iStageSection++
				ENDIF
			BREAK
			
		ENDSWITCH
			

//---------¦ JSKIP ¦---------
		/*IF JSkip
			iStageSection = 1
		ENDIF*/
	
//---------¦ CLEANUP ¦---------
		IF iStageSection >= 2
			IF DOES_BLIP_EXIST(blipVeh)
				REMOVE_BLIP(blipVeh)
			ENDIF
			IF DOES_BLIP_EXIST(blipDest)
				REMOVE_BLIP(blipDest)
			ENDIF
			IF DOES_BLIP_EXIST(blipPed)
				REMOVE_BLIP(blipPed)
			ENDIF
			bSetUpStageData = FALSE
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SAFE_HOUSE_CUTSCENE()

	SETTIMERA(0)
	bcutsceneplaying = TRUE
	SETUP_MISSION_REQUIREMENT(REQ_TRAILER_INTERIOR, <<0,0,0>>)
	OVERRIDE_LODSCALE_THIS_FRAME(1.0)
	SET_FRONTEND_RADIO_ACTIVE(FALSE)
	
	IF DOES_CAM_EXIST(cutscene_cam)
		PRINTSTRING("cam phase: ")PRINTFLOAT(GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam))
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
				STOP_SYNCHRONIZED_ENTITY_ANIM(PLAYER_PED_ID(),NORMAL_BLEND_OUT,TRUE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				icutsceneprog = 6
			ENDIF
		ENDIF
	#ENDIF

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		
		IF icutsceneprog >2 
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() // controls the cutscene skip
				SETTIMERB(7001)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<<1972.8237, 3816.6880, 32.4287>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),208.2972)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 208.2972, FALSE)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
				
				// url:bugstar:2041838
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_FIRST_PERSON_AIM_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(-1,1)
					SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING()
				ENDIF
						
				NEW_LOAD_SCENE_STOP()
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				SET_WIDESCREEN_BORDERS(FALSE,0)
				CLEAR_HELP()
				CLEAR_PRINTS()
				
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					SET_VEHICLE_ENGINE_ON(vehTrevor.veh,FALSE,TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
					SET_VEHICLE_LIGHTS(vehTrevor.veh,NO_VEHICLE_LIGHT_OVERRIDE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					SET_ENTITY_LOD_DIST(vehTrevor.veh,-1)
				ENDIF
				DISPLAY_HUD(TRUE)
				SET_FRONTEND_RADIO_ACTIVE(TRUE)
				icutsceneprog = 6
				
				bcutsceneplaying = FALSE
			ELSE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("T1Safehouse")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("Leave vehicle")
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor.veh)
						SET_VEHICLE_LIGHTS(vehTrevor.veh, FORCE_VEHICLE_LIGHTS_OFF)
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
						SET_LABEL_AS_TRIGGERED("Leave vehicle",TRUE)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH icutsceneprog
			
			CASE 0
				CONTROL_FADE_IN(500)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

				SET_WIDESCREEN_BORDERS(TRUE,0)
				PRINT_HELP("TRV_SAFEH_1")
				SETTIMERB(0)
				REQUEST_ANIM_DICT("misstrevor1bathroom")
				IF NOT HAS_LABEL_BEEN_TRIGGERED("PIN INTERIOR END")
					IF SETUP_MISSION_REQUIREMENT(REQ_TRAILER_INTERIOR, <<0,0,0>>)
						SET_LABEL_AS_TRIGGERED("PIN INTERIOR END",TRUE)
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(),vehTrevor.veh,VS_DRIVER)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehTrevor.veh)
					ENDIF
					SET_VEHICLE_LIGHTS(vehTrevor.veh,FORCE_VEHICLE_LIGHTS_OFF)
				ENDIF
				REQUEST_VEHICLE_RECORDING(1,"T1Safehouse")
				LOAD_STREAM("TREVOR_SAFE_HOUSE_INTRO_MASTER")
				
				icutsceneprog++
			BREAK
			
			CASE 1
				IF NOT HAS_LABEL_BEEN_TRIGGERED("T1Safehouse")
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"T1Safehouse")
						IF DOES_ENTITY_EXIST(vehTrevor.veh)
							IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor.veh)
									SET_VEHICLE_ENGINE_ON(vehTrevor.veh,TRUE,TRUE)
									START_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh,1,"T1Safehouse")
									//SET_VEH_RADIO_STATION(vehTrevor.veh,"OFF")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTrevor.veh,3000)
									SET_PLAYBACK_SPEED(vehTrevor.veh,0.7)
									SET_VEHICLE_LIGHTS(vehTrevor.veh,FORCE_VEHICLE_LIGHTS_ON)
									SET_LABEL_AS_TRIGGERED("T1Safehouse",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF LOAD_STREAM("TREVOR_SAFE_HOUSE_INTRO_MASTER")
				OR GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) > 0.45
					IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) > 0.5
						NEW_LOAD_SCENE_START_SPHERE(<<1973.7573, 3816.8745, 33.8607>>,  8.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
						icutsceneprog++
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) = 1
					SET_FRONTEND_RADIO_ACTIVE(TRUE) 
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1969.8226, 3818.3149, 32.4985>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 152.1604)
					PLAY_CAM_ANIM(cutscene_cam,  "trv_trailer_tut_int_cam", "misstrevor1",<< 1971.579, 3819.524, 32.956 >>,<< 0.000, 0.000, 121.250 >>)
					SET_CAM_ACTIVE(cutscene_cam,TRUE)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					SET_WIDESCREEN_BORDERS(TRUE,0)
					PRINT_HELP("TRV_SAFEH_2")
					SETTIMERB(0)
					NEW_LOAD_SCENE_STOP()
					PLAY_STREAM_FRONTEND()
					icutsceneprog++
				ENDIF
			BREAK
			
			CASE 3
				IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) > 0.5
					PRINT_HELP("TRV_SAFEH_3")
					icutsceneprog++
				ENDIF
			BREAK
			
			CASE 4
				IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) >= 0.99
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iTrevorPissing)
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							iTrevorPissing = CREATE_SYNCHRONIZED_SCENE(vTrevorPissPos, vTrevorPissRot)
							TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(),iTrevorPissing, "misstrevor1bathroom", "trevor_peeing", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE)
							SET_SYNCHRONIZED_SCENE_LOOPED(iTrevorPissing,FALSE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(),TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF			
			
			
				IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) = 1
					PLAY_SYNCHRONIZED_CAM_ANIM(cutscene_cam, iTrevorPissing, "trevor_peeing_cam", "misstrevor1bathroom")
					SET_CAM_ACTIVE(cutscene_cam,TRUE)
					icutsceneprog++
				ENDIF
			BREAK
			
			CASE 5
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iTrevorPissing)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iTrevorPissing) > 0.99
						IF DOES_CAM_EXIST(cutscene_cam)
							STOP_CAM_POINTING(cutscene_cam)
							SET_CAM_ACTIVE(cutscene_cam,FALSE)
							DESTROY_CAM(cutscene_cam)
							RENDER_SCRIPT_CAMS(FALSE,TRUE,1500)
						ENDIF
						
						STOP_SYNCHRONIZED_ENTITY_ANIM(PLAYER_PED_ID(),NORMAL_BLEND_OUT,TRUE)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						
						NEW_LOAD_SCENE_STOP()
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						SET_WIDESCREEN_BORDERS(FALSE,0)
						CLEAR_HELP()
						CLEAR_PRINTS()
						icutsceneprog++
					ENDIF
				ENDIF
			BREAK
			
			
			CASE 6
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					SET_VEHICLE_ENGINE_ON(vehTrevor.veh,FALSE,TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor.veh)
					SET_VEHICLE_LIGHTS(vehTrevor.veh,NO_VEHICLE_LIGHT_OVERRIDE)
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					SET_ENTITY_LOD_DIST(vehTrevor.veh,-1)
				ENDIF
				DISPLAY_HUD(TRUE)
				SET_FRONTEND_RADIO_ACTIVE(TRUE)
				icutsceneprog = 0
				
				SET_MULTIHEAD_SAFE(FALSE)
				SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(FALSE)
				PRINTLN("@@@@@@@@@@ SET_CUTSCENE_MULTIHEAD_FADE_MANUAL(FALSE) @@@@@@@@@@")
				bcutsceneplaying = FALSE
			BREAK

		ENDSWITCH
		
	ENDWHILE

ENDPROC

//PURPOSE:		Performs the cutscene which introduces the garage and the three characters
FUNC BOOL stageCutsceneDropOffRon()
	
	IF bSetUpStageData = FALSE
		SETUP_PEDS_FOR_DIALOGUE()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Drop Off Ron",TRUE)
		bSetUpStageData = TRUE
		PRINTSTRING("<STAGE_CUTSCENE_DROP_OFF_RON - SETUP1>")PRINTNL()
	ENDIF	
	
//	IF CREATE_CONVERSATION(structConvoPeds, "T1M1AUD", "T1M1_AR1", CONV_PRIORITY_MEDIUM)
//	ENDIF

//---------¦ SETUP ¦---------
	IF NOT bStageSetup	
		RESET_NEED_LIST()
		MANAGE_LOADING_NEED_LIST()
		SET_RADAR_ZOOM(0)
		SET_START_AND_END_VECTORS()												
		iStageSection = 0
		bStageSetup = TRUE
		bDoFails = TRUE
		
		//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
		IF NOT bTrevorSplashBackEffectOn
			APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
			bTrevorSplashBackEffectOn = TRUE
		ENDIF			
		
		REQUEST_CUTSCENE("TRV_1_MCS_4")
		PRINTSTRING("<STAGE_CUTSCENE_DROP_OFF_RON - SETUP2>")PRINTNL()
//---------¦ UPDATE ¦---------		
	ELSE
		SWITCH iStageSection
		
			CASE 0
				i_current_event = 0
				//SETUP
				REQUEST_CUTSCENE("TRV_1_MCS_4")
				PRINTSTRING("<STAGE_CUTSCENE_DROP_OFF_RON - SETUP3>")PRINTNL()	
				KILL_ANY_CONVERSATION()
				iStageSection ++
			BREAK
			
			CASE 1
				PRINTSTRING("<STAGE_CUTSCENE_DROP_OFF_RON - SETUP4>")PRINTNL()	
				RUN_DROP_OFF_RON_CUTSCENE()
				icutsceneprog = 0
				iStageSection ++
			BREAK
			
			CASE 2
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					SAFE_HOUSE_CUTSCENE()
				ENDIF
				DISPLAY_HUD(TRUE)
				iStageSection ++
			BREAK
			
			CASE 3
			
			BREAK
			
		ENDSWITCH
		
//---------¦ JSKIP ¦---------
		/*IF JSkip
		OR (IS_CUTSCENE_SKIP_BUTTON_PRESSED() AND ReadyToSkipCutscene())	
			doEndCutscene()
			
			iStageSection = 2
		ENDIF*/
				
//---------¦ CLEANUP ¦---------
		IF iStageSection >= 3		

			REMOVE_SCENARIO_BLOCKING_AREAS()
			//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			RESET_MISSION_STAGE_INTS()
			INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, TRUE, FALSE)
			doEndCutscene()
			bStageSetup = FALSE
			//bCameraShotSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_PLAYER_IS_TREVOR()
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
	ELSE		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_FADE_OUT_WITH_WAIT(BOOL bHideHud = FALSE)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		WHILE NOT IS_SCREEN_FADED_OUT()
			IF bHideHud
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			ENDIF
		
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

BOOL bSkipLoaded = FALSE

/// PURPOSE:
/// Play the into mocap
/// -cutscene_tty=debug3
PROC RUN_OPENING_CUTSCENE()

	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying
	
		WAIT(0)
		IF NOT IS_CUTSCENE_PLAYING()
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_MrPhilips")
		
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		
		#IF IS_DEBUG_BUILD
			IF IS_CUTSCENE_ACTIVE()
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
					b_skipped_mocap = TRUE
					PRINTSTRING("e_section_stage = SECTION_STAGE_SKIP")PRINTNL()
					e_section_stage = SECTION_STAGE_SKIP
				ENDIF
			ENDIF
		#ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
		ENDIF
		
		IF bClearCutscenArea = TRUE
			IF IS_CUTSCENE_ACTIVE()
				IF NOT DOES_ENTITY_EXIST(budRon.ped)
					ENTITY_INDEX entity_ron = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ron")
					IF DOES_ENTITY_EXIST(entity_ron)
						budRon.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_ron)
						PRINTSTRING("GOT RON")PRINTNL()
					ENDIF
				ELSE
					IF budRon.iAction = 0
						IF NOT IS_PED_INJURED(budRon.ped)
							SET_UP_RON_COMPONENTS()
							budRon.iAction = 1
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(budWade.ped)
					ENTITY_INDEX entity_wade = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Wade")
					IF DOES_ENTITY_EXIST(entity_wade)
						budWade.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_wade)
						PRINTSTRING("GOT WADE")PRINTNL()
					ENDIF
				ELSE
					IF budWade.iAction = 0
						SET_UP_WADE_COMPONENTS()
						budWade.iAction = 1
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedJohnny)
					ENTITY_INDEX entity_johnny = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Johnny")
					IF DOES_ENTITY_EXIST(entity_johnny)
						pedJohnny = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_johnny)
						PRINTSTRING("GOT JOHNNY")PRINTNL()
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedAshley)
					ENTITY_INDEX entity_ashley = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ashley")
					IF DOES_ENTITY_EXIST(entity_ashley)
						pedAshley = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_ashley)
						PRINTSTRING("GOT ASHLEY")PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
				//	IF IS_REPLAY_IN_PROGRESS()
						SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE)
				//	ENDIF
					
					IF IS_REPEAT_PLAY_ACTIVE()
						IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
						OR NOT DOES_ENTITY_EXIST( sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR, <<0,0,0>>)
								WAIT(0)
							ENDWHILE
							
							SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
						ENDIF
					ENDIF
					
					IF IS_REPEAT_PLAY_ACTIVE()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							SET_GAME_PAUSES_FOR_STREAMING(FALSE)
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED_ID(), TRUE)
								ENDIF
							ENDIF
							NEW_LOAD_SCENE_START_SPHERE(<<1975.3849, 3818.8652, 32.4363>>, 1.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE | NEWLOADSCENE_FLAG_REQUIRE_COLLISION) //change to 1
							SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
							SET_CLOCK_TIME(13,00,00)
							SETUP_MISSION_REQUIREMENT(REQ_TRAILER_INTERIOR, <<0,0,0>>)
							WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
								WAIT(0)
							ENDWHILE
						ELSE
							//create trev
							WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR, <<0,0,0>>)
								WAIT(0)
							ENDWHILE
							SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
						ENDIF
					ENDIF

					SET_PED_POPULATION_BUDGET(0)		
					SET_VEHICLE_POPULATION_BUDGET(0)
					
					bcutsceneplaying = TRUE
					SET_START_AND_END_VECTORS()
					INITALISE_ARRAYS(STAGE_INIT_MISSION)
					REQUEST_ANIM_DICT("misstrevor1")
				
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						#IF NOT IS_JAPANESE_BUILD
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TREVOR_1_INT",CS_SECTION_1 |CS_SECTION_2 | CS_SECTION_3| CS_SECTION_5| CS_SECTION_6| CS_SECTION_7| CS_SECTION_8| CS_SECTION_9| CS_SECTION_10| CS_SECTION_11| CS_SECTION_12 | CS_SECTION_13)
						#ENDIF
						#IF IS_JAPANESE_BUILD
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TREVOR_1_INT_JAP",CS_SECTION_1 |CS_SECTION_2 | CS_SECTION_3| CS_SECTION_5| CS_SECTION_6| CS_SECTION_7| CS_SECTION_8| CS_SECTION_9| CS_SECTION_10| CS_SECTION_11 | CS_SECTION_12 | CS_SECTION_13)
						#ENDIF
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						#IF NOT IS_JAPANESE_BUILD
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TREVOR_1_INT",CS_SECTION_3| CS_SECTION_5| CS_SECTION_6| CS_SECTION_7| CS_SECTION_8| CS_SECTION_9| CS_SECTION_10| CS_SECTION_11| CS_SECTION_12 | CS_SECTION_13)
						#ENDIF
						#IF IS_JAPANESE_BUILD
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TREVOR_1_INT_JAP",CS_SECTION_3| CS_SECTION_5| CS_SECTION_6| CS_SECTION_7| CS_SECTION_8| CS_SECTION_9| CS_SECTION_10| CS_SECTION_11| CS_SECTION_12 | CS_SECTION_13)
						#ENDIF
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						#IF NOT IS_JAPANESE_BUILD
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TREVOR_1_INT",CS_SECTION_5 | CS_SECTION_6| CS_SECTION_7| CS_SECTION_8| CS_SECTION_9| CS_SECTION_10| CS_SECTION_11| CS_SECTION_12 | CS_SECTION_13)
						#ENDIF
						#IF IS_JAPANESE_BUILD
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("TREVOR_1_INT_JAP",CS_SECTION_5 | CS_SECTION_6| CS_SECTION_7| CS_SECTION_8| CS_SECTION_9| CS_SECTION_10| CS_SECTION_11| CS_SECTION_12 | CS_SECTION_13)
						#ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
							PRINTSTRING("ASSIGNING ENTITY IDS - MIKE")PRINTNL()
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0],TRUE,TRUE)
							sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = g_sTriggerSceneAssets.ped[0]
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
							PRINTSTRING("ASSIGNING ENTITY IDS - TREVOR")PRINTNL()
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1],TRUE,TRUE)
							sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = g_sTriggerSceneAssets.ped[1]
						ENDIF
					ENDIF
	
					Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
					SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)

					bSetJohnnyComponent = FALSE
					bClearCutscenArea = FALSE
					bSkipLoaded = FALSE
					budWade.iAction = 0
					iJohnnyAndAshley = 0
					b_skipped_mocap = FALSE
					i_current_event = 1
				ELIF i_current_event = 1
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
					ELSE
						IF IS_REPEAT_PLAY_ACTIVE()
							SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_MICHAEL,"Michael")
						ENDIF
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())
					ELSE
						IF IS_REPEAT_PLAY_ACTIVE()
							SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_FRANKLIN,"Franklin")
						ENDIF
					ENDIF
					
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())
//					ENDIF
					
					PRINTLN("!!!COMPONENTS SET")
					PRINTLN("!!!CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY() =")PRINTLN(CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY())
					
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_HEAD,0,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_BERD ,0,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_HAIR,0,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_TORSO,0,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_HAND,0,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_FEET,10,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_SPECIAL,0,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_SPECIAL2,0,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_DECL,0,0)
//					SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor",PED_COMP_JBIB,0,0)

					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						IF NOT IS_REPEAT_PLAY_ACTIVE()
							IF IS_SCREEN_FADED_OUT()
								DO_SCREEN_FADE_IN(500)
							ELSE
								WAIT(0)
							ENDIF
						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						      MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
						      TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
							  SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							  SET_FOCUS_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							ENDIF
						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						      MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
						      TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
						      SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							  SET_FOCUS_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ENDIF
						ENDIF

						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
							ENDIF
						ENDIF
						
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Ron", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_RON))
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Wade", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_WADE))
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Johnny", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_JOHNNYKLEBITZ)
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Ashley", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_ASHLEY) 
						REQUEST_ANIM_DICT("misstrevor1")
						
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_TREVOR_1)
												
						START_CUTSCENE()
						//Wait a couple of frames before clearing/warping stuff, so the cutscene is active first.
						//WAIT(0)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						REMOVE_IPL("cs4_04_trash")
						
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						e_section_stage = SECTION_STAGE_RUNNING

					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
			
		IF e_section_stage = SECTION_STAGE_RUNNING
		
			PRINTSTRING("e_section_stage = IN HERE - e_section_stage = SECTION_STAGE_RUNNING")PRINTNL()
			IF bClearCutscenArea = TRUE
				IF NOT b_skipped_mocap
					IF MANAGE_MY_TIMER(iSkipCutsceneTimer,2000)
					AND GET_CUTSCENE_TIME() < 225005
						IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
							e_section_stage = SECTION_STAGE_SKIP
							SET_CUTSCENE_FADE_VALUES(FALSE,FALSE,FALSE,FALSE)
							
							CLEAR_AREA(<<2001.83, 3830.42, 31.28>>,15,TRUE)
							b_skipped_mocap = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_CUTSCENE_PLAYING()
				IF IS_REPEAT_PLAY_ACTIVE()
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
				
				
				//[MF] Fix for B* 1740311
				IF GET_CUTSCENE_TIME() > 234894
//					PRINTLN("@@@@@@@@@@@@@@ GET_CUTSCENE_TIME() > 224894 @@@@@@@@@@@@@")
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SKIP_CUTSCENE)
					SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
				ENDIF
				
				//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
				IF NOT bTrevorSplashBackEffectOn
					IF GET_CUTSCENE_TIME() > 202893
						APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
						bTrevorSplashBackEffectOn = TRUE
					ENDIF
				ENDIF

				#IF NOT IS_JAPANESE_BUILD
					IF GET_CUTSCENE_TIME() > 121400
						SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<0,0,0>>)
					ENDIF
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						IF GET_CUTSCENE_TIME() > 96400
							SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
							SET_CLOCK_TIME(13,00,00)
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("STREAMING REQUEST")
							IF GET_CUTSCENE_TIME() > 73934.326
								//SET_GAME_PAUSES_FOR_STREAMING(FALSE)
//								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
//									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//										SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED_ID(), TRUE)
//									ENDIF
//								ENDIF
								//NEW_LOAD_SCENE_START_SPHERE(<<1975.3849, 3818.8652, 32.4363>>, 1.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE | NEWLOADSCENE_FLAG_REQUIRE_COLLISION) //change to 1
								PRINTSTRING("STREAMING REQUEST DONE")PRINTNL()
								SET_LABEL_AS_TRIGGERED("STREAMING REQUEST",TRUE)
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("PIN INTERIOR")
								//IF SETUP_MISSION_REQUIREMENT(REQ_TRAILER_INTERIOR, <<0,0,0>>)
									//SET_LABEL_AS_TRIGGERED("PIN INTERIOR",TRUE)
								//ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SWITCH FOCUS")
						IF GET_CUTSCENE_TIME() > 88467.674
//							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//								SET_FOCUS_ENTITY(PLAYER_PED_ID())
//							ENDIF
							SET_LABEL_AS_TRIGGERED("SWITCH FOCUS",TRUE)
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("PAUSE FOR STREAMING")
//							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//								IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
//									REQUEST_COLLISION_AT_COORD(<<1992.3885, 3830.4963, 31.1573>>)
//								ENDIF
//							ENDIF
//							REQUEST_COLLISION_AT_COORD(<<1992.3885, 3830.4963, 31.1573>>)
							SET_LABEL_AS_TRIGGERED("PAUSE FOR STREAMING",TRUE)
							//SET_GAME_PAUSES_FOR_STREAMING(TRUE)
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("UNPIN")
						IF GET_CUTSCENE_TIME() > 115433.342
							IF interior_trev_trailer != NULL
								UNPIN_INTERIOR(interior_trev_trailer)
								SET_LABEL_AS_TRIGGERED("UNPIN",TRUE)
							ENDIF
						ENDIF
					ENDIF
				#ENDIF
				
				#IF IS_JAPANESE_BUILD
					IF GET_CUTSCENE_TIME() > 121400
						SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<0,0,0>>)
					ENDIF
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						IF GET_CUTSCENE_TIME() > 96400
							SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
							SET_CLOCK_TIME(13,00,00)
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("STREAMING REQUEST")
							IF GET_CUTSCENE_TIME() > 73934.326
								//SET_GAME_PAUSES_FOR_STREAMING(FALSE)
//								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
//									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//										SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED_ID(), TRUE)
//									ENDIF
//								ENDIF
								//NEW_LOAD_SCENE_START_SPHERE(<<1975.3849, 3818.8652, 32.4363>>, 1.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE | NEWLOADSCENE_FLAG_REQUIRE_COLLISION) //change to 1
								PRINTSTRING("STREAMING REQUEST DONE")PRINTNL()
								SET_LABEL_AS_TRIGGERED("STREAMING REQUEST",TRUE)
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("PIN INTERIOR")
								IF SETUP_MISSION_REQUIREMENT(REQ_TRAILER_INTERIOR, <<0,0,0>>)
									SET_LABEL_AS_TRIGGERED("PIN INTERIOR",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SWITCH FOCUS")
						IF GET_CUTSCENE_TIME() > 88467.674
//							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//								SET_FOCUS_ENTITY(PLAYER_PED_ID())
//							ENDIF
							SET_LABEL_AS_TRIGGERED("SWITCH FOCUS",TRUE)
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("PAUSE FOR STREAMING")
//							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//								IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
//									REQUEST_COLLISION_AT_COORD(<<1992.3885, 3830.4963, 31.1573>>)
//								ENDIF
//							ENDIF
//							REQUEST_COLLISION_AT_COORD(<<1992.3885, 3830.4963, 31.1573>>)
							SET_LABEL_AS_TRIGGERED("PAUSE FOR STREAMING",TRUE)
//							SET_GAME_PAUSES_FOR_STREAMING(TRUE)
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("UNPIN")
						IF GET_CUTSCENE_TIME() > 107433.342
							IF interior_trev_trailer != NULL
								UNPIN_INTERIOR(interior_trev_trailer)
								SET_LABEL_AS_TRIGGERED("UNPIN",TRUE)
							ENDIF
						ENDIF
					ENDIF
				#ENDIF
				
				IF bClearCutscenArea = FALSE
					IF IS_REPLAY_IN_PROGRESS()
						CONTROL_FADE_IN(500)
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						//Move the last vehicle somewhere if it's in the way.
						veh_pre_mission_car = GET_PLAYERS_LAST_VEHICLE()
					
						IF IS_VEHICLE_DRIVEABLE(veh_pre_mission_car)
							IF IS_ENTITY_AT_COORD(veh_pre_mission_car, <<-16.3030, -1457.5975, 29.4831>>, <<20.0, 20.0, 10.0>>)
							AND GET_ENTITY_MODEL(veh_pre_mission_car) != GET_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR)
								SET_ENTITY_AS_MISSION_ENTITY(veh_pre_mission_car)
								SET_ENTITY_COORDS(veh_pre_mission_car, <<-24.9712, -1436.6658, 29.6542>>)
								SET_ENTITY_HEADING(veh_pre_mission_car, 180.5934)
							ENDIF
						ENDIF
						
						//RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<1992.2, 3834, 32.3>>, 207.5433,FALSE)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1963.691162,3809.464355,30.892273>>, <<1994.771606,3826.749023,37.406181>>, 29.500000,<<1992.2, 3834, 32.3>>, 207.5433, FALSE)
						STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
					ELSE
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								STORE_PLAYER_PED_VARIATIONS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
						DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
					ENDIF
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							REMOVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							REMOVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],TRUE)
						ENDIF
					ENDIF
					
					//STREAMING REQUEST STUFF
					
					//SET_SRL_LONG_JUMP_MODE(TRUE) 
					SET_SRL_READAHEAD_TIMES(10,10,10,10)

					SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
					REMOVE_ANIM_DICT("missheist_jewelleadinoutjh_endscene")
										
					CLEAR_AREA(<<1975.3729, 3818.3162, 32.4363>>, 200.0, TRUE)
					CLEAR_AREA(<<-820.734192,179.547073,68.112610>>, 100.0, TRUE, TRUE)
					PRINTSTRING("Clear area called")PRINTNL()
					iSkipCutsceneTimer = GET_GAME_TIMER()
					STOP_GAMEPLAY_HINT()
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
					bClearCutscenArea = TRUE
				ENDIF
			ENDIF
		
			IF bSetJohnnyComponent
				IF DOES_CUTSCENE_ENTITY_EXIST("Johnny", IG_JOHNNYKLEBITZ)        
	                SET_CUTSCENE_PED_COMPONENT_VARIATION("Johnny", PED_COMP_HEAD, 0, 0, IG_JOHNNYKLEBITZ) 
	            ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			
				REPLAY_STOP_EVENT()
			
				NEW_LOAD_SCENE_STOP()
				
				IF IS_REPEAT_PLAY_ACTIVE()
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("BLOOD")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.657,0.566,126.360,0.07,3,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.589,0.266,0,0.01,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.612,0.269,126.360,0.22,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.709,0.396,126.360,0.00,1,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.582,0.383,200.360,0.1,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.582,0.383,111.360,0.05,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.582,0.383,111.360,0.05,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.100,0.383,111.360,0.05,2,0,"cs_trev1_blood")
					APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),0,0.050,0.383,111.360,0.05,2,0,"cs_trev1_blood")
					SET_LABEL_AS_TRIGGERED("BLOOD",TRUE)
				ENDIF
				
				PRINTSTRING("e_section_stage = Hitting exit state for Trevor -76")PRINTNL()
				
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 4000, 96, FALSE)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
			ENDIF

		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ron")
				IF NOT IS_ENTITY_DEAD(budRon.ped)
					PRINTSTRING("e_section_stage = Hitting exit state for Ron")PRINTNL()
					SET_UP_RON_COMPONENTS()
					FORCE_PED_MOTION_STATE(budRon.ped, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Wade")
				IF NOT IS_ENTITY_DEAD(budWade.ped)
					PRINTSTRING("e_section_stage = Hitting exit state for Wade")PRINTNL()
					SET_UP_WADE_COMPONENTS()
					FORCE_PED_MOTION_STATE(budWade.ped, MS_ON_FOOT_WALK,FALSE, FAUS_CUTSCENE_EXIT)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ashley")
				PRINTSTRING("e_section_stage = Hitting exit state for Ashley")PRINTNL()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
				SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
				//IF NOT b_skipped_mocap
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds,FALSE,TRUE)
							CLEAR_FOCUS()
							SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED_ID(), TRUE)
						ENDIF
					ENDIF
				//ENDIF
				SET_LABEL_AS_TRIGGERED("TRV1_INT CAM EXIT",TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()

				PRINTSTRING("e_section_stage = Hitting exit state for Camera")PRINTNL()
			ENDIF

	
			IF DOES_ENTITY_EXIST(vehTrevor.veh)
				IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						
					ENDIF
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
			AND bClearCutscenArea = TRUE
				SET_GAME_PAUSES_FOR_STREAMING(TRUE)
				PRINTSTRING("e_section_stage = SECTION_STAGE_CLEANUP")PRINTNL()
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			
			IF b_skipped_mocap
				IF IS_SCREEN_FADED_OUT()	
					PRINTSTRING("Skipped Mocap")PRINTNL()
					
					REPLAY_CANCEL_EVENT()
					
					STOP_CUTSCENE(TRUE)
					//If the player skipped the mocap the peds need to be warped.
					WHILE IS_CUTSCENE_ACTIVE()
						WAIT(0)
					ENDWHILE
					
			
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					SET_CLOCK_TIME(13,00,00)
					
					Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
					SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
//					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
//						SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
//					ENDIF
					
					CLEAR_FOCUS()
					
					IF bAddedRelGroups = FALSE
						IF	ADD_RELATIONSHIP_GROUP("BUDDIES", grpBuddies)
						AND ADD_RELATIONSHIP_GROUP("ENEMIES", grpEnemies)
							bAddedRelGroups = TRUE
						ENDIF
					ENDIF
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,grpBuddies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpEnemies,RELGROUPHASH_PLAYER)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,grpBuddies,grpEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,grpBuddies,RELGROUPHASH_PLAYER)
					
					IF IS_VEHICLE_DRIVEABLE(vehTrevor.veh)
						IF NOT IS_PED_INJURED(budWade.ped) AND NOT IS_PED_INJURED(budRon.ped)
							IF bAssignWadeTask = FALSE
								SETUP_JOHNNY_AND_ASHLEY()
								bAssignWadeTask = TRUE
							ENDIF
						ENDIF
					ENDIF

					NEW_LOAD_SCENE_STOP()
					SET_GAME_PAUSES_FOR_STREAMING(TRUE)
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DO LOAD SCENE")
						LOAD_SCENE(<<1995.97, 3829.84, 32.27>>)
						SET_LABEL_AS_TRIGGERED("DO LOAD SCENE",TRUE)
					ENDIF
			
					bSkipLoaded = TRUE
				ELSE
					CONTROL_FADE_OUT(200)
				ENDIF
			ENDIF
			
			SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
			
			IF IS_REPEAT_PLAY_ACTIVE()
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
			ENDIF
		
			//Setup buddy relationship groups etc.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),FALSE)
			ENDIF
			
			INFORM_MISSION_STATS_OF_INCREMENT(TRV1_KILLS)		//one kill for stomping Johnny
			
			SET_PED_POPULATION_BUDGET(3)		
			SET_VEHICLE_POPULATION_BUDGET(3)
			IF b_skipped_mocap
				IF bSkipLoaded = TRUE
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					i_current_event = 0
					e_section_stage = SECTION_STAGE_SETUP
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
					budWade.iAction = 0
					Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
					SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
					SET_PLAYER_AS_TREVOR()
					bcutsceneplaying = FALSE
				ENDIF
			ELSE
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				i_current_event = 0
				e_section_stage = SECTION_STAGE_SETUP
				//SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
				budWade.iAction = 0
				bcutsceneplaying = FALSE
			ENDIF
			
			//[MF] Toggle on Trevor's bloody pants leg and shirt B* 1760047
			IF NOT bTrevorSplashBackEffectOn
				APPLY_HEAD_STOMP_BLOOD_TO_TREVOR()
				bTrevorSplashBackEffectOn = TRUE
			ENDIF
			
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			CONTROL_FADE_OUT(200)
			
			STOP_CUTSCENE(TRUE)
			
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
			ENDIF
			
			CLEAR_FOCUS()
			
			NEW_LOAD_SCENE_STOP()
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			
			IF bAddedRelGroups = FALSE
				IF	ADD_RELATIONSHIP_GROUP("BUDDIES", grpBuddies)
				AND ADD_RELATIONSHIP_GROUP("ENEMIES", grpEnemies)
					bAddedRelGroups = TRUE
				ENDIF
			ENDIF
			
			//Make sure necessary assets get made.
			PRINTLN("@@@@@@@@@@@@@ SETUP_MISSION_REQUIREMENT 4 @@@@@@@@@@@@@@")
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_RON, <<0,0,0>>)
			OR NOT SETUP_MISSION_REQUIREMENT(REQ_WADE, <<0,0,0>>)
			OR NOT SETUP_MISSION_REQUIREMENT(REQ_JOHNNY, <<0,0,0>>)
			OR NOT SETUP_MISSION_REQUIREMENT(REQ_ASHLEY, <<0,0,0>>)
			OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<0,0,0>>)
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_ENTITY_DEAD(budRon.ped)
				SET_ENTITY_COORDS(budRon.ped, <<2001.55, 3831.55, 31.2735>>)
				SET_ENTITY_HEADING(budRon.ped, 94.23)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(budRon.ped,TRUE)
				SET_UP_RON_COMPONENTS()
			ENDIF

			IF NOT IS_ENTITY_DEAD(budWade.ped)
				SET_ENTITY_COORDS(budWade.ped, <<2000.40, 3829.17, 32.30>>)
				SET_ENTITY_HEADING(budWade.ped, 85.16)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(budWade.ped,TRUE)
				SET_UP_WADE_COMPONENTS()
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<1995.97, 3829.84, 31.2735>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 100.81)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(),TRUE)
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_OUT()
				PRINTSTRING("DOING LOAD SCENE")PRINTNL()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DO LOAD SCENE")
					LOAD_SCENE(<<1983.1481, 3831.1982, 31.3746>>)
					SET_LABEL_AS_TRIGGERED("DO LOAD SCENE",TRUE)
				ENDIF
			ENDIF
			
			b_skipped_mocap = TRUE
			PRINTSTRING("e_section_stage = SECTION_STAGE_SKIP")PRINTNL()
			e_section_stage = SECTION_STAGE_CLEANUP
		ENDIF
	ENDWHILE
ENDPROC


//---------------------------------¦ MAIN SCRIPT ¦-----------------------------------

PROC MANAGE_FAKE_INTERIOR()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<40.079273,3602.131836,24.754978>>, <<70.157211,3758.732178,70.401665>>, 151.500000)
		SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakeTrailerParkCS304"),65,3680)
	ENDIF
ENDPROC

PROC MANAGE_BUDDY_RESPONSE_TO_ARREST()

	PRINTSTRING("ARREST SCRIPT")PRINTNL()

	IF DOES_ENTITY_EXIST(budWade.ped)
		IF NOT IS_PED_INJURED(budWade.ped)
			PED_INDEX pedClosestCopToWade
			GET_CLOSEST_PED(GET_ENTITY_COORDS(budWade.ped, FALSE), 10.0, FALSE,TRUE,pedClosestCopToWade,FALSE)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("WADE ARREST")
				OPEN_SEQUENCE_TASK(seqSequence)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					TASK_HANDS_UP(NULL,-1,pedClosestCopToWade,-1,HANDS_UP_NOTHING)
				CLOSE_SEQUENCE_TASK(seqSequence)
				TASK_PERFORM_SEQUENCE(budWade.ped, seqSequence)
				CLEAR_SEQUENCE_TASK(seqSequence)
				SET_PED_KEEP_TASK(budWade.ped,TRUE)
				SET_LABEL_AS_TRIGGERED("WADE ARREST",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(budRon.ped)
		IF NOT IS_PED_INJURED(budRon.ped)
			PED_INDEX pedClosestCopToRon
			GET_CLOSEST_PED(GET_ENTITY_COORDS(budRon.ped, FALSE), 10.0, FALSE,TRUE,pedClosestCopToRon,FALSE)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("RON ARREST")
				TASK_HANDS_UP(budRon.ped,-1,NULL,-1,HANDS_UP_NOTHING)
				SET_PED_KEEP_TASK(budWade.ped,TRUE)
				SET_LABEL_AS_TRIGGERED("RON ARREST",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


SCRIPT
	SET_MISSION_FLAG(TRUE)
	REGISTER_SCRIPT_WITH_AUDIO()
	//INFORM_MISSION_STATS_OF_MISSION_START_TREVOR_ONE()
	IF HAS_FORCE_CLEANUP_OCCURRED()
		IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
			MANAGE_BUDDY_RESPONSE_TO_ARREST()
		ENDIF
		STORE_FAIL_WEAPON(PLAYER_PED_ID(), ENUM_TO_INT(SELECTOR_PED_TREVOR))
		TRIGGER_MUSIC_EVENT("TRV1_FAIL")
		PRINTSTRING("******* Force cleanup")PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		MissionCleanup()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	
//	PRINTSTRING("SCRIPTS ACTUALLY HAVE COMPILED")PRINTNL()
//	
//	IF eMissionStage =  STAGE_GET_TO_CHASE
//	OR eMissionStage = STAGE_CUTSCENE_CHASE_START
//	OR eMissionStage = STAGE_CHASE
//		MANAGE_WADE_IN_BACK_SEAT()
//	ELSE
//		REMOVE_ANIM_DICT("misstrevor1ig_2")	
//	ENDIF

	IF DOES_ENTITY_EXIST(budWade.ped)
		IF NOT IS_PED_INJURED(budWade.ped)
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(budWade.ped,false)
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(budWade.ped,false)
		ENDIF
	ENDIF
	
	eMissionStage = STAGE_INIT_MISSION
	e_section_stage = SECTION_STAGE_SETUP
	
	WHILE bScriptInProgress
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_MrPhilips")
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(budRon.ped)
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF GET_RELATIONSHIP_BETWEEN_GROUPS(grpBuddies, RELGROUPHASH_COP) != ACQUAINTANCE_TYPE_PED_HATE
						IF NOT IS_PED_INJURED(budRon.ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, FALSE)
						ENDIF
						IF NOT IS_PED_INJURED(budWade.ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, FALSE)
						ENDIF
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, grpBuddies, RELGROUPHASH_COP)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, grpBuddies)
						PRINTLN("@@@@@@@@@@@@@@@ SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, grpBuddies, RELGROUPHASH_COP) @@@@@@@@@@@@@@@")
					ENDIF
				ELSE
					IF GET_RELATIONSHIP_BETWEEN_GROUPS(grpBuddies, RELGROUPHASH_COP) != ACQUAINTANCE_TYPE_PED_NONE
						IF NOT IS_PED_INJURED(budRon.ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budRon.ped, TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(budWade.ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(budWade.ped, TRUE)
						ENDIF
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, grpBuddies, RELGROUPHASH_COP)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_COP, grpBuddies)
						PRINTLN("@@@@@@@@@@@@@@@ SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, grpBuddies, RELGROUPHASH_COP) @@@@@@@@@@@@@@@")
					ENDIF
				ENDIF
			ENDIF
			IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
				PRINTSTRING("Arrest check")PRINTNL()
				MANAGE_BUDDY_RESPONSE_TO_ARREST()
			ENDIF
		ENDIF
		
		//MANAGE_TREVORS_ABILITY_EFFECTS()
		
		IF eMissionStage > STAGE_INIT_MISSION

			#IF IS_DEBUG_BUILD 
				DONT_DO_J_SKIP(sLocatesData)
			#ENDIF

			IF eMissionStage = STAGE_GET_TO_CHASE
				SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
				//PRINTSTRING("VEHICLE DENSITY = 1")
			ENDIF
			
			IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
				SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				//PRINTSTRING("VEHICLE DENSITY = 0")
			ENDIF
			
			IF eMissionStage > STAGE_CARAVAN_PARK_FIGHT
			AND eMissionStage < STAGE_DROP_OFF_RON
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.4)
				//PRINTSTRING("VEHICLE DENSITY = 0.4")
			ENDIF
			
			IF eMissionStage = STAGE_DROP_OFF_RON
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
				//PRINTSTRING("VEHICLE DENSITY = 1")
			ENDIF
		ENDIF

		BLOCK_FIRST_PERSON()

		SWITCH eMissionStage
			CASE STAGE_INIT_MISSION
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<24.5298, 3590.7327, 0.0>>, <<147.1179, 3772.4255,100>>, FALSE)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<  5.9328, 3682.4102,0.0 >>,<<110.0308, 3801.6506, 100.0>>, FALSE)
				REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<24.5298, 3590.7327, 0.0>>, <<147.1179, 3772.4255,100>>)
				REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<  5.9328, 3682.4102,0.0 >>,<<110.0308, 3801.6506, 100.0>>)
				
				// Override the position the replay controller thinks we started the mission in.
				// We really started the mission where the Jewelry Heist ended, but if we reject a 
				// replay we want to be positioned on the Trevor mission blip.
				g_startSnapshot.mPlayerStruct.vPos = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_TREVOR)
				
				//bCanSkipChaseStartScene 		= FALSE
				//bWasChaseStartSceneSkipped 		= FALSE

				IF (Is_Replay_In_Progress())
					eTrevorsFailWeapon = GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR))
					IF 	( eTrevorsFailWeapon <> WEAPONTYPE_INVALID )
					AND ( eTrevorsFailWeapon <> WEAPONTYPE_UNARMED )
						REQUEST_WEAPON_ASSET(eTrevorsFailWeapon)
					ENDIF
					
					IF GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR)) != WEAPONTYPE_UNARMED AND GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR)) != WEAPONTYPE_INVALID
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR)))
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_TREVOR)), TRUE)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), TRUE)
						ENDIF
					ENDIF
					
					IF g_bShitskipAccepted = TRUE
						stageInitMission()
						iReplayStage = Get_Replay_Mid_Mission_Stage()	
						iReplayStage = iReplayStage + 1
						bStageSetup = FALSE
						IF iReplayStage < ENUM_TO_INT(PASSED_MISSION)
							IF iReplayStage = 0
								START_REPLAY_SETUP(<<1995.97, 3829.84, 32.27>>,100.81)
								eMissionStage = STAGE_GET_TO_CHASE
								PRINTSTRING("******* iSkipStage = 1")PRINTNL()
							ELIF iReplayStage = 1
								START_REPLAY_SETUP(<< 2016.4196, 4642.4399, 40.1808 >>,143.064)
								eMissionStage = STAGE_CUTSCENE_CHASE_START
								PRINTSTRING("******* iSkipStage = 2")PRINTNL()
							ELIF iReplayStage = 2
								START_REPLAY_SETUP(<< 58.4031, 3630.9597, 38.7482 >>,20.8123)
								eMissionStage = STAGE_CARAVAN_PARK_FIGHT
								PRINTSTRING("******* iSkipStage = 3")PRINTNL()
							ELIF iReplayStage = 3
								START_REPLAY_SETUP(<< 80.4212, 3607.0464, 38.6962 >> , 189.0543)
								eMissionStage = STAGE_DRIVE_TO_CARAVAN
								PRINTSTRING("******* iSkipStage = 4")PRINTNL()
							ELIF iReplayStage = 4
								START_REPLAY_SETUP(<<-11.4373, 3007.7844, 40.6016>>,180.4163)
								eMissionStage = STAGE_CARAVAN_SMASH
								PRINTSTRING("******* iSkipStage = 5")PRINTNL()
							ELIF iReplayStage = 5
								START_REPLAY_SETUP(<< -45.8227, 3094.8867, 26.0223 >>,23.9401)
								eMissionStage = STAGE_KILL_ORTEGA
								PRINTSTRING("******* iSkipStage = 6")PRINTNL()
							ELIF iReplayStage = 6
								START_REPLAY_SETUP(<< -6.8119, 3039.8992, 39.6770 >> ,287.4757 )
								eMissionStage = STAGE_DROP_OFF_RON
								PRINTSTRING("******* iSkipStage = 7")PRINTNL()
							ELIF iReplayStage = 7
								START_REPLAY_SETUP(<<1952.2297, 3787.3960, 31.2922>>,27.2205)
								eMissionStage = STAGE_CUTSCENE_DROP_OFF_RON
								PRINTSTRING("******* iSkipStage = 8")PRINTNL()
								//SCRIPT_ASSERT("Trevor1: Trying to replay a mission stage that doesn't exist!")
							ENDIF
						ELSE
							eMissionStage =  PASSED_MISSION
						ENDIF
						
						//manageSkip()
					ELSE
					
						stageInitMission()
						//Use iReplayStage to restart mission at the correct stage
				        iReplayStage = Get_Replay_Mid_Mission_Stage()		
						
						bStageSetup = FALSE
						//If the player chose to start from a checkpoint, jump to the last checkpoint reached.
						IF iReplayStage = 0
							START_REPLAY_SETUP(<<1995.97, 3829.84, 32.27>>,100.81)
							eMissionStage = STAGE_GET_TO_CHASE
							PRINTSTRING("******* iReplayStage = 1")PRINTNL()
						ELIF iReplayStage = 1
							START_REPLAY_SETUP(<<1965.3540, 4646.6006, 39.8213>>,99.27)
							eMissionStage = STAGE_CHASE
							PRINTSTRING("******* iReplayStage = 2")PRINTNL()
						ELIF iReplayStage = 2
							START_REPLAY_SETUP(<< 58.4031, 3630.9597, 38.7482 >>,20.8123)
							eMissionStage = STAGE_CARAVAN_PARK_FIGHT
							PRINTSTRING("******* iReplayStage = 3")PRINTNL()
						ELIF iReplayStage = 3
							START_REPLAY_SETUP(<< 80.4212, 3607.0464, 38.6962 >> , 189.0543)
							eMissionStage = STAGE_DRIVE_TO_CARAVAN
							PRINTSTRING("******* iReplayStage = 4")PRINTNL()
						ELIF iReplayStage = 4
							START_REPLAY_SETUP(<<-11.4373, 3007.7844, 40.6016>>,180.4163)
							eMissionStage = STAGE_CARAVAN_SMASH
							PRINTSTRING("******* iReplayStage = 5")PRINTNL()
						ELIF iReplayStage = 5
							START_REPLAY_SETUP(<< -45.8227, 3094.8867, 26.0223 >>,23.9401)
							eMissionStage = STAGE_KILL_ORTEGA
							PRINTSTRING("******* iReplayStage = 6")PRINTNL()
						ELIF iReplayStage = 6
							START_REPLAY_SETUP(<< -6.8119, 3039.8992, 39.6770 >> ,287.4757 )
							eMissionStage = STAGE_DROP_OFF_RON
							PRINTSTRING("******* iReplayStage = 7")PRINTNL()
						ELSE
							//SCRIPT_ASSERT("Trevor1: Trying to replay a mission stage that doesn't exist!")
						ENDIF
					ENDIF
					
					manageSkip()
					
				ELSE
					//Mission is being played normally, not being replayed
					PRINTSTRING("******* No Replay")PRINTNL()
					IF NOT IS_REPLAY_IN_PROGRESS()
						RUN_OPENING_CUTSCENE()
					ENDIF
					stageInitMission()
					//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0,"Initialise Mission") 
				ENDIF
			BREAK
			CASE STAGE_GET_TO_CHASE
				IF stageGetToChase()
					eMissionStage = STAGE_CUTSCENE_CHASE_START
				ENDIF
			BREAK
			CASE STAGE_CUTSCENE_CHASE_START
				IF stageCutsceneChaseStart()
					eMissionStage = STAGE_CHASE
				ENDIF
			BREAK
			CASE STAGE_CHASE
				IF stageChaseV2()
					eMissionStage = STAGE_CARAVAN_PARK_FIGHT
				ENDIF
			BREAK
			CASE STAGE_CARAVAN_PARK_FIGHT
				IF stageCaravanParkFightV2()
					eMissionStage = STAGE_DRIVE_TO_CARAVAN
				ENDIF
			BREAK
			CASE STAGE_DRIVE_TO_CARAVAN
				IF stageDriveToCaravan()
					eMissionStage = STAGE_CARAVAN_SMASH
				ENDIF
			BREAK
			CASE STAGE_CARAVAN_SMASH
				IF stageCaravanSmash()
					eMissionStage = STAGE_KILL_ORTEGA
				ENDIF
			BREAK
			CASE STAGE_KILL_ORTEGA
				IF stageKillSmashMan()
					eMissionStage = STAGE_DROP_OFF_RON
				ENDIF
			BREAK
			CASE STAGE_DROP_OFF_RON
				IF stageDropOffRon()
					eMissionStage = STAGE_CUTSCENE_DROP_OFF_RON
				ENDIF
			BREAK
			CASE STAGE_CUTSCENE_DROP_OFF_RON
				IF stageCutsceneDropOffRon()
					eMissionStage = PASSED_MISSION
				ENDIF
			BREAK	
			CASE PASSED_MISSION
				MissionPassed()
			BREAK

			CASE UBER_RECORD
				#IF IS_DEBUG_BUILD
					//stageRecord()
					//stagePlayback()
					//stageMiniCut()
					//stageRerecord()
					RECORD_CHASE_TRIGGER_AND_TRAFFIC()
				#ENDIF
			BREAK
			
		ENDSWITCH
		
		IF DOES_ENTITY_EXIST(vehTrevor.veh)
			IF NOT bTrevorCarGrabbedForStats
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehTrevor.veh, TRV1_CAR_DAMAGE)
				bTrevorCarGrabbedForStats = TRUE
			ENDIF
		ELSE
			IF bTrevorCarGrabbedForStats
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, TRV1_CAR_DAMAGE)
				bTrevorCarGrabbedForStats = FALSE
			ENDIF
		ENDIF
		
		IF eMissionStage = STAGE_CARAVAN_PARK_FIGHT
		OR eMissionStage = STAGE_CHASE
			//MANAGE_FAKE_INTERIOR()
		ENDIF
	
		FailCheck()
		
		IF bMissionFailedCleanup
			MissionCleanup(TRUE)
		ENDIF
		
		IF eMissionStage > STAGE_CARAVAN_SMASH
			//MANAGE_ORTEGA_IF_SPARED()
		ENDIF

		// controls Enemy behaviour
		CONTROL_ENEMIES() 
		// controls Buddy behaviour
		CONTROL_BUDDY()
	
		#IF IS_DEBUG_BUILD
			Debug_Options()
			//UPDATE_WIDGETS()
			//SHOW_PED_DISTANCE_FROM_PLAYER(cmbtChase[0].ped )
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(eMissionStage), FALSE, sMissionName,TRUE,TRUE)
				eMissionStage = INT_TO_ENUM(MISSION_STAGE, iReturnStage)
				fadeOutAndWait()
				MissionTidyUp(TRUE)			
				iStageSection = 99
				bStageSetup = FALSE
				manageSkip()
			ENDIF
		#ENDIF
				 
		WAIT(0)
	ENDWHILE

	MissionCleanup()
ENDSCRIPT
