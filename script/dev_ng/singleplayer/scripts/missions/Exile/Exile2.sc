// *****************************************************************************************************
// *****************************************************************************************************
// *****************************************************************************************************
//
//		MISSION NAME	:	Predator
//		AUTHOR			:	Lawrence Kerr
//		DESCRIPTION		:	Franklin chases a gang who lose him in a forest, Trevor and Michael
//							must fly a chopper overhead and use a thermal sniper rifle to find them.
//
// *****************************************************************************************************
// *****************************************************************************************************
// *****************************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0

const_int total_number_of_enemies	4
const_int total_number_of_animals   19

CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					1//11
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					6
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				22  

CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		8 
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		14
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		1

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_cutscene.sch"
using "commands_recording.sch"
USING "dialogue_public.sch"
USING "flow_public_core_override.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "chase_hint_cam.sch"                            
USING "locates_public.sch"
USING "selector_public.sch"
USING "replay_public.sch"
USING "timer_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "CompletionPercentage_public.sch"
using "vehicle_gen_public.sch"
using "chop_cam_public.sch"
using "social_public.sch"
using "timelapse.sch"
using "building_control_public.sch"
using "player_scene_private.sch"
using "clearmissionarea.sch"
using "emergency_call.sch"
using "spline_cam_edit.sch"
using "cheat_controller_public.sch"
using "push_in_public.sch"

#IF IS_DEBUG_BUILD
	USING "hud_creator_tool.sch"
#ENDIF


const_int stick_dead_zone 								28

USING "traffic.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"

	USING "select_mission_stage.sch"
	CONST_INT MAX_SKIP_MENU_LENGTH 9
#ENDIF

ENUM MISSION_STAGE
	STAGE_OPENING_MOCAP = 0,
	switching_into_franklin,
	stage_get_to_mountains,
	STAGE_CRASH_CUTSCENE,
	STAGE_INVESTIGATE_CRASH,
	stage_get_helicopter,
	STAGE_SWAP_TO_MICHAEL_CUTSCENE,
	STAGE_SNIPE_ENEMIES,
	stage_mission_pass_conversation,
	stage_switch_to_trevor,
	stage_pickup_franklin_and_chop,
	stage_get_to_airstrip,
	stage_pass_mocap,
	STAGE_RPG_CUTSCENE,
	STAGE_PGM_BULLETS,
	STAGE_RECORD_TRAFFIC,
	STAGE_RECORD_SETPIECES,
	STAGE_PLAYBACK_UBER,
	STAGE_TEST,
	helicopter_mission_fail_cutscene,
	mission_fail_stage,
	load_stage_selector_assets,
	STAGE_DONE
ENDENUM

ENUM FAILED_REASON
	FAILED_GENERIC = 0,
	FAILED_DESTROYED_CAR,
	FAILED_FRANKLIN_DIED,
	FAILED_TREVOR_DIED,
	FAILED_MICHAEL_DIED,
	FAILED_DESTROYED_HELI,
	FAILED_LOST_ENEMIES,
	FAILED_OUT_OF_TIME
ENDENUM

ENUM SECTION_STAGE
	SECTION_STAGE_SETUP = 0,
	SECTION_STAGE_RUNNING,
	SECTION_STAGE_CLEANUP,
	SECTION_STAGE_SKIP
ENDENUM

enum chop_audio_enum
	chop_with_franklin = 0,
	chop_running,
	chop_finds_enemy,
	chop_bark_button,
	chop_no_audio
endenum

enum exile_2_switch_cam_enums
	switch_cam_to_michael_from_franklin = 0,
	switch_cam_to_michael_from_michael_chop,
	switch_cam_to_michael_from_franklin_chop,
	switch_cam_to_franklin_from_michael,
	switch_cam_to_franklin_from_michael_chop,
	switch_cam_to_franklin_from_franklin_chop,
	switch_cam_to_chop_from_michael,
	switch_cam_to_chop_from_franklin,
	switch_cam_to_trevor_from_franklin,
	switch_cam_to_trevor_from_franklin_chop, 
	switch_cam_to_trevor_from_michael, 
	switch_cam_to_trevor_from_michael_chop
endenum
exile_2_switch_cam_enums exile_2_switch_cam_status = switch_cam_to_michael_from_franklin


STRUCT MISSION_VEHICLE
	VEHICLE_INDEX veh
	BLIP_INDEX blip 
ENDSTRUCT

STRUCT HELI_HOVER_DATA
	FLOAT f_vertical_offset
	FLOAT f_horizontal_offset
	FLOAT f_turn_offset
ENDSTRUCT

struct ped_structure
	ped_index ped 
	model_names model 
	blip_index blip
	AI_BLIP_STRUCT blip_2
	vector pos 
	vector rot
	vector run_to_pos
	float heading 
	int health 
	int accuracy
	int scene_id
	int time
	bool damaged_by_player
	bool allow_body_armour
	bool created 
	weapon_type weapon
	text_label name
endstruct 


struct vehicle_struct
	vehicle_index veh
	model_names model
	blip_index blip
	vector pos 
	float heading
	float speed
	float skip_time
	int health
	float engine_health
	float petrol_tank_health
	int colour
	int recording_number
	bool been_created 
endstruct

struct object_struct 
	object_index obj
	blip_index blip
	model_names model
	vector pos 
	vector offset_pos
	vector offset_pos_2
	vector rot
	vector offset_rot
	float heading
	float scale 
	float health
	string room_name
	int time
	bool been_created
endstruct 

struct animal_positions 
	vector animal_pos_0
	vector animal_pos_1
	vector animal_pos_2
endstruct

struct cover_node_struct
	//coverpoint_index cover_point
	vector pos
endstruct
	

CONST_INT CARREC_GANG				1
CONST_INT CARREC_GANG_NO_CRASH		200
CONST_INT CARREC_CRASH				3
CONST_INT CARREC_HELI_HOVER			4
CONST_INT CARREC_HELI_ARRIVE		5

CONST_FLOAT CHOPPER_HOVER_SPEED 	1.0


//lawrence declared variables 

bool vehicle_text_printed = false
//bool target_text_printed = false
bool set_uber_speed_to_zero = false
bool start_rpg_cutscene = false
bool activate_helicopter_flying_system = false
//bool allow_chop_on_mission = true 
bool load_assets = false
bool initialise_sniper_aim_at_pos = false
bool mission_starts_as_franklin = false
bool resume_lower_priority_conversation = false
bool force_sniper_hud_for_start_of_mission = false
bool outside_area_text_printed = false
bool hunt_down_rpg_enemy = false
bool stop_heli_cam_explosion_shake = false
bool allow_helicopter_camera_system_to_run_during_fail = false
bool replay_video_recorder_0_activated = false
bool force_sniper_hud_for_first_person_view_mode = false
bool initialise_push_in_data_for_switch_cam_trevor_michael_to_bros_and_franklin = false
bool scope_to_franklin_hit_out_0 = false
bool scope_to_franklin_hit_out_1 = false
bool scope_to_franklin_transition_0 = false
bool franklin_to_truck_outro_short_transition_in = false
bool bros_to_franklin_transition = false
bool bros_to_franklin_hit = false
bool trigger_hit_out_sound_fx_when_switching_to_michael = false

BOOL bVehGrabbedForStats = FALSE  //PD added this so that the vehicle is only grabbed once for damage tracking
BOOL bAvoidOneFrameGameCameraFlag = FALSE //RW added

int dialogue_system_status = 0
int dialogue_time
int snipe_enemies_master_flow_system_status = 0
int original_time 
int current_seq_pos_0
int current_seq_pos_2
//int current_seq_pos_3
int dialogue_and_help_text_system_status = 0
int switch_system_status = 0
int helicopter_switch_system_status = 0
int rpg_system_status = 0
//int number_of_switches_to_trevor = 0
int rpg_cutscene_system_status = 0
int animal_ai_status[total_number_of_animals]
//int trevor_get_to_mountains_dialogue_system_status = 0
int franklin_get_to_mountains_dialogue_system_status = 0
int switch_system_2_status = 0
int investigate_crash_status = 0
int snipe_enemies_skip_status = 0
int chop_ai_system_status = 0
int chop_time 
int current_line 
int phone_hang_up_count = 0
int chop_ptfx_time = 0
int sniper_hud_time
int render_sniper_hud_status = 0
int chop_cutscene_index = -1
int mission_pass_conversation_status = 0
int franklin_ai_system_status = 0
//int chop_dialogue_time = 0
int trevor_looking_at_player_entering_vehicle_status = 0
int get_to_the_helicopter_dialogue_system_status = 0
int get_helicopter_status = 0
int gang_enemy_3_dialogue_time = 0
int switch_to_trevor_status = 0
int get_to_airstrip_status = 0
int closest_warp_pos_to_franklin = 0
int intro_mocap_status = 0
int area_fail_time = 0
int uber_chase_status = 0
int uber_chase_time = 0
int gang_enemy_3_heatscale_value = 0
int get_to_mountains_master_flow_system_status = 0
int get_to_airstrip_dialogue_system_staus = 0
int pickup_franklin_and_chop_dialogue_system_status = 0
int bullet_count = 0
int crash_cutscene_dialogue_system = 0
int switching_into_franklin_status = 0
int tunnel_pinning_system_status = 0
int pass_mocap_status = 0
int gang_car_ptfx_cutscene_system_status = 0
int trevor_get_to_mountains_dialogue_system_status = 0
int cutscene_index = 0
int shake_time = 0
int find_rpg_enemy_dialogue_time = 0
int switch_to_franklin_time = 0
int chop_enter_chopper_time = 0
int big_foot_ai_system_status = 0
int switch_sound_time = 0

int sniper_rifle_help_text_system_status = 0
int sniper_rifle_zoom_time = 0

int gang_car_sound_id = get_sound_id()
int rpg_sound = get_sound_id()
int rpg_alarm_sound = get_sound_id()

#IF IS_DEBUG_BUILD
	int p_skip_time
#endif 

float sniper_rifle_aim_heading
float sniper_rifle_aim_pitch 
//float current_time_scale
float disired_playback_speed
//float sniper_rifle_aim_fov
float uber_acceleration_multiplier
float f_playback_time = 0.0
float rpg_enemy_relative_heading_diff = 0.0
float rpg_enemy_relative_pitch_diff = 0.0

vector cutscene_rpg_target_pos
vector cutscene_rpg_direction_vector
vector chop_cutscene_pos = <<-1588.001, 4358.562, 0.658>>
vector chop_cutscene_rot = <<-3.900, 0.000, -57.560>>
//vector direction_vect_ba
vector cutscene_pos
vector cutscene_rot 
vector pickup_pos[2]
vector franklin_run_to_pos
vector chop_anim_rot

vehicle_index trevors_current_car

blip_index franklins_blip

string mission_failed_text
string trevor_go_to_instruction_text
string trevor_pickup_buddy_text
//string trevor_get_in_veh_text 
string trevor_get_back_in_veh_text

text_label dialogue_root
text_label specific_label


streamvol_id streaming_volume

ptfx_id gang_car_ptfx[1]

//streamvol_id streaming_volume

//


BOOL b_is_p_skipping						= FALSE
BOOL b_thermal_active						= FALSE
BOOL bPlayingHoverBackwards					= FALSE
bool stop_mission_fail_checks				= false
bool stop_thermal_help						= false
bool allow_switch							= true
bool render_sniper_hud						= false
bool aiming_at_chop 						= false
bool aiming_at_franklin						= false
bool aiming_at_gang_enemy_0					= false
bool aiming_at_gang_enemy_1					= false
bool aiming_at_gang_enemy_2					= false
bool aiming_at_gang_enemy_3					= false
bool crash_cutscene_fail					= false 
bool water_enemy_shot_at					= false
bool reset_heli_freeze_waiting_on_collision	= false
bool setup_trevors_truck_for_switch			= false
bool helicopter_check_point_exists			= false
bool rpg_enemy_attack_task_given 			= false


//FLOAT f_chopper_float_heading 		= 325.1966

INT i_triggered_text_hashes[30]
INT i_current_event					= 0
INT gang_enemy_3_time
INT iRandTime
int helicopter_cam_status 			= 0

PTFX_ID i_ptfx_fire	
PTFX_ID fxRpg
INT i_car_break_event				= 0
INT manage_gang_enemy_0_status				= 0
INT manage_gang_enemy_1_status				= 0
INT manage_gang_enemy_2_status				= 0
INT manage_gang_enemy_3_status				= 0
INT iFranklinCallProg						= 0
int gang_enemy_3_attack_conv_status			= 0

STRING str_carrec					= "MattForest"

VECTOR v_crash_pos					= <<-1874.7830, 4484.7104, 24.2898>>
//VECTOR v_chopper_float_pos 			= <<-1481.9680, 4334.3276, 95.3992>>

vector player_heli_offset  			= <<-0.9, 0.9, 1.10>>	         
vector current_cam_rot 				= <<-24.00, 0.0, 65.9>>	 
float min_x_cam_change				= -31.000
float max_x_cam_change				= -22.00//-19.00
float min_z_cam_change				= 30.000
float max_z_cam_change				= 100.00
float cam_speed_multiplier			 = 3.5


VECTOR vRocketStart
VECTOR vPedToHeliNorm

VECTOR vCountryRoadMin, vCountryRoadMax
	
BLIP_INDEX blip_current_destination

CAMERA_INDEX cam_cutscene
CAMERA_INDEX cam_interp_helper
CAMERA_INDEX cam_attach_helper
camera_index heli_cam
camera_index node_1_cam
camera_index node_2_cam
camera_index node_3_cam
camera_index camera_anim


OBJECT_INDEX oRPG

SEQUENCE_INDEX seq

interior_instance_index tunnel_inerior[2]

//group_index players_group


ped_structure gang_enemy[total_number_of_enemies]
ped_structure animal[total_number_of_animals]
ped_structure chop
ped_structure big_foot

vehicle_struct helicopter
vehicle_struct franklins_car
vehicle_struct gang_car
vehicle_struct trevors_truck
vehicle_struct ambient_car
vehicle_struct pass_cutscene_vehicle

object_struct rpg  

cover_node_struct franklin_cover_node[5]

animal_positions animal_movement[total_number_of_animals]


MODEL_NAMES trevor_model = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
MODEL_NAMES michael_model = GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
MODEL_NAMES franklin_model = GET_PLAYER_PED_MODEL(CHAR_FRANKLIN)

SELECTOR_PED_STRUCT selector_ped
SELECTOR_CAM_STRUCT selector_cam

CHOP_VIEW_CAM_DATA chop_cam

structPedsForConversation scripted_speech
structTimer count_down_timer

LOCATES_HEADER_DATA locates_data

chop_behaviour_enum chop_behaviour 

//timelapse variables
structTimelapse sTimelapse
										
checkpoint_index helicopter_check_point

CHASE_HINT_CAM_STRUCT localChaseHintCamStruct



MISSION_STAGE mission_flow 			= STAGE_OPENING_MOCAP
//mission_stage mission_flow_cached 
SECTION_STAGE e_section_stage 		= SECTION_STAGE_SETUP

//chop_audio_enum chop_audio_state	= chop_with_franklin

//	BOOL bDwUseDebug = FALSE

//NOT PRESENT IN INITIALSIE_MISSION_VARIABLES**********
int launch_mission_stage_menu_status = 0

bool replay_active = false

entity_index rpg_obj

PUSH_IN_DATA push_in_camera

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widget_debug
	MISSION_STAGE e_p_skip_stage
	
	MissionStageMenuTextStruct menu_stage_selector[MAX_SKIP_MENU_LENGTH]
	int menu_return_stage
	
	
	BOOL b_debug_record_traffic		= FALSE
	BOOL b_debug_record_setpieces	= FALSE
	BOOL b_debug_pause_playback		= FALSE
	BOOL bWdTestRpgGuyRunOut		= FALSE
	bool widget_entity_camera_active = false
	bool widget_reset_big_foot_global	= false
	
	vector widget_cam_attach_offset
	vector widget_rot
	vector widget_object_pos
	vector widget_object_rot
	vector widget_offset_bl
	vector widget_offset_br
	vector widget_bl
	vector widget_br
	
	float widget_angled_length
	float widget_heading
	float widget_pitch
	
	float widget_fov = 45.0

	camera_index widget_camera
	
	PROC CREATE_WIDGETS()
		widget_debug = START_WIDGET_GROUP("Forest Snipe")
			START_WIDGET_GROUP("Chase Recording")
				ADD_WIDGET_BOOL("Pause playback", b_debug_pause_playback)
				ADD_WIDGET_BOOL("Record traffic", b_debug_record_traffic)
				ADD_WIDGET_BOOL("Record set-pieces", b_debug_record_setpieces)
			STOP_WIDGET_GROUP()
		
			start_widget_group("camaera attached to entity widget")
				add_widget_bool("create camera", widget_entity_camera_active)
				
				ADD_WIDGET_FLOAT_SLIDER("pos x", widget_cam_attach_offset.x, -3000.0, 3000.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("pos y", widget_cam_attach_offset.y, -3000.0, 3000.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("pos z", widget_cam_attach_offset.z, -3000.0, 3000.0, 0.01)
				
				ADD_WIDGET_FLOAT_SLIDER("point offset x", widget_rot.x, -3000.0, 3000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("point offset y", widget_rot.y, -3000.0, 3000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("point offset z", widget_rot.z, -3000.0, 3000.0, 0.1)
				
				ADD_WIDGET_FLOAT_SLIDER("FOV", widget_fov, -3000.0, 3000.0, 0.1)
			stop_widget_group()
		
			start_widget_group("object ped attached to object widget")
				add_widget_float_slider("pos offset x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
				add_widget_float_slider("pos offset y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
				add_widget_float_slider("pos offset z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
				
				add_widget_float_slider("rot offset x", widget_object_rot.x, -3000.0, 3000.0, 0.01)
				add_widget_float_slider("rot offset y", widget_object_rot.y, -3000.0, 3000.0, 0.01)
				add_widget_float_slider("rot offset z", widget_object_rot.z, -3000.0, 3000.0, 0.01)
			stop_widget_group()

			start_widget_group("angled_area")
				add_widget_float_slider("left offset x", widget_offset_bl.x, -2000.0, 2000.0, 0.1)
				add_widget_float_slider("left offset y", widget_offset_bl.y, -2000.0, 2000.0, 0.1)
				add_widget_float_slider("left offset z", widget_offset_bl.z, -2000.0, 2000.0, 0.1)
				
				add_widget_float_slider("right offset x", widget_offset_br.x, -2000.0, 2000.0, 0.1)
				add_widget_float_slider("right offset y", widget_offset_br.y, -2000.0, 2000.0, 0.1)
				add_widget_float_slider("right offset z", widget_offset_br.z, -2000.0, 2000.0, 0.1)
				
				add_widget_float_slider("angled area length", widget_angled_length, -2000.0, 2000.0, 0.1)
				
				add_widget_float_slider("heading", widget_heading, -2000.0, 2000.0, 0.1)
				
				add_widget_float_read_only("widget_bl x", widget_bl.x)
				add_widget_float_read_only("widget_bl y", widget_bl.y)
				add_widget_float_read_only("widget_bl z", widget_bl.z)
				
				add_widget_float_read_only("widget_br x", widget_br.x)
				add_widget_float_read_only("widget_br y", widget_br.y)
				add_widget_float_read_only("widget_br z", widget_br.z)
			stop_widget_group()
			
			start_widget_group("variables")
				add_widget_bool("aiming_at_franklin", aiming_at_franklin)
				add_widget_bool("render_sniper_hud", render_sniper_hud)
				add_widget_bool("b_thermal_active", b_thermal_active)
			stop_widget_group()
			
			start_widget_group("widget relative heading and pitch")
				add_widget_float_slider("heading", widget_heading, -360, 360, 1)
				add_widget_float_slider("pitch", widget_pitch, -360, 360, 1)
			stop_widget_group()
			
			start_widget_group("draw_offset_marker_on_vehicle")
				add_widget_float_slider("vehicle offset pos", widget_object_pos.x, -2000.0, 2000.0, 0.1)
				add_widget_float_slider("vehicle offset pos", widget_object_pos.y, -2000.0, 2000.0, 0.1)
				add_widget_float_slider("vehicle offset pos", widget_object_pos.z, -2000.0, 2000.0, 0.1)
			stop_widget_group()

			start_widget_group("widget relative heading and pitch")
				add_widget_float_read_only("rpg_enemy_relative_heading_diff", rpg_enemy_relative_heading_diff)
				add_widget_float_read_only("rpg_enemy_relative_pitch_diff", rpg_enemy_relative_pitch_diff)
			stop_widget_group()
			
			start_widget_group("status int's")
				add_widget_int_read_only("chop_ai_system_status", chop_ai_system_status)
				add_widget_int_read_only("snipe_enemies_master_flow_system_status", snipe_enemies_master_flow_system_status)
				add_widget_int_read_only("dialogue_system_status", dialogue_system_status)
			stop_widget_group()
			
			start_widget_group("reset big foot global")
				add_widget_bool("reset global", widget_reset_big_foot_global)
			stop_widget_group()
				
		//#endif
	
		stop_widget_group()
		
		SET_UBER_PARENT_WIDGET_GROUP(widget_debug)
		
		SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
	ENDPROC
	
	PROC DESTROY_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widget_debug)
			DELETE_WIDGET_GROUP(widget_debug)
		ENDIF
	ENDPROC
	
	PROC SET_PED_FLOAT_NAME_DEBUG(PED_INDEX ped, FLOAT f)
		STRING s = GET_STRING_FROM_FLOAT(f)
		IF NOT IS_ENTITY_DEAD(ped)	
			SET_PED_NAME_DEBUG(ped, s)
		ENDIF

	ENDPROC


	
	PROC SHOW_PED_CAR_REC_TIME_DEBUG(PED_INDEX ped)
		VEHICLE_INDEX veh
		FLOAT f
		IF NOT IS_ENTITY_DEAD(ped)
			IF IS_PED_IN_ANY_VEHICLE(ped) 	
				veh = GET_VEHICLE_PED_IS_IN(ped)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
					f = GET_TIME_POSITION_IN_RECORDING(veh)
					SET_PED_FLOAT_NAME_DEBUG(ped, f)
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
	
	PROC GIVE_NAME_TO_PED(PED_INDEX ped, INT index, STRING sName)
		TEXT_LABEL t1

		IF NOT IS_ENTITY_DEAD(ped)
			t1 = ""
			t1 += sName
			t1+= " "
			t1 += index
			SET_PED_NAME_DEBUG(ped, t1)
		ENDIF
		
	ENDPROC
	
	proc camera_attached_to_entity_widget(entity_index mission_entity)

		if widget_entity_camera_active
			if not does_cam_exist(widget_camera)

				widget_camera = create_cam("default_scripted_camera", false)
				ATTACH_CAM_TO_ENTITY(widget_camera, mission_entity, widget_cam_attach_offset)
			
				set_cam_active(widget_camera, true)
				render_script_cams(true, false)
				
			else 
			
				ATTACH_CAM_TO_ENTITY(widget_camera, mission_entity, widget_cam_attach_offset)
				set_cam_rot(widget_camera, widget_rot)
				set_cam_fov(widget_camera, widget_fov)
				
			endif 

		endif 
	endproc 
	
	proc angled_area_locate_widget(entity_index mission_entity)
	
		if does_entity_exist(mission_entity)

			widget_bl = get_offset_from_entity_in_world_coords(mission_entity, widget_offset_bl) 
			widget_br = get_offset_from_entity_in_world_coords(mission_entity, widget_offset_br)

			IS_ENTITY_IN_ANGLED_AREA(mission_entity, widget_bl, widget_br, widget_angled_length, false, true)	
			
			if widget_offset_br.z > 0.0
				SET_ENTITY_HEADING(mission_entity, widget_heading)
			endif 
			
		endif 

	endproc
	
	
	proc attach_ped_to_object_and_move(ped_index miss_ped, vehicle_index miss_object)

		if DOES_ENTITY_EXIST(miss_ped)
			if not is_ped_injured(miss_ped)
				if DOES_ENTITY_EXIST(miss_object)
					ATTACH_ENTITY_TO_ENTITY(miss_ped, miss_object, 0, widget_object_pos, <<0.0, 0.0, 0.0>>)
					set_entity_rotation(miss_ped, widget_object_rot)
				endif 
			endif 
		endif
		
	endproc 
	
	proc draw_offset_marker_on_vehicle(vehicle_index mission_veh)

		if DOES_ENTITY_EXIST(mission_veh)
			if not is_entity_dead(mission_veh)
				DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(mission_veh, widget_object_pos), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.1, 0.1, 0.1>>, 255, 0, 128, 178)
				set_vehicle_fixed(mission_veh)
			endif 
		endif
		
	endproc 
	
	proc relative_pitch_and_heading_widget()
	
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(widget_heading)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(widget_pitch)
	
	endproc 
	
	proc reset_big_foot_global()
		
		if widget_reset_big_foot_global
			big_foot_global_created	= false
			widget_reset_big_foot_global = false
		endif 
		
	endproc 
	
#ENDIF

//***********LAWRENCE KERR
//*****************************************PUBLIC FUNCTIONS ******************************************************   

func bool mission_ped_injured(ped_index &this_ped)

	if DOES_ENTITY_EXIST(this_ped)
		return is_ped_injured(this_ped)
	endif 
	
	return false
	
endfunc 

func bool mission_vehicle_injured(vehicle_index &this_vehicle)
	
	if DOES_ENTITY_EXIST(this_vehicle)
		if not is_vehicle_driveable(this_vehicle)
			return true
		endif 
	endif 
	
	return false
	
endfunc 

func bool has_ped_task_finished_2(ped_index ped, script_task_name taskname = script_task_perform_sequence)
	
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if status = finished_task 
	or status = dormant_task
		return true
	endif
	
	return false
endfunc

//The players group does not include the player himself. RELGROUPHASH_PLAYER is the player
REL_GROUP_HASH player_group
REL_GROUP_HASH enemy_group
rel_group_hash animal_group

    
proc add_relationship_groups()

	ADD_RELATIONSHIP_GROUP("players group", player_group)
	ADD_RELATIONSHIP_GROUP("enemy group", enemy_group)
	ADD_RELATIONSHIP_GROUP("animal_group", animal_group)
	
endproc 

proc setup_relationship_contact(ped_index &this_ped, bool block_temporary_events = false)

	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, animal_group, player_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, animal_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
		
			SET_PED_RELATIONSHIP_GROUP_HASH(this_ped, player_group)
			
			SET_PED_COMBAT_MOVEMENT(this_ped, CM_DEFENSIVE)
			
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, true)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_USE_VEHICLE, false)
			
			sET_PED_TARGET_LOSS_RESPONSE(this_ped, tlr_never_lose_target)
			
			set_entity_is_target_priority(this_ped, false)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
			
		endif 
	endif 

endproc

proc setup_relationship_enemy(ped_index &this_ped, bool block_temporary_events = false)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
			
			SET_ped_RELATIONSHIP_GROUP_hash(this_ped, enemy_group) 
			
			SET_PED_COMBAT_MOVEMENT(this_ped, CM_DEFENSIVE)
			
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_USE_VEHICLE, false)
			
			sET_PED_TARGET_LOSS_RESPONSE(this_ped, tlr_never_lose_target)
			
			set_entity_is_target_priority(this_ped, true)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
		endif 
	endif 
	
endproc

proc setup_relationship_animal(ped_index this_ped)

	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, animal_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, animal_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, animal_group, player_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, animal_group, player_group)

	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
			
			SET_ped_RELATIONSHIP_GROUP_hash(this_ped, animal_group) 
			
			set_blocking_of_non_temporary_events(this_ped, true)
		endif 
	endif 

endproc

proc setup_enemy(ped_structure &enemy)

	clear_area(enemy.pos, 2.0, true)
		
	enemy.ped = create_ped(pedtype_mission, enemy.model, enemy.pos, enemy.heading)
	
	ADD_DEADPOOL_TRIGGER(enemy.ped, EXL2_KILLS)
	
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_MONEY(enemy.ped, 0)
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_seeing_range(enemy.ped, 250.00)
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_id_range(enemy.ped, 250.00)
	
	enemy.blip = CREATE_BLIP_FOR_PED(enemy.ped, true)
	set_blip_display(enemy.blip, DISPLAY_BLIP) 

	set_ped_can_evasive_dive(enemy.ped, true)
	
	setup_relationship_enemy(enemy.ped, true)
	
	set_ped_accuracy(enemy.ped, enemy.accuracy)
	
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemy.ped)
	
	enemy.created = true 

endproc

proc setup_enemy_in_vehicle(ped_structure &enemy, vehicle_index &mission_veh, vehicle_seat veh_seat = vs_driver)

	enemy.ped = create_ped_inside_vehicle(mission_veh, pedtype_mission, enemy.model, veh_seat)
	
	ADD_DEADPOOL_TRIGGER(enemy.ped, EXL2_KILLS)
	
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	set_current_ped_weapon(enemy.ped, enemy.weapon, true)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	enemy.created = true 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_hearing_range(enemy.ped, 250.00)
	
	enemy.blip = CREATE_BLIP_FOR_PED(enemy.ped, true)
	set_blip_display(enemy.blip, DISPLAY_BLIP) 
	
	set_ped_can_evasive_dive(enemy.ped, true)
	
	setup_relationship_enemy(enemy.ped, true)
	
	set_ped_accuracy(enemy.ped, enemy.accuracy)

endproc

proc setup_buddy(ped_structure &mission_buddy, bool setup_relationship_group = true)

	clear_area(mission_buddy.pos, 4.0, true)

	mission_buddy.ped = create_ped(pedtype_mission, mission_buddy.model, mission_buddy.pos, mission_buddy.heading)
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, false)
	set_ped_can_evasive_dive(mission_buddy.ped, false)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_keep_task(mission_buddy.ped, true)
	set_ped_can_ragdoll(mission_buddy.ped, false)
	
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableExplosionReactions, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableHurt, true)
	
	SET_PED_MONEY(mission_buddy.ped, 0)
	
	give_weapon_to_ped(mission_buddy.ped, mission_buddy.weapon, infinite_ammo, false)
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)
	
	mission_buddy.blip = CREATE_BLIP_FOR_PED(mission_buddy.ped, false)
	set_blip_display(mission_buddy.blip, DISPLAY_BLIP) 
	
	setup_relationship_contact(mission_buddy.ped, true)
	
	if setup_relationship_group = true
		setup_relationship_contact(mission_buddy.ped, true)
	else 
		set_blocking_of_non_temporary_events(mission_buddy.ped, true)
	endif 
	
	set_ped_name_debug(mission_buddy.ped, mission_buddy.name)

endproc 

PROC setup_CHOP_COLLAR()

	int chop_collar = 0
	// get chop collar
	
	IF HAS_PLAYER_USED_CHOP_APP()
	    chop_collar = GET_CHOP_COLLAR_FROM_APP()
	ELSE
	   chop_collar = 1
	ENDIF  
	
	SET_PED_COMPONENT_VARIATION(chop.ped, PED_COMP_TORSO, 0, chop_collar)
	
ENDPROC

proc setup_chop(ped_structure &mission_buddy)

	clear_area(mission_buddy.pos, 4.0, true)

	CREATE_CHOP(chop.ped, chop.pos, chop.heading)
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, false)
	set_ped_can_evasive_dive(mission_buddy.ped, false)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_keep_task(mission_buddy.ped, true)
	set_ped_can_ragdoll(mission_buddy.ped, false)
	//CLEAR_RAGDOLL_BLOCKING_FLAGS(chop.ped, rbf_vehicle_impact)
	
	SET_PED_MONEY(mission_buddy.ped, 0)
	SET_PED_MAX_TIME_UNDERWATER(chop.ped, 10000000)
	SET_PED_DIES_IN_WATER(chop.ped, false)
	
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableExplosionReactions, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableHurt, true)
	
	give_weapon_to_ped(mission_buddy.ped, mission_buddy.weapon, infinite_ammo, false)
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)
	
	mission_buddy.blip = CREATE_BLIP_FOR_PED(mission_buddy.ped, false)
	set_blip_display(mission_buddy.blip, DISPLAY_BLIP) 
	
	set_blocking_of_non_temporary_events(mission_buddy.ped, true)
	
	set_ped_name_debug(mission_buddy.ped, mission_buddy.name)

	add_ped_for_dialogue(scripted_speech, 5, chop.ped, "chop")
	
	//setup_relationship_contact(mission_buddy.ped, true)
	setup_relationship_animal(mission_buddy.ped)

endproc 

proc setup_buddy_attributes(ped_index miss_ped)
	
	set_ped_dies_when_injured(miss_ped, false)
	set_ped_can_be_targetted(miss_ped, false)
	set_ped_suffers_critical_hits(miss_ped, false)
	set_ped_can_evasive_dive(miss_ped, false)
	set_entity_is_target_priority(miss_ped, false)

	//set_ped_can_ragdoll(miss_ped, false)
	SET_RAGDOLL_BLOCKING_FLAGS(miss_ped, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE)
	
	set_ped_seeing_range(miss_ped, 250.00)
	set_ped_hearing_range(miss_ped, 250.00)
	set_ped_id_range(miss_ped, 250.00)
	
	SET_PED_CONFIG_FLAG(miss_ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(miss_ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(miss_ped, PCF_DisableExplosionReactions, true)
	SET_PED_CONFIG_FLAG(miss_ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(miss_ped, PCF_DisableHurt, true)
endproc 

proc setup_buddy_in_vehicle(ped_structure &mission_buddy, vehicle_index mission_veh, bool setup_relationship_group = true)

	clear_area(mission_buddy.pos, 4.0, true)

	mission_buddy.ped = create_ped_inside_vehicle(mission_veh, pedtype_mission, mission_buddy.model, vs_front_right)
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, false)
	set_ped_can_evasive_dive(mission_buddy.ped, false)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_keep_task(mission_buddy.ped, true)
	SET_PED_CONFIG_FLAG(mission_buddy, PCF_KeepRelationshipGroupAfterCleanUp)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_WillFlyThroughWindscreen, false) 
	set_ped_can_ragdoll(mission_buddy.ped, false)
	SET_PED_MONEY(mission_buddy.ped, 0)
	
	give_weapon_to_ped(mission_buddy.ped, mission_buddy.weapon, infinite_ammo, false)
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)
	
	if setup_relationship_group = true
		setup_relationship_contact(mission_buddy.ped, true)
	else 
		set_blocking_of_non_temporary_events(mission_buddy.ped, true)
	endif 
	
	set_ped_name_debug(mission_buddy.ped, mission_buddy.name)

endproc 

proc setup_animal(ped_structure &mission_buddy)

	clear_area(mission_buddy.pos, 4.0, true)

	mission_buddy.ped = create_ped(pedtype_mission, mission_buddy.model, mission_buddy.pos, mission_buddy.heading)
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, true)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_can_ragdoll(mission_buddy.ped, true)
	set_ped_keep_task(mission_buddy.ped, true)
	SET_PED_MONEY(mission_buddy.ped, 0)
	
	if mission_buddy.damaged_by_player
		set_entity_only_damaged_by_player(mission_buddy.ped, true)
	endif 
	
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)

	//setup_relationship_animal(mission_buddy.ped)
	set_blocking_of_non_temporary_events(mission_buddy.ped, true)
	
	set_ped_name_debug(mission_buddy.ped, mission_buddy.name)
	
	mission_buddy.created = true

endproc 


proc disable_dispatch_services()
		
	enable_dispatch_service(dt_fire_department, false)
	enable_dispatch_service(dt_police_automobile, false)
	enable_dispatch_service(dt_police_helicopter, false)
	enable_dispatch_service(dt_ambulance_department, false)
	
endproc 

proc enable_dispatch_services()

	enable_dispatch_service(dt_fire_department, true)
	enable_dispatch_service(dt_police_automobile, true)
	enable_dispatch_service(dt_police_helicopter, true)
	enable_dispatch_service(dt_ambulance_department, true)

endproc 

func bool start_new_cutscene_no_fade(bool clear_players_tasks = true, bool hide_weapon_for_cutscene = true, bool kill_conversation_line_immediately = true)

	if can_player_start_cutscene()
	
		SPECIAL_ABILITY_DEACTIVATE(player_id())
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
		
		clear_prints()
		clear_help()
		if kill_conversation_line_immediately
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
		else 
			kill_any_conversation()
		endif 

		display_hud(false)
		display_radar(false)
		set_widescreen_borders(true, 500)
		
		if clear_players_tasks
			set_player_control(player_id(), false, spc_clear_tasks)
		else 
			set_player_control(player_id(), false)
		endif 

		if hide_weapon_for_cutscene
			hide_ped_weapon_for_scripted_cutscene(player_ped_id(), true)
		endif 
		
		disable_dispatch_services()
		
		return true
	endif 
		
	return false

endfunc


proc end_cutscene(bool clear_tasks = true, float interp_heading = 0.0, float interp_pitch = 0.0, bool enable_emergency_services = true)

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)

	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	SET_FRONTEND_RADIO_ACTIVE(true)
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	
	if is_player_playing(player_id())
		
		render_script_cams(false, false)
		set_gameplay_cam_relative_heading(interp_heading)
		set_gameplay_cam_relative_pitch(interp_pitch)
			
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		
		set_player_control(player_id(), true)
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
	endif 

	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 
	
//	if mission_car_marked 
//		mark_vehicle_as_no_longer_needed(current_car)
//		mission_car_marked = false
//	endif 
	
	//allow_emergency_services(true)
	do_screen_fade_in(DEFAULT_FADE_TIME)
	
endproc

proc end_cutscene_no_fade(bool clear_tasks = true, bool update_game_camera = true, bool interpolate_behind_player = false, float interp_heading = 0.0, float interp_pitch = 0.0, int interp_to_game_time = 3000, bool enable_emergency_services = true)

	INFORM_MISSION_STATS_SYSTEM_OF_INengine_CUTSCENE_END()
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
	
	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	if is_player_playing(player_id())
		
		destroy_all_cams()
		
		if update_game_camera
			if interpolate_behind_player
				render_script_cams(false, true, interp_to_game_time)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			else 
				render_script_cams(false, false)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			endif 
		endif 
		
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		
		enable_special_ability(player_id(), true)
		
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
		
		set_player_control(player_id(), true)
	endif 
	
	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 

endproc


func bool ped_structure_are_all_enemies_dead(ped_structure &enemy_array[], bool set_ped_no_longer_needed = true)
	
	int peds_dead
	int i
	
	for i = 0 to (count_of(enemy_array) - 1)
		
		if DOES_ENTITY_EXIST(enemy_array[i].ped)
			if is_ped_injured(enemy_array[i].ped)
				
				if set_ped_no_longer_needed
					SET_PED_AS_NO_LONGER_NEEDED(enemy_array[i].ped) 
				endif 
				
				if does_blip_exist(enemy_array[i].blip)
					REMOVE_BLIP(enemy_array[i].blip)
				endif 
				
				peds_dead ++
			endif 
		
		else 
		
			if enemy_array[i].created
				peds_dead ++
			endif 
			
		endif 
		
	endfor 
	
	if peds_dead = count_of(enemy_array) 
		return TRUE
	endif 
	
	return FALSE
endfunc

proc setup_ped_proofs(ped_index this_ped)
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)
			set_entity_proofs(this_ped, true, true, true, true, true)
		endif 
	endif 
	
endproc 

proc deactivate_ped_proofs(ped_index this_ped)

	int ped_health
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)

			ped_health = get_entity_health(this_ped)

			if ped_health < 110
				set_entity_health(this_ped, 120)
			else 
				set_entity_health(this_ped, ped_health)
			endif 
			
			set_entity_proofs(this_ped, false, false, false, false, false)
		endif 
	endif 

endproc


proc setup_vehicle_proofs(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if is_vehicle_driveable(this_vehicle)
			
			if get_entity_health(this_vehicle) < 500
				set_entity_health(this_vehicle, 500)
			endif  
			
			if get_vehicle_engine_health(this_vehicle) < 500
				set_vehicle_engine_health(this_vehicle, 500.00)
			endif 
			
			if get_vehicle_petrol_tank_health(this_vehicle) < 500.00
				set_vehicle_petrol_tank_health(this_vehicle, 500.00)
			endif 
				
			set_entity_proofs(this_vehicle, true, true, true, true, true)
		endif 
	endif 
	
endproc

proc restore_vehicle_health(vehicle_index &mission_veh)
								
	if does_entity_exist(mission_veh)
		if is_vehicle_driveable(mission_veh)

			if get_entity_health(mission_veh) < 500
				set_entity_health(mission_veh, 500)
			endif  
			
			if get_vehicle_engine_health(mission_veh) < 500
				set_vehicle_engine_health(mission_veh, 500.00)
			endif 
			
			if get_vehicle_petrol_tank_health(mission_veh) < 500.00
				set_vehicle_petrol_tank_health(mission_veh, 500.00)
			endif 
			
		endif 
	endif 
	
endproc

proc deactivate_vehicle_proofs(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if is_vehicle_driveable(this_vehicle)
			set_entity_proofs(this_vehicle, false, false, false, false, false)
			restore_vehicle_health(this_vehicle)
		endif 
	endif 
	
endproc

//*****call this function underneath ped_structure_are_all_enemies_dead
func bool ped_structure_are_specific_number_enemies_dead(ped_structure &enemy_array[], int number_of_dead_peds)
	
	int peds_dead = 0
	int i
	
	for i = 0 to count_of(enemy_array) - 1
		if enemy_array[i].created
			if is_ped_injured(enemy_array[i].ped)
				peds_dead++
			endif
		endif 
	endfor 
	
	if peds_dead >= number_of_dead_peds
		return TRUE
	endif 
	
	return FALSE
endfunc

func int ped_structure_get_total_number_of_enemies_dead(ped_structure &enemy_array[])

	int peds_dead = 0
	int i
	
	for i = 0 to count_of(enemy_array) - 1
		if enemy_array[i].created
			if is_ped_injured(enemy_array[i].ped)
				peds_dead++
			endif
		endif 
	endfor 

	return peds_dead

endfunc 

func bool lk_timer(int &start_time, int time) 
	int current_time 
	current_time = get_game_timer()
	
	if ((current_time - start_time) > time) 
		return true
	endif 
	return false
endfunc

proc exit_vehicle_vehicle_blip_check(vehicle_index &mission_veh, blip_index &mission_veh_blip, vehicle_index &target_veh, blip_index &target_veh_blip, string vehicle_string, string target_string, bool &veh_text_printed, bool &trgt_text_printed) 
	
	if does_blip_exist(target_veh_blip)
		if is_vehicle_driveable(mission_veh)
			if not is_ped_sitting_in_vehicle(player_ped_id(), mission_veh)
				
				remove_blip(target_veh_blip)
		
				if not does_blip_exist(mission_veh_blip)
					
					mission_veh_blip = create_blip_for_vehicle(mission_veh) 
		
					clear_prints()

					if not vehicle_text_printed
						print_now(vehicle_string, default_god_text_time, 1)
						veh_text_printed = true 
					endif 
				endif
				
			endif 
		endif 
		
	else
	
		if is_vehicle_driveable(mission_veh)
			if is_ped_sitting_in_vehicle(player_ped_id(), mission_veh)
				
				if does_blip_exist(mission_veh_blip)
					REMOVE_BLIP(mission_veh_blip)
				endif 
				
				if does_entity_exist(target_veh)
					if is_vehicle_driveable(target_veh)
						
						target_veh_blip = create_blip_for_vehicle(target_veh, true)
				
						clear_prints()
	
						if not trgt_text_printed
							print_now(target_string, default_god_text_time, 1)
							trgt_text_printed = true 
						endif 
					endif 
				endif 
			endif
		endif 
	endif 
	
endproc

//gets the currenent car the player is in
func bool get_current_player_vehicle(vehicle_index &test_car)
	
	if is_ped_sitting_in_any_vehicle(player_ped_id())
		test_car = get_players_last_vehicle()
		if DOES_ENTITY_EXIST(test_car)				
			if is_vehicle_driveable(test_car)
				return true
			endif 
		endif 
	endif 
	
	return false
endfunc

struct quaternion
	float x
	float y
	float z
	float w
endstruct

func quaternion build_quaternion_from_euler_angles(float fPitch, float fRoll, float fHeading)

	quaternion quat

    float fltPitch2 
    float fltYaw2   
    float fltRoll2  

    fltPitch2 = fPitch * 0.5
    fltYaw2   = fRoll * 0.5 
    fltRoll2  = fHeading * 0.5

    float cosY2 = Cos(fltYaw2)
    float sinY2 = Sin(fltYaw2)
    float cosP2 = Cos(fltPitch2)
    float sinP2 = Sin(fltPitch2)
    float cosR2 = Cos(fltRoll2)
    float sinR2 = Sin(fltRoll2)

    Quat.x = cosR2 * sinP2 * cosY2 + sinR2 * cosP2 * sinY2
    Quat.y = cosR2 * cosP2 * sinY2 - sinR2 * sinP2 * cosY2
    Quat.y *= -1.0
    Quat.z = sinR2 * cosP2 * cosY2 - cosR2 * sinP2 * sinY2
    Quat.w = cosR2 * cosP2 * cosY2 + sinR2 * sinP2 * sinY2
	
	return quat

endfunc 

BOOL bHasSlideBeenInitialised
INT iRemainingTime
VECTOR vCarVelocity

FUNC BOOL SLIDE_entity_TO_POSITION_AND_ROTATION(entity_index entity_id, INT iTimeToSlide, VECTOR vTargetPosition, VECTOR vTargetRotation, FLOAT fZOffsetCorrection = 0.0)

	VECTOR pos
	VECTOR rot

	VECTOR vPos
	VECTOR vRot
	VECTOR vVel
	
	//quaternion quat

	FLOAT fFrameTime

	IF NOT (bHasSlideBeenInitialised)
	    iRemainingTime = iTimeToSlide
	    bHasSlideBeenInitialised = TRUE
	   if not is_entity_dead(entity_id)
	    	vCarVelocity = GET_ENTITY_SPEED_VECTOR(entity_id)
	    ENDIF
	ENDIF

	// if time goes below zero we are near enough there.
	IF (iRemainingTime <= 0)
        bHasSlideBeenInitialised = FALSE
        RETURN(TRUE)
	ENDIF 

	if not is_entity_dead(entity_id)

        fFrameTime = GET_FRAME_TIME()
        
        // get car current pos and rot
        pos = GET_ENTITY_COORDS(entity_id)

        
        rot.x = GET_ENTITY_PITCH(entity_id)
        rot.y = GET_ENTITY_ROLL(entity_id )
        rot.z = GET_ENTITY_HEADING(entity_id )                    
        
        // update the stored car velocity.
        vVel = <<0.0, 0.0, 0.0>> - vCarVelocity
        vVel /= (TO_FLOAT(iRemainingTime) /1000.0) / fFrameTime
        vCarVelocity += vVel
        
        // update the pos with the velocity
        pos += (vCarVelocity * fFrameTime)
        
        // get vec from current to target
        vPos = vTargetPosition - pos
        vRot = vTargetRotation - rot
		
        
        //-- Figure out if it's quicker rotating clockwise or anti-clockwise
        IF vRot.z < 0.0
        	vRot.z += 360.0
        ENDIF
        IF vRot.z > (vTargetRotation.z + 180.0)
           vRot.z -= 360.0
        ENDIF
        
        VECTOR vRotNorm = NORMALISE_VECTOR(vRot)
        VECTOR vPosNorm = NORMALISE_VECTOR(vPos)

        // resize vectors to proper length, taking the frame time into account  
		//get speed via v = d/t d = VMAG(vPos)  t = TO_FLOAT(iRemainingTime)) * (fFrameTime * 1000.0)
        vPosNorm *= (VMAG(vPos) / TO_FLOAT(iRemainingTime)) * (fFrameTime * 1000.0)
        vRotNorm *= (VMAG(vRot) / TO_FLOAT(iRemainingTime)) * (fFrameTime * 1000.0)

        
        // calculate new position and rotation
        pos += vPosNorm
        rot += vRotNorm

        
        // set the car position / rotation
        //quat = BUILD_QUATERNION_FROM_EULER_ANGLES(rot.x, rot.y, rot.z)
        
		pos.z += fZOffsetCorrection
       	SET_ENTITY_COORDS_NO_OFFSET(entity_id, pos)
		
		//SET_ENTITY_QUATERNION(entity_id, quat.x, quat.y, quat.z, quat.w) 
		set_entity_rotation(entity_id, rot)
        
        // update the remaining time                     
        iRemainingTime -= ROUND(fFrameTime * 1000.0)    
     
	ENDIF

	RETURN(FALSE)

ENDFUNC

func bool has_player_antagonised_ped(ped_index &miss_ped, float distance, bool distance_check_on = true)

	if not is_ped_injured(miss_ped)
		
		if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(miss_ped)) < distance
			//if has_char_spotted_char(this_ped, player_char_id())
			if CAN_PED_SEE_HATED_PED(miss_ped, PLAYER_PED_ID())
				IF IS_ped_ARMED(PLAYER_ped_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)
					
					if IS_PLAYER_TARGETTING_ENTITY(player_id(), miss_ped)
						return TRUE
					endif 
					
					if is_player_free_aiming_at_entity(player_id(), miss_ped)
						return TRUE
					endif
				endif 
			endif 
			
			if is_ped_shooting(player_ped_id())
				return true
			endif
			
			if distance_check_on
				return true
			endif 
			
		endif
		
		if is_bullet_in_area(get_entity_coords(miss_ped), 4.0)
			return true
		endif 

	endif 

	return FALSE
endfunc 


func bool is_vehicle_stuck_every_check(vehicle_index &vehicle)

	if DOES_ENTITY_EXIST(vehicle)

		if is_vehicle_driveable(vehicle)
		
			if not IS_PLAYER_SWITCH_IN_PROGRESS()
			
				if is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_ON_ROOF, ROOF_TIME)
				or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_JAMMED, JAMMED_TIME)
				or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
				or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_ON_SIDE, SIDE_TIME)
				
					return true
				endif 
			endif 
		endif 
	endif 
	
	return false
endfunc


proc repostion_players_last_vehicle(vehicle_index &mission_veh, vector area_pos_check, vector area_dimensions, vector veh_pos, float veh_heading)
	
	mission_veh = get_players_last_vehicle()

	if does_entity_exist(mission_veh)
		if is_vehicle_driveable(mission_veh)
			if is_entity_at_coord(mission_veh, area_pos_check, area_dimensions)
				
				if not is_entity_a_mission_entity(mission_veh)
					set_entity_as_mission_entity(mission_veh)
				endif 
				
				if not is_vehicle_in_players_garage(mission_veh, get_current_player_ped_enum(), false)
				
					clear_area(veh_pos, 10.0, true)
					set_entity_coords(mission_veh, veh_pos)  
					set_entity_heading(mission_veh, veh_heading)
					set_vehicle_on_ground_properly(mission_veh)
					
				endif 
			endif 
		endif
	endif 
	
endproc 

func bool is_mission_ped_on_screen(ped_index &miss_ped)
				
	if does_entity_exist(miss_ped)
		if not is_ped_injured(miss_ped)
			if is_entity_on_screen(miss_ped)
				
				return true 
				
			endif 
		endif 
	endif 

	return false 

endfunc 


PROC EQUIP_BEST_PLAYER_WEAPON(ped_index mission_ped, BOOL bForceIntoHand = true)
  
  IF NOT IS_PED_INJURED(mission_ped)
       
		WEAPON_TYPE bestWeapon = GET_BEST_PED_WEAPON(mission_ped)
        
		IF bestWeapon != WEAPONTYPE_UNARMED
             SET_CURRENT_PED_WEAPON(mission_ped, bestWeapon, bForceIntoHand)
        ENDIF
  ENDIF
  
ENDPROC

proc setup_weapon_for_ped(ped_index mission_ped, weapon_type ped_weapon, int num_of_bullets, bool force_weapon_into_hand = true, bool equip_weapon = true, bool equipe_best_weapon = true)
	
	IF not HAS_PED_GOT_FIREARM(mission_ped, false)
		
		give_weapon_to_ped(mission_ped, ped_weapon, num_of_bullets, force_weapon_into_hand, equip_weapon)
		exit
	
	else 
	
		weapon_type players_weapon

		if equipe_best_weapon
		
			EQUIP_BEST_PLAYER_WEAPON(mission_ped, force_weapon_into_hand)
			get_current_ped_weapon(mission_ped, players_weapon)
			
		else 
		
			players_weapon = Get_Fail_Weapon(enum_to_int(get_player_ped_enum(mission_ped)))
				
			if players_weapon = WEAPONTYPE_INVALID
			or players_weapon = WEAPONTYPE_UNARMED 
				
				if not HAS_PED_GOT_WEAPON(mission_ped, ped_weapon)
					give_weapon_to_ped(mission_ped, ped_weapon, num_of_bullets, force_weapon_into_hand, equip_weapon)
					exit
				else 
					players_weapon = ped_weapon 
				endif 

			endif 

		endif 
		
		if players_weapon = WEAPONTYPE_GRENADELAUNCHER
		or players_weapon = WEAPONTYPE_RPG
			
			if get_ammo_in_ped_weapon(mission_ped, players_weapon) < 5
				set_ped_ammo(mission_ped, players_weapon, 5)
			endif 
			
		else 

			if get_ammo_in_ped_weapon(mission_ped, players_weapon) < num_of_bullets
				set_ped_ammo(mission_ped, players_weapon, num_of_bullets)
			endif 
			
		endif 
		
		set_current_ped_weapon(mission_ped, players_weapon, force_weapon_into_hand) 

	endif 
	
endproc 

func int get_closest_pos_to_player(vector &position[])
				
	int i
	int closest_node
	float smallest_distance_between_vectors
	float current_distance_between_vectors

	for i = 0 to count_of(position) - 1
	
		current_distance_between_vectors = get_distance_between_coords(get_entity_coords(player_ped_id()), position[i]) 
	
		if i = 0
		
			smallest_distance_between_vectors = current_distance_between_vectors
			closest_node = i
			
		else 
		
			if current_distance_between_vectors < smallest_distance_between_vectors
				smallest_distance_between_vectors = current_distance_between_vectors
				closest_node = i
			endif 
			
		endif 
		
	endfor 
	
	
	return closest_node	

endfunc  

proc get_dialogue_data_for_future_resumption() 

	//if not obtain_dialogue_data
	
		if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		
			dialogue_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			specific_label = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
			
			if not are_strings_equal(dialogue_root, "null")
			and not are_strings_equal(specific_label, "null")
				resume_lower_priority_conversation = true
			endif 
			
//			printstring(dialogue_root)
//			printnl()
//			
//			printstring(specific_label)
//			printnl()
			
			//script_assert("store label")
		
		endif 
		
		//obtain_dialogue_data = true 
		
	//endif
	
endproc 

func bool in_car_dialogue_monitoring_system(blip_index target_blip, is_any_text_displayed_flags text_displayed_flag = iat_ignore_god_text_if_subtitles_off)

	if does_blip_exist(target_blip)
	
		if IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			PAUSE_FACE_TO_FACE_CONVERSATION(false)
		endif
		
		if resume_lower_priority_conversation
			if not is_any_text_being_displayed(locates_data)
				if create_conversation_from_specific_line(scripted_speech, "fam4aud", dialogue_root, specific_label, CONV_PRIORITY_LOW) 
					resume_lower_priority_conversation = false
				endif 
			endif 
		endif 

		if not is_any_text_being_displayed(locates_data, text_displayed_flag)
		and not resume_lower_priority_conversation
			return true 
		endif 
		
	else 
	
		if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			PAUSE_FACE_TO_FACE_CONVERSATION(true)
		endif 
	
	endif 
	
	return false 

endfunc 

func bool is_ped_in_front_of_ped(ped_index mission_ped_0, ped_index mission_ped_1)

	if does_entity_exist(mission_ped_0)
		if does_entity_exist(mission_ped_1)
			if not is_ped_injured(mission_ped_0)
				if not is_ped_injured(mission_ped_1)
					
					vector pos_a
					vector pos_b
					vector vec_ba
					
					vector ped_1_offset
					vector ped_1_unit_vector
	
					pos_a = get_entity_coords(mission_ped_0) 
					
					pos_b = get_entity_coords(mission_ped_1) 
				
					vec_ba = pos_b - pos_a
					
					ped_1_offset = get_offset_from_entity_in_world_coords(mission_ped_1, <<0.0, 0.5, 0.0>>)
					
					ped_1_unit_vector = ped_1_offset - pos_b
					
					if dot_product(vec_ba, ped_1_unit_vector) < 0.0
						
						#IF IS_DEBUG_BUILD
						printstring("infront")
						printnl()
						#endif
						
						return (true)
					endif 
				endif 
			endif 
		endif 
	endif 
	
	#IF IS_DEBUG_BUILD
	printstring("behind")
	printnl()
	#endif 
					
	return false
	
endfunc 

proc set_players_last_vehicle_to_vehicle_gen(vector car_pos, float car_heading)

	vehicle_index players_last_car 
	
	players_last_car = GET_PLAYERS_LAST_VEHICLE()
	
	if does_entity_exist(players_last_car)
		if is_vehicle_driveable(players_last_car)
			
			SET_MISSION_VEHICLE_GEN_VEHICLE(players_last_car, car_pos, car_heading) 
			
		endif 
	endif 
	
endproc 

func bool HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(ped_index miss_ped)
	
	if does_entity_exist(miss_ped)
		if not is_ped_injured(miss_ped)
			if HAVE_ALL_STREAMING_REQUESTS_COMPLETED(miss_ped)
				return true 
			endif 
		endif 
	endif 
	
	return false 

endfunc


FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

/// PURPOSE:
///    Sorts the triggered label array such that all the empty elements are at the end.
PROC REMOVE_LABEL_ARRAY_SPACES()
	INT i = 0
	REPEAT (COUNT_OF(i_triggered_text_hashes) - 1) i
	    IF i_triggered_text_hashes[i] = 0
			IF i_triggered_text_hashes[i+1] != 0
	        	i_triggered_text_hashes[i] = i_triggered_text_hashes[i+1]
	            i_triggered_text_hashes[i+1] = 0
	        ENDIF
	    ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Finds the array index of a particular triggered label, or -1 if the label hasn't been added.
FUNC INT GET_LABEL_INDEX(INT i_label_hash)
	INT i = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) i
	    IF i_triggered_text_hashes[i] = 0 //We've reached the end of the filled section of the array, no need to continue.
	    	RETURN -1
	    ELIF i_triggered_text_hashes[i] = i_label_hash
	    	RETURN i
	    ENDIF
	ENDREPEAT

	RETURN -1
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the given label has been triggered.  
FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING str_label)
	RETURN (GET_LABEL_INDEX(GET_HASH_KEY(str_label)) != -1)
ENDFUNC

/// PURPOSE:
///    Adds/removes a text label to/from the list of labels that have been triggered.
PROC SET_LABEL_AS_TRIGGERED(STRING str_label, BOOL b_trigger)
	
	INT i_hash = GET_HASH_KEY(str_label)
	INT i = 0

	IF b_trigger
	    BOOL b_added = FALSE
	    
	    WHILE NOT b_added AND i < COUNT_OF(i_triggered_text_hashes)
			
			IF i_triggered_text_hashes[i] = i_hash //The label is already in the array, don't add it again.
            	b_added = TRUE 
         	ELIF i_triggered_text_hashes[i] = 0
            	i_triggered_text_hashes[i] = i_hash
                b_added = TRUE
          	ENDIF
          
         	 i++
	    ENDWHILE

	    #IF IS_DEBUG_BUILD
	    	IF NOT b_added
		        SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Failed to add label, array is full.")
		  ENDIF
	    #ENDIF
	
	ELSE
	   
		INT i_index = GET_LABEL_INDEX(i_hash)
	    IF i_index != -1
	          i_triggered_text_hashes[i_index] = 0
	          REMOVE_LABEL_ARRAY_SPACES()
	    ENDIF
		
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT i = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) i
		i_triggered_text_hashes[i] = 0
	ENDREPEAT
ENDPROC

// PURPOSE: caps heading to between 0.0 and 360.0
///    
FUNC FLOAT CAP_HEADING(FLOAT heading)

    float mod = heading
    
    IF heading < 0.0
        heading += ((FLOOR(mod / -360.0) + 1) * 360)
    ELIF heading >= 360.0
        heading = mod % 360.0
    ENDIF

    RETURN heading

ENDFUNC

func bool are_vectors_alomost_equal(vector vector_a, vector vector_b, vector unit_vector_x_multiplier)

	//vec b is the target vector
	//vec a is the current positon of the object
	//unit_vector_x_multiplier is the unitvector * frame time * multiplier
	
	vector vector_ba
	vector future_vector

	vector_ba = vector_b - vector_a
	
//	//--DEBUG OUTPUT
//	printstring("vector ba magnitude: ")
//	printfloat(vmag(vector_ba))
//	printnl()
//	
//	future_vector = (vector_b - (vector_a + unit_vector_x_multiplier))
//	
//	printstring("future vector magnitude: ")
//	printfloat(vmag(future_vector))
//	printnl()
//	//--
	
	
	if (vmag(vector_ba) < 0.2)
		return true
	endif
	

	future_vector = (vector_b - (vector_a + unit_vector_x_multiplier))
	
	if vmag(vector_ba) < 20	//added for rocket launcher swerve function
		if (vmag(future_vector) > vmag(vector_ba))
			return true
		endif 
	endif 
	
	return false

endfunc 

vector current_direction_vec
vector desired_direction_vec
vector rpg_target_pos_0 = <<-1552.5, 4451, 38.3>>
vector rpg_target_pos_1 
vector rpg_velocity

func bool rpg_system()

//	printstring("rpg_system_status: ")
//	printint(rpg_system_status)
//	printnl()
				
	switch rpg_system_status 
	
		case 0
		
			rpg.pos = get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 1.5, 0.7>>)

//			rpg_obj = SET_PED_SHOOT_ORDNANCE_WEAPON(gang_enemy[3].ped)
//			rpg.pos = get_entity_coords(rpg_obj)
//			rpg.rot = get_entity_rotation(rpg_obj)
//			clear_area(rpg.pos, 0.5, true)
			
			rpg_obj = create_object(rpg.model, rpg.pos)
			set_entity_proofs(rpg_obj, true, true, true, true, true)
			set_entity_rotation(rpg_obj, rpg.rot)
			set_entity_load_collision_flag(rpg_obj, true)
			fxRpg = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ex2_rpg_trail", rpg_obj, <<0.0, -0.2, 0.0>>,  <<0.0, 0.0, 0.0>>)

			rpg_target_pos_0 = <<-1532.0, 4469.2, 23.00>>//<<-1537.21, 4469.92, 19.00>>
			current_direction_vec = normalise_vector(rpg_target_pos_0 - rpg.pos)

			//SOH CAH TOA            opposite	         adjacient 
			rpg.rot.x = ATAN2(current_direction_vec.z, VMAG(<<current_direction_vec.x, current_direction_vec.y, 0.0>>))
			rpg.rot.z = CAP_HEADING(ATAN2(current_direction_vec.y, current_direction_vec.x) - 90.0)
	
			
			switch get_current_player_ped_enum()
			
				case char_michael
			
					PLAY_SOUND_FROM_coord(-1, "Fire_RPG", get_entity_coords(rpg_obj), "EXILE_2_SOUNDS")
					PLAY_SOUND_FROM_ENTITY(rpg_sound, "Fire_Missile_Loop", rpg_obj, "EXILE_2_SOUNDS")
					
				break 
				
				case char_franklin
				
					PLAY_SOUND_FROM_coord(-1, "Fire_RPG_On_Foot", get_entity_coords(rpg_obj), "EXILE_2_SOUNDS")
					PLAY_SOUND_FROM_ENTITY(rpg_sound, "Fire_Missile_Loop", rpg_obj, "EXILE_2_SOUNDS")
				
				break 
				
			endswitch 
			
			PLAY_SOUND_FROM_ENTITY(rpg_alarm_sound, "Missile_Warning_Sound", helicopter.veh, "EXILE_2_SOUNDS")
			
			rpg_target_pos_1 = get_offset_from_entity_in_world_coords(helicopter.veh, <<-5.0, 0.0, -0.5>>)
			
			original_time = get_game_timer()
			
			rpg_system_status++
			
			//rpg_system_status = 22
			
			//script_assert("test 1")

		break 
		
		case 1
		
			rpg.pos = get_entity_coords(rpg_obj)
		
			rpg_target_pos_1 = get_offset_from_entity_in_world_coords(helicopter.veh, <<-5.0, 0.0, -0.5>>)

			desired_direction_vec = normalise_vector(rpg_target_pos_1 - rpg.pos)

			current_direction_vec.x = current_direction_vec.x + ((desired_direction_vec.x - current_direction_vec.x) * 0.15)
			current_direction_vec.y = current_direction_vec.y + ((desired_direction_vec.y - current_direction_vec.y) * 0.15)
			current_direction_vec.z = current_direction_vec.z + ((desired_direction_vec.z - current_direction_vec.z) * 0.15)
			
			rpg.rot.x = ATAN2(current_direction_vec.z, VMAG(<<current_direction_vec.x, current_direction_vec.y, 0.0>>))
			rpg.rot.z = CAP_HEADING(ATAN2(current_direction_vec.y, current_direction_vec.x) - 90.0)
																	
			//rpg.pos = get_entity_coords(rpg_obj)
			
			if not are_vectors_alomost_equal(rpg.pos, rpg_target_pos_1, (current_direction_vec * get_frame_time() * 50.00)) 
//				printstring("VECTORS NOT EQUAL")
//				printnl()
			else
//				printstring("EQUAL")
//				printnl()
			endif 
				
			
			if not are_vectors_alomost_equal(rpg.pos, rpg_target_pos_1, (current_direction_vec * get_frame_time() * 50.00)) 
			and not lk_timer(original_time, 2500)//safety check	
				
				rpg.pos += (current_direction_vec * get_frame_time() * 50.00)
				
				rpg_velocity = (current_direction_vec * 50.00)
				
				//set_entity_coords_no_offset(rpg_obj, rpg.pos)  
				set_entity_velocity(rpg_obj, rpg_velocity)
				set_entity_rotation(rpg_obj, rpg.rot)
				
			else 
	
				start_rpg_cutscene = true
					
				rpg_system_status = 3

				return true

			endif 
			
		break 
		
		case 2
		
			if not selector_cam.bSplineActive
			
				start_rpg_cutscene = true
					
				rpg_system_status = 3
			
				return true
			
			endif 
		
		break 
		
		case 3
		
			int ammo_in_clip 
			ammo_in_clip = 0
		
			if get_ammo_in_clip(gang_enemy[3].ped, weapontype_rpg, ammo_in_clip)
				if ammo_in_clip >= 1
		
					if IS_PED_WEAPON_READY_TO_SHOOT(gang_enemy[3].ped)
					
						rpg_obj = SET_PED_SHOOT_ORDNANCE_WEAPON(gang_enemy[3].ped)

						if does_entity_exist(rpg_obj)

							set_entity_proofs(rpg_obj, true, true, true, true, true)
						
							rpg.pos = get_entity_coords(rpg_obj)
							rpg.rot = get_entity_rotation(rpg_obj)
							//delete_entity(rpg_obj)
							//REMOVE_ALL_PROJECTILES_OF_TYPE(WEAPONTYPE_RPG)
							clear_area(rpg.pos, 0.5, true)
							
							rpg_obj = create_object(rpg.model, rpg.pos)
							set_entity_rotation(rpg_obj, rpg.rot)
							set_entity_load_collision_flag(rpg_obj, true)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(rpg_obj, false)
							fxRpg = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ex2_rpg_trail", rpg_obj, <<0.0, -0.2, 0.0>>,  <<0.0, 0.0, 0.0>>)
							
							//SOH CAH TOA            opposite	         adjacient 

							current_direction_vec = normalise_vector(<<-1537.21, 4469.92, 19.00>> - get_entity_coords(rpg_obj))
							//current_direction_vec = normalise_vector(<<-1513.6, 4468.4, 19.1>> - get_entity_coords(rpg_obj))
							
							rpg.rot.x = ATAN2(current_direction_vec.z, VMAG(<<current_direction_vec.x, current_direction_vec.y, 0.0>>))
							rpg.rot.z = CAP_HEADING(ATAN2(current_direction_vec.y, current_direction_vec.x) - 90.0)

							
							switch get_current_player_ped_enum()
							
								case char_michael
							
									PLAY_SOUND_FROM_coord(-1, "Fire_RPG", get_entity_coords(rpg_obj), "EXILE_2_SOUNDS")
									PLAY_SOUND_FROM_ENTITY(rpg_sound, "Fire_Missile_Loop", rpg_obj, "EXILE_2_SOUNDS")
									
								break 
								
								case char_franklin
								
									PLAY_SOUND_FROM_coord(-1, "Fire_RPG_On_Foot", get_entity_coords(rpg_obj), "EXILE_2_SOUNDS")
									PLAY_SOUND_FROM_ENTITY(rpg_sound, "Fire_RPG_On_Foot", rpg_obj, "EXILE_2_SOUNDS")
								
								break 
								
							endswitch 
							
							PLAY_SOUND_FROM_ENTITY(rpg_alarm_sound, "Missile_Warning_Sound", helicopter.veh, "EXILE_2_SOUNDS")
							
							rpg_system_status++
							
						endif 
						
					endif 
					
				endif 
			endif 

		break 
		
		case 4
		
			rpg_target_pos_1 = get_offset_from_entity_in_world_coords(helicopter.veh, <<-18.0, 0.5, 0.5>>)

			desired_direction_vec = normalise_vector(rpg_target_pos_1 - rpg.pos)

			current_direction_vec.x = current_direction_vec.x + ((desired_direction_vec.x - current_direction_vec.x) * 0.15)
			current_direction_vec.y = current_direction_vec.y + ((desired_direction_vec.y - current_direction_vec.y) * 0.15)
			current_direction_vec.z = current_direction_vec.z + ((desired_direction_vec.z - current_direction_vec.z) * 0.15)
			
			rpg.rot.x = ATAN2(current_direction_vec.z, VMAG(<<current_direction_vec.x, current_direction_vec.y, 0.0>>))
			rpg.rot.z = CAP_HEADING(ATAN2(current_direction_vec.y, current_direction_vec.x) - 90.0)
																		
			rpg.pos = get_entity_coords(rpg_obj)
			
			printvector(rpg.pos)
			printnl()
			
			if not are_vectors_alomost_equal(rpg.pos, rpg_target_pos_1, (current_direction_vec * get_frame_time() * 35.00))
				
				rpg.pos += (current_direction_vec * get_frame_time() * 35.00)
				
				rpg_velocity = (current_direction_vec * 35.00)//35
				
				//set_entity_coords(rpg_obj, rpg.pos) 
				set_entity_velocity(rpg_obj, rpg_velocity)
				set_entity_rotation(rpg_obj, rpg.rot)
				
//				printstring("not equal")
//				printnl()
				
			else 

				ADD_EXPLOSION(GET_ENTITY_COORDS(rpg_obj), EXP_TAG_ROCKET, 1.0)
				if does_particle_fx_looped_exist(fxrpg)
					STOP_PARTICLE_FX_LOOPED(fxrpg)
				endif 
				DELETE_entity(rpg_obj)
				
				if not has_sound_finished(rpg_sound)
					stop_sound(rpg_sound)
				endif 
				
				if not has_sound_finished(rpg_alarm_sound)
					stop_sound(rpg_alarm_sound)
				endif 
				
				rpg_system_status = 3
				
				//script_assert("test 2")
				
				return true
			endif 
		
		break
		
	endswitch 
	
	return false

endfunc 



//--------------------------------------------PRIVATE FUNCTIONS---------------------------------------------------

proc get_chop_to_the_edge_of_the_river()
	//123
	open_sequence_task(seq)
		TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_0", 126, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_USE_TIGHTER_TURN_SETTINGS, 227)  //EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT
		task_go_straight_to_coord(null, chop_cutscene_pos, pedmove_run, -1, chop_cutscene_rot.z)
	close_sequence_task(seq)
	task_perform_sequence(chop.ped, seq)
	clear_sequence_task(seq)
	
	SET_PED_MIN_MOVE_BLEND_RATIO(chop.ped, PEDMOVE_SPRINT)
	FORCE_PED_MOTION_STATE(chop.ped, MS_ON_FOOT_SPRINT, TRUE)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(chop.ped)

endproc 

proc chop_breathing_effect()

	if chop_cam.state = CHOP_VIEW_CAM_ON_CHOP
			
		if not DOES_PARTICLE_FX_LOOPED_EXIST(chop_cam.scentPTFX)
		
			if lk_timer(chop_ptfx_time, 500)

				chop_cam.scentPTFX = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ex2_chop_trail", chop.ped,  <<0.0, 10.0, 0.3>>, <<0.0, 0.0, 0.0>>, 7.0) //<<0.0, 0.0, get_entity_heading(chop.ped)>>
			
				chop_ptfx_time = get_game_timer()
				
			endif 
			
		else 
		
			if lk_timer(chop_ptfx_time, 500)
				if does_particle_fx_looped_exist(chop_cam.scentPTFX)
					stop_particle_fx_looped(chop_cam.scentPTFX)
					chop_ptfx_time = get_game_timer()
				endif 
			endif 
		
		endif
		
	else 
	
		if DOES_PARTICLE_FX_LOOPED_EXIST(chop_cam.scentPTFX)
			stop_particle_fx_looped(chop_cam.scentPTFX)
		endif 
		
	endif 
	
endproc 

proc render_chop_move_help_message()
						
	if not has_label_been_triggered("chop_help3")
		
		if not IS_HELP_MESSAGE_ON_SCREEN()
		and get_current_player_ped_enum() = char_michael 
		and not does_cam_exist(selector_cam.camID)

			print_help("chop_help3")
				
			set_label_as_triggered("chop_help3", true)
			
		endif 
	endif 
	
endproc

proc reset_chop_audio_flags()

endproc 


proc chop_audio_system(chop_audio_enum chop_audio_status, bool force_chop_audio = false)

	if lk_timer(chop_time, 3000) //not animal_VOCALIZATION_playing
	or force_chop_audio
	
		switch chop_audio_status

			case chop_with_franklin

				switch get_random_int_in_range(0, 4) 
				
					case 0
					case 1

						PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "CHOP_SNIFF_SEQ")
						
					break 
					
					case 2
					
						PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "CHOP_WHINE")
						
					break 
					
					case 3
					
						PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "CHOP_LICKS_MOUTH")
						
					break 
					
				endswitch
				
//				if force_chop_audio
//					force_chop_audio = false
//				endif 
				
				chop_time = get_game_timer()
				
			break 
 
		
			case chop_running

				PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "CHOP_PANT") 
					
				chop_time = get_game_timer()
			
			break 
			
			case chop_finds_enemy
			
				switch get_random_int_in_range(0, 5) 
				
					case 0
					case 1
					case 2
				
						PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "bark")
		
					break 
					
					case 3

						PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "GROWL")
						
					break 
					
					case 4

						PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "SNARL")
						
					break 

				endswitch
				
				chop_time = get_game_timer()
			
			break 
			
			case chop_bark_button
			
				switch get_random_int_in_range(0, 2) 
				
					case 0
				
						PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "BARK_SEQ") 
						
					break 
					
					case 1
			
						PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "bark") 
						
					break 
					
				endswitch 
				
				original_time = get_game_timer()
			
			break 
			
			case chop_no_audio
			
			break 
		
		endswitch 
		
	endif 

endproc 


proc chop_ai_system()

	if not is_ped_injured(chop.ped)
	
		if snipe_enemies_master_flow_system_status > 3
			SET_CHOP_VIEW_CAM_HANDLE_WATER(chop_cam) 
		endif 
	
		switch chop_behaviour 
		
			case CHOP_BEHAVIOUR_BAD
			
				switch chop_ai_system_status
		
					
					case 0

						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<-1891.7518, 4432.0229, 43.7444>>, pedmove_run, -1, -1) //<<-1893.8273, 4432.7607, 43.4261>>, pedmove_run, -1, -1)
							task_turn_ped_to_face_entity(null, player_ped_id())
						close_sequence_task(seq)
						task_perform_sequence(chop.ped, seq)
						clear_sequence_task(seq)
						
						fORCE_PED_MOTION_STATE(chop.ped, MS_ON_FOOT_run, TRUE)
						
						force_ped_ai_and_animation_update(chop.ped)

						//chop_audio_system(chop_running)
					
						chop_ai_system_status++
					
					break 
					
					case 1
					
						if not is_entity_at_coord(player_ped_id(), <<-1895.8339, 4431.2158, 43.2800>>, <<4.0, 4.0, 2.0>>)
						or not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
							open_sequence_task(seq)
								task_follow_nav_mesh_to_coord(null, <<-1878.6715, 4479.3267, 25.4878>>, pedmove_sprint, -1, 0.5, ENAV_STOP_EXACTLY)
							close_sequence_task(seq)
							task_perform_sequence(chop.ped, seq)
							clear_sequence_task(seq)
						
							//chop_audio_system(chop_bark_button, true)
							
							chop_ai_system_status++
						
						endif 
		
					break 
					
					case 2
					
						request_anim_dict("missexile2")
						
						if not is_entity_at_coord(chop.ped, <<-1878.6715, 4479.3267, 25.4878>>, <<4.0, 4.0, 2.0>>)
							if has_ped_task_finished_2(chop.ped)
								
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1878.6715, 4479.3267, 25.4878>>, pedmove_run, -1, 0.5, ENAV_STOP_EXACTLY)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
								
							endif 
							
							//chop_audio_system(chop_running)
							
						else 
						
							if has_anim_dict_loaded("missexile2")

								if has_ped_task_finished_2(chop.ped)
							
									open_sequence_task(seq)					
										task_play_anim(null, "missexile2", "fra0_ig_14_chop_sniff_fwds", slow_blend_in, WALK_BLEND_OUT, -1)  
										task_play_anim(null, "missexile2", "fra0_ig_12_chop_waiting_a", WALK_BLEND_IN, normal_blend_out, -1, af_looping)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)
									
									chop_ai_system_status++
									
								endif 
								
							endif 

							//chop_audio_system(chop_finds_enemy)

						endif 

					break 
					
					case 3
					
					
					break 
					
					case 101

						switch get_current_player_ped_enum() 
						
							case char_franklin

								if chop_cam.state != chop_view_cam_on_chop

									if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) > 10
										SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
									endif 

									if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
											
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

											task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
										
										endif 
								
									else 
									
										if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

											if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
												if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
													
													float moveSpeed 
													moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(player_ped_id())
													
													if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
											
														task_turn_ped_to_face_entity(chop.ped, player_ped_id())	
														
													endif 

												endif 
											endif 
											
										endif 
										
									endif 
								
								else 
								
									if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 5.0, 0.0>>), <<2.5, 2.5, 2.5>>)
										
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

											task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, 0.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
										
										endif 
								
									else 
									
										if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

											if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
												if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											
													task_turn_ped_to_face_entity(chop.ped, player_ped_id())		

												endif 
											endif 
											
										endif 
										
									endif 
									
								
								endif 
								
							break 
							
							case char_michael
							
								//you are MICHAEL and not fake chop
								if chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
							
									IF IS_ENTITY_ON_SCREEN(chop.ped) 
										IF GET_GAMEPLAY_CAM_FOV() < 10.0
											FLOAT screen_x
											FLOAT screen_y 
											screen_x = 0.0
											screen_y = 0.0
											GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
										
											IF ABSF(screen_x - 0.5) < 0.1
												IF ABSF(screen_y - 0.5) < 0.1
													
													create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
														
													if not is_ped_injured(gang_enemy[1].ped)
														
														open_sequence_task(seq)
															TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_0", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT)
															task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
														close_sequence_task(seq)
														task_perform_sequence(chop.ped, seq)
														clear_sequence_task(seq)
														
														chop_ai_system_status++
														
													else 
													
														get_chop_to_the_edge_of_the_river()
														
														chop_ai_system_status = 105
													
													endif 
													
													//kills all the dialogue and help text relevant to using chop.
													//clear_help()
													set_label_as_triggered("TM44_chop1", true)
													set_label_as_triggered("chop_help2", true)
													
													chop_time = get_game_timer()
			
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								else 
								
									//reposition chop so he is at an offset from franklin then get ground z for 3d coord. 							
								ENDIF 
							
							break 
							
							case char_trevor
							
								if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(chop.ped)) > 10
									SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
								endif 

								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
										
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

										task_goto_entity_offset_xy(chop.ped, selector_ped.pedID[selector_ped_franklin], -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
									
									endif 
							
								else 
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

										if not is_ped_facing_ped(chop.ped, selector_ped.pedID[selector_ped_franklin], 50.0)
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
												
												float moveSpeed 
												moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(selector_ped.pedID[selector_ped_franklin])
												
												if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
										
													task_turn_ped_to_face_entity(chop.ped, selector_ped.pedID[selector_ped_franklin])	
													
												endif 

											endif 
										endif 
										
									endif 
									
								endif 
							
							break 
							
						endswitch 
						
						if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)
							//chop_audio_system(chop_with_franklin)
						else 
							//chop_audio_system(chop_running)
						endif 
					
					break 
					
					case 102
					
						int total_number_of_points
						vector target_waypoint_pos

						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0)
						endif 

						if not is_ped_injured(gang_enemy[1].ped)
							
							if manage_gang_enemy_1_status = 1
							
								waypoint_recording_get_num_points("exile2_0", total_number_of_points)
								total_number_of_points -= 1
								
								if waypoint_recording_get_coord("exile2_0", total_number_of_points, target_waypoint_pos)  
									if IS_ENTITY_AT_COORD(chop.ped, target_waypoint_pos, <<3.0, 3.0, 3.0>>, false, true)
										//chop_audio_system(chop_finds_enemy)
									else 
										//chop_audio_system(chop_running)
									endif 
								endif 
								
							elif manage_gang_enemy_1_status > 1 
							
								//chop_audio_system(chop_running)
							
								int chop_node
								int gang_enemy_1_node 
								int node_diff 
								
								waypoint_recording_get_closest_waypoint("exile2_0", get_entity_coords(chop.ped), chop_node)
								waypoint_recording_get_closest_waypoint("exile2_0", get_entity_coords(gang_enemy[1].ped), gang_enemy_1_node)
								
								node_diff = chop_node - gang_enemy_1_node
								
								if node_diff >= 0 //switch to new waypoint when enemy infront of chop.

									//*********have extra new case 103 where chop will play a bark anim
		//							open seq
		//							task_turn_ped_to_face_entity
		//							task_play_anim_bark

									open_sequence_task(seq)
										task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)

									//chop_audio_system(chop_finds_enemy, true)
									
									chop_ai_system_status = 103
								
								endif 

							endif 
							
						else 
						
							render_chop_move_help_message()
							
							clear_ped_tasks(chop.ped)

							chop_ai_system_status = 104

						endif 
						
					break 

					case 103
					
						int chop_node
						int gang_enemy_1_node 
						int node_diff 
						
						waypoint_recording_get_closest_waypoint("exile2_1", get_entity_coords(chop.ped), chop_node)
						
						if not is_ped_injured(gang_enemy[1].ped)
						
							waypoint_recording_get_closest_waypoint("exile2_1", get_entity_coords(gang_enemy[1].ped), gang_enemy_1_node)
						
							node_diff = chop_node - gang_enemy_1_node
					
								
							if node_diff < -4
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0)
								endif 
								
								//chop_audio_system(chop_running, true)

								if has_ped_task_finished_2(chop.ped)
									
									open_sequence_task(seq)
										task_follow_waypoint_recording(null, "exile2_1", gang_enemy_1_node, ewaypoint_navmesh_to_initial_waypoint | EWAYPOINT_START_FROM_CLOSEST_POINT)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)
									
								endif 
								
							elif node_diff < -1
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 2.0)
								endif 
								
								//chop_audio_system(chop_finds_enemy)

								if not is_ped_facing_ped(chop.ped, gang_enemy[1].ped, 50.00)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										open_sequence_task(seq)
											task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
										close_sequence_task(seq)
										task_perform_sequence(chop.ped, seq)
										clear_sequence_task(seq)
									endif 
									
								endif 
								
							else 
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 1.0)
								endif 
							
//								if get_script_task_status(chop.ped, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = performing_task
//									clear_ped_tasks(chop.ped)
//								endif 
								
								//chop_audio_system(chop_finds_enemy)
								
								if not is_ped_facing_ped(chop.ped, gang_enemy[1].ped, 50.00)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											
										open_sequence_task(seq)
											task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
										close_sequence_task(seq)
										task_perform_sequence(chop.ped, seq)
										clear_sequence_task(seq)
									endif 
									
								endif 
								
							endif 	
							
						else 
						
							clear_ped_tasks(chop.ped)
						
							render_chop_move_help_message()

							chop_ai_system_status = 104
						
						endif 
			
					break 
					
					case 104
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
				//									script_assert("chop run")
													
													get_chop_to_the_edge_of_the_river()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 

					break 
					
					case 105
					
						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 2.5) //3.0
						endif 
						
						//chop_audio_system(chop_running)
					
						IF VDIST2(get_entity_coords(chop.ped), chop_cutscene_pos) < 0.25  //0.1 speak within Jonathon to make sure the dog has been setup properly.

							cutscene_pos = chop_cutscene_pos
							cutscene_rot = chop_cutscene_rot
							chop_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot) 
							TASK_SYNCHRONIZED_SCENE(chop.ped, chop_cutscene_index, "missexile2", "chop_swim_across", WALK_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
							
							//***********
							
							chop_ai_system_status = 106

						endif
						
					break 
					
					case 106
					
						if IS_SYNCHRONIZED_SCENE_RUNNING(chop_cutscene_index)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop_cutscene_index) >= 1.0
								
								clear_ped_tasks(chop.ped)
							
								chop_ai_system_status++
								
							endif 
						endif 
						
						
					break 
					
					case 107

						if not is_ped_injured(gang_enemy[0].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[0].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 2.5)
							endif 
							
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[0].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 2.5>>)
							//if not is_entity_at_coord(chop.ped, get_entity_coords(gang_enemy[0].ped), <<8.0, 8.0, 8.0>>)
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[0].ped, -1, 8.0, 0.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[0].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)

									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 
									
									if not is_ped_facing_ped(chop.ped, gang_enemy[0].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[0].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif 
								
						else 
						
							clear_ped_tasks(chop.ped)
							
							set_label_as_triggered("TM44_chop4", false)

							render_chop_move_help_message()
							
							chop_ai_system_status = 108
							
						endif 

					break
					
					case 108
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 
					
					break 
					
					case 109
					
						if not is_ped_injured(gang_enemy[2].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[2].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
							endif 
						
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[2].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 4.0>>)
								
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[2].ped, -1, 0.5, 8.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[2].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)
									
									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 

									if not is_ped_facing_ped(chop.ped, gang_enemy[2].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[2].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif 
							
						else 
						
							clear_ped_tasks(chop.ped)
							
							set_label_as_triggered("TM44_chop4", false)

							render_chop_move_help_message()
							
							chop_ai_system_status = 110

						endif 
						
					break 
					
					case 110
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 
					
					break 
						
					case 111
						
						if not is_ped_injured(gang_enemy[3].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[3].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
							endif 
							
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 4.0>>)
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[3].ped, -1, 0.5, 8.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)
									
									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 

									if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif
							
						endif 
					
					break 

					case 200
					
						//before switch set ai system to 200 
					
//						chop_wait_to_be_let_off()
//						
//						scene_pos = << 3558.898, 3677.850, 27.125 >>
//						scene_rot = << 0.000, 0.000, 170.00 >>
//						chop.scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)        
//						TASK_SYNCHRONIZED_SCENE(chop.ped, chop.scene_id, "missfbi5ig_21", "holdup_hallway_scientist", normal_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//					
//					
//						//switch is finished
//						chop_let_off_by_franklin()
//						
//						if is_synchronized_scene_running(chop.scene_id)
//
//							if get_synchronized_scene_phase(chop.scene_id) > 0.9
//						
//								get_chop_to_the_edge_of_the_river()
					
					
						//if has_label_been_triggered("TM44_FSWITCH")
								
							get_chop_to_the_edge_of_the_river()
							
							chop_ai_system_status++

						//endif 

					break 

					case 201

						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0) //3.0
						endif 
						
						//chop_audio_system(chop_running)
					
						IF VDIST2(get_entity_coords(chop.ped), chop_cutscene_pos) < 0.25  //0.1 speak within Jonathon to make sure the dog has been setup properly.
							
							cutscene_pos = chop_cutscene_pos
							cutscene_rot = chop_cutscene_rot
							chop_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot) 
							TASK_SYNCHRONIZED_SCENE(chop.ped, chop_cutscene_index, "missexile2", "chop_swim_across", WALK_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, rbf_none, WALK_BLEND_IN)

							chop_ai_system_status++

						endif
						
					break 

					case 202

						if IS_SYNCHRONIZED_SCENE_RUNNING(chop_cutscene_index)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop_cutscene_index) >= 1.0
							
//								//HACK FIX UNTILL BUG 396054 is fixed
//								chop.pos = get_entity_coords(chop.ped)
//								chop.pos.z += 5.0
//								get_ground_z_for_3d_coord(chop.pos, chop.pos.z)
//								set_entity_coords(chop.ped, chop.pos) 
								
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1559.7913, 4417.9468, 3.8546>>, pedmove_run, -1, -1, enav_no_stopping)
									task_follow_nav_mesh_to_coord(null, <<-1541.6936, 4476.7192, 17.7150>>, pedmove_run, -1)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
							
								chop_ai_system_status++
								
							endif 
						endif 

					break 
					
					case 203
					
						SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0) //2.6
					
						if not is_entity_at_coord(chop.ped, <<-1541.6936, 4476.7192, 17.7150>>, <<1.5, 1.5, 2.0>>)
							
							if has_ped_task_finished_2(chop.ped)
							
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1541.6936, 4476.7192, 17.7150>>, pedmove_run, -1)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
								
							endif 
							
						else 
						
							if not is_ped_injured(gang_enemy[3].ped)
			
								if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 40.0)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)	

									endif 
								
								else 
								
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_play_anim)
								
										if lk_timer(chop_time, 5000)
											if not is_entity_playing_anim(chop.ped, "missexile2", "fra_0_ig_1_chop_growl")
												task_play_anim(chop.ped, "missexile2", "fra_0_ig_1_chop_growl", normal_BLEND_IN, normal_BLEND_OUT)
												chop_time = get_game_timer()
											endif 
										endif 
											
									endif 

								endif 
								
								//make chop run after gang enemy 3 when he runs.
								if manage_gang_enemy_3_status >= 21
									chop_ai_system_status++
								endif 
	
								
							else 
							
								chop_ai_system_status = 300
							
							endif 

						endif 
						
					break
					
					//---
					
					case 204

						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<-1551.9299, 4484.7402, 18.5944>>, pedmove_sprint, -1, 0.2)
						close_sequence_task(seq)
						task_perform_sequence(chop.ped, seq)
						clear_sequence_task(seq)
						
						chop_ai_system_status++
						
		
						if is_ped_injured(gang_enemy[3].ped)
							chop_ai_system_status = 300
						endif 
					
					break 
					
					case 205
					
						if is_entity_at_coord(chop.ped, <<-1551.9299, 4484.7402, 18.5944>>, <<2.5, 2.5, 2.0>>)
							
							if not is_ped_injured(gang_enemy[3].ped)
								TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 0.1, pedmove_sprint, 0.0, EGOTO_ENTITY_NEVER_SLOW_FOR_PATH_LENGTH)
								chop_ai_system_status++
							else 
								chop_ai_system_status = 300
							endif 
			
						endif 

					break 
					
					case 206

						if not is_ped_injured(gang_enemy[3].ped)
						
							request_anim_dict("CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@")
						
//							if not is_ped_ragdoll(gang_enemy[3].ped)
//							and not is_ped_in_combat(gang_enemy[3].ped)
							if not rpg_enemy_attack_task_given 

								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<2.5, 2.5, 5.0>>) //-4.0
									
									//keeps chop chasing after the rpg enemy
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GO_TO_ENTITY) 
									and ((not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack"))
									     or (is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") and get_entity_anim_current_time(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") > 0.7)) 

										TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 0.1, pedmove_sprint, 0.0, EGOTO_ENTITY_NEVER_SLOW_FOR_PATH_LENGTH)
										
										force_ped_motion_state(chop.ped, ms_on_foot_sprint)
										
										chop_time = get_game_timer()

									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								else 
									
									//chop jumps up to bite but does not take down enemy
									
									if has_anim_dict_loaded("CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@")
										if not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") 
											task_play_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack", slow_BLEND_IN, slow_BLEND_OUT)
										endif 
									endif 
								
								endif
								
							else 
							
								printstring("chop 1")
								printnl()
							
								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<9.0, 9.0, 5.0>>) //-4.0
								
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GO_TO_ENTITY)
									and not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") 
									and not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01")
									
										TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 1.0, pedmove_sprint) //we want chop to slow down here 
									
									endif 
									
								else
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<1.0, 1.0, 5.0>>) //-4.0

										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY)
						
											task_follow_to_offset_of_entity(chop.ped, gang_enemy[3].ped, <<-1.0, 2.0, 0.0>>, pedmove_sprint, -1, 0.1) 
											
										endif 
									
									else
									
										if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 40.0)
											
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											and (not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01"))
											
												task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)
												
											endif 

										else 
										
											request_anim_dict("CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@")
										
											if has_anim_dict_loaded("CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@")

												//if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_play_anim)
												if lk_timer(chop_time, 3500)
													if not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01")
														task_play_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01", normal_BLEND_IN, normal_BLEND_OUT)
														chop_time = get_game_timer()
													endif 
												endif 
													
											endif 

										endif 
										
									endif
									
								endif 
							
							endif
							
						else 
							chop_ai_system_status = 300
						endif 
						
					break 
					
					//--
					
					case 300
					
						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) > 10
							SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
						endif 

						if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
								
							if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

								task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
							
							endif 
					
						else 
						
							if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

								if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										float moveSpeed 
										moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(player_ped_id())
										
										if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
								
											task_turn_ped_to_face_entity(chop.ped, player_ped_id())	
											
										endif 

									endif 
								endif 
								
							endif 
							
						endif 

					break 
					
					case 400

						if is_entity_at_coord(helicopter.veh, pickup_pos[closest_warp_pos_to_franklin], <<20.00, 20.00, 5.5>>) //<<10.00, 10.00, 5.00>>)
						and IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh)
						and get_entity_speed(helicopter.veh) < 0.2

							chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
							chop_anim_rot = GET_ANIM_INITIAL_OFFSET_rotation("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)

							task_follow_nav_mesh_to_coord(chop.ped, chop.run_to_pos, pedmove_run, 60000, 0.15, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION, chop_anim_rot.z)
								
							chop_ai_system_status++
							
						else 
						
							//reset_chop_getting_into_helicopter()
						
						endif 
						
						if get_distance_between_coords(get_entity_coords(chop.ped), get_entity_coords(helicopter.veh)) > 45.00
	
							if not is_ped_in_vehicle(selector_ped.pedID[selector_ped_franklin], helicopter.veh, true)
							
								if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(chop.ped)) > 10
									SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
								endif 
								
								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
										
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

										task_goto_entity_offset_xy(chop.ped, selector_ped.pedID[selector_ped_franklin], -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
									
									endif 
							
								else 
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

										if not is_ped_facing_ped(chop.ped, selector_ped.pedID[selector_ped_franklin], 50.0)
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
												
												float moveSpeed 
												moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(selector_ped.pedID[selector_ped_franklin])
												
												if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
										
													task_turn_ped_to_face_entity(chop.ped, selector_ped.pedID[selector_ped_franklin])	
													
												endif 

											endif 
										endif 
										
									endif 
									
								endif 
								
							else 
							
								//walk to original position
								///task turn ped to face entity. 
							
							endif 
							
						endif 
					
					break 
					
					case 401

						if IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh)
						and get_entity_speed(helicopter.veh) < 0.2
						
							chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
							
//							printstring("chop.run_to_pos")
//							printvector(chop.run_to_pos)
//							printnl()
						
							if not is_entity_at_coord(chop.ped, chop.run_to_pos, <<0.3, 0.3, 2.0>>)//<<0.1, 0.1, 2.0>>)
	
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								
									if lk_timer(chop_enter_chopper_time, 10000) //saftey fic for bug 1553555
									
										chop_enter_chopper_time = get_game_timer()
									
										chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
										chop_anim_rot = GET_ANIM_INITIAL_OFFSET_rotation("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)

										task_follow_nav_mesh_to_coord(chop.ped, chop.run_to_pos, pedmove_run, 60000, 0.15, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION, chop_anim_rot.z)
										
										chop_enter_chopper_time = get_game_timer()
										
									endif 
								
								endif 
								
							else 
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								
									cutscene_pos = <<0.0, 0.0, 0.0>> //GET_ENTITY_COORDS(army_truck.veh)
									cutscene_rot = <<0.0, 0.0, 0.0>> //GET_ENTITY_ROTATION(army_truck.veh)
		
									chop.scene_id = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
									//SYNCED_SCENE_DONT_INTERRUPT
									task_synchronized_scene(chop.ped, chop.scene_id, "missexile2", "chop_get_in_frogger_rds", normal_blend_in, normal_blend_out, SYNCED_SCENE_USE_PHYSICS, rbf_none, normal_blend_in)	
							
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(chop.scene_id, helicopter.veh, 0)
									
									//SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(helicopter.veh, false)

									chop_ai_system_status++
								endif 
								
							endif 
							
						else 
						
							if not has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								clear_ped_tasks(chop.ped)
							endif 

						endif 
					
					break 
					
					case 402
					
						//block vehicle crontols here
						//accelerate
						//hover rotation
						//left analogue stick
					
						if IS_SYNCHRONIZED_SCENE_RUNNING(chop.scene_id)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop.scene_id) >= 0.99
							
								clear_ped_tasks(chop.ped)
								//detach_SYNCHRONIZED_SCENE_TO_ENTITY()
								
								attach_entity_to_entity(chop.ped, helicopter.veh, GET_ENTITY_BONE_INDEX_BY_NAME(helicopter.veh, "seat_dside_r"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								task_play_anim(chop.ped, "missexile2", "Chop_Sit_In_Frogger_RDS", normal_blend_in, normal_blend_out, -1, af_looping) 
								force_ped_ai_and_animation_update(chop.ped)
								
								chop_ai_system_status++
								
							endif 
						
						else 
						
							//if the snyched scene fails - speak with Graeme Sales about putting on non interuptable flag
							chop_ai_system_status = 401
						
						endif 

					break
					
					case 403
					
					break 

				endswitch 
			
			break 
			
			case CHOP_BEHAVIOUR_medium
			
				switch chop_ai_system_status
		
						
					case 0
					
						fORCE_PED_MOTION_STATE(chop.ped, MS_ON_FOOT_run, TRUE)
					
						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<-1891.7518, 4432.0229, 43.7444>>, pedmove_run, -1, -1) //<<-1893.8273, 4432.7607, 43.4261>>, pedmove_run, -1, -1)
							task_turn_ped_to_face_entity(null, player_ped_id())
						close_sequence_task(seq)
						task_perform_sequence(chop.ped, seq)
						clear_sequence_task(seq)

						//chop_audio_system(chop_running)
					
						chop_ai_system_status++
					
					break 
					
					case 1
					
						if not is_entity_at_coord(player_ped_id(), <<-1895.8339, 4431.2158, 43.2800>>, <<4.0, 4.0, 2.0>>)
						or not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
							open_sequence_task(seq)
								task_follow_nav_mesh_to_coord(null, <<-1878.6715, 4479.3267, 25.4878>>, pedmove_sprint, -1, 0.5, ENAV_STOP_EXACTLY)
							close_sequence_task(seq)
							task_perform_sequence(chop.ped, seq)
							clear_sequence_task(seq)
						
							//chop_audio_system(chop_bark_button, true)
							
							chop_ai_system_status++
						
						endif 
		
					break 
					
					case 2
					
						request_anim_dict("missexile2")
						
						if not is_entity_at_coord(chop.ped, <<-1878.6715, 4479.3267, 25.4878>>, <<4.0, 4.0, 2.0>>)
							if has_ped_task_finished_2(chop.ped)
								
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1878.6715, 4479.3267, 25.4878>>, pedmove_run, -1, 0.5, ENAV_STOP_EXACTLY)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
								
							endif 
							
							//chop_audio_system(chop_running)
							
						else 
						
							if has_anim_dict_loaded("missexile2")

								if has_ped_task_finished_2(chop.ped)
							
									open_sequence_task(seq)					
										task_play_anim(null, "missexile2", "fra0_ig_14_chop_sniff_fwds", normal_blend_in, normal_blend_out, -1)  
										task_play_anim(null, "missexile2", "fra0_ig_12_chop_waiting_a", normal_blend_in, normal_blend_out, -1, af_looping)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)
									
									chop_ai_system_status++
									
								endif 
								
							endif 

							//chop_audio_system(chop_finds_enemy)

						endif 

					break 
					
					case 3
					
					
					break 
					
					case 101

						switch get_current_player_ped_enum() 
						
							case char_franklin

								if chop_cam.state != chop_view_cam_on_chop

									if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) > 10
										SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
									endif 

									if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
											
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

											task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
										
										endif 
								
									else 
									
										if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

											if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
												if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
													
													float moveSpeed 
													moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(player_ped_id())
													
													if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
											
														task_turn_ped_to_face_entity(chop.ped, player_ped_id())	
														
													endif 

												endif 
											endif 
											
										endif 
										
									endif 
								
								else 
								
									if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 5.0, 0.0>>), <<2.5, 2.5, 2.5>>)
										
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

											task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, 0.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
										
										endif 
								
									else 
									
										if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

											if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
												if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											
													task_turn_ped_to_face_entity(chop.ped, player_ped_id())		

												endif 
											endif 
											
										endif 
										
									endif 
									
								
								endif 
								
							break 
							
							case char_michael
							
								//you are MICHAEL and not fake chop
								if chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
							
									IF IS_ENTITY_ON_SCREEN(chop.ped) 
										IF GET_GAMEPLAY_CAM_FOV() < 10.0
											FLOAT screen_x
											FLOAT screen_y 
											screen_x = 0.0
											screen_y = 0.0
											GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
										
											IF ABSF(screen_x - 0.5) < 0.1
												IF ABSF(screen_y - 0.5) < 0.1
													
													create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
														
													if not is_ped_injured(gang_enemy[1].ped)
														
														open_sequence_task(seq)
															TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_0", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT)
															task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
														close_sequence_task(seq)
														task_perform_sequence(chop.ped, seq)
														clear_sequence_task(seq)
														
														chop_ai_system_status++
														
													else 
													
														get_chop_to_the_edge_of_the_river()
														
														chop_ai_system_status = 105
													
													endif 
													
													//kills all the dialogue and help text relevant to using chop.
													//clear_help()
													set_label_as_triggered("TM44_chop1", true)
													set_label_as_triggered("chop_help2", true)
													
													chop_time = get_game_timer()
			
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								else 
								
									//reposition chop so he is at an offset from franklin then get ground z for 3d coord. 							
								ENDIF 
							
							break 
							
							case char_trevor
							
								if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(chop.ped)) > 10
									SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
								endif 

								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
										
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

										task_goto_entity_offset_xy(chop.ped, selector_ped.pedID[selector_ped_franklin], -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
									
									endif 
							
								else 
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

										if not is_ped_facing_ped(chop.ped, selector_ped.pedID[selector_ped_franklin], 50.0)
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
												
												float moveSpeed 
												moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(selector_ped.pedID[selector_ped_franklin])
												
												if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
										
													task_turn_ped_to_face_entity(chop.ped, selector_ped.pedID[selector_ped_franklin])	
													
												endif 

											endif 
										endif 
										
									endif 
									
								endif 
							
							break 
							
						endswitch 
						
						if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)
							//chop_audio_system(chop_with_franklin)
						else 
							//chop_audio_system(chop_running)
						endif 
					
					break 
					
					case 102
					
						int total_number_of_points
						vector target_waypoint_pos

						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0)
						endif 

						if not is_ped_injured(gang_enemy[1].ped)
							
							if manage_gang_enemy_1_status = 1
							
								waypoint_recording_get_num_points("exile2_0", total_number_of_points)
								total_number_of_points -= 1
								
								if waypoint_recording_get_coord("exile2_0", total_number_of_points, target_waypoint_pos)  
									if IS_ENTITY_AT_COORD(chop.ped, target_waypoint_pos, <<3.0, 3.0, 3.0>>, false, true)
										//chop_audio_system(chop_finds_enemy)
									else 
										//chop_audio_system(chop_running)
									endif 
								endif 
								
							elif manage_gang_enemy_1_status > 1 
							
								//chop_audio_system(chop_running)
							
								int chop_node
								int gang_enemy_1_node 
								int node_diff 
								
								waypoint_recording_get_closest_waypoint("exile2_0", get_entity_coords(chop.ped), chop_node)
								waypoint_recording_get_closest_waypoint("exile2_0", get_entity_coords(gang_enemy[1].ped), gang_enemy_1_node)
								
								node_diff = chop_node - gang_enemy_1_node
								
								if node_diff >= 0 //switch to new waypoint when enemy infront of chop.

									//*********have extra new case 103 where chop will play a bark anim
		//							open seq
		//							task_turn_ped_to_face_entity
		//							task_play_anim_bark

									open_sequence_task(seq)
										task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)

									//chop_audio_system(chop_finds_enemy, true)
									
									chop_ai_system_status = 103
								
								endif 

							endif 
							
						else 
						
							render_chop_move_help_message()
							
							clear_ped_tasks(chop.ped)

							chop_ai_system_status = 104

						endif 
						
					break 

					case 103
					
						int chop_node
						int gang_enemy_1_node 
						int node_diff 
						
						waypoint_recording_get_closest_waypoint("exile2_1", get_entity_coords(chop.ped), chop_node)
						
						if not is_ped_injured(gang_enemy[1].ped)
						
							waypoint_recording_get_closest_waypoint("exile2_1", get_entity_coords(gang_enemy[1].ped), gang_enemy_1_node)
						
							node_diff = chop_node - gang_enemy_1_node
					
								
							if node_diff < -4
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0)
								endif 
								
								//chop_audio_system(chop_running, true)

								if has_ped_task_finished_2(chop.ped)
									
									open_sequence_task(seq)
										task_follow_waypoint_recording(null, "exile2_1", gang_enemy_1_node, ewaypoint_navmesh_to_initial_waypoint | EWAYPOINT_START_FROM_CLOSEST_POINT)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)
									
								endif 
								
							elif node_diff < -1
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 2.0)
								endif 
								
								//chop_audio_system(chop_finds_enemy)

								if not is_ped_facing_ped(chop.ped, gang_enemy[1].ped, 50.00)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										open_sequence_task(seq)
											task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
										close_sequence_task(seq)
										task_perform_sequence(chop.ped, seq)
										clear_sequence_task(seq)
									endif 
									
								endif 
								
							else 
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 1.0)
								endif 
							
//								if get_script_task_status(chop.ped, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = performing_task
//									clear_ped_tasks(chop.ped)
//								endif 
								
								//chop_audio_system(chop_finds_enemy)
								
								if not is_ped_facing_ped(chop.ped, gang_enemy[1].ped, 50.00)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											
										open_sequence_task(seq)
											task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
										close_sequence_task(seq)
										task_perform_sequence(chop.ped, seq)
										clear_sequence_task(seq)
									endif 
									
								endif 
								
							endif 	
							
						else 
						
							clear_ped_tasks(chop.ped)
						
							render_chop_move_help_message()

							chop_ai_system_status = 104
						
						endif 
			
					break 
					
					case 104
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
				//									script_assert("chop run")
													
													get_chop_to_the_edge_of_the_river()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 

					break 
					
					case 105
					
						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 2.5) //3.0
						endif 
						
						//chop_audio_system(chop_running)
					
						IF VDIST2(get_entity_coords(chop.ped), chop_cutscene_pos) < 0.25  //0.1 speak within Jonathon to make sure the dog has been setup properly.
						
							cutscene_pos = chop_cutscene_pos
							cutscene_rot = chop_cutscene_rot
							chop_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot) 
							TASK_SYNCHRONIZED_SCENE(chop.ped, chop_cutscene_index, "missexile2", "chop_swim_across", WALK_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
							
							//***********
							
							chop_ai_system_status = 106

						endif
						
					break 
					
					case 106
					
						if IS_SYNCHRONIZED_SCENE_RUNNING(chop_cutscene_index)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop_cutscene_index) >= 1.0
								
								clear_ped_tasks(chop.ped)
							
								chop_ai_system_status++
								
							endif 
						endif 
						
						//chop_audio_system(chop_running)
						
					break 
					
					case 107

						if not is_ped_injured(gang_enemy[0].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[0].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 2.5)
							endif 
							
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[0].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 2.5>>)
							//if not is_entity_at_coord(chop.ped, get_entity_coords(gang_enemy[0].ped), <<8.0, 8.0, 8.0>>)
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[0].ped, -1, 8.0, 0.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[0].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)

									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 
									
									if not is_ped_facing_ped(chop.ped, gang_enemy[0].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[0].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif 
								
						else 
						
							clear_ped_tasks(chop.ped)
							
							set_label_as_triggered("TM44_chop4", false)

							render_chop_move_help_message()
							
							chop_ai_system_status = 108
							
						endif 

					break
					
					case 108
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 
					
					break 
					
					case 109
					
						if not is_ped_injured(gang_enemy[2].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[2].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
							endif 
						
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[2].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 4.0>>)
								
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[2].ped, -1, 0.5, 8.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[2].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)
									
									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 

									if not is_ped_facing_ped(chop.ped, gang_enemy[2].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[2].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif 
							
						else 
						
							clear_ped_tasks(chop.ped)
							
							set_label_as_triggered("TM44_chop4", false)

							render_chop_move_help_message()
							
							chop_ai_system_status = 110

						endif 
						
					break 
					
					case 110
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 
					
					break 
						
					case 111
						
						if not is_ped_injured(gang_enemy[3].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[3].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
							endif 
							
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 4.0>>)
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[3].ped, -1, 0.5, 8.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)
									
									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 

									if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif
							
						endif 
					
					break 

					case 200
					
						//before switch set ai system to 200 
					
//						chop_wait_to_be_let_off()
//						
//						scene_pos = << 3558.898, 3677.850, 27.125 >>
//						scene_rot = << 0.000, 0.000, 170.00 >>
//						chop.scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)        
//						TASK_SYNCHRONIZED_SCENE(chop.ped, chop.scene_id, "missfbi5ig_21", "holdup_hallway_scientist", normal_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//					
//					
//						//switch is finished
//						chop_let_off_by_franklin()
//						
//						if is_synchronized_scene_running(chop.scene_id)
//
//							if get_synchronized_scene_phase(chop.scene_id) > 0.9
//						
//								get_chop_to_the_edge_of_the_river()
					
					
						//if has_label_been_triggered("TM44_FSWITCH")
								
							get_chop_to_the_edge_of_the_river()
							
							chop_ai_system_status++

						//endif 

					break 

					case 201

						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0) //3.0
						endif 
						
						//chop_audio_system(chop_running)
					
						IF VDIST2(get_entity_coords(chop.ped), chop_cutscene_pos) < 0.25  //0.1 speak within Jonathon to make sure the dog has been setup properly.
							
							cutscene_pos = chop_cutscene_pos
							cutscene_rot = chop_cutscene_rot
							chop_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot) 
							TASK_SYNCHRONIZED_SCENE(chop.ped, chop_cutscene_index, "missexile2", "chop_swim_across", WALK_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, rbf_none, WALK_BLEND_IN)

							chop_ai_system_status++

						endif
						
					break 

					case 202

						if IS_SYNCHRONIZED_SCENE_RUNNING(chop_cutscene_index)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop_cutscene_index) >= 1.0
							
//								//HACK FIX UNTILL BUG 396054 is fixed
//								chop.pos = get_entity_coords(chop.ped)
//								chop.pos.z += 5.0
//								get_ground_z_for_3d_coord(chop.pos, chop.pos.z)
//								set_entity_coords(chop.ped, chop.pos) 
								
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1559.7913, 4417.9468, 3.8546>>, pedmove_run, -1, -1, enav_no_stopping)
									task_follow_nav_mesh_to_coord(null, <<-1541.6936, 4476.7192, 17.7150>>, pedmove_run, -1)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
							
								chop_ai_system_status++
								
							endif 
						endif 

					break 
					
					case 203
					
						SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0) //2.6
					
						if not is_entity_at_coord(chop.ped, <<-1541.6936, 4476.7192, 17.7150>>, <<1.5, 1.5, 2.0>>)
							
							if has_ped_task_finished_2(chop.ped)
							
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1541.6936, 4476.7192, 17.7150>>, pedmove_run, -1)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
								
							endif 
							
						else 
						
							if not is_ped_injured(gang_enemy[3].ped)
			
								if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 40.0)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)	

									endif 
								
								else 
								
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_play_anim)
								
										if lk_timer(chop_time, 5000)
											if not is_entity_playing_anim(chop.ped, "missexile2", "fra_0_ig_1_chop_growl")
												task_play_anim(chop.ped, "missexile2", "fra_0_ig_1_chop_growl", normal_BLEND_IN, normal_BLEND_OUT)
												chop_time = get_game_timer()
											endif 
										endif 
											
									endif 

								endif 
								
								//added back for bug 952195
								//chop_audio_system(chop_finds_enemy)
								
								//make chop run after gang enemy 3 when he runs.
								if manage_gang_enemy_3_status >= 21
									chop_ai_system_status++
								endif 
								
							else 
							
								chop_ai_system_status = 300
							
							endif 

						endif 
						
					break
					
					//----
					
					case 204

						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<-1551.9299, 4484.7402, 18.5944>>, pedmove_sprint, -1, 0.2)
						close_sequence_task(seq)
						task_perform_sequence(chop.ped, seq)
						clear_sequence_task(seq)
						
						chop_ai_system_status++
						
		
						if is_ped_injured(gang_enemy[3].ped)
							chop_ai_system_status = 300
						endif 
					
					break 
					
					case 205
					
						if is_entity_at_coord(chop.ped, <<-1551.9299, 4484.7402, 18.5944>>, <<2.5, 2.5, 2.0>>)
							
							if not is_ped_injured(gang_enemy[3].ped)
								TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 0.1, pedmove_sprint, 0.0, EGOTO_ENTITY_NEVER_SLOW_FOR_PATH_LENGTH)
								chop_ai_system_status++
							else 
								chop_ai_system_status = 300
							endif 
			
						endif 

					break 
					
					case 206
					
						if not is_ped_injured(gang_enemy[3].ped)
						
							request_anim_dict("CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@")
						
//							if not is_ped_ragdoll(gang_enemy[3].ped)
//							and not is_ped_in_combat(gang_enemy[3].ped)
							if not rpg_enemy_attack_task_given 

								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<2.5, 2.5, 5.0>>) //-4.0
									
									//keeps chop chasing after the rpg enemy
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GO_TO_ENTITY) 
									and ((not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack"))
									     or (is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") and get_entity_anim_current_time(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") > 0.7)) 

										TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 0.1, pedmove_sprint, 0.0, EGOTO_ENTITY_NEVER_SLOW_FOR_PATH_LENGTH)
										
										force_ped_motion_state(chop.ped, ms_on_foot_sprint)
										
										chop_time = get_game_timer()

									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								else 
									
									//chop jumps up to bite but does not take down enemy
									
									if has_anim_dict_loaded("CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@")
										if not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") 
											task_play_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack", slow_BLEND_IN, slow_BLEND_OUT)
										endif 
									endif 
								
								endif
								
							else 
							
								printstring("chop 1")
								printnl()
							
								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<9.0, 9.0, 5.0>>) //-4.0
								
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GO_TO_ENTITY)
									and not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") 
									and not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01")
									
										TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 1.0, pedmove_sprint) //we want chop to slow down here 
									
									endif 
									
								else
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<1.0, 1.0, 5.0>>) //-4.0

										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY)
						
											task_follow_to_offset_of_entity(chop.ped, gang_enemy[3].ped, <<-1.0, 2.0, 0.0>>, pedmove_sprint, -1, 0.1) 
											
										endif 
									
									else
									
										if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 40.0)
											
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											and (not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01"))
											
												task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)
												
											endif 

										else 
										
											request_anim_dict("CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@")
										
											if has_anim_dict_loaded("CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@")

												//if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_play_anim)
												if lk_timer(chop_time, 3500)
													if not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01")
														task_play_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01", normal_BLEND_IN, normal_BLEND_OUT)
														chop_time = get_game_timer()
													endif 
												endif 
													
											endif 

										endif 
										
									endif
									
								endif 
							
							endif
							
						else 
							chop_ai_system_status = 300
						endif 
						
					break 
					
					
					//-----
					
					
					case 300
					
						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) > 10
							SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
						endif 

						if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
								
							if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

								task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
							
							endif 
					
						else 
						
							if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

								if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										float moveSpeed 
										moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(player_ped_id())
										
										if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
								
											task_turn_ped_to_face_entity(chop.ped, player_ped_id())	
											
										endif 

									endif 
								endif 
								
							endif 
							
						endif 

					break 
					
					case 400

						if is_entity_at_coord(helicopter.veh, pickup_pos[closest_warp_pos_to_franklin], <<20.00, 20.00, 5.5>>) //<<10.00, 10.00, 5.00>>)
						and IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh)
						and get_entity_speed(helicopter.veh) < 0.2

							chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
							chop_anim_rot = GET_ANIM_INITIAL_OFFSET_rotation("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)

							task_follow_nav_mesh_to_coord(chop.ped, chop.run_to_pos, pedmove_run, 60000, 0.15, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION, chop_anim_rot.z)
								
							chop_ai_system_status++
							
						else 
						
							//reset_chop_getting_into_helicopter()
						
						endif 
						
						if get_distance_between_coords(get_entity_coords(chop.ped), get_entity_coords(helicopter.veh)) > 45.00
	
							if not is_ped_in_vehicle(selector_ped.pedID[selector_ped_franklin], helicopter.veh, true)
							
								if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(chop.ped)) > 10
									SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
								endif 
								
								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
										
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

										task_goto_entity_offset_xy(chop.ped, selector_ped.pedID[selector_ped_franklin], -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
									
									endif 
							
								else 
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

										if not is_ped_facing_ped(chop.ped, selector_ped.pedID[selector_ped_franklin], 50.0)
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
												
												float moveSpeed 
												moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(selector_ped.pedID[selector_ped_franklin])
												
												if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
										
													task_turn_ped_to_face_entity(chop.ped, selector_ped.pedID[selector_ped_franklin])	
													
												endif 

											endif 
										endif 
										
									endif 
									
								endif 
								
							else 
							
								//walk to original position
								///task turn ped to face entity. 
							
							endif 
							
						endif 
					
					break 
					
					case 401

						if IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh)
						and get_entity_speed(helicopter.veh) < 0.2
						
							chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
							
//							printstring("chop.run_to_pos")
//							printvector(chop.run_to_pos)
//							printnl()
						
							if not is_entity_at_coord(chop.ped, chop.run_to_pos, <<0.3, 0.3, 2.0>>)//<<0.1, 0.1, 2.0>>)
	
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								
									chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
									chop_anim_rot = GET_ANIM_INITIAL_OFFSET_rotation("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)

									task_follow_nav_mesh_to_coord(chop.ped, chop.run_to_pos, pedmove_run, 60000, 0.15, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION, chop_anim_rot.z)
								
								endif 
								
							else 
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								
									cutscene_pos = <<0.0, 0.0, 0.0>> //GET_ENTITY_COORDS(army_truck.veh)
									cutscene_rot = <<0.0, 0.0, 0.0>> //GET_ENTITY_ROTATION(army_truck.veh)
		
									chop.scene_id = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
									//SYNCED_SCENE_DONT_INTERRUPT
									task_synchronized_scene(chop.ped, chop.scene_id, "missexile2", "chop_get_in_frogger_rds", normal_blend_in, normal_blend_out, SYNCED_SCENE_USE_PHYSICS, rbf_none, normal_blend_in)	
							
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(chop.scene_id, helicopter.veh, 0)
									
									//SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(helicopter.veh, false)

									chop_ai_system_status++
								endif 
								
							endif 
							
						else 
						
							if not has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								clear_ped_tasks(chop.ped)
							endif 

						endif 
					
					break 
					
					case 402
					
						//block vehicle crontols here
						//accelerate
						//hover rotation
						//left analogue stick
					
						if IS_SYNCHRONIZED_SCENE_RUNNING(chop.scene_id)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop.scene_id) >= 0.99
							
								clear_ped_tasks(chop.ped)
								//detach_SYNCHRONIZED_SCENE_TO_ENTITY()
								
								attach_entity_to_entity(chop.ped, helicopter.veh, GET_ENTITY_BONE_INDEX_BY_NAME(helicopter.veh, "seat_dside_r"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								task_play_anim(chop.ped, "missexile2", "Chop_Sit_In_Frogger_RDS", normal_blend_in, normal_blend_out, -1, af_looping) 
								force_ped_ai_and_animation_update(chop.ped)
								
								chop_ai_system_status++
								
							endif 
						
						else 
						
							//if the snyched scene fails - speak with Graeme Sales about putting on non interuptable flag
							chop_ai_system_status = 401
						
						endif 

					break
					
					case 403
					
					break 

				endswitch 
			
			break 
			
			case CHOP_BEHAVIOUR_good
			
				switch chop_ai_system_status
		
					case 0
					
						fORCE_PED_MOTION_STATE(chop.ped, MS_ON_FOOT_run, TRUE)
					
						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<-1891.7518, 4432.0229, 43.7444>>, pedmove_run, -1, -1) //<<-1893.8273, 4432.7607, 43.4261>>, pedmove_run, -1, -1)
							task_turn_ped_to_face_entity(null, player_ped_id())
						close_sequence_task(seq)
						task_perform_sequence(chop.ped, seq)
						clear_sequence_task(seq)

						//chop_audio_system(chop_running)
					
						chop_ai_system_status++
					
					break 
					
					case 1
					
						if not is_entity_at_coord(player_ped_id(), <<-1895.8339, 4431.2158, 43.2800>>, <<4.0, 4.0, 2.0>>)
						or not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
							open_sequence_task(seq)
								task_follow_nav_mesh_to_coord(null, <<-1878.6715, 4479.3267, 25.4878>>, pedmove_sprint, -1, 0.5, ENAV_STOP_EXACTLY)
							close_sequence_task(seq)
							task_perform_sequence(chop.ped, seq)
							clear_sequence_task(seq)
						
							//chop_audio_system(chop_bark_button, true)
							
							chop_ai_system_status++
						
						endif 
		
					break 
					
					case 2
					
						request_anim_dict("missexile2")
						
						if not is_entity_at_coord(chop.ped, <<-1878.6715, 4479.3267, 25.4878>>, <<4.0, 4.0, 2.0>>)
							if has_ped_task_finished_2(chop.ped)
								
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1878.6715, 4479.3267, 25.4878>>, pedmove_run, -1, 0.5, ENAV_STOP_EXACTLY)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
								
							endif 
							
							//chop_audio_system(chop_running)
							
						else 
						
							if has_anim_dict_loaded("missexile2")

								if has_ped_task_finished_2(chop.ped)
							
									open_sequence_task(seq)					
										task_play_anim(null, "missexile2", "fra0_ig_14_chop_sniff_fwds", normal_blend_in, normal_blend_out, -1)  
										task_play_anim(null, "missexile2", "fra0_ig_12_chop_waiting_a", normal_blend_in, normal_blend_out, -1, af_looping)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)
									
									chop_ai_system_status++
									
								endif 
								
							endif 

							//chop_audio_system(chop_finds_enemy)

						endif 

					break 
					
					case 3
					
					
					break 
					
					case 101

						switch get_current_player_ped_enum() 
						
							case char_franklin

								if chop_cam.state != chop_view_cam_on_chop

									if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) > 10
										SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
									endif 

									if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
											
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

											task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
										
										endif 
								
									else 
									
										if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

											if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
												if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
													
													float moveSpeed 
													moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(player_ped_id())
													
													if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
											
														task_turn_ped_to_face_entity(chop.ped, player_ped_id())	
														
													endif 

												endif 
											endif 
											
										endif 
										
									endif 
								
								else 
								
									if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 5.0, 0.0>>), <<2.5, 2.5, 2.5>>)
										
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

											task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, 0.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
										
										endif 
								
									else 
									
										if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

											if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
												if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											
													task_turn_ped_to_face_entity(chop.ped, player_ped_id())		

												endif 
											endif 
											
										endif 
										
									endif 
									
								
								endif 
								
							break 
							
							case char_michael
							
								//you are MICHAEL and not fake chop
								if chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
							
									IF IS_ENTITY_ON_SCREEN(chop.ped) 
										IF GET_GAMEPLAY_CAM_FOV() < 10.0
											FLOAT screen_x
											FLOAT screen_y 
											screen_x = 0.0
											screen_y = 0.0
											GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
										
											IF ABSF(screen_x - 0.5) < 0.1
												IF ABSF(screen_y - 0.5) < 0.1
													
													create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
														
													if not is_ped_injured(gang_enemy[1].ped)
														
														open_sequence_task(seq)
															TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_0", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT)
															task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
														close_sequence_task(seq)
														task_perform_sequence(chop.ped, seq)
														clear_sequence_task(seq)
														
														chop_ai_system_status++
														
													else 
													
														get_chop_to_the_edge_of_the_river()
														
														chop_ai_system_status = 105
													
													endif 
													
													//kills all the dialogue and help text relevant to using chop.
													//clear_help()
													set_label_as_triggered("TM44_chop1", true)
													set_label_as_triggered("chop_help2", true)
													
													chop_time = get_game_timer()
			
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								else 
								
									//reposition chop so he is at an offset from franklin then get ground z for 3d coord. 							
								ENDIF 
							
							break 
							
							case char_trevor
							
								if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(chop.ped)) > 10
									SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
								endif 

								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
										
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

										task_goto_entity_offset_xy(chop.ped, selector_ped.pedID[selector_ped_franklin], -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
									
									endif 
							
								else 
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

										if not is_ped_facing_ped(chop.ped, selector_ped.pedID[selector_ped_franklin], 50.0)
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
												
												float moveSpeed 
												moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(selector_ped.pedID[selector_ped_franklin])
												
												if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
										
													task_turn_ped_to_face_entity(chop.ped, selector_ped.pedID[selector_ped_franklin])	
													
												endif 

											endif 
										endif 
										
									endif 
									
								endif 
							
							break 
							
						endswitch 
						
						if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)
							//chop_audio_system(chop_with_franklin)
						else 
							//chop_audio_system(chop_running)
						endif 
					
					break 
					
					case 102
					
						int total_number_of_points
						vector target_waypoint_pos

						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0)
						endif 

						if not is_ped_injured(gang_enemy[1].ped)
							
							if manage_gang_enemy_1_status = 1
							
								waypoint_recording_get_num_points("exile2_0", total_number_of_points)
								total_number_of_points -= 1
								
								if waypoint_recording_get_coord("exile2_0", total_number_of_points, target_waypoint_pos)  
									if IS_ENTITY_AT_COORD(chop.ped, target_waypoint_pos, <<3.0, 3.0, 3.0>>, false, true)
										//chop_audio_system(chop_finds_enemy)
									else 
										//chop_audio_system(chop_running)
									endif 
								endif 
								
							elif manage_gang_enemy_1_status > 1 
							
								//chop_audio_system(chop_running)
							
								int chop_node
								int gang_enemy_1_node 
								int node_diff 
								
								waypoint_recording_get_closest_waypoint("exile2_0", get_entity_coords(chop.ped), chop_node)
								waypoint_recording_get_closest_waypoint("exile2_0", get_entity_coords(gang_enemy[1].ped), gang_enemy_1_node)
								
								node_diff = chop_node - gang_enemy_1_node
								
								if node_diff >= 0 //switch to new waypoint when enemy infront of chop.

									//*********have extra new case 103 where chop will play a bark anim
		//							open seq
		//							task_turn_ped_to_face_entity
		//							task_play_anim_bark

									open_sequence_task(seq)
										task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)

									//chop_audio_system(chop_finds_enemy, true)
									
									chop_ai_system_status = 103
								
								endif 

							endif 
							
						else 
						
							render_chop_move_help_message()
							
							clear_ped_tasks(chop.ped)

							chop_ai_system_status = 104

						endif 
						
					break 

					case 103
					
						int chop_node
						int gang_enemy_1_node 
						int node_diff 
						
						waypoint_recording_get_closest_waypoint("exile2_1", get_entity_coords(chop.ped), chop_node)
						
						if not is_ped_injured(gang_enemy[1].ped)
						
							waypoint_recording_get_closest_waypoint("exile2_1", get_entity_coords(gang_enemy[1].ped), gang_enemy_1_node)
						
							node_diff = chop_node - gang_enemy_1_node
					
								
							if node_diff < -4
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0)
								endif 
								
								//chop_audio_system(chop_running, true)

								if has_ped_task_finished_2(chop.ped)
									
									open_sequence_task(seq)
										task_follow_waypoint_recording(null, "exile2_1", gang_enemy_1_node, ewaypoint_navmesh_to_initial_waypoint | EWAYPOINT_START_FROM_CLOSEST_POINT)
									close_sequence_task(seq)
									task_perform_sequence(chop.ped, seq)
									clear_sequence_task(seq)
									
								endif 
								
							elif node_diff < -1
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 2.0)
								endif 
								
								//chop_audio_system(chop_finds_enemy)

								if not is_ped_facing_ped(chop.ped, gang_enemy[1].ped, 50.00)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										open_sequence_task(seq)
											task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
										close_sequence_task(seq)
										task_perform_sequence(chop.ped, seq)
										clear_sequence_task(seq)
									endif 
									
								endif 
								
							else 
							
								if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 1.0)
								endif 
							
//								if get_script_task_status(chop.ped, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = performing_task
//									clear_ped_tasks(chop.ped)
//								endif 
								
								//chop_audio_system(chop_finds_enemy)
								
								if not is_ped_facing_ped(chop.ped, gang_enemy[1].ped, 50.00)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											
										open_sequence_task(seq)
											task_turn_ped_to_face_entity(null, gang_enemy[1].ped)
										close_sequence_task(seq)
										task_perform_sequence(chop.ped, seq)
										clear_sequence_task(seq)
									endif 
									
								endif 
								
							endif 	
							
						else 
						
							clear_ped_tasks(chop.ped)
						
							render_chop_move_help_message()

							chop_ai_system_status = 104
						
						endif 
			
					break 
					
					case 104
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
				//									script_assert("chop run")
													
													get_chop_to_the_edge_of_the_river()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 

					break 
					
					case 105
					
						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 2.5) //3.0
						endif 
						
						//chop_audio_system(chop_running)
					
						IF VDIST2(get_entity_coords(chop.ped), chop_cutscene_pos) < 0.25  //0.1 speak within Jonathon to make sure the dog has been setup properly.
						
							//*****
							cutscene_pos = chop_cutscene_pos
							cutscene_rot = chop_cutscene_rot
							chop_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot) 
							TASK_SYNCHRONIZED_SCENE(chop.ped, chop_cutscene_index, "missexile2", "chop_swim_across", WALK_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
							
							//***********
							
							chop_ai_system_status = 106

						endif
						
					break 
					
					case 106
					
						if IS_SYNCHRONIZED_SCENE_RUNNING(chop_cutscene_index)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop_cutscene_index) >= 1.0
								
								clear_ped_tasks(chop.ped)
							
								chop_ai_system_status++
								
							endif 
						endif 
						
						//chop_audio_system(chop_running)
						
					break 
					
					case 107

						if not is_ped_injured(gang_enemy[0].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[0].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 2.5)
							endif 
							
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[0].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 2.5>>)
							//if not is_entity_at_coord(chop.ped, get_entity_coords(gang_enemy[0].ped), <<8.0, 8.0, 8.0>>)
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[0].ped, -1, 8.0, 0.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[0].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)

									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 
									
									if not is_ped_facing_ped(chop.ped, gang_enemy[0].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[0].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif 
								
						else 
						
							clear_ped_tasks(chop.ped)
							
							set_label_as_triggered("TM44_chop4", false)

							render_chop_move_help_message()
							
							chop_ai_system_status = 108
							
						endif 

					break
					
					case 108
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 
					
					break 
					
					case 109
					
						if not is_ped_injured(gang_enemy[2].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[2].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
							endif 
						
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[2].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 4.0>>)
								
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[2].ped, -1, 0.5, 8.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[2].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)
									
									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 

									if not is_ped_facing_ped(chop.ped, gang_enemy[2].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[2].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif 
							
						else 
						
							clear_ped_tasks(chop.ped)
							
							set_label_as_triggered("TM44_chop4", false)

							render_chop_move_help_message()
							
							chop_ai_system_status = 110

						endif 
						
					break 
					
					case 110
					
						switch get_current_player_ped_enum()
					
							case char_michael
							
								IF IS_ENTITY_ON_SCREEN(chop.ped) 
									IF GET_GAMEPLAY_CAM_FOV() < 10.0
										FLOAT screen_x
										FLOAT screen_y 
										screen_x = 0.0
										screen_y = 0.0
										GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(chop.ped), screen_x, screen_y)
									
										IF ABSF(screen_x - 0.5) < 0.1
											IF ABSF(screen_y - 0.5) < 0.1
												if create_conversation(scripted_speech, "TM44AUD", "TM44_chop2", CONV_PRIORITY_MEDIUM)
													
													chop_time = get_game_timer()
													
													clear_help()
													
													chop_ai_system_status++
												endif 
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							break 
							
						endswitch 
					
					break 
						
					case 111
						
						if not is_ped_injured(gang_enemy[3].ped)
						
							if get_distance_between_coords(get_entity_coords(gang_enemy[3].ped), get_entity_coords(chop.ped)) > 12
								SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
							endif 
							
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<8.0, 0.0, 0.0>>), <<2.5, 2.5, 4.0>>)
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

									task_goto_entity_offset_xy(chop.ped, gang_enemy[3].ped, -1, 0.5, 8.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
								
								endif 
								
								//chop_audio_system(chop_running)
						
							else 
							
								if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<8.0, 0.0, 0.0>>), <<1.8, 1.8, 4.0>>)
									
									if not has_label_been_triggered("TM44_chop4")
										if create_conversation(scripted_speech, "TM44AUD", "TM44_chop4", conv_priority_medium)
											set_label_as_triggered("TM44_chop4", true)
										endif 
									endif 

									if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 50.0)
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									
											task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)		

										endif 
									endif 
									
									//chop_audio_system(chop_finds_enemy)
									
								endif 
								
							endif
							
						endif 
					
					break 

					case 200
					
						//before switch set ai system to 200 
					
//						chop_wait_to_be_let_off()
//						
//						scene_pos = << 3558.898, 3677.850, 27.125 >>
//						scene_rot = << 0.000, 0.000, 170.00 >>
//						chop.scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)        
//						TASK_SYNCHRONIZED_SCENE(chop.ped, chop.scene_id, "missfbi5ig_21", "holdup_hallway_scientist", normal_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//					
//					
//						//switch is finished
//						chop_let_off_by_franklin()
//						
//						if is_synchronized_scene_running(chop.scene_id)
//
//							if get_synchronized_scene_phase(chop.scene_id) > 0.9
//						
//								get_chop_to_the_edge_of_the_river()
					
					
						//if has_label_been_triggered("TM44_FSWITCH")
								
							get_chop_to_the_edge_of_the_river()
							
							chop_ai_system_status++

						//endif 

					break 

					case 201

						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0) //3.0
						endif 
						
						//chop_audio_system(chop_running)
					
						IF VDIST2(get_entity_coords(chop.ped), chop_cutscene_pos) < 0.25  //0.1 speak within Jonathon to make sure the dog has been setup properly.
							
							cutscene_pos = chop_cutscene_pos
							cutscene_rot = chop_cutscene_rot
							chop_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot) 
							TASK_SYNCHRONIZED_SCENE(chop.ped, chop_cutscene_index, "missexile2", "chop_swim_across", WALK_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, rbf_none, WALK_BLEND_IN)

							chop_ai_system_status++

						endif
						
					break 

					case 202

						if IS_SYNCHRONIZED_SCENE_RUNNING(chop_cutscene_index)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop_cutscene_index) >= 1.0
							
//								//HACK FIX UNTILL BUG 396054 is fixed
//								chop.pos = get_entity_coords(chop.ped)
//								chop.pos.z += 5.0
//								get_ground_z_for_3d_coord(chop.pos, chop.pos.z)
//								set_entity_coords(chop.ped, chop.pos) 
								
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1559.7913, 4417.9468, 3.8546>>, pedmove_run, -1, -1, enav_no_stopping)
									task_follow_nav_mesh_to_coord(null, <<-1541.6936, 4476.7192, 17.7150>>, pedmove_run, -1)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
							
								chop_ai_system_status++
								
							endif 
						endif 

					break 
					
					case 203
					
						SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0) //2.6
					
						if not is_entity_at_coord(chop.ped, <<-1541.6936, 4476.7192, 17.7150>>, <<1.5, 1.5, 2.0>>)
							
							if has_ped_task_finished_2(chop.ped)
							
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, <<-1541.6936, 4476.7192, 17.7150>>, pedmove_run, -1)
								close_sequence_task(seq)
								task_perform_sequence(chop.ped, seq)
								clear_sequence_task(seq)
								
							endif 
							
						else 
						
							if not is_ped_injured(gang_enemy[3].ped)
			
								if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 40.0)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)	

									endif 
								
								else 
								
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_play_anim)
								
										if lk_timer(chop_time, 5000)
											if not is_entity_playing_anim(chop.ped, "missexile2", "fra_0_ig_1_chop_growl")
												task_play_anim(chop.ped, "missexile2", "fra_0_ig_1_chop_growl", normal_BLEND_IN, normal_BLEND_OUT)
												chop_time = get_game_timer()
											endif 
										endif 
											
									endif 

								endif 
								
								//added back for bug 952195
								//chop_audio_system(chop_finds_enemy)
								
								//make chop run after gang enemy 3 when he runs.
								if manage_gang_enemy_3_status >= 21
									chop_ai_system_status++
								endif 
								
							else 
							
								chop_ai_system_status = 300
							
							endif 

						endif 
						
					break
					
					case 204

						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<-1551.9299, 4484.7402, 18.5944>>, pedmove_sprint, -1, 0.2)
						close_sequence_task(seq)
						task_perform_sequence(chop.ped, seq)
						clear_sequence_task(seq)
						
						chop_ai_system_status++
						
		
						if is_ped_injured(gang_enemy[3].ped)
							chop_ai_system_status = 300
						endif 
					
					break 
					
					case 205
					
						if is_entity_at_coord(chop.ped, <<-1551.9299, 4484.7402, 18.5944>>, <<2.5, 2.5, 2.0>>)
							
							if not is_ped_injured(gang_enemy[3].ped)
								//task_goto_entity_offset_xy(chop.ped, gang_enemy[3].ped, -1, 4.0, 0.0, 0.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY) //8.0, 0.0
								//task_follow_to_offset_of_entity(chop.ped, gang_enemy[3].ped, <<0.0, 0.0, 0.0>>, pedmove_sprint, -1, 4.0) 
								//TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 1.0, pedmove_sprint) 
								TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 0.1, pedmove_sprint, 0.0, EGOTO_ENTITY_NEVER_SLOW_FOR_PATH_LENGTH)
								chop_ai_system_status++
							else 
								chop_ai_system_status = 300
							endif 
			
						endif 

					break 
					
					case 206
					
						int waypoint_node_pos
					
						if not is_ped_injured(gang_enemy[3].ped)
						
							request_anim_dict("CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@")
						
//							if not is_ped_ragdoll(gang_enemy[3].ped)
//							and not is_ped_in_combat(gang_enemy[3].ped)
							if not rpg_enemy_attack_task_given 

								if not (waypoint_recording_get_closest_waypoint("exile2_2", GET_ENTITY_COORDS(gang_enemy[3].ped), waypoint_node_pos) and (waypoint_node_pos >= 40))

									if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<2.5, 2.5, 5.0>>) //-4.0
										
										//keeps chop chasing after the rpg enemy
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GO_TO_ENTITY) 
										and ((not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack"))
										     or (is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") and get_entity_anim_current_time(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") > 0.7)) 

											TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 0.1, pedmove_sprint, 0.0, EGOTO_ENTITY_NEVER_SLOW_FOR_PATH_LENGTH)
											
											force_ped_motion_state(chop.ped, ms_on_foot_sprint)
											
											chop_time = get_game_timer()

										endif 
										
										//chop_audio_system(chop_finds_enemy)
										
									else 
									
										//have a chop anim flag 
										
										//chop jumps up to bite but does not take down enemy
										
										if has_anim_dict_loaded("CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@")
											if not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") 
												task_play_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack", slow_BLEND_IN, slow_BLEND_OUT)
											endif 
										endif 
									
									endif
									
								else 
								
									if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<1.0, 1.0, 1.0>>) //-4.0
									
										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GO_TO_ENTITY) 
										and ((not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack"))
										     or (is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") and get_entity_anim_current_time(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") > 0.7)) //0.6

											TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 0.1, pedmove_sprint, 0.0, EGOTO_ENTITY_NEVER_SLOW_FOR_PATH_LENGTH)
											
											force_ped_motion_state(chop.ped, ms_on_foot_sprint)
											
											chop_time = get_game_timer()
											
											//reset chop anim flag = true 
										
										endif 
										
									endif 
								

									request_anim_dict("missfra0_chop_find")
									
									if has_anim_dict_loaded("missfra0_chop_find")
									
										printstring("take down")
										printnl()
										
										
										is_entity_in_angled_area(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<-1.0, -1.0, -1.0>>), get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<1.0, -1.0, 2.0>>), 7.0) //3.0
										
										
//										if (get_script_task_status(chop.ped, SCRIPT_TASK_GO_TO_ENTITY) = performing_task)
//											printstring("take down 0")
//											printnl()
//										endif 
//										
//										if (get_script_task_status(gang_enemy[3].ped, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = performing_task)
//											printstring("take down 1")
//											printnl()
//										endif 
//											
//										if (waypoint_recording_get_closest_waypoint("exile2_2", GET_ENTITY_COORDS(gang_enemy[3].ped), waypoint_node_pos) and (waypoint_node_pos >= 40))
//											printstring("take down 2")
//											printnl()
//										endif 
									
									
										if (get_script_task_status(chop.ped, SCRIPT_TASK_GO_TO_ENTITY) = performing_task)
										and is_synchronized_scene_running(gang_enemy[3].scene_id)
											if (waypoint_recording_get_closest_waypoint("exile2_2", GET_ENTITY_COORDS(gang_enemy[3].ped), waypoint_node_pos) and (waypoint_node_pos >= 40))
												if is_entity_in_angled_area(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<-1.0, -1.0, -1.0>>), get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<1.0, -1.0, 2.0>>), 3.5) //3.0
													
													task_synchronized_scene(chop.ped, gang_enemy[3].scene_id, "missexile2", "take_down_bad_guy_chop", 1, normal_blend_out, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, rbf_none, 1)	
	
													chop_ai_system_status = 207
			
												endif 
											endif 
										endif 
										
									endif 
									
								endif 
											
							else 
							
								printstring("chop 1")
								printnl()
							
								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<9.0, 9.0, 5.0>>) //-4.0
								
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GO_TO_ENTITY)
									and not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_CORE@", "attack") 
									and not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01")
									
										TASK_GO_TO_ENTITY(chop.ped, gang_enemy[3].ped, -1, 1.0, pedmove_sprint) //we want chop to slow down here 
									
									endif 
									
								else
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<1.0, 1.0, 5.0>>) //-4.0

										if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY)
						
											task_follow_to_offset_of_entity(chop.ped, gang_enemy[3].ped, <<-1.0, 2.0, 0.0>>, pedmove_sprint, -1, 0.1) 
											
										endif 
									
									else
									
										if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 40.0)
											
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											and (not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01"))
											
												task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)
												
											endif 

										else 
										
											request_anim_dict("CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@")
										
											if has_anim_dict_loaded("CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@")

												//if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_play_anim)
												if lk_timer(chop_time, 3500)
													if not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01")
														task_play_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01", normal_BLEND_IN, normal_BLEND_OUT)
														chop_time = get_game_timer()
													endif 
												endif 
													
											endif 

										endif 
										
									endif
									
								endif 
	
							endif 
							
						else 

							chop_ai_system_status = 300
							
						endif 
					
					break 
					
					case 207
					
//						bool pos_met
//						bool rot_met 
//						
//						pos_met = false
//						rot_met  = false
					
						if not is_ped_injured(gang_enemy[3].ped)
						
							if is_synchronized_scene_running(gang_enemy[3].scene_id)
								if get_synchronized_scene_phase(gang_enemy[3].scene_id) >= 1.0
						
									set_ped_can_ragdoll(chop.ped, true)
									set_ragdoll_blocking_flags(chop.ped, RBF_PLAYER_IMPACT)
									
									task_follow_to_offset_of_entity(chop.ped, gang_enemy[3].ped, <<-2.0, 0.0, 0.0>>, pedmove_walk, -1, 0.1) 
									
									chop_ai_system_status++
									
								endif
								
							endif 

						
//							//DRAW_MARKER(MARKER_CYLINDER, take_down_pos, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.1, 0.1, 0.1>>, 255, 0, 128, 178)
//						
//							printstring("take down pos = ")
//							printvector(take_down_pos)
//							printnl()
//			
//							vector vec_chop_pos_direction
//							vector vec_chop_rot_direction
//
//							vec_chop_pos_direction = normalise_vector(take_down_pos - chop.pos)
//							vec_chop_rot_direction = normalise_vector(take_down_rot - chop.rot)
//								  
//							if not are_vectors_alomost_equal(chop.pos, take_down_pos, (vec_chop_pos_direction * get_frame_time() * 1.00)) 
//
//								chop.pos += (vec_chop_pos_direction * get_frame_time() * 2.0)
//
//								printstring("chop pos")
//								printvector(chop.pos)
//								printnl()
//							
//							else
//							
//								chop.pos = take_down_pos 
//							
//								printstring("chop pos DONE = ")
//								printvector(chop.pos)
//								printnl()
//								
//								pos_met = true
//								
//							endif
//							
//							if not are_vectors_alomost_equal(chop.rot, take_down_rot, (vec_chop_rot_direction * get_frame_time() * 1.00)) 
//
//								chop.rot += (vec_chop_rot_direction * get_frame_time() * 2.0)
//								
//								//set_entity_coords(chop.ped, chop.pos, true, true, true)
//	
//								//take_down_rot = take_down_rot
//								
//								printstring("chop rot")
//								printvector(chop.rot)
//								printnl()
//						
//							else 
//							
//								printstring("chop rot DONE")
//								printvector(chop.rot)
//								printnl()
//								
//								chop.rot = take_down_rot
//							
//								//chop_ai_system_status = 208
//								
//								rot_met = true
//							
//							endif
//							
//							if is_synchronized_scene_running(chop.scene_id)
//								SET_SYNCHRONIZED_SCENE_ORIGIN(chop.scene_id, chop.pos, chop.rot) 
//							endif 
//						
//							if (pos_met and rot_met)
//							or not is_synchronized_scene_running(chop.scene_id)
//								chop_ai_system_status = 208
//							endif 
				
						else 
						
							chop_ai_system_status = 300
						
						endif 
					
					break 
					
					case 208
					
						if not is_ped_injured(gang_enemy[3].ped)
					
							if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<-2.0, 0.0, 0.0>>), <<1.0, 1.0, 5.0>>) //-4.0

								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY)
				
									task_follow_to_offset_of_entity(chop.ped, gang_enemy[3].ped, <<-2.0, 0.0, 0.0>>, pedmove_walk, -1, 0.1) 
									
								endif 
							
							else
							
								if not is_ped_facing_ped(chop.ped, gang_enemy[3].ped, 40.0)
									
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									and (not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01"))
									
										task_turn_ped_to_face_entity(chop.ped, gang_enemy[3].ped)
										
									endif 

								else 
								
									request_anim_dict("CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@")
								
									if has_anim_dict_loaded("CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@")

										//if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_play_anim)
										if lk_timer(chop_time, 3500)
											if not is_entity_playing_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01")
												task_play_anim(chop.ped, "CREATURES@ROTTWEILER@MELEE@STREAMED_TAUNTS@", "taunt_01", normal_BLEND_IN, normal_BLEND_OUT)
												chop_time = get_game_timer()
											endif 
										endif 
											
									endif 

								endif 
								
							endif
							
						else 
						
							chop_ai_system_status = 300
						
						endif 
					
					break 
					
					case 300
					
						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) > 10
							SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
						endif 

						if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
								
							if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

								task_goto_entity_offset_xy(chop.ped, player_ped_id(), -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
							
							endif 
					
						else 
						
							if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(player_ped_id(), <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

								if not is_ped_facing_ped(chop.ped, player_ped_id(), 50.0)
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										
										float moveSpeed 
										moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(player_ped_id())
										
										if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
								
											task_turn_ped_to_face_entity(chop.ped, player_ped_id())	
											
										endif 

									endif 
								endif 
								
							endif 
							
						endif 

					break 
					
					case 400
							
						if is_entity_at_coord(helicopter.veh, pickup_pos[closest_warp_pos_to_franklin], <<20.00, 20.00, 5.5>>) //<<10.00, 10.00, 5.00>>)
						and IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh)
						and get_entity_speed(helicopter.veh) < 0.2

							chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
							chop_anim_rot = GET_ANIM_INITIAL_OFFSET_rotation("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)

							task_follow_nav_mesh_to_coord(chop.ped, chop.run_to_pos, pedmove_run, 60000, 0.15, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION, chop_anim_rot.z)
								
							chop_ai_system_status++
							
						else 
						
							//reset_chop_getting_into_helicopter()
						
						endif 
						
						if get_distance_between_coords(get_entity_coords(chop.ped), get_entity_coords(helicopter.veh)) > 45.00
	
							if not is_ped_in_vehicle(selector_ped.pedID[selector_ped_franklin], helicopter.veh, true)
							
								if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(chop.ped)) > 10
									SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
								endif 
								
								if not is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<2.5, 2.5, 4.0>>)
										
									if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_GOTO_ENTITY_OFFSET)

										task_goto_entity_offset_xy(chop.ped, selector_ped.pedID[selector_ped_franklin], -1, 0.5, -2.0, 5.0, pedmove_run, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
									
									endif 
							
								else 
								
									if is_entity_at_coord(chop.ped, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<-2.0, 5.0, 0.0>>), <<1.8, 1.8, 4.0>>)

										if not is_ped_facing_ped(chop.ped, selector_ped.pedID[selector_ped_franklin], 50.0)
											if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
												
												float moveSpeed 
												moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(selector_ped.pedID[selector_ped_franklin])
												
												if IS_MOVE_BLEND_RATIO_STILL(moveSpeed)
										
													task_turn_ped_to_face_entity(chop.ped, selector_ped.pedID[selector_ped_franklin])	
													
												endif 

											endif 
										endif 
										
									endif 
									
								endif 
								
							else 
							
								//walk to original position
								///task turn ped to face entity. 
							
							endif 
							
						endif 
					
					break 
					
					case 401

						if IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh)
						and get_entity_speed(helicopter.veh) < 0.2
						
							chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
							
//							printstring("chop.run_to_pos")
//							printvector(chop.run_to_pos)
//							printnl()
						
							if not is_entity_at_coord(chop.ped, chop.run_to_pos, <<0.3, 0.3, 2.0>>)//<<0.1, 0.1, 2.0>>)
	
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								
									chop.run_to_pos = GET_ANIM_INITIAL_OFFSET_POSITION("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)
									chop_anim_rot = GET_ANIM_INITIAL_OFFSET_rotation("missexile2", "chop_get_in_frogger_rds", get_entity_coords(helicopter.veh), get_entity_rotation(helicopter.veh), 0.0)

									task_follow_nav_mesh_to_coord(chop.ped, chop.run_to_pos, pedmove_run, 60000, 0.15, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION, chop_anim_rot.z)
								
								endif 
								
							else 
							
								if has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								
									cutscene_pos = <<0.0, 0.0, 0.0>> //GET_ENTITY_COORDS(army_truck.veh)
									cutscene_rot = <<0.0, 0.0, 0.0>> //GET_ENTITY_ROTATION(army_truck.veh)
		
									chop.scene_id = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
									//SYNCED_SCENE_DONT_INTERRUPT
									task_synchronized_scene(chop.ped, chop.scene_id, "missexile2", "chop_get_in_frogger_rds", normal_blend_in, normal_blend_out, SYNCED_SCENE_USE_PHYSICS, rbf_none, normal_blend_in)	
							
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(chop.scene_id, helicopter.veh, 0)
									
									//SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(helicopter.veh, false)

									chop_ai_system_status++
								endif 
								
							endif 
							
						else 
						
							if not has_ped_task_finished_2(chop.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								clear_ped_tasks(chop.ped)
							endif 

						endif 
					
					break 
					
					case 402
					
						//block vehicle crontols here
						//accelerate
						//hover rotation
						//left analogue stick
					
						if IS_SYNCHRONIZED_SCENE_RUNNING(chop.scene_id)
							if GET_SYNCHRONIZED_SCENE_PHASE(chop.scene_id) >= 0.99
							
								clear_ped_tasks(chop.ped)
								//detach_SYNCHRONIZED_SCENE_TO_ENTITY()
								
								attach_entity_to_entity(chop.ped, helicopter.veh, GET_ENTITY_BONE_INDEX_BY_NAME(helicopter.veh, "seat_dside_r"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								task_play_anim(chop.ped, "missexile2", "Chop_Sit_In_Frogger_RDS", normal_blend_in, normal_blend_out, -1, af_looping) 
								force_ped_ai_and_animation_update(chop.ped)
								
								chop_ai_system_status++
								
							endif 
						
						else 
						
							//if the snyched scene fails - speak with Graeme Sales about putting on non interuptable flag
							chop_ai_system_status = 401
						
						endif 

					break
					
					case 403
					
					break 

				endswitch 
			
			break 
			
		endswitch 
		
		chop_breathing_effect()

	endif 
	
endproc 

proc chop_camera_system()

	if does_entity_exist(chop.ped)
	
		SWITCH chop_cam.state
		
			CASE CHOP_VIEW_CAM_ON_CHOP
				// check to see if selected a human character
				IF Selector_Ped.eNewSelectorPed = SELECTOR_PED_FRANKLIN
					// franklin
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						// if already franklin
						SET_CHOP_VIEW_CAM_TO_PLAYER(chop_cam, Selector_Ped)
					ELSE
					ENDIF
				ELIF Selector_Ped.eNewSelectorPed = SELECTOR_PED_MICHAEL
					// michael
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						// if already michael
						SET_CHOP_VIEW_CAM_TO_PLAYER(chop_cam, Selector_Ped)
					ELSE
					ENDIF
				ENDIF
			BREAK
			
			CASE CHOP_VIEW_CAM_TO_CHOP
				// set black and white view on when cam has arrived at chop
				SET_CHOP_VIEW_CAM_ON_CHOP(chop_cam, Selector_Ped, TRUE)
			BREAK
			
			CASE CHOP_VIEW_CAM_TO_PLAYER
				// set player view back on when cam has arrived at player
				SET_CHOP_VIEW_CAM_DISABLED(chop_cam, Selector_Ped)
			BREAK
			
			CASE CHOP_VIEW_CAM_DISABLED
				//chop_breathing_effect()
				IF IS_CHOP_SELECTED(chop_cam, Selector_Ped)
					// set chop cam state to chop. Doesn't do any cam stuff, allows mission to take care of it.
					SET_CHOP_VIEW_CAM_TO_CHOP(chop_cam, TRUE, TRUE, FALSE)
				ENDIF
			BREAK
			
		ENDSWITCH
		
	endif 
	
endproc 


//--------------------------------------------EXILE 2 SWITCH CAMS-------------------------------------------------- 

proc request_start_of_mission_assets()
			
	request_model(franklin_model)
	request_model(gang_enemy[0].model)
	request_model(chop.model)
	request_model(franklins_car.model)
	request_model(gang_car.model)
	request_model(radi)
	request_model(sabregt)
	request_vehicle_recording(003, "lkexcile2")
	request_vehicle_recording(004, "lkexcile2")
	request_vehicle_recording(005, "lkexile2")
	request_vehicle_recording(001, "lkexcile2_chase")
	request_vehicle_recording(002, "lkexcile2_chase")
	request_vehicle_recording(003, "lkexcile2_chase")
	request_vehicle_recording(004, "lkexcile2_chase")
	request_vehicle_recording(005, "lkexcile2_chase")
	request_vehicle_recording(006, "lkexcile2_chase")
	request_vehicle_recording(007, "lkexcile2_chase")
	request_vehicle_recording(008, "lkexcile2_chase")
	request_vehicle_recording(009, "lkexcile2_chase")
	request_vehicle_recording(010, "lkexcile2_chase")
	request_vehicle_recording(011, "lkexcile2_chase")
	request_vehicle_recording(012, "lkexcile2_chase")
	request_vehicle_recording(013, "lkexcile2_chase")
	request_vehicle_recording(014, "lkexcile2_chase")
	request_vehicle_recording(015, "lkexcile2_chase")
	request_vehicle_recording(016, "lkexcile2_chase")
	request_vehicle_recording(017, "lkexcile2_chase")
	request_vehicle_recording(018, "lkexcile2_chase")
	request_vehicle_recording(019, "lkexcile2_chase")
	request_vehicle_recording(100, "lkexcile2_chase")
	request_anim_dict("missexile2")

endproc 

func bool has_start_of_mission_assets_loaded()

	if has_model_loaded(franklin_model)
	and has_model_loaded(gang_enemy[0].model)
	and has_model_loaded(chop.model)
	and has_model_loaded(franklins_car.model)
	and has_model_loaded(gang_car.model)
	and has_model_loaded(radi)
	and has_model_loaded(sabregt)
	and has_vehicle_recording_been_loaded(003, "lkexcile2")
	and has_vehicle_recording_been_loaded(004, "lkexcile2")
	and has_vehicle_recording_been_loaded(005, "lkexile2")
	and has_vehicle_recording_been_loaded(001, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(002, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(003, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(004, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(005, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(006, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(007, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(008, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(009, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(010, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(011, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(012, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(013, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(014, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(015, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(016, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(017, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(018, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(019, "lkexcile2_chase")
	and has_vehicle_recording_been_loaded(100, "lkexcile2_chase")
	and has_anim_dict_loaded("missexile2")
	
		return true 
	
	endif 
	
	return false

endfunc

proc load_uber_data()

	SetPieceCarPos[0] = <<-2653.0444, 2624.0652, 16.2347>>
	SetPieceCarQuatX[0] = -0.0004
	SetPieceCarQuatY[0] = -0.0027
	SetPieceCarQuatZ[0] = -0.0782
	SetPieceCarQuatW[0] = 0.9969
	SetPieceCarRecording[0] = 2
	SetPieceCarStartime[0] = 0.0000
	SetPieceCarRecordingSpeed[0] = 1.0000
	SetPieceCarModel[0] = radi
	
	SetPieceCarPos[1] = <<-2640.9871, 2801.9900, 16.1853>>
	SetPieceCarQuatX[1] = -0.0004
	SetPieceCarQuatY[1] = -0.0007
	SetPieceCarQuatZ[1] = 0.9985
	SetPieceCarQuatW[1] = 0.0550
	SetPieceCarRecording[1] = 3
	SetPieceCarStartime[1] = 0.0000
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = sabregt
	
	SetPieceCarPos[2] = <<-2619.3706, 2936.4963, 16.2274>>
	SetPieceCarQuatX[2] = 0.0073
	SetPieceCarQuatY[2] = -0.0002
	SetPieceCarQuatZ[2] = 0.9985
	SetPieceCarQuatW[2] = 0.0545
	SetPieceCarRecording[2] = 4
	SetPieceCarStartime[2] = 8000.0000
	SetPieceCarRecordingSpeed[2] = 1.0000
	SetPieceCarModel[2] = radi
	
	SetPieceCarPos[3] = <<-2589.6963, 3257.1921, 12.9785>>
	SetPieceCarQuatX[3] = 0.0071
	SetPieceCarQuatY[3] = 0.0030
	SetPieceCarQuatZ[3] = 0.9990
	SetPieceCarQuatW[3] = 0.0441
	SetPieceCarRecording[3] = 5
	SetPieceCarStartime[3] = 21000.0000
	SetPieceCarRecordingSpeed[3] = 1.0000
	SetPieceCarModel[3] = radi
	
	SetPieceCarPos[4] = <<-2577.5564, 3311.5303, 12.9895>>
	SetPieceCarQuatX[4] = -0.0041
	SetPieceCarQuatY[4] = -0.0036
	SetPieceCarQuatZ[4] = 0.9987
	SetPieceCarQuatW[4] = 0.0514
	SetPieceCarRecording[4] = 6
	SetPieceCarStartime[4] = 21000.0000
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = sabregt
	
	SetPieceCarPos[5] = <<-2596.8572, 3022.3042, 16.0236>>
	SetPieceCarQuatX[5] = -0.0107
	SetPieceCarQuatY[5] = -0.0070
	SetPieceCarQuatZ[5] = -0.0460
	SetPieceCarQuatW[5] = 0.9989
	SetPieceCarRecording[5] = 7
	SetPieceCarStartime[5] = 15000.0000
	SetPieceCarRecordingSpeed[5] = 1.0000
	SetPieceCarModel[5] = sabregt
	
	SetPieceCarPos[6] = <<-2589.4927, 3110.0151, 14.6915>>
	SetPieceCarQuatX[6] = -0.0077
	SetPieceCarQuatY[6] = -0.0046
	SetPieceCarQuatZ[6] = -0.0227
	SetPieceCarQuatW[6] = 0.9997
	SetPieceCarRecording[6] = 8
	SetPieceCarStartime[6] = 15000.0000
	SetPieceCarRecordingSpeed[6] = 1.0000
	SetPieceCarModel[6] = radi
	
	SetPieceCarPos[7] = <<-2533.4851, 3487.0640, 13.3270>>
	SetPieceCarQuatX[7] = -0.0050
	SetPieceCarQuatY[7] = -0.0050
	SetPieceCarQuatZ[7] = 0.9850
	SetPieceCarQuatW[7] = 0.1723
	SetPieceCarRecording[7] = 9
	SetPieceCarStartime[7] = 25000.0000
	SetPieceCarRecordingSpeed[7] = 1.0000
	SetPieceCarModel[7] = radi
	
	SetPieceCarPos[8] = <<-2551.2148, 3411.7734, 12.7331>>
	SetPieceCarQuatX[8] = -0.0008
	SetPieceCarQuatY[8] = -0.0074
	SetPieceCarQuatZ[8] = -0.1508
	SetPieceCarQuatW[8] = 0.9885
	SetPieceCarRecording[8] = 10
	SetPieceCarStartime[8] = 25000.0000
	SetPieceCarRecordingSpeed[8] = 1.0000
	SetPieceCarModel[8] = sabregt
	
	SetPieceCarPos[9] = <<-2483.4678, 3592.1587, 14.0380>>
	SetPieceCarQuatX[9] = -0.0077
	SetPieceCarQuatY[9] = -0.0065
	SetPieceCarQuatZ[9] = -0.1401
	SetPieceCarQuatW[9] = 0.9901
	SetPieceCarRecording[9] = 11
	SetPieceCarStartime[9] = 24500.0000 //25000.00
	SetPieceCarRecordingSpeed[9] = 1.0000
	SetPieceCarModel[9] = radi
	
	SetPieceCarPos[10] = <<-2481.9602, 3680.1619, 13.5370>>
	SetPieceCarQuatX[10] = 0.0054
	SetPieceCarQuatY[10] = -0.0109
	SetPieceCarQuatZ[10] = 0.9979
	SetPieceCarQuatW[10] = 0.0635
	SetPieceCarRecording[10] = 12
	SetPieceCarStartime[10] = 29000.0000
	SetPieceCarRecordingSpeed[10] = 1.0000
	SetPieceCarModel[10] = radi
	
	SetPieceCarPos[11] = <<-2456.9958, 3795.6919, 20.0964>>
	SetPieceCarQuatX[11] = -0.0045
	SetPieceCarQuatY[11] = -0.0323
	SetPieceCarQuatZ[11] = 0.9875
	SetPieceCarQuatW[11] = 0.1541
	SetPieceCarRecording[11] = 13
	SetPieceCarStartime[11] = 29000.0000
	SetPieceCarRecordingSpeed[11] = 1.0000
	SetPieceCarModel[11] = sabregt
	
	SetPieceCarPos[12] = <<-2388.1111, 3944.1819, 24.2239>>
	SetPieceCarQuatX[12] = 0.0008
	SetPieceCarQuatY[12] = -0.0038
	SetPieceCarQuatZ[12] = 0.9860
	SetPieceCarQuatW[12] = 0.1669
	SetPieceCarRecording[12] = 14
	SetPieceCarStartime[12] = 35000.0000
	SetPieceCarRecordingSpeed[12] = 1.0000
	SetPieceCarModel[12] = sabregt
	
	SetPieceCarPos[13] = <<-2303.0967, 4192.7466, 39.5016>>
	SetPieceCarQuatX[13] = -0.0082
	SetPieceCarQuatY[13] = -0.0267
	SetPieceCarQuatZ[13] = 0.9687
	SetPieceCarQuatW[13] = 0.2465
	SetPieceCarRecording[13] = 15
	SetPieceCarStartime[13] = 44000.0000
	SetPieceCarRecordingSpeed[13] = 1.0000
	SetPieceCarModel[13] = sabregt
	
	SetPieceCarPos[14] = <<-2194.6785, 4398.2886, 56.0071>>
	SetPieceCarQuatX[14] = -0.0063
	SetPieceCarQuatY[14] = -0.0611
	SetPieceCarQuatZ[14] = 0.9841
	SetPieceCarQuatW[14] = 0.1667
	SetPieceCarRecording[14] = 16
	SetPieceCarStartime[14] = 52000.0000
	SetPieceCarRecordingSpeed[14] = 1.0000
	SetPieceCarModel[14] = sabregt
	
//	SetPieceCarPos[15] = <<-2198.2463, 4384.1123, 54.2829>>
//	SetPieceCarQuatX[15] = -0.0020
//	SetPieceCarQuatY[15] = -0.0573
//	SetPieceCarQuatZ[15] = 0.9874
//	SetPieceCarQuatW[15] = 0.1477
//	SetPieceCarRecording[15] = 17
//	SetPieceCarStartime[15] = 52000.0000
//	SetPieceCarRecordingSpeed[15] = 1.0000
//	SetPieceCarModel[15] = sabregt
	
	SetPieceCarPos[16] = <<-2642.9128, 2781.5168, 16.2316>>
	SetPieceCarQuatX[16] = 0.0046
	SetPieceCarQuatY[16] = -0.0004
	SetPieceCarQuatZ[16] = 0.9978
	SetPieceCarQuatW[16] = 0.0668
	SetPieceCarRecording[16] = 18
	SetPieceCarStartime[16] = 4500.0000
	SetPieceCarRecordingSpeed[16] = 1.0000
	SetPieceCarModel[16] = radi
	
	SetPieceCarPos[17] = <<-2633.2573, 2853.9099, 16.1789>>
	SetPieceCarQuatX[17] = -0.0000
	SetPieceCarQuatY[17] = -0.0008
	SetPieceCarQuatZ[17] = 0.9984
	SetPieceCarQuatW[17] = 0.0573
	SetPieceCarRecording[17] = 19
	SetPieceCarStartime[17] = 4500.0000
	SetPieceCarRecordingSpeed[17] = 1.0000
	SetPieceCarModel[17] = sabregt

endproc 

proc create_uber_chase_assets()
										
	chop.pos = get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, 1.5>>)
	setup_chop(chop)
	remove_blip(chop.blip)
	attach_entity_to_entity(chop.ped, franklins_car.veh, GET_ENTITY_BONE_INDEX_BY_NAME(franklins_car.veh, "seat_pside_f"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	task_play_anim(chop.ped, "missexile2", "chop_sit_in_baller_ps", normal_blend_in, normal_blend_out, -1, af_looping) 
	
	//helicopter.veh = create_vehicle(helicopter.model, <<1760.0851, 3242.4521, 40.8998>>, 57.3614)
	
	gang_car.veh = create_vehicle(gang_car.model, <<-2639.9, 2733.9, 16.6>>, 350.00)
	SET_VEHICLE_ACTIVE_DURING_PLAYBACK(gang_car.veh, TRUE)
	SET_VEHICLE_DOORS_LOCKED(gang_car.veh, vehiclelock_lockout_player_only)
	set_vehicle_colours(gang_car.veh, 7, 0)//7,7
	set_entity_proofs(gang_car.veh, true, true, true, true, true)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(gang_car.veh, "EXILE_2_ENEMY_CAR")
	SET_DECAL_BULLET_IMPACT_RANGE_SCALE(3.0) //set bullets show up on proofed car fix for bug 1490817
	
	setup_enemy_in_vehicle(gang_enemy[0], gang_car.veh, vs_driver)
	set_ped_can_be_targetted(gang_enemy[0].ped, false)
	set_ped_suffers_critical_hits(gang_enemy[0].ped, false)
	set_entity_proofs(gang_enemy[0].ped, true, true, true, true, true)
	remove_blip(gang_enemy[0].blip)
		
	setup_enemy_in_vehicle(gang_enemy[1], gang_car.veh, vs_front_right)
	set_ped_can_be_targetted(gang_enemy[1].ped, false)
	set_ped_suffers_critical_hits(gang_enemy[1].ped, false)
	set_entity_proofs(gang_enemy[1].ped, true, true, true, true, true)
	remove_blip(gang_enemy[1].blip)
	add_ped_for_dialogue(scripted_speech, 6, gang_enemy[1].ped, "oneil")
	
	setup_enemy_in_vehicle(gang_enemy[2], gang_car.veh, vs_back_left)
	set_ped_can_be_targetted(gang_enemy[2].ped, false)
	set_ped_suffers_critical_hits(gang_enemy[2].ped, false)
	set_entity_proofs(gang_enemy[2].ped, true, true, true, true, true)
	remove_blip(gang_enemy[2].blip)

	INITIALISE_UBER_PLAYBACK("lkexcile2_chase", 001, true)
	load_uber_data()
	//switch_SetPieceCar_to_ai_on_collision = true
	fUberPlaybackDensitySwitchOffRange = 200
	allow_veh_to_stop_on_any_veh_impact = true
	bTrafficDontCleanupRecordingFiles = true
	traffic_block_vehicle_colour(true, traffic_black, traffic_white)
	start_playback_recorded_vehicle(gang_car.veh, 001, "lkexcile2_chase")
	update_uber_playback(gang_car.veh, 1.0)
	SET_UBER_PLAYBACK_TO_TIME_NOW(gang_car.veh, 7500)//10500
	update_uber_playback(gang_car.veh, 1.0)
	force_playback_recorded_vehicle_update(gang_car.veh)
	
	set_uber_speed_to_zero = true
	
endproc

ENUM EXILE_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE = 0,
	SWITCH_CAM_SETUP_GAMEPLAY,
	SWITCH_CAM_START_SPLINE1,
	SWITCH_CAM_PLAYING_SPLINE1,
	SWITCH_CAM_START_CODE_SWITCH,
	SWITCH_CAM_PLAYING_CODE_SWITCH,
	SWITCH_CAM_START_SPLINE2,
	SWITCH_CAM_PLAYING_SPLINE2,
	SWITCH_CAM_SHUTDOWN_SPLINE2,
	SWITCH_CAM_RETURN_TO_GAMEPLAY,
	SWITCH_CAM_HANDLE_POST_SCENE_TASKS
ENDENUM
EXILE_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsSwitchCamFranklinToTruckOutro
SWITCH_CAM_STRUCT scsSwitchCamScopeToFranklin
SWITCH_CAM_STRUCT scsSwitchCamTrevorMichael
SWITCH_CAM_STRUCT scsSwitchCamBrosToFranklin

STRING strAnimDictCamShake = "shake_cam_all@"

//FLOAT fGameplayCamHeadingFranklinToTruck = 0.0
FLOAT fGameplayCamPitchFranklinToTruck = 0.0

FLOAT fGameplayCamHeadingScopeToFranklin = 40.000
FLOAT fGameplayCamPitchScopeToFranklin = 0.0

FLOAT fGameplayCamHeadingFranklinToScope = 0.0
FLOAT fGameplayCamPitchFranklinToScope = 0.0
//VECTOR vFranklinToScopeMichaelAimCoord = <<-1687.800, 4457.800, 1.700>>

FLOAT fWooshStartPhase = 0.328
FLOAT fWooshStopPhase = 0.721
INT iWooshSoundId = -1
BOOL bWooshStarted = FALSE
BOOL bWooshStopped = FALSE

BOOL bMichaelSnipeAnimStarted = FALSE
BOOL bPlayerControlGiven = FALSE

#IF IS_DEBUG_BUILD
BOOL bAttachMichaelToHeli = FALSE
#ENDIF
VECTOR vAttachMichaelToHeliOffset
VECTOR vAttachMichaelToHeliRot

//ScopeToFranklin specific variables
STRING strAnimDict = "missexile2switch" 
				      
//STRING wayPointRec = "exile2_0"

VECTOR vSyncScenePos = <<-1683.497, 4455.500, 1.620>>
VECTOR vSyncSceneRot = <<0.0, 0.0, 190.000>>
INT iSyncedScene

FLOAT fSyncSceneStartPhase = 0.508
FLOAT fSyncSceneStopPhase = 1.0
FLOAT fFranklinBlendoutDelta = -3.000
FLOAT fChopBlendoutDelta = INSTANT_BLEND_OUT

BOOL bOkToSwitchPed = FALSE
BOOL bSyncSceneStarted = FALSE
BOOL bSyncSceneStopped = FALSE
BOOL bPlayerControlOff = FALSE
//BOOL bCanStartActionMode = FALSE
BOOL bCanStartChop = FALSE
BOOL bCanStartChopOverrideSpeed = FALSE

FLOAT fScopeToFrankFX1StartPhase = 0.328
FLOAT fScopeToFrankFX2StartPhase = 0.475

BOOL bScopeToFrankFX1Started = FALSE
BOOL bScopeToFrankFX2Started = FALSE

INT iPlayerControlDelay = 2200

//
//FLOAT fFranklinsCarRecordingSkipTime1 = 10000
//FLOAT fFranklinsCarRecordingSkipTime2 = 10000
//FLOAT fGangCarRecordingTime1 = 10000
//FLOAT fGangCarRecordingTime2 = 10000

FLOAT fGangAnimPhase = 0.080
FLOAT fFranklinAnimStartPhase = 0.450
FLOAT fFranklinAnimPhase = 0.080
FLOAT fSetEntitiesIntoCarsPhase = 0.900
FLOAT fGangConvoStartPhase = 0.0
FLOAT fFranklinConvoStartPhase = 0.5

BOOL bSplineCamStarted = FALSE
BOOL bFranklinAnimPaused = FALSE
BOOL bFranklinAnimUnpaused = FALSE
BOOL bSetEntitiesIntoCarsStarted = FALSE
//BOOL bPlayerControlGiven = FALSE
BOOL bGangConvoStarted = FALSE
BOOL bFranklinConvoStarted = FALSE

//BOOL bForceSwitchTimerDone = FALSE

FUNC BOOL IS_PLAYER_TRYING_TO_DRIVE()
	//CDEBUG3LN(DEBUG_MISSION, "IS_PLAYER_TRYING_TO_DRIVE")
	
	INT iLX, iLY, iRX, iRY
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iLX, iLY, iRX, iRY)
	IF iLX > 0.5
	OR iLX < -0.5
	OR iLY > 0.5
	OR iLY < -0.5
		RETURN TRUE
	ENDIF
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC START_MICHAEL_AND_TREVOR_ANIMS()
	CDEBUG3LN(DEBUG_MISSION, "START_MICHAEL_AND_TREVOR_ANIMS")
	
	IF DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_MICHAEL])
		IF NOT IS_ENTITY_DEAD(selector_ped.pedID[SELECTOR_PED_MICHAEL])
			CLEAR_PED_TASKS(selector_ped.pedID[SELECTOR_PED_MICHAEL])
			TASK_PLAY_ANIM(selector_ped.pedID[SELECTOR_PED_MICHAEL], "missexile2switch", "IG_3_SWITCH_TREV_to_FRA_AND_ONEILLS_MIC", SLOW_BLEND_IN, SLOW_BLEND_OUT)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(selector_ped.pedID[SELECTOR_PED_MICHAEL])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_TREVOR])
		IF NOT IS_ENTITY_DEAD(selector_ped.pedID[SELECTOR_PED_TREVOR])
			CLEAR_PED_TASKS(selector_ped.pedID[SELECTOR_PED_TREVOR])
			TASK_PLAY_ANIM(selector_ped.pedID[SELECTOR_PED_TREVOR], "missexile2switch", "IG_3_SWITCH_TREV_to_FRA_AND_ONEILLS_TREV", SLOW_BLEND_IN, SLOW_BLEND_OUT)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(selector_ped.pedID[SELECTOR_PED_TREVOR])
		ENDIF
	ENDIF
	
ENDPROC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_MICHAEL(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX viVeh)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_MICHAEL")
	
	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<0.6806, 2.1454, 0.7637>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].vNodeDir = <<-0.0696, -0.0387, 0.4774>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 50.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1000
		thisSwitchCam.nodes[1].vNodePos = <<0.1257, 2.4361, 0.7774>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].vNodeDir = <<-0.0780, -0.0526, 0.4550>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 50.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

//		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
//		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
//		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
//		thisSwitchCam.nodes[2].iNodeTime = 2850
//		thisSwitchCam.nodes[2].vNodePos = <<-0.9670, 2.2358, 0.7752>>
//		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
//		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
//		thisSwitchCam.nodes[2].vNodeDir = <<0.1931, 0.0343, 0.4317>>
//		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
//		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
//		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
//		thisSwitchCam.nodes[2].fNodeFOV = 50.0000
//		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
//		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
//		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
//		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.0000
//		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
//		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
//		thisSwitchCam.nodes[2].iCamEaseType = 0
//		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
//		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
//		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
//		thisSwitchCam.nodes[2].fTimeScale = 1.0000
//		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
//		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
//		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
//		thisSwitchCam.nodes[2].fMinExposure = 0.0000
//		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
//		thisSwitchCam.nodes[2].iRampUpDuration = 0
//		thisSwitchCam.nodes[2].iRampDownDuration = 0
//		thisSwitchCam.nodes[2].iHoldDuration = 0
//		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
//		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
//		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
//		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
//		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
//		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.iNumNodes = 2
		thisSwitchCam.iCamSwitchFocusNode = 0
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Exile2_TrevorMichael.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_Exile2_TrevorMichael.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.viVehicles[0] = viVeh
	
ENDPROC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_BROS_TO_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX viVeh1, VEHICLE_INDEX viVeh2)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_BROS_TO_FRANKLIN")

	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-1.3868, 5.1388, 0.9563>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<0.3650, 0.0876, 0.3022>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 40.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[0].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1000
		thisSwitchCam.nodes[1].vNodePos = <<-1.5970, 3.0990, 1.1270>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<-0.0320, -0.1090, 0.6850>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 40.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[1].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 2000
		thisSwitchCam.nodes[2].vNodePos = <<-1.5737, 2.6795, 1.0848>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<-0.2538, -0.2104, 0.6868>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 40.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[2].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = -0.1000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[3].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[3].iNodeTime = 500
		thisSwitchCam.nodes[3].vNodePos = <<-2.0237, 1.6637, 0.9185>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[3].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[3].vNodeDir = <<-0.6248, -0.4769, 0.7120>>
		thisSwitchCam.nodes[3].bPointAtEntity = TRUE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].fNodeFOV = 40.0000
		thisSwitchCam.nodes[3].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].iNodeToClone = 0
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[3].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[3].fCamNodeVelocityScale = 0.7280
		thisSwitchCam.nodes[3].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 0
		thisSwitchCam.nodes[3].iRampDownDuration = 0
		thisSwitchCam.nodes[3].iHoldDuration = 0
		thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 500
		thisSwitchCam.nodes[4].vNodePos = <<-1.7515, 1.2756, 0.5311>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<-0.1985, -0.2436, 0.3584>>
		thisSwitchCam.nodes[4].bPointAtEntity = TRUE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].fNodeFOV = 50.0000
		thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].iNodeToClone = 0
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[4].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[4].iCamEaseType = 0
		thisSwitchCam.nodes[4].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.6000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 2000
		thisSwitchCam.nodes[5].vNodePos = <<-2.0881, 0.2630, 0.5245>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<0.0062, -0.0933, 0.4130>>
		thisSwitchCam.nodes[5].bPointAtEntity = TRUE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].fNodeFOV = 50.0000
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[5].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 2000
		thisSwitchCam.nodes[6].vNodePos = <<-2.6841, -4.8933, 1.0322>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<-0.3427, 0.1818, 0.1332>>
		thisSwitchCam.nodes[6].bPointAtEntity = TRUE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].fNodeFOV = 50.0000
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[6].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.3980
		thisSwitchCam.nodes[6].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[7].bIsGameplayCamCopy = TRUE
		thisSwitchCam.nodes[7].iNodeTime = 1000
		thisSwitchCam.nodes[7].vNodePos = <<-0.5163, -7.5330, 1.2559>>
		thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[7].vNodeDir = <<0.5619, -0.1310, -0.2427>>
		thisSwitchCam.nodes[7].bPointAtEntity = TRUE
		thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[7].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[7].fNodeFOV = 50.0000
		thisSwitchCam.nodes[7].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].iNodeToClone = 0
		thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[7].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[7].iCamEaseType = 0
		thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[7].fTimeScale = 1.0000
		thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bFlashEnabled = FALSE
		thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[7].fMinExposure = 0.0000
		thisSwitchCam.nodes[7].fMaxExposure = 0.0000
		thisSwitchCam.nodes[7].iRampUpDuration = 0
		thisSwitchCam.nodes[7].iRampDownDuration = 0
		thisSwitchCam.nodes[7].iHoldDuration = 0
		thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.camVelocityOverrides[0].fStartPoint = 0.0000
		thisSwitchCam.camVelocityOverrides[0].fSpeed = 0.5500
		thisSwitchCam.camVelocityOverrides[1].fStartPoint = 1.0000
		thisSwitchCam.camVelocityOverrides[1].fSpeed = 0.5500
		thisSwitchCam.camVelocityOverrides[2].fStartPoint = 2.0000
		thisSwitchCam.camVelocityOverrides[2].fSpeed = 0.5500
		thisSwitchCam.camVelocityOverrides[3].fStartPoint = 2.8000
		thisSwitchCam.camVelocityOverrides[3].fSpeed = 80.0000
		thisSwitchCam.camVelocityOverrides[4].fStartPoint = 3.0000
		thisSwitchCam.camVelocityOverrides[4].fSpeed = 80.0000
		thisSwitchCam.camVelocityOverrides[5].fStartPoint = 3.8000
		thisSwitchCam.camVelocityOverrides[5].fSpeed = 60.0000
		thisSwitchCam.camVelocityOverrides[6].fStartPoint = 4.0000
		thisSwitchCam.camVelocityOverrides[6].fSpeed = 0.2000
		thisSwitchCam.camVelocityOverrides[7].fStartPoint = 4.2000
		thisSwitchCam.camVelocityOverrides[7].fSpeed = 5.0000
		thisSwitchCam.camVelocityOverrides[8].fStartPoint = 5.0000
		thisSwitchCam.camVelocityOverrides[8].fSpeed = 20.0000
		thisSwitchCam.camVelocityOverrides[9].fStartPoint = 6.7000
		thisSwitchCam.camVelocityOverrides[9].fSpeed = 15.0000
		thisSwitchCam.camVelocityOverrides[10].fStartPoint = 7.0000
		thisSwitchCam.camVelocityOverrides[10].fSpeed = 15.0000

		thisSwitchCam.camBlurOverrides[0].fBlurOverrideStartPoint = 3.0000
		thisSwitchCam.camBlurOverrides[0].fBlurOverrideBlurLevel = 1.0000
		thisSwitchCam.camBlurOverrides[1].fBlurOverrideStartPoint = 3.9000
		thisSwitchCam.camBlurOverrides[1].fBlurOverrideBlurLevel = 1.0000
		thisSwitchCam.camBlurOverrides[2].fBlurOverrideStartPoint = 4.0000
		thisSwitchCam.camBlurOverrides[2].fBlurOverrideBlurLevel = 0.0000
		thisSwitchCam.camBlurOverrides[3].fBlurOverrideStartPoint = 5.1500
		thisSwitchCam.camBlurOverrides[3].fBlurOverrideBlurLevel = 0.0000
		thisSwitchCam.camBlurOverrides[4].fBlurOverrideStartPoint = 6.0000
		thisSwitchCam.camBlurOverrides[4].fBlurOverrideBlurLevel = 0.7000
		thisSwitchCam.camBlurOverrides[5].fBlurOverrideStartPoint = 7.0000
		thisSwitchCam.camBlurOverrides[5].fBlurOverrideBlurLevel = 0.0000

		thisSwitchCam.iNumNodes = 8
		thisSwitchCam.iCamSwitchFocusNode = 4
		thisSwitchCam.fSwitchSoundAudioStartPhase = -1.0000
		thisSwitchCam.fSwitchSoundAudioEndPhase = -1.0000
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = TRUE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 1000


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Exile2_BrosToFranklin.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_Exile2_BrosToFranklin.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.viVehicles[0] = viVeh1
	thisSwitchCam.viVehicles[1] = viVeh2
	
ENDPROC

proc cleanup_HANDLE_SWITCH_CAM_TREVOR_MICHAEL_TO_BROS_AND_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam2)
						
	RENDER_SCRIPT_CAMS(FALSE, FALSE)

	IF DOES_CAM_EXIST(thisSwitchCam2.ciSpline)
		SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, FALSE)
		DESTROY_CAM(thisSwitchCam2.ciSpline)
	ENDIF
	DESTROY_ALL_CAMS()

	IF DOES_ENTITY_EXIST(franklins_car.veh)
		IF NOT IS_ENTITY_DEAD(franklins_car.veh)
			IF IS_VEHICLE_DRIVEABLE(franklins_car.veh)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(franklins_car.veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh)
					
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, EXL2_MAX_SPEED)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, EXL2_CAR_DAMAGE)
					
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(franklins_car.veh, EXL2_MAX_SPEED)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(franklins_car.veh, EXL2_CAR_DAMAGE)
					PRINTLN("\n\n\n Forced franklins_car.veh for stats \n\n\n")
					bVehGrabbedForStats = TRUE
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF



	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF					
	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)

	SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", FALSE)

	//disable_cellphone(false)
		
	eSwitchCamState = SWITCH_CAM_IDLE
	CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")

endproc 


FUNC BOOL HANDLE_SWITCH_CAM_TREVOR_MICHAEL_TO_BROS_AND_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam1, SWITCH_CAM_STRUCT &thisSwitchCam2)
	//CDEBUG3LN(DEBUG_MISSION, "HANDLE_SWITCH_CAM_TREVOR_MICHAEL_TO_BROS_AND_FRANKLIN")
	
	INT iCurrentNode
	FLOAT fCamPhase
	float first_person_cut_phase = 0.72
	
//	printstring("eSwitchCamState = ")
//	printint(enum_to_int(eSwitchCamState))
//	printnl()
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		/*CASE SWITCH_CAM_START_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE1")
			
			DESTROY_ALL_CAMS()
			
			SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_MICHAEL(thisSwitchCam1, trevors_truck.veh)
			CREATE_SPLINE_CAM(thisSwitchCam1)
			
			SET_CAM_ACTIVE(thisSwitchCam1.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			START_MICHAEL_AND_TREVOR_ANIMS()

			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE1
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
			
			IF IS_CAM_ACTIVE(thisSwitchCam1.ciSpline)
			
				iCurrentNode = iCurrentNode
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam1)
				fCamPhase = fCamPhase
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam1.ciSpline)
				
				IF fCamPhase >= 1.0
		
					SET_TIME_SCALE(1.0)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)

					IF DOES_CAM_EXIST(thisSwitchCam1.ciSpline)
						SET_CAM_ACTIVE(thisSwitchCam1.ciSpline, FALSE)
						DESTROY_CAM(thisSwitchCam1.ciSpline)
					ENDIF
					DESTROY_ALL_CAMS()
						
					eSwitchCamState = SWITCH_CAM_START_CODE_SWITCH
				
				ELSE
					RETURN FALSE
				ENDIF
				
			ELSE
				RETURN FALSE
			ENDIF
				
		FALLTHRU*/
		
		CASE SWITCH_CAM_START_CODE_SWITCH
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_CODE_SWITCH")
			
			thisSwitchCam1.iNumNodes = thisSwitchCam1.iNumNodes
			
			START_MICHAEL_AND_TREVOR_ANIMS()
			
			//SET_SELECTOR_PED_FAKE_STATE(selector_ped, SELECTOR_PED_FRANKLIN, SELECTOR_STATE_AVAILABLE)
			MAKE_SELECTOR_PED_SELECTION(selector_ped, SELECTOR_PED_FRANKLIN)
			selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]
			allow_switch = true
			
			bSplineCamStarted = FALSE
			
			eSwitchCamState = SWITCH_CAM_PLAYING_CODE_SWITCH
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_CODE_SWITCH")
		FALLTHRU

		CASE SWITCH_CAM_PLAYING_CODE_SWITCH
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_CODE_SWITCH")

			printstring("SWITCH_CAM_PLAYING_CODE_SWITCH test 0")
			printnl()

			if mission_flow != switching_into_franklin //only run when NOT switching into franklin
				
				printstring("SWITCH_CAM_PLAYING_CODE_SWITCH test 1")
				printnl()
				
				IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_auto, 0, 999, 999, true)// enum_to_int(SWITCH_FLAG_SKIP_OUTRO)
					
					if IS_PLAYER_SWITCH_IN_PROGRESS()

						SWITCH_STATE camera_switch_state
						camera_switch_state = GET_PLAYER_SWITCH_STATE() 

						if not does_entity_exist(gang_car.veh)            
						
							IF (camera_switch_state >= SWITCH_STATE_JUMPCUT_ASCENT) //before the pan. //SWITCH_STATE_WAITFORINPUT_OUTRO
									
								IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() >= 2
								
									clear_ped_tasks_immediately(player_ped_id())
									
									if does_entity_exist(selector_ped.pedID[selector_ped_michael])
										delete_ped(selector_ped.pedID[selector_ped_michael])
										set_model_as_no_longer_needed(michael_model)
										remove_ped_for_dialogue(scripted_speech, 0)
									endif 
									
									if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
										delete_ped(selector_ped.pedID[selector_ped_trevor])
										set_model_as_no_longer_needed(trevor_model)
										remove_ped_for_dialogue(scripted_speech, 2)
									endif 
									
									if does_entity_exist(trevors_truck.veh)
										delete_vehicle(trevors_truck.veh)
										set_model_as_no_longer_needed(trevors_truck.model)
										remove_vehicle_asset(trevors_truck.model)
									endif 
									
									clear_mission_locate_stuff(locates_data)
								
									request_start_of_mission_assets()
								
									if has_start_of_mission_assets_loaded()
									
										create_uber_chase_assets()
										
										ALLOW_PLAYER_SWITCH_PAN()
									
									endif 
								endif 
								
							endif 
						
						endif 
						
					endif 

					//---
					IF selector_cam.bOKToSwitchPed
						IF NOT selector_cam.bPedSwitched
							//IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped)//old
							IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, false)
							
								switch get_current_player_ped_enum()

									case char_franklin
									
										SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
										SPECIAL_ABILITY_CHARGE_NORMALIZED(player_id(), 0.33, true)
									
										if is_vehicle_driveable(gang_car.veh)
											if is_playback_going_on_for_vehicle(gang_car.veh)
											
												SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
													
												clear_ped_tasks_immediately(player_ped_id())
												set_ped_into_vehicle(player_ped_id(), franklins_car.veh) 
		
												gang_car.blip = create_blip_for_vehicle(gang_car.veh, true)
												
												//this will clear any random cars which have not been cleaned up by the uber
												clear_area(get_entity_coords(franklins_car.veh), 10000.00, true)
							
											endif 
										endif 
										
									break 
									
								endswitch 

								selector_cam.bPedSwitched = TRUE
								
							endif 
							
						endif 

					endif 
					
				endif 
			endif 
			
			printstring("SWITCH_CAM_PLAYING_CODE_SWITCH test 2")
			printnl()

			IF NOT bSplineCamStarted
					
				IF IS_PLAYER_SWITCH_IN_PROGRESS()

					SWITCH_STATE camera_switch_state
					camera_switch_state = GET_PLAYER_SWITCH_STATE() 
					
					//unpauses the uber system so you see the cars starting moving during the decent
					IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
				 		IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() <= 1
							set_uber_speed_to_zero = false 
						endif 
					endif 

					IF camera_switch_state >= SWITCH_STATE_OUTRO_HOLD
					
						selector_cam.bSplineComplete = true
						//selector_cam.bSplineActive = true //done in setup for switching to franklin
		
						//cleans up the code switch cam - added by instruction from alwyn
						RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_auto, 0, 999, 999, true)
						
						set_player_control(player_id(), false)
						
						eSwitchCamState = SWITCH_CAM_START_SPLINE2
						
						bSplineCamStarted = TRUE
					
					ELSE
						RETURN FALSE
					ENDIF
				
				ELSE
					RETURN FALSE
				ENDIF 
				
			ELSE
				RETURN FALSE
			ENDIF
				
			//endif 
			
		FALLTHRU
		
		CASE SWITCH_CAM_START_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE2")
			
			set_uber_speed_to_zero = false 
			
			IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
				SET_UBER_PLAYBACK_TO_TIME_NOW(gang_car.veh, 7500)//10500 
				update_uber_playback(gang_car.veh, 1.0)
				SET_ENTITY_VISIBLE(gang_car.veh, TRUE)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(gang_car.veh)
			ENDIF 
			
			IF IS_VEHICLE_DRIVEABLE(franklins_car.veh)
				SET_ENTITY_VISIBLE(franklins_car.veh, TRUE)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh, (7500 - GET_TIME_POSITION_IN_RECORDING(franklins_car.veh)))//9500) 11450
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(franklins_car.veh)
			ENDIF
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					set_entity_visible(player_ped_id(), true)
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "missexile2switch", "ig_4_switch_trev_to_franklin_fra", SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				ENDIF
			ENDIF

			IF DOES_ENTITY_EXIST(gang_enemy[0].ped)
				IF NOT IS_ENTITY_DEAD(gang_enemy[0].ped)
					CLEAR_PED_TASKS_IMMEDIATELY(gang_enemy[0].ped)
					ATTACH_ENTITY_TO_ENTITY(gang_enemy[0].ped, gang_car.veh, GET_ENTITY_BONE_INDEX_BY_NAME(gang_car.veh, "seat_dside_f"), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>))
					TASK_PLAY_ANIM(gang_enemy[0].ped, "missswitch", "in_car_driver", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING, fGangAnimPhase)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(gang_enemy[0].ped)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(gang_enemy[1].ped)
				IF NOT IS_ENTITY_DEAD(gang_enemy[1].ped)
					CLEAR_PED_TASKS_IMMEDIATELY(gang_enemy[1].ped)
					ATTACH_ENTITY_TO_ENTITY(gang_enemy[1].ped, gang_car.veh, GET_ENTITY_BONE_INDEX_BY_NAME(gang_car.veh, "seat_pside_f"), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>))
					TASK_PLAY_ANIM(gang_enemy[1].ped, "missswitch", "in_car_passenger", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING, fGangAnimPhase)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(gang_enemy[1].ped)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(gang_enemy[2].ped)
				IF NOT IS_ENTITY_DEAD(gang_enemy[2].ped)
					CLEAR_PED_TASKS_IMMEDIATELY(gang_enemy[2].ped)
					ATTACH_ENTITY_TO_ENTITY(gang_enemy[2].ped, gang_car.veh, GET_ENTITY_BONE_INDEX_BY_NAME(gang_car.veh, "seat_pside_r"), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>))
					TASK_PLAY_ANIM(gang_enemy[2].ped, "missswitch", "in_car_backseater", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING, fGangAnimPhase)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(gang_enemy[2].ped)
				ENDIF
			ENDIF
									
			DESTROY_ALL_CAMS()
			
			CLEAR_AREA(GET_ENTITY_COORDS(franklins_car.veh), 1000, TRUE)
			
			SETUP_SPLINE_CAM_NODE_ARRAY_BROS_TO_FRANKLIN(thisSwitchCam2, gang_car.veh, franklins_car.veh)
			CREATE_SPLINE_CAM(thisSwitchCam2)
			
			if get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person
				ANIMPOSTFX_STOP_AND_FLUSH_REQUESTS("SwitchOpenFranklinOut")
			endif 

			SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", TRUE)
			
			bFranklinAnimPaused = FALSE
			bFranklinAnimUnpaused = FALSE
			bSetEntitiesIntoCarsStarted = FALSE
			bPlayerControlGiven = FALSE
			bGangConvoStarted = FALSE
			bFranklinConvoStarted = FALSE
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2")
			
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE2

			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2")
			
			IF IS_CAM_ACTIVE(thisSwitchCam2.ciSpline)

				iCurrentNode = iCurrentNode
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam2)
				fCamPhase = fCamPhase
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam2.ciSpline)
				//CDEBUG3LN(DEBUG_MISSION, "fCamPhase = ", fCamPhase)
				
				printnl()
				printstring("CamPhase = ")
				printfloat(fCamPhase)
				printnl()
				
//				if not clear_area_for_car_chase //the car is set as no longer needed by update uber so clear_area above will miss it
//					clear_area(get_entity_coords(franklins_car.veh), 1000, true)
//				endif 
				
				if not bros_to_franklin_hit 
					PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 
					bros_to_franklin_hit = true 
				endif 
				
				IF NOT bGangConvoStarted
					IF fCamPhase >= fGangConvoStartPhase
						CDEBUG3LN(DEBUG_MISSION, "Starting Gang Dialogue...")
						bGangConvoStarted = CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_ONEIL", CONV_PRIORITY_MEDIUM)
					ENDIF
				ENDIF
				
				if not bros_to_franklin_transition
					IF fCamPhase >= 0.065
						PLAY_SOUND_FRONTEND(-1, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 
						bros_to_franklin_transition = true
					endif 
				endif 
				
				IF NOT bFranklinConvoStarted
					IF fCamPhase >= fFranklinConvoStartPhase
						CDEBUG3LN(DEBUG_MISSION, "Starting Franklin Dialogue...")
						bFranklinConvoStarted = CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_SWIT", conv_priority_medium)
					ENDIF
				ENDIF
				
				IF NOT bFranklinAnimPaused
					IF iCurrentNode >= 1
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missexile2switch", "ig_4_switch_trev_to_franklin_fra")	
									CDEBUG3LN(DEBUG_MISSION, "Pausing Franklin's Anim...")
									SET_ANIM_RATE(PLAYER_PED_ID(), 0.0)
									TASK_PLAY_ANIM(PLAYER_PED_ID(), "missexile2switch", "ig_4_switch_trev_to_franklin_fra", SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									bFranklinAnimPaused = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bFranklinAnimUnpaused
					IF fCamPhase >= fFranklinAnimStartPhase
						IF DOES_ENTITY_EXIST(franklins_car.veh)
							IF NOT IS_ENTITY_DEAD(franklins_car.veh)
								IF IS_VEHICLE_DRIVEABLE(franklins_car.veh)
									IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missexile2switch", "ig_4_switch_trev_to_franklin_fra")
												CDEBUG3LN(DEBUG_MISSION, "Starting Franklin's Anim...")
												SET_ANIM_RATE(PLAYER_PED_ID(), 1.0)
												TASK_PLAY_ANIM(PLAYER_PED_ID(), "missexile2switch", "ig_4_switch_trev_to_franklin_fra", SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, fFranklinAnimPhase)
												FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						bFranklinAnimUnpaused = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSetEntitiesIntoCarsStarted
					
					IF fCamPhase >= fSetEntitiesIntoCarsPhase
					or ((get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person) and (fCamPhase > first_person_cut_phase))
						
						CDEBUG3LN(DEBUG_MISSION, "Setting Entities into Cars...")
						
						//Stop Gang anims and set into vehicle
						IF DOES_ENTITY_EXIST(gang_car.veh)
							IF NOT IS_ENTITY_DEAD(gang_car.veh)
								IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
									IF DOES_ENTITY_EXIST(gang_enemy[0].ped)
										IF NOT IS_ENTITY_DEAD(gang_enemy[0].ped)
											DETACH_ENTITY(gang_enemy[0].ped)
											CLEAR_PED_TASKS_IMMEDIATELY(gang_enemy[0].ped)
											SET_PED_INTO_VEHICLE(gang_enemy[0].ped, gang_car.veh, VS_DRIVER)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(gang_enemy[0].ped)
										ENDIF
									ENDIF
									IF DOES_ENTITY_EXIST(gang_enemy[1].ped)
										IF NOT IS_ENTITY_DEAD(gang_enemy[1].ped)
											DETACH_ENTITY(gang_enemy[1].ped)
											CLEAR_PED_TASKS_IMMEDIATELY(gang_enemy[1].ped)
											SET_PED_INTO_VEHICLE(gang_enemy[1].ped, gang_car.veh, VS_FRONT_RIGHT)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(gang_enemy[1].ped)
										ENDIF
									ENDIF
									IF DOES_ENTITY_EXIST(gang_enemy[2].ped)
										IF NOT IS_ENTITY_DEAD(gang_enemy[2].ped)
											DETACH_ENTITY(gang_enemy[2].ped)
											CLEAR_PED_TASKS_IMMEDIATELY(gang_enemy[2].ped)
											SET_PED_INTO_VEHICLE(gang_enemy[2].ped, gang_car.veh, VS_BACK_RIGHT)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(gang_enemy[2].ped)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						bSetEntitiesIntoCarsStarted = TRUE
					ENDIF
				ENDIF

//				IF NOT bPlayerControlGiven
//					IF fCamPhase >= fPlayerControlGivenPhase
//						IF IS_PLAYER_TRYING_TO_DRIVE()
//						
//							CDEBUG3LN(DEBUG_MISSION, "Players trying to drive, giving control back...")
//							
//							IF DOES_ENTITY_EXIST(franklins_car.veh)
//								IF NOT IS_ENTITY_DEAD(franklins_car.veh)
//									IF IS_VEHICLE_DRIVEABLE(franklins_car.veh)
//										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(franklins_car.veh)
//											STOP_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh)
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//								
//							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						
//							DISPLAY_RADAR(TRUE)
//							DISPLAY_HUD(TRUE)
//				
//							bPlayerControlGiven = TRUE
//						ENDIF
//					ENDIF
//				ENDIF


				
				if (get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person) 
					if (fCamPhase > first_person_cut_phase)
				
						SET_TIME_SCALE(1.0)
					
						if not initialise_push_in_data_for_switch_cam_trevor_michael_to_bros_and_franklin

							FILL_PUSH_IN_DATA(push_in_camera, franklins_car.veh, char_franklin, 0.75, push_in_interp_time, push_in_cut_time, push_in_postfx_time, push_in_speed_up_time)
							SET_PUSH_IN_DIRECTION_MODIFIER(push_in_camera, <<10.00, 0.0, -10.0>>)   
							
							initialise_push_in_data_for_switch_cam_trevor_michael_to_bros_and_franklin = true 
	
						endif 
						
						if HANDLE_PUSH_IN(push_in_camera, true)
						
							cleanup_HANDLE_SWITCH_CAM_TREVOR_MICHAEL_TO_BROS_AND_FRANKLIN(thisSwitchCam2)
				
							RETURN TRUE
							
						endif 
						
					endif 
					
				else 
				
					IF fCamPhase >= 1.0
					
						SET_TIME_SCALE(1.0)
					
						cleanup_HANDLE_SWITCH_CAM_TREVOR_MICHAEL_TO_BROS_AND_FRANKLIN(thisSwitchCam2)
						
						return true 
					
					endif 

				ENDIF
		
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC


PROC SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_TRUCK_INTRO(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX piPed)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_TRUCK_INTRO")
	
	IF NOT thisSwitchCam.bInitialized

		//--- Start of Cam Data ---
		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 1000
		thisSwitchCam.nodes[0].vNodePos = <<0.3680, 1.4526, 0.5203>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].vNodeDir = <<0.2027, -0.0358, 0.3792>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].fNodeFOV = 60.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1000
		thisSwitchCam.nodes[1].vNodePos = <<0.2267, 1.6885, 2.8863>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].vNodeDir = <<-0.1356, 0.1599, -0.1024>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].fNodeFOV = 60.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 1000
		thisSwitchCam.nodes[2].vNodePos = <<2.8070, 7.2368, 17.6841>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].vNodeDir = <<-69.4125, 1.2049, 169.1701>>
		thisSwitchCam.nodes[2].bPointAtEntity = FALSE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].fNodeFOV = 60.0000
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 0.5000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0

		
		thisSwitchCam.iNumNodes = 3
		thisSwitchCam.iCamSwitchFocusNode = 0
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---



		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Exile2_FranklinToTruckIntro.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_Exile2_FranklinToTruckIntro.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piPed
	
ENDPROC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_TRUCK_OUTRO(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX viTruck)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_TRUCK_OUTRO")
	
	IF NOT thisSwitchCam.bInitialized

		//--- Start of Cam Data ---
		if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person

			thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[0].iNodeTime = 0
			thisSwitchCam.nodes[0].vNodePos = <<6.8668, -11.1680, 28.3491>>
			thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[0].vNodeDir = <<-72.7517, -0.0281, 35.9953>>
			thisSwitchCam.nodes[0].bPointAtEntity = FALSE
			thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[0].fNodeFOV = 60.0000
			thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[0].iNodeToClone = 0
			thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[0].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[0].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[0].iCamEaseType = 0
			thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[0].fTimeScale = 1.0000
			thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[0].bFlashEnabled = FALSE
			thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[0].fMinExposure = 0.0000
			thisSwitchCam.nodes[0].fMaxExposure = 0.0000
			thisSwitchCam.nodes[0].iRampUpDuration = 0
			thisSwitchCam.nodes[0].iRampDownDuration = 0
			thisSwitchCam.nodes[0].iHoldDuration = 0
			thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[1].iNodeTime = 1500
			thisSwitchCam.nodes[1].vNodePos = <<5.7354, -9.3271, 25.0715>>
			thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[1].vNodeDir = <<-74.5660, 0.2104, 32.3517>>
			thisSwitchCam.nodes[1].bPointAtEntity = FALSE
			thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[1].fNodeFOV = 60.0000
			thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[1].iNodeToClone = 0
			thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[1].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[1].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[1].iCamEaseType = 0
			thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
			thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[1].fTimeScale = 1.0000
			thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[1].bFlashEnabled = FALSE
			thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[1].fMinExposure = 0.0000
			thisSwitchCam.nodes[1].fMaxExposure = 0.0000
			thisSwitchCam.nodes[1].iRampUpDuration = 0
			thisSwitchCam.nodes[1].iRampDownDuration = 0
			thisSwitchCam.nodes[1].iHoldDuration = 0
			thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[2].iNodeTime = 500
			thisSwitchCam.nodes[2].vNodePos = <<5.1772, -7.9185, 7.7020>>
			thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[2].vNodeDir = <<-0.1686, 0.4822, -0.5376>>
			thisSwitchCam.nodes[2].bPointAtEntity = TRUE
			thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[2].fNodeFOV = 45.0000
			thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[2].iNodeToClone = 0
			thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[2].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[2].iCamEaseType = 0
			thisSwitchCam.nodes[2].fCamEaseScaler = 1.0000
			thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[2].fTimeScale = 1.0000
			thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 1.0000
			thisSwitchCam.nodes[2].bFlashEnabled = FALSE
			thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[2].fMinExposure = 0.0000
			thisSwitchCam.nodes[2].fMaxExposure = 0.0000
			thisSwitchCam.nodes[2].iRampUpDuration = 0
			thisSwitchCam.nodes[2].iRampDownDuration = 0
			thisSwitchCam.nodes[2].iHoldDuration = 0
			thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[3].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[3].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[3].iNodeTime = 400
			thisSwitchCam.nodes[3].vNodePos = <<3.7694, -5.7515, 1.8344>>
			thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[3].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[3].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[3].vNodeDir = <<0.0332, 0.1615, -0.4690>>
			thisSwitchCam.nodes[3].bPointAtEntity = TRUE
			thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[3].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[3].fNodeFOV = 40.0000
			thisSwitchCam.nodes[3].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[3].iNodeToClone = 0
			thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[3].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[3].iCamEaseType = 0
			thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[3].fCamNodeVelocityScale = 0.4000
			thisSwitchCam.nodes[3].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[3].fTimeScale = 1.0000
			thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 1.0000
			thisSwitchCam.nodes[3].bFlashEnabled = FALSE
			thisSwitchCam.nodes[3].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[3].fMinExposure = 0.0000
			thisSwitchCam.nodes[3].fMaxExposure = 0.0000
			thisSwitchCam.nodes[3].iRampUpDuration = 0
			thisSwitchCam.nodes[3].iRampDownDuration = 0
			thisSwitchCam.nodes[3].iHoldDuration = 0
			thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[4].iNodeTime = 800
			thisSwitchCam.nodes[4].vNodePos = <<2.7853, -4.2615, 1.0039>>
			thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[4].vNodeDir = <<-0.0299, 0.0337, -0.1757>>
			thisSwitchCam.nodes[4].bPointAtEntity = TRUE
			thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[4].fNodeFOV = 40.0000
			thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[4].iNodeToClone = 0
			thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[4].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[4].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[4].iCamEaseType = 0
			thisSwitchCam.nodes[4].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.9580
			thisSwitchCam.nodes[4].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[4].fTimeScale = 1.0000
			thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 1.0000
			thisSwitchCam.nodes[4].bFlashEnabled = FALSE
			thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[4].fMinExposure = 0.0000
			thisSwitchCam.nodes[4].fMaxExposure = 0.0000
			thisSwitchCam.nodes[4].iRampUpDuration = 0
			thisSwitchCam.nodes[4].iRampDownDuration = 0
			thisSwitchCam.nodes[4].iHoldDuration = 0
			thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[5].iNodeTime = 1200
			thisSwitchCam.nodes[5].vNodePos = <<2.6530, -4.0735, 0.9800>>
			thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[5].vNodeDir = <<0.0424, -0.0112, 0.0879>>
			thisSwitchCam.nodes[5].bPointAtEntity = TRUE
			thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[5].fNodeFOV = 40.0000
			thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[5].iNodeToClone = 0
			thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[5].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[5].iCamEaseType = 0
			thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[5].fTimeScale = 1.0000
			thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 1.0000
			thisSwitchCam.nodes[5].bFlashEnabled = FALSE
			thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[5].fMinExposure = 0.0000
			thisSwitchCam.nodes[5].fMaxExposure = 0.0000
			thisSwitchCam.nodes[5].iRampUpDuration = 0
			thisSwitchCam.nodes[5].iRampDownDuration = 0
			thisSwitchCam.nodes[5].iHoldDuration = 0
			thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

//			thisSwitchCam.nodes[6].bIsCamCutNode = TRUE
//
//			thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
//			thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
//			thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
//			thisSwitchCam.nodes[7].iNodeTime = 0
//			thisSwitchCam.nodes[7].vNodePos = <<-4.4810, 7.8828, 1.7302>>
//			thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
//			thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
//			thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
//			thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
//			thisSwitchCam.nodes[7].vNodeDir = <<-0.4777, 0.0964, 0.7823>>
//			thisSwitchCam.nodes[7].bPointAtEntity = TRUE
//			thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = FALSE
//			thisSwitchCam.nodes[7].bAttachOffsetIsRelative = FALSE
//			thisSwitchCam.nodes[7].fNodeFOV = 40.0000
//			thisSwitchCam.nodes[7].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
//			thisSwitchCam.nodes[7].iNodeToClone = 0
//			thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
//			thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
//			thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
//			thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
//			thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_DEFAULT
//			thisSwitchCam.nodes[7].fNodeCamShake = 0.0000
//			thisSwitchCam.nodes[7].iCamEaseType = 0
//			thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
//			thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
//			thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
//			thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
//			thisSwitchCam.nodes[7].fTimeScale = 1.0000
//			thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
//			thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
//			thisSwitchCam.nodes[7].bFlashEnabled = FALSE
//			thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
//			thisSwitchCam.nodes[7].fMinExposure = 0.0000
//			thisSwitchCam.nodes[7].fMaxExposure = 0.0000
//			thisSwitchCam.nodes[7].iRampUpDuration = 0
//			thisSwitchCam.nodes[7].iRampDownDuration = 0
//			thisSwitchCam.nodes[7].iHoldDuration = 0
//			thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
//			thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
//			thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
//			thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
//			thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
//			thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.iNumNodes = 6
			thisSwitchCam.iCamSwitchFocusNode = 0
			thisSwitchCam.fSwitchSoundAudioStartPhase = -1.0000
			thisSwitchCam.fSwitchSoundAudioEndPhase = -1.0000
			thisSwitchCam.bSplineNoSmoothing = TRUE
			thisSwitchCam.bAddGameplayCamAsLastNode = false
			thisSwitchCam.iGameplayNodeBlendDuration = 0//1500
			
		else 
		
			thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[0].iNodeTime = 0
			thisSwitchCam.nodes[0].vNodePos = <<6.8668, -11.1680, 28.3491>>
			thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[0].vNodeDir = <<-72.7517, -0.0281, 35.9953>>
			thisSwitchCam.nodes[0].bPointAtEntity = FALSE
			thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[0].fNodeFOV = 60.0000
			thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[0].iNodeToClone = 0
			thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[0].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[0].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[0].iCamEaseType = 0
			thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[0].fTimeScale = 1.0000
			thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[0].bFlashEnabled = FALSE
			thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[0].fMinExposure = 0.0000
			thisSwitchCam.nodes[0].fMaxExposure = 0.0000
			thisSwitchCam.nodes[0].iRampUpDuration = 0
			thisSwitchCam.nodes[0].iRampDownDuration = 0
			thisSwitchCam.nodes[0].iHoldDuration = 0
			thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[1].iNodeTime = 1500
			thisSwitchCam.nodes[1].vNodePos = <<5.7354, -9.3271, 25.0715>>
			thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[1].vNodeDir = <<-74.5660, 0.2104, 32.3517>>
			thisSwitchCam.nodes[1].bPointAtEntity = FALSE
			thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[1].fNodeFOV = 60.0000
			thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[1].iNodeToClone = 0
			thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[1].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[1].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[1].iCamEaseType = 0
			thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
			thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[1].fTimeScale = 1.0000
			thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[1].bFlashEnabled = FALSE
			thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[1].fMinExposure = 0.0000
			thisSwitchCam.nodes[1].fMaxExposure = 0.0000
			thisSwitchCam.nodes[1].iRampUpDuration = 0
			thisSwitchCam.nodes[1].iRampDownDuration = 0
			thisSwitchCam.nodes[1].iHoldDuration = 0
			thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[2].iNodeTime = 500
			thisSwitchCam.nodes[2].vNodePos = <<5.1772, -7.9185, 7.7020>>
			thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[2].vNodeDir = <<-0.1686, 0.4822, -0.5376>>
			thisSwitchCam.nodes[2].bPointAtEntity = TRUE
			thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[2].fNodeFOV = 40.0000
			thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[2].iNodeToClone = 0
			thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[2].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[2].iCamEaseType = 0
			thisSwitchCam.nodes[2].fCamEaseScaler = 1.0000
			thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[2].fTimeScale = 1.0000
			thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 1.0000
			thisSwitchCam.nodes[2].bFlashEnabled = FALSE
			thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[2].fMinExposure = 0.0000
			thisSwitchCam.nodes[2].fMaxExposure = 0.0000
			thisSwitchCam.nodes[2].iRampUpDuration = 0
			thisSwitchCam.nodes[2].iRampDownDuration = 0
			thisSwitchCam.nodes[2].iHoldDuration = 0
			thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[3].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[3].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[3].iNodeTime = 400
			thisSwitchCam.nodes[3].vNodePos = <<3.7694, -5.7515, 1.8344>>
			thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[3].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[3].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[3].vNodeDir = <<0.0332, 0.1615, -0.4690>>
			thisSwitchCam.nodes[3].bPointAtEntity = TRUE
			thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[3].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[3].fNodeFOV = 40.0000
			thisSwitchCam.nodes[3].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[3].iNodeToClone = 0
			thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[3].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[3].iCamEaseType = 0
			thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[3].fCamNodeVelocityScale = 0.4000
			thisSwitchCam.nodes[3].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[3].fTimeScale = 1.0000
			thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 1.0000
			thisSwitchCam.nodes[3].bFlashEnabled = FALSE
			thisSwitchCam.nodes[3].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[3].fMinExposure = 0.0000
			thisSwitchCam.nodes[3].fMaxExposure = 0.0000
			thisSwitchCam.nodes[3].iRampUpDuration = 0
			thisSwitchCam.nodes[3].iRampDownDuration = 0
			thisSwitchCam.nodes[3].iHoldDuration = 0
			thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[4].iNodeTime = 800
			thisSwitchCam.nodes[4].vNodePos = <<2.7853, -4.2615, 1.0039>>
			thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[4].vNodeDir = <<-0.0299, 0.0337, -0.1757>>
			thisSwitchCam.nodes[4].bPointAtEntity = TRUE
			thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[4].fNodeFOV = 40.0000
			thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[4].iNodeToClone = 0
			thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[4].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[4].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[4].iCamEaseType = 0
			thisSwitchCam.nodes[4].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.9580
			thisSwitchCam.nodes[4].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[4].fTimeScale = 1.0000
			thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 1.0000
			thisSwitchCam.nodes[4].bFlashEnabled = FALSE
			thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[4].fMinExposure = 0.0000
			thisSwitchCam.nodes[4].fMaxExposure = 0.0000
			thisSwitchCam.nodes[4].iRampUpDuration = 0
			thisSwitchCam.nodes[4].iRampDownDuration = 0
			thisSwitchCam.nodes[4].iHoldDuration = 0
			thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[5].iNodeTime = 1200
			thisSwitchCam.nodes[5].vNodePos = <<2.6530, -4.0735, 0.9800>>
			thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[5].vNodeDir = <<0.0424, -0.0112, 0.0879>>
			thisSwitchCam.nodes[5].bPointAtEntity = TRUE
			thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[5].fNodeFOV = 40.0000
			thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[5].iNodeToClone = 0
			thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_MEDIUM
			thisSwitchCam.nodes[5].fNodeCamShake = 0.2000
			thisSwitchCam.nodes[5].iCamEaseType = 0
			thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[5].fTimeScale = 1.0000
			thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 1.0000
			thisSwitchCam.nodes[5].bFlashEnabled = FALSE
			thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[5].fMinExposure = 0.0000
			thisSwitchCam.nodes[5].fMaxExposure = 0.0000
			thisSwitchCam.nodes[5].iRampUpDuration = 0
			thisSwitchCam.nodes[5].iRampDownDuration = 0
			thisSwitchCam.nodes[5].iHoldDuration = 0
			thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[6].bIsCamCutNode = TRUE

			thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[7].iNodeTime = 0
			thisSwitchCam.nodes[7].vNodePos = <<-4.4810, 7.8828, 1.7302>>
			thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[7].vNodeDir = <<-0.4777, 0.0964, 0.7823>>
			thisSwitchCam.nodes[7].bPointAtEntity = TRUE
			thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[7].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[7].fNodeFOV = 40.0000
			thisSwitchCam.nodes[7].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[7].iNodeToClone = 0
			thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[7].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[7].iCamEaseType = 0
			thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
			thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[7].fTimeScale = 1.0000
			thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[7].bFlashEnabled = FALSE
			thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[7].fMinExposure = 0.0000
			thisSwitchCam.nodes[7].fMaxExposure = 0.0000
			thisSwitchCam.nodes[7].iRampUpDuration = 0
			thisSwitchCam.nodes[7].iRampDownDuration = 0
			thisSwitchCam.nodes[7].iHoldDuration = 0
			thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.iNumNodes = 8
			thisSwitchCam.iCamSwitchFocusNode = 0
			thisSwitchCam.fSwitchSoundAudioStartPhase = -1.0000
			thisSwitchCam.fSwitchSoundAudioEndPhase = -1.0000
			thisSwitchCam.bSplineNoSmoothing = TRUE
			thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
			thisSwitchCam.iGameplayNodeBlendDuration = 1500
		
		endif 


		//--- End of Cam Data ---



		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Exile2_FranklinToTruckOutro.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_Exile2_FranklinToTruckOutro.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.viVehicles[0] = viTruck
	
ENDPROC

FUNC BOOL PRELOAD_ASCENT_SHOT()
	CDEBUG3LN(DEBUG_MISSION, "PRELOAD_ASCENT_SHOT")
	
	RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, DEFAULT, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO | SWITCH_FLAG_PAUSE_BEFORE_ASCENT | SWITCH_FLAG_SKIP_OUTRO))
	
	IF IS_SWITCH_READY_FOR_ASCENT()
		CDEBUG3LN(DEBUG_MISSION, "Ascent Shot Ready.")
		RETURN TRUE
	ENDIF
	CDEBUG3LN(DEBUG_MISSION, "Ascent Shot NOT READY!")
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_SWITCH_CAM_FRANKLIN_TO_TRUCK(SWITCH_CAM_STRUCT &thisSwitchCam2)
	
	INT iCurrentNode
	
	printstring("eSwitchCamState")
	printint(enum_to_int(eSwitchCamState))
	printnl()

	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		/*CASE SWITCH_CAM_START_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE1")
			
			PRELOAD_ASCENT_SHOT()
			
			DESTROY_ALL_CAMS()			
	
			SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_TRUCK_INTRO(thisSwitchCam1, PLAYER_PED_ID())
			CREATE_SPLINE_CAM(thisSwitchCam1)

			SET_CAM_ACTIVE(thisSwitchCam1.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
		
			MAKE_SELECTOR_PED_SELECTION(selector_ped, SELECTOR_PED_TREVOR)
			
			bWooshOut = FALSE
			bPlayerControlGiven = FALSE
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
			
			IF IS_CAM_ACTIVE(thisSwitchCam1.ciSpline)

				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam1)
			
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam1.ciSpline) >= 1.0
					eSwitchCamState = SWITCH_CAM_START_CODE_SWITCH
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
		FALLTHRU*/
		
		CASE SWITCH_CAM_START_CODE_SWITCH
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_CODE_SWITCH")
			
			MAKE_SELECTOR_PED_SELECTION(selector_ped, SELECTOR_PED_TREVOR)
		
			/*IF DOES_CAM_EXIST(thisSwitchCam1.ciSpline)
				DESTROY_CAM(thisSwitchCam1.ciSpline)
				DESTROY_ALL_CAMS()
			ENDIF*/
			
			//PRELOAD_ASCENT_SHOT()
			
			//ALLOW_PLAYER_SWITCH_ASCENT()
			
			eSwitchCamState = SWITCH_CAM_PLAYING_CODE_SWITCH
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_CODE_SWITCH
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_CODE_SWITCH")
			
//			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, DEFAULT, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO | SWITCH_FLAG_PAUSE_BEFORE_ASCENT | SWITCH_FLAG_SKIP_OUTRO))
//				RETURN FALSE
//
//			ELSE
//				eSwitchCamState = SWITCH_CAM_START_SPLINE2
//			ENDIF
			
			
			//IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_auto, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO | SWITCH_FLAG_PAUSE_BEFORE_ASCENT))
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_auto)//, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO | SWITCH_FLAG_PAUSE_BEFORE_ASCENT))
			
				IF selector_cam.bOKToSwitchPed
					IF NOT selector_cam.bPedSwitched

						IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped)
						
							switch get_current_player_ped_enum()
							
								case char_trevor
	
									if is_vehicle_driveable(trevors_current_car)
										
										if is_ped_sitting_in_vehicle(player_ped_id(), trevors_current_car)
										and is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], trevors_current_car)
										
											clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_franklin])
											SET_ENTITY_COORDS(selector_ped.pedID[selector_ped_franklin], <<-1683.4967, 4454.5952, 1.5660>>)
											SET_ENTITY_HEADING(selector_ped.pedID[selector_ped_franklin], 239.1512)
											
											clear_ped_tasks_immediately(chop.ped)
											SET_ENTITY_COORDS(chop.ped, <<-1679.8206, 4455.9878, 1.2653>>)
											SET_ENTITY_HEADING(chop.ped, 114.6932)
											set_ped_can_ragdoll(chop.ped, false)
	
										endif 
									endif
	
								break 
								
								case char_franklin
									
								break 
								
							endswitch 

							selector_cam.bPedSwitched = TRUE
	
						endif
						
					endif 

				endif 
				
				if not setup_trevors_truck_for_switch
					if IS_PLAYER_SWITCH_IN_PROGRESS()
//
						SWITCH_STATE camera_switch_state
						camera_switch_state = GET_PLAYER_SWITCH_STATE() 

						if camera_switch_state >= SWITCH_STATE_OUTRO_HOLD
						
							clear_area(get_entity_coords(trevors_truck.veh), 50.00, true)
							
							clear_area_of_vehicles(get_entity_coords(player_ped_id()), 200)

							if is_playback_going_on_for_vehicle(trevors_truck.veh)
												
								set_playback_speed(trevors_truck.veh, 1.0)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(trevors_truck.veh, (5000 - GET_TIME_POSITION_IN_RECORDING(trevors_truck.veh)))
								force_playback_recorded_vehicle_update(trevors_truck.veh)
								setup_trevors_truck_for_switch = true
										
							else 
							
								start_playback_recorded_vehicle(trevors_truck.veh, 002, "lkexile2")
								skip_time_in_playback_recorded_vehicle(trevors_truck.veh, 5000)
								force_playback_recorded_vehicle_update(trevors_truck.veh)
								setup_trevors_truck_for_switch = true
								
							
							endif 
							
							selector_cam.bSplineComplete = true

							RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_auto)
						
							eSwitchCamState = SWITCH_CAM_START_SPLINE2
							
						else 
						
							return false 
					
						endif 
						
					else 
					
						return false 
						
					endif
					
				else 
				
					return false 
					
				endif 
				
			endif 

		FALLTHRU
		
		CASE SWITCH_CAM_START_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE2")
			
			DESTROY_ALL_CAMS()			
	
			SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_TRUCK_OUTRO(thisSwitchCam2, trevors_truck.veh)
			
			set_player_control(player_id(), false)
			set_entity_visible(trevors_truck.veh, true)
			set_entity_visible(selector_ped.pedID[selector_ped_michael], true)
			set_entity_visible(player_ped_id(), true)
			
			CREATE_SPLINE_CAM(thisSwitchCam2)

			SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 

			bPlayerControlGiven = FALSE

			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE2
		
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2")
			
			IF IS_CAM_ACTIVE(thisSwitchCam2.ciSpline)
							
				IF iCurrentNode >= 1
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamPitchFranklinToTruck)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchFranklinToTruck)
				ENDIF	
				
				if not franklin_to_truck_outro_short_transition_in
					if GET_CAM_SPLINE_PHASE(thisSwitchCam2.ciSpline) >= 0.2
						franklin_to_truck_outro_short_transition_in = false	
					endif 
				endif 
				
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam2)
			
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam2.ciSpline) >= 1.0
				
					if get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person

						FILL_PUSH_IN_DATA(push_in_camera, trevors_truck.veh, char_trevor, 1.75, push_in_interp_time, push_in_cut_time, push_in_postfx_time, push_in_speed_up_time)
						SET_PUSH_IN_DIRECTION_MODIFIER(push_in_camera, <<5.0, 0.0, -5.0>>)   
						HANDLE_PUSH_IN(push_in_camera, true, true, true, false, false)
					
					endif 
				
					//eSwitchCamState = SWITCH_CAM_SHUTDOWN_SPLINE2
					
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
					
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
		//FALLTHRU
		break 
		
		CASE SWITCH_CAM_SHUTDOWN_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_SHUTDOWN_SPLINE2")
			
			IF DOES_CAM_EXIST(thisSwitchCam2.ciSpline)
				DESTROY_CAM(thisSwitchCam2.ciSpline)
				DESTROY_ALL_CAMS()
			ENDIF

			eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
		FALLTHRU
		
		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
		
			SET_TIME_SCALE(1.0)
		
			if get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person
			
				if HANDLE_PUSH_IN(push_in_camera, true, true, true, false, false)
		
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")

					SET_TIME_SCALE(1.0)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)

					IF DOES_CAM_EXIST(thisSwitchCam2.ciSpline)
						SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, FALSE)
						DESTROY_CAM(thisSwitchCam2.ciSpline)
					ENDIF
					DESTROY_ALL_CAMS()
			
					IF NOT bPlayerControlGiven
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DISPLAY_RADAR(TRUE)
						DISPLAY_HUD(TRUE)
						bPlayerControlGiven = TRUE
					ENDIF
					
//					if get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person
//						ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
//						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
//					endif 
					
					eSwitchCamState = SWITCH_CAM_IDLE

					RETURN TRUE
					
				endif 
				
			else 
			
				CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")

				RENDER_SCRIPT_CAMS(FALSE, FALSE)

				IF DOES_CAM_EXIST(thisSwitchCam2.ciSpline)
					SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, FALSE)
					DESTROY_CAM(thisSwitchCam2.ciSpline)
				ENDIF
				DESTROY_ALL_CAMS()
		
				IF NOT bPlayerControlGiven
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					bPlayerControlGiven = TRUE
				ENDIF
				
//				if get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person
//					ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
//					PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
//				endif 
				
				eSwitchCamState = SWITCH_CAM_IDLE

				RETURN TRUE
				
			endif 

		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC


PROC SETUP_SPLINE_CAM_NODE_ARRAY_SCOPE_TO_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX piPed1, PED_INDEX piPed2)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_SCOPE_TO_FRANKLIN")
	
	//spline duration = 3500
	
	IF NOT thisSwitchCam.bInitialized

		//--- Start of Cam Data ---
		if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
		
			thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[0].iNodeTime = 0
			thisSwitchCam.nodes[0].vNodePos = <<-1.8104, 1.2455, -0.3514>>
			thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[0].vNodeDir = <<0.4595, -0.4049, 0.7081>>
			thisSwitchCam.nodes[0].bPointAtEntity = TRUE
			thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = true
			thisSwitchCam.nodes[0].bAttachOffsetIsRelative = true
			thisSwitchCam.nodes[0].fNodeFOV = 41.0000
			thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[0].iNodeToClone = 0
			thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[0].iCamEaseType = 0
			thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[0].fTimeScale = 1.0000
			thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[0].bFlashEnabled = FALSE
			thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[0].fMinExposure = 0.0000
			thisSwitchCam.nodes[0].fMaxExposure = 0.0000
			thisSwitchCam.nodes[0].iRampUpDuration = 0
			thisSwitchCam.nodes[0].iRampDownDuration = 0
			thisSwitchCam.nodes[0].iHoldDuration = 0
			thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[1].iNodeTime = 1000
			thisSwitchCam.nodes[1].vNodePos = <<-1.8104, 1.2455, -0.7>>
			thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[1].vNodeDir = <<0.4595, -0.4049, 0.7081>>  
			thisSwitchCam.nodes[1].bPointAtEntity = TRUE
			thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = true
			thisSwitchCam.nodes[1].bAttachOffsetIsRelative = true
			thisSwitchCam.nodes[1].fNodeFOV = 41.0000
			thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[1].iNodeToClone = 0
			thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[1].iCamEaseType = 2
			thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
			thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[1].fTimeScale = 1.0000
			thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[1].bFlashEnabled = FALSE
			thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[1].fMinExposure = 0.0000
			thisSwitchCam.nodes[1].fMaxExposure = 0.0000
			thisSwitchCam.nodes[1].iRampUpDuration = 0
			thisSwitchCam.nodes[1].iRampDownDuration = 0
			thisSwitchCam.nodes[1].iHoldDuration = 0
			thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[2].iNodeTime = 600
			thisSwitchCam.nodes[2].vNodePos = <<-0.3160, 2.7700, -3.1371>>
			thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[2].vNodeDir = <<10.3624, 0.9979, -172.5415>>
			thisSwitchCam.nodes[2].bPointAtEntity = FALSE
			thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[2].fNodeFOV = 60.0000
			thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[2].iNodeToClone = 0
			thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[2].iCamEaseType = 0
			thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[2].fTimeScale = 1.0000
			thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[2].bFlashEnabled = FALSE
			thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[2].fMinExposure = 0.0000
			thisSwitchCam.nodes[2].fMaxExposure = 0.0000
			thisSwitchCam.nodes[2].iRampUpDuration = 0
			thisSwitchCam.nodes[2].iRampDownDuration = 0
			thisSwitchCam.nodes[2].iHoldDuration = 0
			thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

			thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_WORLD_POS
			thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[4].iNodeTime = 0
			thisSwitchCam.nodes[4].vNodePos = <<-1680.620972,4452.502930,4.8078>>
			thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[4].vNodeDir = <<4.134039,-0.000000,71.846382>>
			thisSwitchCam.nodes[4].bPointAtEntity = FALSE
			thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[4].fNodeFOV = 29.8
			thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[4].iNodeToClone = 0
			thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[4].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[4].iCamEaseType = 1
			thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
			thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[4].fTimeScale = 1.0000
			thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[4].bFlashEnabled = FALSE
			thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[4].fMinExposure = 0.0000
			thisSwitchCam.nodes[4].fMaxExposure = 0.0000
			thisSwitchCam.nodes[4].iRampUpDuration = 0
			thisSwitchCam.nodes[4].iRampDownDuration = 0
			thisSwitchCam.nodes[4].iHoldDuration = 0
			thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_WORLD_POS
			thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[5].iNodeTime = 600
			thisSwitchCam.nodes[5].vNodePos = <<-1680.620972,4452.502930,2.3>>
			thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[5].vNodeDir = <<4.134039,-0.000000,71.846382>>
			thisSwitchCam.nodes[5].bPointAtEntity = FALSE
			thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[5].fNodeFOV = 29.5
			thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[5].iNodeToClone = 0
			thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[5].iCamEaseType = 0
			thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[5].fTimeScale = 1.0000
			thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[5].bFlashEnabled = FALSE
			thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[5].fMinExposure = 0.0000
			thisSwitchCam.nodes[5].fMaxExposure = 0.0000
			thisSwitchCam.nodes[5].iRampUpDuration = 0
			thisSwitchCam.nodes[5].iRampDownDuration = 0
			thisSwitchCam.nodes[5].iHoldDuration = 0
			thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_WORLD_POS
			thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[6].iNodeTime = 850
			thisSwitchCam.nodes[6].vNodePos = <<-1680.620972,4452.502930,2.33>>
			thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[6].vNodeDir = <<4.134039,-0.000000,71.846382>>
			thisSwitchCam.nodes[6].bPointAtEntity = FALSE
			thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[6].fNodeFOV = 29.1
			thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[6].iNodeToClone = 0
			thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[6].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[6].iCamEaseType = 0
			thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[6].fTimeScale = 1.0000
			thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[6].bFlashEnabled = FALSE
			thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[6].fMinExposure = 0.0000
			thisSwitchCam.nodes[6].fMaxExposure = 0.0000
			thisSwitchCam.nodes[6].iRampUpDuration = 0
			thisSwitchCam.nodes[6].iRampDownDuration = 0
			thisSwitchCam.nodes[6].iHoldDuration = 0
			thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000
			
		else 
		
			thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[0].iNodeTime = 0
			thisSwitchCam.nodes[0].vNodePos = <<-1.9066, 2.8135, -0.0906>>
			thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[0].vNodeDir = <<-0.0185, -0.0210, 0.1023>>
			thisSwitchCam.nodes[0].bPointAtEntity = TRUE
			thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[0].fNodeFOV = 60.0000
			thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[0].iNodeToClone = 0
			thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[0].iCamEaseType = 0
			thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[0].fTimeScale = 1.0000
			thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[0].bFlashEnabled = FALSE
			thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[0].fMinExposure = 0.0000
			thisSwitchCam.nodes[0].fMaxExposure = 0.0000
			thisSwitchCam.nodes[0].iRampUpDuration = 0
			thisSwitchCam.nodes[0].iRampDownDuration = 0
			thisSwitchCam.nodes[0].iHoldDuration = 0
			thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[1].iNodeTime = 1000
			thisSwitchCam.nodes[1].vNodePos = <<-1.1243, 3.1880, -0.2969>>
			thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[1].vNodeDir = <<0.0483, 0.1020, 0.0414>>
			thisSwitchCam.nodes[1].bPointAtEntity = TRUE
			thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[1].fNodeFOV = 60.0000
			thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[1].iNodeToClone = 0
			thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[1].iCamEaseType = 2
			thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
			thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[1].fTimeScale = 1.0000
			thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[1].bFlashEnabled = FALSE
			thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[1].fMinExposure = 0.0000
			thisSwitchCam.nodes[1].fMaxExposure = 0.0000
			thisSwitchCam.nodes[1].iRampUpDuration = 0
			thisSwitchCam.nodes[1].iRampDownDuration = 0
			thisSwitchCam.nodes[1].iHoldDuration = 0
			thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
			thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[2].iNodeTime = 600
			thisSwitchCam.nodes[2].vNodePos = <<-0.3160, 2.7700, -3.1371>>
			thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[2].vNodeDir = <<10.3624, 0.9979, -172.5415>>
			thisSwitchCam.nodes[2].bPointAtEntity = FALSE
			thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[2].fNodeFOV = 60.0000
			thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[2].iNodeToClone = 0
			thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[2].iCamEaseType = 0
			thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[2].fTimeScale = 1.0000
			thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[2].bFlashEnabled = FALSE
			thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[2].fMinExposure = 0.0000
			thisSwitchCam.nodes[2].fMaxExposure = 0.0000
			thisSwitchCam.nodes[2].iRampUpDuration = 0
			thisSwitchCam.nodes[2].iRampDownDuration = 0
			thisSwitchCam.nodes[2].iHoldDuration = 0
			thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

			thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_WORLD_POS
			thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[4].iNodeTime = 0
			thisSwitchCam.nodes[4].vNodePos = <<-1680.9720, 4453.0132, 4.8078>>
			thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[4].vNodeDir = <<6.0105, 0.0000, 85.0164>>
			thisSwitchCam.nodes[4].bPointAtEntity = FALSE
			thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[4].fNodeFOV = 40.0000
			thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[4].iNodeToClone = 0
			thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[4].fNodeMotionBlur = 1.0000
			thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[4].iCamEaseType = 1
			thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
			thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[4].fTimeScale = 1.0000
			thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[4].bFlashEnabled = FALSE
			thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[4].fMinExposure = 0.0000
			thisSwitchCam.nodes[4].fMaxExposure = 0.0000
			thisSwitchCam.nodes[4].iRampUpDuration = 0
			thisSwitchCam.nodes[4].iRampDownDuration = 0
			thisSwitchCam.nodes[4].iHoldDuration = 0
			thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_WORLD_POS
			thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[5].iNodeTime = 600
			thisSwitchCam.nodes[5].vNodePos = <<-1681.2314, 4453.0356, 2.3345>>
			thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[5].vNodeDir = <<6.0105, 0.0000, 85.0164>>
			thisSwitchCam.nodes[5].bPointAtEntity = FALSE
			thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[5].fNodeFOV = 40.0000
			thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[5].iNodeToClone = 0
			thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[5].iCamEaseType = 0
			thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[5].fTimeScale = 1.0000
			thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[5].bFlashEnabled = FALSE
			thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[5].fMinExposure = 0.0000
			thisSwitchCam.nodes[5].fMaxExposure = 0.0000
			thisSwitchCam.nodes[5].iRampUpDuration = 0
			thisSwitchCam.nodes[5].iRampDownDuration = 0
			thisSwitchCam.nodes[5].iHoldDuration = 0
			thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

			thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_WORLD_POS
			thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
			thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
			thisSwitchCam.nodes[6].iNodeTime = 850
			thisSwitchCam.nodes[6].vNodePos = <<-1681.2314, 4453.0356, 2.3345>>
			thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
			thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
			thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
			thisSwitchCam.nodes[6].vNodeDir = <<6.0105, 0.0000, 85.0164>>
			thisSwitchCam.nodes[6].bPointAtEntity = FALSE
			thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = FALSE
			thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
			thisSwitchCam.nodes[6].fNodeFOV = 40.0000
			thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
			thisSwitchCam.nodes[6].iNodeToClone = 0
			thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
			thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
			thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
			thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
			thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
			thisSwitchCam.nodes[6].fNodeCamShake = 0.0000
			thisSwitchCam.nodes[6].iCamEaseType = 0
			thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
			thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
			thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
			thisSwitchCam.nodes[6].fTimeScale = 1.0000
			thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
			thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
			thisSwitchCam.nodes[6].bFlashEnabled = FALSE
			thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
			thisSwitchCam.nodes[6].fMinExposure = 0.0000
			thisSwitchCam.nodes[6].fMaxExposure = 0.0000
			thisSwitchCam.nodes[6].iRampUpDuration = 0
			thisSwitchCam.nodes[6].iRampDownDuration = 0
			thisSwitchCam.nodes[6].iHoldDuration = 0
			thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
			thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
			thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
			thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
			thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
			thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000
		endif 


		thisSwitchCam.iNumNodes = 7
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.fSwitchSoundAudioStartPhase = -1.0000
		thisSwitchCam.fSwitchSoundAudioEndPhase = -1.0000
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = TRUE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Exile2_ScopeToFranklin.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_Exile2_ScopeToFranklin.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piPed1
	thisSwitchCam.piPeds[1] = piPed2
	
ENDPROC

FUNC BOOL HANDLE_SWITCH_CAM_SCOPE_TO_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam)
	//CDEBUG3LN(DEBUG_MISSION, "HANDLE_SWITCH_CAM_SCOPE_TO_FRANKLIN")
	
	INT iCurrentNode
	FLOAT fCamPhase
	
	printstring("enum number = ")
	printint(enum_to_int(eSwitchCamState))
	printnl()
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_START_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE1")
			
			//DESTROY_ALL_CAMS()	
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			SETUP_SPLINE_CAM_NODE_ARRAY_SCOPE_TO_FRANKLIN(thisSwitchCam, PLAYER_PED_ID(), selector_ped.pedID[SELECTOR_PED_FRANKLIN])
			CREATE_SPLINE_CAM(thisSwitchCam)

			SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			PED_INDEX piFranklin
			IF DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				piFranklin = selector_ped.pedID[SELECTOR_PED_FRANKLIN]
			ELSE
				piFranklin = PLAYER_PED_ID()
			ENDIF
			IF DOES_ENTITY_EXIST(piFranklin)
				IF NOT IS_ENTITY_DEAD(piFranklin)
					GIVE_WEAPON_TO_PED(piFranklin, WEAPONTYPE_UNARMED, 0, TRUE)
					SET_CURRENT_PED_WEAPON(piFranklin, WEAPONTYPE_UNARMED, TRUE)
					SET_PED_USING_ACTION_MODE(piFranklin, FALSE)
				ENDIF
			ENDIF
			
			vAttachMichaelToHeliOffset = vAttachMichaelToHeliOffset
			vAttachMichaelToHeliRot = vAttachMichaelToHeliRot
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND DOES_ENTITY_EXIST(helicopter.veh)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(helicopter.veh)
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
					IF IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED_ID(), helicopter.veh)
						DETACH_ENTITY(PLAYER_PED_ID())
					ENDIF
					ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), helicopter.veh, GET_ENTITY_BONE_INDEX_BY_NAME(helicopter.veh, "seat_dside_r"), vAttachMichaelToHeliOffset, vAttachMichaelToHeliRot)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_PLAY_ANIM(PLAYER_PED_ID(), strAnimDict, "ex02_heli_sniper_idle_michael", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, AIK_DISABLE_HEAD_IK)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				ENDIF
			ENDIF

			bPlayerControlGiven = FALSE

			bOkToSwitchPed = FALSE
			bSyncSceneStarted = FALSE
			bSyncSceneStopped = FALSE
			bPlayerControlOff = TRUE
			bCanStartChop = TRUE
			bCanStartChopOverrideSpeed = TRUE
			bWooshStarted = FALSE
			bWooshStopped = FALSE
			bScopeToFrankFX1Started = FALSE
			bScopeToFrankFX2Started = FALSE
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
			
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
					
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam)
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline)
				
				printstring("CamPhase = ") 
				printfloat(fCamPhase)
				printnl()
				
				IF iCurrentNode >= 1
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeadingScopeToFranklin)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchScopeToFranklin)
				ENDIF
				
				IF NOT bOkToSwitchPed
					IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode
						selector_cam.bOKToSwitchPed = TRUE
						selector_cam.bPedSwitched = FALSE
						bOkToSwitchPed = TRUE
					ENDIF
				ENDIF
			
				IF NOT bSyncSceneStarted
					IF fCamPhase >= fSyncSceneStartPhase 
					
						//PED_INDEX piFranklin
						IF DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
							piFranklin = selector_ped.pedID[SELECTOR_PED_FRANKLIN]
						ELSE
							piFranklin = PLAYER_PED_ID()
						ENDIF
						
						IF DOES_ENTITY_EXIST(piFranklin)
						AND DOES_ENTITY_EXIST(chop.ped)
							IF NOT IS_ENTITY_DEAD(piFranklin)
							AND NOT IS_ENTITY_DEAD(chop.ped)
								CDEBUG3LN(DEBUG_MISSION, "Starting Sync Scene...")
								CLEAR_PED_TASKS(piFranklin)
								CLEAR_PED_TASKS(chop.ped)
								iSyncedScene = CREATE_SYNCHRONIZED_SCENE(vSyncScenePos, vSyncSceneRot)
								TASK_SYNCHRONIZED_SCENE(piFranklin, iSyncedScene, strAnimDict, "ex02_heli_switch_chop_run_franklin", INSTANT_BLEND_IN, fFranklinBlendoutDelta)//, SYNCED_SCENE_DONT_INTERRUPT)
								TASK_SYNCHRONIZED_SCENE(chop.ped, iSyncedScene, strAnimDict, "ex02_heli_switch_chop_run_chop", INSTANT_BLEND_IN, fChopBlendoutDelta, SYNCED_SCENE_TAG_SYNC_OUT)// | SYNCED_SCENE_DONT_INTERRUPT)
								SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncedScene, FALSE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(piFranklin)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(chop.ped)
							ENDIF
						ENDIF
		
						bSyncSceneStarted = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSyncSceneStopped
					IF fCamPhase >= fSyncSceneStopPhase 
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedScene)
							//DETACH_SYNCHRONIZED_SCENE(iSyncedScene)
							bSyncSceneStopped = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				if not scope_to_franklin_hit_out_0
					if fCamPhase >= fScopeToFrankFX1StartPhase
						PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 
						switch_sound_time = get_game_timer()
						scope_to_franklin_hit_out_0 = true 
					endif 
					
				else 
				
					if not scope_to_franklin_transition_0
					
						if lk_timer(switch_sound_time, 200)
						
							iWooshSoundID = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(iWooshSoundID, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
	
							scope_to_franklin_transition_0 = true 
						endif 
					
					endif 
					
				endif 
				
				//added to fix unreferenced variable compile.
				fWooshStartPhase = fWooshStartPhase
				bwooshstopped = bwooshstopped
				bwooshstarted = bwooshstarted
				fwooshstartphase = fwooshstartphase
				fwooshstopphase = fwooshstopphase

//				IF NOT bWooshStarted
//					IF fCamPhase > fWooshStartPhase
//						iWooshSoundID = GET_SOUND_ID()
//						//PLAY_SOUND_FRONTEND(iWooshSoundID, "In", "SHORT_PLAYER_SWITCH_SOUND_SET")
//						PLAY_SOUND_FRONTEND(iWooshSoundID, "Short_Transition_In", "SHORT_PLAYER_SWITCH_SOUND_SET") 
//						bWooshStarted = TRUE
//					ENDIF
//				ENDIF
				
//				IF NOT bWooshStopped
//					IF fCamPhase > fWooshStopPhase
//						STOP_SOUND(iWooshSoundID)
//						bWooshStopped = TRUE
//					ENDIF
//				ENDIF
				
				

				IF NOT bScopeToFrankFX1Started
					IF fCamPhase >= fScopeToFrankFX1StartPhase
						CDEBUG3LN(DEBUG_MISSION, "Scope to Frank FX1 Started...")
						ANIMPOSTFX_PLAY("SwitchShortMichaelIn", 0, FALSE)
						bScopeToFrankFX1Started = TRUE
					ENDIF
				ENDIF
				
				
				fScopeToFrankFX2StartPhase = fScopeToFrankFX2StartPhase
				
				IF NOT bScopeToFrankFX2Started
					IF fCamPhase >= 0.5//fScopeToFrankFX2StartPhase
						
						CDEBUG3LN(DEBUG_MISSION, "Scope to Frank FX2 Started...")
						
						ANIMPOSTFX_PLAY("SwitchShortFranklinMid", 0, FALSE)
						
						if not has_sound_finished(iWooshSoundID)
							stop_sound(iWooshSoundID) 
						endif 

						PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 
						
						bScopeToFrankFX2Started = TRUE
					ENDIF
				ENDIF
				
				if not scope_to_franklin_hit_out_1
					IF fCamPhase >= 0.85
						
						scope_to_franklin_hit_out_1 = true 
					endif 
				endif 
				
				if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
							
					if fCamPhase >= 0.9
					
						FILL_PUSH_IN_DATA(push_in_camera, player_ped_id(), char_franklin, 1.0, push_in_interp_time, push_in_cut_time, push_in_postfx_time, push_in_speed_up_time) 
						SET_PUSH_IN_DIRECTION_MODIFIER(push_in_camera, <<25.0, 0.0, 0.0>>)   
						HANDLE_PUSH_IN(push_in_camera, false, true, true, false, false)
					
						eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
						
					endif 
					
				else 
				
					IF fCamPhase >= 1.0
				
						eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
						
					endif 

				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
		break 
		
		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
		
			if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
				
				if HANDLE_PUSH_IN(push_in_camera, false, true, true, false, false)
		
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")

					STOP_SOUND(iWooshSoundId)
					
					IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
						SET_CAM_ACTIVE(thisSwitchCam.ciSpline, FALSE)
						DESTROY_CAM(thisSwitchCam.ciSpline)
					ENDIF

					SET_TIME_SCALE(1.0)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)

					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeadingScopeToFranklin)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchScopeToFranklin)
					
					original_time = get_game_timer()

					eSwitchCamState = SWITCH_CAM_HANDLE_POST_SCENE_TASKS
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_HANDLE_POST_SCENE_TASKS")

				endif 
				
			else 
			
				CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")

				STOP_SOUND(iWooshSoundId)

				SET_TIME_SCALE(1.0)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)

				IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
					SET_CAM_ACTIVE(thisSwitchCam.ciSpline, FALSE)
					DESTROY_CAM(thisSwitchCam.ciSpline)
				ENDIF
		
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeadingScopeToFranklin)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchScopeToFranklin)
				
				original_time = get_game_timer()

				eSwitchCamState = SWITCH_CAM_HANDLE_POST_SCENE_TASKS
				CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_HANDLE_POST_SCENE_TASKS")
				
			endif 
		
		break
		//fallthru
			
		CASE SWITCH_CAM_HANDLE_POST_SCENE_TASKS
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_HANDLE_POST_SCENE_TASKS")

			IF bCanStartChop
				
				if lk_timer(original_time, 700) //800
				//or ((IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedScene)) and (GET_SYNCHRONIZED_SCENE_PHASE(iSyncedScene) >= 0.84))
				or get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person //chops synced scene is almost finished by the time the push in is done so no need to wait. Give chop the ai immediately.
				
					chop_ai_system_status = 200
					chop_ai_system()
					
//					SET_PED_MIN_MOVE_BLEND_RATIO(chop.ped, PEDMOVE_SPRINT)
//					FORCE_PED_MOTION_STATE(chop.ped, MS_ON_FOOT_SPRINT, TRUE)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(chop.ped)
						
					bCanStartChop = FALSE

				ENDIF
				
			ENDIF
			
			IF bCanStartChopOverrideSpeed
				IF DOES_ENTITY_EXIST(chop.ped)
					IF NOT IS_ENTITY_DEAD(chop.ped)
						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(chop.ped)
							WAYPOINT_PLAYBACK_OVERRIDE_SPEED(chop.ped, 3.0)
							bCanStartChopOverrideSpeed = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bPlayerControlOff
				IF TIMERA() > iPlayerControlDelay
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					bPlayerControlOff = FALSE
				ENDIF
			ENDIF
			
			IF bPlayerControlOff
				printstring("bPlayerControlOff TRUE")
				printnl()
			else 
				printstring("bPlayerControlOff FALSE")
				printnl()
			endif 
			
			IF bCanStartChop
				printstring("bCanStartChop TRUE")
				printnl()
			else 
				printstring("bCanStartChop FALSE")
				printnl()
			endif 
			
			IF bCanStartChopOverrideSpeed
				printstring("bCanStartChopOverrideSpeed TRUE")
				printnl()
			else 
				printstring("bCanStartChopOverrideSpeed FALSE")
				printnl()
			endif 

			
			IF bPlayerControlOff = FALSE
			AND bCanStartChop = FALSE
			AND bCanStartChopOverrideSpeed = FALSE	
				eSwitchCamState = SWITCH_CAM_IDLE
				RETURN TRUE
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC


PROC SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_SCOPE(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX piPed1, PED_INDEX piPed2)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_SCOPE")
	
	IF NOT thisSwitchCam.bInitialized

		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-4.8562, 3.4375, -0.9110>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].vNodeDir = <<9.9831, 0.0000, -115.1286>>
		thisSwitchCam.nodes[0].bPointAtEntity = FALSE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].fNodeFOV = 30.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 6500
		thisSwitchCam.nodes[1].vNodePos = <<-2.4559, 1.7065, -0.4637>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].vNodeDir = <<15.4501, -0.0000, -116.3971>>
		thisSwitchCam.nodes[1].bPointAtEntity = FALSE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].fNodeFOV = 30.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 200.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 50
		thisSwitchCam.nodes[1].iRampDownDuration = 50
		thisSwitchCam.nodes[1].iHoldDuration = 100

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 500
		thisSwitchCam.nodes[2].vNodePos = <<-12.2993, 5.9326, -4.4567>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].vNodeDir = <<-0.1788, -0.1712, -0.0147>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].fNodeFOV = 30.0000
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 200.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 50
		thisSwitchCam.nodes[2].iRampDownDuration = 50
		thisSwitchCam.nodes[2].iHoldDuration = 100

		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[3].iNodeTime = 200
		thisSwitchCam.nodes[3].vNodePos = <<-9.7510, 4.4595, -3.9479>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].vNodeDir = <<-0.0816, -0.1602, -0.1601>>
		thisSwitchCam.nodes[3].bPointAtEntity = TRUE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].fNodeFOV = 30.0000
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[3].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 0
		thisSwitchCam.nodes[3].iRampDownDuration = 0
		thisSwitchCam.nodes[3].iHoldDuration = 0

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 1300
		thisSwitchCam.nodes[4].vNodePos = <<-6.2438, 3.0615, -2.2728>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].vNodeDir = <<-0.0207, -0.1847, 0.0885>>
		thisSwitchCam.nodes[4].bPointAtEntity = TRUE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 30.0000
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[4].iCamEaseType = 0
		thisSwitchCam.nodes[4].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 100
		thisSwitchCam.nodes[5].vNodePos = <<-4.8359, 2.6328, -2.0177>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].vNodeDir = <<0.4477, -0.0730, -0.3307>>
		thisSwitchCam.nodes[5].bPointAtEntity = TRUE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 30.0000
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = TRUE
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 200.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 50
		thisSwitchCam.nodes[5].iRampDownDuration = 50
		thisSwitchCam.nodes[5].iHoldDuration = 100

		thisSwitchCam.iNumNodes = 6
		thisSwitchCam.iCamSwitchFocusNode = 2
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Exile2_FranklinToScope.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_Exile2_FranklinToScope.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piPed1
	thisSwitchCam.piPeds[1] = piPed2
	
ENDPROC

FUNC BOOL HANDLE_SWITCH_CAM_FRANKLIN_TO_SCOPE(SWITCH_CAM_STRUCT &thisSwitchCam)
	CDEBUG3LN(DEBUG_MISSION, "HANDLE_SWITCH_CAM_FRANKLIN_TO_SCOPE")
	
	INT iCurrentNode
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_START_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE1")
			
			DESTROY_ALL_CAMS()			
			
			SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_SCOPE(thisSwitchCam, selector_ped.pedID[SELECTOR_PED_FRANKLIN], PLAYER_PED_ID())
			CREATE_SPLINE_CAM(thisSwitchCam)

			SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			/*IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND DOES_ENTITY_EXIST(helicopter.veh)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(helicopter.veh)
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
					ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), helicopter.veh, GET_ENTITY_BONE_INDEX_BY_NAME(helicopter.veh, "seat_dside_r"), vAttachMichaelToHeliOffset, vAttachMichaelToHeliRot)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_PLAY_ANIM(PLAYER_PED_ID(), strAnimDict, "ex02_heli_sniper_idle_michael", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, AIK_DISABLE_HEAD_IK)
				ENDIF
			ENDIF*/
					
			//woosh in
			//iWooshInSoundId = GET_SOUND_ID()
			//PLAY_SOUND_FRONTEND(iWooshInSoundId , "Out", "SHORT_PLAYER_SWITCH_SOUND_SET")
			
			bMichaelSnipeAnimStarted = FALSE
			bPlayerControlGiven = FALSE

			SETTIMERA(0)
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
			
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
							
				IF iCurrentNode >= 1
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeadingFranklinToScope)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchFranklinToScope)
				ENDIF	
				
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam)

				#IF IS_DEBUG_BUILD
					IF bAttachMichaelToHeli
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						AND DOES_ENTITY_EXIST(helicopter.veh)
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							AND NOT IS_ENTITY_DEAD(helicopter.veh)
								SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
								ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), helicopter.veh, GET_ENTITY_BONE_INDEX_BY_NAME(helicopter.veh, "seat_dside_r"), vAttachMichaelToHeliOffset, vAttachMichaelToHeliRot)
							ENDIF
						ENDIF
						bAttachMichaelToHeli = FALSE
					ENDIF
				#ENDIF
				IF bMichaelSnipeAnimStarted = FALSE
				AND iCurrentNode >= 2
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					AND DOES_ENTITY_EXIST(helicopter.veh)
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						AND NOT IS_ENTITY_DEAD(helicopter.veh)
							TASK_PLAY_ANIM(PLAYER_PED_ID(), strAnimDict, "ex02_heli_sniper_idle_michael", SLOW_BLEND_IN, INSTANT_BLEND_OUT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, AIK_DISABLE_HEAD_IK)
							bMichaelSnipeAnimStarted = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 1.0
					eSwitchCamState = SWITCH_CAM_START_CODE_SWITCH
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
		FALLTHRU
		
		CASE SWITCH_CAM_START_CODE_SWITCH
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_CODE_SWITCH")
			
			IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
				DESTROY_CAM(thisSwitchCam.ciSpline)
				DESTROY_ALL_CAMS()
			ELSE
				RETURN FALSE
			ENDIF

			eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
		FALLTHRU
		
		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")

			SET_TIME_SCALE(1.0)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)

			IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
				SET_CAM_ACTIVE(thisSwitchCam.ciSpline, FALSE)
				DESTROY_CAM(thisSwitchCam.ciSpline)
			ENDIF
			DESTROY_ALL_CAMS()
	
			/*IF NOT bPlayerControlGiven
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				bPlayerControlGiven = TRUE
			ENDIF*/

			//forces the sniper hud to render immediately
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND DOES_ENTITY_EXIST(helicopter.veh)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(helicopter.veh)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), TRUE)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
					
					GIVE_WEAPON_TO_PED(player_ped_id(), WEAPONTYPE_HEAVYSNIPER, INFINITE_AMMO, TRUE, TRUE)//1000 infinite_ammo
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, WEAPONCOMPONENT_AT_SCOPE_MAX)
					SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), FALSE)
					
					SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "move_ped_crouched")
					SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "Wpn_AssaultRifle_WeaponHoldingCrouched")
					SET_PED_STRAFE_CLIPSET(PLAYER_PED_ID(), "move_ped_crouched_strafing")
					
					SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_InstantBlendToAim, TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				ENDIF
			ENDIF
			
			eSwitchCamState = SWITCH_CAM_IDLE

			RETURN TRUE

		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC


//*****************************************LOCAL FUNCTIONS ****************************************************** 



/*
PROC DW_PRINTS(STRING S)
	IF bDwUseDebug
		PRINTSTRING(S)
		PRINTNL()
		PRINTNL()
	ENDIF
ENDPROC

//PURPOSE: Debug print int
PROC DW_PRINTI(INT i, STRING S)
	IF bDwUseDebug
		PRINTSTRING(S)
		PRINTINT(i)
		PRINTNL()
		PRINTNL()
	ENDIF
ENDPROC

//PURPOSE: Debug print float
PROC DW_PRINTF(FLOAT f, STRING S)
	IF bDwUseDebug
		PRINTSTRING(S)
		PRINTFLOAT(f)
		PRINTNL()
		PRINTNL()
	ENDIF
ENDPROC
*/




PROC PRINT_GOD_TEXT(STRING strMessage, INT iTimeToDisplay = DEFAULT_GOD_TEXT_TIME, BOOl bSetAsTriggered = TRUE)
	IF NOT IS_STRING_NULL(strMessage)
		IF NOT ARE_STRINGS_EQUAL(strMessage, "")
			PRINT_NOW(strMessage, iTimeToDisplay, 1)
			IF bSetAsTriggered 
				SET_LABEL_AS_TRIGGERED(strMessage, TRUE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &veh, FLOAT f_time)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh, f_time - GET_TIME_POSITION_IN_RECORDING(veh))
		ENDIF
	ENDIF
ENDPROC

// PURPOSE: Returns True if the specified time has passed 
FUNC BOOL HAS_DW_TIME_PASSED(INT iStartTimer, INT iTimeToCheck)

	// INPUT PARAMS:	iStartTimer			This is considered time = 0
	//					iTimeToCheck	 	The time since iStartTimer being tested for
	// RETURN VALUE		BOOL				TRUE if time iTimeToCheck  has passed since time iStartTimer 
	
	// NOTE: Requires iStartTimer to have been set before the fn is called
	
	INT iTempTime
	IF iStartTimer > 0	
		iTempTime = GET_GAME_TIMER()
		IF iTempTime - iStartTimer > iTimeToCheck
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_ROTATION_FROM_DIRECTION_VECTOR(VECTOR vDir)
	RETURN (<<ASIN(vDir.z), 0.0, ATAN2(-vDir.x, vDir.y)>>)
ENDFUNC

FUNC INT GET_ALL_VEHICLE_SEARCH_FLAGS()
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING)
ENDFUNC


PROC MAINTAIN_CHOPPER_HOVER(vehicle_index heli, float iMinRecTime, float iMaxRecTime, float fHoverPlaybackSpeed = 1.0)
	
	IF IS_VEHICLE_DRIVEABLE(heli)
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(heli)
		
			IF NOT bPlayingHoverBackwards
				
				IF GET_TIME_POSITION_IN_RECORDING(heli) >= iMaxRecTime //31000
				
					bPlayingHoverBackwards = TRUE
	
				ENDIF
			ELSE
				
				IF GET_TIME_POSITION_IN_RECORDING(heli) <= iMinRecTime //5000

					bPlayingHoverBackwards = FALSE
					
				ENDIF
			ENDIF
			
			if bPlayingHoverBackwards
				fHoverPlaybackSpeed *= -1
			else 
				fHoverPlaybackSpeed *= 1
			endif 
			
			SET_PLAYBACK_SPEED(heli, fHoverPlaybackSpeed)
			
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PED_PROCESSED_SEQ(PED_INDEX this_ped, BOOL CHECK_FOR_FINISHED = TRUE, INT this_prg = 0)
	SCRIPTTASKSTATUS task_status
	INT seq_prog
	IF NOT IS_ENTITY_DEAD(this_ped)
		task_status = GET_SCRIPT_TASK_STATUS (this_ped, SCRIPT_TASK_PERFORM_SEQUENCE)
		IF CHECK_FOR_FINISHED
			IF task_status = FINISHED_TASK
				RETURN TRUE
			ELSE
			//	DW_PRINTI(ENUM_TO_INT(task_status), "task_status....") 
				RETURN FALSE
			ENDIF
		ELSE
			IF task_status = PERFORMING_TASK
				seq_prog = GET_SEQUENCE_PROGRESS(this_ped)

				IF seq_prog > this_prg
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

proc switch_off_vehicle_generators_for_mission()
	
	//trevors trailer
	set_all_vehicle_generators_active_in_area(<<1965.07, 3784.35, 100.00>>, <<1994.62, 3841.22, -100.00>>, false)
	
	//heli pad at airport 
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1784.3, 3195.5, 100.00>>, <<1707.6, 3262.2, -100.00>>, false)
	
	//vehicle gens on dirt path
	//car at these coords -1622.1, 4381.9, 3.4
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1635.92, 4389.35, 100.00>>, <<-1603.12, 4371.75, -100.00>>, false)
	
	//car at coords = -1538.2, 4312.4, 6.5
	set_all_vehicle_generators_active_in_area(<<-1517.4, 4319.4, 103.00>>, <<-1569.5, 4309.2, -100.00>>, false) //vehicle on path
	
	//near the rpg enemy 
	set_all_vehicle_generators_active_in_area(<<-1602.200, 4452.300, 50.000>>, <<-1486.800, 4519.200, 0.00>>, false) 
	
	//near to the far right of the rpg enemy 
	set_all_vehicle_generators_active_in_area(<<-1320.300, 4424.900, 30.00 >>, <<-1409.000, 4556.000, 0.00>>, false) 

	
endproc 

func bool has_gang_member_been_found_by_sniper_scope(ped_index miss_ped, float fov_value = 10.00)

	IF NOT IS_PED_INJURED(miss_ped)
	
		if not (chop_cam.state = CHOP_VIEW_CAM_ON_CHOP)
		and not is_player_switch_in_progress()
		
//			printstring("sniper test 0")
//			printnl()
//			printstring("sniper test 0")
//			printnl()
//			printstring("sniper test 0")
//			printnl()
//			printstring("sniper test 0")
//			printnl()
//			printstring("sniper test 0")
//			printnl()
//			
//			FLOAT x = 0.0
//			FLOAT y = 0.0
//			GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(miss_ped), x, y)
//			printfloat(x)
//			printnl()
//			printfloat(y)
//			printnl()

//			printstring("GET_GAMEPLAY_CAM_FOV() = ")
//			printfloat(GET_GAMEPLAY_CAM_FOV())
//			printnl()
			
		
			IF IS_ENTITY_ON_SCREEN(miss_ped)

				if not render_sniper_hud //when SET_PLAYER_FORCED_AIM it forces the sniper to aim. This sets the gamplay cam to sniper view which was being set at 5 fov. This will make the gamplay_cam_fov condition return true. However 
										 //sniper hud takes a frame to update the fov meaning get_screen_coord_from_world_coord() returns normal spline cam view
					
					if miss_ped != gang_enemy[3].ped
					
						IF GET_GAMEPLAY_CAM_FOV() < fov_value //10
							
							FLOAT f_x = 0.0
							FLOAT f_y = 0.0
							GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(miss_ped), f_x, f_y)
							
						
							IF ABSF(f_x - 0.5) < 0.1 //0.1
								
								IF ABSF(f_y - 0.5) < 0.1 //0.1
									
									return true 
									
								ENDIF
							ENDIF
						ENDIF
						
					else 
					
						IF GET_GAMEPLAY_CAM_FOV() < 25.8 //12.0
							
							FLOAT f_x = 0.0
							FLOAT f_y = 0.0
							GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(miss_ped), f_x, f_y)
							
						
							IF ABSF(f_x - 0.5) < 0.225 //0.12
								
								IF ABSF(f_y - 0.5) < 0.225 //0.12
									
									return true 
									
								ENDIF
							ENDIF
						ENDIF
					
					endif 
					
				endif 
				
			ENDIF
			
		endif 

	endif 
	
	return false 
	
endfunc 

func bool okay_for_vehicle_to_drive_off_waiting_on_ped(ped_index &miss_Ped)
				
	if not is_ped_injured(miss_ped)
		if not is_ped_sitting_in_vehicle(miss_ped, ambient_car.veh)
			if not IS_PED_GETTING_INTO_A_VEHICLE(miss_ped)

				return true
				
			endif 
			
		else 
			
			return true 
		
		endif 
	else
	
		return true 
		
	endif 
	
	return false 
	
endfunc 


PROC manage_gang_enemy_0()
	
	IF NOT IS_PED_INJURED(gang_enemy[0].ped)
	
		if manage_gang_enemy_0_status != 22
			if GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[0].ped)) < 15.00
			or ((get_current_player_ped_enum() = char_franklin) and CAN_PED_SEE_HATED_PED(gang_enemy[0].ped, PLAYER_PED_ID()) and (GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[0].ped)) < 15.00)) 
			or has_entity_been_damaged_by_entity(gang_enemy[0].ped, PLAYER_PED_ID())
			
				if not does_blip_exist(gang_enemy[0].blip)
					gang_enemy[0].blip = create_blip_for_ped(gang_enemy[0].ped, true)
				endif 

				set_blocking_of_non_temporary_events(gang_enemy[0].ped, false)
				set_ped_sphere_defensive_area(gang_enemy[0].ped, <<-1492.5629, 4479.9956, 17.2309>>, 10.0)
				task_combat_ped(gang_enemy[0].ped, player_ped_id())
				
				manage_gang_enemy_0_status = 22
				
			endif 
		endif 
	
		SWITCH manage_gang_enemy_0_status
		
			//guy in the front right of forrest
			
			case 0
			
				SET_PED_STEALTH_MOVEMENT(gang_enemy[0].ped, true, "DEFAULT_ACTION")
			
				open_sequence_task(seq)
					task_follow_nav_mesh_to_coord(null, <<-1523.34, 4454.33, 10.43>>, pedmove_walk, -1)
					task_aim_gun_at_entity(null, helicopter.veh, 2000)
					task_follow_nav_mesh_to_coord(null, <<-1517.54, 4452.38, 9.24>>, pedmove_walk, -1)
					task_aim_gun_at_entity(null, helicopter.veh, 2000)
					task_follow_nav_mesh_to_coord(null, <<-1511.87, 4440.37, 11.60>>, pedmove_walk, -1)
					set_sequence_to_repeat(seq, repeat_forever)
				close_sequence_task(seq)
				task_perform_sequence(gang_enemy[0].ped, seq)
				clear_sequence_task(seq)
			
				manage_gang_enemy_0_status++

			break 
		
			CASE 1

				if has_gang_member_been_found_by_sniper_scope(gang_enemy[0].ped)
				
					gang_enemy[0].blip = CREATE_BLIP_FOR_ENTITY(gang_enemy[0].ped, TRUE)
			 
					create_conversation(scripted_speech, "TM44AUD", "TM44_FOUND", CONV_PRIORITY_MEDIUM)

			 		open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, <<-1565.04, 4560.87, 16.76>>, pedmove_walk, -1, -1, enav_no_stopping)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_2", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
					close_sequence_task(seq)
					task_perform_sequence(gang_enemy[0].ped, seq)
					clear_sequence_task(seq)
					
					current_seq_pos_0 = 0

					manage_gang_enemy_0_status++
				endif 
				
				//fix issue with fov 
				if has_gang_member_been_found_by_sniper_scope(gang_enemy[0].ped, 15.0) //add larger fov to command.
					if is_bullet_in_area(get_entity_coords(gang_enemy[0].ped), 4.0)
						
						open_sequence_task(seq)
							task_play_anim(null, "missexile2", "enter_crouch_a", normal_blend_in, normal_blend_out, -1, AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
							task_play_anim(null, "missexile2", "crouching_idle_a", normal_blend_in, normal_blend_out, -1, AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
						close_sequence_task(seq)
						task_perform_sequence(gang_enemy[0].ped, seq)
						clear_sequence_task(seq)
						
						manage_gang_enemy_0_status = 3
					endif 
				endif

			BREAK
			
			case 2
			
				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(gang_enemy[0].ped)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(gang_enemy[0].ped, 2.0) 
				endif 

				if not is_entity_at_coord(gang_enemy[0].ped, <<-1567.9077, 4731.3516, 49.3402>>, <<1.5, 1.5, 2.0>>)
					
					if has_ped_task_finished_2(gang_enemy[0].ped, SCRIPT_TASK_PERFORM_SEQUENCE_FROM_PROGRESS)
					and has_ped_task_finished_2(gang_enemy[0].ped)
					
						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<-1565.04, 4560.87, 16.76>>, pedmove_walk, -1, -1, enav_no_stopping)
							TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_2", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
						close_sequence_task(seq)
						TASK_PERFORM_SEQUENCE_FROM_PROGRESS(gang_enemy[0].ped, seq, current_seq_pos_0, 1)
						clear_sequence_task(seq)

					else 
					
						//if get_script_task_status(gang_enemy[0].ped) = performing_task
							current_seq_pos_0 = get_sequence_progress(gang_enemy[0].ped)
						//endif 

					endif
					
					if is_bullet_in_area(get_entity_coords(gang_enemy[0].ped), 4.0)
					
						open_sequence_task(seq)
							task_play_anim(null, "missexile2", "enter_crouch_a", normal_blend_in, normal_blend_out, -1, AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
							task_play_anim(null, "missexile2", "crouching_idle_a", normal_blend_in, normal_blend_out, -1, AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
						close_sequence_task(seq)
						task_perform_sequence(gang_enemy[0].ped, seq)
						clear_sequence_task(seq)
						
						manage_gang_enemy_0_status = 3
					endif 
				
				else 
				
					//get in vehicle 
			
					manage_gang_enemy_0_status = 4
					
				endif
				
			BREAK
			
			case 3
			
				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(gang_enemy[0].ped)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(gang_enemy[0].ped, 2.0) 
				endif 
			
				SET_PED_DESIRED_MOVE_BLEND_RATIO(gang_enemy[0].ped, 2.0)
			
				if not is_entity_at_coord(gang_enemy[0].ped, <<-1567.9077, 4731.3516, 49.3402>>, <<2.0, 2.0, 2.0>>)
					if has_ped_task_finished_2(gang_enemy[0].ped, SCRIPT_TASK_PERFORM_SEQUENCE_FROM_PROGRESS)
					and has_ped_task_finished_2(gang_enemy[0].ped)
					
						open_sequence_task(seq)
//							task_follow_nav_mesh_to_coord(null, <<-1570.7145, 4447.6685, 10.7225>>, 1.5, -1, -1, ENAV_NO_STOPPING)
//							task_follow_nav_mesh_to_coord(null, <<-1591.6, 4452.0, 6.3>>, 1.5, -1, -1, ENAV_NO_STOPPING)
//							task_follow_nav_mesh_to_coord(null, <<-1604.6509, 4472.7900, 13.2951>>, 1.5, -1, -1, ENAV_NO_STOPPING)
//							task_follow_nav_mesh_to_coord(null, <<-1573.8104, 4525.1421, 17.4637>>, 1.5, -1, -1, ENAV_NO_STOPPING)
							
							task_follow_nav_mesh_to_coord(null, <<-1565.04, 4560.87, 16.76>>, pedmove_walk, -1, -1, enav_no_stopping)
							TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_2", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
						close_sequence_task(seq)
						TASK_PERFORM_SEQUENCE_FROM_PROGRESS(gang_enemy[0].ped, seq, current_seq_pos_0, 1)
						clear_sequence_task(seq)
					
					else 
					
						if get_script_task_status(gang_enemy[0].ped, SCRIPT_TASK_PERFORM_SEQUENCE_FROM_PROGRESS) = performing_task
							current_seq_pos_0 = get_sequence_progress(gang_enemy[0].ped)
						endif 
					
					endif
				else 

					if not is_ped_injured(gang_enemy[1].ped)
						if IS_PED_GETTING_INTO_A_VEHICLE(gang_enemy[1].ped)
						
							open_sequence_task(seq)
								task_enter_vehicle(null, ambient_car.veh, -1, vs_front_right)
							close_sequence_task(seq)
							TASK_PERFORM_SEQUENCE(gang_enemy[0].ped, seq)
							clear_sequence_task(seq)
							
						else 

							open_sequence_task(seq)
								task_enter_vehicle(null, ambient_car.veh, -1)
							close_sequence_task(seq)
							TASK_PERFORM_SEQUENCE(gang_enemy[0].ped, seq)
							clear_sequence_task(seq)
							
						endif 
						
					else 
					
						open_sequence_task(seq)
							task_enter_vehicle(null, ambient_car.veh, -1)
						close_sequence_task(seq)
						TASK_PERFORM_SEQUENCE(gang_enemy[0].ped, seq)
						clear_sequence_task(seq)
					
					endif 
					
					manage_gang_enemy_0_status++
				
				endif
				
			break 
			
			case 4

				if is_ped_sitting_in_vehicle(gang_enemy[0].ped, ambient_car.veh)
				and okay_for_vehicle_to_drive_off_waiting_on_ped(gang_enemy[1].ped)

					if not is_playback_going_on_for_vehicle(ambient_car.veh)
						start_playback_recorded_vehicle(ambient_car.veh, ambient_car.recording_number, "lkexile2")
					endif 
					
					manage_gang_enemy_0_status++
				endif
			
			break 
			
			case 5
		
			
			break 
			
			case 22
			
			break 

		ENDSWITCH
		
	endif 
	
ENDPROC


PROC manage_gang_enemy_1() //water enemy

	//vector target_waypoint_pos
	//int waypoint_node_pos

	if not IS_PED_INJURED(gang_enemy[1].ped)

		if manage_gang_enemy_1_status != 22
		
			if (GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[1].ped)) < 40.00 and is_entity_on_screen(gang_enemy[1].ped))
			or ((get_current_player_ped_enum() = char_franklin) and CAN_PED_SEE_HATED_PED(gang_enemy[1].ped, PLAYER_PED_ID()) and (GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[1].ped)) < 20.00)) 
			or ((get_current_player_ped_enum() != char_franklin) and (GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(gang_enemy[1].ped)) < 20.00)) 
			or has_entity_been_damaged_by_entity(gang_enemy[1].ped, PLAYER_PED_ID())
			
				if not does_blip_exist(gang_enemy[1].blip)
					gang_enemy[1].blip = create_blip_for_ped(gang_enemy[1].ped, true)
				endif 
				
				if manage_gang_enemy_1_status < 5

					set_blocking_of_non_temporary_events(gang_enemy[1].ped, false)
					set_ped_sphere_defensive_area(gang_enemy[1].ped, <<-1643.4071, 4396.6016, 7.5152>>, 10.0)
					task_combat_ped(gang_enemy[1].ped, player_ped_id())
				
				elif manage_gang_enemy_1_status < 10
				
					set_blocking_of_non_temporary_events(gang_enemy[1].ped, false)
					//set_ped_sphere_defensive_area(gang_enemy[1].ped, <<-1662.8707, 4452.1641, 1.1048>>, 10.0)
					set_ped_sphere_defensive_area(gang_enemy[1].ped, <<-1744.2083, 4464.2241, 3.2479>>, 3.0)
					task_combat_ped(gang_enemy[1].ped, player_ped_id())
			
				endif 

				manage_gang_enemy_1_status = 22
				
			endif 
		endif 
		
		
		if not water_enemy_shot_at
//			if manage_gang_enemy_1_status > 1
//				if is_bullet_in_area(get_entity_coords(gang_enemy[1].ped), 4.0)
//					SET_PED_DESIRED_MOVE_BLEND_RATIO(gang_enemy[1].ped, 2.5)
					water_enemy_shot_at = true
//				endif 
//			endif 
//		else 
//			if is_entity_in_water(gang_enemy[1].ped)
//				SET_PED_DESIRED_MOVE_BLEND_RATIO(gang_enemy[1].ped, 2.5)
//			endif
		endif 
		
		
		SWITCH manage_gang_enemy_1_status

			case 0
			
				OPEN_SEQUENCE_TASK(seq)
					task_go_straight_to_coord(null, <<-1562.4369, 4365.8770, -0.1>>, pedmove_walk, -1)
					task_go_straight_to_coord(null, <<-1556.8, 4361.9, -0.1>>, pedmove_run, -1)
					task_go_straight_to_coord(null, <<-1552.2439, 4353.1201, -0.1>>, pedmove_run, -1)
					task_go_straight_to_coord(null, <<-1570.4601, 4360.9722, -0.1>>, pedmove_run, -1)
					set_sequence_to_repeat(seq, repeat_forever)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				manage_gang_enemy_1_status++
			
			break 


			CASE 1
				
				if has_gang_member_been_found_by_sniper_scope(gang_enemy[1].ped)
				
					gang_enemy[1].blip = CREATE_BLIP_FOR_ENTITY(gang_enemy[1].ped, TRUE)

					create_conversation(scripted_speech, "TM44AUD", "TM44_FOUND", CONV_PRIORITY_MEDIUM)
					
					OPEN_SEQUENCE_TASK(seq)
						task_go_straight_to_coord(null, <<-1572.7, 4363.5, -0.1>>, pedmove_walk, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					manage_gang_enemy_1_status++
					
				endif 
				
				SET_PED_RESET_FLAG(gang_enemy[1].ped, PRF_DisableNMForRiverRapids, true)

			BREAK
			
			case 2
			
				if is_entity_at_coord(gang_enemy[1].ped, <<-1572.7, 4363.5, -0.1>>, <<3.0, 3.0, 2.0>>)
			
					OPEN_SEQUENCE_TASK(seq)
						task_go_straight_to_coord(null, <<-1577.53, 4365.07, -0.1>>, pedmove_walk, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
					CLEAR_SEQUENCE_TASK(seq)

					manage_gang_enemy_1_status++
					
				endif 
			
			break 
			
			case 3
			
				if is_entity_at_coord(gang_enemy[1].ped, <<-1577.53, 4365.07, -0.1>>, <<3.0, 3.0, 2.0>>)

					OPEN_SEQUENCE_TASK(seq)
						task_go_straight_to_coord(null, <<-1582.07, 4372.06, -0.1>>, pedmove_run, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					manage_gang_enemy_1_status++

				endif 
				
			break 
			
			case 4
			
				if is_entity_at_coord(gang_enemy[1].ped, <<-1582.07, 4372.06, -0.1>>, <<3.0, 3.0, 2.0>>)
			
					OPEN_SEQUENCE_TASK(seq)
						task_go_straight_to_coord(null, <<-1587.37, 4386.34, -0.1>>, pedmove_run, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					manage_gang_enemy_1_status++
					
				endif 
				
			break 
			
			case 5
				
				if is_entity_at_coord(gang_enemy[1].ped, <<-1587.37, 4386.34, -0.1>>, <<3.0, 3.0, 2.0>>)
							
					OPEN_SEQUENCE_TASK(seq)
						task_go_straight_to_coord(null, <<-1567.61, 4404.78, 0.17>>, pedmove_run, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					manage_gang_enemy_1_status++
					
				endif 
				
			break 
			
			case 6
			
				if is_entity_at_coord(gang_enemy[1].ped, <<-1567.61, 4404.78, 0.17>>, <<3.0, 3.0, 2.0>>)
				
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, <<-1568.7, 4539.6, 16.6>>, pedmove_run, -1, -1, enav_no_stopping)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_2", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
					close_sequence_task(seq)
					task_perform_sequence(gang_enemy[1].ped, seq)
					clear_sequence_task(seq)
					
					manage_gang_enemy_1_status++
				
				endif 
				
			break 
			
			case 7
			
				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(gang_enemy[1].ped)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(gang_enemy[1].ped, 2.0) 
				else 
					SET_PED_DESIRED_MOVE_BLEND_RATIO(gang_enemy[1].ped, 2.0)
				endif 
			
				if is_entity_at_coord(gang_enemy[1].ped, <<-1567.9077, 4731.3516, 49.3402>>, <<2.0, 2.0, 2.0>>)
				
					if not is_ped_injured(gang_enemy[0].ped)
						if IS_PED_GETTING_INTO_A_VEHICLE(gang_enemy[0].ped)
						
							open_sequence_task(seq)
								task_enter_vehicle(null, ambient_car.veh, -1, vs_front_right)
							close_sequence_task(seq)
							TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
							clear_sequence_task(seq)
							
						else 

							open_sequence_task(seq)
								task_enter_vehicle(null, ambient_car.veh, -1)
							close_sequence_task(seq)
							TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
							clear_sequence_task(seq)
							
						endif 
						
					else 
					
						open_sequence_task(seq)
							task_enter_vehicle(null, ambient_car.veh, -1)
						close_sequence_task(seq)
						TASK_PERFORM_SEQUENCE(gang_enemy[1].ped, seq)
						clear_sequence_task(seq)
					
					endif 
					
					manage_gang_enemy_1_status++
					
				endif 
				
			break 
			
			case 8
			
				if is_ped_sitting_in_vehicle(gang_enemy[1].ped, ambient_car.veh)
				and okay_for_vehicle_to_drive_off_waiting_on_ped(gang_enemy[0].ped)

					if not is_playback_going_on_for_vehicle(ambient_car.veh)
						start_playback_recorded_vehicle(ambient_car.veh, ambient_car.recording_number, "lkexile2")
					endif 
					
					manage_gang_enemy_1_status++
				endif 
			
			break 
			
			case 9 
				
			break 
			
			case 22
			
			break 
			
		ENDSWITCH
		
	endif 
	
ENDPROC


PROC manage_gang_enemy_2()

	IF NOT IS_PED_INJURED(gang_enemy[2].ped)
	
		if manage_gang_enemy_2_status != 22
			
			if (GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[2].ped)) < 15.00 and is_entity_on_screen(gang_enemy[2].ped))
			or ((get_current_player_ped_enum() = char_franklin) and CAN_PED_SEE_HATED_PED(gang_enemy[2].ped, PLAYER_PED_ID()) and (GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[2].ped)) < 15.00)) 
			or has_entity_been_damaged_by_entity(gang_enemy[2].ped, PLAYER_PED_ID())
			
				if not does_blip_exist(gang_enemy[2].blip)
					gang_enemy[2].blip = create_blip_for_ped(gang_enemy[2].ped, true)
				endif 

				set_blocking_of_non_temporary_events(gang_enemy[2].ped, false)
				set_ped_sphere_defensive_area(gang_enemy[2].ped, <<-1569.2168, 4424.8916, 6.1680>>, 10.0)
				task_combat_ped(gang_enemy[2].ped, player_ped_id())
				
				manage_gang_enemy_2_status = 22
				
			endif 
		endif 
	
		SWITCH manage_gang_enemy_2_status
			
			case 0
			
				SET_PED_STEALTH_MOVEMENT(gang_enemy[2].ped, true, "DEFAULT_ACTION")
			
				open_sequence_task(seq)
					//TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_ON)
					task_follow_nav_mesh_to_coord(null, <<-1574.28, 4493.85, 21.11>>, 1.5, -1)
					task_pause(null, 2000)
					task_follow_nav_mesh_to_coord(null, <<-1585.68, 4500.92, 19.81>>, 1.5, -1)
					task_pause(null, 2000)
					task_follow_nav_mesh_to_coord(null, <<-1579.22, 4515.91, 18.30>>, 1.5, -1)
					set_sequence_to_repeat(seq, repeat_forever)
				close_sequence_task(seq)
				task_perform_sequence(gang_enemy[2].ped, seq)
				clear_sequence_task(seq)
			
				manage_gang_enemy_2_status++	
			
			break 
			
			CASE 1

				if has_gang_member_been_found_by_sniper_scope(gang_enemy[2].ped)
				
					gang_enemy[2].blip = CREATE_BLIP_FOR_ENTITY(gang_enemy[2].ped, TRUE)

					create_conversation(scripted_speech, "TM44AUD", "TM44_FOUND", CONV_PRIORITY_MEDIUM)

					open_sequence_task(seq)
						//TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_ON)
						task_follow_nav_mesh_to_coord(null, <<-1545.07, 4470.35, 18.78>>, 1.5, -1)
						task_pause(null, 2000)
						task_follow_nav_mesh_to_coord(null, <<-1541.45, 4457.35, 15.75>>, 1.5, -1)
						task_pause(null, 2000)
						task_follow_nav_mesh_to_coord(null, <<-1551.03, 4457.64, 16.10>>, 1.5, -1)
						task_pause(null, 2000)
						task_follow_nav_mesh_to_coord(null, <<-1556.83, 4447.04, 11.10>>, 1.5, -1)
					close_sequence_task(seq)
					task_perform_sequence(gang_enemy[2].ped, seq)
					clear_sequence_task(seq)
					
					current_seq_pos_2 = 0

					manage_gang_enemy_2_status++
				endif 

			BREAK
			
			case 2

				if not is_entity_at_coord(gang_enemy[2].ped, <<-1556.83, 4447.04, 11.10>>, <<1.5, 1.5, 2.0>>)
					if has_ped_task_finished_2(gang_enemy[2].ped, SCRIPT_TASK_PERFORM_SEQUENCE_FROM_PROGRESS)
					and has_ped_task_finished_2(gang_enemy[2].ped)
					
						open_sequence_task(seq)
							//TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_ON)
							task_follow_nav_mesh_to_coord(null, <<-1545.07, 4470.35, 18.78>>, 1.5, -1)
							task_pause(null, 2000)
							task_follow_nav_mesh_to_coord(null, <<-1541.45, 4457.35, 15.75>>, 1.5, -1)
							task_pause(null, 2000)
							task_follow_nav_mesh_to_coord(null, <<-1551.03, 4457.64, 16.10>>, 1.5, -1)
							task_pause(null, 2000)
							task_follow_nav_mesh_to_coord(null, <<-1556.83, 4447.04, 11.10>>, 1.5, -1)
						close_sequence_task(seq)
						TASK_PERFORM_SEQUENCE_FROM_PROGRESS(gang_enemy[2].ped, seq, current_seq_pos_2, 6) 
						clear_sequence_task(seq)
					
					else 
					
						current_seq_pos_2 = get_sequence_progress(gang_enemy[2].ped)
					
					endif
				endif
				
				if is_bullet_in_area(get_entity_coords(gang_enemy[2].ped), 4.0)
					
					open_sequence_task(seq)
						task_play_anim(null, "missexile2", "enter_crouch_b", normal_blend_in, normal_blend_out, -1, AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
						task_play_anim(null, "missexile2", "crouching_idle_b", normal_blend_in, normal_blend_out, -1, AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
					close_sequence_task(seq)
					task_perform_sequence(gang_enemy[2].ped, seq)
					clear_sequence_task(seq)
					
					manage_gang_enemy_2_status = 3
				endif 
			
			break 
			
			case 3
			
				SET_PED_DESIRED_MOVE_BLEND_RATIO(gang_enemy[2].ped, 2.0)
			
				if not is_entity_at_coord(gang_enemy[2].ped, <<-1556.83, 4447.04, 11.10>>, <<1.5, 1.5, 2.0>>)
					if has_ped_task_finished_2(gang_enemy[2].ped, SCRIPT_TASK_PERFORM_SEQUENCE_FROM_PROGRESS)
					and has_ped_task_finished_2(gang_enemy[2].ped)
					
						open_sequence_task(seq)
							//TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_ON)
							task_follow_nav_mesh_to_coord(null, <<-1545.07, 4470.35, 18.78>>, 1.5, -1)
							task_pause(null, 2000)
							task_follow_nav_mesh_to_coord(null, <<-1541.45, 4457.35, 15.75>>, 1.5, -1)
							task_pause(null, 2000)
							task_follow_nav_mesh_to_coord(null, <<-1551.03, 4457.64, 16.10>>, 1.5, -1)
							task_pause(null, 2000)
							task_follow_nav_mesh_to_coord(null, <<-1556.83, 4447.04, 11.10>>, 1.5, -1)
						close_sequence_task(seq)
						TASK_PERFORM_SEQUENCE_FROM_PROGRESS(gang_enemy[2].ped, seq, current_seq_pos_2, 7) 
						clear_sequence_task(seq)
					
					else 

						if get_script_task_status(gang_enemy[2].ped, SCRIPT_TASK_PERFORM_SEQUENCE_FROM_PROGRESS) = performing_task
							current_seq_pos_2 = get_sequence_progress(gang_enemy[2].ped)
						endif 
					
					endif
				endif
			
			break 
			
			case 22
			
			break 

		ENDSWITCH
		
	endif 
	
ENDPROC	

PROC GET_RPG_GUY_TO_ATTACK_CHOPPER()
	
	IF NOT IS_PED_INJURED(gang_enemy[3].ped)
		IF NOT IS_ENTITY_DEAD(helicopter.veh)
			VECTOR vTemp = GET_ENTITY_COORDS(helicopter.veh)
			vTemp.z += 15.0
			vTemp.x += 3.0
			OPEN_SEQUENCE_TASK(seq)
				//TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << -1632.4982, 4508.4448, 13.5796 >>, helicopter.veh, PEDMOVE_RUN, FALSE)
				//task_toggle_duck(null, TOGGLE_DUCK_off)
				TASK_AIM_GUN_AT_ENTITY(NULL, helicopter.veh, 5000)
				TASK_SHOOT_AT_COORD(NULL, vTemp, 1500, FIRING_TYPE_1_THEN_AIM)
				//TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_ON)
				//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,  <<-1636.8694, 4512.0166, 15.1772>>, PEDMOVE_RUN, -1, 0.5)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(gang_enemy[3].ped, seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
	ENDIF
ENDPROC



proc reduce_gang_enemy_3_heatscale()

	if not is_ped_injured(gang_enemy[3].ped)
	
//		printstring("gang_enemy_3_heatscale_value: ")
//		printint(gang_enemy_3_heatscale_value)
//		printnl()
	
		gang_enemy_3_heatscale_value -= 9
		
		if gang_enemy_3_heatscale_value < 1
			gang_enemy_3_heatscale_value = 1
		endif 

		SET_PED_HEATSCALE_OVERRIDE(gang_enemy[3].ped, gang_enemy_3_heatscale_value)
		
	endif 

endproc 

PROC manage_gang_enemy_3()

	if not is_ped_injured(gang_enemy[3].ped)
	
		if ! (manage_gang_enemy_3_status >= 21)
			
			if not is_any_text_being_displayed(locates_data)
				if get_current_player_ped_enum() = char_franklin
					if lk_timer(gang_enemy_3_dialogue_time, 12000)
						
						if GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[3].ped)) < 50.00

							switch gang_enemy_3_attack_conv_status 
							
								case 0

									if create_conversation(scripted_speech, "TM44AUD", "TM44_SHOUT", conv_priority_medium)
										gang_enemy_3_dialogue_time = get_game_timer()
									endif 
									
								break 
								
								case 1

									if GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(chop.ped), get_entity_coords(gang_enemy[3].ped)) < 15.0
										if create_conversation(scripted_speech, "TM44AUD", "TM44_DOG", conv_priority_medium)
											gang_enemy_3_dialogue_time = get_game_timer()
										endif 
									else 
										gang_enemy_3_attack_conv_status = 0
									endif 
									
								break 
								
							endswitch  

						endif 
					endif 
				endif 
			endif 
			
			if get_current_player_ped_enum() = char_franklin
			and chop_cam.state != CHOP_VIEW_CAM_on_CHOP
			
				if (GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[3].ped)) < 25.00 and is_entity_on_screen(gang_enemy[3].ped)) //18
				or has_player_antagonised_ped(gang_enemy[3].ped, 35.00)
				or has_entity_been_damaged_by_entity(gang_enemy[3].ped, PLAYER_PED_ID())
		
					give_weapon_to_ped(gang_enemy[3].ped, weapontype_pistol, infinite_ammo, false, false) 
					set_current_ped_weapon(gang_enemy[3].ped, WEAPONTYPE_unarmed, true)

					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, <<-1565.04, 4560.87, 16.76>>, pedmove_run, -1, 1.0, enav_no_stopping)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_2", 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
					close_sequence_task(seq)
					task_perform_sequence(gang_enemy[3].ped, seq)
					clear_sequence_task(seq)
					
					SET_PED_HEATSCALE_OVERRIDE(gang_enemy[3].ped, 255)
					
					gang_enemy_3_dialogue_time = 0 //resets time for dialogue system when manage_gang_enemy_3_status >= 21
					
					manage_gang_enemy_3_status = 21
					
				endif 
			endif 
			
		else 
		
			//add flee dialogue here.
		
			UPDATE_AI_PED_BLIP(gang_enemy[3].ped, gang_enemy[3].blip_2, -1, null, true)
				
		endif 
		
//		printstring("manage_gang_enemy_3_status: ")
//		printint(manage_gang_enemy_3_status)
//		printnl()

		SWITCH manage_gang_enemy_3_status
			
			CASE 0
			
				prepare_music_event("EXL2_RPG_FIRED")
			
				//SET_ENTITY_PROOFS(gang_enemy[3].ped, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_CURRENT_PED_WEAPON(gang_enemy[3].ped, WEAPONTYPE_RPG, TRUE)
				
				OPEN_SEQUENCE_TASK(seq)
					TASK_AIM_GUN_AT_ENTITY(NULL, helicopter.veh, 99999999)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(gang_enemy[3].ped, seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				gang_enemy[3].time = get_game_timer()

				manage_gang_enemy_3_status++
				
			BREAK
			
			CASE 1
			
				if has_gang_member_been_found_by_sniper_scope(gang_enemy[3].ped)
				or lk_timer(gang_enemy[3].time, 90000)
				
					REPLAY_RECORD_BACK_FOR_TIME(3)
				
					reduce_gang_enemy_3_heatscale()
				
					trigger_music_event("EXL2_RPG_FIRED")

					rpg_system()

					manage_gang_enemy_3_status++

				endif 

			break 
			
			case 2
			
				reduce_gang_enemy_3_heatscale()
			
				rpg_system()
			
				//enemy has been re-posiitioned and must run to rock
				if gang_enemy[3].pos.x != -1541.2722
				and gang_enemy[3].pos.y != 4489.3423
				and gang_enemy[3].pos.z != 18.4372
				
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, gang_enemy[3].run_to_pos, pedmove_run, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gang_enemy[3].ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
				endif 
				
				manage_gang_enemy_3_status++
			
			break 
			
			case 3

				reduce_gang_enemy_3_heatscale()
			
				if rpg_system()
					
					iRandTime = GET_RANDOM_INT_IN_RANGE(5000, 10000)
					gang_enemy_3_time = get_game_timer()
					manage_gang_enemy_3_status = 22
					
				endif 
				
			break 

			
			case 4
			
				//SET_PED_STEALTH_MOVEMENT(gang_enemy[3].ped, true)
			
				OPEN_SEQUENCE_TASK(seq)
					//reload 
					//TASK_RELOAD_WEAPON(
					task_go_to_coord_while_aiming_at_entity(null, <<-1545.5265, 4486.7324, 18.2761>>, helicopter.veh, 1.5, false, 0.5, 0.5)
					TASK_AIM_GUN_AT_ENTITY(NULL, helicopter.veh, 2000)
					TASK_AIM_GUN_AT_ENTITY(NULL, helicopter.veh, 500)
					task_go_to_coord_while_aiming_at_entity(null, gang_enemy[3].pos, helicopter.veh, 1.5, false, 0.5, 0.5)
					task_turn_ped_to_face_entity(null, helicopter.veh) 
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(gang_enemy[3].ped, seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				//script_assert("test 0")
				
				manage_gang_enemy_3_status++
			
			break 

			case 5

				scripttaskstatus task_status
				
				task_status = GET_SCRIPT_TASK_STATUS(gang_enemy[3].ped, SCRIPT_TASK_PERFORM_SEQUENCE)
				
				IF task_status = PERFORMING_TASK
					if GET_SEQUENCE_PROGRESS(gang_enemy[3].ped) = 2//2
						
						rpg_system()

						manage_gang_enemy_3_status++
					endif 
				
				else 
				
					manage_gang_enemy_3_status = 4
				
				endif

			break 
			
			case 6
			
			//set_entity_load_collision_flag(rpg.o, true)
			
				if rpg_system()
					
					iRandTime = GET_RANDOM_INT_IN_RANGE(5000, 10000)
					gang_enemy_3_time = get_game_timer()
					manage_gang_enemy_3_status++
		
				ENDIF
			
			break 
			
			case 7
			
				if lk_timer(gang_enemy_3_time, iRandTime)
					set_blocking_of_non_temporary_events(gang_enemy[3].ped, true) //fix for bug 381761
					manage_gang_enemy_3_status = 4
				ENDIF
			
			break
			
			case 21

				if does_entity_exist(rpg_obj)
					rpg_system()
				endif 
				
				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(gang_enemy[3].ped)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(gang_enemy[3].ped, 2.0) 
				endif 
			
//				int gang_enemy_3_node 
//				
//				waypoint_recording_get_closest_waypoint("exile2_2", get_entity_coords(gang_enemy[3].ped), gang_enemy_3_node)  
			
				//IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK

//				if has_entity_been_damaged_by_entity(gang_enemy[3].ped, player_ped_id())
//					printstring("test 0********************")
//					printnl()
//					printstring("test 0********************")
//					printnl()
//					printstring("test 0********************")
//					printnl()
//				endif

				//could have in it's own case 
				//then go into another case and remove thir or check so that the player would still run regardless
				//if the player is infront of the enemy


				if has_ped_task_finished_2(gang_enemy[3].ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) 
					if (chop_cam.state != CHOP_VIEW_CAM_on_CHOP)
						if has_entity_been_damaged_by_entity(gang_enemy[3].ped, player_ped_id())
						or is_ped_ragdoll(gang_enemy[3].ped)
						or (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[3].ped)) < 10
							and is_ped_in_front_of_ped(player_ped_id(), gang_enemy[3].ped)
							and (not rpg_enemy_attack_task_given and CAN_PED_SEE_HATED_PED(gang_enemy[3].ped, player_ped_id())))
							//CAN_PED_SEE_HATED_PED wrapped with not rpg_enemy_attack_task_given
							
	//						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[3].ped)) < 18.0

								set_blocking_of_non_temporary_events(gang_enemy[3].ped, false)
								set_ped_sphere_defensive_area(gang_enemy[3].ped, get_entity_coords(gang_enemy[3].ped), 8.0) //<<-1567.9077, 4731.3516, 49.3402>>, 3.0)
								SET_PED_COMBAT_ATTRIBUTES(gang_enemy[3].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(gang_enemy[3].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(gang_enemy[3].ped, 100.00)
								
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(gang_enemy[3].ped)
						
								gang_enemy_3_time = 0
								
								gang_enemy_3_time = get_game_timer()
								
								rpg_enemy_attack_task_given = true 
						endif 

					endif 
				endif 
				
				
				if has_ped_task_finished_2(gang_enemy[3].ped)
					
					if not is_ped_ragdoll(gang_enemy[3].ped)

						if lk_timer(gang_enemy_3_time, 12000)
						or chop_cam.state = CHOP_VIEW_CAM_on_CHOP //make ped run when player is chop. This will stop the gang enemy fighting but not loosing any health which looks weird.

							set_blocking_of_non_temporary_events(gang_enemy[3].ped, true)
							clear_ped_tasks(gang_enemy[3].ped)
					
							open_sequence_task(seq)
								TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_2", 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
							close_sequence_task(seq)
							task_perform_sequence(gang_enemy[3].ped, seq)
							clear_sequence_task(seq)
							
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(gang_enemy[3].ped)
							
							rpg_enemy_attack_task_given = false
							
						endif 
						
					endif 
					
				endif 
				
						
				int waypoint_node_pos
				waypoint_recording_get_closest_waypoint("exile2_2", GET_ENTITY_COORDS(player_ped_id()), waypoint_node_pos)
				
				if not is_entity_at_coord(gang_enemy[3].ped, <<-1567.9077, 4731.3516, 49.3402>>, <<4.0, 4.0, 2.0>>)
				and (waypoint_recording_get_closest_waypoint("exile2_2", GET_ENTITY_COORDS(gang_enemy[3].ped), waypoint_node_pos) and waypoint_node_pos < 191)
					
					printstring("crew 3 test 0")
					printnl()
					
					request_anim_dict("missfra0_chop_find")
									
					if has_anim_dict_loaded("missfra0_chop_find")
					
						printstring("crew 3 test 1")
						printnl()
						
						if (get_current_player_ped_enum() = char_franklin)
						and (not is_player_switch_in_progress())
						and (chop_cam.state != CHOP_VIEW_CAM_on_CHOP)
						and chop_behaviour = CHOP_BEHAVIOUR_GOOD 
						

							if (get_script_task_status(chop.ped, SCRIPT_TASK_GO_TO_ENTITY) = performing_task)
							//and (get_script_task_status(gang_enemy[3].ped, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = performing_task)
							and not has_ped_task_finished_2(gang_enemy[3].ped)
								
								if (waypoint_recording_get_closest_waypoint("exile2_2", GET_ENTITY_COORDS(gang_enemy[3].ped), waypoint_node_pos) and (waypoint_node_pos >= 40))
											
									if is_entity_in_angled_area(chop.ped, get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<-1.0, -1.0, -1.0>>), get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<1.0, -1.0, 2.0>>), 3.5)
			
										cutscene_pos = GET_ENTITY_COORDS(gang_enemy[3].ped)
										cutscene_rot = GET_ENTITY_ROTATION(gang_enemy[3].ped)
										
										vector vec_a
										vector vec_b 
										vector vec_ba
										vector direction_vec
										float ground_z
										
										vec_a = cutscene_pos
										vec_b = get_anim_initial_offset_position("missexile2", "take_down_bad_guy_victim", cutscene_pos, cutscene_rot, 1.0)  
										
										//vec_b this will give us a position which is directly infront of the enemy.
										//this is no good when on a hill as that position will be under the map
										//we need to get the ground z as that posit
										
										
										printstring("vec_b.z = ")
										printfloat(vec_b.z)
										printnl()
										
										get_ground_z_for_3d_coord(vec_b, ground_z) 

										vec_b.z = (ground_z + 1.0) //adding 1.0 to get the proper ground z
										
										printstring("vec_b.z 2 = ")
										printfloat(vec_b.z)
										printnl()
										
										vec_ba = vec_b - vec_a
										
										direction_vec = normalise_vector(vec_ba)

										//calculate the pitch of the vector ba and update cutscene_rot.x with it
										cutscene_rot.x = ATAN2(direction_vec.z, VMAG(<<direction_vec.x, direction_vec.y, 0.0>>))
										
										
										//obtain the new cutscene_pos using the newly calculated pitch x
//										vector vec_target_hill
//										
//										vec_target_hill = get_anim_initial_offset_position("missexile2", "take_down_bad_guy_victim", cutscene_pos, cutscene_rot, 1.0) //1.0 
//	
//										cutscene_pos = vec_target_hill

									
										gang_enemy[3].scene_id = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
									
										task_synchronized_scene(gang_enemy[3].ped, gang_enemy[3].scene_id, "missexile2", "take_down_bad_guy_victim", normal_blend_in, normal_blend_out, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, rbf_PLAYER_IMPACT, normal_blend_in)	
			
										manage_gang_enemy_3_status = 22
										
									endif 
								endif 
							endif 
							
						endif 
						
					endif 

				else 
				
					set_blocking_of_non_temporary_events(gang_enemy[3].ped, true)
				
					open_sequence_task(seq)
						task_enter_vehicle(null, ambient_car.veh, -1)
					close_sequence_task(seq)
					TASK_PERFORM_SEQUENCE(gang_enemy[3].ped, seq)
					clear_sequence_task(seq)
					
					manage_gang_enemy_3_status = 24
				
				endif
				
			break 
			
			case 22
			
				if is_synchronized_scene_running(gang_enemy[3].scene_id)
					if get_synchronized_scene_phase(gang_enemy[3].scene_id) >= 1.0
						
						SET_HIGH_FALL_TASK(gang_enemy[3].ped, 5000, 7000)
						
						manage_gang_enemy_3_status++
						
					endif 
					
				endif 					
			
			break 
			
			case 23
			
				reset_ped_ragdoll_timer(gang_enemy[3].ped)
			
			break 
			
			case 24
			
				if is_vehicle_driveable(ambient_car.veh)
					
					if is_ped_sitting_in_vehicle(gang_enemy[3].ped, ambient_car.veh)

						if not is_playback_going_on_for_vehicle(ambient_car.veh)
							start_playback_recorded_vehicle(ambient_car.veh, ambient_car.recording_number, "lkexile2")
						endif

						manage_gang_enemy_3_status = 24
						
					endif 
				
				else 
				
					//give_weapon_to_ped(gang_enemy[3].ped, WEAPONTYPE_ASSAULTRIFLE, infinite_ammo)
					
					set_current_ped_weapon(gang_enemy[3].ped, weapontype_pistol)

					set_blocking_of_non_temporary_events(gang_enemy[3].ped, false)
					set_ped_sphere_defensive_area(gang_enemy[3].ped, <<-1567.9077, 4731.3516, 49.3402>>, 10.0)
					task_combat_ped(gang_enemy[3].ped, player_ped_id())
					
					manage_gang_enemy_3_status = 25
				
				endif
			
			break 
			
			case 25
			
				if not is_any_text_being_displayed(locates_data)
					if get_current_player_ped_enum() = char_franklin
						if create_conversation(scripted_speech, "TM44AUD", "TM44_ORUN", CONV_PRIORITY_medium)   
							manage_gang_enemy_3_status = 24
						endif 
					endif 
				endif 

			break 
			
			case 26
			
			break 
			
		ENDSWITCH
		

		if is_explosion_in_sphere(exp_tag_dontcare, get_entity_coords(helicopter.veh), 10.00)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PLAY_SINGLE_LINE_FROM_CONVERSATION(scripted_speech, "TM44AUD", "TM44_RPG2", "TM44_RPG2_1", CONV_PRIORITY_LOW)
			ENDIF
		ENDIF
		
//		if explosion_count > 10
//		
//			KILL_ANY_CONVERSATION()
//	
////			delete_vehicle(dummy_helicopter.veh)
////				
////			EXPLODE_VEHICLE(helicopter.veh)
////			EXPLODE_VEHICLE(helicopter.veh)
//		ENDIF

	else 
	
		if gang_enemy[3].created
		
			if is_vehicle_driveable(ambient_car.veh)
				if is_playback_going_on_for_vehicle(ambient_car.veh)
					stop_playback_recorded_vehicle(ambient_car.veh)
				endif 
			endif 

			if does_entity_exist(rpg_obj)
				rpg_system()
			endif 
			
			UPDATE_AI_PED_BLIP(gang_enemy[3].ped, gang_enemy[3].blip_2, -1, null, true)

		endif 

	endif 
	
//	printstring("manage_gang_enemy_3_status: ")
//	printint(manage_gang_enemy_3_status)
//	printnl()
	
ENDPROC

//PROC manage_gang_enemy_4()
//
//	if not is_ped_injured(gang_enemy[4].ped)
//
//		SWITCH manage_gang_enemy_4_status
//			
//			case 0
//			
//				set_blocking_of_non_temporary_events(gang_enemy[4].ped, false)
//				set_ped_sphere_defensive_area(gang_enemy[4].ped, gang_enemy[4].pos, 4.0)
//				task_combat_hated_targets_around_ped(gang_enemy[4].ped, 200.00)
//				
//				manage_gang_enemy_4_status++
//			
//			break 
//			
//			case 1
//			
//				if GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[4].ped)) < 15.00
//				or CAN_PED_SEE_HATED_PED(gang_enemy[4].ped, PLAYER_PED_ID())
//				or has_entity_been_damaged_by_entity(gang_enemy[4].ped, PLAYER_PED_ID())
//				
//					//clear_ped_tasks(gang_enemy[0].ped)
//					set_blocking_of_non_temporary_events(gang_enemy[4].ped, false)
//					set_ped_sphere_defensive_area(gang_enemy[4].ped, gang_enemy[4].pos, 4.0)
//					task_combat_ped(gang_enemy[4].ped, player_ped_id())
//					
//					manage_gang_enemy_4_status++
//					
//				endif 
//			
//			break 
//			
//			case 2
//			
//			break 
//			
//		endswitch 
//		
//	endif 
//	
//endproc 
			

PROC MANAGE_ENEMY_PEDS_ON_GROUND()
	
	ped_structure_are_all_enemies_dead(gang_enemy)
	
	manage_gang_enemy_0() //forrest
	manage_gang_enemy_1() //water guy
	manage_gang_enemy_2()
	manage_gang_enemy_3() //rpg guy

ENDPROC

FUNC FLOAT DISTANCE_BETWEEN_COORDS(VECTOR v1, VECTOR v2, BOOL in_2d = FALSE, BOOL use_square_root = TRUE)
	IF in_2d
		v1.z = 0.0
		v2.z = 0.0
	ENDIF
	
	IF use_square_root
		RETURN VDIST(v1, v2)
	ELSE
		RETURN VDIST2(v1, v2)
	ENDIF
ENDFUNC

FUNC BOOL DO_FRANKLIN_PHONE_CALL()
	SWITCH iFranklinCallProg
		CASE 0
			IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER (scripted_speech, CHAR_FRANKLIN, "TM44AUD", "TM44_COPS", CONV_PRIORITY_AMBIENT_MEDIUM)
				iFranklinCallProg++
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_CELLPHONE_CALL_FINISHED()
				UNPAUSE_TIMER(count_down_timer)
				iFranklinCallProg++
			ENDIF
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DO_SAFE_FADE_OUT()
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(500)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC DO_SAFE_FADE_IN(BOOL b_force_fade = FALSE)
	IF NOT b_is_p_skipping OR b_force_fade
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(500)
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_OBJECT(OBJECT_INDEX &obj, BOOL b_force_delete = FALSE)
	IF DOES_ENTITY_EXIST(obj)
		IF IS_ENTITY_ATTACHED(obj)
			DETACH_ENTITY(obj)
		ENDIF

		IF b_force_delete
			DELETE_OBJECT(obj)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL b_force_delete = FALSE)
	
	IF DOES_ENTITY_EXIST(veh)
		
		IF IS_VEHICLE_DRIVEABLE(veh)
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
		ENDIF

		IF IS_ENTITY_A_MISSION_ENTITY(veh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh, false)

			if is_player_playing(player_id())
				
				if is_vehicle_driveable(veh) 
					
					if not is_ped_sitting_in_vehicle(player_ped_id(), veh) 

						IF b_force_delete
							DELETE_VEHICLE(veh)
						ELSE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
						ENDIF
					
					else
					
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) 
					
					endif 
				
				else 
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) 
				
				endif 
			
			else 
				SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) //can't delete vehicle with player in it. Fail system requires player to be in car can't use clear_ped_tasks_immediaely()
			endif 
		endif 
		
	ENDIF
	
ENDPROC

PROC REMOVE_PED(PED_INDEX &ped, BOOL b_force_delete = FALSE)
	IF DOES_ENTITY_EXIST(ped)
		IF NOT IS_PED_INJURED(ped)
			if not IS_PED_IN_ANY_VEHICLE(ped)
				if not IS_PED_GETTING_INTO_A_VEHICLE(ped)
					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(ped)
						DETACH_ENTITY(ped)
					endif 
				endif 
			ENDIF
			
			if not is_entity_attached(ped)
				FREEZE_ENTITY_POSITION(ped, FALSE)
			endif 
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
				SET_ENTITY_COLLISION(ped, TRUE)
			ENDIF
		ENDIF

		IF b_force_delete
			DELETE_PED(ped)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(ped)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ALL_BLIPS()	

	if does_blip_exist(trevors_truck.blip)
		remove_blip(trevors_truck.blip)
	endif 

	IF DOES_BLIP_EXIST(blip_current_destination)
		REMOVE_BLIP(blip_current_destination)
	ENDIF
	
	IF DOES_BLIP_EXIST(franklins_car.blip)
		REMOVE_BLIP(franklins_car.blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(gang_car.blip)
		REMOVE_BLIP(gang_car.blip)
	ENDIF
	
	INT i = 0
	REPEAT COUNT_OF(gang_enemy) i
		IF DOES_BLIP_EXIST(gang_enemy[i].blip)
			REMOVE_BLIP(gang_enemy[i].blip)
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_OBJECTS(/*BOOL b_force_delete = FALSE*/)

ENDPROC

PROC REMOVE_ALL_PEDS(BOOL b_force_delete = FALSE)
	INT i = 0
	
	for i = 0 to count_of(gang_enemy) - 1
		if does_entity_exist(gang_enemy[i].ped)
			delete_ped(gang_enemy[i].ped)
		endif 
	endfor 
	
	REPEAT COUNT_OF(animal) i
		REMOVE_PED(animal[i].ped, b_force_delete)
	ENDREPEAT
	
	REMOVE_PED(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
	REMOVE_PED(selector_ped.pedID[SELECTOR_PED_MICHAEL])
	REMOVE_PED(selector_ped.pedID[SELECTOR_PED_TREVOR])
	
	REMOVE_PED(chop.ped, true)
	
	remove_ped(big_foot.ped, true)
	set_model_as_no_longer_needed(big_foot.model)
			
ENDPROC

proc remove_all_animations()

	remove_anim_dict("missexile2deer_crash")

endproc 

PROC REMOVE_ALL_VEHICLES(BOOL b_force_delete = FALSE)
	remove_vehicle(trevors_truck.veh, b_force_delete)
	REMOVE_VEHICLE(franklins_car.veh, b_force_delete)
	REMOVE_VEHICLE(gang_car.veh, b_force_delete)
	REMOVE_VEHICLE(helicopter.veh, b_force_delete)
	REMOVE_VEHICLE(SetPieceCarID[0], b_force_delete)
	REMOVE_VEHICLE(ambient_car.veh)
	
	set_model_as_no_longer_needed(radi)
	set_model_as_no_longer_needed(sabregt)
ENDPROC

PROC REMOVE_ALL_CAMERAS()
	IF DOES_CAM_EXIST(cam_cutscene)
		DESTROY_CAM(cam_cutscene)
	ENDIF
	
	IF DOES_CAM_EXIST(cam_attach_helper)
		DESTROY_CAM(cam_attach_helper)
	ENDIF
	
	IF DOES_CAM_EXIST(cam_interp_helper)
		DESTROY_CAM(cam_interp_helper)
	ENDIF
	
	if does_cam_exist(heli_cam)
		destroy_cam(heli_cam)
	endif 
	
	if does_cam_exist(node_1_cam)
		destroy_cam(node_1_cam)
	endif 
	
	if does_cam_exist(node_2_cam)
		destroy_cam(node_2_cam)
	endif 
	
	if does_cam_exist(node_3_cam)
		destroy_cam(node_3_cam)
	endif 

	DISPLAY_RADAR(TRUE) 
	DISPLAY_HUD(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
ENDPROC

proc initialise_mission_variables()

	#IF IS_DEBUG_BUILD
		menu_stage_selector[0].sTxtLabel = "Load start of mission"
		menu_stage_selector[1].sTxtLabel = "car chase"
		menu_stage_selector[2].sTxtLabel = "Crash Cutscene"  
		menu_stage_selector[3].sTxtLabel = "fly to franklin"
		menu_stage_selector[4].sTxtLabel = "helicopter cutscene"
		menu_stage_selector[5].sTxtLabel = "snipe enemies part 0"
		menu_stage_selector[6].sTxtLabel = "shoot the rpg enemy"
		menu_stage_selector[7].sTxtLabel = "pickup franklin and chop"
		menu_stage_selector[8].sTxtLabel = "fly to airport"
	#endif

	int i 

	stop_thermal_help = false
	allow_switch = true
	vehicle_text_printed = false
	//target_text_printed = false
	set_uber_speed_to_zero = false
	start_rpg_cutscene = false
	activate_helicopter_flying_system = false
	render_sniper_hud = false
	crash_cutscene_fail = false
	water_enemy_shot_at	= false
	load_assets = false
	initialise_sniper_aim_at_pos = false
	mission_starts_as_franklin = false
	reset_heli_freeze_waiting_on_collision = false
	stop_mission_fail_checks = false
	resume_lower_priority_conversation = false
	force_sniper_hud_for_start_of_mission = false
	setup_trevors_truck_for_switch = false
	helicopter_check_point_exists = false
	outside_area_text_printed = false
	hunt_down_rpg_enemy = false
	rpg_enemy_attack_task_given = false
	stop_heli_cam_explosion_shake = false
	allow_helicopter_camera_system_to_run_during_fail = false
	force_sniper_hud_for_first_person_view_mode = false
	initialise_push_in_data_for_switch_cam_trevor_michael_to_bros_and_franklin = false
	scope_to_franklin_hit_out_0 = false
	scope_to_franklin_hit_out_1 = false
	scope_to_franklin_transition_0 = false
	franklin_to_truck_outro_short_transition_in = false
	bros_to_franklin_transition = false
	bros_to_franklin_hit = false
	trigger_hit_out_sound_fx_when_switching_to_michael = false
	
	
	dialogue_system_status = 0
	snipe_enemies_master_flow_system_status = 0
	current_seq_pos_0 = 0
	current_seq_pos_2 = 0
	dialogue_and_help_text_system_status = 0
	switch_system_status = 0
	//number_of_switches_to_trevor = 0
	rpg_system_status = 0
	rpg_cutscene_system_status = 0
	//trevor_get_to_mountains_dialogue_system_status = 0
	franklin_get_to_mountains_dialogue_system_status = 0
	switch_system_2_status = 0
	investigate_crash_status = 0
	snipe_enemies_skip_status = 0
	chop_ai_system_status = 0
	phone_hang_up_count = 0
	chop_cutscene_index = -1
	helicopter_cam_status = 0
	mission_pass_conversation_status = 0
	franklin_ai_system_status = 0
	//chop_dialogue_time = 0
	current_seq_pos_0 = 0
	current_seq_pos_2 = 0
	//current_seq_pos_3 = 0
	trevor_looking_at_player_entering_vehicle_status = 0
	get_to_the_helicopter_dialogue_system_status = 0
	helicopter_switch_system_status = 0
	get_helicopter_status = 0
	gang_enemy_3_dialogue_time = 0
	gang_enemy_3_attack_conv_status = 0
	switch_to_trevor_status = 0
	get_to_airstrip_status = 0
	closest_warp_pos_to_franklin = 0
	//current_time_scale = 1.0
	disired_playback_speed = 1.0
	area_fail_time = 0
	uber_chase_status = 0
	uber_chase_time = 0
	get_to_mountains_master_flow_system_status = 0
	get_to_airstrip_dialogue_system_staus = 0
	pickup_franklin_and_chop_dialogue_system_status = 0
	bullet_count = 0
	crash_cutscene_dialogue_system = 0
	switching_into_franklin_status = 0
	tunnel_pinning_system_status = 0
	pass_mocap_status = 0
	gang_car_ptfx_cutscene_system_status = 0
	trevor_get_to_mountains_dialogue_system_status = 0
	cutscene_index = 0
	shake_time = 0
	find_rpg_enemy_dialogue_time = 0
	switch_to_franklin_time = 0
	chop_enter_chopper_time = 0
	big_foot_ai_system_status = 0
	switch_sound_time = 0
	sniper_rifle_help_text_system_status = 0
	sniper_rifle_zoom_time = 0
	
	gang_enemy_3_heatscale_value = 128
	uber_acceleration_multiplier = 0.01
	f_playback_time = 0.0
	
	manage_gang_enemy_0_status = 0
	manage_gang_enemy_1_status = 0
	manage_gang_enemy_2_status = 0
	manage_gang_enemy_3_status = 0
	
	scsSwitchCamFranklinToTruckOutro.bInitialized = false
	scsSwitchCamScopeToFranklin.bInitialized = false
	scsSwitchCamTrevorMichael.bInitialized = false
	scsSwitchCamBrosToFranklin.bInitialized = false		
	
	for i = 0 to count_of(gang_enemy) - 1
		gang_enemy[i].time = 0
	endfor 
	
	big_foot.created = false
	
	dialogue_root = ""
	specific_label = ""

	franklins_car.model = BALLER
	
	gang_car.model = dubsta
	gang_car.pos = <<-2442.8577, 3681.4836, 12.4540>>
	
	trevors_truck.model = get_player_veh_model(char_trevor)
	trevors_truck.pos = <<1982.1279, 3829.4192, 31.4094>>
	trevors_truck.heading = 299.4815
	//1983.4915, 3830.6367, 31.3859 >>, 297.5736
	
	gang_enemy[0].pos = <<-1521.8604, 4447.2949, 7.7>> 
	gang_enemy[0].heading = 217.2352
	gang_enemy[0].model = a_m_m_hillbilly_02 
	gang_enemy[0].health = 110
	gang_enemy[0].accuracy = 10
	gang_enemy[0].weapon = WEAPONTYPE_ASSAULTRIFLE 
	gang_enemy[0].damaged_by_player = true
	gang_enemy[0].name = "gang enemy 0"

	gang_enemy[1].pos = <<-1570.4601, 4360.9722, -0.42>>
	gang_enemy[1].heading = 41.3091
	gang_enemy[1].model = a_m_m_hillbilly_02 
	gang_enemy[1].health = 110
	gang_enemy[1].accuracy = 10
	gang_enemy[1].weapon = WEAPONTYPE_PUMPSHOTGUN 
	gang_enemy[1].damaged_by_player = true
	gang_enemy[1].name = "gang enemy 1"
	
	gang_enemy[2].pos = <<-1569.83, 4495.49, 20.24>>
	gang_enemy[2].heading = 196.22
	gang_enemy[2].model = a_m_m_hillbilly_02 
	gang_enemy[2].health = 110
	gang_enemy[2].accuracy = 10
	gang_enemy[2].weapon = WEAPONTYPE_MICROSMG
	gang_enemy[2].damaged_by_player = true
	gang_enemy[2].name = "gang enemy 2"
	
	gang_enemy[3].pos =  <<-1541.2722, 4489.3423, 18.4372>>
	gang_enemy[3].heading = 187.5681
	gang_enemy[3].run_to_pos = <<-1545.6222, 4484.1372, 18.1388>>//<<-1524.9790, 4470.6548, 12.5267>>
	gang_enemy[3].model = a_m_m_hillbilly_02 
	gang_enemy[3].health = 150
	gang_enemy[3].accuracy = 10
	gang_enemy[3].weapon = weapontype_rpg
	gang_enemy[3].damaged_by_player = true
	gang_enemy[3].name = "gang enemy 3"
	
//	gang_enemy[4].pos = << -1636.6315, 4512.5015, 15.2567>>
//	gang_enemy[4].heading = 200.6334
//	gang_enemy[4].model = G_M_Y_SalvaGoon_01
//	gang_enemy[4].health = 200
//	gang_enemy[4].accuracy = 10
//	gang_enemy[4].weapon = WEAPONTYPE_ASSAULTRIFLE 
//	gang_enemy[4].damaged_by_player = true
//	gang_enemy[4].name = "gang enemy 4"
	
	chop.model = GET_CHOP_MODEL()
	chop.pos = <<-1896.4316, 4427.4692, 43.3257>>
	chop.heading = 9.9039
	chop.health = 200
	chop.accuracy = 40
	chop.weapon = weapontype_unarmed
	
	helicopter.pos = <<-1481.96, 4334.3, 95.4>>
	helicopter.heading = 325.2
	helicopter.model = frogger2
	helicopter.recording_number = 5

	animal[0].pos = <<-1891.4313, 4420.2329, 46.6325>>//<<-1890.6168, 4422.1177, 44.9616>>
	animal[0].heading = 341.7364
	animal[0].run_to_pos = <<-1884.7, 4427.5, 44.37>>
	animal[0].model = a_c_deer
	animal[0].weapon = weapontype_unarmed
	animal[0].health = 200
	animal[0].damaged_by_player = true
	animal[0].name = "animal 0"
	
	animal[1].pos = <<-1598.5602, 4498.4004, 20.3763>>
	animal[1].heading = 288.4696
	animal[1].model = a_c_deer
	animal[1].weapon = weapontype_unarmed
	animal[1].health = 110
	animal[1].damaged_by_player = true
	animal[1].name = "animal 1"
	
	animal_movement[1].animal_pos_0 = <<-1563.57, 4495.20, 18.95>>
	animal_movement[1].animal_pos_1 = <<-1566.95, 4469.38, 13.07>>
	animal_movement[1].animal_pos_2 = <<-1551.51, 4453.79, 12.66>>
	
	animal[2].pos = <<-1602.14, 4484.01, 17.36>>
	animal[2].heading = 113.0293
	animal[2].model = a_c_deer
	animal[2].weapon = weapontype_unarmed
	animal[2].health = 110
	animal[2].damaged_by_player = true
	animal[2].name = "animal 2"
	
	animal_movement[2].animal_pos_0 = <<-1571.22, 4482.00, 15.75>>
	animal_movement[2].animal_pos_1 = <<-1565.01, 4488.43, 17.81>>
	animal_movement[2].animal_pos_2 = <<-1547.75, 4481.77, 16.13>>
	
	animal[3].pos = <<-1506.3915, 4396.4653, 19.7348>>
	animal[3].heading = 212.8962
	animal[3].model = a_c_deer
	animal[3].weapon = weapontype_unarmed
	animal[3].health = 110
	animal[3].damaged_by_player = true
	animal[3].name = "animal 3"
	
	animal_movement[3].animal_pos_0 = <<-1489.18, 4391.40, 19.62>>
	animal_movement[3].animal_pos_1 = <<-1485.70, 4402.55, 22.35>>
	animal_movement[3].animal_pos_2 = <<-1468.47, 4421.03, 23.18>>
	
	animal[4].pos = <<-1575.6837, 4382.2974, 0.6292>>
	animal[4].heading = 212.8962
	animal[4].model = a_c_deer
	animal[4].weapon = weapontype_unarmed
	animal[4].health = 110
	animal[4].damaged_by_player = true
	animal[4].name = "animal 4"
	
	animal_movement[4].animal_pos_0 = <<-1577.08, 4388.03, -0.00>>
	animal_movement[4].animal_pos_1 = <<-1550.03, 4401.53, 5.66>>
	animal_movement[4].animal_pos_2 = <<-1557.21, 4430.33, 7.39>>
	
	animal[5].pos = <<-1572.5833, 4420.3745, 5.2758>>
	animal[5].heading = 273.6496
	animal[5].model = a_c_deer
	animal[5].weapon = weapontype_unarmed
	animal[5].health = 110
	animal[5].damaged_by_player = true
	animal[5].name = "animal 5"
	
	animal_movement[5].animal_pos_0 = <<-1573.33, 4414.50, 3.80>>
	animal_movement[5].animal_pos_1 = <<-1569.05, 4401.55, 0.96>>
	animal_movement[5].animal_pos_2 = <<-1561.75, 4373.00, 1.69>>
	
	animal[6].pos = <<-1509.13, 4439.37, 12.93>>
	animal[6].heading = 230.6402
	animal[6].model = a_c_deer
	animal[6].weapon = weapontype_unarmed
	animal[6].health = 110
	animal[6].damaged_by_player = true
	animal[6].name = "animal 6"
	
	animal_movement[6].animal_pos_0 = <<-1490.22, 4448.28, 17.03>>
	animal_movement[6].animal_pos_1 = <<-1487.36, 4468.40, 13.94>>
	animal_movement[6].animal_pos_2 = <<-1494.88, 4473.39, 15.39>>
	
	animal[7].pos = <<-1537.6617, 4451.6699, 12.6956>>
	animal[7].heading = 47.9748
	animal[7].model = a_c_deer
	animal[7].weapon = weapontype_unarmed
	animal[7].health = 110
	animal[7].damaged_by_player = true
	animal[7].name = "animal 7"
	
	animal_movement[7].animal_pos_0 = <<-1552.87, 4451.38, 11.94>>
	animal_movement[7].animal_pos_1 = <<-1570.00, 4474.65, 13.29>>
	animal_movement[7].animal_pos_2 = <<-1574.39, 4484.14, 16.42>>
	
	animal[8].pos = <<-1556.0271, 4413.7944, 1.4634>>
	animal[8].heading = 150.4522
	animal[8].model = a_c_deer
	animal[8].weapon = weapontype_unarmed
	animal[8].health = 110
	animal[8].damaged_by_player = true
	animal[8].name = "animal 8"
	
	animal_movement[8].animal_pos_0 = <<-1575.16, 4377.77, 1.00>>
	animal_movement[8].animal_pos_1 = <<-1572.54, 4400.54, -0.82>>
	animal_movement[8].animal_pos_2 = <<-1580.69, 4389.16, -0.05>>
	
	animal[9].pos = <<-1555.4232, 4392.6206, 4.9483>>
	animal[9].heading = 240.9626
	animal[9].model = a_c_deer
	animal[9].weapon = weapontype_unarmed
	animal[9].health = 110
	animal[9].damaged_by_player = true
	animal[9].name = "animal 9"
	
	animal_movement[9].animal_pos_0 = <<-1549.21, 4381.09, 3.24>>
	animal_movement[9].animal_pos_1 = <<-1539.72, 4386.36, 6.51>>
	animal_movement[9].animal_pos_2 = <<-1528.13, 4398.42, 14.80>>
	
	animal[10].pos = <<-1502.0311, 4409.0488, 19.1684>>
	animal[10].heading = 303.9064
	animal[10].model = a_c_deer
	animal[10].weapon = weapontype_unarmed
	animal[10].health = 110
	animal[10].damaged_by_player = true
	animal[10].name = "animal 10"
	
	animal_movement[10].animal_pos_0 = <<-1488.77, 4420.96, 20.74>>
	animal_movement[10].animal_pos_1 = <<-1478.69, 4430.50, 20.78>>
	animal_movement[10].animal_pos_2 = <<-1483.92, 4444.40, 18.41>>

	animal[11].pos = <<-1465.9343, 4457.2319, 15.2421>>
	animal[11].heading = 293.9127
	animal[11].model = a_c_deer
	animal[11].weapon = weapontype_unarmed
	animal[11].health = 110
	animal[11].damaged_by_player = true
	animal[11].name = "animal 11"
	
	animal_movement[11].animal_pos_0 = <<-1451.9957, 4455.7271, 20.1974>>
	animal_movement[11].animal_pos_1 = <<-1413.6312, 4475.2310, 19.4944>>
	animal_movement[11].animal_pos_2 = <<-1374.3199, 4467.6401, 23.8694>>
	
	animal[12].pos = <<-1533.0565, 4465.5620, 16.3504>>
	animal[12].heading = 154.9713
	animal[12].model = a_c_deer
	animal[12].weapon = weapontype_unarmed
	animal[12].health = 110
	animal[12].damaged_by_player = true
	animal[12].name = "animal 12"
	
	animal_movement[12].animal_pos_0 = <<-1520.3300, 4468.0098, 16.8353>>
	animal_movement[12].animal_pos_1 = <<-1502.4172, 4472.1909, 16.8261>>
	animal_movement[12].animal_pos_2 = <<-1480.8113, 4461.7622, 13.0587>>
	
	animal[13].pos = <<-1544.3579, 4470.2085, 18.6654>>
	animal[13].heading = 65.5701
	animal[13].model = a_c_deer
	animal[13].weapon = weapontype_unarmed
	animal[13].health = 110
	animal[13].damaged_by_player = true
	animal[13].name = "animal 13"
	
//	animal_movement[13].animal_pos_0 = <<-1546.5950, 4453.8257, 14.6312>>
//	animal_movement[13].animal_pos_1 = <<-1551.60, 4441.35, 10.25>>
//	animal_movement[13].animal_pos_2 = <<-1553.92, 4430.11, 7.89>>
	animal_movement[13].animal_pos_0 = <<-1546.15222, 4438.76318, 10.00660>>
	animal_movement[13].animal_pos_1 = <<-1549.37256, 4431.57666, 8.06592>>
	animal_movement[13].animal_pos_2 = <<-1549.31970, 4425.25439, 5.26764>>
	
	animal[14].pos = <<-1562.5874, 4475.5615, 17.6221>>
	animal[14].heading = 121.7079
	animal[14].model = a_c_deer
	animal[14].weapon = weapontype_unarmed
	animal[14].health = 110
	animal[14].damaged_by_player = true
	animal[14].name = "animal 14"
	
	animal_movement[14].animal_pos_0 = <<-1556.2760, 4456.6602, 15.3244>>
	animal_movement[14].animal_pos_1 = <<-1566.3101, 4449.7402, 11.3981>>
	animal_movement[14].animal_pos_2 = <<-1564.05, 4435.02, 7.09>>
	
	animal[15].pos = <<-1571.7758, 4467.3960, 12.9388>>
	animal[15].heading = 124.5367 
	animal[15].model = a_c_deer
	animal[15].weapon = weapontype_unarmed
	animal[15].health = 110
	animal[15].damaged_by_player = true
	animal[15].name = "animal 15"
	
	animal_movement[15].animal_pos_0 = <<-1560.9200, 4481.5098, 19.4704>>
	animal_movement[15].animal_pos_1 = <<-1587.4895, 4494.5811, 18.7403>>
	animal_movement[15].animal_pos_2 = <<-1596.4083, 4519.4780, 15.8511>>
	
	animal[16].pos = <<-1596.6232, 4477.6167, 14.6926>>
	animal[16].heading = 57.9158 
	animal[16].model = a_c_deer
	animal[16].weapon = weapontype_unarmed
	animal[16].health = 110
	animal[16].damaged_by_player = true
	animal[16].name = "animal 16"
	
	animal_movement[16].animal_pos_0 = <<-1593.12, 4463.00, 11.06>>
	animal_movement[16].animal_pos_1 = <<-1598.42310, 4480.75439, 15.73979>>
	animal_movement[16].animal_pos_2 = <<-1599.51672, 4492.65527, 18.75338>>
	
	animal[17].pos = <<-1593.8879, 4506.0298, 19.4619>>
	animal[17].heading = 345.5760 
	animal[17].model = a_c_deer
	animal[17].weapon = weapontype_unarmed
	animal[17].health = 110
	animal[17].damaged_by_player = true
	animal[17].name = "animal 17"
	
	animal_movement[17].animal_pos_0 = <<-1560.73, 4475.14, 13.50>>
	animal_movement[17].animal_pos_1 = <<-1548.74, 4463.98, 14.49>>
	animal_movement[17].animal_pos_2 = <<-1557.14, 4453.28, 11.88>>
	
	animal[18].pos = <<-1575.2400, 4482.1201, 20.9403>>
	animal[18].heading = 243.2081 
	animal[18].model = a_c_deer
	animal[18].weapon = weapontype_unarmed
	animal[18].health = 110
	animal[18].damaged_by_player = true
	animal[18].name = "animal 18"
	
	animal_movement[18].animal_pos_0 = <<-1566.05, 4467.64, 17.04>>
	animal_movement[18].animal_pos_1 = <<-1577.9872, 4438.6372, 7.7595>>
	animal_movement[18].animal_pos_2 = <<-1592.84741, 4435.65283, 3.48900>>
	
	big_foot.pos = <<-1481.0570, 4402.7729, 22.3394>>
	big_foot.heading = 156.8618
	big_foot.model = ig_orleans
	big_foot.weapon = weapontype_unarmed
	big_foot.health = 200
	big_foot.damaged_by_player = true
	
	franklin_cover_node[0].pos = <<-1512.5464, 4451.0454, 10.6712>>
	franklin_cover_node[1].pos = <<-1562.0181, 4475.0278, 17.6919>>
	franklin_cover_node[2].pos = <<-1562.5328, 4436.8013, 7.5904>>
	franklin_cover_node[3].pos = <<-1562.2717, 4475.6440, 17.6786>>
	franklin_cover_node[4].pos = <<-1600.7300, 4496.0059, 20.0414>>
	
	ambient_car.pos = <<-1570.9187, 4734.1489, 49.5480>>
	ambient_car.heading = 46.5972
	ambient_car.model = dubsta
	ambient_car.recording_number = 003
	
	pickup_pos[0] = <<-1584.9344, 4750.7358, 49.8765>>
	pickup_pos[1] = <<-1561.5793, 4572.1475, 17.6341>>
	
	pass_cutscene_vehicle.pos = <<1755.3712, 3284.6497, 40.1144>>
	pass_cutscene_vehicle.heading = 283.4647
	pass_cutscene_vehicle.model = cavalcade //cavalcade2
	
	selector_cam.bOKToSwitchPed = false
	selector_cam.bPedSwitched = false

//	SetPieceCarPos[0] = <<-1729.6932, 4332.7773, 63.4274>>
//	SetPieceCarQuatX[0] = -0.0520
//	SetPieceCarQuatY[0] = -0.0392
//	SetPieceCarQuatZ[0] = 0.3739
//	SetPieceCarQuatW[0] = 0.9252
//	SetPieceCarRecording[0] = 2
//	SetPieceCarStartime[0] = 39000.0000
//	SetPieceCarRecordingSpeed[0] = 1.0000
//	SetPieceCarModel[0] = dubsta
	
	rpg.model = GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG)
	
	for i = 0 to count_of(animal) - 1
		animal_ai_status[i] = 0
	endfor
	
	for i = 0 to count_of(gang_enemy) - 1
		gang_enemy[i].created = false
	endfor 
	
	eSwitchCamState = SWITCH_CAM_IDLE//SWITCH_CAM_START_SPLINE1
		
endproc 

func bool area_system_fail()
			
	if get_current_player_ped_enum() = char_franklin
	
		if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-1627.1, 4413.2, 3.66>>) < 450.00
			
			if is_this_print_being_displayed("FS_GOD_3")
				clear_this_print("FS_GOD_3")
			endif 
			
			area_fail_time = get_game_timer()
			
		else 
		
			if not outside_area_text_printed
			
				print_now("FS_GOD_3", default_god_text_time, 1)
				printnl()
				
				outside_area_text_printed = true
		
			endif 
			
			if lk_timer(area_fail_time, 7000)
			
				if does_entity_exist(gang_enemy[3].ped)
					mission_failed_text = "FS_FAIL_4"
				else
					mission_failed_text = "FS_FAIL_0"
				endif 
				
				return true 	
				
			endif 
			
		endif 
		
	endif 
	
	return false

endfunc 

func bool mission_fail_checks()

	switch mission_flow
	
		case stage_opening_mocap
		
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_Michael])
				return true
			endif 
			
			if mission_vehicle_injured(trevors_truck.veh)
				return true
			endif 

			if mission_vehicle_injured(helicopter.veh)
				return true
			endif 
		
		break 
		
		case stage_get_to_mountains

			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
				mission_failed_text = "FS_MICHDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
				mission_failed_text = "FS_FRANDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "FS_TREVDEAD"
				return true
			endif
			
			if mission_ped_injured(chop.ped)
				mission_failed_text = "FS_CHOP_DEAD"
				return true 
			endif 

			if mission_vehicle_injured(helicopter.veh)
				mission_failed_text = "FS_HELIDEAD"
				return true
			endif 
			
			if is_vehicle_stuck_every_check(helicopter.veh)
				mission_failed_text = "FS_HELISTUCK"
				return true 
			endif 
			
			if mission_vehicle_injured(franklins_car.veh)
				mission_failed_text = "FS_CARDEAD"
				return true
			endif 
			
			if get_current_player_ped_enum() = char_franklin
				if is_vehicle_stuck_every_check(franklins_car.veh)
					mission_failed_text = "FS_CARSTUCK"
					return true 
				endif 
			endif 

			if mission_vehicle_injured(trevors_truck.veh)
				mission_failed_text = "FS_CARDEAD2"
				return true
			endif 

			if get_current_player_ped_enum() = char_trevor
				if is_vehicle_stuck_every_check(trevors_truck.veh)
					mission_failed_text = "FS_CARSTUCK2"
					return true 
				endif 
			endif 
			
			if mission_vehicle_injured(gang_car.veh)
			endif 

			
			switch get_current_player_ped_enum() 
			
				case char_franklin
				
					if not selector_cam.bSplineActive
						if is_vehicle_driveable(gang_car.veh)
							if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(gang_car.veh)) > 200.00
								if not is_entity_on_screen(gang_car.veh)
									mission_failed_text = "FS_FAIL_0"
									return true
								else 
									 if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(gang_car.veh)) > 300.00
										mission_failed_text = "FS_FAIL_0"
										return true
									endif 
								endif 
							endif 
						endif 
					endif 
					
					//driving forward up the road instead of driving down the dirt track.
					if is_entity_in_angled_area(player_ped_id(), <<-2203.082, 4411.719, 48.152>>, <<-2155.247, 4394.323, 76.352>>, 5.000)
						
						int i
						
						for i = 0 to count_of(gang_enemy) - 1
							if does_entity_exist(gang_enemy[i].ped)
								delete_ped(gang_enemy[i].ped)
							endif 
						endfor 
						
						if does_entity_exist(gang_car.veh)
							delete_vehicle(gang_car.veh)
						endif 
						
						mission_failed_text = "FS_FAIL_0"

						return true
					endif 
					
					if crash_cutscene_fail	

						mission_failed_text = "FS_FAIL_0"

						return true
						
					endif 

					//player drove off cliff
					vector player_pos 
					player_pos = get_entity_coords(player_ped_id())
					
					if player_pos.z < 10.00
						mission_failed_text = "FS_FAIL_0"
						return true
					endif 
					
					//added as a safety precaution to stop the player being stuck on the mission for bug 1587095
					if is_playback_going_on_for_vehicle(gang_car.veh)
						if get_time_position_in_recording(gang_car.veh) > 92000
							mission_failed_text = "FS_FAIL_0"
							return true
						endif 
					endif 
				break 
				
				case char_trevor 
				
					if does_entity_exist(selector_ped.pedID[selector_ped_michael])
						if not is_entity_on_screen(selector_ped.pedID[selector_ped_michael])
							if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) > 200.00
							
								mission_failed_text = "FS_FAIL_1"
							
								return true 
								
							endif 
							
						else 
						
							if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) > 300.00
							
								mission_failed_text = "FS_FAIL_1"
							
								return true 
								
							endif 
							
						endif 
					endif 
				
				break 
				
			endswitch 
				
		break 
		
		case STAGE_CRASH_CUTSCENE

			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
				mission_failed_text = "FS_MICHDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
				mission_failed_text = "FS_FRANDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text ="FS_TREVDEAD"
				return true
			endif
			
			if mission_ped_injured(chop.ped)
				mission_failed_text = "FS_CHOP_DEAD"
				return true 
			endif 

			if mission_vehicle_injured(helicopter.veh)
				mission_failed_text = "FS_HELIDEAD"
				return true
			endif 
			
			if mission_vehicle_injured(franklins_car.veh)
				mission_failed_text = "FS_CARDEAD"
				return true
			endif 
			
			if mission_vehicle_injured(trevors_truck.veh)
				return true
			endif 
			
			//added as a safety precaution to stop the player being stuck on the mission for bug 1587095
			if is_playback_going_on_for_vehicle(gang_car.veh)
				if get_time_position_in_recording(gang_car.veh) > 92000
					mission_failed_text = "FS_FAIL_0"
					return true
				endif 
			endif 
		
		break 
		
		case STAGE_INVESTIGATE_CRASH
		case stage_get_helicopter
		
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
				mission_failed_text = "FS_MICHDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
				mission_failed_text = "FS_FRANDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "FS_TREVDEAD"
				return true
			endif
			
			if mission_ped_injured(chop.ped)
				mission_failed_text = "FS_CHOP_DEAD"
				return true 
			endif 

			if mission_vehicle_injured(helicopter.veh)
				mission_failed_text = "FS_HELIDEAD"
				return true
			endif 
			
			if is_vehicle_stuck_every_check(helicopter.veh)
				mission_failed_text = "FS_HELISTUCK"
				return true 
			endif 
			
			if mission_vehicle_injured(trevors_truck.veh)
				mission_failed_text = "FS_CARDEAD2"
				return true
			endif 
			
			if get_current_player_ped_enum() = char_trevor
			and not is_player_switch_in_progress()
				
				if is_vehicle_stuck_every_check(trevors_truck.veh)
					mission_failed_text = "FS_CARSTUCK2"
					return true 
				endif 
				
			endif 
			
			switch get_current_player_ped_enum()
			
				case char_franklin

					if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-1874.09, 4485.97, 25.35>>) > 400.00
						mission_failed_text = "FS_FAIL_8"
						return true 	
					endif 
					
				break 
				
				case char_trevor
				
					if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-1874.7830, 4484.7104, 90.00>>) < 120.00
						if get_player_wanted_level(player_id()) > 0
							mission_failed_text = "FS_FAIL_2"
							return true 	
						endif 
					endif 
					
					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[SELECTOR_PED_michael])) > 200.00
						mission_failed_text = "FS_FAIL_1"
						return true
					endif 
					
					if get_helicopter_status = 0
					
						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(helicopter.veh)) > 1000.00
							mission_failed_text = "FS_FAIL_10"
							return true
						endif 
					
					elif get_helicopter_status > 0
						
						if not is_ped_sitting_in_vehicle(player_ped_id(), helicopter.veh)
							if get_entity_height_above_ground(helicopter.veh) > 15
								mission_failed_text = "FS_FAIL_10"
								return true
							endif 
						endif 
						
						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(helicopter.veh)) > 200.00
							mission_failed_text = "FS_FAIL_10"
							return true
						endif 

					endif 			

				break 

			endswitch 

		break 
		
		CASE STAGE_SWAP_TO_MICHAEL_CUTSCENE
		
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
				mission_failed_text = "FS_MICHDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
				mission_failed_text = "FS_FRANDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "FS_TREVDEAD"
				return true
			endif
			
			if mission_ped_injured(chop.ped)
				mission_failed_text = "FS_CHOP_DEAD"
				return true 
			endif 

			if mission_vehicle_injured(helicopter.veh)
				mission_failed_text = "FS_HELIDEAD"
				return true
			endif 
			
			if is_vehicle_stuck_every_check(helicopter.veh)
				mission_failed_text = "FS_HELISTUCK"
				return true 
			endif 

			if mission_vehicle_injured(trevors_truck.veh)
				mission_failed_text = "FS_CARDEAD2"
				return true
			endif 

			if is_vehicle_stuck_every_check(trevors_truck.veh)
				mission_failed_text = "FS_CARSTUCK2"
				return true 
			endif 
			
			if get_current_player_ped_enum() = char_trevor
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[SELECTOR_PED_MICHAEL])) > 200.00
					mission_failed_text = "FS_FAIL_3"
					return true 	 
				endif 
			endif 
			
		BREAK
		
		CASE STAGE_SNIPE_ENEMIES
		
			if mission_vehicle_injured(helicopter.veh)
				mission_failed_text = "FS_HELIDEAD"
				return true
			endif 

			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
				mission_failed_text = "FS_MICHDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
				mission_failed_text = "FS_FRANDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "FS_TREVDEAD"
				return true
			endif
			
			if mission_ped_injured(chop.ped)
				mission_failed_text = "FS_CHOP_DEAD"
				return true
			endif
			
			if get_current_player_ped_enum() = char_franklin
				if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-1627.1, 4413.2, 3.66>>) > 450.00
					if does_entity_exist(gang_enemy[3].ped)
						mission_failed_text = "FS_FAIL_4"
					else
						mission_failed_text = "FS_FAIL_0"
					endif 
					return true 	
				endif 
			endif 
			
			area_system_fail()

			
			if is_vehicle_driveable(ambient_car.veh)
				if is_playback_going_on_for_vehicle(ambient_car.veh)
					if get_time_position_in_recording(ambient_car.veh) > 3000
						if does_entity_exist(gang_enemy[3].ped)
							mission_failed_text = "FS_FAIL_4"
						else
							mission_failed_text = "FS_FAIL_0"
						endif 
						return true 	
					endif 
				endif 
			endif 
			
			//have huge area beside the chooer is franklin goes out of it fail mission.
			//usefull for camera stuff so don't need to worry about collision 
//			if not is_entity_in_angled_area(player_ped_id())
//				clear_prints()
//				clear_help()
//				print_now("FS_LOSTGANG", default_god_text_time, 1)
//				
//				if get_current_player_ped_enum() = char_michael
//					mission_flow = mission_fail_cutscene_helicopter
//				else 
//					mission_flow = mission_fail_cutscene
//				endif 
//				
//				return true 
//			endif 
			
//			if timer_system_status > 0
//				IF TIMER_DO_WHEN_READY(count_down_timer, 0)
//					//if not HAS_ANY_SELECTOR_PED_BEEN_SELECTED(selector_ped) OR not Selector_Cam.bSplineActive
//					if not Selector_Cam.bSplineActive
//						set_entity_proofs(helicopter.veh, true, true, true, true, true)
//						clear_prints()
//						clear_help()
//						print_now("FS_OUTTIME", default_god_text_time, 1)
//						
//						if get_current_player_ped_enum() = char_michael
//							mission_flow = mission_fail_cutscene_helicopter
//						else 
//							mission_flow = mission_fail_cutscene
//						endif 
//						
//						return true
//					endif 
//				ENDIF
//			endif 
			
		BREAK
		
		case stage_mission_pass_conversation
		case stage_switch_to_trevor
		case stage_pickup_franklin_and_chop
		
			if mission_vehicle_injured(helicopter.veh)
				mission_failed_text = "FS_HELIDEAD"
				return true
			endif 

			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
				mission_failed_text = "FS_MICHDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
				mission_failed_text = "FS_FRANDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "FS_TREVDEAD"
				return true
			endif
			
			if mission_ped_injured(chop.ped)
				mission_failed_text = "FS_CHOP_DEAD"
				return true
			endif
			
			switch  get_current_player_ped_enum() 
			
				case char_franklin
					
					if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-1627.1, 4413.2, 3.66>>) > 600.00
						mission_failed_text = "FS_FAIL_5"
						return true 	
					endif 
					
				break 
				
				case char_trevor
				
					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[SELECTOR_PED_franklin])) > 700.00
						mission_failed_text = "FS_FAIL_6"
						return true 	
					endif 
					
					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) > 700.00
						mission_failed_text = "FS_FAIL_8"
						return true 	
					endif 
					
					
					if mission_flow = stage_pickup_franklin_and_chop
						
						if not is_ped_sitting_in_vehicle(player_ped_id(), helicopter.veh)
							if get_entity_height_above_ground(helicopter.veh) > 15
								mission_failed_text = "FS_FAIL_10"
								return true
							endif 
						endif 
						
						if is_vehicle_stuck_every_check(helicopter.veh)
							mission_failed_text = "FS_HELISTUCK"
							return true 
						endif 
						
					endif 	
					
				break 
				
			endswitch 
			
		break 
		
		case stage_get_to_airstrip
		
			if mission_vehicle_injured(helicopter.veh)
				mission_failed_text = "FS_HELIDEAD"
				return true

			endif 

			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
				mission_failed_text = "FS_MICHDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
				mission_failed_text = "FS_FRANDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "FS_TREVDEAD"
				return true
			endif
			
			if mission_ped_injured(chop.ped)
				mission_failed_text = "FS_CHOP_DEAD"
				return true
			endif
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(helicopter.veh)) > 200.00
				mission_failed_text = "FS_FAIL_10"
				return true 	
			endif 

			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[SELECTOR_PED_michael])) > 200.00
				mission_failed_text = "FS_FAIL_3"
				return true 	
			endif 
	
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[SELECTOR_PED_franklin])) > 200.00
				mission_failed_text = "FS_FAIL_7"
				return true 	
			endif 
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) > 200.00
				mission_failed_text = "FS_FAIL_8"
				return true 	
			endif 
			
			if not is_ped_in_vehicle(player_ped_id(), helicopter.veh)
				if get_entity_height_above_ground(helicopter.veh) > 15
					mission_failed_text = "FS_FAIL_10"
					return true
				endif 
			endif 
			
			if is_vehicle_stuck_every_check(helicopter.veh)
				mission_failed_text = "FS_HELISTUCK"
				return true 
			endif 
		
		break 
		
		case stage_pass_mocap
		
			if mission_vehicle_injured(helicopter.veh)
				mission_failed_text = "FS_HELIDEAD"
				return true

			endif 

			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
				mission_failed_text = "FS_MICHDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
				mission_failed_text = "FS_FRANDEAD"
				return true
			endif
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "FS_TREVDEAD"
				return true
			endif
			
			if mission_ped_injured(chop.ped)
				mission_failed_text = "FS_CHOP_DEAD"
				return true
			endif
		
		break 

	endswitch 
	
	return false

endfunc 

/// PURPOSE: Removes all the mission models. This removes all the models that Alwyn has request during the switch cam to     
///    franklin. Plus more which means it can also be used in the z skip. 
proc remove_all_mission_models()

	//fix for failing mission before initialise mission variables is called
	chop.model = GET_CHOP_MODEL()
	gang_enemy[0].model = a_m_m_hillbilly_02 
	franklins_car.model = BALLER
	gang_car.model = dubsta
	helicopter.model = frogger
	trevors_truck.model = get_player_veh_model(char_trevor) 

	set_model_as_no_longer_needed(franklin_model)
	set_model_as_no_longer_needed(chop.model)
	set_model_as_no_longer_needed(gang_enemy[0].model)
	set_model_as_no_longer_needed(franklins_car.model)
	set_model_as_no_longer_needed(gang_car.model)
	set_model_as_no_longer_needed(radi)
	set_model_as_no_longer_needed(sabregt)
	set_model_as_no_longer_needed(helicopter.model)
	set_model_as_no_longer_needed(trevors_truck.model)
	remove_PTFX_ASSET()  
	remove_vehicle_recording(003, "lkexcile2")
	remove_vehicle_recording(004, "lkexcile2")
	remove_vehicle_recording(005, "lkexile2")
	remove_vehicle_recording(001, "lkexcile2_chase")
	remove_vehicle_recording(002, "lkexcile2_chase")
	remove_vehicle_recording(003, "lkexcile2_chase")
	remove_vehicle_recording(004, "lkexcile2_chase")
	remove_vehicle_recording(005, "lkexcile2_chase")
	remove_vehicle_recording(006, "lkexcile2_chase")
	remove_vehicle_recording(007, "lkexcile2_chase")
	remove_vehicle_recording(008, "lkexcile2_chase")
	remove_vehicle_recording(009, "lkexcile2_chase")
	remove_vehicle_recording(010, "lkexcile2_chase")
	remove_vehicle_recording(011, "lkexcile2_chase")
	remove_vehicle_recording(012, "lkexcile2_chase")
	remove_vehicle_recording(013, "lkexcile2_chase")
	remove_vehicle_recording(014, "lkexcile2_chase")
	remove_vehicle_recording(015, "lkexcile2_chase")
	remove_vehicle_recording(016, "lkexcile2_chase")
	remove_vehicle_recording(017, "lkexcile2_chase")
	remove_vehicle_recording(018, "lkexcile2_chase")
	remove_vehicle_recording(019, "lkexcile2_chase")
	remove_vehicle_recording(100, "lkexcile2_chase")

endproc 

///PURPOSE: resets all the ambient player ped system variables for trevor. bug 2040115
proc reset_player_system_variables()
		
	g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[CHAR_TREVOR] = <<0,0,0>>
	g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[CHAR_TREVOR] = 0
	g_savedGlobals.sPlayerData.sInfo.iLastKnownRoomKey[CHAR_TREVOR] = 0
	g_savedGlobals.sPlayerData.sInfo.vLastKnownVelocity[CHAR_TREVOR] = <<0,0,0>>
	
endproc 

PROC MISSION_CLEANUP() //bool forcecleanup = true

	kill_any_conversation()
	
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
	
	set_fake_wanted_level(0)
	set_max_wanted_level(6)
	set_create_random_cops(true)
	set_wanted_level_multiplier(1.0)
	
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	SET_DECAL_BULLET_IMPACT_RANGE_SCALE(1.0)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		
		if not IS_PED_IN_ANY_VEHICLE(PLAYER_ped_ID())
			if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
				endif 
			endif 
		ENDIF
		
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(player_ped_id())
    	RESET_PED_MOVEMENT_CLIPSET(player_ped_id())
		RESET_PED_STRAFE_CLIPSET(player_ped_id())

		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
		if not is_entity_attached(player_ped_id())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		endif 
		
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
		
		SET_PLAYER_FORCED_AIM(player_id(), false)
		
		SET_PED_USING_ACTION_MODE(player_ped_id(), false)
		
		set_ped_config_flag(player_ped_id(), PCF_ForceDirectEntry, false)
		
	ENDIF
	
	reset_player_system_variables()

	clear_mission_locate_stuff(locates_data)

	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	
	UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, <<0,0,0>>, 0.0)

	disable_cellphone(false)
	HIDE_ACTIVE_PHONE(FALSE)
	ALLOW_DIALOGUE_IN_WATER(false)
	
	BLOCK_MISSION_TITLE(false)

	cleanup_uber_playback(false)
	
	cleanup_selector_cam(selector_cam)
	
	RELEASE_CHOP_CAM_SFX(chop_cam)
	COMMON_DISABLE_CHOP_VIEW_CAM(chop_cam, selector_ped)
	
	CLEAR_CONTROL_LIGHT_EFFECT(player_control) //clears the light effect on the ps4 controler when switching to chop
	
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, false)
	
	ENABLE_PROCOBJ_CREATION()
	
	trigger_music_event("EXL2_MISSION_FAIL")
	
	destroy_all_cams()
		
	CLEAR_FOCUS()
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)

	//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
	//SET_PED_DENSITY_MULTIPLIER(1.0)
	SET_SEETHROUGH(FALSE)
	
	SET_ROADS_BACK_TO_ORIGINAL(vCountryRoadMin, vCountryRoadMax)
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)

	remove_anim_dict(strAnimDictCamShake)
	
	if does_entity_exist(rpg_obj)
		add_explosion(get_entity_coords(rpg_obj), EXP_TAG_ROCKET, 1.0) 
		delete_entity(rpg_obj)
		if not has_sound_finished(rpg_sound)
			stop_sound(rpg_sound)
		endif 
	endif 

	IF does_particle_fx_looped_exist(i_ptfx_fire)
		STOP_PARTICLE_FX_LOOPED(i_ptfx_fire)
	ENDIF
	
	remove_all_mission_models() 
	
	set_time_scale(1.0)

	terminate_this_thread()

ENDPROC

proc delete_all_mission_assets_for_fail()
	
	set_time_scale(1.0)
	
	clear_player_wanted_level(player_id())
	set_fake_wanted_level(0)
	set_max_wanted_level(6)
	set_create_random_cops(true)
	set_wanted_level_multiplier(1.0)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		
		if not IS_PED_IN_ANY_VEHICLE(PLAYER_ped_ID())
			if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
				endif 
			endif 
		ENDIF

		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		clear_ped_tasks(player_ped_id())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
		if not is_entity_attached(player_ped_id())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		endif 
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
		
		SET_PLAYER_FORCED_AIM(player_id(), false)
		
		set_ped_config_flag(player_ped_id(), PCF_ForceDirectEntry, false)
		
	ENDIF
	
	REMOVE_ALL_BLIPS()
	REMOVE_ALL_CAMERAS()
	REMOVE_ALL_OBJECTS()
	REMOVE_ALL_PEDS(true)
	REMOVE_ALL_VEHICLES(true)
	
	trigger_music_event("EXL2_MISSION_FAIL")
	
	stop_audio_scenes()
	
	STOP_STREAM()
	
	remove_all_mission_models()
	
	CLEAR_TRIGGERED_LABELS()
	i_current_event = 0
	
	kill_any_conversation()
	clear_help()
	clear_prints()

	clear_mission_locate_stuff(locates_data)

	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)

	disable_cellphone(false)
	HIDE_ACTIVE_PHONE(FALSE)

	cleanup_uber_playback(false)
	
	cleanup_selector_cam(selector_cam)
		
	COMMON_DISABLE_CHOP_VIEW_CAM(chop_cam, selector_ped)
//	SET_CHOP_VIEW_CAM_TO_PLAYER(chop_cam, selector_ped)
//	chop_cam.state = CHOP_VIEW_CAM_DISABLED
	destroy_all_cams()
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	CLEAR_FOCUS()
	
	STREAMVOL_DELETE(streaming_volume)

	//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
	//SET_PED_DENSITY_MULTIPLIER(1.0)
	SET_SEETHROUGH(FALSE)
	
	
	SET_ROADS_BACK_TO_ORIGINAL(vCountryRoadMin, vCountryRoadMax)
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)

	set_selector_ped_blocked(selector_ped, SELECTOR_PED_michael, false)
	set_selector_ped_blocked(selector_ped, SELECTOR_PED_franklin, false)
	set_selector_ped_blocked(selector_ped, SELECTOR_PED_TREVOR, false)
	
	if does_entity_exist(rpg_obj)
		add_explosion(get_entity_coords(rpg_obj), EXP_TAG_ROCKET, 1.0) 
		delete_entity(rpg_obj)
		if not has_sound_finished(rpg_sound)
			stop_sound(rpg_sound)
		endif 
	endif 

	IF does_particle_fx_looped_exist(i_ptfx_fire)
		STOP_PARTICLE_FX_LOOPED(i_ptfx_fire)
	ENDIF
	
	remove_ped_for_dialogue(scripted_speech, 0)
	remove_ped_for_dialogue(scripted_speech, 1)
	remove_ped_for_dialogue(scripted_speech, 2)
	remove_ped_for_dialogue(scripted_speech, 3)

	
	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		delete_ped(selector_ped.pedID[selector_ped_michael])
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
		delete_ped(selector_ped.pedID[selector_ped_franklin])
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
		delete_ped(selector_ped.pedID[selector_ped_trevor])
	endif 
	
	if is_valid_interior(tunnel_inerior[0])
		unpin_interior(tunnel_inerior[0])
	endif 
	
	if is_valid_interior(tunnel_inerior[1])
		unpin_interior(tunnel_inerior[1])
	endif 
	
endproc 

proc remove_all_mission_assets()

	if IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE()
	endif 

	while is_cutscene_active()
		wait(0)
	endwhile
	
	REMOVE_CUTSCENE()
	
	set_time_scale(1.0)
	
	SET_DECAL_BULLET_IMPACT_RANGE_SCALE(1.0)
	
	SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(1.0)
	
	clear_player_wanted_level(player_id())
	set_fake_wanted_level(0)
	set_max_wanted_level(6)
	set_create_random_cops(true)
	set_wanted_level_multiplier(1.0)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		
		if not IS_PED_IN_ANY_VEHICLE(PLAYER_ped_ID())
			if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
				endif 
			endif 
		ENDIF

		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
		if not is_entity_attached(player_ped_id())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		endif 
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
		
		SET_PLAYER_FORCED_AIM(player_id(), false)
		
		set_ped_config_flag(player_ped_id(), PCF_ForceDirectEntry, false)
		
	ENDIF
	
	REMOVE_ALL_BLIPS()
	REMOVE_ALL_CAMERAS()
	REMOVE_ALL_OBJECTS()
	REMOVE_ALL_PEDS(true)
	REMOVE_ALL_VEHICLES(true)
	remove_all_animations()
	
	trigger_music_event("EXL2_MISSION_FAIL")
	
	stop_audio_scenes()
	
	STOP_STREAM()
	
	REPLAY_CANCEL_EVENT()
	
	remove_all_mission_models()
	
	CLEAR_TRIGGERED_LABELS()
	i_current_event = 0
	
	kill_any_conversation()
	clear_help()
	clear_prints()

	clear_mission_locate_stuff(locates_data)

	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)

	disable_cellphone(false)
	HIDE_ACTIVE_PHONE(FALSE)

	cleanup_uber_playback(true)
		
	switch_off_vehicle_generators_for_mission()
	
	cleanup_selector_cam(selector_cam)
		
	COMMON_DISABLE_CHOP_VIEW_CAM(chop_cam, selector_ped)
//	SET_CHOP_VIEW_CAM_TO_PLAYER(chop_cam, selector_ped)
//	chop_cam.state = CHOP_VIEW_CAM_DISABLED
	destroy_all_cams()
	RESET_PUSH_IN(push_in_camera)
	
	CLEAR_CONTROL_LIGHT_EFFECT(player_control)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	CLEAR_FOCUS()
	
	STREAMVOL_DELETE(streaming_volume)

	//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
	//SET_PED_DENSITY_MULTIPLIER(1.0)
	SET_SEETHROUGH(FALSE)
	
	
	SET_ROADS_BACK_TO_ORIGINAL(vCountryRoadMin, vCountryRoadMax)
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	
	ENABLE_PROCOBJ_CREATION()

	set_selector_ped_blocked(selector_ped, SELECTOR_PED_michael, false)
	set_selector_ped_blocked(selector_ped, SELECTOR_PED_franklin, false)
	set_selector_ped_blocked(selector_ped, SELECTOR_PED_TREVOR, false)
	
	if does_entity_exist(rpg_obj)
		add_explosion(get_entity_coords(rpg_obj), EXP_TAG_ROCKET, 1.0) 
		delete_entity(rpg_obj)
		if not has_sound_finished(rpg_sound)
			stop_sound(rpg_sound)
		endif 
	endif 

	IF does_particle_fx_looped_exist(i_ptfx_fire)
		STOP_PARTICLE_FX_LOOPED(i_ptfx_fire)
	ENDIF
	
	remove_ped_for_dialogue(scripted_speech, 0)
	remove_ped_for_dialogue(scripted_speech, 1)
	remove_ped_for_dialogue(scripted_speech, 2)
	remove_ped_for_dialogue(scripted_speech, 3)

	
	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		delete_ped(selector_ped.pedID[selector_ped_michael])
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
		delete_ped(selector_ped.pedID[selector_ped_franklin])
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
		delete_ped(selector_ped.pedID[selector_ped_trevor])
	endif 
	
	if is_valid_interior(tunnel_inerior[0])
		unpin_interior(tunnel_inerior[0])
	endif 
	
	if is_valid_interior(tunnel_inerior[1])
		unpin_interior(tunnel_inerior[1])
	endif 
	
	CLEAR_AREA(get_entity_coords(player_ped_id()), 10000.0, TRUE)

endproc 





//PROC DISPLAY_TIME_STAT(FLOAT fXpos, FLOAT fYpos, STRING sTimeTaken,BOOL bSuccess, STRING sTitle = NULL)
//	SET_TEXT_SCALE(STAT_X_SCALE, STAT_Y_SCALE)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(10, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	IF IS_STRING_NULL(sTitle)
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos, fYpos, "STRING", "Time")
//	ELSE
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos, fYpos, "STRING", sTitle)
//	ENDIF
//	
//	SET_TEXT_SCALE(STAT_X_SCALE, STAT_Y_SCALE)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(10, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos + RHS_X_OFFSET, fYpos, "STRING", sTimeTaken)
//	
//	IF bSuccess
//		DRAW_SPRITE("DaveW","tick_white",fXpos + TICK_BOX_X_OFF, fYpos + TICK_BOX_Y_OFF, 0.0110, 0.0220, 0.0000, 255, 255, 255, 255)
//	ELSE
//		DRAW_SPRITE("DaveW","box_white",fXpos + TICK_BOX_X_OFF, fYpos + TICK_BOX_Y_OFF, 0.0110, 0.0220, 0.0000, 255, 255, 255, 255)
//	ENDIF
//	
////	SCRIPT_ASSERT("Hit")
////	DRAW_RECT(0.2210, 0.1900, 0.0110, 0.0220, 255, 255, 255, 255)
//	// For tickbox
//	// X_pos is MISSION_STAT_X + 0.171
//	// Y_Pos is MISSION_STAT_Y + 0.012
//	
//ENDPROC

//PROC DISPLAY_INT_STAT(FLOAT fXpos, FLOAT fYpos, INT iToDisplay,BOOL bSuccess, STRING sTitle = NULL)
//	
//	SET_TEXT_SCALE(STAT_X_SCALE, STAT_Y_SCALE)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(10, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	IF IS_STRING_NULL(sTitle)
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos, fYpos, "STRING", "Number")
//	ELSE
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos, fYpos, "STRING", sTitle)
//	ENDIF
//	
//	TEXT_LABEL t1 = ""
//	t1+= iToDisplay
//	SET_TEXT_SCALE(STAT_X_SCALE, STAT_Y_SCALE)
//	IF bSuccess
//		DRAW_SPRITE("DaveW","tick_white",fXpos + TICK_BOX_X_OFF, fYpos + TICK_BOX_Y_OFF, 0.0110, 0.0220, 0.0000, 255, 255, 255, 255)
//	ELSE
//		DRAW_SPRITE("DaveW","box_white",fXpos + TICK_BOX_X_OFF, fYpos + TICK_BOX_Y_OFF, 0.0110, 0.0220, 0.0000, 255, 255, 255, 255)
//	ENDIF
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(10, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos + RHS_X_OFFSET, fYpos, "STRING", t1)
//	
//ENDPROC

//PROC DISPLAY_PERCENT_STAT(FLOAT fXpos, FLOAT fYpos, INT iToDisplay,BOOL bSuccess, STRING sTitle = NULL)
//	SET_TEXT_SCALE(STAT_X_SCALE, STAT_Y_SCALE)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(10, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	IF IS_STRING_NULL(sTitle)
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos, fYpos, "STRING", "Percent")
//	ELSE
//		DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos, fYpos, "STRING", sTitle)
//	ENDIF
//	TEXT_LABEL t1 = ""
//	t1+= iToDisplay
//	t1 += "%"
//	SET_TEXT_SCALE(STAT_X_SCALE, STAT_Y_SCALE)
//	IF bSuccess
//		DRAW_SPRITE("DaveW","tick_white",fXpos + TICK_BOX_X_OFF, fYpos + TICK_BOX_Y_OFF, 0.0110, 0.0220, 0.0000, 255, 255, 255, 255)
//	ELSE
//		DRAW_SPRITE("DaveW","box_white",fXpos + TICK_BOX_X_OFF, fYpos + TICK_BOX_Y_OFF, 0.0110, 0.0220, 0.0000, 255, 255, 255, 255)
//	ENDIF
//	SET_TEXT_COLOUR(255, 255, 255,255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(10, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos+ RHS_X_OFFSET, fYpos, "STRING", t1)
//ENDPROC

//PROC DISPLAY_TOTAL_STAT(FLOAT fXpos, FLOAT fYpos, INT iPercentTotal)
//	/*
//		MISSION_NAME_X_SCALE				0.24
//		MISSION_NAME_Y_SCALE				0.59
//	*/
//	SET_TEXT_SCALE(TOTAL_X_SCALE, TOTAL_Y_SCALE)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos, fYpos, "STRING", "Total")
//	
//	TEXT_LABEL t1 = ""
//	t1+= iPercentTotal
//	t1 += "%"
//	SET_TEXT_SCALE(MISSION_NAME_X_SCALE, MISSION_NAME_Y_SCALE)
//	SET_TEXT_COLOUR(255, 255, 255,255)
//	SET_TEXT_CENTRE(FALSE)
//	SET_TEXT_DROPSHADOW(10, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos+ RHS_X_OFFSET, fYpos, "STRING", t1)
//ENDPROC


//PROC DO_MISSION_PASSED_SCREEN()
//
//	
//	
//
//
//	REQUEST_STREAMED_TEXTURE_DICT("DaveW")
//	WHILE NOT HAS_STREAMED_TEXTURE_DICT_LOADED("DaveW")
//		WAIT(0)
//		REQUEST_STREAMED_TEXTURE_DICT("DaveW")
//		
//	ENDWHILE		
//
//	//Main background 
//	DRAW_RECT(0.1390, 0.1930, 0.1900, 0.2430, 10, 10, 10, 180)
//
//	//Display the menu title		
//	SET_TEXT_SCALE(0.2400, 0.6800)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(TRUE)
//	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	DISPLAY_TEXT_WITH_LITERAL_STRING(MISSION_PASSED_X, MISSION_PASSED_Y, "STRING", "Mission Passed")
//	
//	// Mission title
//	SET_TEXT_SCALE(MISSION_NAME_X_SCALE, MISSION_NAME_Y_SCALE)
//	SET_TEXT_COLOUR(255, 255, 255, 255)
//	SET_TEXT_CENTRE(TRUE)
//	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
//	SET_TEXT_EDGE(0, 0, 0, 0, 255)
//	//DISPLAY_TEXT_WITH_LITERAL_STRING(0.1360, 0.1180, "STRING", "Forest Snipe")
//	DISPLAY_TEXT_WITH_LITERAL_STRING(MISSION_NAME_X, MISSION_NAME_Y, "STRING", "Forest Snipe")
//
//
//	/*
//		NEW_LINE_HEIGHT			0.0300
//		MISSION_STAT_X 				0.0500 
//		MISSION_STAT_Y 				0.177
//		
//		STAT_X_SCALE 						0.2400 
//	 	STAT_Y_SCALE						0.3300
//		
//		TOTAL_X_CENTRE					0.05
//	*/
//	
//	//DISPLAY_TIME_STAT(MISSION_STAT_X, MISSION_STAT_Y, "12:15", TRUE)
//	DISPLAY_PERCENT_STAT(MISSION_STAT_X, MISSION_STAT_Y + NEW_LINE_HEIGHT, 95, TRUE, "Accuracy")
//	DISPLAY_INT_STAT(MISSION_STAT_X, MISSION_STAT_Y + (NEW_LINE_HEIGHT * 2), 10, FALSE, "Wildlife killed")
//	DISPLAY_TOTAL_STAT(TOTAL_X_CENTRE, MISSION_STAT_Y + (NEW_LINE_HEIGHT *3) + (0.5 * NEW_LINE_HEIGHT),  66)
//ENDPROC

PROC MISSION_PASSED()

	Mission_Flow_Mission_Passed()
	
	MISSION_CLEANUP() 
		
ENDPROC

PROC OPENING_MOCAP()
	
	IF e_section_stage = SECTION_STAGE_SETUP
		
		REQUEST_CUTSCENE("Exile_2_int")
		
		if DO_TIMELAPSE(SP_MISSION_EXILE_2, sTimelapse, true)  
			
			if HAS_CUTSCENE_LOADED()

				switch get_current_player_ped_enum() 
				
					case char_michael
				
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(char_trevor))
						
					break 
			
					case char_trevor 
					
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "michael", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(char_michael))
					
					break 
				
				endswitch 
				
				//REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevors_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_veh_model(char_trevor))

				set_scripts_safe_for_cutscene(true)
				
				start_cutscene()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				//HACK FIX REMOVE ONCE UPDATE BUG HAS BEEN FIXED
				//wait(0)
				//
				
				e_section_stage = section_stage_running
				
			endif 
		endif 
		
	endif 
	
	if e_section_stage = section_stage_running
	
	
		switch intro_mocap_status
		
			case 0
			
				if is_cutscene_playing()
				
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
					
					SET_SRL_POST_CUTSCENE_CAMERA(<<1981.1268, 3830.4668, 31.4241>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 296.00>>))
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1994.075, 3816.573, 31.096>>, <<1983.271, 3834.697, 35.396>>, 22.50, <<1994.9022, 3822.9993, 31.1571>>, 211.22, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
					set_players_last_vehicle_to_vehicle_gen(<<1994.9022, 3822.9993, 31.1571>>, 211.22)
					
					vehicle_index test_vehicle
					test_vehicle = GET_PLAYERS_LAST_VEHICLE()
					if is_vehicle_driveable(test_vehicle)
						printstring("get_entity_coords(test_vehicle) = ")
						printvector(get_entity_coords(test_vehicle))
						printnl()
					endif 
					
					DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(char_trevor)
					
					clear_area(<<1984.2650, 3818.7625, 31.3800>>, 10000.00, true)
					
					intro_mocap_status++
			
				endif 

			break 
			
			case 1
			
				request_model(michael_model)
				request_model(trevor_model)	
				request_model(trevors_truck.model)	
				REQUEST_VEHICLE_ASSET(trevors_truck.model, enum_to_int(VRF_REQUEST_ALL_ANIMS))
			
				if is_cutscene_active()
				
					if not WAS_CUTSCENE_SKIPPED()

						switch get_current_player_ped_enum() 
					
							case char_michael
							
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor"))
									selector_ped.pedID[selector_ped_trevor] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor"))
								ENDIF	
							
							break 
							
							case char_trevor
							
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michael"))
									selector_ped.pedID[selector_ped_michael] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michael"))
								ENDIF	
							
							break 
							
						endswitch
	
						if not does_entity_exist(trevors_truck.veh)
							if has_model_loaded(trevors_truck.model)
								CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, trevors_truck.pos, trevors_truck.heading)
							endif 
						endif 


						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor))

							if get_current_player_ped_enum() = char_michael
								make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
								take_control_of_selector_ped(selector_ped, true, true)
							endif 

							clear_ped_tasks_immediately(player_ped_id())
							set_entity_coords(player_ped_id(), <<1981.1268, 3830.4668, 31.4241>>)
							set_entity_heading(player_ped_id(), 218.8945)//)30.0) //299 + 90 = 30
							SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
							add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
							task_enter_vehicle(player_ped_id(), trevors_truck.veh, -1, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED | ECF_WARP_ENTRY_POINT)
							force_ped_ai_and_animation_update(player_ped_id(), true)

						endif 
								
						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))

							if get_current_player_ped_enum() = char_michael
								make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
								take_control_of_selector_ped(selector_ped, true, true)
							endif 
							
							if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_Michael])
								clear_ped_tasks_immediately(selector_ped.pedID[SELECTOR_PED_Michael])
								set_entity_coords(selector_ped.pedID[SELECTOR_PED_Michael], <<1982.65125, 3827.78638, 31.41647>>)//<<1982.4924, 3827.5999, 31.4489>>)
								set_entity_heading(selector_ped.pedID[SELECTOR_PED_Michael], 332.5456)
								SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
								add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")
								task_enter_vehicle(selector_ped.pedID[SELECTOR_PED_Michael], trevors_truck.veh, 30000, VS_front_right)
								force_ped_ai_and_animation_update(selector_ped.pedID[SELECTOR_PED_Michael])
							endif
									
						endif 
						
						if CAN_SET_EXIT_STATE_FOR_CAMERA()
						
							REPLAY_STOP_EVENT()
						
							set_gameplay_cam_relative_heading(95)
							set_gameplay_cam_relative_pitch(0)
						endif
						
					else 
				
						SET_CUTSCENE_FADE_VALUES(false, false, true)
					
						e_section_stage = SECTION_STAGE_cleanup
					
					endif 

				else 
				
					end_cutscene_no_fade(false, false, false, 0, 0, 0, false)
					//end_cutscene_no_fade(false, true, false, 90.00, 0)
					
					e_section_stage = SECTION_STAGE_SETUP
					mission_flow = stage_get_to_mountains
					
				endif 

			break 
			
		endswitch 
		
	ENDIF
		

	IF e_section_stage = SECTION_STAGE_CLEANUP
	
		request_model(michael_model)
		request_model(trevor_model)	
		request_model(trevors_truck.model)	
		REQUEST_VEHICLE_ASSET(trevors_truck.model, enum_to_int(VRF_REQUEST_ALL_ANIMS))

		if is_cutscene_active()

			switch get_current_player_ped_enum() 
		
				case char_michael
				
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor"))
						selector_ped.pedID[selector_ped_trevor] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor"))
					ENDIF	
				
				break 
				
				case char_trevor
				
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michael"))
						selector_ped.pedID[selector_ped_michael] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michael"))
					ENDIF	
				
				break 
				
			endswitch

		else 
		
			if has_model_loaded(michael_model)
			and has_model_loaded(trevor_model)
			and has_model_loaded(trevors_truck.model)	
			and has_vehicle_asset_loaded(trevors_truck.model)
				
				if not does_entity_exist(trevors_truck.veh)
					CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, trevors_truck.pos, trevors_truck.heading)
				endif

				if get_current_player_ped_enum() = char_michael 
					make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
					take_control_of_selector_ped(selector_ped, true, true)
				endif 

				clear_ped_tasks_immediately(player_ped_id())
				set_entity_coords(player_ped_id(), <<1981.1268, 3830.4668, 31.4241>>)
				set_entity_heading(player_ped_id(), 218.8945)//)30.0) //299 + 90 = 30
				SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
				add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
				task_enter_vehicle(player_ped_id(), trevors_truck.veh, -1, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED | ECF_WARP_ENTRY_POINT)
				force_ped_ai_and_animation_update(player_ped_id())
						
				if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_Michael])
					clear_ped_tasks_immediately(selector_ped.pedID[SELECTOR_PED_Michael])
					set_entity_coords(selector_ped.pedID[SELECTOR_PED_Michael], <<1982.65125, 3827.78638, 31.41647>>)//<<1982.4924, 3827.5999, 31.4489>>)
					set_entity_heading(selector_ped.pedID[SELECTOR_PED_Michael], 332.5456)
					SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
					add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")
					task_enter_vehicle(selector_ped.pedID[SELECTOR_PED_Michael], trevors_truck.veh, -1, VS_front_right)
				endif
				
				//SET_SRL_POST_CUTSCENE_CAMERA(<<1981.1268, 3830.4668, 31.4241>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 60.0>>))

				end_cutscene(false, 90, 0, false)

				e_section_stage = SECTION_STAGE_SETUP
				mission_flow = stage_get_to_mountains

			endif 

		endif

	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
	
		if is_cutscene_active()
			STOP_CUTSCENE()
		endif 

	ENDIF

ENDPROC


PROC CRASH_CUTSCENE()

	printstring("crash_cutscene")
	printnl()
	
	int i = 0
	
	str_carrec = "MattForest"
	
	IF e_section_stage = SECTION_STAGE_SETUP
	
		//stop tracking gang car INFORM_MISSION_STATS_car_damage  pass in null as vehicle index
	
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		SET_FRONTEND_RADIO_ACTIVE(false)
	
		disable_control_action(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
		request_model(animal[0].model)
		request_anim_dict("missexile2ig_5")
		request_anim_dict("missexile2deer_crash")
		REQUEST_VEHICLE_RECORDING(CARREC_CRASH, str_carrec)
		REQUEST_VEHICLE_RECORDING(CARREC_GANG, str_carrec)
		load_stream("CAR_CRASH_OFF_CLIFF_STREAM", "EXILE_2_SOUNDS")
		prepare_music_event("EXL2_DEER")
		REQUEST_PTFX_ASSET() 
		

		IF has_model_loaded(animal[0].model)
			printstring("test 0")
			printnl()
		endif 
		
		if has_anim_dict_loaded("missexile2ig_5")
			printstring("test 1")
			printnl()
		endif 
		
		if HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_CRASH, str_carrec)
			printstring("test 2")
			printnl()
		endif 
		
		if HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_GANG, str_carrec)
			printstring("test 3")
			printnl()
		endif 
		
		if load_stream("CAR_CRASH_OFF_CLIFF_STREAM", "EXILE_2_SOUNDS")
			printstring("test 4")
			printnl()
		endif 
		
		if has_ptfx_asset_loaded()
			printstring("test 5")
			printnl()
		endif 
		
		IF has_model_loaded(animal[0].model)
		and has_anim_dict_loaded("missexile2ig_5")
		and has_anim_dict_loaded("missexile2deer_crash")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_CRASH, str_carrec)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_GANG, str_carrec)
		and load_stream("CAR_CRASH_OFF_CLIFF_STREAM", "EXILE_2_SOUNDS")
		and has_ptfx_asset_loaded()
		
			if start_new_cutscene_no_fade()

				clear_area(get_entity_coords(player_ped_id()), 10000, true)
		
				switch_off_vehicle_generators_for_mission()
			
				SET_SELECTOR_PED_HINT(selector_ped, selector_ped_michael, false)
				SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, false)
				SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor, false)
			
				clear_player_wanted_level(player_id())
				SET_WANTED_LEVEL_MULTIPLIER(0.0)

				IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
				
					DISABLE_PROCOBJ_CREATION()
				
					SET_ROADS_IN_AREA(vCountryRoadMin, vCountryRoadMax, FALSE)
					
					add_scenario_blocking_area(<<-2607.6, 3715.3, 300.00>>, <<-541.9, 4969.0, -100.00>>) //whole forrest area
					
					CLEAR_AREA(get_entity_coords(player_ped_id()), 10000, true)
				
					if does_blip_exist(gang_car.blip)
						remove_blip(gang_car.blip)
					endif 
					
					
					if get_current_player_ped_enum() != char_franklin
						make_selector_ped_selection(selector_ped, SELECTOR_PED_franklin)
						take_control_of_selector_ped(selector_ped)
					endif
					
					clear_ped_tasks_immediately(player_ped_id())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), franklins_car.veh)
					
					freeze_entity_position(franklins_car.veh, false)
					START_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh, CARREC_CRASH, str_carrec)
					SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh, 48300.0)	//45500.00	
					force_playback_recorded_vehicle_update(franklins_car.veh)	
					setup_vehicle_proofs(franklins_car.veh)
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(gang_car.veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(gang_car.veh)
					endif 
					
					//SET_VEHICLE_ACTIVE_DURING_PLAYBACK(gang_car.veh, true) //makes the vehicle anim play whilst the vehicle recording is running
					START_PLAYBACK_RECORDED_VEHICLE(gang_car.veh, CARREC_GANG, str_carrec)
					
					SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(gang_car.veh, 48300.0)	
					force_playback_recorded_vehicle_update(gang_car.veh)
					set_entity_load_collision_flag(gang_car.veh, true)
					SET_VEHICLE_ACTIVE_DURING_PLAYBACK(gang_car.veh, true)
					setup_vehicle_proofs(gang_car.veh)
					SET_VEHICLE_CAN_DEFORM_WHEELS(gang_car.veh, false)
					
					add_ped_for_dialogue(scripted_speech, 6, gang_enemy[0].ped, "oneil") 
					
					//stops ped shooting during cutscene. 
					if not is_ped_injured(gang_enemy[2].ped)
						clear_ped_tasks(gang_enemy[2].ped)
					endif 
					
					
					cutscene_pos = <<0.0, 0.0, 0.0>> 
					cutscene_rot = <<0.0, 0.0, 0.0>> 
		
					cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)

					if is_vehicle_driveable(gang_car.veh)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(cutscene_index, gang_car.veh, 0)
					endif
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(gang_car.veh, cutscene_index, "getout_car_stumble_dubsta", "missexile2ig_5", instant_BLEND_IN, normal_BLEND_OUT, enum_to_int(SYNCED_SCENE_USE_PHYSICS) | enum_to_int(SYNCED_SCENE_BLOCK_MOVER_UPDATE))
					force_entity_ai_and_animation_update(gang_car.veh)	
					
					if not is_ped_injured(gang_enemy[0].ped)
						task_synchronized_scene(gang_enemy[0].ped, cutscene_index, "missexile2ig_5", "getout_car_stumble_bryce", instant_blend_in, normal_blend_out)	
						force_ped_ai_and_animation_update(gang_enemy[0].ped, true)
					endif 
					
					if not is_ped_injured(gang_enemy[1].ped)
						task_synchronized_scene(gang_enemy[1].ped, cutscene_index, "missexile2ig_5", "getout_car_stumble_jeff", instant_blend_in, normal_blend_out)	
						force_ped_ai_and_animation_update(gang_enemy[1].ped, true)	
					endif 
					
					if not is_ped_injured(gang_enemy[2].ped)
						task_synchronized_scene(gang_enemy[2].ped, cutscene_index, "missexile2ig_5", "getout_car_stumble_andre", instant_blend_in, normal_blend_out)	
						force_ped_ai_and_animation_update(gang_enemy[2].ped, true)	
					endif 
				
					setup_animal(animal[0])

					cutscene_pos = <<-1889.262, 4419.208, 47.745>> 
					cutscene_rot = <<0.000, 0.000, -6.120>> 

					animal[0].scene_id = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
					task_synchronized_scene(animal[0].ped, animal[0].scene_id, "missexile2deer_crash", "deer_crash", instant_blend_in, normal_blend_out)	
					force_ped_ai_and_animation_update(animal[0].ped)

					cam_interp_helper = create_cam_with_params("default_scripted_camera", <<-1873.221558,4420.158691,49.323524>>, <<-6.420567,-0.016574,65.465591>>, 24.283463)
									
					cam_cutscene = create_cam_with_params("default_scripted_camera", <<-1877.723999,4422.953125,47.547523>>, <<-6.420536,-0.016574,65.465347>>, 24.283463)
						
					set_cam_active(cam_interp_helper, true)
					SET_CAM_ACTIVE_WITH_INTERP(cam_cutscene, cam_interp_helper, 5000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
							
					shake_cam(cam_cutscene, "HAND_SHAKE", 1.0)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
										
					if IS_AUDIO_SCENE_ACTIVE("EXILE_2_FOLLOW_CAR_PHONE")
						STOP_AUDIO_SCENE("EXILE_2_FOLLOW_CAR_PHONE")
					endif 
					
					PLAY_STREAM_FRONTEND()
					
					START_AUDIO_SCENE("EXILE_2_CRASH_CUTSCENE")
				
					trigger_music_event("EXL2_DEER")

					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					SET_WIDESCREEN_BORDERS(TRUE, 0)
					
					bAvoidOneFrameGameCameraFlag = FALSE
					
					SETTIMERA(0)
					SETTIMERB(0)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					i_current_event = 0
					i_car_break_event = 0
					e_section_stage = SECTION_STAGE_RUNNING
					
				ENDIF
			endif 
					
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		
		IF bAvoidOneFrameGameCameraFlag = FALSE
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			bAvoidOneFrameGameCameraFlag = TRUE
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
			IF IS_VEHICLE_DRIVEABLE(franklins_car.veh)
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(gang_car.veh)
				
					f_playback_time = GET_TIME_POSITION_IN_RECORDING(gang_car.veh)
				
					printfloat(f_playback_time)
					printnl()
		
				endif 
				
				printint(i_current_event)
				printnl()
					
				SWITCH i_current_event
					
					CASE 0
						
						IF f_playback_time > 50750//50500 
							
							IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
								IF IS_VEHICLE_DRIVEABLE(franklins_car.veh)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(gang_car.veh)
										SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(gang_car.veh, 52428.0)
										force_playback_recorded_vehicle_update(gang_car.veh)
									ENDIF
								ENDIF
							ENDIF
						
							IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
								DESTROY_CAM(cam_interp_helper)
								cam_interp_helper = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)

								SET_CAM_COORD(cam_interp_helper, <<-1873.518677,4435.840820,46.726810>>)
								SET_CAM_ROT(cam_interp_helper, <<-1.481913,-0.602657,-42.641331>>)
								SET_CAM_FOV(cam_interp_helper, 30.126801)
								SHAKE_CAM(cam_interp_helper, "ROAD_VIBRATION_SHAKE", 1.0)

								DESTROY_CAM(cam_cutscene)
								cam_cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
								SET_CAM_COORD(cam_cutscene, <<-1872.068359,4439.052734,49.046635>>)
								SET_CAM_ROT(cam_cutscene, <<-24.354137,-0.577697,-29.000818>>)
								SET_CAM_FOV(cam_cutscene, 28.853399)
								SHAKE_CAM(cam_cutscene, "ROAD_VIBRATION_SHAKE", 1.0)
								
								SET_CAM_ACTIVE_WITH_INTERP(cam_cutscene, cam_interp_helper, 1600, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
								
								SETTIMERB(0)
								i_current_event++
							ENDIF
						ENDIF
					BREAK
				
					CASE 1

						IF f_playback_time > 53912.0
							IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
								IF IS_VEHICLE_DRIVEABLE(franklins_car.veh)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(gang_car.veh)
										SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(gang_car.veh, 53552.0)
										force_playback_recorded_vehicle_update(gang_car.veh)
									ENDIF
								ENDIF
							ENDIF
						
							IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
								
								DESTROY_CAM(cam_interp_helper)
								cam_interp_helper = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
								SET_CAM_COORD(cam_interp_helper, <<-1870.899780,4476.434082,27.505926>>)
								SET_CAM_ROT(cam_interp_helper, <<28.299524,-0.577695,-160.568649>>)
								SET_CAM_FOV(cam_interp_helper, 26.023769)
								
									
								DESTROY_CAM(cam_cutscene)
								cam_cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
								SET_CAM_COORD(cam_cutscene, <<-1872.597778,4481.578613,24.811926>>)
								SET_CAM_ROT(cam_cutscene, <<24.790777,-0.577697,-152.086853>>)
								SET_CAM_FOV(cam_cutscene, 26.023769)
								
								SET_CAM_ACTIVE_WITH_INTERP(cam_cutscene, cam_interp_helper, 5000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
								
								shake_cam(cam_cutscene, "HAND_SHAKE", 1.0) 
								
								SETTIMERB(0)
								i_current_event++
								
							ENDIF
						ENDIF
					BREAK
					
					case 2
					
						IF f_playback_time > 54213.304688
						
							shake_cam(cam_cutscene, "MEDIUM_EXPLOSION_SHAKE", 0.2) 
							
							i_current_event++
						
						endif 
					 
					break 
					
					case 3
					
						IF f_playback_time > 55498.500000//55625.320313

							DESTROY_CAM(cam_interp_helper)
							cam_interp_helper = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							SET_CAM_COORD(cam_interp_helper, <<-1875.610107,4472.933594,28.464901>>)
							SET_CAM_ROT(cam_interp_helper, <<-12.234248,-4.103173,-44.424568>>)
							SET_CAM_FOV(cam_interp_helper, 36.006031)
							
	
							DESTROY_CAM(cam_cutscene)
							cam_cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							SET_CAM_COORD(cam_cutscene, <<-1877.327759,4474.560547,27.996382>>)
							SET_CAM_ROT(cam_cutscene, <<-12.219757,-4.103173,-19.276518>>)
							SET_CAM_FOV(cam_cutscene, 36.006031)
							
							SET_CAM_ACTIVE_WITH_INTERP(cam_cutscene, cam_interp_helper, 1250, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
								
							shake_cam(cam_cutscene, "ROAD_VIBRATION_SHAKE", 2.0) 

							if not does_particle_fx_looped_exist(i_ptfx_fire)
								i_ptfx_fire = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ex2_jeep_engine_fire", gang_car.veh, <<0.0, 1.5, 0.5>>, <<0.0, 0.0, 0.0>>, 1.0)
							endif 

							i_current_event++

						endif 
					
					break 
					
					case 4
					
						if f_playback_time > 56751.867188
					
							DESTROY_CAM(cam_interp_helper)
							cam_interp_helper = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							SET_CAM_COORD(cam_interp_helper, <<-1876.130005,4489.383789,24.415810>>)
							SET_CAM_ROT(cam_interp_helper, <<15.511820,-4.103173,-166.403625>>)
							SET_CAM_FOV(cam_interp_helper, 35.730843)
							shake_cam(cam_interp_helper, "ROAD_VIBRATION_SHAKE", 1.0)

							DESTROY_CAM(cam_cutscene)
							cam_cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							SET_CAM_COORD(cam_cutscene, <<-1876.130005,4489.383789,24.415810>>)
							SET_CAM_ROT(cam_cutscene, <<10.798196,-4.103173,-158.778931>>)
							SET_CAM_FOV(cam_cutscene, 35.730843)
							shake_cam(cam_cutscene, "ROAD_VIBRATION_SHAKE", 1.0)
							
							SET_CAM_ACTIVE_WITH_INTERP(cam_cutscene, cam_interp_helper, 2000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
								
							shake_cam(cam_cutscene, "ROAD_VIBRATION_SHAKE", 1.0) 

							set_synchronized_scene_phase(cutscene_index, 0.45)//0.4
							
							i_current_event++
						
						endif 

					break 
					
					case 5
					
						if f_playback_time > 57606.820313
						
							shake_cam(cam_cutscene, "small_EXPLOSION_SHAKE", 0.15)
							
							//--
//							if is_playback_going_on_for_vehicle(gang_car.veh)
//								stop_playback_recorded_vehicle(gang_car.veh)
//							endif 
//							
//							PLAY_SYNCHRONIZED_ENTITY_ANIM(gang_car.veh, cutscene_index, "getout_car_stumble_dubsta", "missexile2ig_5", instant_BLEND_IN, normal_BLEND_OUT)
//							force_entity_ai_and_animation_update(gang_car.veh)
							
							//--
							
							i_current_event++
						
						endif 
					
					break 
					
					CASE 6

						if f_playback_time > 59000 //59169
						
							original_time = get_game_timer()
							
//							DESTROY_CAM(cam_interp_helper)
//							cam_interp_helper = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//							SET_CAM_COORD(cam_interp_helper, <<-1878.146118,4481.939941,26.803799>>)
//							SET_CAM_ROT(cam_interp_helper, <<-11.980042,-0.874818,-56.969074>>)
//							SET_CAM_FOV(cam_interp_helper, 35.730843)
//							
//							DESTROY_CAM(cam_cutscene)
//							cam_cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//							SET_CAM_COORD(cam_cutscene, <<-1877.305054,4482.329102,26.511227>>)
//							SET_CAM_ROT(cam_cutscene, <<-12.262194,-0.874819,-60.320076>>)
//							SET_CAM_FOV(cam_cutscene, 34.922203)
//								
//							SET_CAM_ACTIVE_WITH_INTERP(cam_cutscene, cam_interp_helper, 6000, GRAPH_TYPE_linear, GRAPH_TYPE_linear)
							
							DESTROY_CAM(cam_interp_helper)
							DESTROY_CAM(cam_cutscene)
							
							camera_anim = CREATE_CAMERA(camtype_animated, true)
							play_cam_anim(camera_anim, "getout_car_stumble_cam", "missexile2ig_5", get_entity_coords(gang_car.veh), get_entity_rotation(gang_car.veh))
							SET_CAM_ANIM_CURRENT_PHASE(camera_anim, 0.596)
							//PLAY_SYNCHRONIZED_CAM_ANIM(camera_anim, cutscene_index, "getout_car_stumble_cam", "missexile2ig_5")
							

							if is_playback_going_on_for_vehicle(gang_car.veh)
								stop_playback_recorded_vehicle(gang_car.veh)
							endif 
							
//							PLAY_SYNCHRONIZED_ENTITY_ANIM(gang_car.veh, cutscene_index, "getout_car_stumble_dubsta", "missexile2ig_5", instant_BLEND_IN, normal_BLEND_OUT)
//							force_entity_ai_and_animation_update(gang_car.veh)	
							
							//set_synchronized_scene_phase(cutscene_index, 0.52) //0.6
							//set_synchronized_scene_phase(cutscene_index, 0.596)

							i_current_event++
							
						endif 
						
					break 
					
					case 7
					
						if get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person	 
							if is_synchronized_scene_running(cutscene_index)
								if get_synchronized_scene_phase(cutscene_index) >= 0.96
					
									ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE) 
									PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								
									i_current_event++
								
								endif 
								
							endif 
						
						else 
						
							i_current_event++
						
						endif 

					break 
					
					case 8
					
						//if not is_cam_interpolating(cam_cutscene)
						//if GET_CAM_ANIM_CURRENT_PHASE(camera_anim) >= 1.0
						if is_synchronized_scene_running(cutscene_index)
							if get_synchronized_scene_phase(cutscene_index) >= 0.99

								task_leave_vehicle(player_ped_id(), franklins_car.veh)
							
								detach_entity(chop.ped)
								
								chop.pos = <<-1895.80762, 4429.06787, 43.38081>>//<<-1896.4316, 4427.4692, 43.3257>>
								chop.heading = 9.9039
								set_entity_coords(chop.ped, chop.pos)
								set_entity_heading(chop.ped, chop.heading)
								chop.blip = create_blip_for_entity(chop.ped)
								chop_ai_system()

								if not is_ped_injured(animal[0].ped)
									
									clear_ped_tasks_immediately(animal[0].ped)
									set_entity_coords(animal[0].ped, <<-1868.9681, 4427.4082, 46.6996>>) 
									set_entity_heading(animal[0].ped, 267.4871)
					
									open_sequence_task(seq)
										task_go_straight_to_coord(null, <<-1820.6013, 4417.2197, 45.8910>>, 2.0, -1)
									close_sequence_task(seq)
									task_perform_sequence(animal[0].ped, seq)
									clear_sequence_task(seq)
									
									fORCE_PED_MOTION_STATE(animal[0].ped, MS_ON_FOOT_RUN, TRUE)

									force_ped_ai_and_animation_update(animal[0].ped)
								endif 
								
								e_section_stage = SECTION_STAGE_CLEANUP
								
							ENDIF 
							
						ENDIF
					BREAK
				ENDSWITCH
				
				switch crash_cutscene_dialogue_system
				
					case 0
					
						if f_playback_time > 48500
							if create_conversation(scripted_speech, "TM44AUD", "TM44_ELK", CONV_PRIORITY_medium)   
								crash_cutscene_dialogue_system++
							endif 
						endif 
					
					break 
					
					case 1
					
						IF f_playback_time > 50500 
							if create_conversation(scripted_speech, "TM44AUD", "TM44_CRASH", CONV_PRIORITY_medium)   
								crash_cutscene_dialogue_system++
							endif 
						endif 
					
					break 
					
					case 2
					
						if is_synchronized_scene_running(cutscene_index)
							if get_synchronized_scene_phase(cutscene_index) >= 0.7
							
								if not is_ped_injured(gang_enemy[0].ped)
									PLAY_PAIN(gang_enemy[0].ped, AUD_DAMAGE_REASON_COUGH)
								endif 
								
								crash_cutscene_dialogue_system++
							
							endif 
							
						endif 
					
					break 
					
					case 3
					
						if is_synchronized_scene_running(cutscene_index)
							if get_synchronized_scene_phase(cutscene_index) >= 0.85
							
								if not is_ped_injured(gang_enemy[1].ped)
									PLAY_PED_AMBIENT_SPEECH_with_voice(gang_enemy[1].ped, "GENERIC_SHOCKED_HIGH",  "A_M_M_HillBilly_01_WHITE_MINI_02")
								endif 
								
								crash_cutscene_dialogue_system++
							
							endif 
						endif 
					
					break 
					
				endswitch 

				
				SWITCH i_car_break_event
					
					CASE 0
						IF f_playback_time >= 51180.0
							i_car_break_event++
						ENDIF
					BREAK

					CASE 1
						IF f_playback_time >= 54187 //54295
							IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
								SET_VEHICLE_DAMAGE(gang_car.veh, <<-0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
								SET_VEHICLE_DAMAGE(gang_car.veh, <<0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
							ENDIF
							
							i_car_break_event++
						ENDIF
					BREAK
					
					case 2
					
						IF f_playback_time >= 54591
					
							SET_VEHICLE_DOOR_BROKEN(gang_car.veh, SC_DOOR_BONNET, FALSE)
							
							i_car_break_event++
							
						endif 
					
					break 
					
					case 3
					
						if f_playback_time >= 55258.000000
					
							SET_VEHICLE_DAMAGE(gang_car.veh, <<0.3, -0.1, 0.75>>, 200.00, 200.00, TRUE)
						
							i_car_break_event++
							
						endif 
					
					break 
					
					case 4
					
					break 
					
				ENDSWITCH
				
				
				
				switch gang_car_ptfx_cutscene_system_status 
				
//						1. scr_ex2_car_impact - This will be a triggered effect, with a thick dirt kick up. <<0.0, 2.2, 0.4>>
//						2. scr_ex2_car_bang - Another triggered effect to kick up dust. <<-0.5, -1.6, -0.5>>
//						3. scr_ex2_car_slid <<0.8, -0.8, 1.35>>
				
					case 0
					
						if f_playback_time >= 54204.00
						
							start_particle_fx_non_looped_on_entity("scr_ex2_car_impact", gang_car.veh, <<0.0, 2.2, 0.4>>, <<0.0, 0.0, 0.0>>)
							//start_particle_fx_looped_on_entity("scr_ex2_car_impact", gang_car.veh, <<0.0, 2.2, 0.4>>, <<0.0, 0.0, 0.0>>)

							gang_car_ptfx_cutscene_system_status++
							
						endif 
					
					break 
					
					case 1

						if f_playback_time > 55321.000000//55498.500000
						
							gang_car_ptfx[0] = start_particle_fx_looped_on_entity("scr_ex2_car_slide", gang_car.veh, <<0.8, -0.8, 1.35>>, <<0.0, 0.0, 0.0>>)
							
							gang_car_ptfx_cutscene_system_status++
						
						endif 
					
					break 
					
					case 2
					
						if f_playback_time > 56751.867188
						
							if does_particle_fx_looped_exist(gang_car_ptfx[0])
								stop_particle_fx_looped(gang_car_ptfx[0])
							endif 
							
							//potentialy start another particle fx here
							
							gang_car_ptfx_cutscene_system_status++
						endif 

					break 
					
					case 3
					
					break 
					
					case 4
					
					break 
					
				endswitch 

				
			else 
			
				printstring("franklins car not driveable")
				printnl()
			ENDIF
			
		else 
		
			printstring("gang car not driveable")
			printnl()
		ENDIF
		
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			IF i_current_event > 0
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ENDIF
	ENDIF
	

	IF e_section_stage = SECTION_STAGE_CLEANUP

		STOP_STREAM()
		
		REPLAY_STOP_EVENT()
		
		ENABLE_PROCOBJ_CREATION()
		
		if IS_AUDIO_SCENE_ACTIVE("EXILE_2_CRASH_CUTSCENE")
			STOP_AUDIO_SCENE("EXILE_2_CRASH_CUTSCENE")
		endif 
		
		for i = 0 to count_of(gang_car_ptfx) - 1
			if does_particle_fx_looped_exist(gang_car_ptfx[i])
				stop_particle_fx_looped(gang_car_ptfx[i])
			endif 
		endfor 
		
		remove_anim_dict("missexile2ig_5")
		remove_anim_dict("missexile2deer_crash")
	
		set_time_scale(1.0)
	
		if is_vehicle_driveable(gang_car.veh)
			STOP_SYNCHRONIZED_ENTITY_ANIM(gang_car.veh, instant_blend_in, true)
			set_entity_health(gang_car.veh, 1000)
			set_vehicle_petrol_tank_health(gang_car.veh, 1000)
			set_vehicle_engine_health(gang_car.veh, 1000)
		endif 
	
		deactivate_vehicle_proofs(franklins_car.veh)
		deactivate_vehicle_proofs(trevors_truck.veh)
	
		IF IS_VEHICLE_DRIVEABLE(franklins_car.veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(franklins_car.veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh)
			ENDIF		
		ENDIF
		
		set_entity_coords(franklins_car.veh, <<-1899.3949, 4431.2559, 42.8050>>)
		set_entity_heading(franklins_car.veh, 228.1822)
		set_vehicle_engine_on(franklins_car.veh, false, true)
		set_vehicle_on_ground_properly(franklins_car.veh)
		
		IF IS_VEHICLE_DRIVEABLE(gang_car.veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(gang_car.veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(gang_car.veh)
			ENDIF
		ENDIF
		
		//fix for vehicle recording sometimes ending up in the air.
		set_entity_coords(gang_car.veh, <<-1873.92, 4486.02, 24.625>>)
		set_entity_heading(gang_car.veh, 239.264404)
		
		REMOVE_VEHICLE_RECORDING(CARREC_CRASH, str_carrec)
		REMOVE_VEHICLE_RECORDING(CARREC_GANG, str_carrec)
		REMOVE_VEHICLE_RECORDING(CARREC_GANG_NO_CRASH, str_carrec)
		
		SET_FRONTEND_RADIO_ACTIVE(true)
		
		REPLAY_STOP_EVENT()
	
		if is_screen_faded_in()
			end_cutscene_no_fade(false, true, false, 0, 0, 3000, false)
		else 
			end_cutscene(false, 0, 0, false)
		endif 
	
		e_section_stage = SECTION_STAGE_SETUP
		mission_flow = STAGE_INVESTIGATE_CRASH
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
	
//		if not is_screen_faded_out()
//			if not is_screen_fading_out()
//				do_screen_fade_out(default_fade_time)
//			endif 
//			
//		else 

		DO_SAFE_FADE_OUT()
		
		mission_fail_checks() //dev check all entities


			if is_vehicle_driveable(gang_car.veh)
			
				set_entity_health(gang_car.veh, 1000)
				set_vehicle_petrol_tank_health(gang_car.veh, 1000)
				set_vehicle_engine_health(gang_car.veh, 1000)
					
				if is_playback_going_on_for_vehicle(gang_car.veh)
					skip_to_end_and_stop_playback_recorded_vehicle(gang_car.veh)
				endif 
			
				if i_car_break_event < 2
					SET_VEHICLE_DAMAGE(gang_car.veh, <<-0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
					SET_VEHICLE_DAMAGE(gang_car.veh, <<0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
				endif
				
				if i_car_break_event < 3
					SET_VEHICLE_DOOR_BROKEN(gang_car.veh, SC_DOOR_BONNET, true)
				endif 
				
				if i_car_break_event < 4
					SET_VEHICLE_DAMAGE(gang_car.veh, <<0.3, -0.1, 0.75>>, 200.00, 200.00, TRUE)
				endif 
				
			endif 
			
			if not is_ped_injured(animal[0].ped)
									
				clear_ped_tasks_immediately(animal[0].ped)
				set_entity_coords(animal[0].ped, <<-1868.9681, 4427.4082, 46.6996>>) 
				set_entity_heading(animal[0].ped, 267.4871)

				open_sequence_task(seq)
					task_go_straight_to_coord(null, <<-1820.6013, 4417.2197, 45.8910>>, 2.0, -1)
				close_sequence_task(seq)
				task_perform_sequence(animal[0].ped, seq)
				clear_sequence_task(seq)
				
				fORCE_PED_MOTION_STATE(animal[0].ped, MS_ON_FOOT_RUN, TRUE)

				force_ped_ai_and_animation_update(animal[0].ped)
			endif 
			
			
			if is_vehicle_driveable(franklins_car.veh)
			
				if is_playback_going_on_for_vehicle(franklins_car.veh)
					stop_playback_recorded_vehicle(franklins_car.veh)
				endif 
				
				set_entity_coords(franklins_car.veh, <<-1899.3949, 4431.2559, 42.8050>>)
				set_entity_heading(franklins_car.veh, 228.1822)
				set_vehicle_engine_on(franklins_car.veh, false, true)
				set_vehicle_on_ground_properly(franklins_car.veh)
					
					//freeze_entity_position(franklins_car.veh, true)
					//freeze_entity_position(franklins_car.veh, false)
					
	//			if is_ped_sitting_in_vehicle(player_ped_id(), franklins_car.veh)
	//				
	//				open_sequence_task(seq)
	//					task_leave_vehicle(null, franklins_car.veh)
	//				close_sequence_task(seq)
	//				task_perform_sequence(player_ped_id(), seq)
	//				clear_sequence_task(seq)
	//
	//			endif 
			endif
			
//			clear_ped_tasks_immediately(player_ped_id())
//			set_entity_coords(player_ped_id(), <<-1898.5886, 4433.0776, 42.8662>>)
//			set_entity_heading(player_ped_id(), 237.5338)
			task_leave_vehicle(player_ped_id(), franklins_car.veh)
			//force_ped_ai_and_animation_update(player_ped_id())

			detach_entity(chop.ped)
			chop.pos = <<-1896.4316, 4427.4692, 43.3257>>
			chop.heading = 9.9039
			set_entity_coords(chop.ped, chop.pos)
			set_entity_heading(chop.ped, chop.heading)
			chop.blip = create_blip_for_entity(chop.ped)
			chop_ai_system()

			deactivate_vehicle_proofs(gang_car.veh)
			deactivate_vehicle_proofs(franklins_car.veh) 
			
			e_section_stage = SECTION_STAGE_CLEANUP
			
//		endif 
	ENDIF
ENDPROC

/// PURPOSE: Sets the player on fire if they go near the fire ptfx on the gang car
//proc gang_car_fire_system()
//
//	if not is_entity_dead(gang_car.veh)
//		if is_entity_in_angled_area(player_ped_id(), get_offset_from_entity_in_world_coords(gang_car.veh, <<-1.000, 1.550, -1.000>>), get_offset_from_entity_in_world_coords(gang_car.veh, <<0.800, 1.6, 2.3>>), 1.5)   
//			
//			if not is_entity_on_fire(player_ped_id())
//				start_entity_fire(player_ped_id())
//			endif 
//		endif 
//	endif 
//	
//endproc 

//PURPOSE: Stops the fire ptfx setting farnklins baller  on fire because of the large collision box the vehicle has.
proc stop_ptfx_fire_harming_franklins_car()
		
	if is_vehicle_driveable(gang_car.veh)
		if is_vehicle_driveable(franklins_car.veh)
			if is_entity_at_coord(franklins_car.veh, get_entity_coords(gang_car.veh), <<5.0, 5.0, 2.5>>, false)
				set_entity_proofs(franklins_car.veh, false, true, false, false, false)
			else 	
				set_entity_proofs(franklins_car.veh, false, false, false, false, false)
			endif
		endif 
	endif 

endproc 

proc request_get_helicopter_assets()
		
	request_model(michael_model)
	request_model(trevor_model)	
	request_model(trevors_truck.model)	
	request_model(helicopter.model)
	request_anim_dict(strAnimDictCamShake)
	request_vehicle_asset(helicopter.model)
	request_vehicle_recording(002, "lkexile2")
	prepare_music_event("EXL2_SWITCH_START")

endproc 

PROC INVESTIGATE_CRASH()
	IF e_section_stage = SECTION_STAGE_SETUP
		
		SET_ROADS_IN_AREA(vCountryRoadMin, vCountryRoadMax, FALSE)
		
		set_ped_can_ragdoll(chop.ped, true)
		set_ragdoll_blocking_flags(chop.ped, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT) 
	
		IF IS_VEHICLE_DRIVEABLE(gang_car.veh)

			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(gang_car.veh, false)
			SET_VEHICLE_LOD_MULTIPLIER(gang_car.veh, 1.1)
			SET_VEHICLE_CAN_DEFORM_WHEELS(gang_car.veh, true)
			set_entity_proofs(gang_car.veh, true, true, true, true, true)
			//FREEZE_ENTITY_POSITION(gang_car.veh, TRUE)
			set_entity_health(gang_car.veh, 1000)
			set_vehicle_petrol_tank_health(gang_car.veh, 1000)
			set_vehicle_engine_health(gang_car.veh, 1000)
			SET_VEHICLE_RADIO_ENABLED(gang_car.veh, false)
			if not does_particle_fx_looped_exist(i_ptfx_fire)
				i_ptfx_fire = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ex2_jeep_engine_fire", gang_car.veh, <<0.0, 1.5, 0.5>>, <<0.0, 0.0, 0.0>>, 1.0)
			endif 
		
			
			PLAY_SOUND_FROM_ENTITY(gang_car_sound_id, "EXILE_2_GANG_CAR_FIRE", gang_car.veh) 

			SET_VEHICLE_DOOR_BROKEN(gang_car.veh, SC_DOOR_FRONT_RIGHT, TRUE)
			SET_VEHICLE_DOOR_BROKEN(gang_car.veh, SC_DOOR_BONNET, TRUE)
			SET_VEHICLE_DOOR_OPEN(gang_car.veh, SC_DOOR_FRONT_LEFT)
			SET_VEHICLE_DOOR_OPEN(gang_car.veh, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(gang_car.veh, SC_DOOR_REAR_RIGHT)
			
			set_vehicle_doors_locked(gang_car.veh, vehiclelock_locked)
			
			v_crash_pos = get_entity_coords(gang_car.veh)
			
//			request_model(michael_model)
//			request_model(trevor_model)	
//			request_model(trevors_truck.model)	
//			request_model(helicopter.model)
//			request_vehicle_recording(002, "lkexile2")
			
			request_get_helicopter_assets()
			
		ENDIF
	
		INT i = 0
		for i = 0 to count_of(gang_enemy) - 1
			IF does_entity_exist(gang_enemy[i].ped)
				delete_ped(gang_enemy[i].ped)
			ENDIF
			set_model_as_no_longer_needed(gang_enemy[i].model)
		endfor

		
	//	CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 2000)
	/*	
		IF NOT DOES_ENTITY_EXIST(SetPieceCarID[0])
			SetPieceCarID[0] = CREATE_VEHICLE(SetPieceCarModel[0],SetPieceCarPos[0])
			SET_ENTITY_QUATERNION(SetPieceCarID[0],SetPieceCarQuatX[0], SetPieceCarQuatY[0], SetPieceCarQuatZ[0], SetPieceCarQuatW[0])
			SET_ENTITY_LOAD_COLLISION_FLAG(SetPieceCarID[0], TRUE)
		ELSE
			SCRIPT_ASSERT("Exists!")
			BLIP_INDEX blipTemp
			blipTemp = ADD_BLIP_FOR_ENTITY(SetPieceCarID[0])
		ENDIF
		*/

		CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_GOCHOP1", CONV_PRIORITY_MEDIUM)
		
		REPLAY_RECORD_BACK_FOR_TIME(3.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
		
		START_AUDIO_SCENE("EXILE_2_GO_TO_CRASH_SITE")
		
		i_current_event = 0
		e_section_stage = SECTION_STAGE_RUNNING
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
	
		#if is_debug_build
			dont_do_j_skip(locates_data)
		#endif 
		
	
//		printstring("investigate_crash_status = ")
//		printint(investigate_crash_status)
//		printnl()
//		
//		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			printstring("conversation ongoing")
//			printnl()
//		else 
//			printstring("conversation NOT ongoing")
//			printnl()
//		endif 
//		
//		if IS_FACE_TO_FACE_CONVERSATION_PAUSED()
//			printstring("IS_FACE_TO_FACE_CONVERSATION_PAUSED")
//			printnl()
//		else 
//			printstring("IS_FACE_TO_FACE_CONVERSATION_PAUSED NOT PAUSED")
//			printnl()
//		endif 
//		
//		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
//			printstring("TEST 0")
//			printnl()
//		ELSE 
//			printstring("TEST 1")
//			printnl()
//		ENDIF
		
		IS_PLAYER_AT_LOCATION_any_means(locates_data, v_crash_pos, <<10.0, 10.0, 3.0>>, false, "FS_CRASHSITE", true)
	
	
		switch investigate_crash_status 
		
			case 0

				if is_entity_at_coord(player_ped_id(), v_crash_pos, <<45.0, 45.0, 25.0>>)
					IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_SPOT", CONV_PRIORITY_MEDIUM) //TM44_SPOT 
						investigate_crash_status++
					endif 
				endif 
				
			break 
			
			case 1
			
				if not is_any_text_being_displayed(locates_data)
				
//					set_current_ped_weapon(player_ped_id(), weapontype_unarmed, false)
//					set_ped_can_switch_weapon(player_ped_id(), false)
				
					original_time = get_game_timer()
					
					investigate_crash_status++
					
				endif 
				
			break 
			
			case 2
				
				if lk_timer(original_time, 1000)
					if PLAYER_CALL_CHAR_CELLPHONE(scripted_speech, char_trevor, "TM44AUD", "TM44_FUPDC", CONV_PRIORITY_high) //TM44_CALL
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						
						current_line = -1
						investigate_crash_status++
					endif 
				endif 

			break 
			
			case 3
				

//				if not HAS_CELLPHONE_CALL_FINISHED() 
//				or IS_MOBILE_PHONE_CALL_ONGOING()
				if IS_MOBILE_PHONE_CALL_ONGOING()
					if get_CURRENT_SCRIPTED_CONVERSATION_LINE() <> -1
						current_line = get_CURRENT_SCRIPTED_CONVERSATION_LINE()
						
						GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL() 
					endif 
				else
				
					if current_line <> -1
						if (NOT WAS_LAST_CELLPHONE_CALL_INTERRUPTED()) OR (current_line >= 0)
							
							e_section_stage = SECTION_STAGE_CLEANUP

						else 
							investigate_crash_status = 2
						endif 
						
					else 

						investigate_crash_status = 2
						
						phone_hang_up_count++ //if the player constantly hang up the phone the mission will progress
						
						if phone_hang_up_count > 2
							e_section_stage = SECTION_STAGE_CLEANUP
						endif 

					endif 
				
				endif 
			
			break 

		endswitch 
		
		chop_ai_system()
		
		stop_ptfx_fire_harming_franklins_car()

		//gang_car_fire_system()

	endif 

	IF e_section_stage = SECTION_STAGE_CLEANUP
	
		//reset the lod mulitplier to original.
		if is_vehicle_driveable(gang_car.veh)
			SET_VEHICLE_LOD_MULTIPLIER(gang_car.veh, 1.0)
		endif 
	
		set_ped_can_switch_weapon(player_ped_id(), true)
	
		clear_mission_locate_stuff(locates_data)
	
		CLEAR_PRINTS()
		REMOVE_ALL_BLIPS()
		
		e_section_stage = SECTION_STAGE_SETUP
		mission_flow = stage_get_helicopter
	
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		clear_ped_tasks_immediately(player_ped_id())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1876.1885, 4482.8340, 24.5287>>)
		set_entity_heading(player_ped_id(), 300.6762) 
		e_section_stage = SECTION_STAGE_CLEANUP
	ENDIF
ENDPROC

func int get_closest_vehicle_recording_node(int recording_number, string recording_name, int total_number_of_nodes)
	
	float smallest_distance_between_vectors
	float current_distance_between_vectors
	float node_multiplier_time
	int current_node = 0
	int i = 0
	
	node_multiplier_time = (get_total_duration_of_vehicle_recording(recording_number, recording_name) / total_number_of_nodes)

	for i = 0 to total_number_of_nodes //can use -1 e.g. 0 * 10 = 0, 10 * 10 = 100 11 nodes required

		current_distance_between_vectors = get_distance_between_coords(GET_ENTITY_COORDS(player_ped_id()), get_position_of_vehicle_recording_at_time(recording_number, (i * node_multiplier_time), recording_name)) 

		if i = 0
		
			smallest_distance_between_vectors = current_distance_between_vectors
			current_node = 0
		
		else 
			
			if current_distance_between_vectors < smallest_distance_between_vectors
				smallest_distance_between_vectors = current_distance_between_vectors
				current_node = i
			endif 
		
		endif 
		
	endfor 
	
	return current_node
	
endfunc 

func float calculate_trevor_vehicle_recording_progression(vehicle_index mission_veh, int car_rec_file_number, string car_rec_file_name, int total_number_of_recording_nodes = 10)

	float recording_multiplier_time
	int closest_node_to_vehicle = 0
	vector vec_BA
	vector node_forward_vector
	
	recording_multiplier_time = (get_total_duration_of_vehicle_recording(car_rec_file_number, car_rec_file_name) / total_number_of_recording_nodes)
	
	closest_node_to_vehicle = get_closest_vehicle_recording_node(car_rec_file_number, car_rec_file_name, total_number_of_recording_nodes)
	
	//find out if the player is infront of the closest node.
	vec_BA = get_position_of_vehicle_recording_at_time(car_rec_file_number, (closest_node_to_vehicle * recording_multiplier_time), car_rec_file_name) - get_entity_coords(player_ped_id()) 
	
	//swap out when you get:
	//get_rot_of_vehicle_recording_at_time(recording_number, (i * node_multiplier_time), recording_name)
	//GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(
	
	start_playback_recorded_vehicle(mission_veh, car_rec_file_number, car_rec_file_name)
	skip_time_in_playback_recorded_vehicle(mission_veh, (closest_node_to_vehicle * recording_multiplier_time))
	force_playback_recorded_vehicle_update(mission_veh)
	//use dummy car no collision if fucks up
	
	node_forward_vector = get_offset_from_entity_in_world_coords(mission_veh, <<0.0, 2.0, 0.0>>) - get_entity_coords(player_ped_id())
	
//	printint(closest_node_to_vehicle)
//	printnl()
//	printint(closest_node_to_vehicle)
//	printnl()
//	printint(closest_node_to_vehicle)
//	printnl()
	
	if dot_product(vec_BA, node_forward_vector) < 0.0
		
		//player infront of node
		if closest_node_to_vehicle < 10
			closest_node_to_vehicle++
		endif 
	
	else 
	
		//player behind of node
		if closest_node_to_vehicle = 0
			closest_node_to_vehicle = 1 //make player drive to node 1 if behind node 0.
		endif 
		
		
	endif 
	
	return(recording_multiplier_time * closest_node_to_vehicle)

endfunc 

proc get_to_the_helicopter_dialogue_system()

//	printstring("get_to_the_helicopter_dialogue_system_status: ")
//	printint(get_to_the_helicopter_dialogue_system_status)
//	printnl()
		
	switch get_to_the_helicopter_dialogue_system_status 

		case 0
		
			if create_conversation(scripted_speech, "TM44AUD", "TM44_TSWITCH", conv_priority_medium)
				
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				get_to_the_helicopter_dialogue_system_status++
			endif 

		break 
		
		case 1
		
			if not is_any_text_being_displayed(locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				if create_conversation(scripted_speech, "TM44AUD", "TM44_BANT1", conv_priority_low)
					get_to_the_helicopter_dialogue_system_status++
				endif 
			endif 
		
		break 
		
		case 2
		
			if get_helicopter_status > 0
				if does_blip_exist(locates_data.vehicleBlip)
					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(helicopter.veh)) < 45
						if create_conversation(scripted_speech, "TM44AUD", "TM44_CHOPPER", conv_priority_medium)
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							//if not is_ped_sitting_in_any_vehicle(player_ped_id())
							get_to_the_helicopter_dialogue_system_status++
						endif 
					endif 
				endif 
			
			else 
				
				//monitor conversation when driving to locate near helicopter
				in_car_dialogue_monitoring_system(locates_data.LocationBlip)
			
			endif
	
		break 
		
		case 3
		
			if does_blip_exist(locates_data.LocationBlip)
				if not is_any_text_being_displayed(locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					if create_conversation(scripted_speech, "TM44AUD", "TM44_BANT2", conv_priority_low)
						get_to_the_helicopter_dialogue_system_status++
					endif 
				endif 
			endif 
		
		break 
		
		case 4
		
			if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
				get_to_the_helicopter_dialogue_system_status++
			endif 
		
//			if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
//				if is_entity_at_coord(player_ped_id(), <<-1503.4, 4358.3, 155.7>>, <<400.0, 400.0, 200.0>>)
//				or get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_franklin])) < 125  
//					if create_conversation(scripted_speech, "TM44AUD", "TM44_FIND", conv_priority_medium)
//						get_to_the_helicopter_dialogue_system_status++
//					endif 
//				endif
//			endif 

		break 
		
		case 5
		
		break 
		
	endswitch

endproc 

proc switch_system_2()

	switch switch_system_2_status 
	
		case 0

			if allow_switch
				if update_selector_hud(selector_ped)
				
					switch get_current_player_ped_enum()
					
						case char_trevor
						
							if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_FRaNKLIN)
							
								kill_any_conversation()
								
								selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]

								clear_prints()
								clear_help()
								clear_mission_locate_stuff(locates_data)
								
								set_entity_proofs(helicopter.veh, true, true, true, true, true)
								
								//trevors_current_car = null //might not need this null
								//get_current_player_vehicle(trevors_current_car)
								trevors_current_car = trevors_truck.veh  

								switch_system_2_status++
							endif 
							
						break 
						
						case char_franklin
						
							if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_trevor)
							
								if does_blip_exist(chop.blip)
									remove_blip(chop.blip)
								endif 
								
								selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_TREVOR]
								
								clear_prints()
								
								trevors_current_car = trevors_truck.veh  
	
								switch_system_2_status++
							
							endif 
						
						break 
						
					endswitch 
					
				endif 
			endif 
			
		break 
		
		case 1
		
			//IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(selector_cam, 1000, 1000, selector_cam_default, 0)	// Returns FALSE when the camera spline is complete
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam)
			IF NOT HANDLE_SWITCH_CAM_FRANKLIN_TO_TRUCK(scsSwitchCamFranklinToTruckOutro)

				
			else 
			
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(), EXL2_DAMAGE)

				switch get_current_player_ped_enum()
					
					case char_trevor
				
						SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)
						
						set_entity_proofs(helicopter.veh, false, false, false, false, false)
						
						//FREEZE_ENTITY_POSITION(franklins_car.veh, TRUE)
						
						if does_entity_exist(trevors_truck.veh)
							if is_vehicle_driveable(trevors_truck.veh)
								if is_playback_going_on_for_vehicle(trevors_truck.veh)
									stop_playback_recorded_vehicle(trevors_truck.veh)
								endif 
							endif 
						endif 
						
						allow_switch = false
						
						switch_system_2_status = 0
						
						//switch_system_2_status = 2
						
						START_AUDIO_SCENE("EXILE_2_GO_TO_HELI")
						
						trigger_music_event("EXL2_TRUCK")
						
						NEW_LOAD_SCENE_STOP()

						get_to_the_helicopter_dialogue_system()

					break 
					
					case char_franklin
					
						SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor, true)
						
						if is_ped_sitting_in_vehicle(player_ped_id(), helicopter.veh)
							FREEZE_ENTITY_POSITION(helicopter.veh, TRUE)
						endif 

						switch_system_2_status = 0
					
					break 
					
				endswitch 

			endif 
		
		break 
		
	endswitch 			
		
endproc

proc michael_looking_at_player_entering_vehicle_system(vehicle_index miss_vehicle)
			
	switch trevor_looking_at_player_entering_vehicle_status 

		case 0
		
			if is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], miss_vehicle)
				if not is_ped_sitting_in_vehicle(player_ped_id(), miss_vehicle)
					task_look_at_entity(selector_ped.pedID[selector_ped_michael], player_ped_id(), -1)
					trevor_looking_at_player_entering_vehicle_status++
				endif 
			endif 
		
		break 
		
		case 1
		
			if is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], miss_vehicle)
			and is_ped_sitting_in_vehicle(player_ped_id(), miss_vehicle)
				task_clear_look_at(selector_ped.pedID[selector_ped_michael])
				trevor_looking_at_player_entering_vehicle_status = 0
			endif 
			
		break 
		
	endswitch 

endproc

proc request_swap_to_michael_cutscene_assets()
			
	REQUEST_MODEL(michael_model)
	REQUEST_MODEL(trevor_model)
	REQUEST_MODEL(franklin_model)
	request_model(gang_enemy[0].model)
	REQUEST_MODEL(animal[0].model)
	REQUEST_MODEL(helicopter.model)
	REQUEST_MODEL(franklins_car.model)
	request_model(gang_car.model)
	request_model(rpg.model)
	REQUEST_VEHICLE_RECORDING(helicopter.recording_number, str_carrec)
	REQUEST_VEHICLE_RECORDING(CARREC_HELI_HOVER, str_carrec)
	REQUEST_VEHICLE_RECORDING(007, str_carrec)
	request_vehicle_recording(001, "lkexile2")
	request_vehicle_recording(ambient_car.recording_number, "lkexile2")
	REQUEST_WAYPOINT_RECORDING("exile2_0")
	REQUEST_WAYPOINT_RECORDING("exile2_1")
	REQUEST_WAYPOINT_RECORDING("exile2_2")
	REQUEST_WAYPOINT_RECORDING("exile2_3")
	//REQUEST_ANIM_DICT("missexile2switch")
	request_anim_dict("missexile2")
	request_anim_dict(strAnimDict)
	REQUEST_ANIM_SET("move_ped_crouched")
	REQUEST_ANIM_SET("Wpn_AssaultRifle_WeaponHoldingCrouched")
	REQUEST_ANIM_SET("move_ped_crouched_strafing")
	request_weapon_asset(WEAPONTYPE_HEAVYSNIPER)
	request_model(get_weapontype_model(WEAPONTYPE_HEAVYSNIPER))
	REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	//REQUEST_CHOP_CAM_SFX()
	REQUEST_PTFX_ASSET() 
	
endproc

func bool has_swap_to_micahel_cutscene_assets_loaded()

	IF HAS_MODEL_LOADED(michael_model) 
	AND HAS_MODEL_LOADED(franklin_model)
	AND HAS_MODEL_LOADED(trevor_model)
	and has_model_loaded(gang_enemy[0].model)
	and HAS_MODEL_LOADED(animal[0].model)
	and HAS_MODEL_LOADED(helicopter.model)
	and HAS_MODEL_LOADED(franklins_car.model)
	and HAS_MODEL_LOADED(gang_car.model)
	and has_model_loaded(rpg.model)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(helicopter.recording_number, str_carrec)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_HELI_HOVER, str_carrec)
	and has_vehicle_recording_been_loaded(007, str_carrec)
	and has_vehicle_recording_been_loaded(001, "lkexile2")
	and has_vehicle_recording_been_loaded(ambient_car.recording_number, "lkexile2")
	and get_is_waypoint_recording_loaded("exile2_0")
	and get_is_waypoint_recording_loaded("exile2_1")
	and get_is_waypoint_recording_loaded("exile2_2")
	and get_is_waypoint_recording_loaded("exile2_3")
	and has_anim_dict_loaded("missexile2")
	and has_anim_dict_loaded(strAnimDict)
	and has_anim_set_loaded("move_ped_crouched")
	and has_anim_set_loaded("Wpn_AssaultRifle_WeaponHoldingCrouched")
	and has_anim_set_loaded("move_ped_crouched_strafing")
	and has_weapon_asset_loaded(WEAPONTYPE_HEAVYSNIPER)
	and has_model_loaded(get_weapontype_model(WEAPONTYPE_HEAVYSNIPER))
	and REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	//and REQUEST_CHOP_CAM_SFX()
	and has_ptfx_asset_loaded()
	//and STREAMVOL_HAS_LOADED(streaming_volume)
	
		return true 
		
	endif 
	
	return false
	
endfunc 

proc remove_swap_to_michael_cutscene_assets()

	set_model_as_no_longer_needed(michael_model)
	set_model_as_no_longer_needed(trevor_model)
	set_model_as_no_longer_needed(franklin_model)
	set_model_as_no_longer_needed(gang_enemy[0].model)
	set_model_as_no_longer_needed(animal[0].model)
	set_model_as_no_longer_needed(helicopter.model)
	set_model_as_no_longer_needed(franklins_car.model)
	set_model_as_no_longer_needed(gang_car.model)
	set_model_as_no_longer_needed(rpg.model)
	remove_VEHICLE_RECORDING(helicopter.recording_number, str_carrec)
	remove_VEHICLE_RECORDING(CARREC_HELI_HOVER, str_carrec)
	remove_VEHICLE_RECORDING(007, str_carrec)
	remove_vehicle_recording(001, "lkexile2")
	remove_WAYPOINT_RECORDING("exile2_0")
	remove_WAYPOINT_RECORDING("exile2_1")
	remove_WAYPOINT_RECORDING("exile2_2")
	remove_WAYPOINT_RECORDING("exile2_3")
	remove_anim_dict("missexile2")
	remove_ANIM_SET("move_ped_crouched")
	remove_ANIM_SET("Wpn_AssaultRifle_WeaponHoldingCrouched")
	remove_ANIM_SET("move_ped_crouched_strafing")
	remove_weapon_asset(WEAPONTYPE_HEAVYSNIPER)
	set_model_as_no_longer_needed(get_weapontype_model(WEAPONTYPE_HEAVYSNIPER))
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("EXILE_2_01")

endproc 

proc stream_get_helicopter_assets_system()

	if is_entity_at_coord(player_ped_id(), <<-1503.4, 4358.3, 155.7>>, <<220.0, 220, 120.0>>)//120
		
		if not load_assets
		
			request_swap_to_michael_cutscene_assets()	
			
			//streaming_volume = STREAMVOL_CREATE_SPHERE(<<-1683.4967, 4454.5952, 1.5660>>, 10.00, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
		
			vector vec_ba_direction
			
			vec_ba_direction = normalise_vector((<<-1429.0, 4308.2, 113.4>> - <<-1687.750000,4457.776855,1.732926>>))
			
			streaming_volume = STREAMVOL_CREATE_frustum(<<-1687.750000,4457.776855,1.732926>>, vec_ba_direction, 500.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)   
			
			//streaming_volume = STREAMVOL_CREATE_SPHERE(<<-1683.4967, 4454.5952, 1.5660>>, 10.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
			
			load_assets = true
			
		endif 
	
	elif not is_entity_at_coord(player_ped_id(), <<-1503.4, 4358.3, 155.7>>, <<300.0, 300.0, 200.0>>)
	
		if load_assets
		
			remove_swap_to_michael_cutscene_assets()
			
			STREAMVOL_DELETE(streaming_volume)
			
			load_assets = false
			
		endif 

	endif
	
endproc 

func int get_closest_cover_node(ped_index mission_buddy, cover_node_struct &node[])
	
	float smallest_distance_between_vectors
	float current_distance_between_vectors
	int current_node = 0
	int i = 0
	
	for i = 0 to count_of(node)  - 1
	
		current_distance_between_vectors = get_distance_between_coords(GET_ENTITY_COORDS(mission_buddy), node[i].pos) 

		if i = 0
		
			smallest_distance_between_vectors = current_distance_between_vectors
			current_node = 0
		
		else 
			
			if current_distance_between_vectors < smallest_distance_between_vectors
				smallest_distance_between_vectors = current_distance_between_vectors
				current_node = i
			endif 
		
		endif 
		
	endfor 
	
	return current_node
	
endfunc 

proc franklin_ai_system()
	
	//int closest_cover_index 
		
	if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
	
		switch franklin_ai_system_status 
		
			case 0
			
				if not is_entity_at_coord(selector_ped.pedID[selector_ped_franklin], <<-1683.4967, 4454.5952, 1.5660>>, <<1.0, 1.0, 2.0>>)
					if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin])
						
						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<-1683.4967, 4454.5952, 1.5660>>, pedmove_run, -1, 0.2)
						close_sequence_task(seq)
						task_perform_sequence(selector_ped.pedID[selector_ped_franklin], seq)
						clear_sequence_task(seq)
						
					endif 
					
				else 
				
					if not is_ped_facing_ped(selector_ped.pedID[selector_ped_franklin], player_ped_id(), 30.00)
							
						if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin], SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
								
							task_turn_ped_to_face_entity(selector_ped.pedID[selector_ped_franklin], player_ped_id())
									
						endif 
						
					else 
							
						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_franklin])) < 10.00
							if not has_label_been_triggered("TM44_FRKHELI")
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "TM44AUD", "TM44_FRKHELI", CONV_PRIORITY_medium)
										set_label_as_triggered("TM44_FRKHELI", true)
									endif 
								endif
							endif 
						endif 
					
					endif 
				
				endif 

			break 
		
			case 100
			
				if not (manage_gang_enemy_3_status >= 21)
				
					//wood on the other side of the river 
					if not is_entity_in_angled_area(selector_ped.pedID[selector_ped_franklin], <<-1638.336, 4506.230, -3.750>>, <<-1498.742, 4400.691, 28.250>>, 66.5, false)
				
						clear_ped_tasks(selector_ped.pedID[selector_ped_franklin])
						set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_franklin], true)
						
						open_sequence_task(seq)
							TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_3", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT)
							task_aim_gun_at_coord(null, <<-1542.6, 4480.2, 21.4>>, -1)
						close_sequence_task(seq)
						task_perform_sequence(selector_ped.pedID[selector_ped_franklin], seq)
						clear_sequence_task(seq)
						
						franklin_ai_system_status = 101
						
					else 
					
//						int closest_cover_index 
//					
//						closest_cover_index = get_closest_cover_node(selector_ped.pedID[selector_ped_franklin], franklin_cover_node)

						int total_number_of_points
						vector target_waypoint_pos 
						
						waypoint_recording_get_num_points("exile2_3", total_number_of_points)
						total_number_of_points -= 1
					
						if waypoint_recording_get_coord("exile2_3", total_number_of_points, target_waypoint_pos)

							//CHANGE closest_cover_index TO FINAL POSITION OF WAYPOINT
						
							open_sequence_task(seq)
								//task_follow_nav_mesh_to_coord(null, franklin_cover_node[closest_cover_index].pos, pedmove_run, -1, 0.2) 
								task_follow_nav_mesh_to_coord(null, target_waypoint_pos, pedmove_run, -1, 0.2)
								task_aim_gun_at_coord(null, <<-1542.6, 4480.2, 21.4>>, -1)
							close_sequence_task(seq)
							task_perform_sequence(selector_ped.pedID[selector_ped_franklin], seq)
							clear_sequence_task(seq)
							
							franklin_ai_system_status = 101
							
						endif 

					endif 
					
				else 
				
					franklin_ai_system_status = 103
				
				endif 


			break 
			
			case 101
			
				if not (manage_gang_enemy_3_status >= 21)
				
					if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[selector_ped_franklin])
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(selector_ped.pedID[selector_ped_franklin], 2.0) 
					endif 
					
					int total_number_of_points
					vector target_waypoint_pos 
					
					waypoint_recording_get_num_points("exile2_3", total_number_of_points)
					total_number_of_points -= 1
					
					if waypoint_recording_get_coord("exile2_3", total_number_of_points, target_waypoint_pos)
					
						if not is_entity_at_coord(selector_ped.pedID[selector_ped_franklin], target_waypoint_pos, <<2.0, 2.0, 2.0>>)  
					
							if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin])
							
								if not is_entity_in_angled_area(selector_ped.pedID[selector_ped_franklin], <<-1638.336, 4506.230, -3.750>>, <<-1498.742, 4400.691, 28.250>>, 66.5, false)
							
									open_sequence_task(seq)
										TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_3", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT)
										task_aim_gun_at_coord(null, <<-1542.6, 4480.2, 21.4>>, -1)
									close_sequence_task(seq)
									task_perform_sequence(selector_ped.pedID[selector_ped_franklin], seq)
									clear_sequence_task(seq)
									
								else 
								
//									int closest_cover_index 
//								
//									closest_cover_index = get_closest_cover_node(selector_ped.pedID[selector_ped_franklin], franklin_cover_node)

									open_sequence_task(seq)
										//task_follow_nav_mesh_to_coord(null, franklin_cover_node[closest_cover_index].pos, pedmove_run, -1, 0.2) 
										task_follow_nav_mesh_to_coord(null, target_waypoint_pos, pedmove_run, -1, 0.2)
										task_aim_gun_at_coord(null, <<-1542.6, 4480.2, 21.4>>, -1)
									close_sequence_task(seq)
									task_perform_sequence(selector_ped.pedID[selector_ped_franklin], seq)
									clear_sequence_task(seq)

								endif 
								
							endif 
							
						else 
						
							if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin])
	
								//int closest_cover_index 
								
								//closest_cover_index = get_closest_cover_node(selector_ped.pedID[selector_ped_franklin], franklin_cover_node)
								set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped_franklin], target_waypoint_pos, 4.0)
								set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_franklin], false)
								
								franklin_ai_system_status++
							
							endif 
						
						endif 
					endif 
					
				else 
			
					franklin_ai_system_status = 103
					
				endif 
			
			break 
			
			case 102
			
				if (manage_gang_enemy_3_status >= 21)
					set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_franklin], true)
					franklin_ai_system_status = 103
				endif 
			
			break 
			
			case 103
			
				//if chop_cam.state != chop_view_cam_on_chop

					if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(gang_enemy[3].ped)) > 10
						SET_PED_DESIRED_MOVE_BLEND_RATIO(selector_ped.pedID[selector_ped_franklin], 3.0)
					endif 

					if not is_entity_at_coord(selector_ped.pedID[selector_ped_franklin], get_entity_coords(gang_enemy[3].ped), <<2.5, 2.5, 4.0>>)
							
						if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin], SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY)

							task_follow_to_offset_of_entity(selector_ped.pedID[selector_ped_franklin], gang_enemy[3].ped, <<0.0, 0.0, 0.0>>, pedmove_run, -1, 1.0)
							
						endif 
				
					else 
					
						if is_entity_at_coord(selector_ped.pedID[selector_ped_franklin], get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 0.0>>), <<1.5, 1.5, 4.0>>)

							if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin], SCRIPT_TASK_SHOOT_AT_COORD)
								task_shoot_at_coord(selector_ped.pedID[selector_ped_franklin], get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 3.0>>), 2000, firing_type_1_then_aim)
							endif 
							
						endif 
					
					endif 			
			
			break 
			
			case 200
			
				printstring("test 0")
				printnl()
			
				//if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(helicopter.veh)) < 30.00
			
					printstring("test 1")
					printnl()
			
					if not is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_franklin], helicopter.veh)
						
						//if is_entity_at_coord(player_ped_id(), pickup_pos[closest_warp_pos_to_franklin], <<20.00, 20.00, 5.5>>) //<<10.00, 10.00, 5.00>>)

							
						//if does_blip_exist(locates_data.LocationBlip)
						if is_entity_at_coord(helicopter.veh, pickup_pos[closest_warp_pos_to_franklin], <<20.00, 20.00, 5.5>>) //<<10.00, 10.00, 5.00>>)
						and IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh)
						and get_entity_speed(helicopter.veh) < 0.2

							if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin], script_task_enter_vehicle)
								
								task_enter_vehicle(selector_ped.pedID[selector_ped_franklin], helicopter.veh, -1, vs_back_right)
								
							endif 
							
						endif 
			
					endif 
				
				//else 
				
					if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), get_entity_coords(helicopter.veh)) > 45.00
				
						if not is_entity_at_coord(selector_ped.pedID[selector_ped_franklin], franklin_run_to_pos, <<2.0, 2.0, 2.0>>)
						
							if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin])
							
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, franklin_run_to_pos, pedmove_run, -1, 0.2)
									task_turn_ped_to_face_entity(null, helicopter.veh)
								close_sequence_task(seq)
								task_perform_sequence(selector_ped.pedID[selector_ped_franklin], seq)
								clear_sequence_task(seq)
								
							endif 
							
						else 
						
							if is_ped_facing_ped(selector_ped.pedID[selector_ped_franklin], player_ped_id(), 40)
								if has_ped_task_finished_2(selector_ped.pedID[selector_ped_franklin])
								
									open_sequence_task(seq)
										task_turn_ped_to_face_entity(null, helicopter.veh)
									close_sequence_task(seq)
									task_perform_sequence(selector_ped.pedID[selector_ped_franklin], seq)
									clear_sequence_task(seq)
									
								endif 
							endif 

						endif 
						
					endif 
					
				//endif 
				
			break 
			
			case 999
			
				//do nothing 
			
			break 
			
		endswitch 
	
	else 
	
		if get_current_player_ped_enum() = char_franklin
		and chop_cam.state = CHOP_VIEW_CAM_on_CHOP
		
			switch franklin_ai_system_status 
		
//				case 100
//				
//					//water enemy
//					//if is_entity_in_angled_area(selector_ped.pedID[selector_ped_franklin], 
//			
//						if not is_ped_injured(gang_enemy[1].ped)
//							if is_ped_in_combat(gang_enemy[1].ped) or manage_gang_enemy_1_status = 22
//								if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[1].ped)) < 80.00
//									
//									set_ped_sphere_defensive_area(player_ped_id(), <<-1708.74, 4440.79, 4.25>>, 3.0)
//									task_combat_ped(player_ped_id(), gang_enemy[1].ped)
//									set_blocking_of_non_temporary_events(player_ped_id(), false)
//									//script_assert("test 0")
//									
//									franklin_ai_system_status++
//									
//								endif 
//							endif 
//						endif 
//						
//					//endif 
//					
//					
//					//enemies in tree area across the river
//					if is_entity_in_angled_area(player_ped_id(), <<-1638.336, 4506.230, -3.750>>, <<-1498.742, 4400.691, 28.250>>, 66.5, false)
//
//						int closest_cover_index 
//						
//						closest_cover_index = get_closest_cover_node(player_ped_id(), franklin_cover_node)
//
//						if not is_ped_in_combat(player_ped_id())
//
//							open_sequence_task(seq)
//								TASK_GO_TO_COORD_WHILE_AIMING_AT_coord(null, franklin_cover_node[closest_cover_index].pos, <<-1556.55, 4488.35, 22.97>>, pedmove_run, false, -1) 
//								task_put_ped_directly_into_cover(null, franklin_cover_node[closest_cover_index].pos, -1)
//							close_sequence_task(seq)
//							task_perform_sequence(player_ped_id(), seq)
//							clear_sequence_task(seq)
//
//						endif 
//
//						//setup_relationship_contact(player_ped_id())
//						set_ped_sphere_defensive_area(player_ped_id(), franklin_cover_node[closest_cover_index].pos, 4.0)
//						set_blocking_of_non_temporary_events(player_ped_id(), false)
//						
//						franklin_ai_system_status++
//					
//						printvector(franklin_cover_node[closest_cover_index].pos)
//						printnl()
//						
//					endif 
//					
//					if is_entity_in_water(player_ped_id())
//						//walk to the closest node 
//						//set the defensive sphere incase the ped comes into combat with an enemy 
//						//set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped_franklin], franklin_cover_node[closest_cover_index].pos, 4.0)
//						//set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_franklin], false)
//	//					franklin_ai_system_status++
//					endif 
//
//				break
//				
//				case 102
//				
////					if not is_ped_injured( gang_enemy[1].ped)
////						task_combat_ped(player_ped_id(), gang_enemy[1].ped)
////					endif 
//					
//				break 

				case 100
			
					if not (manage_gang_enemy_3_status >= 21)
					
						//wood on the other side of the river 
						if not is_entity_in_angled_area(player_ped_id(), <<-1638.336, 4506.230, -3.750>>, <<-1498.742, 4400.691, 28.250>>, 66.5, false)
					
							open_sequence_task(seq)
								TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_3", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT)
								task_aim_gun_at_coord(null, <<-1542.6, 4480.2, 21.4>>, -1)
							close_sequence_task(seq)
							task_perform_sequence(player_ped_id(), seq)
							clear_sequence_task(seq)
							
							franklin_ai_system_status = 101
							
						else 
						
	//						int closest_cover_index 
	//					
	//						closest_cover_index = get_closest_cover_node(player_ped_id(), franklin_cover_node)

							int total_number_of_points
							vector target_waypoint_pos 
							
							waypoint_recording_get_num_points("exile2_3", total_number_of_points)
							total_number_of_points -= 1
						
							if waypoint_recording_get_coord("exile2_3", total_number_of_points, target_waypoint_pos)

								//CHANGE closest_cover_index TO FINAL POSITION OF WAYPOINT
							
								open_sequence_task(seq)
									//task_follow_nav_mesh_to_coord(null, franklin_cover_node[closest_cover_index].pos, pedmove_run, -1, 0.2) 
									task_follow_nav_mesh_to_coord(null, target_waypoint_pos, pedmove_run, -1, 0.2)
									task_aim_gun_at_coord(null, <<-1542.6, 4480.2, 21.4>>, -1)
								close_sequence_task(seq)
								task_perform_sequence(player_ped_id(), seq)
								clear_sequence_task(seq)
								
								franklin_ai_system_status = 101
								
							endif 

						endif 
						
					else 
					
						franklin_ai_system_status = 103
					
					endif 


			break 
			
			case 101
			
				if not (manage_gang_enemy_3_status >= 21)
				
					if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(player_ped_id())
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(player_ped_id(), 2.0) 
					endif 
					
					int total_number_of_points
					vector target_waypoint_pos 
					
					waypoint_recording_get_num_points("exile2_3", total_number_of_points)
					total_number_of_points -= 1
					
					if waypoint_recording_get_coord("exile2_3", total_number_of_points, target_waypoint_pos)
					
						if not is_entity_at_coord(player_ped_id(), target_waypoint_pos, <<2.0, 2.0, 2.0>>)  
					
							if has_ped_task_finished_2(player_ped_id())
							
								if not is_entity_in_angled_area(player_ped_id(), <<-1638.336, 4506.230, -3.750>>, <<-1498.742, 4400.691, 28.250>>, 66.5, false)
							
									open_sequence_task(seq)
										TASK_FOLLOW_WAYPOINT_RECORDING(null, "exile2_3", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT)
										task_aim_gun_at_coord(null, <<-1542.6, 4480.2, 21.4>>, -1)
									close_sequence_task(seq)
									task_perform_sequence(player_ped_id(), seq)
									clear_sequence_task(seq)
									
								else 
								
//									int closest_cover_index 
//								
//									closest_cover_index = get_closest_cover_node(player_ped_id(), franklin_cover_node)

									open_sequence_task(seq)
										//task_follow_nav_mesh_to_coord(null, franklin_cover_node[closest_cover_index].pos, pedmove_run, -1, 0.2) 
										task_follow_nav_mesh_to_coord(null, target_waypoint_pos, pedmove_run, -1, 0.2)
										task_aim_gun_at_coord(null, <<-1542.6, 4480.2, 21.4>>, -1)
									close_sequence_task(seq)
									task_perform_sequence(player_ped_id(), seq)
									clear_sequence_task(seq)

								endif 
								
							endif 
							
						else 
						
							if has_ped_task_finished_2(player_ped_id())
	
								int closest_cover_index 
								
								closest_cover_index = get_closest_cover_node(player_ped_id(), franklin_cover_node)
								set_ped_sphere_defensive_area(player_ped_id(), franklin_cover_node[closest_cover_index].pos, 4.0)
								set_blocking_of_non_temporary_events(player_ped_id(), false)
								
								franklin_ai_system_status++
							
							endif 
						
						endif 
					endif 
					
				else 
			
					franklin_ai_system_status = 103
					
				endif 
			
			break 
			
			case 102
			
				if (manage_gang_enemy_3_status >= 21)
					set_blocking_of_non_temporary_events(player_ped_id(), true)
					franklin_ai_system_status = 103
				endif 
			
			break 
			
			case 103
			
				//if chop_cam.state != chop_view_cam_on_chop

					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(gang_enemy[3].ped)) > 10
						SET_PED_DESIRED_MOVE_BLEND_RATIO(chop.ped, 3.0)
					endif 

					if not is_entity_at_coord(player_ped_id(), get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, -10.0, 0.0>>), <<2.5, 2.5, 4.0>>)
							
						if has_ped_task_finished_2(player_ped_id(), SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY)

							task_follow_to_offset_of_entity(player_ped_id(), gang_enemy[3].ped, <<0.0, 0.0, 0.0>>, pedmove_run, -1, 1.0)
							
						endif 
				
					else 
					
						if is_entity_at_coord(player_ped_id(), get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, -10.0, 0.0>>), <<1.5, 1.5, 4.0>>)

							if has_ped_task_finished_2(player_ped_id(), SCRIPT_TASK_SHOOT_AT_COORD)
								task_shoot_at_coord(player_ped_id(), get_offset_from_entity_in_world_coords(gang_enemy[3].ped, <<0.0, 0.0, 3.0>>), 2000, firing_type_1_then_aim)
							endif 
							
						endif 
					
					endif 			
			
			break 
			
			case 200
			
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(helicopter.veh)) < 30.00
			
					if is_entity_at_coord(helicopter.veh, pickup_pos[closest_warp_pos_to_franklin], <<20.00, 20.00, 5.00>>) //<<10.00, 10.00, 5.00>>)

						if not is_ped_sitting_in_vehicle(player_ped_id(), helicopter.veh)
							
							if has_ped_task_finished_2(player_ped_id(), script_task_enter_vehicle)
								
								task_enter_vehicle(player_ped_id(), helicopter.veh, -1, vs_back_right)
								
							endif 
				
						endif 
						
					endif 
				
				else 
				
					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(helicopter.veh)) > 45.00
				
						if not is_entity_at_coord(player_ped_id(), franklin_run_to_pos, <<2.0, 2.0, 2.0>>)
						
							if has_ped_task_finished_2(player_ped_id())
							
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, franklin_run_to_pos, pedmove_run, -1, 0.2)
									task_turn_ped_to_face_entity(null, helicopter.veh)
								close_sequence_task(seq)
								task_perform_sequence(player_ped_id(), seq)
								clear_sequence_task(seq)
								
							endif 
							
						else 
						
							if is_ped_facing_ped(player_ped_id(), player_ped_id(), 40)
								if has_ped_task_finished_2(player_ped_id())
								
									open_sequence_task(seq)
										task_turn_ped_to_face_entity(null, helicopter.veh)
									close_sequence_task(seq)
									task_perform_sequence(player_ped_id(), seq)
									clear_sequence_task(seq)
									
								endif 
							endif 

						endif 
						
					endif 
					
				endif 
				
			break 
			
			endswitch 
		
		endif 
		
	endif 

endproc 

proc helicopter_check_point_system()
							
	if does_blip_exist(locates_data.LocationBlip)
		if not helicopter_check_point_exists
			helicopter_check_point = CREATE_CHECKPOINT(checkpoint_race_air_chevron_1, <<-1503.4, 4358.3, 155.7>>, <<-1516.6, 4352.1, 155.7>>, 1.0)
			helicopter_check_point_exists = true
		endif 
	else 
		if helicopter_check_point_exists
			DELETE_CHECKPOINT(helicopter_check_point)
			helicopter_check_point_exists = false
		endif 
	endif 
	
endproc 


func bool get_helicopter()

	#IF IS_DEBUG_BUILD
		dont_do_j_skip(Locates_Data)
	#endif 

	IF e_section_stage = SECTION_STAGE_SETUP
	
		request_get_helicopter_assets()
		
		if has_model_loaded(michael_model)
		and has_model_loaded(trevor_model)
		and has_model_loaded(trevors_truck.model)	
		and has_model_loaded(helicopter.model)
		and has_vehicle_asset_loaded(helicopter.model)
		and has_vehicle_recording_been_loaded(002, "lkexile2")
		and prepare_music_event("EXL2_SWITCH_START")

			if does_entity_exist(gang_car.veh)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(gang_car.veh)
			endif 
		
			clear_area(<<1554.6674, 3743.1150, 33.5237>>, 100, true)
		
			remove_ped_for_dialogue(scripted_speech, 2)//removed fake trevor

			CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, <<1554.6674, 3743.1150, 33.5237>>, 212.6490)
			SET_VEHICLE_USE_PLAYER_LIGHT_SETTINGS(trevors_truck.veh, true)//stops lighting pop bug 1506513
			start_playback_recorded_vehicle(trevors_truck.veh, 002, "lkexile2")
			skip_time_in_playback_recorded_vehicle(trevors_truck.veh, 5000)
			force_playback_recorded_vehicle_update(trevors_truck.veh)
			set_playback_speed(trevors_truck.veh, 0.0)
			set_entity_visible(trevors_truck.veh, false)
					
			create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, trevors_truck.veh, vs_front_right)
			setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
			setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
			SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
			add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
			set_entity_visible(selector_ped.pedID[selector_ped_michael], false)
	
			create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_trevor], char_trevor, trevors_truck.veh, vs_driver)
			setup_buddy_attributes(selector_ped.pedID[selector_ped_trevor])
			setup_relationship_contact(selector_ped.pedID[selector_ped_trevor], true)
			SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_trevor], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
			add_ped_for_dialogue(scripted_speech, 2, selector_ped.pedID[selector_ped_trevor], "trevor")
			set_entity_visible(selector_ped.pedID[selector_ped_trevor], false)

			trevors_current_car = trevors_truck.veh  //fix by refferencing trevors_truck instead of general 
			
			clear_player_wanted_level(player_id())
			set_wanted_level_multiplier(0.1)
			set_max_wanted_level(5)
			set_create_random_cops(true)
			
			if does_blip_exist(chop.blip)
				remove_blip(chop.blip)
			endif 
		
			//COMMENT BACK IN FOR AUTOMATIC GENERIC SWITCH
			make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
			selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_trevor]
			switch_system_2_status = 1
			eSwitchCamState = SWITCH_CAM_START_CODE_SWITCH
  
			clear_area(<<1768.1128, 3240.0544, 41.0869>>, 10.00, true)
			
			// Force the current one to respawn at new location
			// Set the position you want the vehicle to spawn at.
			UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, <<1702.9482, 3272.0217, 40.1539>>, 247.3247)

			// Force the current one to respawn at new location
			DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)//forces 
			
			helicopter.veh = create_vehicle(helicopter.model, <<1768.1128, 3240.0544, 41.0869>>, 42.5289)
			SET_VEHICLE_AS_RESTRICTED(helicopter.veh, 0)
			set_vehicle_livery(helicopter.veh, 1)
			SET_VEHICLE_ENGINE_CAN_DEGRADE(helicopter.veh, false)
			set_entity_only_damaged_by_player(helicopter.veh, true)
			set_vehicle_colours(helicopter.veh, 34, 34)
			SET_VEHICLE_EXTRA_COLOURS(helicopter.veh, 0, 0)	
			set_vehicle_on_ground_properly(helicopter.veh)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(helicopter.veh, true)
			
			//print_now("FS_help_1", default_god_text_time, 1)
			
			trigger_music_event("EXL2_SWITCH_START")
			
			if IS_AUDIO_SCENE_ACTIVE("EXILE_2_GO_TO_CRASH_SITE")
				STOP_AUDIO_SCENE("EXILE_2_GO_TO_CRASH_SITE")
			endif 
			
			e_section_stage = SECTION_STAGE_RUNNING
			
		endif 
	endif 
		
	IF e_section_stage = SECTION_STAGE_RUNNING

		if not IS_PLAYER_SWITCH_IN_PROGRESS()
	
			switch get_current_player_ped_enum()
						
				case char_trevor
				
					set_ped_reset_flag(player_ped_id(), PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly, true) //fix for bug 1276211
				
					switch get_helicopter_status 
					
						case 0
						
							chop_ai_system_status = 101 //makes chop follow selector_ped.pedID[selector_ped_frnaklin)
							
							DISABLE_LOCATES_REMOVE_BUDDIES_FROM_GROUP_ON_CLEANUP(locates_data, TRUE) 
						
							if IS_PLAYER_AT_LOCATION_WITH_BUDDY_any_means(locates_data, get_entity_coords(helicopter.veh), <<50.00, 50.00, 4.0>>, false, selector_ped.pedID[selector_ped_michael], "FS_HELI_2", "FS_michael_0", false, true)
							
								clear_mission_locate_stuff(locates_data, false)

								get_helicopter_status++
								
							endif 

						break 
						
						case 1
					
							michael_looking_at_player_entering_vehicle_system(helicopter.veh)
							
							stream_get_helicopter_assets_system()

							if not reset_heli_freeze_waiting_on_collision
								if is_ped_sitting_in_vehicle(player_ped_id(), helicopter.veh)
								
									if IS_AUDIO_SCENE_ACTIVE("EXILE_2_GO_TO_HELI")
										STOP_AUDIO_SCENE("EXILE_2_GO_TO_HELI")
									endif 
									
									START_AUDIO_SCENE("EXILE_2_FLY_HELI")
									
									
									if does_entity_exist(trevors_truck.veh)
										if is_vehicle_driveable(trevors_truck.veh)
											SET_MISSION_VEHICLE_GEN_VEHICLE(trevors_truck.veh, <<1791.2455, 3242.5083, 41.5105>>, 83.8551)
										endif 
									endif 
									set_model_as_no_longer_needed(trevors_truck.model)
								
									//stops chopper from freezing in mid air.
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(helicopter.veh, false)
									set_entity_load_collision_flag(helicopter.veh, true)
									reset_heli_freeze_waiting_on_collision = true
									REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "fly to franklin")
								endif 
							endif
							
							IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(locates_data, <<-1503.4, 4358.3, 155.7>>, <<0.1, 0.1, 0.1>>, false, selector_ped.pedID[selector_ped_michael], helicopter.veh, "FS_forest_0", "FS_michael_0", "", "FS_HELI_1", false, true) //"FS_HELI_0" = get the heliciopter

							if does_blip_exist(locates_data.LocationBlip)

								if not has_label_been_triggered("EXL2_HELI_LIFT")
									if is_ped_sitting_in_vehicle(player_ped_id(), helicopter.veh)
										if get_entity_height_above_ground(helicopter.veh) > 2.0
											trigger_music_event("EXL2_HELI_LIFT")
											set_label_as_triggered("EXL2_HELI_LIFT", true)
										endif 
									endif 
								endif 
							
								if IS_PED_IN_ANY_HELI(player_ped_id())
									set_blip_route(locates_data.LocationBlip, false)
								endif 

								if is_entity_at_coord(player_ped_id(), <<-1503.4, 4358.3, 155.7>>, <<70.0, 70.0, 70.0>>)//80 reduced for 1004578
								or get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_franklin])) < 50  
									
									kill_any_conversation()
									
									clear_mission_locate_stuff(locates_data)
									
									setup_vehicle_proofs(helicopter.veh)
									
									e_section_stage = SECTION_STAGE_SETUP
									mission_flow = STAGE_SWAP_TO_MICHAEL_CUTSCENE
									
									return true 
								
								else 
								
									int r
									int g
									int b
									int a
								
									get_hud_colour(hud_colour_yellow, r, g, b, a)
									
									//draws a wee arrow to aid the player
									draw_marker(marker_ring, <<-1503.4, 4358.3, 155.7>>, <<0.0, 0.0, 0.0>>, <<0.00, 0.0, 90.00>>, <<25.0, 25.0, 25.0>>, r, g, b, 100, true)//252, 238, 0)
									
								endif 
							endif 
							
						break 
						
					endswitch 
					
					get_to_the_helicopter_dialogue_system()
					
				break 
				
			endswitch
			
		endif 
		
		switch_system_2()
		
		franklin_ai_system()
		
		chop_ai_system()
		
		//gang_car_fire_system()

	endif
	
	IF e_section_stage = SECTION_STAGE_CLEANUP
		
		e_section_stage = SECTION_STAGE_SETUP
		mission_flow = STAGE_SWAP_TO_MICHAEL_CUTSCENE
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
	
		if not is_player_switch_in_progress()
	
			if start_new_cutscene_no_fade(false)
							
				clear_mission_locate_stuff(locates_data)
			
				set_entity_proofs(helicopter.veh, true, true, true, true, true)
			
				e_section_stage = SECTION_STAGE_SETUP
				mission_flow = STAGE_SWAP_TO_MICHAEL_CUTSCENE
				
			endif 
			
		endif 

	ENDIF
	
	return false

endfunc 

PROC SWAP_TO_MICHAEL_CUTSCENE()
	
	INT i
	
	IF e_section_stage = SECTION_STAGE_SETUP
		
		SET_ROADS_IN_AREA(vCountryRoadMin, vCountryRoadMax, FALSE)

		request_swap_to_michael_cutscene_assets()
		
		if not IS_STREAMVOL_ACTIVE()
		
//			vector vec_ba_direction
//			
//			vec_ba_direction = normalise_vector((<<-1429.0, 4308.2, 113.4>> - <<-1687.750000,4457.776855,1.732926>>))
//			
//			streaming_volume = STREAMVOL_CREATE_frustum(<<-1687.750000,4457.776855,1.732926>>, vec_ba_direction, 100, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)   
	
		
//			streaming_volume = STREAMVOL_CREATE_SPHERE(<<-1683.4967, 4454.5952, 1.5660>>, 10.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
		endif
		
		disable_control_action(PLAYER_CONTROL, INPUT_VEH_EXIT)

		if has_swap_to_micahel_cutscene_assets_loaded()
		
			if not is_any_text_being_displayed(locates_data)

				if start_new_cutscene_no_fade()
				
					add_scenario_blocking_area(<<-2607.6, 3715.3, 300.00>>, <<-541.9, 4969.0, -100.00>>) //whole forrest area
						
					CLEAR_AREA(get_entity_coords(player_ped_id()), 10000, true)
				
					clear_player_wanted_level(player_id())
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					set_max_wanted_level(0)
					set_create_random_cops(false)
					
					set_selector_ped_blocked(selector_ped, selector_ped_michael, false)
					make_selector_ped_selection(selector_ped, SELECTOR_PED_MICHAEL)
					take_control_of_selector_ped(selector_ped)
					
					clear_ped_tasks_immediately(player_ped_id())
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(player_ped_id(), TRUE)
					SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), false)
					
					ATTACH_ENTITY_TO_ENTITY(player_ped_id(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
					//SET_PED_DUCKING(player_ped_id(), TRUE)
					//TASK_TOGGLE_DUCK(player_ped_id(), TOGGLE_DUCK_ON)
					GIVE_WEAPON_TO_PED(player_ped_id(), WEAPONTYPE_HEAVYSNIPER, infinite_ammo, true, true)//1000 infinite_ammo
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, WEAPONCOMPONENT_AT_SCOPE_MAX) 
					SET_PED_MOVEMENT_CLIPSET(player_ped_id(), "move_ped_crouched")
					SET_PED_WEAPON_MOVEMENT_CLIPSET(player_ped_id(), "Wpn_AssaultRifle_WeaponHoldingCrouched")
					SET_PED_STRAFE_CLIPSET(player_ped_id(), "move_ped_crouched_strafing")
					SET_PLAYER_FORCED_AIM(player_id(), true)
					set_ped_reset_flag(player_ped_id(), PRF_InstantBlendToAim, true)
					force_ped_ai_and_animation_update(player_ped_id(), true)
						
					SET_SELECTOR_PED_ACTIVITY(selector_ped, SELECTOR_PED_MICHAEL, SELECTOR_ACTIVITY_ATTACHED_TO_HELI)
					SET_SELECTOR_PED_ACTIVITY(selector_ped, SELECTOR_PED_MICHAEL, SELECTOR_ACTIVITY_THERMAL_VISION)

					clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_franklin])
					SET_ENTITY_COORDS(selector_ped.pedID[selector_ped_franklin], <<-1683.4967, 4454.5952, 1.5660>>)
					SET_ENTITY_HEADING(selector_ped.pedID[selector_ped_franklin], 239.1512)
					SET_ENTITY_LOAD_COLLISION_FLAG(selector_ped.pedID[selector_ped_franklin], true)
					task_play_anim(selector_ped.pedID[selector_ped_franklin], "missexile2", "FranklinWaveToHelicopter", instant_blend_in, normal_blend_out, -1)//, af_looping) 
					force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_franklin])
					franklin_ai_system_status = 999 //do nothing 
					
					if does_entity_exist(chop.ped)
						clear_ped_tasks_immediately(chop.ped)
						set_entity_coords(chop.ped, <<-1679.8206, 4455.9878, 1.2653>>)
						set_entity_heading(chop.ped, 244.1821)//114
						SET_ENTITY_LOAD_COLLISION_FLAG(chop.ped, true)

						open_sequence_task(seq)					
							task_play_anim(null, "missexile2", "fra_0_ig_1_chop_growl", INSTANT_BLEND_IN, normal_BLEND_OUT)
							task_play_anim(null, "missexile2", "fra0_ig_12_chop_waiting_a", slow_blend_in, normal_blend_out, -1, af_looping)
						close_sequence_task(seq)
						task_perform_sequence(chop.ped, seq)
						clear_sequence_task(seq)
						
						force_ped_ai_and_animation_update(chop.ped)
									
						chop_ai_system_status = 200 
					endif 

					setup_vehicle_proofs(helicopter.veh)
					set_vehicle_extra(helicopter.veh, 12, true) 
					set_entity_proofs(helicopter.veh, false, true, true, true, true)
					set_entity_load_collision_flag(helicopter.veh, false)
					SET_VEHICLE_ENGINE_ON(helicopter.veh, TRUE, TRUE)
					SET_HELI_BLADES_FULL_SPEED(helicopter.veh)
					set_entity_visible(helicopter.veh, true)
					set_entity_only_damaged_by_player(helicopter.veh, true)
					FREEZE_ENTITY_POSITION(helicopter.veh, FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, helicopter.recording_number, str_carrec)
					//skip_time_in_playback_recorded_vehicle(helicopter.veh, 4850.0)
					skip_time_in_playback_recorded_vehicle(helicopter.veh, 7000.00)
					force_playback_recorded_vehicle_update(helicopter.veh)


					clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_trevor])
					set_ped_into_vehicle(selector_ped.pedID[selector_ped_trevor], helicopter.veh)

					for i = 1 to count_of(animal) - 1
						if i != 7
						and i != 14
						and i != 15
						and i != 16
							setup_animal(animal[i])
						endif 
					endfor 	
					
					ambient_car.veh = create_vehicle(ambient_car.model, ambient_car.pos, ambient_car.heading)
					set_vehicle_doors_locked(ambient_car.veh, vehiclelock_lockout_player_only)
					set_vehicle_on_ground_properly(ambient_car.veh)
					set_entity_proofs(ambient_car.veh, true, true, true, true, true)

					cam_cutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-1687.750000,4457.776855,1.732926>>, <<11.104589,-1.886935,-116.443832>>, 30.961531, TRUE)
					SET_CAM_PARAMS(cam_cutscene, <<-1687.750000,4457.776855,1.732926>>,<<15.726158,-1.886931,-118.336342>>, 18.311649, 2000)				
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					REPLAY_START_EVENT()
						
					if IS_AUDIO_SCENE_ACTIVE("EXILE_2_FLY_HELI")
						STOP_AUDIO_SCENE("EXILE_2_FLY_HELI")
					endif 
					
					start_AUDIO_SCENE("EXILE_2_HELI_IN_POSITION_CUTSCENE")
					
					trigger_music_event("EXL2_HELI_CS")
					
					original_time = get_game_timer()
					
					bPlayingHoverBackwards = FALSE
					
					i_current_event = 0
								
					e_section_stage = SECTION_STAGE_RUNNING
				endif 
				
			endif 
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
	
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() //forces michael to use third person anims. 
		
		HIDE_ACTIVE_PHONE(TRUE)
		
		OVERRIDE_LODSCALE_THIS_FRAME(1.0)

		SWITCH i_current_event
		
			case 0
			
				//chop_audio_system(chop_bark_button)

				if PLAY_SINGLE_LINE_FROM_CONVERSATION(scripted_speech, "TM44AUD", "TM44_SCENE", "TM44_SCENE_1", conv_priority_medium)
					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 10, REPLAY_IMPORTANCE_HIGHEST)
					
					task_aim_gun_at_coord(player_ped_id(), <<-1616.5, 4424.6, 9.4>>, -1, true) 
					i_current_event++
				endif 

			break 
			
			case 1
			
				if not is_any_text_being_displayed(locates_data)
				
					destroy_cam(cam_cutscene) 

					cam_cutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-1482.602295,4334.973633,98.817444>>,<<21.264481,-0.005575,-157.200668>>,39.269314, true)
					SET_CAM_PARAMS(cam_cutscene, <<-1482.611084,4334.993164,98.717444>>,<<20.558702,-0.005577,-158.632187>>,39.269314, 1000)				
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(helicopter.veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(helicopter.veh)
					ENDIF
						
					START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, CARREC_HELI_HOVER, str_carrec)
					SET_VEHICLE_ENGINE_ON(helicopter.veh, TRUE, TRUE)
					SET_HELI_BLADES_FULL_SPEED(helicopter.veh)
					force_playback_recorded_vehicle_update(helicopter.veh)
					
					
					task_aim_gun_at_coord(player_ped_id(), <<-1616.5, 4424.6, 9.4>>, -1, true)
					FORCE_PED_MOTION_STATE(player_ped_id(), MS_AIMING, TRUE)
					SET_PED_RESET_FLAG(player_ped_id(), PRF_InstantBlendToAimNoSettle, TRUE)
					force_ped_ai_and_animation_update(player_ped_id())
					
					set_gameplay_cam_relative_heading(-12)
					set_gameplay_cam_relative_pitch(-41)
					
					ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)
					//PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
					PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 

					i_current_event++
					
				endif
			
			break
			
			case 2
			
				if not is_cam_interpolating(cam_cutscene)

					e_section_stage = SECTION_STAGE_CLEANUP
					
				endif 
			
			break 

		ENDSWITCH
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			IF TIMERA() > 1000
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ENDIF
		
		//gang_car_fire_system()
		
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
	
		if IS_AUDIO_SCENE_ACTIVE("EXILE_2_HELI_IN_POSITION_CUTSCENE")
			stop_AUDIO_SCENE("EXILE_2_HELI_IN_POSITION_CUTSCENE")
		endif 
	
		deactivate_vehicle_proofs(helicopter.veh)
	
		SET_ENTITY_LOAD_COLLISION_FLAG(selector_ped.pedID[selector_ped_franklin], false)
		SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)

		set_selector_ped_blocked(selector_ped, SELECTOR_PED_TREVOR, TRUE)

		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, TRUE)
		
		DISABLE_AIM_CAM_THIS_UPDATE() //stops the flicker from the gameplay cam as the sniper hud cam is still active even though clear_ped_tasks is called 
		
		REPLAY_STOP_EVENT()
	
		e_section_stage = SECTION_STAGE_SETUP
		mission_flow = STAGE_SNIPE_ENEMIES
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		DO_SAFE_FADE_OUT()
		
		kill_any_conversation()
		
		if get_current_player_ped_enum() != char_michael
			make_selector_ped_selection(selector_ped, SELECTOR_PED_MICHAEL)
			take_control_of_selector_ped(selector_ped)
		endif 
		
		e_section_stage = SECTION_STAGE_CLEANUP
	ENDIF
ENDPROC

PROC SET_CAMERA_ROT_TO_SAME_AS_veh(VEHICLE_INDEX &mission_veh, CAMERA_INDEX &camera, VECTOR vAdditionalRot)
	
	VECTOR vCamRot
	
	IF is_vehicle_driveable(mission_veh)
		IF DOES_CAM_EXIST(camera)
			vCamRot.x = GET_entity_PITCH(mission_veh) 
			vCamRot.y = GET_entity_ROLL(mission_veh)              
			vCamRot.z = GET_entity_HEADING(mission_veh)
			
			vCamRot += vAdditionalRot
			
			SET_CAM_ROT(camera, vCamRot)
		ENDIF
	ENDIF
	
ENDPROC

func float get_sniper_rifle_aim_heading_for_ped(ped_index miss_ped)

	vector vec_ba
	float aim_heading
	
	vec_ba = get_entity_coords(miss_ped) - get_entity_coords(player_ped_id())
	
	//z rotation
	aim_heading = (get_heading_from_vector_2d(vec_ba.x, vec_ba.y) - get_entity_heading(player_ped_id()))
	
	if aim_heading > 180
		aim_heading -= 360
	endif 
	if aim_heading < -180
		aim_heading += 360
	endif 

	return aim_heading
	
endfunc 

func float get_sniper_rifle_aim_pitch_for_ped(ped_index miss_ped)

	vector vec_ba
	vector direction_vec
	
	vec_ba = get_entity_coords(miss_ped) - get_entity_coords(player_ped_id())
	
	direction_vec = normalise_vector(vec_ba)

	return (ATAN2(direction_vec.z, VMAG(<<direction_vec.x, direction_vec.y, 0.0>>)))

endfunc

func float get_aim_heading_for_ped(ped_index miss_ped)

	vector vec_ba
	float aim_heading
	
	vector game_cam = GET_FINAL_RENDERED_CAM_ROT()
	
	vec_ba = get_entity_coords(miss_ped) - GET_FINAL_RENDERED_CAM_coord()
	
	//z rotation
	aim_heading = (get_heading_from_vector_2d(vec_ba.x, vec_ba.y) - game_cam.z)
	
	if aim_heading > 180
		aim_heading -= 360
	endif 
	if aim_heading < -180
		aim_heading += 360
	endif 

	return aim_heading
	
endfunc 

func float get_aim_pitch_for_ped(ped_index miss_ped)

	vector vec_ba
	vector direction_vec
	
	vector game_cam = GET_FINAL_RENDERED_CAM_ROT()
	
	vec_ba = get_entity_coords(miss_ped) - GET_FINAL_RENDERED_CAM_coord()
	
	direction_vec = normalise_vector(vec_ba)

	return ((ATAN2(direction_vec.z, VMAG(<<direction_vec.x, direction_vec.y, 0.0>>))) - game_cam.x)

endfunc

/// PURPOSE:
//			stops gameplay camera rendering for a frame when switching to michael in the helicopter. Forces helicam to 
//			helicopter_camera_systemrender the sniper hud or scripted heli cam
func bool activate_heli_cam_a_frame_early_for_switch_to_michael()

	printstring("frame_early_for_switch_to_michael 0")
	printnl()

	if get_current_player_ped_enum() = char_michael
	
		printstring("frame_early_for_switch_to_michael 1")
		printnl()
	
		if is_player_switch_in_progress()
			
			printstring("frame_early_for_switch_to_michael 2")
			printnl()
			
			if (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
				
				printstring("frame_early_for_switch_to_michael 3")
				printnl()
				
				if GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_OUTRO
					printstring("frame_early_for_switch_to_michael 4")
					printnl()
					return true
				endif 
			endif 
		endif 
		
	endif 
	
	return false 
	
endfunc 

func bool should_block_heli_and_sniper_camera_for_chop()

	if is_player_switch_in_progress()
	
		if (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
		
			if GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_OUTRO
			
				if chop_cam.state = CHOP_VIEW_CAM_TO_CHOP 
				
					return true //forces the chop cam to render during the switch to chop
					
				endif 
			
			elif GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_INTRO
			
				if chop_cam.state = CHOP_VIEW_CAM_TO_PLAYER  
				
					return true  //forces the chop cam to render during the switch up to michael
					
				endif 

			endif 
		endif
	endif 
	
	return false
	
endfunc 

proc setup_heli_cam()
		
	heli_cam = CREATE_CAM("default_scripted_camera")
	ATTACH_CAM_TO_ENTITY(heli_cam, helicopter.veh, <<-0.390, 0.850, 1.350>>)
	SET_CAMERA_ROT_TO_SAME_AS_veh(helicopter.veh, heli_cam, <<current_cam_rot.x, 0.0, current_cam_rot.z>>)
	SET_CAM_FOV(heli_cam, 45.00)
	SET_CAM_CONTROLS_MINI_MAP_HEADING(heli_cam, true)
	SET_CAM_ACTIVE(heli_cam, TRUE)
		
endproc 


proc helicopter_camera_system(bool add_explosion_shake = false)

 	float fTemp
	
//	if does_cam_exist(heli_cam)
//		printstring("heli_cam exists")	
//		printnl()
//	else 
//		printstring("heli_cam DOES NOT exists")	
//		printnl()
//	endif 
//	
//	if (get_player_ped_enum(player_ped_id()) = char_michael) 
//		printstring("test 0")
//		printnl()
//	endif 
//	
//	if not (chop_cam.state = CHOP_VIEW_CAM_ON_CHOP)
//		printstring("test 1")
//		printnl()
//	endif  
//	
//	if not is_player_switch_in_progress()
//		printstring("test 2")
//		printnl() 
//	else 
//
//		printstring("switch_in_progress state = ")
//		printint(enum_to_int(GET_PLAYER_SHORT_SWITCH_STATE()))
//		printnl()
//		
//	endif 
//	
//	if not does_cam_exist(scsSwitchCamScopeToFranklin.ciSpline)
//		printstring("test 3")
//		printnl()
//	endif 
//	
//	if (activate_heli_cam_a_frame_early_for_switch_to_michael())
//		//script_assert("frame before")
//	endif 

//	if (get_player_ped_enum(player_ped_id()) = char_michael  
//	and not (chop_cam.state = CHOP_VIEW_CAM_ON_CHOP) 
//	and ((not is_player_switch_in_progress()) or (activate_heli_cam_a_frame_early_for_switch_to_michael() and (chop_cam.state != CHOP_VIEW_CAM_TO_CHOP))))
	
	
	if (get_player_ped_enum(player_ped_id()) = char_michael  
	and not (chop_cam.state = CHOP_VIEW_CAM_ON_CHOP) 
	and not should_block_heli_and_sniper_camera_for_chop())

//		printstring("helicopter_camera_system 0")
//		printnl()

		SET_FOLLOW_CAM_IGNORE_ATTACH_PARENT_MOVEMENT_THIS_UPDATE()
		
		//resets forced_aim() which is switched on at the start of the mission stage
		if force_sniper_hud_for_start_of_mission
			if is_control_pressed(PLAYER_CONTROL, INPUT_AIM) 
			//and IS_FIRST_PERSON_AIM_CAM_ACTIVE()
				//clear_ped_tasks(player_ped_id())
				SET_PLAYER_FORCED_AIM(player_id(), false)
				force_sniper_hud_for_start_of_mission = false
			endif 
		endif 
		
		//when in first person mode the heli cam will never get a chance to create. Therfore when setup_switch_cam_to_michael_from_franklin() is called it initialises the pos and rotation of the camera. Therefore we need to ensure the camera exits
		if force_sniper_hud_for_first_person_view_mode
			if not does_cam_exist(heli_cam)
				setup_heli_cam()
			endif 
		endif
		
		//trigger the hit out on the frame the heli camera activates or the sniper camera is active
		if not trigger_hit_out_sound_fx_when_switching_to_michael
		
			//PLAY_SOUND_FRONTEND(-1, "Hit_out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
			
			trigger_hit_out_sound_fx_when_switching_to_michael = true 
			
			printstring("trigger_hit_out_sound_fx_when_switching_to_michael")
			printnl()
 
		endif 
		
		if render_sniper_hud
		or force_sniper_hud_for_first_person_view_mode
		
			if not lk_timer(sniper_hud_time, 2500)//2000

				switch render_sniper_hud_status 
				
					case 0

						if aiming_at_chop 
						
							if not is_ped_injured(chop.ped)
								sniper_rifle_aim_heading = get_sniper_rifle_aim_heading_for_ped(chop.ped)
								sniper_rifle_aim_pitch = get_sniper_rifle_aim_pitch_for_ped(chop.ped)
							endif 
						
						elif aiming_at_franklin
						
							if not is_ped_injured(selector_ped.pedID[selector_ped_franklin])
								sniper_rifle_aim_heading = get_sniper_rifle_aim_heading_for_ped(selector_ped.pedID[selector_ped_franklin])
								sniper_rifle_aim_pitch = get_sniper_rifle_aim_pitch_for_ped(selector_ped.pedID[selector_ped_franklin])
							endif 

						elif aiming_at_gang_enemy_0
							
							if not is_ped_injured(gang_enemy[0].ped)
								sniper_rifle_aim_heading = get_sniper_rifle_aim_heading_for_ped(gang_enemy[0].ped)
								sniper_rifle_aim_pitch = get_sniper_rifle_aim_pitch_for_ped(gang_enemy[0].ped)
							endif 
							
						elif aiming_at_gang_enemy_1
							
							if not is_ped_injured(gang_enemy[1].ped)
								sniper_rifle_aim_heading = get_sniper_rifle_aim_heading_for_ped(gang_enemy[1].ped)
								sniper_rifle_aim_pitch = get_sniper_rifle_aim_pitch_for_ped(gang_enemy[1].ped)
							endif 
							
						elif aiming_at_gang_enemy_2
							
							if not is_ped_injured(gang_enemy[2].ped)
								sniper_rifle_aim_heading = get_sniper_rifle_aim_heading_for_ped(gang_enemy[2].ped)
								sniper_rifle_aim_pitch = get_sniper_rifle_aim_pitch_for_ped(gang_enemy[2].ped)
							endif 
							
						elif aiming_at_gang_enemy_3
							
							if not is_ped_injured(gang_enemy[3].ped)
								sniper_rifle_aim_heading = get_sniper_rifle_aim_heading_for_ped(gang_enemy[3].ped)
								sniper_rifle_aim_pitch = get_sniper_rifle_aim_pitch_for_ped(gang_enemy[3].ped)
							endif 
							
						endif 
	
						//SET_PED_NO_TIME_DELAY_BEFORE_SHOT(player_ped_id()) 
						
						set_gameplay_cam_relative_heading(sniper_rifle_aim_heading)
						set_gameplay_cam_relative_pitch(sniper_rifle_aim_pitch)
						SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(2.8) //10 / 2.2 = 4.5 FOV
						
						render_sniper_hud_status++
						
					break 
					
					case 1
					
						//SET_PED_NO_TIME_DELAY_BEFORE_SHOT(player_ped_id()) 
					
					break 
					
				endswitch 
				
				DISPLAY_SNIPER_SCOPE_THIS_FRAME() 
				
				show_hud_component_this_frame(new_hud_reticle)
				
				printstring("display sniper")
				printnl()

			else 
				
				aiming_at_chop = false
				aiming_at_franklin = false
				aiming_at_gang_enemy_0 = false
				aiming_at_gang_enemy_1 = false
				aiming_at_gang_enemy_2 = false
				aiming_at_gang_enemy_3 = false

				if not force_sniper_hud_for_first_person_view_mode
				//if render_sniper_hud same logic as above
				
					//clear_ped_tasks(player_ped_id())
					SET_PLAYER_FORCED_AIM(player_id(), false)
					
				endif 

				render_sniper_hud = false

			endif 
		
		endif 
		

		if not render_sniper_hud
		and not force_sniper_hud_for_start_of_mission
		and not force_sniper_hud_for_first_person_view_mode
		
			printstring("heli cam 0")
			printnl()
			
			if is_player_free_aiming(player_id())
				printstring("player is free aiming")
				printnl()
			else 
				printstring("player is NOT aiming")
				printnl()
			endif 
			
			if IS_FIRST_PERSON_AIM_CAM_ACTIVE()
				printstring("FIRST_PERSON_AIM_CAM_ACTIVE")
				printnl()
			else 
				printstring("FIRST_PERSON_AIM_CAM_ACTIVE NOT active")
				printnl()
			endif 

			if not is_player_free_aiming(player_id())
			or not IS_FIRST_PERSON_AIM_CAM_ACTIVE()
			
				printstring("heli cam 1")
				printnl()

				if does_cam_exist(heli_cam)
					if not is_cam_active(heli_cam)
					
						//DISABLE_AIM_CAM_THIS_UPDATE() //stops the flicker from the gameplay cam as the sniper hud cam is still active even though clear_ped_tasks is called 
					
						SET_CAM_active(heli_cam, true)
						render_script_cams(true, false)
						set_entity_visible(player_ped_id(), true)
						
//						script_assert("heli cam active ")
						
					else 
					
						//if not is_cam
						render_script_cams(true, false)

					endif 
					
					//adds cam shake after rpg cutscene.
					if add_explosion_shake	
						shake_cam(heli_cam, "medium_EXPLOSION_SHAKE", 0.15)
						stop_heli_cam_explosion_shake = true 
						shake_time = get_game_timer()
					endif 
				
					if stop_heli_cam_explosion_shake
						if lk_timer(shake_time, 1000)
							if is_cam_shaking(heli_cam)
								stop_cam_shaking(heli_cam)
							endif 
						endif 
					endif 

					if not is_cam_shaking(heli_cam)
						
						shake_cam(heli_cam, "HAND_SHAKE", 1.0)
					endif 

				endif

				SWITCH helicopter_cam_status
													
					CASE 0
					
						setup_heli_cam()
						
						render_script_cams(TRUE, false)
		
						helicopter_cam_status++
					BREAK
					
					CASE 1
					
						if not IS_SELECTOR_ONSCREEN()
			
							INT left_stick_x
							int left_stick_y
							INT right_stick_x
							int right_stick_y
				
							GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)
							
							IF NOT IS_LOOK_INVERTED()
								right_stick_y *= -1
							ENDIF
							
							// invert the vertical
							IF (right_stick_y > STICK_DEAD_ZONE)
							OR (right_stick_y < (STICK_DEAD_ZONE * -1))
				
								fTemp = TO_FLOAT(right_stick_y)
								fTemp *= fTemp
								fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
								fTemp *= cam_speed_multiplier
								
								IF (right_stick_y < 0)
									fTemp *= -1.0
								ENDIF 
				
								current_cam_rot.x += fTemp
								
								IF (current_cam_rot.x < min_x_cam_change)
									current_cam_rot.x = min_x_cam_change	
								ENDIF
								IF (current_cam_rot.x >= max_x_cam_change)
									current_cam_rot.x = max_x_cam_change	
								ENDIF
								
							ENDIF
				
							IF (right_stick_x > STICK_DEAD_ZONE)
							OR (right_stick_x < (STICK_DEAD_ZONE * -1))
				
								fTemp = TO_FLOAT(right_stick_x)
								fTemp *= fTemp
								fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
								fTemp *= cam_speed_multiplier
								
								IF (right_stick_x > 0)
									fTemp *= -1.0
								ENDIF
								
								current_cam_rot.z += fTemp
								
								IF (current_cam_rot.z < min_z_cam_change)
									current_cam_rot.z = min_z_cam_change	
								ENDIF
								IF (current_cam_rot.z >= max_z_cam_change)
									current_cam_rot.z = max_z_cam_change	
								ENDIF
				
							ENDIF
							
						endif 
																		
						//SET_CAM_ATTACH_OFFSET(heli_cam, heli_cam_offset.x, heli_cam_offset.y, heli_cam_offset.z)
						SET_CAM_FOV(heli_cam, 45.00)
						SET_CAMERA_ROT_TO_SAME_AS_veh(helicopter.veh, heli_cam, <<current_cam_rot.x, 0.0, current_cam_rot.z>>)
						//SET_CAM_INHERIT_ROLL_VEHICLE(heli_cam, helicopter.veh)

					BREAK
				ENDSWITCH
				
			else 
			
				printstring("deactivte heli cam")
				printnl()
			
				if not is_player_switch_in_progress() //only deactivate heli cam if not switch ongoing
					if does_cam_exist(heli_cam)
						if is_cam_active(heli_cam)
						
							if is_cam_shaking(heli_cam)
								stop_cam_shaking(heli_cam, true)
							endif 
			
							SET_CAM_active(heli_cam, false)
							render_script_cams(false, false)
							
							//resets sniper rifle view
							//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							
//							script_Assert("heli cam deactivated ")

						endif 

					endif
				endif 
				
			endif 
			
		endif 
		
		 //or helicopter.veh + heading value
		
		//SET_GAMEPLAY_CAM_RELATIVE_PITCH
	
	else 

		//updates the heli cam in the background
		if does_cam_exist(heli_cam)
			if is_cam_active(heli_cam)
				SET_CAMERA_ROT_TO_SAME_AS_veh(helicopter.veh, heli_cam, <<current_cam_rot.x, 0.0, current_cam_rot.z>>)
			endif 
		endif
		
		//when switching from michael we must ensure the heli cam is only rendering untill the switch changes 
		//to the new player and not once the switch has finished. This will avoid us not seeing a flash back of 
		//frankklin as the heli cam is rendering. 
		//Added checks all the chop cam to render.

		if get_current_player_ped_enum() != char_michael
		and ((chop_cam.state != CHOP_VIEW_CAM_TO_CHOP) and (chop_cam.state != CHOP_VIEW_CAM_ON_CHOP))
		//and (chop_cam.state != CHOP_VIEW_CAM_ON_CHOP)
		and is_player_switch_in_progress()
			
			render_script_cams(false, false)
			
		endif 
		
		if get_current_player_ped_enum() = char_michael
			if chop_cam.state = CHOP_VIEW_CAM_TO_CHOP

				IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
					
					if (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
						
						if GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_OUTRO
							
							render_script_cams(false, false)
						
						endif 
						
					endif 
				endif 
			endif 
		endif 
	
	endif 
	
	if does_cam_exist(heli_cam)
		if is_cam_active(heli_cam)
			printstring("heli cam active")
			printnl()
		else 
			printstring("heli cam NOT active")
			printnl()
		endif 
	endif 

endproc 

proc thermal_vision_system()
		
	//IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
	//OR GET_CONTROL_VALUE(PLAYER_CONTROL, INPUT_AIM) >= 11
	
	if get_player_ped_enum(player_ped_id()) = char_michael
	and not (chop_cam.state = CHOP_VIEW_CAM_ON_CHOP)
	and ((not is_player_switch_in_progress()) or (activate_heli_cam_a_frame_early_for_switch_to_michael() and (chop_cam.state != CHOP_VIEW_CAM_TO_CHOP)))

//	if get_player_ped_enum(player_ped_id()) = char_michael
//	and not (chop_cam.state = CHOP_VIEW_CAM_ON_CHOP)
//	and not is_player_switch_in_progress() 

//	printstring("thermal vission processing 0")
//	printnl()
	
//	if (get_player_ped_enum(player_ped_id()) = char_michael  
//	and not (chop_cam.state = CHOP_VIEW_CAM_ON_CHOP) 
//	and not should_block_heli_and_sniper_camera_for_chop())

//		printstring("thermal vission processing 1")
//		printnl()
			
		if (is_control_pressed(PLAYER_CONTROL, INPUT_AIM) or force_sniper_hud_for_start_of_mission or render_sniper_hud or force_sniper_hud_for_first_person_view_mode)
		//and IS_FIRST_PERSON_AIM_CAM_ACTIVE()
		and ((IS_FIRST_PERSON_AIM_CAM_ACTIVE()) or (activate_heli_cam_a_frame_early_for_switch_to_michael() and (chop_cam.state != CHOP_VIEW_CAM_TO_CHOP)))
		
//			printstring("thermal vission processing 2")
//			printnl()
		
			if not initialise_sniper_aim_at_pos
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-12)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-41)
				initialise_sniper_aim_at_pos = true
			endif 
			
			if not stop_thermal_help
				if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FS_SNIPEHELP")
					PRINT_HELP_FOREVER("FS_SNIPEHELP")
				endif 
			endif 
			
			if GET_USINGSEETHROUGH() 
				
				if IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FS_SNIPEHELP")
					CLEAR_HELP()
				endif 
				
				stop_thermal_help = true
				
				if not IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
					START_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
				endif 
				
			else 
			
				if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
					stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
				endif 

			ENDIF
			
			//when switching to michael he is aiming with sniper hud on using thermal vision.
			//this allows the thermal vision to be switched off via the flag b_thermal_active
			IF IS_disabled_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)//INPUT_VEH_EXIT)
				IF b_thermal_active
					b_thermal_active = FALSE
					//SET_SEETHROUGH(false) 
				else 	
					b_thermal_active = true
				endif 
			endif 
			
			if b_thermal_active
			
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
				SET_SEETHROUGH(TRUE)
//				printstring("thermal vission processing 3")
//				printnl()
			else 
				SET_SEETHROUGH(false)
//				printstring("thermal vission processing 4")
//				printnl()
			endif 

		ELSE
		
//			printstring("thermal vission processing 5")
//			printnl()
		
			b_thermal_active = false
			
			SET_SEETHROUGH(FALSE)
			
			if IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FS_SNIPEHELP")
				CLEAR_HELP()
			endif 
			
			if not GET_USINGSEETHROUGH() 
				if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
					stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
				endif 
			endif 

		ENDIF
		
	else 
	
//		printstring("therermal vission NOT processing")
//		printnl()
		
	endif 
	
endproc 

proc find_rpg_enemy_dialogue_system()

	find_rpg_enemy_dialogue_time = find_rpg_enemy_dialogue_time 
				
	if lk_timer(find_rpg_enemy_dialogue_time, 15000) 
		if not is_any_text_being_displayed(locates_data)

			rpg_enemy_relative_heading_diff = get_aim_heading_for_ped(gang_enemy[3].ped)
			rpg_enemy_relative_pitch_diff = get_aim_pitch_for_ped(gang_enemy[3].ped)

			if absf(rpg_enemy_relative_heading_diff) > absf(rpg_enemy_relative_pitch_diff)
			
				if absf(rpg_enemy_relative_heading_diff) > 10
				
					if rpg_enemy_relative_heading_diff > 0
					
						IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_LEFT", CONV_PRIORITY_MEDIUM)
							find_rpg_enemy_dialogue_time = get_game_timer()
						endif 
						
					else
					
						IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_RIGHT", CONV_PRIORITY_MEDIUM)
							find_rpg_enemy_dialogue_time = get_game_timer()
						endif 
					
					endif 
					
				else 
				
					//look in the hill dialogue 
				
				endif 
				
			else 
		
				if absf(rpg_enemy_relative_pitch_diff) > 10
				
					if rpg_enemy_relative_pitch_diff > 0
					
						IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_UP", CONV_PRIORITY_MEDIUM)
							find_rpg_enemy_dialogue_time = get_game_timer()
						endif 
						
					else
					
						IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_DOWN", CONV_PRIORITY_MEDIUM)
							find_rpg_enemy_dialogue_time = get_game_timer()
						endif 
					
					endif 
					
				endif 
			
			endif 
			
		endif 
		
	endif 

endproc 

proc dialogue_system()

	int i = 0
		
	switch dialogue_system_status 
	
		case 0
		
			IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_FINDG", CONV_PRIORITY_medium)
				dialogue_time = get_game_timer()
				dialogue_system_status++
			endif 

		break 

		case 1
		
			if not is_any_text_being_displayed(locates_data)
				PRINT_NOW("FS_FINDGANG", DEFAULT_GOD_TEXT_TIME, 0)
				dialogue_system_status++
			endif 

		break 
		
		case 2
		
//			if not is_any_text_being_displayed(locates_data)
//				if create_conversation(scripted_speech, "TM44AUD", "TM44_chop1", CONV_PRIORITY_MEDIUM)
//					chop_time = get_game_timer()
//					set_label_as_triggered("TM44_chop1", true)
					dialogue_system_status++
//				endif 
//			endif 
		
		break 

		case 3
		
//			if lk_timer(original_time, 2000)
//				if not IS_HELP_MESSAGE_BEING_DISPLAYED()
//					print_help("chop_help1")
//					set_label_as_triggered("chop_help1", true)
					dialogue_system_status++
//				endif 
//			endif 
		
		break 
		
		case 4

//			if not has_label_been_triggered("TM44_M2L")
//				if not is_any_text_being_displayed(locates_data)
//					if ped_structure_are_specific_number_enemies_dead(gang_enemy, 1)
//						IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_M1L", CONV_PRIORITY_MEDIUM)
//							set_label_as_triggered("TM44_M2L", true)
//						endif 
//					endif 
//				endif 
//			endif 
//			
//			if not has_label_been_triggered("TM44_M2L")
//				if not is_any_text_being_displayed(locates_data)
//					if ped_structure_are_specific_number_enemies_dead(gang_enemy, 2)
//						IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_M0L", CONV_PRIORITY_MEDIUM)
//							set_label_as_triggered("TM44_M1L", true)
//						endif 
//					endif 
//				endif 
//			endif 
			
			if ped_structure_are_specific_number_enemies_dead(gang_enemy, 2)
				kill_any_conversation()
				dialogue_system_status = 5
			endif 
		
		break 
		
		case 5

			if not is_any_text_being_displayed(locates_data)
				if ped_structure_are_specific_number_enemies_dead(gang_enemy, 2)
					IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_M0L", CONV_PRIORITY_MEDIUM)
						dialogue_time = get_game_timer()
						dialogue_system_status++
					endif 
				endif  
			endif 
		
		break 
		
		case 6

			if lk_timer(dialogue_time, 4000)
				if not is_any_text_being_displayed(locates_data)
					IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_THL", CONV_PRIORITY_low)
						dialogue_time = get_game_timer()
						dialogue_system_status = 7
					endif 
				endif 
			endif 	
			
			//rocket fired - cutscene about to start soon
			if does_entity_exist(rpg_obj)
				kill_any_conversation()
				dialogue_system_status = 7
			endif 

		break 
		
		case 7

			if does_entity_exist(rpg_obj)
				
				IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_RPG3", CONV_PRIORITY_MEDIUM)
					dialogue_system_status = 22
				endif 
			
			else 
			
				find_rpg_enemy_dialogue_system()

			endif 
			
		break 
		
		case 8
				
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TM44_NOGET")  
				if not is_any_text_being_displayed(locates_data)
					if CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_NOGET", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("TM44_NOGET", TRUE)
						dialogue_system_status++
					endif 
				endif 
			endif 
			
		break 
		
		case 9
		
			dialogue_system_status++
		
//			IF NOT HAS_LABEL_BEEN_TRIGGERED("TM44_CANGO") 
//				if get_current_player_ped_enum() = char_michael
//					if not is_any_text_being_displayed(locates_data)
//						if CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_CANGO", CONV_PRIORITY_MEDIUM)
//							SET_LABEL_AS_TRIGGERED("TM44_CANGO", TRUE)
//							dialogue_system_status++
//						endif 
//					endif 
//				endif 
//			endif 

		break 
		
		case 10
	
			if get_current_player_ped_enum() = char_franklin 
			and not is_player_switch_in_progress()
				
				if create_conversation(scripted_speech, "TM44AUD", "TM44_FSWITCH", conv_priority_medium)
				
					ALLOW_DIALOGUE_IN_WATER(true)
					
					set_label_as_triggered("TM44_FSWITCH", true)
				
					dialogue_system_status++
					
				endif 
			endif 

		break 
		
		case 11
		
			if not is_any_text_being_displayed(locates_data)
			
				if chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
					print_now("FS_GOD2b", default_god_text_time, 1)
				endif 
				
				dialogue_system_status++
					
			endif 
		
		break 
		
		case 12
		
			CLEAR_TRIGGERED_LABELS()

			hunt_down_rpg_enemy = true
			
			dialogue_system_status++

		
		break 
		
		case 13
		
		break 
		
		case 22
		
		break 
		
	endswitch 
	
	if not is_any_text_being_displayed(locates_data)
	and not has_any_selector_ped_been_selected(selector_ped) 
	//and not does_cam_exist(selector_cam.camID)
	and not is_player_switch_in_progress()
	
		//if (dialogue_system_status != 13) //set to 13 w
		if not hunt_down_rpg_enemy

			//river enemy dialogue
			if not has_label_been_triggered("TM44_TWT")
				if not is_ped_injured(gang_enemy[1].ped)
					if (get_current_player_ped_enum() = char_michael 
					and (not is_player_switch_in_progress()))
						IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_TWT", CONV_PRIORITY_medium) 
							set_label_as_triggered("TM44_TWT", true)
							gang_enemy[1].time = get_game_timer()
						endif 
					endif 
				else 
					set_label_as_triggered("TM44_TWT", true)
				endif 
			endif

			//water enemy 
			if not is_ped_injured(gang_enemy[1].ped)
				if not has_label_been_triggered("TM44_WATER")
					if does_blip_exist(gang_enemy[1].blip)
						if create_conversation(scripted_speech, "TM44AUD", "TM44_WATER", conv_priority_medium)
							set_label_as_triggered("TM44_WATER", true)
						endif 
					endif 
				endif 
			
			else 
			
				if not has_label_been_triggered("TM44_DWATER")
					if create_conversation(scripted_speech, "TM44AUD", "TM44_DWATER", conv_priority_medium)
						set_label_as_triggered("TM44_DWATER", true)
					endif 
				endif 
			endif 
		
			//forrest enemy
			if not is_ped_injured(gang_enemy[1].ped)
				if not has_label_been_triggered("TM44_GWOOD")
					if does_blip_exist(gang_enemy[0].blip)
						if create_conversation(scripted_speech, "TM44AUD", "TM44_GWOOD", conv_priority_medium)
							set_label_as_triggered("TM44_GWOOD", true)
						endif 
					endif 
				endif 
				
			else 
			
				if not has_label_been_triggered("TM44_DEADR")
					if create_conversation(scripted_speech, "TM44AUD", "TM44_DEADR", conv_priority_medium)
						set_label_as_triggered("TM44_DEADR", true)
					endif 
				endif 

			endif 
			
		
			//reminder dialogue for the player for enemies 
			//water ped dialogue 
			if not is_ped_injured(gang_enemy[1].ped)
				if not does_blip_exist(gang_enemy[1].blip)
					if lk_timer(gang_enemy[1].time, 40000)
						IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_TWT", CONV_PRIORITY_low)
							gang_enemy[1].time = get_game_timer()
						endif 
					endif 
				endif 

			
			//TM44_THL switch out for new random dialogue see bug 965114   MISION SET BUG = 675091
			//east of the woods 
			elif not is_ped_injured(gang_enemy[0].ped)
				if not does_blip_exist(gang_enemy[0].blip)
					if lk_timer(gang_enemy[0].time, 40000)
						IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_THL2", CONV_PRIORITY_low) //TM44_THL
							gang_enemy[0].time = get_game_timer()
						endif 
					endif 
				endif 
				
			//middle of the woods
//			elif not is_ped_injured(gang_enemy[2].ped)
//				if not does_blip_exist(gang_enemy[2].blip)
//					if lk_timer(gang_enemy[2].time, 40000)
//						IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_INT", CONV_PRIORITY_low)
//							gang_enemy[2].time = get_game_timer()
//						endif 
//					endif 
//				endif 
			endif
			
			//if not has_label_been_triggered("TM44_DEADR")
				if is_ped_shooting(player_ped_id())
				
					bullet_count++
				
					if bullet_count > 4
						if not is_ped_injured(gang_enemy[1].ped)
						or not is_ped_injured(gang_enemy[0].ped)
					
							IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_MISS", CONV_PRIORITY_low)
						
								bullet_count = 0
								
							endif 

						endif 
					endif 
				endif 
				
			//endif 

		else 
		
			if get_current_player_ped_enum() = char_michael
			and chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
			and not is_player_switch_in_progress()
			
				if not has_label_been_triggered("TM44_MLOOK")
					IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_MLOOK", CONV_PRIORITY_MEDIUM)
						set_label_as_triggered("TM44_MLOOK", true)
					endif 
				endif 
				
			endif 

			if (manage_gang_enemy_3_status >= 21)
		
				if get_current_player_ped_enum() = char_franklin
				and chop_cam. state != CHOP_VIEW_CAM_ON_CHOP
				and not is_player_switch_in_progress()
			
					if not has_label_been_triggered("TM44_CHOPGO")
						if get_distance_between_coords(get_entity_coords(chop.ped), get_entity_coords(gang_enemy[3].ped)) < 30
							if create_conversation(scripted_speech, "TM44AUD", "TM44_CHOPGO", CONV_PRIORITY_medium)  
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								
								set_label_as_triggered("TM44_CHOPGO", true)
							endif 
						endif 
					endif 
					
					//trigger if not in combat or driving off in car
					if manage_gang_enemy_3_status = 21
						if lk_timer(gang_enemy_3_dialogue_time, 10000)
							//if has_label_been_triggered("TM44_CHOPGO")
								if has_ped_task_finished_2(gang_enemy[3].ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
									if create_conversation(scripted_speech, "TM44AUD", "TM44_RPGRUN0", CONV_PRIORITY_medium)  
										set_label_as_triggered("TM44_RPGRUN0", true)
										gang_enemy_3_dialogue_time = get_game_timer()
									endif 
								endif 
							//endif 
						endif 
					endif 
			
					if not has_label_been_triggered("TM44_mike")

						//trigger dialogue if label set or ped in combat as label won't have been set because he is in combat
						//if has_label_been_triggered("TM44_RPGRUN0")
						//or not has_ped_task_finished_2(gang_enemy[3].ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)

						IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_mike", CONV_PRIORITY_low)
					
							//michael_dialogue_time = get_game_timer()
							set_label_as_triggered("TM44_mike", true)
							
						endif

					endif 
					
				endif 
				
			else 
			
				if not has_label_been_triggered("TM44_LOK")

					if get_current_player_ped_enum() = char_franklin
					and not is_player_switch_in_progress()
						//if is_explosion_in_sphere(EXP_TAG_ROCKET, rpg.pos, 1.0)

						//if does_entity_exist(rpg_obj)
							if create_conversation(scripted_speech, "TM44AUD", "TM44_LOK", CONV_PRIORITY_low)
								set_label_as_triggered("TM44_LOK", true)
							endif 
						//endif 
					endif 
				
				else 
				
					printstring("test ")
					printnl()
				
					if not has_label_been_triggered("TM44_GOTIT")
						if get_current_player_ped_enum() = char_franklin
							if create_conversation(scripted_speech, "TM44AUD", "TM44_GOTIT", CONV_PRIORITY_low)
								set_label_as_triggered("TM44_GOTIT", true)
							endif 
						endif
					endif
				
				endif 

				if not has_label_been_triggered("TM44_BANG")
					if get_current_player_ped_enum() = char_franklin
						if get_distance_between_coords(get_entity_coords(chop.ped), get_entity_coords(gang_enemy[3].ped)) < 50
						and get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) < 30
							if create_conversation(scripted_speech, "TM44AUD", "TM44_BANG", CONV_PRIORITY_low)  
								set_label_as_triggered("TM44_BANG", true)
							endif 
						endif 
					endif 
				endif 
			
			endif 
			
			if not has_label_been_triggered("TM44_CHOPSWM")
				if get_current_player_ped_enum() = char_franklin
					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(chop.ped)) < 35
						if (IS_SYNCHRONIZED_SCENE_RUNNING(chop_cutscene_index) and GET_SYNCHRONIZED_SCENE_PHASE(chop_cutscene_index) < 0.5)
						or is_entity_at_coord(chop.ped, chop_cutscene_pos, <<4.0, 4.0, 2.5>>)
							if CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_CHOPSWM", CONV_PRIORITY_MEDIUM)
								set_label_as_triggered("TM44_CHOPSWM", true)
							endif 
						endif 
					endif 
				endif 
			endif 

			if get_current_player_ped_enum() = char_franklin
				if not has_label_been_triggered("TM44_COLD")
					if is_entity_in_water(player_ped_id())
						if not is_ped_swimming_under_water(player_ped_id())
							if CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_COLD", CONV_PRIORITY_MEDIUM)
								set_label_as_triggered("TM44_COLD", true)
							endif 
						endif 
					endif 
				endif 
			endif

		endif 
	endif 

	if not has_label_been_triggered("TM44_FRK0")
		if get_current_player_ped_enum() = char_michael
			IF IS_ENTITY_ON_SCREEN(selector_ped.pedID[selector_ped_franklin]) 
				IF GET_GAMEPLAY_CAM_FOV() < 10.0
					FLOAT f_x = 0.0
					FLOAT f_y = 0.0
					GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(selector_ped.pedID[selector_ped_franklin]), f_x, f_y)
				
					IF ABSF(f_x - 0.5) < 0.05
						IF ABSF(f_y - 0.5) < 0.05
							if create_conversation(scripted_speech, "TM44AUD", "TM44_FRK0", CONV_PRIORITY_MEDIUM)
								set_label_as_triggered("TM44_FRK0", true)
							endif 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		endif 
	endif 
	
	if get_current_player_ped_enum() = char_michael
		for i = 0 to count_of(animal) - 1
			if animal[i].created
				if is_ped_injured(animal[i].ped)
					IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_AML", CONV_PRIORITY_low)
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(EXL2_ANIMAL_KILLED) 
						animal[i].created = false
					endif 
				endif 
			endif 
		endfor  
	endif 

endproc 

proc setup_all_ped_proofs()

	switch get_current_player_ped_enum()
		
		case char_michael 
		
			setup_ped_proofs(player_ped_id())
			setup_ped_proofs(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
			setup_ped_proofs(chop.ped)
			setup_ped_proofs(gang_enemy[0].ped)
			setup_ped_proofs(gang_enemy[1].ped)
			setup_ped_proofs(gang_enemy[2].ped)
			setup_ped_proofs(gang_enemy[3].ped)
		
		break 
		
		case char_franklin 
		
			setup_ped_proofs(player_ped_id())
			setup_ped_proofs(selector_ped.pedID[SELECTOR_PED_MICHAEL])
			setup_ped_proofs(chop.ped)
			setup_ped_proofs(gang_enemy[0].ped)
			setup_ped_proofs(gang_enemy[1].ped)
			setup_ped_proofs(gang_enemy[2].ped)
			setup_ped_proofs(gang_enemy[3].ped)
			
			if not is_ped_injured(gang_enemy[3].ped)
				gang_enemy[3].health = get_entity_health(gang_enemy[3].ped) //cach health and resotre when switching back to franklin
			endif 
		
		break 
		
	endswitch 

endproc 

//enemies can only be killed by the player 				
//if chop michael or chop franklin proof enemies				
//you don't want the enemies to die if you are chop 
proc deactivate_all_ped_proofs()

	switch get_current_player_ped_enum()
		
		case char_michael 
		
			deactivate_ped_proofs(player_ped_id())
			deactivate_ped_proofs(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
			deactivate_ped_proofs(chop.ped)
			
			if chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
				
				deactivate_ped_proofs(gang_enemy[0].ped)
				deactivate_ped_proofs(gang_enemy[1].ped)
				deactivate_ped_proofs(gang_enemy[2].ped)
				
				//only deactivate proofs once the rpg enemy runs. You can't kill him if you are michael before he runs.
				if (manage_gang_enemy_3_status >= 21)
					
					if not is_ped_injured(gang_enemy[3].ped) 
						if get_entity_health(gang_enemy[3].ped) > 120 
							set_entity_health(gang_enemy[3].ped, 120)
							deactivate_ped_proofs(gang_enemy[3].ped)
						endif 
					endif 
				endif 
				
			endif 
		
		break 
		
		case char_franklin 
		
			deactivate_ped_proofs(player_ped_id())
			deactivate_ped_proofs(selector_ped.pedID[SELECTOR_PED_MICHAEL])
			deactivate_ped_proofs(chop.ped)
			
			if chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
			
				if not is_ped_injured(gang_enemy[3].ped) 
					set_entity_health(gang_enemy[3].ped, gang_enemy[3].health) //restore health to the cached health before the switch to michael
				endif 
			
				deactivate_ped_proofs(gang_enemy[0].ped)
				deactivate_ped_proofs(gang_enemy[1].ped)
				deactivate_ped_proofs(gang_enemy[2].ped)
				deactivate_ped_proofs(gang_enemy[3].ped)
				
			endif 
		
		break 
		
	endswitch 
	
endproc 


proc stop_dialogue_when_switching_to_michael()

	if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
		text_label dialogue_label
		
		dialogue_label = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT() 
		
		IF ARE_STRINGS_EQUAL (dialogue_label, "TM44_chop1")
			STOP_SCRIPTED_CONVERSATION(TRUE)
		endif 
	endif 
	
endproc 

func bool setup_switch_cam_to_michael_from_franklin()
	
	clear_help()
	
	if is_this_print_being_displayed("FS_GOD2b")
		clear_this_print("FS_GOD2b")
	endif 

	render_sniper_hud = true  //forces michael to aim at franklin set in RUN_CAM_SPLINE_FROM_PLAYER_TO_PED
	aiming_at_franklin = true //calculates relative heading and pitch to franklin from player.
	b_thermal_active = true
	
	if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
		force_sniper_hud_for_first_person_view_mode = true 
		render_sniper_hud = false //turn off normal render_sniper_hud system so it does not time out after 2000 ms 
	else 
		force_sniper_hud_for_first_person_view_mode = false
	endif
	
	//**********HACK for bug 404202 WILL REQUIRE CHANGE **********
	stop_dialogue_when_switching_to_michael()

	setup_all_ped_proofs()

	//initialise the current heli cam rotation
	
	//----NEW
	//force_sniper_hud_for_first_person_view_mode will result in the heli cam never being created as we force 
	//michael to aim constantly.  
	if not does_cam_exist(heli_cam)
	
		//setup_heli_cam()	
		
	else 
	
		current_cam_rot = <<-24.00, 0.0, 65.9>>	
		SET_CAMERA_ROT_TO_SAME_AS_veh(helicopter.veh, heli_cam, <<current_cam_rot.x, 0.0, current_cam_rot.z>>)
		set_cam_active(heli_cam, true)	
		
	endif 
	
	//sniper_hud_time = get_game_timer()
	
	turn_on_sniper_hud_for_exile_2 = true
	
	selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_MICHAEL]
		
	exile_2_switch_cam_status = switch_cam_to_michael_from_franklin  
	
	return true
		
	//
	
	//return false 

endfunc

func bool setup_switch_cam_to_chop_from_franklin() 
	
	clear_help()
	
	setup_all_ped_proofs()
		
	if is_this_print_being_displayed("FS_GOD2b")
		clear_this_print("FS_GOD2b")
	endif 
	
	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
		stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
	endif 

	if does_blip_exist(franklins_blip)
		remove_blip(franklins_blip)
	endif
	
	//selector_cam.pedTo = chop.ped//player_ped_id()
	
	START_PLAYER_SWITCH(PLAYER_PED_ID(), chop_cam.chopPed, INT_TO_ENUM(SWITCH_FLAGS, 0), SWITCH_TYPE_SHORT)
	
	chop_cam.state = CHOP_VIEW_CAM_to_CHOP
								
	exile_2_switch_cam_status = switch_cam_to_chop_from_franklin 

	return true

endfunc 

func bool setup_switch_cam_to_franklin_from_franklin_chop()

	clear_help()
	
	setup_all_ped_proofs()
		
	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
		stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
	endif 

	if does_blip_exist(franklins_blip)
		remove_blip(franklins_blip)
	endif
	
	//selector_cam.pedTo = player_ped_id()
	
	START_PLAYER_SWITCH(chop_cam.chopPed, PLAYER_PED_ID(), INT_TO_ENUM(SWITCH_FLAGS, 0), SWITCH_TYPE_SHORT)
	
	exile_2_switch_cam_status = switch_cam_to_franklin_from_franklin_chop

	return true
	
endfunc 
					
func bool setup_switch_cam_to_michael_from_franklin_chop()

	clear_help()

	aiming_at_franklin = true
	render_sniper_hud = true
	
	if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person

		force_sniper_hud_for_first_person_view_mode = true 

		render_sniper_hud = false //turn off normal render_sniper_hud system so it does not time out after 2000 ms 

	else 

		force_sniper_hud_for_first_person_view_mode = false

	endif

	//**********HACK for bug 404202 WILL REQUIRE CHANGE **********
	stop_dialogue_when_switching_to_michael()

	setup_all_ped_proofs()
	

	START_PLAYER_SWITCH(chop.ped, selector_ped.pedID[SELECTOR_PED_MICHAEL], SWITCH_FLAG_SKIP_OUTRO | SWITCH_FLAG_ALLOW_SNIPER_AIM_OUTRO, SWITCH_TYPE_SHORT)

	//START_PLAYER_SWITCH(chop.ped, selector_ped.pedID[SELECTOR_PED_MICHAEL], SWITCH_FLAG_SKIP_OUTRO | CONTROL_FLAG_ALLOW_SNIPER_AIM_INTRO, SWITCH_TYPE_SHORT)
	
	turn_on_sniper_hud_for_exile_2 = true

	exile_2_switch_cam_status = switch_cam_to_michael_from_franklin_chop

	return true

endfunc 


func bool setup_switch_cam_to_franklin_from_michael()
	
	clear_help()

	setup_all_ped_proofs()
	
	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	trigger_hit_out_sound_fx_when_switching_to_michael = false
	
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
		stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
	endif 

	if does_blip_exist(franklins_blip)
		remove_blip(franklins_blip)
	endif

	if IS_FIRST_PERSON_AIM_CAM_ACTIVE()
	
		IF GET_GAMEPLAY_CAM_FOV() < 20.0 //10.00
		
			IF is_mission_ped_on_screen(chop.ped)
				aiming_at_chop = true 
				
			elif is_mission_ped_on_screen(selector_ped.pedID[selector_ped_franklin])
				aiming_at_franklin = true 
			
			elif is_mission_ped_on_screen(gang_enemy[0].ped)
				aiming_at_gang_enemy_0 = true 
				
			elif is_mission_ped_on_screen(gang_enemy[1].ped)
				aiming_at_gang_enemy_1 = true 
				
			elif is_mission_ped_on_screen(gang_enemy[2].ped)
				aiming_at_gang_enemy_2 = true 
				
			elif is_mission_ped_on_screen(gang_enemy[3].ped)
				aiming_at_gang_enemy_3 = true 
			endif 
		endif 
			
		//log the sniper heading and pitch regardless if any entity is on screen.
		sniper_rifle_aim_heading = get_gameplay_cam_relative_heading()
		sniper_rifle_aim_pitch = get_gameplay_cam_relative_pitch()
		//sniper_rifle_aim_fov = get_gameplay_cam_relative_fov()

		render_sniper_hud = true 
	
	endif 
		
	//if player in sniper view activate heli cam and reposition camera rot to centre behind player.
	if does_cam_exist(heli_cam)
		if not is_cam_active(heli_cam)
			SET_CAM_ACTIVE(heli_cam, TRUE)
			SET_CAMERA_ROT_TO_SAME_AS_veh(helicopter.veh, heli_cam, <<current_cam_rot.x, 0.0, current_cam_rot.z>>)
		endif
	endif 
	
	turn_on_sniper_hud_for_exile_2 = true
	
	selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]
	
	exile_2_switch_cam_status = switch_cam_to_franklin_from_michael
	
	return true
	
	//---
	
	return false 

endfunc 

func bool setup_switch_cam_to_trevor_from_michael()

	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
		stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
	endif 
	
	if is_playback_going_on_for_vehicle(helicopter.veh)
		stop_playback_recorded_vehicle(helicopter.veh)
	endif 
	
	open_sequence_task(seq)
		task_heli_mission(null, helicopter.veh, null, null, <<-1562.2906, 4498.4482, 150.00>>, mission_goto, 5, -1, -1, 150, 70)
	close_sequence_task(seq)
	task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
	clear_sequence_task(seq)
	
	selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_trevor]
	
	turn_on_sniper_hud_for_exile_2 = true
	
	exile_2_switch_cam_status = switch_cam_to_trevor_from_michael

	return true

endfunc 

func bool setup_switch_cam_to_trevor_from_michael_chop

	clear_this_print("FS_help_1")
	
	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	setup_all_ped_proofs()
	
	if is_playback_going_on_for_vehicle(helicopter.veh)
		stop_playback_recorded_vehicle(helicopter.veh)
	endif 
	
	open_sequence_task(seq)
		task_heli_mission(null, helicopter.veh, null, null, <<-1562.2906, 4498.4482, 150.00>>, mission_goto, 5, -1, -1, 150, 70)
	close_sequence_task(seq)
	task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
	clear_sequence_task(seq)
	
	START_PLAYER_SWITCH(chop.ped, selector_ped.pedID[SELECTOR_PED_trevor], int_to_enum(switch_flags, 0), SWITCH_TYPE_SHORT)

	exile_2_switch_cam_status = switch_cam_to_trevor_from_michael_chop 

	return false

endfunc 

func bool setup_switch_cam_to_trevor_from_franklin()
	
	clear_this_print("FS_help_1")
	
	setup_all_ped_proofs()
	
	if is_playback_going_on_for_vehicle(helicopter.veh)
		stop_playback_recorded_vehicle(helicopter.veh)
	endif 
	
	open_sequence_task(seq)
		task_heli_mission(null, helicopter.veh, null, null, <<-1562.2906, 4498.4482, 150.00>>, mission_goto, 5, -1, -1, 150, 70)
	close_sequence_task(seq)
	task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
	clear_sequence_task(seq)
	
	selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_trevor]
	
	exile_2_switch_cam_status = switch_cam_to_trevor_from_franklin
	
	return true

endfunc 

func bool setup_switch_cam_to_trevor_from_franklin_chop()

	clear_this_print("FS_help_1")
	
	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	setup_all_ped_proofs()
	
	if is_playback_going_on_for_vehicle(helicopter.veh)
		stop_playback_recorded_vehicle(helicopter.veh)
	endif 
	
	open_sequence_task(seq)
		task_heli_mission(null, helicopter.veh, null, null, <<-1562.2906, 4498.4482, 150.00>>, mission_goto, 5, -1, -1, 150, 70)
	close_sequence_task(seq)
	task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
	clear_sequence_task(seq)
	
	START_PLAYER_SWITCH(chop.ped, selector_ped.pedID[SELECTOR_PED_trevor], int_to_enum(switch_flags, 0), SWITCH_TYPE_SHORT)

	exile_2_switch_cam_status = switch_cam_to_trevor_from_franklin_chop 

	return true

endfunc 


proc setup_rpg_enemy_to_fire_rpg()

	//forces rocket to fire and gets ped into good position
	//if does_blip_exist(gang_enemy[3].blip) //OLD WAY
	if ! (manage_gang_enemy_3_status >= 21)
		if does_entity_exist(gang_enemy[3].ped)
			if not is_ped_injured(gang_enemy[3].ped)
				if not does_entity_exist(rpg_obj)

					clear_ped_tasks_immediately(gang_enemy[3].ped)
					set_entity_coords(gang_enemy[3].ped, gang_enemy[3].run_to_pos)
					set_entity_heading(gang_enemy[3].ped, 196.2294)
					
					REFILL_AMMO_INSTANTLY(gang_enemy[3].ped)
					
					TASK_AIM_GUN_AT_ENTITY(gang_enemy[3].ped, helicopter.veh, 3000, true)
					
					force_ped_ai_and_animation_update(gang_enemy[3].ped)
					
					//make_ped_reload(
					
					manage_gang_enemy_3_status = 6
					
				endif 
			endif 
		endif 
	endif 
	
endproc

func bool is_gang_enemy_3_pos_on_screen()

	IF GET_GAMEPLAY_CAM_FOV() < 15.0 
							
		FLOAT f_x = 0.0
		FLOAT f_y = 0.0
		GET_SCREEN_COORD_FROM_WORLD_COORD(gang_enemy[3].pos, f_x, f_y)
							
					
		IF ABSF(f_x - 0.5) < 0.3 
								
			IF ABSF(f_y - 0.5) < 0.3
									
				return true 
									
			ENDIF
		ENDIF

	ENDIF
	
	return false 

endfunc

//only allows the player to switch to a ped index which they are not. e.g. if they are michael chop they can't 
//switch to michael
func bool process_take_control_of_selector_ped()
	
	if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_MICHAEL)
		if get_current_player_ped_enum() = char_franklin
			IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, true, true)
			
				INFORM_MISSION_STATS_OF_INCREMENT(EXL2_SWITCHES) 
				
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, EXL2_MAX_SPEED)
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, EXL2_CAR_DAMAGE)
				bVehGrabbedForStats = FALSE
				
				return true 
			endif 
		endif 
	endif 
	
	if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_FRANKLIN)
		if get_current_player_ped_enum() = char_michael
			IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, true, true)
			
				INFORM_MISSION_STATS_OF_INCREMENT(EXL2_SWITCHES) 
				
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, EXL2_MAX_SPEED)
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, EXL2_CAR_DAMAGE)
				bVehGrabbedForStats = FALSE
				
				return true 
			endif 
		endif 
	endif 
	
	//player can never be trevor chop
	if mission_flow = stage_switch_to_trevor
		if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_TREVOR) 
			IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, true, true)
			
				INFORM_MISSION_STATS_OF_INCREMENT(EXL2_SWITCHES) 
			
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, EXL2_MAX_SPEED)
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, EXL2_CAR_DAMAGE)
				bVehGrabbedForStats = FALSE
			
				return true 
			endif 
		endif 
	endif 
	
//	INFORM_MISSION_STATS_OF_INCREMENT(EXL2_SWITCHES) 

	//return true 
	return false

endfunc 

func bool snipe_enemies_master_flow_system()

	if ped_structure_are_all_enemies_dead(gang_enemy, false)
	or ped_structure_are_specific_number_enemies_dead(gang_enemy, 3)
	
		UPDATE_AI_PED_BLIP(gang_enemy[3].ped, gang_enemy[3].blip_2, -1, null, true) //process for final rpg enemy
		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(EXL2_DEATH_FROM_ABOVE)
		
//		INFORM_MISSION_STATS_OF_INCREMENT(EXL2_KILLS) 

		if does_entity_exist(rpg_obj)
			rpg_system()
		endif 
		
		if not is_player_switch_in_progress()
		
//			if is_vehicle_driveable(ambient_car.veh)
//				set_vehicle_doors_locked(ambient_car.veh, vehiclelock_unlocked)
//			endif 

			allow_switch = false

			if not is_entity_dead(ambient_car.veh)
				if is_playback_going_on_for_vehicle(ambient_car.veh)
					stop_playback_recorded_vehicle(ambient_car.veh)
				endif 
			endif

			if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_MICHAEL")
				STOP_AUDIO_SCENE("EXILE_2_KILL_ENEMY_MICHAEL")
			endif 

			if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_FRANKLIN")
				STOP_AUDIO_SCENE("EXILE_2_KILL_ENEMY_FRANKLIN")
			endif 
			
			//probably not needed as enemy[3] will be proofed when chop so can never die.
			if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_CHOP")
				STOP_AUDIO_SCENE("EXILE_2_KILL_ENEMY_CHOP")
			endif 
		
			trigger_music_event("EXL2_RPG_DEAD")
			
			if get_current_player_ped_enum() = char_franklin
				SET_PED_USING_ACTION_MODE(player_ped_id(), false)
			else 
				SET_PED_USING_ACTION_MODE(selector_ped.pedID[selector_ped_franklin], false)
			endif 
			
			//only stop helicopter recording if the player is franklin or micahel chop
			if get_current_player_ped_enum() = char_franklin
			or ((get_current_player_ped_enum() = char_michael) and (chop_cam.state = CHOP_VIEW_CAM_ON_CHOP))
				
				if is_playback_going_on_for_vehicle(helicopter.veh)
					stop_playback_recorded_vehicle(helicopter.veh)
				endif 
				
				open_sequence_task(seq)
					task_heli_mission(null, helicopter.veh, null, null, <<-1562.2906, 4498.4482, 150.00>>, mission_goto, 5, -1, -1, 150, 70)
				close_sequence_task(seq)
				task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
				clear_sequence_task(seq)
				
			endif 
			//--
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0)
		
			return true 
		endif 
		
	endif 
	
	switch snipe_enemies_master_flow_system_status 
	
		case 0
		
			if ped_structure_are_specific_number_enemies_dead(gang_enemy, 1)
			
//				INFORM_MISSION_STATS_OF_INCREMENT(EXL2_KILLS) 
		
//			if has_label_been_triggered("chop_help1") 
//			or chop_ai_system_status >= 102 //chop been targeted and running
//				allow_switch = true 
//				snipe_enemies_master_flow_system_status++
//			endif 
				
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0)

				snipe_enemies_master_flow_system_status++
				
			endif 
		
		break 
	
		case 1
		
			if ped_structure_are_specific_number_enemies_dead(gang_enemy, 2)
			
//				INFORM_MISSION_STATS_OF_INCREMENT(EXL2_KILLS) 
			
				//removed the peds even though ped_structure_are_all_enemies_dead is set to false
				set_ped_as_no_longer_needed(gang_enemy[0].ped)
				set_ped_as_no_longer_needed(gang_enemy[1].ped)
				
				if is_gang_enemy_3_pos_on_screen() //create gang member at different pos if onscreen
					gang_enemy[3].pos = <<-1603.7502, 4513.5586, 15.1901>>
					gang_enemy[3].heading = 227.6631 
					rpg_target_pos_0 = <<-1595.696533,4510.316895,20.188784>>
				endif 
				
				setup_enemy(gang_enemy[3])
				set_entity_proofs(gang_enemy[3].ped, true, true, true, true, true)
				SET_PED_CAN_BE_DRAGGED_OUT(gang_enemy[3].ped, false)
				SET_PED_HEATSCALE_OVERRIDE(gang_enemy[3].ped, gang_enemy_3_heatscale_value)
				remove_blip(gang_enemy[3].blip)
				SET_ENTITY_LOAD_COLLISION_FLAG(gang_enemy[3].ped, true)

				if get_current_player_ped_enum() = char_michael
					set_entity_proofs(gang_enemy[3].ped, true, true, true, true, true)
				endif 
				
				add_ped_for_dialogue(scripted_speech, 6, gang_enemy[3].ped, "oneil") 
				
				snipe_enemies_master_flow_system_status++
				
			endif 

		break 
		
		case 2
	
			if activate_helicopter_flying_system //rpg CUTSCENE finsihed
				
//				if get_current_player_ped_enum() = char_michael 
//				and not does_cam_exist(selector_cam.camID)
				
					

//					IF HAS_LABEL_BEEN_TRIGGERED("TM44_CANGO")
//						if not is_any_text_being_displayed(locates_data)

							SET_PED_HEATSCALE_OVERRIDE(gang_enemy[3].ped, 1)
							
							set_entity_proofs(gang_enemy[3].ped, false, false, false, false, false)
							
							clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_franklin])
							SET_ENTITY_COORDS(selector_ped.pedID[selector_ped_franklin], <<-1683.4967, 4454.5952, 1.5660>>)
							SET_ENTITY_HEADING(selector_ped.pedID[selector_ped_franklin], 239.1512)
							//play loop let off synched scene on franklin

							clear_ped_tasks_immediately(chop.ped)
							set_entity_coords(chop.ped, <<-1677.73071, 4454.21191, 1.2653>>)//<<-1679.8206, 4455.9878, 1.2653>>)
							set_entity_heading(chop.ped, 257.7928)//114.6932)

							chop_ai_system_status = 200 //set chop ai to hunt for the rpg enemy - put chop into idle let off loop
							
							trigger_music_event("EXL2_ON_FOOT")
							
							if IS_AUDIO_SCENE_ACTIVE("EXILE_2_RPG_CUTSCENE")
								STOP_AUDIO_SCENE("EXILE_2_RPG_CUTSCENE")
							endif 

//							make_selector_ped_selection(selector_ped, SELECTOR_PED_franklin)
//							eSwitchCamState = SWITCH_CAM_START_SPLINE1
							
							setup_rpg_enemy_to_fire_rpg()
							
							HANDLE_SWITCH_CAM_SCOPE_TO_FRANKLIN(scsSwitchCamScopeToFranklin)
							
							REPLAY_START_EVENT()
				
							snipe_enemies_master_flow_system_status++
//							
//						endif 
//					endif 
					
//				else 
//				
//					if is_this_print_being_displayed("FS_help_0")
//						clear_this_print("FS_help_0")
//					endif 
//					
//					snipe_enemies_master_flow_system_status++
//				
//				endif 
				
			endif 

		break 
		
		case 3
		
			IF NOT HANDLE_SWITCH_CAM_SCOPE_TO_FRANKLIN(scsSwitchCamScopeToFranklin)
			
				IF selector_cam.bOKToSwitchPed
					
					IF NOT selector_cam.bPedSwitched
						
						if process_take_control_of_selector_ped()

							switch get_current_player_ped_enum()

								case char_franklin
	
									clear_focus()
									
									set_ped_config_flag(selector_ped.pedID[selector_ped_michael], PCF_DisableExplosionReactions, true)

									ATTACH_ENTITY_TO_ENTITY(selector_ped.pedID[selector_ped_michael], helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
									SET_PED_MOVEMENT_CLIPSET(selector_ped.pedID[selector_ped_michael], "move_ped_crouched")
									SET_PED_WEAPON_MOVEMENT_CLIPSET(selector_ped.pedID[selector_ped_michael], "Wpn_AssaultRifle_WeaponHoldingCrouched")
									SET_PED_STRAFE_CLIPSET(selector_ped.pedID[selector_ped_michael], "move_ped_crouched_strafing")

									SET_ped_DESIRED_HEADING(selector_ped.pedID[selector_ped_michael], get_entity_heading(player_ped_id()))
									
									SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), true)
									
									SET_PED_USING_ACTION_MODE(player_ped_id(), true)
									
									//if the player has been in combat whilst the player is chop
									REMOVE_PED_DEFENSIVE_AREA(player_ped_id(), true)
									
									franklin_ai_system_status = 100
									
								break 
								
							endswitch 
	
							selector_cam.bPedSwitched = TRUE

						endif 
					
					endif 
					
				endif 
			
			else 
			
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(), EXL2_DAMAGE)
			
				switch get_current_player_ped_enum()
					
					case char_franklin

						DISABLE_CELLPHONE(false)

						task_aim_gun_at_coord(selector_ped.pedID[selector_ped_michael], <<-1531.6, 4376.7, 68.00>>, -1)

						//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_michael, true)

						helicopter_switch_system_status = 0
						
						IF chop_cam.state = CHOP_VIEW_CAM_to_CHOP
						
							set_player_control(player_id(), false, spc_allow_player_damage)
							SET_CAM_CONTROLS_MINI_MAP_HEADING(chop_cam.chopCam, true)
							//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)
							SET_CHOP_VIEW_CAM_ON_CHOP(chop_cam, Selector_Ped, TRUE)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(EXL2_TIME_AS_CHOP)
						
						else 
											
							if does_entity_exist(chop.ped)
								//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor, true) //chop
							endif 
							
							set_ped_dies_in_water(player_ped_id(), true)

							REMOVE_PED_DEFENSIVE_AREA(player_ped_id(), true)
							clear_ped_tasks(player_ped_id())
							set_chop_view_cam_disabled(chop_cam, Selector_Ped)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED() 
						endif 
						
					break 
					
				endswitch 
				
				//initialise switch mission variables back to false. 
				selector_cam.bOKToSwitchPed = false
				selector_cam.bPedSwitched = false
				
				deactivate_all_ped_proofs()
				
				REPLAY_STOP_EVENT()

				snipe_enemies_master_flow_system_status++
			
			endif 	
		
		break 
		
		case 4
		
			if (get_player_ped_enum(player_ped_id()) = char_franklin 
			and not selector_cam.bSplineActive)
			
				remove_anim_dict(strAnimDict)
			
				allow_switch = true
			
				//play let off sycnhed scene on franklin
				//allow_switch = false //don't allow switch till after synched scene is finished

				franklin_ai_system_status = 100
				//chop_ai_system_status = 200 
			
				//car only damamged when the player is Franklin on the ground.
				if is_vehicle_driveable(ambient_car.veh)
					set_entity_proofs(ambient_car.veh, false, false, false, false, false)
				endif 
			
				if not does_blip_exist(chop.blip)
					chop.blip = create_blip_for_ped(chop.ped)
				endif 
				
				SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, selector_ped_franklin, selector_ped_franklin)
				
				start_audio_scene("EXILE_2_KILL_ENEMY_FRANKLIN")
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "shooting the rpg enemy")
			
				snipe_enemies_master_flow_system_status++
				
			endif 
		
		break 
		
		case 5
		
			//******
//			do cutsom switch here. 
//			
//			them allow_switch = true to use the nommal switch system from then on
			
		
//			if is_synchronized_scene_running(cutscene_index)
//				if get_synchronized_scene_phase(cutscene_index) >= 1.0
//					clear_ped_tasks(player_ped_id())
//					allow_switch = true
//					snipe_enemies_master_flow_system_status++
//				endif 
//			else 
//				allow_switch = true //fail safe.
//				snipe_enemies_master_flow_system_status++
//			endif 
			
			//check if there is any instances where playing a synched scene with do not interput could abort and 
			//cause is_synchronized_scene_running to fail and thus breaking the switch 

		break 
		
		
//		case 1
//			
//				KILL_ANY_CONVERSATION()
//				
//				snipe_enemies_master_flow_system_status++
//		
//			endif 
//			
//		break 
//		
//		case 2
//
//			IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_DEAD", CONV_PRIORITY_medium)
//				snipe_enemies_master_flow_system_status++
//			ENDIF
//
//		break 
//		
//		case 3
//		
//			IF not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			
//				set_ped_keep_task(selector_ped.pedID[selector_ped_trevor], true)
//				task_heli_mission(selector_ped.pedID[selector_ped_trevor], helicopter.veh, null, null, <<1800, 3772.22, 150.22>>, mission_goto, 20, -1, -1, 150, 70)
//					
//				make_selector_ped_selection(selector_ped, SELECTOR_PED_FRANKLIN)
//				TAKE_CONTROL_OF_SELECTOR_PED(selector_ped)
//					
//				return true
//		
//			ENDIF 
//
//		break 

	endswitch 

	return false 

endfunc
						

func bool is_the_entity_on_screen(entity_index mission_entity)

	if does_entity_exist(mission_entity)
		if is_entity_on_screen(mission_entity)
			return true 
		endif 
	endif 
	
	return false 

endfunc 

func bool setup_switch_cam_to_chop_from_michael()
		
	clear_help()
	
	setup_all_ped_proofs()
	
	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
		stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
	endif 

	if does_blip_exist(franklins_blip)
		remove_blip(franklins_blip)
	endif
	
	if IS_FIRST_PERSON_AIM_CAM_ACTIVE()
	
		IF GET_GAMEPLAY_CAM_FOV() < 20.0 //10.00
			IF is_the_entity_on_screen(chop.ped)
				aiming_at_chop = true 
			
			elif is_the_entity_on_screen(gang_enemy[0].ped)
				aiming_at_gang_enemy_0 = true 
				
			elif is_the_entity_on_screen(gang_enemy[1].ped)
				aiming_at_gang_enemy_1 = true 
				
			elif is_the_entity_on_screen(gang_enemy[2].ped)
				aiming_at_gang_enemy_2 = true 
				
			elif is_the_entity_on_screen(gang_enemy[3].ped)
				aiming_at_gang_enemy_3 = true 
			endif 
		endif 
			
		sniper_rifle_aim_heading = get_gameplay_cam_relative_heading()
		sniper_rifle_aim_pitch = get_gameplay_cam_relative_pitch()
		//sniper_rifle_aim_fov = get_gameplay_cam_relative_fov()

		render_sniper_hud = true 
		
	endif 
	
	//START_PLAYER_SWITCH(PLAYER_PED_ID(), chop_cam.chopPed, INT_TO_ENUM(SWITCH_FLAGS, 0), SWITCH_TYPE_SHORT)
	START_PLAYER_SWITCH(PLAYER_PED_ID(), chop_cam.chopPed, SWITCH_FLAG_ALLOW_SNIPER_AIM_INTRO, SWITCH_TYPE_SHORT)
	
	turn_on_sniper_hud_for_exile_2 = true
	
	chop_cam.state = CHOP_VIEW_CAM_to_CHOP
	
	exile_2_switch_cam_status = switch_cam_to_chop_from_michael

	//--

	return true


endfunc 

func bool setup_switch_cam_to_franklin_from_michael_chop()

//	float distance_between_peds
//	int max_distance_limit = 40
//	int minimum_distance_limit = 2
//	int max_interp_limit = 3000
//	int min_interp_limit = 1500
//	int interp_time
//	
//	IF NOT selector_cam.bSplineCreated
//	
//		setup_all_ped_proofs()
//		
//		b_thermal_active = false
//		SET_SEETHROUGH(FALSE)
//		
//		if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
//			stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
//		endif 
//
//		if does_blip_exist(franklins_blip)
//			remove_blip(franklins_blip)
//		endif
//		
//		distance_between_peds = get_distance_between_coords(GET_ENTITY_COORDS(selector_ped.pedID[selector_ped_franklin]), GET_ENTITY_COORDS(chop.ped))
//		
//		if distance_between_peds > minimum_distance_limit
//			
//			printfloat(distance_between_peds)
//			printnl()
//			
//			interp_time = round((((distance_between_peds - minimum_distance_limit) / (max_distance_limit - minimum_distance_limit)) * (max_interp_limit - min_interp_limit)) + min_interp_limit)
//		
//		else 
//			interp_time = min_interp_limit
//		endif 
//		
//		
//		IF NOT DOES_CAM_EXIST(selector_cam.camID)
//			selector_cam.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
//		ENDIF
//
//		if does_cam_exist(node_1_cam)
//			destroy_cam(node_1_cam)
//		endif 
//		
//		if does_cam_exist(node_2_cam)
//			destroy_cam(node_2_cam)
//		endif 
//		
//		node_1_cam = create_cam("default_scripted_camera", true)
//		set_cam_params(node_1_cam, get_offset_from_entity_in_world_coords(selector_ped.pedID[SELECTOR_PED_FRANKLIN], <<0.263986, -2.75, 0.8>>), GET_ENTITY_ROTATION(selector_ped.pedID[SELECTOR_PED_FRANKLIN]), 45.00) 		
//		
//		ADD_CAM_SPLINE_NODE_USING_CAMERA(selector_cam.camID, chop_cam.chopCam, 0)
//		ADD_CAM_SPLINE_NODE_USING_CAMERA(selector_cam.camID, node_1_cam, interp_time)	
//		
//		selector_cam.bSplineCreated = TRUE
//		selector_cam.camType = SELECTOR_CAM_STRAIGHT_INTERP
		
//		selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]

//		set_cam_active(selector_cam.camID, true)
		
//		clear_help()


	//--
	
	clear_help()

	setup_all_ped_proofs()
	
	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
		stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
	endif 

	if does_blip_exist(franklins_blip)
		remove_blip(franklins_blip)
	endif
	
	//selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]

	START_PLAYER_SWITCH(chop.ped, selector_ped.pedID[SELECTOR_PED_franklin], int_to_enum(SWITCH_FLAGS, 0), SWITCH_TYPE_SHORT)
	
	exile_2_switch_cam_status = switch_cam_to_franklin_from_michael_chop

//--

	return true

endfunc

func bool setup_switch_cam_to_michael_from_michael_chop()
	
	clear_help()
	
	render_sniper_hud = true  //forces michael to aim at franklin set in RUN_CAM_SPLINE_FROM_PLAYER_TO_PED
	aiming_at_franklin = true //calculates relative heading and pitch to franklin from player.
	//b_thermal_active = true
	
	if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person

		force_sniper_hud_for_first_person_view_mode = true 

		render_sniper_hud = false //turn off normal render_sniper_hud system so it does not time out after 2000 ms 
	
	else 
		
		force_sniper_hud_for_first_person_view_mode = false
	
	endif 
	
	sniper_hud_time = get_game_timer() //set timer here as helicopter camera system will get hit before the run exile 2 code because the conditions will be met for a frame. This will set render_sniper_hud  to false

	//**********HACK for bug 404202 WILL REQUIRE CHANGE **********
	stop_dialogue_when_switching_to_michael()

	setup_all_ped_proofs()
	
	b_thermal_active = false
	SET_SEETHROUGH(FALSE)
	
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
		stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
	endif 

	if does_blip_exist(franklins_blip)
		remove_blip(franklins_blip)
	endif

	//selector_cam.pedTo = player_ped_id()
	
	START_PLAYER_SWITCH(chop_cam.chopPed, PLAYER_PED_ID(), SWITCH_FLAG_SKIP_OUTRO | SWITCH_FLAG_ALLOW_SNIPER_AIM_OUTRO, SWITCH_TYPE_SHORT)
	
	turn_on_sniper_hud_for_exile_2 = true
	
	exile_2_switch_cam_status = switch_cam_to_michael_from_michael_chop

	return true

endfunc


proc put_michael_into_helicopter_when_switching_to_trevor()
	
	if mission_flow = stage_switch_to_trevor
		
		if is_player_switch_in_progress()

			if (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)

				if GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_OUTRO

				
					if get_current_player_ped_enum() = char_michael
							
						if not is_ped_sitting_in_vehicle(player_ped_id(), helicopter.veh)
						
							if is_entity_attached(player_ped_id())
								detach_entity(player_ped_id())
							endif 
	
							set_ped_into_vehicle(player_ped_id(), helicopter.veh, vs_front_right)
							SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_PILOT_HEADSET, false)
							force_ped_ai_and_animation_update(player_ped_id())
							
						endif 

						RESET_PED_WEAPON_MOVEMENT_CLIPSET(player_ped_id())
			    		RESET_PED_MOVEMENT_CLIPSET(player_ped_id())
						RESET_PED_STRAFE_CLIPSET(player_ped_id())
								
					else 
				
						if not is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], helicopter.veh)
						
							if is_entity_attached(selector_ped.pedID[selector_ped_michael])
								detach_entity(selector_ped.pedID[selector_ped_michael])
							endif 
								
							set_ped_into_vehicle(selector_ped.pedID[selector_ped_michael], helicopter.veh, vs_front_right)
							SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_PILOT_HEADSET, false)
							force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])
							
							printstring("exile2 PROPS_P0_PILOT_HEADSET")
							printnl()
							
							RESET_PED_WEAPON_MOVEMENT_CLIPSET(selector_ped.pedID[selector_ped_michael])
				    		RESET_PED_MOVEMENT_CLIPSET(selector_ped.pedID[selector_ped_michael])
							RESET_PED_STRAFE_CLIPSET(selector_ped.pedID[selector_ped_michael])
							
						endif
						
						if is_playback_going_on_for_vehicle(helicopter.veh)
							stop_playback_recorded_vehicle(helicopter.veh)
						endif 
						
						if IS_VEHICLE_EXTRA_TURNED_ON(helicopter.veh, 1)
							set_vehicle_extra(helicopter.veh, 12, false) 
						endif 
						
						//set_entity_coords(helicopter.veh, get_entity_coords(helicopter.veh))
//						set_entity_coords(helicopter.veh, <<-1600.00, 4341.2, 80.00>>)
//						set_entity_heading(helicopter.veh, 344.9161)
//						ACTIVATE_PHYSICS(helicopter.veh)
						
						//SET_ENTITY_velocity(
						
//						force_entity_ai_and_animation_update(helicopter.veh)
						
//						open_sequence_task(seq)
//							task_heli_mission(null, helicopter.veh, null, null, <<-1562.2906, 4498.4482, 150.00>>, mission_goto, 5, -1, -1, 150, 70)
//						close_sequence_task(seq)
//						task_perform_sequence(player_ped_id(), seq)
//						clear_sequence_task(seq)
						
					endif 
					
				endif 

			endif 

		endif 
		
	endif 
	
endproc 

proc setup_audio_scene_for_switiching_to_chop()
							
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_FRANKLIN")
		stop_AUDIO_SCENE("EXILE_2_KILL_ENEMY_FRANKLIN")
	endif 
	
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_MICHAEL")
		stop_AUDIO_SCENE("EXILE_2_KILL_ENEMY_MICHAEL")
	endif 

	if not IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_CHOP")
		start_AUDIO_SCENE("EXILE_2_KILL_ENEMY_CHOP")
	endif
	
endproc 

proc setup_audio_scene_for_switiching_to_michael()
		
	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_FRANKLIN")
		stop_AUDIO_SCENE("EXILE_2_KILL_ENEMY_FRANKLIN")
	endif 

	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_CHOP")
		stop_AUDIO_SCENE("EXILE_2_KILL_ENEMY_CHOP")
	endif 
	
	if not IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_MICHAEL")
		START_AUDIO_SCENE("EXILE_2_KILL_ENEMY_MICHAEL")
	endif 
	
endproc 

proc setup_audio_scene_for_switiching_to_franklin()

	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_MICHAEL")
		STOP_AUDIO_SCENE("EXILE_2_KILL_ENEMY_MICHAEL")
	endif 

	if IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_CHOP")
		stop_AUDIO_SCENE("EXILE_2_KILL_ENEMY_CHOP")
	endif 
	
	if NOT IS_AUDIO_SCENE_ACTIVE("EXILE_2_KILL_ENEMY_FRANKLIN")
		START_AUDIO_SCENE("EXILE_2_KILL_ENEMY_FRANKLIN")
	endif 
	
endproc 


func bool run_exile_2_switch_cam()

	switch exile_2_switch_cam_status
	
		case switch_cam_to_michael_from_franklin
		
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, (enum_to_int(SWITCH_FLAG_SKIP_OUTRO | CONTROL_FLAG_ALLOW_SNIPER_AIM_INTRO)), 999, 999)				
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, (enum_to_int(SWITCH_FLAG_SKIP_OUTRO | SWITCH_FLAG_ALLOW_SNIPER_AIM_OUTRO)), 999, 999)
			
				return true
			endif 
		
		break 
		
		case switch_cam_to_michael_from_michael_chop
		
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, (enum_to_int(SWITCH_FLAG_SKIP_OUTRO | CONTROL_FLAG_ALLOW_SNIPER_AIM_INTRO)), 999, 999)	
//			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, (enum_to_int(SWITCH_FLAG_SKIP_OUTRO)), 999, 999)				
//				return true 
//			endif 
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
				return true
			endif 
		
		break 
		
		case switch_cam_to_michael_from_franklin_chop
		
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, (enum_to_int(SWITCH_FLAG_SKIP_OUTRO | CONTROL_FLAG_ALLOW_SNIPER_AIM_INTRO)), 999, 999)	
//			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, (enum_to_int(SWITCH_FLAG_SKIP_OUTRO)), 999, 999)				
//				return true 
//			endif 
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
				
				if (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
				
					if GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_OUTRO
						
						selector_cam.bOKToSwitchPed = true
						
					endif 
				endif 
				
				return true
			endif 
		
		break 
		
		case switch_cam_to_franklin_from_michael
		
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, enum_to_int(SWITCH_FLAG_ALLOW_SNIPER_AIM_INTRO), 999, 999)
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, enum_to_int(SWITCH_FLAG_ALLOW_SNIPER_AIM_INTRO), 999, 999)
				return true 
			endif 
		
		break 
		
		case switch_cam_to_franklin_from_michael_chop
		
//			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, 0, 999, 999)
//				return true 
//			endif 

			IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
				
				if (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
				
					if GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_OUTRO
						
						selector_cam.bOKToSwitchPed = true
						
					endif 
				endif 
				
				return true
			endif 
		
		break 
		
		case switch_cam_to_franklin_from_franklin_chop
//		
//			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, 0, 999, 999)
//				return true 
//			endif 

			IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
				return true
			endif 
		
		break 
		
		case switch_cam_to_chop_from_michael
		
			////if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, enum_to_int(SWITCH_FLAG_ALLOW_SNIPER_AIM_INTRO), 999, 999)
//			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, 0, 999, 999)
//				return true 
//			endif 

			IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
				return true
			endif 
		
		break 
		
		case switch_cam_to_chop_from_franklin
		
//			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, 0, 999, 999)
//				return true 
//			endif 
		
			IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
				return true
			endif 

		break 
		
		case switch_cam_to_trevor_from_franklin
		
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, 0, 999, 999)
				return true 
			endif 
		
		break 
		
		case switch_cam_to_trevor_from_franklin_chop 
		
			IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
				
				if (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
				
					if GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_OUTRO
						
						selector_cam.bOKToSwitchPed = true
						
					endif 
				endif 
				
				return true
			endif 
		
		break 
		
		case switch_cam_to_trevor_from_michael
		
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, enum_to_int(SWITCH_FLAG_ALLOW_SNIPER_AIM_INTRO), 999, 999)
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_short, enum_to_int(SWITCH_FLAG_ALLOW_SNIPER_AIM_INTRO), 999, 999)
				return true 
			endif 
		
		break 
		
		case switch_cam_to_trevor_from_michael_chop
		
			IF IS_PLAYER_SWITCH_IN_PROGRESS()                                                      
				
				if (GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT)
				
					if GET_PLAYER_SHORT_SWITCH_STATE() = SHORT_SWITCH_STATE_OUTRO
						
						selector_cam.bOKToSwitchPed = true
						
					endif 
				endif 
				
				return true
			endif 
		
		break 
		
	endswitch 
	
	return false 
	
endfunc 

proc helicopter_switch_system()

//	printstring("helicopter_switch_system_status: ")
//	printint(helicopter_switch_system_status)
//	printnl()

	HANDLE_CHOP_CAM_AUDIO(chop_cam)
		
	switch helicopter_switch_system_status 

		case 0
		
			if allow_switch
				if update_selector_hud(selector_ped, chop_cam.state <> CHOP_VIEW_CAM_ON_CHOP)

					switch get_current_player_ped_enum()
					
						case char_michael
						
							if mission_flow = stage_snipe_enemies
							
								if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_FRaNKLIN)

									IF chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
										if setup_switch_cam_to_franklin_from_michael()
										
											setup_audio_scene_for_switiching_to_franklin()
										
											setup_rpg_enemy_to_fire_rpg()

											helicopter_switch_system_status++
										endif 
									
									ELSE
									
										if setup_switch_cam_to_franklin_from_michael_chop()
										
											setup_audio_scene_for_switiching_to_franklin()
											
											SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
											RENDER_SCRIPT_CAMS(FALSE, FALSE)
											
											helicopter_switch_system_status++
										
										endif 
									
									ENDIF
									
								endif 
								
								
								IF IS_CHOP_SELECTED(chop_cam, Selector_Ped)
									// set chop cam state to chop. Doesn't do any cam stuff, allows mission to take care of it.
									
									if setup_switch_cam_to_chop_from_michael()
									
										setup_audio_scene_for_switiching_to_chop()
									
										setup_rpg_enemy_to_fire_rpg()
								
										helicopter_switch_system_status++
										
									endif 
								ENDIF
								
								if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_MICHAEL)
								
									IF setup_switch_cam_to_michael_from_michael_chop()
									
										setup_audio_scene_for_switiching_to_michael()
	
										SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
										RENDER_SCRIPT_CAMS(FALSE, FALSE)

										helicopter_switch_system_status++
									endif 
								endif 
								
							
							elif mission_flow = stage_switch_to_trevor 
							
								if has_selector_ped_been_selected(selector_ped, selector_ped_trevor)
								
									IF chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
										setup_switch_cam_to_trevor_from_michael()
									else 
										
										setup_switch_cam_to_trevor_from_michael_chop()
										
										SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
										RENDER_SCRIPT_CAMS(FALSE, FALSE)
									endif  

									closest_warp_pos_to_franklin = get_closest_pos_to_player(pickup_pos)
									
								endif 
							
							endif 
							
						break 
						
						case char_franklin
							
							if mission_flow = stage_snipe_enemies
							
								if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_MICHAEL)
									
									IF chop_cam.state <> CHOP_VIEW_CAM_ON_CHOP
										if setup_switch_cam_to_michael_from_franklin()
										
											setup_audio_scene_for_switiching_to_michael()
											
											//ensures mission won't complete by gang_enemy dying during switching 
	//										if not is_ped_injured(gang_enemy[3].ped)
	//											set_entity_proofs(gang_enemy[3].ped, true, true, true, true, true)
	//										endif 
											
											helicopter_switch_system_status++
										endif
										
									else
							
										if setup_switch_cam_to_michael_from_franklin_chop()
										
											setup_audio_scene_for_switiching_to_michael()
										
											//turn black and white off and tells the chop view cam we are going to interpoalte to the player
											SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
											RENDER_SCRIPT_CAMS(FALSE, FALSE)

											helicopter_switch_system_status++
										endif 
									
										//switching to michael from franklin in chop mode
										// setup custom switch to michael from chop incorporating take control
										// uncomment below when ready to add cam
										//SET_CHOP_VIEW_CAM_TO_PLAYER(chop_cam, Selector_Ped)
									
									endif 
									
								endif 
								
								IF IS_CHOP_SELECTED(chop_cam, Selector_Ped)
									// set chop cam state to chop. Doesn't do any cam stuff, allows mission to take care of it.
								
									if setup_switch_cam_to_chop_from_franklin()
									
										setup_audio_scene_for_switiching_to_chop()
									
										//SET_CHOP_VIEW_CAM_TO_CHOP(chop_cam, false, true, FALSE) //TRUE, TRUE, FALSE
										
										helicopter_switch_system_status++
										
									endif 
								ENDIF
								
								if has_selector_ped_been_selected(selector_ped, selector_ped_franklin)
									if setup_switch_cam_to_franklin_from_franklin_chop()
										
										setup_audio_scene_for_switiching_to_franklin()
										
										SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
										RENDER_SCRIPT_CAMS(FALSE, FALSE)
										
										helicopter_switch_system_status++
									endif 
								endif 
								
							
							elif mission_flow = stage_switch_to_trevor
							
								if has_selector_ped_been_selected(selector_ped, selector_ped_trevor)
								
									IF chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
									
										setup_switch_cam_to_trevor_from_franklin()
										
										helicopter_switch_system_status++
										
									else 
									
										setup_switch_cam_to_trevor_from_franklin_chop()
										
										//turn black and white off and tells the chop view cam we are going to interpoalte to the player
										SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
										RENDER_SCRIPT_CAMS(FALSE, FALSE) //disables the script cam and allows the code chop cam to run
										
										helicopter_switch_system_status++
									endif 
									
									closest_warp_pos_to_franklin = get_closest_pos_to_player(pickup_pos)

								endif 
		
							endif 
							
						break 
						
					endswitch 
					
				endif 
				
			endif 
			
		break 

		case 1
		
			if run_exile_2_switch_cam()
			
				IF selector_cam.bOKToSwitchPed
					IF NOT selector_cam.bPedSwitched
						
						if process_take_control_of_selector_ped()

//							set_ped_can_ragdoll(player_ped_id(), true)
//							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)

							switch get_current_player_ped_enum()
							
								case char_michael
								
//									SET_CAM_active(heli_cam, true)
//									render_script_cams(true, false)

									SET_PED_USING_ACTION_MODE(player_ped_id(), false)
									ATTACH_ENTITY_TO_ENTITY(player_ped_id(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
									SET_PED_MOVEMENT_CLIPSET(player_ped_id(), "move_ped_crouched")
									SET_PED_WEAPON_MOVEMENT_CLIPSET(player_ped_id(), "Wpn_AssaultRifle_WeaponHoldingCrouched")
									SET_PED_STRAFE_CLIPSET(player_ped_id(), "move_ped_crouched_strafing")

									SET_ped_DESIRED_HEADING(player_ped_id(), get_entity_heading(player_ped_id()))
									//force_ped_motion_state(player_ped_id(), ms_crouch_idle, true)
									SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), false)
									set_current_ped_weapon(player_ped_id(), WEAPONTYPE_HEAVYSNIPER, true)
									//TASK_FORCE_MOTION_STATE(null, enum_to_int(MS_diving_swim))
									if render_sniper_hud
									or force_sniper_hud_for_first_person_view_mode
										SET_PLAYER_FORCED_AIM(player_id(), true)
										//script_assert("SET_PLAYER_FORCED_AIM")
									else 
										//task_aim_gun_at_coord(player_ped_id(), <<-1531.6, 4376.7, 68.00>>, 4000, true)
									endif 
		
									set_ped_dies_when_injured(selector_ped.pedID[selector_ped_franklin], false)
									set_ped_can_be_targetted(selector_ped.pedID[selector_ped_franklin], false)	
									set_ped_suffers_critical_hits(selector_ped.pedID[selector_ped_franklin], false)
									set_ped_can_evasive_dive(selector_ped.pedID[selector_ped_franklin], false)
									set_entity_is_target_priority(selector_ped.pedID[selector_ped_franklin], false)
									set_ped_can_ragdoll(selector_ped.pedID[selector_ped_franklin], false)
									set_ped_dies_in_water(selector_ped.pedID[selector_ped_franklin], false)
									
									set_ped_seeing_range(selector_ped.pedID[selector_ped_franklin], 250.00)
									set_ped_hearing_range(selector_ped.pedID[selector_ped_franklin], 250.00)
									set_ped_id_range(selector_ped.pedID[selector_ped_franklin], 250.00)

									setup_relationship_contact(selector_ped.pedID[selector_ped_franklin], true)
									
									EQUIP_BEST_PLAYER_WEAPON(selector_ped.pedID[selector_ped_franklin], true)
									
									set_label_as_triggered("TM44_MLOOK", false)
							
								break 
								
								case char_franklin
								
									clear_focus()
									
									set_ped_config_flag(selector_ped.pedID[selector_ped_michael], PCF_DisableExplosionReactions, true)
					
									ATTACH_ENTITY_TO_ENTITY(selector_ped.pedID[selector_ped_michael], helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
									SET_PED_MOVEMENT_CLIPSET(selector_ped.pedID[selector_ped_michael], "move_ped_crouched")
									SET_PED_WEAPON_MOVEMENT_CLIPSET(selector_ped.pedID[selector_ped_michael], "Wpn_AssaultRifle_WeaponHoldingCrouched")
									SET_PED_STRAFE_CLIPSET(selector_ped.pedID[selector_ped_michael], "move_ped_crouched_strafing")

									//SET_PED_DUCKING(selector_ped.pedID[selector_ped_michael], TRUE)
									//TASK_TOGGLE_DUCK(selector_ped.pedID[selector_ped_michael], TOGGLE_DUCK_ON)
									SET_ped_DESIRED_HEADING(selector_ped.pedID[selector_ped_michael], get_entity_heading(player_ped_id()))
									//force_ped_motion_state(selector_ped.pedID[selector_ped_michael], ms_crouch_idle, true)
						
									SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), true)
									
									SET_PED_USING_ACTION_MODE(player_ped_id(), true)
									
									//if the player has been in combat whilst the player is chop
									REMOVE_PED_DEFENSIVE_AREA(player_ped_id(), true)
									//REMOVE_PED_DEFENSIVE_AREA(selector_ped.pedID[selector_ped_franklin], true)
									//clear_ped_tasks(player_ped_id())
	
									franklin_ai_system_status = 100
									
									//michael_dialogue_time = get_game_timer() //resets dialogue timer for michael speaking to the player as franklin.
									//set_label_as_triggered("TM44_mike", false)

								break 
								
								case char_trevor
								
									SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), true)
								
								break 
								
							endswitch 


							selector_cam.bPedSwitched = TRUE

						endif
						
					endif 

				endif 
				
				put_michael_into_helicopter_when_switching_to_trevor()
				
				//sets timer for sniper aim.
				if activate_heli_cam_a_frame_early_for_switch_to_michael()
					sniper_hud_time = get_game_timer()
					render_sniper_hud_status = 0
				endif 
				
			else 
			
				turn_on_sniper_hud_for_exile_2 = false
			
				selector_cam.bOKToSwitchPed = false
				selector_cam.bPedSwitched = false

				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(), EXL2_DAMAGE)
				
				CLEAR_CONTROL_LIGHT_EFFECT(player_control)
			
				switch get_current_player_ped_enum()

					case char_michael
					
						//only put focus when you are michael and not michael chop
						SET_FOCUS_POS_AND_VEL(<<-1664.92, 4438.81, 80.00>>, <<0.0, 0.0, 0.0>>)

						DISABLE_CELLPHONE(true)

						//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)

						if not does_blip_exist(franklins_blip)
							franklins_blip = create_blip_for_ped(selector_ped.pedID[selector_ped_franklin])
						endif 

						//resets the sniper hud
						sniper_hud_time = get_game_timer()
						render_sniper_hud_status = 0
						
						helicopter_switch_system_status = 0
						
						IF chop_cam.state = CHOP_VIEW_CAM_to_CHOP
						
							//task_aim_gun_at_coord(player_ped_id(), <<-1531.6, 4376.7, 68.00>>, -1)
							SET_PLAYER_FORCED_AIM(player_id(), true)
							set_ped_reset_flag(player_ped_id(), PRF_InstantBlendToAim, true)
							force_ped_ai_and_animation_update(player_ped_id(), true)

							//SET_PLAYER_FORCED_AIM(player_id(), false)

							set_player_control(player_id(), false, spc_allow_player_damage)
							SET_CAM_CONTROLS_MINI_MAP_HEADING(chop_cam.chopCam, true)
							//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_michael, true)
							SET_CHOP_VIEW_CAM_ON_CHOP(chop_cam, Selector_Ped, TRUE)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(EXL2_TIME_AS_CHOP)
							
							int r, g, b, a
							//get_chop_control_light_effect_colour(r, g, b)
							get_hud_colour(HUD_COLOUR_CONTROLLER_CHOP, r, g, b, a) 
							SET_CONTROL_LIGHT_EFFECT_COLOR(player_control, r, g, b)

							//SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, selector_ped_franklin, selector_ped_franklin)
						
						else 
							
							//only put focus when you are michael and not michael chop
							//put focus here
							
							if does_entity_exist(chop.ped)
								//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor, true)//trevor slot = chop
							endif 
							
							set_chop_view_cam_disabled(chop_cam, Selector_Ped, false, true, true, false)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED() 
							
							set_cam_active(heli_cam, false)
							render_script_cams(false, false) //forces the game camera to activate without interpolating back to game play camera from spline camera. Overrides kenneth's run_spline function. 
							//destroy_cam(heli_cam)	//forces helicopter camera system to reset itself to the correct centre position when switching back to micahel.
							//helicopter_cam_status = 0
						endif 
						
					break 
					
					case char_franklin
					
						//script_assert("player is franklin")

						DISABLE_CELLPHONE(false)
	
						task_aim_gun_at_coord(selector_ped.pedID[selector_ped_michael], <<-1531.6, 4376.7, 68.00>>, -1)
						set_ped_reset_flag(selector_ped.pedID[selector_ped_michael], PRF_InstantBlendToAim, true)
						force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael], true)

						//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_michael, true)

						helicopter_switch_system_status = 0
						
						IF chop_cam.state = CHOP_VIEW_CAM_to_CHOP
						
							set_player_control(player_id(), false, spc_allow_player_damage)
							SET_CAM_CONTROLS_MINI_MAP_HEADING(chop_cam.chopCam, true)
							//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)
							SET_CHOP_VIEW_CAM_ON_CHOP(chop_cam, Selector_Ped, TRUE)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(EXL2_TIME_AS_CHOP)
							
							int r, g, b, a
							//get_chop_control_light_effect_colour(r, g, b)
							get_hud_colour(HUD_COLOUR_CONTROLLER_CHOP, r, g, b, a) 
							SET_CONTROL_LIGHT_EFFECT_COLOR(player_control, r, g, b)
 
						else 
											
							if does_entity_exist(chop.ped)
								//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor, true) //chop
							endif 
							
							set_ped_dies_in_water(player_ped_id(), true)

							REMOVE_PED_DEFENSIVE_AREA(player_ped_id(), true)
							clear_ped_tasks(player_ped_id())
							set_chop_view_cam_disabled(chop_cam, Selector_Ped)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED() 
						endif 
						
					break 
					
					case char_trevor
					
						set_chop_view_cam_disabled(chop_cam, Selector_Ped)
					
					break 
					
				endswitch 
				
				deactivate_all_ped_proofs()

			endif 
		
		break 
		
	endswitch 

endproc


//proc helicopter_recording_system()
//
//	#IF IS_DEBUG_BUILD
//
//	switch helicopter_recording_system_status 
//	
//		case 0
//
//			if is_vehicle_driveable(dummy_helicopter.veh)
//				if is_playback_going_on_for_vehicle(dummy_helicopter.veh)
//					if START_RECORDING_VEHICLE(helicopter.veh, 001, "lkexile2", true)
//						helicopter_recording_system_status++
//					endif
//				endif 
//			endif 
//			
//		break 
//		
//		case 1
//		
//			if is_playback_going_on_for_vehicle(dummy_helicopter.veh)
//				if get_time_position_in_recording(dummy_helicopter.veh) > (get_total_duration_of_vehicle_recording(7, str_carrec) - 100)
//					stop_recording_all_vehicles()
//					helicopter_recording_system_status++
//				endif 
//			endif 
//		
//		break 
//		
//		case 2
//		
//			script_assert("recording finished")
//			
//			helicopter_recording_system_status++
//		
//		break 
//		
//		case 3
//		
//		break 
//		
//	endswitch 
//	
//	#endif 
//
//endproc 

func bool rpg_cutscene_system()

//	printfloat(GET_TIME_POSITION_IN_RECORDING(helicopter.veh))
//	printnl()

	if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		rpg_cutscene_system_status = 22
	endif 
	
	int i = 0
	
//	if is_playback_going_on_for_vehicle(helicopter.veh)
//		printstring("vehicle recording time: ")
//		printfloat(get_time_position_in_recording(helicopter.veh))
//		printnl()
//	endif

//	printint(rpg_cutscene_system_status)
//	printnl()
	
	switch rpg_cutscene_system_status

		case 0
		
			if not does_cam_exist(selector_cam.camID)
				
				if start_new_cutscene_no_fade(false, false)
				
					//ensure player is michael and no chop hud avaliable 
					if not (get_current_player_ped_enum() = char_michael)
						make_selector_ped_selection(selector_ped, SELECTOR_PED_michael)
						take_control_of_selector_ped(selector_ped)
					endif 
					
					SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
				
					allow_switch = false

					destroy_cam(heli_cam)
					helicopter_cam_status = 0
			
					SET_SEETHROUGH(FALSE)
					
					if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
						stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
					endif 
					
					big_foot.created = true
					if does_entity_exist(big_foot.ped)
						delete_ped(big_foot.ped)
					endif 
					set_model_as_no_longer_needed(big_foot.model)

					//SEETHROUGH_SET_MAX_THICKNESS(500.00) // 10.00
				
					if not is_ped_injured(gang_enemy[3].ped)
						gang_enemy[3].pos = <<-1541.2722, 4489.3423, 18.4372>>
						gang_enemy[3].heading = 187.5681
						rpg_target_pos_0 = <<-1552.5, 4451, 38.3>>
						clear_ped_tasks_immediately(gang_enemy[3].ped)
						set_entity_coords(gang_enemy[3].ped, gang_enemy[3].pos)
						set_entity_heading(gang_enemy[3].ped, gang_enemy[3].heading)
						set_entity_proofs(gang_enemy[3].ped, true, true, true, true, true)
						manage_gang_enemy_3_status = 7
					endif
					
					stop_playback_recorded_vehicle(helicopter.veh)
					START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, 7, str_carrec)
					skip_time_in_playback_recorded_vehicle(helicopter.veh, 4500) //4500 3900
					force_playback_recorded_vehicle_update(helicopter.veh)
					
					if does_particle_fx_looped_exist(fxrpg)
						STOP_PARTICLE_FX_LOOPED(fxrpg)
					endif 
					
					if not has_sound_finished(rpg_sound)
						stop_sound(rpg_sound)
					endif 
					DELETE_entity(rpg_obj)
					
					
					clear_area(<<-1532.08, 4416.26, 65.35>>, 200.00, true)
					
					rpg.pos = <<-1532.08, 4416.26, 65.35>>
					
					//cutscene_rpg_target_pos = get_offset_from_entity_in_world_coords(helicopter.veh, <<-7.0, 0.0, -1.0>>) //<<-3.0, 0.0, -0.5>>)
					cutscene_rpg_target_pos = <<-1474.7, 4385.4, 93.9>>
					
					cutscene_rpg_direction_vector = normalise_vector(cutscene_rpg_target_pos - rpg.pos)

					//obtains rotation from directional vector 
					rpg.rot.x = ATAN2(cutscene_rpg_direction_vector.z, VMAG(<<cutscene_rpg_direction_vector.x, cutscene_rpg_direction_vector.y, 0.0>>))
					rpg.rot.z = CAP_HEADING(ATAN2(cutscene_rpg_direction_vector.y, cutscene_rpg_direction_vector.x) - 90.0)

					rpg_obj = CREATE_OBJECT(rpg.model, rpg.pos)
					set_entity_rotation(rpg_obj, rpg.rot)
					fxRpg = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ex2_rpg_trail", rpg_obj, <<0.0, -0.2, 0.0>>,  <<0.0, 0.0, 0.0>>)
					PLAY_SOUND_FROM_ENTITY(rpg_sound, "Fire_Missile_Loop", rpg_obj, "EXILE_2_SOUNDS")
					
					for i = 0 to count_of(animal) - 1
						if does_entity_exist(animal[i].ped)
							delete_ped(animal[i].ped)
							animal[i].created = false
						endif 
					endfor 

					for i = 10 to count_of(animal) - 1
						if i != 15
							setup_animal(animal[i])
							animal_ai_status[i] = 0
						endif 
					endfor  

					cam_cutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<<-1528.078369,4415.430176,67.797333>>,<<16.769043,0.011513,-122.888321>>, 27.570831, true)
					SET_CAM_PARAMS(cam_cutscene, <<-1514.494751,4405.609375,75.447258>>,<<16.155361,0.011514,-115.520653>>, 27.570831, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					
					shake_cam(cam_cutscene, "ROAD_VIBRATION_SHAKE", 2.0)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					REPLAY_START_EVENT()

					if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE")
						STOP_AUDIO_SCENE("EXILE_2_SNIPE_STAGE")
					endif 
					
					if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
						STOP_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
					endif 

					start_audio_scene("EXILE_2_RPG_CUTSCENE")
					
					trigger_music_event("EXL2_RPG_HELI")

					rpg_cutscene_system_status++

				endif 
				
			endif 
			
		break 
		
		case 1

			//rpg.pos += (cutscene_rpg_direction_vector * get_frame_time() * 35.00)
			
			rpg.pos = get_entity_coords(rpg_obj)

			rpg_velocity = (cutscene_rpg_direction_vector * 35.00)
			
			rpg.rot.x = ATAN2(cutscene_rpg_direction_vector.z, VMAG(<<cutscene_rpg_direction_vector.x, cutscene_rpg_direction_vector.y, 0.0>>))
			rpg.rot.z = CAP_HEADING(ATAN2(cutscene_rpg_direction_vector.y, cutscene_rpg_direction_vector.x) - 90.0)
				
			//st_entity_coords(rpg_obj, rpg.pos) 
			set_entity_velocity(rpg_obj, rpg_velocity)
			set_entity_rotation(rpg_obj, rpg.rot)
			
			if get_time_position_in_recording(helicopter.veh) > 3800 //4800
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TM44_ROCKET")
					if CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_ROCKET", CONV_PRIORITY_MEDIUM)
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						
						SET_LABEL_AS_TRIGGERED("TM44_ROCKET", TRUE)
					endif 
				endif 
			endif 
			
			printstring("get_time_position_in_recording = ")
			printfloat(get_time_position_in_recording(helicopter.veh))
			printnl()
			
			if get_time_position_in_recording(helicopter.veh) > 5800

				destroy_cam(cam_cutscene)			
				
				cam_cutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<<-1458.351807,4370.578125,95.681839>>,<<-9.241117,1.313850,43.577080>>,23.889305, true)
				SET_CAM_PARAMS(cam_cutscene, <<-1447.828735,4363.346680,104.088165>>, <<-11.966377,1.313851,46.565411>>, 23.889305, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				
				//stream in map area when cutting back to gameplay.
				streaming_volume = STREAMVOL_CREATE_SPHERE(<<-1531.9, 4288.0, 103.2>>, 100.00, FLAG_MAPDATA)
				
				rpg_cutscene_system_status++
				
			endif 

		break 
			
		case 2
			
			rpg.pos = get_entity_coords(rpg_obj)
		
			if not are_vectors_alomost_equal(rpg.pos, cutscene_rpg_target_pos, (cutscene_rpg_direction_vector * get_frame_time() * 35.00))
				
				rpg_velocity = (cutscene_rpg_direction_vector * 35.00)

				set_entity_velocity(rpg_obj, rpg_velocity)
				set_entity_rotation(rpg_obj, rpg.rot)
				
			else 
				
				ADD_EXPLOSION(GET_ENTITY_COORDS(rpg_obj), EXP_TAG_ROCKET, 1.0)

				if does_particle_fx_looped_exist(fxrpg)
					STOP_PARTICLE_FX_LOOPED(fxrpg)
				endif 
				
				if not has_sound_finished(rpg_sound)
					stop_sound(rpg_sound)
				endif 

				DELETE_entity(rpg_obj)
				
				SHAKE_CAM(cam_cutscene, "sky_diving_shake", 1.0)
				
				rpg_cutscene_system_status++

			endif 
		
		break 
		
		case 3
		
			if GET_TIME_POSITION_IN_RECORDING(helicopter.veh) > 7300
				original_time = get_game_timer()
				rpg_cutscene_system_status++
			endif 
		
		break 

		case 4
		
			if lk_timer(original_time, 750)
		
				//reset gang enemy 3 attack time
				iRandTime = 5000//GET_RANDOM_INT_IN_RANGE(5000, 10000)
				
				if is_playback_going_on_for_vehicle(helicopter.veh)
					stop_playback_recorded_vehicle(helicopter.veh)
				endif 
				
				START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, 001, "lkexile2")
				skip_time_in_playback_recorded_vehicle(helicopter.veh, 10000)
				force_playback_recorded_vehicle_update(helicopter.veh)
				
				activate_helicopter_flying_system = true 
				
				bPlayingHoverBackwards = false
				
				dialogue_system_status = 8
				
				SET_PLAYER_FORCED_AIM(player_id(), false)
				force_sniper_hud_for_start_of_mission = false
				render_sniper_hud = false
				clear_ped_tasks_immediately(player_ped_id())
				SET_ped_DESIRED_HEADING(player_ped_id(), get_entity_heading(player_ped_id()))
				ATTACH_ENTITY_TO_ENTITY(player_ped_id(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
				SET_PED_MOVEMENT_CLIPSET(player_ped_id(), "move_ped_crouched")
				SET_PED_WEAPON_MOVEMENT_CLIPSET(player_ped_id(), "Wpn_AssaultRifle_WeaponHoldingCrouched")
				SET_PED_STRAFE_CLIPSET(player_ped_id(), "move_ped_crouched_strafing")
				//force_ped_ai_and_animation_update(player_ped_id())
				
				REPLAY_STOP_EVENT()

				end_cutscene_no_fade(false, true, false, 0, 0, 3000, false)

				//helicopter_flying_system()
				
				//helicopter_recording_system()
				
				streamvol_delete(streaming_volume)
				
				CREATE_CHOP_CAM(chop_cam)
				
				helicopter_camera_system(true)

				start_rpg_cutscene = false
				
				allow_switch = false
				
				//--
				
				make_selector_ped_selection(selector_ped, SELECTOR_PED_franklin)
				eSwitchCamState = SWITCH_CAM_START_SPLINE1
				
				HANDLE_SWITCH_CAM_SCOPE_TO_FRANKLIN(scsSwitchCamScopeToFranklin)
				
				
				//--

				rpg_cutscene_system_status = 23
				
			endif 
		
		break 
		
		case 5
		
		break
		
		case 22
		
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(1000)
				endif 
			else 
		
				if DOES_PARTICLE_FX_LOOPED_EXIST(fxRpg)
					sTOP_PARTICLE_FX_LOOPED(fxrpg)
				endif 
				DELETE_entity(rpg_obj)
				
				clear_area(<<-1532.08, 4416.26, 65.35>>, 200.00, true)
			
				iRandTime = GET_RANDOM_INT_IN_RANGE(5000, 10000)
	
				
				if is_playback_going_on_for_vehicle(helicopter.veh)
					stop_playback_recorded_vehicle(helicopter.veh)
				endif 
			
				START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, 001, "lkexile2")
				skip_time_in_playback_recorded_vehicle(helicopter.veh, 10000)
				force_playback_recorded_vehicle_update(helicopter.veh)
	
				activate_helicopter_flying_system = true 
				
				bPlayingHoverBackwards = false
				
				dialogue_system_status = 8
				
				SET_PLAYER_FORCED_AIM(player_id(), false)
				force_sniper_hud_for_start_of_mission = false
				render_sniper_hud = false
				clear_ped_tasks_immediately(player_ped_id())
				SET_ped_DESIRED_HEADING(player_ped_id(), get_entity_heading(player_ped_id()))
				ATTACH_ENTITY_TO_ENTITY(player_ped_id(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
				SET_PED_MOVEMENT_CLIPSET(player_ped_id(), "move_ped_crouched")
				SET_PED_WEAPON_MOVEMENT_CLIPSET(player_ped_id(), "Wpn_AssaultRifle_WeaponHoldingCrouched")
				SET_PED_STRAFE_CLIPSET(player_ped_id(), "move_ped_crouched_strafing")
				
				end_cutscene(false, 0, 0, false)
				
				streamvol_delete(streaming_volume)
				
				helicopter_camera_system()

				start_rpg_cutscene = false
				
				allow_switch = false 

				//---
				make_selector_ped_selection(selector_ped, SELECTOR_PED_franklin)
				eSwitchCamState = SWITCH_CAM_START_SPLINE1
				
				HANDLE_SWITCH_CAM_SCOPE_TO_FRANKLIN(scsSwitchCamScopeToFranklin)
				
				
				//--

				rpg_cutscene_system_status = 23

			endif 
		
		break 
		
		case 23
		
		break 
		
	endswitch
	
	return false

endfunc 

proc animal_ai_system()

	int i 


	for i = 1 to count_of(animal) - 1

		if not is_ped_injured(animal[i].ped)
		
			if not (animal_ai_status[i] >= 22)

				//if get_script_task_status(animal[i].ped, SCRIPT_TASK_SMART_FLEE_PED) != performing_task
				if has_ped_task_finished_2(animal[i].ped, SCRIPT_TASK_SMART_FLEE_PED)
				
					if (not is_ped_injured(gang_enemy[0].ped) and get_distance_between_coords(get_entity_coords(animal[i].ped), get_entity_coords(gang_enemy[0].ped)) < 2.0)

						task_smart_flee_ped(animal[i].ped, gang_enemy[0].ped, 50.00, 5000)

						animal_ai_status[i] = 22
					
					elif (not is_ped_injured(gang_enemy[1].ped) and get_distance_between_coords(get_entity_coords(animal[i].ped), get_entity_coords(gang_enemy[1].ped)) < 2.0)
						
						task_smart_flee_ped(animal[i].ped, gang_enemy[1].ped, 50.00, 5000)
						
						animal_ai_status[i] = 22
					
					elif get_distance_between_coords(get_entity_coords(animal[i].ped), get_entity_coords(player_ped_id())) < 4.0
					
						task_smart_flee_ped(animal[i].ped, player_ped_id(), 50.00, 5000)
					
						animal_ai_status[i] = 22

					elif is_bullet_in_area(get_entity_coords(animal[i].ped), 4.0)
				
						task_smart_flee_ped(animal[i].ped, player_ped_id(), 350.00, -1) //350
								
						animal_ai_status[i] = 22
					
					endif 
					
				endif 
	
			endif 
			
			
		
			switch animal_ai_status[i]
			
				case 0
				
					switch i 
						
						case 1
						case 3
						case 5
						case 7
						case 9
						case 11
						case 13
						case 15
						case 17
						
							if has_ped_task_finished_2(animal[i].ped)
								
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, animal_movement[i].animal_pos_0, pedmove_run, -1)
									task_pause(null, 5000)
								close_sequence_task(seq)
								task_perform_sequence(animal[i].ped, seq)
								clear_sequence_task(seq)
								
								animal_ai_status[i]++
							endif 
							
						break 
						
						case 2
						case 4
						case 6
						case 8
						case 10
						case 12
						case 14
						case 16
						case 18
						
							if has_ped_task_finished_2(animal[i].ped)
								
								open_sequence_task(seq)
									task_pause(null, 5000)
									task_follow_nav_mesh_to_coord(null, animal_movement[i].animal_pos_0, pedmove_walk, -1)
								close_sequence_task(seq)
								task_perform_sequence(animal[i].ped, seq)
								clear_sequence_task(seq)
								
								animal_ai_status[i]++
								
							endif 
						
						break 
						
					endswitch 
				
				break 
				
				case 1
				
					switch i 
					
						case 1
						case 3
						case 5
						case 7
						case 9
						case 11
						case 13
						case 15
						case 17
						
							if has_ped_task_finished_2(animal[i].ped)
								
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, animal_movement[i].animal_pos_1, pedmove_run, -1)
									task_pause(null, 5000)
								close_sequence_task(seq)
								task_perform_sequence(animal[i].ped, seq)
								clear_sequence_task(seq)
								
								animal_ai_status[i]++
								
							endif 
						
						break 
						
						case 2
						case 4
						case 6
						case 8
						case 10
						case 12
						case 14
						case 16
						case 18
						
							if has_ped_task_finished_2(animal[i].ped)
								
								open_sequence_task(seq)
									task_pause(null, 5000)
									task_follow_nav_mesh_to_coord(null, animal_movement[i].animal_pos_1, pedmove_walk, -1)
									task_pause(null, 5000)
								close_sequence_task(seq)
								task_perform_sequence(animal[i].ped, seq)
								clear_sequence_task(seq)
								
								animal_ai_status[i]++
								
							endif 
							
						break 
						
					endswitch 
				
				break 
				
				case 2
				
					switch i 
					
						case 1
						case 3
						case 5
						case 7
						case 9
						case 11
						case 13
						case 15
						case 17
						
							if has_ped_task_finished_2(animal[i].ped)
						
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, animal_movement[i].animal_pos_2, pedmove_run, -1)
									task_pause(null, 5000)
								close_sequence_task(seq)
								task_perform_sequence(animal[i].ped, seq)
								clear_sequence_task(seq)
								
								animal_ai_status[i] = 0
								
							endif 
						
						break 
						
						case 2
						case 4
						case 6
						case 8
						case 10
						case 12
						case 14
						case 16
						case 18
						
							if has_ped_task_finished_2(animal[i].ped)
								
								open_sequence_task(seq)
									task_pause(null, 5000)
									task_follow_nav_mesh_to_coord(null, animal_movement[i].animal_pos_2, pedmove_walk, -1)
									task_pause(null, 5000)
								close_sequence_task(seq)
								task_perform_sequence(animal[i].ped, seq)
								clear_sequence_task(seq)
								
								animal_ai_status[i] = 0
								
							endif 
							
						break 
						
					endswitch 
				
				break 
				
				case 22
				
					if has_ped_task_finished_2(animal[i].ped, SCRIPT_TASK_SMART_FLEE_PED)
					
//						printstring("animal name task finished = ")
//						printint(i)
//						printnl()
					
						animal_ai_status[i] = 0
						
					endif 
				
				break 

			endswitch 
		
		endif 
		
	endfor 

endproc

proc big_foot_ai_system()

	if snipe_enemies_master_flow_system_status < 3//stop running once you need to kill the final rpg enemy
			
		if not is_ped_injured(big_foot.ped)
		
			switch big_foot_ai_system_status 

				case 0
				
					IF IS_ENTITY_ON_SCREEN(big_foot.ped)

						if not render_sniper_hud //when SET_PLAYER_FORCED_AIM it forces the sniper to aim. This sets the gamplay cam to sniper view which was being set at 5 fov. This will make the gamplay_cam_fov condition return true. However 
												 //sniper hud takes a frame to update the fov meaning get_screen_coord_from_world_coord() returns normal spline cam view

							IF GET_GAMEPLAY_CAM_FOV() < 10
								
								FLOAT f_x
								FLOAT f_y 
								
								f_x = 0.0
								f_y = 0.0
								GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(big_foot.ped), f_x, f_y)
								
							
								IF ABSF(f_x - 0.5) < 0.18
									
									IF ABSF(f_y - 0.5) < 0.18
									
										big_foot.time = get_game_timer()
										
										big_foot_ai_system_status++
										
									endif 
								endif 
								
							endif 
						endif 
						
					endif 
					
					if is_bullet_in_area(get_entity_coords(big_foot.ped), 4.0)
						delete_ped(big_foot.ped)
					endif 

				break 
				
				case 1
		
					if b_thermal_active

						if lk_timer(big_foot.time, 2000)
							delete_ped(big_foot.ped)
							big_foot_ai_system_status++
						endif 
						
					else 
					
						if lk_timer(big_foot.time, 1300)
							delete_ped(big_foot.ped)
							big_foot_ai_system_status++
						endif 
					
					endif 
				
				break 
				
				case 2
				
				break 
				
			endswitch 
			
		else 

			if not big_foot_global_created 
				if not big_foot.created
					
					request_model(big_foot.model)
					
					if has_model_loaded(big_foot.model)
					
						setup_animal(big_foot)
						set_entity_proofs(big_foot.ped, true, true, true, true, true, true)
						big_foot_global_created = true 
						
					endif 
					
				endif 
			endif 
		
		endif 
	endif 

endproc 

///if the player starts zooming before thermal help text has finsihed then we don't process the help text
proc sniper_rifle_help_text_system()
			
	if not stop_thermal_help 
	
		if is_player_zooming_with_sniper_rifle()
			sniper_rifle_help_text_system_status = 99
		endif 
	
	else 
	
		sniper_rifle_controls_help_text_system(sniper_rifle_help_text_system_status, sniper_rifle_zoom_time)
		
	endif 
	
endproc


func bool SNIPE_ENEMIES()
	
	INT i = 0
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()

	IF e_section_stage = SECTION_STAGE_SETUP

		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(EXL2_DEATH_FROM_ABOVE)
	
		//SEETHROUGH_SET_MAX_THICKNESS(500.00)
		
		STREAMVOL_DELETE(streaming_volume)
		
		SEETHROUGH_SET_HEATSCALE(tb_dead, 0.0)
		SEETHROUGH_SET_HEATSCALE(tb_cold, 1.0) //small animals
		SEETHROUGH_SET_HEATSCALE(tb_warm, 1.0) //large animals and PEDS 
		SEETHROUGH_SET_HEATSCALE(tb_hot, 1.0) //peds
		
		SET_FOCUS_POS_AND_VEL(<<-1664.92, 4438.81, 80.00>>, <<0.0, 0.0, 0.0>>) //OLD
		//SET_FOCUS_POS_AND_VEL(<<-1593.8, 4506.7, 80.00>>, <<0.0, 0.0, 0.0>>)
	
		helicopter_cam_status = 0
		
		//resets the flags after entities used in previous section of mission.
		for i = 0 to count_of(gang_enemy) - 1
			gang_enemy[i].created = false 
		endfor 

		for i = 0 to 1
			setup_enemy(gang_enemy[i])
			remove_blip(gang_enemy[i].blip)
			SET_PED_DIES_IN_WATER(gang_enemy[i].ped, false)
			SET_ENTITY_LOAD_COLLISION_FLAG(gang_enemy[i].ped, true)
		endfor 
		
		SET_ROADS_IN_AREA(vCountryRoadMin, vCountryRoadMax, FALSE)
		
		SET_VEHICLE_POPULATION_BUDGET(0) //removes all cars from vehicle population
		SET_PED_POPULATION_BUDGET(0)
		
		REQUEST_ANIM_DICT(strAnimDictCamShake)
		
		b_thermal_active = FALSE
		
		manage_gang_enemy_0_status = 0
		manage_gang_enemy_1_status = 0
		manage_gang_enemy_2_status = 0
		manage_gang_enemy_3_status = 0
		i_current_event = 0
		iFranklinCallProg = 0
		
		DISABLE_CELLPHONE(TRUE)
		
		IF IS_VEHICLE_DRIVEABLE(helicopter.veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(helicopter.veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(helicopter.veh)
			ENDIF
			
			REMOVE_VEHICLE_RECORDING(CARREC_HELI_ARRIVE, str_carrec)

			FREEZE_ENTITY_POSITION(helicopter.veh, FALSE)
			
			START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, CARREC_HELI_HOVER, str_carrec)
			SET_VEHICLE_ENGINE_ON(helicopter.veh, TRUE, TRUE)
			SET_HELI_BLADES_FULL_SPEED(helicopter.veh)
			force_playback_recorded_vehicle_update(helicopter.veh)
		ENDIF

		SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), false)
		SET_FIRST_PERSON_AIM_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(-80.00, 80.00)
		
		//forces the sniper hud to render immediately
		clear_ped_tasks(player_ped_id())
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() //always want to force the 3rd person anims. 
		SET_PLAYER_FORCED_AIM(player_id(), true)
		set_ped_reset_flag(player_ped_id(), PRF_InstantBlendToAim, true)
		force_ped_ai_and_animation_update(player_ped_id(), true)
		
		//if in first person mode the snipper hud will remain constantly on. Else it will render but if the player presses the aim button and depresses it. Then the player will stop aiming and as a result the sniper hud will not render.
		if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
			force_sniper_hud_for_first_person_view_mode = true 
		else 
			force_sniper_hud_for_start_of_mission = true
		endif 
			


		setup_weapon_for_ped(selector_ped.pedID[selector_ped_franklin], weapontype_carbinerifle, 100, false, false)

		if is_screen_faded_in()
			end_cutscene_no_fade(false, true, false, -12, -41, 3000, false)
		else 
			end_cutscene(false, -12, -41, false)
		endif
		
		SET_SELECTOR_PED_HINT(selector_ped, selector_ped_michael, false)
		SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)
		
		if does_entity_exist(chop.ped)
			chop_ai_system_status = 200
			set_selector_ped_blocked(selector_ped, selector_ped_trevor, false)
			SET_SELECTOR_PED_ICON(selector_ped, SELECTOR_PED_TREVOR, SELECTOR_ICON_CHOP)
			chop_cam.selectorSlot = SELECTOR_PED_TREVOR
			chop_cam.chopPed = chop.ped
			//selector_ped.pedID[SELECTOR_PED_TREVOR] = chopPed
			CREATE_CHOP_CAM(chop_cam)
			chop_cam.state = CHOP_VIEW_CAM_DISABLED
			set_chop_view_cam_disabled(chop_cam, selector_ped)
			SET_ENTITY_LOAD_COLLISION_FLAG(chop.ped, true)
			SET_ENTITY_LOD_DIST(chop.ped, 400)
			clear_ped_tasks_immediately(chop.ped)
			set_entity_coords(chop.ped, <<-1679.8206, 4455.9878, 1.2653>>)
			set_entity_heading(chop.ped, 114.22)		
			task_play_anim(chop.ped, "missexile2", "fra0_ig_12_chop_waiting_a", normal_blend_in, normal_blend_out, -1, af_looping)
			set_ped_can_ragdoll(chop.ped, false)
		endif 
		
		clear_ped_tasks(selector_ped.pedID[selector_ped_franklin])
		SET_PED_USING_ACTION_MODE(selector_ped.pedID[selector_ped_franklin], true)
		SET_ENTITY_LOAD_COLLISION_FLAG(selector_ped.pedID[selector_ped_franklin], true)
		SET_ENTITY_LOD_DIST(selector_ped.pedID[selector_ped_franklin], 300) 
		
		START_AUDIO_SCENE("EXILE_2_SNIPE_STAGE")
		
		trigger_music_event("EXL2_SNIPE_START")

		allow_switch = false
		
		helicopter_camera_system()
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "start of sniper section")

		e_section_stage = SECTION_STAGE_RUNNING
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING

		switch get_player_ped_enum(player_ped_id()) 
		
			case char_michael
				
				set_ped_can_play_ambient_anims(player_ped_id(), false)
				SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(player_ped_id(), false)
				SET_PED_RESET_FLAG(player_ped_id(), PRF_DisableSecondaryAnimationTasks, true)
				disable_control_action(PLAYER_CONTROL, INPUT_JUMP)
				disable_control_action(PLAYER_CONTROL, INPUT_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				disable_control_action(PLAYER_CONTROL, INPUT_sprint) 
				disable_control_action(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
				//disable_control_action(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
				disable_control_action(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
				disable_control_action(PLAYER_CONTROL, input_veh_cin_cam)
				
				SET_PED_RESET_FLAG(player_ped_id(), PRF_UseBulletPenetration, true) //allows sniper bullet to shoot through walls
				
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED_ID(), helicopter.veh)
					ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
				ENDIF
				
				//sets the player can only free aim.
				//SET_PLAYER_TARGETING_MODE_OVERRIDE(PLAYER_ID(), TARGETING_OPTION_FREEAIM)
				
				SET_AMMO_IN_CLIP(player_ped_id(), WEAPONTYPE_HEAVYSNIPER, 6)
				
			break 
			
			case char_franklin
				
			break 
			
		endswitch 

		if snipe_enemies_master_flow_system()
			mission_flow = stage_mission_pass_conversation
			return true 
		endif 
		
		if not start_rpg_cutscene
		
			dialogue_system()
			
			helicopter_switch_system()
			
			helicopter_camera_system() 

			thermal_vision_system()
			
			sniper_rifle_help_text_system()

			MANAGE_ENEMY_PEDS_ON_GROUND()
			
			animal_ai_system()
			
			big_foot_ai_system()

			if snipe_enemies_master_flow_system_status >= 4 //processes when custum switch to franklin not running. 
				chop_ai_system()
				handle_chop_cam_hidof(chop_cam) //reduces blurryness of chop cam 
				franklin_ai_system()
			ENDIF
			
			if get_current_player_ped_enum() = char_michael
				SET_FIRST_PERSON_AIM_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(-80.00, 80.00)
				SET_FIRST_PERSON_AIM_CAM_NEAR_CLIP_THIS_UPDATE(0.3)//fix 1393972
				set_ped_ammo(player_ped_id(), WEAPONTYPE_HEAVYSNIPER, 200)
				SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(), 0) //fix for bug 1546512 
			endif 

			if not activate_helicopter_flying_system
				MAINTAIN_CHOPPER_HOVER(helicopter.veh, 5000, 31000) 
			else 
				MAINTAIN_CHOPPER_HOVER(helicopter.veh, (get_total_duration_of_vehicle_recording(001, "lkexile2") - 12000), (get_total_duration_of_vehicle_recording(001, "lkexile2") - 500), 0.9) //-500
			endif
			
		endif 
			
		if start_rpg_cutscene
		
			rpg_cutscene_system()
			
		endif 
		
		//forces the 3rd person sniper anims for michael. First person camera is never used for michael. Michael uses scripted helicopter camera (third person) or sniper camera hud (first / thrid person) 
		if get_current_player_ped_enum() = char_michael 
			
			if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			endif 
			
			DISABLE_CAMERA_VIEW_MODE_CYCLE(player_id())
		endif 

	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
		PAUSE_TIMER(count_down_timer)
		KILL_ANY_CONVERSATION()
		e_section_stage = SECTION_STAGE_SETUP
		mission_flow = STAGE_RPG_CUTSCENE
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
	
		switch snipe_enemies_skip_status 
		
			case 0
				
				if not does_entity_exist(gang_enemy[3].ped)
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
					
					for i = 0 to 2
						if not is_ped_injured(gang_enemy[i].ped)
							set_entity_health(gang_enemy[i].ped, 2)
						endif 
					endfor
					
					if not is_gang_enemy_3_pos_on_screen()
						gang_enemy[3].pos = <<-1541.2722, 4489.3423, 18.4372>>
						gang_enemy[3].heading = 187.5681
						rpg_target_pos_0 = <<-1552.5, 4451, 38.3>>
					endif 
					
					setup_enemy(gang_enemy[3])
					set_entity_proofs(gang_enemy[3].ped, true, true, true, true, true)
					SET_PED_CAN_BE_DRAGGED_OUT(gang_enemy[3].ped, false)
					remove_blip(gang_enemy[3].blip)
					if get_current_player_ped_enum() = char_michael
						set_entity_proofs(gang_enemy[3].ped, true, true, true, true, true)
					endif 
					SET_PED_HEATSCALE_OVERRIDE(gang_enemy[3].ped, 1)
					SET_ENTITY_LOAD_COLLISION_FLAG(gang_enemy[3].ped, true)
					add_ped_for_dialogue(scripted_speech, 6, gang_enemy[3].ped, "oneil") 
					
					snipe_enemies_master_flow_system_status = 2

					manage_gang_enemy_3_status = 2	
					
					set_label_as_triggered("TM44_TWT", true)
					set_label_as_triggered("TM44_WATER", true)
					set_label_as_triggered("TM44_DWATER", true)
					set_label_as_triggered("TM44_GWOOD", true)
					set_label_as_triggered("TM44_DEADR", true)
					
					snipe_enemies_skip_status++

				endif 
				
			break 
			
			case 1

				if snipe_enemies_master_flow_system_status > 2
				
					switch get_current_player_ped_enum()
					
						case char_michael
						
							if helicopter_switch_system_status = 0 //system in a normal state and not switching already
						
								make_selector_ped_selection(selector_ped, SELECTOR_PED_franklin)
								 setup_switch_cam_to_franklin_from_michael()
								helicopter_switch_system_status = 1
							
								snipe_enemies_skip_status++
								
							endif 
						
						break 
						
						case char_franklin
						
							if not is_ped_injured(gang_enemy[3].ped)
								set_entity_health(gang_enemy[3].ped, 2)
								snipe_enemies_skip_status = 3
							endif 
						
						break 
						
					endswitch 
				
				endif
			
			break 
			
			case 2
			
				if not does_cam_exist(selector_cam.camID)
				
					if not is_ped_injured(gang_enemy[3].ped)
						set_entity_health(gang_enemy[3].ped, 2)
						snipe_enemies_skip_status = 3
					endif 
					
				endif 
			
			break
			
			case 3
			
			break 
			
		endswitch 
		
	ENDIF
	
	return false 
	
endfunc 





FUNC FLOAT GET_RPG_DISTANCE_COVERED()
	IF DOES_ENTITY_EXIST(oRPG)
		IF NOT IS_ENTITY_DEAD(helicopter.veh)
			RETURN ((VMAG((GET_ENTITY_COORDS(oRPG) - vRocketStart))) / (VMAG(GET_ENTITY_COORDS(helicopter.veh) - vRocketStart )))
		ENDIF
	ENDIF
	
	RETURN 0.0
ENDFUNC

PROC SET_RPG_DISTANCE_COVERED(FLOAT fDistance)
	IF DOES_ENTITY_EXIST(oRPG)
		IF NOT IS_ENTITY_DEAD(helicopter.veh)
			SET_ENTITY_COORDS(oRPG, vRocketStart + (fDistance * VMAG((GET_ENTITY_COORDS(helicopter.veh) - vRocketStart)) * vPedToHeliNorm))
		ENDIF
	ENDIF
ENDPROC

//PROC RPG_CUTSCENE()
//	INT i
//	MODEL_NAMES mRocket = GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG)
//	
//	IF e_section_stage = SECTION_STAGE_SETUP
//		MODEL_NAMES wRPG = GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG)
//		
//		REQUEST_MODEL(G_M_Y_SalvaGoon_01)
//		REQUEST_MODEL(mRocket)
//		REQUEST_MODEL(wRPG)
//		REQUEST_VEHICLE_RECORDING(6, str_carrec)
//		request_vehicle_recording(001, "lkexile2")
//		REQUEST_VEHICLE_RECORDING(8, str_carrec)
//		REQUEST_PTFX_ASSET("Data")
//		SET_ROADS_IN_AREA(vCountryRoadMin, vCountryRoadMax, FALSE)
//		IF HAS_MODEL_LOADED(G_M_Y_SalvaGoon_01)
//		AND HAS_MODEL_LOADED(mRocket)
//		AND HAS_MODEL_LOADED(wRPG)
//		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(6, str_carrec)
//		AND has_vehicle_recording_been_loaded(001, "lkexile2")
//		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(8, str_carrec)
//		AND HAS_PTFX_ASSET_LOADED("Data")
//			SET_SEETHROUGH(FALSE)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//			KILL_ANY_CONVERSATION()
//			CLEAR_PRINTS()
//			IF (NOT DOES_ENTITY_EXIST(gang_enemy[3].ped))
//			OR IS_ENTITY_DEAD(gang_enemy[3].ped)	
//				gang_enemy[3].ped = CREATE_PED(PEDTYPE_MISSION, G_M_Y_SalvaGoon_01, << -1659.5070, 4491.0112, 0.4586 >>, 221.7444)
//				SET_PED_DEFAULT_COMPONENT_VARIATION(gang_enemy[3].ped)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gang_enemy[3].ped, TRUE)
//				GIVE_WEAPON_TO_PED(gang_enemy[3].ped, WEAPONTYPE_RPG, 10, TRUE)
//				SET_CURRENT_PED_WEAPON(gang_enemy[3].ped, WEAPONTYPE_RPG, TRUE)
//				IF NOT IS_ENTITY_DEAD(helicopter.veh)
//					TASK_AIM_GUN_AT_ENTITY(gang_enemy[3].ped, helicopter.veh, 9999999)
//				ENDIF
//				//SCRIPT_ASSERT("Doesn't exist")
//			ELSE
//				IF NOT IS_ENTITY_DEAD(gang_enemy[3].ped)
//					
//					IF HAS_PED_PROCESSED_SEQ(gang_enemy[3].ped, FALSE, 1)
//						//-- Will get in here if the rpg guy is already aiming at the chopper
//					
//						SET_ENTITY_COORDS(gang_enemy[3].ped, << -1659.5070, 4491.0112, 0.4586 >>)
//						SET_ENTITY_HEADING(gang_enemy[3].ped, 221.7444)
//					ELSE
//						CLEAR_PED_TASKS_IMMEDIATELY(gang_enemy[3].ped)
//						SET_ENTITY_COORDS(gang_enemy[3].ped, << -1659.5070, 4491.0112, 0.4586 >>)
//						SET_ENTITY_HEADING(gang_enemy[3].ped, 221.7444)
//						IF NOT IS_ENTITY_DEAD(helicopter.veh)
//							TASK_AIM_GUN_AT_ENTITY(gang_enemy[3].ped, helicopter.veh, 9999999)
//						ENDIF
//					ENDIF
//					//SCRIPT_ASSERT("Setting coords")
//				ELSE
//					SCRIPT_ASSERT("Dead")
//				ENDIF
//			ENDIF
//			
//			REPEAT COUNT_OF(animal) i
//				IF DOES_ENTITY_EXIST(animal[i].ped)
//					DELETE_PED(animal[i].ped)
//				ENDIF
//			ENDREPEAT
//			
//			IF NOT IS_ENTITY_DEAD(helicopter.veh)
//				STOP_PLAYBACK_RECORDED_VEHICLE(helicopter.veh)
//				START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh,  6, str_carrec)
//			ENDIF
//			REMOVE_PED_FOR_DIALOGUE(scripted_speech, 0)
//			REMOVE_PED_FOR_DIALOGUE(scripted_speech, 1)
//			ADD_PED_FOR_DIALOGUE(scripted_speech, 0, PLAYER_PED_ID(), "MICHAEL")
//			IF NOT IS_PED_INJURED(selector_ped.pedID[SELECTOR_PED_TREVOR])
//				ADD_PED_FOR_DIALOGUE(scripted_speech, 1, selector_ped.pedID[SELECTOR_PED_TREVOR], "TREVOR")	
//			ENDIF
//			oRPG = CREATE_OBJECT(mRocket, << -1677.0735, 4490.0962, 2.3194 >>)
//			fxRpg = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ex2_rpg_trail", oRPG, <<0.0, 0.0, 0.0>>,  <<0.0, 0.0, 0.0>>)
//			
//			IF NOT IS_PED_INJURED(gang_enemy[3].ped)
//				vRocketStart = GET_ENTITY_COORDS(gang_enemy[3].ped)
//			ENDIF
//							
//							
//		//	cam_cutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1662.5211, 4491.2627, 0.8336 >>, << 14.8415, 0.0000, -112.0981 >>, 42.3743, TRUE)
//		//	SET_CAM_PARAMS(cam_cutscene, << -1662.5211, 4491.2627, 0.8336 >>, << 14.8415, 0.0000, -112.0981 >>,  37.6496, 10000)
//		
//			cam_cutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -1663.5969, 4492.0996, 0.2936 >>, << 20.0377, -0.0000, -118.1245 >>, 24.1213, TRUE)
//			SET_CAM_PARAMS(cam_cutscene, << -1663.5969, 4492.0996, 0.2936 >>, << 20.0377, -0.0000, -118.1245 >>, 22.2963, 6000)
//			
//			
//			cam_interp_helper = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", << -1601.8394, 4357.2554, 16.8713 >>, << 22.1229, 0.0000, -49.4603 >>, 39.8408)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			
//			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//			DISPLAY_RADAR(FALSE)
//			DISPLAY_HUD(FALSE)
//			SET_WIDESCREEN_BORDERS(TRUE, 0)
//			SETTIMERA(0)
//			
//			bRpgGuyToldToMove = FALSE
//			i_current_event = 0
//			fRpgSpeedMult = 10.0
//			e_section_stage = SECTION_STAGE_RUNNING
//			
//			SETTIMERB(0)
//		ENDIF
//	ENDIF
//	
//	IF e_section_stage = SECTION_STAGE_RUNNING
//
//		SWITCH i_current_event
//			CASE 0
//			
//				IF TIMERA() > 2000
//					IF NOT IS_ENTITY_DEAD(gang_enemy[3].ped)
//						IF NOT IS_ENTITY_DEAD(helicopter.veh)
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_SHOOT_AT_ENTITY(NULL, helicopter.veh, 1500,FIRING_TYPE_1_THEN_AIM)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(gang_enemy[3].ped, seq)
//							CLEAR_SEQUENCE_TASK(seq)
//							SETTIMERA(0)
//							i_current_event++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 1	
//				IF NOT IS_ENTITY_DEAD(gang_enemy[3].ped)
//					IF IS_PED_SHOOTING(gang_enemy[3].ped)
//						SHAKE_CAM(cam_cutscene, "SMALL_EXPLOSION_SHAKE", 0.5)
//						i_current_event++
//					ENDIF
//				ENDIF
//			BREAK
//			CASE 2
//				IF NOT IS_ENTITY_DEAD(gang_enemy[3].ped)
//					IF NOT bRpgGuyToldToMove
//						IF HAS_PED_PROCESSED_SEQ(gang_enemy[3].ped)
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,  << -1658.4519, 4499.8525, 4.6378 >>, PEDMOVE_RUN, -1, 0.5)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(gang_enemy[3].ped, seq)
//							CLEAR_SEQUENCE_TASK(Seq)
//							bRpgGuyToldToMove = TRUE
//						ENDIF
//					ENDIF
//					IF TIMERA() > 3000
//						IF NOT IS_ENTITY_DEAD(helicopter.veh)	
//							CLEAR_AREA(vRocketStart, 200.0, TRUE)
//							VECTOR vPedToHeli
//							vPedToHeli = (GET_ENTITY_COORDS(helicopter.veh) - vRocketStart)
//							vPedToHeliNorm = NORMALISE_VECTOR(vPedToHeli)
//							FLOAT fDist
//							fDist = VMAG(vPedToHeli)
//							vRpgRot = GET_ROTATION_FROM_DIRECTION_VECTOR(vPedToHeliNorm)
//							SET_ENTITY_COORDS(oRPG, vRocketStart + ((fDist * fRpgStartMul) * vPedToHeliNorm))
//							SET_ENTITY_ROTATION(oRPG, vRpgRot)
//							
//						ENDIF
//						fRpgSpeedMult = 18.0
//						SET_CAM_ACTIVE(cam_interp_helper, TRUE)	
//						SET_CAM_PARAMS(cam_interp_helper, << -1597.5243, 4347.9160, 23.7340 >>, << 22.1229, 0.0000, -49.4603 >>, 39.8408, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//						SET_CAM_ACTIVE(cam_cutscene, FALSE)
//						SHAKE_CAM(cam_interp_helper, "ROAD_VIBRATION_SHAKE", 0.35)
//						
//						IF NOT IS_PED_INJURED(selector_ped.pedID[SELECTOR_PED_TREVOR])
//							TASK_LOOK_AT_ENTITY(selector_ped.pedID[SELECTOR_PED_TREVOR], oRPG, 9999999)
//						ENDIF
//						SETTIMERA(0)
//						i_current_event = 4
//					ENDIF
//				ENDIF 
//			BREAK
//			
//			CASE 3
//				IF GET_RPG_DISTANCE_COVERED() > 0.65
//
//					fRpgSpeedMult = 14.5
//					SET_CAM_ACTIVE(cam_cutscene, TRUE)	
//					SET_CAM_PARAMS(cam_cutscene, << -1572.1094, 4436.7847, 63.5839 >>, << 7.5650, -0.0041, -117.0162 >>, 25.4984)
//					SET_CAM_PARAMS(cam_cutscene, << -1567.8678, 4443.3979, 63.5834 >>, << 7.5650, -0.0041, -125.9522 >>, 25.4984, 4000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//					SET_CAM_ACTIVE(cam_interp_helper, FALSE)
//					SHAKE_CAM(cam_cutscene, "ROAD_VIBRATION_SHAKE", 0.5)
//					i_current_event++
//				ENDIF
//			BREAK
//			
//			CASE 4
//				//IF GET_RPG_DISTANCE_COVERED() > 0.85
//				IF TIMERA() > 2000
//					SET_RPG_DISTANCE_COVERED(0.85)
//					IF NOT IS_ENTITY_DEAD(helicopter.veh)
//						STOP_PLAYBACK_RECORDED_VEHICLE(helicopter.veh)
//						START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh,  7, str_carrec)
//					ENDIF
//					
//					SET_CAM_ACTIVE(cam_cutscene, TRUE)	
//					SET_CAM_PARAMS(cam_cutscene, << -1449.7739, 4378.0879, 98.4719 >>, << -17.4907, -0.0023, 75.6979 >>, 16.6489)
//					SET_CAM_PARAMS(cam_cutscene, << -1450.2456, 4376.3765, 98.4719 >>, << -17.4907, -0.0023, 71.7980 >>, 16.6489, 8000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//					SHAKE_CAM(cam_cutscene, "sky_diving_shake", 0.3)
//					SET_CAM_ACTIVE(cam_interp_helper, FALSE)
//					IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_ROCKET", CONV_PRIORITY_MEDIUM)
//					ENDIF
//					i_current_event++
//				ENDIF
//			BREAK
//			
//			CASE 5
//				IF GET_RPG_DISTANCE_COVERED() > 0.93
//					IF NOT IS_ENTITY_DEAD(helicopter.veh)
//						//-- pulls up at 5500
//						FLOAT f
//						f = GET_TIME_POSITION_IN_RECORDING(helicopter.veh)
//						IF f < 5500
//							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, 5500.0 - f)
//						ENDIF
//						fRpgSpeedMult = 15.0
//				/*		SET_CAM_ACTIVE(cam_cutscene, TRUE)	
//						SET_CAM_PARAMS(cam_cutscene, << -1484.5222, 4356.9165, 96.3961 >>, << -7.6120, -0.0187, -19.6093 >>, 28.7347)
//						SET_CAM_PARAMS(cam_cutscene, << -1484.5222, 4356.9165, 96.3961 >>, << 12.0769, -0.0182, -82.4351 >>, 28.7347, 6000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
//						*/
//						
///*
//						SET_CAM_ACTIVE(cam_cutscene, TRUE)	
//						SET_CAM_PARAMS(cam_cutscene, << -1495.0413, 4371.0435, 95.3287 >>, << -5.7435, -1.3733, -58.4955 >>, 28.7347)
//						SET_CAM_PARAMS(cam_cutscene, << -1495.0416, 4371.0425, 95.3288 >>, << 7.5147, -1.4295, -86.2792 >>, 28.7347, 4000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
//						*/ 
//						SET_CAM_ACTIVE(cam_interp_helper, TRUE)	
//						SET_CAM_PARAMS(cam_interp_helper, << -1496.9464, 4385.6792, 90.2245 >>, << 1.5556, -1.4551, -89.1424 >>, 28.7347)
//						SET_CAM_PARAMS(cam_interp_helper, << -1496.9464, 4385.6792, 90.2245 >>, << 11.8340, -1.4551, -107.9670 >>, 28.7347, 4500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
//						SET_CAM_ACTIVE(cam_cutscene, FALSE)	
//						//STOP_CAM_SHAKING(cam_interp_helper) 
//						SHAKE_CAM(cam_interp_helper, "sky_diving_shake", 0.3)
//						KILL_ANY_CONVERSATION()
//						SETTIMERA(0)
//						i_current_event++
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 6
//				IF TIMERA() >= 4500
//					e_section_stage = SECTION_STAGE_CLEANUP
//				ELIF TIMERA() > 2500
//					IF DOES_ENTITY_EXIST(oRPG)
//						ADD_EXPLOSION(GET_ENTITY_COORDS(oRPG), EXP_TAG_ROCKET, 1.0)
//						STOP_PARTICLE_FX_LOOPED(fxRpg)
//						DELETE_OBJECT(oRPG)
//					ENDIF
//				ENDIF
//			BREAK
//		ENDSWITCH
//		
//		IF DOES_ENTITY_EXIST(oRPG)
//			IF i_current_event > 2
//				VECTOR vRpgPos
//				vRpgPos = GET_ENTITY_COORDS(oRPG)
//				vRpgPos += ( vPedToHeliNorm * GET_FRAME_TIME() * fRpgSpeedMult)
//				SET_ENTITY_COORDS(oRPG, vRpgPos)
//			ENDIF
//		ENDIF
//		
//		IF NOT IS_ENTITY_DEAD(helicopter.veh)
//			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(helicopter.veh)
//				REQUEST_VEHICLE_RECORDING(6, str_carrec)
//				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(6, str_carrec)
//					START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, 6, str_carrec)
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
//			e_section_stage = SECTION_STAGE_CLEANUP
//		ENDIF
//		
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
//			IF TIMERB() > 1000
//				e_section_stage = SECTION_STAGE_SKIP
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF e_section_stage = SECTION_STAGE_CLEANUP
//		STOP_PARTICLE_FX_LOOPED(fxRpg)
//		IF DOES_ENTITY_EXIST(oRPG)
//			DELETE_OBJECT(oRPG)
//		ENDIF
//		IF NOT IS_ENTITY_DEAD(helicopter.veh)
//			STOP_PLAYBACK_RECORDED_VEHICLE(helicopter.veh)
//			//START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, 8, str_carrec) 
//			//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, 4000)
//			
//			
//			dummy_helicopter.veh = create_vehicle(dummy_helicopter.model, dummy_helicopter.pos, dummy_helicopter.heading)
//			set_vehicle_engine_on(dummy_helicopter.veh, false, false)
//			set_entity_collision(dummy_helicopter.veh, false)
//			set_entity_visible(dummy_helicopter.veh, false)
//			START_PLAYBACK_RECORDED_VEHICLE(dummy_helicopter.veh, 8, str_carrec)
//			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(dummy_helicopter.veh, 4000)
//			set_entity_proofs(dummy_helicopter.veh, true, true, true, true, true)
//			
//		ENDIF
//		
//		if not is_ped_injured(gang_enemy[3].ped)
//			set_entity_proofs(gang_enemy[3].ped, false, false, false, false, false)
//		endif 
//		
//		if is_screen_faded_in()
//			end_cutscene_no_fade(false)
//		else 
//			end_cutscene(false)
//		endif 
//		
//		e_section_stage = SECTION_STAGE_SETUP
//		mission_flow = STAGE_PGM_BULLETS
//	ENDIF
//	
//	IF e_section_stage = SECTION_STAGE_SKIP
//		DO_SAFE_FADE_OUT()
//		
//		e_section_stage = SECTION_STAGE_CLEANUP
//	ENDIF
//ENDPROC

OBJECT_INDEX oFakeRocket
PTFX_ID fxFakeRocket
INT iFakeRocketPRog
PROC FIRE_FAKE_ROCKET(VECTOR vStartPos, VECTOR vDir, FLOAT fDistanceToTravel = 200.0, FLOAT fSpeedMult = 10.0)
	VECTOR vDirn = NORMALISE_VECTOR(vDir - vStartPos)
	SWITCH iFakeRocketPRog
		CASE 0
			oFakeRocket = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG), vStartPos)
			SET_ENTITY_ROTATION(oFakeRocket , GET_ROTATION_FROM_DIRECTION_VECTOR(vDirn))
			fxFakeRocket = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ex2_rpg_trail", oFakeRocket, <<0.0, 0.0, 0.0>>,  <<0.0, 0.0, 0.0>>)
			//SCRIPT_AsSERT("Launched")
			iFakeRocketPRog++
		BREAK
		
		CASE 1
			VECTOR vRocketPos
			vRocketPos = GET_ENTITY_COORDS(oFakeRocket)
			IF VMAG(vRocketPos- vStartPos) <= fDistanceToTravel
			
			
				vRocketPos += ( vDirn * GET_FRAME_TIME() * fSpeedMult)
				SET_ENTITY_COORDS(oFakeRocket, vRocketPos)
			ELSE 
				iFakeRocketPRog++
			ENDIF
		BREAK
		
		CASE 2
			IF DOES_ENTITY_EXIST(oFakeRocket)
				ADD_EXPLOSION(GET_ENTITY_COORDS(oFakeRocket), EXP_TAG_ROCKET, 1.0)
				STOP_PARTICLE_FX_LOOPED(fxFakeRocket)
				DELETE_OBJECT(oFakeRocket)
				iFakeRocketPRog++
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


//func bool pgm_bullets_master_flow_system()
//
//	switch pgm_bullets_master_flow_system_status 
//	
//		case 0
//		
//			IF is_vehicle_driveable(dummy_helicopter.veh)
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(dummy_helicopter.veh)
//
//					IF GET_TIME_POSITION_IN_RECORDING(dummy_helicopter.veh) > 27000
//						IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_USE", CONV_PRIORITY_MEDIUM)
//							GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PROGRAMMABLEAR, 50, FALSE)
//							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PROGRAMMABLEAR, TRUE)
//							
//							SETTIMERB(0)
//							
//							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "shooting the rpg enemy")  
//							
//							pgm_bullets_master_flow_system_status++
//							
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF IS_PED_INJURED(gang_enemy[3].ped)
//				KILL_ANY_CONVERSATION()
//				pgm_bullets_master_flow_system_status = 2
//			endif 
//			
//		break 
//		
//		case 1
//		
//			IF IS_PED_INJURED(gang_enemy[3].ped)
//				KILL_ANY_CONVERSATION()
//				pgm_bullets_master_flow_system_status++
//			endif 
//		
//		break 
//		
//		case 2
//		
//			IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_DEAD", CONV_PRIORITY_HIGH)
//				pgm_bullets_master_flow_system_status++
//			ENDIF
//		
//		break 
//		
//		case 3
//		
//			IF not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			
//				set_ped_keep_task(selector_ped.pedID[selector_ped_trevor], true)
//				task_heli_mission(selector_ped.pedID[selector_ped_trevor], helicopter.veh, null, null, <<1800, 3772.22, 150.22>>, mission_goto, 20, -1, -1, 150, 70)
//					
//				make_selector_ped_selection(selector_ped, SELECTOR_PED_FRANKLIN)
//			
//				TAKE_CONTROL_OF_SELECTOR_PED(selector_ped)
//					
//				MISSION_PASSED()
//		
//			ENDIF 
//
//		break 
//		
//	endswitch 
//	
//	return false 
//
//endfunc

proc dialogue_and_help_text_system()

	switch dialogue_and_help_text_system_status 
	
		case 0
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_FLY", CONV_PRIORITY_MEDIUM)
					original_time = get_game_timer()
					dialogue_and_help_text_system_status++
				ENDIF
			endif 
			
		break 
		
		case 1
		
			if lk_timer(original_time, 2000)
				PRINT_HELP("FS_USEPGM")
				dialogue_and_help_text_system_status++
			endif 
		
		break 
		
		case 2
		
		break 
		
		case 3
		
		break 
		
	endswitch 

endproc

//proc fire_fake_rocket_system()
//		
//	IF NOT IS_ENTITY_DEAD(dummy_helicopter.veh)
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(dummy_helicopter.veh)
//			IF GET_TIME_POSITION_IN_RECORDING(dummy_helicopter.veh) > 15000
//				FIRE_FAKE_ROCKET(<<-1648.0480, 4492.5757, 9.5435>>, get_offset_from_entity_in_world_coords(helicopter.veh, <<-3.0, 10.0, -10.0>>), 200, 20.0)
//			ENDIF
//		ENDIF
//		IF NOT HAS_LABEL_BEEN_TRIGGERED("TM44_INC")
//			IF DOES_ENTITY_EXIST(oFakeRocket)
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(oFakeRocket), GET_ENTITY_COORDS(dummy_helicopter.veh)) < 50.0
//					IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_INC", CONV_PRIORITY_MEDIUM)
//						SET_LABEL_AS_TRIGGERED("TM44_INC", TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//endproc 

//PROC PGM_BULLETS()
//
//	disable_control_action(PLAYER_CONTROL, INPUT_JUMP)
//	disable_control_action(PLAYER_CONTROL, INPUT_DUCK)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
//
//	ATTACH_ENTITY_TO_ENTITY(player_ped_id(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
//
//	IF e_section_stage = SECTION_STAGE_SETUP
//			
//		IF CREATE_CONVERSATION (scripted_speech, "TM44AUD", "TM44_CLOSE", CONV_PRIORITY_MEDIUM)
//		ENDIF
//		
//		IF NOT IS_PED_INJURED(gang_enemy[3].ped)
//			CLEAR_PED_TASKS_IMMEDIATELY(gang_enemy[3].ped)
//			SET_ENTITY_COORDS(gang_enemy[3].ped,  << -1604.9904, 4514.2798, 15.2442 >>)
//			SET_ENTITY_PROOFS(gang_enemy[3].ped, FALSE, FALSE, FALSE, FALSE, FALSE)
//			TASK_TOGGLE_DUCK(gang_enemy[3].ped, TOGGLE_DUCK_ON)
//		ENDIF
//		
//		IF bDoMissionPassedScreen
//			IF NOT IS_ENTITY_DEAD(gang_car.veh)
//
//				VECTOR vPos = GET_ENTITY_COORDS(gang_car.veh)
//				FLOAT fHead = GET_ENTITY_HEADING(gang_car.veh)
//				DELETE_VEHICLE(gang_car.veh)
//				gang_car.veh = CREATE_VEHICLE(DUBSTA, vPos, fHead+180.0)
//				SET_VEHICLE_COLOURS(gang_car.veh, 0, 0)
//			//	BLIP_INDEX blipTemp = ADD_BLIP_FOR_ENTITY(franklins_car.veh)
//			//	SET_VEHICLE_COLOURS(franklins_car.veh, 12, 12)
//			ENDIF
//		ENDIF
//		
//		SET_ROADS_IN_AREA(vCountryRoadMin, vCountryRoadMax, FALSE)
//		
//		SET_LABEL_AS_TRIGGERED("FS_SNIPEHELP",FALSE)
//		SET_LABEL_AS_TRIGGERED("TM44_USE", FALSE)
//		UNPAUSE_TIMER(count_down_timer)
//		bPlayingHoverBackwards = FALSE
//		b_thermal_active = FALSE
//		stop_thermal_help = false 
//		
//		e_section_stage = SECTION_STAGE_RUNNING
//		manage_gang_enemy_3_status = 2
//		iFakeRocketPRog = 0
//		fHoverPlaybackSpeed = CHOPPER_HOVER_SPEED
//		i_current_event = 0
//		
//		helicopter_cam_status = 0
//	ENDIF
//
//	IF e_section_stage = SECTION_STAGE_RUNNING
//	
//		float playback_time 
//	
//		if DOES_ENTITY_EXIST(dummy_helicopter.veh)
//			if is_vehicle_driveable(dummy_helicopter.veh)
//				if is_playback_going_on_for_vehicle(dummy_helicopter.veh)
//					playback_time = get_time_position_in_recording(dummy_helicopter.veh)
////					printfloat(playback_time)
////					printnl()
//				endif 
//			endif 
//		endif 
//		
//		if pgm_bullets_master_flow_system()
//			e_section_stage = SECTION_STAGE_CLEANUP
//		endif 
//		
//		dialogue_and_help_text_system()
//
//		helicopter_camera_system()
//		
//		thermal_vision_system()
//		
//		countdown_timer_system()
//		
//		MAINTAIN_CHOPPER_HOVER(dummy_helicopter.veh, (get_total_duration_of_vehicle_recording(8, str_carrec) - 8000), (get_total_duration_of_vehicle_recording(8, str_carrec) - 500))
//		
//		helicopter_flying_system()
//		
//		MANAGE_ENEMY_PEDS_ON_GROUND()
//		
//		fire_fake_rocket_system() //fires one fake rocket
//		
//	ENDIF
//	
//	IF e_section_stage = SECTION_STAGE_SKIP
//		IF NOT IS_ENTITY_DEAD(helicopter.veh)
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(helicopter.veh)
//				FLOAT fCurTime = GET_TIME_POSITION_IN_RECORDING(helicopter.veh)
//				IF fCurTime < 45000.0
//					KILL_ANY_CONVERSATION()
//					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, 45000.0 - fCurTime)
//				ELSE
//					IF NOT IS_PED_INJURED(gang_enemy[3].ped)
//						SET_ENTITY_HEALTH(gang_enemy[3].ped, 5)
//					ENDIF
//
//				ENDIF
//				
//			ENDIF
//		ENDIF
//		e_section_stage = SECTION_STAGE_RUNNING
//	ENDIF
//ENDPROC
	
proc setup_mission_fail()
	IF mission_flow != mission_fail_stage 
		// only do the fail setup if we've not already failed the mission
	
		remove_all_blips()

		if does_entity_exist(helicopter.veh)
			if is_vehicle_driveable(helicopter.veh)
				set_entity_health(helicopter.veh, 1000)
				set_vehicle_petrol_tank_health(helicopter.veh, 1000.00)
				set_vehicle_engine_health(helicopter.veh, 1000.00)
				set_entity_proofs(helicopter.veh, true, true, true, true, true)
			endif 
		endif 
		
		CLEAR_FOCUS()
								
		SET_SEETHROUGH(FALSE)
		
		if IS_AUDIO_SCENE_ACTIVE("EXILE_2_SNIPE_STAGE_THERMAL")
			stop_AUDIO_SCENE("EXILE_2_SNIPE_STAGE_THERMAL")
		endif 
		
		clear_prints()
		clear_help()
		KILL_ANY_CONVERSATION()

		//mission_flow_cached = mission_flow

		//only allow helicopter camera system to run when you are in these stages before a fail.
		if mission_flow = STAGE_SNIPE_ENEMIES
		or mission_flow = stage_mission_pass_conversation
		or mission_flow = stage_pickup_franklin_and_chop
			allow_helicopter_camera_system_to_run_during_fail = true
		endif 

		stop_mission_fail_checks = true 
		
		trigger_music_event("EXL2_MISSION_FAIL")
		
		MISSION_FLOW_MISSION_FAILED_WITH_REASON(mission_failed_text) 
		
		mission_flow = mission_fail_stage 
	ENDIF
endproc 

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Records the trigger car and all of the traffic
	PROC RECORD_CHASE_TRIGGER_AND_TRAFFIC()
		IF e_section_stage = SECTION_STAGE_SETUP
			MODEL_NAMES model_trigger_car = DUBSTA
			
			vector v_gang_car_start_pos
			  
			REQUEST_MODEL(model_trigger_car)
			v_gang_car_start_pos = << -2463.4102, 3687.8987, 13.3021 >>
			IF HAS_MODEL_LOADED(model_trigger_car)
				gang_car.veh = CREATE_VEHICLE(model_trigger_car, v_gang_car_start_pos,  3.0575)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_trigger_car)
//				SET_CLOCK_TIME(9,0,0)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), gang_car.veh)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), gang_car.veh)
				ENDIF
				str_carrec = "Exile2DW"
				INIT_UBER_RECORDING(str_carrec)
				
				e_section_stage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
		
		IF e_section_stage = SECTION_STAGE_RUNNING
			UPDATE_UBER_RECORDING()
		ENDIF
	ENDPROC
	
	PROC SKIP_TO_STAGE(MISSION_STAGE stage)
		DO_SAFE_FADE_OUT()
		e_p_skip_stage = stage
		b_is_p_skipping = TRUE
		initialise_mission_variables()
		MISSION_CLEANUP()
	ENDPROC
	
	/// PURPOSE:
	///    J-skips through each stage until the desired stage is reached
	PROC HANDLE_P_SKIP()
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF mission_flow >= e_p_skip_stage
				b_is_p_skipping = FALSE
				DO_SAFE_FADE_IN()
			ELSE
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ELSE
			//If skipped last frame, change stage so the mission has a chance to update before skipping again.
			IF e_section_stage = SECTION_STAGE_SKIP
				e_section_stage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
	ENDPROC
	
	INT iWdGenForProg
	BOOL bWdSetTrafficDensity
	BOOL bWdTakeControlOfHeli
	BOOL bWdBlipEnemyPeds
	BOOL bWdForceThermal
	BOOL bWdDetachPed
	BOOL bKillNonRpgPeds
	FLOAT fWdVehicleDens
	PROC DO_FOREST_GENERAL_WIDGET()
		INT i
		SWITCH iWdGenForProg
			CASE 0
				SET_CURRENT_WIDGET_GROUP(widget_debug)
					START_WIDGET_GROUP("Forest general Widget")	
//						ADD_WIDGET_BOOL("Use debug", bDwUseDebug)
						ADD_WIDGET_FLOAT_SLIDER("Veh density", fWdVehicleDens, 0.0, 10.0, 0.1)
						ADD_WIDGET_BOOL("Set density", bWdSetTrafficDensity)
						ADD_WIDGET_BOOL("Take control of Heli", bWdTakeControlOfHeli)
						ADD_WIDGET_BOOL("Blip gang_enemy peds", bWdBlipEnemyPeds)
						ADD_WIDGET_BOOL("Force thermal on", bWdForceThermal)
						ADD_WIDGET_BOOL("RPG run out", bWdTestRpgGuyRunOut)
						ADD_WIDGET_BOOL("Detach player", bWdDetachPed)
						ADD_WIDGET_BOOL("Kill non-rpg peds" ,bKillNonRpgPeds)
					STOP_WIDGET_GROUP()	
				CLEAR_CURRENT_WIDGET_GROUP(widget_debug)
				iWdGenForProg++
			BREAK
			
			CASE 1
				IF bWdSetTrafficDensity
					//SET_VEHICLE_DENSITY_MULTIPLIER(fWdVehicleDens)
				ENDIF
				
				IF bWdTakeControlOfHeli
					bWdTakeControlOfHeli = FALSE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						selector_ped.eCurrentSelectorPed = SELECTOR_PED_MICHAEL
						selector_ped.eNewSelectorPed = SELECTOR_PED_TREVOR
						TAKE_CONTROL_OF_SELECTOR_PED(selector_ped)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF NOT IS_ENTITY_DEAD(helicopter.veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(helicopter.veh)
						ENDIF
						mission_flow = STAGE_DONE
					ELSE
						SCRIPT_ASSERT("Player not Michael!")
					ENDIF
				ENDIF
				
				IF bWdBlipEnemyPeds
					bWdBlipEnemyPeds = FALSE
					REPEAT COUNT_OF(gang_enemy) i
						IF NOT IS_ENTITY_DEAD(gang_enemy[i].ped)
							IF NOT DOES_BLIP_EXIST(gang_enemy[i].blip)
								gang_enemy[i].blip = ADD_BLIP_FOR_ENTITY(gang_enemy[i].ped)
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF	
				
				IF bWdDetachPed
					bWdDetachPed = FALSE
					DETACH_ENTITY(PLAYER_PED_ID())
				ENDIF
				
				IF bWdForceThermal
					SET_SEETHROUGH(TRUE)
				ENDIF
				
				IF bKillNonRpgPeds
					bKillNonRpgPeds = FALSE
					FOR i = 0 TO 2
						IF NOT IS_PED_INJURED(gang_enemy[i].ped)
							SET_ENTITY_HEALTH(gang_enemy[i].ped, 5)
						ENDIF
					ENDFOR
				ENDIF
			BREAK
		ENDSWITCH	
		
	ENDPROC
	
	PROC UPDATE_WIDGETS()
		
		Maintain_Hud_Creator_Tool(widget_debug)
		
		DO_FOREST_GENERAL_WIDGET()
		
	ENDPROC

	PROC DO_DEBUG()
		
		UPDATE_WIDGETS()
	
		IF b_is_p_skipping
			HANDLE_P_SKIP()
		ENDIF
		
		IF NOT b_is_p_skipping
			//Reset any skipping from the previous frame
			
			IF e_section_stage = SECTION_STAGE_SKIP
				e_section_stage = SECTION_STAGE_RUNNING
			ENDIF
		
			IF e_section_stage = SECTION_STAGE_RUNNING
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					e_section_stage = SECTION_STAGE_SKIP
				ENDIF
				
			ENDIF
			
		endif 
			
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
//			MISSION_PASSED()
//		endif 
//		
//		if IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
//			setup_mission_fail()
//		ENDIF

	ENDPROC
#ENDIF

/// PURPOSE:
///    if replay is rejected we respawn as Trevor at his trailer (bug 986500)
PROC HANDLE_FAIL_AS_FRANKLIN()
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		
		switch GET_PLAYER_CHARACTER_AT_MISSION_START() 
		
			case char_michael 
			
				SET_REPLAY_REJECTED_CHARACTER(CHAR_MICHAEL)
			
			break 
			
			case char_franklin
			
				SET_REPLAY_REJECTED_CHARACTER(CHAR_TREVOR)
			
			break 
			
			case char_trevor
			
				SET_REPLAY_REJECTED_CHARACTER(CHAR_TREVOR)
			
			break 

		endswitch 
		
		CPRINTLN(DEBUG_REPLAY, "Exile 2 failed as Franklin. Will respawn as Trevor at trailer if rejected")
		
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<1980.3524, 3816.6270, 31.2685>>, 298.2293)
	ENDIF
	
ENDPROC

PROC HANDLE_FAIL_reposition()
	
	switch GET_PLAYER_CHARACTER_AT_MISSION_START() 
	
		case char_michael 
		
			SET_REPLAY_REJECTED_CHARACTER(CHAR_MICHAEL)
		
		break 
		
		case char_franklin
		
			SET_REPLAY_REJECTED_CHARACTER(CHAR_TREVOR) //misison can never start with the player being franklin before the mission has launched. As you are swithcing into franklin
		
		break 
		
		case char_trevor
		
			SET_REPLAY_REJECTED_CHARACTER(CHAR_TREVOR)
		
		break 

	endswitch 
	
	CPRINTLN(DEBUG_REPLAY, "Exile 2 failed as Franklin. Will respawn as Trevor at trailer if rejected")
	
	MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<1980.3524, 3816.6270, 31.2685>>, 298.2293)
	
ENDPROC

proc mission_fail_stage_system()
	
	if allow_helicopter_camera_system_to_run_during_fail
		helicopter_camera_system()
	endif 

	if is_vehicle_driveable(helicopter.veh)
		
		if get_current_player_ped_enum() = char_michael
			ATTACH_ENTITY_TO_ENTITY(player_ped_id(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
		endif 
		
	endif 
	
	if does_entity_exist(gang_car.veh)
		if is_vehicle_driveable(gang_car.veh)
			if is_playback_going_on_for_vehicle(gang_car.veh)
				update_uber_playback(gang_car.veh, 1.0)
			endif 
		endif 
	endif 
	
	if GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		
//		//depending on what stage you were when you failed position the player / vehicle for this
//		switch mission_flow_cached
//		
//			case stage_get_to_mountains
//			
//			break 
//			
//			case STAGE_SNIPE_ENEMIES
//			
//				switch get_current_Player_ped_enum()
//				
//					case char_michael 
//					
//						MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<1980.3524, 3816.6270, 31.2685>>, 298.2293)
//					
//					break 
//					
//					case char_franklin
//					
//					break 
//					
//				endswitch 
//			
//			break 
//			
//		endswitch 
		
		// check if we need to respawn the player in a different position, 
		// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
		
		//HANDLE_FAIL_AS_FRANKLIN()
		
		HANDLE_FAIL_reposition()
	
		//RESTORE_PLAYER_PED_VARIATIONS(player_ped_id())

		//remove_all_mission_assets()
		
		delete_all_mission_assets_for_fail()

		MISSION_CLEANUP() // must only take 1 frame and terminate the thread

	endif 

endproc 

proc get_to_mountains_dialogue_system()

	//if not does_cam_exist(selector_cam.camID)
	if not is_player_switch_in_progress()
	and eSwitchCamState = SWITCH_CAM_IDLE

		switch get_current_player_ped_enum()
			
			case char_franklin
			
				switch franklin_get_to_mountains_dialogue_system_status 
				
					case 0
					
						//if create_conversation(scripted_speech, "TM44AUD", "TM44_SWIT", conv_priority_medium)
							franklin_get_to_mountains_dialogue_system_status = 2
						//endif 
					
					break 
				
					case 1
					
//						//if not is_any_text_being_displayed(locates_data)
//						if create_conversation(scripted_speech, "TM44AUD", "TM44_SFRANK", conv_priority_medium)
//							franklin_get_to_mountains_dialogue_system_status++
//						endif 

					break 
					
					case 2
					
						if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
							
							add_ped_for_dialogue(scripted_speech, 0, null, "michael")
							add_ped_for_dialogue(scripted_speech, 2, null, "trevor")
							add_ped_for_dialogue(scripted_speech, 5, null, "chop")
							
							franklin_get_to_mountains_dialogue_system_status++
							
						endif 

					break 
					
					case 3
					
						//if not is_any_text_being_displayed(locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
							if create_conversation(scripted_speech, "TM44AUD", "TM44_BARKS", conv_priority_medium)
								chop_audio_system(chop_finds_enemy)
								franklin_get_to_mountains_dialogue_system_status++
							endif 
						endif 
					
					break 
					
					case 4
					
						chop_audio_system(chop_finds_enemy)
					
						if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
							if create_conversation(scripted_speech, "TM44AUD", "TM44_TALKC", conv_priority_medium)
								franklin_get_to_mountains_dialogue_system_status++
							endif 
						endif 
					
					break 
					
					case 5
					
						if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
							if PLAYER_CALL_CHAR_CELLPHONE(scripted_speech, char_trevor, "TM44AUD", "TM44_TCALL", CONV_PRIORITY_high) //TM44_CALL
								START_AUDIO_SCENE("EXILE_2_FOLLOW_CAR_PHONE")
								current_line = -1
								
								REPLAY_RECORD_BACK_FOR_TIME(1.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
								
								franklin_get_to_mountains_dialogue_system_status++
							endif 
						endif 
					
					break 
					
					case 6
					
						if not HAS_CELLPHONE_CALL_FINISHED()
							
							if get_CURRENT_SCRIPTED_CONVERSATION_LINE() <> -1
								current_line = get_CURRENT_SCRIPTED_CONVERSATION_LINE()
							endif 
							
							if current_line = 11
								if not IS_ANIMAL_VOCALIZATION_PLAYING(chop.ped)
									PLAY_ANIMAL_VOCALIZATION(chop.ped, AUD_ANIMAL_DOG_ROTTWEILER, "BARK_SEQ")
								endif 
							endif 		
						
						else 
						
							if IS_AUDIO_SCENE_ACTIVE("EXILE_2_FOLLOW_CAR_PHONE")
								STOP_AUDIO_SCENE("EXILE_2_FOLLOW_CAR_PHONE")
							endif 

							franklin_get_to_mountains_dialogue_system_status++
						
						endif  
					
					break 
					
					case 7
					
					break 
					
				endswitch
			
			break 
			
			case char_trevor
			
				switch trevor_get_to_mountains_dialogue_system_status
				
					case 0
					
						if lk_timer(original_time, 5500)
			
							if create_conversation(scripted_speech, "TM44AUD", "TM44_WHERE", conv_priority_medium)
								REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0)
								set_label_as_triggered("TM44_WHERE", true)
								trevor_get_to_mountains_dialogue_system_status++
							endif 
							
						endif 
						
					break 
					
					case 1
					
						
					
					break 
					
				endswitch 
				
			break 
			
		endswitch 
		
	ENDIF 
	
endproc 

proc switch_system()

	switch switch_system_status 
	
		case 0

			if allow_switch
			
				if update_selector_hud(selector_ped)
				
					switch get_current_player_ped_enum()
					
						case char_trevor
					
							if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_FRaNKLIN)
							
								kill_any_conversation()
								
								selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]

								clear_prints()
								clear_mission_locate_stuff(locates_data)
								
								if does_entity_exist(helicopter.veh)
									set_entity_proofs(helicopter.veh, true, true, true, true, true)
								endif 
								
								//trevors_current_car = null //might not need this null
								get_current_player_vehicle(trevors_current_car)
								
								SET_FRONTEND_RADIO_ACTIVE(FALSE)

								switch_system_status++
							endif 
							
						break 
						
						case char_franklin
						
							if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_trevor)
								
								selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_TREVOR]
								
								clear_prints()
								
								if does_blip_exist(gang_car.blip)
									remove_blip(gang_car.blip)
								endif
								
								if does_blip_exist(franklins_car.blip)
									remove_blip(franklins_car.blip)
								endif 
								
								set_entity_proofs(franklins_car.veh, true, true, true, true, true)
								
								SET_FRONTEND_RADIO_ACTIVE(FALSE)

								switch_system_status++
							
							endif 
						
						break 
						
					endswitch 
					
				endif 
			
			endif 
			
		break 
		
		case 1
		
			//IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(selector_cam)	// Returns FALSE when the camera spline is complete
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(selector_cam, switch_type_auto, 0, 999, 999, true)
			IF not HANDLE_SWITCH_CAM_TREVOR_MICHAEL_TO_BROS_AND_FRANKLIN(scsSwitchCamTrevorMichael, scsSwitchCamBrosToFranklin)

			else 
			
				BLOCK_MISSION_TITLE(false) //makes the predator mission title appear. Bug 1559497 
				
				remove_anim_dict("missswitch")
			
//				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())

				SET_FRONTEND_RADIO_ACTIVE(TRUE)

				switch get_current_player_ped_enum()

					case char_franklin
					
						set_ped_config_flag(player_ped_id(), PCF_ForceDirectEntry, true)
					
						START_AUDIO_SCENE("EXILE_2_FOLLOW_CAR")
					
						//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor, true)
						
						if is_playback_going_on_for_vehicle(franklins_car.veh)
							stop_playback_recorded_vehicle(franklins_car.veh)
						endif
						
						set_entity_proofs(franklins_car.veh, false, false, false, false, false)
						
						uber_chase_time = get_game_timer() //force playback speed to 1.1 for 2 seconds in 

						switch_system_status = 0
						
						allow_switch = false
						
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "start of car chase") 

					break 
					
				endswitch 

			endif 
		
		break 
		
		case 2
		
			if lk_timer(original_time, 500)
			
				if does_entity_exist(trevors_current_car)
					if is_vehicle_driveable(trevors_current_car)
						if is_playback_going_on_for_vehicle(trevors_current_car)
							stop_playback_recorded_vehicle(trevors_current_car)
						endif 
					endif 
				endif 
				
			endif 
		
		break 
		
	endswitch 			
		
endproc

proc uber_chase_system()

	if is_vehicle_driveable(gang_car.veh)
		if is_playback_going_on_for_vehicle(gang_car.veh)
		
			bCreateAllWaitingCars = true

			//calculate_new_playback_speed_from_char(gang_car.veh, PLAYER_ped_ID(), gang_car.speed, 1.0, 10.0, 18.0, 35.0, 2.0, 1.0, 0.65) //0.7
			
			printstring("uber_chase_system 1")
			printnl()
			
			switch uber_chase_status 
			
				case 0

					if set_uber_speed_to_zero
						
						update_uber_playback(gang_car.veh, 0.0)
						
						if is_vehicle_driveable(franklins_car.veh)
							if is_playback_going_on_for_vehicle(franklins_car.veh)
								set_playback_speed(franklins_car.veh, 0.0) 
							endif 
						endif 
						
					else 
						
						update_uber_playback(gang_car.veh, 1.0)
						
						if is_vehicle_driveable(franklins_car.veh)
							if is_playback_going_on_for_vehicle(franklins_car.veh)
								set_playback_speed(franklins_car.veh, 1.0) 
							endif 
						endif 
						
					endif 
					
					bCreateAllWaitingCars = true
					
					//forces the uber to go fast for 2000 ms
					if uber_chase_time != 0 //set to cpu time when the switch is complete or replay launched
						if lk_timer(uber_chase_time, 2000)
							disired_playback_speed = 1.1
							gang_car.speed = 1.1
							uber_chase_status++
						endif 
					endif 

				break 
				
				case 1
				
					uber_acceleration_multiplier = 0.01
					
					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, franklins_car.veh, gang_car.veh, 10.0, 18.0, 35.0, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					
					//helps accelerate the gang car when the player starts to get close to them from being a large distance away
					IF gang_car.speed < 1.0
					AND disired_playback_speed > gang_car.speed
						uber_acceleration_multiplier *= 2.0
					ENDIF
					
					//franklins special ability slows all the other cars down and allows franklin to maintain his
					//current velocity. Increasing the multiplier will speed the smoothing up.  bug 1977004  
					if is_special_ability_active(player_id())
						uber_acceleration_multiplier *= 2.0
					endif 
					
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(gang_car.speed, disired_playback_speed, uber_acceleration_multiplier)
				
					update_uber_playback(gang_car.veh, gang_car.speed)
				
				break 
				
			endswitch 
			
//			if is_vehicle_driveable(franklins_car.veh)
//				if is_playback_going_on_for_vehicle(franklins_car.veh)
//					set_playback_speed(franklins_car.veh, gang_car.speed) 
//				endif 
//			endif 
					
					//temp use when recording chase
//					update_uber_playback(gang_car.veh, 1.0)
					
					//set_entity_collision(gang_car.veh, false)
					
//					DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(gang_car.veh, <<-15.00, 10, 0.0>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.0, 1.0, 1.0>>, 255, 0, 128, 178)
//					DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(gang_car.veh, <<-10.00, 10, 0.0>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.0, 1.0, 1.0>>, 255, 0, 128, 178)
//					DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(gang_car.veh, <<-5.00, 10, 0.0>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.0, 1.0, 1.0>>, 255, 0, 128, 178)
//					DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(gang_car.veh, <<0.00, 10, 0.0>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.0, 1.0, 1.0>>, 255, 0, 128, 178)
//					DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(gang_car.veh, <<5.00, 10, 0.0>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.0, 1.0, 1.0>>, 255, 0, 128, 178)
//					DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(gang_car.veh, <<10.00, 10, 0.0>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.0, 1.0, 1.0>>, 255, 0, 128, 178)
//					DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(gang_car.veh, <<15.00, 10, 0.0>>), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.0, 1.0, 1.0>>, 255, 0, 128, 178)
					
//					printstring("test 0")
//					printnl()

			
			//**PUT BACK IN
			
			//if has_entity_been_damaged_by_entity(gang_car.veh, player_ped_id())
			if is_bullet_in_area(get_entity_coords(gang_car.veh), 1.7)
			//if get_time_position_in_recording(gang_car.veh) > 30000
				if not is_ped_injured(gang_enemy[2].ped)
				
					if has_ped_task_finished_2(gang_enemy[2].ped)
					
						set_ped_accuracy(gang_enemy[2].ped, 5)
						
						open_sequence_task(seq)
							TASK_DRIVE_BY(null, player_ped_id(), null, <<0.0, 0.0, 2.0>>, 300.00, 20)
						close_sequence_task(seq)
						task_perform_sequence(gang_enemy[2].ped, seq)
						clear_sequence_task(seq)
						
					endif 
					
				endif 
			
			endif 
			
		endif 
	endif 

endproc

proc gang_car_blipping_system()				
		
	IS_PLAYER_AT_LOCATION_IN_VEHICLE(locates_data, get_entity_coords(gang_car.veh), <<0.01, 0.01, 0.01>>, false, franklins_car.veh, "FS_CHASE", "", "FS_car", false, 0, false)  

	if does_blip_exist(locates_data.LocationBlip)

		set_blip_alpha(locates_data.LocationBlip, 0)
		set_blip_route(locates_data.LocationBlip, false)
		
		if not does_blip_exist(gang_car.blip)
			gang_car.blip = create_blip_for_vehicle(gang_car.veh, true)
		endif 
		
	else 
	
		if does_blip_exist(gang_car.blip)
			remove_blip(gang_car.blip)
		endif
	
	endif 
	
endproc 

func bool get_to_mountains_master_flow_system()
		
	switch get_to_mountains_master_flow_system_status 
	
		case 0
		
			is_player_at_location_with_buddies_in_vehicle(locates_data, <<-2769.7, 220.4, 24.7>>, <<0.01, 0.01, 0.01>>, false, selector_ped.pedID[selector_ped_michael], null, null, trevors_truck.veh, trevor_go_to_instruction_text, trevor_pickup_buddy_text, "", "", "", "", trevor_get_back_in_veh_text)  //trevor_go_to_instruction_text

			if has_label_been_triggered("TM44_WHERE")
			
				if switch_to_franklin_time = 0
					if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() and get_CURRENT_SCRIPTED_CONVERSATION_LINE() = 1
						switch_to_franklin_time = get_game_timer()
					endif 
				endif 

				if not is_any_text_being_displayed(locates_data)
				or (IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() and get_CURRENT_SCRIPTED_CONVERSATION_LINE() = 1 and lk_timer(switch_to_franklin_time, 2500))

					make_selector_ped_selection(selector_ped, SELECTOR_PED_FRANKLIN)
					selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]
					switch_system_status = 1
					
					eSwitchCamState = SWITCH_CAM_START_CODE_SWITCH
					
					allow_switch = true
				
					clear_mission_locate_stuff(locates_data, true)
					
					get_to_mountains_master_flow_system_status++
				
				endif 
				
			endif 
		
		break 
		
		case 1
		
			if not IS_PLAYER_SWITCH_IN_PROGRESS()
			and eSwitchCamState = SWITCH_CAM_IDLE //HANDLE_SWITCH_CAM_TREVOR_MICHAEL_TO_BROS_AND_FRANKLIN has finished
				
//				printstring("get_to_mountains 2")
//				printnl()
				
				switch get_current_player_ped_enum()
		
					case char_franklin
					
						gang_car_blipping_system()
						
						if not replay_video_recorder_0_activated
							REPLAY_RECORD_BACK_FOR_TIME(4)
							replay_video_recorder_0_activated = true
						endif 
								

						if get_time_position_in_recording(gang_car.veh) < 75400
							UPDATE_CHASE_BLIP(gang_car.blip, gang_car.veh, 200.00, 0.9) //flashes blip when almost failing
						else 
							UPDATE_CHASE_BLIP(gang_car.blip, gang_car.veh, 60.00, 0.9)
						endif 
				
						if is_playback_going_on_for_vehicle(gang_car.veh)
						
							if get_time_position_in_recording(gang_car.veh) > 72000
								request_model(animal[0].model)
								request_anim_dict("missexile2ig_5")
								request_anim_dict("missexile2deer_crash")
								REQUEST_VEHICLE_RECORDING(CARREC_CRASH, str_carrec)
								REQUEST_VEHICLE_RECORDING(CARREC_GANG, str_carrec)
								//LOAD_STREAM_WITH_START_OFFSET("CAR_CRASH_OFF_CLIFF_STREAM", 500, "EXILE_2_SOUNDS")
								load_stream("CAR_CRASH_OFF_CLIFF_STREAM", "EXILE_2_SOUNDS")
								prepare_music_event("EXL2_DEER")
								REQUEST_PTFX_ASSET() 

							endif 
						
//							is_entity_in_angled_area(player_ped_id(), <<-1946.126, 4473.044, 25.943>>, <<-1979.192, 4435.007, 46.443>>, 106.100) 
//							is_entity_in_angled_area(player_ped_id(),<<-1922.338, 4462.304, 25.943>>, <<-1960.324, 4418.606, 46.443>>, 40.400)

							if get_time_position_in_recording(gang_car.veh) > 85400
							
								SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

								if (does_blip_exist(gang_car.blip)
								and ((is_entity_in_angled_area(player_ped_id(), <<-1946.126, 4473.044, 25.943>>, <<-1979.192, 4435.007, 46.443>>, 106.100)) 
								or (is_entity_in_angled_area(player_ped_id(),<<-1914.003, 4463.934, 25.943>>, <<-1957.432, 4414.501, 46.443>>, 51.900)))
								and (not is_entity_in_angled_area(player_ped_id(), <<-1959.981, 4434.166, 25.943>>, <<-1968.885, 4423.886, 46.443>>, 19.200)))
								//and (players_heading > 150.0  and players_heading  < 328.00)

									set_ped_config_flag(player_ped_id(), PCF_ForceDirectEntry, false)
									
									//BLOCK PLAYER EXIT VEHICLE BUTTON HERE THEN ACTIVATE IN SETUP STAGE. 
									disable_control_action(PLAYER_CONTROL, INPUT_VEH_EXIT)
									
									clear_mission_locate_stuff(locates_data)
										
									cleanup_uber_playback()
									switch_off_vehicle_generators_for_mission()
									set_model_as_no_longer_needed(radi)
									set_model_as_no_longer_needed(sabregt)
									
									remove_vehicle_recording(001, "lkexcile2_chase")
									remove_vehicle_recording(002, "lkexcile2_chase")
									remove_vehicle_recording(003, "lkexcile2_chase")
									remove_vehicle_recording(004, "lkexcile2_chase")
									remove_vehicle_recording(005, "lkexcile2_chase")
									remove_vehicle_recording(006, "lkexcile2_chase")
									remove_vehicle_recording(007, "lkexcile2_chase")
									remove_vehicle_recording(008, "lkexcile2_chase")
									remove_vehicle_recording(009, "lkexcile2_chase")
									remove_vehicle_recording(010, "lkexcile2_chase")
									remove_vehicle_recording(011, "lkexcile2_chase")
									remove_vehicle_recording(012, "lkexcile2_chase")
									remove_vehicle_recording(013, "lkexcile2_chase")
									remove_vehicle_recording(014, "lkexcile2_chase")
									remove_vehicle_recording(015, "lkexcile2_chase")
									remove_vehicle_recording(016, "lkexcile2_chase")
									remove_vehicle_recording(017, "lkexcile2_chase")
									remove_vehicle_recording(018, "lkexcile2_chase")
									remove_vehicle_recording(019, "lkexcile2_chase")
									remove_vehicle_recording(100, "lkexcile2_chase")
								
									if does_entity_exist(helicopter.veh)
										set_entity_proofs(helicopter.veh, true, true, true, true, true)
									endif 
									
									if IS_AUDIO_SCENE_ACTIVE("EXILE_2_FOLLOW_CAR")
										STOP_AUDIO_SCENE("EXILE_2_FOLLOW_CAR")
									endif 
									
									if is_audio_scene_active("EXILE_2_FOCUS_CAM")
										stop_audio_scene("EXILE_2_FOCUS_CAM")
									endif 
									
									SET_DECAL_BULLET_IMPACT_RANGE_SCALE(1.0)
									
									clear_triggered_labels()
									
									e_section_stage = SECTION_STAGE_SETUP
									mission_flow = STAGE_CRASH_CUTSCENE
								
									return true 
	
								else 
									
									crash_cutscene_fail = true 
									
								endif 
								
							endif 
							
						endif 

					break 
					
				endswitch 
				
			endif 
		
		break 
		
		case 2
		
		break 
		
	endswitch 
	
	return false 
	
endfunc 

proc tunnel_pinning_system()

	switch tunnel_pinning_system_status 

		case 0
		
			if get_current_player_ped_enum() = char_franklin
				if not IS_PLAYER_SWITCH_IN_PROGRESS()

					tunnel_inerior[0] = get_interior_at_coords_with_type(<<-2592.2947, 3129.9180, 13.8053>>, "ch1_roadsdint_tun2")
					pin_interior_in_memory(tunnel_inerior[0])
					
					tunnel_inerior[1] = get_interior_at_coords_with_type(<<-2572.2483, 3325.1445, 12.5609>>, "ch1_roadsdint_tun1")
					pin_interior_in_memory(tunnel_inerior[1])
					
					tunnel_pinning_system_status++
					
				endif 
			endif 
			
		break 
		
		case 1
		
			if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-2589.6570, 3173.9524, 13.2004>>) > 1000

				if is_valid_interior(tunnel_inerior[0])
					unpin_interior(tunnel_inerior[0])
				endif 
				
				if is_valid_interior(tunnel_inerior[1])
					unpin_interior(tunnel_inerior[1])
				endif 
				
				tunnel_pinning_system_status++
			
			endif 
		
		break 
		
		case 2

		break 
		
	endswitch 
	
endproc 

proc audio_scene_hint_cam_system()
			
	if IS_GAMEPLAY_HINT_ACTIVE()
		
		if not is_audio_scene_active("EXILE_2_FOCUS_CAM")
			start_audio_scene("EXILE_2_FOCUS_CAM")
		endif 
		
	else 
	
		if is_audio_scene_active("EXILE_2_FOCUS_CAM")
			stop_audio_scene("EXILE_2_FOCUS_CAM")
		endif 
		
	endif 
	
endproc 

proc trevor_truck_blipping_system()
		
	if not does_blip_exist(trevors_truck.blip)
		if not is_ped_sitting_in_vehicle(player_ped_id(), trevors_truck.veh)
			if get_script_task_status(player_ped_id(), script_task_enter_vehicle) != performing_task 
			
				trevors_truck.blip = create_blip_for_entity(trevors_truck.veh)
				
			endif 
			
		endif 
		
	else 
	
		if is_ped_sitting_in_vehicle(player_ped_id(), trevors_truck.veh)
		
			remove_blip(trevors_truck.blip)
		
		endif 

	endif 
	
endproc 


func bool get_to_mountains()

	IF e_section_stage = SECTION_STAGE_SETUP
		
		setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
		
		request_model(franklin_model)
		request_model(franklins_car.model)
		request_anim_dict("missswitch")  
		request_anim_dict("missexile2switch")  
		request_anim_dict(strAnimDictCamShake)
		request_vehicle_recording(100, "lkexcile2_chase")

		if is_ped_sitting_in_vehicle(player_ped_id(), trevors_truck.veh) and is_ped_sitting_in_vehicle(selector_ped.pedID[SELECTOR_PED_Michael], trevors_truck.veh)

			if has_model_loaded(franklin_model)
			and has_model_loaded(franklins_car.model)
			and has_anim_dict_loaded("missswitch")  
			and has_anim_dict_loaded("missexile2switch")					
			and has_anim_dict_loaded(strAnimDictCamShake)
			and has_vehicle_recording_been_loaded(100, "lkexcile2_chase")

				clear_player_wanted_level(player_id())
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				set_max_wanted_level(0)
				set_create_random_cops(false)

				clear_area(<<-2689.2244, 2368.0752, 15.7681>>, 300.00, true)

				franklins_car.veh = create_vehicle(franklins_car.model, <<-2646.31, 2677.32, 16.6427>>, 350.3382) 
				SET_VEHICLE_ACTIVE_DURING_PLAYBACK(franklins_car.veh, TRUE)
				set_vehicle_has_strong_axles(franklins_car.veh, true)
				set_entity_only_damaged_by_player(franklins_car.veh, true)
				set_vehicle_tyres_can_burst(franklins_car.veh, false)
				SET_VEHICLE_ENGINE_ON(franklins_car.veh, TRUE, TRUE)
				SET_VEHICLE_COLOURS(franklins_car.veh, 0, 0)
				SET_VEH_RADIO_STATION(franklins_car.veh, "RADIO_03_HIPHOP_NEW")
				start_playback_recorded_vehicle(franklins_car.veh, 100, "lkexcile2_chase")
				skip_time_in_playback_recorded_vehicle(franklins_car.veh, 7500)//10500
				set_playback_speed(franklins_car.veh, 0.0)
				force_playback_recorded_vehicle_update(franklins_car.veh)

				//stop_playback_recorded_vehicle(franklins_car.veh)
				//INFORM_MISSION_STATS_car_damage
			
				create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_franklin], char_franklin, franklins_car.veh)
				setup_buddy_attributes(selector_ped.pedID[selector_ped_franklin])
				setup_relationship_contact(selector_ped.pedID[selector_ped_franklin], true)
				
				add_ped_for_dialogue(scripted_speech, 1, selector_ped.pedID[selector_ped_franklin], "franklin")
				SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)
				SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
				SET_SELECTOR_PED_blocked(selector_ped, selector_ped_michael, true)
			
				//get_to_mountains_dialogue_system()

				//locates text for Trevor
				trevor_go_to_instruction_text = "FS_GOD_0"
				trevor_pickup_buddy_text = "FS_michael_0"
				//trevor_get_in_veh_text = "FS_GOD_1"
				trevor_get_back_in_veh_text = "FS_BACKINCAR"
				
				allow_switch = false
				
				original_time = get_game_timer()
//
//				make_selector_ped_selection(selector_ped, SELECTOR_PED_FRANKLIN)
//				selector_cam.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]
//				switch_system_status = 1

				//add_ped_for_dialogue(scripted_speech, 2, null, "trevor")
				
				e_section_stage = SECTION_STAGE_RUNNING
				
			endif 

//***********END COMMENT BACK IN
		endif
		
		trevor_truck_blipping_system()
		
	endif 
	
	IF e_section_stage = SECTION_STAGE_RUNNING

		get_to_mountains_dialogue_system()
	
		if get_to_mountains_master_flow_system()
			
			e_section_stage = SECTION_STAGE_SETUP
			mission_flow = STAGE_CRASH_CUTSCENE
										
			return true 
		endif
		
		tunnel_pinning_system()

		switch_system()
		
		uber_chase_system()
		
		if get_current_player_ped_enum() = char_franklin
			if not allow_switch //only allow the hint cam to activate once the switch code and scripted has finished.
				if not is_ped_injured(gang_enemy[0].ped)
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, gang_enemy[0].ped)
					audio_scene_hint_cam_system()
				endif 
			endif 
		endif 

		//ensures the collision has been streamed into memory before stopping the playback
		if mission_starts_as_franklin
			if not IS_PLAYER_SWITCH_IN_PROGRESS()
				if is_vehicle_driveable(franklins_car.veh)
					if is_playback_going_on_for_vehicle(franklins_car.veh)
						stop_playback_recorded_vehicle(franklins_car.veh)
					endif 
				endif 
			endif 
		endif 
		
	endif 
	
	IF e_section_stage = SECTION_STAGE_CLEANUP
		e_section_stage = SECTION_STAGE_SETUP
		mission_flow = STAGE_CRASH_CUTSCENE
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
	
		if get_current_player_ped_enum() = char_franklin
	
			switch get_to_mountains_master_flow_system_status
			
				case 0
				
					original_time = 0
					
					e_section_stage = SECTION_STAGE_RUNNING
				
				break 
				
				case 1

					clear_mission_locate_stuff(locates_data)
							
					clear_ped_tasks_immediately(player_ped_id())
					set_ped_into_vehicle(player_ped_id(), franklins_car.veh)
					if is_vehicle_driveable(franklins_car.veh)
						set_entity_coords(franklins_car.veh, <<-1989.4053, 4501.2700, 29.7859>>)
						set_entity_heading(franklins_car.veh, 215.9919)	
					endif 
					
					SET_UBER_PLAYBACK_TO_TIME_NOW(gang_car.veh, 83000)
					update_uber_playback(gang_car.veh, 1.0)
					force_playback_recorded_vehicle_update(gang_car.veh)
					
					e_section_stage = SECTION_STAGE_RUNNING
					
				break 
				
			endswitch 
		
		endif 

	endif 
	
	return false 

endfunc 

proc skip_system()

	#IF IS_DEBUG_BUILD

		if is_keyboard_key_just_pressed(key_j)
			
			switch mission_flow

				case stage_get_to_mountains
				
					switch get_current_player_ped_enum()
					
						case char_franklin

//							clear_ped_tasks_immediately(player_ped_id())
//							set_ped_into_vehicle(player_ped_id(), franklins_car.veh)
//							set_entity_coords(franklins_car.veh, <<-1964.7330, 4478.2007, 32.6109>>) 
						
						break 
						
					endswitch 
				
				break
				
				case stage_SNIPE_ENEMIES

				break 
				
				case stage_mission_pass_conversation
					
					mission_flow = stage_switch_to_trevor
				
				break 
				
				case stage_switch_to_trevor
				
					switch switch_to_trevor_status
					
						case 1
						
							if helicopter_switch_system_status = 0
							
								make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
				
								switch get_current_player_ped_enum()
								
									case char_michael
						
//										IF chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
//											setup_switch_cam_to_franklin()
//										else 
//											setup_switch_cam_to_franklin_from_michael_chop()
//										endif 
										
									break 
									
									case char_franklin
									
										IF chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
											setup_switch_cam_to_trevor_from_franklin()
										else 
											setup_switch_cam_to_trevor_from_franklin_chop()
										endif 
										
									break 
									
								endswitch 

								helicopter_switch_system_status = 1
								
							endif 
							
						break 
						
					endswitch 
					
				break 
				
				case stage_pickup_franklin_and_chop
				
					clear_ped_tasks_immediately(player_ped_id())
					set_ped_into_vehicle(player_ped_id(), helicopter.veh)
					
					set_ped_into_vehicle(selector_ped.pedID[selector_ped_franklin], helicopter.veh, vs_back_right)
					
					if not IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(helicopter.veh)
						ATTACH_ENTITY_TO_ENTITY(chop.ped, helicopter.veh, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						attach_entity_to_entity(chop.ped, helicopter.veh, GET_ENTITY_BONE_INDEX_BY_NAME(helicopter.veh, "seat_dside_r"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						task_play_anim(chop.ped, "missexile2", "Chop_Sit_In_Frogger_RDS", normal_blend_in, normal_blend_out, -1, af_looping) 
					endif 

					mission_flow = stage_get_to_airstrip

				break 
				
				case stage_get_to_airstrip
				
					set_entity_coords(helicopter.veh, <<1744.9111, 3254.2053, 41.1>>)
				
				break 
			endswitch 
			
		elif is_keyboard_key_just_pressed(key_p)
		
			switch mission_flow 
			

			case stage_get_to_mountains

				remove_all_mission_assets()
				launch_mission_stage_menu_status = 0
				mission_flow = load_stage_selector_assets
			
			break 

			CASE STAGE_CRASH_CUTSCENE
			CASE STAGE_INVESTIGATE_CRASH

				remove_all_mission_assets()
				mission_flow = load_stage_selector_assets
				
				if (get_game_timer() - p_skip_time) > 3000	
					launch_mission_stage_menu_status = 1
				else
					launch_mission_stage_menu_status = 0
				endif 
					
				p_skip_time = get_game_timer()
				
			BREAK
			
			case stage_get_helicopter
			
				remove_all_mission_assets()
				mission_flow = load_stage_selector_assets
				
				if (get_game_timer() - p_skip_time) > 3000	
					launch_mission_stage_menu_status = 2
				else
					launch_mission_stage_menu_status = 1
				endif 
					
				p_skip_time = get_game_timer()
			
			break 
			
			CASE STAGE_SWAP_TO_MICHAEL_CUTSCENE
			
				remove_all_mission_assets()
				mission_flow = load_stage_selector_assets
				
				if (get_game_timer() - p_skip_time) > 3000	
					launch_mission_stage_menu_status = 3
				else
					launch_mission_stage_menu_status = 2
				endif 
					
				p_skip_time = get_game_timer()
				
			break 
			
			CASE STAGE_SNIPE_ENEMIES
				
				remove_all_mission_assets()
				mission_flow = load_stage_selector_assets
				
				if not does_entity_exist(gang_enemy[3].ped)
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 4
					else
						launch_mission_stage_menu_status = 3
					endif 
					
				else 
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 5
					else
						launch_mission_stage_menu_status = 4
					endif 
					
				endif 
						
				p_skip_time = get_game_timer()
				
			BREAK
			
			case stage_mission_pass_conversation
			
				remove_all_mission_assets()
				mission_flow = load_stage_selector_assets

				launch_mission_stage_menu_status = 5

			break 
			
			
			case stage_pickup_franklin_and_chop
			case stage_get_to_airstrip
			
				remove_all_mission_assets()
				mission_flow = load_stage_selector_assets
				
				if (get_game_timer() - p_skip_time) > 3000	
					launch_mission_stage_menu_status = 6
				else
					launch_mission_stage_menu_status = 5
				endif 
					
				p_skip_time = get_game_timer()
			
			break 

		endswitch 
		
		endif 
		
		if LAUNCH_MISSION_STAGE_MENU(menu_stage_selector, menu_return_stage) 
			
			remove_all_mission_assets()

			launch_mission_stage_menu_status = menu_return_stage
			
			mission_flow = load_stage_selector_assets
			
			//do_screen_fade_out(1000)
			
		endif
		
	#endif 

endproc 

proc allow_chop_on_mission_debug()
		
	if mission_flow = stage_get_to_mountains

		if not has_label_been_triggered("chop_help0")
			print_help("chop_help0")
			set_label_as_triggered("chop_help0", true)
		endif 

		#IF IS_DEBUG_BUILD
			 IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
          	
//				PRINTSTRING("Setting Chop behaviour to good") 
//				PRINTNL()
				
				clear_help()
	          	
				//chopBehaviour = CHOP_BEHAVIOUR_GOOD

				//allow_chop_on_mission = true 
			endif 
		#endif 
		
	endif 

endproc 

proc skip_to_crash_cutscene()

	request_model(franklin_model)
	request_model(franklins_car.model)
	request_model(gang_car.model)
	request_model(gang_enemy[0].model)
	request_model(chop.model)
	request_model(animal[0].model)
	request_anim_dict("missexile2")
	request_anim_dict("missexile2ig_5")
	request_anim_dict("missexile2deer_crash")
	REQUEST_VEHICLE_RECORDING(CARREC_CRASH, str_carrec)
	REQUEST_VEHICLE_RECORDING(CARREC_GANG, str_carrec)
	//LOAD_STREAM_WITH_START_OFFSET("CAR_CRASH_OFF_CLIFF_STREAM", 500, "EXILE_2_SOUNDS")
	load_stream("CAR_CRASH_OFF_CLIFF_STREAM", "EXILE_2_SOUNDS")
	REQUEST_PTFX_ASSET() 
		
	while not has_model_loaded(franklin_model)
	or not has_model_loaded(franklins_car.model)
	or not has_model_loaded(gang_car.model)
	or not has_model_loaded(gang_enemy[0].model)
	or not has_model_loaded(chop.model)
	or not has_model_loaded(animal[0].model)
	or not has_anim_dict_loaded("missexile2")
	or not has_anim_dict_loaded("missexile2ig_5")
	or not has_anim_dict_loaded("missexile2deer_crash")
	or not HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_CRASH, str_carrec)
	or not HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_GANG, str_carrec)
	//or not LOAD_STREAM_WITH_START_OFFSET("CAR_CRASH_OFF_CLIFF_STREAM", 500, "EXILE_2_SOUNDS")
	or not load_stream("CAR_CRASH_OFF_CLIFF_STREAM", "EXILE_2_SOUNDS")
	or not has_ptfx_asset_loaded()
	
		wait(0)
		
	endwhile 
	
	clear_area(<<1984.2650, 3818.7625, 31.3800>>, 10000.00, true)
	
	if get_current_player_ped_enum() != char_franklin
		set_current_selector_ped(selector_ped_franklin, false)
	endif 
	
	franklins_car.veh = create_vehicle(franklins_car.model, <<-1969.0022, 4481.5786, 32.3209>>, 215.9919) 
	set_vehicle_has_strong_axles(franklins_car.veh, true)
	set_entity_only_damaged_by_player(franklins_car.veh, true)
	set_vehicle_tyres_can_burst(franklins_car.veh, false)
	SET_VEHICLE_ENGINE_ON(franklins_car.veh, TRUE, TRUE)
	SET_VEHICLE_COLOURS(franklins_car.veh, 0, 0)
	SET_VEH_RADIO_STATION(franklins_car.veh, "RADIO_03_HIPHOP_NEW")

	set_ped_into_vehicle(player_ped_id(), franklins_car.veh)
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	add_ped_for_dialogue(scripted_speech, 1, player_ped_id(), "franklin")
	
	chop.pos = get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, 1.5>>)
	setup_chop(chop)
	remove_blip(chop.blip)
	attach_entity_to_entity(chop.ped, franklins_car.veh, GET_ENTITY_BONE_INDEX_BY_NAME(franklins_car.veh, "seat_pside_f"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	task_play_anim(chop.ped, "missexile2", "chop_sit_in_baller_ps", normal_blend_in, normal_blend_out, -1, af_looping) 
	
	gang_car.veh = create_vehicle(gang_car.model, <<-1964.0022, 4484.5786, 32.3209>>, 338.0473)
	set_entity_proofs(gang_car.veh, true, true, true, true, true)
	set_vehicle_colours(gang_car.veh, 7, 0)
	
	setup_enemy_in_vehicle(gang_enemy[0], gang_car.veh, vs_driver)
	set_entity_proofs(gang_enemy[0].ped, true, true, true, true, true)
	remove_blip(gang_enemy[0].blip)
		
	setup_enemy_in_vehicle(gang_enemy[1], gang_car.veh, vs_front_right)
	set_entity_proofs(gang_enemy[1].ped, true, true, true, true, true)
	remove_blip(gang_enemy[1].blip)
	
	setup_enemy_in_vehicle(gang_enemy[2], gang_car.veh, vs_back_left)
	set_entity_proofs(gang_enemy[2].ped, true, true, true, true, true)
	remove_blip(gang_enemy[2].blip)
	
	add_ped_for_dialogue(scripted_speech, 0, null, "michael") 
	add_ped_for_dialogue(scripted_speech, 2, null, "trevor") 
	
	load_scene(get_entity_coords(player_ped_id()))
	
	e_section_stage = SECTION_STAGE_setup
	mission_flow = stage_crash_cutscene
	
	end_cutscene(false, 0, 0, false)
		
endproc  

proc load_replay_skip_0()

	if replay_active
		start_replay_setup(<<1981.1268, 3830.4668, 31.4241>>, 218.8945)
	endif 

	request_model(michael_model)
	request_model(trevor_model)	
	request_model(trevors_truck.model)	
	REQUEST_VEHICLE_ASSET(trevors_truck.model, enum_to_int(VRF_REQUEST_ALL_ANIMS))
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(michael_model)
	or not has_model_loaded(trevor_model)
	or not has_model_loaded(trevors_truck.model)	
	or not has_vehicle_asset_loaded(trevors_truck.model)
	
		wait(0)
		
	endwhile 
	
	SET_VEHICLE_POPULATION_BUDGET(1)
	SET_PED_POPULATION_BUDGET(1)
	
	clear_area(<<1982.1276, 3829.4204, 31.3848>>, 10000, true)

	CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, trevors_truck.pos, trevors_truck.heading)
	set_vehicle_on_ground_properly(trevors_truck.veh)
	
	set_current_selector_ped(selector_ped_trevor)
	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<1981.1268, 3830.4668, 31.4241>>)
	set_entity_heading(player_ped_id(), 218.8945)
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	task_enter_vehicle(player_ped_id(), trevors_truck.veh, -1, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED | ECF_WARP_ENTRY_POINT)
	force_ped_ai_and_animation_update(player_ped_id())
			
	create_player_ped_on_foot(selector_ped.pedID[selector_ped_michael], char_michael, <<1982.65125, 3827.78638, 31.41647>>, 332.5456, false) //<<-1874.7228, 4482.9673, 24.4418>>, 334.1932, false)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
	setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")
	task_enter_vehicle(selector_ped.pedID[SELECTOR_PED_Michael], trevors_truck.veh, -1, VS_front_right)
	force_ped_ai_and_animation_update(selector_ped.pedID[SELECTOR_PED_Michael])
	
	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	wait(0)
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(player_ped_id())
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[selector_ped_michael])
	
		wait(0)
	
	endwhile 

	end_cutscene(false, 90, 0, false)

	e_section_stage = SECTION_STAGE_SETUP
	mission_flow = stage_get_to_mountains
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "start of mission")

endproc 

proc request_replay_skip_1_assets()
	
	request_model(franklin_model)
	request_model(franklins_car.model)
	request_model(gang_car.model)
	request_model(gang_enemy[0].model)
	request_model(chop.model)
	request_model(radi)
	request_model(sabregt)
	REQUEST_PTFX_ASSET()  
	request_vehicle_recording(003, "lkexcile2")
	request_vehicle_recording(004, "lkexcile2")
	request_vehicle_recording(005, "lkexile2")
	request_vehicle_recording(001, "lkexcile2_chase")
	request_vehicle_recording(002, "lkexcile2_chase")
	request_vehicle_recording(003, "lkexcile2_chase")
	request_vehicle_recording(004, "lkexcile2_chase")
	request_vehicle_recording(005, "lkexcile2_chase")
	request_vehicle_recording(006, "lkexcile2_chase")
	request_vehicle_recording(007, "lkexcile2_chase")
	request_vehicle_recording(008, "lkexcile2_chase")
	request_vehicle_recording(009, "lkexcile2_chase")
	request_vehicle_recording(010, "lkexcile2_chase")
	request_vehicle_recording(011, "lkexcile2_chase")
	request_vehicle_recording(012, "lkexcile2_chase")
	request_vehicle_recording(013, "lkexcile2_chase")
	request_vehicle_recording(014, "lkexcile2_chase")
	request_vehicle_recording(015, "lkexcile2_chase")
	request_vehicle_recording(016, "lkexcile2_chase")
	request_vehicle_recording(017, "lkexcile2_chase")
	request_vehicle_recording(018, "lkexcile2_chase")
	request_vehicle_recording(019, "lkexcile2_chase")
	request_vehicle_recording(100, "lkexcile2_chase")
	request_anim_dict("missexile2")
	request_anim_dict("missexile2switch")
	
endproc

proc load_replay_skip_1()

	if replay_active
		start_replay_setup(<<-2638.6746, 2725.4666, 15.6897>>, 351.2094, false)
	endif

	request_replay_skip_1_assets()
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
		
	while not has_model_loaded(franklin_model)
	or not has_model_loaded(franklins_car.model)
	or not has_model_loaded(gang_car.model)
	or not has_model_loaded(gang_enemy[0].model)
	or not has_model_loaded(chop.model)
	or not has_model_loaded(radi)
	or not has_model_loaded(sabregt)
	or not has_ptfx_asset_loaded()
	or not has_vehicle_recording_been_loaded(003, "lkexcile2")
	or not has_vehicle_recording_been_loaded(004, "lkexcile2")
	or not has_vehicle_recording_been_loaded(005, "lkexile2")
	or not has_vehicle_recording_been_loaded(001, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(002, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(003, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(004, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(005, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(006, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(007, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(008, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(009, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(010, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(011, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(012, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(013, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(014, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(015, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(016, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(017, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(018, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(019, "lkexcile2_chase")
	or not has_vehicle_recording_been_loaded(100, "lkexcile2_chase")
	or not has_anim_dict_loaded("missexile2")
	or not has_anim_dict_loaded("missexile2switch")
		
		wait(0)
		
	endwhile 
	
	clear_player_wanted_level(player_id())
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	set_max_wanted_level(0)
	set_create_random_cops(false)
	
	SET_VEHICLE_POPULATION_BUDGET(1)
	SET_PED_POPULATION_BUDGET(1)
	
	clear_area(<<1984.2650, 3818.7625, 31.3800>>, 10000.00, true)

	gang_car.veh = create_vehicle(gang_car.model, <<-2639.9, 2733.9, 16.6>>, 338.0473)
	SET_VEHICLE_ACTIVE_DURING_PLAYBACK(gang_car.veh, TRUE)
	set_entity_proofs(gang_car.veh, true, true, true, true, true)
	set_vehicle_colours(gang_car.veh, 7, 0)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(gang_car.veh, "EXILE_2_ENEMY_CAR")
	SET_DECAL_BULLET_IMPACT_RANGE_SCALE(3.0) //set bullets show up on proofed car fix for bug 1490817
	
	setup_enemy_in_vehicle(gang_enemy[0], gang_car.veh, vs_driver)
	set_entity_proofs(gang_enemy[0].ped, true, true, true, true, true)
	remove_blip(gang_enemy[0].blip)
		
	setup_enemy_in_vehicle(gang_enemy[1], gang_car.veh, vs_front_right)
	set_entity_proofs(gang_enemy[1].ped, true, true, true, true, true)
	remove_blip(gang_enemy[1].blip)
	
	setup_enemy_in_vehicle(gang_enemy[2], gang_car.veh, vs_back_left)
	set_entity_proofs(gang_enemy[2].ped, true, true, true, true, true)
	remove_blip(gang_enemy[2].blip)

	INITIALISE_UBER_PLAYBACK("lkexcile2_chase", 001, true)
	load_uber_data()
	//switch_SetPieceCar_to_ai_on_collision = true
	fUberPlaybackDensitySwitchOffRange = 200
	allow_veh_to_stop_on_any_veh_impact = true
	bTrafficDontCleanupRecordingFiles = true
	bCreateAllWaitingCars = true
	traffic_block_vehicle_colour(true, traffic_black, traffic_white)
	start_playback_recorded_vehicle(gang_car.veh, 001, "lkexcile2_chase")
	//update_uber_playback(gang_car.veh, 1.0)
	SET_UBER_PLAYBACK_TO_TIME_NOW(gang_car.veh, 11450)//9500
	bCreateAllWaitingCars = true
	update_uber_playback(gang_car.veh, 1.0)
	force_playback_recorded_vehicle_update(gang_car.veh)
		
	
	if get_current_player_ped_enum() != char_franklin
		set_current_selector_ped(selector_ped_franklin, false)
	endif 
	
	franklins_car.veh = create_vehicle(franklins_car.model, <<-2646.31, 2677.32, 16.6427>>, 351.1425) 
	SET_VEHICLE_ACTIVE_DURING_PLAYBACK(franklins_car.veh, TRUE)
	set_vehicle_has_strong_axles(franklins_car.veh, true)
	set_entity_only_damaged_by_player(franklins_car.veh, true)
	set_vehicle_tyres_can_burst(franklins_car.veh, false)
	SET_VEHICLE_ENGINE_ON(franklins_car.veh, TRUE, TRUE)
	SET_VEHICLE_COLOURS(franklins_car.veh, 0, 0)
	SET_VEH_RADIO_STATION(franklins_car.veh, "RADIO_03_HIPHOP_NEW")
	start_playback_recorded_vehicle(franklins_car.veh, 100, "lkexcile2_chase")
	skip_time_in_playback_recorded_vehicle(franklins_car.veh, 11450)//9500)
	force_playback_recorded_vehicle_update(franklins_car.veh)
	//stop_playback_recorded_vehicle(franklins_car.veh)

	freeze_entity_position(player_ped_id(), false)
	set_ped_into_vehicle(player_ped_id(), franklins_car.veh)
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	add_ped_for_dialogue(scripted_speech, 1, player_ped_id(), "franklin")
	
	chop.pos = get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, 1.5>>)
	setup_chop(chop)
	remove_blip(chop.blip)
	attach_entity_to_entity(chop.ped, franklins_car.veh, GET_ENTITY_BONE_INDEX_BY_NAME(franklins_car.veh, "seat_pside_f"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	task_play_anim(chop.ped, "missexile2", "chop_sit_in_baller_ps", normal_blend_in, normal_blend_out, -1, af_looping) 

	//locates text for Trevor
	trevor_go_to_instruction_text = "FS_GOD_0"
	trevor_pickup_buddy_text = "FS_michael_0"
	//trevor_get_in_veh_text = "FS_GOD_1"
	trevor_get_back_in_veh_text = "FS_BACKINCAR"
	
	
//	NEW_LOAD_SCENE_START(<<-2634.3069, 2753.4336, 15.6932>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 351.0222>>), 600.0)
//
//	while not IS_NEW_LOAD_SCENE_LOADED()
//	
//		wait(0)
//		
//	endwhile 
//	
//	NEW_LOAD_SCENE_STOP()

	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	original_time = get_game_timer()
	
	while not lk_timer(original_time, 1000)
	
		wait(0)
		
		bCreateAllWaitingCars = true
		
		update_uber_playback(gang_car.veh, 1.0)
		
	endwhile 
	
	if is_vehicle_driveable(franklins_car.veh)
		if is_playback_going_on_for_vehicle(franklins_car.veh)
			stop_playback_recorded_vehicle(franklins_car.veh)
		endif 
	endif 
	
	uber_chase_time = get_game_timer() //force playback speed to 1.1 for 2 seconds in 
	
	allow_switch = false
	
	get_to_mountains_master_flow_system_status = 1
	e_section_stage = SECTION_STAGE_RUNNING
	mission_flow = stage_get_to_mountains
	
	//stop the recording to give control back to the player
	
	start_audio_scene("EXILE_2_FOLLOW_CAR")
	
	end_cutscene(false, 0, 0, false)

	get_to_mountains_dialogue_system()
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "start of casr chase mission")

endproc 

proc load_replay_skip_2()

	if replay_active
		start_replay_setup(<<1768.1128, 3240.0544, 41.0869>>, 42.5289, false)
	endif

	request_model(michael_model)
	request_model(franklin_model)
	request_model(trevor_model)
	request_model(chop.model)
	request_model(helicopter.model)
	request_model(franklins_car.model)
	request_model(gang_car.model)
	request_model(trevors_truck.model)
	request_anim_dict("missexile2")
	REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	
	if replay_active
		END_REPLAY_SETUP()
	endif

	while not has_model_loaded(michael_model)
	or not has_model_loaded(franklin_model)
	or not has_model_loaded(trevor_model)
	or not has_model_loaded(chop.model)
	or not has_model_loaded(helicopter.model)
	or not has_model_loaded(franklins_car.model)
	or not has_model_loaded(gang_car.model)
	or not has_model_loaded(trevors_truck.model)
	or not has_anim_dict_loaded("missexile2")
	or not REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	
		wait(0)
	
	endwhile 
	
	SET_VEHICLE_POPULATION_BUDGET(1)
	SET_PED_POPULATION_BUDGET(1)
	
	clear_area(<<1760.0851, 3242.4521, 40.8998>>, 1000, true)

	helicopter.veh = create_vehicle(helicopter.model, <<1768.1128, 3240.0544, 41.0869>>, 42.5289)
	SET_VEHICLE_AS_RESTRICTED(helicopter.veh, 0)
	set_vehicle_livery(helicopter.veh, 1)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(helicopter.veh, false)
	set_entity_only_damaged_by_player(helicopter.veh, true)
	set_vehicle_colours(helicopter.veh, 34, 34)
	SET_VEHICLE_EXTRA_COLOURS(helicopter.veh, 0, 0)	
	set_vehicle_on_ground_properly(helicopter.veh)

	
	set_current_selector_ped(selector_ped_trevor, false)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	set_ped_into_vehicle(player_ped_id(), helicopter.veh)
	
	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, helicopter.veh, vs_front_right, false)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
	setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
	
	create_player_ped_on_foot(selector_ped.pedID[selector_ped_franklin], char_franklin, <<-1683.4967, 4454.5952, 1.5660>>, 239.1512, false)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_franklin])
	setup_relationship_contact(selector_ped.pedID[selector_ped_franklin], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	setup_weapon_for_ped(selector_ped.pedID[selector_ped_franklin], weapontype_carbinerifle, 100, false, false)add_ped_for_dialogue(scripted_speech, 1, selector_ped.pedID[selector_ped_franklin], "franklin")
	SET_ENTITY_LOAD_COLLISION_FLAG(selector_ped.pedID[selector_ped_franklin], true)
	SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)

	chop.pos = <<-1679.8206, 4455.9878, 1.2653>>
	chop.heading = 114.6932
	setup_chop(chop)
	remove_blip(chop.blip)
	setup_relationship_animal(chop.ped)
	SET_ENTITY_LOAD_COLLISION_FLAG(chop.ped, true)
	chop_ai_system_status = 101
	
	gang_car.veh = CREATE_VEHICLE(gang_car.model, <<-1873.92, 4486.02, 24.625>>, 239.264404)
	set_vehicle_colours(gang_car.veh, 7, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(gang_car.model) 
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(gang_car.veh, false)
	//force_playback_recorded_vehicle_update(franklins_car.veh)	
	SET_VEHICLE_DAMAGE(gang_car.veh, <<-0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DAMAGE(gang_car.veh, <<0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DAMAGE(gang_car.veh, <<0.3, -0.1, 0.75>>, 200.00, 200.00, TRUE)
	SET_VEHICLE_DOOR_BROKEN(gang_car.veh, SC_DOOR_BONNET, true)
	set_vehicle_on_ground_properly(gang_car.veh)
	
	franklins_car.veh = CREATE_VEHICLE(franklins_car.model, <<-1899.3949, 4431.2559, 42.8050>>, 228.1822)
	set_vehicle_has_strong_axles(franklins_car.veh, true)
	set_entity_only_damaged_by_player(franklins_car.veh, true)
	SET_VEHICLE_COLOURS(franklins_car.veh, 12, 12)
	
//	SET_CLOCK_TIME(21, 0, 0)
	
	if not replay_active
		load_scene(<<1760.0851, 3242.4521, 40.8998>>)
	endif 
	
	wait(0)
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(player_ped_id())
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[selector_ped_michael])
	
		wait(0)
		
	endwhile 
	
	trigger_music_event("EXL2_FLY_HELI_RT")
	
	get_helicopter_status = 1

	get_to_the_helicopter_dialogue_system_status = 3

	allow_switch = false					
	switch_system_2_status = 0
	
	e_section_stage = SECTION_STAGE_running
	mission_flow = stage_get_helicopter
	
	end_cutscene()

	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "fly to franklin")

endproc 

proc load_replay_skip_3()
		
	int i 
	
	if replay_active
		start_replay_setup(<<-1574.7, 4344.9, 43.4>>, 42.6626, false)
	else 
		set_entity_coords(player_ped_id(), <<-1588.7863, 4346.2402, 2.0581>>)
		set_entity_heading(player_ped_id(), 21.5456)
	endif 

	request_model(michael_model)
	request_model(franklin_model)
	request_model(trevor_model)
	request_model(helicopter.model)
	request_model(franklins_car.model)
	request_model(gang_car.model)
	request_model(gang_enemy[3].model)
	request_model(animal[0].model)
	request_model(chop.model)
	request_model(rpg.model)
	request_vehicle_recording(CARREC_CRASH, str_carrec)
	request_vehicle_recording(CARREC_GANG, str_carrec)
	request_vehicle_recording(CARREC_HELI_HOVER, str_carrec)
	request_vehicle_recording(007, str_carrec)
	request_vehicle_recording(001, "lkexile2")
	request_vehicle_recording(ambient_car.recording_number, "lkexile2")
	REQUEST_WAYPOINT_RECORDING("exile2_0")
	REQUEST_WAYPOINT_RECORDING("exile2_1")
	REQUEST_WAYPOINT_RECORDING("exile2_2")
	REQUEST_WAYPOINT_RECORDING("exile2_3")
	request_weapon_asset(WEAPONTYPE_HEAVYSNIPER)
	request_anim_dict("missexile2")
	request_anim_dict(strAnimDict)
	REQUEST_ANIM_SET("move_ped_crouched")
	REQUEST_ANIM_SET("Wpn_AssaultRifle_WeaponHoldingCrouched")
	REQUEST_ANIM_SET("move_ped_crouched_strafing")
	REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	REQUEST_PTFX_ASSET() 
	
	if replay_active
		END_REPLAY_SETUP()
	endif 

	while not has_model_loaded(michael_model)
	or not has_model_loaded(franklin_model)
	or not has_model_loaded(trevor_model)
	or not has_model_loaded(helicopter.model)
	or not has_model_loaded(franklins_car.model)
	or not has_model_loaded(gang_car.model)
	or not has_model_loaded(gang_enemy[3].model)
	or not has_model_loaded(animal[0].model)
	or not has_model_loaded(chop.model)
	or not has_model_loaded(rpg.model)
	or not has_vehicle_recording_been_loaded(CARREC_CRASH, str_carrec)
	or not has_vehicle_recording_been_loaded(CARREC_GANG, str_carrec)
	or not has_vehicle_recording_been_loaded(CARREC_HELI_HOVER, str_carrec)
	or not has_vehicle_recording_been_loaded(007, str_carrec)
	or not has_vehicle_recording_been_loaded(001, "lkexile2")
	or not has_vehicle_recording_been_loaded(ambient_car.recording_number, "lkexile2")
	or not get_is_waypoint_recording_loaded("exile2_0")
	or not get_is_waypoint_recording_loaded("exile2_1")
	or not get_is_waypoint_recording_loaded("exile2_2")
	or not get_is_waypoint_recording_loaded("exile2_3")
	or not has_weapon_asset_loaded(WEAPONTYPE_HEAVYSNIPER)
	or not has_anim_dict_loaded("missexile2")
	or not has_anim_dict_loaded(strAnimDict)
	or not has_anim_set_loaded("move_ped_crouched")
	or not has_anim_set_loaded("Wpn_AssaultRifle_WeaponHoldingCrouched")
	or not has_anim_set_loaded("move_ped_crouched_strafing")
	or not REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	or not has_ptfx_asset_loaded()

		wait(0)
	
	endwhile 
	
	SET_ROADS_IN_AREA(vCountryRoadMin, vCountryRoadMax, FALSE)
	
	add_scenario_blocking_area(<<-2607.6, 3715.3, 300.00>>, <<-541.9, 4969.0, -100.00>>) //whole forrest area
	
	SET_VEHICLE_POPULATION_BUDGET(0) //removes all cars from vehicle population
	SET_PED_POPULATION_BUDGET(0)

	clear_area(<<-1664.92, 4438.81, 80.00>>, 10000, true)

	SET_FOCUS_POS_AND_VEL(<<-1664.92, 4438.81, 80.00>>, <<0.0, 0.0, 0.0>>)//old
	//SET_FOCUS_POS_AND_VEL(<<-1569.8, 4524.0, 80.00>>, <<0.0, 0.0, 0.0>>) //NEW
	
	DISABLE_CELLPHONE(TRUE)


	SEETHROUGH_SET_HEATSCALE(tb_dead, 0.0)
	SEETHROUGH_SET_HEATSCALE(tb_cold, 1.0) //animals
	SEETHROUGH_SET_HEATSCALE(tb_warm, 1.0) //animals PEDS 
	SEETHROUGH_SET_HEATSCALE(tb_hot, 1.0) //peds
	
	clear_player_wanted_level(player_id())
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	set_max_wanted_level(0)
	set_create_random_cops(false)
	
	helicopter.veh = create_vehicle(helicopter.model,  get_position_of_vehicle_recording_at_time(CARREC_HELI_HOVER, 0, str_carrec))
	SET_VEHICLE_AS_RESTRICTED(helicopter.veh, 0)
	set_vehicle_extra(helicopter.veh, 12, true) 
	set_vehicle_livery(helicopter.veh, 1)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(helicopter.veh, false)
	set_vehicle_colours(helicopter.veh, 34, 34)
	SET_VEHICLE_EXTRA_COLOURS(helicopter.veh, 0, 0)	
	set_entity_proofs(helicopter.veh, false, true, true, true, true)
	set_entity_only_damaged_by_player(helicopter.veh, true)
	START_PLAYBACK_RECORDED_VEHICLE(helicopter.veh, CARREC_HELI_HOVER, str_carrec)
	SET_VEHICLE_ENGINE_ON(helicopter.veh, TRUE, TRUE)
	SET_HELI_BLADES_FULL_SPEED(helicopter.veh)
	force_playback_recorded_vehicle_update(helicopter.veh)
	
	if get_current_player_ped_enum() != char_michael
		set_current_selector_ped(selector_ped_michael, false)
	endif 
	CLEAR_PED_TASKS_IMMEDIATELY(player_ped_id())
	ATTACH_ENTITY_TO_ENTITY(player_ped_id(), helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
	SET_ped_DESIRED_HEADING(player_ped_id(), get_entity_heading(player_ped_id()))
	GIVE_WEAPON_TO_PED(player_ped_id(), WEAPONTYPE_HEAVYSNIPER, infinite_ammo, true, true)//1000 infinite_ammo
	GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, WEAPONCOMPONENT_AT_SCOPE_MAX)
	SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), false)
	SET_PED_MOVEMENT_CLIPSET(player_ped_id(), "move_ped_crouched")
	SET_PED_WEAPON_MOVEMENT_CLIPSET(player_ped_id(), "Wpn_AssaultRifle_WeaponHoldingCrouched")
	SET_PED_STRAFE_CLIPSET(player_ped_id(), "move_ped_crouched_strafing")
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	add_ped_for_dialogue(scripted_speech, 0, player_ped_ID(), "michael")
	SET_SELECTOR_PED_ACTIVITY(selector_ped, SELECTOR_PED_MICHAEL, SELECTOR_ACTIVITY_ATTACHED_TO_HELI)
	SET_SELECTOR_PED_ACTIVITY(selector_ped, SELECTOR_PED_MICHAEL, SELECTOR_ACTIVITY_THERMAL_VISION)
	SET_PLAYER_FORCED_AIM(player_id(), true)
	set_ped_reset_flag(player_ped_id(), PRF_InstantBlendToAim, true)
	force_ped_ai_and_animation_update(player_ped_id(), true)
	
	//if in first person mode the snipper hud will remain constantly on. Else it will render but if the player presses the aim button and depresses it. Then the player will stop aiming and as a result the sniper hud will not render.
	if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
		force_sniper_hud_for_first_person_view_mode = true 
	else 
		force_sniper_hud_for_start_of_mission = true
	endif 
	
	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_trevor], char_trevor, helicopter.veh, vs_driver)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_trevor])
	setup_relationship_contact(selector_ped.pedID[selector_ped_trevor], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_trevor], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	add_ped_for_dialogue(scripted_speech, 2, selector_ped.pedID[selector_ped_trevor], "trevor")

	create_player_ped_on_foot(selector_ped.pedID[selector_ped_franklin], char_franklin, <<-1683.4967, 4454.5952, 1.5660>>, 239.1512, false)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_franklin])
	setup_relationship_contact(selector_ped.pedID[selector_ped_franklin], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
//	if not HAS_PED_GOT_WEAPON(selector_ped.pedID[selector_ped_franklin], weapontype_carbinerifle)
//		give_weapon_to_ped(selector_ped.pedID[selector_ped_franklin], weapontype_carbinerifle, 100, false, false)
//	endif 
	setup_weapon_for_ped(selector_ped.pedID[selector_ped_franklin], weapontype_carbinerifle, 100, false, false)
	add_ped_for_dialogue(scripted_speech, 1, selector_ped.pedID[selector_ped_franklin], "franklin")
	SET_ENTITY_LOAD_COLLISION_FLAG(selector_ped.pedID[selector_ped_franklin], true)
	SET_ENTITY_LOD_DIST(selector_ped.pedID[selector_ped_franklin], 300) 
	SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)
	SET_PED_USING_ACTION_MODE(selector_ped.pedID[selector_ped_franklin], true)
	franklin_ai_system_status = 999


	chop.pos = <<-1679.8206, 4455.9878, 1.2653>>
	chop.heading = 114.6932
	setup_chop(chop)
	remove_blip(chop.blip)
	setup_relationship_animal(chop.ped)
	SET_ENTITY_LOAD_COLLISION_FLAG(chop.ped, true)
	SET_ENTITY_LOD_DIST(chop.ped, 400)//300
	chop_ai_system_status = 200 
	set_selector_ped_blocked(selector_ped, selector_ped_trevor, false)
	SET_SELECTOR_PED_ICON(selector_ped, SELECTOR_PED_TREVOR, SELECTOR_ICON_CHOP)
	chop_cam.selectorSlot = SELECTOR_PED_TREVOR
	chop_cam.chopPed = chop.ped
	CREATE_CHOP_CAM(chop_cam)
	chop_cam.state = CHOP_VIEW_CAM_DISABLED
	set_chop_view_cam_disabled(chop_cam, selector_ped)
	task_play_anim(chop.ped, "missexile2", "fra0_ig_12_chop_waiting_a", normal_blend_in, normal_blend_out, -1, af_looping)

	for i = 1 to count_of(animal) - 1
		if i != 7
		and i != 14
			setup_animal(animal[i])
		endif 
	endfor 	
	
	for i = 0 to 1
		setup_enemy(gang_enemy[i])
		remove_blip(gang_enemy[i].blip)
		SET_PED_DIES_IN_WATER(gang_enemy[i].ped, FALSE)
		SET_ENTITY_LOAD_COLLISION_FLAG(gang_enemy[i].ped, true)
	endfor 
	
	gang_car.veh = CREATE_VEHICLE(gang_car.model, <<-1873.92, 4486.02, 24.625>>, 239.264404)
	set_vehicle_colours(gang_car.veh, 7, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(gang_car.model) 
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(gang_car.veh, false)
	//force_playback_recorded_vehicle_update(franklins_car.veh)	
	SET_VEHICLE_DAMAGE(gang_car.veh, <<-0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DAMAGE(gang_car.veh, <<0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DAMAGE(gang_car.veh, <<0.3, -0.1, 0.75>>, 200.00, 200.00, TRUE)
	SET_VEHICLE_DOOR_BROKEN(gang_car.veh, SC_DOOR_BONNET, true)
	set_vehicle_on_ground_properly(gang_car.veh)

	franklins_car.veh = CREATE_VEHICLE(franklins_car.model, <<-2442.8577, 3670.4836, 12.4540>>)
	set_vehicle_has_strong_axles(franklins_car.veh, true)
	set_entity_only_damaged_by_player(franklins_car.veh, true)
	SET_VEHICLE_COLOURS(franklins_car.veh, 12, 12)
	START_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh, CARREC_CRASH, str_carrec)
	skip_to_end_and_stop_playback_recorded_vehicle(franklins_car.veh)
	//force_playback_recorded_vehicle_update(franklins_car.veh)		
	STOP_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh)
	
	ambient_car.veh = create_vehicle(ambient_car.model, ambient_car.pos, ambient_car.heading)
	set_vehicle_doors_locked(ambient_car.veh, vehiclelock_lockout_player_only)
	set_vehicle_on_ground_properly(ambient_car.veh)
	set_entity_proofs(ambient_car.veh, true, true, true, true, true)
	
//	SET_CLOCK_TIME(22, 0, 0)
	
	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	START_AUDIO_SCENE("EXILE_2_SNIPE_STAGE")

	trigger_music_event("EXL2_SNIPE_RT")
	
	end_cutscene(false, -12, -41, false)
	
	allow_switch = false

	helicopter_camera_system()
	
	mission_flow = STAGE_SNIPE_ENEMIES
	e_section_stage = SECTION_STAGE_RUNNING
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "start of sniper stage")

endproc 

proc load_replay_skip_4()

	int i 
	
	if replay_active
		start_replay_setup(<<-1683.4967, 4454.5952, 1.5660>>, 239.1512, false)
	endif 
	
	request_model(michael_model)
	request_model(franklin_model)
	request_model(trevor_model)
	request_model(helicopter.model)
	request_model(franklins_car.model)
	request_model(gang_car.model)
	request_model(gang_enemy[3].model)
	request_model(animal[0].model)
	request_model(chop.model)
	request_model(rpg.model)
	request_vehicle_recording(001, "lkexile2")
	request_vehicle_recording(ambient_car.recording_number, "lkexile2")
	request_vehicle_recording(CARREC_CRASH, str_carrec)
	request_vehicle_recording(CARREC_GANG, str_carrec)
	REQUEST_WAYPOINT_RECORDING("exile2_0")
	REQUEST_WAYPOINT_RECORDING("exile2_1")
	REQUEST_WAYPOINT_RECORDING("exile2_2")
	REQUEST_WAYPOINT_RECORDING("exile2_3")
	request_anim_dict("missexile2")
	REQUEST_ANIM_SET("move_ped_crouched")
	REQUEST_ANIM_SET("Wpn_AssaultRifle_WeaponHoldingCrouched")
	REQUEST_ANIM_SET("move_ped_crouched_strafing")
	REQUEST_ACTION_MODE_ASSET("franklin_ACTION")
	request_weapon_asset(WEAPONTYPE_HEAVYSNIPER)
	request_model(get_weapontype_model(WEAPONTYPE_HEAVYSNIPER))
	REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	//REQUEST_CHOP_CAM_SFX()
	REQUEST_PTFX_ASSET()
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(michael_model)
	or not has_model_loaded(franklin_model)
	or not has_model_loaded(trevor_model)
	or not has_model_loaded(helicopter.model)
	or not has_model_loaded(franklins_car.model)
	or not has_model_loaded(gang_car.model)
	or not has_model_loaded(gang_enemy[3].model)
	or not has_model_loaded(animal[0].model)
	or not has_model_loaded(chop.model)
	or not has_model_loaded(rpg.model)
	or not has_vehicle_recording_been_loaded(001, "lkexile2")
	or not has_vehicle_recording_been_loaded(ambient_car.recording_number, "lkexile2")
	or not has_vehicle_recording_been_loaded(CARREC_CRASH, str_carrec)
	or not has_vehicle_recording_been_loaded(CARREC_GANG, str_carrec)
	or not get_is_waypoint_recording_loaded("exile2_0")
	or not get_is_waypoint_recording_loaded("exile2_1")
	or not get_is_waypoint_recording_loaded("exile2_2")
	or not get_is_waypoint_recording_loaded("exile2_3")
	or not has_anim_dict_loaded("missexile2")
	or not has_anim_set_loaded("move_ped_crouched")
	or not has_anim_set_loaded("Wpn_AssaultRifle_WeaponHoldingCrouched")
	or not has_anim_set_loaded("move_ped_crouched_strafing")
	or not HAS_ACTION_MODE_ASSET_LOADED("franklin_action")
	or not has_weapon_asset_loaded(WEAPONTYPE_HEAVYSNIPER)
	or not has_model_loaded(get_weapontype_model(WEAPONTYPE_HEAVYSNIPER))
	or not REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	//or not REQUEST_CHOP_CAM_SFX()
	or not has_ptfx_asset_loaded()

		wait(0)
	
	endwhile 
	

	clear_ped_tasks_immediately(player_ped_id())
	
	clear_player_wanted_level(player_id())
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	set_max_wanted_level(0)
	set_create_random_cops(false)
	
	SET_ROADS_IN_AREA(vCountryRoadMin, vCountryRoadMax, FALSE)
	
	add_scenario_blocking_area(<<-2607.6, 3715.3, 300.00>>, <<-541.9, 4969.0, -100.00>>) //whole forrest area

	SET_VEHICLE_POPULATION_BUDGET(0) //removes all cars from vehicle population
	SET_PED_POPULATION_BUDGET(0)

	CLEAR_AREA(<<-1912.3848, 4432.4248, 40.8423>>, 10000.0, TRUE)
	
	//SEETHROUGH_SET_MAX_THICKNESS(500.00)
	
	SEETHROUGH_SET_HEATSCALE(tb_dead, 0.0)
	SEETHROUGH_SET_HEATSCALE(tb_cold, 1.0) //animals
	SEETHROUGH_SET_HEATSCALE(tb_warm, 1.0) //peds
	SEETHROUGH_SET_HEATSCALE(tb_hot, 1.0) 
	
	
	helicopter.veh = create_vehicle(helicopter.model, get_position_of_vehicle_recording_at_time(001, 0, "lkexile2"), 0)
	SET_VEHICLE_AS_RESTRICTED(helicopter.veh, 0)
	set_vehicle_extra(helicopter.veh, 12, true) 
	set_vehicle_livery(helicopter.veh, 1)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(helicopter.veh, false)
	set_entity_only_damaged_by_player(helicopter.veh, true)
	set_vehicle_colours(helicopter.veh, 34, 34)
	SET_VEHICLE_EXTRA_COLOURS(helicopter.veh, 0, 0)	
	set_heli_blades_full_speed(helicopter.veh)
	set_vehicle_engine_on(helicopter.veh, true, true)
	start_playback_recorded_vehicle(helicopter.veh, 001, "lkexile2")
	skip_time_in_playback_recorded_vehicle(helicopter.veh, 12000)
	force_playback_recorded_vehicle_update(helicopter.veh)
	
	if get_current_player_ped_enum() != char_franklin
		set_current_selector_ped(selector_ped_franklin, false)
	endif 
	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<-1683.4967, 4454.5952, 1.5660>>) //<<-1874.7228, 4482.9673, 24.4418>>, 334.1932, false)
	set_entity_heading(player_ped_id(), 239.1512)
	SET_ENTITY_LOAD_COLLISION_FLAG(player_ped_id(), true) //setup so when you switch to michael selector ped franklin is set to load collision.
	SET_ENTITY_LOD_DIST(player_ped_id(), 300) 
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)
	force_ped_motion_state(player_ped_id(), MS_ACTIONMODE_idle)
	force_ped_ai_and_animation_update(player_ped_id())
	add_ped_for_dialogue(scripted_speech, 1, player_ped_id(), "franklin")
	setup_weapon_for_ped(player_ped_id(), weapontype_carbinerifle, 100, true)
	franklin_ai_system_status = 100
	
	create_player_ped_on_foot(selector_ped.pedID[selector_ped_michael], char_michael, (get_entity_coords(helicopter.veh) + <<0.0, 0.0, 10.0>>), 258.7014, false) //<<-1874.7228, 4482.9673, 24.4418>>, 334.1932, false)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
	setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	ATTACH_ENTITY_TO_ENTITY(selector_ped.pedID[selector_ped_michael], helicopter.veh, 0, player_heli_offset, <<0.0, 0.0, 90.0>>)
	SET_PED_MOVEMENT_CLIPset(selector_ped.pedID[selector_ped_michael], "move_ped_crouched")
	SET_PED_WEAPON_MOVEMENT_CLIPSET(selector_ped.pedID[selector_ped_michael], "Wpn_AssaultRifle_WeaponHoldingCrouched")
	SET_PED_STRAFE_CLIPSET(selector_ped.pedID[selector_ped_michael], "move_ped_crouched_strafing")
	//SET_PED_DUCKING(selector_ped.pedID[selector_ped_michael], TRUE)
	//TASK_TOGGLE_DUCK(selector_ped.pedID[selector_ped_michael], TOGGLE_DUCK_ON)
	SET_ped_DESIRED_HEADING(selector_ped.pedID[selector_ped_michael], get_entity_heading(selector_ped.pedID[selector_ped_michael]))
	//force_ped_motion_state(selector_ped.pedID[selector_ped_michael], ms_crouch_idle, true)
	GIVE_WEAPON_TO_PED(selector_ped.pedID[selector_ped_michael], WEAPONTYPE_HEAVYSNIPER, 10000, true, true)//infinite_ammo
	GIVE_WEAPON_COMPONENT_TO_PED(selector_ped.pedID[selector_ped_michael], WEAPONTYPE_HEAVYSNIPER, WEAPONCOMPONENT_AT_SCOPE_MAX)
	SET_PED_CAN_SWITCH_WEAPON(selector_ped.pedID[selector_ped_michael], false)
	task_aim_gun_at_coord(selector_ped.pedID[selector_ped_michael], <<-1531.6, 4376.7, 68.00>>, -1)
	set_ped_reset_flag(selector_ped.pedID[selector_ped_michael], PRF_InstantBlendToAim, true)
	force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael], true)
	SET_SELECTOR_PED_ACTIVITY(selector_ped, SELECTOR_PED_MICHAEL, SELECTOR_ACTIVITY_ATTACHED_TO_HELI)
	SET_SELECTOR_PED_ACTIVITY(selector_ped, SELECTOR_PED_MICHAEL, SELECTOR_ACTIVITY_THERMAL_VISION)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
	
	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_trevor], char_trevor, helicopter.veh) 
	setup_buddy_attributes(selector_ped.pedID[selector_ped_trevor])
	setup_relationship_contact(selector_ped.pedID[selector_ped_trevor], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_trevor], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	add_ped_for_dialogue(scripted_speech, 2, selector_ped.pedID[selector_ped_trevor], "trevor")
	set_selector_ped_blocked(selector_ped, selector_ped_trevor, true)
	
	chop.pos = <<-1680.68384, 4452.64648, 1.55916>>//<<-1677.73071, 4454.21191, 1.2653>>
	chop.heading = 257.7928//277
	setup_chop(chop)
	remove_blip(chop.blip)
	setup_relationship_animal(chop.ped)
	SET_ENTITY_LOAD_COLLISION_FLAG(chop.ped, true)
	SET_ENTITY_LOD_DIST(chop.ped, 400)
	chop_ai_system_status = 200 //101
	set_selector_ped_blocked(selector_ped, selector_ped_trevor, false)
	SET_SELECTOR_PED_ICON(selector_ped, SELECTOR_PED_TREVOR, SELECTOR_ICON_CHOP)
	chop_cam.selectorSlot = SELECTOR_PED_TREVOR
	chop_cam.chopPed = chop.ped
	CREATE_CHOP_CAM(chop_cam)
	chop_cam.state = CHOP_VIEW_CAM_DISABLED
	set_chop_view_cam_disabled(chop_cam, selector_ped)
	chop_ai_system()

	setup_enemy(gang_enemy[3])
	SET_PED_CAN_BE_DRAGGED_OUT(gang_enemy[3].ped, false)
	remove_blip(gang_enemy[3].blip)
	SET_ENTITY_LOAD_COLLISION_FLAG(gang_enemy[3].ped, true)
	SET_PED_HEATSCALE_OVERRIDE(gang_enemy[3].ped, 1)
	set_entity_coords(gang_enemy[3].ped, gang_enemy[3].run_to_pos)
	set_entity_heading(gang_enemy[3].ped, 118.2162)
	manage_gang_enemy_3_status = 7
	iRandTime = GET_RANDOM_INT_IN_RANGE(5000, 10000)
	add_ped_for_dialogue(scripted_speech, 6, gang_enemy[3].ped, "oneil") 

	gang_car.veh = CREATE_VEHICLE(gang_car.model, <<-1873.92, 4486.02, 24.625>>, 239.264404)
	set_vehicle_colours(gang_car.veh, 7, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(gang_car.model) 
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(gang_car.veh, false)
	//force_playback_recorded_vehicle_update(franklins_car.veh)	
	SET_VEHICLE_DAMAGE(gang_car.veh, <<-0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DAMAGE(gang_car.veh, <<0.2, 0.5, 0.0>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DAMAGE(gang_car.veh, <<0.3, -0.1, 0.75>>, 200.00, 200.00, TRUE)
	SET_VEHICLE_DOOR_BROKEN(gang_car.veh, SC_DOOR_BONNET, true)
	set_vehicle_on_ground_properly(gang_car.veh)
	
	franklins_car.veh = CREATE_VEHICLE(franklins_car.model, <<-2442.8577, 3670.4836, 12.4540>>)
	set_vehicle_has_strong_axles(franklins_car.veh, true)
	set_entity_only_damaged_by_player(franklins_car.veh, true)
	SET_VEHICLE_COLOURS(franklins_car.veh, 12, 12)
	START_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh, CARREC_CRASH, str_carrec)
	skip_time_in_playback_recorded_vehicle(franklins_car.veh, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(CARREC_CRASH, str_carrec) - 100.0)
	force_playback_recorded_vehicle_update(franklins_car.veh)		
	STOP_PLAYBACK_RECORDED_VEHICLE(franklins_car.veh)	

	for i = 10 to count_of(animal) - 1
		setup_animal(animal[i])
		animal_ai_status[i] = 0
		animal[i].created = false
	endfor  
	
	ambient_car.veh = create_vehicle(ambient_car.model, ambient_car.pos, ambient_car.heading)
	set_vehicle_doors_locked(ambient_car.veh, vehiclelock_lockout_player_only)
	set_vehicle_on_ground_properly(ambient_car.veh)

	REMOVE_VEHICLE_RECORDING(CARREC_CRASH, str_carrec)
	REMOVE_VEHICLE_RECORDING(CARREC_GANG, str_carrec)	
	
	//setup heli cam
	heli_cam = CREATE_CAM("default_scripted_camera", true)
	ATTACH_CAM_TO_ENTITY(heli_cam, helicopter.veh, <<-0.390, 0.650, 1.350>>)
	SET_CAM_FOV(heli_cam, 45.0)
	// rotate camera this frame
	SET_CAMERA_ROT_TO_SAME_AS_veh(helicopter.veh, heli_cam, <<current_cam_rot.x, 0.0, current_cam_rot.z>>)
	helicopter_cam_status = 1
	
	
//	SET_SELECTOR_PED_HINT(selector_ped, selector_ped_michael, true)
//	SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor, true)
	
	if not replay_active
		load_scene(<<-1683.4967, 4454.5952, 1.5660>>)
	endif 
	
	wait(0)
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(player_ped_id())
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(chop.ped)
	
		wait(0)
	
	endwhile
	
	trigger_music_event("EXL2_ON_FOOT_RT")
	
	//flags enemies as created and dead for ped_structure_are_all_enemies_dead()
	for i = 0 to 1
		gang_enemy[i].created = true
	endfor 
	
	//setting up flags to put mission systems in the correct state for the skip
	allow_switch = true
	initialise_sniper_aim_at_pos = true  //stops the realtive gameplay heading snapping to 0 when already set to aim at michael in this section when the trigger is pressed
	rpg_system_status = 3
	activate_helicopter_flying_system = true
	bPlayingHoverBackwards = false
	dialogue_system_status = 10
	snipe_enemies_master_flow_system_status = 4
	set_label_as_triggered("TM44_TWT", true)
	set_label_as_triggered("TM44_WATER", true)
	set_label_as_triggered("TM44_DWATER", true)
	set_label_as_triggered("TM44_GWOOD", true)
	set_label_as_triggered("TM44_DEADR", true)

	
	snipe_enemies_skip_status = 2
	
	sniper_rifle_help_text_system_status = 99

	e_section_stage = SECTION_STAGE_RUNNING
	mission_flow = STAGE_SNIPE_ENEMIES
	
	end_cutscene(false, 22.0, 0, false)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "shooting the rpg enemy")
	
endproc 

proc load_replay_skip_5()

	int i = 0
	
	if replay_active
		start_replay_setup(<<-1607.5647, 4370.1123, 65.1>>, 336.5597, false)
	endif 

	request_model(michael_model)
	request_model(franklin_model)
	request_model(trevor_model)
	request_model(helicopter.model)
	request_model(animal[0].model)
	request_model(chop.model)
	request_anim_dict("missexile2")
	REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	//REQUEST_CHOP_CAM_SFX()
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(michael_model)
	or not has_model_loaded(franklin_model)
	or not has_model_loaded(trevor_model)
	or not has_model_loaded(helicopter.model)
	or not has_model_loaded(animal[0].model)
	or not has_model_loaded(chop.model)
	or not has_anim_dict_loaded("missexile2")
	or not REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	//or not REQUEST_CHOP_CAM_SFX()

		wait(0)
	
	endwhile 
	
	SET_VEHICLE_POPULATION_BUDGET(1)
	SET_PED_POPULATION_BUDGET(1)
	
	add_scenario_blocking_area(<<-2607.6, 3715.3, 300.00>>, <<-541.9, 4969.0, -100.00>>) //whole forrest area

	CLEAR_AREA(<<-1912.3848, 4432.4248, 40.8423>>, 10000.0, TRUE)

	//collect franklin and chop with the helicopter.

	helicopter.veh = create_vehicle(helicopter.model, <<-1607.5647, 4370.1123, 65.1>>, 336.5597)
	SET_VEHICLE_AS_RESTRICTED(helicopter.veh, 0)
	set_vehicle_livery(helicopter.veh, 1)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(helicopter.veh, false) 
	set_entity_only_damaged_by_player(helicopter.veh, true)
	set_vehicle_colours(helicopter.veh, 34, 34)
	SET_VEHICLE_EXTRA_COLOURS(helicopter.veh, 0, 0)	
	set_heli_blades_full_speed(helicopter.veh)
	set_vehicle_engine_on(helicopter.veh, true, true)
	
	set_current_selector_ped(selector_ped_trevor, false)
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	set_ped_into_vehicle(player_ped_id(), helicopter.veh)

	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, helicopter.veh, vs_front_right)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
	setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")

	create_player_ped_on_foot(selector_ped.pedID[selector_ped_franklin], char_franklin, <<-1563.7963, 4589.2729, 18.5544>>, 178.1914, false) //<<-1874.7228, 4482.9673, 24.4418>>, 334.1932, false)
	franklins_blip = create_blip_for_entity(selector_ped.pedID[selector_ped_franklin])
	setup_buddy_attributes(selector_ped.pedID[selector_ped_franklin])
	setup_relationship_contact(selector_ped.pedID[selector_ped_franklin], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	setup_weapon_for_ped(selector_ped.pedID[selector_ped_franklin], weapontype_carbinerifle, 100, true)
	add_ped_for_dialogue(scripted_speech, 1, selector_ped.pedID[selector_ped_franklin], "franklin")
	franklin_run_to_pos = <<-1563.7963, 4589.2729, 18.5544>>
	

	chop.pos = <<-1561.6675, 4585.8989, 18.3892>>
	chop.heading = 28.3876
	chop.run_to_pos = <<-1561.6675, 4585.8989, 18.3892>>
	setup_chop(chop)
	setup_relationship_animal(chop.ped)
	SET_ENTITY_LOAD_COLLISION_FLAG(chop.ped, true)
	
	for i = 10 to count_of(animal) - 1
		setup_animal(animal[i])
		animal_ai_status[i] = 0
		animal[i].created = false
	endfor  

	closest_warp_pos_to_franklin = 1
	franklin_ai_system_status = 200
	chop_ai_system_status = 400
	
	sniper_rifle_help_text_system_status = 99
	
	mission_flow = stage_pickup_franklin_and_chop
	
	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	start_audio_scene("EXILE_2_HELI_PICK_UP")
	
	end_cutscene()
	
	print_now("FS_GOD_2", default_god_text_time, 1)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "pickup franklin and chop", true)

endproc 

proc load_replay_skip_6()

	if replay_active
		start_replay_setup(<<1731.0, 3273.7, 40.5>>, 264.5150)
	endif 

	request_model(michael_model)
	request_model(franklin_model)
	request_model(trevor_model)
	request_model(chop.model)
	request_model(helicopter.model)
	request_anim_dict("missexile2")
	//request_cutscene
	
	if replay_active
		END_REPLAY_SETUP()
	endif 

	while not has_model_loaded(michael_model)
	or not has_model_loaded(franklin_model)
	or not has_model_loaded(trevor_model)
	or not has_model_loaded(chop.model)
	or not has_model_loaded(helicopter.model)
	//or not has_cutscene_loaded()
	or not has_anim_dict_loaded("missexile2")

		wait(0)
		
	endwhile 
	
	clear_area(<<1731.0, 3273.7, 40.5>>, 1000, true)
	
	helicopter.veh = create_vehicle(helicopter.model, <<1731.0, 3273.7, 40.5>>, 264.5150)
	SET_VEHICLE_AS_RESTRICTED(helicopter.veh, 0)
	set_vehicle_livery(helicopter.veh, 1)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(helicopter.veh, false)
	set_entity_only_damaged_by_player(helicopter.veh, true)
	set_vehicle_colours(helicopter.veh, 34, 34)
	SET_VEHICLE_EXTRA_COLOURS(helicopter.veh, 0, 0)	
	set_heli_blades_full_speed(helicopter.veh)
	set_vehicle_engine_on(helicopter.veh, true, true)
		
	set_current_selector_ped(selector_ped_trevor, false)
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	set_ped_into_vehicle(player_ped_id(), helicopter.veh)

	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, helicopter.veh, vs_front_right)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")

	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_franklin], char_franklin, helicopter.veh, vs_back_right)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	add_ped_for_dialogue(scripted_speech, 1, selector_ped.pedID[selector_ped_franklin], "franklin")

	chop.pos = get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, 2.0>>)
	setup_chop(chop)
	remove_blip(chop.blip)
	setup_relationship_animal(chop.ped)
	SET_ENTITY_LOAD_COLLISION_FLAG(chop.ped, true)
	//chop_ai_system_status = 200 //101
	set_selector_ped_blocked(selector_ped, selector_ped_trevor, false)
	SET_SELECTOR_PED_ICON(selector_ped, SELECTOR_PED_TREVOR, SELECTOR_ICON_CHOP)
	chop_cam.selectorSlot = SELECTOR_PED_TREVOR
	chop_cam.chopPed = chop.ped
	CREATE_CHOP_CAM(chop_cam)
	chop_cam.state = CHOP_VIEW_CAM_DISABLED
	set_chop_view_cam_disabled(chop_cam, selector_ped)
	//ATTACH_ENTITY_TO_ENTITY(chop.ped, helicopter.veh, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	attach_entity_to_entity(chop.ped, helicopter.veh, GET_ENTITY_BONE_INDEX_BY_NAME(helicopter.veh, "seat_dside_r"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	task_play_anim(chop.ped, "missexile2", "Chop_Sit_In_Frogger_RDS", normal_blend_in, normal_blend_out, -1, af_looping) 
								 
	request_cutscene("EXL_2_MCS_3")
	
	while not has_cutscene_loaded()
	
		wait(0)
		
	endwhile

	if is_vehicle_driveable(helicopter.veh)
		register_entity_for_cutscene(helicopter.veh, "main_heli", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, helicopter.model)
	endif 
	
	if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
		register_entity_for_cutscene(selector_ped.pedID[selector_ped_michael], "michael", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
	endif 
	
	if not is_ped_injured(selector_ped.pedID[selector_ped_franklin])
		register_entity_for_cutscene(selector_ped.pedID[selector_ped_franklin], "franklin", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
	endif 
	
	if not is_ped_injured(chop.ped)
		register_entity_for_cutscene(chop.ped, "chop", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, chop.model)
	endif 
	
	if is_entity_attached(chop.ped)
		detach_entity(chop.ped)
	endif 

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
	
	start_cutscene()
	
	while not is_cutscene_playing()
		
		wait(0)
		
	endwhile 
	
	SET_SRL_POST_CUTSCENE_CAMERA(<<1742.7886, 3280.7942, 40.0904>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 77.8678>>))
	
	SET_VEHICLE_ENGINE_ON(helicopter.veh, FALSE, FALSE)

	if not replay_active
		load_scene(<<1731.0, 3273.7, 40.5>>)
	endif 
	
	do_screen_fade_in(default_fade_time)
	
	pass_mocap_status = 1
	
	mission_flow = stage_pass_mocap

endproc 

func bool is_franklin_camera_switch_ready(int target_jump_cut_index)
			
	if IS_PLAYER_SWITCH_IN_PROGRESS()

		SWITCH_STATE camera_switch_state = GET_PLAYER_SWITCH_STATE() 
		
		//Alwyn - fix for PT 1240172
		IF camera_switch_state >= SWITCH_STATE_JUMPCUT_DESCENT 
			IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() < target_jump_cut_index
				RETURN TRUE 
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		PRINTSTRING("is_franklin_camera_switch_ready - FALSE (")
		PRINTSTRING(Get_String_From_Switch_State(camera_switch_state))
		PRINTSTRING(", cut ")
		PRINTINT(GET_PLAYER_SWITCH_JUMP_CUT_INDEX())
		PRINTSTRING(")")
		PRINTNL()
		#ENDIF
		
	else 
	
		script_assert("switch has finished before mission asserts loaded has trigered")
		
		return true 

	endif 

	return false 

endfunc 

proc load_repeat_play_assets()

	request_model(get_player_ped_model(char_michael))

	while not has_model_loaded(get_player_ped_model(char_michael))
	
		wait(0)
		
	endwhile 

	clear_area(<<1974.9646, 3814.8625, 32.4249>>, 5.0, true)
	set_current_selector_ped(selector_ped_michael, false)
	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<1974.9646, 3814.8625, 32.4249>>)
	set_entity_heading(player_ped_id(), 80.0455)


	NEW_LOAD_SCENE_START(<<1974.9646, 3814.8625, 32.4249>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 80.0455>>), 100.0)

	while not IS_NEW_LOAD_SCENE_LOADED()

		wait(0)
		
	endwhile
			
	NEW_LOAD_SCENE_STOP()
	

	
	REQUEST_CUTSCENE("Exile_2_int")
	
	while not has_cutscene_loaded()
	
		wait(0)
		
	endwhile 

	switch get_current_player_ped_enum() 
	
		case char_michael
	
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(char_trevor))
			
		break 

		case char_trevor 
		
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "michael", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(char_michael))
		
		break 
	
	endswitch 
	
	set_scripts_safe_for_cutscene(true)
	
	start_cutscene()
	
	
	while not is_cutscene_playing()

		wait(0)
		
	endwhile 
		
	SET_SRL_POST_CUTSCENE_CAMERA(<<1981.1268, 3830.4668, 31.4241>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 296.00>>))
	
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1994.075, 3816.573, 31.096>>, <<1983.271, 3834.697, 35.396>>, 22.50, <<1994.9022, 3822.9993, 31.1571>>, 211.22, <<15.0, 15.0, 15.0>>)
	set_players_last_vehicle_to_vehicle_gen(<<1994.9022, 3822.9993, 31.1571>>, 211.22)
	
	vehicle_index test_vehicle
	test_vehicle = GET_PLAYERS_LAST_VEHICLE()
	if is_vehicle_driveable(test_vehicle)
		printstring("get_entity_coords(test_vehicle) = ")
		printvector(get_entity_coords(test_vehicle))
		printnl()
	endif 
	
	DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(char_trevor)
	
	clear_area(<<1984.2650, 3818.7625, 31.3800>>, 10000.00, true)
	
	intro_mocap_status = 1
	e_section_stage = section_stage_running	
	mission_flow = STAGE_OPENING_MOCAP	
	
	do_screen_fade_in(default_fade_time)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "start of mission")

endproc 

PROC MISSION_SETUP()

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	SUPPRESS_EMERGENCY_CALLS()
	
	set_wanted_level_multiplier(0.5)
	
	SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(1.0)
	
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(), EXL2_DAMAGE)
//	INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(player_ped_id()) 

	INIT_PC_SCRIPTED_CONTROLS("Exile2_Thermal_Scope")
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_CANYON_RIVERBED, BUILDINGSTATE_DESTROYED) 
	
	add_relationship_groups()
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	
	//REGISTER_SCRIPT_WITH_AUDIO()
	
	REQUEST_ADDITIONAL_TEXT("FORSNIP", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("TM44AUD", MISSION_DIALOGUE_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		WAIT(0)
	ENDWHILE

	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	
	vCountryRoadMin = << -2353.0459, 4401.4536, 0.0>>
	vCountryRoadMax = <<-1198.5111,   4483.3555, 100.0>>
	
	//SET_ENTITY_HEADING(PLAYER_PED_ID(), 217.2759)
	
	
	SET_VEHICLE_POPULATION_BUDGET(1)
	SET_PED_POPULATION_BUDGET(1)
	
	
	switch_off_vehicle_generators_for_mission()
	
	add_scenario_blocking_area(<<-1692.00, 4577.00, 120.000>>, <<-2112.00, 4339.00, -100.00>>) //area near crash cutscene

//	 Force the current one to respawn at new location
//	 Set the position you want the vehicle to spawn at.
	UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, <<1702.9482, 3272.0217, 40.1539>>, 247.3247)

	// Force the current one to respawn at new location
	DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
	CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)//forces 

	initialise_mission_variables()
	
	chop_behaviour = GET_CHOP_BEHAVIOUR_FROM_APP()
	#IF IS_DEBUG_BUILD
		chop_behaviour = CHOP_BEHAVIOUR_good
	#endif 
	
	BLOCK_MISSION_TITLE(TRUE) 
	
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, true)

	IF (Is_Replay_In_Progress())
	
		replay_active = true
	
		if not g_bShitskipAccepted
		
			SWITCH Get_Replay_Mid_Mission_Stage()
			
				case 0
					
					load_replay_skip_0() //start of mission
				
				break 
				
				CASE 1
			
					load_replay_skip_1() //start of car chase
					
				BREAK
				
				case 2
				
					load_replay_skip_2() //fly to franklin

				break 
				
				CASE 3
					
					load_replay_skip_3() //start of helicopter sniper section. 

				BREAK
				
				CASE 4
					
					load_replay_skip_4() //Franklin on the ground one enemy left. 

				BREAK
				
				case 5
				
					load_replay_skip_5()//Pickup franklin and chop
				
				break 
				
			ENDSWITCH
			
		else 
		
			int shit_skip_status 
			
			shit_skip_status = Get_Replay_Mid_Mission_Stage() + 1
			
			SWITCH shit_skip_status
			
				case 1
				
					load_replay_skip_1()
					
				break 
				
				case 2
				
					load_replay_skip_2()
				
				break 
				
				case 3
				
					load_replay_skip_3()
				
				break 
				
				case 4
				
					load_replay_skip_4()
				
				break 
				
				case 5
				
					load_replay_skip_5()
				
				break 
				
				case 6
				
					load_replay_skip_6()
				
				break 
				
			endswitch 
			
		endif 
		
		replay_active = false
		
	ELSE
	
		if not is_repeat_play_active()
	
			//if get_current_player_ped_enum() = char_franklin
			if IS_PLAYER_SWITCH_IN_PROGRESS()
			
				mission_flow = switching_into_franklin
			
			endif 

			if not is_screen_faded_in()
				if not is_screen_fading_in()
					do_screen_fade_in(default_fade_time)
				endif 
			endif 
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "start of mission")  
			
		else 
		
			load_repeat_play_assets()

		endif 
	ENDIF 
	
	//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "start of mission")
	
	//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)

ENDPROC

proc mission_pass_conversation()

	switch mission_pass_conversation_status 
	
		case 0
		
			switch get_current_player_ped_enum()
			
				case char_michael
				
					IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_ENDM", CONV_PRIORITY_medium) 
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
						
						mission_pass_conversation_status++
					endif 
				
				break 
				
				case char_franklin
				
					IF CREATE_CONVERSATION(scripted_speech, "TM44AUD", "TM44_ENDF", CONV_PRIORITY_medium) 
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
						
						mission_pass_conversation_status++
					endif 
				
				break 
				
			endswitch 
			
		break 
		
		case 1
		
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
				original_time = get_game_timer()
			
				mission_flow = stage_switch_to_trevor
				
			endif 
		
		break 
		
	endswitch 
	
	if does_entity_exist(rpg_obj) //keeps rocket moving incase player kills the last enemy as he has just fired the rocket.
		rpg_system()
	endif 
	
	chop_ai_system()
	handle_chop_cam_hidof(chop_cam)
	
	helicopter_camera_system() 
	
	thermal_vision_system()
	
	MAINTAIN_CHOPPER_HOVER(helicopter.veh, (get_total_duration_of_vehicle_recording(001, "lkexile2") - 12000), (get_total_duration_of_vehicle_recording(001, "lkexile2") - 500)) //-500
	
endproc 


proc switch_to_trevor()

	switch switch_to_trevor_status 
	
		case 0
		
			set_selector_ped_hint(selector_ped, selector_ped_michael, false)
			set_selector_ped_hint(selector_ped, selector_ped_franklin, false)
		
			set_selector_ped_blocked(selector_ped, selector_ped_michael, true)
			set_selector_ped_blocked(selector_ped, selector_ped_franklin, true)

			set_selector_ped_blocked(selector_ped, selector_ped_trevor, false)
			set_selector_ped_hint(selector_ped, selector_ped_trevor, true)
			SET_SELECTOR_PED_ICON(selector_ped, SELECTOR_PED_TREVOR, SELECTOR_ICON_DEFAULT)
			set_selector_ped_fake_state(selector_ped, selector_ped_trevor, selector_state_default)

			allow_switch = true
			
			IF chop_cam.state = CHOP_VIEW_CAM_ON_CHOP

				make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
					
				helicopter_switch_system_status = 1
			
				switch get_current_player_ped_enum()
					
					case char_michael
						
						setup_switch_cam_to_trevor_from_michael_chop()
						SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
					break  
						
					case char_franklin
						
						setup_switch_cam_to_trevor_from_franklin_chop()
						SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
							
					break 
						
				endswitch 
				
			endif 

			original_time = get_game_timer()
			
			switch_to_trevor_status++

			
		
		break 
	
		case 1

			if is_player_switch_in_progress() //incase player manually forces the switch

				closest_warp_pos_to_franklin = get_closest_pos_to_player(pickup_pos)

				switch_to_trevor_status++
				
			else
		
				if lk_timer(original_time, 3000)

					allow_switch = true
					make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
					
					helicopter_switch_system_status = 1
						
					switch get_current_player_ped_enum()
					
						case char_michael
			
							IF chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
								setup_switch_cam_to_trevor_from_michael()
							else 
								setup_switch_cam_to_trevor_from_michael_chop()
							endif 
							
						break 
						
						case char_franklin
						
							IF chop_cam.state != CHOP_VIEW_CAM_ON_CHOP
								setup_switch_cam_to_trevor_from_franklin()
							else 
								setup_switch_cam_to_trevor_from_franklin_chop()
								SET_CHOP_VIEW_CAM_to_player(chop_cam, Selector_Ped, TRUE)
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
							endif 
							
						break 
						
					endswitch 
				
					closest_warp_pos_to_franklin = get_closest_pos_to_player(pickup_pos)

					switch_to_trevor_status++
					
				endif 
			
			endif 

		break 
		
		case 2
		
			if get_current_player_ped_enum() = char_trevor
			and not is_player_switch_in_progress()
			
				remove_ANIM_SET("move_ped_crouched")
				remove_ANIM_SET("Wpn_AssaultRifle_WeaponHoldingCrouched")
				remove_ANIM_SET("move_ped_crouched_strafing")
				
				remove_VEHICLE_RECORDING(helicopter.recording_number, str_carrec)
				remove_VEHICLE_RECORDING(CARREC_HELI_HOVER, str_carrec)
				remove_VEHICLE_RECORDING(007, str_carrec)
				remove_vehicle_recording(001, "lkexile2")
				remove_vehicle_recording(ambient_car.recording_number, "lkexile2")
				
//				if is_playback_going_on_for_vehicle(helicopter.veh)
//					stop_playback_recorded_vehicle(helicopter.veh)
//				endif 
//				
//				set_entity_coords(helicopter.veh, <<-1622.4403, 4350.1313, 86.4732>>)
//				set_entity_heading(helicopter.veh, 344.9161)
//				force_entity_ai_and_animation_update(helicopter.veh)
				
				set_vehicle_as_no_longer_needed(gang_car.veh)
				set_model_as_no_longer_needed(gang_car.model)
				
				set_vehicle_as_no_longer_needed(franklins_car.veh)
				set_model_as_no_longer_needed(franklins_car.model)
				
				set_vehicle_as_no_longer_needed(ambient_car.veh)
				set_model_as_no_longer_needed(ambient_car.model)
				
				remove_vehicle_recording(helicopter.recording_number, str_carrec)
				remove_vehicle_recording(CARREC_HELI_HOVER, str_carrec)
				remove_vehicle_recording(007, str_carrec)
				remove_vehicle_recording(001, "lkexile2")
				remove_vehicle_recording(ambient_car.recording_number, "lkexile2")
				
				set_model_as_no_longer_needed(rpg.model)
				
				remove_ptfx_asset()
				
				SET_VEHICLE_POPULATION_BUDGET(1)
				SET_PED_POPULATION_BUDGET(1)
				
				switch closest_warp_pos_to_franklin //set when the player switches to Trevor
					
					case 0
					
						clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_franklin])
						set_entity_coords(selector_ped.pedID[selector_ped_franklin], <<-1594.5796, 4770.9741, 50.0451>>)
						set_entity_heading(selector_ped.pedID[selector_ped_franklin], 214.7628)
						franklin_run_to_pos = <<-1594.5796, 4770.9741, 50.0451>>
						
						clear_ped_tasks_immediately(chop.ped)
						set_entity_coords(chop.ped, <<-1593.29163, 4770.80811, 49.94669>>)
						set_entity_heading(chop.ped, 214.7628)
						chop.run_to_pos = <<-1593.29163, 4770.80811, 49.94669>>
					
					break 
					
					case 1
					
						clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_franklin])
						set_entity_coords(selector_ped.pedID[selector_ped_franklin], <<-1563.7963, 4589.2729, 18.5544>>)
						set_entity_heading(selector_ped.pedID[selector_ped_franklin], 178.1914)
						franklin_run_to_pos = <<-1563.7963, 4589.2729, 18.5544>>
						
						clear_ped_tasks_immediately(chop.ped)
						set_entity_coords(chop.ped, <<-1561.6675, 4585.8989, 18.3892>>)
						set_entity_heading(chop.ped, 28.3876)
						chop.run_to_pos = <<-1561.6675, 4585.8989, 18.3892>>
					
					break 
					
				endswitch 
				
				if not does_blip_exist(franklins_blip)
					franklins_blip = create_blip_for_entity(selector_ped.pedID[selector_ped_franklin])
				endif 
				
				if not does_blip_exist(chop.blip)
					chop.blip = create_blip_for_entity(chop.ped)
				endif 
				
				START_AUDIO_SCENE("EXILE_2_HELI_PICK_UP")
				
				franklin_ai_system_status = 200
				chop_ai_system_status = 400
			
				mission_flow = stage_pickup_franklin_and_chop
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "pickup franklin and chop", true)
				
				print_now("FS_GOD_2", default_god_text_time, 1)

			endif 
		
		break 
	
	endswitch
	
	if does_entity_exist(rpg_obj) //keeps rocket moving incase player kills the last enemy as he has just fired the rocket.
		rpg_system()
	endif 
	
	chop_ai_system()
	handle_chop_cam_hidof(chop_cam)
	
	MAINTAIN_CHOPPER_HOVER(helicopter.veh, (get_total_duration_of_vehicle_recording(001, "lkexile2") - 12000), (get_total_duration_of_vehicle_recording(001, "lkexile2") - 500)) //-500
	
	helicopter_switch_system()
	
	helicopter_camera_system()

endproc 

proc pickup_franklin_and_chop_dialogue_system()

	switch pickup_franklin_and_chop_dialogue_system_status 
	
		case 0
		
			if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
				if create_conversation(scripted_speech, "TM44AUD", "TM44_PICKUP", CONV_PRIORITY_medium) 
					pickup_franklin_and_chop_dialogue_system_status++
				endif 
			endif 
		
		break 
		
		case 1
		
			//if is_ped_sitting_in_vehicle(player_ped_id(), helicopter.veh)
			//and is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael])
			if does_blip_exist(locates_data.LocationBlip)
				
				if is_entity_at_coord(player_ped_id(), pickup_pos[closest_warp_pos_to_franklin], <<10.00, 10.00, 5.00>>)
					
					if IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh)
					and get_entity_speed(helicopter.veh) < 0.2
						
						if create_conversation(scripted_speech, "TM44AUD", "TM44_LANDS", CONV_PRIORITY_medium)
							
							REPLAY_RECORD_BACK_FOR_TIME(8.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							
							pickup_franklin_and_chop_dialogue_system_status++
						endif 
					endif 
				endif 
			endif 
			
		break 
		
		case 2
		
		break 
		
	endswitch 
	
endproc 

func bool pickup_franklin_and_chop()

	int i

	is_player_at_location_in_vehicle(locates_data, pickup_pos[closest_warp_pos_to_franklin], <<0.01, 0.01, 0.01>>, false, helicopter.veh, "", "", "")
	
	if does_blip_exist(locates_data.LocationBlip)
	and is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_franklin], helicopter.veh)
	and is_entity_attached_to_entity(chop.ped, helicopter.veh)
	
		if create_conversation(scripted_speech, "TM44AUD", "TM44_GETIN", CONV_PRIORITY_medium) 
		
			CLEAR_FOCUS()
			
			clear_mission_locate_stuff(locates_data, true)
	
			if does_blip_exist(franklins_blip)
				remove_blip(franklins_blip)
			endif
			
			if does_blip_exist(chop.blip)
				remove_blip(chop.blip)
			endif
			
			for i = 0 to count_of(animal) - 1
				if does_entity_exist(animal[i].ped)
					set_ped_as_no_longer_needed(animal[i].ped)
				endif 
				set_model_as_no_longer_needed(animal[i].model)
			endfor 

			
			for i = 0 to count_of(gang_enemy) - 1
				if does_entity_exist(gang_enemy[i].ped)
					set_ped_as_no_longer_needed(gang_enemy[i].ped)
					set_model_as_no_longer_needed(gang_enemy[i].model)
				endif 
			endfor
			
			REPLAY_RECORD_BACK_FOR_TIME(3)

			mission_flow = stage_get_to_airstrip
			
			return true 
			
		endif 
	
	endif 

	if does_blip_exist(locates_data.LocationBlip)

		if is_entity_at_coord(player_ped_id(), pickup_pos[closest_warp_pos_to_franklin], <<20.00, 20.00, 5.5>>) //<<10.00, 10.00, 5.00>>)
		
			//remove_blip(locates_data.LocationBlip)
			set_blip_alpha(locates_data.LocationBlip, 0)

		else 
		
			set_blip_alpha(locates_data.LocationBlip, 255)
		
			is_entity_at_coord(player_ped_id(), pickup_pos[closest_warp_pos_to_franklin], <<20.00, 20.00, LOCATE_SIZE_HEIGHT>>, true)
		
		endif 
		
	endif
	
	franklin_ai_system()
	
	chop_ai_system()
	
	pickup_franklin_and_chop_dialogue_system()
	
	return false 

endfunc 

proc get_to_airstrip_dialogue_system()
	
	switch get_to_airstrip_dialogue_system_staus 
	
		case 0

			if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
				if create_conversation(scripted_speech, "TM44AUD", "TM44_AIR1", CONV_PRIORITY_low)  
					get_to_airstrip_dialogue_system_staus++
				endif 
			endif 
			
		break 
		
		case 1
		
			if in_car_dialogue_monitoring_system(locates_data.LocationBlip)
				get_to_airstrip_dialogue_system_staus = 2
			endif 
			
			if does_blip_exist(locates_data.LocationBlip)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), <<1744.9111, 3254.2053, 39.8>>) < 50.00 
					if get_entity_height_above_ground(helicopter.veh) > 5.0
						if create_conversation(scripted_speech, "TM44AUD", "TM44_LAND", CONV_PRIORITY_medium)
							get_to_airstrip_dialogue_system_staus = 3
						endif 
					endif 
				endif 
			endif 
			
		break 
		
		case 2
		
			if does_blip_exist(locates_data.LocationBlip)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), <<1744.9111, 3254.2053, 39.8>>) < 50.00 
					if get_entity_height_above_ground(helicopter.veh) > 5.0
						if create_conversation(scripted_speech, "TM44AUD", "TM44_LAND", CONV_PRIORITY_medium)
							get_to_airstrip_dialogue_system_staus++
						endif 
					endif 
				endif 
			endif 

		break 
		
		case 3
		
		break 
		
	endswitch 

endproc 


func bool get_to_airstrip()

	switch get_to_airstrip_status 
	
		case 0
		
			mocap_streaming_system(<<1731.0, 3273.7, 40.5>>, DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_UNLOAD_DIST, "EXL_2_MCS_3") 
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("chop", chop.ped, chop.model)
		
			is_player_at_location_with_buddies_in_vehicle(locates_data, <<1731.0, 3273.7, 40.5>>, <<0.01, 0.01, 0.01>>, false,
			selector_ped.pedID[selector_ped_michael], selector_ped.pedID[selector_ped_franklin], null, helicopter.veh,
			"FS_HELI_2", "FS_michael_0", "FS_franklin_0", "FS_chop_0", "", "FS_HELI_0", "FS_HELI_1")  
			
			if does_blip_exist(locates_data.LocationBlip)
			
				if is_entity_at_coord(player_ped_id(), <<1731.0, 3273.7, 40.5>>, <<20.0, 20.0, LOCATE_SIZE_HEIGHT>>, true)
			
					if IS_VEHICLE_ON_ALL_WHEELS(helicopter.veh) and get_entity_speed(helicopter.veh) < 0.2
					
						if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						
							if start_new_cutscene_no_fade()
							
								clear_mission_locate_stuff(locates_data, true)
								
								if IS_AUDIO_SCENE_ACTIVE("EXILE_2_HELI_PICK_UP")
									STOP_AUDIO_SCENE("EXILE_2_HELI_PICK_UP")
								endif 
							
								mission_flow = stage_pass_mocap

								return true 
								
							endif 
							
						endif 
						
					endif 
								
				endif 
			
			endif
			
			if not does_entity_exist(pass_cutscene_vehicle.veh)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), <<1731.0, 3273.7, 40.5>>) < 500.00
					
					request_model(pass_cutscene_vehicle.model)
					
					if has_model_loaded(pass_cutscene_vehicle.model)
					
						pass_cutscene_vehicle.veh = create_vehicle(pass_cutscene_vehicle.model, pass_cutscene_vehicle.pos, pass_cutscene_vehicle.heading)
						set_vehicle_on_ground_properly(pass_cutscene_vehicle.veh)
						set_vehicle_colours(pass_cutscene_vehicle.veh, 0, 0)
						set_vehicle_number_plate_text(pass_cutscene_vehicle.veh, "22LJK483")
						
					endif 
				endif 
			endif 
				
				
			get_to_airstrip_dialogue_system()

		break 
		
		case 1
		
		break 

	endswitch 
	
	return false

endfunc 

proc pass_mocap()

	switch pass_mocap_status 
	
		case 0

			if is_entity_attached(chop.ped)
				detach_entity(chop.ped)
			endif 

			register_entity_for_cutscene(helicopter.veh, "main_heli", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, helicopter.model)
			register_entity_for_cutscene(selector_ped.pedID[selector_ped_michael], "michael", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			register_entity_for_cutscene(selector_ped.pedID[selector_ped_franklin], "franklin", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			register_entity_for_cutscene(chop.ped, "chop", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, chop.model)
			
			setup_vehicle_proofs(pass_cutscene_vehicle.veh)
			
			if does_entity_exist(pass_cutscene_vehicle.veh)
				if is_vehicle_driveable(pass_cutscene_vehicle.veh)
					register_entity_for_cutscene(pass_cutscene_vehicle.veh, "Rear_Car", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, pass_cutscene_vehicle.model)
				else 
					set_vehicle_as_no_longer_needed(pass_cutscene_vehicle.veh)
				endif 
			endif 
			
			BLOCK_MISSION_TITLE(TRUE)

			start_cutscene()
			
			set_ped_comp_item_current_sp(player_ped_id(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, false)
			
			REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			pass_mocap_status++
		
		break 
		
		case 1
		
			if is_cutscene_playing()
			
				remove_ped_helmet(player_ped_id(), true)
			
				SET_SRL_POST_CUTSCENE_CAMERA(<<1742.7886, 3280.7942, 40.0904>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 77.8678>>))
			
				SET_VEHICLE_ENGINE_ON(helicopter.veh, FALSE, FALSE)

				clear_area(<<1744.9111, 3254.2053, 40.5>>, 1000, true)
				
				pass_mocap_status++

			endif 
		
		break 
		
		case 2
		
			if is_cutscene_active()
				if not WAS_CUTSCENE_SKIPPED()
				
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor))
					endif 

				else 
				
					SET_CUTSCENE_FADE_VALUES(false, false, true)
					
					pass_mocap_status++
					
				endif 
				
			else 
				
				REPLAY_STOP_EVENT()
					
				end_cutscene_no_fade(false, false)
					
				mission_passed()	
				
			endif 
		
		break 
		
		case 3
		
			if not is_cutscene_active()
			
				REPLAY_STOP_EVENT()
			
				end_cutscene()
					
				mission_passed()	
			
			endif 
		
		break 
		
	endswitch 
	
endproc 

proc skip_to_helicopter_cutscene()
			
	REQUEST_MODEL(michael_model)
	REQUEST_MODEL(franklin_model)
	REQUEST_MODEL(trevor_model)
	request_model(gang_enemy[0].model)
	request_model(chop.model)
	REQUEST_MODEL(animal[0].model)
	REQUEST_MODEL(helicopter.model)
	request_model(gang_car.model)
	request_model(franklins_car.model)
	request_model(rpg.model)
	REQUEST_VEHICLE_RECORDING(helicopter.recording_number, str_carrec)
	REQUEST_VEHICLE_RECORDING(CARREC_HELI_HOVER, str_carrec)
	REQUEST_VEHICLE_RECORDING(007, str_carrec)
	request_vehicle_recording(001, "lkexile2")
	request_vehicle_recording(ambient_car.recording_number, "lkexile2")
	REQUEST_WAYPOINT_RECORDING("exile2_0")
	REQUEST_WAYPOINT_RECORDING("exile2_1")
	REQUEST_WAYPOINT_RECORDING("exile2_2")
	request_anim_dict("missexile2")
	request_anim_dict(strAnimDict)
	REQUEST_ANIM_SET("move_ped_crouched")
	REQUEST_ANIM_SET("Wpn_AssaultRifle_WeaponHoldingCrouched")
	REQUEST_ANIM_SET("move_ped_crouched_strafing")
	REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
 	//REQUEST_CHOP_CAM_SFX()

	while not HAS_MODEL_LOADED(michael_model)
	or not HAS_MODEL_LOADED(franklin_model)
	or not HAS_MODEL_LOADED(trevor_model)
	or not has_model_loaded(chop.model)
	or not HAS_MODEL_LOADED(animal[0].model)
	or not has_model_loaded(rpg.model)
	or not HAS_MODEL_LOADED(helicopter.model)
	or not has_model_loaded(gang_car.model)
	or not has_model_loaded(franklins_car.model)
	or not HAS_VEHICLE_RECORDING_BEEN_LOADED(helicopter.recording_number, str_carrec)
	or not HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_HELI_HOVER, str_carrec)
	or not has_vehicle_recording_been_loaded(007, str_carrec)
	or not has_vehicle_recording_been_loaded(001, "lkexile2")
	or not has_vehicle_recording_been_loaded(ambient_car.recording_number, "lkexile2")
	or not get_is_waypoint_recording_loaded("exile2_0")
	or not get_is_waypoint_recording_loaded("exile2_1")
	or not get_is_waypoint_recording_loaded("exile2_2")
	or not has_anim_dict_loaded("missexile2")
	or not has_anim_dict_loaded(strAnimDict)
	or not has_anim_set_loaded("move_ped_crouched")
	or not has_anim_set_loaded("Wpn_AssaultRifle_WeaponHoldingCrouched")
	or not has_anim_set_loaded("move_ped_crouched_strafing")
	or not REQUEST_SCRIPT_AUDIO_BANK("EXILE_2_01")
	//or not REQUEST_CHOP_CAM_SFX()
	
		wait(0)
		
	endwhile 
	
	if get_current_player_ped_enum() != char_trevor
		set_current_selector_ped(selector_ped_trevor, false)
	endif 
	
	//forces the helocopter to render in third person. 
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI, cam_view_mode_third_person_medium)
	
	helicopter.veh = create_vehicle(helicopter.model, <<-1503.4, 4358.3, 155.7>>, 57.3614)
	SET_VEHICLE_AS_RESTRICTED(helicopter.veh, 0)
	set_vehicle_livery(helicopter.veh, 1)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(helicopter.veh, false)
	set_entity_only_damaged_by_player(helicopter.veh, true)
	set_vehicle_colours(helicopter.veh, 34, 34)
	SET_VEHICLE_EXTRA_COLOURS(helicopter.veh, 0, 0)	
	
	set_ped_into_vehicle(player_ped_id(), helicopter.veh)
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	
	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, helicopter.veh, vs_front_right)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
	setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
	set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[selector_ped_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")

	create_player_ped_on_foot(selector_ped.pedID[selector_ped_franklin], char_franklin, <<-1683.4967, 4454.5952, 1.5660>>, 239.1512, false)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_franklin])
	setup_relationship_contact(selector_ped.pedID[selector_ped_franklin], true)
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	setup_weapon_for_ped(selector_ped.pedID[selector_ped_franklin], weapontype_carbinerifle, 100, false, false)

	add_ped_for_dialogue(scripted_speech, 1, selector_ped.pedID[selector_ped_franklin], "franklin")
	SET_ENTITY_LOAD_COLLISION_FLAG(selector_ped.pedID[selector_ped_franklin], true)
	SET_SELECTOR_PED_HINT(selector_ped, selector_ped_franklin, true)

	chop.pos = <<-1679.8206, 4455.9878, 1.2653>>
	chop.heading = 244.00
	setup_chop(chop)
	setup_relationship_animal(chop.ped)
	SET_ENTITY_LOAD_COLLISION_FLAG(chop.ped, true)
	chop_ai_system_status = 200 
	
	gang_car.veh = CREATE_VEHICLE(gang_car.model, <<-1873.92, 4486.02, 24.625>>, 239.264404)
	set_vehicle_colours(gang_car.veh, 7, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(gang_car.model) 
	SET_VEHICLE_DAMAGE(gang_car.veh, <<0.0, 1.0, -0.336>>, 1000.0, 1000.0, TRUE)
	SET_VEHICLE_DOOR_BROKEN(gang_car.veh, SC_DOOR_BONNET, FALSE)
	
	franklins_car.veh = CREATE_VEHICLE(franklins_car.model, <<-1899.3949, 4431.2559, 42.8050>>, 228.1822)
	set_vehicle_has_strong_axles(franklins_car.veh, true)
	set_entity_only_damaged_by_player(franklins_car.veh, true)
	SET_VEHICLE_COLOURS(franklins_car.veh, 12, 12)
		
	e_section_stage = SECTION_STAGE_SETUP
	mission_flow = STAGE_SWAP_TO_MICHAEL_CUTSCENE
	
	load_scene(<<-1683.4967, 4454.5952, 1.5660>>)
	
endproc 

proc exile2_load_stage_selector_assets()
				
	initialise_mission_variables()
	
	switch launch_mission_stage_menu_status

		case 0
		
			load_replay_skip_0() //start of mission
		
		break
		
		case 1
		
			load_replay_skip_1()
			
		break 
		
		case 2
		
			skip_to_crash_cutscene() 
		
		break 
		
		case 3
		
			load_replay_skip_2()
			
		break 
		
		case 4
		
			skip_to_helicopter_cutscene()
			
		break 
		
		case 5
		
			load_replay_skip_3() 

		break 
		
		case 6
		
			load_replay_skip_4() 
		
		break 
		
		case 7
		
			load_replay_skip_5() 
		
		break 
		
		case 8
		
			load_replay_skip_6() 
		
		break 
		
	endswitch 
	
	#IF IS_DEBUG_BUILD
		p_skip_time = get_game_timer()
	#endif 

endproc 

proc exile_2_switching_into_franklin()

	printstring("switching_into_franklin_status = ")
	printint(switching_into_franklin_status)
	printnl()
	
	if switching_into_franklin_status >= 2 
		set_player_control(player_id(), false)
	endif 

	switch switching_into_franklin_status 
	
		case 0
		
			request_replay_skip_1_assets()
			request_anim_dict("missswitch")
			request_anim_dict(strAnimDictCamShake)
			
			switching_into_franklin_status++
		
		break 
		
		case 1
		
			while not has_model_loaded(franklin_model)
			or not has_model_loaded(franklins_car.model)
			or not has_model_loaded(gang_car.model)
			or not has_model_loaded(gang_enemy[0].model)
			or not has_model_loaded(chop.model)
			or not has_model_loaded(radi)
			or not has_model_loaded(sabregt)
			or not has_ptfx_asset_loaded()
			or not has_vehicle_recording_been_loaded(003, "lkexcile2")
			or not has_vehicle_recording_been_loaded(004, "lkexcile2")
			or not has_vehicle_recording_been_loaded(005, "lkexile2")
			or not has_vehicle_recording_been_loaded(001, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(002, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(003, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(004, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(005, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(006, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(007, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(008, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(009, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(010, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(011, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(012, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(013, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(014, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(015, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(016, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(017, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(018, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(019, "lkexcile2_chase")
			or not has_vehicle_recording_been_loaded(100, "lkexcile2_chase")
			or not has_anim_dict_loaded("missexile2")
			or not has_anim_dict_loaded("missexile2switch")
			or not has_anim_dict_loaded("missswitch")
			or not has_anim_dict_loaded(strAnimDictCamShake)
			
				printstring("loading switch into franklin assets")
				printnl()
				
				wait(0)
				
			endwhile 
			
			//create_uber_chase_assets()

			switching_into_franklin_status++
			
		break 
		
		case 2
		
			//if is_franklin_camera_switch_ready(3)
			
			if IS_PLAYER_SWITCH_IN_PROGRESS()

				SWITCH_STATE camera_switch_state
				camera_switch_state = GET_PLAYER_SWITCH_STATE()    
						
				IF (camera_switch_state >= SWITCH_STATE_JUMPCUT_ASCENT) //before the pan. //SWITCH_STATE_WAITFORINPUT_OUTRO
									
					IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() >= 0
			
						clear_player_wanted_level(player_id())
						SET_WANTED_LEVEL_MULTIPLIER(0.0)
						set_max_wanted_level(0)
						set_create_random_cops(false)
						
						clear_area(<<-2664.2244, 2689.3, 15.7681>>, 10000.00, true)

//						if get_current_player_ped_enum() != char_franklin
//							set_current_selector_ped(selector_ped_franklin, false)
//						endif 
						
						franklins_car.veh = create_vehicle(franklins_car.model, <<-2646.31, 2677.32, 16.6427>>, 350.3382) 
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(franklins_car.veh, TRUE)
						set_vehicle_has_strong_axles(franklins_car.veh, true)
						set_entity_only_damaged_by_player(franklins_car.veh, true)
						set_vehicle_tyres_can_burst(franklins_car.veh, false)
						SET_VEHICLE_ENGINE_ON(franklins_car.veh, TRUE, TRUE)
						SET_VEHICLE_COLOURS(franklins_car.veh, 0, 0)
						SET_VEH_RADIO_STATION(franklins_car.veh, "RADIO_03_HIPHOP_NEW")
						start_playback_recorded_vehicle(franklins_car.veh, 100, "lkexcile2_chase")
						skip_time_in_playback_recorded_vehicle(franklins_car.veh, 7500)//10500)//9500)
						force_playback_recorded_vehicle_update(franklins_car.veh)

//						freeze_entity_position(player_ped_id(), false)
//						set_ped_into_vehicle(player_ped_id(), franklins_car.veh)
//						SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
//						add_ped_for_dialogue(scripted_speech, 1, player_ped_id(), "franklin")

						create_uber_chase_assets()
						
						set_entity_visible(franklins_car.veh, false)
						set_entity_visible(gang_car.veh, false)
						
						ALLOW_PLAYER_SWITCH_PAN()
						
						switching_into_franklin_status++
						
					endif 
					
				endif 
				
			endif 
			
		break 
		
		case 3
		
			//waiting on take control to happen on alwyns switch 
			if get_current_player_ped_enum() = char_franklin

				if is_vehicle_driveable(franklins_car.veh)
					
					freeze_entity_position(player_ped_id(), false)
					set_ped_into_vehicle(player_ped_id(), franklins_car.veh)
					set_entity_visible(player_ped_id(), false)
					SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
					add_ped_for_dialogue(scripted_speech, 1, player_ped_id(), "franklin")
					
				endif 
				
				switching_into_franklin_status++
				
			endif 
			
			uber_chase_system()
		
		break 
		
		case 4
		
			printvector(get_position_of_vehicle_recording_at_time(100, 7500, "lkexcile2_chase"))
			printnl()
		
			//update_uber_playback(gang_car.veh, 0.0)//called to ensure the multipliers are set every frame 
			uber_chase_system()

			if is_franklin_camera_switch_ready(1) //1
			
				selector_cam.bSplineActive = true //required to fix issue where switch camera though the ped who started the switch was dead. The frame before the switch was about to run the spline custom camera showing franklin in the car with chop. Makes the switch system think that a switch is already ongoing 
			
				eSwitchCamState = SWITCH_CAM_PLAYING_CODE_SWITCH
				
				AR_ALLOW_PLAYER_SWITCH_OUTRO()

				switching_into_franklin_status++
				
			endif 

		break 
		
		case 5
		
			//update_uber_playback(gang_car.veh, 0.0)
			uber_chase_system()
			
			IF not HANDLE_SWITCH_CAM_TREVOR_MICHAEL_TO_BROS_AND_FRANKLIN(scsSwitchCamTrevorMichael, scsSwitchCamBrosToFranklin)
			
			else 
			
				BLOCK_MISSION_TITLE(false) 

				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(), EXL2_DAMAGE)

				SET_FRONTEND_RADIO_ACTIVE(TRUE)

				switch get_current_player_ped_enum()

					case char_franklin
					
						set_ped_config_flag(player_ped_id(), PCF_ForceDirectEntry, true)
					
						START_AUDIO_SCENE("EXILE_2_FOLLOW_CAR")
						
						if is_playback_going_on_for_vehicle(franklins_car.veh)
							stop_playback_recorded_vehicle(franklins_car.veh)
						endif

						set_entity_proofs(franklins_car.veh, false, false, false, false, false)

						uber_chase_time = get_game_timer() //force playback speed to 1.1 for 2 seconds in 

						switch_system_status = 0
						
						allow_switch = false
						
						get_to_mountains_master_flow_system_status = 1
						e_section_stage = SECTION_STAGE_RUNNING
						mission_flow = stage_get_to_mountains
						
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "start of car chase") 

					break 
					
				endswitch 

			endif 

			//---------------
			
		break 
		
		case 6
		
		break 
		
	endswitch 

endproc 



SCRIPT
	
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		HANDLE_FAIL_AS_FRANKLIN()
		Mission_Flow_Mission_Force_Cleanup()
		MISSION_CLEANUP()
	ENDIF

	MISSION_SETUP()

	WHILE (TRUE)
		
		WAIT(0)

		if not stop_mission_fail_checks
			if mission_fail_checks()
				setup_mission_fail()
			endif 
		endif
		
		skip_system()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_PREDATOR")
		
		SWITCH mission_flow 

			CASE STAGE_OPENING_MOCAP
				OPENING_MOCAP()
			BREAK
			
			case switching_into_franklin

				exile_2_switching_into_franklin()

			break 
			
			case stage_get_to_mountains

				get_to_mountains()
			
			break 
			
//			case stage_get_to_mountains_starting_mission_as_franklin
//			
//				get_to_mountains_starting_mission_as_franklin()
//			
//			break 
			
			CASE STAGE_CRASH_CUTSCENE
				CRASH_CUTSCENE()
			BREAK
			
			CASE STAGE_INVESTIGATE_CRASH
				INVESTIGATE_CRASH()
			BREAK
			
			case stage_get_helicopter
			
				get_helicopter()
			
			break 
			
			CASE STAGE_SWAP_TO_MICHAEL_CUTSCENE
				SWAP_TO_MICHAEL_CUTSCENE()
			BREAK
			
			CASE STAGE_SNIPE_ENEMIES
				SNIPE_ENEMIES()
			BREAK
			
			case stage_mission_pass_conversation
			
				mission_pass_conversation()
			
			break 
			
			case stage_switch_to_trevor
			
				switch_to_trevor()
			
			break 
			
			case stage_pickup_franklin_and_chop
			
				pickup_franklin_and_chop()
				
			break 
			
			case stage_get_to_airstrip
			
				get_to_airstrip()
			
			break 
			
			case stage_pass_mocap
			
				pass_mocap()
			
			break 
			
			CASE STAGE_RECORD_TRAFFIC
				#IF IS_DEBUG_BUILD
					RECORD_CHASE_TRIGGER_AND_TRAFFIC()
				#ENDIF
			BREAK
			
			CASE STAGE_TEST
				#IF IS_DEBUG_BUILD
				//	TEST_MISSION_START()
				//	TEST_RPG_CUT()
					//TEST_MISSION_PASSED_SCREEN()
				#ENDIF	
			BREAK
			
			case mission_fail_stage
			
				mission_fail_stage_system()

			break 
			
			case load_stage_selector_assets
			
				exile2_load_stage_selector_assets()

			break 
	
		ENDSWITCH
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF NOT bVehGrabbedForStats
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
						INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), EXL2_MAX_SPEED)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), EXL2_CAR_DAMAGE)
						PRINTLN("\n\n\n Grabbed vehicle for stats \n\n\n")
						bVehGrabbedForStats = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, EXL2_MAX_SPEED)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, EXL2_CAR_DAMAGE)
					PRINTLN("\n\n\n DROPPED vehicle for stats \n\n\n")
					bVehGrabbedForStats = FALSE
				ENDIF	
			ENDIF
		ENDIF

		#IF IS_DEBUG_BUILD

			//is_entity_in_angled_area(player_ped_id(), <<1988.564, 3821.785, 30.034>>, <<1996.500, 3826.238, 34.134>>, 17.000)
		
//			is_entity_in_angled_area(player_ped_id(), <<-1946.126, 4473.044, 25.943>>, <<-1979.192, 4435.007, 46.443>>, 106.100) 
//			is_entity_in_angled_area(player_ped_id(), <<-1914.003, 4463.934, 25.943>>, <<-1957.432, 4414.501, 46.443>>, 51.900)
//			is_entity_in_angled_area(player_ped_id(), <<-1959.981, 4434.166, 25.943>>, <<-1968.885, 4423.886, 46.443>>, 19.200) 
		
		
			get_distance_between_coords(get_entity_coords(player_ped_id()), <<-1627.1, 4413.2, 3.66>>)
		
			//attach_ped_to_object_and_move(chop.ped, franklins_car.veh)
		
		//	BREAK_ON_NATIVE_COMMAND("FREEZE_ENTITY_POSITION",FALSE)
		//	PRINT_DW_INT_TO_GAME_SCREEN((GET_CONTROL_VALUE(PLAYER_CONTROL, INPUT_AIM)), "Val ")
			//camera_attached_to_entity_widget(helicopter.veh)
			angled_area_locate_widget(player_ped_id())
			//SHOW_PED_CAR_REC_TIME_DEBUG(gang_enemy[0].ped)
			
//			relative_pitch_and_heading_widget()

			//draw_offset_marker_on_vehicle(gang_car.veh)
			
			
			
			DO_DEBUG()
			
			reset_big_foot_global()

			//angled_area_locate_widget(gang_car.veh)
			
//			if does_entity_exist(gang_car.veh)
//				freeze_entity_position(gang_car.veh, false)
//			endif 
			
			//is_entity_in_angled_area(player_ped_id(), <<-1682.745, 4537.192, -1.0>>, <<-1529.905, 4364.519, 42.469>>, 219.000) 
			
//			if does_entity_exist(helicopter.veh)
//				printstring("get_entity_height_above_ground: ")
//				printfloat(get_entity_height_above_ground(helicopter.veh))
//				printnl()
//				
//				printstring("get_entity_speed: ")
//				printfloat(get_entity_speed(helicopter.veh))
//				printnl()
//			endif 
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_PASSED()
			endif 
			
			if not stop_mission_fail_checks
				if IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
					setup_mission_fail()
				ENDIF
			endif 
			
			if IS_KEYBOARD_KEY_JUST_PRESSED(KEY_k)
				printstring("test key k")
				printnl()
//				if not is_ped_injured(gang_enemy[3].ped)
//					set_entity_health(gang_enemy[3].ped, 2)
//				endif 
				if not is_ped_injured(chop.ped)
					set_entity_health(chop.ped, 2)
				endif 
			ENDIF
			
			if IS_KEYBOARD_KEY_JUST_PRESSED(KEY_t)
				PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 
			endif 
			
			if IS_KEYBOARD_KEY_JUST_PRESSED(KEY_o)
				PLAY_SOUND_FRONTEND(-1, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET") 
			endif 
			
		#ENDIF	
	ENDWHILE
ENDSCRIPT

//Rag > Rendering > Seethrough Effect

//MISSION_FAILED(FAILED_OUT_OF_TIME)


//float rec_time = (1 * (get_total_duration_of_vehicle_recording(003, "lkexcile2") / 10))
//
//	vector vec_BA = get_position_of_vehicle_recording_at_time(003, rec_time, "lkexcile2") - get_entity_coords(player_ped_id()) 
//	vector node_forward_vector = <<1945.58, 3782.25, 32.257>> - get_position_of_vehicle_recording_at_time(003, rec_time, "lkexcile2")
//
//	if dot_product(vec_BA, node_forward_vector) < 0.0
//		
//		printstring("infront**************")
//		printnl()
//		printstring("infront**************")
//		printnl()
//		printstring("infront**************")
//		printnl()
//			
//	else 
//		
//		printstring("behind**************")
//		printnl()
//		printstring("behind**************")
//		printnl()
//		printstring("behind**************")
//		printnl()
//
//	endif 
//	
//	int i
//	
//	for i = 0 to 9
//		draw_sphere(get_position_of_vehicle_recording_at_time(003, (i * (get_total_duration_of_vehicle_recording(003, "lkexcile2") / 10)), "lkexcile2"), 4.0)
//	endfor 
	
	
//	printvector(get_position_of_vehicle_recording_at_time(003, recording_time, "lkexcile2"))
//	printnl()

//			franklin_ai_system()
//			
//			proc franklin_ai_system()
//			
//				if get_current_player_ped_enum() = char_michael
//				
//					if not is_ped_in_combat(
//					
//					switch franklin_ai_status 
//					
//						case 0
//						
//							open_sequence_task(seq)
//								task_follow_nav_mesh_to_coord(null, <<-1644.8438, 4487.6313, 0.9437>>, pedmove_run, -1) 
//							close_sequence_task(seq)
//							task_perform_sequence(selector_ped[selector_ped_franklin], seq)
//							clear_sequence_task(seq)
//							
//							franklin_ai_status++
//						
//						break 
//						
//						case 1
//				
//							if not is_entity_at_coord(selector_ped[selector_ped_franklin], <<-1644.8438, 4487.6313, 0.9437>>)  
//								if has_ped_task_finished_2(selector_ped[selector_ped_franklin])
//									
//									open_sequence_task(seq)
//										task_follow_nav_mesh_to_coord(null, <<-1644.8438, 4487.6313, 0.9437>>, pedmove_run, -1) 
//									close_sequence_task(seq)
//									task_perform_sequence(selector_ped[selector_ped_franklin], seq)
//									clear_sequence_task(seq)
//									
//								endif 
//							endif 
//							
//						break 
//						
//						case 2
//						
//						break 
//						
//					endswitch 
//					
//				endif 
//			
//			endproc



///scenario: player drives to franklins car in helicopter
///    if helicopter gets within a certain distance of franklins car or vehicle visible
///    start franklins uber vehicle recording. 
///    disable vehicle warping section within switch system for both franklin and trevors vehicles.
///    when switching from helicopter to franklin have helicopter fly to target position in mountains via ai and
///    all uber to continue without ever stopping.
///    start cutscene if who ever gets to the target first franklin or trevor. might need to fade out and 
///    load scene if chopper is to far away.
///   
///    
///    
///    Renderer/Debug/Render Phases/SeeThrough/SeeThrough Effect/Enable in Rag



//THERMAL BUG
//392271
//392283

//chop anim bug - 396054
//547242

//GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR and SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR 


//HANDLE_P_SKIP() handles all the skipping in the mission.

//rpg bug 482934

//"move_characters@michael@fire"


//rpg rocket bug 482934

//ptfx bug = 806468


//ptfx bug 385782

//Alwyn requesting assets fro exile 2
//Request_SpecialMissionAssets in:
//player_scene_Assets.sch
//the switch statement case for PR_SCENE_F_S_EXILE2

//881292 - drop rpg weapon bug.

//984619 - enemy vehicle recording issue.

//chop dormant tasks and ragdoll bug = 1021072

//963905 = chop anim bugs

//dropping down suspension issue when stopping a vehicle recording = 1002300

//michael standing up = 1022182  986941


//chop audio no barking when finding rpg enemy - 1023848

//chop sounds = 967874


//rpg weapon bugs 
//799358    800300

//chop get into helicopter anims - 963905

//chop task_follow_nav_mesh helicopter bounds issue = 1032018

//audio does not play on kill rpg guy - 1033355

//thermal vision bug = 1038365

//thermal vission blurry = 967383

//rpg reloads twice. - 1011287

//chop take down anims - 952195

//broken sniper hud -  1077927

//dear not fleeing - 1059957

//1059645 - Chop had trouble entering the helicopter

//993911 - audio stream

//1072411 - vehicle damage

//chop move blend bug when chasing rpg enemy = 1090500


//1069027 - plane pilot bug

//1112433 - thermal vision not working
//1112447 - can't fire sniper rifle.

//1133843 - players falls to ground after repaly / z skip

//1176573 - p load music stream issue

//SET_GAMEPLAY_ENTITY_HINT(selector_ped.pedID[selector_ped_franklin], <<-0.1, 1.0, 1.1>>, TRUE, 2000, 6000, 2000, HINTTYPE_ARM3_VEHICLE)

//1218594 - switch camera point at coords functionality

//1293536 - boat disapearing in cutscene and scene handle issue. 

//1304339 - 1290573 helicopter exploding.

//SET_PLAYER_CAN_USE_VEHICLE_SEARCH_LIGHT

//1305943 - player is to high on the seat when aiming.

//1307027 - chop running and stopping

//1293357 - chop anims with audio

//1263132 - replay aiming issue.

//1110701 - chop and franklin let off anims.

//1471557 - elk anim bug

//1506629 - conversation gets paused by code.


//It's a sync scene.. heres the anim dict
//STRING strAnimDict = "missexile2switch"
//
//Franklins anim: strAnimDict
//Chops anim: "ex02_heli_switch_chop_run_chop"


//Dict "missfra0_chop_find"
//
//Chop anim "FRA_0_IG_CHOP_TAKE_DOWN_BALLA_CHOP"
//Balla anim “FRA_0_IG_CHOP_TAKE_DOWN_BALLA_VICTIM"
//
//Triggered when Chop is 1.6m away behind the victim.


//after 40s go into a new ai case for this make sure chop
//
//
//if chop is behind the ped
//	if chop and enemy within range 1.6m 
//
//		dist = target range - distance between peds 
//
//		dist = 1.6 - 1.7
//
//		if absl(dist) < 1.0
//
//			play anim anims
//		endif 
//
//	elif distance > 1.6
//
//		speed chop up
//
//	else 
//
//		slow chop down
//
//	endif 
//
//	
//
//endif 

//chop anim 


//1562050 - sniper hud not rendering

//1568389 - set_focus_pos_and_vel 


//anchor_head, PROPS_P0_PILOT_HEADSET
//SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), comp_type_head, PROPS_P0_PILOT_HEADSET, false)



