//|=======================================================================================|
//|                 Author:  Craig Vincent / Lukasz Bogaj 	 Date: 08/06/2013             |
//|=======================================================================================|
//|                                  	  FAMILY6.sc	                                  |
//|                            		  	REUNITE FAMILY		                        	  |
//|                             									                      |
//|=======================================================================================|

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//===================================== INCLUDE FILES ====================================|

USING "rage_builtins.sch"
USING "globals.sch"

//Commands headers
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_graphics.sch"
USING "commands_itemsets.sch"
USING "commands_vehicle.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_object.sch"
USING "commands_camera.sch"
USING "commands_audio.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_task.sch"
USING "commands_fire.sch"
USING "commands_hud.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_recording.sch"

//Script headers
USING "script_blips.sch"
USING "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "taxi_functions.sch"
USING "taxi_functions.sch"
USING "timeLapse.sch"
USING "RC_helper_functions.sch" 

//Public headers
USING "CompletionPercentage_public.sch"
USING "mission_stat_public.sch"
USING "player_ped_scenes.sch"
USING "cutscene_public.sch"
USING "carsteal_public.sch"
USING "locates_public.sch"
USING "replay_public.sch"
USING "family_public.sch"
USING "shop_public.sch"
USING "yoga.sch"

//Debug headers
#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//===================================== ENUMS & STRUCTS ==================================|

/// PURPOSE: Specifies stages of the mission.
ENUM MISSION_STAGES
	MISSION_STAGE_PRE_INTRO					= 0,		//Pre into scripted cutscene showing player enter the house
	MISSION_STAGE_CUTSCENE_INTRO_P1			= 1,		//Intro cutscene part 1 - fam_6_int
	MISSION_STAGE_TIMELAPSE					= 2,		//Timelapse
	MISSION_STAGE_CUTSCENE_INTRO_P2			= 3,		//Intro cutscene part 2 - fam_6_int_p3_t7
	MISSION_STAGE_DRIVE_TO_BEAN_MACHINE		= 4,		//Go to Bean Machine with Jimmy
	MISSION_STAGE_CUTSCENE_BEAN_MACHINE		= 5,		//Bean Machine cutscene - 
	MISSION_STAGE_DRIVE_TO_TRACEY			= 6,		//Go to tatto parlor with Jimmy
	MISSION_STAGE_CUTSCENE_SHOP_1			= 7,		//
	MISSION_STAGE_PIERCE_LAZLOW				= 8,
	MISSION_STAGE_CUTSCENE_SHOP_2			= 9,
	MISSION_STAGE_TATTOO_LAZLOW				= 10,
	MISSION_STAGE_CUTSCENE_SHOP_3			= 11,
	MISSION_STAGE_CUT_LAZLOW_HAIR			= 12,
	MISSION_STAGE_CUTSCENE_SHOP_4			= 13,
	MISSION_STAGE_DRIVE_TO_SHRINK			= 14,
	MISSION_STAGE_CUTSCENE_SHRINK			= 15,
	MISSION_STAGE_DRIVE_HOME				= 16,
	MISSION_STAGE_CUTSCENE_END				= 17,
		
	MISSION_STAGE_PASSED,								//Mission passed
	MISSION_STAGE_FAILED								//Mission failed
ENDENUM

/// PURPOSE: Specifies mission fail reasons.
ENUM MISSION_FAILS
	MISSION_FAIL_EMPTY = 0,								//Generic fail
	MISSION_FAIL_AMANDA_DEAD,							//Amanda dead
	MISSION_FAIL_FABIEN_DEAD,							//Fabien dead
	MISSION_FAIL_JIMMY_DEAD,							//Jimmy dead
	MISSION_FAIL_TRACEY_DEAD,							//Tracey dead
	MISSION_FAIL_LAZLOW_DEAD,							//Lazlow dead
	MISSION_FAIL_AMANDAS_CAR_DEAD,						//Amanda's car destroyed
	MISSION_FAIL_AMANDA_SCARED_OFF,						//Amanda scared off by player behaviour
	MISSION_FAIL_AMANDA_LEFT_BEHIND,					//Amanda left behind
	MISSION_FAIL_JIMMY_LEFT_BEHIND,						//Jimmy left behind
	MISSION_FAIL_TRACEY_LEFT_BEHIND,					//Tracey left behind
	MISSION_FAIL_FAMILY_LEFT_BEHIND,					//Family left behind	
	MISSION_FAIL_BEAN_MACHINE_SMASHED,					//Bean machine smashed by player vehicle
	MISSION_FAIL_FORCE_FAIL,							//Debug forced fail
	
	MAX_MISSION_FAILS
ENDENUM

/// PURPOSE: Specifies mid-mission checkpoints for mission retry.
ENUM MID_MISSION_STAGES
	MID_MISSION_STAGE_DRIVE_TO_BEAN_MACHINE		= 0,	//Checkpoint at MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
	MID_MISSION_STAGE_CUTSCENE_BEAN_MACHINE		= 1,	//Checkpoint at MISSION_STAGE_CUTSCENE_BEAN_MACHINE
	MID_MISSION_STAGE_DRIVE_TO_TRACEY			= 2,	//Checkpoint at MISSION_STAGE_DRIVE_TO_TRACEY
	MID_MISSION_STAGE_PIERCE_LAZLOW				= 3,	//Checkpoint at MISSION_STAGE_PIERCE_LAZLOW
	MID_MISSION_STAGE_DRIVE_TO_SHRINK			= 4,	//Checkpoint at MISSION_STAGE_DRIVE_TO_SHRINK
	MID_MISSION_STAGE_DRIVE_HOME				= 5		//Checkpoint at MISSION_STAGE_DRIVE_HOME
ENDENUM

ENUM PIERCING_STAGES
	PIERCING_IDLE								= 0,
	PIERCING_BROW								= 1,
	PIERCING_EAR								= 2,
	PIERCING_NOSE								= 3
ENDENUM

ENUM TATTOOING_STAGES
	TATTOOING_IDLE								= 0,
	TATTOOING_CHEST								= 1,
	TATTOOING_BACK								= 2
ENDENUM

ENUM TATTOO_BUTTONS
	STYLE_ONE_UP 								= 0,
	STYLE_ONE_DIAGONAL_UP_RIGHT,
	STYLE_ONE_RIGHT,
	STYLE_ONE_DIAGONAL_DOWN_RIGHT,
	STYLE_ONE_DOWN,
	STYLE_ONE_DIAGONAL_DOWN_LEFT,
	STYLE_ONE_LEFT,
	STYLE_ONE_DIAGONAL_UP_LEFT,		
			
	RIGHT_STICK_UP,
	RIGHT_STICK_DIAGONAL_UP_RIGHT,
	RIGHT_STICK_RIGHT,
	RIGHT_STICK_DIAGONAL_DOWN_RIGHT,
	RIGHT_STICK_DOWN,
	RIGHT_STICK_DIAGONAL_DOWN_LEFT,
	RIGHT_STICK_LEFT,
	RIGHT_STICK_DIAGONAL_UP_LEFT,
	SPACER
ENDENUM

/// PURPOSE: Specifies details of mission peds.
STRUCT PED_STRUCT
	PED_INDEX				PedIndex				//Ped Index
	BLIP_INDEX				BlipIndex				//Ped's Blip Index
	VECTOR					vPosition				//Ped's start location
	FLOAT					fHeading				//Ped's start heading
	MODEL_NAMES				ModelName				//Ped's model
	INT						iProgress				//Ped's progress
	INT						iPedConversationTimer	//Ped's conversation timer
	INT						iTimer					//Ped's timer
	BOOL					bComponentVariationsSet
ENDSTRUCT

/// PURPOSE: Specifies details of mission vehicles.
STRUCT VEH_STRUCT 
	VEHICLE_INDEX			VehicleIndex			//Vehicle index
	BLIP_INDEX				BlipIndex				//Vehicle blip
	VECTOR					vPosition				//Vehicle postion
	FLOAT					fHeading				//Vehicle heading
	MODEL_NAMES				ModelName				//Vehicle model
ENDSTRUCT

/// PURPOSE: Specifies details of mission specific objects.
STRUCT OBJECT_STRUCT
	OBJECT_INDEX			ObjectIndex				//Object index
	MODEL_NAMES				ModelName				//Object model
	VECTOR					vPosition				//Object postion
	VECTOR					vRotation				//Object rotation
	VECTOR					vOffset					//Object offset
ENDSTRUCT

/// PURPOSE: Stores player postion and heading.
STRUCT LOCATION_STRUCT
	VECTOR 					vPosition				//Player position
	FLOAT 					fHeading				//Player heading
ENDSTRUCT

//|======================================= CONSTANTS =====================================|

CONST_INT 					MAX_MODELS 						10
CONST_INT 					MAX_ANIMATIONS					8
CONST_INT					IDEAL_MISSION_START_TIME		7

FLOAT						TATTOO_BUTTONS_X				= 0.6
FLOAT						TATTOO_BUTTONS_Y				= 0.795
TWEAK_FLOAT					TATTOO_BUTTONS_WIDTH			0.390
TWEAK_FLOAT					TATTOO_BUTTONS_HEIGHT			0.10425	//0.139 * 0.75
TWEAK_FLOAT					TATTOO_BUTTONS_SCALE			1.2

VECTOR						vMichaelsHousePosition			= << -824.89, 180.72, 70.57 >>
VECTOR						vBeanMachinePosition			= << -632.30, 255.25, 80.46 >>
VECTOR						vTattooShopPosition				= << 299.31, 174.02, 102.98 >>
VECTOR						vTattooShopDoorPosition			= << 320.84, 177.82, 102.56 >>
VECTOR						vShrinkOfficePosition			= << -1899.75, -561.64, 10.78 >>
VECTOR						vShrinkInteriorPosition			= << -1903.60, -573.20, 19.10 >>

VECTOR						vBeanMachineScenePosition		= << -626.917, 244.277, 80.9 >>
VECTOR						vBeanMachineSceneRotation		= << 0.0, 0.0, -6.530 >>

VECTOR						vTattooShopScenePosition		= << 320.57, 181.39, 103.79 >>
VECTOR						vTattooShopSceneRotation		= << 0.0, 0.0, -111.08>>

VECTOR						vScissorsScenePosition			= << 322.200, 180.990, 102.600 >> 
VECTOR						vScissorsSceneRotation			= << 0.0, 0.0, 80.0 >>

VECTOR						vLazlowLeadoutPosition			= << 323.225, 179.725, 103.600 >>
VECTOR						vLazlowLeadoutRotation			= << 0.000, 0.000, 113.400 >>

VECTOR						vTatguyLeadoutPosition			= << 321.525, 182.075, 103.570 >>
VECTOR						vTatguyLeadoutRotation			= << 0.000, 0.000, -162.720 >>

//======================================== VARIABLES =====================================|

MISSION_STAGES				eMissionStage					= MISSION_STAGE_PRE_INTRO
MISSION_FAILS				eMissionFail					= MISSION_FAIL_EMPTY
PIERCING_STAGES				ePiercingStage					= PIERCING_IDLE
TATTOOING_STAGES			eTattooingStage					= TATTOOING_IDLE
TATTOO_BUTTONS				eTattooDirection

//mission peds
PED_STRUCT					psJimmy
PED_STRUCT					psAmanda
PED_STRUCT					psFabien
PED_STRUCT					psTracey
PED_STRUCT					psLazlow
PED_STRUCT					psTatguy
PED_STRUCT					psHipster

//mission vehicles
VEH_STRUCT 					vsAmandasCar
VEH_STRUCT 					vsMichaelsCar

//mission objects
OBJECT_STRUCT				osMug
OBJECT_STRUCT				osShirt
OBJECT_STRUCT				osLaptop
OBJECT_STRUCT				osScissors
OBJECT_STRUCT				osPonyTail
OBJECT_STRUCT				osPiercingGun
OBJECT_STRUCT				osTattooNeedle
OBJECT_STRUCT				osLeftDoorDummy
OBJECT_STRUCT				osRightDoorDummy

LOCATES_HEADER_DATA 		sLocatesData
LOCATION_STRUCT 			MissionPosition

structTimelapse				sTimelapse
structPedsForConversation 	FAM6Conversation

//progress counters
INT 						iStageSetupProgress
INT 						iMissionStageProgress
INT							iAmandaProgress
INT							iLazlowLeadoutProgress
INT							iBeanMachineSceneProgress
INT							iTattooParlorSceneProgress

BOOL 						bSkipFlag
BOOL 						bReplayFlag
BOOL 						bLoadingFlag
BOOL						bStageReplayInProgress

//asset request counters
INT 						iModelsRequested
INT 						iAnimationsRequested

BOOL						bEar
BOOL						bNose
BOOL						bBrow
BOOL						bChestTattoo

BOOL						bCutsceneSkipped
BOOL						bTimelapseTriggered
BOOL						bTimelapseCutsceneSkipped
BOOL						bCutsceneAssetsRequested
BOOL						bHouseEntryCutsceneSkipped
BOOL						bDestinationReached
BOOL						bLeadInTriggered
BOOL						bVehicleReversed
BOOL						bSetSRLForcePrestream
BOOL						bLookAtAmandaActive

BOOL						bPCControlsSetup = FALSE

INT							iTODAdditionalHours

INT							iSpeedZone

INT							iHouseEnterSceneID			= -1
INT							iTattooShopSceneID			= -1
INT							iPierceSceneID				= -1
INT							iScissorsSceneID			= -1

INT							iScaleformProgressPad
INT							iScaleformProgressKBM
FLOAT						fTattooSpeed
FLOAT						fTattooPosition
BOOL						bTattooInputReady
BOOL						bScaleformDrawing

BOOL						bCustomRouteActive
BOOL						bStopUsingDefaultRoute
BOOL						bPlayerPedScriptTaskInterrupted

INT							iTattooSoundID
INT							iTattooCameraProgress
INT							iIdle1ConversationCounter 
INT							iIdle2ConversationCounter
INT							iIdle3ConversationCounter
INT							iIdleConversationcounter

INT							iAwayConversationTimer
INT							iAwayConversationCounter


INT							iStruggleConversationTimer

BOOL						bPlayerHitPed
BOOL						bPlayerActingBad
BOOL						bFamilyArrivedAtHouse
INT							iLoseCopsReminderCount
INT							iLoseCopsReminderTimer
INT							iPlayerOffenceTimer
INT							iPlayerOffencesCount
INT							iBadConversationCount
INT							iHitPedConversationCount
INT							iCopsLostConversationCount
BOOL						bRunConversationPausingCheck
BOOL						bCurrentConversationInterrupted
INT							iConversationTimer, iConversationCounter
TEXT_LABEL_23				CurrentConversationRoot, CurrentConversationLabel

PTFX_ID						ptfxHair
CAMERA_INDEX				ScriptedCamera, AnimatedCamera
SCENARIO_BLOCKING_INDEX		BeanMachineScenarioBlockingIndex
SCENARIO_BLOCKING_INDEX		TattooParlorScenarioBlockingIndex
SCALEFORM_INDEX 			mov
SCALEFORM_INDEX 			mov_kb

TEXT_LABEL					LabelFAM6SHRINK = "FAM6_SHRINK"

BOOL						bReplayStopEventTriggered, bReplayStartEventTriggered

//|========================================= ARRAYS ======================================|

//text printing
INT 						iTriggeredTextHashes[30]
INT 						iNumTextHashesStored

BOOL						FailFlags[MAX_MISSION_FAILS]

//asset arrays
MODEL_NAMES 				MissionModels[MAX_MODELS]
STRING						MissionAnimations[MAX_ANIMATIONS]

//|========================= MISCELLANEOUS PROCEDURES & FUNCTIONS ========================|

// Initialise PC controls for peircing and tatooing minigames
PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF NOT bPCControlsSetup
			INIT_PC_SCRIPTED_CONTROLS( "FAMILY6")
			bPCControlsSetup = TRUE
		ENDIF
	ENDIF

ENDPROC


PROC CLEANUP_PC_CONTROLS()
	IF IS_PC_VERSION()
		IF bPCControlsSetup
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCControlsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL HAS_SCALEFORM_LOADED()

	IF IS_PC_VERSION()
	
		IF HAS_SCALEFORM_MOVIE_LOADED(mov) 
		AND HAS_SCALEFORM_MOVIE_LOADED(mov_kb)
			RETURN TRUE
		ENDIF
	
	ELSE
	
		IF HAS_SCALEFORM_MOVIE_LOADED(mov)
			RETURN TRUE
		ENDIF
	
	ENDIF

	RETURN FALSE

ENDFUNC

PROC RESET_MISSION_FLAGS()

	//reset setup progress to 1, so that iSetupProgress = 0 only runs once, on initial mission load.
	iStageSetupProgress				= 1
	
	iMissionStageProgress			= 0
	iAmandaProgress					= 0
	iLazlowLeadoutProgress			= 0
	iBeanMachineSceneProgress		= 0
	iTattooParlorSceneProgress		= 0
	
	bStageReplayInProgress			= FALSE
	
	bDestinationReached				= FALSE
	bLeadInTriggered				= FALSE
	bCutsceneSkipped				= FALSE
	bTimelapseCutsceneSkipped		= FALSE
	bCutsceneAssetsRequested		= FALSE
	
	bScaleformDrawing				= FALSE
	
	bLookAtAmandaActive				= FALSE
	
	bCustomRouteActive				= FALSE
	bStopUsingDefaultRoute			= FALSE
	bPlayerPedScriptTaskInterrupted	= FALSE
	
	bPlayerHitPed					= FALSE
	bPlayerActingBad				= FALSE
	bFamilyArrivedAtHouse			= FALSE
	iLoseCopsReminderCount			= 0
	iLoseCopsReminderTimer			= 0
	iPlayerOffenceTimer 			= 0
	iPlayerOffencesCount			= 0
	iBadConversationCount			= 0
	iHitPedConversationCount		= 0
	iCopsLostConversationCount		= 0
	iConversationTimer 				= 0
	iConversationCounter			= 0	
	CurrentConversationRoot			= ""
	CurrentConversationLabel		= ""
	bRunConversationPausingCheck	= FALSE
	bCurrentConversationInterrupted = FALSE
	
	iIdleConversationcounter 		= 0
	iIdle1Conversationcounter 		= 0
	iIdle2Conversationcounter 		= 0
	iIdle3ConversationCounter		= 0
	iStruggleConversationTimer		= 0
	
	iAwayConversationTimer			= 0
	iAwayConversationCounter		= 0
	
	LabelFAM6SHRINK					= "FAM6_SHRINK"
	
	bSetSRLForcePrestream			= FALSE
	
	bReplayStopEventTriggered		= FALSE
	bReplayStartEventTriggered		= FALSE

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission flags and progress counters are reset.")
	#ENDIF

ENDPROC

PROC REMOVE_LABEL_ARRAY_SPACES()
	INT iArrayLength = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT (iArrayLength - 1) i
		IF iTriggeredTextHashes[i] = 0
			IF iTriggeredTextHashes[i+1] != 0
				iTriggeredTextHashes[i] = iTriggeredTextHashes[i+1]
				iTriggeredTextHashes[i+1] = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_LABEL_INDEX(INT iLabelHash)
	INT i = 0
	
	REPEAT iNumTextHashesStored i
		IF iTriggeredTextHashes[i] = iLabelHash
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF GET_LABEL_INDEX(iHash) != -1
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bTrigger)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF bTrigger
		IF NOT HAS_LABEL_BEEN_TRIGGERED(strLabel)
			INT iArraySize = COUNT_OF(iTriggeredTextHashes)
		
			IF iNumTextHashesStored < iArraySize
				iTriggeredTextHashes[iNumTextHashesStored] = iHash
				iNumTextHashesStored++
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", strLabel, " TRUE.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", iNumTextHashesStored, "/", COUNT_OF(iTriggeredTextHashes), ".")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Label array is full.")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		INT iIndex = GET_LABEL_INDEX(iHash)
		IF iIndex != -1
			iTriggeredTextHashes[iIndex] = 0
			REMOVE_LABEL_ARRAY_SPACES()
			iNumTextHashesStored--
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", strLabel, " FALSE.")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", iNumTextHashesStored, "/", COUNT_OF(iTriggeredTextHashes), ".")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iArraySize i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
	
	iNumTextHashesStored = 0
ENDPROC

PROC UPDATE_TRIGGERED_LABEL(TEXT_LABEL &sLabel)

	IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)					//if the label is not empty, which means not yet printed by locates header
	
		IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)				//if it was not set as triggered by script

			IF IS_THIS_PRINT_BEING_DISPLAYED(sLabel)		//if the label was displayed on screen

				SET_LABEL_AS_TRIGGERED(sLabel, TRUE)		//mark it as triggered

			ENDIF
			
		ELSE												//if the label was set as triggered by script,
															//which means it was printed by locates header
			IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sLabel)	//but is not currently being printed, cleared off screen or timed out
			
				sLabel = ""									//mark is as empty
			
			ENDIF
		
		ENDIF
		
		
	ENDIF
	
ENDPROC

FUNC SCENARIO_BLOCKING_INDEX CREATE_SCENARIO_BLOCKING_AREA(VECTOR vPosition, VECTOR vSize)
	RETURN ADD_SCENARIO_BLOCKING_AREA(<< vPosition.x - vSize.x, vPosition.y - vSize.y, vPosition.z - vSize.z >>,
									  << vPosition.x + vSize.x, vPosition.y + vSize.y, vPosition.z + vSize.z >>)
ENDFUNC

/// PURPOSE:
///    Sets all fail flags to specified boolean value.
/// PARAMS:
///    bNewBool - Boolean value to set all flags to TRUE or FALSE.
PROC SET_FAIL_FLAGS(BOOL bNewBool)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(FailFlags) - 1 )

		FailFlags[i] = bNewBool

	ENDFOR

ENDPROC

PROC SET_JIMMY_PED_COMPONENT_VARIATIONS(BOOL &bComponentVariationsSet)
	IF ( bComponentVariationsSet = FALSE )
		IF DOES_ENTITY_EXIST(psJimmy.PedIndex)
			IF NOT IS_ENTITY_DEAD(psJimmy.PedIndex)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_HEAD, 		0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 		0, 0) //(berd)  //berd 1 for cutscenes
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_HAIR, 		0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_TORSO, 		4, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_LEG, 		0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_HAND, 		0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_FEET, 		0, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_TEETH, 		0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_SPECIAL, 	3, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_SPECIAL2, 	0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_DECL, 		2, 0) //(decl)
				bComponentVariationsSet = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_HEAD, 		0, 0) //(head)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_BERD, 		1, 0) //(berd)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_HAIR, 		0, 0) //(hair)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_TORSO, 		4, 0) //(uppr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_LEG, 		0, 0) //(lowr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_HAND, 		0, 0) //(hand)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_FEET, 		0, 0) //(feet)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_TEETH, 		0, 0) //(teef)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_SPECIAL, 	3, 0) //(accs)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_SPECIAL2, 	0, 0) //(task)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_DECL, 		2, 0) //(decl)
ENDPROC

PROC SET_TRACEY_PED_COMPONENT_VARIATIONS(BOOL &bComponentVariationsSet)
	IF ( bComponentVariationsSet = FALSE )
		IF DOES_ENTITY_EXIST(psTracey.PedIndex)
			IF NOT IS_ENTITY_DEAD(psTracey.PedIndex)
				SET_PED_PROP_INDEX(psTracey.PedIndex, ANCHOR_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(psTracey.PedIndex, PED_COMP_HEAD, 	0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(psTracey.PedIndex, PED_COMP_HAIR, 	3, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(psTracey.PedIndex, PED_COMP_TORSO, 	4, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(psTracey.PedIndex, PED_COMP_LEG, 	4, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(psTracey.PedIndex, PED_COMP_HAND, 	0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(psTracey.PedIndex, PED_COMP_FEET, 	2, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(psTracey.PedIndex, PED_COMP_TEETH, 	0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(psTracey.PedIndex, PED_COMP_SPECIAL, 3, 0) //(accs)
				bComponentVariationsSet = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
	SET_CUTSCENE_PED_PROP_VARIATION("Tracy", ANCHOR_HEAD, 0, 0)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", PED_COMP_HEAD, 	0, 0) //(head)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", PED_COMP_HAIR, 	3, 0) //(hair)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", PED_COMP_TORSO, 	4, 0) //(uppr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", PED_COMP_LEG, 	4, 0) //(lowr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", PED_COMP_HAND, 	0, 0) //(hand)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", PED_COMP_FEET, 	2, 0) //(feet)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", PED_COMP_TEETH, 	0, 0) //(teef)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", PED_COMP_SPECIAL, 3, 0) //(accs)
ENDPROC

PROC SET_AMANDA_PED_COMPONENT_VARIATIONS(BOOL &bComponentVariationsSet)
	IF ( bComponentVariationsSet = FALSE )
		IF DOES_ENTITY_EXIST(psAmanda.PedIndex)
			IF NOT IS_ENTITY_DEAD(psAmanda.PedIndex)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_HEAD, 		0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_BERD, 		0, 0) //(berd)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_HAIR, 		4, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_TORSO, 		5, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_LEG, 		5, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_HAND, 		0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_TEETH, 		0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_SPECIAL, 	0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_DECL, 		1, 0) //(decl)
				bComponentVariationsSet = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_AMANDA_CUTSCENE_COMPONENT_VARIATIONS()
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HEAD, 		0, 0) //(head)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_BERD, 		0, 0) //(berd)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HAIR, 		4, 0) //(hair)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_TORSO, 		5, 0) //(uppr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_LEG, 		5, 0) //(lowr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HAND, 		0, 0) //(hand)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_TEETH, 		0, 0) //(teef)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_SPECIAL, 	0, 0) //(accs)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_DECL, 		1, 0) //(decl)
ENDPROC

PROC SET_FABIEN_PED_COMPONENT_VARIATIONS(BOOL &bComponentVariationsSet)
	IF ( bComponentVariationsSet = FALSE )
		IF DOES_ENTITY_EXIST(psFabien.PedIndex)
			IF NOT IS_ENTITY_DEAD(psFabien.PedIndex)
				SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_HEAD, 		0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_HAIR, 		0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_TORSO, 		1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_LEG, 		1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_HAND, 		0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_TEETH, 		0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_SPECIAL, 	1, 0) //(accs)
				bComponentVariationsSet = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_FABIEN_CUTSCENE_COMPONENT_VARIATIONS()
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HEAD, 		0, 0) //(head)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HAIR, 		0, 0) //(hair)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_TORSO, 		1, 0) //(uppr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_LEG, 		1, 0) //(lowr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HAND, 		0, 0) //(hand)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_TEETH, 		0, 0) //(teef)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_SPECIAL, 	1, 0) //(accs)
ENDPROC

PROC SET_LAZLOW_PED_COMPONENT_VARIATIONS(BOOL &bComponentVariationsSet, BOOL bShirtOff, BOOL bPierced, BOOL bHairCut, BOOL bTattooed, BOOL bChestTattooed)
	IF ( bComponentVariationsSet = FALSE )
		IF DOES_ENTITY_EXIST(psLazlow.PedIndex)
			IF NOT IS_ENTITY_DEAD(psLazlow.PedIndex)
				SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 		PED_COMP_HEAD, 			0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 		PED_COMP_LEG, 			0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 		PED_COMP_HAND, 			0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 		PED_COMP_FEET, 			1, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 		PED_COMP_TEETH, 		0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 		PED_COMP_SPECIAL, 		0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 		PED_COMP_DECL, 			0, 0) //(decl)
				
				IF bHairCut = FALSE
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_HAIR, 			0, 0) //(hair)
				ELSE
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_HAIR, 			1, 0) //(hair)
				ENDIF
				IF bShirtOff = FALSE
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_TORSO, 		2, 0) //(uppr)
				ELSE
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_TORSO, 		1, 0)	//uppr
				ENDIF
				IF bPierced = FALSE
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_SPECIAL2, 		0, 0) //(task)
				ELSE
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_SPECIAL2, 		0, 7)	//task
				ENDIF
				IF bTattooed
					IF bChestTattooed
						SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_DECL, 		0, 5)	//decl
					ELSE
						SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_DECL, 		1, 5)	//decl
					ENDIF
				ELSE
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, 	PED_COMP_DECL, 			0, 0)	//decl
				ENDIF
				bComponentVariationsSet = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(BOOL bShirtOff, BOOL bPierced, BOOL bHairCut, BOOL bTattooed, BOOL bChestTattooed)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 		PED_COMP_HEAD, 			0, 0) //(head)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 		PED_COMP_LEG, 			0, 0) //(lowr)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 		PED_COMP_HAND, 			0, 0) //(hand)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 		PED_COMP_FEET, 			1, 0) //(feet)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 		PED_COMP_TEETH, 		0, 0) //(teef)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 		PED_COMP_SPECIAL, 		0, 0) //(accs)
	SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 		PED_COMP_DECL, 			0, 0) //(decl)
	IF bHairCut = FALSE
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_HAIR, 			0, 0) //(hair)
	ELSE
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_HAIR, 			1, 0) //(hair)
	ENDIF
	IF bShirtOff = FALSE
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_TORSO, 		2, 0) //(uppr)
	ELSE
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_TORSO, 		1, 0)	//uppr
	ENDIF
	IF bPierced = FALSE
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_SPECIAL2, 		0, 0) //(task)
	ELSE
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_SPECIAL2, 		0, 7)	//task
	ENDIF
	IF bTattooed
		IF bChestTattooed
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_DECL, 		0, 5)	//decl
		ELSE
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_DECL, 		1, 5)	//decl
		ENDIF
	ELSE
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Lazlow", 	PED_COMP_DECL, 			0, 0)	//decl
	ENDIF
ENDPROC

PROC SET_TATGUY_PED_COMPONENT_VARIATIONS(BOOL &bComponentVariationsSet)
	IF ( bComponentVariationsSet = FALSE )
		IF DOES_ENTITY_EXIST(psTatguy.PedIndex)
			IF NOT IS_ENTITY_DEAD(psTatguy.PedIndex)
				SET_PED_COMPONENT_VARIATION(psTatguy.PedIndex, PED_COMP_HEAD, 		0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(psTatguy.PedIndex, PED_COMP_HAIR, 		0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(psTatguy.PedIndex, PED_COMP_TORSO, 		0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(psTatguy.PedIndex, PED_COMP_LEG, 		0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(psTatguy.PedIndex, PED_COMP_FEET, 		0, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(psTatguy.PedIndex, PED_COMP_DECL, 		0, 0) //(decl)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_REVERSING(VEHICLE_INDEX VehicleIndex)

	IF DOES_ENTITY_EXIST(VehicleIndex)

		IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
		
			VECTOR vVelocity = GET_ENTITY_SPEED_VECTOR(VehicleIndex, TRUE)

			IF ( vVelocity.y < -1.0 )
						
				RETURN TRUE
				
			ENDIF

		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

PROC RUN_INTERIOR_UNPINNING_CHECK(VECTOR vInteriorCoords, FLOAT fRadius)
	
	IF NOT ARE_VECTORS_EQUAL(vInteriorCoords, << 0.0, 0.0, 0.0 >>)		//don't check the interior if the coordinates are not valid

		IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorCoords))	//unpin the interior if it is pinned and player is away from it

			IF ( GET_DISTANCE_BETWEEN_COORDS(vInteriorCoords, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE) > fRadius )
		
				UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorCoords))
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unpinning interior at coordinates ", vInteriorCoords, " in memory.")
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Warps specified ped to new coordinates and sets specified ped's new heading.
/// PARAMS:
///    ped - PED_INDEX to warp.
///    vNewPosition - VECTOR coordinates specifying new ped's position.
///    fNewHeading - FLOAT specifying new ped's heading.
///    bKeepVehicle - Specifies if ped should keep vehicle they are currently in.
///    bResetGameplayCamera - Specifies if gameplay camera should be positioned behind player's back. Works only for PLAYER_PED_ID().
///    bLoadScene - Specifies if scene at new provided coordinates should be loaded. Works only for PLAYER_PED_ID().
PROC WARP_PED(PED_INDEX ped, VECTOR vNewPosition, FLOAT fNewHeading, BOOL bKeepVehicle, BOOL bResetGameplayCamera, BOOL bLoadScene)

	IF NOT IS_PED_INJURED(ped)
	
		IF ( bKeepVehicle = TRUE )
			SET_PED_COORDS_KEEP_VEHICLE(ped, vNewPosition)
		ELIF ( bKeepVehicle = FALSE ) 
			SET_ENTITY_COORDS(ped, vNewPosition)
		ENDIF
		
		SET_ENTITY_HEADING(ped, fNewHeading)
		
		IF ( ped = PLAYER_PED_ID())
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
			ENDIF
		
			IF ( bResetGameplayCamera = TRUE)
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
			ENDIF
			
			IF ( bLoadScene = TRUE )
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", vNewPosition, ". This might take a while to load.")
				#ENDIF
			
				LOAD_SCENE(vNewPosition)
				
			ENDIF
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
		
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CLEANUP_PED(PED_STRUCT &ped, BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	IF ( bDelete = FALSE )
	
		IF DOES_ENTITY_EXIST(ped.PedIndex)
			
			IF ( bDeathOnly = TRUE )
		
				IF IS_PED_INJURED(ped.PedIndex)
					
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF

				ENDIF
				
			ELIF ( bDeathOnly = FALSE )

				IF NOT IS_PED_INJURED(ped.PedIndex)
				
					IF IS_PED_GROUP_MEMBER(ped.PedIndex, PLAYER_GROUP_ID())
						REMOVE_PED_FROM_GROUP(ped.PedIndex)
					ENDIF
				
					SET_PED_KEEP_TASK(ped.PedIndex, bKeepTask)
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF
					
				ENDIF

			ENDIF
		
		ENDIF
	
	ELIF ( bDelete = TRUE )

		IF DOES_ENTITY_EXIST(ped.PedIndex)

			IF NOT IS_PED_INJURED(ped.PedIndex)
				IF IS_PED_GROUP_MEMBER(ped.PedIndex, PLAYER_GROUP_ID())
					REMOVE_PED_FROM_GROUP(ped.PedIndex)
				ENDIF
			ENDIF
			
			DELETE_PED(ped.PedIndex)
			
			IF DOES_BLIP_EXIST(ped.BlipIndex)
				REMOVE_BLIP(ped.BlipIndex)
			ENDIF

		ENDIF
				
		ped.iTimer							= 0
		ped.iProgress 						= 0
		ped.bComponentVariationsSet			= FALSE
		
	ENDIF

ENDPROC

PROC CLEANUP_MISSION_OBJECT(OBJECT_STRUCT &osObject, BOOL bDelete = FALSE)

	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)

		IF IS_ENTITY_ATTACHED(osObject.ObjectIndex)
			DETACH_ENTITY(osObject.ObjectIndex)
		ENDIF

		IF ( bDelete = TRUE )
			DELETE_OBJECT(osObject.ObjectIndex)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(osObject.ObjectIndex)
		ENDIF
	
	ENDIF

ENDPROC

FUNC INT GET_VEHICLE_EXIT_DELAY(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex)

	IF IS_PED_SITTING_IN_THIS_VEHICLE(PedIndex, VehicleIndex)
		
		SWITCH GET_PED_VEHICLE_SEAT(PedIndex, VehicleIndex)
			CASE VS_DRIVER
				RETURN 1250
			BREAK
			CASE VS_FRONT_RIGHT
				RETURN 1500
			BREAK
			CASE VS_BACK_LEFT
				RETURN 250
			BREAK
			CASE VS_BACK_RIGHT
				RETURN 50
			BREAK
			DEFAULT
				RETURN 0
			BREAK
		ENDSWITCH
		
	ENDIF
	
	RETURN 0

ENDFUNC

//|======================= DEBUG VARIABLES, PROCEDURES & FUNCTIONS =======================|

#IF IS_DEBUG_BUILD

	//mission skip menu variables
	MissionStageMenuTextStruct	SkipMenu[18]
	INT 			iReturnStage
	BOOL			bStageResetFlag
	
	WIDGET_GROUP_ID Family6Widgets

	/// PURPOSE:
	///    Returns string name of the specified mission stage.
	/// PARAMS:
	///    eStage - Mission stage to get string name for.
	/// RETURNS:
	///    String name of the specified mission stage.
	FUNC STRING GET_MISSION_STAGE_NAME_FOR_DEBUG(MISSION_STAGES eStage)
	
		SWITCH eStage

			CASE MISSION_STAGE_PRE_INTRO
				RETURN "MISSION_STAGE_PRE_INTRO"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_INTRO_P1
				RETURN "MISSION_STAGE_CUTSCENE_INTRO_P1"
			BREAK
			CASE MISSION_STAGE_TIMELAPSE
				RETURN "MISSION_STAGE_TIMELAPSE"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_INTRO_P2
				RETURN "MISSION_STAGE_CUTSCENE_INTRO_P2"
			BREAK
			CASE MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
				RETURN "MISSION_STAGE_DRIVE_TO_BEAN_MACHINE"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_BEAN_MACHINE
				RETURN "MISSION_STAGE_CUTSCENE_BEAN_MACHINE"
			BREAK
			CASE MISSION_STAGE_DRIVE_TO_TRACEY
				RETURN "MISSION_STAGE_DRIVE_TO_TRACEY"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SHOP_1
				RETURN "MISSION_STAGE_CUTSCENE_SHOP_1"
			BREAK
			CASE MISSION_STAGE_PIERCE_LAZLOW
				RETURN "MISSION_STAGE_PIERCE_LAZLOW"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SHOP_2
				RETURN "MISSION_STAGE_CUTSCENE_SHOP_2"
			BREAK
			CASE MISSION_STAGE_TATTOO_LAZLOW
				RETURN "MISSION_STAGE_TATTOO_LAZLOW"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SHOP_3
				RETURN "MISSION_STAGE_CUTSCENE_SHOP_3"
			BREAK
			CASE MISSION_STAGE_CUT_LAZLOW_HAIR
				RETURN "MISSION_STAGE_CUT_LAZLOW_HAIR"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SHOP_4
				RETURN "MISSION_STAGE_CUTSCENE_SHOP_4"
			BREAK
			CASE MISSION_STAGE_DRIVE_TO_SHRINK
				RETURN "MISSION_STAGE_DRIVE_TO_SHRINK"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SHRINK
				RETURN "MISSION_STAGE_CUTSCENE_SHRINK"
			BREAK
			CASE MISSION_STAGE_DRIVE_HOME
				RETURN "MISSION_STAGE_DRIVE_HOME"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_END
				RETURN "MISSION_STAGE_CUTSCENE_END"
			BREAK
		ENDSWITCH
		
		RETURN "INCORRECT_MISSION_STAGE"
	
	ENDFUNC

	PROC CREATE_DEBUG_WIDGETS()
		
		Family6Widgets = START_WIDGET_GROUP("Mission: Family6")
		
			START_WIDGET_GROUP("Tattoo Buttons")
				ADD_WIDGET_FLOAT_SLIDER("TATTOO_BUTTONS_X", 		TATTOO_BUTTONS_X, 		0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("TATTOO_BUTTONS_Y", 		TATTOO_BUTTONS_Y, 		0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("TATTOO_BUTTONS_WIDTH", 	TATTOO_BUTTONS_WIDTH, 	0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("TATTOO_BUTTONS_HEIGHT", 	TATTOO_BUTTONS_HEIGHT, 	0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("TATTOO_BUTTONS_SCALE", 	TATTOO_BUTTONS_SCALE, 	0.0, 2.0, 0.001)
			STOP_WIDGET_GROUP()
		
		STOP_WIDGET_GROUP()
		
		SET_LOCATES_HEADER_WIDGET_GROUP(Family6Widgets)	
	
	ENDPROC
	
	PROC DELETE_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(Family6Widgets)
			DELETE_WIDGET_GROUP(Family6Widgets)
		ENDIF
	ENDPROC
	
	PROC CREATE_DEBUG_MISSION_STAGE_MENU()

		SkipMenu[0].sTxtLabel		= "PRE INTRO"
		SkipMenu[1].sTxtLabel		= "CUT - INTRO P1 - FAM_6_INT"
		SkipMenu[2].sTxtLabel		= "TIMELAPSE"
		SkipMenu[3].sTxtLabel	 	= "CUT - INTRO P2 - FAM_6_INT_P3_T7"
		SkipMenu[4].sTxtLabel	 	= "DRIVE TO BEAN MACHINE"
		SkipMenu[5].sTxtLabel	 	= "CUT - BEAN MACHINE - FAM_6_MCS_1"
		SkipMenu[6].sTxtLabel	 	= "DRIVE TO TRACEY"
		SkipMenu[7].sTxtLabel	 	= "CUT - SHOP 1 - FAM_6_MCS_2_CONCAT"
		SkipMenu[8].sTxtLabel	 	= "PIERCE LAZLOW"
		SkipMenu[9].sTxtLabel	 	= "CUT - SHOP 2 - FAM_6_MCS_3"
		SkipMenu[10].sTxtLabel	 	= "TATTOO LAZLOW"
		SkipMenu[11].sTxtLabel	 	= "CUT - SHOP 3 - FAM_6_MCS_4"
		SkipMenu[12].sTxtLabel	 	= "CUT LAZLOW HAIR"
		SkipMenu[13].sTxtLabel	 	= "CUT - SHOP 4 - FAM_6_MCS_5"
		SkipMenu[14].sTxtLabel		= "DRIVE TO SHRINK"
		SkipMenu[15].sTxtLabel		= "CUT - SHRINK - FAM_6_MCS_6"
		SkipMenu[16].sTxtLabel		= "DRIVE HOME"
		SkipMenu[17].sTxtLabel		= "CUT - END - FAM_6_MCS_6_P4_B"
	
	ENDPROC

	PROC RUN_DEBUG_PASS_AND_FAIL_CHECK(MISSION_STAGES &eStage, MISSION_FAILS &eFailReason)
	
		IF IS_KEYBOARD_KEY_PRESSED (KEY_S)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			eStage 		= MISSION_STAGE_PASSED
	    ENDIF
		
	    IF IS_KEYBOARD_KEY_PRESSED (KEY_F)
			eStage 		= MISSION_STAGE_FAILED
			eFailReason = MISSION_FAIL_FORCE_FAIL
	    ENDIF
	
	ENDPROC
	
	/// PURPOSE:
	///    Performs a screen fade in with a wait for a specified time.
	/// PARAMS:
	///    iTime - Duration of the fade in miliseconds.
	PROC DO_SCREEN_FADE_IN_FOR_DEBUG(INT iTime)

		IF NOT IS_SCREEN_FADED_IN()
		
			DO_SCREEN_FADE_IN(iTime)
			
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
		ENDIF

	ENDPROC

	/// PURPOSE:
	///    Performs a screen fade out with a wait for a specified time.
	/// PARAMS:
	///    iTime - Duration of the fade in miliseconds.
	PROC DO_SCREEN_FADE_OUT_FOR_DEBUG(INT iTime)

		IF NOT IS_SCREEN_FADED_OUT()
		
			DO_SCREEN_FADE_OUT(iTime)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT(0)
			ENDWHILE
			
		ENDIF

	ENDPROC

	/// PURPOSE:
	///    Stops an active mocap cutscene and waits until it has finished.
	PROC DO_SAFE_STOP_CUTSCENE()

		IF IS_CUTSCENE_PLAYING()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			STOP_CUTSCENE(TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)	//needs to be called to make sure all scripted systems work correctly when cutscene is skipped		
		ENDIF

		WHILE NOT HAS_CUTSCENE_FINISHED()
			WAIT(0)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for the cutscene to finish.")
			#ENDIF
		ENDWHILE
		
	ENDPROC
	
	PROC RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()

		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting mission stage cleanup.")

		KILL_PHONE_CONVERSATION()
		HANG_UP_AND_PUT_AWAY_PHONE(TRUE)

		KILL_ANY_CONVERSATION()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		REMOVE_CUTSCENE()
		
		IF ( bReplayStartEventTriggered = TRUE )
			REPLAY_STOP_EVENT()
		ENDIF

		CLEANUP_PED(psJimmy, TRUE)
		CLEANUP_PED(psAmanda, TRUE)
		CLEANUP_PED(psFabien, TRUE)
		CLEANUP_PED(psTracey, TRUE)
		CLEANUP_PED(psLazlow, TRUE)
		CLEANUP_PED(psHipster, TRUE)
		CLEANUP_PED(psTatguy, TRUE)
				
		IF DOES_ENTITY_EXIST(osLeftDoorDummy.ObjectIndex)
			REMOVE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName)
		ENDIF
		
		IF DOES_ENTITY_EXIST(osRightDoorDummy.ObjectIndex)
			REMOVE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName)
		ENDIF
		
		CLEANUP_MISSION_OBJECT(osMug, TRUE)
		CLEANUP_MISSION_OBJECT(osShirt, TRUE)
		CLEANUP_MISSION_OBJECT(osLaptop, TRUE)
		CLEANUP_MISSION_OBJECT(osScissors, TRUE)
		CLEANUP_MISSION_OBJECT(osPonyTail, TRUE)
		CLEANUP_MISSION_OBJECT(osPiercingGun, TRUE)
		CLEANUP_MISSION_OBJECT(osTattooNeedle, TRUE)
		CLEANUP_MISSION_OBJECT(osLeftDoorDummy, TRUE)
		CLEANUP_MISSION_OBJECT(osRightDoorDummy, TRUE)

		IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
			DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vsAmandasCar.VehicleIndex)
			DELETE_VEHICLE(vsAmandasCar.VehicleIndex)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHair)
			STOP_PARTICLE_FX_LOOPED(ptfxHair)
		ENDIF
		
		IF NOT HAS_SOUND_FINISHED(iTattooSoundID)
			STOP_SOUND(iTattooSoundID)
		ENDIF
		
		STOP_AUDIO_SCENES()
		REMOVE_PTFX_ASSET()
		RELEASE_SCRIPT_AUDIO_BANK()
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
		
		IF IS_PC_VERSION()
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov_kb)
		ENDIF
				
		//remove anim dictionaries
		REMOVE_ANIM_DICT("missfam2mcs_intp1")
		REMOVE_ANIM_DICT("missfam6ig_1_pierce")
		REMOVE_ANIM_DICT("missfam6ig_7_tattoo")
		REMOVE_ANIM_DICT("missfam6ig_8_ponytail")
		REMOVE_ANIM_DICT("missfam6leadinoutfam_6_mcs_1")
		REMOVE_ANIM_DICT("missfam6leadinoutfam_6_mcs_2")
		REMOVE_ANIM_DICT("missfam6leadinoutfam_6_mcs_5")
		REMOVE_ANIM_DICT("amb@world_human_stand_impatient@female@no_sign@idle_a")
		
		IF NOT HAS_SOUND_FINISHED(iTattooSoundID)
			STOP_SOUND(iTattooSoundID)
		ENDIF
		
		DESTROY_ALL_CAMS()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		
		CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
		
		CLEAR_TRIGGERED_LABELS()
		
		//DISABLE_TAXI_HAILING(FALSE)
		
		REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedZone)
		
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission stage cleanup.")
	
	ENDPROC
	
	PROC RUN_DEBUG_MISSION_STAGE_MENU(INT &iSelectedStage, MISSION_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenu, iSelectedStage, ENUM_TO_INT(eStage), FALSE, "FAMILY 6")
		
			DO_SCREEN_FADE_OUT_FOR_DEBUG(DEFAULT_FADE_TIME)
						
			DO_SAFE_STOP_CUTSCENE()
			
			CLEAR_PRINTS()
			
			CLEAR_HELP()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
			
			eStage = INT_TO_ENUM(MISSION_STAGES, iSelectedStage)
			
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via mission stage menu.")
			
			//reset the debug stage reset flag
			bResetStage	= FALSE
			
			bSkipActive = TRUE
			
			RESET_MISSION_FLAGS()
		
		ENDIF
	
	ENDPROC
	
	PROC RUN_DEBUG_SKIPS(MISSION_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		INT iCurrentStage = ENUM_TO_INT(eStage)
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
			IF eStage <> MISSION_STAGE_CUTSCENE_END
		
				DO_SCREEN_FADE_OUT_FOR_DEBUG(DEFAULT_FADE_TIME)
				
				DO_SAFE_STOP_CUTSCENE()
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
				
				iCurrentStage++
				
				eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)
								
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via J skip.")
				
				//reset the debug stage reset flag
				bResetStage	= FALSE
				
				bSkipActive = TRUE
				
				RESET_MISSION_FLAGS()

			ENDIF
			
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		
			IF eStage <> MISSION_STAGE_PRE_INTRO
		
				DO_SCREEN_FADE_OUT_FOR_DEBUG(DEFAULT_FADE_TIME)
				
				DO_SAFE_STOP_CUTSCENE()
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
				
				IF ( bResetStage = FALSE )
				
					iCurrentStage--
					
					eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)			
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via P skip.")
				
				ELIF ( bResetStage = TRUE )
				
					eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)			
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " reset via P skip.")
				
				ENDIF
				
				//reset the debug stage reset flag
				bResetStage	= FALSE
				
				bSkipActive = TRUE
				
				RESET_MISSION_FLAGS()
				
			ENDIF
			
		ENDIF
	
	ENDPROC
	
#ENDIF

//|===================== END DEBUG VARIABLES, PROCEDURES & FUNCTIONS =====================|

/// PURPOSE:
///    Adds a model to model request array.
/// PARAMS:
///    ModelName - Model name being requested.
///    array - MODEL_NAMES array to add the requested model name to.
///    iArrayLength - Maximum length of the array.
///    iModelsAlreadyRequested - Number of models already requested in this array.
PROC ADD_MODEL_REQUEST_TO_ARRAY(MODEL_NAMES ModelName, MODEL_NAMES &array[], INT iArrayLength, INT &iModelsAlreadyRequested)

	INT i = 0
	
	BOOL bModelAlreadyRequested = FALSE
	
	IF ( iModelsAlreadyRequested >= 0 )
	
		IF ( iModelsAlreadyRequested <= ( iArrayLength - 1 ) )
		
			//loop through the array (from 0 to number of models already requested)
			//to see if the array already contains the model name being requested
			//modify the boolean flag if the model was found in array, otherwise leave the flag unchanged
			FOR i = 0 TO ( iModelsAlreadyRequested )
			
				IF ( array[i] = ModelName )
				
					bModelAlreadyRequested = TRUE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " is already in model array.")
					#ENDIF
					
				ENDIF
				
			ENDFOR
			
			IF ( bModelAlreadyRequested = FALSE )
			
				REQUEST_MODEL(ModelName)
				
				//iModelsAlreadyRequested will be the model array index of the model being requested
				array[iModelsAlreadyRequested] = ModelName
				
				iModelsAlreadyRequested++
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " and added to model array.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Number of models requested: ", iModelsAlreadyRequested, ".")
				#ENDIF
				
			ENDIF
		
		ELSE
		
			SCRIPT_ASSERT("Number of requested models is greater than the length of the model array.")
		
		ENDIF
	
	ELSE
	
		SCRIPT_ASSERT("Number of requested models is less than 0.")
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if all requested models from model array are loaded into memory.
/// PARAMS:
///    array - MODEL_NAMES array holding the models.
///    iModelsAlreadyRequested - Number of already requested models. Should be greated than 0, since 0 means no models have been requested.
/// RETURNS:
///    TRUE if all models from model array are loaded into memory. FALSE if otherwise.
FUNC BOOL ARE_REQUESTED_MODELS_LOADED(MODEL_NAMES &array[], INT &iModelsAlreadyRequested) 

	INT i = 0
	
	IF ( iModelsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iModelsAlreadyRequested - 1 )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model in array[", i, "]: ", GET_MODEL_NAME_FOR_DEBUG(array[i]), ".")
			#ENDIF
		
			IF NOT HAS_MODEL_LOADED(array[i])
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), " is loading.")
				#ENDIF
				
				//if the model is not loaded keep requesting it
				REQUEST_MODEL(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), ".")
				#ENDIF
				
				RETURN FALSE
				
			ENDIF
			
		ENDFOR
		
	ENDIF	
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Models from model array loaded. Number of models loaded: ", iModelsAlreadyRequested, ".")
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleans model array from requested models.
/// PARAMS:
///    array - MODEL_NAMES array.
///    iModelsAlreadyRequested - Number of already requested models. Should be greated than 0, since 0 means no models have been requested.
PROC CLEANUP_MODEL_ARRAY(MODEL_NAMES &array[], INT &iModelsAlreadyRequested)
	
	INT i = 0
	
	IF ( iModelsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iModelsAlreadyRequested - 1 )
		
			IF array[i] <> DUMMY_MODEL_FOR_SCRIPT
			
				SET_MODEL_AS_NO_LONGER_NEEDED(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), " cleaned up from model array by setting it as no longer needed.")
				#ENDIF
				
				array[i] = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
			
		ENDFOR
		
	ENDIF
	
	iModelsAlreadyRequested = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model array cleaned up. Number of requested models set to 0.")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if two strings are equal.
/// PARAMS:
///    sString1 - First string.
///    sString2 - Second string.
/// RETURNS:
///    TRUE if strings are equal, FALSE if otherwise or if any of the strings is empty or NULL.
FUNC BOOL IS_STRING_EQUAL_TO_STRING(STRING sString1, STRING sString2)

	IF NOT Is_String_Null_Or_Empty(sString1)
	AND NOT Is_String_Null_Or_Empty(sString2)

		RETURN ARE_STRINGS_EQUAL(sString1, sString2)

	ELSE
	
		RETURN FALSE
	
	ENDIF

ENDFUNC

/// PURPOSE:
///    Adds animation dictionary to animation request array.
/// PARAMS:
///    sAnimName - Name of the animation dictionary to request.
///    array - STRING array containing requested animations.
///    iArrayLength - Maximum length of the array
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
PROC ADD_ANIMATION_REQUEST_TO_ARRAY(STRING sAnimName, STRING &array[], INT iArrayLength, INT &iAnimationsAlreadyRequested)

	INT i = 0
	
	BOOL bAnimationRequested
	
	IF ( iAnimationsAlreadyRequested >= 0 )
	
		IF ( iAnimationsAlreadyRequested <= (iArrayLength - 1 ) )
	
		    FOR i = 0 TO ( iAnimationsAlreadyRequested )
			
				IF IS_STRING_EQUAL_TO_STRING(sAnimName, array[i])
				
				    bAnimationRequested = TRUE
					
				    #IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested animation ", sAnimName, " is already in animation array.")
					#ENDIF
					
				ENDIF
				
		    ENDFOR
			
		    IF ( bAnimationRequested = FALSE )
			
				REQUEST_ANIM_DICT(sAnimName)
			
				array[iAnimationsAlreadyRequested] = sAnimName

				iAnimationsAlreadyRequested++

				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested animation ", sAnimName, " and added to animation array.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Number of animations requested: ", iAnimationsAlreadyRequested, ".")
				#ENDIF
				  
		    ENDIF
		
		ELSE
		
			SCRIPT_ASSERT("Number of requested animations is greater than the length of the animation array.")
			
		ENDIF
	
	ELSE
	
		SCRIPT_ASSERT("Number of requested animations is less than 0.")
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if all requested animations are loaded into memory.
/// PARAMS:
///    array - STRING array containing requested animations.
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
/// RETURNS:
///    TRUE if all requested animations from animation array are loaded into memory. FALSE if otherwise.
FUNC BOOL ARE_REQUESTED_ANIMATIONS_LOADED(STRING &array[], INT &iAnimationsAlreadyRequested) 
	
	INT i = 0
	
	IF ( iAnimationsAlreadyRequested > 0 )
	
	    FOR i = 0 TO ( iAnimationsAlreadyRequested - 1 )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary in array[", i, "]: ", array[i], ".")
			#ENDIF
		
			IF NOT HAS_ANIM_DICT_LOADED(array[i])
			
			    #IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary ", array[i], " is loading.")
				#ENDIF
				
				//if the animation dictionary is not loaded keep requesting it
				REQUEST_ANIM_DICT(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting animation dictionary ", array[i], ".")
				#ENDIF
				
				
			    RETURN FALSE
				
			ENDIF
			
	    ENDFOR
		
	ENDIF   
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animations from animation array loaded. Number of animations loaded: ", iAnimationsAlreadyRequested, ".")
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleans animation array from requested animations.
/// PARAMS:
///    array - STRING array of animations to clean.
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
PROC CLEANUP_ANIMATION_ARRAY(STRING &array[], INT &iAnimationsAlreadyRequested)
	
	INT i = 0
	
	STRING sNull = NULL
	
	IF ( iAnimationsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iAnimationsAlreadyRequested - 1 )
		
			//brute force remove animation dictionary from memory
		
			REMOVE_ANIM_DICT(array[i])
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary ", array[i], " removed from memory and cleaned up from animation array.")
			#ENDIF
			
			array[i] = sNull

		ENDFOR
		
	ENDIF
	
	iAnimationsAlreadyRequested = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation array cleaned up. Number of requested animations set to 0.")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if the specified mocap cutscene has loaded.
///    Removes any other loaded mocap cutscene and requests the specified cutscene until it is loaded.
/// PARAMS:
///    sSceneName - Mocap cutscene name to check for being loaded.
///    CutsceneSection - Cutscene section specifying at what section to start the cutscene. Use CS_SECTION_1 to play from start.
/// RETURNS:
///    TRUE if the specified cutscene has loaded, FALSE if otherwise.   
FUNC BOOL HAS_REQUESTED_CUTSCENE_LOADED(STRING sSceneName, CUTSCENE_SECTION CutsceneSection = CS_SECTION_1)

	//check if the specified cutscene has loaded first
	IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE(sSceneName)
	
		RETURN TRUE
	
	ELSE
		
		//if the expected reuqested cutscene has not loaded, but there already is a loaded cutscene
		//then remove it and keep requesting specified mocap cutscene
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
			REMOVE_CUTSCENE()
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": HAS_REQUESTED_CUTSCENE_LOADED() Removing cutscene from memory, because cutscene ", sSceneName, " was expected to be loaded.")
			#ENDIF
			
		ENDIF
	
		IF ( CutsceneSection = CS_SECTION_1) 
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading.")
			#ENDIF
			
			//if the cutscene is not loaded, keep requesting it
			REQUEST_CUTSCENE(sSceneName)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, ".")
			#ENDIF
		
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading with playback list.")
			#ENDIF
			
			//if the cutscene is not loaded, keep requesting it
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sSceneName, CutsceneSection)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, " cutscene with playback list.")
			#ENDIF
		
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_MISSION_PED_PROPERTIES(PED_INDEX PedIndex, REL_GROUP_HASH relGroupHash, BOOL bCanFlyThroughWindscreen, BOOL bKeepRelGroupOnCleanup,
								BOOL bCanBeTargetted, BOOL bIsEnemy)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_PED_INJURED(PedIndex)
		
			SET_PED_CONFIG_FLAG(PedIndex, PCF_OpenDoorArmIK, TRUE)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_WillFlyThroughWindscreen, bCanFlyThroughWindscreen)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, bKeepRelGroupOnCleanup)
			SET_PED_CAN_BE_TARGETTED(PedIndex, bCanBeTargetted)
			SET_PED_AS_ENEMY(PedIndex, bIsEnemy)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PedIndex, TRUE)
			
			
			
					
			SET_PED_LOD_MULTIPLIER(PedIndex, 5.0)
			
			IF ( relGroupHash != RELGROUPHASH_NO_RELATIONSHIP )
				SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, relGroupHash)
			ENDIF
		
		ENDIF
	ENDIF

ENDPROC

PROC SET_VEHICLE_AS_MISSION_CRITICAL(VEH_STRUCT &vehicle, BOOL bMissionCritical)

	SET_VEHICLE_HAS_STRONG_AXLES(vehicle.VehicleIndex, bMissionCritical)

	//limit damage that can be done to the vehicle
	SET_VEHICLE_CAN_LEAK_OIL(vehicle.VehicleIndex, NOT bMissionCritical)
	SET_VEHICLE_CAN_LEAK_PETROL(vehicle.VehicleIndex, NOT bMissionCritical)
	
	//make the vehicle not attach to tow truck if mission critical
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehicle.VehicleIndex, NOT bMissionCritical)
	
	IF IS_THIS_MODEL_A_CAR(vehicle.ModelName)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates a ped or player ped to be used by player and returns TRUE if such pad was created successfully.
/// PARAMS:
///    psPed - PED_STRUCT containg ped details, like PED_INDEX, blip, coordinates, heading, model etc.
///    bPlayerPed - Boolean indicating if a player ped or NPC ped should be created.
///    relGroupHash - Relationship group hash for the created ped.
///    bCreateBlip - Boolean indicating if a blip for this ped should be created.
///    eCharacter - Enum specifying character from story characters list. Use NO_CHARACTER for characters not stored in that list and provide a MODEL_NAMES in PED_STRUCT.
///    bCanFlyThroughWindscreen - Sets if ped can fly through windscreen when car crashes.
///    bCanBeTargetted - Sets if ped can be targetted by player.
///    bIsEnemy - Sets if ped is considered an enemy.
///    VehicleIndex - Specify a vehicle index if ped should be created inside a vehicle. IF not use NULL.
///    eVehicleSeat - Vehicle seat enum for peds created in vehicles.
/// RETURNS:
///    TRUE if ped was created successfully, FALSE if otherwise.
FUNC BOOL HAS_MISSION_PED_BEEN_CREATED(PED_STRUCT &psPed, BOOL bPlayerPed, REL_GROUP_HASH relGroupHash, BOOL bCreateBlip, enumCharacterList eCharacter,
									   BOOL bCanFlyThroughWindscreen = FALSE, BOOL bCanBeTargetted = FALSE, BOOL bIsEnemy = FALSE, VEHICLE_INDEX VehicleIndex = NULL,
									   VEHICLE_SEAT eVehicleSeat = VS_DRIVER, BOOL bKeepRelGroupOnCleanup = TRUE #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	IF ( bPlayerPed = FALSE )	//create non player ped
		
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
		
			REQUEST_MODEL(psPed.ModelName)
			
			IF HAS_MODEL_LOADED(psPed.ModelName)
		
				IF ( VehicleIndex = NULL )	//create ped outside of vehicle
				
					IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT if no character was specified
					
						psPed.PedIndex = CREATE_PED(PEDTYPE_MISSION, psPed.ModelName, psPed.vPosition, psPed.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
					
					ELSE								//use character name to define the ped MODEL_NAMES
					
						IF CREATE_NPC_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
						ENDIF
					
					ENDIF
					
					IF NOT IS_PED_INJURED(psPed.PedIndex)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
												   
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
					
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF
					#ENDIF
					
				ELIF ( VehicleIndex != NULL )	//create ped in a vehicle
				
					IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
						IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT
					
							psPed.PedIndex = CREATE_PED_INSIDE_VEHICLE(VehicleIndex, PEDTYPE_MISSION, psPed.ModelName, eVehicleSeat)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							
						ELSE								//use character name to define the ped MODEL_NAMES
						
							IF CREATE_NPC_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat)
								SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							ENDIF
						
						ENDIF
						
						IF NOT IS_PED_INJURED(psPed.PedIndex)
						
							SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
													   bCanBeTargetted, bIsEnemy)
							
							IF ( bCreateBlip = TRUE )
								psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
							ENDIF
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ENDIF
						#ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ELSE
		
			//if the ped already exists return true
			RETURN TRUE
		
		ENDIF
		
		
	ELIF ( bPlayerPed = TRUE )	//create player ped, for example a ped that player can hotswap to and take control of
	
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
	
			IF ( VehicleIndex = NULL )	//create player ped outside of vehicle
	
				IF CREATE_PLAYER_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading, TRUE)
				
					SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
					
					IF ( bCreateBlip = TRUE )
						psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF	
					#ENDIF
					
					RETURN TRUE
					
				ENDIF
				
			ELIF ( VehicleIndex != NULL )	//create player ped in a vehicle
			
				IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
					IF CREATE_PLAYER_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat, TRUE)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
						
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
						
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped  in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ENDIF
						#ENDIF
						
						RETURN TRUE
					
					ENDIF
				
				ENDIF
			
			ENDIF
		
		ELSE
		
			//if the ped already exists, return true
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Creates vehicle to be used on a mission by player and returns TRUE if such vehicle was created successfully.
/// PARAMS:
///    vsVehicle - VEH_STRUCT containing model, position and heading of the vehicle.
///    bPlayerVehicle - If set to TRUE will create player specific vehicles using CREATE_PLAYER_VEHICLE()
///    bCarStealVehicle - If set to TRUE will create car steal strand vehicle with a specific command. Works when bPlayerVehicle is FALSE.
///    eCharacter - Character enum indicating which character specific vehicle to create.
///    bMissionCritical - Sets the vehicle to be unable to leak oil/petrol and break off doors. Set to TRUE to stop these damage types to vehicle.
///    iColourCombination - Colour combination of the vehicle.
///    iColour1 - Colour 1 of the vehicle.
///    iColour2 - Colour 2 of the vehicle.
/// RETURNS:
///    TRUE if vehicle was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_VEHICLE_BEEN_CREATED(VEH_STRUCT &vsVehicle, BOOL bPlayerVehicle = FALSE, BOOL bCarStealVehicle = FALSE, enumCharacterList eCharacter = CHAR_MICHAEL,
										   BOOL bMissionCritical = TRUE, INT iColourCombination = -1, INT iColour1 = -1, INT iColour2 = -1
										   #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	//create any vehicle that can be used on a mission by player
	IF ( bPlayerVehicle = FALSE )
	
		IF NOT DOES_ENTITY_EXIST(vsVehicle.VehicleIndex)
		
			REQUEST_MODEL(vsVehicle.ModelName)
			
			IF HAS_MODEL_LOADED(vsVehicle.ModelName)

				IF( bCarStealVehicle = TRUE )
				
					vsVehicle.VehicleIndex = CREATE_CAR_STEAL_STRAND_CAR(vsVehicle.ModelName, vsVehicle.vPosition, vsVehicle.fHeading)
					SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
					
				ELSE
				
					IF ( eCharacter = NO_CHARACTER )
					
						vsVehicle.VehicleIndex = CREATE_VEHICLE(vsVehicle.ModelName, vsVehicle.vPosition, vsVehicle.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
					
					ELSE								//create npc vehicle based on the character enum specified
					
						IF CREATE_NPC_VEHICLE(vsVehicle.VehicleIndex, eCharacter, vsVehicle.vPosition, vsVehicle.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
						ENDIF
					
					ENDIF
					
				ENDIF
				
				
				
				IF IS_VEHICLE_DRIVEABLE(vsVehicle.VehicleIndex)
				
					//set vehicle colours, if they are provided
					IF ( iColour1 != -1)
					AND ( iColour2 != -1 )
						SET_VEHICLE_COLOURS(vsVehicle.VehicleIndex, iColour1, iColour2)
					ENDIF
					
					//set vehicle colour combination, if it is provided
					IF ( iColourCombination != -1 )
						SET_VEHICLE_COLOUR_COMBINATION(vsVehicle.VehicleIndex, iColourCombination)
					ENDIF
					
					SET_VEHICLE_HAS_STRONG_AXLES(vsVehicle.VehicleIndex, bMissionCritical)
					
					//limit damage that can be done to the vehicle
					SET_VEHICLE_CAN_LEAK_OIL(vsVehicle.VehicleIndex, NOT bMissionCritical)
					SET_VEHICLE_CAN_LEAK_PETROL(vsVehicle.VehicleIndex, NOT bMissionCritical)
					
					//make the vehicle not attach to tow truck if mission critical
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsVehicle.VehicleIndex, NOT bMissionCritical)
					
					IF IS_THIS_MODEL_A_CAR(vsVehicle.ModelName)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
					ENDIF
					
					SET_VEHICLE_ON_GROUND_PROPERLY(vsVehicle.VehicleIndex)
					
				ENDIF

				#IF IS_DEBUG_BUILD
					IF NOT Is_String_Null_Or_Empty(sDebugName)
						SET_VEHICLE_NAME_DEBUG(vsVehicle.VehicleIndex, sDebugName)
						IF ( bCarStealVehicle = TRUE )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created car steal strand vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ENDIF
					ELSE		
						IF ( bCarStealVehicle = TRUE )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created car steal strand vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ENDIF
					ENDIF
				#ENDIF
				
				RETURN TRUE
			
			ENDIF
		
		ELSE	//if the vehicle exists, return true
		
			RETURN TRUE
			
		ENDIF
		
	//create player specific vehicle
	ELIF ( bPlayerVehicle = TRUE )
	
		IF NOT DOES_ENTITY_EXIST(vsVehicle.VehicleIndex)
		
			//delete all previous instances of player ped vehicle in the world
			DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(eCharacter)
	
			//call the player vehicle creation until it returns true
			IF CREATE_PLAYER_VEHICLE(vsVehicle.VehicleIndex, eCharacter, vsVehicle.vPosition, vsVehicle.fHeading, TRUE)
			
				SET_VEHICLE_HAS_STRONG_AXLES(vsVehicle.VehicleIndex, bMissionCritical)
			
				//limit damage that can be done to the vehicle
				SET_VEHICLE_CAN_LEAK_OIL(vsVehicle.VehicleIndex, NOT bMissionCritical)
				SET_VEHICLE_CAN_LEAK_PETROL(vsVehicle.VehicleIndex, NOT bMissionCritical)
				
				//make the vehicle not attach to tow truck if mission critical
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsVehicle.VehicleIndex, NOT bMissionCritical)
				
				IF IS_THIS_MODEL_A_CAR(vsVehicle.ModelName)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
				ENDIF
			
				#IF IS_DEBUG_BUILD
					IF NOT Is_String_Null_Or_Empty(sDebugName)
						SET_VEHICLE_NAME_DEBUG(vsVehicle.VehicleIndex, sDebugName)
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
					ELSE
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
					ENDIF	
				#ENDIF
				
				RETURN TRUE
			
			ENDIF
		
		ELSE	//if the vehicle exists, return true
		
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Creates an object and returns TRUE if such object was created successfully.
/// PARAMS:
///    osObject - OBJECT_STRUCT containing model, position and rotation of the object.
///    bFreezeObject - Specify if this object's position should be frozen.
/// RETURNS:
///    TRUE if object was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_OBJECT_BEEN_CREATED(OBJECT_STRUCT &osObject, BOOL bFreezeObject = FALSE)

	IF NOT DOES_ENTITY_EXIST(osObject.ObjectIndex)
		
		REQUEST_MODEL(osObject.ModelName)
		
		IF HAS_MODEL_LOADED(osObject.ModelName)
		
			osObject.ObjectIndex = CREATE_OBJECT(osObject.ModelName, osObject.vPosition)
			
			SET_ENTITY_COORDS_NO_OFFSET(osObject.ObjectIndex, osObject.vPosition)
			SET_ENTITY_ROTATION(osObject.ObjectIndex, osObject.vRotation)
			SET_ENTITY_INVINCIBLE(osObject.ObjectIndex, TRUE)
			FREEZE_ENTITY_POSITION(osObject.ObjectIndex, bFreezeObject)
			SET_MODEL_AS_NO_LONGER_NEEDED(osObject.ModelName)
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object with model ", GET_MODEL_NAME_FOR_DEBUG(osObject.ModelName), " at coordinates ", osObject.vPosition, ".")
			#ENDIF
		
		ENDIF
	
	ENDIF
	
	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)

		RETURN TRUE
	
	ENDIF

	RETURN FALSE

ENDFUNC

PROC BUILD_REQUEST_BANK_FOR_MISSION_STAGE(MISSION_STAGES eStage)

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Building request bank for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

	SWITCH eStage

		CASE MISSION_STAGE_PRE_INTRO
		CASE MISSION_STAGE_CUTSCENE_INTRO_P1
		CASE MISSION_STAGE_TIMELAPSE
		CASE MISSION_STAGE_CUTSCENE_INTRO_P2
			//request nothing here
		BREAK
		
		CASE MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_BEAN_MACHINE
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psAmanda.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psAmanda.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psFabien.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psFabien.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsAmandasCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsAmandasCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_DRIVE_TO_TRACEY
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_SHOP_1
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psTracey.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psTracey.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psLazlow.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psLazlow.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_DRIVE_TO_SHRINK
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psTracey.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psTracey.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psLazlow.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psLazlow.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psTatguy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psTatguy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_ANIMATION_REQUEST_TO_ARRAY("missfam6leadinoutfam_6_mcs_5", MissionAnimations, MAX_ANIMATIONS, iAnimationsRequested)
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_SHRINK
		CASE MISSION_STAGE_DRIVE_HOME
		CASE MISSION_STAGE_CUTSCENE_END
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psTracey.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psTracey.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psAmanda.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psAmanda.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK

		DEFAULT
			
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished building request bank for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks for various mission fail conditions. Redirects mission flow to MISSION_STAGE_FAILED when one of fail conditions is true.
/// PARAMS:
///    eStage - Current mission stage.
///    eFail - Mission fail reason.
PROC RUN_FAIL_CHECKS(MISSION_STAGES &eStage, MISSION_FAILS &eFail)

	IF 	eStage <> MISSION_STAGE_PASSED
	AND eStage <> MISSION_STAGE_FAILED
	
		IF NOT IS_CUTSCENE_PLAYING()
		
			IF DOES_ENTITY_EXIST(psJimmy.PedIndex)
				IF ( FailFlags[MISSION_FAIL_JIMMY_DEAD] = TRUE )
					IF IS_PED_INJURED(psJimmy.PedIndex)
					OR IS_ENTITY_DEAD(psJimmy.PedIndex)
						eFail 	= MISSION_FAIL_JIMMY_DEAD
						eStage 	= MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] = TRUE )
				OR ( FailFlags[MISSION_FAIL_FAMILY_LEFT_BEHIND] = TRUE )
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psJimmy.PedIndex) > ABANDON_BUDDY_FAIL_RANGE
							IF ( FailFlags[MISSION_FAIL_TRACEY_LEFT_BEHIND] = TRUE )
								IF NOT IS_PED_INJURED(psTracey.PedIndex)
									IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psTracey.PedIndex) > ABANDON_BUDDY_FAIL_RANGE / 2
										eFail 	= MISSION_FAIL_FAMILY_LEFT_BEHIND
										eStage	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
							IF ( FailFlags[MISSION_FAIL_AMANDA_LEFT_BEHIND] = TRUE )
								IF NOT IS_PED_INJURED(psAmanda.PedIndex)
									IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psAmanda.PedIndex) > ABANDON_BUDDY_FAIL_RANGE / 2
										eFail 	= MISSION_FAIL_FAMILY_LEFT_BEHIND
										eStage	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
							IF ( eFail = MISSION_FAIL_EMPTY )
								eFail 	= MISSION_FAIL_JIMMY_LEFT_BEHIND
								eStage 	= MISSION_STAGE_FAILED
							ENDIF
						ENDIF
						IF ( eStage = MISSION_STAGE_DRIVE_TO_BEAN_MACHINE )
							IF DOES_BLIP_EXIST(sLocatesData.BuddyBlipID[0]) OR NOT IS_PED_IN_GROUP(psJimmy.PedIndex)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-641.87726, 247.57806, 80.30839>>, <<-607.46051, 244.61778, 87.00999>>, 22.0)
									eFail 	= MISSION_FAIL_JIMMY_LEFT_BEHIND
									eStage 	= MISSION_STAGE_FAILED
								ENDIF
							ENDIF
						ENDIF
						IF ( eStage = MISSION_STAGE_DRIVE_TO_SHRINK )
							IF DOES_BLIP_EXIST(sLocatesData.BuddyBlipID[1]) OR NOT IS_PED_IN_GROUP(psJimmy.PedIndex)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1894.675781,-567.717773,10.614735>>, <<-1906.339966,-557.923096,13.812391>>, 16.0)
									IF NOT IS_PED_INJURED(psTracey.PedIndex)
										IF DOES_BLIP_EXIST(sLocatesData.BuddyBlipID[0]) OR NOT IS_PED_IN_GROUP(psTracey.PedIndex)
											eFail 	= MISSION_FAIL_FAMILY_LEFT_BEHIND
											eStage	= MISSION_STAGE_FAILED
										ENDIF
									ENDIF
									IF ( eFail = MISSION_FAIL_EMPTY )
										eFail 	= MISSION_FAIL_JIMMY_LEFT_BEHIND
										eStage 	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psTracey.PedIndex)
				IF ( FailFlags[MISSION_FAIL_TRACEY_DEAD] = TRUE )
					IF IS_PED_INJURED(psTracey.PedIndex)
					OR IS_ENTITY_DEAD(psTracey.PedIndex)
						eFail 	= MISSION_FAIL_TRACEY_DEAD
						eStage 	= MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_TRACEY_LEFT_BEHIND] = TRUE )
				OR ( FailFlags[MISSION_FAIL_FAMILY_LEFT_BEHIND] = TRUE )
					IF NOT IS_PED_INJURED(psTracey.PedIndex)
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psTracey.PedIndex) > ABANDON_BUDDY_FAIL_RANGE
							IF ( FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] = TRUE )
								IF NOT IS_PED_INJURED(psJimmy.PedIndex)
									IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psJimmy.PedIndex) > ABANDON_BUDDY_FAIL_RANGE / 2
										eFail 	= MISSION_FAIL_FAMILY_LEFT_BEHIND
										eStage	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
							IF ( FailFlags[MISSION_FAIL_AMANDA_LEFT_BEHIND] = TRUE )
								IF NOT IS_PED_INJURED(psAmanda.PedIndex)
									IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psAmanda.PedIndex) > ABANDON_BUDDY_FAIL_RANGE / 2
										eFail 	= MISSION_FAIL_FAMILY_LEFT_BEHIND
										eStage	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
							IF ( eFail = MISSION_FAIL_EMPTY )
								eFail 	= MISSION_FAIL_TRACEY_LEFT_BEHIND
								eStage	= MISSION_STAGE_FAILED
							ENDIF
						ENDIF
						IF ( eStage = MISSION_STAGE_DRIVE_TO_SHRINK )
							IF DOES_BLIP_EXIST(sLocatesData.BuddyBlipID[0]) OR NOT IS_PED_IN_GROUP(psTracey.PedIndex)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1894.675781,-567.717773,10.614735>>, <<-1906.339966,-557.923096,13.812391>>, 16.0)
									IF NOT IS_PED_INJURED(psJimmy.PedIndex)
										IF DOES_BLIP_EXIST(sLocatesData.BuddyBlipID[1]) OR NOT IS_PED_IN_GROUP(psJimmy.PedIndex)
											eFail 	= MISSION_FAIL_FAMILY_LEFT_BEHIND
											eStage	= MISSION_STAGE_FAILED
										ENDIF
									ENDIF
									IF ( eFail = MISSION_FAIL_EMPTY )
										eFail 	= MISSION_FAIL_TRACEY_LEFT_BEHIND
										eStage 	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psAmanda.PedIndex)
				IF ( FailFlags[MISSION_FAIL_AMANDA_DEAD] = TRUE )
					IF IS_PED_INJURED(psAmanda.PedIndex)
					OR IS_ENTITY_DEAD(psAmanda.PedIndex)
						eFail 	= MISSION_FAIL_AMANDA_DEAD
						eStage 	= MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_AMANDA_LEFT_BEHIND] = TRUE )
					IF NOT IS_PED_INJURED(psAmanda.PedIndex)
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psAmanda.PedIndex) > ABANDON_BUDDY_FAIL_RANGE
							IF ( FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] = TRUE )
								IF NOT IS_PED_INJURED(psJimmy.PedIndex)
									IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psJimmy.PedIndex) > ABANDON_BUDDY_FAIL_RANGE / 2
										eFail 	= MISSION_FAIL_FAMILY_LEFT_BEHIND
										eStage	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
							IF ( FailFlags[MISSION_FAIL_TRACEY_LEFT_BEHIND] = TRUE )
								IF NOT IS_PED_INJURED(psTracey.PedIndex)
									IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psTracey.PedIndex) > ABANDON_BUDDY_FAIL_RANGE / 2
										eFail 	= MISSION_FAIL_FAMILY_LEFT_BEHIND
										eStage	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
							IF ( eFail = MISSION_FAIL_EMPTY )
								eFail 	= MISSION_FAIL_AMANDA_LEFT_BEHIND
								eStage	= MISSION_STAGE_FAILED
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_AMANDA_SCARED_OFF] = TRUE )
					IF NOT IS_PED_INJURED(psAmanda.PedIndex)
						IF HAS_PED_RECEIVED_EVENT(psAmanda.PedIndex, EVENT_EXPLOSION)
						OR HAS_PED_RECEIVED_EVENT(psAmanda.PedIndex, EVENT_SHOCKING_EXPLOSION)
						OR HAS_PED_RECEIVED_EVENT(psAmanda.PedIndex, EVENT_SHOT_FIRED)
						OR HAS_PED_RECEIVED_EVENT(psAmanda.PedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
						OR HAS_PED_RECEIVED_EVENT(psAmanda.PedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psAmanda.PedIndex, PLAYER_PED_ID())
							eFail 	= MISSION_FAIL_AMANDA_SCARED_OFF
							eStage 	= MISSION_STAGE_FAILED
						ENDIF
						IF eStage = MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
							IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
								IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-641.87726, 247.57806, 80.30839>>, <<-607.46051, 244.61778, 87.00999>>, 22.0)
										eFail 	= MISSION_FAIL_AMANDA_SCARED_OFF
										eStage 	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF eStage = MISSION_STAGE_DRIVE_TO_SHRINK
							IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1894.675781,-567.717773,10.614735>>, <<-1906.339966,-557.923096,13.812391>>, 16.0)
									eFail 	= MISSION_FAIL_AMANDA_SCARED_OFF
									eStage 	= MISSION_STAGE_FAILED
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_BEAN_MACHINE_SMASHED] = TRUE )
					IF eStage = MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
						IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) AND NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYERS_LAST_VEHICLE(), <<-615.175964,241.813858,80.011978>>, <<-638.159058,239.557037,85.011978>>, 8.0)
							OR IS_ENTITY_IN_ANGLED_AREA(GET_PLAYERS_LAST_VEHICLE(), <<-615.159668,243.767014,80.011978>>, <<-638.516357,246.040314,84.995193>>, 4.0)
								IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_PLAYERS_LAST_VEHICLE())
									eFail 	= MISSION_FAIL_BEAN_MACHINE_SMASHED
									eStage 	= MISSION_STAGE_FAILED
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psFabien.PedIndex)
				IF ( FailFlags[MISSION_FAIL_FABIEN_DEAD] = TRUE )
					IF IS_PED_INJURED(psFabien.PedIndex)
					OR IS_ENTITY_DEAD(psFabien.PedIndex)
						eFail 	= MISSION_FAIL_FABIEN_DEAD
						eStage 	= MISSION_STAGE_FAILED
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psLazlow.PedIndex)
				IF ( FailFlags[MISSION_FAIL_LAZLOW_DEAD] = TRUE )
					IF IS_PED_INJURED(psLazlow.PedIndex)
					OR IS_ENTITY_DEAD(psLazlow.PedIndex)
						eFail 	= MISSION_FAIL_LAZLOW_DEAD
						eStage 	= MISSION_STAGE_FAILED
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets mission as failed and provides mission flow with reason to print on screen.
/// PARAMS:
///    eFailReason - One of MISSION_FAILS values.
PROC SET_MISSION_FAILED_WITH_REASON(MISSION_FAILS &eFailReason)   

	SWITCH eFailReason
	
		CASE MISSION_FAIL_JIMMY_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_JDIED")
		BREAK
		CASE MISSION_FAIL_TRACEY_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_TCDIED")
		BREAK
		CASE MISSION_FAIL_AMANDA_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_ADIED")
		BREAK
		CASE MISSION_FAIL_FABIEN_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM6_FAILFD")
		BREAK
		CASE MISSION_FAIL_LAZLOW_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM6_FAILLAZ")
		BREAK
		CASE MISSION_FAIL_AMANDA_SCARED_OFF
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM6_FAILBMA")
		BREAK
		CASE MISSION_FAIL_AMANDAS_CAR_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM6_FAILACD")
		BREAK
		CASE MISSION_FAIL_FAMILY_LEFT_BEHIND
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM6_FFAMILY")
		BREAK
		CASE MISSION_FAIL_JIMMY_LEFT_BEHIND
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_JLEFT")
		BREAK
		CASE MISSION_FAIL_TRACEY_LEFT_BEHIND
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_TCLEFT")
		BREAK
		CASE MISSION_FAIL_AMANDA_LEFT_BEHIND
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_ALEFT")
		BREAK
		CASE MISSION_FAIL_BEAN_MACHINE_SMASHED
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM6_FBMSMASH")
		BREAK
		CASE MISSION_FAIL_EMPTY
		CASE MISSION_FAIL_FORCE_FAIL
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM6_FAILDB")
		BREAK

	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Sets mission stage enum for replay.
/// PARAMS:
///    eStage - Mission stage enum variable.
///    iValue - Mid-mission replay value returned by the script when replaying the mission.
PROC SET_MISSION_STAGE_FOR_REPLAY(MISSION_STAGES &eStage, INT iValue)

	IF iValue = ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_BEAN_MACHINE)
	
		eStage = MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_CUTSCENE_BEAN_MACHINE)
	
		eStage = MISSION_STAGE_CUTSCENE_BEAN_MACHINE
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_TRACEY)
	
		eStage = MISSION_STAGE_DRIVE_TO_TRACEY
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_PIERCE_LAZLOW)
	
		eStage = MISSION_STAGE_PIERCE_LAZLOW
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_SHRINK)
	
		eStage = MISSION_STAGE_DRIVE_TO_SHRINK
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_HOME)
	
		eStage = MISSION_STAGE_DRIVE_HOME
		
	ENDIF
	
	bStageReplayInProgress = TRUE
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission is being replayed at stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

ENDPROC

FUNC MISSION_STAGES GET_NEXT_MISSION_STAGE_FOR_SHITSKIP(MISSION_STAGES eStage)
	
	INT 			iNextStage						//get next mission stage in mission flow, if this is not a special case stage
	MISSION_STAGES 	eNextStage

	iNextStage = ENUM_TO_INT(eStage) + 1
	eNextStage = INT_TO_ENUM(MISSION_STAGES, iNextStage)
	
	RETURN eNextStage

ENDFUNC

PROC INITIALISE_ARRAYS_FOR_MISSION_STAGE(MISSION_STAGES eStage)

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Initialising arrays for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
	osLeftDoorDummy.vPosition					= <<-816.7160, 179.0980, 72.8274>>
	osLeftDoorDummy.ModelName					= V_ILEV_MM_DOORM_L
	
	osRightDoorDummy.vPosition					= <<-816.1068, 177.5108, 72.8274>>
	osRightDoorDummy.ModelName					= V_ILEV_MM_DOORM_R

	SWITCH eStage
	
		CASE MISSION_STAGE_PRE_INTRO
		
			MissionPosition.vPosition 			= <<-822.7947, 176.4619, 70.2210>>
			MissionPosition.fHeading  			= 292.6546

		BREAK
	
		CASE MISSION_STAGE_CUTSCENE_INTRO_P1
		CASE MISSION_STAGE_TIMELAPSE
		CASE MISSION_STAGE_CUTSCENE_INTRO_P2		
		CASE MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
		
			MissionPosition.vPosition 			= <<-807.5781, 176.1437, 71.8347>>
			MissionPosition.fHeading  			= 17.2773
			
			psJimmy.vPosition					= <<-808.7352, 178.4024, 71.1531>> 
			psJimmy.fHeading					= 44.4325
			psJimmy.ModelName					= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			vsMichaelsCar.vPosition   			= << -824.3593, 179.5045, 70.4652 >>    
			vsMichaelsCar.fHeading  			= 137.1847
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
	
			vsAmandasCar.vPosition				= <<-622.3307, 253.6852, 80.5665>>
			vsAmandasCar.fHeading				= 263.3081
			vsAmandasCar.ModelName				= GET_NPC_VEH_MODEL(CHAR_AMANDA)
			
			psAmanda.vPosition					= <<-626.1675, 244.5767, 80.8952>>
			psAmanda.fHeading					= 265.6175
			psAmanda.ModelName					= GET_NPC_PED_MODEL(CHAR_AMANDA)
			
			psFabien.vPosition					= <<-623.6904, 244.6078, 80.8952>>
			psFabien.fHeading					= 85.7330
			psFabien.ModelName					= IG_FABIEN
			
			psHipster.vPosition					= <<-622.4567, 243.7266, 80.8952>>
			psHipster.fHeading					= 274.1241
			psHipster.ModelName					= CSB_SCREEN_WRITER
			
			osLaptop.vPosition					= <<-621.67029, 244.90756, 81.69887>>
			osLaptop.vRotation					= << 0.0, 0.0, 0.0 >>
			osLaptop.ModelName					= P_LAPTOP_02_S
			
			osMug.vPosition						= << -625.02, 245.39, 81.69 >>
			osMug.vRotation						= << 0.0, 0.0, 0.0 >>
			osMug.ModelName						= PROP_MUG_04
			
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_BEAN_MACHINE
		
			MissionPosition.vPosition 			= <<-628.1871, 246.2558, 80.6955>> 
			MissionPosition.fHeading  			= 358.2773
			
			psJimmy.vPosition					= <<-629.4871, 248.0318, 80.5708>>
			psJimmy.fHeading					= 1.8819
			psJimmy.ModelName					= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			vsMichaelsCar.vPosition   			= <<-630.5650, 255.2820, 80.5020>> 
			vsMichaelsCar.fHeading  			= 265.1243 
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
	
			vsAmandasCar.vPosition				= <<-622.3307, 253.6852, 80.5665>>
			vsAmandasCar.fHeading				= 263.3081
			vsAmandasCar.ModelName				= GET_NPC_VEH_MODEL(CHAR_AMANDA)
			
			psAmanda.vPosition					= <<-626.1675, 244.5767, 80.8952>>
			psAmanda.fHeading					= 265.6175
			psAmanda.ModelName					= GET_NPC_PED_MODEL(CHAR_AMANDA)
			
			psFabien.vPosition					= <<-623.6904, 244.6078, 80.8952>>
			psFabien.fHeading					= 85.7330
			psFabien.ModelName					= IG_FABIEN
		
		BREAK
		
		CASE MISSION_STAGE_DRIVE_TO_TRACEY
		
			MissionPosition.vPosition 			= <<-628.1871, 246.2558, 80.6955>>
			MissionPosition.fHeading  			= 358.2910
			
			psJimmy.vPosition					= <<-629.4871, 248.0318, 80.5708>>
			psJimmy.fHeading					= 1.8819
			psJimmy.ModelName					= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			vsMichaelsCar.vPosition   			= <<-630.5650, 255.2820, 80.5020>> 
			vsMichaelsCar.fHeading  			= 265.1243 
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			
			psTracey.vPosition					= <<321.6396, 181.2119, 102.5865>>
			psTracey.fHeading					= 196.2891
			psTracey.ModelName					= GET_NPC_PED_MODEL(CHAR_TRACEY)
			
			psLazlow.vPosition					= <<321.0728, 180.1358, 102.5865>>
			psLazlow.fHeading					= 349.9787
			psLazlow.ModelName					= CS_LAZLOW
		
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_SHOP_1
		CASE MISSION_STAGE_PIERCE_LAZLOW
		CASE MISSION_STAGE_CUTSCENE_SHOP_2
		CASE MISSION_STAGE_TATTOO_LAZLOW
		CASE MISSION_STAGE_CUTSCENE_SHOP_3
		CASE MISSION_STAGE_CUT_LAZLOW_HAIR
		CASE MISSION_STAGE_CUTSCENE_SHOP_4
			
			MissionPosition.vPosition 			= <<323.8220, 179.8134, 102.5865>>
			MissionPosition.fHeading  			= 62.0417
			
			vsMichaelsCar.vPosition   			= <<300.6550, 173.2841, 102.9553>>   
			vsMichaelsCar.fHeading  			= 69.9177
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			
			psJimmy.vPosition					= <<321.65167, 182.52638, 102.58653>>
			psJimmy.fHeading					= -115.96
			psJimmy.ModelName					= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			psTracey.vPosition					= <<321.58789, 181.57816, 102.58653>>
			psTracey.fHeading					= -100.54
			psTracey.ModelName					= GET_NPC_PED_MODEL(CHAR_TRACEY)
			
			psLazlow.vPosition					= <<321.8900, 182.1672, 102.5865>>
			psLazlow.fHeading					= 214.4331
			psLazlow.ModelName					= CS_LAZLOW
			
			psTatguy.vPosition					= <<322.07504, 182.24998, 102.58653>>
			psTatguy.fHeading					= -153.83
			psTatguy.ModelName					= U_M_Y_TATTOO_01
			
			osShirt.vPosition					= << 0.0, 0.0, 0.0 >>
			osShirt.vRotation					= << 0.0, 0.0, 0.0 >>
			osShirt.ModelName					= P_LAZLOW_SHIRT_S
			
			osPonyTail.vPosition				= << 322.68, 179.47, 102.59 >>
			osPonyTail.vRotation				= << 0.0, 0.0, 0.0 >>
			osPonyTail.ModelName				= P_CS_LAZ_PTAIL_S
			
			osPiercingGun.vPosition				= << 322.28, 183.48, 102.59 >>
			osPiercingGun.vRotation				= << 0.0, 0.0, 0.0 >>
			osPiercingGun.ModelName				= PROP_PIERCING_GUN
			
			osTattooNeedle.vPosition			= << 322.28, 183.48, 102.59 >>
			osTattooNeedle.vRotation			= << 0.0, 0.0, 0.0 >>
			osTattooNeedle.ModelName			= V_ILEV_TA_TATGUN
			
			osScissors.vPosition				= << 322.28, 183.48, 102.59 >>
			osScissors.vRotation				= << 0.0, 0.0, 0.0 >>
			osScissors.ModelName				= PROP_CS_SCISSORS
			
		BREAK
		
		CASE MISSION_STAGE_DRIVE_TO_SHRINK
		
			MissionPosition.vPosition 			= <<315.4439, 175.3920, 102.8468>>//<<315.4439, 175.3920, 102.8468>>
			MissionPosition.fHeading  			= 106.3951//92.4830
		
			vsMichaelsCar.vPosition   			= <<300.6550, 173.2841, 102.9553>>   
			vsMichaelsCar.fHeading  			= 69.9177
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			
			psJimmy.vPosition					= <<313.6881, 174.4399, 102.8722>> 
			psJimmy.fHeading					= 101.0260
			psJimmy.ModelName					= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			psTracey.vPosition					= <<313.3786, 176.4375, 102.8908>> 
			psTracey.fHeading					= 95.2572
			psTracey.ModelName					= GET_NPC_PED_MODEL(CHAR_TRACEY)
			
			psLazlow.vPosition					= <<321.8900, 182.1672, 102.5865>>
			psLazlow.fHeading					= 214.4331
			psLazlow.ModelName					= CS_LAZLOW
			
			psTatguy.vPosition					= <<322.07504, 182.24998, 102.58653>>
			psTatguy.fHeading					= -153.83
			psTatguy.ModelName					= U_M_Y_TATTOO_01
			
			psAmanda.vPosition					= <<-1906.76477, -559.85950, 10.79799>>
			psAmanda.fHeading					= -88.88
			psAmanda.ModelName					= GET_NPC_PED_MODEL(CHAR_AMANDA)
		
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_SHRINK
		CASE MISSION_STAGE_DRIVE_HOME
			
			MissionPosition.vPosition 			= <<-1902.8654, -558.0767, 10.7794>>
			MissionPosition.fHeading  			= 225.8389
			
			vsMichaelsCar.vPosition   			= <<-1898.9677, -561.0737, 10.7827>>  
			vsMichaelsCar.fHeading  			= 321.4044
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			
			psJimmy.vPosition					= <<-1904.7207, -560.4189, 10.7980>>
			psJimmy.fHeading					= 238.9373
			psJimmy.ModelName					= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			psTracey.vPosition					= <<-1903.3005, -560.9316, 10.7980>>
			psTracey.fHeading					= 224.1973
			psTracey.ModelName					= GET_NPC_PED_MODEL(CHAR_TRACEY)
			
			psAmanda.vPosition					= <<-1899.4695, -557.8484, 10.7553>>
			psAmanda.fHeading					= 227.6959
			psAmanda.ModelName					= GET_NPC_PED_MODEL(CHAR_AMANDA)
		
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_END
		
			MissionPosition.vPosition 			= <<-823.1286, 176.7446, 70.2141>> 
			MissionPosition.fHeading  			= 292.7178
			
			vsMichaelsCar.vPosition   			= <<-825.6646, 180.1217, 70.4785>> 
			vsMichaelsCar.fHeading  			= 316.5125 
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			
			psJimmy.vPosition					= <<-822.4412, 175.9556, 70.3352>>
			psJimmy.fHeading					= 293.9573
			psJimmy.ModelName					= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			psTracey.vPosition					= <<-821.4911, 175.1127, 70.5573>>
			psTracey.fHeading					= 284.8115
			psTracey.ModelName					= GET_NPC_PED_MODEL(CHAR_TRACEY)
			
			psAmanda.vPosition					= <<-820.2170, 175.4369, 70.6240>>
			psAmanda.fHeading					= 284.8128
			psAmanda.ModelName					= GET_NPC_PED_MODEL(CHAR_AMANDA)
			
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initialising arrays for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_LOADED(MISSION_STAGES eStage, INT &iSetupProgress, BOOL &bStageLoaded, BOOL &bStageSkippedTo, BOOL &bStageReplayed)

	//handle initial mission setup
	IF ( iSetupProgress = 0 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting initial mission loading.")
		#ENDIF
		
		//clear the screen of any text displayed before mission started
		CLEAR_PRINTS()

		//load the mission text
		REQUEST_ADDITIONAL_TEXT("FAM6", MISSION_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
		
		//suppress player's car model
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_NPC_VEH_MODEL(CHAR_AMANDA), TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
		
		//DISABLE_TAXI_HAILING(TRUE)
		
		IF IS_SCREEN_FADED_OUT()
			DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
			DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
			DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
		ENDIF
		
		LOCK_SHOP_DOORS(TATTOO_PARLOUR_01_HW, TRUE)
		SET_SHOP_LOCATES_ARE_BLOCKED(TATTOO_PARLOUR_01_HW, TRUE)
		SET_SHOP_DIALOGUE_IS_BLOCKED(TATTOO_PARLOUR_01_HW, TRUE)
		SET_SHOP_IS_OPEN_FOR_BUSINESS(TATTOO_PARLOUR_01_HW, FALSE)
		SET_SHOP_HAS_RUN_ENTRY_INTRO(TATTOO_PARLOUR_01_HW, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(TATTOO_PARLOUR_01_HW, TRUE)
		FORCE_SHOP_CLEANUP(TATTOO_PARLOUR_01_HW)
		SET_INTERIOR_DISABLED(INTERIOR_V_PSYCHEOFFICE, FALSE)
		
		//bean machine
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -614.28 - 10.0, 252.81 - 4.0, 81.66 - 2.0 >>, << -614.28 + 10.0, 252.81 + 4.0, 81.66 + 2.0 >>, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< -614.28 - 10.0, 252.81 - 4.0, 81.66 - 2.0 >>, << -614.28 + 10.0, 252.81 + 4.0, 81.66 + 2.0 >>)
		
		//tattoo parlor
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 295.26 - 8.0, 175.57 - 4.0, 104.06 - 2.0>>, << 295.26 + 8.0, 175.57 + 4.0, 104.06 + 2.0>>, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< 295.26 - 8.0, 175.57 - 4.0, 104.06 - 2.0>>, << 295.26 + 8.0, 175.57 + 4.0, 104.06 + 2.0>>)
		
		//shrink office
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1894.98 - 4.0, -565.30 - 4.0, 12.68 - 2.0 >>, << -1894.98 + 4.0, -565.30 + 4.0, 12.68 + 2.0 >>, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< -1894.98 - 4.0, -565.30 - 4.0, 12.68 - 2.0 >>, << -1894.98 + 4.0, -565.30 + 4.0, 12.68 + 2.0 >>)
		
		BeanMachineScenarioBlockingIndex 	= CREATE_SCENARIO_BLOCKING_AREA(<< -627.05, 243.58, 82.89 >>, << 16.0, 12.0, 4.0 >>)
		TattooParlorScenarioBlockingIndex 	= CREATE_SCENARIO_BLOCKING_AREA(<< 295.59, 174.36, 104.05 >>, << 6.0, 3.0, 2.0 >>)

		iTattooSoundID = GET_SOUND_ID()
		
		IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())
			SET_GROUP_FORMATION_SPACING(PLAYER_GROUP_ID(), 3.5)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_GROUP_FORMATION_SPACING() for PLAYER_GROUP_ID().")
			#ENDIF
		ENDIF
		
		IF IS_REPLAY_IN_PROGRESS()
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		ENDIF
		
		IF NOT IS_REPLAY_IN_PROGRESS()
			g_replay.iReplayInt[0] = ENUM_TO_INT(TATTOOING_IDLE)	//reset the global variable when not a retry, used for Lazlow's tattoo info
		ELSE
																	//do nothing, read this global variable later when creating Lazlow's tattoo
		ENDIF

		CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
		CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())

		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initial mission loading.")
		#ENDIF
				
		iSetupProgress++
	
	ENDIF
	
	//run this each time new mission stage needs to be loaded
	IF ( iSetupProgress = 1 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Started mission stage loading for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded = FALSE
		
		//initialise arrays for peds, vehicles and objects
		INITIALISE_ARRAYS_FOR_MISSION_STAGE(eStage)
		
		//cleanup the asset arrays so that they are ready to be populated with assets needed for the stage being loaded
		CLEANUP_MODEL_ARRAY(MissionModels, iModelsRequested)
		CLEANUP_ANIMATION_ARRAY(MissionAnimations, iAnimationsRequested)
		
		//clear triggered text labels
		CLEAR_TRIGGERED_LABELS()

		iSetupProgress++
	
	ENDIF
	
	//handle player positioning when current stage is being skipped to or played normally
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 2 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to new position for mission stage being loaded due to skipping or replay.")
			#ENDIF
		
			WARP_PED(PLAYER_PED_ID(), MissionPosition.vPosition, MissionPosition.fHeading, FALSE, TRUE, FALSE)
						
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_TATTOOS(PLAYER_PED_ID())
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
				
				STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				REMOVE_DECALS_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
			ENDIF
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF

			bVehicleReversed = FALSE

		ENDIF
		
		IF ( bStageReplayed = TRUE )
			START_REPLAY_SETUP( MissionPosition.vPosition, MissionPosition.fHeading)
		ENDIF
		
		//request assets needed for current stage
		BUILD_REQUEST_BANK_FOR_MISSION_STAGE(eStage)
		
		iSetupProgress++
	
	ENDIF
	
	//handle loading of mission models that have been requested in the previous stage in BUILD_REQUEST_BANK_FOR_MISSION_STAGE()
	//handle loading of mission animations that been requested in the previous stage in BUILD_REQUEST_BANK_FOR_MISSION_STAGE()
	IF ( iSetupProgress = 3 )
	
		IF ARE_REQUESTED_MODELS_LOADED(MissionModels, iModelsRequested)
			IF ARE_REQUESTED_ANIMATIONS_LOADED(MissionAnimations, iAnimationsRequested)
				iSetupProgress++
			ENDIF
		ENDIF
	
	ENDIF
	
	//handle creating vehicles/peds and setting up fail flags
	//run this each time new mission stage needs to be loaded
	//THIS SETUP SECTION ASSUMES THAT ALL REQUIRED ASSETS HAVE BEEN ALREADY LOADED TO MEMORY IN PREVIOUS SETUP SECTIONS
	IF ( iSetupProgress = 4 )
	
		SET_FAIL_FLAGS(FALSE)
	
		SWITCH eStage
		
			CASE MISSION_STAGE_PRE_INTRO
			CASE MISSION_STAGE_CUTSCENE_INTRO_P1
			CASE MISSION_STAGE_TIMELAPSE
			CASE MISSION_STAGE_CUTSCENE_INTRO_P2
			
				IF IS_REPEAT_PLAY_ACTIVE()			//create mission vehicle on mission repeat play only
					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
						OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.VehicleIndex)
						iSetupProgress++
					ENDIF
				ELSE								//move on if not repeat play
					iSetupProgress++
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
			
				FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
				FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE
				
				FailFlags[MISSION_FAIL_AMANDA_DEAD]			= TRUE
				FailFlags[MISSION_FAIL_FABIEN_DEAD]			= TRUE
				FailFlags[MISSION_FAIL_AMANDA_SCARED_OFF]	= TRUE
				FailFlags[MISSION_FAIL_BEAN_MACHINE_SMASHED]= TRUE
			
				IF HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				
					IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
						
							REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
							
							IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

								vsMichaelsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.vPosition, vsMichaelsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
								
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RC_Michaels_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
								#ENDIF
								
							ENDIF
													
						ELSE
						
							IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
							
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ELSE
						
						iSetupProgress++
						
					ENDIF
				
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BEAN_MACHINE
			
				IF 	HAS_MISSION_VEHICLE_BEEN_CREATED(vsAmandasCar, FALSE, FALSE, CHAR_AMANDA)
				AND	HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				AND	HAS_MISSION_PED_BEEN_CREATED(psAmanda, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_AMANDA)
				AND	HAS_MISSION_PED_BEEN_CREATED(psFabien, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
				
					IF IS_VEHICLE_DRIVEABLE(vsAmandasCar.VehicleIndex)
						LOWER_CONVERTIBLE_ROOF(vsAmandasCar.VehicleIndex, TRUE)
					ENDIF
				
					IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
						
							REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
							
							IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

								vsMichaelsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.vPosition, vsMichaelsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
								
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RC_Michaels_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
								#ENDIF
								
							ENDIF
													
						ELSE
						
							IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
							
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ELSE
						
						iSetupProgress++
						
					ENDIF
				
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_DRIVE_TO_TRACEY
			
				FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
				FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE
				
				FailFlags[MISSION_FAIL_AMANDA_DEAD] 		= TRUE
				FailFlags[MISSION_FAIL_FABIEN_DEAD]		 	= TRUE
				FailFlags[MISSION_FAIL_TRACEY_DEAD] 		= TRUE
				FailFlags[MISSION_FAIL_LAZLOW_DEAD] 		= TRUE
	
				IF HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
					
					IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
						
							REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
							
							IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

								vsMichaelsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.vPosition, vsMichaelsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
								
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RC_Michaels_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
								#ENDIF
								
							ENDIF
													
						ELSE
						
							IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
							
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ELSE
						
						iSetupProgress++
						
					ENDIF
					
				ENDIF

			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_SHOP_1
			CASE MISSION_STAGE_PIERCE_LAZLOW
			CASE MISSION_STAGE_CUTSCENE_SHOP_2
			CASE MISSION_STAGE_TATTOO_LAZLOW
			CASE MISSION_STAGE_CUTSCENE_SHOP_3
			CASE MISSION_STAGE_CUT_LAZLOW_HAIR
			CASE MISSION_STAGE_CUTSCENE_SHOP_4
			
				FailFlags[MISSION_FAIL_JIMMY_DEAD] 	= TRUE
				FailFlags[MISSION_FAIL_TRACEY_DEAD] = TRUE
				FailFlags[MISSION_FAIL_LAZLOW_DEAD]	= TRUE
			
				IF 	HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				AND HAS_MISSION_PED_BEEN_CREATED(psTracey, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_TRACEY)
				AND HAS_MISSION_PED_BEEN_CREATED(psLazlow, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
					
					IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
						
							REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
							
							IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

								vsMichaelsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.vPosition, vsMichaelsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
								
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RC_Michaels_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
								#ENDIF
								
							ENDIF
													
						ELSE
						
							IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
							
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ELSE
						
						iSetupProgress++
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			
			CASE MISSION_STAGE_DRIVE_TO_SHRINK
			
				FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
				FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE
				
				FailFlags[MISSION_FAIL_TRACEY_DEAD] 		= TRUE
				FailFlags[MISSION_FAIL_TRACEY_LEFT_BEHIND] 	= TRUE
				
				FailFlags[MISSION_FAIL_FAMILY_LEFT_BEHIND]	= TRUE
				
				FailFlags[MISSION_FAIL_LAZLOW_DEAD]			= TRUE
				FailFlags[MISSION_FAIL_AMANDA_DEAD]			= TRUE
				
				FailFlags[MISSION_FAIL_AMANDA_SCARED_OFF]	= TRUE
				
				IF 	HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				AND HAS_MISSION_PED_BEEN_CREATED(psTracey, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_TRACEY)
				AND HAS_MISSION_PED_BEEN_CREATED(psLazlow, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
				AND HAS_MISSION_PED_BEEN_CREATED(psTatguy, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
					
					IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
						
							REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
							
							IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

								vsMichaelsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.vPosition, vsMichaelsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
								
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RC_Michaels_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
								#ENDIF
								
							ENDIF
													
						ELSE
						
							IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
							
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ELSE
						
						iSetupProgress++
						
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_SHRINK
				
				IF	HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				AND	HAS_MISSION_PED_BEEN_CREATED(psTracey, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_TRACEY)
				AND	HAS_MISSION_PED_BEEN_CREATED(psAmanda, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_AMANDA)
				
					IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
						
							REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
							
							IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

								vsMichaelsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.vPosition, vsMichaelsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
								
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RC_Michaels_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
								#ENDIF
								
							ENDIF
													
						ELSE
						
							IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
							
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ELSE
						
						iSetupProgress++
						
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_DRIVE_HOME
			
				FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
				FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE
				
				FailFlags[MISSION_FAIL_TRACEY_DEAD] 		= TRUE
				FailFlags[MISSION_FAIL_TRACEY_LEFT_BEHIND] 	= TRUE
				
				FailFlags[MISSION_FAIL_AMANDA_DEAD]			= TRUE
				FailFlags[MISSION_FAIL_AMANDA_LEFT_BEHIND]	= TRUE
				
				FailFlags[MISSION_FAIL_FAMILY_LEFT_BEHIND]	= TRUE
				
				IF	HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				AND	HAS_MISSION_PED_BEEN_CREATED(psTracey, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_TRACEY)
				AND	HAS_MISSION_PED_BEEN_CREATED(psAmanda, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_AMANDA)

					IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
						
							REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
							
							IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

								vsMichaelsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.vPosition, vsMichaelsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
								
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RC_Michaels_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
								#ENDIF
								
							ENDIF
													
						ELSE
						
							IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
							
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ELSE
						
						iSetupProgress++
						
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_END
				
				IF	HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				AND	HAS_MISSION_PED_BEEN_CREATED(psTracey, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_TRACEY)
				AND	HAS_MISSION_PED_BEEN_CREATED(psAmanda, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_AMANDA)

					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
					
						iSetupProgress++
						
					ENDIF
				
				ENDIF
			
			BREAK
			
			DEFAULT
				iSetupProgress++	
			BREAK
		
		ENDSWITCH

		SET_JIMMY_PED_COMPONENT_VARIATIONS(psJimmy.bComponentVariationsSet)
		SET_TRACEY_PED_COMPONENT_VARIATIONS(psTracey.bComponentVariationsSet)
		SET_AMANDA_PED_COMPONENT_VARIATIONS(psAmanda.bComponentVariationsSet)
		SET_FABIEN_PED_COMPONENT_VARIATIONS(psFabien.bComponentVariationsSet)
		SET_TATGUY_PED_COMPONENT_VARIATIONS(psTatguy.bComponentVariationsSet)
		
		SWITCH eMissionStage
			CASE MISSION_STAGE_CUTSCENE_SHOP_1
			CASE MISSION_STAGE_PIERCE_LAZLOW
				SET_LAZLOW_PED_COMPONENT_VARIATIONS(psLazlow.bComponentVariationsSet, FALSE, FALSE, FALSE, FALSE, FALSE)
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SHOP_2
			CASE MISSION_STAGE_TATTOO_LAZLOW
				SET_LAZLOW_PED_COMPONENT_VARIATIONS(psLazlow.bComponentVariationsSet, TRUE, TRUE, FALSE, FALSE, FALSE)
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SHOP_3
			CASE MISSION_STAGE_CUT_LAZLOW_HAIR
				SET_LAZLOW_PED_COMPONENT_VARIATIONS(psLazlow.bComponentVariationsSet, TRUE, TRUE, FALSE, TRUE, bChestTattoo)
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SHOP_4
			BREAK
			CASE MISSION_STAGE_DRIVE_TO_SHRINK
			
				//fix for B*1918478, read Lazlow's tattoo info from a global int when retrying this stage
				IF ( bStageReplayed = TRUE )
					IF ( g_replay.iReplayInt[0] = ENUM_TO_INT(TATTOOING_CHEST) )
						bChestTattoo = TRUE
					ELIF ( g_replay.iReplayInt[0] = ENUM_TO_INT(TATTOOING_BACK) )
						bChestTattoo = FALSE
					ENDIF
				ENDIF
			
				SET_LAZLOW_PED_COMPONENT_VARIATIONS(psLazlow.bComponentVariationsSet, TRUE, TRUE, TRUE, TRUE, bChestTattoo)
			BREAK
		ENDSWITCH
		
	ENDIF

	IF ( iSetupProgress = 5 )
		
		DISABLE_CELLPHONE(FALSE)
		
		REMOVE_PED_FOR_DIALOGUE(FAM6Conversation, 0)	//Michael
		REMOVE_PED_FOR_DIALOGUE(FAM6Conversation, 1)	//Jimmy
		REMOVE_PED_FOR_DIALOGUE(FAM6Conversation, 2)	//Tracey/Lazlow
		REMOVE_PED_FOR_DIALOGUE(FAM6Conversation, 4)	//Amanda
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(FAM6Conversation, 0, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		
		SWITCH eStage
		
			CASE MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
			CASE MISSION_STAGE_DRIVE_TO_TRACEY
			CASE MISSION_STAGE_DRIVE_TO_SHRINK
			CASE MISSION_STAGE_DRIVE_HOME
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					ADD_PED_FOR_DIALOGUE(FAM6Conversation, 1, psJimmy.PedIndex, "JIMMY")
				ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					ADD_PED_FOR_DIALOGUE(FAM6Conversation, 2, psTracey.PedIndex, "TRACEY")
				ENDIF
				
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					ADD_PED_FOR_DIALOGUE(FAM6Conversation, 4, psAmanda.PedIndex, "AMANDA")
				ENDIF
			
				//add peds here
			
			BREAK
			
			CASE MISSION_STAGE_PIERCE_LAZLOW
			CASE MISSION_STAGE_TATTOO_LAZLOW
			CASE MISSION_STAGE_CUT_LAZLOW_HAIR
			
				DISABLE_CELLPHONE(TRUE)
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					ADD_PED_FOR_DIALOGUE(FAM6Conversation, 2, psLazlow.PedIndex, "LAZLOW")
				ENDIF
				
			BREAK
		
			DEFAULT
			
			BREAK
		
		ENDSWITCH
		
		SWITCH eStage
			CASE MISSION_STAGE_PRE_INTRO
			CASE MISSION_STAGE_CUTSCENE_INTRO_P1
			CASE MISSION_STAGE_TIMELAPSE
			CASE MISSION_STAGE_CUTSCENE_INTRO_P2
			CASE MISSION_STAGE_CUTSCENE_BEAN_MACHINE
			CASE MISSION_STAGE_CUTSCENE_SHOP_1
			CASE MISSION_STAGE_PIERCE_LAZLOW
			CASE MISSION_STAGE_CUTSCENE_SHOP_2
			CASE MISSION_STAGE_TATTOO_LAZLOW
			CASE MISSION_STAGE_CUTSCENE_SHOP_3
			CASE MISSION_STAGE_CUT_LAZLOW_HAIR
			CASE MISSION_STAGE_CUTSCENE_SHOP_4
			CASE MISSION_STAGE_CUTSCENE_SHRINK
			CASE MISSION_STAGE_CUTSCENE_END
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
			BREAK
			
			DEFAULT
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
			BREAK
			
		ENDSWITCH
		
		SWITCH eStage
			CASE MISSION_STAGE_CUTSCENE_SHOP_1
			CASE MISSION_STAGE_PIERCE_LAZLOW
			CASE MISSION_STAGE_CUTSCENE_SHOP_2
			CASE MISSION_STAGE_TATTOO_LAZLOW
			CASE MISSION_STAGE_CUTSCENE_SHOP_3
			CASE MISSION_STAGE_CUT_LAZLOW_HAIR
			CASE MISSION_STAGE_CUTSCENE_SHOP_4
				SET_MAX_WANTED_LEVEL(0)
				SET_CREATE_RANDOM_COPS(FALSE)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			BREAK
			DEFAULT
				SET_MAX_WANTED_LEVEL(5)
				SET_CREATE_RANDOM_COPS(TRUE)
				SET_WANTED_LEVEL_MULTIPLIER(1.0)
			BREAK
		ENDSWITCH
		
		DISABLE_TAXI_HAILING(FALSE)			//enable taxi hailing for every stage by default
		
		SWITCH eStage
			CASE MISSION_STAGE_DRIVE_HOME
				DISABLE_TAXI_HAILING(TRUE)	//disable taxi hailing for drive home stage, see B*1715392
			BREAK
		ENDSWITCH
		
		iSetupProgress++
		
	ENDIF

	IF ( iSetupProgress = 6 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
		
			IF ( bStageReplayed = TRUE )
			
				END_REPLAY_SETUP(NULL, DEFAULT, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling END_REPLAY_SETUP().")
				#ENDIF
		
			ELSE
			
				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20.0)
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START_SPHERE() at coordinates ", MissionPosition.vPosition, " for mission replay or stage skip. This might take a while to load.")
				#ENDIF
				
				INT iLoadSceneTimer = GET_GAME_TIMER() + 15000

				WHILE IS_NEW_LOAD_SCENE_ACTIVE()
				AND NOT IS_NEW_LOAD_SCENE_LOADED()
				AND GET_GAME_TIMER() < iLoadSceneTimer
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Load scene timer: ", iLoadSceneTimer - GET_GAME_TIMER(), ".")
					#ENDIF

					WAIT(0)
					  
				ENDWHILE

				NEW_LOAD_SCENE_STOP()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP().")
				#ENDIF
			
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)

			//handle fade in for each mission stage
			//some stages start with or require a fade out
			SWITCH eStage
			
				CASE MISSION_STAGE_PRE_INTRO
				CASE MISSION_STAGE_CUTSCENE_INTRO_P1
				CASE MISSION_STAGE_CUTSCENE_INTRO_P2
				CASE MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
				CASE MISSION_STAGE_CUTSCENE_BEAN_MACHINE
				CASE MISSION_STAGE_DRIVE_TO_TRACEY
				CASE MISSION_STAGE_CUTSCENE_SHOP_1
				CASE MISSION_STAGE_PIERCE_LAZLOW
				CASE MISSION_STAGE_CUTSCENE_SHOP_2
				CASE MISSION_STAGE_TATTOO_LAZLOW
				CASE MISSION_STAGE_CUTSCENE_SHOP_3
				CASE MISSION_STAGE_CUT_LAZLOW_HAIR
				CASE MISSION_STAGE_CUTSCENE_SHOP_4
				CASE MISSION_STAGE_DRIVE_TO_SHRINK
				CASE MISSION_STAGE_CUTSCENE_SHRINK
				CASE MISSION_STAGE_CUTSCENE_END
				CASE MISSION_STAGE_DRIVE_HOME
				
					//do not fade in, mission stage will handle a fade in from fade out
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Halting loading screen fade in for skip to a mission stage that will fade the screen in.")
					#ENDIF
					
				BREAK
			
				DEFAULT
					
					//fade in
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling a loading screen fade in.")
					#ENDIF
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				
				BREAK

			ENDSWITCH

		ENDIF
		
		iSetupProgress++
	
	ENDIF

	IF ( iSetupProgress = 7 )
	
		SWITCH eStage
		
			CASE MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_BEAN_MACHINE), "STAGE 0: DRIVE TO BEAN", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_BEAN_MACHINE), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BEAN_MACHINE
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_CUTSCENE_BEAN_MACHINE), "STAGE 1: BEAN CUTSCENE", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_CUTSCENE_BEAN_MACHINE), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_DRIVE_TO_TRACEY
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_TRACEY), "STAGE 2: DRIVE TO TRACEY", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_TRACEY), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_PIERCE_LAZLOW
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_PIERCE_LAZLOW), "STAGE 3: DADDY ISSUES", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_PIERCE_LAZLOW), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_DRIVE_TO_SHRINK
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_SHRINK), "STAGE 4: DRIVE TO SHRINK", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_SHRINK), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
		
			
			CASE MISSION_STAGE_DRIVE_HOME
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_HOME), "STAGE 5: SHRINK CUTSCENE", TRUE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_HOME), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
		ENDSWITCH
	
		iSetupProgress++
	
	ENDIF
	
	IF ( iSetupProgress = 8 )
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission stage loading for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded 	= TRUE
		bStageReplayed 	= FALSE
		bStageSkippedTo = FALSE
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		iSetupProgress++
		
	ENDIF
	
	IF ( iSetupProgress = 9 )

		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE	

ENDFUNC

PROC MISSION_CLEANUP()

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting mission cleanup.")
		DELETE_DEBUG_WIDGETS()
	#ENDIF
	
	IF ( bReplayStartEventTriggered = TRUE )
		REPLAY_STOP_EVENT()
	ENDIF

	CLEANUP_PED(psJimmy, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psTracey, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psAmanda, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psFabien, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psLazlow, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psHipster, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psTatguy, IS_SCREEN_FADED_OUT(), FALSE)
	
	IF DOES_ENTITY_EXIST(osLeftDoorDummy.ObjectIndex)
		REMOVE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName)
	ENDIF

	IF DOES_ENTITY_EXIST(osRightDoorDummy.ObjectIndex)
		REMOVE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName)
	ENDIF
	
	CLEANUP_MISSION_OBJECT(osMug, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osShirt, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osLaptop, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osScissors, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osPonyTail, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osPiercingGun, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osTattooNeedle, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osLeftDoorDummy, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osRightDoorDummy, IS_SCREEN_FADED_OUT())
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHair)
		STOP_PARTICLE_FX_LOOPED(ptfxHair)
	ENDIF
	
	IF NOT HAS_SOUND_FINISHED(iTattooSoundID)
		STOP_SOUND(iTattooSoundID)
	ENDIF
	
	STOP_AUDIO_SCENES()
	REMOVE_PTFX_ASSET()
	RELEASE_SCRIPT_AUDIO_BANK()
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
	
	IF IS_PC_VERSION()
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov_kb)
	ENDIF
	
	KillAllConversations()
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_NPC_VEH_MODEL(CHAR_AMANDA), FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), FALSE)
	
	IF DOES_CAM_EXIST(AnimatedCamera)
		DESTROY_CAM(AnimatedCamera)
	ENDIF

	IF DOES_CAM_EXIST(ScriptedCamera)
		DESTROY_CAM(ScriptedCamera)
	ENDIF

	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)

	DISABLE_CELLPHONE(FALSE)
	DISABLE_TAXI_HAILING(FALSE)
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	
	LOCK_SHOP_DOORS(TATTOO_PARLOUR_01_HW, FALSE)
	SET_SHOP_LOCATES_ARE_BLOCKED(TATTOO_PARLOUR_01_HW, FALSE)
	SET_SHOP_DIALOGUE_IS_BLOCKED(TATTOO_PARLOUR_01_HW, FALSE)
	SET_SHOP_IS_OPEN_FOR_BUSINESS(TATTOO_PARLOUR_01_HW, TRUE)
	SET_SHOP_HAS_RUN_ENTRY_INTRO(TATTOO_PARLOUR_01_HW, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(TATTOO_PARLOUR_01_HW, FALSE)
	FORCE_SHOP_CLEANUP(TATTOO_PARLOUR_01_HW)
	
	SET_INTERIOR_DISABLED(INTERIOR_V_PSYCHEOFFICE, TRUE)

	SET_MAX_WANTED_LEVEL(5)
	SET_CREATE_RANDOM_COPS(TRUE)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)

	CLEAR_WEATHER_TYPE_PERSIST()

	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedZone)
	REMOVE_SCENARIO_BLOCKING_AREA(BeanMachineScenarioBlockingIndex)
	REMOVE_SCENARIO_BLOCKING_AREA(TattooParlorScenarioBlockingIndex)
	
	CLEANUP_PC_CONTROLS()
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission cleanup.")
	#ENDIF

ENDPROC

//|=============================== MISSION STAGES FUNCTIONS ==============================|

FUNC BOOL IS_MISSION_STAGE_PRE_INTRO_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
				IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
			
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
						iStageProgress++
						
					ENDIF
					
				ELSE
				
					iStageProgress++
			
				ENDIF
			ENDIF

		BREAK
		
		CASE 1
		
			REQUEST_ANIM_DICT("missfam2mcs_intp1")
		
			REQUEST_MODEL(osLeftDoorDummy.ModelName)
			REQUEST_MODEL(osRightDoorDummy.ModelName)
			
			IF 	HAS_ANIM_DICT_LOADED("missfam2mcs_intp1")
			AND	HAS_MODEL_LOADED(osLeftDoorDummy.ModelName)
			AND	HAS_MODEL_LOADED(osRightDoorDummy.ModelName)
		
				iStageProgress++
				
			ENDIF
		
		BREAK                  

		CASE 2
		
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iHouseEnterSceneID)
			
				STOP_FIRE_IN_RANGE(<< -817.305, 179.330, 71.241 >>, 20.0)
				REMOVE_DECALS_IN_RANGE(<< -817.305, 179.330, 71.241 >>, 20.0)
				CLEAR_AREA_OF_PROJECTILES(<< -817.305, 179.330, 71.241 >>, 20.0)
				REMOVE_PARTICLE_FX_IN_RANGE(<< -817.305, 179.330, 71.241 >>, 20.0)
				
				iHouseEnterSceneID = CREATE_SYNCHRONIZED_SCENE(<< -817.305, 179.330, 71.225 >>, << 0.0, 0.0, -113.0 >>)
			
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iHouseEnterSceneID, TRUE)
			
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iHouseEnterSceneID, "missfam2mcs_intp1", "fam_2_int_p1_michael",
										INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), -1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				CREATE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName, FALSE)
            	CREATE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName, FALSE)
				
				osLeftDoorDummy.ObjectIndex 	= CREATE_OBJECT(osLeftDoorDummy.ModelName, <<-816.72, 179.10, 72.83>>)
                osRightDoorDummy.ObjectIndex 	= CREATE_OBJECT(osRightDoorDummy.ModelName, <<-816.11, 177.51, 72.83>>)
				
				PLAY_SYNCHRONIZED_ENTITY_ANIM(osLeftDoorDummy.ObjectIndex, iHouseEnterSceneID, "fam_2_int_p1_doorl", "missfam2mcs_intp1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
                PLAY_SYNCHRONIZED_ENTITY_ANIM(osRightDoorDummy.ObjectIndex, iHouseEnterSceneID, "fam_2_int_p1_doorr", "missfam2mcs_intp1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				
                FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(osLeftDoorDummy.ObjectIndex)
                FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(osRightDoorDummy.ObjectIndex)
				
				DESTROY_ALL_CAMS()
					
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)

				PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iHouseEnterSceneID, "fam5_intro_cam", "missfam2mcs_intp1")
				
				CLEAR_HELP(TRUE)
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)			
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(osLeftDoorDummy.ModelName)
				SET_MODEL_AS_NO_LONGER_NEEDED(osRightDoorDummy.ModelName)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 3
		
			IF DOES_CAM_EXIST(ScriptedCamera)
				
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
				
					IF 	DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
					AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE(), TRUE) AND NOT IS_ENTITY_ON_FIRE(GET_PLAYERS_LAST_VEHICLE())
					
						IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
							SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting last player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " as mission entity.")
						#ENDIF

						DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MISSION_VEH)
						SET_VEHICLE_DOORS_SHUT(GET_PLAYERS_LAST_VEHICLE())
						SET_VEHICLE_ENGINE_ON(GET_PLAYERS_LAST_VEHICLE(), FALSE, TRUE)
						SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), << -825.8718, 157.3143, 69.4619 >>)
						SET_ENTITY_HEADING(GET_PLAYERS_LAST_VEHICLE(), 90.0)
						SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
						
					ENDIF
				ENDIF
				
				CLEAR_AREA(<< -817.305, 179.330, 71.241 >>, 500.0, TRUE)
				CLEAR_AREA_OF_PEDS(<< -817.305, 179.330, 71.241 >>, 500.0)
				CLEAR_AREA_OF_COPS(<< -817.305, 179.330, 71.241 >>, 500.0)
				CLEAR_AREA_OF_VEHICLES(<< -817.305, 179.330, 71.241 >>, 500.0)
				
				iStageProgress++
			
			ENDIF
		
		BREAK
		
		CASE 4
		
			REQUEST_CUTSCENE("fam_6_int")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
				bCutsceneAssetsRequested = TRUE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for intro cutscene peds during stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting bCutsceneAssetsRequested to ", bCutsceneAssetsRequested, ".")
				#ENDIF
			ENDIF
		
			IF DOES_CAM_EXIST(ScriptedCamera)
				
				IF ( bHouseEntryCutsceneSkipped = FALSE )
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iHouseEnterSceneID)
				
						IF GET_SYNCHRONIZED_SCENE_PHASE(iHouseEnterSceneID) >= 0.99
						
							IF ( bCutsceneAssetsRequested = TRUE )
					
								iStageProgress++
								
							ENDIF
							
						ENDIF
						
						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
							
							bHouseEntryCutsceneSkipped = TRUE
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Pre intro cutscene skipped by the player.")
							#ENDIF
							
						ENDIF
					
					ENDIF

				ELSE
				
					IF IS_SCREEN_FADED_OUT()
					
						IF ( bCutsceneAssetsRequested = TRUE )
					
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
							ENDIF

							STOP_SYNCHRONIZED_ENTITY_ANIM(osLeftDoorDummy.ObjectIndex, 0, FALSE)
							STOP_SYNCHRONIZED_ENTITY_ANIM(osRightDoorDummy.ObjectIndex, 0, FALSE)
							
							IF DOES_ENTITY_EXIST(osLeftDoorDummy.ObjectIndex)
								DELETE_OBJECT(osLeftDoorDummy.ObjectIndex)
								REMOVE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName)
							ENDIF				
							
							IF DOES_ENTITY_EXIST(osRightDoorDummy.ObjectIndex)
								DELETE_OBJECT(osRightDoorDummy.ObjectIndex)
								REMOVE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName)
							ENDIF
							
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
							ENDIF
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
							ENDIF
							
							REMOVE_ANIM_DICT("missfam2mcs_intp1")
							
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							DESTROY_CAM(ScriptedCamera)
							DESTROY_ALL_CAMS()
							
							iStageProgress++
					
						ENDIF
					
					ENDIF

				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 5
		
			IF ( bHouseEntryCutsceneSkipped = FALSE )
		
				IF DOES_CAM_EXIST(ScriptedCamera)

					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
					ENDIF

					RETURN TRUE
					
				ENDIF
				
			ELSE		
			
				RETURN TRUE
			
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_INTRO_P1_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_int")
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				START_CUTSCENE()
				
				//REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				IF DOES_CAM_EXIST(ScriptedCamera)
					DESTROY_CAM(ScriptedCamera)
				ENDIF
				
				DESTROY_ALL_CAMS()
								
				IF 	IS_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_SAVEHOUSE)
				AND	DOES_ENTITY_EXIST(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE))
				AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE), TRUE)
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Vehicle gen vehicle VEHGEN_MICHAEL_SAVEHOUSE is available for mission use.")
					#ENDIF

					IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE))
					
						SET_ENTITY_AS_MISSION_ENTITY(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE), TRUE, TRUE)
						
						vsMichaelsCar.VehicleIndex = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE)
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting vehicle gen vehicle VEHGEN_MICHAEL_SAVEHOUSE as mission entity.")
						#ENDIF
						
					ENDIF
					
					IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 2.5)
						SET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex, GET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						SET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						SET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						
						SET_VEHICLE_AS_MISSION_CRITICAL(vsMichaelsCar, TRUE)
						SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, FALSE, TRUE)
						SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vsMichaelsCar.vPosition)
						SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, vsMichaelsCar.fHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)

					ENDIF
				
				ENDIF
				
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay start vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_START_VEHICLE_MODEL()), " is available.")
					#ENDIF

					IF 	DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
					AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE(), TRUE) AND NOT IS_ENTITY_ON_FIRE(GET_PLAYERS_LAST_VEHICLE())
					
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Last player vehicle is model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " which is allowed for mission.")
						#ENDIF
						
						IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
							
							IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
								SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
							ENDIF
						
							vsMichaelsCar.VehicleIndex = GET_PLAYERS_LAST_VEHICLE()
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting last player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " as mission entity.")
							#ENDIF
						
							SET_VEHICLE_AS_MISSION_CRITICAL(vsMichaelsCar, TRUE)
							SET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex, GET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
							SET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
							SET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
							STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 2.5)
							
							SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
							SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, FALSE, TRUE)
							SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vsMichaelsCar.vPosition)
							SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, vsMichaelsCar.fHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
						
						ELSE
						
							DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MISSION_VEH)
							SET_VEHICLE_DOORS_SHUT(GET_PLAYERS_LAST_VEHICLE(), TRUE)
							SET_VEHICLE_ENGINE_ON(GET_PLAYERS_LAST_VEHICLE(), FALSE, TRUE)
							SET_MISSION_VEHICLE_GEN_VEHICLE(GET_PLAYERS_LAST_VEHICLE(), << -825.8718, 157.3143, 69.4619 >>, 90.0)
							CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting last player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " as mission vehicle gen vehicle.")
							#ENDIF
						
						ENDIF
						
					ENDIF
				
				ENDIF
				
				IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.VehicleIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Overriding replay checkpoint vehicle with vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vsMichaelsCar.VehicleIndex)), ".")
					#ENDIF
				ENDIF
				
				DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
				//DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
				//DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
			
				CLEAR_AREA(MissionPosition.vPosition, 500.0, TRUE)
				CLEAR_AREA_OF_PEDS(MissionPosition.vPosition, 500.0)
				CLEAR_AREA_OF_COPS(MissionPosition.vPosition, 500.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vPosition, 500.0)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)	//Don't disable multihead fade after intro_P1
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
								
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				SET_CAM_PARAMS(ScriptedCamera, <<-776.580688,181.103302,72.205902>>,<<11.813498,0.000000,95.727486>>,46.725498)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			//	REPLAY_STOP_EVENT()
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
		
				RETURN TRUE
							
			ELSE
				
				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_TIMELAPSE_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0
		
			IF ( bTimelapseCutsceneSkipped = FALSE )

		        IF REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				
					STRUCTTIMELAPSE sLocalTimelapse
				
					sTimelapse 					= sLocalTimelapse						//reset timelapse struct
		            sTimelapse.splineCamera 	= CREATE_CAM("DEFAULT_SPLINE_CAMERA")	//create timelapse camera
					
					ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<-776.5807, 181.1033, 72.2059>>,<<11.8135, -0.0000, 95.7275>>, 5500)
		            ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<-776.8288, 181.0926, 72.2551>>,<<11.8134, -0.0000, 95.7284>>, 5500)
		            SET_CAM_FOV(sTimelapse.splineCamera, 46.7255)
		            SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
					
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
					
		            iStageProgress++
					
				ENDIF
			
			ELSE
			
				IF IS_SCREEN_FADED_OUT()
			
					iStageProgress++
					
				ENDIF
			
			ENDIF

		BREAK
		
		CASE 1
		
			INT iCurrentHour, iStartTime, iEndTime
		
			iCurrentHour = GET_CLOCK_HOURS()
					
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current time hour is ", iCurrentHour, ".")
			#ENDIF
	
			iTODAdditionalHours = 0
	
			GET_SP_MISSION_TOD_WINDOW_TIME(SP_MISSION_FAMILY_6, iStartTime, iEndTime)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": iStarttime is  ", iStartTime, " and iEndTime is ", iEndTime, ".")
			#ENDIF
			
			IF iCurrentHour < iEndTime AND iCurrentHour >= iEndTime - 2
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current time hour needs additional hours added for the skip.")
				#ENDIF
				iTODAdditionalHours = 24
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current time hour does not need additional hours added for the skip.")
				#ENDIF
				iTODAdditionalHours = 0
			ENDIF
			
			WARP_PED(PLAYER_PED_ID(), MissionPosition.vPosition, MissionPosition.fHeading, FALSE, FALSE, FALSE)
			
			bTimelapseTriggered = TRUE
			
			iStageProgress++
		
		BREAK
		
		CASE 2
		
			REQUEST_CUTSCENE("fam_6_int_p3_t7")
				
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
				SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
				bCutsceneAssetsRequested = TRUE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for intro cutscene peds during stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting bCutsceneAssetsRequested to ", bCutsceneAssetsRequested, ".")
				#ENDIF
			ENDIF
			
			IF ( bTimelapseCutsceneSkipped = FALSE )
		
				IF DOES_CAM_EXIST(sTimelapse.splineCamera)
				
					IF DOES_CAM_EXIST(ScriptedCamera)
						DESTROY_CAM(ScriptedCamera)
					ENDIF
					
				ENDIF

	            IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(IDEAL_MISSION_START_TIME, 0, "EXTRASUNNY", "cirrocumulus", sTimelapse, DEFAULT, iTODAdditionalHours)
				
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					IF ( bCutsceneAssetsRequested = TRUE )
			
	               		iStageProgress++
						
					ENDIF
					
	            ENDIF
				
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					
					bTimelapseCutsceneSkipped = TRUE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Timelapse cutscene skipped by the player.")
					#ENDIF
					
				ENDIF
				
			ELSE
			
				IF IS_SCREEN_FADED_OUT()
				
					IF NOT HAS_SOUND_FINISHED(sTimelapse.iSplineStageSound)
						STOP_SOUND(sTimelapse.iSplineStageSound)
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
					ENDIF
					
					IF ( bCutsceneAssetsRequested = TRUE )
			
						iStageProgress++
						
					ENDIF
					
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 3
		
			RETURN TRUE
			
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_INTRO_P2_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_int_p3_t7")
			
				REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, psJimmy.ModelName)				
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				IF ( bTimelapseTriggered = TRUE )
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
					SET_CLOCK_TIME(IDEAL_MISSION_START_TIME, 0, 0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting clock time after time lapse cutscene.")
					#ENDIF
				ENDIF
				
				SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
				
				IF NOT HAS_SOUND_FINISHED(sTimelapse.iSplineStageSound)
					STOP_SOUND(sTimelapse.iSplineStageSound)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				ENDIF
				
				IF DOES_CAM_EXIST(ScriptedCamera)
					DESTROY_CAM(ScriptedCamera)
				ENDIF
				
				DESTROY_ALL_CAMS()
				
				IF DOES_ENTITY_EXIST(osLeftDoorDummy.ObjectIndex)
					DELETE_OBJECT(osLeftDoorDummy.ObjectIndex)
					REMOVE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName)
				ENDIF				
				
				IF DOES_ENTITY_EXIST(osRightDoorDummy.ObjectIndex)
					DELETE_OBJECT(osRightDoorDummy.ObjectIndex)
					REMOVE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				
				REMOVE_ANIM_DICT("missfam2mcs_intp1")				//remove anim dictionary from previous scripted cutscene
				
				REQUEST_MODEL(osLaptop.ModelName)
				REQUEST_MODEL(psAmanda.ModelName)
				REQUEST_MODEL(psFabien.ModelName)
				REQUEST_MODEL(psHipster.ModelName)
				REQUEST_MODEL(vsAmandasCar.ModelName)
				REQUEST_ANIM_DICT("missfam6leadinoutfam_6_mcs_1")	//request assets for Bean Machine scene after the intro cutscene
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
								
				iStageProgress++
				
			ENDIF
		
		BREAK

		CASE 2
		
			//create Jimmy's ped during the intro mocap cutscene
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Jimmy")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy"))
					
						psJimmy.PedIndex =  GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy"))
						
						SET_MISSION_PED_PROPERTIES(psJimmy.PedIndex, RELGROUPHASH_PLAYER, FALSE, TRUE, FALSE, FALSE)
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Set up properites for mission player ped Jimmy created in the intro cutscene.")
						#ENDIF

					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(17.2773 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF ( bCutsceneSkipped = FALSE )						
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -809.27, 179.55, 71.15 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -811.32, 180.11, 71.15 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.17, 179.48, 71.16 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.92, 179.22, 71.16 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					ENDIF
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
				#ENDIF
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)				
					IF ( bCutsceneSkipped = FALSE )
						TASK_FOLLOW_NAV_MESH_TO_COORD(psJimmy.PedIndex, << -819.95709, 177.42998, 70.60026 >>, PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					ENDIF
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 0, 0)
				ENDIF			
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				IF ( bCutsceneSkipped = TRUE )
				
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						WARP_PED(psJimmy.PedIndex, psJimmy.vPosition, psJimmy.fHeading, FALSE, FALSE, FALSE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(psJimmy.PedIndex, << -819.95709, 177.42998, 70.60026 >>, PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						WARP_PED(PLAYER_PED_ID(), MissionPosition.vPosition, MissionPosition.fHeading, FALSE, TRUE, FALSE)
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -809.27, 179.55, 71.15 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -811.32, 180.11, 71.15 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.17, 179.48, 71.16 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.92, 179.22, 71.16 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					WAIT(250)
				
				ENDIF
					
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				RETURN TRUE
				
			ELSE
				
				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_DURING_GO_TO_BEAN_MACHINE()

	IF  NOT IS_PED_INJURED(psJimmy.PedIndex)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())

		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TOGO")
			IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TOGO", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM6_TOGO", TRUE)
						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
		ELSE
		
			IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_BEAN")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_BEAN2")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psJimmy.PedIndex)
						IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", PICK_STRING(bStageReplayInProgress, "FAM6_BEAN2", "FAM6_BEAN"), CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED(PICK_STRING(bStageReplayInProgress, "FAM6_BEAN2", "FAM6_BEAN"), TRUE)
							bRunConversationPausingCheck = TRUE 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//if main conversation was interrupted, resume it from the next label
			IF ( bCurrentConversationInterrupted = TRUE )
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JBAD")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JWARN")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")

						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_BEAN")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_BEAN2")
						
							//play conversation for Jimmy saying cops were lost and it's safe to go to shrink
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOST_3")
									IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")	//this will only play if conversation to lose cops was triggered
										IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOST")
											IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JLOST", CONV_PRIORITY_MEDIUM)
												iCopsLostConversationCount++
												SET_LABEL_AS_TRIGGERED("FAM6_JLOST", TRUE)
												IF ( iCopsLostConversationCount >= 3)
													SET_LABEL_AS_TRIGGERED("FAM6_JLOST_3", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							//resume the interrupted coversation
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF 	NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
								AND NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationLabel)
									IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_BEAN")		//resume only the main conversations
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_BEAN2")
									
										
										IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(FAM6Conversation, "FAM6AUD", CurrentConversationRoot, CurrentConversationLabel, CONV_PRIORITY_MEDIUM)
											bCurrentConversationInterrupted = FALSE
											SET_LABEL_AS_TRIGGERED("FAM6_JBAD", FALSE)	//mark all the interrupt conversations as not triggered
											SET_LABEL_AS_TRIGGERED("FAM6_JLOST", FALSE)
											SET_LABEL_AS_TRIGGERED("FAM6_JWANT", FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
	
				//resume paused conversations when player gets back in the car
				//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						//PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					//ENDIF
				//ENDIF
			
			ELSE //locates header blip does not exist
				
				//play interrupt conversation when player becomes wanted
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
					
					IF ( bCurrentConversationInterrupted = FALSE )
					
						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_BEAN")	
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_BEAN2")
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
								CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								
								IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
									IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_BEAN")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_BEAN2")
										bCurrentConversationInterrupted = TRUE
										CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
										CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF

					ENDIF
					
					IF ( bCurrentConversationInterrupted = TRUE )
						IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JWANT", CONV_PRIORITY_HIGH)
									iPlayerOffencesCount++
									iLoseCopsReminderTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 12500)
									SET_LABEL_AS_TRIGGERED("FAM6_JWANT", TRUE)
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOSE_5")
						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")
							IF GET_GAME_TIMER() - iLoseCopsReminderTimer > 0
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JLOSE", CONV_PRIORITY_MEDIUM)
										iLoseCopsReminderCount++
										iLoseCopsReminderTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12500, 15000)
										IF ( iLoseCopsReminderCount >= 5 )
											SET_LABEL_AS_TRIGGERED("FAM6_JLOSE_5", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//keep reseting those flags during wanted level as the player might be driving recklessly
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
					
				ELSE
				
					//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//	IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					//		PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					//	ENDIF
					//ENDIF
					
				ENDIF

			ENDIF
			
			//play interrupt conversations for player offences
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF ( GET_GAME_TIMER() - iPlayerOffenceTimer > 5000 )
					
					//play conversation for player running over peds
					IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JBAD_5")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JWARN_6")

						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF	HAS_PLAYER_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
							AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
								bPlayerHitPed = TRUE
							ENDIF
						ENDIF
						
						IF IS_PED_SHOOTING(PLAYER_PED_ID())	//add more conditions here if needed
							bPlayerActingBad = TRUE
						ENDIF
						
						IF ( bPlayerHitPed = TRUE OR bPlayerActingBad = TRUE )
						
							IF HAS_LABEL_BEEN_TRIGGERED("FAM6_BEAN")
							OR HAS_LABEL_BEEN_TRIGGERED("FAM6_BEAN2")
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
									CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
									
									IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
										IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_BEAN")
										OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_BEAN2")
											CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
											CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
											bCurrentConversationInterrupted = TRUE
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ENDIF
									ENDIF
								
								ENDIF
							ENDIF							

							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JBAD", CONV_PRIORITY_MEDIUM)
								iPlayerOffencesCount++
								iBadConversationCount++
								bPlayerHitPed		= FALSE
								bPlayerActingBad 	= FALSE
								iPlayerOffenceTimer = GET_GAME_TIMER()
								SET_LABEL_AS_TRIGGERED("FAM6_JBAD", TRUE)
								IF ( iBadConversationCount >= 5 )
									SET_LABEL_AS_TRIGGERED("FAM6_JBAD_5", TRUE)
								ENDIF
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
								#ENDIF
							ENDIF

						ENDIF
					ENDIF
				
				ELSE
				
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
				
				ENDIF
			ENDIF
			
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_STEAL")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF IS_PED_JACKING(PLAYER_PED_ID())
						IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(psJimmy.PedIndex, PLAYER_PED_ID())
						OR HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(psJimmy.PedIndex, PLAYER_PED_ID())
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_STEAL", CONV_PRIORITY_MEDIUM)
								//
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_MCS1LI")
				IF 	NOT IS_PED_INJURED(psAmanda.PedIndex)
				AND NOT IS_PED_INJURED(psFabien.PedIndex)
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF ( bLeadInTriggered = TRUE )
							IF IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_amanda")
							OR IS_ENTITY_PLAYING_ANIM(psFabien.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_fabien")
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_MCS1LI", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("FAM6_MCS1LI", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_MOM")
				IF ( bDestinationReached = TRUE AND bLeadInTriggered = FALSE )
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psJimmy.PedIndex) < 10.0
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_MOM", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("FAM6_MOM", TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_COME")
					IF ( bDestinationReached = TRUE AND bLeadInTriggered = FALSE )
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psJimmy.PedIndex) < 10.0
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_COME", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("FAM6_COME", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_MOM2")
						IF ( bDestinationReached = TRUE AND bLeadInTriggered = FALSE )
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psJimmy.PedIndex) < 10.0
									IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_MOM2", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("FAM6_MOM2", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
			ENDIF

		ENDIF
		
		IF ( bRunConversationPausingCheck = TRUE )
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
				IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
						ENDIF
					ENDIF
				ELSE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF				
			ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
			AND NOT IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex) 		
				IF 	GET_DISTANCE_BETWEEN_ENTITIES(psJimmy.PedIndex, PLAYER_PED_ID()) < 10 		
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
						ENDIF
					ENDIF
				ELSE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF

ENDPROC

PROC MANAGE_BEAN_MACHINE_SCENE(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBeanMachineScenePosition) < 225.0
			
				iProgress++
				
			ENDIF
				
		BREAK
	
		CASE 1	//create dealer ped and door object to animate

			REQUEST_MODEL(osLaptop.ModelName)
			REQUEST_MODEL(psAmanda.ModelName)
			REQUEST_MODEL(psFabien.ModelName)
			REQUEST_MODEL(psHipster.ModelName)
			REQUEST_MODEL(vsAmandasCar.ModelName)
			REQUEST_ANIM_DICT("missfam6leadinoutfam_6_mcs_1")
			
			IF 	HAS_MODEL_LOADED(osLaptop.ModelName)
			AND	HAS_MODEL_LOADED(psAmanda.ModelName)
			AND	HAS_MODEL_LOADED(psFabien.ModelName)
			AND	HAS_MODEL_LOADED(psHipster.ModelName)
			AND	HAS_MODEL_LOADED(vsAmandasCar.ModelName)
			AND	HAS_ANIM_DICT_LOADED("missfam6leadinoutfam_6_mcs_1")
			
				//CLEAR_AREA()

				IF 	HAS_MISSION_VEHICLE_BEEN_CREATED(vsAmandasCar, FALSE, FALSE, CHAR_AMANDA)
				AND	HAS_MISSION_PED_BEEN_CREATED(psAmanda, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_AMANDA)
				AND HAS_MISSION_PED_BEEN_CREATED(psFabien, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
				AND HAS_MISSION_PED_BEEN_CREATED(psHipster, FALSE, RELGROUPHASH_NO_RELATIONSHIP, FALSE, NO_CHARACTER)
				AND HAS_MISSION_OBJECT_BEEN_CREATED(osLaptop) AND HAS_MISSION_OBJECT_BEEN_CREATED(osMug)
				
					IF IS_VEHICLE_DRIVEABLE(vsAmandasCar.VehicleIndex)
						SET_VEHICLE_AS_MISSION_CRITICAL(vsAmandasCar, TRUE)
						LOWER_CONVERTIBLE_ROOF(vsAmandasCar.VehicleIndex, TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vsAmandasCar.VehicleIndex, FALSE)
					ENDIF
					
					SET_FABIEN_PED_COMPONENT_VARIATIONS(psFabien.bComponentVariationsSet)
					ADD_PED_FOR_DIALOGUE(FAM6Conversation, 4, psFabien.PedIndex, "FABIAN")
					
					SET_AMANDA_PED_COMPONENT_VARIATIONS(psAmanda.bComponentVariationsSet)
					ADD_PED_FOR_DIALOGUE(FAM6Conversation, 5, psAmanda.PedIndex, "AMANDA")

					IF NOT IS_PED_INJURED(psAmanda.PedIndex)
						TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_loop_a_amanda",
												vBeanMachineScenePosition, vBeanMachineSceneRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
						SET_ENTITY_LOAD_COLLISION_FLAG(psAmanda.PedIndex, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psAmanda.PedIndex, TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(psFabien.PedIndex)
						TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_loop_b_fabien",
												vBeanMachineScenePosition, vBeanMachineSceneRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
						SET_ENTITY_LOAD_COLLISION_FLAG(psFabien.PedIndex, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psFabien.PedIndex, TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(psHipster.PedIndex)
						TASK_PLAY_ANIM_ADVANCED(psHipster.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_loop_c_laptop_girl",
												vBeanMachineScenePosition, vBeanMachineSceneRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psHipster.PedIndex)
						SET_ENTITY_LOAD_COLLISION_FLAG(psHipster.PedIndex, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psHipster.PedIndex, TRUE)
					ENDIF
				
					IF DOES_ENTITY_EXIST(osLaptop.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osLaptop.ObjectIndex)
							IF NOT IS_PED_INJURED(psHipster.PedIndex)
								ATTACH_ENTITY_TO_ENTITY(osLaptop.ObjectIndex, psHipster.PedIndex, GET_PED_BONE_INDEX(psHipster.PedIndex,BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
							ENDIF
						ENDIF
					ENDIF
				
					IF DOES_ENTITY_EXIST(osMug.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osMug.ObjectIndex)
							IF NOT IS_PED_INJURED(psFabien.PedIndex)
								ATTACH_ENTITY_TO_ENTITY(osMug.ObjectIndex, psFabien.PedIndex, GET_PED_BONE_INDEX(psFabien.PedIndex, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
							ENDIF
						ENDIF
					ENDIF
				
					SET_MODEL_AS_NO_LONGER_NEEDED(osLaptop.ModelName)
					SET_MODEL_AS_NO_LONGER_NEEDED(psAmanda.ModelName)
					SET_MODEL_AS_NO_LONGER_NEEDED(psFabien.ModelName)
					SET_MODEL_AS_NO_LONGER_NEEDED(psHipster.ModelName)
					SET_MODEL_AS_NO_LONGER_NEEDED(vsAmandasCar.ModelName)
					
					iProgress++
		
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 2
			
			IF ( bLeadInTriggered = TRUE )
			
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_amanda",
											vBeanMachineScenePosition, vBeanMachineSceneRotation, NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
											-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psAmanda.PedIndex, TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_fabien",
											vBeanMachineScenePosition, vBeanMachineSceneRotation, NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
											-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psFabien.PedIndex, TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(psHipster.PedIndex)
					TASK_PLAY_ANIM_ADVANCED(psHipster.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_laptop_girl",
											vBeanMachineScenePosition, vBeanMachineSceneRotation, NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
											-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(psHipster.PedIndex)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psHipster.PedIndex, TRUE)
				ENDIF

				iProgress++
			
			ENDIF
		
		BREAK
		
		CASE 3
		
			IF NOT IS_PED_INJURED(psAmanda.PedIndex)
				IF IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_amanda")
					SET_ENTITY_ANIM_SPEED(psAmanda.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_amanda", 0.9)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(psFabien.PedIndex)
				IF IS_ENTITY_PLAYING_ANIM(psFabien.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_fabien")
					SET_ENTITY_ANIM_SPEED(psFabien.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_fabien", 0.9)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(psHipster.PedIndex)
				IF IS_ENTITY_PLAYING_ANIM(psHipster.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_laptop_girl")
					SET_ENTITY_ANIM_SPEED(psHipster.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_laptop_girl", 0.9)
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC BOOL IS_MISSION_STAGE_DRIVE_TO_BEAN_MACHINE_COMPLETED(INT &iStageProgress)

	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
	
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	MANAGE_CONVERSATIONS_DURING_GO_TO_BEAN_MACHINE()
	MANAGE_BEAN_MACHINE_SCENE(iBeanMachineSceneProgress)
	
	IF NOT IS_PED_INJURED(psJimmy.PedIndex)
		SET_PED_RESET_FLAG(psJimmy.PedIndex,PRF_SearchForClosestDoor, TRUE)
	ENDIF
	
	IF 	DOES_ENTITY_EXIST(sLocatesData.vehStartCar)
	AND NOT IS_ENTITY_DEAD(sLocatesData.vehStartCar)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sLocatesData.vehStartCar, TRUE)
		IF NOT IS_PED_IN_VEHICLE(psJimmy.PedIndex, sLocatesData.vehStartCar, TRUE)
			DISABLE_LOCATES_CLEAR_BUDDY_TASKS_IF_ENTERING_NEAREST_VEHICLE(sLocatesData, TRUE)
		ENDIF
	ELSE
		DISABLE_LOCATES_CLEAR_BUDDY_TASKS_IF_ENTERING_NEAREST_VEHICLE(sLocatesData, FALSE)
	ENDIF
	
	//handle requestting mocap cutscene 
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_PLAYER_CHANGING_CLOTHES()
          
		IF HAS_THIS_CUTSCENE_LOADED("fam_6_mcs_1")
		OR HAS_CUTSCENE_LOADED()
		OR IS_CUTSCENE_ACTIVE()
			REMOVE_CUTSCENE()
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
			#ENDIF
		ENDIF
		                        
	ELSE
	
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBeanMachinePosition) < DEFAULT_CUTSCENE_LOAD_DIST
			
			REQUEST_CUTSCENE("fam_6_mcs_1")

			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())	//Michael's variations
				SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
				SET_AMANDA_CUTSCENE_COMPONENT_VARIATIONS()
				SET_FABIEN_CUTSCENE_COMPONENT_VARIATIONS()
			ENDIF
			
		ELSE
		
			IF HAS_THIS_CUTSCENE_LOADED("fam_6_mcs_1")
			OR HAS_CUTSCENE_LOADED()
			OR IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
				#ENDIF
			ENDIF
		
		ENDIF

	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK	
		IF ( bPlayerPedScriptTaskInterrupted = FALSE )
			IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
				OR (ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) * 255.0) < -20 OR ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) * 255.0) > 20)
				OR (ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) * 255.0) < -20 OR ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) * 255.0) > 20)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					bPlayerPedScriptTaskInterrupted = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SWITCH iStageProgress
		
		CASE 0
		
			IF NOT IS_PED_INJURED(psJimmy.PedIndex)	
				IF ( GET_SCRIPT_TASK_STATUS(psJimmy.PedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK )
					TASK_FOLLOW_NAV_MESH_TO_COORD(psJimmy.PedIndex, << -819.95709, 177.42998, 70.60026 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
					FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
				IF ( GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK )
					SEQUENCE_INDEX SequenceIndex
					CLEAR_SEQUENCE_TASK(SequenceIndex)
					OPEN_SEQUENCE_TASK(SequenceIndex)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -809.27, 179.55, 71.15 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -811.32, 180.11, 71.15 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.17, 179.48, 71.16 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.92, 179.22, 71.16 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
					CLOSE_SEQUENCE_TASK(SequenceIndex)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
					CLEAR_SEQUENCE_TASK(SequenceIndex)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
				ENDIF
			ENDIF
						
			IF IS_SCREEN_FADED_OUT()
				WAIT(2)
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		
			IF HAS_LABEL_BEEN_TRIGGERED("FAM6_TOGO")
			
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
				SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(sLocatesData, vsMichaelsCar.VehicleIndex)
			
				START_AUDIO_SCENE("FAMILY_6_DRIVE_TO_AMANDA")
			
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 1
		
			IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(sLocatesData, vBeanMachinePosition, DEFAULT_LOCATES_SIZE_VECTOR(), TRUE, psJimmy.PedIndex, "FAM6_YOGA", "CMN_JLEAVE", FALSE, TRUE)
			
			//do the check to progress the stage here
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psJimmy.PedIndex)
				
					IF 	IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-630.973633,255.312454,82.295090>>, <<6.000000,4.000000,2.000000>>)
					AND IS_ENTITY_AT_COORD(psJimmy.PedIndex, <<-630.973633,255.312454,82.295090>>, <<6.000000,4.000000,2.000000>>)
					
						IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()	//kill conversations and let last line play out
							
							bDestinationReached = TRUE
							
							REPLAY_RECORD_BACK_FOR_TIME(6.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
							
							iStageProgress++
							
						ENDIF
						
					ENDIF
					
				ELSE
				
					IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), 	<<-641.87726, 247.57806, 80.30839>>, <<-607.46051, 244.61778, 87.00999>>, 22.0) 	
					AND IS_ENTITY_IN_ANGLED_AREA(psJimmy.PedIndex, 	<<-641.87726, 247.57806, 80.30839>>, <<-607.46051, 244.61778, 87.00999>>, 22.0)	
					
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							
								KILL_ANY_CONVERSATION()
								KILL_FACE_TO_FACE_CONVERSATION()	//kill conversations and let last line play out
								
								bDestinationReached = TRUE
							
								iStageProgress++
								
							ENDIF
						ELSE
						
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()	//kill conversations and let last line play out
							
							bDestinationReached = TRUE
						
							iStageProgress++
							
						ENDIF
							
					ENDIF
				ENDIF

					
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 5.0)
					
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()		//check if scripted conversations are not ongoing using native command
					OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//check if any conversations are not ongoing using script command

						iStageProgress++
						
					ENDIF

				ENDIF
			ELSE

				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()		//check if scripted conversations are not ongoing using native command
				OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//check if any conversations are not ongoing using script command

					iStageProgress++
					
				ENDIF					
			ENDIF
		
		BREAK
		
		CASE 3
		
			IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(sLocatesData, <<-628.04919, 242.85347, 80.89529>>, DEFAULT_LOCATES_SIZE_VECTOR(), TRUE, psJimmy.PedIndex, "", "", FALSE, TRUE)
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-641.87726, 247.57806, 80.30839>>, <<-607.46051, 244.61778, 87.00999>>, 22.0)
			
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
				
				IF ( bLookAtAmandaActive = FALSE )
					IF NOT IS_PED_INJURED(psAmanda.PedIndex)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), psAmanda.PedIndex, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
						bLookAtAmandaActive = TRUE
					ENDIF
				ENDIF
				
			ELSE
			
				IF ( bLookAtAmandaActive = TRUE )
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					bLookAtAmandaActive = FALSE
				ENDIF
			
			ENDIF
			

			IF 	NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_PED_GROUP_MEMBER(psJimmy.PedIndex, PLAYER_GROUP_ID())
				IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), 	<<-641.87726, 247.57806, 80.30839>>, <<-607.46051, 244.61778, 87.00999>>, 22.0) 	
				//AND IS_ENTITY_IN_ANGLED_AREA(psJimmy.PedIndex, 	<<-641.87726, 247.57806, 80.30839>>, <<-607.46051, 244.61778, 87.00999>>, 22.0)	
				
					bLeadInTriggered = TRUE
					
				ENDIF
			ENDIF
			
			IF ( bLeadInTriggered = TRUE )
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-629.12653, 242.94969, 80.89502>>, <<-621.01813, 242.78120, 83>>, 3.5)
				OR IS_ENTITY_IN_ANGLED_AREA(psJimmy.PedIndex, <<-629.12653, 242.94969, 80.89502>>, <<-621.01813, 242.78120, 83>>, 3.5)		
				OR ( IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_amanda") AND GET_ENTITY_ANIM_CURRENT_TIME(psAmanda.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_amanda") >= 0.99 )
				OR ( IS_ENTITY_PLAYING_ANIM(psFabien.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_fabien") AND GET_ENTITY_ANIM_CURRENT_TIME(psFabien.PedIndex, "missfam6leadinoutfam_6_mcs_1", "leadin_action_fabien") >= 0.99 )
					
					KILL_ANY_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION()
				
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
					RETURN TRUE
				
				ENDIF
				
			ENDIF
		
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_BEAN_MACHINE_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_mcs_1")
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psJimmy.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psAmanda.PedIndex, "Amanda", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psAmanda.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psFabien.PedIndex, "Fabien", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psFabien.ModelName)
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(vsAmandasCar.VehicleIndex)	
					SET_VEHICLE_LOD_MULTIPLIER(vsAmandasCar.VehicleIndex, 2.0)
					REGISTER_ENTITY_FOR_CUTSCENE(vsAmandasCar.VehicleIndex, "Amandas_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(psHipster.PedIndex) 
					REGISTER_ENTITY_FOR_CUTSCENE(psHipster.PedIndex, "Laptop_Girl", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF DOES_ENTITY_EXIST(osLaptop.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osLaptop.ObjectIndex)
						IF IS_ENTITY_ATTACHED(osLaptop.ObjectIndex)	
							DETACH_ENTITY(osLaptop.ObjectIndex, FALSE, TRUE)
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(osLaptop.ObjectIndex, "Fam_Laptop", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
				ENDIF
								
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				
				
				STOP_AUDIO_SCENES()
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE

				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_AMANDA_CUTSCENE_COMPONENT_VARIATIONS()
					SET_FABIEN_CUTSCENE_COMPONENT_VARIATIONS()
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()

				VEHICLE_INDEX LastPlayerVehicleIndex
				
				LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
				
				IF DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
					IF NOT IS_ENTITY_DEAD(LastPlayerVehicleIndex)
					
						IF IS_VEHICLE_AVAILABLE_TO_SNAPSHOT(PLAYER_PED_ID(), LastPlayerVehicleIndex, TRUE)
	
							IF NOT IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
								
								SET_ENTITY_AS_MISSION_ENTITY(LastPlayerVehicleIndex)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
						
								vsMichaelsCar.VehicleIndex = LastPlayerVehicleIndex
							ENDIF
							
							STOP_VEHICLE_FIRE(LastPlayerVehicleIndex)
							
							SET_VEHICLE_DOORS_SHUT(LastPlayerVehicleIndex, TRUE)
							SET_VEHICLE_ENGINE_ON(LastPlayerVehicleIndex, FALSE, TRUE)
							SET_ENTITY_COORDS(LastPlayerVehicleIndex, vsMichaelsCar.vPosition)
							SET_ENTITY_HEADING(LastPlayerVehicleIndex, vsMichaelsCar.fHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(LastPlayerVehicleIndex)
							
								
							SET_VEHICLE_LOD_MULTIPLIER(LastPlayerVehicleIndex, 2.0)
							
							OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(LastPlayerVehicleIndex)
						
						ENDIF
					ENDIF
					
				ENDIF
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				CLEANUP_MISSION_OBJECT(osMug, TRUE)
				REMOVE_ANIM_DICT("missfam6leadinoutfam_6_mcs_1")
				
				CLEAR_AREA_OF_PEDS(MissionPosition.vPosition, 10.0)
				CLEAR_AREA_OF_OBJECTS(MissionPosition.vPosition, 10.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vPosition, 10.0)
				CLEAR_AREA_OF_PROJECTILES(MissionPosition.vPosition, 10.0)
				REMOVE_PARTICLE_FX_IN_RANGE(MissionPosition.vPosition, 10.0)
				
				iSpeedZone = ADD_ROAD_NODE_SPEED_ZONE(<< -643.08, 249.90, 80.27 >>, 15.0, 5.0)

				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vsAmandasCar.VehicleIndex) AND NOT IS_ENTITY_DEAD(vsAmandasCar.VehicleIndex)
				SET_FORCE_HD_VEHICLE(vsAmandasCar.VehicleIndex, TRUE)
				SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vsAmandasCar.VehicleIndex, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex) AND NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
				SET_FORCE_HD_VEHICLE(vsMichaelsCar.VehicleIndex, TRUE)
				SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vsMichaelsCar.VehicleIndex, TRUE)
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF ( bCutsceneSkipped = TRUE )
						WARP_PED(PLAYER_PED_ID(), MissionPosition.vPosition, MissionPosition.fHeading, FALSE, FALSE, FALSE)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					ENDIF
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 0, 0)
					IF ( bCutsceneSkipped = TRUE )
						WARP_PED(psJimmy.PedIndex, psJimmy.vPosition, psJimmy.fHeading, FALSE, FALSE, FALSE)
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(psJimmy.PedIndex, <<-629.51685, 248.82660, 80.57744>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
					FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Amanda")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Amanda.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psAmanda.PedIndex) AND IS_VEHICLE_DRIVEABLE(vsAmandasCar.VehicleIndex)
					SET_VEHICLE_DOORS_SHUT(vsAmandasCar.VehicleIndex, TRUE)
					SET_PED_INTO_VEHICLE(psAmanda.PedIndex, vsAmandasCar.VehicleIndex, VS_DRIVER)
					TASK_VEHICLE_DRIVE_WANDER(psAmanda.PedIndex, vsAmandasCar.VehicleIndex, 30, DRIVINGMODE_AVOIDCARS)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Fabien")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Fabien.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psFabien.PedIndex) AND IS_VEHICLE_DRIVEABLE(vsAmandasCar.VehicleIndex)
					SET_PED_INTO_VEHICLE(psFabien.PedIndex, vsAmandasCar.VehicleIndex, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Amandas_car")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Amandas_car.")
				#ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vsAmandasCar.VehicleIndex)
					SET_VEHICLE_DOORS_SHUT(vsAmandasCar.VehicleIndex, TRUE)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vsAmandasCar.VehicleIndex)	//fix for B*1808939
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling FORCE_ENTITY_AI_AND_ANIMATION_UPDATE() for Amandas_car.")
					#ENDIF
					SET_VEHICLE_ON_GROUND_PROPERLY(vsAmandasCar.VehicleIndex)
					SET_VEHICLE_ENGINE_ON(vsAmandasCar.VehicleIndex, TRUE, TRUE)
					SET_VEHICLE_FORWARD_SPEED(vsAmandasCar.VehicleIndex, 5)
					PLAY_SOUND_FROM_ENTITY(-1, "Amanda_Pulls_Away", vsAmandasCar.VehicleIndex, "FAMILY_6_SOUNDS")
					START_AUDIO_SCENE("FAMILY_6_AMANDA_PULLS_AWAY")
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vsAmandasCar.VehicleIndex, "FAMILY_6_AMANDA_PULLS_AWAY_GROUP")
					SET_AUDIO_VEHICLE_PRIORITY(vsAmandasCar.VehicleIndex, AUDIO_VEHICLE_PRIORITY_MAX)
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				IF ( bCutsceneSkipped = TRUE )
					CLEANUP_PED(psAmanda, TRUE)
					CLEANUP_PED(psFabien, TRUE)
					IF DOES_ENTITY_EXIST(vsAmandasCar.VehicleIndex)
						DELETE_VEHICLE(vsAmandasCar.VehicleIndex)
					ENDIF
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedZone)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
												
				RETURN TRUE
				
			ELSE

				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_DURING_GO_TO_TRACEY()

	IF  NOT IS_PED_INJURED(psJimmy.PedIndex)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())

		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JIM")
			IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JIM", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM6_JIM", TRUE)
						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
		
			IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATT2")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATTOO")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psJimmy.PedIndex)
						IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", PICK_STRING(bStageReplayInProgress, "FAM6_TATT2", "FAM6_TATTOO"), CONV_PRIORITY_MEDIUM)
							CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							SET_LABEL_AS_TRIGGERED(PICK_STRING(bStageReplayInProgress, "FAM6_TATT2", "FAM6_TATTOO"), TRUE)
							bRunConversationPausingCheck = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//if main conversation was interrupted, resume it from the next label
			IF ( bCurrentConversationInterrupted = TRUE )
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JBAD")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JWARN")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")

						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_TATT2")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_TATTOO")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK")
						
							//play conversation for Jimmy saying cops were lost and it's safe to go to shrink
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOST_3")
									IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")	//this will only play if conversation to lose cops was triggered
										IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOST")
											IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JLOST", CONV_PRIORITY_MEDIUM)
												iCopsLostConversationCount++
												SET_LABEL_AS_TRIGGERED("FAM6_JLOST", TRUE)
												IF ( iCopsLostConversationCount >= 3)
													SET_LABEL_AS_TRIGGERED("FAM6_JLOST_3", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							//resume the interrupted coversation
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF 	NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
								AND NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationLabel)
									IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_TATT2")		//resume only the main conversations
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_TATTOO")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_JTALK")
									
										IF 	( iPlayerOffencesCount >= 3 )
										AND	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK")
											//if player commited more than 3 offences and the weird conversation did not trigger
											//let the weird conversations trigger first before it can be resumed
										ELSE
											//resume conversation from specific root and line label
											IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(FAM6Conversation, "FAM6AUD", CurrentConversationRoot, CurrentConversationLabel, CONV_PRIORITY_MEDIUM)
												bCurrentConversationInterrupted = FALSE
												SET_LABEL_AS_TRIGGERED("FAM6_JBAD", FALSE)	//mark all the interrupt conversations as not triggered
												SET_LABEL_AS_TRIGGERED("FAM6_JLOST", FALSE)
												SET_LABEL_AS_TRIGGERED("FAM6_JWANT", FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
	
				//resume paused conversations when player gets back in the car
				//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						//PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					//ENDIF
				//ENDIF
			
			ELSE //locates header blip does not exist
				
				//play interrupt conversation when player becomes wanted
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
					
					IF ( bCurrentConversationInterrupted = FALSE )
					
						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_TATT2")	
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_TATTOO")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK")
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
								CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								
								IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
									IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_TATT2")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_TATTOO")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_JTALK")
										bCurrentConversationInterrupted = TRUE
										CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
										CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF

					ENDIF
					
					IF ( bCurrentConversationInterrupted = TRUE )
						IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JWANT", CONV_PRIORITY_HIGH)
									iPlayerOffencesCount++
									iLoseCopsReminderTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 12500)
									SET_LABEL_AS_TRIGGERED("FAM6_JWANT", TRUE)
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOSE_5")
						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")
							IF GET_GAME_TIMER() - iLoseCopsReminderTimer > 0
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JLOSE", CONV_PRIORITY_MEDIUM)
										iLoseCopsReminderCount++
										iLoseCopsReminderTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12500, 15000)
										IF ( iLoseCopsReminderCount >= 5 )
											SET_LABEL_AS_TRIGGERED("FAM6_JLOSE_5", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//keep reseting those flags during wanted level as the player might be driving recklessly
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
					
				ELSE
				
					//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//	IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					//		PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					//	ENDIF
					//ENDIF
					
				ENDIF

			ENDIF
			
			//play interrupt conversations for player offences
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF ( GET_GAME_TIMER() - iPlayerOffenceTimer > 5000 )
					
					//play conversation for player running over peds
					IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JBAD_5")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JWARN_6")

						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF	HAS_PLAYER_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
							AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
								bPlayerHitPed = TRUE
							ENDIF
						ENDIF
						
						IF IS_PED_SHOOTING(PLAYER_PED_ID())	//add more conditions here if needed
							bPlayerActingBad = TRUE
						ENDIF
						
						IF ( bPlayerHitPed = TRUE OR bPlayerActingBad = TRUE )
						
							IF HAS_LABEL_BEEN_TRIGGERED("FAM6_TATT2")
							OR HAS_LABEL_BEEN_TRIGGERED("FAM6_TATTOO")
							OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK")
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
									CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
									
									IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
										IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_TATT2")
										OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_TATTOO")
										OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_JTALK")
											CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
											CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
											bCurrentConversationInterrupted = TRUE
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ENDIF
									ENDIF
								
								ENDIF
							ENDIF							

							IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK")	//if jimmy did no start his talk for player messing around

								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JBAD", CONV_PRIORITY_MEDIUM)
									iPlayerOffencesCount++
									iBadConversationCount++
									bPlayerHitPed		= FALSE
									bPlayerActingBad 	= FALSE
									iPlayerOffenceTimer = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("FAM6_JBAD", TRUE)
									IF ( iBadConversationCount >= 5 )
										SET_LABEL_AS_TRIGGERED("FAM6_JBAD_5", TRUE)
									ENDIF
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
									#ENDIF
								ENDIF
							
							ELSE											//if jimmy started his talk for player messing around
							
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JWARN", CONV_PRIORITY_MEDIUM)
									iPlayerOffencesCount++
									iBadConversationCount++
									bPlayerHitPed		= FALSE
									bPlayerActingBad 	= FALSE
									iPlayerOffenceTimer = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("FAM6_JWARN", TRUE)
									IF ( iBadConversationCount >= 6 )
										SET_LABEL_AS_TRIGGERED("FAM6_JWARN_6", TRUE)
									ENDIF
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
									#ENDIF
								ENDIF
							
							ENDIF
							
						ENDIF
					ENDIF
				
				ELSE
				
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
				
				ENDIF
			ENDIF
		
			//play conversation when more than 3 offences have been commited
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK")
				IF ( iPlayerOffencesCount >= 3 )
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF 	IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))  
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JTALK", CONV_PRIORITY_MEDIUM)
										bCurrentConversationInterrupted = FALSE
										SET_LABEL_AS_TRIGGERED("FAM6_JTALK", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_ARRIVE")
				IF ( bDestinationReached = TRUE )
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_ARRIVE", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM6_ARRIVE", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF ( bRunConversationPausingCheck = TRUE )
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
				IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
						ENDIF
					ENDIF
				ELSE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF				
			ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
			AND NOT IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex) 		
				IF 	GET_DISTANCE_BETWEEN_ENTITIES(psJimmy.PedIndex, PLAYER_PED_ID()) < 10 		
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
						ENDIF
					ENDIF
				ELSE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF

ENDPROC

PROC MANAGE_TATTOO_PARLOR_SCENE(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTattooShopScenePosition) < 150.0
			
				iProgress++
				
			ENDIF
				
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(psTracey.ModelName)
			REQUEST_MODEL(psLazlow.ModelName)
			
			REQUEST_ANIM_DICT("missfam6leadinoutfam_6_mcs_2")
			
			IF 	HAS_MODEL_LOADED(psTracey.ModelName)
			AND	HAS_MODEL_LOADED(psLazlow.ModelName)
			AND HAS_ANIM_DICT_LOADED("missfam6leadinoutfam_6_mcs_2")
			
				IF 	HAS_MISSION_PED_BEEN_CREATED(psTracey, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_TRACEY)
				AND HAS_MISSION_PED_BEEN_CREATED(psLazlow, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
			
					SET_TRACEY_PED_COMPONENT_VARIATIONS(psTracey.bComponentVariationsSet)
					SET_LAZLOW_PED_COMPONENT_VARIATIONS(psLazlow.bComponentVariationsSet, FALSE, FALSE, FALSE, FALSE, FALSE)
					
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iTattooShopSceneID)
					
						iTattooShopSceneID = CREATE_SYNCHRONIZED_SCENE(vTattooShopScenePosition, vTattooShopSceneRotation)
						
						SET_SYNCHRONIZED_SCENE_LOOPED(iTattooShopSceneID, TRUE)
						
						TASK_SYNCHRONIZED_SCENE(psTracey.PedIndex, iTattooShopSceneID, "missfam6leadinoutfam_6_mcs_2", "_leadin_loop_tracy", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psTracey.PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTracey.PedIndex, TRUE)
						
						TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iTattooShopSceneID, "missfam6leadinoutfam_6_mcs_2", "_leadin_loop_lazlow", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psLazlow.PedIndex, TRUE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(psTracey.ModelName)
						SET_MODEL_AS_NO_LONGER_NEEDED(psLazlow.ModelName)
					
						iProgress++
					
					ENDIF
				
				ENDIF
			
			ENDIF
			
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_AMANDA_AND_FABIEN_DURING_GO_TO_TRACEY()

	IF 	DOES_ENTITY_EXIST(psFabien.PedIndex)
	AND DOES_ENTITY_EXIST(psAmanda.PedIndex)
	AND DOES_ENTITY_EXIST(vsAmandasCar.VehicleIndex)
	
		IF 	NOT IS_ENTITY_DEAD(psFabien.PedIndex)
		AND NOT IS_ENTITY_DEAD(psAmanda.PedIndex)
		AND NOT IS_ENTITY_DEAD(vsAmandasCar.VehicleIndex)
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vsAmandasCar.VehicleIndex)) > 125.0
			
				SET_PED_KEEP_TASK(psAmanda.PedIndex, TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(psAmanda.PedIndex)
				
				SET_PED_KEEP_TASK(psFabien.PedIndex, TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(psFabien.PedIndex)
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vsAmandasCar.VehicleIndex)
			
			ENDIF
			
		ENDIF
	
	ENDIF
	
ENDPROC

FUNC BOOL IS_MISSION_STAGE_DRIVE_TO_TRACEY_COMPLETED(INT &iStageProgress)

	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	MANAGE_CONVERSATIONS_DURING_GO_TO_TRACEY()
	MANAGE_AMANDA_AND_FABIEN_DURING_GO_TO_TRACEY()
	MANAGE_TATTOO_PARLOR_SCENE(iTattooParlorSceneProgress)
	
	//handle requestting mocap cutscene 
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_PLAYER_CHANGING_CLOTHES()
          
		IF HAS_THIS_CUTSCENE_LOADED("fam_6_mcs_2_concat")
		OR HAS_CUTSCENE_LOADED()
		OR IS_CUTSCENE_ACTIVE()
			REMOVE_CUTSCENE()
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
			#ENDIF
		ENDIF
		                        
	ELSE
	
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTattooShopPosition) < DEFAULT_CUTSCENE_LOAD_DIST / 2
			
			REQUEST_CUTSCENE("fam_6_mcs_2_concat")
			
			IF NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vTattooShopScenePosition))		
				PIN_INTERIOR_IN_MEMORY(GET_INTERIOR_AT_COORDS(vTattooShopScenePosition))
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Pinning interior at coordinates ", vTattooShopScenePosition, " in memory.")
				#ENDIF
			ENDIF

			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
				SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()	
				SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(FALSE, FALSE, FALSE, FALSE, FALSE)		
			ENDIF
			
		ELSE
		
			IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vTattooShopScenePosition))
				UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(vTattooShopScenePosition))
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unpinning interior at coordinates ", vTattooShopScenePosition, ".")
				#ENDIF
			ENDIF
		
			IF HAS_THIS_CUTSCENE_LOADED("fam_6_mcs_2_concat")
			OR HAS_CUTSCENE_LOADED()
			OR IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
				#ENDIF
			ENDIF
		
		ENDIF

	ENDIF
	
	SWITCH iStageProgress
		
		CASE 0
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
				IF IS_SCREEN_FADED_OUT()
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(psJimmy.PedIndex)	
				IF ( GET_SCRIPT_TASK_STATUS(psJimmy.PedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK )
					IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex) AND NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
						TASK_ENTER_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP * 3, VS_FRONT_RIGHT, PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_SCREEN_FADED_OUT()
				WAIT(2)
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		
			IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JIM")
			
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
			
				SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(sLocatesData, vsMichaelsCar.VehicleIndex)
			
				START_AUDIO_SCENE("FAMILY_6_DRIVE_TO_TATTOO")
			
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 1
		
			IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(sLocatesData, vTattooShopPosition, DEFAULT_LOCATES_SIZE_VECTOR(), TRUE, psJimmy.PedIndex, "FAM6_TATTOO", "CMN_JLEAVE", FALSE, TRUE)
		
			//do the check to progress the stage here
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psJimmy.PedIndex)
				
					//locate to park the car
					IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<302.371002,172.648605,102.820877>>, <<294.813934,175.443298,106.071678>>, 5.0) 	
					AND IS_ENTITY_IN_ANGLED_AREA(psJimmy.PedIndex, <<302.371002,172.648605,102.820877>>, <<294.813934,175.443298,106.071678>>, 5.0)	
					
						IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()	//kill conversations and let last line play out
							
							bDestinationReached = TRUE
							
							iStageProgress++
						
						ENDIF
						
					ENDIF
					//bigger locate around the tattoo parlor entrance
					IF	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<320.562500,178.723297,102.389084>>, <<317.622711,170.784821,106.276665>>, 12.5)
					AND	IS_ENTITY_IN_ANGLED_AREA(psJimmy.PedIndex, <<320.562500,178.723297,102.389084>>, <<317.622711,170.784821,106.276665>>, 12.5)
					
						IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()	//kill conversations and let last line play out
							
							bDestinationReached = TRUE
							
							iStageProgress++
							
						ENDIF
					ENDIF
				
				ELSE
				
					IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<314.117126,180.860062,102.418434>>, <<309.434204,168.080780,106.292229>>, 28.0)
					AND	IS_ENTITY_IN_ANGLED_AREA(psJimmy.PedIndex, <<314.117126,180.860062,102.418434>>, <<309.434204,168.080780,106.292229>>, 28.0)
					
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
								
								KILL_ANY_CONVERSATION()
								KILL_FACE_TO_FACE_CONVERSATION()	//kill conversations and let last line play out
								
								bDestinationReached = TRUE
								
								iStageProgress++
								
							ENDIF
						ELSE
						
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
								
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()	//kill conversations and let last line play out
							
							bDestinationReached = TRUE
							
							iStageProgress++
						
						ENDIF
						
					ELSE
						
						//player is on foot in the locate, jimmy is in player group but not in locate but still nearby, fix for B*1982582
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<314.117126,180.860062,102.418434>>, <<309.434204,168.080780,106.292229>>, 28.0)
							IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_PED_INJURED(psJimmy.PedIndex) AND IS_PED_IN_GROUP(psJimmy.PedIndex)
								
									CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
									
									KILL_ANY_CONVERSATION()
									KILL_FACE_TO_FACE_CONVERSATION()	//kill conversations and let last line play out
									
									bDestinationReached = TRUE
									
									iStageProgress++
								
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), DEFAULT_VEH_STOPPING_DISTANCE)
					
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()		//check if scripted conversations are not ongoing using native command
					OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//check if any conversations are not ongoing using script command

						iStageProgress++
						
					ENDIF

				ENDIF
			ELSE

				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()		//check if scripted conversations are not ongoing using native command
				OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//check if any conversations are not ongoing using script command

					iStageProgress++
					
				ENDIF					
			ENDIF
		
		BREAK
		
		CASE 3
		
			IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(sLocatesData, vTattooShopDoorPosition, DEFAULT_LOCATES_SIZE_VECTOR(), TRUE, psJimmy.PedIndex, "", "", FALSE, TRUE)
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<309.861786,176.901199,101.957222>>, <<324.602966,171.513519,106.653946>>, 15.0)
			
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<321.037598,178.496094,102.441963>>, <<319.134644,173.429993,106.266571>>, 6.0)
				
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 4
		
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 3.0)
					
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iStageProgress++
					
				ENDIF
			ELSE

				REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)

				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 5
		
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
				CLEANUP_PED(psAmanda, TRUE)
				CLEANUP_PED(psFabien, TRUE)
				IF DOES_ENTITY_EXIST(vsAmandasCar.VehicleIndex)
					DELETE_VEHICLE(vsAmandasCar.VehicleIndex)
				ENDIF
			
				RETURN TRUE
				
			ENDIF
		
		BREAK
				
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_SHOP_1_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_mcs_2_concat")
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					IF IS_PED_GROUP_MEMBER(psJimmy.PedIndex, PLAYER_GROUP_ID())
						REMOVE_PED_FROM_GROUP(psJimmy.PedIndex)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psJimmy.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psTracey.PedIndex, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psTracey.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psLazlow.PedIndex, "Lazlow", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psLazlow.ModelName)
				ENDIF
								
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				STOP_AUDIO_SCENES()
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				VEHICLE_INDEX LastPlayerVehicleIndex
				
				LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
				
				IF DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
					IF NOT IS_ENTITY_DEAD(LastPlayerVehicleIndex)
					
						IF IS_VEHICLE_AVAILABLE_TO_SNAPSHOT(PLAYER_PED_ID(), LastPlayerVehicleIndex, TRUE)
	
							IF NOT IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
								
								SET_ENTITY_AS_MISSION_ENTITY(LastPlayerVehicleIndex)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
						
								vsMichaelsCar.VehicleIndex = LastPlayerVehicleIndex
							ENDIF
							
							STOP_VEHICLE_FIRE(LastPlayerVehicleIndex)
							
							SET_ENTITY_COORDS(LastPlayerVehicleIndex, vsMichaelsCar.vPosition)
							SET_ENTITY_HEADING(LastPlayerVehicleIndex, vsMichaelsCar.fHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(LastPlayerVehicleIndex)
							SET_VEHICLE_DOORS_SHUT(LastPlayerVehicleIndex, TRUE)
							SET_VEHICLE_ENGINE_ON(LastPlayerVehicleIndex, FALSE, TRUE)
							
							OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(LastPlayerVehicleIndex)

						ENDIF
					ENDIF
					
				ENDIF
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				REMOVE_ANIM_DICT("missfam6leadinoutfam_6_mcs_2")
				
				CLEAR_AREA_OF_COPS(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_PEDS(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_OBJECTS(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_PROJECTILES(MissionPosition.vPosition, 100.0)
				REMOVE_PARTICLE_FX_IN_RANGE(MissionPosition.vPosition, 100.0)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			REQUEST_MODEL(osPiercingGun.ModelName)
			REQUEST_ANIM_DICT("missfam6ig_1_pierce")
			REQUEST_ANIM_DICT("missfam6ig_7_tattoo")
			REQUEST_SCRIPT_AUDIO_BANK("FAM6_PIERCING_GUN")
			
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)	
				SET_CAM_PARAMS(ScriptedCamera, << 323.4799, 178.8823, 104.7932 >>, << -23.9135, 13.3903, -17.3261 >>, 25.0)
				SHAKE_CAM(ScriptedCamera,"HAND_SHAKE",0.5)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(AnimatedCamera)
				AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, FALSE) 
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_CAMERA() returned true for camera.")
				#ENDIF
				
				REPLAY_STOP_EVENT()
				
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 0, 0)
					
					IF ( bCutsceneSkipped = FALSE )
						WARP_PED(psJimmy.PedIndex, psJimmy.vPosition, psJimmy.fHeading, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM(psJimmy.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_01_jimmy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psJimmy.PedIndex, TRUE)
					ENDIF
					
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tracy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Tracy.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
				
					IF ( bCutsceneSkipped = FALSE )
						WARP_PED(psTracey.PedIndex, psTracey.vPosition, psTracey.fHeading, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM(psTracey.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_02_tracey", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psTracey.PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTracey.PedIndex, TRUE)
					ENDIF
				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lazlow")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Lazlow.")
				#ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				
				IF ( bCutsceneSkipped = FALSE )
				
					IF 	HAS_MODEL_LOADED(osPiercingGun.ModelName)
					AND HAS_ANIM_DICT_LOADED("missfam6ig_1_pierce")
					AND HAS_ANIM_DICT_LOADED("missfam6ig_7_tattoo")
					AND REQUEST_SCRIPT_AUDIO_BANK("FAM6_PIERCING_GUN")
				
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID)
						
							iPierceSceneID = CREATE_SYNCHRONIZED_SCENE(<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)
						
							IF HAS_MISSION_OBJECT_BEEN_CREATED(osPiercingGun)
								IF DOES_ENTITY_EXIST(osPiercingGun.ObjectIndex) AND NOT IS_ENTITY_DEAD(osPiercingGun.ObjectIndex)
									ATTACH_ENTITY_TO_ENTITY(osPiercingGun.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_michael", INSTANT_BLEND_IN, SLOW_BLEND_OUT  )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
							
							IF NOT IS_PED_INJURED(psLazlow.PedIndex)
								TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_lazlow", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
							ENDIF
							
							iStageProgress++
							
						ENDIF
				
					ENDIF
					
				ENDIF
				
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				IF ( bCutsceneSkipped = TRUE )
				
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID)
				
						IF 	HAS_MODEL_LOADED(osPiercingGun.ModelName)
						AND HAS_ANIM_DICT_LOADED("missfam6ig_1_pierce")
						AND HAS_ANIM_DICT_LOADED("missfam6ig_7_tattoo")
						AND REQUEST_SCRIPT_AUDIO_BANK("FAM6_PIERCING_GUN")
				
							iPierceSceneID = CREATE_SYNCHRONIZED_SCENE(<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)
						
							IF HAS_MISSION_OBJECT_BEEN_CREATED(osPiercingGun)
								IF DOES_ENTITY_EXIST(osPiercingGun.ObjectIndex) AND NOT IS_ENTITY_DEAD(osPiercingGun.ObjectIndex)
									ATTACH_ENTITY_TO_ENTITY(osPiercingGun.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_michael", INSTANT_BLEND_IN, SLOW_BLEND_OUT  )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
							
							
							IF NOT IS_PED_INJURED(psLazlow.PedIndex)
								TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_lazlow", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
							ENDIF
							
							IF NOT IS_PED_INJURED(psJimmy.PedIndex)
								WARP_PED(psJimmy.PedIndex, psJimmy.vPosition, psJimmy.fHeading, FALSE, FALSE, FALSE)
								TASK_PLAY_ANIM(psJimmy.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_01_jimmy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psJimmy.PedIndex, TRUE)
							ENDIF
							
							IF NOT IS_PED_INJURED(psTracey.PedIndex)
								WARP_PED(psTracey.PedIndex, psTracey.vPosition, psTracey.fHeading, FALSE, FALSE, FALSE)
								TASK_PLAY_ANIM(psTracey.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_02_tracey", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psTracey.PedIndex)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTracey.PedIndex, TRUE)
							ENDIF

							iStageProgress++
							
						ENDIF
						
					ENDIF

				ELSE
				
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID)
					
						iStageProgress++
					
					ENDIF
				
				ENDIF

			ELSE

				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
		
		CASE 3
		
			SET_MODEL_AS_NO_LONGER_NEEDED(osPiercingGun.ModelName)
					
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
											
			RETURN TRUE
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_PIERCING_HELP_TEXT()

	IF 	DOES_CAM_EXIST(ScriptedCamera)
	AND	DOES_CAM_EXIST(AnimatedCamera)
		IF 	IS_CAM_ACTIVE(ScriptedCamera)
		AND NOT IS_CAM_ACTIVE(AnimatedCamera)
			IF 	IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				IF NOT bBrow AND NOT bNose AND NOT bEar
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_PIERC0")
						PRINT_HELP_FOREVER("FAM6_PIERC0")
						SET_LABEL_AS_TRIGGERED("FAM6_PIERC0", TRUE)
					ENDIF
				ELIF NOT bBrow AND NOT bNose AND bEar
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_PIERC1")
						PRINT_HELP_FOREVER("FAM6_PIERC1")
						SET_LABEL_AS_TRIGGERED("FAM6_PIERC1", TRUE)
					ENDIF
				ELIF NOT bBrow AND bNose AND NOT bEar
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_PIERC2")
						PRINT_HELP_FOREVER("FAM6_PIERC2")
						SET_LABEL_AS_TRIGGERED("FAM6_PIERC2", TRUE)
					ENDIF
				ELIF bBrow AND NOT bNose AND NOT bEar
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_PIERC3")
						PRINT_HELP_FOREVER("FAM6_PIERC3")
						SET_LABEL_AS_TRIGGERED("FAM6_PIERC3", TRUE)
					ENDIF
				ELIF NOT bBrow AND bNose AND bEar
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_PIERC4")
						PRINT_HELP_FOREVER("FAM6_PIERC4")
						SET_LABEL_AS_TRIGGERED("FAM6_PIERC4", TRUE)
					ENDIF
				ELIF bBrow AND NOT bNose AND bEar
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_PIERC5")
						PRINT_HELP_FOREVER("FAM6_PIERC5")
						SET_LABEL_AS_TRIGGERED("FAM6_PIERC5", TRUE)
					ENDIF
				ELIF bBrow AND bNose AND NOT bEar
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_PIERC6")
						PRINT_HELP_FOREVER("FAM6_PIERC6")
						SET_LABEL_AS_TRIGGERED("FAM6_PIERC6", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_PIERCING_STAGES()

	SWITCH ePiercingStage
	
		CASE PIERCING_IDLE
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID)
				
					IF ( iConversationTimer != 0 )
						IF GET_GAME_TIMER() - iConversationTimer > 0

							IF NOT bBrow AND NOT bNose AND NOT bEar

								IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_IDLEP_5")
									IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_IDLEP", CONV_PRIORITY_MEDIUM)
										iIdleConversationcounter++
										iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 12500)
										IF ( iIdleConversationcounter >= 5 )
											SET_LABEL_AS_TRIGGERED("FAM6_IDLEP_5", TRUE)
										ENDIF
									ENDIF
								ENDIF
							
							ELSE
							
								SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
								
									CASE 0
										IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_IDLEP_5")
											IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_IDLEP", CONV_PRIORITY_MEDIUM)
												iIdleConversationcounter++
												iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 12500)
												IF ( iIdleConversationcounter >= 5 )
													SET_LABEL_AS_TRIGGERED("FAM6_IDLEP_5", TRUE)
												ENDIF
											ENDIF
										ENDIF
									BREAK
									
									CASE 1
										IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_P1_3")
											IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_P1", CONV_PRIORITY_MEDIUM)
												iIdle1Conversationcounter++
												iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 12500)
												IF ( iIdle1Conversationcounter >= 3 )
													SET_LABEL_AS_TRIGGERED("FAM6_P1_3", TRUE)
												ENDIF
											ENDIF
										ENDIF
									BREAK
									
									CASE 2
										IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_P2_2")
											IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_P2", CONV_PRIORITY_MEDIUM)
												iIdle2Conversationcounter++
												iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 12500)
												IF ( iIdle2Conversationcounter >= 2 )
													SET_LABEL_AS_TRIGGERED("FAM6_P2_2", TRUE)
												ENDIF
											ENDIF
										ENDIF
									BREAK
									
								ENDSWITCH
					
							ENDIF
						ENDIF
					ENDIF
					
					IF ( iStruggleConversationTimer != 0 )
						IF GET_GAME_TIMER() - iStruggleConversationTimer > 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_STRUG", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
									iStruggleConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 6000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE PIERCING_BROW
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iPierceSceneID) >= 0.068
			
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 256)
				PLAY_SOUND_FROM_ENTITY(-1, "Pierce", osPiercingGun.ObjectIndex, "FAMILY_6_SOUNDS")
				
				IF ( iConversationCounter < 2 ) 
					IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_BROW", CONV_PRIORITY_MEDIUM)
						iConversationCounter++
					ENDIF
				ENDIF
				
				IF NOT bEar AND NOT bNose		
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_SPECIAL2, 0, 2)
				ELIF bEar AND NOT bNose	
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_SPECIAL2, 0, 5)
				ELIF NOT bEar AND bNose	
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_SPECIAL2, 0, 6)
				ELIF bEar AND bNose
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_SPECIAL2, 0, 7)
				ENDIF

				ePiercingStage 		= PIERCING_IDLE
				iConversationTimer	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 2000)
			
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
		
		BREAK
		
		CASE PIERCING_NOSE
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iPierceSceneID) >= 0.075
			
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 256)
				PLAY_SOUND_FROM_ENTITY(-1, "Pierce", osPiercingGun.ObjectIndex, "FAMILY_6_SOUNDS")
				
				IF ( iConversationCounter < 2 ) 
					IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_NOSE", CONV_PRIORITY_MEDIUM)
						iConversationCounter++
					ENDIF
				ENDIF
				
				IF NOT bEar AND NOT bBrow		
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_SPECIAL2, 0, 3)
				ELIF bEar AND NOT bBrow	
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_SPECIAL2, 0, 4)
				ELIF NOT bEar AND bBrow
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_SPECIAL2, 0, 6)
				ELIF bEar AND bBrow
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_SPECIAL2, 0, 7)
				ENDIF

				ePiercingStage 		= PIERCING_IDLE
				iConversationTimer	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 2000)
				
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
		
		BREAK
		
		CASE PIERCING_EAR
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iPierceSceneID) >= 0.07
			
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 256)
				PLAY_SOUND_FROM_ENTITY(-1, "Pierce", osPiercingGun.ObjectIndex, "FAMILY_6_SOUNDS")
				
				IF ( iConversationCounter < 2 ) 
					IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_EAR", CONV_PRIORITY_MEDIUM)
						iConversationCounter++
					ENDIF
				ENDIF
				
				IF NOT bNose AND NOT bBrow		
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_SPECIAL2,0,1)
				ELIF bNose AND NOT bBrow	
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_SPECIAL2,0,4)
				ELIF NOT bNose AND bBrow
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_SPECIAL2,0,5)
				ELIF bNose AND bBrow
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_SPECIAL2,0,7)
				ENDIF
				
				ePiercingStage 		= PIERCING_IDLE
				iConversationTimer	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 2000)
				
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC BOOL IS_MISSION_STAGE_PIERCE_LAZLOW_COMPLETED(INT &iStageProgress)

	MANAGE_PIERCING_STAGES()
	MANAGE_PIERCING_HELP_TEXT()
	
	SWITCH iStageProgress
		
		CASE 0
		
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID)
			
				REQUEST_MODEL(osPiercingGun.ModelName)
				REQUEST_ANIM_DICT("missfam6ig_1_pierce")
				REQUEST_ANIM_DICT("missfam6ig_7_tattoo")
				REQUEST_SCRIPT_AUDIO_BANK("FAM6_PIERCING_GUN")
				
				IF 	HAS_MODEL_LOADED(osPiercingGun.ModelName)
				AND HAS_ANIM_DICT_LOADED("missfam6ig_1_pierce")
				AND HAS_ANIM_DICT_LOADED("missfam6ig_7_tattoo")
				AND REQUEST_SCRIPT_AUDIO_BANK("FAM6_PIERCING_GUN")
					
					iPierceSceneID = CREATE_SYNCHRONIZED_SCENE(<< 324.130, 181.290, 102.600 >>, << 0, 0, -22.320 >>)
				
					IF HAS_MISSION_OBJECT_BEEN_CREATED(osPiercingGun)
						IF DOES_ENTITY_EXIST(osPiercingGun.ObjectIndex) AND NOT IS_ENTITY_DEAD(osPiercingGun.ObjectIndex)
							ATTACH_ENTITY_TO_ENTITY(osPiercingGun.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_michael", INSTANT_BLEND_IN, SLOW_BLEND_OUT  )
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					IF NOT IS_PED_INJURED(psLazlow.PedIndex)
						TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_lazlow", INSTANT_BLEND_IN, SLOW_BLEND_OUT )
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
					ENDIF
					
					IF NOT DOES_CAM_EXIST(ScriptedCamera)
						ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)	
						SET_CAM_PARAMS(ScriptedCamera, << 323.4799, 178.8823, 104.7932 >>, << -23.9135, 13.3903, -17.3261 >>, 25.0)
						SHAKE_CAM(ScriptedCamera,"HAND_SHAKE",0.5)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					
					IF NOT DOES_CAM_EXIST(AnimatedCamera)
						AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, FALSE) 
					ENDIF
					
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						WARP_PED(psJimmy.PedIndex, psJimmy.vPosition, psJimmy.fHeading, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM(psJimmy.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_01_jimmy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psJimmy.PedIndex, TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(psTracey.PedIndex)
						WARP_PED(psTracey.PedIndex, psTracey.vPosition, psTracey.fHeading, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM(psTracey.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_02_tracey", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psTracey.PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTracey.PedIndex, TRUE)
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(osPiercingGun.ModelName)
					
				ENDIF
			
			ELSE

				bEar							= FALSE
				bNose 							= FALSE
				bBrow							= FALSE
				iStruggleConversationTimer		= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
				iConversationTimer				= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(250, 1000)
				iConversationCounter			= 0
				iIdle1Conversationcounter 		= 0
				iIdle2Conversationcounter 		= 0
				iIdleConversationcounter 		= 0
				ePiercingStage 					= PIERCING_IDLE
				
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM6_PIERCING_TIME)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				START_AUDIO_SCENE("FAMILY_6_PIERCE_LAZLOW_MG")
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
				bReplayStartEventTriggered = TRUE
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				SETUP_PC_CONTROLS()
												
				iStageProgress++
			
			ENDIF
		
		BREAK
		
		CASE 101
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iPierceSceneID) >= 1
			
				SET_CAM_ACTIVE(AnimatedCamera, FALSE)
				SET_CAM_ACTIVE(ScriptedCamera, TRUE)
				
				iPierceSceneID = CREATE_SYNCHRONIZED_SCENE(<< 324.130, 181.290, 102.600 >>, << 0, 0, -22.320 >>)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT )
				ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_lazlow",SLOW_BLEND_IN, SLOW_BLEND_OUT )
				ENDIF
				
				iStageProgress = 1
			ENDIF
			
		BREAK
		
		CASE 1
		
			IF 	IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID)
			AND GET_SYNCHRONIZED_SCENE_PHASE(iPierceSceneID) >= 1
			
				iPierceSceneID = CREATE_SYNCHRONIZED_SCENE(<< 324.130, 181.290, 102.600 >>, << 0, 0, -22.320 >>)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT )
				ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iPierceSceneID, "missfam6ig_1_pierce", "ig_1_loop_lazlow",SLOW_BLEND_IN, SLOW_BLEND_OUT )
				ENDIF
				
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iPierceSceneID)
				IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RDOWN) AND NOT bEar)
				
					iPierceSceneID = CREATE_SYNCHRONIZED_SCENE(<< 324.130, 181.290, 102.600 >>, << 0.0, 0.0, -22.320 >>)
					
					SET_CAM_ACTIVE(AnimatedCamera, TRUE)
					PLAY_CAM_ANIM(AnimatedCamera,"ig_1_ear_cam","missfam6ig_1_pierce",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>,CAF_LOOPING)
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPierceSceneID, "missfam6ig_1_pierce", "ig_1_ear_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT )
					ENDIF
					
					IF NOT IS_PED_INJURED(psLazlow.PedIndex)
						TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iPierceSceneID, "missfam6ig_1_pierce", "ig_1_ear_lazlow", SLOW_BLEND_IN, SLOW_BLEND_OUT  )
					ENDIF
				
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						TASK_PLAY_ANIM(psJimmy.PedIndex,"missfam6ig_7_tattoo", "ig_7_idle_01_jimmy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,-1,AF_LOOPING)				
					ENDIF
					
					IF NOT IS_PED_INJURED(psTracey.PedIndex)
						TASK_PLAY_ANIM(psTracey.PedIndex,"missfam6ig_7_tattoo", "ig_7_idle_03_tracey", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,-1,AF_LOOPING)
					ENDIF
			
					bEar 			= TRUE
					ePiercingStage 	= PIERCING_EAR
			
					IF bNose AND bBrow					
						iStageProgress++				
					ELSE
						iStageProgress = 101
					ENDIF
					
				ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RRIGHT) AND NOT bNose)
				
					iPierceSceneID = CREATE_SYNCHRONIZED_SCENE(<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)
					
					SET_CAM_ACTIVE(AnimatedCamera, TRUE)
					PLAY_CAM_ANIM(AnimatedCamera, "ig_1_nose_cam", "missfam6ig_1_pierce", << 324.130, 181.290, 102.600 >>, << 0.0, 0.0, -22.320 >>, CAF_LOOPING)
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPierceSceneID, "missfam6ig_1_pierce", "ig_1_nose_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT  )
					ENDIF
					
					IF NOT IS_PED_INJURED(psLazlow.PedIndex)
						TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iPierceSceneID, "missfam6ig_1_pierce", "ig_1_nose_lazlow", SLOW_BLEND_IN, SLOW_BLEND_OUT  )	
					ENDIF
					
					bNose			= TRUE
					ePiercingStage 	= PIERCING_NOSE
					
					IF bEar AND bBrow
						iStageProgress++				
					ELSE
						iStageProgress = 101
					ENDIF
					
				ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RLEFT) AND NOT bBrow)
				
					iPierceSceneID = CREATE_SYNCHRONIZED_SCENE(<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)
					
					SET_CAM_ACTIVE(AnimatedCamera, TRUE)
					PLAY_CAM_ANIM(AnimatedCamera,"ig_1_brow_cam","missfam6ig_1_pierce",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>,CAF_LOOPING)					
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPierceSceneID, "missfam6ig_1_pierce", "ig_1_brow_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT  )
					ENDIF
					
					IF NOT IS_PED_INJURED(psLazlow.PedIndex)
						TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iPierceSceneID, "missfam6ig_1_pierce", "ig_1_brow_lazlow", SLOW_BLEND_IN, SLOW_BLEND_OUT  )			
					ENDIF
					
					bBrow			= TRUE
					ePiercingStage 	= PIERCING_BROW

					IF bEar AND bNose					
						iStageProgress++				
					ELSE
						iStageProgress = 101
					ENDIF
					
				ENDIF	
			ENDIF
		
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("last_piercing"))
					IF ( bCutsceneAssetsRequested = TRUE )
					
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF ( bReplayStartEventTriggered = TRUE )
							IF ( bReplayStopEventTriggered = FALSE )
								REPLAY_STOP_EVENT()
								bReplayStopEventTriggered = TRUE
							ENDIF
						ENDIF
						
						CLEANUP_PC_CONTROLS()
						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	
	IF ( iConversationCounter >= 2  )
		REQUEST_CUTSCENE("fam_6_mcs_3")	//request the cutscene after two piercings have been done
		
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
			SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
			SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
			SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(FALSE, TRUE, FALSE, FALSE, FALSE)
			bCutsceneAssetsRequested = TRUE
		ENDIF
	ENDIF
	

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_SHOP_2_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_mcs_3")
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					IF IS_PED_GROUP_MEMBER(psJimmy.PedIndex, PLAYER_GROUP_ID())
						REMOVE_PED_FROM_GROUP(psJimmy.PedIndex)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psJimmy.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psTracey.PedIndex, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psTracey.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psLazlow.PedIndex, "Lazlow", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psLazlow.ModelName)
				ENDIF
				
				REGISTER_ENTITY_FOR_CUTSCENE(osShirt.ObjectIndex, "Lazlow_Shirt", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osShirt.ModelName)
				REGISTER_ENTITY_FOR_CUTSCENE(osTattooNeedle.ObjectIndex, "Tattoo_Gun", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osTattooNeedle.ModelName)
				
				IF DOES_ENTITY_EXIST(osPiercingGun.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osPiercingGun.ObjectIndex)
						IF IS_ENTITY_ATTACHED(osPiercingGun.ObjectIndex)
							DETACH_ENTITY(osPiercingGun.ObjectIndex, FALSE, TRUE)
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(osPiercingGun.ObjectIndex, "Piercing_Gun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osPiercingGun.ModelName)
					ENDIF
				ENDIF
								
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				STOP_AUDIO_SCENES()
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(FALSE, TRUE, FALSE, FALSE, FALSE)
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
				
				REMOVE_ANIM_DICT("missfam6ig_1_pierce")
				
				IF DOES_CAM_EXIST(ScriptedCamera)
					DESTROY_CAM(ScriptedCamera)
				ENDIF

				IF DOES_CAM_EXIST(AnimatedCamera)
					DESTROY_CAM(AnimatedCamera)
				ENDIF
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			REQUEST_MODEL(osTattooNeedle.ModelName)
			REQUEST_ANIM_DICT("missfam6ig_7_tattoo")
			
			mov = REQUEST_SCALEFORM_MOVIE("tattoo_buttons")
			
			IF IS_PC_VERSION()
				mov_kb = REQUEST_SCALEFORM_MOVIE("tattoo_keys")
			ENDIF
			
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)	
				SET_CAM_PARAMS(ScriptedCamera, << 326.1551, 179.2442, 104.5627 >>, << -36.8795, 13.4340, 33.8400 >>, 52.0, 0)
				SHAKE_CAM(ScriptedCamera,"HAND_SHAKE",0.5)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(AnimatedCamera)
				AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, FALSE) 
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(osShirt.ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Lazlow_Shirt")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow_Shirt"))
						osShirt.ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow_Shirt"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object Lazlow_Shirt in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(osTattooNeedle.ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Tattoo_Gun")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tattoo_Gun"))
						osTattooNeedle.ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tattoo_Gun"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object Tattoo_Gun in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
								
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				REPLAY_STOP_EVENT()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lazlow_Shirt")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Lazlow_Shirt.")
				#ENDIF
				
				IF DOES_ENTITY_EXIST(osShirt.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osShirt.ObjectIndex)
						SET_ENTITY_INVINCIBLE(osShirt.ObjectIndex, TRUE)
						FREEZE_ENTITY_POSITION(osShirt.ObjectIndex, TRUE)
						SET_ENTITY_COLLISION(osShirt.ObjectIndex, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Piercing_Gun")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Piercing_Gun.")
				#ENDIF
				
				IF DOES_ENTITY_EXIST(osPiercingGun.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osPiercingGun.ObjectIndex)
						SET_ENTITY_INVINCIBLE(osPiercingGun.ObjectIndex, TRUE)
						FREEZE_ENTITY_POSITION(osPiercingGun.ObjectIndex, TRUE)
						SET_ENTITY_COLLISION(osPiercingGun.ObjectIndex, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tattoo_Gun")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Tattoo_Gun.")
				#ENDIF
				
				IF DOES_ENTITY_EXIST(osTattooNeedle.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osTattooNeedle.ObjectIndex)
						IF NOT IS_ENTITY_ATTACHED(osTattooNeedle.ObjectIndex)
							ATTACH_ENTITY_TO_ENTITY(osTattooNeedle.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 0, 0)
					
					IF ( bCutsceneSkipped = FALSE )
						SEQUENCE_INDEX JimmySequenceIndex
						CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
						OPEN_SEQUENCE_TASK(JimmySequenceIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, psLazlow.PedIndex)
							TASK_PLAY_ANIM(NULL, "missfam6ig_7_tattoo", "ig_7_idle_02_jimmy", WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)				
						CLOSE_SEQUENCE_TASK(JimmySequenceIndex)
						TASK_PERFORM_SEQUENCE(psJimmy.PedIndex, JimmySequenceIndex)
						CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psJimmy.PedIndex, TRUE)
					ENDIF
				ENDIF				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tracy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Tracy.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					IF ( bCutsceneSkipped = FALSE )
						TASK_PLAY_ANIM(psTracey.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_01_tracey", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psTracey.PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTracey.PedIndex, TRUE)
					ENDIF
				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					VECTOR vMichaelPosition, vMichaelRotation
					vMichaelPosition = GET_ANIM_INITIAL_OFFSET_POSITION("missfam6ig_7_tattoo","ig_7_loop_michael",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)
					vMichaelRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("missfam6ig_7_tattoo","ig_7_loop_michael",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)
					SET_ENTITY_ROTATION(PLAYER_PED_ID(), vMichaelRotation)
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vMichaelPosition)
				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lazlow")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Lazlow.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					VECTOR vLazlowPosition, vLazlowRotation
					vLazlowPosition = GET_ANIM_INITIAL_OFFSET_POSITION("missfam6ig_7_tattoo","ig_7_loop_lazlow",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)
					vLazlowRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("missfam6ig_7_tattoo","ig_7_loop_lazlow",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)
					SET_ENTITY_ROTATION(psLazlow.PedIndex, vLazlowRotation)
					SET_ENTITY_COORDS_NO_OFFSET(psLazlow.PedIndex, vLazlowPosition)
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_TORSO, 1, 0)
				ENDIF
				
				IF ( bCutsceneSkipped = FALSE )
				
					IF 	NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND NOT IS_PED_INJURED(psLazlow.PedIndex)
				
						IF 	NOT IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
						AND NOT IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					
							IF HAS_SCALEFORM_LOADED()
							AND HAS_MODEL_LOADED(osTattooNeedle.ModelName)
							AND HAS_ANIM_DICT_LOADED("missfam6ig_7_tattoo")
						
								IF HAS_MISSION_OBJECT_BEEN_CREATED(osTattooNeedle)
									IF NOT IS_ENTITY_ATTACHED(osTattooNeedle.ObjectIndex)
										ATTACH_ENTITY_TO_ENTITY(osTattooNeedle.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
									ENDIF
								ENDIF
								
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									TASK_MOVE_NETWORK_ADVANCED_BY_NAME(PLAYER_PED_ID(), "minigame_tattoo_michael_parts", << 324.130, 181.290, 102.600 >>, << 0.0, 0.0, -22.320 >>)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								ENDIF
								
								IF NOT IS_PED_INJURED(psLazlow.PedIndex)
									TASK_MOVE_NETWORK_ADVANCED_BY_NAME(psLazlow.PedIndex, "minigame_tattoo_lazlow_parts",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)					
									FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex, FALSE, TRUE)	//fix for B*1567738, CS_LAZLOW not being in synch
								ENDIF

							ENDIF
					
						ENDIF
						
					ENDIF
				ENDIF
				
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
				IF ( bCutsceneSkipped = TRUE )
					
					IF 	NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND NOT IS_PED_INJURED(psLazlow.PedIndex)
					
						IF 	NOT IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
						AND NOT IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					
							IF HAS_SCALEFORM_LOADED()
							AND HAS_MODEL_LOADED(osTattooNeedle.ModelName)
							AND HAS_ANIM_DICT_LOADED("missfam6ig_7_tattoo")
						
								IF HAS_MISSION_OBJECT_BEEN_CREATED(osTattooNeedle)
									ATTACH_ENTITY_TO_ENTITY(osTattooNeedle.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
								ENDIF
								
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									TASK_MOVE_NETWORK_ADVANCED_BY_NAME(PLAYER_PED_ID(), "minigame_tattoo_michael_parts", << 324.130, 181.290, 102.600 >>, << 0.0, 0.0, -22.320 >>)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								ENDIF
								
								IF NOT IS_PED_INJURED(psLazlow.PedIndex)
									TASK_MOVE_NETWORK_ADVANCED_BY_NAME(psLazlow.PedIndex, "minigame_tattoo_lazlow_parts",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)					
									FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
								ENDIF
								
								IF NOT IS_PED_INJURED(psJimmy.PedIndex)
									SEQUENCE_INDEX JimmySequenceIndex
									CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
									OPEN_SEQUENCE_TASK(JimmySequenceIndex)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, psLazlow.PedIndex)
										TASK_PLAY_ANIM(NULL, "missfam6ig_7_tattoo", "ig_7_idle_02_jimmy", WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)				
									CLOSE_SEQUENCE_TASK(JimmySequenceIndex)
									TASK_PERFORM_SEQUENCE(psJimmy.PedIndex, JimmySequenceIndex)
									CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psJimmy.PedIndex, TRUE)
								ENDIF
								
								IF NOT IS_PED_INJURED(psTracey.PedIndex)
									TASK_PLAY_ANIM(psTracey.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_01_tracey", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(psTracey.PedIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTracey.PedIndex, TRUE)
								ENDIF
								
								iStageProgress++

							ENDIF
						ENDIF
					ENDIF

				ELSE
				
					IF 	NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND NOT IS_PED_INJURED(psLazlow.PedIndex)
				
						IF 	IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
						AND IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
						
							iStageProgress++
						
						ENDIF
					ENDIF
				ENDIF

			ELSE

				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
		
		CASE 3
		
			SET_MODEL_AS_NO_LONGER_NEEDED(osTattooNeedle.ModelName)

			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
			//don't fade in here, wait with fade in the next stage
			//this should fix issues like B*2079140 where move task was given but peds don't perform it just yet
			//IF IS_SCREEN_FADED_OUT()
			//	DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			//ENDIF
											
			RETURN TRUE
		
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_TATTOO_STAGES()

	IF HAS_SOUND_FINISHED(iTattooSoundID)			
		PLAY_SOUND_FROM_ENTITY(iTattooSoundID, "Tattoo", osTattooNeedle.ObjectIndex, "FAMILY_6_SOUNDS")	
	ENDIF

	SWITCH eTattooingStage
	
		CASE TATTOOING_IDLE

			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

				IF ( iConversationTimer != 0 )
					IF GET_GAME_TIMER() - iConversationTimer > 0
						IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_IDLET_3")
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_IDLET", CONV_PRIORITY_MEDIUM)
								iConversationCounter++
								iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(9000, 12000)
								IF ( iConversationCounter >= 3 )
									SET_LABEL_AS_TRIGGERED("FAM6_IDLET_3", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ( iStruggleConversationTimer != 0 )
					IF GET_GAME_TIMER() - iStruggleConversationTimer > 0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_STRUG", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
								iStruggleConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 6000)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE TATTOOING_CHEST	

			IF fTattooPosition < 0.95
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
					IF ( iConversationTimer != 0 )
						IF GET_GAME_TIMER() - iConversationTimer > 0
						
							SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
								CASE 0
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATC_3")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TATC", CONV_PRIORITY_MEDIUM)
											iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											iIdleConversationcounter++
											IF ( iIdleConversationcounter >= 3 )
												SET_LABEL_AS_TRIGGERED("FAM6_TATC_3", TRUE)
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 1
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATA_14")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TATA", CONV_PRIORITY_MEDIUM)
											iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											iIdle1Conversationcounter++
											IF ( iIdle1Conversationcounter >= 14 )
												SET_LABEL_AS_TRIGGERED("FAM6_TATA_14", TRUE)
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 2
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATM_8")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TATM", CONV_PRIORITY_MEDIUM)
											iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											iIdle2Conversationcounter++
											IF ( iIdle2Conversationcounter >= 8 )
												SET_LABEL_AS_TRIGGERED("FAM6_TATM_8", TRUE)
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 3
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_REACT_3")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_REACT", CONV_PRIORITY_MEDIUM)
											iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											iIdle3Conversationcounter++
											IF ( iIdle3Conversationcounter >= 3 )
												SET_LABEL_AS_TRIGGERED("FAM6_REACT_3", TRUE)
											ENDIF
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						
						ENDIF
					ENDIF
					
					IF ( iStruggleConversationTimer != 0 )
						IF GET_GAME_TIMER() - iStruggleConversationTimer > 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_STRUG", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
									iStruggleConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 6000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF	
			ENDIF
			
			SWITCH iTattooCameraProgress
				CASE 0
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
							bScaleformDrawing = TRUE
							PLAY_CAM_ANIM(AnimatedCamera, "front_cam", "missfam6ig_7_tattoo", << 324.130, 181.290, 102.600 >>, << 0, 0, -22.320 >>, CAF_LOOPING)					
							iTattooCameraProgress++
						ENDIF
					ENDIF
				BREAK				
				CASE 1
					IF fTattooPosition > 0.96
					
						//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//ENDIF
						
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TATFIN", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("FAM6_TATFIN", TRUE)
								iTattooCameraProgress++
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE TATTOOING_BACK
			
			IF fTattooPosition < 0.95
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
					IF ( iConversationTimer != 0 )
						IF GET_GAME_TIMER() - iConversationTimer > 0
						
							SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
								CASE 0
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATB_4")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TATB", CONV_PRIORITY_MEDIUM)
											iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											iIdleConversationcounter++
											IF ( iIdleConversationcounter >= 4 )
												SET_LABEL_AS_TRIGGERED("FAM6_TATB_4", TRUE)
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 1
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATA_14")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TATA", CONV_PRIORITY_MEDIUM)
											iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											iIdle1Conversationcounter++
											IF ( iIdle1Conversationcounter >= 14 )
												SET_LABEL_AS_TRIGGERED("FAM6_TATA_14", TRUE)
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 2
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATM_8")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TATM", CONV_PRIORITY_MEDIUM)
											iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											iIdle2Conversationcounter++
											IF ( iIdle2Conversationcounter >= 8 )
												SET_LABEL_AS_TRIGGERED("FAM6_TATM_8", TRUE)
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 3
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_REACT_3")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_REACT", CONV_PRIORITY_MEDIUM)
											iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											iIdle3Conversationcounter++
											IF ( iIdle3Conversationcounter >= 3 )
												SET_LABEL_AS_TRIGGERED("FAM6_REACT_3", TRUE)
											ENDIF
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						
						ENDIF
					ENDIF
					
					IF ( iStruggleConversationTimer != 0 )
						IF GET_GAME_TIMER() - iStruggleConversationTimer > 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_STRUG", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
									iStruggleConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 6000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			SWITCH iTattooCameraProgress
				CASE 0
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF 	IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
							bScaleformDrawing = TRUE
							PLAY_CAM_ANIM(AnimatedCamera, "back_cam", "missfam6ig_7_tattoo", << 324.130, 181.290, 102.600 >>, << 0.0, 0.0, -22.320 >>, CAF_LOOPING)			
							iTattooCameraProgress++
						ENDIF
					ENDIF
				BREAK				
				CASE 1
					IF fTattooPosition > 0.96
					
						//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//ENDIF
					
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_TATFIN", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("FAM6_TATFIN", TRUE)
								iTattooCameraProgress++
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC CHEST_DRAWING(SCALEFORM_INDEX &localMov, INT &iScaleformProgress)

	switch iScaleformProgress
		case 0
		bTattooInputReady = false
			if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(SPACER))
			   	END_SCALEFORM_MOVIE_METHOD()
				iScaleformProgress++
			endif							
		break
		case 1
			BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_LEFT))
			END_SCALEFORM_MOVIE_METHOD()			
			BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DOWN))
	    	END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_RIGHT))
			END_SCALEFORM_MOVIE_METHOD()			
			iScaleformProgress++			
		break		
		case 2			
			iScaleformProgress++			
		break
		case 3	
			eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_LEFT
			bTattooInputReady = true
			iScaleformProgress++	
		break
		case 4					
			if fTattooPosition >= 0.066	
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				AND IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF 	IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)					
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "PartOne")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "PartOne")
						IF BEGIN_SCALEFORM_MOVIE_METHOD(localMov, "ADD_BUTTON")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_UP))
							END_SCALEFORM_MOVIE_METHOD()
							eTattooDirection = RIGHT_STICK_DOWN	
							fTattooSpeed = 0.005					
							fTattooPosition = 0.066	
							bTattooInputReady = TRUE
							iScaleformProgress++	
						ELSE
							bTattooInputReady = FALSE
						ENDIF
					ENDIF
				ENDIF			
			ENDIF
		BREAK
		case 5
			if fTattooPosition >= 0.133	
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_UP_RIGHT))
					END_SCALEFORM_MOVIE_METHOD()
					eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_RIGHT
					fTattooSpeed = 0.005
					fTattooPosition = 0.133	
					bTattooInputReady = true
					iScaleformProgress++	
				else	
					bTattooInputReady = false
				endif
			endif
		break
		case 6 
			if fTattooPosition >= 0.2
				bTattooInputReady = false
				fTattooPosition = 0.2	
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)						
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "PartTwo")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "PartTwo")
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
							SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,0,1)	
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_RIGHT))
							END_SCALEFORM_MOVIE_METHOD()
							eTattooDirection = RIGHT_STICK_UP
							fTattooSpeed = 0.005
							fTattooPosition = 0.2
							bTattooInputReady = true
							iScaleformProgress++	
						else
							bTattooInputReady = false
						endif
					endif
				endif
			endif
		break
		case 7			
			if fTattooPosition >= 0.4
				bTattooInputReady = false
				fTattooPosition = 0.4	
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)						
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "PartThree")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "PartThree")
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")							
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_RIGHT))
							END_SCALEFORM_MOVIE_METHOD()
							fTattooSpeed = 0.005
							fTattooPosition = 0.4
							iScaleformProgress++
							eTattooDirection = RIGHT_STICK_DIAGONAL_UP_RIGHT	
							SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,0,2)
							bTattooInputReady = true
						else
							bTattooInputReady = false
						endif
					endif
				endif
			endif
		break
		case 8
			if fTattooPosition >= 0.466			
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DOWN))
					END_SCALEFORM_MOVIE_METHOD()
					fTattooSpeed = 0.005
					fTattooPosition = 0.466	
					eTattooDirection = RIGHT_STICK_RIGHT
					bTattooInputReady = true
					iScaleformProgress++	
				else
					bTattooInputReady = false
				endif
			endif									
		break
		case 9
			if fTattooPosition >= 0.533					
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_RIGHT))
					END_SCALEFORM_MOVIE_METHOD()
					eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_RIGHT	
					fTattooSpeed = 0.005
					fTattooPosition = 0.533
					bTattooInputReady = true
					iScaleformProgress++	
				else
					bTattooInputReady = false
				endif	
				
			endif
		break
		case 10
			if fTattooPosition >= 0.6
				fTattooPosition = 0.6
				bTattooInputReady = false
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "PartFour")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "PartFour")	
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
							SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,0,3)
							eTattooDirection = RIGHT_STICK_DOWN	
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DOWN))
							END_SCALEFORM_MOVIE_METHOD()
							fTattooSpeed = 0.005
							fTattooPosition = 0.6
							bTattooInputReady = true
							iScaleformProgress++	
						else
							bTattooInputReady = false
						endif
					endif
				endif
			endif	
		break
		case 11
			if fTattooPosition >= 0.8
				fTattooPosition = 0.8
				bTattooInputReady = false		
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "PartFive")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "PartFive")	
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_LEFT))
						   END_SCALEFORM_MOVIE_METHOD()
							SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,0,4)
							eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_RIGHT								
							fTattooSpeed = 0.005
							fTattooPosition = 0.8
							iScaleformProgress++		
							bTattooInputReady = true
						else
							bTattooInputReady = false
						endif				
					endif
				endif
			endif
		break
		case 12
			if fTattooPosition >= 0.866			
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(SPACER))
				   END_SCALEFORM_MOVIE_METHOD()
					eTattooDirection = RIGHT_STICK_DOWN		
					fTattooSpeed = 0.005
					fTattooPosition = 0.866
					iScaleformProgress++	
					bTattooInputReady = true
				else
					bTattooInputReady = false
				endif
			endif
		break
		case 13
			if fTattooPosition >= 0.933		
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(SPACER))
				   END_SCALEFORM_MOVIE_METHOD()
				  	eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_LEFT
					bTattooInputReady = true
					fTattooSpeed = 0.005
					fTattooPosition = 0.933	
					iScaleformProgress++
				else
					bTattooInputReady = false
				ENDIF
			ENDIF	
		BREAK
		CASE 14
			IF fTattooPosition >= 0.99		
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SPACER))
				   	END_SCALEFORM_MOVIE_METHOD()
					fTattooSpeed = 0.005
					fTattooPosition = 1
					iScaleformProgress = 0
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,0,5)
					eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_LEFT
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC BACK_DRAWING(SCALEFORM_INDEX &localMov, INT &iScaleformProgress)
	switch iScaleformProgress
		case 0
		//scale form
		bTattooInputReady = false
			if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(SPACER))
			   	END_SCALEFORM_MOVIE_METHOD()				
				iScaleformProgress++
			endif							
		break
		case 1
			BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_LEFT))
			END_SCALEFORM_MOVIE_METHOD()			
			BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DOWN))
	    	END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_RIGHT))
			END_SCALEFORM_MOVIE_METHOD()			
			iScaleformProgress++			
		break		
		case 2			
			iScaleformProgress++			
		break
		case 3	
			eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_LEFT
			bTattooInputReady = true
			iScaleformProgress++	
		break
		case 4	
			if fTattooPosition >= 0.066	
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)					
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "BackPartOne")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "BackPartOne")
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_UP))
							END_SCALEFORM_MOVIE_METHOD()
							eTattooDirection = RIGHT_STICK_DOWN	
							fTattooSpeed = 0.005					
							fTattooPosition = 0.066	
							bTattooInputReady = true
							iScaleformProgress++	
						else
							bTattooInputReady = false
						endif
					endif
				endif			
			endif
		break
		case 5
			if fTattooPosition >= 0.133	
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_UP_RIGHT))
					END_SCALEFORM_MOVIE_METHOD()
					eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_RIGHT
					fTattooSpeed = 0.005
					fTattooPosition = 0.133	
					bTattooInputReady = true
					iScaleformProgress++	
				else	
					bTattooInputReady = false
				endif
			endif
		break
		case 6	
			if fTattooPosition >= 0.2
				bTattooInputReady = false
				fTattooPosition = 0.2	
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)						
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "BackPartTwo")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "BackPartTwo")
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
							SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,1,1)	
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_RIGHT))
							END_SCALEFORM_MOVIE_METHOD()
							eTattooDirection = RIGHT_STICK_UP
							fTattooSpeed = 0.005
							fTattooPosition = 0.2
							bTattooInputReady = true
							iScaleformProgress++	
						else
							bTattooInputReady = false
						endif
					endif
				endif
			endif
		break
		case 7			
			if fTattooPosition >= 0.4
				bTattooInputReady = false
				fTattooPosition = 0.4	
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)						
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "BackPartThree")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "BackPartThree")
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")							
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_RIGHT))
							END_SCALEFORM_MOVIE_METHOD()
							fTattooSpeed = 0.005
							fTattooPosition = 0.4
							iScaleformProgress++
							eTattooDirection = RIGHT_STICK_DIAGONAL_UP_RIGHT	
							SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,1,2)
							bTattooInputReady = true
						else
							bTattooInputReady = false
						endif	
					endif
				endif
			endif
		break
		case 8
			if fTattooPosition >= 0.466			
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DOWN))
					END_SCALEFORM_MOVIE_METHOD()
					fTattooSpeed = 0.005
					fTattooPosition = 0.466	
					eTattooDirection = RIGHT_STICK_RIGHT
					bTattooInputReady = true
					iScaleformProgress++	
				else
					bTattooInputReady = false
				endif
			endif									
		break
		case 9
			if fTattooPosition >= 0.533					
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_RIGHT))
					END_SCALEFORM_MOVIE_METHOD()
					eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_RIGHT	
					fTattooSpeed = 0.005
					fTattooPosition = 0.533
					bTattooInputReady = true
					iScaleformProgress++	
				else
					bTattooInputReady = false
				endif	
				
			endif
		break
		case 10
			if fTattooPosition >= 0.6
				fTattooPosition = 0.6
				bTattooInputReady = false
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "BackPartFour")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "BackPartFour")	
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
							SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,1,3)
							eTattooDirection = RIGHT_STICK_DOWN	
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DOWN))
							END_SCALEFORM_MOVIE_METHOD()
							fTattooSpeed = 0.005
							fTattooPosition = 0.6
							bTattooInputReady = true
							iScaleformProgress++	
						else
							bTattooInputReady = false
						endif
					endif
				endif
			endif	
		break
		case 11
			if fTattooPosition >= 0.8
				fTattooPosition = 0.8
				bTattooInputReady = false		
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				and IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					and IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "BackPartFive")
						REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "BackPartFive")	
						if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(RIGHT_STICK_DIAGONAL_DOWN_LEFT))
						   END_SCALEFORM_MOVIE_METHOD()
							SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,1,4)
							eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_RIGHT								
							fTattooSpeed = 0.005
							fTattooPosition = 0.8
							iScaleformProgress++		
							bTattooInputReady = true
						else
							bTattooInputReady = false
						endif				
					endif
				endif
			endif
		break
		case 12
			if fTattooPosition >= 0.866			
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(SPACER))
				   END_SCALEFORM_MOVIE_METHOD()
					eTattooDirection = RIGHT_STICK_DOWN		
					fTattooSpeed = 0.005
					fTattooPosition = 0.866
					iScaleformProgress++	
					bTattooInputReady = true
				else
					bTattooInputReady = false
				endif
			endif
		break
		case 13
			if fTattooPosition >= 0.933		
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(SPACER))
				   END_SCALEFORM_MOVIE_METHOD()
				  	eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_LEFT
					bTattooInputReady = true
					fTattooSpeed = 0.005
					fTattooPosition = 0.933	
					iScaleformProgress++
				else
					bTattooInputReady = false
				endif
			endif	
		break
		case 14
			if fTattooPosition >= 0.99		
				if BEGIN_SCALEFORM_MOVIE_METHOD(localMov,"ADD_BUTTON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(enum_to_int(SPACER))
				   	END_SCALEFORM_MOVIE_METHOD()
					fTattooSpeed = 0.005
					fTattooPosition = 1
					iScaleformProgress = 0
					SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex,PED_COMP_DECL,1,5)
					eTattooDirection = RIGHT_STICK_DIAGONAL_DOWN_LEFT
				endif
			endif
		break
	endswitch
endproc

PROC MANAGE_TATTOO_MOVE()

	SCALEFORM_INDEX	mov_local
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		mov_local = mov_kb
	
	ELSE
		mov_local = mov
	
	ENDIF

	IF bScaleformDrawing
	
		INT 	i_right_stick_x, i_right_stick_y, angle
		FLOAT	increase_val, Max_val
		
		angle 			= GET_STICK_ROTATION_ANGLE(STICK_RIGHT)
		i_right_stick_x = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 127)
		i_right_stick_y	= FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 127)
		
		
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			
		//DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.1, "NUMBER", iRightX )
		//DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.2, "NUMBER", iRightY )
			
			// Make input 'sticky' so it doesn't auto-centre.
			IF ( i_right_stick_x = 0 AND i_right_stick_y = 0 )
			
				i_right_stick_x = iStickXPrev
				i_right_stick_y = iStickYPrev
			
			ENDIF
			
			iStickXPrev = i_right_stick_x
			iStickYPrev = i_right_stick_y
		
		ENDIF
	
		IF HAS_SCALEFORM_LOADED()
		
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
			SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)
			
			IF ( GET_IS_WIDESCREEN() = FALSE )
			OR ( GET_PROFILE_SETTING(PROFILE_DISPLAY_LANGUAGE) = ENUM_TO_INT(LANGUAGE_KOREAN) 	)
			OR ( GET_PROFILE_SETTING(PROFILE_DISPLAY_LANGUAGE) = ENUM_TO_INT(LANGUAGE_CHINESE) 	)
			OR ( GET_PROFILE_SETTING(PROFILE_DISPLAY_LANGUAGE) = ENUM_TO_INT(LANGUAGE_JAPANESE) )
			OR ( GET_PROFILE_SETTING(PROFILE_DISPLAY_LANGUAGE) = ENUM_TO_INT(LANGUAGE_CHINESE_SIMPLIFIED) )
				TATTOO_BUTTONS_Y = 0.745
			ENDIF
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				DRAW_SCALEFORM_MOVIE(mov_local, TATTOO_BUTTONS_X, TATTOO_BUTTONS_Y,
									 	TATTOO_BUTTONS_WIDTH * TATTOO_BUTTONS_SCALE, TATTOO_BUTTONS_HEIGHT * TATTOO_BUTTONS_SCALE, 100, 100, 100, 255)
			ENDIF
			
			IF eTattooingStage = TATTOOING_CHEST
				fTattooSpeed = 0.005
				CHEST_DRAWING(mov, iScaleformProgressPad)
				
				IF IS_PC_VERSION()
					CHEST_DRAWING(mov_kb, iScaleformProgressKBM)
				ENDIF
			ELIF eTattooingStage = TATTOOING_BACK
				fTattooSpeed =  0.005
				BACK_DRAWING(mov, iScaleformProgressPad)
				IF IS_PC_VERSION()
					BACK_DRAWING(mov_kb, iScaleformProgressKBM)
				ENDIF
			ENDIF
			
			IF bTattooInputReady	
		
				IF NOT IS_STICK_IN_DEAD_ZONE(STICK_RIGHT)
		
					switch eTattooDirection
						case RIGHT_STICK_UP
							if i_right_stick_x >= -40 
							and i_right_stick_x <= 40
								max_val = -127
								increase_val = clamp(TO_FLOAT(i_right_stick_y),max_val,0)
							endif
						break
						
						case RIGHT_STICK_DIAGONAL_UP_RIGHT
							if i_right_stick_x >= 45 
							and i_right_stick_y <= -45									
								max_val = 192
								increase_val = clamp(TO_FLOAT((i_right_stick_x + -i_right_stick_y)),0,max_val)
							endif				
						break
						
						case RIGHT_STICK_RIGHT
							if i_right_stick_y >= -40 
							and i_right_stick_y <= 40
								max_val = 127
								increase_val = clamp(TO_FLOAT(i_right_stick_x),0,max_val)
							endif
						break
						
						case RIGHT_STICK_DIAGONAL_DOWN_RIGHT
							if i_right_stick_x >= 45
							and i_right_stick_y >= 45 								
								max_val = 192
								increase_val = clamp(TO_FLOAT((i_right_stick_x + i_right_stick_y)),0,max_val)
							endif
						break
						
						case RIGHT_STICK_DOWN
							if i_right_stick_x >= -40 
							and i_right_stick_x <= 40
								max_val = 127
								increase_val = clamp(TO_FLOAT(i_right_stick_y),0,max_val)
							endif
						break
						case RIGHT_STICK_DIAGONAL_DOWN_LEFT
							if i_right_stick_x <= -45 
							and i_right_stick_y >= 45									
								max_val = 192
								increase_val = clamp(TO_FLOAT((-i_right_stick_x + i_right_stick_y)),0,max_val)
							endif
						break
						case RIGHT_STICK_LEFT
							if i_right_stick_y >= -40 
							and i_right_stick_y <= 40
								max_val = -127
								increase_val = clamp(TO_FLOAT(i_right_stick_x),max_val,0)	
							endif
						break
						case RIGHT_STICK_DIAGONAL_UP_LEFT
							if i_right_stick_x <= -45 
							and i_right_stick_y <= -45									
								max_val = 192
								increase_val = clamp(TO_FLOAT((-i_right_stick_x + -i_right_stick_y)),0,max_val)
							endif
						break
					endswitch
					
				ENDIF
				
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(increase_val), << 0.5, 0.5, 0.0 >>)
				#ENDIF
				
				IF NOT IS_STICK_IN_DEAD_ZONE(STICK_RIGHT)
					BEGIN_SCALEFORM_MOVIE_METHOD(mov_local, "SET_STICK_POINTER_ANGLE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(angle)								//pass in the current stick angle
						IF  ABSF(increase_val) > 0.0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_YOGA))	//pass in the colour when stick is in position
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))	//reset the color when stick is not in position
						ENDIF
				   	END_SCALEFORM_MOVIE_METHOD()
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(mov_local, "SET_STICK_POINTER_ANGLE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)								//reset the stick and set default color
				   	END_SCALEFORM_MOVIE_METHOD()	
				ENDIF
				

				//work out percentage * speed
				float increasePercentage = (increase_val / max_val)
				FLOAT f_vel_y = increasePercentage * fTattooSpeed
				
				IF NOT HAS_SOUND_FINISHED(iTattooSoundID)
					SET_VARIABLE_ON_SOUND(iTattooSoundID,"DoTattoo",increasePercentage)
				ENDIF
				
				IF increasePercentage <= 0
					SET_CONTROL_SHAKE(PLAYER_CONTROL,100,50)
				ELSE			
					SET_CONTROL_SHAKE(PLAYER_CONTROL,250,ROUND(256*increasePercentage))
				ENDIF
								
				fTattooPosition = clamp((fTattooPosition + f_vel_y),0,1)	
				
			ELSE
			
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 50)
				
				IF NOT HAS_SOUND_FINISHED(iTattooSoundID)
					SET_VARIABLE_ON_SOUND(iTattooSoundID, "DoTattoo", 0)
				ENDIF
				
			ENDIF
			
		ENDIF

	ENDIF
	
ENDPROC

FUNC BOOL IS_MISSION_STAGE_TATTOO_LAZLOW_COMPLETED(INT &iStageProgress)

	MANAGE_TATTOO_STAGES()

	SWITCH iStageProgress
		
		CASE 0
		
			bChestTattoo	= FALSE
			eTattooingStage	= TATTOOING_IDLE
		
			IF 	NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(psLazlow.PedIndex) 
		
				IF 	NOT IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				AND NOT IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
				
					REQUEST_MODEL(osTattooNeedle.ModelName)
					REQUEST_ANIM_DICT("missfam6ig_7_tattoo")
					
					mov = REQUEST_SCALEFORM_MOVIE("tattoo_buttons")
					
					IF IS_PC_VERSION()
						mov_kb = REQUEST_SCALEFORM_MOVIE("tattoo_keys")
					ENDIF
					
					IF HAS_SCALEFORM_LOADED()
					AND HAS_MODEL_LOADED(osTattooNeedle.ModelName)
					AND HAS_ANIM_DICT_LOADED("missfam6ig_7_tattoo") 
					
						IF HAS_MISSION_OBJECT_BEEN_CREATED(osTattooNeedle)
							ATTACH_ENTITY_TO_ENTITY(osTattooNeedle.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						ENDIF
						
						TASK_MOVE_NETWORK_ADVANCED_BY_NAME(PLAYER_PED_ID(), "minigame_tattoo_michael_parts", << 324.130, 181.290, 102.600 >>, << 0.0, 0.0, -22.320 >>)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						
						TASK_MOVE_NETWORK_ADVANCED_BY_NAME(psLazlow.PedIndex, "minigame_tattoo_lazlow_parts",<< 324.130, 181.290, 102.600 >>,<< 0, 0, -22.320 >>)					
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
						
						IF NOT DOES_CAM_EXIST(ScriptedCamera)
							ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)	
							SET_CAM_PARAMS(ScriptedCamera, << 326.1551, 179.2442, 104.5627 >>, << -36.8795, 13.4340, 33.8400 >>, 52.0)
							SHAKE_CAM(ScriptedCamera,"HAND_SHAKE",0.5)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ENDIF
						
						IF NOT DOES_CAM_EXIST(AnimatedCamera)
							AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, FALSE) 
						ENDIF
						
						IF NOT IS_PED_INJURED(psJimmy.PedIndex)
							SEQUENCE_INDEX JimmySequenceIndex
							CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
							OPEN_SEQUENCE_TASK(JimmySequenceIndex)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, psLazlow.PedIndex)
								TASK_PLAY_ANIM(NULL, "missfam6ig_7_tattoo", "ig_7_idle_02_jimmy", WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)				
							CLOSE_SEQUENCE_TASK(JimmySequenceIndex)
							TASK_PERFORM_SEQUENCE(psJimmy.PedIndex, JimmySequenceIndex)
							CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psJimmy.PedIndex, TRUE)
						ENDIF
						
						IF NOT IS_PED_INJURED(psTracey.PedIndex)
							TASK_PLAY_ANIM(psTracey.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_01_tracey", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psTracey.PedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTracey.PedIndex, TRUE)
						ENDIF
						
						SET_MODEL_AS_NO_LONGER_NEEDED(osTattooNeedle.ModelName)
					
					ENDIF
				
				ELSE
					
					iConversationCounter			= 0
					iIdleConversationcounter 		= 0
					iIdle1Conversationcounter 		= 0
					iIdle2Conversationcounter 		= 0
					iStruggleConversationTimer		= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
					iConversationTimer				= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(250, 1000)
					iScaleformProgressPad			= 0
					iScaleformProgressKBM 			= 0
					iTattooCameraProgress			= 0
					fTattooPosition 				= 0.0
					bChestTattoo					= FALSE
					bTattooInputReady 				= FALSE
					eTattooingStage					= TATTOOING_IDLE
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					START_AUDIO_SCENE("FAMILY_6_TATTOO_LAZLOW_MG")
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					bReplayStartEventTriggered = TRUE
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					
					SETUP_PC_CONTROLS()
					iStickXPrev = 0
					iStickYPrev = 0
													
					iStageProgress++
					
				ENDIF
		
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF 	IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TATCH")
					PRINT_HELP_FOREVER("FAM6_TATCH")
					SET_LABEL_AS_TRIGGERED("FAM6_TATCH", TRUE)
				ENDIF
			ENDIF
			
			IF 	NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(psLazlow.PedIndex)
				IF 	IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				AND IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)		
					IF 	IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
				
							REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "back")
							REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "back")
							
							SET_CAM_ACTIVE(AnimatedCamera, TRUE)
							PLAY_CAM_ANIM(AnimatedCamera, "ig_7_back_intro_cam", "missfam6ig_7_tattoo", << 324.130, 181.290, 102.600 >>, << 0.0, 0.0, -22.320 >>)
							
							bChestTattoo			= FALSE
							eTattooingStage 		= TATTOOING_BACK
							g_replay.iReplayInt[0] 	= ENUM_TO_INT(TATTOOING_BACK)	//fix for B*1918478, store Lazlow's tattoo info in a global int for next stage retry
							
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FAM6_FRONT_OR_BACK)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM6_TATTOO_TIME) 
							
							iStageProgress++
						
						ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
						
							REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "front")
							REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "front")
							
							SET_CAM_ACTIVE(AnimatedCamera, TRUE)
							PLAY_CAM_ANIM(AnimatedCamera, "ig_7_front_intro_cam", "missfam6ig_7_tattoo", << 324.130, 181.290, 102.600 >>, << 0.0, 0.0, -22.320 >>)							
							
							bChestTattoo			= TRUE
							eTattooingStage 		= TATTOOING_CHEST
							g_replay.iReplayInt[0] 	= ENUM_TO_INT(TATTOOING_CHEST)	//fix for B*1918478, store Lazlow's tattoo info in a global int for next stage retry
							
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM6_TATTOO_TIME) 
							
							iStageProgress++
						
						ENDIF

					ENDIF
				ENDIF	
			ENDIF
		BREAK
		
		CASE 2
		
			IF 	NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(psLazlow.PedIndex)
				IF 	IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				AND IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)			
					IF 	IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)
					
						iConversationTimer 	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(500, 1500)
					
						iStageProgress++
					
					ENDIF
				ENDIF
			ENDIF
				
		BREAK
		
		CASE 3

			IF 	IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_TAT")
					PRINT_HELP_FOREVER("FAM6_TAT")
					SET_LABEL_AS_TRIGGERED("FAM6_TAT", TRUE)
				ENDIF
			ENDIF
			
			MANAGE_TATTOO_MOVE()
			MANAGE_TATTOO_STAGES()
			
			IF fTattooPosition >= 1.0
			
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP(TRUE)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 4
		
			IF HAS_LABEL_BEEN_TRIGGERED("FAM6_TATFIN")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					IF 	NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND NOT IS_PED_INJURED(psLazlow.PedIndex)

						IF 	IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
						AND IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
							IF 	IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
							AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)
								IF fTattooPosition >= 1.0
								
									REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "success")
									REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(psLazlow.PedIndex, "success")
									
									INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						
									iStageProgress++
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 5
			
			IF 	NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(psLazlow.PedIndex)
				IF 	IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				AND IS_TASK_MOVE_NETWORK_ACTIVE(psLazlow.PedIndex)
					IF 	IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(psLazlow.PedIndex)
						
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF ( bReplayStartEventTriggered = TRUE )
							IF ( bReplayStopEventTriggered = FALSE )
								REPLAY_STOP_EVENT()
								bReplayStopEventTriggered = TRUE
							ENDIF
						ENDIF
						
						CLEANUP_PC_CONTROLS()
						
						RETURN TRUE
						
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
	SWITCH eTattooingStage
		CASE TATTOOING_BACK
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("fam_6_mcs_4", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_5)
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
				SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
				SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
				SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(TRUE, TRUE, FALSE, TRUE, bChestTattoo)
				bCutsceneAssetsRequested = TRUE
			ENDIF
		
		BREAK
		CASE TATTOOING_CHEST
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("fam_6_mcs_4", CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
			SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
			SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
			SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(TRUE, TRUE, FALSE, TRUE, bChestTattoo)
			bCutsceneAssetsRequested = TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_SHOP_3_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			REQUEST_ANIM_DICT("missfam6ig_7_tattoo")
		
			SWITCH eTattooingStage
			
				CASE TATTOOING_IDLE
				CASE TATTOOING_BACK
				
					bChestTattoo = FALSE
					
					IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_mcs_4", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_5) AND HAS_ANIM_DICT_LOADED("missfam6ig_7_tattoo")
				
						IF NOT IS_PED_INJURED(psLazlow.PedIndex)
							REGISTER_ENTITY_FOR_CUTSCENE(psLazlow.PedIndex, "Lazlow", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psLazlow.ModelName)
						ENDIF
						
						IF DOES_ENTITY_EXIST(osTattooNeedle.ObjectIndex)
							
							IF NOT HAS_SOUND_FINISHED(iTattooSoundID)
								STOP_SOUND(iTattooSoundID)
							ENDIF
						
							IF NOT IS_ENTITY_DEAD(osTattooNeedle.ObjectIndex)
								IF IS_ENTITY_ATTACHED(osTattooNeedle.ObjectIndex)
									DETACH_ENTITY(osTattooNeedle.ObjectIndex, FALSE, TRUE)
								ENDIF
								REGISTER_ENTITY_FOR_CUTSCENE(osTattooNeedle.ObjectIndex, "Tattoo_Gun", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, osTattooNeedle.ModelName)
							ENDIF
						ENDIF
										
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						STOP_AUDIO_SCENES()
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						iStageProgress++
					
					ELSE
					
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
							SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
							SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
							SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(TRUE, TRUE, FALSE, TRUE, bChestTattoo)
						ENDIF
					
					ENDIF
				
				BREAK
				
				CASE TATTOOING_CHEST
					
					bChestTattoo = TRUE
				
					IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_mcs_4", CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5) AND HAS_ANIM_DICT_LOADED("missfam6ig_7_tattoo")
				
						IF NOT IS_PED_INJURED(psLazlow.PedIndex)
							REGISTER_ENTITY_FOR_CUTSCENE(psLazlow.PedIndex, "Lazlow", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psLazlow.ModelName)
						ENDIF
						
						IF DOES_ENTITY_EXIST(osTattooNeedle.ObjectIndex)
							
							IF NOT HAS_SOUND_FINISHED(iTattooSoundID)
								STOP_SOUND(iTattooSoundID)
							ENDIF
						
							IF NOT IS_ENTITY_DEAD(osTattooNeedle.ObjectIndex)
								IF IS_ENTITY_ATTACHED(osTattooNeedle.ObjectIndex)
									DETACH_ENTITY(osTattooNeedle.ObjectIndex, FALSE, TRUE)
								ENDIF
								REGISTER_ENTITY_FOR_CUTSCENE(osTattooNeedle.ObjectIndex, "Tattoo_Gun", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, osTattooNeedle.ModelName)
							ENDIF
						ENDIF
										
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						STOP_AUDIO_SCENES()
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						iStageProgress++
					
					ELSE
					
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
							SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
							SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
							SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(TRUE, TRUE, FALSE, TRUE, bChestTattoo)
						ENDIF
					
					ENDIF
				
				BREAK
			
			ENDSWITCH
		
		BREAK
		
		CASE 1
		
			IF IS_CUTSCENE_PLAYING()
			
				CLEANUP_MISSION_OBJECT(osShirt, TRUE)
				CLEANUP_MISSION_OBJECT(osPiercingGun, TRUE)
			
				IF DOES_CAM_EXIST(ScriptedCamera)
					DESTROY_CAM(ScriptedCamera)
				ENDIF

				IF DOES_CAM_EXIST(AnimatedCamera)
					DESTROY_CAM(AnimatedCamera)
				ENDIF
				
				//give tasks to jimmy and tracey as they are not part of this cutscene
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					WARP_PED(psTracey.PedIndex, <<321.58142, 181.34822, 102.58653>>, -125.61, FALSE, FALSE, FALSE)
					TASK_PLAY_ANIM(psTracey.PedIndex, "missfam6ig_7_tattoo", "ig_7_idle_01_tracey", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
					TASK_LOOK_AT_ENTITY(psTracey.PedIndex, psLazlow.PedIndex, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(psTracey.PedIndex)
				ENDIF
				
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					WARP_PED(psJimmy.PedIndex, <<321.65167, 182.52638, 102.58653>>, -115.96, FALSE, FALSE, FALSE)
					SEQUENCE_INDEX JimmySequenceIndex
					CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
					OPEN_SEQUENCE_TASK(JimmySequenceIndex)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, psLazlow.PedIndex)
						TASK_PLAY_ANIM(NULL, "missfam6ig_7_tattoo", "ig_7_idle_02_jimmy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)				
					CLOSE_SEQUENCE_TASK(JimmySequenceIndex)
					TASK_PERFORM_SEQUENCE(psJimmy.PedIndex, JimmySequenceIndex)
					CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
					TASK_LOOK_AT_COORD(psJimmy.PedIndex, <<324.57983, 178.60126, 102.59248>>, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			REQUEST_PTFX_ASSET()
			REQUEST_MODEL(osScissors.ModelName)
			REQUEST_MODEL(osPonyTail.ModelName)
			REQUEST_ANIM_DICT("missfam6ig_8_ponytail")
			REQUEST_SCRIPT_AUDIO_BANK("FAM6_PONYTAIL")
			
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				SET_CAM_PARAMS(ScriptedCamera, <<323.9763, 177.9908, 102.7844>>, <<23.5296, 13.8430, -15.5478>>, 45.0)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				REPLAY_STOP_EVENT()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lazlow")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Lazlow.")
				#ENDIF
			
			ENDIF
						
			IF HAS_CUTSCENE_FINISHED() OR CAN_SET_EXIT_STATE_FOR_CAMERA() //fix for lighting pop when cutscene ends
			
				IF 	HAS_PTFX_ASSET_LOADED()
				AND	HAS_MODEL_LOADED(osScissors.ModelName)
				AND REQUEST_SCRIPT_AUDIO_BANK("FAM6_PONYTAIL")
				AND HAS_ANIM_DICT_LOADED("missfam6ig_8_ponytail")
			
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScissorsSceneID)
					
						iScissorsSceneID = CREATE_SYNCHRONIZED_SCENE(vScissorsScenePosition, vScissorsSceneRotation)

						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iScissorsSceneID, "missfam6ig_8_ponytail", "ig_7_loop_michael", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						ENDIF
						
						IF NOT IS_PED_INJURED(psLazlow.PedIndex)
							TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iScissorsSceneID, "missfam6ig_8_ponytail", "ig_7_loop_lazlow", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
						ENDIF
						
						IF HAS_MISSION_OBJECT_BEEN_CREATED(osScissors)
							IF DOES_ENTITY_EXIST(osScissors.ObjectIndex) AND NOT IS_ENTITY_DEAD(osScissors.ObjectIndex)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(osScissors.ObjectIndex, iScissorsSceneID, "ig_7_loop_scissors", "missfam6ig_8_ponytail", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
							ENDIF
						ENDIF
						
						IF NOT DOES_CAM_EXIST(AnimatedCamera)
							AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
							PLAY_CAM_ANIM(AnimatedCamera, "ig_7_Loop_cam", "missfam6ig_8_ponytail", vScissorsScenePosition, vScissorsSceneRotation, CAF_LOOPING)
						ENDIF
					
					ELSE
					
						SET_MODEL_AS_NO_LONGER_NEEDED(osScissors.ModelName)
					
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
						ENDIF
														
						RETURN TRUE
					
					ENDIF
					
				ENDIF

			ELSE

				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUT_LAZLOW_HAIR_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScissorsSceneID)
			
				REQUEST_PTFX_ASSET()
				REQUEST_MODEL(osScissors.ModelName)
				REQUEST_MODEL(osPonyTail.ModelName)
				REQUEST_ANIM_DICT("missfam6ig_8_ponytail")
				REQUEST_SCRIPT_AUDIO_BANK("FAM6_PONYTAIL")
			
				IF 	HAS_PTFX_ASSET_LOADED()
				AND	HAS_MODEL_LOADED(osScissors.ModelName)
				AND HAS_MODEL_LOADED(osPonyTail.ModelName)
				AND	REQUEST_SCRIPT_AUDIO_BANK("FAM6_PONYTAIL")
				AND HAS_ANIM_DICT_LOADED("missfam6ig_8_ponytail")
				
					iScissorsSceneID = CREATE_SYNCHRONIZED_SCENE(vScissorsScenePosition, vScissorsSceneRotation)

					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iScissorsSceneID, "missfam6ig_8_ponytail", "ig_7_loop_michael", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					IF NOT IS_PED_INJURED(psLazlow.PedIndex)
						TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iScissorsSceneID, "missfam6ig_8_ponytail", "ig_7_loop_lazlow", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
					ENDIF
					
					IF HAS_MISSION_OBJECT_BEEN_CREATED(osScissors)
						IF DOES_ENTITY_EXIST(osScissors.ObjectIndex) AND NOT IS_ENTITY_DEAD(osScissors.ObjectIndex)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(osScissors.ObjectIndex, iScissorsSceneID, "ig_7_loop_scissors", "missfam6ig_8_ponytail", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
						ENDIF
					ENDIF
					
					IF NOT DOES_CAM_EXIST(AnimatedCamera)
						AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						PLAY_CAM_ANIM(AnimatedCamera, "ig_7_Loop_cam", "missfam6ig_8_ponytail", vScissorsScenePosition, vScissorsSceneRotation, CAF_LOOPING)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(osScissors.ModelName)
					
				ENDIF
			
			ELSE
			
				iConversationTimer 			= 0
				iConversationCounter		= 0
				iStruggleConversationTimer	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
			
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
				START_AUDIO_SCENE("FAMILY_6_HAIRCUT_LAZLOW_MG")
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
				bReplayStartEventTriggered = TRUE
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
								
				iStageProgress++
			
			ENDIF
		
		BREAK
		
		CASE 1

			IF 	IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_SCIS")
					PRINT_HELP_FOREVER("FAM6_SCIS")
					SET_LABEL_AS_TRIGGERED("FAM6_SCIS", TRUE)
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScissorsSceneID)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_IDLES_4")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF GET_GAME_TIMER() - iConversationTimer > 0
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_IDLES", CONV_PRIORITY_MEDIUM)
								iConversationCounter++
								iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(9000, 12000)
								IF ( iConversationCounter >= 4 )
									SET_LABEL_AS_TRIGGERED("FAM6_IDLES_4", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ( iStruggleConversationTimer != 0 )
					IF GET_GAME_TIMER() - iStruggleConversationTimer > 0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_STRUG", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
								iStruggleConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 6000)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScissorsSceneID)
			OR ( IS_SYNCHRONIZED_SCENE_RUNNING(iScissorsSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iScissorsSceneID) >= 1.0 )
			
				iScissorsSceneID = CREATE_SYNCHRONIZED_SCENE(vScissorsScenePosition, vScissorsSceneRotation)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iScissorsSceneID, "missfam6ig_8_ponytail", "ig_7_loop_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iScissorsSceneID, "missfam6ig_8_ponytail", "ig_7_loop_lazlow", SLOW_BLEND_IN, SLOW_BLEND_OUT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
				ENDIF

				IF DOES_ENTITY_EXIST(osScissors.ObjectIndex) AND NOT IS_ENTITY_DEAD(osScissors.ObjectIndex)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(osScissors.ObjectIndex, iScissorsSceneID, "ig_7_loop_scissors", "missfam6ig_8_ponytail", SLOW_BLEND_IN, SLOW_BLEND_OUT)
				ENDIF
				
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScissorsSceneID) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)		
			
				iScissorsSceneID = CREATE_SYNCHRONIZED_SCENE(vScissorsScenePosition, vScissorsSceneRotation)
				
				IF HAS_MISSION_OBJECT_BEEN_CREATED(osPonyTail, FALSE)
					IF DOES_ENTITY_EXIST(osPonyTail.ObjectIndex) AND NOT IS_ENTITY_DEAD(osPonyTail.ObjectIndex)
						ATTACH_ENTITY_TO_ENTITY(osPonyTail.ObjectIndex, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
					ENDIF
				ENDIF
				
				PLAY_CAM_ANIM(AnimatedCamera, "ig_7_Outro_Cam","missfam6ig_8_ponytail", vScissorsScenePosition, vScissorsSceneRotation)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iScissorsSceneID, "missfam6ig_8_ponytail", "ig_7_outro_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT)
				ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					TASK_SYNCHRONIZED_SCENE(psLazlow.PedIndex, iScissorsSceneID, "missfam6ig_8_ponytail", "ig_7_outro_lazlow", SLOW_BLEND_IN, SLOW_BLEND_OUT)
				ENDIF
				
				IF DOES_ENTITY_EXIST(osScissors.ObjectIndex) AND NOT IS_ENTITY_DEAD(osScissors.ObjectIndex)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(osScissors.ObjectIndex,iScissorsSceneID, "ig_7_outro_scissors", "missfam6ig_8_ponytail", SLOW_BLEND_IN, SLOW_BLEND_OUT)
				ENDIF
				
				CLEAR_HELP()
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScissorsSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iScissorsSceneID) >= 0.05
			
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 150, 120)
				
				STOP_CURRENT_PLAYING_AMBIENT_SPEECH(psLazlow.PedIndex)
				
				PLAY_SOUND_FROM_ENTITY(-1, "Snip_Ponytail",osScissors.ObjectIndex, "FAMILY_6_SOUNDS")
				
				SET_PED_COMPONENT_VARIATION(psLazlow.PedIndex, PED_COMP_HAIR, 1, 0)
				
				ptfxHair = START_PARTICLE_FX_LOOPED_ON_PED_BONE("cs_fam6_hair_snip", PLAYER_PED_ID(), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, BONETAG_PH_L_HAND)
				
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ENDIF
			
		BREAK
		
		CASE 3
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScissorsSceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iScissorsSceneID) >= 0.85
				OR ( GET_SYNCHRONIZED_SCENE_PHASE(iScissorsSceneID) >= 0.45 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() )
					IF ( bCutsceneAssetsRequested = TRUE )
					
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF ( bReplayStartEventTriggered = TRUE )
							IF ( bReplayStopEventTriggered = FALSE )
								REPLAY_STOP_EVENT()
								bReplayStopEventTriggered = TRUE
							ENDIF
						ENDIF
					
						RETURN TRUE	
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	REQUEST_CUTSCENE("fam_6_mcs_5")

	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
		SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
		SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
		SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(TRUE, TRUE, TRUE, TRUE, bChestTattoo)
		bCutsceneAssetsRequested = TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_SHOP_4_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_mcs_5")
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psJimmy.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psTracey.PedIndex, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psTracey.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psLazlow.PedIndex, "Lazlow", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psLazlow.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psTatguy.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psTatguy.PedIndex, "Tattooist", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, psTatguy.ModelName)
				ENDIF
				
				IF DOES_ENTITY_EXIST(osScissors.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osScissors.ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osScissors.ObjectIndex, "Tattoo_Scissors", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, osScissors.ModelName)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(osPonyTail.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osPonyTail.ObjectIndex)
						IF IS_ENTITY_ATTACHED(osPonyTail.ObjectIndex)
							DETACH_ENTITY(osPonyTail.ObjectIndex, FALSE, TRUE)
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(osPonyTail.ObjectIndex, "Lazlow_Pony", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, osPonyTail.ModelName)
					ENDIF
				ENDIF
												
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				STOP_AUDIO_SCENES()
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
				
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_LAZLOW_CUTSCENE_COMPONENT_VARIATIONS(TRUE, TRUE, TRUE, TRUE, bChestTattoo)	
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1
		
			IF IS_CUTSCENE_PLAYING()
			
				IF DOES_CAM_EXIST(ScriptedCamera)
					DESTROY_CAM(ScriptedCamera)
				ENDIF

				IF DOES_CAM_EXIST(AnimatedCamera)
					DESTROY_CAM(AnimatedCamera)
				ENDIF
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHair)
					STOP_PARTICLE_FX_LOOPED(ptfxHair)
				ENDIF
				
				REMOVE_PTFX_ASSET()
				RELEASE_SCRIPT_AUDIO_BANK()
				REMOVE_ANIM_DICT("missfam6ig_1_pierce")
				REMOVE_ANIM_DICT("missfam6ig_7_tattoo")
				REMOVE_ANIM_DICT("missfam6ig_8_ponytail")
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			REQUEST_ANIM_DICT("missfam6leadinoutfam_6_mcs_5")
			
			IF NOT DOES_ENTITY_EXIST(psTatguy.PedIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Tattooist")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tattooist"))
					
						psTatguy.PedIndex =  GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tattooist"))
						
						SET_MISSION_PED_PROPERTIES(psTatguy.PedIndex, RELGROUPHASH_PLAYER, FALSE, TRUE, FALSE, FALSE)
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Set up properites for mission player ped Tattooist created in the intro cutscene.")
						#ENDIF

					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					WARP_PED(PLAYER_PED_ID(), <<315.4439, 175.3920, 102.8468>>, 106.3951, FALSE, FALSE, FALSE)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500)
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
				#ENDIF
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)				
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 0, 0)
					WARP_PED(psJimmy.PedIndex, <<313.6881, 174.4399, 102.8722>>, 101.0260, FALSE, FALSE, FALSE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(psJimmy.PedIndex, <<306.69650, 171.19496, 102.97732>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
					FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
				ENDIF			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tracy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Tracy.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					WARP_PED(psTracey.PedIndex, <<313.3786, 176.4375, 102.8908>>, 95.2572, FALSE, FALSE, FALSE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(psTracey.PedIndex, <<306.20856, 174.41803, 103.00662>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
					FORCE_PED_MOTION_STATE(psTracey.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lazlow")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Lazlow.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					IF HAS_ANIM_DICT_LOADED("missfam6leadinoutfam_6_mcs_5")
						IF NOT IS_ENTITY_PLAYING_ANIM(psLazlow.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_lazlow")
							TASK_PLAY_ANIM_ADVANCED(psLazlow.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_lazlow",
													vLazlowLeadoutPosition, vLazlowLeadoutRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
													-1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_NOT_INTERRUPTABLE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psLazlow.PedIndex, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tattooist")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Tattooist.")
				#ENDIF
			
				IF NOT IS_PED_INJURED(psTatguy.PedIndex)
					IF HAS_ANIM_DICT_LOADED("missfam6leadinoutfam_6_mcs_5")
						IF NOT IS_ENTITY_PLAYING_ANIM(psTatguy.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_tattoo_guy")
							TASK_PLAY_ANIM_ADVANCED(psTatguy.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_tattoo_guy",
													vTatguyLeadoutPosition, vTatguyLeadoutRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
													-1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_NOT_INTERRUPTABLE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psTatguy.PedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTatguy.PedIndex, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			
				RETURN TRUE
			
			ELSE
				
				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_DURING_GO_TO_SHRINK()

	IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF0")
		IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_DRF0", CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("FAM6_DRF0", TRUE)
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
				ENDIF
			ENDIF
		ENDIF
	ELSE
		
		//kill the first conversation if player is away from buddies
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		
			TEXT_LABEL_23 LocalCurrentConversationLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							
			IF NOT IS_STRING_NULL_OR_EMPTY(LocalCurrentConversationLabel)
				IF ARE_STRINGS_EQUAL(LocalCurrentConversationLabel, "FAM6_DRF0")
					IF 	GET_DISTANCE_BETWEEN_ENTITIES(psJimmy.PedIndex, PLAYER_PED_ID()) > 10 		
					AND GET_DISTANCE_BETWEEN_ENTITIES(psTracey.PedIndex, PLAYER_PED_ID()) > 10
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
	
		IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF1")
		AND	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF2")
			IF HAS_LABEL_BEEN_TRIGGERED(LabelFAM6SHRINK) OR IS_STRING_NULL_OR_EMPTY(LabelFAM6SHRINK)
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))  
							AND IS_PED_IN_VEHICLE(psTracey.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", PICK_STRING(bStageReplayInProgress, "FAM6_DRF1", "FAM6_DRF2"), CONV_PRIORITY_MEDIUM)
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									SET_LABEL_AS_TRIGGERED(PICK_STRING(bStageReplayInProgress, "FAM6_DRF1", "FAM6_DRF2"), TRUE)
									bRunConversationPausingCheck = TRUE
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//if main conversation was interrupted, resume it from the next label
		IF ( bCurrentConversationInterrupted = TRUE )			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JBAD")
				OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JWARN")
				OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")
					IF HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF1")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF2")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK2")
						//play conversation for Jimmy saying cops were lost and it's safe to go to shrink
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOST_3")
								IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")	//this will only play if conversation to lose cops was triggered
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOST")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JLOST", CONV_PRIORITY_MEDIUM)
											iCopsLostConversationCount++
											SET_LABEL_AS_TRIGGERED("FAM6_JLOST", TRUE)
											IF ( iCopsLostConversationCount >= 3)
												SET_LABEL_AS_TRIGGERED("FAM6_JLOST_3", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						//resume the interrupted coversation
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF 	NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
							AND NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationLabel)							
								IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_DRF1")	//resume only the main conversations
								OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_DRF2")
								OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_JTALK2")
									IF 	( iPlayerOffencesCount >= 3 )
									AND	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK2")
										//if player commited more than 3 offences and the weird conversation did not trigger
										//let the weird conversations trigger first before it can be resumed
									ELSE
										//resume conversation from specific root and line label
										IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(FAM6Conversation, "FAM6AUD", CurrentConversationRoot, CurrentConversationLabel, CONV_PRIORITY_MEDIUM)
											bCurrentConversationInterrupted = FALSE
											SET_LABEL_AS_TRIGGERED("FAM6_JBAD", FALSE)	//mark all the interrupt conversations as not triggered
											SET_LABEL_AS_TRIGGERED("FAM6_JLOST", FALSE)
											SET_LABEL_AS_TRIGGERED("FAM6_JWANT", FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
	
			//resume paused conversations when player gets back in the car
			//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					//PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				//ENDIF
			//ENDIF
		
		ELSE //locates header blip does not exist
			
			//play interrupt conversation when player becomes wanted
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
				
				IF ( bCurrentConversationInterrupted = FALSE )
				
					IF HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF1")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF2")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK2")
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
							CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							
							IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
								IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_DRF1")
								OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_DRF2")
								OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_JTALK2")
									bCurrentConversationInterrupted = TRUE
									CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
									CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF

				ENDIF
				
				IF ( bCurrentConversationInterrupted = TRUE )
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JWANT", CONV_PRIORITY_HIGH)
								iPlayerOffencesCount++
								iLoseCopsReminderTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 12500)
								SET_LABEL_AS_TRIGGERED("FAM6_JWANT", TRUE)
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JLOSE_5")
					IF HAS_LABEL_BEEN_TRIGGERED("FAM6_JWANT")
						IF GET_GAME_TIMER() - iLoseCopsReminderTimer > 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JLOSE", CONV_PRIORITY_MEDIUM)
									iLoseCopsReminderCount++
									iLoseCopsReminderTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12500, 15000)
									IF ( iLoseCopsReminderCount >= 5 )
										SET_LABEL_AS_TRIGGERED("FAM6_JLOSE_5", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//keep reseting those flags during wanted level as the player might be driving recklessly
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
				
			ELSE
			
				//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//	IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				//		PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				//	ENDIF
				//ENDIF
				
			ENDIF

		ENDIF
		
		//play interrupt conversations for player offences
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			IF ( GET_GAME_TIMER() - iPlayerOffenceTimer > 5000 )
				
				//play conversation for player running over peds
				IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JBAD_5")
				AND NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JWARN_6")

					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF	HAS_PLAYER_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
						AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
							bPlayerHitPed = TRUE
						ENDIF
					ENDIF
					
					IF IS_PED_SHOOTING(PLAYER_PED_ID())	//add more conditions here if needed
						bPlayerActingBad = TRUE
					ENDIF
					
					IF ( bPlayerHitPed = TRUE OR bPlayerActingBad = TRUE )
					
						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF1")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF2")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK2")
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
								CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								
								IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
									IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_DRF1")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_DRF2")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_JTALK2")
										CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
										CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": CurrentConversationRoot = ", CurrentConversationRoot, ".")
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": CurrentConversationLabel = ", CurrentConversationLabel, ".")
										#ENDIF
										bCurrentConversationInterrupted = TRUE
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
								ENDIF
							
							ENDIF
						ENDIF							

						IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK2")	//if jimmy did no start his talk for player messing around

							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JBAD", CONV_PRIORITY_MEDIUM)
								iPlayerOffencesCount++
								iBadConversationCount++
								bPlayerHitPed		= FALSE
								bPlayerActingBad 	= FALSE
								iPlayerOffenceTimer = GET_GAME_TIMER()
								SET_LABEL_AS_TRIGGERED("FAM6_JBAD", TRUE)
								IF ( iBadConversationCount >= 5 )
									SET_LABEL_AS_TRIGGERED("FAM6_JBAD_5", TRUE)
								ENDIF
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
								#ENDIF
							ENDIF
						
						ELSE											//if jimmy started his talk for player messing around
						
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JWARN", CONV_PRIORITY_MEDIUM)
								iPlayerOffencesCount++
								iBadConversationCount++
								bPlayerHitPed		= FALSE
								bPlayerActingBad 	= FALSE
								iPlayerOffenceTimer = GET_GAME_TIMER()
								SET_LABEL_AS_TRIGGERED("FAM6_JWARN", TRUE)
								IF ( iBadConversationCount >= 6 )
									SET_LABEL_AS_TRIGGERED("FAM6_JWARN_6", TRUE)
								ENDIF
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
								#ENDIF
							ENDIF
						
						ENDIF
						
					ENDIF
				ENDIF
			
			ELSE
			
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
			
			ENDIF
		ENDIF
		
		//play conversation when more than 3 offences have been commited
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_JTALK2")
			IF ( iPlayerOffencesCount >= 3 )
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF 	IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))  
						AND IS_PED_IN_VEHICLE(psTracey.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_JTALK2", CONV_PRIORITY_MEDIUM)
									bCurrentConversationInterrupted = FALSE
									SET_LABEL_AS_TRIGGERED("FAM6_JTALK2", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_GREET")
			IF ( bDestinationReached = TRUE )
				bRunConversationPausingCheck = FALSE
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
				ENDIF
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_GREET", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM6_GREET", TRUE)
						IF NOT IS_PED_INJURED(psAmanda.PedIndex)
							TASK_LOOK_AT_ENTITY(psAmanda.PedIndex, PLAYER_PED_ID(), INFINITE_TASK_TIME,
												SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
		
	IF ( bRunConversationPausingCheck = TRUE )
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
			IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND IS_PED_IN_VEHICLE(psTracey.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
			ENDIF				
		ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
		AND NOT IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex) 		
		AND NOT IS_PED_IN_ANY_VEHICLE(psTracey.PedIndex)
			IF 	GET_DISTANCE_BETWEEN_ENTITIES(psJimmy.PedIndex, PLAYER_PED_ID()) < 10 		
			AND GET_DISTANCE_BETWEEN_ENTITIES(psTracey.PedIndex, PLAYER_PED_ID()) < 10
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_LAZLOW_LEADOUT_DURING_GO_TO_SHRINK(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			REQUEST_ANIM_DICT("missfam6leadinoutfam_6_mcs_5")
		
			IF HAS_ANIM_DICT_LOADED("missfam6leadinoutfam_6_mcs_5")
			
				IF NOT IS_PED_INJURED(psLazlow.PedIndex)
					IF NOT IS_ENTITY_PLAYING_ANIM(psLazlow.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_lazlow")
						TASK_PLAY_ANIM_ADVANCED(psLazlow.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_lazlow",
												vLazlowLeadoutPosition, vLazlowLeadoutRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												-1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psLazlow.PedIndex)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(psTatguy.PedIndex)
					IF NOT IS_ENTITY_PLAYING_ANIM(psTatguy.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_tattoo_guy")
						TASK_PLAY_ANIM_ADVANCED(psTatguy.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_tattoo_guy",
												vTatguyLeadoutPosition, vTatguyLeadoutRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												-1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psTatguy.PedIndex)
					ENDIF
				ENDIF
				
				iAwayConversationCounter	= 0
				iAwayConversationTimer 		= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 5000)
				
				
				iProgress++
			
			ENDIF
		
		BREAK
		
		CASE 1
	
			IF DOES_ENTITY_EXIST(psLazlow.PedIndex)
				IF NOT IS_ENTITY_DEAD(psLazlow.PedIndex)
					IF IS_ENTITY_PLAYING_ANIM(psLazlow.PedIndex, "missfam6leadinoutfam_6_mcs_5","_leadout_loop_lazlow")
						IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_AWAY_3")
							IF GET_GAME_TIMER() - iAwayConversationTimer > 0
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<320.446808,178.033951,104.459259>>, << 1.5, 1.5, 2.0 >>)
										ADD_PED_FOR_DIALOGUE(FAM6Conversation, 3, psLazlow.PedIndex, "LAZLOW")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_AWAY", CONV_PRIORITY_MEDIUM)
											TASK_LOOK_AT_ENTITY(psLazlow.PedIndex, PLAYER_PED_ID(), 3000, SLF_WHILE_NOT_IN_FOV)
											iAwayConversationCounter++
											iAwayConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 10000)
											IF ( iAwayConversationCounter >= 3)
												SET_LABEL_AS_TRIGGERED("FAM6_AWAY_3", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vLazlowLeadoutPosition) > 150.0
			
				CLEANUP_PED(psLazlow, TRUE)
				CLEANUP_PED(psTatguy, TRUE)
				REMOVE_ANIM_DICT("missfam6leadinoutfam_6_mcs_5")
				
				iProgress++
			
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_AMANDA_DURING_GO_TO_SHRINK(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vShrinkOfficePosition) < DEFAULT_CUTSCENE_LOAD_DIST * 2
		
				iProgress++
		
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(psAmanda.ModelName)
			REQUEST_ANIM_DICT("amb@world_human_stand_impatient@female@no_sign@idle_a")
			
			IF 	HAS_MODEL_LOADED(psAmanda.ModelName)
			AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_impatient@female@no_sign@idle_a")
			
				CLEAR_AREA(psAmanda.vPosition, 3.0, TRUE)
			
				IF HAS_MISSION_PED_BEEN_CREATED(psAmanda, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_AMANDA)
				
					ADD_PED_FOR_DIALOGUE(FAM6Conversation, 4, psAmanda.PedIndex, "AMANDA")
					SET_AMANDA_PED_COMPONENT_VARIATIONS(psAmanda.bComponentVariationsSet)
					
					TASK_LOOK_AT_ENTITY(psAmanda.PedIndex, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
					TASK_PLAY_ANIM(psAmanda.PedIndex, "amb@world_human_stand_impatient@female@no_sign@idle_a", "idle_c",
								   NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE |
								   AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psAmanda.PedIndex, TRUE)
					
					SET_RAGDOLL_BLOCKING_FLAGS(psAmanda.PedIndex, RBF_MELEE)
					SET_RAGDOLL_BLOCKING_FLAGS(psAmanda.PedIndex, RBF_PLAYER_BUMP)
					SET_RAGDOLL_BLOCKING_FLAGS(psAmanda.PedIndex, RBF_PLAYER_IMPACT)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(psAmanda.ModelName)
					
					iProgress++
				
				ENDIF
			
			ENDIF
			
		BREAK
		
		CASE 2
		
			IF NOT IS_PED_INJURED(psAmanda.PedIndex)
				IF IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "amb@world_human_stand_impatient@female@no_sign@idle_a", "idle_c")
					SET_PED_CAPSULE(psAmanda.PedIndex, 0.25)
					SET_PED_RESET_FLAG(psAmanda.PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC BOOL IS_MISSION_STAGE_DRIVE_TO_SHRINK_COMPLETED(INT &iStageProgress)

	//don't use locates header J skip
	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	UPDATE_TRIGGERED_LABEL(LabelFAM6SHRINK)
	MANAGE_CONVERSATIONS_DURING_GO_TO_SHRINK()
	MANAGE_AMANDA_DURING_GO_TO_SHRINK(iAmandaProgress)
	RUN_INTERIOR_UNPINNING_CHECK(vTattooShopScenePosition, 100.0)
	MANAGE_LAZLOW_LEADOUT_DURING_GO_TO_SHRINK(iLazlowLeadoutProgress)
	
	IF 	DOES_ENTITY_EXIST(sLocatesData.vehStartCar)
	AND NOT IS_ENTITY_DEAD(sLocatesData.vehStartCar)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sLocatesData.vehStartCar, TRUE)
		IF NOT IS_PED_IN_VEHICLE(psJimmy.PedIndex, sLocatesData.vehStartCar, TRUE)
		OR NOT IS_PED_IN_VEHICLE(psTracey.PedIndex, sLocatesData.vehStartCar, TRUE)
			DISABLE_LOCATES_CLEAR_BUDDY_TASKS_IF_ENTERING_NEAREST_VEHICLE(sLocatesData, TRUE)
		ENDIF
	ELSE
		DISABLE_LOCATES_CLEAR_BUDDY_TASKS_IF_ENTERING_NEAREST_VEHICLE(sLocatesData, FALSE)
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_PLAYER_CHANGING_CLOTHES()

		IF HAS_THIS_CUTSCENE_LOADED("fam_6_mcs_6")
		OR HAS_CUTSCENE_LOADED()
		OR IS_CUTSCENE_ACTIVE()
			REMOVE_CUTSCENE()
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
			#ENDIF
		ENDIF
		                        
	ELSE

		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vShrinkOfficePosition) < DEFAULT_CUTSCENE_LOAD_DIST

			REQUEST_CUTSCENE("fam_6_mcs_6")
			
			IF NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vShrinkInteriorPosition))		
				PIN_INTERIOR_IN_MEMORY(GET_INTERIOR_AT_COORDS(vShrinkInteriorPosition))
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Pinning interior at coordinates ", vShrinkInteriorPosition, " in memory.")
				#ENDIF
			ENDIF
		
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
				SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
				SET_AMANDA_CUTSCENE_COMPONENT_VARIATIONS()
				bCutsceneAssetsRequested = TRUE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene peds from peds during stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting bCutsceneAssetsRequested to ", bCutsceneAssetsRequested, ".")
				#ENDIF
			ENDIF
			
		ELSE
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vShrinkOfficePosition) > DEFAULT_CUTSCENE_UNLOAD_DIST * 2
			
				IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vShrinkInteriorPosition))
					UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(vShrinkInteriorPosition))
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unpinning interior at coordinates ", vShrinkInteriorPosition, ".")
					#ENDIF
				ENDIF
		
				IF HAS_THIS_CUTSCENE_LOADED("fam_6_mcs_6")
				OR HAS_CUTSCENE_LOADED()
				OR IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()
					SET_MODEL_AS_NO_LONGER_NEEDED(BALLER)
					bCutsceneAssetsRequested = FALSE
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting bCutsceneAssetsRequested to ", bCutsceneAssetsRequested, ".")
					#ENDIF
				ENDIF
				
			ENDIF
		
		ENDIF
		
	ENDIF

	SWITCH iStageProgress
		
		CASE 0
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_SCREEN_FADED_OUT()
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500)
				ENDIF
			ENDIF
		
			IF NOT IS_PED_INJURED(psJimmy.PedIndex)	
				IF ( GET_SCRIPT_TASK_STATUS(psJimmy.PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK )
					TASK_FOLLOW_NAV_MESH_TO_COORD(psJimmy.PedIndex, <<306.69650, 171.19496, 102.97732>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
					FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(psTracey.PedIndex)	
				IF ( GET_SCRIPT_TASK_STATUS(psTracey.PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK )
					TASK_FOLLOW_NAV_MESH_TO_COORD(psTracey.PedIndex, <<306.20856, 174.41803, 103.00662>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
					FORCE_PED_MOTION_STATE(psTracey.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				WAIT(2)
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		
			IF HAS_LABEL_BEEN_TRIGGERED("FAM6_DRF0")
		
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
		
				SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(sLocatesData, vsMichaelsCar.VehicleIndex)
			
				START_AUDIO_SCENE("FAMILY_6_DRIVE_TO_THERAPIST")
			
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 1
		
			IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(sLocatesData, vShrinkOfficePosition, DEFAULT_LOCATES_SIZE_VECTOR(), TRUE,
														 psTracey.PedIndex, psJimmy.PedIndex, NULL, LabelFAM6SHRINK, "CMN_TCLEAVE", "CMN_JLEAVE", "", "FAM6_FAM", FALSE, TRUE)
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF	ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psJimmy.PedIndex)
				AND ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psTracey.PedIndex)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1894.675781,-567.717773,10.614735>>, <<-1906.339966,-557.923096,13.812391>>, 8.0)
				 		
						IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
						
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
									
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()
							
							bDestinationReached = TRUE
							
							iStageProgress++
						
						ENDIF
						
				 	ENDIF
				ELSE
					IF 	IS_PED_GROUP_MEMBER(psJimmy.PedIndex, PLAYER_GROUP_ID())
					AND IS_PED_GROUP_MEMBER(psTracey.PedIndex, PLAYER_GROUP_ID())
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1894.675781,-567.717773,10.614735>>, <<-1906.339966,-557.923096,13.812391>>, 8.0)
						
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
							
									CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
										
									KILL_ANY_CONVERSATION()
									KILL_FACE_TO_FACE_CONVERSATION()
									
									bDestinationReached = TRUE
									
									iStageProgress++
									
								ENDIF
								
							ELSE
							
								IF 	NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								AND NOT IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex)
								AND NOT IS_PED_IN_ANY_VEHICLE(psTracey.PedIndex)
									IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1894.675781,-567.717773,10.614735>>, <<-1906.339966,-557.923096,13.812391>>, 16.0)
									AND IS_ENTITY_IN_ANGLED_AREA(psJimmy.PedIndex, <<-1894.675781,-567.717773,10.614735>>, <<-1906.339966,-557.923096,13.812391>>, 16.0)
									AND IS_ENTITY_IN_ANGLED_AREA(psTracey.PedIndex, <<-1894.675781,-567.717773,10.614735>>, <<-1906.339966,-557.923096,13.812391>>, 16.0)
								
										CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
												
										KILL_ANY_CONVERSATION()
										KILL_FACE_TO_FACE_CONVERSATION()
										
										bDestinationReached = TRUE

										iStageProgress++
										
									ENDIF
								
								ENDIF
								
							ENDIF	
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
	
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 2.5)
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						iStageProgress++
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					iStageProgress++
				ENDIF				
			ENDIF
			
		BREAK
		
		CASE 3
		
			bLeadInTriggered = TRUE
		
			IF NOT IS_PED_INJURED(psJimmy.PedIndex)
				SEQUENCE_INDEX JimmySequenceIndex
				CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
				OPEN_SEQUENCE_TASK(JimmySequenceIndex)
					IF IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex)
						TASK_LEAVE_ANY_VEHICLE(NULL, GET_VEHICLE_EXIT_DELAY(psJimmy.PedIndex, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1913.51172, -568.23083, 10.82617>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
				CLOSE_SEQUENCE_TASK(JimmySequenceIndex)
				TASK_PERFORM_SEQUENCE(psJimmy.PedIndex, JimmySequenceIndex)
				CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
			ENDIF
			
			IF NOT IS_PED_INJURED(psTracey.PedIndex)
				SEQUENCE_INDEX TraceySequenceIndex
				CLEAR_SEQUENCE_TASK(TraceySequenceIndex)
				OPEN_SEQUENCE_TASK(TraceySequenceIndex)
					IF IS_PED_IN_ANY_VEHICLE(psTracey.PedIndex)
						TASK_LEAVE_ANY_VEHICLE(NULL, GET_VEHICLE_EXIT_DELAY(psTracey.PedIndex, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1913.51172, -568.23083, 10.82617>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
				CLOSE_SEQUENCE_TASK(TraceySequenceIndex)
				TASK_PERFORM_SEQUENCE(psTracey.PedIndex, TraceySequenceIndex)
				CLEAR_SEQUENCE_TASK(TraceySequenceIndex)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SEQUENCE_INDEX PlayerSequenceIndex
				CLEAR_SEQUENCE_TASK(PlayerSequenceIndex)
				OPEN_SEQUENCE_TASK(PlayerSequenceIndex)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_LEAVE_ANY_VEHICLE(NULL, GET_VEHICLE_EXIT_DELAY(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1913.51172, -568.23083, 10.82617>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
				CLOSE_SEQUENCE_TASK(PlayerSequenceIndex)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), PlayerSequenceIndex)
				CLEAR_SEQUENCE_TASK(PlayerSequenceIndex)
			ENDIF
			
			iStageProgress++
			
		BREAK
		
		CASE 4
		
			IF  NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex)
			AND NOT IS_PED_IN_ANY_VEHICLE(psTracey.PedIndex)
			
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					CLEAR_RAGDOLL_BLOCKING_FLAGS(psAmanda.PedIndex, RBF_MELEE)
					CLEAR_RAGDOLL_BLOCKING_FLAGS(psAmanda.PedIndex, RBF_PLAYER_BUMP)
					CLEAR_RAGDOLL_BLOCKING_FLAGS(psAmanda.PedIndex, RBF_PLAYER_IMPACT)
					TASK_FOLLOW_NAV_MESH_TO_COORD(psAmanda.PedIndex, <<-1913.51172, -568.23083, 10.82617>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
				ENDIF
				
				DESTROY_ALL_CAMS()

				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				SET_CAM_PARAMS(ScriptedCamera, <<-1887.2565, -532.6524, 21.8295>>, <<-8.7969, 1.7310, 154.3490>>, 36.7)
				SET_CAM_PARAMS(ScriptedCamera, <<-1887.3967, -532.7990, 23.3122>>, <<-9.4823, 1.7310, 154.3490>>, 36.7,
							   15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SHAKE_CAM(ScriptedCamera,"HAND_SHAKE",0.1)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				REPLAY_RECORD_BACK_FOR_TIME(16.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				
				psAmanda.iTimer = GET_GAME_TIMER()
				
				iStageProgress++
				
			ENDIF
			
		BREAK
		
		CASE 5
		
			IF GET_GAME_TIMER() - psAmanda.iTimer > 3000
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
					IF ( bCutsceneAssetsRequested = TRUE )
				
						CLEANUP_PED(psLazlow, TRUE)
						CLEANUP_PED(psTatguy, TRUE)
						REMOVE_ANIM_DICT("missfam6leadinoutfam_6_mcs_5")
						REMOVE_ANIM_DICT("amb@world_human_stand_impatient@female@no_sign@idle_a")
						
						IF NOT IS_PED_INJURED(psAmanda.PedIndex)
							TASK_CLEAR_LOOK_AT(psAmanda.PedIndex)
						ENDIF
						
						REPLAY_STOP_EVENT()
						
						RETURN TRUE
					
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_SHRINK_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_mcs_6")
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					IF IS_PED_GROUP_MEMBER(psJimmy.PedIndex, PLAYER_GROUP_ID())
						REMOVE_PED_FROM_GROUP(psJimmy.PedIndex)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psJimmy.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					IF IS_PED_GROUP_MEMBER(psTracey.PedIndex, PLAYER_GROUP_ID())
						REMOVE_PED_FROM_GROUP(psTracey.PedIndex)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(psTracey.PedIndex, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psTracey.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psAmanda.PedIndex, "Amanda", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psAmanda.ModelName)
				ENDIF
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				STOP_AUDIO_SCENES()
				START_CUTSCENE(CUTSCENE_DELAY_ENABLING_PLAYER_CONTROL_FOR_UP_TO_DATE_GAMEPLAY_CAMERA)
								
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
								
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)					
					SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_AMANDA_CUTSCENE_COMPONENT_VARIATIONS()
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1
		
			IF IS_CUTSCENE_PLAYING()
				
				VEHICLE_INDEX LastPlayerVehicleIndex
				
				LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
				
				IF DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
					IF NOT IS_ENTITY_DEAD(LastPlayerVehicleIndex)
					
						IF IS_VEHICLE_AVAILABLE_TO_SNAPSHOT(PLAYER_PED_ID(), LastPlayerVehicleIndex, TRUE)
						
							IF 	NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(LastPlayerVehicleIndex))
							AND	NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(LastPlayerVehicleIndex))
							
								IF NOT IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
									
									SET_ENTITY_AS_MISSION_ENTITY(LastPlayerVehicleIndex)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
							
									vsMichaelsCar.VehicleIndex = LastPlayerVehicleIndex
								ENDIF
								
								STOP_VEHICLE_FIRE(LastPlayerVehicleIndex)
								
								SET_ENTITY_COORDS(LastPlayerVehicleIndex, vsMichaelsCar.vPosition)
								SET_ENTITY_HEADING(LastPlayerVehicleIndex, vsMichaelsCar.fHeading)
								SET_VEHICLE_ON_GROUND_PROPERLY(LastPlayerVehicleIndex)
								SET_VEHICLE_DOORS_SHUT(LastPlayerVehicleIndex, TRUE)
								SET_VEHICLE_ENGINE_ON(LastPlayerVehicleIndex, FALSE, TRUE)
								
									
								SET_VEHICLE_LOD_MULTIPLIER(LastPlayerVehicleIndex, 2.0)
								REQUEST_VEHICLE_HIGH_DETAIL_MODEL(LastPlayerVehicleIndex)
								
								OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(LastPlayerVehicleIndex)
								
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				CLEAR_AREA_OF_PEDS(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_OBJECTS(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_PROJECTILES(MissionPosition.vPosition, 100.0)
				REMOVE_PARTICLE_FX_IN_RANGE(MissionPosition.vPosition, 100.0)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex) AND NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
				SET_FORCE_HD_VEHICLE(vsMichaelsCar.VehicleIndex, TRUE)
				SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vsMichaelsCar.VehicleIndex, TRUE)
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Amanda")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Amanda.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						FORCE_PED_MOTION_STATE(psAmanda.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						TASK_ENTER_VEHICLE(psAmanda.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP ,VS_FRONT_RIGHT, PEDMOVE_WALK)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tracy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Tracy.")
				#ENDIF
			
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						FORCE_PED_MOTION_STATE(psTracey.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						TASK_ENTER_VEHICLE(psTracey.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP ,VS_BACK_RIGHT, PEDMOVE_WALK)
					ENDIF
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
				#ENDIF
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
				
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 0, 0)
				
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						TASK_ENTER_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP ,VS_BACK_LEFT, PEDMOVE_WALK)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_CAMERA() returned true.")
				#ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				REPLAY_STOP_EVENT()
			
				IF ( bCutsceneSkipped = TRUE )
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						WARP_PED(PLAYER_PED_ID(), MissionPosition.vPosition, MissionPosition.fHeading, FALSE, FALSE, FALSE)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
				ENDIF
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				DEBIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_SHRINK, 4000)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				RETURN TRUE
				
			ELSE
			
				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_DURING_GO_HOME()

	IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_HOME")
		IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_HOME", CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("FAM6_HOME", TRUE)
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
				ENDIF
			ENDIF
		ENDIF
	ELSE

		IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT1")
		AND	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT2")
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))  
						AND IS_PED_IN_VEHICLE(psTracey.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						AND IS_PED_IN_VEHICLE(psAmanda.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", PICK_STRING(bStageReplayInProgress, "FAM6_CHAT2", "FAM6_CHAT1"), CONV_PRIORITY_MEDIUM)
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))	
								SET_LABEL_AS_TRIGGERED(PICK_STRING(bStageReplayInProgress, "FAM6_CHAT2", "FAM6_CHAT1"), TRUE)
								bRunConversationPausingCheck = TRUE
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//if main conversation was interrupted, resume it from the next label
		IF ( bCurrentConversationInterrupted = TRUE )
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF HAS_LABEL_BEEN_TRIGGERED("FAM6_BAD")
				OR HAS_LABEL_BEEN_TRIGGERED("FAM6_HITPED")
				OR HAS_LABEL_BEEN_TRIGGERED("FAM6_WANTED")
					IF HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT1")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT2")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_WEIRD")
					
						//play conversation for Amanda saying cops were lost and it's safe to go back to house (3 random variations)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_LOSE_3")
								IF HAS_LABEL_BEEN_TRIGGERED("FAM6_WANTED")	//this will only play if conversation to lose cops was triggered
									IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_LOSE")
										IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_LOSE", CONV_PRIORITY_MEDIUM)
											iCopsLostConversationCount++
											SET_LABEL_AS_TRIGGERED("FAM6_LOSE", TRUE)
											IF ( iCopsLostConversationCount >= 3)
												SET_LABEL_AS_TRIGGERED("FAM6_LOSE_3", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						//resume the interrupted coversation
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF 	NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
							AND NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationLabel)
								IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_CHAT1")	//resume only the main conversations
								OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_CHAT2")
								OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_WEIRD")
								
									IF 	( iPlayerOffencesCount >= 3 )
									AND	NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_WEIRD")
										//if player commited more than 3 offences and the weird conversation did not trigger
										//let the weird conversations trigger first before it can be resumed
									ELSE
										//resume conversation from specific root and line label
										IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(FAM6Conversation, "FAM6AUD", CurrentConversationRoot, CurrentConversationLabel, CONV_PRIORITY_MEDIUM)
											bCurrentConversationInterrupted = FALSE
											SET_LABEL_AS_TRIGGERED("FAM6_BAD", FALSE)	//mark all the interrupt conversations as not triggered
											SET_LABEL_AS_TRIGGERED("FAM6_LOSE", FALSE)
											SET_LABEL_AS_TRIGGERED("FAM6_HITPED", FALSE)
											SET_LABEL_AS_TRIGGERED("FAM6_WANTED", FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
	
			//resume paused conversations when player gets back in the car
			//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					//PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				//ENDIF
			//ENDIF
		
		ELSE //locates header blip does not exist
			
			//play interrupt conversation when player becomes wanted
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
				
				IF ( bCurrentConversationInterrupted = FALSE )
				
					IF HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT1")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT2")
					OR HAS_LABEL_BEEN_TRIGGERED("FAM6_WEIRD")
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
							CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							
							IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
								IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_CHAT1")
								OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_CHAT2")
								OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_WEIRD")
									bCurrentConversationInterrupted = TRUE
									CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
									CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF

				ENDIF
				
				IF ( bCurrentConversationInterrupted = TRUE )
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_WANTED")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_WANTED", CONV_PRIORITY_HIGH)
								iPlayerOffencesCount++
								SET_LABEL_AS_TRIGGERED("FAM6_WANTED", TRUE)
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//keep reseting those flags during wanted level as the player might be driving recklessly
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
				
			ELSE
			
				//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//	IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				//		PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				//	ENDIF
				//ENDIF
				
			ENDIF

		ENDIF

		//play interrupt conversations for player offences
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			IF ( GET_GAME_TIMER() - iPlayerOffenceTimer > 5000 )
				
				//play conversation for player running over peds
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_HITPED_5")

					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF	HAS_PLAYER_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
						AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
							bPlayerHitPed = TRUE
						ENDIF
					ENDIF
					
					IF ( bPlayerHitPed = TRUE )
					
						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT1")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT2")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_WEIRD")
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
								CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								
								IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
									IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_CHAT1")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_CHAT2")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_WEIRD")
										CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
										CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
										bCurrentConversationInterrupted = TRUE
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
								ENDIF
							
							ENDIF
						ENDIF									

						IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_HITPED", CONV_PRIORITY_MEDIUM)
							iPlayerOffencesCount++
							iHitPedConversationCount++
							bPlayerHitPed		= FALSE
							iPlayerOffenceTimer = GET_GAME_TIMER()
							SET_LABEL_AS_TRIGGERED("FAM6_HITPED", TRUE)
							IF ( iHitPedConversationCount >= 5 )
								SET_LABEL_AS_TRIGGERED("FAM6_HITPED_5", TRUE)
							ENDIF
							CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
							CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
							#ENDIF
						ENDIF
						
					ENDIF
				ENDIF
				
				//play conversation for player shooting, acting bad etc
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_BAD_3")
				
					IF IS_PED_SHOOTING(PLAYER_PED_ID())	//add more conditions here if needed
						bPlayerActingBad = TRUE
					ENDIF
					
					IF ( bPlayerActingBad = TRUE)
					
						IF HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT1")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_CHAT2")
						OR HAS_LABEL_BEEN_TRIGGERED("FAM6_WEIRD")
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
								CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								
								IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
									IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_CHAT1")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_CHAT2")
									OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM6_WEIRD")
										CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
										CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
										bCurrentConversationInterrupted = TRUE
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF									
				
						IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_BAD", CONV_PRIORITY_MEDIUM)
							iPlayerOffencesCount++
							iBadConversationCount++
							bPlayerActingBad	= FALSE
							iPlayerOffenceTimer = GET_GAME_TIMER()
							SET_LABEL_AS_TRIGGERED("FAM6_BAD", TRUE)
							IF ( iBadConversationCount >= 3 )
								SET_LABEL_AS_TRIGGERED("FAM6_BAD_3", TRUE)
							ENDIF
							CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
							CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": iPlayerOffencesCount: ", iPlayerOffencesCount, ".")
							#ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			
			ELSE
			
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
			
			ENDIF
		ENDIF

		//play conversation for player being weird when more than 3 offences have been commited
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_WEIRD")
			IF ( iPlayerOffencesCount >= 3 )
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF 	IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))  
						AND IS_PED_IN_VEHICLE(psTracey.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						AND IS_PED_IN_VEHICLE(psAmanda.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(FAM6Conversation, "FAM6AUD", "FAM6_WEIRD", CONV_PRIORITY_MEDIUM)
									bCurrentConversationInterrupted = FALSE
									SET_LABEL_AS_TRIGGERED("FAM6_WEIRD", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM6_HOUSE")
			IF ( bFamilyArrivedAtHouse = FALSE )
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF GET_DISTANCE_BETWEEN_COORDS(vMichaelsHousePosition, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 50
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(FAM6Conversation,"FAM6AUD","FAM6_HOUSE",CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("FAM6_HOUSE", TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ( bRunConversationPausingCheck = TRUE )
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
				IF 	IS_PED_IN_VEHICLE(psJimmy.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				AND IS_PED_IN_VEHICLE(psTracey.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				AND IS_PED_IN_VEHICLE(psAmanda.PedIndex, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				//AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
						ENDIF
					ENDIF
				ELSE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF				
			ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
			AND NOT IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex) 		
			AND NOT IS_PED_IN_ANY_VEHICLE(psTracey.PedIndex)
			AND NOT IS_PED_IN_ANY_VEHICLE(psAmanda.PedIndex)
				IF 	GET_DISTANCE_BETWEEN_ENTITIES(psJimmy.PedIndex, PLAYER_PED_ID()) < 10 		
				AND GET_DISTANCE_BETWEEN_ENTITIES(psTracey.PedIndex, PLAYER_PED_ID()) < 10
				AND GET_DISTANCE_BETWEEN_ENTITIES(psAmanda.PedIndex, PLAYER_PED_ID()) < 10
				//AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	
						ENDIF
					ENDIF
				ELSE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_DRIVE_HOME_COMPLETED(INT &iStageProgress)

	//don't use locates header J skip
	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
	
	MANAGE_CONVERSATIONS_DURING_GO_HOME()
	RUN_INTERIOR_UNPINNING_CHECK(vShrinkInteriorPosition, 30.0)

	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_PLAYER_CHANGING_CLOTHES()

		IF HAS_THIS_CUTSCENE_LOADED("fam_6_mcs_6_p4_b")
		OR HAS_CUTSCENE_LOADED()
		OR IS_CUTSCENE_ACTIVE()
			REMOVE_CUTSCENE()
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
			#ENDIF
		ENDIF
		                        
	ELSE

		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMichaelsHousePosition) < DEFAULT_CUTSCENE_LOAD_DIST

			REQUEST_CUTSCENE("fam_6_mcs_6_p4_b")
			
			IF ( bSetSRLForcePrestream = FALSE )
				SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF)
				bSetSRLForcePrestream = TRUE
			ENDIF
			
		
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
				SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
				SET_AMANDA_CUTSCENE_COMPONENT_VARIATIONS()
				bCutsceneAssetsRequested = TRUE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene peds from peds during stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting bCutsceneAssetsRequested to ", bCutsceneAssetsRequested, ".")
				#ENDIF
			ENDIF
			
		ELSE
		
			IF HAS_THIS_CUTSCENE_LOADED("fam_6_mcs_6_p4_b")
			OR HAS_CUTSCENE_LOADED()
			OR IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
				SET_MODEL_AS_NO_LONGER_NEEDED(BALLER)
				bCutsceneAssetsRequested = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting bCutsceneAssetsRequested to ", bCutsceneAssetsRequested, ".")
				#ENDIF
			ENDIF
		
		ENDIF
		
	ENDIF

	SWITCH iStageProgress
		
		CASE 0
		
			IF NOT IS_PED_INJURED(psJimmy.PedIndex)	AND IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				IF ( GET_SCRIPT_TASK_STATUS(psJimmy.PedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK )
					TASK_ENTER_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP * 3, VS_BACK_LEFT, PEDMOVE_WALK)
					FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(psTracey.PedIndex) AND IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				IF ( GET_SCRIPT_TASK_STATUS(psTracey.PedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK )
					TASK_ENTER_VEHICLE(psTracey.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP * 3, VS_BACK_RIGHT, PEDMOVE_WALK)
					FORCE_PED_MOTION_STATE(psTracey.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(psAmanda.PedIndex) AND IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				IF ( GET_SCRIPT_TASK_STATUS(psAmanda.PedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK )
					TASK_ENTER_VEHICLE(psAmanda.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP * 3, VS_FRONT_RIGHT, PEDMOVE_WALK)
					FORCE_PED_MOTION_STATE(psAmanda.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				WAIT(2)
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		
			IF HAS_LABEL_BEEN_TRIGGERED("FAM6_HOME")
		
				SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(sLocatesData, vsMichaelsCar.VehicleIndex)
			
				START_AUDIO_SCENE("FAMILY_6_DRIVE_HOME")
			
				bVehicleReversed = FALSE
			
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 1
				
			IF ( bStopUsingDefaultRoute = FALSE )
				IF ( bCustomRouteActive = FALSE )
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						SET_GPS_DISABLED_ZONE(<< -1908.517456 - 24.0, -518.293640 - 24.0, 17.816877 - 8.0 >>,
											  << -1908.517456 + 24.0, -518.293640 + 24.0, 17.816877 + 8.0 >>)
						
						bCustomRouteActive = TRUE
					ENDIF
				ELSE
				
					IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						SET_GPS_DISABLED_ZONE(<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						bCustomRouteActive = FALSE
					ENDIF
				
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1908.517456, -518.293640, 17.816877 >>, << 24.0, 24.0, 8.0 >>)
						OR NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1901.539307,-537.078613,17.767612>>, <<36.000000,32.000000,8.000000>>)
							
							SET_GPS_DISABLED_ZONE(<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						
							bStopUsingDefaultRoute = TRUE
						
						ENDIF
					
					ENDIF
					
				ENDIF		
			ENDIF
			
			IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(sLocatesData, vMichaelsHousePosition, DEFAULT_LOCATES_SIZE_VECTOR(),
														 TRUE, psAmanda.PedIndex, psJimmy.PedIndex, psTracey.PedIndex, "FAM6_DHOUSE",
														 "CMN_ALEAVE", "CMN_JLEAVE", "CMN_TCLEAVE", "FAM6_FAM", FALSE, TRUE)
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF	ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psJimmy.PedIndex)
				AND ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psAmanda.PedIndex)
				AND ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psTracey.PedIndex)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-826.59497, 177.70781, 68.15234>>, <<-823.29541, 183.13243, 75.85941>>, 5.54)
				 		
						IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
						
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
									
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()
							
							iStageProgress++
						
						ENDIF
				 	ENDIF
					
				ELSE
				
					IF 	IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-825.972961,179.888916,72.441452>>, << 10.0, 10.0, 4.0 >>)
					AND IS_ENTITY_AT_COORD(psJimmy.PedIndex, <<-825.972961,179.888916,72.441452>>, << 10.0, 10.0, 4.0 >>)
					AND IS_ENTITY_AT_COORD(psAmanda.PedIndex, <<-825.972961,179.888916,72.441452>>, << 10.0, 10.0, 4.0 >>)
					AND IS_ENTITY_AT_COORD(psTracey.PedIndex, <<-825.972961,179.888916,72.441452>>, << 10.0, 10.0, 4.0 >>)
					
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
										
								KILL_ANY_CONVERSATION()
								KILL_FACE_TO_FACE_CONVERSATION()

								iStageProgress++
								
							ENDIF
							
						ELSE
						
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
										
							KILL_ANY_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()

							iStageProgress++
							
						ENDIF
						
					ENDIF
					
				ENDIF

			ENDIF
		
		BREAK
		
		CASE 2
	
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				IF ( bVehicleReversed = FALSE )
					IF IS_VEHICLE_REVERSING(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
						bVehicleReversed = TRUE
					ENDIF
				ENDIF
			
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 5.0)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						iStageProgress++
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					iStageProgress++
				ENDIF				
			ENDIF
			
		BREAK
		
		CASE 3
			
			IF NOT IS_PED_INJURED(psJimmy.PedIndex)
				SEQUENCE_INDEX JimmySequenceIndex
				CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
				OPEN_SEQUENCE_TASK(JimmySequenceIndex)
					IF IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex)
						TASK_LEAVE_ANY_VEHICLE(NULL, GET_VEHICLE_EXIT_DELAY(psJimmy.PedIndex, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -817.9304, 177.7482, 71.2278 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -808.3328, 171.4540, 75.7457 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
				CLOSE_SEQUENCE_TASK(JimmySequenceIndex)
				TASK_PERFORM_SEQUENCE(psJimmy.PedIndex, JimmySequenceIndex)
				CLEAR_SEQUENCE_TASK(JimmySequenceIndex)
			ENDIF
			
			IF NOT IS_PED_INJURED(psTracey.PedIndex)
				SEQUENCE_INDEX TraceySequenceIndex
				CLEAR_SEQUENCE_TASK(TraceySequenceIndex)
				OPEN_SEQUENCE_TASK(TraceySequenceIndex)
					IF IS_PED_IN_ANY_VEHICLE(psTracey.PedIndex)
						TASK_LEAVE_ANY_VEHICLE(NULL, GET_VEHICLE_EXIT_DELAY(psTracey.PedIndex, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -817.9304, 177.7482, 71.2278 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -803.5399, 177.6053, 75.7483 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
				CLOSE_SEQUENCE_TASK(TraceySequenceIndex)
				TASK_PERFORM_SEQUENCE(psTracey.PedIndex, TraceySequenceIndex)
				CLEAR_SEQUENCE_TASK(TraceySequenceIndex)
			ENDIF
			
			IF NOT IS_PED_INJURED(psAmanda.PedIndex)
				SEQUENCE_INDEX AmandaSequenceIndex
				CLEAR_SEQUENCE_TASK(AmandaSequenceIndex)
				OPEN_SEQUENCE_TASK(AmandaSequenceIndex)
					IF IS_PED_IN_ANY_VEHICLE(psAmanda.PedIndex)
						TASK_LEAVE_ANY_VEHICLE(NULL, GET_VEHICLE_EXIT_DELAY(psAmanda.PedIndex, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
					ENDIF 
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-817.93042, 177.74828, 71.22781>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-798.39508, 183.62172, 71.60547>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-775.53857, 183.19688, 71.83523>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
				CLOSE_SEQUENCE_TASK(AmandaSequenceIndex)
				TASK_PERFORM_SEQUENCE(psAmanda.PedIndex, AmandaSequenceIndex)
				CLEAR_SEQUENCE_TASK(AmandaSequenceIndex)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SEQUENCE_INDEX PlayerSequenceIndex
				CLEAR_SEQUENCE_TASK(PlayerSequenceIndex)
				OPEN_SEQUENCE_TASK(PlayerSequenceIndex)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_LEAVE_ANY_VEHICLE(NULL, GET_VEHICLE_EXIT_DELAY(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -819.05, 177.24, 70.83 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3)
				CLOSE_SEQUENCE_TASK(PlayerSequenceIndex)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), PlayerSequenceIndex)
				CLEAR_SEQUENCE_TASK(PlayerSequenceIndex)
			ENDIF
			
			iStageProgress++
			
		BREAK
		
		CASE 4
		
			IF 	NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_VEHICLE(psJimmy.PedIndex)
			AND NOT IS_PED_IN_ANY_VEHICLE(psTracey.PedIndex)
			AND NOT IS_PED_IN_ANY_VEHICLE(psAmanda.PedIndex)
			
				IF ( bCutsceneAssetsRequested = TRUE )	//progress if cutscene ped variations were requested
														//otherwise cutscene might play	with default ped variations
					
					RETURN TRUE
					
				ENDIF
				
			ENDIF
		
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_END_COMPLETED(INT &iStageProgress)

	IF DOES_CAM_EXIST(ScriptedCamera)
		SET_USE_HI_DOF()
	ENDIF

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("fam_6_mcs_6_p4_b")
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, psJimmy.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psTracey.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psTracey.PedIndex, "Tracy", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, psTracey.ModelName)
				ENDIF
				
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psAmanda.PedIndex, "Amanda", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, psAmanda.ModelName)
				ENDIF
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				STOP_AUDIO_SCENES()
				START_CUTSCENE(CUTSCENE_CREATE_OBJECTS_AT_SCENE_ORIGIN)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					SET_JIMMY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_TRACEY_CUTSCENE_COMPONENT_VARIATIONS()
					SET_AMANDA_CUTSCENE_COMPONENT_VARIATIONS()
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1
		
			IF IS_CUTSCENE_PLAYING()
			
				VEHICLE_INDEX LastPlayerVehicleIndex
				
				LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
				
				IF DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
				
					IF NOT IS_ENTITY_DEAD(LastPlayerVehicleIndex)
					
						//if last player vehicle is in the cutscene area and would interfere with cutscene entities
						IF IS_ENTITY_IN_ANGLED_AREA(LastPlayerVehicleIndex, <<-822.864136,175.135193,69.303169>>, <<-815.251099,178.129684,76.153091>>, 8.0)
						
							IF NOT IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
								SET_ENTITY_AS_MISSION_ENTITY(LastPlayerVehicleIndex)
							ENDIF
							
							STOP_VEHICLE_FIRE(LastPlayerVehicleIndex)
							
							SET_ENTITY_COORDS(LastPlayerVehicleIndex, vsMichaelsCar.vPosition)
							SET_ENTITY_HEADING(LastPlayerVehicleIndex, vsMichaelsCar.fHeading)
							
							SET_VEHICLE_DOORS_SHUT(LastPlayerVehicleIndex, TRUE)
							SET_VEHICLE_ON_GROUND_PROPERLY(LastPlayerVehicleIndex)
							SET_VEHICLE_ENGINE_ON(LastPlayerVehicleIndex, FALSE, TRUE)
						
						ENDIF
	
						IF GET_DISTANCE_BETWEEN_COORDS(vMichaelsHousePosition, GET_ENTITY_COORDS(LastPlayerVehicleIndex)) < 15.0
						
							IF NOT IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
								SET_ENTITY_AS_MISSION_ENTITY(LastPlayerVehicleIndex)
							ENDIF
							
							STOP_VEHICLE_FIRE(LastPlayerVehicleIndex)
							
							SET_ENTITY_COORDS(LastPlayerVehicleIndex, vsMichaelsCar.vPosition)
							SET_ENTITY_HEADING(LastPlayerVehicleIndex, PICK_FLOAT(bVehicleReversed, 136.4846, vsMichaelsCar.fHeading))
							SET_VEHICLE_ON_GROUND_PROPERLY(LastPlayerVehicleIndex)
							SET_VEHICLE_DOORS_SHUT(LastPlayerVehicleIndex, TRUE)
							SET_VEHICLE_ENGINE_ON(LastPlayerVehicleIndex, FALSE, TRUE)
							
									
							SET_VEHICLE_LOD_MULTIPLIER(LastPlayerVehicleIndex, 2.0)
							REQUEST_VEHICLE_HIGH_DETAIL_MODEL(LastPlayerVehicleIndex)
							
							IF DOES_VEHICLE_OVERLAP_ANGLED_AREA(LastPlayerVehicleIndex, <<-828.601990,176.916138,67.952744>>,
																<<-822.840515,183.031448,74.878098>>, 4.0)
								
								//reposition last player vehicle across the street
								SET_ENTITY_COORDS(LastPlayerVehicleIndex, << -867.9103, 158.2215, 63.9014 >>)
								SET_ENTITY_HEADING(LastPlayerVehicleIndex, 174.2918)
								
								SET_VEHICLE_DOORS_SHUT(LastPlayerVehicleIndex, TRUE)
								SET_VEHICLE_ON_GROUND_PROPERLY(LastPlayerVehicleIndex)
								SET_VEHICLE_ENGINE_ON(LastPlayerVehicleIndex, FALSE, TRUE)
												
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
				CLEAR_AREA_OF_PEDS(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_OBJECTS(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_PROJECTILES(MissionPosition.vPosition, 100.0)
				REMOVE_PARTICLE_FX_IN_RANGE(MissionPosition.vPosition, 100.0)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex) AND NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
				SET_FORCE_HD_VEHICLE(vsMichaelsCar.VehicleIndex, TRUE)
				SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vsMichaelsCar.VehicleIndex, TRUE)
			ENDIF
		
			IF NOT DOES_CAM_EXIST(ScriptedCamera)

				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				SET_CAM_PARAMS(ScriptedCamera, <<-844.676208,180.998871,73.017410>>,<<5.071437,-0.000056,-101.571411>>,33.000000)
				SET_CAM_DOF_PLANES(ScriptedCamera, 4.3662, 8.608, 1000.913, 10069.130)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
		
			IF HAS_CUTSCENE_FINISHED()
				REPLAY_STOP_EVENT()
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			
				iStageProgress++
				
			ENDIF
			
		BREAK
		
		CASE 3
		
			IF DOES_CAM_EXIST(ScriptedCamera)
				IF IS_CAM_RENDERING(ScriptedCamera)
				
					TRIGGER_MISSION_STATS_UI(TRUE, TRUE)
					
					iStageProgress++
					
				ENDIF	
			ENDIF
		
		BREAK
		
		CASE 4
		
			IF DOES_CAM_EXIST(ScriptedCamera)
				IF IS_CAM_RENDERING(ScriptedCamera)
		
					IF ( g_bResultScreenDisplaying = TRUE )
					
						iStageProgress++
					
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 5
		
			IF DOES_CAM_EXIST(ScriptedCamera)
				IF IS_CAM_RENDERING(ScriptedCamera)
		
					IF ( g_bResultScreenDisplaying = FALSE )
				
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						iStageProgress++
				
					ENDIF
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 6
			
			IF NOT IS_REPEAT_PLAY_ACTIVE()
			
				RETURN TRUE
			
			ELSE
			
				IF IS_SCREEN_FADED_IN()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
			
				IF IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
				
					RETURN TRUE
				
				ENDIF
			
			ENDIF
		
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

//|============================= END MISSION STAGES FUNCTIONS ============================|

//|=================================== MAIN SCRIPT LOOP ==================================|

SCRIPT

    SET_MISSION_FLAG (TRUE)
	
	//check for death or arrest
    IF HAS_FORCE_CLEANUP_OCCURRED() 

		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		
        MISSION_CLEANUP()
		
		TERMINATE_THIS_THREAD()
		
    ENDIF
	
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_MISSION_STAGE_MENU()
		CREATE_DEBUG_WIDGETS()
	#ENDIF
	
	IF IS_REPLAY_IN_PROGRESS()
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission replay is in progress.")
		#ENDIF
	
		SET_MISSION_STAGE_FOR_REPLAY(eMissionStage, GET_REPLAY_MID_MISSION_STAGE())
		
		bReplayFlag = TRUE
	
		//handle shitskip message
		IF ( g_bShitSkipAccepted = TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip accepted by the player.")
			#ENDIF
		
			eMissionStage = GET_NEXT_MISSION_STAGE_FOR_SHITSKIP(eMissionStage)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip changed mission stage to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
			#ENDIF
		
		ELSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip not triggered.")
			#ENDIF
		
		ENDIF

	ELSE
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_TO_BEAN_MACHINE), "STAGE 0: DRIVE TO BEAN", FALSE)
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active.")
		#ENDIF

		IF NOT IS_REPLAY_IN_PROGRESS()
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active and mission replay is not in progress.")
			#ENDIF
		
			eMissionStage = MISSION_STAGE_CUTSCENE_INTRO_P1
		
			bReplayFlag = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting local replay flag to TRUE for stage loading.")
			#ENDIF
					
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission stage is ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), " for repeat play and no mission replay.")
			#ENDIF
		
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active and mission replay is in progress.")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission stage is ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), " for repeat play and mission replay.")
			#ENDIF
		
		ENDIF

	ENDIF
	
	// PC custom keyboard and mouse control scheme
	bPCControlsSetup = FALSE

	WHILE TRUE
	
		//check each frame if mission is failed due to fail conditions
		//check only if mission stage loading has finished
		//check mission stats for vehicle damage and vehicle speed
		IF ( bLoadingFlag = TRUE )
			RUN_FAIL_CHECKS(eMissionStage, eMissionFail)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
				ELSE
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL) 
				ENDIF
			ENDIF
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ReuniteFamily")

		SWITCH eMissionStage
			
			CASE MISSION_STAGE_PRE_INTRO
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_PRE_INTRO_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()	
						eMissionStage = MISSION_STAGE_CUTSCENE_INTRO_P1
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_INTRO_P1
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_INTRO_P1_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()	
						eMissionStage = MISSION_STAGE_TIMELAPSE
					ENDIF
				ENDIF
			BREAK
		
			CASE MISSION_STAGE_TIMELAPSE
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_TIMELAPSE_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()	
						eMissionStage = MISSION_STAGE_CUTSCENE_INTRO_P2
					ENDIF
				ENDIF
			BREAK
		
			CASE MISSION_STAGE_CUTSCENE_INTRO_P2
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)		
					IF IS_MISSION_STAGE_CUTSCENE_INTRO_P2_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
					ENDIF
				ENDIF
			BREAK

			CASE MISSION_STAGE_DRIVE_TO_BEAN_MACHINE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBeanMachinePosition) < 150
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
				ENDIF
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_DRIVE_TO_BEAN_MACHINE_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_BEAN_MACHINE
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BEAN_MACHINE
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)				
					IF IS_MISSION_STAGE_CUTSCENE_BEAN_MACHINE_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_DRIVE_TO_TRACEY
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_DRIVE_TO_TRACEY
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_DRIVE_TO_TRACEY_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_SHOP_1
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_SHOP_1
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_SHOP_1_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_PIERCE_LAZLOW
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_PIERCE_LAZLOW
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_PIERCE_LAZLOW_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_SHOP_2
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_SHOP_2
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_SHOP_2_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_TATTOO_LAZLOW
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_TATTOO_LAZLOW
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_TATTOO_LAZLOW_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_SHOP_3
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_SHOP_3
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_SHOP_3_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUT_LAZLOW_HAIR
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUT_LAZLOW_HAIR
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUT_LAZLOW_HAIR_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_SHOP_4
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_SHOP_4
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.75)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.50)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_SHOP_4_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_DRIVE_TO_SHRINK
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_DRIVE_TO_SHRINK
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_DRIVE_TO_SHRINK_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_SHRINK
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_SHRINK
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.75)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_SHRINK_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_DRIVE_HOME
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_DRIVE_HOME
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_DRIVE_HOME_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_END
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_END
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_END_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_PASSED
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_PASSED
			
				CLEAR_HELP()
				CLEAR_PRINTS()
				KILL_ANY_CONVERSATION()
			
				MISSION_FLOW_MISSION_PASSED()
				
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					TRIGGER_SPECIFIC_SWITCH_AND_WAIT(PR_SCENE_T_NAKED_ISLAND , ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO))
					UpdatePostMissionInfo(g_savedGlobals.sPlayerData.sInfo, CHAR_MICHAEL, PR_SCENE_Ma_FAMILY6)	//#1570801
				ENDIF
				
				MISSION_CLEANUP()
				TERMINATE_THIS_THREAD()
				
			BREAK
			
			CASE MISSION_STAGE_FAILED
			
				SET_MISSION_FAILED_WITH_REASON(eMissionFail)
				
				CLEAR_HELP()
				CLEAR_PRINTS()
				KILL_ANY_CONVERSATION()
				
				WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
					WAIT(0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for GET_MISSION_FLOW_SAFE_TO_CLEANUP() to return TRUE.")
					#ENDIF
				ENDWHILE

				MISSION_CLEANUP()
				TERMINATE_THIS_THREAD()
			BREAK
			
		ENDSWITCH

		#IF IS_DEBUG_BUILD																					//handle debug functionality
			RUN_DEBUG_PASS_AND_FAIL_CHECK(eMissionStage, eMissionFail)										//handle key_f and key_s presses
			RUN_DEBUG_MISSION_STAGE_MENU(iReturnStage, eMissionStage, bSkipFlag, bStageResetFlag)			//handle z-skip menu
			RUN_DEBUG_SKIPS(eMissionStage, bSkipFlag, bStageResetFlag)										//handle j-skip and p-skip
		#ENDIF

		WAIT(0)
	
    ENDWHILE
	
ENDSCRIPT
