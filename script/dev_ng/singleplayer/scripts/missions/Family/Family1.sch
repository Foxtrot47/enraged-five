


VECTOR v_cam_point_offset
VECTOR v_cam_last_point_offset

/// PURPOSE:
///    Converges a value towards a given destination, by adding/removing a given amount.
PROC CONVERGE_VALUE(FLOAT &val, FLOAT desired_val, FLOAT amount_to_converge, BOOL adjust_for_framerate = FALSE)
      IF val != desired_val
            FLOAT converge_amount_this_frame = amount_to_converge
            IF adjust_for_framerate
                  converge_amount_this_frame = 0.0 +@ amount_to_converge
            ENDIF
      
            IF val - desired_val > converge_amount_this_frame
                  val -= converge_amount_this_frame
            ELIF val - desired_val < -converge_amount_this_frame
                  val += converge_amount_this_frame
            ELSE
                  val = desired_val
            ENDIF
      ENDIF
ENDPROC

PROC RESET_CAM_SHAKE_VARIABLES()
      v_cam_last_point_offset = <<0.0, 0.0, 0.0>>
      v_cam_point_offset = <<0.0, 0.0, 0.0>>
ENDPROC

/// PURPOSE:
///    Moves the cam's rotation randomly according to the given parameters.
/// PARAMS:
///    cam - The camera
///    origin - The rotation that the camera would normally be at if it wasn't shaking.
///    min_offset - The minimum difference between the origin and the desired shake rotation (in degrees)
///    max_offset - The maximum difference between the origin and the desired shake rotation (in degrees)
///    min_speed - The minimum shake speed
///    max_speed - The maximum shake speed
///    
FLOAT f_point_offset_speed
///    
PROC SHAKE_CAM_ROTATION(CAMERA_INDEX cam, VECTOR origin, FLOAT min_offset, FLOAT max_offset, FLOAT min_speed, FLOAT max_speed, BOOL b_rotate_y = FALSE)
      IF v_cam_last_point_offset.x = v_cam_point_offset.x 
            v_cam_point_offset.x = GET_RANDOM_FLOAT_IN_RANGE(min_offset, max_offset)
            IF b_rotate_y
                  v_cam_point_offset.y = GET_RANDOM_FLOAT_IN_RANGE(min_offset, max_offset)
            ENDIF
            v_cam_point_offset.z = GET_RANDOM_FLOAT_IN_RANGE(min_offset, max_offset)
            
            f_point_offset_speed = GET_RANDOM_FLOAT_IN_RANGE(min_speed, max_speed)
      ELSE
            //The speed ratio is used to scale speed so that every axis reaches its target at the same moment.
            FLOAT f_speed_ratio
            VECTOR v_diff = <<ABSF(v_cam_last_point_offset.x - v_cam_point_offset.x), ABSF(v_cam_last_point_offset.y - v_cam_point_offset.y), ABSF(v_cam_last_point_offset.z - v_cam_point_offset.z)>>
            
            IF v_diff.x > v_diff.y AND v_diff.x > v_diff.z
                  f_speed_ratio = f_point_offset_speed / v_diff.x
            ELIF v_diff.y > v_diff.z
                  f_speed_ratio = f_point_offset_speed / v_diff.y
            ELSE
                  f_speed_ratio = f_point_offset_speed / v_diff.z
            ENDIF
            
            IF f_speed_ratio > 1.0
                  f_speed_ratio = 1.0
            ENDIF
      
            CONVERGE_VALUE(v_cam_last_point_offset.x, v_cam_point_offset.x, f_point_offset_speed)
            IF b_rotate_y
                  CONVERGE_VALUE(v_cam_last_point_offset.y, v_cam_point_offset.y, f_speed_ratio * v_diff.y)
            ENDIF
            CONVERGE_VALUE(v_cam_last_point_offset.z, v_cam_point_offset.z, f_speed_ratio * v_diff.z)
      ENDIF
      
      SET_CAM_ROT(cam, origin + v_cam_last_point_offset)
ENDPROC




PROC SHAKE_CAM_POINT_OFFSET(CAMERA_INDEX cam, ENTITY_INDEX entity, VECTOR origin, FLOAT min_offset, FLOAT max_offset, FLOAT min_speed, FLOAT max_speed)   
      IF v_cam_last_point_offset.x = v_cam_point_offset.x 
      AND v_cam_last_point_offset.y = v_cam_point_offset.y  
      AND v_cam_last_point_offset.z = v_cam_point_offset.z
            v_cam_point_offset.x = GET_RANDOM_FLOAT_IN_RANGE(min_offset, max_offset)
            v_cam_point_offset.y = GET_RANDOM_FLOAT_IN_RANGE(min_offset, max_offset)
            v_cam_point_offset.z = GET_RANDOM_FLOAT_IN_RANGE(min_offset, max_offset)
            
            f_point_offset_speed = GET_RANDOM_FLOAT_IN_RANGE(min_speed, max_speed)
      ELSE
            CONVERGE_VALUE(v_cam_last_point_offset.x, v_cam_point_offset.x, f_point_offset_speed)
            CONVERGE_VALUE(v_cam_last_point_offset.y, v_cam_point_offset.y, f_point_offset_speed)
            CONVERGE_VALUE(v_cam_last_point_offset.z, v_cam_point_offset.z, f_point_offset_speed)
      ENDIF
      
      IF NOT IS_ENTITY_DEAD(entity)
	  	IF DOES_CAM_EXIST(cam)
           	POINT_CAM_AT_ENTITY(cam, entity, origin + v_cam_last_point_offset)
		ENDIF
      ENDIF
ENDPROC

PROC RESET_GAME_CAMERA(FLOAT fHeading = 0.0)

	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(fHeading)

ENDPROC


