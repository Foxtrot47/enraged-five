
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//yachtBattle.sc
//Ross Wallace 31/03/2010

USING "rage_builtins.sch"
USING "commands_clock.sch"
USING "globals.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "blip_control_public.sch"
// ____________________________________ INCLUDES ___________________________________________
USING "commands_script.sch"
USING "chase_hint_cam.sch"
USING "CompletionPercentage_public.sch"

//USING "general_include.sch"
using "commands_pad.sch"
using "commands_misc.sch"
using "commands_player.sch"
USING "Commands_streaming.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "script_heist.sch"
USING "commands_fire.sch"
USING "commands_interiors.sch"
USING "script_MISC.sch"
USING "commands_object.sch"
USING "commands_graphics.sch"
USING "script_ped.sch"
USING "chase_hint_cam.sch"
USING "script_blips.sch" 
USING "commands_graphics.sch"
USING "locates_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "script_maths.sch"
USING "mission_stat_public.sch"
USING "cutscene_public.sch"
USING "cam_recording_public.sch"
using "selector_public.sch"
using "shop_public.sch"
USING "taxi_functions.sch"
USING "timeLapse.sch"
USING "family_public.sch"
USING "shop_public.sch"
USING "clearMissionArea.sch"
USING "randomChar_public.sch"
USING "commands_recording.sch"

//VECTOR vSittingPos = <<-782.1956, 187.6187, 72.2029>>
//VECTOR vSittingRot

FLOAT fBehindBoatCheckValue = 500.0

BOOL bProcessCinematicCamera = TRUE
BOOL bBlipsFlashing = FALSE
BOOL bForceOffAfter1Box = TRUE
BOOL bStopScenePoppingToSideOfBoat = FALSE

VEHICLE_INDEX cameraCar
VEHICLE_INDEX cameraCarCamera

SELECTOR_PED_STRUCT pedSelector

PTFX_ID ptfxCarSmoke

//INT iTimeOfLastSegmentGrab
//INT segmentClosestToPlayer

FLOAT fVehicleActualSpeed

CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					144
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					35	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				28	
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		6
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK		7
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		5
USING "traffic.sch"

USING "family1.sch"
#IF IS_DEBUG_BUILD 
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF


BOOL bUberInit
BOOL bInitAccelReading = FALSE

BOOL bCloseToYachtMusicEvent = FALSE
BOOL bMusicEvent1 = FALSE
BOOL bMusicEvent2 = FALSE
BOOL bMusicEvent3 = FALSE
BOOL bMusicEvent4 = FALSE

BOOL bJimmyCaughtDialoguePlayed = FALSE

//BOOL bMusicStarted = FALSE

BOOL bSceneRestarted
BOOL bFranklinWaving
INT iLeanStage

VECTOR vecDesiredOffset
VECTOR vecFranklinCoords
VECTOR vecActualOffset

INT iRandomWaveTime = 2500
INT iTimeOfLastConstantDialogue
FLOAT prevSyncPhase

STRING constantDialogue
BOOL bDoDialogue = TRUE

INT	iControlBoxes
INT iNumberOfBoxesThrown

INT iControlGunshots
INT iJimmysLegState
INT iProgressPoint
INT iRageText
INT iCatchJimmySubstages
INT iSmashDialogues
INT iCameraCuts

INT iTimeOfBadGuy2Death
INT iDelayAfterBadGuysDeath  = 1200


FLOAT fRecordingProgressPointsWhereJimmyLiftsLegs[4]

BOOL bFlagGetBackInCar
BOOL bFlagGetBackInText

BOOL bAlreadyWarnedWeAreLosingYacht
BOOL bPlayInitialDialogue

BOOL bJimmySaysdadSpeedh

INT iTimeOfPreloadedJimXSpeech = -1

BOOL bMeetingConvoPreloaded = FALSE
BOOL bJimmyIsHitByBoom

REL_GROUP_HASH thievesRelGroup

LOCATES_HEADER_DATA sLocatesData
BOOL bCustomGPSActive

INT missionStartTime

INT iTimeOfBoxThrowerShot
BOOL bCinecameraUsed
BOOL bPrintCinematicCameraHelpText = FALSE

BOOL bCollisionHappening

BOOL bNearMissCouldHaveHappened	
INT iMissStartTime

BOOL bHasChanged      //variable for storing the state of “player has changed outfit”

// ____________________________________ VARIABLES __________________________________________
INT iGetIntoCarStage
INT iGetToLocation
INT iDontShootChat
INT iTakeCarToChopShopStage
INT i_current_event
INT iSetPieceFlatbed
INT iFirstTrafficCar
INT iSecondTrafficCar
INT iWrestleDialogue
INT iChopShopStage
INT iDropOffStage
INT iTakeJimmyHomeStage
INT iFailCutsceneStage

BLIP_DISPLAY bDisplayBlipMichaelSon
BLIP_DISPLAY bDisplayBlippedBadguy2
BLIP_DISPLAY bDisplayBlippedBadGuy3BoxThrower
//BLIP_DISPLAY bDisplayBlipTruckYachtPacker
BLIP_DISPLAY bDisplayBlipTruckYachtPackerTrailer


//	STAGE_INITIALISE
CONST_FLOAT WAYPOINT_PAUSE_DISTANCE		14.0
CONST_FLOAT WAYPOINT_RESUME_DISTANCE	5.5
CONST_INT	WAYPOINT_CHECK_INTERVAL		2000

INT iCameraSelection

CAMERA_INDEX cutsceneCamera
CAMERA_INDEX initialCam
CAMERA_INDEX destinationCam
CAMERA_INDEX cameraRecorded1
CAMERA_INDEX cameraRecorded2

INT carColour1 = 28

//	STAGE_INTRO_MOCAP_CUT
//INTERIOR_INSTANCE_INDEX intMichaelsHouse

//	STAGE_GET_INTO_AMANDAS_CAR
BLIP_INDEX blipPlayersCar


//	STAGE_GET_TO_LOCATION
VECTOR vMissionLocation = <<-2151.2722, -268.0492, 12.8775>>//<< -2167.3977, -323.2864, 12.1113 >> //<< -2169.33, -296.17 , 12.3926 >>
BOOL bPreStreamCut1 = FALSE
BOOL bPreStreamCut2 = FALSE
BOOL bUberRecReadyToGo = FALSE
BOOL bStartAudioScene = FALSE

//	STAGE_YACHT_GOES_BY_ON_PCH
//BOOL bCarBlendingIntoGamePlayAfterCut

//	STAGE_DO_CHASE
//FLOAT fDistanceToYachtBoardingPoint
//FLOAT fSlowDownForce

FLOAT fBoomratio

BOOL bToldTokeepCarSteady
FLOAT fJumpMaxSpeedVariance = 5.0
//BOOL bJimmyWhineConvoPlayed

BLIP_INDEX blippedBadguy2
BLIP_INDEX blippedBadGuy3BoxThrower
//BLIP_INDEX blipTruckYachtPacker
BLIP_INDEX blipMichaelSon
BLIP_INDEX blipFranklin

OBJECT_INDEX oiBlipObject
//OBJECT_INDEX oiBlipObjectCatchJimmy
//VECTOR vCatchyJimmyMarker

VEHICLE_INDEX carPlayersJeep
VEHICLE_INDEX carTracey
VEHICLE_INDEX carMichael
VEHICLE_INDEX truckYachtPacker
VEHICLE_INDEX truckYachtPackerTrailer
VEHICLE_INDEX TrafficCarTrailer[TOTAL_NUMBER_OF_TRAFFIC_CARS]
VEHICLE_INDEX SetPieceCarTrailer[TOTAL_NUMBER_OF_SET_PIECE_CARS]

PED_INDEX pedYachtPacker

PED_INDEX pedBadguy1
PED_INDEX pedBadguy2
PED_INDEX pedBadGuy3BoxThrower
PED_INDEX pedBadguy4BoomGuy
PED_INDEX pedJimmy

OBJECT_INDEX oiMast
OBJECT_INDEX oiChampagneBox


BOOL bPedGuy1Appeared
BOOL bPedGuy1DoorOpened = FALSE

BOOL bFranklinShoutedForHelp
BOOL bBoomBaddieDialogueDisplayed
BOOL bDialogueForLandingOnYacht
BOOL bJumpMusicCue

BOOL bFakeBloodInCutscene
//BOOL bSafeToEndMission
BOOL bFranklinNotSaidSpeechThatWontWork
BOOL bCutOfBoxThrowerOver
BOOL bFranklinTellsToGetEnderJImmyOnce
BOOL bJimmyBoomHitIntroAnimComplete

INT iDealWithFailDialogue

INT sceneId = -1
INT sceneIDKeepBodyOnBoat = -1
INT sceneIdA = -1
INT sceneIdB = -1
INT sceneIDboomGuy = -1
//INT sceneIdBoomJimmy = -1
INT sceneIdSon = -1
INT sceneIdFailure = -1
INT sceneIDbonnetStatic = -1
INT sceneIDclimbOutOfCar = -1
INT sceneIDStumble = -1
INT sceneIDLegPullUp = -1

INT sceneIDPulledOverboard

INT headings_captured

VECTOR vTruckYachtPackerStartPosition =  << -2322.0911, -320.1779, 12.8207 >> 
FLOAT fTruckYachtPackerStartHeading = 240.7513 
//VECTOR vJeepLandingOffsetFranklin = <<2.0, -7.3, 0.0>>
//FLOAT fLandThresholdFranklin = 5.3
VECTOR vJeepStartPosition =  << -825.1512, 178.9821, 70.3781 >> //<< -823.5428, 181.3025, 70.6662 >>  
FLOAT  fJeepStartHeading =  145.2545 //142.8150   
VECTOR vTruckRearOffset = <<0.0, -17.5, -0.5>>
VECTOR vCatchJimmyOffset = <<-7.0, 2.5, -1.0>>

FLOAT fJumpThreshold  = 3.0

//FLOAT fPLaybackSpeedNumerator = 26.0

FLOAT mainPlaybackSpeed = 1.0
FLOAT mainPlaybackSpeedModifiedForChasingCar = 1.0
FLOAT fDesiredPlaybackSpeed = 1.0
FLOAT fRecordingProgress
FLOAT heading_timer
FLOAT fRotationAccel_average
FLOAT fRotationAccel_total
FLOAT fRotation, fRotationPrev
FLOAT fRotationAccel
FLOAT fForwardSpeed
FLOAT fFowardAccel 
FLOAT fForwardSpeedPrev1
FLOAT fForwardSpeedPrev2
INT carHealthPrevious
FLOAT fLeftThreshold		=	-10.0
FLOAT fRightThreshold		=	10.0
//FLOAT fBackwardsThreshold	=	-0.3
FLOAT fSteadinessModifier	=	1.0

FLOAT fMinPlaybackSpeed  = 0.20
FLOAT fMaxPlaybackSpeed  = 0.85
VECTOR vTargetVehicleOffset
BLIP_INDEX blipTruckYachtPackerTrailer


BOOL bMichaelWarnsFranklinOfGuy2
OBJECT_INDEX seatHelperObject				
OBJECT_INDEX vectorXHelperObject
OBJECT_INDEX mastHelperObject
VECTOR vectorD
VECTOR rotationD
VECTOR vectorX
//VECTOR rotationX
VECTOR VectorJ 
//VECTOR rotationJ
VECTOR vectorS
VECTOR rotationS

BOOL bJimmyLanded

CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
CHASE_HINT_CAM_STRUCT		localChaseHintCamStructButtonPress

//	STAGE_TAKE_CAR_TO_CHOP_SHOP

//	STAGE_CLEANUP

//	STAGE_DEBUG
#IF IS_DEBUG_BUILD 

	CAM_RECORDING_DATA camRecData

	VECTOR vSlidePosition = <<0.0, 0.0, -0.3>>
	
	VECTOR AttachOffset = <<0.0, -2.0, 0.9>>
	VECTOR AttachRotation = <<0.0, 0.0, 0.0>>

	VECTOR forceVector = <<0.0, 0.0, 1.5>>
	VECTOR forceOffsetVector = <<1.0, 0.0, 0.0>>
		
	USING "select_mission_stage.sch"
	
	USING "shared_debug.sch"
	USING "script_debug.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 14                     // number of stages in mission + 2 (for menu )
	INT iReturnStage                                       // mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 

	VECTOR vJackKnifeForce = <<15.0, 0.0, 0.>>
	VECTOR vJackKnifeForceOffset = <<0.0, -6.0, 0.0>>

	BOOL bRecordTrailer
	BOOL bSaveRecording
	
	INT iDebugStage = 0 //13
	
	WIDGET_GROUP_ID family1WidgetGroup
	TEXT_WIDGET_ID dialogueThatShouldBePlaying
	
	BOOL bDebugOn = TRUE
	
	BOOL bcameraCarCameraAlive
	BOOL bcameraCarAlive
	
	FLOAT fSlideLimit = 2.75
	FLOAT fSlideIncrement = 0.001
	FLOAT fFrameMultiplier = 30.00
	
	INT iChaseStage
	INT iBalanceStage
	
	BOOL bDebugInitialised
	BOOL bRunDebugLocates
	VECTOR vLocateDimensions
	VECTOR vPlayersCoords
	
	INT iDebugMissionStage
	INT iDebugSkipMissionStage
	INT iDebugBoomBaddie
	BOOL bRunDebugAttachedCamera
		
	VEHICLE_INDEX attachVehicle1	
	VEHICLE_INDEX attachVehicle2
	
	BOOL bDEbugIsInFront
	
	BOOL bIsSuperDebugEnabled

	
#ENDIF

//BOOL bIsTrailerRunning

structPedsForConversation myScriptedSpeech

/// PURPOSE:Wrapper fro Franklin's ped index.
///    /
/// RETURNS:
///    
FUNC PED_INDEX pedFranklin()
	RETURN pedSelector.pedID[SELECTOR_PED_FRANKLIN]
ENDFUNC

PROC DO_SWITCH_EFFECT(enumCharacterList PedToSwitchTo)

	SWITCH PedToSwitchTo
		CASE CHAR_MICHAEL
			ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)	
		BREAK
		CASE CHAR_FRANKLIN
			ANIMPOSTFX_PLAY("SwitchSceneFranklin", 0, FALSE)
		BREAK
		CASE CHAR_TREVOR
			ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
		BREAK
	ENDSWITCH
	
	//Slot in sound effect
	PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")

ENDPROC


CONST_INT MAX_ROUTE_NODES 800
INT iClosest = 0

//Gets the closest section of an array of vectors
FUNC INT GET_CLOSEST_PATH_SECTION(VECTOR point, VECTOR &path[])
    INT iTemp
    
    FLOAT closest_distance = 99999
	FLOAT fDistance 
	  	  
	//PRINTLN("Scanning:", iClosest, " TO ", iSearchAhead)
	  
    FOR iTemp = iClosest TO MAX_ROUTE_NODES - 2 //iSearchAhead
        fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_CLOSEST_POINT_ON_LINE(point, path[iTemp], path[iTemp+1]), point)
				
		
		//fDistance = GET_DISTANCE_BETWEEN_COORDS(point, path[iTemp])
			
		IF  fDistance < closest_distance
			iClosest = itemp
            closest_distance = fDistance
        ENDIF
    ENDFOR
    RETURN iClosest
	
ENDFUNC

INT iClosestPlayer = 0

//Gets the closest section of an array of vectors
FUNC INT GET_CLOSEST_PATH_SECTION_FOR_PLAYER(VECTOR point, VECTOR &path[])
    INT iTemp
    
    FLOAT closest_distance = 99999
	FLOAT fDistance 
	  
	INT iSearchAhead
	  
	IF iSearchAhead < MAX_ROUTE_NODES
	  	iSearchAhead = (iClosestPlayer + 13)
		IF iSearchAhead >= MAX_ROUTE_NODES - 1
			iSearchAhead = MAX_ROUTE_NODES - 1
		ENDIF
	ELSE
		iSearchAhead = MAX_ROUTE_NODES - 1
	ENDIF
	  
	//PRINTLN("Scanning:", iClosest, " TO ", iSearchAhead)
	  
    FOR iTemp = iClosestPlayer TO iSearchAhead
            //fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_CLOSEST_POINT_ON_LINE(point, path[iTemp], path[iTemp+1]), point)
						
		fDistance = GET_DISTANCE_BETWEEN_COORDS(point, path[iTemp], FALSE)
			
		IF  fDistance < closest_distance
			iClosestPlayer = itemp
            closest_distance = fDistance
        ENDIF
    ENDFOR
	
	//if its more than the stray-from-a-node-fail distances - do a full scan incase a skip has occured.
	IF fDistance > 65.0
		closest_distance = 99999
		iClosestPlayer = 0
		//PRINTLN("*** FULL SCAN - FAILD DISTANCE APPROACHING - Scanning:", iClosestPlayer, " TO ", MAX_ROUTE_NODES - 1)
		FOR iTemp = iClosestPlayer TO MAX_ROUTE_NODES - 1						
			fDistance = GET_DISTANCE_BETWEEN_COORDS(point, path[iTemp], FALSE)		
			IF  fDistance < closest_distance
				iClosestPlayer = itemp
	            closest_distance = fDistance
	        ENDIF
    	ENDFOR
	ENDIF
	
    RETURN iClosestPlayer
	
ENDFUNC


VECTOR route_path[MAX_ROUTE_NODES]
BOOL bForceCinematicCamera
BOOL bForceCutsceneCameraAsCinematicCamera
BOOL bForceActionCameraAsCinematicCamera

BOOL bFranklinLandAudioPlayed

INT iTimeOfBoomBanddieShot

//FLOAT fSwitchToRecordingPlaybackSpeedModifier

//draws a car recording (LOW RES)
PROC DRAW_UBER_ROUTE_DEBUG_PATH(VECTOR &path[], INT red = 0, INT green = 0, INT blue = 255)
      INT iTemp
     
	 SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	  
	  REPEAT COUNT_OF(path) iTemp
	  		#IF IS_DEBUG_BUILD
				IF bISSuperDebugEnabled
            		DRAW_DEBUG_SPHERE(path[iTemp], 0.2, red, green, blue, 170)
				ENDIF
			#ENDIF
		//	DRAW_*SPHERE(path[iTemp], 0.2)
            IF iTemp > 0
                  DRAW_DEBUG_LINE(path[iTemp], path[iTemp-1], red, green, blue, 170)
            ENDIF
      ENDREPEAT
ENDPROC

//Builds an array of vectors for a car recording path
PROC BUILD_UBER_ROUTE_DEBUG_PATH(VECTOR &path[], INT FileNumber, STRING pRecordingName)
      IF HAS_VEHICLE_RECORDING_BEEN_LOADED(FileNumber, pRecordingName)
            FLOAT duration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(FileNumber, pRecordingName)
            FLOAT route_segment = duration/(MAX_ROUTE_NODES)
            INT iTemp
            path[MAX_ROUTE_NODES-1] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, duration, pRecordingName)
            REPEAT COUNT_OF(path) iTemp
                  path[iTemp] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, route_segment*iTemp, pRecordingName)
            ENDREPEAT
      ENDIF
ENDPROC


INT iFrameCount
FLOAT durationMF
FLOAT route_segmentMF
#IF IS_DEBUG_BUILD
INT iTimeOfStartExecution
#ENDIF
//
//PURPOSE: Builds an array of vectors for a car recording path, does it over multiple frames, returns TRUE when done.
FUNC BOOL BUILD_UBER_ROUTE_DEBUG_PATH_OVER_MULTIPLE_FRAMES(VECTOR &path[], INT FileNumber, STRING pRecordingName)
	
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(FileNumber, pRecordingName) 
	AND iFrameCount > -1
	  	IF iFrameCount = 0
			durationMF = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(FileNumber, pRecordingName)
	    	route_segmentMF = durationMF/(MAX_ROUTE_NODES)
			path[MAX_ROUTE_NODES-1] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, durationMF, pRecordingName)
		
			#IF IS_DEBUG_BUILD
			iTimeOfStartExecution = GET_GAME_TIMER()
			#ENDIF
		ENDIF
		
	    IF iFrameCount < MAX_ROUTE_NODES - 1
	        //Let's do 4 a frame
			path[iFrameCount] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, route_segmentMF*iFrameCount, pRecordingName)
			
			IF (iFrameCount + 1) < MAX_ROUTE_NODES - 1
				path[iFrameCount + 1] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, route_segmentMF*(iFrameCount + 1), pRecordingName)		
			ENDIF
			IF (iFrameCount + 2) < MAX_ROUTE_NODES - 1
				path[iFrameCount + 2] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, route_segmentMF*(iFrameCount + 2), pRecordingName)
			ENDIF
			IF (iFrameCount + 3) < MAX_ROUTE_NODES - 1
				path[iFrameCount + 3] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, route_segmentMF* (iFrameCount + 3), pRecordingName)
			ENDIF
						
			iFrameCount += 4
			RETURN FALSE
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("*** BUILD_UBER_ROUTE_DEBUG_PATH_OVER_MULTIPLE_FRAMES COMPLETE!! ***")
				PRINTLN("Time take to execute: ", (GET_GAME_TIMER() - iTimeOfStartExecution))			
			#ENDIF
			iFrameCount = -1
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	  
ENDFUNC

PROC BUILD_UBER_ROUTE_DEBUG_PATH_OVER_MULTIPLE_FRAMES_RESET()
	iFrameCount = 0
ENDPROC



///// PURPOSE: Given a position this will return the time along a recording of the closest position to the recording from the input
  ///    /
  /// PARAMS: 
  ///    position - position passed for query
  ///    FileNumber - file number of the recording
  ///    pRecordingName - name of the recording
  ///    recording_path - An array of vectors forming the recording nodes
  /// RETURNS: Float representing time along recoridng
  ///    NOTE: the recording must already be loaded and the recording path built WORKS ONLY FOR PLAYER
FUNC FLOAT GET_TIME_IN_RECORDING_FROM_POSITION(VECTOR position, INT FileNumber, STRING pRecordingName, VECTOR &recording_path[])
	FLOAT segment_time
	FLOAT current_segment_distance
	INT segments_passed
	VECTOR closest_point_on_segment
	FLOAT current_segment_progress_distance
	FLOAT current_segment_progress_in_time_step
	
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(FileNumber, pRecordingName)
		segment_time = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(FileNumber, pRecordingName)/(MAX_ROUTE_NODES)
		segments_passed = GET_CLOSEST_PATH_SECTION(position, recording_path)
		current_segment_distance = GET_DISTANCE_BETWEEN_COORDS(recording_path[segments_passed], recording_path[segments_passed+1])
		closest_point_on_segment = GET_CLOSEST_POINT_ON_LINE(position, recording_path[segments_passed], recording_path[segments_passed+1])
		current_segment_progress_distance = GET_DISTANCE_BETWEEN_COORDS(recording_path[segments_passed], closest_point_on_segment)          
		current_segment_progress_in_time_step = (current_segment_progress_distance/current_segment_distance)*segment_time
	ELSE
	    SCRIPT_ASSERT("Vehicle recording has not been loaded, pleas load the recording before calling GET_TIME_IN_RECORDING_FROM_POSITION()")
	ENDIF
	
	RETURN current_segment_progress_in_time_step + (segments_passed*segment_time)
      
ENDFUNC

FUNC FLOAT GET_TIME_IN_RECORDING_FROM_POSITION_NOT_ACCURATE(VECTOR position, INT FileNumber, STRING pRecordingName, VECTOR &recording_path[])
	FLOAT segment_time
	INT segments_passed
		
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(FileNumber, pRecordingName)
		segment_time = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(FileNumber, pRecordingName)/(MAX_ROUTE_NODES)
		segments_passed = GET_CLOSEST_PATH_SECTION(position, recording_path)
		
	ELSE
	    SCRIPT_ASSERT("Vehicle recording has not been loaded, pleas load the recording before calling GET_TIME_IN_RECORDING_FROM_POSITION()")
	ENDIF
	
	RETURN (segments_passed*segment_time)
      
ENDFUNC

// ____________________________________ FUNCTIONS __________________________________________

ENUM MISSION_STAGE_FLAG 

	STAGE_TIME_LAPSE,						//0
	STAGE_INITIALISE,						//1
	STAGE_INTRO_MOCAP_CUT,					//2
	STAGE_GET_INTO_AMANDAS_CAR,				//3
	STAGE_GET_TO_LOCATION,					//4
	//STAGE_YACHT_GOES_BY_ON_PCH,				//X
	STAGE_DO_CHASE,							//5
	STAGE_JIMMY_APPEARS,					//6
	STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT,	//7
	STAGE_TAKE_CAR_TO_CHOP_SHOP,			//8
	STAGE_CHOP_SHOP_CUTSCENE, 				//9
	STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME,					//10
	STAGE_DROP_OFF_CUTSCENE,				//11
	STAGE_FAIL_CUTSCENE,					//12
	STAGE_DEBUG								//13

ENDENUM


MISSION_STAGE_FLAG mission_stage =  STAGE_TIME_LAPSE //STAGE_INITIALISE //STAGE_TIME_LAPSE

ENUM CHASE_STAGE_FLAG 

	CHASE_SETUP, 							//0
	CHASE_TRIGGER_MUSIC,					//1
	CHASE_SAY_INITIAL_LINE,					//2
	CHASE_FRANKLIN_STANDS_UP,				//3
	CHASE_FRANKLIN_STANDS_LOOP,				//4
	CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR,		//5
	CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR_LOOP,	//6
	CHASE_WAIT_A_FRAME_TO_STOP_TWO_ANIM_UPDATES,	//7
	CHASE_FRANKLIN_JUMPS_ON_YACHT,			//8
	CHASE_FRANKLIN_BOARDS_INTRO,			//9
	CHASE_FRANKLIN_BOARDS_LOOP,				//10
	CHASE_FRANKLIN_BOARDS_OUTRO,			//11
	CHASE_STRUGGLE_1,						//12
	CHASE_STRUGGLE_1_LOOP,					//13
	CHASE_STRUGGLE_1_UPDATE,				//14
	CHASE_STRUGGLE_1_OUTRO,					//15
	CHASE_HIDE_BEHIND_WHEEL_INTRO,			//16
	CHASE_HIDE_BEHIND_WHEEL_LOOP,			//17
	CHASE_STRUGGLE_2,						//18
	CHASE_STRUGGLE_2_LOOP,					//19
	CHASE_STRUGGLE_2_OUTRO,					//20
	CHASE_HIDE_BEHIND_WHEEL_2_INTRO,		//21
	CHASE_MEET_JIMMY,						//22
	CHASE_SIGNAL_PLAYER,					//23
	CHASE_BOOM_SWING,						//24
	CHASE_BOOM_SWING_LOOP,					//25
		CHASE_FRANKLIN_NOTIONS_TO_SAVE_JIMMY,	//26
		CHASE_CATCH_JIMMY_DETACH,				//27
		CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3, //28
	CHASE_FRANKLIN_RECOVERS,				//29
	CHASE_FRANKLIN_RECOVERS_LOOP,			//30
	CHASE_FRANKLIN_ANIM_ALLOW_ONE_FRAME,	//31
	CHASE_CREATE_SPEECH_FOR_FRANKLIN,		//32
	CHASE_CATCH_FRANKLIN,					//33
	CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR,	//34
	CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_2,	//35
	CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_UPDATE,//36
	CHASE_KEEP_CHASING,						//37
	CHASE_PUT_FRANKLIN_BACK_IN_CAR_PROPERLY,//38
	CHASE_NO_MORE_STAGES,					//39
	CHASE_NO_MORE_STAGES_2,					//40
	CHASE_FAILURE,							//41
	CHASE_FAILURE_LOST_YACHT,				//42
	CHASE_FAILURE_DESTROYED_CAR,			//43
	CHASE_FAILURE_STUCK_CAR,				//44
	CHASE_FAILURE_LOST_BMW,					//45
	CHASE_FAILURE_BLEW_COVER				//46
	
ENDENUM

CHASE_STAGE_FLAG eChaseStage = CHASE_SETUP //CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR //CHASE_FRANKLIN_STANDS_UP

ENUM BALANCE_STATE

	STATE_STATIONARY,
	STATE_BALANCE,
	STATE_JUMP_PREP,
	STATE_JUMP_PREP_LOOP,
	STATE_JUMP_PREP_CHECK_FOR_CANCEL,
	STATE_FALL_LEFT,
	STATE_FALL_RIGHT,
	STATE_FALL_FORWARDS,
	STATE_FALL_BACKWARDS,
	STATE_FALL_LEFT_FAIL,
	STATE_FALL_RIGHT_FAIL,
	STATE_FALL_FORWARDS_FAIL
	
ENDENUM

BALANCE_STATE eBalanceState = STATE_BALANCE



PROC setupTrafficFamily1()
// ****  UBER RECORDED TRAFFIC  **** 

TrafficCarPos[0] = <<-2104.2654, -382.9811, 12.1691>>
TrafficCarQuatX[0] = -0.0194
TrafficCarQuatY[0] = -0.0190
TrafficCarQuatZ[0] = 0.8467
TrafficCarQuatW[0] = -0.5314
TrafficCarRecording[0] = 979
TrafficCarStartime[0] = 1.0000
TrafficCarModel[0] = comet2

//TrafficCarPos[1] = <<-2196.7788, -340.6996, 12.8538>>
//TrafficCarQuatX[1] = 0.0016
//TrafficCarQuatY[1] = 0.0013
//TrafficCarQuatZ[1] = 0.6903
//TrafficCarQuatW[1] = 0.7235
//TrafficCarRecording[1] = 3
//TrafficCarStartime[1] = 10403.0000
//TrafficCarModel[1] = buffalo

TrafficCarPos[2] = <<-2111.3669, -372.7313, 12.7216>>
TrafficCarQuatX[2] = 0.0060
TrafficCarQuatY[2] = -0.0187
TrafficCarQuatZ[2] = 0.8281
TrafficCarQuatW[2] = -0.5602
TrafficCarRecording[2] = 674
TrafficCarStartime[2] = 12000.0000
TrafficCarModel[2] = blista

TrafficCarPos[3] = <<-2158.9729, -326.0327, 12.9229>>
TrafficCarQuatX[3] = 0.0006
TrafficCarQuatY[3] = -0.0129
TrafficCarQuatZ[3] = -0.0595
TrafficCarQuatW[3] = 0.9981
TrafficCarRecording[3] = 8
TrafficCarStartime[3] = 12515.0000
TrafficCarModel[3] = burrito

TrafficCarPos[4] = <<-2119.2563, -352.3024, 12.5434>>
TrafficCarQuatX[4] = 0.0085
TrafficCarQuatY[4] = -0.0024
TrafficCarQuatZ[4] = 0.5792
TrafficCarQuatW[4] = 0.8151
TrafficCarRecording[4] = 10
TrafficCarStartime[4] = 14561.0000
TrafficCarModel[4] = manana

//TrafficCarPos[5] = <<-2103.5061, -377.0059, 12.3290>>
//TrafficCarQuatX[5] = 0.0015
//TrafficCarQuatY[5] = -0.0022
//TrafficCarQuatZ[5] = 0.8440
//TrafficCarQuatW[5] = -0.5363
//TrafficCarRecording[5] = 11
//TrafficCarStartime[5] = 17287.0000 //16287.0000
//TrafficCarModel[5] = dilettante

TrafficCarPos[6] = <<-2000.9590, -458.8005, 11.1928>>
TrafficCarQuatX[6] = 0.0046
TrafficCarQuatY[6] = -0.0073
TrafficCarQuatZ[6] = 0.9001
TrafficCarQuatW[6] = -0.4356
TrafficCarRecording[6] = 681
TrafficCarStartime[6] = 16560.0000
TrafficCarModel[6] = issi2

TrafficCarPos[7] = <<-2066.1736, -391.4844, 11.1701>>
TrafficCarQuatX[7] = 0.0100
TrafficCarQuatY[7] = -0.0173
TrafficCarQuatZ[7] = 0.8823
TrafficCarQuatW[7] = -0.4703
TrafficCarRecording[7] = 12
TrafficCarStartime[7] = 16937.0000
TrafficCarModel[7] = buffalo

TrafficCarPos[8] = <<-2143.9626, -349.9859, 12.6447>>
TrafficCarQuatX[8] = -0.0008
TrafficCarQuatY[8] = 0.0009
TrafficCarQuatZ[8] = 0.5944
TrafficCarQuatW[8] = 0.8041
TrafficCarRecording[8] = 9
TrafficCarStartime[8] = 17439.0000
TrafficCarModel[8] = penumbra

TrafficCarPos[9] = <<-1960.1580, -491.4438, 11.6188>>
TrafficCarQuatX[9] = 0.0034
TrafficCarQuatY[9] = -0.0071
TrafficCarQuatZ[9] = 0.9039
TrafficCarQuatW[9] = -0.4277
TrafficCarRecording[9] = 675
TrafficCarStartime[9] = 19142.0000
TrafficCarModel[9] = blista

TrafficCarPos[10] = <<-1992.5828, -438.6674, 11.3191>>
TrafficCarQuatX[10] = -0.0042
TrafficCarQuatY[10] = -0.0042
TrafficCarQuatZ[10] = 0.4238
TrafficCarQuatW[10] = 0.9057
TrafficCarRecording[10] = 13
TrafficCarStartime[10] = 19907.0000
TrafficCarModel[10] = feltzer2

TrafficCarPos[11] = <<-1980.8217, -455.6247, 11.3957>>
TrafficCarQuatX[11] = -0.0042
TrafficCarQuatY[11] = -0.0022
TrafficCarQuatZ[11] = 0.4303
TrafficCarQuatW[11] = 0.9027
TrafficCarRecording[11] = 14
TrafficCarStartime[11] = 20435.0000
TrafficCarModel[11] = feltzer2

TrafficCarPos[12] = <<-1964.9023, -454.8819, 11.4294>>
TrafficCarQuatX[12] = -0.0042
TrafficCarQuatY[12] = -0.0023
TrafficCarQuatZ[12] = 0.4251
TrafficCarQuatW[12] = 0.9051
TrafficCarRecording[12] = 15
TrafficCarStartime[12] = 20897.0000
TrafficCarModel[12] = feltzer2

TrafficCarPos[13] = <<-1943.2075, -472.5447, 11.4491>>
TrafficCarQuatX[13] = -0.0021
TrafficCarQuatY[13] = -0.0028
TrafficCarQuatZ[13] = 0.4255
TrafficCarQuatW[13] = 0.9049
TrafficCarRecording[13] = 16
TrafficCarStartime[13] = 21755.0000
TrafficCarModel[13] = feltzer2

TrafficCarPos[14] = <<-1942.2732, -478.8694, 11.4584>>
TrafficCarQuatX[14] = 0.0013
TrafficCarQuatY[14] = 0.0001
TrafficCarQuatZ[14] = 0.4265
TrafficCarQuatW[14] = 0.9045
TrafficCarRecording[14] = 17
TrafficCarStartime[14] = 21887.0000
TrafficCarModel[14] = feltzer2

TrafficCarPos[15] = <<-1914.1107, -496.6292, 11.4417>>
TrafficCarQuatX[15] = -0.0021
TrafficCarQuatY[15] = -0.0015
TrafficCarQuatZ[15] = 0.4272
TrafficCarQuatW[15] = 0.9042
TrafficCarRecording[15] = 18
TrafficCarStartime[15] = 22943.0000
TrafficCarModel[15] = feltzer2

TrafficCarPos[16] = <<-1924.8973, -513.7120, 11.5863>>
TrafficCarQuatX[16] = -0.0007
TrafficCarQuatY[16] = 0.0028
TrafficCarQuatZ[16] = 0.9070
TrafficCarQuatW[16] = -0.4211
TrafficCarRecording[16] = 19
TrafficCarStartime[16] = 23009.0000
TrafficCarModel[16] = bison

TrafficCarPos[17] = <<-1916.4690, -507.8683, 11.2995>>
TrafficCarQuatX[17] = -0.0032
TrafficCarQuatY[17] = -0.0012
TrafficCarQuatZ[17] = 0.4258
TrafficCarQuatW[17] = 0.9048
TrafficCarRecording[17] = 20
TrafficCarStartime[17] = 23141.0000
TrafficCarModel[17] = manana

//TrafficCarPos[18] = <<-1895.0011, -544.0021, 11.3484>>
//TrafficCarQuatX[18] = -0.0003
//TrafficCarQuatY[18] = 0.0007
//TrafficCarQuatZ[18] = 0.9067
//TrafficCarQuatW[18] = -0.4218
//TrafficCarRecording[18] = 22
//TrafficCarStartime[18] = 24395.0000
//TrafficCarModel[18] = feltzer2

TrafficCarPos[19] = <<-1884.2034, -552.8060, 11.1889>>
TrafficCarQuatX[19] = -0.0017
TrafficCarQuatY[19] = 0.0034
TrafficCarQuatZ[19] = 0.9008
TrafficCarQuatW[19] = -0.4341
TrafficCarRecording[19] = 23
TrafficCarStartime[19] = 24857.0000
TrafficCarModel[19] = asterope

//TrafficCarPos[20] = <<-1875.8270, -554.2006, 11.2760>>
//TrafficCarQuatX[20] = -0.0017
//TrafficCarQuatY[20] = 0.0032
//TrafficCarQuatZ[20] = 0.8996
//TrafficCarQuatW[20] = -0.4366
//TrafficCarRecording[20] = 24
//TrafficCarStartime[20] = 25055.0000
//TrafficCarModel[20] = feltzer2

TrafficCarPos[21] = <<-1813.3405, -602.7710, 11.0781>>
TrafficCarQuatX[21] = 0.0062
TrafficCarQuatY[21] = -0.0129
TrafficCarQuatZ[21] = 0.8954
TrafficCarQuatW[21] = -0.4450
TrafficCarRecording[21] = 673
TrafficCarStartime[21] = 25314.0000
TrafficCarModel[21] = blista

TrafficCarPos[22] = <<-1874.7090, -555.2547, 10.9783>>
TrafficCarQuatX[22] = -0.0010
TrafficCarQuatY[22] = 0.0012
TrafficCarQuatZ[22] = 0.9012
TrafficCarQuatW[22] = -0.4333
TrafficCarRecording[22] = 676
TrafficCarStartime[22] = 25332.0000
TrafficCarModel[22] = Dominator

//TrafficCarPos[23] = <<-1852.1007, -579.8507, 11.1132>>
//TrafficCarQuatX[23] = -0.0015
//TrafficCarQuatY[23] = 0.0023
//TrafficCarQuatZ[23] = 0.9043
//TrafficCarQuatW[23] = -0.4269
//TrafficCarRecording[23] = 26
//TrafficCarStartime[23] = 26177.0000
//TrafficCarModel[23] = feltzer2

TrafficCarPos[24] = <<-1829.2272, -607.2402, 10.9377>>
TrafficCarQuatX[24] = 0.0045
TrafficCarQuatY[24] = -0.0096
TrafficCarQuatZ[24] = 0.9175
TrafficCarQuatW[24] = -0.3976
TrafficCarRecording[24] = 27
TrafficCarStartime[24] = 28223.0000
TrafficCarModel[24] = feltzer2

TrafficCarPos[25] = <<-1799.1798, -635.4329, 10.5009>>
TrafficCarQuatX[25] = 0.0015
TrafficCarQuatY[25] = -0.0033
TrafficCarQuatZ[25] = 0.9153
TrafficCarQuatW[25] = -0.4028
TrafficCarRecording[25] = 29
TrafficCarStartime[25] = 30005.0000
TrafficCarModel[25] = asterope

TrafficCarPos[26] = <<-1786.6638, -647.2526, 10.2952>>
TrafficCarQuatX[26] = 0.0081
TrafficCarQuatY[26] = -0.0060
TrafficCarQuatZ[26] = 0.9232
TrafficCarQuatW[26] = -0.3841
TrafficCarRecording[26] = 30
TrafficCarStartime[26] = 30005.0000
TrafficCarModel[26] = feltzer2

TrafficCarPos[27] = <<-1737.7423, -634.0445, 10.5597>>
TrafficCarQuatX[27] = 0.0004
TrafficCarQuatY[27] = -0.0202
TrafficCarQuatZ[27] = 0.5159
TrafficCarQuatW[27] = 0.8564
TrafficCarRecording[27] = 31
TrafficCarStartime[27] = 31853.0000
TrafficCarModel[27] = feltzer2

TrafficCarPos[28] = <<-1719.3136, -703.9955, 10.0800>>
TrafficCarQuatX[28] = -0.0417
TrafficCarQuatY[28] = -0.0105
TrafficCarQuatZ[28] = 0.9029
TrafficCarQuatW[28] = -0.4276
TrafficCarRecording[28] = 32
TrafficCarStartime[28] = 32645.0000
TrafficCarModel[28] = feltzer2

TrafficCarPos[29] = <<-1672.9750, -687.3683, 10.8393>>
TrafficCarQuatX[29] = 0.0001
TrafficCarQuatY[29] = -0.0017
TrafficCarQuatZ[29] = 0.4795
TrafficCarQuatW[29] = 0.8776
TrafficCarRecording[29] = 33
TrafficCarStartime[29] = 33503.0000
TrafficCarModel[29] = buffalo

TrafficCarPos[30] = <<-1674.2244, -693.2474, 10.8404>>
TrafficCarQuatX[30] = -0.0012
TrafficCarQuatY[30] = -0.0021
TrafficCarQuatZ[30] = 0.4821
TrafficCarQuatW[30] = 0.8761
TrafficCarRecording[30] = 34
TrafficCarStartime[30] = 33569.0000
TrafficCarModel[30] = feltzer2

TrafficCarPos[31] = <<-1649.4194, -768.9033, 9.4656>>
TrafficCarQuatX[31] = -0.0164
TrafficCarQuatY[31] = -0.0077
TrafficCarQuatZ[31] = 0.9084
TrafficCarQuatW[31] = -0.4178
TrafficCarRecording[31] = 35
TrafficCarStartime[31] = 35681.0000
TrafficCarModel[31] = feltzer2

TrafficCarPos[32] = <<-1596.6958, -727.3316, 10.9103>>
TrafficCarQuatX[32] = -0.0001
TrafficCarQuatY[32] = -0.0045
TrafficCarQuatZ[32] = 0.5534
TrafficCarQuatW[32] = 0.8329
TrafficCarRecording[32] = 36
TrafficCarStartime[32] = 36341.0000
TrafficCarModel[32] = feltzer2

//TrafficCarPos[33] = <<-1606.4501, -749.8375, 11.0267>>
//TrafficCarQuatX[33] = -0.0031
//TrafficCarQuatY[33] = 0.0002
//TrafficCarQuatZ[33] = 0.8433
//TrafficCarQuatW[33] = -0.5375
//TrafficCarRecording[33] = 37
//TrafficCarStartime[33] = 36473.0000
//TrafficCarModel[33] = jackal

TrafficCarPos[34] = <<-1587.6296, -730.3649, 10.9194>>
TrafficCarQuatX[34] = -0.0019
TrafficCarQuatY[34] = -0.0026
TrafficCarQuatZ[34] = 0.5655
TrafficCarQuatW[34] = 0.8248
TrafficCarRecording[34] = 39
TrafficCarStartime[34] = 36737.0000
TrafficCarModel[34] = feltzer2

TrafficCarPos[35] = <<-1574.8739, -746.1680, 10.9264>>
TrafficCarQuatX[35] = -0.0022
TrafficCarQuatY[35] = -0.0024
TrafficCarQuatZ[35] = 0.5830
TrafficCarQuatW[35] = 0.8124
TrafficCarRecording[35] = 40
TrafficCarStartime[35] = 37463.0000
TrafficCarModel[35] = feltzer2

TrafficCarPos[36] = <<-1308.0039, -730.2979, 10.0408>>
TrafficCarQuatX[36] = 0.0001
TrafficCarQuatY[36] = -0.0032
TrafficCarQuatZ[36] = 1.0000
TrafficCarQuatW[36] = -0.0015
TrafficCarRecording[36] = 981
TrafficCarStartime[36] = 37878.0000
TrafficCarModel[36] = coach

TrafficCarPos[37] = <<-1544.9573, -744.9665, 10.9180>>
TrafficCarQuatX[37] = 0.0043
TrafficCarQuatY[37] = -0.0097
TrafficCarQuatZ[37] = 0.6112
TrafficCarQuatW[37] = 0.7914
TrafficCarRecording[37] = 41
TrafficCarStartime[37] = 38321.0000
TrafficCarModel[37] = feltzer2

TrafficCarPos[38] = <<-1540.6364, -751.1502, 10.9216>>
TrafficCarQuatX[38] = 0.0035
TrafficCarQuatY[38] = -0.0014
TrafficCarQuatZ[38] = 0.6411
TrafficCarQuatW[38] = 0.7675
TrafficCarRecording[38] = 42
TrafficCarStartime[38] = 38519.0000
TrafficCarModel[38] = feltzer2

TrafficCarPos[39] = <<-1525.6652, -758.7118, 10.9032>>
TrafficCarQuatX[39] = 0.0007
TrafficCarQuatY[39] = -0.0051
TrafficCarQuatZ[39] = 0.6528
TrafficCarQuatW[39] = 0.7575
TrafficCarRecording[39] = 43
TrafficCarStartime[39] = 39047.0000
TrafficCarModel[39] = feltzer2

TrafficCarPos[40] = <<-1520.3099, -783.9903, 18.2027>>
TrafficCarQuatX[40] = 0.0099
TrafficCarQuatY[40] = -0.0160
TrafficCarQuatZ[40] = 0.3778
TrafficCarQuatW[40] = 0.9257
TrafficCarRecording[40] = 44
TrafficCarStartime[40] = 39641.0000
TrafficCarModel[40] = feltzer2

TrafficCarPos[41] = <<-1491.3031, -784.0678, 10.9133>>
TrafficCarQuatX[41] = 0.0122
TrafficCarQuatY[41] = 0.0252
TrafficCarQuatZ[41] = -0.7007
TrafficCarQuatW[41] = 0.7129
TrafficCarRecording[41] = 672
TrafficCarStartime[41] = 39796.0000
TrafficCarModel[41] = blista

TrafficCarPos[42] = <<-1508.5243, -776.6022, 10.8840>>
TrafficCarQuatX[42] = -0.0014
TrafficCarQuatY[42] = 0.0018
TrafficCarQuatZ[42] = 0.7176
TrafficCarQuatW[42] = -0.6964
TrafficCarRecording[42] = 45
TrafficCarStartime[42] = 39839.0000
TrafficCarModel[42] = feltzer2

TrafficCarPos[43] = <<-1508.0730, -786.4115, 18.3222>>
TrafficCarQuatX[43] = 0.0240
TrafficCarQuatY[43] = -0.0103
TrafficCarQuatZ[43] = 0.3936
TrafficCarQuatW[43] = 0.9189
TrafficCarRecording[43] = 46
TrafficCarStartime[43] = 40103.0000
TrafficCarModel[43] = jackal

TrafficCarPos[44] = <<-1511.6647, -810.4238, 13.8757>>
TrafficCarQuatX[44] = -0.0073
TrafficCarQuatY[44] = 0.0132
TrafficCarQuatZ[44] = 0.8309
TrafficCarQuatW[44] = -0.5562
TrafficCarRecording[44] = 47
TrafficCarStartime[44] = 40235.0000
TrafficCarModel[44] = feltzer2

TrafficCarPos[45] = <<-1485.4950, -750.4311, 10.9279>>
TrafficCarQuatX[45] = -0.0020
TrafficCarQuatY[45] = -0.0046
TrafficCarQuatZ[45] = 0.7196
TrafficCarQuatW[45] = 0.6943
TrafficCarRecording[45] = 48
TrafficCarStartime[45] = 40499.0000
TrafficCarModel[45] = jackal

TrafficCarPos[46] = <<-1408.1489, -748.8594, 10.8686>>
TrafficCarQuatX[46] = 0.0037
TrafficCarQuatY[46] = -0.0024
TrafficCarQuatZ[46] = 0.7565
TrafficCarQuatW[46] = 0.6540
TrafficCarRecording[46] = 49
TrafficCarStartime[46] = 42809.0000
TrafficCarModel[46] = bison

TrafficCarPos[47] = <<-1151.9882, -655.5941, 10.0751>>
TrafficCarQuatX[47] = 0.0002
TrafficCarQuatY[47] = -0.0061
TrafficCarQuatZ[47] = 1.0000
TrafficCarQuatW[47] = -0.0035
TrafficCarRecording[47] = 982
TrafficCarStartime[47] = 42878.0000
TrafficCarModel[47] = coach

TrafficCarPos[48] = <<-1376.3154, -789.7238, 18.9654>>
TrafficCarQuatX[48] = 0.0038
TrafficCarQuatY[48] = 0.0017
TrafficCarQuatZ[48] = 0.4158
TrafficCarQuatW[48] = 0.9094
TrafficCarRecording[48] = 50
TrafficCarStartime[48] = 44063.0000
TrafficCarModel[48] = feltzer2

TrafficCarPos[49] = <<-1372.0023, -800.3631, 18.9660>>
TrafficCarQuatX[49] = -0.0004
TrafficCarQuatY[49] = 0.0132
TrafficCarQuatZ[49] = 0.4230
TrafficCarQuatW[49] = 0.9060
TrafficCarRecording[49] = 51
TrafficCarStartime[49] = 44327.0000
TrafficCarModel[49] = jackal

TrafficCarPos[50] = <<-1346.7297, -752.2042, 10.8556>>
TrafficCarQuatX[50] = 0.0004
TrafficCarQuatY[50] = -0.0004
TrafficCarQuatZ[50] = -0.6204
TrafficCarQuatW[50] = 0.7843
TrafficCarRecording[50] = 52
TrafficCarStartime[50] = 44921.0000
TrafficCarModel[50] = bison

TrafficCarPos[51] = <<-1312.2633, -724.9147, 10.6913>>
TrafficCarQuatX[51] = 0.0008
TrafficCarQuatY[51] = -0.0037
TrafficCarQuatZ[51] = 0.8043
TrafficCarQuatW[51] = 0.5942
TrafficCarRecording[51] = 53
TrafficCarStartime[51] = 46175.0000
TrafficCarModel[51] = feltzer2

TrafficCarPos[52] = <<-1291.6584, -734.2235, 10.7767>>
TrafficCarQuatX[52] = -0.0180
TrafficCarQuatY[52] = -0.0080
TrafficCarQuatZ[52] = -0.5769
TrafficCarQuatW[52] = 0.8166
TrafficCarRecording[52] = 671
TrafficCarStartime[52] = 46808.0000
TrafficCarModel[52] = blista

TrafficCarPos[53] = <<-1287.1271, -746.1357, 10.6849>>
TrafficCarQuatX[53] = 0.0001
TrafficCarQuatY[53] = 0.0001
TrafficCarQuatZ[53] = -0.5769
TrafficCarQuatW[53] = 0.8168
TrafficCarRecording[53] = 54
TrafficCarStartime[53] = 46835.0000
TrafficCarModel[53] = feltzer2

TrafficCarPos[54] = <<-1204.9954, -712.9043, 10.7888>>
TrafficCarQuatX[54] = 0.0019
TrafficCarQuatY[54] = 0.0212
TrafficCarQuatZ[54] = -0.5360
TrafficCarQuatW[54] = 0.8440
TrafficCarRecording[54] = 670
TrafficCarStartime[54] = 47964.0000
TrafficCarModel[54] = blista

TrafficCarPos[55] = <<-1247.9370, -726.0201, 10.6805>>
TrafficCarQuatX[55] = 0.0043
TrafficCarQuatY[55] = -0.0025
TrafficCarQuatZ[55] = -0.5481
TrafficCarQuatW[55] = 0.8364
TrafficCarRecording[55] = 56
TrafficCarStartime[55] = 48155.0000
TrafficCarModel[55] = feltzer2

TrafficCarPos[56] = <<-1173.1163, -670.5189, 22.5925>>
TrafficCarQuatX[56] = 0.0057
TrafficCarQuatY[56] = -0.0161
TrafficCarQuatZ[56] = 0.9373
TrafficCarQuatW[56] = -0.3481
TrafficCarRecording[56] = 58
TrafficCarStartime[56] = 51323.0000
TrafficCarModel[56] = jackal

TrafficCarPos[57] = <<-1166.5515, -677.7913, 22.3152>>
TrafficCarQuatX[57] = 0.0015
TrafficCarQuatY[57] = -0.0062
TrafficCarQuatZ[57] = 0.9370
TrafficCarQuatW[57] = -0.3493
TrafficCarRecording[57] = 59
TrafficCarStartime[57] = 51455.0000
TrafficCarModel[57] = jackal

TrafficCarPos[58] = <<-1152.4274, -682.2510, 10.6786>>
TrafficCarQuatX[58] = 0.0058
TrafficCarQuatY[58] = -0.0013
TrafficCarQuatZ[58] = -0.4910
TrafficCarQuatW[58] = 0.8712
TrafficCarRecording[58] = 60
TrafficCarStartime[58] = 51521.0000
TrafficCarModel[58] = feltzer2

TrafficCarPos[59] = <<-1133.7329, -656.7540, 11.3981>>
TrafficCarQuatX[59] = 0.0161
TrafficCarQuatY[59] = -0.0099
TrafficCarQuatZ[59] = -0.4822
TrafficCarQuatW[59] = 0.8758
TrafficCarRecording[59] = 61
TrafficCarStartime[59] = 52445.0000
TrafficCarModel[59] = jackal

TrafficCarPos[60] = <<-1171.9824, -680.8687, 10.6805>>
TrafficCarQuatX[60] = 0.0043
TrafficCarQuatY[60] = -0.0025
TrafficCarQuatZ[60] = -0.4963
TrafficCarQuatW[60] = 0.8681
TrafficCarRecording[60] = 57
TrafficCarStartime[60] = 52993.0000
TrafficCarModel[60] = buffalo //feltzer2

TrafficCarPos[61] = <<-1095.1566, -645.9737, 13.8981>>
TrafficCarQuatX[61] = 0.0387
TrafficCarQuatY[61] = -0.0218
TrafficCarQuatZ[61] = -0.4903
TrafficCarQuatW[61] = 0.8704
TrafficCarRecording[61] = 62
TrafficCarStartime[61] = 53699.0000
TrafficCarModel[61] = buffalo

TrafficCarPos[62] = <<-1084.0941, -633.2120, 15.3240>>
TrafficCarQuatX[62] = 0.0322
TrafficCarQuatY[62] = -0.0200
TrafficCarQuatZ[62] = -0.5058
TrafficCarQuatW[62] = 0.8618
TrafficCarRecording[62] = 63
TrafficCarStartime[62] = 54161.0000
TrafficCarModel[62] = bison
//
//TrafficCarPos[63] = <<-1061.2101, -627.7006, 16.9946>>
//TrafficCarQuatX[63] = 0.0282
//TrafficCarQuatY[63] = -0.0165
//TrafficCarQuatZ[63] = -0.5147
//TrafficCarQuatW[63] = 0.8567
//TrafficCarRecording[63] = 64
//TrafficCarStartime[63] = 54887.0000
//TrafficCarModel[63] = jackal

//TrafficCarPos[64] = <<-1039.3313, -615.9235, 17.8695>>
//TrafficCarQuatX[64] = 0.0035
//TrafficCarQuatY[64] = -0.0021
//TrafficCarQuatZ[64] = -0.5179
//TrafficCarQuatW[64] = 0.8554
//TrafficCarRecording[64] = 65
//TrafficCarStartime[64] = 55679.0000
//TrafficCarModel[64] = feltzer2

TrafficCarPos[65] = <<-1042.5024, -586.2520, 18.0793>>
TrafficCarQuatX[65] = -0.0077
TrafficCarQuatY[65] = -0.0085
TrafficCarQuatZ[65] = 0.8537
TrafficCarQuatW[65] = 0.5206
TrafficCarRecording[65] = 66
TrafficCarStartime[65] = 56075.0000
TrafficCarModel[65] = jackal

TrafficCarPos[66] = <<-1043.7283, -581.4434, 17.9913>>
TrafficCarQuatX[66] = -0.0051
TrafficCarQuatY[66] = -0.0076
TrafficCarQuatZ[66] = 0.8554
TrafficCarQuatW[66] = 0.5178
TrafficCarRecording[66] = 67
TrafficCarStartime[66] = 56141.0000
TrafficCarModel[66] = feltzer2

TrafficCarPos[67] = <<-999.3893, -588.7261, 18.0031>>
TrafficCarQuatX[67] = 0.0023
TrafficCarQuatY[67] = -0.0014
TrafficCarQuatZ[67] = -0.5369
TrafficCarQuatW[67] = 0.8437
TrafficCarRecording[67] = 68
TrafficCarStartime[67] = 57461.0000
TrafficCarModel[67] = feltzer2

TrafficCarPos[68] = <<-958.6710, -576.3694, 18.0496>>
TrafficCarQuatX[68] = 0.0018
TrafficCarQuatY[68] = -0.0014
TrafficCarQuatZ[68] = -0.5487
TrafficCarQuatW[68] = 0.8360
TrafficCarRecording[68] = 69
TrafficCarStartime[68] =  59677.0000//59177.0000
TrafficCarModel[68] = buffalo

TrafficCarPos[69] = <<-960.8952, -546.3404, 18.2592>>
TrafficCarQuatX[69] = 0.0001
TrafficCarQuatY[69] = -0.0010
TrafficCarQuatZ[69] = 0.8323
TrafficCarQuatW[69] = 0.5544
TrafficCarRecording[69] = 70
TrafficCarStartime[69] = 59705.0000
TrafficCarModel[69] = bison

//TrafficCarPos[70] = <<-937.2994, -564.1520, 18.3436>>
//TrafficCarQuatX[70] = 0.0029
//TrafficCarQuatY[70] = -0.0169
//TrafficCarQuatZ[70] = -0.5972
//TrafficCarQuatW[70] = 0.8019
//TrafficCarRecording[70] = 71
//TrafficCarStartime[70] = 60695.0000
//TrafficCarModel[70] = jackal

TrafficCarPos[71] = <<-918.2991, -524.3688, 19.2461>>
TrafficCarQuatX[71] = -0.0163
TrafficCarQuatY[71] = -0.0184
TrafficCarQuatZ[71] = 0.8114
TrafficCarQuatW[71] = 0.5840
TrafficCarRecording[71] = 73
TrafficCarStartime[71] = 61487.0000
TrafficCarModel[71] = manana

TrafficCarPos[72] = <<-882.9097, -509.5293, 21.2244>>
TrafficCarQuatX[72] = -0.0172
TrafficCarQuatY[72] = -0.0187
TrafficCarQuatZ[72] = 0.7932
TrafficCarQuatW[72] = 0.6084
TrafficCarRecording[72] = 75
TrafficCarStartime[72] = 62807.0000
TrafficCarModel[72] = asterope

TrafficCarPos[73] = <<-867.7180, -505.6333, 22.0587>>
TrafficCarQuatX[73] = -0.0179
TrafficCarQuatY[73] = -0.0196
TrafficCarQuatZ[73] = 0.7789
TrafficCarQuatW[73] = 0.6266
TrafficCarRecording[73] = 76
TrafficCarStartime[73] = 63203.0000
TrafficCarModel[73] = manana

TrafficCarPos[74] = <<-852.1915, -518.4356, 22.6722>>
TrafficCarQuatX[74] = -0.0209
TrafficCarQuatY[74] = -0.0218
TrafficCarQuatZ[74] = 0.7745
TrafficCarQuatW[74] = 0.6318
TrafficCarRecording[74] = 77
TrafficCarStartime[74] = 63467.0000
TrafficCarModel[74] = dilettante

TrafficCarPos[75] = <<-826.5883, -538.0093, 23.7758>>
TrafficCarQuatX[75] = 0.0184
TrafficCarQuatY[75] = -0.0164
TrafficCarQuatZ[75] = -0.6495
TrafficCarQuatW[75] = 0.7600
TrafficCarRecording[75] = 78
TrafficCarStartime[75] = 64457.0000
TrafficCarModel[75] = bison

TrafficCarPos[76] = <<-813.6053, -535.4616, 24.0711>>
TrafficCarQuatX[76] = 0.0176
TrafficCarQuatY[76] = -0.0153
TrafficCarQuatZ[76] = -0.6574
TrafficCarQuatW[76] = 0.7532
TrafficCarRecording[76] = 79
TrafficCarStartime[76] = 64589.0000
TrafficCarModel[76] = asterope

TrafficCarPos[77] = <<-778.2493, -531.1539, 24.6557>>
TrafficCarQuatX[77] = 0.0028
TrafficCarQuatY[77] = -0.0025
TrafficCarQuatZ[77] = -0.6848
TrafficCarQuatW[77] = 0.7287
TrafficCarRecording[77] = 82
TrafficCarStartime[77] = 65975.0000
TrafficCarModel[77] = dilettante

TrafficCarPos[78] = <<-737.4198, -535.4118, 24.6755>>
TrafficCarQuatX[78] = 0.0056
TrafficCarQuatY[78] = -0.0056
TrafficCarQuatZ[78] = -0.7065
TrafficCarQuatW[78] = 0.7077
TrafficCarRecording[78] = 83
TrafficCarStartime[78] = 67361.0000
TrafficCarModel[78] = asterope

TrafficCarPos[79] = <<-742.9792, -536.2603, 24.6668>>
TrafficCarQuatX[79] = 0.0034
TrafficCarQuatY[79] = -0.0037
TrafficCarQuatZ[79] = -0.7047
TrafficCarQuatW[79] = 0.7095
TrafficCarRecording[79] = 84
TrafficCarStartime[79] = 67757.0000
TrafficCarModel[79] = asterope

TrafficCarPos[80] = <<-724.1472, -529.7708, 24.6895>>
TrafficCarQuatX[80] = -0.0001
TrafficCarQuatY[80] = 0.0001
TrafficCarQuatZ[80] = 0.7077
TrafficCarQuatW[80] = -0.7065
TrafficCarRecording[80] = 85
TrafficCarStartime[80] = 67757.0000
TrafficCarModel[80] = manana

TrafficCarPos[81] = <<-701.8006, -520.3475, 24.7083>>
TrafficCarQuatX[81] = -0.0051
TrafficCarQuatY[81] = 0.0051
TrafficCarQuatZ[81] = 0.7075
TrafficCarQuatW[81] = -0.7067
TrafficCarRecording[81] = 86
TrafficCarStartime[81] = 68219.0000
TrafficCarModel[81] = asterope

TrafficCarPos[82] = <<-692.2917, -536.0488, 24.7530>>
TrafficCarQuatX[82] = 0.0275
TrafficCarQuatY[82] = 0.0244
TrafficCarQuatZ[82] = -0.6604
TrafficCarQuatW[82] = 0.7500
TrafficCarRecording[82] = 87
TrafficCarStartime[82] = 68615.0000
TrafficCarModel[82] = dilettante

TrafficCarPos[83] = <<-653.8910, -560.2650, 34.5999>>
TrafficCarQuatX[83] = 0.0017
TrafficCarQuatY[83] = 0.0128
TrafficCarQuatZ[83] = 0.7310
TrafficCarQuatW[83] = -0.6822
TrafficCarRecording[83] = 88
TrafficCarStartime[83] = 69671.0000
TrafficCarModel[83] = bison

TrafficCarPos[84] = <<-646.4771, -559.4749, 34.5956>>
TrafficCarQuatX[84] = -0.0015
TrafficCarQuatY[84] = 0.0221
TrafficCarQuatZ[84] = 0.9992
TrafficCarQuatW[84] = -0.0342
TrafficCarRecording[84] = 89
TrafficCarStartime[84] = 69935.0000
TrafficCarModel[84] = bison

TrafficCarPos[85] = <<-640.9517, -547.1611, 34.3248>>
TrafficCarQuatX[85] = 0.0001
TrafficCarQuatY[85] = 0.0079
TrafficCarQuatZ[85] = 0.9996
TrafficCarQuatW[85] = 0.0276
TrafficCarRecording[85] = 90
TrafficCarStartime[85] = 70133.0000
TrafficCarModel[85] = dilettante

IF g_family1GarbageTruckSeen = TRUE
	TrafficCarPos[86] = <<-640.7111, -535.6561, 34.2955>>
	TrafficCarQuatX[86] = 0.0016
	TrafficCarQuatY[86] = 0.0020
	TrafficCarQuatZ[86] = 0.9999
	TrafficCarQuatW[86] = 0.0167
	TrafficCarRecording[86] = 91
	TrafficCarStartime[86] = 72793.0000
	TrafficCarModel[86] = asterope
ENDIF

TrafficCarPos[87] = <<-543.9510, -578.3604, 25.0337>>
TrafficCarQuatX[87] = 0.0019
TrafficCarQuatY[87] = 0.0000
TrafficCarQuatZ[87] = 0.7044
TrafficCarQuatW[87] = 0.7098
TrafficCarRecording[87] = 95
TrafficCarStartime[87] = 73499.0000
TrafficCarModel[87] = bison

TrafficCarPos[88] = <<-536.1589, -520.3383, 24.8108>>
TrafficCarQuatX[88] = -0.0011
TrafficCarQuatY[88] = 0.0011
TrafficCarQuatZ[88] = 0.7072
TrafficCarQuatW[88] = -0.7070
TrafficCarRecording[88] = 96
TrafficCarStartime[88] = 74027.0000
TrafficCarModel[88] = manana

TrafficCarPos[89] = <<-523.3940, -529.7448, 24.8229>>
TrafficCarQuatX[89] = -0.0049
TrafficCarQuatY[89] = 0.0049
TrafficCarQuatZ[89] = 0.7079
TrafficCarQuatW[89] = -0.7063
TrafficCarRecording[89] = 97
TrafficCarStartime[89] = 74357.0000
TrafficCarModel[89] = asterope

TrafficCarPos[90] = <<-492.4363, -535.2606, 25.0720>>
TrafficCarQuatX[90] = -0.0027
TrafficCarQuatY[90] = 0.0037
TrafficCarQuatZ[90] = 0.7087
TrafficCarQuatW[90] = -0.7055
TrafficCarRecording[90] = 98
TrafficCarStartime[90] = 75545.0000
TrafficCarModel[90] = bison

TrafficCarPos[91] = <<-477.6514, -525.7468, 24.8202>>
TrafficCarQuatX[91] = -0.0048
TrafficCarQuatY[91] = 0.0048
TrafficCarQuatZ[91] = 0.7074
TrafficCarQuatW[91] = -0.7068
TrafficCarRecording[91] = 99
TrafficCarStartime[91] = 76007.0000
TrafficCarModel[91] = asterope

TrafficCarPos[92] = <<-464.8362, -536.2483, 24.8450>>
TrafficCarQuatX[92] = 0.0076
TrafficCarQuatY[92] = -0.0077
TrafficCarQuatZ[92] = -0.7069
TrafficCarQuatW[92] = 0.7072
TrafficCarRecording[92] = 100
TrafficCarStartime[92] = 76535.0000
TrafficCarModel[92] = dilettante

TrafficCarPos[93] = <<-422.1921, -752.8593, 37.3530>>
TrafficCarQuatX[93] = 0.0002
TrafficCarQuatY[93] = -0.0061
TrafficCarQuatZ[93] = 1.0000
TrafficCarQuatW[93] = -0.0035
TrafficCarRecording[93] = 988
TrafficCarStartime[93] = 83279.0000
TrafficCarModel[93] = PHANTOM //Replaced for memory reasons PACKER

//TrafficCarPos[94] = <<-427.5499, -756.4049, 37.3601>>
//TrafficCarQuatX[94] = 0.0001
//TrafficCarQuatY[94] = -0.0032
//TrafficCarQuatZ[94] = 1.0000
//TrafficCarQuatW[94] = -0.0015
//TrafficCarRecording[94] = 987
//TrafficCarStartime[94] = 83345.0000
//TrafficCarModel[94] = PHANTOM //Replaced for memory reasons PACKER

//TrafficCarPos[95] = <<-382.6625, -836.4967, 37.5544>>
//TrafficCarQuatX[95] = -0.0016
//TrafficCarQuatY[95] = 0.0004
//TrafficCarQuatZ[95] = 0.0211
//TrafficCarQuatW[95] = 0.9998
//TrafficCarRecording[95] = 101
//TrafficCarStartime[95] = 87095.0000
//TrafficCarModel[95] = PHANTOM //Replaced for memory reasons PACKER


//TrafficCarPos[95] = <<-399.9667, -900.1065, 37.2774>>
//TrafficCarQuatX[95] = 0.0026
//TrafficCarQuatY[95] = -0.0176
//TrafficCarQuatZ[95] = -0.0058
//TrafficCarQuatW[95] = 0.9998
//TrafficCarRecording[95] = 101
//TrafficCarStartime[95] = 82942.0000
//TrafficCarModel[95] = Phantom


TrafficCarPos[96] = <<-390.7148, -978.7253, 37.1997>>
TrafficCarQuatX[96] = -0.0054
TrafficCarQuatY[96] = 0.0013
TrafficCarQuatZ[96] = 0.1264
TrafficCarQuatW[96] = 0.9920
TrafficCarRecording[96] = 102
TrafficCarStartime[96] = 90527.0000
TrafficCarModel[96] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[97] = <<-385.0823, -1038.9622, 37.2327>>
TrafficCarQuatX[97] = 0.0044
TrafficCarQuatY[97] = -0.0185
TrafficCarQuatZ[97] = -0.0039
TrafficCarQuatW[97] = 0.9998
TrafficCarRecording[97] = 103
TrafficCarStartime[97] = 92375.0000
TrafficCarModel[97] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[98] = <<-380.3994, -1039.6571, 36.9645>>
TrafficCarQuatX[98] = 0.0011
TrafficCarQuatY[98] = -0.0033
TrafficCarQuatZ[98] = -0.0035
TrafficCarQuatW[98] = 1.0000
TrafficCarRecording[98] = 104
TrafficCarStartime[98] = 92441.0000
TrafficCarModel[98] = bison

TrafficCarPos[99] = <<-427.9002, -1063.8818, 39.1192>>
TrafficCarQuatX[99] = 0.0032
TrafficCarQuatY[99] = 0.0409
TrafficCarQuatZ[99] = 0.9964
TrafficCarQuatW[99] = 0.0739
TrafficCarRecording[99] = 105
TrafficCarStartime[99] = 93167.0000
TrafficCarModel[99] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[100] = <<-432.0537, -1093.8250, 41.9649>>
TrafficCarQuatX[100] = -0.0140
TrafficCarQuatY[100] = 0.0647
TrafficCarQuatZ[100] = 0.9962
TrafficCarQuatW[100] = 0.0567
TrafficCarRecording[100] = 106
TrafficCarStartime[100] = 94157.0000
TrafficCarModel[100] = feltzer2

TrafficCarPos[101] = <<-437.6666, -1099.8080, 43.0629>>
TrafficCarQuatX[101] = -0.0261
TrafficCarQuatY[101] = 0.0659
TrafficCarQuatZ[101] = 0.9961
TrafficCarQuatW[101] = 0.0525
TrafficCarRecording[101] = 107
TrafficCarStartime[101] = 94355.0000
TrafficCarModel[101] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[102] = <<-426.7242, -1107.8737, 28.5356>>
TrafficCarQuatX[102] = -0.0133
TrafficCarQuatY[102] = 0.0108
TrafficCarQuatZ[102] = 0.6069
TrafficCarQuatW[102] = 0.7946
TrafficCarRecording[102] = 108
TrafficCarStartime[102] = 94751.0000
TrafficCarModel[102] = manana

TrafficCarPos[103] = <<-436.5396, -1109.8170, 29.2992>>
TrafficCarQuatX[103] = -0.0130
TrafficCarQuatY[103] = 0.0098
TrafficCarQuatZ[103] = 0.6011
TrafficCarQuatW[103] = 0.7990
TrafficCarRecording[103] = 109
TrafficCarStartime[103] = 94817.0000
TrafficCarModel[103] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[104] = <<-434.3285, -1114.3945, 45.2462>>
TrafficCarQuatX[104] = -0.0134
TrafficCarQuatY[104] = 0.0654
TrafficCarQuatZ[104] = 0.9959
TrafficCarQuatW[104] = 0.0612
TrafficCarRecording[104] = 110
TrafficCarStartime[104] = 94883.0000
TrafficCarModel[104] = PHANTOM //Replaced for memory reasons PACKER

//TrafficCarPos[105] = <<-386.5169, -1118.0197, 29.2722>>
//TrafficCarQuatX[105] = -0.0185
//TrafficCarQuatY[105] = 0.0080
//TrafficCarQuatZ[105] = 0.6061
//TrafficCarQuatW[105] = 0.7952
//TrafficCarRecording[105] = 111
//TrafficCarStartime[105] = 94949.0000
//TrafficCarModel[105] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[106] = <<-411.273,-1251.470,37.280>>
TrafficCarQuatX[106] = 0.0194
TrafficCarQuatY[106] = 0.0027
TrafficCarQuatZ[106] = 0.9998
TrafficCarQuatW[106] = 0.0012
TrafficCarRecording[106] = 984
TrafficCarStartime[106] = 98987.0000
TrafficCarModel[106] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[107] = <<-409.7785, -1299.3727, 37.2757>>
TrafficCarQuatX[107] = 0.0196
TrafficCarQuatY[107] = -0.0029
TrafficCarQuatZ[107] = 0.9998
TrafficCarQuatW[107] = 0.0018
TrafficCarRecording[107] = 983
TrafficCarStartime[107] = 100637.0000
TrafficCarModel[107] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[109] = <<-427.1969, -1374.2290, 37.4014>>
TrafficCarQuatX[109] = -0.0176
TrafficCarQuatY[109] = -0.0086
TrafficCarQuatZ[109] = 0.9993
TrafficCarQuatW[109] = -0.0330
TrafficCarRecording[109] = 114
TrafficCarStartime[109] = 103001.0000
TrafficCarModel[109] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[110] = <<-399.1656, -1376.4208, 37.5457>>
TrafficCarQuatX[110] = -0.0167
TrafficCarQuatY[110] = -0.0208
TrafficCarQuatZ[110] = 0.0005
TrafficCarQuatW[110] = 0.9996
TrafficCarRecording[110] = 115
TrafficCarStartime[110] = 103133.0000
TrafficCarModel[110] = PHANTOM //Replaced for memory reasons PACKER

TrafficCarPos[111] = <<-413.6619, -1500.5460, 37.2355>>
TrafficCarQuatX[111] = 0.0205
TrafficCarQuatY[111] = -0.0077
TrafficCarQuatZ[111] = 0.9969
TrafficCarQuatW[111] = 0.0754
TrafficCarRecording[111] = 687
TrafficCarStartime[111] = 105268.0000
TrafficCarModel[111] = blista

TrafficCarPos[112] = <<-438.6665, -1573.3806, 39.1088>>
TrafficCarQuatX[112] = 0.0175
TrafficCarQuatY[112] = -0.0066
TrafficCarQuatZ[112] = 0.9744
TrafficCarQuatW[112] = 0.2238
TrafficCarRecording[112] = 685
TrafficCarStartime[112] = 106595.0000
TrafficCarModel[112] = boxville

TrafficCarPos[113] = <<-653.7279, -1719.7207, 37.0866>>
TrafficCarQuatX[113] = 0.0054
TrafficCarQuatY[113] = -0.0223
TrafficCarQuatZ[113] = 0.7512
TrafficCarQuatW[113] = 0.6597
TrafficCarRecording[113] = 686
TrafficCarStartime[113] = 115082.0000
TrafficCarModel[113] = regina

TrafficCarPos[114] = <<-714.7027, -1725.2677, 38.5975>>
TrafficCarQuatX[114] = 0.0216
TrafficCarQuatY[114] = 0.0056
TrafficCarQuatZ[114] = 0.7227
TrafficCarQuatW[114] = 0.6908
TrafficCarRecording[114] = 684
TrafficCarStartime[114] = 116075.0000
TrafficCarModel[114] = boxville2

TrafficCarPos[115] = <<-911.5505, -1823.8572, 35.3282>>
TrafficCarQuatX[115] = 0.0261
TrafficCarQuatY[115] = -0.0438
TrafficCarQuatZ[115] = 0.9755
TrafficCarQuatW[115] = 0.2140
TrafficCarRecording[115] = 688
TrafficCarStartime[115] = 124268.0000
TrafficCarModel[115] = blista

TrafficCarPos[116] = <<-469.3387, -2281.9646, 62.7470>>
TrafficCarQuatX[116] = -0.0081
TrafficCarQuatY[116] = -0.0056
TrafficCarQuatZ[116] = 0.4754
TrafficCarQuatW[116] = 0.8797
TrafficCarRecording[116] = 682
TrafficCarStartime[116] = 127881.0000
TrafficCarModel[116] = boxville

TrafficCarPos[119] = <<-752.3480, -2092.7856, 35.6287>>
TrafficCarQuatX[119] = -0.0230
TrafficCarQuatY[119] = 0.0481
TrafficCarQuatZ[119] = 0.8761
TrafficCarQuatW[119] = -0.4791
TrafficCarRecording[119] = 116
TrafficCarStartime[119] = 139470.0000
TrafficCarModel[119] = asterope

TrafficCarPos[120] = <<-608.5975, -2183.4438, 53.3943>>
TrafficCarQuatX[120] = -0.0367
TrafficCarQuatY[120] = -0.0194
TrafficCarQuatZ[120] = 0.4665
TrafficCarQuatW[120] = 0.8836
TrafficCarRecording[120] = 118
TrafficCarStartime[120] = 143882.0000
TrafficCarModel[120] = manana

TrafficCarPos[122] = <<-547.9759, -2226.3779, 58.5132>>
TrafficCarQuatX[122] = -0.0309
TrafficCarQuatY[122] = -0.0168
TrafficCarQuatZ[122] = 0.4614
TrafficCarQuatW[122] = 0.8865
TrafficCarRecording[122] = 120
TrafficCarStartime[122] = 146258.0000
TrafficCarModel[122] = prairie

TrafficCarPos[123] = <<-516.3538, -2247.3684, 60.1171>>
TrafficCarQuatX[123] = -0.0195
TrafficCarQuatY[123] = -0.0097
TrafficCarQuatZ[123] = 0.4611
TrafficCarQuatW[123] = 0.8871
TrafficCarRecording[123] = 121
TrafficCarStartime[123] = 147380.0000
TrafficCarModel[123] = penumbra

TrafficCarPos[124] = <<-501.1565, -2252.9497, 60.8029>>
TrafficCarQuatX[124] = -0.0186
TrafficCarQuatY[124] = -0.0094
TrafficCarQuatZ[124] = 0.4610
TrafficCarQuatW[124] = 0.8871
TrafficCarRecording[124] = 122
TrafficCarStartime[124] = 147842.0000
TrafficCarModel[124] = manana

TrafficCarPos[125] = <<-478.5707, -2275.4658, 61.8251>>
TrafficCarQuatX[125] = -0.0164
TrafficCarQuatY[125] = -0.0090
TrafficCarQuatZ[125] = 0.4631
TrafficCarQuatW[125] = 0.8861
TrafficCarRecording[125] = 123
TrafficCarStartime[125] = 148898.0000
TrafficCarModel[125] = manana

TrafficCarPos[126] = <<-470.8296, -2272.8818, 62.1378>>
TrafficCarQuatX[126] = -0.0162
TrafficCarQuatY[126] = -0.0074
TrafficCarQuatZ[126] = 0.4650
TrafficCarQuatW[126] = 0.8851
TrafficCarRecording[126] = 125
TrafficCarStartime[126] = 149096.0000
TrafficCarModel[126] = prairie

TrafficCarPos[127] = <<-438.2917, -2303.7395, 63.2256>>
TrafficCarQuatX[127] = -0.0142
TrafficCarQuatY[127] = -0.0058
TrafficCarQuatZ[127] = 0.4674
TrafficCarQuatW[127] = 0.8839
TrafficCarRecording[127] = 126
TrafficCarStartime[127] = 150416.0000
TrafficCarModel[127] = baller

TrafficCarPos[128] = <<-166.1050, -2493.9893, 49.2098>>
TrafficCarQuatX[128] = 0.0383
TrafficCarQuatY[128] = 0.0205
TrafficCarQuatZ[128] = 0.4597
TrafficCarQuatW[128] = 0.8870
TrafficCarRecording[128] = 127
TrafficCarStartime[128] = 160970.0000
TrafficCarModel[128] = sentinel

TrafficCarPos[129] = <<-115.5276, -2528.1096, 42.8922>>
TrafficCarQuatX[129] = 0.0459
TrafficCarQuatY[129] = 0.0258
TrafficCarQuatZ[129] = 0.4626
TrafficCarQuatW[129] = 0.8850
TrafficCarRecording[129] = 128
TrafficCarStartime[129] = 162884.0000
TrafficCarModel[129] = sentinel

TrafficCarPos[130] = <<-95.0777, -2536.1013, 40.4089>>
TrafficCarQuatX[130] = 0.0495
TrafficCarQuatY[130] = 0.0272
TrafficCarQuatZ[130] = 0.4638
TrafficCarQuatW[130] = 0.8841
TrafficCarRecording[130] = 129
TrafficCarStartime[130] = 163544.0000
TrafficCarModel[130] = sentinel

TrafficCarPos[133] = <<53.9231, -2636.0845, 23.0517>>
TrafficCarQuatX[133] = 0.0079
TrafficCarQuatY[133] = -0.0276
TrafficCarQuatZ[133] = 0.8152
TrafficCarQuatW[133] = -0.5785
TrafficCarRecording[133] = 133
TrafficCarStartime[133] = 169220.0000
TrafficCarModel[133] = asterope

TrafficCarPos[134] = <<514.6580, -2672.6165, 34.7354>>
TrafficCarQuatX[134] = -0.0411
TrafficCarQuatY[134] = -0.0500
TrafficCarQuatZ[134] = 0.7582
TrafficCarQuatW[134] = 0.6488
TrafficCarRecording[134] = 691
TrafficCarStartime[134] = 173837.0000
TrafficCarModel[134] = pbus

TrafficCarPos[135] = <<341.4317, -2660.4058, 20.4027>>
TrafficCarQuatX[135] = -0.0031
TrafficCarQuatY[135] = -0.0368
TrafficCarQuatZ[135] = 0.6381
TrafficCarQuatW[135] = 0.7690
TrafficCarRecording[135] = 690
TrafficCarStartime[135] = 173837.0000
TrafficCarModel[135] = boxville2

TrafficCarPos[136] = <<237.1271, -2666.9763, 17.6863>>
TrafficCarQuatX[136] = 0.0006
TrafficCarQuatY[136] = 0.0010
TrafficCarQuatZ[136] = -0.6810
TrafficCarQuatW[136] = 0.7323
TrafficCarRecording[136] = 136
TrafficCarStartime[136] = 175094.0000
TrafficCarModel[136] = sentinel

TrafficCarPos[137] = <<298.2273, -2659.6318, 18.0835>>
TrafficCarQuatX[137] = 0.0058
TrafficCarQuatY[137] = -0.0153
TrafficCarQuatZ[137] = 0.6820
TrafficCarQuatW[137] = 0.7312
TrafficCarRecording[137] = 138
TrafficCarStartime[137] = 177074.0000
TrafficCarModel[137] = sentinel

TrafficCarPos[138] = <<576.3924, -2655.9517, 41.3115>>
TrafficCarQuatX[138] = -0.0321
TrafficCarQuatY[138] = -0.0386
TrafficCarQuatZ[138] = 0.7623
TrafficCarQuatW[138] = 0.6453
TrafficCarRecording[138] = 692
TrafficCarStartime[138] = 179000.0000
TrafficCarModel[138] = taxi

TrafficCarPos[139] = <<590.3512, -2658.9763, 42.6148>>
TrafficCarQuatX[139] = -0.0267
TrafficCarQuatY[139] = -0.0327
TrafficCarQuatZ[139] = 0.7583
TrafficCarQuatW[139] = 0.6505
TrafficCarRecording[139] = 697
TrafficCarStartime[139] = 182440.0000
TrafficCarModel[139] = tiptruck

TrafficCarPos[140] = <<595.0726, -2666.9644, 42.9179>>
TrafficCarQuatX[140] = 0.0373
TrafficCarQuatY[140] = -0.0303
TrafficCarQuatZ[140] = -0.6535
TrafficCarQuatW[140] = 0.7554
TrafficCarRecording[140] = 696
TrafficCarStartime[140] = 182440.0000
TrafficCarModel[140] = tiptruck

TrafficCarPos[141] = <<660.3993, -2655.1409, 48.2919>>
TrafficCarQuatX[141] = 0.0226
TrafficCarQuatY[141] = -0.0190
TrafficCarQuatZ[141] = -0.6549
TrafficCarQuatW[141] = 0.7552
TrafficCarRecording[141] = 689
TrafficCarStartime[141] = 184837.0000
TrafficCarModel[141] = boxville2

TrafficCarPos[142] = <<246.1709, -2723.9399, 17.3719>>
TrafficCarQuatX[142] = 0.0142
TrafficCarQuatY[142] = -0.0029
TrafficCarQuatZ[142] = -0.2114
TrafficCarQuatW[142] = 0.9773
TrafficCarRecording[142] = 139
TrafficCarStartime[142] = 186446.0000
TrafficCarModel[142] = mixer



// ****  UBER RECORDED PARKED CARS  **** 

ParkedCarPos[0] = <<-2173.3704, -371.1576, 12.6234>>
ParkedCarQuatX[0] = -0.0027
ParkedCarQuatY[0] = 0.0008
ParkedCarQuatZ[0] = -0.0813
ParkedCarQuatW[0] = 0.9967
ParkedCarModel[0] = asterope

ParkedCarPos[1] = <<-2164.9753, -383.6164, 13.0582>>
ParkedCarQuatX[1] = 0.0057
ParkedCarQuatY[1] = 0.0055
ParkedCarQuatZ[1] = 0.7699
ParkedCarQuatW[1] = -0.6381
ParkedCarModel[1] = burrito

ParkedCarPos[2] = <<-2166.2371, -392.2645, 13.1134>>
ParkedCarQuatX[2] = 0.0039
ParkedCarQuatY[2] = 0.0075
ParkedCarQuatZ[2] = 0.7664
ParkedCarQuatW[2] = -0.6423
ParkedCarModel[2] = bison

ParkedCarPos[3] = <<-2166.4758, -395.1155, 12.8803>>
ParkedCarQuatX[3] = 0.0009
ParkedCarQuatY[3] = 0.0039
ParkedCarQuatZ[3] = 0.6205
ParkedCarQuatW[3] = 0.7842
ParkedCarModel[3] = asterope

ParkedCarPos[4] = <<-2158.5652, -386.8991, 12.8140>>
ParkedCarQuatX[4] = -0.0046
ParkedCarQuatY[4] = 0.0049
ParkedCarQuatZ[4] = 0.6179
ParkedCarQuatW[4] = 0.7863
ParkedCarModel[4] = dilettante

ParkedCarPos[5] = <<-2158.9890, -390.0757, 13.1374>>
ParkedCarQuatX[5] = 0.0055
ParkedCarQuatY[5] = 0.0044
ParkedCarQuatZ[5] = 0.7650
ParkedCarQuatW[5] = -0.6440
ParkedCarModel[5] = burrito

ParkedCarPos[6] = <<-1940.7916, -528.7653, 11.2814>>
ParkedCarQuatX[6] = -0.0012
ParkedCarQuatY[6] = -0.0030
ParkedCarQuatZ[6] = 0.9432
ParkedCarQuatW[6] = 0.3321
ParkedCarModel[6] = penumbra

ParkedCarPos[7] = <<-1938.2628, -530.9329, 11.3676>>
ParkedCarQuatX[7] = 0.0010
ParkedCarQuatY[7] = 0.0008
ParkedCarQuatZ[7] = -0.3625
ParkedCarQuatW[7] = 0.9320
ParkedCarModel[7] = dilettante

ParkedCarPos[8] = <<-1879.6661, -579.1818, 11.3935>>
ParkedCarQuatX[8] = -0.0096
ParkedCarQuatY[8] = 0.0033
ParkedCarQuatZ[8] = -0.3631
ParkedCarQuatW[8] = 0.9317
ParkedCarModel[8] = feltzer2

ParkedCarPos[9] = <<-1870.9495, -585.9692, 11.2385>>
ParkedCarQuatX[9] = 0.0043
ParkedCarQuatY[9] = 0.0122
ParkedCarQuatZ[9] = 0.9382
ParkedCarQuatW[9] = 0.3458
ParkedCarModel[9] = penumbra

ParkedCarPos[10] = <<-1845.7974, -612.0888, 10.8610>>
ParkedCarQuatX[10] = -0.0112
ParkedCarQuatY[10] = -0.0034
ParkedCarQuatZ[10] = 0.9177
ParkedCarQuatW[10] = -0.3970
ParkedCarModel[10] = feltzer2

ParkedCarPos[11] = <<-1849.1945, -616.7211, 10.7627>>
ParkedCarQuatX[11] = 0.0032
ParkedCarQuatY[11] = 0.0000
ParkedCarQuatZ[11] = 0.4186
ParkedCarQuatW[11] = 0.9081
ParkedCarModel[11] = feltzer2

ParkedCarPos[12] = <<-1853.2931, -622.0193, 10.7611>>
ParkedCarQuatX[12] = 0.0015
ParkedCarQuatY[12] = 0.0042
ParkedCarQuatZ[12] = 0.9215
ParkedCarQuatW[12] = -0.3882
ParkedCarModel[12] = feltzer2

ParkedCarPos[13] = <<-1805.9993, -642.0218, 10.5112>>
ParkedCarQuatX[13] = 0.0159
ParkedCarQuatY[13] = -0.0047
ParkedCarQuatZ[13] = -0.3410
ParkedCarQuatW[13] = 0.9399
ParkedCarModel[13] = feltzer2

ParkedCarPos[14] = <<-1751.6616, -692.5601, 9.6470>>
ParkedCarQuatX[14] = -0.0082
ParkedCarQuatY[14] = 0.0015
ParkedCarQuatZ[14] = -0.3666
ParkedCarQuatW[14] = 0.9304
ParkedCarModel[14] = dilettante

ParkedCarPos[15] = <<-1732.7915, -711.7750, 9.5733>>
ParkedCarQuatX[15] = -0.0003
ParkedCarQuatY[15] = -0.0045
ParkedCarQuatZ[15] = 0.8933
ParkedCarQuatW[15] = -0.4495
ParkedCarModel[15] = penumbra

ParkedCarPos[16] = <<-1736.6632, -716.8813, 9.8400>>
ParkedCarQuatX[16] = 0.0268
ParkedCarQuatY[16] = 0.0097
ParkedCarQuatZ[16] = 0.8899
ParkedCarQuatW[16] = -0.4553
ParkedCarModel[16] = dilettante

ParkedCarPos[17] = <<-1739.3000, -719.2399, 9.9768>>
ParkedCarQuatX[17] = 0.0011
ParkedCarQuatY[17] = 0.0133
ParkedCarQuatZ[17] = 0.4318
ParkedCarQuatW[17] = 0.9019
ParkedCarModel[17] = asterope

ParkedCarPos[18] = <<-1330.1243, -790.8218, 19.4837>>
ParkedCarQuatX[18] = 0.0123
ParkedCarQuatY[18] = -0.0133
ParkedCarQuatZ[18] = 0.8733
ParkedCarQuatW[18] = 0.4869
ParkedCarModel[18] = jackal

ParkedCarPos[19] = <<-1321.3926, -785.3258, 19.3172>>
ParkedCarQuatX[19] = 0.0185
ParkedCarQuatY[19] = -0.0058
ParkedCarQuatZ[19] = 0.8856
ParkedCarQuatW[19] = 0.4640
ParkedCarModel[19] = feltzer2

ParkedCarPos[20] = <<-444.4617, -531.1383, 24.8485>>
ParkedCarQuatX[20] = -0.0040
ParkedCarQuatY[20] = 0.0040
ParkedCarQuatZ[20] = -0.7071
ParkedCarQuatW[20] = 0.7071
ParkedCarModel[20] = dilettante

ParkedCarPos[21] = <<-404.1469, -649.8705, 31.0670>>
ParkedCarQuatX[21] = 0.0002
ParkedCarQuatY[21] = -0.0009
ParkedCarQuatZ[21] = 0.7043
ParkedCarQuatW[21] = 0.7099
ParkedCarModel[21] = bison

ParkedCarPos[22] = <<-391.1157, -649.4840, 30.8430>>
ParkedCarQuatX[22] = 0.0009
ParkedCarQuatY[22] = 0.0010
ParkedCarQuatZ[22] = 0.7076
ParkedCarQuatW[22] = 0.7066
ParkedCarModel[22] = manana

ParkedCarPos[23] = <<-397.3228, -656.3524, 31.0987>>
ParkedCarQuatX[23] = 0.0011
ParkedCarQuatY[23] = -0.0002
ParkedCarQuatZ[23] = 0.7043
ParkedCarQuatW[23] = 0.7099
ParkedCarModel[23] = bison

ParkedCarPos[24] = <<-383.3736, -655.1855, 30.8697>>
ParkedCarQuatX[24] = -0.0042
ParkedCarQuatY[24] = -0.0042
ParkedCarQuatZ[24] = 0.7069
ParkedCarQuatW[24] = 0.7073
ParkedCarModel[24] = manana

ParkedCarPos[25] = <<-397.5586, -665.7125, 30.8526>>
ParkedCarQuatX[25] = 0.0015
ParkedCarQuatY[25] = -0.0015
ParkedCarQuatZ[25] = -0.7071
ParkedCarQuatW[25] = 0.7071
ParkedCarModel[25] = manana

//ParkedCarPos[26] = <<-412.6671, -1556.3729, 38.3425>>
//ParkedCarQuatX[26] = -0.0102
//ParkedCarQuatY[26] = 0.0114
//ParkedCarQuatZ[26] = -0.1777
//ParkedCarQuatW[26] = 0.9840
//ParkedCarModel[26] = PHANTOM //Replaced for memory reasons PACKER

ParkedCarPos[27] = <<-410.1394, -1574.8274, 38.3190>>
ParkedCarQuatX[27] = -0.0199
ParkedCarQuatY[27] = 0.0216
ParkedCarQuatZ[27] = -0.2855
ParkedCarQuatW[27] = 0.9579
ParkedCarModel[27] = dilettante

ParkedCarPos[28] = <<-423.7487, -1607.9525, 27.6239>>
ParkedCarQuatX[28] = -0.0349
ParkedCarQuatY[28] = 0.0535
ParkedCarQuatZ[28] = -0.1590
ParkedCarQuatW[28] = 0.9852
ParkedCarModel[28] = PHANTOM //Replaced for memory reasons PACKER

ParkedCarPos[29] = <<-495.3004, -1617.7054, 35.9875>>
ParkedCarQuatX[29] = 0.0291
ParkedCarQuatY[29] = 0.0940
ParkedCarQuatZ[29] = 0.9243
ParkedCarQuatW[29] = 0.3687
ParkedCarModel[29] = feltzer2

ParkedCarPos[30] = <<-499.6737, -1622.2612, 36.7879>>
ParkedCarQuatX[30] = 0.0038
ParkedCarQuatY[30] = 0.0305
ParkedCarQuatZ[30] = 0.9276
ParkedCarQuatW[30] = 0.3723
ParkedCarModel[30] = feltzer2

ParkedCarPos[31] = <<-509.7415, -1632.2365, 36.6421>>
ParkedCarQuatX[31] = 0.0085
ParkedCarQuatY[31] = 0.0438
ParkedCarQuatZ[31] = 0.9163
ParkedCarQuatW[31] = 0.3980
ParkedCarModel[31] = PHANTOM //Replaced for memory reasons PACKER

ParkedCarPos[32] = <<-650.9254, -2154.5872, 48.9261>>
ParkedCarQuatX[32] = -0.0441
ParkedCarQuatY[32] = -0.0214
ParkedCarQuatZ[32] = 0.4611
ParkedCarQuatW[32] = 0.8860
ParkedCarModel[32] = manana

//ParkedCarPos[33] = <<-334.4651, -2369.9304, 62.3249>>
//ParkedCarQuatX[33] = 0.0050
//ParkedCarQuatY[33] = 0.0007
//ParkedCarQuatZ[33] = 0.4621
//ParkedCarQuatW[33] = 0.8868
//ParkedCarModel[33] = baller
//
//ParkedCarPos[34] = <<-314.7362, -2382.6150, 61.6223>>
//ParkedCarQuatX[34] = 0.0066
//ParkedCarQuatY[34] = 0.0050
//ParkedCarQuatZ[34] = 0.4658
//ParkedCarQuatW[34] = 0.8848
//ParkedCarModel[34] = baller



// ****  UBER RECORDED SET PIECE CARS  **** 

SetPieceCarPos[0] = <<-622.8682, -616.4202, 32.5207>>
SetPieceCarQuatX[0] = 0.0196
SetPieceCarQuatY[0] = 0.0296
SetPieceCarQuatZ[0] = 0.0009
SetPieceCarQuatW[0] = 0.9994
SetPieceCarRecording[0] = 999
SetPieceCarStartime[0] = 68669.0000
SetPieceCarRecordingSpeed[0] = 1.0000
SetPieceCarModel[0] = jackal


IF g_family1GarbageTruckSeen = FALSE

	SetPieceCarPos[1] = <<-622.8682, -616.4202, 32.5207>>
	SetPieceCarQuatX[1] = 0.0196
	SetPieceCarQuatY[1] = 0.0296
	SetPieceCarQuatZ[1] = 0.0009
	SetPieceCarQuatW[1] = 0.9994
	SetPieceCarRecording[1] = 998
	SetPieceCarStartime[1] = 59669.0000
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = trash

ENDIF

SetPieceCarPos[2] = <<-1126.4215, -690.3713, 20.2923>>
SetPieceCarQuatX[2] = 0.0196
SetPieceCarQuatY[2] = 0.0296
SetPieceCarQuatZ[2] = 0.0009
SetPieceCarQuatW[2] = 0.9994
SetPieceCarRecording[2] = 995
SetPieceCarStartime[2] = 48200.0000
SetPieceCarRecordingSpeed[2] = 1.1500
SetPieceCarModel[2] = flatbed

SetPieceCarPos[3] = <<-1126.4215, -690.3713, 20.2923>>
SetPieceCarQuatX[3] = 0.0196
SetPieceCarQuatY[3] = 0.0296
SetPieceCarQuatZ[3] = 0.0009
SetPieceCarQuatW[3] = 0.9994
SetPieceCarRecording[3] = 994
SetPieceCarStartime[3] = 48200.0000
SetPieceCarRecordingSpeed[3] = 1.1500
SetPieceCarModel[3] = Gauntlet


IF g_Family1BallerSpawnAfter1stTry = TRUE
	SetPieceCarPos[4] = <<-383.3884, -498.7355, 40.7443>>
	SetPieceCarQuatX[4] = 0.0196
	SetPieceCarQuatY[4] = 0.0296
	SetPieceCarQuatZ[4] = 0.0009
	SetPieceCarQuatW[4] = 0.9994
	SetPieceCarRecording[4] = 967//993
	SetPieceCarStartime[4] = 72450.0000
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = baller2
ENDIF

SetPieceCarPos[5] = <<-238.5849, -1205.8152, 35.9661>>
SetPieceCarQuatX[5] = 0.0196
SetPieceCarQuatY[5] = 0.0296
SetPieceCarQuatZ[5] = 0.0009
SetPieceCarQuatW[5] = 0.9994
SetPieceCarRecording[5] = 990
SetPieceCarStartime[5] = 106000.0000
SetPieceCarRecordingSpeed[5] = 1.0000
SetPieceCarModel[5] = banshee

SetPieceCarPos[6] = <<-398.7839, -1147.3693, 37.3474>>
SetPieceCarQuatX[6] = 0.0000
SetPieceCarQuatY[6] = -0.0159
SetPieceCarQuatZ[6] = 0.0000
SetPieceCarQuatW[6] = 0.9999
SetPieceCarRecording[6] = 985
SetPieceCarStartime[6] = 95819.0000
SetPieceCarRecordingSpeed[6] = 1.0000
SetPieceCarModel[6] = PHANTOM //Replaced for memory reasons PACKER

SetPieceCarPos[7] = <<-2027.8131, -381.2849, 10.0633>>
SetPieceCarQuatX[7] = -0.0067
SetPieceCarQuatY[7] = 0.0002
SetPieceCarQuatZ[7] = -0.0009
SetPieceCarQuatW[7] = 1.0000
SetPieceCarRecording[7] = 980
SetPieceCarStartime[7] = 13000.0000
SetPieceCarRecordingSpeed[7] = 1.1000
SetPieceCarModel[7] = surfer

SetPieceCarPos[8] = <<-1982.0554, -481.2250, 11.5967>>
SetPieceCarQuatX[8] = 0.0001
SetPieceCarQuatY[8] = -0.0010
SetPieceCarQuatZ[8] = 0.9071
SetPieceCarQuatW[8] = -0.4210
SetPieceCarRecording[8] = 978
SetPieceCarStartime[8] = 20000.0000
SetPieceCarRecordingSpeed[8] = 1.0000
SetPieceCarModel[8] = jackal

SetPieceCarPos[9] = <<-1705.3868, -689.9981, 11.1523>>
SetPieceCarQuatX[9] = 0.0145
SetPieceCarQuatY[9] = 0.0039
SetPieceCarQuatZ[9] = 0.8919
SetPieceCarQuatW[9] = -0.4520
SetPieceCarRecording[9] = 977
SetPieceCarStartime[9] = 32000.0000
SetPieceCarRecordingSpeed[9] = 1.0000
SetPieceCarModel[9] = jackal

SetPieceCarPos[11] = <<-859.6334, -2006.7787, 27.6975>>
SetPieceCarQuatX[11] = 0.0095
SetPieceCarQuatY[11] = -0.0133
SetPieceCarQuatZ[11] = 0.4281
SetPieceCarQuatW[11] = 0.9036
SetPieceCarRecording[11] = 973
SetPieceCarStartime[11] = 126150.0000
SetPieceCarRecordingSpeed[11] = 1.0000
SetPieceCarModel[11] = phantom

SetPieceCarPos[12] = <<-866.5450, -1779.6897, 37.4865>>
SetPieceCarQuatX[12] = 0.0089
SetPieceCarQuatY[12] = 0.0018
SetPieceCarQuatZ[12] = -0.4099
SetPieceCarQuatW[12] = 0.9121
SetPieceCarRecording[12] = 972
SetPieceCarStartime[12] = 121150.0000
SetPieceCarRecordingSpeed[12] = 1.0000
SetPieceCarModel[12] = phantom

SetPieceCarPos[13] = <<-1629.0958, -743.3837, 11.1949>>
SetPieceCarQuatX[13] = 0.0072
SetPieceCarQuatY[13] = -0.0048
SetPieceCarQuatZ[13] = 0.8415
SetPieceCarQuatW[13] = -0.5402
SetPieceCarRecording[13] = 971
SetPieceCarStartime[13] = 36475.0000
SetPieceCarRecordingSpeed[13] = 1.0000
SetPieceCarModel[13] = tourbus

SetPieceCarPos[14] = <<-1465.2949, -770.0398, 10.7633>>
SetPieceCarQuatX[14] = -0.0128
SetPieceCarQuatY[14] = -0.0108
SetPieceCarQuatZ[14] = -0.6759
SetPieceCarQuatW[14] = 0.7368
SetPieceCarRecording[14] = 970
SetPieceCarStartime[14] = 41475.0000
SetPieceCarRecordingSpeed[14] = 1.0000
SetPieceCarModel[14] = stretch

//SetPieceCarPos[15] = <<-629.6671, -540.7661, 24.9790>>
//SetPieceCarQuatX[15] = 0.0114
//SetPieceCarQuatY[15] = 0.0070
//SetPieceCarQuatZ[15] = 0.8000
//SetPieceCarQuatW[15] = -0.5999
//SetPieceCarRecording[15] = 969
//SetPieceCarStartime[15] = 61475.0000//58475.0000
//SetPieceCarRecordingSpeed[15] = 1.0000
//SetPieceCarModel[15] = burrito

SetPieceCarPos[16] = <<-370.3791, -2359.6836, 63.0124>>
SetPieceCarQuatX[16] = 0.0020
SetPieceCarQuatY[16] = -0.0040
SetPieceCarQuatZ[16] = 0.8833
SetPieceCarQuatW[16] = -0.4688
SetPieceCarRecording[16] = 968
SetPieceCarStartime[16] = 154280.0000
SetPieceCarRecordingSpeed[16] = 1.0000
SetPieceCarModel[16] = burrito

SetPieceCarPos[17] = <<826.9321, -2616.7432, 52.1236>>
SetPieceCarQuatX[17] = 0.0072
SetPieceCarQuatY[17] = 0.0082
SetPieceCarQuatZ[17] = 0.7724
SetPieceCarQuatW[17] = 0.6350
SetPieceCarRecording[17] = 695
SetPieceCarStartime[17] = 188440.0000
SetPieceCarRecordingSpeed[17] = 1.0000
SetPieceCarModel[17] = tiptruck

SetPieceCarPos[18] = <<1022.2120, -2583.2656, 40.5674>>
SetPieceCarQuatX[18] = 0.0324
SetPieceCarQuatY[18] = 0.0397
SetPieceCarQuatZ[18] = 0.7572
SetPieceCarQuatW[18] = 0.6512
SetPieceCarRecording[18] = 694
SetPieceCarStartime[18] = 196588.0000
SetPieceCarRecordingSpeed[18] = 1.0000
SetPieceCarModel[18] = taxi

SetPieceCarPos[20] = <<-842.8928, -1750.5757, 37.5273>>
SetPieceCarQuatX[20] = 0.0259
SetPieceCarQuatY[20] = -0.0149
SetPieceCarQuatZ[20] = 0.8538
SetPieceCarQuatW[20] = 0.5198
SetPieceCarRecording[20] = 679
SetPieceCarStartime[20] = 120952.0000
SetPieceCarRecordingSpeed[20] = 1.0000
SetPieceCarModel[20] = speedo

SetPieceCarPos[21] = <<-1896.5000, -544.8254, 11.5101>>
SetPieceCarQuatX[21] = 0.0009
SetPieceCarQuatY[21] = -0.0020
SetPieceCarQuatZ[21] = 0.8961
SetPieceCarQuatW[21] = -0.4439
SetPieceCarRecording[21] = 678
SetPieceCarStartime[21] = 23320.0000
SetPieceCarRecordingSpeed[21] = 1.0000
SetPieceCarModel[21] = speedo

SetPieceCarPos[22] = <<-1825.2657, -603.6853, 11.1391>>
SetPieceCarQuatX[22] = 0.0053
SetPieceCarQuatY[22] = -0.0114
SetPieceCarQuatZ[22] = 0.9085
SetPieceCarQuatW[22] = -0.4177
SetPieceCarRecording[22] = 669
SetPieceCarStartime[22] = 26662.0000
SetPieceCarRecordingSpeed[22] = 1.0000
SetPieceCarModel[22] = blista

SetPieceCarPos[24] = <<-417.5042, -809.2904, 36.9324>>
SetPieceCarQuatX[24] = -0.0126
SetPieceCarQuatY[24] = -0.0041
SetPieceCarQuatZ[24] = 0.9999
SetPieceCarQuatW[24] = -0.0106
SetPieceCarRecording[24] = 667
SetPieceCarStartime[24] = 77000.0000
SetPieceCarRecordingSpeed[24] = 1.0000
SetPieceCarModel[24] = fugitive

SetPieceCarPos[25] = <<-415.8903, -773.4919, 36.7720>>
SetPieceCarQuatX[25] = -0.0161
SetPieceCarQuatY[25] = 0.0005
SetPieceCarQuatZ[25] = 0.9999
SetPieceCarQuatW[25] = 0.0034
SetPieceCarRecording[25] = 666
SetPieceCarStartime[25] = 77520.0000
SetPieceCarRecordingSpeed[25] = 1.0000
SetPieceCarModel[25] = washington

SetPieceCarPos[26] = <<-399.9667, -900.1065, 37.2774>>
SetPieceCarQuatX[26] = 0.0026
SetPieceCarQuatY[26] = -0.0176
SetPieceCarQuatZ[26] = -0.0058
SetPieceCarQuatW[26] = 0.9998
SetPieceCarRecording[26] = 101
SetPieceCarStartime[26] = 82942.0000
SetPieceCarRecordingSpeed[26] = 1.0000
SetPieceCarModel[26] = Phantom

SetPieceCarPos[27] = <<-1606.4501, -749.8375, 11.0267>>
SetPieceCarQuatX[27] = -0.0031
SetPieceCarQuatY[27] = 0.0002
SetPieceCarQuatZ[27] = 0.8433
SetPieceCarQuatW[27] = -0.5375
SetPieceCarRecording[27] = 37
SetPieceCarStartime[27] = 36173.0000 //36473.0000
SetPieceCarRecordingSpeed[27] = 1.0000
SetPieceCarModel[27] = jackal

// ****  POSSIBLE REDUNDANT CAR RECORDINGS  **** 
//
// The following car recordings MIGHT be redundant. 
// They were associated with traffic and set piece cars 
// which are now invalid. Please check carefully if they 
// are being used somewhere else. If not, remove them from 
// alienbrian and the car recording image. 
//
// 676
// 675
// 674
// 673
// 672
// 671
// 670
//

 // **** end of uber output **** 
 
 block_vehicle_colour = FALSE

ENDPROC


//PURPOSE: Starts or stops a cutscene. If TRUE is passed TIMERA() is set to Zero
PROC SET_CUTSCENE_RUNNING(BOOL isRunning, BOOL doGameCamInterp = FALSE, INT durationFromInterp = 2000, BOOL turnOffGadgets = TRUE, BOOL bToggleBlinders = TRUE)

//	SET_USE_HIGHDOF(isRunning)removed
	SET_WIDESCREEN_BORDERS(isRunning,0)
	IF turnOffGadgets = TRUE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning),  SPC_DEACTIVATE_GADGETS)
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning))
	ENDIF
	RENDER_SCRIPT_CAMS(isRunning, doGameCamInterp, durationFromInterp)
	
	CLEAR_HELP(TRUE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(isRunning, DEFAULT, bToggleBlinders)
		
	DISABLE_CELLPHONE(isRunning)
	DISPLAY_HUD(NOT isRunning)
	DISPLAY_RADAR(NOT isRunning)
ENDPROC

//Cleanup
PROC MISSION_CLEANUP()

	ALLOW_SONAR_BLIPS(TRUE)

	TRIGGER_MUSIC_EVENT("FAM1_FAIL")
	STOP_AUDIO_SCENE("FAM1_RADIO_FADE")
	#IF IS_DEBUG_BUILD
		DELETE_WIDGET_GROUP(family1WidgetGroup)
	
		g_b_CellDialDebugTextToggle = TRUE
	
		STOP_RECORDING_ALL_VEHICLES()
	
	#ENDIF
	INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_01_AP, FALSE)
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, FALSE)

	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	UNLOAD_ALL_CLOUD_HATS()
	CLEAR_WEATHER_TYPE_PERSIST()
	
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_IS_IGNORED_BY_AUTO_OPEN_DOORS(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	SET_CUTSCENE_RUNNING(FALSE)
	
	HANG_UP_AND_PUT_AWAY_PHONE() 
	
	
	SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
		
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, FALSE)
	
	CLEANUP_UBER_PLAYBACK()
	
	RESET_MISSION_STATS_ENTITY_WATCH()

	STOP_AUDIO_SCENE("FAM1_HIGHWAY_CHASE")

	SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
	SET_REDUCE_PED_MODEL_BUDGET(FALSE)
	
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	//CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MICHAEL_SAVEHOUSE)
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_MAX_WANTED_LEVEL(5)
	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(carPlayersJeep, SC_DOOR_BONNET, TRUE)
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SENTINEL2, FALSE)

	SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), TRUE)

	CLEAR_TIMECYCLE_MODIFIER()
	SET_TIME_SCALE(1.0)		

	//SET_VEHICLE_DENSITY_MULTIPLIER(0.25)
	
	DELETE_VEHICLE(cameraCar)
	DELETE_VEHICLE(cameraCarCamera)
	
	IF IS_SCREEN_FADED_OUT()
		DELETE_VEHICLE(truckYachtPacker)
		DELETE_VEHICLE(truckYachtPackerTrailer)
		DELETE_OBJECT(oiMast)
		DELETE_PED(pedBadguy1)
		DELETE_PED(pedBadguy2)
		DELETE_PED(pedBadGuy3BoxThrower)
		DELETE_PED(pedBadguy4BoomGuy)
		DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
		DELETE_PED(pedJimmy)
		//DO_SCREEN_FADE_IN(500)
	ENDIF
	
ENDPROC

BOOL bIsReplayInProgress

PROC FADE_IN_IF_NEEDED()
	
	IF IS_SCREEN_FADED_OUT()
	//OR IS_SCREEN_FADING_OUT()
	//OR NOT IS_SCREEN_FADING_IN()
		RESET_GAME_CAMERA()
		CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20.0, TRUE, TRUE)
		IF NOT bIsReplayInProgress
			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		DO_SCREEN_FADE_IN(500)
	ENDIF

ENDPROC

INT VehicleStartColour1, VehicleStartColour2

PROC Mission_Passed()	

//  Removed for TODO #1223338 -BenR
//
//	IF GET_ENTITY_HEALTH(carPlayersJeep) > 950
//		SET_MISSION_FLOW_INT_VALUE(FLOWINT_MISSION_FAM1_CAR_END_STATE, 1)	//Car repaired
//	ELSE
//		SET_MISSION_FLOW_INT_VALUE(FLOWINT_MISSION_FAM1_CAR_END_STATE,2)	//Car Damaged
//	ENDIF
//	
//	INT VehicleColour1, VehicleColour2
//		
//	GET_VEHICLE_COLOURS(carPlayersJeep, VehicleColour1, VehicleColour2)
//	PRINTLN("Previoous colours:", VehicleStartColour1, ":", VehicleStartColour2 )
//	PRINTLN("Current   colours:", VehicleColour1, ":", VehicleColour2 )
//	
//	IF (GET_MAX_VEHICLE_UPGRADE_LEVEL(carPlayersJeep) > 0.0) //GET_MAX_VEHICLE_UPGRADE_LEVEL(carPlayersJeep)
//	OR (VehicleColour1 <> VehicleStartColour1) //Cars been painted - primary
//	OR (VehicleColour2 <> VehicleStartColour2) //Cars been paitned - secondary
//		SCRIPT_ASSERT("caar colour changed")
//		SET_MISSION_FLOW_INT_VALUE(FLOWINT_MISSION_FAM1_CAR_END_STATE, 3)	//Car modded!
//	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF DOES_BLIP_EXIST(blipTruckYachtPackerTrailer)
			REMOVE_BLIP(blipTruckYachtPackerTrailer)
		ENDIF
		IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
			STOP_RECORDING_VEHICLE(truckYachtPackerTrailer)
		ENDIF
		
		STOP_CAM_RECORDING(camRecData)
		
	#ENDIF
	SET_CUTSCENE_RUNNING(FALSE)
	
	//TODO 1263442: Closing Michael's gate behind Franklin. The flow will
	//reenable this when Franklin is out of range of the gate.
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
	ENDIF
	
	Mission_Flow_Mission_Passed()
	CLEAR_PRINTS () 	
	Mission_Cleanup()
	FADE_IN_IF_NEEDED()
	TERMINATE_THIS_THREAD()
		
ENDPROC

//Mission Failed
PROC Mission_Failed()	
	TRIGGER_MUSIC_EVENT("FAM1_FAIL") //- BRoken
	
	//TEmp
	//TRIGGER_MUSIC_EVENT("CHN1_FAIL ")
	
	KILL_ANY_CONVERSATION()
	
	SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_01_AP, FALSE)
	
	#IF IS_DEBUG_BUILD
		REMOVE_BLIP(blipTruckYachtPackerTrailer)
		IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
			STOP_RECORDING_VEHICLE(truckYachtPackerTrailer)
		ENDIF
		
		STOP_CAM_RECORDING(camRecData)
	#ENDIF

//	IF IS_PLAYER_PLAYING(PLAYER_ID())
//		SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
//	ENDIF
	
	Mission_Flow_Mission_Failed()
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS("YB_JIMTXT")
	
	DELETE_PED(pedJimmy)
	DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread	
	TERMINATE_THIS_THREAD()
ENDPROC


PROC REQUEST_CHASE_INITIAL_ASSETS()
	
	REQUEST_MODEL(TR3)
	REQUEST_MODEL(PHANTOM)
	REQUEST_MODEL(G_M_Y_SalvaGoon_01)
	REQUEST_MODEL(Prop_LD_Test_01)
	
	REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_01")
	REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_02")
	REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
	REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_BOAT_STRAIN")
	
ENDPROC

FUNC BOOL HAVE_CHASE_INITIAL_ASSETS_LOADED()

	IF HAS_MODEL_LOADED(TR3)
	AND HAS_MODEL_LOADED(PHANTOM)
	AND HAS_MODEL_LOADED(G_M_Y_SalvaGoon_01)
	AND HAS_MODEL_LOADED(Prop_LD_Test_01)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC RELEASE_CHASE_INITIAL_ASSETS()

	SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS2)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(TR3)
	SET_MODEL_AS_NO_LONGER_NEEDED(PHANTOM)
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_SalvaGoon_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_LD_Test_01)
	
ENDPROC

PROC CREATE_CAMERAS_AND_YACHT()

	DESTROY_ALL_CAMS()
	cutsceneCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")					
//	initialCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//	destinationCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	cameraRecorded1 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	cameraRecorded2 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	
	SET_CAM_FOV(cameraRecorded1, 28.0)
	SET_CAM_FOV(cameraRecorded2, 25.0)
	
	PRINTSTRING("CREATE_CAMERAS_AND_YACHT() HIT ") PRINTNL()

	REQUEST_CHASE_INITIAL_ASSETS()
		
	WHILE NOT HAVE_CHASE_INITIAL_ASSETS_LOADED()
		WAIT(0)
	ENDWHILE

	truckYachtPacker = CREATE_VEHICLE(PHANTOM, vTruckYachtPackerStartPosition, fTruckYachtPackerStartHeading)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(truckYachtPacker, "FAMILY_1_TARGET_TRUCK_AND_TRAILER")
	FREEZE_ENTITY_POSITION(truckYachtPacker, TRUE)
	//SET_ENTITY_PROOFS(truckYachtPacker, TRUE, TRUE, TRUE, TRUE, TRUE)
	SET_ENTITY_PROOFS(truckYachtPacker, TRUE, TRUE, TRUE, TRUE, TRUE)
	
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(truckYachtPacker, TRUE)
	SET_ENTITY_INVINCIBLE(truckYachtPacker, TRUE)

	//[MF] hide the trailer so the player doesn't see it if they take a obscure round-about route
	IF mission_stage < STAGE_DO_CHASE
	
		SET_ENTITY_VISIBLE(truckYachtPacker, FALSE)

	ENDIF

	DELETE_VEHICLE(cameraCar)
	DELETE_VEHICLE(cameraCarCamera)

	cameraCar = CREATE_VEHICLE(PHANTOM, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(3, 0.0, "Fam1Cam"), fTruckYachtPackerStartHeading)
	cameraCarCamera = CREATE_VEHICLE(PHANTOM, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(4, 0.0, "Fam1Cam"), fTruckYachtPackerStartHeading)
	
	SET_ENTITY_INVINCIBLE(cameraCar, TRUE)
	SET_ENTITY_INVINCIBLE(cameraCarCamera, TRUE)
	
	//IF NOT IS_ENTITY_DEAD(truckYachtPacker)
	//AND NOT IS_ENTITY_DEAD(carPlayersJeep)
		truckYachtPackerTrailer = CREATE_VEHICLE(TR3, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, 3.0, 0.0>>), fTruckYachtPackerStartHeading)
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(truckYachtPackerTrailer, "FAMILY_1_TARGET_TRUCK_AND_TRAILER")
		SET_VEHICLE_EXTRA(truckYachtPackerTrailer, 1, FALSE)
		FREEZE_ENTITY_POSITION(truckYachtPackerTrailer, TRUE)
		SET_ENTITY_PROOFS(truckYachtPackerTrailer, FALSE, TRUE, FALSE, TRUE, FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(truckYachtPackerTrailer, TRUE)
		SET_ENTITY_INVINCIBLE(truckYachtPackerTrailer, TRUE)
		oiBlipObject = CREATE_OBJECT(Prop_LD_Test_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<0.0, -10.0, 0.0>>))
		ATTACH_ENTITY_TO_ENTITY(oiBlipObject, truckYachtPackerTrailer, 0, <<0.0, -7.0, -0.5>>, <<0.0, 0.0, 0.0>>)
		SET_ENTITY_VISIBLE(oiBlipObject, FALSE)
		
		//[MF] hide the trailer so the player doesn't see it if they take a obscure round-about route
		IF mission_stage < STAGE_DO_CHASE
		
			SET_ENTITY_VISIBLE(truckYachtPackerTrailer, FALSE)
		
		ENDIF
		//SET_ENTITY_COLLISION(oiBlipObject, FALSE)
		//ATTACH_VEHICLE_TO_TRAILER(truckYachtPacker, truckYachtPackerTrailer)
	//ENDIF
	
	pedYachtPacker = CREATE_PED_INSIDE_VEHICLE(truckYachtPacker, PEDTYPE_MISSION, G_M_Y_SalvaGoon_01)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(pedYachtPacker, RELGROUPHASH_PLAYER)
	
	//SET_ENTITY_INVINCIBLE(pedYachtPacker, TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedYachtPacker, TRUE)
//		
	REMOVE_BLIP(blipPlayersCar)
			
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	IF NOT IS_PED_INJURED(pedFranklin())
		SET_PED_CONFIG_FLAG(pedFranklin(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF

ENDPROC


PROC ADD_BLIP_FOR_YACHT()
	
	IF DOES_BLIP_EXIST(blipTruckYachtPackerTrailer)
		REMOVE_BLIP(blipTruckYachtPackerTrailer)
	ENDIF
	
	blipTruckYachtPackerTrailer =  ADD_BLIP_FOR_ENTITY(oiBlipObject) //CREATE_BLIP_FOR_VEHICLE(truckYachtPackerTrailer)
	SET_BLIP_AS_FRIENDLY(blipTruckYachtPackerTrailer, TRUE)

ENDPROC

PROC requestAndCreateInitialStuff(MISSION_STAGE_FLAG thisStage = STAGE_INITIALISE)

	PRINTLN("**************************************")
	PRINTLN("**   RequestAndCreateInitialStuff   **")
	PRINTLN("**", mission_stage)
	PRINTLN("**************************************")
	
	REQUEST_VEHICLE_ASSET(SENTINEL2)
	REQUEST_NPC_VEH_MODEL(CHAR_AMANDA)
	REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))  
	REQUEST_ADDITIONAL_TEXT("FAMILY1", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("FAM1AUD", MISSION_DIALOGUE_TEXT_SLOT)
	
	WHILE NOT HAS_VEHICLE_ASSET_LOADED(SENTINEL2)
	OR NOT HAS_NPC_VEH_MODEL_LOADED(CHAR_AMANDA)
	OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		PRINTSTRING("waiting for assets...")
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		
		WAIT(0)
	ENDWHILE

	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)	  

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
//	IF NOT IS_VEHICLE_IN_PLAYERS_GARAGE(GET_LAST_DRIVEN_VEHICLE(), CHAR_MICHAEL, FALSE)
//		REPOSITION_PLAYERS_VEHICLE(<< -824.5910, 157.7898, 68.6935 >>, 87.2018)
//	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
		IF NOT IS_VEHICLE_IN_PLAYERS_GARAGE(GET_PLAYERS_LAST_VEHICLE(), CHAR_MICHAEL, FALSE)
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-843.985779,158.654846,64.907990>>, <<-777.540039,183.186569,81.335342>>, 34.250000, <<-858.9615, 147.4493, 61.4344>>, 357.0481, <<5.0, 7.0, 8.5>>, TRUE, TRUE, TRUE, TRUE)
		ENDIF
	ENDIF
		
	//CLEAR_AREA(vJeepStartPosition, 60.0, TRUE)
	
	//carPlayersJeep = CREATE_VEHICLE(SENTINEL2, vJeepStartPosition, fJeepStartHeading)
	
	IF thisStage <> STAGE_INITIALISE
		
		SWITCH thisStage
		
			CASE STAGE_GET_INTO_AMANDAS_CAR
			CASE STAGE_GET_TO_LOCATION
				vJeepStartPosition = <<-825.1714, 178.9298, 70.3721>>
			BREAK
		
			CASE STAGE_DO_CHASE
				vJeepStartPosition = <<-2146.3821, -255.5757, 13.9293>>
			BREAK
			
			CASE STAGE_JIMMY_APPEARS
				vJeepStartPosition = <<-416.0598, -1124.7811, 36.1469>>
			BREAK
		
			CASE STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
				vJeepStartPosition = <<-650.3306, -2171.4126, 49.4297>>
			BREAK
				
		ENDSWITCH
	
	ENDIF
	
	CREATE_NPC_VEHICLE(carPlayersJeep, CHAR_AMANDA, vJeepStartPosition, fJeepStartHeading )
	PRINTLN("Creating PLayers car at :", vJeepStartPosition)
	
	#IF IS_NEXTGEN_BUILD
		SET_VEHICLE_EXTRA(carPlayersJeep, 1, TRUE)
	#ENDIF
	
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(carPlayersJeep, FALSE)
	ADD_VEHICLE_UPSIDEDOWN_CHECK(carPlayersJeep)
	SET_VEHICLE_COLOURS(carPlayersJeep, carColour1, carColour1)
	//SET_VEHICLE_EXTRA_COLOURS(carPlayersJeep, carExtraColour1, carExtraColour2)
	SET_VEHICLE_DIRT_LEVEL(carPlayersJeep, 0.0)
	SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(carPlayersJeep, SC_DOOR_BONNET, FALSE)
	SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(carPlayersJeep, SC_DOOR_FRONT_LEFT, FALSE)
	SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
	SET_VEHICLE_STRONG(carPlayersJeep, TRUE)
	SET_VEHICLE_CAN_LEAK_OIL(carPlayersJeep, FALSE)
	SET_VEHICLE_CAN_LEAK_PETROL(carPlayersJeep, FALSE)
	SET_ENTITY_HEALTH(carPlayersJeep, 1200)
	SET_VEHICLE_TYRES_CAN_BURST(carPlayersJeep, FALSE)
	LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
	SET_VEHICLE_HAS_STRONG_AXLES(carPlayersJeep, TRUE)
	
	INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(carPlayersJeep)
	
	GET_VEHICLE_COLOURS(carPlayersJeep, VehicleStartColour1, VehicleStartColour2)
	
	//SET_VEHICLE_NUMBER_PLATE_TEXT(carPlayersJeep, " KRYST4L")
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SENTINEL2, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SENTINEL, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TR3, TRUE)
	
	//pedSelector.pedID[SELECTOR_PED_FRANKLIN] = CREATE_PED(PEDTYPE_MISSION, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN), <<723.9294, -1087.4998, 21.1693>>, 96.2191)
		
	IF NOT DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
		CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, <<-794.9062, 176.0349, 71.8348>>, 96.2191)
		//SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_SHIRT_JEANS , FALSE)
		//sorry(pedSelector.pedID[SELECTOR_PED_FRANKLIN], INT_TO_ENUM(PED_PROP_POSITION,0), 16, 0)
	ENDIF				
	
	IF NOT IS_PED_INJURED(pedFranklin())
		SET_PED_SUFFERS_CRITICAL_HITS(pedFranklin(), FALSE)
		//SET_ENTITY_INVINCIBLE(pedFranklin(), TRUE)
		SET_PED_CONFIG_FLAG(pedFranklin(), PCF_WillFlyThroughWindscreen, FALSE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedFranklin(), FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFranklin(), TRUE)
		SET_PED_CAN_BE_DRAGGED_OUT(pedFranklin(), FALSE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFranklin(), FALSE)
		
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedFranklin(), TRUE)
		
		SET_CURRENT_PED_WEAPON(pedFranklin(), WEAPONTYPE_UNARMED, TRUE)
		
		SET_PED_RELATIONSHIP_GROUP_HASH(pedFranklin(), RELGROUPHASH_PLAYER)
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedSelector.pedID[SELECTOR_PED_FRANKLIN], "FAMILY_1_BOAT_PEDS")
	ENDIF
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, thievesRelGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, thievesRelGroup, RELGROUPHASH_PLAYER)
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, pedFranklin(), "FRANKLIN")

//	IF IS_SCREEN_FADED_OUT()
//		RESET_GAME_CAMERA()
//	ENDIF
	
ENDPROC

PROC DELETE_ARRAY_OF_VEHICLES(VEHICLE_INDEX &Vehicles[], BOOL bDelete = TRUE)

	INT i
	
	FOR i = 0 TO COUNT_OF(Vehicles) -1
		IF DOES_ENTITY_EXIST(vehicles[i])
			SET_ENTITY_AS_MISSION_ENTITY(vehicles[i])
			IF bDelete
				DELETE_VEHICLE(vehicles[i])
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicles[i])
			ENDIF
			PRINTLN("deleted vehicle in array at: ", i)
		ENDIF
	ENDFOR
	
ENDPROC


PROC REQUEST_CHASE_ASSETS()
	
	setupTrafficFamily1()
	
	//REQUEST_VEHICLE_RECORDING(1, "Fam1Blend")
	
	REQUEST_VEHICLE_RECORDING(1, "Fam1New")
	REQUEST_VEHICLE_RECORDING(2, "Fam1New")
	
	REQUEST_VEHICLE_RECORDING(1, "Fam1Cine")
	REQUEST_CAM_RECORDING(3, "Fam1Cam")
	REQUEST_CAM_RECORDING(4, "Fam1Cam")
	
	REQUEST_ANIM_DICT("MISSFAM1_YachtBattle")
	
	REQUEST_MODEL(G_M_Y_SalvaGoon_01)
	REQUEST_MODEL(PHANTOM)
	REQUEST_MODEL(TR3)
	REQUEST_ANIM_DICT("MISSFAM1_YachtBattleincar01_")
	
	REQUEST_PTFX_ASSET()
	
	REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_01")
	REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_02")
	REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
	REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_BOAT_STRAIN")	//
	

	//INT iNumRecs
	
//	FOR iNumRecs = 0 TO 10 //TOTAL_NUMBER_OF_TRAFFIC_CARS - 1
//		IF TrafficCarRecording[iNumRecs] <> 0
//			REQUEST_VEHICLE_RECORDING(TrafficCarRecording[iNumRecs], "Fam1")
//		ENDIF
//	ENDFOR

//	FOR iNumRecs = 0 TO 5 //TOTAL_NUMBER_OF_SET_PIECE_CARS - 1
//		IF SetPieceCarRecording[iNumRecs] <> 0
//			REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[iNumRecs], "Fam1")
//		ENDIF
//	ENDFOR

ENDPROC


FUNC BOOL HAVE_CHASE_ASSETS_LOADED()

	//IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Fam1Blend")
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Fam1New")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Fam1Cine")
	AND HAS_CAM_RECORDING_LOADED(3, "Fam1Cam")
	AND HAS_CAM_RECORDING_LOADED(4, "Fam1Cam")
	AND REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_01")
	AND REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_02")
	AND REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
	AND REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_BOAT_STRAIN")	//Dont need to wait for this to load.
		PRINTLN("Streamed first lot of assets")
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "Fam1New")
			PRINTLN("Streamed: (4, Fam1T)")
			IF HAS_MODEL_LOADED(G_M_Y_SalvaGoon_01)
				PRINTLN("Streamed: G_M_Y_SalvaGoon_01")
				IF HAS_MODEL_LOADED(PHANTOM)
					PRINTLN("Streamed: PHANTOM")
					IF HAS_MODEL_LOADED(TR3)
						PRINTLN("Streamed: TR3")
						IF HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleincar01_")
							PRINTLN("Streamed: MISSFAM1_YachtBattleincar01_")
							//IF HAS_ANIM_SET_LOADED("drive_by@low_ds")
								PRINTLN("Streamed: drive_by")
								IF HAS_PTFX_ASSET_LOADED()								
									PRINTLN("Streamed: PTFX")
									RETURN TRUE
								ENDIF
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	PRINTLN("Streaming assets")

	RETURN FALSE

ENDFUNC

PROC RELEASE_CHASE_ASSETS()
	
	REMOVE_VEHICLE_RECORDING(1, "Fam1New")
	REMOVE_VEHICLE_RECORDING(1, "Fam1Cine")
	REMOVE_VEHICLE_RECORDING(2, "Fam1New")

	REMOVE_VEHICLE_RECORDING(3, "Fam1Cam")
	REMOVE_VEHICLE_RECORDING(4, "Fam1Cam")
		
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_SalvaGoon_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(PHANTOM)
	SET_MODEL_AS_NO_LONGER_NEEDED(TR3)
	REMOVE_ANIM_DICT("MISSFAM1_YachtBattleincar01_")

	REMOVE_ANIM_SET("drive_by@low_ds")
	
	REMOVE_PTFX_ASSET()
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_01")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_02")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_BOAT_STRAIN")
	
	INT iNumRecs
	
	FOR iNumRecs = 0 TO 10 //TOTAL_NUMBER_OF_TRAFFIC_CARS - 1
		IF TrafficCarRecording[iNumRecs] <> 0
			REMOVE_VEHICLE_RECORDING(TrafficCarRecording[iNumRecs], "Fam1")
		ENDIF
	ENDFOR

	FOR iNumRecs = 0 TO 10 //TOTAL_NUMBER_OF_SET_PIECE_CARS - 1
		IF SetPieceCarRecording[iNumRecs] <> 0
			REMOVE_VEHICLE_RECORDING(SetPieceCarRecording[iNumRecs], "Fam1")
		ENDIF
	ENDFOR
	
	DELETE_ARRAY_OF_VEHICLES(SetPieceCarTrailer, FALSE)
	DELETE_ARRAY_OF_VEHICLES(TrafficCarTrailer, FALSE)

ENDPROC

PROC cutsceneSetYachtAbsolute(FLOAT fSkipTime = 13500.0)

//	BUILD_UBER_ROUTE_DEBUG_PATH(route_path, 1, "Fam1New")

	SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)
	SET_REDUCE_PED_MODEL_BUDGET(TRUE)
	
	SET_VEHICLE_POPULATION_BUDGET(0)
	SET_PED_POPULATION_BUDGET(1)

	IF NOT IS_PED_INJURED(pedYachtPacker)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedYachtPacker, TRUE)
	ENDIF

	IF NOT IS_ENTITY_DEAD(truckYachtPacker)
	AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
			IF bUberInit
				CLEANUP_UBER_PLAYBACK()
			ENDIF
			#IF IS_DEBUG_BUILD
				set_uber_parent_widget_group(family1WidgetGroup)
			#ENDIF
			
			
			INITIALISE_UBER_PLAYBACK("Fam1", 502, FALSE)
			bUberInit = TRUE
			setupTrafficFamily1()
			SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(G_M_Y_SalvaGoon_01)
			SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
			
			SET_ENTITY_NO_COLLISION_ENTITY(truckYachtPacker, truckYachtPackerTrailer, FALSE)
						
			FREEZE_ENTITY_POSITION(truckYachtPacker, FALSE)
			FREEZE_ENTITY_POSITION(truckYachtPackerTrailer, FALSE)
			
			SET_ENTITY_LOD_DIST(truckYachtPacker, 1000)
			SET_ENTITY_LOD_DIST(truckYachtPackerTrailer, 1000)
			
			SET_VEHICLE_LOD_MULTIPLIER(truckYachtPackerTrailer, 3.0)
			
			IF NOT IS_ENTITY_DEAD(truckYachtPacker)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPacker)
					STOP_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPackerTrailer)
					STOP_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(cameraCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cameraCar)
					STOP_PLAYBACK_RECORDED_VEHICLE(cameraCar)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(cameraCarCamera)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cameraCarCamera)
					STOP_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera)
				ENDIF
			ENDIF
			
			
			START_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 1, "Fam1New")
			START_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, 2, "Fam1New")
			SET_VEHICLE_ACTIVE_DURING_PLAYBACK(truckYachtPacker, TRUE)		
			SET_VEHICLE_ACTIVE_DURING_PLAYBACK(truckYachtPackerTrailer, TRUE)		
			
			SET_AUDIO_VEHICLE_PRIORITY(truckYachtPacker, AUDIO_VEHICLE_PRIORITY_MAX)
			
			START_CAM_PLAYBACK(cameraRecorded1, cameraCar,  "Fam1Cam", 3)//, 32.0)
			START_CAM_PLAYBACK(cameraRecorded2, cameraCarCamera, "Fam1Cam", 4)//, 32.0)
			
			//WAIT(0)
			
			SET_UBER_PLAYBACK_TO_TIME_NOW(truckYachtPacker, fSkipTime) //11500 + (5500.0 - TIMERA() ) )
			
			IF NOT IS_ENTITY_DEAD(truckYachtPacker)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, ((GET_TIME_POSITION_IN_RECORDING(truckYachtPacker) * -1.0) + fSkipTime))
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(truckYachtPacker)
				
				PAUSE_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker)
				
			ENDIF
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, ((GET_TIME_POSITION_IN_RECORDING(truckYachtPackerTrailer) * -1.0) + fSkipTime))
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(truckYachtPackerTrailer)
				
				PAUSE_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer)
				
			ENDIF
			IF NOT IS_ENTITY_DEAD(cameraCar)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCar, ((GET_TIME_POSITION_IN_RECORDING(cameraCar) * -1.0) + fSkipTime))
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(cameraCar)
				PAUSE_PLAYBACK_RECORDED_VEHICLE(cameraCar)
			ENDIF
			IF NOT IS_ENTITY_DEAD(cameraCarCamera)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera, ((GET_TIME_POSITION_IN_RECORDING(cameraCarCamera) * -1.0) + fSkipTime))
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(cameraCarCamera)
				PAUSE_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera)
			ENDIF						
			//SET_UBER_PLAYBACK_TO_TIME_NOW(truckYachtPacker, 16000) //11500 + (5500.0 - TIMERA() ) )
			//SET_ENTITY_COORDS(truckYachtPackerTrailer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, 3.0, 0.0>>))
		
	ENDIF
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	
	//CREATE_ALL_WAITING_UBER_CARS()
		
ENDPROC

PROC UNPAUSE_UBER_CARS()

	IF NOT IS_ENTITY_DEAD(truckYachtPacker)
		UNPAUSE_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker)
	ENDIF
	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
		UNPAUSE_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer)
	ENDIF
	IF NOT IS_ENTITY_DEAD(cameraCar)
		
		UNPAUSE_PLAYBACK_RECORDED_VEHICLE(cameraCar)
	ENDIF
	IF NOT IS_ENTITY_DEAD(cameraCarCamera)
		UNPAUSE_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera)
	ENDIF	
	
ENDPROC

structTimelapse sTimelapse
BOOL bCutsceneRequested = FALSE
BOOL bLeadinConvoPlayed = FALSE
INT iTimeLapseCut
INT iTODScene
CAMERA_INDEX camAnim
OBJECT_INDEX oiLeftDoor
OBJECT_INDEX oiRightDoor
FLOAT fDistanceToMichael

OBJECT_INDEX oiTumbler
OBJECT_INDEX oiCigar

BOOL bMIssionTriggeredAsMichael
BOOL bSafeToContinueTimelapse

INT iCutsceneRequestTimer = 0

FUNC BOOL CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY_WITH_TIMEOUT( INT &iTimer, INT iTimeout = 5000 )
	iTimer += ROUND( GET_FRAME_TIME() * 1000.0 )
	#IF IS_DEBUG_BUILD
	IF( iTimer >= iTimeout )
		DEBUG_PRINTCALLSTACK()
		CPRINTLN( DEBUG_MISSION, "CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY_WITH_TIMEOUT - timed out!" )
	ENDIF
	#ENDIF
	RETURN ( iTimer >= iTimeout ) OR CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
ENDFUNC

PROC timeLapseCutscene()

	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		bMIssionTriggeredAsMichael = TRUE
	ELSE
		bMIssionTriggeredAsMichael = FALSE
	ENDIF

	IF IS_REPLAY_IN_PROGRESS()
		iTimeLapseCut = -1
		mission_stage = STAGE_INITIALISE	
	ENDIF

	
	SWITCH iTimeLapseCut
			
		CASE 0
		
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
				
				//Has player triggere mission from inside house/dont show walking in-cut
				IF NOT g_sTriggerSceneAssets.flag
					
				
					REQUEST_ANIM_DICT("MISSFAM2MCS_intp1")
					REQUEST_MODEL(V_ILEV_MM_DOORM_L)
					REQUEST_MODEL(V_ILEV_MM_DOORM_R)
					iTimeLapseCut = 1
				ELSE
				
					iTimeLapseCut = 4	
				ENDIF
			ELSE
				SETTIMERA(0)
				iTimeLapseCut = 100
			ENDIF
			
		BREAK
		
		CASE 1
			IF HAS_ANIM_DICT_LOADED("MISSFAM2MCS_intp1")
				iTimeLapseCut++
			ENDIF
		BREAK
		
		CASE 2		
		
			SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
		
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
                DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 1.0, FALSE, FALSE)
                DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_LOCKED, FALSE, TRUE)
            ENDIF
            IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
            	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), -1.0, FALSE, FALSE)
                DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_LOCKED, FALSE, TRUE)
            ENDIF
		
			CREATE_MODEL_HIDE(<<-816.72, 179.10, 72.83>>, 1.0, V_ILEV_MM_DOORM_L, TRUE)
			CREATE_MODEL_HIDE(<<-816.11, 177.51, 72.83>>, 1.0, V_ILEV_MM_DOORM_R, TRUE)
		
			camAnim = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())			
			//iTODScene = CREATE_SYNCHRONIZED_SCENE(<<-817.5673, 179.2546, 71.2152>>, << 0.000, 0.000, -113.000 >>)
			
			iTODScene = CREATE_SYNCHRONIZED_SCENE(<< -817.311, 179.330, 71.241 >>, << 0.000, 0.000, -113.000 >>)
			
			
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iTODScene, "MISSFAM2MCS_intp1", "FAM_2_INT_P1_MICHAEL", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, iTODScene, "FAM_2_INT_P1_CAM", "MISSFAM2MCS_intp1")
			SET_CAM_ACTIVE(camAnim, TRUE)
			SET_CAM_CONTROLS_MINI_MAP_HEADING(camAnim, TRUE)
			
			oiLeftDoor = CREATE_OBJECT(V_ILEV_MM_DOORM_L, <<-816.72, 179.10, 72.83>>)
			oiRightDoor = CREATE_OBJECT(V_ILEV_MM_DOORM_R, <<-816.11, 177.51, 72.83>>)
			
			PLAY_SYNCHRONIZED_ENTITY_ANIM(oiLeftDoor, iTODScene, "FAM_2_INT_P1_doorL", "MISSFAM2MCS_intp1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(oiRightDoor, iTODScene, "FAM_2_INT_P1_doorR", "MISSFAM2MCS_intp1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iTODScene, TRUE)
			
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiLeftDoor)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiRightDoor)
			
			CLEAR_AREA_OF_PROJECTILES(<<-816.72, 179.10, 72.83>>, 10.0)
		
			IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
				IF NOT IS_VEHICLE_IN_PLAYERS_GARAGE(GET_PLAYERS_LAST_VEHICLE(), CHAR_MICHAEL, FALSE)
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-843.985779,158.654846,64.907990>>, <<-777.540039,183.186569,81.335342>>, 34.250000, <<-858.9615, 147.4493, 61.4344>>, 357.0481, <<5.0, 7.0, 8.5>>, TRUE)
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA( << -825.1512, 178.9821, 70.3781 >>, 25.0)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-858.9615, 147.4493, 61.4344>>, 357.0481)
				ENDIF
			ENDIF
			
			SET_CUTSCENE_RUNNING(TRUE)
			
			FADE_IN_IF_NEEDED()
			
			iTimeLapseCut++
		BREAK
		
		CASE 3
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iTODScene)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iTODScene) >= 0.95
					SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
					iTimeLapseCut++	
				ENDIF
			ENDIF
		
		BREAK
			
		CASE 4

			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF NOT DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				IF CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, <<-794.9062, 176.0349, 71.8348>>, 96.2191)	
					SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_SHIRT_JEANS)
				ENDIF
			ENDIF
			
			
			//2350354, blocking the skip for 360 due to bad const-a-load bug which we are unable to diagnose 
			IF DO_TIMELAPSE(SP_MISSION_FAMILY_1, sTimelapse, TRUE, FALSE, DEFAULT, TRUE)
			AND bSafeToContinueTimelapse = TRUE
				IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
					iTimeLapseCut++	
					SETTIMERA(0)
				ENDIF
//				mission_stage = STAGE_INITIALISE	
			ELSE
			
				IF NOT bCutsceneRequested
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-843.985779,158.654846,64.907990>>, <<-777.540039,183.186569,81.335342>>, 34.250000, <<-858.9615, 147.4493, 61.4344>>, 357.0481, <<5.0, 7.0, 8.5>>, TRUE, TRUE, TRUE, TRUE)
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA( << -825.1512, 178.9821, 70.3781 >>, 25.0)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-858.9615, 147.4493, 61.4344>>, 357.0481)
				
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					AND NOT g_sTriggerSceneAssets.flag
						SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-813.2593, 179.4029, 71.1592>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 117.4587)
					ENDIF
					
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
                		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.025, FALSE, TRUE)
                		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
            		ENDIF
            		IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
            			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.025, FALSE, TRUE)
                		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
            		ENDIF
					
					IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_FRANKLIN
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_1_int", CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 )
					ELSE		
						REQUEST_CUTSCENE("family_1_int")
					ENDIF
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, NULL, "TRACEY")
					PRELOAD_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_INT_LI", CONV_PRIORITY_VERY_HIGH)
					
					DISPLAY_RADAR(FALSE)
					
					bCutsceneRequested = TRUE
				ENDIF
				
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY_WITH_TIMEOUT( iCutsceneRequestTimer )
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL//GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL	
						SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_WHITE_SHIRT_JEANS)
					ENDIF
				
					SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_SHIRT_SHORTS_1)
					bSafeToContinueTimelapse = TRUE				
				ENDIF
				
			ENDIF
					
		BREAK
		
		CASE 5
			IF bCutsceneRequested = TRUE
			AND bLeadinConvoPlayed = FALSE
			AND GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL				
			
				BEGIN_PRELOADED_CONVERSATION()
				iTimeLapseCut++	
				bLeadinConvoPlayed = TRUE
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				mission_stage = STAGE_INITIALISE		
			ENDIF
		BREAK
		
		//Franklin Starting mission
		
		CASE 100
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			//2350354, blocking the skip for 360 due to bad const-a-load bug which we are unable to diagnose 
			IF DO_TIMELAPSE(SP_MISSION_FAMILY_1, sTimelapse, TRUE, FALSE, DEFAULT, TRUE)
			AND DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_MICHAEL])	
			AND bSafeToContinueTimelapse = TRUE
				iTimeLapseCut = 101	
			ELSE
				//Create the stuff
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
				
			ENDIF
			
				
				//Make this happen the frame after
				IF bCutsceneRequested
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					OR TIMERA() > 15000	//Incase this blocks the script - 2017393
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())				
						SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_SHIRT_SHORTS_1)
						bSafeToContinueTimelapse = TRUE
					ENDIF
				ENDIF
			
		//	______>
		//	|
			
				IF NOT bCutsceneRequested
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_1_int", CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 )					
					DISPLAY_RADAR(FALSE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					bCutsceneRequested = TRUE
				ENDIF
								
				REQUEST_ANIM_DICT("missfam1leadinoutfamily_1_intp1_3")
				REQUEST_PLAYER_PED_MODEL(CHAR_MICHAEL)
		//	|	
		//	--------
				
				IF NOT DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				AND HAS_ANIM_DICT_LOADED("missfam1leadinoutfamily_1_intp1_3")
				AND HAS_PLAYER_PED_MODEL_LOADED(CHAR_MICHAEL)
					CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, << -780.647, 187.402, 72.90 >>, 96.2191)
					SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_SHIRT_SHORTS_1 , FALSE)
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-843.985779,158.654846,64.907990>>, <<-777.540039,183.186569,81.335342>>, 34.250000, <<-858.9615, 147.4493, 61.4344>>, 357.0481, <<5.0, 7.0, 8.5>>, TRUE, TRUE, TRUE, TRUE)
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA( << -825.1512, 178.9821, 70.3781 >>, 25.0)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-858.9615, 147.4493, 61.4344>>, 357.0481)
				
					
					//Temp glasses
					SET_PED_PROP_INDEX(pedSelector.pedID[SELECTOR_PED_MICHAEL], INT_TO_ENUM(PED_PROP_POSITION,1), 5, 0)
					
					FREEZE_ENTITY_POSITION(pedSelector.pedID[SELECTOR_PED_MICHAEL], TRUE)
					TASK_PLAY_ANIM_ADVANCED(pedSelector.pedID[SELECTOR_PED_MICHAEL], "missfam1leadinoutfamily_1_intp1_3", "base", << -780.647, 187.402, 72.90 >>, << 0.000, 0.000, 109.440 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
				
					oiTumbler = CREATE_OBJECT(P_TUMBLER_02_s1, GET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_MICHAEL]))
					oiCigar = CREATE_OBJECT(PROP_CIGAR_02, GET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_MICHAEL]))
					ATTACH_ENTITY_TO_ENTITY(oiTumbler, pedSelector.pedID[SELECTOR_PED_MICHAEL], GET_PED_BONE_INDEX(pedSelector.pedID[SELECTOR_PED_MICHAEL], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ATTACH_ENTITY_TO_ENTITY(oiCigar, pedSelector.pedID[SELECTOR_PED_MICHAEL], GET_PED_BONE_INDEX(pedSelector.pedID[SELECTOR_PED_MICHAEL], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					STOP_PED_SPEAKING(pedSelector.pedID[SELECTOR_PED_MICHAEL], TRUE)
					
				ENDIF
			
		BREAK
		
		CASE 101
			IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-790.2235, 181.3467, 71.8353>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),  320.3507 )
					RESET_GAME_CAMERA()
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
					iTimeLapseCut = 102
				ENDIF
			ENDIF
		BREAK
		
		CASE 102
			//WAit til Franklin triggers the mocap
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			
			IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					fDistanceToMichael = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_MICHAEL]))
				ENDIF
			ELSE
				fDistanceToMichael = 999.99
			ENDIF
				
			IF (fDistanceToMichael > 45.0)
			OR (fDistanceToMichael < 8.5)
			OR (GET_CLOCK_HOURS() >= 16)
				mission_stage = STAGE_INITIALISE
			ENDIF
			
		BREAK
		
	ENDSWITCH
		
ENDPROC

FUNC INT SETUP_SYNCED_SCENE(PED_INDEX firstPed, PED_INDEX secondPed, ENTITY_INDEX attachEntity, STRING strAttachBone, BOOL bLooped, STRING strAnimDict, STRING strAnimFirstPed, STRING strAnimSecondPed, BOOL bForceUpdate = TRUE, BOOL bInstantBlend = TRUE, SYNCED_SCENE_PLAYBACK_FLAGS ssFlags = SYNCED_SCENE_DONT_INTERRUPT)
	 
	
	INT thisSceneID = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	
	FLOAT fBlendIn
	FLOAT fBlendOut
	
	IF bInstantBlend
		fBlendIn = 	INSTANT_BLEND_IN
		fBlendOut = INSTANT_BLEND_OUT
	ELSE
		fBlendIn = 	SLOW_BLEND_IN
		fBlendOut = SLOW_BLEND_OUT
	ENDIF
	
	TASK_SYNCHRONIZED_SCENE(firstPed, thisSceneID, strAnimDict, strAnimFirstPed, fBlendIn, fBlendOut, ssFlags)
	IF secondPed <> NULL
		TASK_SYNCHRONIZED_SCENE(secondPed, thisSceneID, strAnimDict, strAnimSecondPed, fBlendIn, fBlendOut, ssFlags)	
	ENDIF
	IF NOT IS_ENTITY_DEAD(attachEntity)
		ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(thisSceneID, attachEntity, GET_ENTITY_BONE_INDEX_BY_NAME(attachEntity, strAttachBone))
	ELSE
		SCRIPT_ASSERT("SETUP_SYNCED_SCENE: attach entity dead!")
	ENDIF
	SET_SYNCHRONIZED_SCENE_LOOPED(thisSceneID, bLooped)
	
	IF bForceUpdate
		FORCE_PED_AI_AND_ANIMATION_UPDATE(firstPed)
		PRINTLN("FORCING UPDATE ON FIRST PED")
		IF secondPed <> NULL
			FORCE_PED_AI_AND_ANIMATION_UPDATE(secondPed)
			PRINTLN("FORCING UPDATE ON SECOND PED")
		ENDIF
	ENDIF
	
	PRINTLN("SETUP_SYNCED_SCENE: ", bLooped, " : ", strAnimFirstPed, " : ", strAnimSecondPed, " : ", bForceUpdate)
	
	RETURN thisSceneID

ENDFUNC


FUNC INT SETUP_SYNCED_SCENE_WITH_OFFSET(VECTOR vOffset, VECTOR vRotation, PED_INDEX firstPed, PED_INDEX secondPed, ENTITY_INDEX attachEntity, STRING strAttachBone, BOOL bLooped, STRING strAnimDict, STRING strAnimFirstPed, STRING strAnimSecondPed, BOOL bForceUpdate = TRUE)
	
	INT thisSceneID = CREATE_SYNCHRONIZED_SCENE(vOffset, vRotation)
	
	TASK_SYNCHRONIZED_SCENE(firstPed, thisSceneID, strAnimDict, strAnimFirstPed, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
	IF secondPed <> NULL
		TASK_SYNCHRONIZED_SCENE(secondPed, thisSceneID, strAnimDict, strAnimSecondPed, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)	
	ENDIF
	IF NOT IS_ENTITY_DEAD(attachEntity)
		ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(thisSceneID, attachEntity, GET_ENTITY_BONE_INDEX_BY_NAME(attachEntity, strAttachBone))
	ELSE
		SCRIPT_ASSERT("SETUP_SYNCED_SCENE: attach entity dead!")
	ENDIF
	SET_SYNCHRONIZED_SCENE_LOOPED(thisSceneID, bLooped)
	
	IF bForceUpdate
		FORCE_PED_AI_AND_ANIMATION_UPDATE(firstPed)
		IF secondPed <> NULL
			FORCE_PED_AI_AND_ANIMATION_UPDATE(secondPed)
		ENDIF
	ENDIF
	
	PRINTLN("SETUP_SYNCED_SCENE: ", bLooped, " : ", strAnimFirstPed, " : ", strAnimSecondPed, " : ", bForceUpdate)
	
	RETURN thisSceneID

ENDFUNC

FUNC INT GET_REPLAY_STAGE_FROM_MISSION_STAGE(MISSION_STAGE_FLAG thisStage)
	
	INT iReplayNumber
	
	iReplayNumber = ENUM_TO_INT(thisStage) - ENUM_TO_INT(STAGE_GET_INTO_AMANDAS_CAR)
	
	IF iReplayNumber < 0
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("FAm1 - trying to set replay to a negative value")
		#ENDIF
		iReplayNumber = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("****** --- GET_REPLAY_STAGE_FROM_MISSION_STAGE: ", iReplayNumber)
	#ENDIF
	
	RETURN iReplayNumber

ENDFUNC


INT iFirstTrailerToProcessTraffic = 93
INT iFirstTrailerToProcessSetPiece = 6

PROC MANAGE_TRAILER_CREATION()

	IF NOT IS_ENTITY_DEAD(truckYachtPacker)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPacker)

			INT i
			FOR i = iFirstTrailerToProcessTraffic TO TOTAL_NUMBER_OF_TRAFFIC_CARS - 1
				IF DOES_ENTITY_EXIST(TrafficCarID[i])
				AND IS_ENTITY_A_MISSION_ENTITY(TrafficCarID[i])
				AND i <> 99
					IF NOT IS_ENTITY_DEAD(TrafficCarID[i])
						IF IS_ENTITY_A_VEHICLE(TrafficCarID[i])
							IF GET_ENTITY_MODEL(TrafficCarID[i]) = PHANTOM
							OR GET_ENTITY_MODEL(TrafficCarID[i]) = PACKER		
							
								REQUEST_MODEL(TRAILERS2)
							
								//IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TrafficCarID[i])
								IF HAS_MODEL_LOADED(TRAILERS2)
									IF NOT DOES_ENTITY_EXIST(TrafficCarTrailer[i])
										IF GET_RANDOM_BOOL()
											TrafficCarTrailer[i] = CREATE_VEHICLE(TRAILERS2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TrafficCarID[i], <<0.0, 3.0, 10.0>>), GET_ENTITY_HEADING(TrafficCarID[i]))
										ELSE
											TrafficCarTrailer[i] = CREATE_VEHICLE(TRAILERS2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TrafficCarID[i], <<0.0, 3.0, 10.0>>), GET_ENTITY_HEADING(TrafficCarID[i]))
										ENDIF
										
										REQUEST_COLLISION_AT_COORD(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TrafficCarID[i], <<0.0, 3.0, 10.0>>))
										
										SET_DISABLE_SUPERDUMMY(TrafficCarID[i], TRUE)
										
										SET_VEHICLE_ON_GROUND_PROPERLY(TrafficCarTrailer[i])
										ATTACH_VEHICLE_TO_TRAILER(TrafficCarID[i], TrafficCarTrailer[i])
										SET_ENTITY_DYNAMIC(TrafficCarTrailer[i], TRUE)
										//SET_VEHICLE_LIVERY(TrafficCarID[i], (i%4))
										//START_RECORDING_VEHICLE(TrafficCarTrailer[i], i, "Fam1TrT", TRUE)
										//START_PLAYBACK_RECORDED_VEHICLE(TrafficCarTrailer[i], i, "Fam1TrT")
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
		//				IF DOES_ENTITY_EXIST(TrafficCarTrailer[i])
		//					IF NOT IS_ENTITY_DEAD(TrafficCarTrailer[i])
		//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TrafficCarTrailer[i])
		//							SET_PLAYBACK_SPEED(TrafficCarTrailer[i], mainPlaybackSpeed)
		//						ENDIF
		//					ENDIF
		//				ENDIF
					ENDIF
				ELSE
					//STOP_RECORDING_VEHICLE(TrafficCarTrailer[i])
					IF DOES_ENTITY_EXIST(TrafficCarTrailer[i])
						//IF NOT IS_ENTITY_DEAD(TrafficCarTrailer[i])
							//STOP_PLAYBACK_RECORDED_VEHICLE(TrafficCarTrailer[i])
							iFirstTrailerToProcessTraffic = i
							SET_VEHICLE_AS_NO_LONGER_NEEDED(TrafficCarTrailer[i])
						//ENDIF
					ENDIF
					
					
					
				ENDIF
			ENDFOR
			
			FOR i = iFirstTrailerToProcessSetPiece TO TOTAL_NUMBER_OF_SET_PIECE_CARS - 1
				IF DOES_ENTITY_EXIST(SetPieceCarID[i])
				AND IS_ENTITY_A_MISSION_ENTITY(SetPieceCarID[i])
					IF NOT IS_ENTITY_DEAD(SetPieceCarID[i])
						IF IS_ENTITY_A_VEHICLE(SetPieceCarID[i])
						
							IF GET_ENTITY_MODEL(SetPieceCarID[i]) = PHANTOM
							OR GET_ENTITY_MODEL(SetPieceCarID[i]) = PACKER	
								//IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[i])
								
								REQUEST_MODEL(TRAILERS2)
								
								IF HAS_MODEL_LOADED(TRAILERS2)
									IF NOT DOES_ENTITY_EXIST(SetPieceCarTrailer[i])
										IF GET_RANDOM_BOOL()
											SetPieceCarTrailer[i] = CREATE_VEHICLE(TRAILERS2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SetPieceCarID[i], <<0.0, 3.0, 10.0>>), GET_ENTITY_HEADING(SetPieceCarID[i]))
										ELSE
											SetPieceCarTrailer[i] = CREATE_VEHICLE(TRAILERS2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SetPieceCarID[i], <<0.0, 3.0, 10.0>>), GET_ENTITY_HEADING(SetPieceCarID[i]))
										ENDIF
										
										REQUEST_COLLISION_AT_COORD(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SetPieceCarID[i], <<0.0, 3.0, 10.0>>))
																				
										SET_VEHICLE_ON_GROUND_PROPERLY(SetPieceCarTrailer[i])
										
										SET_DISABLE_SUPERDUMMY(SetPieceCarID[i], TRUE)
										
										ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[i], SetPieceCarTrailer[i])
										SET_ENTITY_DYNAMIC(SetPieceCarTrailer[i], TRUE)
										//START_RECORDING_VEHICLE(SetPieceCarTrailer[i], i, "Fam1SpT", TRUE)
										//START_PLAYBACK_RECORDED_VEHICLE(SetPieceCarTrailer[i], i, "Fam1SpT")
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
		//				IF DOES_ENTITY_EXIST(SetPieceCarTrailer[i])
		//					IF NOT IS_ENTITY_DEAD(SetPieceCarTrailer[i])
		//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarTrailer[i])
		//							SET_PLAYBACK_SPEED(SetPieceCarTrailer[i], mainPlaybackSpeed)
		//						ENDIF
		//					ENDIF
		//				ENDIF
						
					ENDIF
				ELSE
					//STOP_RECORDING_VEHICLE(SetPieceCarTrailer[i])
					IF DOES_ENTITY_EXIST(SetPieceCarTrailer[i])
						//IF NOT IS_ENTITY_DEAD(SetPieceCarTrailer[i])
							//STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarTrailer[i])
							iFirstTrailerToProcessSetPiece = i
							SET_VEHICLE_AS_NO_LONGER_NEEDED(SetPieceCarTrailer[i])
						//ENDIF
					ENDIF
										
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
ENDPROC


ENUM eBoomBaddieStage

	BOOMBADDIE_CREATE_AND_ANIMATE_INTRO,			//0
	BOOMBADDIE_WAIT_FOR_INTRO_TO_FINISH,			//1
	BOOMBADDIE_WAIT_FOR_BOOM_CLIMBING_TO_FINISH,	//2
	BOOMBADDIE_START_PUNCHING_JIMMY_OFF_BOOM,		//3
	BOOMBADDIE_JIMMY_GOES_TO_ONE_HANDED_HANG,		//4
	BOOMBADDIE_JIMMY_DIES,							//5
	BOOMBADDIE_BAD_GUY_SHOT,						//6
	BOOMBADDIE_BAD_GUY_SHOT_KILL_OFF_ENTITY,		//7
	BOOMBADDIE_STOP_PROCESSING,						//8
	BOOMBADDIE_FORCE_BAD_GUY_TO_FALL_OFF			//9
	
ENDENUM

eBoomBaddieStage eBoomBaddie = BOOMBADDIE_CREATE_AND_ANIMATE_INTRO

PROC CREATE_JIMMY()

	pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<723.9294, -1087.4998, 21.1693>>, 96.2191)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedJimmy,	"FAMILY_1_BOAT_PEDS")
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
	
	SET_ENTITY_HEALTH(pedJimmy, 1000)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJimmy, TRUE)
	SET_PED_SUFFERS_CRITICAL_HITS(pedJimmy, FALSE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedJimmy, FALSE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedJimmy, RELGROUPHASH_PLAYER)
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")

ENDPROC

PROC SHUNT_RESET_FLAGS()

	IF NOT IS_ENTITY_DEAD(pedJimmy)
		SET_PED_RESET_FLAG(pedJimmy,PRF_PreventGoingIntoShuntInVehicleState, TRUE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(pedFranklin())
		SET_PED_RESET_FLAG(pedFranklin(),PRF_PreventGoingIntoShuntInVehicleState, TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_PreventGoingIntoShuntInVehicleState, TRUE)
		ENDIF
	ENDIF
			
ENDPROC

PROC REPLAY_OR_Z_SKIP_REPOSITION(BOOL bIsReplay, VECTOR vPos, FLOAT fHeading)

	IF bIsReplay
		START_REPLAY_SETUP(vPos, fHeading)		
	ELSE
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
	ENDIF

ENDPROC

BOOL bHaveAllStreamingRequestsCompleted
//VECTOR vSkipRotation

PROC MANAGE_SKIP(MISSION_STAGE_FLAG &stage_to_skip_to, BOOL bIsReplay = FALSE)

	FLOAT fJUmpTime
	
	bIsReplayInProgress = bIsReplay
	
	PRINTLN("*****************************")
	PRINTLN("**** Fam1 - MANAGE SKIP TO : ", ENUM_TO_INT(stage_to_skip_to), " *")
	PRINTLN("*****************************")

	SWITCH stage_to_skip_to
		
		CASE STAGE_GET_INTO_AMANDAS_CAR
				
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-825.1714, 178.9298, 70.3721>>, 145.3533 )
		
			RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
			RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)
			iGetIntoCarStage = 0
			
			#IF IS_DEBUG_BUILD
				set_uber_parent_widget_group(family1WidgetGroup)
			#ENDIF
			
			CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
			IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
				SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
			ENDIF
			
			mission_stage = STAGE_GET_INTO_AMANDAS_CAR
			CLEAR_PED_TASKS_IMMEDIATELY(player_ped_id())
			SET_ENTITY_ROTATION(player_ped_id(), <<0.0, 0.0, 0.0>>)
			CLEAR_AREA(<<-825.1714, 178.9298, 70.3721>>, 100.00, TRUE)
			
			//LOAD_SCENE(<< -821.9089, 178.7543, 70.5698 >>)
			
			iGetIntoCarStage = 5
			RESET_GAME_CAMERA(0.0)
			
			END_REPLAY_SETUP(carPlayersJeep)
			
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				bHaveAllStreamingRequestsCompleted = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFranklin())
			ENDIF
			
			WHILE NOT bHaveAllStreamingRequestsCompleted
				IF NOT IS_ENTITY_DEAD(pedFranklin())
					bHaveAllStreamingRequestsCompleted = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFranklin())
				ENDIF
				WAIT(0)
			ENDWHILE
			
			IF NOT bIsReplay
			
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), carPlayersJeep,DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED | ECF_WARP_ENTRY_POINT)
				ENDIF
							
				WHILE NOT IS_FOLLOW_VEHICLE_CAM_ACTIVE()
					WAIT(0)
				ENDWHILE
			
			ENDIF
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
			FADE_IN_IF_NEEDED()
		BREAK
		
		CASE STAGE_GET_TO_LOCATION
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, << -821.9089, 178.7543, 70.5698 >>, 78.660)
					
			RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
			RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)
						
			#IF IS_DEBUG_BUILD
				set_uber_parent_widget_group(family1WidgetGroup)
			#ENDIF
			
			CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
			IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
				SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				bHaveAllStreamingRequestsCompleted = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFranklin())
			ENDIF
			
			WHILE NOT bHaveAllStreamingRequestsCompleted
				IF NOT IS_ENTITY_DEAD(pedFranklin())
					bHaveAllStreamingRequestsCompleted = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFranklin())
				ENDIF
				WAIT(0)
			ENDWHILE
			
			CLEAR_AREA(<< -821.9089, 178.7543, 70.5698 >>, 100.00, TRUE)
						
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
			ENDIF
			//LOAD_SCENE(<< -821.9089, 178.7543, 70.5698 >>)
			
			WHILE NOT IS_FOLLOW_VEHICLE_CAM_ACTIVE()
				WAIT(0)
			ENDWHILE
			
			RESET_GAME_CAMERA(0.0)
			
			END_REPLAY_SETUP()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
			FADE_IN_IF_NEEDED()
		
		BREAK
		
		CASE STAGE_DO_CHASE
							
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-2146.3821, -255.5757, 13.9293>>, 159.6675)
			
			IF NOT IS_PED_INJURED(pedFranklin())
			AND NOT IS_ENTITY_DEAD(carPlayersJeep)
				CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
				IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
					SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedFranklin())
				ENDIF
				SET_VEHICLE_ENGINE_ON(carPlayersJeep, TRUE, TRUE)
				LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
			ENDIF
			SHUNT_RESET_FLAGS()
			RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
			RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)
			CLEAR_AREA(<<-2146.3821, -255.5757, 13.9293>>, 20.0, TRUE)
					
			SHUNT_RESET_FLAGS()
			CLEAR_GPS_MULTI_ROUTE()
			bCustomGPSActive = TRUE
			iGetToLocation = 3
			
			TRIGGER_MUSIC_EVENT("FAM1_FADE_RADIO")
					
			REQUEST_CHASE_INITIAL_ASSETS()
			REQUEST_CHASE_ASSETS()
				
			WHILE NOT  HAVE_CHASE_ASSETS_LOADED()	//If player is in an area that can see the car....
				SHUNT_RESET_FLAGS()
				WAIT(0)
			ENDWHILE
			
			setupTrafficFamily1()
			CREATE_CAMERAS_AND_YACHT()
			cutsceneSetYachtAbsolute(14000)
	
			UNPAUSE_UBER_CARS()
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)		
			KILL_FACE_TO_FACE_CONVERSATION()

			eChaseStage = CHASE_SETUP
			mission_stage = STAGE_DO_CHASE// STAGE_GET_TO_LOCATION //STAGE_DO_CHASE

			RESET_GAME_CAMERA()
			
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				//IF NOT bIsReplay
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, VS_DRIVER)
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<-2146.3821, -255.5757, 13.9293>>)//<<-2147.1565, -257.1366, 13.7908>>)
					SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
					SET_ENTITY_HEADING(carPlayersJeep, 159.6675)
				//ENDIF
				SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
				SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 15.0)
			ENDIF
			
			END_REPLAY_SETUP(carPlayersJeep)
			IF bIsReplay
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 15.0)
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(truckYachtPacker)				
				SET_ENTITY_VISIBLE(truckYachtPacker, TRUE)				
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				SET_ENTITY_VISIBLE(truckYachtPackerTrailer, TRUE)
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
			PRELOAD_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_START", CONV_PRIORITY_VERY_HIGH)
			
			FADE_IN_IF_NEEDED()
		BREAK
	

		CASE STAGE_JIMMY_APPEARS
		CASE STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
			
						
			IF stage_to_skip_to = STAGE_JIMMY_APPEARS
				REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-416.0598, -1124.7811, 36.1469>>, 179.8945)
			ELIF stage_to_skip_to = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
				REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-650.3306, -2171.4126, 49.4297>>, 235.8943)
			ENDIF
				
			IF NOT IS_PED_INJURED(pedFranklin())
				SET_PED_CAN_PLAY_GESTURE_ANIMS(pedFranklin(), FALSE)
			ENDIF
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING) 
			TRIGGER_MUSIC_EVENT("FAM1_DO_CHASE_RT")
		
			iClosest = 0
			iClosestPlayer = 0
		
			bPlayInitialDialogue = TRUE
		
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 150, TRUE)
			mainPlaybackSpeed = 1.0
		
			REQUEST_CHASE_ASSETS()
			REQUEST_CHASE_INITIAL_ASSETS()
			REQUEST_MODEL(IG_JIMMYDISANTO)
			REQUEST_ANIM_DICT("MISSFAM1_YachtBattleonYacht02_")
			REQUEST_ANIM_DICT("MISSFAM1_YachtBattleincar02_")
			REQUEST_PTFX_ASSET()
			REQUEST_MODEL(TRAILERS2)
			REQUEST_ANIM_DICT("MISSFAM1_YachtBattle")
			
			WHILE NOT HAVE_CHASE_ASSETS_LOADED()
			OR NOT HAVE_CHASE_INITIAL_ASSETS_LOADED()
			OR NOT HAS_MODEL_LOADED(IG_JIMMYDISANTO)
			OR NOT HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleonYacht02_")
			OR NOT HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleincar02_")
			OR NOT HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattle")
			OR NOT HAS_PTFX_ASSET_LOADED()
			OR NOT HAS_MODEL_LOADED(TRAILERS2)
				WAIT(0)
			ENDWHILE
		
			REMOVE_ANIM_DICT("MISSFAM1_YachtBattleincar01_")
		
			CREATE_CAMERAS_AND_YACHT()
			
			IF NOT IS_PED_INJURED(pedFranklin())
			AND NOT IS_ENTITY_DEAD(carPlayersJeep)
				CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
				SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
				//SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vMissionLocation)
				//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, VS_DRIVER)
				LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
				SET_VEHICLE_ENGINE_ON(carPlayersJeep, TRUE, TRUE)
			ENDIF
			
//				IF stage_to_skip_to = STAGE_JIMMY_APPEARS
//					SET_ENTITY_COORDS(carPlayersJeep, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 90000, "Fam1Cine"))
//					SET_ENTITY_ROTATION(carPlayersJeep, GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(1, 90000, "Fam1Cine"))
//				ELIF stage_to_skip_to = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
//					SET_ENTITY_COORDS(carPlayersJeep, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 127000, "Fam1Cine"))
//					SET_ENTITY_ROTATION(carPlayersJeep, GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(1, 127000, "Fam1Cine"))
//				ENDIF
		
			setupTrafficFamily1()
			SET_ENTITY_COORDS(truckYachtPackerTrailer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, 3.0, 0.0>>))
			ADD_BLIP_FOR_YACHT()
			//WAIT(100)
			//RESET_GAME_CAMERA()
			
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			
			cutsceneSetYachtAbsolute()
			
			UNPAUSE_UBER_CARS()
			
			IF stage_to_skip_to = STAGE_JIMMY_APPEARS

				//fJUmpTime = 83500
				
				IF g_bTrailer2Active 
					fJUmpTime = 70000 //For Trailer 2
					//bDontCleanUpUberCarsForTrailer = TRUE
					
					bCarsAreOn  = FALSE
					SWITCH_ALL_RANDOM_CARS_ON()
					SWITCH_ALL_ROADS_BACK_TO_ORIGINAL()
					//bIsTrailerRunning = TRUE
					iTimeOfBadGuy2Death = GET_GAME_TIMER()
					iDelayAfterBadGuysDeath =  10700 //1200
				ELSE
					
					fJUmpTime = 90000// 85900
					iDelayAfterBadGuysDeath =  1200
				ENDIF
						
			
				TRIGGER_MUSIC_EVENT("FAM1_JIMMY_APPEARS_RT")
				//Skip about 45 seconds in...
				//Set_Replay_Mid_Mission_Stage(ENUM_TO_INT(STAGE_JIMMY_APPEARS)) 
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_JIMMY_APPEARS")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, fJUmpTime  )
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, fJUmpTime)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCar, fJUmpTime  )
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera, fJUmpTime)
				
				ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.2979, -8.6807, 2.8214>>)
				POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.5470, -5.9555, 2.7092>>)
				SET_CAM_FOV(cutsceneCamera, 28.0593)
				
				SET_UBER_PLAYBACK_TO_TIME_NOW(truckYachtPacker, fJUmpTime + 13500)
				
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					START_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 1, "Fam1Cine")	
					fJUmpTime = fJUmpTime + 12000
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, fJUmpTime )			
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(carPlayersJeep, TRUE)
				ENDIF
				
				iRageText = -1
				
				iSmashDialogues = 9 //Stops all the previous dialogue playing
			ELIF stage_to_skip_to = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
				bJimmyLanded = TRUE
				TRIGGER_MUSIC_EVENT("FAM1_FRANK_JUMPS_RT")
				//May need synced version.			
				PLAY_ENTITY_ANIM(truckYachtPackerTrailer,"onYacht_hitByBoom_boom", "MISSFAM1_YachtBattleonYacht02_", 8.0, FALSE, TRUE)	
					
				//Set_Replay_Mid_Mission_Stage(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_JIMMY_APPEARS)) 
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 134000  )
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, 134000)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCar, 134000  )
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera, 134000)
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					START_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 1, "Fam1Cine")	
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 145700  )	//+ absolute jump time!!!						
				ENDIF		
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(carPlayersJeep, TRUE)
				CREATE_JIMMY()
				
				iSmashDialogues = 9 //Stops all the previous dialogue playing
				eBoomBaddie = BOOMBADDIE_STOP_PROCESSING	//Dont spawn the boom baddie
				
				sceneIdSon = SETUP_SYNCED_SCENE(pedJimmy, NULL, carPlayersJeep, "seat_pside_r", FALSE, "MISSFAM1_YachtBattleinCar02_", "JimmyInCar_intro", "", TRUE)			
				
				SET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon, 0.45)
				
				//iTimeOfLastSegmentGrab = GET_GAME_TIMER()
				SET_UBER_PLAYBACK_TO_TIME_NOW(truckYachtPacker, 134000 + 13500)
			ENDIF
			
			KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()
			CLEAR_HELP()

			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				STOP_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep)
			ENDIF
			REMOVE_BLIP(blipPlayersCar)
			
			IF stage_to_skip_to = STAGE_JIMMY_APPEARS
				
				REQUEST_ANIM_DICT("MISSFAM1_YachtBattleonYacht01_")
				
				WHILE NOT HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleonYacht01_")
					WAIT(0)
				ENDWHILE
				
				IF NOT IS_PED_INJURED(pedFranklin())
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_byWheelLoop02_F", "", FALSE)
				ENDIF
				
				eChaseStage = CHASE_MEET_JIMMY
				mission_stage = STAGE_JIMMY_APPEARS //STAGE_DO_CHASE
			ELIF stage_to_skip_to = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
				
				IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				AND NOT IS_PED_INJURED(pedFranklin())				
					sceneIdA = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					TASK_SYNCHRONIZED_SCENE (pedFranklin(), sceneIDA, "MISSFAM1_YACHTBATTLEonYacht02_", "onYacht_jumpToCarIntro_F", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)							
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIdA, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "chassis"))
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdA, FALSE)
					SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIdA,  <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.0528, -4.5224, 2.7052>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<0.1428, -7.4610, 2.1340>>)
					SET_CAM_FOV(cutsceneCamera, 27.3552)
					
					KILL_ANY_CONVERSATION()
					
					eChaseStage = CHASE_CREATE_SPEECH_FOR_FRANKLIN
				ENDIF
				
				mission_stage = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
			ENDIF
					
			IF stage_to_skip_to = STAGE_JIMMY_APPEARS
				eChaseStage = CHASE_MEET_JIMMY
				mission_stage = STAGE_JIMMY_APPEARS //STAGE_DO_CHASE
				
				START_AUDIO_SCENE("FAM1_HIGHWAY_CHASE")
				
			ELIF stage_to_skip_to = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT 
				eChaseStage = CHASE_CREATE_SPEECH_FOR_FRANKLIN
				stage_to_skip_to = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
				
				START_AUDIO_SCENE("FAMILY_1_PROTECT_FRANKLIN")
				START_AUDIO_SCENE("FAM1_HIGHWAY_CHASE")
				
			ENDIF
													
//			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 25.0)
//				SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
//				SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 20.0)
//			ENDIF
			
			END_REPLAY_SETUP(carPlayersJeep)
			
//			IF bIsReplay
//				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//					SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 20.0)
//				ENDIF
//			ENDIF
			
			SETTIMERA(0)
			WHILE TIMERA() < 1000
				CREATE_ALL_WAITING_UBER_CARS()
				UPDATE_UBER_PLAYBACK(truckYachtPacker, 1.0)
				
				MANAGE_TRAILER_CREATION()
				WAIT(0)
			ENDWHILE
			
			BUILD_UBER_ROUTE_DEBUG_PATH(route_path, 1, "Fam1New")
			
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
				SET_VEHICLE_AUDIO_ENGINE_DAMAGE_FACTOR(carPlayersJeep, 0.25)
				
				#IF IS_NEXTGEN_BUILD
					//overheat
					ptfxCarSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_fam1_veh_smoke", carPlayersJeep, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, GET_ENTITY_BONE_INDEX_BY_NAME(carPlayersJeep, "overheat"))
				#ENDIF
				
				#IF NOT IS_NEXTGEN_BUILD
					ptfxCarSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam1_veh_smoke", carPlayersJeep, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				#ENDIF
				
				
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 0.5)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedFranklin())
				SET_CURRENT_PED_WEAPON(pedFranklin(), WEAPONTYPE_UNARMED, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedFranklin())		
				PRINTSTRING("forced FORCE_PED_AI_AND_ANIMATION_UPDATE")
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(truckYachtPacker)	
				SET_ENTITY_VISIBLE(truckYachtPacker, TRUE)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				SET_ENTITY_VISIBLE(truckYachtPackerTrailer, TRUE)
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
		BREAK
		
		CASE STAGE_TAKE_CAR_TO_CHOP_SHOP
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<3.5141, -2605.7639, 26.7756>>, 61.8483)
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_TAKE_CAR_TO_CHOP_SHOP")
				
			REQUEST_MODEL(IG_JIMMYDISANTO)
			REQUEST_PTFX_ASSET()
			REQUEST_CLIP_SET("clipset@missfam1_jimmy_sit_rear")
			
			WHILE NOT HAS_MODEL_LOADED(IG_JIMMYDISANTO)
			OR NOT HAS_PTFX_ASSET_LOADED()
			OR NOT HAS_CLIP_SET_LOADED("clipset@missfam1_jimmy_sit_rear")
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				IF NOT IS_PED_INJURED(pedFranklin())
					CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
					SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedFranklin())
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vMissionLocation)
				ENDIF
				
				SET_VEHICLE_ENGINE_HEALTH(carPlayersJeep, 70.0)
				
				
				IF NOT DOES_ENTITY_EXIST(pedJimmy)
					pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<3.5141, -2605.7639, 32.7756>>, 96.2191)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedJimmy, "FAMILY_1_BOAT_PEDS")
					SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedJimmy, RELGROUPHASH_PLAYER)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJimmy, TRUE)
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")
				ENDIF
				
				IF NOT IS_PED_INJURED(pedJimmy)
					CLEAR_PED_TASKS_IMMEDIATELY(pedJimmy)
					SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_BACK_LEFT)
					SET_PED_IN_VEHICLE_CONTEXT(pedJimmy, GET_HASH_KEY("MISS_FAMILY1_JIMMY_SIT_REAR"))//MISS_FAMILY1_JIMMY_SIT_REAR"))
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy, TRUE)	
				ENDIF
				
				LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
				
				SET_VEHICLE_ENGINE_ON(carPlayersJeep, TRUE, TRUE)
				
				//SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
			ENDIF
			CLEAR_AREA_OF_VEHICLES(<<3.5141, -2605.7639, 26.7756>>, 25.0)
			SET_ENTITY_COORDS(carPlayersJeep, <<3.5141, -2605.7639, 26.7756>>)
			SET_ENTITY_HEADING(carPlayersJeep, 61.8483)
			
			SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 15.0)
			
			//LOAD_SCENE(<<-968.1192, -1828.3011, 18.7805>>)
			
			iTakeCarToChopShopStage = 3
			
			//WAIT(0)
		
			END_REPLAY_SETUP(carPlayersJeep)
			
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, VS_DRIVER)
			ENDIF
			START_AUDIO_SCENE("FAMILY_1_CAR_BREAKS_DOWN")
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")		
			mission_stage = STAGE_TAKE_CAR_TO_CHOP_SHOP
		BREAK

		CASE STAGE_CHOP_SHOP_CUTSCENE
			
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-1104.3104, -1975.6715, 12.0488>>, 92.2029)
					
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "STAGE_CHOP_SHOP_CUTSCENE")
		
			REQUEST_MODEL(IG_JIMMYDISANTO)
			
			WHILE NOT HAS_MODEL_LOADED(IG_JIMMYDISANTO)
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				IF NOT IS_PED_INJURED(pedFranklin())
					CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
					SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedFranklin())
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vMissionLocation)
				ENDIF
				
				SET_VEHICLE_ENGINE_HEALTH(carPlayersJeep, 70.0)
				
				
				IF NOT DOES_ENTITY_EXIST(pedJimmy)
					pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<-1104.3104, -1975.6715, 15.0488>>, 96.2191)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedJimmy, "FAMILY_1_BOAT_PEDS")
					SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedJimmy, RELGROUPHASH_PLAYER)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJimmy, TRUE)
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")
				ENDIF
				
				IF NOT IS_PED_INJURED(pedJimmy)
					CLEAR_PED_TASKS_IMMEDIATELY(pedJimmy)
					SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_BACK_LEFT)
				ENDIF
				
				LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
				
				SET_VEHICLE_ENGINE_ON(carPlayersJeep, TRUE, TRUE)
				
				//SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
			ENDIF
			
			
			SET_ENTITY_COORDS(carPlayersJeep, <<-1104.3104, -1975.6715, 12.0488>>)
			SET_ENTITY_HEADING(carPlayersJeep, 92.2029)
		
			SET_SHOP_HAS_RUN_ENTRY_INTRO(CARMOD_SHOP_01_AP, FALSE)
			SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_01_AP, TRUE)
			FORCE_SHOP_RESET(CARMOD_SHOP_01_AP)
			
			END_REPLAY_SETUP()
		
			mission_stage = STAGE_CHOP_SHOP_CUTSCENE //STAGE_CHOP_SHOP_CUTSCENE
		BREAK
		
		CASE STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay,  <<-1137.9795, -1985.8516, 12.1666>>, 294.9323)
		
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME")
			
			SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_01_AP, TRUE)
			FORCE_SHOP_RESET(CARMOD_SHOP_01_AP)
		
			SET_SHOP_HAS_RUN_ENTRY_INTRO(CARMOD_SHOP_01_AP, TRUE)
		
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) <> SELECTOR_PED_FRANKLIN
		
				MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_FRANKLIN)
				TAKE_CONTROL_OF_SELECTOR_PED(pedSelector, FALSE)
			
			ENDIF

			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)

			DELETE_PED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				
			REQUEST_MODEL(IG_JIMMYDISANTO)
			REQUEST_CLIP_SET("clipset@missfam1_jimmy_sit")
			
			WHILE NOT HAS_MODEL_LOADED(IG_JIMMYDISANTO)
			OR NOT HAS_CLIP_SET_LOADED("clipset@missfam1_jimmy_sit")
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, VS_DRIVER)
				ENDIF
								
				IF NOT DOES_ENTITY_EXIST(pedJimmy)
					pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<-1137.9795, -1985.8516, 14.1666>>, 96.2191)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedJimmy, "FAMILY_1_BOAT_PEDS")
					SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedJimmy, RELGROUPHASH_PLAYER)
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")
				ENDIF
				
				IF NOT IS_PED_INJURED(pedJimmy)
					CLEAR_PED_TASKS_IMMEDIATELY(pedJimmy)
					SET_PED_IN_VEHICLE_CONTEXT(pedJimmy, GET_HASH_KEY("MISS_FAMILY1_JIMMY_SIT"))
					SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_FRONT_RIGHT)
				ENDIF
				
				LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
				
				SET_VEHICLE_ENGINE_ON(carPlayersJeep, TRUE, TRUE)
				
			ENDIF
					
			SET_ENTITY_COORDS(carPlayersJeep, <<-1137.9795, -1985.8516, 12.1666>>)
			SET_ENTITY_HEADING(carPlayersJeep, 294.9323)
					
			//LOAD_SCENE(<<-968.1192, -1828.3011, 18.7805>>)
			
			iTakeJImmyHomeStage = 5
			mission_stage = STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, VS_DRIVER)
			END_REPLAY_SETUP(carPlayersJeep)
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
				
			FADE_IN_IF_NEEDED()
			
		
		BREAK
		
		CASE STAGE_DROP_OFF_CUTSCENE
			
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay,  <<-834.7897, 165.5490, 67.7670>>, 326.1774)
					
			REMOVE_CUTSCENE()
			
			WAIT(0)
			
			MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_FRANKLIN)
			TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
			DELETE_PED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
			REQUEST_MODEL(IG_JIMMYDISANTO)
			
			WHILE NOT HAS_MODEL_LOADED(IG_JIMMYDISANTO)
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, VS_DRIVER)
				ENDIF
				
				SET_VEHICLE_ENGINE_HEALTH(carPlayersJeep, 70.0)
				
				SET_ENTITY_COORDS(carPlayersJeep, <<-834.7897, 165.5490, 67.7670>>)
				SET_ENTITY_HEADING(carPlayersJeep, 326.1774)
				SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
				
				
				IF NOT DOES_ENTITY_EXIST(pedJimmy)
					pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<723.9294, -1087.4998, 21.1693>>, 96.2191)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedJimmy, "FAMILY_1_BOAT_PEDS")
					SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedJimmy, RELGROUPHASH_PLAYER)
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")
				ENDIF
				
				IF NOT IS_PED_INJURED(pedJimmy)
					CLEAR_PED_TASKS_IMMEDIATELY(pedJimmy)
					//SET_PED_IN_VEHICLE_CONTEXT(pedJimmy, GET_HASH_KEY("MISS_FAMILY1_JIMMY_SIT"))
					SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_FRONT_RIGHT)
				ENDIF
				
				LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
			ENDIF
			
			mission_stage = STAGE_DROP_OFF_CUTSCENE
			
			END_REPLAY_SETUP()
			
			//FADE_IN_IF_NEEDED()
			
		BREAK

		CASE STAGE_FAIL_CUTSCENE
					
			LOAD_SCENE(<<-655.768860,-563.664795,34.357941>>)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
						
		BREAK
	
	ENDSWITCH

ENDPROC


MISSION_STAGE_FLAG skip_mission_stage	//For debug and replays.
INT myStage

PROC initialiseMission()

//	#IF IS_DEBUG_BUILD
//		g_b_CellDialDebugTextToggle = FALSE
//	#ENDIF

	#IF IS_NEXTGEN_BUILD
	
	fPlaybackCarStreamingDistance = 180.0
	fUberMinTimeBeforePlaybackStartToCreate = 13000.0//(7000.0 is the default)
	fUberPlaybackMinCreationDistance  = 180.0//(100.0 is the default)

	#ENDIF


	ALLOW_SONAR_BLIPS(FALSE)

	SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)

	SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_01_AP, FALSE)

	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)

	SET_AUDIO_FLAG("ScriptedConvListenerMaySpeak", TRUE) 

	REQUEST_ADDITIONAL_TEXT("FAMILY1", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("FAM1AUD", MISSION_DIALOGUE_TEXT_SLOT)
		
	REMOVE_RELATIONSHIP_GROUP(thievesRelGroup)
	
	ADD_RELATIONSHIP_GROUP("THIEVES", thievesRelGroup)
		
	SET_PED_NON_CREATION_AREA(<< -822.4180, 177.4556, 70.3269 >>, << -822.4180, 177.4556, 70.3269 >>)
	SET_PED_PATHS_IN_AREA(<< -832.4180, 167.4556, 60.3269 >>, << -812.4180, 187.4556, 80.3269 >>, FALSE)
	SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<< -820.7206, 178.5732, 70.5724 >>, 30.0, 0)
	
	//SET_ROADS_IN_ANGLED_AREA(<<-2251.529297,-338.419952,10.961194>>, <<-2102.537109,-369.083832,19.683681>>, 74.500000, FALSE, FALSE)
	//SET_ROADS_IN_ANGLED_AREA(<<-2194.891113,-363.800781,10.876474>>, <<-1974.205444,-159.776367,48.984055>>, 127.750000, FALSE, FALSE)

	SET_ROADS_IN_ANGLED_AREA(<<-2181.964111,-332.705200,11.149142>>, <<-2112.790527,-175.243469,44.184326>>, 17.000000, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<-2041.399902,-157.060654,11.016596>>, <<-2139.924072,-233.372955,34.054882>>, 17.000000, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<-1921.896729,-166.517349,20.289425>>, <<-2062.191162,-168.297379,42.182777>>, 23.250000, FALSE, FALSE)

	//SET_ROADS_IN_ANGLED_AREA(<<-2194.891113,-363.800781,10.876474>>, <<-2066.113770,-185.062683,41.433807>>, 127.750000, FALSE, FALSE)

	DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
	
	bHasChanged= GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)  //ensure bChange is set to the state of “player has changed outfit” at the start of the mission
	
	DISABLE_TAXI_HAILING(TRUE)
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS("YB_JIMTXT")
	
	mission_stage = STAGE_INTRO_MOCAP_CUT
	//mission_stage = STAGE_DEBUG
//	iDebugStage = 13
	
	IF (Is_Replay_In_Progress())
		
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FAMILY_1)
		
		DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
		
		SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
		
				
		//SET_CLOCK_TIME(16, 00, 00)
			
        // Your mission is being replayed
        myStage = Get_Replay_Mid_Mission_Stage() //+ ENUM_TO_INT(STAGE_GET_INTO_AMANDAS_CAR)
		
		requestAndCreateInitialStuff(skip_mission_stage)
		
		PRINTLN("Trying to replay at stage: ", myStage )
		
		SWITCH myStage
			CASE 0
			CASE 1
				skip_mission_stage = STAGE_GET_INTO_AMANDAS_CAR
			BREAK
			CASE 2
				skip_mission_stage = STAGE_DO_CHASE
			BREAK
			CASE 3
				skip_mission_stage = STAGE_JIMMY_APPEARS
			BREAK
			CASE 4
				skip_mission_stage = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
			BREAK
			CASE 5
				skip_mission_stage = STAGE_TAKE_CAR_TO_CHOP_SHOP
			BREAK
			CASE 6
				skip_mission_stage = STAGE_CHOP_SHOP_CUTSCENE
			BREAK
			CASE 7
				skip_mission_stage = STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME
			BREAK
		ENDSWITCH
		
		
        //Use the myStage variable to restart your mission at the correct stage
        //Warp the player to appropriate start coordinates (because the player won’t be at the contact point)
    	//Fade In (the game gets faded out when a replay is selected)
		
		//skip_mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, myStage)
		
		IF g_bShitskipAccepted = TRUE
            // player chose to shitskip
            // you need to skip to stage after Get_Replay_Mid_Mission_Stage()
			
			IF skip_mission_stage = STAGE_GET_INTO_AMANDAS_CAR
				skip_mission_stage = STAGE_DO_CHASE
			ELIF skip_mission_stage = STAGE_DO_CHASE
				skip_mission_stage = STAGE_JIMMY_APPEARS
			ELIF skip_mission_stage = STAGE_JIMMY_APPEARS
				skip_mission_stage = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
			ELIF skip_mission_stage = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
				skip_mission_stage = STAGE_TAKE_CAR_TO_CHOP_SHOP
			ELIF skip_mission_stage = STAGE_TAKE_CAR_TO_CHOP_SHOP
				skip_mission_stage = STAGE_CHOP_SHOP_CUTSCENE
			ELIF skip_mission_stage = STAGE_CHOP_SHOP_CUTSCENE
				skip_mission_stage = STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME
			ELIF skip_mission_stage = STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME
				skip_mission_stage = STAGE_DROP_OFF_CUTSCENE
			ELIF skip_mission_stage = STAGE_DROP_OFF_CUTSCENE
				mission_passed()
			ENDIF
			
        ENDIF
		
		MANAGE_SKIP(skip_mission_stage, TRUE)
	
	ELSE
        // Your mission is being played normally, not being replayed
    	//Set_Replay_Mid_Mission_Stage(0) 
	ENDIF
	
	
//	//////////////
//truckYachtPackerTrailer = CREATE_VEHICLE(TR3, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, -9.0, 0.0>>), GET_ENTITY_HEADING(truckYachtPacker))
//ATTACH_VEHICLE_TO_TRAILER(truckYachtPacker, truckYachtPackerTrailer)
//	
//	////////////////
		
ENDPROC

#IF IS_DEBUG_BUILD

PROC APPLY_JACKKNIFE_FORCE()
	
	IF IS_BUTTON_PRESSED(PAD1, RIGHTSHOULDER1)
    	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
    		APPLY_FORCE_TO_ENTITY(truckYachtPackerTrailer, APPLY_TYPE_FORCE, vJackKnifeForce, vJackKnifeForceOffset, 0, TRUE, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF IS_BUTTON_PRESSED(PAD1, LEFTSHOULDER1)
    	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
			VECTOR tempJackKnifeForce = <<vJackKnifeForce.x * -1.0, vJackKnifeForce.y * -1.0, vJackKnifeForce.z>>
    		APPLY_FORCE_TO_ENTITY(truckYachtPackerTrailer, APPLY_TYPE_FORCE, tempJackKnifeForce, vJackKnifeForceOffset, 0, TRUE, TRUE, TRUE)
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF

//BOOL bFranklinsOutfitSet
BOOL bSetSunGlassesTint = FALSE
BOOL bSwitchEffectIntro
BOOL bFranklinExitStateHit = FALSE
BOOL bCameraExitStateHit   = FALSE		
BOOL bMichaelExitStateHit  = FALSE

PROC stageIntroMocapCutscene()

	IF i_current_event <= 3
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF

	SWITCH i_current_event

		CASE 0
			bFranklinExitStateHit = FALSE
			bCameraExitStateHit   = FALSE		
			bMichaelExitStateHit  = FALSE
			DESTROY_ALL_CAMS()
			cutsceneCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")					
//			initialCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//			destinationCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_FRANKLIN
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_1_int", CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 )
			ELSE		
				REQUEST_CUTSCENE("family_1_int")
			ENDIF
			i_current_event++
		BREAK
	 
		CASE 1
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL //GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL					
				SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_WHITE_SHIRT_JEANS)
			ENDIF
			
			SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_SHIRT_SHORTS_1)
		
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()//HAS_CUTSCENE_LOADED()
				i_current_event++
			ENDIF
		BREAK
	
		CASE 2
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
//			SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
//			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")			
			
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
					REGISTER_ENTITY_FOR_CUTSCENE(pedSelector.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					REGISTER_ENTITY_FOR_CUTSCENE(pedSelector.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
				ENDIF
			ENDIF
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			//SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			START_CUTSCENE()
			bSetSunGlassesTint = FALSE			
			bSwitchEffectIntro= FALSE
			//bFranklinsOutfitSet = FALSE
			i_current_event++
		BREAK
	
		CASE 3
		CASE 4
		CASE 5
		//CASE 6
			
			
			IF IS_CUTSCENE_PLAYING()
			AND i_current_event = 3
												
				IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
					
					CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID()) 
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SHIRT_SHORTS_1 , FALSE)

				ELSE
					SET_TIMECYCLE_MODIFIER("sunglasses")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_SHIRT_SHORTS_1, FALSE)				
		
				ENDIF
				
				DELETE_OBJECT(oiTumbler)
				DELETE_OBJECT(oiCigar)
				
				REMOVE_MODEL_HIDE(<<-816.72, 179.10, 72.83>>, 1.0, V_ILEV_MM_DOORM_L)
			   	REMOVE_MODEL_HIDE(<<-816.11, 177.51, 72.83>>, 1.0, V_ILEV_MM_DOORM_R)
				
				IF DOES_ENTITY_EXIST(oiLeftDoor)
               		DELETE_OBJECT(oiLeftDoor)
            	ENDIF                   
            
            	IF DOES_ENTITY_EXIST(oiRightDoor)
               		DELETE_OBJECT(oiRightDoor)
            	ENDIF		
				
				SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
				STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 150.00)
				
				SET_CLOCK_TIME(16, 00, 00)
				
				KILL_ANY_CONVERSATION()
				
									
				SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
				SET_CUTSCENE_RUNNING(FALSE, DEFAULT, DEFAULT, DEFAULT, FALSE)
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FAMILY_1)
								
				i_current_event = 4
			ENDIF

			IF i_current_event = 4
				IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
					i_current_event = 5
				ELSE
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
						pedSelector.pedID[SELECTOR_PED_MICHAEL] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
						CLEAR_PED_BLOOD_DAMAGE(pedSelector.pedID[SELECTOR_PED_MICHAEL]) 
						i_current_event = 5
					ENDIF
				ENDIF
			ENDIF		
			
//			IF i_current_event = 5
//				IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
//					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_MICHAEL)
//					TAKE_CONTROL_OF_SELECTOR_PED(pedSelector, TRUE, TRUE)
//					i_current_event = 6
//				ENDIF	
//			ENDIF	
			
			
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 40400.0
			AND GET_CUTSCENE_TIME() < 41434.0
				IF bSetSunGlassesTint = FALSE
					SET_TIMECYCLE_MODIFIER("sunglasses")
					SET_CLOCK_TIME(11, 00, 0)
					bSetSunGlassesTint = TRUE
				ENDIF
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 46065.6//46101.0
				IF bSetSunGlassesTint = TRUE
					CLEAR_TIMECYCLE_MODIFIER()
					bSetSunGlassesTint = FALSE
				ENDIF
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 185532.349//46101.0
				IF bSwitchEffectIntro= FALSE
					IF NOT bMIssionTriggeredAsMichael
						DO_SWITCH_EFFECT(CHAR_MICHAEL)
					ENDIF
					bSwitchEffectIntro = TRUE
				ENDIF
			ENDIF

//			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
//				IF NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael")))
//					SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael")))
//				ENDIF
//			ENDIF
			IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(pedSelector.pedID[SELECTOR_PED_MICHAEL])
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			//OR (IS_SCREEN_FADED_OUT() AND WAS_CUTSCENE_SKIPPED())
				PRINTLN("Cutscene: Franklin exit state")
				IF NOT IS_PED_INJURED(pedFranklin())
					//TASK_FOLLOW_NAV_MESH_TO_COORD (pedFranklin(), <<-791.2382, 180.6539, 71.8347>>, PEDMOVE_WALK)		
					FORCE_PED_MOTION_STATE(pedFranklin(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					//SCRIPT_ASSERT("EXIT STATE: Franklin")
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedFranklin())						
				ENDIF
				bFranklinExitStateHit = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()				
				RESET_GAME_CAMERA()
				bCameraExitStateHit = TRUE
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
	
				
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(pedSelector, TRUE, FALSE)
				ENDIF				
				
				PRINTLN("Cutscene: camera exit state hit")				
				bCameraExitStateHit = TRUE
			
			
				IF WAS_CUTSCENE_SKIPPED()
					RESET_GAME_CAMERA()
				ENDIF
			
			//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			//OR (IS_SCREEN_FADED_OUT() AND WAS_CUTSCENE_SKIPPED())
				PRINTLN("Cutscene: Michael exit state")
				CLEAR_TIMECYCLE_MODIFIER()			
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FAMILY_1)						
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")			
				
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SHIRT_SHORTS_1, FALSE)
				
				STORE_PLAYER_PED_VARIATIONS(pedFranklin(), TRUE)
				
				REPLAY_STOP_EVENT()
				
				SETTIMERB(0)
				bMichaelExitStateHit = TRUE
			ENDIF
				
			IF bFranklinExitStateHit
			AND bCameraExitStateHit
			AND bMichaelExitStateHit
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)		
				mission_stage = STAGE_GET_INTO_AMANDAS_CAR
			ENDIF
								
		BREAK
			
	ENDSWITCH
	
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedFranklin())
		IF NOT IS_ENTITY_DEAD(pedFranklin())
			SET_PED_MAX_MOVE_BLEND_RATIO(pedFranklin(), PEDMOVE_WALK)
		ENDIF
	ENDIF
	
ENDPROC

FLOAt fTimeToScan

FUNC FLOAT GET_RECORDING_TIME_AT_DISTANCE_BEHIND_YACHT(FLOAT fStartTime, FLOAT fDistanceBehindYacht)

	fTimeToScan = fStartTime
	//VECTOR vDistaAtTime = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, fTimeToScan, "Fam1Cine")
	VECTOR vDistaAtTimeTest = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(502, fTimeToScan, "Fam1")
	
	//PRINTLN("START!!!: vDistaAtTime : ", vDistaAtTime, " @ inc back: ", fTimeToScan)
	PRINTLN("START!!!: vDistaAtTimeTest : ", vDistaAtTimeTest, " @ inc back: ", fTimeToScan)
	WHILE GET_DISTANCE_BETWEEN_COORDS(vDistaAtTimeTest, GET_ENTITY_COORDS(truckYachtPacker)) < fDistanceBehindYacht
		
		PRINTLN("vDistaAtTimeTest : ", vDistaAtTimeTest, " @ inc back: ", fTimeToScan, "DIST: ", GET_DISTANCE_BETWEEN_COORDS(vDistaAtTimeTest, GET_ENTITY_COORDS(truckYachtPacker)))
		
		vDistaAtTimeTest = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(502, fTimeToScan, "Fam1")
		fTimeToScan = fTimeToScan - 100.0
		
		IF fTimeToScan < 0.0
			RETURN 0.0
		ENDIF
		
	ENDWHILE

	RETURN fTimeToScan + 100

ENDFUNC



FUNC BOOL IS_CAR_BEHIND_BOAT()

	IF GET_TIME_POSITION_IN_RECORDING(carPlayersJeep) < GET_TIME_POSITION_IN_RECORDING(carPlayersJeep) - fBehindBoatCheckValue //milliseconds
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC MANAGE_SWITCH_TO_RECORDING()

	FLOAT fPlayersProgress, fYachtProgress
	FLOAT fDistance
	
	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
	AND NOT IS_ENTITY_DEAD(truckYachtPacker)
		
		#IF IS_DEBUG_BUILD
			//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		//	DRAW_UBER_ROUTE_DEBUG_PATH(route_path)
			//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<0.0, -20.0, 0.0>>), 0.25, 200, 0, 0, 200)
		#ENDIF
				
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carPlayersJeep)
				
			IF IS_CAM_ACTIVE(cutsceneCamera) 
			OR IS_CAM_ACTIVE(cameraRecorded1)
			OR IS_CAM_ACTIVE(cameraRecorded2)
			
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
					
					fYachtProgress = GET_TIME_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(truckYachtPacker), 1, "Fam1New", route_path)
	
					#IF IS_DEBUG_BUILD
						IF bIsSuperdebugEnabled
							PRINTLN("fYachtProgress = ", fYachtProgress)
							PRINTLN("fPlayersProgress = ", fPlayersProgress)
							PRINTLN("fYachtProgress - fPlayersProgress = ", (fYachtProgress - fPlayersProgress))
						ENDIF
					#ENDIF
					
					IF NOT bForceCinematicCamera
						fDistance = GET_DISTANCE_BETWEEN_ENTITIES(carPlayersJeep, truckYachtPacker, TRUE)
						IF fDistance < 25.0
							fDistance = 25.0
						ENDIF
						fPlayersProgress = GET_RECORDING_TIME_AT_DISTANCE_BEHIND_YACHT(fYachtProgress, fDistance)
						#IF IS_DEBUG_BUILD
							IF bIsSuperdebugEnabled
								PRINTLN("PROGRESS BEHIND YACHT AT :",fPlayersProgress, " D:", fDistance )
							ENDIF
						#ENDIF
						
					ELSE
						IF eChaseStage > CHASE_FRANKLIN_BOARDS_LOOP
							fPlayersProgress = GET_RECORDING_TIME_AT_DISTANCE_BEHIND_YACHT(fYachtProgress, 23.0)
						ELSE
							fPlayersProgress = GET_RECORDING_TIME_AT_DISTANCE_BEHIND_YACHT(fYachtProgress, 24.5)
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF bIsSuperdebugEnabled
							PRINTLN("ADJUSTED: fPlayersProgress = ", fPlayersProgress)
						ENDIF
					#ENDIF
					
					mainPlaybackSpeedModifiedForChasingCar = mainPlaybackSpeed
					
					START_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 1, "Fam1Cine")	
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, fPlayersProgress)
					SET_PLAYBACK_SPEED(carPlayersJeep, mainPlaybackSpeed)
					
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(carPlayersJeep)
					
				ENDIF
			ENDIF
						
		ELSE
			
//			mainPlaybackSpeed = 1.0
			
			SET_PLAYBACK_SPEED(truckYachtPacker, mainPlaybackSpeed)
			SET_PLAYBACK_SPEED(truckYachtPackerTrailer, mainPlaybackSpeed)
			
			//when going at the same speed, the car sometimes collides with the yacht due to the concertina effect of the truck slowing down.
									
			IF eChaseStage > CHASE_FRANKLIN_BOARDS_LOOP
				//JUmping back from boat to car
				//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				IF GET_ENTITY_SPEED(carPlayersJeep)  > (GET_ENTITY_SPEED(truckYachtPackerTrailer)) //mainPlaybackSpeed < fDesiredPlaybackSpeed
				AND NOT IS_CAR_BEHIND_BOAT()
					mainPlaybackSpeedModifiedForChasingCar = mainPlaybackSpeedModifiedForChasingCar -@ 0.815
					//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(truckYachtPackerTrailer), 5.0, 127, 0, 0)
					//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(truckYachtPackerTrailer), GET_ENTITY_COORDS(carPlayersJeep),  255, 0, 0)
				ELSE
					mainPlaybackSpeedModifiedForChasingCar = mainPlaybackSpeedModifiedForChasingCar +@ 0.81					
					//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(truckYachtPackerTrailer), 5.0, 0, 127, 0)
					//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(truckYachtPackerTrailer), GET_ENTITY_COORDS(carPlayersJeep),  0, 255, 0)
				ENDIF
			ELSE
				//JUmping from car to boat.

				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				IF GET_ENTITY_SPEED(carPlayersJeep)  > GET_ENTITY_SPEED(truckYachtPackerTrailer)// GET_DISTANCE_BETWEEN_ENTITIES(carPlayersJeep, truckYachtPackerTrailer, TRUE) < 12.75
				AND NOT IS_CAR_BEHIND_BOAT()
				//IF GET_ENTITY_SPEED(carPlayersJeep) > GET_ENTITY_SPEED(truckYachtPacker)
					mainPlaybackSpeedModifiedForChasingCar = mainPlaybackSpeedModifiedForChasingCar -@ 0.18					
					//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(truckYachtPackerTrailer), 5.0, 255, 0, 0)
					//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(truckYachtPackerTrailer), GET_ENTITY_COORDS(carPlayersJeep),  255, 0, 0)
				ELSE
					mainPlaybackSpeedModifiedForChasingCar = mainPlaybackSpeedModifiedForChasingCar +@ 0.1
					//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(truckYachtPackerTrailer), 5.0, 0, 255, 0)
					//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(truckYachtPackerTrailer), GET_ENTITY_COORDS(carPlayersJeep),  0, 255, 0)
				ENDIF									
			ENDIF
			
			SET_PLAYBACK_SPEED(carPlayersJeep, mainPlaybackSpeedModifiedForChasingCar)
			
			IF NOT (IS_CAM_ACTIVE(cutsceneCamera) OR IS_CAM_ACTIVE(cameraRecorded1) OR IS_CAM_ACTIVE(cameraRecorded2))   //  IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			//AND NOT bForceCinematicCamera
			//AND NOT bCarBlendingIntoGamePlayAfterCut
				SET_ENTITY_COLLISION(carPlayersJeep, TRUE)
				STOP_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep)
			ENDIF
		ENDIF		
				
	ENDIF
	
ENDPROC

BOOL bCinematicCameraActive

//BOOL bUberRoutePathReady = FALSE

PROC OVERRIDE_CINEMATIC_CAMERA()


	IF BUILD_UBER_ROUTE_DEBUG_PATH_OVER_MULTIPLE_FRAMES(route_path, 1, "Fam1New")
		//bUberRoutePathReady = TRUE
	ENDIF
	
	UPDATE_CAM_PLAYBACK(cameraRecorded1, cameraCar, mainPlaybackSpeed)
	UPDATE_CAM_PLAYBACK(cameraRecorded2, cameraCarCamera, mainPlaybackSpeed)

//	IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cameraCar)
//		DELETE_VEHICLE(cameraCar)
//	ENDIF
//
//	IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cameraCarCamera)
//		DELETE_VEHICLE(cameraCarCamera)
//	ENDIF

	SWITCH iCameraCuts
		CASE 0
			IF fRecordingProgress > 0.0
				iCameraSelection = 0
				iCameraCuts++
			ENDIF		
		FALLTHRU
		
		CASE 1
			IF fRecordingProgress > 10000.050
				iCameraSelection = 1  //Heli
				iCameraCuts++
			ENDIF		
		FALLTHRU
		
		CASE 2
			IF fRecordingProgress > 37351.900
				iCameraSelection = 2 //Car
				iCameraCuts++
			ENDIF		
		FALLTHRU
				
		CASE 3
			IF fRecordingProgress > 64958.8800
				iCameraSelection = 1
				iCameraCuts++
			ENDIF		
		FALLTHRU
		
		CASE 4
			IF fRecordingProgress > 67958.8800
				iCameraSelection = 2
				iCameraCuts++
			ENDIF		
		FALLTHRU
		
		CASE 5
			IF fRecordingProgress > 73188.950
				iCameraSelection = 1
				iCameraCuts++
			ENDIF		
		FALLTHRU
		
		CASE 6
			IF fRecordingProgress > 96840.610
				iCameraSelection = 2
				iCameraCuts++
			ENDIF		
		FALLTHRU
		
		CASE 7
			IF fRecordingProgress > 106840.610
				
				iCameraSelection = 1
				iCameraCuts++
			ENDIF		
		FALLTHRU
		
		CASE 8
			IF fRecordingProgress > 109547.610
				iCameraSelection = 2
				iCameraCuts++
			ENDIF		
		FALLTHRU		
		
		CASE 9
			IF fRecordingProgress > 116840.610
						
				IF bCinecameraUsed = FALSE
					bPrintCinematicCameraHelpText = FALSE
					bCinecameraUsed = TRUE
				ENDIF
				
				iCameraSelection = 0
				iCameraCuts++
			ENDIF		
		FALLTHRU
				
		CASE 10
			IF fRecordingProgress > 126840.610
				iCameraSelection = 1
				iCameraCuts++
			ENDIF		
		FALLTHRU
		
		CASE 11
			IF fRecordingProgress > 136840.610
				iCameraSelection = 2
				iCameraCuts++
			ENDIF
			
		FALLTHRU
		
		CASE 12
			IF fRecordingProgress > 185840.610

				iCameraCuts++
			ENDIF
		FALLTHRU
		
		
				
	ENDSWITCH
	
	//In tunnel have car cams
//	IF fRecordingProgress > 35791.0200
//	AND fRecordingProgress < 56007.570
//		iCameraSelection = 2 //Car
//	ENDIF
	
	//Remember selection if we force camera
	
//	IF bForceCinematicCamera
//	OR bForceCutsceneCameraAsCinematicCamera
//		IF bFlagCameraSelection = FALSE
//			iPrevCameraSelection = iCameraSelection
//			bFlagCameraSelection = TRUE
//		ENDIF
//		iCameraSelection = 0
//	ELSE
//		IF bFlagCameraSelection = TRUE
//			iCameraSelection = iPrevCameraSelection
//			iCameraCuts = 0
//			bFlagCameraSelection = FALSE
//		ENDIF
//	ENDIF


	//IF DOES_CAM_EXIST(cutsceneCamera)
		//PRINTSTRING("Cam exists") PRINTNL()
		
		VECTOR vPlayerCarCoords, vTruckCoords
			//FLOAT fDistToSeg
			FLOAT fDiffZ
		
		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
			vPlayerCarCoords = GET_ENTITY_COORDS(carPlayersJeep)
		ENDIF
			
		IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
			vTruckCoords = GET_ENTITY_COORDS(truckYachtPackerTrailer)
		ENDIF
		
		//Only calculate if pressing the cinematic button
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		OR bForceCinematicCamera	
			
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carPlayersJeep)
				//fDistToSeg = GET_DISTANCE_BETWEEN_COORDS(vPlayerCarCoords, route_path[segmentClosestToPlayer], FALSE)
				fDiffZ = ABSF(vPlayerCarCoords.z - vTruckCoords.z)
			ELSE
				//fDistToSeg = 0.0
				fDiffZ = 0.0
			ENDIF
			//PRINTLN("Dist to segment: ", fDistToSeg)
			//PRINTLN("Z dif: ", (vPlayerCarCoords.z - route_path[segmentClosestToPlayer].z))		
		ENDIF

		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
		AND eChaseStage > CHASE_FRANKLIN_JUMPS_ON_YACHT
		AND eChaseStage < CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_UPDATE		
		AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
		AND fDiffZ < 3.0
		AND NOT IS_PHONE_ONSCREEN()
		AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(carPlayersJeep), GET_ENTITY_HEADING(truckYachtPackerTrailer), 50.0)
		AND NOT IS_ENTITY_AT_COORD(carPlayersJeep, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, vCatchJimmyOffset), <<8.0, 14.0, 5.0>>)
		//AND bUberRoutePathReady
		OR (bForceCinematicCamera)
				
			IF bPrintCinematicCameraHelpText = FALSE
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_CAM_ACTIVE(cutsceneCamera) 
			AND NOT IS_CAM_ACTIVE(cameraRecorded1) 
			AND NOT IS_CAM_ACTIVE(cameraRecorded2) 
			AND (eChaseStage > CHASE_FRANKLIN_BOARDS_INTRO)
				PRINT_HELP("YB_HELP3")
				bPrintCinematicCameraHelpText = TRUE
			ENDIF
		
			IF SHOULD_CONTROL_CHASE_HINT_CAM(localChaseHintCamStructButtonPress, FALSE, FALSE)	
			OR bForceCinematicCamera
				
				//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_1_FOCUS_ON_YACHT")
					START_AUDIO_SCENE("FAMILY_1_FOCUS_ON_YACHT")
				ENDIF
				
				//Record the fact the player has used the cinematic camera.
				IF NOT bForceCinematicCamera
					bCinecameraUsed = TRUE
				ENDIF
				
				IF fRecordingProgress > 145840.610
					iCameraSelection = 1
				ENDIF
				
				IF NOT bForceCinematicCamera
				AND NOT bForceCutsceneCameraAsCinematicCamera
				
				SWITCH iCameraSelection
					CASE 0
						IF NOT bForceCinematicCamera
							IF NOT IS_CAM_ACTIVE(cutsceneCamera)
								SET_CAM_ACTIVE(cutsceneCamera, TRUE)
								SHAKE_CAM(cutsceneCamera, "SKY_DIVING_SHAKE", 1.0)
							ENDIF
						ENDIF
						SET_CAM_ACTIVE(cameraRecorded1,FALSE)
						SET_CAM_ACTIVE(cameraRecorded2,FALSE)
						SET_CAM_MOTION_BLUR_STRENGTH(cutsceneCamera, 0.1)
					BREAK
				
					CASE 1 //HeliCam
						SET_CAM_ACTIVE(cutsceneCamera, FALSE)
						SET_CAM_ACTIVE(cameraRecorded1, TRUE)
						SET_CAM_ACTIVE(cameraRecorded2,FALSE)
						SET_CAM_MOTION_BLUR_STRENGTH(cameraRecorded1, 0.1)
						SET_CAM_NEAR_CLIP(cameraRecorded1, 0.0)
					BREAK
				
					CASE 2 //HeliCam
						SET_CAM_ACTIVE(cutsceneCamera, FALSE)
						SET_CAM_ACTIVE(cameraRecorded1,FALSE)
						SET_CAM_ACTIVE(cameraRecorded2, TRUE)
						SET_CAM_MOTION_BLUR_STRENGTH(cameraRecorded2, 0.1)
						SET_CAM_NEAR_CLIP(cameraRecorded2, 0.0)
					BREAK
								
				ENDSWITCH
					
				ENDIF
				
				IF bForceCinematicCamera
				OR bForceCutsceneCameraAsCinematicCamera
					SET_CAM_ACTIVE(cutsceneCamera, TRUE)
					SET_CAM_ACTIVE(cameraRecorded1,FALSE)
					SET_CAM_ACTIVE(cameraRecorded2,FALSE)
				ENDIF
				
				IF bForceActionCameraAsCinematicCamera
					SET_CAM_ACTIVE(cutsceneCamera, FALSE)
					SET_CAM_ACTIVE(cameraRecorded1,TRUE)
					SET_CAM_ACTIVE(cameraRecorded2,FALSE)
				ENDIF
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				SET_WIDESCREEN_BORDERS(TRUE,0)
				
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
				
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				IF bForceCinematicCamera //IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),"MISSFAM1_YACHTBATTLEonYacht01_", "grab_pistol")
				AND bForceCutsceneCameraAsCinematicCamera
				ELSE
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
				
				//Clear the cinematic help text but not the RAGE help text.
				IF iRageText = 0
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
				ENDIF
				
				bCinematicCameraActive = TRUE
				
			ELSE
			
				IF IS_AUDIO_SCENE_ACTIVE("FAMILY_1_FOCUS_ON_YACHT")
					STOP_AUDIO_SCENE("FAMILY_1_FOCUS_ON_YACHT")
				ENDIF
			
				IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
			
				//PRINTSTRING("button NOT pressed") PRINTNL()
				
				SET_CAM_ACTIVE(cutsceneCamera, FALSE)
				SET_CAM_ACTIVE(cameraRecorded1,FALSE)
				SET_CAM_ACTIVE(cameraRecorded2,FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				SET_WIDESCREEN_BORDERS(FALSE,0)
				
				bCinematicCameraActive = FALSE
				
			ENDIF
										
		ELSE
		
			IF IS_GAMEPLAY_HINT_ACTIVE()
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_1_FOCUS_ON_YACHT")
					START_AUDIO_SCENE("FAMILY_1_FOCUS_ON_YACHT")
				ENDIF
			ELSE
				IF IS_AUDIO_SCENE_ACTIVE("FAMILY_1_FOCUS_ON_YACHT")
					STOP_AUDIO_SCENE("FAMILY_1_FOCUS_ON_YACHT")
				ENDIF
			ENDIF
			
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
			bCinematicCameraActive = FALSE
						
			SET_CAM_ACTIVE(cutsceneCamera, FALSE)
			SET_CAM_ACTIVE(cameraRecorded1,FALSE)
			SET_CAM_ACTIVE(cameraRecorded2,FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
			SET_WIDESCREEN_BORDERS(FALSE,0)
			
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
			AND (eChaseStage < CHASE_FRANKLIN_JUMPS_ON_YACHT
				OR eChaseStage > CHASE_CATCH_FRANKLIN)
				
				IF eChaseStage > CHASE_CATCH_FRANKLIN
					CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, truckYachtPackerTrailer, "YB_FOCUS", HINTTYPE_DEFAULT, TRUE, FALSE)
				ELSE
					CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, truckYachtPackerTrailer, "YB_FOCUS", HINTTYPE_DEFAULT, TRUE, (g_replay.iReplayInt[0] = 0) AND (eChaseStage > CHASE_FRANKLIN_BOARDS_INTRO))
				ENDIF
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					g_replay.iReplayInt[0] = 1 //First time we show this.
				ENDIF
			ENDIF
			
		ENDIF
		
		
		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carPlayersJeep)
				SHUNT_RESET_FLAGS()
			ENDIF	
		ENDIF	
		
		MANAGE_SWITCH_TO_RECORDING()
	

ENDPROC

BOOL bSoundVehicleHorn
INT iTimeOfTruckHornSounding

INT iHornOffTime = 900
INT iHornOnTime = 1200
INT iHornCounter
INT iHornFreq = 2

INT iCarHornTimer

PROC UPDATE_CAR_HORNS()

	INT i
	
	
	//IF eChaseStage > CHASE_FRANKLIN_JUMPS_ON_YACHT
		
		FOR i = iFirstCarToProcess TO TOTAL_NUMBER_OF_TRAFFIC_CARS - 1
		
			IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(carPlayersJeep)
				bNearMissCouldHaveHappened = FALSE
			ENDIF
			
			IF DOES_ENTITY_EXIST(TrafficCarID[i])
				IF GET_DISTANCE_BETWEEN_ENTITIES(carPlayersJeep, TrafficCarID[i], FALSE) < 8.0
				AND eChaseStage > CHASE_FRANKLIN_JUMPS_ON_YACHT
					//Near miss happened... now make sure we dont hit anything for 500ms
					//SET_HORN_PERMANENTLY_ON_TIME(TrafficCarID[i], 5000.0)	
					bNearMissCouldHaveHappened	= TRUE
					iMissStartTime = GET_GAME_TIMER()
				ENDIF
				
				//Truck horn
				IF GET_DISTANCE_BETWEEN_ENTITIES(TrafficCarID[i], truckYachtPacker, FALSE) < 15.0
				AND GET_GAME_TIMER() - iTimeOfTruckHornSounding > iHornOnTime // 900
				AND bSoundVehicleHorn = FALSE
					bSoundVehicleHorn = TRUE
					
					iHornCounter++
					
					IF iHornCounter % iHornFreq = 0 //3
						iHornOnTime = GET_RANDOM_INT_IN_RANGE(500, 1000)
						
						IF iHornFreq = 2
							iHornFreq = 3
						ELSE
							iHornFreq = 2
						ENDIF
					ELSE
						iHornOnTime = GET_RANDOM_INT_IN_RANGE(200, 250)
					ENDIF
					
					iTimeOfTruckHornSounding = GET_GAME_TIMER()
				ENDIF
				
				IF bSoundVehicleHorn = TRUE
					IF GET_GAME_TIMER() - iTimeOfTruckHornSounding > iHornOffTime
						IF iHornCounter % iHornFreq = 0
							iHornOffTime = GET_RANDOM_INT_IN_RANGE(900, 1200)
						ELSE
							iHornOffTime = GET_RANDOM_INT_IN_RANGE(400, 500)
						ENDIF
						iTimeOfTruckHornSounding = GET_GAME_TIMER()
						bSoundVehicleHorn = FALSE
					ENDIF
				ENDIF
				
				IF bSoundVehicleHorn
					//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(truckYachtPacker), 3.0, 0, 0, 255, 127)
					SET_HORN_PERMANENTLY_ON(truckYachtPacker)	
				ENDIF
				
			ENDIF
			
			IF bNearMissCouldHaveHappened				
				IF (GET_GAME_TIMER() - iMissStartTime) > 500
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_NEARMIS", CONV_PRIORITY_HIGH)
							bNearMissCouldHaveHappened = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDFOR

		FOR i = iFirstCarToProcess TO TOTAL_NUMBER_OF_SET_PIECE_CARS - 1
					
			IF DOES_ENTITY_EXIST(setPieceCarID[i])
//				IF GET_DISTANCE_BETWEEN_ENTITIES(carPlayersJeep, setPieceCarID[i], FALSE) < 8.0
//					//SET_HORN_PERMANENTLY_ON_TIME(setPieceCarID[i], 5000.0)	
//				ENDIF
				
				IF GET_DISTANCE_BETWEEN_ENTITIES(setPieceCarID[i], truckYachtPacker, FALSE) < 15.0
					SET_HORN_PERMANENTLY_ON(truckYachtPacker)	
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	//ENDIF
	
	VEHICLE_INDEX thisCar = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0, DUMMY_MODEL_FOR_SCRIPT, 0)
	
	IF DOES_ENTITY_EXIST(thisCar)
	AND GET_GAME_TIMER() - iCarHornTimer > 1000
		IF NOT IS_ENTITY_DEAD(thisCar)
			IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(thisCar))
				//IF GET_ENTITY_SPEED(thisCar) > 0.5
				IF VMAG2(GET_ENTITY_VELOCITY(thisCar)) > 0.5
				
//					TEXT_LABEL_63 thisString = "horn on this car:"
//					thisString += GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(thisCar)
//				
//					SCRIPT_ASSERT(thisString)
					SET_HORN_PERMANENTLY_ON_TIME(thisCar, 5000.0)	
					
					iCarHornTimer = GET_GAME_TIMER() 
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
ENDPROC


// get the average change in truck heading over the last 0.1 sec
//Stolen from GTA IV's Truck Hustle.LOD dista
PROC HANDLE_CHANGE_IN_fRotation()
    IF NOT IS_ENTITY_DEAD(carPlayersJeep)
        fRotationPrev = fRotation
        fRotation = GET_ENTITY_HEADING(carPlayersJeep)
        fRotationAccel = fRotation - fRotationPrev
        
        IF fRotationAccel > 180.0
            fRotationAccel -= 360.0
        ELIF fRotationAccel < -180.0
            fRotationAccel += 360.0
        ENDIF
        
        IF heading_timer >= 0.1
            heading_timer = 0.0                       
            fRotationAccel_average = (fRotationAccel_total / headings_captured) * 10

            fRotationAccel_total = 0.0 
            headings_captured = 0
        ELSE
            fRotationAccel_total += fRotationAccel
            heading_timer = heading_timer +@ 1.0
            headings_captured++
        ENDIF        
    ENDIF
ENDPROC

//INT iTimeOfLastSpeedGrab

FUNC BOOL IS_FRANKLIN_IN_JUMP_ZONE(FLOAT fScale = 1.0, BOOL bDoSlowDown = TRUE)

	VECTOR vSubZone = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<0.0, -10.0, 0.0>>)
	VECTOR vSubZone1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<0.0, -11.0, 0.0>>)
	VECTOR vSubZone2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<0.0, -13.0, 0.0>>)
	VECTOR vSubZone3 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<0.0, -14.0, 0.0>>)
	//VECTOR vSubZone4 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<0.0, -15.0, 0.0>>)
	FLOAT fZoneDistance = (2.30 * fScale)
	
	#IF IS_DEBUG_BUILD
		IF bIsSuperDebugEnabled
			DRAW_DEBUG_SPHERE(vSubZone, fZoneDistance, 0, 0, 255, 30)
			DRAW_DEBUG_SPHERE(vSubZone1, fZoneDistance, 0, 0, 255, 30)
			DRAW_DEBUG_SPHERE(vSubZone2, fZoneDistance, 0, 0, 255, 30)
			DRAW_DEBUG_SPHERE(vSubZone3, fZoneDistance, 0, 0, 255, 30)
		ENDIF
	#ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(carPlayersJeep, vSubZone, FALSE) < fZoneDistance
		IF bDoSlowDown
			IF GET_ENTITY_SPEED(truckYachtPackerTrailer) < GET_ENTITY_SPEED(carPlayersJeep)
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.0)
			ELSE
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
			ENDIF
		ELSE
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
		ENDIF
		#IF IS_DEBUG_BUILD
			IF bIsSuperDebugEnabled
				DRAW_DEBUG_SPHERE(vSubZone,  fZoneDistance, 255, 0, 0, 30)
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF

	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(carPlayersJeep, vSubZone1, FALSE) < fZoneDistance
		IF bDoSlowDown
			IF GET_ENTITY_SPEED(truckYachtPackerTrailer) < GET_ENTITY_SPEED(carPlayersJeep)
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 12.5)
			ELSE
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
			IF bIsSuperDebugEnabled
				DRAW_DEBUG_SPHERE(vSubZone1,  fZoneDistance, 255, 0, 0, 30)
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(carPlayersJeep, vSubZone2, FALSE) < fZoneDistance
		IF bDoSlowDown
			IF GET_ENTITY_SPEED(truckYachtPackerTrailer) < GET_ENTITY_SPEED(carPlayersJeep)
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 10.5)
			ELSE
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
			IF bIsSuperDebugEnabled
				DRAW_DEBUG_SPHERE(vSubZone2,  fZoneDistance, 255, 0, 0, 30)
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(carPlayersJeep, vSubZone3, FALSE) < fZoneDistance
		IF bDoSlowDown
			IF GET_ENTITY_SPEED(truckYachtPackerTrailer) < GET_ENTITY_SPEED(carPlayersJeep)
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 8.5)
			ELSE
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
			IF bIsSuperDebugEnabled
				DRAW_DEBUG_SPHERE(vSubZone3,  fZoneDistance, 255, 0, 0, 30)
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
//	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(carPlayersJeep, vSubZone4, FALSE) < fZoneDistance
//		IF bDoSlowDown
//			IF GET_ENTITY_SPEED(truckYachtPackerTrailer) < GET_ENTITY_SPEED(carPlayersJeep)
//				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 6.5)
//			ELSE
//				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
//			ENDIF
//		ENDIF
//		#IF IS_DEBUG_BUILD
//			IF bIsSuperDebugEnabled
//				DRAW_DEBUG_SPHERE(vSubZone4,  fZoneDistance, 255, 0, 0, 30)
//			ENDIF
//		#ENDIF
//		RETURN TRUE
//	ENDIF

	SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)

	RETURN FALSE
	
ENDFUNC

FUNC BOOL CAN_JIMMY_DROP_INTO_CAR(FLOAT fMultiplier = 1.0)

	BOOL bDisplayLocate
	
	IF fMultiplier > 1.0
		bDisplayLocate = FALSE
	ELSE
		bDisplayLocate = TRUE
	ENDIF

	VECTOR vLocateSize = <<2.0, 2.0, 1.75>> * fMultiplier
	VECTOR vLocatePos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<vCatchJimmyOffset.x, vCatchJimmyOffset.y, 0.2>>)
	GET_GROUND_Z_FOR_3D_COORD(vLocatePos, vLocatePos.z)
	//vLocatePos.z = vLocatePos.z + 0.5

	RETURN IS_ENTITY_AT_COORD(carPlayersJeep, vLocatePos, vLocateSize, bDisplayLocate, bDisplayLocate) //Dont do 3d locate if checking for the fail
	
ENDFUNC

PROC SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(PED_INDEX thisPed, PED_RESET_FLAGS prfThisFlag, BOOL bSetResetFlag )
	
	SET_RAGDOLL_BLOCKING_FLAGS(thisPed, RBF_IMPACT_OBJECT)
	SET_PED_RESET_FLAG(thisPed, prfThisFlag, bSetResetFlag )
	REQUEST_RAGDOLL_BOUNDS_UPDATE(thisPed, 1)
	SET_PED_RESET_FLAG(thisPed, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(thisPed, PCF_RunFromFiresAndExplosions, FALSE)		
	SET_PED_CAN_EVASIVE_DIVE(thisPed, FALSE)
	
ENDPROC

PROC MANAGE_FRANKLIN_ON_BONNET()

	// IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING returns true only if pov camera is active.
	// So this will only disable the bonnet camera (aka hood camera)
	IF NOT IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	ENDIF
	
	SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedFranklin(), PRF_BlockWeaponReactionsUnlessDead, TRUE)
	
	//If you get out and walk into franklin have him ragdoll.
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, TRUE)
		PRINTLN("fall of dist: ", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedFranklin()), GET_ENTITY_COORDS(PLAYER_PED_ID())))
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedFranklin()), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 1.5					
		OR IS_ENTITY_ON_FIRE(pedFranklin())
			PROCESS_ENTITY_ATTACHMENTS(pedFranklin())
			SET_PED_TO_RAGDOLL(pedFranklin(), 0, 60000, TASK_NM_SCRIPT)
			CLEAR_HELP()
			MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
			TRIGGER_MUSIC_EVENT("FAM1_FAIL")
			Mission_Failed()	
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
	AND NOT IS_PED_INJURED(pedFranklin())
	
		//SET_ENTITY_COLLISION(pedFranklin(), FALSE)


		HANDLE_CHANGE_IN_fRotation()

		fForwardSpeed = GET_ENTITY_SPEED(carPlayersJeep)
		IF bInitAccelReading = FALSE
			fForwardSpeedPrev1 = GET_ENTITY_SPEED(carPlayersJeep)
			fForwardSpeedPrev2 = GET_ENTITY_SPEED(carPlayersJeep)
			carHealthPrevious = GET_ENTITY_HEALTH(carPlayersJeep)
			bInitAccelReading = TRUE
		ENDIF

		#IF IS_DEBUG_BUILD
			IF bIsSuperDebugEnabled
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(carPlayersJeep, GET_STRING_FROM_FLOAT(fForwardSpeed), 2.15)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(carPlayersJeep, GET_STRING_FROM_FLOAT(fForwardSpeedPrev1), 1.85)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(carPlayersJeep, GET_STRING_FROM_FLOAT(fForwardSpeedPrev2), 1.75)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(carPlayersJeep, GET_STRING_FROM_FLOAT(fFowardAccel), 1.65)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(carPlayersJeep, GET_STRING_FROM_FLOAT(fRotationAccel_average), 1.25)
			ENDIF
		#ENDIF

		fFowardAccel = (fForwardSpeed - ((fForwardSpeedPrev1 + fForwardSpeedPrev2) / 2) ) / GET_FRAME_TIME()
		fForwardSpeedPrev2 = fForwardSpeedPrev1
		fForwardSpeedPrev1 = fForwardSpeed
		
		//PRINTLN("\nfFowardAccel:", fFowardAccel, "fForwardSpeedPrev1", fForwardSpeedPrev1, "fForwardSpeedPrev2: ", fForwardSpeedPrev2 )
		
		//Make rotational forces more significant
		
		#IF IS_DEBUG_BUILD
			IF bIsSuperDebugEnabled
				PRINTLN("\nCarHealthPrevious: ", carHealthPrevious, "GET_ENTITY_HEALTH(carPlayersJeep): ", GET_ENTITY_HEALTH(carPlayersJeep))
			ENDIF
		#ENDIF
		
		//FAILS
		VECTOR vCarRotation = GET_ENTITY_ROTATION(carPlayersJeep)
		
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carPlayersJeep)
		AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
		AND NOT IS_ENTITY_TOUCHING_ENTITY(carPlayersJeep, truckYachtPackerTrailer)
		AND carHealthPrevious - GET_ENTITY_HEALTH(carPlayersJeep) > 0
			IF	ABSF(fFowardAccel) >   65.00 //(fBackwardsThreshold * 25.0)
			OR (vCarRotation.y > 90.0 AND vCarRotation.y < 270.0)
			OR (vCarRotation.x > 90.0 AND vCarRotation.x < 270.0)
				
				#IF IS_DEBUG_BUILD
					IF bIsSuperDebugEnabled
						PRINTLN("\nfFowardAccel:", fFowardAccel, "fForwardSpeedPrev1", fForwardSpeedPrev1, "fForwardSpeedPrev2: ", fForwardSpeedPrev2, "GET_FRAME_TIME() ->", GET_FRAME_TIME() )
						PRINTFLOAT(fFowardAccel) PRINTSTRING(" > 65.00")
						PRINTSTRING("FAIL fall forwards") PRINTNL()			
					ENDIF
				#ENDIF
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
				
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "GENERIC_FRIGHTENED_HIGH", "MICHAEL_NORMAL", SPEECH_PARAMS_FORCE)
					
				eBalanceState = STATE_FALL_FORWARDS_FAIL
			ENDIF											
		ENDIF			
		
		carHealthPrevious = GET_ENTITY_HEALTH(carPlayersJeep)
		
		SWITCH eBalanceState

			CASE STATE_STATIONARY
			
				PRINTLN("eBalanceState: GET_ENTITY_SPEED(carPlayersJeep)", GET_ENTITY_SPEED(carPlayersJeep))
			
				IF GET_ENTITY_SPEED(carPlayersJeep) > 3.5
					sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_loop", "", FALSE, FALSE, SYNCED_SCENE_NONE)
					iLeanStage = 0
					eBalanceState =  STATE_BALANCE
				ENDIF
			BREAK
			
			CASE STATE_BALANCE
				
				PRINTLN("STATE_BALANCE")
				
				iLeanStage = 0
				
				IF IS_FRANKLIN_IN_JUMP_ZONE() 
					fSteadinessModifier = 2.0	
				ELSE
					fSteadinessModifier = 1.0
				ENDIF
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
					//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<0.0, 0.0, 3.0>>), 1.6, 255, 0, 0)
					
					PRINTLN("STATE_BALANCE - > IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)")
					
					//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					//	CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_FALTER", CONV_PRIORITY_MEDIUM)
					//ENDIF
					
					//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedFranklin(), "FAM1_FALTER", "FRANKLIN", "SPEECH_PARAMS_FORCE_FRONTEND")
					//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_FRIGHTENED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE)
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >=  0.95 //responsiveness test - 1.0
						sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_loop", "", FALSE, TRUE, SYNCED_SCENE_NONE)
						iLeanStage = 0
					ENDIF
		
					//IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 0.95 //1.0
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDclimbOutOfCar)
						IF IS_FRANKLIN_IN_JUMP_ZONE() //GET_DISTANCE_BETWEEN_ENTITIES(pedFranklin(), truckYachtPackerTrailer) < 20.0
							eBalanceState = STATE_JUMP_PREP
						ENDIF
					ENDIF
					//ENDIF
				
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDclimbOutOfCar)
						
						PRINTLN("STATE_BALANCE - > IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDclimbOutOfCar)")
							
						
						//Do Y falling animations
						IF fFowardAccel < -21.0 * fSteadinessModifier
							PRINTSTRING("setting STATE STATE_FALL_FORWARDS") PRINTNL()
							eBalanceState = STATE_FALL_FORWARDS
						ENDIF
						
						//Do X falling animations
						IF fRotationAccel_average < (fLeftThreshold  * fSteadinessModifier)
							PRINTSTRING("setting STATE STATE_FALL_LEFT") PRINTNL()
							eBalanceState = STATE_FALL_LEFT
						ELIF fRotationAccel_average > (fRightThreshold  * fSteadinessModifier)
							PRINTSTRING("setting STATE STATE_FALL_RIGHT") PRINTNL()
							eBalanceState = STATE_FALL_RIGHT
						ENDIF
									
						IF fRotationAccel_average > fLeftThreshold
						AND fRotationAccel_average < fRightThreshold
						//AND fFowardAccel > fBackwardsThreshold
							PRINTSTRING("setting STATE STATE_BALANCE") PRINTNL()
							eBalanceState = STATE_BALANCE
						ENDIF
						
						IF IS_FRANKLIN_IN_JUMP_ZONE() //GET_DISTANCE_BETWEEN_ENTITIES(pedFranklin(), truckYachtPackerTrailer) < 20.0
							eBalanceState = STATE_JUMP_PREP
						ENDIF
										
						IF GET_ENTITY_SPEED(carPlayersJeep) < 3.0
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDbonnetStatic)
								sceneIDbonnetStatic = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_loopStop", "", FALSE, FALSE, SYNCED_SCENE_NONE)
							ENDIF
							PRINTSTRING("setting STATE STATE_STATIONARY") PRINTNL()
							eBalanceState = STATE_STATIONARY
						ENDIF
						
				
					ENDIF
				ENDIF
						
			BREAK
			
			CASE STATE_JUMP_PREP
				
				sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_jumpPrep", "", FALSE, TRUE, SYNCED_SCENE_NONE)
				PRINTSTRING("setting STATE STATE_JUMP_PREP_LOOP from STATE_JUMP_PREP") PRINTNL()
				eBalanceState = STATE_JUMP_PREP_LOOP
			BREAK
			
			CASE STATE_JUMP_PREP_LOOP
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 1.0
						sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_jumpPrep_loop", "", FALSE, FALSE, SYNCED_SCENE_NONE)
						PRINTSTRING("setting STATE STATE_JUMP_PREP_CHECK_FOR_CANCEL from STATE_JUMP_PREP_LOOP") PRINTNL()
						eBalanceState = STATE_JUMP_PREP_CHECK_FOR_CANCEL
					ENDIF
				ENDIF
			BREAK
			
			CASE STATE_JUMP_PREP_CHECK_FOR_CANCEL
				//If distance is too far... cancel prep...
				IF NOT IS_FRANKLIN_IN_JUMP_ZONE(1.35) // GET_DISTANCE_BETWEEN_ENTITIES(pedFranklin(), truckYachtPackerTrailer) > 52.5
					sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_jumpPrep_cancel", "", FALSE, FALSE, SYNCED_SCENE_NONE)
					PRINTSTRING("setting STATE STATE_BALANCE from STATE_JUMP_PREP_CHECK_FOR_CANCEL") PRINTNL()
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_SHOCKED_MED", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)
					eBalanceState = STATE_BALANCE
				ENDIF
			BREAK
			
			CASE STATE_FALL_LEFT
				//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<-4.0, 0.0, 0.0>>), 0.6)
				
				SWITCH iLeanStage
				
					CASE 0
						sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_leanLeft_intro", "", FALSE, TRUE, SYNCED_SCENE_NONE)						
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "FAM1_BWAA", "FRANKLIN", SPEECH_PARAMS_FORCE_FRONTEND)
						iLeanStage++
					BREAK
					
					CASE 1
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 1.0
								sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_leanLeft_loop", "", FALSE, TRUE, SYNCED_SCENE_NONE)
								//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_SHOCKED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)
								iLeanStage++
							ENDIF
						ENDIF
					BREAK
					
					CASE 2
//					
//						PRINTSTRING("fRotationAccel_average > ") PRINTFLOAT(fRotationAccel_average)
//						PRINTNL()
//						PRINTSTRING("fLeftThreshold") PRINTFLOAT(fLeftThreshold)
//						PRINTNL()
//						
						IF fRotationAccel_average > fLeftThreshold
						//IF fRotationAccel_average > (fRightThreshold / 2.0)
							iLeanStage++
						ENDIF
						
					BREAK
					
					CASE 3
						sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_leanLeft_outro", "", FALSE, TRUE, SYNCED_SCENE_NONE)
						eBalanceState = STATE_BALANCE
						iLeanStage++
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE STATE_FALL_RIGHT
				//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<4.0, 0.0, 0.0>>), 0.6)
				
				SWITCH iLeanStage
				
					CASE 0
						sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_leanRight_intro", "", FALSE, TRUE, SYNCED_SCENE_NONE)
						//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_SHOCKED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)
						iLeanStage++
					BREAK
					
					CASE 1
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDStumble)
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDStumble) >= 1.0
								sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_leanRight_loop", "", FALSE, TRUE, SYNCED_SCENE_NONE)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_FRIGHTENED_MED", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)
								iLeanStage++
							ENDIF
						ENDIF
					BREAK
					
					CASE 2
					
						PRINTSTRING("fRotationAccel_average > ") PRINTFLOAT(fRotationAccel_average)
						PRINTNL()
						PRINTSTRING("fLeftThreshold") PRINTFLOAT(fLeftThreshold)
						PRINTNL()
						
						IF fRotationAccel_average < fRightThreshold
						//IF fRotationAccel_average < (fLeftThreshold / 2.0)
							iLeanStage++
						ENDIF
						
					BREAK
					
					CASE 3
						sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_leanRight_outro", "", FALSE, TRUE, SYNCED_SCENE_NONE)
						eBalanceState = STATE_BALANCE
						iLeanStage++
						
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE STATE_FALL_FORWARDS
				//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<0.0, 4.0, 0.0>>), 0.6)
				sceneIDStumble = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleinCar01_", "FranklinOnBonnet_leanBrake", "", FALSE, TRUE, SYNCED_SCENE_NONE)
				//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedFranklin(), "FAM1_FALTER", "FRANKLIN", "SPEECH_PARAMS_FORCE_FRONTEND")
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_SHOCKED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)	
				eBalanceState = STATE_BALANCE
			BREAK
			
			
			CASE STATE_FALL_FORWARDS_FAIL
				//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedFranklin(), "FAM1_FALTER", "FRANKLIN", "SPEECH_PARAMS_FORCE_FRONTEND")
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_FRIGHTENED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)	
				//CLEAR_PED_TASKS(pedFranklin())
				PROCESS_ENTITY_ATTACHMENTS(pedFranklin())
				SET_PED_TO_RAGDOLL(pedFranklin(), 0, 60000, TASK_NM_BALANCE) 
				
				eChaseStage = CHASE_FAILURE
				
			BREAK
			
			
//			CASE STATE_FALL_BACKWARDS
//				//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<0.0, -4.0, 0.0>>), 0.6)
//				
//				sceneIDStumble = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//				TASK_SYNCHRONIZED_SCENE(pedFranklin(), sceneIDStumble, "MISSFAM1_  YACHTBATTLE", "FranklinOnBonnet_leanAccel", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
//				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDStumble, FALSE)
//				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDStumble, carPlayersJeep, GET_ENTITY_BONE_INDEX_BY_NAME(carPlayersJeep, "seat_pside_f"))
//				eBalanceState = STATE_BALANCE
//			BREAK
			
		ENDSWITCH
		
//		
//		PRINTSTRING("Rotation Speed: ") PRINTFLOAT(fRotation) PRINTNL()
//		PRINTSTRING("Rotation accel: ") PRINTFLOAT(fRotationAccel_average) PRINTNL()
//		
//			
//		PRINTSTRING("fForwardSpeed: ") PRINTFLOAT(fForwardSpeed) PRINTNL()
//		PRINTSTRING("fFowardAccel: ") PRINTFLOAT(fFowardAccel) PRINTNL()
			
		
		
		
		
	ENDIF
	
	
		
ENDPROC
//Text

PROC failConditionsPreChase()

	IF IS_SCREEN_FADED_IN()
		
		//Shoehorn in weapons in safehouse disabling		
		INTERIOR_INSTANCE_INDEX iPlayerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())

		IF iPlayerInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -812.0607, 179.5117, 71.1531 >>, "v_michael") 
		
			IF IS_VALID_INTERIOR(iPlayerInterior) AND IS_INTERIOR_READY(iPlayerInterior)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
				
				WEAPON_TYPE currentWeaponType
				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeaponType)
					IF (currentWeaponType <> WEAPONTYPE_UNARMED)
					AND (currentWeaponType <> WEAPONTYPE_OBJECT)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
				ENDIF
				IF GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), currentWeaponType)
					IF (currentWeaponType <> WEAPONTYPE_UNARMED)
					AND (currentWeaponType <> WEAPONTYPE_OBJECT)
						SET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
					ENDIF
				ENDIF
			ENDIF

		ENDIF
		
		
		
		//Jeep dies
		IF DOES_ENTITY_EXIST(carPlayersJeep)
			IF NOT IS_VEHICLE_DRIVEABLE(carPlayersJeep)	
			OR IS_ENTITY_ON_FIRE(carPlayersJeep)
				MISSION_FLOW_SET_FAIL_REASON("CMN_GENDEST")
				Mission_Failed()
			ENDIF
			
			IF IS_VEHICLE_PERMANENTLY_STUCK(carPlayersJeep) 
				MISSION_FLOW_SET_FAIL_REASON("CMN_GENSTCK")
				Mission_Failed()
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(carPlayersJeep), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 150.0
					IF NOT IS_ENTITY_ON_SCREEN(carPlayersJeep)

						MISSION_FLOW_SET_FAIL_REASON("YB_FAIL2")
									
						SETTIMERA(0)
						TRIGGER_MUSIC_EVENT("FAM1_FAIL")

						Mission_Failed()
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC


PROC REMOVE_BLIPS_ON_FAIL()

	//REMOVE_BLIP(blipTruckYachtPacker)				
	IF DOES_BLIP_EXIST(blippedBadguy2)
		REMOVE_BLIP(blippedBadguy2)
	ENDIF
	IF DOES_BLIP_EXIST(blippedBadGuy3BoxThrower)
		REMOVE_BLIP(blippedBadGuy3BoxThrower)
	ENDIF
	IF DOES_BLIP_EXIST(blipMichaelSon)
		REMOVE_BLIP(blipMichaelSon)
	ENDIF
	IF DOES_BLIP_EXIST(blipPlayersCar)
		REMOVE_BLIP(blipPlayersCar)
	ENDIF
	IF DOES_BLIP_EXIST(blipTruckYachtPackerTrailer)
		REMOVE_BLIP(blipTruckYachtPackerTrailer)
	ENDIF
	CLEAR_ALL_FLOATING_HELP()
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	CLEAR_PRINTS()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
	ENDIF
		
ENDPROC			


FUNC BOOL failConditions()

	IF IS_SCREEN_FADED_IN()

		//Jimmy dies
		IF DOES_ENTITY_EXIST(pedJimmy)
			IF IS_PED_INJURED(pedJimmy)
				//PRINT_NOW("YB_FAIL1", DEFAULT_GOD_TEXT_TIME, 1)
				MISSION_FLOW_SET_FAIL_REASON("YB_FAIL1")
				Mission_Failed()
				RETURN TRUE
			ENDIF
		ENDIF
				
		IF DOES_ENTITY_EXIST(carPlayersJeep)
			
			//Jeep dies
			IF NOT IS_VEHICLE_DRIVEABLE(carPlayersJeep)
			OR IS_ENTITY_DEAD(carPlayersJeep)
			
				IF NOT IS_PED_INJURED(pedFranklin())
					//CLEAR_PED_TASKS(pedFranklin())
					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedFranklin(), PRF_BlockWeaponReactionsUnlessDead, TRUE)
				ENDIF
			
				eChaseStage = CHASE_FAILURE_DESTROYED_CAR
				RETURN TRUE
			ENDIF
			
			IF IS_VEHICLE_PERMANENTLY_STUCK(carPlayersJeep)
			//IF IS_ENTITY_UPSIDEDOWN(carPlayersJeep)
				IF NOT IS_PED_INJURED(pedFranklin())
					//CLEAR_PED_TASKS(pedFranklin())
					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedFranklin(), PRF_BlockWeaponReactionsUnlessDead, TRUE)
				ENDIF
				eChaseStage = CHASE_FAILURE_STUCK_CAR
				RETURN TRUE
			ENDIF
			
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
				//IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(carPlayersJeep), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 120.0
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), carPlayersJeep, <<100.0, 100.0, 100.0>>)
					MISSION_FLOW_SET_FAIL_REASON("YB_FAIL2")
								
					SETTIMERA(0)
					TRIGGER_MUSIC_EVENT("FAM1_FAIL")
					WHILE TIMERA() < DEFAULT_GOD_TEXT_TIME
						IF eChaseStage < CHASE_FRANKLIN_JUMPS_ON_YACHT
							MANAGE_FRANKLIN_ON_BONNET()
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(pedFranklin())		
							SET_ENTITY_INVINCIBLE(pedFranklin(), TRUE)
							SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedFranklin(), PRF_BlockWeaponReactionsUnlessDead, TRUE)
						ENDIF
						
						WAIT(0)
					ENDWHILE
					TRIGGER_MUSIC_EVENT("FAM1_FAIL")
					DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
					Mission_Failed()
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(truckYachtPacker)
				IF NOT IS_ENTITY_DEAD(truckYachtPacker)				
				//AND eChaseStage > CHASE_FRANKLIN_STANDS_UP
				AND eChaseStage <> CHASE_PUT_FRANKLIN_BACK_IN_CAR_PROPERLY
				
					IF eChaseStage < CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
						
						IF NOT IS_ENTITY_AT_ENTITY(carPlayersJeep, truckYachtPacker, <<230.0, 230.0, 230.0>>, FALSE, FALSE)
													
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
							AND bAlreadyWarnedWeAreLosingYacht = FALSE
								PRINT_NOW("YB_WARN1", DEFAULT_GOD_TEXT_TIME, 1)
								bAlreadyWarnedWeAreLosingYacht = TRUE
							ENDIF
							
														
							IF NOT IS_ENTITY_AT_ENTITY(carPlayersJeep, truckYachtPacker, <<300.0, 300.0, 300.0>>, FALSE, FALSE)							
								eChaseStage = CHASE_FAILURE_LOST_YACHT
								RETURN TRUE
							ENDIF
							
						ENDIF
												
					ENDIF
					
					//Reset warning.
					IF bAlreadyWarnedWeAreLosingYacht = TRUE
						IF IS_ENTITY_AT_ENTITY(carPlayersJeep, truckYachtPacker, <<180.0, 180.0, 180.0>>, FALSE, FALSE)
							bAlreadyWarnedWeAreLosingYacht = FALSE
						ENDIF
					ENDIF
				
//					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//					DRAW_DEBUG_SPHERE(route_path[segmentClosestToPlayer], 2.0)
					
				ENDIF
				
				UPDATE_CHASE_BLIP(blipTruckYachtPackerTrailer, truckYachtPackerTrailer, 300.0, 0.85) 
				
			ENDIF		
			
			//Perhaps we should have a fail if you destroy the yacht somehow...
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
			ENDIF
		ELSE
			PRINTLN("Warning Jeep is NULL")
		ENDIF
	ELSE
		PRINTLN("Warning Screen faded out Family 1 Fail")
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_IN_FRONT_OF_TRANSPORTER()
	
	VECTOR a, b, c
	
	IF NOT IS_ENTITY_DEAD(truckYachtPacker)
		a = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<-1.0, 6.0, 0.0>>)
		b = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<1.0, 6.0, 0.0>>)
		c = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0
	
ENDFUNC


FUNC BOOL IS_PLAYER_IN_FRONT_OF_TRANSPORTER_OFFSET(FLOAT fOffset)
	
	VECTOR a, b, c
	
	IF NOT IS_ENTITY_DEAD(truckYachtPacker)
		a = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<-1.0, fOffset, 0.0>>)
		b = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<1.0, fOffset, 0.0>>)
		c = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_FRONT_OF_JUMP_POINT()
	
	VECTOR a, b, c
	
	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
		a = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<-1.0, -7.5, 0.0>>)
		b = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<1.0, -7.5, 0.0>>)
		c = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0
	
ENDFUNC

INT iAltRespond

PROC CONSTANT_DIALOGUE()

	bDoDialogue = TRUE
	INT iTimeBetweenSpeech = 3000
	FLOAT fPlayRange = 150.0

	SWITCH eChaseStage 
	
		CASE CHASE_FRANKLIN_STANDS_UP		
		CASE CHASE_FRANKLIN_STANDS_LOOP
			
			IF iAltRespond % 2= 0
				IF  eBalanceState = STATE_BALANCE
					constantDialogue = "FAM1_CLOSERF"
				ELSE
					constantDialogue = ""
				ENDIF
			ELSE
				constantDialogue = "FAM1_ENC1"
			ENDIF
			
			iTimeBetweenSpeech = 2750
		BREAK
		
		CASE CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR
		CASE CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR_LOOP
		CASE CHASE_FRANKLIN_JUMPS_ON_YACHT
			
			IF NOT IS_FRANKLIN_IN_JUMP_ZONE(1.0, FALSE)
				IF iAltRespond % 2= 0
					
					constantDialogue = "FAM1_CLOSERF"
					iTimeBetweenSpeech = 1000
				ELSE
					constantDialogue = "FAM1_ENC1" //"FAM1_CLOSERM"
					iTimeBetweenSpeech = GET_RANDOM_INT_IN_RANGE(1500, 2500)
				ENDIF
			ELSE
				IF iAltRespond % 2= 0
					constantDialogue = "FAM1_ENC2"
					iTimeBetweenSpeech = GET_RANDOM_INT_IN_RANGE(1500, 2500)
				ELSE
					constantDialogue = "FAM1_CLOSERM" //"FAM1_CLOSERM"
					iTimeBetweenSpeech = GET_RANDOM_INT_IN_RANGE(1500, 2500)
				ENDIF
			ENDIF
			
			
			
			IF IS_PLAYER_IN_FRONT_OF_JUMP_POINT()
				//SCRIPT_ASSERT("in front")
				constantDialogue = "FAM1_SIDE"
			ENDIF
			
		BREAK
		
		CASE CHASE_FRANKLIN_BOARDS_INTRO
		CASE CHASE_FRANKLIN_BOARDS_LOOP
		CASE CHASE_FRANKLIN_BOARDS_OUTRO
			constantDialogue = "FAM1_TLK2SLF"
			fPlayRange = 100.0
			iTimeBetweenSpeech = 8000
		BREAK
		
		CASE CHASE_STRUGGLE_1
		CASE CHASE_STRUGGLE_1_LOOP
			constantDialogue = "FAM1_TROUBLE"
			fPlayRange = 70.0
		BREAK
		
		CASE CHASE_STRUGGLE_1_UPDATE
			constantDialogue = "FAM1_ENC3"
			fPlayRange = 70.0
		BREAK
		
		CASE CHASE_HIDE_BEHIND_WHEEL_INTRO
		CASE CHASE_HIDE_BEHIND_WHEEL_LOOP			
			constantDialogue = "FAM1_SHOUT"
			fPlayRange = 100.0
			iTimeBetweenSpeech = 8000
		BREAK
		
		CASE CHASE_STRUGGLE_1_OUTRO
			constantDialogue = "FAM1_ATTACK"
			fPlayRange = 70.0
		BREAK
		
		CASE CHASE_STRUGGLE_2
		CASE CHASE_STRUGGLE_2_LOOP		
		//CASE CHASE_STRUGGLE_2_OUTRO
			//At the poitn where we should shoot the guy...
			IF iRageText >= 4	
			AND NOT IS_PED_INJURED(pedBadguy2)
				IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
					constantDialogue = "FAM1_ENC4"		
					fPlayRange = 70.0
				ELSE
					constantDialogue = "FAM1_ATTACK"		
					fPlayRange = 70.0
				ENDIF
			ENDIF
		BREAK
						
		CASE CHASE_HIDE_BEHIND_WHEEL_2_INTRO
			//constantDialogue = "FAM1_ATTACK"
			constantDialogue = ""	
		BREAK
		
		CASE CHASE_MEET_JIMMY
		CASE CHASE_SIGNAL_PLAYER
			constantDialogue = ""	
		BREAK
		
		CASE CHASE_BOOM_SWING
			constantDialogue = "FAM1_ATTACK"
		BREAK
		
		
		CASE CHASE_BOOM_SWING_LOOP	
			constantDialogue = ""	
		BREAK
		
		CASE CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3
			
			SWITCH iAltRespond
				CASE 0
					IF NOT IS_PED_INJURED(pedBadGuy3BoxThrower)
						constantDialogue = "FAM1_ATTACK"
						fPlayRange = 70.0
					ENDIF
				BREAK
				CASE 1
					IF bJimmyLanded = FALSE
						constantDialogue = "FAM1_HANGON"
						fPlayRange = 70.0
					ENDIF
				BREAK
				
				CASE 2
					IF NOT IS_PED_INJURED(pedBadGuy3BoxThrower)
						constantDialogue = "FAM1_ENC4"
						fPlayRange = 70.0
					ENDIF
				BREAK
			
			ENDSWITCH
			
			iTimeBetweenSpeech = 1200
					
		BREAK	
		
		//CASE CHASE_WAIT_FOR_CONVO						//updateBoomBaddie()
		CASE CHASE_FRANKLIN_NOTIONS_TO_SAVE_JIMMY
		CASE CHASE_CATCH_JIMMY_DETACH
			
			IF CAN_JIMMY_DROP_INTO_CAR()
				IF bJimmyLanded = FALSE
//					constantDialogue = "FAM1_REASSU2"
				ENDIF
			ELSE
			
				SWITCH iAltRespond
										
					CASE 0
						IF GET_RANDOM_INT_IN_RANGE(0, 10) > 7
							IF NOT IS_PED_INJURED(pedBadguy4BoomGuy)
								constantDialogue = "FAM1_ENC6"
							ELSE
								constantDialogue = "FAM1_ENC5"
							ENDIF
						ELSE
							constantDialogue = "FAM1_ENC5"
						ENDIF
						fPlayRange = 70.0
					BREAK
									
					CASE 1
//						IF GET_RANDOM_BOOL()
//							constantDialogue = "FAM1_JIMMAST"
//						ELSE
							constantDialogue = "FAM1_HANG"
//						ENDIF
						fPlayRange = 70.0
					BREAK
				
					CASE 2
						IF bJimmyLanded = FALSE
							IF GET_RANDOM_BOOL()
								constantDialogue = "FAM1_REASSUR"
							ELSE	
								constantDialogue = "FAM1_HANGN2"
							ENDIF
							fPlayRange = 70.0
						ENDIF
				 	BREAK
										
				ENDSWITCH 
			
			ENDIF
			
			iTimeBetweenSpeech = 1200
			
		BREAK
		
		CASE CHASE_FRANKLIN_RECOVERS
			constantDialogue = "FAM1_TROUBLE" //NULL	
			fPlayRange = 70.0
		BREAK
		
		CASE CHASE_FRANKLIN_RECOVERS_LOOP
		CASE CHASE_CATCH_FRANKLIN
		//CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
			constantDialogue =  "FAM1_CTCHF" ////FAM1_CTCHF"
			fPlayRange = 70.0
		BREAK	
		
		CASE CHASE_PUT_FRANKLIN_BACK_IN_CAR_PROPERLY
		CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_2
		CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_UPDATE
			constantDialogue = NULL	
		BREAK
				
		CASE CHASE_KEEP_CHASING
			constantDialogue = "FAM1_BAD" //NULL	
			//constantDialogue = "FAM1_GETYCHT"
			
		BREAK	
		
		CASE CHASE_NO_MORE_STAGES
		CASE CHASE_NO_MORE_STAGES_2
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer, FALSE) < 45.0
				constantDialogue = "FAM1_PLLOVER"	
				fPlayRange = 120.0
			ELSE
				constantDialogue = "FAM1_GETYCHT"
				fPlayRange = 120.0
			ENDIF
			iTimeBetweenSpeech = 4000
		BREAK
		
		DEFAULT
			bDoDialogue = FALSE
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
	AND bPlayInitialDialogue = TRUE
	
		SWITCH iSmashDialogues
		
			CASE 0
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
				iSmashDialogues++
			BREAK
		
			CASE 1
				IF fRecordingProgress > 38813.050
					PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_CAR_CRASH_BIG", truckYachtPackerTrailer)
					iSmashDialogues++
				ENDIF
				
			BREAK
			
			CASE 2
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()  //IS_CONVERSATION_STATUS_FREE()
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CRASH", CONV_PRIORITY_VERY_HIGH)		
						iSmashDialogues++
					ENDIF
				ELSE
					iSmashDialogues++
				ENDIF
			BREAK
			
			CASE 3
				IF fRecordingProgress > 43880.470
					PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_CAR_CRASH_BIG", truckYachtPackerTrailer)		
					iSmashDialogues++
				ENDIF
			BREAK
			
			CASE 4
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()  //IS_CONVERSATION_STATUS_FREE()
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPacker, FALSE) < fPlayRange
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CRASH", CONV_PRIORITY_VERY_HIGH)		
						iSmashDialogues++
					ENDIF
				ELSE
					iSmashDialogues++
				ENDIF
			BREAK
			
			CASE 5
				IF fRecordingProgress > 62000.470
					PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_CAR_CRASH_BIG", truckYachtPackerTrailer)
					iSmashDialogues++
				ENDIF
			BREAK
			
			CASE 6
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()  //IS_CONVERSATION_STATUS_FREE()
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPacker, FALSE) < fPlayRange
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CRASH", CONV_PRIORITY_VERY_HIGH)		
						iSmashDialogues++
					ENDIF
				ELSE
					iSmashDialogues++
				ENDIF
			BREAK
			
			CASE 7
				IF fRecordingProgress > 73145.520
					PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_CAR_CRASH_BIG", truckYachtPackerTrailer)
					iSmashDialogues++
				ENDIF
			BREAK
			
			CASE 8
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()  //IS_CONVERSATION_STATUS_FREE()
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPacker, FALSE) < fPlayRange
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_GARB", CONV_PRIORITY_VERY_HIGH)		
						iSmashDialogues++
					ENDIF
				ELSE
					iSmashDialogues++
				ENDIF
			BREAK
			
			CASE 9
				IF fRecordingProgress > 133791.0200
					PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_CAR_CRASH_BIG", truckYachtPackerTrailer)
					iSmashDialogues++
				ENDIF
			BREAK
			
			CASE 10
				IF iAltRespond = 3
					//IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_BRIDGE", CONV_PRIORITY_VERY_HIGH)	
						iSmashDialogues++
					//ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
	
	ENDIF

	#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL(constantDialogue)
			SET_CONTENTS_OF_TEXT_WIDGET(dialogueThatShouldBePlaying, constantDialogue)
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(dialogueThatShouldBePlaying, "NULL")
		ENDIF
	#ENDIF
	

	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
	AND bPlayInitialDialogue = TRUE
	AND bDoDialogue
		IF (GET_GAME_TIMER() - iTimeOfLastConstantDialogue) > iTimeBetweenSpeech //1200
		AND GET_ENTITY_SPEED(carPlayersJeep) > 0.1	//Dont play when stationary
		AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPacker, FALSE) < fPlayRange
		
			//PRINTSTRING("time passed") PRINTNL()
			//IF NOT IS_STRING_NULL(constantDialogue)
			IF NOT IS_STRING_NULL_OR_EMPTY(constantDialogue)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING() //IS_CONVERSATION_STATUS_FREE() //IS_SCRIPTED_CONVERSATION_ONGOING()
				AND NOT IS_AMBIENT_SPEECH_PLAYING(pedFranklin())
					PRINTSTRING(constantDialogue) PRINTNL()
					
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", constantDialogue, CONV_PRIORITY_VERY_HIGH)
				
						IF iAltRespond < 3
							iAltRespond++
						ELSE
							iAltRespond= 0
						ENDIF
											
						//constantDialogue = NULL
						iTimeOfLastConstantDialogue = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	
	//Franklin comments on player hitting stuff
	
		IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(carPlayersJeep)
		OR bCollisionHappening
		
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()//  IS_CONVERSATION_STATUS_FREE()
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND eChaseStage < CHASE_FRANKLIN_BOARDS_INTRO
				bCollisionHappening = TRUE
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_COLLIDE", CONV_PRIORITY_MEDIUM)
					bCollisionHappening = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC

PED_INDEX guyInFlatBedCar
INT iDeleteGuyTimer
BOOL bSetPiece3BeenHit

PROC checkForSetpieces()

	SWITCH iSetPieceFlatbed
	
		CASE 0
			IF DOES_ENTITY_EXIST(SetPieceCarID[3])
				IF NOT IS_ENTITY_DEAD(SetPieceCarID[3])
					SET_VEHICLE_COLOURS(SetPieceCarID[3], 88, 1)
					guyInFlatBedCar = GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3], VS_DRIVER)
					IF DOES_ENTITY_EXIST(guyInFlatBedCar)
						SET_ENTITY_AS_MISSION_ENTITY(guyInFlatBedCar, TRUE, TRUE)
						SET_ENTITY_VISIBLE(guyInFlatBedCar, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(guyInFlatBedCar, TRUE)
						iDeleteGuyTimer = GET_GAME_TIMER()
						iSetPieceFlatbed++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			PRINTLN("Kill HIM TIMER:", GET_GAME_TIMER() - iDeleteGuyTimer)
			
			IF NOT IS_ENTITY_DEAD(SetPieceCarID[3])
			AND NOT IS_ENTITY_DEAD(carPlayersJeep)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(SetPieceCarID[3], carPlayersJeep)  //[MF] fix for B* 1737866
					bSetPiece3BeenHit = TRUE
				ENDIF
				
				IF IS_ENTITY_TOUCHING_ENTITY(SetPieceCarID[3], carPlayersJeep)
					IF DOES_ENTITY_EXIST(guyInFlatBedCar)
						DELETE_PED(guyInFlatBedCar)
					ENDIF	
				ENDIF
				
			ENDIF
			
			IF GET_GAME_TIMER() - iDeleteGuyTimer > (18000 / mainPlaybackSpeed)
			OR bSetPiece3BeenHit
				IF DOES_ENTITY_EXIST(guyInFlatBedCar)
					DELETE_PED(guyInFlatBedCar)
				ENDIF
				iSetPieceFlatbed++
			ENDIF
		BREAK

	ENDSWITCH
	
	SWITCH iFirstTrafficCar
	
		CASE 0
			IF DOES_ENTITY_EXIST(SetPieceCarID[9])
				IF NOT IS_ENTITY_DEAD(SetPieceCarID[9])
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[9])
						IF IS_ENTITY_AT_ENTITY(SetPieceCarID[9], PLAYER_PED_ID(), <<8.75, 8.75, 3.0>>)
							IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[9], VS_DRIVER))
								TASK_VEHICLE_TEMP_ACTION(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[9], VS_DRIVER), SetPieceCarID[9], TEMPACT_HANDBRAKETURNRIGHT, 10000)
								iFirstTrafficCar++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH iSecondTrafficCar
	
		CASE 0
			IF DOES_ENTITY_EXIST(TrafficCarID[8])
				IF NOT IS_ENTITY_DEAD(TrafficCarId[8])
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TrafficCarId[8])
						IF IS_ENTITY_AT_ENTITY(TrafficCarId[8], PLAYER_PED_ID(), <<12.75, 12.75, 3.0>>)
							IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(TrafficCarId[8], VS_DRIVER))
								TASK_VEHICLE_TEMP_ACTION(GET_PED_IN_VEHICLE_SEAT(TrafficCarId[8], VS_DRIVER), TrafficCarId[8], TEMPACT_HANDBRAKETURNRIGHT, 10000)
								iSecondTrafficCar++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//Imran#s range rover.
	IF DOES_ENTITY_EXIST(SetPieceCarID[4])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[4])
			SET_VEHICLE_COLOURS(SetPieceCarID[4], 73, 1)
			//SET_VEHICLE_NUMBER_PLATE_TEXT(SetPieceCarID[4], "  JBM 1")
			//SET_HORN_PERMANENTLY_ON(SetPieceCarID[4])
		ENDIF
	ENDIF

ENDPROC

PROC checkForDistanceFromFranklin()

	
	#IF IS_DEBUG_BUILD
	IF mission_stage <> STAGE_DEBUG
	#ENDIF
	
	IF DOES_ENTITY_EXIST(pedFranklin())
	
		IF NOT IS_ENTITY_DEAD(pedFranklin())
		
			SET_PED_RESET_FLAG(pedFranklin(), PRF_DisablePotentialBlastReactions, TRUE)
		
			IF NOT (mission_stage = STAGE_DO_CHASE
					OR mission_stage = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
					OR mission_stage = STAGE_JIMMY_APPEARS)//	eChaseStage = CHASE_FRANKLIN_STANDS_UP
				//IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedFranklin()), TRUE) > 120.00
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedFranklin(), <<100.0, 100.0, 100.0>>)
					//PRINT_NOW("CMN_FLEFT", DEFAULT_GOD_TEXT_TIME, 1) //You left Franklin behind.
					MISSION_FLOW_SET_FAIL_REASON("CMN_FLEFT")
					Mission_Failed()
				ENDIF
			ENDIF
		ENDIF	
	
		IF IS_PED_INJURED(pedFranklin())
		AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)
			//PRINT_NOW("CMN_FDIED", DEFAULT_GOD_TEXT_TIME, 1)
			MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
			Mission_Failed()
				
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		//This allows franklin to be mortal but still do all his animations.
		IF NOT IS_ENTITY_DEAD(pedFranklin())
			IF NOT IS_ENTITY_DEAD(pedFranklin())
			AND eChaseStage <> CHASE_FAILURE
				SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedFranklin(), PRF_BlockWeaponReactionsUnlessDead, TRUE)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(pedFranklin())
			SET_ENTITY_INVINCIBLE(pedFranklin(), FALSE)
		ENDIF
	ENDIF


ENDPROC

PROC updatePlaybackProgressFloat()
	
	IF DOES_ENTITY_EXIST(truckYachtPacker)
		IF NOT IS_ENTITY_DEAD(truckYachtPacker)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPacker)
				fRecordingProgress = GET_TIME_POSITION_IN_RECORDING(truckYachtPacker)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC




#IF IS_DEBUG_BUILD

PROC debugRoutines()

	APPLY_JACKKNIFE_FORCE()

	SWITCH iDebugStage
		
		CASE 0 //Debug init
		
			REQUEST_MODEL(TR3)
			REQUEST_MODEL(PHANTOM)
			REQUEST_MODEL(SENTINEL2)
			REQUEST_VEHICLE_RECORDING(1, "Fam1New")
			
			REQUEST_VEHICLE_RECORDING(2, "Fam1Cut")
			
			REQUEST_VEHICLE_RECORDING(1, "Fam1Cine")
			
			
			
			IF HAS_MODEL_LOADED(TR3)
			AND HAS_MODEL_LOADED(PHANTOM)
			AND HAS_MODEL_LOADED(SENTINEL2)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Fam1New")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "Fam1Cut")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Fam1Cine")
				iDebugStage = 13	
			ENDIF
			
		BREAK
	
		CASE 1
		
			IF bRecordTrailer
								
				iDebugStage = 2
				bRecordTrailer = FALSE
			ENDIF
			
		BREAK
		////
		CASE 2
			DELETE_PED(pedYachtPacker)
			truckYachtPacker = CREATE_VEHICLE(PHANTOM, << -2319.9204, -321.5558, 12.8060 >>, 239.5773)
			SET_ENTITY_PROOFS(truckYachtPacker, TRUE, TRUE, TRUE, TRUE, TRUE)
			
			carPlayersJeep = CREATE_VEHICLE(SENTINEL2, << -2319.9204, -321.5558, 12.8060 >>, 239.5773)
			SET_ENTITY_COLLISION(carPlayersJeep, FALSE)
			
//			truckYachtPackerTrailer = CREATE_VEHICLE(TR3, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, 3.0, 0.0>>), fTruckYachtPackerStartHeading)
//			ATTACH_VEHICLE_TO_TRAILER(truckYachtPacker, truckYachtPackerTrailer)	
//			
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), truckYachtPacker, VS_FRONT_RIGHT)
				//WAIT(1000)
				START_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 1, "Fam1New")
	
				//START_RECORDING_VEHICLE(truckYachtPackerTrailer, 2, "Fam1New", TRUE)
	
				START_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 1, "Fam1Cine")
				//WAIT(0)	
				
//			

				
				
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 80000)
//				WAIT(100)
//				STOP_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker)
//				
//			ENDIF
							
							
			iDebugStage = 6
				
		BREAK
		
		CASE 3
			
			IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(truckYachtPacker)
				ATTACH_VEHICLE_TO_TRAILER(truckYachtPacker, truckYachtPackerTrailer)	
			ENDIF
				
			IF NOT IS_ENTITY_DEAD(truckYachtPacker)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPacker)
					IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
						STOP_RECORDING_VEHICLE(truckYachtPackerTrailer)	
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 4
		
			//START_RECORDING_VEHICLE(truckYachtPacker, 1000, "Fam1C")
			//START_RECORDING_VEHICLE(truckYachtPackerTrailer, 1001, "Fam1C")
			truckYachtPacker = CREATE_VEHICLE(PHANTOM, << -2319.9204, -321.5558, 12.8060 >>, 239.5773)
			SET_ENTITY_PROOFS(truckYachtPacker, TRUE, TRUE, TRUE, TRUE, TRUE)
			
			SET_ENTITY_COORDS(truckYachtPacker, vTruckYachtPackerStartPosition)
			SET_ENTITY_HEADING(truckYachtPacker, fTruckYachtPackerStartHeading )
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), truckYachtPacker, VS_DRIVER)
			
		
			INIT_UBER_RECORDING("Fam1")
			set_uber_parent_widget_group(family1WidgetGroup)
			iDebugStage = 5
		BREAK
	
		CASE 5
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_N)
				START_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 1, "Fam1New")
				SET_ENTITY_COLLISION(truckYachtPacker, FALSE)
				bStartRecordingPlayersCarWithTraffic = TRUE
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(truckYachtPacker)
				IF IS_BUTTON_PRESSED(PAD1, DPADDOWN)
					SET_ENTITY_COLLISION(truckYachtPacker, TRUE)
				ELSE
					SET_ENTITY_COLLISION(truckYachtPacker, FALSE)
				ENDIF
			ENDIF
						
						
			UPDATE_UBER_RECORDING()
//			
//			IF bSaveRecording = TRUE
//				bSaveRecording = FALSE
//				STOP_RECORDING_VEHICLE(truckYachtPacker)
//				STOP_RECORDING_VEHICLE(truckYachtPackerTrailer)
//			ENDIF
			
		BREAK
		
		CASE 6
			
			IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(truckYachtPacker)
				ATTACH_VEHICLE_TO_TRAILER(truckYachtPacker, truckYachtPackerTrailer)	
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_N)
				
				REMOVE_BLIP(blipTruckYachtPackerTrailer)
				STOP_RECORDING_VEHICLE(truckYachtPackerTrailer)
			ENDIF
			
		BREAK
		
		CASE 7
		
			REQUEST_MODEL(GAUNTLET)
			REQUEST_MODEL(FLATBED)
			
			WHILE NOT HAS_MODEL_LOADED(GAUNTLET)
			OR NOT HAS_MODEL_LOADED(FLATBED)
				WAIT(0)
			ENDWHILE
			
			CLEAR_AREA(<< -1141.3258, -702.7331, 20.3691 >>, 100.0, TRUE)
			LOAD_SCENE(<< -1141.3258, -702.7331, 20.3691 >>)
			
			attachVehicle1 = CREATE_VEHICLE(GAUNTLET, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			attachVehicle2 = CREATE_VEHICLE(FLATBED, << -1141.3258, -702.7331, 20.3691 >>, 311.1768)
		
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), attachVehicle2)
			
			START_RECORDING_VEHICLE(attachVehicle2, 995, "Fam1", TRUE)
			START_RECORDING_VEHICLE(attachVehicle1, 994, "Fam1", TRUE)
			
			iDebugStage = 8
		BREAK
		
		CASE 8
		
			IF NOT IS_ENTITY_DEAD(attachVehicle1)
			AND NOT IS_ENTITY_DEAD(attachVehicle2)
				ATTACH_ENTITY_TO_ENTITY(attachVehicle1, attachVehicle2, 0, attachOffset, AttachRotation)
			ENDIF
			
			IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
				iDebugStage = 9
			ENDIF
			
		BREAK
		
		CASE 9
			
			IF NOT IS_ENTITY_DEAD(attachVehicle1)
				DETACH_ENTITY(attachVehicle1, TRUE, FALSE)
				APPLY_FORCE_TO_ENTITY(attachVehicle1, APPLY_TYPE_IMPULSE, forceVector, forceOffsetVector, 0, TRUE, TRUE, TRUE)
				iDebugStage = 10
			ENDIF
		
		BREAK
		
		CASE 10
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1)
				SCRIPT_ASSERT("rec finished")
				
				IF NOT IS_ENTITY_DEAD(attachVehicle1)
					STOP_RECORDING_VEHICLE(attachVehicle1)
					DELETE_VEHICLE(attachVehicle1)
				ENDIF
				IF NOT IS_ENTITY_DEAD(attachVehicle2)
					STOP_RECORDING_VEHICLE(attachVehicle2)
					DELETE_VEHICLE(attachVehicle2)
				ENDIF
				
			ENDIF
			
		BREAK
		
		
				
		CASE 11
			DELETE_PED(pedYachtPacker)
			truckYachtPacker = CREATE_VEHICLE(PHANTOM, << -10.1903, -2610.3462, 27.5527 >>, 242.4301)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), truckYachtPacker, VS_FRONT_RIGHT)
			LOAD_SCENE(<< -10.1903, -2610.3462, 27.5527 >>)
			SET_ENTITY_PROOFS(truckYachtPacker, TRUE, TRUE, TRUE, TRUE, TRUE)
			
			truckYachtPackerTrailer = CREATE_VEHICLE(TR3, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, 3.0, 0.0>>), fTruckYachtPackerStartHeading)
			ATTACH_VEHICLE_TO_TRAILER(truckYachtPacker, truckYachtPackerTrailer)	
			
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), truckYachtPacker, VS_FRONT_RIGHT)
				WAIT(1000)
				START_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 2, "Fam1Cut")
	
				START_RECORDING_VEHICLE(truckYachtPackerTrailer, 3, "Fam1Cut", TRUE)
	
	
							
				
			iDebugStage = 12
		BREAK
		
		CASE 12
			
//			IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(truckYachtPacker)
//				ATTACH_VEHICLE_TO_TRAILER(truckYachtPacker, truckYachtPackerTrailer)	
//			ENDIF
			
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
			//AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)			
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_N)
					STOP_RECORDING_VEHICLE(truckYachtPackerTrailer)	
					//STOP_RECORDING_VEHICLE(truckYachtPackerTrailer)	
				ENDIF
			ENDIF
			
		BREAK
		
		//Set the uber playback going for set piece recording purposes... etc.
		CASE 13
		
			REQUEST_CHASE_ASSETS()
			REQUEST_VEHICLE_RECORDING(503, "Fam1")				
			REQUEST_VEHICLE_RECORDING(505, "Fam1")		
			WHILE NOT HAVE_CHASE_ASSETS_LOADED()
				WAIT(0)
			ENDWHILE
			WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(503, "Fam1")			
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(505, "Fam1")			
				WAIT(0)
			ENDWHILE
			
			CREATE_CAMERAS_AND_YACHT()
			
			set_uber_parent_widget_group(family1WidgetGroup)
			
			
		
			setupTrafficFamily1()
			START_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 1, "Fam1New")
			
			START_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, 2, "Fam1New")
			//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, ((GET_TIME_POSITION_IN_RECORDING(truckYachtPacker) * -1.0) + 16000.0))
			//
			//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, ((GET_TIME_POSITION_IN_RECORDING(truckYachtPackerTrailer) * -1.0) + 16000.0))
										
			//SET_UBER_PLAYBACK_TO_TIME_NOW(truckYachtPacker, 16000) //11500 + (5500.0 - TIMERA() ) )
			SET_ENTITY_COORDS(truckYachtPackerTrailer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, 3.0, 0.0>>))
			
			cameraCar = CREATE_VEHICLE(PHANTOM, <<0.0, 0.0, 0.0>>)
			cameraCarCamera = CREATE_VEHICLE(PHANTOM, <<0.0, 0.0, 0.0>>)
			
			START_PLAYBACK_RECORDED_VEHICLE(cameraCar, 503, "Fam1")
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), cameraCar)
			SET_ENTITY_INVINCIBLE(cameraCar, TRUE)
			
			SET_CAM_RECORDING_WIDGET_GROUP(camRecData, family1WidgetGroup)
			START_CAM_RECORDING_RELATIVE_TO_ENTITY(camRecData, cameraCar, cameraCarCamera, <<0.1, 1.75, 0.0>>, <<0.0, 0.0, 0.0>>, 29.5, "Fam1Cam", 3, TRUE, TRUE)		
			
			INITIALISE_UBER_PLAYBACK("Fam1", 502, TRUE)
			bUberInit = TRUE
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), truckYachtPacker)
			
			iDebugStage = 14
		BREAK
		CASE 14
			CREATE_ALL_WAITING_UBER_CARS()
			UPDATE_UBER_PLAYBACK(truckYachtPacker, 1.0)
			iDebugStage = 15
		BREAK
		
		CASE 15
		
			UPDATE_UBER_PLAYBACK(truckYachtPacker, 1.0)
		
			UPDATE_CAM_RECORDING(camRecData)
		
		
			
		BREAK
		
		CASE 16
		
			STOP_CAM_RECORDING(camRecData)
		
		BREAK
		
	ENDSWITCH
		
ENDPROC


PED_INDEX tempPed

PROC RECORD_SETPIECES()

	SWITCH iDebugStage
			//Set the uber playback going for set piece recording purposes... etc.
		CASE 0
		
			g_Family1BallerSpawnAfter1stTry = TRUE
		
			REQUEST_CHASE_ASSETS()
			//REQUEST_VEHICLE_RECORDING(503, "Fam1")				
			//REQUEST_VEHICLE_RECORDING(504, "Fam1")		
			WHILE NOT HAVE_CHASE_ASSETS_LOADED()
				WAIT(0)
			ENDWHILE
			//WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(503, "Fam1")			
			//OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(504, "Fam1")			
			//	WAIT(0)
			//ENDWHILE
			
			CREATE_CAMERAS_AND_YACHT()
			
			set_uber_parent_widget_group(family1WidgetGroup)
		
			setupTrafficFamily1()
			
			FREEZE_ENTITY_POSITION(truckYachtPacker, FALSE)
			FREEZE_ENTITY_POSITION(truckYachtPackerTrailer, FALSE)
			
			START_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 1, "Fam1New")
			//START_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, 2, "Fam1New")
		
			
			
			START_RECORDING_VEHICLE(truckYachtPacker, 1, "Fam1New")
			START_RECORDING_VEHICLE(truckYachtPackerTrailer, 2, "Fam1New")
			
			SET_ENTITY_COORDS(truckYachtPackerTrailer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, 3.0, 0.0>>))
			
			ATTACH_VEHICLE_TO_TRAILER(truckYachtPacker, truckYachtPackerTrailer)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -2021.9744, -428.4273, 10.4572 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 49.4800)
			
			RESET_GAME_CAMERA()
			//cameraCar = CREATE_VEHICLE(PHANTOM, <<0.0, 0.0, 0.0>>)
			//cameraCarCamera = CREATE_VEHICLE(PHANTOM, <<0.0, 0.0, 0.0>>)
			
			//START_PLAYBACK_RECORDED_VEHICLE(cameraCar, 504, "Fam1")
			
			INITIALISE_UBER_PLAYBACK("Fam1", 502, TRUE)
			bUberInit = TRUE
			setupTrafficFamily1()
			
			tempPed = GET_PED_IN_VEHICLE_SEAT(truckYachtPacker)
			
			DELETE_PED(tempPed)
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), truckYachtPacker)
						
			
			ADD_BLIP_FOR_ENTITY(truckYachtPacker)
			SET_ENTITY_LOD_DIST(truckYachtPacker, 3000)
			iDebugStage = 1
		BREAK
		
		CASE 1
			CREATE_ALL_WAITING_UBER_CARS()
			UPDATE_UBER_PLAYBACK(truckYachtPacker, 1.0)
			iDebugStage = 2
		BREAK
		
		CASE 2
		
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				IF NOT IS_ENTITY_DEAD(truckYachtPacker)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPacker)
						STOP_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPackerTrailer)
						STOP_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer)
					ENDIF			
				ENDIF			
			ENDIF
		
			
//			IF (GET_TIME_POSITION_IN_RECORDING(truckYachtPacker) - GET_TIME_POSITION_IN_RECORDING(truckYachtPackerTrailer)) <> 0.0
//				SCRIPT_ASSERT("boom")
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, (GET_TIME_POSITION_IN_RECORDING(truckYachtPackerTrailer) * -1) + GET_TIME_POSITION_IN_RECORDING(truckYachtPacker) )
//			
//			ENDIF
//			UPDATE_UBER_PLAYBACK(truckYachtPacker, 1.0)			
//			
//			MANAGE_TRAILER_CREATION()
			
		BREAK
		
		CASE 3
			STOP_RECORDING_ALL_VEHICLES()
			CLEANUP_UBER_PLAYBACK()
			bUberInit = FALSE
		BREAK
	
	ENDSWITCH
	
ENDPROC


#ENDIF

INT iTimeOfBlipping
BOOL bOldRouteOff = FALSE

PROC MANAGE_CUSTOM_GPS_ROUTE()
	
	IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
	//Plot manual GPS route
		IF NOT bCustomGPSActive		
			START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
			ADD_POINT_TO_GPS_MULTI_ROUTE(<< -1461.1736, -85.7604, 49.9523 >>)
			//ADD_POINT_TO_GPS_MULTI_ROUTE(<<-1934.1880, -169.5724, 33.7543>>)
			ADD_POINT_TO_GPS_MULTI_ROUTE(vMissionLocation)
			SET_GPS_MULTI_ROUTE_RENDER(TRUE)
			iTimeOfBlipping = GET_GAME_TIMER()
			bOldRouteOff = FALSE
			bCustomGPSActive = TRUE
		ENDIF
		
		IF bCustomGPSActive = TRUE
		AND GET_GAME_TIMER() - iTimeOfBlipping > 10000
		AND bOldRouteOff = FALSE
			SET_BLIP_ROUTE(sLocatesData.GPSBlipID, FALSE)
			bOldRouteOff = TRUE
		ENDIF
	ELSE
		IF bCustomGPSActive
			SET_GPS_MULTI_ROUTE_RENDER(FALSE)
			CLEAR_GPS_MULTI_ROUTE()
			bCustomGPSActive = FALSE
		ENDIF
	ENDIF
	
ENDPROC

//***************************************************************
//****************** MISSION STAGES *****************************
//***************************************************************

BOOL bCallRejected = FALSE
BOOL bFranklinLookingAtPlayerFlag = FALSE

PROC stageGetIntoAmandasCar()

	failConditionsPreChase()
	checkForDistanceFromFranklin()

	
	IF NOT IS_ENTITY_DEAD(pedFranklin())
		IF NOT IS_ENTITY_IN_ANGLED_AREA(pedFranklin(), <<-809.901306,184.739029,71.540131>>, <<-812.344238,190.597275,74.042068>>, 8.250000)
			SET_PED_IS_IGNORED_BY_AUTO_OPEN_DOORS(pedFranklin(), TRUE)
		ENDIF
		SET_PED_RESET_FLAG(pedFranklin(), PRF_SearchForClosestDoor, TRUE )
		
		SET_DOOR_STATE(DOORNAME_M_MANSION_G1, DOORSTATE_FORCE_OPEN_THIS_FRAME)
		
	ENDIF

	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
		AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), carPlayersJeep, <<12.0, 12.0, 5.0>>, FALSE)
		AND TIMERA() < 4500
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		ENDIF
	ENDIF

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
	SET_BIT(sLocatesData.iLocatesBitSet, BS_BUDDIES_WILL_ONLY_GET_IN_WHEN_PLAYER_DOES)

	
	SWITCH iGetIntoCarStage

		CASE 0
		
			SET_PED_CONFIG_FLAG(pedFranklin(), PCF_OpenDoorArmIK, TRUE )
			
			SET_PED_DROPS_WEAPONS_WHEN_DEAD(pedFranklin(), FALSE)
			SET_CURRENT_PED_VEHICLE_WEAPON(pedFranklin(), WEAPONTYPE_UNARMED)
			
			requestAndCreateInitialStuff()
				
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				TASK_LOOK_AT_ENTITY(pedFranklin(), PLAYER_PED_ID(), 30000)
				SET_ENTITY_COORDS(carPlayersJeep, vJeepStartPosition)
				SET_ENTITY_HEADING(carPlayersJeep, fJeepStartHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
			ENDIF
				
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), carPlayersJeep, 30000)	
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				FREEZE_ENTITY_POSITION(carPlayersJeep, FALSE)
			ENDIF
			REMOVE_CUTSCENE() //Remove the introcutscen from memory if not already cleared - 406579
			//Set_Replay_Mid_Mission_Stage(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_GET_INTO_AMANDAS_CAR)) 
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_GET_INTO_AMANDAS_CAR")
			sLocatesData.vehicleBlip = CREATE_BLIP_FOR_VEHICLE(carPlayersJeep)
			SET_BLIP_AS_FRIENDLY(sLocatesData.vehicleBlip, TRUE)
						
			STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
			SET_BIT(sLocatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
						
			SET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_TRACEY)
			SET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_AMANDA)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(carTracey)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(carMichael)
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "JIMMY")
			
			SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedFranklin(), VS_FRONT_RIGHT)
			SET_GROUP_FORMATION_SPACING(GET_PLAYER_GROUP(PLAYER_ID()), 3.5)
			
			SET_PED_AS_GROUP_MEMBER(pedFranklin(), GET_PED_GROUP_INDEX(PLAYER_PED_ID()))
			
			bCallRejected = FALSE
						
			SETTIMERA(0)
			
			iGetIntoCarStage++		
		BREAK
		
		CASE 1
			IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_ACAR", CONV_PRIORITY_VERY_HIGH)
				iGetIntoCarStage++
			ENDIF
		BREAK
		
		CASE 2		
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, TRUE)
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					IF PLAYER_CALL_CHAR_CELLPHONE(myScriptedSpeech, CHAR_JIMMY, "FAM1AUD", "FAM1_PHONEX", CONV_PRIORITY_VERY_HIGH, TRUE,  DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
						iGetIntoCarStage++
					ENDIF
				ENDIF
			ELSE
				iGetIntoCarStage++
			ENDIF
		BREAK
	
		CASE 3
			
			//Has the phone been hung up before it could be answered?
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF CHECK_CELLPHONE_LAST_CALL_REJECTED() 
				OR WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
				OR HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()
					IF bCallRejected = FALSE
						SETTIMERB(0)
						bCallRejected = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF bCallRejected = TRUE
			AND TIMERB() > 2000 
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_JIMMY, "YB_JIMTXT", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
			ENDIF
			
			IF (TIMERB() > 1000 AND NOT IS_SCRIPTED_CONVERSATION_ONGOING() AND bCallRejected = FALSE)
			OR (TIMERB() > 4000 AND bCallRejected = TRUE)
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CHANGE", CONV_PRIORITY_VERY_HIGH)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iGetIntoCarStage++
				ENDIF
			ENDIF
		BREAK
	
		CASE 4
			IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HELPOUT", CONV_PRIORITY_VERY_HIGH)
				iGetIntoCarStage++
			ENDIF
		BREAK
	
		CASE 5
		
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF NOT IS_REPLAY_IN_PROGRESS()
					PRINT_NOW("YB_GETIN", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
				iGetIntoCarStage++
			ENDIF
			
		BREAK
		
		CASE 6
		
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
				AND NOT IS_PED_INJURED(pedFranklin())
								
					//playersGroup = GET_PLAYER_GROUP(PLAYER_ID())
					//SET_PED_AS_GROUP_MEMBER(pedFranklin(), playersGroup)
					//SET_BLIP_DISPLAY(blipPlayersCar, DISPLAY_NOTHING)
					//REMOVE_BLIP(blipPlayersCar)
					SET_PED_CONFIG_FLAG(pedFranklin(), PCF_OpenDoorArmIK, FALSE)
					mission_Stage = STAGE_GET_TO_LOCATION
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), carPlayersJeep, <<8.0, 8.0, 8.0>>, FALSE)
		IF NOT IS_PED_INJURED(pedFranklin())
			IF bFranklinLookingAtPlayerFlag = FALSE
				TASK_LOOK_AT_ENTITY(pedFranklin(), PLAYER_PED_ID(), 60000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
				bFranklinLookingAtPlayerFlag = TRUE
			ENDIF
		ENDIF
	ENDIF
	
//	IF iGetIntoCarStage < 4
//		SET_PED_MAX_MOVE_BLEND_RATIO(pedFranklin(), PEDMOVE_WALK)
//	ELSE	
//		SET_PED_MAX_MOVE_BLEND_RATIO(pedFranklin(), PEDMOVE_RUN)
//	ENDIF
	
	IF TIMERA() > 0
		IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, vMissionLocation, <<0.0001, 0.0001, 2.01>>, FALSE, pedFranklin(), NULL, NULL, carPlayersJeep, "YB_GETTO", "CMN_FLEAVE", "", "", "CMN_FLEAVE", "YB_GETIN", "CMN_GENGETBCK", FALSE, TRUE) //, NULL, TRUE)	
	ENDIF
	
	MANAGE_CUSTOM_GPS_ROUTE()
	
ENDPROC


FUNC BOOL IS_PLAYER_ON_BACK_ROAD()

	RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2209.346924,-344.320313,12.567600>>, <<-2493.139160,-201.687546,55.299278>>, 80.250000)

ENDFUNC

/// PURPOSE:
///    Check if the player is in an edge-case trigger
/// PARAMS:
///    bDoBuddyAndVehicleCheck - TRUE: Only return TRUE if the player and Franklin are inside the mission vehicle and inside an edge case trigger.  
/// RETURNS:
///    TRUE if player is in an edge case trigger (RETURN value also depends on value of 'bDoBuddyAndVehicleCheck')
FUNC BOOL IS_PLAYER_IN_YACHT_EDGE_CASE_TRIGGER(BOOL bDoBuddyAndVehicleCheck = TRUE)

	BOOL bIsPlayerInEdgeCaseTrigger

	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2036.1, -356.0, 50.3>>, <<-2035.1, -382.8, 42.7>>, 30.0) AND IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(truckYachtPackerTrailer), 1.0) //Covering two parking lot balconies overlooking freeway
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2003.1, -323.0, 50.3>>, <<-2042.9, -375.5, 47.0>>, 30.0) AND IS_SPHERE_VISIBLE((GET_ENTITY_COORDS(truckYachtPackerTrailer) + <<0.0, 0.0, -90.0>>), 1.0) //Top level parking lot overlooking freeway
//	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2003.1, -323.0, 47.3>>, <<-2042.9, -375.5, 43.0>>, 30.0) AND IS_SPHERE_VISIBLE((GET_ENTITY_COORDS(truckYachtPackerTrailer) + <<0.0, 0.0, -40.0>>), 1.0) //Lower Level parking lot overlooking freeway
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1928.2, -425.3, 27.5>>, <<-2063.9, -634.6, 0.04>>, 10.0) AND IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(truckYachtPackerTrailer), 7.0) //From freeway onramp to beach
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2443.9, -248.3, 15.0>>, <<-2420.8, -226.7, 20.04>>, 40.0) AND IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(truckYachtPackerTrailer), 7.0) //From freeway going the same direction as the yacht (in case player goes over the mountains and attempts to flank the yacht)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2222.1, -225.3, 90.0>>, <<-2430.8, -140.7, 10.04>>, 10.0) AND IS_SPHERE_VISIBLE((GET_ENTITY_COORDS(truckYachtPackerTrailer) + <<0.0, 0.0, -8.0>>), 7.0) //Mountain ridge overlooking freeway
		
		bIsPlayerInEdgeCaseTrigger = TRUE
		
		IF bDoBuddyAndVehicleCheck
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep) AND IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
				bIsPlayerInEdgeCaseTrigger = TRUE
			ELSE
				bIsPlayerInEdgeCaseTrigger = FALSE
			ENDIF
		ENDIF		
			
	ENDIF
	
	RETURN bIsPlayerInEdgeCaseTrigger
	
ENDFUNC

/// PURPOSE:
///    Adjust the Yacht's position depending on which edge-case trigger the player is in in order to reduce the likelyhood the player will spot the Yacht suddenly popping into view.
PROC UPDATE_YACHT_POS_TO_MATCH_PLAYER_EDGE_CASE_TRIGGER()

	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2036.1, -357.0, 50.3>>, <<-2035.1, -382.8, 42.7>>, 30.0) //Parking Lot
		PRINTLN("IN ANGLED AREA: Parking Lot")
		cutsceneSetYachtAbsolute(9000)
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2222.1, -240.3, 90.0>>, <<-2430.8, -140.7, 10.04>>, 10.0) //Mountains
		PRINTLN("IN ANGLED AREA: Mountains")
		cutsceneSetYachtAbsolute(5500)
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1928.2, -425.3, 27.5>>, <<-2063.9, -634.6, 0.04>>, 10.0) //Freeway Onramp
		PRINTLN("IN ANGLED AREA: Freeway Onramp")
		cutsceneSetYachtAbsolute(13100)
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2443.9, -248.3, 15.0>>, <<-2420.8, -226.7, 20.04>>, 20.0)//Freeway Behind boat
		PRINTLN("IN ANGLED AREA: Freeway Behind boat")
		cutsceneSetYachtAbsolute(16200)
	ENDIF
	
ENDPROC

TEXT_LABEL_23 currentRootLabel
BOOL bMastConvoPlayingOrPlayed
BOOL bBanterFinishedEarly = FALSE
BOOL bSawBoatConvoPreloaded = FALSE
				
PROC stageGetToLocation()

	failConditionsPreChase()
	checkForDistanceFromFranklin()
	
	//SET_PED_IS_IGNORED_BY_AUTO_OPEN_DOORS(PLAYER_PED_ID(), TRUE)
	IF NOT IS_ENTITY_DEAD(pedFranklin())
		SET_PED_IS_IGNORED_BY_AUTO_OPEN_DOORS(pedFranklin(), TRUE)
	ENDIF
	
	SET_BIT(sLocatesData.iLocatesBitSet, BS_BUDDIES_WILL_ONLY_GET_IN_WHEN_PLAYER_DOES)

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMissionLocation, <<130.0, 130.0, 30.0>>, FALSE)
	OR IS_PLAYER_ON_BACK_ROAD()
		IF bStartAudioScene = FALSE
			TRIGGER_MUSIC_EVENT("FAM1_FADE_RADIO")
			CLEAR_AREA(vMissionLocation, 20.0, TRUE)
			bStartAudioScene = TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMissionLocation, <<190.0, 190.0, 30.0>>, FALSE)
	OR IS_PLAYER_ON_BACK_ROAD()
	OR bPreStreamCut1 = TRUE		
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	ENDIF
	

	IF NOT bPreStreamCut2
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMissionLocation, <<350.0, 350.0, 60.0>>, FALSE)
		OR IS_PLAYER_ON_BACK_ROAD()
			REQUEST_CHASE_INITIAL_ASSETS()
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0, TRUE)
			bPreStreamCut2 = TRUE
		ENDIF		
	ENDIF

	IF NOT bPreStreamCut1
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMissionLocation, <<300.0, 300.0, 60.0>>, FALSE)
		OR IS_PLAYER_ON_BACK_ROAD()
			REQUEST_CHASE_ASSETS()
			bPreStreamCut1 = TRUE
		ENDIF
	ENDIF
	
	
	IF bUberRecReadyToGo = FALSE
	AND bPreStreamCut1 = TRUE
	AND bPreStreamCut2 = TRUE
		IF HAVE_CHASE_ASSETS_LOADED()	//If player is in an area that can see the car....
		//AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2315.712158,-329.049561,1.591853>>, <<-2022.748901,-413.252106,41.035339>>, 62.000000) 	//Juntction along PCF
		//AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2169.748779,-328.356964,11.179832>>, <<-2148.260986,-232.740372,27.960501>>, 81.500000)	//Junction road down to PCF
		//AND NOT IS_SPHERE_VISIBLE(<<-2375.4468, -308.6183, 19.1580>>, 1.0)//Point further back up the road //vTruckYachtPackerStartPosition, 1.0)
		//AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMissionLocation, <<200.0001, 200.0001, 24.01>>)
		AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			setupTrafficFamily1()
			CREATE_CAMERAS_AND_YACHT()
			cutsceneSetYachtAbsolute()
		
			currentRootLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			PRINTLN("FAM1_BANT: ? :",currentRootLabel)
			IF ARE_STRINGS_EQUAL(currentRootLabel, "FAM1_BANT")
				KILL_FACE_TO_FACE_CONVERSATION()
				bBanterFinishedEarly = TRUE
			ENDIF
		
			bUberRecReadyToGo = TRUE
		ENDIF
	ENDIF
		
	IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, vMissionLocation, <<0.0001, 0.0001, 2.01>>, FALSE, pedFranklin(), NULL, NULL, carPlayersJeep, "YB_GETTO", "CMN_FLEAVE", "", "", "CMN_FLEAVE", "YB_GETIN", "CMN_GENGETBCK", FALSE, TRUE) //, NULL, TRUE)	
	OR IS_PLAYER_IN_YACHT_EDGE_CASE_TRIGGER()
	OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2118.952637,-224.515121,13.085728>>, <<-2150.723145,-279.301636,24.510395>>, 53.500000))
	OR (DOES_ENTITY_EXIST(truckYachtPackerTrailer) 
//	OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip)
//		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2206.664307,-368.100800,10.999576>>, <<-2370.630127,-279.892090,16.897869>>, 39.750000))	//Back road trigger
//		
		AND IS_ENTITY_ON_SCREEN(truckYachtPackerTrailer)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2131.759033,-242.856400,11.266136>>, <<-2153.037842,-283.376709,19.493343>>, 30.250000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2136.612549,-234.874298,11.222314>>, <<-1999.356201,-161.102127,35.768311>>, 64.500000)
		AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMissionLocation, <<130.0, 130.0, 30.0>>, FALSE))
		OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2206.664307,-368.100800,10.999576>>, <<-2370.630127,-279.892090,16.897869>>, 39.750000))//backroad
			IF bUberRecReadyToGo = TRUE
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
				
				IF IS_PLAYER_IN_YACHT_EDGE_CASE_TRIGGER()
					UPDATE_YACHT_POS_TO_MATCH_PLAYER_EDGE_CASE_TRIGGER()
				ENDIF

				STOP_AUDIO_SCENE("FAMILY_1_DRIVE_TO_HIGHWAY")		
				UNPAUSE_UBER_CARS()
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							
				//KILL_FACE_TO_FACE_CONVERSATION()			
				mission_stage = STAGE_DO_CHASE //STAGE_YACHT_GOES_BY_ON_PCH
			ENDIF
	ENDIF
	
	//Deal with GPS
	MANAGE_CUSTOM_GPS_ROUTE()
	
	//Deal with triggering banter
	
	SWITCH iGetToLocation
	
		CASE 0
			bBanterFinishedEarly = FALSE
			bSawBoatConvoPreloaded = FALSE
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)							
				IF GET_CONVERTIBLE_ROOF_STATE(carPlayersJeep) <> CRS_LOWERED
				AND GET_CONVERTIBLE_ROOF_STATE(carPlayersJeep) <> CRS_LOWERING
					LOWER_CONVERTIBLE_ROOF(carPlayersJeep, FALSE)
				ENDIF
			ENDIF
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING) 
			START_AUDIO_SCENE("FAMILY_1_DRIVE_TO_HIGHWAY")
			
			REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_01")
			REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_02")
			REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
			//REQUEST_SCRIPT_AUDIO_BANK("FAMILY_1_BOAT_STRAIN")
			
			bPreStreamCut1 = FALSE
			bPreStreamCut2 = FALSE
			//Jimmy phones...
			
			//PRINT_NOW("YB_GETTO", DEFAULT_GOD_TEXT_TIME, 1)
			iGetToLocation++
		BREAK
	
		CASE 1
			//Preload banter (maybe buys us 7 seconds)
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF PRELOAD_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_BANT", CONV_PRIORITY_VERY_HIGH)
					iGetToLocation++	
				ENDIF
			ENDIF
		BREAK
	
		CASE 2
			//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON())
			OR (NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON())
				IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedFranklin())
					
					BEGIN_PRELOADED_CONVERSATION()
					//IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_BANT", CONV_PRIORITY_VERY_HIGH)
						iGetToLocation++
					//ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
					
			//preload next conversation just in case...
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF PRELOAD_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_START", CONV_PRIORITY_VERY_HIGH)
					iGetToLocation++
				ENDIF
			ENDIF
		BREAK
			
	ENDSWITCH

	//Deal with conversation pausing/unpausing
	IF NOT IS_ENTITY_DEAD(carPlayersJeep)	
		IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
		AND mission_stage <> STAGE_DO_CHASE //iGetToLocation = 3
			IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
			ENDIF
		ELSE
			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedFranklin())
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
		
ENDPROC

//BOOL bCutsceneUberSetup

//PROC stageYachtGoesByOnPchCutscene()
//
//
//	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//	AND iQuickCutsceneStage  >= 1
//		STOP_CUTSCENE()
//		DO_SCREEN_FADE_OUT(500)
////		WHILE IS_SCREEN_FADING_OUT()
////			WAIT(0)
////		ENDWHILE
////		
////		WHILE NOT HAS_CUTSCENE_FINISHED()
////			WAIT(0)
////		ENDWHILE
//			
////		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
////		AND NOT IS_ENTITY_DEAD(pedFranklin())
////			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
////			SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
////		ENDIF
////		
////		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
////			SET_ENTITY_COORDS(carPlayersJeep, << -2167.4087, -323.8105, 12.5601 >>)
////			SET_ENTITY_HEADING(carPlayersJeep, 169.9605)
////						
////			IF GET_CONVERTIBLE_ROOF_STATE(carPlayersJeep) <> CRS_LOWERED
////			AND GET_CONVERTIBLE_ROOF_STATE(carPlayersJeep) <> CRS_LOWERING
////				LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
////			ENDIF
////
////		ENDIF
//		
//		CREATE_ALL_WAITING_UBER_CARS()
//		UPDATE_UBER_PLAYBACK(truckYachtPacker, 1.0)
//		
//		REQUEST_CHASE_ASSETS()
//	
//		WHILE NOT HAVE_CHASE_ASSETS_LOADED()
//			WAIT(0)
//		ENDWHILE
//		
//		SET_CAM_COORD(destinationCam,<<-2171.767578,-323.419983,12.775518>>)
//		SET_CAM_ROT(destinationCam,<<2.591985,1.150317,-159.791214>>)
//		SET_CAM_FOV(destinationCam,45.000000)
//
//		SET_CAM_ACTIVE(destinationCam, TRUE)
//			
//						
//		SET_CUTSCENE_RUNNING(TRUE)
//		bCarBlendingIntoGamePlayAfterCut = TRUE
//		iQuickCutsceneStage = 3 //Exit cutscene in same manner as normal
//	ENDIF
//
//	
//	SET_USE_HI_DOF()
//
//	SWITCH iQuickCutsceneStage
//	
//	
//		CASE 0
//
//			KILL_ANY_CONVERSATION()
//
//			REQUEST_CUTSCENE("Family_1_mcs_1")
//			// Load the models	
//		
//			REQUEST_CHASE_ASSETS()
//					
//			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//				
//				SET_ENTITY_LOD_DIST(carPlayersJeep, 1000)
//								
//				IF NOT IS_ENTITY_DEAD(pedFranklin())
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
//					ENDIF
//					IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
//						SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
//					ENDIF
//				ENDIF
//				
//				IF GET_CONVERTIBLE_ROOF_STATE(carPlayersJeep) <> CRS_LOWERED
//				AND GET_CONVERTIBLE_ROOF_STATE(carPlayersJeep) <> CRS_LOWERING
//					LOWER_CONVERTIBLE_ROOF(carPlayersJeep)
//				ENDIF
//			ENDIF
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//			bCutsceneUberSetup = FALSE
//			bMusicStarted = FALSE
//			iQuickCutsceneStage++
//		BREAK
//		
//		CASE 1	
//			IF  HAS_CUTSCENE_LOADED()
//			AND HAVE_CHASE_ASSETS_LOADED()
//				iQuickCutsceneStage++
//			ENDIF
//		BREAK
//	
//		CASE 2
//			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//				REGISTER_ENTITY_FOR_CUTSCENE(carPlayersJeep, "Amandas_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//			ENDIF
//			//REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "PLAYER_ZERO", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//			IF NOT IS_ENTITY_DEAD(pedFranklin())
//				REGISTER_ENTITY_FOR_CUTSCENE(pedFranklin(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//			ENDIF
//			
//			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//				REGISTER_ENTITY_FOR_CUTSCENE(carPlayersJeep, "SENTINEL2", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//			ENDIF
//			//REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "PLAYER_ZERO", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//			IF NOT IS_ENTITY_DEAD(pedFranklin())
//				REGISTER_ENTITY_FOR_CUTSCENE(pedFranklin(), "PLAYER_ONE", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//			ENDIF
//			//SET_RADIO_POSITION_AUDIO_MUTE(TRUE)
//			START_AUDIO_SCENE("MUTES_RADIO_SCENE")
//			START_CUTSCENE()
//			
//			iQuickCutsceneStage++
//		BREAK
//		
//		CASE 3
//		
//			RESET_GAME_CAMERA()
//			
//			IF IS_CUTSCENE_PLAYING()
//					
//				IF bMusicStarted = FALSE
//				AND GET_CUTSCENE_TIME() > 9000.0
//				
//					IF PREPARE_MUSIC_EVENT("FAM1_START")
//						IF TRIGGER_MUSIC_EVENT("FAM1_START")
//							bMusicStarted = TRUE
//						ENDIF
//					ENDIF
//				ENDIF	
//			ENDIF
//			
//			IF IS_CUTSCENE_PLAYING()
//			AND bCutsceneUberSetup = FALSE
//				CLEAR_AREA(<<-2170.208496,-328.865112,12.963207>>, 55.00, TRUE, TRUE)
//				STOP_FIRE_IN_RANGE(<<-2170.208496,-328.865112,12.963207>>, 100.0)
//				cutsceneSetYachtAbsolute()
//				WAIT(0)
//				
////				SET_CAM_COORD(destinationCam,<<-2171.767578,-323.419983,12.775518>>)
////				SET_CAM_ROT(destinationCam,<<2.591985,1.150317,-159.791214>>)
////				SET_CAM_FOV(destinationCam,45.000000)
////
////				SET_CAM_ACTIVE(destinationCam, TRUE)
//			
//				SET_CAM_COORD(destinationCam,<< -2165.8325, -318.0308, 13.7444 >>)
//				SET_CAM_ROT(destinationCam,<< -1.2550, 0.0000, 169.9557 >>)
//				SET_CAM_FOV(destinationCam,50.000000)
//
//				SET_CAM_ACTIVE(destinationCam, TRUE)
//			
//						
//				bCutsceneUberSetup = TRUE
//			ENDIF
//			
//			//Keep them out of trouble as the cutscene does not update car recordings when playing.
//			IF NOT IS_ENTITY_DEAD(truckYachtPacker)
//			AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
//				SET_ENTITY_COORDS(truckYachtPacker, << -2319.9204, -321.5558, 12.8060 >>)
//				SET_ENTITY_HEADING(truckYachtPacker,  239.5773)
//			
//				SET_ENTITY_COORDS(truckYachtPackerTrailer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, -7.0, 0.0>>))
//				SET_ENTITY_HEADING(truckYachtPackerTrailer,  239.5773)
//			ENDIF
//			
//			CREATE_ALL_WAITING_UBER_CARS()
//			UPDATE_UBER_PLAYBACK(truckYachtPacker, 1.0)
//			
//			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Amandas_car")
//				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//					SET_ENTITY_COORDS(carPlayersJeep, << -2167.4087, -323.8105, 12.5601 >>)
//					SET_ENTITY_HEADING(carPlayersJeep, 169.9605)
//					SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)			
//				
//									//SCRIPT_ASSERT("boom")
////					START_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 1, "Fam1Blend")
////					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 2000)
////					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(carPlayersJeep)
////					SET_PLAYBACK_SPEED(carPlayersJeep, 1.6)
//					bCarBlendingIntoGamePlayAfterCut = TRUE
//				ENDIF
//			ENDIF
//			
//			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//			
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
//				AND NOT IS_ENTITY_DEAD(pedFranklin())
//					SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedFranklin())
//				ENDIF
//				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
//					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//					RESET_GAME_CAMERA()
//						
//					SET_CUTSCENE_RUNNING(TRUE)
//							
//					UNPAUSE_UBER_CARS()
//					WAIT(0)
//				ENDIF
//			ENDIF
//						
//			IF HAS_CUTSCENE_FINISHED()	
//						
//				IF bCutsceneUberSetup = FALSE
//					cutsceneSetYachtAbsolute()
//					bCutsceneUberSetup = TRUE
//				ENDIF
//				
//				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//				AND NOT IS_ENTITY_DEAD(pedFranklin())
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
//					ENDIF
//					IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
//						SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
//					ENDIF
//				ENDIF
//		
//				IF NOT IS_ENTITY_DEAD(truckYachtPacker)
//				AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
//				AND NOT IS_ENTITY_DEAD(carPlayersJeep)
//					
//					LOWER_CONVERTIBLE_ROOF(carPlayersJeep, TRUE)
//					
//					//cutsceneSetYachtAbsolute()
//									
//					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), truckYachtPackerTrailer, 9000)
//					IF NOT IS_PED_INJURED(pedFranklin())
//						TASK_LOOK_AT_ENTITY(pedFranklin(), truckYachtPackerTrailer, 30000)
//					ENDIF
//					//CREATE_ALL_WAITING_UBER_CARS()
//					//UPDATE_UBER_PLAYBACK(truckYachtPacker, 1.0)
//					UNPAUSE_UBER_CARS()
//					
////					//SCRIPT_ASSERT("boom")
////					START_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 1, "Fam1Blend")
////					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 2000)
////					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(carPlayersJeep)
////					SET_PLAYBACK_SPEED(carPlayersJeep, 1.6)
////					bCarBlendingIntoGamePlayAfterCut = TRUE
//							
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)	
//					SET_CUTSCENE_RUNNING(FALSE, TRUE, 2000)
//					SET_CAM_ACTIVE(destinationCam, FALSE)
//					
//				ENDIF
//								
//				iQuickCutsceneStage++
//
//				KILL_ANY_CONVERSATION()
//				
//				RESET_GAME_CAMERA()
//				
//				
//				ADD_BLIP_FOR_YACHT()			
//				//SET_RADIO_POSITION_AUDIO_MUTE(FALSE)
//				STOP_AUDIO_SCENE("MUTES_RADIO_SCENE")
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			
//				IF IS_SCREEN_FADED_OUT()	
//					WAIT(250)
//					DO_SCREEN_FADE_IN(500)
//				ENDIF
//			
//				mission_stage = STAGE_DO_CHASE
//			ENDIF
//		BREAK
//	
//	ENDSWITCH
//
//ENDPROC

FUNC BOOL IS_PLAYER_PUSHING_ANALOGUE_STICKS()

	INT padLX, padLy, padRX, padRy

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(padLX, padLy, padRX, padRy)
	
	IF ABSI(padLX) > 75
	OR ABSI(padLY) > 75
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC dealWithFranklinsCar()

	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
			IF NOT bFlagGetBackInCar
			
				IF NOT DOES_BLIP_EXIST(blipPlayersCar)
					blipPlayersCar = CREATE_BLIP_FOR_VEHICLE(carPlayersJeep, FALSE)
				ENDIF				
				SET_BLIP_DISPLAY(blipPlayersCar, DISPLAY_BOTH)
				
				IF DOES_BLIP_EXIST(blipMichaelSon)		
					bDisplayBlipMichaelSon = GET_BLIP_INFO_ID_DISPLAY(blipMichaelSon)
					SET_BLIP_DISPLAY(blipMichaelSon, DISPLAY_NOTHING)
				ENDIF
				IF DOES_BLIP_EXIST(blippedBadguy2)		
					bDisplayBlippedBadguy2 = GET_BLIP_INFO_ID_DISPLAY(blippedBadguy2)
					SET_BLIP_DISPLAY(blippedBadguy2, DISPLAY_NOTHING)
				ENDIF
				IF DOES_BLIP_EXIST(blippedBadGuy3BoxThrower)		
					bDisplayBlippedBadGuy3BoxThrower = GET_BLIP_INFO_ID_DISPLAY(blippedBadGuy3BoxThrower)
					SET_BLIP_DISPLAY(blippedBadGuy3BoxThrower, DISPLAY_NOTHING)
				ENDIF
//				IF DOES_BLIP_EXIST(blipTruckYachtPacker)		
//					bDisplayBlipTruckYachtPacker = GET_BLIP_INFO_ID_DISPLAY(blipTruckYachtPacker)
//					SET_BLIP_DISPLAY(blipTruckYachtPacker, DISPLAY_NOTHING)
//				ENDIF
				IF DOES_BLIP_EXIST(blipTruckYachtPackerTrailer)		
					bDisplayBlipTruckYachtPackerTrailer = GET_BLIP_INFO_ID_DISPLAY(blipTruckYachtPackerTrailer)
					SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_NOTHING)
				ENDIF
												
				IF bFlagGetBackInText = FALSE
					PRINT_NOW("CMN_GENGETBCK", DEFAULT_GOD_TEXT_TIME, 1)
					bFlagGetBackInText = TRUE
				ENDIF
				CLEAR_HELP()
				bFlagGetBackInCar = TRUE
			ENDIF
			
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
			
		ELSE
			IF bFlagGetBackInCar
				IF DOES_BLIP_EXIST(blipPlayersCar)		
					SET_BLIP_DISPLAY(blipPlayersCar, DISPLAY_NOTHING)
				ENDIF
				IF DOES_BLIP_EXIST(blipMichaelSon)		
					SET_BLIP_DISPLAY(blipMichaelSon, bDisplayBlipMichaelSon)
				ENDIF
				IF DOES_BLIP_EXIST(blippedBadguy2)		
					SET_BLIP_DISPLAY(blippedBadguy2, bDisplayBlippedBadguy2)
				ENDIF
				IF DOES_BLIP_EXIST(blippedBadGuy3BoxThrower)		
					SET_BLIP_DISPLAY(blippedBadGuy3BoxThrower, bDisplayBlippedBadGuy3BoxThrower)
				ENDIF
//				IF DOES_BLIP_EXIST(blipTruckYachtPacker)		
//					SET_BLIP_DISPLAY(blipTruckYachtPacker, bDisplayBlipTruckYachtPacker)
//				ENDIF
				IF DOES_BLIP_EXIST(blipTruckYachtPackerTrailer)		
					SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, bDisplayBlipTruckYachtPackerTrailer)
				ENDIF
				IF IS_THIS_PRINT_BEING_DISPLAYED("CMN_GENGETBCK")
					CLEAR_PRINTS()
				ENDIF
				bFlagGetBackInCar = FALSE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//

INT iTimeBoomBaddieAppears
BOOL bTrackStat = FALSE
INT iTimeOfJimmyDropping
INT iTimeOfJimmyBeingHit
BOOL bStopBashing = FALSE

PROC updateBoomBaddie()

	SWITCh eBoomBaddie
	
		CASE BOOMBADDIE_CREATE_AND_ANIMATE_INTRO

			//IF NOT IS_PED_INJURED(pedBadguy4BoomGuy)
				//SCRIPT_ASSERT("case 0")
				
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND GET_GAME_TIMER() - iTimeOfBoxThrowerShot > 2200
				bTrackStat = FALSE
				pedBadguy4BoomGuy = CREATE_PED(PEDTYPE_MISSION, G_M_Y_SalvaGoon_01, <<723.9294, -1087.4998, 21.1693>>, 96.2191)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedBadguy4BoomGuy, "FAMILY_1_BOAT_PEDS")
				//SET_PED_COMPONENT_VARIATION(pedBadguy4BoomGuy, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				//SET_PED_COMPONENT_VARIATION(pedBadguy4BoomGuy, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
				//SET_PED_COMPONENT_VARIATION(pedBadguy4BoomGuy, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				//SET_PED_COMPONENT_VARIATION(pedBadguy4BoomGuy, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(pedBadguy4BoomGuy, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedBadguy4BoomGuy, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedBadguy4BoomGuy, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedBadguy4BoomGuy, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBadguy4BoomGuy, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedBadguy4BoomGuy, thievesRelGroup)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedBadguy4BoomGuy, FALSE)
							
				sceneIDboomGuy = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIDboomGuy, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "chassis") )
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, pedBadguy4BoomGuy, "YACHTPED3")	
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDboomGuy, FALSE)
				SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDboomGuy, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				
				TASK_SYNCHRONIZED_SCENE (pedBadguy4BoomGuy, sceneIDboomGuy, "MISSFAM1_YACHTBATTLEonYacht02_", "onYacht_getOnBoom_thug", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				
				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(pedBadguy4BoomGuy)
				
				iTimeBoomBaddieAppears = GET_GAME_TIMER()
				STOP_ENTITY_ANIM(truckYachtPackerTrailer, "onYacht_hitByBoom_boom", "MISSFAM1_YACHTBATTLEonYacht02_", INSTANT_BLEND_OUT)	
				PLAY_ENTITY_ANIM(truckYachtPackerTrailer, "Yacht_Door_Opening_mastOUT", "MISSFAM1_YachtBattle", INSTANT_BLEND_IN, FALSE, FALSE, TRUE)
				SWING_BOAT_BOOM_FREELY(truckYachtPackerTrailer, TRUE)
				ALLOW_BOAT_BOOM_TO_ANIMATE(truckYachtPackerTrailer, TRUE)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(truckYachtPackerTrailer)
				eBoomBaddie = BOOMBADDIE_WAIT_FOR_INTRO_TO_FINISH
			ENDIF
		
		BREAK
		
		CASE BOOMBADDIE_WAIT_FOR_INTRO_TO_FINISH
		
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				ALLOW_BOAT_BOOM_TO_ANIMATE(truckYachtPackerTrailer, TRUE)
			ENDIF
							
			IF eChaseStage > CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3 //CHASE_FRANKLIN_RECOVERS_LOOP //CHASE_CATCH_JIMMY_DETACH
				IF NOT IS_PED_INJURED(pedBadguy4BoomGuy)
					//IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedBadguy4BoomGuy), 1.0)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDboomGuy) >= 1.0
						sceneIDboomGuy = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIDboomGuy, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e") )
					
						SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDboomGuy, FALSE)
						SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDboomGuy, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
													
						TASK_SYNCHRONIZED_SCENE (pedBadguy4BoomGuy, sceneIDboomGuy, "MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_shuffleAlong_Thug", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						eBoomBaddie = BOOMBADDIE_WAIT_FOR_BOOM_CLIMBING_TO_FINISH
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDboomGuy)
			AND NOT IS_PED_INJURED(pedBadguy4BoomGuy)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDboomGuy) >= 1.0
					
					sceneIDboomGuy = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIDboomGuy, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e") )
					
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDboomGuy, FALSE)
					SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDboomGuy, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
													
					TASK_SYNCHRONIZED_SCENE (pedBadguy4BoomGuy, sceneIDboomGuy, "MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_shuffleAlong_Thug", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					eBoomBaddie = BOOMBADDIE_WAIT_FOR_BOOM_CLIMBING_TO_FINISH
				ENDIF
			ENDIF
			IF bCutOfBoxThrowerOver = TRUE
			
				IF (GET_GAME_TIMER() - iTimeBoomBaddieAppears) > 3000
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBadguy4BoomGuy, PLAYER_PED_ID(), FALSE)
					//IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedBadguy4BoomGuy, BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 9.0, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
					#ENDIF
						eBoomBaddie = BOOMBADDIE_BAD_GUY_SHOT
					ENDIF
				ENDIF
			
			ENDIF
		
		BREAK

		CASE BOOMBADDIE_WAIT_FOR_BOOM_CLIMBING_TO_FINISH
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDboomGuy)
			AND NOT IS_PED_INJURED(pedBadguy4BoomGuy)
			AND eChaseStage < CHASE_CATCH_JIMMY_DETACH
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDboomGuy) >= 1.0

					sceneIDboomGuy = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIDboomGuy, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e") )
					
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDboomGuy, TRUE)
					SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDboomGuy, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
													
					TASK_SYNCHRONIZED_SCENE (pedBadguy4BoomGuy, sceneIDboomGuy, "MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_hitJimmyLoop_Thug", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					iTimeOfJimmyBeingHit = GET_GAME_TIMER()
					eBoomBaddie = BOOMBADDIE_START_PUNCHING_JIMMY_OFF_BOOM
				ENDIF
			ENDIF
		
			IF eChaseStage > CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3 //CHASE_FRANKLIN_RECOVERS //CHASE_CREATE_SPEECH_FOR_FRANKLIN //CHASE_FRANKLIN_RECOVERS_LOOP //CHASE_CATCH_JIMMY_DETACH
				IF NOT IS_PED_INJURED(pedBadguy4BoomGuy)
				AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
					IF IS_ENTITY_ON_SCREEN(truckYachtPackerTrailer)
					AND IS_ENTITY_ON_SCREEN(pedBadguy4BoomGuy)
					AND GET_GAME_TIMER() - iTimeOfJimmyDropping > 5000

						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBadguy4BoomGuy, "SCREAM_TERROR", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
						PROCESS_ENTITY_ATTACHMENTS(pedBadguy4BoomGuy)
						SET_PED_TO_RAGDOLL(pedBadguy4BoomGuy, 0, 10000, TASK_NM_BALANCE)
													
						PLAY_SOUND_FROM_COORD(-1, "TEST_SCREAM_LONG", GET_ENTITY_COORDS(pedBadguy4BoomGuy))
						eBoomBaddie = BOOMBADDIE_BAD_GUY_SHOT_KILL_OFF_ENTITY
					ENDIF
				ENDIF
			ENDIF
			
			IF bCutOfBoxThrowerOver = TRUE
			
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBadguy4BoomGuy, PLAYER_PED_ID(), FALSE)
				//IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedBadguy4BoomGuy, BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 2.0, TRUE)
				#IF IS_DEBUG_BUILD
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
				#ENDIF
					eBoomBaddie = BOOMBADDIE_BAD_GUY_SHOT
				ENDIF
			
			ENDIF
			
		BREAK
		
//		onBoom_twohand_hang_enter
//onBoom_twohand_hang_idle
//onBoom_twohand_drop
		
		CASE BOOMBADDIE_START_PUNCHING_JIMMY_OFF_BOOM
			
			IF eChaseStage < CHASE_CATCH_JIMMY_DETACH
				IF GET_GAME_TIMER()	- iTimeOfJimmyBeingHit > 10000	
					//Go back to loop...
					sceneIdSon = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)		
					IF NOT IS_PED_INJURED(pedJimmy)								
						TASK_SYNCHRONIZED_SCENE(pedJimmy, sceneIdSon, "MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_twohand_hang_enter", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdSon, FALSE)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIdSon, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e")) //11 )
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
					iJimmysLegState = 100
					eBoomBaddie = BOOMBADDIE_JIMMY_GOES_TO_ONE_HANDED_HANG
				ENDIF
			ELSE
				//Do we go elsehwerE?
				eBoomBaddie = BOOMBADDIE_FORCE_BAD_GUY_TO_FALL_OFF	
			ENDIF
		BREAK
				
		CASE BOOMBADDIE_JIMMY_GOES_TO_ONE_HANDED_HANG
			
			IF eChaseStage < CHASE_CATCH_JIMMY_DETACH
			
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) >= 0.99
				AND iJimmysLegState <> 101
					//Go back to loop...
					sceneIdSon = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)		
					IF NOT IS_PED_INJURED(pedJimmy)								
						TASK_SYNCHRONIZED_SCENE(pedJimmy, sceneIdSon, "MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_twohand_hang_idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdSon, FALSE)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIdSon, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e")) //11 )
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
					iJimmysLegState = 101
				ENDIF
				
				IF GET_GAME_TIMER()	- iTimeOfJimmyBeingHit > 20000
					STOP_PED_SPEAKING(pedJimmy, TRUE)
					
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "SCREAM_TERROR", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
					
					PROCESS_ENTITY_ATTACHMENTS(pedJimmy)
					SET_PED_TO_RAGDOLL(pedJimmy, 10000, 30000, TASK_NM_SCRIPT)
					eBoomBaddie = BOOMBADDIE_JIMMY_DIES
				ENDIF
			ELSE
				eBoomBaddie = BOOMBADDIE_FORCE_BAD_GUY_TO_FALL_OFF
			ENDIF
			
		BREAK
		
		CASE BOOMBADDIE_JIMMY_DIES
			IF GET_GAME_TIMER()	- iTimeOfJimmyBeingHit > 23000
				IF eChaseStage < CHASE_CATCH_JIMMY_DETACH
					IF NOT IS_PED_INJURED(pedJimmy)
						SET_ENTITY_HEALTH(pedJimmy, 0)
					ENDIF
				ELSE				
					eBoomBaddie = BOOMBADDIE_FORCE_BAD_GUY_TO_FALL_OFF
				ENDIF
			ENDIF
		BREAK
		
		CASE BOOMBADDIE_BAD_GUY_SHOT
			
			IF NOT IS_PED_INJURED(pedBadguy4BoomGuy)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBadguy4BoomGuy, "SCREAM_TERROR", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
				
				PROCESS_ENTITY_ATTACHMENTS(pedBadguy4BoomGuy)
				SET_PED_TO_RAGDOLL(pedBadguy4BoomGuy, 0, 10000, TASK_NM_BALANCE)	
				PLAY_SOUND_FROM_COORD(-1, "TEST_SCREAM_LONG", GET_ENTITY_COORDS(pedBadguy4BoomGuy))
			ENDIF
			
			iTimeOfBoomBanddieShot = GET_GAME_TIMER()
			
			ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<1.3624, -7.9447, 3.2304>>)
			POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.2507, -5.4743, 2.6868>>)
			SET_CAM_FOV(cutsceneCamera, 50.7682)
			
			eBoomBaddie = BOOMBADDIE_BAD_GUY_SHOT_KILL_OFF_ENTITY
		BREAK
	
		
		CASE BOOMBADDIE_FORCE_BAD_GUY_TO_FALL_OFF
		
			IF bStopBashing = FALSE
				
				sceneIDboomGuy = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIDboomGuy, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e") )
					
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDboomGuy, TRUE)
				SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIDboomGuy, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
													
				TASK_SYNCHRONIZED_SCENE (pedBadguy4BoomGuy, sceneIDboomGuy, "MISSFAM1_YACHTBATTLEonYacht02_", "onboom_loop_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			
				//SCRIPT_ASSERT("falling off!")
			
				bStopBashing = TRUE
			ENDIF
		
			//Deal with the bad guy falling off...
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDboomGuy)
			AND NOT IS_PED_INJURED(pedBadguy4BoomGuy)
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDboomGuy) >= 0.99
				
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBadguy4BoomGuy, "SCREAM_TERROR", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
					PROCESS_ENTITY_ATTACHMENTS(pedBadguy4BoomGuy)
					SET_PED_TO_RAGDOLL(pedBadguy4BoomGuy, 0, 10000, TASK_NM_BALANCE)
					iTimeOfBoomBanddieShot = GET_GAME_TIMER()
					eBoomBaddie = BOOMBADDIE_BAD_GUY_SHOT_KILL_OFF_ENTITY
					
				ENDIF
			ENDIF
		BREAK
	
		CASE BOOMBADDIE_BAD_GUY_SHOT_KILL_OFF_ENTITY
		
			IF (GET_GAME_TIMER() - iTimeOfBoomBanddieShot) > 3500
				SET_ENTITY_HEALTH(pedBadguy4BoomGuy, 0)
				eBoomBaddie = BOOMBADDIE_STOP_PROCESSING
			ENDIF
		BREAK
		
		
	ENDSWITCH

	IF IS_PED_INJURED(pedBadguy4BoomGuy)
	AND bTrackStat = FALSE
		REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 5)
		INFORM_MISSION_STATS_OF_INCREMENT(FAM1_KILLS) 
		bTrackStat = TRUE
	ELSE
		IF NOT IS_ENTITY_DEAD(pedBadguy4BoomGuy)
			IF IS_PED_RAGDOLL(pedBadguy4BoomGuy)
			AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBadguy4BoomGuy, PLAYER_PED_ID())
				SET_ENTITY_HEALTH(pedBadguy4BoomGuy, 0)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
	AND eBoomBaddie >= BOOMBADDIE_WAIT_FOR_BOOM_CLIMBING_TO_FINISH
	AND eBoomBaddie <= BOOMBADDIE_BAD_GUY_SHOT
	AND eChaseStage < CHASE_FRANKLIN_RECOVERS
		IF NOT IS_PED_INJURED(pedBadguy4BoomGuy)
			IF bBoomBaddieDialogueDisplayed = FALSE
				CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_B3", CONV_PRIORITY_MEDIUM)
			
				bBoomBaddieDialogueDisplayed = TRUE
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

PROC UPDATE_JIMMYS_LEGS()

	SWITCH iJimmysLegState
	
		CASE 0
			//Set up leg lift points...
			fRecordingProgressPointsWhereJimmyLiftsLegs[0] = 97838.590
			fRecordingProgressPointsWhereJimmyLiftsLegs[1] = 101355.200
			fRecordingProgressPointsWhereJimmyLiftsLegs[2] = 103463.800
			fRecordingProgressPointsWhereJimmyLiftsLegs[3] = 109879.900
			
			//If we skip dont have the legs lift up buffer up.
			IF fRecordingProgress > fRecordingProgressPointsWhereJimmyLiftsLegs[0]
				iProgressPoint = 1
			ENDIF
			IF fRecordingProgress > fRecordingProgressPointsWhereJimmyLiftsLegs[1]
				iProgressPoint = 2
			ENDIF
			IF fRecordingProgress > fRecordingProgressPointsWhereJimmyLiftsLegs[2]
				iProgressPoint = 3
			ENDIF
			
			iJimmysLegState++
		BREAK
			
		CASE 1
			//Go back to loop...
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdSon) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) >= 1.0)
			OR bJimmyBoomHitIntroAnimComplete = TRUE
				sceneIdSon = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.1>>, <<0.0, 0.0, 0.0>>)

				IF NOT IS_PED_INJURED(pedJimmy)								
					TASK_SYNCHRONIZED_SCENE(pedJimmy, sceneIdSon, "MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_hangLoop_J", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				ENDIF
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdSon, TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIdSon, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e")) //11 )
				
				bJimmyBoomHitIntroAnimComplete = TRUE
				
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
				
				iJimmysLegState++
			ENDIF
		BREAK
		
		
		
		CASE 2
			//Wait for start times...

			IF iProgressPoint < 4
				IF fRecordingProgress > fRecordingProgressPointsWhereJimmyLiftsLegs[iProgressPoint]
					iProgressPoint++
					iJimmysLegState++
				ENDIF
			ELSE
				//Go back to loop...
				sceneIdSon = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.1>>, <<0.0, 0.0, 0.0>>)
						
				IF NOT IS_PED_INJURED(pedJimmy)								
					TASK_SYNCHRONIZED_SCENE(pedJimmy, sceneIdSon, "MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_hangLoop_J", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				ENDIF
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdSon, TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIdSon, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e")) //11 )
			
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
				iJimmysLegState = -1
			ENDIF
			
		BREAK
		
		CASE 3
			//Trigger leg pull up anims.
			
			sceneIDLegPullUp = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.1>>, <<0.0, 0.0, 0.0>>)
						
			IF NOT IS_PED_INJURED(pedJimmy)								
				TASK_SYNCHRONIZED_SCENE(pedJimmy, sceneIDLegPullUp, "MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_liftLegs_J", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			ENDIF
			SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDLegPullUp, FALSE)
			ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIDLegPullUp, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e")) // "misc_e")) //11 )
		
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
			
			iJimmysLegState++
		BREAK
		
		CASE 4
			//Wait until leg pull up anim is finished
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDLegPullUp)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDLegPullUp) >= 1.0
					iJimmysLegState = 1
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH



ENDPROC

CONST_FLOAT FTRUCK_BASE_SPEED  30.0

FLOAT fDesiredActual
FLOAT fMinActual
FLOAT fMaxActual

PROC CONTROL_PLAYBACK_SPEED()

	VEHICLE_INDEX vehicleForPlaybackSpeed
	vehicleForPlaybackSpeed = truckYachtPacker
	
	MODIFY_VEHICLE_TOP_SPEED(carPlayersJeep, 0.0)
	
	vTargetVehicleOffset = <<0.0, 0.0, 0.0>>

	SWITCH eChaseStage 
	
		CASE CHASE_FRANKLIN_STANDS_UP		
		CASE CHASE_FRANKLIN_STANDS_LOOP
		CASE CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR
		CASE CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR_LOOP
		
			fminPlaybackSpeed = 0.65
			fMaxPlaybackSpeed = 1.3
			vTargetVehicleOffset.y  = -18.0
		BREAK
		
		CASE CHASE_FRANKLIN_JUMPS_ON_YACHT
		CASE CHASE_FRANKLIN_BOARDS_INTRO
			fminPlaybackSpeed = 0.65
			fMaxPlaybackSpeed = 1.05
			vTargetVehicleOffset.y  = -7.0
		BREAK
		
		CASE CHASE_FRANKLIN_BOARDS_LOOP
		CASE CHASE_FRANKLIN_BOARDS_OUTRO
		CASE CHASE_STRUGGLE_1
			fminPlaybackSpeed = 0.8
			fmaxPlaybackSpeed = 1.2
			
			vTargetVehicleOffset.y  = -13.0
		BREAK
		
		CASE CHASE_STRUGGLE_1_LOOP
		CASE CHASE_STRUGGLE_1_UPDATE
		CASE CHASE_STRUGGLE_1_OUTRO
		CASE CHASE_HIDE_BEHIND_WHEEL_INTRO
		CASE CHASE_HIDE_BEHIND_WHEEL_LOOP
			
			fminPlaybackSpeed = 0.6
			fmaxPlaybackSpeed = 1.12	
			
			vTargetVehicleOffset.y  = -14.0
		BREAK
		
		CASE CHASE_STRUGGLE_2
			fminPlaybackSpeed = 0.475
			fmaxPlaybackSpeed = 1.07
			vTargetVehicleOffset.y  = -15.5
			//vehicleForPlaybackSpeed = truckYachtPackerTrailer
		BREAK	
		
		CASE CHASE_STRUGGLE_2_LOOP
			fminPlaybackSpeed = 0.55
			fmaxPlaybackSpeed = 1.18
			vTargetVehicleOffset.y  = -15.8
		BREAK	
		
		CASE CHASE_STRUGGLE_2_OUTRO
		CASE CHASE_HIDE_BEHIND_WHEEL_2_INTRO
		CASE CHASE_MEET_JIMMY
		CASE CHASE_SIGNAL_PLAYER
			fminPlaybackSpeed = 0.55
			fmaxPlaybackSpeed = 1.08
			vTargetVehicleOffset.y  = -17.5
			//vehicleForPlaybackSpeed = truckYachtPackerTrailer
		BREAK	
		
		CASE CHASE_BOOM_SWING
		CASE CHASE_BOOM_SWING_LOOP	
			fminPlaybackSpeed = 0.55
			fmaxPlaybackSpeed = 1.125
			vTargetVehicleOffset.y  = -17.0
			//vehicleForPlaybackSpeed = truckYachtPackerTrailer
		BREAK	
		
		CASE CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3
			fminPlaybackSpeed = 0.55
			fmaxPlaybackSpeed = 1.08
			vTargetVehicleOffset.y  = -20.0
			//vehicleForPlaybackSpeed = truckYachtPackerTrailer
		BREAK	
		
		CASE CHASE_FRANKLIN_NOTIONS_TO_SAVE_JIMMY
		CASE CHASE_CATCH_JIMMY_DETACH
			fminPlaybackSpeed = 0.6
			fmaxPlaybackSpeed = 1.04
			vTargetVehicleOffset.y  = 4.0
		BREAK
		
		CASE CHASE_FRANKLIN_RECOVERS
		CASE CHASE_FRANKLIN_RECOVERS_LOOP
			fminPlaybackSpeed = 0.65
			fmaxPlaybackSpeed = 1.15
			vTargetVehicleOffset.y  = -10.5
			//vehicleForPlaybackSpeed = truckYachtPacker
		BREAK	
		
		CASE CHASE_CATCH_FRANKLIN
		CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
			fminPlaybackSpeed = 0.5
			fmaxPlaybackSpeed = 1.0
			vTargetVehicleOffset.y  = -6.5
			//vehicleForPlaybackSpeed = truckYachtPacker
		BREAK	
		
//		CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
//			fminPlaybackSpeed = 1.0
//			fmaxPlaybackSpeed = 1.45
//			vTargetVehicleOffset.y  = -22.5//-15.5
//			//vehicleForPlaybackSpeed = truckYachtPacker
//		BREAK	
		
		CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_2
		CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_UPDATE
			//CASE CHASE_
			fminPlaybackSpeed = 1.0
			fmaxPlaybackSpeed = 1.45
			vTargetVehicleOffset.y  = -22.5
		BREAK
		
		CASE CHASE_PUT_FRANKLIN_BACK_IN_CAR_PROPERLY
			fminPlaybackSpeed = 1.05
			fmaxPlaybackSpeed = 1.5
			vTargetVehicleOffset.y  = -100.5
			
		//	fminPlaybackSpeed = 0.6
		//	fmaxPlaybackSpeed = 1.05
		//	vTargetVehicleOffset.y  = -23.5
		BREAK
		
		CASE CHASE_NO_MORE_STAGES
		CASE CHASE_KEEP_CHASING
		CASE CHASE_NO_MORE_STAGES_2
			fminPlaybackSpeed = 1.15
			fmaxPlaybackSpeed = 1.6
			vTargetVehicleOffset.y  = -150.5
			
		//	fminPlaybackSpeed = 0.6
		//	fmaxPlaybackSpeed = 1.05
		//	vTargetVehicleOffset.y  = -23.5
		BREAK
				
	ENDSWITCH

	//Overides... lets see the mast break happen.
	IF fRecordingProgress < 48791.0200
		fminPlaybackSpeed = 0.8
		fMaxPlaybackSpeed = 1.3
		vTargetVehicleOffset.y  = -39.0
	ENDIF
	
//	//Overides... bedore tunnel
//	IF fRecordingProgress > 23791.0200
//	AND fRecordingProgress < 35791.0200
//		fminPlaybackSpeed = 0.75
//		fMaxPlaybackSpeed = 1.15
//		vTargetVehicleOffset.y  = -25.0
//	ENDIF
	
	//Overides... lets see the flatbed set piece happen...
	IF fRecordingProgress > 52368.330
	AND fRecordingProgress < 54317.410
		fminPlaybackSpeed = 0.6
		fMaxPlaybackSpeed = 1.125
		vTargetVehicleOffset.y  = -5.0 //Gradual
	ENDIF
	IF fRecordingProgress > 54368.330
	AND fRecordingProgress < 55317.410
		fminPlaybackSpeed = 0.6
		fMaxPlaybackSpeed = 1.15
		vTargetVehicleOffset.y  = -10.0 //Gradual
	ENDIF
	IF fRecordingProgress > 55368.330
	AND fRecordingProgress < 63317.410
		fminPlaybackSpeed = 0.6
		fMaxPlaybackSpeed = 1.18
		vTargetVehicleOffset.y  = -20.0 //Now at full speed Gradual
	ENDIF

	//Dont rubber band until it's past the junction....
	IF fRecordingProgress < 17356.00
		fminPlaybackSpeed = 1.0
		fMaxPlaybackSpeed = 1.05
		vTargetVehicleOffset.y  = -20.0 //Now at full speed Gradual
	ENDIF

	//FLOAT fSpeedMultiplier

	IF NOT IS_CAM_ACTIVE(cutsceneCamera)
	OR NOT IS_CAM_ACTIVE(cameraRecorded1)
	OR NOT IS_CAM_ACTIVE(cameraRecorded2)

		IF NOT IS_PLAYER_IN_FRONT_OF_TRANSPORTER()
			IF NOT IS_ENTITY_DEAD(vehicleForPlaybackSpeed)
			
				fVehicleActualSpeed = GET_ENTITY_SPEED(vehicleForPlaybackSpeed)
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicleForPlaybackSpeed)
					//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
								
					
					fMinActual = (fMinPlaybackSpeed * FTRUCK_BASE_SPEED)
					fMaxActual = (fMaxPlaybackSpeed * FTRUCK_BASE_SPEED)
					
					IF fMinActual = 0.0
					ENDIF
																		
					FLOAT fDistBetweenCarAndYachtOffset = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, vTargetVehicleOffset), FALSE)
					
				//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, vTargetVehicleOffset), 0.5, 128, 128, 128)
					
//					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicleForPlaybackSpeed, FALSE) < fDistBetweenCarAndYachtOffset
//						fDesiredPlaybackSpeed = ( fPLaybackSpeedNumerator / GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicleForPlaybackSpeed))
//						#IF IS_DEBUG_BUILD
//							IF bISSuperDebugEnabled
//								DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, -1.0, 0.0>>, 1.2, 0, 255, 0)
//							ENDIF
//						#ENDIF
//						fSpeedMultiplier = 0.08 //0.5 //1.09
//					ELSE
//						#IF IS_DEBUG_BUILD
//							IF bISSuperDebugEnabled
//								DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, -1.0, 0.0>>, 1.2, 255, 0, 0)
//							ENDIF
//						#ENDIF
//						fDesiredPlaybackSpeed = ( fPLaybackSpeedNumerator / fDistBetweenCarAndYachtOffset)
//						fSpeedMultiplier = 0.04 //0.30 //1.007
//					ENDIF
					
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicleForPlaybackSpeed, FALSE) < fDistBetweenCarAndYachtOffset
					
						IF fDistBetweenCarAndYachtOffset > 40.0
							fDistBetweenCarAndYachtOffset = 40.0
						ENDIF
					
						fDesiredPlaybackSpeed = fMinPlaybackSpeed + ((fMaxPlaybackSpeed - fMinPlaybackSpeed)  * (1.0 - (fDistBetweenCarAndYachtOffset) / (40.00)))
						//fSpeedMultiplier = 0.08 //0.5 //1.09
					ELSE
						
						IF fDistBetweenCarAndYachtOffset > 40.0
							fDistBetweenCarAndYachtOffset = 40.0
						ENDIF
						
						fDesiredPlaybackSpeed = fMinPlaybackSpeed + ((fMaxPlaybackSpeed - fMinPlaybackSpeed)  * (1.0 - (fDistBetweenCarAndYachtOffset) / (40.00)))
						//fSpeedMultiplier = 0.04 //0.30 //1.007
					ENDIF
										
					fDesiredActual = (fDesiredPlaybackSpeed * FTRUCK_BASE_SPEED)
					
					
					IF fVehicleActualSpeed < (fDesiredActual) //mainPlaybackSpeed < fDesiredPlaybackSpeed
					OR IS_PLAYER_IN_FRONT_OF_TRANSPORTER_OFFSET(vTargetVehicleOffset.y)
						IF fVehicleActualSpeed < fMaxActual
							IF fVehicleActualSpeed < (fDesiredActual) 
								mainPlaybackSpeed = mainPlaybackSpeed +@ ((fDesiredActual - fVehicleActualSpeed) / 30.0) //fSpeedMultiplier //) //1.007)
								//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 3.0, 0.0>>, 1.2, 255, 0, 0)
							
								PRINTLN("CPS: speeding up", ((fDesiredActual - fVehicleActualSpeed) / 30.0))
							ELSE
								mainPlaybackSpeed = mainPlaybackSpeed +@ 0.04
							ENDIF
						ELSE
							PRINTLN("CPS: AT MAX SPEED!", fMaxActual)
						ENDIF
					ELSE
						//IF fDistBetweenCarAndYachtOffset > 40.0
							//((mainPlaybackSpeed - fDesiredPlaybackSpeed) / 30.0)
							mainPlaybackSpeed = mainPlaybackSpeed -@ ((mainPlaybackSpeed - fDesiredPlaybackSpeed) / 7.0) //0.008 //0.15 // 0.995)
							//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 3.0, 0.0>>, 1.0, 50, 120, 255)
							PRINTLN("CPS:Slowing down lots!", ((mainPlaybackSpeed - fDesiredPlaybackSpeed) / 1.0))
//						ELSE
//							DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 3.0, 0.0>>, 0.9, 0, 0, 255)
//							PRINTLN("CPS:Slowing down a little!", ((mainPlaybackSpeed - fDesiredPlaybackSpeed) / 14.0))
//							mainPlaybackSpeed = mainPlaybackSpeed -@ ((mainPlaybackSpeed - fDesiredPlaybackSpeed) / 14.0) //0.002 //0.15 // 0.995)
//						ENDIF
					ENDIF
					
//					IF fVehicleActualSpeed > fMaxActual  //Lock to maximum
//						mainPlaybackSpeed = fMaxPlaybackSpeed 
//					ENDIF
							
//					IF mainPlaybackSpeed > fMaxPlaybackSpeed
//						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicleForPlaybackSpeed, FALSE) < fDistBetweenCarAndYachtOffset
//					
//							IF NOT (eChaseStage = CHASE_FRANKLIN_NOTIONS_TO_SAVE_JIMMY OR eChaseStage = CHASE_CATCH_JIMMY_DETACH)
//								MODIFY_VEHICLE_TOP_SPEED(carPlayersJeep, -20.0)
//							ENDIF
//						ENDIF
//						//mainPlaybackSpeed = fMaxPlaybackSpeed 	
//					ELSE
//						
//						//mainPlaybackSpeed = mainPlaybackSpeed -@ 0.001 //0.15 // 0.995)
//						
//						MODIFY_VEHICLE_TOP_SPEED(carPlayersJeep, 0.0)
//					ENDIF
							
					#IF IS_DEBUG_BUILD
						IF bIsSuperDebugEnabled
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(carPlayersJeep, GET_STRING_FROM_FLOAT(fVehicleActualSpeed), 0.5)
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(carPlayersJeep, GET_STRING_FROM_FLOAT(fDesiredPlaybackSpeed * fVehicleActualSpeed), 1.0, 0, 255, 127, 0)
						ENDIF
					#ENDIF
				ENDIF	
			ENDIF
		ELSE
			IF mainPlaybackSpeed < 2.0
				PRINTLN("CPS: CURVE BALL!")
				//MODIFY_VEHICLE_TOP_SPEED(carPlayersJeep, -20.0)
				mainPlaybackSpeed = mainPlaybackSpeed +@ 0.2 // 1.0125 //* (60.0 * GET_FRAME_TIME())
			ENDIF
		ENDIF
	
	ENDIF
	
	#IF IS_DEBUG_BUILD 
		bDEbugIsInFront = IS_PLAYER_IN_FRONT_OF_TRANSPORTER()
	#ENDIF
	
	//CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehicleForPlaybackSpeed, PLAYER_PED_ID(),mainPlaybackSpeed, fminPlaybackSpeed, ABSF(vTargetVehicleOffset.y), ABSF(2.0 * vTargetVehicleOffset.y))  
			
	
ENDPROC

FLOAT fPlayersActualSpeed

PROC MATCH_PLAYERS_CAR_SPEED(VEHICLE_INDEX vehicleForPlaybackSpeed)

	FLOAT fSpeedMultiplier


	IF NOT IS_ENTITY_DEAD(vehicleForPlaybackSpeed)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicleForPlaybackSpeed)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			MODIFY_VEHICLE_TOP_SPEED(carPlayersJeep, 0.0)
			
			fVehicleActualSpeed = GET_ENTITY_SPEED(vehicleForPlaybackSpeed)
			fPlayersActualSpeed = GET_ENTITY_SPEED(carPlayersJeep)
			
			//fMinActual = (fMinPlaybackSpeed * FTRUCK_BASE_SPEED)
												
			IF fPlayersActualSpeed > fVehicleActualSpeed
				//Allow a small buffer to allow catch up.
				IF (fPlayersActualSpeed + 1.0) > fVehicleActualSpeed
					fSpeedMultiplier = 0.05 //0.5 //1.09
				ENDIF
				
				//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(carPlayersJeep), 1.0)
				PRINTLN("speeding up")
				
			ELSE
				//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(carPlayersJeep), 1.0, 255, 0, 0)
				PRINTLN("Slowing down")
				fSpeedMultiplier = -0.06 //0.30 //1.007
			ENDIF
			
			fDesiredActual = (fDesiredPlaybackSpeed * FTRUCK_BASE_SPEED)
			
			mainPlaybackSpeed = mainPlaybackSpeed +@ fSpeedMultiplier //) //1.007)
						
			#IF IS_DEBUG_BUILD
				IF bIsSuperDebugEnabled
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(vehicleForPlaybackSpeed, GET_STRING_FROM_FLOAT(fVehicleActualSpeed), 0.5)
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(carPlayersJeep, GET_STRING_FROM_FLOAT(fPlayersActualSpeed), 1.0, 0, 255, 127, 0)
				ENDIF
			#ENDIF
			
		ENDIF	
	ENDIF

ENDPROC


//PROC PROCESS_ADAPTIVE_CRUISE_CONTROL(BOOL bCatchingJimmy = FALSE)
//	
////	IF bCatchingJimmy
////		vTargetAdaptiveCruise = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, vCatchJimmyOffset)
////		vLocateSizeAdaptiveCruise = <<2.0, 2.0, 3.0>>
////	ELSE
////		vTargetAdaptiveCruise = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, vTruckRearOffset)
////		vLocateSizeAdaptiveCruise = <<4.25, 3.75, 3.0>>
////	ENDIF
////
////	//Adaptive Cruise Control!!
////	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
////	AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
////		IF IS_ENTITY_AT_COORD(carPlayersJeep, vTargetAdaptiveCruise, vLocateSizeAdaptiveCruise )	
////						
////			IF GET_ENTITY_SPEED(carPlayersJeep) > (1.15 * GET_ENTITY_SPEED(truckYachtPackerTrailer))
////				
////				fDistanceToYachtBoardingPoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(carPlayersJeep), vTargetAdaptiveCruise)
////				
////				#IF IS_DEBUG_BUILD
////					IF bIsSuperDebugEnabled
////						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
////						DRAW_DEBUG_SPHERE(vTargetAdaptiveCruise, 0.5)
////					ENDIF
////				#ENDIF
////				IF fDistanceToYachtBoardingPoint > 13.0
////					fDistanceToYachtBoardingPoint = 13.0
////				ENDIF
////				fSlowDownForce = 9.0 - ( (ABSF(fDistanceToYachtBoardingPoint)) / 2.0)
////				
////				//APPLY_FORCE_TO_ENTITY(carPlayersJeep, APPLY_TYPE_FORCE, <<0.0, (fSlowDownForce), 0.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
////								
////			ENDIF
////		ELSE
////			fSlowDownForce = 1.0
////		ENDIF
////	ENDIF
////	
////	IF fSlowDownForce < 1.0
////		fSlowDownForce = 1.0
////	ENDIF
////	IF fSlowDownForce > 10.0
////		fSlowDownForce = 10.0
////	ENDIF
////	
////	SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), fSlowDownForce)
//
//ENDPROC

FUNC BOOL IS_BMW_STEADY_FOR_JUMP(FLOAT fAllowedAngleDif = 90.0)

	//FAILS
	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
	AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
		
		VECTOR vCarRotation = GET_ENTITY_ROTATION(carPlayersJeep)
		VECTOR vTruckRotation = GET_ENTITY_ROTATION(truckYachtPackerTrailer)
		
		IF (vCarRotation.y > 90.0 AND vCarRotation.y < 270.0)
		OR (vCarRotation.x > 90.0 AND vCarRotation.x < 270.0)
	
			RETURN FALSE
		ENDIF											
		
//		PRINTSTRING("Speed dif:")
//		PRINTFLOAT(GET_ENTITY_SPEED(carPlayersJeep))
//		PRINTSTRING(" - ") GET_ENTITY_SPEED(truckYachtPackerTrailer)
//		PRINTSTRING(" = ") PRINTFLOAT((GET_ENTITY_SPEED(carPlayersJeep) - GET_ENTITY_SPEED(truckYachtPackerTrailer)))
//		PRINTNL()
		
		IF ABSF(GET_ENTITY_SPEED(carPlayersJeep) - GET_ENTITY_SPEED(truckYachtPackerTrailer)) > fJumpMaxSpeedVariance
			RETURN FALSE
		ENDIF	
		
		IF ABSF((vCarRotation.z) - (vTruckRotation.z)) > fAllowedAngleDif
			RETURN FALSE
		ENDIF	
		
		
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC



FUNC FLOAT GET_SYNCED_SCENE_PHASE(INT thisSceneID)

	IF IS_SYNCHRONIZED_SCENE_RUNNING(thisSceneID)
		RETURN GET_SYNCHRONIZED_SCENE_PHASE(thisSceneID)
	ELSE
		PRINTLN("Chase stage: ", ENUM_TO_INT(eChaseStage))
		SCRIPT_ASSERT("GET_SYNCED_SCENE_PHASE: scene not running ")
	ENDIF
	
	RETURN 1.0 //If its not running its as good as completed?

ENDFUNC




INT iAmbientDialogueTimer

FUNC INT GET_TRAFFIC_VEHICLE_SEARCH_FLAGS()
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED)
ENDFUNC

PROC UPDATE_AMBIENT_SWEARING()

	VEHICLE_INDEX vehClosest
	PED_INDEX pedCurrentAngryDriver

	IF GET_GAME_TIMER() - iAmbientDialogueTimer > 3000
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			vehClosest = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10.0, DUMMY_MODEL_FOR_SCRIPT, GET_TRAFFIC_VEHICLE_SEARCH_FLAGS())

			IF NOT IS_ENTITY_DEAD(vehClosest) //AND vehClosest != vehBike[0]
				BOOL bCrashed = FALSE
				BOOL bNearMiss = FALSE

				IF IS_ENTITY_TOUCHING_ENTITY(vehClosest, carPlayersJeep)
					bCrashed = TRUE
				ELIF ABSF(GET_ENTITY_SPEED(vehClosest)) > 2.0
					IF ABSF(GET_ENTITY_HEADING(vehClosest) - GET_ENTITY_HEADING(carPlayersJeep)) > 60.0 
						bNearMiss = TRUE
					ENDIF
				ENDIF

				IF bCrashed OR bNearMiss
					pedCurrentAngryDriver = GET_PED_IN_VEHICLE_SEAT(vehClosest)
					IF NOT IS_PED_INJURED(pedCurrentAngryDriver)                                                                
						IF bCrashed
						    PLAY_PED_AMBIENT_SPEECH(pedCurrentAngryDriver, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_NO_REPEAT_FRONTEND)
						ELSE
						    PLAY_PED_AMBIENT_SPEECH(pedCurrentAngryDriver, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_NO_REPEAT_FRONTEND)
						ENDIF
						iAmbientDialogueTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

VECTOR vCarRotationStandingUp

PROC RAGDOLL_AND_FAIL_IF_UPSIDE_DOWN()

	SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedFranklin(), PRF_BlockWeaponReactionsUnlessDead, TRUE)

	vCarRotationStandingUp = GET_ENTITY_ROTATION(carPlayersJeep)

	//FAILS
	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carPlayersJeep)
			IF (vCarRotationStandingUp.y > 90.0 AND vCarRotationStandingUp.y < 270.0)
			OR (vCarRotationStandingUp.x > 90.0 AND vCarRotationStandingUp.x < 270.0)
										
				//SCRIPT_ASSERT("death")
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
										
				SET_PED_TO_RAGDOLL(pedFranklin(), 0, 60000, TASK_NM_SCRIPT)
									
				CLEAR_HELP()
										
				MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
				TRIGGER_MUSIC_EVENT("FAM1_FAIL")
				Mission_Failed()		
			ENDIF											
		ENDIF		
	ENDIF		
	
ENDPROC

INT iTimeOfHelpText

PROC DEAL_WITH_RAGE_HELP_TEXT()

	SWITCH iRageText

	CASE 0
		SPECIAL_ABILITY_UNLOCK(PLAYER_ZERO)
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
		//SPECIAL_ABILITY_CHARGE_ABSOLUTE(PLAYER_ID(), 100, TRUE)
		iRageText++
	BREAK

	CASE 1
	CASE 2
	
		IF NOT IS_ENTITY_DEAD(pedBadGuy2)
		AND iRageText=1
		AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			//IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_WHERE", CONV_PRIORITY_VERY_HIGH)
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "FAM1AUD", "FAM1_WHERE", CONV_PRIORITY_VERY_HIGH)
			
			iRageText=2
		ENDIF
					
	
		SPECIAL_ABILITY_CHARGE_CONTINUOUS(PLAYER_ID(), TRUE)
		
		IF GET_SYNCED_SCENE_PHASE(sceneId) > 0.0
			
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) <> CAM_VIEW_MODE_FIRST_PERSON
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "MISSFAM1_YACHTBATTLEonYacht01_", "grab_pistol", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_SECONDARY)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			ENDIF
			
			REQUEST_ANIM_SET("drive_by@low_ds")	//Force driveby anims to be in memory.
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
			
			iRageText = 3
		ENDIF
	BREAK
	
	CASE 3
		
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "MISSFAM1_YACHTBATTLEonYacht01_", "grab_pistol")
			SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), "MISSFAM1_YACHTBATTLEonYacht01_", "grab_pistol", 2.0)
			
			IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "MISSFAM1_YACHTBATTLEonYacht01_", "grab_pistol") > 0.6
			OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
				
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
				//IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 160, TRUE, TRUE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
					iRageText = 4
				//ENDIF
			ENDIF
		ELSE
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) <> CAM_VIEW_MODE_FIRST_PERSON
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "MISSFAM1_YACHTBATTLEonYacht01_", "grab_pistol", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_SECONDARY)
			ELSE
				//Skip the anim if gone to first person
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 160, TRUE, TRUE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
				iRageText = 4
			ENDIF
		ENDIF
		
	BREAK
	
	CASE 4
		IF GET_SYNCED_SCENE_PHASE(sceneId) > 0.35
		//AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
			//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "FAM1AUD", "FAM1_WHO2", CONV_PRIORITY_HIGH)
					
				ATTACH_CAM_TO_ENTITY(cutsceneCamera, carPlayersJeep, <<-1.1121, -0.6545, 0.7565>>)
				POINT_CAM_AT_ENTITY(cutsceneCamera, carPlayersJeep, <<0.8794, 1.5678, 0.4467>>)
				SET_CAM_FOV(cutsceneCamera, 50.0013)
							
				IF NOT IS_ENTITY_DEAD(pedBadGuy2)
					PRINT_NOW("YB_OBJ2", DEFAULT_GOD_TEXT_TIME - 2000, 1)
					blippedBadguy2 = CREATE_BLIP_FOR_PED(pedBadguy2, TRUE)
				ENDIF
				
				SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_NOTHING)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
																		
				//PRINT_HELP("YB_RAGEBAR")
				
				iRageText++
			//ENDIF
		ENDIF
	BREAK

	CASE 5
		//IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
		//OR NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		//	CLEAR_HELP()
			iRageText++
		//ENDIF
	BREAK

	CASE 6
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			//~s	~Press ~INPUT_VEH_ATTACK~ to shoot while driving. Use ~PAD_RSTICK_ALL~ to aim.						
			IF NOT GET_IS_USING_ALTERNATE_DRIVEBY()
				PRINT_HELP("YB_HELP4", 15000)
			ELSE
				PRINT_HELP("YB_HELP4ALT", 15000)
			ENDIF
			iTimeOfHelpText = GET_GAME_TIMER()
			iRageText++
		ENDIF
	BREAK

	CASE 7
		IF IS_PED_INJURED(pedBadguy2)
		AND GET_GAME_TIMER() - iTimeOfHelpText > 3000
			CLEAR_HELP()
			iRageText++
		ENDIF
	BREAK
	
	CASE 8
//		IF NOT IS_PED_INJURED(pedBadguy2)
//			SPECIAL_ABILITY_CHARGE_CONTINUOUS(PLAYER_ID(), TRUE)
//		ELSE
			iRageText++
		//ENDIF
	BREAK
	

ENDSWITCH

ENDPROC

BOOL bJimmyLandedNoise

PROC INTERP_MAST_DETACH_OFFSET()

	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdSon)

		PRINTLN("vectorS: ", vectorS)
		
		SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIdSon, vectorS, rotationS)
		
		
		vectorS.x = vectorS.x  * (1.0 -@ 2.5)
		vectorS.y = vectorS.y  * (1.0 -@ 2.5)
		vectorS.z = vectorS.z  * (1.0 -@ 8.5)
				
		
		rotationS.x = rotationS.x * (1.0 -@ 8.9) //  * 0.25 * 30.0 * GET_FRAME_TIME()
		
		rotationS.y = rotationS.y * (1.0 -@ 8.9) //* 0.25 * 30.0 * GET_FRAME_TIME()
				
		rotationS.z = rotationS.z * (1.0 -@ 8.9) //* 0.25 * 30.0 * GET_FRAME_TIME()
								
		IF bJimmyLanded = FALSE
		
			PRINTLN("INTERPING Jimmy Landing: ", VDIST(vectorS, <<0.0, 0.0, 0.0>>))
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdSon)									
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) > 0.01
				AND bJimmyLandedNoise = FALSE
					//STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedJimmy)
					PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_DROP_IN_CAR", pedJimmy)
					bJimmyLandedNoise = TRUE
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) > 0.024
										
					//If we dont have far to Interp just snap the scene into place.
					IF VDIST(vectorS, <<0.0, 0.0, 0.0>>) < 1.15
						PRINTLN("PASSED Jimmy Landing: ", VDIST(vectorS, <<0.0, 0.0, 0.0>>))
						SHAKE_GAMEPLAY_CAM("JOLT_SHAKE", 0.42)					
												
						APPLY_FORCE_TO_ENTITY(carPlayersJeep, APPLY_TYPE_IMPULSE, <<0.0, 0.0, -5.4>>, <<0.0, -0.9, 0.0>>, 0, TRUE, TRUE, TRUE)
						vectorS.x = 0.0			
						vectorS.y = 0.0
						vectorS.z = 0.0
						rotationS.x = 0.0			
						rotationS.y = 0.0
						rotationS.z = 0.0
						bJimmyLanded = TRUE
					ELSE
						//RAGDOLL JIMMY
						PRINTLN("FAILED Jimmy landing: ", VDIST(vectorS, <<0.0, 0.0, 0.0>>))
						SET_PED_TO_RAGDOLL(pedJimmy, 0, 60000, TASK_NM_BALANCE) 
						MISSION_FLOW_SET_FAIL_REASON("YB_FAIL1")
						IF NOT IS_PED_INJURED(pedJimmy)
							APPLY_DAMAGE_TO_PED(pedJimmy, 1000, TRUE)
						ENDIF
						TRIGGER_MUSIC_EVENT("FAM1_FAIL")
						Mission_Failed()
					ENDIF
				ENDIF		
			ENDIF	
		ELSE
			IF NOT bMusicEvent3
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) >= 0.021
				IF PREPARE_MUSIC_EVENT("FAM1_JIMMY_LANDS")
					TRIGGER_MUSIC_EVENT("FAM1_JIMMY_LANDS")					
					bMusicEvent3 = TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF					
	
ENDPROC

INT iControlBoxGuyDeath

PROC CONTROL_BOX_THROWING_LOOP()


	

		//When the loop ends play another random one...
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
			PRINTFLOAT(GET_SYNCHRONIZED_SCENE_PHASE(sceneId)) PRINTSTRING("A!")
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 1.0 // - prevSyncPhase) < 0.0   // >= 0.99
				bSceneRestarted = TRUE
			ENDIF
		ENDIF
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdB)
			PRINTFLOAT(GET_SYNCHRONIZED_SCENE_PHASE(sceneIdB)) PRINTSTRING("B!")
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdB) >= 1.0 // - prevSyncPhase) < 0.0   // >= 0.99
				bSceneRestarted = TRUE
			ENDIF
		ENDIF
		
		IF bSceneRestarted = TRUE
		AND NOT (iNumberOfBoxesThrown > 0 AND bForceOffAfter1Box)
			//Choose a variation to play.
			
			IF NOT IS_PED_INJURED(pedBadGuy3BoxThrower)
				IF NOT IS_PED_RAGDOLL(pedBadGuy3BoxThrower)
				AND NOT IS_PED_RAGDOLL(pedFranklin())
				AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDPulledOverboard)
					IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
						sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedBadGuy3BoxThrower, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboardLoop_F", "onYacht_overboardLoop_thugA", TRUE)			
					ELSE
						sceneIdB = SETUP_SYNCED_SCENE(pedFranklin(), pedBadGuy3BoxThrower, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboardLoop2_F", "onYacht_overboardLoop2_thugA", TRUE)			
					ENDIF
				ENDIF
			ENDIF
			//Increment number of boxes thrown
			iNumberOfBoxesThrown++
			iControlBoxes = 0
			prevSyncPhase = 0.0
			
			iControlBoxGuyDeath = 0
																
			PRINTSTRING("Box thrown!") PRINTNL()
			bSceneRestarted = FALSE
		ENDIF
		
		
		//IF iNumberOfBoxesThrown > 4 //5
		IF eChaseStage > CHASE_CATCH_JIMMY_DETACH
		OR (iNumberOfBoxesThrown > 0 AND bForceOffAfter1Box)
		
			
//				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)
//					sceneIdFailure = SETUP_SYNCED_SCENE(pedFranklin(), pedBadGuy3BoxThrower, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboardfailure_F", "onYacht_overboardfailure_thugA", FALSE)			
//					iControlBoxes = 0
//					prevSyncPhase = 0.0
//				ENDIF					
				
				SWITCH iControlBoxGuyDeath
				
					CASE 0
						//This puts a minimum of 2 frame between the above anims and the below anims.
						iControlBoxGuyDeath++
					BREAK
					CASE 1
						
						iControlBoxGuyDeath++
					BREAK
									
					CASE 2
						
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDPulledOverboard)
										
							sceneIDPulledOverboard = SETUP_SYNCED_SCENE(pedFranklin(), pedBadGuy3BoxThrower, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboard_pulldown_F", "onYacht_overboard_pulldown_thugA", TRUE, TRUE, SYNCED_SCENE_DONT_INTERRUPT)			
							oiChampagneBox = CREATE_OBJECT(Prop_Champ_Box_01, GET_ENTITY_COORDS(pedBadGuy3BoxThrower))
							PRINTLN("creating box: case 0")
							ATTACH_ENTITY_TO_ENTITY(oiChampagneBox, pedBadGuy3BoxThrower, GET_PED_BONE_INDEX(pedBadGuy3BoxThrower, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)
							iControlBoxes = 0
							prevSyncPhase = 0.0		
						
							iControlBoxGuyDeath++
						ENDIF
					
					BREAK
					CASE 3
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDPulledOverboard) >= 0.50
						AND DOES_ENTITY_EXIST(oiChampagneBox)
							PROCESS_ENTITY_ATTACHMENTS(oiChampagneBox)	
							DETACH_ENTITY(oiChampagneBox)
							PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_BOX_HIT_FRANKLIN", oiChampagneBox)
							PLAY_PED_AMBIENT_SPEECH(pedFranklin(), "MELEE_LARGE_GRUNT", SPEECH_PARAMS_FORCE)
							SET_OBJECT_AS_NO_LONGER_NEEDED(oiChampagneBox)
						ENDIF
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDPulledOverboard) >= 0.544//0.27
							
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBadGuy3BoxThrower, "SCREAM_TERROR", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
							PROCESS_ENTITY_ATTACHMENTS(pedBadGuy3BoxThrower)						
							SET_HIGH_FALL_TASK(pedBadGuy3BoxThrower, 10000, 30000)
							
							IF NOT IS_ENTITY_DEAD(pedBadGuy3BoxThrower)
								APPLY_DAMAGE_TO_PED(pedBadGuy3BoxThrower, 1000, TRUE)
							ENDIF
							
							//SET_PED_TO_RAGDOLL(pedBadGuy3BoxThrower, 10000, 60000, TASK_RELAX)
							//SET_ENTITY_HEALTH(pedBadGuy3BoxThrower, 0)
														
							CLEAR_RAGDOLL_BLOCKING_FLAGS(pedBadGuy3BoxThrower,RBF_FALLING)
							
							iControlBoxGuyDeath++
						ENDIF
					BREAK
					
					CASE 4
						PRINTLN("FUCKING FUCK: GET_SYNCHRONIZED_SCENE_PHASE(sceneIDPulledOverboard)", GET_SYNCHRONIZED_SCENE_PHASE(sceneIDPulledOverboard))
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDPulledOverboard) >= 0.99
							sceneIDPulledOverboard = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboardLoop_F", "", FALSE)			
							iControlBoxGuyDeath++
						ENDIF
					BREAK
					
				ENDSWITCH
				
			//ENDIF
		
		ELSE
		
			//Deal with boxes in Sequence variation 1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
				//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 0.5, 0, 255, 0)
				SWITCH iControlBoxes
				
					CASE 0
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.02 //0.179
							oiChampagneBox = CREATE_OBJECT(Prop_Champ_Box_01, GET_ENTITY_COORDS(pedBadGuy3BoxThrower))
							PRINTLN("creating box: sceneId: ", prevSyncPhase)
							ATTACH_ENTITY_TO_ENTITY(oiChampagneBox, pedBadGuy3BoxThrower, GET_PED_BONE_INDEX(pedBadGuy3BoxThrower, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)
							
							SET_ENTITY_LOD_DIST(oiChampagneBox, 300)
							iControlBoxes++
						ENDIF
					BREAK
											
					CASE 1
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.594
							PROCESS_ENTITY_ATTACHMENTS(oiChampagneBox)
							DETACH_ENTITY(oiChampagneBox, TRUE, FALSE)					
							PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_BOX_HIT_FRANKLIN", oiChampagneBox)
							PLAY_PED_AMBIENT_SPEECH(pedFranklin(), "MELEE_LARGE_GRUNT", SPEECH_PARAMS_FORCE)
							
							//SET_ENTITY_DYNAMIC(oiChampagneBox, TRUE)
							//SET_ENTITY_COLLISION(oiChampagneBox, TRUE)
							SET_ENTITY_VELOCITY(oiChampagneBox, GET_ENTITY_VELOCITY(truckYachtPackerTrailer))
							//APPLY_FORCE_TO_ENTITY(oiChampagneBox, APPLY_TYPE_IMPULSE, <<0.0, 0.0, -10.00>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
							SET_OBJECT_AS_NO_LONGER_NEEDED(oiChampagneBox)
							iControlBoxes++
						ENDIF
					BREAK						
					
					CASE 2
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_B4", CONV_PRIORITY_HIGH)
								iControlBoxes++
							ENDIF
						ELSE
							iControlBoxes++
						ENDIF
					BREAK
						
					CASE 3
						IF (GET_SYNCHRONIZED_SCENE_PHASE(sceneId) - prevSyncPhase) < 0.0
							iControlBoxes=0
						ENDIF
					BREAK
				ENDSWITCH
				
				prevSyncPhase = GET_SYNCHRONIZED_SCENE_PHASE(sceneId)
				
			ENDIF
			
			//Deal with boxes in Sequence variation 2
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdB)
				//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 0.5, 255, 0, 0)
				SWITCH iControlBoxes		
				
					CASE 0
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdB) >= 0.007 //0.183 
							oiChampagneBox = CREATE_OBJECT(Prop_Champ_Box_01, GET_ENTITY_COORDS(pedBadGuy3BoxThrower))
							PRINTLN("creating box: sceneIdB: ", prevSyncPhase)
							ATTACH_ENTITY_TO_ENTITY(oiChampagneBox, pedBadGuy3BoxThrower, GET_PED_BONE_INDEX(pedBadGuy3BoxThrower, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)
							
							SET_ENTITY_LOD_DIST(oiChampagneBox, 200)
							iControlBoxes++
						ENDIF
					BREAK
					
					CASE 1
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdB) >= 0.616
							PROCESS_ENTITY_ATTACHMENTS(oiChampagneBox)
							DETACH_ENTITY(oiChampagneBox, TRUE, FALSE)					
							PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_BOX_HIT_FRANKLIN", oiChampagneBox)
							//sPLAY_PED_AMBIENT_SPEECH(pedFranklin(), "MELEE_LARGE_GRUNT", SPEECH_PARAMS_FORCE)
							PLAY_PED_AMBIENT_SPEECH(pedFranklin(), "MELEE_LARGE_GRUNT", SPEECH_PARAMS_FORCE)
							
							//SET_ENTITY_DYNAMIC(oiChampagneBox, TRUE)
							//SET_ENTITY_COLLISION(oiChampagneBox, TRUE)
							SET_ENTITY_VELOCITY(oiChampagneBox, GET_ENTITY_VELOCITY(truckYachtPackerTrailer))
							//APPLY_FORCE_TO_ENTITY(oiChampagneBox, APPLY_TYPE_IMPULSE, <<0.0, 0.0, -10.00>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
							SET_OBJECT_AS_NO_LONGER_NEEDED(oiChampagneBox)
							iControlBoxes++
						ENDIF
					BREAK
					
					CASE 2
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_B4", CONV_PRIORITY_HIGH)
								iControlBoxes++
							ENDIF
						ELSE
							iControlBoxes++
						ENDIF
					BREAK
					
					CASE 3
						IF (GET_SYNCHRONIZED_SCENE_PHASE(sceneIdB) - prevSyncPhase) < 0.0
							iControlBoxes=0
						ENDIF
					BREAK
					
				ENDSWITCH
				
				prevSyncPhase = GET_SYNCHRONIZED_SCENE_PHASE(sceneIdB)
				
				IF IS_PED_INJURED(pedBadGuy3BoxThrower)
					IF DOES_ENTITY_EXIST(oiChampagneBox)
						PROCESS_ENTITY_ATTACHMENTS(oiChampagneBox)
						DETACH_ENTITY(oiChampagneBox, TRUE, FALSE)	
						//SET_OBJECT_AS_NO_LONGER_NEEDED(oiChampagneBox)
						SET_ENTITY_VELOCITY(oiChampagneBox, GET_ENTITY_VELOCITY(truckYachtPackerTrailer))
					ENDIF
				ENDIF
				
			ENDIF
		
		ENDIF
		
		//Deal with boxes in Failure sequence
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)
			//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 0.5, 0, 0, 255)
			SWITCH iControlBoxes		
			
				CASE 0
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure) >= 0.05//1	//53 
						oiChampagneBox = CREATE_OBJECT(Prop_Champ_Box_01, GET_ENTITY_COORDS(pedBadGuy3BoxThrower))
						PRINTLN("creating box: FAILURE: ", prevSyncPhase)
						ATTACH_ENTITY_TO_ENTITY(oiChampagneBox, pedBadGuy3BoxThrower, GET_PED_BONE_INDEX(pedBadGuy3BoxThrower, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)
						PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_BOX_HIT_FRANKLIN", oiChampagneBox)
						
						PLAY_PED_AMBIENT_SPEECH(pedFranklin(), "MELEE_LARGE_GRUNT", SPEECH_PARAMS_FORCE)
						
						SET_ENTITY_LOD_DIST(oiChampagneBox, 200)
						iControlBoxes++
					ENDIF
				BREAK
				
				CASE 1
				
					PRINTLN("GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure) CASE 1:", GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure))
				
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure) >= 0.6
						iControlBoxes++
					ENDIF
				BREAK
				
				CASE 2
					//Allow failure animations to finish before cleaning up
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)
					
						PRINTLN("GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure) CASE 2:", GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure))
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure) >= 0.61
						
							//SCRIPT_ASSERT("bang!")
							//SET_PED_TO_RAGDOLL(pedFranklin(), 0, 60000, TASK_NM_BALANCE)
							
							SET_ENTITY_HEALTH(pedFranklin(), 0)
							
							sceneIdFailure = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								
	//									//Depending how we've have the appropriate guy play his animation
	//									IF DOES_ENTITY_EXIST(pedBadguy2)
	//										IF NOT IS_PED_INJURED(pedBadguy2)
	//											TASK_SYNCHRONIZED_SCENE (pedBadguy2, sceneIdFailure, "MISSFAM1_YACHTBATTLEonYacht01_", "onYacht_failIdle_thug", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
	//										ENDIF
	//									ENDIF
	//									
	//									IF DOES_ENTITY_EXIST(pedBadGuy3BoxThrower)
	//										IF NOT IS_PED_INJURED(pedBadGuy3BoxThrower)
	//											TASK_SYNCHRONIZED_SCENE (pedBadGuy3BoxThrower, sceneIdFailure, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboardLoop_thugA", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
	//										ENDIF
	//									ENDIF
							
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdFailure, FALSE)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIdFailure, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "chassis")  )
					
							
							CLEAR_HELP()
							PRINTLN("FRANKLIN IS DEAD!")
							
							SETTIMERB(0)
							
							iControlBoxes++
						ENDIF
					ENDIF

		
				BREAK
				
	//						
				CASE 3
					IF TIMERB() > 4000
						PROCESS_ENTITY_ATTACHMENTS(oiChampagneBox)
						DETACH_ENTITY(oiChampagneBox, TRUE, FALSE)					
						//SET_ENTITY_DYNAMIC(oiChampagneBox, TRUE)
						//SET_ENTITY_COLLISION(oiChampagneBox, TRUE)
						SET_ENTITY_VELOCITY(oiChampagneBox, GET_ENTITY_VELOCITY(truckYachtPackerTrailer))
						//APPLY_FORCE_TO_ENTITY(oiChampagneBox, APPLY_TYPE_IMPULSE, <<0.0, 0.0, -10.00>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
						SET_OBJECT_AS_NO_LONGER_NEEDED(oiChampagneBox)
						
						SET_ENTITY_HEALTH(pedFranklin(), 0)
						CLEAR_HELP()
						//PRINT_NOW("CMN_FDIED", DEFAULT_GOD_TEXT_TIME, 1)
						MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
						PRINTLN("FRANKLIN IS TOTALLY DEAD!")
						REMOVE_BLIPS_ON_FAIL()
						TRIGGER_MUSIC_EVENT("FAM1_FAIL")
						Mission_Failed()									
					ENDIF
				BREAK
				
			ENDSWITCH
			
			prevSyncPhase = GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure)
			
			IF IS_PED_INJURED(pedBadGuy3BoxThrower)
				DETACH_ENTITY(oiChampagneBox, TRUE, FALSE)					
				SET_ENTITY_VELOCITY(oiChampagneBox, GET_ENTITY_VELOCITY(truckYachtPackerTrailer))
			ENDIF				
		
		ENDIF
		
	IF GET_ENTITY_HEALTH(pedBadGuy3BoxThrower) < 200
		PRINTLN("time to spam: should go into loop")
		IF iControlBoxes  = 0
		AND iControlBoxGuyDeath <= 1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDPulledOverboard)
				IF GET_SYNCED_SCENE_PHASE(sceneIDPulledOverboard) > 0.99//453
					sceneIDPulledOverboard = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboardLoop_F", "", FALSE)			
					iControlBoxes = -1
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC BRING_IN_BOOM_TO_CENTRE() 

	SWING_BOAT_BOOM_TO_RATIO(truckYachtPackerTrailer, fBoomratio)
	IF fBoomratio < 0.85
		fBoomratio = fBoomratio +@ 0.1
	ELSE
		fBoomratio = 0.85
	ENDIF

ENDPROC

BOOL bInitialGodTextPrinted
BOOL bAlternateLines
INT iDialogueTimeOut

TEXT_LABEL_23 currentTextLabel
BOOL bFranklinThrownOverBoard
FLOAT fJumpPhase
BOOL bCarBrokeDownForFailCut = FALSE
//BOOL bJimmyPlayingCustomSeatAnim = FALSE
//BOOL bFranklinIsJumpingOntoCar = FALSE

PROC stageDoChase()
	
//	IF NOT IS_ENTITY_DEAD(carPlayersJeep)	
//		IF (GET_GAME_TIMER() - iTimeOfLastSegmentGrab) > 1000
//		OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(carPlayersJeep), route_path[segmentClosestToPlayer], FALSE) > 50.0
//			//PRINTLN("updating distance segment")
//			segmentClosestToPlayer = GET_CLOSEST_PATH_SECTION_FOR_PLAYER(GET_ENTITY_COORDS(carPlayersJeep), route_path)
//			iTimeOfLastSegmentGrab = GET_GAME_TIMER()
//		ENDIF
//	ENDIF
	
	
//	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
//	AND eChaseStage <= CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR
//		PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
//	ELSE
//		IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedFranklin())
//			PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
//		ENDIF
//	ENDIF
	
	IF NOT IS_GAMEPLAY_CAM_RENDERING()
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2140618
	ENDIF
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.1)
	
	checkForDistanceFromFranklin()
	checkForSetpieces()
	//checkForMastBreak()	
	failConditions()		//Comment out when making set pieces
	
	CONSTANT_DIALOGUE()
	UPDATE_CAR_HORNS()
	UPDATE_AMBIENT_SWEARING()
	MANAGE_TRAILER_CREATION()
	
	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	
	IF GET_SELECTED_PED_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_DLC_PROXMINE
		PRINTLN("@@@@@@@@@@@@@@@@@ DISABLE_CONTROL_ACTION @@@@@@@@@@@@@@@@@@@@@@")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE) 	//Fix 1551838
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_SupressGunfireEvents, TRUE)
//		
//		IF NOT IS_PED_INJURED(pedBadguy1)
//			SET_PED_RESET_FLAG(pedBadguy1,PRF_SupressGunfireEvents, TRUE)
//		ENDIF

		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS_FOR_AMBIENT_PEDS_THIS_FRAME(TRUE)
	ENDIF
	
	IF eChaseStage <= CHASE_FRANKLIN_JUMPS_ON_YACHT
		IF DOES_ENTITY_EXIST(oiBlipObject)
			IF IS_GAMEPLAY_CAM_RENDERING()
				//DRAW_*SPHERE(GET_ENTITY_COORDS(oiBlipObject), 1.5)
				//DRAW_MARKER(MARKER_CYLINDER, GET_ENTITY_COORDS(oiBlipObject) + <<0.0, 0.0, -1.0>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.5, 1.5, 1.5>>, 255, 0, 128, 50)
						
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
		SET_FORCE_HD_VEHICLE(truckYachtPackerTrailer, TRUE)
	ENDIF

	IF NOT IS_PED_INJURED(pedFranklin())
		SET_PED_CAN_PLAY_GESTURE_ANIMS(pedFranklin(), FALSE)
	ENDIF
	
	//mainPlaybackSpeed = 1.0	//Uncomment when making set pieces...
	
	//Stop the player putting roof back up
	//D!SABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSHOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)

	DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
	UPDATE_UBER_PLAYBACK(truckYachtPacker, mainPlaybackSpeed)
	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPackerTrailer)
			SET_PLAYBACK_SPEED(truckYachtPackerTrailer,  mainPlaybackSpeed) 
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carPlayersJeep)
			SET_PLAYBACK_SPEED(carPlayersJeep, mainPlaybackSpeed)
		ENDIF
	ENDIF
	
	//Only see garbage truck on first try
	IF g_family1GarbageTruckSeen = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[1])
			g_family1GarbageTruckSeen = TRUE
		ENDIF
	ENDIF
	
	//Don't see spinning Range rover on first try only second and on etc.
	IF g_Family1BallerSpawnAfter1stTry = FALSE
		If fRecordingProgress > 72600.0000
			g_Family1BallerSpawnAfter1stTry = TRUE
		ENDIF
	ENDIF

	//#IF IS_DEBUG_BUILD	
//		IF bIsTrailerRunning
//			SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
//		ENDIF
	//#ENDIF
	
	dealWithFranklinsCar()
	
	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
	AND NOT IS_ENTITY_DEAD(truckYachtPacker)
	AND NOT IS_ENTITY_DEAD(carPlayersJeep)
	AND NOT IS_PED_INJURED(pedFranklin())
	ENDIF

		SWITCH eChaseStage 
		
			CASE CHASE_SETUP
			
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING) 
			
				ADD_BLIP_FOR_YACHT()
			
				BUILD_UBER_ROUTE_DEBUG_PATH_OVER_MULTIPLE_FRAMES_RESET()
			
				//Set_Replay_Mid_Mission_Stage(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_DO_CHASE)) 
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_DO_CHASE")
				SET_AGGRESSIVE_HORNS(TRUE)
				IF NOT IS_ENTITY_DEAD(truckYachtPacker)
				
				
					START_VEHICLE_HORN(truckYachtPacker, 7000)
					IF NOT IS_ENTITY_VISIBLE(truckYachtPacker)
						SET_ENTITY_VISIBLE(truckYachtPacker, TRUE)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
					IF NOT IS_ENTITY_VISIBLE(truckYachtPackerTrailer)
						SET_ENTITY_VISIBLE(truckYachtPackerTrailer, TRUE)
					ENDIF
				ENDIF
				
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				//SET_ENTITY_LOAD_COLLISION_FLAG(truckYachtPackerTrailer, TRUE)
				
				TASK_LOOK_AT_ENTITY(pedFranklin(), truckYachtPackerTrailer, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
				
				SET_CURRENT_PED_WEAPON(pedFranklin(), WEAPONTYPE_UNARMED, TRUE)
				
				START_AUDIO_SCENE("FAM1_HIGHWAY_CHASE")
				SET_MAX_WANTED_LEVEL(0)
				SETTIMERA(0)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
				//ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
				//TRIGGER_MUSIC_EVENT("FAM1_CHASE_BEGINS")
				bCloseToYachtMusicEvent = FALSE		
				iDealWithFailDialogue = 0
				bInitAccelReading = FALSE
				bPlayInitialDialogue = FALSE		
				bCarBrokeDownForFailCut = FALSE
				bForceCutsceneCameraAsCinematicCamera = TRUE
//				bJimmyPlayingCustomSeatAnim = FALSE
				
				CREATE_FORCED_OBJECT(<<-2005.6289, -441.1416, 11.8253>>, 5.0, PROP_STREETLIGHT_03D, TRUE)
				
				CREATE_FORCED_OBJECT(<<-1958.6633, -479.5325, 12.0568>>, 5.0, PROP_STREETLIGHT_03D, TRUE)
				
				CREATE_FORCED_OBJECT(<<-1911.8635, -517.8719, 11.9657>>, 5.0, PROP_STREETLIGHT_03D, TRUE)
				
				CLEAR_PRINTS()
												
				
				IF bBanterFinishedEarly = FALSE
					bSawBoatConvoPreloaded = TRUE
				ENDIF				
				
				eChaseStage = CHASE_TRIGGER_MUSIC
			BREAK
			
			CASE CHASE_TRIGGER_MUSIC
				IF PREPARE_MUSIC_EVENT("FAM1_START")
					IF TRIGGER_MUSIC_EVENT("FAM1_START")
						
						START_AUDIO_SCENE("FAMILY_1_YACHT_ARRIVES")
						
						
						eChaseStage = CHASE_SAY_INITIAL_LINE
					ENDIF
				ENDIF
			BREAK
			
			CASE CHASE_SAY_INITIAL_LINE
			
				IF bBanterFinishedEarly = TRUE
				AND bSawBoatConvoPreloaded = FALSE
					PRELOAD_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_START", CONV_PRIORITY_VERY_HIGH)
					bSawBoatConvoPreloaded = TRUE
				ENDIF
			
				IF TIMERA() > 500
				AND bSawBoatConvoPreloaded = TRUE
					BEGIN_PRELOADED_CONVERSATION()
					//IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_START", CONV_PRIORITY_VERY_HIGH)
							eChaseStage = CHASE_FRANKLIN_STANDS_UP
					//ENDIF					
				ENDIF
			BREAK

			CASE CHASE_FRANKLIN_STANDS_UP
				
				SHAKE_CAM_POINT_OFFSET(cutsceneCamera, pedFranklin(), <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
				SHAKE_CAM_ROTATION(cutsceneCamera, <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
	
				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	
//				IF bCarBlendingIntoGamePlayAfterCut
//				AND TIMERA() > 2300
//				AND (IS_PLAYER_PUSHING_ANALOGUE_STICKS() OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE))
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carPlayersJeep)
//						STOP_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep)
//						
//					ENDIF
//					bCarBlendingIntoGamePlayAfterCut = FALSE
//				ENDIF
				
			
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				//AND bCarBlendingIntoGamePlayAfterCut = FALSE
				
							
					IF bPlayInitialDialogue = FALSE
					AND bInitialGodTextPrinted = TRUE
						IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_INIT1", CONV_PRIORITY_VERY_HIGH)
								bPlayInitialDialogue = TRUE	
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_INIT2", CONV_PRIORITY_VERY_HIGH)
								bPlayInitialDialogue = TRUE	
							ENDIF
						ENDIF
						SETTIMERA(0)
						
					ENDIF
											
					IF bInitialGodTextPrinted = FALSE
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
							PRINT_NOW("YB_OBJ1B", DEFAULT_GOD_TEXT_TIME, 1)
							bInitialGodTextPrinted = TRUE
						ENDIF
					ENDIF
					
				ENDIF
				
				
				//IF GET_DISTANCE_BETWEEN_ENTITIES(pedFranklin(), truckYachtPackerTrailer) < 100.0
				//IF TIMERA() > 4000
				//AND bPlayInitialDialogue = TRUE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1840.488770,-724.232849,7.849245>>, <<-1738.859009,-591.090881,32.377640>>, 27.500000)
				OR GET_DISTANCE_BETWEEN_ENTITIES(pedFranklin(), truckYachtPackerTrailer, FALSE) < 20.0 //Safety if locate is missed for some reason.
				//AND bCarBlendingIntoGamePlayAfterCut = FALSE
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
						//warp him out of car
						
						REPLAY_RECORD_BACK_FOR_TIME(1.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						
						IF NOT IS_PED_INJURED(pedFranklin())
							SET_PED_CAN_PLAY_GESTURE_ANIMS(pedFranklin(), FALSE)
						ENDIF
						
						CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
						sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleincar01_", "FRANKLINSTANDSINCAR_INTRO", "", TRUE, TRUE, SYNCED_SCENE_NONE)
											
						ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-5.0, -2.0, -0.5>>)
						POINT_CAM_AT_ENTITY(cutsceneCamera, pedFranklin(), <<0.0, 0.0, 0.0>>)
						SET_CAM_FOV(cutsceneCamera, 15.00)
																	
						
						missionStartTime = GET_GAME_TIMER()	
						SET_RADAR_ZOOM(1)
						eChaseStage = CHASE_FRANKLIN_STANDS_LOOP
					ENDIF	
				ENDIF	
			
			BREAK

			CASE CHASE_FRANKLIN_STANDS_LOOP
			
				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
			
				SHAKE_CAM_POINT_OFFSET(cutsceneCamera, pedFranklin(), <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
				SHAKE_CAM_ROTATION(cutsceneCamera, <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
			
				RAGDOLL_AND_FAIL_IF_UPSIDE_DOWN()	
			
				IF GET_SYNCED_SCENE_PHASE(sceneIDclimbOutOfCar) >= 1.0
					sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleincar01_", "FRANKLINSTANDSINCAR_LOOP", "", TRUE, TRUE, SYNCED_SCENE_NONE)
					SET_ENTITY_HEALTH(pedFranklin(), 1000)
					
					bFranklinWaving = FALSE
					//
					eChaseStage = CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR
				ENDIF
				
			BREAK

			CASE CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR
			
				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
			
				SHAKE_CAM_POINT_OFFSET(cutsceneCamera, pedFranklin(), <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
				SHAKE_CAM_ROTATION(cutsceneCamera, <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
				
				//Every 3500 make Franklin wave.
				IF (GET_GAME_TIMER() - missionStartTime) > iRandomWaveTime
				AND bFranklinWaving = FALSE
				AND ACOS(DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(carPlayersJeep), GET_ENTITY_FORWARD_VECTOR(truckYachtPackerTrailer))) < 45.0
				
					IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
						sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleincar01_", "FRANKLINSTANDSINCAR_LOOPD", "", FALSE, TRUE, SYNCED_SCENE_NONE)
					ELSE
						IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
							sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleincar01_", "FRANKLINSTANDSINCAR_LOOPB", "", FALSE, TRUE, SYNCED_SCENE_NONE)
						ELSE
							sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleincar01_", "FRANKLINSTANDSINCAR_LOOPC", "", FALSE, TRUE, SYNCED_SCENE_NONE)
						ENDIF
					ENDIF
//					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//					AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
//					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
//						CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CLOSERF", CONV_PRIORITY_HIGH)
//					ENDIF
					//
					DISABLE_PED_PAIN_AUDIO(pedFranklin(), TRUE)
					bFranklinWaving = TRUE
					
				ELSE
					IF GET_SYNCED_SCENE_PHASE(sceneIDclimbOutOfCar) >= 1.0
						sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleincar01_", "FRANKLINSTANDSINCAR_LOOP", "", FALSE, TRUE, SYNCED_SCENE_NONE)
						
						missionStartTime = GET_GAME_TIMER()
						iRandomWaveTime = GET_RANDOM_INT_IN_RANGE(2500, 5000)
						//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "FAM1AUD", "FAM1_DNTSHT", CONV_PRIORITY_HIGH)
						//		
						bFranklinWaving = FALSE
					ENDIF
				ENDIF
				
				SWITCH iDontShootChat
				
					CASE 0
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_DNTSHT", CONV_PRIORITY_HIGH)
								iDontShootChat++
							ENDIF
						ENDIF
					BREAK
					
					CASE 1
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_RESP1", CONV_PRIORITY_HIGH)
								iDontShootChat++
							ENDIF
						ENDIF
					BREAK
				
				ENDSWITCH
								
				//FAILS
				RAGDOLL_AND_FAIL_IF_UPSIDE_DOWN()	
					
				
				IF GET_CONVERTIBLE_ROOF_STATE(carPlayersJeep) = CRS_LOWERED
				AND GET_DISTANCE_BETWEEN_ENTITIES(pedFranklin(), truckYachtPackerTrailer, FALSE) < 80.0
				AND bFranklinWaving = FALSE
				
					START_AUDIO_SCENE("FAMILY_1_FRANKLIN_CLIMBING")
				
					//HELP_AT_ENTITY("YB_OBJ1B", oiBlipObject, HELP_TEXT_SOUTH, -1, FLOATING_HELP_MISSION_SLOT, HUD_COLOUR_BLACK, HELP_MESSAGE_STYLE_TAGGABLE, 3.0)
					
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					
					sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleincar01_", "FranklinOnBonnet_intro", "", TRUE, TRUE, SYNCED_SCENE_NONE)
												
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-5.0, -2.0, -0.5>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, pedFranklin(), <<0.0, 0.0, 0.0>>)
					SET_CAM_FOV(cutsceneCamera, 15.00)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0)
					
					eChaseStage = CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR_LOOP		
				ENDIF
			
			BREAK
			
			CASE CHASE_FRANKLIN_CLIMBS_OUT_OF_CAR_LOOP
			
				RAGDOLL_AND_FAIL_IF_UPSIDE_DOWN()	
				
				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
				
				SHAKE_CAM_POINT_OFFSET(cutsceneCamera, pedFranklin(), <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
				SHAKE_CAM_ROTATION(cutsceneCamera, <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
				
				//STop constant dialogue for this short section by resetign timer each frame.
				iTimeOfLastConstantDialogue = GET_GAME_TIMER()
				
				IF GET_SYNCED_SCENE_PHASE(sceneIDclimbOutOfCar) >= 0.877 //1.0
					sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", TRUE, "MISSFAM1_YachtBattleincar01_", "FranklinOnBonnet_loop", "", TRUE, TRUE, SYNCED_SCENE_NONE)
//					bToldTojump = FALSE
					bCloseToYachtMusicEvent = FALSE
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM1_QUICK_BOARDING)
					eChaseStage = CHASE_WAIT_A_FRAME_TO_STOP_TWO_ANIM_UPDATES //CHASE_FRANKLIN_JUMPS_ON_YACHT
				ENDIF
					
			BREAK
			
			CASE CHASE_WAIT_A_FRAME_TO_STOP_TWO_ANIM_UPDATES	
				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
				eChaseStage = CHASE_FRANKLIN_JUMPS_ON_YACHT
			BREAK
			
			CASE CHASE_FRANKLIN_JUMPS_ON_YACHT
			
				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
			
				SHAKE_CAM_POINT_OFFSET(cutsceneCamera, pedFranklin(), <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
				SHAKE_CAM_ROTATION(cutsceneCamera, <<0.0, 0.0, 0.0>>, -0.05, 0.05, 2.0, 2.0)
				
				RAGDOLL_AND_FAIL_IF_UPSIDE_DOWN()
				
				//PROCESS_ADAPTIVE_CRUISE_CONTROL()
	
				IF NOT IS_BMW_STEADY_FOR_JUMP(45.0)
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, vTruckRearOffset) ,FALSE) 	< fJumpThreshold
				AND bToldTokeepCarSteady = FALSE
					//DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(PLAYER_PED_ID()), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<1.5, 1.5, 1.5>>)
					PRINT_HELP("YB_HELP1")
					bToldTokeepCarSteady = TRUE
				ENDIF
				
				//If distance is close enough prep for jump...
				IF bCloseToYachtMusicEvent = FALSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, vTruckRearOffset) ,FALSE) < 22.5
						TRIGGER_MUSIC_EVENT("FAM1_CLOSE_YACHT")
						bCloseToYachtMusicEvent = TRUE
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				
					IF bIsSuperDebugEnabled
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, vTruckRearOffset),fJumpThreshold, 0, 0, 255, 127)
					ENDIF
							
				#ENDIF
			
				
				//iDRAW_*SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, vTruckRearOffset), 1.0)
				//IF IS_ENTITY_AT_COORD(carPlayersJeep, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, vTruckRearOffset), <<2.25, 2.25, 3.0>> )	
				//IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, vTruckRearOffset)) 	< fJumpThreshold
				IF IS_FRANKLIN_IN_JUMP_ZONE()
				//AND IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDclimbOutOfCar) //Only jump from correct pose
				//AND IS_VEHICLE_ON_ALL_WHEELS(carPlayersJeep)
				AND IS_BMW_STEADY_FOR_JUMP()
				AND eBalanceState = STATE_JUMP_PREP_CHECK_FOR_CANCEL //I.e. we are in the looping state of Jump prep (checking for cancel..)
				
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)

					IF NOT IS_PED_INJURED(pedFranklin())
						
						CLEAR_HELP()
						
						START_AUDIO_SCENE("FAMILY_1_FRANKLIN_JUMPS")
						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedFranklin(), 4000)
											
						ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-3.3084, -20.4882, -0.6056>>)
						POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-2.2464, -17.6954, -0.3368>>)
						SET_CAM_FOV(cutsceneCamera, 25.0000)
						
														
						//sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleincar01_", "FranklinOnBonnet_yachtJump", "")
						sceneIDclimbOutOfCar = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleincar01_", "FranklinOnBonnet_yachtJump_fail", "", TRUE, TRUE, SYNCED_SCENE_NONE)
						
						TASK_CLEAR_LOOK_AT(pedFranklin())
						
						CLEAR_ALL_FLOATING_HELP()
						PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_JUMPTOYACHT", pedFranklin())
												
						SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						eChaseStage = CHASE_FRANKLIN_BOARDS_INTRO
					ENDIF
				ENDIF
			
				MANAGE_FRANKLIN_ON_BONNET()
				
			BREAK
			
			CASE CHASE_FRANKLIN_BOARDS_INTRO
			
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			
				IF bDialogueForLandingOnYacht = FALSE
				AND PREPARE_MUSIC_EVENT("FAM1_FRANKLIN_JUMPS")
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_JUMP", CONV_PRIORITY_VERY_HIGH)
						bJumpMusicCue = FALSE
						bDialogueForLandingOnYacht = TRUE
					ENDIF
				ENDIF
				
				IF bDialogueForLandingOnYacht = TRUE
					IF bJumpMusicCue = FALSE
						IF PREPARE_MUSIC_EVENT("FAM1_FRANKLIN_JUMPS")
							TRIGGER_MUSIC_EVENT("FAM1_FRANKLIN_JUMPS")
							bJumpMusicCue = TRUE
						ENDIF
					ENDIF
				ENDIF
				
//				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
							
				REQUEST_RAGDOLL_BOUNDS_UPDATE (pedFranklin(),1)
				
				PRINTLN("distance to yacht!:", GET_DISTANCE_BETWEEN_ENTITIES(pedFranklin(), truckYachtPackerTrailer))
				
				
				IF GET_DISTANCE_BETWEEN_ENTITIES(pedFranklin(), truckYachtPackerTrailer) < 11.1
					fJumpPhase = 0.3
				ELSE
					fJumpPhase = 0.50
				ENDIF
				
				IF GET_SYNCED_SCENE_PHASE(sceneIDclimbOutOfCar) >= fJumpPhase//0.47//0.55
				AND IS_FRANKLIN_IN_JUMP_ZONE(1.3, FALSE) 
					
					REPLAY_RECORD_BACK_FOR_TIME(6.5, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					
					STOP_AUDIO_SCENE("FAMILY_1_FRANKLIN_CLIMBING")
					STOP_AUDIO_SCENE("FAMILY_1_YACHT_ARRIVES")
										
					START_AUDIO_SCENE("FAMILY_1_PROTECT_FRANKLIN")
					
					KILL_ANY_CONVERSATION()
					
					bProcessCinematicCamera = TRUE
					
					DISABLE_PED_PAIN_AUDIO(pedFranklin(), FALSE)
					
					//Pre calc position of ideal launch point.
					vecDesiredOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, <<0.0, -5.64535, 0.0>>)				
											
					IF VDIST(vecDesiredOffset, vecDesiredOffset) > 0.0
					ENDIF
					
					//Position of where he actually is on takeoff
					vecFranklinCoords = GET_PED_BONE_COORDS(pedFranklin(), BONETAG_PELVIS, <<0.0, 0.0, 0.0>>)
					vecActualOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(truckYachtPackerTrailer, vecFranklinCoords)	
					vecActualOffset = vecActualOffset - (<<0.0, -5.64535, 0.0>>)
					//sceneId = CREATE_SYNCHRONIZED_SCENE(vecActualOffset, <<0.0, 0.0, 0.0>>)
				
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleincar01_", "franklinonYacht_clingIntro", "")
								
					//Yacht					
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.1273, -5.4712, 3.1607>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<0.1256, -8.0088, 1.5807>>)
					SET_CAM_FOV(cutsceneCamera, 45.0000)					
					//bForceCinematicCamera = TRUE
					
						/////////
						
						IF NOT DOES_CAM_EXIST(initialCam)		
							initialCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						ENDIF
						IF NOT DOES_CAM_EXIST(destinationCam)		
							destinationCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						ENDIF
											
						//Yacht
						ATTACH_CAM_TO_ENTITY(initialCam, truckYachtPackerTrailer, <<-2.4481, -8.7167, -0.4626>>)
						POINT_CAM_AT_ENTITY(initialCam, truckYachtPackerTrailer, <<-0.2318, -8.5614, 1.5534>>)
						SET_CAM_FOV(initialCam, 32.0179)
						
						SET_CAM_ACTIVE(initialCam, TRUE)
						
						SHAKE_CAM(initialCam, "SKY_DIVING_SHAKE", 1.0)
						
						ATTACH_CAM_TO_ENTITY(destinationCam, truckYachtPackerTrailer, <<-2.0769, -9.7101, 0.3741>>)
						POINT_CAM_AT_ENTITY(destinationCam, truckYachtPackerTrailer, <<-0.3513, -7.4178, 1.2502>>)
						SET_CAM_FOV(destinationCam, 31.9652)
						
												
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 1500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
				
						SHAKE_CAM(destinationCam, "SKY_DIVING_SHAKE", 1.0)
				
						SET_CAM_MOTION_BLUR_STRENGTH(destinationCam, 0.1)
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						SET_WIDESCREEN_BORDERS(TRUE,0)
						
						SET_ENTITY_VISIBLE(carPlayersJeep, FALSE)
						
						bProcessCinematicCamera = FALSE
					
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						
						REMOVE_ANIM_DICT("MISSFAM1_YachtBattleincar01_")
					
						fBoomratio = GET_BOAT_BOOM_POSITION_RATIO(truckYachtPackerTrailer)
					
					eChaseStage = CHASE_FRANKLIN_BOARDS_LOOP
					//If the player crashes or gets massively out of position.
				ELSE
					IF GET_SYNCED_SCENE_PHASE(sceneIDclimbOutOfCar) > 0.99
						IF NOT IS_PED_INJURED(pedFranklin())
							PROCESS_ENTITY_ATTACHMENTS(pedFranklin())
							SET_PED_TO_RAGDOLL(pedFranklin(), 0, 60000, TASK_NM_BALANCE) 
						ENDIF
						eChaseStage = CHASE_FAILURE
					ENDIF
				ENDIF		
				
			BREAK
			
			
			CASE CHASE_FRANKLIN_BOARDS_LOOP
				
				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
				
				REQUEST_RAGDOLL_BOUNDS_UPDATE (pedFranklin(),1)
				
				PRINTSTRING("actual offset ") PRINTVECTOR( vecActualOffset) PRINTNL()
				//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPacker, vecActualOffset), 0.4, 255, 0, 0)
			
				//VECTOR vecTarget = <<0.0, 0.0, 0.0>>
			
				vecActualOffset.x += 0.0 - vecActualOffset.x * GET_FRAME_TIME()  * 1.5
				vecActualOffset.y += 0.0 - vecActualOffset.y * GET_FRAME_TIME()  * 1.5
				vecActualOffset.z += 0.0 - vecActualOffset.z * GET_FRAME_TIME()  * 1.5
				
				vecActualOffset = <<0.0, 0.0, 0.0>>
				
				//SET_SYNCHRONIZED_SCENE_ORIGIN(sceneId, vecActualOffset, <<0.0, 0.0, 0.0>>)
					
				IF GET_SYNCED_SCENE_PHASE(sceneId) >= 0.33 //0.25
					
					bProcessCinematicCamera = TRUE
					SET_ENTITY_VISIBLE(carPlayersJeep, TRUE)
					IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					ENDIF
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
					bForceCinematicCamera = FALSE
					bForceCutsceneCameraAsCinematicCamera = FALSE
				ENDIF
				
				BRING_IN_BOOM_TO_CENTRE() 
							
				IF GET_SYNCED_SCENE_PHASE(sceneId) >= 1.0
										
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleincar01_", "onYacht_clingLoop_F", "", TRUE, TRUE, SYNCED_SCENE_DONT_INTERRUPT)
					
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.6101, -10.2165, 0.1842>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.5263, -7.7526, 1.5087>>)
					SET_CAM_FOV(cutsceneCamera, 35.9390)
			
					REQUEST_ANIM_DICT("MISSFAM1_YachtBattleonYacht01_")
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
					bForceCinematicCamera = FALSE
					SET_ENTITY_VISIBLE(carPlayersJeep, TRUE)
					IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					ENDIF
					bForceCutsceneCameraAsCinematicCamera = FALSE
					//PRINT_NOW("YB_OBJ1C", DEFAULT_GOD_TEXT_TIME - 2000, 1) //DEFAULT_GOD_TEXT_TIME, 1)
					eChaseStage = CHASE_FRANKLIN_BOARDS_OUTRO
				ENDIF
				
			
			BREAK
			
			CASE CHASE_FRANKLIN_BOARDS_OUTRO
			
				BRING_IN_BOOM_TO_CENTRE() 
			
				//Small window where the first bad guy wont appear to stop missing cool set piece./
				//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS((truckYachtPackerTrailer)), 3.0)
				IF NOT (fRecordingProgress > 56671.180 AND fRecordingProgress < 62727.270)
				AND HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleonYacht01_")
					IF GET_DISTANCE_BETWEEN_ENTITIES(carPlayersJeep, truckYachtPackerTrailer) < 50.0
					OR bCinematicCameraActive
					//AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								
						eChaseStage = CHASE_STRUGGLE_1
					ENDIF
				ENDIF
			BREAK
			
			//Attach the buddy and the badguy to the yacht
			CASE CHASE_STRUGGLE_1
				pedBadguy1 = CREATE_PED(PEDTYPE_MISSION, G_M_Y_SalvaGoon_01, <<723.9294, -1087.4998, 21.1693>>, 96.2191)
				SET_PED_COMPONENT_VARIATION(pedBadguy1, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedBadguy1, INT_TO_ENUM(PED_COMPONENT,2), 3, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedBadguy1, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedBadguy1, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
				SET_ENTITY_INVINCIBLE(pedBadguy1, TRUE)
				SET_PED_SUFFERS_CRITICAL_HITS(pedBadguy1, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBadguy1, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedBadguy1, FALSE)
				GIVE_WEAPON_TO_PED(pedBadguy1, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedBadguy1, thievesRelGroup)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedBadguy1, FALSE)
				//SET_ENTITY_HEALTH(pedBadguy1, 1000)
				PREPARE_MUSIC_EVENT("FAM1_1ST_ENEMY_OS")
				//SETUP_YACHT_BADGUY(pedBadguy1, TRUE)
				bPedGuy1DoorOpened = FALSE		
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedBadguy1, "YACHTPED1")								
				bPedGuy1Appeared = FALSE
				sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedBadguy1, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_Fight01intro_F", "onYacht_Fight01intro_thug", TRUE, TRUE, SYNCED_SCENE_DONT_INTERRUPT)
										
				eChaseStage = CHASE_STRUGGLE_1_LOOP				
			BREAK
			
			CASE CHASE_STRUGGLE_1_LOOP
				
				IF NOT bMusicEvent1
					IF PREPARE_MUSIC_EVENT("FAM1_1ST_ENEMY_OS")
						TRIGGER_MUSIC_EVENT("FAM1_1ST_ENEMY_OS")
						bMusicEvent1 = TRUE
					ENDIF
				ENDIF
				
				BRING_IN_BOOM_TO_CENTRE() 
						
//				IF NOT IS_ENTITY_DEAD(pedBadguy1)
//					IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedBadguy1), 4.0, TRUE)
//						eChaseStage = CHASE_FAILURE_BLEW_COVER
//					ENDIF
//				ENDIF
				
				IF GET_SYNCED_SCENE_PHASE(sceneId) > 0.1
				AND bPedGuy1DoorOpened = FALSE
					PLAY_ENTITY_ANIM(truckYachtPackerTrailer, "yacht_door_opening", "MISSFAM1_YachtBattle", INSTANT_BLEND_IN, FALSE, FALSE)
					bPedGuy1DoorOpened = TRUE
				ENDIF
				
				IF GET_SYNCED_SCENE_PHASE(sceneId) > 0.35
					IF bPedGuy1Appeared = FALSE
						ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-4.7607, -7.0218, 2.7643>>)
						POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-2.0131, -5.9395, 2.2357>>)
						SET_CAM_FOV(cutsceneCamera, 43.0420)
						KILL_ANY_CONVERSATION()				
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "FAM1AUD", "FAM1_NOISE", CONV_PRIORITY_VERY_HIGH)
					
						bPedGuy1Appeared = TRUE
					ENDIF
				ENDIF
								
				IF GET_SYNCED_SCENE_PHASE(sceneId) >= 1.0
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedBadguy1, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_Fight01loop_F", "onYacht_Fight01loop_thug")
					SETTIMERA(0)
					// 2296784 - removed this as being able to kill the ped at this point can break the struggle animations and also leave franklin stuck in his last pose.
//					IF NOT IS_ENTITY_DEAD(pedBadguy1)
//						SET_ENTITY_INVINCIBLE(pedBadguy1, FALSE)
//					ENDIF
					eChaseStage = CHASE_STRUGGLE_1_UPDATE
				ENDIF
			
			BREAK


			//Wait until correct point in anim and turn bad guy into a ragdoll...
			CASE CHASE_STRUGGLE_1_UPDATE
			
				IF (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) < 100.0)
				OR bCinematicCameraActive
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						SET_SYNCHRONIZED_SCENE_RATE(sceneId, 1.0)
					ENDIF
					
					SWITCH iControlGunshots
				
						CASE 0
						
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.25 //0.101
								KILL_ANY_CONVERSATION()
								IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_WHO", CONV_PRIORITY_HIGH)
									iControlGunshots++
								ENDIF
							ENDIF
						BREAK
				
						CASE 1
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.501
								START_AUDIO_SCENE("FAM1_HIGHWAY_CHASE")
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedBadguy1, "FAM1_BOAT_SHOOTER")
								iControlGunshots++
							ENDIF
						BREAK
					
						CASE 2
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.531
								SET_PED_SHOOTS_AT_COORD(pedBadguy1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<0.1, 0.71, 0.0>>))
								IF NOT IS_ENTITY_DEAD(carPlayersJeep)
									PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_FAKE_RICOCHET", carPlayersJeep)
								ENDIF
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "GENERIC_FRIGHTENED_HIGH", "MICHAEL_NORMAL", SPEECH_PARAMS_FORCE)
								iControlGunshots++
							ENDIF
						BREAK
						
						CASE 3
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.533
								SET_PED_SHOOTS_AT_COORD(pedBadguy1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<0.14, 0.76, 0.0>>))
								IF NOT IS_ENTITY_DEAD(carPlayersJeep)
									PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_FAKE_RICOCHET", carPlayersJeep)
								ENDIF
								iControlGunshots++
							ENDIF
						BREAK
						
						CASE 4
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.561
								SET_PED_SHOOTS_AT_COORD(pedBadguy1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<1.0, 1.0, 0.0>>))
								IF NOT IS_ENTITY_DEAD(carPlayersJeep)
									PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_FAKE_RICOCHET", carPlayersJeep)
								ENDIF
								iControlGunshots++
							ENDIF
						BREAK
						
						CASE 5
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.637
								SET_PED_SHOOTS_AT_COORD(pedBadguy1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<-1.0, 0.0, 0.0>>))
								IF NOT IS_ENTITY_DEAD(carPlayersJeep)
									PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_FAKE_RICOCHET", carPlayersJeep)
								ENDIF
								
								REQUEST_PTFX_ASSET()
								
								iControlGunshots++
							ENDIF
						BREAK
						
						CASE 6
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.7 
								SET_PED_SHOOTS_AT_COORD(pedBadguy1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<0.0, 0.5, 0.0>>))
								IF NOT IS_ENTITY_DEAD(carPlayersJeep)
									PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_FAKE_RICOCHET", carPlayersJeep)
								ENDIF
																
								iControlGunshots++
							ENDIF
						BREAK
						
						CASE 7
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.920
								SET_PED_SHOOTS_AT_COORD(pedBadguy1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carPlayersJeep, <<0.8, 0.7, 0.0>>))
								IF NOT IS_ENTITY_DEAD(carPlayersJeep)
									PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_FAKE_RICOCHET", carPlayersJeep)
								ENDIF
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "GENERIC_FRIGHTENED_HIGH", "MICHAEL_NORMAL", SPEECH_PARAMS_FORCE)
															
								iControlGunshots++
							ENDIF
						BREAK
						
						CASE 8
							IF HAS_PTFX_ASSET_LOADED()
								#IF IS_NEXTGEN_BUILD
									//overheat
									ptfxCarSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_fam1_veh_smoke", carPlayersJeep, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, GET_ENTITY_BONE_INDEX_BY_NAME(carPlayersJeep, "overheat"))
								#ENDIF
								
								#IF NOT IS_NEXTGEN_BUILD
									ptfxCarSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam1_veh_smoke", carPlayersJeep, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								#ENDIF
								SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 0.5)
								SET_VEHICLE_AUDIO_ENGINE_DAMAGE_FACTOR(carPlayersJeep, 0.25)
								iControlGunshots++
							ENDIF	
						BREAK
						
						CASE 9
							//STOP_AUDIO_SCENE("FAM1_BOAT_SHOOTER")
							IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CARSH", CONV_PRIORITY_VERY_HIGH)
									iControlGunshots++	
								ENDIF
							ENDIF
						BREAK
						
					ENDSWITCH
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						SET_SYNCHRONIZED_SCENE_RATE(sceneId, 0.0)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedBadguy1)
					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedBadguy1, PRF_BlockWeaponReactionsUnlessDead, TRUE)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedFranklin())
					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedFranklin(), PRF_BlockWeaponReactionsUnlessDead, TRUE)
				ENDIF
				
				//Throw him off
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer, FALSE) < 50.0
				AND IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedBadguy1), 1.0)
				AND iControlGunshots > 7
				AND (GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.95) 
				OR GET_ENTITY_HEALTH(pedBadguy1) < 200
					IF TIMERA() > 4000
					//OR GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 1.0
					OR GET_ENTITY_HEALTH(pedBadguy1) < 200
						sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedBadguy1, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_Fight01outro_F", "onYacht_Fight01outro_thug", FALSE)								
						START_AUDIO_SCENE("FAM1_THROWN_OVERBOARD")
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneID, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "chassis") )
						
						REPLAY_RECORD_BACK_FOR_TIME(3.5)
						
						eChaseStage = CHASE_STRUGGLE_1_OUTRO
					ENDIF
				
				ENDIF
			
			BREAK
			
			CASE CHASE_STRUGGLE_1_OUTRO
			
				IF NOT IS_ENTITY_DEAD(pedBadguy1)
					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedBadguy1, PRF_BlockWeaponReactionsUnlessDead, TRUE)
				ENDIF
			
				// 2296784 - reordered some checks here to avoid script getting stuck if pedBadguy1 is dead
				IF GET_SYNCED_SCENE_PHASE(sceneId) >=  0.95 //0.99
				
					IF NOT IS_PED_INJURED(pedBadguy1)
						PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_THROWN_OVERBOARD1", pedBadguy1)
						
						//CLEAR_PED_TASKS(pedBadguy1)
						PROCESS_ENTITY_ATTACHMENTS(pedBadguy1)
						SET_PED_TO_RAGDOLL(pedBadguy1, 0, 10000, TASK_RELAX)
						SET_ENTITY_HEALTH(pedBadguy1, 0)
					ENDIF
					
					INFORM_MISSION_STATS_OF_INCREMENT(FAM1_KILLS) 
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<5.5868, -10.9326, 4.0687>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<3.4853, -8.8562, 3.5470>>)
					SET_CAM_FOV(cutsceneCamera, 19.5173)
					
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "FAM1AUD", "FAM1_ARG", CONV_PRIORITY_HIGH)
					REPLAY_RECORD_BACK_FOR_TIME(3.5)
					eChaseStage = CHASE_HIDE_BEHIND_WHEEL_INTRO
				ENDIF
				
			BREAK
			
			CASE CHASE_HIDE_BEHIND_WHEEL_INTRO
				//IF GET_SYNCED_SCENE_PHASE(sceneId) >= 1.00
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht01_",  "onYacht_byWheelIntro_F", "")								
//										
//					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.9949, 1.1417, 3.5440>>)
//					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.8401, -1.8067, 3.0120>>)
//					SET_CAM_FOV(cutsceneCamera, 25.6667)
					
					//Yacht
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-2.1652, -0.7630, 2.7882>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.9706, -3.5107, 2.6373>>)
					SET_CAM_FOV(cutsceneCamera, 34.9040)
								
					eChaseStage = CHASE_HIDE_BEHIND_WHEEL_LOOP
				//ENDIF
			BREAK
			
						
			CASE CHASE_HIDE_BEHIND_WHEEL_LOOP
				IF GET_SYNCED_SCENE_PHASE(sceneId) >= 1.00
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_byWheelLoop_F", "")								
					eChaseStage = CHASE_STRUGGLE_2
				ENDIF
			BREAK

			
			CASE CHASE_STRUGGLE_2		
			
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer, FALSE) < 65.0
			
					pedBadguy2 = CREATE_PED(PEDTYPE_MISSION, G_M_Y_SalvaGoon_01, <<723.9294, -1087.4998, 21.1693>>, 96.2191)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedBadguy2,	"FAMILY_1_BOAT_PEDS")
					SET_PED_COMPONENT_VARIATION(pedBadguy2, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedBadguy2, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(pedBadguy2, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedBadguy2, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(pedBadguy2, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
					SET_PED_COMPONENT_VARIATION(pedBadguy2, PED_COMP_SPECIAL2, 0, 0, 0) //(lowr)
					//SET_ENTITY_HEALTH(pedBadguy2, 1000)
					GIVE_WEAPON_TO_PED(pedBadguy2, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedBadguy2, thievesRelGroup)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedBadguy2, FALSE)
					
					SET_PED_DROPS_WEAPONS_WHEN_DEAD(pedBadguy2, FALSE)
					
					REQUEST_PTFX_ASSET()
					
					INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(pedBadguy2)
					
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedBadguy2, "YACHTPED2")	
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBadguy2, TRUE)
					SET_PED_SUFFERS_CRITICAL_HITS(pedBadguy2, FALSE)					
				
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedBadguy2, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_fight02Intro_F", "onYacht_fight02Intro_thug", FALSE)								
					
					START_AUDIO_SCENE("FAMILY_1_SHOOT_THE_ENEMY_02")
					
					//PRINT_HELP("YB_HELP4") //~S~Press ~PAD_LEFTSHOULDER~ to shoot while driving. ~Use PAD_RSTICK~ to aim.

					PLAY_ENTITY_ANIM(truckYachtPackerTrailer, "yacht_door_opening", "MISSFAM1_YachtBattle", INSTANT_BLEND_IN, FALSE, FALSE)

					REPLAY_RECORD_BACK_FOR_TIME(1.0)
					bStopScenePoppingToSideOfBoat = FALSE
					eChaseStage = CHASE_STRUGGLE_2_LOOP
				ENDIF
			BREAK
			
			//Wait for Intro to end before starting loop
			CASE CHASE_STRUGGLE_2_LOOP
				
				DEAL_WITH_RAGE_HELP_TEXT()
				
				IF GET_SYNCED_SCENE_PHASE(sceneId) > 0.0
				AND GET_SYNCED_SCENE_PHASE(sceneId) < 0.2
				AND bMichaelWarnsFranklinOfGuy2 = FALSE	
				
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HIDE", CONV_PRIORITY_HIGH)
						bMichaelWarnsFranklinOfGuy2 = TRUE
					ENDIF
				ENDIF
				
//				IF NOT IS_PED_INJURED(pedBadguy2)	//Script duplicated below!
//				AND GET_SYNCED_SCENE_PHASE(sceneId) < 0.735// 0.6 - 1742597
//					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedBadguy2, PRF_BlockWeaponReactionsUnlessDead, TRUE)
//				ENDIF		
				
				//Yacht
				IF GET_SYNCED_SCENE_PHASE(sceneId) >= 0.35
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-3.0120, -2.7163, 2.3151>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.9770, -5.5249, 2.5155>>)
					SET_CAM_FOV(cutsceneCamera, 37.4049)
					bForceCutsceneCameraAsCinematicCamera = TRUE
				ENDIF
			
				IF GET_SYNCED_SCENE_PHASE(sceneId) >= 1.0	
					//bForceCinematicCamera = FALSE
					
					
					IF DOES_BLIP_EXIST(blippedBadguy2)
						SET_BLIP_AS_FRIENDLY(blippedBadguy2, FALSE)
					ENDIF
					SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_NOTHING)
					
					//Start a timer for Failure
					SETTIMERA(0)
							
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedBadguy2, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_fight02Loop_f", "onYacht_fight02Loop_thug")								
					
					IF IS_PED_INJURED(pedBadguy2)
						PRINTSTRING("Badguy dead skipping intro") PRINTNL()
						eChaseStage = CHASE_STRUGGLE_2_OUTRO
					ENDIF
						
					REQUEST_MODEL(IG_JIMMYDISANTO)
					REQUEST_ANIM_DICT("MISSFAM1_YachtBattleonYacht02_")
										
//					//Yacht
//					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-3.0120, -2.7163, 2.3151>>)
//					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.9770, -5.5249, 2.5155>>)
//					SET_CAM_FOV(cutsceneCamera, 37.4049)
					
					//Yacht
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-3.3454, -1.9978, 2.5094>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.6431, -4.4679, 2.4869>>)
					SET_CAM_FOV(cutsceneCamera, 36.8337)
					
					bForceCutsceneCameraAsCinematicCamera = TRUE
					eChaseStage = CHASE_STRUGGLE_2_OUTRO
				ENDIF
		
				//If the intro is running set ped dont die flag during early part...
				//IF GET_SYNCED_SCENE_PHASE(sceneId) < 0.75
					IF NOT IS_PED_INJURED(pedBadguy2)
						SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedBadguy2, PRF_BlockWeaponReactionsUnlessDead, TRUE)
					ENDIF
				//ENDIF
				
				IF TIMERB() > 2500
				AND GET_SYNCED_SCENE_PHASE(sceneId) > 0.535//42
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) < 70.0
					//SCRIPT_ASSERT("fuck sticks 1")
					//IF IS_MESSAGE_BEING_DISPLAYED()
					IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)	
						IF GET_RANDOM_BOOL()
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "FAM1_DDAA", "FRANKLIN", SPEECH_PARAMS_FORCE_FRONTEND)
							SETTIMERB(0)
						ELSE
							//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "FAM1_DHAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_ATTACK", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								SETTIMERB(0)
							ENDIF
						ENDIF
					ELSE
						IF GET_RANDOM_BOOL()
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_ENC4", CONV_PRIORITY_VERY_HIGH)
								SETTIMERB(0)
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_ATTACK", CONV_PRIORITY_VERY_HIGH)
								SETTIMERB(0)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				IF GET_SYNCED_SCENE_PHASE(sceneId) > 0.745
					bStopScenePoppingToSideOfBoat = TRUE
				ENDIF
				
				//IN case he is killed during the intro...
				IF NOT IS_ENTITY_DEAD(pedBadguy2)
				AND bStopScenePoppingToSideOfBoat = TRUE
					IF GET_ENTITY_HEALTH(pedBadguy2) < 200
					OR IS_PED_RAGDOLL(pedBadguy2)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_FRIGHTENED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)	
					
						SET_ENTITY_HEALTH(pedBadguy2, 0)
					
						REMOVE_BLIP(blippedBadguy2)
						SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_BOTH)
						
						PRINT_NOW("YB_OBJ1", DEFAULT_GOD_TEXT_TIME, 1)
		
						sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YACHTBATTLEonYacht01_", "onYacht_fight02Success_f", "")								
			
						IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
							//INFORM_STAT_FAMILY_ONE_SPECIAL_ABILITY_KILL_EXECUTED()
						ENDIF
						
						iTimeOfBadGuy2Death = GET_GAME_TIMER()
						bForceCutsceneCameraAsCinematicCamera = FALSE
						eChaseStage = CHASE_HIDE_BEHIND_WHEEL_2_INTRO //CHASE_MEET_JIMMY
										
					ENDIF
				ENDIF
	
			BREAK
						
				
			CASE CHASE_STRUGGLE_2_OUTRO
			
				DEAL_WITH_RAGE_HELP_TEXT()
			
				IF NOT IS_ENTITY_DEAD(pedBadguy2)
					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedBadguy2, PRF_BlockWeaponReactionsUnlessDead, TRUE)
				ENDIF
						
				IF NOT IS_PED_INJURED(pedFranklin())
				AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				//AND NOT IS_PED_INJURED(pedBadguy2)	
					PRINTFLOAT(GET_SYNCED_SCENE_PHASE(sceneId)) PRINTSTRING("-")
									
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										
						SWITCH iWrestleDialogue
							CASE 0
								IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_FIGHT", CONV_PRIORITY_HIGH)
									iWrestleDialogue++
								ENDIF
							BREAK
							
//							CASE 1
//								IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_B_WIN", CONV_PRIORITY_HIGH)
//									iWrestleDialogue++
//								ENDIF
//							BREAK
//							
//							CASE 2
//								IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_F_WIN", CONV_PRIORITY_HIGH)
//									iWrestleDialogue++
//								ENDIF
//							BREAK
							
						ENDSWITCH
						
					ENDIF
									
					IF TIMERA() > 35000
					AND NOT IS_PED_INJURED(pedBadguy2)
					AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)
						sceneIdFailure = SETUP_SYNCED_SCENE(pedFranklin(), pedBadguy2, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_fight02Failure_f", "onYacht_fight02Failure_thug")								
						ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.9076, -5.6960, 5.4957>>)
						POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.9315, -5.7537, 2.4963>>)
						SET_CAM_FOV(cutsceneCamera, 45.0000)
						REMOVE_BLIPS_ON_FAIL()
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedFranklin())
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_FRIGHTENED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)	
						
					ENDIF	
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)
						IF GET_SYNCED_SCENE_PHASE(sceneIdFailure) > 0.7
						AND bFranklinThrownOverBoard = FALSE	
						
							REPLAY_RECORD_BACK_FOR_TIME(3.5)
						
							PROCESS_ENTITY_ATTACHMENTS(pedFranklin())
							SET_PED_TO_RAGDOLL(pedFranklin(), 0, 10000, TASK_RELAX)
							SET_ENTITY_HEALTH(pedFranklin(), 0)
							WAIT(2000)
							MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
							Mission_Failed()
							bFranklinThrownOverBoard = TRUE
						ENDIF
					ENDIF
					
				
					IF TIMERB() > 2500
					AND NOT IS_PED_INJURED(pedBadguy2)
					AND NOT IS_PED_INJURED(pedFranklin())
					AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) < 70.0
						//SCRIPT_ASSERT("fuck sticks 1")
						//IF IS_MESSAGE_BEING_DISPLAYED()
						//AND IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)	
						
						IF GET_RANDOM_BOOL()
							//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "FAM1_DDAA", "FRANKLIN", SPEECH_PARAMS_FORCE_FRONTEND)
							SETTIMERB(0)
						ELSE
							//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "FAM1_DGAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)
						ENDIF
						
					ENDIF
				
					//The player shoots the thug as intended.....
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)	//Failure hasn't kicked in...
							
						IF NOT IS_ENTITY_DEAD(pedBadguy2)
						ENDIF
											
						IF GET_ENTITY_HEALTH(pedBadguy2) < 200
						OR (iRageText > 0 AND IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedBadguy2, BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 1.0, TRUE))
						#IF IS_DEBUG_BUILD
						OR IS_KEYBOARD_KEY_PRESSED(KEY_K)
						#ENDIF
						
							KILL_FACE_TO_FACE_CONVERSATION()
						
							REPLAY_RECORD_BACK_FOR_TIME(3.5)
						
//							IF NOT IS_PED_RAGDOLL(pedBadguy2)
//								//APPLY_DAMAGE_TO_PED(pedBadguy2, 101, TRUE)
//								SET_PED_TO_RAGDOLL(pedBadguy2, 0, 10000, TASK_RELAX)
//							ENDIF
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedFranklin())						
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_FRIGHTENED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)	
						
							REMOVE_BLIP(blippedBadguy2)
							SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_BOTH)
							CLEAR_HELP(TRUE)
							PRINT_NOW("YB_OBJ1", DEFAULT_GOD_TEXT_TIME, 1)
			
							sceneId = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							TASK_SYNCHRONIZED_SCENE (pedFranklin(), sceneID, "MISSFAM1_YACHTBATTLEonYacht01_", "onYacht_fight02Success_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)							
							IF  NOT IS_PED_INJURED(pedBadguy2)
							AND NOT IS_PED_RAGDOLL(pedBadguy2)
								TASK_SYNCHRONIZED_SCENE(pedBadguy2, sceneID, "MISSFAM1_YACHTBATTLEonYacht01_", "onYacht_fight02Success_thug", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
																
								//SET_PED_DROPS_WEAPON(pedBadguy2)
							ELSE
								SET_ENTITY_HEALTH(pedBadguy2, 0)
								PRINTSTRING("Badguy dead skipping OUTRO") PRINTNL()
								eChaseStage = CHASE_HIDE_BEHIND_WHEEL_2_INTRO
							ENDIF
							
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, FALSE)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneID, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "chassis")  )
							
							//Yacht
							ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<0.4170, -8.8525, 2.7537>>)
							POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.7230, -6.0810, 2.6157>>)
							SET_CAM_FOV(cutsceneCamera, 29.0725)
							//bForceCinematicCamera = TRUE
							
							IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
								//INFORM_STAT_FAMILY_ONE_SPECIAL_ABILITY_KILL_EXECUTED()
							ENDIF
							bForceCutsceneCameraAsCinematicCamera = FALSE	
							eChaseStage = CHASE_HIDE_BEHIND_WHEEL_2_INTRO
						ENDIF
						
					ENDIF	
					
				ENDIF
			
			BREAK
			
			
			CASE CHASE_HIDE_BEHIND_WHEEL_2_INTRO
				
				IF IS_AUDIO_SCENE_ACTIVE("FAMILY_1_SHOOT_THE_ENEMY_02")
					STOP_AUDIO_SCENE("FAMILY_1_SHOOT_THE_ENEMY_02")
				ENDIF
				
				//SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())							
				
				DEAL_WITH_RAGE_HELP_TEXT()

				//Make sure Dying pedBadGuy2 doesn't get shot during his animation.
				IF NOT IS_ENTITY_DEAD(pedBadguy2)
					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedBadguy2, PRF_BlockWeaponReactionsUnlessDead, TRUE)
				ENDIF
				
				//Guy dying "cutscene"
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.02
					AND bFakeBloodInCutscene = FALSE
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_fam1_blood_headshot", GET_PED_BONE_COORDS(pedBadguy2, BONETAG_HEAD, <<0.0, 0.0, 0.0>>), <<0, 0, 0>>, 1.0)	
						bFakeBloodInCutscene = TRUE
					ENDIF
					
//					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.20
//						
//						bForceCinematicCamera = FALSE
//						bForceCutsceneCameraAsCinematicCamera = FALSE
//					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 1.0		
						
//						SET_ENTITY_HEALTH(pedBadguy2, 0)
						//Set to end pose			
						IF NOT IS_PED_INJURED(pedBadguy2)
						AND NOT IS_PED_RAGDOLL(pedBadguy2)
							sceneIDKeepBodyOnBoat = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							
							TASK_SYNCHRONIZED_SCENE (pedBadguy2, sceneIDKeepBodyOnBoat, "MISSFAM1_YACHTBATTLEonYacht01_", "onYacht_fight02Success_thug", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneIDKeepBodyOnBoat, FALSE)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIDKeepBodyOnBoat, TRUE)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDKeepBodyOnBoat, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "chassis")  )
							SET_SYNCHRONIZED_SCENE_PHASE(sceneIDKeepBodyOnBoat, 0.99)
						ELSE
							IF NOT IS_ENTITY_DEAD(pedBadguy2)
								SET_ENTITY_HEALTH(pedBadguy2, 0)
							ENDIF
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(3.5)
						
						sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht01_", "onYacht_byWheelLoop02_F", "", FALSE)
						
						//Set_Replay_Mid_Mission_Stage(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_JIMMY_APPEARS)) 
						eChaseStage = CHASE_MEET_JIMMY
					ENDIF
				ENDIF
			BREAK
			
			CASE CHASE_MEET_JIMMY
			
				DEAL_WITH_RAGE_HELP_TEXT()
			
				REQUEST_MODEL(IG_JIMMYDISANTO)
				REQUEST_ANIM_DICT("MISSFAM1_YachtBattleonYacht02_")
				REQUEST_ANIM_DICT("MISSFAM1_YachtBattle")
				REQUEST_MODEL(Prop_Champ_Box_01)
				
				
				//Creat Jimmy early as his fucking variations dont load in.
				IF HAS_MODEL_LOADED(IG_JIMMYDISANTO)
				AND NOT DOES_ENTITY_EXIST(pedJimmy)
					CREATE_JIMMY()
					ATTACH_ENTITY_TO_ENTITY(pedJimmy, truckYachtPackerTrailer, 0, <<0.0, 0.0,0.0>>, <<0.0, 0.0, 0.0>>)
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedJimmy)
				AND NOT IS_ENTITY_DEAD(pedJimmy)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedJimmy)
				AND NOT IS_PED_INJURED(pedFranklin())
				AND NOT IS_ENTITY_DEAD(truckYachtPacker)
				AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				AND HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleonYacht02_")
				AND HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattle")
					
					IF ((GET_GAME_TIMER() - iTimeOfBadGuy2Death) > iDelayAfterBadGuysDeath //1200
					AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) < 60.0
					AND (fRecordingProgress < 88132.0 OR fRecordingProgress > 91399.0)) 	//Stop Jimmy swinging through a truck
					OR IS_SCREEN_FADED_OUT()
						IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_FINDJIM", CONV_PRIORITY_VERY_HIGH)
						
							//Kill bad guy so he doesnt come back and fuck up the synced scene.
							INFORM_MISSION_STATS_OF_INCREMENT(FAM1_KILLS) 
							bForceCinematicCamera = FALSE
							bForceCutsceneCameraAsCinematicCamera = FALSE
							
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_JIMMY_APPEARS")
							
							DETACH_ENTITY(pedJimmy, FALSE, FALSE)						
							IF IS_ENTITY_ATTACHED_TO_ANY_OBJECT(pedFranklin())
								DETACH_ENTITY(pedFranklin(), FALSE, FALSE)
							ENDIF
							sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedJimmy, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_meetJimmy_F", "onYacht_meetJimmy_J", FALSE)
																		
							ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.1790, -14.9702, 3.4881>>)
							POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.1063, -11.9819, 3.2332>>)
							SET_CAM_FOV(cutsceneCamera, 13.5778)
							
							PLAY_ENTITY_ANIM(truckYachtPackerTrailer, "yacht_door_opening", "MISSFAM1_YachtBattle", INSTANT_BLEND_IN, FALSE, FALSE)
							
							SETTIMERA(0)
							
							//For replay purposes...
							IF IS_SCREEN_FADED_OUT()
								CREATE_ALL_WAITING_UBER_CARS()
								//WAIT(100)
								CLEAR_PRINTS()
								FADE_IN_IF_NEEDED()
								IF NOT IS_ENTITY_DEAD(carPlayersJeep)
									STOP_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep)
									SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 30.0)
									//SCRIPT_ASSERT("sakes!")
								ENDIF
							ENDIF
							
							//SWING_IN_BOAT_BOOM(truckYachtPackerTrailer)
							
							fBoomratio = GET_BOAT_BOOM_POSITION_RATIO(truckYachtPackerTrailer)
							//iTimeOfMeetingIntroAnims = GET_GAME_TIMER()
							bJimmySaysdadSpeedh = FALSE
							bMeetingConvoPreloaded = FALSE
							eChaseStage = CHASE_SIGNAL_PLAYER
							
						ENDIF
					ENDIF	
				ENDIF
				
			BREAK
			
			CASE CHASE_SIGNAL_PLAYER
								
				BRING_IN_BOOM_TO_CENTRE() 
				
				//Prestream FAM1_JIMX
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND bMeetingConvoPreloaded = FALSE
					
					IF iTimeOfPreloadedJimXSpeech < 0
						iTimeOfPreloadedJimXSpeech = GET_GAME_TIMER()
					ENDIF
					
					IF PRELOAD_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_JIMX", CONV_PRIORITY_VERY_HIGH)
						bMeetingConvoPreloaded = TRUE
					ENDIF
				ENDIF
				
				IF bMeetingConvoPreloaded = TRUE
					PRINTLN("GET_SYNCED_SCENE_PHASE(sceneId):", GET_SYNCED_SCENE_PHASE(sceneId))
					IF GET_SYNCED_SCENE_PHASE(sceneId) > 0.2
						IF bJimmySaysdadSpeedh = FALSE
							
							BEGIN_PRELOADED_CONVERSATION()
														
							bJimmySaysdadSpeedh = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF ARE_STRINGS_EQUAL("FAM1_JIMX", currentRootLabel) 
					bMastConvoPlayingOrPlayed = TRUE
				ENDIF
				
				REQUEST_MODEL(IG_JIMMYDISANTO)
				REQUEST_ANIM_DICT("MISSFAM1_YachtBattleonYacht02_")
				
				IF NOT IS_PED_INJURED(pedFranklin())
				AND NOT IS_PED_INJURED(pedJimmy)
				AND NOT IS_ENTITY_DEAD(truckYachtPacker)
				AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				AND HAS_MODEL_LOADED(IG_JIMMYDISANTO)	
				AND HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleonYacht02_")
				
				AND bJimmySaysdadSpeedh OR (	iTimeOfPreloadedJimXSpeech > 0
											AND GET_GAME_TIMER() - iTimeOfPreloadedJimXSpeech > 10000)
				
					PRINTLN("in here:\n")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						PRINTLN("and in here\n", GET_SYNCHRONIZED_SCENE_PHASE(sceneId))
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 1.0
							
							//If timeout hits and the convo didn't start kill it.
							IF bMeetingConvoPreloaded
							AND NOT bJimmySaysdadSpeedh
								KILL_ANY_CONVERSATION()
							ENDIF
							
							bMastConvoPlayingOrPlayed = FALSE
							sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedJimmy, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_signalPlayer_loop_F", "onYacht_signalPlayer_loop_J", FALSE)
							SETTIMERA(0)
							eChaseStage = CHASE_BOOM_SWING
						ENDIF
					ENDIF
				ENDIF
			
				SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedJimmy, PRF_BlockWeaponReactionsUnlessDead, TRUE)
				
			BREAK
			
			CASE CHASE_BOOM_SWING
			
				SWING_BOAT_BOOM_TO_RATIO(truckYachtPackerTrailer, fBoomratio)
				
				IF fBoomratio < 0.85
					fBoomratio = fBoomratio +@ 1.0
				ELSE
					fBoomratio = 0.85
				ENDIF
						
				currentTextLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				
				currentRootLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
				IF ARE_STRINGS_EQUAL("FAM1_JIMX", currentRootLabel) 
				OR GET_GAME_TIMER() - iTimeOfPreloadedJimXSpeech > 10000
					bMastConvoPlayingOrPlayed = TRUE
				ENDIF
						
				PRINTLN("currentTextLabel: ", currentTextLabel)
			
				REQUEST_MODEL(IG_JIMMYDISANTO)
				REQUEST_ANIM_DICT("MISSFAM1_YachtBattleonYacht02_")
				
				IF NOT IS_PED_INJURED(pedFranklin())
				AND NOT IS_PED_INJURED(pedJimmy)
				AND NOT IS_ENTITY_DEAD(truckYachtPacker)
				AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				AND HAS_MODEL_LOADED(IG_JIMMYDISANTO)
				AND HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleonYacht02_")
				AND bMastConvoPlayingOrPlayed
				AND (NOT ARE_STRINGS_EQUAL("FAM1_JIMX_1", currentTextLabel)
					AND NOT ARE_STRINGS_EQUAL("FAM1_JIMX_2", currentTextLabel)
					AND NOT ARE_STRINGS_EQUAL("FAM1_JIMX_3", currentTextLabel)
					AND NOT ARE_STRINGS_EQUAL("FAM1_JIMX_4", currentTextLabel))
				// Rob B - 2044882 - try and prevent boom moving if jimx has been started but not even hit line 1 yet
				AND (ARE_STRINGS_EQUAL("FAM1_JIMX_5", currentTextLabel)
					OR ARE_STRINGS_EQUAL("FAM1_JIMX_6", currentTextLabel)
					OR ARE_STRINGS_EQUAL("FAM1_JIMX_7", currentTextLabel)
					OR ARE_STRINGS_EQUAL("FAM1_JIMX_8", currentTextLabel)
					OR ARE_STRINGS_EQUAL("FAM1_JIMX_9", currentTextLabel)
					OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
					
					// Rob B - 2044882 - kill any conversation that's not jimx
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT ARE_STRINGS_EQUAL("FAM1_JIMX", currentRootLabel)
							KILL_ANY_CONVERSATION()
						ENDIF	
					ENDIF
						
					//Kill bad guy so he doesnt come back and fuck up the synced scene.
//					DELETE_PED(pedBadguy2)

					REPLAY_RECORD_BACK_FOR_TIME(4.5, 4, REPLAY_IMPORTANCE_HIGHEST)

											
					pedBadGuy3BoxThrower = CREATE_PED(PEDTYPE_MISSION, G_M_Y_SalvaGoon_01, GET_ENTITY_COORDS(truckYachtPackerTrailer), 96.2191)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedBadGuy3BoxThrower,	"FAMILY_1_BOAT_PEDS")
					SET_PED_COMPONENT_VARIATION(pedBadGuy3BoxThrower, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedBadGuy3BoxThrower, INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(pedBadGuy3BoxThrower, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedBadGuy3BoxThrower, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)

					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBadGuy3BoxThrower, TRUE)
					//SET_ENTITY_HEALTH(pedBadGuy3BoxThrower, 1000)
					SET_PED_SUFFERS_CRITICAL_HITS(pedBadGuy3BoxThrower, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedBadGuy3BoxThrower, thievesRelGroup)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedBadGuy3BoxThrower, FALSE)
					
					SET_ENTITY_LOD_DIST(pedBadGuy3BoxThrower, 200)
					
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 5, pedBadGuy3BoxThrower, "YACHTPED4")	
					
					INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(pedBadGuy3BoxThrower)
		
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedBadGuy3BoxThrower, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_hitByBoom_F", "onYacht_hitByBoom_thugA", TRUE)	
					//sceneIdBoomJimmy =
					
					sceneIdSon = SETUP_SYNCED_SCENE(pedJimmy, NULL, truckYachtPackerTrailer, "misc_e", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_hitByBoom_J", "", FALSE)
					SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIdSon, <<0.0, 0.0, 0.08>>, <<0.0, 0.0, 0.0>>)
					bJimmyBoomHitIntroAnimComplete = FALSE
					//May need synced version.	
					
					//SWING_BOAT_BOOM_TO_RATIO(truckYachtPackerTrailer, 0.0)
					
					START_AUDIO_SCENE("FAMILY_1_SAVE_JIMMY")
					
					PLAY_ENTITY_ANIM(truckYachtPackerTrailer,"onYacht_hitByBoom_boom", "MISSFAM1_YACHTBATTLEonYacht02_", 8.0, FALSE, TRUE, TRUE)	
					SWING_BOAT_BOOM_FREELY(truckYachtPackerTrailer, TRUE)
					ALLOW_BOAT_BOOM_TO_ANIMATE(truckYachtPackerTrailer, TRUE)
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(truckYachtPackerTrailer)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
					
					//PLAY_SYNCHRONIZED_ENTITY_ANIM(truckYachtPackerTrailer, sceneID, "BOOM_SWING", "MISSWIP", INSTANT_BLEND_IN)
					PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_BOOM_SWING1", truckYachtPackerTrailer)
					START_AUDIO_SCENE("FAM1_BOOM_SWING")
											
					SET_PED_RELATIONSHIP_GROUP_HASH(pedJimmy, RELGROUPHASH_PLAYER)
				
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.2979, -8.6807, 2.8214>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.5470, -5.9555, 2.7092>>)
					SET_CAM_FOV(cutsceneCamera, 28.0593)
					SETTIMERB(0)						
					SETTIMERA(0)
				
					REMOVE_ANIM_DICT("MISSFAM1_YachtBattleonyacht01_")
					
					//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBadGuy3BoxThrower, "FAM1_AYAA", "JIMMY", SPEECH_PARAMS_FORCE_FRONTEND)
					
					//new dialogue wait until its in the build. FAM1_BOOM
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBadGuy3BoxThrower, "FAM1_HCAA", "JIMMY", SPEECH_PARAMS_FORCE_FRONTEND)
					
					//PLAY_PAIN(pedJImmy, AUD_DAMAGE_REASON_SCREAM_TERROR)
					eChaseStage = CHASE_BOOM_SWING_LOOP
			
					
				ENDIF
			
				IF DOES_ENTITY_EXIST(pedBadGuy3BoxThrower)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						//The point where he throw's Franklin over the edge (now we can shoot him)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) < 0.659
							IF NOT IS_ENTITY_DEAD(pedBadGuy3BoxThrower)
								SET_ENTITY_INVINCIBLE(pedBadGuy3BoxThrower, TRUE)
								SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedBadGuy3BoxThrower, PRF_BlockWeaponReactionsUnlessDead, TRUE)
							ENDIF
						ELSE
							SET_ENTITY_INVINCIBLE(pedBadGuy3BoxThrower, FALSE)
						ENDIF
					ELSE
						SET_ENTITY_INVINCIBLE(pedBadGuy3BoxThrower, FALSE)
					ENDIF
				ENDIF
				
				SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedJimmy, PRF_BlockWeaponReactionsUnlessDead, TRUE)
			
			BREAK

			//C_LOOP
			CASE CHASE_BOOM_SWING_LOOP
			
				SWING_BOAT_BOOM_FREELY(truckYachtPackerTrailer, TRUE)
				ALLOW_BOAT_BOOM_TO_ANIMATE(truckYachtPackerTrailer, TRUE)
			
				IF NOT bJimmyIsHitByBoom
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HELP", CONV_PRIORITY_VERY_HIGH)
						bJimmyIsHitByBoom = TRUE
					ENDIF
				ENDIF
				
				IF NOT bMusicEvent2
					IF PREPARE_MUSIC_EVENT("FAM1_JIMMY_BOOM")
						TRIGGER_MUSIC_EVENT("FAM1_JIMMY_BOOM")
						bMusicEvent2 = TRUE
					ENDIF
				ENDIF
			
				UPDATE_JIMMYS_LEGS()
			
				IF NOT IS_PED_INJURED(pedFranklin())				
				AND NOT IS_ENTITY_DEAD(truckYachtPacker)
				AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
								
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 0.35
							ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-9.1614, 5.5768, 2.9331>>)
							POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-7.2843, 3.2468, 2.7138>>)
							SET_CAM_FOV(cutsceneCamera, 30.4968)
						ENDIF						
					ENDIF
					
					IF NOT IS_PED_INJURED(pedBadGuy3BoxThrower)
						IF IS_PED_RAGDOLL(pedBadGuy3BoxThrower)
							SET_ENTITY_HEALTH(pedBadGuy3BoxThrower, 0)
						ENDIF
					ENDIF
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 1.0
						OR IS_PED_INJURED(pedBadGuy3BoxThrower)
														
							SET_PED_LOD_MULTIPLIER(pedBadGuy3BoxThrower, 2.0)
							
							IF NOT IS_PED_INJURED(pedBadGuy3BoxThrower)
								sceneId = SETUP_SYNCED_SCENE(pedFranklin(), pedBadGuy3BoxThrower, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboardLoop_F", "onYacht_overboardLoop_thugA", TRUE)			
							ELSE
								sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboardLoop_F", "onYacht_overboardLoop_thugA", TRUE)			
							ENDIF
//							sceneIdSon = SETUP_SYNCED_SCENE(pedJimmy, NULL, truckYachtPackerTrailer, "misc_e", TRUE, "MISSFAM1_YachtBattleonYacht02_", "onBoom_hangLoop_J", "", TRUE)	
//							SET_SYNCHRONIZED_SCENE_ORIGIN(sceneIdSon, <<0.0, 0.0, 0.07>>, <<0.0, 0.0, 0.0>>)				

							//Yacht
							ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-3.6975, -1.8150, 3.8292>>)
							POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-2.4490, -4.2695, 2.6389>>)
							SET_CAM_FOV(cutsceneCamera, 37.4547)
							
							bForceActionCameraAsCinematicCamera = FALSE
							bForceCutsceneCameraAsCinematicCamera = TRUE
							
							fMaxPlaybackSpeed = 0.8
							//ALLOW_BOAT_BOOM_TO_ANIMATE(truckYachtPackerTrailer, FALSE)
							
							SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_NOTHING)
							
							blipMichaelSon = CREATE_BLIP_FOR_PED(pedJimmy, FALSE)
							PRINT("YB_OBJ3", DEFAULT_GOD_TEXT_TIME, 1)
							PRINT_HELP("YB_HELPJ", 15000)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM1_QUICK_CATCH)
							iControlBoxes = 0
							prevSyncPhase = 0.0
							iControlBoxGuyDeath = 0
							
							SETTIMERA(0)
							bJimmyIsHitByBoom = FALSE
							eChaseStage = CHASE_FRANKLIN_NOTIONS_TO_SAVE_JIMMY //CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3
						ENDIF
					ENDIF
				ELSE
					bJimmyIsHitByBoom = FALSE
					eChaseStage = CHASE_FRANKLIN_NOTIONS_TO_SAVE_JIMMY //CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedJimmy)
					SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedJimmy, PRF_BlockWeaponReactionsUnlessDead, TRUE)
				ENDIF
								
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					//The point where he throw's Franklin over the edge (now we can shoot him)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) < 0.659
						IF NOT IS_ENTITY_DEAD(pedBadGuy3BoxThrower)
							SET_ENTITY_INVINCIBLE(pedBadGuy3BoxThrower, TRUE)
							SET_PED_RESET_FLAG_AND_REQUEST_RAGDOLL_BOUNDS_UPDATE(pedBadGuy3BoxThrower, PRF_BlockWeaponReactionsUnlessDead, TRUE)
						ENDIF
					ELSE
						SET_ENTITY_INVINCIBLE(pedBadGuy3BoxThrower, FALSE)
					ENDIF
				ELSE
					SET_ENTITY_INVINCIBLE(pedBadGuy3BoxThrower, FALSE)
				ENDIF
								
			BREAK
			
			CASE CHASE_FRANKLIN_NOTIONS_TO_SAVE_JIMMY
			
				UPDATE_JIMMYS_LEGS()
				
				updateBoomBaddie()
				
				CONTROL_BOX_THROWING_LOOP()
			
				SWITCH iCatchJimmySubstages
			
					CASE 0
					
						IF NOT IS_PED_INJURED(pedFranklin())
						AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
						AND NOT IS_ENTITY_DEAD(truckYachtPacker)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 1.0				
								
									//sceneIdA = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_waitForJimmySafeLoop_F", "", FALSE)			
								
//									IF NOT IS_PED_INJURED(pedBadGuy3BoxThrower)
//										SET_ENTITY_HEALTH(pedBadGuy3BoxThrower, 1000)
//									ENDIF
									
									fMaxPlaybackSpeed = 0.7
									
									iCatchJimmySubstages = 1
									
								ENDIF
							ELSE
								iCatchJimmySubstages= 1
							ENDIF
						
						ENDIF
					
						IF TIMERB() > 4500
						AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) < 70.0
							//SCRIPT_ASSERT("fuck sticks")
							
							IF GET_RANDOM_BOOL()
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "FAM1_AYAA", "JIMMY", SPEECH_PARAMS_FORCE_FRONTEND)
							ELSE
								IF NOT bFranklinTellsToGetEnderJImmyOnce
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "FAM1_CBAA", "FRANKLIN", SPEECH_PARAMS_FORCE_FRONTEND)
									bFranklinTellsToGetEnderJImmyOnce = TRUE
								ELSE
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "FAM1_DMAA", "FRANKLIN", SPEECH_PARAMS_FORCE_FRONTEND)
								ENDIF
							ENDIF
							
							SETTIMERB(0)
						ENDIF
					
					BREAK
									
					CASE 1

						IF TIMERB() > 5000
						AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) < 70.0
							//Catch Jimmy
							IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
								IF bAlternateLines
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "FAM1_GCAA", "JIMMY", SPEECH_PARAMS_FORCE_FRONTEND)
									bAlternateLines = FALSE
								ELSE
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "FAM1_AYAA", "JIMMY", SPEECH_PARAMS_FORCE_FRONTEND)
									bAlternateLines = TRUE
								ENDIF
							ELSE
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "FAM1_DMAA", "FRANKLIN", SPEECH_PARAMS_FORCE_FRONTEND)
							ENDIF
							SETTIMERB(0)
						ENDIF
						
						IF (GET_GAME_TIMER() - iTimeOfBoxThrowerShot) > 30000
							iCatchJimmySubstages= 2
						ENDIF
						
					BREAK
					
					CASE 2
						
						IF TIMERB() > 4000
						AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) < 70.0
							IF GET_RANDOM_BOOL()
								//I'm slipping I'm about to fall!
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "FAM1_GDAA", "JIMMY", SPEECH_PARAMS_FORCE_FRONTEND)
								SETTIMERB(0)
							ENDIF
						ENDIF
					BREAK
				
				ENDSWITCH
																
				REQUEST_ANIM_DICT("MISSFAM1_YachtBattleinCar02_")
				//REQUEST_ANIM_DICT("veh@low@front_ps@jimmy_sit")
				
				REQUEST_CLIP_SET("clipset@missfam1_jimmy_sit") 
				
				IF NOT IS_ENTITY_DEAD(pedJimmy)
				AND NOT IS_ENTITY_DEAD(carPlayersJeep)
				AND HAS_ANIM_DICT_LOADED("MISSFAM1_YachtBattleinCar02_")
				
					IF CAN_JIMMY_DROP_INTO_CAR() 
						IF IS_BMW_STEADY_FOR_JUMP()
															
							CLEAR_ALL_FLOATING_HELP()
							DETACH_ENTITY(pedJimmy)
							
							IF eBoomBaddie < BOOMBADDIE_JIMMY_GOES_TO_ONE_HANDED_HANG
								sceneIdSon = SETUP_SYNCED_SCENE(pedJimmy, NULL, truckYachtPackerTrailer, "misc_e", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onBoom_dropToCar_J", "", FALSE)			
							ELSE
								sceneIdSon = SETUP_SYNCED_SCENE(pedJimmy, NULL, truckYachtPackerTrailer, "misc_e", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onBoom_twohand_drop", "", FALSE)			
							ENDIF
							
							REMOVE_BLIP(blipMichaelSon)
							
							REPLAY_RECORD_BACK_FOR_TIME(2.5, 4.0, REPLAY_IMPORTANCE_HIGHEST)
							
							CLEAR_HELP()
							
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedJImmy)
							
							//STOP_PED_SPEAKING(pedJimmy, TRUE)							
							//PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_DROP_IN_CAR_YELL", pedJimmy)								
							//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "FAM1_HCAA", "JIMMY", SPEECH_PARAMS_FORCE_FRONTEND)
							
							//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "SCREAM_TERROR", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE_FRONTEND)
							
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "MAST_FALL", "JIMMY_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)
														
							//PLAY_PAIN(pedJimmy, AUD_DAMAGE_REASON_FALLING)
							
							START_AUDIO_SCENE("FAM1_DROP_IN_CAR")
						
							seatHelperObject = CREATE_OBJECT(PROP_LD_TEST_01, GET_ENTITY_COORDS(carPlayersJeep))
							SET_ENTITY_COLLISION(seatHelperObject, TRUE)
							SET_ENTITY_VISIBLE(seatHelperObject, FALSE)
							ATTACH_ENTITY_TO_ENTITY(seatHelperObject, carPlayersJeep, GET_ENTITY_BONE_INDEX_BY_NAME(carPlayersJeep, "seat_pside_r"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
							mastHelperObject = CREATE_OBJECT(PROP_LD_TEST_01, GET_ENTITY_COORDS(truckYachtPackerTrailer))
							ATTACH_ENTITY_TO_ENTITY(mastHelperObject, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "misc_e"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							SET_ENTITY_VISIBLE(mastHelperObject, FALSE)
							SET_ENTITY_COLLISION(mastHelperObject, TRUE)
							
							bForceCutsceneCameraAsCinematicCamera = FALSE
							bJimmyCaughtDialoguePlayed = FALSE
							eChaseStage = CHASE_CATCH_JIMMY_DETACH
							
							//PRINT_NOW("YB_OBJ2", DEFAULT_GOD_TEXT_TIME - 1000, 1)
							//blippedBadGuy3BoxThrower = CREATE_BLIP_FOR_PED(pedBadGuy3BoxThrower)
							//SET_BLIP_AS_FRIENDLY(blippedBadGuy3BoxThrower, FALSE)
							//SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_NOTHING)
							
							SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_BOTH)
							
							
							ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "FAM1AUD", "FAM1_CATCH", CONV_PRIORITY_VERY_HIGH)
							
						ELSE
							PRINTSTRING("Going much faster than the yacht!")
							//Add some dialogue for Jimmy saying to put the roof down!
							IF bJimmyCaughtDialoguePlayed = FALSE
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_STEADY", CONV_PRIORITY_VERY_HIGH)
								bJimmyCaughtDialoguePlayed = TRUE	
							ENDIF
						ENDIF
					ELSE
						bJimmyCaughtDialoguePlayed = FALSE
					ENDIF
				ENDIF
			
			BREAK
						
			CASE CHASE_CATCH_JIMMY_DETACH
			
				updateBoomBaddie()
				
				IF bJimmyCaughtDialoguePlayed = FALSE
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CATCH2", CONV_PRIORITY_VERY_HIGH)
						bJimmyCaughtDialoguePlayed = TRUE	
					ENDIF				
				ENDIF
			
				CONTROL_BOX_THROWING_LOOP()
							
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdSon)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) >= 1.0
					AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
					AND NOT IS_PED_INJURED(pedFranklin())

						STOP_PED_SPEAKING(pedJimmy, FALSE)
					
						//Calculate the initial position for Jimmy based on his last pose of the drop anim
						//Then attach this as offset given world coords of the players car.
						//We then interp to the 0,0,0 attachement offset to the car seat node.
						vectorD = <<0.0, 0.0, 0.0>> //GET_ENTITY_COORDS(seatHelperObject)
						vectorX = GET_ANIM_INITIAL_OFFSET_POSITION("MISSFAM1_YACHTBATTLEinCar02_", "JimmyInCar_intro", vectorD, rotationD, 0.0)
												
						vectorXHelperObject = CREATE_OBJECT(PROP_LD_TEST_01, GET_ENTITY_COORDS(carPlayersJeep))
						SET_ENTITY_COLLISION(vectorXHelperObject, FALSE)
						SET_ENTITY_VISIBLE(vectorXHelperObject, FALSE)
						ATTACH_ENTITY_TO_ENTITY(vectorXHelperObject, carPlayersJeep, GET_ENTITY_BONE_INDEX_BY_NAME(carPlayersJeep, "seat_pside_r"), vectorX, <<0.0, 0.0, 0.0>>)
						vectorJ = GET_ANIM_INITIAL_OFFSET_POSITION("MISSFAM1_YACHTBATTLEonYacht02_", "onBoom_dropToCar_J", GET_ENTITY_COORDS(mastHelperObject), GET_ENTITY_ROTATION(mastHelperObject) , 1.0)						
						vectorJ = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(seatHelperObject, vectorJ)
						vectorS = VectorJ - VectorX
						
						PRINTLN("\nvectorD: ", vectorD, "\nvectorX: ",vectorX, "\nvectorJ :", vectorJ, "\nvectorS: ", vectorS)	
						
						sceneIdSon = SETUP_SYNCED_SCENE_WITH_OFFSET(vectorS, rotationS, pedJimmy, NULL, carPlayersJeep, "seat_pside_r", FALSE, "MISSFAM1_YachtBattleincar02_", "JimmyInCar_intro", "", TRUE)			
													
						//KILL_ANY_CONVERSATION()
						
						SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)					
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED() 
						bJimmyLanded = FALSE
						//Dialogue for Jimmy being caught...
						bMusicEvent3 = FALSE
						bJimmyLandedNoise = FALSE
						iNumberOfBoxesThrown = 0
						iTimeOfJimmyDropping = GET_GAME_TIMER()
						eChaseStage = CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3 //CHASE_FRANKLIN_RECOVERS
					ENDIF
				ENDIF
				
			BREAK
			
			CASE CHASE_WAIT_FOR_PLAYER_TO_SHOOT_BADGUY3
				
				//UPDATE_JIMMYS_LEGS()
				
				INTERP_MAST_DETACH_OFFSET()
			
				CONTROL_BOX_THROWING_LOOP()
				
				
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdSon)
					AND IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDPulledOverboard)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIDPulledOverboard) > 0.99)	//Either the animation of Franklin pulling the guy off has completed
				OR (GET_ENTITY_HEALTH(pedBadGuy3BoxThrower) < 200				//Or the player has shot the bad guy.
					AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDPulledOverboard))
					CLEAR_PRINTS()
					REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 4)	
					
					IF DOES_ENTITY_EXIST(oiChampagneBox)
						DETACH_ENTITY(oiChampagneBox, TRUE, FALSE)					
						SET_ENTITY_VELOCITY(oiChampagneBox, GET_ENTITY_VELOCITY(truckYachtPackerTrailer))
					ENDIF
					
					REMOVE_BLIP(blippedBadGuy3BoxThrower)
					IF NOT IS_ENTITY_DEAD(pedBadGuy3BoxThrower)
						//CLEAR_PED_TASKS(pedBadGuy3BoxThrower)
						PROCESS_ENTITY_ATTACHMENTS(pedBadGuy3BoxThrower)
						SET_PED_TO_RAGDOLL(pedBadGuy3BoxThrower, 0, 10000, TASK_RELAX)
					ENDIF
											
					REMOVE_BLIP(blippedBadGuy3BoxThrower)
					SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_BOTH)
					
					sceneId = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_overboard_success_F", "", FALSE)			
					
					SET_SYNCHRONIZED_SCENE_RATE(sceneId, 1.25)
					
					IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
						//INFORM_STAT_FAMILY_ONE_SPECIAL_ABILITY_KILL_EXECUTED()
					ENDIF
					
					SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_BOTH)
					 
					INFORM_MISSION_STATS_OF_INCREMENT(FAM1_KILLS) 											
										
					SETTIMERB(0)
					//Yacht
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-2.7513, 0.6709, 4.0208>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-1.8015, -2.0438, 3.1672>>)
					SET_CAM_FOV(cutsceneCamera, 32.6628)
					//SET_TIME_SCALE(0.6)
					
					iTimeOfBoxThrowerShot = GET_GAME_TIMER()
					
					eChaseStage = CHASE_FRANKLIN_RECOVERS
				ENDIF
								
			BREAK				
			
			//Now catch Franklin!
			CASE CHASE_FRANKLIN_RECOVERS
			
				IF (GET_GAME_TIMER() - iTimeOfBoxThrowerShot) > 1100
				AND bCutOfBoxThrowerOver = FALSE
					//bForceCinematicCamera = FALSE
					//bForceCutsceneCameraAsCinematicCamera = FALSE
					
					SET_OBJECT_AS_NO_LONGER_NEEDED(oiChampagneBox)
					SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Champ_Box_01)
					
					//oiBlipObjectCatchJimmy = CREATE_OBJECT(Prop_LD_Test_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedJimmy, <<0.0, 0.0, -1.75>>))		
					//SET_ENTITY_COLLISION(oiBlipObjectCatchJimmy, FALSE)
					//HELP_AT_ENTITY("YB_OBJ3", oiBlipObjectCatchJimmy, HELP_TEXT_NORTH, -1, FLOATING_HELP_MISSION_SLOT, HUD_COLOUR_BLACK, HELP_MESSAGE_STYLE_TAGGABLE, 1.0)
									
					IF NOT IS_ENTITY_DEAD(pedBadGuy3BoxThrower)
						SET_ENTITY_HEALTH(pedBadGuy3BoxThrower, 0)
					ENDIF
					
					//Yacht
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-7.4246, 4.2393, 2.5574>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-5.7172, 1.7728, 2.5182>>)
					SET_CAM_FOV(cutsceneCamera, 45.0000)
					bForceCutsceneCameraAsCinematicCamera = TRUE
					SET_TIME_SCALE(1.0)
					bFranklinTellsToGetEnderJImmyOnce = FALSE
					bCutOfBoxThrowerOver = TRUE
				ENDIF
			
				//CONTROL_BOX_THROWING_LOOP()
			
				INTERP_MAST_DETACH_OFFSET()
				
				updateBoomBaddie()
							
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdSon)
														
					//If Michael vastly changes position during the drop
//					IF NOT CAN_JIMMY_DROP_INTO_CAR(2.35)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) < 0.024
//						//SCRIPT_ASSERT("script is killing Jimmy")
//						SET_PED_TO_RAGDOLL(pedJimmy, 0, 60000, TASK_NM_BALANCE) 
//						MISSION_FLOW_SET_FAIL_REASON("YB_FAIL1")
//						IF NOT IS_PED_INJURED(pedJimmy)
//							APPLY_DAMAGE_TO_PED(pedJimmy, 1000, TRUE)
//						ENDIF
//						TRIGGER_MUSIC_EVENT("FAM1_FAIL")
//						Mission_Failed()
//					ENDIF
					
					
				
					
										
//					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) >= 1.0
//						IF NOT IS_ENTITY_DEAD(pedJimmy)
//						AND NOT IS_ENTITY_DEAD(carPlayersJeep)
//							IF NOT IS_PED_IN_VEHICLE(pedJimmy, carPlayersJeep)
//								SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_BACK_LEFT)
//								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy, TRUE)
//							ENDIF
//						ENDIF
//					ENDIF
					
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				AND NOT IS_PED_INJURED(pedFranklin())
				AND bMusicEvent3 = TRUE
				AND bJimmyCaughtDialoguePlayed = TRUE	
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) >= 1.0	//Franklins climbed back up and into position
										
					STOP_AUDIO_SCENE("FAMILY_1_SAVE_JIMMY")
				
					sceneIdA = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_jumpToCarIntro_F", "", TRUE)			
						
					ATTACH_CAM_TO_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<-0.0528, -4.5224, 2.7052>>)
					POINT_CAM_AT_ENTITY(cutsceneCamera, truckYachtPackerTrailer, <<0.1428, -7.4610, 2.1340>>)
					SET_CAM_FOV(cutsceneCamera, 27.3552)
					
					KILL_ANY_CONVERSATION()
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)					
					eChaseStage = CHASE_CREATE_SPEECH_FOR_FRANKLIN
				ENDIF
				
			BREAK
			
			CASE CHASE_CREATE_SPEECH_FOR_FRANKLIN
						
				updateBoomBaddie()
				//Set_Replay_Mid_Mission_Stage(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT)) 
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT")
				//IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_LEAP", CONV_PRIORITY_HIGH)
					eChaseStage = CHASE_FRANKLIN_RECOVERS_LOOP
				//ENDIF
			BREAK
			
			CASE CHASE_FRANKLIN_RECOVERS_LOOP
		
				//INTERP_MAST_DETACH_OFFSET()
			
				//For replay purposes...
				IF IS_SCREEN_FADED_OUT()
					CREATE_ALL_WAITING_UBER_CARS()
					SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 20.0)
					CLEAR_PRINTS()
					FADE_IN_IF_NEEDED()
					IF NOT IS_ENTITY_DEAD(carPlayersJeep)
						STOP_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep)
					ENDIF
				ENDIF
			
				IF NOT bFranklinNotSaidSpeechThatWontWork
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_LEAP", CONV_PRIORITY_HIGH)
						bFranklinNotSaidSpeechThatWontWork = TRUE
					ENDIF
				ENDIF
				
				updateBoomBaddie()
							
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) >= 1.0
					AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
					AND NOT IS_ENTITY_DEAD(pedFranklin())
						sceneIdA = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", TRUE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_jumpToCarLoop_F", "", TRUE)			
						SET_ENTITY_INVINCIBLE(pedFranklin(), FALSE)
						
						//PRINT_NOW("YB_OBJ4", DEFAULT_GOD_TEXT_TIME, 1) //Catch fRanklin!
						SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_NOTHING)
						blipFranklin = CREATE_BLIP_FOR_PED(pedFranklin())
						bFranklinShoutedForHelp = FALSE
						eChaseStage = CHASE_FRANKLIN_ANIM_ALLOW_ONE_FRAME
					ENDIF
				ENDIF
				
			BREAK
			
			//Stops animation updating twice in one frame if you are in position when the loop anim starts../
			//then jump anim immediately kicks in.
			CASE CHASE_FRANKLIN_ANIM_ALLOW_ONE_FRAME
				eChaseStage = CHASE_CATCH_FRANKLIN
			BREAK		
			
			CASE CHASE_CATCH_FRANKLIN
						
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					IF  bFranklinShoutedForHelp = FALSE
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						//HELP_AT_ENTITY("YB_OBJ4", oiBlipObject, HELP_TEXT_SOUTH, -1, FLOATING_HELP_MISSION_SLOT, HUD_COLOUR_BLACK, HELP_MESSAGE_STYLE_TAGGABLE, 3.0)
						PRINT_NOW("YB_OBJ4", DEFAULT_GOD_TEXT_TIME, 1) //Catch fRanklin!
						bFranklinShoutedForHelp = TRUE
					ENDIF
				ENDIF
				
				updateBoomBaddie()
			
//				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//				AND bFranklinShoutedForHelp = TRUE
//					//CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HELP2", CONV_PRIORITY_HIGH)
//					CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_LEAP", CONV_PRIORITY_HIGH)
//					//eChaseStage = 18
//				ENDIF

				IF (TIMERB() > 2500)
				AND bFranklinShoutedForHelp = TRUE
				AND NOT IS_FRANKLIN_IN_JUMP_ZONE(1.0)
					IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "FAM1_AAAA", "FRANKLIN", SPEECH_PARAMS_FORCE_FRONTEND)
						SETTIMERB(0)
					ELSE
						IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CTCHF", CONV_PRIORITY_HIGH)
							SETTIMERB(0)
						ENDIF
					ENDIF
				ENDIF

			
				IF  NOT IS_PED_INJURED(pedFranklin())
				AND NOT IS_ENTITY_DEAD(carPlayersJeep)
				AND NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				
					IF IS_FRANKLIN_IN_JUMP_ZONE(1.0)
					AND IS_BMW_STEADY_FOR_JUMP()
					AND bCinematicCameraActive = FALSE
						KILL_ANY_CONVERSATION()			
						
						REPLAY_RECORD_BACK_FOR_TIME(3.5, 9.0, REPLAY_IMPORTANCE_HIGHEST)				
						
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedFranklin())
						
						START_AUDIO_SCENE("FAMILY_1_CATCH_FRANKLIN")
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedFranklin())	
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "GENERIC_FRIGHTENED_HIGH", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_FRONTEND)	
											
						CLEAR_ALL_FLOATING_HELP()
						
						sceneIdA = SETUP_SYNCED_SCENE(pedFranklin(), NULL, truckYachtPackerTrailer, "chassis", FALSE, "MISSFAM1_YachtBattleonYacht02_", "onYacht_jumpToCar_F", "", TRUE)			
						
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						
						IF NOT DOES_CAM_EXIST(initialCam)		
							initialCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						ENDIF
						IF NOT DOES_CAM_EXIST(destinationCam)		
							destinationCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						ENDIF
						
						ATTACH_CAM_TO_ENTITY(initialCam, truckYachtPackerTrailer, <<-1.1738, -6.6237, 3.0274>>)
						POINT_CAM_AT_ENTITY(initialCam, truckYachtPackerTrailer, <<0.9368, -7.5638, 1.1138>>)
						SET_CAM_FOV(initialCam, 29.9168)
						
						SET_CAM_ACTIVE(initialCam, TRUE)
						
						SHAKE_CAM(initialCam, "SKY_DIVING_SHAKE", 1.0)
//												
						ATTACH_CAM_TO_ENTITY(destinationCam, truckYachtPackerTrailer, <<-0.7922, -6.4277, 3.3964>>)
						POINT_CAM_AT_ENTITY(destinationCam, truckYachtPackerTrailer, <<0.1019, -8.8480, 1.8657>>)
						SET_CAM_FOV(destinationCam, 30.6854)
						
						
						ATTACH_CAM_TO_ENTITY(destinationCam, truckYachtPackerTrailer, <<-0.7425, -6.6230, 3.0168>>)
						POINT_CAM_AT_ENTITY(destinationCam, truckYachtPackerTrailer, <<0.1238, -9.3185, 2.0247>>)
						SET_CAM_FOV(destinationCam, 30.6854)
						//SHAKE_CAM(destinationCam, "SKY_DIVING_SHAKE", 1.0)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 1500, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
				
						SHAKE_CAM(destinationCam, "SKY_DIVING_SHAKE", 1.0)
				
						SET_CAM_MOTION_BLUR_STRENGTH(destinationCam, 0.1)
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						SET_WIDESCREEN_BORDERS(TRUE,0)
						
						bProcessCinematicCamera = FALSE
						//shake / motion blur

						//roation linear
				
						//translation ease in ease out.
						
						CLEAR_PRINTS()
						REMOVE_BLIP(blipFranklin)
						
						START_AUDIO_SCENE("FAM1_JUMP_TO_YACHT")
						
						REQUEST_PTFX_ASSET()
											
						eChaseStage = CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
					ENDIF
				ENDIF
						
			BREAK
			
			CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
				
				updateBoomBaddie()

				IF NOT bMusicEvent4
					IF PREPARE_MUSIC_EVENT("FAM1_FRANK_LEAPS")
						TRIGGER_MUSIC_EVENT("FAM1_FRANK_LEAPS")
						bMusicEvent4 = TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
					DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
				
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) > 0.1
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) < 0.9
						//RENDER_SCRIPT_CAMS(TRUE, FALSE)
						//bForceCinematicCamera = TRUE
					ENDIF
				
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) >= 1.0
					AND NOT IS_ENTITY_DEAD(carPlayersJeep)
					AND NOT IS_ENTITY_DEAD(pedFranklin())
					
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						bProcessCinematicCamera = TRUE
						sceneIdA = SETUP_SYNCED_SCENE(pedFranklin(), NULL, carPlayersJeep, "seat_pside_f", FALSE, "MISSFAM1_YachtBattleinCar02_", "FranklinOnBonnet_landOnCar", "", TRUE)			
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIdA, TRUE)
						START_AUDIO_SCENE("FAM1_LAND_ON_BONNET")
						
						bFranklinLandAudioPlayed = FALSE
						bForceCinematicCamera = FALSE		
						
						//SET_UBER_PLAYBACK_TO_TIME_NOW(truckYachtPacker, 16000) //11500 + (5500.0 - TIMERA() ) )
						
						SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 2.2)
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, TRUE)
						
						REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht02_")
							
						eChaseStage = CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_UPDATE
					ENDIF
				ENDIF
				
			BREAK		
			
			CASE CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_UPDATE
			
				updateBoomBaddie()
			
				IF NOT IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
					DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
				ENDIF
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) >= 0.022
					AND bFranklinLandAudioPlayed = FALSE
						PLAY_SOUND_FROM_ENTITY(-1, "FAMILY1_LAND_ON_BONNET", carPlayersJeep)
						//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "FAM1AUD", "FAM1_LEAP2", CONV_PRIORITY_VERY_HIGH)
						CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_LEAP2", CONV_PRIORITY_VERY_HIGH)
						SHAKE_GAMEPLAY_CAM("JOLT_SHAKE", 0.7)
						
						IF NOT IS_ENTITY_DEAD(carPlayersJeep)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(carPlayersJeep, SC_DOOR_BONNET, TRUE)
						ENDIF
						
						SET_BLIP_DISPLAY(blipTruckYachtPackerTrailer, DISPLAY_BOTH)
						
						SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 3.5)
						
						SET_VEHICLE_DAMAGE(carPlayersJeep, <<0.0, -0.9, 0.0>>, 0.0, 150.0, TRUE)
						APPLY_FORCE_TO_ENTITY(carPlayersJeep, APPLY_TYPE_IMPULSE, <<0.0, 0.0, -1.0>>, <<0.0, 1.0, 0.0>>, 0, TRUE, TRUE, TRUE)
						SET_VEHICLE_AUDIO_ENGINE_DAMAGE_FACTOR(carPlayersJeep, 0.7)
						
						DELETE_VEHICLE(cameraCar)
						DELETE_VEHICLE(cameraCarCamera)
						
						bForceCinematicCamera = FALSE		//Fail safe
						bFranklinLandAudioPlayed = TRUE
					ENDIF
				ENDIF
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) >= 0.325
						
						IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_SAFE", CONV_PRIORITY_VERY_HIGH)
						
							STOP_AUDIO_SCENE("FAMILY_1_CATCH_FRANKLIN")
						
							START_AUDIO_SCENE("FAMILY_1_GET_THE_YACHT")
						
							//Franklin is no longer on bonnet and has climbed into car.
							IF NOT IS_ENTITY_DEAD(carPlayersJeep)
								SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(carPlayersJeep, SC_DOOR_BONNET, TRUE)
							ENDIF
				
							//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "FAM1AUD", "FAM1_LEAP2", CONV_PRIORITY_HIGH)
							SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 7.5)
							
							STOP_AUDIO_SCENE("FAMILY_1_PROTECT_FRANKLIN")
							
							eChaseStage = CHASE_KEEP_CHASING
						ENDIF
					ENDIF
				ELSE
					PRINTSTRING("Warning: Franklin synced scene invalid. Mission SHOULD fail.")
					
				ENDIF
				
				SET_PED_RESET_FLAG(pedFranklin(), PRF_BlockWeaponReactionsUnlessDead, TRUE)
				
				IF IS_PED_INJURED(pedFranklin())
					MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
					Mission_Failed()		
				ELSE
					IF IS_ENTITY_ON_FIRE(pedFranklin())
						SET_ENTITY_HEALTH(pedFranklin(), 0)
					ENDIF
				ENDIF
			
			BREAK
			
			CASE CHASE_KEEP_CHASING
				
				updateBoomBaddie()
				
				//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
							
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) >= 0.55
						
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_01")
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_02")
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_BOAT_STRAIN")
						
						STOP_AUDIO_SCENE("FAMILY_1_GET_THE_YACHT")
					
						START_AUDIO_SCENE("FAMILY_1_CAR_BREAKS_DOWN")
					
						IF NOT IS_ENTITY_DEAD(carPlayersJeep)
							SET_VEHICLE_ENGINE_HEALTH(carPlayersJeep, 70.0)
							SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.9)
						ENDIF
						
						IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCarSmoke)
							#IF IS_NEXTGEN_BUILD
								//overheat
								ptfxCarSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_fam1_veh_smoke", carPlayersJeep, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, GET_ENTITY_BONE_INDEX_BY_NAME(carPlayersJeep, "overheat"))
							#ENDIF
							
							#IF NOT IS_NEXTGEN_BUILD
								ptfxCarSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam1_veh_smoke", carPlayersJeep, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							#ENDIF
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(3.5, 5.0, REPLAY_IMPORTANCE_HIGH)	
						
						SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 1.0)
											
						SET_VEHICLE_AUDIO_ENGINE_DAMAGE_FACTOR(carPlayersJeep, 0.75)
						ENABLE_VEHICLE_FANBELT_DAMAGE(carPlayersJeep, TRUE)
						iDialogueTimeOut = GET_GAME_TIMER()
						eChaseStage = CHASE_PUT_FRANKLIN_BACK_IN_CAR_PROPERLY
					ENDIF
				ENDIF
				
			BREAK
			
			
			CASE CHASE_PUT_FRANKLIN_BACK_IN_CAR_PROPERLY //CHASE_KEEP_CHASING
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) >= 1.0
						IF NOT IS_ENTITY_DEAD(pedFranklin())
						AND NOT IS_ENTITY_DEAD(carPlayersJeep)
							IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)	
								SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
							ENDIF
						ENDIF
					ENDIF					
				ENDIF
				
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND REQUEST_AMBIENT_AUDIO_BANK("FAMILY_1_CAR_BREAKDOWN")
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CARFUCK", CONV_PRIORITY_VERY_HIGH)
					OR (GET_GAME_TIMER() - iDialogueTimeOut) >  15000
						TRIGGER_MUSIC_EVENT("FAM1_BROKE_CAR")
						
						IF DOES_BLIP_EXIST(blipTruckYachtPackerTrailer)
							REMOVE_BLIP(blipTruckYachtPackerTrailer)
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1, "FAMILY_1_CAR_BREAKDOWN", "FAMILY1_BOAT")
						PLAY_SOUND_FRONTEND(-1, "FAMILY_1_CAR_BREAKDOWN_ADDITIONAL", "FAMILY1_BOAT")
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_CAR_BREAKDOWN")
						
						mission_stage = STAGE_TAKE_CAR_TO_CHOP_SHOP
					ENDIF
				ENDIF
			BREAK
			
		
			//Special cases			
			CASE CHASE_FAILURE
			
				REMOVE_BLIPS_ON_FAIL()
			
				//Allow failure animations to finish before cleaning up
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)
				
					PRINTLN("GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure):", GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure))
				
//					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure) >= 0.88
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure) >= 0.7
					
						//SCRIPT_ASSERT("bang!")
						PROCESS_ENTITY_ATTACHMENTS(pedFranklin())
						SET_PED_TO_RAGDOLL(pedFranklin(), 0, 60000, TASK_NM_SCRIPT)
						
						//SET_ENTITY_HEALTH(pedFranklin(), 0)
						
						sceneIdFailure = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							
						//Depending how we've have the appropriate guy play his animation
						IF DOES_ENTITY_EXIST(pedBadguy2)
							IF NOT IS_PED_INJURED(pedBadguy2)
								TASK_SYNCHRONIZED_SCENE (pedBadguy2, sceneIdFailure, "MISSFAM1_YACHTBATTLEonYacht01_", "onYacht_failIdle_thug", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							ENDIF
						ENDIF
						
						SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdFailure, FALSE)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY( sceneIdFailure, truckYachtPackerTrailer, GET_ENTITY_BONE_INDEX_BY_NAME(truckYachtPackerTrailer, "chassis")  )
										
						CLEAR_HELP()
						PRINTLN("FRANKLIN IS DEAD!")
						
						MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
						TRIGGER_MUSIC_EVENT("FAM1_FAIL")			
					ENDIF
				ENDIF
			
				IF NOT IS_PED_INJURED(pedFranklin())
					IF NOT IS_ENTITY_IN_AIR(pedFranklin())
						IF IS_PED_RAGDOLL(pedFranklin())
							
							//WAIT(1500)
							IF NOT IS_PED_INJURED(pedFranklin())
								SET_ENTITY_HEALTH(pedFranklin(), 0)
							ENDIF
							CLEAR_HELP()
							//PRINT_NOW("CMN_FDIED", DEFAULT_GOD_TEXT_TIME, 1)
							MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
							PRINTLN("FRANKLIN IS TOTALLY DEAD!")
							REMOVE_BLIPS_ON_FAIL()
							TRIGGER_MUSIC_EVENT("FAM1_FAIL")
							Mission_Failed()	
						ENDIF
					ENDIF
				ELSE
					SCRIPT_ASSERT("Franklin died during Fail.")
				ENDIF
				
			BREAK
			
			CASE CHASE_FAILURE_LOST_YACHT
			
				REMOVE_BLIPS_ON_FAIL()				
				CLEAR_HELP()
				IF eChaseStage < CHASE_CATCH_JIMMY_DETACH
				
					WHILE CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_F1", CONV_PRIORITY_VERY_HIGH)
						WAIT(0)
					ENDWHILE
				ELIF eChaseStage < CHASE_FRANKLIN_JUMPS_ON_YACHT
					WHILE CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_F2", CONV_PRIORITY_VERY_HIGH)
						WAIT(0)
					ENDWHILE
					WHILE CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_F3", CONV_PRIORITY_VERY_HIGH)
						WAIT(0)
					ENDWHILE
				ENDIF
				
				MISSION_FLOW_SET_FAIL_REASON("YB_FAIL3")
				TRIGGER_MUSIC_EVENT("FAM1_FAIL")
				SETTIMERA(0)
				
//				WAIT(2000)
//				
//				DO_SCREEN_FADE_OUT(500)
//				WHILE IS_SCREEN_FADING_OUT()
//					WAIT(0)
//				ENDWHILE
				//DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				Mission_Failed()					
							
			BREAK
			
			CASE CHASE_FAILURE_DESTROYED_CAR
			CASE CHASE_FAILURE_STUCK_CAR
				
				REMOVE_BLIPS_ON_FAIL()
				
				IF eChaseStage = CHASE_FAILURE_STUCK_CAR
					MISSION_FLOW_SET_FAIL_REASON("CMN_GENSTCK")
				ELSE
					MISSION_FLOW_SET_FAIL_REASON("CMN_GENDEST")
				ENDIF
								
				TRIGGER_MUSIC_EVENT("FAM1_FAIL")				
				WHILE TIMERA() < 200
					IF eChaseStage < CHASE_FRANKLIN_JUMPS_ON_YACHT
						MANAGE_FRANKLIN_ON_BONNET()
					ENDIF
					WAIT(0)
				ENDWHILE
				
				Mission_Failed()		
			BREAK
			
			CASE CHASE_FAILURE_BLEW_COVER
				
				REMOVE_BLIPS_ON_FAIL()
				
				MISSION_FLOW_SET_FAIL_REASON("YB_BLEWCOV")
				TRIGGER_MUSIC_EVENT("FAM1_FAIL")				
//				WHILE TIMERA() < 2000
//					WAIT(0)
//				ENDWHILE
//				
//				DO_SCREEN_FADE_OUT(500)
//				WHILE IS_SCREEN_FADING_OUT()
//					WAIT(0)
//				ENDWHILE
				Mission_Failed()		
				
			BREAK
			
		ENDSWITCH
	//ENDIF
		
	
	
	//If the player has done everything trigger the cutscene early
//	IF eChaseStage >= CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
//		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
//			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) >= 0.99
//				bSafeToEndMission = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF fRecordingProgress > 191000.000
	AND eChaseStage < CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
		SWITCH iDealWithFailDialogue
			CASE 0
				KILL_FACE_TO_FACE_CONVERSATION()
				iDealWithFailDialogue++
			BREAK
			
			CASE 1
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_GNAFAIL", CONV_PRIORITY_VERY_HIGH)
						iDealWithFailDialogue = 1
					ENDIF			
				ENDIF			
			BREAK
		
		ENDSWITCH
		
	ENDIF
	
	IF fRecordingProgress > 195053.000
	AND bBlipsFlashing = FALSE
		IF DOES_BLIP_EXIST(blipTruckYachtPackerTrailer)
			SET_BLIP_FLASHES(blipTruckYachtPackerTrailer,TRUE)		
		ENDIF
		IF DOES_BLIP_EXIST(blipFranklin)
			SET_BLIP_FLASHES(blipFranklin,TRUE)		
		ENDIF
		IF DOES_BLIP_EXIST(blipMichaelSon)
			SET_BLIP_FLASHES(blipMichaelSon,TRUE)		
		ENDIF
		
		//Start requesting fail cutscene assets
		REQUEST_VEHICLE_RECORDING(2, "Fam1New")
		REQUEST_VEHICLE_RECORDING(1, "Fam1New")
		REQUEST_VEHICLE_RECORDING(1, "Fam1End")
		bBlipsFlashing = TRUE
	ENDIF
	
	IF fRecordingProgress > 197000.000
	AND bCarBrokeDownForFailCut = FALSE
		IF REQUEST_AMBIENT_AUDIO_BANK("FAMILY_1_CAR_BREAKDOWN")
			PLAY_SOUND_FRONTEND(-1, "FAMILY_1_CAR_BREAKDOWN", "FAMILY1_BOAT")
			PLAY_SOUND_FRONTEND(-1, "FAMILY_1_CAR_BREAKDOWN_ADDITIONAL", "FAMILY1_BOAT")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_CAR_BREAKDOWN")
			
			REPLAY_RECORD_BACK_FOR_TIME(3.5)
			
			bCarBrokeDownForFailCut = TRUE
		ENDIF
	ENDIF
	
	
	IF fRecordingProgress > 200053.000
	//AND IS_GAMEPLAY_CAM_RENDERING()
			
		KILL_FACE_TO_FACE_CONVERSATION()
						
//		IF (eChaseStage >= CHASE_KEEP_CHASING OR bSafeToEndMission)
//			mission_stage = STAGE_TAKE_CAR_TO_CHOP_SHOP	
//		ENDIF
		
		IF eChaseStage < CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR //CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR_UPDATE //CHASE_KEEP_CHASING
			MISSION_FLOW_SET_FAIL_REASON("YB_FAIL")
			mission_stage = STAGE_FAIL_CUTSCENE
		ENDIF			
	
	ENDIF

	
	
//	#IF IS_DEBUG_BUILD
//					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//				DRAW_DEBUG_SPHERE(vectorD, 0.1, 255, 0, 0, 100)
//				DRAW_DEBUG_SPHERE(vectorX, 0.1, 0,  255, 0, 100)
//				DRAW_DEBUG_SPHERE(VectorJ, 0.5, 0, 0, 255, 100)
//				DRAW_DEBUG_SPHERE(vectorS, 0.5, 128, 0, 128, 100)
//	#ENDIF
	
	
	IF bProcessCinematicCamera
		OVERRIDE_CINEMATIC_CAMERA()	
	ENDIF
	
	IF CAN_JIMMY_DROP_INTO_CAR(2.0) 
	AND (eChaseStage = CHASE_FRANKLIN_NOTIONS_TO_SAVE_JIMMY OR eChaseStage = CHASE_CATCH_JIMMY_DETACH)
		//If player is near to the jump zone match the speed of the yach to the players car
		MATCH_PLAYERS_CAR_SPEED(truckYachtPackerTrailer)
	ELSE
		//Control Rubber banding as normal. Except when Franklin is jumping o nyacht
		
		IF eChaseStage <> CHASE_FRANKLIN_BOARDS_INTRO	
			CONTROL_PLAYBACK_SPEED()
		ENDIF
	ENDIF
	
	//Put Jimmy back into the car after falling in.
	IF (eChaseStage > CHASE_CATCH_JIMMY_DETACH)
	AND (mission_stage <= STAGE_TAKE_CAR_TO_CHOP_SHOP)
		//REQUEST_ANIM_DICT("veh@low@front_ps@jimmy_sit")
		REQUEST_CLIP_SET("clipset@missfam1_jimmy_sit_rear")
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdSon)
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon) >= 0.80
				IF NOT IS_ENTITY_DEAD(pedJimmy)
				AND NOT IS_ENTITY_DEAD(carPlayersJeep)
					IF NOT IS_PED_IN_VEHICLE(pedJimmy, carPlayersJeep)
					AND HAS_CLIP_SET_LOADED("clipset@missfam1_jimmy_sit_rear")
						STOP_SYNCHRONIZED_ENTITY_ANIM(pedJimmy, REALLY_SLOW_BLEND_OUT, TRUE)
//						CLEAR_PED_TASKS_IMMEDIATELY(pedJimmy)
						SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_BACK_LEFT)
						SET_PED_IN_VEHICLE_CONTEXT(pedJimmy, GET_HASH_KEY("MISS_FAMILY1_JIMMY_SIT_REAR"))//MISS_FAMILY1_JIMMY_SIT_REAR"))
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy, TRUE)		
						PRINTLN("@@@@@@@@@@@ SET_PED_IN_VEHICLE 1 @@@@@@@@@@@@")
					ENDIF
				ENDIF
			ENDIF
		ELSE
//			IF NOT bJimmyPlayingCustomSeatAnim
//				IF NOT IS_ENTITY_DEAD(pedJimmy)
//				AND NOT IS_ENTITY_DEAD(carPlayersJeep)
//					IF HAS_CLIP_SET_LOADED("clipset@missfam1_jimmy_sit_rear")
//						CLEAR_PED_TASKS_IMMEDIATELY(pedJimmy)
//						SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_BACK_LEFT)
//						SET_PED_IN_VEHICLE_CONTEXT(pedJimmy, GET_HASH_KEY("MISS_FAMILY1_JIMMY_SIT_REAR"))//MISS_FAMILY1_JIMMY_SIT_REAR"))
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy, TRUE)		
//						PRINTLN("@@@@@@@@@@@ SET_PED_IN_VEHICLE 2 @@@@@@@@@@@@")
//						bJimmyPlayingCustomSeatAnim = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		
		TEXT_LABEL_63 debugName
		
		INT rounded = ROUND(mainPlaybackSpeed * 1000.0)
		
		debugName += rounded
		SET_VEHICLE_NAME_DEBUG(carPlayersJeep, debugName)					
	#ENDIF

ENDPROC

BOOL bRadioTurnedOff = TRUE

BOOL bToldToExitFreeway = FALSE
BOOL bTurnLeftHere1 = FALSE
BOOL bTurnLeftHere2 = FALSE
BOOL bGiveFranklinCash = FALSE
BOOL bArriveAtChopShopDialogue = FALSE
BOOL bArriveAtChopShopDialogue2 = FALSE

PROC stageTakeCarToChopShopEntrance()
	
	failConditionsPreChase()
	
	IF NOT IS_PED_INJURED(pedFranklin())
		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
			IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
				SET_PED_CAN_RAGDOLL(pedFranklin(), FALSE)
			ELSE
				SET_PED_CAN_RAGDOLL(pedFranklin(), TRUE)
			ENDIF
		ENDIF
	ELSE
		MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")					
		REMOVE_BLIPS_ON_FAIL()
		TRIGGER_MUSIC_EVENT("FAM1_FAIL")
		Mission_Failed()	
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCarSmoke)
		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
			IF IS_ENTITY_IN_WATER(carPlayersJeep)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 0.0)
			ELSE
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 1.0)
			ENDIF
		ELSE
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 0.0)
		ENDIF
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
	
	IF IS_PED_INJURED(pedJImmy)
		//SCRIPT_ASSERT("script is killing Jimmy")
		MISSION_FLOW_SET_FAIL_REASON("YB_FAIL1")
		Mission_Failed()
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPackerTrailer)
			CONTROL_PLAYBACK_SPEED()
		ENDIF
	ENDIF
		
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
		IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdA) >= 1.0
			IF NOT IS_ENTITY_DEAD(pedFranklin())
			AND NOT IS_ENTITY_DEAD(carPlayersJeep)
				IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)	
					SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF					
	ENDIF
	
	//Dont delte truck until far away.
	IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) > 500.0
		OR NOT IS_ENTITY_ON_SCREEN(truckYachtPackerTrailer)
			DELETE_VEHICLE(truckYachtPackerTrailer)
			DELETE_VEHICLE(truckYachtPacker)
			DELETE_PED(pedYachtPacker)
			DELETE_PED(pedBadguy2)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(truckYachtPacker)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPacker)
			DELETE_VEHICLE(truckYachtPacker)
			DELETE_PED(pedYachtPacker)
		ENDIF
	ENDIF
		
	SWITCH iTakeCarToChopShopStage 
		
		CASE 0
			RELEASE_CHASE_ASSETS()
			RELEASE_CHASE_INITIAL_ASSETS()	
			bArriveAtChopShopDialogue = FALSE
			bArriveAtChopShopDialogue2 = FALSE
			bGiveFranklinCash = FALSE
			iTakeCarToChopShopStage++
		BREAK
		CASE 1
		
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
		
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_COAX", CONV_PRIORITY_VERY_HIGH)
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 3.95)
					iTakeCarToChopShopStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
		
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_DIST", CONV_PRIORITY_VERY_HIGH)
					REPLAY_RECORD_BACK_FOR_TIME(3.5, 5.0, REPLAY_IMPORTANCE_HIGH)	
					iTakeCarToChopShopStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
					
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
			
			SET_RADAR_ZOOM(0)
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.9)
			iTakeCarToChopShopStage++
		BREAK
		
		CASE 4
		
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
		
			IF NOT IS_ENTITY_DEAD(truckYachtPacker)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPacker)
					UPDATE_UBER_PLAYBACK(truckYachtPacker, mainPlaybackSpeed)
				ENDIF
			ENDIF	
			
			IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPackerTrailer)
					SET_PLAYBACK_SPEED(truckYachtPackerTrailer,  mainPlaybackSpeed) 
				ENDIF
			ENDIF
		
			IF NOT DOES_ENTITY_EXIST(truckYachtPackerTrailer)
			OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), truckYachtPackerTrailer) > 250.0
			OR NOT IS_ENTITY_ON_SCREEN(truckYachtPackerTrailer)
				SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS2)
				SET_MODEL_AS_NO_LONGER_NEEDED(Prop_LD_Test_01)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_SALVAGOON_01)
				SET_PED_AS_NO_LONGER_NEEDED(pedBadguy1)
				SET_PED_AS_NO_LONGER_NEEDED(pedBadguy2)
				SET_PED_AS_NO_LONGER_NEEDED(pedBadGuy3BoxThrower)
				SET_PED_AS_NO_LONGER_NEEDED(pedBadguy4BoomGuy)
				SET_PED_AS_NO_LONGER_NEEDED(pedYachtPacker)
				
				REMOVE_PTFX_ASSET()
				REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEinCar01_")
				REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht01_")
				REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht02_")
				REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEinCar02_")
				REMOVE_ANIM_DICT("veh@drivebystd_ds")
				
//				DELETE_VEHICLE(truckYachtPackerTrailer)
//				DELETE_VEHICLE(truckYachtPacker)
//				DELETE_PED(pedYachtPacker)
//				
				IF NOT IS_PED_INJURED(pedFranklin())
					SET_PED_CAN_PLAY_GESTURE_ANIMS(pedFranklin(), TRUE)
				ENDIF
				
				bGiveFranklinCash = FALSE
				
				CLEANUP_UBER_PLAYBACK()
				REMOVE_CUTSCENE()
				REMOVE_BLIPS_ON_FAIL()
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 10.9)
				//Set_Replay_Mid_Mission_Stage(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_TAKE_CAR_TO_CHOP_SHOP)) 
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_TAKE_CAR_TO_CHOP_SHOP")
				
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCarSmoke)
				AND NOT IS_ENTITY_DEAD(carPlayersJeep)
					#IF IS_NEXTGEN_BUILD
						//overheat
						ptfxCarSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_fam1_veh_smoke", carPlayersJeep, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, GET_ENTITY_BONE_INDEX_BY_NAME(carPlayersJeep, "overheat"))
					#ENDIF
					
					#IF NOT IS_NEXTGEN_BUILD
						ptfxCarSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam1_veh_smoke", carPlayersJeep, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					#ENDIF
				ENDIF
				SET_VEHICLE_AUDIO_ENGINE_DAMAGE_FACTOR(carPlayersJeep, 0.75)		
				ENABLE_VEHICLE_FANBELT_DAMAGE(carPlayersJeep, TRUE)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 1.0)
				
				SET_VEHICLE_STRONG(carPlayersJeep, TRUE)
				
				IF IS_SCREEN_FADED_OUT()
					SET_VEHICLE_ENGINE_ON(carPlayersJeep, TRUE, TRUE)
				ENDIF
				
				FADE_IN_IF_NEEDED()
				iTakeCarToChopShopStage++
			ENDIF
		BREAK
		
		CASE 5
		
			//Dont let locates kick in until franklin is in the car
			IF IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)	
			
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.35)
							
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CHOP", CONV_PRIORITY_VERY_HIGH)
					
					//sLocatesData.LocationBlip = CREATE_BLIP_FOR_COORD(<<-1104.3127, -1975.6726, 12.0489>>, TRUE)
					
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 7.9)
					iTakeCarToChopShopStage++
				ELSE
					IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<-1104.3127, -1975.6726, 12.0489>>, <<3.9, 3.9, 2.25>>, TRUE, pedFranklin(), NULL, NULL, carPlayersJeep, "YB_OBJ5", "CMN_FLEAVE", "", "", "CMN_FLEAVE", "YB_GETIN", "CMN_GENGETBCK", FALSE, TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
		
			IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<-1104.3127, -1975.6726, 12.0489>>, <<3.9, 3.9, 2.25>>, TRUE, pedFranklin(), NULL, NULL, carPlayersJeep, "YB_OBJ5", "CMN_FLEAVE", "", "", "CMN_FLEAVE", "YB_GETIN", "CMN_GENGETBCK", FALSE, TRUE)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.35)
		
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 6.9)
				iTakeCarToChopShopStage++
				
			ENDIF
		BREAK
			
				
		CASE 7
			
			IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<-1104.3127, -1975.6726, 12.0489>>, <<3.9, 3.9, 2.25>>, TRUE, pedFranklin(), NULL, NULL, carPlayersJeep, "YB_OBJ5", "CMN_FLEAVE", "", "", "CMN_FLEAVE", "YB_GETIN", "CMN_GENGETBCK", FALSE, TRUE)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.75)
		
			IF PREPARE_MUSIC_EVENT("FAM1_RADIO_START")
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 5.45)
				TRIGGER_MUSIC_EVENT("FAM1_RADIO_START")
				iTakeCarToChopShopStage++
			ENDIF
		BREAK
		
		CASE 8
		CASE 9
		CASE 10
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		CASE 15
			
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<-1104.3127, -1975.6726, 12.0489>>, <<3.9, 3.9, 2.25>>, TRUE, pedFranklin(), NULL, NULL, carPlayersJeep, "YB_OBJ5", "CMN_FLEAVE", "", "", "CMN_FLEAVE", "YB_GETIN", "CMN_GENGETBCK", FALSE, TRUE)
		
				KILL_FACE_TO_FACE_CONVERSATION()
				
				WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(carPlayersJeep, 4.0)
					WAIT(0)
				ENDWHILE
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_01_AP, TRUE)
				SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_01_AP, TRUE)
				FORCE_SHOP_RESET(CARMOD_SHOP_01_AP)
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			
				mission_stage = STAGE_CHOP_SHOP_CUTSCENE				
			ENDIF
			
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iTakeCarToChopShopStage = 8
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_DRIVE", CONV_PRIORITY_VERY_HIGH)
					iTakeCarToChopShopStage= 9
				ENDIF
			ENDIF
						
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iTakeCarToChopShopStage = 9
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CAB", CONV_PRIORITY_VERY_HIGH)
					iTakeCarToChopShopStage = 10
				ENDIF
			ENDIF
						
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iTakeCarToChopShopStage = 10
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_OKAY", CONV_PRIORITY_VERY_HIGH)
					IF NOT IS_PED_INJURED(pedFranklin())
						TASK_USE_MOBILE_PHONE(pedFranklin(), TRUE)
						//Line below added by Steve T. Resolves #1895413
						SET_PED_PHONE_PALETTE_IDX(pedFranklin(), GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER(CHAR_FRANKLIN)) 
					ENDIF
					SETTIMERA(0)
					iTakeCarToChopShopStage = 11
				ENDIF
			ENDIF
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iTakeCarToChopShopStage = 11
			AND TIMERA() > 3300
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_FCALL", CONV_PRIORITY_VERY_HIGH)
					iTakeCarToChopShopStage = 12
				ENDIF
			ENDIF
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iTakeCarToChopShopStage = 12
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_CHEERS", CONV_PRIORITY_VERY_HIGH)
					IF NOT IS_PED_INJURED(pedFranklin())
						TASK_USE_MOBILE_PHONE(pedFranklin(), FALSE)
					ENDIF
					iTakeCarToChopShopStage = 13
				ENDIF
			ENDIF
						
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iTakeCarToChopShopStage = 13
				//Miss out if quite near the chop shop.
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1104.3127, -1975.6726, 12.0489>>) > 700.0
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_TALK1", CONV_PRIORITY_VERY_HIGH)
						iTakeCarToChopShopStage = 14
					ENDIF
				ELSE
					iTakeCarToChopShopStage = 14
				ENDIF
			ENDIF
						
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iTakeCarToChopShopStage = 14
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_TALK2", CONV_PRIORITY_VERY_HIGH)
					bRadioTurnedOff = FALSE
					iTakeCarToChopShopStage = 15
				ENDIF
			ENDIF
			
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND NOT bRadioTurnedOff 
			AND iTakeCarToChopShopStage >= 15
			//	IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_RADIOFF", CONV_PRIORITY_VERY_HIGH)
					//SET_RADIO_TO_STATION_NAME("OFF")
					//1263357
					//1491848
					bRadioTurnedOff = TRUE
				//ENDIF
			ENDIF

			//Franklin says to take this exit for the freeway.
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND bToldToExitFreeway = FALSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-848.585388,-1785.808838,39.207729>>, <<-908.421509,-1853.111450,31.637165>>, 21.250000)
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_DIROFF", CONV_PRIORITY_VERY_HIGH)
						bToldToExitFreeway = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//Franklin says to take this left
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND bTurnLeftHere1 = FALSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-769.808594,-1758.869385,31.080801>>, <<-803.904541,-1764.791992,27.928370>>, 16.500000)
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_LEFT", CONV_PRIORITY_VERY_HIGH)
						bTurnLeftHere1 = TRUE
					ENDIF
				ENDIF
			ENDIF

			//Franklin says to take this left
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND bTurnLeftHere2 = FALSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-646.465759,-1480.473022,12.647015>>, <<-653.455872,-1520.634277,7.573039>>, 16.500000)
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_LEFT", CONV_PRIORITY_VERY_HIGH)
						bTurnLeftHere2 = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK

	ENDSWITCH
	
	// url:bugstar:2025623
	IF iTakeCarToChopShopStage > 7
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1104.3127, -1975.6726, 12.0489>>) < (DEFAULT_CUTSCENE_LOAD_DIST * 1.8)
			IF bArriveAtChopShopDialogue = TRUE
			AND bArriveAtChopShopDialogue2 = FALSE
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_ARRIVE", CONV_PRIORITY_VERY_HIGH)
					bArriveAtChopShopDialogue2 = TRUE
				ENDIF
			ENDIF
							
			IF bArriveAtChopShopDialogue = FALSE
			//AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				KILL_ANY_CONVERSATION()
				iTakeCarToChopShopStage = 15	
				bRadioTurnedOff = TRUE
				bArriveAtChopShopDialogue = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1104.3127, -1975.6726, 12.0489>>) < (DEFAULT_CUTSCENE_LOAD_DIST * 1.6)
					
		IF bGiveFranklinCash = FALSE
		
			REQUEST_CUTSCENE("FAM_1_MCS_2")	//pre-stream the cutscene
	
			CREDIT_BANK_ACCOUNT(CHAR_FRANKLIN, BAAC_CASH_DEPOSIT, 2000)
			bGiveFranklinCash = TRUE
		ELSE
			//Set cut variations for pre-streamed scene
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", pedJimmy)	
			ENDIF
	
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", pedFranklin())	
			ENDIF
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())	
			ENDIF
			
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
	AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
		IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
		ENDIF
	ELSE
		IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedFranklin())
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//VEHICLE_INDEX carTaxi
BOOL bChopShopInit
BOOL bTaxiHelpPrinted = FALSE
BOOL bTaxiTextCleared = FALSE

PROC stageChopShopCutscene()

	SET_USE_HI_DOF()

	//SEQUENCE_INDEX seqChopShopLeave

	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
		SET_VEHICLE_ENGINE_HEALTH(carPlayersJeep, GET_VEHICLE_ENGINE_HEALTH(carPlayersJeep))
		SET_ENTITY_HEALTH(carPlayersJeep, GET_ENTITY_HEALTH(carPlayersJeep))
	ENDIF

	IF NOT IS_VEHICLE_DRIVEABLE(carPlayersJeep)
	OR IS_VEHICLE_PERMANENTLY_STUCK(carPlayersJeep)
	OR IS_ENTITY_ON_FIRE(carPlayersJeep)
		//PRINT_NOW("CMN_GENDEST", DEFAULT_GOD_TEXT_TIME, 1)
			
		MISSION_FLOW_SET_FAIL_REASON("CMN_GENDEST")
		
		Mission_Failed()
	ENDIF
	
	IF IS_PED_INJURED(pedJImmy)
		//SCRIPT_ASSERT("script is killing Jimmy")
		MISSION_FLOW_SET_FAIL_REASON("YB_FAIL1")
		Mission_Failed()
	ENDIF
		

	IF NOT IS_PED_INJURED(pedFranklin())
	ENDIF
	
	//IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<-1104.3127, -1975.6726, 12.0489>>, <<3.50, 3.5, 1.75>>, TRUE, pedFranklin(), NULL, NULL, carPlayersJeep, "YB_OBJ5", "CMN_FLEAVE", "", "", "CMN_FLEAVE", "YB_GETIN", "CMN_GENGETBCK") //, NULL, TRUE)	
		
	
	SWITCH iChopShopStage 
		
		CASE 0
			SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_01_AP, TRUE)
			SET_SHOP_HAS_RUN_ENTRY_INTRO(CARMOD_SHOP_01_AP, FALSE)
			RELEASE_CHASE_ASSETS()
			RELEASE_CHASE_INITIAL_ASSETS()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
			SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS2)
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_LD_Test_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_SALVAGOON_01)
			REMOVE_PTFX_ASSET()
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEinCar01_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht01_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht02_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEinCar02_")
			REMOVE_ANIM_DICT("veh@drivebystd_ds")
					
			REQUEST_CLIP_SET("clipset@missfam1_jimmy_sit")
			REQUEST_CUTSCENE("FAM_1_MCS_2")
						
			CLEAR_PRINTS()
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
			STOP_AUDIO_SCENE("FAM1_HIGHWAY_CHASE")
			
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 3)
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 5)
			bTaxiHelpPrinted = FALSE		
			bTaxiTextCleared = FALSE
			KILL_ANY_CONVERSATION()
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")
			
			KILL_FACE_TO_FACE_CONVERSATION()
			
			DELETE_OBJECT(oiBlipObject)
			iChopShopStage++
		BREAK
		
		CASE 1
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
      			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())	
			ENDIF
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
      			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", pedJimmy)	
			ENDIF
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
      			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", pedFranklin())	
			ENDIF
			
			IF HAS_CLIP_SET_LOADED("clipset@missfam1_jimmy_sit") 
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()//
				iChopShopStage++
			ENDIF
		
		BREAK
		
		CASE 2
			REGISTER_ENTITY_FOR_CUTSCENE(carPlayersJeep, "Amandas_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(pedFranklin(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			//REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_DRIVER_NEW], "Michael", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, Asset.mnDriver)
			REGISTER_ENTITY_FOR_CUTSCENE(pedJImmy, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
			//REGISTER_ENTITY_FOR_CUTSCENE(carTaxi, "Taxi", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
			bChopShopInit = FALSE
		
			SET_CUTSCENE_FADE_VALUES(FALSE, TRUE, FALSE, FALSE)
		
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
		
			START_CUTSCENE()			
			iChopShopStage++
		BREAK
		
		CASE 3
			
			IF IS_CUTSCENE_PLAYING()
			AND bChopShopInit = FALSE
				CLEAR_AREA(<<-1127.7976, -1980.4473, 12.1875>>, 150.0, TRUE)
				FADE_IN_IF_NEEDED()
				bChopShopInit = TRUE
			ENDIF
			
			
			IF GET_CUTSCENE_TIME() > 11000.0
			AND HAS_CUTSCENE_CUT_THIS_FRAME()
			AND bTaxiHelpPrinted = FALSE
				PRINT_HELP("YB_TAXI", 9000)
				bTaxiHelpPrinted = TRUE
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 23000.0
			AND bTaxiHelpPrinted = TRUE
			AND bTaxiTextCleared = FALSE
			AND IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
				bTaxiTextCleared = TRUE
			ENDIF
			
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
			//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
			
			IF GET_CUTSCENE_TIME() >= 8332
			AND GET_CUTSCENE_TIME() < 14300
				CLEAR_AREA_OF_VEHICLES(<<-1127.7976, -1980.4473, 12.1875>>, 150.0)
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 27850
				TRIGGER_CARMOD_SHOP_INTRO_NOW(TRUE)		
			ENDIF
			
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Amandas_car", SENTINEL2)
				SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
				FREEZE_ENTITY_POSITION(carPlayersJeep, TRUE)
		    ENDIF

			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_FRANKLIN)
					TAKE_CONTROL_OF_SELECTOR_PED(pedSelector, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
				ENDIF
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()// IS_CUTSCENE_PLAYING()
						
				IF NOT IS_ENTITY_DEAD(pedJimmy)
				AND NOT IS_ENTITY_DEAD(carPLayersJeep)
					//CLEAR_PED_TASKS_IMMEDIATELY(pedJimmy)
					SET_PED_IN_VEHICLE_CONTEXT(pedJimmy, GET_HASH_KEY("MISS_FAMILY1_JIMMY_SIT"))
					SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_FRONT_RIGHT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy, TRUE)
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_FRANKLIN)
					TAKE_CONTROL_OF_SELECTOR_PED(pedSelector, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
				ENDIF
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, VS_DRIVER)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				ENDIF
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
				SET_VEHICLE_DOORS_SHUT(carPlayersJeep, TRUE)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				REPLAY_STOP_EVENT()
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				FADE_IN_IF_NEEDED()
							
				mission_stage = STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME
				iChopShopStage++
				
			ENDIF
			
		BREAK
			
	ENDSWITCH

ENDPROC

INT iTimeOfLastComment
INT iPrevMenuvalue
BOOL bHaoGreets
BOOL bCommentsOnCarBeingFixed
BOOL bPimpMyRide = TRUE

PROC JIMMY_COMMENT_ON_MODS()


	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)

		CARMOD_MENU_ENUM theMenu = GET_CURRENT_CARMOD_SHOP_MENU()
		
		INT iMenuvalue = ENUM_TO_INT(theMenu)
		
		PRINTLN("GET_CURRENT_CARMOD_SHOP_MENU: ", ENUM_TO_INT(theMenu))

		IF NOT bCommentsOnCarBeingFixed
		AND NOT IS_ENTITY_DEAD(carPlayersJeep)
			IF GET_ENTITY_HEALTH(carPlayersJeep) >= 1000
			AND GET_VEHICLE_ENGINE_HEALTH(carPlayersJeep) >= 1000
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_RECON", CONV_PRIORITY_VERY_HIGH)
					bPimpMyRide = FALSE
					bCommentsOnCarBeingFixed = TRUE
				ENDIF
			ENDIF
		ENDIF

		IF NOT bHaoGreets
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		AND iTakeJImmyHomeStage <= 4	
		AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP(CARMOD_SHOP_01_AP) < 6.0 //12.0
		AND GET_ENTITY_HEALTH(carPlayersJeep) < 1000
			IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HAO",  CONV_PRIORITY_VERY_HIGH)
				bHaoGreets = TRUE
			ENDIF
		ENDIF

		IF GET_GAME_TIMER() - iTimeOfLastComment > 18000
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		OR ((iPrevMenuvalue - iMenuvalue) != 0)
		AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
									
			SWITCH theMenu //iMenuvalue  
			
				CASE CMM_ENGINE
				CASE CMM_TURBO
				CASE CMM_EXHAUST
				CASE CMM_TRANSMISSION
	//			CASE 10
	//			CASE 29
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_MOD4",  CONV_PRIORITY_VERY_HIGH)
						iTimeOfLastComment = GET_GAME_TIMER()
					ENDIF
				BREAK
				
				CASE CMM_PAINT_JOB
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_MOD2",  CONV_PRIORITY_VERY_HIGH)
						iTimeOfLastComment = GET_GAME_TIMER()
					ENDIF
				BREAK
									
				CASE CMM_WHEELS
				CASE CMM_WHEEL_ACCS
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_MOD1",  CONV_PRIORITY_VERY_HIGH)
						iTimeOfLastComment = GET_GAME_TIMER()
					ENDIF
				BREAK
				
				CASE CMM_WINDOWS
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_MOD6",  CONV_PRIORITY_VERY_HIGH)
						iTimeOfLastComment = GET_GAME_TIMER()
					ENDIF
				BREAK
				
				CASE CMM_SUSPENSION //23 // CMM_SUSPENSION
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_MOD3",  CONV_PRIORITY_VERY_HIGH)
						iTimeOfLastComment = GET_GAME_TIMER()
					ENDIF
				BREAK
				
				CASE  CMM_BUMPERS
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_MOD5",  CONV_PRIORITY_VERY_HIGH)
						iTimeOfLastComment = GET_GAME_TIMER()
					ENDIF
				BREAK
				
				CASE CMM_MAINTAIN
				CASE CMM_ARMOUR
				CASE CMM_BRAKES
				CASE CMM_BULLBARS
				CASE CMM_CHASSIS
				CASE CMM_EXPLOSIVES
				CASE CMM_FENDERS
				CASE CMM_GRILL
				CASE CMM_HOOD
				CASE CMM_HORN
				CASE CMM_INSURANCE
				CASE CMM_LIGHTS
				CASE CMM_PLATES
				CASE CMM_ROOF
				CASE CMM_SKIRTS
				CASE CMM_SPOILER
				//CASE CMM_TRACKER
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_MODG",  CONV_PRIORITY_VERY_HIGH)
						iTimeOfLastComment = GET_GAME_TIMER()
					ENDIF
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		IF (iPrevMenuvalue = iMenuvalue)
		AND GET_GAME_TIMER() - iTimeOfLastComment > 20000
		AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_ENTITY_DEAD(carPlayersJeep)
				
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
			AND NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(carPlayersJeep)
				IF bPimpMyRide = FALSE
//					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_NOMOD2",  CONV_PRIORITY_VERY_HIGH)
//						iTimeOfLastComment = GET_GAME_TIMER()
						bPimpMyRide = TRUE
//					ENDIF
				ELSE								
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_NOMOD",  CONV_PRIORITY_VERY_HIGH)
						iTimeOfLastComment = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iPrevMenuvalue = iMenuvalue
	
	ENDIF
	
ENDPROC

BOOL bToldToGetBackInCar
BOOL bToldOnce
BOOL bToldFixOnce

PROC DEAL_WITH_CHOP_SHOP_BLIPS()
	
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
		IF bToldToGetBackInCar = FALSE						
			blipPlayersCar = CREATE_BLIP_FOR_ENTITY(carPlayersJeep)
			IF bToldOnce = FALSE
				PRINT_NOW("CMN_GENGETBCK", DEFAULT_GOD_TEXT_TIME, 1)
				bToldOnce = TRUE
			ENDIF
			bToldToGetBackInCar = TRUE
		ENDIF
	ELSE
		IF bToldToGetBackInCar = TRUE
			IF bToldFixOnce = FALSE
				PRINT_NOW("YB_OBJ6", DEFAULT_GOD_TEXT_TIME, 1)
				bToldFixOnce = TRUE
			ENDIF
			REMOVE_BLIP(blipPlayersCar)
			bToldToGetBackInCar = FALSE
		ENDIF
	ENDIF

ENDPROC

BOOL bModAppHelpDisplayed
BOOL bModAppHelpDisplayed2

INT PreStreamCut
INT iJimmySaysLetsDoitTimer
INT iTimeofLastShootingComment

PROC stageFixCarAndTakeJimmyHome()

	//Wait until car is fixed...		
	failConditionsPreChase()
	
	IF iTakeJImmyHomeStage < 7
	AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1127.7976, -1980.4473, 12.1875>>, <<75.0, 75.0, 75.0>>)	
		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
			IF GET_VEHICLE_ENGINE_HEALTH(carPlayersJeep) > 0.0
				SET_VEHICLE_ENGINE_HEALTH(carPlayersJeep, 0.0)
			ENDIF
			IF GET_VEHICLE_PETROL_TANK_HEALTH(carPlayersJeep) > 0.0
				SET_VEHICLE_PETROL_TANK_HEALTH(carPlayersJeep, 0.0)
			ENDIF			
			SET_VEHICLE_ENGINE_CAN_DEGRADE(carPlayersJeep, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCarSmoke)
		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
			IF IS_ENTITY_IN_WATER(carPlayersJeep)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 0.0)
			ELSE
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 1.0)
			ENDIF
		ELSE
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCarSmoke, "damage", 0.0)
		ENDIF
	ENDIF
	
	IF iTakeJImmyHomeStage > 0
	AND iTakeJImmyHomeStage < 5
		IF IS_PLAYER_KICKING_OFF_IN_SHOP(CARMOD_SHOP_01_AP)
		OR NOT IS_SHOP_OPEN_FOR_BUSINESS(CARMOD_SHOP_01_AP)
			MISSION_FLOW_SET_FAIL_REASON("YB_BLEWSHOP")		
			Mission_Failed()
		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_MICHAEL])
	AND NOT IS_CUTSCENE_PLAYING()
		DELETE_PED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
	ENDIF
	
	//Fade in if cutscene was skipped
	IF NOT IS_SHOP_INTRO_RUNNING(CARMOD_SHOP_01_AP)
	AND IS_SCREEN_FADED_OUT()
	AND iTakeJImmyHomeStage <= 1
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(500)
		ENDIF
	ENDIF
	
	
	SWITCH iTakeJImmyHomeStage
		
		CASE 0
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 9.0, REPLAY_IMPORTANCE_HIGH)
				
				RELEASE_CHASE_ASSETS()
				RELEASE_CHASE_INITIAL_ASSETS()				
				iTakeJImmyHomeStage++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			//AND IS_GAMEPLAY_CAM_RENDERING() 
			
				IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
					REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
				ENDIF
			
				DO_SWITCH_EFFECT(CHAR_FRANKLIN)
			
				SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_01_AP, FALSE)
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, NULL, "HAO")
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
				SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_01_AP, TRUE)
				bToldFixOnce = FALSE
				bCommentsOnCarBeingFixed = FALSE
				//Tell player to fix car / drive to mod shop
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				g_savedGlobals.sShopData.bFirstModShopRepairComplete = FALSE
				iTimeOfLastComment = GET_GAME_TIMER()
				PRINT_NOW("YB_OBJ6", DEFAULT_GOD_TEXT_TIME, 1)
				FREEZE_ENTITY_POSITION(carPlayersJeep, FALSE) // url:bugstar:2081940
				iJimmySaysLetsDoitTimer = GET_GAME_TIMER()
				bHaoGreets = FALSE		
				iTakeJImmyHomeStage++			
			ENDIF
		BREAK
		
		CASE 2
		CASE 3
		CASE 4
		CASE 5
		
			IF iTakeJImmyHomeStage = 2
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND GET_GAME_TIMER() - iJimmySaysLetsDoitTimer > 5000
			//AND IS_FOLLOW_VEHICLE_CAM_ACTIVE()
			AND IS_GAMEPLAY_CAM_RENDERING()
				IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_FIX2", CONV_PRIORITY_VERY_HIGH)
					iTakeJImmyHomeStage = 4
				ENDIF
			ENDIF
			
			IF iTakeJImmyHomeStage = 4
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND NOT IS_GAMEPLAY_CAM_RENDERING()
								
				//Franklin has lost the street race so trigger FAM1_HAO_SRL. TICK
				IF (g_savedGlobals.sRandomChars.g_bTriggeredHao1 = TRUE)
				AND NOT IS_BIT_SET(g_savedGlobals.sStreetRaceData.iRaceWon, ENUM_TO_INT(STREET_RACE_LOS_SANTOS))
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HAO_SRL", CONV_PRIORITY_VERY_HIGH)
						iTakeJImmyHomeStage = 5
					ENDIF
				ENDIF
				
				//Franklin has won the street race so trigger FAM1_HAO_SRW. TICK
				IF (g_savedGlobals.sRandomChars.g_bTriggeredHao1 = TRUE)
				AND IS_BIT_SET(g_savedGlobals.sStreetRaceData.iRaceWon, ENUM_TO_INT(STREET_RACE_LOS_SANTOS))
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HAO_SRW", CONV_PRIORITY_VERY_HIGH)
						iTakeJImmyHomeStage = 5
					ENDIF
				ENDIF
				
				//Franklin hasn't done the street race so trigger FAM1_HAO_SR.
				IF (g_savedGlobals.sRandomChars.g_bTriggeredHao1 = FALSE)
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HAO_SR", CONV_PRIORITY_VERY_HIGH)
						iTakeJImmyHomeStage = 5
					ENDIF
				ENDIF
				
			ENDIF
			
			
			DEAL_WITH_CHOP_SHOP_BLIPS()
		
			IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_01_AP)
				IF IS_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					CLEAR_PRINTS()
				ENDIF
			ENDIF
											
			IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					
				IF GET_ENTITY_HEALTH(carPlayersJeep) >= 1000
				AND GET_VEHICLE_ENGINE_HEALTH(carPlayersJeep) >= 1000
					SET_VEHICLE_AUDIO_ENGINE_DAMAGE_FACTOR(carPlayersJeep, 0.0)	
					ENABLE_VEHICLE_FANBELT_DAMAGE(carPlayersJeep, FALSE)
					IF IS_AUDIO_SCENE_ACTIVE("FAMILY_1_CAR_BREAKS_DOWN")
						STOP_AUDIO_SCENE("FAMILY_1_CAR_BREAKS_DOWN")
					ENDIF
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCarSmoke)
						STOP_PARTICLE_FX_LOOPED(ptfxCarSmoke)
					ENDIF
				ENDIF
							
				IF GET_ENTITY_HEALTH(carPlayersJeep) >= 1000
				AND GET_VEHICLE_ENGINE_HEALTH(carPlayersJeep) >= 1000
				//AND IS_FOLLOW_VEHICLE_CAM_ACTIVE()  
				//AND IS_GAMEPLAY_CAM_RENDERING()
				//AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_01_AP)
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME", TRUE)
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
					REMOVE_BLIP(blipPlayersCar)
					PRINT_HELP("AM_H_CARMOD")
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(carPlayersJeep)
					bModAppHelpDisplayed  = FALSE
					bModAppHelpDisplayed2 = FALSE
					
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 9.0, REPLAY_IMPORTANCE_HIGH)
					
					iTakeJImmyHomeStage = 6
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
		CASE 7
		CASE 8
		
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND bModAppHelpDisplayed = FALSE
			AND NOT g_savedGlobals.sSocialData.bCarAppUsed
				IF NOT HAS_PLAYER_USED_CAR_APP()
					#IF NOT FEATURE_GEN9_STANDALONE PRINT_HELP("YB_MODAPP", 12000) #ENDIF
				ENDIF
				bModAppHelpDisplayed = TRUE
			ENDIF
			//BUG:1243641
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND bModAppHelpDisplayed
			AND bModAppHelpDisplayed2 = false
			AND NOT g_savedGlobals.sSocialData.bCarAppUsed
				IF NOT HAS_PLAYER_USED_CAR_APP()
					#IF NOT FEATURE_GEN9_STANDALONE PRINT_HELP("YB_CARAPP") #ENDIF
				ENDIF
				bModAppHelpDisplayed2 = TRUE
			ENDIF
				
			
			IF iTakeJImmyHomeStage = 6			
				IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<-828.5845, 172.4130, 69.4003>>, <<7.00, 7.0, 1.75>>, TRUE, pedJimmy, NULL, NULL, carPlayersJeep, "YB_OBJ7", "CMN_JLEAVE", "", "", "CMN_JLEAVE", "YB_GETIN", "CMN_GENGETBCK", FALSE, TRUE)	
						
				IF GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP(CARMOD_SHOP_01_AP) > 20.0
					IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_HOME", CONV_PRIORITY_VERY_HIGH)
						
						
						
						//IF NOT HAS_PLAYER_VISITED_SHOP_TYPE(SHOP_TYPE_CARMOD)
							
						//ENDIF
						PreStreamCut = 0
						iTakeJImmyHomeStage = 7
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH PreStreamCut
			
				CASE 0
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-828.5845, 172.4130, 69.4503>>) < DEFAULT_CUTSCENE_LOAD_DIST
						REQUEST_CUTSCENE("FAM_1_EXT_2")
						REQUEST_MODEL(PROP_LRGGATE_02_LD)
						PreStreamCut++
					ENDIF
				BREAK
				
				CASE 1
				
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//	      				IF NOT IS_PED_INJURED(pedJimmy)
//							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", pedJimmy)	
//						ENDIF						
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())	
						PreStreamCut++
					ENDIF
				
				BREAK
			
			ENDSWITCH
			
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<-828.5845, 172.4130, 69.4003>>, <<5.00, 5.0, 1.75>>, TRUE, pedJimmy, NULL, NULL, carPlayersJeep, "YB_OBJ7", "CMN_JLEAVE", "", "", "CMN_JLEAVE", "YB_GETIN", "CMN_GENGETBCK", FALSE, TRUE)	
				WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(carPlayersJeep)
					
					REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FatherAndSon")
					
					WAIT(0)
				ENDWHILE
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGH)
				
				//RELEASE_PED_TO_FAMILY_SCENE(pedJimmy)
				iChopShopStage = 0
				mission_stage = STAGE_DROP_OFF_CUTSCENE
			ENDIF
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iTakeJImmyHomeStage = 7
				IF CREATE_CONVERSATION(myScriptedSpeech,  "FAM1AUD", "FAM1_FJCHAT", CONV_PRIORITY_VERY_HIGH)
					iTakeJImmyHomeStage = 8
				ENDIF
			ENDIF
			
			
			IF iTakeJImmyHomeStage = 8
			AND GET_GAME_TIMER() - iTimeofLastShootingComment > 10000
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
					IF CREATE_CONVERSATION(myScriptedSpeech,  "FAM1AUD", "FAM1_FRKILL", CONV_PRIORITY_VERY_HIGH)
						iTimeofLastShootingComment = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
			
			IF (NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip))
			OR (IS_ENTITY_IN_WATER(carPlayersJeep))
				IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				ENDIF
			ELSE
				IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedJimmy)
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH

	IF iTakeJImmyHomeStage > 1
	AND iTakeJImmyHomeStage < 6
		JIMMY_COMMENT_ON_MODS()
	ENDIF
	
ENDPROC


PROC stageDropOffCutscene()

	SET_USE_HI_DOF()

	//SEQUENCE_INDEX seqChopShopLeave

	IF NOT IS_ENTITY_DEAD(carPlayersJeep)
		SET_VEHICLE_ENGINE_HEALTH(carPlayersJeep, GET_VEHICLE_ENGINE_HEALTH(carPlayersJeep))
		SET_ENTITY_HEALTH(carPlayersJeep, GET_ENTITY_HEALTH(carPlayersJeep))
	ENDIF

	IF NOT IS_VEHICLE_DRIVEABLE(carPlayersJeep)
	OR IS_VEHICLE_PERMANENTLY_STUCK(carPlayersJeep)
	OR IS_ENTITY_ON_FIRE(carPlayersJeep)
		//PRINT_NOW("CMN_GENDEST", DEFAULT_GOD_TEXT_TIME, 1)
			
//		MISSION_FLOW_SET_FAIL_REASON("CMN_GENDEST")
//		
//		Mission_Failed()
	ENDIF
	
	IF IS_PED_INJURED(pedJImmy)
//		//SCRIPT_ASSERT("script is killing Jimmy")
//		MISSION_FLOW_SET_FAIL_REASON("YB_FAIL1")
//		Mission_Failed()
	ENDIF
		

	IF NOT IS_PED_INJURED(pedFranklin())
	ENDIF
	
	//IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<-1104.3127, -1975.6726, 12.0489>>, <<3.50, 3.5, 1.75>>, TRUE, pedFranklin(), NULL, NULL, carPlayersJeep, "YB_OBJ5", "CMN_FLEAVE", "", "", "CMN_FLEAVE", "YB_GETIN", "CMN_GENGETBCK") //, NULL, TRUE)	
	
	SET_VEHICLE_ENGINE_ON(carPlayersJeep, FALSE, FALSE)
	
	SWITCH iDropOffStage 
		
		CASE 0
			RELEASE_CHASE_ASSETS()
			SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS2)
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_LD_Test_01)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_SALVAGOON_01)
			REMOVE_PTFX_ASSET()
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEinCar01_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht01_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht02_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEinCar02_")
			REMOVE_ANIM_DICT("veh@drivebystd_ds")
											
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 3)
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 5)
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")
										
			REQUEST_CUTSCENE("FAM_1_EXT_2")
						
			CLEAR_PRINTS()
			
//			REQUEST_VEHICLE_RECORDING(1, "Fam1Chop")
//			
//			WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Fam1Chop")
//				WAIT(0)
//			ENDWHILE

			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)						
			STOP_AUDIO_SCENE("FAM1_HIGHWAY_CHASE")
			
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 3)
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 5)
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")
			
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
			
			KILL_ANY_CONVERSATION()
			
			DELETE_OBJECT(oiBlipObject)
			iDropOffStage++
		BREAK
		
		CASE 1
		
//			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//      			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", pedJimmy)	
//			ENDIF
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())	
			
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				iDropOffStage++
			ENDIF
		
		BREAK
		
		CASE 2
			
			REGISTER_ENTITY_FOR_CUTSCENE(carPlayersJeep, "Amandas_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
			//REGISTER_ENTITY_FOR_CUTSCENE(pedFranklin(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(pedJImmy, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
			// url:bugstar:2078819
			HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
			
			START_CUTSCENE()
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			iDropOffStage++
		BREAK
		
		CASE 3
		CASE 4
			
			IF iDropOffStage = 3
				IF IS_CUTSCENE_PLAYING()
					FADE_IN_IF_NEEDED()
					iDropOffStage = 4
				ENDIF
			ENDIF
						 
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				RESET_GAME_CAMERA()
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 32000
				DOOR_SYSTEM_SET_HOLD_OPEN(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].doorID, FALSE)
				DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].doorID, 1.0)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				DELETE_PED(pedJImmy)					
				
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 3000)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
			ENDIF
		
			IF NOT IS_CUTSCENE_ACTIVE()
			
				REPLAY_STOP_EVENT()
				SETTIMERA(0)
				iDropOffStage =5
			ENDIF
			
		BREAK
		
		CASE 5
			IF TIMERA() > 1000
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				Mission_Passed()
			ENDIF
		BREAK
				
	ENDSWITCH

ENDPROC

PROC stageFailCutscene()

	SET_USE_HI_DOF()

	SWITCH iFailCutsceneStage 
		
		CASE 0
			
			TRIGGER_MUSIC_EVENT("FAM1_FAIL")	
			REMOVE_BLIPS_ON_FAIL()
		
			SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS2)
			SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS2)
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_LD_Test_01)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_SALVAGOON_01)
			REMOVE_PTFX_ASSET()
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEinCar01_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht01_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEonYacht02_")
			REMOVE_ANIM_DICT("MISSFAM1_YACHTBATTLEinCar02_")
			REMOVE_ANIM_DICT("veh@drivebystd_ds")
			REMOVE_CUTSCENE()
			//NEW_LOAD_SCENE_START_SPHERE(<<1203.360596,-2561.645752,36.018429>>, 26.0)
			
			NEW_LOAD_SCENE_START(<<1207.549072,-2559.342041,37.093716>>, NORMALISE_VECTOR(<<1206.660645,-2559.403809,37.021198>> - <<1207.549072,-2559.342041,37.093716>>), 80.0)
						
			iFailCutsceneStage++
		BREAK
		
		CASE 1
			CLEAR_PRINTS()
			
			REQUEST_VEHICLE_RECORDING(2, "Fam1New")
			REQUEST_VEHICLE_RECORDING(1, "Fam1New")
			REQUEST_VEHICLE_RECORDING(1, "Fam1End")
					
			SETTIMERA(0)
			
			WHILE (NOT IS_NEW_LOAD_SCENE_LOADED() OR TIMERA() < 2000)
				WAIT(0)
			ENDWHILE
			
			STOP_AUDIO_SCENE("FAM1_HIGHWAY_CHASE")
			
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 3)
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 5)
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedJimmy, "JIMMY")
			
			KILL_ANY_CONVERSATION()
			CLEANUP_UBER_PLAYBACK()
			
			iFailCutsceneStage++
		BREAK
		
		CASE 2
				
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "Fam1New")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Fam1New")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Fam1End")
		
				IF eChaseStage > CHASE_CATCH_JIMMY_DETACH
					REQUEST_CUTSCENE("FAM_1_EXT_ALT2")
				ELSE
					REQUEST_CUTSCENE("FAM_1_EXT_ALT3")
				ENDIF
				
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), truckYachtPacker, 20000)
				
				IF NOT IS_PED_INJURED(pedFranklin())
				AND NOT IS_ENTITY_DEAD(truckYachtPacker)
				AND NOT IS_ENTITY_DEAD(carPlayersJeep)
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep, VS_DRIVER)
					ENDIF
					
					IF eChaseStage >= CHASE_FRANKLIN_CLIMBS_BACK_INTO_CAR
						CLEAR_PED_TASKS(pedFranklin())
						TASK_LOOK_AT_ENTITY(pedFranklin(), truckYachtPacker, 20000)
					
						IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
							SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
						ENDIF
					ENDIF
					
				ENDIF
				
				IF NOT IS_PED_INJURED(pedJimmy)
				AND NOT IS_ENTITY_DEAD(truckYachtPacker)
				AND NOT IS_ENTITY_DEAD(carPlayersJeep)
					
					IF eChaseStage >= CHASE_CATCH_JIMMY_DETACH
						
						CLEAR_PED_TASKS(pedJimmy)
						TASK_LOOK_AT_ENTITY(pedJimmy, truckYachtPacker, 20000)
					
						IF NOT IS_PED_IN_VEHICLE(pedJimmy, carPlayersJeep)
							SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_BACK_RIGHT)
						ENDIF
					ENDIF
					
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(truckYachtPacker)
			
					STOP_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker)
					FREEZE_ENTITY_POSITION(truckYachtPacker, FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 1, "Fam1New")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 207553)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(truckYachtPacker, TRUE)
					//SET_PLAYBACK_SPEED(truckYachtPacker, 0.80)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
					STOP_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer)
					FREEZE_ENTITY_POSITION(truckYachtPackerTrailer, FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, 2, "Fam1New")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, 207553)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(truckYachtPackerTrailer, TRUE)
					//SET_PLAYBACK_SPEED(truckYachtPackerTrailer, 0.80)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					STOP_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep)
					FREEZE_ENTITY_POSITION(carPlayersJeep, FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 1, "Fam1End")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep, 6000)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(carPlayersJeep, TRUE)
				ENDIF	
			
				initialCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				destinationCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				
//				SET_CAM_COORD(_cams_index,<<1165.661865,-2571.093750,33.868950>>)
//				SET_CAM_ROT(_cams_index,<<5.430789,-0.019851,-56.786278>>)
//				SET_CAM_FOV(_cams_index,31.395754)
//
//				SET_CAM_COORD(_cams_index,<<1165.621826,-2571.119629,34.373425>>)
//				SET_CAM_ROT(_cams_index,<<5.430789,-0.019851,-56.786278>>)
//SET_CAM_FOV(_cams_index,31.395754)
				
				SET_CAM_COORD(initialCam,<<1203.360596,-2561.645752,36.018429>>)
				SET_CAM_ROT(initialCam,<<-1.537818,0.000000,87.200500>>)
				SET_CAM_FOV(initialCam,36.856476)

				SET_CAM_COORD(destinationCam,<<1203.100586,-2563.073486,36.018429>>)
				SET_CAM_ROT(destinationCam,<<0.612468,-0.000000,80.394722>>)
				SET_CAM_FOV(destinationCam,36.856476)

				SET_CAM_FAR_DOF(destinationCam, 290.5)
				SET_CAM_NEAR_DOF(destinationCam, 5.0)

				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 5000, GRAPH_TYPE_ACCEL, GRAPH_TYPE_LINEAR)
				
				CLEAR_AREA(<<267.730438,-2688.268311,18.785252>>, 50.00, TRUE)
						
				SETTIMERA(0)
				SET_CUTSCENE_RUNNING(TRUE)
				
				KILL_ANY_CONVERSATION()
				DELETE_OBJECT(oiBlipObject)
				iFailCutsceneStage++
				SETTIMERA(0)
			ENDIF
		BREAK
		
		CASE 3
		
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())	
				
//			IF NOT IS_PED_INJURED(pedJimmy)	//1744099 
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", pedJimmy)	
//			ENDIF
				
			IF TIMERA() > 3500
			AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						
				IF NOT IS_ENTITY_DEAD(pedJimmy)
				AND eChaseStage > CHASE_CATCH_JIMMY_DETACH
					REGISTER_ENTITY_FOR_CUTSCENE(pedJimmy, "JIMMY", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF				
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					REGISTER_ENTITY_FOR_CUTSCENE(carPlayersJeep, "AMANDAS_CAR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF				
								
				START_CUTSCENE()
				
				iFailCutsceneStage++
				SETTIMERA(0)
			ENDIF
		BREAK		
		
		
		CASE 4
			
			IF IS_CUTSCENE_PLAYING()
			
				DELETE_VEHICLE(cameraCar)
				DELETE_VEHICLE(cameraCarCamera)
				DELETE_VEHICLE(truckYachtPacker)
			
				SET_CUTSCENE_RUNNING(FALSE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				DISABLE_CELLPHONE(TRUE)
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					STOP_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep)
				ENDIF
				iFailCutsceneStage++
			ENDIF		
			
		BREAK

				
		CASE 5
			//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael", PLAYER_ZERO)
			IF NOT IS_CUTSCENE_PLAYING() // IS_CUTSCENE_ACTIVE()
			//IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				iFailCutsceneStage++
		
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)				
				IF eChaseStage >= CHASE_CATCH_JIMMY_DETACH
					IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					AND NOT IS_PED_INJURED(pedJimmy)
						SET_PED_INTO_VEHICLE(pedJimmy, carPlayersJeep, VS_BACK_LEFT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
				ENDIF
				
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
				SET_ENTITY_COORDS(carPlayersJeep,<<1195.4417, -2563.0918, 35.0116>>)
				SET_ENTITY_HEADING(carPlayersJeep, 289.3138)
				
				SET_VEHICLE_ON_GROUND_PROPERLY(carPlayersJeep)
				
				RESET_GAME_CAMERA()
					
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				FADE_IN_IF_NEEDED()
				Mission_Failed()					
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF (IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() AND iFailCutsceneStage < 4)
				
		DO_SCREEN_FADE_OUT(500)
		WHILE IS_SCREEN_FADING_OUT()
			WAIT(0)
		ENDWHILE
		
		WHILE NOT HAS_CUTSCENE_FINISHED()
			WAIT(0)
		ENDWHILE
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(carPlayersJeep)
			STOP_PLAYBACK_RECORDED_VEHICLE(carPlayersJeep)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
		ENDIF
		DELETE_VEHICLE(truckYachtPacker)
		DELETE_VEHICLE(truckYachtPackerTrailer)
		RESET_GAME_CAMERA()
		iFailCutsceneStage = 5
	ENDIF

ENDPROC



#IF IS_DEBUG_BUILD
		
	PROC jSkip()
	
		KILL_ANY_CONVERSATION()
			
		SWITCH mission_stage
	
			CASE STAGE_INITIALISE //keep compiler happy
			BREAK
			
			CASE STAGE_INTRO_MOCAP_CUT
				STOP_CUTSCENE()
			BREAK
			
			CASE STAGE_GET_INTO_AMANDAS_CAR
			
				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
				AND NOT IS_ENTITY_DEAD(pedFranklin())
					
					CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
					IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
						SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
					ENDIF
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carPlayersJeep)
					ENDIF
				
				ENDIF
			
			BREAK
			
			CASE STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME
				
				IF iTakeJImmyHomeStage = 2
					IF NOT IS_ENTITY_DEAD(carPlayersJeep)
						SET_ENTITY_HEALTH(carPlayersJeep, 1000)
						SET_VEHICLE_ENGINE_HEALTH(carPlayersJeep, 1000)
						SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
					ENDIF
				ENDIF
				
				IF iTakeJImmyHomeStage = 3
					IF NOT IS_ENTITY_DEAD(carPlayersJeep)
						LOAD_SCENE(vJeepStartPosition)
						SET_ENTITY_COORDS(carPlayersJeep, vJeepStartPosition)
					ENDIF
				ENDIF
						
			BREAK
			
//			CASE STAGE_GET_TO_LOCATION
//			
//				IF NOT IS_ENTITY_DEAD(carPlayersJeep)
//				AND NOT IS_ENTITY_DEAD(pedFranklin())
//				
//					//SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1995.1901, -164.2914, 28.7928 >>)
//					//SET_ENTITY_HEADING(carPlayersJeep, 93.5534)
//					
//						
//					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<-1927.2472, -176.8996, 34.2988>>)
//					SET_ENTITY_HEADING(carPlayersJeep, 76.7695)
//					
//					LOAD_SCENE(<<-1927.2472, -176.8996, 34.2988>>)
//					
//					SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 10.0)
//					IF NOT IS_PED_IN_VEHICLE(pedFranklin(), carPlayersJeep)
//						SET_PED_INTO_VEHICLE(pedFranklin(), carPlayersJeep, VS_FRONT_RIGHT)
//					ENDIF
//					
//					CLEAR_GPS_MULTI_ROUTE()
//					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					bCustomGPSActive = TRUE
//					iGetToLocation = 3
//					//SET_ENTITY_COORDS(carPlayersJeep, vMissionLocation)
//					//SCRIPT_ASSERT("boom time")
//				ENDIF
//										
//			BREAK
			
		ENDSWITCH
		
	ENDPROC
	
	PROC pSkip()
	
//		SWITCH mission_stage
//			
//
//			
//		ENDSWITCH
		
	ENDPROC
	
			
	PROC debugProcedures()
		
		DONT_DO_J_SKIP(sLocatesData)
	
		IF DOES_ENTITY_EXIST(cameraCar)
			IF NOT IS_ENTITY_DEAD(cameraCar)
				bcameraCarAlive = TRUE
			ENDIF
		ENDIF

		IF DOES_ENTITY_EXIST(cameraCarCamera)
			IF NOT IS_ENTITY_DEAD(cameraCarCamera)
				bcameraCarCameraAlive = TRUE
			ENDIF
		ENDIF

		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_I)
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "FAM1_BWAA", "FRANKLIN", SPEECH_PARAMS_FORCE_FRONTEND)
			ENDIF	
		ENDIF
	
	
		
		iDebugMissionStage = ENUM_TO_INT(mission_stage)
		iDebugSkipMissionStage = ENUM_TO_INT(skip_mission_stage)
		
		iDebugBoomBaddie = ENUM_TO_INT(eBoomBaddie)
		

		IF bDebugInitialised = FALSE
			//Init debug stuff.
		
			
			family1WidgetGroup = START_WIDGET_GROUP("Family 1 - Father & Son")
											
				//ADD_WIDGET_BOOL("bFranklinIsJumpingOntoCar",bFranklinIsJumpingOntoCar)
				
				ADD_WIDGET_BOOL("bIsSuperDebugEnabled", bIsSuperDebugEnabled)
				ADD_WIDGET_BOOL("Enable Debug Processing", bDebugOn)
				
				ADD_WIDGET_BOOL("bForceOffAfter1Box", bForceOffAfter1Box)
				
				
				ADD_WIDGET_INT_READ_ONLY("iGetToLocation", iGetToLocation)
//				ADD_WIDGET_VECTOR_SLIDER("vSittingPos", vSittingPos, -3000.0, 3000.0, 0.05)
//				ADD_WIDGET_VECTOR_SLIDER("vSittingRot", vSittingRot, -3000.0, 3000.0, 0.05)
				
				ADD_WIDGET_INT_READ_ONLY("iTimelapseCut", sTimelapse.iTimelapseCut)
				ADD_WIDGET_INT_READ_ONLY("i_current_event", i_current_event)
				ADD_WIDGET_INT_READ_ONLY("mission_stage", iDebugMissionStage)
				ADD_WIDGET_INT_READ_ONLY("skip_mission_stage", iDebugSkipMissionStage)
				ADD_WIDGET_INT_READ_ONLY("eChaseStage", iChaseStage)
				ADD_WIDGET_INT_READ_ONLY("eBalanceState", iBalanceStage)
				ADD_WIDGET_INT_READ_ONLY("iJimmysLegState", iJimmysLegState)
				ADD_WIDGET_INT_READ_ONLY("eBoomBaddie", iDebugBoomBaddie)
				ADD_WIDGET_INT_READ_ONLY("iBalanceStage", iBalanceStage)
				ADD_WIDGET_INT_READ_ONLY("iLeanStage", iLeanStage)
				ADD_WIDGET_INT_READ_ONLY("iSetPieceFlatbed", iSetPieceFlatbed)
				ADD_WIDGET_INT_READ_ONLY("iChopShopStage", iChopShopStage)
				ADD_WIDGET_INT_READ_ONLY("iDropOffStage", iDropOffStage)
				ADD_WIDGET_INT_READ_ONLY("iTakeJimmyHomeStage", iTakeJimmyHomeStage)
				ADD_WIDGET_INT_READ_ONLY("iRageText", iRageText)
				ADD_WIDGET_INT_READ_ONLY("iTakeCarToChopShopStage", iTakeCarToChopShopStage)	
				
				ADD_WIDGET_INT_READ_ONLY("iControlBoxes", iControlBoxes)		
				ADD_WIDGET_INT_READ_ONLY("iControlBoxGuyDeath", iControlBoxGuyDeath)	
					
																
				ADD_WIDGET_FLOAT_READ_ONLY("fRecordingProgress", fRecordingProgress)
				
				ADD_WIDGET_INT_SLIDER("iDebugStage", iDebugStage, -100, 1000, 1)
				ADD_WIDGET_BOOL("bRunDebugAttachedCamera", bRunDebugAttachedCamera)
				
				
				ADD_WIDGET_BOOL("bStartAudioScene", bStartAudioScene)
				ADD_WIDGET_BOOL("bPreStreamCut1", bPreStreamCut1)
				ADD_WIDGET_BOOL("bPreStreamCut2", bPreStreamCut2)
				ADD_WIDGET_BOOL("bUberRecReadyToGo", bUberRecReadyToGo)
						
				ADD_WIDGET_BOOL("bRecordTrailer", bRecordTrailer)
				ADD_WIDGET_BOOL("bSaveRecording", bSaveRecording)
				ADD_WIDGET_FLOAT_READ_ONLY("fBoomratio", fBoomratio)
				
				
				START_WIDGET_GROUP("Franklin on Bonnet")					
					ADD_WIDGET_FLOAT_READ_ONLY("fForwardSpeedPrev1", fForwardSpeedPrev1)
					ADD_WIDGET_FLOAT_READ_ONLY("fForwardSpeedPrev2", fForwardSpeedPrev2)
					ADD_WIDGET_FLOAT_READ_ONLY("fForwardSpeed", fForwardSpeed)
					ADD_WIDGET_FLOAT_READ_ONLY("fFowardAccel", fFowardAccel)
					ADD_WIDGET_FLOAT_READ_ONLY("fRotationAccel_average", fRotationAccel_average)
					ADD_WIDGET_FLOAT_READ_ONLY("fLeftThreshold", fLeftThreshold)
					ADD_WIDGET_FLOAT_READ_ONLY("fRightThreshold", fRightThreshold)
//					ADD_WIDGET_FLOAT_READ_ONLY("fBackwardsThreshold", fBackwardsThreshold)
					ADD_WIDGET_FLOAT_READ_ONLY("fSteadinessModifier", fSteadinessModifier)
					
				STOP_WIDGET_GROUP()
				
				
//				START_WIDGET_GROUP("Adaptive Cruise")
//					ADD_WIDGET_FLOAT_READ_ONLY("fDistanceToYachtBoardingPoint", fDistanceToYachtBoardingPoint)
//					ADD_WIDGET_FLOAT_READ_ONLY("fSlowDownForce", fSlowDownForce)
//				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Action Camera")
					ADD_WIDGET_BOOL("bcameraCarAlive", bcameraCarAlive)
					ADD_WIDGET_BOOL("bcameraCarAlive", bcameraCarCameraAlive)
				
					ADD_WIDGET_BOOL("bForceCinematicCamera", bForceCinematicCamera)
					ADD_WIDGET_BOOL("bForceCutsceneCameraAsCinematicCamera", bForceCutsceneCameraAsCinematicCamera)
					ADD_WIDGET_INT_SLIDER("iCameraSelection", iCameraSelection, 0, 2, 1)
					ADD_WIDGET_INT_READ_ONLY("iCameraCuts", iCameraCuts)
			
				STOP_WIDGET_GROUP()
										
				ADD_WIDGET_VECTOR_SLIDER("vLocateDimensions", vLocateDimensions, -3000.0, 3000.0, 0.1)
				ADD_WIDGET_BOOL("bRunDebugLocates", bRunDebugLocates)
				
				START_WIDGET_GROUP("Playback Speed")
					ADD_WIDGET_FLOAT_READ_ONLY("fDesiredPlaybackSpeed", fDesiredPlaybackSpeed )
					ADD_WIDGET_FLOAT_READ_ONLY("mainPlaybackSpeed", mainPlaybackSpeed)
					ADD_WIDGET_FLOAT_READ_ONLY("mainPlaybackSpeedModifiedForChasingCar", mainPlaybackSpeedModifiedForChasingCar)
					ADD_WIDGET_FLOAT_READ_ONLY("fMinPlaybackSpeed", fMinPlaybackSpeed)
					ADD_WIDGET_FLOAT_READ_ONLY("fMaxPlaybackSpeed", fMaxPlaybackSpeed )
					
					ADD_WIDGET_FLOAT_READ_ONLY("fVehicleActualSpeed", fVehicleActualSpeed)
					ADD_WIDGET_FLOAT_READ_ONLY("fDesiredActual", fDesiredActual)
					ADD_WIDGET_FLOAT_READ_ONLY("fMinActual", fMinActual)
					ADD_WIDGET_FLOAT_READ_ONLY("fMaxActual", fMaxActual)
					
					ADD_WIDGET_VECTOR_SLIDER("vTargetVehicleOffset",vTargetVehicleOffset, -999.9, 999.9, 0.1)
					//ADD_WIDGET_FLOAT_SLIDER("fPLaybackSpeedNumerator", fPLaybackSpeedNumerator, 0.0, 1000.0, 1.0)
					
					
					ADD_WIDGET_FLOAT_SLIDER("fBehindBoatCheckValue", fBehindBoatCheckValue, -5000.00, 5000.00, 50.0)
					
					ADD_WIDGET_BOOL("bDEbugIsInFront", bDEbugIsInFront)
					
				STOP_WIDGET_GROUP()
			
				START_WIDGET_GROUP("Dialogue Debug")
					ADD_WIDGET_BOOL("bDoDialogue", bDoDialogue)
							
					ADD_WIDGET_INT_READ_ONLY("iTimeOfLastConstantDialogue", iTimeOfLastConstantDialogue)
					
					ADD_WIDGET_BOOL("bNearMissCouldHaveHappened", bNearMissCouldHaveHappened)
					ADD_WIDGET_INT_READ_ONLY("iMissStartTime", iMissStartTime)	
					
							
					ADD_WIDGET_BOOL("bDoDialogue", bDoDialogue)
					ADD_WIDGET_BOOL("bPlayInitialDialogue", bPlayInitialDialogue)
					dialogueThatShouldBePlaying = ADD_TEXT_WIDGET("Dialogue")
					ADD_WIDGET_INT_READ_ONLY("iAltRespond", iAltRespond)
					
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Catch and Land offsets")				
					ADD_WIDGET_VECTOR_SLIDER("vTruckRearOffset", vTruckRearOffset, -100.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fJumpThreshold", fJumpThreshold, 0.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fJumpMaxSpeedVariance", fJumpMaxSpeedVariance, 0.0, 100.0, 0.1)
									
					ADD_WIDGET_FLOAT_SLIDER("fLeftThreshold", fLeftThreshold, -100.0, 100.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("fRightThreshold", fRightThreshold, -100.0, 100.0, 0.0)
					//ADD_WIDGET_FLOAT_SLIDER("fForwardsThreshold", fForwardsThreshold, -100.0, 100.0, 0.0)
					//ADD_WIDGET_FLOAT_SLIDER("fBackwardsThreshold", fBackwardsThreshold, -100.0, 100.0, 0.0)
					
							
					//ADD_WIDGET_VECTOR_SLIDER("vJeepLandingOffset", vJeepLandingOffsetFranklin, -100.0, 100.0, 0.1)
					//ADD_WIDGET_FLOAT_SLIDER("fLandThreshold", fLandThresholdFranklin, 0.0, 100.0, 0.1)
					
									
					ADD_WIDGET_FLOAT_SLIDER("vCatchJimmyOffset.x", vCatchJimmyOffset.x, -3000.0, 3000.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("vCatchJimmyOffset.y", vCatchJimmyOffset.y, -3000.0, 3000.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("vCatchJimmyOffset.z", vCatchJimmyOffset.z, -3000.0, 3000.0, 0.01)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Car on trailer attach debug")	
					ADD_WIDGET_VECTOR_SLIDER("AttachOffset", AttachOffset, -100.0, 100.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("AttachRotation", AttachRotation, -360.0, 360.0, 0.1)					
				
					ADD_WIDGET_VECTOR_SLIDER("forceVector", forceVector, -1000.0, 1000.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("forceOffsetVector", forceOffsetVector, -1000.0, 1000.0, 0.1)		
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Bad guy on Boom")
					ADD_WIDGET_VECTOR_SLIDER("vSlidePosition", vSlidePosition, -100.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fSlideLimit", fSlideLimit, -100.0, 100.0, 0.1)
					
					ADD_WIDGET_FLOAT_SLIDER("fSlideIncrement", fSlideIncrement, -100.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fFrameMultiplier", fFrameMultiplier, -1000.0, 1000.0, 0.1)
				STOP_WIDGET_GROUP()	
								
				START_WIDGET_GROUP("Box thrower debug")
					ADD_WIDGET_INT_SLIDER("iControlBoxes", iControlBoxes, 0, 10, 1)
					ADD_WIDGET_INT_SLIDER("iNumberOfBoxesThrown", iNumberOfBoxesThrown, 0, 10, 1)
					
					ADD_WIDGET_FLOAT_SLIDER("prevSyncPhase", prevSyncPhase , -1000.0, 1000.0, 0.1)
										
				STOP_WIDGET_GROUP()
				
				
				START_WIDGET_GROUP("Jackknife debug")
					ADD_WIDGET_VECTOR_SLIDER("vJackKnifeForce", vJackKnifeForce, -100.0, 100.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vJackKnifeForceOffset", vJackKnifeForceOffset, -100.0, 100.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vSlidePosition", vSlidePosition, -100.0, 100.0, 0.1)
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_INT_SLIDER("CarCol1", carColour1, 0, 255, 1)
									
			STOP_WIDGET_GROUP()		
			
			SET_LOCATES_HEADER_WIDGET_GROUP(family1WidgetGroup)
		
			SkipMenuStruct[ENUM_TO_INT(STAGE_TIME_LAPSE)].bSelectable = FALSE
			SkipMenuStruct[ENUM_TO_INT(STAGE_INITIALISE)].bSelectable = FALSE
					
			SkipMenuStruct[ENUM_TO_INT(STAGE_INTRO_MOCAP_CUT)].sTxtLabel 				= "Intro cut: family_1_int"              
			SkipMenuStruct[ENUM_TO_INT(STAGE_GET_INTO_AMANDAS_CAR)].sTxtLabel 		= "STAGE_GET_INTO_AMANDAS_CAR"
			SkipMenuStruct[ENUM_TO_INT(STAGE_GET_TO_LOCATION)].sTxtLabel 				= "STAGE_GET_TO_LOCATION"              
			//SkipMenuStruct[ENUM_TO_INT(STAGE_YACHT_GOES_BY_ON_PCH)].sTxtLabel 			= "Cutsene: Family_1_mcs_1"    
			SkipMenuStruct[ENUM_TO_INT(STAGE_DO_CHASE)].sTxtLabel 						= "STAGE_DO_CHASE"                       
			SkipMenuStruct[ENUM_TO_INT(STAGE_JIMMY_APPEARS)].sTxtLabel 					= "STAGE_JIMMY_APPEARS"       
			SkipMenuStruct[ENUM_TO_INT(STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT)].sTxtLabel  = "STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT"     
			SkipMenuStruct[ENUM_TO_INT(STAGE_TAKE_CAR_TO_CHOP_SHOP)].sTxtLabel 		= "STAGE_TAKE_CAR_TO_CHOP_SHOP"     
			SkipMenuStruct[ENUM_TO_INT(STAGE_CHOP_SHOP_CUTSCENE)].sTxtLabel 		= "STAGE_CHOP_SHOP_CUTSCENE"     
			SkipMenuStruct[ENUM_TO_INT(STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME)].sTxtLabel 		= "STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME"     
			SkipMenuStruct[ENUM_TO_INT(STAGE_DROP_OFF_CUTSCENE)].sTxtLabel 		= "STAGE_DROP_OFF_CUTSCENE"     
			
			SkipMenuStruct[ENUM_TO_INT(STAGE_FAIL_CUTSCENE)].sTxtLabel 					= "STAGE_FAIL_CUTSCENE"
			SkipMenuStruct[ENUM_TO_INT(STAGE_DEBUG)].sTxtLabel 							= "STAGE_DEBUG"                
					
			bDebugInitialised = TRUE
		ENDIF
		
		iBalanceStage = ENUM_TO_INT(eBalanceState)
			
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
	
		FLOAT fX, fY, fZ, fW
		VECTOR vCoords
		
		ENTITY_INDEX thisCar = GET_FOCUS_ENTITY_INDEX()
		IF DOES_ENTITY_EXIST(thisCar)
			GET_ENTITY_QUATERNION(thisCar, fX, fY, fZ, fW)
			vCoords = GET_ENTITY_COORDS(thiscar)
		
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_QUATERNION(thisCar,") SAVE_FLOAT_TO_DEBUG_FILE(fX)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fY)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fZ)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fW)
			SAVE_STRING_TO_DEBUG_FILE(")")
			
			
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS(thisCar,") SAVE_VECTOR_TO_DEBUG_FILE(vCoords) 
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
	ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2)
		
			REQUEST_MODEL(prop_LD_breakmast)
			
			IF HAS_MODEL_LOADED(prop_LD_breakmast)
				CREATE_OBJECT(prop_LD_breakmast, GET_ENTITY_COORDS(PLAYER_PED_ID()))				
			ENDIF
		
		ENDIF
		
		
		IF bDebugOn
			
				
			iChaseStage = ENUM_TO_INT(eChaseStage)
			
			iBalanceStage = ENUM_TO_INT(eBalanceState)
			
			IF NOT IS_ENTITY_DEAD(pedBadguy1)
				SET_PED_NAME_DEBUG(pedBadguy1, "pedBadguy1")
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedBadguy2)
				SET_PED_NAME_DEBUG(pedBadguy2, "pedBadguy2")
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedBadGuy3BoxThrower)
				SET_PED_NAME_DEBUG(pedBadGuy3BoxThrower, "pedBadGuy3BoxThrower")
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedBadguy4BoomGuy)
				SET_PED_NAME_DEBUG(pedBadguy4BoomGuy, "pedBadguy4BoomGuy")
			ENDIF
			
			//iChaseStageAdvance = ENUM_TO_INT(eChaseStage)
			
	//		IF DOES_ENTITY_EXIST(pedFranklin())
	//		AND DOES_ENTITY_EXIST(truckYachtPacker)
	//		
	//			IF NOT IS_ENTITY_DEAD(pedFranklin())
	//			AND NOT IS_ENTITY_DEAD(truckYachtPacker)
	//			
	//				
	//				//Position of where he actually is on takeoff
	//				VECTOR vecA = GET_PED_BONE_COORDS(pedFranklin(), BONETAG_PELVIS, <<0.0, 0.0, 0.0>>)
	//				VECTOR vecOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(truckYachtPackerTrailer, vecA)
	//				
	//				
	//				
	//				VECTOR vecYacht = GET_ENTITY_COORDS(truckYachtPacker)
	//		
	//				
	//				//vecA - vecYacht
	//				
	//				
	//				VECTOR vecFrank = GET_ENTITY_COORDS(truckYachtPacker)
	//		
	//				
	//				//Pre calc position of ideal launch point.
	//				//vecOffset<< -0.732189, -11.339, 0.5703 >>
	//				//<< -0.100044, -9.87228, 1.56525 >>
	//				VECTOR vecB = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<-0.732189, -11.339, 0.5703>>)				
	//				
	//				//DRAW_DEBUG_SPHERE(vecB, 0.5)
	//				
	//
	//				
	//				VECTOR startingOFfsetForlanding = vecA - vecB
	//				
	//				PRINTSTRING("PC?: ") PRINTVECTOR(vecYacht - vecA)PRINTNL()
	//				PRINTSTRING("vecOffset") PRINTVECTOR(vecOffset)PRINTNL()
	//				PRINTSTRING("preCalcLanding:") PRINTVECTOR(vecB) PRINTNL()
	//				PRINTSTRING("Franklin:") PRINTVECTOR(vecA)PRINTNL()		
	//				PRINTSTRING("difference:") PRINTVECTOR(startingOFfsetForlanding) PRINTNL()		PRINTNL()
	//				
	//				
	//			ENDIF
	//		ENDIF
			
			
			
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1)
				
				//iChaseStageAdvance++
				
			//	eChaseStage = INT_TO_ENUM(CHASE_STAGE_FLAG, iChaseStageAdvance)
			
				IF NOT IS_ENTITY_DEAD(truckYachtPacker)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPacker)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPacker, 5000.0)
					ENDIF
				ENDIF
			
				IF NOT IS_ENTITY_DEAD(truckYachtPackerTrailer)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(truckYachtPackerTrailer)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(truckYachtPackerTrailer, 5000.0)
					ENDIF
			
					WAIT(0)
					
					IF NOT IS_ENTITY_DEAD(carPlayersJeep)
						SET_ENTITY_COORDS(carPlayersJeep, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckYachtPackerTrailer, <<0.0, -10.0, 0.0>>))	
						SET_ENTITY_HEADING(carPlayersJeep, GET_ENTITY_HEADING(truckYachtPackerTrailer))
						SET_VEHICLE_FORWARD_SPEED(carPlayersJeep, 15.0)
					ENDIF
					
				ENDIF
						
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 1.0)
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdA)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIdA, 1.0)
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdB)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIdB, 1.0)
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDboomGuy)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIDboomGuy, 1.0)
				ENDIF
				
//				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdBoomJimmy)
//					SET_SYNCHRONIZED_SCENE_PHASE(sceneIdBoomJimmy, 1.0)
//				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdSon)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIdSon, 1.0)
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdFailure)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIdFailure, 1.0)
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIDclimbOutOfCar)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIDclimbOutOfCar, 1.0)
				ENDIF

				IF NOT IS_PED_INJURED(pedBadguy1)
					APPLY_DAMAGE_TO_PED(pedBadguy1, 100, TRUE)
				ENDIF

				IF NOT IS_PED_INJURED(pedBadguy2)
					APPLY_DAMAGE_TO_PED(pedBadguy2, 100, TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedBadGuy3BoxThrower)
					APPLY_DAMAGE_TO_PED(pedBadGuy3BoxThrower, 100, TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedBadguy4BoomGuy)
					APPLY_DAMAGE_TO_PED(pedBadguy4BoomGuy, 100, TRUE)
				ENDIF

			ENDIF
			
			
			//IF NOT IS_KEYBOARD_KEY_PRESSED(KEY_J)	//Let menu do P skips but not J skips.
//			OR mission_stage = STAGE_GET_TO_LOCATION
//			OR mission_stage = STAGE_DO_CHASE
//			OR mission_stage = STAGE_JIMMY_APPEARS
//			OR mission_stage = STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
//			OR mission_stage = STAGE_TAKE_CAR_TO_CHOP_SHOP
//			OR mission_stage = STAGE_CHOP_SHOP_CUTSCENE
			
				IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(mission_stage), TRUE)
					
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
					STOP_AUDIO_SCENES()
					DO_SCREEN_FADE_OUT(500)
					
					WHILE IS_SCREEN_FADING_OUT()
						WAIT(0)
					ENDWHILE
					
					STOP_CUTSCENE(TRUE)
					WHILE NOT HAS_CUTSCENE_FINISHED()
						WAIT(0)
					ENDWHILE
					WHILE IS_CUTSCENE_ACTIVE()
						WAIT(0)
					ENDWHILE
					REMOVE_CUTSCENE()
					
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					KILL_CHASE_HINT_CAM(localChaseHintCamStructButtonPress)
					
					TRIGGER_MUSIC_EVENT("FAM1_FAIL")
					
					REMOVE_BLIPS_ON_FAIL()
					
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					CLEAR_GPS_MULTI_ROUTE()
					bCustomGPSActive = FALSE
					
					IF bUberInit = TRUE
						CLEANUP_UBER_PLAYBACK(TRUE)
						bUberInit = FALSE
					ENDIF
					
					KILL_ANY_CONVERSATION()
					
					SET_CUTSCENE_RUNNING(FALSE)
				
					iTakeCarToChopShopStage = 0
					iGetToLocation = 0
					eChaseStage = CHASE_SETUP
					//iQuickCutsceneStage = 0
					iControlGunshots = 0
					eBoomBaddie = BOOMBADDIE_CREATE_AND_ANIMATE_INTRO
					iDontShootChat = 0
					iControlBoxes = 0					
					iGetIntoCarStage = 0
					i_current_event = 0
					iSetPieceFlatbed = 0
					iJimmysLegState = 0
					
					iFirstTrafficCar = 0
					iSecondTrafficCar = 0
					iCatchJimmySubstages = 0
					iWrestleDialogue = 0
					iSmashDialogues = 0
					iChopShopStage = 0
					iDropOffStage = 0
					iTakeJimmyHomeStage = 0
					
					iDealWithFailDialogue = 0
					
					sTimelapse.iTimelapseCut = 0
					
					iFirstTrailerToProcessTraffic = 93
					iFirstTrailerToProcessSetPiece = 6
					
					iRageText = 0
					iCameraCuts = 0
					iCameraSelection = 0
					
					iTimeOfPreloadedJimXSpeech = -1
					
					iClosestPlayer = 0
					iClosest = 0
					
					fRecordingProgress = 0.0
				
//					bMusicStarted = FALSE
				
					bFranklinShoutedForHelp = FALSE
					//bJimmyCaughtDialogue = FALSE
					bBoomBaddieDialogueDisplayed = FALSE
					bDialogueForLandingOnYacht = FALSE
					
					bMichaelWarnsFranklinOfGuy2 = FALSE
					bPrintCinematicCameraHelpText = FALSE
					bPrintCinematicCameraHelpText = FALSE
					bCutOfBoxThrowerOver = FALSE

					bFakeBloodInCutscene = FALSE
					
					bForceCinematicCamera = FALSE
					bForceCutsceneCameraAsCinematicCamera = FALSE
					
					//bSafeToEndMission = FALSE
					bFlagGetBackInText = FALSE
					
					bFranklinNotSaidSpeechThatWontWork = FALSE
					
					bRadioTurnedOff = TRUE
					
					//bCarBlendingIntoGamePlayAfterCut = FALSE
					
					bCloseToYachtMusicEvent = FALSE
					bMusicEvent1 = FALSE
					bMusicEvent2 = FALSE
					bMusicEvent3 = FALSE
					bMusicEvent4 = FALSE
					
					bUberRecReadyToGo = FALSE
					
					bStartAudioScene = FALSE
					
					bUberRecReadyToGo = FALSE
					bPreStreamCut1 = FALSE
					bPreStreamCut2 = FALSE
					
					bBlipsFlashing = FALSE
					
					bCarBrokeDownForFailCut = FALSE
//					bJimmyPlayingCustomSeatAnim = FALSE
					
					bStopBashing = FALSE
					
					//iTimeOfLastSegmentGrab = GET_GAME_TIMER()
					
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
					DELETE_OBJECT(oiBlipObject)
					
					DELETE_PED(pedYachtPacker)
					DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
					DELETE_PED(pedBadguy1)
					DELETE_PED(pedBadguy2)
					DELETE_PED(pedBadGuy3BoxThrower)
					DELETE_PED(pedBadguy4BoomGuy)
					DELETE_PED(pedJimmy)
					
					REMOVE_MODEL_HIDE(<<-816.72, 179.10, 72.83>>, 1.0, V_ILEV_MM_DOORM_L, TRUE)
					REMOVE_MODEL_HIDE(<<-816.11, 177.51, 72.83>>, 1.0, V_ILEV_MM_DOORM_R, TRUE)
					DELETE_OBJECT(oiLeftDoor)
					DELETE_OBJECT(oiRightDoor)
					
					SET_SHOP_HAS_RUN_ENTRY_INTRO(CARMOD_SHOP_01_AP, FALSE)
										
					DELETE_ARRAY_OF_VEHICLES(TrafficCarTrailer)
					DELETE_ARRAY_OF_VEHICLES(SetpieceCarTrailer)
					
					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(carPlayersJeep)
					
					DELETE_VEHICLE(carPlayersJeep)
					DELETE_VEHICLE(truckYachtPacker)
					DELETE_VEHICLE(truckYachtPackerTrailer)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LRGGATE_02_LD)
					
					SET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_TRACEY)
					SET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_AMANDA)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(carTracey)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(carMichael)
										
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_01")
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_CHASE_02")
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAMILY_1_BOAT_STRAIN")
					
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCarSmoke)						
						STOP_PARTICLE_FX_LOOPED(ptfxCarSmoke)
					ENDIF
						
					REMOVE_BLIP(blipPlayersCar)
					
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					
					CLEAR_ALL_FLOATING_HELP()
					
					SET_CUTSCENE_RUNNING(FALSE)
										
					initialiseMission()
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
					CLEAR_PED_WETNESS(PLAYER_PED_ID())
					
					requestAndCreateInitialStuff()
					
					//Set stage enum to required stage
					mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)
					
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
									
					MANAGE_SKIP(mission_stage)
					CLEAR_PED_WETNESS(PLAYER_PED_ID())
					RESET_GAME_CAMERA()
					
					//When a stage has been selected from the menu LAUNCH_MISSION_STAGE_MENU() returns true it passes back the stage that has been selected as an INT iReturnStage
					//This INT can then be used to change the current mission stage using your J/P skip functions.
					// iReturnStage = 0 is returned if STAGE_1 is selected from the menu.
				ENDIF
			//ENDIF
				
	//		eSelectedPointAt = INT_TO_ENUM(CINEMATIC_POINT_AT, iEnumConvertedPoint)
	//		eSelectedAttach = INT_TO_ENUM(CINEMATIC_ATTACH_TO, iEnumConvertedAttach)
	//		
			IF bRunDebugLocates
				vPlayersCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPlayersCoords, vLocateDimensions, FALSE)
			
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
					SAVE_STRING_TO_DEBUG_FILE("IS_ENTITY_AT_COORD(PLAYER_PED_ID(), ")
					SAVE_VECTOR_TO_DEBUG_FILE(vPlayersCoords)
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_VECTOR_TO_DEBUG_FILE(vLocateDimensions)
					SAVE_STRING_TO_DEBUG_FILE(", FALSE, TM_ANY_3D)")
				ENDIF
			
			ENDIF

			IF bRunDebugAttachedCamera
				
				SET_PLAYBACK_SPEED(truckYachtPackerTrailer, 0.0)
				
				WHILE NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2)
					WAIT(0)
				ENDWHILE
				
				SAVE_STRING_TO_DEBUG_FILE("Yacht")
				OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(truckYachtPackerTrailer)
			
				SAVE_STRING_TO_DEBUG_FILE("Jeep")
				OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(carPlayersJeep)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				bRunDebugAttachedCamera  = FALSE
			ENDIF

			IF IS_KEYBOARD_KEY_PRESSED(KEY_S)		
				MISSION_PASSED()
			ENDIF
		
			IF IS_KEYBOARD_KEY_PRESSED(KEY_f)	
				Mission_Failed()
			ENDIF
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 
				jSkip()	
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P) 
				pSkip()	
			ENDIF
			
		ENDIF
	
	ENDPROC

#ENDIF	


// ____________________________________ MISSION SCRIPT _____________________________________
INT iTimeOfShuntFlag

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
        Mission_Flow_Mission_Force_Cleanup()
		MISSION_CLEANUP()  
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// ____________________________________ MISSION LOOP _______________________________________
	
	SET_MISSION_FLAG(TRUE)
		
	SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, FALSE)
	
	WHILE TRUE	
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FatherAndSon")
				
		#IF IS_DEBUG_BUILD
			debugProcedures()
			//mission_stage = STAGE_DEBUG	//Comment out...
		#ENDIF
										
		updatePlaybackProgressFloat()
				
		SWITCH mission_stage 
		
			#IF IS_DEBUG_BUILD
		
				CASE STAGE_DEBUG
					//debugRoutines()
					RECORD_SETPIECES()
				BREAK
			
			#ENDIF
		
			//Stage 0		
			
			CASE STAGE_TIME_LAPSE
				timeLapseCutscene()
			BREAK
			
			CASE STAGE_INITIALISE	
				initialiseMission()		
			BREAK		
			
			CASE STAGE_INTRO_MOCAP_CUT
				stageIntroMocapCutscene()
			BREAK
			
			CASE STAGE_GET_INTO_AMANDAS_CAR				
				stageGetIntoAmandasCar()
			BREAK
			
			CASE STAGE_GET_TO_LOCATION			
				stageGetToLocation()
			BREAK
						
			CASE STAGE_DO_CHASE
			CASE STAGE_JIMMY_APPEARS
			CASE STAGE_FRANKLIN_JUMPS_BACK_ONTO_BOAT
				stageDoChase()
			BREAK
						
			CASE STAGE_TAKE_CAR_TO_CHOP_SHOP
				stageTakeCarToChopShopEntrance()
			BREAK 
			
			CASE STAGE_CHOP_SHOP_CUTSCENE
				stageChopShopCutscene()		
			BREAK 
					
			CASE STAGE_FIX_CAR_AND_TAKE_JIMMY_HOME
				stageFixCarAndTakeJimmyHome()
			BREAK 
			
			CASE STAGE_DROP_OFF_CUTSCENE
				stageDropOffCutscene()
			BREAK 
			
			CASE STAGE_FAIL_CUTSCENE				
				stageFailCutscene()
			BREAK 
			
			DEFAULT
				SCRIPT_ASSERT("Unrecognised Mission Stage!")
			BREAK
			
		ENDSWITCH	
		
		IF IS_SCREEN_FADED_OUT()
			SHUNT_RESET_FLAGS()
			iTimeOfShuntFlag = GET_GAME_TIMER()
		ELSE
			IF (GET_GAME_TIMER() - iTimeOfShuntFlag) < 5000
				PRINTLN("stopping shunt", GET_GAME_TIMER() - iTimeOfShuntFlag)
				SHUNT_RESET_FLAGS()
			ENDIF
		ENDIF
			
		WAIT(0)
		
	ENDWHILE
	
	//	should never reach here - always ends by going through the cleanup function

ENDSCRIPT					



