
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					80
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					4
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				95 //12

CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		13 //6 12
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		20 //12
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		4

const_int MAX_SKIP_MENU_LENGTH							6

const_int max_number_of_cylinders						5

const_int total_number_of_ambient_peds					26

USING "rage_builtins.sch"
USING "globals.sch"
using "script_heist.sch"
USING "flow_public_core_override.sch"
using "commands_misc.sch"
using "commands_pad.sch" 
using "commands_script.sch"
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "commands_camera.sch"
using "commands_path.sch"
using "commands_fire.sch"
using "commands_graphics.sch"
using "commands_object.sch"
using "commands_misc.sch"
using "commands_recording.sch"
using "script_player.sch"
using "script_debug.sch"
using "streamed_scripts.sch"
using "model_enums.sch"
using "cellphone_public.sch"
using "selector_public.sch"
using "dialogue_public.sch"
using "player_ped_public.sch"
using "chase_hint_cam.sch"
using "locates_public.sch"
using "script_blips.sch" 
using "traffic.sch"
using "select_mission_stage.sch"
using "replay_public.sch"
using "commands_cutscene.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
using "shop_public.sch"
USING "CompletionPercentage_public.sch"
using "vehicle_gen_public.sch"
using "timelapse.sch"
using "clearmissionarea.sch"
using "taxi_functions.sch"
using "emergency_call.sch"
using "player_scene_private.sch"
using "achievement_public.sch"
using "cheat_controller_public.sch"


struct ped_structure
	ped_index ped 
	model_names model 
	blip_index blip
	vector pos 
	vector run_to_pos
	float heading 
	int health 
	int scene_id
	bool damaged_by_player
	bool allow_body_armour
	bool created 
	weapon_type weapon
	text_label name
endstruct 


struct vehicle_struct
	vehicle_index veh
	model_names model
	blip_index blip
	vector pos 
	float heading
	float speed
	float skip_time
	int health
	float engine_health
	float petrol_tank_health
	int colour
	int recording_number
	bool been_created 
endstruct

struct object_struct 
	object_index obj
	blip_index blip
	model_names model
	vector pos 
	vector offset
	vector offset_pos_2
	vector rot
	vector offset_rot
	float heading
	float scale 
	float health
	string room_name
	int time
	bool been_created
endstruct 

struct quaternion
	float x
	float y
	float z
	float w
endstruct

enum main_mission_flow
	intro_mocap = 0,
	trevor_leadin, 
	switching_into_michael,
	get_to_the_stadium,
	play_stadium_mocap,
	run_after_lazlow, 
	stadium_cutscene, 
	uber_chase,
	storm_drain_cutscene,
	play_final_mocap,
	record_uber_chase, 
	train_crash_cutscene_test_bed, 
	detach_trailer_cutscene_test_bed,
	record_setpiece_cars, 
	load_stage_selector_assets, 
	mission_failed_stage
endenum 

enum ambient_ped_enum
	get_into_position = 0,
	talk_to_partner_status,
	walk_to_coords_status,
	look_at_poster,
	look_at_poster_status,
	play_reaction_anim_during_cutscene,
	play_reaction_chasing_lazlow,
	flee,
	do_nothing
endenum

ambient_ped_enum ambient_ped_status[total_number_of_ambient_peds] 

main_mission_flow mission_flow = intro_mocap
main_mission_flow cached_mission_flow 

vehicle_struct michaels_car
vehicle_struct truck
vehicle_struct trailer
vehicle_struct lazlows_car
vehicle_struct train[3]
vehicle_struct cylinder_truck
vehicle_struct parked_truck[3]
vehicle_struct parked_trailer[3]
vehicle_struct amandas_car

ped_structure lazlow
ped_structure michael
ped_structure tracey
ped_structure trevor
ped_structure ambient_ped[total_number_of_ambient_peds]
ped_structure camera_crew[2]
ped_structure truck_driver
ped_structure groopie
ped_structure security 
ped_structure ambient_driver

object_struct cylinder[max_number_of_cylinders]
object_struct micro_phone
object_struct video_camera
object_struct clip_board
object_struct lazlows_jeans

SELECTOR_PED_STRUCT selector_ped

bool stop_mission_fail_checks = false
bool police_car_created = false
bool lazlow_chase_cam_active = false
bool help_text_on = false
bool reminder_help_played = false
bool train_cutscene_playing = false
bool trailer_cutscene_playing = false
//bool storm_drain_cutscene_playing = false
//bool waypoint_playback_paused = false
//bool mocap_requested = false
//bool train_crash_cutscene_fail = false
bool resume_lower_priority_conversation = false
//bool secondary_audio = false
bool force_buddy_to_run = false
bool apply_fail_wanted_level = false
bool player_arrived_in_a_vehicle = false
bool allow_switch_to_contiune = false
bool taxi_drop_off_set_for_get_to_the_stadium = false
bool trigger_switch_effect_to_trevor = false
bool trevors_vehicle_set_as_vehicle_gen = false
bool deactivate_truck_blipping_system = false
bool new_load_scene_activated_for_michaels_house = false

int original_time = 0
int truck_wanted_status = 0
int uber_speed_status = 0
//int cam_help_time = 0
int train_system_status 
int cylinder_object_status[max_number_of_cylinders]
int lazlow_ai_system_status = 0
int run_after_lazlow_master_flow_system_status = 0
int michael_ai_system_status = 0
//int detach_trailer_system_status = 0
int detach_trailer_cutscene_status = 0
int deatch_trailer_dialogue_status = 0
int manual_car_recording_system_status = 0
int create_parked_vehicles_status = 0
int dialogue_status = 0
int cleanup_asset_status = 0
int intro_mocap_status = 0
int play_final_mocap_status = 0
int get_to_the_stadium_status = 0
int family_4_detach_trailer_status = 0
//int instruction_text_time
int get_to_the_stadium_dialogue_system_status = 0
int lazlow_dialogue_time
int set_piece_driver_ai_status = 0
int ambient_ped_time
int ambient_train_system_status = 0
int uber_chase_in_car_conversation = 0
int family_4_play_stadium_mocap_status = 0
int family_4_storm_drain_cutscene_status = 0
int trigger_storm_drain_cutscene_status = 0
int run_after_lazlow_dialogue_system_status = 0
int lazlow_ai_system_2_status = 0
int groupie_ai_system_status = 0
int truck_driver_ai_system_2_status = 0
int family_4_record_lazlow_status = 0
int i_triggered_text_hashes[10]
int tracey_ai_system_status = 0
//int lazlow_cam_time = 0
int dialogue_time = 0
int fail_time = 0
int detach_trailer_help_text_system_status = 0
int get_to_stadium_audio_scene_system_status = 0
int run_after_lazlow_audio_scene_system_status = 0
int uber_chase_audio_system_status = 0
int truck_driver_ai_system_status = 0
int lazlow_horn_time = 0
int run_after_lazlow_walla_system_status = 0
int storm_drain_lane = 0
int family_4_trevor_leadin_status = 0
int ambient_ped_flee_time = 0
int create_entities_outside_stadium_status = 0
int detach_trailer_time = 0
int create_vehicles_outside_stadium_status = 0
int run_after_lazlow_switch_effect_status = 0
int lazlow_cam_dialogue_counter = 0

//int camera_crew_ai_status[2]

float current_minimum_distance
float current_ideal_distance 
float current_slow_down_distance 
float multiplier_speed 
float target_speed
float disired_playback_speed = 1.0

vector scene_pos
vector scene_rot 

//reslove pos outside michaels houes
vector car_pos_outside_house = <<-868.3934, 152.2284, 62.5499>>
float car_heading_outside_house = 174.5712

#IF IS_DEBUG_BUILD
	int get_to_the_stadium_skip_status = 0
	int p_skip_time
	int run_after_lazlo_skip_status = 0
	int set_piece_recording_system_status = 0
	int get_to_the_audition_room_skip_status = 0
#endif 

//*****not present in initialise_mission_variables()
int launch_mission_stage_menu_status = 0
bool replay_active = false

string mission_failed_text

blip_index michaels_blip

ped_index set_piece_ped 

camera_index lazlow_cam
camera_index lazlow_cam_2
camera_index camera_a
camera_index camera_b
camera_index camera_c
camera_index camera_d
camera_index camera_e
camera_index camera_f
camera_index camera_g
camera_index camera_h
camera_index spline_cam

vehicle_index temp_lazlows_car
vehicle_index ambient_train[4]
vehicle_index ambient_car
vehicle_index broken_down_cars[2]
vehicle_index ambient_car_2
vehicle_index ambient_car_3
vehicle_index ambient_car_4
vehicle_index ambient_car_5
vehicle_index cutscene_car[2]
vehicle_index set_piece_veh
//vehicle_index players_last_vehicle
vehicle_index players_vehicle
vehicle_index players_last_vehicle

structPedsForConversation scripted_speech

CHASE_HINT_CAM_STRUCT chase_hint_cam

LOCATES_HEADER_DATA locates_data

interior_instance_index stadium_interior
interior_instance_index michaels_house_interior

sequence_index seq

ptfx_id lazlows_car_ptfx
//ptfx_id truck_ptfx

//timelapse variables
structTimelapse sTimelapse

text_label_23 dialogue_root
text_label_23 specific_label

#IF IS_DEBUG_BUILD
widget_group_id family_4_widget_group
MissionStageMenuTextStruct menu_stage_selector[MAX_SKIP_MENU_LENGTH]
int menu_return_stage = 0
#endif 

//****************************************LAWRENCE SDK****************************************

PROC ENABLE_STADIUM_INTERIOR()

	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF IS_IPL_ACTIVE("SP1_10_fake_interior")
		REMOVE_IPL("SP1_10_fake_interior")
	ENDIF
	
	IF NOT IS_IPL_ACTIVE("SP1_10_real_interior")
		REQUEST_IPL("SP1_10_real_interior")
	ENDIF

ENDPROC

proc disable_dispatch_services()
		
	enable_dispatch_service(dt_fire_department, false)
	enable_dispatch_service(dt_police_automobile, false)
	enable_dispatch_service(dt_police_helicopter, false)
	enable_dispatch_service(dt_ambulance_department, false)
	
endproc 

proc enable_dispatch_services()

	enable_dispatch_service(dt_fire_department, true)
	enable_dispatch_service(dt_police_automobile, true)
	enable_dispatch_service(dt_police_helicopter, true)
	enable_dispatch_service(dt_ambulance_department, true)

endproc 

func bool start_new_cutscene_no_fade(bool clear_players_tasks = true, bool hide_weapon_for_cutscene = true, bool kill_conversation_line_immediately = true, bool ignore_can_player_start_cutscene = false)

	if can_player_start_cutscene() or ignore_can_player_start_cutscene
	
		SPECIAL_ABILITY_DEACTIVATE(player_id())
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)

		clear_help()
		clear_prints()
		
		if kill_conversation_line_immediately
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
		else 
			kill_any_conversation()
		endif 
		
		display_hud(false)
		display_radar(false)
		set_widescreen_borders(true, 500)
		
		if clear_players_tasks
			set_player_control(player_id(), false, spc_clear_tasks)
		else 
			set_player_control(player_id(), false)
		endif 

		if hide_weapon_for_cutscene
			hide_ped_weapon_for_scripted_cutscene(player_ped_id(), true)
		else 
			hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
		endif 
		
		disable_dispatch_services()
		
		return true
	endif 
		
	return false

endfunc


proc end_cutscene(bool clear_tasks = true, float interp_heading = 0.0, float interp_pitch = 0.0, bool enable_emergency_services = true)

	//INFORM_MISSION_STATS_SYSTEM_OF_INengine_CUTSCENE_END()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)

	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	
	if is_player_playing(player_id())
	
		destroy_all_cams()
		
		render_script_cams(false, false)
		set_gameplay_cam_relative_heading(interp_heading)
		set_gameplay_cam_relative_pitch(interp_pitch)
			
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		
		set_player_control(player_id(), true)
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
		
	endif 

	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 
	
	do_screen_fade_in(DEFAULT_FADE_TIME)
	
endproc 

proc end_cutscene_no_fade(bool clear_tasks = true, bool update_game_camera = true, bool interpolate_behind_player = false, float interp_heading = 0.0, float interp_pitch = 0.0, int interp_to_game_time = 3000, bool enable_emergency_services = true)

	INFORM_MISSION_STATS_SYSTEM_OF_INengine_CUTSCENE_END()
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
	
	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	if is_player_playing(player_id())
		
		destroy_all_cams()
		
		if update_game_camera
			if interpolate_behind_player
				render_script_cams(false, true, interp_to_game_time)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			else 
				render_script_cams(false, false)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			endif 
		endif 
		
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		
		enable_special_ability(player_id(), true)
		
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
		
		set_player_control(player_id(), true)
	endif 
	
	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 

endproc 

func bool lk_timer(int &start_time, int time) 
	
	int current_time 
	current_time = get_game_timer()

	if ((current_time - start_time) > time) 
		return true	
	endif 
	
	return false
endfunc

func bool is_skip_button_pressed()
	
	return is_control_just_pressed(frontend_control, input_frontend_accept)
	
endfunc 

func bool skip_scripted_cut(int &original_time_temp, int cutscene_skip_time)
	if lk_timer(original_time_temp, cutscene_skip_time)
		if is_screen_faded_in()
			if IS_CUTSCENE_SKIP_BUTTON_PRESSED()	
				return true 
			endif 
		endif 
	endif 
	
	return false 
endfunc 

func bool mission_ped_created_and_injured(bool &mission_ped_created, ped_index &mission_ped)

	if mission_ped_created
		return is_ped_injured(mission_ped)
	endif 
	
	return false 

endfunc 

func bool mission_ped_injured(ped_index &this_ped)

	if DOES_ENTITY_EXIST(this_ped)
		return is_ped_injured(this_ped)
	endif 
	
	return false
	
endfunc 

func bool mission_vehicle_injured(vehicle_index &this_vehicle)
	
	if DOES_ENTITY_EXIST(this_vehicle)
		if not is_vehicle_driveable(this_vehicle)
			return true
		endif 
	endif 
	
	return false
	
endfunc 

func bool is_mission_entity_attacked(ped_index &mission_ped, bool clear_damage_entity = false)
	
	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_ped, PLAYER_ped_ID())
				if clear_damage_entity
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mission_ped)
				endif 
				return true
			endif 
		else 
			return true
		endif
	endif 
	
	return false
endfunc 

func bool has_char_task_finished_2(ped_index ped, script_task_name taskname)
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if status = finished_task 
	or status = dormant_task
		return true
	endif
	
	return false
endfunc

//gets the currenent car the player is in
func bool get_current_player_vehicle(vehicle_index &test_car)
	
	if is_ped_sitting_in_any_vehicle(player_ped_id())
		test_car = get_players_last_vehicle()
		if DOES_ENTITY_EXIST(test_car)				
			if is_vehicle_driveable(test_car)
				return true
			endif 
		endif 
	endif 
	
	return false
endfunc

func bool is_ped_in_same_vehicle_as_player(ped_index &mission_ped_0)
	
	vehicle_index mission_vehicle
	
	if get_current_player_vehicle(mission_vehicle)
		if not is_ped_injured(mission_ped_0)
			if is_ped_sitting_in_vehicle(mission_ped_0, mission_vehicle)
				return true
			endif 
		endif 
	endif 
	
	return false
	
endfunc 

PROC STOP_PLAYER_vehicle()

	VEHICLE_INDEX player_car
	
	if get_current_player_vehicle(player_car)
		
		FLOAT player_car_speed
			
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		SET_ENTITY_PROOFS(player_car, true, true, true, true, true)

		player_car_speed = GET_ENTITY_SPEED(player_car)
			 
		WHILE player_car_speed > 0.2
			WAIT(0)
			if not IS_ENTITY_DEAD(player_car)
				player_car_speed = GET_ENTITY_SPEED(player_car)
			endif 
		ENDWHILE

		SET_ENTITY_PROOFS(player_car, false, false, false, false, false)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	ENDIF
ENDPROC

func bool add_ped_to_players_group(ped_index &this_ped, group_index &players_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if not is_ped_injured(this_ped)
	
			players_group = get_player_group(player_id())
			
			set_ped_as_group_member(this_ped, players_group)
			
			return true
		endif 
	endif 

	return false
	
endfunc 

func float distance_from_player_to_ped(ped_index &mission_ped)

	vector player_pos
	vector mission_ped_pos 

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			
			player_pos = GET_ENTITY_COORDS(player_ped_id()) 
			mission_ped_pos = GET_ENTITY_COORDS(mission_ped)
			
			return get_distance_between_coords(player_pos, mission_ped_pos)
		endif 
	endif 
	
	return -1.0

endfunc 

func bool has_ped_task_finished_2(ped_index ped, script_task_name taskname = script_task_perform_sequence, bool use_seq = true, int sequence_progress = -2)
	
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if use_seq
	
		if status = finished_task 
		or status = dormant_task
		or get_sequence_progress(ped) = sequence_progress
			return true
		endif
		
	else 
	
		if status = finished_task 
		or status = dormant_task
			return true
		endif
	
	endif 
	
	return false
endfunc

func bool is_ped_playing_anim_at_phase(string anim_dict_name, string anim_name, float phase_target)
		
	if IS_ENTITY_PLAYING_ANIM(player_ped_id(), anim_dict_name, anim_name) 
		if GET_ENTITY_ANIM_CURRENT_TIME(player_ped_id(), anim_dict_name, anim_name) > phase_target 
			return true 
		endif 
	endif 
	
	return false 
	
endfunc

//The players group does not include the player himself. RELGROUPHASH_PLAYER is the player
REL_GROUP_HASH player_group
REL_GROUP_HASH enemy_group
rel_group_hash gropie_group

    
proc add_relationship_groups()

	ADD_RELATIONSHIP_GROUP("players group", player_group)
	ADD_RELATIONSHIP_GROUP("enemy group", enemy_group)
	ADD_RELATIONSHIP_GROUP("gropie group", gropie_group)
	
endproc 

proc setup_relationship_contact(ped_index &this_ped, bool block_temporary_events = false)

	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
			SET_PED_RELATIONSHIP_GROUP_HASH(this_ped, player_group) 

			SET_PED_TARGET_LOSS_RESPONSE(this_ped, tlr_never_lose_target)
			
			set_entity_is_target_priority(this_ped, false)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
			
		endif 
	endif 

endproc

proc setup_relationship_enemy(ped_index &this_ped, bool block_temporary_events = false)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
			
			SET_ped_RELATIONSHIP_GROUP_hash(this_ped, enemy_group) 
			
			set_ped_combat_attributes(this_ped, ca_will_scan_for_dead_peds, false)
			SET_PED_TARGET_LOSS_RESPONSE(this_ped, tlr_never_lose_target)
			
			set_entity_is_target_priority(this_ped, true)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
		endif 
	endif 
	
endproc

proc setup_buddy_attributes(ped_index mission_ped)
	
	set_ped_dies_when_injured(mission_ped, false)
	set_ped_can_be_targetted(mission_ped, false)
	set_ped_suffers_critical_hits(mission_ped, false)
	set_ped_can_evasive_dive(mission_ped, false)
	set_entity_is_target_priority(mission_ped, false)
	set_ped_keep_task(mission_ped, true)
	//set_ped_can_ragdoll(mission_ped, false)
	
	SET_RAGDOLL_BLOCKING_FLAGS(mission_ped, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE) //buddy won't ragdoll on bullet impact player impact and melee
	
	SET_PED_CONFIG_FLAG(mission_ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(mission_ped, PCF_DisableExplosionReactions, true)
	SET_PED_CONFIG_FLAG(mission_ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(mission_ped, PCF_DisableHurt, true)
	
	set_ped_seeing_range(mission_ped, 250.00)
	set_ped_hearing_range(mission_ped, 250.00)
	set_ped_id_range(mission_ped, 250.00)

endproc 

proc setup_enemy(ped_structure &enemy)

	clear_area(enemy.pos, 2.0, true)
		
	enemy.ped = create_ped(pedtype_mission, enemy.model, enemy.pos, enemy.heading)
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	enemy.created = true 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_seeing_range(enemy.ped, 250.00)
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_id_range(enemy.ped, 250.00)
	
	enemy.blip = ADD_BLIP_FOR_ENTITY(enemy.ped)
	set_blip_display(enemy.blip, DISPLAY_BLIP) 
	
	set_ped_can_evasive_dive(enemy.ped, true)
	
	set_blocking_of_non_temporary_events(enemy.ped, true)

endproc

proc setup_buddy(ped_structure &mission_buddy)

	clear_area(mission_buddy.pos, 4.0, true)

	mission_buddy.ped = create_ped(pedtype_mission, mission_buddy.model, mission_buddy.pos, mission_buddy.heading)
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, false)
	set_ped_can_evasive_dive(mission_buddy.ped, false)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_keep_task(mission_buddy.ped, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_WillFlyThroughWindscreen, false) 
	set_ped_can_ragdoll(mission_buddy.ped, false)
	
	give_weapon_to_ped(mission_buddy.ped, mission_buddy.weapon, infinite_ammo, false)
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)
	
	setup_relationship_contact(mission_buddy.ped, true)

endproc 


proc setup_enemy_in_vehicle(ped_structure &enemy, vehicle_index &mission_veh, vehicle_seat veh_seat = vs_driver)

	clear_area(enemy.pos, 2.0, true)
		
	enemy.ped = create_ped_inside_vehicle(mission_veh, pedtype_mission, enemy.model, veh_seat)
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	enemy.created = true 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_seeing_range(enemy.ped, 250.00)
	
	enemy.blip = ADD_BLIP_FOR_ENTITY(enemy.ped)
	set_blip_display(enemy.blip, DISPLAY_BLIP) 
	
	set_ped_can_evasive_dive(enemy.ped, true)
	
	set_blocking_of_non_temporary_events(enemy.ped, true)

endproc

func bool ped_structure_are_all_enemies_dead(ped_structure &enemy_array[], bool set_ped_no_longer_needed = true)
	
	int i = 0
	int peds_dead
	
	for i = 0 to (count_of(enemy_array) - 1)
		
		if DOES_ENTITY_EXIST(enemy_array[i].ped)
			if is_ped_injured(enemy_array[i].ped)
				
				if set_ped_no_longer_needed
					SET_PED_AS_NO_LONGER_NEEDED(enemy_array[i].ped) 
				endif 
				
				if does_blip_exist(enemy_array[i].blip)
					REMOVE_BLIP(enemy_array[i].blip)
				endif 
				
				peds_dead ++
			endif 
		
		else 
		
			if enemy_array[i].created
				peds_dead ++
			endif 
			
		endif 
		
	endfor 
	
	if peds_dead = count_of(enemy_array) 
		return TRUE
	endif 
	
	return FALSE
endfunc

//*****call this function underneath ped_structure_are_all_enemies_dead
func bool ped_structure_are_specific_number_enemies_dead(ped_structure &enemy_array[], int number_of_dead_peds)
	
	int i = 0
	int peds_dead = 0
	
	for i = 0 to count_of(enemy_array) - 1
		if enemy_array[i].created
			if is_ped_injured(enemy_array[i].ped)
				peds_dead++
			endif
		endif 
	endfor 
	
	if peds_dead >= number_of_dead_peds
		return TRUE
	endif 
	
	return FALSE
endfunc

func int ped_structure_get_total_number_of_enemies_dead(ped_structure &enemy_array[])

	int i = 0
	int peds_dead = 0
	
	for i = 0 to count_of(enemy_array) - 1
		if enemy_array[i].created
			if is_ped_injured(enemy_array[i].ped)
				peds_dead++
			endif
		endif 
	endfor 

	return peds_dead

endfunc 




func bool is_coord_in_area_2d(vector test_coord, vector top_left, vector bottom_right)
				
	vector tl
	vector br

	if top_left.x < bottom_right.x	
		tl.x = top_left.x
		br.x = bottom_right.x
	else
		tl.x = bottom_right.x
		br.x = top_left.x
	endif
	
	if top_left.y < bottom_right.y	
		tl.y = top_left.y
		br.y = bottom_right.y	
	else
		tl.y = bottom_right.y
		br.y = top_left.y
	endif

	if (test_coord.x > tl.x) and (test_coord.x < br.x)
		if (test_coord.y > tl.y) and (test_coord.y < br.y)
			return true
		endif 
	endif 

	return false 
endfunc

func bool has_ped_been_harmed(ped_index &mission_ped, int &original_health)

	int current_health
	
	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			
			current_health = GET_ENTITY_HEALTH(mission_ped)
			
			if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_ped , player_ped_id())		
			or (current_health < original_health) 
			or is_ped_responding_to_event(mission_ped, EVENT_DAMAGE)	
				
				return true
			
			endif 
			
		else 
		
			return true
		
		endif  
	
	endif 
	
	return false

endfunc 

func bool has_peds_been_harmed(ped_structure &ped_struct[])

	int i = 0
	
	for i = 0 to (count_of(ped_struct) - 1)
		
		if has_ped_been_harmed(ped_struct[i].ped, ped_struct[i].health)
			return true 
		endif 
		
	endfor 
	
	return false
	
endfunc 


func bool has_player_antagonised_ped(ped_index &mission_ped, float distance, bool distance_check_on = true)

	if not is_ped_injured(mission_ped)
		
		if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(mission_ped)) < distance
		
			//if has_char_spotted_char(this_ped, player_char_id())
			if CAN_PED_SEE_HATED_PED(mission_ped, PLAYER_PED_ID())
				
				IF IS_ped_ARMED(PLAYER_ped_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)
					
					if IS_PLAYER_TARGETTING_ENTITY(player_id(), mission_ped)
						return TRUE
					endif 
					
					if is_player_free_aiming_at_entity(player_id(), mission_ped)
						return TRUE
					endif
					
				endif 
			endif 
			
			if is_ped_shooting(player_ped_id())
				return true
			endif	
			
		endif
		
		if is_bullet_in_area(get_entity_coords(mission_ped), 4.0)
			return true
		endif 
		
		if distance_check_on
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(mission_ped)) < 1.5
				return true
			endif
		endif 

	endif 

	return FALSE
endfunc 


func bool has_vehicle_been_harmed(vehicle_index &mission_vehicle, int &original_car_health)

	int vehicle_health
	
	if DOES_ENTITY_EXIST(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)
			
			vehicle_health = GET_ENTITY_HEALTH(mission_vehicle)
			
			if get_vehicle_petrol_tank_health(mission_vehicle) < original_car_health
			or get_vehicle_engine_health(mission_vehicle) < original_car_health
			or /*HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED*/ HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_vehicle, player_ped_id())
			or vehicle_health < original_car_health
			
				return true
			
			endif 
		
		else 
		
			return true 
			
		endif 
	
	endif 
	
	return false
		
endfunc

func bool has_vehicles_been_harmed(vehicle_struct &veh_struct[])

	int i = 0
	
	for i = 0 to (count_of(veh_struct) - 1)
		
		if has_vehicle_been_harmed(veh_struct[i].veh, veh_struct[i].health)
			return true 
		endif 
		
	endfor 
	
	return false
endfunc

func bool is_ped_at_coords(ped_index &mission_ped, vector pos, vector locate_size)
	
	if not is_ped_injured(mission_ped)
		if IS_ENTITY_AT_COORD(mission_ped, pos, locate_size)
			return true 
		endif 
	endif 

	return false 

endfunc 

func quaternion build_quaternion_from_euler_angles(float fPitch, float fRoll, float fHeading)

	quaternion quat

    float fltPitch2 
    float fltYaw2   
    float fltRoll2  

    fltPitch2 = fPitch * 0.5
    fltYaw2   = fRoll * 0.5 
    fltRoll2  = fHeading * 0.5

    float cosY2 = Cos(fltYaw2)
    float sinY2 = Sin(fltYaw2)
    float cosP2 = Cos(fltPitch2)
    float sinP2 = Sin(fltPitch2)
    float cosR2 = Cos(fltRoll2)
    float sinR2 = Sin(fltRoll2)

    Quat.x = cosR2 * sinP2 * cosY2 + sinR2 * cosP2 * sinY2
    Quat.y = cosR2 * cosP2 * sinY2 - sinR2 * sinP2 * cosY2
    Quat.y *= -1.0
    Quat.z = sinR2 * cosP2 * cosY2 - cosR2 * sinP2 * sinY2
    Quat.w = cosR2 * cosP2 * cosY2 + sinR2 * sinP2 * sinY2
	
	return quat

endfunc 

func bool are_vectors_alomost_equal(vector vector_a, vector vector_b, vector unit_vector_x_multiplier)

	//vec b is the target vector
	//vec a is the current positon of the object
	//unit_vector_x_multiplier is the unitvector * frame time * multiplier
	
	vector vector_ba
	vector future_vector

	vector_ba = vector_b - vector_a
	
	if (vmag(vector_ba) < 0.2)
		return true
	endif
	
	future_vector = (vector_b - (vector_a + unit_vector_x_multiplier))
	if (vmag(future_vector) > vmag(vector_ba))
		return true
	endif 
	
	return false

endfunc 

func bool is_ped_in_front_of_ped(ped_index mission_ped_0, ped_index mission_ped_1)

	if DOES_ENTITY_EXIST(mission_ped_0)
		if DOES_ENTITY_EXIST(mission_ped_1)
			if not is_ped_injured(mission_ped_0)
				if not is_ped_injured(mission_ped_1)
					
					vector pos_a
					vector pos_b
					vector vec_BA
					
					vector ped_1_forward_vector
	
					pos_a = GET_ENTITY_COORDS(mission_ped_0) 
					
					pos_b = GET_ENTITY_COORDS(mission_ped_1) 
				
					vec_BA = pos_b - pos_a
					
					ped_1_forward_vector = get_offset_from_entity_in_world_coords(mission_ped_1, <<0.0, 0.5, 0.0>>) - pos_b
					
					if dot_product(vec_BA, ped_1_forward_vector) < 0.0
						
						return (true)
					endif 
				endif 
			endif 
		endif 
	endif 
					
	return false
	
endfunc 

func bool is_ped_within_range_of_target_heading(ped_index miss_ped, float target_heading, float heading_range)
					
	float heading_dif
	
	heading_dif = (target_heading - get_entity_heading(miss_ped))
	
	if heading_dif > 180
		heading_dif -= 360
	endif 
	if heading_dif < -180
		heading_dif += 360
	endif 
	
	if absf(heading_dif) <= heading_range
		return true 
	endif 
	
	return false

endfunc

func bool is_vehicle_stuck_every_check(vehicle_index &vehicle)
	
	if DOES_ENTITY_EXIST(vehicle)

		if is_vehicle_driveable(vehicle)
			
			if is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_ON_ROOF, ROOF_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_JAMMED, JAMMED_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_ON_SIDE, SIDE_TIME)
			
				return true
			endif 
		endif 
	endif 
	
	return false
endfunc 

func bool is_ped_inside_interior(ped_index mission_ped, vector interior_pos)

	interior_instance_index interior
	interior_instance_index mission_ped_interior

	interior = get_interior_at_coords(interior_pos)
	mission_ped_interior = GET_INTERIOR_FROM_ENTITY(mission_ped)
	
	if not (mission_ped_interior = null)
		if (mission_ped_interior = interior)
			return true 
		endif 
	endif 
	
	return false 

endfunc

func bool is_ped_inside_interior_with_name(ped_index mission_ped, vector interior_pos, string interior_name)

	interior_instance_index interior
	interior_instance_index mission_ped_interior
	
	interior = get_interior_at_coords_with_type(interior_pos, interior_name)
	mission_ped_interior = GET_INTERIOR_FROM_ENTITY(mission_ped)
	
	if not (mission_ped_interior = null)
		if (mission_ped_interior = interior)
			return true 
		endif 
	endif 
	
	return false 

endfunc 

func bool is_ped_in_specific_room(ped_index mission_ped, vector interior_pos, string room_name)

	interior_instance_index interior
	interior_instance_index ped_current_interior
	
	int ped_current_room_hash_key
	int room_hash_key
	
	if not is_ped_injured(mission_ped)

		interior = get_interior_at_coords(interior_pos)	
		ped_current_interior = GET_INTERIOR_FROM_ENTITY(mission_ped)
		
		if not (interior = null)
			if (interior = ped_current_interior)
				
				ped_current_room_hash_key = GET_KEY_FOR_ENTITY_IN_ROOM(mission_ped)
				room_hash_key = get_hash_key(room_name)
		
				if (ped_current_room_hash_key != 0)
					if ped_current_room_hash_key = room_hash_key
						return true 
					endif 
				endif 
			endif 
		endif 
		
	endif 
	
	return false 
	
endfunc 

proc equip_player_strongest_weapon()

	if has_ped_got_weapon (player_ped_id(), WEAPONTYPE_ASSAULTRIFLE)
		set_current_ped_weapon(player_ped_id(), WEAPONTYPE_ASSAULTRIFLE,true)
	elif has_ped_got_weapon (player_ped_id(), weapontype_smg)
		set_current_ped_weapon(player_ped_id(), weapontype_smg,true)
	elif has_ped_got_weapon (player_ped_id(), WEAPONTYPE_PUMPSHOTGUN)
		set_current_ped_weapon(player_ped_id(), WEAPONTYPE_PUMPSHOTGUN,true)
	elif has_ped_got_weapon (player_ped_id(), weapontype_pistol)
		set_current_ped_weapon(player_ped_id(), weapontype_pistol,true)
	endif

endproc 


func bool activate_vehicle_tracking_cam(vehicle_index &mission_car)
	// function check if player is pressing the button to access the camera from cops pov
	// only lets you do so if certan other requirments are met

	interior_instance_index current_interior
	
//	if does_blip_exist(lazlow.blip) //player inside specific vehicle - using locates header check for mission entitites during chase.
//		if is_control_just_pressed(player_control, input_veh_cin_cam)
//			if not lazlow_chase_cam_active
//				lazlow_chase_cam_active = true
//			else 
//				lazlow_chase_cam_active = false
//			endif 
//		endif 
//	endif 
//	
//	if is_control_pressed(player_control, input_veh_cin_cam)
//		if lazlow_cam_time = 0
//			lazlow_cam_time = get_game_timer()
//		endif 
//	else 
//		
//		if not (lazlow_cam_time = 0)
//			if lk_timer(lazlow_cam_time, 1500)
//				lazlow_chase_cam_active = false
//			endif 
//		endif 
//		
//		lazlow_cam_time = 0
//		
//	endif 
	
	if does_blip_exist(lazlow.blip)
		if SHOULD_CONTROL_CHASE_HINT_CAM(chase_hint_cam,true,false,true)
			lazlow_chase_cam_active = true
		else 
			lazlow_chase_cam_active = false
		endif 
	endif 
	
//	if IS_VEHICLE_ON_ALL_WHEELS
//		vehicle_on_all_wheels_time = get_gamet_timer()
//	endif 
	
	
	
	
	if lazlow_chase_cam_active
		if is_vehicle_driveable(mission_car)
			if does_blip_exist(lazlow.blip) //synched up the locates function.
			and is_playback_going_on_for_vehicle(mission_car)
				if not is_phone_onscreen()

					current_interior = GET_INTERIOR_FROM_ENTITY(player_ped_id())
					
					//tunnel on the route. Every other interior is banned. 
					if (current_interior = null)	
					or is_ped_inside_interior_with_name(player_ped_id(), <<249.3368, -622.8621, 29.6647>>, "dt1_rd1_tun") 
					or is_ped_inside_interior_with_name(player_ped_id(), <<132.835007,-577.426636,33.010033>>, "dt1_rd1_tun2")
					or is_ped_inside_interior_with_name(player_ped_id(), <<287.4,-641.1,30.510033>>, "dt1_rd1_tun3")
					
//						if is_vehicle_on_all_wheels()
//						or (not is_vehicle_on_all_wheels()) and (not lk_timer(vehicle_on_all_wheels_time, 5000))
						
						vector truck_rot
						
						truck_rot = get_entity_rotation(truck.veh)
						
						printstring("truck_rot.x: ")
						printfloat(absf(truck_rot.x))
						printnl()
						
						printstring("truck_rot.y: ")
						printfloat(absf(truck_rot.y))
						printnl()
						
						if absf(truck_rot.x) < 45.0 
						and absf(truck_rot.y) < 45.0
						
							return true 
							
						endif 
						
					endif 
	
				endif
			endif 
		endif
	
	endif
	
	return false
	
endfunc

func bool allow_dialogue_to_continue(ped_index &mission_ped, int instruct_text_time, bool allow_wanted_level_check = false)

	if not is_ped_injured(mission_ped)
		if is_ped_in_group(mission_ped)
			
			if allow_wanted_level_check
				if is_player_wanted_level_greater(player_id(), 0) 
					instruct_text_time = get_game_timer()
					return false
				endif 
				
				//if you get a wanted level and the god text renders. If you then lose the wanted level and get
				//instruction text this ensures the instruction text is rendered for 3 seconds before the 
				//dialogue is unpaused
				if not lk_timer(instruct_text_time, 3000) 
					return false
				endif 
			endif 
		
			if (is_ped_on_foot(mission_ped) and is_ped_on_foot(player_ped_id()))
				return true 
			endif 
			

			if (is_ped_sitting_in_any_vehicle(mission_ped) and is_ped_sitting_in_any_vehicle(player_ped_id()))
				return true 
			endif 
			
		endif 
	
	endif 
	
	return false
endfunc 

FUNC INT GET_TRAFFIC_VEHICLE_SEARCH_FLAGS()
	
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED)
ENDFUNC


proc repostion_players_last_vehicle(vehicle_index &mission_veh, vector area_pos_check, vector area_dimensions, vector veh_pos, float veh_heading)
	
	mission_veh = get_players_last_vehicle()

	if does_entity_exist(mission_veh)
		if is_vehicle_driveable(mission_veh)
			if not is_big_vehicle(mission_veh)
				if not (get_entity_model(mission_veh) = taxi)
					if is_entity_at_coord(mission_veh, area_pos_check, area_dimensions)
				
						if not is_entity_a_mission_entity(mission_veh)
							set_entity_as_mission_entity(mission_veh)
						else 
							if not DOES_ENTITY_BELONG_TO_THIS_SCRIPT(mission_veh, false)
								set_entity_as_mission_entity(mission_veh, true, true)
							endif 
						endif 
						
						if not is_vehicle_in_players_garage(mission_veh, get_current_player_ped_enum(), false)
						
							clear_area(veh_pos, 10.0, true)
							set_entity_coords(mission_veh, veh_pos)  
							set_entity_heading(mission_veh, veh_heading)
							set_vehicle_on_ground_properly(mission_veh)
							
						endif 
					endif 
				endif 
			endif 
		endif
	endif 
	
endproc 

proc kill_ped_on_certain_damage(ped_structure &miss_ped)

	if not is_ped_injured(miss_ped.ped)

		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(miss_ped.ped, WEAPONTYPE_GRENADE)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(miss_ped.ped, WEAPONTYPE_GRENADELAUNCHER)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(miss_ped.ped, WEAPONTYPE_STICKYBOMB)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(miss_ped.ped, WEAPONTYPE_RPG)
		
			if is_synchronized_scene_running(miss_ped.scene_id)
				stop_synchronized_entity_anim(miss_ped.ped, normal_blend_out, true)
			endif 
			
			set_ped_can_ragdoll(miss_ped.ped, true)
			
			set_entity_health(miss_ped.ped, 1)
		endif 
	endif 
		
endproc 

/// PURPOSE: repositions the players last vehicle if it exists via calling GET_PLAYERS_LAST_VEHICLE()
//		     and sets the vehicle as a mission entity so that clear area will not clear it. Remember to set the 
//			 vehicle as no longer needed in the mocap cleanup

// sets the vehicle as a vehicle gen. The vehicle gen controller sets the vehicle as a mission entity bellonging 
//to it. If the player walks 200m away from the car and the vehicle gen position passed in. It will reposition  
//the car to those coords.

proc set_players_last_vehicle_to_vehicle_gen(vector car_pos, float car_heading)

	vehicle_index players_last_car 
	
	players_last_car = GET_PLAYERS_LAST_VEHICLE()
	
	if does_entity_exist(players_last_car)
		if is_vehicle_driveable(players_last_car)
		
			SET_MISSION_VEHICLE_GEN_VEHICLE(players_last_car, car_pos, car_heading) 
			
		endif 
	endif 
	
endproc  

func bool is_players_last_vehicle_present_and_acceptable(vehicle_index player_vehicle, vector pos, int number_of_seats, vector secondary_pos, float secondary_heading, bool allow_helicopter = true)
	
	model_names player_vehicle_model 
	
	if does_entity_exist(player_vehicle)
		
		if is_vehicle_driveable(player_vehicle)
		
			if is_entity_at_coord(player_vehicle, pos, <<10.0, 10.0, 10.0>>, false)

				player_vehicle_model = get_entity_model(player_vehicle)

				if is_this_model_a_heli(player_vehicle_model)
				
					if allow_helicopter

						if get_vehicle_max_number_of_passengers(player_vehicle) >= number_of_seats
				
							return true 
							
						else 

							clear_area(secondary_pos, 10.0, true)
							set_entity_coords(player_vehicle, secondary_pos)
							set_entity_heading(player_vehicle, secondary_heading)
							set_vehicle_on_ground_properly(player_vehicle)
							//set_vehicle_as_no_longer_needed(player_vehicle)
							set_players_last_vehicle_to_vehicle_gen(secondary_pos, secondary_heading)

						endif
	
					endif
					
				else 
				
					if get_vehicle_max_number_of_passengers(player_vehicle) >= number_of_seats
			
						return true 
						
					else 
					
						//delete_vehicle(player_vehicle)
			
						clear_area(secondary_pos, 10.0, true)
						set_entity_coords(player_vehicle, secondary_pos)
						set_entity_heading(player_vehicle, secondary_heading)
						set_vehicle_on_ground_properly(player_vehicle)
						//set_vehicle_as_no_longer_needed(player_vehicle)
						set_players_last_vehicle_to_vehicle_gen(secondary_pos, secondary_heading)

					endif
				
				endif 
				
			endif 
		endif 
	endif 
	
	return false
	
endfunc 

proc clear_players_task_on_control_input(script_task_name script_task = script_task_perform_sequence)//script_task_name task_name)

	if get_script_task_status(player_ped_id(), script_task) = performing_task
				
		int left_stick_x
		int left_stick_y
		int right_stick_x
		int right_stick_y
		int stick_dead_zone = 28
	
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)

		IF NOT IS_LOOK_INVERTED()
			right_stick_y *= -1
		ENDIF
		
		// invert the vertical
		IF (left_stick_y > STICK_DEAD_ZONE)
		OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
		or (left_stick_x > STICK_DEAD_ZONE)
		OR (left_stick_x < (STICK_DEAD_ZONE * -1))
		or is_control_pressed(player_control, input_sprint) 
		OR is_control_pressed(player_control, INPUT_VEH_EXIT)
		or is_control_pressed(player_control, INPUT_JUMP)
		or is_control_pressed(player_control, INPUT_ENTER)
		or is_control_pressed(player_control, INPUT_ATTACK)
		
			clear_ped_tasks(player_ped_id())
			
		endif 
		
	endif 

endproc 

//proc clear_players_task_on_control_input(script_task_name task_name)
//
//	if get_script_task_status(player_ped_id(), task_name) = performing_task
//			
//		int left_stick_x
//		int left_stick_y
//		int right_stick_x
//		int right_stick_y
//		int stick_dead_zone = 28
//	
//		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)
//
//		IF NOT IS_LOOK_INVERTED()
//			right_stick_y *= -1
//		ENDIF
//		
//		// invert the vertical
//		IF (left_stick_y > STICK_DEAD_ZONE)
//		OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
//		or (left_stick_x > STICK_DEAD_ZONE)
//		OR (left_stick_x < (STICK_DEAD_ZONE * -1))
//		//or is_control_pressed(player_control, input_sprint) 
//		
//			clear_ped_tasks(player_ped_id())
//			
//		endif 
//		
//	endif 
//
//endproc 

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC


/// PURPOSE:
///    Sorts the triggered label array such that all the empty elements are at the end.
PROC REMOVE_LABEL_ARRAY_SPACES()
    INT k = 0
 	REPEAT (COUNT_OF(i_triggered_text_hashes) - 1) k
	    IF i_triggered_text_hashes[k] = 0
	    	IF i_triggered_text_hashes[k+1] != 0
	        	i_triggered_text_hashes[k] = i_triggered_text_hashes[k+1]
	            i_triggered_text_hashes[k+1] = 0
	        ENDIF
	    ENDIF
    ENDREPEAT
ENDPROC

/// PURPOSE:
///    Finds the array index of a particular triggered label, or -1 if the label hasn't been added.
FUNC INT GET_LABEL_INDEX(INT i_label_hash)
    INT k = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) k
		IF i_triggered_text_hashes[k] = 0 //We've reached the end of the filled section of the array, no need to continue.
        	RETURN -1
        ELIF i_triggered_text_hashes[k] = i_label_hash
        	RETURN k
        ENDIF
    ENDREPEAT
      
    RETURN -1
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the given label has been triggered.  
FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING str_label)
	RETURN (GET_LABEL_INDEX(GET_HASH_KEY(str_label)) != -1)
ENDFUNC

/// PURPOSE:
///    Adds/removes a text label to/from the list of labels that have been triggered.
PROC SET_LABEL_AS_TRIGGERED(STRING str_label, BOOL b_trigger)
	
	INT i_hash = GET_HASH_KEY(str_label)
	INT k = 0

	IF b_trigger
	    BOOL b_added = FALSE
	    
	    WHILE NOT b_added AND k < COUNT_OF(i_triggered_text_hashes)
	    	
			IF i_triggered_text_hashes[k] = i_hash //The label is already in the array, don't add it again.
	        	b_added = TRUE 
	        ELIF i_triggered_text_hashes[k] = 0
	        	i_triggered_text_hashes[k] = i_hash
	            b_added = TRUE
	        ENDIF
	          
	        k++
	    ENDWHILE

	    #IF IS_DEBUG_BUILD
	    	IF NOT b_added
	        	SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Failed to add label, array is full.")
	        ENDIF
	    #ENDIF
	ELSE
	    
		INT i_index = GET_LABEL_INDEX(i_hash)
	    IF i_index != -1
			i_triggered_text_hashes[i_index] = 0
	        REMOVE_LABEL_ARRAY_SPACES()
	    ENDIF
		
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT k = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) k
   		i_triggered_text_hashes[k] = 0
	ENDREPEAT
ENDPROC


func bool is_player_interacting_with_controller()//is_direction_stick_pushed()

	int left_stick_x
	int left_stick_y
	int right_stick_x
	int right_stick_y
	int stick_dead_zone = 28//127

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)

	IF NOT IS_LOOK_INVERTED()
		right_stick_y *= -1
	ENDIF

	IF (left_stick_y > (STICK_DEAD_ZONE + 28))
	OR (left_stick_y < (STICK_DEAD_ZONE - 28))	
	or is_control_pressed(player_control, input_sprint) 
	or is_control_pressed(player_control, input_jump)
	OR is_control_pressed(player_control, INPUT_VEH_EXIT)
	or is_control_pressed(player_control, input_reload)
	or is_control_pressed(player_control, INPUT_MELEE_ATTACK_LIGHT)
	//or is_control_pressed(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	or is_control_pressed(player_control, INPUT_MELEE_ATTACK_HEAVY)
	or is_control_pressed(player_control, input_aim)
	or is_control_pressed(player_control, INPUT_VEH_ACCELERATE)

		return true
	
	endif 
	
	return false
	
endfunc 

func bool is_players_last_car_present()

	vehicle_index players_last_veh = get_players_last_vehicle()
	
	printstring("last car 0")
	printnl()

	if does_entity_exist(players_last_veh)
	
		printstring("last car 1")
		printnl()
		
		if is_vehicle_driveable(players_last_veh)

			printstring("last car 2")
			printnl()
			
			//if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(players_last_veh, false)) < 150.00
			
				IF IS_VEHICLE_MODEL(players_last_veh, TAXI)
					IF GET_PED_IN_VEHICLE_SEAT(players_last_veh, VS_DRIVER) = player_ped_id()
					or GET_PED_IN_VEHICLE_SEAT(players_last_veh, VS_DRIVER) = NULL //no driver - player must have took the car and parked it
						
						TRACK_VEHICLE_FOR_IMPOUND(players_last_veh)//adds players car to inpound so when clear area is called it gets deleted then it gets stored there. 
						
						if not is_entity_a_mission_entity(players_last_veh)
							set_entity_as_mission_entity(players_last_veh, true, true)
						endif 
						delete_vehicle(players_last_veh)
						
						return true
						
					endif 
					
				else 
				
					TRACK_VEHICLE_FOR_IMPOUND(players_last_veh)//adds players car to inpound so when clear area is called it gets deleted then it gets stored there. 
					
					if not is_entity_a_mission_entity(players_last_veh)
						set_entity_as_mission_entity(players_last_veh, true, true)
					endif 
					delete_vehicle(players_last_veh)

					return true 
					
				endif 
			//endif 
		endif 
	
	else 
	
		printstring("last car 4")
		printnl()
	
	endif 
	
	return false

endfunc


proc setup_ped_proofs(ped_index &this_ped)
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)
			set_entity_proofs(this_ped, true, true, true, true, true)
		endif 
	endif 
	
endproc 

proc deactivate_ped_proofs(ped_index &this_ped)

	int ped_health
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)

			ped_health = get_entity_health(this_ped)

			if ped_health < 110
				set_entity_health(this_ped, 120)
			else 
				set_entity_health(this_ped, ped_health)
			endif 
			
			set_entity_proofs(this_ped, false, false, false, false, false)
		endif 
	endif 

endproc


proc setup_vehicle_proofs(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if is_vehicle_driveable(this_vehicle)
			
			if get_entity_health(this_vehicle) < 500
				set_entity_health(this_vehicle, 500)
			endif  
			
			if get_vehicle_engine_health(this_vehicle) < 500
				set_vehicle_engine_health(this_vehicle, 500.00)
			endif 
			
			if get_vehicle_petrol_tank_health(this_vehicle) < 500.00
				set_vehicle_petrol_tank_health(this_vehicle, 500.00)
			endif 
				
			set_entity_proofs(this_vehicle, true, true, true, true, true)
		endif 
	endif 
	
endproc

proc restore_vehicle_health(vehicle_index &mission_vehicle)
								
	if does_entity_exist(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)

			if get_entity_health(mission_vehicle) < 500
				set_entity_health(mission_vehicle, 500)
			endif  
			
			if get_vehicle_engine_health(mission_vehicle) < 500
				set_vehicle_engine_health(mission_vehicle, 500.00)
			endif 
			
			if get_vehicle_petrol_tank_health(mission_vehicle) < 500.00
				set_vehicle_petrol_tank_health(mission_vehicle, 500.00)
			endif 
			
		endif 
	endif 
	
endproc

proc deactivate_vehicle_proofs(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if is_vehicle_driveable(this_vehicle)
			set_entity_proofs(this_vehicle, false, false, false, false, false)
			restore_vehicle_health(this_vehicle)
		endif 
	endif 
	
endproc


proc setup_mission_ped_and_vehicle_proofs(ped_index &mission_ped, vehicle_index &mission_car)
									
	setup_ped_proofs(mission_ped) 
	
	setup_vehicle_proofs(mission_car)
	
endproc 


func bool HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(ped_index miss_ped)
	
	if does_entity_exist(miss_ped)
		if not is_ped_injured(miss_ped)
			if HAVE_ALL_STREAMING_REQUESTS_COMPLETED(miss_ped)
				return true 
			endif 
		endif 
	endif 
	
	return false 

endfunc 


//****************************************END OF LAWRENCE SDK****************************************


//**************************************** WIDGETS****************************************

#IF IS_DEBUG_BUILD

bool widget_reset_cutscene = false
bool widget_create_object = false
bool widget_modify = false
bool widget_entity_camera_active = false
bool widget_vehicle_colour_active = false
bool widget_reset_recording = false
bool widget_vehicle_schafter2
bool widget_vehicle_sentinel
bool widget_vehicle_manana
bool widget_vehicle_speedo
bool widget_vehicle_Mule
bool widget_add_scenario_area

int widget_colour_0 
int widget_colour_1

int widget_blip_alpha

float widget_temp_width
float widget_temp_height
float widget_temp_depth
float widget_angled_length
float widget_anim_speed = 1.0
float widget_heading
float widget_fov = 45.00
float widget_blip_scale
float widget_time_a
float widget_time_b

vector widget_offset_bl
vector widget_offset_br
vector widget_bl
vector widget_br
vector widget_object_pos
vector widget_object_rot
vector widget_object_pos_2
vector widget_object_rot_2
vector widget_cam_attach_offset
vector widget_cam_point_offset
vector widget_blip_pos


camera_index widget_camera

scenario_blocking_index widget_scenario_index

proc locate_widget()

	vector widget_player_pos
			
	widget_player_pos = GET_ENTITY_COORDS(player_ped_id())
	IS_ENTITY_AT_COORD(player_ped_id(), widget_player_pos, <<widget_temp_width, widget_temp_depth, widget_temp_height>>, false, true)		

endproc

proc angled_area_locate_widget()

	widget_bl = get_offset_from_entity_in_world_coords(player_ped_id(), widget_offset_bl) 
	widget_br = get_offset_from_entity_in_world_coords(player_ped_id(), widget_offset_br)

	IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), widget_bl, widget_br, widget_angled_length, false, TRUE)	
	
	if widget_offset_br.z > 0.0
		SET_ENTITY_HEADING(player_ped_id(), widget_heading)
	endif 

endproc

proc attach_object_to_ped_and_move(ped_index mission_ped, object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if DOES_ENTITY_EXIST(mission_object)
				ATTACH_ENTITY_TO_ENTITY(mission_object, mission_ped, bonetag_ph_r_hand, widget_object_pos, widget_object_rot)
			endif 
		endif 
	endif
	
endproc 

proc set_the_ped_anim_speed(ped_index mission_ped, string dict_name, string anim_name, float anim_speed)

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if IS_ENTITY_PLAYING_ANIM(mission_ped, dict_name, anim_name)
				SET_ENTITY_ANIM_SPEED(mission_ped, dict_name, anim_name, anim_speed)
			endif 
		endif 
	endif 
	
endproc

proc create_and_move_object(object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_object)
		SET_ENTITY_COORDS_NO_OFFSET(mission_object, widget_object_pos)
		SET_ENTITY_ROTATION(mission_object, widget_object_rot)  
	endif 
		
endproc 

proc create_and_move_object_2(object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_object)
		SET_ENTITY_COORDS_NO_OFFSET(mission_object, widget_object_pos_2)
		SET_ENTITY_ROTATION(mission_object, widget_object_rot_2)  
	endif 
		
endproc 

proc attach_object_to_object_and_move(object_index &parent_object, object_index &mission_object)

	if DOES_ENTITY_EXIST(parent_object)
		if DOES_ENTITY_EXIST(mission_object)
			ATTACH_ENTITY_TO_ENTITY(mission_object, parent_object, -1, widget_object_pos_2, widget_object_rot_2)
		endif 
	endif 
	
endproc 

proc create_and_move_pickup_widget(pickup_index &widget_mission_pickup, pickup_type pick_type, vector &pos, vector &rot)

	if widget_create_object
		if not widget_modify
			widget_mission_pickup = create_pickup_rotate(pick_type, pos, rot, true)
			
			widget_object_pos.x = pos.x 
			widget_object_pos.y = pos.y
			widget_object_pos.z = pos.z 
			widget_object_rot.x = rot.x
			widget_object_rot.y = rot.y
			widget_object_rot.z = rot.z
			
			widget_modify = true 
		else 
			
			if lk_timer(original_time, 500)
				if does_pickup_exist(widget_mission_pickup)
					remove_pickup(widget_mission_pickup)
				else
					widget_mission_pickup = create_pickup_rotate(pick_type, widget_object_pos, widget_object_rot, true)
				endif
				
				original_time = get_game_timer()
			endif 
		endif 
	endif 
	
endproc 

			
proc camera_attached_to_entity_widget(entity_index mission_entity)

	if widget_entity_camera_active
		if not does_cam_exist(widget_camera)

			widget_camera = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(widget_camera, mission_entity, widget_cam_attach_offset)
			POINT_CAM_AT_ENTITY(widget_camera, mission_entity, widget_cam_point_offset)

			set_cam_active(widget_camera, true)
								
			render_script_cams(true, false)
			
		else 
		
			ATTACH_CAM_TO_ENTITY(widget_camera, mission_entity, widget_cam_attach_offset)
			
			POINT_CAM_AT_ENTITY(widget_camera, mission_entity, widget_cam_point_offset)
			
			set_cam_fov(widget_camera, widget_fov)
			
		endif 

	endif 

endproc 

proc blip_pos_scale_and_alpha_widget(blip_index &mission_blip)

	if does_blip_exist(mission_blip)
	
		set_blip_coords(mission_blip, widget_blip_pos)
		set_blip_scale(mission_blip, widget_blip_scale)
		set_blip_alpha(mission_blip, widget_blip_alpha)
	
	else 
	
		mission_blip = add_blip_for_coord(widget_blip_pos)
		set_blip_as_friendly(mission_blip, true)
		
	endif 
		
endproc 

proc vehicle_colour_widget(vehicle_index mission_car)

	if widget_vehicle_colour_active
		if DOES_ENTITY_EXIST(mission_car)
			if is_vehicle_driveable(mission_car)
				set_vehicle_colours(mission_car, widget_colour_0, widget_colour_1)
			endif 
		endif 
	endif
	
endproc	

proc attach_entity_to_entity_widget(entity_index entity_0, entity_index entity_1, int bone)

	if does_entity_exist(entity_0)
		if does_entity_exist(entity_1)
		
			ATTACH_ENTITY_TO_ENTITY(entity_0, entity_1, bone, widget_object_pos, widget_object_rot, FALSE) 
			
		endif 
	endif 

endproc 

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &veh, FLOAT fTime)
	
	IF NOT IS_ENTITY_DEAD(veh)
	    IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
	          SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh, fTime - GET_TIME_POSITION_IN_RECORDING(veh))
	    ENDIF
	ENDIF
	
ENDPROC

proc add_scenario_blocking_area_widget()

	if widget_add_scenario_area

		remove_scenario_blocking_area(widget_scenario_index)

		widget_scenario_index = add_scenario_blocking_area(widget_offset_bl, widget_offset_br)
		
		widget_add_scenario_area = false
		
	endif 
		
endproc 

//**************************************************WIDGETS**************************************************
proc load_lk_widgets()

	family_4_widget_group = start_widget_group("family_4")
	
		start_widget_group("locate widget")
			add_widget_float_slider("width", widget_temp_width, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("depth", widget_temp_depth, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("height", widget_temp_height, -4000.0, 4000.0, 0.1)
		stop_widget_group()
		
		start_widget_group("angled_area")
			add_widget_float_slider("left offset x", widget_offset_bl.x, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("left offset y", widget_offset_bl.y, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("left offset z", widget_offset_bl.z, -4000.0, 4000.0, 0.1)
			
			add_widget_float_slider("right offset x", widget_offset_br.x, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("right offset y", widget_offset_br.y, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("right offset z", widget_offset_br.z, -4000.0, 4000.0, 0.1)
			
			add_widget_float_slider("angled area length", widget_angled_length, -4000.0, 4000.0, 0.1)
			
			add_widget_float_slider("heading", widget_heading, -4000.0, 4000.0, 0.1)
			
			add_widget_float_read_only("widget_bl x", widget_bl.x)
			add_widget_float_read_only("widget_bl y", widget_bl.y)
			add_widget_float_read_only("widget_bl z", widget_bl.z)
			
			add_widget_float_read_only("widget_br x", widget_br.x)
			add_widget_float_read_only("widget_br y", widget_br.y)
			add_widget_float_read_only("widget_br z", widget_br.z)
			
		stop_widget_group()
		
		start_widget_group("object attach to ped widget")
			add_widget_float_slider("pos offset x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos offset y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos offset z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot offset x", widget_object_rot.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot offset y", widget_object_rot.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot offset z", widget_object_rot.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		
		start_widget_group("anim speed widget")
			add_widget_float_slider("anim speed", widget_anim_speed, 0.1, 10.0, 0.01)
		stop_widget_group()
		
		
		start_widget_group("reset cutscene widget")
			add_widget_bool("reset cutscene", widget_reset_cutscene)
		stop_widget_group()
		
		
		start_widget_group("object pos and rot widget")
			add_widget_float_slider("pos x", widget_object_pos.x, -4000.0, 4000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos.y, -4000.0, 4000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos.z, -4000.0, 4000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot.x, -4000.0, 4000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot.y, -4000.0, 4000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot.z, -4000.0, 4000.0, 0.01)
		stop_widget_group()
		
		start_widget_group("object pos and rot widget 2")
			add_widget_float_slider("pos x", widget_object_pos_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos_2.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot_2.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		
		start_widget_group("attach object to object pos and rot widget")
			add_widget_float_slider("pos x", widget_object_pos_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos_2.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot_2.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		
		start_widget_group("create and move PICKUP inside interior")
			ADD_WIDGET_bool("create_object", widget_create_object)
			
			ADD_WIDGET_FLOAT_SLIDER("object pos x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("object pos y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("object pos z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("object rotation x", widget_object_rot.x, 0.0, 360.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("object rotation y", widget_object_rot.y, 0.0, 360.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("object rotation z", widget_object_rot.z, 0.0, 360.0, 0.1)
		stop_widget_group()
		
		start_widget_group("camaera attached to entity widget")
			add_widget_bool("create camera", widget_entity_camera_active)
			
			ADD_WIDGET_FLOAT_SLIDER("pos x", widget_cam_attach_offset.x, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos y", widget_cam_attach_offset.y, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos z", widget_cam_attach_offset.z, -3000.0, 3000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("point offset x", widget_cam_point_offset.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("point offset y", widget_cam_point_offset.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("point offset z", widget_cam_point_offset.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("FOV", widget_fov, -3000.0, 3000.0, 0.1)
		stop_widget_group()
		
		
		start_widget_group("blip position, scale and alpha widget")
			
			ADD_WIDGET_FLOAT_SLIDER("pos x", widget_blip_pos.x, -4000.0, 4000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos y", widget_blip_pos.y, -4000.0, 4000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos z", widget_blip_pos.z, -4000.0, 4000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("scale", widget_blip_scale, -1.0, 40.0, 0.01)
			
			ADD_WIDGET_int_SLIDER("alpha", widget_blip_alpha, -1, 400, 1)
			
		stop_widget_group()
		
		start_widget_group("reset recording widget")
			add_widget_bool("reset recording", widget_reset_recording)
		stop_widget_group()
		
		start_WIDGET_GROUP("vehicle colour")
			ADD_WIDGET_bool("activate car colour", widget_vehicle_colour_active)
			
			ADD_WIDGET_int_SLIDER("colour 0:", widget_colour_0, -50, 550, 1)
			ADD_WIDGET_int_SLIDER("colour 1", widget_colour_1, -50, 550, 1)
		stop_widget_group()
		
		start_WIDGET_GROUP("attach entity to entity")
			ADD_WIDGET_float_SLIDER("entity 0 pos.x:", widget_object_pos.x, -50.0, 350.0, 0.1)
			ADD_WIDGET_float_SLIDER("entity 0 pos.y:", widget_object_pos.y, -50.0, 350.0, 0.1)
			ADD_WIDGET_float_SLIDER("entity 0 pos.z:", widget_object_pos.z, -50.0, 350.0, 0.1)
			ADD_WIDGET_float_SLIDER("entity 0 rot.x:", widget_object_rot.x, 0.0, 360.00, 0.1)
			ADD_WIDGET_float_SLIDER("entity 0 rot.y:", widget_object_rot.y, 0.0, 360.00, 0.1)
			ADD_WIDGET_float_SLIDER("entity 0 rot.z:", widget_object_rot.z, 0.0, 360.00, 0.1)
		stop_widget_group()
		
		start_WIDGET_GROUP("uber distances")
			ADD_WIDGET_FLOAT_READ_ONLY("current_minimum_distance", current_minimum_distance)
			ADD_WIDGET_FLOAT_READ_ONLY("current_ideal_distance", current_ideal_distance)
			ADD_WIDGET_FLOAT_READ_ONLY("current_slow_down_distance", current_slow_down_distance)
			ADD_WIDGET_int_READ_ONLY("current_slow_down_distance", uber_speed_status)
		stop_widget_group()
		
		
		start_WIDGET_GROUP("update vehicle recording times")
			ADD_WIDGET_float_SLIDER("old recording time", widget_time_a, -50.0, 5000000.0, 100)
			ADD_WIDGET_float_SLIDER("new recording time", widget_time_b, -50.0, 5000000.0, 100)
		stop_widget_group()
		
		start_WIDGET_GROUP("create vehicles")
			ADD_WIDGET_bool("schafter2", widget_vehicle_schafter2)
			ADD_WIDGET_bool("sentinel", widget_vehicle_sentinel)
			ADD_WIDGET_bool("manana", widget_vehicle_manana)
			ADD_WIDGET_bool("speedo", widget_vehicle_speedo)
			ADD_WIDGET_bool("Mule", widget_vehicle_Mule)
		stop_widget_group()
		
		start_WIDGET_GROUP("scenario_blocking_area_widget")
			add_widget_float_slider("scenario x", widget_offset_bl.x, -7000.0, 7000.0, 0.1)
			add_widget_float_slider("scenario y", widget_offset_bl.y, -7000.0, 7000.0, 0.1)
			add_widget_float_slider("scenario z", widget_offset_bl.z, -7000.0, 7000.0, 0.1)
			
			add_widget_float_slider("scenario x", widget_offset_br.x, -7000.0, 7000.0, 0.1)
			add_widget_float_slider("scenario y", widget_offset_br.y, -7000.0, 7000.0, 0.1)
			add_widget_float_slider("scenario z", widget_offset_br.z, -7000.0, 7000.0, 0.1)
			
			add_widget_bool("add scenario blocking area", widget_add_scenario_area)
		stop_widget_group()
		
		start_WIDGET_GROUP("dialogue staus debug")
			ADD_WIDGET_int_READ_ONLY("dialogue_status", dialogue_status)
			ADD_WIDGET_int_READ_ONLY("uber_chase_in_car_conversation", uber_chase_in_car_conversation)
		stop_widget_group()
		
		start_WIDGET_GROUP("status int's")
			ADD_WIDGET_int_READ_ONLY("get_to_the_stadium_dialogue_system_status", get_to_the_stadium_dialogue_system_status)
			ADD_WIDGET_int_READ_ONLY("get_to_the_stadium_status", get_to_the_stadium_status)
		stop_widget_group()
		
	stop_widget_group()

endproc

#endif


////****************************************VEHICLE FORCE WIDGET VARIABLES****************************************
#IF IS_DEBUG_BUILD

bool reset_vehicle_position = false
bool create_veh = false
bool apply_force_position = false
bool update_vehicle = false
bool original_creation = false
bool output_data = false

int force_status = 0

float force_multiplier = 1.0
float vehicle_rot

vector force_vec_a
vector force_vec_b 
vector force_vec_ba
vector force_offset = <<0.0, 0.0, 0.0>>
vector angular_impulse

vehicle_index current_car

TEXT_WIDGET_ID widget_model_name

model_names vehicle_model


proc load_vehicle_force_widget_data()

	set_current_widget_group(family_4_widget_group)
		start_widget_group("vehicle force widget")
			
			add_widget_bool("create_vehicle", create_veh)
			add_widget_bool("reset vehicle position", reset_vehicle_position)
			add_widget_bool("update vehicle model", update_vehicle)
			
			widget_model_name = ADD_TEXT_WIDGET("Vehicle Model")
			SET_CONTENTS_OF_TEXT_WIDGET(widget_model_name, "sultan")
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle pos.x", force_vec_a.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle pos.y", force_vec_a.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle pos.z", force_vec_a.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle rot", vehicle_rot, 0, 360, 0.1)
		
			ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.x", force_vec_b.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.y", force_vec_b.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.z", force_vec_b.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.x", force_offset.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.y", force_offset.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.z", force_offset.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("angular force.x", angular_impulse.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("angular force.y", angular_impulse.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("angular force.z", angular_impulse.z, -3000.0, 3000.0, 0.1)
			
			add_widget_float_slider("force multiplier", force_multiplier, 0.0, 200, 0.1)
			
			add_widget_bool("apply force to vehicle", apply_force_position)
			
			add_widget_bool("Output data to temp_debug.txt", output_data)
			
			add_widget_string("")
			add_widget_string("Data to input into function apply_force_to_car()")
			ADD_WIDGET_FLOAT_SLIDER("vforce.x", force_vec_ba.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vforce.y", force_vec_ba.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vforce.z", force_vec_ba.z, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("voffset.x", force_offset.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("voffset.y", force_offset.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("voffset.z", force_offset.z, -3000.0, 3000.0, 0.1)
			
		stop_widget_group()
	clear_current_widget_group(family_4_widget_group)

endproc
	
proc vehicle_force_system()

	if reset_vehicle_position
		force_status = 0
		create_veh = true
		update_vehicle = false
		reset_vehicle_position = false
		
		STOP_RECORDING_ALL_VEHICLES()
		
		if DOES_ENTITY_EXIST(current_car)
			DELETE_VEHICLE(current_car)
		endif 
		
		clear_area(force_vec_a, 100, true)
	endif 
	
//	if update_vehicle or mission_vehicle_injured(current_car)
//		set_vehicle_as_no_longer_needed(current_car)
//		DELETE_VEHICLE(current_car)
//		create_veh = true 
//		force_status = 0
//		update_vehicle = false
//	endif 
	
	if output_data
		OPEN_DEBUG_FILE()
		save_string_to_debug_file("vForce = ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.z)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		save_string_to_debug_file("vOffset = ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.z)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
			
		output_data = false
	endif 
	
	
	//check script / draw debug lines and sphers 
	DRAW_MARKER(MARKER_CYLINDER, force_vec_b, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.5, 0.5, 0.5>>, 255, 0, 128, 178)

	switch force_status 
	
		case 0
			
			if create_veh
			
				vehicle_model = INT_TO_ENUM(model_names, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(widget_model_name)))
				
				IF NOT IS_MODEL_IN_CDIMAGE(vehicle_model)
					SCRIPT_ASSERT("Vehicle model does not exist!") 
				else 
				
					if not has_model_loaded(vehicle_model)
						request_model(vehicle_model)
					else 
						
						if DOES_ENTITY_EXIST(current_car)
							if is_vehicle_driveable(current_car)
								SET_ENTITY_COORDS(current_car, force_vec_a)
								SET_ENTITY_HEADING(current_car, vehicle_rot)
								force_status++
							endif 
						else 
							if not original_creation
								if not is_ped_injured(player_ped_id())
									force_vec_a = get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 4.0, 2.5>>)  
									force_vec_b = get_offset_from_entity_in_world_coords(player_ped_id(), <<-1.0, 4.0, 0.0>>)  
									original_creation = true 
								endif 
							endif 
								
							current_car = create_vehicle(vehicle_model, force_vec_a, vehicle_rot)
							set_model_as_no_longer_needed(vehicle_model)
							SET_ENTITY_PROOFS(current_car, true, true, true, true, true)
							force_status++
						endif 
						
					endif 
					
				endif 
				
			endif 

		break 
		
		case 1

			if apply_force_position
			
				if START_RECORDING_VEHICLE(current_car, 0, "lkfamily4", true)
									
					//get_car_coordinates(current_car, force_vec_a.x, force_vec_a.y, force_vec_a.z)
					
					force_vec_ba = force_vec_b - force_vec_a
					
					normalise_vector(force_vec_ba)
					
					force_vec_ba.x *= force_multiplier
					force_vec_ba.y *= force_multiplier
					force_vec_ba.z *= force_multiplier
					
					//*****TEMP WAIT TILL JACK COMES BACK AND GET HIM TO UPDATE CODE
	//				angular_impulse.x *= 60
	//				angular_impulse.y *= 60
	//				angular_impulse.z *= 60
		
					APPLY_FORCE_TO_ENTITY(current_car, APPLY_TYPE_EXTERNAL_IMPULSE, force_vec_ba, force_offset, 0, false, true, true)  
					//APPLY_FORCE_TO_ENTITY(current_car, APPLY_TYPE_IMPULSE, force_vec_ba, force_offset, 0, false, true, true)  
					APPLY_FORCE_TO_ENTITY(current_car, APPLY_TYPE_ANGULAR_IMPULSE, angular_impulse, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
	
					apply_force_position = false
				
					force_status++
				endif 
			else 
			
				SET_ENTITY_HEADING(current_car, vehicle_rot)
				SET_ENTITY_COORDS_NO_OFFSET(current_car, force_vec_a)
				
			endif 
											
		break 
		
		case 3
		
		
		break 
		
	endswitch 

endproc 



proc create_uber_vehicles_widget()

	if widget_vehicle_schafter2
		vehicle_model = schafter2
	endif 

	if widget_vehicle_sentinel
		vehicle_model = sentinel
	endif 
	
	if widget_vehicle_manana
		vehicle_model = manana
	endif 

	if widget_vehicle_speedo
		vehicle_model = speedo
	endif 
	
	if widget_vehicle_Mule
		vehicle_model = Mule
	endif 
	
	if widget_vehicle_schafter2 
	or widget_vehicle_sentinel 
	or widget_vehicle_manana
	or widget_vehicle_speedo 
	or widget_vehicle_Mule 
	
		request_model(vehicle_model)

		if has_model_loaded(vehicle_model)

			vehicle_index temp_vehicle
		
			clear_ped_tasks_immediately(player_ped_id())
		
			clear_area(get_entity_coords(player_ped_id()), 100, true)

			temp_vehicle = create_vehicle(vehicle_model, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 5.0, 1.0>>))
			set_ped_into_vehicle(player_ped_id(), temp_vehicle)
			set_vehicle_as_no_longer_needed(temp_vehicle)
			set_model_as_no_longer_needed(vehicle_model)
			
			widget_vehicle_schafter2 = false
		 	widget_vehicle_sentinel = false
			widget_vehicle_manana = false
			widget_vehicle_speedo = false
			widget_vehicle_Mule = false
			
		endif
		
	endif 

endproc 

#endif

//**************************************** END VEHICLE FORCE WIDGET ****************************************



//**************************************************LOCAL MISSION FUNCTIONS**************************************************
proc remove_vehicle_recordings()

	remove_vehicle_recording(001, "lkfamily4") 
	remove_vehicle_recording(002, "lkfamily4") 
	remove_vehicle_recording(003, "lkfamily4") 
	remove_vehicle_recording(004, "lkfamily4") 
	remove_vehicle_recording(005, "lkfamily4") 
	remove_vehicle_recording(006, "lkfamily4") 
	remove_vehicle_recording(007, "lkfamily4") 
	remove_vehicle_recording(008, "lkfamily4") 
	remove_vehicle_recording(009, "lkfamily4") 
	remove_vehicle_recording(010, "lkfamily4") 
	remove_vehicle_recording(011, "lkfamily4") 
	remove_vehicle_recording(012, "lkfamily4") 
	remove_vehicle_recording(013, "lkfamily4") 
	remove_vehicle_recording(014, "lkfamily4") 
	remove_vehicle_recording(015, "lkfamily4") 
	remove_vehicle_recording(016, "lkfamily4") 
	remove_vehicle_recording(017, "lkfamily4") 
	remove_vehicle_recording(018, "lkfamily4") 
	remove_vehicle_recording(019, "lkfamily4") 
	remove_vehicle_recording(020, "lkfamily4") 
	remove_vehicle_recording(021, "lkfamily4") 
	remove_vehicle_recording(022, "lkfamily4") 
	remove_vehicle_recording(023, "lkfamily4") 
	remove_vehicle_recording(024, "lkfamily4") 
	// remove_vehicle_recording(025, "lkfamily4") 
	remove_vehicle_recording(026, "lkfamily4") 
	remove_vehicle_recording(027, "lkfamily4") 
	remove_vehicle_recording(028, "lkfamily4") 
	remove_vehicle_recording(029, "lkfamily4") 
	remove_vehicle_recording(030, "lkfamily4") 
	remove_vehicle_recording(031, "lkfamily4") 
	remove_vehicle_recording(032, "lkfamily4") 
	remove_vehicle_recording(033, "lkfamily4") 
	remove_vehicle_recording(034, "lkfamily4") 
	remove_vehicle_recording(035, "lkfamily4") 
	remove_vehicle_recording(036, "lkfamily4") 
	remove_vehicle_recording(037, "lkfamily4") 
	remove_vehicle_recording(038, "lkfamily4") 
	remove_vehicle_recording(039, "lkfamily4") 
	remove_vehicle_recording(040, "lkfamily4") 
	remove_vehicle_recording(041, "lkfamily4") 
	remove_vehicle_recording(042, "lkfamily4") 
	remove_vehicle_recording(043, "lkfamily4") 
	// remove_vehicle_recording(044, "lkfamily4") 
	remove_vehicle_recording(045, "lkfamily4") 
	remove_vehicle_recording(046, "lkfamily4") 
	remove_vehicle_recording(047, "lkfamily4")
	remove_vehicle_recording(048, "lkfamily4")
	remove_vehicle_recording(049, "lkfamily4")
	remove_vehicle_recording(050, "lkfamily4")
	remove_vehicle_recording(051, "lkfamily4")
	remove_vehicle_recording(052, "lkfamily4")
	remove_vehicle_recording(053, "lkfamily4")
	remove_vehicle_recording(054, "lkfamily4")
	remove_vehicle_recording(055, "lkfamily4")
	remove_vehicle_recording(056, "lkfamily4")
	remove_vehicle_recording(057, "lkfamily4")
	remove_vehicle_recording(058, "lkfamily4")
	remove_vehicle_recording(059, "lkfamily4")
	remove_vehicle_recording(060, "lkfamily4")
	remove_vehicle_recording(061, "lkfamily4")
	remove_vehicle_recording(062, "lkfamily4")
	remove_vehicle_recording(063, "lkfamily4")
	remove_vehicle_recording(064, "lkfamily4")
	remove_vehicle_recording(065, "lkfamily4")
	remove_vehicle_recording(066, "lkfamily4")
	// remove_vehicle_recording(067, "lkfamily4")
	// remove_vehicle_recording(068, "lkfamily4")
	remove_vehicle_recording(069, "lkfamily4")
	remove_vehicle_recording(070, "lkfamily4")
	remove_vehicle_recording(071, "lkfamily4")
	remove_vehicle_recording(072, "lkfamily4")
	remove_vehicle_recording(073, "lkfamily4")
	remove_vehicle_recording(074, "lkfamily4")
	remove_vehicle_recording(075, "lkfamily4")
	remove_vehicle_recording(076, "lkfamily4")
	remove_vehicle_recording(077, "lkfamily4")
	// remove_vehicle_recording(078, "lkfamily4")
	// remove_vehicle_recording(079, "lkfamily4")
	remove_vehicle_recording(080, "lkfamily4")
	remove_vehicle_recording(081, "lkfamily4")
	remove_vehicle_recording(082, "lkfamily4")
	remove_vehicle_recording(083, "lkfamily4")
	remove_vehicle_recording(084, "lkfamily4")
	remove_vehicle_recording(085, "lkfamily4")
	remove_vehicle_recording(086, "lkfamily4")
	remove_vehicle_recording(087, "lkfamily4")
	remove_vehicle_recording(088, "lkfamily4")
	remove_vehicle_recording(089, "lkfamily4")
	remove_vehicle_recording(090, "lkfamily4")
	remove_vehicle_recording(091, "lkfamily4")
	remove_vehicle_recording(092, "lkfamily4")
	remove_vehicle_recording(093, "lkfamily4")
	remove_vehicle_recording(094, "lkfamily4")
	remove_vehicle_recording(095, "lkfamily4")
	remove_vehicle_recording(096, "lkfamily4")
	remove_vehicle_recording(097, "lkfamily4")
	remove_vehicle_recording(098, "lkfamily4")
	remove_vehicle_recording(099, "lkfamily4")
	remove_vehicle_recording(100, "lkfamily4")
	remove_vehicle_recording(101, "lkfamily4")
	remove_vehicle_recording(102, "lkfamily4") 
	remove_vehicle_recording(103, "lkfamily4")
	remove_vehicle_recording(104, "lkfamily4")
	remove_vehicle_recording(105, "lkfamily4")
	remove_vehicle_recording(106, "lkfamily4")
	remove_vehicle_recording(107, "lkfamily4")
	remove_vehicle_recording(108, "lkfamily4")
	remove_vehicle_recording(109, "lkfamily4")
	remove_vehicle_recording(110, "lkfamily4")
	remove_vehicle_recording(111, "lkfamily4")
	remove_vehicle_recording(112, "lkfamily4")
	remove_vehicle_recording(113, "lkfamily4")
	remove_vehicle_recording(114, "lkfamily4")
	remove_vehicle_recording(115, "lkfamily4")
	remove_vehicle_recording(116, "lkfamily4")
	remove_vehicle_recording(117, "lkfamily4")
	remove_vehicle_recording(118, "lkfamily4")
	remove_vehicle_recording(119, "lkfamily4")
	remove_vehicle_recording(120, "lkfamily4")
	remove_vehicle_recording(121, "lkfamily4")
	remove_vehicle_recording(122, "lkfamily4")
	remove_vehicle_recording(123, "lkfamily4")
	remove_vehicle_recording(124, "lkfamily4")
	remove_vehicle_recording(125, "lkfamily4")
	remove_vehicle_recording(126, "lkfamily4")
	remove_vehicle_recording(127, "lkfamily4")
	remove_vehicle_recording(128, "lkfamily4")
	remove_vehicle_recording(129, "lkfamily4")
	remove_vehicle_recording(130, "lkfamily4")
	remove_vehicle_recording(131, "lkfamily4")
	remove_vehicle_recording(132, "lkfamily4")
	remove_vehicle_recording(133, "lkfamily4")
	remove_vehicle_recording(134, "lkfamily4")
	remove_vehicle_recording(135, "lkfamily4")
	remove_vehicle_recording(136, "lkfamily4")
	remove_vehicle_recording(137, "lkfamily4")
	remove_vehicle_recording(138, "lkfamily4")
	remove_vehicle_recording(139, "lkfamily4")
	remove_vehicle_recording(140, "lkfamily4")

	remove_vehicle_recording(301, "lkfamily4")
	remove_vehicle_recording(302, "lkfamily4")
	
	remove_vehicle_recording(401, "lkfamily4")
	remove_vehicle_recording(402, "lkfamily4")
	
endproc 

PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL b_force_delete = FALSE)
	
	IF DOES_ENTITY_EXIST(veh)
		
		IF IS_VEHICLE_DRIVEABLE(veh)
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
		ENDIF

		IF IS_ENTITY_A_MISSION_ENTITY(veh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh, false)

			if is_player_playing(player_id())
				
				if is_vehicle_driveable(veh) 
					
					if not is_ped_sitting_in_vehicle(player_ped_id(), veh) 

						IF b_force_delete
							DELETE_VEHICLE(veh)
						ELSE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
						ENDIF
					
					else
					
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) 
					
					endif 
				
				else 
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) 
				
				endif 
			
			else 
				SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) //can't delete vehicle with player in it. Fail system requires player to be in car can't use clear_ped_tasks_immediaely()
			endif 
		endif 
		
	ENDIF
	
ENDPROC

proc remove_all_vehicles()

	int i = 0

	REMOVE_VEHICLE(michaels_car.veh)
	REMOVE_VEHICLE(lazlows_car.veh)
	REMOVE_VEHICLE(temp_lazlows_car)
	REMOVE_VEHICLE(truck.veh)
	REMOVE_VEHICLE(trailer.veh)
	
	for i = 0 to count_of(parked_truck) - 1
		REMOVE_VEHICLE(parked_truck[i].veh)
		REMOVE_VEHICLE(parked_trailer[i].veh)
	endfor 
	
	REMOVE_VEHICLE(ambient_car, true)
	REMOVE_VEHICLE(ambient_car_2)
	REMOVE_VEHICLE(ambient_car_3)
	REMOVE_VEHICLE(ambient_car_4)
	REMOVE_VEHICLE(ambient_car_5)
	
	for i = 0 to count_of(broken_down_cars) - 1
		REMOVE_VEHICLE(broken_down_cars[i])
	endfor 

endproc 



proc remove_all_mission_assets()

	int i = 0

	SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)

	if IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE()
	else 
		REMOVE_CUTSCENE()
	endif 

	while is_cutscene_active()
		wait(0)
	endwhile
	
	REMOVE_CUTSCENE()
	
	if does_cam_exist(lazlow_cam)
		destroy_cam(lazlow_cam)
		render_script_cams(false, false)
	endif
	kill_chase_hint_cam(chase_hint_cam)
	
	clear_ped_tasks_immediately(player_ped_id())
	clear_player_wanted_level(player_id()) 
	
	clear_mission_locate_stuff(locates_data)
	
	//CLEAR_ADDITIONAL_TEXT()
	
	remove_ped_for_dialogue(scripted_speech, 0)
	remove_ped_for_dialogue(scripted_speech, 2)
	remove_ped_for_dialogue(scripted_speech, 4)
	
	RELEASE_MISSION_AUDIO_BANK()
	
	stop_audio_scenes()
	
	trigger_music_event("FAM4_MISSION_FAIL")
	
	REPLAY_CANCEL_EVENT()

	cleanup_uber_playback(true)
	remove_all_vehicles()
	
	if does_blip_exist(michaels_blip)
		remove_blip(michaels_blip)
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		delete_ped(selector_ped.pedID[selector_ped_michael])
	endif 
	set_model_as_no_longer_needed(michael.model)
	
	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
		delete_ped(selector_ped.pedID[selector_ped_trevor])
	endif 
	set_model_as_no_longer_needed(trevor.model)
	
	if does_entity_exist(lazlow.ped)
		delete_ped(lazlow.ped)
	endif 
	set_model_as_no_longer_needed(lazlow.model)
	
	if does_particle_fx_looped_exist(lazlows_car_ptfx)
		stop_particle_fx_looped(lazlows_car_ptfx)
	endif 
	
//	if does_particle_fx_looped_exist(truck_ptfx)
//		stop_particle_fx_looped(truck_ptfx)
//	endif 
	
	if does_entity_exist(tracey.ped)
		DELETE_ped(tracey.ped)
	endif 
	set_model_as_no_longer_needed(tracey.model)
	
	if does_entity_exist(michaels_car.veh)
		if not DOES_ENTITY_BELONG_TO_THIS_SCRIPT(michaels_car.veh, false)
			set_entity_as_mission_entity(michaels_car.veh, true, true)
		endif 
		delete_vehicle(michaels_car.veh)
	endif 

	if does_entity_exist(truck.veh)
		DELETE_VEHICLE(truck.veh)
	endif 
	set_model_as_no_longer_needed(truck.model)
	
	if does_entity_exist(trailer.veh)
		DELETE_VEHICLE(trailer.veh)
	endif 
	set_model_as_no_longer_needed(trailer.model)

	for i = 0 to count_of(parked_truck) - 1
		if does_entity_exist(parked_truck[i].veh)
			DELETE_VEHICLE(parked_truck[i].veh)
		endif 
		
		if does_entity_exist(parked_trailer[i].veh)
			DELETE_VEHICLE(parked_trailer[i].veh)
		endif 
	endfor
					
	if does_entity_exist(ambient_car)
		DELETE_VEHICLE(ambient_car)
	endif
	
	if does_entity_exist(ambient_car_2)
		DELETE_VEHICLE(ambient_car_2)
	endif
	
	if does_entity_exist(ambient_car_3)
		DELETE_VEHICLE(ambient_car_3)
	endif
	
	if does_entity_exist(ambient_car_4)
		DELETE_VEHICLE(ambient_car_4)
	endif
	
	if does_entity_exist(ambient_car_5)
		DELETE_VEHICLE(ambient_car_5)
	endif 
	
	for i = 0 to count_of(ambient_ped) - 1
		delete_ped(ambient_ped[i].ped)
		set_model_as_no_longer_needed(ambient_ped[i].model)
		REMOVE_FORCED_OBJECT(ambient_ped[i].pos, 0.5, V_ILev_Chair02_ped)
	endfor 
	
	if does_entity_exist(ambient_driver.ped)
		delete_ped(ambient_driver.ped)
	endif 
	set_model_as_no_longer_needed(ambient_driver.model)
	
	if does_entity_exist(truck_driver.ped)
		delete_ped(truck_driver.ped)
		set_model_as_no_longer_needed(truck_driver.model)
	endif 
	
	if does_entity_exist(groopie.ped)
		delete_ped(groopie.ped)
		set_model_as_no_longer_needed(groopie.model)
	endif 
	
	if does_entity_exist(cylinder_truck.veh)
		DELETE_VEHICLE(cylinder_truck.veh)
		set_model_as_no_longer_needed(cylinder_truck.model)
	endif 
	
	if does_entity_exist(clip_board.obj)				
		set_model_as_no_longer_needed(clip_board.model)
		delete_object(clip_board.obj)
	endif 

	delete_all_trains()
	
	for i = 0 to count_of(broken_down_cars) - 1
		if does_entity_exist(broken_down_cars[i])
			DELETE_VEHICLE(broken_down_cars[i])
		endif 
	endfor 
	
	
	if is_valid_interior(stadium_interior)
		unpin_interior(stadium_interior)
	endif 

	if is_valid_interior(michaels_house_interior)
		unpin_interior(michaels_house_interior)
	endif 
	
	set_model_as_no_longer_needed(towtruck)
	
	set_model_as_no_longer_needed(sentinel)
	set_model_as_no_longer_needed(schafter2)
	set_model_as_no_longer_needed(manana)
	
	remove_anim_dict("veh@truck@ds@idle_panic")

	remove_vehicle_recordings()
	
	SET_PED_WALLA_DENSITY(0,0)
	
endproc 

proc delete_all_mission_assets()

	int i = 0
	
	clear_ped_tasks_immediately(player_ped_id())
	
	clear_mission_locate_stuff(locates_data)
	
	cleanup_uber_playback(true)

	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		delete_ped(selector_ped.pedID[selector_ped_michael])
	endif 
	set_model_as_no_longer_needed(michael.model)
	
	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
		delete_ped(selector_ped.pedID[selector_ped_trevor])
	endif 
	set_model_as_no_longer_needed(trevor.model)
	
	if does_entity_exist(lazlow.ped)
		delete_ped(lazlow.ped)
	endif 
	set_model_as_no_longer_needed(lazlow.model)
	
	if does_entity_exist(tracey.ped)
		DELETE_ped(tracey.ped)
	endif 
	set_model_as_no_longer_needed(tracey.model)
	
	for i = 0 to count_of(ambient_ped) - 1
		SET_PED_AS_NO_LONGER_NEEDED(ambient_ped[i].ped)
		set_model_as_no_longer_needed(ambient_ped[i].model)
		REMOVE_FORCED_OBJECT(ambient_ped[i].pos, 0.5, V_ILev_Chair02_ped)
	endfor 
	
	if does_entity_exist(truck_driver.ped)
		delete_ped(truck_driver.ped)
	endif 
	
	if does_entity_exist(clip_board.obj)
		delete_object(clip_board.obj)
	endif 
	
	if does_entity_exist(groopie.ped)
		delete_ped(groopie.ped)
	endif 
	
	
	remove_all_vehicles()

	set_model_as_no_longer_needed(lazlows_car.model)
	set_model_as_no_longer_needed(truck.model)
	set_model_as_no_longer_needed(trailer.model)
	set_model_as_no_longer_needed(towtruck)
	
	if does_particle_fx_looped_exist(lazlows_car_ptfx)
		stop_particle_fx_looped(lazlows_car_ptfx)
	endif 
	
//	if does_particle_fx_looped_exist(truck_ptfx)
//		stop_particle_fx_looped(truck_ptfx)
//	endif 

	delete_all_trains()
	
	if does_blip_exist(michaels_blip)
		remove_blip(michaels_blip)
	endif 

endproc 


proc mission_cleanup()

	kill_any_conversation()

	set_fake_wanted_level(0)
	set_max_wanted_level(6)
	set_create_random_cops(true)
	set_wanted_level_multiplier(1.0)
	
	SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", true)//enabling radio after disabling it for mocap
	
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	SET_ALL_RANDOM_PEDS_FLEE(player_id(), false)
	
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, FALSE)	
	
	if DOES_ENTITY_EXIST(truck.veh)
		if is_vehicle_driveable(truck.veh)
			set_vehicle_tyres_can_burst(truck.veh, true)
		endif 
	endif 
	
	if DOES_ENTITY_EXIST(trailer.veh)
		if is_vehicle_driveable(trailer.veh)
			set_vehicle_tyres_can_burst(trailer.veh, true)
		endif 
	endif 
	
	set_vehicle_model_is_suppressed(truck.model, false)
	set_vehicle_model_is_suppressed(trailer.model, false)
	
	if does_cam_exist(lazlow_cam)
		destroy_cam(lazlow_cam)
		render_script_cams(false, false)
	endif
	
	kill_chase_hint_cam(chase_hint_cam)
	
	REMOVE_WAYPOINT_RECORDING("family4_0")
	REMOVE_WAYPOINT_RECORDING("family4_1")
	remove_waypoint_recording("family4_2")
	
	ASSISTED_MOVEMENT_remove_ROUTE("fame1")
	
	SET_CINEMATIC_BUTTON_ACTIVE(true)
	
	SET_VEHICLE_POPULATION_BUDGET(3)

	#if is_debug_build
		STOP_RECORDING_ALL_VEHICLES()
	#endif 
	
	set_time_scale(1.0)
	
	SET_RANDOM_TRAINS(true) 
	
	cleanup_uber_playback(false)
	
	clear_mission_locate_stuff(locates_data, true)
	
	enable_dispatch_services()
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(HAIRDO_SHOP_02_SC, false)

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-365.5, -2063.4, 100.00>>, <<-141.4, -1837.0, -100.00>>, true)
	
	trigger_music_event("FAM4_MISSION_FAIL")
	
//	if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
//		if not is_ped_injured(selector_ped.pedID[selector_ped_franklin])
//			set_ped_can_ragdoll(selector_ped.pedID[selector_ped_franklin], true)
//		endif 
//	endif 
	
	//SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_62_aud_door02, <<-242.6, -2005.5, 24.8>>, false, 0)
	//SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_62_aud_door01, <<-243.1, -2007.8, 24.8>>, false, 0)
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_STADIUM_INTERIOR, BUILDINGSTATE_NORMAL, true)
						
	SET_PED_WALLA_DENSITY(0,0)
	
	SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
	
	terminate_this_thread()

endproc 
		
proc mission_passed()

	Mission_Flow_Mission_Passed()
	
	clear_player_wanted_level(player_id())
	
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, FALSE)
	AWARD_ACHIEVEMENT_FOR_MISSION(ACH01) // Who needs enemies
	mission_cleanup()

endproc 

proc mission_failed()

	delete_all_mission_assets()
	
	MISSION_CLEANUP() 
	
endproc 

proc initialise_mission_variables()

	int i = 0

	#IF IS_DEBUG_BUILD
		menu_stage_selector[0].sTxtLabel = "Family 4 start of mission"                  
		menu_stage_selector[1].sTxtLabel = "outside stadium - family_4_mcs_2"  
		menu_stage_selector[2].sTxtLabel = "run after lazlow"
		menu_stage_selector[3].sTxtLabel = "Chase truck" 
		menu_stage_selector[4].sTxtLabel = "half way through chase"
		menu_stage_selector[5].sTxtLabel = "Final Mocap - Family_4_MCS_3_concat"
	#endif 
	
	stop_mission_fail_checks = false
	police_car_created = false
	lazlow_chase_cam_active = false
	help_text_on = false
	reminder_help_played = false
	train_cutscene_playing = false
	trailer_cutscene_playing = false
	//storm_drain_cutscene_playing = false
	//waypoint_playback_paused = false
	//mocap_requested = false
	resume_lower_priority_conversation = false
	force_buddy_to_run = false
	apply_fail_wanted_level = false
	player_arrived_in_a_vehicle = false
	allow_switch_to_contiune = false
	taxi_drop_off_set_for_get_to_the_stadium = false
	trigger_switch_effect_to_trevor = false
	deactivate_truck_blipping_system = false
	new_load_scene_activated_for_michaels_house = false

	original_time = 0
	truck_wanted_status = 0
	uber_speed_status = 0
	//cam_help_time = 0
	train_system_status = 0
	lazlow_ai_system_status = 0
	run_after_lazlow_master_flow_system_status = 0
	michael_ai_system_status = 0
	//detach_trailer_system_status = 0
	detach_trailer_cutscene_status = 0
	deatch_trailer_dialogue_status = 0
	manual_car_recording_system_status = 0
	create_parked_vehicles_status = 0
	dialogue_status = 0
	cleanup_asset_status = 0
	intro_mocap_status = 0
	play_final_mocap_status = 0
	get_to_the_stadium_status = 0
	family_4_detach_trailer_status = 0
	get_to_the_stadium_dialogue_system_status = 0
	set_piece_driver_ai_status = 0
	ambient_train_system_status = 0
	uber_chase_in_car_conversation = 0
	family_4_play_stadium_mocap_status = 0
	family_4_storm_drain_cutscene_status = 0
	trigger_storm_drain_cutscene_status = 0
	run_after_lazlow_dialogue_system_status = 0
	lazlow_ai_system_2_status = 0
	groupie_ai_system_status = 0
	truck_driver_ai_system_2_status = 0
	family_4_record_lazlow_status = 0
	tracey_ai_system_status = 0
	//lazlow_cam_time = 0
	dialogue_time = 0
	fail_time = 0
	detach_trailer_help_text_system_status = 0
	get_to_stadium_audio_scene_system_status = 0
	run_after_lazlow_audio_scene_system_status = 0
	uber_chase_audio_system_status = 0
	truck_driver_ai_system_status = 0
	lazlow_horn_time = 0
	run_after_lazlow_walla_system_status = 0
	storm_drain_lane = 0
	family_4_trevor_leadin_status = 0
	ambient_ped_flee_time = 0
	create_entities_outside_stadium_status = 0
	detach_trailer_time = 0
	create_vehicles_outside_stadium_status = 0
	run_after_lazlow_switch_effect_status = 0
	lazlow_cam_dialogue_counter = 0
	
	disired_playback_speed = 1.0

	#IF IS_DEBUG_BUILD
		get_to_the_stadium_skip_status = 0
		set_piece_recording_system_status = 0
		run_after_lazlo_skip_status = 0
	#endif 

	michaels_car.model = get_player_veh_model(char_michael)
	michaels_car.pos = <<-823.5428, 181.3025, 70.6662>>
	michaels_car.heading = 142.8150  
	
	amandas_car.model = GET_NPC_VEH_MODEL(char_amanda)
	amandas_car.pos = <<-814.9363, 159.0206, 70.1653>>
	amandas_car.heading = 111.4780
	
	tracey.model = get_npc_ped_model(char_tracey)
	tracey.pos = <<-235.6421, -2002.9620, 23.7019>>      
	tracey.heading = 346.8777
	
	trevor.model = get_player_ped_model(char_trevor)
	trevor.pos = <<-821.6787, 177.1601, 70.4371>>
	trevor.heading = 60.5512
	
	lazlow.model = get_npc_ped_model(char_lazlow) 
	lazlow.pos = <<-251.9397, -2015.8335, 29.1458>>//<<-255.1706, -2000.8589, 29.1458>> //<<-244.12, -2003.60, 23.69>> 
	lazlow.heading = 179.1901//209.1077//347.1136
	
	michael.model = get_player_ped_model(char_michael)
	michael.pos = <<-818.2477, 177.6348, 71.2278>>
	michael.heading = 55.00

	groopie.model = a_m_y_bevhills_02
	groopie.pos = <<-225.6118, -2036.6278, 26.7552>>
	groopie.heading = 234.7701
	groopie.health = 200
	groopie.weapon = weapontype_unarmed
	groopie.created = false
	
	clip_board.model = P_CS_Clipboard 
	
	camera_crew[0].pos = <<-238.6681, -1999.3461, 23.6856>>
	camera_crew[0].heading = 228.3978
	camera_crew[0].model = A_M_Y_BevHills_02
	camera_crew[0].health = 200
	camera_crew[0].weapon = weapontype_unarmed
	
	camera_crew[1].pos = <<-239.6867, -2000.6594, 23.6857>>
	camera_crew[1].heading = 291.4120
	camera_crew[1].model = A_M_Y_BevHills_02
	camera_crew[1].health = 200
	camera_crew[1].weapon = weapontype_unarmed
	
	truck_driver.model = A_M_Y_Hipster_03
	truck_driver.created = false
	
	ambient_driver.model = A_M_Y_Hipster_03
	ambient_driver.created = false
	
	micro_phone.model = Prop_V_BMike_01
	video_camera.model = Prop_V_Cam_01

	lazlows_car.pos = <<-228.9739, -2048.9016, 26.6199>>     
	lazlows_car.heading = 237.5635
	lazlows_car.model = dilettante
	
	truck.pos = <<-236.0620, -2061.1475, 26.6199>>  
	truck.heading = 312.1216
	truck.model = phantom //phantom hauler
	truck.health = 1500
	truck.engine_health = 1500
	truck.petrol_tank_health = 1500
	
	trailer.pos = <<-236.0620, -2061.1475, 36.7554>>
	trailer.heading = 312.1205
	trailer.model = tvtrailer//docktrailer//trailers
	trailer.health = 1500
	trailer.engine_health = 1500
	trailer.petrol_tank_health = 1500

	parked_truck[0].pos = <<-201.8963, -2032.4478, 26.6203>>//<<-193.5531, -1979.4688, 26.6207>>
	parked_truck[0].heading = 337.8227//180.8506
	parked_truck[0].model = phantom //phantom hauler
	
	parked_truck[1].pos = <<-182.2236, -2037.1106, 26.6207>>   
	parked_truck[1].heading = 159.8179
	parked_truck[1].model = phantom //phantom hauler
	
	parked_truck[2].pos = <<-194.6085, -2059.6265, 26.6199>>
	parked_truck[2].heading = 323.6319
	parked_truck[2].model = phantom //phantom hauler
	
	parked_trailer[0].pos = <<-201.8963, -2032.4478, 26.6203>>//<<-193.5531, -1979.4688, 36.6204>>
	parked_trailer[0].heading = 337.8227//180.8506 
	parked_trailer[0].model = tvtrailer//docktrailer//trailers
	
	parked_trailer[1].pos = <<-182.2236, -2037.1106, 26.6207>>
	parked_trailer[1].heading = 159.8179 
	parked_trailer[1].model = tvtrailer//tvtrailer//docktrailer//trailers
	
	parked_trailer[2].pos = <<-194.6085, -2059.6265, 26.6199>>
	parked_trailer[2].heading = 323.6319 
	parked_trailer[2].model = tvtrailer//tvtrailer//docktrailer//trailers
	
	
	
	ambient_ped[0].pos = <<-251.91357, -2009.55750, 29.77090>>//<<-247.1424, -2013.0612, 29.1457>>   
	ambient_ped[0].heading = 7.2320
	ambient_ped[0].model = A_M_Y_Hipster_03
	ambient_ped[0].health = 200
	ambient_ped[0].name = "ambient 0"
	
	ambient_ped[1].pos = <<-253.35614, -2009.17676, 29.61563>> 
	ambient_ped[1].heading = 157.1295
	ambient_ped[1].model = A_F_Y_Hipster_03
	ambient_ped[1].health = 200
	ambient_ped[1].name = "ambient 1"
	
	ambient_ped[2].pos = <<-254.06537, -2008.93652, 29.61491>>   
	ambient_ped[2].heading = 346.0804
	ambient_ped[2].model = A_M_Y_Hipster_03
	ambient_ped[2].health = 200
	ambient_ped[2].name = "ambient 2"
	
	ambient_ped[3].pos = <<-252.69810, -2011.39661, 29.61507>>   
	ambient_ped[3].heading = 190.8480
	ambient_ped[3].model = A_F_Y_Hipster_03
	ambient_ped[3].health = 200
	ambient_ped[3].name = "ambient 3"
	
	ambient_ped[4].pos = <<-254.08226, -2010.93152, 29.61472>>   
	ambient_ped[4].heading = 137.1725
	ambient_ped[4].model = A_M_Y_Hipster_03
	ambient_ped[4].health = 200
	ambient_ped[4].name = "ambient 4"

	ambient_ped[5].pos = <<-256.99323, -2011.69604, 29.61485>>   
	ambient_ped[5].heading = 25.3523
	ambient_ped[5].model = A_M_Y_Hipster_03
	ambient_ped[5].health = 200
	ambient_ped[5].run_to_pos = <<-264.8563, -2026.9689, 29.1457>>  
	ambient_ped[5].name = "ambient 5"
	
	ambient_ped[6].pos = <<-254.78746, -2015.16748, 29.61569>>   
	ambient_ped[6].heading = 55.1177
	ambient_ped[6].model = A_F_Y_Hipster_03
	ambient_ped[6].health = 200
	ambient_ped[6].run_to_pos = <<-267.6687, -2026.7245, 29.1457>>
	ambient_ped[6].name = "ambient 6"
	
	ambient_ped[7].pos = <<-256.78110, -2016.88977, 29.61457>>   
	ambient_ped[7].heading = 61.2313
	ambient_ped[7].model = A_F_Y_Hipster_03
	ambient_ped[7].run_to_pos = <<-262.9180, -2015.7728, 29.1457>>
	ambient_ped[7].health = 200
	ambient_ped[7].name = "ambient 7"
	
	ambient_ped[8].pos = <<-256.91928, -2019.32251, 29.61442>>   
	ambient_ped[8].heading = 53.5010
	ambient_ped[8].model = A_F_Y_Hipster_03
	ambient_ped[8].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[8].health = 200
	ambient_ped[8].name = "ambient 8"
	
	ambient_ped[9].pos = <<-261.9, -2012.4, 30.4>>   
	ambient_ped[9].heading = 53.5010
	ambient_ped[9].model = A_M_Y_Hipster_03
	ambient_ped[9].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[9].health = 200
	ambient_ped[9].name = "ambient 9" //ATM 
	
	ambient_ped[10].pos = <<-253.39073, -2011.14832, 29.61450>>   
	ambient_ped[10].heading = 53.5010
	ambient_ped[10].model = A_M_Y_Hipster_03
	ambient_ped[10].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[10].health = 200
	ambient_ped[10].name = "ambient 10" 
	
	ambient_ped[11].pos = <<-253.18893, -2013.81421, 29.61418>>   
	ambient_ped[11].heading = 53.5010
	ambient_ped[11].model = A_F_Y_Hipster_03
	ambient_ped[11].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[11].health = 200
	ambient_ped[11].name = "ambient 11"
	
	ambient_ped[12].pos = <<-254.51971, -2013.18542, 29.61651>>   
	ambient_ped[12].heading = 53.5010
	ambient_ped[12].model = A_F_Y_Hipster_03
	ambient_ped[12].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[12].health = 200
	ambient_ped[12].name = "ambient 12"
	
	ambient_ped[13].pos = <<-255.71133, -2012.37500, 29.61684>>   
	ambient_ped[13].heading = 53.5010
	ambient_ped[13].model = A_M_Y_Hipster_03
	ambient_ped[13].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[13].health = 200
	ambient_ped[13].name = "ambient 13" 

	ambient_ped[14].pos = <<-256.03769, -2014.47168, 29.61486>>   
	ambient_ped[14].heading = 53.5010
	ambient_ped[14].model = A_M_Y_Hipster_03
	ambient_ped[14].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[14].health = 200
	ambient_ped[14].name = "ambient 14" 
	
	ambient_ped[15].pos = <<-258.00876, -2018.37549, 29.61538>>   
	ambient_ped[15].heading = 53.5010
	ambient_ped[15].model = A_F_Y_Hipster_03
	ambient_ped[15].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[15].health = 200
	ambient_ped[15].name = "ambient 15"
	
	ambient_ped[16].pos = <<-258.58649, -2017.88989, 29.61535>>   
	ambient_ped[16].heading = 53.5010
	ambient_ped[16].model = A_M_Y_Hipster_03
	ambient_ped[16].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[16].health = 200
	ambient_ped[16].name = "ambient 16" 
	
	ambient_ped[17].pos = <<-259.07278, -2017.37207, 29.61586>>   
	ambient_ped[17].heading = 53.5010
	ambient_ped[17].model = A_F_Y_Hipster_03
	ambient_ped[17].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[17].health = 200
	ambient_ped[17].name = "ambient 17"
	
	ambient_ped[18].pos = <<-259.38562, -2021.03564, 29.61567>>   
	ambient_ped[18].heading = 53.5010
	ambient_ped[18].model = A_M_Y_Hipster_03
	ambient_ped[18].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[18].health = 200
	ambient_ped[18].name = "ambient 18"

	ambient_ped[19].pos = <<-260.50043, -2020.14050, 29.61454>>   
	ambient_ped[19].heading = 53.5010
	ambient_ped[19].model = A_M_Y_Hipster_03
	ambient_ped[19].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[19].health = 200
	ambient_ped[19].name = "ambient 19"
	
	ambient_ped[20].pos = <<-260.61868, -2022.49890, 29.61511>>   
	ambient_ped[20].heading = 53.5010
	ambient_ped[20].model = A_M_Y_Hipster_03
	ambient_ped[20].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[20].health = 200
	ambient_ped[20].name = "ambient 20"

	ambient_ped[21].pos = <<-261.72968, -2021.56665, 29.61508>>   
	ambient_ped[21].heading = 53.5010
	ambient_ped[21].model = A_F_Y_Hipster_03
	ambient_ped[21].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[21].health = 200
	ambient_ped[21].name = "ambient 21"
	
	ambient_ped[22].pos = <<-262.80533, -2020.64758, 29.61561>>   
	ambient_ped[22].heading = 53.5010
	ambient_ped[22].model = A_M_Y_Hipster_03
	ambient_ped[22].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[22].health = 200
	ambient_ped[22].name = "ambient 22"
	
	ambient_ped[23].pos = <<-261.82846, -2023.94946, 29.61547>>   
	ambient_ped[23].heading = 53.5010
	ambient_ped[23].model = A_F_Y_Hipster_03
	ambient_ped[23].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[23].health = 200
	ambient_ped[23].name = "ambient 23"
	
	ambient_ped[24].pos = <<-262.92258, -2023.05359, 29.61676>>   
	ambient_ped[24].heading = 53.5010
	ambient_ped[24].model = A_M_Y_Hipster_03
	ambient_ped[24].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[24].health = 200
	ambient_ped[24].name = "ambient 24"
	
	ambient_ped[25].pos = <<-263.04446, -2025.40100, 29.61566>>   
	ambient_ped[25].heading = 53.5010
	ambient_ped[25].model = A_M_Y_Hipster_03
	ambient_ped[25].run_to_pos = <<-261.5929, -2013.7483, 29.1457>>
	ambient_ped[25].health = 200
	ambient_ped[25].name = "ambient 25"
	
	security.model = s_m_y_doorman_01


	train[0].pos = <<473.57, -1204.89, 29.3429>>
	train[0].heading = 270.0
	train[0].model = metrotrain//FREIGHT
	train[0].recording_number = 4
	
	train[1].pos = <<497.61, -1206.0, 29.0>>
	train[1].heading = 322.00
	train[1].model = TANKERCAR
	
	train[2].pos = <<440.77, -1201.98, 33.0>>
	train[2].model = FREIGHT
	
	cylinder_truck.model = biff
	cylinder_truck.pos = <<497.5658, -1094.8477, 28.0248>>   
	cylinder_truck.heading = 179.3035
	cylinder_truck.recording_number = 7
	
	
	cylinder[0].pos = <<458.3, -1084.37, 29.94>>
	cylinder[0].model = PROP_GASCYL_01A //PROP_BARREL_01A //prop_gascyl_01a//Prop_barrel_Exp_01A//P_LD_Conc_Cyl_01
	cylinder[0].offset = <<0.0, -2.53, 0.66>>
	cylinder[0].offset_rot = <<0.0, 0.0, 90.00>>
	cylinder[0].scale = 194000//19100.000000 
	
	cylinder[1].pos = <<458.3, -1084.37, 30.94>>
	cylinder[1].model = prop_gascyl_01a//P_LD_Conc_Cyl_01
	cylinder[1].offset = <<0.0, -1.740, 0.66>> 
	cylinder[1].offset_rot = <<0.0, 0.0, 90.00>>
	cylinder[1].scale = 19700.000000 
	
	cylinder[2].pos = <<458.3, -1084.37, 31.94>>
	cylinder[2].model = prop_gascyl_01a//Prop_barrel_Exp_01A//P_LD_Conc_Cyl_01
	cylinder[2].offset = <<0.0, -0.910, 0.66>>
	cylinder[2].offset_rot = <<0.0, 0.0, 90.00>>
	cylinder[2].scale = 20000.000000
	
	cylinder[3].pos = <<458.3, -1084.37, 32.94>>
	cylinder[3].model = prop_gascyl_01a//Prop_barrel_Exp_01A//P_LD_Conc_Cyl_01
	cylinder[3].offset = <<0.0, -0.1, 0.66>>
	cylinder[3].offset_rot = <<0.0, 0.0, 90.00>>
	cylinder[3].scale = 20300.000000 
	
	cylinder[4].pos = <<458.3, -1084.37, 33.94>>
	cylinder[4].model = prop_gascyl_01a//Prop_barrel_Exp_01A//P_LD_Conc_Cyl_01
	cylinder[4].offset = <<0.0, 0.72, 0.66>> 
	cylinder[4].offset_rot = <<0.0, 0.0, 90.00>>
	cylinder[4].scale = 20600.000000 
	
	
	lazlows_jeans.model = P_Laz_J02_S
	
	
//	cylinder[0].pos = <<458.3, -1084.37, 29.94>>
//	cylinder[0].model = P_LD_Conc_Cyl_01
//	cylinder[0].offset = <<0.0, 0.72, 0.66>>
//	cylinder[0].offset_rot = <<0.0, 0.0, 90.00>>
//	cylinder[0].scale = 20600.000000 
//	
//	cylinder[1].pos = <<458.3, -1084.37, 30.94>>
//	cylinder[1].model = P_LD_Conc_Cyl_01
//	cylinder[1].offset = <<0.0, -0.1, 0.66>>
//	cylinder[1].offset_rot = <<0.0, 0.0, 90.00>>
//	cylinder[1].scale = 20300.000000 
//	
//	cylinder[2].pos = <<458.3, -1084.37, 31.94>>
//	cylinder[2].model = P_LD_Conc_Cyl_01
//	cylinder[2].offset = <<0.0, -0.910, 0.66>>
//	cylinder[2].offset_rot = <<0.0, 0.0, 90.00>>
//	cylinder[2].scale = 20000.000000
//	
//	cylinder[3].pos = <<458.3, -1084.37, 32.94>>
//	cylinder[3].model = P_LD_Conc_Cyl_01
//	cylinder[3].offset = <<0.0, -1.740, 0.66>>
//	cylinder[3].offset_rot = <<0.0, 0.0, 90.00>>
//	cylinder[3].scale = 19700.000000
//	
//	cylinder[4].pos = <<458.3, -1084.37, 33.94>>
//	cylinder[4].model = P_LD_Conc_Cyl_01
//	cylinder[4].offset = <<0.0, -2.53, 0.66>>
//	cylinder[4].offset_rot = <<0.0, 0.0, 90.00>>
//	cylinder[4].scale = 19400.000000
	
	for i = 0 to count_of(cylinder) - 1
		cylinder_object_status[i] = 0
	endfor 
	
	for i = 0 to count_of(ambient_ped_status) - 1
		ambient_ped_status[i] = play_reaction_anim_during_cutscene//get_into_position
		ambient_ped[i].created = false
	endfor 
	
//	for i = 0 to count_of(camera_crew) - 1
//		camera_crew_ai_status[i] = 0
//	endfor 

endproc 

proc load_text_and_dialogue()
	
	register_script_with_audio()
	
	if not has_this_additional_text_loaded("fam4aud", mission_dialogue_text_slot)
		request_additional_text("fam4aud", mission_dialogue_text_slot)
	endif 
	
	if not has_this_additional_text_loaded("family4", mission_text_slot)
		request_additional_text("family4", mission_text_slot)
	endif 
	
	while not has_additional_text_loaded(mission_text_slot)
	or not has_additional_text_loaded(mission_dialogue_text_slot)
		wait(0)	
	endwhile

endproc

//new time addition = (new time - old time)  6800 - 6000 = 800  add 800 onto setpiece car time
//										   20000 - 18000 = -2000  remove 2000 to get the car to trigger earlier

proc load_uber_data()

// ****  UBER RECORDED SET PIECE CARS  **** 

//	TrafficCarPos[0] = <<-164.4686, -2056.1094, 23.8370>>
//	TrafficCarQuatX[0] = -0.0710
//	TrafficCarQuatY[0] = 0.0019
//	TrafficCarQuatZ[0] = -0.1413
//	TrafficCarQuatW[0] = 0.9874
//	TrafficCarRecording[0] = 80
//	TrafficCarStartime[0] = 3500.0000
//	TrafficCarModel[0] = schafter2

// ****  UBER RECORDED TRAFFIC  **** 

	TrafficCarPos[0] = <<-206.0217, -1880.7305, 27.4747>>
	TrafficCarQuatX[0] = -0.0032
	TrafficCarQuatY[0] = -0.0196
	TrafficCarQuatZ[0] = 0.9262
	TrafficCarQuatW[0] = -0.3766
	TrafficCarRecording[0] = 2
	TrafficCarStartime[0] = 6000.0000
	TrafficCarModel[0] = sentinel

	TrafficCarPos[1] = <<-243.0447, -1852.2184, 28.4643>>
	TrafficCarQuatX[1] = -0.0366
	TrafficCarQuatY[1] = -0.0132
	TrafficCarQuatZ[1] = 0.9692
	TrafficCarQuatW[1] = -0.2433
	TrafficCarRecording[1] = 3
	TrafficCarStartime[1] = 7000.0000
	TrafficCarModel[1] = manana

	TrafficCarPos[2] = <<-276.6977, -1841.8983, 26.6822>>
	TrafficCarQuatX[2] = 0.0378
	TrafficCarQuatY[2] = -0.0052
	TrafficCarQuatZ[2] = -0.6626
	TrafficCarQuatW[2] = 0.7480
	TrafficCarRecording[2] = 4
	TrafficCarStartime[2] = 9500.0000
	TrafficCarModel[2] = schafter2

	TrafficCarPos[3] = <<-263.7085, -1834.5907, 27.6854>>
	TrafficCarQuatX[3] = 0.0243
	TrafficCarQuatY[3] = -0.0221
	TrafficCarQuatZ[3] = -0.5941
	TrafficCarQuatW[3] = 0.8037
	TrafficCarRecording[3] = 72
	TrafficCarStartime[3] = 10000.0000
	TrafficCarModel[3] = schafter2

	TrafficCarPos[4] = <<-293.1659, -1838.3026, 25.6359>>
	TrafficCarQuatX[4] = 0.0270
	TrafficCarQuatY[4] = -0.0221
	TrafficCarQuatZ[4] = -0.6649
	TrafficCarQuatW[4] = 0.7461
	TrafficCarRecording[4] = 5
	TrafficCarStartime[4] = 12000.0000
	TrafficCarModel[4] = manana

	TrafficCarPos[5] = <<-291.3370, -1843.3103, 25.4949>>
	TrafficCarQuatX[5] = 0.0439
	TrafficCarQuatY[5] = -0.0029
	TrafficCarQuatZ[5] = -0.6870
	TrafficCarQuatW[5] = 0.7253
	TrafficCarRecording[5] = 6
	TrafficCarStartime[5] = 12000.0000
	TrafficCarModel[5] = sentinel

	TrafficCarPos[6] = <<-338.9504, -1830.9271, 23.1457>>
	TrafficCarQuatX[6] = -0.0179
	TrafficCarQuatY[6] = 0.0108
	TrafficCarQuatZ[6] = 0.7347
	TrafficCarQuatW[6] = -0.6781
	TrafficCarRecording[6] = 7
	TrafficCarStartime[6] = 13000.0000
	TrafficCarModel[6] = schafter2

	TrafficCarPos[7] = <<-153.5809, -1718.4812, 29.8741>>
	TrafficCarQuatX[7] = -0.0115
	TrafficCarQuatY[7] = -0.0252
	TrafficCarQuatZ[7] = 0.9155
	TrafficCarQuatW[7] = -0.4014
	TrafficCarRecording[7] = 8
	TrafficCarStartime[7] = 13250.0000
	TrafficCarModel[7] = speedo

	TrafficCarPos[8] = <<-120.4816, -1714.1544, 29.1332>>
	TrafficCarQuatX[8] = 0.0088
	TrafficCarQuatY[8] = 0.0254
	TrafficCarQuatZ[8] = 0.9429
	TrafficCarQuatW[8] = 0.3320
	TrafficCarRecording[8] = 9
	TrafficCarStartime[8] = 15000.0000
	TrafficCarModel[8] = sentinel

	TrafficCarPos[9] = <<-212.9735, -1810.0007, 29.4217>>
	TrafficCarQuatX[9] = -0.0018
	TrafficCarQuatY[9] = -0.0041
	TrafficCarQuatZ[9] = -0.4979
	TrafficCarQuatW[9] = 0.8672
	TrafficCarRecording[9] = 69
	TrafficCarStartime[9] = 15000.0000
	TrafficCarModel[9] = manana

	TrafficCarPos[10] = <<-72.3315, -1654.2415, 28.8377>>
	TrafficCarQuatX[10] = 0.0003
	TrafficCarQuatY[10] = 0.0006
	TrafficCarQuatZ[10] = 0.9405
	TrafficCarQuatW[10] = 0.3397
	TrafficCarRecording[10] = 73
	TrafficCarStartime[10] = 17000.0000
	TrafficCarModel[10] = manana

	TrafficCarPos[11] = <<-53.5047, -1623.8011, 28.6285>>
	TrafficCarQuatX[11] = -0.0239
	TrafficCarQuatY[11] = 0.0097
	TrafficCarQuatZ[11] = 0.9455
	TrafficCarQuatW[11] = 0.3246
	TrafficCarRecording[11] = 74
	TrafficCarStartime[11] = 17100.0000
	TrafficCarModel[11] = sentinel

	TrafficCarPos[12] = <<-14.4963, -1698.9298, 28.8408>>
	TrafficCarQuatX[12] = 0.0013
	TrafficCarQuatY[12] = 0.0006
	TrafficCarQuatZ[12] = 0.8138
	TrafficCarQuatW[12] = 0.5812
	TrafficCarRecording[12] = 10
	TrafficCarStartime[12] = 18000.0000
	TrafficCarModel[12] = manana

	TrafficCarPos[13] = <<-81.5279, -1756.8303, 29.3488>>
	TrafficCarQuatX[13] = -0.0096
	TrafficCarQuatY[13] = -0.0131
	TrafficCarQuatZ[13] = -0.1676
	TrafficCarQuatW[13] = 0.9857
	TrafficCarRecording[13] = 71
	TrafficCarStartime[13] = 20500.0000
	TrafficCarModel[13] = speedo

	TrafficCarPos[14] = <<2.0713, -1646.3097, 28.8887>>
	TrafficCarQuatX[14] = -0.0161
	TrafficCarQuatY[14] = -0.0094
	TrafficCarQuatZ[14] = 0.9179
	TrafficCarQuatW[14] = -0.3963
	TrafficCarRecording[14] = 11
	TrafficCarStartime[14] = 22000.0000
	TrafficCarModel[14] = schafter2

	TrafficCarPos[15] = <<-34.5467, -1718.3431, 28.8460>>
	TrafficCarQuatX[15] = 0.0102
	TrafficCarQuatY[15] = 0.0177
	TrafficCarQuatZ[15] = -0.5537
	TrafficCarQuatW[15] = 0.8325
	TrafficCarRecording[15] = 12
	TrafficCarStartime[15] = 22000.0000
	TrafficCarModel[15] = schafter2

	TrafficCarPos[16] = <<108.9714, -1704.7122, 28.6355>>
	TrafficCarQuatX[16] = -0.0080
	TrafficCarQuatY[16] = 0.0172
	TrafficCarQuatZ[16] = 0.4206
	TrafficCarQuatW[16] = 0.9070
	TrafficCarRecording[16] = 62
	TrafficCarStartime[16] = 24500.0000
	TrafficCarModel[16] = manana

	TrafficCarPos[17] = <<217.4134, -1616.3182, 28.8266>>
	TrafficCarQuatX[17] = -0.0075
	TrafficCarQuatY[17] = 0.0182
	TrafficCarQuatZ[17] = 0.4621
	TrafficCarQuatW[17] = 0.8866
	TrafficCarRecording[17] = 63
	TrafficCarStartime[17] = 25500.0000
	TrafficCarModel[17] = schafter2

	TrafficCarPos[18] = <<-5.5010, -1694.9628, 28.8337>>
	TrafficCarQuatX[18] = -0.0037
	TrafficCarQuatY[18] = -0.0040
	TrafficCarQuatZ[18] = 0.8342
	TrafficCarQuatW[18] = 0.5514
	TrafficCarRecording[18] = 83
	TrafficCarStartime[18] = 25900.0000
	TrafficCarModel[18] = manana

	TrafficCarPos[19] = <<157.9259, -1603.9789, 29.0520>>
	TrafficCarQuatX[19] = -0.0020
	TrafficCarQuatY[19] = -0.0002
	TrafficCarQuatZ[19] = 0.8672
	TrafficCarQuatW[19] = 0.4979
	TrafficCarRecording[19] = 14
	TrafficCarStartime[19] = 26000.0000
	TrafficCarModel[19] = speedo

	TrafficCarPos[20] = <<297.3597, -1518.5847, 28.6524>>
	TrafficCarQuatX[20] = -0.0133
	TrafficCarQuatY[20] = 0.0080
	TrafficCarQuatZ[20] = 0.8617
	TrafficCarQuatW[20] = 0.5071
	TrafficCarRecording[20] = 64
	TrafficCarStartime[20] = 27500.0000
	TrafficCarModel[20] = sentinel

	TrafficCarPos[21] = <<235.9646, -1635.4471, 28.7485>>
	TrafficCarQuatX[21] = -0.0094
	TrafficCarQuatY[21] = 0.0210
	TrafficCarQuatZ[21] = 0.4102
	TrafficCarQuatW[21] = 0.9117
	TrafficCarRecording[21] = 15
	TrafficCarStartime[21] = 28000.0000
	TrafficCarModel[21] = manana

	TrafficCarPos[22] = <<188.7709, -1558.2264, 28.5961>>
	TrafficCarQuatX[22] = -0.0025
	TrafficCarQuatY[22] = 0.0043
	TrafficCarQuatZ[22] = 0.9477
	TrafficCarQuatW[22] = -0.3190
	TrafficCarRecording[22] = 16
	TrafficCarStartime[22] = 29000.0000
	TrafficCarModel[22] = sentinel

	TrafficCarPos[23] = <<277.7000, -1491.5587, 29.0148>>
	TrafficCarQuatX[23] = -0.0140
	TrafficCarQuatY[23] = -0.0069
	TrafficCarQuatZ[23] = 0.9238
	TrafficCarQuatW[23] = -0.3825
	TrafficCarRecording[23] = 65
	TrafficCarStartime[23] = 31000.0000
	TrafficCarModel[23] = speedo

	TrafficCarPos[24] = <<381.9591, -1474.3505, 28.7077>>
	TrafficCarQuatX[24] = -0.0003
	TrafficCarQuatY[24] = 0.0011
	TrafficCarQuatZ[24] = 0.8734
	TrafficCarQuatW[24] = 0.4870
	TrafficCarRecording[24] = 66
	TrafficCarStartime[24] = 33000.0000
	TrafficCarModel[24] = sentinel

	TrafficCarPos[25] = <<338.1085, -1464.6277, 29.0680>>
	TrafficCarQuatX[25] = 0.0073
	TrafficCarQuatY[25] = -0.0125
	TrafficCarQuatZ[25] = 0.9359
	TrafficCarQuatW[25] = -0.3519
	TrafficCarRecording[25] = 68
	TrafficCarStartime[25] = 33000.0000
	TrafficCarModel[25] = manana

	TrafficCarPos[26] = <<412.8397, -1456.0123, 28.9169>>
	TrafficCarQuatX[26] = -0.0006
	TrafficCarQuatY[26] = 0.0005
	TrafficCarQuatZ[26] = 0.8832
	TrafficCarQuatW[26] = 0.4689
	TrafficCarRecording[26] = 17
	TrafficCarStartime[26] = 34000.0000
	TrafficCarModel[26] = schafter2

	TrafficCarPos[27] = <<433.0769, -1418.8912, 28.6627>>
	TrafficCarQuatX[27] = -0.0150
	TrafficCarQuatY[27] = -0.0062
	TrafficCarQuatZ[27] = 0.9995
	TrafficCarQuatW[27] = -0.0262
	TrafficCarRecording[27] = 18
	TrafficCarStartime[27] = 36000.0000
	TrafficCarModel[27] = sentinel

	TrafficCarPos[28] = <<268.1830, -1477.3182, 28.9245>>
	TrafficCarQuatX[28] = -0.0002
	TrafficCarQuatY[28] = -0.0006
	TrafficCarQuatZ[28] = 0.9097
	TrafficCarQuatW[28] = -0.4152
	TrafficCarRecording[28] = 67
	TrafficCarStartime[28] = 36700.0000
	TrafficCarModel[28] = schafter2

	TrafficCarPos[29] = <<412.7635, -1395.7159, 29.0825>>
	TrafficCarQuatX[29] = 0.0050
	TrafficCarQuatY[29] = -0.0104
	TrafficCarQuatZ[29] = 0.9080
	TrafficCarQuatW[29] = -0.4188
	TrafficCarRecording[29] = 76
	TrafficCarStartime[29] = 37500.0000
	TrafficCarModel[29] = schafter2

	TrafficCarPos[30] = <<533.9572, -1398.3364, 28.7811>>
	TrafficCarQuatX[30] = -0.0186
	TrafficCarQuatY[30] = 0.0001
	TrafficCarQuatZ[30] = 0.9985
	TrafficCarQuatW[30] = 0.0517
	TrafficCarRecording[30] = 19
	TrafficCarStartime[30] = 38500.0000
	TrafficCarModel[30] = manana

	TrafficCarPos[31] = <<528.0538, -1428.0734, 28.7182>>
	TrafficCarQuatX[31] = -0.0005
	TrafficCarQuatY[31] = -0.0001
	TrafficCarQuatZ[31] = 0.6927
	TrafficCarQuatW[31] = 0.7212
	TrafficCarRecording[31] = 75
	TrafficCarStartime[31] = 40100.0000
	TrafficCarModel[31] = sentinel

	TrafficCarPos[32] = <<472.6559, -1260.8285, 29.6180>>
	TrafficCarQuatX[32] = -0.0105
	TrafficCarQuatY[32] = 0.0036
	TrafficCarQuatZ[32] = -0.6968
	TrafficCarQuatW[32] = 0.7172
	TrafficCarRecording[32] = 21
	TrafficCarStartime[32] = 47500.0000
	TrafficCarModel[32] = Mule

	TrafficCarPos[33] = <<500.1397, -1274.9419, 28.8656>>
	TrafficCarQuatX[33] = -0.0159
	TrafficCarQuatY[33] = 0.0001
	TrafficCarQuatZ[33] = 0.9992
	TrafficCarQuatW[33] = -0.0378
	TrafficCarRecording[33] = 20
	TrafficCarStartime[33] = 48500.0000
	TrafficCarModel[33] = schafter2

	TrafficCarPos[34] = <<498.5310, -1126.8260, 28.7957>>
	TrafficCarQuatX[34] = -0.0130
	TrafficCarQuatY[34] = 0.0020
	TrafficCarQuatZ[34] = 0.9999
	TrafficCarQuatW[34] = -0.0026
	TrafficCarRecording[34] = 22
	TrafficCarStartime[34] = 49000.0000
	TrafficCarModel[34] = sentinel

	TrafficCarPos[35] = <<502.8969, -737.7939, 24.4451>>
	TrafficCarQuatX[35] = -0.0153
	TrafficCarQuatY[35] = -0.0009
	TrafficCarQuatZ[35] = 0.9991
	TrafficCarQuatW[35] = 0.0390
	TrafficCarRecording[35] = 23
	TrafficCarStartime[35] = 62000.0000
	TrafficCarModel[35] = schafter2

	TrafficCarPos[36] = <<466.5153, -679.8666, 26.6178>>
	TrafficCarQuatX[36] = 0.0065
	TrafficCarQuatY[36] = -0.0422
	TrafficCarQuatZ[36] = 0.7491
	TrafficCarQuatW[36] = -0.6610
	TrafficCarRecording[36] = 24
	TrafficCarStartime[36] = 63500.0000
	TrafficCarModel[36] = manana

	TrafficCarPos[37] = <<406.9349, -710.4351, 28.6729>>
	TrafficCarQuatX[37] = 0.0005
	TrafficCarQuatY[37] = 0.0125
	TrafficCarQuatZ[37] = -0.0197
	TrafficCarQuatW[37] = 0.9997
	TrafficCarRecording[37] = 25
	TrafficCarStartime[37] = 68500.0000
	TrafficCarModel[37] = sentinel

	TrafficCarPos[38] = <<335.3656, -738.0789, 28.8257>>
	TrafficCarQuatX[38] = 0.0009
	TrafficCarQuatY[38] = 0.0002
	TrafficCarQuatZ[38] = -0.1776
	TrafficCarQuatW[38] = 0.9841
	TrafficCarRecording[38] = 26
	TrafficCarStartime[38] = 71500.0000
	TrafficCarModel[38] = manana

	TrafficCarPos[39] = <<372.9934, -611.9315, 28.2991>>
	TrafficCarQuatX[39] = -0.0142
	TrafficCarQuatY[39] = 0.0084
	TrafficCarQuatZ[39] = 0.9689
	TrafficCarQuatW[39] = 0.2470
	TrafficCarRecording[39] = 27
	TrafficCarStartime[39] = 71700.0000
	TrafficCarModel[39] = sentinel

	TrafficCarPos[40] = <<366.9933, -632.9999, 28.6681>>
	TrafficCarQuatX[40] = 0.0013
	TrafficCarQuatY[40] = 0.0090
	TrafficCarQuatZ[40] = 0.9814
	TrafficCarQuatW[40] = 0.1915
	TrafficCarRecording[40] = 28
	TrafficCarStartime[40] = 72000.0000
	TrafficCarModel[40] = schafter2

	TrafficCarPos[41] = <<390.9968, -591.7441, 28.3359>>
	TrafficCarQuatX[41] = -0.0010
	TrafficCarQuatY[41] = 0.0075
	TrafficCarQuatZ[41] = 0.9487
	TrafficCarQuatW[41] = 0.3159
	TrafficCarRecording[41] = 29
	TrafficCarStartime[41] = 72500.0000
	TrafficCarModel[41] = schafter2

	TrafficCarPos[42] = <<212.6834, -606.1230, 28.5511>>
	TrafficCarQuatX[42] = -0.0064
	TrafficCarQuatY[42] = -0.0164
	TrafficCarQuatZ[42] = 0.8544
	TrafficCarQuatW[42] = -0.5194
	TrafficCarRecording[42] = 30
	TrafficCarStartime[42] = 73000.0000
	TrafficCarModel[42] = manana

	TrafficCarPos[43] = <<109.3219, -593.4142, 31.3559>>
	TrafficCarQuatX[43] = 0.0013
	TrafficCarQuatY[43] = 0.0075
	TrafficCarQuatZ[43] = -0.1903
	TrafficCarQuatW[43] = 0.9817
	TrafficCarRecording[43] = 31
	TrafficCarStartime[43] = 74500.0000
	TrafficCarModel[43] = speedo

	TrafficCarPos[44] = <<102.9036, -596.8571, 31.1318>>
	TrafficCarQuatX[44] = 0.0005
	TrafficCarQuatY[44] = -0.0020
	TrafficCarQuatZ[44] = -0.1782
	TrafficCarQuatW[44] = 0.9840
	TrafficCarRecording[44] = 32
	TrafficCarStartime[44] = 79000.0000
	TrafficCarModel[44] = manana

	TrafficCarPos[45] = <<-23.0533, -549.3762, 38.6333>>
	TrafficCarQuatX[45] = -0.0020
	TrafficCarQuatY[45] = -0.0357
	TrafficCarQuatZ[45] = 0.7315
	TrafficCarQuatW[45] = -0.6809
	TrafficCarRecording[45] = 33
	TrafficCarStartime[45] = 80000.0000
	TrafficCarModel[45] = sentinel

	TrafficCarPos[46] = <<-60.2264, -570.8044, 37.5655>>
	TrafficCarQuatX[46] = 0.0763
	TrafficCarQuatY[46] = 0.0013
	TrafficCarQuatZ[46] = -0.1846
	TrafficCarQuatW[46] = 0.9798
	TrafficCarRecording[46] = 34
	TrafficCarStartime[46] = 81500.0000
	TrafficCarModel[46] = schafter2

	TrafficCarPos[47] = <<-68.8900, -584.9039, 35.8065>>
	TrafficCarQuatX[47] = 0.0228
	TrafficCarQuatY[47] = -0.0029
	TrafficCarQuatZ[47] = -0.1597
	TrafficCarQuatW[47] = 0.9869
	TrafficCarRecording[47] = 35
	TrafficCarStartime[47] = 84000.0000
	TrafficCarModel[47] = sentinel

	TrafficCarPos[48] = <<5.5875, -324.5258, 44.1159>>
	TrafficCarQuatX[48] = -0.0045
	TrafficCarQuatY[48] = -0.0484
	TrafficCarQuatZ[48] = 0.9805
	TrafficCarQuatW[48] = 0.1906
	TrafficCarRecording[48] = 37
	TrafficCarStartime[48] = 85200.0000
	TrafficCarModel[48] = schafter2

	TrafficCarPos[49] = <<-5.0009, -276.8104, 46.5723>>
	TrafficCarQuatX[49] = -0.0178
	TrafficCarQuatY[49] = -0.0006
	TrafficCarQuatZ[49] = 0.9544
	TrafficCarQuatW[49] = -0.2980
	TrafficCarRecording[49] = 38
	TrafficCarStartime[49] = 90000.0000
	TrafficCarModel[49] = sentinel

	TrafficCarPos[50] = <<45.5980, -165.0067, 54.8493>>
	TrafficCarQuatX[50] = -0.0080
	TrafficCarQuatY[50] = -0.0223
	TrafficCarQuatZ[50] = 0.8178
	TrafficCarQuatW[50] = -0.5749
	TrafficCarRecording[50] = 40
	TrafficCarStartime[50] = 95000.0000
	TrafficCarModel[50] = speedo

	TrafficCarPos[51] = <<54.4349, -301.0910, 46.7789>>
	TrafficCarQuatX[51] = -0.0185
	TrafficCarQuatY[51] = -0.0310
	TrafficCarQuatZ[51] = 0.8226
	TrafficCarQuatW[51] = -0.5674
	TrafficCarRecording[51] = 42
	TrafficCarStartime[51] = 100000.0000
	TrafficCarModel[51] = manana

	TrafficCarPos[52] = <<186.4678, -334.2580, 43.6281>>
	TrafficCarQuatX[52] = -0.0010
	TrafficCarQuatY[52] = 0.0010
	TrafficCarQuatZ[52] = 0.5602
	TrafficCarQuatW[52] = 0.8283
	TrafficCarRecording[52] = 43
	TrafficCarStartime[52] = 101000.0000
	TrafficCarModel[52] = schafter2

	TrafficCarPos[53] = <<337.9920, -391.9135, 44.7304>>
	TrafficCarQuatX[53] = 0.0021
	TrafficCarQuatY[53] = 0.0021
	TrafficCarQuatZ[53] = 0.5651
	TrafficCarQuatW[53] = 0.8250
	TrafficCarRecording[53] = 48
	TrafficCarStartime[53] = 104000.0000
	TrafficCarModel[53] = manana

	TrafficCarPos[54] = <<196.7375, -313.3328, 43.7309>>
	TrafficCarQuatX[54] = -0.0055
	TrafficCarQuatY[54] = -0.0176
	TrafficCarQuatZ[54] = 0.9897
	TrafficCarQuatW[54] = 0.1420
	TrafficCarRecording[54] = 46
	TrafficCarStartime[54] = 106000.0000
	TrafficCarModel[54] = schafter2

	TrafficCarPos[55] = <<250.8725, -365.2076, 44.1542>>
	TrafficCarQuatX[55] = -0.0174
	TrafficCarQuatY[55] = -0.0052
	TrafficCarQuatZ[55] = 0.8039
	TrafficCarQuatW[55] = -0.5944
	TrafficCarRecording[55] = 50
	TrafficCarStartime[55] = 106000.0000
	TrafficCarModel[55] = schafter2

	TrafficCarPos[56] = <<319.1082, -346.9253, 45.8237>>
	TrafficCarQuatX[56] = -0.0191
	TrafficCarQuatY[56] = -0.0550
	TrafficCarQuatZ[56] = 0.9719
	TrafficCarQuatW[56] = 0.2282
	TrafficCarRecording[56] = 47
	TrafficCarStartime[56] = 106100.0000
	TrafficCarModel[56] = sentinel

	TrafficCarPos[57] = <<324.4210, -342.9175, 46.7807>>
	TrafficCarQuatX[57] = -0.0172
	TrafficCarQuatY[57] = -0.0711
	TrafficCarQuatZ[57] = 0.9864
	TrafficCarQuatW[57] = 0.1474
	TrafficCarRecording[57] = 49
	TrafficCarStartime[57] = 108000.0000
	TrafficCarModel[57] = manana

	TrafficCarPos[58] = <<451.9781, -337.0199, 47.2480>>
	TrafficCarQuatX[58] = 0.0068
	TrafficCarQuatY[58] = 0.0028
	TrafficCarQuatZ[58] = 0.9618
	TrafficCarQuatW[58] = 0.2738
	TrafficCarRecording[58] = 52
	TrafficCarStartime[58] = 111000.0000
	TrafficCarModel[58] = speedo

	TrafficCarPos[59] = <<423.7515, -307.0238, 49.6123>>
	TrafficCarQuatX[59] = 0.0044
	TrafficCarQuatY[59] = -0.0397
	TrafficCarQuatZ[59] = 0.8441
	TrafficCarQuatW[59] = -0.5346
	TrafficCarRecording[59] = 53
	TrafficCarStartime[59] = 111100.0000
	TrafficCarModel[59] = manana

	TrafficCarPos[60] = <<440.5575, -303.2848, 48.7468>>
	TrafficCarQuatX[60] = 0.0190
	TrafficCarQuatY[60] = -0.0290
	TrafficCarQuatZ[60] = 0.8561
	TrafficCarQuatW[60] = -0.5157
	TrafficCarRecording[60] = 54
	TrafficCarStartime[60] = 113100.0000
	TrafficCarModel[60] = manana

	TrafficCarPos[61] = <<438.0004, -307.1298, 48.8818>>
	TrafficCarQuatX[61] = 0.0188
	TrafficCarQuatY[61] = -0.0296
	TrafficCarQuatZ[61] = 0.8477
	TrafficCarQuatW[61] = -0.5293
	TrafficCarRecording[61] = 55
	TrafficCarStartime[61] = 115100.0000
	TrafficCarModel[61] = schafter2

	TrafficCarPos[62] = <<639.7904, -383.6028, 42.3535>>
	TrafficCarQuatX[62] = 0.0190
	TrafficCarQuatY[62] = -0.0018
	TrafficCarQuatZ[62] = 0.5239
	TrafficCarQuatW[62] = 0.8516
	TrafficCarRecording[62] = 58
	TrafficCarStartime[62] = 116000.0000
	TrafficCarModel[62] = sentinel

	TrafficCarPos[63] = <<690.0759, -415.4828, 40.4451>>
	TrafficCarQuatX[63] = 0.0262
	TrafficCarQuatY[63] = 0.0264
	TrafficCarQuatZ[63] = 0.4536
	TrafficCarQuatW[63] = 0.8904
	TrafficCarRecording[63] = 59
	TrafficCarStartime[63] = 120000.0000
	TrafficCarModel[63] = schafter2

	TrafficCarPos[64] = <<725.4980, -450.7779, 37.6910>>
	TrafficCarQuatX[64] = 0.0197
	TrafficCarQuatY[64] = 0.0206
	TrafficCarQuatZ[64] = 0.4235
	TrafficCarQuatW[64] = 0.9055
	TrafficCarRecording[64] = 60
	TrafficCarStartime[64] = 121000.0000
	TrafficCarModel[64] = sentinel

	TrafficCarPos[65] = <<717.0089, -436.9935, 38.4648>>
	TrafficCarQuatX[65] = 0.0177
	TrafficCarQuatY[65] = 0.0240
	TrafficCarQuatZ[65] = 0.4359
	TrafficCarQuatW[65] = 0.8995
	TrafficCarRecording[65] = 61
	TrafficCarStartime[65] = 121000.0000
	TrafficCarModel[65] = manana



	// ****  UBER RECORDED PARKED CARS  **** 

	ParkedCarPos[0] = <<25.7395, -1708.2628, 28.7747>>
	ParkedCarQuatX[0] = 0.0000
	ParkedCarQuatY[0] = 0.0000
	ParkedCarQuatZ[0] = 0.2217
	ParkedCarQuatW[0] = 0.9751
	ParkedCarModel[0] = sentinel

	ParkedCarPos[1] = <<383.5260, -1452.0549, 29.0287>>
	ParkedCarQuatX[1] = 0.0142
	ParkedCarQuatY[1] = 0.0098
	ParkedCarQuatZ[1] = 0.2300
	ParkedCarQuatW[1] = 0.9730
	ParkedCarModel[1] = schafter2

	ParkedCarPos[2] = <<486.8542, -1167.3306, 28.9366>>
	ParkedCarQuatX[2] = 0.0000
	ParkedCarQuatY[2] = 0.0000
	ParkedCarQuatZ[2] = 0.7338
	ParkedCarQuatW[2] = -0.6794
	ParkedCarModel[2] = schafter2

	ParkedCarPos[3] = <<458.2476, -639.8495, 28.1547>>
	ParkedCarQuatX[3] = 0.0001
	ParkedCarQuatY[3] = 0.0003
	ParkedCarQuatZ[3] = 0.2649
	ParkedCarQuatW[3] = 0.9643
	ParkedCarModel[3] = schafter2



	// ****  UBER RECORDED SET PIECE CARS  **** 

	SetPieceCarPos[0] = <<494.3779, -634.8876, 24.9650>>
	SetPieceCarQuatX[0] = -0.0085
	SetPieceCarQuatY[0] = 0.0132
	SetPieceCarQuatZ[0] = 0.6339
	SetPieceCarQuatW[0] = 0.7733
	SetPieceCarRecording[0] = 137
	SetPieceCarStartime[0] = 91000.0000
	SetPieceCarRecordingSpeed[0] = 1.0000
	SetPieceCarModel[0] = Phantom

	SetPieceCarPos[1] = <<-76.4095, -606.6107, 35.8049>>
	SetPieceCarQuatX[1] = -0.0004
	SetPieceCarQuatY[1] = 0.0000
	SetPieceCarQuatZ[1] = -0.1581
	SetPieceCarQuatW[1] = 0.9874
	SetPieceCarRecording[1] = 36
	SetPieceCarStartime[1] = 84500.0000
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = manana

	SetPieceCarPos[2] = <<-40.1297, -260.0975, 45.7146>>
	SetPieceCarQuatX[2] = -0.0087
	SetPieceCarQuatY[2] = 0.0118
	SetPieceCarQuatZ[2] = 0.8115
	SetPieceCarQuatW[2] = -0.5842
	SetPieceCarRecording[2] = 39
	SetPieceCarStartime[2] = 96000.0000
	SetPieceCarRecordingSpeed[2] = 1.0000
	SetPieceCarModel[2] = schafter2

	SetPieceCarPos[3] = <<84.8523, -293.1762, 46.0589>>
	SetPieceCarQuatX[3] = -0.0019
	SetPieceCarQuatY[3] = 0.0293
	SetPieceCarQuatZ[3] = 0.5779
	SetPieceCarQuatW[3] = 0.8156
	SetPieceCarRecording[3] = 41
	SetPieceCarStartime[3] = 97000.0000
	SetPieceCarRecordingSpeed[3] = 1.0000
	SetPieceCarModel[3] = manana

	SetPieceCarPos[4] = <<172.4911, -375.0501, 42.2740>>
	SetPieceCarQuatX[4] = 0.0282
	SetPieceCarQuatY[4] = -0.0032
	SetPieceCarQuatZ[4] = -0.2149
	SetPieceCarQuatW[4] = 0.9762
	SetPieceCarRecording[4] = 44
	SetPieceCarStartime[4] = 104000.0000
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = sentinel

	SetPieceCarPos[5] = <<171.7265, -390.1432, 41.6091>>
	SetPieceCarQuatX[5] = 0.0283
	SetPieceCarQuatY[5] = -0.0041
	SetPieceCarQuatZ[5] = -0.1949
	SetPieceCarQuatW[5] = 0.9804
	SetPieceCarRecording[5] = 45
	SetPieceCarStartime[5] = 106000.0000
	SetPieceCarRecordingSpeed[5] = 1.0000
	SetPieceCarModel[5] = manana

	SetPieceCarPos[6] = <<333.1232, -351.4108, 45.8325>>
	SetPieceCarQuatX[6] = 0.0103
	SetPieceCarQuatY[6] = -0.0667
	SetPieceCarQuatZ[6] = 0.9862
	SetPieceCarQuatW[6] = 0.1509
	SetPieceCarRecording[6] = 51
	SetPieceCarStartime[6] = 109000.0000
	SetPieceCarRecordingSpeed[6] = 1.0000
	SetPieceCarModel[6] = sentinel

	SetPieceCarPos[7] = <<504.7013, -264.2179, 46.8504>>
	SetPieceCarQuatX[7] = 0.0004
	SetPieceCarQuatY[7] = -0.0199
	SetPieceCarQuatZ[7] = 0.9636
	SetPieceCarQuatW[7] = 0.2664
	SetPieceCarRecording[7] = 56
	SetPieceCarStartime[7] = 116000.0000
	SetPieceCarRecordingSpeed[7] = 1.0000
	SetPieceCarModel[7] = sentinel

	SetPieceCarPos[8] = <<617.3689, -343.3064, 42.9542>>
	SetPieceCarQuatX[8] = -0.0003
	SetPieceCarQuatY[8] = -0.0011
	SetPieceCarQuatZ[8] = 0.9826
	SetPieceCarQuatW[8] = 0.1858
	SetPieceCarRecording[8] = 57
	SetPieceCarStartime[8] = 118000.0000
	SetPieceCarRecordingSpeed[8] = 1.0000
	SetPieceCarModel[8] = manana

	SetPieceCarPos[9] = <<-164.4686, -2056.1094, 23.8370>>
	SetPieceCarQuatX[9] = -0.0710
	SetPieceCarQuatY[9] = 0.0019
	SetPieceCarQuatZ[9] = -0.1413
	SetPieceCarQuatW[9] = 0.9874
	SetPieceCarRecording[9] = 80
	SetPieceCarStartime[9] = 3500.0000
	SetPieceCarRecordingSpeed[9] = 1.0000
	SetPieceCarModel[9] = schafter2

	SetPieceCarPos[10] = <<-48.2772, -1712.6143, 28.8332>>
	SetPieceCarQuatX[10] = -0.0030
	SetPieceCarQuatY[10] = -0.0045
	SetPieceCarQuatZ[10] = 0.8231
	SetPieceCarQuatW[10] = 0.5679
	SetPieceCarRecording[10] = 13
	SetPieceCarStartime[10] = 17900.0000
	SetPieceCarRecordingSpeed[10] = 1.0000
	SetPieceCarModel[10] = sentinel

	SetPieceCarPos[11] = <<-77.0531, -1786.7689, 27.7276>>
	SetPieceCarQuatX[11] = 0.0108
	SetPieceCarQuatY[11] = 0.0247
	SetPieceCarQuatZ[11] = 0.3258
	SetPieceCarQuatW[11] = 0.9451
	SetPieceCarRecording[11] = 82
	SetPieceCarStartime[11] = 20000.0000
	SetPieceCarRecordingSpeed[11] = 1.0000
	SetPieceCarModel[11] = sentinel

	SetPieceCarPos[12] = <<625.6348, -1433.3500, 29.9579>>
	SetPieceCarQuatX[12] = -0.0056
	SetPieceCarQuatY[12] = -0.0043
	SetPieceCarQuatZ[12] = 0.7058
	SetPieceCarQuatW[12] = 0.7084
	SetPieceCarRecording[12] = 77
	SetPieceCarStartime[12] = 42000.0000
	SetPieceCarRecordingSpeed[12] = 1.0000
	SetPieceCarModel[12] = schafter2

	SetPieceCarPos[13] = <<648.6110, -1429.1584, 29.9931>>
	SetPieceCarQuatX[13] = -0.0140
	SetPieceCarQuatY[13] = 0.0060
	SetPieceCarQuatZ[13] = 0.7031
	SetPieceCarQuatW[13] = 0.7110
	SetPieceCarRecording[13] = 78
	SetPieceCarStartime[13] = 42200.0000
	SetPieceCarRecordingSpeed[13] = 1.0000
	SetPieceCarModel[13] = sentinel

	SetPieceCarPos[14] = <<524.4578, -1438.7028, 28.7174>>
	SetPieceCarQuatX[14] = -0.0011
	SetPieceCarQuatY[14] = 0.0000
	SetPieceCarQuatZ[14] = 0.7202
	SetPieceCarQuatW[14] = -0.6938
	SetPieceCarRecording[14] = 79
	SetPieceCarStartime[14] = 41500.0000
	SetPieceCarRecordingSpeed[14] = 1.0000
	SetPieceCarModel[14] = sentinel

	SetPieceCarPos[15] = <<559.7665, -1443.7693, 28.9727>>
	SetPieceCarQuatX[15] = -0.0151
	SetPieceCarQuatY[15] = -0.0034
	SetPieceCarQuatZ[15] = 0.7189
	SetPieceCarQuatW[15] = -0.6949
	SetPieceCarRecording[15] = 83
	SetPieceCarStartime[15] = 43000.0000
	SetPieceCarRecordingSpeed[15] = 1.0000
	SetPieceCarModel[15] = manana

	SetPieceCarPos[16] = <<667.5609, -1432.7021, 30.4244>>
	SetPieceCarQuatX[16] = -0.0045
	SetPieceCarQuatY[16] = -0.0037
	SetPieceCarQuatZ[16] = 0.7154
	SetPieceCarQuatW[16] = 0.6987
	SetPieceCarRecording[16] = 84
	SetPieceCarStartime[16] = 43100.0000
	SetPieceCarRecordingSpeed[16] = 1.0000
	SetPieceCarModel[16] = manana

	
	//start from recording number = 85
	
	//dont use 80, 82
	

endproc 

proc load_mission_vehicle_recordings()

	request_vehicle_recording(001, "lkfamily4") 
	request_vehicle_recording(002, "lkfamily4") 
	request_vehicle_recording(003, "lkfamily4") 
	request_vehicle_recording(004, "lkfamily4") 
	request_vehicle_recording(005, "lkfamily4") 
	request_vehicle_recording(006, "lkfamily4") 
	request_vehicle_recording(007, "lkfamily4") 
	request_vehicle_recording(008, "lkfamily4") 
	request_vehicle_recording(009, "lkfamily4") 
	request_vehicle_recording(010, "lkfamily4") 
	request_vehicle_recording(011, "lkfamily4") 
	request_vehicle_recording(012, "lkfamily4") 
	request_vehicle_recording(013, "lkfamily4") 
	request_vehicle_recording(014, "lkfamily4") 
	request_vehicle_recording(015, "lkfamily4") 
	request_vehicle_recording(016, "lkfamily4") 
	request_vehicle_recording(017, "lkfamily4") 
	request_vehicle_recording(018, "lkfamily4") 
	request_vehicle_recording(019, "lkfamily4") 
	request_vehicle_recording(020, "lkfamily4") 
	request_vehicle_recording(021, "lkfamily4") 
	request_vehicle_recording(022, "lkfamily4") 
	request_vehicle_recording(023, "lkfamily4") 
	request_vehicle_recording(024, "lkfamily4") 
	//request_vehicle_recording(025, "lkfamily4") 
	request_vehicle_recording(026, "lkfamily4") 
	request_vehicle_recording(027, "lkfamily4") 
	request_vehicle_recording(028, "lkfamily4") 
	request_vehicle_recording(029, "lkfamily4") 
	request_vehicle_recording(030, "lkfamily4") 
	request_vehicle_recording(031, "lkfamily4") 
	request_vehicle_recording(032, "lkfamily4") 
	request_vehicle_recording(033, "lkfamily4") 
	request_vehicle_recording(034, "lkfamily4") 
	request_vehicle_recording(035, "lkfamily4") 
	request_vehicle_recording(036, "lkfamily4") 
	request_vehicle_recording(037, "lkfamily4") 
	request_vehicle_recording(038, "lkfamily4") 
	request_vehicle_recording(039, "lkfamily4") 
	request_vehicle_recording(040, "lkfamily4") 
	request_vehicle_recording(041, "lkfamily4") 
	request_vehicle_recording(042, "lkfamily4") 
	request_vehicle_recording(043, "lkfamily4") 
	//request_vehicle_recording(044, "lkfamily4") 
	request_vehicle_recording(045, "lkfamily4") 
	request_vehicle_recording(046, "lkfamily4") 
	request_vehicle_recording(047, "lkfamily4")
	request_vehicle_recording(048, "lkfamily4")
	request_vehicle_recording(049, "lkfamily4")
	request_vehicle_recording(050, "lkfamily4")
	request_vehicle_recording(051, "lkfamily4")
	request_vehicle_recording(052, "lkfamily4")
	request_vehicle_recording(053, "lkfamily4")
	request_vehicle_recording(054, "lkfamily4")
	request_vehicle_recording(055, "lkfamily4")
	request_vehicle_recording(056, "lkfamily4")
	request_vehicle_recording(057, "lkfamily4")
	request_vehicle_recording(058, "lkfamily4")
	request_vehicle_recording(059, "lkfamily4")
	request_vehicle_recording(060, "lkfamily4")
	request_vehicle_recording(061, "lkfamily4")
	request_vehicle_recording(062, "lkfamily4")
	request_vehicle_recording(063, "lkfamily4")
	request_vehicle_recording(064, "lkfamily4")
	request_vehicle_recording(065, "lkfamily4")
	request_vehicle_recording(066, "lkfamily4")
	//request_vehicle_recording(067, "lkfamily4")
	//request_vehicle_recording(068, "lkfamily4")
	request_vehicle_recording(069, "lkfamily4")
	request_vehicle_recording(070, "lkfamily4")
	request_vehicle_recording(071, "lkfamily4")
	request_vehicle_recording(072, "lkfamily4")
	request_vehicle_recording(073, "lkfamily4")
	request_vehicle_recording(074, "lkfamily4")
	request_vehicle_recording(075, "lkfamily4")
	request_vehicle_recording(076, "lkfamily4")
	request_vehicle_recording(077, "lkfamily4")
	//request_vehicle_recording(078, "lkfamily4")
	//request_vehicle_recording(079, "lkfamily4")
	request_vehicle_recording(080, "lkfamily4")
	request_vehicle_recording(081, "lkfamily4")
	request_vehicle_recording(082, "lkfamily4")
	request_vehicle_recording(083, "lkfamily4")
	request_vehicle_recording(084, "lkfamily4")
	request_vehicle_recording(085, "lkfamily4")
	request_vehicle_recording(086, "lkfamily4")
	request_vehicle_recording(087, "lkfamily4")
	request_vehicle_recording(088, "lkfamily4")
	request_vehicle_recording(089, "lkfamily4")
	request_vehicle_recording(090, "lkfamily4")
	request_vehicle_recording(091, "lkfamily4")
	request_vehicle_recording(092, "lkfamily4")
	request_vehicle_recording(093, "lkfamily4")
	request_vehicle_recording(094, "lkfamily4")
	request_vehicle_recording(095, "lkfamily4")
//	request_vehicle_recording(096, "lkfamily4")
//	request_vehicle_recording(097, "lkfamily4")
//	request_vehicle_recording(098, "lkfamily4")
//	request_vehicle_recording(099, "lkfamily4")
//	request_vehicle_recording(100, "lkfamily4")
//	request_vehicle_recording(101, "lkfamily4")
//	request_vehicle_recording(102, "lkfamily4") 
//	request_vehicle_recording(103, "lkfamily4")
//	request_vehicle_recording(104, "lkfamily4")
//	request_vehicle_recording(105, "lkfamily4")
//	request_vehicle_recording(106, "lkfamily4")
//	request_vehicle_recording(107, "lkfamily4")
//	request_vehicle_recording(108, "lkfamily4")
//	request_vehicle_recording(109, "lkfamily4")
//	request_vehicle_recording(110, "lkfamily4")
//	request_vehicle_recording(111, "lkfamily4")
	request_vehicle_recording(112, "lkfamily4")
//	request_vehicle_recording(113, "lkfamily4")
//	request_vehicle_recording(114, "lkfamily4")
//	request_vehicle_recording(115, "lkfamily4")
//	request_vehicle_recording(116, "lkfamily4")
//	request_vehicle_recording(117, "lkfamily4")
//	request_vehicle_recording(118, "lkfamily4")
//	request_vehicle_recording(119, "lkfamily4")
//	request_vehicle_recording(120, "lkfamily4")
//	request_vehicle_recording(121, "lkfamily4")
//	request_vehicle_recording(122, "lkfamily4")
//	request_vehicle_recording(123, "lkfamily4")
//	request_vehicle_recording(124, "lkfamily4")
//	request_vehicle_recording(125, "lkfamily4")
//	request_vehicle_recording(126, "lkfamily4")
//	request_vehicle_recording(127, "lkfamily4")
//	request_vehicle_recording(128, "lkfamily4")
//	request_vehicle_recording(129, "lkfamily4")
//	request_vehicle_recording(130, "lkfamily4")
//	request_vehicle_recording(131, "lkfamily4")
//	request_vehicle_recording(132, "lkfamily4")
//	request_vehicle_recording(133, "lkfamily4")
//	request_vehicle_recording(134, "lkfamily4")
//	request_vehicle_recording(135, "lkfamily4")
//	request_vehicle_recording(136, "lkfamily4")
	request_vehicle_recording(137, "lkfamily4")
//	request_vehicle_recording(138, "lkfamily4")
//	request_vehicle_recording(139, "lkfamily4")
//	request_vehicle_recording(140, "lkfamily4")

//	request_vehicle_recording(001, "lkfamily4tr")
//	request_vehicle_recording(002, "lkfamily4tr")
//	request_vehicle_recording(003, "lkfamily4tr")
//	request_vehicle_recording(004, "lkfamily4tr")
//	request_vehicle_recording(005, "lkfamily4tr")
//	request_vehicle_recording(006, "lkfamily4tr")
//	request_vehicle_recording(007, "lkfamily4tr")
//	request_vehicle_recording(008, "lkfamily4tr")
//	request_vehicle_recording(011, "lkfamily4tr")
//	request_vehicle_recording(012, "lkfamily4tr")
	
	request_vehicle_recording(201, "lkfamily4")
	request_vehicle_recording(202, "lkfamily4")
//	request_vehicle_recording(203, "lkfamily4")
//	request_vehicle_recording(204, "lkfamily4")
//	request_vehicle_recording(205, "lkfamily4")
	
	request_vehicle_recording(301, "lkfamily4")
	request_vehicle_recording(302, "lkfamily4")
	
	request_vehicle_recording(401, "lkfamily4")
	request_vehicle_recording(402, "lkfamily4")
	request_vehicle_recording(403, "lkfamily4")
	request_vehicle_recording(404, "lkfamily4")
	request_vehicle_recording(405, "lkfamily4")
	request_vehicle_recording(406, "lkfamily4")
	
	request_vehicle_recording(501, "lkfamily4")
	request_vehicle_recording(502, "lkfamily4")
	
	//recordings removed
//	096 - 111
//	113 = 136
//	138 - 140

endproc 

func bool has_mission_vehicle_recordings_loaded()

	if has_vehicle_recording_been_loaded(001, "lkfamily4") 
	and has_vehicle_recording_been_loaded(002, "lkfamily4") 
	and has_vehicle_recording_been_loaded(003, "lkfamily4") 
	and has_vehicle_recording_been_loaded(004, "lkfamily4") 
	and has_vehicle_recording_been_loaded(005, "lkfamily4") 
	and has_vehicle_recording_been_loaded(006, "lkfamily4") 
	and has_vehicle_recording_been_loaded(007, "lkfamily4") 
	and has_vehicle_recording_been_loaded(008, "lkfamily4") 
	and has_vehicle_recording_been_loaded(009, "lkfamily4") 
	and has_vehicle_recording_been_loaded(010, "lkfamily4") 
	and has_vehicle_recording_been_loaded(011, "lkfamily4") 
	and has_vehicle_recording_been_loaded(012, "lkfamily4") 
	and has_vehicle_recording_been_loaded(013, "lkfamily4") 
	and has_vehicle_recording_been_loaded(014, "lkfamily4") 
	and has_vehicle_recording_been_loaded(015, "lkfamily4") 
	and has_vehicle_recording_been_loaded(016, "lkfamily4") 
	and has_vehicle_recording_been_loaded(017, "lkfamily4") 
	and has_vehicle_recording_been_loaded(018, "lkfamily4") 
	and has_vehicle_recording_been_loaded(019, "lkfamily4") 
	and has_vehicle_recording_been_loaded(020, "lkfamily4") 
	and has_vehicle_recording_been_loaded(021, "lkfamily4") 
	and has_vehicle_recording_been_loaded(022, "lkfamily4") 
	and has_vehicle_recording_been_loaded(023, "lkfamily4") 
	and has_vehicle_recording_been_loaded(024, "lkfamily4") 
	//and has_vehicle_recording_been_loaded(025, "lkfamily4") 
	and has_vehicle_recording_been_loaded(026, "lkfamily4") 
	and has_vehicle_recording_been_loaded(027, "lkfamily4") 
	and has_vehicle_recording_been_loaded(028, "lkfamily4") 
	and has_vehicle_recording_been_loaded(029, "lkfamily4") 
	and has_vehicle_recording_been_loaded(030, "lkfamily4") 
	and has_vehicle_recording_been_loaded(031, "lkfamily4") 
	and has_vehicle_recording_been_loaded(032, "lkfamily4") 
	and has_vehicle_recording_been_loaded(033, "lkfamily4") 
	and has_vehicle_recording_been_loaded(034, "lkfamily4") 
	and has_vehicle_recording_been_loaded(035, "lkfamily4") 
	and has_vehicle_recording_been_loaded(036, "lkfamily4") 
	and has_vehicle_recording_been_loaded(037, "lkfamily4") 
	and has_vehicle_recording_been_loaded(038, "lkfamily4") 
	and has_vehicle_recording_been_loaded(039, "lkfamily4") 
	and has_vehicle_recording_been_loaded(040, "lkfamily4") 
	and has_vehicle_recording_been_loaded(041, "lkfamily4") 
	and has_vehicle_recording_been_loaded(042, "lkfamily4") 
	and has_vehicle_recording_been_loaded(043, "lkfamily4") 
	//and has_vehicle_recording_been_loaded(044, "lkfamily4") 
	and has_vehicle_recording_been_loaded(045, "lkfamily4") 
	and has_vehicle_recording_been_loaded(046, "lkfamily4") 
	and has_vehicle_recording_been_loaded(047, "lkfamily4")
	and has_vehicle_recording_been_loaded(048, "lkfamily4")
	and has_vehicle_recording_been_loaded(049, "lkfamily4")
	and has_vehicle_recording_been_loaded(050, "lkfamily4")
	and has_vehicle_recording_been_loaded(051, "lkfamily4")
	and has_vehicle_recording_been_loaded(052, "lkfamily4")
	and has_vehicle_recording_been_loaded(053, "lkfamily4")
	and has_vehicle_recording_been_loaded(054, "lkfamily4")
	and has_vehicle_recording_been_loaded(055, "lkfamily4")
	and has_vehicle_recording_been_loaded(056, "lkfamily4")
	and has_vehicle_recording_been_loaded(057, "lkfamily4")
	and has_vehicle_recording_been_loaded(058, "lkfamily4")
	and has_vehicle_recording_been_loaded(059, "lkfamily4")
	and has_vehicle_recording_been_loaded(060, "lkfamily4")
	and has_vehicle_recording_been_loaded(061, "lkfamily4")
	and has_vehicle_recording_been_loaded(062, "lkfamily4")
	and has_vehicle_recording_been_loaded(063, "lkfamily4")
	and has_vehicle_recording_been_loaded(064, "lkfamily4")
	and has_vehicle_recording_been_loaded(065, "lkfamily4")
	and has_vehicle_recording_been_loaded(066, "lkfamily4")
	//and has_vehicle_recording_been_loaded(067, "lkfamily4")
	//and has_vehicle_recording_been_loaded(068, "lkfamily4")
	and has_vehicle_recording_been_loaded(069, "lkfamily4")
	and has_vehicle_recording_been_loaded(070, "lkfamily4")
	and has_vehicle_recording_been_loaded(071, "lkfamily4")
	and has_vehicle_recording_been_loaded(072, "lkfamily4")
	and has_vehicle_recording_been_loaded(073, "lkfamily4")
	and has_vehicle_recording_been_loaded(074, "lkfamily4")
	and has_vehicle_recording_been_loaded(075, "lkfamily4")
	and has_vehicle_recording_been_loaded(076, "lkfamily4")
	and has_vehicle_recording_been_loaded(077, "lkfamily4")
	//and has_vehicle_recording_been_loaded(078, "lkfamily4")
	//and has_vehicle_recording_been_loaded(079, "lkfamily4")
	and has_vehicle_recording_been_loaded(080, "lkfamily4")
	and has_vehicle_recording_been_loaded(081, "lkfamily4")
	and has_vehicle_recording_been_loaded(082, "lkfamily4")
	and has_vehicle_recording_been_loaded(083, "lkfamily4")
	and has_vehicle_recording_been_loaded(084, "lkfamily4")
	and has_vehicle_recording_been_loaded(085, "lkfamily4")
	and has_vehicle_recording_been_loaded(086, "lkfamily4")
	and has_vehicle_recording_been_loaded(087, "lkfamily4")
	and has_vehicle_recording_been_loaded(088, "lkfamily4")
	and has_vehicle_recording_been_loaded(089, "lkfamily4")
	and has_vehicle_recording_been_loaded(090, "lkfamily4")
	and has_vehicle_recording_been_loaded(091, "lkfamily4")
	and has_vehicle_recording_been_loaded(092, "lkfamily4")
	and has_vehicle_recording_been_loaded(093, "lkfamily4")
	and has_vehicle_recording_been_loaded(094, "lkfamily4")
	and has_vehicle_recording_been_loaded(095, "lkfamily4")
//	and has_vehicle_recording_been_loaded(096, "lkfamily4")
//	and has_vehicle_recording_been_loaded(097, "lkfamily4")
//	and has_vehicle_recording_been_loaded(098, "lkfamily4")
//	and has_vehicle_recording_been_loaded(099, "lkfamily4")
//	and has_vehicle_recording_been_loaded(100, "lkfamily4")
//	and has_vehicle_recording_been_loaded(101, "lkfamily4")
//	and has_vehicle_recording_been_loaded(102, "lkfamily4") 
//	and has_vehicle_recording_been_loaded(103, "lkfamily4")
//	and has_vehicle_recording_been_loaded(104, "lkfamily4")
//	and has_vehicle_recording_been_loaded(105, "lkfamily4")
//	and has_vehicle_recording_been_loaded(106, "lkfamily4")
//	and has_vehicle_recording_been_loaded(107, "lkfamily4")
//	and has_vehicle_recording_been_loaded(108, "lkfamily4")
//	and has_vehicle_recording_been_loaded(109, "lkfamily4")
//	and has_vehicle_recording_been_loaded(110, "lkfamily4")
//	and has_vehicle_recording_been_loaded(111, "lkfamily4")
	and has_vehicle_recording_been_loaded(112, "lkfamily4")
//	and has_vehicle_recording_been_loaded(113, "lkfamily4")
//	and has_vehicle_recording_been_loaded(114, "lkfamily4")
//	and has_vehicle_recording_been_loaded(115, "lkfamily4")
//	and has_vehicle_recording_been_loaded(116, "lkfamily4")
//	and has_vehicle_recording_been_loaded(117, "lkfamily4")
//	and has_vehicle_recording_been_loaded(118, "lkfamily4")
//	and has_vehicle_recording_been_loaded(119, "lkfamily4")
//	and has_vehicle_recording_been_loaded(120, "lkfamily4")
//	and has_vehicle_recording_been_loaded(121, "lkfamily4")
//	and has_vehicle_recording_been_loaded(122, "lkfamily4")
//	and has_vehicle_recording_been_loaded(123, "lkfamily4")
//	and has_vehicle_recording_been_loaded(124, "lkfamily4")
//	and has_vehicle_recording_been_loaded(125, "lkfamily4")
//	and has_vehicle_recording_been_loaded(126, "lkfamily4")
//	and has_vehicle_recording_been_loaded(127, "lkfamily4")
//	and has_vehicle_recording_been_loaded(128, "lkfamily4")
//	and has_vehicle_recording_been_loaded(129, "lkfamily4")
//	and has_vehicle_recording_been_loaded(130, "lkfamily4")
//	and has_vehicle_recording_been_loaded(131, "lkfamily4")
//	and has_vehicle_recording_been_loaded(132, "lkfamily4")
//	and has_vehicle_recording_been_loaded(133, "lkfamily4")
//	and has_vehicle_recording_been_loaded(134, "lkfamily4")
//	and has_vehicle_recording_been_loaded(135, "lkfamily4")
//	and has_vehicle_recording_been_loaded(136, "lkfamily4")
	and has_vehicle_recording_been_loaded(137, "lkfamily4")
//	and has_vehicle_recording_been_loaded(138, "lkfamily4")
//	and has_vehicle_recording_been_loaded(139, "lkfamily4")
//	and has_vehicle_recording_been_loaded(140, "lkfamily4")

//	and has_vehicle_recording_been_loaded(001, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(002, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(003, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(004, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(005, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(006, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(007, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(008, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(011, "lkfamily4tr")
//	and has_vehicle_recording_been_loaded(012, "lkfamily4tr")
	
	and has_vehicle_recording_been_loaded(201, "lkfamily4")
	and has_vehicle_recording_been_loaded(202, "lkfamily4")
//	and has_vehicle_recording_been_loaded(203, "lkfamily4")
//	and has_vehicle_recording_been_loaded(204, "lkfamily4")
//	and has_vehicle_recording_been_loaded(205, "lkfamily4")
//	
	and has_vehicle_recording_been_loaded(301, "lkfamily4")
	and has_vehicle_recording_been_loaded(302, "lkfamily4")
	
	and has_vehicle_recording_been_loaded(401, "lkfamily4")
	and has_vehicle_recording_been_loaded(402, "lkfamily4")
	and has_vehicle_recording_been_loaded(403, "lkfamily4")
	and has_vehicle_recording_been_loaded(404, "lkfamily4")
	and has_vehicle_recording_been_loaded(405, "lkfamily4")
	and has_vehicle_recording_been_loaded(406, "lkfamily4")
	
		return true 
		
	endif 
	
	return false 

endfunc  

func bool create_vehicles_outside_stadium()

	int i = 0

	if not does_entity_exist(truck.veh)
	
		switch create_vehicles_outside_stadium_status
		
			case 0
	
				if get_distance_between_coords(get_entity_coords(player_ped_id()), truck.pos) < 550.00  

					request_model(truck.model)
					set_vehicle_model_is_suppressed(truck.model, true)
						
					request_model(trailer.model)
					set_vehicle_model_is_suppressed(trailer.model, true)

					request_model(lazlows_car.model)
					set_vehicle_model_is_suppressed(lazlows_car.model, true)

					if has_model_loaded(truck.model)
					and has_model_loaded(trailer.model)
					and has_model_loaded(lazlows_car.model)
					
						create_vehicles_outside_stadium_status++
						
					endif 
					
				endif 
				
			break 
			
			case 1
			
				request_model(truck.model)
				set_vehicle_model_is_suppressed(truck.model, true)
					
				request_model(trailer.model)
				set_vehicle_model_is_suppressed(trailer.model, true)

				request_model(lazlows_car.model)
				set_vehicle_model_is_suppressed(lazlows_car.model, true)
			
				request_model(sentinel)
				request_model(schafter2)

				if has_model_loaded(truck.model)
				and has_model_loaded(trailer.model)
				and has_model_loaded(lazlows_car.model)
				and has_model_loaded(sentinel)
				and has_model_loaded(schafter2)

					clear_area(truck.pos, 10.0, true)
					truck.veh = create_vehicle(truck.model, truck.pos, truck.heading)
					//SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
					set_vehicle_doors_locked(truck.veh, vehiclelock_locked)
					set_vehicle_tyres_can_burst(truck.veh, false)
					set_vehicle_colours(truck.veh, 0, 0)
					set_entity_health(truck.veh, 1500)
					set_vehicle_engine_health(truck.veh, 1500)
					set_vehicle_petrol_tank_health(truck.veh, 1500)
					//set_vehicle_dirt_level(truck.veh, 0.0)
					set_vehicle_strong(truck.veh, true)
					SET_VEHICLE_HAS_STRONG_AXLES(truck.veh, true)
					
					clear_area(trailer.pos, 10.0, true)
					trailer.veh = create_vehicle(trailer.model, trailer.pos, trailer.heading)
					set_vehicle_tyres_can_burst(trailer.veh, false)
					attach_vehicle_to_trailer(truck.veh, trailer.veh)
					//DISABLE_TRAILER_BREAKING_FROM_VEHICLE(trailer.veh, true)

					lazlows_car.veh = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car.heading)
					set_vehicle_strong(lazlows_car.veh, true)
					SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
					SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
					set_vehicle_tyres_can_burst(lazlows_car.veh, false)
					set_entity_only_damaged_by_player(lazlows_car.veh, true)
					set_vehicle_doors_locked(lazlows_car.veh, vehiclelock_lockout_player_only) 
					set_vehicle_colours(lazlows_car.veh, 27, 0)
					//SET_ENTITY_LOAD_COLLISION_FLAG(lazlows_car.veh, true)
					SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
					SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)

					
					//truck 0 is created in TRUCK_DRIVER_AI_SYSTEM_2
					for i = 1 to count_of(parked_truck) - 1
						clear_area(parked_truck[i].pos, 10.0, true)
						parked_truck[i].veh = create_vehicle(parked_truck[i].model, parked_truck[i].pos, parked_truck[i].heading)
						set_vehicle_doors_locked(parked_truck[i].veh, vehiclelock_locked)
						set_vehicle_colours(parked_truck[i].veh, (10 * i), 0)
						parked_trailer[i].veh = create_vehicle(parked_trailer[i].model, parked_trailer[i].pos, parked_trailer[i].heading)
						attach_vehicle_to_trailer(parked_truck[i].veh, parked_trailer[i].veh)
				//		set_vehicle_as_no_longer_needed(parked_truck[i].veh)
				//		set_vehicle_as_no_longer_needed(parked_trailer[i].veh)
					endfor 
					
					clear_area(<<-210.2187, -2015.1078, 27.1112>>, 4.0, true)
					ambient_car = create_vehicle(schafter2, <<-209.6465, -2015.4807, 26.6206>>)
					set_entity_heading(ambient_car, 73.3412) 
					set_vehicle_colours(ambient_car, 64, 0) 
					
					clear_area(<<-210.8506, -2018.3702, 26.6207>>, 4.0, true)
					ambient_car_2 = create_vehicle(schafter2, <<-210.8506, -2018.3702, 26.6207>>)
					set_entity_heading(ambient_car_2, 70.9631) 
					
					clear_area(<<-206.0113, -2055.3301, 26.6199>>, 4.0, true)
					ambient_car_3 = create_vehicle(sentinel, <<-206.0113, -2055.3301, 26.6199>>)
					set_entity_heading(ambient_car_3, 53.3779)
					
					clear_area(<<-199.4559, -2045.2799, 26.6199>>, 4.0, true)
					ambient_car_4 = create_vehicle(sentinel, <<-199.4559, -2045.2799, 26.6199>>)
					set_entity_heading(ambient_car_4, 59.9934)
					
					clear_area(<<-218.2066, -2070.0325, 26.6199>>, 4.0, true)
					ambient_car_5 = create_vehicle(sentinel, <<-218.2066, -2070.0325, 26.6199>>)
					set_entity_heading(ambient_car_5, 45.2218)
					
					create_vehicles_outside_stadium_status++
				
					return true  
					
				endif 
				
			break 

		endswitch 
	
	else 
		
		return true 

	endif 
	
	return false

endfunc  



func bool create_entities_outside_stadium(bool create_truck_driver = true)

	int i = 0
	
	switch create_entities_outside_stadium_status 
	
		case 0

			if not does_entity_exist(truck.veh)

				request_model(truck.model)
				set_vehicle_model_is_suppressed(truck.model, true)
					
				request_model(trailer.model)
				set_vehicle_model_is_suppressed(trailer.model, true)

				request_model(lazlows_car.model)
				set_vehicle_model_is_suppressed(lazlows_car.model, true)

				request_model(sentinel)
				request_model(schafter2)
				request_model(manana)
				
				if create_truck_driver 
					request_model(truck_driver.model)
				endif 
				
				request_anim_dict("missfam4")
				
				request_vehicle_recording(001, "lkfamily4") 
				request_vehicle_recording(112, "lkfamily4")

				REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FAM4_UNHITCH_TRAILER")
			
				REQUEST_PTFX_ASSET()

				if has_model_loaded(truck.model)
				and has_model_loaded(trailer.model)
				and has_model_loaded(lazlows_car.model)
				and has_model_loaded(sentinel)
				and has_model_loaded(schafter2)
				and has_model_loaded(manana)
				and (not create_truck_driver or has_model_loaded(truck_driver.model))
				and has_anim_dict_loaded("missfam4")
				and has_vehicle_recording_been_loaded(001, "lkfamily4") 
				and has_vehicle_recording_been_loaded(112, "lkfamily4")
				and REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FAM4_UNHITCH_TRAILER")
				and has_ptfx_asset_loaded()

					clear_area(truck.pos, 10.0, true)
					truck.veh = create_vehicle(truck.model, truck.pos, truck.heading)
					SET_FORCE_HD_VEHICLE(truck.veh, true)
					set_vehicle_on_ground_properly(truck.veh)	
					SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
					set_vehicle_tyres_can_burst(truck.veh, false)
					set_vehicle_colours(truck.veh, 0, 0)
					set_entity_health(truck.veh, 1500)
					set_vehicle_engine_health(truck.veh, 1500)
					set_vehicle_petrol_tank_health(truck.veh, 1500)
					//set_vehicle_dirt_level(truck.veh, 0.0)
					set_vehicle_strong(truck.veh, true)
					SET_VEHICLE_HAS_STRONG_AXLES(truck.veh, true)
					//set_vehicle_radio_enabled(truck.veh, false)
					SET_RADIO_TO_STATION_NAME("off")

					
					clear_area(trailer.pos, 10.0, true)
					trailer.veh = create_vehicle(trailer.model, trailer.pos, trailer.heading)
					set_vehicle_on_ground_properly(trailer.veh)
					set_vehicle_tyres_can_burst(trailer.veh, false)
					attach_vehicle_to_trailer(truck.veh, trailer.veh)
					//DISABLE_TRAILER_BREAKING_FROM_VEHICLE(trailer.veh, true)
					SET_TRAILER_LEGS_RAISED(trailer.veh)
					activate_physics(truck.veh)
					activate_physics(trailer.veh)
					
					if create_truck_driver
						truck_driver.ped = create_ped_inside_vehicle(truck.veh, pedtype_mission, truck_driver.model)
						set_model_as_no_longer_needed(truck_driver.model)
						set_blocking_of_non_temporary_events(truck_driver.ped, true)
						STOP_PED_SPEAKING(truck_driver.ped, true)
						add_ped_for_dialogue(scripted_speech, 8, truck_driver.ped, "truckdriver")
					endif 
				
					clear_area(lazlows_car.pos, 10.0, true)
					lazlows_car.veh = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car.heading)
					set_vehicle_strong(lazlows_car.veh, true)
					SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
					SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
					set_vehicle_tyres_can_burst(lazlows_car.veh, false)
					set_entity_only_damaged_by_player(lazlows_car.veh, true)
					set_entity_proofs(lazlows_car.veh, false, true, true, true, true)
					set_vehicle_strong(lazlows_car.veh, true)
					SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
					set_vehicle_doors_locked(lazlows_car.veh, vehiclelock_lockout_player_only) 
					set_vehicle_colours(lazlows_car.veh, 27, 0)
					//SET_ENTITY_LOAD_COLLISION_FLAG(lazlows_car.veh, true)
					SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
					SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(lazlows_car.veh, "FAMILY_4_LAZLOWS_CAR")

					for i = 1 to count_of(parked_truck) - 1
						clear_area(parked_truck[i].pos, 10.0, true)
						parked_truck[i].veh = create_vehicle(parked_truck[i].model, parked_truck[i].pos, parked_truck[i].heading)
						set_vehicle_doors_locked(parked_truck[i].veh, vehiclelock_locked)
						set_vehicle_colours(parked_truck[i].veh, (10 * i), 0)
						parked_trailer[i].veh = create_vehicle(parked_trailer[i].model, parked_trailer[i].pos, parked_trailer[i].heading)
						attach_vehicle_to_trailer(parked_truck[i].veh, parked_trailer[i].veh)
				//		set_vehicle_as_no_longer_needed(parked_truck[i].veh)
				//		set_vehicle_as_no_longer_needed(parked_trailer[i].veh)
					endfor 
					
					clear_area(<<209.64, -2015.48, 26.6>>, 10.0, true)
					ambient_car = create_vehicle(schafter2, <<-209.6465, -2015.4807, 26.6206>>)
					set_entity_heading(ambient_car, 73.3412) 
					set_vehicle_colours(ambient_car, 64, 0) 
					
					clear_area(<<-210.8506, -2018.3702, 26.6207>>, 10.0, true)
					ambient_car_2 = create_vehicle(schafter2, <<-210.8506, -2018.3702, 26.6207>>)
					set_entity_heading(ambient_car_2, 70.9631) 
					
					clear_area(<<-206.0113, -2055.3301, 26.6199>>, 10.0, true)
					ambient_car_3 = create_vehicle(sentinel, <<-206.0113, -2055.3301, 26.6199>>)
					set_entity_heading(ambient_car_3, 53.3779)
					
					clear_area(<<-199.4559, -2045.2799, 26.6199>>, 10.0, true)
					ambient_car_4 = create_vehicle(sentinel, <<-199.4559, -2045.2799, 26.6199>>)
					set_entity_heading(ambient_car_4, 59.9934)
					
					clear_area(<<-218.2066, -2070.0325, 26.6199>>, 10.0, true)
					ambient_car_5 = create_vehicle(sentinel, <<-218.2066, -2070.0325, 26.6199>>)
					set_entity_heading(ambient_car_5, 45.2218)
					
					create_entities_outside_stadium_status++
					
				endif 
				
			endif 
			
		break 
		
		case 1
		
			load_mission_vehicle_recordings()
			
			if has_mission_vehicle_recordings_loaded()
			
				create_entities_outside_stadium_status++
			
				return true 
				
			endif 
			
		break 
		
		case 2
		
			return true 
		
		break 
		
	endswitch 
	
	return false 
	
endfunc

proc michael_ai_system()

//	int player_node
//	int buddy_node
//	int node_diff

	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
	
		set_ped_reset_flag(selector_ped.pedID[selector_ped_michael], PRF_UseProbeSlopeStairsDetection, true)

		switch michael_ai_system_status 
		
			case 0

//				open_sequence_task(seq)
//					TASK_FORCE_MOTION_STATE(null, enum_to_int(MS_ON_FOOT_RUN))
//					TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 8, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
//				close_sequence_task(seq)
//				task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
//				clear_sequence_task(seq)
//				
//				force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_trevor])
				
				michael_ai_system_status++

			break 
			
			case 1

				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[selector_ped_michael])
					if is_entity_in_angled_area(selector_ped.pedID[selector_ped_michael], <<-244.914, -2007.479, 23.691>>, <<-244.325, -2004.946, 26.191>>, 4.300)     
					or not is_ped_in_specific_room(player_ped_id(), <<-249.8176, -2008.0590, 29.1458>>, "GtaMloRoom001")
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(selector_ped.pedID[selector_ped_michael], 2.0)
					else
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(selector_ped.pedID[selector_ped_michael], 3.0) 
					endif 
				endif 
				
				//int buddy_node 
				
				vector run_to_pos
				
				//if has_ped_task_finished_2(selector_ped.pedID[selector_ped_michael])
				//if waypoint_recording_get_closest_waypoint("family4_1", get_entity_coords(selector_ped.pedID[selector_ped_michael]), buddy_node)  
				waypoint_recording_get_coord("family4_1", 55, run_to_pos)  
				
				if is_entity_at_coord(selector_ped.pedID[selector_ped_michael], run_to_pos, <<1.5, 1.5, 2.0>>)

					if is_ped_inside_interior(player_ped_id(), <<-260.7652, -2026.1630, 29.1458>>)
						
						open_sequence_task(seq)
							task_turn_ped_to_face_entity(null, player_ped_id())
						close_sequence_task(seq)
						task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
						clear_sequence_task(seq)

						michael_ai_system_status = 2
						
					else 

						SET_PED_CONFIG_FLAG(selector_ped.pedID[selector_ped_michael], PCF_ForceDirectEntry, TRUE)
						
						open_sequence_task(seq)
							task_enter_vehicle(null, truck.veh, -1, vs_front_right, pedmoveblendratio_run, ecf_jack_anyone)
						close_sequence_task(seq)
						task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
						clear_sequence_task(seq)
						
						michael_ai_system_status = 3
						
					endif 

				endif 

			break 
			
			case 2
			
				if is_ped_inside_interior(player_ped_id(), <<-260.7652, -2026.1630, 29.1458>>)
				//and not is_entity_in_angled_area(player_ped_id(), <<-251.653, -2019.017, 29.146>>, <<-261.255, -2030.541, 32.146>>, 5.5)

					if is_ped_facing_ped(selector_ped.pedID[selector_ped_michael], player_ped_id(), 45)
						if has_anim_dict_loaded("missfam4")
							if lk_timer(original_time, 8000)

								if is_ped_in_specific_room(player_ped_id(), <<-249.8176, -2008.0590, 29.1458>>, "GtaMloRoom001")
									create_conversation(scripted_speech, "fam4Aud", "FAM4_LAG", conv_priority_low)
								endif 

								open_sequence_task(seq)
									task_play_anim(null, "missfam4", "say_hurry_up_a_trevor", normal_blend_in, slow_blend_out)
								close_sequence_task(seq)
								task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
								clear_sequence_task(seq)
								
								original_time = get_game_timer()
								
							endif 
							
						endif 
						
					else 
						
						if has_ped_task_finished_2(selector_ped.pedID[selector_ped_michael], SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
					
							open_sequence_task(seq)
								task_turn_ped_to_face_entity(null, player_ped_id())
							close_sequence_task(seq)
							task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
							clear_sequence_task(seq)
							
						endif 
						
					endif 
					
				else 
				
					SET_PED_CONFIG_FLAG(selector_ped.pedID[selector_ped_michael], PCF_ForceDirectEntry, TRUE)
						
					open_sequence_task(seq)
						task_enter_vehicle(null, truck.veh, -1, vs_front_right, pedmoveblendratio_run, ecf_jack_anyone)
					close_sequence_task(seq)
					task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
					clear_sequence_task(seq)
					
					michael_ai_system_status = 3

				endif 
			
			break 
			
			case 3
			
				if not is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh)
			
					if not does_blip_exist(michaels_blip)
						michaels_blip = create_blip_for_entity(selector_ped.pedID[selector_ped_michael])
					endif 
					
					if has_ped_task_finished_2(selector_ped.pedID[selector_ped_michael])
				
						open_sequence_task(seq)
							task_enter_vehicle(null, truck.veh, -1, vs_front_right)
						close_sequence_task(seq)
						task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
						clear_sequence_task(seq)
				
					endif 
				
				else 
				
					if does_blip_exist(michaels_blip)
						remove_blip(michaels_blip)
					endif 	
				
				endif 
			
			break 
			
			case 4
			
			break 
			
		endswitch 

	endif 

endproc 

proc tracey_ai_system()

	if not is_ped_injured(tracey.ped)

		switch tracey_ai_system_status 
		
			case 0
			
				if has_anim_dict_loaded("missfam4leadinoutmcs2")
			
					scene_pos = <<-234.834, -1999.252, 24.68>>
					scene_rot = <<0.000, 0.000, -127.000>>

					tracey.scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)
					TASK_SYNCHRONIZED_SCENE(tracey.ped, tracey.scene_id, "missfam4leadinoutmcs2", "tracy_loop", INSTANT_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					set_synchronized_scene_looped(tracey.scene_id, true)
					force_ped_ai_and_animation_update(tracey.ped, true)
					SET_RAGDOLL_BLOCKING_FLAGS(tracey.ped, RBF_PLAYER_IMPACT)
					
					tracey_ai_system_status++ 
				endif 
			
			break 
		
			case 1
			
				if not has_label_been_triggered("FAM4_CRIES")
					if not is_any_text_being_displayed(locates_data)
						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(tracey.ped)) < 5.0
							if create_conversation(scripted_speech, "fam4aud", "FAM4_CRIES", conv_priority_low)
								set_label_as_triggered("FAM4_CRIES", true)
							endif 
						endif 
					endif 
				endif 
				
				if is_synchronized_scene_running(tracey.scene_id)
				and not is_ped_ragdoll(tracey.ped)
				and not IS_PED_DEAD_OR_DYING(tracey.ped)
				
					if has_ped_task_finished_2(tracey.ped)
				
						open_sequence_task(seq)
							task_turn_ped_to_face_entity(null, player_ped_id())
							task_cower(null)
						close_sequence_task(seq)
						task_perform_sequence(tracey.ped, seq)
						clear_sequence_task(seq)
						
					endif 
				
				endif 
			
			break 
			
			case 2
			
			break 
			
		endswitch 
		
	endif 

endproc 

proc run_after_lazlow_switch_effect()
	
	switch run_after_lazlow_switch_effect_status 
	
		case 0
		
			if trigger_switch_effect_to_trevor 
				if is_screen_faded_in()
				
					if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) != cam_view_mode_first_person

						ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
					
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
				
						run_after_lazlow_switch_effect_status++
					else 
					
						run_after_lazlow_switch_effect_status++
					
					endif 
					
				endif 
			endif 
		
		break 
		
		case 1
		
		break 
		
	endswitch 

endproc 

proc create_stadium_assets()

	int i = 0

	//puts peds into the state for chasing lazlow. Incase cutsecne was skipped.
	for i = 0 to count_of(ambient_ped_status) - 1
		ambient_ped_status[i] = play_reaction_chasing_lazlow
	endfor 
	
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, true)
	
	michaels_blip = create_blip_for_entity(selector_ped.pedID[selector_ped_michael])
	setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
	setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)

	clear_player_wanted_level(player_id())
	set_max_wanted_level(0)
	set_create_random_cops(false)
	
	if not is_ped_injured(tracey.ped)
		setup_buddy_attributes(tracey.ped)
		setup_relationship_contact(tracey.ped, true)
		set_ped_can_ragdoll(tracey.ped, true)
		add_ped_for_dialogue(scripted_speech, 4, tracey.ped, "tracey")
		tracey_ai_system()
	endif 
	

	DELETE_ALL_TRAINS() 
	SET_RANDOM_TRAINS(false) 

	set_vehicle_model_is_suppressed(utillitruck, true)
	set_vehicle_model_is_suppressed(utillitruck2, true)
	set_vehicle_model_is_suppressed(utillitruck3, true)
	
	run_after_lazlow_switch_effect()

	if is_screen_faded_out()
		IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_FAR
			end_cutscene(false, 0.0, 0, false)
		else 
			end_cutscene(false, -60.0, 0, false)
		endif 
	else 
		IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_FAR
			end_cutscene_no_fade(false, true, false, 0.0, 0, 3000, false)
		else 
			end_cutscene_no_fade(false, true, false, -60.0, 0, 3000, false)
		endif 
	endif 
	
	printstring("run after lazlow 2")
	printnl()
	
	if not has_label_been_triggered("FAM4_MISSION_START")
		trigger_music_event("FAM4_MISSION_START")
		set_label_as_triggered("FAM4_MISSION_START", true)
	endif 
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "run after lazlow")
	
	mission_flow = run_after_lazlow
	
endproc

proc taxi_drop_off_for_get_to_the_stadium()
			
	if does_blip_exist(locates_data.LocationBlip)
		if not taxi_drop_off_set_for_get_to_the_stadium 
			SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(locates_data.LocationBlip, <<-210.9444, -2041.7289, 26.6206>>, 100.8531)
			taxi_drop_off_set_for_get_to_the_stadium = true
		endif 
	else 
		taxi_drop_off_set_for_get_to_the_stadium = false 
	endif 
	
endproc 

proc reset_dialogue_for_future_resumption_data()

	resume_lower_priority_conversation = false
	dialogue_root = "null"
	specific_label = "null"
	
endproc 

//PURPOSE:
//obtains the lower priority dialogue data before a higher priority conversation overwrites the lower conversation.
//Must be called every frame whilst getting ready to trigger higher conversation. 
//only needed when you have 2 priority conversation levels which can clash. e.g. medium and low.
proc get_dialogue_data_for_future_resumption() 

	//if not obtain_dialogue_data
	
		if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		//if IS_SCRIPTED_CONVERSATION_ONGOING()
		
			if not resume_lower_priority_conversation

				//returns the string NULL by default
				dialogue_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				//dialogue_root = GET_LAST_PASSED_STANDARD_CONVERSATION_ROOT()
				specific_label = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
				
				if not are_strings_equal(dialogue_root, "null")
				and not are_strings_equal(specific_label, "null")
					resume_lower_priority_conversation = true
				endif 

				printstring("stored dialogue root = ")
				printstring(dialogue_root)
				printnl()
				
				printstring("stored specific_label = ")
				printstring(specific_label)
				printnl()
				
			endif 
			
			//script_assert("store label")
			
		else 
		
			printstring("family 4 - no conversation ongoing")
			printnl()
		
		endif 
		
		//obtain_dialogue_data = true 
		
	//endif
	
endproc 

//PURPOSE:
//Checks to see if a specific conversation route should be played regardless is the player is out of the truck.
//i.e. the target_blip does not exist
//This will stop the dialogue monitoring system from pausing that specific conversation.
func bool is_conversation_root_part_of_force_trigger_list
			
	text_label root

	if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
		root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

		if are_strings_equal(root, "FAM4_POW") 
		or are_strings_equal(root, "FAM4_POW2")
		
			return true 
			
		endif 
		
	endif 
	
	return false 
	
endfunc 


func bool allow_dialogue_to_trigger(blip_index target_blip, ped_index &buddy_ped)

	if does_blip_exist(target_blip)
		if not is_ped_injured(buddy_ped)
			if is_ped_in_same_vehicle_as_player(buddy_ped) 
			or (is_ped_on_foot(player_ped_id()) and is_ped_on_foot(buddy_ped))
				if not is_calling_any_contact()
				and not is_player_changing_clothes()
		
					return true 
					
				endif 
				
			endif 
			
		endif 
		
	endif 
	
	return false
	
endfunc 



//Purpose:
//Will pause a conversation when the target blip does not exist. e.g. the player has left the vehicle or ran away from the buddy
//In the event where you want to play a specific line when you leave the car etc then DONT USE THIS and do something 
//specific - see dialogue example in lawrence_gta5_sdk - (bottom of file) 
func bool dialogue_monitoring_system(blip_index target_blip, ped_index &buddy_ped)

//	if does_blip_exist(target_blip)
//	and (is_ped_in_same_vehicle_as_player(buddy_ped) or (is_ped_on_foot(player_ped_id()) and is_ped_on_foot(buddy_ped)))
	if allow_dialogue_to_trigger(target_blip, buddy_ped)
	
		if IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			PAUSE_FACE_TO_FACE_CONVERSATION(false)
		endif
		
		if resume_lower_priority_conversation //dialogue_paused
			if not is_any_text_being_displayed(locates_data)
			//and not lazlow_chase_cam_active
				
				printstring("dialogue_root = ")
				printstring(dialogue_root)
				printnl()
				
				
				printstring("specific_label = ")
				printstring(specific_label)
				printnl()
				
				if create_conversation_from_specific_line(scripted_speech, "fam4aud", dialogue_root, specific_label, CONV_PRIORITY_LOW) 
					resume_lower_priority_conversation = false
				endif 
			endif 
		endif 

		if not is_any_text_being_displayed(locates_data)
		and not resume_lower_priority_conversation
		//and not lazlow_chase_cam_active
			return true 
		endif 
		
	else 
	
		if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
			if not is_conversation_root_part_of_force_trigger_list()
				
				//RESUME_INTERRUPT_CONVERSATION(false)
				
				if not IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					
					PAUSE_FACE_TO_FACE_CONVERSATION(true)
					
					if is_calling_any_contact()
						get_dialogue_data_for_future_resumption() 
					endif 
					
				endif 
			endif 
		endif 
	
	endif 
	
	return false 

endfunc 


proc get_to_the_stadium_dialogue_system()

	switch get_to_the_stadium_dialogue_system_status 
	
		case 0

//			if create_conversation(scripted_speech, "fam4Aud", "FAM4_GO", conv_priority_medium)
				get_to_the_stadium_dialogue_system_status++
//			endif 
		
		break 
		
		case 1
		
			//ensures the locate header god text triggers before the dialogue 
			if not is_any_text_being_displayed(locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				get_to_the_stadium_dialogue_system_status++
			endif 
		
		break 
		
		case 2

			if not is_any_text_being_displayed(locates_data)

				if dialogue_monitoring_system(locates_data.LocationBlip, selector_ped.pedID[selector_ped_michael])
				or dialogue_monitoring_system(locates_data.LocationBlip, selector_ped.pedID[selector_ped_trevor])
					
					switch get_random_int_in_range(0, 2)
					
						case 0
					
							if create_conversation(scripted_speech, "fam4Aud", "FAM4_B1A", conv_priority_low)
								get_to_the_stadium_dialogue_system_status++
							endif 
							
						break 
						
						case 1
						
							if create_conversation(scripted_speech, "fam4Aud", "FAM4_B1A2", conv_priority_low)
								get_to_the_stadium_dialogue_system_status++
							endif 
							
						break 
						
					endswitch 
				endif 
			endif 

		break 
		
		case 3
		
			switch get_current_player_ped_enum()

				case char_michael
		
					if dialogue_monitoring_system(locates_data.LocationBlip, selector_ped.pedID[selector_ped_trevor])
						get_to_the_stadium_dialogue_system_status = 4
					endif 

				break 
				
				case char_trevor
				
					if dialogue_monitoring_system(locates_data.LocationBlip, selector_ped.pedID[selector_ped_michael])
						get_to_the_stadium_dialogue_system_status = 4
					endif 
				
				break 
				
			endswitch 
		
		break 
		
		case 4
		
			if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-219.5123, -2037.8512, 26.9>>) > 400
		
				if lk_timer(dialogue_time, 30000)
				
					switch get_current_player_ped_enum() 
				
						case char_michael
						
							if create_conversation(scripted_speech, "fam4Aud", "FAM4_HURRY", conv_priority_low) 
								dialogue_time = get_game_timer()
							endif 
							
						break 

						case char_trevor
						
							if create_conversation(scripted_speech, "fam4Aud", "FAM4_HURRY2", conv_priority_low) 
								dialogue_time = get_game_timer()
							endif 
						
						break 
						
					endswitch 
					
				endif 
				
			endif 
			
			if not has_label_been_triggered("FAM4_DRV2")
				if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-219.5123, -2037.8512, 26.9>>) < 50.00
					
					switch get_current_player_ped_enum() 
				
						case char_michael
					
							if dialogue_monitoring_system(locates_data.LocationBlip, selector_ped.pedID[selector_ped_trevor])
							
								if create_conversation(scripted_speech, "fam4Aud", "FAM4_DRV2", conv_priority_medium)
									
									REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_HIGH)
									
									set_label_as_triggered("FAM4_DRV2", true)
								endif 

							endif 
							
						break 
						
						case char_trevor
						
							if dialogue_monitoring_system(locates_data.LocationBlip, selector_ped.pedID[selector_ped_michael])
							
								if create_conversation(scripted_speech, "fam4Aud", "FAM4_DRV2", conv_priority_medium)
								
									REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_HIGH)
								
									set_label_as_triggered("FAM4_DRV2", true)
								endif 
							
							endif 
						
						break 
						
					endswitch 
					
				endif 
			endif 

		break 
		
		case 5
		
			switch get_current_player_ped_enum()
			
				case char_michael
				
					if not is_ped_sitting_in_any_vehicle(selector_ped.pedID[selector_ped_trevor])
				
						if not is_any_text_being_displayed(locates_data)
			
							if create_conversation(scripted_speech, "fam4Aud", "walk_stad0", conv_priority_medium)
								
								REPLAY_RECORD_BACK_FOR_TIME(6.0, 6.0, REPLAY_IMPORTANCE_HIGH)
								
								get_to_the_stadium_dialogue_system_status = 7
							endif 
							
						endif 
					endif 
						
				break 
				
				case char_trevor
				
					if not is_ped_sitting_in_any_vehicle(player_ped_id())

						if not is_any_text_being_displayed(locates_data)
					
							if create_conversation(scripted_speech, "fam4Aud", "walk_stad0", conv_priority_medium)
								
								REPLAY_RECORD_BACK_FOR_TIME(6.0, 6.0, REPLAY_IMPORTANCE_HIGH)
								
								get_to_the_stadium_dialogue_system_status = 7
							endif 
							
						endif
					endif 
					
				break 
			
			endswitch  
		
		break 
		
		case 6
		
			switch get_current_player_ped_enum()
			
				case char_michael
				
					if not is_ped_sitting_in_any_vehicle(selector_ped.pedID[selector_ped_trevor])
				
						if not is_any_text_being_displayed(locates_data)
			
							if create_conversation(scripted_speech, "fam4Aud", "walk_stad1", conv_priority_medium)
								get_to_the_stadium_dialogue_system_status = 7
							endif 
							
						endif 
					endif 
						
				break 
				
				case char_trevor
				
					if not is_ped_sitting_in_any_vehicle(player_ped_id())

						if not is_any_text_being_displayed(locates_data)
					
							if create_conversation(scripted_speech, "fam4Aud", "walk_stad1", conv_priority_medium)
								get_to_the_stadium_dialogue_system_status = 7
							endif 
							
						endif
					endif 
					
				break 
			
			endswitch  
		
		break 
		
		case 7
		
			if create_conversation(scripted_speech, "fam4Aud", "FAM4_VIP", conv_priority_medium)
				get_to_the_stadium_dialogue_system_status++
			endif 
		
		break 
		
		case 8
		
			switch get_current_player_ped_enum()
			
				case char_michael
				
					if dialogue_monitoring_system(locates_data.LocationBlip, selector_ped.pedID[selector_ped_trevor]) 
				
						print_now("family4_god_13", default_god_text_time, 1)
						
						get_to_the_stadium_dialogue_system_status++
						
					endif 
				
				break 
				
				case char_trevor
				
					if dialogue_monitoring_system(locates_data.LocationBlip, selector_ped.pedID[selector_ped_michael])
						
						print_now("family4_god_13", default_god_text_time, 1)
						
						get_to_the_stadium_dialogue_system_status++
						

					endif 
				
				break 
				
			endswitch 
		
		break 
		
		case 9
		
		break 
		
	endswitch 
	
endproc 

proc deactivate_lazlow_chase_cam()
	
	//if lazlow_chase_cam_active
	if is_cam_active(lazlow_cam)
		
		set_cam_active(lazlow_cam_2, false)
		set_cam_active(lazlow_cam, false)
		render_script_cams(false, false)
		
		//SET_CINEMATIC_BUTTON_ACTIVE(true)

		if not is_ped_injured(lazlow.ped)
			task_clear_look_at(lazlow.ped)
		endif 

		//if not dialogue_paused
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

		lazlow_chase_cam_active = false	
		
		kill_chase_hint_cam(chase_hint_cam) //resets the hint variables.
		
		if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_CAMERA")
			stop_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_CAMERA")
		endif 
		
		//lazlow_cam_time = 0
		
	endif

endproc 

proc lazlow_camera_system()

	if is_vehicle_driveable(lazlows_car.veh) 
		if not does_cam_exist(lazlow_cam)
		
			lazlow_cam = create_cam("default_scripted_camera", false)
			set_cam_fov(lazlow_cam, 45.00)
			
			lazlow_cam_2 = create_cam("default_scripted_camera", false)
			set_cam_fov(lazlow_cam_2, 40.00)

			set_cam_active(lazlow_cam, true)
					
			lazlow_chase_cam_active = false		

		else
//
//				if not help_text_on
//					//if not is_this_print_being_displayed("GOD_8")
//					if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
//						//if (is_entity_in_angled_area(player_ped_id(), <<-162.352, -2003.968, 21.899>>, <<-145.830, -2005.703, 32.879>>, 75.00) or get_distance_between_coords(get_entity_coords(player_ped_id()), <<-199.8088, -2003.7095, 26.6207>>) > 150.00)
//						//if lk_timer(cam_help_time, 2000)
//						if is_entity_in_angled_area(player_ped_id(), <<-264.563, -1828.329, 27.333>>, <<-238.324, -1817.461, 36.033>>, 41.000)     
//						or lk_timer(cam_help_time, 30000)
//							if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 250.00
//								cam_help_time = get_game_timer()
//								print_help("family4_help_0")
//								help_text_on = true 
//							endif 
//						endif
//					endif 
//					
//				else 
//					
//					if not reminder_help_played
//						if lk_timer(cam_help_time, 15000)
//							if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
//								if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 250.00
//									cam_help_time = get_game_timer()
//									print_help("family4_help_0")
//									reminder_help_played = true
//								endif 
//							endif 
//						endif 
//					endif 
//				endif 	
//		
//				// check for button press and suitable conditions			
				
			if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 120.00)
				if activate_vehicle_tracking_cam(lazlows_car.veh)
					
					if not is_cam_rendering(lazlow_cam)
					and not is_cam_rendering(lazlow_cam_2)
				
						if is_entity_in_angled_area(truck.veh, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-10.0, -22.0, -10.0>>), get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-0.4, -22.0, 10.0>>), 55.00, false, true)
							
							attach_cam_to_entity(lazlow_cam, lazlows_car.veh, <<-0.68, 1.580, 0.59>>)
							POINT_CAM_AT_ENTITY(lazlow_cam, lazlows_car.veh, <<2.5, -4.9, 0.2>>)
							attach_cam_to_entity(lazlow_cam_2, lazlows_car.veh, <<-1.5, 1.580, 0.59>>)
							POINT_CAM_AT_ENTITY(lazlow_cam_2, truck.veh, <<0.0, 0.0, 0.0>>)
							
							task_look_at_coord(lazlow.ped, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-1.5, -10.0, 0.7>>), 2000)

						elif is_entity_in_angled_area(truck.veh, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<1.4, -22.0, -10.0>>), get_offset_from_entity_in_world_coords(lazlows_car.veh, <<10.0, -22.0, 10.0>>), 55.00, false, true)

							attach_cam_to_entity(lazlow_cam, lazlows_car.veh, <<0.68, 1.580, 0.590>>)
							POINT_CAM_AT_ENTITY(lazlow_cam, lazlows_car.veh, <<-2.1, -4.9, 0.2>>)
							attach_cam_to_entity(lazlow_cam_2, lazlows_car.veh, <<1.5, 1.580, 0.59>>)
							POINT_CAM_AT_ENTITY(lazlow_cam_2, truck.veh, <<0.0, 0.0, 0.0>>)
							
							task_look_at_coord(lazlow.ped, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<1.5, -10.0, 0.7>>), 2000)
							
						else 
						
							attach_cam_to_entity(lazlow_cam, lazlows_car.veh, <<-0.68, 1.580, 0.59>>)
							POINT_CAM_AT_ENTITY(lazlow_cam, lazlows_car.veh, <<2.5, -4.9, 0.2>>)
							attach_cam_to_entity(lazlow_cam_2, lazlows_car.veh, <<-1.5, 1.580, 0.59>>)
							POINT_CAM_AT_ENTITY(lazlow_cam_2, truck.veh, <<0.0, 0.0, 0.0>>)
							
							task_look_at_coord(lazlow.ped, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-1.5, -10.0, 0.7>>), 2000)
							
						endif 

						get_dialogue_data_for_future_resumption()
						
						//create_conversation(scripted_speech, "fam4aud", "lazlo_chase", CONV_PRIORITY_medium)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						lazlow_dialogue_time = 0
								
						set_cam_active(lazlow_cam, true)
						set_cam_active_with_interp(lazlow_cam_2, lazlow_cam, 2900, graph_type_linear)//, graph_type_linear)
						
						shake_cam(lazlow_cam, "ROAD_VIBRATION_SHAKE", 0.2)
						shake_cam(lazlow_cam_2, "ROAD_VIBRATION_SHAKE", 0.2)
						
						render_script_cams(true, false)
						lazlow_chase_cam_active = true
						
						// update flag to show its been used so dont need reminder
						reminder_help_played = true
						
						INFORM_MISSION_STATS_OF_INCREMENT(FAM4_LAZLOW_CAM_USE) 
						
						SET_CINEMATIC_BUTTON_ACTIVE(false)
					
					
					else 
					
						disable_control_action(PLAYER_CONTROL, INPUT_VEH_ATTACK) 
						disable_control_action(PLAYER_CONTROL, INPUT_VEH_ATTACK2) 

						if lazlow_cam_dialogue_counter < 8
						
							if lazlow_dialogue_time = 0

								if create_conversation(scripted_speech, "fam4aud", "lazlo_chase", CONV_PRIORITY_medium)
									
									REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_HIGH)
									
									lazlow_cam_dialogue_counter++
									
									lazlow_dialogue_time = get_game_timer()
								endif 
								
							else 
						
								if lk_timer(lazlow_dialogue_time, 5500)
									if create_conversation(scripted_speech, "fam4aud", "lazlo_chase", CONV_PRIORITY_medium)
										lazlow_cam_dialogue_counter++
										lazlow_dialogue_time = get_game_timer()
									endif 
								endif 
								
							endif 
							
						endif 
						
						if not IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_CAMERA")
							START_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_CAMERA")
						endif 
		
					endif
					
					OVERRIDE_LODSCALE_THIS_FRAME(1.0)
					SET_SCENE_STREAMING_TRACKS_CAM_POS_THIS_FRAME()
					
				else 
				
					deactivate_lazlow_chase_cam()

				endif 
				
			else
			
				deactivate_lazlow_chase_cam()
				
				//CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, lazlow.ped)

			endif

		endif
		
	else 
		
		if help_text_on	or reminder_help_played
			if IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("family4_help_0")
				clear_help()
			endif
			help_text_on = false
			reminder_help_played = false
		endif 
	
	endif 

endproc 

func float calculate_cam_z_height()

	float min_distance = 10
	float max_distance = 80
	float max_offset_height = 4.2 
	float min_offset_height = 4.65
	float z_offset

	float distance_between_vehicles = get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlows_car.veh)) 
	
	if distance_between_vehicles > min_distance
		z_offset = ((((distance_between_vehicles - min_distance) / (max_distance - min_distance)) * (max_offset_height - min_offset_height)) + min_offset_height)
	else 
		z_offset = max_offset_height
	endif 
	
	return z_offset 

endfunc 

//proc lazlow_camera_system()
//
//	if is_vehicle_driveable(lazlows_car.veh)
//	and is_vehicle_driveable(truck.veh)
////		if not train_cutscene_playing and not trailer_cutscene_playing and not storm_drain_cutscene_playing 
//			if not does_cam_exist(lazlow_cam)
//			
//				lazlow_cam = create_cam("default_scripted_camera", false)
//				set_cam_fov(lazlow_cam, 45.00)
//				
//				lazlow_cam_2 = create_cam("default_scripted_camera", false)
//				set_cam_fov(lazlow_cam_2, 40.00)
//
//				set_cam_active(lazlow_cam, true)
//						
//				lazlow_chase_cam_active = false		
//
//			else
//
////				if not help_text_on
////					if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
////						if is_entity_in_angled_area(player_ped_id(), <<-264.563, -1828.329, 27.333>>, <<-238.324, -1817.461, 36.033>>, 41.000)     
////							if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 250.00
////								cam_help_time = get_game_timer()
////								print_help("family4_help_0")
////								help_text_on = true 
////							endif 
////						endif
////					endif 
////					
////				else 
////					
////					if not reminder_help_played
////						if lk_timer(cam_help_time, 15000)
////							if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
////								if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 250.00
////									cam_help_time = get_game_timer()
////									print_help("family4_help_0")
////									reminder_help_played = true
////								endif 
////							endif 
////						endif 
////					endif 
////				endif 	
//		
//				// check for button press and suitable conditions			
//				
//				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 150.00)
//				//and (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) > 5.0)
//					if activate_vehicle_tracking_cam(truck.veh)
//
//						float cam_attach_z = calculate_cam_z_height()
//						
////						printfloat(cam_attach_z)
////						printnl()
//						
//						//if not lazlow_chase_cam_active
//						if not is_cam_rendering(lazlow_cam)
//						and not is_cam_rendering(lazlow_cam_2)
//						
//							if is_entity_in_angled_area(truck.veh, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-10.0, -22.0, -10.0>>), get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-0.4, -22.0, 10.0>>), 55.00, false, true)
//								
//								attach_cam_to_entity(lazlow_cam, truck.veh, <<-0.720, -4.28, cam_attach_z>>)
//								POINT_CAM_AT_ENTITY(lazlow_cam, truck.veh, <<0.000, 15.800, -1.000>>)
//								attach_cam_to_entity(lazlow_cam_2, truck.veh, <<-0.720, -3.980, cam_attach_z>>)
//								POINT_CAM_AT_ENTITY(lazlow_cam_2, lazlows_car.veh, <<0.0, 0.0, 0.0>>)
//								
//								task_look_at_coord(lazlow.ped, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-1.5, -10.0, 0.7>>), 2000)
//
//							elif is_entity_in_angled_area(truck.veh, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<1.4, -22.0, -10.0>>), get_offset_from_entity_in_world_coords(lazlows_car.veh, <<10.0, -22.0, 10.0>>), 55.00, false, true)
//
//								attach_cam_to_entity(lazlow_cam, truck.veh, <<0.720, -4.28, cam_attach_z>>)
//								POINT_CAM_AT_ENTITY(lazlow_cam, truck.veh, <<0.000, 15.800, -1.000>>)
//								attach_cam_to_entity(lazlow_cam_2, truck.veh, <<0.720, -3.980, cam_attach_z>>)
//								POINT_CAM_AT_ENTITY(lazlow_cam_2, lazlows_car.veh, <<0.0, 0.0, 0.0>>)
//								
//								task_look_at_coord(lazlow.ped, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<1.5, -10.0, 0.7>>), 2000)
//								
//							else 
//							
//								attach_cam_to_entity(lazlow_cam, truck.veh, <<-0.720, -4.28, cam_attach_z>>)
//								POINT_CAM_AT_ENTITY(lazlow_cam, truck.veh, <<0.000, 15.800, -1.000>>)
//								attach_cam_to_entity(lazlow_cam_2, truck.veh, <<-0.720, -3.980, cam_attach_z>>)
//								POINT_CAM_AT_ENTITY(lazlow_cam_2, lazlows_car.veh, <<0.0, 0.0, 0.0>>)
//							
//								task_look_at_coord(lazlow.ped, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-1.5, -10.0, 0.7>>), 2000)
//								
//							endif 
//
//							//get_dialogue_data_for_future_resumption()
//							
//							//create_conversation(scripted_speech, "fam4aud", "lazlo_chase", CONV_PRIORITY_medium)
//							//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//							//lazlow_dialogue_time = 0
//									
//							set_cam_active(lazlow_cam, true)
//							set_cam_active_with_interp(lazlow_cam_2, lazlow_cam, 2900, graph_type_linear)//, graph_type_linear)
//							
//							//hei
//							
//							shake_cam(lazlow_cam, "ROAD_VIBRATION_SHAKE", 0.2)
//							shake_cam(lazlow_cam_2, "ROAD_VIBRATION_SHAKE", 0.2)
//							
//							render_script_cams(true, false)
//							//lazlow_chase_cam_active = true
//							
//							// update flag to show its been used so dont need reminder
//							//help_text_on = true
//							reminder_help_played = true
//							
//							SET_CAM_CONTROLS_MINI_MAP_HEADING(lazlow_cam_2, true)
//							
//							SET_CINEMATIC_BUTTON_ACTIVE(false)
//						
//						
//						else 
//						
//							if not IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_CAMERA")
//								START_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_CAMERA")
//							endif 
////						
////							if lazlow_dialogue_time = 0
////							
////								if create_conversation(scripted_speech, "fam4aud", "lazlo_chase", CONV_PRIORITY_medium)
////									lazlow_dialogue_time = get_game_timer()
////								endif 
////								
////							else 
////						
////								if lk_timer(lazlow_dialogue_time, 5500)
////									if create_conversation(scripted_speech, "fam4aud", "lazlo_chase", CONV_PRIORITY_medium)
////										lazlow_dialogue_time = get_game_timer()
////									endif 
////								endif 
////	
//								detach_cam(lazlow_cam_2)
//								attach_cam_to_entity(lazlow_cam_2, truck.veh, <<0.720, -4.28, cam_attach_z>>)
//							
////							endif 
//			
//						endif
//						
//						INFORM_MISSION_STATS_OF_INCREMENT(FAM4_LAZLOW_CAM_USE) 
//						
//					else 
//					
//						deactivate_lazlow_chase_cam()
//
//					endif 
//					
//				else
//				
//					deactivate_lazlow_chase_cam()
//					
////					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, lazlow.ped)
//
//				endif
//			endif 
////		endif
//		
//	else 
//		
//		if help_text_on	or reminder_help_played
//			if IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("family4_help_0")
//				clear_help()
//			endif
//			help_text_on = false
//			reminder_help_played = false
//		endif 
//	
//	endif 
//
//endproc


func bool request_mocap_data_for_Family_4_MCS_3_concat()
				
	REQUEST_CUTSCENE("Family_4_MCS_3_concat")
	
//	SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("trevor", PLAYER_PED_ID())
//	SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael", selector_ped.pedID[selector_ped_michael])
//	SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lazlow", lazlow.ped)
	
	return true
	
endfunc

proc setup_ped_weapon(ped_index mission_ped, weapon_type weapon, int ammo, bool force_into_hand = false, bool equip = false)
								
	if not has_ped_got_weapon(mission_ped, weapon)
		give_weapon_to_ped(mission_ped, weapon, ammo, force_into_hand, equip)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), weapon) < 50
			set_ped_ammo(player_ped_id(), weapon, ammo)
		endif 
	endif 

endproc 

proc load_stadium_assets()

	request_model(lazlow.model)
	set_ped_model_is_suppressed(lazlow.model, true)

	request_model(tracey.model)
	set_ped_model_is_suppressed(tracey.model, true)
	
	request_anim_dict("missfam4leadinoutmcs2")
	
	ASSISTED_MOVEMENT_REQUEST_ROUTE("fame1")

endproc 

func bool has_stadium_assets_loaded()

	if has_model_loaded(lazlow.model)
	and has_model_loaded(tracey.model)
	and has_anim_dict_loaded("missfam4leadinoutmcs2")

		return true 

	endif 
	
	printstring("assets not loaded")
	printnl()
	
	return false
	
endfunc 

proc load_trip_skip_0()

	if replay_active
		start_replay_setup(michael.pos, michael.heading)
	endif 
	
	request_model(michael.model)
	request_model(trevor.model)
	
	if IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		michaels_car.model = GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()
	endif
	request_model(michaels_car.model)
	set_vehicle_model_is_suppressed(michaels_car.model, true)
	
	REQUEST_WAYPOINT_RECORDING("family4_0")
	REQUEST_WAYPOINT_RECORDING("family4_1")
	request_waypoint_recording("family4_2")
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(michael.model)
	or not has_model_loaded(trevor.model)
	or not has_model_loaded(michaels_car.model)
	or not GET_IS_WAYPOINT_RECORDING_LOADED("family4_0")
	or not GET_IS_WAYPOINT_RECORDING_LOADED("family4_1")
	or not GET_IS_WAYPOINT_RECORDING_LOADED("family4_2")
	
		wait(0)
		
	endwhile 
	
	IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID()) 
		REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID()) 
	ENDIF 
	
	clear_area(michaels_car.pos, 300.00, true)
	DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<-800.31, 184.80, 72.72>>, 300.00)
	DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(char_michael)
	
	if IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		michaels_car.veh = CREATE_REPLAY_CHECKPOINT_VEHICLE(michaels_car.pos, michaels_car.heading)
		set_vehicle_automatically_attaches(michaels_car.veh, false)
	else 
		CREATE_PLAYER_VEHICLE(michaels_car.veh, char_michael, michaels_car.pos, michaels_car.heading)
		set_vehicle_automatically_attaches(michaels_car.veh, false)
	endif 
	
	if get_current_player_ped_enum() = char_michael
		printstring("player is MICHAEL")
		printnl()
	elif get_current_player_ped_enum() = char_trevor
		printstring("player is TREVOR")
		printnl()
	endif 
		
	switch get_current_player_ped_enum()
	
		case char_michael
		
			create_player_ped_on_foot(selector_ped.pedID[selector_ped_trevor], char_trevor, trevor.pos, trevor.heading, false)
			setup_buddy_attributes(selector_ped.pedID[selector_ped_trevor])
			setup_ped_weapon(selector_ped.pedID[selector_ped_trevor], weapontype_pistol, 50)
			setup_relationship_contact(selector_ped.pedID[selector_ped_trevor], true)
			add_ped_for_dialogue(scripted_speech, 2, selector_ped.pedID[selector_ped_trevor], "trevor")
									
			while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(player_ped_id())
			or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[selector_ped_trevor])
			
				wait(0)
			
			endwhile 
			
			if not is_ped_injured(selector_ped.pedID[selector_ped_trevor])
			
				force_ped_motion_state(selector_ped.pedID[selector_ped_trevor], ms_on_foot_walk, false)//, faus_cutscene_exit)
				force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_trevor])
				
			endif 

			set_entity_coords(player_ped_id(), michael.pos)
			set_entity_heading(player_ped_id(), michael.heading)
			SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
			force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
			SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
			force_ped_ai_and_animation_update(player_ped_id(), true)
			add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael")
		
		break 
		
		case char_trevor
		
			create_player_ped_on_foot(selector_ped.pedID[selector_ped_michael], char_michael, trevor.pos, trevor.heading, false)
			setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
			setup_ped_weapon(selector_ped.pedID[selector_ped_michael], weapontype_pistol, 50)
			setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
			add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
			IF not HAS_PED_GOT_FIREARM(selector_ped.pedID[selector_ped_michael], false)
				give_weapon_to_ped(selector_ped.pedID[selector_ped_michael], weapontype_pistol, 50, false, false)
			endif 
			
			while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(player_ped_id())
			or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[selector_ped_michael])
			
				wait(0)
			
			endwhile 
			
			if not is_ped_injured(selector_ped.pedID[selector_ped_michael])

				force_ped_motion_state(selector_ped.pedID[selector_ped_michael], ms_on_foot_walk, false)//, faus_cutscene_exit)
				force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])
				
			endif 

			set_entity_coords(player_ped_id(), michael.pos)
			set_entity_heading(player_ped_id(), michael.heading)
			force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
			SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
			force_ped_ai_and_animation_update(player_ped_id(), true)
			add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")

		break 
			
	endswitch 
	

	
	//SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(locates_data.LocationBlip, <<-219.2400, -2037.6470, 27.0>>, 57.2397)
	
	if not replay_active
		load_scene(<<-819.9973, 176.7442, 70.6074>>)
	endif 
	
	end_cutscene()
	
	SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(locates_data, michaels_car.veh)
	
	//set_vehicle_as_no_longer_needed(michaels_car.veh)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "start of mission")
	
	mission_flow = get_to_the_stadium
		
endproc 

proc groupie_ai_system(bool allow_stat_to_process = true)
	
	if not is_ped_injured(groopie.ped)
	
		if groupie_ai_system_status != 22

			if has_ped_been_harmed(groopie.ped, groopie.health)
			or has_player_antagonised_ped(groopie.ped, 30.00, false)
			or is_ped_ragdoll(groopie.ped)
			or (is_entity_in_angled_area(player_ped_id(), <<-175.828, -2039.052, 26.621>>, <<-276.227, -1989.866, 35.621>>, 195.700)  and is_ped_shooting(player_ped_id()))
				
				if IS_ENTITY_PLAYING_ANIM(groopie.ped, "missfam4", "base") //amb@world_human_clipboard@male@base
					stop_anim_task(groopie.ped, "missfam4", "base")
				endif 
			
				detach_entity(clip_board.obj)
				set_model_as_no_longer_needed(clip_board.model)
				set_object_as_no_longer_needed(clip_board.obj)
				
				task_smart_flee_ped(groopie.ped, player_ped_id(), 100.00, -1)
				set_ped_as_no_longer_needed(groopie.ped)
				set_model_as_no_longer_needed(groopie.model)
				
				groupie_ai_system_status = 22
			
			endif 
			
		endif 

		switch groupie_ai_system_status 
		
			case 0
			
				players_vehicle = get_players_last_vehicle()
				
				if is_vehicle_driveable(players_vehicle)
					if is_entity_in_angled_area(players_vehicle, <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, 6.7)
					or get_to_the_stadium_status >= 2 //fix incase car overshoots the angled area when bring_vehicle_to_halt_and_disable_controls_is_called() and is outside the angled area
					
						switch get_current_player_ped_enum() 
						
							case char_michael

								if not is_ped_sitting_in_vehicle(selector_ped.pedID[char_trevor], players_vehicle)
									
									open_sequence_task(seq)
										task_look_at_entity(null, selector_ped.pedID[char_trevor], -1)
										task_turn_ped_to_face_entity(null, selector_ped.pedID[char_trevor])
									close_sequence_task(seq)
									task_perform_sequence(groopie.ped, seq)
									clear_sequence_task(seq)
										
									groupie_ai_system_status++
								endif 

							break 
							
							case char_trevor
							
								if not is_ped_sitting_in_vehicle(player_ped_id(), players_vehicle)
									
									open_sequence_task(seq)
										task_look_at_entity(null, player_ped_id(), -1)
										task_turn_ped_to_face_entity(null, player_ped_id())
									close_sequence_task(seq)
									task_perform_sequence(groopie.ped, seq)
									clear_sequence_task(seq)
	
									groupie_ai_system_status++
								endif 
							
							break 
							
						endswitch 
						
					endif 
					
				endif 

			break 
			
			case 1
				
				switch get_current_player_ped_enum() 
						
					case char_michael
					
						if (not is_ped_injured(selector_ped.pedID[char_trevor]) //added for fail system as groppie system is called within it
						and get_distance_between_coords(get_entity_coords(groopie.ped), get_entity_coords(selector_ped.pedID[char_trevor])) < 20)
						
							if not is_ped_facing_ped(groopie.ped, selector_ped.pedID[char_trevor], 40)
							
								if has_ped_task_finished_2(groopie.ped)
									
									open_sequence_task(seq)
										task_turn_ped_to_face_entity(null, selector_ped.pedID[char_trevor])
									close_sequence_task(seq)
									task_perform_sequence(groopie.ped, seq)
									clear_sequence_task(seq)
									
								endif 
							endif 
							
						else 
						
							if not is_ped_facing_ped(groopie.ped, player_ped_id(), 40)
							
								if has_ped_task_finished_2(groopie.ped)
									
									open_sequence_task(seq)
										task_turn_ped_to_face_entity(null, player_ped_id())
									close_sequence_task(seq)
									task_perform_sequence(groopie.ped, seq)
									clear_sequence_task(seq)
									
								endif 
							endif 
						
						endif 

					break 
					
					case char_trevor
					
						if not is_ped_facing_ped(groopie.ped, player_ped_id(), 40)
							
							if has_ped_task_finished_2(groopie.ped)
								
								open_sequence_task(seq)
									task_turn_ped_to_face_entity(null, player_ped_id(), -1)
								close_sequence_task(seq)
								task_perform_sequence(groopie.ped, seq)
								clear_sequence_task(seq)
								
							endif 
						endif 
					
					break 
					
				endswitch 
			
			break 
			
			case 22
			
			break 
			
		endswitch 
		
	else 
	
		if not groopie.created
		
			if get_distance_between_coords(get_entity_coords(player_ped_id()), truck.pos) < 650.00 
			
				request_model(groopie.model)
				request_model(clip_board.model)
				request_anim_dict("missfam4")
				
				if has_model_loaded(groopie.model)
				and has_model_loaded(clip_board.model)
				and has_anim_dict_loaded("missfam4")
				
					setup_enemy(groopie)
					SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, gropie_group, RELGROUPHASH_PLAYER)
					SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, gropie_group, player_group)
					SET_ped_RELATIONSHIP_GROUP_hash(groopie.ped, gropie_group)
					set_ped_can_ragdoll(groopie.ped, true)
					set_ped_config_flag(groopie.ped, PCF_DontInfluenceWantedLevel, true)
					remove_blip(groopie.blip)
					add_ped_for_dialogue(scripted_speech, 6, groopie.ped, "groupie1") 
					
					task_play_anim(groopie.ped, "missfam4", "base", instant_blend_in, normal_blend_out, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING) 
					force_ped_ai_and_animation_update(groopie.ped)
					
					clip_board.obj = create_object(clip_board.model, (groopie.pos + <<0.0, 0.0, 1.0>>))
					ATTACH_ENTITY_TO_ENTITY(clip_board.obj, groopie.ped, get_ped_bone_index(groopie.ped, BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>) 
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(clip_board.obj)
					
				endif 
				
			endif 
			
		else 
		
			if allow_stat_to_process
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FAM4_COORD_KO)
			endif 
		
		endif 
		
	endif 

endproc 

proc play_female_reaction_anim(ped_index &miss_ped, string anim_name)
						
	if IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@female@arms_folded@base", "base")
	
		PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@female@arms_folded@react_shock", anim_name)
	
	elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@female@legs_crossed@base", "base")
	
		PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@female@legs_crossed@react_shock", anim_name)
	
	elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@female@proper@base", "base")
	
		PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@female@proper@react_shock", anim_name)
	
	elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@female@proper_skinny@base", "base")
	
		PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@female@proper_skinny@react_shock", anim_name)
	
	else 
	
		printstring("not playing any anim")
		printnl()
	
	endif 
	
endproc 

proc play_male_reaction_anim(ped_index &miss_ped, string anim_name)

	if IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@male@elbows_on_knees@base", "base")
							
		PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@male@elbows_on_knees@react_shock", anim_name)

	elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@male@generic_skinny@base", anim_name)

		PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@male@generic_skinny@react_shock", anim_name)

	elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@male@left_elbow_on_knee@base", anim_name)

		PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@male@left_elbow_on_knee@react_shock", anim_name)
				
	else 

		printstring("not playing any anim")
		printnl()

	endif
	
endproc 

proc play_reaction_anim(ped_index &miss_ped, string anim_name)

	switch get_entity_model(miss_ped)
	
		case A_M_Y_Hipster_03
		
			if IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@male@elbows_on_knees@base", "base")
							
				PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@male@elbows_on_knees@react_shock", anim_name)

			elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@male@generic_skinny@base", "base")

				PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@male@generic_skinny@react_shock", anim_name)

			elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@male@left_elbow_on_knee@base", "base")

				PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@male@left_elbow_on_knee@react_shock", anim_name)
						
			else 

				printstring("not playing any anim")
				printnl()

			endif
		
		break 


		case A_F_Y_Hipster_03
		
			if IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@female@arms_folded@base", "base")
	
				PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@female@arms_folded@react_shock", anim_name)
			
			elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@female@legs_crossed@base", "base")
			
				PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@female@legs_crossed@react_shock", anim_name)
			
			elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@female@proper@base", "base")
			
				PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@female@proper@react_shock", anim_name)
			
			elif IS_SCRIPTED_SCENARIO_PED_USING_CONDITIONAL_ANIM(miss_ped, "amb@prop_human_seat_chair@female@proper_skinny@base", "base")
			
				PLAY_ANIM_ON_RUNNING_SCENARIO(miss_ped, "amb@prop_human_seat_chair@female@proper_skinny@react_shock", anim_name)
			
			else 
			
				printstring("not playing any anim")
				printnl()
			
			endif 
			
		break 
		
	endswitch 

endproc 

///PURPOSE: Makes the ambient ped by setting their ambient_ped_status[i] = flee ambient_ped_ai_system must be called in mission fail system
///    
proc make_ambient_peds_flee()

	int i

	for i = 0 to count_of(ambient_ped) - 1
		
		if ambient_ped_status[i] != do_nothing 
		and ambient_ped_status[i] != flee

			ambient_ped_status[i] = flee
			
		endif 
		
	endfor 
	
	if ambient_ped_flee_time = 0
		ambient_ped_flee_time = get_game_timer()
	endif 

endproc 

proc ambient_ped_system()

	int i
	
	if ambient_ped_status[i] != do_nothing
	and ambient_ped_status[i] != flee
		
		if (is_entity_in_angled_area(player_ped_id(), <<-175.828, -2039.052, 26.621>>, <<-276.227, -1989.866, 35.621>>, 195.700) and is_ped_shooting(player_ped_id()))
		or IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-231.798, -2041.398, 26.361>>, <<-260.114, -2021.864, 34.161>>, 39.2)
		or is_explosion_in_angled_area(EXP_TAG_DONTCARE, <<-252.133, -2011.385, 29.146>>, <<-269.892, -2032.251, 32.146 >>, 16.50)

			//if not is_ped_in_specific_room(player_ped_id(), <<-236.6, -2002.0, 25.1>>, "GtaMloRoom004") 
			if not is_explosion_in_angled_area(EXP_TAG_DONTCARE, <<-251.280, -2001.547, 23.685>>, <<-221.936, -2007.784, 26.685>>, 38.000)

				make_ambient_peds_flee()
				
			endif 
			
		endif 
		
	endif 
	
	for i = 0 to count_of(ambient_ped) - 1
	
		if not is_ped_injured(ambient_ped[i].ped)
		
			if ambient_ped_status[i] != do_nothing
			and ambient_ped_status[i] != flee
			
				//deletes peds if the player drives off
				if mission_flow = get_to_the_stadium
					if get_distance_between_coords(get_entity_coords(player_ped_id()), ambient_ped[i].pos) > 100
						if not DOES_SCENARIO_EXIST_IN_AREA(ambient_ped[i].pos, 0.5, true)
							ambient_ped[i].created = false
							delete_ped(ambient_ped[i].ped)
						endif 
					endif 
				endif 

//				if not is_ped_using_any_scenario(ambient_ped[i].ped)
//					if DOES_SCENARIO_EXIST_IN_AREA(ambient_ped[i].pos, 0.5, true)
//						
//						TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(ambient_ped[i].ped, ambient_ped[i].pos, 0.5)
//						
//					endif 
//				endif 

				if has_ped_been_harmed(ambient_ped[i].ped, ambient_ped[i].health)
				or has_player_antagonised_ped(ambient_ped[i].ped, 20.00, false)

					make_ambient_peds_flee()
					
				endif 
				
			endif 

			switch ambient_ped_status[i] 
			
				case get_into_position
				
				break 

				case play_reaction_anim_during_cutscene
				
					if mission_flow = play_stadium_mocap
					
						switch i 
				
							case 0
							
							break 
							
							case 1
							
//								if get_cutscene_time() > 3000
//									play_reaction_anim(ambient_ped[i].ped, "right")
//									ambient_ped_status[i] = play_reaction_chasing_lazlow
//								endif 
							
							break 
							
							case 7
	
								if get_cutscene_time() > 7000
									play_reaction_anim(ambient_ped[i].ped, "back_right")
									ambient_ped_status[i] = play_reaction_chasing_lazlow
								endif 
							
							break 
							
							case 8
							
								if get_cutscene_time() > 13000
									play_reaction_anim(ambient_ped[i].ped, "right")
									ambient_ped_status[i] = play_reaction_chasing_lazlow
								endif 
							
							break 
							
							case 18
							
								if get_cutscene_time() > 10000
									play_reaction_anim(ambient_ped[i].ped, "forward")
									ambient_ped_status[i] = play_reaction_chasing_lazlow
								endif 
							
							break 
							
							case 19
							
								if get_cutscene_time() > 10500
									play_reaction_anim(ambient_ped[i].ped, "right")
									ambient_ped_status[i] = play_reaction_chasing_lazlow
								endif 
							
							break 
							
							case 21
							
								if get_cutscene_time() > 9000
									play_reaction_anim(ambient_ped[i].ped, "right")
									ambient_ped_status[i] = play_reaction_chasing_lazlow
								endif 
							
							break 
							
							case 23
							
								if get_cutscene_time() > 8000
									play_reaction_anim(ambient_ped[i].ped, "right")
									ambient_ped_status[i] = play_reaction_chasing_lazlow
								endif 
							
							break 
							
						endswitch 
						
					endif 
					
				break 
				
				case play_reaction_chasing_lazlow
				
					ambient_ped_status[i] = get_into_position
				
				break 

				case flee	

					if is_ped_using_any_scenario(ambient_ped[i].ped)

						switch i 
		
							case 0
							case 3
							case 6
							case 9
							
								TOGGLE_SCENARIO_PED_COWER_IN_PLACE(ambient_ped[i].ped, true)
							
								//task_smart_flee_ped(ambient_ped[i].ped, player_ped_id(), 200, -1) 
									
								ambient_ped_status[i] = do_nothing
								
							break 

							case 1
							case 4
							case 7

								//if lk_timer(ambient_ped_flee_time, 1000)
									
									//see bug 1122820 for info
								if SET_PED_SHOULD_PLAY_FLEE_SCENARIO_EXIT(ambient_ped[i].ped, get_offset_from_entity_in_world_coords(ambient_ped[i].ped, <<0.0, 0.5, 0.0>>))

									task_smart_flee_ped(ambient_ped[i].ped, player_ped_id(), 200, -1) 
									
									ambient_ped_status[i] = do_nothing
									
								endif 
								
							break 

							case 2
							case 10
							case 13

								if lk_timer(ambient_ped_flee_time, 4000)
								
									//see bug 1122820 for info
									if SET_PED_SHOULD_PLAY_FLEE_SCENARIO_EXIT(ambient_ped[i].ped, get_offset_from_entity_in_world_coords(ambient_ped[i].ped, <<0.0, 0.5, 0.0>>))
				
										task_smart_flee_ped(ambient_ped[i].ped, player_ped_id(), 200, -1) 
									
										ambient_ped_status[i] = do_nothing
									
									endif 
									
								endif 
								
							break 
	
							case 5
							case 8
							case 11
							case 16
							
								if lk_timer(ambient_ped_flee_time, 2000)
								
									//task_smart_flee_ped(ambient_ped[i].ped, player_ped_id(), 200, -1) 
									TOGGLE_SCENARIO_PED_COWER_IN_PLACE(ambient_ped[i].ped, true)
									
									ambient_ped_status[i] = do_nothing
									
								endif 
								
							break 
							
							case 12
							case 15
							case 18
							case 21
							case 24

								//if lk_timer(ambient_ped_flee_time, 500)
								
									TOGGLE_SCENARIO_PED_COWER_IN_PLACE(ambient_ped[i].ped, true)
									//task_smart_flee_ped(ambient_ped[i].ped, player_ped_id(), 200, -1) 
									//SET_PED_PANIC_EXIT_SCENARIO(ambient_ped[i].ped, get_entity_coords(player_ped_id()))
									
									ambient_ped_status[i] = do_nothing
									
								//endif 
								
							break 

							case 14
							case 17
							case 20
							case 23
							
								if lk_timer(ambient_ped_flee_time, 2500)
						
									//task_smart_flee_ped(ambient_ped[i].ped, player_ped_id(), 200, -1) 
									TOGGLE_SCENARIO_PED_COWER_IN_PLACE(ambient_ped[i].ped, true)
									
									ambient_ped_status[i] = do_nothing
									
								endif 
								
							break 
							
							case 19
							case 22
							case 25
							
								if lk_timer(ambient_ped_flee_time, 9000)
								
									//see bug 1122820 for info
									if SET_PED_SHOULD_PLAY_FLEE_SCENARIO_EXIT(ambient_ped[i].ped, get_offset_from_entity_in_world_coords(ambient_ped[i].ped, <<0.0, 0.5, 0.0>>))
				
										task_smart_flee_ped(ambient_ped[i].ped, player_ped_id(), 200, -1) 
									
										ambient_ped_status[i] = do_nothing
									
									endif 
									
								endif 
							
							break 
							
						endswitch 
					
					else 
					
						task_smart_flee_ped(ambient_ped[i].ped, player_ped_id(), 200, -1) 
						
						ambient_ped_status[i] = do_nothing
						
					endif

			
				break 
				
				case do_nothing
				
				break 
				
			endswitch 
			
		else 

			if not ambient_ped[i].created
			
				//optimization as DOES_SCENARIO_EXIST_IN_AREA
				if get_distance_between_coords(get_entity_coords(player_ped_id()), ambient_ped[i].pos) < 150

					request_model(ambient_ped[i].model)
					
					request_anim_dict("amb@prop_human_seat_chair@male@elbows_on_knees@react_shock")
					request_anim_dict("amb@prop_human_seat_chair@male@generic_skinny@react_shock")
					request_anim_dict("amb@prop_human_seat_chair@male@left_elbow_on_knee@react_shock")

					request_anim_dict("amb@prop_human_seat_chair@female@arms_folded@react_shock")
					request_anim_dict("amb@prop_human_seat_chair@female@legs_crossed@react_shock")
					request_anim_dict("amb@prop_human_seat_chair@female@proper@react_shock")
					request_anim_dict("amb@prop_human_seat_chair@female@proper_skinny@react_shock")
					
					if has_model_loaded(ambient_ped[i].model)
//					and has_anim_dict_loaded("amb@prop_human_seat_chair@male@elbows_on_knees@base")
//					and has_anim_dict_loaded("amb@prop_human_seat_chair@male@generic_skinny@base")
//					and has_anim_dict_loaded("amb@prop_human_seat_chair@male@left_elbow_on_knee@base")

					and has_anim_dict_loaded("amb@prop_human_seat_chair@male@elbows_on_knees@react_shock")
					and has_anim_dict_loaded("amb@prop_human_seat_chair@male@generic_skinny@react_shock")
					and has_anim_dict_loaded("amb@prop_human_seat_chair@male@left_elbow_on_knee@react_shock")

					and has_anim_dict_loaded("amb@prop_human_seat_chair@female@arms_folded@react_shock")
					and has_anim_dict_loaded("amb@prop_human_seat_chair@female@legs_crossed@react_shock")
					and has_anim_dict_loaded("amb@prop_human_seat_chair@female@proper@react_shock")
					and has_anim_dict_loaded("amb@prop_human_seat_chair@female@proper_skinny@react_shock")
						
						if DOES_SCENARIO_EXIST_IN_AREA(ambient_ped[i].pos, 0.5, true)
							
							ambient_ped[i].ped = create_ped(pedtype_mission, ambient_ped[i].model, ambient_ped[i].pos, ambient_ped[i].heading)
							set_ped_keep_task(ambient_ped[i].ped, true)
							set_entity_health(ambient_ped[i].ped, ambient_ped[i].health)
							set_ped_random_component_variation(ambient_ped[i].ped)
							set_blocking_of_non_temporary_events(ambient_ped[i].ped, true)
							set_ped_name_debug(ambient_ped[i].ped, ambient_ped[i].name)
							SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(ambient_ped[i].ped, true)

							CREATE_FORCED_OBJECT(ambient_ped[i].pos, 0.5, V_ILev_Chair02_ped, true)
		
							//RETAIN_ENTITY_IN_INTERIOR(ambient_ped[i].ped, get_interior_at_coords_with_type(<<-251.4008, -2008.3384, 29.1458>>, "v_stadium"))
							
							//set_entity_load_collision_flag(ambient_ped[i].ped, true)
					
							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(ambient_ped[i].ped, ambient_ped[i].pos, 0.5) 

							
							force_ped_ai_and_animation_update(ambient_ped[i].ped)
							
							ambient_ped[i].created = true
							
							exit
							
						endif 
						
					endif 
					
				endif 

			else 
			
				if ambient_ped_status[i] != do_nothing
				and ambient_ped_status[i] != flee
					
					//peds using a seated scenario will die via code when injured from being attacked.
					//This is required as the is_ped_harmed_checks() will never be hit because if not is_ped_injured() will return false
					if is_ped_injured(ambient_ped[i].ped)
					
						make_ambient_peds_flee()
						
					endif 
					
				endif 
			
			endif 
		
		endif 
		
	endfor 

endproc 

proc load_trip_skip_1()

	if replay_active
		start_replay_setup(<<-219.6464, -2037.8220, 26.6207>>, 56.6757, false)
	endif 

	request_model(michael.model)
	request_model(trevor.model)
	
	request_model(ambient_ped[0].model)
	request_model(ambient_ped[1].model)
	
	request_model(groopie.model)
	request_model(clip_board.model)
	
	if IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		if IS_REPLAY_CHECKPOINT_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
			michaels_car.model = GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()
		endif 
	endif
	
	request_model(michaels_car.model)
	set_vehicle_model_is_suppressed(michaels_car.model, true)
	
	request_model(parked_truck[0].model)
	set_vehicle_model_is_suppressed(parked_truck[0].model, true)
			
	request_model(parked_trailer[0].model)
	set_vehicle_model_is_suppressed(parked_trailer[0].model, true)
	
	request_model(lazlows_car.model)
	set_vehicle_model_is_suppressed(lazlows_car.model, true)

	request_model(sentinel)
	
	request_model(schafter2)
	
	request_anim_dict("missfam4")
	
	request_vehicle_recording(501, "lkfamily4")
	request_vehicle_recording(502, "lkfamily4")
			
	REQUEST_WAYPOINT_RECORDING("family4_0")
	REQUEST_WAYPOINT_RECORDING("family4_1")
	request_waypoint_recording("family4_2")
	
	stadium_interior  = get_interior_at_coords_with_type(<<-251.4008, -2008.3384, 29.1458>>, "v_stadium")
	pin_interior_in_memory(stadium_interior)
	
	ASSISTED_MOVEMENT_REQUEST_ROUTE("fame1")
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(michael.model)
	or not has_model_loaded(trevor.model)
	or not has_model_loaded(ambient_ped[0].model)
	or not has_model_loaded(ambient_ped[1].model)
	or not has_model_loaded(groopie.model)
	or not has_model_loaded(clip_board.model)
	or not has_model_loaded(michaels_car.model)
	or not has_model_loaded(parked_truck[0].model)
	or not has_model_loaded(parked_trailer[0].model)
	or not has_model_loaded(lazlows_car.model)
	or not has_model_loaded(sentinel)
	or not has_model_loaded(schafter2)
	or not has_anim_dict_loaded("missfam4")
	or not has_vehicle_recording_been_loaded(501, "lkfamily4")
	or not has_vehicle_recording_been_loaded(502, "lkfamily4")
	or not GET_IS_WAYPOINT_RECORDING_LOADED("family4_0")
	or not GET_IS_WAYPOINT_RECORDING_LOADED("family4_1")
	or not GET_IS_WAYPOINT_RECORDING_LOADED("family4_2")
	or not is_interior_ready(stadium_interior)

		wait(0)
		
	endwhile 
	
	clear_area(<<-219.6464, -2037.8220, 26.6207>>, 10000.00, true)
			
	if IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		if IS_REPLAY_CHECKPOINT_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
		
			michaels_car.veh = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-219.6464, -2037.8220, 26.6207>>, 56.6757)
			set_vehicle_automatically_attaches(michaels_car.veh, false)
		
		else 
		
			CREATE_PLAYER_VEHICLE(michaels_car.veh, char_michael, <<-219.6464, -2037.8220, 26.6207>>, 56.6757)
			set_vehicle_automatically_attaches(michaels_car.veh, false)
		
		endif 
	
	else 
		
		CREATE_PLAYER_VEHICLE(michaels_car.veh, char_michael, <<-219.6464, -2037.8220, 26.6207>>, 56.6757)
		set_vehicle_automatically_attaches(michaels_car.veh, false)
		
	endif 
	
	SET_FORCE_HD_VEHICLE(michaels_car.veh, true)
	set_vehicle_engine_on(michaels_car.veh, true, true)
	
	if get_current_player_ped_enum() = char_michael
										
		set_ped_into_vehicle(player_ped_id(), michaels_car.veh) 
		add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael")
	
		create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_trevor], char_trevor, michaels_car.veh, vs_front_right, false)
		setup_buddy_attributes(selector_ped.pedID[selector_ped_trevor])
		setup_relationship_contact(selector_ped.pedID[selector_ped_trevor], true)
		add_ped_for_dialogue(scripted_speech, 2, selector_ped.pedID[selector_ped_trevor], "trevor")
		IF not HAS_PED_GOT_FIREARM(selector_ped.pedID[selector_ped_trevor], false)
			give_weapon_to_ped(selector_ped.pedID[selector_ped_trevor], weapontype_pistol, 50, false, false)
		endif 

	else
	
		set_ped_into_vehicle(player_ped_id(), michaels_car.veh)
		add_ped_for_dialogue(scripted_speech, 2, selector_ped.pedID[selector_ped_michael], "trevor")
		
		create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, michaels_car.veh, vs_front_right, false)
		setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
		setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
		add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
		IF not HAS_PED_GOT_FIREARM(selector_ped.pedID[selector_ped_michael], false)
			give_weapon_to_ped(selector_ped.pedID[selector_ped_michael], weapontype_pistol, 50, false, false)
		endif 

	endif 
	
	while not create_vehicles_outside_stadium()
		wait(0)
	endwhile
	
	parked_truck[0].veh = create_vehicle(parked_truck[0].model, parked_truck[0].pos, parked_truck[0].heading)
	set_vehicle_doors_locked(parked_truck[0].veh, vehiclelock_locked)
	set_vehicle_colours(parked_truck[0].veh, (10 * 0), 0)
	parked_trailer[0].veh = create_vehicle(parked_trailer[0].model, parked_trailer[0].pos, parked_trailer[0].heading)
	attach_vehicle_to_trailer(parked_truck[0].veh, parked_trailer[0].veh)
	
	truck_driver.ped = create_ped_inside_vehicle(parked_truck[0].veh, pedtype_mission, truck_driver.model)
	set_model_as_no_longer_needed(truck_driver.model)
	set_blocking_of_non_temporary_events(truck_driver.ped, true)

	start_playback_recorded_vehicle(parked_truck[0].veh, 501, "lkfamily4")
	start_playback_recorded_vehicle(parked_trailer[0].veh, 502, "lkfamily4")
	force_playback_recorded_vehicle_update(parked_truck[0].veh)
	force_playback_recorded_vehicle_update(parked_trailer[0].veh)
	
	truck_driver_ai_system_2_status = 2
	
	groupie_ai_system()
	
	ambient_ped_system()
	
	while not does_entity_exist(ambient_ped[0].ped)
	
		wait(0)
		
		ambient_ped_system()
		
	endwhile
	
	if not replay_active
		load_scene(<<-219.6464, -2037.8220, 26.6207>>)
	endif 
	
	get_to_the_stadium_dialogue_system_status = 5
	get_to_the_stadium_status = 2
	
	end_cutscene()
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "outside stadium") 
	
	mission_flow = get_to_the_stadium

endproc

proc load_trip_skip_2()

	if replay_active
		start_replay_setup(<<-239.1158, -2005.1418, 23.6855>>, 121.9928, false)
	endif 

	request_model(get_player_ped_model(char_michael))
	request_model(get_player_ped_model(char_trevor))
	
	load_stadium_assets()
	request_model(security.model)
	
	REQUEST_WAYPOINT_RECORDING("family4_0")
	REQUEST_WAYPOINT_RECORDING("family4_1")
	
	stadium_interior  = get_interior_at_coords_with_type(<<-251.4008, -2008.3384, 29.1458>>, "v_stadium")
	pin_interior_in_memory(stadium_interior)
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(get_player_ped_model(char_michael))
	or not has_model_loaded(get_player_ped_model(char_trevor))
	or not has_stadium_assets_loaded()
	or not has_model_loaded(security.model)
	or not GET_IS_WAYPOINT_RECORDING_LOADED("family4_0")
	or not GET_IS_WAYPOINT_RECORDING_LOADED("family4_1")
	or not is_interior_ready(stadium_interior)

		wait(0)
	
	endwhile
	
	set_current_selector_ped(selector_ped_trevor)
	set_entity_coords(player_ped_id(), <<-239.1158, -2005.1418, 23.6855>>)
	set_entity_heading(player_ped_id(), 121.9928)
	
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, true)
	
	clear_area(<<-239.1602, -2002.6151, 23.6856>>, 1000, true)

	if not replay_active
	
		NEW_LOAD_SCENE_START(<<-239.1602, -2002.6151, 23.6856>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 145.8395>>), 40.0)

		while not IS_NEW_LOAD_SCENE_LOADED()

			wait(0)
			
		endwhile
				
		NEW_LOAD_SCENE_STOP()
		
	endif 
	
	set_current_selector_ped(selector_ped_trevor)
	set_entity_coords(player_ped_id(), <<-238.8316, -2005.5457, 23.6855>>)
	set_entity_heading(player_ped_id(), 103.8542)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	FORCE_ROOM_FOR_ENTITY(player_ped_id(), get_interior_at_coords_with_type(<<-239.1158, -2005.1418, 23.6855>>, "v_stadium"), get_hash_key("GtaMloRoom004"))
	
	create_player_ped_on_foot(selector_ped.pedID[selector_ped_michael], char_michael, <<-240.5416, -2006.2936, 23.6856>>, 85.0646)
	setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
	setup_relationship_contact(selector_ped.pedID[selector_ped_michael], true)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
	FORCE_ROOM_FOR_ENTITY(selector_ped.pedID[selector_ped_michael], get_interior_at_coords_with_type(<<-239.1158, -2005.1418, 23.6855>>, "v_stadium"), get_hash_key("GtaMloRoom004"))
	
	create_npc_ped_on_foot(lazlow.ped, CHAR_LAZLOW, lazlow.pos, lazlow.heading, false)
	setup_buddy_attributes(lazlow.ped)
	SET_PED_COMBAT_ATTRIBUTES(lazlow.ped, ca_leave_vehicles, false)
	set_blocking_of_non_temporary_events(lazlow.ped, true)
	lazlow.blip = create_blip_for_ped(lazlow.ped, true) 
	add_ped_for_dialogue(scripted_speech, 5, lazlow.ped, "lazlow")
	FORCE_ROOM_FOR_ENTITY(lazlow.ped, get_interior_at_coords_with_type(<<-239.1158, -2005.1418, 23.6855>>, "v_stadium"), get_hash_key("GtaMloRoom001"))
	
	create_npc_ped_on_foot(tracey.ped, char_tracey, <<-235.0557, -2000.0452, 23.6854>>, 86.6000)  
	setup_buddy_attributes(tracey.ped)
	setup_relationship_contact(tracey.ped, true)
	set_ped_can_ragdoll(tracey.ped, true)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_HAIR, 4, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_TORSO, 5, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_LEG, 1, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_FEET, 1, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_SPECIAL, 2, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(tracey.ped, PED_COMP_JBIB, 0, 0)
	add_ped_for_dialogue(scripted_speech, 4, tracey.ped, "tracey")
	FORCE_ROOM_FOR_ENTITY(tracey.ped, get_interior_at_coords_with_type(<<-239.1158, -2005.1418, 23.6855>>, "v_stadium"), get_hash_key("GtaMloRoom004"))
	
	security.ped = create_ped(pedtype_mission, security.model, <<-235.4601, -2003.0004, 23.6854>>, 86.6000) 
	FORCE_ROOM_FOR_ENTITY(security.ped, get_interior_at_coords_with_type(<<-239.1158, -2005.1418, 23.6855>>, "v_stadium"), get_hash_key("GtaMloRoom004"))
	set_entity_health(security.ped, 2)
	set_ped_as_no_longer_needed(security.ped)
	set_model_as_no_longer_needed(security.model)

	wait(0)//wait for streaming requests to be registered.
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(player_ped_id())
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[selector_ped_michael])
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(lazlow.ped)
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(tracey.ped)
	
		wait(0)
	
	endwhile 

	if not is_ped_injured(player_ped_id())
		
		open_sequence_task(seq)
			TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 5,  EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS, 13)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
		close_sequence_task(seq)
		task_perform_sequence(player_ped_id(), seq)
		clear_sequence_task(seq)
		
		FORCE_ped_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN)
		force_ped_ai_and_animation_update(player_ped_id())
		
	endif 

	if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
		
		open_sequence_task(seq)
			TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 8,  EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
		close_sequence_task(seq)
		task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
		clear_sequence_task(seq)
		
		FORCE_ped_MOTION_STATE(selector_ped.pedID[selector_ped_michael], MS_ON_FOOT_RUN)
		force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])
	endif 

	original_time = get_game_timer()
	
	create_stadium_assets()
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "run after lazlow") 

endproc 

proc load_trip_skip_3()

	clear_player_wanted_level(player_id())
	set_max_wanted_level(0)
	set_create_random_cops(false)

	DELETE_ALL_TRAINS() 
	SET_RANDOM_TRAINS(false) 

	set_vehicle_model_is_suppressed(utillitruck, true)
	set_vehicle_model_is_suppressed(utillitruck2, true)
	set_vehicle_model_is_suppressed(utillitruck3, true)
	
	if replay_active
		start_replay_setup(truck.pos, truck.heading, false)
	endif 
	
	request_model(michael.model)
	request_model(trevor.model)
	
	request_model(ambient_driver.model)

	request_model(truck.model)
	set_vehicle_model_is_suppressed(truck.model, true)
	
	request_model(trailer.model)
	set_vehicle_model_is_suppressed(trailer.model, true)
	
	request_model(lazlows_car.model)
	set_vehicle_model_is_suppressed(lazlows_car.model, true)
	
	request_model(lazlow.model)
	set_ped_model_is_suppressed(lazlow.model, true)
	
	request_model(sentinel)
	request_model(schafter2)
	request_model(manana)
	
	request_anim_dict("missfam4")
	
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FAM4_UNHITCH_TRAILER")
	
	REQUEST_PTFX_ASSET()

	load_mission_vehicle_recordings()
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(michael.model)
	or not has_model_loaded(trevor.model)
	or not has_model_loaded(ambient_driver.model)
	or not has_model_loaded(truck.model)
	or not has_model_loaded(trailer.model)
	or not has_model_loaded(lazlows_car.model)
	or not has_model_loaded(lazlow.model)
	or not has_model_loaded(sentinel)
	or not has_model_loaded(schafter2)
	or not has_model_loaded(manana)
	or not has_anim_dict_loaded("missfam4")
	or not REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FAM4_UNHITCH_TRAILER")
	or not has_ptfx_asset_loaded()
	or not has_mission_vehicle_recordings_loaded()

		wait(0)
		
	endwhile 
	
	clear_area(<<-219.6464, -2037.8220, 26.6207>>, 10000.00, true)
	
	create_entities_outside_stadium(false)
	
	MODIFY_VEHICLE_TOP_SPEED(truck.veh, -15)
	
	create_npc_ped_inside_vehicle(lazlow.ped, char_lazlow, lazlows_car.veh, vs_driver, false)
	set_ped_can_ragdoll(lazlow.ped, false)
	SET_PED_COMBAT_ATTRIBUTES(lazlow.ped, ca_leave_vehicles, false)
	set_blocking_of_non_temporary_events(lazlow.ped, true)
	add_ped_for_dialogue(scripted_speech, 5, lazlow.ped, "lazlow")
	
	set_current_selector_ped(selector_ped_trevor, false)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	set_ped_into_vehicle(player_ped_id(), truck.veh)
	
	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, truck.veh, vs_front_right, false)
	set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
	
	

	
	#IF IS_DEBUG_BUILD
		set_uber_parent_widget_group(family_4_widget_group)
	#endif 
	INITIALISE_UBER_PLAYBACK("lkfamily4", 001, true)
	load_uber_data()
	fUberPlaybackDensitySwitchOffRange = 200
	bCreateAllWaitingCars = true
	bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = true 
	allow_veh_to_stop_on_any_veh_impact = true
//	bTrafficDontCleanupRecordingFiles = true
	allow_trailer_touching_check = true
	traffic_block_vehicle_colour(true, traffic_red, traffic_orange)
	start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4")
	update_uber_playback(lazlows_car.veh, 1.0)
	SET_UBER_PLAYBACK_TO_TIME_NOW(lazlows_car.veh, 3000.00)
	update_uber_playback(lazlows_car.veh, 1.0)
	force_playback_recorded_vehicle_update(lazlows_car.veh)
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
	
	INFORM_MISSION_STATS_OF_FINANCE_MODEL(sentinel, 100)
	INFORM_MISSION_STATS_OF_FINANCE_MODEL(schafter2, 100)
	INFORM_MISSION_STATS_OF_FINANCE_MODEL(taco, 100)

	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	start_audio_scene("FAMILY_4_CHASE_TRUCK_1")
	
	trigger_music_event("FAM4_CHASE_RESTART")

	end_cutscene()
	
	original_time = get_game_timer()
	//cam_help_time = get_game_timer()
	dialogue_time = get_game_timer()
	
	print_now("family4_god_2", 4000, 1)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "Chase truck")

	mission_flow = uber_chase

endproc 


proc load_trip_skip_4()
	
	clear_player_wanted_level(player_id())
	set_max_wanted_level(0)
	set_create_random_cops(false)

	DELETE_ALL_TRAINS() 
	SET_RANDOM_TRAINS(false) 

	set_vehicle_model_is_suppressed(utillitruck, true)
	set_vehicle_model_is_suppressed(utillitruck2, true)
	set_vehicle_model_is_suppressed(utillitruck3, true)
	
	if replay_active
		start_replay_setup(<<503.0987, -851.0613, 24.0433>>, 359.5455, false)
	endif 
	
	request_model(michael.model)
	request_model(trevor.model)

	request_model(truck.model)
	set_vehicle_model_is_suppressed(truck.model, true)
	
	request_model(trailer.model)
	set_vehicle_model_is_suppressed(trailer.model, true)
	
	request_model(lazlows_car.model)
	set_vehicle_model_is_suppressed(lazlows_car.model, true)
	
	request_model(lazlow.model)
	set_ped_model_is_suppressed(lazlow.model, true)
	
	request_model(sentinel)
	request_model(schafter2)
	request_model(manana)
	
	request_anim_dict("missfam4")
	
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FAM4_UNHITCH_TRAILER")
	
	REQUEST_PTFX_ASSET()

	load_mission_vehicle_recordings()
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(michael.model)
	or not has_model_loaded(trevor.model)
	or not has_model_loaded(truck.model)
	or not has_model_loaded(trailer.model)
	or not has_model_loaded(lazlows_car.model)
	or not has_model_loaded(lazlow.model)
	or not has_model_loaded(sentinel)
	or not has_model_loaded(schafter2)
	or not has_model_loaded(manana)
	or not has_anim_dict_loaded("missfam4")
	or not REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FAM4_UNHITCH_TRAILER")
	or not has_ptfx_asset_loaded()
	or not has_mission_vehicle_recordings_loaded()

		wait(0)
		
	endwhile 
	
	SET_PED_POPULATION_BUDGET(2)
	
	clear_area(<<502.97, -860.93, 24.15>>, 10000.00, true)
	
	truck.veh = create_vehicle(truck.model, <<502.9799, -860.9365, 24.1538>>, 358.9030)
	set_vehicle_on_ground_properly(truck.veh)	
	SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
	set_vehicle_tyres_can_burst(truck.veh, false)
	set_vehicle_colours(truck.veh, 0, 0)
	set_entity_health(truck.veh, 1500)
	set_vehicle_engine_health(truck.veh, 1500)
	set_vehicle_petrol_tank_health(truck.veh, 1500)
	//set_vehicle_dirt_level(truck.veh, 0.0)
	set_vehicle_strong(truck.veh, true)
	SET_VEHICLE_HAS_STRONG_AXLES(truck.veh, true)
	//set_vehicle_radio_enabled(truck.veh, false)
	SET_RADIO_TO_STATION_NAME("off")
	
	if not family4_trailer_detached
		trailer.veh = create_vehicle(trailer.model, <<502.9191, -865.4985, 24.2171>>, 358.8516)
		set_vehicle_on_ground_properly(trailer.veh)
		set_vehicle_tyres_can_burst(trailer.veh, false)
		attach_vehicle_to_trailer(truck.veh, trailer.veh)
		//DISABLE_TRAILER_BREAKING_FROM_VEHICLE(trailer.veh, true)
		activate_physics(truck.veh)
		activate_physics(trailer.veh)
	endif 
	
	start_playback_recorded_vehicle(truck.veh, 201, "lkfamily4") 
	skip_time_in_playback_recorded_vehicle(truck.veh, 7000)
	force_playback_recorded_vehicle_update(truck.veh)	
	
	if does_entity_exist(trailer.veh)
		start_playback_recorded_vehicle(trailer.veh, 202, "lkfamily4") 
		skip_time_in_playback_recorded_vehicle(trailer.veh, 7000)
		force_playback_recorded_vehicle_update(trailer.veh)
	endif 
	
	MODIFY_VEHICLE_TOP_SPEED(truck.veh, -15)
	
	lazlows_car.pos = get_position_of_vehicle_recording_at_time(001, 68000, "lkfamily4")
	vector lazlows_car_rot 
	lazlows_car_rot = get_position_of_vehicle_recording_at_time(001, 68000, "lkfamily4")
	
	lazlows_car.veh = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car_rot.z)
	set_vehicle_strong(lazlows_car.veh, true)
	SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
	SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
	set_vehicle_tyres_can_burst(lazlows_car.veh, false)
	set_entity_only_damaged_by_player(lazlows_car.veh, true)
	set_entity_proofs(lazlows_car.veh, false, true, true, true, true)
	set_vehicle_strong(lazlows_car.veh, true)
	SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
	set_vehicle_doors_locked(lazlows_car.veh, vehiclelock_lockout_player_only) 
	set_vehicle_colours(lazlows_car.veh, 27, 0)
	//SET_ENTITY_LOAD_COLLISION_FLAG(lazlows_car.veh, true)
	SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
	SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(lazlows_car.veh, "FAMILY_4_LAZLOWS_CAR")
	
	create_npc_ped_inside_vehicle(lazlow.ped, char_lazlow, lazlows_car.veh, vs_driver, false)
	set_ped_can_ragdoll(lazlow.ped, false)
	SET_PED_COMBAT_ATTRIBUTES(lazlow.ped, ca_leave_vehicles, false)
	set_blocking_of_non_temporary_events(lazlow.ped, true)
	add_ped_for_dialogue(scripted_speech, 5, lazlow.ped, "lazlow")
	
	set_current_selector_ped(selector_ped_trevor, false)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	set_ped_into_vehicle(player_ped_id(), truck.veh)
	
	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, truck.veh, vs_front_right, false)
	set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")

	
	#IF IS_DEBUG_BUILD
		set_uber_parent_widget_group(family_4_widget_group)
	#endif 
	INITIALISE_UBER_PLAYBACK("lkfamily4", 001, true)
	load_uber_data()
	fUberPlaybackDensitySwitchOffRange = 200
	bCreateAllWaitingCars = true
	bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = true 
	allow_veh_to_stop_on_any_veh_impact = true
//	bTrafficDontCleanupRecordingFiles = true
	allow_trailer_touching_check = true
	traffic_block_vehicle_colour(true, traffic_red, traffic_orange)
	start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4")
	//update_uber_playback(lazlows_car.veh, 1.0)
	SET_UBER_PLAYBACK_TO_TIME_NOW(lazlows_car.veh, 68000)
	update_uber_playback(lazlows_car.veh, 1.0)
	force_playback_recorded_vehicle_update(lazlows_car.veh)
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
	
	INFORM_MISSION_STATS_OF_FINANCE_MODEL(sentinel, 100)
	INFORM_MISSION_STATS_OF_FINANCE_MODEL(schafter2, 100)
	INFORM_MISSION_STATS_OF_FINANCE_MODEL(taco, 100)

	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	start_audio_scene("FAMILY_4_CHASE_TRUCK_1")
	
	trigger_music_event("FAM4_CHASE_RESTART")
	
	uber_speed_status = 3
	dialogue_status = 3
	uber_chase_in_car_conversation = 6 //8
	cleanup_asset_status = 2
	ambient_train_system_status = 5
	
	disired_playback_speed = 1.0
	lazlows_car.speed = 1.0 //forces the speed to be 1.0 and won't cause it to start from 0.0 which causes slow acceleration.

	original_time = get_game_timer()
	
	while not lk_timer(original_time, 1000)
		wait(0)
		update_uber_playback(lazlows_car.veh, 1.0)	
	endwhile

	if is_vehicle_driveable(truck.veh)
		if is_playback_going_on_for_vehicle(truck.veh)
			stop_playback_recorded_vehicle(truck.veh)
		endif 
	endif 
	
	if does_entity_exist(trailer.veh)
		if is_vehicle_driveable(trailer.veh)
			if is_playback_going_on_for_vehicle(trailer.veh)
				stop_playback_recorded_vehicle(trailer.veh)
			endif 
		endif 
	endif 
	
	end_cutscene()

	original_time = get_game_timer()
	//cam_help_time = get_game_timer()
	dialogue_time = get_game_timer()
	
	print_now("family4_god_2", 4000, 1)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "half way through truck chase", true)

	mission_flow = uber_chase

endproc

proc load_trip_skip_5()

	set_max_wanted_level(0)
	set_create_random_cops(false)

	DELETE_ALL_TRAINS() 
	SET_RANDOM_TRAINS(false) 
	
	if replay_active
		start_replay_setup(<<1059.5020, -291.9928, 49.4891>>, 332.7211)
	endif 

	request_model(michael.model)
	request_model(trevor.model)
	request_model(truck.model)
	set_vehicle_model_is_suppressed(truck.model, true)	
	request_model(trailer.model)
	set_vehicle_model_is_suppressed(trailer.model, true)
	request_model(lazlow.model)
	request_model(lazlows_car.model)
	set_vehicle_model_is_suppressed(lazlows_car.model, true)
	
	request_vehicle_recording(001, "lkfamily4") 
	
	request_mocap_data_for_Family_4_MCS_3_concat()
	
	if replay_active
		END_REPLAY_SETUP()
	endif 

	while not has_model_loaded(michael.model)
	or not has_model_loaded(trevor.model)
	or not has_model_loaded(truck.model)
	or not has_model_loaded(trailer.model)
	or not has_model_loaded(lazlow.model)
	or not has_model_loaded(lazlows_car.model)
	or not has_vehicle_recording_been_loaded(001, "lkfamily4")
	or not has_cutscene_loaded()

		wait(0)

	endwhile
	
	clear_area(<<1059.5020, -291.9928, 49.4891>>, 1000, true)
	
	truck.veh = create_vehicle(truck.model, <<1059.5020, -291.9928, 49.4891>>, 332.7211)
	set_vehicle_strong(truck.veh, true)
	SET_VEHICLE_HAS_STRONG_AXLES(truck.veh, true)
	SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
	set_vehicle_tyres_can_burst(truck.veh, false)
	set_vehicle_colours(truck.veh, 0, 0) //73.0

	trailer.veh = create_vehicle(trailer.model, <<1051.93, -304.8, 49.4891>>, 332.7211)
	set_vehicle_tyres_can_burst(trailer.veh, false)
	SET_ENTITY_PROOFS(trailer.veh, true, true, true, true, true)
	
	attach_vehicle_to_trailer(truck.veh, trailer.veh)	
	
	set_current_selector_ped(selector_ped_trevor, false)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	set_ped_into_vehicle(player_ped_id(), truck.veh)

	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, truck.veh, vs_front_right, false)
	set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")

	lazlows_car.veh = create_vehicle(lazlows_car.model, get_position_of_vehicle_recording_at_time(001, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "lkfamily4") - 3500), "lkfamily4"), 332.2)
	set_vehicle_strong(lazlows_car.veh, true)
	SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
	SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
	set_vehicle_tyres_can_burst(lazlows_car.veh, false)
	set_vehicle_colours(lazlows_car.veh, 27, 0)
	start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4")
	skip_to_end_and_stop_playback_recorded_vehicle(lazlows_car.veh)
	SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
	SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
	
	create_npc_ped_inside_vehicle(lazlow.ped, CHAR_LAZLOW, lazlows_car.veh, vs_driver, false)
	add_ped_for_dialogue(scripted_speech, 5, lazlow.ped, "lazlow")
	
	register_entity_for_cutscene(selector_ped.pedID[SELECTOR_PED_Michael], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
	register_entity_for_cutscene(lazlow.ped, "Lazlow", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
	register_entity_for_cutscene(lazlows_car.veh, "Lazlows_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
	
	register_entity_for_cutscene(truck.veh, "family_4_truck", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
	if does_entity_exist(trailer.veh)
		register_entity_for_cutscene(trailer.veh, "Family_4_trailer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
	else 
		register_entity_for_cutscene(null, "Family_4_trailer", CU_DONT_ANIMATE_ENTITY, trailer.model)
	endif 
	
	register_entity_for_cutscene(null, "Lazlow_Jeans", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, lazlows_jeans.model)
		
	if not replay_active
		load_scene(<<1059.5020, -291.9928, 49.4891>>)
	endif 
	
	start_new_cutscene_no_fade()
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
	
	start_cutscene()
	
	while not is_cutscene_playing()
		wait(0)
	endwhile
	
	do_screen_fade_in(default_fade_time)
	
	play_final_mocap_status = 2
	mission_flow = play_final_mocap

endproc 

proc load_repeat_play_assets()

	request_model(get_player_ped_model(char_michael))

	while not has_model_loaded(get_player_ped_model(char_michael))
	
		wait(0)
		
	endwhile 
	

	set_current_selector_ped(selector_ped_michael)
	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<-822.2418, 180.9213, 70.6951>>)
	set_entity_heading(player_ped_id(), 73.8140)
	
	NEW_LOAD_SCENE_START(<<-822.2418, 180.9213, 70.6951>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 73.8140>>), 100.0)

	while not IS_NEW_LOAD_SCENE_LOADED()

		wait(0)
		
	endwhile
			
	NEW_LOAD_SCENE_STOP()

	
	michaels_house_interior = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-800.31, 184.80, 72.72>>, "V_Michael")

	PIN_INTERIOR_IN_MEMORY(michaels_house_interior)

	REQUEST_CUTSCENE("FAM_4_INT_ALT1")


	while not IS_INTERIOR_READY(michaels_house_interior)
	or not HAS_CUTSCENE_LOADED()
	
		wait(0)
		
	endwhile 

	
	switch get_current_player_ped_enum() 
	
		case char_michael
		
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))

		break 
				
	endswitch 
	
		
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)

	START_CUTSCENE()
		
	
	while not is_cutscene_playing()

		wait(0)
		
	endwhile 
	
	SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", FALSE)
	
	SET_SRL_POST_CUTSCENE_CAMERA(michael.pos, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, michael.heading>>))
	SET_SRL_READAHEAD_TIMES(5,5,5,5) 

	DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<-800.31, 184.80, 72.72>>, 300.00)
	
	DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(char_michael)
	
	players_last_vehicle = get_players_last_vehicle()
	
	//---
	//players last vehicle in the grounds of michaels house.
	if does_entity_exist(players_last_vehicle)
		if is_vehicle_driveable(players_last_vehicle)
			if is_entity_in_angled_area(players_last_vehicle, <<-817.510, 151.703, 64.606>>, <<-815.789, 192.767, 77.706>>, 61.400)   
			
				//put vehicle ouside michaels front door
				//area round michaels house covering kitchen and driveway.
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-812.943, 167.079, 69.294>>, <<-811.455, 192.135, 76.794>>, 37.000, michaels_car.pos, michaels_car.heading, <<15.0, 15.0, 15.0>>, false) 
				set_players_last_vehicle_to_vehicle_gen(michaels_car.pos, michaels_car.heading)
					
			else 
			
				//put players last vehicle across the road from michaels house
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-860.221, 150.219, 62.021>>, <<-858.733, 175.275, 72.921>>, 31.500, car_pos_outside_house, car_heading_outside_house, <<15.0, 15.0, 15.0>>, false)//, false) //2.5, 5.5, 2.0
				set_players_last_vehicle_to_vehicle_gen(car_pos_outside_house, car_heading_outside_house)
			
			endif 
		endif 
	endif 

	clear_area(<<-800.31, 184.80, 72.72>>, 10000, true)
	
	intro_mocap_status = 2
	
	do_screen_fade_in(default_fade_time)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "start of mission")
	g_stageSnapshot.eCharacter = char_michael	//the snap shot data locks itself off once a stage is set. So the same stage can not be set again. As a result stage 0 is called at the begining automatically by flow so by calling it here will not update anything. That's why we need to override it.
												
endproc 

/// PURPOSE: if the player as franklin switches to michael. It will set the last players vehicle
///    	     to the inpound. 
///    		 This fixes the issue where the player as franklin can drive to michaels house in a 
///     	 vehicle then performs a switch to michael leaving the vehicle when the mission starts.
///    	
///    		 The player switch cleanup the previous ped (franklin) but will not cleanup the vehicle for some reason
proc cleanup_franklins_vehicle_on_switch_to_michael() 

	players_last_vehicle = get_players_last_vehicle()
	
	if does_entity_exist(players_last_vehicle)
	
		if is_vehicle_driveable(players_last_vehicle)
	
			set_entity_as_mission_entity(players_last_vehicle, true, true)
			
			SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(players_last_vehicle)
			
			delete_vehicle(players_last_vehicle)
			
		else 
		
			set_entity_as_mission_entity(players_last_vehicle, true, true)
		
			delete_vehicle(players_last_vehicle)
			
		endif 
		
	endif 

endproc 

proc mission_setup()

	#IF IS_DEBUG_BUILD
		load_lk_widgets()
		load_vehicle_force_widget_data()
		SET_LOCATES_HEADER_WIDGET_GROUP(family_4_widget_group)
	#endif 
	
	//INFORM_MISSION_STATS_OF_MISSION_START_family_four()
	
	clear_prints()
	clear_help()
	
	initialise_mission_variables()

	load_text_and_dialogue()
	
	add_relationship_groups()
	
	SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)
	
	set_vehicle_model_is_suppressed(truck.model, true)
	set_vehicle_model_is_suppressed(trailer.model, true)
	set_vehicle_model_is_suppressed(lazlows_car.model, true)
	set_vehicle_model_is_suppressed(michaels_car.model, true)
	
	if is_player_playing(player_id())	
		clear_player_wanted_level(player_id())
	endif 
	set_wanted_level_multiplier(0.5)

	SUPPRESS_EMERGENCY_CALLS()
	

	//blocks all vehicle generators in stadium car park.
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-392.1, -2114.8, 100.00>>, <<-141.4, -1837.0, -100.00>>, false)
	
	//entrance to car park to stop scenario cars driving in
	add_scenario_blocking_area(<<-193.70, -2025.00, 35.000>>, <<-145.700, -1988.000, 20.000>>)
	
	//security guard scenarios on front of stadium
	add_scenario_blocking_area(<<-267.800, -2032.000, 35.000>>, <<-246.200, -2024.000, -28.000>>) 

	//inside stadium
	add_scenario_blocking_area(<<-260.800, -2029.000, -100.000>>, <<-333.000, -1931.000, 100.000>>)
	add_scenario_blocking_area(<<-254.0, -2024.9, -100.000>>, <<-333.000, -1931.000, 100.000>>)
	add_scenario_blocking_area(<<-249.700, -2016.000, -100.000>>, <<-333.000, -1931.000, 100.000>>)
	
	//storm drain pass mocap area
	add_scenario_blocking_area(<<987.000, -209.500, 0.000>>, <<1121.000, -413.000, 78.000>>)
	
	//lock and open stadium doors
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILev_STAD_FDOOR, <<-259.49, -2031.94, 30.52>>, true, 0.0, 0.0, 1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILev_STAD_FDOOR, <<-257.49, -2029.56, 30.52>>, true, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILev_STAD_FDOOR, <<-256.08, -2027.87, 30.52>>, true, 0.0, 0.0, 1.0)  
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILev_STAD_FDOOR, <<-254.09, -2025.5, 30.52>>, true, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILev_STAD_FDOOR, <<-252.67, -2023.81, 30.52>>, true, 0.0, 0.0, 1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILev_STAD_FDOOR, <<-250.68, -2021.44, 30.52>>, true, 0.0, 0.0, -1.0)

	SET_BUILDING_STATE(BUILDINGNAME_IPL_STADIUM_INTERIOR, BUILDINGSTATE_DESTROYED)

	if is_replay_in_progress()
	
		replay_active = true
	
		if not g_bShitskipAccepted
	
			SWITCH Get_Replay_Mid_Mission_Stage()
		
				case 0
				
					load_trip_skip_0() //start of mission
				
				break 
				
				case 1
				
					load_trip_skip_1() //outside stadium
				
				break 
				
				case 2
				
					load_trip_skip_2() //chase lazlow
				
				break 
				
				case 3
				
					load_trip_skip_3() //truck chase
				
				break 
				
				case 4
				
					load_trip_skip_4() //half way through chase
				
				break 
				
				case 5
				
				break
				
			endswitch 
			
		else 
		
			int shit_skip_status 
			
			shit_skip_status = Get_Replay_Mid_Mission_Stage() + 1
			
			switch shit_skip_status 
				
				case 1
				
					load_trip_skip_1()
				
				break 
				
				case 2
				
					load_trip_skip_2()
				
				break 
				
				case 3
				
					load_trip_skip_3()
				
				break 
				
				case 4
				
					load_trip_skip_4()
				
				break 
				
				case 5
				
					load_trip_skip_5()
				
				break 
				
			endswitch 
		
		endif 
		
		replay_active = false //this switches off propper replay funcitonality to ensure the z skips work.
		
	else 
	
		family4_trailer_detached = false
	
		if not is_repeat_play_active()

			if not is_screen_faded_in()
				if not is_screen_fading_in()
					do_screen_fade_in(default_fade_time)
				endif 
			endif 

			
			//if the player is trevor and has switched to michael the mission will start with the intro mocap 
			if IS_PLAYER_SWITCH_IN_PROGRESS()
				
				if get_current_player_ped_enum() = char_michael
		
					mission_flow = switching_into_michael
					
				endif 
				
				cleanup_franklins_vehicle_on_switch_to_michael() 

			else 
			
				switch get_current_player_ped_enum()
					
					case char_trevor
					
						mission_flow = trevor_leadin
					
					break 
					
				endswitch 

			endif 

			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "start of mission")
			
		else 
		
			load_repeat_play_assets()

		endif 
	
	endif 	
	
endproc 

proc create_start_of_mission_assets()
		
	clear_area(<<-800.31, 184.80, 72.72>>, 1000, true)
	
	SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
	
	IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID()) 
		REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID()) 
	ENDIF 
	
	SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", true)//enabling radio after disabling it for mocap

	if is_valid_interior(michaels_house_interior)
		unpin_interior(michaels_house_interior)
	endif 
			
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)

	CREATE_NPC_VEHICLE(amandas_car.veh, char_amanda, amandas_car.pos, amandas_car.heading)
	set_vehicle_as_no_longer_needed(amandas_car.veh)

	set_all_random_peds_flee(player_id(), true)

	//in the event michaels car is created by setup_players_vehicle_for_return_to_gameplay()
	if does_entity_exist(michaels_car.veh)
		if is_entity_a_mission_entity(michaels_car.veh)
			SET_MISSION_VEHICLE_GEN_VEHICLE(michaels_car.veh, michaels_car.pos, michaels_car.heading)
			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(michaels_car.veh)
			//set_vehicle_gen_vehicle(vehgen_michael_savehouse, michaels_car.veh)
			//set_vehicle_as_no_longer_needed(michaels_car.veh)
		endif 
	endif 
	set_model_as_no_longer_needed(michaels_car.model)//set model as no longer needed in the event michaels car model was not created via setup_players_vehicle_for_return_to_gameplay() as it was not needed or it has been given to the vehicle gen controller
	
	
	//SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(locates_data.LocationBlip, <<-219.2400, -2037.6470, 27.0>>, 57.2397)
	
	if is_screen_faded_out()
		end_cutscene()
	else
		end_cutscene_no_fade(false, false)
	endif 
	
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "start of mission") 
	
	mission_flow = get_to_the_stadium
	
//	clear_ped_tasks_immediately(player_ped_id())
//	delete_ped(selector_ped.pedID[selector_ped_michael])
//	DELETE_VEHICLE(truck.veh)
//	DELETE_VEHICLE(trailer.veh)
//	for i = 0 to count_of(parked_truck) - 1
//		DELETE_VEHICLE(parked_truck[i].veh)
//		DELETE_VEHICLE(parked_trailer[i].veh)
//	endfor 
//	set_model_as_no_longer_needed(michael.model)
//	set_model_as_no_longer_needed(truck.model)
//	set_model_as_no_longer_needed(trailer.model)
//	set_entity_collision(lazlows_car.veh, false)
//	
//	
//	delete_ped(lazlow.ped)
//	DELETE_VEHICLE(lazlows_car.veh)	

//	delete_ped(selector_ped.pedID[selector_ped_trevor])
//	mission_flow = train_crash_cutscene_test_bed
	//mission_flow = detach_trailer_cutscene_test_bed

	
//USE WHEN RECORDING THE MAIN CAR OR ANY OTHER SETPIECE VEHICLE.
//	mission_flow = record_setpiece_cars
	
//	//when comparing recording times between the 2 enties
//	family_4_record_lazlow_status = 2

	//re-recording traffic and setpiece recordings
//	family_4_record_lazlow_status = 4

//	delete_ped(selector_ped.pedID[selector_ped_trevor])
//	mission_flow = storm_drain_cutscene
	
endproc 

proc request_start_of_mission_assets()

	request_model(michael.model)
	request_model(trevor.model)

	request_model(michaels_car.model)
	set_vehicle_model_is_suppressed(michaels_car.model, true)
	REQUEST_VEHICLE_ASSET(michaels_car.model, enum_to_int(VRF_REQUEST_ALL_ANIMS))
	
	request_model(amandas_car.model)
	set_vehicle_model_is_suppressed(amandas_car.model, true)
	
	REQUEST_WAYPOINT_RECORDING("family4_0")
	REQUEST_WAYPOINT_RECORDING("family4_1")
	request_waypoint_recording("family4_2")
	
endproc 

func bool has_start_of_mission_assets_loaded()

	if not has_model_loaded(michael.model)
		printstring("a")
		printnl()
	endif 
		
	if not has_model_loaded(trevor.model)
		printstring("b")
		printnl()
	endif 
		
	if not has_model_loaded(michaels_car.model)
		printstring("c")
		printnl()
	endif 
	
	if not has_vehicle_asset_loaded(michaels_car.model)
		printstring("d")
		printnl()
	endif 
	
	if not GET_IS_WAYPOINT_RECORDING_LOADED("family4_0")
		printstring("e")
		printnl()
	endif 
	
	if not GET_IS_WAYPOINT_RECORDING_LOADED("family4_1")
		printstring("f")
		printnl()
	endif 
	
	if not GET_IS_WAYPOINT_RECORDING_LOADED("family4_2")
		printstring("g")
		printnl()
	endif 

	if has_model_loaded(michael.model)
	and has_model_loaded(trevor.model)
	and has_model_loaded(michaels_car.model)
	and has_vehicle_asset_loaded(michaels_car.model)
	and GET_IS_WAYPOINT_RECORDING_LOADED("family4_0")
	and GET_IS_WAYPOINT_RECORDING_LOADED("family4_1")
	and GET_IS_WAYPOINT_RECORDING_LOADED("family4_2")
	
		return true
		
	endif 
	
	return false
	
endfunc

proc setup_players_vehicle_for_return_to_gameplay()

	if not does_entity_exist(michaels_car.veh)
		if has_model_loaded(michaels_car.model)
	
			//michaels_car.veh = GET_MISSION_START_VEHICLE_INDEX() //get_players_last_vehicle() //GET_MISSION_START_VEHICLE_INDEX()
			
			if not is_players_last_vehicle_present_and_acceptable(GET_PLAYERS_LAST_VEHICLE(), michaels_car.pos, 1, <<-822.8611, 157.7590, 69.0090>>, 87.7002)
				
				CREATE_PLAYER_VEHICLE(michaels_car.veh, char_michael, michaels_car.pos, michaels_car.heading, false)
				set_vehicle_automatically_attaches(michaels_car.veh, false)
				SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(locates_data, michaels_car.veh)

			else 
			
				vehicle_index players_last_veh = GET_PLAYERS_LAST_VEHICLE()
				
				set_vehicle_automatically_attaches(players_last_veh, false)
				SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(locates_data, players_last_veh)
			
			endif 
			
		endif 
	
	endif
	
endproc

proc family_4_trevor_leadin()

	switch family_4_trevor_leadin_status 
	
		case 0
		
			start_new_cutscene_no_fade(false, true, true, true)
			
			players_last_vehicle = get_players_last_vehicle()

			//---
			//players last vehicle in the grounds of michaels house.
			if does_entity_exist(players_last_vehicle)
				if is_vehicle_driveable(players_last_vehicle)
					if is_entity_in_angled_area(players_last_vehicle, <<-817.510, 151.703, 64.606>>, <<-815.789, 192.767, 77.706>>, 61.400)   
	
						//area in the grounds of michaels house
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-817.510, 151.703, 64.606>>, <<-815.789, 192.767, 77.706>>, 61.400, michaels_car.pos, michaels_car.heading, <<15.0, 15.0, 15.0>>)
						set_players_last_vehicle_to_vehicle_gen(michaels_car.pos, michaels_car.heading)
	
						set_players_last_vehicle_to_vehicle_gen(michaels_car.pos, michaels_car.heading)
						trevors_vehicle_set_as_vehicle_gen = true 
							
					else 
		
						//SET_MISSION_VEHICLE_GEN_VEHICLE will set the vehicle as mission entity beloning to the 
						//vehicle gen controller
						//It spawns the vehicle at the new coords when the distance between the player and the vehicle
						//is greater than 310m and the distance between the player and the new spawn pos > 310m
					
						//put players last vehicle across the road from michaels house
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-860.221, 150.219, 62.021>>, <<-858.733, 175.275, 72.921>>, 31.500, car_pos_outside_house, car_heading_outside_house, <<15.0, 15.0, 15.0>>, false)//, false) //2.5, 5.5, 2.0
						set_players_last_vehicle_to_vehicle_gen(car_pos_outside_house, car_heading_outside_house)
						trevors_vehicle_set_as_vehicle_gen = true 
					
					endif 
				endif 
			endif 
			

			clear_area(<<-868.3934, 152.2284, 62.5499>>, 1000, true)
			
			if get_current_player_vehicle(players_vehicle)
			
				set_entity_heading(players_vehicle, car_heading_outside_house)
				set_entity_coords(players_vehicle, car_pos_outside_house)
				set_vehicle_on_ground_properly(players_vehicle)
			
				//no need to re-position car as it is done above.

			else 

				clear_ped_tasks_immediately(player_ped_id())
				set_entity_coords(player_ped_id(), <<-870.8275, 152.0728, 62.6623>>)
				set_entity_heading(player_ped_id(), 287.7694)
				force_ped_ai_and_animation_update(player_ped_id(), true)
			
			endif 

			task_look_at_coord(player_ped_id(), <<-831.9, 165.2, 71.3>>, 5000)
			
			spline_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true) 
			
			camera_a = create_cam_with_params("default_scripted_camera", <<-872.385559,148.430267,63.400925>>,<<8.263765,-0.000000,-63.055546>>,30.017197)
			camera_b = create_cam_with_params("default_scripted_camera", <<-869.306824,147.817139,64.735840>>,<<9.493369,0.000000,-63.179226>>,30.017197)
			
			ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(spline_cam, camera_a, 0)
			ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(spline_cam, camera_b, 5000)
			
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			
			render_script_cams(true, false)

			family_4_trevor_leadin_status++

		break 
		
		case 1
		
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)

			if not is_cam_interpolating(spline_cam)

				family_4_trevor_leadin_status++
		
			endif 
		
		break 
		
		case 2

			if DO_TIMELAPSE(SP_MISSION_FAMILY_4, sTimelapse)
			
				mission_flow = intro_mocap
			
			endif 
		
		break 
		
	endswitch 

endproc 

proc family_4_intro_mocap()

	//debug code ********
//	initialise_mission_variables()
//	mission_flow = train_crash_cutscene_test_bed
//	//mission_flow = play_final_mocap 
//	
//	#IF IS_DEBUG_BUILD
//		load_lk_widgets()
//		load_vehicle_force_widget_data()
//		SET_LOCATES_HEADER_WIDGET_GROUP(family_4_widget_group)
//	#endif 
//////
//	return true
	//end debug code *******
//	bShowSetPieceIndices = true
	//bShowTrafficIndices = true 

	switch intro_mocap_status
		
		case 0

			michaels_house_interior = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-800.31, 184.80, 72.72>>, "V_Michael")

			PIN_INTERIOR_IN_MEMORY(michaels_house_interior)

			switch get_current_player_ped_enum()
		
				case char_michael
				
					REQUEST_CUTSCENE("FAM_4_INT_ALT1")

				break 
				
				case char_trevor

					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FAM_4_INT_ALT1", CS_SECTION_3 | CS_SECTION_4)
				
				break 
				
			endswitch 
			
			//fix for lighting flash - bug 1574084
			if not new_load_scene_activated_for_michaels_house
				
				NEW_LOAD_SCENE_START(<<-797.4664, 185.0637, 72.6055>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 97.6005>>), 20.00)
				
				new_load_scene_activated_for_michaels_house = true
			
			endif 

			
			if IS_INTERIOR_READY(michaels_house_interior)
				if HAS_CUTSCENE_LOADED()

					switch get_current_player_ped_enum() 
					
						case char_michael
						
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))

						break 
						
						case char_trevor
						
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "michael", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_michael))

						break 
						
					endswitch 
					
						
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
			
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					intro_mocap_status++
				endif 
			endif 

		break 
		
		case 1
		
			if is_cutscene_playing()
			
				if new_load_scene_activated_for_michaels_house
					NEW_LOAD_SCENE_STOP()
				endif 

				SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
				
				SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", FALSE)

				SET_SRL_POST_CUTSCENE_CAMERA(michael.pos, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, michael.heading>>))
				SET_SRL_READAHEAD_TIMES(5,5,5,5) 

				//do not delete the vehicle gen in area if they have already been set by the trevor during lead in stage.
				//this will cause trevors car parked outside to be set as no longer needed then the clear area bellow 
				//will delete it.
				if not trevors_vehicle_set_as_vehicle_gen
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<-800.31, 184.80, 72.72>>, 300.00)
				endif 
				
				DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(char_michael)
				//DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(char_franklin)

				players_last_vehicle = get_players_last_vehicle()
	
				//---
				//players last vehicle in the grounds of michaels house.
				if does_entity_exist(players_last_vehicle)
					if is_vehicle_driveable(players_last_vehicle)
						if is_entity_in_angled_area(players_last_vehicle, <<-817.510, 151.703, 64.606>>, <<-815.789, 192.767, 77.706>>, 61.400)   
						
							//area in the grounds of michaels house
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-817.510, 151.703, 64.606>>, <<-815.789, 192.767, 77.706>>, 61.400, michaels_car.pos, michaels_car.heading, <<15.0, 15.0, 15.0>>)
							set_players_last_vehicle_to_vehicle_gen(michaels_car.pos, michaels_car.heading)
	
						else 
						
							//put players last vehicle across the road from michaels house
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-860.221, 150.219, 62.021>>, <<-858.733, 175.275, 72.921>>, 31.500, car_pos_outside_house, car_heading_outside_house, <<15.0, 15.0, 15.0>>)//, false) //2.5, 5.5, 2.0
							set_players_last_vehicle_to_vehicle_gen(car_pos_outside_house, car_heading_outside_house)

						
						endif 
					endif 
				
				endif 
				
				clear_area(<<-800.31, 184.80, 72.72>>, 10000, true)
				
				intro_mocap_status++
			
			endif 
		
		break 
		
		case 2
		
			request_start_of_mission_assets()
			
			has_start_of_mission_assets_loaded()

			if not WAS_CUTSCENE_SKIPPED()
			
				if is_cutscene_active()

					setup_players_vehicle_for_return_to_gameplay()
			
					switch get_player_ped_enum(player_ped_id())
					
						case char_michael

							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
								selector_ped.pedID[selector_ped_trevor] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
							ENDIF
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)) 
								if not is_ped_injured(selector_ped.pedID[selector_ped_trevor])
									set_entity_coords(selector_ped.pedID[selector_ped_trevor], trevor.pos)
									set_entity_heading(selector_ped.pedID[selector_ped_trevor], trevor.heading)
									setup_buddy_attributes(selector_ped.pedID[selector_ped_trevor])
									setup_ped_weapon(selector_ped.pedID[selector_ped_trevor], weapontype_pistol, 50)
									setup_relationship_contact(selector_ped.pedID[selector_ped_trevor])
									add_ped_for_dialogue(scripted_speech, 2, selector_ped.pedID[selector_ped_trevor], "trevor")
									
									if does_entity_exist(michaels_car.veh)
										if is_vehicle_driveable(michaels_car.veh)
											task_enter_vehicle(selector_ped.pedID[selector_ped_trevor], michaels_car.veh, 30000, vs_front_right, PEDMOVE_walk)
										endif 
									endif 
									
									force_ped_motion_state(selector_ped.pedID[selector_ped_trevor], ms_on_foot_walk, false)//, faus_cutscene_exit)
									force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_trevor])
								endif 
							endif 
			
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael)) 	
								set_entity_coords(player_ped_id(), michael.pos)
								set_entity_heading(player_ped_id(), michael.heading)
								force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
								SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
								force_ped_ai_and_animation_update(player_ped_id(), true)
								add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael")
							endif
							
						break 
						
						case char_trevor

							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael)))
								selector_ped.pedID[selector_ped_michael] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael)))
							ENDIF

							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael)) 	
								if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
									set_entity_coords(selector_ped.pedID[selector_ped_michael], trevor.pos)
									set_entity_heading(selector_ped.pedID[selector_ped_michael], trevor.heading)
									setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
									setup_ped_weapon(selector_ped.pedID[selector_ped_michael], weapontype_pistol, 50)
									setup_relationship_contact(selector_ped.pedID[selector_ped_michael])
									add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
								
									if does_entity_exist(michaels_car.veh)
										if is_vehicle_driveable(michaels_car.veh)
											task_enter_vehicle(selector_ped.pedID[selector_ped_michael], michaels_car.veh, 30000, vs_front_right, PEDMOVE_walk)
										endif 
									endif 
									force_ped_motion_state(selector_ped.pedID[selector_ped_michael], ms_on_foot_walk, false)//, faus_cutscene_exit)
									force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])
								endif 
							endif 
								
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)) 
								set_entity_coords(player_ped_id(), michael.pos)
								set_entity_heading(player_ped_id(), michael.heading)
								force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
								SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
								force_ped_ai_and_animation_update(player_ped_id(), true)
								add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
							endif 

						break 
						
					endswitch 
					
					if CAN_SET_EXIT_STATE_FOR_CAMERA()
						set_gameplay_cam_relative_heading(0)
						set_gameplay_cam_relative_pitch(0)
					endif
					
				else 
				
					if has_start_of_mission_assets_loaded()

						REPLAY_STOP_EVENT()

						create_start_of_mission_assets()
					
					endif 
				
				endif 
				
			else 
			
				SET_CUTSCENE_FADE_VALUES(false, false, true)
		
				intro_mocap_status++
			
			endif 
			
		break 
		
		case 3
		
			request_start_of_mission_assets()
		
			//if is_screen_faded_out()
				if is_cutscene_active()

					switch get_player_ped_enum(player_ped_id())
					
						case char_michael
						
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor"))
								selector_ped.pedID[selector_ped_trevor] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor"))
							ENDIF
	
						break 
						
						case char_trevor
						
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michael"))
								selector_ped.pedID[selector_ped_michael] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michael"))
							ENDIF
			
						break 
						
					endswitch
					
				else 
					
					NEW_LOAD_SCENE_START(michael.pos, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, michael.heading>>), 20.00)

					while not has_start_of_mission_assets_loaded()
					or not IS_NEW_LOAD_SCENE_LOADED()
					
						wait(0)
						
					endwhile
					
					NEW_LOAD_SCENE_STOP()

					setup_players_vehicle_for_return_to_gameplay()
				
					switch get_player_ped_enum(player_ped_id())
					
						case char_michael

							if not is_ped_injured(selector_ped.pedID[selector_ped_trevor])
								set_entity_coords(selector_ped.pedID[selector_ped_trevor], trevor.pos)
								set_entity_heading(selector_ped.pedID[selector_ped_trevor], trevor.heading)
								setup_buddy_attributes(selector_ped.pedID[selector_ped_trevor])
								setup_ped_weapon(selector_ped.pedID[selector_ped_trevor], weapontype_pistol, 50)
								setup_relationship_contact(selector_ped.pedID[selector_ped_trevor])
								add_ped_for_dialogue(scripted_speech, 2, selector_ped.pedID[selector_ped_trevor], "trevor")
								
								if does_entity_exist(michaels_car.veh)
									if is_vehicle_driveable(michaels_car.veh)
										task_enter_vehicle(selector_ped.pedID[selector_ped_trevor], michaels_car.veh, 30000, vs_front_right, PEDMOVE_walk)
									endif 
								endif 
								force_ped_motion_state(selector_ped.pedID[selector_ped_trevor], ms_on_foot_walk, false)//, faus_cutscene_exit)
								//force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_trevor])
								//script_assert("trevor weapon")
							endif 
			
							set_entity_coords(player_ped_id(), michael.pos)
							set_entity_heading(player_ped_id(), michael.heading)
							//task_follow_nav_mesh_to_coord(player_ped_id(), <<-821.7558, 179.6038, 70.5790>>, pedmove_walk)
						
							force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
							SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
							force_ped_ai_and_animation_update(player_ped_id())
							add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael")
		
						break 
						
						case char_trevor
				
							if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
								set_entity_coords(selector_ped.pedID[selector_ped_michael], trevor.pos)
								set_entity_heading(selector_ped.pedID[selector_ped_michael], trevor.heading)
								setup_buddy_attributes(selector_ped.pedID[selector_ped_michael])
								setup_ped_weapon(selector_ped.pedID[selector_ped_michael], weapontype_pistol, 50)
								setup_relationship_contact(selector_ped.pedID[selector_ped_michael])
								add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
								
								if does_entity_exist(michaels_car.veh)
									if is_vehicle_driveable(michaels_car.veh)
										task_enter_vehicle(selector_ped.pedID[selector_ped_michael], michaels_car.veh, 30000, vs_front_right, PEDMOVE_walk)
									endif 
								endif 
								force_ped_motion_state(selector_ped.pedID[selector_ped_michael], ms_on_foot_walk, false)//, faus_cutscene_exit)
								force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])
							endif 
								
							set_entity_coords(player_ped_id(), michael.pos)
							set_entity_heading(player_ped_id(), michael.heading)
							force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
							SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
							force_ped_ai_and_animation_update(player_ped_id())
							add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")

						break 
						
					endswitch 

					create_start_of_mission_assets()
						
				endif 
			//endif 
				
		break 
		
	endswitch 

endproc

proc family_4_switching_into_michael()
						
	michaels_house_interior = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-800.31, 184.80, 72.72>>, "V_Michael")
	PIN_INTERIOR_IN_MEMORY(michaels_house_interior)

	REQUEST_CUTSCENE("FAM_4_INT_ALT1")
	SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_COMPLETELY_OFF)//need to do this whilst loading a cutscene whilst doing a cutscene
	SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)

	if IS_PLAYER_SWITCH_IN_PROGRESS() 
	
		SWITCH_STATE camera_switch_state
		camera_switch_state = GET_PLAYER_SWITCH_STATE()
	
		//if (camera_switch_state != SWITCH_STATE_OUTRO_SWOOP)
		if (camera_switch_state != SWITCH_STATE_OUTRO_HOLD)
	
			//alwyns code will hold on switch_state_hold in the event all the assets have not streamed in
			if not allow_switch_to_contiune
				
				IF ((camera_switch_state >= SWITCH_STATE_JUMPCUT_DESCENT) and (GET_PLAYER_SWITCH_JUMP_CUT_INDEX() < 1))  
				and IS_INTERIOR_READY(michaels_house_interior)
				and HAS_CUTSCENE_LOADED()
			
					ar_ALLOW_PLAYER_SWITCH_OUTRO()

					allow_switch_to_contiune = true
					
//					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))
//
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
//
//					START_CUTSCENE()
//
//					intro_mocap_status = 1
//					
//					mission_flow = intro_mocap	
					
				endif 
				
			endif 
			
		else 
		
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))

			SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)

			START_CUTSCENE()

			intro_mocap_status = 1

			mission_flow = intro_mocap	
	
		endif 
		
	elif has_cutscene_loaded()
	
		if not is_cutscene_playing()
	
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))

			SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)

			START_CUTSCENE()

			intro_mocap_status = 1
			
			mission_flow = intro_mocap	
			
			script_assert("SWITCH_IN_PROGRESS something gone wrong")
			
		endif 
			
	endif  

endproc 


//proc family_4_get_the_truck()
//	
//	get_the_truck_blipping_system()
//	
//	if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
//			
//		if does_blip_exist(truck.blip)
//			remove_blip(truck.blip)
//		endif 
//		
//		set_max_wanted_level(4)
//		clear_player_wanted_level(player_id())
//		set_player_wanted_level(player_id(), 4)
//		SET_PLAYER_WANTED_LEVEL_NOW(player_id())
//		//RESET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER()
//
//		clear_prints()
//		print_now("family4_god_2", default_god_text_time, 1)
//		
//		original_time = get_game_timer()
//		
//		mission_flow = lose_the_cops
//			
//	endif 
//	
//	//******TEMP
//	temp_blip = add_blip_for_coord(<<-841.5194, 7.6417, 41.8163>>)
//	set_blip_route(temp_blip, true)
//	//******END TEMP
//	
//	mission_flow = lose_the_cops
//
//endproc 


proc truck_blipping_system()

	if not deactivate_truck_blipping_system

		IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(locates_data, get_entity_coords(lazlow.ped), <<0.01, 0.01, 0.01>>, false, selector_ped.pedID[selector_ped_michael], truck.veh, "", "family4_god_14", "", "family4_god_3")  
			
		if does_blip_exist(locates_data.LocationBlip)

			set_blip_alpha(locates_data.LocationBlip, 0)
			set_blip_route(locates_data.LocationBlip, false)
			
			if not does_blip_exist(lazlow.blip)
				
				lazlow.blip = create_blip_for_ped(lazlow.ped, true) 
				SET_BLIP_NAME_FROM_TEXT_FILE(lazlow.blip, "family4_god_17")
				set_blip_scale(lazlow.blip, 1.0)
			
			else
				
				if is_ped_sitting_in_vehicle(lazlow.ped, lazlows_car.veh)
					set_blip_scale(lazlow.blip, 1.0)
				else
					set_blip_scale(lazlow.blip, 0.5)
				endif 
				
			endif 
			
			UPDATE_CHASE_BLIP(lazlow.blip, lazlow.ped, 200.00)
		
		else 
		
			if does_blip_exist(lazlow.blip)
				remove_blip(lazlow.blip)
			endif
		
		endif 
		
	endif 
	
endproc 

proc police_car_creation()

//	vector players_offset
//	vector players_pos
//	vector node_face_vector
//	
//	float angle_dif = 0
//
//						
//	players_pos = get_entity_coords(player_ped_id())
//	players_offset = get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 180.00, 0.0>>)
//	
//	GET_NTH_CLOSEST_VEHICLE_NODE_FAVOUR_DIRECTION(players_offset, players_pos, 1, random_police_car.pos, random_police_car.heading)
//	
//	node_face_vector = << -SIN(random_police_car.heading), COS(random_police_car.heading), 0.0 >>	                 // Forward vector of node
//	angle_dif = GET_ANGLE_BETWEEN_2D_VECTORS(node_face_vector.x, node_face_vector.y, (players_pos.x - random_police_car.pos.x), (players_pos.y - random_police_car.pos.y))
//
//	// Is node pointing away from player?
//	IF angle_dif > 90.0		                     
//		random_police_car.heading += 180.0
//	ENDIF
//	
//	random_police_car.veh = create_vehicle(random_police_car.model, random_police_car.pos, random_police_car.heading)
//
//	
//	for i = 0 to 1
//	
//		if i = 0
//			random_police_man[i].ped = create_ped_inside_vehicle(random_police_car.veh, pedtype_cop, random_police_man[i].model) 
//		else 
//			random_police_man[i].ped = create_ped_inside_vehicle(random_police_car.veh, pedtype_cop, random_police_man[i].model, vs_front_right) 
//		endif 
//		
//		set_ped_random_component_variation(random_police_man[i].ped)
//		give_weapon_to_ped(random_police_man[i].ped, weapontype_pistol, infinite_ammo, true) 
//		set_current_ped_weapon(random_police_man[i].ped, weapontype_pistol, true)
//		set_ped_keep_task(random_police_man[i].ped, true)
//		
//	endfor 	
//
//	SET_PED_AS_NO_LONGER_NEEDED(random_police_man[0].ped)
//	SET_PED_AS_NO_LONGER_NEEDED(random_police_man[1].ped)
//	set_vehicle_as_no_longer_needed(random_police_car.veh)

endproc 

func bool truck_wanted_level_system()

	//fix for if player goes to airport with a 2 star wanted level when the airport automatically increases the 
	//wanted level to 4
	if is_player_wanted_level_greater(player_id(), 3)
		if truck_wanted_status > 0
			
			set_max_wanted_level(4)
			clear_player_wanted_level(player_id())
			set_player_wanted_level(player_id(), 4)
			SET_PLAYER_WANTED_LEVEL_NOW(player_id())
			//RESET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER()
		
			original_time = get_game_timer()
			
			truck_wanted_status = 0
			
		endif 
	endif 
	
//	printint(GET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER())
//	printnl()
			
	switch truck_wanted_status 
	
		case 0

			if not lk_timer(original_time, 85000) //and not (GET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER() >= 7)
				set_player_wanted_level(player_id(), 4)
				SET_PLAYER_WANTED_LEVEL_NOW(player_id())

				if not police_car_created
					if lk_timer(original_time, 22000)
						police_car_creation()
						police_car_created = true
					endif 
				endif 
					
			else 
				//RESET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER()
				set_max_wanted_level(3)
				set_player_wanted_level(player_id(), 3)
				SET_PLAYER_WANTED_LEVEL_NOW(player_id())
				original_time = get_game_timer()
				police_car_creation()
				police_car_created = false
				truck_wanted_status++
			endif 
			
		break 
		
		case 1
		
			if not lk_timer(original_time, 85000) //and not (GET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER() >= 6)
				set_player_wanted_level(player_id(), 3)
				SET_PLAYER_WANTED_LEVEL_NOW(player_id())
				
				if not police_car_created
					if lk_timer(original_time, 22000)
						police_car_creation()
						police_car_created = true
					endif 
				endif 
			else
				//RESET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER()
				set_max_wanted_level(2)
				set_player_wanted_level(player_id(), 2)
				SET_PLAYER_WANTED_LEVEL_NOW(player_id())
				original_time = get_game_timer()
				police_car_creation()
				police_car_created = false
				truck_wanted_status++
			endif 
			
		break 
		
		case 2
		
			if not lk_timer(original_time, 85000) //and not (GET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER() >= 5)
				set_player_wanted_level(player_id(), 2)
				SET_PLAYER_WANTED_LEVEL_NOW(player_id())
				
				if not police_car_created
					if lk_timer(original_time, 22000)
						police_car_creation()
						police_car_created = true
					endif 
				endif 
			else
				//RESET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER()
				clear_player_wanted_level(player_id())
				set_wanted_level_multiplier(0.0)
				set_max_wanted_level(1)
				set_player_wanted_level(player_id(), 1)
				SET_PLAYER_WANTED_LEVEL_NOW(player_id())
				original_time = get_game_timer()
				police_car_creation()
				police_car_created = false
				
//				clear_prints()
//				print_now("Y3_COPS", default_god_text_time, 1)
				
				truck_wanted_status++
			endif 
			
		break 
		
		
		case 3
		
			if not is_player_wanted_level_greater(player_id(), 0) //or (GET_NO_LAW_VEHICLES_DESTROYED_BY_LOCAL_PLAYER() >= 2)
				
				clear_player_wanted_level(player_id())
				//g_allowmaxwantedlevelcheck = false
				set_max_wanted_level(0)
				set_create_random_cops(false)

				return true
			endif 
		
		break 
		
	endswitch 

	return false

endfunc

proc lazlow_ai_system()

	switch lazlow_ai_system_status 
	
		case 0
		
			request_anim_dict("missfamily4_fameshame")
		
			if is_entity_at_coord(player_ped_id(), <<-252.8894, -1997.5737, 29.1458>>, <<1.5, 1.5, 1.5>>)
			or is_entity_at_coord(selector_ped.pedID[selector_ped_michael], <<-252.8894, -1997.5737, 29.1458>>, <<1.5, 1.5, 1.5>>)
		
				open_sequence_task(seq)
					task_force_motion_state(null, ENUM_TO_INT(MS_ON_FOOT_RUN))
					TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_0", 12, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
				close_sequence_task(seq)
				task_perform_sequence(lazlow.ped, seq)
				clear_sequence_task(seq)
				
				force_ped_ai_and_animation_update(lazlow.ped)
				
				lazlow_ai_system_status++
				
			endif 

		break 
		
		case 1
		
			vector target_waypoint_pos
			int waypoint_node_pos
		
			
			if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(lazlow.ped)
//				if is_entity_in_angled_area(lazlow.ped, <<-244.914, -2007.479, 23.691>>, <<-244.325, -2004.946, 26.191>>, 4.300)     
//					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(lazlow.ped, 2.0)
//				else
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(lazlow.ped, 3.0) //2.0 3.0
//				endif 
			endif 
			
			if waypoint_recording_get_coord("family4_0", 48, target_waypoint_pos)  //52 59
				if not IS_ENTITY_AT_COORD(lazlow.ped, target_waypoint_pos, <<1.5, 1.5, 1.6>>, false, true)
					if has_ped_task_finished_2(lazlow.ped, script_task_perform_sequence)
					
						if waypoint_recording_get_closest_waypoint("family4_0", GET_ENTITY_COORDS(lazlow.ped), waypoint_node_pos)
						
							waypoint_node_pos += 1
						
							if waypoint_node_pos > 59
								waypoint_node_pos = 59
							endif 
					
							set_blocking_of_non_temporary_events(lazlow.ped, true)
							clear_ped_tasks(lazlow.ped)
							
							open_sequence_task(seq)
								task_follow_waypoint_recording(null, "family4_0", waypoint_node_pos, ewaypoint_navmesh_to_initial_waypoint)
							close_sequence_task(seq)
							task_perform_sequence(lazlow.ped, seq)
							clear_sequence_task(seq)
						endif 
					endif 
				
				 else 

				 	open_sequence_task(seq)
						task_go_straight_to_coord(null, get_anim_initial_offset_position("missfamily4_fameshame", "lazlow_getincar_l", <<-228.974, -2048.902, 27.120>>, <<0.0, 0.0, -122.436>>, 0.0), pedmove_run, -1) 
					close_sequence_task(seq)
					task_perform_sequence(lazlow.ped, seq)
					clear_sequence_task(seq)
					
					lazlow_ai_system_status++
					
				endif 
				 
			endif
		
		break 
		
		case 2

			if has_anim_dict_loaded("missfamily4_fameshame")
			and does_entity_exist(lazlows_car.veh)
				
				if is_entity_at_coord(lazlow.ped, get_anim_initial_offset_position("missfamily4_fameshame", "lazlow_getincar_l", <<-228.974, -2048.902, 27.120>>, <<0.0, 0.0, -122.436>>, 0.0), <<0.5, 0.5, 1.6>>)
				or has_ped_task_finished_2(lazlow.ped, script_task_perform_sequence)

					set_entity_no_collision_entity(lazlow.ped, lazlows_car.veh, false)
					
					scene_pos = <<-228.974, -2048.902, 27.120>>
					scene_rot = <<0.0, 0.0, -122.436>>
					lazlow.scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)
					
					TASK_SYNCHRONIZED_SCENE(lazlow.ped, lazlow.scene_id, "missfamily4_fameshame", "lazlow_getincar_l", slow_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)

					PLAY_SYNCHRONIZED_ENTITY_ANIM(lazlows_car.veh, lazlow.scene_id, "lazlow_getincar_car", "missfamily4_fameshame", INSTANT_BLEND_IN, normal_BLEND_OUT)
					
					SET_SYNCHRONIZED_SCENE_RATE(lazlow.scene_id, 1.15)

					lazlow_ai_system_status++
					
				endif 
				
			endif 

		break 
		
		case 3

			if is_synchronized_scene_running(lazlow.scene_id)
				if get_synchronized_scene_phase(lazlow.scene_id) > 0.93
				
					set_blip_scale(lazlow.blip, 1.0)
				
					STOP_SYNCHRONIZED_ENTITY_ANIM(lazlows_car.veh, instant_blend_in, true)
					clear_ped_tasks(lazlow.ped)
					set_ped_into_vehicle(lazlow.ped, lazlows_car.veh)
					SET_VEHICLE_DOORS_SHUT(lazlows_car.veh, true)

					SET_VEHICLE_POPULATION_BUDGET(1)
					SET_PED_POPULATION_BUDGET(2)

					clear_player_wanted_level(player_id())
					set_max_wanted_level(0)
					set_create_random_cops(false)
					set_wanted_level_multiplier(0.0)
					
					#IF IS_DEBUG_BUILD
						set_uber_parent_widget_group(family_4_widget_group)
					#endif 
					INITIALISE_UBER_PLAYBACK("lkfamily4", 001, true)
					load_uber_data()
					fUberPlaybackDensitySwitchOffRange = 200
					bCreateAllWaitingCars = true
					bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = true 
					allow_veh_to_stop_on_any_veh_impact = true
//							bTrafficDontCleanupRecordingFiles = true
					allow_trailer_touching_check = true
					traffic_block_vehicle_colour(true, traffic_red, traffic_orange)
					start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4")
					update_uber_playback(lazlows_car.veh, 0.7)
//							SET_UBER_PLAYBACK_TO_TIME_NOW(lazlows_car.veh, 2500.00)
//							update_uber_playback(lazlows_car.veh, 1.0)
					force_playback_recorded_vehicle_update(lazlows_car.veh)
					set_entity_proofs(lazlows_car.veh, false, true, false, true, true)
					
					INFORM_MISSION_STATS_OF_FINANCE_MODEL(sentinel, 100)
					INFORM_MISSION_STATS_OF_FINANCE_MODEL(schafter2, 100)
					INFORM_MISSION_STATS_OF_FINANCE_MODEL(taco, 100)
				
					lazlow_ai_system_status++

				endif 
			endif 

		break 
		
		case 4
		
		break 
		
	endswitch 

endproc

proc cleanup_run_after_lazlo_assets()

	int i = 0

	ASSISTED_MOVEMENT_remove_ROUTE("fame1")

	delete_ped(tracey.ped)
	set_model_as_no_longer_needed(tracey.model)
	
	delete_object(video_camera.obj)
	set_model_as_no_longer_needed(video_camera.model)
	
	delete_object(micro_phone.obj)
	set_model_as_no_longer_needed(micro_phone.model)
	
	for i = 0 to count_of(camera_crew) - 1
		delete_ped(camera_crew[i].ped)
		set_model_as_no_longer_needed(camera_crew[i].model)
	endfor 

endproc 

proc create_parked_vehicles_system()
	
	switch create_parked_vehicles_status 
	
		case 0

			if get_time_position_in_recording(lazlows_car.veh) > 142000.00
		
				request_model(towtruck)
				request_model(schafter2)
				set_vehicle_model_is_suppressed(towtruck, true)
				set_vehicle_model_is_suppressed(schafter2, true)
			
				if has_model_loaded(towtruck) and has_model_loaded(schafter2)
					
					broken_down_cars[0] = create_vehicle(towtruck, <<690.6760, -380.5773, 39.6706>>, 301.5845)
					broken_down_cars[1] = create_vehicle(schafter2, <<685.2643, -384.7363, 39.7185>>, 312.2964)

					create_parked_vehicles_status++
				endif 
				
			endif 

		break 
		
		case 1
		
		break 
		
	endswitch 

endproc

proc dialogue_system()

	if does_blip_exist(lazlow.blip)
	
		if is_playback_going_on_for_vehicle(lazlows_car.veh)
		
			//secondary_audio = false
		
			//specific dialogue which must trriger e.g. turn left / right etc
			
			switch dialogue_status 
			
				case 0
					
					if is_entity_in_angled_area(player_ped_id(), <<-187.434, -1998.193, 26.626>>, <<-189.068, -2012.098, 33.626>>, 5.0) 
						
						get_dialogue_data_for_future_resumption()
						
						if create_conversation(scripted_speech, "fam4aud", "FAM4_LEF", CONV_PRIORITY_medium)
							printstring("turn left dialogue")
							printnl()
							printstring("turn left dialogue")
							printnl()
							printstring("turn left dialogue")
							printnl()
							dialogue_status++
						else
							printstring("can't play dialogue")
							printnl()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
						endif 
					endif
					
					//safety system incase player doesn't go down slope
					if get_time_position_in_recording(lazlows_car.veh) > 40000.00
						dialogue_status++
					endif 
				
				break 
			
				case 1
				
					if get_time_position_in_recording(lazlows_car.veh) > 47150.920 //47050.920 47350.920
						if ((get_distance_between_coords(get_entity_coords(lazlows_car.veh), get_entity_coords(player_ped_id())) < 150
						or is_entity_on_screen(lazlows_car.veh))
						and not lazlow_chase_cam_active)
							
							get_dialogue_data_for_future_resumption()
							
							if create_conversation(scripted_speech, "fam4aud", "FAM4_LEF", CONV_PRIORITY_medium)
								dialogue_status++
							endif
						else 
							dialogue_status++
						endif 
					endif 

				break 
				
				case 2
				
					if get_time_position_in_recording(lazlows_car.veh) > 56283.160
						if ((get_distance_between_coords(get_entity_coords(lazlows_car.veh), get_entity_coords(player_ped_id())) < 120
						or is_entity_on_screen(lazlows_car.veh))
						and not lazlow_chase_cam_active)
							
							get_dialogue_data_for_future_resumption()
							
							if create_conversation(scripted_speech, "fam4aud", "fam4_tline", CONV_PRIORITY_medium)
								
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								start_audio_scene("FAMILY_4_CHASE_TRUCK_TRACKS")
								
								dialogue_status++
							endif
						else 
							dialogue_status++
						endif 
					endif 

				break 
				
				case 3
				
					if get_time_position_in_recording(lazlows_car.veh) > 70551.430
						if ((get_distance_between_coords(get_entity_coords(lazlows_car.veh), get_entity_coords(player_ped_id())) < 120
						or is_entity_on_screen(lazlows_car.veh))
						and not lazlow_chase_cam_active)
							
							get_dialogue_data_for_future_resumption()
							
							if create_conversation(scripted_speech, "fam4aud", "FAM4_LEF", CONV_PRIORITY_medium)
								dialogue_status++
							endif
						else 
							dialogue_status++
						endif 
						
						if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_TRACKS")
							stop_audio_scene("FAMILY_4_CHASE_TRUCK_TRACKS")
						endif 
					endif 
				
				break 
				
				case 4
				
					if get_time_position_in_recording(lazlows_car.veh) > 90100.550 //90280.550
						if ((get_distance_between_coords(get_entity_coords(lazlows_car.veh), get_entity_coords(player_ped_id())) < 120) 
						or is_entity_on_screen(lazlows_car.veh)
						and not lazlow_chase_cam_active)
							
							get_dialogue_data_for_future_resumption()
							
							if create_conversation(scripted_speech, "fam4aud", "FAM4_RIG", CONV_PRIORITY_medium)
								
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								dialogue_status++
							endif 
						else 
							dialogue_status++
						endif 
					endif 
				
				break 
				
				case 5
				
					if get_time_position_in_recording(lazlows_car.veh) > 102000.400
						if ((get_distance_between_coords(get_entity_coords(lazlows_car.veh), get_entity_coords(player_ped_id())) < 120
						or is_entity_on_screen(lazlows_car.veh))
						and not lazlow_chase_cam_active)
							
							get_dialogue_data_for_future_resumption()
							
							if create_conversation(scripted_speech, "fam4aud", "FAM4_RIG", CONV_PRIORITY_medium)
								dialogue_status++
							endif
						else 
							dialogue_status++
						endif 
					endif 
				
				break 
				
				case 6
																		
					if get_time_position_in_recording(lazlows_car.veh) > 126768.000
						if ((get_distance_between_coords(get_entity_coords(lazlows_car.veh), get_entity_coords(player_ped_id())) < 120
						or is_entity_on_screen(lazlows_car.veh))
						and not lazlow_chase_cam_active)
							
							get_dialogue_data_for_future_resumption()
							
							if create_conversation(scripted_speech, "fam4aud", "FAM4_RIV", CONV_PRIORITY_medium)
								
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								dialogue_status++
							endif 
						else 
							dialogue_status++
						endif  
					endif 
				
				break 
				
				case 7
				
					
				
				break 
				
				case 22
				
				break
				
			endswitch 
			
			if lk_timer(ambient_ped_time, 5000)
//				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			
//					vehicle_index closest_vehicle = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10.0, DUMMY_MODEL_FOR_SCRIPT, GET_TRAFFIC_VEHICLE_SEARCH_FLAGS())
//					
//					if closest_vehicle != lazlows_car.veh
//						if not is_entity_dead(closest_vehicle)
//							
//							if is_entity_touching_entity(truck.veh, closest_vehicle)
//							
//								ped_index angry_driver = GET_PED_IN_VEHICLE_SEAT(closest_vehicle)
//								
//								if not is_ped_injured(angry_driver)
//
//									PLAY_PED_AMBIENT_SPEECH(angry_driver, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_NORMAL)
//									ambient_ped_time = GET_GAME_TIMER()
//									
//								endif 
//								
//							endif 
//						endif 
//					endif 
//				endif
			endif 
			
			
			
			
			//**********normal in car conversation which is lower priority than above specific dialogue.
		
//			time in seconds for the turn left / right dialogue 
//			47350.920
//			56283.160
//			70551.430
//			90100.550
//			102000.400
//			126768.000

			//conversation dialogue
//			10000
//			25000
//			62000
//			74000
//			103000 //plays for 20s = 123000
//			122000 //removed
//			130000
		
			if not lazlow_chase_cam_active

				switch uber_chase_in_car_conversation
				
					case 0
					
						if not is_any_text_being_displayed(locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							if create_conversation(scripted_speech, "fam4aud", "fam4_c0", CONV_PRIORITY_low)
								
								REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0, REPLAY_IMPORTANCE_HIGH)

								uber_chase_in_car_conversation++
							endif 
						endif 
						
					break 
					
					case 1
					
						if does_blip_exist(lazlow.blip)
							if is_entity_in_angled_area(player_ped_id(), <<-165.656, -1985.216, 22.043>>, <<-127.556, -1985.216, 27.643>>, 4.0) 
							or get_time_position_in_recording(lazlows_car.veh) > 20000
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "fam4aud", "FAM4_C1", CONV_PRIORITY_low)
										uber_chase_in_car_conversation++
									endif 
								endif 
							endif 
						endif 
					
					break 
					
					case 2
					
						if dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
							if create_conversation(scripted_speech, "fam4aud", "FAM4_WAY", CONV_PRIORITY_low)
								uber_chase_in_car_conversation++
							endif 
						endif 
						
					break 
					
					case 3

						if does_blip_exist(lazlow.blip)
							if get_time_position_in_recording(lazlows_car.veh) > 25000
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "fam4aud", "FAM4_C2", CONV_PRIORITY_low)
										uber_chase_in_car_conversation++
									endif 
								endif 
							endif
						endif 

					break 
					
					case 4
					
						if not is_any_text_being_displayed(locates_data)
							if create_conversation(scripted_speech, "fam4aud", "FAM4_CU2", CONV_PRIORITY_low)
								uber_chase_in_car_conversation++
							endif 
						endif 
					
					break 
					
					case 5
					
//						if dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
//							if get_time_position_in_recording(lazlows_car.veh) < 41000
//								if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 40.00
//									if create_conversation(scripted_speech, "fam4aud", "lazlo_chase", CONV_PRIORITY_low)
//										uber_chase_in_car_conversation++
//									endif 
//								endif 
//							else
								uber_chase_in_car_conversation++
//							endif 
//						endif 

					break 
					
					
					case 6
					
						if dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
							if get_time_position_in_recording(lazlows_car.veh) > 62000
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "fam4aud", "FAM4_C3", CONV_PRIORITY_low)
										uber_chase_in_car_conversation++
									endif 
								endif 
							endif
						endif 

					break 
				
					
					case 7
					
						if dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
							uber_chase_in_car_conversation++
						endif 
						
					break 
					
					case 8
					
						if does_blip_exist(lazlow.blip)
							if get_time_position_in_recording(lazlows_car.veh) > 74000
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "fam4aud", "FAM4_C4", CONV_PRIORITY_low)
										
										if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_1")
											STOP_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_1")
										endif 
										
										START_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_2")
										
										uber_chase_in_car_conversation++
									endif 
								endif 
							endif
						endif 
						
					break 
					
					case 9
					
						if dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
							uber_chase_in_car_conversation++
						endif 
					
					break 
					
					case 10
					
						if does_blip_exist(lazlow.blip)
							if get_time_position_in_recording(lazlows_car.veh) > 103000
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "fam4aud", "FAM4_C5", CONV_PRIORITY_low)
										//script_assert("test 0")
										uber_chase_in_car_conversation++
									endif 
								endif 
							endif
						endif 
						
					break 
					
					case 11
					
						if dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
							uber_chase_in_car_conversation++
						endif 
						
					break 

					
					case 12
					
						if dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
							uber_chase_in_car_conversation++
						endif 
					
					break 
					
					case 13
					
						if does_blip_exist(lazlow.blip)
							if get_time_position_in_recording(lazlows_car.veh) > 131000
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "fam4aud", "FAM4_C7", CONV_PRIORITY_low)
										uber_chase_in_car_conversation++
									endif 
								endif 
							endif
						endif 
					
					break 
					
					case 14
					
						if does_blip_exist(lazlow.blip)
							if get_time_position_in_recording(lazlows_car.veh) > 140000
								kill_any_conversation()
								uber_chase_in_car_conversation++
							endif 
						endif 
					
						dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
	
					break
					
					case 15
					
					break 
					
				endswitch 
				
			endif 
			
		endif 
		
	else 
	
		dialogue_monitoring_system(lazlow.blip, selector_ped.pedID[selector_ped_michael])
	
		get_dialogue_data_for_future_resumption()

	
//		if not dialogue_paused
//		
//			if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//
//				dialogue_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT() 
//				
//				IF ARE_STRINGS_EQUAL(dialogue_root, "fbi1_doc3")
//				
//					specific_label = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
//	
//					dialogue_paused = true 
//							
//				endif 
//			
//			endif 
//		
//		endif 
	
//		player_leaves_vehicle_dialogue("fam4aud", "FAM4_RIG", "family4_god_3", get_back_in_veh_text_printed, secondary_audio)

	endif 


endproc 

proc set_piece_driver_ai()

	switch set_piece_driver_ai_status 
	
		case 0
		
//			printstring("test 0")
//			printnl()

			if does_entity_exist(SetPieceCarID[1])
				if is_vehicle_driveable(SetPieceCarID[1])
						
					set_piece_veh = SetPieceCarID[1]
					set_piece_ped = get_ped_in_vehicle_seat(SetPieceCarID[1])
					
					if does_entity_exist(set_piece_ped)
						set_piece_driver_ai_status++
					endif 
					
				endif 
			endif 
			
		break 
		
		case 1
		
			if is_vehicle_driveable(SetPieceCarID[1])
				if is_playback_going_on_for_vehicle(set_piece_veh)
					set_piece_driver_ai_status++
				endif 
			endif 
		
		break 
		
		case 2

//			if is_vehicle_driveable(set_piece_veh)
//				if not is_playback_going_on_for_vehicle(set_piece_veh)
//					if not is_ped_injured(set_piece_ped)
//					
//						set_ped_keep_task(set_piece_ped, true)
//				
//						open_sequence_task(seq)
//							task_leave_vehicle(null, set_piece_veh)
//							task_hands_up(null, 3000)
//							task_vehicle_drive_wander(null, set_piece_veh, 10, DRIVINGMODE_STOPFORCARS) 
//						close_sequence_task(seq)
//						task_perform_sequence(set_piece_ped, seq)
//						clear_sequence_task(seq)
//						
//						set_piece_driver_ai_status++
//					endif 
//				endif 
//			endif 
						
		break 
		
		case 3
		
		break 
		
	endswitch 
	
endproc 


proc lazlow_horn_system()
	
	if lk_timer(lazlow_horn_time, 35000)
	
		start_vehicle_horn(lazlows_car.veh, 2500)
		
		lazlow_horn_time = get_game_timer()
	
	endif 
		
endproc 

proc uber_speed_system()

	if is_vehicle_driveable(lazlows_car.veh)
		if is_playback_going_on_for_vehicle(lazlows_car.veh)
		
			printfloat(current_ideal_distance)
			printnl()

			switch uber_speed_status
			
				case 0

					if get_time_position_in_recording(lazlows_car.veh) < 12000 
						
						if not is_entity_on_screen(lazlows_car.veh)
							
							lazlows_car.speed = 0.3

							
						else 
						
							calculate_new_playback_speed_from_char(lazlows_car.veh, PLAYER_ped_ID(), lazlows_car.speed, 1.0, 10.0, 20.0, 50, 2.0, 1.0, 0.7)
							
						endif 
						
					else 
					
						SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING) 
					
						//reset uber traffic flags
						bCreateAllWaitingCars = false
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = false
					
						CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, 10.0, 20.0, 50, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
						UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)

						uber_speed_status++
					endif 

					update_uber_playback(lazlows_car.veh, lazlows_car.speed)

				break 

				case 1
				
					if get_time_position_in_recording(lazlows_car.veh) > 43521.090 //54000
						
						current_minimum_distance = 10.0
						current_ideal_distance = 20.0
						current_slow_down_distance = 50.00
						
						REPLAY_RECORD_BACK_FOR_TIME(3)
						
						uber_speed_status++
					endif 

//					calculate_new_playback_speed_from_char(lazlows_car.veh, PLAYER_ped_ID(), lazlows_car.speed, 1.0, 10.0, 20.0, 40.0, 2.0, 1.0, 0.7)
//					update_uber_playback(lazlows_car.veh, lazlows_car.speed)
					
					
					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, 10.0, 20.0, 50, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)
					update_uber_playback(lazlows_car.veh, lazlows_car.speed)

					
					//slow the player down via modify_vehicle_top_speed when you want lazlow to go faster. 
					//speak with Matt
					
				break 
				
				case 2
				
					if get_time_position_in_recording(lazlows_car.veh) > 52234.770 
						uber_speed_status++
					endif 
					
					multiplier_speed = 1.2
					
					current_minimum_distance += get_frame_time() * multiplier_speed 
					if current_minimum_distance > 20.00
						current_minimum_distance = 20
					endif 
					
					current_ideal_distance += get_frame_time() * multiplier_speed 
					if current_ideal_distance > 30.0
						current_ideal_distance = 30.0
					endif 
					
					current_slow_down_distance += get_frame_time() * multiplier_speed 
					if current_slow_down_distance > 60.0
						current_slow_down_distance = 60.0
					endif 

					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, current_minimum_distance, current_ideal_distance, current_slow_down_distance, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)
					update_uber_playback(lazlows_car.veh, lazlows_car.speed)
					
					
					//when setting the current_minimum_distance = 20 in one frame then make the smoothing accel 
					//half the value 10 / 20 = 0.5
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed, 0.01 * 0.5)

				break 
				
				case 3
				
					if get_time_position_in_recording(lazlows_car.veh) > 66924.330
						REPLAY_RECORD_BACK_FOR_TIME(3)
						uber_speed_status++
					endif
					
					multiplier_speed = 1.2
				
					current_minimum_distance -= get_frame_time() * multiplier_speed 
					if current_minimum_distance < 10.00
						current_minimum_distance = 10
					endif 
					
					current_ideal_distance -= get_frame_time() * multiplier_speed 
					if current_ideal_distance < 20.0
						current_ideal_distance = 20.0
					endif 
					
					current_slow_down_distance -= get_frame_time() * multiplier_speed 
					if current_slow_down_distance < 50.0
						current_slow_down_distance = 50.0
					endif 
				
					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, current_minimum_distance, current_ideal_distance, current_slow_down_distance, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)
					update_uber_playback(lazlows_car.veh, lazlows_car.speed)
				
				break 
				
				case 4
				
					if is_entity_in_angled_area(player_ped_id(), <<487.246, -879.994, 9.750>>, <<651.344, -879.135, 32.450>>, 4.0)
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "half way through truck chase", true)
					endif 
						
					if get_time_position_in_recording(lazlows_car.veh) > 76193.880
						uber_speed_status++
					endif
				
					multiplier_speed = 1.2
					
					current_minimum_distance += get_frame_time() * multiplier_speed 
					if current_minimum_distance > 20.00
						current_minimum_distance = 20
					endif 
					
					current_ideal_distance += get_frame_time() * multiplier_speed 
					if current_ideal_distance > 30.0
						current_ideal_distance = 30.0
					endif 
					
					current_slow_down_distance += get_frame_time() * multiplier_speed 
					if current_slow_down_distance > 60.0
						current_slow_down_distance = 60.0
					endif 

					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, current_minimum_distance, current_ideal_distance, current_slow_down_distance, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)
					update_uber_playback(lazlows_car.veh, lazlows_car.speed)
				
				break 
				
				case 5
				
					if get_time_position_in_recording(lazlows_car.veh) > 84967.060
						REPLAY_RECORD_BACK_FOR_TIME(3)
						uber_speed_status++
					endif
				
					multiplier_speed = 1.2
				
					current_minimum_distance -= get_frame_time() * multiplier_speed 
					if current_minimum_distance < 10.00
						current_minimum_distance = 10
					endif 
					
					current_ideal_distance -= get_frame_time() * multiplier_speed 
					if current_ideal_distance < 20.0
						current_ideal_distance = 20.0
					endif 
					
					current_slow_down_distance -= get_frame_time() * multiplier_speed 
					if current_slow_down_distance < 50.0
						current_slow_down_distance = 50.0
					endif 
					
					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, current_minimum_distance, current_ideal_distance, current_slow_down_distance, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)
					update_uber_playback(lazlows_car.veh, lazlows_car.speed)
	

				break 
				
				case 6
				
					if get_time_position_in_recording(lazlows_car.veh) > 126768.000 //128070.600
						REPLAY_RECORD_BACK_FOR_TIME(3)
						uber_speed_status++
					endif
				
					multiplier_speed = 1.2
					
					current_minimum_distance += get_frame_time() * multiplier_speed 
					if current_minimum_distance > 20.00
						current_minimum_distance = 20
					endif 
					
					current_ideal_distance += get_frame_time() * multiplier_speed 
					if current_ideal_distance > 30.0
						current_ideal_distance = 30.0
					endif 
					
					current_slow_down_distance += get_frame_time() * multiplier_speed 
					if current_slow_down_distance > 60.0
						current_slow_down_distance = 60.0
					endif 

					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, current_minimum_distance, current_ideal_distance, current_slow_down_distance, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)
					update_uber_playback(lazlows_car.veh, lazlows_car.speed)
				
				break 

				case 7
				
					if get_time_position_in_recording(lazlows_car.veh) > 128070.600 //134007.200 
						uber_speed_status++
					endif
				
					multiplier_speed = 5.0//1.2
					
					current_minimum_distance += get_frame_time() * multiplier_speed 
					if current_minimum_distance > 25.00
						current_minimum_distance = 25
					endif 
					
					current_ideal_distance += get_frame_time() * multiplier_speed 
					if current_ideal_distance > 35.0
						current_ideal_distance = 35.0
					endif 
					
					current_slow_down_distance += get_frame_time() * multiplier_speed 
					if current_slow_down_distance > 65.0
						current_slow_down_distance = 65.0
					endif 

					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, current_minimum_distance, current_ideal_distance, current_slow_down_distance, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)
					update_uber_playback(lazlows_car.veh, lazlows_car.speed)

				break 
				
				case 8
				
					multiplier_speed = 8.0
				
					current_minimum_distance += get_frame_time() * multiplier_speed 
					if current_minimum_distance > 180
						current_minimum_distance = 180
					endif 
					
					current_ideal_distance += get_frame_time() * multiplier_speed 
					if current_ideal_distance > 200
						current_ideal_distance = 200
					endif 
					
					current_slow_down_distance += get_frame_time() * multiplier_speed 
					if current_slow_down_distance > 235.0
						current_slow_down_distance = 235.0
					endif 
					
					CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(disired_playback_speed, truck.veh, lazlows_car.veh, current_minimum_distance, current_ideal_distance, current_slow_down_distance, 100, 30, 1.0, 0.7, 0.5, 2.0, true, 0, 15, true)
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(lazlows_car.speed, disired_playback_speed)
					update_uber_playback(lazlows_car.veh, lazlows_car.speed)

				break 
				
			endswitch 
			
			create_parked_vehicles_system()

			set_piece_driver_ai()
			
			lazlow_horn_system()

		else 
		
//			update_uber_playback(lazlows_car.veh, 1.0)
					
			if not train_cutscene_playing and not trailer_cutscene_playing
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlows_car.veh)) < 15.00
					
					if does_entity_exist(broken_down_cars[0])
						set_vehicle_as_no_longer_needed(broken_down_cars[0])
					endif 
					
					if does_entity_exist(broken_down_cars[1])
						set_vehicle_as_no_longer_needed(broken_down_cars[1])
					endif 

					set_model_as_no_longer_needed(towtruck)
					
					//mission_flow = play_final_mocap
				endif 
			endif 
		
		endif 
		
	endif 
	
endproc 


func bool family_4_run_after_lazlo_master_flow_system()

	int i = 0

	switch run_after_lazlow_master_flow_system_status 
	
		case 0

			if is_entity_in_angled_area(player_ped_id(), <<-248.775, -1999.858, 25.003>>, <<-248.012, -1997.058, 31.302>>, 2.0)

				delete_ped(tracey.ped)
				set_model_as_no_longer_needed(tracey.model)
				
				remove_anim_dict("missfam4leadinoutmcs2")
				
				delete_ped(security.ped)
				set_model_as_no_longer_needed(security.model)
				
				for i = 0 to count_of(camera_crew) - 1
					delete_ped(camera_crew[i].ped)
					set_model_as_no_longer_needed(camera_crew[i].model)
				endfor 
				
				delete_object(micro_phone.obj)
				set_model_as_no_longer_needed(micro_phone.model)
				
				delete_object(video_camera.obj)
				set_model_as_no_longer_needed(video_camera.model)
				
				if is_valid_interior(stadium_interior)
					unpin_interior(stadium_interior)
				endif 
				
				run_after_lazlow_master_flow_system_status++
				
			endif 
			
			create_entities_outside_stadium()
		
		break 
		
		case 1
		
			create_entities_outside_stadium()
		
			if not does_blip_exist(truck.blip)
				if not is_ped_inside_interior(player_ped_id(), <<-260.7652, -2026.1630, 29.1458>>)
					REPLAY_RECORD_BACK_FOR_TIME(3)
					truck.blip = create_blip_for_entity(truck.veh)
				endif 
			endif 
		
			uber_speed_system()
	
			if does_entity_exist(truck.veh)

				if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
				and is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh)
				and is_playback_going_on_for_vehicle(lazlows_car.veh)
				
					//set_player_control(player_id(), false)
					
					CLEAR_TRIGGERED_LABELS()
					
					reset_dialogue_for_future_resumption_data()
					
					DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, false)
					
					start_audio_scene("FAMILY_4_CHASE_TRUCK_1")
				
					trigger_music_event("FAM4_CHASE_START")
					
					if does_blip_exist(michaels_blip)
						remove_blip(michaels_blip)
					endif 
					
					if does_blip_exist(truck.blip)
						remove_blip(truck.blip)
					endif 
										
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "Chase truck", true)

					return true 

				endif 
			endif 

		break 

	endswitch 
	
	return false 

endfunc

proc enemy_ai_system()

endproc



//proc player_leaves_vehicle_dialogue(string dialogue_to_load, string dialogue_root, string get_back_in_text, bool &get_back_in_text_printed, bool &secondary_audio_played) 
//
//	if not secondary_audio_played
//		if not is_ped_sitting_in_any_vehicle(player_ped_id())// and not CHECK_STUCK_TIMER(mission_vehicle, VEH_STUCK_ON_ROOF, 0)
//			if create_conversation(scripted_speech, dialogue_to_load, dialogue_root, CONV_PRIORITY_medium)
//				secondary_audio_played = true
//			endif 
//		endif
//	
//	else 
//	
//		if not get_back_in_text_printed
//			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				clear_prints()
//				print_now(get_back_in_text, DEFAULT_GOD_TEXT_TIME, 1)
//				get_back_in_text_printed = true
//			endif 
//		endif 
//	endif
//
//endproc 



proc camera_crew_ai_system()

//	for i = 0 to count_of(camera_crew) - 1
//
//		if not is_ped_injured(camera_crew[0].ped)
//			
//			switch camera_crew_ai_status[i]
//			
//				case 0
//				
//					switch i
//					
//						case 0
//				
//							if has_ped_been_harmed(camera_crew[i].ped, camera_crew[i].health)
//							or has_player_antagonised_ped(camera_crew[i].ped, 10, false)
//								
//								open_sequence_task(seq)
//									task_cower(null, -1)
//								close_sequence_task(seq)
//								task_perform_sequence(camera_crew[i].ped, seq)
//								clear_sequence_task(seq)
//								
//								if is_entity_attached(video_camera.obj)
//									detach_entity(video_camera.obj)
//								endif 
//								
//								camera_crew_ai_status[i]++
//								
//							endif 
//
//						break 
//						
//						case 1
//
//							if has_ped_been_harmed(camera_crew[i].ped, camera_crew[i].health)
//							or has_player_antagonised_ped(camera_crew[i].ped, 10, false)
//								
//								open_sequence_task(seq)
//									task_cower(null, -1)
//								close_sequence_task(seq)
//								task_perform_sequence(camera_crew[i].ped, seq)
//								clear_sequence_task(seq)
//								
//								if is_entity_attached(micro_phone.obj)
//									detach_entity(micro_phone.obj)
//								endif 
//								
//								camera_crew_ai_status[i]++
//								
//							endif 
//						
//						break 
//						
//					endswitch 
//				
//				break 
//				
//				case 1
//				
//				break 
//				
//			endswitch 
//
//		endif 
//	
//	endfor 
//			
//			
//	
//	camera_crew[0].ped

endproc 

proc run_after_lazlow_dialogue_system()
	
	switch run_after_lazlow_dialogue_system_status 
	
		case 0
		
			if create_conversation(scripted_speech, "fam4Aud", "FAM4_GOHOME", CONV_PRIORITY_medium)
				run_after_lazlow_dialogue_system_status++
			endif 
			
		break 
		
		case 1
		
			if create_conversation(scripted_speech, "fam4Aud", "FAM4_SHOUT", CONV_PRIORITY_medium)
				run_after_lazlow_dialogue_system_status++
			endif 
		
		break 
		
		case 2
		
			if not is_any_text_being_displayed(locates_data)
				print_now("family4_god_11", default_god_text_time, 1)
				run_after_lazlow_dialogue_system_status++
			endif 
		
		break 
		
		case 3
		
			if not is_ped_inside_interior(player_ped_id(), <<-260.7652, -2026.1630, 29.1458>>)

				if player_arrived_in_a_vehicle 
					if create_conversation(scripted_speech, "fam4Aud", "FAM4_TOW", CONV_PRIORITY_medium)

						original_time = get_game_timer()
					
						run_after_lazlow_dialogue_system_status++
						
					endif 
				
				else 
					
					run_after_lazlow_dialogue_system_status++
					
				endif 
			
			endif 
		
		break 
		
		case 4
		
			if create_conversation(scripted_speech, "fam4Aud", "FAM4_TRU", CONV_PRIORITY_medium)
				
				original_time = get_game_timer()
				
				run_after_lazlow_dialogue_system_status++
				
			endif 
		
		break 
		
		case 5
		
			if not is_any_text_being_displayed(locates_data)
				print_now("family4_god_1", default_god_text_time, 1)
				run_after_lazlow_dialogue_system_status++
			endif 
		
		break 
		
		case 6
		
//			if is_ped_in_vehicle(player_ped_id(), truck.veh)
//			or is_ped_in_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh)
//			
//				if create_conversation(scripted_speech, "fam4aud", "FAM4_NOTRU", conv_priority_medium)
//					run_after_lazlow_dialogue_system_status++
//				endif 
//			
//			endif
		
			//****************UNCOMMENT THIS DIALOGUE WHEN THE PLAYER IS PLAYING AS TREVOR ********************
		
			if not is_any_text_being_displayed(locates_data)
//				if lk_timer(original_time, 7000)
//					if is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh) 
//						if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
//							if create_conversation(scripted_speech, "fam4Aud", "FAM4_LAG", CONV_PRIORITY_medium)
//								original_time = get_game_timer()
//							endif 
//						endif 
//					endif 
//				endif 
				
//				if not has_label_been_triggered("FAM4_MCALLT")
//					if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
//						if not IS_PED_GETTING_INTO_A_VEHICLE(selector_ped.pedID[selector_ped_michael])
//							if create_conversation(scripted_speech, "fam4Aud", "FAM4_MCALLT", CONV_PRIORITY_medium)
//								set_label_as_triggered("FAM4_MCALLT", true)
//							endif 
//						endif 
//					endif 
//				endif 
				
			endif 


		break 
		
	endswitch 

endproc 

proc player_enter_passenger_door_of_truck_system()

	REQUEST_VEHICLE_ASSET(truck.model, enum_to_int(VRF_REQUEST_ALL_ANIMS))

	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_ENTER)
		if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(truck.veh)) < 10
    		if not is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
	   			if has_ped_task_finished_2(player_ped_id(), script_task_enter_vehicle, false)
				
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), truck.veh, -1, vs_front_right, PEDMOVEBLENDRATIO_run, ECF_DONT_JACK_ANYONE)
					
				endif 
			endif 
		endif 

	endif 

endproc

proc truck_driver_ai_system()

	switch truck_driver_ai_system_status 
	
		case 0

			if does_entity_exist(truck_driver.ped) 
			and does_entity_exist(truck.veh)
				
				if not is_ped_injured(truck_driver.ped)
				
					request_anim_dict("veh@truck@ds@idle_panic")

					if is_ped_in_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh)
					
						if has_anim_dict_loaded("veh@truck@ds@idle_panic")

							if not is_entity_playing_anim(truck_driver.ped, "veh@truck@ds@idle_panic", "sit") 
						
								task_play_anim(truck_driver.ped, "veh@truck@ds@idle_panic", "sit", normal_blend_in, normal_blend_out, -1, af_looping) 
							
							endif 
							
						endif 
						
					endif 

						
//					if is_ped_sitting_in_vehicle(truck_driver.ped, truck.veh)
//							
//						set_ped_keep_task(truck_driver.ped, true)
//							
//						open_sequence_task(seq)
//							task_leave_vehicle(null, truck.veh)
//						task_smart_flee_ped(null, player_ped_id(), 100, -1)
//						close_sequence_task(seq)
//						task_perform_sequence(truck_driver.ped, seq)
//						clear_sequence_task(seq)
//								
//								
//						truck_driver_ai_system_status = 1
//							
//					endif 
								

					if not is_ped_sitting_in_vehicle(truck_driver.ped, truck.veh)
						
						set_ped_keep_task(truck_driver.ped, true)
						task_smart_flee_ped(truck_driver.ped, player_ped_id(), 100, -1)
						
						//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(truck_driver.ped, "FAM4_NOTRU", "truckdriver")
						
						truck_driver_ai_system_status = 1
						
					endif 
					
				endif 
				
			endif 
			
		break 
		
		case 1

			if create_conversation(scripted_speech, "fam4aud", "FAM4_NOTRU", conv_priority_medium)//, do_not_display_subtitles) 

				set_ped_as_no_longer_needed(truck_driver.ped)
				set_model_as_no_longer_needed(truck_driver.model)
				
				remove_anim_dict("veh@truck@ds@idle_panic")
	
				truck_driver_ai_system_status++
				
			endif 

		break 
		
		case 2
		
		break 
		
	endswitch 
	
endproc

proc run_after_lazlo_audio_scene_system()

	switch run_after_lazlow_audio_scene_system_status 
	
		case 0
		
			START_AUDIO_SCENE("FAMILY_4_CHASE_FOOT")
			
			run_after_lazlow_audio_scene_system_status++
		
		break 
		
		case 1
		
			if not is_ped_inside_interior(player_ped_id(), <<-260.7652, -2026.1630, 29.1458>>)
			
				if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_FOOT")
					STOP_AUDIO_SCENE("FAMILY_4_CHASE_FOOT")
				endif 
				
				START_AUDIO_SCENE("FAMILY_4_CHASE_FOOT_OUTSIDE")
				
				run_after_lazlow_audio_scene_system_status++
				
			endif 
		
		break 
		
		case 2
		
			if get_script_task_status(player_ped_id(), script_task_enter_vehicle) = performing_task
			
				if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_FOOT_OUTSIDE")
					STOP_AUDIO_SCENE("FAMILY_4_CHASE_FOOT_OUTSIDE")
				endif 
				
				run_after_lazlow_audio_scene_system_status++
			
			endif 

		break 
		
		case 3
		
		break 
		
	endswitch 

endproc 

proc run_after_lazlow_walla_system()

	switch run_after_lazlow_walla_system_status 
	
		case 0
		
			SET_PED_WALLA_DENSITY(1,1)
			
			run_after_lazlow_walla_system_status++
		
		break 
		
		case 1
		
			if is_entity_at_coord(player_ped_id(), <<-252.8894, -1997.5737, 29.1458>>, <<1.5, 1.5, 1.5>>)
			or is_entity_at_coord(selector_ped.pedID[selector_ped_michael], <<-252.8894, -1997.5737, 29.1458>>, <<1.5, 1.5, 1.5>>)
				
				FORCE_PED_PANIC_WALLA()
				
				if not is_ped_injured(ambient_ped[0].ped)
					play_pain(ambient_ped[0].ped, aud_damage_reason_scream_panic) 
				endif 
			
				if not is_ped_injured(ambient_ped[10].ped)
					play_pain(ambient_ped[10].ped, aud_damage_reason_scream_terror) 
				endif 
				
				run_after_lazlow_walla_system_status++
				
			endif 
		
		break 

		case 2
		
			if not is_ped_inside_interior(player_ped_id(), <<-260.7652, -2026.1630, 29.1458>>)
			
				SET_PED_WALLA_DENSITY(0,0)
				
				run_after_lazlow_walla_system_status++
			
			endif 
		
		break 
		
	endswitch 

endproc 

proc player_in_truck_With_michael_system()
	
	if not has_label_been_triggered("family4_god_18")
		if not is_any_text_being_displayed(locates_data)
			if does_entity_exist(truck.veh)
				if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
					if not is_entity_at_coord(truck.veh, trailer.pos, <<2.0, 2.0, 2.0>>)
						if not is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh)
						and not is_ped_in_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh)
						and not is_ped_getting_into_a_vehicle(selector_ped.pedID[selector_ped_michael])
							
							REPLAY_RECORD_BACK_FOR_TIME(8.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							print_now("family4_god_18", default_god_text_time, 1)
							printnl()
							
							set_label_as_triggered("family4_god_18", true)
						endif 
					endif 
				endif
			endif 
		endif 
	endif 

endproc 

/// PURPOSE: 
///    flashes lazlows blip red to let the player know they are about to fail.   
proc run_after_lazlow_blip_fail_flashing()
			
	if does_blip_exist(lazlow.blip)
		if does_entity_exist(lazlows_car.veh)
			if is_playback_going_on_for_vehicle(lazlows_car.veh)
				if get_time_position_in_recording(lazlows_car.veh) > 7000
					if not is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
						UPDATE_CHASE_BLIP(lazlow.blip, lazlow.ped, 200.00, 0.6, true)
					else 
						UPDATE_CHASE_BLIP(lazlow.blip, lazlow.ped, 200.00)
					endif 
				endif 
			endif 
		endif 
	endif 
	
endproc 

func bool family_4_run_after_lazlo()
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2161276
	
	if family_4_run_after_lazlo_master_flow_system()
		//mission_flow = stadium_cutscene
		mission_flow = uber_chase
		return true 
	endif 
	
	run_after_lazlow_switch_effect()
	
	clear_players_task_on_control_input(script_task_perform_sequence)
	
	enemy_ai_system()

	run_after_lazlow_blip_fail_flashing()
	
	lazlow_ai_system()
	
	ambient_ped_system()
	
	truck_driver_ai_system()
	
	camera_crew_ai_system()
	
	michael_ai_system()
	
	tracey_ai_system()
	
	run_after_lazlow_dialogue_system()
	
	run_after_lazlo_audio_scene_system()
	
	run_after_lazlow_walla_system()

	//has_mission_vehicle_recordings_loaded()
	
	player_in_truck_with_michael_system()
	
	//player_enter_passenger_door_of_truck_system()
	
	return false

endfunc  

proc family_4_get_the_truck_cutscene()

endproc 

func bool uber_chase_master_flow_system()

	if not is_ped_injured(lazlow.ped)
		if is_vehicle_driveable(lazlows_car.veh)
			if not is_playback_going_on_for_vehicle(lazlows_car.veh)
				if not train_cutscene_playing
//				if get_distance_between_coords(get_entity_coords(lazlow.ped), get_entity_coords(player_ped_id())) < 10.0
//				or is_entity_at_coord(player_ped_id(), <<-924.9842, 16.0313, 45.0531>>, <<10.00, 10.00, 3.0>>)
					//return true 
				endif 
			endif 
		endif 
	
	else 
	
		return true
		
	endif 
	
	
	return false 

endfunc  


proc switch_setpiece_car_to_ai_system()

	int i = 0

	if does_entity_exist(trailer.veh)
	
		for i = 0 to count_of(SetPieceCarID) - 1

			if is_vehicle_driveable(SetPieceCarID[i])
				if is_playback_going_on_for_vehicle(SetPieceCarID[i])
					if is_entity_touching_entity(trailer.veh, SetPieceCarID[i])
						
						SET_PLAYBACK_TO_USE_AI(SetPieceCarID[i])
						
						MAKE_CAR_DRIVE_RANDOMLY(SetPieceCarID[i])
					endif
				endif 
				
			endif 
			
		endfor 
		
	endif 
	
endproc 

proc train_system()
	
	switch train_system_status 
	
		case 0
		
//				CREATE_MISSION_TRAIN(0, 
//				
//				//Tanker x3
//
//				SET_TRAIN_SPEED()
//				SET_TRAIN_CRUISE_SPEED() 

		break 
		
		case 1
		
//				SET_TRAIN_SPEED()
//				SET_TRAIN_CRUISE_SPEED() 
		
		break 
		
		case 2
		
		break 
		
	endswitch 

endproc 

proc reset_train_crash_cutscene()

	#IF IS_DEBUG_BUILD
	
	int i = 0
	
	if widget_reset_cutscene
		
		initialise_mission_variables()
		
		for i = 0 to count_of(train) - 1
			if does_entity_exist(train[i].veh)
				DELETE_VEHICLE(train[i].veh)
			endif 
		endfor 
		
		end_cutscene_no_fade()
		
		widget_reset_cutscene = false
		
	endif 
	
	#endif 
	
endproc

proc cylinder_falling_system()

	vector vec_a
	vector vec_b
	vector vec_ba
	int k
	int i = 0
//	float cylinder_groud_z

	if does_entity_exist(cylinder_truck.veh)
		if is_vehicle_driveable(cylinder_truck.veh)
			if is_playback_going_on_for_vehicle(cylinder_truck.veh)
			
				for i = 0 to count_of(cylinder) - 1
				
					switch cylinder_object_status[i]
					
						case 0

							if get_time_position_in_recording(cylinder_truck.veh) > cylinder[i].scale
							
								if DOES_ENTITY_HAVE_PHYSICS(cylinder[i].obj)
								
									for k = 0 to count_of(cylinder) -1
										DETACH_ENTITY(cylinder[k].obj, true, false)
										SET_ENTITY_COLLISION(cylinder[k].obj, true)
									endfor 


		//							if IS_ENTITY_ATTACHED(cylinder[i].obj)				
		//								DETACH_ENTITY(cylinder[i].obj, true) //false
		//							endif 
		//							
		//							SET_ENTITY_COLLISION(cylinder[i].obj, true)
									
									
									vec_a = GET_ENTITY_COORDS(cylinder[i].obj)
						
									//cylinders
		//							if i = 0
		//								vec_b = get_offset_from_entity_in_world_coords(cylinder_truck.veh, <<27.0, 15.5, 0.0>>) //4.5
		//							else 
		//								vec_b = get_offset_from_entity_in_world_coords(cylinder_truck.veh, <<12.5, -7.5, 0.0>>) //4.5
		//							endif 

									if i = 0
										vec_b = get_offset_from_entity_in_world_coords(cylinder_truck.veh, <<5.5, 12.0, 1.0>>) //4.5
									else 
										vec_b = get_offset_from_entity_in_world_coords(cylinder_truck.veh, <<2.5, -7.5, 0.0>>) //4.5
									endif 
									
											
									vec_ba = vec_b - vec_a
									
									normalise_vector(vec_ba)
									
									vec_ba.x *= 0.1 //0.3
									vec_ba.y *= 0.1 //0.3
									vec_ba.z *= 0.1 //0.3 object_force_multiplier
								
									//APPLY_FORCE_TO_ENTITY(falling_object.obj, APPLY_TYPE_EXTERNAL_IMPULSE, vec_ba, <<2.0, 2.0, 0.0>>, 0, false, true, true)
									APPLY_FORCE_TO_ENTITY(cylinder[i].obj, APPLY_TYPE_EXTERNAL_IMPULSE, vec_ba, <<0.0, 0.0, 0.0>>, 0, false, true, true) //<<1.0, 1.0, 0.0>>
			
									cylinder_object_status[i] = 1
									
								endif 
							endif 

						break 
						
						case 1

							if does_entity_exist(cylinder[i].obj)
								if get_entity_speed(cylinder[i].obj) < 0.2
									
				//					if does_entity_exist(cylinder[i].obj)
				//						SET_OBJECT_PHYSICS_PARAMS(cylinder[i].obj, -1, -1.0, <<3.0, 2.0, 3.0>>, <<3.0, 2.0, 3.0>>, -1.0, -1.0)
				//						//set_entity_proofs(cylinder[i].obj, false, false, false, false, false)
				//					endif 
									
									cylinder_object_status[i]++
								endif 
							endif 
						
						break 
						
						case 2
						
						break 
						
					endswitch 
					
				endfor 
				
			endif 
		endif 
	endif 
	
endproc


proc truck_and_trailer_recording_system()

	#IF IS_DEBUG_BUILD
	
	if not widget_reset_recording

		if is_vehicle_driveable(truck.veh) and is_vehicle_driveable(trailer.veh)
			
			if not is_recording_going_on_for_vehicle(truck.veh) or not is_recording_going_on_for_vehicle(trailer.veh)

				if is_control_pressed(player_control, input_veh_accelerate)

					if START_RECORDING_VEHICLE(truck.veh, 901, "lkfamily4tr", true)
					and START_RECORDING_VEHICLE(trailer.veh, 902, "lkfamily4tr", true)
					
						print_now("family4_god_0", default_god_text_time, 1)
					
					endif 

				endif 
						
			else 
				
				if is_control_pressed(player_control, input_jump)
					
					print_now("family4_god_1", default_god_text_time, 1)
					
					stop_recording_all_vehicles()
					
				endif 
				
			endif 
		endif 

	else 
		
		request_model(truck.model)
		request_model(trailer.model)
		
		while not has_model_loaded(truck.model)
		or not has_model_loaded(trailer.model)
			wait(0)
		endwhile
	
	
		if is_recording_going_on_for_vehicle(truck.veh)
			//stop_recording_all_vehicles()
			stop_recording_vehicle(truck.veh)
			stop_recording_vehicle(trailer.veh)
		endif 
		
//		set_entity_coords(truck.veh, <<538.8203, -1404.0972, 28.2649>>)
//		set_entity_heading(truck.veh, 1.3840)
//		set_entity_coords(truck.veh, <<602.2477, -362.0152, 42.6390>>)
//		set_entity_heading(truck.veh, 254.0559)
//		set_entity_coords(truck.veh, <<529.0752, -1246.5540, 29.0265>>)
//		set_entity_heading(truck.veh, 358.8973)
//		set_entity_coords(truck.veh, <<-57.6500, -547.7206, 39.0133>>)   
//		set_entity_heading(truck.veh, 341.9844)

//		set_entity_coords(truck.veh, <<548.6320, -337.7935, 42.6035>>)
//		set_entity_heading(trailer.veh, 244.1964) 

		clear_ped_tasks_immediately(player_ped_id())
		
		DELETE_VEHICLE(truck.veh)
		DELETE_VEHICLE(trailer.veh)
		
		//truck.veh = create_vehicle(truck.model, <<529.0752, -1246.5540, 32.0265>>, 358.8973)
		//truck.veh = create_vehicle(truck.model, <<-236.0620, -2061.1475, 26.6199>>, 312.1216) //outside stadium
		//truck.veh = create_vehicle(truck.model, <<936.7462, -410.2646, 40.4924>>, 286.2033) //storm drain
		//truck.veh = create_vehicle(truck.model, <<-201.8963, -2032.4478, 26.6203>>, 337.8227)
		//truck.veh = create_vehicle(truck.model, <<934.9937, -397.8746, 40.6275>>, 290.9926)
		truck.veh = create_vehicle(truck.model, <<503.4054, -907.2874, 25.0053>>, 0.6430)
		set_vehicle_strong(truck.veh, true)
		SET_VEHICLE_HAS_STRONG_AXLES(truck.veh, true)
		SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
		set_vehicle_tyres_can_burst(truck.veh, false)
		set_vehicle_colours(truck.veh, 0, 0)
		set_entity_health(truck.veh, 1500)
		set_vehicle_engine_health(truck.veh, 1500)
		set_vehicle_petrol_tank_health(truck.veh, 1500)
		
		
		//trailer.veh = create_vehicle(trailer.model, <<501, -1248.36, 32.2>>, 0.4684)
		//trailer.veh = create_vehicle(trailer.model, <<-236.0620, -2061.1475, 36.7554>>, 312.1205)
		//trailer.veh = create_vehicle(trailer.model, <<936.7462, -410.2646, 50.4924>>, 286.2033)
		//trailer.veh = create_vehicle(trailer.model, <<-201.8963, -2032.4478, 36.6203>>, 337.8227)
		//trailer.veh = create_vehicle(trailer.model, <<934.9937, -397.8746, 50.6275>>, 290.9926)
		trailer.veh = create_vehicle(trailer.model, <<503.5043, -912.8253, 25.1358>>, 0.6430)
		set_vehicle_tyres_can_burst(trailer.veh, false)
		attach_vehicle_to_trailer(truck.veh, trailer.veh)
		//DISABLE_TRAILER_BREAKING_FROM_VEHICLE(trailer.veh, true)
		
		//DISPLAY_PLAYBACK_RECORDED_VEHICLE(temp_truck, rdm_wholeline)
		
//		set_entity_coords(
		
		
		
//		 <<538.8203, -1404.0972, 28.2649>>, 1.3840)
//			SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
//			set_vehicle_tyres_can_burst(truck.veh, false)
//			set_vehicle_colours(truck.veh, 0, 0) //73.0
//			
//			trailer.veh = create_vehicle(trailer.model, <<538.8203, -1424.0972, 28.2649>>, 1.3840)

		clear_ped_tasks_immediately(player_ped_id())
		set_ped_into_vehicle(player_ped_id(), truck.veh) 
		
		clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_trevor])
		set_ped_into_vehicle(selector_ped.pedID[selector_ped_trevor], truck.veh, vs_front_right) 
  
		widget_reset_recording = false
		
	endif 
	
	#endif 

endproc

proc set_piece_recording_system()
	
	#if is_debug_build
	
		switch set_piece_recording_system_status 
		
			case 0
					
				if is_vehicle_driveable(trailer.veh)
					//if fPausePlaybackTime = 1.0
					if START_RECORDING_VEHICLE(trailer.veh, 0, "lkfamily4tr", true)
						set_piece_recording_system_status++
					endif 
				endif 
				
			break 
			
			case 1
			
				start_playback_recorded_vehicle(truck.veh, 1, "lkfamily4tr") 
				skip_time_in_playback_recorded_vehicle(truck.veh, 13050)
				set_playback_speed(truck.veh, 0.9)
				
				start_playback_recorded_vehicle(trailer.veh, 2, "lkfamily4tr") 
				skip_time_in_playback_recorded_vehicle(trailer.veh, 13050)
				set_playback_speed(trailer.veh, 0.9) //remember to use original recording in family 4 train folder
				
				train[0].veh = create_vehicle(train[0].model, train[0].pos, train[0].heading)
				start_playback_recorded_vehicle(train[0].veh, 3, "lkfamily4tr") 
				skip_time_in_playback_recorded_vehicle(train[0].veh, 500) 
				set_playback_speed(train[0].veh, 2.0)
				
				set_piece_recording_system_status++
			
			break 
			
			case 2
			
				if GET_TIME_POSITION_IN_RECORDING(train[0].veh) > 3818 

					stop_playback_recorded_vehicle(trailer.veh)
					
					detach_entity(trailer.veh, false)
					
					APPLY_FORCE_TO_ENTITY(trailer.veh, APPLY_TYPE_EXTERNAL_IMPULSE, <<7.7144, 10.7496, -0.0529>>, <<0.0000, -3.0000, -1.0000>>, 0, false, true, true)  
					APPLY_FORCE_TO_ENTITY(trailer.veh, APPLY_TYPE_ANGULAR_IMPULSE, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 1.0>>, 0, TRUE, TRUE, TRUE)
		
					set_piece_recording_system_status++
				endif 
				
			break 
			
			case 3
			
				if is_control_pressed(player_control, input_jump)
				
					STOP_RECORDING_ALL_VEHICLES()
					
					set_piece_recording_system_status++
					
				endif 
			
			break 
			
			case 4
			
			break 
			
		endswitch 
		
	#endif 
	
endproc

//proc michael_ai_system()
//
//	//add michael blipping_system
//
//	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
//	
//		if not is_ped_sitting_in_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh)
//			
//			if has_ped_task_finished_2(selector_ped.pedID[selector_ped_michael], script_task_perform_sequence)
//			
//				set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
//				clear_ped_tasks(selector_ped.pedID[selector_ped_michael])
//				
//				open_sequence_task(seq)
//					task_enter_vehicle(null, truck.veh, -1, vs_front_right)
//				close_sequence_task(seq)
//				task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
//				clear_sequence_task(seq)
//				
//			endif 
//		
//		endif 
//		
//	endif 
//
//endproc 

proc stop_playback_of_cutscene_car()

	int i = 0
		
	if is_vehicle_driveable(trailer.veh)
		for i = 0 to count_of(cutscene_car) - 1
			if is_vehicle_driveable(cutscene_car[i])
				if is_playback_going_on_for_vehicle(cutscene_car[i])
					if is_entity_touching_entity(trailer.veh, cutscene_car[i])
						stop_playback_recorded_vehicle(cutscene_car[i])
					endif 
				endif 
			endif 
		endfor  
	endif 
	
endproc 

func bool detach_trailer_cutscene_system()

//	if does_entity_exist(truck.veh)
//		if is_playback_going_on_for_vehicle(truck.veh)
//			printfloat(get_time_position_in_recording(truck.veh))
//			printnl()
//		endif 
//	endif 
	
//	if does_entity_exist(trailer.veh)
//		if is_playback_going_on_for_vehicle(trailer.veh)
//			printfloat(get_time_position_in_recording(trailer.veh))
//			printnl()
//		endif
//	endif 
	
	
	//**************script debug tool REMOVE 
	//truck_and_trailer_recording_system()
	//***** end remove

	switch detach_trailer_cutscene_status

		case 0
//		
//////**********************script debug REMOVE 
//			vehicle_index temp_truck
//			
//			temp_truck = create_vehicle(truck.model, <<-47.2755, -515.6834, 39.4660>>, 339.7119)
//			set_entity_collision(temp_truck, false)
//			start_playback_recorded_vehicle(temp_truck, 201, "lkfamily4")
//			skip_time_in_playback_recorded_vehicle(temp_truck, (get_total_duration_of_vehicle_recording(201, "lkfamily4") - 250)) 
//			DISPLAY_PLAYBACK_RECORDED_VEHICLE(temp_truck, rdm_wholeline)
			

			
//			start_playback_recorded_vehicle(truck.veh, 201, "lkfamily4")
//			start_playback_recorded_vehicle(trailer.veh, 202, "lkfamily4")
			
//			wait(0)
			
//			if is_vehicle_driveable(temp_truck)
//				pause_playback_recorded_vehicle(temp_truck)
//			endif 
			
//			if is_vehicle_driveable(truck.veh)
//				if is_vehicle_driveable(trailer.veh)
//					skip_time_in_playback_recorded_vehicle(truck.veh, -1000)//8700
//					skip_time_in_playback_recorded_vehicle(trailer.veh, -1000)
//					
//					stop_playback_recorded_vehicle(truck.veh)
//					stop_playback_recorded_vehicle(trailer.veh)
//				endif 
//			endif 
//			
			
			
//			detach_trailer_cutscene_status = 22
//			
//			end_cutscene_no_fade()
//			
//			return true

////**********************end 

			camera_a = create_cam_with_params("default_scripted_camera", <<-22.708345,-451.572784,41.743435>>,<<-1.770246,7.460166,144.979492>>,32.984249)
			camera_b = create_cam_with_params("default_scripted_camera", <<-16.802435,-433.538116,41.621296>>,<<4.556459,7.441601,142.014786>>,32.984249)
			
			camera_g = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(camera_g, truck.veh, <<6.180, 2.500, 0.490>>)
			POINT_CAM_AT_ENTITY(camera_g, truck.veh, <<-0.800, -2.200, 0.600>>)
			set_cam_fov(camera_g, 40.00)

			camera_h = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(camera_h, truck.veh, <<6.180, -0.520, 0.230>>)
			POINT_CAM_AT_ENTITY(camera_h, truck.veh, <<-0.800, -2.200, 0.6>>)
			set_cam_fov(camera_h, 37.7)
			
			
			camera_c = create_cam_with_params("default_scripted_camera", <<-20.613693,-440.846039,39.883686>>,<<0.674877,3.426711,-0.729025>>,33.299999)
			camera_d = create_cam_with_params("default_scripted_camera", <<-40.991886,-441.019714,40.563812>>,<<0.401580,6.064670,-74.572281>>,30.671715)
			camera_e = create_cam_with_params("default_scripted_camera", <<-39.753429,-441.033356,40.007267>>,<<2.512532,6.064665,-110.036415>>,27.355047)
			
			camera_f = create_cam_with_params("default_scripted_camera", <<-42.095036,-458.759216,39.879986>>,<<4.366019,5.129760,-38.707199>>,26.954762)
			

			start_playback_recorded_vehicle(truck.veh, 201, "lkfamily4")
			skip_time_in_playback_recorded_vehicle(truck.veh, 7500)//8700
			
			start_playback_recorded_vehicle(trailer.veh, 202, "lkfamily4")
			skip_time_in_playback_recorded_vehicle(trailer.veh, (7500 - 105))//8700

			wait(0)
			
			//**********************debug 
//			pause_playback_recorded_vehicle(truck.veh)
//			pause_playback_recorded_vehicle(trailer.veh)
//			
//			wait(1000)

//			unpause_playback_recorded_vehicle(truck.veh)
//			unpause_playback_recorded_vehicle(trailer.veh)
			
			//***********************end debug 

			if is_vehicle_driveable(lazlows_car.veh)
				stop_playback_recorded_vehicle(lazlows_car.veh)
				set_entity_visible(lazlows_car.veh, false)
				set_entity_collision(lazlows_car.veh, false)
				set_entity_proofs(lazlows_car.veh, true, true, true, true, true)
			endif 
			
			cleanup_uber_playback(true)

			set_roads_in_area(<<-106.62, -88.47, 100.00>>, <<45.00, -780, -100>>, false) 
		
			clear_area(<<-47.2755, -515.6834, 39.4660>>, 200.00, true)
			
			set_cam_active(camera_g, true)
			set_cam_active_with_interp(camera_h, camera_g, 1500, graph_type_linear)

			render_script_cams(true, false)
			
			if is_vehicle_driveable(truck.veh)
				START_PARTICLE_FX_NON_LOOPED_on_entity("scr_fam4_trailer_sparks", truck.veh, <<0.0, -1.2, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0)
			endif 
			
			original_time = get_game_timer()
			
			trailer_cutscene_playing = true
			
			detach_trailer_cutscene_status++
			
		break 
		
		case 1
		
			//request_vehicle_recording(001, "lkfamily4")
		
			START_PARTICLE_FX_NON_LOOPED_on_entity("scr_fam4_trailer_sparks", truck.veh, <<0.0, -1.2, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0)
			
			detach_trailer_cutscene_status++
			
		break 
		
		case 2
		
//			if is_vehicle_driveable(truck.veh)
//				draw_sphere(get_offset_from_entity_in_world_coords(truck.veh, <<0.0, -1.2, 0.0>>), 1.0)
//			endif 
		
			if is_vehicle_driveable(truck.veh)
				if is_playback_going_on_for_vehicle(truck.veh)
					if get_time_position_in_recording(truck.veh) > 8500 //8000
					
						skip_time_in_playback_recorded_vehicle(truck.veh, -100000)
						skip_time_in_playback_recorded_vehicle(trailer.veh, -100000)
						
						skip_time_in_playback_recorded_vehicle(truck.veh, 7650) //7500
						skip_time_in_playback_recorded_vehicle(trailer.veh, (7650 - 105))//7500
						
						wait(0)
		
						set_cam_active(camera_g, false)
						set_cam_active(camera_h, false)
		
						set_cam_active(camera_a, true)
						set_cam_active_with_interp(camera_b, camera_a, 2200, graph_type_linear) //3250 2750
						
						
						detach_trailer_cutscene_status++
						
					endif 
				endif 
			endif 
		
		break 
		
		case 3
		
			if is_vehicle_driveable(truck.veh)
				if is_playback_going_on_for_vehicle(truck.veh)
					if get_time_position_in_recording(truck.veh) > 9000 //10500

						if is_playback_going_on_for_vehicle(trailer.veh)
							stop_playback_recorded_vehicle(trailer.veh)
						endif 
						detach_entity(trailer.veh, false)
						//DISABLE_TRAILER_BREAKING_FROM_VEHICLE(trailer.veh, false)
						
						start_playback_recorded_vehicle(trailer.veh, 203, "lkfamily4")
						
						skip_time_in_playback_recorded_vehicle(truck.veh, 1800)
						
						if has_model_loaded(sentinel)
							cutscene_car[0] = create_vehicle(sentinel, <<-9.6958, -361.1301, 40.1008>>, 0) 
							start_playback_recorded_vehicle(cutscene_car[0], 204, "lkfamily4")
							skip_time_in_playback_recorded_vehicle(cutscene_car[0], 6000)//6200
						endif 
						
						if has_model_loaded(premier)
							cutscene_car[1] = create_vehicle(premier, <<-40.6385, -497.1492, 39.9915>>, 0) 
							start_playback_recorded_vehicle(cutscene_car[1], 205, "lkfamily4")
							skip_time_in_playback_recorded_vehicle(cutscene_car[1], 4250)
						endif 
						
						wait(0)
						
						set_time_scale(0.5)

						set_cam_active(camera_a, false)
						set_cam_active(camera_b, false)

						set_cam_active(camera_c, true)
						
						request_model(taco)
						request_model(schafter2)
						
						detach_trailer_cutscene_status++
						
					endif 
				endif 
			endif 
			
		break 
		
		case 4
		
			stop_playback_of_cutscene_car()
		
			if is_playback_going_on_for_vehicle(trailer.veh)
				if get_time_position_in_recording(trailer.veh) > 600
				
					set_cam_active(camera_c, false)
					
					if is_playback_going_on_for_vehicle(truck.veh)
						pause_playback_recorded_vehicle(truck.veh)
					endif 
					
					set_time_scale(0.6)
					
					set_cam_active(camera_d, true)
					set_cam_active_with_interp(camera_e, camera_d, 3000, graph_type_linear)
					
					detach_trailer_cutscene_status++
					
				endif 
			endif 
		
		break
		
		case 5
		
			stop_playback_of_cutscene_car()
		
			if is_playback_going_on_for_vehicle(trailer.veh)
				if get_time_position_in_recording(trailer.veh) > 1258
				
					set_cam_active(camera_d, false)
					set_cam_active(camera_e, false)
					
					set_cam_active(camera_f, true)

					detach_trailer_cutscene_status++
				endif 
			endif 
		
		break 

		
		case 6

			//***********************debug 
		
//			if is_playback_going_on_for_vehicle(trailer.veh)
//				skip_time_in_playback_recorded_vehicle(trailer.veh, -100000)
//				skip_time_in_playback_recorded_vehicle(trailer.veh, 400) //1300
//				wait(0)
//				pause_playback_recorded_vehicle(trailer.veh)
//			endif 
//			
//			set_time_scale(1.0)
//		
//			end_cutscene_no_fade()
//			
//			detach_trailer_cutscene_status++
//			
//			return true

			//*********************end debug 
			
			stop_playback_of_cutscene_car()
		
			if is_playback_going_on_for_vehicle(trailer.veh)
				if get_time_position_in_recording(trailer.veh) > 2000
				
					set_roads_in_area(<<-106.62, -88.47, 100.00>>, <<45.00, -780, -100>>, true)
					
					#IF IS_DEBUG_BUILD
						set_uber_parent_widget_group(family_4_widget_group)
					#endif 
					INITIALISE_UBER_PLAYBACK("lkfamily4", 001, true)
					load_uber_data()
					fUberPlaybackDensitySwitchOffRange = 200
					deactivate_SetPieceCars_up_to_time(127000)
					//switch_SetPieceCar_to_ai_on_collision = true
					allow_veh_to_stop_on_any_veh_impact = true
//					bTrafficDontCleanupRecordingFiles = true
					allow_trailer_touching_check = true
					traffic_block_vehicle_colour(true, traffic_red, traffic_orange)

					#IF IS_DEBUG_BUILD
						if not does_entity_exist(lazlows_car.veh)
							lazlows_car.veh = create_vehicle(lazlows_car.model, <<529.3939, -1334.3630, 28.2729>>, 34.9842)
							set_vehicle_strong(lazlows_car.veh, true)
							SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
							SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
							set_vehicle_tyres_can_burst(lazlows_car.veh, false)
							set_vehicle_colours(lazlows_car.veh, 27, 0)
							start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4")
							skip_to_end_and_stop_playback_recorded_vehicle(lazlows_car.veh)
							SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
							SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
						endif 
					#endif 
					
					start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4")
					
					if is_vehicle_driveable(truck.veh)
						if is_playback_going_on_for_vehicle(truck.veh)
							unpause_playback_recorded_vehicle(truck.veh)
							skip_time_in_playback_recorded_vehicle(truck.veh, -100000)
							skip_time_in_playback_recorded_vehicle(truck.veh, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(201, "lkfamily4") - 1500))
						endif 
					endif 
					
					update_uber_playback(lazlows_car.veh, 1.0)

					wait(0)


					if is_vehicle_driveable(lazlows_car.veh)
						SET_UBER_PLAYBACK_TO_TIME_NOW(lazlows_car.veh, 133000)
						
						update_uber_playback(lazlows_car.veh, 1.0)
						
						set_entity_visible(lazlows_car.veh, true)
						set_entity_collision(lazlows_car.veh, true)
						set_entity_proofs(lazlows_car.veh, false, false, false, false, false)
					endif 
					
					set_time_scale(1.0)
					
					if is_vehicle_driveable(truck.veh)
						stop_playback_recorded_vehicle(truck.veh)
					endif
					
					if is_vehicle_driveable(trailer.veh)
						set_vehicle_tyres_can_burst(trailer.veh, true)
					endif 
						
					set_vehicle_as_no_longer_needed(trailer.veh)
					set_model_as_no_longer_needed(trailer.model)

					end_cutscene_no_fade()
					
					trailer_cutscene_playing = false
					
					detach_trailer_cutscene_status++
					
					return true
					
				endif 
					
			endif 
		
		break
		
		case 7
		
		break 
		
		case 22
		
			if is_vehicle_driveable(truck.veh)
				#if is_debug_build
					DISPLAY_PLAYBACK_RECORDED_VEHICLE(truck.veh, rdm_wholeline)
				#endif 
			endif 
			
			clear_area(<<-57.6468, -547.7130, 39.0136>>, 100.00, true)
		
		break 
		
	endswitch 			

//	if is_vehicle_driveable(trailer.veh)
//		detach_entity(trailer.veh, false)
//		APPLY_FORCE_TO_ENTITY(trailer.veh, APPLY_TYPE_EXTERNAL_IMPULSE, <<0.0, -5.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0, true, true, true)
//		APPLY_FORCE_TO_ENTITY(trailer.veh, APPLY_TYPE_ANGULAR_IMPULSE, <<1.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0, true, true, true)
//	endif 

//trailer force data
//-18.32, -428.001, 42.150
//340.700
//-35.900, -450.720, 45.8
//0.0, 0.0, 0.0
//0.0, -2.3, -1.5
//0.55
//

	return false

endfunc 

proc detach_trailer_from_truck()
						
	//detach_vehicle_from_trailer()

	if does_entity_exist(trailer.veh)
		
		if not is_entity_dead(trailer.veh)
		
			if IS_VEHICLE_ATTACHED_TO_TRAILER(truck.veh)
			
				detach_entity(trailer.veh, false)
				
				APPLY_FORCE_TO_ENTITY(trailer.veh, APPLY_TYPE_EXTERNAL_IMPULSE, <<0.0, -5.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0, true, true, true)
				APPLY_FORCE_TO_ENTITY(trailer.veh, APPLY_TYPE_ANGULAR_IMPULSE, <<0.0, 0.0, -1.0>>, <<0.0, 0.0, 0.0>>, 0, true, true, true)
			
				set_vehicle_tyres_can_burst(trailer.veh, true)
				
			endif 

		endif 
		
		play_sound_from_entity(-1, "UNHITCH_TRAILER", trailer.veh, "FAM4_UNHITCH_TRAILER")
		
		set_vehicle_as_no_longer_needed(trailer.veh)
		set_model_as_no_longer_needed(trailer.model)
		
		if is_this_help_message_being_displayed("family4_help_1")
			clear_help()
		endif 
		
		MODIFY_VEHICLE_TOP_SPEED(truck.veh, -10)
		
		create_conversation(scripted_speech, "fam4aud", "FAM4_DIT2", CONV_PRIORITY_low)
		
		family4_trailer_detached = true
		
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FAM4_TRUCK_UNHOOKED)
		
	endif 
	
endproc 

proc detach_trailer_dialogue_system()
	
	switch deatch_trailer_dialogue_status
	
		case 0
	
			if is_entity_in_angled_area(truck.veh, <<-264.563, -1828.329, 27.333>>, <<-238.324, -1817.461, 36.033>>, 41.000)
				deatch_trailer_dialogue_status++
			endif 

		break 
		
		case 1
		
			if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
				if not is_any_text_being_displayed(locates_data)
				//and not within_certain_recording_time()//turn left right etc
					if create_conversation(scripted_speech, "fam4aud", "trailer_det", CONV_PRIORITY_medium)
						detach_trailer_time = get_game_timer()
						deatch_trailer_dialogue_status++
					endif 
				endif 
			endif 
			
		break 
		
		case 2
		
			if lk_timer(detach_trailer_time, 15000)
				if not is_any_text_being_displayed(locates_data)
				//and not within_certain_recording_time()//turn left right etc
					detach_trailer_time = get_game_timer()
				endif 
			endif 
		
		break 
		
		case 22
		
		break 
		
	endswitch 
				
endproc 

proc detach_trailer_help_text_system()
			
	switch detach_trailer_help_text_system_status 
	
		case 0
		
			if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
				if not is_help_message_on_screen() 
					if is_entity_in_angled_area(player_ped_id(), <<-264.563, -1828.329, 27.333>>, <<-238.324, -1817.461, 36.033>>, 41.000)     
						print_help("family4_help_1")
						detach_trailer_help_text_system_status++
					endif 
				endif 
			endif 
			
			//if is_entity_in_angled_area(truck.veh), <<-264.563, -1828.329, 27.333>>, <<-238.324, -1817.461, 36.033>>, 41.000)
		
		break 
		
		case 1
		
			//possibly add remider help text after 20 s
		
		break 
		
		case 2
		
		break 
		
	endswitch 

endproc 


proc detach_trailer_system()
	
	if does_entity_exist(trailer.veh)
		
		if is_entity_dead(trailer.veh)
		
			detach_trailer_from_truck()
		
		else 
		
			detach_trailer_help_text_system()
			
			//detach_trailer_dialogue_system()

			if not IS_VEHICLE_ATTACHED_TO_TRAILER(truck.veh)
			
				detach_trailer_from_truck()
			
			else 

				if get_entity_health(trailer.veh) < 300.00
					
					vehicle_index closest_vehicle = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(trailer.veh), 10.0, DUMMY_MODEL_FOR_SCRIPT, GET_TRAFFIC_VEHICLE_SEARCH_FLAGS())
					
					if closest_vehicle != lazlows_car.veh
					and closest_vehicle != truck.veh
						if not is_entity_dead(closest_vehicle)
							if is_entity_touching_entity(truck.veh, closest_vehicle)
								detach_trailer_from_truck()
							endif 
						endif 
					endif 

				endif 

			endif 
		endif
	endif 

endproc 


proc manual_car_recording_playback_system()
	
	if does_entity_exist(ambient_car)
	
		if is_vehicle_driveable(ambient_car)
			if get_distance_between_coords(get_entity_coords(ambient_car), get_entity_coords(player_ped_id())) < 150
	
				switch manual_car_recording_system_status 
				
					case 0
					
						if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
							if is_entity_in_angled_area(player_ped_id(), <<-216.640, -2023.141, 26.651>>, <<-176.791, -2037.643, 30.896>>, 45.50) //45  
								
								ambient_driver.ped = create_ped_inside_vehicle(ambient_car, pedtype_mission, ambient_driver.model)
								set_blocking_of_non_temporary_events(ambient_driver.ped, true)
								set_playback_speed(ambient_car, 1.2)
								
								//start_playback_recorded_vehicle(ambient_car, 112, "lkfamily4")
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(ambient_car, 112, "lkfamily4", ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT) | ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY))
								manual_car_recording_system_status++
							endif 
						endif 
						
					break 
					
					case 1
					
						if is_entity_touching_entity(ambient_car, truck.veh) 
							stop_playback_recorded_vehicle(ambient_car)
							set_vehicle_as_no_longer_needed(ambient_car)
							set_ped_as_no_longer_needed(ambient_driver.ped)
							set_vehicle_as_no_longer_needed(ambient_car_2)
							manual_car_recording_system_status++
						endif 
					
					break 
					
					case 2
					
					break 
					
				endswitch 
				
			else 
			
				set_vehicle_as_no_longer_needed(ambient_car)
				set_model_as_no_longer_needed(ambient_driver.model)
			
			endif 
			
		endif 
		
	else 
	
		printstring("ambient car does not exist")
		printnl()
		
	endif 

endproc 

proc cleanup_asset_system()
	
	int i = 0
	
	switch cleanup_asset_status
	
		case 0
		
			if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-219.6464, -2037.8220, 26.6207>>) > 70
		
				for i = 0 to count_of(ambient_ped) - 1
					SET_PED_AS_NO_LONGER_NEEDED(ambient_ped[i].ped)
					set_model_as_no_longer_needed(ambient_ped[i].model)
					REMOVE_FORCED_OBJECT(ambient_ped[i].pos, 0.5, V_ILev_Chair02_ped)
				endfor 
				
				for i = 0 to count_of(parked_truck) - 1
					set_vehicle_as_no_longer_needed(parked_truck[i].veh)
					set_model_as_no_longer_needed(parked_truck[i].model)
					
					set_vehicle_as_no_longer_needed(parked_trailer[i].veh)
					set_model_as_no_longer_needed(parked_trailer[i].model)
				endfor 
				
				if does_entity_exist(michaels_car.veh)
					set_vehicle_as_no_longer_needed(michaels_car.veh)
				endif 
				set_model_as_no_longer_needed(michaels_car.model)
				
				if does_entity_exist(ambient_driver.ped)
					delete_ped(ambient_driver.ped)
				endif 
				set_model_as_no_longer_needed(ambient_driver.model)
				
				set_vehicle_as_no_longer_needed(ambient_car)
				set_vehicle_as_no_longer_needed(ambient_car_2)
				set_vehicle_as_no_longer_needed(ambient_car_3)
				set_vehicle_as_no_longer_needed(ambient_car_4)
				set_vehicle_as_no_longer_needed(ambient_car_5)

//				set_model_as_no_longer_needed(sentinel)
//				set_model_as_no_longer_needed(schafter2)
				
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-365.5, -2063.4, 100.00>>, <<-141.4, -1837.0, -100.00>>, false)

				cleanup_asset_status++
				
			endif 
		
		break 
		
		case 1
		
//			if does_entity_exist(ambient_train[2])
//				if is_vehicle_driveable(ambient_train[2])
//					set_train_speed(ambient_train[2], 0)
//				endif 
//			endif 
		
			if is_playback_going_on_for_vehicle(lazlows_car.veh)
				if get_time_position_in_recording(lazlows_car.veh) > 83000
				
					for i = 0 to count_of(ambient_train) - 1
						if does_entity_exist(ambient_train[i])
							set_vehicle_as_no_longer_needed(ambient_train[i])
						endif 
					endfor
					set_model_as_no_longer_needed(freight)
					
					cleanup_asset_status++
					
				endif 
			endif 
		
		break 
		
		case 2
		
		break 
		
	endswitch 

endproc 

proc setup_mocap()
	
	if is_entity_at_coord(lazlow.ped, <<1063.57, -284.17, 50.6>>, <<40.0, 40.0, 10.0>>)
		
		REQUEST_CUTSCENE("Family_4_MCS_3_concat")

	endif 

endproc 


proc reset_storm_drain_cutscene()
	
	#IF IS_DEBUG_BUILD

	if widget_reset_cutscene
	
		clear_ped_tasks_immediately(player_ped_id())
		clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_michael])
	
		if is_playback_going_on_for_vehicle(truck.veh)
			stop_playback_recorded_vehicle(truck.veh)
		endif 
		
		if is_playback_going_on_for_vehicle(trailer.veh)
			stop_playback_recorded_vehicle(trailer.veh)
		endif 
		
		delete_ped(selector_ped.pedID[selector_ped_michael])
		delete_ped(lazlow.ped)
		DELETE_VEHICLE(lazlows_car.veh)
		DELETE_VEHICLE(truck.veh)
		DELETE_VEHICLE(trailer.veh)

		destroy_all_cams()
		
		family_4_storm_drain_cutscene_status  = 0
		
		widget_reset_cutscene = false
	
	endif 
	
	#endif 

endproc

/// PURPOSE: monitors which lane the player has driven down whilst chasing lazlow in the storm drain.
///     
proc storm_drain_cutscene_lane_system()

	if is_entity_in_angled_area(player_ped_id(), <<1025.122, -337.243, 47.967>>, <<1044.686, -349.899, 62.0>>, 5.0)
	
		storm_drain_lane = 0
		
	elif is_entity_in_angled_area(player_ped_id(), <<1014.375, -330.291, 47.867>>, <<1023.359, -336.103, 62.067>>, 5.0)
	
		storm_drain_lane = 1
	
	elif is_entity_in_angled_area(player_ped_id(), <<992.965, -316.440, 47.867>>, <<1012.612, -329.150, 62.067>>, 5.0) 
		
		storm_drain_lane = 2
	
	endif 
	
endproc 

/// PURPOSE: returns the lane the player has driven down whilst chasing lazlow in the storm drain.
/// 
func int get_storm_drain_lane_player_entered()

	return storm_drain_lane

endfunc 

//int lazlows_car_scene_id

proc family_4_storm_drain_cutscene()

	reset_storm_drain_cutscene()
	
	if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		family_4_storm_drain_cutscene_status = 22
	endif 
	
	request_mocap_data_for_Family_4_MCS_3_concat()

	switch family_4_storm_drain_cutscene_status 
	
		case 0
		
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(truck.veh) //to keep the truck hd to fix lod pop 
		
			//**********debug warp jump 
//			request_model(michael.model)
//			request_model(trevor.model)
//
//			request_model(truck.model)
//			set_vehicle_model_is_suppressed(truck.model, true)
//			
//			request_model(trailer.model)
//			set_vehicle_model_is_suppressed(trailer.model, true)
//			
//			request_model(lazlows_car.model)
//			set_vehicle_model_is_suppressed(lazlows_car.model, true)
//			
//			request_model(lazlow.model)
//			set_ped_model_is_suppressed(lazlow.model, true)
//			
//			request_anim_dict("missfam4mcs3")
//
//			load_mission_vehicle_recordings()
//			
//			while not has_model_loaded(michael.model)
//			or not has_model_loaded(trevor.model)
//			or not has_model_loaded(truck.model)
//			or not has_model_loaded(trailer.model)
//			or not has_model_loaded(lazlows_car.model)
//			or not has_model_loaded(lazlow.model)
//			or not has_anim_dict_loaded("missfam4mcs3")
//			or not has_vehicle_recording_been_loaded(401, "lkfamily4")
//			or not has_vehicle_recording_been_loaded(402, "lkfamily4")
//
//				wait(0)
//				
//			endwhile 
//
//			truck.veh = create_vehicle(truck.model, <<1057.9988, -279.0626, 50.3196>>, truck.heading)
//			SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
//			set_vehicle_tyres_can_burst(truck.veh, false)
//			set_vehicle_colours(truck.veh, 0, 0)
//			set_entity_health(truck.veh, 1500)
//			set_vehicle_engine_health(truck.veh, 1500)
//			set_vehicle_petrol_tank_health(truck.veh, 1500)
//			//set_vehicle_dirt_level(truck.veh, 0.0)
//			set_vehicle_strong(truck.veh, true)

//			
//			trailer.veh = create_vehicle(trailer.model, <<1057.9988, -279.0626, 60.3196>>, trailer.heading)
//			set_vehicle_tyres_can_burst(trailer.veh, false)
//			attach_vehicle_to_trailer(truck.veh, trailer.veh)
//			//DISABLE_TRAILER_BREAKING_FROM_VEHICLE(trailer.veh, true)
//			activate_physics(truck.veh)
//			activate_physics(trailer.veh)
//			
//			storm_drain_lane = 2
//			
//
//			lazlows_car.veh = create_vehicle(lazlows_car.model, <<1067.9988, -279.0626, 50.3196>>, lazlows_car.heading)
//			SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
//			set_vehicle_tyres_can_burst(lazlows_car.veh, false)
//			set_entity_only_damaged_by_player(lazlows_car.veh, true)
//			set_vehicle_doors_locked(lazlows_car.veh, vehiclelock_lockout_player_only) 
//			set_vehicle_colours(lazlows_car.veh, 27, 0)
//			//SET_ENTITY_LOAD_COLLISION_FLAG(lazlows_car.veh, true)
//			SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
//			SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
//			
//
//			create_npc_ped_inside_vehicle(lazlow.ped, char_lazlow, lazlows_car.veh, vs_driver, false)
//			set_ped_can_ragdoll(lazlow.ped, false)
//			SET_PED_COMBAT_ATTRIBUTES(lazlow.ped, ca_leave_vehicles, false)
//			set_blocking_of_non_temporary_events(lazlow.ped, true)
//			add_ped_for_dialogue(scripted_speech, 5, lazlow.ped, "lazlow")
//			task_look_at_coord(lazlow.ped, get_offset_from_entity_in_world_coords(lazlows_car.veh, <<-1.5, -10.0, 0.7>>), 10000)
//			
//			set_current_selector_ped(selector_ped_trevor, false)
//			add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
//			set_ped_into_vehicle(player_ped_id(), truck.veh)
//			
//			create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, truck.veh, vs_front_right, false)
//			set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
//			set_ped_into_vehicle(player_ped_id(), truck.veh)
//			add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")
			
			//**********END OF DEBUG
		
		
			clear_area(<<1067.9988, -279.0626, 50.3196>>, 1000, true)
			
			clear_ped_tasks_immediately(player_ped_id())
			set_ped_into_vehicle(player_ped_id(), truck.veh)
			
			clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_michael])
			set_ped_into_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh, vs_front_right)
			
			switch get_storm_drain_lane_player_entered()
			
				case 0
			
					start_playback_recorded_vehicle(truck.veh, 401, "lkfamily4")
					skip_time_in_playback_recorded_vehicle(truck.veh, 10500)
					force_playback_recorded_vehicle_update(truck.veh)
						
					if does_entity_exist(trailer.veh)
						if not is_entity_dead(trailer.veh)
							start_playback_recorded_vehicle(trailer.veh, 402, "lkfamily4")
							skip_time_in_playback_recorded_vehicle(trailer.veh, 10500)
							force_playback_recorded_vehicle_update(trailer.veh)
						endif 
					endif 
					
					truck.recording_number = 401
					
				break 
				
				case 1
				
					start_playback_recorded_vehicle(truck.veh, 403, "lkfamily4")
					skip_time_in_playback_recorded_vehicle(truck.veh, 9500)
					force_playback_recorded_vehicle_update(truck.veh)
						
					if does_entity_exist(trailer.veh)
						if not is_entity_dead(trailer.veh)
							start_playback_recorded_vehicle(trailer.veh, 404, "lkfamily4")
							skip_time_in_playback_recorded_vehicle(trailer.veh, 9500)
							force_playback_recorded_vehicle_update(trailer.veh)
						endif 
					endif 
					
					truck.recording_number = 403
				
				break 
				
				case 2
				
					start_playback_recorded_vehicle(truck.veh, 405, "lkfamily4")
					skip_time_in_playback_recorded_vehicle(truck.veh, 9500)
					force_playback_recorded_vehicle_update(truck.veh)
						
					if does_entity_exist(trailer.veh)
						if not is_entity_dead(trailer.veh)
							start_playback_recorded_vehicle(trailer.veh, 406, "lkfamily4")
							skip_time_in_playback_recorded_vehicle(trailer.veh, 9500)
							force_playback_recorded_vehicle_update(trailer.veh)
						endif 
					endif 
					
					truck.recording_number = 405
				
				break 
				
				default 
				
					start_playback_recorded_vehicle(truck.veh, 401, "lkfamily4")
					skip_time_in_playback_recorded_vehicle(truck.veh, 10500)
					force_playback_recorded_vehicle_update(truck.veh)
						
					if does_entity_exist(trailer.veh)
						if not is_entity_dead(trailer.veh)
							start_playback_recorded_vehicle(trailer.veh, 402, "lkfamily4")
							skip_time_in_playback_recorded_vehicle(trailer.veh, 10500)
							force_playback_recorded_vehicle_update(trailer.veh)
						endif 
					endif 
					
					truck.recording_number = 401
				
				break 
				
			endswitch 
			
			
			if is_playback_going_on_for_vehicle(lazlows_car.veh)
				stop_playback_recorded_vehicle(lazlows_car.veh)
			endif 
			
			STOP_SYNCHRONIZED_ENTITY_ANIM(lazlows_car.veh, instant_blend_in, true)
			set_entity_coords(lazlows_car.veh, <<1067.9988, -279.0626, 50.3196>>)
			set_entity_heading(lazlows_car.veh, 334.6674)
			set_vehicle_on_ground_properly(lazlows_car.veh)
			set_vehicle_doors_shut(lazlows_car.veh, true)
			setup_vehicle_proofs(lazlows_car.veh)
			
			clear_ped_tasks_immediately(lazlow.ped)
			scene_pos = get_entity_coords(lazlows_car.veh)
			scene_rot = get_entity_rotation(lazlows_car.veh) 
			lazlow.scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)
			
			//SET_VEHICLE_DOOR_OPEN(lazlows_car.veh, sc_door_front_left) //sc_door_list
			
			TASK_SYNCHRONIZED_SCENE(lazlow.ped, lazlow.scene_id, "missfam4mcs3", "loop_lazlow", instant_BLEND_IN, normal_BLEND_OUT)//, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(lazlow.scene_id, true)
			
			task_look_at_entity(lazlow.ped, player_ped_id(), -1)
			
			
			
//			scene_pos = get_entity_coords(lazlows_car.veh)
//			scene_rot = get_entity_rotation(lazlows_car.veh) 
//			lazlows_car_scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)
//			
//			PLAY_SYNCHRONIZED_ENTITY_ANIM(lazlows_car.veh, lazlows_car_scene_id, "loop_intro_car", "missfam4mcs3", instant_BLEND_IN, normal_BLEND_OUT)
			//set_synchronized_scene_phase(lazlows_car_scene_id, 0.99)
			//FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(lazlows_car.veh)
	

	

			camera_a = create_cam_with_params("default_scripted_camera", <<1070.399902,-276.613190,52.565422>>,<<-7.739586,2.908233,130.966339>>,31.374056)
			camera_b = create_cam_with_params("default_scripted_camera", <<1070.461914,-276.590302,52.109283>>,<<-7.739586,2.908233,130.966339>>,28.281126)

//			camera_c = create_cam_with_params("default_scripted_camera", <<1065.437256,-285.258972,50.535084>>,<<0.413196,2.907482,132.180130>>,29.336329)
//			camera_d = create_cam_with_params("default_scripted_camera", <<1064.869873,-285.258972,50.535084>>,<<1.861601,2.907488,133.649506>>,28.515699)

			camera_c = create_cam_with_params("default_scripted_camera", <<1054.241089,-292.129944,49.776012>>,<<7.170268,2.907489,-50.980068>>,28.515699)
			camera_d = create_cam_with_params("default_scripted_camera", <<1054.567871,-291.851379,49.748013>>,<<7.170268,2.907489,-50.127289>>,26.945950)
			
			set_cam_active(camera_a, true)
			set_cam_active_with_interp(camera_b, camera_a, 5000, graph_type_linear)
			render_script_cams(true, false)
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			family_4_storm_drain_cutscene_status++
		
		break 
		
		case 1
		
			printfloat(get_time_position_in_recording(truck.veh))
			printnl()
		
			if get_time_position_in_recording(truck.veh) > (get_total_duration_of_vehicle_recording(truck.recording_number, "lkfamily4") - 200)
			
				open_sequence_task(seq)
					task_leave_vehicle(null, truck.veh)
					task_follow_nav_mesh_to_coord(null, <<1067.9988, -279.0626, 50.3196>>, pedmove_walk, -1)
					//task_go_straight_to_coord(null, <<1066.7, -274.9, 50.3196>>, pedmove_walk, -1)
				close_sequence_task(seq)
				task_perform_sequence(player_ped_id(), seq)
				clear_sequence_task(seq)
				
				open_sequence_task(seq)
					task_pause(null, 200)
					task_leave_vehicle(null, truck.veh)
					//task_go_straight_to_coord(null, <<1066.7, -274.9, 50.3196>>, pedmove_walk, -1)
					task_follow_nav_mesh_to_coord(null, <<1067.9988, -279.0626, 50.3196>>, pedmove_walk, -1)
				close_sequence_task(seq)
				task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
				clear_sequence_task(seq)
				
				original_time = get_game_timer()
				
				family_4_storm_drain_cutscene_status++
				
			endif 

		break 
		
		case 2
		
			if not is_cam_interpolating(camera_b)
			
				set_cam_active(camera_a, false)
				set_cam_active(camera_b, false)
				
				set_entity_coords(truck.veh, <<1057.25, -294.328, 50.4676>>)
				set_entity_heading(truck.veh, 327.891846)
				set_vehicle_on_ground_properly(truck.veh)
				freeze_entity_position(truck.veh, true)

				clear_ped_tasks_immediately(player_ped_id())
				set_entity_coords(player_ped_id(), <<1055.86, -292.17, 49.4391>>)
				set_entity_heading(player_ped_id(), 316.7899) 
				
				fORCE_PED_MOTION_STATE(player_ped_id(), MS_ON_FOOT_walk, TRUE)
				task_go_straight_to_coord(player_ped_id(), <<1066.7, -274.9, 50.3196>>, pedmove_walk, -1)

				set_cam_active(camera_d, true)
				set_cam_active_with_interp(camera_d, camera_c, 3000, graph_type_linear)
		
				family_4_storm_drain_cutscene_status++

			endif 

		break 
		
		case 3
		
			if not is_cam_interpolating(camera_d)
			
				deactivate_vehicle_proofs(lazlows_car.veh)
			
				freeze_entity_position(truck.veh, false)

				mission_flow = play_final_mocap

			endif 

		break 
		
		case 22
		
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(default_fade_time)	
				endif 
				
			else 
			
				deactivate_vehicle_proofs(lazlows_car.veh)
			
				freeze_entity_position(truck.veh, false)
			
				if is_playback_going_on_for_vehicle(truck.veh)
					stop_playback_recorded_vehicle(truck.veh)
				endif 
				
				if does_entity_exist(trailer.veh)
					if is_vehicle_driveable(trailer.veh)
						if is_playback_going_on_for_vehicle(trailer.veh)
							stop_playback_recorded_vehicle(trailer.veh)
						endif 
					endif 
				endif 
				
				set_entity_coords(truck.veh, get_position_of_vehicle_recording_at_time(401, (get_total_duration_of_vehicle_recording(401, "lkfamily4") - 50), "lkfamily4"))
				set_entity_rotation(truck.veh, get_rotation_of_vehicle_recording_at_time(401, (get_total_duration_of_vehicle_recording(401, "lkfamily4") - 50), "lkfamily4"))
				
				if does_entity_exist(trailer.veh)
					if is_vehicle_driveable(trailer.veh)
						set_entity_coords(trailer.veh, (get_position_of_vehicle_recording_at_time(402, (get_total_duration_of_vehicle_recording(402, "lkfamily4") - 50), "lkfamily4")) + <<0.0, 0.0, 10.00>>)
						set_entity_rotation(trailer.veh, get_rotation_of_vehicle_recording_at_time(402, (get_total_duration_of_vehicle_recording(402, "lkfamily4") - 50), "lkfamily4"))
						attach_vehicle_to_trailer(truck.veh, trailer.veh)
					endif 
				endif 
	
				clear_ped_tasks_immediately(player_ped_id())
				clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_michael])

				REPLAY_STOP_EVENT()

				while not has_cutscene_loaded()
					wait(0)
				endwhile 

				mission_flow = play_final_mocap
				
			endif 
		
		break 
		
	endswitch 

endproc 
	

func bool trigger_storm_drain_cutscene()

	if trigger_storm_drain_cutscene_status > 0
		storm_drain_cutscene_lane_system()
	endif 

	switch trigger_storm_drain_cutscene_status
		
		case 0
		
			if is_entity_at_coord(lazlow.ped, <<1063.57, -284.17, 50.6>>, <<80.0, 80.0, 20.0>>)
			
				request_anim_dict("missfam4mcs3")
				
				request_mocap_data_for_Family_4_MCS_3_concat()

				trigger_storm_drain_cutscene_status++

			endif 

		break 
		
		case 1
		
			uber_speed_system()
			
			request_mocap_data_for_Family_4_MCS_3_concat()
		
			if is_playback_going_on_for_vehicle(lazlows_car.veh)
				if (get_time_position_in_recording(lazlows_car.veh) + 5000) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "lkfamily4")

					if (get_distance_between_coords(get_entity_coords(lazlows_car.veh), get_entity_coords(player_ped_id())) < 80)
				
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech, "fam4aud", "FAM4_POW", CONV_PRIORITY_MEDIUM)
						
						trigger_storm_drain_cutscene_status++
						
					endif
					
				endif 
				
			else 
			
				if (get_distance_between_coords(get_entity_coords(lazlows_car.veh), get_entity_coords(player_ped_id())) < 80)
			
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
					//his car had broken down.
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech, "fam4aud", "FAM4_POW2", CONV_PRIORITY_MEDIUM)

					trigger_storm_drain_cutscene_status++
				endif
				
			endif 

		break 
	
		case 2
		
			uber_speed_system()
		
			if is_playback_going_on_for_vehicle(lazlows_car.veh)
				if (get_time_position_in_recording(lazlows_car.veh) + 100) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "lkfamily4")//3750
					
					stop_playback_recorded_vehicle(lazlows_car.veh)
					freeze_entity_position(lazlows_car.veh, true)
					freeze_entity_position(lazlows_car.veh, false)
					
					lazlows_car_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam4_truck_vent", lazlows_car.veh, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					
					trigger_storm_drain_cutscene_status++
				endif 
			else 
				lazlows_car_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam4_truck_vent", lazlows_car.veh, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				trigger_storm_drain_cutscene_status++
			endif 
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 45.5 //35.5
				
				if not does_particle_fx_looped_exist(lazlows_car_ptfx)
					lazlows_car_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam4_truck_vent", lazlows_car.veh, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				endif 
				
				trigger_storm_drain_cutscene_status = 3
				
			endif 
			
		break 
		
		case 3
		
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 40.00 //45.5
			or get_distance_between_coords(get_entity_coords(truck.veh), get_entity_coords(lazlow.ped)) < 40.00 //incase player jumps out of truck full speed
			
				if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
					
					clear_mission_locate_stuff(locates_data, true)

//					if start_new_cutscene_no_fade(false, true, false)
//						
//						SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
//					
//						setup_mission_ped_and_vehicle_proofs(lazlow.ped, truck.veh)
//						
//						clear_mission_locate_stuff(locates_data, true)
//						
//						if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_2")
//							START_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_2")
//						endif 
//						
//						trigger_music_event("FAM4_STOP_TRACK")
//						
//						family_4_storm_drain_cutscene()
//						
//						mission_flow = storm_drain_cutscene
//						
//						return true 
//						
//					endif

					SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

					if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_2")
						stop_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_2")
					endif 
					
					trigger_music_event("FAM4_STOP_TRACK")
					
					
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(truck.veh, 7)
					
					trigger_storm_drain_cutscene_status++

				else 
				
					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) < 15.00
					or get_distance_between_coords(get_entity_coords(truck.veh), get_entity_coords(lazlow.ped)) < 17.00
				
						if start_new_cutscene_no_fade(false, true, false, true)
						
							SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
							
							setup_mission_ped_and_vehicle_proofs(lazlow.ped, truck.veh)
							
							clear_mission_locate_stuff(locates_data, true)
							
							if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_2")
								START_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_2")
							endif 
							
							trigger_music_event("FAM4_STOP_TRACK")
							
							mission_flow = play_final_mocap
							
							return true 
							
						endif 
						
					endif 
					
				endif 
				
			endif 

		break 
		
		case 4
		
			if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(truck.veh, 7)
			
				start_new_cutscene_no_fade(false, false, false)
			
				open_sequence_task(seq)
					task_follow_nav_mesh_to_coord(null, <<1067.4547, -279.3040, 50.2995>>, pedmove_walk, -1, 0.2) 
				close_sequence_task(seq)
				task_perform_sequence(player_ped_id(), seq)
				clear_sequence_task(seq)
			
				set_player_control(player_id(), false, spc_leave_camera_control_on)
				
				original_time = get_game_timer()
				
				deactivate_truck_blipping_system = true 

				trigger_storm_drain_cutscene_status++
					
			endif 
		
		break 
		
		case 5
		
			if lk_timer(original_time, 3000)
			
				REPLAY_RECORD_BACK_FOR_TIME(5)
			
				mission_flow = play_final_mocap
				
				return true 
			
			endif 
		
		break 
		
	endswitch
	
	return false

endfunc 

proc ambient_train_system()

	int i = 0

	switch ambient_train_system_status 
	
		case 0

			if is_playback_going_on_for_vehicle(lazlows_car.veh)
				if get_time_position_in_recording(lazlows_car.veh) > 45000
					
					request_model(freight)
					
					ambient_train_system_status++
					
				endif 
			endif 
			
		break
		
		case 1
		
			if has_model_loaded(freight)
				if is_playback_going_on_for_vehicle(lazlows_car.veh)
		
//					ambient_train[0] = create_mission_train(18, <<550.4, -1360.4, 29.00>>, false)
//					set_train_cruise_speed(ambient_train[0], 10.00)
//					set_train_speed(ambient_train[0], 10.0)
						
					ambient_train[1] = CREATE_MISSION_TRAIN(18, <<534.5, -946.8, 26.5>>, false)//<<536.1702, -844.5273, 23.8658>>, false) //16
					set_train_cruise_speed(ambient_train[1], 0.00)
					set_train_speed(ambient_train[1], 0.00)
					set_entity_proofs(ambient_train[1], true, true, true, true, true)

					ambient_train[2] = CREATE_MISSION_TRAIN(18, <<517.6304, -1060.3722, 26.7011>>, false) //16
					set_train_cruise_speed(ambient_train[2], 0.0)
					set_train_speed(ambient_train[2], 0.0)
					set_entity_proofs(ambient_train[2], true, true, true, true, true)
				
					ambient_train[3] = create_vehicle(freight, <<522.3235, -1064.8285, 27.0030>>, 184.00)//180
					freeze_entity_position(ambient_train[3], true)
					set_entity_proofs(ambient_train[3], true, true, true, true, true)

					ambient_train_system_status++

				endif 
			endif 
		
		break 
		
		case 2

//			if get_time_position_in_recording(lazlows_car.veh) < 64000.580 
//			
//				if is_vehicle_driveable(ambient_train[0])
//			
//					float train_speed
//					
//					train_speed = lazlows_car.speed * 12
//					
//					set_train_speed(ambient_train[0], train_speed)
//					set_train_cruise_speed(ambient_train[0], train_speed)
//					
////					printstring("train_cruise_speed: ")
////					printnl()
//					
//				endif 
//			
//			else 
//			
//				if is_vehicle_driveable(ambient_train[0])
//					
//					set_train_speed(ambient_train[0], 10)
//					set_train_cruise_speed(ambient_train[0], 10)
			
					ambient_train_system_status++
//				endif 
			
//			endif 
		
		break 
		
		case 3
		
//			if does_entity_exist(ambient_train[0])
//				delete_mission_train(ambient_train[0])
//			endif 

			if is_playback_going_on_for_vehicle(lazlows_car.veh)
				if get_time_position_in_recording(lazlows_car.veh) > 65000
					if is_vehicle_driveable(ambient_train[1])
						start_vehicle_horn(ambient_train[1], 7000)
						ambient_train_system_status++
					endif 
				endif 
			endif 
			
		break 
		
		case 4
		
			if get_time_position_in_recording(lazlows_car.veh) > 82000
			
				for i = 1 to count_of(ambient_train) - 1
				
					//trains proofed so can't be destroyed
					if does_entity_exist(ambient_train[i])
						if not is_entity_dead(ambient_train[i])
							if get_distance_between_coords(get_entity_coords(ambient_train[i], false), get_entity_coords(player_ped_id())) > 50
								if not is_entity_on_screen(ambient_train[i])
									
									if i != 3
										delete_mission_train(ambient_train[i])
									else 
										delete_vehicle(ambient_train[i])
									endif 
								endif 
							endif 
							
						endif 
							
					endif 
					
				endfor 
				
			endif 

				
			if not does_entity_exist(ambient_train[1])
			and not does_entity_exist(ambient_train[2])
			and not does_entity_exist(ambient_train[3])
			
				set_model_as_no_longer_needed(freight)
				
				ambient_train_system_status++
			
			endif 
		
		break
		
		case 5
		
		break 
			
	endswitch
	
	//jumps to case 3 when the train cutscene triggers
//	if train_cutscene_playing
//		uber_speed_status = 4
//	endif 
		
endproc 

proc lazlow_ai_system_2()

//	printstring("lazlow ai system 2: ")
//	printint(lazlow_ai_system_2_status)
//	printnl()
//	
//	printvector(get_entity_coords(lazlow.ped))
//	printnl()

	switch lazlow_ai_system_2_status 
	
		case 0
		
			if not is_playback_going_on_for_vehicle(lazlows_car.veh)
				if get_entity_speed(lazlows_car.veh) < 0.01
					if has_anim_dict_loaded("missfam4mcs3")
					
						CLEAR_PED_TASKS_IMMEDIATELY(lazlow.ped)
						set_entity_no_collision_entity(lazlow.ped, lazlows_car.veh, false)

						scene_pos = get_entity_coords(lazlows_car.veh)//<<-228.974, -2048.902, 27.120>>
						scene_rot = get_entity_rotation(lazlows_car.veh) //<<0.0, 0.0, -122.436>>
						lazlow.scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)
						
						TASK_SYNCHRONIZED_SCENE(lazlow.ped, lazlow.scene_id, "missfam4mcs3", "loop_intro_lazlow", instant_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)

						PLAY_SYNCHRONIZED_ENTITY_ANIM(lazlows_car.veh, lazlow.scene_id, "loop_intro_car", "missfam4mcs3", instant_BLEND_IN, normal_BLEND_OUT)

						//script_assert("start synched scene")
						
						lazlow_ai_system_2_status++
						
					endif 
					
				endif 
				
			endif 
			
		break 
		
		case 1
		
			if is_synchronized_scene_running(lazlow.scene_id)
				if get_synchronized_scene_phase(lazlow.scene_id) >= 1.0
				
					STOP_SYNCHRONIZED_ENTITY_ANIM(lazlows_car.veh, instant_blend_in, true)
				
					scene_pos = get_entity_coords(lazlows_car.veh)//<<-228.974, -2048.902, 27.120>>
					scene_rot = get_entity_rotation(lazlows_car.veh) //<<0.0, 0.0, -122.436>>
					lazlow.scene_id = CREATE_SYNCHRONIZED_SCENE(scene_pos, scene_rot)
					
					task_look_at_entity(lazlow.ped, player_ped_id(), -1)
					
					TASK_SYNCHRONIZED_SCENE(lazlow.ped, lazlow.scene_id, "missfam4mcs3", "loop_lazlow", normal_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_LOOPED(lazlow.scene_id, true)
					
					lazlow_ai_system_2_status++
					
				endif 
			endif 	
		
		break 
		
		case 2
		
			if not is_synchronized_scene_running(lazlow.scene_id)
			and not is_ped_ragdoll(lazlow.ped)
			
				vector lazlow_rot
			
				lazlow.run_to_pos = get_anim_initial_offset_position("missfam4mcs3", "loop_lazlow", scene_pos, scene_rot, 0.0)
				lazlow_rot = get_anim_initial_offset_rotation("missfam4mcs3", "loop_lazlow", scene_pos, scene_rot, 0.0)
				lazlow.heading = lazlow_rot.z
		
				if not is_entity_at_coord(lazlow.ped, lazlow.run_to_pos, <<0.5, 0.5, 1.6>>) 
				
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, lazlow.run_to_pos, pedmove_walk, -1)
					close_sequence_task(seq)
					task_perform_sequence(lazlow.ped, seq)
					close_sequence_task(seq)
					
				else 
				
					TASK_SYNCHRONIZED_SCENE(lazlow.ped, lazlow.scene_id, "missfam4mcs3", "loop_lazlow", normal_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_LOOPED(lazlow.scene_id, true)

				endif 
				
			endif 

		break 
		
	endswitch 

endproc 

proc monitor_stats()

	if get_distance_between_coords(get_entity_coords(lazlow.ped), get_entity_coords(player_ped_id())) > 180.00
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FAM4_GOT_TOO_FAR_FROM_LAZ) 
	endif 

endproc 

proc uber_chase_audio_scene_system()

	switch uber_chase_audio_system_status 
	
		case 0
		
			if is_entity_in_angled_area(player_ped_id(), <<291.306549,-653.226807,28.352633>>, <<297.325104,-637.227722,35.392754>>, 5.500000)
			
				START_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_TUNNEL")
				
				uber_chase_audio_system_status++
			
			endif 
			
		break 
		
		case 1
		
			if is_entity_in_angled_area(player_ped_id(), <<130.126190,-567.933960,31.024401>>, <<124.833488,-583.828430,37.622894>>, 3.250000)
			
				if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_TUNNEL")
					stop_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_TUNNEL")
				endif 
				
				uber_chase_audio_system_status++
			
			endif 
		
		break 
		
		case 2
		
			if is_entity_in_angled_area(player_ped_id(), <<722.705933,-402.142151,35.358883>>, <<705.108582,-412.556213,44.903564>>, 9.000000)
			or is_entity_in_angled_area(player_ped_id(), <<730.191772,-399.918793,35.374161>>, <<719.665283,-400.799957,44.312481>>, 4.250000)
				start_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_JUMP")
				original_time = get_game_timer()
				uber_chase_audio_system_status++
			endif 

		break 
		
		case 3
		
			truck.pos = get_entity_coords(truck.veh)
			
			if ((truck.pos.z > 18) and (truck.pos.z < 24)
			and get_entity_height_above_ground(truck.veh) > 0.5
			and IS_VEHICLE_ON_ALL_WHEELS(truck.veh))
			or lk_timer(original_time, 4000)
			or is_entity_in_angled_area(player_ped_id(), <<749.937622,-418.885803,17.449747>>, <<765.348877,-455.763092,28.306601>>, 78.250000)
			
				if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_CHASE_TRUCK_JUMP")
					stop_AUDIO_SCENE("FAMILY_4_CHASE_TRUCK_JUMP")
				endif 
		
				uber_chase_audio_system_status++
			
			endif 

		break 
		
		case 4
		
		break 
		
	endswitch 
		
endproc 


func bool family_4_uber_chase()

//	if uber_chase_master_flow_system()
//		return true
//	endif 
		
	truck_blipping_system()

//	truck_wanted_level_system()
	//switch_setpiece_car_to_ai_system()

	uber_speed_system()
	
	uber_chase_audio_scene_system()
	
	dialogue_system()
	
	lazlow_ai_system_2()
	
	lazlow_camera_system()
	
//	michael_ai_system()
//	
//	trevor_ai_system()
		
	cleanup_asset_system()

	manual_car_recording_playback_system()
	
	train_system()

	detach_trailer_system()
	
	//storm_drain_cutscene_system()
	
	//train_crash_cutscene_system()
	
	ambient_train_system()

	trigger_storm_drain_cutscene()
	
	monitor_stats()

	return false 
	
endfunc

func bool is_player_in_storm_drain()

	if is_entity_in_angled_area(player_ped_id(), <<746.675, -371.534, 14.751>>, <<769.989, -490.368, 34.751>>, 110.00)
	or is_entity_in_angled_area(player_ped_id(), <<847.737, -352.096, 20.22>>, <<870.975, -470.538, 77.346>>, 164.8)
	
		return true 
		
	endif 

	return false 

endfunc

func bool mission_fail_checks()

	int i = 0

	switch mission_flow 
	
		case get_to_the_stadium
			
			if mission_ped_injured(selector_ped.pedID[selector_ped_trevor])
				mission_failed_text = "family4_fail_1"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[selector_ped_michael])
				mission_failed_text = "family4_fail_0"
				return true
			endif
			
			for i = 0 to count_of(ambient_ped) - 1
				
				if has_ped_been_harmed(ambient_ped[i].ped, ambient_ped[i].health)
				or has_player_antagonised_ped(ambient_ped[i].ped, 20.00, false)
				
					mission_failed_text = "family4_fail_11" 
				
					return true
					
				endif 
			endfor 

			if mission_vehicle_injured(lazlows_car.veh)
				apply_fail_wanted_level = true 
				mission_failed_text = "family4_fail_11" 
				return true
			endif 
			
			if does_entity_exist(lazlows_car.veh)
				
				if get_entity_health(lazlows_car.veh) < 700
				or get_vehicle_petrol_tank_health(lazlows_car.veh) < 700
				or get_vehicle_engine_health(lazlows_car.veh) < 700
					mission_failed_text = "family4_fail_11" 
					return true
				endif 
				
				if not is_entity_at_coord(lazlows_car.veh, lazlows_car.pos, <<2.0, 2.0, 2.0>>)
					mission_failed_text = "family4_fail_11" 
					return true
				endif 
				
			endif 

			if mission_vehicle_injured(truck.veh)
				apply_fail_wanted_level = true 
				mission_failed_text = "family4_fail_11" 
				return true
			endif 
			
			for i = 0 to count_of(parked_truck) - 1
				if mission_vehicle_injured(parked_truck[i].veh)
					apply_fail_wanted_level = true 
					mission_failed_text = "family4_fail_11" 
					return true
				endif 
			endfor
			
			if mission_vehicle_injured(ambient_car)
			or mission_vehicle_injured(ambient_car_2)
			or mission_vehicle_injured(ambient_car_3)
			or mission_vehicle_injured(ambient_car_4)
			or mission_vehicle_injured(ambient_car_5)
			
				apply_fail_wanted_level = true 
			
				mission_failed_text = "family4_fail_11" 
				
				return true
			
			endif 
			
			if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_trevor])) > 200.00)
					mission_failed_text = "family4_fail_10"
					return true
				endif 
			endif 
			
			if does_entity_exist(selector_ped.pedID[selector_ped_michael])
				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) > 200.00)
					mission_failed_text = "family4_fail_9"
					return true
				endif 
			endif 
			

			if is_ped_inside_interior(player_ped_id(), <<-260.7652, -2026.1630, 29.1458>>)
				
				if is_ped_shooting(player_ped_id())
				or get_player_wanted_level(player_id()) > 0
				or is_ped_in_any_vehicle(player_ped_id())
				
					if is_ped_shooting(player_ped_id())
						make_ambient_peds_flee()
					endif 
				
					apply_fail_wanted_level = true 

					mission_failed_text = "family4_fail_11" 
					
					return true
				endif 

			endif 
			
			//player drives car near shop doors.
			vehicle_index last_players_car
			last_players_car = get_players_last_vehicle()
			if does_entity_exist(last_players_car)
				if not is_entity_dead(last_players_car)
					if is_entity_in_angled_area(last_players_car, <<-258.158, -2034.546, 27.946>>, <<-248.087, -2022.372, 31.946>>, 5.200)     
						
						make_ambient_peds_flee()
						
						apply_fail_wanted_level = true 

						mission_failed_text = "family4_fail_11" 
						
						return true
					endif 
				endif 
			endif 
			
			
			
			//if is_entity_in_angled_area(player_ped_id(), <<-231.798, -2041.398, 26.361>>, <<-260.114, -2021.864, 34.161>>, 39.2)  
			if is_entity_in_angled_area(player_ped_id(), <<-175.828, -2039.052, 26.621>>, <<-276.227, -1989.866, 35.621>>, 195.700) 

				if is_ped_shooting(player_ped_id())
				
					weapon_type players_weapon
				
					if get_current_ped_weapon(player_ped_id(), players_weapon)  
					
						if GET_WEAPONTYPE_GROUP(players_weapon) != WEAPONGROUP_THROWN
						
							make_ambient_peds_flee()
							apply_fail_wanted_level = true 
							mission_failed_text = "family4_fail_11" 

							return true
						
						endif 
					
					endif 
					
				endif 
				
				vehicle_index temp_vehicle
				
				if get_current_player_vehicle(temp_vehicle)
					if get_entity_model(temp_vehicle) = firetruk
						if is_control_pressed(player_control, input_veh_attack) 
							apply_fail_wanted_level = true 
							mission_failed_text = "family4_fail_11" 
					
							return true
						endif 
					endif 
				endif 
				
				if get_player_wanted_level(player_id()) > 0
					//apply_fail_wanted_level = true 
					mission_failed_text = "family4_fail_11" 
					return true
				endif 
				
			endif
			
			if IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-231.798, -2041.398, 26.361>>, <<-260.114, -2021.864, 34.161>>, 39.2)
			or is_explosion_in_angled_area(EXP_TAG_DONTCARE, <<-252.133, -2011.385, 29.146>>, <<-269.892, -2032.251, 32.146 >>, 16.50)
				make_ambient_peds_flee()
				apply_fail_wanted_level = true 
				mission_failed_text = "family4_fail_11" 
				return true
			endif 

		break
		
		case play_stadium_mocap
		
			if mission_ped_injured(selector_ped.pedID[selector_ped_trevor])
				mission_failed_text = "family4_fail_1"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[selector_ped_michael])
				mission_failed_text = "family4_fail_0"
				return true
			endif
			
			if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_trevor])) > 200.00)
					mission_failed_text = "family4_fail_10"
					return true
				endif 
			endif 
			
			if does_entity_exist(selector_ped.pedID[selector_ped_michael])
				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) > 200.00)
					mission_failed_text = "family4_fail_9"
					return true
				endif 
			endif 
			
			if (get_distance_between_coords(get_entity_coords(player_ped_id()), <<-243.6647, -2001.4718, 24.7756>>) > 200.00)
				mission_failed_text = "family4_fail_12"
				return true
			endif 

		break 
		
		case run_after_lazlow
		
			//kill_ped_on_certain_damage(lazlow)
		
			if mission_ped_injured(tracey.ped)
				mission_failed_text = "family4_fail_2"
				return true
			endif 
			
			if mission_ped_injured(lazlow.ped)
				mission_failed_text = "family4_fail_3"
				return true
			endif 
					
			if mission_ped_injured(selector_ped.pedID[selector_ped_trevor])
				mission_failed_text = "family4_fail_1"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[selector_ped_michael])
				mission_failed_text = "family4_fail_0"
				return true
			endif 
			
			if mission_vehicle_injured(lazlows_car.veh)
				mission_failed_text = "family4_fail_5"
				return true
			endif 
			
			if mission_vehicle_injured(truck.veh)
			or mission_vehicle_injured(trailer.veh)
				mission_failed_text = "family4_god_5"
				return true
			endif 
			
			if does_entity_exist(lazlows_car.veh)
				if is_ped_sitting_in_vehicle(lazlow.ped, lazlows_car.veh)
					if is_playback_going_on_for_vehicle(lazlows_car.veh)
						
						if is_ped_inside_interior(player_ped_id(), <<-249.8176, -2008.0590, 29.1458>>)
							if not is_ped_in_specific_room(player_ped_id(), <<-249.8176, -2008.0590, 29.1458>>, "GtaMloRoom001") 
								mission_failed_text = "family4_fail_6"
								return true
							endif 
						endif 
						
						
						if get_time_position_in_recording(lazlows_car.veh) > 11000
							if not is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
								mission_failed_text = "family4_fail_6"
								return true
							endif 
						endif 
						
					endif 
				endif 
				
			endif 
			
			switch get_current_player_ped_enum()
			
				case char_trevor
			
					if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) > 200.00)
						mission_failed_text = "family4_fail_9"
						return true
					endif 
					
				break 
				
			endswitch 
			
			if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) > 200.00)
				mission_failed_text = "family4_fail_6"
				return true
			endif 
			
			

			if lk_timer(original_time, 60000)

				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) > 150.00)
					//INFORM_STAT_FAMILY_FOUR_LAZLOW_PROXIMITY_FAILED()
					mission_failed_text = "family4_fail_6"
					return true
				endif 
				
				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) > 100.00)
					mission_failed_text = "family4_fail_9"
					return true 
				endif 
			endif 
			
			if does_entity_exist(truck.veh)
				if is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
			
					if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) > 50.00)
						mission_failed_text = "family4_fail_9"
						return true 
					endif 
					
				endif 
			endif 
		
		break 
		
		case stadium_cutscene
		
			if mission_ped_injured(lazlow.ped)
				mission_failed_text = "family4_fail_3"
				return true
			endif 
		
			if mission_ped_injured(selector_ped.pedID[selector_ped_trevor])
				mission_failed_text = "family4_fail_1"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[selector_ped_michael])
				mission_failed_text = "family4_fail_0"
				return true
			endif 

//			if mission_vehicle_injured(michaels_car.veh)
//				mission_failed_text = "family4_fail_4"
//				return true
//			endif 
			
			if mission_vehicle_injured(lazlows_car.veh)
				mission_failed_text = "family4_fail_5"
				return true
			endif 
			
			if mission_vehicle_injured(truck.veh)
			or mission_vehicle_injured(trailer.veh)
				mission_failed_text = "family4_god_5"
				return true
			endif 
		
		break 
		
		case uber_chase

			if mission_ped_injured(lazlow.ped)
				
				if not is_entity_dead(lazlows_car.veh)
					if is_playback_going_on_for_vehicle(lazlows_car.veh)
						stop_playback_recorded_vehicle(lazlows_car.veh)
					endif 
				endif 
				
				mission_failed_text = "family4_fail_3"
				return true
			endif 

			if mission_ped_injured(selector_ped.pedID[selector_ped_michael])
				mission_failed_text = "family4_fail_0"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[selector_ped_trevor])
				mission_failed_text = "family4_fail_1"
				return true
			endif 
			
			if mission_vehicle_injured(lazlows_car.veh)
				mission_failed_text = "family4_fail_3"
				add_explosion(get_entity_coords(lazlows_car.veh, false), exp_tag_grenade, 2.0)
				return true
			endif 

			if mission_vehicle_injured(truck.veh)
			or mission_vehicle_injured(trailer.veh)
				mission_failed_text = "family4_god_5"
				return true
			endif 
			
			if is_vehicle_stuck_every_check(truck.veh)
				mission_failed_text = "family4_god_4"
				return true
			endif 
//			
//			//failed to be in a position to trigger a train crash cutscene
//			if train_crash_cutscene_fail
//				mission_failed_text = "family4_fail_6"
//				script_assert("test 0")
//				return true
//			endif 

			if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) > 200.00)
				if not is_entity_on_screen(selector_ped.pedID[selector_ped_michael])
					//INFORM_STAT_FAMILY_FOUR_LAZLOW_PROXIMITY_FAILED()
					mission_failed_text = "family4_fail_9"
					return true
				endif 
			endif 
			
			if not is_player_in_storm_drain()

				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) > 250.00)
					if not is_entity_on_screen(lazlow.ped)
						//INFORM_STAT_FAMILY_FOUR_LAZLOW_PROXIMITY_FAILED()
						mission_failed_text = "family4_fail_6"
						return true
					else 
						if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) > 300.00
							mission_failed_text = "family4_fail_6"
							return true
						endif 
					endif 
				endif 
			else 
			
				if (get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(lazlow.ped)) > 400.00)
					mission_failed_text = "family4_fail_6"
					return true
				endif 
				
			endif 
			
		break 
		
		case storm_drain_cutscene
		case play_final_mocap
		
			if mission_ped_injured(lazlow.ped)
				mission_failed_text = "family4_fail_3"
				return true
			endif 

			if mission_ped_injured(selector_ped.pedID[selector_ped_michael])
				mission_failed_text = "family4_fail_0"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[selector_ped_trevor])
				mission_failed_text = "family4_fail_1"
				return true
			endif 
			
			if mission_vehicle_injured(lazlows_car.veh)
				mission_failed_text = "family4_fail_3"
				explode_vehicle(lazlows_car.veh)
				return true
			endif 

			if mission_vehicle_injured(truck.veh)
			or mission_vehicle_injured(trailer.veh)
				mission_failed_text = "family4_god_5"
				return true
			endif 
		
		break 
		
		case train_crash_cutscene_test_bed
		
			if mission_vehicle_injured(truck.veh)
			endif 
			
			if mission_vehicle_injured(trailer.veh)
			endif 
			
			if mission_vehicle_injured(train[0].veh)
			endif 
			
			if mission_vehicle_injured(lazlows_car.veh)
			endif 
			
			if mission_ped_injured(lazlow.ped)
			endif 
		
		break 
		
		case detach_trailer_cutscene_test_bed
		
			if mission_ped_injured(lazlow.ped)
				mission_failed_text = "family4_fail_3"
				return true
			endif 

			if mission_ped_injured(selector_ped.pedID[selector_ped_michael])
				mission_failed_text = "family4_fail_0"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[selector_ped_trevor])
				mission_failed_text = "family4_fail_1"
				return true
			endif 
			
			if mission_vehicle_injured(lazlows_car.veh)
				mission_failed_text = "family4_fail_3"
				explode_vehicle(lazlows_car.veh)
				return true
			endif 

			if mission_vehicle_injured(truck.veh)
			or mission_vehicle_injured(trailer.veh)
				mission_failed_text = "family4_god_5"
				return true
			endif 
			
		break 
		
	endswitch 
	
	return false 

endfunc

proc remove_all_blips()

	if does_blip_exist(lazlow.blip)
		remove_blip(lazlow.blip)
	endif

endproc 

proc setup_mission_fail()
	
	if apply_fail_wanted_level
		set_player_wanted_level(player_id(), 1)
		set_player_wanted_level_now(player_id())
		fail_time = get_game_timer()
	endif 

	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
	
	remove_all_blips()
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(mission_failed_text)
	
	trigger_music_event("FAM4_MISSION_FAIL")

	stop_mission_fail_checks = true 
	
	cached_mission_flow = mission_flow 
	
	mission_flow = mission_failed_stage

endproc 
				
proc skip_system()

	#IF IS_DEBUG_BUILD 
		
		DONT_DO_J_SKIP(locates_data) 

		if is_keyboard_key_just_pressed(key_j)

			switch mission_flow 
			
				case intro_mocap
				
					if is_cutscene_playing()
					
						stop_cutscene()
			
					endif 
				
				break 
			
				case get_to_the_stadium
				
					if not is_cutscene_playing()
					
						switch get_to_the_stadium_skip_status 
						
							case 0

								switch get_current_player_ped_enum()
						
									case char_michael
									
										if does_entity_exist(michaels_car.veh)
										and is_vehicle_driveable(michaels_car.veh)
		
											clear_ped_tasks_immediately(player_ped_id())
											set_ped_into_vehicle(player_ped_id(), michaels_car.veh)
											
											clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_trevor])
											set_ped_into_vehicle(selector_ped.pedID[selector_ped_trevor], michaels_car.veh, vs_front_right)
											
											set_entity_coords(michaels_car.veh, <<-219.2400, -2037.6470, 27.0>>)
											set_entity_heading(michaels_car.veh, 57.2397)//90.00)
											
										else 
										
											clear_ped_tasks_immediately(player_ped_id())
											set_entity_coords(player_ped_id(), <<-219.24, -2037.64, 27.0>>)
											
											clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_trevor])
											set_entity_coords(selector_ped.pedID[selector_ped_trevor], <<-219.24, -2037.94, 27.0>>)
										
										endif 
										
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										get_to_the_stadium_dialogue_system_status = 4
										
										load_scene(<<-219.2400, -2037.6470, 27.0>>)
										
										original_time = get_game_timer()
										
										while not lk_timer(original_time, 1000)
											wait(0)
										endwhile
										
										is_ped_injured(selector_ped.pedID[selector_ped_trevor])

										get_to_the_stadium_skip_status++
									
									break 
									
									case char_trevor
									
										if does_entity_exist(michaels_car.veh)
										and is_vehicle_driveable(michaels_car.veh)
										
											clear_ped_tasks_immediately(player_ped_id())		
											set_ped_into_vehicle(player_ped_id(), michaels_car.veh)
											
											clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_michael])
											set_ped_into_vehicle(selector_ped.pedID[selector_ped_michael], michaels_car.veh, vs_front_right)
											
											set_entity_coords(michaels_car.veh, <<-219.2400, -2037.6470, 27.0>>)
											
										else 
										
											clear_ped_tasks_immediately(player_ped_id())
											set_entity_coords(player_ped_id(), <<-219.24, -2037.64, 27.0>>)
											
											clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_michael])
											set_entity_coords(selector_ped.pedID[selector_ped_michael], <<-219.24, -2037.94, 27.0>>)
										
										endif 
										
										load_scene(<<-219.2400, -2037.6470, 27.0>>)
										
										original_time = get_game_timer()
										
										while not lk_timer(original_time, 1000)
											wait(0)
										endwhile
										
										is_ped_injured(selector_ped.pedID[selector_ped_michael])
										
										get_to_the_stadium_skip_status++
									break 
									
								endswitch 
								
							break 
							
							case 1
							
								switch get_current_player_ped_enum()
						
									case char_michael
									
										clear_ped_tasks_immediately(player_ped_id())
										set_entity_coords(player_ped_id(), <<-253.20, -2028.19, 28.95>>) 
											
										clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_trevor])
										set_entity_coords(selector_ped.pedID[selector_ped_trevor], <<-254.20, -2029.51, 28.95>>)
										
										get_to_the_stadium_skip_status++
									
									break 
									
									case char_trevor
									
										clear_ped_tasks_immediately(player_ped_id())
										set_entity_coords(player_ped_id(), <<-253.20, -2028.19, 28.95>>) 
											
										clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_michael])
										set_entity_coords(selector_ped.pedID[selector_ped_michael], <<-254.20, -2029.51, 28.95>>)
				
										get_to_the_stadium_skip_status++
									break 
									
								endswitch 

							break
							
						endswitch 
						
					else 
						
						stop_cutscene()
						
					endif 
				
				break 
				
				case play_stadium_mocap
				
					switch get_to_the_audition_room_skip_status
					
						case 0
						
							set_entity_coords(player_ped_id(), <<-243.3, -1999.9, 25.6>>) 
				
							if is_cutscene_playing()
					
								stop_cutscene()
					
							endif 
							
							get_to_the_audition_room_skip_status++
							
						break 
						
						case 1
						
							if is_cutscene_playing()
					
								stop_cutscene()
								
							endif 
						
						break 
						
					endswitch 
				
				break 
				
				case run_after_lazlow
				
					switch run_after_lazlo_skip_status 
					
						case 0
						
							clear_ped_tasks_immediately(player_ped_id())
							set_entity_coords(player_ped_id(), <<-249.0080, -1998.2291, 26.7357>>)
							run_after_lazlo_skip_status++
						
						break
						
						case 1
						
							remove_all_mission_assets()

							initialise_mission_variables()

							load_trip_skip_2()

						break 
				
					endswitch 
				
				break 

				case uber_chase

					if is_playback_going_on_for_vehicle(truck.veh)
						stop_playback_recorded_vehicle(truck.veh)
					endif 
					
					if is_playback_going_on_for_vehicle(trailer.veh)
						stop_playback_recorded_vehicle(trailer.veh)
					endif 
			
					clear_ped_tasks_immediately(player_ped_id())
					set_ped_into_vehicle(player_ped_id(), truck.veh)
					
					clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_michael])
					set_ped_into_vehicle(selector_ped.pedID[selector_ped_michael], truck.veh, vs_front_right)
				
					set_entity_coords(truck.veh, <<1056.5160, -295.4885, 49.3719>>)
					set_entity_heading(truck.veh, 326.8398)
					
					set_entity_coords(lazlows_car.veh, <<1063.57, -284.17, 50.6>>)
					
					if is_playback_going_on_for_vehicle(lazlows_car.veh)
						skip_time_in_playback_recorded_vehicle(lazlows_car.veh, -1000000)
						skip_time_in_playback_recorded_vehicle(lazlows_car.veh, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "lkfamily4") - 2500))
						force_playback_recorded_vehicle_update(lazlows_car.veh)
						
						///if (get_time_position_in_recording(lazlows_car.veh) + 2000) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "lkfamily4")
						//stop_playback_recorded_vehicle(lazlows_car.veh)
						
						//SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(lazlows_car.veh)
					endif 
					
					cleanup_uber_playback(true)

				break 
				
				case storm_drain_cutscene

					if family_4_storm_drain_cutscene_status > 0
						family_4_storm_drain_cutscene_status = 22
					endif 
					
				break 
				
				case play_final_mocap
				
					if is_cutscene_playing()
					
						stop_cutscene()
						
					endif 
				
				break 
				
			endswitch 
			
		elif is_keyboard_key_just_pressed(key_p)
			
			switch mission_flow
			
				case get_to_the_stadium
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
					launch_mission_stage_menu_status = 0
				
				break 
			
				case play_stadium_mocap
				case run_after_lazlow
					
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 1	
					else
						launch_mission_stage_menu_status = 0
					endif 
						
					p_skip_time = get_game_timer()
					
				break 
			
				case uber_chase
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 2	
					else
						launch_mission_stage_menu_status = 1
					endif 
						
					p_skip_time = get_game_timer()
					
				break 
				
				case play_final_mocap
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 3	
					else
						launch_mission_stage_menu_status = 2
					endif 
						
					p_skip_time = get_game_timer()
				
				break 
				
			endswitch 
			
		endif 
	
		if LAUNCH_MISSION_STAGE_MENU(menu_stage_selector, menu_return_stage) 
			
			remove_all_mission_assets()

			launch_mission_stage_menu_status = menu_return_stage
			
			mission_flow = load_stage_selector_assets
		endif
		
	#endif 
	
endproc

proc truck_driver_ai_system_2()

	if does_entity_exist(parked_truck[0].veh)
	and does_entity_exist(parked_trailer[0].veh)
	and does_entity_exist(parked_truck[0].veh)
	
		switch truck_driver_ai_system_2_status 
		
			case 0
			
				if not is_entity_dead(parked_truck[0].veh) and not is_entity_dead(parked_trailer[0].veh)

					if is_entity_in_angled_area(player_ped_id(), <<-140.858, -2011.314, 20.385>>, <<-170.442, -2007.734, 27.185>>, 90.00)  
					or is_entity_in_angled_area(player_ped_id(), <<-175.828, -2039.052, 26.621>>, <<-276.227, -1989.866, 35.621>>, 195.700) 
					//or get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(parked_truck[0].veh), false) < 15

						start_playback_recorded_vehicle(parked_truck[0].veh, 501, "lkfamily4")
						
						start_playback_recorded_vehicle(parked_trailer[0].veh, 502, "lkfamily4")
						
						truck_driver_ai_system_2_status++
						
					endif 
		
				endif 
				
			break 
			
			case 1
			
				if is_vehicle_driveable(parked_truck[0].veh)
					if not is_playback_going_on_for_vehicle(parked_truck[0].veh)
						set_vehicle_engine_on(parked_truck[0].veh, false, true)
						truck_driver_ai_system_2_status++
					endif 
				endif 

			break 
			
			case 2
			
			break 
			
		endswitch 
	
	else
	
		if get_distance_between_coords(get_entity_coords(player_ped_id()), truck.pos) < 550.00
		
			request_model(parked_truck[0].model)
			set_vehicle_model_is_suppressed(parked_truck[0].model, true)
				
			request_model(parked_trailer[0].model)
			set_vehicle_model_is_suppressed(parked_trailer[0].model, true)
			
			request_model(truck_driver.model)
		
			request_vehicle_recording(501, "lkfamily4")
			request_vehicle_recording(502, "lkfamily4")
			
		
			if has_model_loaded(parked_truck[0].model)
			and has_model_loaded(parked_trailer[0].model)
			and has_model_loaded(truck_driver.model)
			and has_vehicle_recording_been_loaded(501, "lkfamily4")
			and has_vehicle_recording_been_loaded(502, "lkfamily4")
			
				parked_truck[0].veh = create_vehicle(parked_truck[0].model, parked_truck[0].pos, parked_truck[0].heading)
				set_vehicle_doors_locked(parked_truck[0].veh, vehiclelock_locked)
				set_vehicle_colours(parked_truck[0].veh, (10 * 0), 0)
				parked_trailer[0].veh = create_vehicle(parked_trailer[0].model, parked_trailer[0].pos, parked_trailer[0].heading)
				attach_vehicle_to_trailer(parked_truck[0].veh, parked_trailer[0].veh)
				
				truck_driver.ped = create_ped_inside_vehicle(parked_truck[0].veh, pedtype_mission, truck_driver.model)
				set_model_as_no_longer_needed(truck_driver.model)
				set_blocking_of_non_temporary_events(truck_driver.ped, true)
		
				
	//			freeze_entity_position(parked_truck[0].veh, true)
	//			freeze_entity_position(parked_trailer[0].veh, true)
				
			endif 
			
		endif 
		
	
	endif 
	
endproc 

//PURPOSE: trevor will walk into the stadium but if the player decides to run override his move blend to run
proc force_buddy_to_run_into_stadium(ped_index &mission_ped)

	if not force_buddy_to_run
		if is_ped_on_foot(player_ped_id())
			
			float moveSpeed = GET_PED_DESIRED_MOVE_BLEND_RATIO(player_ped_id())

			if is_move_blend_ratio_running(moveSpeed)
			or is_move_blend_ratio_sprinting(moveSpeed)
			
				SET_PED_DESIRED_MOVE_BLEND_RATIO(mission_ped, 2.0)
				
				force_buddy_to_run = true
			endif 
		endif 
	else 
		SET_PED_DESIRED_MOVE_BLEND_RATIO(mission_ped, 2.0)
	endif 
	
endproc 

proc request_mocap_data_for_family_4_mcs_2()
			
	mocap_streaming_system(<<-252.6443, -2022.6317, 29.1457>>, DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_UNLOAD_DIST, "family_4_mcs_2") 

	//set lazlows clothes
	//SET_CUTSCENE_PED_COMPONENT_VARIATION(
			
endproc 

proc head_track_system()

	switch get_current_Player_ped_enum()
	
		case char_michael 

			if is_ped_in_front_of_ped(player_ped_id(), selector_ped.pedID[selector_ped_trevor])
				if not is_ped_sitting_in_any_vehicle(player_ped_id())
					
					vector offset_pos
					
					offset_pos = get_offset_from_entity_given_world_coords(player_ped_id(), get_entity_coords(selector_ped.pedID[selector_ped_trevor]))
					
					if offset_pos.y < 2.0
				
						if get_script_task_status(player_Ped_id(), SCRIPT_TASK_LOOK_AT_ENTITY) > performing_task

							task_clear_look_at(player_Ped_id())
							
						endif 
					endif
				endif 
			endif
			
		break 
		
		case char_trevor
		
			if is_ped_in_front_of_ped(player_ped_id(), selector_ped.pedID[selector_ped_trevor])
				if get_script_task_status(player_Ped_id(), SCRIPT_TASK_LOOK_AT_ENTITY) > performing_task
					task_clear_look_at(player_Ped_id())
				endif 
			endif 
		
		break 
		
	endswitch 

endproc 
			
proc get_to_stadium_audio_scene_system()

	switch get_to_stadium_audio_scene_system_status 
	
		case 0
		
			if is_ped_sitting_in_any_vehicle(player_ped_id())
				start_AUDIO_SCENE("FAMILY_4_DRIVE_TO_STADIUM")	
				get_to_stadium_audio_scene_system_status++
			endif 

		break 
		
		case 1
		
			if IS_AUDIO_SCENE_ACTIVE("FAMILY_4_DRIVE_TO_STADIUM")
				if (is_ped_on_foot(player_ped_id()) and get_to_the_stadium_status > 0)
				or is_entity_in_angled_area(player_ped_id(), <<-248.824, -2021.740, 28.944>>, <<-258.934, -2033.879, 31.958>>, 4.5) 
					STOP_AUDIO_SCENE("FAMILY_4_DRIVE_TO_STADIUM")	
					get_to_stadium_audio_scene_system_status++
				endif 
			endif 
		
		break 
		
	endswitch 

endproc 

proc wanted_level_multiplier_system()

	if get_distance_between_coords(get_entity_coords(player_ped_id()), <<-219.5123, -2037.8512, 26.9>>, false) < 100
	
		set_wanted_level_multiplier(0.3)
		
	else 
	
		set_wanted_level_multiplier(0.5)
		
	endif 

endproc 

func float calculate_angled_area_width_for_vehicle_slow_down()
			
	float min_distance = 0.5
	float max_distance = 6.7
	
	float min_velocity = 3.0
	float max_velocity = 12.0
	
	float current_velocity
	
	float area_scale_multiplier
	float area_width
	
	vehicle_index players_car 
	
	if get_current_player_vehicle(players_car)
		
		current_velocity = get_entity_speed(players_car)
		
		if current_velocity < 3.0
			current_velocity = 3.0
		endif 
		
		area_scale_multiplier = (current_velocity - min_velocity) / (max_velocity - min_velocity)
		
		area_width = min_distance + ((max_distance - min_distance) * area_scale_multiplier)
			
		return area_width
	
	endif 
	
	return 6.7

endfunc


proc family_4_get_to_the_stadium()

	get_to_the_stadium_dialogue_system()
	
	create_vehicles_outside_stadium()
	
	groupie_ai_system()
	
	truck_driver_ai_system_2()
	
	ambient_ped_system()
	
	get_to_stadium_audio_scene_system()
	
	taxi_drop_off_for_get_to_the_stadium()
	
	//wanted_level_multiplier_system()

	//SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 3000, 53.6080) 
	//clear_players_task_on_control_input(SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
	
	switch get_to_the_stadium_status
	
		case 0
		
			request_mocap_data_for_family_4_mcs_2()
			
			float angled_area_width
			
			angled_area_width = calculate_angled_area_width_for_vehicle_slow_down()

			switch get_current_player_ped_enum()
	
				case char_michael
				
					IS_PLAYER_AT_LOCATION_WITH_BUDDY_any_means(locates_data, <<-219.5123, -2037.8512, 26.9>>, <<0.01, 0.01, LOCATE_SIZE_HEIGHT>>, true, selector_ped.pedID[selector_ped_trevor], "family4_god_0", "family4_god_8", true, true) 
						
					if does_blip_exist(locates_data.LocationBlip)
						
						if is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, angled_area_width)  
						or (IS_PED_IN_ANY_HELI(player_ped_id()) and is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, 6.7))
						or (is_ped_in_any_vehicle(player_ped_id()) and is_ped_within_range_of_target_heading(player_ped_id(), 328.7722, 15.00) and is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, 2.0)) //fix for bug 1833723
						or (is_ped_in_any_vehicle(player_ped_id()) and is_ped_within_range_of_target_heading(player_ped_id(), 148.7722, 15.00) and is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, 2.0))
						
							if get_current_player_vehicle(players_vehicle)
								
								if IS_VEHICLE_ON_ALL_WHEELS(players_vehicle)
								
									kill_any_conversation()
									get_to_the_stadium_dialogue_system_status = 5
							
									clear_mission_locate_stuff(locates_data, true)
							
									original_time = 0
									
									BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(players_vehicle)
							
									SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "outside stadium") 

									get_to_the_stadium_status++

								endif
							
							else 
							
								kill_any_conversation()
								get_to_the_stadium_dialogue_system_status = 5
						
								clear_mission_locate_stuff(locates_data, true)
						
								original_time = 0
							
								SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "outside stadium") 

								get_to_the_stadium_status = 2
								
							endif 
							
						endif 

					endif 

				break 
				
				case char_trevor
				
					IS_PLAYER_AT_LOCATION_WITH_BUDDY_any_means(locates_data, <<-219.5123, -2037.8512, 26.9>>, <<0.01, 0.01, LOCATE_SIZE_HEIGHT>>, true, selector_ped.pedID[selector_ped_michael], "family4_god_0", "family4_god_14", true, true) 

					if does_blip_exist(locates_data.LocationBlip)
						
						//if is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, angled_area_width)  
						if is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, angled_area_width)  
						or (IS_PED_IN_ANY_HELI(player_ped_id()) and is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, 6.7))
						or (is_ped_in_any_vehicle(player_ped_id()) and is_ped_within_range_of_target_heading(player_ped_id(), 328.7722, 15.00) and is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, 2.0))
						or (is_ped_in_any_vehicle(player_ped_id()) and is_ped_within_range_of_target_heading(player_ped_id(), 148.7722, 15.00) and is_entity_in_angled_area(player_ped_id(), <<-221.051, -2039.714, 26.620>>, <<-217.358, -2033.186, 29.620>>, 2.0))
						
//							if get_current_player_vehicle(players_vehicle)
//								
//								if IS_VEHICLE_ON_ALL_WHEELS(players_vehicle)
//		
//									clear_ped_tasks(selector_ped.pedID[selector_ped_michael])
//									set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
//		
//									clear_mission_locate_stuff(locates_data, false)
//							
//									kill_any_conversation()
//									get_to_the_stadium_dialogue_system_status = 5
//									//get_to_the_stadium_dialogue_system()
//							
//									BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(players_vehicle)//5
//							
//									if does_entity_exist(groopie.ped)
//										task_look_at_entity(player_ped_id(), groopie.ped, 8000, slf_while_not_in_fov)
//									endif 
//									
//									SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "outside stadium") 
//							 		
//									get_to_the_stadium_status++
//									
//								endif 
//								
//							else 
//							
//								clear_ped_tasks(selector_ped.pedID[selector_ped_michael])
//								set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
//	
//								clear_mission_locate_stuff(locates_data, false)
//						
//								kill_any_conversation()
//								get_to_the_stadium_dialogue_system_status = 5
//								//get_to_the_stadium_dialogue_system()
//						
//								if does_entity_exist(groopie.ped)
//									task_look_at_entity(player_ped_id(), groopie.ped, 8000, slf_while_not_in_fov)
//								endif 
//								
//								SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "outside stadium") 
//						 		
//								get_to_the_stadium_status++
//								
//							endif 
							
							clear_ped_tasks(selector_ped.pedID[selector_ped_michael])
							set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
		
							clear_mission_locate_stuff(locates_data, false)
							
							kill_any_conversation()
							get_to_the_stadium_dialogue_system_status = 5
							//get_to_the_stadium_dialogue_system()
							
							if get_current_player_vehicle(players_vehicle)
								BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(players_vehicle)//5
							endif 
							
							//task_leave_vehicle(player_ped_id(), players_vehicle)
							
							if does_entity_exist(groopie.ped)
								task_look_at_entity(player_ped_id(), groopie.ped, 8000, slf_while_not_in_fov)
							endif 
							
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "outside stadium") 
					 		
							get_to_the_stadium_status++
							
						endif 

					endif 
					
				break 
				
			endswitch 
			
			//fix for the player by passing the parked car section and entering the stadium
			if is_entity_in_angled_area(player_ped_id(), <<-248.824, -2021.740, 28.944>>, <<-258.934, -2033.879, 31.958>>, 4.5) //3.3
			or is_ped_inside_interior(player_ped_id(), <<-249.8176, -2008.0590, 29.1458>>)
				//if has_cutscene_loaded()	
				if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					if start_new_cutscene_no_fade(false, true, true, true)
					
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "outside stadium") 

						mission_flow = play_stadium_mocap
					
					endif 
				endif 
			endif 

		break 
		
		case 1
		
			if get_current_player_vehicle(players_vehicle)
				if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(players_vehicle)
					
					switch get_current_player_ped_enum()
	
						case char_michael
					
							if not is_ped_injured(selector_ped.pedID[selector_ped_trevor])
										
								clear_ped_tasks(selector_ped.pedID[selector_ped_trevor])
								set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_trevor], true)

								//is trevor is in the right has side of box go to the right to avoid the groopie
								//then walk to the else walk to the left
								//if is_entity_in_angled_area(selector_ped.pedID[selector_ped_trevor], <<-219.975, -2037.298, 26.620>>, <<-217.207, -2032.201, 29.620>>)
								
								if does_entity_exist(groopie.ped)
									task_look_at_entity(selector_ped.pedID[selector_ped_trevor], groopie.ped, 8000, slf_while_not_in_fov)
								endif 
								
								open_sequence_task(seq)
									task_pause(null, 500)
									task_leave_vehicle(null, players_vehicle)
									task_follow_nav_mesh_to_coord(null, <<-253.7119, -2028.4642, 28.9458>>, pedmove_walk, -1, -1)
									task_turn_ped_to_face_entity(null, player_ped_id(), -1)
								close_sequence_task(seq)
								task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
								clear_sequence_task(seq)
								
							endif 
							
							task_look_at_entity(player_ped_id(), selector_ped.pedID[selector_ped_trevor], 8000) 

							task_leave_vehicle(player_ped_id(), players_vehicle) 
							
						break 
						
					endswitch 

					get_to_the_stadium_status++
				
				endif 
			
			else 
				
				get_to_the_stadium_status++
				
			endif 
		
		break 
		
		case 2

			request_anim_dict("missfam4")
		
			request_mocap_data_for_family_4_mcs_2()

			switch get_current_player_ped_enum()
			
				case char_michael
					
					force_buddy_to_run_into_stadium(selector_ped.pedID[selector_ped_trevor])
			
					is_player_at_location_on_foot(locates_data, <<-255.13390, -2026.67908, 28.9458>>, <<0.01, 0.01, LOCATE_SIZE_HEIGHT>>, true, "", true, true, radar_trace_invalid, false)

					if not is_entity_at_coord(selector_ped.pedID[selector_ped_trevor], <<-253.7119, -2028.4642, 28.9458>>, <<1.5, 1.5, 2.0>>)
						if has_ped_task_finished_2(selector_ped.pedID[selector_ped_trevor])

							open_sequence_task(seq)
								task_follow_nav_mesh_to_coord(null, <<-253.7119, -2028.4642, 28.9458>>, pedmove_run, -1, -1)
								task_turn_ped_to_face_entity(null, player_ped_id())
								task_look_at_entity(null, player_ped_id(), -1)
							close_sequence_task(seq)
							task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
							clear_sequence_task(seq)
							
						endif
						
					else 

						if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_trevor]), get_entity_coords(player_ped_id())) > 2
							
							if is_ped_facing_ped(selector_ped.pedID[selector_ped_trevor], player_ped_id(), 20)
								if has_anim_dict_loaded("missfam4")
									if lk_timer(original_time, 8000)
									
										if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_trevor])) < 40.00
											create_conversation(scripted_speech, "fam4Aud", "FAM4_LAG", conv_priority_low)
										endif 
									
										open_sequence_task(seq)
											task_turn_ped_to_face_entity(null, player_ped_id())
											task_play_anim(null, "missfam4", "say_hurry_up_a_trevor", slow_blend_in)
										close_sequence_task(seq)
										task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
										clear_sequence_task(seq)
										
										original_time = get_game_timer()
										
									endif 
									
								endif 
								
							else 
								
								if has_ped_task_finished_2(selector_ped.pedID[selector_ped_trevor])
							
									open_sequence_task(seq)
										task_turn_ped_to_face_entity(null, player_ped_id())
									close_sequence_task(seq)
									task_perform_sequence(selector_ped.pedID[selector_ped_trevor], seq)
									clear_sequence_task(seq)
									
								endif 
								
							endif 
							
						endif 

					endif 
					
				break 
				
				case char_trevor
				
					IS_PLAYER_AT_LOCATION_WITH_BUDDY_ON_FOOT(locates_data, <<-255.13390, -2026.67908, 28.9458>>, <<0.01, 0.01, LOCATE_SIZE_HEIGHT>>, true, selector_ped.pedID[selector_ped_michael], "", "family4_god_14", true, true)  

				break 
				
			endswitch 
		

			head_track_system()
			
			if does_blip_exist(locates_data.LocationBlip)
				if is_entity_in_angled_area(player_ped_id(), <<-248.824, -2021.740, 28.944>>, <<-258.934, -2033.879, 31.958>>, 7.0) //4.5
				or is_ped_inside_interior(player_ped_id(), <<-249.8176, -2008.0590, 29.1458>>)
					if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						if start_new_cutscene_no_fade(false, true, true, true)
						
							mission_flow = play_stadium_mocap
						
						endif 
					endif 
				endif 
			endif 

		break 
		
	endswitch 
	
	//truck_and_trailer_recording_system()
	
	//mission_flow = storm_drain_cutscene //debug to skip to the cutscene
	
endproc 

func bool family_4_play_stadium_mocap()

	int i = 0

	ambient_ped_system()
	
	printstring("get_cutscene_time() = ")
	printint(get_cutscene_time())
	printnl()

	switch family_4_play_stadium_mocap_status 
	
		case 0

			if is_players_last_car_present()
				player_arrived_in_a_vehicle = true 
			endif 

			SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
			
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "lazlow", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, lazlow.model)
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Tracy", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, tracey.model)
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "judges_security", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, security.model)
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			switch get_current_player_ped_enum() 
			
				case char_michael
					
					set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_trevor], true)
					register_entity_for_cutscene(selector_ped.pedID[selector_ped_trevor], "trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					trigger_switch_effect_to_trevor = true
					
					START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_TREVOR)
				break 
				
				case char_trevor
					
					set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
					register_entity_for_cutscene(selector_ped.pedID[selector_ped_michael], "michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					START_CUTSCENE()
				break 
				
			endswitch
			

			family_4_play_stadium_mocap_status++
			
		break 
		
		case 1

			if is_cutscene_playing()
			
				clear_area(<<-228.2306, -2047.4192, 26.6199>>, 1000.00, true)

				SET_VEHICLE_POPULATION_BUDGET(0)
				
				//delete the players car as it has been towed. When they chase lazlow out of the stadium
//				players_vehicle = get_players_last_vehicle()
//				
//				if does_entity_exist(players_vehicle)
//					if not IS_VEHICLE_IN_PLAYERS_GARAGE(players_vehicle, GET_CURRENT_PLAYER_PED_ENUM(), TRUE)
//						delete_vehicle(players_vehicle)
//					endif 
//				endif 
				
				remove_anim_dict("missfam4")

				if does_entity_exist(lazlows_car.veh)
					delete_vehicle(lazlows_car.veh)
					set_model_as_no_longer_needed(lazlows_car.model)
				endif 
				
				if does_entity_exist(truck.veh)
					delete_vehicle(truck.veh)
					set_model_as_no_longer_needed(truck.model)
				endif 
				
				if does_entity_exist(trailer.veh)
					delete_vehicle(trailer.veh)
				endif 
				set_model_as_no_longer_needed(trailer.model)
				
				for i = 0 to count_of(parked_truck) - 1
					
					if does_entity_exist(parked_truck[i].veh)
						delete_vehicle(parked_truck[i].veh)
						set_model_as_no_longer_needed(parked_truck[i].model)
					endif 
					
					if does_entity_exist(parked_trailer[i].veh)
						delete_vehicle(parked_trailer[i].veh)
					endif 
					set_model_as_no_longer_needed(parked_trailer[i].model)
					
				endfor 
				
				if does_entity_exist(truck_driver.ped)
					delete_ped(truck_driver.ped)
				endif 
				
				if does_entity_exist(ambient_car)
					delete_vehicle(ambient_car)
				endif 
				
				if does_entity_exist(ambient_car_2)
					delete_vehicle(ambient_car_2)
				endif 
				
				if does_entity_exist(ambient_car_3)
					delete_vehicle(ambient_car_3)
				endif 
				
				if does_entity_exist(ambient_car_4)
					delete_vehicle(ambient_car_4)
				endif 
				
				if does_entity_exist(ambient_car_5)
					delete_vehicle(ambient_car_5)
				endif 
				
				set_model_as_no_longer_needed(sentinel)
				set_model_as_no_longer_needed(schafter2)
				
				if does_entity_exist(clip_board.obj)				
					set_model_as_no_longer_needed(clip_board.model)
					delete_object(clip_board.obj)
				endif 
				
				if does_entity_exist(groopie.ped)
					delete_ped(groopie.ped)
					set_model_as_no_longer_needed(groopie.model)
				endif 
				
//				for i = 0 to count_of(ambient_ped) - 1
//					if does_entity_exist(ambient_ped[i].ped)
//						delete_ped(ambient_ped[i].ped)
//					endif
//					set_model_as_no_longer_needed(ambient_ped[i].model)
//					ambient_ped[i].created = false
//				endfor 

				remove_vehicle_recording(501, "lkfamily4")
				remove_vehicle_recording(502, "lkfamily4")

				//set_state_of_closest_door_of_type(v_62_aud_door02, <<-242.6, -2005.5, 24.8>>, false, 0.0)
				//set_state_of_closest_door_of_type(v_62_aud_door01, <<-243.1, -2007.8, 24.8>>, false, 0.0)
				
				clear_mission_locate_stuff(locates_data, true)

				for i = 8 to 25
					if not is_ped_injured(ambient_ped[i].ped)
						task_look_at_coord(ambient_ped[i].ped, <<-249.6, -2016.1, 30.8>>, 15000, slf_while_not_in_fov)
					endif 
				endfor 
					
				family_4_play_stadium_mocap_status++
			endif 
		
		break 
		
		case 2

			if is_cutscene_active()
				
				if not WAS_CUTSCENE_SKIPPED()

					if get_cutscene_time() >= 40000
						load_stadium_assets()
					endif 
					
					if not has_label_been_triggered("FAM4_MISSION_START")
						if get_cutscene_time() >= 125366
							trigger_music_event("FAM4_MISSION_START")
							set_label_as_triggered("FAM4_MISSION_START", true)
						endif 
					endif 
					
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow", lazlow.model))
						lazlow.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow", lazlow.model))
					ENDIF
					
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tracy", tracey.model))
						tracey.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tracy", tracey.model))
					ENDIF
					
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("judges_security", security.model))
						security.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("judges_security", security.model))
					ENDIF
					
						
					switch get_current_player_ped_enum()
					
						case char_michael
						
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))

								set_entity_coords(player_ped_id(), <<-240.5416, -2006.2936, 23.6856>>)
								set_entity_heading(player_ped_id(), 85.0646)

								open_sequence_task(seq)
									TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 8, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
								close_sequence_task(seq)
								task_perform_sequence(player_ped_id(), seq)
								clear_sequence_task(seq)
								
								FORCE_ped_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN)
								force_ped_ai_and_animation_update(player_ped_id())
	
							endif 

							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor))

								make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
								take_control_of_selector_ped(selector_ped, true, true)
						
								set_entity_coords(player_ped_id(), <<-238.8316, -2005.5457, 23.6855>>)//<<-239.1158, -2005.1418, 23.6855>>)
								set_entity_heading(player_ped_id(), 103.8542) //122.00
						
								open_sequence_task(seq)
									TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 5, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS, 13)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
								close_sequence_task(seq)
								task_perform_sequence(player_ped_id(), seq)
								clear_sequence_task(seq)
						
								FORCE_ped_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN)
								force_ped_ai_and_animation_update(player_ped_id())

							endif 
						
						break 
						
						case char_trevor
						
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
						
								set_entity_coords(selector_ped.pedID[selector_ped_michael], <<-240.5416, -2006.2936, 23.6856>>)
								set_entity_heading(selector_ped.pedID[selector_ped_michael], 85.0646)

								open_sequence_task(seq)
									TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 8, EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
								close_sequence_task(seq)
								task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
								clear_sequence_task(seq)
								
								FORCE_ped_MOTION_STATE(selector_ped.pedID[selector_ped_michael], MS_ON_FOOT_RUN)
								force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])

							endif 
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor))
	
								set_entity_coords(player_ped_id(), <<-238.8316, -2005.5457, 23.6855>>)//<<-239.1158, -2005.1418, 23.6855>>)
								set_entity_heading(player_ped_id(), 103.8542) //122.00
								
								open_sequence_task(seq)
									TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 5, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS , 13)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
								close_sequence_task(seq)
								task_perform_sequence(player_ped_id(), seq)
								clear_sequence_task(seq)
								
								FORCE_ped_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN)
								force_ped_ai_and_animation_update(player_ped_id())

							endif 
						
						break 
						
					endswitch 

						
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("lazlow", lazlow.model)
						if not is_ped_injured(lazlow.ped)
							//create_npc_ped_on_foot(lazlow.ped, CHAR_LAZLOW, lazlow.pos, lazlow.heading, false)
							set_entity_coords(lazlow.ped, lazlow.pos)
							set_entity_heading(lazlow.ped, lazlow.heading)
							setup_buddy_attributes(lazlow.ped)
							SET_PED_COMBAT_ATTRIBUTES(lazlow.ped, ca_leave_vehicles, false)
							set_blocking_of_non_temporary_events(lazlow.ped, true)
							lazlow.blip = create_blip_for_ped(lazlow.ped, true) 
							add_ped_for_dialogue(scripted_speech, 5, lazlow.ped, "lazlow")
							FORCE_ROOM_FOR_ENTITY(lazlow.ped, get_interior_at_coords_with_type(<<-239.1158, -2005.1418, 23.6855>>, "v_stadium"), get_hash_key("GtaMloRoom001"))
						endif
					endif 
					
//					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("tracy", tracey.model)
//						if not is_ped_injured(tracey.ped)
//							setup_buddy_attributes(tracey.ped)
//							setup_relationship_contact(tracey.ped, true)
//							set_ped_can_ragdoll(tracey.ped, true)
//							add_ped_for_dialogue(scripted_speech, 4, tracey.ped, "tracey")
//						endif 
//					endif 
					
					if can_set_exit_state_for_registered_entity("judges_security", security.model)
						if not is_ped_injured(security.ped)
							set_entity_health(security.ped, 2)
							set_ped_as_no_longer_needed(security.ped)
							set_model_as_no_longer_needed(security.model)
						endif 
					endif 
					
//					if CAN_SET_EXIT_STATE_FOR_CAMERA()
//						set_gameplay_cam_relative_heading(0)
//						set_gameplay_cam_relative_pitch(0)
//					endif
					
				else 
			
					SET_CUTSCENE_FADE_VALUES(false, false, true)
		
					family_4_play_stadium_mocap_status++

				endif 
					
			else 
					
				if has_stadium_assets_loaded()

					original_time = get_game_timer()
				
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)

					create_stadium_assets()
					
					REPLAY_STOP_EVENT()
					
					return true

				endif 
				
			endif 

		break 
		
		case 3

			if is_cutscene_active()
				
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow", lazlow.model))
					lazlow.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow", lazlow.model))
				ENDIF
				
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tracy", tracey.model))
					tracey.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tracy", tracey.model))
				ENDIF
				
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("judges_security", security.model))
					security.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("judges_security", security.model))
				ENDIF
				
				if can_set_exit_state_for_registered_entity("judges_security", security.model)
					if not is_ped_injured(security.ped)
						set_entity_health(security.ped, 2)
						set_ped_as_no_longer_needed(security.ped)
						set_model_as_no_longer_needed(security.model)
					endif 
				endif 
				
				//can't do exit state for michael and trevor in here because new load scene needs to be called
				//hard coding it in the no is_cutscene_active() state.
				
			else 
			
				load_stadium_assets()

				NEW_LOAD_SCENE_START(<<-239.1602, -2002.6151, 23.6856>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 145.8395>>), 3.0)

				while not has_stadium_assets_loaded()
				or not IS_NEW_LOAD_SCENE_LOADED()
				//or not is_cutscene_active()
				
					if not has_stadium_assets_loaded()
						printstring("waiting on stadium_assets to load")
						printnl()
					endif 
					
					if not IS_NEW_LOAD_SCENE_LOADED()
						printstring("waiting on new load scene")
						printnl()
					endif 

					wait(0)
					
				endwhile
					
				NEW_LOAD_SCENE_STOP()

				if get_current_player_ped_enum() != char_trevor
					make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
					take_control_of_selector_ped(selector_ped, true, true)
				endif 
				
				set_entity_coords(player_ped_id(), <<-238.8316, -2005.5457, 23.6855>>)//<<-239.1158, -2005.1418, 23.6855>>)
				set_entity_heading(player_ped_id(), 103.8542) //122.00

				open_sequence_task(seq)
					TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 5,  EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS, 13)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
				close_sequence_task(seq)
				task_perform_sequence(player_ped_id(), seq)
				clear_sequence_task(seq)
				
				FORCE_ped_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN)
				force_ped_ai_and_animation_update(player_ped_id())

				if not is_ped_injured(selector_ped.pedID[selector_ped_michael])

					set_entity_coords(selector_ped.pedID[selector_ped_michael], <<-240.5416, -2006.2936, 23.6856>>)
					set_entity_heading(selector_ped.pedID[selector_ped_michael], 85.0646)

					open_sequence_task(seq)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, "family4_1", 8,  EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)//, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
					close_sequence_task(seq)
					task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
					clear_sequence_task(seq)
					
					FORCE_ped_MOTION_STATE(selector_ped.pedID[selector_ped_michael], MS_ON_FOOT_RUN)
					force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])

				endif 

				if not is_ped_injured(lazlow.ped)
					//create_npc_ped_on_foot(lazlow.ped, CHAR_LAZLOW, lazlow.pos, lazlow.heading, false)
					set_entity_coords(lazlow.ped, lazlow.pos)
					set_entity_heading(lazlow.ped, lazlow.heading)
					setup_buddy_attributes(lazlow.ped)
					SET_PED_COMBAT_ATTRIBUTES(lazlow.ped, ca_leave_vehicles, false)
					set_blocking_of_non_temporary_events(lazlow.ped, true)
					lazlow.blip = create_blip_for_ped(lazlow.ped, true) 
					add_ped_for_dialogue(scripted_speech, 5, lazlow.ped, "lazlow")
				endif

				original_time = get_game_timer()
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
				
				create_stadium_assets()
				
				if not is_ped_injured(security.ped)
					set_entity_health(security.ped, 2)
					set_ped_as_no_longer_needed(security.ped)
					set_model_as_no_longer_needed(security.model)
				endif 
				
				REPLAY_STOP_EVENT()
				
				return true
				
			endif 
		
		break 
		
	endswitch 

	return false 
	
endfunc 


vehicle_index dummy_lazlow_car

proc family_4_record_lazlow()

	int i = 0

	#IF IS_DEBUG_BUILD
		
		if widget_reset_recording
			family_4_record_lazlow_status = 0
			widget_reset_recording = false 
		endif
		
	#ENDIF 
		
	switch family_4_record_lazlow_status
	
		case 0
		
			request_model(lazlows_car.model)
			set_vehicle_model_is_suppressed(lazlows_car.model, true)
			
			request_model(sentinel)
			request_model(schafter2)
			request_model(manana)

			load_mission_vehicle_recordings()


			if has_model_loaded(lazlows_car.model)
			and has_model_loaded(sentinel)
			and has_model_loaded(schafter2)
			and has_model_loaded(manana)
			and has_mission_vehicle_recordings_loaded()
			
				set_vehicle_population_budget(0)


				if not does_entity_exist(lazlows_car.veh)
					
					lazlows_car.veh = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car.heading)
					set_vehicle_strong(lazlows_car.veh, true)
					SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
					SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
					set_vehicle_tyres_can_burst(lazlows_car.veh, false)
					set_entity_only_damaged_by_player(lazlows_car.veh, true)
					set_vehicle_doors_locked(lazlows_car.veh, vehiclelock_lockout_player_only) 
					set_vehicle_colours(lazlows_car.veh, 27, 0)
					//SET_ENTITY_LOAD_COLLISION_FLAG(lazlows_car.veh, true)
					SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
					SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
					set_entity_collision(lazlows_car.veh, false)
					set_entity_visible(lazlows_car.veh, false)
					lazlows_Car.blip = create_blip_for_entity(lazlows_car.veh)

					#IF IS_DEBUG_BUILD
						set_uber_parent_widget_group(family_4_widget_group)
					#endif
					
					INITIALISE_UBER_PLAYBACK("lkfamily4", 001, true)
					//load_uber_data()
					bCreateAllWaitingCars = true
					bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = true 
					allow_veh_to_stop_on_any_veh_impact = true
//					bTrafficDontCleanupRecordingFiles = true
					allow_trailer_touching_check = true
					traffic_block_vehicle_colour(true, traffic_red, traffic_orange)
					start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4")

				endif 
				
				if is_vehicle_driveable(lazlows_car.veh)
					set_vehicle_fixed(lazlows_car.veh)
				endif 

					
				if does_entity_exist(selector_ped.pedID[selector_ped_michael])
					SET_PED_AS_NO_LONGER_NEEDED(selector_ped.pedID[selector_ped_michael])
					set_model_as_no_longer_needed(michael.model)
				endif 
				
				if does_entity_exist(truck.veh)
					set_vehicle_as_no_longer_needed(truck.veh)
					set_model_as_no_longer_needed(truck.model)
				endif 
				
				if does_entity_exist(trailer.veh)
					set_vehicle_as_no_longer_needed(trailer.veh)
					set_model_as_no_longer_needed(trailer.model)
				endif 
				
				for i = 0 to count_of(parked_truck) - 1
					
					if does_entity_exist(parked_truck[i].veh)
						set_vehicle_as_no_longer_needed(parked_truck[i].veh)
						set_model_as_no_longer_needed(parked_truck[i].model)
					endif 
					
					if does_entity_exist(parked_trailer[i].veh)
						set_vehicle_as_no_longer_needed(parked_trailer[i].veh)
						set_model_as_no_longer_needed(parked_trailer[i].model)
					endif 
					
				endfor 
				
				if not does_entity_exist(dummy_lazlow_car)
					dummy_lazlow_car = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car.heading)
					set_entity_proofs(dummy_lazlow_car, true, true, true, true, true)
					set_vehicle_strong(dummy_lazlow_car, true)
					SET_VEHICLE_HAS_STRONG_AXLES(dummy_lazlow_car, true)
					set_vehicle_colours(dummy_lazlow_car, 12, 0)
					MODIFY_VEHICLE_TOP_SPEED(dummy_lazlow_car, -20)
				endif 
				
				if is_vehicle_driveable(dummy_lazlow_car)
					
					set_entity_coords(dummy_lazlow_car, lazlows_car.pos)
					set_entity_heading(dummy_lazlow_car, lazlows_car.heading)
					set_vehicle_on_ground_properly(dummy_lazlow_car)
					set_vehicle_fixed(dummy_lazlow_car)
					
					clear_ped_tasks_immediately(player_ped_id())
					set_ped_into_vehicle(player_ped_id(), dummy_lazlow_car)
					
					target_speed = -20

				endif 
				
				//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
				//DISPLAY_PLAYBACK_RECORDED_vehicle(lazlows_car.veh, RDM_WHOLELINE)
				
				family_4_record_lazlow_status++

			endif 
			
		break 
	
		case 1
		
			printfloat(target_speed)
			printnl()
			
//			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
//			DISPLAY_PLAYBACK_RECORDED_vehicle(lazlows_car.veh, RDM_WHOLELINE)
		
			SET_TEXT_SCALE(0.0000, 1.5)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_CENTRE(FALSE)
			SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
			SET_TEXT_EDGE(0, 0, 0, 0, 0)
			DISPLAY_TEXT_WITH_NUMBER(0.5, 0.2, "NUMBER", round(get_entity_speed(dummy_lazlow_car)))
			
			if is_control_pressed(frontend_control, INPUT_SCRIPT_RDOWN)
				
				if target_speed < 0
					target_speed += get_frame_time() * 2.0
					
					if target_speed >= 0
						target_speed = 0
					endif 
					
					MODIFY_VEHICLE_TOP_SPEED(dummy_lazlow_car, target_speed)
					
				endif 
				
			else
				
				if target_speed > -20
					target_speed -= get_frame_time()
						
					if target_speed <= -20
						target_speed = -20
					endif 
					
					MODIFY_VEHICLE_TOP_SPEED(dummy_lazlow_car, target_speed)

				endif 
			endif 
		
			if not is_entity_dead(dummy_lazlow_car)
				
				set_vehicle_strong(dummy_lazlow_car, true)
				SET_VEHICLE_HAS_STRONG_AXLES(dummy_lazlow_car, true)
				set_vehicle_fixed(dummy_lazlow_car)
				set_entity_health(dummy_lazlow_car, 10000)
				set_vehicle_petrol_tank_health(dummy_lazlow_car, 10000)
				set_vehicle_engine_health(dummy_lazlow_car, 10000)
				set_entity_proofs(dummy_lazlow_car, true, true, true, true, true)
				
			endif
		
			if is_vehicle_driveable(lazlows_car.veh)
				
				set_vehicle_fixed(lazlows_car.veh)
			
				update_uber_playback(lazlows_car.veh, 1.0)
				
				switch_setpiece_car_to_ai_system()
				
				create_parked_vehicles_system()
				
			endif 
			
		break 
		
		case 2
		
			request_model(lazlows_car.model)
			set_vehicle_model_is_suppressed(lazlows_car.model, true)
			
			request_model(sentinel)
			request_model(schafter2)
			request_model(manana)

			load_mission_vehicle_recordings()
			
			request_vehicle_recording(001, "lkfamily4b")


			if has_model_loaded(lazlows_car.model)
			and has_model_loaded(sentinel)
			and has_model_loaded(schafter2)
			and has_model_loaded(manana)
			and has_mission_vehicle_recordings_loaded()
			and has_vehicle_recording_been_loaded(001, "lkfamily4b")
		
				lazlows_car.veh = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car.heading)
				set_vehicle_strong(lazlows_car.veh, true)
				SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
				SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
				set_vehicle_tyres_can_burst(lazlows_car.veh, false)
				set_entity_only_damaged_by_player(lazlows_car.veh, true)
				set_vehicle_doors_locked(lazlows_car.veh, vehiclelock_lockout_player_only) 
				set_vehicle_colours(lazlows_car.veh, 27, 0)
				//SET_ENTITY_LOAD_COLLISION_FLAG(lazlows_car.veh, true)
				SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
				SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
				set_entity_collision(lazlows_car.veh, false)
				set_entity_visible(lazlows_car.veh, true)
				lazlows_Car.blip = create_blip_for_entity(lazlows_car.veh)
				start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4b")

				dummy_lazlow_car = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car.heading)
				set_entity_proofs(dummy_lazlow_car, true, true, true, true, true)
				set_vehicle_strong(dummy_lazlow_car, true)
				SET_VEHICLE_HAS_STRONG_AXLES(dummy_lazlow_car, true)
				set_vehicle_colours(dummy_lazlow_car, 12, 0)
				MODIFY_VEHICLE_TOP_SPEED(dummy_lazlow_car, -25)
				start_playback_recorded_vehicle(dummy_lazlow_car, 001, "lkfamily4")
				
				if does_entity_exist(selector_ped.pedID[selector_ped_michael])
					SET_PED_AS_NO_LONGER_NEEDED(selector_ped.pedID[selector_ped_michael])
					set_model_as_no_longer_needed(michael.model)
				endif 
				
				if does_entity_exist(truck.veh)
					set_vehicle_as_no_longer_needed(truck.veh)
					set_model_as_no_longer_needed(truck.model)
				endif 
				
				if does_entity_exist(trailer.veh)
					set_vehicle_as_no_longer_needed(trailer.veh)
					set_model_as_no_longer_needed(trailer.model)
				endif 
				
				for i = 0 to count_of(parked_truck) - 1
					
					if does_entity_exist(parked_truck[i].veh)
						set_vehicle_as_no_longer_needed(parked_truck[i].veh)
						set_model_as_no_longer_needed(parked_truck[i].model)
					endif 
					
					if does_entity_exist(parked_trailer[i].veh)
						set_vehicle_as_no_longer_needed(parked_trailer[i].veh)
						set_model_as_no_longer_needed(parked_trailer[i].model)
					endif 
					
				endfor 
				
				if not does_entity_exist(dummy_lazlow_car)
					dummy_lazlow_car = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car.heading)
					set_entity_proofs(dummy_lazlow_car, true, true, true, true, true)
					set_vehicle_strong(dummy_lazlow_car, true)
					SET_VEHICLE_HAS_STRONG_AXLES(dummy_lazlow_car, true)
					set_vehicle_colours(dummy_lazlow_car, 12, 0)
					MODIFY_VEHICLE_TOP_SPEED(dummy_lazlow_car, -25)
				endif 
				
				if is_vehicle_driveable(dummy_lazlow_car)
					
					set_entity_coords(dummy_lazlow_car, lazlows_car.pos)
					set_entity_heading(dummy_lazlow_car, lazlows_car.heading)
					set_vehicle_on_ground_properly(dummy_lazlow_car)
					set_vehicle_fixed(dummy_lazlow_car)
					
					clear_ped_tasks_immediately(player_ped_id())
					set_ped_into_vehicle(player_ped_id(), dummy_lazlow_car)

				endif 
				
				family_4_record_lazlow_status++
			endif 
		
		break 
		
		case 3
		
			#IF IS_DEBUG_BUILD
				//old
				SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(lazlows_car.veh, widget_time_a)
	
				//new
				SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(dummy_lazlow_car, widget_time_b)
			#endif 
			
		break 
		
		case 4
		
			request_model(lazlows_car.model)
			set_vehicle_model_is_suppressed(lazlows_car.model, true)
			
			request_model(sentinel)
			request_model(schafter2)
			request_model(manana)

			load_mission_vehicle_recordings()


			if has_model_loaded(lazlows_car.model)
			and has_model_loaded(sentinel)
			and has_model_loaded(schafter2)
			and has_model_loaded(manana)
			and has_mission_vehicle_recordings_loaded()
			
				set_vehicle_population_budget(0)


				if not does_entity_exist(lazlows_car.veh)
					
					lazlows_car.veh = create_vehicle(lazlows_car.model, lazlows_car.pos, lazlows_car.heading)
					set_vehicle_strong(lazlows_car.veh, true)
					SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
					SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
					set_vehicle_tyres_can_burst(lazlows_car.veh, false)
					set_entity_only_damaged_by_player(lazlows_car.veh, true)
					set_vehicle_doors_locked(lazlows_car.veh, vehiclelock_lockout_player_only) 
					set_vehicle_colours(lazlows_car.veh, 27, 0)
					//SET_ENTITY_LOAD_COLLISION_FLAG(lazlows_car.veh, true)
					SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
					SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
					set_entity_collision(lazlows_car.veh, false)
					set_entity_visible(lazlows_car.veh, true)
					lazlows_Car.blip = create_blip_for_entity(lazlows_car.veh)

					#IF IS_DEBUG_BUILD
						set_uber_parent_widget_group(family_4_widget_group)
					#endif
					
					INITIALISE_UBER_PLAYBACK("lkfamily4", 001, true)
					load_uber_data()
					fUberPlaybackDensitySwitchOffRange = 200
					bCreateAllWaitingCars = true
					bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = true 
					allow_veh_to_stop_on_any_veh_impact = true
//					bTrafficDontCleanupRecordingFiles = true
					allow_trailer_touching_check = true
					traffic_block_vehicle_colour(true, traffic_red, traffic_orange)
					start_playback_recorded_vehicle(lazlows_car.veh, 001, "lkfamily4")

				endif 
				
				if is_vehicle_driveable(lazlows_car.veh)
					set_vehicle_fixed(lazlows_car.veh)
				endif 
	
				if does_entity_exist(selector_ped.pedID[selector_ped_michael])
					SET_PED_AS_NO_LONGER_NEEDED(selector_ped.pedID[selector_ped_michael])
					set_model_as_no_longer_needed(michael.model)
				endif 
				
				if does_entity_exist(truck.veh)
					set_vehicle_as_no_longer_needed(truck.veh)
					set_model_as_no_longer_needed(truck.model)
				endif 
				
				if does_entity_exist(trailer.veh)
					set_vehicle_as_no_longer_needed(trailer.veh)
					set_model_as_no_longer_needed(trailer.model)
				endif 
				
				for i = 0 to count_of(parked_truck) - 1
					
					if does_entity_exist(parked_truck[i].veh)
						set_vehicle_as_no_longer_needed(parked_truck[i].veh)
						set_model_as_no_longer_needed(parked_truck[i].model)
					endif 
					
					if does_entity_exist(parked_trailer[i].veh)
						set_vehicle_as_no_longer_needed(parked_trailer[i].veh)
						set_model_as_no_longer_needed(parked_trailer[i].model)
					endif 
					
				endfor 
				
				clear_ped_tasks_immediately(player_ped_id())
				set_entity_coords(player_ped_id(), <<-234.7623, -2046.0051, 26.7555>>)
				set_entity_heading(player_ped_id(), 216.8192)
				
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
//				DISPLAY_PLAYBACK_RECORDED_vehicle(lazlows_car.veh, RDM_WHOLELINE)
				
				family_4_record_lazlow_status++
				
			endif 
		
		break 
		
		case 5
		
			#if is_debug_build
				create_uber_vehicles_widget()
			#endif 
		
			update_uber_playback(lazlows_car.veh, 1.0)
				
//			switch_setpiece_car_to_ai_system()
//				
//			create_parked_vehicles_system()
		
		break 
		
	endswitch 

	//draw_sphere(get_offset_from_entity_in_world_coords(lazlows_car.veh, <<0.00, 0.0, 0.0>>), 2.5)

endproc 

proc family_4_deatch_trailer_cutscene_test_bed()
				
	switch family_4_detach_trailer_status 
	
		case 0

			request_vehicle_recording(001, "lkfamily4")
			request_vehicle_recording(201, "lkfamily4")
			request_vehicle_recording(202, "lkfamily4")
			request_vehicle_recording(203, "lkfamily4")
			request_vehicle_recording(204, "lkfamily4")
			request_vehicle_recording(205, "lkfamily4")
			
			request_model(trailer.model)
			set_vehicle_model_is_suppressed(trailer.model, true)
			
			request_model(truck.model)
			set_vehicle_model_is_suppressed(truck.model, true)
			
			request_model(premier)
			request_model(sentinel)
			
			request_model(lazlows_car.model)
			set_vehicle_model_is_suppressed(lazlows_car.model, true)
			
			while not has_model_loaded(trailer.model)
			or not has_model_loaded(truck.model)
			or not has_model_loaded(premier)
			or not has_model_loaded(sentinel)
			or not has_model_loaded(lazlows_car.model)
			or not has_vehicle_recording_been_loaded(001, "lkfamily4")
			or not has_vehicle_recording_been_loaded(201, "lkfamily4")
			or not has_vehicle_recording_been_loaded(202, "lkfamily4")
			or not has_vehicle_recording_been_loaded(203, "lkfamily4")
			or not has_vehicle_recording_been_loaded(204, "lkfamily4")
			or not has_vehicle_recording_been_loaded(205, "lkfamily4")
//			
				wait(0)
				
			endwhile
			
			truck.veh = create_vehicle(truck.model, <<-57.6468, -547.7130, 39.0136>>, 341.9843)
			set_vehicle_strong(truck.veh, true)
			SET_VEHICLE_HAS_STRONG_AXLES(truck.veh, true)
			SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
			set_vehicle_tyres_can_burst(truck.veh, false)
			set_vehicle_colours(truck.veh, 73, 0)
			trailer.veh = create_vehicle(trailer.model, <<-47.6468, -547.7130, 39.0136>>, 341.9843)
			set_vehicle_tyres_can_burst(trailer.veh, false)
			attach_vehicle_to_trailer(truck.veh, trailer.veh)
//			
			set_ped_into_vehicle(player_ped_id(), truck.veh) 
			
			load_scene(<<-47.2755, -515.6834, 39.4660>>)
			
			//hack fix for weird time delay via jumping to cutscene
			original_time = get_game_timer()
			while not lk_timer(original_time, 5000)
				wait(0)
			endwhile
			
			family_4_detach_trailer_status++
		
		break 
		
		case 1
		
			detach_trailer_cutscene_system()
			
		break 
		
		case 2
		
		break 
		
	endswitch 

endproc 

proc load_final_mocap()

	clear_area(<<1063.57, -284.17, 50.6>>, 200.00, true)
	
	clear_player_wanted_level(player_id())
	set_max_wanted_level(0)
	set_create_random_cops(false)

	DELETE_ALL_TRAINS() 
	SET_RANDOM_TRAINS(false) 

	set_vehicle_model_is_suppressed(utillitruck, true)
	set_vehicle_model_is_suppressed(utillitruck2, true)
	set_vehicle_model_is_suppressed(utillitruck3, true)
	
	request_model(michael.model)
	request_model(trevor.model)

	request_model(truck.model)
	set_vehicle_model_is_suppressed(truck.model, true)
	
	request_model(trailer.model)
	set_vehicle_model_is_suppressed(trailer.model, true)
	
	request_model(lazlows_car.model)
	set_vehicle_model_is_suppressed(lazlows_car.model, true)
	
	request_model(lazlow.model)
	set_ped_model_is_suppressed(lazlow.model, true)
	
	request_vehicle_recording(401, "lkfamily4")
	request_vehicle_recording(402, "lkfamily4")
	
	REQUEST_CUTSCENE("Family_4_MCS_3_concat")
	request_mocap_data_for_Family_4_MCS_3_concat()
	
	request_ptfx_asset()
	
//	while not CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//		wait(0)	
//	endwhile 
	
	while not has_model_loaded(michael.model)
	or not has_model_loaded(trevor.model)
	or not has_model_loaded(truck.model)
	or not has_model_loaded(trailer.model)
	or not has_model_loaded(lazlows_car.model)
	or not has_model_loaded(lazlow.model)
	or not request_mocap_data_for_Family_4_MCS_3_concat()
	or not has_cutscene_loaded()
	or not has_vehicle_recording_been_loaded(401, "lkfamily4")
	or not has_vehicle_recording_been_loaded(402, "lkfamily4")
	or not has_ptfx_asset_loaded()

		wait(0)
		
	endwhile 
	
	clear_area(<<-219.6464, -2037.8220, 26.6207>>, 300.00, true)
	
	truck.veh = create_vehicle(truck.model, <<1054.1014, -298.7344, 49.2696>>, 330.2198)
	SET_VEHICLE_ENGINE_ON(truck.veh, TRUE, TRUE)
	set_vehicle_tyres_can_burst(truck.veh, false)
	set_vehicle_colours(truck.veh, 0, 0)
	set_entity_health(truck.veh, 1500)
	set_vehicle_engine_health(truck.veh, 1500)
	set_vehicle_petrol_tank_health(truck.veh, 1500)
	//set_vehicle_dirt_level(truck.veh, 0.0)
	set_vehicle_strong(truck.veh, true)
	SET_VEHICLE_HAS_STRONG_AXLES(truck.veh, true)
	//truck_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam4_truck_vent", truck.veh, <<0.0, 1.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	
	trailer.veh = create_vehicle(trailer.model, <<1042.4807, -292.3749, 48.8504>>, 330.00)
	set_vehicle_tyres_can_burst(trailer.veh, false)
	attach_vehicle_to_trailer(truck.veh, trailer.veh)
	//DISABLE_TRAILER_BREAKING_FROM_VEHICLE(trailer.veh, true)

	set_current_selector_ped(selector_ped_trevor, false)
	add_ped_for_dialogue(scripted_speech, 2, player_ped_id(), "trevor")
	set_ped_into_vehicle(player_ped_id(), truck.veh)
	
	create_player_ped_inside_vehicle(selector_ped.pedID[selector_ped_michael], char_michael, truck.veh, vs_front_right, false)
	set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
	add_ped_for_dialogue(scripted_speech, 0, selector_ped.pedID[selector_ped_michael], "michael")

	lazlows_car.veh = create_vehicle(lazlows_car.model, <<1063.57, -284.17, 50.6>>, lazlows_car.heading)
	set_vehicle_strong(lazlows_car.veh, true)
	SET_VEHICLE_HAS_STRONG_AXLES(lazlows_car.veh, true)
	SET_VEHICLE_ENGINE_ON(lazlows_car.veh, TRUE, TRUE)
	set_vehicle_tyres_can_burst(lazlows_car.veh, false)
	set_entity_only_damaged_by_player(lazlows_car.veh, true)
	set_vehicle_colours(lazlows_car.veh, 27, 0)
	//SET_ENTITY_LOAD_COLLISION_FLAG(lazlows_car.veh, true)
	SET_VEHICLE_CAN_LEAK_OIL(lazlows_car.veh, false)
	SET_VEHICLE_CAN_LEAK_PETROL(lazlows_car.veh, false)
	lazlows_car_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fam4_truck_vent", lazlows_car.veh, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	
	create_npc_ped_inside_vehicle(lazlow.ped, char_lazlow, lazlows_car.veh, vs_driver, false)
	set_ped_can_ragdoll(lazlow.ped, false)
	SET_PED_COMBAT_ATTRIBUTES(lazlow.ped, ca_leave_vehicles, false)
	set_blocking_of_non_temporary_events(lazlow.ped, true)
	add_ped_for_dialogue(scripted_speech, 5, lazlow.ped, "lazlow")
	
	load_scene(get_entity_coords(player_ped_id()))

	mission_flow = play_final_mocap

	end_cutscene()

endproc 

proc family_4_load_stage_selector_assets()

	initialise_mission_variables()
		
	switch launch_mission_stage_menu_status
	
		case 0
	
			load_trip_skip_0()

		break 
		
		case 1
		
			load_trip_skip_1()

		break 
		
		case 2
		
			load_trip_skip_2()
		
		break 
		
		case 3
		
			load_trip_skip_3()

		break 
		
		case 4
		
			load_trip_skip_4()
		
		break 
		
		case 5
		
			load_trip_skip_5()
		
		break 
		
	endswitch

endproc 


proc family_4_play_final_mocap()

	switch play_final_mocap_status
	
		case 0
		
			request_mocap_data_for_Family_4_MCS_3_concat()
		
			//if has_cutscene_loaded()
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
				clear_mission_locate_stuff(locates_data, true) //cleans up locate data to fix assert where the locates was thinking the player was still trevor and therefore clearing michael who at the end of the mission was the player as clear_locates is called in mission_cleanup after the switch on exit state. Must clear it before the switch on exit state takes place

				if not is_screen_faded_in()
					do_screen_fade_in(default_fade_time) //if skipped previous cutscene.
				endif 
				
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(lazlows_car.veh)
				
				register_entity_for_cutscene(selector_ped.pedID[SELECTOR_PED_Michael], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				register_entity_for_cutscene(lazlow.ped, "Lazlow", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				register_entity_for_cutscene(lazlows_car.veh, "Lazlows_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				register_entity_for_cutscene(truck.veh, "family_4_truck", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)//CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				if does_entity_exist(trailer.veh)
					register_entity_for_cutscene(trailer.veh, "Family_4_trailer", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)//CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				else 
					register_entity_for_cutscene(null, "Family_4_trailer", CU_DONT_ANIMATE_ENTITY, trailer.model)
				endif 
				
				register_entity_for_cutscene(null, "Lazlow_Jeans", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, lazlows_jeans.model)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
				
				start_cutscene(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				play_final_mocap_status++
				
			endif 
		
		break 
		
		case 1
		
			if is_cutscene_playing()
			
				//stops cutscene cams
				destroy_all_cams()
				render_script_cams(false, false)
			
				clear_area(<<1059.5020, -291.9928, 49.4891>>, 1000, true)

				set_vehicle_doors_locked(lazlows_car.veh, vehiclelock_unlocked)
				SET_VEHICLE_UNDRIVEABLE(lazlows_car.veh, true)
				
				play_final_mocap_status++
			
			endif 
		
		break 
		
		case 2
		
			if is_cutscene_active()
				if not was_cutscene_skipped()
				
					printint(get_cutscene_time())
					printnl()
				
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow_Jeans", lazlows_jeans.model))
						lazlows_jeans.obj = GET_object_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow_Jeans", lazlows_jeans.model))
					ENDIF
					
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor))

					endif 
					
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
					
						make_selector_ped_selection(selector_ped, selector_ped_michael)
						take_control_of_selector_ped(selector_ped, true, false)
						force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
						SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
						force_ped_ai_and_animation_update(player_ped_id())

					endif 
					
					if get_current_player_ped_enum() = char_trevor
						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(selector_ped.pedID[selector_ped_michael])
					endif 
						
					
					if CAN_SET_EXIT_STATE_FOR_CAMERA()
						//added for seamless blend beacuse player_ped_id() is trevor on camera exit state and camera is blending to michael
						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(selector_ped.pedID[selector_ped_michael])
						set_gameplay_cam_relative_heading(0)
						set_gameplay_cam_relative_pitch(0)
					endif

				else 
				
					SET_CUTSCENE_FADE_VALUES(false, false, true)
					
					play_final_mocap_status++
				
				endif 
				
			else 
			
				if does_entity_exist(lazlow.ped)
					delete_ped(lazlow.ped)
				endif 
				
				if does_entity_exist(lazlows_jeans.obj)
					set_object_as_no_longer_needed(lazlows_jeans.obj)
				endif 
				
				if does_entity_exist(lazlows_car.veh)
					set_vehicle_as_no_longer_needed(lazlows_car.veh)
				endif 
				
				if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
					delete_ped(selector_ped.pedID[selector_ped_trevor])
				endif 
				
				if does_entity_exist(trailer.veh)
					delete_vehicle(trailer.veh)
				endif 
				
				if does_entity_exist(truck.veh)
					delete_vehicle(truck.veh)
				endif 

				deactivate_vehicle_proofs(truck.veh)
			
				end_cutscene_no_fade(false, false)
				
				REPLAY_STOP_EVENT()
				
				mission_passed()
				
			endif 
		
		break 
		
		case 3
		
			if is_cutscene_active()
			
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow_Jeans", lazlows_jeans.model))
					lazlows_jeans.obj = GET_object_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lazlow_Jeans", lazlows_jeans.model))
				ENDIF

			else 
			
				make_selector_ped_selection(selector_ped, selector_ped_michael)
				take_control_of_selector_ped(selector_ped, false)
			
				if does_entity_exist(lazlow.ped)
					delete_ped(lazlow.ped)
				endif 
				
				if does_entity_exist(lazlows_jeans.obj)
					set_object_as_no_longer_needed(lazlows_jeans.obj)
				endif 
				
				if does_entity_exist(lazlows_car.veh)
					set_vehicle_as_no_longer_needed(lazlows_car.veh)
					//delete_vehicle(lazlows_car.veh)
				endif 
				
				if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
					delete_ped(selector_ped.pedID[selector_ped_trevor])
				endif 
				
				if does_entity_exist(truck.veh)
					delete_vehicle(truck.veh)
				endif 
				
				if does_entity_exist(trailer.veh)
					delete_vehicle(trailer.veh)
				endif 
				
				deactivate_vehicle_proofs(truck.veh)
			
				end_cutscene()
				
				REPLAY_STOP_EVENT()
				
				mission_passed()
			
			endif 
		
		break 
		
	endswitch 

endproc 

proc family_4_mission_failed_stage()

	groupie_ai_system(false)
	
	ambient_ped_system()//forces the peds to flee is antagonised and harmed.

	if lk_timer(fail_time, 3000)
				
		if GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		
			//fixes issue where trevor and michael can meet each other in the game before the mission has been 
			//triggered - bug 1488490
			set_replay_rejected_character(CHAR_TREVOR)
			
			switch cached_mission_flow 
				
				case run_after_lazlow
				case uber_chase
			
					mission_flow_set_fail_warp_location(<<-868.9190, 199.3321, 72.3914>>, 242.1369)
					
				break 
				
			endswitch
			
			//If we've failed for killing Trevor, respawn as him at the hospital.
			if are_strings_equal(mission_failed_text, "family4_fail_1")
				mission_flow_set_fail_warp_location(<<-452.5382, -346.4354, 33.3646>>, 128.5391)
			ENDIF
			
			mission_failed()
			
		endif 
		
	endif
	
endproc 

proc stat_system()

	if is_ped_sitting_in_any_vehicle(player_ped_id())
		inform_mission_stats_of_damage_watch_entity(get_vehicle_ped_is_in(player_ped_id()), fam4_car_damage)
		if does_entity_exist(truck.veh)
		and is_ped_sitting_in_vehicle(player_ped_id(), truck.veh)
			inform_mission_stats_of_speed_watch_entity(get_vehicle_ped_is_in(player_ped_id()))
		else
			inform_mission_stats_of_speed_watch_entity(get_vehicle_ped_is_in(player_ped_id()), fam4_max_speed)
		endif
	else
		inform_mission_stats_of_damage_watch_entity(null, fam4_car_damage) 
		inform_mission_stats_of_speed_watch_entity(null)
	endif
	
endproc 

SCRIPT
	
	set_mission_flag(true)
	
	if has_force_cleanup_occurred()
		Mission_Flow_Mission_Force_Cleanup()
		mission_cleanup()
	endif
	
	mission_setup()
	
	while true

		wait(0)
			
		if not stop_mission_fail_checks
			if mission_fail_checks()
				setup_mission_fail()
			endif 
		endif
		
		skip_system()
		
		stat_system()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FameOrShame")

		switch mission_flow 

			case intro_mocap 

				family_4_intro_mocap()
				
			break 
			
			case trevor_leadin
			
				family_4_trevor_leadin()

			break 
			
			case switching_into_michael
			
				family_4_switching_into_michael()

			break
			
			case get_to_the_stadium
			
				family_4_get_to_the_stadium()
				
			break 
			
			case play_stadium_mocap
			
				family_4_play_stadium_mocap()
				
			break  
			
			case run_after_lazlow
			
				family_4_run_after_lazlo()
				
			break
			
			case uber_chase
			
				family_4_uber_chase()
		
			break 
			
			case storm_drain_cutscene
			
				family_4_storm_drain_cutscene()

			break 
			
			case play_final_mocap

				family_4_play_final_mocap()
			
			break 
			
			case train_crash_cutscene_test_bed
			
				truck_and_trailer_recording_system()
				//detach_trailer_cutscene_system()
				//storm_drain_cutscene()
				
			break 
			
			case detach_trailer_cutscene_test_bed
			
				family_4_deatch_trailer_cutscene_test_bed()

			break 
			
			case record_setpiece_cars
			
				family_4_record_lazlow()

			break 
			
			case load_stage_selector_assets
			
				family_4_load_stage_selector_assets()
			
			break 
			
			case mission_failed_stage
				
				family_4_mission_failed_stage()

			break 
				
		endswitch 
		
		
		#IF IS_DEBUG_BUILD
		
			is_entity_in_angled_area(player_ped_id(), <<-817.510, 151.703, 64.606>>, <<-815.789, 192.767, 77.706>>, 61.400)   
						
			//put vehicle ouside michaels front door
			//area round michaels house covering kitchen and driveway.
			is_entity_in_angled_area(player_ped_id(), <<-812.943, 167.079, 69.294>>, <<-811.455, 192.135, 76.794>>, 37.000) 
			
			is_entity_in_angled_area(player_ped_id(), <<-860.221, 150.219, 62.021>>, <<-858.733, 175.275, 72.921>>, 31.500)		
			//is_entity_in_angled_area(player_ped_id(), <<-248.824, -2021.740, 28.944>>, <<-258.934, -2033.879, 31.958>>, 3.3)
			
			//is_entity_in_angled_area(player_ped_id(), <<-231.798, -2041.398, 26.361>>, <<-260.114, -2021.864, 34.161>>, 39.2)
			
			//is_entity_in_angled_area(player_ped_id(), <<-252.133, -2011.385, 29.146>>, <<-269.892, -2032.251, 32.146 >>, 16.50)

		
//			if does_entity_exist(selector_ped.pedID[selector_ped_michael])
//				if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
//					
//					printvector(get_entity_coords(selector_ped.pedID[selector_ped_michael]))
//					printnl()
//					
//				else 
//					printstring("dead")
//					printnl()
//				endif 
//			else 
//				printstring("not exists")
//				printnl()
//			endif 
			
			add_scenario_blocking_area_widget()
			
			vehicle_force_system()
			angled_area_locate_widget()
			//get_current_player_vehicle(temp_lazlows_car)
			vehicle_colour_widget(temp_lazlows_car)
			vehicle_colour_widget(get_players_last_vehicle())
			
			//attach_entity_to_entity_widget(video_camera.obj, camera_crew[0].ped, GET_PED_BONE_INDEX(camera_crew[0].ped, BONETAG_PH_R_HAND))

			camera_attached_to_entity_widget(truck.veh)
					
			if is_keyboard_key_just_pressed(key_s)
				mission_passed()
			endif
		
			if not stop_mission_fail_checks
				if is_keyboard_key_just_pressed(key_f)
					mission_failed()
				endif
			endif 
			
			if is_keyboard_key_just_pressed(key_t)
				play_reaction_anim(ambient_ped[1].ped, "right")
				play_reaction_anim(ambient_ped[7].ped, "back_right")
				play_reaction_anim(ambient_ped[18].ped, "right")
				play_reaction_anim(ambient_ped[19].ped, "forward")
				//ambient_ped_status[1] = play_reaction_anim_during_cutscene
				//PLAY_ANIM_ON_RUNNING_SCENARIO(ambient_ped[1].ped, "amb@prop_human_seat_chair@female@proper@react_shock", "right")
			endif 

		#endif 

	endwhile 
	
endscript 



//rodeo drive -708.6627, -203.6715, 36.1175

//-841.5194, 7.6417, 41.8163

//petrol station: -336.5343, -1438.4293, 29.2102


//main train force offsets.
//target pos = 528.176, -1208.727, 30.000
//vehicle pos =
//vehicle rot = 
//vehicle force offset = 1.0, 3.0, 0.0
//angular force offset = 0.0, 0.0, 0.8
//force multiplier = 0.6 / 0.5

//liquid tanker 1
//497.0.61, -1206.327, 29.139
//283.2
//528.476, -1216.327, 30.00
//1.0, 3.0, 0.0
//0.0, 0.5, 0.6
//0.3

//liquid tanker 2
//497.61, -1206.327, 29.139
//283.2
//528.476, -1229.127, 29.3
//1.0, 3.0, 0.0
//0.0, 0.0, 0.5
//0.1

//liquid tanker 3
//497.61, -1206.327, 29.139
//283.2


//trailer force data
//505.642, -1200.900, 31.100
//359.836
//518.499, -1182.984, 31.012
//0.0, -3.0, -1.0
//0.0, 0.0, 1.0
//0.6

//3818

//freeway physics 1804


//ambulance = << 1386.6036, -2071.0393, 50.9988 >>   heading: 41.5869  
//
//
//tow truck = << 1379.0072, -2078.6235, 51.0031 >>   heading: 39.5863   


//possible model swaps
//cogsport
//schafter2

//100947 - joe trailer bug


//784631  - set_ped_model_is_suppressed(lazlow.model, true) not working 



//car recording number to re-use
//25
//44
//67
//68
//78
//79


//-scriptprofiling
//SET_PROFILING_OF_THIS_SCRIPT(TRUE)
//get_entity_offset_given_world_coords()

//GET_ENTITY_OFFSET_IN_WORLD_COORDS






//set_vehicle_automatically_attaches(players car false





//				enum block_colour_enum
//											red
//											white
//											blue
//											light_blue
//											black
//											silver
//											green
//											yellow




//re-use recording 102

//re - recording part of the uber car chase
//DO_RE_RECORDING_WIDGET
//if start uber recording flag was true
// then hack that script so it starts the recording. 
//hope there is not a frame lag. 
//then when you press the accelerator button it will take control of that recording. 


//ben lyons change list shelved for handling.bat 335770 

//985824


//991531 - carrec bat files takes too long to compile.

//996633 - recording bug wheels through the ground.

//1037983 - ptfx for truck 

//1043671 - ptfx for lazlows car
//839466 

//scenario ped clear area - 1053663

//guard shooting and not wanted level - 1143890

//1348901 - trauck and trailer constraint in pass cutscene.



//IF sData.sBrowseInfo.iCurrentItem >= 0
//AND sData.sBrowseInfo.iCurrentItem < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
//SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID, sData.sBrowseInfo.iCurrentItem)
//
//
//
//NATIVE PROC SET_VEHICLE_NUMBER_PLATE_TEXT(VEHICLE_INDEX VehicleIndex, STRING licencePlateTxt_Max8Chars)


//P_Laz_J02_S = jeans



//******RESPOTIONING THE PLAYERS CAR AT THE START OF THE MISSION AND CREATING A SUITABLE REPLACEMENT 
//******IF VEHICLE NOT ACCEPTABLE


//PURPOSE: repositions the players last vehicle if it exists via calling GET_MISSION_START_VEHICLE_INDEX()
//		   and sets the vehicle as a mission entity so that clear area will not clear it. Remember to 

//proc set_players_last_vehicle_to_vehicle_gen(vector car_pos, float car_heading)
//
//	vehicle_index players_last_car 
//	
//	players_last_car = GET_MISSION_START_VEHICLE_INDEX()
//	if does_entity_exist(players_last_car)
//		if is_vehicle_driveable(players_last_car)
//		
//			SET_ENTITY_AS_MISSION_ENTITY(players_last_car, true, true)
//			
//			clear_area(car_pos, 5.0, true)
//			set_entity_coords(players_last_car, car_pos)
//			set_entity_heading(players_last_car, car_heading)
//			set_vehicle_on_ground_properly(players_last_car)
//			set_entity_as_mission_entity(players_last_car)
//			
//		endif 
//	endif 
//	
//endproc 
//
//
//RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1980.730, 3073.228, 46.049>>, <<2014.615, 3051.972, 50.049>>, 26.700, <<2000.1245, 3059.5662, 46.0491>>, 59.1994, <<15.0, 15.0, 15.0>>, false)//, false) //2.5, 5.5, 2.0
//set_players_last_vehicle_to_vehicle_gen(<<2000.1245, 3059.5662, 46.0491>>, 59.1994)
//
//
//func bool is_players_last_vehicle_present_and_acceptable(vehicle_index &player_vehicle, vector pos, int number_of_seats, vector secondary_pos, float secondary_heading)
//	
//	if does_entity_exist(player_vehicle)
//
//		if is_vehicle_driveable(player_vehicle)
//			
//			if is_entity_at_coord(player_vehicle, pos, <<10.0, 10.0, 10.0>>, false)
//
//				printint(get_vehicle_max_number_of_passengers(player_vehicle))
//				printnl()
//
//				if get_vehicle_max_number_of_passengers(player_vehicle) >= number_of_seats
//
//					return true 
//					
//				else 
//			
//					clear_area(secondary_pos, 10.0, true)
//					set_entity_coords(player_vehicle, secondary_pos)
//					set_entity_heading(player_vehicle, secondary_heading)
//					set_vehicle_on_ground_properly(player_vehicle)
//					set_vehicle_as_no_longer_needed(player_vehicle)
//
//				endif
//				
//			endif 
//		endif 
//	endif 
//	
//	return false
//	
//endfunc
//
//
////PURPOSE: sets up a vehicle for the player. Either the vehicle they arrived in if suitable or trevors truck.
////Call this every frame when the mocap is running
//proc setup_vehicle_outside_methlab()
//	
//	if not does_entity_exist(trevors_truck.veh)
//		
//		if has_model_loaded(trevors_truck.model)
//			
//			//trevors_truck.veh = get_players_last_vehicle()
//			trevors_truck.veh = GET_MISSION_START_VEHICLE_INDEX()
//
//			if not is_players_last_vehicle_present_and_acceptable(trevors_truck.veh, <<2000.1245, 3059.5662, 46.0491>>, 2, <<2008.9219, 3054.0513, 46.0528>>, 326.3415)
//				
//				CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, <<2000.1245, 3059.5662, 46.0491>>, 59.1994, false)
//
//			endif 
//			
//		endif
//	endif 
//	
//endproc 
//
//
//set_trevors_truck as no longer needed in the setup of the next stage / no is_cutscene_active

//1316247 - game locks up at the start.


//1340973 - Family_4_MCS_3_concat truck and trailer issues

//1341601 - frame lag when switching to michael from trevor

//1355921 - trailer driving in pass cutscene

//1374049 - out of streaming memory when skipping family 4 intro


//
//female 
//
//arms_folded
//
//legs_crossed
//
//proper
//
//proper_skinny
//
//
//
//male 
//
//elbows_on_knees
//
//generic_skinny
//
//left_elbow_on_knee








