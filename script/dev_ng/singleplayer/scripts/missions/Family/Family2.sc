//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:	Paul Davies						Date: 15/09/2010		│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│																				│
//│									FAMILY 2				 					│
//│																				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "script_blips.sch"
USING "select_mission_stage.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "chase_hint_cam.sch"
USING "commands_interiors.sch"
USING "locates_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "CompletionPercentage_public.sch"
USING "vehicle_gen_public.sch"
USING "clearmissionarea.sch"
USING "timeLapse.sch"
USING "flow_public_game.sch"
USING "buddy_head_track_public.sch"
USING "player_ped_scenes.sch"
USING "commands_recording.sch"
USING "taxi_functions.sch"
USING "push_in_public.sch"

//╞═══════════╡ENUMS   	╞═══════════════════════════════════════════════════════╡

CONST_INT NUM_OF_SELECTABLE_STAGES 11

CONST_FLOAT WARP_REFRESH_DISTANCE 400.0 //20
CONST_FLOAT WARP_ALLOWED_DISTANCE 225.0 //15
CONST_FLOAT MIN_DIST_BETWEEN_WARP_POINTS 100.0 // 10
CONST_FLOAT MIN_DIST_PLAYER_TO_WARP 225.0 // 15
CONST_INT MIN_TIME_OCCLUDED 1000
CONST_INT MIN_TIME_BETWEEN_WARPS 2000

ENUM MISSION_STAGE_FLAG
	stage_pre_intro = -1,
	stage_intro = 0,
	stage_drive_to_venice,
	stage_hire_bike,
	stage_bike_ride,
	stage_telltale,
	stage_go_to_yacht,
	stage_rescue,
	stage_speedophile_chase,
	stage_drive_to_beach,
	stage_finish, 
	go_to_end,
	stage_lose_cops,
	stage_test
ENDENUM

MISSION_STAGE_FLAG current_mission_stage = stage_pre_intro
MISSION_STAGE_FLAG mission_stages[NUM_OF_SELECTABLE_STAGES]
MISSION_STAGE_FLAG eSavedStage

ENUM CREATION_STATE
	ON_FOOT,
	IN_BIKE,
	IN_CAR,
	IN_Seashark
ENDENUM

ENUM VEH_TYPE
	BIKE = 0,
	JIMMY_BIKE,
	CAR,
	SEA_SHARK
ENDENUM

ENUM FAM2_CHASE_SECTION_ENUM
	FAM2_CHASE_OPEN_SEA = 0,
	FAM2_CHASE_TUNNELS,
	FAM2_CHASE_CANALS,
	FAM2_CHASE_CANALS_CATCH_UP,
	FAM2_CHASE_CANALS_SINGLE_CHASER,
	FAM2_CHASE_MARINA,
	FAM2_CHASE_LOSE_PEDS,
	FAM2_CHASE_SEEK_AND_DESTROY,
	FAM2_CHASE_INVALID
ENDENUM

FAM2_CHASE_SECTION_ENUM eCurrentChaseSection = FAM2_CHASE_OPEN_SEA
FAM2_CHASE_SECTION_ENUM ePreviousChaseSection = FAM2_CHASE_OPEN_SEA
FAM2_CHASE_SECTION_ENUM eHighestSection = FAM2_CHASE_OPEN_SEA
INT iSectionsHit

ENUM CANAL_SECTION
	FAM2_CS_BOTTOM = 0,
	FAM2_CS_TOP,
	FAM2_CS_LEFT,
	FAM2_CS_MIDDLE,
	FAM2_CS_RIGHT,
	FAM2_CS_NUMBER_OF_SECTIONS,
	FAM2_CS_INVALID
ENDENUM

CANAL_SECTION eCurrentCanalSection = FAM2_CS_BOTTOM
CANAL_SECTION ePreviousCanalSection = FAM2_CS_INVALID

#IF IS_DEBUG_BUILD
	INT iStageHolder
	MissionStageMenuTextStruct skipMenuOptions[NUM_OF_SELECTABLE_STAGES]
	MISSION_STAGE_FLAG selected_stage
	
	WIDGET_GROUP_ID parent_widget
	WIDGET_GROUP_ID chase_widget
	WIDGET_GROUP_ID wave_widget
	TEXT_WIDGET_ID twChaseSection
	TEXT_WIDGET_ID twCanalSection
	TEXT_WIDGET_ID twPrevCanalSection
	
	FLOAT 	fRecordingTime
	FLOAT 	fSectionTime
	FLOAT 	fSectionProgressTime
	FLOAT 	fSectionProgress
	INT 	iClosestPoint
	
	INT iBreak
	BOOL bRun
	
	BOOL bDebugMenuCutsceneSelection = FALSE
#ENDIF

REL_GROUP_HASH rgChasers

structTimelapse sTimelapse

//╞═════════╡CONSTANTS 	╞═══════════════════════════════════════════════════════╡

CONST_FLOAT NUMBER_HEIGHT 2.0
CONST_FLOAT NUMBER_RATIO 0.4

CONST_FLOAT VARIABLE_PLAYBACK_DISTANCE 25.0
CONST_FLOAT SINCOS45 0.707106

CONST_INT MAX_BIKE_NODES 100

//╞═════════╡INDECES  	╞═══════════════════════════════════════════════════════╡

PED_INDEX pedTracy
PED_INDEX pedJimmy
PED_INDEX pedBikeGuy
PED_INDEX pedBoatDancer[4]

VEHICLE_INDEX vehPlayerSeashark
VEHICLE_INDEX vehPlayerBike
VEHICLE_INDEX vehJimmyBike
VEHICLE_INDEX vehPlayerCar
VEHICLE_INDEX vehBikes[5]
VEHICLE_INDEX vehTraceyCar
VEHICLE_INDEX vehMikesCar

VEHICLE_INDEX vehChasers[2]
PED_INDEX pedChasers[2]
BLIP_INDEX blipChasers[2]

BLIP_INDEX blipDest
BLIP_INDEX blipPornBoat
BLIP_INDEX blipBuddy
BLIP_INDEX blipPlayerVeh

OBJECT_INDEX objMP3Dock
OBJECT_INDEX objFitTest
OBJECT_INDEX objChair
OBJECT_INDEX objDoorL
OBJECT_INDEX objDoorR

CAMERA_INDEX camIGIntro
CAMERA_INDEX camRescueOutro

INTERIOR_INSTANCE_INDEX intMikeMansion

REL_GROUP_HASH rghBuds

//╞═════════╡VARIABLES 	╞═══════════════════════════════════════════════════════╡

//BOOL bChaserComponentsGrabbed
//INT iChaserComponents[NUMBER_OF_PED_COMP_TYPES][2]

BOOL bWaterComponentsGrabbed
INT iWaterComponents[NUMBER_OF_PED_COMP_TYPES][2]

BOOL bMovingStart = FALSE

VECTOR v_jimmy_end_pos = <<-1614.5256, -1137.8818, 0.9178>>
VECTOR vFinalDest =  <<-1626.04578, -1137.16504, 0.47598>>

VECTOR vPlayerStartCoords = <<-805.4391, 175.6440, 75.7407>>
FLOAT fPlayerHeading = 336.1657

VECTOR vJimmyCoords =  <<-804.3638, 178.5257, 75.7407>>
FLOAT fJimmyHeading = 14.3431

VECTOR vPlayerOnPierPosition = <<-1862.5293, -1211.2534, 12.0170>>
FLOAT fPlayerOnPierHeading = 45.6064


VECTOR v_car_start_coords = <<-823.2707, 181.5663, 70.6967>>
FLOAT f_car_start = 136.7262

VECTOR v_Seashark_start_coords = <<-2019.921143,-1030.525269, 0.001>>

VECTOR vSeasharkChaseStartCoords = <<-2008.7224, -1033.1538, 0>>
FLOAT fSeasharkChaseStartCoords = 280.6858

VECTOR v_car_stop = <<-1072.7997, -1703.9238, 3.5140>>
FLOAT f_car_stop = 181.9476
VECTOR v_bike_guy_pos = <<-1105.9325, -1695.8932, 3.3731>>

VECTOR v_player_bike_start = <<-1106.255493,-1694.976929, 3.3742>>
FLOAT f_player_bike_start = 122.547714

//telltale cutscene marks and headings
VECTOR v_TT_player_bike_mark = <<-1866.950806,-1208.529297,12.630510>> 
VECTOR v_TT_jimmy_bike_mark = <<-1865.153076,-1210.693726,12.606791>>
FLOAT f_TT_player_bike_mark =  54.539898
FLOAT f_TT_jimmy_bike_mark = 65.012039

VECTOR vBeachPos1 = <<-1627.997803,-1143.906006,-0.612126>>
VECTOR vBeachPos2 = <<-1608.709839,-1116.124878,7.893201>> 
FLOAT fBeachWidth = 45.000000

VECTOR vTheAbsoluteEndPos1 = <<-1084.215820,-1707.051636,-6.638385>>
VECTOR vTheAbsoluteEndPos2 = <<-967.234436,-1829.366333,40.314407>> 
FLOAT fTheAbsoluteEndWidth = 116.000000

VECTOR vRaceFinish = <<-1851.92004, -1217.77734, 12.01752>>

FLOAT f_bike_guy_heading = 332.3918
FLOAT f_jimmy_end_heading = 113.3809
FLOAT f_Seashark_heading = -107.750221
FLOAT fJazzTime

FLOAT fPlayerGroundTime
BOOL bPlayerGroundingChecked
BOOL bChaserGroundingChecked
BOOL bShowStaminaHelp = TRUE
BOOL bReplaySetupRequired

STRING s_jimmy_bike_rec = "fam2"
STRING sJimmyStairRoute = "fam2stairs"

INT i_jimmy_bike_rec = 2
INT iStageProgress = 0
INT iBikeTutStage = 0
INT iCurrentDriveConv
INT iLastConvFinish
INT iSavedStage = 0
INT iCurrentJimmyTarget = 4
INT iJimmyChoice = 4
INT iMikeChoice = 1
INT iTimeGetBackOnPrinted = 0
INT iLastJimmyShout = -1
INT iLastMeleeShout = -1
INT iJimmyWalkStage = -1
INT iRunTime = 900

BOOL b_stage_ready = FALSE
BOOL b_skip = FALSE
BOOL bPlayFullFinalCut = TRUE
//BOOL bClothesHaveChanged = FALSE
BOOL bBlipParkingSpace = FALSE
BOOL bGoBackToJimmyPrinted = FALSE
BOOL bGoBackToTracyPrinted = FALSE
BOOL b_cutscene_request = FALSE
BOOL bCutsceneComponentsSet
BOOL bJimmyWon
BOOL bJimmyFinished
BOOL bShowGodText
BOOL bMoreSeatsPrinted

INT iCurrentConvStage = 0
BOOL bPlayerDone
INT iTimeOfLastMichaelLine = 0

INT iTimeLastBumpCheck
INT iTimeLastBumpLine
INT iTimeLastCheatLine

INT iChaseStage

BOOL bKilledOne
BOOL bKillLineDone
BOOL bKilledBoth
BOOL bShooting
BOOL bShootLineDone

BOOL bShouldFireMikeTakeoverLine
BOOL bShouldFireJimmyTakeoverLine
BOOL bJimmyPierLine
BOOL bPlayerHasCheated
BOOL bPlayerHasBumpedJimmy

STRING sFinalCut = "family_2_mcs_4"
STRING sConvBlock = "FAM2AUD"

BOOL bGoBackPrinted = FALSE
INT iTimeGoBackPrinted = 0

// for final bit on the pier
INT iFinalScene
CAMERA_INDEX camFinal
OBJECT_INDEX objCig
VEHICLE_INDEX vehEndBike

VECTOR v_back_of_the_boat 	= <<-2030.40906, -1039.46301, -23.59287>>
VECTOR vOutlet				= <<-1797.8530, -982.6022, 0>>
VECTOR vMarinaFinish		= << -934.5598, -1360.5444, 0 >>
FLOAT fMarinaFinish			= 288.2051

VECTOR vBikeRoutePath[MAX_BIKE_NODES]

VECTOR vLeftDoorPos = <<-816.7160, 179.0980, 72.8274>>
VECTOR vRightDoorPos = <<-816.1068, 177.5108, 72.8274>>

VECTOR vReplayCoords
FLOAT fReplayHeading

BOOL bTraceySpotsJimmy
BOOL bMikeCompAcquired = FALSE
SEQUENCE_INDEX seqTurnTask
PUSH_IN_DATA pushInData

PROC FADE_IN(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_IN()
		DO_SCREEN_FADE_IN(iTime)
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC FADE_OUT(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(iTime)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

SCENARIO_BLOCKING_INDEX sbiBikeArea
BOOL bBlockAreaBike

SCENARIO_BLOCKING_INDEX sbiMarina
SCENARIO_BLOCKING_INDEX sbiBeach

//---------------------------------------------------- God text stuff

STRUCT TEXT_HOLDER
	STRING label
	BOOL played
ENDSTRUCT

FUNC TEXT_HOLDER CREATE_TEXT_HOLDER(STRING text)
	TEXT_HOLDER returnGT
	returnGT.label = text
	returnGT.played = FALSE
	return returnGT
ENDFUNC

TEXT_HOLDER gtFollowJimmy				
TEXT_HOLDER gtGetBackInBike	
TEXT_HOLDER gtGetBackInSeashark
TEXT_HOLDER gtLoseCops
//TEXT_HOLDER dhSeashark

BOOL bBeenOnBike = FALSE

PROC PRINT_GOD_TEXT(TEXT_HOLDER &gt)
	IF NOT gt.played
		IF NOT IS_STRING_NULL(gt.label)
			PRINT_NOW(gt.label, DEFAULT_GOD_TEXT_TIME, 1)
			gt.played = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PRINT_HELP_TEXT(TEXT_HOLDER &help_text)
	IF NOT help_text.played
		IF NOT IS_STRING_NULL(help_text.label)
			PRINT_HELP(help_text.label)
			help_text.played = TRUE
		ENDIF
	ENDIF
ENDPROC

STRING subGroupID = sConvBlock
structPedsForConversation sSpeech

PROC PLAY_DIALOGUE(TEXT_HOLDER &dialogue)
	IF NOT dialogue.played
		CREATE_CONVERSATION(sSpeech, sConvBlock, dialogue.label, CONV_PRIORITY_VERY_HIGH)
		dialogue.played = TRUE
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INIT_TEXT()
	gtFollowJimmy		= CREATE_TEXT_HOLDER("FM2_GOTOL")
	gtGetBackInBike		= CREATE_TEXT_HOLDER("FM2_GETON")
	gtGetBackInSeashark	= CREATE_TEXT_HOLDER("FM2_BKIN")
	gtLoseCops			= CREATE_TEXT_HOLDER("FM2_COPS")
//	dhSeashark 			= CREATE_TEXT_HOLDER("FAM2_JSKI2")
ENDPROC

BOOL in_car_conv_started = FALSE
BOOL in_car_conv_on_hold = FALSE
TEXT_LABEL_23 sConvResumeLabel = ""
//
//PROC MANAGE_VEHICLE_CONVERSATION(PED_INDEX ped, STRING label, VEHICLE_INDEX veh)
//	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
//	AND NOT IS_PED_INJURED(ped)
//	AND IS_PED_IN_VEHICLE(ped, veh)
//		IF NOT in_car_conv_started
//			CREATE_CONVERSATION(sSpeech, subGroupID, label, CONV_PRIORITY_HIGH)
//			println("in car conv started")
//			in_car_conv_on_hold = FALSE
//			in_car_conv_started = TRUE
//		ENDIF
//		IF in_car_conv_on_hold
//			println("in car conv resumed")
//			CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, subGroupID, label, sConvResumeLabel, CONV_PRIORITY_HIGH)
//			in_car_conv_on_hold = FALSE
//		ENDIF
//	ENDIF
//	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
//	OR (NOT IS_PED_INJURED(ped) AND NOT IS_PED_IN_VEHICLE(ped, veh))
//		IF NOT in_car_conv_on_hold
//			sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
//			KILL_FACE_TO_FACE_CONVERSATION()
//			println("in car conv on hold with label ", sConvResumeLabel)
//			in_car_conv_on_hold = TRUE
//		ENDIF
//	ENDIF
//ENDPROC
//
//PROC MANAGE_DISTANCE_CONVERSATION(STRING label, FLOAT fMinDist, VECTOR vTalkerPos)
//	IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTalkerPos) < fMinDist
//		IF NOT in_car_conv_started
//			IF CREATE_CONVERSATION(sSpeech, subGroupID, label, CONV_PRIORITY_HIGH)
//				in_car_conv_on_hold = FALSE
//				in_car_conv_started = TRUE
//			ENDIF
//		ENDIF
//		IF in_car_conv_on_hold
//			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, subGroupID, label, sConvResumeLabel, CONV_PRIORITY_HIGH)
//				in_car_conv_on_hold = FALSE
//			ENDIF
//		ENDIF
//	ENDIF
//	IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTalkerPos) > fMinDist
//		IF NOT in_car_conv_on_hold
//			sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
//			KILL_FACE_TO_FACE_CONVERSATION()
//			in_car_conv_on_hold = TRUE
//		ENDIF
//	ENDIF
//ENDPROC

PROC MANAGE_CONVERSATION(STRING label, BOOL bCanTalk)
	IF bCanTalk
		IF NOT in_car_conv_started
			IF CREATE_CONVERSATION(sSpeech, subGroupID, label, CONV_PRIORITY_HIGH)
				in_car_conv_on_hold = FALSE
				in_car_conv_started = TRUE
			ENDIF
		ENDIF
		IF in_car_conv_on_hold
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, subGroupID, label, sConvResumeLabel, CONV_PRIORITY_HIGH)
				in_car_conv_on_hold = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF NOT bCanTalk
		IF NOT in_car_conv_on_hold
			sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
			KILL_FACE_TO_FACE_CONVERSATION()
			in_car_conv_on_hold = TRUE
		ENDIF
	ENDIF
ENDPROC

BOOL bPlayerInFrontLastFrame
INT iTimeLastOvertake
INT iTimeLastJimmyLine

PROC MANAGE_JIMMY_BIKE_CONV(FLOAT fProgDiff, FLOAT fDistFromTrack, BOOL bPlayerInFront)

	fProgDiff = ABSF(fProgDiff)
	FLOAT fDistToJimmy = VDIST(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
	AND NOT IS_PED_INJURED(pedJimmy)
	
		SWITCH iCurrentConvStage
		
			CASE 0
				bShouldFireMikeTakeoverLine = FALSE
				bShouldFireJimmyTakeoverLine = FALSE
				bJimmyPierLine = FALSE
				bPlayerHasCheated = FALSE
				bPlayerHasBumpedJimmy = FALSE
				
				iTimeLastBumpCheck = 0
				iTimeLastBumpLine = 0
				iTimeLastCheatLine = 0
				
				IF bPlayerInFront
				AND fProgDiff > 15000
					bPlayerHasCheated = TRUE
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF fDistToJimmy < 35
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_PIER", CONV_PRIORITY_HIGH)
							SETTIMERA(0)
							iCurrentConvStage++
						ENDIF
					ELSE
						iCurrentConvStage++
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF TIMERA() > 2000
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
						SETTIMERA(0)
						CLEAR_PRINTS()
						PRINT_GOD_TEXT(gtFollowJimmy)
						iCurrentConvStage++
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF TIMERA()>1000
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF fDistToJimmy < 35
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_RACEM", CONV_PRIORITY_HIGH)
							iCurrentConvStage++
						ENDIF
					ENDIF
				ELSE
					IF TIMERA() > 15000
						iCurrentConvStage++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND fDistToJimmy < 35
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_TRIA", CONV_PRIORITY_HIGH)
						iCurrentConvStage++
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
			
				IF bPlayerInFront
					DRAW_DEBUG_TEXT_2D("Player in front", <<0.01, 0.68, 0>>)
				ELSE
					DRAW_DEBUG_TEXT_2D("Jimmy in front", <<0.01, 0.68, 0>>)
				ENDIF
				IF bShouldFireMikeTakeoverLine
					DRAW_DEBUG_TEXT_2D("Michael take over line should fire", <<0.01, 0.7, 0>>)
				ENDIF
				IF bShouldFireJimmyTakeoverLine
					DRAW_DEBUG_TEXT_2D("Jimmy take over line should fire", <<0.01, 0.72, 0>>)
				ENDIF
				IF bPlayerHasCheated
					DRAW_DEBUG_TEXT_2D("bPlayerHasCheated", <<0.01, 0.74, 0>>)
				ENDIF
				IF bPlayerHasBumpedJimmy
					DRAW_DEBUG_TEXT_2D("bPlayerHasBumpedJimmy", <<0.01, 0.76, 0>>)
				ENDIF
				
				IF bPlayerInFront <> bPlayerInFrontLastFrame
					iTimeLastOvertake = GET_GAME_TIMER()
					IF bPlayerInFront
						bShouldFireMikeTakeoverLine = TRUE
						bShouldFireJimmyTakeoverLine = FALSE
						iTimeOfLastMichaelLine = GET_GAME_TIMER() - 10000
					ELSE
						bShouldFireMikeTakeoverLine = FALSE
						bShouldFireJimmyTakeoverLine = TRUE
						iTimeOfLastMichaelLine = GET_GAME_TIMER() - 10000
					ENDIF
				ENDIF
				
				IF bShouldFireJimmyTakeoverLine
					IF (GET_GAME_TIMER() - iTimeLastOvertake) < 4000
					AND fDistToJimmy < 30
					AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_STUFF", CONV_PRIORITY_HIGH)
							bShouldFireJimmyTakeoverLine = FALSE
						ENDIF
					ELSE
						bShouldFireJimmyTakeoverLine = FALSE
					ENDIF
				ENDIF
				
				IF bShouldFireMikeTakeoverLine
					IF (GET_GAME_TIMER() - iTimeLastOvertake) < 4000
					AND fDistToJimmy < 30
					AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_PASS", CONV_PRIORITY_HIGH)
							bShouldFireMikeTakeoverLine = FALSE
						ENDIF
					ELSE
						bShouldFireMikeTakeoverLine = FALSE
					ENDIF
				ENDIF
				
				IF NOT bPlayerHasCheated
				AND NOT bPlayerHasBumpedJimmy
				
					IF bPlayerInFront
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF (GET_GAME_TIMER() - iTimeLastJimmyLine) > 10000
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
								AND fDistToJimmy < 35
									IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_ROFF", CONV_PRIORITY_HIGH)
										iTimeLastJimmyLine = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ELIF (GET_GAME_TIMER() - iTimeLastJimmyLine) < 5000
							AND (GET_GAME_TIMER() - iTimeOfLastMichaelLine) > 10000
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
								AND fDistToJimmy < 40
									IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_FRONT", CONV_PRIORITY_HIGH)
										iTimeOfLastMichaelLine = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF (GET_GAME_TIMER() - iTimeLastJimmyLine) > 10000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							AND fDistToJimmy < 35
							AND NOT bPlayerInFront
								IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_CLOSE", CONV_PRIORITY_HIGH)
									iTimeLastJimmyLine = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ELIF (GET_GAME_TIMER() - iTimeLastJimmyLine) < 5000
						AND (GET_GAME_TIMER() - iTimeOfLastMichaelLine) > 10000
							PRINTLN("Can do FAM2_BEH")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							AND fDistToJimmy < 40
								IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_BEH", CONV_PRIORITY_HIGH)
									iTimeOfLastMichaelLine = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF fDistToJimmy < 5
					AND (GET_GAME_TIMER() - iTimeLastBumpCheck) > 1000
					AND (GET_GAME_TIMER() - iTimeLastBumpLine) > 10000
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
						AND IS_ENTITY_TOUCHING_ENTITY(vehPlayerBike, vehJimmyBike)
							bPlayerHasBumpedJimmy = TRUE
						ENDIF
						iTimeLastBumpCheck = GET_GAME_TIMER()
					ENDIF
					
					IF (GET_GAME_TIMER() - iTimeLastCheatLine) > 10000
						IF NOT bPlayerHasCheated
							IF fDistFromTrack > 50
								bPlayerHasCheated = TRUE
							ENDIF
						ENDIF
					ENDIF
					
				ELIF bPlayerHasCheated
				
					IF fDistToJimmy < 35
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_CHEAT", CONV_PRIORITY_HIGH)
							iTimeLastCheatLine = GET_GAME_TIMER()
							bPlayerHasCheated = FALSE
						ENDIF
					ENDIF
					
				ELIF bPlayerHasBumpedJimmy
				
					IF fDistToJimmy < 35
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_HIT", CONV_PRIORITY_HIGH)
							iTimeLastBumpLine = GET_GAME_TIMER()
							bPlayerHasBumpedJimmy = FALSE
						ENDIF
					ENDIF
					
				ENDIF
				
				IF NOT bJimmyPierLine
					IF IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-1545.975464,-937.182190,10.332039>>, <<-1556.067627,-928.880615,16.722149>>, 3.750000)
					AND fDistToJimmy < 35
						iCurrentConvStage++
						iTimeOfLastMichaelLine = GET_GAME_TIMER()
						bJimmyPierLine = TRUE
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 5
				IF fDistToJimmy < 35
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_JEND", CONV_PRIORITY_HIGH)
						iTimeOfLastMichaelLine = GET_GAME_TIMER()
						iCurrentConvStage++
					ENDIF
				ENDIF
				IF (GET_GAME_TIMER() - iTimeOfLastMichaelLine) > 5000
					iCurrentConvStage = 4
				ENDIF
			BREAK
			
			CASE 6
				IF fDistToJimmy < 35
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_ROFF", CONV_PRIORITY_HIGH)
						iCurrentConvStage = 4
					ENDIF
				ENDIF
				IF (GET_GAME_TIMER() - iTimeOfLastMichaelLine) > 5000
					iCurrentConvStage = 4
				ENDIF
			BREAK
			
		ENDSWITCH
		
		bPlayerInFrontLastFrame = bPlayerInFront
		
	ENDIF
	
ENDPROC

PROC DO_NO_STAMINA_HELP_TEXT
	IF bShowStaminaHelp
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF GET_PLAYER_SPRINT_TIME_REMAINING(PLAYER_ID()) <= 5.0
				IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_NO_STAMINA)
					PRINT_HELP("AM_H_NOSTAM")
					SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_NO_STAMINA)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//-------------------------------------------------------------------

PROC DO_QUICK_WARP(VECTOR vPos, FLOAT fHead, BOOL bWarpPlayer = TRUE)
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_FOCUS_POS_AND_VEL(vPos, <<0,0,0>>)
		NEW_LOAD_SCENE_START(vPos, <<COS(fHead+90), SIN(fHead+90), 0>>, 25)
		SETTIMERA(0)
		INT iTimeNow = GET_GAME_TIMER()
		WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND (GET_GAME_TIMER() - iTimeNow) < 12000
			WAIT(0)
		ENDWHILE
		IF bWarpPlayer
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fHead)
		ENDIF
		CLEAR_FOCUS()
		NEW_LOAD_SCENE_STOP()
	ENDIF
ENDPROC

INT iBikeBlockedTimer
//fails checks for a given vehicle
FUNC BOOL MISSION_VEHICLE_FAIL_CHECKS(VEHICLE_INDEX VEHICLE, BOOL &bStuck)
	IF DOES_ENTITY_EXIST(VEHICLE)
		IF IS_VEHICLE_DRIVEABLE(VEHICLE)
//		 	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), VEHICLE)
				IF IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_ROOF, ROOF_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_JAMMED, JAMMED_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_SIDE, SIDE_TIME)
					IF IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_ROOF, ROOF_TIME)
						PRINTLN("@@@@@@@@ VEH_STUCK_ON_ROOF @@@@@@@@")
					ENDIF
					IF IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_JAMMED, JAMMED_TIME)
						PRINTLN("@@@@@@@@ VEH_STUCK_JAMMED @@@@@@@@")
					ENDIF
					IF IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
						PRINTLN("@@@@@@@@ VEH_STUCK_HUNG_UP @@@@@@@@")
					ENDIF
					IF IS_VEHICLE_STUCK_TIMER_UP(VEHICLE, VEH_STUCK_ON_SIDE, SIDE_TIME)
						PRINTLN("@@@@@@@@ VEH_STUCK_ON_SIDE @@@@@@@@")
					ENDIF
					bStuck = TRUE
					RETURN TRUE
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), VEHICLE, TRUE)
				AND NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(), VEHICLE, VS_DRIVER, TRUE)
					PRINTLN("@@@@@@@@ IS_ENTRY_POINT_FOR_SEAT_CLEAR | iBikeBlockedTimer: ", iBikeBlockedTimer, " | iBikeBlockedTimer more than 5000?: ", GET_GAME_TIMER() - iBikeBlockedTimer, " @@@@@@@@")
					IF iBikeBlockedTimer = 0
						iBikeBlockedTimer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - iBikeBlockedTimer >= 5000
						bStuck = TRUE
					RETURN TRUE
				ENDIF
				ELSE
					iBikeBlockedTimer = 0
			ENDIF
//			ENDIF
		ELSE
			RETURN TRUE
		ENDIF 
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BUDDY_ABANDONED(PED_INDEX buddy, FLOAT abandon_distance = 150.0)
	IF DOES_ENTITY_EXIST(buddy)
		IF IS_ENTITY_A_PED(buddy)
			IF NOT IS_PED_INJURED(buddy)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(buddy)) > abandon_distance
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC	

PROC PRE_STREAM_CUTSCENE_AT_DISTANCE(STRING scene, VECTOR location, FLOAT distance, BOOL &checkSwitch)
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), location) < distance
	AND NOT checkSwitch
		PRINTLN("REQUESTING SCENE, ", scene)
		REQUEST_CUTSCENE(scene)
		checkSwitch = TRUE
	ENDIF
ENDPROC

FUNC FLOAT SMART_CLAMP(FLOAT value, FLOAT limit1, FLOAT limit2)
	FLOAT min, max
	IF limit1 <= limit2
		min = limit1
		max = limit2
	ELSE
		min = limit2
		max = limit1
	ENDIF
	RETURN CLAMP(value, min, max)	
ENDFUNC

//-------------------------------------------------- Vector functions

FUNC VECTOR ROTATE_2D_VECTOR_ABOUT_ORIGIN(VECTOR v, FLOAT angle)
	VECTOR return_vector
	//rotate around z
	return_vector.x = (v.x * cos(angle)) - (v.y * sin(angle))
	return_vector.y = (v.x * sin(angle)) + (v.y * cos(angle))
	return_vector.z = v.z
	RETURN return_vector
ENDFUNC

FUNC FLOAT GET_VECTOR_MAGNITUDE(VECTOR vVector)
	RETURN SQRT((vVector.x * vVector.x) + (vVector.y * vVector.y) + (vVector.z * vVector.z))
ENDFUNC

FUNC VECTOR GET_NORMALISED_VERSION_OF_VECTOR(VECTOR vVector)
	FLOAT magnitude = GET_VECTOR_MAGNITUDE(vVector)
	IF vVector.x <> 0
		vVector.x = (vVector.x / magnitude)
	ENDIF
	IF vVector.y <> 0
		vVector.y = (vVector.y / magnitude)
	ENDIF
	IF vVector.z <> 0
		vVector.z = (vVector.z /magnitude)
	ENDIF
	RETURN vVector
ENDFUNC

FUNC FLOAT GET_ANGLE_BETWEEN_TWO_VECTORS(VECTOR vVectorA, VECTOR vVectorB)
	FLOAT return_angle
	vVectorA = NORMALISE_VECTOR(vVectorA)
	vVectorB = NORMALISE_VECTOR(vVectorB)
	return_angle = (ATAN2(vVectorB.y, vVectorB.x) - ATAN2(vVectorA.y, vVectorA.x))
	IF return_angle > 180
		return_angle = (return_angle - 360)
	ELIF return_angle < -180
		return_angle = (return_angle + 360)
	ENDIF
	RETURN return_angle
ENDFUNC

PROC SET_VECTOR_MAGNITUDE(VECTOR &v, FLOAT m)
	VECTOR n = NORMALISE_VECTOR(v)
	n.x = n.x * m
	n.y = n.y * m
	n.z = n.z * m
	v = n
ENDPROC

FUNC VECTOR GET_VECTOR_FROM_DIRECTION_AND_MAGNITUDE(VECTOR v, FLOAT m)
	VECTOR n = NORMALISE_VECTOR(v)
	n.x = n.x * m
	n.y = n.y * m
	n.z = n.z * m
	RETURN n
ENDFUNC

//-------------------------------------------------------------------

//-------------------------------------------------------- Line stuff

STRUCT LINESTRUCT
	VECTOR p1
	VECTOR p2
//	VECTOR middle
ENDSTRUCT

FUNC LINESTRUCT CREATE_LINE(VECTOR v1, VECTOR v2)
	LINESTRUCT line
	line.p1 = v1
	line.p2 = v2
//	line.middle.x = line.p1.x + ((line.p2.x - line.p1.x)/2)
//	line.middle.y = line.p1.y + ((line.p2.y - line.p1.y)/2)
//	line.middle.z = line.p1.z + ((line.p2.z - line.p1.z)/2)
	RETURN line
ENDFUNC

FUNC BOOL IS_POINT_BEHIND_LINE(VECTOR O, LINESTRUCT &line)

	FLOAT test_val
	test_val = (line.p2.x - line.p1.x) * (O.y - line.p1.y) - (line.p2.y - line.p1.y) * (O.x - line.p1.x)
	
	IF test_val >= 0
	 	RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
	
ENDFUNC

FUNC BOOL DO_LINES_INTERSECT(VECTOR a1, VECTOR a2, VECTOR b1, VECTOR b2, VECTOR &vIntersectPoint)   //does not work, I think it's the vMags
	vIntersectPoint = <<0,0,0>>
	VECTOR A = a2 - a1 //r
	VECTOR B = b2 - b1 //s
	FLOAT t = VMAG(CROSS_PRODUCT(b1 - a1,B))/ VMAG(CROSS_PRODUCT(A,B))
	FLOAT u = VMAG(CROSS_PRODUCT(b1 - a1,A))/ VMAG(CROSS_PRODUCT(A,B))
	IF t > 0 AND t < 1
	AND u > 0 AND u < 1
		println("u is ", u, " t is ", t)
		vIntersectPoint = a1 + (A*t)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT MIN(FLOAT a, FLOAT b)
	IF a<b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC FLOAT MAX(FLOAT a, FLOAT b)
	IF a>b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC BOOL DO_LINES_INTERSECT2(VECTOR p1, VECTOR p2, VECTOR p3, VECTOR p4, VECTOR &vIntersectPoint)
	// Store the values for fast access and easy
	// equations-to-code conversion
	FLOAT x1 = p1.x
	FLOAT x2 = p2.x
	FLOAT x3 = p3.x
	FLOAT x4 = p4.x
	FLOAT y1 = p1.y
	FLOAT y2 = p2.y
	FLOAT y3 = p3.y
	FLOAT y4 = p4.y
	
	float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
	// If d is zero, there is no intersection
	IF d = 0 
		RETURN FALSE 
	ENDIF
	
	// Get the x and y
	FLOAT fPre = (x1*y2 - y1*x2)
	FLOAT fPost = (x3*y4 - y3*x4)
	float x = ( fPre * (x3 - x4) - (x1 - x2) * fPost ) / d
	float y = ( fPre * (y3 - y4) - (y1 - y2) * fPost ) / d
	 
	// Check if the x and y coordinates are within both lines
	IF x < min(x1, x2) 
	OR x > max(x1, x2) 
	OR x < min(x3, x4)
	OR x > max(x3, x4)
		RETURN FALSE
	ENDIF
	
	IF y < min(y1, y2) 
	OR y > max(y1, y2)
	OR y < min(y3, y4) 
	OR y > max(y3, y4)
		RETURN FALSE
	ENDIF
	
	// Return the point of intersection
	vIntersectPoint.x = x
	vIntersectPoint.y = y
	vIntersectPoint.z = p1.z
	return TRUE
	
ENDFUNC

FUNC BOOL IS_POINT_BEHIND_LINE2(VECTOR O, VECTOR p1, VECTOR p2)
	RETURN ((p2.x - p1.x) * (O.y - p1.y) - (p2.y - p1.y) * (O.x - p1.x)) >= 0
ENDFUNC

FUNC VECTOR GET_NORMAL_OF_LINE(LINESTRUCT &line)

	VECTOR normal_from_origin
	VECTOR point_of_normal
	VECTOR middle_of_line
	
	FLOAT gradient_of_normal
	
	middle_of_line.x = line.p1.x + ((line.p2.x - line.p1.x)/2)
	middle_of_line.y = line.p1.y + ((line.p2.y - line.p1.y)/2)
	middle_of_line.z = line.p1.z + ((line.p2.z - line.p1.z)/2)
	gradient_of_normal = -1/((line.p2.y - line.p1.y)/(line.p2.x - line.p1.x))
	normal_from_origin.x = 1
	normal_from_origin.y = gradient_of_normal * normal_from_origin.x
	normal_from_origin.z = 0
	normal_from_origin = GET_NORMALISED_VERSION_OF_VECTOR(normal_from_origin)
	
	point_of_normal = middle_of_line + normal_from_origin
	
	IF IS_POINT_BEHIND_LINE(point_of_normal, line)
		point_of_normal = middle_of_line - normal_from_origin
	ENDIF
	
	RETURN point_of_normal
	
ENDFUNC

FUNC BOOL DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(VEHICLE_INDEX veh, VEHICLE_INDEX vehTarget, OBJECT_INDEX objTest, VECTOR vPosition, VECTOR vRotation)

	BOOL bRet
	
	IF DOES_ENTITY_EXIST(objTest)
		VECTOR vFront, vBack
		VECTOR vDimMin, vDimMax
		VECTOR vPlayerDimMin, vPlayerDimMax
		
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vDimMin, vDimMax)
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehTarget), vPlayerDimMin, vPlayerDimMax)
		
		FLOAT fHeight = ABSF(vDimMax.z - vDimMin.z)
		FLOAT fWidth = ABSF(vDimMax.x - vDimMin.x)
		FLOAT fLength = ABSF(vDimMax.y - vDimMin.y)
		
		FLOAT fRad 
		FLOAT fBump
		IF fWidth > fHeight
			fRad = fWidth/2
			fBump = fRad - absf(vDimMin.z)
		ELSE
			fRad = fLength/2
			fBump = fRad - absf(vDimMin.x)
		ENDIF
		
		VECTOR vCoord
		vCoord.z += vPlayerDimMin.z - vDimMin.z
		
		SET_ENTITY_COORDS(objTest, vPosition)
		SET_ENTITY_ROTATION(objTest, vRotation)
		
		vFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objTest, <<0,fLength/2 - fRad*0.75,fBump*1.25>> + vCoord)
		vBack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objTest, <<0,-fLength/2 + fRad*0.75,fBump*1.25>> + vCoord)
		
		INT		iHitResults
		VECTOR	vReturnPosition
		VECTOR	vReturnNormal
		ENTITY_INDEX hitEntity
			
		SHAPETEST_INDEX ShapeTestCapsuleIndex = START_SHAPE_TEST_CAPSULE(vBack, vFront, fRad, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE, PLAYER_PED_ID())
		
		IF GET_SHAPE_TEST_RESULT(ShapeTestCapsuleIndex, iHitResults, vReturnPosition, vReturnNormal, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shape test results not ready.")
			#ENDIF
		ENDIF
		
		IF ( iHitResults != 0 )
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shape test capsule hitting something.")
			#ENDIF
			bRet = FALSE
		ELSE
			bRet =  TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			INT iGreen
			INT iRed
			IF bRet 
				iGreen = 255
			ELSE
				iRed = 255
			ENDIF
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_VECTOR(vDimMin), <<0.8, 0.3, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_VECTOR(vDimMax), <<0.8, 0.32, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fHeight), <<0.8, 0.34, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fWidth), <<0.8, 0.36, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fLength), <<0.8, 0.38, 0>>)
			DRAW_DEBUG_SPHERE(vFront, 	0.05, iRed, iGreen, 0)
			DRAW_DEBUG_SPHERE(vFront, 	fRad, iRed, iGreen, 0, 100)
			DRAW_DEBUG_SPHERE(vBack, 	0.05, iRed, iGreen, 0)
			DRAW_DEBUG_SPHERE(vBack, 	fRad, iRed, iGreen, 0, 100)
			DRAW_DEBUG_LINE(vBack-<<0, 0, fRad>>, vFront-<<0, 0, fRad>>, iRed, iGreen, 0, 100)
			DRAW_DEBUG_LINE(vBack+<<0, 0, fRad>>, vFront+<<0, 0, fRad>>, iRed, iGreen, 0, 100)
		#ENDIF
		
	ENDIF
	
	RETURN bRet
	
ENDFUNC


//INFO: Warps a vehicle to a point that the target vehicle has already been at.
//PARAM NOTES: 	vehWarper - The vehicle that will warp
//				vehTarget - The vehicle that the warping vehicle is following
//				vWarpTarget - The current position that the warper will warp to
//				vWarpRot - The rotation at the warp position
//				iTimeOfLastWarp - The last time the warper warped
//				iLastSpotted - The last time the warper was seen on screen
//				fDistToRefreshTarget - The distance the target vehicle will be from the last warp target before it places a new warp target.
//				fMinWarpDist - The minimum distance the warper will be from the warp point to warrant warping
//				fMinDistTargetToWarpPoint - The minimum distance the Warp point must be from the target vehicle to allow the warper to warp.
//				iMinTime - The minimum time between warping
//				iMinOccludeTime - the minimum time the warping vehicle must be occluded before a warp is allowed
//PURPOSE: WARP_VEHICLE_TO_CHASE_TARGET warps a vehicle to a point that the targte vehicle has already been so that it can carry on chasing the target. 
// A slight rewrite of RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE that's a bit cheaper and uses the 

PROC WARP_VEHICLE_TO_CHASE_TARGET(VEHICLE_INDEX vehWarper, VEHICLE_INDEX vehTarget, VECTOR &vWarpTarget, VECTOR &vWarpRot, INT &iTimeOfLastWarp, INT &iLastSpotted,
										FLOAT fDistToRefreshTarget = 20.0, FLOAT fMinWarpDist = 20.0, FLOAT fMinDistTargetToWarpPoint = 15.0, 
										INT iMinTime = 3000, INT iMinOccludeTime = 2000)
	PRINTLN("WARP CHECK 1")
	IF DOES_ENTITY_EXIST(vehTarget)
	AND IS_VEHICLE_DRIVEABLE(vehTarget)
		PRINTLN("WARP CHECK 2")
		//check the vehicle being chased is far enough away from the current warp location to refresh the warp location
		IF VDIST2(GET_ENTITY_COORDS(vehTarget), vWarpTarget) > fDistToRefreshTarget*fDistToRefreshTarget
			PRINTLN("WARP CHECK 3")
			IF DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(vehWarper, vehTarget, objFitTest, GET_ENTITY_COORDS(vehTarget), GET_ENTITY_ROTATION(vehTarget))
				PRINTLN("WARP CHECK 4")
				vWarpTarget = GET_ENTITY_COORDS(vehTarget)
				vWarpRot = GET_ENTITY_ROTATION(vehTarget)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_SPHERE(vWarpTarget, 0.5, 0, 0, 255, 100)
		#ENDIF
		IF NOT IS_VECTOR_ZERO(vWarpTarget)
		AND vWarpTarget.z < 1
			PRINTLN("WARP CHECK 5")
			IF DOES_ENTITY_EXIST(vehWarper)
			AND IS_VEHICLE_DRIVEABLE(vehWarper)
				PRINTLN("WARP CHECK 6")
				IF NOT IS_ENTITY_OCCLUDED(vehWarper)
					PRINTLN("WARP CHECK 7")
					iLastSpotted = GET_GAME_TIMER()
				ELSE
					PRINTLN("WARP CHECK 8")
					IF (GET_GAME_TIMER() - iLastSpotted) > iMinOccludeTime
					AND (GET_GAME_TIMER() - iTimeOfLastWarp) > iMinTime
						PRINTLN("WARP CHECK 9")
						//Check that the warping vehicle is far enough away from the warp point
						IF VDIST2(GET_ENTITY_COORDS(vehWarper), vWarpTarget) > fMinWarpDist*fMinWarpDist
						//check that warping will place the warping vehicle closer to it's target
						AND VDIST2(GET_ENTITY_COORDS(vehWarper), vWarpTarget) < VDIST2(GET_ENTITY_COORDS(vehWarper), GET_ENTITY_COORDS(vehTarget))
						//check the target vehicle is far enough away from the warp location to allow the warping vehicle to go there
						AND VDIST2(GET_ENTITY_COORDS(vehtarget), vWarpTarget) > fMinDistTargetToWarpPoint*fMinDistTargetToWarpPoint
						AND DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(vehWarper, vehTarget, objFitTest, vWarpTarget, vWarpRot)
							PRINTLN("WARP CHECK 10")
							IF NOT IS_SPHERE_VISIBLE(vWarpTarget, 3.0)
								IF NOT IS_SPHERE_VISIBLE(vWarpTarget, 3.0)
									PRINTLN("WARP CHECK 11")
									CLEAR_AREA_OF_PEDS(vWarpTarget, 3.0)
							        CLEAR_AREA_OF_VEHICLES(vWarpTarget, 3.0)
									SET_ENTITY_COORDS(vehWarper, vWarpTarget)
									SET_ENTITY_ROTATION(vehWarper, vWarpRot)
						            SET_VEHICLE_FORWARD_SPEED(vehWarper, CLAMP(GET_ENTITY_SPEED(vehTarget), 10.0, GET_ENTITY_SPEED(vehTarget)))
						            SET_VEHICLE_ENGINE_ON(vehWarper, TRUE, TRUE)
						            iTimeOfLastWarp = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

STRUCT WARP_TARGET
	VECTOR vPosition
	VECTOR vRotation
	FLOAT fSpeed
ENDSTRUCT

WARP_TARGET chaserWarpTarget[2]

INT iChaserLastWarp[2]
INT iChaserLastSeen[2]
//VECTOR vChaserWarpTargetPosition[2]
//VECTOR vChaserWarpTargetRotation[2]

PROC MANAGE_CHASER_WARPING()

	IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
	
		INT i
		FOR i = 0 TO 1
			
			IF IS_VEHICLE_DRIVEABLE(vehChasers[i])
			AND NOT IS_PED_INJURED(pedChasers[i])
			AND IS_PED_SITTING_IN_VEHICLE(pedChasers[i], vehChasers[i])
				
				VECTOR vWarperCoords = GET_ENTITY_COORDS(vehChasers[i])
				VECTOR vTargetCoords = GET_ENTITY_COORDS(vehPlayerSeashark)
				
				//first check to see if a warp point can be set
				
				//don't set a warp point if the player is too close to either warp points
				IF VDIST2(vTargetCoords, chaserWarpTarget[0].vPosition) > MIN_DIST_BETWEEN_WARP_POINTS
				AND VDIST2(vTargetCoords, chaserWarpTarget[1].vPosition) > MIN_DIST_BETWEEN_WARP_POINTS
					//Set a warp point if the player is far enough from the last version of that warp point
					IF VDIST2(vTargetCoords, chaserWarpTarget[i].vPosition) > WARP_REFRESH_DISTANCE
						//if the chasing vehicle will fit at this warp point then set the warp point.
						IF DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(vehChasers[i], vehPlayerSeashark, objFitTest, vTargetCoords, GET_ENTITY_ROTATION(vehPlayerSeashark))
							chaserWarpTarget[i].vPosition = vTargetCoords
							chaserWarpTarget[i].vRotation = GET_ENTITY_ROTATION(vehPlayerSeashark)
							chaserWarpTarget[i].fSpeed = VMAG(GET_ENTITY_VELOCITY(vehChasers[i]))
						ENDIF
					ENDIF
				ENDIF
				
				//Then check if a warp point can be warped to.
				
				IF NOT IS_VECTOR_ZERO(chaserWarpTarget[i].vPosition)
				AND chaserWarpTarget[i].vPosition.z < 1
					IF NOT IS_ENTITY_OCCLUDED(vehChasers[i])
						iChaserLastSeen[i] = GET_GAME_TIMER()
					ELSE
						IF (GET_GAME_TIMER() - iChaserLastSeen[i]) > MIN_TIME_OCCLUDED
						AND (GET_GAME_TIMER() - iChaserLastWarp[i]) > MIN_TIME_BETWEEN_WARPS
							IF NOT IS_SPHERE_VISIBLE(chaserWarpTarget[i].vPosition, 3.0)
							AND NOT IS_SPHERE_VISIBLE(vWarperCoords, 3.0)
								FLOAT fDistToOtherChaser = 99.0
								INT iOtherGuy = 0
								IF i = 0 iOtherGuy = 1 ENDIF
								IF IS_VEHICLE_DRIVEABLE(vehChasers[iOtherGuy])
									fDistToOtherChaser = VDIST(GET_ENTITY_COORDS(vehChasers[iOtherGuy]), chaserWarpTarget[i].vPosition)
								ENDIF
								
								//Check that the warping vehicle is far enough away from the warp point
								IF VDIST2(vWarperCoords, vTargetCoords) > WARP_ALLOWED_DISTANCE
								//check that warping will place the warping vehicle closer to it's target
								AND VDIST2(vWarperCoords, chaserWarpTarget[i].vPosition) < VDIST2(vWarperCoords, vTargetCoords)
								//check the target vehicle is far enough away from the warp location to allow the warping vehicle to go there
								AND VDIST2(vTargetCoords, chaserWarpTarget[i].vPosition) > MIN_DIST_PLAYER_TO_WARP						
								AND fDistToOtherChaser > 5.0
								AND DOES_VEHICLE_FIT_AT_TARGET_POSITION_AND_ROTATION(vehChasers[i], vehPlayerSeashark, objFitTest, chaserWarpTarget[i].vPosition, chaserWarpTarget[i].vRotation)
								
									CLEAR_AREA_OF_PEDS(chaserWarpTarget[i].vPosition, 3.0)
							        CLEAR_AREA_OF_VEHICLES(chaserWarpTarget[i].vPosition, 3.0)
									SET_ENTITY_COORDS(vehChasers[i], chaserWarpTarget[i].vPosition)
									SET_ENTITY_ROTATION(vehChasers[i], chaserWarpTarget[i].vRotation)
						            SET_VEHICLE_FORWARD_SPEED(vehChasers[i], chaserWarpTarget[i].fSpeed + 5.0)
									TASK_VEHICLE_CHASE(pedChasers[i], PLAYER_PED_ID())
						            SET_VEHICLE_ENGINE_ON(vehChasers[i], TRUE, TRUE)
						            iChaserLastWarp[i] = GET_GAME_TIMER()
									
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_SPHERE(chaserWarpTarget[i].vPosition, 3, 0, 255, 0, 50)
					DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(chaserWarpTarget[i].fSpeed), chaserWarpTarget[i].vPosition + <<0,0,1>>, 0, 255, 0)
					DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(GET_GAME_TIMER() - iChaserLastWarp[i]), chaserWarpTarget[i].vPosition + <<0,0,1.5>>, 0, 255, 0)
				#ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDIF
	
ENDPROC

FUNC BOOL CREATE_CHASER(INT iChaser, BOOL bPreCutscene = FALSE)

	REQUEST_MODEL(SEASHARK)
	REQUEST_MODEL(CSB_PORNDUDES)
	
	IF HAS_MODEL_LOADED(SEASHARK)
	AND HAS_MODEL_LOADED(CSB_PORNDUDES)
	
		IF NOT DOES_ENTITY_EXIST(vehChasers[iChaser])
		
			VECTOR vPos
			FLOAT fHead
			
			IF iChaser = 0
			
				IF bPreCutscene
					vPos = <<-2023.6400, -1047.0404, 0.6916>>
					fHead = -108.0000
				ELSE
					vPos = <<-2024.7164, -1046.3866, 0.0>>
					fHead = -108.9627
				ENDIF
				
			ELSE
			
				IF bPreCutscene
					vPos = <<-2027.4000, -1045.8800, 0.6916>>
					fHead = -108.0000
				ELSE
					vPos = <<-2021.2002, -1046.9255, 0.0>>
					fHead = -84.5780
				ENDIF
				
			ENDIF
			
			FLOAT fHeight
			IF GET_WATER_HEIGHT(vPos, fHeight)
				vPos.z = fHeight
				vehChasers[iChaser] = CREATE_VEHICLE(SEASHARK, vPos, fHead)
				IF iChaser = 0
					SET_VEHICLE_COLOUR_COMBINATION(vehChasers[iChaser], 3)
				ELSE
					SET_VEHICLE_COLOUR_COMBINATION(vehChasers[iChaser], 1)
				ENDIF
				SET_BOAT_LOW_LOD_ANCHOR_DISTANCE(vehChasers[iChaser], 100)
				SET_BOAT_ANCHOR(vehChasers[iChaser], bPreCutscene)
				PRINTLN("Created seashark ", iChaser)
			ENDIF
			
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehChasers[iChaser])
		AND IS_VEHICLE_DRIVEABLE(vehChasers[iChaser])
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehChasers[iChaser], TRUE)
			SET_ENTITY_PROOFS(vehChasers[iChaser], FALSE, FALSE, FALSE, TRUE, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehChasers[iChaser], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			IF bPreCutscene
				SET_VEHICLE_ENGINE_ON(vehChasers[iChaser], FALSE, TRUE)
			ELSE
				SET_VEHICLE_ENGINE_ON(vehChasers[iChaser], TRUE, TRUE)
			ENDIF
			SET_CAN_CLIMB_ON_ENTITY(vehChasers[iChaser], FALSE)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(pedChasers[iChaser])
		AND IS_VEHICLE_DRIVEABLE(vehChasers[iChaser])
			IF NOT bPreCutscene
				pedChasers[iChaser] = CREATE_PED_INSIDE_VEHICLE(vehChasers[iChaser], PEDTYPE_MISSION, CSB_PORNDUDES)
			ELSE
				pedChasers[iChaser] = CREATE_PED(PEDTYPE_MISSION, CSB_PORNDUDES, <<-2068.0266, -1024.3993, 10.9101>> + <<iChaser, 0, 0>>)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedChasers[iChaser])
		AND NOT IS_PED_INJURED(pedChasers[iChaser])
		AND DOES_ENTITY_EXIST(vehChasers[iChaser])
		AND IS_VEHICLE_DRIVEABLE(vehChasers[iChaser])
		
			IF NOT bPreCutscene
				FREEZE_ENTITY_POSITION(pedChasers[iChaser], FALSE)
				IF NOT IS_PED_IN_VEHICLE(pedChasers[iChaser], vehChasers[iChaser])
					SET_PED_INTO_VEHICLE(pedChasers[iChaser], vehChasers[iChaser])
				ENDIF
			ELSE
				FREEZE_ENTITY_POSITION(pedChasers[iChaser], TRUE)
			ENDIF
			
			SET_RAGDOLL_BLOCKING_FLAGS(pedChasers[iChaser], RBF_BULLET_IMPACT)
			SET_ENTITY_PROOFS(pedChasers[iChaser], FALSE, FALSE, FALSE, TRUE, TRUE)
			GIVE_WEAPON_TO_PED(pedChasers[iChaser], WEAPONTYPE_PISTOL, INFINITE_AMMO)
			SET_PED_COMBAT_ATTRIBUTES(pedChasers[iChaser], CA_LEAVE_VEHICLES, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedChasers[iChaser], CA_USE_VEHICLE, TRUE)
			SET_PED_COMBAT_ABILITY(pedChasers[iChaser], CAL_POOR)
			SET_PED_COMBAT_RANGE(pedChasers[iChaser], CR_FAR)
			SET_PED_ACCURACY(pedChasers[iChaser], 0)
			SET_PED_SHOOT_RATE(pedChasers[iChaser], 15)
			SET_ENTITY_LOAD_COLLISION_FLAG(pedChasers[iChaser], TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedChasers[iChaser], TRUE)
			SET_PED_CAN_BE_DRAGGED_OUT(pedChasers[iChaser], FALSE)
			SET_PED_CAN_BE_SHOT_IN_VEHICLE(pedChasers[iChaser], TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedChasers[iChaser], rgChasers)
			SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedChasers[iChaser], FALSE)
			
			IF iChaser = 0
				SET_PED_COMPONENT_VARIATION(pedChasers[iChaser], PED_COMP_HEAD, 2, 0)
				SET_PED_COMPONENT_VARIATION(pedChasers[iChaser], PED_COMP_TORSO, 2, 0)
				SET_PED_COMPONENT_VARIATION(pedChasers[iChaser], PED_COMP_LEG, 2, 0)
				SET_PED_PROP_INDEX(pedChasers[iChaser], ANCHOR_HEAD, 0, 0)
			ELSE
				SET_PED_COMPONENT_VARIATION(pedChasers[iChaser], PED_COMP_HEAD, 1, 0)
				SET_PED_COMPONENT_VARIATION(pedChasers[iChaser], PED_COMP_TORSO, 1, 0)
				SET_PED_COMPONENT_VARIATION(pedChasers[iChaser], PED_COMP_LEG, 1, 0)
				SET_PED_PROP_INDEX(pedChasers[iChaser], ANCHOR_EYES, 1, 0)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_23 sTemp = "Chaser "
				sTemp += iChaser			
				SET_PED_NAME_DEBUG(pedChasers[iChaser], sTemp)
			#ENDIF
			
		ENDIF
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedChasers[iChaser]) 
	AND DOES_ENTITY_EXIST(vehChasers[iChaser])
	AND NOT IS_PED_INJURED(pedChasers[iChaser])
	AND IS_VEHICLE_DRIVEABLE(vehChasers[iChaser])
	AND (IS_PED_IN_VEHICLE(pedChasers[iChaser], vehChasers[iChaser]) OR bPreCutscene)
		RETURN TRUE
	#IF IS_DEBUG_BUILD
	ELSE
		IF NOT DOES_ENTITY_EXIST(pedChasers[iChaser])
			PRINTLN("ped ", iChaser, " doesn't exist")
		ENDIF
		IF IS_PED_INJURED(pedChasers[iChaser])
			PRINTLN("ped ", iChaser, " is injured")
		ENDIF
		IF NOT DOES_ENTITY_EXIST(vehChasers[iChaser])
			PRINTLN("veh ", iChaser, " doesn't exist")
		ENDIF
		IF NOT IS_VEHICLE_DRIVEABLE(vehChasers[iChaser])
			PRINTLN("veh ", iChaser, " is broke")
		ENDIF
		IF NOT bPreCutscene
			IF IS_VEHICLE_DRIVEABLE(vehChasers[iChaser])
			AND NOT IS_PED_INJURED(pedChasers[iChaser])
			AND NOT IS_PED_IN_VEHICLE(pedChasers[iChaser], vehChasers[iChaser])
				IF NOT DOES_ENTITY_EXIST(pedChasers[iChaser])
					PRINTLN("ped ", iChaser, " is not in vehicle")
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

INT iBlockingObj
PROC MANAGE_BLOCKING_AREAS(BOOL bForceCleanup = FALSE)

	IF NOT bBlockAreaBike
	AND NOT bForceCleanup
		IF current_mission_stage < stage_go_to_yacht
			sbiBikeArea= ADD_SCENARIO_BLOCKING_AREA(<< -1120.2920, -1681.0181, 13.2740 >>, << -1081.2887, -1706.5221, 1.5344 >>)
			SET_ROADS_IN_ANGLED_AREA(<<-1082.087646,-1714.032959,3.285254>>, <<-1107.201050,-1681.870239,14.290073>>, 37.500000, FALSE, FALSE)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1112.610718,-1697.382690,5.919011>> - <<42.688000,30.000000,2.750000>>, <<-1112.610718,-1697.382690,5.919011>> + <<42.688000,30.000000,2.750000>>, FALSE)
			iBlockingObj = ADD_NAVMESH_BLOCKING_OBJECT(<<-1107.517, -1692.887, 4.374>>, <<4.161, 13.858, 4.5>>, DEG_TO_RAD(36.0244), FALSE, BLOCKING_OBJECT_WANDERPATH)
			//SET_PED_PATHS_IN_AREA(<<-1106.888672,-1694.244507,4.373740>> + <<2.500000,2.500000,1.000000>>, <<-1106.888672,-1694.244507,4.373740>> - <<2.500000,2.500000,1.000000>>, FALSE)
			//SET_PED_PATHS_IN_AREA(<<-1108.548828,-1691.397339,4.374632>> + <<2.500000,2.500000,1.000000>>, <<-1108.548828,-1691.397339,4.374632>> - <<2.500000,2.500000,1.000000>>, FALSE)
			bBlockAreaBike = TRUE
		ENDIF
	ELSE
		IF current_mission_stage >= stage_go_to_yacht
		OR bForceCleanup
			REMOVE_SCENARIO_BLOCKING_AREA(sbiBikeArea)
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-1082.087646,-1714.032959,3.285254>>, <<-1107.201050,-1681.870239,14.290073>>, 37.500000)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1112.610718,-1697.382690,5.919011>> - <<42.688000,30.000000,2.750000>>, <<-1112.610718,-1697.382690,5.919011>> + <<42.688000,30.000000,2.750000>>, TRUE)
			IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iBlockingObj)
				REMOVE_NAVMESH_BLOCKING_OBJECT(iBlockingObj)
			ENDIF
			//SET_PED_PATHS_IN_AREA(<<-1106.888672,-1694.244507,4.373740>> + <<2.500000,2.500000,1.000000>>, <<-1106.888672,-1694.244507,4.373740>> - <<2.500000,2.500000,1.000000>>, TRUE)
			//SET_PED_PATHS_IN_AREA(<<-1108.548828,-1691.397339,4.374632>> + <<2.500000,2.500000,1.000000>>, <<-1108.548828,-1691.397339,4.374632>> - <<2.500000,2.500000,1.000000>>, TRUE)
			bBlockAreaBike = FALSE
		ENDIF
	ENDIF
	  
ENDPROC

FUNC BOOL SETUP_CLOTHES(BOOL bTopless = FALSE, BOOL bWait = FALSE)

	IF bTopless
		PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_BARECHEST_SHORTS)
		PRINTLN("PRE LOADING OUTFIT_P0_BARECHEST_SHORTS")
		IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
		OR NOT bWait
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BARECHEST_SHORTS, FALSE)
			PRINTLN("SETTING OUTFIT OUTFIT_P0_BARECHEST_SHORTS")
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
//			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
			RETURN TRUE
		ENDIF
	ELSE
		PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_YOGA_FLIP_FLOPS)
		PRINTLN("PRE LOADING OUTFIT_P0_YOGA_FLIP_FLOPS")
		IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
		OR NOT bWait
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA_FLIP_FLOPS, FALSE)
			PRINTLN("SETTING OUTFIT OUTFIT_P0_YOGA_FLIP_FLOPS")
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
//			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//╞═════════╡SCRIPT TERMINATION

PROC Mission_Cleanup()

	IF DOES_ENTITY_EXIST(pedJimmy)
	AND NOT IS_PED_INJURED(pedJimmy)
		CLEAR_PED_TASKS(pedJimmy)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTracy)
	AND NOT IS_PED_INJURED(pedTracy)
		CLEAR_PED_TASKS(pedTracy)
	ENDIF
	
	DISABLE_TAXI_HAILING(FALSE)
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_01_SC	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_02_GS	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_03_DT	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_04_CS	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_05_GSD	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_06_VC	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_07_PB	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_M_01_SM	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_M_03_H		, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_M_04_HW	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_M_05_GOH	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_H_01_BH	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_H_02_B		, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_H_03_MW	, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_A_01_VB	, FALSE)
	
	KILL_ANY_CONVERSATION()
	
	SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerSeashark)
	SET_PED_AS_NO_LONGER_NEEDED(pedJimmy)
	SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerBike)
	SET_VEHICLE_AS_NO_LONGER_NEEDED(vehJimmyBike)
	SET_PED_AS_NO_LONGER_NEEDED(pedTracy)
	
//	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	SET_WANTED_LEVEL_MULTIPLIER(1)
	ASSISTED_MOVEMENT_REMOVE_ROUTE("Family5d")
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	SET_ROADS_BACK_TO_ORIGINAL(<<-1057.899048,-1682.938599,5.785122>> + <<29.000000,30.000000,4.000000>>, <<-1057.899048,-1682.938599,5.785122>> - <<29.000000,30.000000,4.000000>>)
	SET_PED_PATHS_BACK_TO_ORIGINAL(<<-1329.113037,-1325.827026,5.753020>> + <<13.250000,44.000000,2.500000>>, <<-1329.113037,-1325.827026,5.753020>> - <<13.250000,44.000000,2.500000>>)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1112.912964,-1688.035034,6.373647>> + <<22.750000,24.750000,3.500000>>, <<-1112.912964,-1688.035034,6.373647>> - <<22.750000,24.750000,3.500000>>, TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", TRUE)
	SET_PED_PATHS_BACK_TO_ORIGINAL(<<-1106.888672,-1694.244507,4.373740>> + <<2.500000,2.500000,1.000000>>, <<-1106.888672,-1694.244507,4.373740>> - <<2.500000,2.500000,1.000000>>)
	SET_PED_PATHS_BACK_TO_ORIGINAL(<<-1108.548828,-1691.397339,4.374632>> + <<2.500000,2.500000,1.000000>>, <<-1108.548828,-1691.397339,4.374632>> - <<2.500000,2.500000,1.000000>>)
	
	IF DOES_ENTITY_EXIST(objDoorL)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorL)
	ENDIF
	IF DOES_ENTITY_EXIST(objDoorR)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorR)
	ENDIF
	IF DOES_CAM_EXIST(camIGIntro)
		DESTROY_CAM(camIGIntro)
	ENDIF
	
	SET_MAX_WANTED_LEVEL(5)
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(wave_widget)
			DELETE_WIDGET_GROUP(wave_widget)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(chase_widget)
			DELETE_WIDGET_GROUP(chase_widget)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(parent_widget)
			DELETE_WIDGET_GROUP(parent_widget)
		ENDIF
	#ENDIF
	
	MANAGE_BLOCKING_AREAS(TRUE)
	
	REMOVE_FORCED_OBJECT(<<-1105.2411, -1697.0247, 3.3678>>, 2, PROP_TABLE_03_CHR)
	
	SET_AUDIO_FLAG("AvoidMissionCompleteDelay", FALSE)
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC Mission_Passed()
	TRIGGER_MUSIC_EVENT("FAM2_STOP")
	IF DOES_ENTITY_EXIST(vehPlayerCar)
		SET_MISSION_VEHICLE_GEN_VEHICLE(vehPlayerCar, <<-1060.5181, -1691.8796, 3.5097>>, 185.4265)
	ENDIF
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
ENDPROC

PROC Mission_Failed(STRING print_me)

	TRIGGER_MUSIC_EVENT("FAM2_STOP")
	
	IF ARE_STRINGS_EQUAL(print_me, "FM2_JSPOOK")
		PRINTLN("Jimmy spook is fail reason")
		IF IS_ENTITY_ON_FIRE(pedJimmy)
			PRINTLN("Set to jimmy injured")
			print_me = "FM2_JIMHIT"
		ENDIF
		PRINTLN(" onward to fail")
	ENDIF
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(print_me)
	
	IF ARE_STRINGS_EQUAL(print_me, "FM2_TSPOOK")
	OR ARE_STRINGS_EQUAL(print_me, "FM2_JSPOOK")
		IF DOES_ENTITY_EXIST(pedTracy)
			IF IS_PED_GROUP_MEMBER(pedTracy, GET_PLAYER_GROUP(PLAYER_ID()))
				REMOVE_PED_FROM_GROUP(pedTracy)
			ENDIF
			TASK_SMART_FLEE_PED(pedTracy, PLAYER_PED_ID(), 500, 60000)
		ENDIF
		IF DOES_ENTITY_EXIST(pedJimmy)
			IF IS_PED_GROUP_MEMBER(pedJimmy, GET_PLAYER_GROUP(PLAYER_ID()))
				REMOVE_PED_FROM_GROUP(pedJimmy)
			ENDIF
			TASK_SMART_FLEE_PED(pedJimmy, PLAYER_PED_ID(), 500, 60000)
		ENDIF
	ENDIF
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	IF current_mission_stage = stage_drive_to_venice
	
		VEHICLE_INDEX vehTemp
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			PRINTLN("PLAYER IS IN VEHICLE")
		ELSE
			vehTemp = GET_LAST_DRIVEN_VEHICLE()
			PRINTLN("PLAYER IS NOT IN VEHICLE")
		ENDIF
		
		BOOL bNpcVeh = FALSE
		
		IF DOES_ENTITY_EXIST(vehTemp)
		AND IS_VEHICLE_DRIVEABLE(vehTemp)
		
			IF IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(vehTemp)
				INT iVeh
				REPEAT NUM_NPC_VEHICLE_IDS iVeh
					IF DOES_ENTITY_EXIST(g_viCreatedNPCVehicleIDs[iVeh])
						PRINTLN("Player veh is NPC veh")
						bNpcVeh = TRUE
						iVeh = NUM_NPC_VEHICLE_IDS
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF NOT IS_VEHICLE_MODEL(vehTemp, TAXI)
			AND NOT bNpcVeh
				IF VDIST(GET_ENTITY_COORDS(vehTemp), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 50
					IF GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(vehTemp) != CHAR_MICHAEL
						SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehTemp)
						PRINTLN("SETTING PLAYER VEH to impound")
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	REMOVE_IPL("smboat")
	
	INT iTemp
	FOR iTemp = 0 TO 1
		IF DOES_BLIP_EXIST(blipChasers[iTemp])
			REMOVE_BLIP(blipChasers[iTemp])
		ENDIF
		IF DOES_ENTITY_EXIST(pedChasers[iTemp])
			DELETE_PED(pedChasers[iTemp])
		ENDIF
		IF DOES_ENTITY_EXIST(vehChasers[iTemp])
			DELETE_VEHICLE(vehChasers[iTemp])
		ENDIF
	ENDFOR
	
	IF current_mission_stage >= stage_go_to_yacht
	AND current_mission_stage != stage_lose_cops
		WHILE NOT SETUP_CLOTHES(TRUE, TRUE)
			WAIT(0)
		ENDWHILE
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(vPlayerOnPierPosition, fPlayerOnPierHeading)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedJimmy)
		DELETE_PED(pedJimmy)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTracy)
		DELETE_PED(pedTracy)
	ENDIF
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
//	IF DOES_ENTITY_EXIST(vehPlayerSeashark)
//		//SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerSeashark)
//		DELETE_VEHICLE(vehPlayerSeashark)
//	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerBike)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehJimmyBike)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehJimmyBike)
	ENDIF
	
//	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bClothesHaveChanged)
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
ENDPROC

PROC SET_PED_AS_BUDDY(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
		SET_PED_CAN_BE_DRAGGED_OUT(ped, FALSE)
		SET_PED_CAN_BE_TARGETTED(ped, FALSE)
		SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(ped, FALSE)
		SET_PED_CONFIG_FLAG(ped, PCF_OpenDoorArmIK, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ped, FALSE)
		SET_PED_NEVER_LEAVES_GROUP(ped, TRUE)
		SET_PED_DIES_IN_WATER(ped, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped, rghBuds)
		PRINTLN("\n\n\n SETTING PED AS BUDDY \n\n")
	ENDIF
ENDPROC

//-------------------------------------------------------------------

//-------------------------------------------------------- Boat stuff

INT iCreateJimmyStage = 0
FUNC BOOL CREATE_JIMMY(VECTOR vPos, FLOAT fHead)
	SWITCH  iCreateJimmyStage
		CASE 0
			IF CREATE_NPC_PED_ON_FOOT(pedJimmy, CHAR_JIMMY, <<-808.0422, 168.5755, 75.7504>>, 244.0649)
				iCreateJimmyStage++	
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_PED_INJURED(pedJimmy)
//				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_BERD, 	2, 0)			
//				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_TORSO, 	5, 0)			
//				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_LEG, 		3, 0)
//				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_FEET, 	4, 0)			
//				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_DECL, 	2, 0)
				PRINTLN("VARIATIONS FOR JIMMY REQUESTED")
				
				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_BERD, 	0, 0)
				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_TORSO,	0, 0)
				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_LEG, 		0, 0)
				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_FEET, 	0, 0)
				SET_PED_PRELOAD_VARIATION_DATA(pedJimmy, PED_COMP_DECL, 	0, 0)
				
				iCreateJimmyStage++
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(pedJimmy)
				IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(pedJimmy)
//					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_BERD, 	2, 0)	
//					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_TORSO, 	5, 0)	
//					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_LEG,		3, 0)
//					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_FEET, 	4, 0)	
//					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_DECL, 	2, 0)

					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_BERD, 	0, 0)
					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_TORSO, 	0, 0)
					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_LEG,		0, 0)
					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_FEET, 	0, 0)
					SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_DECL, 	0, 0)
					
					RELEASE_PED_PRELOAD_VARIATION_DATA(pedJimmy)
					SET_ENTITY_COORDS(pedJimmy, vPos)
					SET_ENTITY_HEADING(pedJimmy, fHead)
					SET_PED_AS_BUDDY(pedJimmy)
					RELEASE_PED_PRELOAD_VARIATION_DATA(pedJimmy)
					PRINTLN("VARIATIONS SET JIMMY IS READY")
					iCreateJimmyStage=0
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

INT iCreateTraceyStage = 0
FUNC BOOL CREATE_TRACEY(VECTOR vPos, FLOAT fHead)
	SWITCH  iCreateTraceyStage
		CASE 0
			IF CREATE_NPC_PED_ON_FOOT(pedTracy, CHAR_TRACEY, <<-808.0422, 168.5755, 75.7504>>, 244.0649)
				iCreateTraceyStage++
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_PED_INJURED(pedTracy)
				SET_PED_PRELOAD_VARIATION_DATA(pedTracy, PED_COMP_HAIR, 	3, 0) //(berd)	
				SET_PED_PRELOAD_VARIATION_DATA(pedTracy, PED_COMP_TORSO, 	4, 0) //(uppr)	
				SET_PED_PRELOAD_VARIATION_DATA(pedTracy, PED_COMP_LEG, 		4, 0) //(lowr)
				SET_PED_PRELOAD_VARIATION_DATA(pedTracy, PED_COMP_FEET, 	2, 0) //(feet)
				SET_PED_PRELOAD_PROP_DATA(pedTracy, ANCHOR_HEAD, 0, 0)
				SET_PED_PRELOAD_PROP_DATA(pedTracy, ANCHOR_EYES, 1, 0)
				iCreateTraceyStage++
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_PED_INJURED(pedTracy)
				IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(pedTracy)
					SET_PED_COMPONENT_VARIATION(pedTracy, PED_COMP_HAIR, 	3, 0, 0) //(berd)
					SET_PED_COMPONENT_VARIATION(pedTracy, PED_COMP_TORSO, 	4, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedTracy, PED_COMP_LEG, 	4, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(pedTracy, PED_COMP_FEET, 	2, 0, 0) //(feet)
					SET_PED_PROP_INDEX(pedTracy, ANCHOR_HEAD, 0, 0)
					SET_PED_PROP_INDEX(pedTracy, ANCHOR_EYES, 1, 0)
					SET_ENTITY_COORDS(pedTracy, vPos)
					SET_ENTITY_HEADING(pedTracy, fHead)
					SET_PED_AS_BUDDY(pedTracy)
					RELEASE_PED_PRELOAD_VARIATION_DATA(pedTracy)
					RELEASE_PED_PRELOAD_PROP_DATA(pedTracy)
					iCreateTraceyStage=0
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC CREATE_BUDDY_IN_VEHICLE(PED_INDEX &ped, enumCharacterList CHAR, VEHICLE_INDEX &veh, VEHICLE_SEAT vs = VS_FRONT_RIGHT)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF NOT DOES_ENTITY_EXIST(ped)
			VECTOR vTemp
			FLOAT fTemp
			vTemp = GET_ENTITY_COORDS(veh)
			fTemp = GET_ENTITY_HEADING(veh)
			IF CHAR = CHAR_TRACEY
				WHILE NOT CREATE_TRACEY(vTemp, fTemp)
					WAIT(0)
				ENDWHILE
			ELSE
				WHILE NOT CREATE_JIMMY(vTemp, fTemp)
					WAIT(0)
				ENDWHILE
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(ped)
			AND IS_VEHICLE_DRIVEABLE(veh)
				IF NOT IS_PED_IN_VEHICLE(ped, veh)
					SET_PED_INTO_VEHICLE(ped, veh, vs)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_BUDDY_ON_FOOT(PED_INDEX &ped, enumCharacterList CHAR, VECTOR position, FLOAT heading)
	IF NOT DOES_ENTITY_EXIST(ped)
		IF CHAR = CHAR_TRACEY
			WHILE NOT CREATE_TRACEY(position, heading)
				WAIT(0)
			ENDWHILE
		ELSE
			WHILE NOT CREATE_JIMMY(position, heading)
				WAIT(0)
			ENDWHILE
		ENDIF
		IF NOT IS_PED_INJURED(ped)
			CLEAR_PED_TASKS(ped)
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(ped)
			SET_ENTITY_COORDS(ped, position)
			SET_ENTITY_HEADING(ped, heading)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CREATE_MP3_DOCK()

	IF NOT DOES_ENTITY_EXIST(objMP3Dock)
		REQUEST_MODEL(PROP_MP3_DOCK)
		IF HAS_MODEL_LOADED(PROP_MP3_DOCK)
			objMP3Dock = CREATE_OBJECT(PROP_MP3_DOCK, <<-2032.538940,-1032.350342,5.421927>>)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(objMP3Dock)
		SET_ENTITY_COORDS(objMP3Dock, <<-2032.538940,-1032.350342,5.421927>>)
		SET_ENTITY_ROTATION(objMP3Dock, <<-0.000000,0.000000,66.552299>>)
	ENDIF
	
	RETURN DOES_ENTITY_EXIST(objMP3Dock)
	
ENDFUNC

INT iDancingSceneStage
INT iDancingSceneIndex

PROC MANAGE_DANCING_SCENE()

	INT i
	
	SWITCH iDancingSceneStage
	
		CASE 0
			
			IF current_mission_stage = stage_go_to_yacht
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -2033.821, -1031.864, 4.890 >>) < 250*250
			
				REQUEST_MODEL(CSB_PORNDUDES)
				REQUEST_MODEL(A_F_Y_BEACH_01)
				REQUEST_ANIM_DICT("missfam2leadinoutmcs3")
				
				IF HAS_MODEL_LOADED(CSB_PORNDUDES)
				AND HAS_MODEL_LOADED(A_F_Y_BEACH_01)
				AND HAS_ANIM_DICT_LOADED("missfam2leadinoutmcs3")
				AND CREATE_TRACEY(<<-2033.23, -1032.27, 4.89>>, 0)
					IF NOT IS_PED_INJURED(pedTracy)
						pedBoatDancer[0] = CREATE_PED(PEDTYPE_MISSION, CSB_PORNDUDES, <<-2032.69, -1031.36, 5.59>>)
						SET_PED_LOD_MULTIPLIER(pedBoatDancer[0], 1.75)
						pedBoatDancer[1] = CREATE_PED(PEDTYPE_MISSION, CSB_PORNDUDES, <<-2034.42, -1032.71, 5.54>>)
						pedBoatDancer[2] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEACH_01, <<-2033.32, -1031.07, 5.54>>)
						pedBoatDancer[3] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEACH_01, <<-2034.48, -1031.60, 5.48>>)
						iDancingSceneIndex = CREATE_SYNCHRONIZED_SCENE(<< -2033.821, -1031.864, 4.890 >>, << -0.000, 0.000, -4.400 >>)
						TASK_SYNCHRONIZED_SCENE(pedBoatDancer[0], iDancingSceneIndex, "missfam2leadinoutmcs3", "onboat_leadin_pornguy_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(pedBoatDancer[1], iDancingSceneIndex, "missfam2leadinoutmcs3", "onboat_leadin_pornguy_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(pedBoatDancer[2], iDancingSceneIndex, "missfam2leadinoutmcs3", "onboat_leadin_porngal_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(pedBoatDancer[3], iDancingSceneIndex, "missfam2leadinoutmcs3", "onboat_leadin_porngal_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(pedTracy, iDancingSceneIndex, "missfam2leadinoutmcs3", "onboat_leadin_tracy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						
						SET_PED_COMPONENT_VARIATION(pedBoatDancer[0], PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedBoatDancer[0], PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedBoatDancer[0], PED_COMP_LEG, 0, 0)
						SET_PED_PROP_INDEX(pedBoatDancer[0], ANCHOR_EYES, 0, 0)
						
						SET_PED_COMPONENT_VARIATION(pedBoatDancer[1], PED_COMP_HEAD, 2, 0)
						SET_PED_COMPONENT_VARIATION(pedBoatDancer[1], PED_COMP_TORSO, 2, 0)
						SET_PED_COMPONENT_VARIATION(pedBoatDancer[1], PED_COMP_LEG, 2, 0)
						SET_PED_PROP_INDEX(pedBoatDancer[1], ANCHOR_HEAD, 0, 0)
						 
						SET_SYNCHRONIZED_SCENE_LOOPED(iDancingSceneIndex, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(CSB_PORNDUDES)
						SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_BEACH_01)
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_TRACYDISANTO)
						iDancingSceneStage++
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 1
		
			PRE_STREAM_CUTSCENE_AT_DISTANCE("family_2_mcs_3", << -2033.821, -1031.864, 4.890 >>, DEFAULT_CUTSCENE_LOAD_DIST, b_cutscene_request)
			
			IF b_cutscene_request
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				
					REQUEST_VEHICLE_RECORDING(1, "famtwoplayer")
					PRINTLN("SETTING components from player")
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					
					IF DOES_ENTITY_EXIST(pedBoatDancer[0])
					AND NOT IS_PED_INJURED(pedBoatDancer[0])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Guy_on_sofa", pedBoatDancer[0])	
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedBoatDancer[1])
					AND NOT IS_PED_INJURED(pedBoatDancer[1])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Man_getting_wood", pedBoatDancer[1])	
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedBoatDancer[2])
					AND NOT IS_PED_INJURED(pedBoatDancer[2])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Girl_on_sofa", pedBoatDancer[2])	
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedBoatDancer[3])
					AND NOT IS_PED_INJURED(pedBoatDancer[3])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Girl_being_chatted_up", pedBoatDancer[3])	
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedTracy)
					AND NOT IS_PED_INJURED(pedTracy)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Tracy", pedTracy)	
					ENDIF
					
				ENDIF
			ENDIF
			
			IF b_cutscene_request
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -2033.821, -1031.864, 4.890 >>) > DEFAULT_CUTSCENE_UNLOAD_DIST*DEFAULT_CUTSCENE_UNLOAD_DIST
					b_cutscene_request = FALSE
					REMOVE_CUTSCENE()
				ENDIF
			ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -2033.821, -1031.864, 4.890 >>) > 275*275
			OR (current_mission_stage <> stage_go_to_yacht AND current_mission_stage <> stage_rescue)
			
				IF current_mission_stage <> stage_rescue
				
					BOOL bAllOffScreen
					bAllOffScreen = TRUE
					REPEAT COUNT_OF(pedBoatDancer) i
						IF NOT IS_PED_INJURED(pedBoatDancer[i])
							IF NOT IS_ENTITY_OCCLUDED(pedBoatDancer[i])
								bAllOffScreen = FALSE
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF NOT IS_PED_INJURED(pedTracy)
						IF NOT IS_ENTITY_OCCLUDED(pedTracy)
							bAllOffscreen = FALSE
						ENDIF
					ENDIF
					
					IF bAllOffScreen
						REPEAT COUNT_OF(pedBoatDancer) i
							IF DOES_ENTITY_EXIST(pedBoatDancer[i])
								CLEAR_PED_TASKS_IMMEDIATELY(pedBoatDancer[i])
								DELETE_PED(pedBoatDancer[i])
								PRINTLN("Deleting dancer ped ", i)
							ENDIF
						ENDREPEAT
					ENDIF
					
					iDancingSceneStage = 0
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		
	ENDSWITCH

ENDPROC

//-------------------------------------------------------------------

//---------------------------------------------------- Misc functions
INT iBikeShopStage

FUNC BOOL SETUP_BIKE_SHOP(BOOL bAfterBikeHire = FALSE)

	REQUEST_MODEL(U_M_M_BIKEHIRE_01)
	REQUEST_MODEL(CRUISER)
	REQUEST_MODEL(BMX)
	REQUEST_MODEL(SCORCHER)
	REQUEST_MODEL(TRIBIKE)
	
	IF HAS_MODEL_LOADED(U_M_M_BIKEHIRE_01)
	AND HAS_MODEL_LOADED(CRUISER)
	AND HAS_MODEL_LOADED(BMX)
	AND HAS_MODEL_LOADED(SCORCHER)
	AND HAS_MODEL_LOADED(TRIBIKE)
	
		IF iMikeChoice <> 0
		OR NOT bAfterbikeHire
			IF NOT DOES_ENTITY_EXIST(vehBikes[0])
				vehBikes[0] = CREATE_VEHICLE(SCORCHER, <<-1106.8845, -1692.2295, 4.0189>>)  //scorcher
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBikes[0])
				SET_ENTITY_COORDS_NO_OFFSET(vehBikes[0], <<-1106.8845, -1692.2295, 4.0189>>)
				SET_ENTITY_QUATERNION(vehBikes[0], -0.0504, -0.0723, -0.4427, 0.8923)
				SET_VEHICLE_COLOUR_COMBINATION(vehBikes[0], 2)
			ENDIF
		ENDIF
		
		IF iJimmyChoice <> 1
		AND iMikeChoice <> 1
		OR NOT bAfterbikeHire	
			IF NOT DOES_ENTITY_EXIST(vehBikes[1])
				vehBikes[1] = CREATE_VEHICLE(CRUISER, <<-1108.3959, -1690.3990, 3.9549>>) // cruiser
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBikes[1])
				SET_ENTITY_COORDS_NO_OFFSET(vehBikes[1], <<-1108.3959, -1690.3990, 3.9549>>)
				SET_ENTITY_QUATERNION(vehBikes[1], -0.0466, -0.0740, -0.4628, 0.8822)
				SET_VEHICLE_COLOUR_COMBINATION(vehBikes[1], 1)
			ENDIF
		ENDIF
		
		IF iMikeChoice <> 2
		OR NOT bAfterbikeHire
			IF NOT DOES_ENTITY_EXIST(vehBikes[2])
				vehBikes[2] = CREATE_VEHICLE(BMX, <<-1109.4050, -1688.7036, 3.7501>>)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBikes[2])
				SET_ENTITY_COORDS_NO_OFFSET(vehBikes[2], <<-1109.4050, -1688.7036, 3.7501>>)
				SET_ENTITY_QUATERNION(vehBikes[2], 0.0178, 0.0525, -0.4469, 0.8929)
			ENDIF
		ENDIF
		
		IF iMikeChoice <> 3
		OR NOT bAfterbikeHire
			IF NOT DOES_ENTITY_EXIST(vehBikes[3])
				vehBikes[3] = CREATE_VEHICLE(TRIBIKE, <<-1111.0010, -1686.6287, 3.9705>>)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBikes[3])
				SET_ENTITY_COORDS_NO_OFFSET(vehBikes[3], <<-1111.0010, -1686.6287, 3.9705>>)
				SET_ENTITY_QUATERNION(vehBikes[3], -0.0563, -0.0702, -0.5377, 0.8383)
			ENDIF
		ENDIF
		
		IF iJimmyChoice <> 4
		AND iMikeChoice <> 4
		OR NOT bAfterbikeHire
			IF NOT DOES_ENTITY_EXIST(vehBikes[4])
				vehBikes[4] = CREATE_VEHICLE(CRUISER, <<-1105.6056, -1693.9800, 3.9518>>) //cruiser
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBikes[4])
				SET_ENTITY_COORDS_NO_OFFSET(vehBikes[4], <<-1105.6056, -1693.9800, 3.9518>>)
				SET_ENTITY_QUATERNION(vehBikes[4], -0.0437, -0.0759, -0.4104, 0.9077)
				SET_VEHICLE_COLOUR_COMBINATION(vehBikes[4], 0)
				FREEZE_ENTITY_POSITION(vehBikes[4], TRUE)
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(pedBikeGuy)
			pedBikeGuy = CREATE_PED(PEDTYPE_MISSION, U_M_M_BIKEHIRE_01, v_bike_guy_pos, f_bike_guy_heading)
		ENDIF
		
		CLEAR_PED_TASKS_IMMEDIATELY(pedBikeGuy)
		SET_PED_COMBAT_ATTRIBUTES(pedBikeGuy, CA_ALWAYS_FLEE, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedBikeGuy, FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedBikeGuy, FA_DISABLE_COWER, TRUE)
		
		ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedBikeGuy, "FAM2BIKEHIRE")
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBikeGuy, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedBikeGuy, RELGROUPHASH_HATES_PLAYER)
		SET_PED_CAN_BE_TARGETTED(pedBikeGuy, FALSE)
		SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(pedBikeGuy, TRUE)
		INT iTemp
		REPEAT COUNT_OF(vehBikes) iTemp
			IF DOES_ENTITY_EXIST(vehBikes[iTemp])
				SET_ENTITY_PROOFS(vehBikes[iTemp], TRUE, TRUE, TRUE, TRUE, TRUE)
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	BOOL bReturn
	
	#IF IS_DEBUG_BUILD
	
		IF NOT DOES_ENTITY_EXIST(vehBikes[0])	 	println("vehBikes[0]	doesn't exist") ENDIF
		IF NOT DOES_ENTITY_EXIST(vehBikes[1])	 	println("vehBikes[1]	doesn't exist") ENDIF
		IF NOT DOES_ENTITY_EXIST(vehBikes[2])	 	println("vehBikes[2]	doesn't exist") ENDIF
		IF NOT DOES_ENTITY_EXIST(vehBikes[3])	 	println("vehBikes[3]	doesn't exist") ENDIF
		IF NOT DOES_ENTITY_EXIST(vehBikes[4]) 		println("vehBikes[4] 	doesn't exist") ENDIF
		IF NOT DOES_ENTITY_EXIST(pedBikeGuy) 		println("pedBikeGuy 	doesn't exist") ENDIF
		
	#ENDIF
	
	IF bAfterBikeHire
		BOOL bDone = TRUE
		INT i
		REPEAT COUNT_OF(vehBikes) i
			IF i <> iMikeChoice
			AND i <> iJimmyChoice
				IF NOT DOES_ENTITY_EXIST(vehBikes[i])
					bDone = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		bReturn = bDone
	ELSE
		bReturn = DOES_ENTITY_EXIST(vehBikes[0])
		AND DOES_ENTITY_EXIST(vehBikes[1])
		AND DOES_ENTITY_EXIST(vehBikes[2])
		AND DOES_ENTITY_EXIST(vehBikes[3])
		AND DOES_ENTITY_EXIST(vehBikes[4])
		AND DOES_ENTITY_EXIST(pedBikeGuy)
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

PROC CLEANUP_BIKE_SHOP()
	IF DOES_ENTITY_EXIST(pedBikeGuy)
		DELETE_PED(pedBikeGuy)
	ENDIF
	INT iTemp
	REPEAT COUNT_OF(vehBikes) iTemp
		IF DOES_ENTITY_EXIST(vehBikes[iTemp])
			DELETE_VEHICLE(vehBikes[iTemp])
		ENDIF
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(TRIBIKE)
	SET_MODEL_AS_NO_LONGER_NEEDED(BMX)
	SET_MODEL_AS_NO_LONGER_NEEDED(CRUISER)
	SET_MODEL_AS_NO_LONGER_NEEDED(SCORCHER)
	SET_MODEL_AS_NO_LONGER_NEEDED(U_M_M_BIKEHIRE_01)
ENDPROC

FLOAT fAimTime

PROC CHECK_FOR_RENTAL_STAND_INTERFERENCE()

	VEHICLE_INDEX vehTemp
	BOOL bInterference = FALSE
	BOOL bBikesHit = FALSE
	BOOL bWAntedFail = FALSE
	INT iTemp
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
	ELSE
		IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
			vehTemp = GET_PLAYERS_LAST_VEHICLE()
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTemp)
	AND IS_VEHICLE_DRIVEABLE(vehTemp)
	AND NOT IS_VEHICLE_MODEL(vehTemp, SCORCHER)
	AND NOT IS_VEHICLE_MODEL(vehTemp, CRUISER)
	AND NOT IS_VEHICLE_MODEL(vehTemp, TRIBIKE)
	AND NOT IS_VEHICLE_MODEL(vehTemp, BMX)
		IF ABSF(GET_ENTITY_SPEED(vehTemp)) > 2
			IF IS_ENTITY_AT_COORD(vehTemp, <<-1106.308960,-1693.715454,5.123790>>,<<6.250000,7.000000,3.250000>>)
				REPEAT COUNT_OF(vehBikes) iTemp
					IF IS_VEHICLE_DRIVEABLE(vehBikes[iTemp])
						IF IS_ENTITY_TOUCHING_ENTITY(vehBikes[iTemp], vehTemp)
							bBikesHit = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
	IF bInterference
	OR bBikesHit
		REPEAT COUNT_OF(vehBikes) iTemp
			IF IS_VEHICLE_DRIVEABLE(vehBikes[iTemp])
				FREEZE_ENTITY_POSITION(vehBikes[iTemp], FALSE)
				APPLY_FORCE_TO_ENTITY(vehBikes[iTemp], APPLY_TYPE_FORCE, <<0,0,-0.001>>, <<0,0,0>>, 0, TRUE, TRUE, TRUE)
			ENDIF
		ENDREPEAT
		IF NOT IS_PED_INJURED(pedBikeGuy)
			IF bBikesHit
				Mission_Failed("FM2_BIKES")
			ENDIF
		ENDIF
	ENDIF
	
	//check for fail if you disrupt the scene at all
	
	BOOL bDisruptFail = FALSE
	
	IF NOT IS_PED_INJURED(pedBikeGuy)
	
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedBikeGuy), 20)
		OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedBikeGuy), 20)
		OR IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(pedBikeGuy)+<<20,20,20>>, GET_ENTITY_COORDS(pedBikeGuy)-<<20,20,20>>)
		OR IS_PED_FLEEING(pedBikeGuy)
		OR (current_mission_stage = stage_hire_bike AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_bike_guy_pos, <<40,40,20>>))
			 bDisruptFail = TRUE
		ENDIF
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedBikeGuy)) < 10*10
			IF CAN_PED_SEE_HATED_PED(pedBikeGuy, PLAYER_PED_ID())
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE )
				AND IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedBikeGuy)
					fAimTime += GET_FRAME_TIME()
					IF fAimTime > 0.5
						bDisruptFail = TRUE
					ENDIF
				ELSE
					IF fAimTime > 0
						fAimTime -= GET_FRAME_TIME()*2.0
					ELIF fAimTime <> 0.0
						fAimTime = 0.0
					ENDIF
				ENDIF
			ENDIF
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBikeGuy, PLAYER_PED_ID())
			OR IS_PED_RAGDOLL(pedBikeGuy)
				bDisruptFail = TRUE
			ENDIF
		ENDIF
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedBikeGuy)) < 400
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			bWAntedFail = TRUE
			bDisruptFail = TRUE
		ENDIF
		
		
//		#IF IS_DEBUG_BUILD
//			DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(fAimTime), GET_ENTITY_COORDS(pedBikeGuy) + <<0,0,1>>)
//		#ENDIF
		
	ENDIF
	
	IF bDisruptFail
		REPEAT COUNT_OF(vehBikes) iTemp
			IF DOES_ENTITY_EXIST(vehBikes[iTemp])
				FREEZE_ENTITY_POSITION(vehbikes[iTemp], FALSE)
			ENDIF
		ENDREPEAT
		IF NOT IS_PED_INJURED(pedBikeGuy)
		AND NOT IS_PED_FLEEING(pedBikeGuy)
		AND IS_PED_USING_ANY_SCENARIO(pedBikeGuy)
			SET_PED_SHOULD_PLAY_FLEE_SCENARIO_EXIT(pedBikeGuy, GET_ENTITY_COORDS(pedBikeGuy) + GET_ENTITY_FORWARD_VECTOR(pedBikeGuy))
			TASK_SMART_FLEE_PED(pedBikeGuy, PLAYER_PED_ID(), 200, -1)
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehJimmyBike)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
			ENDIF
		ENDIF
		IF current_mission_stage = stage_drive_to_venice
		OR current_mission_stage = stage_hire_bike
		OR (current_mission_stage = stage_bike_ride AND IS_ENTITY_AT_ENTITY(pedjimmy, pedBikeGuy, <<30,30,30>>))
		OR current_mission_stage = stage_lose_cops
			IF NOT IS_PED_INJURED(pedJimmy)
				SET_PED_FLEE_ATTRIBUTES(pedJimmy, FA_USE_VEHICLE, FALSE)
				TASK_SMART_FLEE_PED(pedJimmy, PLAYER_PED_ID(), 200, 20000)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedBikeGuy)
	AND (IS_ENTITY_DEAD(pedBikeGuy)	OR IS_PED_INJURED(pedBikeGuy))
		IF current_mission_stage < stage_bike_ride
		OR current_mission_stage = stage_bike_ride AND IS_ENTITY_AT_ENTITY(pedjimmy, pedBikeGuy, <<30,30,30>>)
		OR current_mission_stage = stage_lose_cops
			MISSION_FAILED("FM2_BGHIT")
		ENDIF
	ELSE
		IF (NOT IS_PED_INJURED(pedBikeGuy) AND IS_PED_FLEEING(pedBikeGuy) AND current_mission_stage < stage_bike_ride)
		OR fAimTime > 0.5
		OR bWAntedFail
			Mission_Failed("FM2_BGRUN")
		ELIF NOT IS_PED_INJURED(pedJimmy)
		AND	IS_PED_FLEEING(pedJimmy)
			PRINTLN("JIMMY SPOOK 1")
			Mission_Failed("FM2_JSPOOK")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTemp)
		vehTemp = NULL
	ENDIF
	
ENDPROC

BOOL bChairFrozen

PROC MANAGE_BIKE_SHOP()
	
	SWITCH iBikeShopStage
		CASE 0
			IF current_mission_stage < stage_bike_ride
			OR (current_mission_stage = stage_lose_cops AND eSavedStage < stage_bike_ride)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_bike_guy_pos) < 200*200
					#IF IS_DEBUG_BUILD
						PRINTLN("\n\nBike hire debug: WITHIN BIKE HIRE RANGE\n\n")
					#ENDIF
					iBikeShopStage++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			BOOL bAfterHire
			bAfterHire = FALSE
			IF (current_mission_stage > stage_hire_bike AND current_mission_stage != stage_lose_cops)
			OR (current_mission_stage = stage_lose_cops AND eSavedStage > stage_hire_bike)
				bAfterHire = TRUE
			ENDIF
			IF SETUP_BIKE_SHOP(bAfterHire)
				#IF IS_DEBUG_BUILD
					PRINTLN("\n\nBike hire debug: BIKE HIRE SET UP\n\n")
				#ENDIF
				fAimTime = 0.0
				iBikeShopStage++
			ENDIF
		BREAK
		CASE 2
		
			IF NOT bChairFrozen
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1105.2411, -1697.0247, 3.3678>>, 2, PROP_TABLE_03_CHR)
					OBJECT_INDEX objTemp
					objTemp = GET_CLOSEST_OBJECT_OF_TYPE(<<-1105.2411, -1697.0247, 3.3678>>, 2, PROP_TABLE_03_CHR)
					SET_ENTITY_PROOFS(objTemp, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					FREEZE_ENTITY_POSITION(objTemp, TRUE)
					bChairFrozen = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedBikeGuy)
				IF NOT IS_PED_FLEEING(pedBikeGuy)
				IF NOT IS_PED_USING_SCENARIO(pedBikeGuy, "PROP_HUMAN_SEAT_CHAIR")
						SET_ENTITY_COORDS(pedBikeGuy, v_bike_guy_pos)
					IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(v_bike_guy_pos, "PROP_HUMAN_SEAT_CHAIR", 2, TRUE)
							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedBikeGuy, <<-1105.2411, -1697.0247, 3.3678>>, 2)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			INT iTemp
			REPEAT COUNT_OF(vehBikes) iTemp
				IF DOES_ENTITY_EXIST(vehBikes[iTemp])
				AND IS_VEHICLE_DRIVEABLE(vehBikes[iTemp])
					IF current_mission_stage <= stage_hire_bike
						IF VDIST2(GET_ENTITY_COORDS(vehBikes[iTemp]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 9
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
						ENDIF
					ENDIF
					IF current_mission_stage < stage_hire_bike
						IF GET_VEHICLE_DOOR_LOCK_STATUS(vehBikes[iTemp]) != VEHICLELOCK_LOCKED
							SET_VEHICLE_DOORS_LOCKED(vehBikes[iTemp], VEHICLELOCK_LOCKED)
						ENDIF
					ELSE
						IF GET_VEHICLE_DOOR_LOCK_STATUS(vehBikes[iTemp]) = VEHICLELOCK_LOCKED
							SET_VEHICLE_DOORS_LOCKED(vehBikes[iTemp], VEHICLELOCK_UNLOCKED)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			CHECK_FOR_RENTAL_STAND_INTERFERENCE()
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_bike_guy_pos) > 210*210
				#IF IS_DEBUG_BUILD
					PRINTLN("\n\nBike hire debug: BIKE HIRE CLEANING UP\n\n")
				#ENDIF
				CLEANUP_BIKE_SHOP()
				iBikeShopStage = 0
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_VEHICLE(VEH_TYPE veh, VECTOR position, FLOAT heading)

	SWITCH veh
	
		CASE CAR
			IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
				WHILE NOT CREATE_PLAYER_VEHICLE(vehPlayerCar, CHAR_MICHAEL, position, heading)
					WAIT(0)
				ENDWHILE
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehPlayerCar, SC_DOOR_FRONT_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehPlayerCar, SC_DOOR_FRONT_RIGHT, FALSE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayerCar, TRUE)
				SET_VEHICLE_NAME_DEBUG(vehPlayerCar, "Mike's Car")
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_ENTITY_COORDS(vehPlayerCar, position)
					SET_ENTITY_HEADING(vehPlayerCar, heading)
				ENDIF
			ENDIF
		BREAK
		
		CASE SEA_SHARK
			IF NOT DOES_ENTITY_EXIST(vehPlayerSeashark)
				REQUEST_MODEL(SEASHARK)
				WHILE NOT HAS_MODEL_LOADED(SEASHARK)
					WAIT(0)
				ENDWHILE
				vehPlayerSeashark = CREATE_VEHICLE(SEASHARK, position, heading)
				SET_VEHICLE_NAME_DEBUG(vehPlayerSeashark, "Mike's Seashark")
				SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, FALSE, TRUE)
				SET_VEHICLE_COLOUR_COMBINATION(vehPlayerSeashark, 1)
				SET_VEH_RADIO_STATION(vehPlayerSeashark, "OFF")
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehPlayerSeashark, FAM2_FASTEST_SPEED)
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					SET_ENTITY_COORDS(vehPlayerSeashark, position)
					SET_ENTITY_HEADING(vehPlayerSeashark, heading)
				ENDIF
			ENDIF
		BREAK
		
		CASE JIMMY_BIKE
			IF NOT DOES_ENTITY_EXIST(vehJimmyBike)
//				REQUEST_MODEL(CRUISER)
//				WHILE NOT HAS_MODEL_LOADED(CRUISER)
//					WAIT(0)
//				ENDWHILE
				REQUEST_MODEL(CRUISER)
				WHILE NOT HAS_MODEL_LOADED(CRUISER)
					WAIT(0)
				ENDWHILE
				vehJimmyBike = CREATE_VEHICLE(CRUISER, position, heading)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehJimmyBike, FALSE)
				SET_VEHICLE_NAME_DEBUG(vehJimmyBike, "Jimmy's Bike")
				SET_VEHICLE_COLOUR_COMBINATION(vehJimmyBike, 0)
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehJimmyBike)
					SET_ENTITY_COORDS(vehJimmyBike, position)
					SET_ENTITY_HEADING(vehJimmyBike, heading)
				ENDIF
			ENDIF
		BREAK
		
		CASE BIKE
			IF NOT DOES_ENTITY_EXIST(vehPlayerBike)
				REQUEST_MODEL(CRUISER)
				WHILE NOT HAS_MODEL_LOADED(CRUISER)
					WAIT(0)
				ENDWHILE
				vehPlayerBike = CREATE_VEHICLE(CRUISER, position, heading)
				SET_VEHICLE_NAME_DEBUG(vehPlayerBike, "Mike's Bike")
				SET_ENTITY_PROOFS(vehPlayerBike, FALSE, TRUE, FALSE, FALSE,FALSE)
				SET_VEHICLE_TYRES_CAN_BURST(vehPlayerBike, FALSE)
				SET_VEHICLE_COLOUR_COMBINATION(vehPlayerBike, 1)
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
					SET_ENTITY_COORDS(vehPlayerBike, position)
					SET_ENTITY_HEADING(vehPlayerBike, heading)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_PED_GROUPED(PED_INDEX ped)
	IF IS_PED_GROUP_MEMBER(ped, GET_PLAYER_GROUP(PLAYER_ID()))
		IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped, <<50,50,50>>)
//			PRINTLN("GROUP CHECK 1 TRUE")
			//REMOVE_PED_FROM_GROUP(ped)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			OR GET_CAN_PED_BE_GRABBED_BY_SCRIPT(pedJImmy, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE)
				REMOVE_PED_FROM_GROUP(ped)
				#IF IS_DEBUG_BUILD
					//PRINTLN("REMOVE JIMMY FROM GROUP!!")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					//PRINTLN("GET_CAN_PED_BE_GRABBED_BY_SCRIPT, returned FALSE")
				#ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ELSE
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped, g_vAnyMeansLocate)
			SET_PED_AS_GROUP_MEMBER(ped, GET_PLAYER_GROUP(PLAYER_ID()))
		ENDIF
	ENDIF
	IF GET_CAN_PED_BE_GRABBED_BY_SCRIPT(ped, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE)
		//PRINTLN("GET_CAN_PED_BE_GRABBED_BY_SCRIPT(pedJImmy, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE)")
	ENDIF
	IF GET_CAN_PED_BE_GRABBED_BY_SCRIPT(ped, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE)
		//PRINTLN("GET_CAN_PED_BE_GRABBED_BY_SCRIPT(pedJImmy, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE)")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CHILD_ABUSE(PED_INDEX child)
	IF DOES_ENTITY_EXIST(child)
	AND NOT IS_PED_INJURED(child)
		//IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(child, PLAYER_PED_ID())
		IF IS_ENTITY_ON_FIRE(child)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_WATER_PED()
	IF NOT DOES_ENTITY_EXIST(pedBoatDancer[0])
		REQUEST_MODEL(CSB_PORNDUDES)
		IF HAS_MODEL_LOADED(CSB_PORNDUDES)
			pedBoatDancer[0] = CREATE_PED(PEDTYPE_MISSION, CSB_PORNDUDES, <<-2028.3362, -1026.3871, 0.0897>>, 222.4424)
			IF bWaterComponentsGrabbed
				INT iTemp
				REPEAT COUNT_OF(PED_COMPONENT) iTemp
					SET_PED_COMPONENT_VARIATION(pedBoatDancer[0], INT_TO_ENUM(PED_COMPONENT, iTemp), iWaterComponents[iTemp][0], iWaterComponents[iTemp][1])
				ENDREPEAT
			ENDIF
			SET_PED_DIES_IN_WATER(pedBoatDancer[0], FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedBoatDancer[0], CA_ALWAYS_FLEE, TRUE)
			SET_PED_LOD_MULTIPLIER(pedBoatDancer[0], 1.75)
//			SET_PED_RELATIONSHIP_GROUP_HASH(pedBoatDancer[0], rgChasers)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBoatDancer[0], TRUE)
			TASK_LOOK_AT_ENTITY(pedBoatDancer[0], PLAYER_PED_ID(), -1, SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_HIGH)
		ENDIF
	ENDIF
	RETURN DOES_ENTITY_EXIST(pedBoatDancer[0])
ENDFUNC

FUNC BOOL IS_SEASHARK_GROUNDED(VEHICLE_INDEX &veh, FLOAT &fGroundTime, BOOL &bGroundCheck)
	bGroundCheck = TRUE
	IF NOT IS_ENTITY_DEAD(veh)
		IF NOT IS_ENTITY_IN_WATER(veh)
		AND GET_ENTITY_SPEED(veh) < 2
			fGroundTime+=GET_FRAME_TIME()
			PRINTLN(fGroundTime)
			RETURN fGroundTime > 3.0
		ELSE
			fGroundTime = 0
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//-------------------------------------------------------------------

//------------------------------------------------------- Stage inits

PROC SET_ALL_SCRIPT_VARS_TO_DEFAULT()

	b_cutscene_request = FALSE
	in_car_conv_started = FALSE
	in_car_conv_on_hold = FALSE
	sConvResumeLabel = ""
	iCurrentDriveConv = 0
	bGoBackToJimmyPrinted = FALSE
	bGoBackToTracyPrinted = FALSE
	
	SETTIMERA(0)
	SETTIMERB(0)
	
ENDPROC

PROC REMOVE_ALL_SCRIPT_BLIPS
	REMOVE_BLIP(blipPlayerVeh)
	REMOVE_BLIP(blipPornBoat)
	REMOVE_BLIP(blipBuddy)
	REMOVE_BLIP(blipPlayerVeh)
	REMOVE_BLIP(blipDest)
ENDPROC

FUNC INT GET_PLAYABLE_STAGE_INDEX(MISSION_STAGE_FLAG stage)
	INT iTemp
	REPEAT COUNT_OF(mission_stages) iTemp
		IF mission_stages[iTemp] = stage
			RETURN iTemp
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC BOOL PRE_INIT_ASSETS(MISSION_STAGE_FLAG stage)

	INT iTemp
	MODEL_NAMES stage_models[10]
	REPEAT COUNT_OF(stage_models) iTemp
		stage_models[iTemp] = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	
	INT	stage_recs[3]
	REPEAT COUNT_OF(stage_recs) iTemp
		stage_recs[iTemp] = 0
	ENDREPEAT
	
	SWITCH stage
	
		CASE stage_intro
			//stage_models[0] = IG_JIMMYDISANTO
		BREAK
		
		CASE stage_drive_to_venice
			stage_models[0] = IG_JIMMYDISANTO
			stage_models[1] = TAILGATER
			stage_models[2] = CRUISER
			stage_models[3] = GET_NPC_VEH_MODEL(CHAR_TRACEY, VEHICLE_TYPE_CAR)
		BREAK
		
		CASE stage_hire_bike
			stage_models[0] = IG_JIMMYDISANTO
			stage_models[1] = TAILGATER
			stage_models[2] = U_M_M_BIKEHIRE_01
			stage_models[3] = CRUISER
			stage_recs[0] = i_jimmy_bike_rec
			stage_recs[1] = 1
		BREAK
		
		CASE stage_bike_ride
			stage_models[0] = IG_JIMMYDISANTO
			stage_models[1] = TAILGATER
			stage_models[2] = U_M_M_BIKEHIRE_01
			stage_models[3] = CRUISER
			stage_recs[0] = i_jimmy_bike_rec
			stage_recs[1] = 1
		BREAK
		
		CASE stage_telltale
			stage_models[0] = IG_JIMMYDISANTO
			stage_models[1] = CRUISER
		BREAK
		
		CASE stage_go_to_yacht
			stage_models[0] = IG_JIMMYDISANTO
			stage_models[1] = CRUISER
			stage_models[2] = SEASHARK
			stage_models[3] = A_M_Y_MUSCLBEAC_01
		BREAK
		
		CASE stage_rescue
			stage_models[0] = SEASHARK
			stage_models[1] = IG_TRACYDISANTO
		BREAK
		
		CASE stage_speedophile_chase
			stage_models[0] = SEASHARK
			stage_models[1] = IG_TRACYDISANTO
			stage_models[2] = CSB_PORNDUDES
			stage_models[3] = IG_JIMMYDISANTO
		BREAK
		
		CASE stage_drive_to_beach
			stage_models[0] = SEASHARK
			stage_models[1] = IG_TRACYDISANTO
			stage_models[2] = IG_JIMMYDISANTO
		BREAK
		
		CASE stage_finish
			stage_models[0] = SEASHARK
			stage_models[1] = IG_TRACYDISANTO
			stage_models[2] = IG_JIMMYDISANTO
		BREAK
		
	ENDSWITCH
	
	BOOL bLoaded = TRUE
	
	REPEAT COUNT_OF(stage_models) iTemp
		IF stage_models[iTemp] <> DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(stage_models[iTemp])
			IF NOT HAS_MODEL_LOADED(stage_models[iTemp])
				bLoaded = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(stage_recs) iTemp
		IF stage_recs[iTemp] <> 0 
			REQUEST_VEHICLE_RECORDING(stage_recs[iTemp], "fam2")
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(stage_recs[iTemp], "fam2")
				bLoaded = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bLoaded
	
ENDFUNC

STRUCT AMBIENT_BOAT_STRUCT
	VEHICLE_INDEX m_Veh
	PED_INDEX m_Ped
	INT m_iStage
	INT m_iRec
	VECTOR m_vTriggerPoint
	BOOL bOnRecording
ENDSTRUCT

AMBIENT_BOAT_STRUCT sMarinaBoats[2]

FUNC BOOL CREATE_AMBIENT_BOAT_FOR_CHASE(INT iBoatIndex, MODEL_NAMES paramBoatModel, VECTOR paramCoords)
	REQUEST_MODEL(paramBoatModel)
	REQUEST_MODEL(A_M_Y_GENSTREET_01)
	REQUEST_VEHICLE_RECORDING(iBoatIndex + 1, "fam2amb")
	IF HAS_MODEL_LOADED(paramBoatModel)
	AND HAS_MODEL_LOADED(A_M_Y_GENSTREET_01)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iBoatIndex + 1, "fam2amb")
		sMarinaBoats[iBoatIndex].m_Veh = CREATE_VEHICLE(paramBoatModel, paramCoords)
		sMarinaBoats[iBoatIndex].m_Ped = CREATE_PED_INSIDE_VEHICLE(sMarinaBoats[iBoatIndex].m_Veh, PEDTYPE_MISSION, A_M_Y_GENSTREET_01)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sMarinaBoats[iBoatIndex].m_Ped, TRUE)
		START_PLAYBACK_RECORDED_VEHICLE(sMarinaBoats[iBoatIndex].m_Veh, iBoatIndex+1, "fam2amb")
		SET_VEHICLE_DOORS_LOCKED(sMarinaBoats[iBoatIndex].m_Veh, VEHICLELOCK_LOCKED)
		SET_MODEL_AS_NO_LONGER_NEEDED(paramBoatModel)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_GENSTREET_01)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEANUP_AMBIENT_BOATS(BOOL bForceDelete = FALSE)
	INT iTemp
	REPEAT COUNT_OF(sMarinaBoats) iTemp
		IF DOES_ENTITY_EXIST(sMarinaBoats[iTemp].m_Ped)
			IF NOT bForceDelete
				SET_PED_AS_NO_LONGER_NEEDED(sMarinaBoats[iTemp].m_Ped)
			ELSE
				DELETE_PED(sMarinaBoats[iTemp].m_Ped)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(sMarinaBoats[iTemp].m_Veh)
			IF NOT bForceDelete
				SET_VEHICLE_AS_NO_LONGER_NEEDED(sMarinaBoats[iTemp].m_Veh)
			ELSE
				DELETE_VEHICLE(sMarinaBoats[iTemp].m_Veh)
			ENDIF
		ENDIF
		REMOVE_VEHICLE_RECORDING(iTemp+1, "fam2amb")
	ENDREPEAT
ENDPROC

STRUCT AMBIENT_BOAT_TRIGGER
	VECTOR m_vStart
	VECTOR m_vTriggerSize
	VECTOR m_vTarget
	FLOAT m_fHeading
ENDSTRUCT

FUNC AMBIENT_BOAT_TRIGGER GET_TRIGGER_INFO_FOR_AMBIENT_BOAT(INT iBoatIndex)
	AMBIENT_BOAT_TRIGGER sReturnTrigger	
	SWITCH iBoatIndex
		CASE 0
			sReturnTrigger.m_vStart = <<-935.5378, -1633.7369, 0.3306>>
			sReturnTrigger.m_vTriggerSize = <<62.750000,48.750000,11.000000>>
			sReturnTrigger.m_vTarget = <<-997.8688, -1383.0035, 0.4060>>
			sReturnTrigger.m_fHeading = 314.1291
		BREAK
		CASE 1
			sReturnTrigger.m_vStart = <<-928.3334, -1359.1943, 0.3531>>
			sReturnTrigger.m_vTriggerSize = <<28.250000,34.750000,11.000000>>
			sReturnTrigger.m_vTarget = <<-723.3589, -1350.0933, 0.3208>>
			sReturnTrigger.m_fHeading = 294.1011
		BREAK
	ENDSWITCH
	RETURN sReturnTrigger
ENDFUNC

PROC INIT_MARINA_BOATS()
	sMarinaBoats[0].m_vTriggerPoint = <<-857.031433,-1415.792114,1.034999>>
	sMarinaBoats[0].m_iStage = 0
	sMarinaBoats[1].m_vTriggerPoint = <<-1037.352417,-1272.711060,-6.625715>>
	sMarinaBoats[1].m_iStage = 0
ENDPROC

PROC MANAGE_AMBIENT_MARINA_BOATS()
	INT iTemp
	REPEAT COUNT_OF(sMarinaBoats) iTemp
		SWITCH sMarinaBoats[iTemp].m_iStage
			CASE 0
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), sMarinaBoats[iTemp].m_vTriggerPoint) < 50*50
					AMBIENT_BOAT_TRIGGER ambTrigger
					ambTrigger = GET_TRIGGER_INFO_FOR_AMBIENT_BOAT(iTemp)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), sMarinaBoats[iTemp].m_vTriggerPoint, ambTrigger.m_vTriggerSize)
						IF CREATE_AMBIENT_BOAT_FOR_CHASE(iTemp, SQUALO, ambTrigger.m_vStart)
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 8.0, REPLAY_IMPORTANCE_HIGH)
							sMarinaBoats[iTemp].m_iStage++
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_BOX(sMarinaBoats[iTemp].m_vTriggerPoint - ambTrigger.m_vTriggerSize, sMarinaBoats[iTemp].m_vTriggerPoint + ambTrigger.m_vTriggerSize, 1, 0, 255, 0)
						DRAW_DEBUG_SPHERE(ambTrigger.m_vStart, 2, 255, 0, 0)
						DRAW_DEBUG_LINE(sMarinaBoats[iTemp].m_vTriggerPoint + <<0,0,2>>, ambTrigger.m_vStart + <<0,0,2>>, 255, 0, 0)
						DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(iTemp), ambTrigger.m_vStart + <<0,0,1.8>>)
					#ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_SPHERE(sMarinaBoats[iTemp].m_vTriggerPoint, 1, 0, 255, 0)
					DRAW_DEBUG_LINE(sMarinaBoats[iTemp].m_vTriggerPoint, sMarinaBoats[iTemp].m_vTriggerPoint + <<0,0,2>>, 0, 255, 0)
					DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(iTemp), sMarinaBoats[iTemp].m_vTriggerPoint + <<0,0,1.8>>)
				#ENDIF
			BREAK
			CASE 1
				IF NOT IS_PED_INJURED(sMarinaBoats[iTemp].m_Ped)
				AND IS_VEHICLE_DRIVEABLE(sMarinaBoats[iTemp].m_Veh)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMarinaBoats[iTemp].m_Veh)
					AND NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(sMarinaBoats[iTemp].m_Veh)
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), sMarinaBoats[iTemp].m_Veh, <<5,5,5>>)
						OR (IS_VEHICLE_DRIVEABLE(vehChasers[0]) AND IS_ENTITY_AT_ENTITY(vehChasers[0], sMarinaBoats[iTemp].m_Veh, <<5,5,5>>))
						OR (IS_VEHICLE_DRIVEABLE(vehChasers[1]) AND IS_ENTITY_AT_ENTITY(vehChasers[1], sMarinaBoats[iTemp].m_Veh, <<5,5,5>>))
							SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(sMarinaBoats[iTemp].m_Veh, 5000, DRIVINGMODE_AVOIDCARS, TRUE)
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(sMarinaBoats[iTemp].m_Veh)
				AND DOES_ENTITY_EXIST(sMarinaBoats[iTemp].m_Ped)
				AND (	//Has ped finished driving to target
						(NOT IS_PED_INJURED(sMarinaBoats[iTemp].m_Ped) 
						AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMarinaBoats[iTemp].m_veh))
						//is the ped or the boat damaged
						OR IS_PED_INJURED(sMarinaBoats[iTemp].m_Ped) 
						OR NOT IS_VEHICLE_DRIVEABLE(sMarinaBoats[iTemp].m_Veh)
					)
					IF VDIST2(GET_ENTITY_COORDS(sMarinaBoats[iTemp].m_Ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100*100
					AND VDIST2(GET_ENTITY_COORDS(sMarinaBoats[iTemp].m_Veh), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100*100
						SET_PED_AS_NO_LONGER_NEEDED(sMarinaBoats[iTemp].m_Ped)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(sMarinaBoats[iTemp].m_Veh)	
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

PROC SET_WATER_CALM(BOOL bCalm, FLOAT iFade = 1.0)

	IF bCalm
	
//		WATER_OVERRIDE_FADE_IN(iFade)
//	
//		WATER_OVERRIDE_SET_OCEANNOISEMINAMPLITUDE(0.25)	
//		WATER_OVERRIDE_SET_OCEANWAVEAMPLITUDE(0.1)	
//		WATER_OVERRIDE_SET_OCEANWAVEMINAMPLITUDE(0.05)	
//		WATER_OVERRIDE_SET_OCEANWAVEMAXAMPLITUDE(0.15)
//		
//		WATER_OVERRIDE_SET_SHOREWAVEAMPLITUDE(0.2)	
//		WATER_OVERRIDE_SET_SHOREWAVEMINAMPLITUDE(0.05)	
//		WATER_OVERRIDE_SET_SHOREWAVEMAXAMPLITUDE(0.4)
//		
//		WATER_OVERRIDE_SET_RIPPLEBUMPINESS(0.25)	
//		WATER_OVERRIDE_SET_RIPPLEDISTURB(0.5)	
//		WATER_OVERRIDE_SET_RIPPLEMAXBUMPINESS(0.4)	
//		WATER_OVERRIDE_SET_RIPPLEMINBUMPINESS(0.125)
//		
//		WATER_OVERRIDE_SET_STRENGTH(0.5)
	
		WATER_OVERRIDE_FADE_IN(iFade)
	
		WATER_OVERRIDE_SET_OCEANNOISEMINAMPLITUDE(0.35)	
		WATER_OVERRIDE_SET_OCEANWAVEAMPLITUDE(0.3)	
		WATER_OVERRIDE_SET_OCEANWAVEMINAMPLITUDE(0.25)	
		WATER_OVERRIDE_SET_OCEANWAVEMAXAMPLITUDE(0.35)
		
		WATER_OVERRIDE_SET_SHOREWAVEAMPLITUDE(0.4)	
		WATER_OVERRIDE_SET_SHOREWAVEMINAMPLITUDE(0.25)	
		WATER_OVERRIDE_SET_SHOREWAVEMAXAMPLITUDE(0.55)
		
		WATER_OVERRIDE_SET_RIPPLEBUMPINESS(0.45)	
		WATER_OVERRIDE_SET_RIPPLEDISTURB(0.5)	
		WATER_OVERRIDE_SET_RIPPLEMAXBUMPINESS(0.6)	
		WATER_OVERRIDE_SET_RIPPLEMINBUMPINESS(0.25)
		
		WATER_OVERRIDE_SET_STRENGTH(0.75)
		
	ELSE
	
		WATER_OVERRIDE_FADE_OUT(iFade)
		
	ENDIF
	
ENDPROC

//╞═════════╡STAT WATCHING
FLOAT fDriveToVeniceCarHealth
//BOOL bDriveToVeniceTopSpeedCheck = FALSE
BOOL bBikeRidePlayerOnBike = FALSE
BOOL bBikeRideWindowOpen = FALSE
BOOL bBikeRideWindowClosed = FALSE
BOOL bBikeHireChoiceMade = FALSE
BOOL bGoToYachtWindowOpen = FALSE
BOOL bGoToYachtWindowClosed = FALSE
BOOL bLoseSeasharkWindowOpen = FALSE
BOOL bLoseSeasharkWindowClosed = FALSE
BOOL bStatsInitialised = FALSE

PROC WATCH_STATS_FOR_MISSION()
	IF NOT bStatsInitialised
		fDriveToVeniceCarHealth = 0
//		bDriveToVeniceTopSpeedCheck = FALSE
		bBikeRidePlayerOnBike = FALSE
		bBikeRideWindowOpen = FALSE
		bBikeRideWindowClosed = FALSE
		bBikeHireChoiceMade = FALSE
		bGoToYachtWindowOpen = FALSE
		bGoToYachtWindowClosed = FALSE
		bLoseSeasharkWindowOpen = FALSE
		bLoseSeasharkWindowClosed = FALSE
		bStatsInitialised = TRUE
	ENDIF
	SWITCH current_mission_stage
		CASE stage_drive_to_venice
			IF DOES_ENTITY_EXIST(vehPlayerCar)
			AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
//				IF NOT bDriveToVeniceTopSpeedCheck
//					IF GET_ENTITY_SPEED(vehPlayerCar) >= GET_VEHICLE_ESTIMATED_MAX_SPEED(vehPlayerCar)
//						PRINTLN("\n\n\n\nStat Increment: FAM2_MAX_CAR_SPEED\n\n\n\n\n")
//						INFORM_MISSION_STATS_OF_INCREMENT(FAM2_MAX_CAR_SPEED)
//						bDriveToVeniceTopSpeedCheck = TRUE
//					ENDIF
//				ELSE
//					IF GET_ENTITY_SPEED(vehPlayerCar) < GET_VEHICLE_ESTIMATED_MAX_SPEED(vehPlayerCar)
//						bDriveToVeniceTopSpeedCheck = FALSE
//					ENDIF
//				ENDIF
				IF fDriveToVeniceCarHealth > GET_ENTITY_HEALTH(vehPlayerCar)+GET_VEHICLE_ENGINE_HEALTH(vehPlayerCar)
					PRINTLN("\n\n\n\n\Stat Increment: FAM2_CAR_DAMAGE\n\n\n\n\n")
					INFORM_MISSION_STATS_OF_INCREMENT(FAM2_CAR_DAMAGE)
				ENDIF
				fDriveToVeniceCarHealth = GET_ENTITY_HEALTH(vehPlayerCar)+GET_VEHICLE_ENGINE_HEALTH(vehPlayerCar)
			ELSE
				fDriveToVeniceCarHealth = 0
			ENDIF
		BREAK
		CASE stage_hire_bike
			IF NOT bBikeHireChoiceMade
				IF DOES_ENTITY_EXIST(vehPlayerBike)
				AND IS_VEHICLE_DRIVEABLE(vehPlayerBike)
					IF IS_VEHICLE_MODEL(vehPlayerBike, TRIBIKE)
						INFORM_MISSION_STATS_OF_INCREMENT(FAM2_CHOICES, 0, TRUE)
						PRINTLN("\n\n\n\n\Stat Increment: FAM2_CHOICES TRIALS \n\n\n\n\n")
					ENDIF
					IF IS_VEHICLE_MODEL(vehPlayerBike, BMX)
						INFORM_MISSION_STATS_OF_INCREMENT(FAM2_CHOICES, 1, TRUE)
						PRINTLN("\n\n\n\n\Stat Increment: FAM2_CHOICES BMX \n\n\n\n\n")
					ENDIF
					IF IS_VEHICLE_MODEL(vehPlayerBike, CRUISER)
						INFORM_MISSION_STATS_OF_INCREMENT(FAM2_CHOICES, 2, TRUE)
						PRINTLN("\n\n\n\n\Stat Increment: FAM2_CHOICES CRUISER \n\n\n\n\n")
					ENDIF
					IF IS_VEHICLE_MODEL(vehPlayerBike, SCORCHER)
						INFORM_MISSION_STATS_OF_INCREMENT(FAM2_CHOICES, 3, TRUE)
						PRINTLN("\n\n\n\n\Stat Increment: FAM2_CHOICES SCORCHER \n\n\n\n\n")
					ENDIF
					bBikeHireChoiceMade = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE stage_bike_ride
			IF NOT bBikeRideWindowOpen
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM2_CYCLING_TIME)
				PRINTLN("\n\n\n\n\Stat Window open: FAM2_CYCLING_TIME \n\n\n\n\n")
				bBikeRideWindowOpen = TRUE
			ELSE
				IF NOT bBikeRideWindowClosed
					IF bPlayerDone
						PRINTLN("\n\n\n\n\Stat Window closed: FAM2_CYCLING_TIME \n\n\n\n\n")
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						bBikeRideWindowClosed = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
				IF NOT bBikeRidePlayerOnBike
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
						bBikeRidePlayerOnBike = TRUE
					ENDIF
				ELSE
					IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
						IF IS_PED_RAGDOLL(PLAYER_PED_ID())
							INFORM_MISSION_STATS_OF_INCREMENT(FAM2_FELL_OFF_BIKE)
							PRINTLN("\n\n\n\n\Stat Increment: FAM2_FELL_OFF_BIKE \n\n\n\n\n")
						ENDIF
						bBikeRidePlayerOnBike = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE stage_go_to_yacht
			IF NOT bGoToYachtWindowOpen
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM2_FAST_SWIM)
				PRINTLN("\n\n\n\n\Stat Window Open: FAM2_FAST_SWIM \n\n\n\n\n")
				bGoToYachtWindowOpen = TRUE
			ELSE
				IF NOT bGoToYachtWindowClosed
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2022.823975,-1045.761963,1.449759>>, <<-2018.303589,-1032.236328,4.478201>>, 8.000000)
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						PRINTLN("\n\n\n\n\Stat Window Closed: FAM2_FAST_SWIM \n\n\n\n\n")
						bGoToYachtWindowClosed = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE stage_speedophile_chase
			IF NOT bLoseSeasharkWindowOpen
				IF DOES_BLIP_EXIST(blipChasers[0])
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM2_LOSE_JETSKI)
					PRINTLN("\n\n\n\n\Stat Window Open: FAM2_LOSE_JETSKI \n\n\n\n\n")
					bLoseSeasharkWindowOpen = TRUE
				ENDIF
			ELSE
				IF NOT bLoseSeasharkWindowClosed
					IF (NOT DOES_ENTITY_EXIST(pedChasers[0]) AND NOT DOES_ENTITY_EXIST(vehChasers[0]))
					OR IS_PED_INJURED(pedChasers[0])
						PRINTLN("\n\n\n\n\Stat Window Closed: FAM2_LOSE_JETSKI \n\n\n\n\n")
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						bLoseSeasharkWindowClosed = TRUE
					ENDIF
				ENDIF
			ENDIF
		FALLTHRU
	ENDSWITCH
	
ENDPROC

//╞═════════╡END OF STAT WATCHING

PROC DO_GENERIC_MISSION_STAGE_INIT()

	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", TRUE)
	CLEAR_GPS_MULTI_ROUTE()
	
	IF current_mission_stage <> stage_speedophile_chase
		REMOVE_ANIM_DICT("missfam2_tracy_jetski")
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(0.6)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiMarina)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiBeach)
	
	IF (current_mission_stage <> stage_go_to_yacht AND current_mission_stage <> stage_drive_to_beach)
	OR b_skip
		CLEAR_PRINTS()
	ENDIF
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
	
	SET_ALL_SCRIPT_VARS_TO_DEFAULT()
	REMOVE_ALL_SCRIPT_BLIPS()
	INIT_TEXT()
	
	IF current_mission_stage < stage_rescue
	AND DOES_ENTITY_EXIST(pedTracy)
		DELETE_PED(pedTracy)
	ENDIF
	
	IF current_mission_stage = stage_bike_ride
	OR current_mission_stage = stage_speedophile_chase
		SET_MAX_WANTED_LEVEL(0)
	ELSE
		SET_MAX_WANTED_LEVEL(5)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objChair)
		DELETE_OBJECT(objChair)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedJimmy)
		IF IS_PED_GROUP_MEMBER(pedJimmy, GET_PLAYER_GROUP(PLAYER_ID()))
			REMOVE_PED_FROM_GROUP(pedJimmy)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedTracy)
		IF IS_PED_GROUP_MEMBER(pedTracy, GET_PLAYER_GROUP(PLAYER_ID()))
			REMOVE_PED_FROM_GROUP(pedTracy)
		ENDIF
	ENDIF
	
	IF b_skip
	
		INT iTemp
		FOR iTemp = 0 TO 1
			IF DOES_BLIP_EXIST(blipChasers[iTemp])
				REMOVE_BLIP(blipChasers[iTemp])
			ENDIF
			IF DOES_ENTITY_EXIST(pedChasers[iTemp])
				DELETE_PED(pedChasers[iTemp])
			ENDIF
			IF DOES_ENTITY_EXIST(vehChasers[iTemp])
				DELETE_VEHICLE(vehChasers[iTemp])
			ENDIF
		ENDFOR
		PRINTLN("DELETING CHASERS 3")
		
		CLEANUP_AMBIENT_BOATS(TRUE)
		
		SET_PED_WALLA_DENSITY(0.0, 0.0)
		
		IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_DRIVE_TO_BEACH")
			STOP_AUDIO_SCENE("FAMILY_2_DRIVE_TO_BEACH")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_BIKE_RACE_JIMMY")
			STOP_AUDIO_SCENE("FAMILY_2_BIKE_RACE_JIMMY")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_DIVE_INTO_WATER")
			STOP_AUDIO_SCENE("FAMILY_2_DIVE_INTO_WATER")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_SWIM_TO_YACHT")
			STOP_AUDIO_SCENE("FAMILY_2_SWIM_TO_YACHT")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_JETSKI_CHASE")
			STOP_AUDIO_SCENE("FAMILY_2_JETSKI_CHASE")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_JETSKI_GO_TO_BEACH")
			STOP_AUDIO_SCENE("FAMILY_2_JETSKI_GO_TO_BEACH")
		ENDIF
		
		TRIGGER_MUSIC_EVENT("FAM2_STOP")
		CLEAR_PRINTS()
		CLEAR_HELP()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		CLEANUP_BIKE_SHOP()
		
		IF IS_CUTSCENE_ACTIVE()
			STOP_CUTSCENE()
			REMOVE_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehJimmyBike)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
			ENDIF
		ENDIF
		
		IF current_mission_stage < stage_telltale
			IF DOES_ENTITY_EXIST(vehPlayerBike)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerBike)
			ENDIF
		ENDIF
		
		IF current_mission_stage != stage_intro
			END_SRL()
		ENDIF
		
		REMOVE_ANIM_DICT("missfam2ig_3")
		bStatsInitialised = FALSE
	ENDIF
	
	IF current_mission_stage > stage_go_to_yacht
		bShowStaminaHelp = FALSE
	ENDIF
	
	IF current_mission_stage <> stage_bike_ride
		REMOVE_ANIM_DICT("veh@bicycle@mountain_f@front@base")
	ENDIF
	
	WHILE NOT PRE_INIT_ASSETS(current_mission_stage)
		PRINTSTRING("WAITING ON STAGE ASSETS\n")
		WAIT(0)
	ENDWHILE
	
	IF NOT IS_PED_INJURED(pedJimmy)
		SET_PED_CONFIG_FLAG(pedJimmy, PCF_UseKinematicModeWhenStationary, TRUE)
	ENDIF
	
	IF current_mission_stage = stage_hire_bike
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "stage_hire_bike", FALSE, FALSE, NULL, TRUE)
	ELIF current_mission_stage = stage_bike_ride
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "stage_hire_bike", FALSE, FALSE, NULL, FALSE)
	ELIF current_mission_stage = stage_telltale
	OR current_mission_stage = stage_go_to_yacht
	OR current_mission_stage = stage_rescue
		PRINTLN("SETTING REPLAY AND GRABBING BIKE")
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "stage_go_to_yacht", FALSE, FALSE, NULL, TRUE)
	ELIF current_mission_stage = stage_speedophile_chase
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "stage_speedophile_chase")
	ELIF current_mission_stage = stage_drive_to_beach
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "stage_drive_to_beach", TRUE)
		IF IS_BIT_SET(iSectionsHit, ENUM_TO_INT(FAM2_CHASE_TUNNELS))
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "stage_drive_to_beach", TRUE)
		ENDIF
		IF IS_BIT_SET(iSectionsHit,  ENUM_TO_INT(FAM2_CHASE_CANALS))
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "stage_drive_to_beach", TRUE)
		ENDIF
		IF IS_BIT_SET(iSectionsHit,  ENUM_TO_INT(FAM2_CHASE_MARINA))
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "stage_drive_to_beach", TRUE)
		ENDIF
		IF IS_BIT_SET(iSectionsHit,  ENUM_TO_INT(FAM2_CHASE_LOSE_PEDS))
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8, "stage_drive_to_beach", TRUE)
		ENDIF
	ENDIF
	
	iStageProgress = 0
	iSavedStage = 0
	
	IF current_mission_stage > stage_bike_ride
		CLEANUP_BIKE_SHOP()
		REMOVE_VEHICLE_RECORDING(1, s_jimmy_bike_rec)
		REMOVE_VEHICLE_RECORDING(2, s_jimmy_bike_rec)
	ENDIF
	
ENDPROC

PROC SETUP_FOR_MISSION_STAGE()

	DO_GENERIC_MISSION_STAGE_INIT()
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF current_mission_stage >= stage_telltale
			IF NOT IS_IPL_ACTIVE("smboat")
				REQUEST_IPL("smBoat")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("smboat")
				REMOVE_IPL("smBoat")
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH current_mission_stage
	
		CASE stage_intro
			IF b_skip
				PRINTLN("SKIPPING IN INTRO")
				DO_QUICK_WARP(<<-803.2507, 172.9356, 71.8447>>, 108.7350)
				WHILE NOT SETUP_CLOTHES(FALSE, TRUE)
					WAIT(0)
				ENDWHILE
				REQUEST_WAYPOINT_RECORDING(sJimmyStairRoute)
				WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED(sJimmyStairRoute)
					WAIT(0)
				ENDWHILE
				WHILE NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(<<-803.2507, 172.9356, 71.8447>>))
					WAIT(0)
				ENDWHILE
				CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, << -809.6773, 171.1910, 75.7409 >>, 138.7408)
			ENDIF
		BREAK
		
		CASE stage_drive_to_venice
		
			bGoBackPrinted = FALSE
			iTimeGoBackPrinted = 0
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
			
			IF NOT IS_PED_INJURED(pedJimmy)
				SET_PED_AS_BUDDY(pedJimmy)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ASSISTED_MOVEMENT_REQUEST_ROUTE("Mansion_1")
			
			IF b_skip
			
				IF DOES_ENTITY_EXIST(vehPlayerCar)
					DELETE_VEHICLE(vehPlayerCar)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehTraceyCar)
					DELETE_VEHICLE(vehTraceyCar)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehMikesCar)
					DELETE_VEHICLE(vehMikesCar)
				ENDIF
				
				WHILE NOT CREATE_PLAYER_VEHICLE(vehMikesCar, CHAR_MICHAEL, v_car_start_coords, f_car_start, TRUE, VEHICLE_TYPE_CAR)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayerCar, TRUE)
				ENDIF
				
				WHILE NOT SETUP_CLOTHES(FALSE, TRUE)
					WAIT(0)
				ENDWHILE
				
				REQUEST_WAYPOINT_RECORDING(sJimmyStairRoute)
				WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED(sJimmyStairRoute)
					WAIT(0)
				ENDWHILE
				
				DO_QUICK_WARP(vPlayerStartCoords, fPlayerHeading)
				IF NOT IS_PED_INJURED(pedJimmy)
					CLEAR_PED_TASKS_IMMEDIATELY(pedJimmy)
				ENDIF
				CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, vJimmyCoords, fJimmyHeading)
				BOOL bJimmyStreamed
				bJimmyStreamed = FALSE
				WHILE NOT bJimmyStreamed
					IF NOT IS_PED_INJURED(pedJimmy)
						IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedJimmy)
							bJimmyStreamed = TRUE
						ENDIF
					ENDIF
					WAIT(0)
				ENDWHILE
				println("setting motion state here 3")
				IF NOT IS_REPLAY_BEING_SET_UP()
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
					SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
				APPLY_FORCE_TO_ENTITY(vehPlayerCar, APPLY_TYPE_FORCE, <<0,0,0.1>>, <<0,0,0>>,0, TRUE, TRUE, FALSE)
				SET_ENTITY_QUATERNION(vehPlayerCar, -0.0062,-0.0656,0.9462,0.3169)
			ENDIF
			SETTIMERA(0)
		BREAK
		
		CASE stage_hire_bike
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
			
			IF DOES_ENTITY_EXIST(vehMikesCar)
				IF (DOES_ENTITY_EXIST(vehPlayerCar) AND vehPlayerCar != vehMikesCar)
				OR NOT DOES_ENTITY_EXIST(vehPlayerCar)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehMikesCar)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehTraceyCar)
				IF (DOES_ENTITY_EXIST(vehPlayerCar) AND vehPlayerCar != vehTraceyCar)
				OR NOT DOES_ENTITY_EXIST(vehPlayerCar)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTraceyCar)
				ENDIF
			ENDIF
			
			IF b_Skip
			
				WHILE NOT SETUP_CLOTHES(FALSE, TRUE)
					WAIT(0)
				ENDWHILE
				
				IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
					SETUP_VEHICLE(CAR, v_car_stop, f_car_stop)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehPlayerCar)
					SET_ENTITY_COORDS(vehPlayerCar, v_car_stop)
					SET_ENTITY_HEADING(vehPlayerCar, f_car_stop)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerCar, VS_DRIVER)
					CREATE_BUDDY_IN_VEHICLE(pedJimmy, CHAR_JIMMY, vehPlayerCar)

					IF NOT IS_ENTITY_DEAD(vehPlayerCar)//[MF] Fix for assert in B* 1769983
					SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<-1072.7998, -1703.9235, 4.1380>>)
					SET_ENTITY_QUATERNION(vehPlayerCar, 0.0123, -0.0057, 0.9998, -0.0169)
				ENDIF
				ENDIF
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1081.7975, -1699.4921, 3.5179>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 91.1321)
				
				IF NOT IS_PED_INJURED(pedJimmy)
					SET_ENTITY_COORDS(pedJimmy, <<-1085.2488, -1703.1886, 3.4167>>)
					SET_ENTITY_HEADING(pedJimmy, 327.7033)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehPlayerBike)
					DELETE_VEHICLE(vehPlayerBike)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehJimmyBike)
					DELETE_VEHICLE(vehJimmyBike)
				ENDIF
				
				WHILE NOT SETUP_BIKE_SHOP()
					PRINTLN("SETTING UP BIKE SHOP")
					WAIT(0)
				ENDWHILE
				
				CLEAR_AREA(v_bike_guy_pos, 10, TRUE)
				
				DO_QUICK_WARP(<<-1081.7975, -1699.4921, 3.5179>>, 91.1321)				
				bShowGodText = TRUE
				bMovingStart = TRUE
				
			ENDIF
		BREAK
		
		CASE stage_bike_ride
			iBikeTutStage = 0
			SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
			PRINTSTRING("STARTING BIKE RIDE INIT\n")
			bGoBackPrinted = FALSE
			iTimeGoBackPrinted = 0
			IF b_skip
				WHILE NOT SETUP_BIKE_SHOP(TRUE)
					WAIT(0)
				ENDWHILE
				DO_QUICK_WARP(v_player_bike_start, f_player_bike_start)
				WHILE NOT SETUP_CLOTHES(FALSE, TRUE)
					WAIT(0)
				ENDWHILE
				SETUP_VEHICLE(BIKE, <<-1103.9978, -1691.6907, 3.3147>>, 255.7254)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
				SETUP_VEHICLE(JIMMY_BIKE, <<-1100.8270, -1695.7578, 3.3089>>, 245.1632)
				CREATE_BUDDY_IN_VEHICLE(pedJimmy, CHAR_JIMMY, vehJimmyBike, VS_DRIVER)
				
				REQUEST_VEHICLE_RECORDING(i_jimmy_bike_rec, s_jimmy_bike_rec)
				REQUEST_VEHICLE_RECORDING(1, s_jimmy_bike_rec)
				REQUEST_ANIM_DICT("veh@bicycle@mountain_f@front@base")
				WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(i_jimmy_bike_rec, s_jimmy_bike_rec)
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, s_jimmy_bike_rec)
				OR NOT HAS_ANIM_DICT_LOADED("veh@bicycle@mountain_f@front@base")
					WAIT(0)
				ENDWHILE
				BOOL bJimmyStreamed
				bJimmyStreamed = FALSE
				IF NOT IS_PED_INJURED(pedJimmy)
					WHILE NOT bJimmyStreamed
						WAIT(0)
						IF NOT IS_PED_INJURED(pedJimmy)
							IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedJimmy)
								SET_PED_IN_VEHICLE_CONTEXT(pedJimmy, GET_HASH_KEY("MISS_FAMILY2_JIMMY_BICYCLE"))
								bJimmyStreamed = TRUE
							ENDIF
						ENDIF
					ENDWHILE
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			in_car_conv_started = FALSE
			in_car_conv_on_hold = FALSE
			sConvResumeLabel = ""
		BREAK
		
		CASE stage_telltale
			IF b_Skip
				WHILE NOT SETUP_CLOTHES(FALSE, TRUE)
					WAIT(0)
				ENDWHILE
				#IF IS_DEBUG_BUILD
					IF bDebugMenuCutsceneSelection
						IF NOT IS_CUTSCENE_ACTIVE()
							IF NOT bJimmyWon
								REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_2_mcs_2", CS_SECTION_2 | CS_SECTION_3)
							ELSE
								REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_2_mcs_2", CS_SECTION_1 | CS_SECTION_3)
							ENDIF
						ENDIF
						bDebugMenuCutsceneSelection = FALSE
						WHILE CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							WAIT(0)
						ENDWHILE
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					ENDIF
				#ENDIF
				DO_QUICK_WARP(v_TT_jimmy_bike_mark, f_TT_jimmy_bike_mark)
				SETUP_VEHICLE(JIMMY_BIKE, v_TT_jimmy_bike_mark, f_TT_jimmy_bike_mark)
				SETUP_VEHICLE(BIKE, v_TT_player_bike_mark, f_TT_player_bike_mark)
				CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, <<-1859.0718, -1217.3331, 12.0174>>, 24.6128)
			ENDIF
		BREAK
		
		CASE stage_go_to_yacht

			SETUP_VEHICLE(SEA_SHARK, v_Seashark_start_coords, f_Seashark_heading)

			IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
				SET_BOAT_ANCHOR(vehPlayerSeashark, TRUE)
			ENDIF

			iLastJimmyShout = -1
			IF b_Skip
				WHILE NOT SETUP_CLOTHES(TRUE, TRUE)
					WAIT(0)
				ENDWHILE
				DO_QUICK_WARP(vPlayerOnPierPosition, fPlayerOnPierHeading)
				CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, <<-1859.0718, -1217.3331, 12.0174>>, 24.6128)
				SETUP_VEHICLE(JIMMY_BIKE, v_TT_jimmy_bike_mark, f_TT_jimmy_bike_mark)
				SETUP_VEHICLE(BIKE, v_TT_player_bike_mark, f_TT_player_bike_mark)
				IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
					SET_ENTITY_COORDS_NO_OFFSET(vehPlayerBike, <<-1867.5173, -1209.1207, 12.5769>>)
					SET_ENTITY_QUATERNION(vehPlayerBike, 0.0501, -0.0721, 0.4606, 0.8833)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehJimmyBike)
					SET_ENTITY_COORDS_NO_OFFSET(vehJimmyBike, <<-1858.6306, -1214.6611, 12.5761>>)
					SET_ENTITY_QUATERNION(vehJimmyBike, 0.0568, -0.0661, 0.6354, 0.7672)
				ENDIF
			ENDIF
		BREAK
		
		CASE stage_rescue
			WHILE NOT SETUP_CLOTHES(TRUE, TRUE)
				WAIT(0)
			ENDWHILE
			IF b_skip
				DO_QUICK_WARP(<< -2059.9348, -1026.1090, 9.7454 >>, 71.3421)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vehPlayerSeashark)
				SETUP_VEHICLE(SEA_SHARK, v_Seashark_start_coords, f_Seashark_heading)
			ENDIF
			WHILE NOT CREATE_CHASER(0, TRUE)
				WAIT(0)
			ENDWHILE
			WHILE NOT CREATE_CHASER(1, TRUE)
				WAIT(0)
			ENDWHILE
			WHILE NOT CREATE_MP3_DOCK()
				WAIT(0)
			ENDWHILE
			CREATE_BUDDY_ON_FOOT(pedTracy, CHAR_TRACEY, << -2059.9348, -1026.1090, 9.7454 >>, 71.3421)
			CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, v_jimmy_end_pos, f_jimmy_end_heading)
		BREAK
		
		CASE stage_speedophile_chase
		
			PRINTLN("REPLAY - Entered setup for stage speedophile chase")
			iStageProgress = 0
			
			sbiMarina = ADD_SCENARIO_BLOCKING_AREA(<<-919.216919,-1509.856934,-10.772810>> + <<232.000000,231.000000,13.250000>>, <<-919.216919,-1509.856934,-10.772810>> - <<232.000000,231.000000,13.250000>>)
			sbiBeach = ADD_SCENARIO_BLOCKING_AREA(<<-2055.107910,-888.841370,-12.186346>> + <<246.000000,250.000000,14.500000>>, <<-2055.107910,-888.841370,-12.186346>> + <<246.000000,250.000000,14.500000>>, FALSE, TRUE, FALSE, TRUE)
			
			INIT_MARINA_BOATS()
			
			IF b_skip
				SET_WATER_CALM(TRUE, 0.0)
				PRINTLN("REPLAY - Entered setup for skip to stage speedophile chase")
				WHILE NOT SETUP_CLOTHES(TRUE, TRUE)
					PRINTLN("REPLAY - Setting up clothes")
					WAIT(0)
				ENDWHILE
				DO_QUICK_WARP(v_Seashark_start_coords, f_Seashark_heading)
				// new chase change SETUP_VEHICLE(SEA_SHARK, v_Seashark_start_coords, f_Seashark_heading)
				SETUP_VEHICLE(SEA_SHARK, vSeasharkChaseStartCoords, fSeasharkChaseStartCoords)
				CREATE_BUDDY_IN_VEHICLE(pedTracy, CHAR_TRACEY, vehPlayerSeashark)
				BOOL bTraceyLoaded
				bTraceyLoaded = FALSE
				WHILE NOT bTraceyLoaded
					PRINTLN("REPLAY - Loading Tracey")
					IF NOT IS_PED_INJURED(pedTracy)
						IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedTracy)
							bTraceyLoaded = TRUE
							PRINTLN("REPLAY - Finished loading Tracey")
						ENDIF
					ENDIF
					WAIT(0)
				ENDWHILE
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, TRUE, TRUE)
				ENDIF
				SET_VEHICLE_FORWARD_SPEED(vehPlayerSeashark, 17)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				TRIGGER_MUSIC_EVENT("FAM2_CHASE_RT")
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
				SET_VEHICLE_DOORS_LOCKED(vehPlayerSeashark, VEHICLELOCK_UNLOCKED)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
				ENDIF
				IF NOT IS_PED_INJURED(pedTracy)
					IF NOT IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
						SET_PED_INTO_VEHICLE(pedTracy, vehPlayerSeashark, VS_FRONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
			CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, <<-1611.5950, -1121.0092, 1.5440>>, 42.8275)
		BREAK
		
		CASE stage_drive_to_beach
			iStageProgress = 0
			CLEANUP_AMBIENT_BOATS()
			
			SETTIMERA(0)
			IF b_skip
				WHILE NOT SETUP_CLOTHES(TRUE, TRUE)
					WAIT(0)
				ENDWHILE
				DO_QUICK_WARP(vMarinaFinish, fMarinaFinish)
				SETUP_VEHICLE(SEA_SHARK, vMarinaFinish, fMarinaFinish)
				CREATE_BUDDY_IN_VEHICLE(pedTracy, CHAR_TRACEY, vehPlayerSeashark)
				BOOL bTraceyLoaded
				bTraceyLoaded = FALSE
				WHILE NOT bTraceyLoaded
					IF NOT IS_PED_INJURED(pedTracy)
						IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedTracy)
							bTraceyLoaded = TRUE
						ENDIF
					ENDIF
					WAIT(0)
				ENDWHILE
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					ENDIF
					IF NOT IS_PED_INJURED(pedTracy)
						IF NOT IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
							SET_PED_INTO_VEHICLE(pedTracy, vehPlayerSeashark, VS_FRONT_RIGHT)
						ENDIF
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					SET_VEHICLE_FORWARD_SPEED(vehPlayerSeashark, 10)
					SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, TRUE, TRUE)
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_VEHICLE_FORWARD_SPEED(vehPlayerSeashark, 10)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)	
				SET_VEHICLE_DOORS_LOCKED(vehPlayerSeashark, VEHICLELOCK_UNLOCKED)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedJImmy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJimmy, TRUE)
			ENDIF
		BREAK
		
		CASE stage_finish
			IF b_skip
				PRINTLN("SKIPPED TO FINAL SCENE")
				WHILE NOT SETUP_CLOTHES(TRUE, TRUE)
					WAIT(0)
				ENDWHILE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1623.5543, -1132.1362, 0.8638>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 107.3556)
				IF NOT IS_REPLAY_BEING_SET_UP()
					NEW_LOAD_SCENE_START_SPHERE(<<-1624.9271, -1136.7485, 1.0450>>, 200, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
					SETTIMERA(0)
					INT iTimeNow
					iTimeNow = GET_GAME_TIMER()
					WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND (GET_GAME_TIMER() - iTimeNow) < 10000
						PRINTLN("LOADING AREA AROUND FINAL SCENE")
						WAIT(0)
					ENDWHILE
					NEW_LOAD_SCENE_STOP()
				ENDIF
				CREATE_BUDDY_ON_FOOT(pedTracy, CHAR_TRACEY, v_jimmy_end_pos + <<0,1,0>>, 0)
				CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, v_jimmy_end_pos, f_jimmy_end_heading)
				SETUP_VEHICLE(SEA_SHARK, v_Seashark_start_coords, f_Seashark_heading)
			ENDIF
		BREAK
		
		CASE go_to_end
		BREAK
		
	ENDSWITCH
	
	IF current_mission_stage = stage_speedophile_chase
		PRINTLN("REPLAY - current stage is speedo chase")
	ELSE
		PRINTLN("REPLAY - current stage is NOT speedo chase!!")
	ENDIF
	
	IF IS_REPLAY_BEING_SET_UP()
		SWITCH current_mission_stage
			CASE stage_drive_to_venice FALLTHRU
			CASE stage_hire_bike FALLTHRU
			CASE stage_telltale FALLTHRU
			CASE stage_rescue
				END_REPLAY_SETUP()
			BREAK
			CASE stage_go_to_yacht
				END_REPLAY_SETUP()
			BREAK
			CASE stage_speedophile_chase
				PRINTLN("REPLAY - Ending replay for seashark chase")
				END_REPLAY_SETUP(vehPlayerSeashark)
			BREAK
			CASE stage_drive_to_beach FALLTHRU
			CASE stage_finish
				END_REPLAY_SETUP(vehPlayerSeashark)
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("REPLAY - Replay is NOT being set up")
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedJimmy)
		IF NOT IS_PED_INJURED(pedJimmy)
			IF NOT DOES_ENTITY_EXIST(sSpeech.PedInfo[1].Index)
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedJimmy, "JIMMY")
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(sSpeech.PedInfo[1].Index)
			REMOVE_PED_FOR_DIALOGUE(sSpeech, 1)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTracy)
		IF NOT IS_PED_INJURED(pedTracy)
			IF sSpeech.PedInfo[2].Index = NULL
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, pedTracy, "TRACEY")
			ENDIF
		ENDIF
	ELSE
		IF sSpeech.PedInfo[2].Index <> NULL
			REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
		ENDIF
	ENDIF
	
	IF current_mission_stage <> stage_finish
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	CLEAR_PRINTS()
	IF b_skip
	AND current_mission_stage <> stage_speedophile_chase
		CLEAR_PED_WETNESS(PLAYER_PED_ID())
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF
	
	b_stage_ready = TRUE
	b_skip = FALSE
	
	PRINTSTRING("DELETE_VEHICLE_GEN_VEHICLES_IN_AREA\n")
	
	//DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(vJimmyCoords, 1000)
	
ENDPROC

PROC GO_TO_STAGE(MISSION_STAGE_FLAG stage)
	b_stage_ready = FALSE
	current_mission_stage = stage
ENDPROC

PROC CHECK_FOR_FAIL()
	IF DOES_ENTITY_EXIST(pedJimmy)
		IF IS_PED_INJURED(pedJimmy)
			MISSION_FAILED("FM2_JIMDED")
		ENDIF
		IF CHILD_ABUSE(pedJimmy)
			MISSION_FAILED("FM2_JIMHIT")
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedTracy)
		IF IS_PED_INJURED(pedTracy)
			MISSION_FAILED("FM2_TRADED")
		ENDIF
		IF CHILD_ABUSE(pedTracy)
			MISSION_FAILED("FM2_TRAHIT")
		ENDIF
	ENDIF
	IF current_mission_stage = stage_bike_ride
		BOOL bStuck = FALSE
		IF MISSION_VEHICLE_FAIL_CHECKS(vehPlayerBike, bStuck)
			PRINTLN("FAIL B")
			IF NOT bStuck
			MISSION_FAILED("FM2_BIKDED")
			ELSE	
				MISSION_FAILED("FM2_BIKSTU")
			ENDIF
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
			IF IS_VEHICLE_TYRE_BURST(vehPlayerBike, SC_WHEEL_BIKE_FRONT)
			OR IS_VEHICLE_TYRE_BURST(vehPlayerBike, SC_WHEEL_BIKE_REAR)
				PRINTLN("FAIL A")
				MISSION_FAILED("FM2_BIKDED")
			ENDIF
		ENDIF
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-2109.599121,-998.324280,-11.185677>>,<<167.000000,122.500000,32.250000>>)
			MISSION_FAILED("FM2_JIMAB")
		ENDIF
	ENDIF
	IF current_mission_stage = stage_drive_to_venice
		IF IS_BUDDY_ABANDONED(pedJimmy, 300)
			MISSION_FAILED("FM2_JIMAB")
		ENDIF
	ENDIF
	IF current_mission_stage = stage_speedophile_chase
		IF IS_BUDDY_ABANDONED(pedTracy, 100)
			MISSION_FAILED("FM2_TRAAB")
		ENDIF
	ENDIF
	IF current_mission_stage = stage_drive_to_beach
		IF IS_BUDDY_ABANDONED(pedTracy)
			MISSION_FAILED("FM2_TRAAB")
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedJimmy)
		IF ( IS_EXPLOSION_IN_AREA(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedJimmy)+<<25,25,25>>, GET_ENTITY_COORDS(pedJimmy)-<<25,25,25>>)
			AND NOT IS_EXPLOSION_IN_AREA(EXP_TAG_DIR_WATER_HYDRANT, GET_ENTITY_COORDS(pedJimmy)+<<25,25,25>>, GET_ENTITY_COORDS(pedJimmy)-<<25,25,25>>) )
		OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedJimmy), 25.0)
			PRINTLN("JIMMY SPOOK 2")
			Mission_Failed("FM2_JSPOOK")
		ENDIF
	ENDIF
ENDPROC

PROC BLIP_MANAGER(VEHICLE_INDEX veh, VECTOR destination, BOOL bBike = FALSE)

	IF IS_VEHICLE_DRIVEABLE(veh)
	AND DOES_ENTITY_EXIST(veh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
			IF NOT DOES_BLIP_EXIST(blipPlayerVeh)
				CLEAR_PRINTS()
				IF bBike
					IF bBeenOnBike
					PRINT_GOD_TEXT(gtGetBackInBike)
				ENDIF
				ENDIF
				blipPlayerVeh = CREATE_BLIP_FOR_VEHICLE(veh)
			ENDIF
			IF DOES_BLIP_EXIST(blipDest)
				REMOVE_BLIP(blipDest)
			ENDIF
		ELSE
			
			bBeenOnBike = TRUE
		
			IF NOT DOES_BLIP_EXIST(blipDest)
				CLEAR_PRINTS()
				blipDest = CREATE_BLIP_FOR_COORD(destination, TRUE)
			ENDIF
			IF DOES_BLIP_EXIST(blipPlayerVeh)
				REMOVE_BLIP(blipPlayerVeh)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC BLIP_MANAGER_FOR_COORD(VEHICLE_INDEX &veh, BLIP_INDEX &vehBlip, VECTOR coord, BLIP_INDEX &coordBlip, TEXT_HOLDER &getBackIn)

	IF IS_VEHICLE_DRIVEABLE(veh)
	AND DOES_ENTITY_EXIST(veh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
			IF NOT DOES_BLIP_EXIST(vehBlip)
				CLEAR_PRINTS()
				IF NOT getBackIn.played
					iTimeGetBackOnPrinted = GET_GAME_TIMER()
					PRINT_GOD_TEXT(getBackIn)
				ENDIF
				vehBlip = CREATE_BLIP_FOR_VEHICLE(veh)
			ENDIF
			IF DOES_BLIP_EXIST(coordBlip)
				REMOVE_BLIP(coordBlip)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(coordBlip)
				coordBlip = CREATE_BLIP_FOR_COORD(coord, TRUE)
			ENDIF
			IF DOES_BLIP_EXIST(vehBlip)
				CLEAR_PRINTS()
				REMOVE_BLIP(vehBlip)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//╞═════════╡MISSION STAGE PROCEDURES

PROC FAM2_INITIALISE()

//	bClothesHaveChanged = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_01_SC	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_02_GS	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_03_DT	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_04_CS	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_05_GSD	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_06_VC	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_L_07_PB	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_M_01_SM	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_M_03_H		, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_M_04_HW	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_M_05_GOH	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_H_01_BH	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_H_02_B		, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_H_03_MW	, TRUE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CLOTHES_SHOP_A_01_VB	, TRUE)
	
	PRE_INIT_ASSETS(stage_intro)
	
	#IF IS_DEBUG_BUILD
		skipMenuOptions[0].sTxtLabel = "family_2_int"
		skipMenuOptions[1].sTxtLabel = "Drive to Venice Beach"
		skipMenuOptions[2].sTxtLabel = "hire bikes"
		skipMenuOptions[3].sTxtLabel = "Bike ride"
		skipMenuOptions[4].sTxtLabel = "family_2_mcs_2 jimmy wins"
		skipMenuOptions[5].sTxtLabel = "family_2_mcs_2 mike wins"
		skipMenuOptions[6].sTxtLabel = "Swim to the yacht"
		skipMenuOptions[7].sTxtLabel = "family_2_mcs_3"
		skipMenuOptions[8].sTxtLabel = "Speedophile chase"
		skipMenuOptions[9].sTxtLabel = "Drive the Speedophile back to the beach"
		skipMenuOptions[10].sTxtLabel = "family_2_mcs_4"
	#ENDIF
	
	mission_stages[0] = stage_intro
	mission_stages[1] = stage_drive_to_venice
	mission_stages[2] = stage_hire_bike
	mission_stages[3] = stage_bike_ride
	mission_stages[4] = stage_telltale
	mission_stages[5] = stage_telltale
	mission_stages[6] = stage_go_to_yacht
	mission_stages[7] = stage_rescue 
	mission_stages[8] = stage_speedophile_chase
	mission_stages[9] = stage_drive_to_beach 
	mission_stages[10] = stage_finish
	
	REQUEST_ADDITIONAL_TEXT("FAMILY2", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT(sConvBlock, MISSION_DIALOGUE_TEXT_SLOT)
	REQUEST_WAYPOINT_RECORDING("fam2Stairs")
	
	DISABLE_TAXI_HAILING(TRUE)
	
	INIT_TEXT()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
	
		IF Is_Replay_In_Progress()
		
			REMOVE_CUTSCENE()
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, DEFAULT, DEFAULT, TRUE, FALSE)
			
			FADE_OUT()
			
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			
//			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
			
			BOOL bToCut
			
			INT iStage = GET_REPLAY_MID_MISSION_STAGE()
			
			IF g_bShitskipAccepted = TRUE
				IF iStage = 1
				OR iStage = 2
					bToCut = TRUE
				ENDIF
				IF iStage < 4
					IF iStage = 3
						iStage = 8
					ELSE
						iStage++
					ENDIF
				ELSE
					iStage=9
				ENDIF
			ENDIF
			
			IF iStage > 8
			
				WHILE NOT SETUP_CLOTHES(TRUE, TRUE)
					WAIT(0)
				ENDWHILE
				
				vReplayCoords = <<-1623.5543, -1132.1362, 0.8638>>
				fReplayHeading = 107.3556
				
				REQUEST_MODEL(SEASHARK)
				
				WHILE NOT HAS_MODEL_LOADED(SEASHARK)
					WAIT(0)
				ENDWHILE
				
				vehPlayerSeashark = CREATE_VEHICLE(SEASHARK, <<-1490.0714, -1406.3827, 1.1279>>, -94.2926)
				SET_ENTITY_COORDS_NO_OFFSET(vehPlayerSeashark, <<-1490.0714, -1406.3827, 1.1279>>)
				SET_ENTITY_ROTATION(vehPlayerSeashark, <<2.7893, 9.6258, -94.2926>>)
				SET_ENTITY_QUATERNION(vehPlayerSeashark, -0.0780, -0.0393, 0.7289, -0.6790)
				APPLY_FORCE_TO_ENTITY(vehPlayerSeashark, APPLY_TYPE_FORCE, <<0,0,-0.01>>, <<0,0,0>>, 0, TRUE, TRUE, FALSE)
				
				GO_TO_STAGE(stage_finish)
				
				b_skip = TRUE
				
			ELSE
						
				SWITCH iStage
					CASE 0
						bReplaySetupRequired = TRUE
						GO_TO_STAGE(stage_drive_to_venice)
						CLEAR_AREA(v_car_start_coords, 100, TRUE) PRINTLN(" \n\n\nCLEARING AREA AROUND START\n\n\n")
						vReplayCoords = vPlayerStartCoords
						fReplayHeading = fPlayerHeading
					BREAK
					CASE 1
						bReplaySetupRequired = TRUE
						CREATE_VEHICLE_FOR_REPLAY(vehPlayerCar, v_car_stop, f_car_stop, FALSE, FALSE, FALSE, FALSE, TRUE)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), v_bike_guy_pos + <<1,1,0>>)
						CLEAR_AREA(v_bike_guy_pos, 100, TRUE)
						WHILE NOT SETUP_BIKE_SHOP()
							WAIT(0)
						ENDWHILE
						GO_TO_STAGE(stage_hire_bike)
						vReplayCoords = <<-1081.7975, -1699.4921, 3.5179>>
						fReplayHeading = 91.1321
					BREAK
					CASE 2
						bReplaySetupRequired = TRUE
						IF bToCut
							vReplayCoords = v_TT_jimmy_bike_mark
							fReplayHeading = f_TT_jimmy_bike_mark
							REQUEST_IPL("smBoat")
							WHILE NOT IS_IPL_ACTIVE("smboat")
								WAIT(0)
							ENDWHILE
							GO_TO_STAGE(stage_telltale)
						ELSE
							IF GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = SCORCHER
							OR GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = BMX
							OR GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = CRUISER
							OR GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = TRIBIKE
							OR GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = TRIBIKE2
							OR GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = TRIBIKE3
								CREATE_VEHICLE_FOR_REPLAY(vehPlayerBike, v_TT_player_bike_mark, f_TT_player_bike_mark)
							ENDIF
							vReplayCoords = vPlayerOnPierPosition
							fReplayHeading = fPlayerOnPierHeading
							GO_TO_STAGE(stage_go_to_yacht)
						ENDIF
					BREAK
					CASE 3
						bReplaySetupRequired = TRUE
						CLEAR_AREA_OF_VEHICLES(v_Seashark_start_coords, 100, TRUE)
						CLEAR_AREA_OF_PEDS(v_Seashark_start_coords, 100)
						IF bToCut
							vReplayCoords = <<-2018.7909, -1033.8073, 1.4462>>
							fReplayHeading = 71.2179
							GO_TO_STAGE(stage_rescue)
						ELSE
							PRINTLN("REPLAY - Skipping to speedo chase")
							vReplayCoords = vSeasharkChaseStartCoords
							fReplayHeading = fSeasharkChaseStartCoords
							GO_TO_STAGE(stage_speedophile_chase)
						ENDIF
					BREAK
					CASE 4
						vMarinaFinish = <<-1937.5155, -1031.9191, 0>>
						fMarinaFinish = 288.2051
						SET_BIT(iSectionsHit, ENUM_TO_INT(FAM2_CHASE_OPEN_SEA))
						CLEAR_AREA(vMarinaFinish, 500, TRUE)
						GO_TO_STAGE(stage_drive_to_beach)
					BREAK
					CASE 5
						vMarinaFinish = <<-1693.7485, -936.2038, 0>>
						fMarinaFinish = 293.6241
						SET_BIT(iSectionsHit, ENUM_TO_INT(FAM2_CHASE_TUNNELS))
						CLEAR_AREA(vMarinaFinish, 500, TRUE)
						GO_TO_STAGE(stage_drive_to_beach)
					BREAK
					CASE 6
						vMarinaFinish = <<-1198.6477, -992.5767, 0>>
						fMarinaFinish = 209.2130
						SET_BIT(iSectionsHit, ENUM_TO_INT(FAM2_CHASE_CANALS))
						CLEAR_AREA(vMarinaFinish, 500, TRUE)
						GO_TO_STAGE(stage_drive_to_beach)
					BREAK
					CASE 7
						vMarinaFinish = <<-966.1338, -1365.7760, 0>>
						fMarinaFinish = 279.4431
						SET_BIT(iSectionsHit, ENUM_TO_INT(FAM2_CHASE_MARINA))
						CLEAR_AREA(vMarinaFinish, 500, TRUE)
						GO_TO_STAGE(stage_drive_to_beach)
					BREAK
					CASE 8
						vMarinaFinish = <<-832.9591, -1506.5366, 0>>
						fMarinaFinish = 151.4369
						SET_BIT(iSectionsHit, ENUM_TO_INT(FAM2_CHASE_LOSE_PEDS))
						CLEAR_AREA(vMarinaFinish, 500, TRUE)
						GO_TO_STAGE(stage_drive_to_beach)
					BREAK
				ENDSWITCH
				
				IF istage >= 4
					bReplaySetupRequired = TRUE
					vReplayCoords 	= vMarinaFinish
					fReplayHeading 	= fMarinaFinish
				ENDIF
				
				b_skip = TRUE
				
			ENDIF
			
			PRINTLN("REPLAY - Starting replay setup for speedo chase")
			START_REPLAY_SETUP(vReplayCoords, fReplayHeading, FALSE)
			
		ENDIF
		
		CLEAR_PED_WETNESS(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), FAM2_DAMAGE)
	ENDIF
	
	DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
	
	CREATE_FORCED_OBJECT(<<-1105.2411, -1697.0247, 3.3678>>, 2, PROP_TABLE_03_CHR, TRUE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BMX, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(U_M_M_BIKEHIRE_01, TRUE)
	SET_CREATE_RANDOM_COPS(FALSE)
	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
	ASSISTED_MOVEMENT_REQUEST_ROUTE("Family5d")
	
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
	SET_WANTED_LEVEL_MULTIPLIER(0.6)
	
//	SET_ROADS_IN_ANGLED_AREA( <<-1179.264771,-1776.414307,2.908462>>, <<-1031.640991,-1654.376465,15.983815>>, 20.000000, FALSE, FALSE)
	SET_ROADS_IN_AREA(<<-1057.899048,-1682.938599,5.785122>> + <<29.000000,30.000000,4.000000>>, <<-1057.899048,-1682.938599,5.785122>> - <<29.000000,30.000000,4.000000>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1112.912964,-1688.035034,6.373647>> + <<22.750000,24.750000,3.500000>>, <<-1112.912964,-1688.035034,6.373647>> - <<22.750000,24.750000,3.500000>>, FALSE)
	
	ADD_RELATIONSHIP_GROUP("chasing_peds", rgChasers)
	ADD_RELATIONSHIP_GROUP("buddies", rghBuds)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgChasers, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, rgChasers)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, rgChasers)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgChasers, RELGROUPHASH_COP)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, rghBuds, rgChasers)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, rgChasers, rghBuds)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghBuds, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, rghBuds)
	
	IF IS_REPEAT_PLAY_ACTIVE()
	
		PRINTLN("FAMILY 2 Triggered repeat play")
		
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
		
		WHILE NOT SETUP_CLOTHES(FALSE, TRUE)
			PRINTLN("SETTING MIKE'S CLOTHES FOR REPEAT PLAY")
			WAIT(0)
		ENDWHILE
		
		WHILE NOT CREATE_JIMMY(vJimmyCoords, fJimmyHeading)
			PRINTLN("CREATING JIMMY FOR REPEAT PLAY")
			WAIT(0)
		ENDWHILE
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			GO_TO_STAGE(stage_intro)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-803.2507, 172.9356, 71.8447>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 108.7350)
			SET_CLOCK_TIME(10,0,0)
		ENDIF
		
		REMOVE_CUTSCENE()
		
	ENDIF
	
ENDPROC

BOOL bStartConv
INT iLastWantedLine
BOOL bLostConv

PROC LOSE_THE_COPS()

	IF current_mission_stage <> STAGE_LOSE_COPS
		eSavedStage = current_mission_stage
		current_mission_stage = STAGE_LOSE_COPS
		IF DOES_BLIP_EXIST(blipDest)
			REMOVE_BLIP(blipDest)
		ENDIF
		IF DOES_BLIP_EXIST(blipPornBoat)
			REMOVE_BLIP(blipPornBoat)
		ENDIF
		IF DOES_BLIP_EXIST(blipBuddy)
			REMOVE_BLIP(blipBuddy)
		ENDIF
		IF DOES_BLIP_EXIST(blipPlayerVeh)
			REMOVE_BLIP(blipPlayerVeh)
		ENDIF
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT in_car_conv_on_hold
				sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
				KILL_FACE_TO_FACE_CONVERSATION()
				in_car_conv_on_hold = TRUE
				bStartConv = FALSE
				bLostConv = FALSE
				iLastWantedLine = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
	IF eSavedStage = stage_drive_to_venice
	
		CHECK_FOR_RENTAL_STAND_INTERFERENCE()
		IF NOT IS_PED_INJURED(pedJimmy)
			
			
			IF VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 25*25
				IF NOT bStartConv
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_WANTED", CONV_PRIORITY_HIGH)
						iLastWantedLine = GET_GAME_TIMER()
						bStartConv = TRUE
					ENDIF
				ELSE
					IF GET_GAME_TIMER() - iLastWantedLine > 15000
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_LOSE", CONV_PRIORITY_HIGH)
							iLastWantedLine = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PED_GROUPED(pedJimmy)
				IF DOES_BLIP_EXIST(blipBuddy)
					REMOVE_BLIP(blipBuddy)
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipBuddy)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					blipBuddy = CREATE_BLIP_FOR_PED(pedJimmy)
				ELSE
					IF NOT bGoBackToJimmyPrinted
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						PRINT("FM2_JIMGB", DEFAULT_GOD_TEXT_TIME, 1)
						bGoBackToJimmyPrinted = TRUE
					ENDIF
					IF IS_BUDDY_ABANDONED(pedJimmy, 300)
						MISSION_FAILED("FM2_JIMAB")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELIF eSavedStage = stage_drive_to_beach
	
		IF NOT IS_PED_INJURED(pedTracy)
			IF IS_PED_GROUPED(pedTracy)
				IF DOES_BLIP_EXIST(blipBuddy)
					REMOVE_BLIP(blipBuddy)
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipBuddy)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					blipBuddy = CREATE_BLIP_FOR_PED(pedTracy)
				ELSE
					IF NOT bGoBackToTracyPrinted
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						PRINT("FM2_TRAGB", DEFAULT_GOD_TEXT_TIME, 1)
						bGoBackToTracyPrinted = TRUE
					ENDIF
					IF IS_BUDDY_ABANDONED(pedTracy, 300)
						MISSION_FAILED("FM2_TRAAB")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bLostConv
			bLostConv = TRUE
		ENDIF
		
	ELSE
	
		IF NOT bLostConv
			bLostConv = TRUE
		ENDIF
		
	ENDIF
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINT_GOD_TEXT(gtLoseCops)
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
	
		IF NOT bLostConv
			IF eSavedStage = stage_drive_to_venice
				IF NOT IS_PED_INJURED(pedJimmy)
					IF VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 25*25
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_LOSTCOP", CONV_PRIORITY_HIGH)
							bLostConv = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			current_mission_stage = eSavedStage
			gtLoseCops.played = FALSE
			CLEAR_PRINTS()
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
	
	PROC JSKIP()
		SWITCH current_mission_stage
			CASE stage_go_to_yacht
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -2011.6843, -1043.1763, -0.6223 >>) > 20.0
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -2002.1965, -1057.2301, -0.5896 >> )
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 40.3391)
				ELSE
					GO_TO_STAGE(stage_rescue)
					b_skip = TRUE
				ENDIF
			BREAK
			DEFAULT
				IF GET_PLAYABLE_STAGE_INDEX(current_mission_stage) >= 0
				AND  GET_PLAYABLE_STAGE_INDEX(current_mission_stage) < (NUM_OF_SELECTABLE_STAGES -1)
					GO_TO_STAGE(mission_stages[GET_PLAYABLE_STAGE_INDEX(current_mission_stage)+1])
					b_skip = TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDPROC
	
	PROC PSKIP()
		IF GET_PLAYABLE_STAGE_INDEX(current_mission_stage) > 0
		AND  GET_PLAYABLE_STAGE_INDEX(current_mission_stage) < NUM_OF_SELECTABLE_STAGES
			GO_TO_STAGE(mission_stages[GET_PLAYABLE_STAGE_INDEX(current_mission_stage)-1])
			b_skip = TRUE
		ENDIF
	ENDPROC
	
	PROC DEBUG_CUTSCENE_CONTROL(BOOL bForceEnd = FALSE)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
			STOP_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		OR bForceEnd
			
			STOP_CUTSCENE()
			INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
			WHILE IS_CUTSCENE_ACTIVE()
				PRINTLN("STUCK HERE")
				WAIT(0)
			ENDWHILE
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			IF NOT bForceEnd
				IF current_mission_stage > stage_intro
					PSKIP()
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

INT iRadioConvStage = 0
INT iPedsKilled = 0
BOOL bRadioChat = FALSE
BOOL bMeleeFlag

PROC MANAGE_DRIVE_DIALOGUE()

	BOOL bCanPlay
	
	IF NOT IS_PED_IN_ANY_VEHICLE(pedJimmy)
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID()))<15*15
	AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<15,15,1>>)
	AND NOT IS_PLAYER_IN_ANY_SHOP()
		bCanPlay = TRUE
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
	AND IS_PED_IN_VEHICLE(pedJimmy, vehPlayerCar)
	AND NOT IS_PLAYER_IN_ANY_SHOP()
		bCanPlay = TRUE
	ENDIF
	
	IF NOT bRadioChat
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
		AND IS_PED_IN_VEHICLE(pedJimmy, vehPlayerCar)
			IF iRadioConvStage = 0
				IF ARE_STRINGS_EQUAL(GET_RADIO_STATION_NAME(GET_PLAYER_RADIO_STATION_INDEX()), "RADIO_01_CLASS_ROCK")
					fJazzTime += GET_FRAME_TIME()
					PRINTLN("ON JAZZ STATION")
				ENDIF
				IF fJazzTime > 7
					bRadioChat = TRUE
				ENDIF
			ELIF iRadioConvStage = 4
				IF GET_PLAYER_RADIO_STATION_INDEX() <> 8
					iRadioConvStage++
					bRadioChat = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		OR (IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
		OR (IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND NOT IS_PED_IN_VEHICLE(pedJimmy, vehPlayerCar))
			bRadioChat = FALSE
		ENDIF
	ENDIF
	
	IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedJimmy)) < 15
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
				IF NOT bMeleeFlag
					IF GET_GAME_TIMER() - iLastJimmyShout > 7500
						bMeleeFlag = TRUE
						bCanPlay = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bMeleeFlag
		bCanPlay = FALSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			PRINTLN("JIMMY MELEE LINE!")
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedTracy, "GENERIC_SHOCKED_MED", "Jimmy", SPEECH_PARAMS_FORCE)
			iLastMeleeShout = GET_GAME_TIMER()
			bMeleeFlag = FALSE
		ENDIF
	ENDIF
	
	PRINTLN("iCurrentDriveConv = ", iCurrentDriveConv, ", iRadioConvStage = ", iRadioConvStage, " sConvResumeLabel = ", sConvResumeLabel, " in_car_conv_started = ", in_car_conv_started, " in_car_conv_on_hold = ", in_car_conv_on_hold)
	
	IF NOT bRadioChat
	
		IF iCurrentDriveConv < 7
		AND NOT ARE_STRINGS_EQUAL(sConvResumeLabel, "FAM2_DRIV2")
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			AND GET_PED_IN_VEHICLE_SEAT(vehPlayerCar) = PLAYER_PED_ID()
				IF GET_ENTITY_SPEED(vehPlayerCar) > 30
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehPlayerCar)
						KILL_FACE_TO_FACE_CONVERSATION()
						in_car_conv_on_hold = FALSE
						in_car_conv_started = FALSE
						sConvResumeLabel = ""
						iCurrentDriveConv = 7
					ENDIF
				ENDIF
				IF g_MissionStatTrackingArray[5].ivalue - iPedsKilled > 3
					KILL_FACE_TO_FACE_CONVERSATION()
					in_car_conv_on_hold = FALSE
					in_car_conv_started = FALSE
					sConvResumeLabel = ""
					iCurrentDriveConv = 7
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH iCurrentDriveConv
		
			CASE 0
				IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_BEACH", CONV_PRIORITY_HIGH)
					
					REPLAY_RECORD_BACK_FOR_TIME(1.0, 7.0)
					
					fJazzTime = 0.0
					iPedsKilled = g_MissionStatTrackingArray[5].ivalue
					iCurrentDriveConv++
					in_car_conv_on_hold = FALSE
					in_car_conv_started = FALSE
					iLastConvFinish = GET_GAME_TIMER()
					sConvResumeLabel = ""
				ENDIF
			BREAK
			
			CASE 1
				IF (GET_GAME_TIMER() - iLastConvFinish) > 2000
					IF NOT DOES_BLIP_EXIST(blipDest)
						blipDest = CREATE_BLIP_FOR_COORD(v_bike_guy_pos)
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINT("FM2_BK2BIKE", DEFAULT_GOD_TEXT_TIME, 1)
							iLastConvFinish = GET_GAME_TIMER()
							iCurrentDriveConv++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF ((GET_GAME_TIMER() - iLastConvFinish) > 2000 AND GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				OR GET_GAME_TIMER() - iLastConvFinish > 6000
					iCurrentDriveConv++
				ENDIF
			BREAK
			
			CASE 3
				IF IS_REPLAY_IN_PROGRESS()
					MANAGE_CONVERSATION("FAM2_WALK", bCanPlay)
				ELSE
					MANAGE_CONVERSATION("FAM2_WALK2", bCanPlay)
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT in_car_conv_on_hold 
				AND in_car_conv_started
					fJazzTime = 0.0
					iCurrentDriveConv++
					in_car_conv_on_hold = FALSE
					in_car_conv_started = FALSE
					sConvResumeLabel = ""
					iLastConvFinish = GET_GAME_TIMER()
				ENDIF
			BREAK
			
			CASE 4
				IF (GET_GAME_TIMER() - iLastConvFinish) > 3000
					iCurrentDriveConv++
				ENDIF
			BREAK
			
			CASE 5
				IF IS_REPLAY_IN_PROGRESS()
					MANAGE_CONVERSATION("FAM2_DRIVB", bCanPlay)
				ELSE
					MANAGE_CONVERSATION("FAM2_DRIVE", bCanPlay)
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT in_car_conv_on_hold 
				AND in_car_conv_started
					iCurrentDriveConv++
					in_car_conv_on_hold = FALSE
					in_car_conv_started = FALSE
					sConvResumeLabel = ""
					iLastConvFinish = GET_GAME_TIMER()
				ENDIF
			BREAK
			
			CASE 6
				//WAITING IN THIS STAGE UNLESS MIKE DRIVES BAD AND TRIGGERS CASE 7
			BREAK
			
			CASE 7
				IF NOT IS_PED_IN_ANY_VEHICLE(pedJimmy)
				OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					bCanPlay = FALSE
				ENDIF
				MANAGE_CONVERSATION("FAM2_DRIV2", bCanPlay)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT in_car_conv_on_hold 
				AND in_car_conv_started
					iCurrentDriveConv++
					in_car_conv_on_hold = FALSE
					in_car_conv_started = FALSE
					sConvResumeLabel = ""
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ELSE
	
		SWITCH iRadioConvStage
			CASE 0
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
					KILL_FACE_TO_FACE_CONVERSATION()
					in_car_conv_on_hold = TRUE
				ENDIF
				iRadioConvStage++
			BREAK
			CASE 1
				IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_JAZZ", CONV_PRIORITY_HIGH)
					SETTIMERB(0)
					iRadioConvStage++
				ENDIF
			BREAK
			CASE 2
				IF TIMERB() > 1000
					SET_RADIO_TO_STATION_INDEX(8)
//					FREEZE_RADIO_STATION(GET_RADIO_STATION_NAME(8))
					SETTIMERB(0)
					iRadioConvStage++
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_JAZZ2", CONV_PRIORITY_HIGH)
						SETTIMERB(0)
						iRadioConvStage++
					ENDIF
				ENDIF
			BREAK
			CASE 4
				bRadioChat = FALSE
				//just waits for next part.
			BREAK
			CASE 5
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_THIS_CONVERSATION_PLAYING("FAM2_JAZZ")
				AND NOT IS_THIS_CONVERSATION_PLAYING("FAM2_JAZZ2")
					TEXT_LABEL_23 sTemp
					sTemp = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
					IF NOT IS_STRING_NULL_OR_EMPTY(sTemp)
					AND NOT ARE_STRINGS_EQUAL("NULL", sTemp)
						sConvResumeLabel = sTemp
					ENDIF
					KILL_FACE_TO_FACE_CONVERSATION()
					in_car_conv_on_hold = TRUE
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_JAZZ3", CONV_PRIORITY_HIGH)
						SETTIMERB(0)
						iRadioConvStage++
					ENDIF
				ENDIF
			BREAK
			CASE 6
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_JAZZ4", CONV_PRIORITY_HIGH)
						SETTIMERB(0)
						iRadioConvStage++
					ENDIF
				ENDIF
			BREAK
			CASE 7
				bRadioChat = FALSE
			BREAK
		ENDSWITCH
		
	ENDIF
	
	PRINTLN("@@@@@@@@ Radio debug FAM iCurrentDriveConv: ", iCurrentDriveConv, " iRadioConvStage: ", iRadioConvStage, " bRadioChat: ", bRadioChat, " sConvResumeLabel: ", sConvResumeLabel, " fJazzTime: ", fJazzTime, " @@@@@@@@@@@@")
	
ENDPROC

BOOL bPlayerHasLeftHouse = FALSE

PROC GO_TO_VENICE()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT DOES_ENTITY_EXIST(objChair)
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayerStartCoords) < 50*50
				REQUEST_MODEL(PROP_CS_OFFICE_CHAIR)
				IF HAS_MODEL_LOADED(PROP_CS_OFFICE_CHAIR)
					objChair = CREATE_OBJECT(PROP_CS_OFFICE_CHAIR, <<-809.4098, 172.7913, 75.7732>>)
					SET_ENTITY_QUATERNION(objChair, 0.0000, 0.0000, 0.3857, 0.9226)
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_OFFICE_CHAIR)
				ENDIF
			ENDIF
		ELSE
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayerStartCoords) > 75*75
			AND IS_ENTITY_OCCLUDED(objChair)
				DELETE_OBJECT(objChair)
			ENDIF
		ENDIF
	ENDIF
	
	MANAGE_DRIVE_DIALOGUE()
	
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_bike_guy_pos) < 1000
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedJimmy)
		IF NOT IS_PED_IN_ANY_VEHICLE(pedJimmy)
			SET_PED_RESET_FLAG(pedJimmy, PRF_SearchForClosestDoor, TRUE)
		ENDIF
		SWITCH iStageProgress
			CASE 0
				bPlayerHasLeftHouse = FALSE
				IF NOT DOES_ENTITY_EXIST(vehMikesCar)
					CREATE_PLAYER_VEHICLE(vehMikesCar, CHAR_MICHAEL, v_car_start_coords, f_car_start, TRUE, VEHICLE_TYPE_CAR)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehMikesCar)
						SET_ENTITY_COORDS(vehMikesCar, v_car_start_coords)
						SET_ENTITY_HEADING(vehMikesCar, f_car_start)
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehMikesCar, TRUE)
				ENDIF
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedJimmy)
					TASK_FOLLOW_WAYPOINT_RECORDING(pedJimmy, sJimmyStairRoute, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
				ENDIF
				bBlipParkingSpace = FALSE
				bGoBackToJimmyPrinted = FALSE
				DO_SCREEN_FADE_IN(500)
				SET_INITIAL_PLAYER_STATION("RADIO_01_CLASS_ROCK")
				
				IF DOES_ENTITY_EXIST(pedJimmy)
				AND NOT IS_ENTITY_VISIBLE(pedJimmy)
					SET_ENTITY_VISIBLE(pedJimmy, TRUE)
				ENDIF
				
				//FREEZE_RADIO_STATION("RADIO_01_CLASS_ROCK")
				
				IF bReplaySetupRequired
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK)
					bReplaySetupRequired = FALSE
				ENDIF
				
				iStageProgress++
			BREAK
			CASE 1
				IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
					CREATE_NPC_VEHICLE(vehTraceyCar, CHAR_TRACEY, <<-812.4639, 159.7855, 71.0588>>, 113.1515, TRUE, VEHICLE_TYPE_CAR)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(vehMikesCar)
					CREATE_PLAYER_VEHICLE(vehMikesCar, CHAR_MICHAEL, v_car_start_coords, f_car_start, TRUE, VEHICLE_TYPE_CAR)
				ENDIF
				IF DOES_ENTITY_EXIST(vehTraceyCar)
				AND DOES_ENTITY_EXIST(vehMikesCar)
					iStageProgress++
				ENDIF
			BREAK
			CASE 2
			
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedJimmy)
				AND NOT IS_PED_IN_GROUP(pedJimmy)
					IF GET_PED_WAYPOINT_PROGRESS(pedJimmy) > 13
						IF DOES_ENTITY_EXIST(vehMikesCar)
						AND IS_VEHICLE_DRIVEABLE(vehMikesCar)
							TASK_ENTER_VEHICLE(pedJimmy, vehMikesCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
						ELIF DOES_ENTITY_EXIST(vehPlayerCar)
						AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
							TASK_ENTER_VEHICLE(pedJimmy, vehPlayerCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
						ENDIF
						REMOVE_WAYPOINT_RECORDING(sJimmyStairRoute)
					ENDIF
				ENDIF
				
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedJimmy)
//				AND NOT IS_PED_IN_ANY_VEHICLE(pedJimmy, TRUE)
//					IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
//						IF DOES_ENTITY_EXIST(vehMikesCar)
//							TASK_ENTER_VEHICLE(pedJimmy, vehMikesCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
//						ELIF DOES_ENTITY_EXIST(vehPlayerCar)
//							TASK_ENTER_VEHICLE(pedJimmy, vehMikesCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN)
//						ENDIF
//						REMOVE_WAYPOINT_RECORDING(sJimmyStairRoute)
//					ENDIF
//				ENDIF
				
				IF NOT bPlayerHasLeftHouse
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-818.154114,177.643127,75.127777>>, <<-819.791138,176.999146,70.625862>>, 3.250000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-795.150085,176.543396,74.135185>>, <<-795.530334,177.550110,71.835182>>, 2.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-793.730225,181.551041,74.135185>>, <<-792.538513,182.000153,71.835182>>, 2.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.401917,167.616302,79.107170>>, <<-801.433594,165.463943,76.072403>>, 2.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.890747,185.845673,71.431641>>, <<-807.430603,187.241699,74.226456>>, 1.750000)
						bPlayerHasLeftHouse = TRUE
					ENDIF
				ENDIF
				
				BOOL bInCarJimmyIsHeadingFor
				bInCarJimmyIsHeadingFor = FALSE
				IF (IS_VEHICLE_DRIVEABLE(vehMikesCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehMikesCar))
				OR NOT DOES_ENTITY_EXIST(vehMikesCar)
					bInCarJimmyIsHeadingFor = TRUE
				ENDIF
				
				IF (VDIST(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 15 OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())))
				AND bPlayerHasLeftHouse
					IF NOT bInCarJimmyIsHeadingFor
						CLEAR_PED_TASKS(pedJimmy)
					ENDIF
					SET_PED_AS_GROUP_MEMBER(pedJimmy, GET_PLAYER_GROUP(PLAYER_ID()))
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedJimmy, VS_FRONT_RIGHT)
					iStageProgress++				
				ENDIF
				
			BREAK
			
			CASE 3
			
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedJimmy)
					IF GET_PED_WAYPOINT_PROGRESS(pedJimmy) > 13
						IF DOES_ENTITY_EXIST(vehMikesCar)
						AND IS_VEHICLE_DRIVEABLE(vehMikesCar)
							TASK_ENTER_VEHICLE(pedJimmy, vehMikesCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
						ELIF DOES_ENTITY_EXIST(vehPlayerCar)
						AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
							TASK_ENTER_VEHICLE(pedJimmy, vehPlayerCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
							FORCE_PED_MOTION_STATE(pedJimmy, MS_ON_FOOT_WALK)
						ENDIF
						REMOVE_WAYPOINT_RECORDING(sJimmyStairRoute)
					ENDIF
				ENDIF
				
				IF IS_PED_GROUPED(pedJimmy)
					IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						AND GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = PLAYER_PED_ID()
							vehPlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							SET_ENTITY_AS_MISSION_ENTITY(vehPlayerCar, TRUE, TRUE)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayerCar, TRUE)
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGH)
							
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehPlayerCar, FAM2_MAX_CAR_SPEED)
						ENDIF
					ELSE
						//set the destination blip to park up if in vehicle, or stragiht to the rental place of on foot.
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
							bBlipParkingSpace = TRUE
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
							AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								vehPlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								SET_ENTITY_AS_MISSION_ENTITY(vehPlayerCar, TRUE, TRUE)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayerCar, TRUE)
								INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehPlayerCar, FAM2_MAX_CAR_SPEED)
							ENDIF
						ELSE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerCar)
						ENDIF
					ENDIF
					
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						bBlipParkingSpace = FALSE
					ENDIF
					
					IF bBlipParkingSpace
						IF DOES_BLIP_EXIST(blipDest)
							IF NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(blipDest), v_car_stop)
								REMOVE_BLIP(blipDest)
							ENDIF
						ELSE
							blipDest = CREATE_BLIP_FOR_COORD(v_car_stop, TRUE)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(blipDest)
							IF NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(blipDest), <<-1103.2129, -1695.1508, 3.3416>>)
								REMOVE_BLIP(blipDest)
							ENDIF
						ELSE
							blipDest = CREATE_BLIP_FOR_COORD(<<-1103.2129, -1695.1508, 3.3416>>, FALSE)
						ENDIF
					ENDIF
					
					IF bBlipParkingSpace
						IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						AND IS_ENTITY_AT_COORD(vehPlayerCar, v_car_stop, <<3,3,LOCATE_SIZE_HEIGHT>>, TRUE)
						AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
							REMOVE_BLIP(blipDest)
							iStageProgress++
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1103.2129, -1695.1508, 3.3416>>, <<6,6,LOCATE_SIZE_HEIGHT>>, TRUE)
							GO_TO_STAGE(stage_hire_bike)
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(blipBuddy)
						CLEAR_PRINTS()
						REMOVE_BLIP(blipBuddy)
					ENDIF
					
				ELSE
				
					IF DOES_BLIP_EXIST(blipDest)
						REMOVE_BLIP(blipDest)
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipBuddy)
						CLEAR_PRINTS()
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
							KILL_FACE_TO_FACE_CONVERSATION()
							in_car_conv_on_hold = TRUE
						ENDIF
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						IF NOT bGoBackToJimmyPrinted
							PRINT("FM2_JIMGB", DEFAULT_GOD_TEXT_TIME, 1)
						ENDIF
						blipBuddy = CREATE_BLIP_FOR_PED(pedJimmy)
					ENDIF
					
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
				AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				AND GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = 0
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT bMoreSeatsPrinted
						PRINT("FM2_BIGGERV", DEFAULT_GOD_TEXT_TIME, 1)
						bMoreSeatsPrinted = TRUE
					ENDIF
				ELSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND bMoreSeatsPrinted
						CLEAR_PRINTS()
						bMoreSeatsPrinted = FALSE
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 4
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar, 4, 1)
						IF IS_PED_IN_VEHICLE(pedJimmy, vehPlayerCar)
														
							IF IS_PHONE_ONSCREEN()
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							ENDIF
							KILL_FACE_TO_FACE_CONVERSATION()
							TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar, <<10,5,0>>), 20000, SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_HIGH)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_HIRE1", CONV_PRIORITY_HIGH)
									in_car_conv_on_hold = FALSE
									TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, 30000, SLF_EXTEND_YAW_LIMIT)
									SEQUENCE_INDEX seqJimmy
									OPEN_SEQUENCE_TASK(seqJimmy)
										TASK_LEAVE_VEHICLE(NULL, vehPlayerCar)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1102.1871, -1697.8910, 3.3562>>, PEDMOVEBLENDRATIO_WALK)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(seqJimmy)
									TASK_PERFORM_SEQUENCE(pedJimmy, seqJimmy)
									SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									REMOVE_PED_FROM_GROUP(pedJimmy)
									CLEAR_SEQUENCE_TASK(seqJimmy)
									
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
									
									iStageProgress=8
								ENDIF
							ENDIF
						ELSE
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGH)
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, FAM2_MAX_CAR_SPEED)
							GO_TO_STAGE(stage_hire_bike)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
				IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_HIRE1", CONV_PRIORITY_HIGH)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, 20000, SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_HIGH)
					SETTIMERA(0)
					iStageProgress=8
				ENDIF
			BREAK
			
			CASE 8
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, FAM2_MAX_CAR_SPEED)
					GO_TO_STAGE(stage_hire_bike)
//				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_2_DRIVE_TO_BEACH")
					START_AUDIO_SCENE("FAMILY_2_DRIVE_TO_BEACH")
				ENDIF
			ELSE
				IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_DRIVE_TO_BEACH")
					STOP_AUDIO_SCENE("FAMILY_2_DRIVE_TO_BEACH")
				ENDIF
			ENDIF
		ENDIF
				
	ENDIF
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		LOSE_THE_COPS()		
	ENDIF
ENDPROC

PROC BIKE_TUTORIAL()

	IF DOES_ENTITY_EXIST(vehPlayerBike)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerBike)
		SWITCH iBikeTutStage
		
			CASE 0
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
				AND NOT IS_BROWSER_OPEN()
					IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						PRINT_HELP("FM2_BIKE1")
					ELSE
						PRINT_HELP("FM2_BIKE1_KM")
					ENDIF
					iBikeTutStage++
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_PC_VERSION()
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE1")
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
					AND NOT IS_BROWSER_OPEN()
						PRINT_HELP("FM2_BIKE2")
						iBikeTutStage++
					ENDIF
				ELSE
					IF  NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE1")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE1_KM")
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
					AND NOT IS_BROWSER_OPEN()
						PRINT_HELP("FM2_BIKE2")
						iBikeTutStage++
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE2")
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
				AND NOT IS_BROWSER_OPEN()
					IF IS_VEHICLE_MODEL(vehPlayerBike, BMX)
					OR IS_VEHICLE_MODEL(vehPlayerBike, SCORCHER)
						PRINT_HELP("FM2_BIKE3")
					ENDIF
					iBikeTutStage++
				ENDIF
			BREAK
//			CASE 3
//				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE3")
//				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
//				AND NOT IS_BROWSER_OPEN()
//					IF NOT IS_VEHICLE_MODEL(vehPlayerBike, TRIBIKE)
//					AND NOT IS_VEHICLE_MODEL(vehPlayerBike, CRUISER)
//						PRINT_HELP("FM2_WHEELIE")
//					ENDIF
//					iBikeTutStage++
//				ENDIF
//			BREAK
			CASE 3
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE2")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE3")
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
				AND NOT IS_BROWSER_OPEN()
					PRINT_HELP("FM2_BIKE4")
					iBikeTutStage++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
		OR IS_BROWSER_OPEN()
			IF IS_PC_VERSION()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE1_KM")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE1")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE2")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE3")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_BIKE4")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_WHEELIE")
				CLEAR_HELP()
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

//BOOL bResentmentConv = FALSE
BOOL bCHooseBikeText = FALSE
BOOL bJimmyBikeFree = FALSE
BOOL bJimmySentForBike =  FALSE
BOOL bChoiceLinePlayed = FALSE

INT iTimeMovingStarted

PROC DO_IN_GAME_BIKE_HIRE()

	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME (PLAYER_ID())
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.4)
	
	INT iTemp
	
	IF iStageProgress <> 1000
		IF VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50*50
			iSavedStage = iStageProgress
			iStageProgress = 1000
		ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_DRIVE_TO_BEACH")
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			STOP_AUDIO_SCENE("FAMILY_2_DRIVE_TO_BEACH")
		ENDIF
	ENDIF
	
	IF bMovingStart
	AND iStageProgress > 1
		IF GET_GAME_TIMER() - iTimeMovingStarted < 2000
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		ELSE
			bMovingStart = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedJimmy)
	AND NOT IS_PED_INJURED(pedBikeGuy)
	
		SWITCH iStageProgress
		
			CASE 0
				bBeenOnBike = FALSE
				REMOVE_PED_FROM_GROUP(pedJimmy)
				REQUEST_VEHICLE_RECORDING(i_jimmy_bike_rec, s_jimmy_bike_rec)
				REQUEST_ANIM_DICT("veh@bicycle@mountain_f@front@base")
				iStageProgress++
				iJimmyChoice = 0
				iMikeChoice = 0
				iCurrentJimmyTarget = 4
				bChoiceLinePlayed = FALSE
				iTimeMovingStarted = GET_GAME_TIMER()
				SET_WANTED_LEVEL_MULTIPLIER(0.3)
				
				IF bReplaySetupRequired
					iStageProgress = -1
				ENDIF
			BREAK
			
			CASE -1
				MANAGE_BIKE_SHOP()
//				IF NOT DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1105.2411, -1697.0247, 3.3678>>, 0.5, PROP_TABLE_03_CHR)
//					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1105.2411, -1697.0247, 3.3678>>, 10, PROP_TABLE_03_CHR)
//						OBJECT_INDEX objTemp
//						objTemp = GET_CLOSEST_OBJECT_OF_TYPE(<<-1105.2411, -1697.0247, 3.3678>>, 10, PROP_TABLE_03_CHR, TRUE)
//						SET_ENTITY_AS_MISSION_ENTITY(objTemp, TRUE, TRUE)
//						DELETE_OBJECT(objTemp)
//					ENDIF
//					OBJECT_INDEX objTempChair
//					REQUEST_MODEL(PROP_TABLE_03_CHR)
//					objTempChair = CREATE_OBJECT(PROP_TABLE_03_CHR, <<-1105.2411, -1697.0247, 3.3678>>)
//					SET_ENTITY_QUATERNION(objTempChair, 0.0000, 0.0000, 0.8708, 0.4917)
//				ENDIF
				IF NOT IS_PED_INJURED(pedBikeGuy)
				AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1105.2411, -1697.0247, 3.3678>>, 2, PROP_TABLE_03_CHR)
				AND IS_PED_USING_SCENARIO(pedBikeGuy, "PROP_HUMAN_SEAT_CHAIR")
					PRINTLN("pedbikeguy is alive")
					bReplaySetupRequired = FALSE
					iStageProgress = 1
				ENDIF
			BREAK
			
			CASE 1
				blipDest = CREATE_BLIP_FOR_COORD(v_bike_guy_pos)
				IF bShowGodText
					PRINT("FM2_BK2BIKE", DEFAULT_GOD_TEXT_TIME, 1)
					SETTIMERA(0)
				ENDIF
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, 40000, SLF_EXTEND_YAW_LIMIT)
				TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), 40000, SLF_EXTEND_YAW_LIMIT)
//				bResentmentConv = FALSE
				bChooseBikeText = FALSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TEXT_LABEL_23 sConv
					sConv = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					IF NOT ARE_STRINGS_EQUAL("FAM2_HIRE1", sConv)
						PRINTLN("FAM2_HIRE1 is not the currently playing label killing conv")
						KILL_FACE_TO_FACE_CONVERSATION()		
					ELSE
						PRINTLN("FAM2_HIRE1 is the currently playing label NOT killing conv")
					ENDIF
				ENDIF
				bJimmySentForBike = FALSE
				IF IS_SCREEN_FADED_OUT()
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
				ENDIF
				iStageProgress++
			BREAK
			
			CASE 2
				IF g_bPlayerIsInTaxi
					PRINTLN("Player is in taxi")
					WHILE g_bPlayerIsInTaxi
						PRINTLN("Waiting for taxi to finish")
						WAIT(0)
					ENDWHILE
				ENDIF
				PRINTLN("DOING SPECIFIC FADE FOR BIKE HIRE")			
				IF IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(pedJimmy, TRUE)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						AND IS_PED_IN_VEHICLE(pedJimmy, vehPlayerCar)
							TASK_LEAVE_VEHICLE(pedJimmy, vehPlayerCar)
						ELSE
							TASK_LEAVE_ANY_VEHICLE(pedJimmy)
						ENDIF
					ENDIF
				ELSE
					SETTIMERB(0)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 3
				IF NOT IS_PED_INJURED(pedJimmy)
				AND TIMERA() > 3000
					IF NOT IS_PED_FACING_PED(pedJimmy, PLAYER_PED_ID(), 60)
						IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
							CLEAR_PED_TASKS(pedJimmy)
							TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
							OPEN_SEQUENCE_TASK(seqTurnTask)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
							CLOSE_SEQUENCE_TASK(seqTurnTask)
							TASK_PERFORM_SEQUENCE(pedJimmy, seqTurnTask)
							CLEAR_SEQUENCE_TASK(seqTurnTask)
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
							IF NOT IS_PED_INJURED(pedJimmy)
								TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(pedBikeGuy)
						IF VDIST(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(pedBikeGuy)) > VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedBikeGuy)) + 0.3
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1099.755737,-1700.726074,3.349046>>, <<-1111.517456,-1683.961792,5.340389>>, 7.250000)
						OR TIMERA() > 6000
							CLEAR_PED_TASKS(pedJimmy)
							iStageProgress++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1099.755737,-1700.726074,3.349046>>, <<-1111.517456,-1683.961792,5.340389>>, 7.250000)
					IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						FREEZE_ENTITY_POSITION(vehPlayerCar, FALSE)
					ENDIF
					SEQUENCE_INDEX seqGoToBikeGuy
					OPEN_SEQUENCE_TASK(seqGoToBikeGuy)
					IF IS_PED_IN_ANY_VEHICLE(pedJimmy)
						TASK_LEAVE_ANY_VEHICLE(NULL)
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1102.1871, -1697.8910, 3.3562>>, PEDMOVEBLENDRATIO_RUN)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seqGoToBikeGuy)
					TASK_PERFORM_SEQUENCE(pedJimmy, seqGoToBikeGuy)
					CLEAR_SEQUENCE_TASK(seqGoToBikeGuy)
					SET_PED_MAX_MOVE_BLEND_RATIO(pedJimmy, PEDMOVEBLENDRATIO_WALK)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 5
				SET_PED_MAX_MOVE_BLEND_RATIO(pedJimmy, PEDMOVEBLENDRATIO_WALK)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1099.755737,-1700.726074,3.349046>>, <<-1111.517456,-1683.961792,5.340389>>, 7.250000)
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_HIRE3", CONV_PRIORITY_HIGH)
							CLEAR_PRINTS()
							TASK_LOOK_AT_ENTITY(pedBikeGuy, PLAYER_PED_ID(), 30000)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedBikeGuy, 10000)
							REMOVE_BLIP(blipDest)
							SETTIMERA(0)
							iStageProgress++
						ENDIF
					ELSE
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			//in this stage we wait for Jimmy to get on a bike.
			CASE 6
			
				IF IS_VEHICLE_DRIVEABLE(vehBikes[iCurrentJimmyTarget])
				AND NOT IS_PED_IN_VEHICLE(pedJimmy, vehBikes[iCurrentJimmyTarget])
					IF NOT IS_VEHICLE_SEAT_FREE(vehBikes[iCurrentJimmyTarget])
						IF bJimmySentForBike
							CLEAR_PED_TASKS(pedJimmy)
							bJimmySentForBike = FALSE
						ENDIF
						IF iCurrentJimmyTarget = 4
							iCurrentJimmyTarget = 1
						ELIF iCurrentJimmyTarget = 1
							iCurrentJimmyTarget = 4
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehPlayerBike)
					SET_PED_MAX_MOVE_BLEND_RATIO(pedJimmy, PEDMOVEBLENDRATIO_WALK)
				ENDIF
				
				REQUEST_ANIM_DICT("veh@bicycle@mountain_f@front@base")
				IF HAS_ANIM_DICT_LOADED("veh@bicycle@mountain_f@front@base")
					IF IS_VEHICLE_DRIVEABLE(vehBikes[iCurrentJimmyTarget])
						IF NOT bJimmySentForBike
						AND (IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-1099.755737,-1700.726074,3.349046>>, <<-1111.517456,-1683.961792,5.340389>>, 9.250000)
						OR NOT IS_ENTITY_IN_ANGLED_AREA(vehBikes[iCurrentJimmyTarget], <<-1099.755737,-1700.726074,3.349046>>, <<-1111.517456,-1683.961792,5.340389>>, 9.250000))
							SEQUENCE_INDEX seqGetOnBike
							OPEN_SEQUENCE_TASK(seqGetOnBike)
							TASK_ENTER_VEHICLE(NULL, vehBikes[iCurrentJimmyTarget], DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN)
							TASK_STAND_STILL(NULL, 2000)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehBikes[iCurrentJimmyTarget], <<-1099.6821, -1694.0726, 3.2758>>, 5, DRIVINGSTYLE_ACCURATE, CRUISER, DRIVINGMODE_AVOIDCARS | DF_StopForPeds, 2.0, 3.0)
							CLOSE_SEQUENCE_TASK(seqGetOnBike)
							TASK_PERFORM_SEQUENCE(pedJimmy, seqGetOnBike)
							CLEAR_SEQUENCE_TASK(seqGetOnBike)
							bJimmySentForBike = TRUE
						ENDIF
					ENDIF
				ELSE
					PRINTLN("loading anim veh@bicycle@mountain_f@front@base")
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehJimmyBike)
					REPEAT COUNT_OF(vehBikes) iTemp
						IF DOES_ENTITY_EXIST(vehBikes[iTemp])
						AND IS_VEHICLE_DRIVEABLE(vehBikes[iTemp])
							IF IS_PED_SITTING_IN_VEHICLE(pedJimmy, vehBikes[iTemp])
								vehJimmyBike = vehBikes[iTemp]
								SET_PED_IN_VEHICLE_CONTEXT(pedJimmy, GET_HASH_KEY("MISS_FAMILY2_JIMMY_BICYCLE"))
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehJimmyBike, FALSE)
								DEBIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, 20)
								PRINTLN("Debitting account for jimmy bike")
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
									CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_JBIKE", CONV_PRIORITY_HIGH)
								ENDIF
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedJimmy, KNOCKOFFVEHICLE_NEVER)
								iJimmyChoice = iTemp
								FREEZE_ENTITY_POSITION(vehJimmyBike, FALSE)
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					IF NOT IS_PED_IN_ANY_VEHICLE(pedJimmy)
						PRINTLN("JIMMY SPOOK 3")
						MISSION_FAILED("FM2_JSPOOK")
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehPlayerBike)
					REPEAT COUNT_OF(vehBikes) iTemp
						IF IS_VEHICLE_DRIVEABLE(vehBikes[iTemp])
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBikes[iTemp], TRUE)
								vehPlayerBike = vehBikes[iTemp]
								DEBIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, 20)
								PRINTLN("Debitting account for player bike")
								FREEZE_ENTITY_POSITION(vehBikes[iTemp], FALSE)
								CLEAR_PRINTS()
								iMikeChoice = iTemp
								IF VDIST(GET_ENTITY_COORDS(pedJimmy), <<-1108.6643, -1691.6234, 3.3746>>) > 5
								AND NOT IS_PED_IN_ANY_VEHICLE(pedJimmy)
								AND bJimmySentForBike
									bJimmySentForBike = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					IF NOT bCHooseBikeText
						IF TIMERA() > 1000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
								PRINT_NOW("FM2_CHSEBIKE", DEFAULT_GOD_TEXT_TIME, 1)
								bCHooseBikeText = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bChoiceLinePlayed
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_HIRE4", CONV_PRIORITY_HIGH)
							bChoiceLinePlayed = TRUE
						ENDIF
					ENDIF
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
						vehPlayerBike = NULL
					ENDIF
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND DOES_ENTITY_EXIST(vehJimmyBike)
				AND DOES_ENTITY_EXIST(vehPlayerBike)
				AND IS_VEHICLE_DRIVEABLE(vehJimmyBike)
				AND IS_VEHICLE_DRIVEABLE(vehPlayerBike)
				AND IS_PED_SITTING_IN_VEHICLE(pedJimmy, vehJimmyBike)
					vehbikes[iMikeChoice] = NULL
					vehbikes[iJimmyChoice] = NULL
					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGH)
					
					iStageProgress++
				ENDIF
				
			BREAK
			
			CASE 7
				IF TIMERA() > 3000
					GO_TO_STAGE(stage_bike_ride)
				ENDIF
			BREAK
			
			CASE 1000
				IF NOT DOES_BLIP_EXIST(blipBuddy)
					IF NOT bGoBackToJimmyPrinted
						CLEAR_PRINTS()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						PRINT("FM2_JIMGB", DEFAULT_GOD_TEXT_TIME, 1)
					ENDIF
					blipBuddy = CREATE_BLIP_FOR_PED(pedJimmy)
				ENDIF
				IF DOES_BLIP_EXIST(blipDest)
					IF GET_BLIP_ALPHA(blipDest) <> 0
						SET_BLIP_ALPHA(blipDest, 0)
					ENDIF
				ENDIF
				IF VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20*20
				OR (NOT IS_PED_INJURED(pedBikeGuy) AND VDIST2(GET_ENTITY_COORDS(pedBikeGuy), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 30*30)
					iStageProgress = iSavedStage
					IF DOES_BLIP_EXIST(blipDest)
						IF GET_BLIP_ALPHA(blipDest) <> 255
							SET_BLIP_ALPHA(blipDest, 255)
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(blipBuddy)
						REMOVE_BLIP(blipBuddy)
					ENDIF
				ENDIF
				IF VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 150*150
					Mission_Failed("FM2_JIMAB")
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF NOT bJimmyBikeFree
			IF IS_VEHICLE_DRIVEABLE(vehJimmyBike)
			AND NOT IS_PED_INJURED(pedJimmy)
			AND IS_PED_IN_VEHICLE(pedJimmy, vehJimmyBike)
				FREEZE_ENTITY_POSITION(vehJimmyBike, FALSE)
				bJimmyBikeFree = TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlayerBike)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerBike)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
		IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_2_BIKE_RACE_JIMMY")
			START_AUDIO_SCENE("FAMILY_2_BIKE_RACE_JIMMY")
		ENDIF
	ENDIF
	
ENDPROC

//Builds an array of vectors for a car recording path
PROC BUILD_PATH_FOR_RECORDING(VECTOR &path[], INT FileNumber, STRING pRecordingName)
	REQUEST_VEHICLE_RECORDING(FileNumber, pRecordingName)
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(FileNumber, pRecordingName)
		FLOAT duration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(FileNumber, pRecordingName)
		FLOAT route_segment = duration/(COUNT_OF(path)-1)
		INT iTemp
		REPEAT COUNT_OF(path) iTemp
			path[iTemp] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, route_segment*iTemp, pRecordingName)
		ENDREPEAT
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC DRAW_PATH(VECTOR &vPath[])
		INT iTemp
		FOR iTemp = 0 TO COUNT_OF(vPath) - 1
			DRAW_DEBUG_LINE(vPath[iTemp], vPath[iTemp]+<<0,0,2>>)
			IF iTemp > 0
				DRAW_DEBUG_LINE(vPath[iTemp]+<<0,0,1>>, vPath[iTemp-1]+<<0,0,1>>, 0, 0, 255)
			ENDIF
		ENDFOR
	ENDPROC
#ENDIF

FUNC FLOAT GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(VECTOR point, INT iRecNumber, STRING sRecName, VECTOR &route[])

	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNumber, sRecName)
	
		INT iTemp, iTemp2
		
		//three closest nodes, seeded at 0, 1 and 2 to prevent stacking on 0
		INT iClosestNodes[3]
		iClosestNodes[0] = COUNT_OF(route) - 1
		iClosestNodes[1] = COUNT_OF(route) - 2
		iClosestNodes[2] = COUNT_OF(route) - 3
		
		//Cycle through all points
		REPEAT COUNT_OF(route) iTemp
		
			//get distance to current point
			FLOAT fDistX = ABSF(point.x - route[iTemp].x)
			FLOAT fDistY = ABSF(point.y - route[iTemp].y) 
			FLOAT fDist = fDistX + fDistY
			
			//cycle through current three closest points and get distance to all to figure furthest
			FLOAT fFurthest = 0.0
			INT iFurthest = 0
			REPEAT COUNT_OF(iClosestNodes) iTemp2
				IF iTemp <> iClosestNodes[iTemp2]
					FLOAT fTempDistX = ABSF(point.x - route[iClosestNodes[iTemp2]].x)
					FLOAT fTempDistY = ABSF(point.y - route[iClosestNodes[iTemp2]].y)
					FLOAT fTempDist = fTempDistX + fTempDistY
					IF fTempDist > fFurthest
						iFurthest = iTemp2
						fFurthest = fTempDist
					ENDIF
				ENDIF
			ENDREPEAT
			
			//if this points distance is closer than the furthest in the set of three closest nodes then replace it with this node.
			IF fDist < fFurthest
				iClosestNodes[iFurthest] = iTemp
			ENDIF
			
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			REPEAT COUNT_OF(iClosestNodes) iTemp2
				//DRAW_DEBUG_SPHERE(route[iClosestNodes[iTemp2]]+<<0,0,0.3*iTemp2+0.3>>, 0.1, 255,0,0)
			ENDREPEAT
		#ENDIF
		
		//now we have the three closest points check line positions to see which line is closest
		FLOAT fClosestDist = 999999
		INT iClosestLineStart
		VECTOR vClosestPoint
		
		REPEAT COUNT_OF(iClosestNodes) iTemp
		
			VECTOR vTemp1
			VECTOR vTemp2
			
			//get the distnace to the line either side of the nearest point, wrapping to start and end point if necessary.
			IF iClosestNodes[iTemp] > 0
				vTemp1 = GET_CLOSEST_POINT_ON_LINE(point, route[iClosestNodes[iTemp]], route[iClosestNodes[iTemp]-1])
			ELSE
				vTemp1 = route[0]
			ENDIF
			
			IF iClosestNodes[iTemp] < COUNT_OF(route)-2
				vTemp2 = GET_CLOSEST_POINT_ON_LINE(point, route[iClosestNodes[iTemp]], route[iClosestNodes[iTemp]+1])
			ELSE
				vTemp2 = route[COUNT_OF(route)-1]
			ENDIF
			
			//Check which line is closer
			VECTOR vClosest
			BOOL bAhead = FALSE //this is a flag to show that the player is ahead of the current closest point
			IF VDIST2(point, vTemp1) < VDIST2(point, vTemp2)
				vClosest = vTemp1
			ELSE
				bAhead = TRUE
				vClosest = vTemp2
			ENDIF
			
			//check if the closest point to the line from the current closest node is closer than the current closest
			IF VDIST2(point, vClosest) < fClosestDist
				IF bAhead
					iClosestLineStart = iClosestNodes[iTemp]
				ELSE
					iClosestLineStart = iClosestNodes[iTemp] - 1
				ENDIF
				fClosestDist = VDIST2(point, vClosest)
				vClosestPoint = vClosest
			ENDIF
			
		ENDREPEAT
		
		iClosestLineStart = CLAMP_INT(iClosestLineStart, 0, COUNT_OF(route)-2)
		
		FLOAT fTimePerNode = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecNumber, sRecName)/(COUNT_OF(route)-1)
		FLOAT fCurrentLineProg = (VDIST(route[iClosestLineStart], vClosestPoint)/VDIST(route[iClosestLineStart], route[iClosestLineStart+1]))*fTimePerNode
		
		#IF IS_DEBUG_BUILD
			//DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(VDIST(route[iClosestLineStart], vClosestPoint)/VDIST(route[iClosestLineStart], route[iClosestLineStart+1])), <<0.5, 0.9, 0>>)
			//DRAW_DEBUG_SPHERE(vClosestPoint + <<0.2,0,0>>, 0.1, 255, 0, 255)
		#ENDIF
		
		RETURN fCurrentLineProg + (iClosestLineStart*fTimePerNode)
		
	ELSE
	
		RETURN 0.0
		
	ENDIF
	
ENDFUNC

BOOL bForcedPlayback = FALSE
BOOL bStickToPathStrict = FALSE
FLOAT fPlayerProg
FLOAT fJimmyProg

PROC KEEP_JIMMY_ON_REC_PATH()
	IF NOT IS_PED_INJURED(pedJimmy)
	AND IS_VEHICLE_DRIVEABLE(vehJimmyBike)
		IF IS_PED_IN_VEHICLE(pedJimmy, vehJimmyBike)
			fPlayerProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), i_jimmy_bike_rec, s_jimmy_bike_rec, vBikeRoutePath)
			fJimmyProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(vehJimmyBike), i_jimmy_bike_rec, s_jimmy_bike_rec, vBikeRoutePath)
			IF NOT bForcedPlayback
			
				IF NOT bStickToPathStrict
					PRINTLN("AVOIDING PEDS")
					IF IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-1572.473755,-994.666687,2.017392>>, <<-1515.634155,-926.809143,29.122761>>, 100.000000)
					OR IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-1382.942139,-1442.318970,3.013994>>, <<-1367.638184,-1471.879272,8.426488>>, 22.250000)
						IF IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehJimmyBike)
						OR IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehJimmyBike, i_jimmy_bike_rec, s_jimmy_bike_rec, 15, DRIVINGMODE_PLOUGHTHROUGH)
						bStickToPathStrict = TRUE
					ENDIF
				ELSE
					PRINTLN("PLOUGHING THROUGH")
					IF NOT IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-1572.473755,-994.666687,2.017392>>, <<-1515.634155,-926.809143,29.122761>>, 100.000000)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-1382.942139,-1442.318970,3.013994>>, <<-1367.638184,-1471.879272,8.426488>>, 22.250000)
						IF IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehJimmyBike)
						OR IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehJimmyBike, i_jimmy_bike_rec, s_jimmy_bike_rec, 15, DRIVINGMODE_AVOIDCARS | DF_SteerAroundPeds | DF_DontSteerAroundPlayerPed)
						bStickToPathStrict = FALSE
					ENDIF
				ENDIF
				
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedJimmy)) > 100*100 
				AND fJimmyProg > 10
				AND (HAS_VEHICLE_RECORDING_BEEN_LOADED(i_jimmy_bike_rec, s_jimmy_bike_rec) AND fJimmyProg < GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(i_jimmy_bike_rec, s_jimmy_bike_rec) - 10)
					IF IS_ENTITY_OCCLUDED(pedJimmy)
					AND IS_ENTITY_OCCLUDED(vehJimmyBike)
					AND NOT IS_SPHERE_VISIBLE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(i_jimmy_bike_rec, fJimmyProg, s_jimmy_bike_rec), 4)
						SET_ENTITY_COLLISION(vehJimmyBike, FALSE)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
						START_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike, i_jimmy_bike_rec, s_jimmy_bike_rec)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike, fJimmyProg)
						bForcedPlayback = TRUE
					ENDIF
				ENDIF
				
			ELSE
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedJimmy)) < 95*95
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike)
						SET_ENTITY_COLLISION(vehJimmyBike, TRUE)
						SET_PLAYBACK_TO_USE_AI(vehJimmyBike, DRIVINGMODE_AVOIDCARS | DF_SteerAroundPeds | DF_DontSteerAroundPlayerPed)
						bForcedPlayback = FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
				TASK_ENTER_VEHICLE(pedJimmy, vehJimmyBike)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FLOAT fPlayerDistanceFromTrack
//BOOL bJimmyFinalMoveTask
INT iJimmyBreatheTime

INT iCurrentBikePathNode = -1
VECTOR vBikeGPSNodes[4]

PROC BUILD_CUSTOM_GPS_ROUTE(VECTOR &vRoute[], INT &iCurrentClosest)

	INT i = 0
	INT iClosestNode = -1
	FLOAT fClosestDist = 0.0
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	//Get the closest node to the player
	REPEAT (COUNT_OF(vRoute) - 1) i
		FLOAT fDist = VDIST2(vPlayerPos, vRoute[i])
		IF iClosestNode = -1 OR fDist < fClosestDist
			iClosestNode = i
			fClosestDist = fDist
		ENDIF
		#IF IS_DEBUG_BUILD
			IF i = iCurrentClosest
				DRAW_DEBUG_SPHERE(vRoute[i], 10, 0, 255, 0, 100)
			ELSE
				DRAW_DEBUG_SPHERE(vRoute[i], 10, 0, 0, 255, 100)
			ENDIF
		#ENDIF
	ENDREPEAT
	
	//If the closest node is closer to the next node than the player then it's the first node in the route, otherwise it's the next node.
	IF VDIST2(vPlayerPos, vRoute[iClosestNode+1]) < VDIST2(vRoute[iClosestNode], vRoute[iClosestNode+1])
		iClosestNode = iClosestNode + 1
	ENDIF
	
	IF iCurrentClosest != iClosestNode
		IF iCurrentClosest >= 0 AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRoute[iCurrentClosest]) > 5*5
			CLEAR_GPS_MULTI_ROUTE()
			//Build the route starting from the node calculated above.
			START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
			
			FOR i = iClosestNode TO COUNT_OF(vRoute) - 1
				ADD_POINT_TO_GPS_MULTI_ROUTE(vRoute[i])
			ENDFOR
			
			ADD_POINT_TO_GPS_MULTI_ROUTE(vRoute[COUNT_OF(vRoute) - 1])
			SET_GPS_MULTI_ROUTE_RENDER(TRUE)
			PRINTLN("GPS DEBUG: Refreshing GPS route")
		ENDIF
		PRINTLN("GPS DEBUG: Updating GPS closest node")
		iCurrentClosest = iClosestNode
	ENDIF
	
ENDPROC

INT iShootCount = 0
INT iLastShootTime
BOOL bRecentShot = FALSE
BOOL bGoBackToPath = FALSE
VECTOR vPlayerPathPoint
FLOAT fPlayerBikePathProg

FUNC BOOL HAS_PLAYER_SPOOKED_JIMMY_WITH_SHOOTING(BOOL &paramShootCheck, INT &paramShootCount, INT &paramShootTime)

	IF DOES_ENTITY_EXIST(pedJimmy)
	
		IF NOT paramShootCheck
			//IF (IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN) AND IS_PED_SHOOTING(PLAYER_PED_ID())	AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<30, 30, 30>>))
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
			OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedJimmy), 10)
				paramShootCheck = TRUE
			ENDIF
		ELSE
			IF GET_GAME_TIMER() - paramShootTime > 200	
				paramShootCount++
				#IF IS_DEBUG_BUILD
					PRINTLN(paramShootCount)
				#ENDIF
				paramShootTime = GET_GAME_TIMER()
				paramShootCheck = FALSE
			ENDIF
		ENDIF
		
		IF paramShootCount > 0
		AND NOT paramShootCheck
		AND GET_GAME_TIMER() - paramShootTime > 1000
			#IF IS_DEBUG_BUILD
				PRINTLN(paramShootCount)
			#ENDIF
			paramShootCount--
		ENDIF
		
		WEAPON_TYPE playerwep
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerwep)
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND GET_GAME_TIMER() - iLastMeleeShout > 7500
			PRINTLN("Jimmy MELEE LINE!")
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "GENERIC_SHOCKED_MED", "Jimmy", SPEECH_PARAMS_FORCE)
			iLastMeleeShout = GET_GAME_TIMER()
		ENDIF
		
		IF paramShootCount > 5
		OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedJimmy)+<<10,10,5>>, GET_ENTITY_COORDS(pedJimmy)-<<10,10,5>>, WEAPONTYPE_GRENADE)
		OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedJimmy)+<<10,10,5>>, GET_ENTITY_COORDS(pedJimmy)-<<10,10,5>>, WEAPONTYPE_GRENADELAUNCHER)
		OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedJimmy)+<<10,10,5>>, GET_ENTITY_COORDS(pedJimmy)-<<10,10,5>>, WEAPONTYPE_GRENADELAUNCHER_SMOKE)
		OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedJimmy)+<<10,10,5>>, GET_ENTITY_COORDS(pedJimmy)-<<10,10,5>>, WEAPONTYPE_RPG)
		OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedJimmy)+<<10,10,5>>, GET_ENTITY_COORDS(pedJimmy)-<<10,10,5>>, WEAPONTYPE_MOLOTOV)
		OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(pedJimmy)+<<10,10,5>>, GET_ENTITY_COORDS(pedJimmy)-<<10,10,5>>, WEAPONTYPE_BZGAS)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(pedJimmy)+<<10,10,2>>, GET_ENTITY_COORDS(pedJimmy)-<<10,10,2>>)
		OR (IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vehJimmyBike, playerwep))
			PRINTLN("JIMMY SPOOK 4")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


PROC BIKE_RIDE2()

	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.7)
	
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))	
		IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), CRUISER)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SCORCHER)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRIBIKE)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRIBIKE2)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRIBIKE3)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), BMX)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerBike)
			vehPlayerBike = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			INT iTemp
			REPEAT COUNT_OF(vehBikes) iTemp
				IF vehBikes[iTemp] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					vehBikes[iTemp] = NULL
				ENDIF
			ENDREPEAT
			SET_ENTITY_AS_MISSION_ENTITY(vehPlayerBike)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlayerCar)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayerCar)) > 1000
		AND IS_ENTITY_OCCLUDED(vehPlayerCar)
		AND NOT IS_SPHERE_VISIBLE(v_car_stop, 5)
			IF VDIST2(v_car_stop, GET_ENTITY_COORDS(vehPlayerCar)) < 625
				SET_MISSION_VEHICLE_GEN_VEHICLE(vehPlayerCar, v_car_stop + <<0,0,1>>, f_car_stop, VEHGEN_MISSION_VEH, FALSE)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
			ENDIF
			DELETE_VEHICLE(vehPlayerCar)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedJimmy)
	AND IS_VEHICLE_DRIVEABLE(vehJimmyBike)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerBike)
	
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1308.714600,-1310.135254,-22.620132>>, <<-1171.371948,-1261.148438,47.989761>>, 250.000000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1348.925171,-1155.041260,-20.531912>>, <<-1163.359619,-1143.442871,34.684460>>, 142.250000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1386.384766,-1054.306030,-16.121284>>, <<-1245.289429,-965.991455,32.391251>>, 142.250000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1462.164063,-972.099915,-20.403019>>, <<-1332.719849,-833.943481,45.988754>>, 227.250000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1178.163330,-1629.206299,1.873874>>, <<-1315.888306,-1365.294434,25.025532>>, 173.750000)
			SET_IGNORE_NO_GPS_FLAG(TRUE)
		ELSE
			SET_IGNORE_NO_GPS_FLAG(FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehPlayerBike)
		AND IS_VEHICLE_DRIVEABLE(vehPlayerBike)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
			IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_2_BIKE_RACE_JIMMY")
				START_AUDIO_SCENE("FAMILY_2_BIKE_RACE_JIMMY")
			ENDIF
		ENDIF
		
		SWITCH iStageProgress
			CASE 0
				fPlayerDistanceFromTrack = 0
//				bJimmyFinalMoveTask = FALSE
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedJimmy, KNOCKOFFVEHICLE_NEVER)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
				ENDIF
				blipBuddy = CREATE_BLIP_FOR_VEHICLE(vehJimmyBike)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipBuddy, "BLIP_280")
				TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), -1)
				bPlayerDone = FALSE
				iCurrentConvStage = 0
				bJimmyWon = FALSE
				bJimmyFinished = FALSE
				iJimmyBreatheTime = 0
				iCurrentBikePathNode = COUNT_OF(vBikeGPSNodes) -1
				iShootCount = 0
				bGoBackToPath = FALSE
				
				vBikeGPSNodes[0] = <<-1266.2878, -1689.5608, 3.4691>>
				vBikeGPSNodes[1] = <<-1345.0573, -1263.8895, 3.8977>>
				vBikeGPSNodes[2] = <<-1564.5465, -928.2069, 12.0173>>
				vBikeGPSNodes[3] = vRaceFinish
				
				SET_PED_PATHS_IN_AREA(<<-1329.113037,-1325.827026,5.753020>> + <<13.250000,44.000000,2.500000>>, <<-1329.113037,-1325.827026,5.753020>> - <<13.250000,44.000000,2.500000>>, FALSE)
				SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<< -1361.5677, -1207.0526, 3.4492 >>, 10, 0)
				SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<< -1396.3402, -1059.0491, 3.2013 >>, 15, 0)
				SET_ENTITY_LOAD_COLLISION_FLAG(pedJimmy,TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedJimmy, TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(vehJimmyBike, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehJimmyBike, TRUE)
				BUILD_PATH_FOR_RECORDING(vBikeRoutePath, i_jimmy_bike_rec, s_jimmy_bike_rec)
				iStageProgress++
				SET_PED_WALLA_DENSITY(0.5, 0.5)
				FADE_IN()
				
				//disable dispatch services
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, FALSE)
				
				REQUEST_IPL("smboat")
				
			BREAK
			
			CASE 1
				IF IS_PED_IN_VEHICLE(pedJimmy, vehJimmyBike)
					TASK_VEHICLE_DRIVE_TO_COORD(pedJimmy, vehJimmyBike, <<-1092.3923, -1708.0920, 3.2771>>, 7, DRIVINGSTYLE_NORMAL, CRUISER, DRIVINGMODE_AVOIDCARS | DF_StopForPeds, 3.0, 4.0)
					iStageProgress++
				ELSE
					IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
						TASK_ENTER_VEHICLE(pedJimmy, vehJimmyBike, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_SPRINT)
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF IS_ENTITY_AT_COORD(vehJimmyBike, <<-1092.3923, -1708.0920, 3.2771>>, <<4,4,4>>)
					CLEAR_PED_TASKS(pedJimmy)
					START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehJimmyBike, i_jimmy_bike_rec, s_jimmy_bike_rec, 15, DRIVINGMODE_AVOIDCARS | DF_SteerAroundPeds | DF_DontSteerAroundPlayerPed)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 3
			
				fPlayerBikePathProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), i_jimmy_bike_rec, s_jimmy_bike_rec, vBikeRoutePath)
				vPlayerPathPoint = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(i_jimmy_bike_rec, fPlayerBikePathProg, s_jimmy_bike_rec)
				fPlayerDistanceFromTrack = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayerPathPoint)
				
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayerPathPoint, 0, 255, 0)
					DRAW_DEBUG_SPHERE(vPlayerPathPoint, 0.4, 0, 255, 0)
					DRAW_DEBUG_TEXT_ABOVE_COORDS(vPlayerPathPoint, GET_STRING_FROM_FLOAT(fPlayerBikePathProg), 2.0)
				#ENDIF
				
				IF bGoBackToPath
				
					IF fPlayerDistanceFromTrack < 75
						REMOVE_BLIP(blipDest)
						bGoBackToPath = FALSE
					ENDIF
					
					IF NOT bGoBackPrinted
						PRINT_NOW("FM2_RIDEBK", DEFAULT_GOD_TEXT_TIME, 1)
						iTimeGoBackPrinted = GET_GAME_TIMER()
						bGoBackPrinted = TRUE
					ENDIF
					
					IF fPlayerDistanceFromTrack > 300
						Mission_Failed("FM2_RACEFAIL")
					ENDIF
					
				ELSE
				
					IF (GET_GAME_TIMER() - iTimeGoBackPrinted) > 20000
						bGoBackPrinted = FALSE
					ENDIF
					
					IF DOES_BLIP_EXIST(blipPlayerVeh)
						IF DOES_BLIP_EXIST(blipBuddy)
							REMOVE_BLIP(blipBuddy)
						ENDIF
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayerBike)) > 150*150
							Mission_Failed("FM2_BIKLOST")						
						ENDIF
					ELSE
						IF NOT bJimmyWon
							IF NOT DOES_BLIP_EXIST(blipBuddy)
								blipBuddy = CREATE_BLIP_FOR_VEHICLE(vehJimmyBike)
								SET_BLIP_NAME_FROM_TEXT_FILE(blipBuddy, "BLIP_280")
							ENDIF
						ENDIF
					ENDIF
					
					IF fPlayerDistanceFromTrack > 150
						REMOVE_BLIP(blipDest)
						REMOVE_BLIP(blipBuddy)
						CLEAR_GPS_MULTI_ROUTE()
						iCurrentBikePathNode = COUNT_OF(vBikeGPSNodes) - 1
						bGoBackToPath = TRUE
					ENDIF
					
				ENDIF
				
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<15,15,10>>)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF (GET_GAME_TIMER() - iJimmyBreatheTime) > GET_RANDOM_INT_IN_RANGE(1200, 1400)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "BREATHING", "JIMMY_NORMAL")
						#IF IS_DEBUG_BUILD 
							PRINTLN("BREATHING") 
						#ENDIF
						iJimmyBreatheTime = GET_GAME_TIMER()
					ENDIF
				ENDIF
				
				IF IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(pedJimmy) + <<3,3,3>>, GET_ENTITY_COORDS(pedJimmy) - <<3,3,3>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, GET_ENTITY_COORDS(pedJimmy) + <<3,3,3>>, GET_ENTITY_COORDS(pedJimmy) - <<3,3,3>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, GET_ENTITY_COORDS(pedJimmy) + <<3,3,3>>, GET_ENTITY_COORDS(pedJimmy) - <<3,3,3>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, GET_ENTITY_COORDS(pedJimmy) + <<3,3,3>>, GET_ENTITY_COORDS(pedJimmy) - <<3,3,3>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_CAR, GET_ENTITY_COORDS(pedJimmy) + <<3,3,3>>, GET_ENTITY_COORDS(pedJimmy) - <<3,3,3>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_BIKE, GET_ENTITY_COORDS(pedJimmy) + <<3,3,3>>, GET_ENTITY_COORDS(pedJimmy) - <<3,3,3>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_PROGRAMMABLEAR, GET_ENTITY_COORDS(pedJimmy) + <<3,3,3>>, GET_ENTITY_COORDS(pedJimmy) - <<3,3,3>>)			
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedJimmy, KNOCKOFFVEHICLE_EASY)
				ENDIF
				
				IF bJimmyWon
				
					IF NOT b_cutscene_request
						PRINTLN("\n\n\n PLAYING JIMMY WINS CUTSCENE \n\n\n")
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_2_mcs_2", CS_SECTION_1 | CS_SECTION_3)
						bCutsceneComponentsSet = FALSE
						b_cutscene_request = TRUE
					ENDIF
					
					IF b_cutscene_request
					AND NOT bCutsceneComponentsSet
						PRINTLN("Jimmy has finished")
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							PRINTLN("SETTING components from player jimmy win")
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
							bCutsceneComponentsSet = TRUE
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1811.905518,-1195.154175,11.767201>>, <<-1846.905518,-1237.279419,19.517199>>, 21.250000)
					OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1816.365234,-1234.039429,7.017199>>, <<-1859.196655,-1241.141235,22.870972>>, 57.750000)
					OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1856.623291,-1201.831787,7.017204>>, <<-1859.196655,-1241.141235,22.870972>>, 57.750000)
						IF bCutsceneComponentsSet
							REMOVE_BLIP(blipDest)
							REMOVE_BLIP(blipBuddy)
							REPLAY_RECORD_BACK_FOR_TIME(12.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							GO_TO_STAGE(stage_telltale)
						ENDIF
					ENDIF
					
				ELSE
				
					KEEP_JIMMY_ON_REC_PATH()
					
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayerOnPierPosition) < 100*100
						IF NOT b_cutscene_request
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_2_mcs_2", CS_SECTION_2 | CS_SECTION_3)
							b_cutscene_request = TRUE
						ELSE
							IF NOT bCutsceneComponentsSet
							AND CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
								PRINTLN("SETTING components from player")
								SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
								bCutsceneComponentsSet = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRaceFinish, g_vAnyMeansLocate, TRUE)
							REMOVE_BLIP(blipDest)
							REMOVE_BLIP(blipBuddy)
							PRINTLN("\n\n\n PLAYING MIKE WINS CUTSCENE \n\n\n")
							iStageProgress++
						ENDIF
					ENDIF
					
				ENDIF
								
			BREAK
			
			CASE 4
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerBike, 30,1)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					TASK_LOOK_AT_COORD(PLAYER_PED_ID(), <<-1823.1949, -1213.5262, 12.1520>>, -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 5
				IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
				AND NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
					SETTIMERA(0)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 6
				IF TIMERA() > 800
					REPLAY_RECORD_BACK_FOR_TIME(12.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					GO_TO_STAGE(stage_telltale)
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF NOT IS_PED_INJURED(pedJimmy)
		AND IS_VEHICLE_DRIVEABLE(vehJimmyBike)
		
			IF HAS_PLAYER_SPOOKED_JIMMY_WITH_SHOOTING(bRecentShot, iShootCount, iLastShootTime)
				Mission_Failed("FM2_JSPOOK")
			ENDIF
		
			IF NOT bJimmyFinished
				IF iStageProgress < 4
					FLOAT distance_between_peds
					fPlayerProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), i_jimmy_bike_rec, s_jimmy_bike_rec, vBikeRoutePath)
					fJimmyProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(pedJimmy), i_jimmy_bike_rec, s_jimmy_bike_rec, vBikeRoutePath)
					distance_between_peds = fJimmyProg - fPlayerProg
					MANAGE_JIMMY_BIKE_CONV(distance_between_peds, fPlayerDistanceFromTrack, fPlayerProg > fJimmyProg)
					IF IS_ENTITY_AT_COORD(pedJimmy, vRaceFinish, g_vAnyMeansLocate, FALSE)
						SET_ENTITY_COLLISION(vehJimmyBike, TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedJimmy, TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehJimmyBike, TRUE)
						IF NOT bJimmyWon
							REMOVE_CUTSCENE()
							b_cutscene_request = FALSE
							bJimmyWon = TRUE
						ENDIF
						bJimmyFinished = TRUE
					ENDIF
				ELSE
					IF NOT bJimmyWon
					AND IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-1806.987427,-1191.762207,-42.982376>>, <<-1854.439697,-1247.811401,30.481899>>, 90.000000)
					AND VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 36
						REMOVE_BLIP(blipDest)
						REMOVE_BLIP(blipBuddy)
						GO_TO_STAGE(stage_telltale)
					ENDIF
				ENDIF
			ENDIF
			
			IF bJimmyFinished
			OR (iStageProgress >= 4 AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike))
				IF IS_PED_IN_VEHICLE(pedJimmy, vehJimmyBike)
					IF NOT IS_ENTITY_OCCLUDED(pedJimmy)
					AND VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 50*50
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
						ENDIF
					ELSE
						STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
						FREEZE_ENTITY_POSITION(vehJimmyBike, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF iStageProgress < 4 AND iStageProgress > 0
		
			IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
			AND DOES_ENTITY_EXIST(vehPlayerBike)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerBike)
					IF NOT DOES_BLIP_EXIST(blipPlayerVeh)
						CLEAR_PRINTS()
						IF bBeenOnBike
						PRINT_GOD_TEXT(gtGetBackInBike)
						ENDIF
						blipPlayerVeh = CREATE_BLIP_FOR_VEHICLE(vehPlayerBike)
					ENDIF
					IF DOES_BLIP_EXIST(blipDest)
						REMOVE_BLIP(blipDest)
						iCurrentBikePathNode = COUNT_OF(vBikeGPSNodes) - 1
						CLEAR_GPS_MULTI_ROUTE()
					ENDIF
					IF DOES_BLIP_EXIST(blipBuddy)
						REMOVE_BLIP(blipBuddy)
					ENDIF
				ELSE
				
					bBeenOnBike = TRUE
									
					IF bGoBackToPath
						IF NOT DOES_BLIP_EXIST(blipDest)
							FLOAT fBLipTime = CLAMP(fPlayerBikePathProg+ 10000.0, 20000.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(i_jimmy_bike_rec, s_jimmy_bike_rec))
							blipDest = CREATE_BLIP_FOR_COORD(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(i_jimmy_bike_rec, fBLipTime, s_jimmy_bike_rec), TRUE)
						ELSE
							//VECTOR vCurrentBlipCoords = GET_BLIP_COORDS(blipDest)
							//SET_BLIP_COORDS(blipDest, vCurrentBlipCoords + (vPlayerPathPoint - vCurrentBlipCoords)/10.0)
						ENDIF
					ELSE
						IF NOT DOES_BLIP_EXIST(blipDest)
							CLEAR_PRINTS()
							blipDest = CREATE_BLIP_FOR_COORD(vRaceFinish, FALSE)
						ENDIF
						BUILD_CUSTOM_GPS_ROUTE(vBikeGPSNodes, iCurrentBikePathNode)
					ENDIF
					IF DOES_BLIP_EXIST(blipPlayerVeh)
						REMOVE_BLIP(blipPlayerVeh)
					ENDIF
				ENDIF
			ENDIF
			
//			BLIP_MANAGER(vehPlayerBike, vRaceFinish, TRUE, FALSE)
		ENDIF
		
	ENDIF
	
	BIKE_TUTORIAL()
	
	#IF IS_DEBUG_BUILD
		FLOAT fTempHeight = 0.06
		DRAW_DEBUG_TEXT_2D("BIKE RIDE DEBUG", <<0.02, fTempHeight, 0>>) fTempHeight+= 0.02
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iStageProgress), <<0.02, fTempHeight, 0>>) fTempHeight+= 0.02
	#ENDIF
	
ENDPROC

//BOOL bSwimHelp[5]
//INT iHelpTimes[5]
//BOOL bHelpUsed[5]
//BOOL bHelpCleared[5]
STRING sSwimHelps[6]
//BOOL bUnderwaterHelpCleared = FALSE
//BOOL bSurfaceHelpCleared = FALSE

INT iJimmyShoutStage = 0
INT iCurrentJimmyShoutAnim
INT iLastAnimCheck
STRING sJimmyShoutAnims[3]
//
//BOOL bDrownHelpNeeded = FALSE
//BOOL bDrownHelpShown = FALSE

BOOL bNearYachtPlayed = FALSE
BOOL bLoadStreamDone = FALSE
BOOL bMikeRun = FALSE
INT iTimeOfGodText

BOOL bPlayerSeaSharkLocked
BOOL bSwimToYachtASCanStart

ENUM FAM2_SWIM_HELP_STAGE
	SWIMHELP_SURFACE_SWIMMING = 0,
	SWIMHELP_SURFACE_DIVE,
	SWIMHELP_UNDERWATER_SWIMMING,
	SWIMHELP_UNDERWATER_MOVE,
	SWIMHELP_STATS,
	SWIMHELP_DROWN
ENDENUM

FAM2_SWIM_HELP_STAGE eSwimHelpStage
FAM2_SWIM_HELP_STAGE ePreviousSwimHelpStage

BOOL bCurrentHelpShown
BOOL bCurrentHelpUsed
BOOL bCurrentHelpCleared

FLOAT fCurrentHelpPrintTime

INT iSwimHelpBits

FUNC BOOL PRINT_SWIM_HELP(STRING sHelpString, BOOL bForever = FALSE)
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpString)
		CLEAR_HELP()
	ELSE
		IF bForever
			PRINT_HELP_FOREVER(sHelpString)
		ELSE
			PRINT_HELP(sHelpString)
		ENDIF
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpString)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SWIM_HELP_BEING_USED(FAM2_SWIM_HELP_STAGE eSwimHelp)
	SWITCH eSwimHelp
		CASE SWIMHELP_UNDERWATER_SWIMMING
		CASE SWIMHELP_SURFACE_SWIMMING
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
			AND GET_ENTITY_SPEED(PLAYER_PED_ID()) > 4
				RETURN TRUE
			ENDIF
		BREAK
		CASE SWIMHELP_SURFACE_DIVE
			IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		BREAK
		CASE SWIMHELP_UNDERWATER_MOVE
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LR)
			OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UD)
				RETURN TRUE
			ENDIF
		BREAK
		CASE SWIMHELP_DROWN
			IF NOT IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_HELP_RELEVANT(FAM2_SWIM_HELP_STAGE eSwimHelp)
	IF IS_PED_SWIMMING(PLAYER_PED_ID())
		IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
			IF eSwimHelp = SWIMHELP_DROWN
			OR eSwimHelp = SWIMHELP_UNDERWATER_MOVE
			OR eSwimHelp = SWIMHELP_UNDERWATER_SWIMMING
			OR eSwimHelp = SWIMHELP_STATS
				RETURN TRUE
			ENDIF
		ELSE
			IF eSwimHelp = SWIMHELP_SURFACE_DIVE
			OR eSwimHelp = SWIMHELP_SURFACE_SWIMMING
			OR eSwimHelp = SWIMHELP_STATS
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RUN_SWIM_HELP()

	ePreviousSwimHelpStage = eSwimHelpStage
	
	IF IS_PED_SWIMMING(PLAYER_PED_ID())
	
		IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
		
			IF NOT IS_BIT_SET(iSwimHelpBits, ENUM_TO_INT(SWIMHELP_DROWN))
			AND GET_PLAYER_UNDERWATER_TIME_REMAINING(PLAYER_ID()) <= 5.0
			
				IF eSwimHelpStage != SWIMHELP_DROWN
					eSwimHelpStage = SWIMHELP_DROWN
				ENDIF
				
			ELSE
			
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_BIT_SET(iSwimHelpBits, ENUM_TO_INT(SWIMHELP_UNDERWATER_SWIMMING))
						IF eSwimHelpStage != SWIMHELP_UNDERWATER_SWIMMING
							eSwimHelpStage = SWIMHELP_UNDERWATER_SWIMMING
						ENDIF
					ELIF NOT IS_BIT_SET(iSwimHelpBits, ENUM_TO_INT(SWIMHELP_UNDERWATER_MOVE))
						IF eSwimHelpStage != SWIMHELP_UNDERWATER_MOVE
							eSwimHelpStage = SWIMHELP_UNDERWATER_MOVE
						ENDIF
					ELIF NOT IS_BIT_SET(iSwimHelpBits, ENUM_TO_INT(SWIMHELP_STATS))
						IF eSwimHelpStage != SWIMHELP_STATS
							eSwimHelpStage = SWIMHELP_STATS
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		ELSE
		
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_BIT_SET(iSwimHelpBits, ENUM_TO_INT(SWIMHELP_SURFACE_SWIMMING))
					IF eSwimHelpStage != SWIMHELP_SURFACE_SWIMMING
						eSwimHelpStage = SWIMHELP_SURFACE_SWIMMING
					ENDIF
				ELIF NOT IS_BIT_SET(iSwimHelpBits, ENUM_TO_INT(SWIMHELP_SURFACE_DIVE))
					IF eSwimHelpStage != SWIMHELP_SURFACE_DIVE
						eSwimHelpStage = SWIMHELP_SURFACE_DIVE
					ENDIF
				ELIF NOT IS_BIT_SET(iSwimHelpBits, ENUM_TO_INT(SWIMHELP_STATS))
					IF eSwimHelpStage != SWIMHELP_STATS
						eSwimHelpStage = SWIMHELP_STATS
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	IF eSwimHelpStage != ePreviousSwimHelpStage
		bCurrentHelpUsed = FALSE
		bCurrentHelpShown = FALSE
		bCurrentHelpCleared = FALSE
		fCurrentHelpPrintTime = 0.0
	ENDIF
	
	IF NOT bCurrentHelpShown
	
		//Check for stages where first person mode shouldn't display help text
		IF eSwimHelpStage != SWIMHELP_SURFACE_DIVE
		AND eSwimHelpStage != SWIMHELP_UNDERWATER_SWIMMING
		AND eSwimHelpStage != SWIMHELP_UNDERWATER_MOVE
			IF PRINT_SWIM_HELP(sSwimHelps[eSwimHelpStage], FALSE)
				bCurrentHelpShown = TRUE
			ENDIF
		ELSE
			if GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) != CAM_VIEW_MODE_FIRST_PERSON
			//Third person print as normal
		IF PRINT_SWIM_HELP(sSwimHelps[eSwimHelpStage], FALSE)
			bCurrentHelpShown = TRUE
		ENDIF
	ELSE
				//First person skip over help text stage
				bCurrentHelpShown = TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sSwimHelps[eSwimHelpStage])
			fCurrentHelpPrintTime += GET_FRAME_TIME()
			IF fCurrentHelpPrintTime > 2.0
			OR NOT IS_THIS_HELP_RELEVANT(eSwimHelpStage)
				IF NOT IS_BIT_SET(iSwimHelpBits, ENUM_TO_INT(eSwimHelpStage))
					SET_BIT(iSwimHelpBits, ENUM_TO_INT(eSwimHelpStage))
				ENDIF
				IF NOT bCurrentHelpUsed
				AND IS_THIS_HELP_RELEVANT(eSwimHelpStage)
					IF IS_SWIM_HELP_BEING_USED(eSwimHelpStage)
						bCurrentHelpUsed = TRUE
					ENDIF
				ELSE
					IF NOT bCurrentHelpCleared
						CLEAR_HELP(FALSE)
						bCurrentHelpCleared = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

INT iTurtleStage
PED_INDEX piTurtle

PROC UPDATE_TURTLE()

	VECTOR vPlayerCoords

	PRINTLN("iTurtleStage:", iTurtleStage)

	SWITCH iTurtleStage
		CASE 0
			REQUEST_MODEL(A_C_STINGRAY)
			iTurtleStage++
		BREAK
		
		CASE 1
			IF HAS_MODEL_LOADED(A_C_STINGRAY)
				iTurtleStage++
			ENDIF
		BREAK
		
		CASE 2
			vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF vPlayerCoords.z < 1.0
				piTurtle = CREATE_PED(PEDTYPE_MISSION, A_C_STINGRAY, <<-1885.4974, -1189.8828, -8.2945>>, -136.1128)
				TASK_GO_STRAIGHT_TO_COORD(piTurtle, GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVE_WALK)
				iTurtleStage++
			ENDIF
		BREAK
		
		CASE 3
			IF GET_DISTANCE_BETWEEN_ENTITIES(piTurtle, PLAYER_PED_ID()) > 20.0
			OR GET_SCRIPT_TASK_STATUS(piTurtle, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
				SET_MODEL_AS_NO_LONGER_NEEDED(A_C_STINGRAY)
				SET_PED_AS_NO_LONGER_NEEDED(piTurtle)
				iTurtleStage++
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

PROC GO_TO_YACHT()

	UPDATE_TURTLE()
	
	IF (DOES_ENTITY_EXIST(vehPlayerSeashark) AND NOT IS_VEHICLE_DRIVEABLE(vehPlayerSeashark))
	OR (DOES_ENTITY_EXIST(vehChasers[0]) AND NOT IS_VEHICLE_DRIVEABLE(vehChasers[0]))
	OR (DOES_ENTITY_EXIST(vehChasers[1]) AND NOT IS_VEHICLE_DRIVEABLE(vehChasers[1]))
		Mission_Failed("FM2_TSPOOK")
	ENDIF
		
	IF NOT DOES_ENTITY_EXIST(objMP3Dock)
		CREATE_MP3_DOCK()
	ENDIF
	
	INT j
	REPEAT COUNT_OF(vehChasers) j
		IF NOT DOES_ENTITY_EXIST(vehChasers[j])
			CREATE_CHASER(j, TRUE)
		ENDIF
	ENDREPEAT
	
	BOOL bPartyStarted = FALSE
	
	REPEAT COUNT_OF(pedBoatDancer) j
		IF DOES_ENTITY_EXIST(pedBoatDancer[j])
			IF IS_PED_INJURED(pedBoatDancer[j])
			OR IS_ENTITY_DEAD(pedBoatDancer[j])
				Mission_Failed("FM2_TSPOOK")
			ENDIF
			bPartyStarted = TRUE
		ENDIF
	ENDREPEAT
	
	IF bPartyStarted
		IF IS_BULLET_IN_AREA(<< -2033.821, -1031.864, 4.890 >>, 5)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, << -2033.821, -1031.864, 4.890 >>, 10)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, << -2033.821, -1031.864, 4.890 >>, 10)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, << -2033.821, -1031.864, 4.890 >>, 10)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE, << -2033.821, -1031.864, 4.890 >>, 10)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE_LAUNCHER, << -2033.821, -1031.864, 4.890 >>, 10)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_MOLOTOV, << -2033.821, -1031.864, 4.890 >>, 10)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BOAT, << -2033.821, -1031.864, 4.890 >>, 10)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, << -2033.821, -1031.864, 4.890 >>, 10)
		OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<< -2033.821, -1031.864, 4.890 >>, WEAPONTYPE_SMOKEGRENADE, 5, TRUE)
		OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<< -2033.821, -1031.864, 4.890 >>, WEAPONTYPE_RPG, 5, TRUE)
		OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<< -2033.821, -1031.864, 4.890 >>, WEAPONTYPE_GRENADE, 5, TRUE)
		OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<< -2033.821, -1031.864, 4.890 >>, WEAPONTYPE_GRENADELAUNCHER, 5, TRUE)
		OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<< -2033.821, -1031.864, 4.890 >>, WEAPONTYPE_GRENADELAUNCHER_SMOKE, 5, TRUE)
		OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<< -2033.821, -1031.864, 4.890 >>, WEAPONTYPE_SMOKEGRENADE, 5, TRUE)
		OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<< -2033.821, -1031.864, 4.890 >>, WEAPONTYPE_BZGAS, 5, TRUE)
		OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<< -2033.821, -1031.864, 4.890 >>, WEAPONTYPE_MOLOTOV, 5, TRUE)
		OR (IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), << -2033.821, -1031.864, 4.890 >> - <<30,30,30>>, << -2033.821, -1031.864, 4.890 >> + <<30,30,30>>, FALSE) AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID()))
			Mission_Failed("FM2_TSPOOK")
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(blipDest)
		blipDest = CREATE_BLIP_FOR_COORD(<< -2034.37, -1032.23, 4.88 >>)
		SET_BLIP_COLOUR(blipDest, BLIP_COLOUR_BLUE)
		SET_BLIP_SCALE(blipDest, BLIP_SIZE_VEHICLE)
		SET_BLIP_NAME_FROM_TEXT_FILE(blipDest, "FM2_YACHTL")
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_DIVOFF")
				PRINT_HELP("FM2_DIVOFF")
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_STOP", CONV_PRIORITY_HIGH)
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM2_FAST_SWIM)
			PRE_INIT_ASSETS(stage_rescue)
			SETTIMERA(0)
			
			// PC swimming help. This isn't ideal to switch it here, as it won't switch if the player
			// changes control during the swimming section, but it would be too problematic to 
			// incorporate a special case into this custom help code. Steve R LDS.
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				sSwimHelps[SWIMHELP_UNDERWATER_SWIMMING	] = "FM2_USWIM1_KM"
				sSwimHelps[SWIMHELP_SURFACE_SWIMMING	] = "FM2_SWIM_KM"
			ELSE
				//First person mode
				if GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					sSwimHelps[SWIMHELP_UNDERWATER_SWIMMING	] = "FM2_SWIM_FP"
					sSwimHelps[SWIMHELP_SURFACE_SWIMMING	] = "FM2_SWIM_FP"
					
				ELSE
					//Third person
			sSwimHelps[SWIMHELP_UNDERWATER_SWIMMING	] = "FM2_USWIM1"
					sSwimHelps[SWIMHELP_SURFACE_SWIMMING	] = "FM2_SWIM"
				ENDIF
			ENDIF
			
			sSwimHelps[SWIMHELP_UNDERWATER_MOVE		] = "FM2_USWIM2"
			sSwimHelps[SWIMHELP_SURFACE_DIVE		] = "FM2_DIVE"
			sSwimHelps[SWIMHELP_STATS				] = "FM2_STAT1"
			sSwimHelps[SWIMHELP_DROWN				] = "FM2_DROWN"
			
			sJimmyShoutAnims[0] = "shouting_jimmy_idle_a"
			sJimmyShoutAnims[1] = "shouting_jimmy_idle_b"
			sJimmyShoutAnims[2] = "shouting_jimmy_idle_c"
			
			bNearYachtPlayed = FALSE
			iStageProgress++
			iLastJimmyShout = -1
			iTimeOfGodText = -1
			iJimmyShoutStage = -1
			bLoadStreamDone = FALSE
			bSwimToYachtASCanStart = FALSE
			bPlayerSeaSharkLocked = FALSE
			
			b_cutscene_request = FALSE
			
			REQUEST_ANIM_DICT("missfam2ig_3")
			
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			
			IF bReplaySetupRequired
				iStageProgress = -1
			ENDIF
			
			bMikeRun = TRUE
			
		BREAK
		
		CASE -1
			REQUEST_IPL("smboat")
			WHILE NOT IS_IPL_ACTIVE("smBoat")
				WAIT(0)
			ENDWHILE
			IF IS_SCREEN_FADED_OUT()
				PRINTLN("FAMILY 2 CUT EXIT - RUN B")
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_SPRINT, FALSE)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_SPRINT, iRunTime)
			ENDIF
			iStageProgress = 1
			bReplaySetupRequired = FALSE
		BREAK
		
		CASE 1
		
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			IF bMikeRun
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_SPRINT)
				IF TIMERA() < 1000
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1871.083374,-1203.116089,13.016579>>, <<2.500000,2.500000,3.000000>>)
					bMikeRun = FALSE
				ENDIF
			ENDIF
			
			IF iTimeOfGodText = -1
			AND (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
 				PRINT("FM2_YACHT", DEFAULT_GOD_TEXT_TIME, 1)
				iTimeOfGodText = GET_GAME_TIMER()
			ENDIF
			
			FLOAT fDistToJimmy
			fDistToJimmy = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedJimmy))
			
			IF HAS_ANIM_DICT_LOADED("missfam2ig_3")
			
				IF VDIST(GET_ENTITY_COORDS(pedJimmy), <<-1859.2643, -1216.7756, 13.0174>>) > 3
					IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
						CLEAR_PED_TASKS(pedJimmy)
						iJimmyShoutStage = 0
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedJimmy, <<-1859.2643, -1216.7756, 13.0174>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS)
					ENDIF
				ELSE
					IF fDistToJimmy < 30*30
						IF NOT IS_PED_FACING_PED(pedJimmy, PLAYER_PED_ID(), 10)
							IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
							AND IS_ENTITY_PLAYING_ANIM(pedJimmy, "missfam2ig_3", "shouting_jimmy_base")
								CLEAR_PED_TASKS(pedJimmy)
								TASK_TURN_PED_TO_FACE_ENTITY(pedJimmy, PLAYER_PED_ID())
								TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), -1)
								iJimmyShoutStage = 0
							ENDIF
						ELSE
							SWITCH iJimmyShoutStage
								CASE -1
									REQUEST_ANIM_DICT("missfam2ig_3")
									IF HAS_ANIM_DICT_LOADED("missfam2ig_3")
										iJimmyShoutStage = 0
									ENDIF
								BREAK
								CASE 0
									IF GET_GAME_TIMER() - iLastAnimCheck > 1000
										IF NOT IS_ENTITY_PLAYING_ANIM(pedJimmy, "missfam2ig_3", "shouting_jimmy_base")
											TASK_PLAY_ANIM(pedJimmy, "missfam2ig_3", "shouting_jimmy_base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_SECONDARY | AF_UPPERBODY)
										ELSE
											iJimmyShoutStage++
										ENDIF
										iLastAnimCheck = GET_GAME_TIMER()
									ENDIF
								BREAK
								CASE 1
									IF ((GET_GAME_TIMER() - iTimeOfGodText)> 5000 OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
									AND fDistToJimmy < 30*30
									AND (iLastJimmyShout = -1 OR (GET_GAME_TIMER() - iLastJimmyShout) > 10000)
										CLEAR_PED_TASKS(pedJimmy)
										IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_STOP", CONV_PRIORITY_HIGH)
											TASK_PLAY_ANIM(pedJimmy, "missfam2ig_3", sJimmyShoutAnims[iCurrentJimmyShoutAnim], SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
											iLastJimmyShout = GET_GAME_TIMER()
											iJimmyShoutStage++
										ENDIF
									ENDIF
								BREAK
								CASE 2
									IF GET_GAME_TIMER() - iLastJimmyShout > 1000
										IF NOT IS_ENTITY_PLAYING_ANIM(pedJimmy, "missfam2ig_3", sJimmyShoutAnims[iCurrentJimmyShoutAnim])
											CLEAR_PED_TASKS(pedJimmy)
											iCurrentJimmyShoutAnim++
											IF iCurrentJimmyShoutAnim >= COUNT_OF(sJimmyShoutAnims)
												iCurrentJimmyShoutAnim = 0
											ENDIF
											iJimmyShoutStage = 0
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF

			IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_2_DIVE_INTO_WATER")
				IF IS_PED_DIVING(PLAYER_PED_ID())
									
					START_AUDIO_SCENE("FAMILY_2_DIVE_INTO_WATER")
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
			OR VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_back_of_the_boat) < 100
				VECTOR vTemp
				vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
				IF vTemp.z < 2
					IF DOES_ENTITY_EXIST(pedJimmy)
						SET_PED_AS_NO_LONGER_NEEDED(pedJimmy)
					ENDIF
					IF DOES_ENTITY_EXIST(vehPlayerBike)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerBike)
					ENDIF
					IF DOES_ENTITY_EXIST(vehJimmyBike)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehJimmyBike)
					ENDIF
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_DIVE_INTO_WATER")
					STOP_AUDIO_SCENE("FAMILY_2_DIVE_INTO_WATER")
				ENDIF
				iStageProgress++
				SETTIMERA(0)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_DIVOFF")
					CLEAR_HELP(FALSE)
				ENDIF
				REMOVE_ANIM_DICT("missfam2ig_3")
				TRIGGER_MUSIC_EVENT("FAM2_SPLASH")
			ENDIF
			
		BREAK
		
		CASE 2
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_DIVOFF")
			AND TIMERA() > 3000
				SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 3
		
			IF NOT bLoadStreamDone
				IF LOAD_STREAM("FAM2_BOAT_PARTY_MASTER")
					PRINTLN("FAM2_BOAT_PARTY_MASTER loaded")
					PLAY_STREAM_FROM_POSITION(<<-2032.5988, -1032.6392, 4.8831>>)
					bLoadStreamDone = TRUE
				ELSE
					PRINTLN("FAM2_BOAT_PARTY_MASTER loading")
				ENDIF
			ENDIF
			
			RUN_SWIM_HELP()
			
			IF IS_PED_SWIMMING(PLAYER_PED_ID())
			
				IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
					IF NOT bSwimToYachtASCanStart
						bSwimToYachtASCanStart = TRUE
					ENDIF
				ELSE
					IF bSwimToYachtASCanStart
						IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_2_SWIM_TO_YACHT")
							START_AUDIO_SCENE("FAMILY_2_SWIM_TO_YACHT")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2033.124146,-1042.981323,0>>, <<-2028.862915,-1028.328369,8.974350>>, 18.250000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2017.999146,-1035.053223,5.946373>>, <<-2023.154419,-1033.718994,1.287564>>, 3.250000))
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				STOP_STREAM()
				IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_SWIM_TO_YACHT")
					STOP_AUDIO_SCENE("FAMILY_2_SWIM_TO_YACHT")
				ENDIF
								
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 4.0, REPLAY_IMPORTANCE_HIGH)
				
				GO_TO_STAGE(stage_rescue)
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2030.859985,-1035.416504,7.882243>>, <<-2006.246582,-1043.832275,-1.481001>>, 21.250000)
				
				PRINTLN("TRIGGERING NEAR BOAT IN VEHICLE")
				TRIGGER_MUSIC_EVENT("FAM2_NEAR_YACHT")
				bNearYachtPlayed = TRUE
				
				iStageProgress++
			ENDIF
			
			IF NOT bNearYachtPlayed
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-2017.1554, -1040.2144, 1.4499>>) < 20*20
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					PRINTLN("TRIGGERING NEAR BOAT")
					TRIGGER_MUSIC_EVENT("FAM2_NEAR_YACHT")
					bNearYachtPlayed = TRUE
					//PRINT_HELP("FM2_LADDERS")
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 4
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
					STOP_STREAM()
					IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_SWIM_TO_YACHT")
						STOP_AUDIO_SCENE("FAMILY_2_SWIM_TO_YACHT")
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGH)
					
					GO_TO_STAGE(stage_rescue)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF HAS_PLAYER_SPOOKED_JIMMY_WITH_SHOOTING(bRecentShot, iShootCount, iLastShootTime)
		Mission_Failed("FM2_JSPOOK")
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		IF DOES_ENTITY_EXIST(pedJimmy)
		AND VDIST(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 30
			Mission_Failed("FM2_JSPOOK")
		ELSE
			LOSE_THE_COPS()	
		ENDIF
	ENDIF

	IF NOT bPlayerSeaSharkLocked
		IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
			SET_VEHICLE_DOORS_LOCKED(vehPlayerSeashark, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			bPlayerSeaSharkLocked = TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	ENDIF
	
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_back_of_the_boat)>500*500
		Mission_Failed("FM2_TRAAB")
	ENDIF
	
	//quick check to see if the player has pushed jimmy off the pier
	IF NOT IS_PED_INJURED(pedJimmy)
		VECTOR vTempCoord = GET_ENTITY_COORDS(pedJimmy)
		FLOAT fTempZ = vTempCoord.z
		IF fTempZ < 10
			Mission_Failed("FM2_JIMHIT")
		ENDIF
	ENDIF
	
ENDPROC

PROC DO_GET_BACK_ON_REMINDER()
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
	AND GET_GAME_TIMER() - iTimeGetBackOnPrinted > 5000
		IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
		AND NOT IS_PED_INJURED(pedTracy)
		AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
		AND IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
		AND IS_ENTITY_IN_WATER(vehPlayerSeashark)
		AND GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) != vehPlayerSeashark
		AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedTracy)) < 35
		AND NOT IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
		AND gtGetBackInSeashark.played
			IF (GET_GAME_TIMER() - iTimeOfLastMichaelLine) > 6000
				IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_GETOFF", CONV_PRIORITY_HIGH)
					PRINTLN("GET BACK ON REMINDER FIRED")
					iTimeOfLastMichaelLine = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iConvStage = 0
FLOAT fWhineTime = 0

PROC DRIVE_BACK()

	BOOL bCanTalk
	BOOL bDone
	
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	
	IF NOT IS_PED_INJURED(pedTracy)
	
//		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
//			MISSION_FAILED("FM2_TSPOOK")
//		ENDIF
		
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_back_of_the_boat) < 10
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ENDIF
		
		IF iStageProgress < 100
			IF IS_SEASHARK_GROUNDED(vehPlayerSeashark, fPlayerGroundTime, bPlayerGroundingChecked)
			OR NOT IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
				iStageProgress = 100
			ENDIF
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_2_JETSKI_GO_TO_BEACH")
			START_AUDIO_SCENE("FAMILY_2_JETSKI_GO_TO_BEACH")			
		ENDIF
		
		SWITCH iStageProgress
		
			CASE 0
				IF /*NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND*/ (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					//IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_TALK1", CONV_PRIORITY_HIGH)
						IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_2_JETSKI_GO_TO_BEACH")
							START_AUDIO_SCENE("FAMILY_2_JETSKI_GO_TO_BEACH")			
						ENDIF
						gtGetBackInSeashark.played = FALSE
//						IF DOES_ENTITY_EXIST(pedJimmy)
//							PRINTLN("Jimmy exists, deleting Jimmy")
//							DELETE_PED(pedJimmy)
//						ELSE
//							PRINTLN("Jimmy does not exist in stage 0")
//						ENDIF
						iConvStage = 0
						fWhineTime = 0
						iJimmyWalkStage = 0
						b_cutscene_request = FALSE
						iStageProgress++
					//ENDIF
					IF bReplaySetupRequired
						iStageProgress =  -1
					ENDIF
				ENDIF
			BREAK
			
			CASE -1
				REQUEST_IPL("smboat")
				WHILE NOT IS_IPL_ACTIVE("smBoat")
					REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_DaddysLittleGirl")
					WAIT(0)
				ENDWHILE
				iStageProgress = 1
			BREAK
			
			CASE 1
				BLIP_MANAGER_FOR_COORD(vehPlayerSeashark, blipPlayerVeh, vFinalDest, blipDest, gtGetBackInSeashark)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					TASK_CLEAR_LOOK_AT(pedTracy)
					SET_ENTITY_PROOFS(vehPlayerSeashark, FALSE, FALSE,FALSE,FALSE,FALSE)
					SET_WANTED_LEVEL_MULTIPLIER(0.2)
					IF NOT bReplaySetupRequired
						PRINT_NOW("FM2_DRIVE", 5000, 1)
					ENDIF
					SETTIMERA(0)
					IF bReplaySetupRequired
						iStageProgress = -2
					ELSE
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE -2
				BLIP_MANAGER_FOR_COORD(vehPlayerSeashark, blipPlayerVeh, vFinalDest, blipDest, gtGetBackInSeashark)
				IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_TALK1", CONV_PRIORITY_HIGH)
					bReplaySetupRequired = FALSE
					iStageProgress = -3
				ENDIF
			BREAK
			
			CASE -3
				BLIP_MANAGER_FOR_COORD(vehPlayerSeashark, blipPlayerVeh, vFinalDest, blipDest, gtGetBackInSeashark)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					SETTIMERA(0)
					PRINT_NOW("FM2_DRIVE", 5000, 1)
					iStageProgress = 2
				ENDIF
			BREAK
			
			
			CASE 2
			
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
				
					PRINTLN("Timer A ", TIMERA())
					IF TIMERA() > 5000
					OR (TIMERA() > 2000 AND GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_PED_HEADTRACKING_PED(pedTracy, PLAYER_PED_ID())
								TASK_LOOK_AT_ENTITY(pedTracy, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
							ENDIF
							iTimeOfLastMichaelLine = GET_GAME_TIMER()
							bTraceySpotsJimmy = FALSE
							bMikeCompAcquired = FALSE
							in_car_conv_started = FALSE
							in_car_conv_on_hold = FALSE
							iStageProgress++
						ENDIF
					ENDIF
					
					BLIP_MANAGER_FOR_COORD(vehPlayerSeashark, blipPlayerVeh, vFinalDest, blipDest, gtGetBackInSeashark)
					
				ENDIF
				
			BREAK
			
			CASE 3
			
				bCanTalk = IS_VEHICLE_DRIVEABLE(vehPlayerSeashark) AND IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
				
				SWITCH iConvStage
				
					CASE 0
					
						IF NOT bKilledOne AND NOT bKilledBoth
							IF IS_REPLAY_IN_PROGRESS()
								MANAGE_CONVERSATION("FAM2_TALK2", bCanTalk)
							ELSE
								MANAGE_CONVERSATION("FAM2_TALK2b", bCanTalk)
							ENDIF
						ELSE	
							MANAGE_CONVERSATION("FAM2_KILTALK", bCanTalk)
						ENDIF
						
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
						AND NOT in_car_conv_on_hold
							in_car_conv_started = FALSE
							in_car_conv_on_hold = FALSE
							TASK_CLEAR_LOOK_AT(pedTracy)
							iConvStage++
						ENDIF
						
					BREAK
					
					CASE 1
					
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_jimmy_end_pos) > 100*100
							IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark) 
							AND IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark) 
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
							AND GET_ENTITY_SPEED(vehPlayerSeashark) < 5// OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_jimmy_end_pos) > 400*400)
								fWhineTime += GET_FRAME_TIME()
							ELSE
								IF fWhineTime > 0
									fWhineTime -= GET_FRAME_TIME()*5
								ENDIF
							ENDIF
						ENDIF
						
						IF fWhineTime > 10.0
							IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_GOBACK", CONV_PRIORITY_HIGH)
								fWhineTime = 0
							ENDIF
						ENDIF
						
						IF NOT bKilledOne
						AND NOT bKilledBoth
							//MANAGE_CONVERSATION(dhSeashark.label, bCanTalk)
						ENDIF
						
					BREAK
					
				ENDSWITCH
				
				DO_GET_BACK_ON_REMINDER()
				
				IF NOT IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
					IF GET_SCRIPT_TASK_STATUS(pedTracy, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
						TASK_ENTER_VEHICLE(pedTracy, vehPlayerSeashark, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
					ENDIF
				ENDIF
				
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
				
					IF NOT IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
						IF NOT DOES_BLIP_EXIST(blipBuddy)
							CLEAR_PRINTS()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF NOT bGoBackToTracyPrinted
								PRINT("FM2_WAIT2", DEFAULT_GOD_TEXT_TIME, 1)
								bGoBackToTracyPrinted = TRUE
							ENDIF
							blipBuddy = CREATE_BLIP_FOR_PED(pedTracy)
						ENDIF
						IF DOES_BLIP_EXIST(blipDest)
							REMOVE_BLIP(blipDest)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(blipBuddy)
							REMOVE_BLIP(blipBuddy)
						ENDIF
					ENDIF
					
					IF IS_SEASHARK_GROUNDED(vehPlayerSeashark, fPlayerGroundTime, bPlayerGroundingChecked)
						IF VDIST(GET_ENTITY_COORDS(vehPlayerSeashark), vFinalDest) < 50
							bDone = TRUE
						ENDIF
					ENDIF
					
					IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBeachPos1, vBeachPos2, fBeachWidth) OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vFinalDest+<<0,0,0.3>>, <<1,1,2.5>>, TRUE))
					AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
						bDone = TRUE
					ENDIF
					
					IF bDone
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						REMOVE_BLIP(blipDest)
						bPlayFullFinalCut = TRUE
						iStageProgress++
					ENDIF
					
				ENDIF
				
				IF NOT bDone
					IF IS_SEASHARK_GROUNDED(vehPlayerSeashark, fPlayerGroundTime, bPlayerGroundingChecked)
					OR NOT IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
						iStageProgress = 100
					ELSE
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_jimmy_end_pos) < 10000
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
								IF GET_ENTITY_HEIGHT_ABOVE_GROUND(pedTracy) < 2.0
									iStageProgress = 100
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF iStageProgress < 100
					BLIP_MANAGER_FOR_COORD(vehPlayerSeashark, blipPlayerVeh, vFinalDest, blipDest, gtGetBackInSeashark)
				ENDIF
				
			BREAK
			
			CASE 4
				PRINTLN("BRINGING SEASHARK TO HALT")
				KILL_FACE_TO_FACE_CONVERSATION()
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerSeashark, 5, 1)
				OR NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					KILL_FACE_TO_FACE_CONVERSATION()
					PRINTLN("SEASHARK HALTED")
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 5
				PRINTLN("WAITING ON CONV STATUS TO BE FREE")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
				
					GO_TO_STAGE(stage_finish)
				ENDIF
			BREAK
			
			//a little transition to on foot
			CASE 100
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_jimmy_end_pos) < POW(100,2)
					IF NOT IS_PED_INJURED(pedJimmy)
						CLEAR_HELP()
						sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						in_car_conv_on_hold = TRUE
						REMOVE_BLIP(blipDest)
						sFinalCut = "family_2_mcs_4p2"
						PRINTLN("@@@@@@@@@@ CUTSCENE sFinalCut = family_2_mcs_4p2 @@@@@@@@@@@")
						b_cutscene_request = FALSE
						bMikeCompAcquired = FALSE
						REMOVE_CUTSCENE()
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_2_mcs_4", CS_SECTION_2)
						PRINTLN("@@@@@@@@@@ CUTSCENE REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(family_2_mcs_4, CS_SECTION_2) @@@@@@@@@@@")
						SETTIMERA(0)
						IF DOES_BLIP_EXIST(blipPlayerVeh)
							REMOVE_BLIP(blipPlayerVeh)
						ENDIF
						blipDest = CREATE_BLIP_FOR_PED(pedJimmy)
						PRINT_NOW("FM2_ONFOOT", DEFAULT_GOD_TEXT_TIME, 1)
						IF NOT IS_PED_GROUP_MEMBER(pedTracy, GET_PLAYER_GROUP(PLAYER_ID()))
							SET_PED_AS_GROUP_MEMBER(pedTracy, GET_PLAYER_GROUP(PLAYER_ID()))
							SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedTracy, VS_FRONT_RIGHT)
						ENDIF
						bGoBackToTracyPrinted = FALSE
						iStageProgress++
					ENDIF
				ELSE
					IF IS_ENTITY_DEAD(vehPlayerSeashark)
						Mission_Failed("FM2_SKIDED")
					ELSE
						Mission_Failed("FM2_SKIGRN")
					ENDIF
				ENDIF
			BREAK
			
			CASE 101
			
				IF NOT IS_PED_INJURED(pedJimmy)
					IF IS_PED_GROUPED(pedTracy)
						IF NOT DOES_BLIP_EXIST(blipDest)
							blipDest = CREATE_BLIP_FOR_PED(pedJimmy)					
						ENDIF
						IF DOES_BLIP_EXIST(blipBuddy)
							REMOVE_BLIP(blipBuddy)
						ENDIF
						IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedJimmy)) < 30
							SETTIMERA(0)
							iStageProgress++
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(blipDest)
							REMOVE_BLIP(blipDest)
						ENDIF
						IF NOT DOES_BLIP_EXIST(blipBuddy)
							IF NOT bGoBackToTracyPrinted
								PRINT("FM2_TRAGB", DEFAULT_GOD_TEXT_TIME, 1)
							ENDIF
							blipBuddy = CREATE_BLIP_FOR_PED(pedTracy)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bKilledOne
				AND NOT bKilledBoth
					bCanTalk = FALSE
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_PED_IN_ANY_VEHICLE(pedTracy)
						AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedTracy)) < 20*20
							bCanTalk = TRUE
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(pedTracy)
						AND GET_VEHICLE_PED_IS_IN(pedTracy) = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							bCanTalk = TRUE
						ENDIF
					ENDIF
//					MANAGE_CONVERSATION(dhSeashark.label, bCanTalk)
				ENDIF
				
			BREAK
			
			CASE 102
				IF TIMERA() > 1000
					bPlayFullFinalCut = FALSE
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 103
				KILL_FACE_TO_FACE_CONVERSATION()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					GO_TO_STAGE(stage_finish)
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF iStageProgress > 0
			PRE_INIT_ASSETS(stage_finish)
		ENDIF
		
		PRE_STREAM_CUTSCENE_AT_DISTANCE(sFinalCut, v_jimmy_end_pos, DEFAULT_CUTSCENE_LOAD_DIST, b_cutscene_request)
		PRINTLN("@@@@@@@@@@ CUTSCENE PRE_STREAM_CUTSCENE_AT_DISTANCE @@@@@@@@@@@")
		
		IF b_cutscene_request
			IF NOT bMikeCompAcquired
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					PRINTLN("@@@@@@@@@@ CUTSCENE SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(Tracy, pedTracy) @@@@@@@@@@@")
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tracy", pedTracy)
					SET_CUTSCENE_PED_PROP_VARIATION("Tracy", ANCHOR_EYES, 1, 0)
					SET_CUTSCENE_PED_PROP_VARIATION("Tracy", ANCHOR_HEAD, 0, 0)
					bMikeCompAcquired = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND GET_GAME_TIMER() - iLastMeleeShout > 7500
				PRINTLN("TRACEY MELEE LINE!")
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedTracy, "GENERIC_SHOCKED_MED", "Tracey", SPEECH_PARAMS_FORCE)
				iLastMeleeShout = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedJimmy)
		
			IF NOT bTraceySpotsJimmy
				IF NOT IS_PED_INJURED(pedTracy)
					IF VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID()))<75*75
					AND VDIST2(GET_ENTITY_COORDS(pedTracy), GET_ENTITY_COORDS(PLAYER_PED_ID()))<15*15
						KILL_FACE_TO_FACE_CONVERSATION()
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_SEES", CONV_PRIORITY_HIGH)
								bTraceySpotsJimmy = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		SWITCH iJimmyWalkStage
			CASE 0
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_jimmy_end_pos) < 90000
					IF NOT IS_SPHERE_VISIBLE(v_jimmy_end_pos, 5)
						CLEAR_AREA(v_jimmy_end_pos, 4, TRUE)
						IF NOT DOES_ENTITY_EXIST(pedJimmy)
						CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, v_jimmy_end_pos, f_jimmy_end_heading)
						ENDIF
						iJimmyWalkStage = 2
					ELSE
						IF NOT DOES_ENTITY_EXIST(pedJimmy)
							CREATE_BUDDY_ON_FOOT(pedJimmy, CHAR_JIMMY, <<-1611.5950, -1121.0092, 1.5440>>, 42.8275)//<<-1609.8441, -1123.1035, 1.3758>>, 42.8275)
						ENDIF
						iJimmyWalkStage++
					ENDIF
				ENDIF	
			BREAK
			CASE 1
				IF NOT IS_PED_INJURED(pedJimmy)
					PRINTLN("Jimmy tasked to coords")
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJimmy, TRUE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedJimmy, v_jimmy_end_pos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, f_jimmy_end_heading)
					iJimmyWalkStage ++
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_PED_INJURED(pedJimmy)
				AND IS_ENTITY_AT_COORD(pedJimmy, v_jimmy_end_pos, g_vOnFootLocate)
					IF VDIST2(GET_ENTITY_COORDS(pedJimmy), GET_ENTITY_COORDS(PLAYER_PED_ID()))<POW(200, 2)
						IF NOT IS_PED_FACING_PED(pedJimmy, PLAYER_PED_ID(), 60)
							IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
								CLEAR_PED_TASKS(pedJimmy)
								TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
								OPEN_SEQUENCE_TASK(seqTurnTask)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
								CLOSE_SEQUENCE_TASK(seqTurnTask)
								TASK_PERFORM_SEQUENCE(pedJimmy, seqTurnTask)
								CLEAR_SEQUENCE_TASK(seqTurnTask)
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(pedJimmy, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
								IF NOT IS_PED_INJURED(pedJimmy)
									TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		PRINTLN("Jimmy walk stage is: ", iJimmyWalkStage)
		
		IF current_mission_stage <> stage_drive_to_beach	
			IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_JETSKI_GO_TO_BEACH")
				STOP_AUDIO_SCENE("FAMILY_2_JETSKI_GO_TO_BEACH")			
			ENDIF
		ENDIF
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			LOSE_THE_COPS()
		ENDIF
		
	ENDIF
	
ENDPROC





//***********************************************************************************************************************************************

//						SPEEDO CHASE

//***********************************************************************************************************************************************

VECTOR vOpenSeaBounds[4]
VECTOR vTunnelBounds[6]
VECTOR vCanalBounds[6]
VECTOR vMarinaBounds[5]
VECTOR vFinalBounds[4]

VECTOR vCanalsPathPos1[5]
VECTOR vCanalsPathPos2[5]
FLOAT fCanalsPathWidth[5]

VECTOR vOpenWaterPath1[4]
VECTOR vOpenWaterPath2[4]
VECTOR vTunnelPath1[15]
VECTOR vTunnelPath2[15]
VECTOR vMarinaPath1[8]
VECTOR vMarinaPath2[8]
VECTOR vCurrentCanalPath[2][8]
VECTOR vAltCanalPath[8]
VECTOR vFinalPath1[6]
VECTOR vFinalPath2[6]

BOOL bChaserPlaybackForced[2]
BOOL bForward
BOOL bPlayerBehind[2]
BOOL bChaserLost[2]
BOOL bEndForced
BOOL bCanLosePeds
BOOL bCanalCatchupDone

INT iLastForwardsCheck

BOOL bVehicleBlips[2]

INT iTraceyPanicTimer

INT iSeaSharkHelpStage
INT iTimeSeasharkHelpShown
BOOL bSeasharkHelpShown
BOOL bSeasharkHelpCleared
FLOAT fPresstime
FLOAT fGlobalSpeedVal = 0

BOOL bPlayerShouldDie = FALSE
BOOL bPlayerIdleInCanals = FALSE
BOOL bPedsSetToKillPlayer = FALSE
FLOAT fKillTime = 0

FUNC BOOL IS_CHASER_BEHIND_TRACEY(INT iIndex)
	IF NOT IS_PED_INJURED(pedChasers[iIndex])
	AND NOT IS_PED_INJURED(pedTracy)
		VECTOR vTowardsChaser = GET_ENTITY_COORDS(pedChasers[iIndex]) - GET_ENTITY_COORDS(pedTracy)
		VECTOR vTraceyForwards = GET_ENTITY_FORWARD_VECTOR(pedTracy)
		IF ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vTraceyForwards.x, vTraceyForwards.y, vTowardsChaser.x, vTowardsChaser.y)) >= 60.0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

INT iLastChaserLine

PROC DO_TRACY_PANIC_LINES(STRING sTraceyRoot, STRING sMichaelRoot, INT iTimer = 8000)

	IF iLastChaserLine > iTraceyPanicTimer
		iTimer = 3000
	ENDIF
	
	IF (GET_GAME_TIMER() - iTraceyPanicTimer) > iTimer
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
	
		INT iWhoSpeaks = GET_RANDOM_INT_IN_RANGE(1, 3)
		
		IF NOT IS_PED_INJURED(pedChasers[0])
		AND IS_VEHICLE_DRIVEABLE(vehChasers[0])
		AND IS_ENTITY_AT_ENTITY(vehChasers[0], vehPlayerSeashark, <<20,20,5>>)
		AND iLastChaserLine < iTraceyPanicTimer
			iWhoSpeaks = GET_RANDOM_INT_IN_RANGE(1, 6)
		ENDIF
		
		PRINTLN("Line in is ", iWhoSpeaks, " Chaser timer: ", iLastChaserLine, " Tracey timer: ", iTraceyPanicTimer)
		
		IF iWhoSpeaks = 1
		AND bKilledOne
			iWhoSpeaks = 2
		ENDIF
		
		IF iWhoSpeaks = 1
		AND NOT IS_STRING_NULL_OR_EMPTY(sMichaelRoot)
			IF CREATE_CONVERSATION(sSpeech, sConvBlock, sMichaelRoot, CONV_PRIORITY_HIGH)
				PRINTLN("MIKE LINE")
				iTraceyPanicTimer = GET_GAME_TIMER()
			ENDIF
		ELIF iWhoSpeaks = 2
			IF ARE_STRINGS_EQUAL(sTraceyRoot, "FAM2_TSHOUT")
				IF IS_CHASER_BEHIND_TRACEY(0)
					INT iRand
					iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
					SWITCH iRand
						CASE 0
							TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_e", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
						BREAK
						CASE 1
							TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_f", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
						BREAK
						CASE 2
							TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_g", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
						BREAK
						CASE 3
							TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_h", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			IF bKilledOne
				IF NOT ARE_STRINGS_EQUAL(sTraceyRoot, "FAM2_ONE")
					sTraceyRoot = "FAM2_ONE"
				ENDIF
			ENDIF
			IF CREATE_CONVERSATION(sSpeech, sConvBlock, sTraceyRoot, CONV_PRIORITY_HIGH)
				PRINTLN("TRACEY LINE")
				iTraceyPanicTimer = GET_GAME_TIMER()
			ENDIF
		ELIF iWhoSpeaks >= 3
			IF DOES_ENTITY_EXIST(sSpeech.PedInfo[7].Index)
			AND NOT IS_PED_INJURED(sSpeech.PedInfo[7].Index)
				IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_BGUY2", CONV_PRIORITY_HIGH)
					PRINTLN("CHASER LINE")
					iLastChaserLine = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_EXCITING_NEAR_BULLET_MISS_ON_PED(PED_INDEX pedToMiss, PED_INDEX sourceOfBullets, INT &iControlTimer, VECTOR sourceOffset, INT timeBetweenBullets = 60, FLOAT minXrange = -3.9, FLOAT maxXrange = -1.0, FLOAT minYRange = -2.9, FLOAT maxYrange = 3.9)
	//Fire bullets at the player as from the bad  guy...
	INT currentBulletTime = GET_GAME_TIMER()
	VECTOR bulletHit
	VECTOR bulletOrigin
	IF ((currentBulletTime - iControlTimer) > timeBetweenBullets)
		IF NOT IS_ENTITY_DEAD(pedToMiss)
		AND NOT IS_ENTITY_DEAD(sourceOfBullets)            
			bulletHit = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedToMiss, <<GET_RANDOM_FLOAT_IN_RANGE(minXrange, maxXrange), GET_RANDOM_FLOAT_IN_RANGE(minYRange, maxYrange), 0.0>>)
			bulletOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sourceOfBullets, sourceOffset)
			GET_GROUND_Z_FOR_3D_COORD(bulletHit, bulletHit.z)           
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(bulletOrigin, bulletHit, 1)
			iControlTimer = currentBulletTime
		ENDIF
	ENDIF
ENDPROC

FLOAT fLostTimes[2]

PROC MANAGE_CHASER_BLIPS(INT iIndex)
	IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
		IF NOT DOES_BLIP_EXIST(blipChasers[iIndex])
			IF NOT IS_PED_INJURED(pedChasers[iIndex])
				IF IS_VEHICLE_DRIVEABLE(vehChasers[iIndex])
				AND IS_PED_IN_VEHICLE(pedChasers[iIndex], vehChasers[iIndex])
					blipChasers[iIndex] = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehChasers[iIndex]))
					SET_BLIP_NAME_FROM_TEXT_FILE(blipChasers[iIndex], "FM2_PURSUER")
					SET_BLIP_COLOUR(blipChasers[iIndex], BLIP_COLOUR_RED)
					SET_BLIP_PRIORITY(blipChasers[iIndex], BLIPPRIORITY_HIGHEST)
					bVehicleBlips[iIndex] = TRUE
				ELSE
					blipChasers[iIndex] = CREATE_BLIP_FOR_PED(pedChasers[iIndex], TRUE)
					bVehicleBlips[iIndex] = FALSE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedChasers[iIndex])
				IF IS_VEHICLE_DRIVEABLE(vehChasers[iIndex])
				AND IS_PED_IN_VEHICLE(pedChasers[iIndex], vehChasers[iIndex])
					IF bVehicleBlips[iIndex]
						VECTOR vBlipPosition    = GET_BLIP_COORDS(blipChasers[iIndex])
						VECTOR vVehiclePosition = GET_ENTITY_COORDS(vehChasers[iIndex])
						vBlipPosition.X = vBlipPosition.X + ((vVehiclePosition.X - vBlipPosition.X) / 25.0)
						vBlipPosition.Y = vBlipPosition.Y + ((vVehiclePosition.Y - vBlipPosition.Y) / 25.0)
						vBlipPosition.Z = vBlipPosition.Z + ((vVehiclePosition.Z - vBlipPosition.Z) / 25.0)
						SET_BLIP_COORDS(blipChasers[iIndex], vBlipPosition)
					ELSE
						REMOVE_BLIP(blipChasers[iIndex])
					ENDIF
				ELSE
					IF bVehicleBlips[iIndex]
						REMOVE_BLIP(blipChasers[iIndex])
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipChasers[iIndex])
					REMOVE_BLIP(blipChasers[iIndex])
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipChasers[iIndex])
			REMOVE_BLIP(blipChasers[iIndex])
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CHECK_IF_CHASER_HAS_BEEN_LOST(INT iChaser, FLOAT fLostDist = 100.0, FLOAT fMaxLostTime = 5.0)
	IF NOT IS_PED_INJURED(pedChasers[iChaser])
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
			FLOAT fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedChasers[iChaser]))
			IF (fDist > fLostDist*fLostDist AND IS_ENTITY_OCCLUDED(pedChasers[iChaser]))
			OR fDist > 300*300
				IF fDist > 300*300
					fLostTimes[iChaser] += GET_FRAME_TIME()*10
				ENDIF
				fLostTimes[iChaser] += GET_FRAME_TIME()
			ELSE
				IF fLostTimes[iChaser] > 0
					fLostTimes[iChaser] -= GET_FRAME_TIME() * 3
				ELSE
					fLostTimes[iChaser] = 0
				ENDIF
			ENDIF
		ENDIF
		PRINTLN("LOST TIME IS ", fLostTimes[iChaser])
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fLostTimes[iChaser]), <<0.08,0.04,0>>)
		#ENDIF
		IF fLostTimes[iChaser] > fMaxLostTime
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_TRACY_AND_PLAYER_ON_SEASHARK()
	IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
	AND NOT IS_PED_INJURED(pedTracy)
		IF IS_PED_SITTING_IN_VEHICLE(pedTracy, vehPlayerSeashark)
		AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RUN_SEASHARK_HELP()
	IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
		SWITCH iSeaSharkHelpStage
			CASE 0
				IF NOT bSeasharkHelpShown
					PRINT_HELP("FM2_JSHLP1", 12000)
					iTimeSeasharkHelpShown = GET_GAME_TIMER()
					bSeasharkHelpShown = TRUE
				ENDIF
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					fPresstime += GET_FRAME_TIME()
				ENDIF
				IF fPresstime > 3.0
				OR GET_GAME_TIMER() - iTimeSeasharkHelpShown > 10000
					fPresstime = 0.0
					iSeaSharkHelpStage++
				ENDIF
			BREAK
			CASE 1
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_JSHLP1")
					IF NOT bSeasharkHelpCleared
						CLEAR_HELP(FALSE)
						bSeasharkHelpCleared = TRUE
					ENDIF
				ELSE
					iTimeSeasharkHelpShown = GET_GAME_TIMER()
					iSeaSharkHelpStage++
				ENDIF
			BREAK
			CASE 2
				IF GET_GAME_TIMER() - iTimeSeasharkHelpShown > 2000
					bSeasharkHelpShown = FALSE
					bSeasharkHelpCleared = FALSE
					iSeaSharkHelpStage++
				ENDIF
			BREAK
			CASE 3
				IF NOT bSeasharkHelpShown
					PRINT_HELP("FM2_JSHLP2", 12000)
					iTimeSeasharkHelpShown = GET_GAME_TIMER()
					bSeasharkHelpShown = TRUE
				ENDIF
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					fPresstime += GET_FRAME_TIME()
				ENDIF
				IF fPresstime > 1.0
				OR GET_GAME_TIMER() - iTimeSeasharkHelpShown > 10000
					iSeaSharkHelpStage++
				ENDIF
			BREAK
			CASE 4
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_JSHLP2")
					IF NOT bSeasharkHelpCleared
						bSeasharkHelpCleared = TRUE
						CLEAR_HELP(FALSE)
					ENDIF
				ELSE
					iSeaSharkHelpStage++
				ENDIF
			BREAK
			CASE 5
				IF GET_GAME_TIMER() - iTimeSeasharkHelpShown > 2000
					bSeasharkHelpShown = FALSE
					bSeasharkHelpCleared = FALSE
					iSeaSharkHelpStage++
				ENDIF
			BREAK
			CASE 6
				IF NOT bSeasharkHelpShown
					PRINT_HELP("FM2_JSHLP3", 12000)
					iTimeSeasharkHelpShown = GET_GAME_TIMER()
					bSeasharkHelpShown = TRUE
				ENDIF
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
					fPresstime += GET_FRAME_TIME()
				ENDIF
				IF fPresstime > 2.0
				OR GET_GAME_TIMER() - iTimeSeasharkHelpShown > 7500
					fPresstime = 0.0
					iSeaSharkHelpStage++
				ENDIF
			BREAK
			CASE 7
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_JSHLP3")
					IF NOT bSeasharkHelpCleared
						CLEAR_HELP(FALSE)
						bSeasharkHelpCleared = TRUE
					ENDIF
				ELSE
					iTimeSeasharkHelpShown = GET_GAME_TIMER()
					iSeaSharkHelpStage++
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_JSHLP1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_JSHLP2")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM2_JSHLP3")
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_POINT_IN_2D_BOUNDS(VECTOR vPoint, VECTOR &vBounds[], BOOL bReport = FALSE)

	IF COUNT_OF(vBounds) < 3
		SCRIPT_ASSERT("Calling IS_POINT_IN_BOUNDS() with a vector array of size < 3")
	ENDIF
	
	BOOL bRet = TRUE
	
	IF NOT IS_POINT_BEHIND_LINE2(vPoint, vBounds[0], vBounds[COUNT_OF(vBounds)-1])
		IF bReport
			PRINTLN("Point is not behind final line")
		ENDIF
		bRet = FALSE
	ENDIF
	
	INT iTemp
	REPEAT COUNT_OF(vBounds)-1 iTemp
		IF NOT IS_POINT_BEHIND_LINE2(vPoint, vBounds[iTemp+1], vBounds[iTemp])
			IF bReport
				PRINTLN("Point is not behind line", iTemp)
			ENDIF
			bRet = FALSE
		ENDIF
	ENDREPEAT
	
	RETURN bRet
	
ENDFUNC

PROC INIT_CHASE_BOUNDS()

	vOpenSeaBounds[0] = <<-2033.7400, -1004.7763, 1>> // to the left of the yacht facing tunnel
	vOpenSeaBounds[1] = <<-1801.4323, -975.5541, 1>>  // the left hand side of the tunnel facing tunnel from yacht
	vOpenSeaBounds[2] = <<-1796.2546, -988.1660, 1>>  // the right hand side of the tunnel facing tunnel from yacht
	vOpenSeaBounds[3] = <<-2012.2445, -1073.7762, 1>> // to the right of the yacht facing tunnel
	
	vTunnelBounds[5] = <<-1241.1379, -933.0004, 1>> // right of tunnel exit leaving tunnel
	vTunnelBounds[4] = <<-1227.2028, -924.2169, 1>> // left of tunnel exit leaving tunnel
	vTunnelBounds[3] = <<-1240.0150, -751.6861, 1>> // far left of the tunnel 2
	vTunnelBounds[2] = <<-1431.8754, -747.5580, 1>> // far left of the tunnel
	vTunnelBounds[1] = vOpenSeaBounds[1] //Border point
	vTunnelBounds[0] = vOpenSeaBounds[2] //Border point
	
	vCanalBounds[0] = <<-988.3516, -908.0377, 1>> 	// Top Left of canals
	vCanalBounds[1] = <<-866.9206, -1116.0399, 1>> 	// Top right of canals
	vCanalBounds[2] = <<-978.9424, -1351.0541, 1>> 	// left of canal exit heading into marina
	vCanalBounds[3] = <<-1007.6198, -1362.3158, 1>> // right of canal exit heading into marina
	vCanalBounds[4] = vTunnelBounds[5] //Border point
	vCanalBounds[5] = vTunnelBounds[4] //Border point
	
	vMarinaBounds[0] = vCanalBounds[3] //Border point
	vMarinaBounds[1] = vCanalBounds[2] //Border point
	vMarinaBounds[2] = <<-844.5858, -1302.0059, 4.1001>> //far left top corner of marina
	vMarinaBounds[3] = <<-768.9856, -1524.3383, 4.9800>> // central top of marina
	vMarinaBounds[4] = <<-1052.3737, -1553.3130, 13.0317>> // central bottom of marina
	
	vFinalBounds[0] = vMarinaBounds[4] //Border point
	vFinalBounds[1] = vMarinaBounds[3] //Border point
	vFinalBounds[2] = <<-1165.8938, -1923.1788, 2.5698>> //left of river mouth
	vFinalBounds[3] = <<-1226.9473, -1873.5306, 4.8338>> //right of river mouth
	
ENDPROC

FUNC FAM2_CHASE_SECTION_ENUM GET_CURRENT_CHASE_ENUM()
	IF IS_POINT_IN_2D_BOUNDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vOpenSeaBounds)
		RETURN FAM2_CHASE_OPEN_SEA
	ENDIF
	IF IS_POINT_IN_2D_BOUNDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTunnelBounds)
		RETURN FAM2_CHASE_TUNNELS
	ENDIF
	IF IS_POINT_IN_2D_BOUNDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCanalBounds)
		IF NOT IS_PED_INJURED(pedChasers[0])
		AND NOT IS_ENTITY_DEAD(pedChasers[0])
			RETURN FAM2_CHASE_CANALS				
		ELIF bCanalCatchupDone
			RETURN FAM2_CHASE_CANALS_SINGLE_CHASER
		ELSE
			RETURN FAM2_CHASE_CANALS_CATCH_UP
		ENDIF
	ENDIF
	IF IS_POINT_IN_2D_BOUNDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMarinaBounds)
		RETURN FAM2_CHASE_MARINA
	ENDIF
	IF IS_POINT_IN_2D_BOUNDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vFinalBounds)
		RETURN FAM2_CHASE_LOSE_PEDS
	ENDIF
	RETURN FAM2_CHASE_SEEK_AND_DESTROY
ENDFUNC

#IF IS_DEBUG_BUILD

	PROC DRAW_CHASE_BOUNDS(VECTOR &vBounds[], BOOL bActiveArea)
	
		INT r,b
		IF bActiveArea
			r = 255
			b = 0
		ELSE
			r = 0
			b = 255
		ENDIF
		
		INT iTemp
		REPEAT COUNT_OF(vBounds)-1 iTemp
			DRAW_DEBUG_LINE(<<vBounds[iTemp].x, vBounds[iTemp].y, 40>>, <<vBounds[iTemp+1].x, vBounds[iTemp+1].y, 40>>, r,0,b)
			IF r = 255
				DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(iTemp), <<vBounds[iTemp].x, vBounds[iTemp].y, 41>>)
			ENDIF
		ENDREPEAT
		
		DRAW_DEBUG_LINE(<<vBounds[COUNT_OF(vBounds)-1].x, vBounds[COUNT_OF(vBounds)-1].y, 40>>, <<vBounds[0].x, vBounds[0].y, 40>>, r,0,b)
		IF r = 255
			DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(COUNT_OF(vBounds)-1), <<vBounds[COUNT_OF(vBounds)-1].x, vBounds[COUNT_OF(vBounds)-1].y, 41>>)
		ENDIF
		
	ENDPROC
	
	PROC DRAW_ALL_CHASE_BOUNDS()
		FAM2_CHASE_SECTION_ENUM eStage = GET_CURRENT_CHASE_ENUM()
		DRAW_CHASE_BOUNDS(vOpenSeaBounds, eStage = FAM2_CHASE_OPEN_SEA)
		DRAW_CHASE_BOUNDS(vTunnelBounds, eStage = FAM2_CHASE_TUNNELS)
		DRAW_CHASE_BOUNDS(vCanalBounds, eStage = FAM2_CHASE_CANALS)
		DRAW_CHASE_BOUNDS(vMarinaBounds, eStage = FAM2_CHASE_MARINA)
		DRAW_CHASE_BOUNDS(vFinalBounds, eStage = FAM2_CHASE_LOSE_PEDS)
	ENDPROC
	
	FUNC STRING GET_STRING_FROM_CHASE_STAGE(FAM2_CHASE_SECTION_ENUM eStage)
		SWITCH eStage
			CASE FAM2_CHASE_OPEN_SEA			RETURN "FAM2_CHASE_OPEN_SEA"			BREAK
			CASE FAM2_CHASE_TUNNELS				RETURN "FAM2_CHASE_TUNNELS"				BREAK
			CASE FAM2_CHASE_CANALS				RETURN "FAM2_CHASE_CANALS"				BREAK
			CASE FAM2_CHASE_MARINA				RETURN "FAM2_CHASE_MARINA"				BREAK
			CASE FAM2_CHASE_SEEK_AND_DESTROY	RETURN "FAM2_CHASE_SEEK_AND_DESTROY"	BREAK
			CASE FAM2_CHASE_LOSE_PEDS			RETURN "FAM2_CHASE_LOSE_PEDS"	BREAK
		ENDSWITCH
		RETURN "INVALID SECTION"
	ENDFUNC
	
#ENDIF

FUNC BOOL INIT_RECORDING_PATHS()

	REQUEST_VEHICLE_RECORDING(101, "famtwo")
	REQUEST_VEHICLE_RECORDING(102, "famtwo")
	REQUEST_VEHICLE_RECORDING(201, "famtwo")
	REQUEST_VEHICLE_RECORDING(202, "famtwo")
	REQUEST_VEHICLE_RECORDING(301, "famtwo")
	REQUEST_VEHICLE_RECORDING(302, "famtwo")
	REQUEST_VEHICLE_RECORDING(1, "famtwoCanalsAlt")
	REQUEST_VEHICLE_RECORDING(1, "famtwoplayer")
	
	IF 	HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "famtwo")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "famtwo")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(201, "famtwo")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(202, "famtwo")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(301, "famtwo")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(302, "famtwo")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "famtwoCanalsAlt")
		BUILD_PATH_FOR_RECORDING(vOpenWaterPath1, 101, "famtwo")
		BUILD_PATH_FOR_RECORDING(vOpenWaterPath2, 102, "famtwo")
		BUILD_PATH_FOR_RECORDING(vTunnelPath1, 201, "famtwo")
		BUILD_PATH_FOR_RECORDING(vTunnelPath2, 202, "famtwo")
		BUILD_PATH_FOR_RECORDING(vMarinaPath1, 301, "famtwo")
		BUILD_PATH_FOR_RECORDING(vMarinaPath2, 302, "famtwo")
		BUILD_PATH_FOR_RECORDING(vFinalPath1, 401, "famtwo")
		BUILD_PATH_FOR_RECORDING(vFinalPath2, 402, "famtwo")
		BUILD_PATH_FOR_RECORDING(vAltCanalPath, 1, "famtwoCanalsAlt")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_CLOSEST_PATH_SECTION(VECTOR point, VECTOR &vPath[])
	INT iTemp
	INT iClosest
	FLOAT closest_distance = 999999
	REPEAT COUNT_OF(vPath)-1 iTemp
		IF VDIST2(GET_CLOSEST_POINT_ON_LINE(point, vPath[iTemp], vPath[iTemp+1]), point) < closest_distance
			iClosest = itemp
			closest_distance = VDIST2(GET_CLOSEST_POINT_ON_LINE(point, vPath[iTemp], vPath[iTemp+1]), point)
		ENDIF
	ENDREPEAT
	RETURN iClosest
ENDFUNC

FUNC FLOAT GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(VECTOR point, INT iRecNumber, STRING sRecName, VECTOR &route[], BOOL bUpdateDisplay = FALSE)

	INT iClosestPathSection = GET_CLOSEST_PATH_SECTION(point, route)
	FLOAT fTimePerNode = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecNumber, sRecName)/(COUNT_OF(route)-1)
	FLOAT fCurrentLineProg = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(point, route[iClosestPathSection], route[iClosestPathSection+1]) * fTimePerNode
	
	IF bUpdateDisplay
		#IF IS_DEBUG_BUILD
			fRecordingTime 			= GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecNumber, sRecName)
			fSectionTime			= fTimePerNode
			fSectionProgressTime	= fCurrentLineProg
			fSectionProgress		= GET_RATIO_OF_CLOSEST_POINT_ON_LINE(point, route[iClosestPathSection], route[iClosestPathSection+1])
			iClosestPoint			= iClosestPathSection
			VECTOR vPoint = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecNumber, fCurrentLineProg + (iClosestPathSection*fTimePerNode)-0.1, sRecName)
			DRAW_DEBUG_SPHERE(vPoint + <<0,0,1>>, 0.2, 0, 255, 0)
		#ENDIF
	ENDIF
	
	RETURN fCurrentLineProg + (iClosestPathSection*fTimePerNode)-0.1
	
ENDFUNC

PROC KEEP_CHASER_ON_PATH(INT iIndex, VECTOR &vPath[], INT iRec, STRING sRec, BOOL bStopPlaybackOnSight = FALSE, BOOL bSlowForCorners = FALSE)

	IF NOT IS_PED_INJURED(pedChasers[iIndex])
	AND NOT IS_ENTITY_DEAD(pedChasers[iIndex])
	AND DOES_ENTITY_EXIST(vehChasers[iIndex])
	AND IS_VEHICLE_DRIVEABLE(vehChasers[iIndex])
	AND IS_PED_IN_VEHICLE(pedChasers[iIndex], vehChasers[iIndex])
	AND IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
	
		FLOAT fPathProg = GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(vehChasers[iIndex]), iRec, sRec, vPath)
		FLOAT fPlayerProgOnPath = GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), iRec, sRec, vPath)
		VECTOR vProgPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRec, fPathProg, sRec)
		VECTOR vPlayerProgPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRec, fPlayerProgOnPath, sRec)
		
		IF NOT bChaserPlaybackForced[iIndex]
		
			IF NOT bPedsSetToKillPlayer
				IF fPathProg < fPlayerProgOnPath  //if the chaser is behind the player on the recording
				AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(vehChasers[iIndex]), 3)		//and they can't be seen and neither can their recording position
				AND NOT IS_SPHERE_VISIBLE(vProgPos, 3)
				AND VDIST2(vPlayerProgPos, GET_ENTITY_COORDS(vehchasers[iIndex])) > 5*5
					PRINTLN("@@@@@@@@@@@ REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME @@@@@@@@@@@")
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
					STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[iIndex])
					START_PLAYBACK_RECORDED_VEHICLE(vehChasers[iIndex], iRec, sRec)
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehChasers[iIndex], <<0,0,0.1>>)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehChasers[iIndex], CLAMP(fPathProg + (iIndex*0.5), 0, 999999))
					bChaserPlaybackForced[iIndex] = TRUE
				ENDIF
			ENDIF
			
			IF eCurrentChaseSection <> FAM2_CHASE_CANALS
				bPlayerBehind[iIndex] = fPathProg > fPlayerProgOnPath
			ELSE
				bPlayerBehind[iIndex] = FALSE
			ENDIF
			
		ELSE
		
			FLOAT fDistToPlayer = VDIST2(vPlayerProgPos, vProgPos)
			FLOAT fDistToOtherChaser = 999999
			
			IF NOT IS_ENTITY_DEAD(vehChasers[0])
			AND NOT IS_ENTITY_DEAD(vehChasers[1])
				fDistToOtherChaser = VDIST2(GET_ENTITY_COORDS(vehChasers[0]), GET_ENTITY_COORDS(vehChasers[1]))
				#IF IS_DEBUG_BUILD
					IF fDistToOtherChaser < 1.5 * 1.5
						DRAW_DEBUG_LINE(GET_ENTITY_COORDS(vehChasers[0]), GET_ENTITY_COORDS(vehChasers[1]), 255, 0, 0)
					ELSE
						DRAW_DEBUG_LINE(GET_ENTITY_COORDS(vehChasers[0]), GET_ENTITY_COORDS(vehChasers[1]), 0, 255, 0)
					ENDIF
				#ENDIF
			ENDIF
			
			IF (IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(vehChasers[iIndex]), 3) AND (fDistToPlayer < 5*5 OR bStopPlaybackOnSight))
			OR fDistToPlayer < 3*3
			OR fPathProg > fPlayerProgOnPath
			OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[iIndex])
			OR fDistToOtherChaser < 1.5*1.5
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[iIndex])
					SET_PLAYBACK_TO_USE_AI(vehChasers[iIndex], DRIVINGMODE_PLOUGHTHROUGH)
				ELSE
					START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehChasers[iIndex], iRec, sRec, 20, DRIVINGMODE_PLOUGHTHROUGH)
				ENDIF
				IF bSlowForCorners
				AND (fPathProg > 0.85 OR fPathProg < 0.15)
					//SET_VEHICLE_FORWARD_SPEED(vehChasers[iIndex], CLAMP(get_entity_SPEED(vehChasers[iIndex]), 0, 5))
				ELSE
					//SET_VEHICLE_FORWARD_SPEED(vehChasers[iIndex], CLAMP(get_entity_SPEED(vehChasers[iIndex]), 0, GET_ENTITY_SPEED(PLAYER_PED_ID())))
				ENDIF
				
				TASK_VEHICLE_CHASE(pedChasers[iIndex], PLAYER_PED_ID())
				
				bChaserPlaybackForced[iIndex] = FALSE
			ELSE
				FLOAT fVariableDist = 40
				FLOAT FMaxSpeed
				IF bSlowForCorners
				AND (fPathProg > 0.8 OR fPathProg < 0.2)
					FMaxSpeed = 1.0
					SET_PLAYBACK_SPEED(vehChasers[iIndex], CLAMP(VDIST(GET_ENTITY_COORDS(vehChasers[iIndex]), GET_ENTITY_COORDS(PLAYER_PED_ID()))*(FMaxSpeed/fVariableDist)*fGlobalSpeedVal, 0.2, FMaxSpeed))
				ELSE
					FMaxSpeed = 1.3
					SET_PLAYBACK_SPEED(vehChasers[iIndex], CLAMP(VDIST(GET_ENTITY_COORDS(vehChasers[iIndex]), GET_ENTITY_COORDS(PLAYER_PED_ID()))*(FMaxSpeed/fVariableDist)*fGlobalSpeedVal, 0.8, FMaxSpeed))
				ENDIF
			ENDIF
			
		ENDIF
		
	ELSE
	
		IF DOES_ENTITY_EXIST(vehChasers[iIndex])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[iIndex])
				STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[iIndex])
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC INIT_CANAL_PATHS()

	vCanalsPathPos1[0] = <<-1237.273926,-923.970032,5.135533>> 
	vCanalsPathPos2[0] = <<-985.134827,-1359.680176,-7.479084>>
	fCanalsPathWidth[0] = 23.000000
	
	vCanalsPathPos1[1] = <<-1003.907837,-914.453613,-1.611186>>
	vCanalsPathPos2[1] = <<-880.547668,-1124.601807,6.150022>> 
	fCanalsPathWidth[1] = 29.000000
	
	vCanalsPathPos1[2] = <<-1169.471313,-1027.086670,5.785080>>
	vCanalsPathPos2[2] = <<-1003.960571,-932.328430,-2.993861>>
	fCanalsPathWidth[2] = 29.000000
	
	vCanalsPathPos1[3] = <<-1116.554321,-1119.791016,5.784778>>
	vCanalsPathPos2[3] = <<-950.776062,-1023.822815,-1.496439>>
	fCanalsPathWidth[3] = 29.000000
	
	vCanalsPathPos1[4] = <<-1062.390869,-1213.473755,-2.315868>>
	vCanalsPathPos2[4] = <<-897.308472,-1116.901001,4.925530>> 
	fCanalsPathWidth[4] = 29.000000
	
ENDPROC

FUNC CANAL_SECTION GET_CURRENT_CANAL_SECTION()
	CANAL_SECTION eCounter
	REPEAT FAM2_CS_NUMBER_OF_SECTIONS eCounter
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), vCanalsPathPos1[eCounter], vCanalsPathPos2[eCounter], fCanalsPathWidth[eCounter])
			RETURN eCounter
		ENDIF
	ENDREPEAT
	RETURN FAM2_CS_INVALID
ENDFUNC

FUNC BOOL IS_PLAYER_HEADING_FORWARD_IN_CANAL_SECTION(CANAL_SECTION eSection)
	VECTOR vPlayerForward = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
	VECTOR vSectionForward = NORMALISE_VECTOR(vCanalsPathPos2[eSection] - vCanalsPathPos1[eSection])
	RETURN ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vSectionForward.x, vSectionForward.y, vPlayerForward.x, vPlayerForward.y)) < 90
ENDFUNC

FUNC STRING GET_CANAL_SECTION_RECORDING_NAME(CANAL_SECTION eSection)
	SWITCH eSection
		CASE FAM2_CS_BOTTOM		RETURN "famtwoCanalsbottom" 	BREAK
		CASE FAM2_CS_TOP		RETURN "famtwoCanalstop" 		BREAK
		CASE FAM2_CS_LEFT		RETURN "famtwoCanalsLeft" 		BREAK
		CASE FAM2_CS_MIDDLE		RETURN "famtwoCanalsMiddle" 	BREAK
		CASE FAM2_CS_RIGHT		RETURN "famtwoCanalsRight" 		BREAK
	ENDSWITCH
	RETURN "famtwoCanalsbottom"
ENDFUNC

FUNC BOOL HAVE_ALL_CANAL_PATHS_LOADED()
	BOOL bAllLoaded = TRUE
	CANAL_SECTION eCounter
	REPEAT FAM2_CS_NUMBER_OF_SECTIONS eCounter
		REQUEST_VEHICLE_RECORDING(1, GET_CANAL_SECTION_RECORDING_NAME(eCounter))
		REQUEST_VEHICLE_RECORDING(2, GET_CANAL_SECTION_RECORDING_NAME(eCounter))
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, GET_CANAL_SECTION_RECORDING_NAME(eCounter))
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(2, GET_CANAL_SECTION_RECORDING_NAME(eCounter))
			bAllLoaded = FALSE
		ENDIF
	ENDREPEAT
	RETURN bAllLoaded
ENDFUNC

PROC KEEP_CHASER_ON_PATH_IN_CANALS(INT iIndex)

	IF bForward
		KEEP_CHASER_ON_PATH(iIndex, vCurrentCanalPath[0], 1, GET_CANAL_SECTION_RECORDING_NAME(ePreviousCanalSection), FALSE, TRUE)
	ELSE
		KEEP_CHASER_ON_PATH(iIndex, vCurrentCanalPath[1], 2, GET_CANAL_SECTION_RECORDING_NAME(ePreviousCanalSection), FALSE, TRUE)
	ENDIF
	
	IF GET_GAME_TIMER() - iLastForwardsCheck > 500
		bForward = IS_PLAYER_HEADING_FORWARD_IN_CANAL_SECTION(ePreviousCanalSection)
		iLastForwardsCheck = GET_GAME_TIMER()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DRAW_PATH(vCurrentCanalPath[0])
		DRAW_PATH(vCurrentCanalPath[1])
	#ENDIF
	
ENDPROC

PROC RUN_CHASERS()

	INT iIndex
	REPEAT 2 iIndex
		IF NOT IS_PED_INJURED(pedchasers[iIndex])
			IF NOT IS_PED_IN_ANY_VEHICLE(pedchasers[iIndex])
			AND IS_ENTITY_IN_WATER(pedchasers[iIndex])
			AND VDIST2(GET_ENTITY_COORDS(pedchasers[iIndex]), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
				EXPLODE_PED_HEAD(pedchasers[iIndex])
			ENDIF
		ENDIF
	ENDREPEAT
	
	SWITCH eCurrentChaseSection
	
		CASE FAM2_CHASE_OPEN_SEA
		
			IF TIMERB() > 5000
				KEEP_CHASER_ON_PATH(0, vOpenWaterPath1, 101, "famtwo", TRUE)
				KEEP_CHASER_ON_PATH(1, vOpenWaterPath2, 102, "famtwo", TRUE)
			ENDIF
			
			IF TIMERB() < 2000
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			ENDIF
			
			IF fGlobalSpeedVal < 1.0
				fGlobalSpeedVal += 0.01
			ELSE
				fGlobalSpeedVal = 1
			ENDIF
			
			#IF IS_DEBUG_BUILD
				DRAW_PATH(vOpenWaterPath1)
				DRAW_PATH(vOpenWaterPath2)
			#ENDIF
			
		BREAK
		
		CASE FAM2_CHASE_TUNNELS
		
			KEEP_CHASER_ON_PATH(0, vTunnelPath1, 201, "famtwo")
			KEEP_CHASER_ON_PATH(1, vTunnelPath2, 202, "famtwo")
			
			#IF IS_DEBUG_BUILD
				GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 201, "famtwo", vTunnelPath1, TRUE)
				DRAW_PATH(vTunnelPath1)
				DRAW_PATH(vTunnelPath2)
			#ENDIF
			
		BREAK
		
		CASE FAM2_CHASE_CANALS
		
			eCurrentCanalSection = GET_CURRENT_CANAL_SECTION()
			
			IF eCurrentCanalSection != FAM2_CS_INVALID
				IF eCurrentCanalSection != ePreviousCanalSection
					BUILD_PATH_FOR_RECORDING(vCurrentCanalPath[0], 1, GET_CANAL_SECTION_RECORDING_NAME(eCurrentCanalSection))
					BUILD_PATH_FOR_RECORDING(vCurrentCanalPath[1], 2, GET_CANAL_SECTION_RECORDING_NAME(eCurrentCanalSection))
					IF DOES_ENTITY_EXIST(vehChasers[0])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[0])
							STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[0])
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(vehChasers[1])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[1])
							STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[1])
						ENDIF
					ENDIF
				ENDIF
				ePreviousCanalSection = eCurrentCanalSection
			ENDIF
			
			KEEP_CHASER_ON_PATH_IN_CANALS(0)
			
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_ABOVE_COORDS(GET_ENTITY_COORDS(vehPlayerSeashark), GET_STRING_FROM_FLOAT(GET_ENTITY_SPEED(vehPlayerSeashark)), 1.5)
			#ENDIF
			
			IF NOT bPlayerIdleInCanals
				IF NOT IS_ENTITY_DEAD(vehChasers[0])
					IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehChasers[0]))) < 10*10
					AND GET_ENTITY_SPEED(vehPlayerSeashark) < 7
						bPlayerIdleInCanals = TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehChasers[1])
					IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehChasers[1]))) < 10*10
				AND GET_ENTITY_SPEED(vehPlayerSeashark) < 7
					bPlayerIdleInCanals = TRUE
				ENDIF
				ENDIF
			ELSE
				IF GET_ENTITY_SPEED(vehPlayerSeashark) > 10
					bPlayerIdleInCanals = FALSE
				ENDIF
			ENDIF
			
			IF eCurrentCanalSection = FAM2_CS_BOTTOM
				IF NOT IS_ENTITY_DEAD(vehChasers[1])
				IF GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(vehChasers[1]), 1, "famtwoCanalsAlt", vAltCanalPath) 
					> GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1, "famtwoCanalsAlt", vAltCanalPath)
					bPlayerBehind[1] = TRUE
				ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehChasers[0])
				IF GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(vehChasers[0]), 1, "famtwoCanalsAlt", vAltCanalPath) 
					> GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1, "famtwoCanalsAlt", vAltCanalPath)
					bPlayerBehind[0] = TRUE
				ENDIF
				ENDIF
				KEEP_CHASER_ON_PATH(1, vAltCanalPath, 1, "famtwoCanalsAlt")
				
			ELSE
				
				IF NOT IS_PED_INJURED(pedChasers[1])
				
					VECTOR vWaitTarget
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1061.247314,-1027.520386,-1.839872>>, <<-1158.562134,-864.583618,17.985117>>, 250.000000)
						vWaitTarget = <<-1191.4796, -1002.4274, 0>>
					ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1061.247314,-1027.520386,-1.839872>>, <<-1006.939270,-1120.255981,6.144058>>, 250.000000)
						vWaitTarget = <<-1137.6525, -1097.1637, 0>>
					ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-900.011719,-1305.718018,15.900065>>, <<-1006.939270,-1120.255981,-8.855943>>, 250.000000)
						vWaitTarget = <<-1083.9927, -1189.7032, 0>>
					ENDIF
					
					IF NOT IS_VECTOR_ZERO(vWaitTarget)
						IF NOT IS_ENTITY_AT_COORD(vehChasers[1], vWaitTarget, <<2.5,2.5,2>>)
							VECTOR vMax
							VECTOR vMin
							vMax = <<-1114.952271,-1082.671021,-1.599754>>+<<81.250000,133.250000,17.750000>>
							vMin = <<-1114.952271,-1082.671021,-1.599754>> - <<81.250000,133.250000,17.750000>>
							IF REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vMin.x, vMin.y, vMax.x, vMax.y)
								IF GET_SCRIPT_TASK_STATUS(pedChasers[1], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK
									TASK_VEHICLE_DRIVE_TO_COORD(pedChasers[1], vehChasers[1], vWaitTarget, 10, DRIVINGSTYLE_ACCURATE, SEASHARK, DRIVINGMODE_PLOUGHTHROUGH, 1, 3)
								ENDIF
							ENDIF
							IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(vehChasers[1]), 2)
							AND NOT IS_SPHERE_VISIBLE(vWaitTarget, 2)
								SET_ENTITY_COORDS(vehChasers[1], vWaitTarget)
								SET_ENTITY_HEADING(vehChasers[1], 214)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE FAM2_CHASE_CANALS_CATCH_UP
			IF NOT IS_PED_INJURED(pedChasers[1])
			AND NOT IS_ENTITY_DEAD(pedChasers[1])
			AND IS_VEHICLE_DRIVEABLE(vehChasers[1])
			AND IS_PED_IN_VEHICLE(pedChasers[1], vehChasers[1])
			AND IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					IF GET_SCRIPT_TASK_STATUS(pedChasers[1], SCRIPT_TASK_VEHICLE_CHASE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedChasers[1], SCRIPT_TASK_VEHICLE_CHASE) <> DORMANT_TASK
						TASK_VEHICLE_CHASE(pedChasers[1], PLAYER_PED_ID())
					ENDIF
				ENDIF
				IF IS_ENTITY_AT_ENTITY(pedChasers[1], PLAYER_PED_ID(), <<10, 10, 5>>)
					bCanalCatchupDone = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE FAM2_CHASE_CANALS_SINGLE_CHASER
		
			eCurrentCanalSection = GET_CURRENT_CANAL_SECTION()
			
			IF eCurrentCanalSection != FAM2_CS_INVALID
				IF eCurrentCanalSection != ePreviousCanalSection
					BUILD_PATH_FOR_RECORDING(vCurrentCanalPath[0], 1, GET_CANAL_SECTION_RECORDING_NAME(eCurrentCanalSection))
					BUILD_PATH_FOR_RECORDING(vCurrentCanalPath[1], 2, GET_CANAL_SECTION_RECORDING_NAME(eCurrentCanalSection))
					IF DOES_ENTITY_EXIST(vehChasers[1])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[1])
							STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[1])
						ENDIF
					ENDIF
				ENDIF
				ePreviousCanalSection = eCurrentCanalSection
			ENDIF
			
			KEEP_CHASER_ON_PATH_IN_CANALS(1)
			
			IF NOT bPlayerIdleInCanals
				IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehChasers[0])) < 10*10 OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehChasers[1])) < 10*10)
				AND GET_ENTITY_SPEED(vehPlayerSeashark) < 7
					bPlayerIdleInCanals = TRUE
				ENDIF
			ELSE
				IF GET_ENTITY_SPEED(vehPlayerSeashark) > 10
					bPlayerIdleInCanals = FALSE
				ENDIF
			ENDIF
			
		BREAK
		
		CASE FAM2_CHASE_MARINA
		
			
			KEEP_CHASER_ON_PATH(0, vMarinaPath1, 301, "famtwo")
			KEEP_CHASER_ON_PATH(1, vMarinaPath2, 302, "famtwo")
			
			#IF IS_DEBUG_BUILD
				GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 301, "famtwo", vMarinaPath1, TRUE)
				DRAW_PATH(vMarinaPath1)
				DRAW_PATH(vMarinaPath2)
			#ENDIF
			
		BREAK
		
		CASE FAM2_CHASE_LOSE_PEDS
		
			IF NOT bEndForced
			
				KEEP_CHASER_ON_PATH(0, vFinalPath1, 401, "famtwo", TRUE)
				KEEP_CHASER_ON_PATH(1, vFinalPath2, 402, "famtwo", TRUE)
				
				IF NOT bPedsSetToKillPlayer
				
					IF fGlobalSpeedVal > 0.1
						fGlobalSpeedVal -= 0.01
					ELSE
						fGlobalSpeedVal = 0.1
					ENDIF
					
					#IF IS_DEBUG_BUILD
						GET_TIME_POSITION_IN_SHORT_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 401, "famtwo", vFinalPath1, TRUE)
						DRAW_PATH(vFinalPath1)
						DRAW_PATH(vFinalPath2)
					#ENDIF
					
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTheAbsoluteEndPos1, vTheAbsoluteEndPos2, fTheAbsoluteEndWidth, FALSE)
					
					INT i
					REPEAT COUNT_OF(pedChasers) i
						IF NOT IS_PED_INJURED(pedChasers[i])
							CLEAR_PED_TASKS(pedChasers[i])
							TASK_VEHICLE_MISSION_COORS_TARGET(pedChasers[i], vehChasers[i], <<-901.3011, -1359.9370, 0>>, MISSION_GOTO_RACING, 20, DRIVINGMODE_PLOUGHTHROUGH, 3, 3, TRUE)
						ENDIF
					ENDREPEAT
					
					bEndForced = TRUE
					
				ENDIF
				
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	MANAGE_CHASER_BLIPS(0)
	MANAGE_CHASER_BLIPS(1)
	
	#IF IS_DEBUG_BUILD
		DRAW_ALL_CHASE_BOUNDS()
	#ENDIF
	
	IF eCurrentChaseSection = FAM2_CHASE_LOSE_PEDS
	OR eCurrentChaseSection = FAM2_CHASE_SEEK_AND_DESTROY
	OR bEndForced
		IF NOT bCanLosePeds
			bCanLosePeds = TRUE
		ENDIF
	ELSE
		IF bCanLosePeds
			bCanLosePeds = FALSE
		ENDIF
	ENDIF

ENDPROC

PROC CHECK_FOR_LOSING_CHASERS()

	INT iTemp
	
	IF bCanLosePeds
	
		FLOAT fLoseTime
		FLOAT fLoseDist
		
		IF bEndForced
			fLoseTime = 3.0
			fLoseDist = 50
		ELSE
			IF eCurrentChaseSection = FAM2_CHASE_SEEK_AND_DESTROY
				fLoseTime = 5.0
				fLoseDist = 250
			ELSE
				fLoseTime = 5.0
				fLoseDist = 100
			ENDIF
		ENDIF
		
		REPEAT COUNT_OF(pedChasers) iTemp		
			IF CHECK_IF_CHASER_HAS_BEEN_LOST(iTemp,fLoseDist, fLoseTime)
				bChaserLost[iTemp] = TRUE
				DELETE_PED(pedChasers[iTemp])
				DELETE_VEHICLE(vehChasers[iTemp])
				IF DOES_BLIP_EXIST(blipChasers[iTemp])
					REMOVE_BLIP(blipChasers[iTemp])
				ENDIF
				PRINTLN("DELETING CHASERS 1")
			ENDIF
		ENDREPEAT
				
	ELSE
	
		IF bEndForced
			bEndForced = FALSE
		ENDIF
		
	ENDIF
	
	IF NOT bChaserLost[0]
	OR NOT bChaserLost[1]
		REPEAT COUNT_OF(pedChasers) iTemp
			IF NOT bChaserLost[iTemp]
				IF DOES_ENTITY_EXIST(pedChasers[iTemp]) AND (IS_PED_INJURED(pedChasers[iTemp]) OR IS_ENTITY_DEAD(pedChasers[iTemp]))
				OR NOT DOES_ENTITY_EXIST(pedChasers[iTemp])
					bChaserLost[iTemp] = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

BOOL bConvCanPlay = FALSE
BOOL bCanCrash = FALSE
BOOL bOffthisFrame = FALSE

PROC SPEEDOPHILE_CHASE2()

	//vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	//INT iCountCoords
	//REPEAT COUNT_OF(vehChasers) iCountCoords
	//	IF NOT IS_PED_INJURED(pedChasers[iCountCoords])
	//		vChaserCoords[iCountCoords] = GET_ENTITY_COORDS(pedChasers[iCountCoords])
	//	ENDIF
	//ENDREPEAT
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
	
		IF NOT bCanCrash
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
		AND VMAG2(GET_ENTITY_VELOCITY(vehPlayerSeashark)) > 18*18
			bCanCrash = TRUE
		ENDIF
		
		IF bCanCrash
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND VMAG2(GET_ENTITY_VELOCITY(vehPlayerSeashark)) < 15*15
			bCanCrash = FALSE
		ENDIF
		
		IF NOT bOffthisFrame
			IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
				bOffthisFrame = TRUE
			ENDIF
		ELSE
			bOffthisFrame = FALSE
		ENDIF
		
		IF bOffthisFrame
		AND bCanCrash
		AND IS_PED_RAGDOLL(PLAYER_PED_ID())
			SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bCanCrash
				DRAW_DEBUG_TEXT_2D("Mike can crash", <<0.9,0.5,0>>)
			ENDIF
			DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(VMAG(GET_ENTITY_VELOCITY(vehPlayerSeashark))), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0,0,1.5>> )
		#ENDIF
		
	ENDIF
	
	eCurrentChaseSection = GET_CURRENT_CHASE_ENUM()
	
	IF eCurrentChaseSection < FAM2_CHASE_SEEK_AND_DESTROY
	AND eCurrentChaseSection > eHighestSection
		eHighestSection = eCurrentChaseSection
	ENDIF
	
	IF eCurrentChaseSection != ePreviousChaseSection
		IF NOT IS_BIT_SET(iSectionsHit, ENUM_TO_INT(eCurrentChaseSection))
			SET_BIT(iSectionsHit, ENUM_TO_INT(eCurrentChaseSection))
		ENDIF
		IF DOES_ENTITY_EXIST(vehChasers[0])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[0])
			 	STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[0])
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(vehChasers[1])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[1])
			 	STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[1])
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
//			IF NOT IS_PED_INJURED(pedChasers[0])
//				EXPLODE_PED_HEAD(pedChasers[0])
//			ENDIF
//			IF NOT IS_PED_INJURED(pedChasers[1])
//				EXPLODE_PED_HEAD(pedChasers[1])
//			ENDIF
			EXPLODE_PED_HEAD(pedTracy)
		ENDIF
	#ENDIF
	
	IF IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehPlayerSeashark)
	AND TIMERA() > 500
		STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerSeashark)
	ENDIF
	
	ePreviousChaseSection = eCurrentChaseSection
	
	IF NOT DOES_ENTITY_EXIST(objFitTest)
		REQUEST_MODEL(PROP_LD_TEST_01)
		IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
			objFitTest = CREATE_OBJECT(PROP_LD_TEST_01, v_jimmy_end_pos)
			SET_ENTITY_COLLISION(objFitTest, FALSE)
			SET_ENTITY_VISIBLE(objFitTest, FALSE)
			FREEZE_ENTITY_POSITION(objFitTest, TRUE)
		ENDIF
	ENDIF
	
	SWITCH iStageProgress
	
		// set up all lines and create chasers.
		CASE 0
		
			SET_MAX_WANTED_LEVEL(0)
			
			IF INIT_RECORDING_PATHS()
			AND CREATE_CHASER(0)
			AND CREATE_CHASER(1)
			AND HAVE_ALL_CANAL_PATHS_LOADED()
			AND CREATE_WATER_PED()
			
				IF NOT IS_PED_INJURED(pedBoatDancer[0])
					TASK_SMART_FLEE_PED(pedBoatDancer[0], PLAYER_PED_ID(), 300, -1)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedBoatDancer[0], rgChasers)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedTracy)
					REQUEST_ANIM_DICT("missfam2_tracy_jetski")
					SET_VEHICLE_ENGINE_HEALTH(vehPlayerSeashark, 1000)
					SET_ENTITY_PROOFS(vehPlayerSeashark, TRUE, FALSE,FALSE,TRUE,FALSE)
					SET_PED_SUFFERS_CRITICAL_HITS(pedTracy, FALSE)
					SET_ENTITY_HEALTH(pedTracy, 5000)
					SET_PED_COMBAT_ATTRIBUTES(pedTracy, CA_CAN_TAUNT_IN_VEHICLE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(pedTracy, CA_DO_DRIVEBYS, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTracy, TRUE)
					SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(pedTracy, TRUE)
				ENDIF
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgChasers, RELGROUPHASH_PLAYER)
				
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, pedTracy, "TRACEY")
				ALLOW_DIALOGUE_IN_WATER(TRUE)
				
				INIT_CHASE_BOUNDS()
				INIT_CANAL_PATHS()
				
				iChaseStage = 0
				fGlobalSpeedVal = 0
				fKillTime = 0
				
				bEndForced 			= FALSE
				bCanLosePeds 		= FALSE
				bChaserLost[0] 		= FALSE
				bChaserLost[1] 		= FALSE
				
				bKilledOne			= FALSE
				bKillLineDone   	= FALSE
				bShooting			= FALSE
				bShootLineDone  	= FALSE
				bKilledBoth			= FALSE
				bCanalCatchupDone 	= FALSE
				bConvCanPlay 		= FALSE
				
				eCurrentCanalSection = FAM2_CS_BOTTOM
				ePreviousCanalSection = FAM2_CS_INVALID
				eHighestSection = FAM2_CHASE_OPEN_SEA
				
				TRIGGER_MUSIC_EVENT("FAM2_ON_JETSKI")
				
				SET_ENTITY_REQUIRES_MORE_EXPENSIVE_RIVER_CHECK(vehChasers[0], TRUE)
				SET_ENTITY_REQUIRES_MORE_EXPENSIVE_RIVER_CHECK(vehChasers[1], TRUE)
				
				START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehChasers[0], 101, "famtwo", 15, DRIVINGMODE_PLOUGHTHROUGH)
				START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehChasers[1], 102, "famtwo", 15, DRIVINGMODE_PLOUGHTHROUGH)
				START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehPlayerSeashark, 1, "famtwoplayer", 24, DRIVINGMODE_PLOUGHTHROUGH)
				
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehChasers[0], "FAMILY_2_ENEMY_JETSKI_Group")
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehChasers[1], "FAMILY_2_ENEMY_JETSKI_Group")
				
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_HARD)
				
				IF DOES_ENTITY_EXIST(vehPlayerSeashark)
				AND IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					SET_INITIAL_PLAYER_STATION("OFF")
					SET_VEH_RADIO_STATION(vehPlayerSeashark, "OFF")
					SET_ENTITY_REQUIRES_MORE_EXPENSIVE_RIVER_CHECK(vehPlayerSeashark, TRUE)
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_2_JETSKI_CHASE")
					START_AUDIO_SCENE("FAMILY_2_JETSKI_CHASE")
				ENDIF
				
				SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, TRUE, TRUE)
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				
				SETTIMERB(0)
				
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
				
				SET_WATER_CALM(FALSE, 4.0)
				
				iSectionsHit = 0
				
				iStageProgress++
				
				IF bReplaySetupRequired
					iStageProgress = -1
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE -1
			REQUEST_IPL("smboat")
			WHILE NOT IS_IPL_ACTIVE("smBoat")
				WAIT(0)
			ENDWHILE
			iStageProgress = 1
			bReplaySetupRequired = FALSE
		BREAK
		
		CASE 1
		
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		
			IF DOES_CAM_EXIST(camRescueOutro)
				IF TIMERB() > 1000
					DESTROY_CAM(camRescueOutro)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(sSpeech.PedInfo[7].Index)
			OR IS_PED_INJURED(sSpeech.PedInfo[7].Index)
				IF NOT IS_PED_INJURED(pedChasers[0])
					ADD_PED_FOR_DIALOGUE(sSpeech, 7, pedChasers[0], "FAM2MAN2")
				ELIF NOT IS_PED_INJURED(pedChasers[1])
					ADD_PED_FOR_DIALOGUE(sSpeech, 7, pedChasers[1], "FAM2MAN2")
				ENDIF
			ENDIF
			
			IF NOT bPlayerShouldDie
			
				IF bPedsSetToKillPlayer
					INT iKill
					FOR iKill = 0 TO 1
						IF NOT IS_PED_INJURED(pedChasers[iKill])
						SET_PED_COMBAT_ABILITY(pedChasers[iKill], CAL_POOR)
						SET_PED_COMBAT_RANGE(pedChasers[iKill], CR_FAR)
						SET_PED_ACCURACY(pedChasers[iKill], 0)
						SET_PED_FIRING_PATTERN(pedChasers[iKill], FIRING_PATTERN_DELAY_FIRE_BY_ONE_SEC)
						SET_PED_SHOOT_RATE(pedChasers[iKill], 50)
						ENDIF
					ENDFOR
					bPedsSetToKillPlayer = FALSE
				ENDIF
				
				IF (eCurrentChaseSection = FAM2_CHASE_SEEK_AND_DESTROY AND iChaseStage < 100)
				OR bPlayerBehind[0]
				OR bPlayerBehind[1]
				OR eCurrentChaseSection < eHighestSection
				OR bPlayerIdleInCanals
				OR IS_VEHICLE_DRIVEABLE(vehPlayerSeashark) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
				
					IF NOT IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
					
						#IF IS_DEBUG_BUILD
						
							IF (eCurrentChaseSection = FAM2_CHASE_SEEK_AND_DESTROY AND iChaseStage < 100)
								PRINTLN("KILLING PLAYER FOR GOING OUT OF BOUNDS")
							ENDIF
							
							IF bPlayerBehind[0]
							OR bPlayerBehind[1]
								PRINTLN("KILLING PLAYER FOR BEING TOO SLOW")
							ENDIF
							
							IF bPlayerIdleInCanals
								PRINTLN("KILLING PLAYER FOR BEING TOO LAZY IN CANALS")
							ENDIF
							
							IF eCurrentChaseSection < eHighestSection
								PRINTLN("KILLING PLAYER FOR GOING BACKWARDS")
							ENDIF
							
							IF (IS_VEHICLE_DRIVEABLE(vehPlayerSeashark) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark))
								PRINTLN("KILLING PLAYER FOR BEING OUT OF HIS VEHICLE")
							ENDIF
							
						#ENDIF
						
						iChaserLastWarp[0] = GET_GAME_TIMER()
						iChaserLastWarp[1] = GET_GAME_TIMER() - 1500
						bPlayerShouldDie = TRUE
					
					ENDIF
					
				ENDIF
				
			ELSE
			
				#IF IS_DEBUG_BUILD
					PRINTLN("KILLING PLAYER!")
				#ENDIF
				
				IF fKillTime > 10.0
				AND fKillTime < 12.0
//					IF IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
//						SET_PED_TO_RAGDOLL(pedTracy, 5000, 10000, TASK_RELAX)
//						fKillTime = 13.0
//					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
						IF (NOT IS_PED_INJURED(pedChasers[0]) AND VDIST2(GET_ENTITY_COORDS(pedChasers[0]), GET_ENTITY_COORDS(vehPlayerSeashark)) < 15*15)
						OR (NOT IS_PED_INJURED(pedChasers[1]) AND VDIST2(GET_ENTITY_COORDS(pedChasers[1]), GET_ENTITY_COORDS(vehPlayerSeashark)) < 15*15)
							fKillTime += GET_FRAME_TIME()
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bPedsSetToKillPlayer
				
					INT iKill
					FOR iKill = 0 TO 1
						IF DOES_ENTITY_EXIST(pedChasers[iKill])
						AND NOT IS_PED_INJURED(pedChasers[iKill])
							SET_PED_COMBAT_ABILITY(pedChasers[iKill], CAL_AVERAGE)
							SET_PED_COMBAT_RANGE(pedChasers[iKill], CR_FAR)
							SET_PED_ACCURACY(pedChasers[iKill], 20)
							SET_PED_FIRING_PATTERN(pedChasers[iKill], FIRING_PATTERN_SINGLE_SHOT)
							SET_PED_SHOOT_RATE(pedChasers[iKill], 40)
							IF DOES_ENTITY_EXIST(vehChasers[iKill])
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[iKill])
									STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[iKill])
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					bPedsSetToKillPlayer = TRUE
					
				ELSE
				
					IF (eCurrentChaseSection != FAM2_CHASE_CANALS OR (eCurrentChaseSection = FAM2_CHASE_CANALS AND eCurrentCanalSection = FAM2_CS_BOTTOM))
					AND NOT bEndForced
						MANAGE_CHASER_WARPING()
					ENDIF
					
				ENDIF
				
				IF NOT bPlayerBehind[0]
				AND NOT bPlayerBehind[1]
				AND eCurrentChaseSection <> FAM2_CHASE_SEEK_AND_DESTROY
				AND eCurrentChaseSection >= eHighestSection
				AND NOT bPlayerIdleInCanals
				AND (IS_VEHICLE_DRIVEABLE(vehPlayerSeashark) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark))
					bPlayerShouldDie = FALSE
				ENDIF
				
				IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
					bPlayerShouldDie = FALSE
				ENDIF
				
			ENDIF
			
			IF NOT IS_PED_INJURED(pedBoatDancer[0])
				IF VDIST2(GET_ENTITY_COORDS(pedBoatDancer[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100*100
					SET_PED_AS_NO_LONGER_NEEDED(pedBoatDancer[0])
				ENDIF
			ENDIF
			
			RUN_SEASHARK_HELP()
			RUN_CHASERS()
			
			IF iChaseStage < 100
			
				IF NOT bShooting
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
					AND IS_PED_SHOOTING(PLAYER_PED_ID())
						bShooting = TRUE					
					ENDIF
				ELSE
					IF NOT bShootLineDone
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_SHOOT", CONV_PRIORITY_HIGH)
							bShootLineDone = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bKilledOne
				OR NOT bKilledBoth
					INT iKillCheck
					bKilledBoth = TRUE
					REPEAT COUNT_OF(pedChasers) iKillCheck
						IF DOES_ENTITY_EXIST(pedChasers[iKillCheck])
							IF (IS_PED_INJURED(pedChasers[iKillCheck]) OR IS_ENTITY_DEAD(pedChasers[iKillCheck]))
								bKilledOne = TRUE
							ELSE
								bKilledBoth = FALSE
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF bKilledOne
				AND NOT bKillLineDone
				AND NOT bKilledBoth
					IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						IF iChaseStage < 50
						OR iChaseStage >= 100
							iSavedStage = iChaseStage
							PRINTLN("IN kill check setting isavedstage to ", iSavedStage)
						ENDIF
						IF DOES_BLIP_EXIST(blipPlayerVeh)
							REMOVE_BLIP(blipPlayerVeh)
						ENDIF
						iChaseStage = 50
						bKillLineDone = TRUE
					ENDIF
				ENDIF
				
				IF eCurrentChaseSection = FAM2_CHASE_TUNNELS
					IF DOES_BLIP_EXIST(blipDest)
						REMOVE_BLIP(blipDest)
						IF IS_MESSAGE_BEING_DISPLAYED()
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CLEAR_PRINTS()
						ENDIF
					ENDIF
				ENDIF
				
				IF iChaseStage < 5
					IF eCurrentChaseSection = FAM2_CHASE_TUNNELS
						iChaseStage = 5
					ENDIF
				ENDIF
				
				CHECK_FOR_LOSING_CHASERS()
				
			ENDIF
			
			IF iChaseStage <> 60
			AND iChaseStage <> 61
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
				AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					IF iChaseStage < 50
					OR iChaseStage >= 100
						iSavedStage = iChaseStage
						PRINTLN("IN vehicle check setting isavedstage to ", iSavedStage)
					ENDIF
					iChaseStage = 60
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(blipDest)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
				IF eCurrentChaseSection = FAM2_CHASE_TUNNELS
					REMOVE_BLIP(blipDest)
				ENDIF
			ENDIF
			
			SWITCH iChaseStage
			
				CASE 0
					IF HAS_ANIM_DICT_LOADED("missfam2_tracy_jetski")
					
						IF NOT IS_PED_INJURED(pedTracy)
						IF NOT IS_ENTITY_PLAYING_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_a")
							IF IS_CHASER_BEHIND_TRACEY(0)
								TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							ELSE
								IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_COMING", CONV_PRIORITY_HIGH)										
									TRIGGER_MUSIC_EVENT("FAM2_COMING")
									TASK_LOOK_AT_ENTITY(pedTracy, pedChasers[0], -1)
									iChaseStage++
									SETTIMERA(0)
								ENDIF
							ENDIF
						ELSE
							IF GET_ENTITY_ANIM_CURRENT_TIME(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_a") > 0.12
								IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_COMING", CONV_PRIORITY_HIGH)
									TRIGGER_MUSIC_EVENT("FAM2_COMING")
									iChaseStage++
									SETTIMERA(0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					ENDIF
				BREAK
				
				CASE 1
				
					IF TIMERA() > 1000
					
						VECTOR vToTunnel
						vToTunnel = vOutlet - GET_ENTITY_COORDS(vehPlayerSeashark)
						VECTOR vForward
						vForward = GET_ENTITY_FORWARD_VECTOR(vehPlayerSeashark)
						FLOAT fAngle
						fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vForward.x, vForward.y, vToTunnel.x, vToTunnel.y)
						
						IF fAngle > 180
							fAngle -= 360
						ENDIF
						IF fAngle < -180
							fAngle += 360
						ENDIF
						
						IF fAngle < 0
							IF fAngle > -90
								IF NOT IS_PED_INJURED(pedTracy)
								TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_point_right", SLOW_BLEND_IN)
							ENDIF
							ENDIF
						ELSE
							IF fAngle < 90
								IF NOT IS_PED_INJURED(pedTracy)
								TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_point_left", SLOW_BLEND_IN)
							ENDIF
						ENDIF
						ENDIF
						
					ENDIF
					
					IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR TIMERA() > 3000)
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					AND IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
					AND NOT DOES_BLIP_EXIST(blipDest)
						blipDest = CREATE_BLIP_FOR_COORD(vOutlet)
						iChaseStage++
					ENDIF
					
				BREAK
				
				CASE 2
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_MRESP", CONV_PRIORITY_HIGH)
						TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vOutlet, 7500, SLF_SLOW_TURN_RATE)
						SETTIMERA(0)
						iChaseStage++
					ENDIF
				BREAK
				
				CASE 3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT("FM2_CHASE1", DEFAULT_GOD_TEXT_TIME, 1)
						SETTIMERA(0)
						iChaseStage++
					ENDIF
				BREAK
				
				CASE 4
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vOutlet) > 30*30
						DO_TRACY_PANIC_LINES("FAM2_OLREM", "", 9000)
					ENDIF
				BREAK
				
				CASE 5
					IF (DOES_ENTITY_EXIST(pedChasers[0]) AND NOT IS_PED_INJURED(pedChasers[0]) AND IS_PED_SHOOTING(pedChasers[0]))
					OR (DOES_ENTITY_EXIST(pedChasers[1]) AND NOT IS_PED_INJURED(pedChasers[1]) AND IS_PED_SHOOTING(pedChasers[1]))
						iChaseStage++
					ENDIF
				BREAK
				
				CASE 6
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_PED_INJURED(pedTracy)
						IF NOT IS_ENTITY_PLAYING_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_c")
							IF IS_CHASER_BEHIND_TRACEY(0)
								TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							ELSE
								IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_HESHOOT", CONV_PRIORITY_HIGH)
									TRIGGER_MUSIC_EVENT("FAM2_SHOOTING")
									TASK_LOOK_AT_ENTITY(pedTracy, pedChasers[0], -1)
									iChaseStage++
									SETTIMERA(0)
								ENDIF
							ENDIF
						ELSE
							IF GET_ENTITY_ANIM_CURRENT_TIME(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_c") > 0.12
								IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_HESHOOT", CONV_PRIORITY_HIGH)
									TRIGGER_MUSIC_EVENT("FAM2_SHOOTING")
									iChaseStage++
									SETTIMERA(0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					ENDIF
				BREAK
				
				CASE 7
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT("FM2_CHASE3", DEFAULT_GOD_TEXT_TIME, 1)
						SETTIMERB(0)
						iChaseStage++
					ENDIF
				BREAK
				
				CASE 8
					IF TIMERB() > 5000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_LOSEM", CONV_PRIORITY_HIGH)
								iChaseStage ++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 9
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_MULTIPART_CONVERSATION_WITH_4_LINES(sSpeech, sConvBlock, 
							"FAM2_WHO", "FAM2_WHO_3",
							"FAM2_WHO", "FAM2_WHO_5",
							"FAM2_WHO", "FAM2_WHO_7",
							"FAM2_WHO", "FAM2_WHO_9",
							CONV_PRIORITY_HIGH)
							iChaseStage++
						ENDIF
					ENDIF
				BREAK
				
				CASE 10
					DO_TRACY_PANIC_LINES("FAM2_TSHOUT", "FAM2_MSHOUT")
					IF eCurrentChaseSection = FAM2_CHASE_CANALS
						iChaseStage++
					ENDIF
				BREAK
				
				CASE 11
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_LOSET", CONV_PRIORITY_HIGH)
						iChaseStage++
					ENDIF
				BREAK
				
				CASE 12
					DO_TRACY_PANIC_LINES("FAM2_TSHOUT", "FAM2_MSHOUT")
				BREAK
				
				CASE 50
					KILL_FACE_TO_FACE_CONVERSATION()
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_KILLONE", CONV_PRIORITY_HIGH)
						iChaseStage++
					ENDIF
				BREAK
				
				CASE 51
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_KILLER", CONV_PRIORITY_HIGH)
						iChaseStage = iSavedStage
					ENDIF
				BREAK
				
				CASE 60
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
						IF NOT DOES_BLIP_EXIST(blipPlayerVeh)
							blipPlayerVeh = CREATE_BLIP_FOR_VEHICLE(vehPlayerSeashark)
							PRINT_GOD_TEXT(gtGetBackInSeashark)
						ENDIF
						IF DOES_BLIP_EXIST(blipDest)
							REMOVE_BLIP(blipDest)
						ENDIF
						iTraceyPanicTimer = GET_GAME_TIMER()
						iChaseStage++
					ENDIF
				BREAK
				
				CASE 61
					IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							IF NOT IS_PED_INJURED(pedTracy)
							AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedTracy, <<20, 20, 20>>)
							AND IS_ENTITY_IN_WATER(PLAYER_PED_ID())
							AND NOT IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
							AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark, TRUE)
								IF GET_GAME_TIMER() - iTraceyPanicTimer > 6000
									IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FAM2_FALL", CONV_PRIORITY_HIGH)
										iTraceyPanicTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF (eCurrentChaseSection = FAM2_CHASE_OPEN_SEA OR eCurrentChaseSection = FAM2_CHASE_SEEK_AND_DESTROY)
							AND NOT DOES_BLIP_EXIST(blipDest)
							AND iSavedStage < 5
								blipDest = CREATE_BLIP_FOR_COORD(vOutlet)
							ENDIF
							IF DOES_BLIP_EXIST(blipPlayerVeh)
								REMOVE_BLIP(blipPlayerVeh)
							ENDIF
							CLEAR_PRINTS()
							iChaseStage = iSavedStage
						ENDIF
					ENDIF
				BREAK
				
				CASE 100
				
					IF DOES_BLIP_EXIST(blipChasers[0])
						REMOVE_BLIP(blipChasers[0])
					ENDIF
					IF DOES_BLIP_EXIST(blipChasers[1])
						REMOVE_BLIP(blipChasers[1])
					ENDIF
					IF DOES_BLIP_EXIST(blipDest)
						REMOVE_BLIP(blipDest)
					ENDIF
					
					KILL_FACE_TO_FACE_CONVERSATION()
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
						AND IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
							IF NOT IS_PED_INJURED(pedTracy)
							IF NOT IS_ENTITY_PLAYING_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_d")
								TASK_PLAY_ANIM(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							ELSE
								IF GET_ENTITY_ANIM_CURRENT_TIME(pedTracy, "missfam2_tracy_jetski", "tracy_jetski_d") > 0.25
									IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_JETSKI_CHASE")
										STOP_AUDIO_SCENE("FAMILY_2_JETSKI_CHASE")
									ENDIF
									TASK_CLEAR_LOOK_AT(pedTracy)
									iChaseStage++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					ENDIF
					
				BREAK
				
				CASE 101
				
					STRING sTraceyEndConv
					
					IF bKilledBoth
						sTraceyEndConv = "FAM2_KILLTWO"
					ELIF bKilledOne
						sTraceyEndCOnv = "FAM2_ESCAPE"
					ELSE
						sTraceyEndCOnv = "FAM2_LOST"
					ENDIF
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					AND IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
						bConvCanPlay = TRUE
					ENDIF
					
					MANAGE_CONVERSATION(sTraceyEndConv, bConvCanPlay)
					
					IF in_car_conv_started
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT in_car_conv_on_hold
						
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0)
						
						in_car_conv_started = FALSE
						iChaseStage++
					ENDIF
					
				BREAK
				
				CASE 102
				
					STRING sMichaelEndConv
					
					IF bKilledBoth
					OR bKilledOne
						sMichaelEndConv = "FAM2_KILLER2"
					ELSE
						sMichaelEndConv = "FAM2_TALK1"
					ENDIF
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					AND IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
						bConvCanPlay = TRUE
					ENDIF
					
					MANAGE_CONVERSATION(sMichaelEndConv, bConvCanPlay)
					
					IF in_car_conv_started
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT in_car_conv_on_hold
						REMOVE_ANIM_DICT("missfam2_tracy_jetski")
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
						in_car_conv_started = FALSE
						GO_TO_STAGE(stage_drive_to_beach)
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
			AND NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
			AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
			AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedTracy)) > 30
				iStageProgress = 2
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
				iStageProgress = 3
			ENDIF
			
			IF NOT IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
			OR IS_SEASHARK_GROUNDED(vehPlayerSeashark, fPlayerGroundTime, bPlayerGroundingChecked)
				iStageProgress = 4
			ENDIF
			
			IF iChaseStage < 100
			AND (IS_PED_INJURED(pedChasers[0]) OR IS_ENTITY_DEAD(pedChasers[0]) OR bChaserLost[0])
			AND (IS_PED_INJURED(pedChasers[1]) OR IS_ENTITY_DEAD(pedChasers[1]) OR bChaserLost[1])
			AND iChaseStage != 60
			AND iChaseStage != 61
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
				TRIGGER_MUSIC_EVENT("FAM2_LOST_HIM")
				iChaseStage = 100
			ENDIF
			
			IF iStageProgress > 1
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION()
				INT iTemp
				REPEAT COUNT_OF(pedChasers) iTemp
					IF NOT IS_PED_INJURED(pedChasers[iTemp])
					AND DOES_ENTITY_EXIST(vehChasers[iTemp])
					AND IS_VEHICLE_DRIVEABLE(vehChasers[iTemp])
					AND IS_PED_IN_VEHICLE(pedChasers[iTemp], vehChasers[iTemp])
						CLEAR_PED_TASKS(pedChasers[iTemp])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChasers[iTemp])
							STOP_PLAYBACK_RECORDED_VEHICLE(vehChasers[iTemp])
						ENDIF
					ENDIF
				ENDREPEAT
				IF DOES_BLIP_EXIST(blipDest)
					REMOVE_BLIP(blipDest)
				ENDIF
				SETTIMERA(0)
			ENDIF
			
		BREAK
		
		CASE 2
			IF TIMERA() > 2000
				Mission_Failed("FM2_TRAAB")
			ENDIF
		BREAK
		
		CASE 3
			IF TIMERA() > 2000
				Mission_Failed("FM2_TRAFELL")
			ENDIF
		BREAK
		
		CASE 4
			IF TIMERA() > 1000
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_HARD)
				IF IS_ENTITY_DEAD(vehPlayerSeashark)
					Mission_Failed("FM2_SKIDED")
				ELSE
					Mission_Failed("FM2_SKIGRN")
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
		
ENDPROC

//╞═════════╡CUTSCENES

BOOL bLoadScene
RAYFIRE_INDEX iTVObject

PROC RUN_INTRO_CUTSCENE()
	
	SWITCH iStageProgress
	
		CASE 0
			SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", FALSE)
			bLoadScene = FALSE
			IF NOT IS_CUTSCENE_ACTIVE()
				REQUEST_CUTSCENE("family_2_int")
				PRINTLN("SET CUTSCENE PED ASSETS IN MISSION SCRIPT")
				iStageProgress = -1
			ELSE
				iStageProgress++
			ENDIF
		BREAK
		
		CASE -1
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_OUTFIT("Michael", GET_PLAYER_PED_MODEL(CHAR_MICHAEL), OUTFIT_P0_YOGA_FLIP_FLOPS)
				iStageProgress = 1
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("family_2_int")
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					IF NOT DOES_ENTITY_EXIST(pedJimmy)
						PRINTLN("CREATING CUTSCENE JIMMY")
						REGISTER_ENTITY_FOR_CUTSCENE(pedJimmy, "Jimmy", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_JIMMY))
					ELIF NOT IS_PED_INJURED(pedJimmy)
						PRINTLN("ANIMATING SCRIPT JIMMY")
						REGISTER_ENTITY_FOR_CUTSCENE(pedJimmy, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					PRE_INIT_ASSETS(stage_drive_to_venice)
					INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
					CLEAR_HELP()
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					END_SRL()
					iStageProgress++
				ELSE
					REMOVE_CUTSCENE()
					iStageProgress = 0
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			SET_WIND(0.1)
			
			IF DOES_ENTITY_EXIST(objDoorL)
				DELETE_OBJECT(objDoorL)
				REMOVE_MODEL_HIDE(vLeftDoorPos, 1.0, V_ILEV_MM_DOORM_L)
            ENDIF									 
            
            IF DOES_ENTITY_EXIST(objDoorR)
				DELETE_OBJECT(objDoorR)
				REMOVE_MODEL_HIDE(vRightDoorPos, 1.0, V_ILEV_MM_DOORM_R)
            ENDIF
            
            IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
            ENDIF
			
            IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
            ENDIF
            
            REMOVE_ANIM_DICT("missfam2mcs_intp1")
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(objDoorL)
				STOP_SYNCHRONIZED_ENTITY_ANIM(objDoorL, INSTANT_BLEND_OUT, TRUE)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorL)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objDoorR)
				STOP_SYNCHRONIZED_ENTITY_ANIM(objDoorR, INSTANT_BLEND_OUT, TRUE)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorR)
			ENDIF
			
			REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 300)
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, DEFAULT, DEFAULT, TRUE, FALSE)
//			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
			CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
			iStageProgress++
			
		BREAK
		
		CASE 3
		
			IF NOT DOES_ENTITY_EXIST(pedJimmy)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy"))
					pedJimmy = GET_PED_INDEX_FROM_ENTITY_INDEX((GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy")))
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
				IF NOT IS_PED_INJURED(pedJimmy)
					SET_ENTITY_COORDS(pedJimmy, vJimmyCoords)
					SET_ENTITY_HEADING(pedJimmy, fJimmyHeading)
					FORCE_PED_MOTION_STATE(pedJimmy, MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					TASK_FOLLOW_WAYPOINT_RECORDING(pedJimmy, sJimmyStairRoute, 0, EWAYPOINT_START_FROM_CLOSEST_POINT, -1)
					PRINTLN("SETTING JIMMY")
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			AND CAN_SET_EXIT_STATE_FOR_CAMERA()
				println("setting motion state here 1")
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK)
				IF NOT IS_PED_INJURED(pedJimmy)
					SET_PED_AS_BUDDY(pedJimmy)
				ENDIF
			ENDIF
			
			IF NOT bLoadScene
				IF IS_SCREEN_FADED_OUT()
				AND WAS_CUTSCENE_SKIPPED()
					PRINTLN("loading scene for skip")
					SETTIMERA(0)
					IF NEW_LOAD_SCENE_START_SPHERE(vPlayerStartCoords, 30, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
						PRINTLN("Starting load sphere")
						WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND TIMERA() < 5000
							PRINTLN("loading sphere...")
							DO_SCREEN_FADE_OUT(0)
							WAIT(0)
						ENDWHILE
						NEW_LOAD_SCENE_STOP()
						PRINTLN("Finished load sphere")
						bLoadScene = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			IF NOT DOES_ENTITY_EXIST(vehMikesCar)
				IF IS_PLAYER_VEHICLE_IN_AREA(CHAR_MICHAEL, VEHICLE_TYPE_CAR, v_car_start_coords, 100.0)
					DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
				ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE)
				CREATE_PLAYER_VEHICLE(vehMikesCar, CHAR_MICHAEL, v_car_start_coords, f_car_start, TRUE, VEHICLE_TYPE_CAR)
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
				PRINTLN("Cutscene over")
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
				GO_TO_STAGE(stage_drive_to_venice)
				REQUEST_ADDITIONAL_TEXT("FAMILY2", MISSION_TEXT_SLOT)
				REQUEST_ADDITIONAL_TEXT(sConvBlock, MISSION_DIALOGUE_TEXT_SLOT)
				IF NOT IS_PED_INJURED(pedJimmy)
					SET_PED_AS_BUDDY(pedJimmy)
				ENDIF
				
				REPLAY_STOP_EVENT()
				
				println("setting motion state here 2")
				IF IS_VALID_INTERIOR(intMikeMansion)
					UNPIN_INTERIOR(intMikeMansion)
				ENDIF
				
				iTVObject = GET_RAYFIRE_MAP_OBJECT(<<-809.9620, 170.9190, 75.7407>>, 3, "des_tvsmash")
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(iTVObject)
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(iTVObject, RFMO_STATE_ENDING)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
				OR WAS_CUTSCENE_SKIPPED()
					IF NOT IS_PED_INJURED(pedJimmy)
						SET_ENTITY_COORDS(pedJimmy, vJimmyCoords)
						SET_ENTITY_HEADING(pedJimmy, fJimmyHeading)
						FORCE_PED_MOTION_STATE(pedJimmy, MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
						TASK_FOLLOW_WAYPOINT_RECORDING(pedJimmy, sJimmyStairRoute, 3, EWAYPOINT_DEFAULT, -1)
						PRINTLN("Setting Jimmy coords and forcing motion state")
					ENDIF
					WAIT(0)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK)
					PRINTLN("Setting Player coords and forcing motion state")
				ENDIF
				
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
				SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", TRUE)
				SET_WIDESCREEN_BORDERS(FALSE, 0)
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			PRINTLN("frame")
			
		BREAK
		
	ENDSWITCH
	
ENDPROC


PROC RUN_PIER_CUTSCENE()

	SWITCH iStageProgress
	
		CASE 0
			IF NOT IS_CUTSCENE_ACTIVE()
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_2_mcs_2", CS_SECTION_2 | CS_SECTION_3)
				iStageProgress++
			ELSE
				iStageProgress+=2
			ENDIF
		BREAK
		
		CASE 1
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				PRINTLN("SETTING components from player")
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 2
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("family_2_mcs_2")
				
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					IF NOT IS_PED_INJURED(pedJimmy)
						SET_PED_SWEAT(pedJimmy, 100)
						REGISTER_ENTITY_FOR_CUTSCENE(pedJimmy, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayerBike)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerBike)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehJimmyBike)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJimmyBike)
							FREEZE_ENTITY_POSITION(vehJimmyBike, FALSE)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehJimmyBike)
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(vehJimmyBike, "FAM_2_Jimmys_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					START_CUTSCENE()
										
					PRINTLN("Cutscene starting")
					CLEAR_PRINTS()
					CLEAR_HELP()
					INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
					SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 26, 0)
					SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 27, 0)
					
					IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_BIKE_RACE_JIMMY")
						STOP_AUDIO_SCENE("FAMILY_2_BIKE_RACE_JIMMY")
					ENDIF
					
					SET_PED_WALLA_DENSITY(0.0, 0.0)
					iStageProgress++
					
				ELSE
					PRINTLN("WRONG CUT LOADED")
					REMOVE_CUTSCENE()
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_2_mcs_2", CS_SECTION_1 | CS_SECTION_3)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
			IF IS_VEHICLE_DRIVEABLE(vehPlayerBike)
				IF bJimmyWon
					SET_ENTITY_COORDS(vehPlayerBike, <<-1848.194702,-1218.705688,12.583574>>)
					SET_ENTITY_HEADING(vehPlayerBike, 129.352661)
				ELSE
					SET_ENTITY_COORDS(vehPlayerBike, <<-1867.382690,-1212.197876,12.574015>>)
					SET_ENTITY_HEADING(vehPlayerBike, -0.030485)
				ENDIF
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerBike)
				ACTIVATE_PHYSICS(vehPlayerBike)
			ENDIF
			CLEAR_AREA(<<-1859.0448, -1215.5027, 12.0665>>, 2, TRUE)
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
			
			iStageProgress++
		BREAK
		
		CASE 4
		
			IF IS_CUTSCENE_ACTIVE()
			AND IS_CUTSCENE_PLAYING()
				OVERRIDE_LODSCALE_THIS_FRAME(1.5)
				IF GET_CUTSCENE_TIME() > 90500
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				REPLAY_STOP_EVENT()
			
				SETUP_CLOTHES(TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				PRINTLN("\n\n\nMIKE'S EXIT FRAME\n\n\n")
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_SPRINT, FALSE, FAUS_CUTSCENE_EXIT)
				PRINTLN("FAMILY 2 CUT EXIT - RUN A")
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_SPRINT, iRunTime)
				PRINT_HELP("FM2_DIVOFF")
			ENDIF
			
//			IF bMikeOut
//			AND (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
//				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_SPRINT)
//			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				IF IS_VEHICLE_DRIVEABLE(vehJimmyBike)
					SET_ENTITY_COORDS_NO_OFFSET(vehJimmyBike, <<-1858.6121, -1214.5627, 12.3070>>)
					SET_ENTITY_QUATERNION(vehJimmyBike, 0.3880, -0.4380, 0.4777, 0.6553)
					APPLY_FORCE_TO_ENTITY(vehJimmyBike, APPLY_TYPE_FORCE, <<0,0,-0.01>>, <<0,0,0>>, 0, TRUE, TRUE, FALSE)
				ENDIF
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
				GO_TO_STAGE(stage_go_to_yacht)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				DEBUG_CUTSCENE_CONTROL()
			#ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

BOOL bGrabMusicPlayed = FALSE
BOOL bRadioKilled = FALSE
BOOL bWaterPedFlee = FALSE
BOOL bCutsceneSkipped = FALSE
FLOAT fHeight

BOOL bWaterCalm

PROC RUN_RESCUE_CUTSCENE()

	SWITCH iStageProgress
	
		CASE 0
			bCutsceneSkipped = FALSE
			REQUEST_VEHICLE_RECORDING(1, "famtwoplayer")
			REQUEST_VEHICLE_RECORDING(101, "famtwo")
			REQUEST_VEHICLE_RECORDING(102, "famtwo")
			REQUEST_ANIM_DICT("missfam2_tracy_jetski")
			IF NOT IS_CUTSCENE_ACTIVE()
				PRINTLN("REQUESTING CUTSCENE IN CUT STAGE")
				REQUEST_CUTSCENE("family_2_mcs_3")
				iStageProgress++
			ELSE
				iStageProgress+=2
			ENDIF
			bWaterPedFlee = FALSE
		BREAK
		
		CASE -1
			REQUEST_IPL("smboat")
			WHILE NOT IS_IPL_ACTIVE("smBoat")
				WAIT(0)
			ENDWHILE
			iStageProgress = 3
			bReplaySetupRequired = FALSE
		BREAK
		
		CASE 1
		
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(pedBoatDancer[0])
				AND NOT IS_PED_INJURED(pedBoatDancer[0])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Guy_on_sofa", pedBoatDancer[0])	
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedBoatDancer[1])
				AND NOT IS_PED_INJURED(pedBoatDancer[1])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Man_getting_wood", pedBoatDancer[1])	
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedBoatDancer[2])
				AND NOT IS_PED_INJURED(pedBoatDancer[2])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Girl_on_sofa", pedBoatDancer[2])	
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedBoatDancer[3])
				AND NOT IS_PED_INJURED(pedBoatDancer[3])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Girl_being_chatted_up", pedBoatDancer[3])	
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedTracy)
				AND NOT IS_PED_INJURED(pedTracy)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Tracy", pedTracy)	
				ENDIF
				
				iStageProgress++
				
			ENDIF
			
		BREAK
		
		CASE 2
		
			BOOL bChasersReady
			bChasersReady = FALSE
			
			IF CREATE_CHASER(0, TRUE)
			AND CREATE_CHASER(1, TRUE)
				PRINTLN("READY")
				bChasersReady = TRUE
			ELSE
				PRINTLN("NOT READY")
			ENDIF
			
			IF bChasersReady
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "famtwoplayer")
				iStageProgress++
				IF bReplaySetupRequired
					iStageProgress = -1
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
		
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
				bWaterComponentsGrabbed = FALSE
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				IF NOT IS_PED_INJURED(pedTracy)
					PRINTLN("REGISTERING TRACEY")
					REGISTER_ENTITY_FOR_CUTSCENE(pedTracy, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					PRINTLN("\n\n\nSETTING PLAYER SEASHARK ANCHOR OFF\n\n\n")
					SET_BOAT_ANCHOR(vehPlayerSeashark, FALSE)
					REGISTER_ENTITY_FOR_CUTSCENE(vehPlayerSeashark, "Michaels_seashark", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, FALSE, TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(objMP3Dock)
					REGISTER_ENTITY_FOR_CUTSCENE(objMP3Dock, "Prop_MP3_Dock", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedChasers[0])
					FREEZE_ENTITY_POSITION(pedChasers[0], FALSE)
					SET_PED_CAN_RAGDOLL(pedChasers[0], TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(pedChasers[0], "Porn_Dude", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedChasers[1])
					FREEZE_ENTITY_POSITION(pedChasers[1], FALSE)
					SET_PED_CAN_RAGDOLL(pedChasers[1], TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(pedChasers[1], "Porn_Dude^1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehChasers[0])
					PRINTLN("Registered seashark 0 to Porn_Dude_seashark")
					REGISTER_ENTITY_FOR_CUTSCENE(vehChasers[0], "Porn_Dude_seashark", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehChasers[1])
					PRINTLN("Registered seashark 1 to Porn_Dude_^1_seashark")
					REGISTER_ENTITY_FOR_CUTSCENE(vehChasers[1], "Porn_Dude_^1_seashark", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedBoatDancer[0])
				AND NOT IS_PED_INJURED(pedBoatDancer[0])
					SET_PED_DIES_IN_WATER(pedBoatDancer[0], FALSE)
					SET_PED_COMBAT_ATTRIBUTES(pedBoatDancer[0], CA_ALWAYS_FLEE, TRUE)
					SET_PED_LOD_MULTIPLIER(pedBoatDancer[0], 1.75)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBoatDancer[0], TRUE)
					TASK_LOOK_AT_ENTITY(pedBoatDancer[0], PLAYER_PED_ID(), -1, SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_HIGH)
					bWaterComponentsGrabbed = TRUE
					REGISTER_ENTITY_FOR_CUTSCENE(pedBoatDancer[0], "Guy_on_sofa", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)	
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedBoatDancer[1])
				AND NOT IS_PED_INJURED(pedBoatDancer[1])
					REGISTER_ENTITY_FOR_CUTSCENE(pedBoatDancer[1], "Man_getting_wood", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)	
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedBoatDancer[2])
				AND NOT IS_PED_INJURED(pedBoatDancer[2])
					REGISTER_ENTITY_FOR_CUTSCENE(pedBoatDancer[2], "Girl_on_sofa", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)	
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedBoatDancer[3])
				AND NOT IS_PED_INJURED(pedBoatDancer[3])
					REGISTER_ENTITY_FOR_CUTSCENE(pedBoatDancer[3], "Girl_being_chatted_up", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)	
				ENDIF
				
				START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
				CLEAR_PRINTS()
				CLEAR_HELP()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				PRE_INIT_ASSETS(stage_speedophile_chase)
				INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
				PREPARE_MUSIC_EVENT("FAM2_NECK_GRAB")
				bGrabMusicPlayed = FALSE
				bRadioKilled = FALSE
				iStageProgress++
				
			ENDIF
			
		BREAK
		
		CASE 4
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			CLEAR_AREA(<<-2006.246582,-1043.832275,-1.481001>>, 200, TRUE)
			iStageProgress++
		BREAK
		
		CASE 5
		
			IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
			
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tracy")
					PRINTLN("Tracey out")
					IF NOT IS_PED_IN_VEHICLE(pedTracy, vehPlayerSeashark)
						SET_PED_INTO_VEHICLE(pedTracy, vehPlayerSeashark, VS_FRONT_RIGHT)
						SET_ENTITY_INVINCIBLE(pedTracy, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTracy)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					PRINTLN("CAMERA OUT")
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Porn_Dude")
					PRINTLN("Porn_Dude OUT")
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Porn_Dude^1")
					PRINTLN("Porn_Dude^1 OUT")
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Porn_Dude_seashark")
					PRINTLN("Porn_Dude_seashark OUT")
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Porn_Dude^1_seashark")
					PRINTLN("Porn_Dude^1_seashark OUT")
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_seashark")
//				AND CAN_SET_EXIT_STATE_FOR_CAMERA()
				
					PRINTLN("Michaels_seashark out")
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerSeashark)
					ENDIF
					
					SET_WATER_CALM(TRUE, 0)
					
					START_PLAYBACK_RECORDED_VEHICLE(vehPlayerSeashark, 1, "famtwoplayer")
					SET_PLAYBACK_SPEED(vehPlayerSeashark, 0.66)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					
					SET_ENTITY_COORDS(vehPlayerSeashark, vSeasharkChaseStartCoords)
					SET_ENTITY_HEADING(vehPlayerSeashark, fSeasharkChaseStartCoords)
					SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, TRUE, TRUE)
					SET_VEHICLE_FORWARD_SPEED(vehPlayerSeashark, 15)
					
					STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerSeashark)
					GET_WATER_HEIGHT(vSeasharkChaseStartCoords, fHeight)
					
					IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
						SET_ENTITY_COORDS(vehPlayerSeashark, <<vSeasharkChaseStartCoords.x, vSeasharkChaseStartCoords.y,  fHeight>>)
						SET_ENTITY_HEADING(vehPlayerSeashark, fSeasharkChaseStartCoords)
						SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, TRUE, TRUE)
						SET_VEHICLE_FORWARD_SPEED(vehPlayerSeashark, 17)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					ENDIF
					
					INT i
					VECTOR vTemp
					FLOAT fTemp
					REPEAT COUNT_OF(vehChasers) i
					
						IF DOES_ENTITY_EXIST(vehChasers[i])
						AND IS_VEHICLE_DRIVEABLE(vehChasers[i])
						AND DOES_ENTITY_EXIST(pedChasers[i])
						AND NOT IS_PED_INJURED(pedChasers[i])
							
							FREEZE_ENTITY_POSITION(pedChasers[i], FALSE)
							SET_PED_CAN_RAGDOLL(pedChasers[i], TRUE)
							SET_BOAT_ANCHOR(vehChasers[i], FALSE)
							
							IF NOT IS_PED_IN_VEHICLE(pedChasers[i], vehChasers[i])
								SET_PED_INTO_VEHICLE(pedChasers[i], vehChasers[i])
							ENDIF
							
							vTemp = GET_ENTITY_COORDS(vehChasers[i])
							GET_WATER_HEIGHT(vTemp, fTemp)
							SET_ENTITY_COORDS(vehChasers[i], <<vTemp.x, vTemp.y, fTemp>>)
							
							//START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehChasers[i], 101+i, "famtwo", 25, DRIVINGMODE_PLOUGHTHROUGH)
							
							SET_VEHICLE_FORWARD_SPEED(vehChasers[i], 10.0 + (2.0*TO_FLOAT(i)))
							
						ENDIF
						
					ENDREPEAT
					
					IF NOT WAS_CUTSCENE_SKIPPED()
						DESTROY_ALL_CAMS()
						camRescueOutro = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
						SET_CAM_SPLINE_SMOOTHING_STYLE(camRescueOutro, CAM_SPLINE_SLOW_IN_SMOOTH)
						ADD_CAM_SPLINE_NODE_USING_CAMERA(camRescueOutro, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-2023.1742, -1052.9800, 1.5290>>, <<2.1162, 0.0000, -39.8484>>, 50.0000, TRUE), 0)
						ADD_CAM_SPLINE_NODE_USING_CAMERA(camRescueOutro, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-2008.2240, -1037.2172, 1.8555>>, <<-2.0921, 0.0684, -60.6791>>, 50.0000, TRUE), 1400, CAM_SPLINE_NODE_SMOOTH_ROT)
						ADD_CAM_SPLINE_NODE_USING_CAMERA(camRescueOutro, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-1985.4370, -1024.5334, 2.0868>>, <<-3.9139, 0.0000, -78.7979>>, 50.0000, TRUE), 1850)
						SET_CAM_ACTIVE(camRescueOutro, TRUE)
						POINT_CAM_AT_ENTITY(camRescueOutro, PLAYER_PED_ID(), <<0,0,0.3>>)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						FILL_PUSH_IN_DATA(pushInData, PLAYER_PED_ID(), CHAR_MICHAEL, 20, 1000, 1000, 500)
						PRINTLN("@@@@@@@@@@@@ GET FILL_PUSH_IN_DATA 1 @@@@@@@@@@@@@@@@")
						SETTIMERB(0)
					ELSE
						bCutsceneSkipped = TRUE
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerSeashark)
						SET_WATER_CALM(TRUE, 0.1)
						
						GET_WATER_HEIGHT(vSeasharkChaseStartCoords, fHeight)
						
						IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
							SET_ENTITY_COORDS(vehPlayerSeashark, <<vSeasharkChaseStartCoords.x, vSeasharkChaseStartCoords.y,  fHeight>>)
							SET_ENTITY_HEADING(vehPlayerSeashark, fSeasharkChaseStartCoords)
							SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, TRUE, TRUE)
							SET_VEHICLE_FORWARD_SPEED(vehPlayerSeashark, 18)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
			IF NOT bGrabMusicPlayed
				IF PREPARE_MUSIC_EVENT("FAM2_NECK_GRAB")
				AND GET_CUTSCENE_TIME() > 31250
					TRIGGER_MUSIC_EVENT("FAM2_NECK_GRAB")
					bGrabMusicPlayed = TRUE
				ENDIF
			ENDIF
			
			IF NOT bRadioKilled
				IF GET_CUTSCENE_TIME() > 9700
					SET_STATIC_EMITTER_ENABLED("SE_FAMILY_2_BOAT_RADIO",FALSE)
					bRadioKilled = TRUE
				ENDIF
			ENDIF
			
			IF (GET_CUTSCENE_TIME() > 37500)
				IF NOT bWaterCalm
					SET_WATER_CALM(TRUE)
					bWaterCalm = TRUE
				ENDIF			
			ENDIF
			
			IF NOT bWaterPedFlee
				IF GET_CUTSCENE_TIME() > 45000
					IF NOT IS_PED_INJURED(pedBoatDancer[0])
						TASK_GO_STRAIGHT_TO_COORD(pedBoatDancer[0], <<-2097.8667, -973.0261, -1>>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
						bWaterPedFlee = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bWaterComponentsGrabbed
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Guy_on_sofa"))
				AND NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Guy_on_sofa")))
					INT iTemp
					REPEAT COUNT_OF(PED_COMPONENT) iTemp
						iWaterComponents[iTemp][0] = GET_PED_DRAWABLE_VARIATION(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Guy_on_sofa")), INT_TO_ENUM(PED_COMPONENT, iTemp))
						iWaterComponents[iTemp][1] = GET_PED_TEXTURE_VARIATION(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Guy_on_sofa")), INT_TO_ENUM(PED_COMPONENT, iTemp))
						PRINTLN("GRABBING COMPONENTS FOR WATER PED")
					ENDREPEAT
					bWaterComponentsGrabbed = TRUE
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(pedBoatDancer[0])
					PRINTLN("CREATING WATER PED")
					CREATE_WATER_PED()
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				PRINTLN("camera out")
			ENDIF
			
			PRINTLN("frame")
			
			IF NOT IS_CUTSCENE_ACTIVE()
			
				REPLAY_STOP_EVENT()
			
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				IF DOES_ENTITY_EXIST(objMP3Dock)
					DELETE_OBJECT(objMP3Dock)
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_MP3_DOCK)
				ENDIF
				IF NOT bGrabMusicPlayed
					TRIGGER_MUSIC_EVENT("FAM2_CS_SKIP")
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				//cutscene finished, back to game
				INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
				SPECIAL_ABILITY_CHARGE_LARGE(PLAYER_ID(), TRUE, FALSE)
				
				IF NOT bGrabMusicPlayed
					CANCEL_MUSIC_EVENT("FAM2_GRAB_NECK")
				ENDIF
				
				SETTIMERA(0)
				
				IF NOT bCutsceneSkipped
					iStageProgress++
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayerSeashark)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerSeashark)
					ENDIF
					GO_TO_STAGE(stage_speedophile_chase)
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 6
		
			IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
			AND GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
				IF TIMERB() > 2350
					RENDER_SCRIPT_CAMS(FALSE, TRUE, 1500, FALSE)
					//STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerSeashark)
				ENDIF
			
			IF TIMERB() > 2500
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayerSeashark)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerSeashark)
					ENDIF
					GO_TO_STAGE(stage_speedophile_chase)
				ENDIF
			ENDIF
			ELIF TIMERB() > 2350
				IF HANDLE_PUSH_IN(pushInData, TRUE)
					PRINTLN("@@@@@@@@@@@@ GET_FOLLOW_VEHICLE_CAM_VIEW_MODE 1 @@@@@@@@@@@@@@@@")
				RENDER_SCRIPT_CAMS(FALSE, TRUE, 1500, FALSE)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayerSeashark)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerSeashark)
					ENDIF
					RESET_PUSH_IN(pushInData)
					GO_TO_STAGE(stage_speedophile_chase)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC RUN_FINAL_CUTSCENE()

	PRINTLN("stage prog ", iStageProgress)
	
	IF IS_SCREEN_FADED_OUT()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
			SET_AUDIO_FLAG("HoldMissionCompleteWhenPrepared", TRUE)
			REQUEST_PTFX_ASSET()
			REQUEST_ANIM_DICT( "missfam2_pier")
			REQUEST_MODEL(PROP_CIGAR_01)
			REQUEST_MODEL(CRUISER)
			IF NOT IS_CUTSCENE_ACTIVE()
				IF bPlayFullFinalCut
					REQUEST_CUTSCENE("family_2_mcs_4")
					PRINTLN("@@@@@@@@@@ CUTSCENE REQUEST_CUTSCENE(family_2_mcs_4) @@@@@@@@@@@")
				ELSE
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("family_2_mcs_4", CS_SECTION_2)
					PRINTLN("@@@@@@@@@@ CUTSCENE REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(family_2_mcs_4, CS_SECTION_2) 2 @@@@@@@@@@@")
				ENDIF
				iStageProgress++
			ELSE
				iStageProgress+=2
			ENDIF
		BREAK
		
		CASE 1
			PRINTLN("WAITING ON CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY")
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tracy", pedTracy)
				SET_CUTSCENE_PED_PROP_VARIATION("Tracy", ANCHOR_EYES, 1, 0)
				SET_CUTSCENE_PED_PROP_VARIATION("Tracy", ANCHOR_HEAD, 0, 0)
				IF IS_AUDIO_SCENE_ACTIVE("FAMILY_2_JETSKI_GO_TO_BEACH")
					STOP_AUDIO_SCENE("FAMILY_2_JETSKI_GO_TO_BEACH")			
				ENDIF
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 2
			PRINTLN("WAITING ON CUTSCENE TO LOAD")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tracy", pedTracy)
			SET_CUTSCENE_PED_PROP_VARIATION("Tracy", ANCHOR_EYES, 1, 0)
			SET_CUTSCENE_PED_PROP_VARIATION("Tracy", ANCHOR_HEAD, 0, 0)
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					SET_VEHICLE_ENGINE_ON(vehPlayerSeashark, FALSE, TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(vehPlayerSeashark, "Michaels_seashark", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_PED_INJURED(pedTracy)
					REGISTER_ENTITY_FOR_CUTSCENE(pedTracy, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_PED_INJURED(pedJimmy)
					REGISTER_ENTITY_FOR_CUTSCENE(pedJimmy, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				CLEAR_PRINTS()
				CLEAR_HELP()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				TRIGGER_MUSIC_EVENT("FAM2_COMPLETE")
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				REQUEST_ANIM_DICT( "missfam2_pier")
				REQUEST_MODEL(PROP_CIGAR_01)
				REQUEST_MODEL(CRUISER)
				INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			REMOVE_DECALS_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100)
			TRIGGER_MISSION_STATS_UI(TRUE, TRUE)
			SET_AUDIO_FLAG("AvoidMissionCompleteDelay", FALSE)
			iStageProgress++
		BREAK
		
		CASE 4
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL")
				PRINTLN("MIKE OUT")
				IF IS_VEHICLE_DRIVEABLE(vehPlayerSeashark)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerSeashark)
					APPLY_FORCE_TO_ENTITY(vehPlayerSeashark, APPLY_TYPE_FORCE, <<0,0,0.1>>, <<0,0,0>>,0, TRUE, TRUE, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(pedJimmy)
					DELETE_PED(pedJimmy)
				ENDIF
				IF DOES_ENTITY_EXIST(pedTracy)
					DELETE_PED(pedTracy)
				ENDIF
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1607.2512, -1138.1034, 1.1443>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 262.2968)
			ENDIF
			
			OVERRIDE_LODSCALE_THIS_FRAME(1.5)
			
			IF IS_SCREEN_FADED_OUT()
			AND WAS_CUTSCENE_SKIPPED()
				PRINTLN("SKIPPED SCENE")
				SETTIMERA(0)
				DO_SCREEN_FADE_OUT(0)
			ENDIF
			
			IF IS_CUTSCENE_ACTIVE()
				IF GET_CUTSCENE_TIME() > 31400
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(1500)
					ENDIF
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE)
				ENDIF
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
				PRINTLN("CUTSCENE OUT")
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
				
				REPLAY_STOP_EVENT()
				
				iStageProgress++
			ENDIF
			
			#IF IS_DEBUG_BUILD
				DEBUG_CUTSCENE_CONTROL()
			#ENDIF
			
		BREAK
		
		CASE 5
		
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				TRIGGER_MUSIC_EVENT("FAM2_STOP")
				IF DOES_ENTITY_EXIST(vehPlayerCar)
					SET_MISSION_VEHICLE_GEN_VEHICLE(vehPlayerCar, <<-1060.5181, -1691.8796, 3.5097>>, 185.4265)
				ENDIF
				CLEAR_PED_BLOOD_DAMAGE_BY_ZONE(PLAYER_PED_ID(), PDZ_TORSO)
				CLEAR_PED_BLOOD_DAMAGE_BY_ZONE(PLAYER_PED_ID(), PDZ_RIGHT_ARM)
				CLEAR_PED_BLOOD_DAMAGE_BY_ZONE(PLAYER_PED_ID(), PDZ_LEFT_ARM)
				WHILE NOT SETUP_CLOTHES(FALSE, TRUE)
					WAIT(0)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
				ENDWHILE
				SETTIMERA(0)
				REMOVE_IPL("smboat")
				iStageProgress=100
			ENDIF
			
		BREAK
		
		CASE 100
		
			REQUEST_PTFX_ASSET()
			DO_QUICK_WARP(<< -1850.139, -1250.253, 8.590 >>, -120.1799)
			REQUEST_ANIM_DICT( "missfam2_pier")
			REQUEST_MODEL(PROP_CIGAR_01)
			REQUEST_MODEL(CRUISER)
			
			WHILE NOT HAS_ANIM_DICT_LOADED( "missfam2_pier")
			OR NOT HAS_MODEL_LOADED(PROP_CIGAR_01)
			OR NOT HAS_MODEL_LOADED(CRUISER)
				PRINTLN("LOADING ASSETS FOR SMOKING SCENE SCENE")
				WAIT(0)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
			ENDWHILE
			
			SET_AUDIO_FLAG("HoldMissionCompleteWhenPrepared", FALSE)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1850.139, -1250.253, 8.590 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 134.280)
			
			vehEndBike = CREATE_VEHICLE(CRUISER, <<-1846.5731, -1214.7306, 12.5755>>, 0)
			SET_ENTITY_QUATERNION(vehEndBike, -0.0155, -0.0834, -0.3060, 0.9483)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehEndBike)
			CLEAR_PED_WETNESS(PLAYER_PED_ID())
			
			objCig = CREATE_OBJECT(PROP_CIGAR_01, << -1850.139, -1250.253, 8.590 >>)
			ATTACH_ENTITY_TO_ENTITY(objCig, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,180>>)
			START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_cig_smoke", objcig, <<0.05,0,0>>, <<0,0,90>>)
			
			camFinal = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			SET_CAM_PARAMS(camFinal, <<-1852.0020, -1250.2244, 8.9706>>, <<-3.6975, 0.0000, -120.1799>>, 45) 
			SET_CAM_PARAMS(camFinal, <<-1852.0870, -1250.3954, 8.9706>>, <<-1.3266, 0.0000, -102.4498>>, 45, 10000)
			SET_WIDESCREEN_BORDERS(TRUE, 0)
			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("mission_stat_watcher")) > 0
				TRIGGER_MISSION_STATS_UI(TRUE, TRUE) // failsafe trigger for 1582752 
				WHILE NOT g_bResultScreenDisplaying
					PRINTLN("g_bResultScreenDisplaying = FALSE")
					WAIT(0)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
				ENDWHILE
				
				WHILE g_bResultScreenDisplaying
					PRINTLN("g_bResultScreenDisplaying = TRUE")
					WAIT(0)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
				ENDWHILE
				
			ELSE
			
				PRINTLN("mission stat watcher is not running")
				WAIT(3000)
				
			ENDIF	
			
			CLEAR_AREA_OF_PEDS(<< -1850.150, -1250.253, 8.625 >>, 5)
			
			iFinalScene = CREATE_SYNCHRONIZED_SCENE(<< -1850.150, -1250.253, 8.625 >>, << 0.000, 0.000, 134.280 >>)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iFinalScene, "missfam2_pier", "pier_lean_toss_cigarette", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SETTIMERA(0)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1500)
			ENDIF
			
			iStageProgress++
			
		BREAK
		
		CASE 101
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iFinalScene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(iFinalScene) > 0.227
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("cs_cig_exhale_nose", PLAYER_PED_ID(), <<-0.025,0.12,0>>, <<0,90,0>>, BONETAG_HEAD)
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 102
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iFinalScene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(iFinalScene) > 0.563
				SET_CAM_PARAMS(camFinal, GET_CAM_COORD(camFinal), GET_CAM_ROT(camFinal), GET_CAM_FOV(camFinal))
				IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) != CAM_VIEW_MODE_FIRST_PERSON)
				SET_CAM_PARAMS(camFinal, <<-1852.9216, -1252.6982, 9.3524>>, <<-2.9046, 0.0000, -46.5763>>, 50, 4000)
				ELSE
					SET_CAM_PARAMS(camFinal, <<-1851.6848, -1251.4376, 9.3524>>, <<-0.0547, 0.0543, -38.2308>>, 50, 4000)
				ENDIF
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 103
			IF DOES_ENTITY_EXIST(objCig)
			AND IS_ENTITY_ATTACHED(objCig)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iFinalScene) > 0.711
					DETACH_ENTITY(objCig)
					SET_ENTITY_VELOCITY(objCig, <<-1852.4299, -1250.9132, 8.3737>> - <<-1850.8989, -1250.4399, 9.2628>>)
				ENDIF
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iFinalScene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(iFinalScene) > 0.768
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("cs_cig_exhale_mouth", PLAYER_PED_ID(), <<-0.025,0.13,0>>, <<0,0,0>>, BONETAG_HEAD)
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 104
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iFinalScene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(iFinalScene) > 0.7116
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("cs_cig_exhale_mouth", PLAYER_PED_ID(), <<-0.025,0.13,0>>, <<0,0,0>>, BONETAG_HEAD)
				FILL_PUSH_IN_DATA(pushInData, PLAYER_PED_ID(), CHAR_MICHAEL)
				PRINTLN("@@@@@@@@@@@@ GET FILL_PUSH_IN_DATA 2 @@@@@@@@@@@@@@@@")
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 105
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iFinalScene)
				IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
					IF DOES_CAM_EXIST(camFinal)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iFinalScene) > 0.85
						AND HANDLE_PUSH_IN(pushInData, FALSE, TRUE, TRUE, FALSE, FALSE)
							PRINTLN("@@@@@@@@@@@@ GET_FOLLOW_PED_CAM_VIEW_MODE 2 @@@@@@@@@@@@@@@@")
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							IF DOES_CAM_EXIST(camFinal)
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								SET_GAMEPLAY_CAM_RELATIVE_PITCH()
								RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000)
								DESTROY_CAM(camFinal)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE(iFinalScene) > 0.98
					IF DOES_CAM_EXIST(camFinal)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000)
						DESTROY_CAM(camFinal)
					ENDIF
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SET_WIDESCREEN_BORDERS(FALSE, 100)
					IF DOES_ENTITY_EXIST(objCig)
						DELETE_OBJECT(objCig)
					ENDIF
					SETTIMERA(0)
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 106
			IF TIMERA() > 100
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				Mission_Flow_Mission_Passed()
				Mission_Cleanup()
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

//╞═════════╡DEBUG PROCEDURES

#IF IS_DEBUG_BUILD

	PROC DEBUG_PROCS()
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			Mission_Failed("FM2_FAIL")
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			JSKIP()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
			IF NOT IS_PED_INJURED(pedJimmy)
				SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_BERD, 	0, 0)
				SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_TORSO, 	0, 0)
				SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_LEG,		0, 0)
				SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_FEET, 	0, 0)
				SET_PED_COMPONENT_VARIATION(pedJimmy, PED_COMP_DECL, 	0, 0)
				PRINTLN("debug setting vars")
			ENDIF
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			PSKIP()
		ENDIF
		
		IF LAUNCH_MISSION_STAGE_MENU(skipMenuOptions, iStageHolder)
			SWITCH iStageHolder
				CASE 0 selected_stage = stage_intro 			BREAK
				CASE 1 selected_stage = stage_drive_to_venice 	BREAK
				CASE 2 selected_stage = stage_hire_bike		 	BREAK
				CASE 3 selected_stage = stage_bike_ride 		BREAK
				CASE 4 selected_stage = stage_telltale 			bJimmyWon = TRUE	bDebugMenuCutsceneSelection = TRUE	BREAK
				CASE 5 selected_stage = stage_telltale 			bJimmyWon = FALSE	bDebugMenuCutsceneSelection = TRUE	BREAK
				CASE 6 selected_stage = stage_go_to_yacht 		BREAK
				CASE 7 selected_stage = stage_rescue 			BREAK
				CASE 8 selected_stage = stage_speedophile_chase	BREAK
				CASE 9 selected_stage = stage_drive_to_beach	BREAK
				CASE 10 selected_stage = stage_finish 			BREAK
			ENDSWITCH
			b_skip = TRUE
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, DEFAULT, DEFAULT, TRUE, FALSE)
			GO_TO_STAGE(selected_stage)
		ENDIF
		
		IF b_skip
			STOP_CUTSCENE()
			FADE_OUT()
		ENDIF
		
	ENDPROC
	
	FLOAT fOceanWaveAmplitude = 0.0
	FLOAT fNoiseAmplitude = 0.0
	FLOAT fOceanWaveMinAmplitude = 0.0
	FLOAT fOceanWaveMaxAmplitude = 0.0
	FLOAT fRippleBumpiness = 0.0
	FLOAT fRippleDisturb = 0.0
	FLOAT fRippleMinBumpiness = 0.0
	FLOAT fRippleMaxBumpiness = 0.0
	FLOAT fShoreWaveamplitude = 0.0
	FLOAT fShoreWaveMinamplitude = 0.0
	FLOAT fShoreWaveMaxamplitude = 0.0
	FLOAT fOverrideStrength = 1.0
	
	BOOL bWaveOverrideFadeIn = FALSE
	BOOL bWaveOverrideFadeOut = FALSE
	BOOL bApply = FALSE
	BOOL bActive = FALSE
	
	FLOAT fFadeTime = 1.0
	
	PROC BUILD_WIDGETS()
	
		parent_widget = START_WIDGET_GROUP("Family 2")
		
			chase_widget = START_WIDGET_GROUP("Chase information")
				ADD_WIDGET_FLOAT_READ_ONLY("Total duration of recording", fRecordingTime)
				ADD_WIDGET_FLOAT_READ_ONLY("Duration per section", fSectionTime)
				ADD_WIDGET_FLOAT_READ_ONLY("Duration per section", fSectionProgressTime)
				ADD_WIDGET_FLOAT_READ_ONLY("Progress in current section", fSectionProgress)
				ADD_WIDGET_INT_READ_ONLY("Closest node is",  iClosestPoint)
				ADD_WIDGET_BOOL("Chaser 1 is on recording",  bChaserPlaybackForced[0])
				ADD_WIDGET_BOOL("Chaser 2 is on recording",  bChaserPlaybackForced[1])
				ADD_WIDGET_INT_READ_ONLY("Chase Stage", iChaseStage)
				ADD_WIDGET_INT_READ_ONLY("Saved Stage", iSavedStage)
				twChaseSection = ADD_TEXT_WIDGET("Current Chase section")
				twCanalSection = ADD_TEXT_WIDGET("Current Canal section")
				twPrevCanalSection = ADD_TEXT_WIDGET("Previous Canal section")
				ADD_WIDGET_INT_SLIDER("Cutscene exit run time", iRunTime, 0, 2000, 100)
				ADD_WIDGET_INT_SLIDER("Break point", iBreak, 0, 20, 1)
				ADD_WIDGET_BOOL("Start test", bRun)
			STOP_WIDGET_GROUP()
			
			wave_widget = START_WIDGET_GROUP("Wave settings")
				ADD_WIDGET_FLOAT_SLIDER("Ocean wave amplitude", 	fOceanWaveAmplitude, 		0.0, 10.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Min ocean wave amplitude", fOceanWaveMinAmplitude, 	0.0, 20.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Max ocean wave amplitude", fOceanWaveMaxAmplitude, 	0.0, 20.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Shore wave amplitude", 	fShoreWaveamplitude, 		0.0, 20.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Min shore wave amplitude", fShoreWaveMinamplitude, 	0.0, 20.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Max shore wave amplitude", fShoreWaveMaxamplitude, 	0.0, 20.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Ripple bumpiness", 		fRippleBumpiness, 			0.0, 2.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Ripple disturb", 			fRippleDisturb, 			0.0, 2.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Min ripple bumpiness", 	fRippleMinBumpiness, 		0.0, 2.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Max ripple bumpiness", 	fRippleMaxBumpiness, 		0.0, 2.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Noise amplitude", 			fNoiseAmplitude, 			0.0, 50.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Override strength", 		fOverrideStrength, 			0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Override fade time", 		fFadeTime, 					0.0, 10.0, 0.1)
				ADD_WIDGET_BOOL("Apply settings", bApply)
				//ADD_WIDGET_BOOL("Fade override in", bWaveOverrideFadeIn)
				//ADD_WIDGET_BOOL("Fade override out", bWaveOverrideFadeOut)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	PROC RUN_WIDGETS()
	
		//chase info
		SET_CONTENTS_OF_TEXT_WIDGET(twChaseSection, GET_STRING_FROM_CHASE_STAGE(GET_CURRENT_CHASE_ENUM()))
		SET_CONTENTS_OF_TEXT_WIDGET(twCanalSection, GET_CANAL_SECTION_RECORDING_NAME(eCurrentCanalSection))
		SET_CONTENTS_OF_TEXT_WIDGET(twPrevCanalSection, GET_CANAL_SECTION_RECORDING_NAME(ePreviousCanalSection))
		
		//wave settings
		IF bApply
		AND bActive
			WATER_OVERRIDE_SET_OCEANNOISEMINAMPLITUDE(fNoiseAmplitude)
			WATER_OVERRIDE_SET_OCEANWAVEAMPLITUDE(fOceanWaveAmplitude)
			WATER_OVERRIDE_SET_OCEANWAVEMINAMPLITUDE(fOceanWaveMinAmplitude)
			WATER_OVERRIDE_SET_OCEANWAVEMAXAMPLITUDE(fOceanWaveMinAmplitude)
			
			WATER_OVERRIDE_SET_SHOREWAVEAMPLITUDE(fShoreWaveamplitude)
			WATER_OVERRIDE_SET_SHOREWAVEMINAMPLITUDE(fShoreWaveMinamplitude)
			WATER_OVERRIDE_SET_SHOREWAVEMAXAMPLITUDE(fShoreWaveMaxamplitude)
			
			WATER_OVERRIDE_SET_RIPPLEBUMPINESS(fRippleBumpiness)
			WATER_OVERRIDE_SET_RIPPLEDISTURB(fRippleDisturb)
			WATER_OVERRIDE_SET_RIPPLEMAXBUMPINESS(fRippleMaxBumpiness)
			WATER_OVERRIDE_SET_RIPPLEMINBUMPINESS(fRippleMinBumpiness)
						
			WATER_OVERRIDE_SET_STRENGTH(fOverrideStrength)
			bApply = FALSE
		ENDIF
		
		IF bWaveOverrideFadeIn
			IF NOT bActive
				WATER_OVERRIDE_FADE_IN(fFadeTime)
				bActive = TRUE
			ENDIF
			bWaveOverrideFadeIn = FALSE
		ENDIF
		
		IF bWaveOverrideFadeOut
			IF bActive
				WATER_OVERRIDE_FADE_OUT(fFadeTime)
				bActive = FALSE
			ENDIF
			bWaveOverrideFadeOut = FALSE
		ENDIF
		
	ENDPROC
	
	PROC RUN_TEST()
	
		INT iTest
		
		FOR iTest = 0 TO 20
			IF iTest = iBreak
				EXIT
			ELSE
				PRINTLN("Test ", iTest)
			ENDIF
		ENDFOR
		
	ENDPROC
	
#ENDIF

PROC SET_PED_CANT_DANCE(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
	AND IS_PED_IN_ANY_VEHICLE(ped)
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(ped, FALSE)
		SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(ped, FALSE)
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_SMALL_ENOUGH(VEHICLE_INDEX vehTemp)
	MODEL_NAMES model = GET_ENTITY_MODEL(vehTemp)
	VECTOR vMin
	VECTOR vMax
	GET_MODEL_DIMENSIONS(model, vMin, vMax)
	FLOAT fMinHeight = -vMin.z + vMax.z
	
	IF fMinHeight < 2.6
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╞═════════╡ MAIN SCRIPT LOOP ╞════════════════════════════════════════════════════╡

INT iSkipToHour
INT iWalkInScene
BOOL bPrefetchCalled
//TIMEOFDAY todReference

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()
		SET_CLEANUP_TO_RUN_ON_RESPAWN(RCI_FAMILY2)
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		TRIGGER_MUSIC_EVENT("FAM2_STOP")
		MISSION_CLEANUP()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
//	REQUEST_CUTSCENE("family_2_int")
	FAM2_INITIALISE()
	SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
	
	#IF IS_DEBUG_BUILD
		BUILD_WIDGETS()
	#ENDIF
	
	bPrefetchCalled = FALSE
	
	WHILE(TRUE)
	
		IF NOT b_skip
			CHECK_FOR_FAIL()
		ENDIF
		
		IF NOT b_stage_ready
			SETUP_FOR_MISSION_STAGE()
		ENDIF
		
		DO_NO_STAMINA_HELP_TEXT()
		WATCH_STATS_FOR_MISSION()
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_DaddysLittleGirl")
		
		SWITCH current_mission_stage
		
			CASE stage_pre_intro
			
				IF IS_PLAYER_PLAYING(PLAYER_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
				ENDIF
				
				SWITCH iStageProgress
				
					CASE 0
					
						IF NOT DOES_ENTITY_EXIST(pedJimmy)
							IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
								PRINTLN("CREATING JIMMY IN SCRIPT")
								CREATE_JIMMY(<<-808.0422, 168.5755, 75.7504>>, 244.0649)
							ELSE
								PRINTLN("GRABBING TRIGGER SCENE JIMMY")
								SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
								pedJimmy = g_sTriggerSceneAssets.ped[0]
								// 1984737
								//g_sTriggerSceneAssets.ped[0] = NULL
							ENDIF
						ENDIF
						
						IF NOT IS_VALID_INTERIOR(intMikeMansion)
							intMikeMansion = GET_INTERIOR_AT_COORDS(<<-813.1963, 179.4912, 71.1592>>)
						ENDIF
						
						REQUEST_ANIM_DICT("missfam2mcs_intp1")
						REQUEST_WAYPOINT_RECORDING(sJimmyStairRoute)
						REQUEST_MODEL(V_ILEV_MM_DOORM_L)
						REQUEST_MODEL(V_ILEV_MM_DOORM_R)
						REQUEST_ADDITIONAL_TEXT("FAMILY2", MISSION_TEXT_SLOT)
						REQUEST_ADDITIONAL_TEXT(sConvBlock, MISSION_DIALOGUE_TEXT_SLOT)
						
						IF NOT bPrefetchCalled
							PREFETCH_SRL("family_2_Timelapse_srl")
							bPrefetchCalled = TRUE
						ENDIF
						
						IF IS_INTERIOR_READY(intMikeMansion)
						AND HAS_ANIM_DICT_LOADED("missfam2mcs_intp1")
						AND DOES_ENTITY_EXIST(pedJimmy)
						AND REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
						AND DOES_ENTITY_EXIST(pedJimmy)
						AND HAS_MODEL_LOADED(V_ILEV_MM_DOORM_L)
						AND HAS_MODEL_LOADED(V_ILEV_MM_DOORM_R)
						AND GET_IS_WAYPOINT_RECORDING_LOADED(sJimmyStairRoute)
						AND IS_SRL_LOADED()
						AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
						AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
							REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
							
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							
							iWalkInScene = CREATE_SYNCHRONIZED_SCENE(<< -817.311, 179.330, 71.241 >>, << 0.000, 0.000, -113.000 >>)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iWalkInScene, "missfam2mcs_intp1", "fam_2_int_p1_michael", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 1.0, FALSE, FALSE)
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_LOCKED, FALSE, TRUE)
							ENDIF
							
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), -1.0, FALSE, FALSE)
								DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_LOCKED, FALSE, TRUE)
							ENDIF
							
							CREATE_MODEL_HIDE(vLeftDoorPos, 1.0, 	V_ILEV_MM_DOORM_L, FALSE)
							CREATE_MODEL_HIDE(vRightDoorPos, 1.0,	V_ILEV_MM_DOORM_R, FALSE)
							
							objDoorL = CREATE_OBJECT(V_ILEV_MM_DOORM_L, vLeftDoorPos)
							objDoorR = CREATE_OBJECT(V_ILEV_MM_DOORM_R, vRightDoorPos)
							
							PLAY_SYNCHRONIZED_ENTITY_ANIM(objDoorL, iWalkInScene, "fam_2_int_p1_doorl", "missfam2mcs_intp1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(objDoorR, iWalkInScene, "fam_2_int_p1_doorr", "missfam2mcs_intp1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objDoorL)
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objDoorR)
							
							DESTROY_ALL_CAMS()
							
							camIGIntro = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
							
							PLAY_SYNCHRONIZED_CAM_ANIM(camIGIntro, iWalkInScene, "fam_2_int_p1_cam", "missfam2mcs_intp1")
							
							SET_WIDESCREEN_BORDERS(TRUE, 0)               
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FAMILY_2)
							
							SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
							
							BEGIN_SRL()
							
							SETTIMERA(0)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_MM_DOORM_L)
							SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_MM_DOORM_R)
							
							PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_YOGA_FLIP_FLOPS)
							
							SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 17, 0)
							SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 16, 0)
							SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_FEET, 1, 0)
							
							REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
							
							IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
								IF IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(GET_PLAYERS_LAST_VEHICLE())
									INT iVeh
									REPEAT NUM_NPC_VEHICLE_IDS iVeh
										IF DOES_ENTITY_EXIST(g_viCreatedNPCVehicleIDs[iVeh])
											IF VDIST2(GET_ENTITY_COORDS(g_viCreatedNPCVehicleIDs[iVeh]), v_car_start_coords) < 30*30
												SET_ENTITY_AS_MISSION_ENTITY(g_viCreatedNPCVehicleIDs[iVeh], TRUE, TRUE)
												DELETE_VEHICLE(g_viCreatedNPCVehicleIDs[iVeh])
												iVeh = NUM_NPC_VEHICLE_IDS
											ENDIF
										ENDIF
									ENDREPEAT
								ENDIF
							ENDIF
							
							DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(vJimmyCoords, 1000)
							
							VEHICLE_INDEX vehTemp
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								PRINTLN("PLAYER IS IN VEHICLE")
							ELSE
								vehTemp = GET_LAST_DRIVEN_VEHICLE()
								PRINTLN("PLAYER IS NOT IN VEHICLE")
							ENDIF
							
							IF DOES_ENTITY_EXIST(vehTemp)
							AND IS_VEHICLE_DRIVEABLE(vehTemp)
								IF NOT IS_VEHICLE_MODEL(vehTemp, TAXI)
								AND NOT IS_VEHICLE_IN_PLAYERS_GARAGE(vehTemp, CHAR_MICHAEL, TRUE)
									IF IS_VEHICLE_SMALL_ENOUGH(vehTemp)
									AND VDIST(GET_ENTITY_COORDS(vehTemp), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 50
										PRINTLN("PLAYER VEHICLE IS NEARBY")
										SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
										#IF IS_DEBUG_BUILD
											PRINTLN("Vehicle belongs to: ", GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(vehTemp))
										#ENDIF
										IF GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(vehTemp) = CHAR_MICHAEL
											DELETE_VEHICLE(vehTemp)
											PRINTLN("Deleting as this is the player's vehicle")
										ELSE
											SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<-822.4229, 157.8134, 69.0913>>, 91.164993)
											SET_ENTITY_COORDS(vehTemp, <<-822.4229, 157.8134, 69.0913>>)
											SET_ENTITY_HEADING(vehTemp, 91.164993)
											SET_VEHICLE_DOORS_SHUT(vehTemp, TRUE)
											PRINTLN("SETTING PLAYER VEH AS veh gen")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							CLEAR_AREA(v_car_start_coords, 200, TRUE)
							CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
														
							STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 300)
							REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 300)
							SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
							
							iStageProgress++
							
						ENDIF
						
					BREAK
					
					CASE 1
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
						
						SET_SRL_TIME(TO_FLOAT(TIMERA()))
						
						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
							FADE_OUT()
							iStageProgress+=2
						ENDIF
						
						IF (IS_SYNCHRONIZED_SCENE_RUNNING(iWalkInScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iWalkInScene) > 0.95)
						OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iWalkInScene)
							iWalkInScene = -1
							sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
							ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<-835.1494, 180.1434, 71.9064>>, <<12.6567, 0.0000, -102.4400>>, 5000)
							ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<-835.7494, 180.2434, 71.9064>>, <<12.6567, 0.0000, -102.4400>>, 5000)
							SET_CAM_FOV(sTimelapse.splineCamera, 47.9931)
							SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
							PLAY_SOUND_FRONTEND(-1, "TIME_LAPSE_MASTER")
							SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE, FALSE)
							SET_CAM_ACTIVE(camIGIntro, FALSE)
							iSkipToHour = 10
							SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
							iStageProgress++
						ENDIF
						
					BREAK
					
					CASE 2
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
						
						SET_SRL_TIME(TO_FLOAT(TIMERA()))
						IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(iSkipToHour, 0, "EXTRASUNNY", "cirrocumulus", sTimelapse)
							RELEASE_AMBIENT_AUDIO_BANK()
							SETTIMERA(0)
							iStageProgress++
						ENDIF
						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
							SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE)
							WAIT(0)
							FADE_OUT()
							iStageProgress++
						ENDIF
					BREAK
					
					CASE 3
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
						
						IF TIMERA() > 300
						AND HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
						AND GET_IS_WAYPOINT_RECORDING_LOADED("fam2Stairs")
							SETUP_CLOTHES()
							IF DOES_ENTITY_EXIST(pedJimmy)
								SET_ENTITY_VISIBLE(pedJimmy, TRUE)
								SET_ENTITY_COLLISION(pedJimmy, TRUE)
								FREEZE_ENTITY_POSITION(pedJimmy, FALSE)
							ENDIF
							SET_CLOCK_TIME(10, 0, 0)
							iStageProgress++
						ENDIF
					BREAK
					
					CASE 4
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
						
						iStageProgress = 0
						current_mission_stage = stage_intro
					BREAK
					
				ENDSWITCH
				
			BREAK
			
			CASE stage_intro
				RUN_INTRO_CUTSCENE()
			BREAK
			CASE stage_drive_to_venice
				GO_TO_VENICE()
			BREAK
			CASE stage_hire_bike
				DO_IN_GAME_BIKE_HIRE()
			BREAK
			CASE stage_bike_ride
				BIKE_RIDE2()
			BREAK
			CASE stage_telltale
				RUN_PIER_CUTSCENE()
			BREAK
			CASE stage_go_to_yacht
				GO_TO_YACHT()
			BREAK
			CASE stage_rescue
				RUN_RESCUE_CUTSCENE()
			BREAK
			CASE stage_speedophile_chase
				SPEEDOPHILE_CHASE2()
				MANAGE_AMBIENT_MARINA_BOATS()
			BREAK
			CASE stage_drive_to_beach
				DRIVE_BACK()
			BREAK
			CASE stage_finish
				RUN_FINAL_CUTSCENE()
			BREAK
			CASE stage_lose_cops
				LOSE_THE_COPS()
			BREAK
			DEFAULT
				SCRIPT_ASSERT("Unrecognised Mission Stage!")
			BREAK
		ENDSWITCH
		
		HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
		
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		AND NOT IS_PED_INJURED(pedJimmy)
			IF IS_PED_IN_VEHICLE(pedJimmy, vehPlayerCar)
				SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedJimmy, FALSE)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bRun
				RUN_TEST()
				bRun = FALSE
			ENDIF
		#ENDIF
		
		MANAGE_BLOCKING_AREAS()
		MANAGE_BIKE_SHOP()
		MANAGE_DANCING_SCENE()
		
		IF DOES_ENTITY_EXIST(pedJimmy)
		AND NOT IS_PED_INJURED(pedJimmy)
			SET_PED_RESET_FLAG(pedJimmy, PRF_DisablePlayerMeleeFriendlyAttacks, TRUE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedTracy)
		AND NOT IS_PED_INJURED(pedTracy)
			SET_PED_RESET_FLAG(pedTracy, PRF_DisablePlayerMeleeFriendlyAttacks, TRUE)
		ENDIF
		
		IF bPlayerGroundingChecked
			bPlayerGroundingChecked = FALSE
		ENDIF
		
		IF bChaserGroundingChecked
			bChaserGroundingChecked = FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DEBUG_PROCS()
			RUN_WIDGETS()
		#ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
