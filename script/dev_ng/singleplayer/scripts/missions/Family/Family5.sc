//|=======================================================================================|
//|                 Author:  Lukasz Bogaj / Brenda Carey 	 Date: 25/02/2011             |
//|=======================================================================================|
//|                                  	  FAMILY5.sc	                                  |
//|                            		  DID SOMEBODY SAY YOGA		                          |
//|                             									                      |
//|=======================================================================================|

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//===================================== INCLUDE FILES ====================================|

USING "rage_builtins.sch"
USING "globals.sch"

//Commands headers
USING "commands_ped.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "commands_camera.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_streaming.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_path.sch"
USING "commands_audio.sch"
USING "commands_entity.sch"
USING "commands_recording.sch"

//Script headers
USING "script_player.sch"
USING "script_ped.sch"
USING "script_buttons.sch"
USING "script_maths.sch"
USING "script_blips.sch"

//Public files
USING "flow_public_core_override.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "locates_public.sch"
USING "building_control_public.sch"
USING "dialogue_public.sch"
USING "vehicle_gen_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "tv_control_public.sch"
USING "context_control_public.sch"
USING "CompletionPercentage_public.sch"
USING "mission_stat_public.sch"
USING "taxi_functions.sch"
USING "drunk_public.sch"
USING "shop_public.sch"
USING "clearMissionArea.sch"
USING "Yoga.sch"
USING "timeLapse.sch"
USING "carsteal_public.sch"
USING "emergency_call.sch"

//Debug headers
#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//===================================== ENUMS & STRUCTS ==================================|

/// PURPOSE: Specifies stages of the mission.
ENUM MISSION_STAGES
	MISSION_STAGE_PRE_TIMELAPSE				= 0,
	MISSION_STAGE_TIMELAPSE					= 1,		//Timelapse
	MISSION_STAGE_CUTSCENE_INTRO			= 2,		//Intro cutscene
	MISSION_STAGE_YOGA_WARRIOR				= 3,		//Yoga minigame	- player has to complete warrior pose
	MISSION_STAGE_CUTSCENE_TRIANGLE			= 4,		//Yoga cutscene - Fabien shows how to do triangle pose
	MISSION_STAGE_YOGA_TRIANGLE				= 5,		//Yoga minigame - player has to complete triangle pose
	MISSION_STAGE_CUTSCENE_SUNSALUTATION	= 6,		//Yoga cutscene - Fabien shows how to do sunsalutation pose
	MISSION_STAGE_YOGA_SUNSALUTATION		= 7,		//Yoga minigame - player has to complete sunsalutation pose
	MISSION_STAGE_CUTSCENE_POOL				= 8,		//Pool cutscene - Michael attacks the yoga instructor and lands in the pool
	MISSION_STAGE_GOTO_JIMMYS_ROOM			= 9,		//Go upstairs to Jimmy's room
	MISSION_STAGE_CUTSCENE_JIMMYS_ROOM		= 10,		//Jimmy's room cutscene
	MISSION_STAGE_GOTO_BURGER_SHOT			= 11,		//Go to Burger Shot
	MISSION_STAGE_CUTSCENE_BURGER_SHOT		= 12,		//Burger Shot cutscene
	MISSION_STAGE_DRIVE_HOME				= 13,		//Drive home with Jimmy and drink
	MISSION_STAGE_CUTSCENE_BLACKOUT			= 14,		//Blackout cutscene - Michael wakes up and sees monkeys and aliens
	MISSION_STAGE_FLIGHT					= 15,		//Michael wakes up flying in the sky
	MISSION_STAGE_GO_HOME_WASTED			= 16,		//Get back to the house after blackout
	MISSION_STAGE_CUTSCENE_END_1			= 17,		//End cutscene part 1
	MISSION_STAGE_CUTSCENE_END_2			= 18,		//End cutscene part 2
	
	#IF IS_DEBUG_BUILD
		MISSION_STAGE_TEST					= 19,		//Debug stage to test triggering the in car mocap cutscene
	#ENDIF
	
	MISSION_STAGE_PASSED,								//Mission passed
	MISSION_STAGE_FAILED								//Mission failed
ENDENUM

/// PURPOSE: Specifies mission fail reasons.
ENUM MISSION_FAILS
	MISSION_FAIL_EMPTY = 0,								//Generic fail
	MISSION_FAIL_YOGA_FAILED,							//Player failed yoga minigame
	MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE, 				//Michael's car undriveable
	MISSION_FAIL_MICHAELS_CAR_DEAD,						//Michael's car destroyed
	MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND,				//Micahel's car left behind
	MISSION_FAIL_AMANDA_DEAD,							//Amanda dead
	MISSION_FAIL_FABIEN_DEAD,							//Fabien dead
	MISSION_FAIL_JIMMY_DEAD,							//Jimmy dead
	MISSION_FAIL_JIMMY_LEFT_BEHIND,						//Jimmy left behind
	MISSION_FAIL_DEALER_ATTACKED,						//Dealer attacked
	MISSION_FAIL_DEALER_SCARED,							//Dealer scared
	MISSION_FAIL_DEALER_DEAD,							//Dealer killed
	MISSION_FAIL_FORCE_FAIL,							//Debug forced fail
	
	MAX_MISSION_FAILS
ENDENUM

/// PURPOSE: Specifies mid-mission checkpoints for mission retry.
ENUM MID_MISSION_STAGES
	MID_MISSION_STAGE_YOGA_WARRIOR				= 0,	//Checkpoint at MISSION_STAGE_YOGA_WARRIOR
	MID_MISSION_STAGE_YOGA_TRIANGLE				= 1,	//Checkpoint at MISSION_STAGE_YOGA_TRIANGLE
	MID_MISSION_STAGE_YOGA_SUNSALUTATION		= 2,	//Checkpoint at MISSION_STAGE_YOGA_SUNSALUTATION
	MID_MISSION_STAGE_GOTO_JIMMYS_ROOM			= 3,	//Checkpoint at MISSION_STAGE_GOTO_JIMMYS_ROOM
	MID_MISSION_STAGE_GOTO_BURGER_SHOT			= 4,	//Checkpoint at MISSION_STAGE_GOTO_BURGER_SHOT
	MID_MISSION_STAGE_DRIVE_HOME				= 5,	//Checkpoint at MISSION_STAGE_DRIVE_HOME
	MID_MISSION_STAGE_GO_HOME_WASTED			= 6		//Checkpoint at MISSION_STAGE_GO_HOME_WASTED
ENDENUM

ENUM MISSION_SFX
	MISSION_SFX_NONE = 0,
	MISSION_SFX_ALIENS,
	MISSION_SFX_CHIMPS,
	MISSION_SFX_HEARTBEAT,
	MISSION_SFX_DRINK,
	MISSION_SFX_VOMIT,
	MISSION_SFX_TAP
ENDENUM

/// PURPOSE: 
ENUM MISSION_OUTFITS
	MO_MISSION_START 							= 0,
	MO_DEFAULT									= 1,
	MO_YOGA										= 2,
	MO_YOGA_FLIP_FLOPS							= 3,
	MO_UNDERWEAR								= 4,
	MO_MISSION_END								= 5
ENDENUM

ENUM MANSION_DOORS
	DOOR_PATIO_LEFT								= 0,
	DOOR_PATIO_RIGHT							= 1,
	DOOR_JIMMYS_ROOM							= 2,
	
	MAX_MANSION_DOORS
ENDENUM

/// PURPOSE: Specifies ped behaviour states.
ENUM PED_STATES
	PED_STATE_IDLE								= 0,
	PED_STATE_MOVING_TO_LOCATION,
	PED_STATE_PLAYING_LOOP_ANIMATION,
	PED_STATE_PLAYING_ACTION_ANIMATION,
	PED_STATE_COMBAT,
	PED_STATE_FLEEING
ENDENUM

ENUM MISSION_TRIGGER_MEANS
	MISSION_TRIGGER_ON_FOOT						= 0,
	MISSION_TRIGGER_IN_VEHICLE
ENDENUM

/// PURPOSE: Specifies details of mission peds.
STRUCT PED_STRUCT
	PED_INDEX				PedIndex				//Ped Index
	BLIP_INDEX				BlipIndex				//Ped's Blip Index
	VECTOR					vPosition				//Ped's start location
	FLOAT					fHeading				//Ped's start heading
	MODEL_NAMES				ModelName				//Ped's model
	INT						iProgress
	INT						iPedConversationTimer	//Ped's conversation timer
	INT						iTimer					//Ped's timer
	INT						iLookAtTimer
	INT						iLookAtTime
	INT						iIntervalTime
	BOOL					bLookingAtTarget
	BOOL					bHasTask
	PED_STATES				eState
ENDSTRUCT

/// PURPOSE: Specifies details of mission vehicles.
STRUCT VEH_STRUCT 
	VEHICLE_INDEX			VehicleIndex			//Vehicle index
	BLIP_INDEX				BlipIndex				//Vehicle blip
	VECTOR					vPosition				//Vehicle postion
	FLOAT					fHeading				//Vehicle heading
	MODEL_NAMES				ModelName				//Vehicle model
ENDSTRUCT

/// PURPOSE: Specifies details of mission specific objects.
STRUCT OBJECT_STRUCT
	OBJECT_INDEX			ObjectIndex				//Object index
	MODEL_NAMES				ModelName				//Object model
	VECTOR					vPosition				//Object postion
	VECTOR					vRotation				//Object rotation
	VECTOR					vOffset					//Object offset
ENDSTRUCT

/// PURPOSE: Specifies details of mission conversation.
STRUCT CONV_STRUCT
	STRING					sBlock
	STRING					sRoot
	BOOL					bTriggered
	BOOL					bPaused
	BOOL					bFinished
ENDSTRUCT

/// PURPOSE: Specifies details of mission sound effects.
STRUCT SFX_STRUCT
	STRING					sSoundName
	INT						iSoundID
	BOOL					bTriggered
ENDSTRUCT

/// PURPOSE: Contains player postion, heading and destination.
STRUCT LOCATION_STRUCT
	BLIP_INDEX				DestinationBlip			//Stage destination blip
	VECTOR 					vStageStart				//Stage start position
	FLOAT 					fStartHeading			//Stage start heading
ENDSTRUCT

/// PURPOSE: Specifies details of angled area and safe position to trigger in game cutscene showing Michael get kicked out of the car
STRUCT LOCATE_STRUCT
	VECTOR					vPosition1				//Position1
	VECTOR					vPosition2				//Position2
	FLOAT					fWidth					//Width
	VECTOR					vCutscenePosition		//Desired cutscene start position
	FLOAT					fCutsceneHeading		//Desired cutscene start heading
ENDSTRUCT

/// PURPOSE: Used for getting input of the analogue sticks.
STRUCT STICKS_STRUCT
	INT						iLeftX
	INT						iLeftY
	INT						iRightX
	INT						iRightY
ENDSTRUCT

STRUCT DOOR_STRUCT
	VECTOR					vPosition					//Gate/door position
	MODEL_NAMES				ModelName					//Gate/door model
	BOOL					bLocked						//Specifies if the gate/door is locked
	BOOL					bClosed						//Specifies if the gate/door is closed (open ratio i 0.0)
ENDSTRUCT

//|======================================= CONSTANTS =====================================|

CONST_INT 					MAX_MODELS 						10
CONST_INT 					MAX_ANIMATIONS					8

CONST_INT					MAX_YOGA_FAILS					3
CONST_INT					IDEAL_MISSION_START_TIME		9

TWEAK_INT					FLIGHT_TIME_MAX					201000

TWEAK_INT					iKickOutDelayTime				30000
TWEAK_FLOAT					fShapeTestRadiusFront			0.5
TWEAK_FLOAT					fShapeTestRadiusSide			0.5
TWEAK_FLOAT					fKickOutCameraShake				1.75
TWEAK_FLOAT					fKickOutCameraBlur				0.25

VECTOR						vShapeTestCapsuleOffsetFront	= << 0.0, 7.0, 0.30 >>
VECTOR						vShapeTestCapsuleOffsetSide0	= << -3.0, 6.0, 0.20 >>
VECTOR						vShapeTestCapsuleOffsetSide2	= << -3.0, 2.0, 0.20 >>
VECTOR						vShapeTestCapsuleOffsetSide3	= << -3.0, -2.0, 0.20 >>
VECTOR						vShapetestCapsuleOffsetAbove	= << -1.27, 1.95, 4.9 >>

VECTOR						vFabienWarriorPosition			= << -789.529, 187.843, 72.835 >>
VECTOR						vFabienWarriorRotation			= << 0.000, 0.000, 130.0 >>
VECTOR						vFabienTrianglePosition			= << -789.529, 187.843, 72.840 >>
VECTOR						vFabienTriangleRotation			= << 0.000, 0.000, 145.000 >>
VECTOR						vFabienSunsalutationPosition	= << -789.229, 187.843, 72.850 >>
VECTOR						vFabienSunsalutationRotation	= << 0.000, 0.000, 123.120 >>

VECTOR						vAmandaWarriorPosition			= << -788.830, 185.793, 72.855 >>//72.875 >>
VECTOR						vAmandaWarriorRotation			= << -0.000, 0.000, 31.680 >>
VECTOR						vAmandaTrianglePosition			= << -788.830, 185.793, 72.840 >>//72.860 >>
VECTOR						vAmandaTriangleRotation			= << -0.000, 0.000, 31.680 >>
VECTOR						vAmandaSunsalutationPosition	= << -788.979, 185.743, 72.860 >>//72.880 >>
VECTOR						vAmandaSunsalutationRotation	= << -0.000, 0.000, 30.000 >>

VECTOR						vWarriorStartCamPosition		= << -782.802368, 187.4389, 73.326614>>
VECTOR						vWarriorEndCamPosition			= <<-786.706604,187.146271,72.723785>>
VECTOR						vWarriorStartCamRotation		= << -3.540541, 0.000000, 99.799942>>
VECTOR						vWarriorEndCamRotation			= <<1.793574,-0.025068,101.152802>>
FLOAT						fWarriorStartCamFOV				= 39.3555
FLOAT						fWarriorEndCamFOV				= 39.355499

VECTOR						vTriangleStartCamPosition		= <<-786.294434,187.576950,72.849815>>
VECTOR						vTriangleEndCamPosition			= <<-786.646545,187.717834,72.843674>>
VECTOR						vTriangleStartCamRotation		= <<-1.207426,0.002569,105.266243>>
VECTOR						vTriangleEndCamRotation			= <<-1.207426,0.002569,108.284561>>
FLOAT						fTriangleStartCamFOV			= 39.355499
FLOAT						fTriangleEndCamFOV				= 39.355499

VECTOR						vSunSalutaionStartCamPosition	= <<-786.237061,187.436996,73.012199>>
VECTOR						vSunSalutaionEndCamPosition		= <<-786.602051,187.224121,73.010460>>
VECTOR						vSunSalutaionStartCamRotation	= <<-0.244137,-0.022206,103.085312>>
VECTOR						vSunSalutaionEndCamRotation		= <<-0.244137,-0.022206,101.277870>>
FLOAT						fSunSalutaionStartCamFOV		= 39.355499
FLOAT						fSunSalutaionEndCamFOV			= 39.355499

TWEAK_FLOAT					fAnimationForcePhase			0.778
TWEAK_FLOAT					fChimpsCameraShake				1.0
TWEAK_FLOAT					fChimpsCameraBlur				1.0
TWEAK_FLOAT					fKickAnimationTriggerDistance	150.0

TWEAK_FLOAT					BASE_Z_FORCE_VALUE				50.0

VECTOR						vForceValue						= << 0.0, 150.0, 50.0 >>//<< 0.0, 250.0, 50.0 >> //z = 2.5 - fast descent
VECTOR						vForceOffset					= << 0.0, 0.0, 0.0 >>

VECTOR						vMichaelsHouseCoords			= << -819.68, 175.87, 70.61 >>
VECTOR						vJimmysRoomsCoords				= << -806.04, 173.59, 75.74 >>
VECTOR						vKitchenCoords					= << -801.42, 182.86, 71.61 >>

TWEAK_INT					iAnimatedCameraInterpTime		3450
TWEAK_FLOAT					fAnimatedCameraStopPhase		0.723
TWEAK_FLOAT					fFallAnimationStopPhase			1.000

TWEAK_FLOAT					fRelativePitch					5.0 //20.0

TWEAK_FLOAT					fFocusPushHintFOV				45.5
TWEAK_FLOAT					fFocusPushSideOffset			1.600
TWEAK_FLOAT					fFocusPushVerticalOffset		-0.880
TWEAK_FLOAT					fFocusPushFollowDistanceScalar	0.735
TWEAK_FLOAT					fFocusPushBaseOrbitPitchOffset	-1.200
VECTOR						vFocusPushHintPosition			= << -1175.32, -897.03, 14.05 >>

TWEAK_FLOAT					iSTART_VOMITING_TIME			0.530
TWEAK_FLOAT					iSTOP_VOMITING_TIME				0.635
TWEAK_FLOAT					iSTART_FP_FLASH_TIME			0.925

TEXT_LABEL 					sLabelOBJ 						= "FAM5_GT4"
TEXT_LABEL					sLabelMGETIN					= "CMN_GENGETINY"

//======================================== VARIABLES =====================================|

MISSION_TRIGGER_MEANS		eMissionTriggerMeans			= MISSION_TRIGGER_ON_FOOT
MISSION_STAGES				eMissionStage					= MISSION_STAGE_PRE_TIMELAPSE
MISSION_FAILS				eMissionFail					= MISSION_FAIL_EMPTY
MISSION_SFX					eMissionSfx						= MISSION_SFX_NONE

//mission peds
PED_STRUCT					psAmanda
PED_STRUCT					psFabien
PED_STRUCT					psJimmy
PED_STRUCT					psDealer

//Michael's car
VEH_STRUCT 					vsMichaelsCar

//mission objects
OBJECT_STRUCT				osCup
OBJECT_STRUCT				osNote
OBJECT_STRUCT				osCiggy
OBJECT_STRUCT				osBSDoor
OBJECT_STRUCT				osMethBag
OBJECT_STRUCT				osGamepad
OBJECT_STRUCT				osHeadset
OBJECT_STRUCT				osNecklace
OBJECT_STRUCT				osBSDoorDummy
OBJECT_STRUCT				osLeftDoorDummy
OBJECT_STRUCT				osRightDoorDummy

YOGASTRUCT					sYogaPlayer

LOCATES_HEADER_DATA 		sLocatesData
CHASE_HINT_CAM_STRUCT 		sCinematicCamStruct
VEHICLE_GEN_DATA_STRUCT		sVehicleGenDataStruct

//progress counters
INT 						iStageSetupProgress
INT 						iMissionStageProgress
BOOL 						bSkipFlag
BOOL 						bReplayFlag
BOOL 						bLoadingFlag
BOOL						bStageReplayInProgress

BOOL 						bTimelapseTriggered
BOOL						bTimelapseCutsceneSkipped

//asset request counters
INT 						iModelsRequested
INT 						iAnimationsRequested

//used for pinning/unpinning interior when stage loading
VECTOR 						vInteriorPosition

BOOL						bJimmysOutfitSet
BOOL 						bBuddiesYogaOutfitsSet
BOOL 						bJimmyLeftBehindSprinting
BOOL 						bPlayerPedScriptTaskInterrupted
BOOL 						bDrinkAnimationFinished
BOOL						bPreDrinkConversationFinished
BOOL						bPostDrinkConversationFinished
BOOL						bLookAtActive

//LOCATE_STRUCT 				sEnteredSafeLocate

FLOAT 						fDrunkCameraShake
FLOAT 						fDrunkCameraBlur

INT							iYogaIdleTimer
INT 						iYogaPosesCounter
INT							iYogaBreathsCounter
INT 						iYogaFailsCounter

BOOL						bBreatheConversationTriggered
BOOL						bBreatheCompletedConversationTriggered
BOOL						bBreatheFailedConversationTriggered
BOOL 						bInhaleConversationTriggered
BOOL 						bExhaleConversationTriggered
BOOL 						bFailConversationTriggered

PTFX_ID 					ptfxPuke

structPedsForConversation 	Family5Conversation
INT 						iConversationProgress
BOOL						bConversationsFinished
BOOL						bInitialConversationFinished
BOOL						bCurrentConversationInterrupted
TEXT_LABEL_23				CurrentConversationRoot
TEXT_LABEL_23				CurrentConversationLabel

BOOL						bStreamTriggered
BOOL						bFlightStreamTriggered
BOOL						bCutsceneTriggeredOnBike
INT							iAnimationProgress

//mission music flags
BOOL						bYogaMusicPlaying

CAMERA_INDEX				ScriptedCamera, UndergroundCamera, AnimatedCamera
BOOL 						bCutsceneSkipped
BOOL						bCutsceneAssetsRequested
BOOL						bIntroCutsceneAssetsRequested

//time of day variables used for timelapse
structTimelapse				sTimelapse

SCENARIO_BLOCKING_INDEX		ParkScenarioBlockingIndex
SCENARIO_BLOCKING_INDEX		BurgerShotScenarioBlockingIndex

BOOL						bAnimationForced
BOOL						bAnimationsSwitched
BOOL						bPlayerRagdolledEarly
BOOL						bCleanupVehiclesForCamera
BOOL						bCleanupVehiclesForCameraAgain

BOOL						bAnimatedCameraStopped

INT							iVomitSceneID			= -1
INT							iFlyingSceneID			= -1
INT 						iKickOutSceneID			= -1
////INT							iKickOutSceneNewID		= -1
INT							iLeadOutSceneID			= -1
INT							iHouseEnterSceneID		= -1
INT							iLeadOutSceneJimmyID 	= -1

INT							iSpeedZone
INT							iKickOutDelayTimer

BOOL						bCupPassed
BOOL						bLeadInTriggered
BOOL						bLeadInStartCutscene
BOOL						bLeadOutCameraCancelled
BOOL						bIgnoreLeadInFocusPush

INT							iFailRootPlayerCounter
INT							iFailConversationSpeaker

INT							iJimmyRagdolledFromPlayerImpact
BOOL						bJimmyRagdolledFromPlayerImpact

INT							iAmbientSpeechTimer

BOOL						bBlackoutStagesActive

INT							iTODSkipHour
INT							iStageTimeHours
INT							iStageTimeMinutes
INT							iStageTimeSeconds

INT							iDriveReminders

BOOL						bCollisionDetected

BOOL						bDoorOpenRatioSwitched

INT							iLoadSceneTimer

INT							iJimmyDrunkConversationCounter
INT							iMichaelDrunkConversationCounter

FLOAT						fForceBaseModifier		= 1.0
FLOAT						fCurrentTimeScale		= 1.0
FLOAT						fDesiredTimeScale		= 1.0
FLOAT						fDesiredTimeScaleSpeed	= 0.001

INT							iBoostTime
INT							iBoostTimer
INT							iDiveTimer
INT							iDiveStartTime

INT							iVFXProgress
INT							iStreamPlayTime
INT							iTimeScaleChangeTimer

BOOL						bFirstPersonFlashPlayed

//|========================================= ARRAYS ======================================|

//text printing
INT 						iTriggeredTextHashes[30]
INT 						iNumTextHashesStored

BOOL						FailFlags[MAX_MISSION_FAILS]

//asset arrays
MODEL_NAMES 				MissionModels[MAX_MODELS]
STRING						MissionAnimations[MAX_ANIMATIONS]

VEH_STRUCT 					vsParkedVehicles[1]

//Three yoga mats for yoga section and Burger Shot drink cup, broom
OBJECT_STRUCT				osYogaMats[3]

//Sound effects
SFX_STRUCT					ssSounds[6]

//safe car mocap cutscene locates
//LOCATE_STRUCT				sSafeLocates[2]

LOCATION_STRUCT 			MissionPosition

//|========================= MISCELLANEOUS PROCEDURES & FUNCTIONS ========================|

PROC RESET_MISSION_FLAGS()

	//reset setup progress to 1, so that iSetupProgress = 0 only runs once, on initial mission load.
	iStageSetupProgress 			= 1
	
	iMissionStageProgress 			= 0
	
	iConversationProgress			= 0
	
	iYogaIdleTimer					= 0
	iYogaPosesCounter				= 0
	iYogaBreathsCounter				= 0
	iYogaFailsCounter				= 0
	
	psAmanda.iProgress				= 0
	psAmanda.iLookAtTime			= 0
	psAmanda.iIntervalTime			= 0
	psAmanda.bLookingAtTarget		= FALSE
	
	psFabien.iProgress				= 0
	psFabien.iLookAtTime			= 0
	psFabien.iIntervalTime			= 0
	psFabien.bLookingAtTarget		= FALSE
	
	psJimmy.iProgress				= 0
	psDealer.iProgress				= 0
	
	bConversationsFinished			= FALSE
	bInitialConversationFinished	= FALSE
	bCurrentConversationInterrupted	= FALSE

	bCutsceneSkipped				= FALSE
	bCutsceneAssetsRequested		= FALSE
	
	bBreatheConversationTriggered	= FALSE
	bBreatheCompletedConversationTriggered = FALSE
	bBreatheFailedConversationTriggered	= FALSE
	bExhaleConversationTriggered	= FALSE
	bInhaleConversationTriggered	= FALSE
	bFailConversationTriggered		= FALSE
	
	bJimmyLeftBehindSprinting		= FALSE
	
	bPlayerPedScriptTaskInterrupted = FALSE
	
	bCutsceneTriggeredOnBike		= FALSE
	
	bDrinkAnimationFinished			= FALSE
	bPreDrinkConversationFinished	= FALSE
	bPostDrinkConversationFinished	= FALSE
	
	bLookAtActive					= FALSE
	
	iAnimationProgress				= 0
	iKickOutDelayTimer				= 0
	
	bStageReplayInProgress			= FALSE
	
	iHouseEnterSceneID				= -1
	iKickOutSceneID					= -1
	iLeadOutSceneID					= -1
	bCupPassed						= FALSE
	bLeadInTriggered				= FALSE
	bLeadInStartCutscene			= FALSE
	bLeadOutCameraCancelled			= FALSE
	bIgnoreLeadInFocusPush			= FALSE
	
	iFailRootPlayerCounter			= 0
	iFailConversationSpeaker		= 0
	
	iJimmyRagdolledFromPlayerImpact	= 0
	bJimmyRagdolledFromPlayerImpact	= FALSE
	
	iAmbientSpeechTimer				= 0
	
	iDriveReminders					= 0
	
	sLabelMGETIN					= "CMN_GENGETINY"
	
	bCollisionDetected 				= FALSE
	
	bDoorOpenRatioSwitched			= FALSE
	
	iLoadSceneTimer					= 0
	iJimmyDrunkConversationCounter	= 0
	iMichaelDrunkConversationCounter= 0
	
	iVFXProgress					= 0
	iStreamPlayTime					= 0
	iTimeScaleChangeTimer			= 0
	bAnimatedCameraStopped			= FALSE
	bFlightStreamTriggered			= FALSE

	iBoostTime						= 0
	iBoostTimer						= 0
	iDiveTimer 						= 0
	iDiveStartTime					= 0
	
	CurrentConversationRoot			= ""
	CurrentConversationLabel		= ""
	
	bFirstPersonFlashPlayed			= FALSE
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission flags and progress counters are reset.")
	#ENDIF

ENDPROC

FUNC MISSION_STAGES GET_NEXT_MISSION_STAGE_FOR_SHITSKIP(MISSION_STAGES eStage)

	SWITCH eStage									//handle special cases
	
		CASE MISSION_STAGE_YOGA_WARRIOR
		CASE MISSION_STAGE_YOGA_TRIANGLE
		CASE MISSION_STAGE_YOGA_SUNSALUTATION
			RETURN MISSION_STAGE_CUTSCENE_POOL		//when shit skipping past any yoga stage, always go to end yoga cutscene
		BREAK
	
		CASE MISSION_STAGE_CUTSCENE_END_1
			RETURN MISSION_STAGE_CUTSCENE_END_1		//return last stage for last mission stage
		BREAK
			
	ENDSWITCH
	
	INT 			iNextStage						//get next mission stage in mission flow, if this is not a special case stage
	MISSION_STAGES 	eNextStage

	iNextStage = ENUM_TO_INT(eStage) + 1
	eNextStage = INT_TO_ENUM(MISSION_STAGES, iNextStage)
	
	RETURN eNextStage

ENDFUNC

PROC GO_TO_PED_STATE(PED_STRUCT &psPed, PED_STATES eNewPedState)

	psPed.bHasTask = FALSE
	psPed.eState = eNewPedState

ENDPROC

FUNC SCENARIO_BLOCKING_INDEX CREATE_SCENARIO_BLOCKING_AREA(VECTOR vPosition, VECTOR vSize)
	RETURN ADD_SCENARIO_BLOCKING_AREA(<< vPosition.x - vSize.x, vPosition.y - vSize.y, vPosition.z - vSize.z >>,
									  << vPosition.x + vSize.x, vPosition.y + vSize.y, vPosition.z + vSize.z >>)
ENDFUNC

//PROC INITIALISE_SAFE_LOCATES()
//
//	sSafeLocates[0].vPosition1			= <<-857.708252,234.978500,71.601776>>
//	sSafeLocates[0].vPosition2			= <<-862.866028,199.273117,78.389381>>
//	sSafeLocates[0].fWidth				= 32.0	
//	sSafeLocates[0].vCutscenePosition 	= <<-854.6779, 70.3448, 51.6033>>
//	sSafeLocates[0].fCutsceneHeading	= 8.4425
//	
//	sSafeLocates[1].vPosition1			= <<-877.420715,135.745331,55.439068>>
//	sSafeLocates[1].vPosition2			= <<-848.451721,129.641541,65.575523>>
//	sSafeLocates[1].fWidth				= 16.0	
//	sSafeLocates[1].vCutscenePosition 	= <<-854.6779, 70.3448, 51.6033>>
//	sSafeLocates[1].fCutsceneHeading	= 8.4425
//
//ENDPROC

/// PURPOSE:
///    Checks if Michael should have a vest on for the Monkey section of the mission
/// RETURNS:
///    TRUE if he should have a vest. FALSE otherwise
FUNC BOOL SHOULD_MICHAEL_HAVE_VEST()
	PED_COMP_NAME_ENUM eTorso
	eTorso = GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO)
	
	IF eTorso = TORSO_P0_BARE_CHEST
	OR(eTorso >= TORSO_P0_OPEN_SHIRT AND eTorso <= TORSO_P0_OPEN_SHIRT_15)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE // michael should have vest
ENDFUNC

/// PURPOSE:
///    Checks if the specified mocap cutscene has loaded.
///    Removes any other loaded mocap cutscene and requests the specified cutscene until it is loaded.
/// PARAMS:
///    sSceneName - Mocap cutscene name to check for being loaded.
///    CutsceneSection - Cutscene section specifying at what section to start the cutscene. Use CS_SECTION_1 to play from start.
/// RETURNS:
///    TRUE if the specified cutscene has loaded, FALSE if otherwise.   
FUNC BOOL HAS_REQUESTED_CUTSCENE_LOADED(STRING sSceneName, CUTSCENE_SECTION CutsceneSection = CS_SECTION_1)

	//check if the specified cutscene has loaded first
	IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE(sSceneName)
	
		RETURN TRUE
	
	ELSE
		
		//if the expected reuqested cutscene has not loaded, but there already is a loaded cutscene
		//then remove it and keep requesting specified mocap cutscene
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
			REMOVE_CUTSCENE()
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": HAS_REQUESTED_CUTSCENE_LOADED() Removing cutscene from memory, because cutscene ", sSceneName, " was expected to be loaded.")
			#ENDIF
			
		ENDIF
	
		IF ( CutsceneSection = CS_SECTION_1) 
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading.")
			#ENDIF
			
			//if the cutscene is not loaded, keep requesting it
			REQUEST_CUTSCENE(sSceneName)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, ".")
			#ENDIF
		
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading with playback list.")
			#ENDIF
			
			//if the cutscene is not loaded, keep requesting it
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sSceneName, CutsceneSection)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, " cutscene with playback list.")
			#ENDIF
		
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC REMOVE_LABEL_ARRAY_SPACES()
	INT iArrayLength = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT (iArrayLength - 1) i
		IF iTriggeredTextHashes[i] = 0
			IF iTriggeredTextHashes[i+1] != 0
				iTriggeredTextHashes[i] = iTriggeredTextHashes[i+1]
				iTriggeredTextHashes[i+1] = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_LABEL_INDEX(INT iLabelHash)
	INT i = 0
	
	REPEAT iNumTextHashesStored i
		IF iTriggeredTextHashes[i] = iLabelHash
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF GET_LABEL_INDEX(iHash) != -1
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bTrigger)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF bTrigger
		IF NOT HAS_LABEL_BEEN_TRIGGERED(strLabel)
			INT iArraySize = COUNT_OF(iTriggeredTextHashes)
		
			IF iNumTextHashesStored < iArraySize
				iTriggeredTextHashes[iNumTextHashesStored] = iHash
				iNumTextHashesStored++
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", strLabel, " TRUE.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", iNumTextHashesStored, "/", COUNT_OF(iTriggeredTextHashes), ".")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Label array is full.")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		INT iIndex = GET_LABEL_INDEX(iHash)
		IF iIndex != -1
			iTriggeredTextHashes[iIndex] = 0
			REMOVE_LABEL_ARRAY_SPACES()
			iNumTextHashesStored--
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", strLabel, " FALSE.")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", iNumTextHashesStored, "/", COUNT_OF(iTriggeredTextHashes), ".")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iArraySize i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
	
	iNumTextHashesStored = 0
ENDPROC

PROC UPDATE_TRIGGERED_LABEL(TEXT_LABEL &sLabel)

	IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)					//if the label is not empty, which means not yet printed by locates header
	
		IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)				//if it was not set as triggered by script

			IF IS_THIS_PRINT_BEING_DISPLAYED(sLabel)		//if the label was displayed on screen

				SET_LABEL_AS_TRIGGERED(sLabel, TRUE)		//mark it as triggered

			ENDIF
			
		ELSE												//if the label was set as triggered by script,
															//which means it was printed by locates header
			IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sLabel)	//but is not currently being printed, cleared off screen or timed out
			
				sLabel = ""									//mark is as empty
			
			ENDIF
		
		ENDIF

	ENDIF
	
ENDPROC

PROC PRINT_GOD_TEXT_ADVANCED(STRING sLabel, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)
		PRINT(sLabel, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Printing god text advanced ", sLabel, ".")
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_TIME_PASSED(INT iTimeAmount, INT iTimer)

	INT iCurrentTime
	INT iTimeDifference
	
	iCurrentTime = GET_GAME_TIMER()
	
	iTimeDifference = iCurrentTime - iTimeAmount
	
	IF iTimeDifference > iTimer
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(VECTOR vPoint, FLOAT fRadius)
	
	IF IS_ANY_VEHICLE_NEAR_POINT(vPoint, fRadius)

		VEHICLE_INDEX 	ClosestVehicleIndex
	
		ClosestVehicleIndex = GET_CLOSEST_VEHICLE(vPoint, fRadius, DUMMY_MODEL_FOR_SCRIPT, 
												  VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK |
												  VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL |
												  VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES |
												  VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
												  
		IF DOES_ENTITY_EXIST(ClosestVehicleIndex)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Found vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ClosestVehicleIndex)), " near point ", vPoint, " and deleted it.")
			#ENDIF
		ENDIF
		
	ENDIF
	
	CLEAR_AREA_OF_PEDS(vPoint, fRadius)
	CLEAR_AREA_OF_VEHICLES(vPoint, fRadius)
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE(vPoint, fRadius, 255, 0, 0, 128)
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling CLEAR_AREA_OF_PEDS() for point ", vPoint, ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling CLEAR_AREA_OF_VEHICLES() for point ", vPoint, ".")
	#ENDIF
	
ENDPROC

PROC CLEANUP_PED(PED_STRUCT &ped, BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	IF ( bDelete = FALSE )
	
		IF DOES_ENTITY_EXIST(ped.PedIndex)
			
			IF ( bDeathOnly = TRUE )
		
				IF IS_PED_INJURED(ped.PedIndex)

					IF IS_ENTITY_DEAD(ped.PedIndex)
						REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
					ENDIF
					
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF

				ENDIF
				
			ELIF ( bDeathOnly = FALSE )

				IF NOT IS_PED_INJURED(ped.PedIndex)
				
					IF IS_PED_GROUP_MEMBER(ped.PedIndex, PLAYER_GROUP_ID()) 
						REMOVE_PED_FROM_GROUP(ped.PedIndex) 
					ENDIF 

					SET_PED_KEEP_TASK(ped.PedIndex, bKeepTask)
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF
					
				ENDIF

			ENDIF
		
		ENDIF
	
	ELIF ( bDelete = TRUE )

		IF DOES_ENTITY_EXIST(ped.PedIndex)

			IF IS_PED_INJURED(ped.PedIndex)
			OR NOT IS_PED_INJURED(ped.PedIndex)

				IF IS_ENTITY_DEAD(ped.PedIndex)
				OR NOT IS_ENTITY_DEAD(ped.PedIndex)
					REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
				ENDIF
				
				DELETE_PED(ped.PedIndex)
				
				IF DOES_BLIP_EXIST(ped.BlipIndex)
					REMOVE_BLIP(ped.BlipIndex)
				ENDIF
				
			ENDIF

		ENDIF
				
		ped.iTimer							= 0
		ped.iProgress 						= 0
		ped.bHasTask						= FALSE
		ped.eState							= PED_STATE_IDLE
		
	ENDIF

ENDPROC

PROC CLEANUP_PED_GROUP(PED_STRUCT &array[], BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		CLEANUP_PED(array[i], bDelete, bDeathOnly, bKeepTask)
	ENDFOR

ENDPROC

PROC CLEANUP_MISSION_OBJECT(OBJECT_STRUCT &osObject, BOOL bDelete = FALSE)

	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)

		IF IS_ENTITY_ATTACHED(osObject.ObjectIndex)
			DETACH_ENTITY(osObject.ObjectIndex)
		ENDIF

		IF ( bDelete = TRUE )
			DELETE_OBJECT(osObject.ObjectIndex)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(osObject.ObjectIndex)
		ENDIF
	
	ENDIF

ENDPROC

PROC CLEANUP_MISSION_OBJECT_GROUP(OBJECT_STRUCT &osArray[], BOOL bDelete = FALSE)
	
	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(osArray) - 1 )
		
		CLEANUP_MISSION_OBJECT(osArray[i], bDelete)
	
	ENDFOR
	
ENDPROC

PROC UPDATE_DRUGS_AUDIO_SCENE()
	IF IS_AUDIO_SCENE_ACTIVE("FAMILY_5_YOGA_ON_DRUGS")
		//REPLAY_PREVENT_RECORDING_THIS_FRAME() //fix for B*1845251 - don't allow recording any of the drug vfx scenes, which is when this scene is active
		SET_AUDIO_SPECIAL_EFFECT_MODE(AUDIO_SPECIAL_EFFECT_MODE_STONED)
		SET_AUDIO_SCENE_VARIABLE("FAMILY_5_YOGA_ON_DRUGS", "DrugsEffect", (SIN(((TO_FLOAT(GET_GAME_TIMER())/1000)*45)%360))*0.5+0.5)
		SET_AUDIO_SCENE_VARIABLE("FAMILY_5_YOGA_ON_DRUGS", "DrugsEffectSpeech", 0.5 * (SIN(((TO_FLOAT(GET_GAME_TIMER())/1000)*45)%360))*0.5+0.5)
	ENDIF
ENDPROC

PROC RUN_MISSION_SFX_LOADING(MISSION_SFX &eSfx)

	SWITCH eSfx
	
		CASE MISSION_SFX_CHIMPS
			IF REQUEST_SCRIPT_AUDIO_BANK("FAM5_YOGA_CHIMPS")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loaded audio bank FAM5_YOGA_CHIMPS")
				#ENDIF
				eSfx = MISSION_SFX_NONE
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loading audio bank FAM5_YOGA_CHIMPS")				
				#ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_SFX_ALIENS
			IF REQUEST_SCRIPT_AUDIO_BANK("FAM5_YOGA_ALIENS")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loaded audio bank FAM5_YOGA_ALIENS")
				#ENDIF
				eSfx = MISSION_SFX_NONE
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loading audio bank FAM5_YOGA_ALIENS")				
				#ENDIF
			ENDIF			
		BREAK
	
		CASE MISSION_SFX_HEARTBEAT
		CASE MISSION_SFX_DRINK
			IF REQUEST_SCRIPT_AUDIO_BANK("FAM5_YOGA_DRUGS_01")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loaded audio bank FAM5_YOGA_DRUGS_01")
				#ENDIF
				eSfx = MISSION_SFX_NONE
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loading audio bank FAM5_YOGA_DRUGS_01")				
				#ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_SFX_VOMIT
			IF REQUEST_SCRIPT_AUDIO_BANK("TAXI_VOMIT")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loaded audio bank TAXI_VOMIT")
				#ENDIF
				eSfx = MISSION_SFX_NONE
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loading audio bank TAXI_VOMIT")				
				#ENDIF
			ENDIF		
		BREAK

		CASE MISSION_SFX_TAP
			IF REQUEST_SCRIPT_AUDIO_BANK("FAM5_WASH_FACE")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loaded audio bank FAM5_WASH_FACE")
				#ENDIF
				eSfx = MISSION_SFX_NONE
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loading audio bank FAM5_WASH_FACE")				
				#ENDIF
			ENDIF		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC LOAD_MISSION_SFX(MISSION_SFX eSfx)

	eMissionSfx = eSfx

ENDPROC

PROC RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX eSfx)

	SWITCH eSfx
	
		CASE MISSION_SFX_ALIENS
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_YOGA_ALIENS")
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Releasing audio bank FAM5_YOGA_ALIENS.")
			#ENDIF
			
		BREAK
		
		CASE MISSION_SFX_CHIMPS
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_YOGA_CHIMPS")
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Releasing audio bank FAM5_YOGA_CHIMPS.")
			#ENDIF
		BREAK
		
		CASE MISSION_SFX_DRINK
		CASE MISSION_SFX_HEARTBEAT
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_YOGA_DRUGS_01")
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Releasing audio bank FAM5_YOGA_DRUGS_01.")
			#ENDIF
		BREAK
		
		CASE MISSION_SFX_VOMIT
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("TAXI_VOMIT")
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Releasing audio bank TAXI_VOMIT.")
			#ENDIF
		BREAK
		
		CASE MISSION_SFX_TAP
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_WASH_FACE")
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Releasing audio bank FAM5_WASH_FACE.")
			#ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

PROC PLAY_MISSION_SFX(MISSION_SFX eSfx)

	SWITCH eSfx
	
		CASE MISSION_SFX_CHIMPS
		
			IF ssSounds[1].bTriggered = FALSE
				
				IF ssSounds[1].iSoundID = -1
					ssSounds[1].iSoundID = GET_SOUND_ID()
				ENDIF
				
				PLAY_SOUND_FRONTEND(ssSounds[1].iSoundID, ssSounds[1].sSoundName, "FAMILY_5_SOUNDS")
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sfx ", ssSounds[1].sSoundName, ".")
				#ENDIF
				
				ssSounds[1].bTriggered = TRUE
			ENDIF
			
		BREAK
		
		CASE MISSION_SFX_ALIENS
			IF ssSounds[0].bTriggered = FALSE
				
				IF ssSounds[0].iSoundID = -1
					ssSounds[0].iSoundID = GET_SOUND_ID()
				ENDIF
				
				PLAY_SOUND_FRONTEND(ssSounds[0].iSoundID, ssSounds[0].sSoundName, "FAMILY_5_SOUNDS")
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sfx ", ssSounds[0].sSoundName, ".")
				#ENDIF
				
				ssSounds[0].bTriggered = TRUE
				
			ENDIF
		BREAK
		
	
		CASE MISSION_SFX_HEARTBEAT
		
			IF ssSounds[2].bTriggered = FALSE
				
				IF ssSounds[2].iSoundID = -1
					ssSounds[2].iSoundID = GET_SOUND_ID()
				ENDIF
				
				PLAY_SOUND_FRONTEND(ssSounds[2].iSoundID, ssSounds[2].sSoundName, "FAMILY_5_SOUNDS")
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sfx ", ssSounds[2].sSoundName, ".")
				#ENDIF
				
				ssSounds[2].bTriggered = TRUE
				
			ENDIF
		
		BREAK
	
		CASE MISSION_SFX_DRINK
		
			IF ssSounds[3].bTriggered = FALSE
			
				PLAY_SOUND_FROM_ENTITY(ssSounds[3].iSoundID, ssSounds[3].sSoundName, PLAYER_PED_ID(), "FAMILY_5_SOUNDS")
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sfx ", ssSounds[3].sSoundName, ".")
				#ENDIF
				
				ssSounds[3].bTriggered = TRUE
				
			ENDIF
		
		BREAK
		
		CASE MISSION_SFX_VOMIT
		
			IF ssSounds[4].bTriggered = FALSE
			
				IF ssSounds[4].iSoundID = -1
					ssSounds[4].iSoundID = GET_SOUND_ID()
				ENDIF
			
				PLAY_SOUND_FROM_ENTITY(ssSounds[4].iSoundID, ssSounds[4].sSoundName, PLAYER_PED_ID(), "FAMILY_5_SOUNDS")
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sfx ", ssSounds[4].sSoundName, ".")
				#ENDIF
				
				ssSounds[4].bTriggered = TRUE
				
			ENDIF
		
		BREAK
		
		CASE MISSION_SFX_TAP
		
			IF ssSounds[5].bTriggered = FALSE
			
				IF ssSounds[5].iSoundID = -1
					ssSounds[5].iSoundID = GET_SOUND_ID()
				ENDIF
			
				PLAY_SOUND_FROM_COORD(ssSounds[5].iSoundID, ssSounds[5].sSoundName, << -804.52, 169.05, 76.65 >>, "FAMILY_5_SOUNDS")
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sfx ", ssSounds[5].sSoundName, ".")
				#ENDIF
				
				ssSounds[5].bTriggered = TRUE
				
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC STOP_MISSION_SFX(MISSION_SFX eSfx)

	SWITCH eSfx
	
		CASE MISSION_SFX_CHIMPS
		
			IF (ssSounds[1].bTriggered = TRUE)
				
				STOP_SOUND(ssSounds[1].iSoundID)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping sfx ", ssSounds[1].sSoundName, ".")
				#ENDIF

			ENDIF
		
		BREAK
	
	
		CASE MISSION_SFX_ALIENS
		
			IF (ssSounds[0].bTriggered = TRUE)
			
				STOP_SOUND(ssSounds[0].iSoundID)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping sfx ", ssSounds[0].sSoundName, ".")
				#ENDIF

			ENDIF
		
		BREAK
	
		CASE MISSION_SFX_HEARTBEAT
		
			IF (ssSounds[2].bTriggered = TRUE)
			
				STOP_SOUND(ssSounds[2].iSoundID)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping sfx ", ssSounds[2].sSoundName, ".")
				#ENDIF
				
			ENDIF
		
		BREAK
		
		CASE MISSION_SFX_VOMIT
		
			IF (ssSounds[4].bTriggered = TRUE)
			
				STOP_SOUND(ssSounds[4].iSoundID)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping sfx ", ssSounds[4].sSoundName, ".")
				#ENDIF
				
			ENDIF
		
		BREAK
		
		CASE MISSION_SFX_TAP
		
			IF (ssSounds[5].bTriggered = TRUE)
			
				STOP_SOUND(ssSounds[5].iSoundID)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping sfx ", ssSounds[5].sSoundName, ".")
				#ENDIF
				
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC RESET_ALL_SFX_FLAGS(SFX_STRUCT &ssSFXArray[])

	INT i = 0
	
	FOR i = 0 TO COUNT_OF(ssSFXArray) - 1
	
		ssSFXArray[i].bTriggered = FALSE
	
	ENDFOR

ENDPROC

PROC RELEASE_SFX_SOUND_IDS()

	INT i = 0
	
	FOR i = 0 TO COUNT_OF(ssSounds) - 1
		IF (ssSounds[i].iSoundID <> -1)
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Releasing sound id: ", ssSounds[i].iSoundID, " for sound index: ", i)
			#ENDIF	
		
			RELEASE_SOUND_ID(ssSounds[i].iSoundID)
			
		ENDIF
	ENDFOR

ENDPROC

FUNC BOOL IS_SHAPE_TEST_CAPSULE_CLEAR(VECTOR vStartPosition, VECTOR vCapsulePosition, FLOAT fTestRadius #IF IS_DEBUG_BUILD, STRING sName #ENDIF)

	INT		iHitResults
	VECTOR	vReturnPosition
	VECTOR	vReturnNormal
	ENTITY_INDEX hitEntity
	
	SHAPETEST_INDEX ShapeTestCapsuleIndex = START_SHAPE_TEST_CAPSULE(vStartPosition, vCapsulePosition, fTestRadius, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE, PLAYER_PED_ID())
					
	IF GET_SHAPE_TEST_RESULT(ShapeTestCapsuleIndex, iHitResults, vReturnPosition, vReturnNormal, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shape test ", sName , " results not ready.")
		#ENDIF
	ENDIF
			
			
	IF ( iHitResults != 0 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shape test ", sName , " capsule hitting something.")
			DRAW_DEBUG_SPHERE(vCapsulePosition, fTestRadius, 255, 0, 0, 64)
			DRAW_DEBUG_TEXT_ABOVE_COORDS(vCapsulePosition, sName, 0.0)
		#ENDIF
		
		RETURN FALSE
		
	ELSE
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_SPHERE(vCapsulePosition, fTestRadius, 0, 255, 0, 64)
			DRAW_DEBUG_TEXT_ABOVE_COORDS(vCapsulePosition, sName, 0.0)
		#ENDIF
		
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_SHAPE_TEST_CAPSULE_CLEAR_FOR_ANIMATION(VEHICLE_INDEX VehicleIndex, VECTOR vRoadNodePosition, INT iRoadLanes, FLOAT fAnimationPhase, FLOAT fZOffset, FLOAT fZOffsetFromRoadNode)
	

	IF DOES_ENTITY_EXIST(VehicleIndex)
		IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
		
			IF HAS_ANIM_DICT_LOADED("missfam5mcs_6")

				VECTOR vVehiclePosition, vVehicleRotation, vAnimInitialPosition, vShapeTestPosition
				
				VECTOR vAnimInitialPositionFromRoadNode, vShapeTestPositionFromRoadNode
				
				vVehiclePosition 		= GET_ENTITY_COORDS(VehicleIndex)
				vVehicleRotation 		= GET_ENTITY_ROTATION(VehicleIndex)
				
				vAnimInitialPosition 	= GET_ANIM_INITIAL_OFFSET_POSITION("missfam5mcs_6", "push_michael_out_player0",
																		   vVehiclePosition, vVehicleRotation, fAnimationPhase)
																		
				vShapeTestPosition		= << vAnimInitialPosition.x, vAnimInitialPosition.y, vAnimInitialPosition.z + fZOffset >>
				
				
				vAnimInitialPositionFromRoadNode = GET_ANIM_INITIAL_OFFSET_POSITION("missfam5mcs_6", "push_michael_out_player0",
																		   			<< vRoadNodePosition.x, vRoadNodePosition.y, vVehiclePosition.z >>,
																					vVehicleRotation, fAnimationPhase)
																					
				vShapeTestPositionFromRoadNode	 = << vAnimInitialPositionFromRoadNode.x, vAnimInitialPositionFromRoadNode.y, vAnimInitialPositionFromRoadNode.z + fZOffsetFromRoadNode >>																	
			
				IF ( iRoadLanes = 1 )								
													
					IF 	IS_SHAPE_TEST_CAPSULE_CLEAR(vAnimInitialPosition, vShapeTestPosition, 0.5 #IF IS_DEBUG_BUILD, "ANIM" #ENDIF)
					AND IS_SHAPE_TEST_CAPSULE_CLEAR(vAnimInitialPositionFromRoadNode, vShapeTestPositionFromRoadNode, 0.65 #IF IS_DEBUG_BUILD, "ANIM FROM ROADNODE" #ENDIF)
						
						RETURN TRUE
						
					ENDIF
					
				ELSE
				
					IF 	IS_SHAPE_TEST_CAPSULE_CLEAR(vAnimInitialPosition, vShapeTestPosition, 0.5 #IF IS_DEBUG_BUILD, "ANIM" #ENDIF)
						
						RETURN TRUE
						
					ENDIF
				
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_PEDS_IN_VEHICLE_AT_SAFE_POSITION_FOR_INGAME_CUTSCENE(PED_INDEX PedIndex1, PED_INDEX PedIndex2, VEHICLE_INDEX VehicleIndex)

	IF 	DOES_ENTITY_EXIST(PedIndex1)
	AND DOES_ENTITY_EXIST(PedIndex2)
	AND DOES_ENTITY_EXIST(VehicleIndex)
		IF 	NOT IS_ENTITY_DEAD(PedIndex1)
		AND NOT IS_ENTITY_DEAD(PedIndex2)
		AND IS_VEHICLE_DRIVEABLE(VehicleIndex)
			IF 	IS_PED_SITTING_IN_VEHICLE(PedIndex1, VehicleIndex)
			AND IS_PED_SITTING_IN_VEHICLE(PedIndex2, VehicleIndex)
			
				IF  IS_VEHICLE_ON_ALL_WHEELS(VehicleIndex)
				AND NOT IS_ENTITY_ON_FIRE(VehicleIndex)
				AND NOT IS_VEHICLE_STOPPED(VehicleIndex)
					
					INT		iTotalLanes
					FLOAT	fHeading
					VECTOR 	vVehiclePosition
					VECTOR 	vClosestNodeToVehicle
					VECTOR	vTestCoordsFront
					VECTOR	vTestCoordsSide0
					VECTOR	vTestCoordsSide2
					VECTOR	vTestCoordsSide3
					VECTOR	vTestCoordsAbove
					
					vVehiclePosition = GET_ENTITY_COORDS(VehicleIndex)

					IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vVehiclePosition, 1, vClosestNodeToVehicle, fHeading, iTotalLanes, NF_NONE)
					
						#IF IS_DEBUG_BUILD
							TEXT_LABEL_63 Label = "LANES: "
							Label +=  iTotalLanes
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, Label, 1.25, 0, 255, 0)
							DRAW_DEBUG_SPHERE(vClosestNodeToVehicle, 0.25, 0, 255, 255)
						#ENDIF
					
						IF ( GET_DISTANCE_BETWEEN_COORDS(vVehiclePosition, vClosestNodeToVehicle) < 4.5 * iTotalLanes )
						
							vTestCoordsFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vShapeTestCapsuleOffsetFront)
							vTestCoordsSide0 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vShapeTestCapsuleOffsetSide0)
							vTestCoordsSide2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vShapeTestCapsuleOffsetSide2)
							vTestCoordsSide3 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vShapeTestCapsuleOffsetSide3)
							vTestCoordsAbove = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vShapetestCapsuleOffsetAbove)
					
							IF 	IS_SHAPE_TEST_CAPSULE_CLEAR(vVehiclePosition, vTestCoordsFront, fShapeTestRadiusFront #IF IS_DEBUG_BUILD, "FRONT" #ENDIF)
							AND IS_SHAPE_TEST_CAPSULE_CLEAR(vVehiclePosition, vTestCoordsSide0, fShapeTestRadiusSide #IF IS_DEBUG_BUILD, "SIDE0" #ENDIF)
							AND IS_SHAPE_TEST_CAPSULE_CLEAR(vVehiclePosition, vTestCoordsSide2, fShapeTestRadiusSide #IF IS_DEBUG_BUILD, "SIDE2" #ENDIF)
							AND IS_SHAPE_TEST_CAPSULE_CLEAR(vVehiclePosition, vTestCoordsSide3, fShapeTestRadiusSide #IF IS_DEBUG_BUILD, "SIDE3" #ENDIF)
							AND IS_SHAPE_TEST_CAPSULE_CLEAR(vVehiclePosition, vTestCoordsAbove, fShapeTestRadiusSide #IF IS_DEBUG_BUILD, "ABOVE" #ENDIF)
							AND IS_SHAPE_TEST_CAPSULE_CLEAR_FOR_ANIMATION(VehicleIndex, vClosestNodeToVehicle, iTotalLanes, fAnimationForcePhase, -0.40, -0.50)

								IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(PedIndex1, VehicleIndex, VS_DRIVER, TRUE, TRUE)
						
									#IF IS_DEBUG_BUILD
										DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, "SAFE TO PLAY", 2.0, 0, 255, 0)
									#ENDIF
									
									RETURN TRUE
									
								ENDIF
								
							ENDIF
							
						ENDIF
					ENDIF
				
				ENDIF
					
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

//|======================= END MISCELLANEOUS PROCEDURES & FUNCTIONS ======================|

//|======================= DEBUG VARIABLES, PROCEDURES & FUNCTIONS =======================|

#IF IS_DEBUG_BUILD

	//mission skip menu variables
	MissionStageMenuTextStruct	SkipMenu[20]
	INT 			iReturnStage
	BOOL			bStageResetFlag
	
	//debug widgets variables
	WIDGET_GROUP_ID Family5Widgets
	BOOL 			bPositionMats
	BOOL 			bPositionCup
	BOOL 			bCutsceneToggle
	BOOL			bStartTimerToggle
	BOOL			bResetGameplayHint
	BOOL			bDrawDebugLinesAndSpheres

	/// PURPOSE:
	///    Returns string name of the specified mission stage.
	/// PARAMS:
	///    eStage - Mission stage to get string name for.
	/// RETURNS:
	///    String name of the specified mission stage.
	FUNC STRING GET_MISSION_STAGE_NAME_FOR_DEBUG(MISSION_STAGES eStage)
	
		SWITCH eStage

			CASE MISSION_STAGE_PRE_TIMELAPSE
				RETURN "MISSION_STAGE_PRE_TIMELAPSE"
			BREAK
			CASE MISSION_STAGE_TIMELAPSE
				RETURN "MISSION_STAGE_TIMELAPSE"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_INTRO
				RETURN "MISSION_STAGE_CUTSCENE_INTRO"
			BREAK
			CASE MISSION_STAGE_YOGA_WARRIOR
				RETURN "MISSION_STAGE_YOGA_WARRIOR"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_TRIANGLE
				RETURN "MISSION_STAGE_CUTSCENE_TRIANGLE"
			BREAK
			CASE MISSION_STAGE_YOGA_TRIANGLE
				RETURN "MISSION_STAGE_YOGA_TRIANGLE"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
				RETURN "MISSION_STAGE_CUTSCENE_SUNSALUTATION"
			BREAK
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
				RETURN "MISSION_STAGE_YOGA_SUNSALUTATION"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_POOL
				RETURN "MISSION_STAGE_CUTSCENE_POOL"
			BREAK
			CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
				RETURN "MISSION_STAGE_GOTO_JIMMYS_ROOM"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
				RETURN "MISSION_STAGE_CUTSCENE_JIMMYS_ROOM"
			BREAK
			CASE MISSION_STAGE_GOTO_BURGER_SHOT
				RETURN "MISSION_STAGE_GOTO_BURGER_SHOT"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
				RETURN "MISSION_STAGE_CUTSCENE_BURGER_SHOT"
			BREAK
			CASE MISSION_STAGE_DRIVE_HOME
				RETURN "MISSION_STAGE_DRIVE_HOME"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_BLACKOUT
				RETURN "MISSION_STAGE_CUTSCENE_BLACKOUT"
			BREAK
			CASE MISSION_STAGE_FLIGHT
				RETURN "MISSION_STAGE_FLIGHT"
			BREAK
			CASE MISSION_STAGE_GO_HOME_WASTED
				RETURN "MISSION_STAGE_GO_HOME_WASTED"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_END_1
				RETURN "MISSION_STAGE_CUTSCENE_END_1"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_END_2
				RETURN "MISSION_STAGE_CUTSCENE_END_2"
			BREAK
			CASE MISSION_STAGE_TEST
				RETURN "MISSION_STAGE_TEST"
			BREAK
		ENDSWITCH
		
		RETURN "INCORRECT_MISSION_STAGE"
	
	ENDFUNC

	PROC CREATE_DEBUG_WIDGETS()
	
		Family5Widgets = START_WIDGET_GROUP("Mission: Family5")

			ADD_WIDGET_BOOL("Can fail yoga?", FailFlags[MISSION_FAIL_YOGA_FAILED])
			ADD_WIDGET_FLOAT_READ_ONLY("fBlushDecalAlpha", sYogaPlayer.fBlushDecalAlpha)
			
			ADD_WIDGET_FLOAT_SLIDER("fKickAnimationTriggerDistance", fKickAnimationTriggerDistance, 0.0, 500.0, 1.0)
			
			START_WIDGET_GROUP("Yoga Buttons")
				ADD_WIDGET_FLOAT_SLIDER("YOGA_BUTTONS_X", 		YOGA_BUTTONS_X, 		0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("YOGA_BUTTONS_Y", 		YOGA_BUTTONS_Y, 		0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("YOGA_BUTTONS_WIDTH", 	YOGA_BUTTONS_WIDTH, 	0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("YOGA_BUTTONS_HEIGHT", 	YOGA_BUTTONS_HEIGHT, 	0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("YOGA_BUTTONS_SCALE", 	YOGA_BUTTONS_SCALE, 	0.0, 2.0, 0.001)
			STOP_WIDGET_GROUP()
	
			START_WIDGET_GROUP("Free Fall")
				
				ADD_WIDGET_INT_READ_ONLY("Stream play time", iStreamPlayTime)
				ADD_WIDGET_INT_SLIDER("FLIGHT_TIME_MAX", FLIGHT_TIME_MAX, 0, 300000, 1)
			
				ADD_WIDGET_FLOAT_SLIDER("fFallAnimationStopPhase", fFallAnimationStopPhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fAnimatedCameraStopPhase", fAnimatedCameraStopPhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_INT_SLIDER("iAnimatedCameraInterpTime", iAnimatedCameraInterpTime, 0, 10000, 100)
			
				ADD_WIDGET_FLOAT_READ_ONLY("fCurrentTimeScale", fCurrentTimeScale)
				ADD_WIDGET_FLOAT_SLIDER("fDesiredTimeScale", fDesiredTimeScale, 0.0, 1.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fDesiredTimeScaleSpeed", fDesiredTimeScaleSpeed, 0.0, 1.0, 0.0001)
				
				ADD_WIDGET_FLOAT_SLIDER("fRelativePitch", fRelativePitch, -90.0, 90.0, 1.0)

				ADD_WIDGET_VECTOR_SLIDER("vForceValue", vForceValue, -500.0, 500.0, 1.0)
				ADD_WIDGET_VECTOR_SLIDER("vForceOffset", vForceOffset, -500.0, 500.0, 1.0)
				
			STOP_WIDGET_GROUP()
	
			START_WIDGET_GROUP("Focus push")
				ADD_WIDGET_BOOL("bResetGameplayHint", bResetGameplayHint)
				ADD_WIDGET_FLOAT_SLIDER("fFocusPushHintFOV", fFocusPushHintFOV, 1.0, 60.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("fFocusPushSideOffset", fFocusPushSideOffset, -10.0, 10.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fFocusPushVerticalOffset", fFocusPushVerticalOffset, -10.0, 10.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("fFocusPushFollowDistanceScalar", fFocusPushFollowDistanceScalar, -10.0, 10.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vFocusPushHintPosition", vFocusPushHintPosition, -1500.0, 1500, 1.0)
			STOP_WIDGET_GROUP()
	
			START_WIDGET_GROUP("Yoga mats position")

				ADD_WIDGET_BOOL("Enable positioning", bPositionMats)
				ADD_WIDGET_STRING("Yoga Mat 01")
				ADD_WIDGET_VECTOR_SLIDER("Position: ", osYogaMats[0].vPosition, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation: ", osYogaMats[0].vRotation, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_STRING("Yoga Mat 02")
				ADD_WIDGET_VECTOR_SLIDER("Position: ", osYogaMats[1].vPosition, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation: ", osYogaMats[1].vRotation, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_STRING("Yoga Mat 03")
				ADD_WIDGET_VECTOR_SLIDER("Position: ", osYogaMats[2].vPosition, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation: ", osYogaMats[2].vRotation, -1000.0, 1000.0, 0.01)
				
				ADD_WIDGET_STRING("Gamepad")
				ADD_WIDGET_VECTOR_SLIDER("Position: ", osGamepad.vPosition, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation: ", osGamepad.vRotation, -1000.0, 1000.0, 0.01)
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Burger Shot cup position")
			
				ADD_WIDGET_BOOL("Enable positioning", bPositionCup)
				ADD_WIDGET_STRING("Burger Shot Cup")
				ADD_WIDGET_VECTOR_SLIDER("Position: ", osCup.vPosition, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation: ", osCup.vRotation, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Offset: ", osCup.vOffset, -1000.0, 1000.0, 0.01)
			
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Car cutscene")
			
				ADD_WIDGET_BOOL("Start cutscene", bCutsceneToggle)
				ADD_WIDGET_BOOL("Start timer", bStartTimerToggle)
				ADD_WIDGET_FLOAT_SLIDER("fAnimationForcePhase", fAnimationForcePhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fKickOutCameraBlur", fKickOutCameraBlur, 0, 3.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fKickOutCameraShake", fKickOutCameraShake, 0, 3.0, 0.1)
				ADD_WIDGET_INT_SLIDER("iKickOutDelayTime", iKickOutDelayTime, 0, 60000, 500)
				ADD_WIDGET_FLOAT_SLIDER("fShapeTestRadiusFront", fShapeTestRadiusFront, 0.1, 10.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fShapeTestRadiusSide", fShapeTestRadiusSide, 0.1, 10.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vShapeTestCapsuleOffsetFront", vShapeTestCapsuleOffsetFront, -10.0, 10.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vShapeTestCapsuleOffsetSide2", vShapeTestCapsuleOffsetSide2, -10.0, 10.0, 0.1)
				
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_FLOAT_SLIDER("iSTART_VOMITING_TIME", iSTART_VOMITING_TIME, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("iSTOP_VOMITING_TIME", iSTOP_VOMITING_TIME, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("iSTART_FP_FLASH_TIME", iSTART_FP_FLASH_TIME, 0.0, 1.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("fChimpsCameraShake", fChimpsCameraShake, 0.0, 10.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fChimpsCameraBlur", fChimpsCameraBlur, 0.0, 10.0, 0.1)			
			
		STOP_WIDGET_GROUP()
		
		SET_LOCATES_HEADER_WIDGET_GROUP(Family5Widgets)	
	
	ENDPROC
	
	PROC DELETE_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(Family5Widgets)
			DELETE_WIDGET_GROUP(Family5Widgets)
		ENDIF
	ENDPROC
	
	PROC CREATE_DEBUG_MISSION_STAGE_MENU()

		SkipMenu[0].sTxtLabel		= "PRE TIMELAPSE"
		SkipMenu[1].sTxtLabel		= "TIMELAPSE"
		SkipMenu[2].sTxtLabel		= "CUT - INTRO - FAMILY_5_INT"
		SkipMenu[3].sTxtLabel	 	= "YOGA - WARRIOR"
		SkipMenu[4].sTxtLabel	 	= "CUT - TRIANGLE - FAMILY_5_MCS_1"
		SkipMenu[5].sTxtLabel	 	= "YOGA - TRIANGLE"
		SkipMenu[6].sTxtLabel	 	= "CUT - SUNSALUTATION - FAMILY_5_MCS_2"
		SkipMenu[7].sTxtLabel	 	= "YOGA - SUNSALUTATION"
		SkipMenu[8].sTxtLabel	 	= "CUT - POOL - FAMILY_5_MCS_3"
		SkipMenu[9].sTxtLabel	 	= "GO TO JIMMY'S ROOM"
		SkipMenu[10].sTxtLabel	 	= "CUT - JIMMY - FAMILY_5_MCS_4"
		SkipMenu[11].sTxtLabel	 	= "GO TO BURGER SHOT"
		SkipMenu[12].sTxtLabel	 	= "CUT - BURGER SHOT - FAMILY_5_MCS_5"
		SkipMenu[13].sTxtLabel	 	= "DRIVE HOME"
		SkipMenu[14].sTxtLabel		= "CUT - BLACKOUT - FAM_5_MCS_6"
		SkipMenu[15].sTxtLabel		= "BLACKOUT FLIGHT"
		SkipMenu[16].sTxtLabel		= "GO HOME WASTED"
		SkipMenu[17].sTxtLabel		= "CUT - END 1 - FAMILY_5_MCS_5_P4"
		SkipMenu[18].sTxtLabel		= "CUT - END 2 - FAMILY_5_MCS_5_P5"
		SkipMenu[19].sTxtLabel		= "DEBUG CAR KICK ANIM TEST"
	
	ENDPROC

	PROC RUN_DEBUG_PASS_AND_FAIL_CHECK(MISSION_STAGES &eStage, MISSION_FAILS &eFailReason)
	
		IF IS_KEYBOARD_KEY_PRESSED (KEY_S)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			eStage = MISSION_STAGE_PASSED
	    ENDIF
		
	    IF IS_KEYBOARD_KEY_PRESSED (KEY_F)
			eStage = MISSION_STAGE_FAILED
			eFailReason = MISSION_FAIL_FORCE_FAIL
	    ENDIF
	
	ENDPROC
	
	/// PURPOSE:
	///    Performs a screen fade in with a wait for a specified time.
	/// PARAMS:
	///    iTime - Duration of the fade in miliseconds.
	PROC DO_SAFE_SCREEN_FADE_IN(INT iTime)

		IF NOT IS_SCREEN_FADED_IN()
		
			DO_SCREEN_FADE_IN(iTime)
			
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
		ENDIF

	ENDPROC

	/// PURPOSE:
	///    Performs a screen fade out with a wait for a specified time.
	/// PARAMS:
	///    iTime - Duration of the fade in miliseconds.
	PROC DO_SAFE_SCREEN_FADE_OUT(INT iTime)

		IF NOT IS_SCREEN_FADED_OUT()
		
			DO_SCREEN_FADE_OUT(iTime)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT(0)
			ENDWHILE
			
		ENDIF

	ENDPROC

	/// PURPOSE:
	///    Stops an active mocap cutscene and waits until it has finished.
	PROC DO_SAFE_STOP_CUTSCENE()

		IF IS_CUTSCENE_PLAYING()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			STOP_CUTSCENE(TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)	//needs to be called to make sure all scripted systems work correctly when cutscene is skipped		
		ENDIF

		WHILE NOT HAS_CUTSCENE_FINISHED()
			WAIT(0)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for the cutscene to finish.")
			#ENDIF
		ENDWHILE
		
	ENDPROC
	
	PROC RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()

		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting mission stage cleanup.")

		KILL_PHONE_CONVERSATION()
		HANG_UP_AND_PUT_AWAY_PHONE(TRUE)

		KILL_ANY_CONVERSATION()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		REMOVE_CUTSCENE()

		CLEANUP_PED(psAmanda, TRUE)
		CLEANUP_PED(psFabien, TRUE)
		CLEANUP_PED(psJimmy, TRUE)
		CLEANUP_PED(psDealer, TRUE)

		//reset buddy outfits flags
		bJimmysOutfitSet		= FALSE
		bBuddiesYogaOutfitsSet 	= FALSE
		
		IF DOES_ENTITY_EXIST(osBSDoorDummy.ObjectIndex)
			REMOVE_MODEL_HIDE(osBSDoor.vPosition, 1.0, osBSDoor.ModelName)
		ENDIF
		
		IF DOES_ENTITY_EXIST(osLeftDoorDummy.ObjectIndex)
			REMOVE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName)
		ENDIF
		
		IF DOES_ENTITY_EXIST(osRightDoorDummy.ObjectIndex)
			REMOVE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName)
		ENDIF
		
		CLEANUP_MISSION_OBJECT(osCup, TRUE)
		CLEANUP_MISSION_OBJECT(osNote, TRUE)
		CLEANUP_MISSION_OBJECT(osCiggy, TRUE)
		CLEANUP_MISSION_OBJECT(osMethBag, TRUE)
		CLEANUP_MISSION_OBJECT(osBSDoor, FALSE)
		CLEANUP_MISSION_OBJECT(osGamepad, TRUE)
		CLEANUP_MISSION_OBJECT(osHeadset, TRUE)
		CLEANUP_MISSION_OBJECT(osNecklace, TRUE)
		CLEANUP_MISSION_OBJECT(osBSDoorDummy, TRUE)
		CLEANUP_MISSION_OBJECT(osLeftDoorDummy, TRUE)
		CLEANUP_MISSION_OBJECT(osRightDoorDummy, TRUE)
		CLEANUP_MISSION_OBJECT_GROUP(osYogaMats, TRUE)

		IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
			DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vsParkedVehicles[0].VehicleIndex)
			DELETE_VEHICLE(vsParkedVehicles[0].VehicleIndex)
		ENDIF
		
		//remove blips
		IF DOES_BLIP_EXIST(MissionPosition.DestinationBlip)
			REMOVE_BLIP(MissionPosition.DestinationBlip)
		ENDIF
		
		REMOVE_ANIM_DICT("missfam5_yoga")
		REMOVE_ANIM_DICT("missfam5mcs_6")
		REMOVE_ANIM_DICT("missfam5_drink")
		REMOVE_ANIM_DICT("missfam5_flying")
		REMOVE_ANIM_DICT("missfam5mcs_6drag")
		REMOVE_ANIM_DICT("missfam5_wet_walk")
		REMOVE_ANIM_DICT("missfam5_blackout")
		REMOVE_ANIM_DICT("missfam2mcs_intp1")
		REMOVE_ANIM_DICT("missfam5mcs_4leadin")
		REMOVE_ANIM_DICT("missfam5leadinoutmcs_5")
		
		//disable cam shaking
		Quit_Drunk_Camera_Immediately()			
		STOP_GAMEPLAY_CAM_SHAKING(TRUE)
		STOP_CUTSCENE_CAM_SHAKING(TRUE)
		
		//turn off ped config flags
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			MAKE_PED_SOBER(PLAYER_PED_ID())
			CLEAR_PED_ALTERNATE_WALK_ANIM(PLAYER_PED_ID())
			REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICE, FALSE)
		
		STOP_STREAM()
		bStreamTriggered = FALSE
		
		SET_TV_VOLUME(0)
		
		STOP_AUDIO_SCENES()
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_DRUG_FLIGHT_END")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_MICHAEL_LONG_SCREAM")
		SET_AUDIO_SPECIAL_EFFECT_MODE(AUDIO_SPECIAL_EFFECT_MODE_NORMAL)
		
		SET_TIME_SCALE(1.0)
		CLEAR_TIMECYCLE_MODIFIER()
		ANIMPOSTFX_STOP("DMT_flight")
		ANIMPOSTFX_STOP("DrugsDrivingIn")
		ANIMPOSTFX_STOP("DrugsDrivingOut")
		
		STOP_MISSION_SFX(MISSION_SFX_TAP)
		STOP_MISSION_SFX(MISSION_SFX_VOMIT)
		STOP_MISSION_SFX(MISSION_SFX_DRINK)
		STOP_MISSION_SFX(MISSION_SFX_CHIMPS)
		STOP_MISSION_SFX(MISSION_SFX_ALIENS)
		STOP_MISSION_SFX(MISSION_SFX_HEARTBEAT)
		
		RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX_TAP)
		RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX_VOMIT)
		RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX_ALIENS)
		RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX_CHIMPS)
		RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX_HEARTBEAT)
		
		RESET_ALL_SFX_FLAGS(ssSounds)
		
		//stop all music
		IF ( bYogaMusicPlaying = TRUE )
			TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_ENDS")
			bYogaMusicPlaying = FALSE
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event FAM5_YOGA_MUSIC_ENDS.")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping yoga music.")
			#ENDIF
		ENDIF
		CANCEL_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cancelling music event FAM5_YOGA_MOVE_START.")
		#ENDIF
		
		IF DOES_CAM_EXIST(ScriptedCamera)
			SET_CAM_ACTIVE(ScriptedCamera, FALSE)
			DESTROY_CAM(ScriptedCamera)
		ENDIF
		
		IF DOES_CAM_EXIST(UndergroundCamera)
			SET_CAM_ACTIVE(UndergroundCamera, FALSE)
			DESTROY_CAM(UndergroundCamera)
		ENDIF
		
		IF DOES_CAM_EXIST(AnimatedCamera)
			SET_CAM_ACTIVE(AnimatedCamera, FALSE)
			DESTROY_CAM(AnimatedCamera)
		ENDIF
		
		DESTROY_ALL_CAMS()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		KILL_CHASE_HINT_CAM(sCinematicCamStruct)
		
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)

		CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
		
		CLEAR_TRIGGERED_LABELS()
		
		ASSISTED_MOVEMENT_REMOVE_ROUTE("mansion_1")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("family5b")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("family5c")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("family5d")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("f5_jimmy1")
		
		DISABLE_TAXI_HAILING(FALSE)
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxPuke)
			STOP_PARTICLE_FX_LOOPED(ptfxPuke)
		ENDIF
		
		REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedZone)
		
		RESET_YOGA_STRUCT(sYogaPlayer, TRUE, TRUE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_BS_CUP)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_BS_MAP_DOOR_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(P_BS_MAP_DOOR_01_S)
		
		REMOVE_VEHICLE_ASSET(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
		
		REMOVE_IPL("SpaceInterior")
		
		STAT_ENABLE_STATS_TRACKING()
		SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
		
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission stage cleanup.")
	
	ENDPROC
	
	PROC RUN_DEBUG_MISSION_STAGE_MENU(INT &iSelectedStage, MISSION_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenu, iSelectedStage, ENUM_TO_INT(eStage), FALSE, "FAMILY 5")
		
			DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
						
			DO_SAFE_STOP_CUTSCENE()
			
			CLEAR_PRINTS()
			
			CLEAR_HELP()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
			
			eStage = INT_TO_ENUM(MISSION_STAGES, iSelectedStage)
			
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via mission stage menu.")
			
			//reset the debug stage reset flag
			bResetStage	= FALSE
			
			bSkipActive = TRUE
			
			bBlackoutStagesActive = FALSE
			
			RESET_MISSION_FLAGS()
		
		ENDIF
	
	ENDPROC
	
	PROC RUN_DEBUG_SKIPS(MISSION_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		INT iCurrentStage = ENUM_TO_INT(eStage)
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
			IF eStage <> MISSION_STAGE_CUTSCENE_END_2
		
				DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				
				DO_SAFE_STOP_CUTSCENE()
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
				
				iCurrentStage++
				
				eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)
								
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via J skip.")
				
				//reset the debug stage reset flag
				bResetStage	= FALSE
				
				bSkipActive = TRUE
				
				bBlackoutStagesActive = FALSE
				
				RESET_MISSION_FLAGS()

			ENDIF
			
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		
			IF eStage <> MISSION_STAGE_CUTSCENE_INTRO
		
				DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				
				DO_SAFE_STOP_CUTSCENE()
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
				
				IF ( bResetStage = FALSE )
				
					iCurrentStage--
					
					eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)			
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via P skip.")
				
				ELIF ( bResetStage = TRUE )
				
					eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)			
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " reset via P skip.")
				
				ENDIF
				
				//reset the debug stage reset flag
				bResetStage	= FALSE
				
				bSkipActive = TRUE
				
				bBlackoutStagesActive = FALSE
				
				RESET_MISSION_FLAGS()
				
			ENDIF
			
		ENDIF
	
	ENDPROC
	
	PROC RUN_DEBUG_WIDGETS()
	
		IF ( bPositionMats = TRUE ) 
			
			IF DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
				IF NOT IS_ENTITY_DEAD(osYogaMats[0].ObjectIndex)
					SET_ENTITY_COORDS(osYogaMats[0].ObjectIndex, osYogaMats[0].vPosition)
					SET_ENTITY_ROTATION(osYogaMats[0].ObjectIndex, osYogaMats[0].vRotation)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
				IF NOT IS_ENTITY_DEAD(osYogaMats[1].ObjectIndex)
					SET_ENTITY_COORDS(osYogaMats[1].ObjectIndex, osYogaMats[1].vPosition)
					SET_ENTITY_ROTATION(osYogaMats[1].ObjectIndex, osYogaMats[1].vRotation)
				ENDIF
			ENDIF
			
//			IF DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
//				IF NOT IS_ENTITY_DEAD(osYogaMats[2].ObjectIndex)
//					SET_ENTITY_COORDS(osYogaMats[2].ObjectIndex, osYogaMats[2].vPosition)
//					SET_ENTITY_ROTATION(osYogaMats[2].ObjectIndex, osYogaMats[2].vRotation)
//				ENDIF
//			ENDIF

			IF DOES_ENTITY_EXIST(osGamepad.ObjectIndex)
				IF NOT IS_ENTITY_DEAD(osGamepad.ObjectIndex)
					SET_ENTITY_COORDS(osGamepad.ObjectIndex, osGamepad.vPosition)
					SET_ENTITY_ROTATION(osGamepad.ObjectIndex, osGamepad.vRotation)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(osHeadset.ObjectIndex)
				IF NOT IS_ENTITY_DEAD(osHeadset.ObjectIndex)
					SET_ENTITY_COORDS(osHeadset.ObjectIndex, osHeadset.vPosition)
					SET_ENTITY_ROTATION(osHeadset.ObjectIndex, osHeadset.vRotation)
				ENDIF
			ENDIF
			
		ENDIF
		
		IF ( bPositionCup = TRUE )
			
			IF DOES_ENTITY_EXIST(osCup.ObjectIndex)
				IF NOT IS_ENTITY_DEAD(osCup.ObjectIndex)
					SET_ENTITY_COORDS(osCup.ObjectIndex, osCup.vPosition)
					SET_ENTITY_ROTATION(osCup.ObjectIndex, osCup.vRotation)
				ENDIF
			ENDIF

			IF NOT IS_PED_INJURED(psJimmy.PedIndex)
			
				ATTACH_ENTITY_TO_ENTITY(osCup.ObjectIndex, psJimmy.PedIndex, 53, << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>) 
			
			ENDIF
		
		ENDIF
		
		IF ( bStartTimerToggle = TRUE )
			iKickOutDelayTimer = GET_GAME_TIMER()
			bStartTimerToggle = FALSE
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			IF ( bDrawDebugLinesAndSpheres = FALSE)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				bDrawDebugLinesAndSpheres = TRUE
			ELSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
				bDrawDebugLinesAndSpheres = FALSE
			ENDIF
		ENDIF
		
	ENDPROC
	
	FUNC STRING GET_PED_STATE_NAME_FOR_DEBUG(PED_STATES eState)
	
		SWITCH eState
		
			CASE PED_STATE_IDLE
				RETURN "PED_STATE_IDLE"
			BREAK
			CASE PED_STATE_MOVING_TO_LOCATION
				RETURN "PED_STATE_MOVING_TO_LOCATION"
			BREAK
			CASE PED_STATE_PLAYING_LOOP_ANIMATION
				RETURN "PED_STATE_PLAYING_LOOP_ANIMATION"
			BREAK
			CASE PED_STATE_PLAYING_ACTION_ANIMATION
				RETURN "PED_STATE_PLAYING_ACTION_ANIMATION"
			BREAK
			CASE PED_STATE_COMBAT
				RETURN "PED_STATE_COMBAT"
			BREAK
			CASE PED_STATE_FLEEING
				RETURN "PED_STATE_FLEEING"
			BREAK
			DEFAULT
				RETURN "INVALID_PED_STATE"
			BREAK
			
		ENDSWITCH
		
		RETURN "INVALID_PED_STATE"
	
	ENDFUNC
	
#ENDIF

//|===================== END DEBUG VARIABLES, PROCEDURES & FUNCTIONS =====================|

//|=============================== PROCEDURES AND FUNCTIONS ==============================|


PROC SETUP_MISSION_SFX()

	ssSounds[0].sSoundName					=	"DRUGS_ALIENS"
	ssSounds[1].sSoundName					=	"DRUGS_CHIMPS"
	ssSounds[2].sSoundName					=	"DRUGS_HEARTBEAT"
	ssSounds[3].sSoundName					=	"DRUGS_TAKE"
	ssSounds[4].sSoundName					=	"DRUGS_VOMIT"
	ssSounds[5].sSoundName					=	"TAP_LOOP"
	
	INT i = 0
	
	FOR i = 0 TO  COUNT_OF(ssSounds) - 1
	
		ssSounds[i].iSoundID = GET_SOUND_ID()
	
	ENDFOR

ENDPROC

PROC INITIALISE_ARRAYS_FOR_YOGA_MATS(MISSION_STAGES eStage)

	SWITCH eStage
	
		CASE MISSION_STAGE_TIMELAPSE
		CASE MISSION_STAGE_CUTSCENE_INTRO
		CASE MISSION_STAGE_YOGA_WARRIOR
		
			//Michael's yoga mat, blue
			osYogaMats[0].vPosition				= <<-791.1636, 186.4052, 71.8295>> 
			osYogaMats[0].vRotation				= <<-0.0000, 0.0000, -85.0403>> 
			osYogaMats[0].ModelName				= PROP_YOGA_MAT_01
			
			//Fabien's yoga mat, black
			osYogaMats[1].vPosition  			= <<-789.3542, 187.8925, 71.8295>>
			osYogaMats[1].vRotation  			= <<-0.0000, 0.0000, -18.5437>>
			osYogaMats[1].ModelName 			= PROP_YOGA_MAT_02
			
			//Amanda's yoga mat, red
			osYogaMats[2].vPosition  			= <<-789.0070, 185.9859, 71.8295>>
			osYogaMats[2].vRotation  			= <<-0.0000, 0.0000, 31.6581>>
			osYogaMats[2].ModelName 			= PROP_YOGA_MAT_03

		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_TRIANGLE
		CASE MISSION_STAGE_YOGA_TRIANGLE
		
			//Michael's yoga mat, blue
			osYogaMats[0].vPosition				= <<-791.1520, 186.2719, 71.8295>>
			osYogaMats[0].vRotation				= <<-0.0000, -0.0000, -85.0403>>
			osYogaMats[0].ModelName				= PROP_YOGA_MAT_01
			
			//Fabien's yoga mat, black
			osYogaMats[1].vPosition  			= <<-789.3542, 187.8925, 71.8295>>
			osYogaMats[1].vRotation  			= <<-0.0000, 0.0000, -18.5437>>
			osYogaMats[1].ModelName 			= PROP_YOGA_MAT_02
			
			//Amanda's yoga mat, red
			osYogaMats[2].vPosition  			= <<-789.0070, 185.9859, 71.8295>>
			osYogaMats[2].vRotation  			= <<-0.0000, 0.0000, 31.6581>>
			osYogaMats[2].ModelName 			= PROP_YOGA_MAT_03
		
		BREAK
		
		DEFAULT
		
			//Michael's yoga mat, blue
			osYogaMats[0].vPosition				= << -791.530, 186.375, 71.835 >> 
			osYogaMats[0].vRotation				= << 0.0, 0.0, 0.0 >> 
			osYogaMats[0].ModelName				= PROP_YOGA_MAT_01
			
			//Fabien's yoga mat, black
			osYogaMats[1].vPosition  			= << -789.164, 188.236, 71.835 >>
			osYogaMats[1].vRotation  			= << 0.0, 0.0, -108.0 >>
			osYogaMats[1].ModelName 			= PROP_YOGA_MAT_02
			
			//Amanda's yoga mat, red
			osYogaMats[2].vPosition  			= << -788.613, 185.352, 71.835 >>
			osYogaMats[2].vRotation  			= << 0.0, 0.0, -238.91 >>
			osYogaMats[2].ModelName 			= PROP_YOGA_MAT_03
		
		BREAK
	
	ENDSWITCH

ENDPROC


PROC INITIALISE_ARRAYS_FOR_MISSION_STAGE(MISSION_STAGES eStage)

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Initialising arrays for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
	INITIALISE_ARRAYS_FOR_YOGA_MATS(eStage)
	
	osLeftDoorDummy.vPosition					= <<-816.7160, 179.0980, 72.8274>>
	osLeftDoorDummy.ModelName					= V_ILEV_MM_DOORM_L
	
	osRightDoorDummy.vPosition					= <<-816.1068, 177.5108, 72.8274>>
	osRightDoorDummy.ModelName					= V_ILEV_MM_DOORM_R
	
	osBSDoor.vPosition							= << -1179.2917, -891.4589, 13.8687 >>
	osBSDoor.ModelName							= PROP_BS_MAP_DOOR_01
	
	osBSDoorDummy.vPosition						= << -1179.29, -891.459, 13.8687 >>
	osBSDoorDummy.vRotation						= << 0.0, 0.0, 124.1 >>
	osBSDoorDummy.ModelName						= P_BS_MAP_DOOR_01_S

	SWITCH eStage
	
		CASE MISSION_STAGE_PRE_TIMELAPSE
		
			MissionPosition.vStageStart 		= <<-823.9692, 179.8976, 70.5228>>
			MissionPosition.fStartHeading  		= 260.0065
					
		BREAK

		CASE MISSION_STAGE_TIMELAPSE
		CASE MISSION_STAGE_CUTSCENE_INTRO
		
			MissionPosition.vStageStart 		= <<-805.9919, 170.9620, 71.8447>>
			MissionPosition.fStartHeading  		= 293.2728
			
			psAmanda.vPosition					= << -789.032, 185.876, 71.8353 >>
			psAmanda.fHeading     				= 26.707853
			psAmanda.ModelName    				= GET_NPC_PED_MODEL(CHAR_AMANDA)
			
			psFabien.vPosition    				= << -789.419, 187.649, 71.8353 >>
			psFabien.fHeading     				= 141.305695
			psFabien.ModelName 	  				= IG_FABIEN

		BREAK
		
		CASE MISSION_STAGE_YOGA_WARRIOR
		CASE MISSION_STAGE_CUTSCENE_TRIANGLE
		CASE MISSION_STAGE_YOGA_TRIANGLE
		CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
		CASE MISSION_STAGE_YOGA_SUNSALUTATION
		CASE MISSION_STAGE_CUTSCENE_POOL
		
			MissionPosition.vStageStart 		= << -790.906, 186.293, 71.8351 >>
			MissionPosition.fStartHeading  		= 275.074219
			
			psAmanda.vPosition					= << -789.032, 185.876, 71.8353 >>
			psAmanda.fHeading     				= 26.707853
			psAmanda.ModelName    				= GET_NPC_PED_MODEL(CHAR_AMANDA)
			
			psFabien.vPosition    				= << -789.419, 187.649, 71.8353 >>
			psFabien.fHeading     				= 141.305695
			psFabien.ModelName 	  				= IG_FABIEN
			
		BREAK
		
		CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
		
			MissionPosition.vStageStart 		= << -789.995, 181.611, 71.8350 >>
			MissionPosition.fStartHeading  		= 92.551315
		
			psJimmy.vPosition	  	 			= << -805.7374, 168.7947, 75.7409 >>
			psJimmy.fHeading  	 				= 132
			psJimmy.ModelName 	  				= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			vsMichaelsCar.vPosition   			= << -824.3593, 179.5045, 70.4652 >>    
			vsMichaelsCar.fHeading  			= 137.1847
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			
			osGamepad.vPosition					= << -808.22, 168.81, 75.75 >>
			osGamepad.ModelName					= PROP_CONTROLLER_01
												 
			osHeadset.vPosition					= << -808.35, 169.36, 75.75 >>
			osHeadset.ModelName					= PROP_HEADSET_01
			
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
		
			MissionPosition.vStageStart 		= << -804.9048, 179.4079, 75.7408 >> 
			MissionPosition.fStartHeading  		= 185.1595
		
			psJimmy.vPosition 	  	  			= << -808.5767, 169.9767, 75.7406 >>
			psJimmy.fHeading  	  				= 89.3525
			psJimmy.ModelName 	  				= GET_NPC_PED_MODEL(CHAR_JIMMY)
				
			vsMichaelsCar.vPosition   			= << -824.3593, 179.5045, 70.4652 >>    
			vsMichaelsCar.fHeading  			= 137.1847
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			
			osGamepad.vPosition					= <<-806.184, 170.435, 76.470>>
			osGamepad.vRotation					= <<-64.000, 50.000, -92.000>>
			osGamepad.ModelName					= PROP_CONTROLLER_01
												 
			osHeadset.vPosition					= << -806.598, 170.280, 76.475 >>
			osHeadset.vRotation					= << -94.000, 0, 0.0>>
			osHeadset.ModelName					= PROP_HEADSET_01
					
		BREAK
		
		CASE MISSION_STAGE_GOTO_BURGER_SHOT
		
			MissionPosition.vStageStart 		= <<-807.1270, 172.5753, 75.7407>> 	//<< -806.0112, 174.1656, 75.7408 >>
			MissionPosition.fStartHeading  		= 324.5707							//321.7517
		
			vsMichaelsCar.vPosition 			= << -824.3593, 179.5045, 70.4652 >>   
			vsMichaelsCar.fHeading  			= 137.1847
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
		
			psJimmy.vPosition 					= <<-804.8173, 176.2879, 75.7408 >>
			psJimmy.fHeading					= 335.184906
			psJimmy.ModelName 					= GET_NPC_PED_MODEL(CHAR_JIMMY)

			psDealer.vPosition 					= << -1177.89, -892.36, 12.76 >> 
			psDealer.fHeading 					= 315.7144
			psDealer.ModelName 					= U_M_Y_Burgerdrug_01
			
			osGamepad.vPosition					= <<-806.184, 170.435, 76.470>>
			osGamepad.vRotation					= <<-64.000, 50.000, -92.000>>
			osGamepad.ModelName					= PROP_CONTROLLER_01
												 
			osHeadset.vPosition					= << -806.598, 170.280, 76.475 >>
			osHeadset.vRotation					= << -94.000, 0, 0.0>>
			osHeadset.ModelName					= PROP_HEADSET_01
			
			osCiggy.vPosition					= << -1176.78, -892.36, 12.93 >>
			osCiggy.ModelName					= PROP_CS_CIGGY_01
			
			osMethBag.vPosition					= << -1176.78, -892.36, 12.93 >>
			osMethBag.ModelName					= P_METH_BAG_01_S
					
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
		
			MissionPosition.vStageStart 		= << -1175.5334, -886.3669, 12.9041 >>
			MissionPosition.fStartHeading  		= 211.4735
		
			vsMichaelsCar.vPosition  			= << -1176.40, -887.60, 12.864 >>
			vsMichaelsCar.fHeading  			= 212.06
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
		
			psJimmy.vPosition	 				= << -1176.74, -887.948, 12.5514 >>
			psJimmy.fHeading	 				= 199.774918
			psJimmy.ModelName	 				= GET_NPC_PED_MODEL(CHAR_JIMMY)

			psDealer.vPosition 					= << -1177.89, -892.36, 12.76 >>
			psDealer.fHeading 					= 315.7144
			psDealer.ModelName 	 				= U_M_Y_Burgerdrug_01
			
			osCup.vPosition						= << -1180.36, -892.87, 12.80 >>
			osCup.vRotation						= << 0.0, 0.0, 0.0 >>
			osCup.vOffset						= << 0.0, 0.0, 0.0 >>
			osCup.ModelName						= PROP_CS_BS_CUP
						
		BREAK
		
		CASE MISSION_STAGE_DRIVE_HOME
		
			MissionPosition.vStageStart 		= << -1175.5334, -886.3669, 12.9041 >>
			MissionPosition.fStartHeading  		= 211.4735

			vsMichaelsCar.vPosition  			= << -1176.40, -887.60, 12.864 >>
			vsMichaelsCar.fHeading  			= 212.06
			vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)

			psJimmy.vPosition	 				= << -1176.74, -887.948, 12.5514 >>
			psJimmy.fHeading	 				= 199.774918
			psJimmy.ModelName	 				= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
			osCup.vPosition						= << -1176.571, -888.149, 13.6039>>
			osCup.vRotation						= << 0.0, 0.0, 0.0 >>
			osCup.vOffset						= << 0.0, -0.06, 0.03 >>
			osCup.ModelName						= PROP_CS_BS_CUP

		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_BLACKOUT
		
			MissionPosition.vStageStart 		= << -20.2621, -219.8899, 45.1815 >>
			MissionPosition.fStartHeading  		= 352.7035

		BREAK
		
		CASE MISSION_STAGE_FLIGHT
		
			MissionPosition.vStageStart 		= << -20.2621, -219.8899, 45.1815 >>
			MissionPosition.fStartHeading  		= 352.7035
			
		BREAK

		CASE MISSION_STAGE_GO_HOME_WASTED
		
			MissionPosition.vStageStart 		= << -966.9352, 309.2517, 69.2397 >>
			MissionPosition.fStartHeading  		= 252.8672
		
			vsParkedVehicles[0].vPosition 		= << -946.8423, 313.0, 70.34 >>   
			vsParkedVehicles[0].fHeading 		= 269.0366
			vsParkedVehicles[0].ModelName 		= SCORCHER
			
			osNote.vPosition					= <<-800.663, 184.017, 72.5250>>
			osNote.vRotation					= << 0.0, 0.0, 20.0 >>
			osNote.ModelName					= PROP_AMANDA_NOTE_01
			
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_END_1
		
			MissionPosition.vStageStart 		= <<-802.3392, 182.5324, 71.6055>>
			MissionPosition.fStartHeading  		= 293.7678
			
			osNote.vPosition					= <<-800.663, 184.017, 72.5250>>
			osNote.vRotation					= << 0.0, 0.0, 20.0 >>
			osNote.ModelName					= PROP_AMANDA_NOTE_01
		
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_END_2
		
			MissionPosition.vStageStart 		= <<-820.5433, 176.9832, 70.6124>>
			MissionPosition.fStartHeading  		= 44.9042
		
		BREAK
		
		#IF IS_DEBUG_BUILD
			CASE MISSION_STAGE_TEST
			
				MissionPosition.vStageStart 		= << -1175.5334, -886.3669, 12.9041 >>
				MissionPosition.fStartHeading  		= 211.4735

				vsMichaelsCar.vPosition  			= << -1176.40, -887.60, 12.864 >>
				vsMichaelsCar.fHeading  			= 212.06
				vsMichaelsCar.ModelName 			= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)

				psJimmy.vPosition	 				= << -1176.74, -887.948, 12.5514 >>
				psJimmy.fHeading	 				= 199.774918
				psJimmy.ModelName	 				= GET_NPC_PED_MODEL(CHAR_JIMMY)
			
				//INITIALISE_SAFE_LOCATES()
				
			BREAK
		#ENDIF
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initialising arrays for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

ENDPROC

/// PURPOSE:
///    Adds a model to model request array.
/// PARAMS:
///    ModelName - Model name being requested.
///    array - MODEL_NAMES array to add the requested model name to.
///    iArrayLength - Maximum length of the array.
///    iModelsAlreadyRequested - Number of models already requested in this array.
PROC ADD_MODEL_REQUEST_TO_ARRAY(MODEL_NAMES ModelName, MODEL_NAMES &array[], INT iArrayLength, INT &iModelsAlreadyRequested)

	INT i = 0
	
	BOOL bModelAlreadyRequested = FALSE
	
	IF ( iModelsAlreadyRequested >= 0 )
	
		IF ( iModelsAlreadyRequested <= ( iArrayLength - 1 ) )
		
			//loop through the array (from 0 to number of models already requested)
			//to see if the array already contains the model name being requested
			//modify the boolean flag if the model was found in array, otherwise leave the flag unchanged
			FOR i = 0 TO ( iModelsAlreadyRequested )
			
				IF ( array[i] = ModelName )
				
					bModelAlreadyRequested = TRUE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " is already in model array.")
					#ENDIF
					
				ENDIF
				
			ENDFOR
			
			IF ( bModelAlreadyRequested = FALSE )
			
				REQUEST_MODEL(ModelName)
				
				//iModelsAlreadyRequested will be the model array index of the model being requested
				array[iModelsAlreadyRequested] = ModelName
				
				iModelsAlreadyRequested++
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " and added to model array.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Number of models requested: ", iModelsAlreadyRequested, ".")
				#ENDIF
				
			ENDIF
		
		ELSE
		
			SCRIPT_ASSERT("Number of requested models is greater than the length of the model array.")
		
		ENDIF
	
	ELSE
	
		SCRIPT_ASSERT("Number of requested models is less than 0.")
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if all requested models from model array are loaded into memory.
/// PARAMS:
///    array - MODEL_NAMES array holding the models.
///    iModelsAlreadyRequested - Number of already requested models. Should be greated than 0, since 0 means no models have been requested.
/// RETURNS:
///    TRUE if all models from model array are loaded into memory. FALSE if otherwise.
FUNC BOOL ARE_REQUESTED_MODELS_LOADED(MODEL_NAMES &array[], INT &iModelsAlreadyRequested) 

	INT i = 0
	
	IF ( iModelsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iModelsAlreadyRequested - 1 )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model in array[", i, "]: ", GET_MODEL_NAME_FOR_DEBUG(array[i]), ".")
			#ENDIF
		
			IF NOT HAS_MODEL_LOADED(array[i])
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), " is loading.")
				#ENDIF
				
				//if the model is not loaded keep requesting it
				REQUEST_MODEL(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), ".")
				#ENDIF
				
				RETURN FALSE
				
			ENDIF
			
		ENDFOR
		
	ENDIF	
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Models from model array loaded. Number of models loaded: ", iModelsAlreadyRequested, ".")
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleans model array from requested models.
/// PARAMS:
///    array - MODEL_NAMES array.
///    iModelsAlreadyRequested - Number of already requested models. Should be greated than 0, since 0 means no models have been requested.
PROC CLEANUP_MODEL_ARRAY(MODEL_NAMES &array[], INT &iModelsAlreadyRequested)
	
	INT i = 0
	
	IF ( iModelsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iModelsAlreadyRequested - 1 )
		
			IF array[i] <> DUMMY_MODEL_FOR_SCRIPT
			
				SET_MODEL_AS_NO_LONGER_NEEDED(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), " cleaned up from model array by setting it as no longer needed.")
				#ENDIF
				
				array[i] = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
			
		ENDFOR
		
	ENDIF
	
	iModelsAlreadyRequested = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model array cleaned up. Number of requested models set to 0.")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if two strings are equal.
/// PARAMS:
///    sString1 - First string.
///    sString2 - Second string.
/// RETURNS:
///    TRUE if strings are equal, FALSE if otherwise or if any of the strings is empty or NULL.
FUNC BOOL IS_STRING_EQUAL_TO_STRING(STRING sString1, STRING sString2)

	IF NOT Is_String_Null_Or_Empty(sString1)
	AND NOT Is_String_Null_Or_Empty(sString2)

		RETURN ARE_STRINGS_EQUAL(sString1, sString2)

	ELSE
	
		RETURN FALSE
	
	ENDIF

ENDFUNC

/// PURPOSE:
///    Adds animation dictionary to animation request array.
/// PARAMS:
///    sAnimName - Name of the animation dictionary to request.
///    array - STRING array containing requested animations.
///    iArrayLength - Maximum length of the array
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
PROC ADD_ANIMATION_REQUEST_TO_ARRAY(STRING sAnimName, STRING &array[], INT iArrayLength, INT &iAnimationsAlreadyRequested)

	INT i = 0
	
	BOOL bAnimationRequested
	
	IF ( iAnimationsAlreadyRequested >= 0 )
	
		IF ( iAnimationsAlreadyRequested <= (iArrayLength - 1 ) )
	
		    FOR i = 0 TO ( iAnimationsAlreadyRequested )
			
				IF IS_STRING_EQUAL_TO_STRING(sAnimName, array[i])
				
				    bAnimationRequested = TRUE
					
				    #IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested animation ", sAnimName, " is already in animation array.")
					#ENDIF
					
				ENDIF
				
		    ENDFOR
			
		    IF ( bAnimationRequested = FALSE )
			
				REQUEST_ANIM_DICT(sAnimName)
			
				array[iAnimationsAlreadyRequested] = sAnimName

				iAnimationsAlreadyRequested++

				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested animation ", sAnimName, " and added to animation array.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Number of animations requested: ", iAnimationsAlreadyRequested, ".")
				#ENDIF
				  
		    ENDIF
		
		ELSE
		
			SCRIPT_ASSERT("Number of requested animations is greater than the length of the animation array.")
			
		ENDIF
	
	ELSE
	
		SCRIPT_ASSERT("Number of requested animations is less than 0.")
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if all requested animations are loaded into memory.
/// PARAMS:
///    array - STRING array containing requested animations.
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
/// RETURNS:
///    TRUE if all requested animations from animation array are loaded into memory. FALSE if otherwise.
FUNC BOOL ARE_REQUESTED_ANIMATIONS_LOADED(STRING &array[], INT &iAnimationsAlreadyRequested) 
	
	INT i = 0
	
	IF ( iAnimationsAlreadyRequested > 0 )
	
	    FOR i = 0 TO ( iAnimationsAlreadyRequested - 1 )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary in array[", i, "]: ", array[i], ".")
			#ENDIF
		
			IF NOT HAS_ANIM_DICT_LOADED(array[i])
			
			    #IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary ", array[i], " is loading.")
				#ENDIF
				
				//if the animation dictionary is not loaded keep requesting it
				REQUEST_ANIM_DICT(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting animation dictionary ", array[i], ".")
				#ENDIF
				
				
			    RETURN FALSE
				
			ENDIF
			
	    ENDFOR
		
	ENDIF   
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animations from animation array loaded. Number of animations loaded: ", iAnimationsAlreadyRequested, ".")
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleans animation array from requested animations.
/// PARAMS:
///    array - STRING array of animations to clean.
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
PROC CLEANUP_ANIMATION_ARRAY(STRING &array[], INT &iAnimationsAlreadyRequested)
	
	INT i = 0
	
	STRING sNull = NULL
	
	IF ( iAnimationsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iAnimationsAlreadyRequested - 1 )
		
			//brute force remove animation dictionary from memory
		
			REMOVE_ANIM_DICT(array[i])
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary ", array[i], " removed from memory and cleaned up from animation array.")
			#ENDIF
			
			array[i] = sNull

		ENDFOR
		
	ENDIF
	
	iAnimationsAlreadyRequested = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation array cleaned up. Number of requested animations set to 0.")
	#ENDIF
	
ENDPROC

PROC BUILD_REQUEST_BANK_FOR_MISSION_STAGE(MISSION_STAGES eStage)

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Building request bank for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

	SWITCH eStage

		CASE MISSION_STAGE_PRE_TIMELAPSE
		CASE MISSION_STAGE_TIMELAPSE
		CASE MISSION_STAGE_CUTSCENE_INTRO

		BREAK
		
		CASE MISSION_STAGE_YOGA_WARRIOR
		CASE MISSION_STAGE_CUTSCENE_TRIANGLE
		CASE MISSION_STAGE_YOGA_TRIANGLE
		CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
		CASE MISSION_STAGE_YOGA_SUNSALUTATION
		
			IF NOT DOES_ENTITY_EXIST(psAmanda.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psAmanda.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psFabien.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psFabien.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[0].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[1].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[2].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_ANIMATION_REQUEST_TO_ARRAY("missfam5_yoga", MissionAnimations, MAX_ANIMATIONS, iAnimationsRequested)
		BREAK
		CASE MISSION_STAGE_CUTSCENE_POOL
			IF NOT DOES_ENTITY_EXIST(psAmanda.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psAmanda.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psFabien.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psFabien.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[0].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[1].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[2].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_ANIMATION_REQUEST_TO_ARRAY("missfam5_wet_walk", MissionAnimations, MAX_ANIMATIONS, iAnimationsRequested)
		BREAK
		
		
		CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[0].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[1].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[2].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_ANIMATION_REQUEST_TO_ARRAY("missfam5_wet_walk", MissionAnimations, MAX_ANIMATIONS, iAnimationsRequested)
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[0].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[1].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[2].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_GOTO_BURGER_SHOT
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[0].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[1].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osYogaMats[2].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psDealer.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psDealer.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF		
		BREAK
		
		CASE MISSION_STAGE_DRIVE_HOME
			IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osCup.ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osCup.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_ANIMATION_REQUEST_TO_ARRAY("missfam5_drink", MissionAnimations, MAX_ANIMATIONS, iAnimationsRequested)
		BREAK
		
		CASE MISSION_STAGE_GO_HOME_WASTED
			IF NOT DOES_ENTITY_EXIST(vsParkedVehicles[0].VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsParkedVehicles[0].ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF			
			ADD_ANIMATION_REQUEST_TO_ARRAY("missfam5_blackout", MissionAnimations, MAX_ANIMATIONS, iAnimationsRequested)
		BREAK
				
		#IF IS_DEBUG_BUILD
			CASE MISSION_STAGE_TEST
				IF NOT DOES_ENTITY_EXIST(psJimmy.PedIndex)
					ADD_MODEL_REQUEST_TO_ARRAY(psJimmy.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
				ENDIF
			BREAK
		#ENDIF
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished building request bank for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets all fail flags to specified boolean value.
/// PARAMS:
///    bNewBool - Boolean value to set all flags to TRUE or FALSE.
PROC SET_FAIL_FLAGS(BOOL bNewBool)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(FailFlags) - 1 )

		FailFlags[i] = bNewBool

	ENDFOR

ENDPROC

/// PURPOSE:
///    Sets mission stage enum for replay.
/// PARAMS:
///    eStage - Mission stage enum variable.
///    iValue - Mid-mission replay value returned by the script when replaying the mission.
PROC SET_MISSION_STAGE_FOR_REPLAY(MISSION_STAGES &eStage, INT iValue)

	IF iValue = ENUM_TO_INT(MID_MISSION_STAGE_YOGA_WARRIOR)
	
		eStage = MISSION_STAGE_YOGA_WARRIOR
		
		INT iStartTime, iEndTime										//get TOD skip start and end time
		
		GET_SP_MISSION_TOD_WINDOW_TIME(SP_MISSION_FAMILY_5, iStartTime, iEndTime)	//these are times ranges for which TOD skip will trigger
																		
		IF NOT IS_TIME_BETWEEN_THESE_HOURS(iEndTime, iStartTime)		//on a replay, check if time of day is between the allowed time ranges
			SET_CLOCK_TIME(IDEAL_MISSION_START_TIME, 0, 0)										//set the time to new mission time if needed
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting clock time for mission replay.")
			#ENDIF
		ENDIF
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_YOGA_TRIANGLE)
	
		eStage = MISSION_STAGE_YOGA_TRIANGLE
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_YOGA_SUNSALUTATION)
	
		eStage = MISSION_STAGE_YOGA_SUNSALUTATION
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GOTO_JIMMYS_ROOM)
	
		eStage = MISSION_STAGE_GOTO_JIMMYS_ROOM
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GOTO_BURGER_SHOT)
	
		eStage = MISSION_STAGE_GOTO_BURGER_SHOT
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_HOME)
	
		eStage = MISSION_STAGE_DRIVE_HOME
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GO_HOME_WASTED)
	
		eStage = MISSION_STAGE_GO_HOME_WASTED
		
	ENDIF
	
	bStageReplayInProgress = TRUE
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission is being replayed at stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

ENDPROC

/// PURPOSE:
///    Warps specified ped to new coordinates and sets specified ped's new heading.
/// PARAMS:
///    ped - PED_INDEX to warp.
///    vNewPosition - VECTOR coordinates specifying new ped's position.
///    fNewHeading - FLOAT specifying new ped's heading.
///    bKeepVehicle - Specifies if ped should keep vehicle they are currently in.
///    bResetGameplayCamera - Specifies if gameplay camera should be positioned behind player's back. Works only for PLAYER_PED_ID().
///    bLoadScene - Specifies if scene at new provided coordinates should be loaded. Works only for PLAYER_PED_ID().
PROC WARP_PED(PED_INDEX ped, VECTOR vNewPosition, FLOAT fNewHeading, BOOL bKeepVehicle, BOOL bResetGameplayCamera, BOOL bLoadScene)

	IF NOT IS_PED_INJURED(ped)
	
		IF ( bKeepVehicle = TRUE )
			SET_PED_COORDS_KEEP_VEHICLE(ped, vNewPosition)
		ELIF ( bKeepVehicle = FALSE ) 
			SET_ENTITY_COORDS(ped, vNewPosition)
		ENDIF
		
		SET_ENTITY_HEADING(ped, fNewHeading)
		
		IF ( ped = PLAYER_PED_ID())
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
			ENDIF
		
			IF ( bResetGameplayCamera = TRUE)
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
			ENDIF
			
			IF ( bLoadScene = TRUE )
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", vNewPosition, ". This might take a while to load.")
				#ENDIF
			
				LOAD_SCENE(vNewPosition)
				
			ENDIF
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
		
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAKE_PED_LOOK_AT_ENTITY_WITH_INTERVAL(PED_INDEX PedIndex, ENTITY_INDEX EntityIndex, BOOL &bLookAtFlag, INT &iLookAtTimer,
										   INT &iIntervalTimer, INT &iLookAtTime, INT &iIntervalTime, 
										   INT iMinLookAtTime, INT iMaxLookAtTime, INT iMinIntervalTimer, INT iMaxIntervalTime)

	IF DOES_ENTITY_EXIST(PedIndex)
	
		IF NOT IS_ENTITY_DEAD(PedIndex)
		
			IF ( bLookAtFlag = FALSE )
		
				//reset interval time and interval timer
				IF ( iIntervalTime = 0 )
				
					iIntervalTime = GET_RANDOM_INT_IN_RANGE(iMinIntervalTimer, iMaxIntervalTime)
				
					iIntervalTimer = GET_GAME_TIMER()
				
				ENDIF
		
				IF HAS_TIME_PASSED(iIntervalTime, iIntervalTimer)
		
					IF DOES_ENTITY_EXIST(EntityIndex)
					
						SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
						
							CASE 0
								TASK_LOOK_AT_ENTITY(PedIndex, EntityIndex, -1, SLF_DEFAULT)
							BREAK
							
							CASE 1
								TASK_LOOK_AT_ENTITY(PedIndex, EntityIndex, -1, SLF_SLOW_TURN_RATE)
							BREAK
							
							CASE 2
								TASK_LOOK_AT_ENTITY(PedIndex, EntityIndex, -1, SLF_FAST_TURN_RATE)
							BREAK
						
						ENDSWITCH
					
						iLookAtTime = GET_RANDOM_INT_IN_RANGE(iMinLookAtTime, iMaxLookAtTime)
					
						iLookAtTimer = GET_GAME_TIMER()
						
						bLookAtFlag = TRUE
						
//						#IF IS_DEBUG_BUILD
//							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Giving ped look at entity task with time ", iLookAtTime, ".")
//						#ENDIF
					
					ENDIF
					
				ENDIF
				
			ELIF ( bLookAtFlag = TRUE )
			
				//reset look at time and look at timer
				IF ( iLookAtTime = 0 )
				
					iLookAtTime = GET_RANDOM_INT_IN_RANGE(iMinLookAtTime, iMaxLookAtTime)
					
					iLookAtTimer = GET_GAME_TIMER()
				
				ENDIF
			
				IF HAS_TIME_PASSED(iLookAtTime, iLookAtTimer)
			
					TASK_CLEAR_LOOK_AT(PedIndex)
					
					iIntervalTime = GET_RANDOM_INT_IN_RANGE(iMinIntervalTimer, iMaxIntervalTime)
					
					iIntervalTimer = GET_GAME_TIMER()
						
					bLookAtFlag = FALSE
			
//					#IF IS_DEBUG_BUILD
//						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Clearing ped look at entity task. Resetting interval time to ", iIntervalTime, ".")
//					#ENDIF
			
				ENDIF
				
			ENDIF

		ENDIF
	
	ENDIF

ENDPROC

PROC SET_MISSION_PED_PROPERTIES(PED_INDEX PedIndex, REL_GROUP_HASH relGroupHash, BOOL bCanFlyThroughWindscreen, BOOL bKeepRelGroupOnCleanup,
								BOOL bCanBeTargetted, BOOL bIsEnemy)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_PED_INJURED(PedIndex)
		
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_WillFlyThroughWindscreen, bCanFlyThroughWindscreen)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, bKeepRelGroupOnCleanup)
			SET_PED_CAN_BE_TARGETTED(PedIndex, bCanBeTargetted)
			SET_PED_AS_ENEMY(PedIndex, bIsEnemy)
			SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, relGroupHash)
		
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates a ped or player ped to be used by player and returns TRUE if such pad was created successfully.
/// PARAMS:
///    psPed - PED_STRUCT containg ped details, like PED_INDEX, blip, coordinates, heading, model etc.
///    bPlayerPed - Boolean indicating if a player ped or NPC ped should be created.
///    relGroupHash - Relationship group hash for the created ped.
///    bCreateBlip - Boolean indicating if a blip for this ped should be created.
///    eCharacter - Enum specifying character from story characters list. Use NO_CHARACTER for characters not stored in that list and provide a MODEL_NAMES in PED_STRUCT.
///    bCanFlyThroughWindscreen - Sets if ped can fly through windscreen when car crashes.
///    bCanBeTargetted - Sets if ped can be targetted by player.
///    bIsEnemy - Sets if ped is considered an enemy.
///    VehicleIndex - Specify a vehicle index if ped should be created inside a vehicle. IF not use NULL.
///    eVehicleSeat - Vehicle seat enum for peds created in vehicles.
/// RETURNS:
///    TRUE if ped was created successfully, FALSE if otherwise.
FUNC BOOL HAS_MISSION_PED_BEEN_CREATED(PED_STRUCT &psPed, BOOL bPlayerPed, REL_GROUP_HASH relGroupHash, BOOL bCreateBlip, enumCharacterList eCharacter,
									   BOOL bCanFlyThroughWindscreen = FALSE, BOOL bCanBeTargetted = FALSE, BOOL bIsEnemy = FALSE, VEHICLE_INDEX VehicleIndex = NULL,
									   VEHICLE_SEAT eVehicleSeat = VS_DRIVER, BOOL bKeepRelGroupOnCleanup = TRUE #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	IF ( bPlayerPed = FALSE )	//create non player ped
		
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
		
			REQUEST_MODEL(psPed.ModelName)
			
			IF HAS_MODEL_LOADED(psPed.ModelName)
		
				IF ( VehicleIndex = NULL )	//create ped outside of vehicle
				
					IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT if no character was specified
					
						psPed.PedIndex = CREATE_PED(PEDTYPE_MISSION, psPed.ModelName, psPed.vPosition, psPed.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
					
					ELSE								//use character name to define the ped MODEL_NAMES
					
						IF CREATE_NPC_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
						ENDIF
					
					ENDIF
					
					IF NOT IS_PED_INJURED(psPed.PedIndex)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
												   
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
					
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF
					#ENDIF
					
				ELIF ( VehicleIndex != NULL )	//create ped in a vehicle
				
					IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
						IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT
					
							psPed.PedIndex = CREATE_PED_INSIDE_VEHICLE(VehicleIndex, PEDTYPE_MISSION, psPed.ModelName, eVehicleSeat)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							
						ELSE								//use character name to define the ped MODEL_NAMES
						
							IF CREATE_NPC_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat)
								SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							ENDIF
						
						ENDIF
						
						IF NOT IS_PED_INJURED(psPed.PedIndex)
						
							SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
													   bCanBeTargetted, bIsEnemy)
							
							IF ( bCreateBlip = TRUE )
								psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
							ENDIF
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ENDIF
						#ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ELSE
		
			//if the ped already exists return true
			RETURN TRUE
		
		ENDIF
		
		
	ELIF ( bPlayerPed = TRUE )	//create player ped, for example a ped that player can hotswap to and take control of
	
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
	
			IF ( VehicleIndex = NULL )	//create player ped outside of vehicle
	
				IF CREATE_PLAYER_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading, TRUE)
				
					SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
					
					IF ( bCreateBlip = TRUE )
						psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF	
					#ENDIF
					
					RETURN TRUE
					
				ENDIF
				
			ELIF ( VehicleIndex != NULL )	//create player ped in a vehicle
			
				IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
					IF CREATE_PLAYER_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat, TRUE)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
						
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
						
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped  in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ENDIF
						#ENDIF
						
						RETURN TRUE
					
					ENDIF
				
				ENDIF
			
			ENDIF
		
		ELSE
		
			//if the ped already exists, return true
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Creates vehicle to be used on a mission by player and returns TRUE if such vehicle was created successfully.
/// PARAMS:
///    vsVehicle - VEH_STRUCT containing model, position and heading of the vehicle.
///    bPlayerVehicle - If set to TRUE will create player specific vehicles using CREATE_PLAYER_VEHICLE()
///    bCarStealVehicle - If set to TRUE will create car steal strand vehicle with a specific command. Works when bPlayerVehicle is FALSE.
///    eCharacter - Character enum indicating which character specific vehicle to create.
///    bMissionCritical - Sets the vehicle to be unable to leak oil/petrol and break off doors. Set to TRUE to stop these damage types to vehicle.
///    iColourCombination - Colour combination of the vehicle.
///    iColour1 - Colour 1 of the vehicle.
///    iColour2 - Colour 2 of the vehicle.
/// RETURNS:
///    TRUE if vehicle was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_VEHICLE_BEEN_CREATED(VEH_STRUCT &vsVehicle, BOOL bPlayerVehicle = FALSE, BOOL bCarStealVehicle = FALSE, enumCharacterList eCharacter = CHAR_MICHAEL,
										   BOOL bMissionCritical = TRUE, INT iColourCombination = -1, INT iColour1 = -1, INT iColour2 = -1
										   #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	//create any vehicle that can be used on a mission by player
	IF ( bPlayerVehicle = FALSE )
	
		IF NOT DOES_ENTITY_EXIST(vsVehicle.VehicleIndex)
		
			REQUEST_MODEL(vsVehicle.ModelName)
			
			IF HAS_MODEL_LOADED(vsVehicle.ModelName)

				IF( bCarStealVehicle = TRUE )
				
					vsVehicle.VehicleIndex = CREATE_CAR_STEAL_STRAND_CAR(vsVehicle.ModelName, vsVehicle.vPosition, vsVehicle.fHeading)
					SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
					
				ELSE
				
					IF ( eCharacter = NO_CHARACTER )
					
						vsVehicle.VehicleIndex = CREATE_VEHICLE(vsVehicle.ModelName, vsVehicle.vPosition, vsVehicle.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
					
					ELSE								//create npc vehicle based on the character enum specified
					
						IF CREATE_NPC_VEHICLE(vsVehicle.VehicleIndex, eCharacter, vsVehicle.vPosition, vsVehicle.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
						ENDIF
					
					ENDIF
					
				ENDIF
				
				
				
				IF IS_VEHICLE_DRIVEABLE(vsVehicle.VehicleIndex)
				
					//set vehicle colours, if they are provided
					IF ( iColour1 != -1)
					AND ( iColour2 != -1 )
						SET_VEHICLE_COLOURS(vsVehicle.VehicleIndex, iColour1, iColour2)
					ENDIF
					
					//set vehicle colour combination, if it is provided
					IF ( iColourCombination != -1 )
						SET_VEHICLE_COLOUR_COMBINATION(vsVehicle.VehicleIndex, iColourCombination)
					ENDIF
					
					SET_VEHICLE_HAS_STRONG_AXLES(vsVehicle.VehicleIndex, bMissionCritical)
					
					//limit damage that can be done to the vehicle
					SET_VEHICLE_CAN_LEAK_OIL(vsVehicle.VehicleIndex, NOT bMissionCritical)
					SET_VEHICLE_CAN_LEAK_PETROL(vsVehicle.VehicleIndex, NOT bMissionCritical)
					
					//make the vehicle not attach to tow truck if mission critical
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsVehicle.VehicleIndex, NOT bMissionCritical)
					
					IF IS_THIS_MODEL_A_CAR(vsVehicle.ModelName)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
					ENDIF
					
					SET_VEHICLE_ON_GROUND_PROPERLY(vsVehicle.VehicleIndex)
					
				ENDIF

				#IF IS_DEBUG_BUILD
					IF NOT Is_String_Null_Or_Empty(sDebugName)
						SET_VEHICLE_NAME_DEBUG(vsVehicle.VehicleIndex, sDebugName)
						IF ( bCarStealVehicle = TRUE )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created car steal strand vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ENDIF
					ELSE		
						IF ( bCarStealVehicle = TRUE )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created car steal strand vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ENDIF
					ENDIF
				#ENDIF
				
				RETURN TRUE
			
			ENDIF
		
		ELSE	//if the vehicle exists, return true
		
			RETURN TRUE
			
		ENDIF
		
	//create player specific vehicle
	ELIF ( bPlayerVehicle = TRUE )
	
		IF NOT DOES_ENTITY_EXIST(vsVehicle.VehicleIndex)
	
			//call the player vehicle creation until it returns true
			IF CREATE_PLAYER_VEHICLE(vsVehicle.VehicleIndex, eCharacter, vsVehicle.vPosition, vsVehicle.fHeading, TRUE)
			
				SET_VEHICLE_HAS_STRONG_AXLES(vsVehicle.VehicleIndex, bMissionCritical)
			
				//limit damage that can be done to the vehicle
				SET_VEHICLE_CAN_LEAK_OIL(vsVehicle.VehicleIndex, NOT bMissionCritical)
				SET_VEHICLE_CAN_LEAK_PETROL(vsVehicle.VehicleIndex, NOT bMissionCritical)
				
				//make the vehicle not attach to tow truck if mission critical
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsVehicle.VehicleIndex, NOT bMissionCritical)
				
				IF IS_THIS_MODEL_A_CAR(vsVehicle.ModelName)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
				ENDIF
			
				#IF IS_DEBUG_BUILD
					IF NOT Is_String_Null_Or_Empty(sDebugName)
						SET_VEHICLE_NAME_DEBUG(vsVehicle.VehicleIndex, sDebugName)
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
					ELSE
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
					ENDIF	
				#ENDIF
				
				RETURN TRUE
			
			ENDIF
		
		ELSE	//if the vehicle exists, return true
		
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Creates an object and returns TRUE if such object was created successfully.
/// PARAMS:
///    osObject - OBJECT_STRUCT containing model, position and rotation of the object.
///    bFreezeObject - Specify if this object's position should be frozen.
/// RETURNS:
///    TRUE if object was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_OBJECT_BEEN_CREATED(OBJECT_STRUCT &osObject, BOOL bFreezeObject = FALSE)

	IF NOT DOES_ENTITY_EXIST(osObject.ObjectIndex)
		
		REQUEST_MODEL(osObject.ModelName)
		
		IF HAS_MODEL_LOADED(osObject.ModelName)
		
			osObject.ObjectIndex = CREATE_OBJECT(osObject.ModelName, osObject.vPosition)
			
			SET_ENTITY_COORDS_NO_OFFSET(osObject.ObjectIndex, osObject.vPosition)
			SET_ENTITY_ROTATION(osObject.ObjectIndex, osObject.vRotation)
			
			SET_ENTITY_INVINCIBLE(osObject.ObjectIndex, TRUE)
			
			FREEZE_ENTITY_POSITION(osObject.ObjectIndex, bFreezeObject)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(osObject.ModelName)
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object with model ", GET_MODEL_NAME_FOR_DEBUG(osObject.ModelName), " at coordinates ", osObject.vPosition, ".")
			#ENDIF
		
		ENDIF
	
	ENDIF
	
	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)

		RETURN TRUE
	
	ENDIF

	RETURN FALSE

ENDFUNC

PROC SET_PLAYER_PED_OUTFIT(MISSION_OUTFITS eOutfit)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())

		SWITCH eOutfit
		
			CASE MO_MISSION_START
			
				RESTORE_MISSION_START_OUTFIT()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring player ped mission start outfit.")
				#ENDIF
				
			BREAK
		
			CASE MO_DEFAULT
			
				RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring default player ped variation.")
				#ENDIF
				
			BREAK
			
			CASE MO_YOGA
			
				SET_PLAYER_PED_VARIATIONS_FOR_YOGA(PLAYER_PED_ID())
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped variation to OUTFIT_P0_YOGA.")
				#ENDIF
				
			BREAK
			
			CASE MO_YOGA_FLIP_FLOPS
			
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA_FLIP_FLOPS, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped variation to OUTFIT_P0_YOGA_FLIP_FLOPS.")
				#ENDIF
			
			BREAK
			
			CASE MO_UNDERWEAR
			
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BARECHEST_BOXERS, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped variation to OUTFIT_P0_BARECHEST_BOXERS.")
				#ENDIF
			
			BREAK
			
			CASE MO_MISSION_END
			
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_LEATHER_AND_JEANS, FALSE)
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped variation to OUTFIT_P0_LEATHER_AND_JEANS.")
				#ENDIF
			
			BREAK
		
		ENDSWITCH
		
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
	
	ENDIF

ENDPROC

PROC RUN_INTERIOR_UNPINNING_CHECK(VECTOR vInteriorCoords, FLOAT fRadius)
	
	IF NOT ARE_VECTORS_EQUAL(vInteriorCoords, << 0.0, 0.0, 0.0 >>)		//don't check the interior if the coordinates are not valid

		IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorCoords))	//unpin the interior if it is pinned and player is away from it

			IF ( GET_DISTANCE_BETWEEN_COORDS(vInteriorCoords, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE) > fRadius )
		
				UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorCoords))
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unpinning interior at coordinates ", vInteriorCoords, " in memory.")
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_LOADED(MISSION_STAGES eStage, INT &iSetupProgress, BOOL &bStageLoaded, BOOL &bStageSkippedTo, BOOL &bStageReplayed)

	//handle initial mission setup
	IF ( iSetupProgress = 0 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting initial mission loading.")
		#ENDIF
		
		//clear the screen of any text displayed before mission started
		CLEAR_PRINTS()

		//load the mission text
		REQUEST_ADDITIONAL_TEXT("FAM5", MISSION_TEXT_SLOT)
		
		REQUEST_ADDITIONAL_TEXT("FAM5AUD", MISSION_DIALOGUE_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
		
		IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		ENDIF
		
		REGISTER_SCRIPT_WITH_AUDIO(TRUE)
		
		SETUP_MISSION_SFX()
		
		//suppress player's car model
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
		
		//suppress burger shot drug dealer ped model
		SET_PED_MODEL_IS_SUPPRESSED(U_M_Y_BURGERDRUG_01, TRUE)
				
		DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
		IF IS_SCREEN_FADED_OUT()
			DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
			DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
		ENDIF

		SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), FAM5_DAMAGE)
		
		//get information about vehicle gen at Michael's house
		GET_VEHICLE_GEN_DATA(sVehicleGenDataStruct, VEHGEN_MICHAEL_SAVEHOUSE)

		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": sVehicleGenDataStruct.coords: ", sVehicleGenDataStruct.coords, ".")
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": sVehicleGenDataStruct.heading: ", sVehicleGenDataStruct.heading, ".")
		#ENDIF
		
		SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", FALSE)
		
		IF ( BurgerShotScenarioBlockingIndex = NULL )
			BurgerShotScenarioBlockingIndex = CREATE_SCENARIO_BLOCKING_AREA(<< -1181.24, -884.43, 12.80 >>, << 2.0, 2.0, 2.0 >>)
			CLEAR_AREA_OF_PEDS(<< -1181.24, -884.43, 12.80 >>, 2.0)
		ENDIF
		
		IF ( ParkScenarioBlockingIndex = NULL )
			ParkScenarioBlockingIndex = CREATE_SCENARIO_BLOCKING_AREA(<< -964.29, 307.89, 70.39 >>, << 16.0, 6.0, 3.0 >>)
			CLEAR_AREA_OF_PEDS(<< -964.29, 307.89, 70.39 >>, 10.0)
		ENDIF

		#IF IS_DEBUG_BUILD
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initial mission loading.")
		#ENDIF
				
		iSetupProgress++
	
	ENDIF
	
	//run this each time new mission stage needs to be loaded
	IF ( iSetupProgress = 1 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Started mission stage loading for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded = FALSE
		
		//initialise arrays for peds, vehicles and objects
		INITIALISE_ARRAYS_FOR_MISSION_STAGE(eStage)
		
		//cleanup the asset arrays so that they are ready to be populated with assets needed for the stage being loaded
		CLEANUP_MODEL_ARRAY(MissionModels, iModelsRequested)
		CLEANUP_ANIMATION_ARRAY(MissionAnimations, iAnimationsRequested)
		
		//clear triggered text labels
		CLEAR_TRIGGERED_LABELS()

		iSetupProgress++
	
	ENDIF
	
	//handle player positioning when current stage is being skipped to or played normally
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 2 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to new position for mission stage being loaded due to skipping or replay.")
			#ENDIF
		
			WARP_PED(PLAYER_PED_ID(), MissionPosition.vStageStart, MissionPosition.fStartHeading, FALSE, TRUE, FALSE)
			
			IF ( bStageSkippedTo = TRUE )
				//pin the interior at player start position for stages starting in interiors or requiring interiors
				IF ( eStage = MISSION_STAGE_CUTSCENE_INTRO )
				OR ( eStage = MISSION_STAGE_GOTO_JIMMYS_ROOM )
				OR ( eStage = MISSION_STAGE_CUTSCENE_JIMMYS_ROOM )
				OR ( eStage = MISSION_STAGE_GOTO_BURGER_SHOT )
				
					SWITCH eStage
						
						CASE MISSION_STAGE_CUTSCENE_INTRO
						CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
						CASE MISSION_STAGE_GOTO_BURGER_SHOT
						CASE MISSION_STAGE_CUTSCENE_END_1
							vInteriorPosition = MissionPosition.vStageStart
						BREAK
						
						CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
							vInteriorPosition = psJimmy.vPosition
						BREAK
						
					ENDSWITCH
				
					WHILE NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorPosition))
						
						PIN_INTERIOR_IN_MEMORY(GET_INTERIOR_AT_COORDS(vInteriorPosition))
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Pinning interior at coordinates ", vInteriorPosition, " in memory.")
						#ENDIF
						
						WAIT(0)
					
					ENDWHILE
				
				ENDIF
			ENDIF
			
			SWITCH eStage
				CASE MISSION_STAGE_PRE_TIMELAPSE
				CASE MISSION_STAGE_TIMELAPSE
				CASE MISSION_STAGE_CUTSCENE_INTRO
				CASE MISSION_STAGE_YOGA_WARRIOR
				CASE MISSION_STAGE_CUTSCENE_TRIANGLE
				CASE MISSION_STAGE_YOGA_TRIANGLE
				CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
				CASE MISSION_STAGE_YOGA_SUNSALUTATION
				CASE MISSION_STAGE_CUTSCENE_POOL
				CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
				CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
				CASE MISSION_STAGE_GOTO_BURGER_SHOT
					SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_NORMAL)				//unhide yoga props
				BREAK
			ENDSWITCH
						
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_TATTOOS(PLAYER_PED_ID())
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
				
				STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				REMOVE_DECALS_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
			ENDIF
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF

			bTimelapseCutsceneSkipped 		= FALSE
			bIntroCutsceneAssetsRequested	= FALSE

		ENDIF
		
		IF ( bStageReplayed = TRUE )
			START_REPLAY_SETUP( MissionPosition.vStageStart, MissionPosition.fStartHeading)
		ENDIF
		
		//request assets needed for current stage
		BUILD_REQUEST_BANK_FOR_MISSION_STAGE(eStage)
		
		iSetupProgress++
	
	ENDIF
	
	//handle loading of mission models that have been requested in the previous stage in BUILD_REQUEST_BANK_FOR_MISSION_STAGE()
	//handle loading of mission animations that been requested in the previous stage in BUILD_REQUEST_BANK_FOR_MISSION_STAGE()
	IF ( iSetupProgress = 3 )
	
		IF ARE_REQUESTED_MODELS_LOADED(MissionModels, iModelsRequested)
			IF ARE_REQUESTED_ANIMATIONS_LOADED(MissionAnimations, iAnimationsRequested)
				iSetupProgress++
			ENDIF
		ENDIF
	
	ENDIF
	
	
	//handle creating vehicles/peds and setting up fail flags
	//run this each time new mission stage needs to be loaded
	//THIS SETUP SECTION ASSUMES THAT ALL REQUIRED ASSETS HAVE BEEN ALREADY LOADED TO MEMORY IN PREVIOUS SETUP SECTIONS
	IF ( iSetupProgress = 4 )
	
		SET_FAIL_FLAGS(FALSE)
	
		SWITCH eStage

			CASE MISSION_STAGE_PRE_TIMELAPSE
			CASE MISSION_STAGE_TIMELAPSE
			CASE MISSION_STAGE_CUTSCENE_INTRO
			
				IF IS_REPEAT_PLAY_ACTIVE()	//on repeat play request the yoga outfit before intro cutscene and check if it is streamed in
				
					IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA)
						SET_PLAYER_PED_OUTFIT(MO_YOGA)
					ENDIF
					IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA)
					
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for all streaming requests to be completed for player ped OUTFIT_P0_YOGA.")
						#ENDIF
					
						IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": All streaming requests completed for player ped OUTFIT_P0_YOGA.")
							#ENDIF
						
							iSetupProgress++
						ENDIF
					ENDIF
					
				ELSE
				
					iSetupProgress++
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_YOGA_WARRIOR
			CASE MISSION_STAGE_CUTSCENE_TRIANGLE
			CASE MISSION_STAGE_YOGA_TRIANGLE
			CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
			CASE MISSION_STAGE_CUTSCENE_POOL
			
				IF HAS_MISSION_PED_BEEN_CREATED(psAmanda, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_AMANDA)
				
					FailFlags[MISSION_FAIL_AMANDA_DEAD] = TRUE
					
					IF HAS_MISSION_PED_BEEN_CREATED(psFabien, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
					
						FailFlags[MISSION_FAIL_FABIEN_DEAD] = TRUE
						FailFlags[MISSION_FAIL_YOGA_FAILED] = TRUE
						
						IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[0])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[1])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[2])
						
							IF ( bBuddiesYogaOutfitsSet = FALSE )
								IF NOT IS_PED_INJURED(psAmanda.PedIndex)
									SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_HAIR, 	4, 0, 0)
									SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_TORSO, 	1, 0, 0)
									SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_LEG, 	1, 0, 0)
									SET_PED_COMPONENT_VARIATION(psAmanda.PedIndex, PED_COMP_DECL, 	1, 0, 0)
								ENDIF
								IF NOT IS_PED_INJURED(psFabien.PedIndex)
									SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_HEAD, 	0, 0, 0)
									SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_TORSO, 	0, 0, 0)
									SET_PED_COMPONENT_VARIATION(psFabien.PedIndex, PED_COMP_LEG, 	0, 0, 0)
								ENDIF
								bBuddiesYogaOutfitsSet = TRUE
							ENDIF
						
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
							ENDIF
						
							IF NOT IS_PED_INJURED(psAmanda.PedIndex)
								SET_FORCE_FOOTSTEP_UPDATE(psAmanda.PedIndex, TRUE)
								SET_RAGDOLL_BLOCKING_FLAGS(psAmanda.PedIndex, RBF_PLAYER_BUMP)
								SET_RAGDOLL_BLOCKING_FLAGS(psAmanda.PedIndex, RBF_PLAYER_IMPACT)
							ENDIF
							
							IF NOT IS_PED_INJURED(psFabien.PedIndex)
								SET_FORCE_FOOTSTEP_UPDATE(psFabien.PedIndex, TRUE)
								SET_RAGDOLL_BLOCKING_FLAGS(psFabien.PedIndex, RBF_PLAYER_BUMP)
								SET_RAGDOLL_BLOCKING_FLAGS(psFabien.PedIndex, RBF_PLAYER_IMPACT)
							ENDIF
						
							iSetupProgress++
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
			
				IF HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				
					FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE

					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)

						FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= TRUE
						FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE] 	= TRUE
						
						IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[0])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[1])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[2])
						
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vsMichaelsCar.VehicleIndex)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vsMichaelsCar.VehicleIndex, FAM5_CAR_DAMAGE)
						
							iSetupProgress++
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
			
				IF HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)
				
					FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE
					
					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
					
						FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= TRUE
						FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE] 	= TRUE
						
						IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[0])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[1])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[2])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osGamepad)
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osHeadset)
						
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vsMichaelsCar.VehicleIndex)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vsMichaelsCar.VehicleIndex, FAM5_CAR_DAMAGE)
							
							iSetupProgress++
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GOTO_BURGER_SHOT
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
			
					FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE] 	= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND]	= TRUE
					
					IF HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY)

						FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
						FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE
						
						IF NOT IS_PED_INJURED(psJimmy.PedIndex)
							SET_ENTITY_LOD_DIST(psJimmy.PedIndex, 150)		
							SET_PED_LOD_MULTIPLIER(psJimmy.PedIndex, 5.0)
						ENDIF
						
						IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[0])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[1])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[2])
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osGamepad, TRUE)
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osHeadset, TRUE)
						
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vsMichaelsCar.VehicleIndex)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vsMichaelsCar.VehicleIndex, FAM5_CAR_DAMAGE)
						
							iSetupProgress++
							
						ENDIF
						
					ENDIF
					
				ENDIF

			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
			
					FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE] 	= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND]	= TRUE
					
					IF HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY, FALSE, FALSE, FALSE, vsMichaelsCar.VehicleIndex, VS_FRONT_RIGHT)
						
						FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
						FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE
						
						IF NOT IS_PED_INJURED(psJimmy.PedIndex)
							SET_ENTITY_LOD_DIST(psJimmy.PedIndex, 150)		
							SET_PED_LOD_MULTIPLIER(psJimmy.PedIndex, 5.0)
						ENDIF
						
						IF 	HAS_MISSION_PED_BEEN_CREATED(psDealer, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
						AND	HAS_MISSION_OBJECT_BEEN_CREATED(osBSDoorDummy)
						
							IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
								IF NOT IS_PED_INJURED(psDealer.PedIndex)
									SET_PED_PROP_INDEX(psDealer.PedIndex, ANCHOR_HEAD, 0, 0)
									SET_PED_PROP_INDEX(psDealer.PedIndex, ANCHOR_EYES, 0, 0)
								ENDIF
							ENDIF
						
							FailFlags[MISSION_FAIL_DEALER_DEAD] = TRUE
							FailFlags[MISSION_FAIL_DEALER_SCARED] = TRUE
							FailFlags[MISSION_FAIL_DEALER_ATTACKED] = TRUE
							
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vsMichaelsCar.VehicleIndex)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vsMichaelsCar.VehicleIndex, FAM5_CAR_DAMAGE)
							
							iSetupProgress++
							
						ENDIF
						
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_DRIVE_HOME
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
			
					FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE] 	= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND]	= TRUE
					
					IF HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY, FALSE, FALSE, FALSE, vsMichaelsCar.VehicleIndex, VS_FRONT_RIGHT)
						
						FailFlags[MISSION_FAIL_JIMMY_DEAD] 			= TRUE
						FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 	= TRUE
						
						IF NOT IS_PED_INJURED(psJimmy.PedIndex)
							SET_ENTITY_LOD_DIST(psJimmy.PedIndex, 150)		
							SET_PED_LOD_MULTIPLIER(psJimmy.PedIndex, 5.0)
						ENDIF

						IF HAS_MISSION_OBJECT_BEEN_CREATED(osCup)
				
							//attach the cup to Jimmy's hand
							IF NOT IS_PED_INJURED(psJimmy.PedIndex)
								ATTACH_ENTITY_TO_ENTITY(osCup.ObjectIndex, psJimmy.PedIndex, GET_PED_BONE_INDEX(psJimmy.PedIndex, BONETAG_PH_L_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
							ENDIF
							
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vsMichaelsCar.VehicleIndex)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vsMichaelsCar.VehicleIndex, FAM5_CAR_DAMAGE)
						
							iSetupProgress++
						
						ENDIF
						
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_GO_HOME_WASTED
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsParkedVehicles[0], FALSE, FALSE, NO_CHARACTER)
					iSetupProgress++
				ENDIF
			
			BREAK
			
			#IF IS_DEBUG_BUILD
				CASE MISSION_STAGE_TEST
				
					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)
						
						IF HAS_MISSION_PED_BEEN_CREATED(psJimmy, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_JIMMY, FALSE, FALSE, FALSE, vsMichaelsCar.VehicleIndex, VS_FRONT_RIGHT)
							
							iSetupProgress++
							
						ENDIF
						
					ENDIF
				
				BREAK
			#ENDIF
			
			DEFAULT
				iSetupProgress++	
			BREAK
		
		ENDSWITCH
		
		IF ( bJimmysOutfitSet = FALSE )
			IF DOES_ENTITY_EXIST(psJimmy.PedIndex)
				IF NOT IS_ENTITY_DEAD(psJimmy.PedIndex)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_HEAD, 		0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 		3, 0, 0) //(berd)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_HAIR, 		0, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_TORSO, 		6, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_LEG, 		0, 2, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_HAND, 		0, 0, 0) //(hand)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_FEET, 		0, 2, 0) //(feet)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_TEETH, 		0, 0, 0) //(teef)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_SPECIAL, 	0, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_SPECIAL2, 	0, 0, 0) //(task)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_DECL, 		3, 0, 0) //(decl)					
					bJimmysOutfitSet = TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	
	//handle player ped component variations and setup player's outfit, also handle shops, also handle yoga props IPL
	IF ( iSetupProgress = 5 )
	
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)	//unblock all shops on skips
	
		SWITCH eStage
		
			CASE MISSION_STAGE_PRE_TIMELAPSE
			CASE MISSION_STAGE_TIMELAPSE
			CASE MISSION_STAGE_CUTSCENE_INTRO
				IF ( g_eCurrentBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_DESTROYED )
					SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_NORMAL)				//unhide yoga props
				ENDIF
				IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_DESTROYED )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_DESTROYED )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_DESTROYED )
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_NORMAL)			//unhide amanda's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_NORMAL)			//unhide jimmy's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_NORMAL)	// Flowers are ok.
					SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_NORMAL)//hide burger shot stuff
				ENDIF
			BREAK

			CASE MISSION_STAGE_YOGA_WARRIOR
			CASE MISSION_STAGE_CUTSCENE_TRIANGLE
			CASE MISSION_STAGE_YOGA_TRIANGLE
			CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
			CASE MISSION_STAGE_CUTSCENE_POOL
			CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
			
				IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA)
					SET_PLAYER_PED_OUTFIT(MO_YOGA)
				ENDIF
				
				IF ( g_eCurrentBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_DESTROYED )
					SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_NORMAL)				//unhide yoga props
				ENDIF
				IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_DESTROYED )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_DESTROYED )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_DESTROYED )
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_NORMAL)				//unhide amanda's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_NORMAL)				//unhide jimmy's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_NORMAL)	// Flowers are ok.
					SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_NORMAL)	//hide burger shot stuff
					
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
			CASE MISSION_STAGE_GOTO_BURGER_SHOT
				IF ( g_eCurrentBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_DESTROYED )
					SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_NORMAL)				//unhide yoga props
				ENDIF
				IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_DESTROYED )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_DESTROYED )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_DESTROYED )
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_NORMAL)			//unhide amanda's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_NORMAL)			//unhide jimmy's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_NORMAL)// Flowers are ok.
					SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_NORMAL)//hide burger shot stuff
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
				IF ( g_eCurrentBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_DESTROYED)				//hide yoga props
				ENDIF
				IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_NORMAL )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_NORMAL )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_DESTROYED)		//hide amanda's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_DESTROYED)		//hide jimmy's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_DESTROYED) // Flowers are dead.
					SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_DESTROYED)//unhide burger shot stuff
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BLACKOUT
			CASE MISSION_STAGE_FLIGHT
			CASE MISSION_STAGE_GO_HOME_WASTED
			
				SWITCH eStage
					CASE MISSION_STAGE_FLIGHT
					CASE MISSION_STAGE_GO_HOME_WASTED
						//IF 	NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BED)
						IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BARECHEST_BOXERS)
							SET_PLAYER_PED_OUTFIT(MO_UNDERWEAR)
						ENDIF
					BREAK
				ENDSWITCH
				
				IF ( g_eCurrentBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_DESTROYED)				//hide yoga props
				ENDIF
				IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_NORMAL )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_NORMAL )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_DESTROYED)				//hide amanda's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_DESTROYED)				//hide jimmy's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_DESTROYED)	// Flowers are dead
					SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_DESTROYED)		//unhide burger shot stuff
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_END_1
			CASE MISSION_STAGE_CUTSCENE_END_2
				IF ( g_eCurrentBuildingState[BUILDINGNAME_IPL_YOGA_GAME] = BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_DESTROYED)				//hide yoga props
				ENDIF
				IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_NORMAL )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_NORMAL )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_DESTROYED)		//hide amanda's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_DESTROYED)			//hide jimmy's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_DESTROYED) // Flowers are dead.
					SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_DESTROYED)//unhide burger shot stuff
				ENDIF
			BREAK

		ENDSWITCH
		
		//cascade shadows setup
		SWITCH eStage
			CASE MISSION_STAGE_YOGA_WARRIOR
			CASE MISSION_STAGE_YOGA_TRIANGLE
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.35)
				CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(0.65)
			BREAK
			DEFAULT
				CASCADE_SHADOWS_INIT_SESSION()
			BREAK
		ENDSWITCH
		
		SWITCH eStage
			CASE MISSION_STAGE_CUTSCENE_INTRO
			CASE MISSION_STAGE_YOGA_WARRIOR
			CASE MISSION_STAGE_CUTSCENE_TRIANGLE
			CASE MISSION_STAGE_YOGA_TRIANGLE
			CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
			CASE MISSION_STAGE_CUTSCENE_POOL
				IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
				ENDIF
			BREAK
			CASE MISSION_STAGE_CUTSCENE_BLACKOUT
			CASE MISSION_STAGE_FLIGHT
				IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
					SET_WEATHER_TYPE_NOW_PERSIST("CLOUDS")
				ENDIF
			BREAK
			DEFAULT
				CLEAR_WEATHER_TYPE_PERSIST()
			BREAK
		ENDSWITCH
		
		//handle clothes loading
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
	
			IF ( eStage = MISSION_STAGE_GOTO_JIMMYS_ROOM )
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_WETNESS_HEIGHT(PLAYER_PED_ID(), 2.0)
				ENDIF
			ENDIF
	
			IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())

				iSetupProgress++					//move on when all streaming requests have completed
			
			ELSE
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for all streaming requests for player ped.")
				#ENDIF

			ENDIF
		
		ELSE
	
			iSetupProgress++
		
		ENDIF
	
	ENDIF
		
	//handle adding mission peds for dialogue depending on mission stage
	//handle cellphone enabling/disabling
	IF ( iSetupProgress = 6 )
		
		REMOVE_PED_FOR_DIALOGUE(Family5Conversation, 0)
		REMOVE_PED_FOR_DIALOGUE(Family5Conversation, 1)
		REMOVE_PED_FOR_DIALOGUE(Family5Conversation, 2)
		REMOVE_PED_FOR_DIALOGUE(Family5Conversation, 3)
		REMOVE_PED_FOR_DIALOGUE(Family5Conversation, 4)
		REMOVE_PED_FOR_DIALOGUE(Family5Conversation, 5)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(Family5Conversation, 0, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		
		SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE)
		
		SWITCH eStage
		
			CASE MISSION_STAGE_PRE_TIMELAPSE
				DISABLE_CELLPHONE(FALSE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
			BREAK
		
			CASE MISSION_STAGE_TIMELAPSE
			CASE MISSION_STAGE_CUTSCENE_INTRO
				DISABLE_CELLPHONE(TRUE)
				DISPLAY_RADAR(FALSE)
			BREAK
		
			CASE MISSION_STAGE_YOGA_WARRIOR
			CASE MISSION_STAGE_YOGA_TRIANGLE
			CASE MISSION_STAGE_YOGA_SUNSALUTATION

				DISABLE_CELLPHONE(TRUE)
			
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					ADD_PED_FOR_DIALOGUE(Family5Conversation, 1, psAmanda.PedIndex, "AMANDA")
				ENDIF
				
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					ADD_PED_FOR_DIALOGUE(Family5Conversation, 3, psFabien.PedIndex, "FABIAN")
				ENDIF
				
				DISPLAY_RADAR(FALSE)
				
				SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_TRIANGLE
			CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
			CASE MISSION_STAGE_CUTSCENE_POOL
				DISABLE_CELLPHONE(TRUE)
				DISPLAY_RADAR(FALSE)
			BREAK
			
			CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
				DISABLE_CELLPHONE(FALSE)
				
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					ADD_PED_FOR_DIALOGUE(Family5Conversation, 2, psJimmy.PedIndex, "JIMMY", FALSE, FALSE)
				ENDIF
				
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
				DISABLE_CELLPHONE(TRUE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
			BREAK
			
			CASE MISSION_STAGE_GOTO_BURGER_SHOT
			
				DISABLE_CELLPHONE(FALSE)
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					ADD_PED_FOR_DIALOGUE(Family5Conversation, 2, psJimmy.PedIndex, "JIMMY", FALSE, TRUE)
				ENDIF
				
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
				DISABLE_CELLPHONE(TRUE)
				
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					ADD_PED_FOR_DIALOGUE(Family5Conversation, 2, psJimmy.PedIndex, "JIMMY")
				ENDIF
				
				IF NOT IS_PED_INJURED(psDealer.PedIndex)
					ADD_PED_FOR_DIALOGUE(Family5Conversation, 5, psDealer.PedIndex, "FAM5DEALER")
				ENDIF
				
				
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
			BREAK
			
			CASE MISSION_STAGE_DRIVE_HOME
				
				DISABLE_CELLPHONE(FALSE)
				
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					ADD_PED_FOR_DIALOGUE(Family5Conversation, 2, psJimmy.PedIndex, "JIMMY")
				ENDIF
	
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BLACKOUT
			CASE MISSION_STAGE_FLIGHT
				DISABLE_CELLPHONE(TRUE)
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
			BREAK
			
			CASE MISSION_STAGE_GO_HOME_WASTED
				DISABLE_CELLPHONE(TRUE)
				DISPLAY_RADAR(FALSE)		//initially disabled, will be turned on after waking up
				DISPLAY_HUD(FALSE)
			BREAK
		
			CASE MISSION_STAGE_CUTSCENE_END_1
			CASE MISSION_STAGE_CUTSCENE_END_2
				DISABLE_CELLPHONE(TRUE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
			BREAK
			
			#IF IS_DEBUG_BUILD
				CASE MISSION_STAGE_TEST
					DISABLE_CELLPHONE(FALSE)
					DISPLAY_RADAR(TRUE)
					
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						ADD_PED_FOR_DIALOGUE(Family5Conversation, 2, psJimmy.PedIndex, "JIMMY")
					ENDIF
				BREAK
			#ENDIF
		
		ENDSWITCH
		
		iSetupProgress++
		
	ENDIF
	
	IF ( iSetupProgress = 7 )
		
		RELEASE_SUPPRESSED_EMERGENCY_CALLS()							//by default allow emergency phone calls
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN, FALSE)	//by default allow player to be wanted for driving into the film studio
		
		SET_MAX_WANTED_LEVEL(5)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_CREATE_RANDOM_COPS(TRUE)
		
		RELEASE_SUPPRESSED_EMERGENCY_CALLS()
		
		DISABLE_TV_CONTROLS(TV_LOC_JIMMY_BEDROOM, FALSE)
		
		SET_PARTICLE_FX_CAM_INSIDE_VEHICLE(FALSE)
		
		SWITCH eStage
			
			CASE MISSION_STAGE_PRE_TIMELAPSE
			CASE MISSION_STAGE_TIMELAPSE
			CASE MISSION_STAGE_CUTSCENE_INTRO

				CLEAR_TIMECYCLE_MODIFIER()

				SET_PED_POPULATION_BUDGET(0)
				SET_VEHICLE_POPULATION_BUDGET(0)
								
				iSetupProgress++
				
			BREAK
			
			CASE MISSION_STAGE_YOGA_WARRIOR
			CASE MISSION_STAGE_CUTSCENE_TRIANGLE
			CASE MISSION_STAGE_YOGA_TRIANGLE
			CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
			CASE MISSION_STAGE_CUTSCENE_POOL
			
				//CLEAR_TIMECYCLE_MODIFIER()
				
				SUPPRESS_EMERGENCY_CALLS()
				
				SET_PED_POPULATION_BUDGET(0)
				SET_VEHICLE_POPULATION_BUDGET(0)
			
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_YOGA_TRANQUIL")
					START_AUDIO_SCENE("FAMILY_5_YOGA_TRANQUIL")
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_YOGA_MG")
					START_AUDIO_SCENE("FAMILY_5_YOGA_MG")
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), -1.0/*-1.2*/, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), 1.0/*1.2*/, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF

				iSetupProgress++
			
			BREAK
			
			CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
			
				CLEAR_TIMECYCLE_MODIFIER()
			
				SUPPRESS_EMERGENCY_CALLS()
				
				DISABLE_TV_CONTROLS(TV_LOC_JIMMY_BEDROOM, TRUE)
				
				SET_MAX_WANTED_LEVEL(0)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_CREATE_RANDOM_COPS(FALSE)
				
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_TO_JIMMY")
					START_AUDIO_SCENE("FAMILY_5_GO_TO_JIMMY")
				ENDIF
				
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON), V_ILEV_MM_DOORSON, <<-806.7716, 174.0236, 76.8903>>, FALSE, FALSE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON), -1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_SON), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF

				iSetupProgress++
			
			BREAK
			
			
			CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
				
				CLEAR_TIMECYCLE_MODIFIER()
				
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_TO_JIMMY")
					START_AUDIO_SCENE("FAMILY_5_GO_TO_JIMMY")
				ENDIF
				
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON), V_ILEV_MM_DOORSON, <<-806.7716, 174.0236, 76.8903>>, FALSE, FALSE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON), -1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_SON), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF

				iSetupProgress++
		
			BREAK
			
			CASE MISSION_STAGE_GOTO_BURGER_SHOT
			
				CLEAR_TIMECYCLE_MODIFIER()
				
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_TO_JIMMY")
					START_AUDIO_SCENE("FAMILY_5_GO_TO_JIMMY")
				ENDIF
				
				SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICE, TRUE)
				
				//set vehicle generators to spawn ambient cars around the burger shot area
				SET_VEHICLE_GENERATOR_AREA_OF_INTEREST(<< -1174.32, -887.45, 14.41 >>, 25.0)

				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
				
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON), V_ILEV_MM_DOORSON, <<-806.7716, 174.0236, 76.8903>>, FALSE, FALSE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON), -1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_SON), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF

				iSetupProgress++
			
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT

				CLEAR_TIMECYCLE_MODIFIER()
				SET_PARTICLE_FX_CAM_INSIDE_VEHICLE(TRUE)
				
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
				ENDIF		
				
				SET_MAX_WANTED_LEVEL(0)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_CREATE_RANDOM_COPS(FALSE)
				
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
				
				SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICE, TRUE)
				
				//set vehicle generators to spawn ambient cars around the burger shot area
				SET_VEHICLE_GENERATOR_AREA_OF_INTEREST(<< -1174.32, -887.45, 14.41 >>, 25.0)

				iSetupProgress++
				
			BREAK
			
			CASE MISSION_STAGE_DRIVE_HOME
			#IF IS_DEBUG_BUILD
				CASE MISSION_STAGE_TEST
			#ENDIF
			
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_TAKE_JIMMY")
					START_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY")
				ENDIF
			
				CLEAR_TIMECYCLE_MODIFIER()
			
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN, TRUE)
				
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
				ENDIF
				
				SUPPRESS_EMERGENCY_CALLS()
			
				SET_MAX_WANTED_LEVEL(0)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)	
				SET_CREATE_RANDOM_COPS(FALSE)
				
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
				
				SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICE, TRUE)
				
				//set vehicle generators to spawn ambient cars around the burger shot area
				SET_VEHICLE_GENERATOR_AREA_OF_INTEREST(<< -1174.32, -887.45, 14.41 >>, 25.0)

				iSetupProgress++
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BLACKOUT
			CASE MISSION_STAGE_FLIGHT

				IF IS_PLAYER_PLAYING(PLAYER_ID())
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
							
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_YOGA_ON_DRUGS")
					START_AUDIO_SCENE("FAMILY_5_YOGA_ON_DRUGS")
				ENDIF

				DISABLE_TAXI_HAILING(TRUE)
				
				iSetupProgress++
			
			BREAK
			
			CASE MISSION_STAGE_GO_HOME_WASTED
			
				CLEAR_TIMECYCLE_MODIFIER()
			
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
				
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
								
				//set the time 
				ADVANCE_CLOCK_TIME_TO(6, 0, 0)
				
				DISABLE_TAXI_HAILING(FALSE)
				
				//set vehicle generators to spawn ambient cars in the park parking lot
				SET_VEHICLE_GENERATOR_AREA_OF_INTEREST(<< -941.51, 308.92, 70.21 >>, 10.0)
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
				
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON), V_ILEV_MM_DOORSON, <<-806.7716, 174.0236, 76.8903>>, FALSE, FALSE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON), -1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_SON), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF

				iSetupProgress++
			
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_END_1
			CASE MISSION_STAGE_CUTSCENE_END_2
			
				CLEAR_TIMECYCLE_MODIFIER()
				
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
				
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON), V_ILEV_MM_DOORSON, <<-806.7716, 174.0236, 76.8903>>, FALSE, FALSE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON), -1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_SON), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
	
				iSetupProgress++
			
			BREAK

		ENDSWITCH
			
	ENDIF

	//handle player positioning when current stage is being skipped to or played normally
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 8 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
		
			IF ( bStageReplayed = TRUE )
			
				END_REPLAY_SETUP(NULL, DEFAULT, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling END_REPLAY_SETUP().")
				#ENDIF
		
			ELSE
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", MissionPosition.vStageStart, " for mission replay or stage skip. This might take a while to load.")
				#ENDIF

				LOAD_SCENE(MissionPosition.vStageStart)
				
				WAIT(1000)
			
			ENDIF

			//put player in car when required during the skip
			SWITCH eStage
			
				CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
				CASE MISSION_STAGE_DRIVE_HOME
				
				#IF IS_DEBUG_BUILD
					CASE MISSION_STAGE_TEST
				#ENDIF
				
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
							SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
							SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
												
						ENDIF
					ENDIF
				
				BREAK
			
			ENDSWITCH
			
			SWITCH eStage
			
				CASE MISSION_STAGE_YOGA_WARRIOR
				CASE MISSION_STAGE_YOGA_TRIANGLE
				CASE MISSION_STAGE_YOGA_SUNSALUTATION
				
					IF ( bYogaMusicPlaying = FALSE )
						IF TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_RESTART")
							bYogaMusicPlaying = TRUE
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event FAM5_YOGA_MOVE_RESTART.")
							#ENDIF
						ENDIF
					ENDIF
					
					IF PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Prepared music event FAM5_YOGA_MOVE_START.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Preparing music event FAM5_YOGA_MOVE_START.")
						#ENDIF
					ENDIF
				
				BREAK
				
				CASE MISSION_STAGE_CUTSCENE_TRIANGLE
				CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
				CASE MISSION_STAGE_CUTSCENE_POOL
					IF ( bYogaMusicPlaying = FALSE )
						IF TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_RESTART")
							bYogaMusicPlaying = TRUE
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event FAM5_YOGA_MOVE_RESTART.")
							#ENDIF
						ENDIF
					ENDIF
				BREAK
				
				DEFAULT
					IF ( bYogaMusicPlaying = TRUE )
						IF TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_ENDS")
							bYogaMusicPlaying = FALSE
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event FAM5_YOGA_MUSIC_ENDS.")
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping yoga music.")
							#ENDIF
						ENDIF
					ENDIF
				BREAK
			
			ENDSWITCH

			//handle fade in for each mission stage
			//some stages start with or require a fade out
			SWITCH eStage
			
				CASE MISSION_STAGE_CUTSCENE_INTRO
				CASE MISSION_STAGE_YOGA_WARRIOR
				CASE MISSION_STAGE_CUTSCENE_TRIANGLE
				CASE MISSION_STAGE_YOGA_TRIANGLE
				CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
				CASE MISSION_STAGE_YOGA_SUNSALUTATION
				CASE MISSION_STAGE_CUTSCENE_POOL
				CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
				CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
				CASE MISSION_STAGE_GOTO_BURGER_SHOT
				CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
				CASE MISSION_STAGE_CUTSCENE_BLACKOUT
				CASE MISSION_STAGE_FLIGHT
				CASE MISSION_STAGE_GO_HOME_WASTED
				CASE MISSION_STAGE_CUTSCENE_END_1
				CASE MISSION_STAGE_CUTSCENE_END_2
				
					//do not fade in, mission stage will handle a fade in from fade out
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Halting loading screen fade in for skip to a mission stage that will fade the screen in.")
					#ENDIF
					
				BREAK
			
				DEFAULT
				
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					//fade in
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling a loading screen fade in.")
					#ENDIF
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				
				BREAK

			ENDSWITCH

		ENDIF
		
		iSetupProgress++
	
	ENDIF
	
	//handle skipping to or replaying stages that require peds to play animations or be in cover
	IF ( iSetupProgress = 9 )
	
		//stage was skipped to or is being replayed
		IF 	( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
		
			SWITCH eStage
			
				CASE MISSION_STAGE_YOGA_WARRIOR
				CASE MISSION_STAGE_YOGA_TRIANGLE
				CASE MISSION_STAGE_YOGA_SUNSALUTATION

					STRING 	sPlayerAnimName
					STRING 	sAmandaAnimName
					STRING 	sFabienAnimName
					
					VECTOR	vAmandaPosition
					VECTOR	vAmandaRotation
					VECTOR 	vFabienPosition
					VECTOR 	vFabienRotation
					
					
					SWITCH eStage
						CASE MISSION_STAGE_YOGA_WARRIOR
							sPlayerAnimName = "start_pose"
							sAmandaAnimName = "f_yogapose_a"
							sFabienAnimName = "i_yogapose_a"
							
							vAmandaPosition = vAmandaWarriorPosition
							vAmandaRotation = vAmandaWarriorRotation
							vFabienPosition = vFabienWarriorPosition
							vFabienRotation = vFabienWarriorRotation
						BREAK
						CASE MISSION_STAGE_YOGA_TRIANGLE
							sPlayerAnimName = "start_pose"
							sAmandaAnimName = "f_yogapose_b"
							sFabienAnimName = "i_yogapose_b"
							
							vAmandaPosition = vAmandaTrianglePosition
							vAmandaRotation = vAmandaTriangleRotation
							vFabienPosition = vFabienTrianglePosition
							vFabienRotation = vFabienTriangleRotation
						BREAK
						CASE MISSION_STAGE_YOGA_SUNSALUTATION
							sPlayerAnimName = "start_pose"
							sAmandaAnimName = "f_yogapose_c"
							sFabienAnimName = "i_yogapose_c"
							
							vAmandaPosition = vAmandaSunsalutationPosition
							vAmandaRotation = vAmandaSunsalutationRotation
							vFabienPosition = vFabienSunsalutationPosition
							vFabienRotation = vFabienSunsalutationRotation
						BREAK
					ENDSWITCH
	
					IF 	DOES_ENTITY_EXIST(PLAYER_PED_ID())
					AND DOES_ENTITY_EXIST(psAmanda.PedIndex)
					AND DOES_ENTITY_EXIST(psFabien.PedIndex)
						
						IF 	NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						AND NOT IS_ENTITY_DEAD(psAmanda.PedIndex)
						AND NOT IS_ENTITY_DEAD(psFabien.PedIndex)
						AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
						AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(psAmanda.PedIndex)
						AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(psFabien.PedIndex)
						
							IF  IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_yoga", sPlayerAnimName)
							AND IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "missfam5_yoga", sAmandaAnimName)
							AND IS_ENTITY_PLAYING_ANIM(psFabien.PedIndex, "missfam5_yoga", sFabienAnimName)
							AND DOES_CAM_EXIST(ScriptedCamera)
							AND IS_CAM_RENDERING(ScriptedCamera)
							
								IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			
									NEW_LOAD_SCENE_START(<< -782.81, 187.44, 73.33 >>, << -0.98, -0.17, -0.06 >>, 20.0)
									
									iLoadSceneTimer = GET_GAME_TIMER()
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
									#ENDIF
									
								ENDIF
								
								IF IS_NEW_LOAD_SCENE_LOADED()
								OR HAS_TIME_PASSED(10000, iLoadSceneTimer)

									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to IS_NEW_LOAD_SCENE_LOADED() returning TRUE.")
									#ENDIF
									
									NEW_LOAD_SCENE_STOP()
											
									iSetupProgress++
								
								ENDIF
							
							ELSE

								IF HAS_ANIM_DICT_LOADED("missfam5_yoga")

									IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_yoga", sPlayerAnimName)
										TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5_yoga", sPlayerAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0, FALSE, AIK_DISABLE_LEG_IK)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									ENDIF
		
									IF NOT IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "missfam5_yoga", sAmandaAnimName)
										TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam5_yoga", sAmandaAnimName,
																vAmandaPosition, vAmandaRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
																AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
																0, EULER_YXZ, AIK_DISABLE_LEG_IK)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
									ENDIF			

									IF NOT IS_ENTITY_PLAYING_ANIM(psFabien.PedIndex, "missfam5_yoga", sFabienAnimName)
										TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam5_yoga", sFabienAnimName,
																vFabienPosition, vFabienRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
																AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
																0, EULER_YXZ, AIK_DISABLE_LEG_IK)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
									ENDIF			
								
								ELSE
								
									REQUEST_ANIM_DICT("missfam5_yoga")
								
								ENDIF
								
								IF NOT DOES_CAM_EXIST(ScriptedCamera)
					
									ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)

									SET_CAM_PARAMS(ScriptedCamera, << -782.802368, 187.4389, 73.326614>>, << -3.540541, 0.000000, 99.799942>>, 39.3555)	   
								  	SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.125)
								   
								   	SET_CAM_FAR_CLIP(ScriptedCamera, 500)
									SET_CAM_FAR_CLIP(ScriptedCamera, 500)
								   									
									DISPLAY_RADAR(FALSE)
									DISPLAY_HUD(FALSE)
									
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								ENDIF
							
							ENDIF
						
						ENDIF
						
						
					ENDIF
		
				BREAK
				
				DEFAULT
					iSetupProgress++
				BREAK
				
			ENDSWITCH 
		
		ELSE
		
			iSetupProgress++
		
		ENDIF
	
	ENDIF
	
	
	//handle setting of mid-mission replay checkpoints
	IF ( iSetupProgress = 10 )
	
		SWITCH eStage
		
			CASE MISSION_STAGE_YOGA_WARRIOR
			
				iStageTimeHours 	= GET_CLOCK_HOURS()
				iStageTimeMinutes 	= GET_CLOCK_MINUTES()
				iStageTimeSeconds 	= GET_CLOCK_SECONDS()
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_YOGA_WARRIOR), "YOGA - WARRIOR", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_YOGA_WARRIOR), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_YOGA_TRIANGLE
			
				iStageTimeHours 	= GET_CLOCK_HOURS()
				iStageTimeMinutes 	= GET_CLOCK_MINUTES()
				iStageTimeSeconds 	= GET_CLOCK_SECONDS()
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_YOGA_TRIANGLE), "YOGA - TRIANGLE", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_YOGA_TRIANGLE), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
			
				iStageTimeHours 	= GET_CLOCK_HOURS()
				iStageTimeMinutes 	= GET_CLOCK_MINUTES()
				iStageTimeSeconds 	= GET_CLOCK_SECONDS()
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_YOGA_SUNSALUTATION), "YOGA - SUNSALUTATION", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_YOGA_SUNSALUTATION), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GOTO_JIMMYS_ROOM
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GOTO_JIMMYS_ROOM), "GO TO JIMMY'S ROOM", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GOTO_JIMMYS_ROOM), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GOTO_BURGER_SHOT
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GOTO_BURGER_SHOT), "GO TO BURGER SHOT", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GOTO_BURGER_SHOT), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_DRIVE_HOME
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_HOME), "DRIVE HOME", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_DRIVE_HOME), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GO_HOME_WASTED
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GO_HOME_WASTED), "GO HOME WASTED", TRUE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GO_HOME_WASTED), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
		ENDSWITCH
	
		iSetupProgress++
	
	ENDIF
	
	//print the message only once per mission stage
	//handle fades
	IF ( iSetupProgress = 11 )
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission stage loading for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded 	= TRUE
		bStageReplayed 	= FALSE
		bStageSkippedTo = FALSE
		
		SWITCH eStage
			CASE MISSION_STAGE_YOGA_WARRIOR
			CASE MISSION_STAGE_YOGA_TRIANGLE
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			BREAK
			DEFAULT
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			BREAK
		ENDSWITCH
		
		iSetupProgress++
		
	ENDIF
	
	IF ( iSetupProgress = 12 )

		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE	

ENDFUNC

FUNC BOOL IS_PED_SHOOTING_AT_AREA(PED_INDEX PedIndex, BOOL bIsPlayer, VECTOR vPosition, FLOAT fRadius #IF IS_DEBUG_BUILD, BOOL bDrawDebugSphere = TRUE,
								  INT iRed = 255, INT iGreen = 0, INT iBlue = 0, INT iAlpha = 128 #ENDIF)

	IF NOT IS_PED_INJURED(PedIndex)

		#IF IS_DEBUG_BUILD
			IF ( bDrawDebugSphere = TRUE )
				DRAW_DEBUG_SPHERE(vPosition, fRadius, iRed, iGreen, iBlue, iAlpha)
			ENDIF
		#ENDIF

		IF IS_PED_SHOOTING(PedIndex)
			RETURN IS_BULLET_IN_AREA(vPosition, fRadius, bIsPlayer)
		ENDIF
	
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PED_SHOOTING_AT_ENTITY(PED_INDEX PedIndex, BOOL bIsPlayer, ENTITY_INDEX EntityIndex, BOOL bCheckArea = TRUE, FLOAT fRadius = 2.5
									#IF IS_DEBUG_BUILD, BOOL bDrawDebugSphere = TRUE, INT iRed = 255, INT iGreen = 0, INT iBlue = 0,
									INT iAlpha = 128 #ENDIF)

	IF NOT IS_PED_INJURED(PedIndex)

		IF DOES_ENTITY_EXIST(EntityIndex)
		
			IF NOT IS_ENTITY_DEAD(EntityIndex)

				IF ( bCheckArea = TRUE )
					
					VECTOR vEntityPosition = GET_ENTITY_COORDS(EntityIndex)
					
					FLOAT fGroundZ
					
					IF GET_GROUND_Z_FOR_3D_COORD(vEntityPosition, fGroundZ)
						vEntityPosition.Z = fGroundZ
					ENDIF
					
					IF IS_PED_SHOOTING_AT_AREA(PedIndex, bIsPlayer, vEntityPosition, fRadius #IF IS_DEBUG_BUILD, bDrawDebugSphere, iRed, iGreen, iBlue, iAlpha #ENDIF)
						RETURN TRUE
					ENDIF

				ENDIF

				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(EntityIndex, PedIndex, FALSE)
				
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(EntityIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
					
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(EntityIndex)
						CLEAR_ENTITY_LAST_WEAPON_DAMAGE(EntityIndex)
						
						RETURN TRUE
						
					ENDIF
				
				ENDIF
				
			ENDIF
		
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks if specified ped is using petrol can weapon to pour petrol at or around the specified entity.
/// PARAMS:
///    PedIndex - Ped to check for petrol can usage.
///    EntityIndex - Entity to check for petrol being used against.
///    fRadius - Radius to check around the specified entity.
/// RETURNS:
///    TRUE if specified ped is using petrol can to pour petrol around specified entity in the specified radius. FALSE if otherwise.
FUNC BOOL IS_PED_USING_PETROL_CAN_AT_ENTITY(PED_INDEX PedIndex, ENTITY_INDEX EntityIndex, FLOAT fRadius = 2.5 
											#IF IS_DEBUG_BUILD, BOOL bDrawDebugSphere = TRUE, INT iRed = 0, INT iGreen = 255, INT iBlue = 0,
											INT iAlpha = 128 #ENDIF)

	IF NOT IS_PED_INJURED(PedIndex)

		IF DOES_ENTITY_EXIST(EntityIndex)
		
			IF NOT IS_ENTITY_DEAD(EntityIndex)
			
				WEAPON_TYPE PedCurrentWeaponType

				IF GET_CURRENT_PED_WEAPON(PedIndex, PedCurrentWeaponType)	//check if ped has petrol can equipped first
			
					IF ( PedCurrentWeaponType = WEAPONTYPE_PETROLCAN )

						VECTOR vEntityPosition = GET_ENTITY_COORDS(EntityIndex)
					
						FLOAT fGroundZ
						
						IF GET_GROUND_Z_FOR_3D_COORD(vEntityPosition, fGroundZ)
							vEntityPosition.Z = fGroundZ
						ENDIF
						
						//draw debug sphere to visualise the check area, this will only draw when petrol can is equipped by ped
						#IF IS_DEBUG_BUILD
							IF ( bDrawDebugSphere = TRUE )
								DRAW_DEBUG_SPHERE(vEntityPosition, fRadius, iRed, iGreen, iBlue, iAlpha)
							ENDIF
						#ENDIF
						
						//check if ped is using petrol can and if any petrol decals are the entity
						IF IS_PED_SHOOTING(PedIndex)
						AND GET_IS_PETROL_DECAL_IN_RANGE(vEntityPosition, fRadius)

							RETURN TRUE
						
						ENDIF
					
					ENDIF
			
				ENDIF
				
			ENDIF
			
		ENDIF

	ENDIF

	RETURN FALSE

ENDFUNC

PROC MAKE_PED_FLEE_PED(PED_INDEX PedIndex, PED_INDEX PedIndexToFleeFrom, FLOAT fSafeDistance, INT iTime)

	IF NOT IS_PED_INJURED(PedIndexToFleeFrom)
		IF DOES_ENTITY_EXIST(PedIndex)
			IF NOT IS_PED_INJURED(PedIndex)
				CLEAR_PED_TASKS(PedIndex)
				SET_PED_FLEE_ATTRIBUTES(PedIndex, FA_CAN_SCREAM, TRUE)
				TASK_SMART_FLEE_PED(PedIndex, PedIndexToFleeFrom, fSafeDistance, iTime)
				SET_PED_KEEP_TASK(PedIndex, TRUE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks for various mission fail conditions. Redirects mission flow to MISSION_STAGE_FAILED when one of fail conditions is true.
/// PARAMS:
///    eStage - Current mission stage.
///    eFail - Mission fail reason.
PROC RUN_FAIL_CHECKS(MISSION_STAGES &eStage, MISSION_FAILS &eFail)

	IF 	eStage <> MISSION_STAGE_PASSED
	AND eStage <> MISSION_STAGE_FAILED
	
		IF NOT IS_CUTSCENE_PLAYING()
		
			IF ( eStage = MISSION_STAGE_YOGA_WARRIOR )
			OR ( eStage = MISSION_STAGE_YOGA_TRIANGLE )
			OR ( eStage = MISSION_STAGE_YOGA_SUNSALUTATION )
			
				IF ( FailFlags[MISSION_FAIL_YOGA_FAILED] = TRUE )
					IF ( sYogaPlayer.iFailCounter >= MAX_YOGA_FAILS )
						eFail = MISSION_FAIL_YOGA_FAILED
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
			
			ENDIF
		
			IF DOES_ENTITY_EXIST(psAmanda.PedIndex)
				IF ( FailFlags[MISSION_FAIL_AMANDA_DEAD] = TRUE )
					IF IS_PED_INJURED(psAmanda.PedIndex)
						eFail = MISSION_FAIL_AMANDA_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psFabien.PedIndex)
				IF ( FailFlags[MISSION_FAIL_FABIEN_DEAD] = TRUE )
					IF IS_PED_INJURED(psFabien.PedIndex)
						eFail = MISSION_FAIL_FABIEN_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
			ENDIF
	
			IF DOES_ENTITY_EXIST(psJimmy.PedIndex)
				IF ( FailFlags[MISSION_FAIL_JIMMY_DEAD] = TRUE )
					IF IS_PED_INJURED(psJimmy.PedIndex)
						eFail = MISSION_FAIL_JIMMY_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] = TRUE )
					IF DOES_BLIP_EXIST(sLocatesData.BuddyBlipID[0])
					OR DOES_BLIP_EXIST(psJimmy.BlipIndex)
						IF NOT IS_PED_INJURED(psJimmy.PedIndex)
							IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psJimmy.PedIndex) > ABANDON_BUDDY_FAIL_RANGE
							OR ( bJimmyLeftBehindSprinting = TRUE )
								eFail = MISSION_FAIL_JIMMY_LEFT_BEHIND
								eStage = MISSION_STAGE_FAILED
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				IF ( FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE] = TRUE )
					IF IS_VEHICLE_PERMANENTLY_STUCK(vsMichaelsCar.VehicleIndex)
						eFail = MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] = TRUE )
					IF NOT IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex, TRUE)
					OR ( HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vsMichaelsCar.VehicleIndex, PLAYER_PED_ID()) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vsMichaelsCar.VehicleIndex, WEAPONTYPE_MOLOTOV) )
						eFail = MISSION_FAIL_MICHAELS_CAR_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND] = TRUE )
					IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
					OR DOES_BLIP_EXIST(vsMichaelsCar.BlipIndex)
						IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex) > ABANDON_BUDDY_FAIL_RANGE
								eFail 	= MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND
								eStage 	= MISSION_STAGE_FAILED
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
				
			IF DOES_ENTITY_EXIST(psDealer.PedIndex)
				IF ( FailFlags[MISSION_FAIL_DEALER_DEAD] = TRUE )			
					IF IS_PED_INJURED(psDealer.PedIndex)
						eFail = MISSION_FAIL_DEALER_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_DEALER_SCARED] = TRUE )
					IF NOT IS_PED_INJURED(psDealer.PedIndex)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psDealer.PedIndex, PLAYER_PED_ID(), TRUE)
						OR (IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psDealer.PedIndex, vsMichaelsCar.VehicleIndex))
						OR IS_PED_SHOOTING_AT_ENTITY(PLAYER_PED_ID(), TRUE, psDealer.PedIndex, TRUE, 2.5)
						OR IS_PED_USING_PETROL_CAN_AT_ENTITY(PLAYER_PED_ID(), psDealer.PedIndex, 3.0)
						OR IS_ENTITY_TOUCHING_ENTITY(psDealer.PedIndex, PLAYER_PED_ID())
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_PED_COLLISION_WITH_PLAYER)
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_SWITCH_2_NM_TASK)
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_EXPLOSION)
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_SHOCKING_EXPLOSION)
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_SHOT_FIRED)
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_POTENTIAL_BLAST)
						OR HAS_PED_RECEIVED_EVENT(psDealer.PedIndex, EVENT_POTENTIAL_GET_RUN_OVER)
						OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1163.59, -901.60, 1.64 >>, << -1184.38, -870.28, 20.92 >>, 32.0)
						OR IS_PED_FLEEING(psDealer.PedIndex)
							PLAY_PAIN(psDealer.PedIndex, AUD_DAMAGE_REASON_SCREAM_SCARED)
							IF NOT IS_PED_FLEEING(psDealer.PedIndex)
								MAKE_PED_FLEE_PED(psDealer.PedIndex, PLAYER_PED_ID(), 300, -1)
							ENDIF
							eFail 	= MISSION_FAIL_DEALER_SCARED //MISSION_FAIL_DEALER_ATTACKED
							eStage 	= MISSION_STAGE_FAILED
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets mission as failed and provides mission flow with reason to print on screen.
/// PARAMS:
///    eFailReason - One of MISSION_FAILS values.
PROC SET_MISSION_FAILED_WITH_REASON(MISSION_FAILS &eFailReason)   

	SWITCH eFailReason
	
		//Player failed yoga
		CASE MISSION_FAIL_YOGA_FAILED
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM5_F_Y1")
		BREAK	
		//Michael's car is undriveable
		CASE MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_GENSTCK")
		BREAK
		//Michael's car is destroyed
		CASE MISSION_FAIL_MICHAELS_CAR_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_MDEST")
		BREAK
		CASE MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM5_F_CLB")
		BREAK
		//Wife is dead
		CASE MISSION_FAIL_AMANDA_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_ADIED")
		BREAK
		//Instructor is dead
		CASE MISSION_FAIL_FABIEN_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM5_F_B1")
		BREAK
		//Son is dead
		CASE MISSION_FAIL_JIMMY_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_JDIED")
		BREAK
		//Son is left behind
		CASE MISSION_FAIL_JIMMY_LEFT_BEHIND
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_JLEFT")
		BREAK
		//Dealer attacked
		CASE MISSION_FAIL_DEALER_ATTACKED
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM5_F_E0")
		BREAK
		//Dealer scared off
		CASE MISSION_FAIL_DEALER_SCARED
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM5_F_E1")
		BREAK
		//Dealer killed
		CASE MISSION_FAIL_DEALER_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM5_F_E2")
		BREAK
		//Generic fail
		//Debug forced fail
		CASE MISSION_FAIL_EMPTY
		CASE MISSION_FAIL_FORCE_FAIL
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("FAM5_FAIL")
		BREAK

	ENDSWITCH
	
ENDPROC

PROC MANAGE_YOGA_CONVERSATIONS(YOGASTRUCT Yoga, INT&iPosesCompleted, INT &iBreathsCompleted, INT &iIdleTimer, INT &iFails,
							   STRING sIdleRoot1, STRING sIdleRoot2, STRING sPraiseRoot1, STRING sPraiseRoot2, STRING sEncourageRoot,
							   BOOL &bBreatheStartConversationFlag, STRING sBreatheStartRoot,
							   BOOL &bBreatheCompletedConversationFlag, STRING sBreatheCompletedRoot,
							   BOOL &bBreatheFailedConversationFlag, STRING sBreatheFailedRoot,
							   BOOL &bInhaleConversationFlag, STRING sInhaleRoot,
							   BOOL &bExhaleConversationFlag, STRING sExhaleRoot,
							   BOOL &bFailConversationFlag, STRING sFailRoot1, STRING sFailRoot2, STRING sFailRootPlayer, INT iFailRootPlayerLines)

	SWITCH Yoga.eYogaMinigameStage
	
		CASE YOGAMINIGAMESTAGE_SETUP
			iIdleTimer = GET_GAME_TIMER()
		BREAK

		CASE YOGAMINIGAMESTAGE_DOMOVE
		
			bFailConversationFlag 		= FALSE
			
			IF ( iPosesCompleted = Yoga.iPosesCompleted )					//pose counters are equal, no pose was completed, play idle dialogue
		
				IF ( Yoga.eYogaMoveStage = YOGAMOVESTAGE_DOINGPOSES )		//only play the idle conversation when player is meant to do the pose
																			//which means player has to position sticks
					
					bBreatheStartConversationFlag 		= FALSE
					bBreatheCompletedConversationFlag	= FALSE
					bBreatheFailedConversationFlag		= FALSE
					
																			//check if fail happened, if so play retry conversation													
					IF ( iFails <> Yoga.iFailCounter )						//fail counters are not equal, fail just happened
																			//play retry conversation and reset the idle timer
						IF NOT HAS_LABEL_BEEN_TRIGGERED(sEncourageRoot)
			
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sEncourageRoot, CONV_PRIORITY_MEDIUM)								
									SET_LABEL_AS_TRIGGERED(sEncourageRoot, TRUE)
								ENDIF
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
							
						ELSE
						
							IF NOT IS_THIS_CONVERSATION_PLAYING(sEncourageRoot)
								SET_LABEL_AS_TRIGGERED(sEncourageRoot, FALSE)
								iFails 		= Yoga.iFailCounter
								iIdleTimer 	= GET_GAME_TIMER()
							ENDIF
						
						ENDIF
					
					ENDIF
																			
					IF NOT HAS_LABEL_BEEN_TRIGGERED(sIdleRoot1)
					OR NOT HAS_LABEL_BEEN_TRIGGERED(sIdleRoot2)
					
						IF HAS_TIME_PASSED(12500, iIdleTimer)							//check idle timer
					
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
								SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)					//select random idle root
								
									CASE 0
										IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sIdleRoot1, CONV_PRIORITY_MEDIUM)	//play idle root 1
											SET_LABEL_AS_TRIGGERED(sIdleRoot1, TRUE)									
											SET_LABEL_AS_TRIGGERED(sIdleRoot2, TRUE)	//set both idle roots as triggered
										ENDIF	
									BREAK
									
									CASE 1
										IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sIdleRoot2, CONV_PRIORITY_MEDIUM)	//play idle root 2
											SET_LABEL_AS_TRIGGERED(sIdleRoot1, TRUE)
											SET_LABEL_AS_TRIGGERED(sIdleRoot2, TRUE)	//set both idle roots as triggered
										ENDIF
									BREAK
								
								ENDSWITCH
								
								
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
							
						ENDIF
					
					ELSE
						
						 IF	 NOT IS_THIS_CONVERSATION_PLAYING(sIdleRoot1)
						 AND NOT IS_THIS_CONVERSATION_PLAYING(sIdleRoot2)
							SET_LABEL_AS_TRIGGERED(sIdleRoot1, FALSE)
							SET_LABEL_AS_TRIGGERED(sIdleRoot2, FALSE)
							iIdleTimer = GET_GAME_TIMER()
						ENDIF
					
					ENDIF
				
				ELSE
				
					iIdleTimer = GET_GAME_TIMER()	//reset the idle timer is player is in a different move stage than doing poses
				
				ENDIF
			
			ELSE	//pose counters are not equal, pose was completed, play praise conversation and reset the idle timer
			
				bBreatheStartConversationFlag = FALSE
				
				iBreathsCompleted = Yoga.iBreathingCounter
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": iBreathsCompleted: ", iBreathsCompleted, " Yoga.iBreathingCounter: ", Yoga.iBreathingCounter, ".")
				#ENDIF
			
				IF NOT HAS_LABEL_BEEN_TRIGGERED(sPraiseRoot1)
				OR NOT HAS_LABEL_BEEN_TRIGGERED(sPraiseRoot2)
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
						SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)					//select random praise root
						
							CASE 0
								IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sPraiseRoot1, CONV_PRIORITY_MEDIUM)	//play praise root 1
									SET_LABEL_AS_TRIGGERED(sPraiseRoot1, TRUE)									
									SET_LABEL_AS_TRIGGERED(sPraiseRoot2, TRUE)	//set both praise roots as triggered
								ENDIF	
							BREAK
							
							CASE 1
								IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sPraiseRoot2, CONV_PRIORITY_MEDIUM)	//play praise root 2
									SET_LABEL_AS_TRIGGERED(sPraiseRoot1, TRUE)
									SET_LABEL_AS_TRIGGERED(sPraiseRoot2, TRUE)	//set both praise roots as triggered
								ENDIF
							BREAK
						
						ENDSWITCH
							
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				
				ELSE
				
					 IF	 NOT IS_THIS_CONVERSATION_PLAYING(sPraiseRoot1)
					 AND NOT IS_THIS_CONVERSATION_PLAYING(sPraiseRoot1)
						SET_LABEL_AS_TRIGGERED(sPraiseRoot1, FALSE)
						SET_LABEL_AS_TRIGGERED(sPraiseRoot1, FALSE)
						iPosesCompleted = Yoga.iPosesCompleted
						iIdleTimer 		= GET_GAME_TIMER()
					ENDIF
				
				ENDIF

			ENDIF
		
			SWITCH Yoga.eYogaMoveStage
			
				CASE YOGAMOVESTAGE_BREATHE
				
					IF ( iBreathsCompleted <> Yoga.iBreathingCounter)
						bBreatheCompletedConversationFlag 	= FALSE
						bBreatheFailedConversationFlag 		= FALSE
						iBreathsCompleted = Yoga.iBreathingCounter
					ENDIF
				
					IF ( bBreatheStartConversationFlag = FALSE )
	
						IF NOT HAS_LABEL_BEEN_TRIGGERED(sBreatheStartRoot)
					
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

								IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sBreatheStartRoot, CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED(sBreatheStartRoot, TRUE)
									bBreatheStartConversationFlag = TRUE
								ENDIF
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
							
						ELSE
						
							IF NOT IS_THIS_CONVERSATION_PLAYING(sBreatheStartRoot)
								SET_LABEL_AS_TRIGGERED(sBreatheStartRoot, FALSE)
							ENDIF
							
						ENDIF
					
					ENDIF
					
					IF ( bBreatheCompletedConversationFlag = FALSE )
					
						IF 	( Yoga.bInhaleCompleted = TRUE )
						AND	( Yoga.bExhaleCompleted = TRUE )
						
							IF NOT HAS_LABEL_BEEN_TRIGGERED(sBreatheCompletedRoot)
					
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

									IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sBreatheCompletedRoot, CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED(sBreatheCompletedRoot, TRUE)
										bBreatheCompletedConversationFlag = TRUE
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
								
							ELSE
							
								IF NOT IS_THIS_CONVERSATION_PLAYING(sBreatheCompletedRoot)
									SET_LABEL_AS_TRIGGERED(sBreatheCompletedRoot, FALSE)
								ENDIF
								
							ENDIF

						ENDIF
					
					ENDIF

					IF ( bBreatheFailedConversationFlag = FALSE )
					
						IF ( Yoga.bInhaleFailed = TRUE OR Yoga.bExhaleFailed = TRUE )
							
								IF NOT HAS_LABEL_BEEN_TRIGGERED(sBreatheFailedRoot)
					
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

										IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sBreatheFailedRoot, CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED(sBreatheFailedRoot, TRUE)
											bBreatheFailedConversationFlag = TRUE
										ENDIF
									ELSE
										KILL_FACE_TO_FACE_CONVERSATION()
									ENDIF
									
								ELSE
								
									IF NOT IS_THIS_CONVERSATION_PLAYING(sBreatheFailedRoot)
										SET_LABEL_AS_TRIGGERED(sBreatheFailedRoot, FALSE)
									ENDIF
									
								ENDIF
							
							ENDIF
					
					ENDIF
					
					SWITCH Yoga.eYogaBreatheStage

						CASE YOGABREATHESTAGES_INHALE
						
							bExhaleConversationFlag = FALSE
						
							IF Yoga.bInhaleInProgress = FALSE
						
								IF ( bInhaleConversationFlag = FALSE )
		
									IF NOT HAS_LABEL_BEEN_TRIGGERED(sInhaleRoot)
								
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

											IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", PICK_STRING(GET_RANDOM_BOOL(), sInhaleRoot, "FAM5_YOIA"), CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED(sInhaleRoot, TRUE)
												bInhaleConversationFlag = TRUE
											ENDIF
										ELSE
											KILL_FACE_TO_FACE_CONVERSATION()
										ENDIF
										
									ELSE
									
										IF NOT IS_THIS_CONVERSATION_PLAYING(sInhaleRoot)
											SET_LABEL_AS_TRIGGERED(sInhaleRoot, FALSE)
										ENDIF
										
									ENDIF
								
								ENDIF
								
								
							ELSE
							
								bInhaleConversationFlag = TRUE
								
							ENDIF
						
						BREAK
						
						CASE YOGABREATHESTAGES_EXHALE
						
							bInhaleConversationFlag = FALSE
							
							bBreatheCompletedConversationFlag = FALSE
							
							IF Yoga.bExhaleInProgress = FALSE
							
								IF ( bExhaleConversationFlag = FALSE )
		
									IF NOT HAS_LABEL_BEEN_TRIGGERED(sExhaleRoot)
								
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

											IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", PICK_STRING(GET_RANDOM_BOOL(), sExhaleRoot, "FAM5_YOEA"), CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED(sExhaleRoot, TRUE)
												bExhaleConversationFlag = TRUE
											ENDIF
										ELSE
											KILL_FACE_TO_FACE_CONVERSATION()
										ENDIF
										
									ELSE
									
										IF NOT IS_THIS_CONVERSATION_PLAYING(sExhaleRoot)
											SET_LABEL_AS_TRIGGERED(sExhaleRoot, FALSE)
										ENDIF
										
									ENDIF
								
								ENDIF
								
							ELSE
							
								bExhaleConversationFlag = TRUE
							
							ENDIF
						
						BREAK
					
					ENDSWITCH
					
						
				BREAK

			ENDSWITCH
		
		BREAK
		
		CASE YOGAMINIGAMESTAGE_DOFAIL
		CASE YOGAMINIGAMESTAGE_WAITFAIL
		
			bExhaleConversationFlag 			= FALSE
			bInhaleConversationFlag 			= FALSE
			bFailConversationFlag 				= FALSE
			bBreatheStartConversationFlag 		= FALSE
			bBreatheCompletedConversationFlag	= FALSE
			bBreatheFailedConversationFlag		= FALSE
			
			IF ( Yoga.bFailing = TRUE )										//fail conversation player
				
				iIdleTimer 				= GET_GAME_TIMER()					//reset idle timer
				iPosesCompleted		 	= Yoga.iPosesCompleted				//this should be set to 0 during fail
				iBreathsCompleted		= Yoga.iBreathingCounter
				
				SET_LABEL_AS_TRIGGERED(sFailRoot1, FALSE)		
				SET_LABEL_AS_TRIGGERED(sFailRoot2, FALSE)
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(sFailRootPlayer)
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF ( iFailRootPlayerCounter >= iFailRootPlayerLines )
							IF NOT IS_AMBIENT_SPEECH_PLAYING(Yoga.PedIndex)
								IF HAS_SOUND_FINISHED(Yoga.iFailSoundID)
									SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
										CASE 0
											PLAY_PED_AMBIENT_SPEECH(Yoga.PedIndex, "GENERIC_CURSE_MED", SPEECH_PARAMS_FORCE_NORMAL)
										BREAK
									ENDSWITCH
									SET_LABEL_AS_TRIGGERED(sFailRootPlayer, TRUE)
								ENDIF
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sFailRootPlayer, CONV_PRIORITY_MEDIUM)
								iFailRootPlayerCounter++
								SET_LABEL_AS_TRIGGERED(sFailRootPlayer, TRUE)
							ENDIF
						ENDIF
					ELSE
						IF IS_SCRIPTED_SPEECH_PLAYING(Yoga.PedIndex)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ELSE
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
				
				ELSE
				
					IF NOT IS_THIS_CONVERSATION_PLAYING(sFailRootPlayer)
						SET_LABEL_AS_TRIGGERED(sFailRootPlayer, FALSE)
					ENDIF
					
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE YOGAMINIGAMESTAGE_FAILRESET
		CASE YOGAMINIGAMESTAGE_RESTART
		
			bExhaleConversationFlag 			= FALSE
			bInhaleConversationFlag 			= FALSE
			bBreatheStartConversationFlag 		= FALSE
			bBreatheCompletedConversationFlag	= FALSE
			bBreatheFailedConversationFlag		= FALSE
			
			IF ( bFailConversationFlag = FALSE )
	
				IF NOT HAS_LABEL_BEEN_TRIGGERED(sFailRoot1)
				OR NOT HAS_LABEL_BEEN_TRIGGERED(sFailRoot2)
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											
						SWITCH iFailConversationSpeaker
						
							CASE 0
								IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sFailRoot1, CONV_PRIORITY_MEDIUM)	//play fail root 1
									SET_LABEL_AS_TRIGGERED(sFailRoot1, TRUE)									
									SET_LABEL_AS_TRIGGERED(sFailRoot2, TRUE)						//set both fail roots as triggered
									bFailConversationFlag = TRUE
									iPosesCompleted 	= Yoga.iPosesCompleted						//this should be set to 0 during fail
									iBreathsCompleted	= Yoga.iBreathingCounter
									iIdleTimer 			= GET_GAME_TIMER()							//reset idle timer
									REPLAY_RECORD_BACK_FOR_TIME(5, 4, REPLAY_IMPORTANCE_HIGHEST)
								ENDIF	
							BREAK
							
							CASE 1
								IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sFailRoot2, CONV_PRIORITY_MEDIUM)	//play fail root 2
									SET_LABEL_AS_TRIGGERED(sFailRoot1, TRUE)
									SET_LABEL_AS_TRIGGERED(sFailRoot2, TRUE)						//set both fail roots as triggered
									bFailConversationFlag = TRUE
									iPosesCompleted		= Yoga.iPosesCompleted						//this should be set to 0 during fail
									iBreathsCompleted	= Yoga.iBreathingCounter
									iIdleTimer 			= GET_GAME_TIMER()							//reset idle timer
									REPLAY_RECORD_BACK_FOR_TIME(5, 4, REPLAY_IMPORTANCE_HIGHEST)
								ENDIF
							BREAK
						
						ENDSWITCH
							
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				
				ENDIF
				
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
//	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF HAS_TIME_PASSED(7000, iAmbientSpeechtimer)
//					IF NOT IS_AMBIENT_SPEECH_PLAYING(psAmanda.PedIndex)
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psAmanda.PedIndex, "FAM5_FGAA", "AMANDA", SPEECH_PARAMS_FORCE)
//						iAmbientSpeechtimer = GET_GAME_TIMER()
//					ENDIF
//				ENDIF
//			ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_PRE_TIMELAPSE_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
				IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
			
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
						eMissionTriggerMeans 	= MISSION_TRIGGER_IN_VEHICLE

						iStageProgress++
						
					ENDIF
			
				ELSE

					eMissionTriggerMeans 	= MISSION_TRIGGER_ON_FOOT

					iStageProgress++
			
				ENDIF
			ENDIF

		BREAK
		
		CASE 1
		
			REQUEST_ANIM_DICT("missfam2mcs_intp1")
		
			REQUEST_MODEL(osLeftDoorDummy.ModelName)
			REQUEST_MODEL(osRightDoorDummy.ModelName)
			
			IF 	HAS_ANIM_DICT_LOADED("missfam2mcs_intp1")
			AND	HAS_MODEL_LOADED(osLeftDoorDummy.ModelName)
			AND	HAS_MODEL_LOADED(osRightDoorDummy.ModelName)
		
				SWITCH eMissionTriggerMeans
					CASE MISSION_TRIGGER_ON_FOOT
						iStageProgress++
					BREAK
					CASE MISSION_TRIGGER_IN_VEHICLE
						iStageProgress++
					BREAK
				ENDSWITCH
				
			ENDIF
		
		BREAK                  

		CASE 2
		
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iHouseEnterSceneID)
				
				STOP_FIRE_IN_RANGE(<< -817.305, 179.330, 71.241 >>, 20.0)
				REMOVE_DECALS_IN_RANGE(<< -817.305, 179.330, 71.241 >>, 20.0)
				CLEAR_AREA_OF_PROJECTILES(<< -817.305, 179.330, 71.241 >>, 20.0)
				REMOVE_PARTICLE_FX_IN_RANGE(<< -817.305, 179.330, 71.241 >>, 20.0)
				
				iHouseEnterSceneID = CREATE_SYNCHRONIZED_SCENE(<< -817.305, 179.330, 71.225 >>, << 0.0, 0.0, -113.0 >>)
			
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iHouseEnterSceneID, TRUE)
			
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iHouseEnterSceneID, "missfam2mcs_intp1", "fam_2_int_p1_michael",
										INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), -1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				CREATE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName, FALSE)
            	CREATE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName, FALSE)
				
				osLeftDoorDummy.ObjectIndex 	= CREATE_OBJECT(osLeftDoorDummy.ModelName, <<-816.72, 179.10, 72.83>>)
                osRightDoorDummy.ObjectIndex 	= CREATE_OBJECT(osRightDoorDummy.ModelName, <<-816.11, 177.51, 72.83>>)
				
				PLAY_SYNCHRONIZED_ENTITY_ANIM(osLeftDoorDummy.ObjectIndex, iHouseEnterSceneID, "fam_2_int_p1_doorl", "missfam2mcs_intp1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
                PLAY_SYNCHRONIZED_ENTITY_ANIM(osRightDoorDummy.ObjectIndex, iHouseEnterSceneID, "fam_2_int_p1_doorr", "missfam2mcs_intp1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				
                FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(osLeftDoorDummy.ObjectIndex)
                FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(osRightDoorDummy.ObjectIndex)
				
				DESTROY_ALL_CAMS()
					
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)

				PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iHouseEnterSceneID, "fam5_intro_cam", "missfam2mcs_intp1")
				
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)			
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(osLeftDoorDummy.ModelName)
				SET_MODEL_AS_NO_LONGER_NEEDED(osRightDoorDummy.ModelName)
				
				NEW_LOAD_SCENE_START(<< -839.51, 180.32, 71.61 >>, << 1.0, -0.2, 0.2 >>, 1500.0)
				
				iLoadSceneTimer 				= GET_GAME_TIMER()
				bIntroCutsceneAssetsRequested 	= FALSE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
				#ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 3
		
			IF DOES_CAM_EXIST(ScriptedCamera)
			
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
				
					IF 	DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
					AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE(), TRUE) AND NOT IS_ENTITY_ON_FIRE(GET_PLAYERS_LAST_VEHICLE())					
					AND	NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
					AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
				
						IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
							SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting last player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " as mission entity.")
						#ENDIF

						DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MISSION_VEH)
						SET_VEHICLE_DOORS_SHUT(GET_PLAYERS_LAST_VEHICLE())
						SET_VEHICLE_ENGINE_ON(GET_PLAYERS_LAST_VEHICLE(), FALSE, TRUE)
						SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), << -867.9103, 158.2215, 63.9014 >>)
						SET_ENTITY_HEADING(GET_PLAYERS_LAST_VEHICLE(), 174.2918)
						SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
						
					ENDIF
				ENDIF
				
				CLEAR_AREA(<< -817.305, 179.330, 71.241 >>, 500.0, TRUE)
				CLEAR_AREA_OF_PEDS(<< -817.305, 179.330, 71.241 >>, 500.0)
				CLEAR_AREA_OF_COPS(<< -817.305, 179.330, 71.241 >>, 500.0)
				CLEAR_AREA_OF_VEHICLES(<< -817.305, 179.330, 71.241 >>, 500.0)
				
				iStageProgress++
			
			ENDIF
		
		BREAK
		
		CASE 4
		
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
		
			IF DOES_CAM_EXIST(ScriptedCamera)
				
				IF ( bTimelapseCutsceneSkipped = FALSE )
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iHouseEnterSceneID)
				
						IF GET_SYNCHRONIZED_SCENE_PHASE(iHouseEnterSceneID) >= 0.99
						
							IF bIntroCutsceneAssetsRequested = TRUE
							
								iStageProgress++
								
							ENDIF
							
						ENDIF
						
						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
							
							bTimelapseCutsceneSkipped = TRUE
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Pre timelapse cutscene skipped by the player.")
							#ENDIF
							
						ENDIF
					
					ENDIF

				ELSE
				
					IF IS_SCREEN_FADED_OUT()
					
						IF bIntroCutsceneAssetsRequested = TRUE
					
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							
							STOP_SYNCHRONIZED_ENTITY_ANIM(osLeftDoorDummy.ObjectIndex, 0, FALSE)
							STOP_SYNCHRONIZED_ENTITY_ANIM(osRightDoorDummy.ObjectIndex, 0, FALSE)
							
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							DESTROY_CAM(ScriptedCamera)
							DESTROY_ALL_CAMS()
							
							iStageProgress++
							
						ENDIF
					
					ENDIF

				ENDIF
				
			ENDIF
			
			REQUEST_CUTSCENE("family_5_int")											//request intro cutscene
					
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_YOGA)			//Michael's variations
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HAIR, 	4, 0)	//Amanda's variations
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_TORSO, 	1, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_LEG, 	1, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_DECL, 	1, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HEAD, 	0, 0)	//Fabien's variations
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_TORSO, 	0, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_LEG, 	0, 0)
				bIntroCutsceneAssetsRequested = TRUE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for intro cutscene peds during stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting bIntroCutsceneAssetsRequested to ", bIntroCutsceneAssetsRequested, ".")
				#ENDIF
			ENDIF
		
		BREAK
		
		CASE 5
		
			OVERRIDE_LODSCALE_THIS_FRAME(1.0) 
		
			IF ( bTimelapseCutsceneSkipped = FALSE )
		
				IF DOES_CAM_EXIST(ScriptedCamera)
				
					IF IS_NEW_LOAD_SCENE_LOADED()
					OR HAS_TIME_PASSED(10000, iLoadSceneTimer)
					
						#IF IS_DEBUG_BUILD
						
							IF HAS_TIME_PASSED(10000, iLoadSceneTimer)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to iLoadSceneTimer timeout.")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to IS_NEW_LOAD_SCENE_LOADED() returning TRUE.")
							ENDIF
						#ENDIF
						
						NEW_LOAD_SCENE_STOP()
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
						ENDIF

						RETURN TRUE
						
					ENDIF
					
				ENDIF
				
			ELSE
			
				NEW_LOAD_SCENE_STOP()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to pre timelapse cutscene being skipped.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
				ENDIF
			
				RETURN TRUE
			
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_TIMELAPSE_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0
		
			IF ( bTimelapseCutsceneSkipped = FALSE )

		        IF REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				
		            sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
					
					ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<-839.514160,180.320984,71.617966>>,<<10.805817,0.000000,-101.184196>>, 5000)
		            ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<-839.305664,180.279785,72.148422>>,<<10.330491,0.000000,-100.120872>>, 5000)
		            SET_CAM_FOV(sTimelapse.splineCamera, 40.633255)
		            SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
					
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE)
					
		            iStageProgress++
					
				ENDIF
			
			ELSE
			
				IF IS_SCREEN_FADED_OUT()
			
					iStageProgress++
					
				ENDIF
			
			ENDIF

		BREAK
		
		CASE 1
		
			INT iCurrentHour, iStartTime, iEndTime
		
			iCurrentHour = GET_CLOCK_HOURS()
					
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current time hour is ", iCurrentHour, ".")
			#ENDIF
	
			iTODSkipHour = iCurrentHour + 2
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current skip to time hour is ", iTODSkipHour, ".")
			#ENDIF
	
			GET_SP_MISSION_TOD_WINDOW_TIME(SP_MISSION_FAMILY_5, iStartTime, iEndTime)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": iStarttime is  ", iStartTime, " and iEndTime is ", iEndTime, ".")
			#ENDIF
			
			IF iTODSkipHour > iStartTime
            OR iTODSkipHour < iEndTime
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current skip to time hour is not between start and end time.")
				#ENDIF
				iTODSkipHour = IDEAL_MISSION_START_TIME
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current skip to time hour is between start and end time.")
				#ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": TOD skip to time hour is ", iTODSkipHour, ".")
			#ENDIF
			
			WARP_PED(PLAYER_PED_ID(), MissionPosition.vStageStart, MissionPosition.fStartHeading, FALSE, FALSE, FALSE)
			
			IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA)	
				SET_PLAYER_PED_OUTFIT(MO_YOGA)
			ENDIF
			
			IF IS_THIS_TV_ON(TV_LOC_MICHAEL_PROJECTOR)
				FORCE_STOP_TV(TV_LOC_MICHAEL_PROJECTOR)
			ENDIF
			
			bTimelapseTriggered = TRUE
			
			iStageProgress++
		
		BREAK
		
		CASE 2
		
			REQUEST_CUTSCENE("family_5_int")											//request intro cutscene
					
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_YOGA)			//Michael's variations
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HAIR, 	4, 0)	//Amanda's variations
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_TORSO, 	1, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_LEG, 	1, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_DECL, 	1, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HEAD, 	0, 0)	//Fabien's variations
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_TORSO, 	0, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_LEG, 	0, 0)
				bIntroCutsceneAssetsRequested = TRUE
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for intro cutscene peds during stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting bIntroCutsceneAssetsRequested to ", bIntroCutsceneAssetsRequested, ".")
				#ENDIF
			ENDIF
			
			IF ( bTimelapseCutsceneSkipped = FALSE )
		
				IF DOES_CAM_EXIST(sTimelapse.splineCamera)
				
					IF DOES_CAM_EXIST(ScriptedCamera)
						DESTROY_CAM(ScriptedCamera)
					ENDIF
					
				ENDIF

	            IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(iTODSkipHour, 0, "EXTRASUNNY", "cirrocumulus", sTimelapse, DEFAULT, 24)
				
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					IF ( bIntroCutsceneAssetsRequested = TRUE )
			
	               		iStageProgress++
						
					ENDIF
					
	            ENDIF
				
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					
					bTimelapseCutsceneSkipped = TRUE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Timelapse cutscene skipped by the player.")
					#ENDIF
					
				ENDIF
				
			ELSE
			
				IF IS_SCREEN_FADED_OUT()
				
					IF IS_AUDIO_SCENE_ACTIVE("TOD_SHIFT_SCENE")	//fix for assert B*1737456, only check the sound id when timelapse audio scene is running
						IF NOT HAS_SOUND_FINISHED(sTimelapse.iSplineStageSound)
							STOP_SOUND(sTimelapse.iSplineStageSound)
							RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
						ENDIF
					ENDIF
					
					IF ( bIntroCutsceneAssetsRequested = TRUE )
			
						iStageProgress++
						
					ENDIF
					
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 3
		
			RETURN TRUE
			
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_INTRO_COMPLETED(INT &iStageProgress)
	
	SWITCH iStageProgress
	          
      	CASE 0
		
			IF IS_THIS_TV_ON(TV_LOC_MICHAEL_PROJECTOR)
				FORCE_STOP_TV(TV_LOC_MICHAEL_PROJECTOR)
			ENDIF

          	IF  HAS_REQUESTED_CUTSCENE_LOADED("family_5_int")

           		REGISTER_ENTITY_FOR_CUTSCENE(psAmanda.PedIndex, "Amanda", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, psAmanda.ModelName)				
				REGISTER_ENTITY_FOR_CUTSCENE(psFabien.PedIndex, "Fabien", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, psFabien.ModelName)

				//Michael's yoga mat, blue
				REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[0].ObjectIndex, "Michaels_YogaMat", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osYogaMats[0].ModelName)
				
				//Fabien's yoga mat, black
				REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[1].ObjectIndex, "Fabiens_YogaMat", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osYogaMats[1].ModelName)
				
				//Amanda's yoga mat, red
				REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[2].ObjectIndex, "Amandas_yogamat", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osYogaMats[2].ModelName)

				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					//Michael's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting variations for cutscene ped Michael for the intro cutscene.")
					#ENDIF
					//Amanda's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HAIR, 	4, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_TORSO, 	1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_LEG, 	1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_DECL, 	1, 0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting variations for cutscene ped Amanda for the intro cutscene.")
					#ENDIF
					//Fabien's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HEAD, 	0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_TORSO, 	0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_LEG, 	0, 0)					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting variations for cutscene ped Fabien for the intro cutscene.")
					#ENDIF
				ENDIF
			
         	ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				IF ( bTimelapseTriggered = TRUE )
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
					SET_CLOCK_TIME(iTODSkipHour, 0, 0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting clock time after time lapse cutscene.")
					#ENDIF
				ENDIF
				
				//force weather to extra sunny so that it does not rain during yoga
				SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
				
				DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
				DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
				
				IF 	IS_REPLAY_START_VEHICLE_AVAILABLE()
				AND DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
					IF ( GET_MISSION_START_VEHICLE_INDEX() = GET_PLAYERS_LAST_VEHICLE() )
						IF 	NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
						AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE(), TRUE)
						AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
						AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
							
							STOP_VEHICLE_FIRE(GET_PLAYERS_LAST_VEHICLE())
																
							SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), << -867.9103, 158.2215, 63.9014 >>)
							SET_ENTITY_HEADING(GET_PLAYERS_LAST_VEHICLE(), 174.2918)
							
							SET_VEHICLE_DOORS_SHUT(GET_PLAYERS_LAST_VEHICLE())
							SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
							SET_VEHICLE_ENGINE_ON(GET_PLAYERS_LAST_VEHICLE(), FALSE, TRUE)

							SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<< -867.9103, 158.2215, 63.9014 >>, 174.2918)
							CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
							
						ENDIF
					ENDIF
				ENDIF
				
				CLEAR_AREA(MissionPosition.vStageStart, 5000.0, TRUE)
				
				CLEAR_PED_WETNESS(PLAYER_PED_ID())										//clear any visible damage player has before mission
				CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_TATTOOS(PLAYER_PED_ID())
				
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))	//restore player health
				
				IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA)
					SET_PLAYER_PED_OUTFIT(MO_YOGA)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), -1.2, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), 1.2, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_LOCKED, FALSE, TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(osLeftDoorDummy.ObjectIndex)
					DELETE_OBJECT(osLeftDoorDummy.ObjectIndex)
					REMOVE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName)
				ENDIF				
				
				IF DOES_ENTITY_EXIST(osRightDoorDummy.ObjectIndex)
					DELETE_OBJECT(osRightDoorDummy.ObjectIndex)
					REMOVE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				
				DESTROY_ALL_CAMS()					
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				REMOVE_ANIM_DICT("missfam2mcs_intp1")		//clear animation dictionary and camera from previous stages
				
				bDoorOpenRatioSwitched = FALSE
							
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
								
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF ( bDoorOpenRatioSwitched = FALSE )
				IF IS_CUTSCENE_PLAYING()
					IF GET_CUTSCENE_TIME() > 80650
						IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
							DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), -1.0, FALSE, FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_LOCKED, FALSE, TRUE)
						ENDIF
						
						IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
							DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), 1.0, FALSE, FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_LOCKED, FALSE, TRUE)
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Switching door open ratio.")
						#ENDIF
						
						bDoorOpenRatioSwitched = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			//create Amanda's ped during the intro mocap cutscene
			IF NOT DOES_ENTITY_EXIST(psAmanda.PedIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Amanda", psAmanda.ModelName)
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Amanda"))
					
						psAmanda.PedIndex =  GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Amanda"))
						
						SET_MISSION_PED_PROPERTIES(psAmanda.PedIndex, RELGROUPHASH_PLAYER, FALSE, TRUE, FALSE, FALSE)

						SET_FORCE_FOOTSTEP_UPDATE(psAmanda.PedIndex, TRUE)
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Set up properites for mission player ped Amanda created in the intro cutscene.")
						#ENDIF

					ENDIF
				ENDIF

			ENDIF
			
			//create Fabien's ped during the intro mocap cutscene
			IF NOT DOES_ENTITY_EXIST(psFabien.PedIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Fabien")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Fabien"))
					
						psFabien.PedIndex =  GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Fabien"))
						
						SET_MISSION_PED_PROPERTIES(psFabien.PedIndex, RELGROUPHASH_PLAYER, FALSE, TRUE, FALSE, FALSE)
						
						SET_FORCE_FOOTSTEP_UPDATE(psFabien.PedIndex, TRUE)
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Set up properites for mission player ped Fabien created in the intro cutscene.")
						#ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			
			//create Michael's yoga mat
			IF NOT DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Michaels_YogaMat")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_YogaMat"))
					
						osYogaMats[0].ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_YogaMat"))
					
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created Michael's yoga mat object in the intro cutscene.")
						#ENDIF

					ENDIF
				ENDIF
			ENDIF			
			
			//create Fabien's yoga mat
			IF NOT DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Fabiens_YogaMat")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Fabiens_YogaMat"))
					
						osYogaMats[1].ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Fabiens_YogaMat"))
					
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created Fabien's yoga mat object in the intro cutscene.")
						#ENDIF

					ENDIF
				ENDIF
			ENDIF
			
			//create Amanda's yoga mat
			IF NOT DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Amandas_yogamat")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Amandas_yogamat"))
					
						osYogaMats[2].ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Amandas_yogamat"))
					
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created Amanda's yoga mat object in the intro cutscene.")
						#ENDIF

					ENDIF
				ENDIF
			ENDIF

			//keep requesting the animation dictionary containing yoga moves animations
			REQUEST_ANIM_DICT("missfam5_yoga")
		
			//create scripted camera for yoga warrior pose during family_5_int mocap cutscene
			IF IS_CUTSCENE_PLAYING()

				IF NOT DOES_CAM_EXIST(ScriptedCamera)
			
					ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					
					SET_CAM_PARAMS(ScriptedCamera,  vWarriorStartCamPosition, vWarriorStartCamRotation, fWarriorStartCamFOV)
					
					SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.125)
					
					SET_CAM_FAR_CLIP(ScriptedCamera, 500)
					SET_CAM_FAR_CLIP(ScriptedCamera, 500)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera during family_5_int mocap cutscene.")
					#ENDIF
					
				ENDIF
			
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						WARP_PED(PLAYER_PED_ID(), << -790.906, 186.293, 71.8351 >>, 275.074219, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5_yoga", "start_pose", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0, FALSE, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
				
				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.35)
				CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(0.65)
				
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Amanda")
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						WARP_PED(psAmanda.PedIndex, vAmandaWarriorPosition, vAmandaWarriorRotation.z, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_a",
												vAmandaWarriorPosition, vAmandaWarriorRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Fabien")
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						WARP_PED(psFabien.PedIndex, vFabienWarriorPosition, vFabienWarriorRotation.z, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_a",
												vFabienWarriorPosition, vFabienWarriorRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			
				REPLAY_STOP_EVENT()
			
				//start rendering from the scripted camera
				IF DOES_CAM_EXIST(ScriptedCamera)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting rendering from scripted camera when CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
					#ENDIF
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()

				IF  HAS_MISSION_PED_BEEN_CREATED(psAmanda, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_AMANDA)
				AND HAS_MISSION_PED_BEEN_CREATED(psFabien, FALSE, RELGROUPHASH_PLAYER, FALSE, NO_CHARACTER)
				AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[0])
				AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[1])
				AND HAS_MISSION_OBJECT_BEEN_CREATED(osYogaMats[2])
				AND HAS_ANIM_DICT_LOADED("missfam5_yoga")
				AND PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")

					IF ( bCutsceneSkipped = TRUE )	//cutscene skipped
					
						IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			
							NEW_LOAD_SCENE_START(<< -782.81, 187.44, 73.33 >>, << -0.98, -0.17, -0.06 >>, 20.0)	//this is the camera postion and camera direction on the first yoga move
							
							iLoadSceneTimer = GET_GAME_TIMER()
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
							#ENDIF
							
						ENDIF
						
						IF IS_NEW_LOAD_SCENE_LOADED()
						OR HAS_TIME_PASSED(10000, iLoadSceneTimer)

							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to IS_NEW_LOAD_SCENE_LOADED() returning TRUE.")
							#ENDIF
							
							NEW_LOAD_SCENE_STOP()
									
							IF IS_SCREEN_FADED_OUT()
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							ENDIF
						
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, FALSE)
						
							RETURN TRUE
						
						ENDIF
						
					ELSE							//cutscene not skipped
					
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
						ENDIF
					
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, FALSE)
					
						RETURN TRUE
						
					ENDIF

				ENDIF
				
			ELSE

				IF NOT REQUEST_SCRIPT_AUDIO_BANK("FAM5_YOGA_01")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loading audio bank FAM5_YOGA_01")
					#ENDIF
				ENDIF
				
				IF NOT REQUEST_SCRIPT_AUDIO_BANK("FAM5_YOGA_02")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loading audio bank FAM5_YOGA_02")
					#ENDIF
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_YOGA_TRANQUIL")
					IF ( GET_CUTSCENE_TIME() > 75500 )
						START_AUDIO_SCENE("FAMILY_5_YOGA_TRANQUIL")
					ENDIF
				ENDIF
				
				IF ( bYogaMusicPlaying = FALSE )
					IF GET_CUTSCENE_TIME() > 88000
						IF TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_START")
							bYogaMusicPlaying = TRUE
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event FAM5_YOGA_MUSIC_START.")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
				
				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK

	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_MISSION_STAGE_YOGA_WARRIOR_COMPLETED(INT &iStageProgress)

	INT i

	SWITCH iStageProgress
	
		CASE 0	//create scripted camera for yoga warrior pose during gameplay
		
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
					
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)

				SET_CAM_PARAMS(ScriptedCamera, vWarriorStartCamPosition, vWarriorStartCamRotation, fWarriorStartCamFOV)
				SET_CAM_PARAMS(ScriptedCamera, vWarriorEndCamPosition, vWarriorEndCamRotation, fWarriorEndCamFOV, 25000,
							   GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
							   
			  	SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.125)
			   
			   	SET_CAM_FAR_CLIP(ScriptedCamera, 500)
				SET_CAM_FAR_CLIP(ScriptedCamera, 500)
			   
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera during yoga warrior pose gameplay.")
				#ENDIF
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			ENDIF
			
			IF DOES_CAM_EXIST(ScriptedCamera)
			
				IF NOT IS_CAM_RENDERING(ScriptedCamera)
			
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)

				ENDIF
				
				//if camera is rendering already for example from previous stage when it started rendering at the end of the mocap,
				//then just change camera params
				IF IS_CAM_RENDERING(ScriptedCamera)

					SET_CAM_PARAMS(ScriptedCamera, vWarriorStartCamPosition, vWarriorStartCamRotation, fWarriorStartCamFOV)
					SET_CAM_PARAMS(ScriptedCamera, vWarriorEndCamPosition, vWarriorEndCamRotation, fWarriorEndCamFOV, 25000,
								   GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

				ENDIF
			
			ENDIF

			RESET_YOGA_STRUCT(sYogaPlayer, TRUE, TRUE)

			IF NOT IS_PED_INJURED(psAmanda.PedIndex)
				IF NOT IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_a")
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_a",
												vAmandaWarriorPosition, vAmandaWarriorRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF NOT IS_PED_INJURED(psFabien.PedIndex)
				IF NOT IS_ENTITY_PLAYING_ANIM(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_a")
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_a",
												vFabienWarriorPosition, vFabienWarriorRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			//freeze yoga mats in case they get pushed around
			FOR i = 0 TO (COUNT_OF(osYogaMats) - 1 )
				IF DOES_ENTITY_EXIST(osYogaMats[i].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[i].ObjectIndex)
						FREEZE_ENTITY_POSITION(osYogaMats[i].ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDFOR
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			
			IF PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
				TRIGGER_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
			ENDIF
			
			iStageProgress++

		BREAK
		
		CASE 1
			
			//play opening conversation
			IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_WARB")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_WARBF")
				
				IF ( bStageReplayInProgress = FALSE )
				
					IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_WARB", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM5_WARB", TRUE)
					ENDIF
					
				ELSE
				
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_WARBF", "FAM5_WARBF_1", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM5_WARBF", TRUE)
					ENDIF
				
				ENDIF

			ELSE

				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
				
			MAKE_PED_LOOK_AT_ENTITY_WITH_INTERVAL(psAmanda.PedIndex, PLAYER_PED_ID(), psAmanda.bLookingAtTarget, psAmanda.iLookAtTimer,
												  psAmanda.iTimer, psAmanda.iLookAtTime, psAmanda.iIntervalTime, 3000, 6000, 2000, 7500)
												  
			MAKE_PED_LOOK_AT_ENTITY_WITH_INTERVAL(psFabien.PedIndex, PLAYER_PED_ID(), psFabien.bLookingAtTarget, psFabien.iLookAtTimer,
												  psFabien.iTimer, psFabien.iLookAtTime, psFabien.iIntervalTime, 3750, 7500, 3000, 8000)
			
			IF ( sYogaPlayer.eYogaMinigameStage = YOGAMINIGAMESTAGE_FAILRESET )
				IF ( iFailConversationSpeaker = -1 ) 
					iFailConversationSpeaker = GET_RANDOM_INT_IN_RANGE(0, 2)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": iFailConversationSpeaker is ", iFailConversationSpeaker, ".")
					#ENDIF
					
				ENDIF
			ELSE
				iFailConversationSpeaker = -1
			ENDIF
			
			UPDATE_YOGA_TIMECYCLE_MODIFIER(sYogaPlayer, TRUE)
			RUN_YOGA_FAIL_CAMERA(sYogaPlayer, iFailConversationSpeaker)

			MANAGE_YOGA_CONVERSATIONS(sYogaPlayer, iYogaPosesCounter, iYogaBreathsCounter, iYogaIdleTimer, iYogaFailsCounter,
									  "FAM5_WARIA", "FAM5_WARIF", "FAM5_WARPA", "FAM5_WARPF", "FAM5_WARE",
									  bBreatheConversationTriggered, "FAM5_WARFB",
									  bBreatheCompletedConversationTriggered, "FAM5_WARBC",
									  bBreatheFailedConversationTriggered, "FAM5_WARBTF",
									  bInhaleConversationTriggered, "FAM5_WARIN",
									  bExhaleConversationTriggered, "FAM5_WAREX",
									  bFailConversationTriggered, "FAM5_WARDA", "FAM5_WARDF", "FAM5_WARF", 2)

			IF HAS_PLAYER_COMPLETED_YOGA_MOVE(sYogaPlayer, YOGAMOVE_WARRIOR, FALSE, TRUE, TRUE)	
				
				//Play Michael's success conversation				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_WARS")
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_WARS", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_WARS", TRUE)
						ENDIF
					ENDIF

				ELSE
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
						//if player didn't fail warrior even one time, set the warrior stat to completed
						IF ( sYogaPlayer.iFailCounter = 0 )
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FAM5_WARRIOR_POSE) 
						ENDIF
					
						RESET_YOGA_STRUCT(sYogaPlayer, FALSE, FALSE)
						
						RESET_ALL_SFX_FLAGS(ssSounds)
						
						IF NOT IS_PED_INJURED(psAmanda.PedIndex)
							TASK_CLEAR_LOOK_AT(psAmanda.PedIndex)
							psAmanda.bLookingAtTarget = FALSE
						ENDIF
						
						IF NOT IS_PED_INJURED(psFabien.PedIndex)
							TASK_CLEAR_LOOK_AT(psFabien.PedIndex)
							psFabien.bLookingAtTarget = FALSE
						ENDIF
						
						//unfreeze yoga mats in case they get pushed around
						FOR i = 0 TO (COUNT_OF(osYogaMats) - 1 )
							IF DOES_ENTITY_EXIST(osYogaMats[i].ObjectIndex)
								IF NOT IS_ENTITY_DEAD(osYogaMats[i].ObjectIndex)
									FREEZE_ENTITY_POSITION(osYogaMats[i].ObjectIndex, FALSE)
								ENDIF
							ENDIF
						ENDFOR
						
						REPLAY_RECORD_BACK_FOR_TIME(5, 3, REPLAY_IMPORTANCE_HIGHEST)
						
						RETURN TRUE
						
					ENDIF
					
				ENDIF

			ENDIF
			
			//request subsequent mocap cutscene
			REQUEST_CUTSCENE("family_5_mcs_1")

			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Amanda", psAmanda.PedIndex)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Fabien", psFabien.PedIndex)
			ENDIF
			

		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_TRIANGLE_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
			
		CASE 0
			
			IF HAS_REQUESTED_CUTSCENE_LOADED("family_5_mcs_1")
				
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psAmanda.PedIndex, "Amanda", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psFabien.PedIndex, "Fabien", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				//Michael's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[0].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[0].ObjectIndex, "Michaels_YogaMat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[0].ModelName)
					ENDIF
				ENDIF
				
				//Fabien's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[1].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[1].ObjectIndex, "Fabiens_YogaMat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[1].ModelName)
					ENDIF
				ENDIF
				
				//Amanda's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[2].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[2].ObjectIndex, "Amandas_yogamat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[2].ModelName)
					ENDIF
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_CHANGE")
				
				START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					//Michael's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					//Amanda's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HAIR, 	4, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_TORSO, 	1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_LEG, 	1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_DECL, 	1, 0)
					//Fabien's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HEAD, 	0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_TORSO, 	0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_LEG, 	0, 0)	
				ENDIF
				
			ENDIF
	
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			//keep requesting the animation dictionary containing yoga moves animations
			REQUEST_ANIM_DICT("missfam5_yoga")
		
			//create scripted camera for yoga triangle pose during family_5_mcs_1 mocap cutscene
			IF IS_CUTSCENE_PLAYING()

				IF NOT DOES_CAM_EXIST(ScriptedCamera)
			
					ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					
					SET_CAM_PARAMS(ScriptedCamera, vTriangleStartCamPosition, vTriangleStartCamRotation, fTriangleStartCamFOV)
					
					SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.125)
					
					SET_CAM_FAR_CLIP(ScriptedCamera, 500)
					SET_CAM_FAR_CLIP(ScriptedCamera, 500)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera during family_5_mcs_1 mocap cutscene.")
					#ENDIF

				ELIF DOES_CAM_EXIST(ScriptedCamera)

					SET_CAM_PARAMS(ScriptedCamera,  vTriangleStartCamPosition, vTriangleStartCamRotation, fTriangleStartCamFOV)

				ENDIF
			
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5_yoga", "start_pose", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0, FALSE, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF

				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.35)
				CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(0.65)

			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Amanda")
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						WARP_PED(psAmanda.PedIndex, vAmandaTrianglePosition, vAmandaTriangleRotation.z, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_b",
												vAmandaTrianglePosition, vAmandaTriangleRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Fabien")
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						WARP_PED(psFabien.PedIndex, vFabienTrianglePosition, vFabienTriangleRotation.z, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_b",
												vFabienTrianglePosition, vFabienTriangleRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				//start rendering from the scripted camera
				IF DOES_CAM_EXIST(ScriptedCamera)
					SET_CAM_PARAMS(ScriptedCamera,  vTriangleStartCamPosition, vTriangleStartCamRotation, fTriangleStartCamFOV)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)				
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
				
				IF PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
				
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, FALSE)
				
					REPLAY_STOP_EVENT()
				
					RETURN TRUE
				
				ENDIF

			ELSE
			
				PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
				
				IF ( bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_YOGA_TRIANGLE_COMPLETED(INT &iStageProgress)

	INT i

	SWITCH iStageProgress
	
		CASE 0	//create scripted camera for yoga warrior pose during gameplay
		
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
					
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)

				SET_CAM_PARAMS(ScriptedCamera, vTriangleStartCamPosition, vTriangleStartCamRotation, fTriangleStartCamFOV)
				SET_CAM_PARAMS(ScriptedCamera, vTriangleEndCamPosition, vTriangleEndCamRotation, fTriangleEndCamFOV, 25000,
							   GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
							   
			  	SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.125)
			   
			   	SET_CAM_FAR_CLIP(ScriptedCamera, 500)
				SET_CAM_FAR_CLIP(ScriptedCamera, 500)
			   
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera during yoga triangle pose gameplay.")
				#ENDIF
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			ENDIF
			
			IF DOES_CAM_EXIST(ScriptedCamera)
			
				IF NOT IS_CAM_RENDERING(ScriptedCamera)
			
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)

				ENDIF
				
				//if camera is rendering already for example from previous stage when it started rendering at the end of the mocap,
				//then just change camera params
				IF IS_CAM_RENDERING(ScriptedCamera)

					SET_CAM_PARAMS(ScriptedCamera, vTriangleStartCamPosition, vTriangleStartCamRotation, fTriangleStartCamFOV)
					SET_CAM_PARAMS(ScriptedCamera, vTriangleEndCamPosition, vTriangleEndCamRotation, fTriangleEndCamFOV, 25000,
								   GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

				ENDIF
			
			ENDIF

			RESET_YOGA_STRUCT(sYogaPlayer, FALSE, FALSE)
			
			IF NOT IS_PED_INJURED(psAmanda.PedIndex)
				IF NOT IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_b")
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_b",
												vAmandaTrianglePosition, vAmandaTriangleRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF NOT IS_PED_INJURED(psFabien.PedIndex)
				IF NOT IS_ENTITY_PLAYING_ANIM(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_b")
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_b",
												vFabienTrianglePosition, vFabienTriangleRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			//freeze yoga mats in case they get pushed around
			FOR i = 0 TO (COUNT_OF(osYogaMats) - 1 )
				IF DOES_ENTITY_EXIST(osYogaMats[i].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[i].ObjectIndex)
						FREEZE_ENTITY_POSITION(osYogaMats[i].ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDFOR
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			
			IF PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
				TRIGGER_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
			ENDIF
			
			iStageProgress++

		BREAK
	
		CASE 1

			//play opening conversation
			IF 	NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_TRIB")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_TRIBF")
			
				IF ( bStageReplayInProgress = FALSE )
			
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_TRIB", "FAM5_TRIB_1", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM5_TRIB", TRUE)
					ENDIF
				
				ELSE
				
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_TRIBF", "FAM5_TRIBF_3", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM5_TRIBF", TRUE)
					ENDIF

				ENDIF
			
			ELSE
				iStageProgress++
			ENDIF
			
		BREAK
		
		CASE 2
		
			MAKE_PED_LOOK_AT_ENTITY_WITH_INTERVAL(psAmanda.PedIndex, PLAYER_PED_ID(), psAmanda.bLookingAtTarget, psAmanda.iLookAtTimer,
												  psAmanda.iTimer, psAmanda.iLookAtTime, psAmanda.iIntervalTime, 3000, 6000, 2000, 7500)
												  
			MAKE_PED_LOOK_AT_ENTITY_WITH_INTERVAL(psFabien.PedIndex, PLAYER_PED_ID(), psFabien.bLookingAtTarget, psFabien.iLookAtTimer,
												  psFabien.iTimer, psFabien.iLookAtTime, psFabien.iIntervalTime, 3750, 7500, 3000, 8000)
		
			IF ( sYogaPlayer.eYogaMinigameStage = YOGAMINIGAMESTAGE_FAILRESET )
				IF ( iFailConversationSpeaker = -1 ) 
					iFailConversationSpeaker = GET_RANDOM_INT_IN_RANGE(0, 2)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": iFailConversationSpeaker is ", iFailConversationSpeaker, ".")
					#ENDIF
					
				ENDIF
			ELSE
				iFailConversationSpeaker = -1
			ENDIF
			
			UPDATE_YOGA_TIMECYCLE_MODIFIER(sYogaPlayer, TRUE)
			RUN_YOGA_FAIL_CAMERA(sYogaPlayer, iFailConversationSpeaker)
		
			MANAGE_YOGA_CONVERSATIONS(sYogaPlayer, iYogaPosesCounter, iYogaBreathsCounter, iYogaIdleTimer, iYogaFailsCounter,
									  "FAM5_TRIIA", "FAM5_TRIIF", "FAM5_TRIPA", "FAM5_TRIPF", "FAM5_TRIE",
									  bBreatheConversationTriggered, "FAM5_TRIFB",
									  bBreatheCompletedConversationTriggered, "FAM5_TRIBC",
									  bBreatheFailedConversationTriggered, "FAM5_TRIBTF",
									  bInhaleConversationTriggered, "FAM5_WARIN",
									  bExhaleConversationTriggered, "FAM5_WAREX",
									  bFailConversationTriggered, "FAM5_TRIDA", "FAM5_TRIDF", "FAM5_TRIF", 4)

			IF HAS_PLAYER_COMPLETED_YOGA_MOVE(sYogaPlayer, YOGAMOVE_TRIANGLE, FALSE, bStageReplayInProgress, FALSE)
		
				//Play Michael's success conversation
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_TRIS")

					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_TRIS", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_TRIS", TRUE)
						ENDIF
					ENDIF
				
				ELSE
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
						//if player didn't fail traingle even one time set the traingle stat to completed
						IF ( sYogaPlayer.iFailCounter = 0 )
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FAM5_TRIANGLE_POSE) 
						ENDIF
					
						RESET_YOGA_STRUCT(sYogaPlayer, FALSE, FALSE)
						
						RESET_ALL_SFX_FLAGS(ssSounds)

						IF NOT IS_PED_INJURED(psAmanda.PedIndex)
							TASK_CLEAR_LOOK_AT(psAmanda.PedIndex)
							psAmanda.bLookingAtTarget = FALSE
						ENDIF
						
						IF NOT IS_PED_INJURED(psFabien.PedIndex)
							TASK_CLEAR_LOOK_AT(psFabien.PedIndex)
							psFabien.bLookingAtTarget = FALSE
						ENDIF
						
						//unfreeze yoga mats in case they get pushed around
						FOR i = 0 TO (COUNT_OF(osYogaMats) - 1 )
							IF DOES_ENTITY_EXIST(osYogaMats[i].ObjectIndex)
								IF NOT IS_ENTITY_DEAD(osYogaMats[i].ObjectIndex)
									FREEZE_ENTITY_POSITION(osYogaMats[i].ObjectIndex, FALSE)
								ENDIF
							ENDIF
						ENDFOR
						
						REPLAY_RECORD_BACK_FOR_TIME(5, 4, REPLAY_IMPORTANCE_HIGHEST)

						RETURN TRUE
					
					ENDIF
					
				ENDIF
		
			ENDIF
		
			//request mocap cutscene
			REQUEST_CUTSCENE("family_5_mcs_2")

			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Amanda", psAmanda.PedIndex)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Fabien", psFabien.PedIndex)
			ENDIF

		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_SUNSALUTATION_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("family_5_mcs_2")
				
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psAmanda.PedIndex, "Amanda", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psFabien.PedIndex, "Fabien", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				//Michael's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[0].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[0].ObjectIndex, "Michaels_YogaMat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[0].ModelName)
					ENDIF
				ENDIF
				
				//Fabien's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[1].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[1].ObjectIndex, "Fabiens_YogaMat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[1].ModelName)
					ENDIF
				ENDIF
				
				//Amanda's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[2].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[2].ObjectIndex, "Amandas_yogamat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[2].ModelName)
					ENDIF
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					//Michael's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					//Amanda's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HAIR, 	4, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_TORSO, 	1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_LEG, 	1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_DECL, 	1, 0)
					//Fabien's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HEAD, 	0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_TORSO, 	0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_LEG, 	0, 0)
				ENDIF
					
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
	
			//keep requesting the animation dictionary containing yoga moves animations
			REQUEST_ANIM_DICT("missfam5_yoga")
		
			//create scripted camera for yoga warrior pose during family_5_mcs_2 mocap cutscene
			IF IS_CUTSCENE_PLAYING()

				IF NOT DOES_CAM_EXIST(ScriptedCamera)
			
					ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					
					SET_CAM_PARAMS(ScriptedCamera, vSunSalutaionStartCamPosition, vSunSalutaionStartCamRotation, fSunSalutaionStartCamFOV)
					
					SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.125)
					
					SET_CAM_FAR_CLIP(ScriptedCamera, 500)
					SET_CAM_FAR_CLIP(ScriptedCamera, 500)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera during family_5_mcs_2 mocap cutscene.")
					#ENDIF

				ELIF DOES_CAM_EXIST(ScriptedCamera)

					SET_CAM_PARAMS(ScriptedCamera, vSunSalutaionStartCamPosition, vSunSalutaionStartCamRotation, fSunSalutaionStartCamFOV)

				ENDIF
			
			ENDIF
	
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						IF ( bCutsceneSkipped = TRUE )
							WARP_PED(PLAYER_PED_ID(), << -790.906, 186.293, 71.8351 >>, 275.074219, FALSE, FALSE, FALSE)
						ENDIF
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5_yoga", "start_pose", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0, FALSE, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
				
				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.35)
				CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(0.65)
				
			ENDIF
			
			/*	//this is a better yoga mat postion when doing seamless exit into gameplay, avoids michael's hands clipping into the mat
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_YogaMat")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ":CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michaels_YogaMat.")
				#ENDIF
				IF ( bCutsceneSkipped = FALSE )
					IF DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osYogaMats[0].ObjectIndex)
							SET_ENTITY_COORDS_NO_OFFSET(osYogaMats[0].ObjectIndex, << -791.530, 186.375, 71.821 >>)
							SET_ENTITY_ROTATION(osYogaMats[0].ObjectIndex, << 0.0, 0.0, 0.0 >>)
							FREEZE_ENTITY_POSITION(osYogaMats[0].ObjectIndex, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			*/
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Amanda")
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						WARP_PED(psAmanda.PedIndex, vAmandaSunsalutationPosition, vAmandaSunsalutationRotation.z, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_c",
												vAmandaSunsalutationPosition, vAmandaSunsalutationRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Fabien")
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						WARP_PED(psFabien.PedIndex, vFabienSunsalutationPosition, vFabienSunsalutationRotation.z, FALSE, FALSE, FALSE)
						TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_c",
												vFabienSunsalutationPosition, vFabienSunsalutationRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				//start rendering from the scripted camera
				IF DOES_CAM_EXIST(ScriptedCamera)
					SET_CAM_PARAMS(ScriptedCamera, vSunSalutaionStartCamPosition, vSunSalutaionStartCamRotation, fSunSalutaionStartCamFOV)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)				
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
			
				IF PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
				
					IF IS_SCREEN_FADED_OUT()
						IF ( bCutsceneSkipped = TRUE )
							WAIT(500)	//do a short wait before fading in, this hides a pop on michael on cutscene skip
						ENDIF
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, FALSE)
					
					REPLAY_STOP_EVENT()
					
					RETURN TRUE
				
				ENDIF
				
			ELSE
			
				PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
				
				IF ( bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
							
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_YOGA_SUNSALUTATION_COMPLETED(INT &iStageProgress)

	INT i

	SWITCH iStageProgress
		
		CASE 0	//create scripted camera for yoga warrior pose during gameplay
		
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
					
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)

				SET_CAM_PARAMS(ScriptedCamera, vSunSalutaionStartCamPosition, vSunSalutaionStartCamRotation, fSunSalutaionStartCamFOV)
				SET_CAM_PARAMS(ScriptedCamera, vSunSalutaionEndCamPosition, vSunSalutaionEndCamRotation, fSunSalutaionEndCamFOV, 25000,
							   GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
							   
			  	SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.125)
			   
			   	SET_CAM_FAR_CLIP(ScriptedCamera, 500)
				SET_CAM_FAR_CLIP(ScriptedCamera, 500)
			   
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera during yoga sun salutation pose gameplay.")
				#ENDIF
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			ENDIF
			
			IF DOES_CAM_EXIST(ScriptedCamera)
			
				IF NOT IS_CAM_RENDERING(ScriptedCamera)
			
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)

				ENDIF
				
				//if camera is rendering already for example from previous stage when it started rendering at the end of the mocap,
				//then just change camera params
				IF IS_CAM_RENDERING(ScriptedCamera)

					SET_CAM_PARAMS(ScriptedCamera, vSunSalutaionStartCamPosition, vSunSalutaionStartCamRotation, fSunSalutaionStartCamFOV)
					SET_CAM_PARAMS(ScriptedCamera, vSunSalutaionEndCamPosition, vSunSalutaionEndCamRotation, fSunSalutaionEndCamFOV, 25000,
								   GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)

				ENDIF
			
			ENDIF

			RESET_YOGA_STRUCT(sYogaPlayer, FALSE, FALSE)
			
			IF NOT IS_PED_INJURED(psAmanda.PedIndex)
				IF NOT IS_ENTITY_PLAYING_ANIM(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_c")
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						TASK_PLAY_ANIM_ADVANCED(psAmanda.PedIndex, "missfam5_yoga", "f_yogapose_c",
												vAmandaSunsalutationPosition, vAmandaSunsalutationRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psAmanda.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			IF NOT IS_PED_INJURED(psFabien.PedIndex)
				IF NOT IS_ENTITY_PLAYING_ANIM(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_c")
					IF HAS_ANIM_DICT_LOADED("missfam5_yoga")
						TASK_PLAY_ANIM_ADVANCED(psFabien.PedIndex, "missfam5_yoga", "i_yogapose_c",
												vFabienSunsalutationPosition, vFabienSunsalutationRotation, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
												AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_OVERRIDE_PHYSICS,
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(psFabien.PedIndex)
					ENDIF
				ENDIF			
			ENDIF
			
			//freeze yoga mats in case they get pushed around
			FOR i = 0 TO (COUNT_OF(osYogaMats) - 1 )
				IF DOES_ENTITY_EXIST(osYogaMats[i].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[i].ObjectIndex)
						FREEZE_ENTITY_POSITION(osYogaMats[i].ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDFOR
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			
			IF PREPARE_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
				TRIGGER_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
			ENDIF
			
			iStageProgress++

		BREAK

		CASE 1

			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SUNBF")

				IF ( bStageReplayInProgress = FALSE ) 

					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SUNBF", "FAM5_SUNBF_4", CONV_PRIORITY_VERY_HIGH)
						SET_LABEL_AS_TRIGGERED("FAM5_SUNBF", TRUE)
					ENDIF
					
				ELSE
				
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SUNBF", "FAM5_SUNBF_3", CONV_PRIORITY_VERY_HIGH)
						SET_LABEL_AS_TRIGGERED("FAM5_SUNBF", TRUE)
					ENDIF
				
				ENDIF
			
			ELSE
				iStageProgress++
			ENDIF
			
		BREAK
		
		CASE 2
		
			MAKE_PED_LOOK_AT_ENTITY_WITH_INTERVAL(psAmanda.PedIndex, PLAYER_PED_ID(), psAmanda.bLookingAtTarget, psAmanda.iLookAtTimer,
												  psAmanda.iTimer, psAmanda.iLookAtTime, psAmanda.iIntervalTime, 3000, 6000, 2000, 7500)
												  
			MAKE_PED_LOOK_AT_ENTITY_WITH_INTERVAL(psFabien.PedIndex, PLAYER_PED_ID(), psFabien.bLookingAtTarget, psFabien.iLookAtTimer,
												  psFabien.iTimer, psFabien.iLookAtTime, psFabien.iIntervalTime, 3750, 7500, 3000, 8000)
			
			IF ( sYogaPlayer.eYogaMinigameStage = YOGAMINIGAMESTAGE_FAILRESET )
				IF ( iFailConversationSpeaker = -1 ) 
					iFailConversationSpeaker = GET_RANDOM_INT_IN_RANGE(0, 2)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": iFailConversationSpeaker is ", iFailConversationSpeaker, ".")
					#ENDIF
					
				ENDIF
			ELSE
				iFailConversationSpeaker = -1
			ENDIF
			
			UPDATE_YOGA_TIMECYCLE_MODIFIER(sYogaPlayer, TRUE)
			RUN_YOGA_FAIL_CAMERA(sYogaPlayer, iFailConversationSpeaker)
			
			MANAGE_YOGA_CONVERSATIONS(sYogaPlayer, iYogaPosesCounter, iYogaBreathsCounter, iYogaIdleTimer, iYogaFailsCounter,
									  "FAM5_SUNIA", "FAM5_SUNIF", "FAM5_SUNPA", "FAM5_SUNPF", "FAM5_SUNE",
									  bBreatheConversationTriggered, "FAM5_SUNFB",
									  bBreatheCompletedConversationTriggered, "FAM5_SUNBC",
									  bBreatheFailedConversationTriggered, "FAM5_SUNBTF",
									  bInhaleConversationTriggered, "FAM5_WARIN",
									  bExhaleConversationTriggered, "FAM5_WAREX",
									  bFailConversationTriggered, "FAM5_SUNDA", "FAM5_SUNDF", "FAM5_SUNF", 5)
			
			IF HAS_PLAYER_COMPLETED_YOGA_MOVE(sYogaPlayer, YOGAMOVE_SUNSALUTATION, FALSE, bStageReplayInProgress, FALSE)

				//Play Michael's success conversation
				//IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SUNS")

					//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//	IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SUNS", CONV_PRIORITY_MEDIUM)	
					//		SET_LABEL_AS_TRIGGERED("FAM5_SUNS", TRUE)
					//	ENDIF
					//ENDIF
				
				//ELSE
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						//if player didn't fail sun salutation even one time set the sun salutation stat to completed
						IF ( sYogaPlayer.iFailCounter = 0 )
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FAM5_SUN_SALUTATION_POSE) 
						ENDIF
					
						RESET_YOGA_STRUCT(sYogaPlayer, FALSE, FALSE)
						
						RESET_ALL_SFX_FLAGS(ssSounds)
						
						IF NOT IS_PED_INJURED(psAmanda.PedIndex)
							TASK_CLEAR_LOOK_AT(psAmanda.PedIndex)
							psAmanda.bLookingAtTarget = FALSE
						ENDIF
						
						IF NOT IS_PED_INJURED(psFabien.PedIndex)
							TASK_CLEAR_LOOK_AT(psFabien.PedIndex)
							psFabien.bLookingAtTarget = FALSE
						ENDIF
						
						//freeze yoga mats in case they get pushed around
						FOR i = 0 TO (COUNT_OF(osYogaMats) - 1 )
							IF DOES_ENTITY_EXIST(osYogaMats[i].ObjectIndex)
								IF NOT IS_ENTITY_DEAD(osYogaMats[i].ObjectIndex)
									FREEZE_ENTITY_POSITION(osYogaMats[i].ObjectIndex, FALSE)
								ENDIF
							ENDIF
						ENDFOR
						
						REPLAY_RECORD_BACK_FOR_TIME(5, 4, REPLAY_IMPORTANCE_HIGHEST)
						
						RETURN TRUE
						
					ENDIF
					
				//ENDIF
				
			ENDIF

			//request mocap cutscene
			REQUEST_CUTSCENE("family_5_mcs_3")

			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Amanda", psAmanda.PedIndex)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Fabien", psFabien.PedIndex)
			ENDIF

		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_POOL_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF HAS_REQUESTED_CUTSCENE_LOADED("family_5_mcs_3")
			
				IF NOT IS_PED_INJURED(psAmanda.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psAmanda.PedIndex, "Amanda", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(psFabien.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psFabien.PedIndex, "Fabien", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				//Michael's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[0].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[0].ObjectIndex, "Michaels_YogaMat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[0].ModelName)
					ENDIF
				ENDIF
				
				//Fabien's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[1].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[1].ObjectIndex, "Fabiens_YogaMat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[1].ModelName)
					ENDIF
				ENDIF
				
				//Amanda's yoga mat
				IF DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osYogaMats[2].ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osYogaMats[2].ObjectIndex, "Amandas_yogamat", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osYogaMats[2].ModelName)
					ENDIF
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_CHANGE_DOWN")
				
				STOP_AUDIO_SCENE("FAMILY_5_YOGA_MG")
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					//Michael's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					//Amanda's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_HAIR, 	4, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_TORSO, 	1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_LEG, 	1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", PED_COMP_DECL, 	1, 0)
					//Fabien's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_HEAD, 	0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_TORSO, 	0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Fabien", PED_COMP_LEG, 	0, 0)	
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				REMOVE_ANIM_DICT("missfam5_yoga")
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
				//destroy scripted camera after yoga section
				IF DOES_CAM_EXIST(ScriptedCamera)
					SET_CAM_ACTIVE(ScriptedCamera, FALSE)
					DESTROY_CAM(ScriptedCamera)	
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Destroying scripted camera.")
					#ENDIF
				ENDIF
				
				DESTROY_ALL_CAMS(TRUE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Destroying all scripted cameras.")
				#ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF IS_CUTSCENE_PLAYING()
				IF ( GET_CUTSCENE_TIME() > 33000 )						//when player ped hits the water in the cutscene
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_WETNESS_HEIGHT(PLAYER_PED_ID(), 2.0)	//keep setting the wetness every frame until cutscene is finished
					ENDIF
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					//play animation after exiting the mocap cutscene
					IF HAS_ANIM_DICT_LOADED("missfam5_wet_walk")
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5_wet_walk", "wet_idle", WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_DEFAULT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Giving player ped task to play animation 'wet_idle' when CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary 'missfam5_wet_walk' is not loaded.")
						#ENDIF
					ENDIF
					SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
				ENDIF

			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_CAMERA() returned true.")
				#ENDIF
			ENDIF
			
			IF ( bYogaMusicPlaying = TRUE )
				IF ( GET_CUTSCENE_TIME() > 33000 )
					IF TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_ENDS")
						bYogaMusicPlaying = FALSE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event FAM5_YOGA_MUSIC_ENDS.")
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping yoga music.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF ( GET_CUTSCENE_TIME() > 33000 )
				
					UPDATE_YOGA_TIMECYCLE_MODIFIER(sYogaPlayer, FALSE)
					
					IF ( sYogaPlayer.fBlushDecalAlpha != 0.0 )
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_DAMAGE_DECAL_BY_ZONE(PLAYER_PED_ID(), PDZ_HEAD, "blushing")	//clear blushing decal
							sYogaPlayer.fBlushDecalAlpha = 0.0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
						
			IF HAS_CUTSCENE_FINISHED()
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)

				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				STOP_AUDIO_SCENE("FAMILY_5_YOGA_TRANQUIL")
				
				START_AUDIO_SCENE("FAMILY_5_GO_TO_JIMMY")
				
				CLEAR_TIMECYCLE_MODIFIER()
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_DAMAGE_DECAL_BY_ZONE(PLAYER_PED_ID(), PDZ_HEAD, "blushing")			//clear blushing decal
				ENDIF
				
				REPLAY_STOP_EVENT()
				
				RETURN TRUE
				
			ELSE
				
				//keep requesting animation dictionary
				REQUEST_ANIM_DICT("missfam5_wet_walk")
				
				//keep requesting assisted movement routes needed after the cutscene
				ASSISTED_MOVEMENT_REQUEST_ROUTE("family5b")
				ASSISTED_MOVEMENT_REQUEST_ROUTE("mansion_1")
				ASSISTED_MOVEMENT_REQUEST_ROUTE("family5c")
				ASSISTED_MOVEMENT_REQUEST_ROUTE("f5_jimmy1")
					
			ENDIF
				
		BREAK

	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_DURING_GOTO_JIMMYS_ROOM()

	IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SON1A")
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.520630,187.521057,71.105469>>, <<-795.681763,177.692230,74.834709>>, 6.0)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.700134,185.895218,71.105469>>, <<-800.207031,176.436371,74.834709>>, 4.0)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.559814,168.317978,71.334709>>, <<-805.525635,178.113297,74.834709>>, 8.0)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SON1A", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM5_SON1A", TRUE)
					ENDIF
				ELSE
					IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "FAM5_ATAA", "MICHAEL", SPEECH_PARAMS_FORCE)
						SET_LABEL_AS_TRIGGERED("FAM5_SON1A", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SON1B")
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.901978,185.396698,73.504105>>, <<-804.658386,179.531998,78.740730>>, 2.5)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SON1B", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM5_SON1B", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
//	IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SON1C")	
//		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.901978,185.396698,73.504105>>, <<-804.658386,179.531998,78.740730>>, 2.5)
//		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5)
//		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-813.023193,181.754562,70.653091>>, <<-810.363708,182.782074,74.807320>>, 2.5)
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SON1C", CONV_PRIORITY_MEDIUM)
//					SET_LABEL_AS_TRIGGERED("FAM5_SON1C", TRUE)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SON1D")
		IF HAS_LABEL_BEEN_TRIGGERED("FAM5_SON1B")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SON1D", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("FAM5_SON1D", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_JROOM1")
		IF HAS_LABEL_BEEN_TRIGGERED("FAM5_SON1D")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.901978,185.396698,73.504105>>, <<-804.658386,179.531998,78.740730>>, 2.5)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.397156,179.202255,75.240730>>, <<-809.928223,175.562454,78.740730>>, 4.0)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.150208,172.722031,75.740730>>, <<-806.707092,174.138565,78.740730>>, 2.0)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_JROOM1", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_JROOM1", TRUE)
							SET_LABEL_AS_TRIGGERED("FAM5_SON1A", TRUE)
							SET_LABEL_AS_TRIGGERED("FAM5_SON1B", TRUE)
							SET_LABEL_AS_TRIGGERED("FAM5_SON1C", TRUE)
							SET_LABEL_AS_TRIGGERED("FAM5_SON1D", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_THIS_CONVERSATION_PLAYING("FAM5_JROOM1")
			IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			
				IF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.901978,185.396698,73.504105>>, <<-804.658386,179.531998,78.740730>>, 2.5)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.397156,179.202255,75.240730>>, <<-809.928223,175.562454,78.740730>>, 4.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.150208,172.722031,75.740730>>, <<-806.707092,174.138565,78.740730>>, 2.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.520630,187.521057,71.105469>>, <<-795.681763,177.692230,74.834709>>, 6.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.700134,185.895218,71.105469>>, <<-800.207031,176.436371,74.834709>>, 4.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.559814,168.317978,71.334709>>, <<-805.525635,178.113297,74.834709>>, 8.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.917725,176.369095,70.834709>>, <<-808.785706,178.632828,74.653091>>, 2.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.726013,176.068436,75.240730>>, <<-799.839966,168.956497,78.740730>>, 3.5)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.094360,180.614655,75.240730>>, <<-816.723267,177.806808,78.862457>>, 8.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-810.866333,175.499969,75.240730>>, <<-814.803284,173.922989,78.740730>>, 3.6)
				AND NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-802.712769,167.797989,75.240730>>, <<-805.571716,175.191330,78.740730>>, 3.0)
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				ENDIF
			
			ELSE
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.901978,185.396698,73.504105>>, <<-804.658386,179.531998,78.740730>>, 2.5)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.397156,179.202255,75.240730>>, <<-809.928223,175.562454,78.740730>>, 4.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.150208,172.722031,75.740730>>, <<-806.707092,174.138565,78.740730>>, 2.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.520630,187.521057,71.105469>>, <<-795.681763,177.692230,74.834709>>, 6.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.700134,185.895218,71.105469>>, <<-800.207031,176.436371,74.834709>>, 4.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.559814,168.317978,71.334709>>, <<-805.525635,178.113297,74.834709>>, 8.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.917725,176.369095,70.834709>>, <<-808.785706,178.632828,74.653091>>, 2.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.726013,176.068436,75.240730>>, <<-799.839966,168.956497,78.740730>>, 3.5)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.094360,180.614655,75.240730>>, <<-816.723267,177.806808,78.862457>>, 8.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-810.866333,175.499969,75.240730>>, <<-814.803284,173.922989,78.740730>>, 3.6)
				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-802.712769,167.797989,75.240730>>, <<-805.571716,175.191330,78.740730>>, 3.0)
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_JROOM2")
		IF HAS_LABEL_BEEN_TRIGGERED("FAM5_JROOM1")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.150208,172.722031,75.740730>>, <<-806.707092,174.138565,78.740730>>, 2.0)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_JROOM2", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_JROOM2", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_THIS_CONVERSATION_PLAYING("FAM5_JROOM2")
			bConversationsFinished = TRUE
		ENDIF
	ENDIF
		
ENDPROC

PROC MANAGE_ANIMATIONS_DURING_GOTO_JIMMYS_ROOM(INT &iProgress)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF ( bLookAtActive = FALSE )
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), psJimmy.PedIndex, -1, SLF_DEFAULT)
					bLookAtActive = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				bLookAtActive = FALSE
			ENDIF
		ENDIF
	
		SWITCH iProgress
		
			CASE 0
			
				IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_wet_walk", "wet_idle")
			
					IF HAS_ANIM_DICT_LOADED("missfam5_wet_walk")
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5_wet_walk", "wet_idle", INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, AF_DEFAULT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Giving player ped task to play animation 'wet_idle'.")
						#ENDIF
					ENDIF
				
				ELSE
				
					iProgress++
				
				ENDIF
			
			BREAK
			
			CASE 1
			
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_wet_walk", "wet_idle")
					
					IF ( bPlayerPedScriptTaskInterrupted = FALSE )
					
						INT iXValue
						INT iYValue
						
						iXValue = ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) * 255.0)
						iYValue = ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) * 255.0)

						IF (iXValue < -50 OR iXValue > 50)
						OR (iYValue < -50 OR iYValue > 50)
						
							CLEAR_PED_TASKS(PLAYER_PED_ID())

							bPlayerPedScriptTaskInterrupted = TRUE
						ENDIF

					ENDIF
								
				ENDIF
			
			BREAK

		ENDSWITCH
	
	ENDIF

ENDPROC

PROC MANAGE_JIMMY(PED_INDEX PedIndex, INT &iProgress)
	
	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
		
			SWITCH iProgress
			
				CASE 0
				
					iProgress++
				
				BREAK
	
				CASE 1	//create props for Jimmy and start playing animation
				
					REQUEST_ANIM_DICT("missfam5mcs_4leadin")
					
					IF 	HAS_ANIM_DICT_LOADED("missfam5mcs_4leadin")
					AND HAS_MISSION_OBJECT_BEEN_CREATED(osGamepad, FALSE)
					AND HAS_MISSION_OBJECT_BEEN_CREATED(osHeadset, FALSE)
					
						CLEAR_PED_TASKS(PedIndex)
											
						SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)
						
						TASK_PLAY_ANIM_ADVANCED(PedIndex, "missfam5mcs_4leadin", "family_5_mcs_4_loop_jimmy", << -806.260, 170.664, 77.520 >>,
												<< 0.000, 0.000, 98.280 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
												AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
												AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_ABORT_ON_WEAPON_DAMAGE, 
												0, EULER_YXZ, AIK_DISABLE_LEG_IK)						
						
						IF DOES_ENTITY_EXIST(osGamepad.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osGamepad.ObjectIndex)
								SET_ENTITY_COLLISION(osGamepad.ObjectIndex, FALSE)
								ATTACH_ENTITY_TO_ENTITY(osGamepad.ObjectIndex, PedIndex, GET_PED_BONE_INDEX(PedIndex, BONETAG_PH_L_HAND),
														<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(osHeadset.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osHeadset.ObjectIndex)
								SET_ENTITY_COLLISION(osHeadset.ObjectIndex, FALSE)
								ATTACH_ENTITY_TO_ENTITY(osHeadset.ObjectIndex, PedIndex, GET_PED_BONE_INDEX(PedIndex, BONETAG_HEAD),
														<< 0.02, 0.00, 0.00 >>, << -180.0, 90.0, 0.0 >>)
							ENDIF
						ENDIF
	
						REMOVE_ANIM_DICT("missfam5mcs_4leadin")
						
						iProgress++
					
					ENDIF
				
				BREAK
				
				CASE 2
				
					SET_PED_RESET_FLAG(PedIndex, PRF_SkipExplosionOcclusion, TRUE)
					SET_PED_RESET_FLAG(PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
					
				BREAK
			
			ENDSWITCH
		
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_GOTO_JIMMYS_ROOM_COMPLETED(INT &iStageProgress)
	
	MANAGE_CONVERSATIONS_DURING_GOTO_JIMMYS_ROOM()
	MANAGE_JIMMY(psJimmy.PedIndex, psJimmy.iProgress)
	MANAGE_ANIMATIONS_DURING_GOTO_JIMMYS_ROOM(iAnimationProgress)
	
	IF 	NOT ASSISTED_MOVEMENT_IS_ROUTE_LOADED("family5b")
	AND NOT ASSISTED_MOVEMENT_IS_ROUTE_LOADED("mansion_1")
	AND NOT ASSISTED_MOVEMENT_IS_ROUTE_LOADED("family5c")
	AND	NOT ASSISTED_MOVEMENT_IS_ROUTE_LOADED("f5_jimmy1")
	
		ASSISTED_MOVEMENT_REQUEST_ROUTE("family5b")
		ASSISTED_MOVEMENT_REQUEST_ROUTE("mansion_1")
		ASSISTED_MOVEMENT_REQUEST_ROUTE("family5c")
		ASSISTED_MOVEMENT_REQUEST_ROUTE("f5_jimmy1")
	
	ENDIF
		
	SWITCH iStageProgress
	
		CASE 0
		
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), -1.0/*-1.2*/, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
			
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), 1.0/*1.2*/, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		
			iStageProgress++
		
		BREAK
	
		CASE 1
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_GT2")
				IF NOT DOES_BLIP_EXIST(psJimmy.BlipIndex)
					IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
						
						REPLAY_RECORD_BACK_FOR_TIME(0.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
						
						psJimmy.BlipIndex = CREATE_BLIP_FOR_COORD(vJimmysRoomsCoords)
						PRINT_GOD_TEXT_ADVANCED("FAM5_GT2", DEFAULT_GOD_TEXT_TIME, TRUE)
					ENDIF
				ENDIF
			ENDIF
		
			IF DOES_BLIP_EXIST(psJimmy.BlipIndex)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)
					REMOVE_BLIP(psJimmy.BlipIndex)
				ENDIF
			ELSE
				IF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.150208,172.722031,75.740730>>, <<-806.707092,174.138565,78.740730>>, 2.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)
					IF HAS_LABEL_BEEN_TRIGGERED("FAM5_GT2")
						psJimmy.BlipIndex = CREATE_BLIP_FOR_COORD(vJimmysRoomsCoords)
					ENDIF
				ENDIF
			ENDIF


			IF ( bStreamTriggered = FALSE )
				
				IF IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_JIMMY_BEDROOM)
					DISABLE_TV_CONTROLS(TV_LOC_JIMMY_BEDROOM, TRUE)
					START_AMBIENT_TV_PLAYBACK(TV_LOC_JIMMY_BEDROOM, TVCHANNELTYPE_CHANNEL_SPECIAL, TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER_CUTSCENE, FALSE, TRUE)
					SET_TV_VOLUME(-9)
					
					bStreamTriggered = TRUE
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": TV script for TV_LOC_JIMMY_BEDROOM is available for use, starting TV playback.")
				ENDIF
				
			ELSE
			
				IF NOT IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_JIMMY_BEDROOM)
					STOP_AMBIENT_TV_PLAYBACK(TV_LOC_JIMMY_BEDROOM)
					
					bStreamTriggered = FALSE
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": TV script for TV_LOC_JIMMY_BEDROOM is not available for use, stopping TV playback.")
				ENDIF
			
			ENDIF
			

			IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			OR IS_PLAYER_CHANGING_CLOTHES()
                  
				IF HAS_THIS_CUTSCENE_LOADED("family_5_mcs_4")
				OR HAS_CUTSCENE_LOADED()
				OR IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
					#ENDIF
				ENDIF
				                        
			ELSE

				REQUEST_CUTSCENE("family_5_mcs_4")
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())	//Michael's variations
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene ped Michael from player ped.")
					#ENDIF
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", psJimmy.PedIndex)	//Jimmy's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_BERD, 1, 0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene ped Jimmy from ped.")
					#ENDIF
				ENDIF
			ENDIF

			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_GT4")
				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), psJimmy.PedIndex) > BUDDY_SEPARATION_DIST
					PRINT_GOD_TEXT_ADVANCED("FAM5_GT4", DEFAULT_GOD_TEXT_TIME, TRUE)
				ENDIF
			ENDIF

			//once the player reaches upper hall, block certain actions for lead in to cutscene
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.150208,172.722031,75.740730>>, <<-806.707092,174.138565,78.740730>>, 2.0)	//
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)	//Jimmy's room
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.965271,175.489349,75.640732>>, <<-800.594666,179.089935,79.240730>>, 4.0)	//upper hall
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.698608,172.015182,75.640366>>, <<-806.011292,175.406418,79.240730>>, 3.0)	//upper hall small
			
				SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_SPRINT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_COVER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_ENTER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_DETONATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SELECT_WEAPON)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_VEH_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_VEH_BRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_VEH_ACCELERATE)
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
				
			ENDIF
			
			IF ( bConversationsFinished = TRUE )				//if all leadin conversations have played, trigger the cutscene
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
					CLEAR_PED_ALTERNATE_WALK_ANIM(PLAYER_PED_ID())
				ENDIF
				
				REMOVE_BLIP(psJimmy.BlipIndex)
				
				REMOVE_ANIM_DICT("missfam5_wet_walk")
				REMOVE_ANIM_DICT("missfam5mcs_4leadin")
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 1.0, REPLAY_IMPORTANCE_HIGHEST)
				
				RETURN TRUE
				
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_JIMMYS_ROOM_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
		
			IF ( bStreamTriggered = FALSE )
				
				START_AMBIENT_TV_PLAYBACK(TV_LOC_JIMMY_BEDROOM, TVCHANNELTYPE_CHANNEL_SPECIAL, TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER_CUTSCENE)
				SET_TV_VOLUME(-9)
				bStreamTriggered = TRUE
			
			ENDIF
			
			IF HAS_REQUESTED_CUTSCENE_LOADED("family_5_mcs_4")
			
				IF ( bStreamTriggered = TRUE)
			
					KILL_ANY_CONVERSATION()
				
					CLEAR_PRINTS()
				
					CLEAR_HELP()
				
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					ENDIF
			
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psJimmy.ModelName)
					ENDIF
					
					IF DOES_ENTITY_EXIST(osHeadset.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osHeadset.ObjectIndex)
							IF IS_ENTITY_ATTACHED(osHeadset.ObjectIndex)	
								DETACH_ENTITY(osHeadset.ObjectIndex, FALSE, TRUE)
							ENDIF
							REGISTER_ENTITY_FOR_CUTSCENE(osHeadset.ObjectIndex, "Headset_Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osHeadset.ModelName)
						ENDIF
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(osHeadset.ObjectIndex, "Headset_Jimmy", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osHeadset.ModelName)
					ENDIF
					
					IF DOES_ENTITY_EXIST(osGamepad.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osGamepad.ObjectIndex)
							IF IS_ENTITY_ATTACHED(osGamepad.ObjectIndex)	
								DETACH_ENTITY(osGamepad.ObjectIndex, FALSE, TRUE)
							ENDIF
							REGISTER_ENTITY_FOR_CUTSCENE(osGamepad.ObjectIndex, "Jimmy_Controller", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osGamepad.ModelName)
						ENDIF
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(osGamepad.ObjectIndex, "Jimmy_Controller", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osGamepad.ModelName)
					ENDIF
					
					START_CUTSCENE(CUTSCENE_DELAY_ENABLING_PLAYER_CONTROL_FOR_UP_TO_DATE_GAMEPLAY_CAMERA)
				
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
					iStageProgress++
					
				ENDIF
				
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", psJimmy.PedIndex)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_BERD, 1, 0)
				ENDIF
			
			ENDIF
			
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				//clear the area
				CLEAR_AREA(MissionPosition.vStageStart, 50.0, TRUE)
				CLEAR_AREA_OF_OBJECTS(MissionPosition.vStageStart, 50.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vStageStart, 50.0)
				CLEAR_AREA_OF_PROJECTILES(MissionPosition.vStageStart, 50.0)
				REMOVE_PARTICLE_FX_IN_RANGE(MissionPosition.vStageStart, 50.0)
			
				//reposition michael's car during cutscene, it might have been moved by the player before cutscene
				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
					SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vsMichaelsCar.vPosition)
					SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, vsMichaelsCar.fHeading)
					SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
				ENDIF
			
				IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA)	
					SET_PLAYER_PED_OUTFIT(MO_YOGA_FLIP_FLOPS)
				ENDIF
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF NOT DOES_ENTITY_EXIST(osGamepad.ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Jimmy_Controller")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy_Controller"))
						osGamepad.ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy_Controller"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object Jimmy_Controller in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(osHeadset.ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Headset_Jimmy")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Headset_Jimmy"))
						osHeadset.ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Headset_Jimmy"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object Headset_Jimmy in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy", psJimmy.ModelName)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
				#ENDIF
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					CLEAR_PED_TASKS(psJimmy.PedIndex)
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						IF ( bCutsceneSkipped = TRUE )
							WARP_PED(psJimmy.PedIndex, << -805.53, 175.98, 75.7407 >>, 350.0319, FALSE, FALSE, FALSE)
						ENDIF
						FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						TASK_ENTER_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP * 3, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
					ENDIF
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped Jimmy received task sequence to enter the vehicle when CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
					#ENDIF
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 3, 0, 0) //(berd)
				ENDIF
			ENDIF
			
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				
					SEQUENCE_INDEX SequenceIndex
					CLEAR_SEQUENCE_TASK(SequenceIndex)
					OPEN_SEQUENCE_TASK(SequenceIndex)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-805.6359, 175.4280, 75.7407>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-804.6224, 179.1443, 75.7407>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
					CLOSE_SEQUENCE_TASK(SequenceIndex)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
					CLEAR_SEQUENCE_TASK(SequenceIndex)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)

				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_CAMERA() returned true.")
				#ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Headset_Jimmy")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Headset_Jimmy.")
				#ENDIF
				IF DOES_ENTITY_EXIST(osHeadset.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osHeadset.ObjectIndex)
						SET_ENTITY_COORDS_NO_OFFSET(osHeadset.ObjectIndex, << -806.598, 170.280, 76.475 >>)
						SET_ENTITY_ROTATION(osHeadset.ObjectIndex, << -94.000, 0, 0.0>>)
						FREEZE_ENTITY_POSITION(osHeadset.ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy_Controller")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy_Controller.")
				#ENDIF
				IF DOES_ENTITY_EXIST(osGamepad.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osGamepad.ObjectIndex)
						SET_ENTITY_COORDS_NO_OFFSET(osGamepad.ObjectIndex, osGamepad.vPosition)
						SET_ENTITY_ROTATION(osGamepad.ObjectIndex, osGamepad.vRotation)
						FREEZE_ENTITY_POSITION(osGamepad.ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF			
			
			IF HAS_CUTSCENE_FINISHED()
					
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				REPLAY_STOP_EVENT()
												
				RETURN TRUE
				
			ELSE
						
				IF ( GET_CUTSCENE_TIME() > 12000 )
					IF bStreamTriggered = TRUE
						STOP_TV_PLAYBACK(TRUE, TV_LOC_JIMMY_BEDROOM)
						bStreamTriggered = FALSE
					ENDIF
				ENDIF
				
				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						bCutsceneSkipped = TRUE	//if cutscene was skipped, set the cutscene skipped flag
					ENDIF
				ENDIF
			
			ENDIF
			
			//keep requesting assisted movement routes needed after the cutscene
			ASSISTED_MOVEMENT_REQUEST_ROUTE("mansion_1")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("family5d")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("f5_jimmy1")
			
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC MANAGE_DEALER_PED(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	VECTOR	vScenePosition	= << -1179.314, -891.318, 12.730 >>	
	VECTOR	vSceneRoation	= << 0.0, 0.0, -147.6 >>

	IF DOES_ENTITY_EXIST(ped.PedIndex)
		IF 	NOT IS_PED_INJURED(ped.PedIndex)
		AND NOT IS_PED_INJURED(TargetPedIndex)
			
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			#ENDIF
			
			FLOAT fDistanceToTargetPed =  GET_DISTANCE_BETWEEN_ENTITIES(TargetPedIndex, ped.PedIndex)

			IF ( ped.eState <> PED_STATE_FLEEING )
			
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped.PedIndex)
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped.PedIndex) 
				
					IF IS_PED_IN_MELEE_COMBAT(TargetPedIndex)
					
						IF ( fDistanceToTargetPed < 5.0 )
					
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
							
						ENDIF
						
					ELSE
					
						IF ( fDistanceToTargetPed < 20.0 )
						
							WEAPON_TYPE	eCurrentWeapon
				
							IF GET_CURRENT_PED_WEAPON(TargetPedIndex, eCurrentWeapon)
							
								IF 	GET_WEAPONTYPE_GROUP(eCurrentWeapon) != WEAPONGROUP_MELEE
								AND GET_WEAPONTYPE_GROUP(eCurrentWeapon) != WEAPONGROUP_PETROLCAN
						
									GO_TO_PED_STATE(ped, PED_STATE_FLEEING)

								ENDIF
								
							ENDIF
							
						ENDIF
					
					ENDIF
					
				ENDIF
			
				IF IS_PED_IN_COMBAT(ped.PedIndex, TargetPedIndex)
					GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
				ENDIF
			ENDIF

			SWITCH ped.eState
			
				CASE PED_STATE_IDLE

					IF ( fDistanceToTargetPed < 200.0 )
						GO_TO_PED_STATE(ped, PED_STATE_PLAYING_LOOP_ANIMATION)
					ENDIF
			
				BREAK
				
				CASE PED_STATE_PLAYING_LOOP_ANIMATION
				
					IF ( ped.bHasTask = FALSE )
						
						IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_loop_alt_dealer")
							
							REQUEST_ANIM_DICT("missfam5leadinoutmcs_5")
							
							IF HAS_ANIM_DICT_LOADED("missfam5leadinoutmcs_5")
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_loop_alt_dealer",
														vScenePosition, vSceneRoation, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
														AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							ENDIF
							
							
						ELSE
							ped.bHasTask = TRUE
						ENDIF
					ENDIF
					
					
					IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_loop_alt_dealer")
						
						IF NOT DOES_ENTITY_EXIST(osCiggy.ObjectIndex)
							IF HAS_MISSION_OBJECT_BEEN_CREATED(osCiggy)
								ATTACH_ENTITY_TO_ENTITY(osCiggy.ObjectIndex, ped.PedIndex, GET_PED_BONE_INDEX(ped.PedIndex, BONETAG_PH_R_HAND),
														<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)		
							ENDIF
						
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(osMethBag.ObjectIndex)
							IF HAS_MISSION_OBJECT_BEEN_CREATED(osMethBag)
								ATTACH_ENTITY_TO_ENTITY(osMethBag.ObjectIndex, ped.PedIndex, GET_PED_BONE_INDEX(ped.PedIndex, BONETAG_PH_L_HAND),
														<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)		
							ENDIF
						
						ENDIF
					
						IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_loop_alt_dealer") > 0.95
							IF ( bLeadInTriggered = TRUE )
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_action_dealer",
														vScenePosition, vSceneRoation, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
														AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ACTION_ANIMATION)
							ENDIF
							
						ENDIF
					
					ENDIF
				
				BREAK
				
				CASE PED_STATE_PLAYING_ACTION_ANIMATION
				
					IF ( ped.bHasTask = FALSE )
						
						IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_action_dealer")
							
							REQUEST_ANIM_DICT("missfam5leadinoutmcs_5")
							
							IF HAS_ANIM_DICT_LOADED("missfam5leadinoutmcs_5")
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_action_dealer",
														vScenePosition, vSceneRoation, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
														AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							ENDIF
							
							
						ELSE
							ped.bHasTask = TRUE
						ENDIF
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_action_dealer")
						SET_ENTITY_ANIM_SPEED(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_action_dealer", 0.85)
						IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "missfam5leadinoutmcs_5", "leadin_action_dealer") >= 0.98
							bLeadInStartCutscene = TRUE
						ENDIF
					ENDIF
				
				BREAK
				
				CASE PED_STATE_FLEEING
					
					IF ( ped.bHasTask = FALSE )
						MAKE_PED_FLEE_PED(ped.PedIndex, TargetPedIndex, 300, -1)
						ped.bHasTask = TRUE
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_DEALER(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), psDealer.vPosition) < 200.0
			
				iProgress++
				
			ENDIF
				
		BREAK
	
		CASE 1	//create dealer ped and door object to animate

			IF 	NOT DOES_ENTITY_EXIST(psDealer.PedIndex)
			AND NOT DOES_ENTITY_EXIST(osBSDoorDummy.ObjectIndex)
				
				REQUEST_MODEL(psDealer.ModelName)
				REQUEST_MODEL(osBSDoorDummy.ModelName)
			
				IF 	HAS_MODEL_LOADED(psDealer.ModelName)
				AND	HAS_MODEL_LOADED(osBSDoorDummy.ModelName)

					psDealer.PedIndex = CREATE_PED(PEDTYPE_MISSION, psDealer.ModelName, psDealer.vPosition, psDealer.fHeading)
					
					IF HAS_MISSION_OBJECT_BEEN_CREATED(osBSDoorDummy, TRUE)
					
						SET_ENTITY_LOD_DIST(osBSDoorDummy.ObjectIndex, 150)
					
						CREATE_MODEL_HIDE(osBSDoor.vPosition, 1.0, osBSDoor.ModelName, TRUE)
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(psDealer.ModelName)
					SET_MODEL_AS_NO_LONGER_NEEDED(osBSDoorDummy.ModelName)
					
				ENDIF
				
			ELSE
			
				FailFlags[MISSION_FAIL_DEALER_DEAD] 	= TRUE
				FailFlags[MISSION_FAIL_DEALER_SCARED] 	= TRUE
				FailFlags[MISSION_FAIL_DEALER_ATTACKED] = TRUE

				IF NOT IS_PED_INJURED(psDealer.PedIndex)
				
					SET_PED_PROP_INDEX(psDealer.PedIndex, ANCHOR_HEAD, 0, 0)
					SET_PED_PROP_INDEX(psDealer.PedIndex, ANCHOR_EYES, 0, 0)
					
					SET_ENTITY_LOD_DIST(psDealer.PedIndex, 150)		
					SET_PED_LOD_MULTIPLIER(psDealer.PedIndex, 5.0)
				
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psDealer.PedIndex, TRUE)
					SET_PED_CONFIG_FLAG(psDealer.PedIndex, PCF_ForceStandardBumpReactionThresholds, TRUE)
					SET_PED_FLEE_ATTRIBUTES(psDealer.PedIndex, FA_CAN_SCREAM | FA_DISABLE_HANDS_UP, TRUE)					
					
					ADD_PED_FOR_DIALOGUE(Family5Conversation, 5, psDealer.PedIndex, "FAM5DEALER")
				ENDIF
				
				GO_TO_PED_STATE(psDealer, PED_STATE_IDLE)

				iProgress++

			ENDIF
		
		BREAK
		
		CASE 2	//update dealer ped

			REQUEST_MODEL(osBSDoor.ModelName)

			MANAGE_DEALER_PED(psDealer, PLAYER_PED_ID())

		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_CONVERSATIONS_DURING_GOTO_BURGER_SHOT(INT &iProgress, STRING sRoot1, STRING sRoot2)

	 TEXT_LABEL txtRoot
	 
	 txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

	//Pause the conversation when player gets too far away from Jimmy
	IF  NOT IS_PED_INJURED(psJimmy.PedIndex)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())

		IF ( bConversationsFinished = FALSE )

			SWITCH iProgress
		
				CASE 0	
				
					//pause the conversation with Jimmy when player gets too far away from Jimmy
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), psJimmy.PedIndex, << 8.0, 8.0, 4.0 >>)
							//IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							//	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							//ENDIF
							KILL_FACE_TO_FACE_CONVERSATION()
						//ELSE
							//IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							//	PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)	//resume the conversation when player gets close
							//ENDIF						
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_PUSH")	//handle conversation when Michael pushes Jimmy and makes him ragdoll
						IF DOES_ENTITY_EXIST(psJimmy.PedIndex)
							IF NOT IS_ENTITY_DEAD(psJimmy.PedIndex)
							
								IF ( bJimmyRagdolledFromPlayerImpact = FALSE )
										
									IF ( IS_PED_RAGDOLL(psJimmy.PedIndex) AND IS_ENTITY_TOUCHING_ENTITY(psJimmy.PedIndex, PLAYER_PED_ID()) )
									OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psJimmy.PedIndex, PLAYER_PED_ID())
									
										IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											KILL_FACE_TO_FACE_CONVERSATION()
										ENDIF
										
										CLEAR_ENTITY_LAST_DAMAGE_ENTITY(psJimmy.PedIndex)
										
										bJimmyRagdolledFromPlayerImpact = TRUE
										
									ENDIF
								ENDIF
								
								IF ( bJimmyRagdolledFromPlayerImpact = TRUE )
									IF NOT IS_PED_RAGDOLL(psJimmy.PedIndex)
									
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
											
												IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_PUSH", CONV_PRIORITY_MEDIUM)
												
													bJimmyRagdolledFromPlayerImpact = FALSE
													iJimmyRagdolledFromPlayerImpact++
												
													IF ( iJimmyRagdolledFromPlayerImpact = 4 )		//only have 4 random variations
														SET_LABEL_AS_TRIGGERED("FAM5_PUSH", TRUE)
													ENDIF
												
												ENDIF
											ELSE
												IF NOT IS_AMBIENT_SPEECH_PLAYING(psJimmy.PedIndex)
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psJimmy.PedIndex, "FAM5_DXAA", "JIMMY", SPEECH_PARAMS_FORCE)
													bJimmyRagdolledFromPlayerImpact = FALSE
													iJimmyRagdolledFromPlayerImpact++
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
	
							ENDIF
						ENDIF
						
					ENDIF
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED(sRoot1)				//handle conversation when Michael and Jimmy walk down the stairs
						IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)	//wait for the objective and blip to get into the car first
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID()) AND NOT IS_PED_RAGDOLL(psJimmy.PedIndex)
										IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sRoot1, CONV_PRIORITY_VERY_HIGH)
											SET_LABEL_AS_TRIGGERED(sRoot1, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
					
						IF 	IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND ARE_STRINGS_EQUAL(txtRoot, sRoot1)
						//IF IS_THIS_CONVERSATION_PLAYING(sRoot1)
						
							IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
								OR IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
								OR IS_PED_GETTING_INTO_A_VEHICLE(psJimmy.PedIndex)
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
						
							ENDIF
							
							IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
							
						ELSE
							bInitialConversationFinished = TRUE				
						ENDIF
					
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SON3")	//handle conversation when Michael and Jimmy get into the car
					
						IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
								
							IF 	IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
							AND IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
							
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0) 
											IF CREATE_CONVERSATION (Family5Conversation, "FAM5AUD", "FAM5_SON3", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("FAM5_SON3", TRUE)
												iProgress++
											ENDIF
										ENDIF
									ENDIF
								ELSE
								
									//resume paused conversation if it was paused during walking to the car
									IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
										PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									ENDIF
									
									IF IS_THIS_CONVERSATION_PLAYING(sRoot1)
										KILL_FACE_TO_FACE_CONVERSATION()
									ENDIF
									
								ENDIF
							ENDIF
						
						ENDIF
					
					ENDIF

				BREAK
				
				CASE 1	//wait for the objective and blip to go to Burger Shot first
				
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_THIS_PRINT_BEING_DISPLAYED("FAM5_GT3")//("FAM5_GT3")
							
							CurrentConversationRoot 	= ""
							CurrentConversationLabel	= ""
							
							iProgress++
						ENDIF
					ENDIF
				
				BREAK
				
				CASE 2	//conversations on the way to Burger Shot
				
					//if main conversation was interrupted, resume it from the next label
					IF ( bCurrentConversationInterrupted = TRUE )
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							IF HAS_LABEL_BEEN_TRIGGERED("FAM5_WNTD1")

								IF HAS_LABEL_BEEN_TRIGGERED(sRoot2)
								OR HAS_LABEL_BEEN_TRIGGERED("FAM5_SON5")
								
									//resume the interrupted coversation
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF 	NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
										AND NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationLabel)
											IF ARE_STRINGS_EQUAL(CurrentConversationRoot, sRoot2)		//resume only the main conversations
											OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM5_SON5")
												IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(Family5Conversation, "FAM5AUD", CurrentConversationRoot, CurrentConversationLabel, CONV_PRIORITY_MEDIUM)
													bCurrentConversationInterrupted = FALSE
													SET_LABEL_AS_TRIGGERED("FAM5_WNTD1", FALSE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
					//handle player getting out of the car and being wanted
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				
						//resume paused conversations when player gets back in the car
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							ENDIF
						ENDIF
					
					ELSE //locates header blip does not exist, player is out of the car or wanted
						
						IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	//when wanted, kill conversation, pause it and play wanted conversation
							
							IF ( bCurrentConversationInterrupted = FALSE )
					
								IF HAS_LABEL_BEEN_TRIGGERED(sRoot2)	
								OR HAS_LABEL_BEEN_TRIGGERED("FAM5_SON5")
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									
										CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
										
										IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
											IF ARE_STRINGS_EQUAL(CurrentConversationRoot, sRoot2)
											OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "FAM5_SON5")
												bCurrentConversationInterrupted = TRUE
												CurrentConversationRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
												CurrentConversationLabel		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
										ENDIF
										
									ENDIF
								ENDIF

							ENDIF
							
							IF ( bCurrentConversationInterrupted = TRUE )
								IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_WNTD1")
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_WNTD1", CONV_PRIORITY_HIGH)
											SET_LABEL_AS_TRIGGERED("FAM5_WNTD1", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ELSE	//player not wanted, pause face to face conversations
						
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								ENDIF
							ENDIF
							
						ENDIF

					ENDIF
					
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					
						//play conversation when Michael and Jimmy drive to Burger Shot
						IF NOT HAS_LABEL_BEEN_TRIGGERED(sRoot2)

							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION (Family5Conversation, "FAM5AUD", sRoot2, CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED(sRoot2, TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF

						//handle conversation when Michael and Jimmy are close to Burger Shot
						IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SON5")
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), psDealer.vPosition, <<130.0, 130.0, 100.0>>)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SON5", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("FAM5_SON5", TRUE)
										ENDIF
									ENDIF
								ELSE
									KILL_ANY_CONVERSATION()
								ENDIF
							ENDIF
						ENDIF

						
						//kill any conversation when they drive into burger shot parking lot, so that it stops before the lead in
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1184.633423,-874.228088,17.855732>>, <<-1168.236450,-901.460083,7.064990>>, 35.0)
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF

					ENDIF
					
				BREAK
			
			ENDSWITCH
			
		ENDIF
		
		IF ( bLeadInTriggered = TRUE )
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_PREBS")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						//if dealer is playing loop anim only, otherwise it might be too late to play conversation
						IF DOES_ENTITY_EXIST(psDealer.PedIndex)
							IF NOT IS_ENTITY_DEAD(psDealer.PedIndex)
								IF IS_ENTITY_PLAYING_ANIM(psDealer.PedIndex, "missfam5leadinoutmcs_5", "leadin_loop_alt_dealer")
									IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_PREBS", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("FAM5_PREBS", TRUE)
									ENDIF
								ELSE
									SET_LABEL_AS_TRIGGERED("FAM5_PREBS", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF

ENDPROC

PROC MANAGE_JIMMY_DURING_GOTO_BURGER_SHOT(INT &iProgress)

	IF NOT IS_PED_INJURED(psJimmy.PedIndex)

		SWITCH iProgress
		
			CASE 0	//make Jimmy enter Michael's car, if he is not doing that already after the cutscene
			
				SET_PED_CONFIG_FLAG(psJimmy.PedIndex, PCF_OpenDoorArmIK, TRUE)
				
				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
					
					IF ( GET_SCRIPT_TASK_STATUS(psJimmy.PedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK )
					
						//this is a hacky way to stop jimmy from stopping when walking to the car on retry
						//warp jimmy, task him to enter vehicle and wait for a second before fading in
						//this should only be done on retry or debug skip
						WARP_PED(psJimmy.PedIndex, <<-805.4584, 174.9825, 75.7407>>, 336.1902, FALSE, FALSE, FALSE)
						TASK_ENTER_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP * 3, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
						FORCE_PED_MOTION_STATE(psJimmy.PedIndex, MS_ON_FOOT_WALK)
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped Jimmy received task sequence to enter the vehicle.")
						#ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF ( GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK )
								
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
								SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
								
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-805.6359, 175.4280, 75.7407>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-804.6224, 179.1443, 75.7407>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT)
								
							ENDIF
						ENDIF
						
						WAIT(1000)
						
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
						ENDIF
						
					ENDIF
					
					iProgress++

				ENDIF
			
			BREAK
			
			CASE 1	//check if Jimmy got into Michael's car
			
				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)

					IF NOT IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
					
						SET_PED_RESET_FLAG(psJimmy.PedIndex, PRF_SearchForClosestDoor, TRUE)
					
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
						OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
													
							//in case Jimmy stops getting into the car
							//for example when he is ragdolling, doing a NM task or doing TASK_DO_NOTHING
							IF GET_SCRIPT_TASK_STATUS(psJimmy.PedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
							//IF GET_SCRIPT_TASK_STATUS(psJimmy.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
								TASK_ENTER_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, DEFAULT_TIME_BEFORE_WARP * 3, VS_FRONT_RIGHT, PEDMOVE_RUN)
							ENDIF
							
							//make Jimmy run to the car
							SET_PED_DESIRED_MOVE_BLEND_RATIO(psJimmy.PedIndex, PEDMOVEBLENDRATIO_RUN)
						
						ENDIF
						
					ENDIF

					IF IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
						iProgress++
					ENDIF				
				
				ENDIF

			BREAK
		
		ENDSWITCH
		
	ENDIF
	
ENDPROC

PROC MANAGE_FOCUS_PUSH()

	IF NOT IS_GAMEPLAY_HINT_ACTIVE()
		SET_GAMEPLAY_HINT_FOV(fFocusPushHintFOV)
		SET_GAMEPLAY_COORD_HINT(vFocusPushHintPosition, -1, 2500)
		
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fFocusPushSideOffset)
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fFocusPushVerticalOffset)

		SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fFocusPushFollowDistanceScalar)
		SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fFocusPushBaseOrbitPitchOffset)
	ELSE
		#IF IS_DEBUG_BUILD
			IF ( bResetGameplayHint = TRUE )
				STOP_GAMEPLAY_HINT(TRUE)
				bResetGameplayHint = FALSE
			ENDIF
		#ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_GOTO_BURGER_SHOT_COMPLETED(INT &iStageProgress)
	
	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
	DISABLE_LOCATES_WANTED_PRINTS_OUTSIDE_OF_VEHICLE(sLocatesData, TRUE)
	
	MANAGE_DEALER(psDealer.iProgress)
	MANAGE_JIMMY_DURING_GOTO_BURGER_SHOT(psJimmy.iProgress)
	MANAGE_CONVERSATIONS_DURING_GOTO_BURGER_SHOT(iConversationProgress, PICK_STRING(bStageReplayInProgress, "FAM5_SON2b", "FAM5_SON2"),
																		PICK_STRING(bStageReplayInProgress, "FAM5_SON4b", "FAM5_SON4"))
																		
	UPDATE_TRIGGERED_LABEL(sLabelMGETIN)
	
	//handle requestting mocap cutscene 
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_PLAYER_CHANGING_CLOTHES()
          
		IF HAS_THIS_CUTSCENE_LOADED("family_5_mcs_5")
		OR HAS_CUTSCENE_LOADED()
		OR IS_CUTSCENE_ACTIVE()
			REMOVE_CUTSCENE()
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
			#ENDIF
		ENDIF
		                        
	ELSE
	
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -1175.67, -888.60, 12.87 >>) < DEFAULT_CUTSCENE_LOAD_DIST
			
			REQUEST_CUTSCENE("family_5_mcs_5")
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			ENDIF
	
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())	//Michael's variations
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene ped Michael from player ped.")
					#ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", psJimmy.PedIndex)	//Jimmy's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_BERD, 1, 0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene ped Jimmy from ped.")
					#ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(psDealer.PedIndex)
					IF NOT IS_ENTITY_DEAD(psDealer.PedIndex)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Burgershot_drugdealer", psDealer.PedIndex)
						SET_CUTSCENE_PED_PROP_VARIATION("Burgershot_drugdealer", ANCHOR_HEAD, 0, 0)
						SET_CUTSCENE_PED_PROP_VARIATION("Burgershot_drugdealer", ANCHOR_EYES, 0, 0)
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene ped Burgershot_drugdealer from ped.")
						#ENDIF
					ENDIF
				ELSE
					SET_CUTSCENE_PED_PROP_VARIATION("Burgershot_drugdealer", ANCHOR_HEAD, 0, 0)
					SET_CUTSCENE_PED_PROP_VARIATION("Burgershot_drugdealer", ANCHOR_EYES, 0, 0)
				ENDIF
				bCutsceneAssetsRequested = TRUE
			ENDIF
			
		ELSE
		
			IF HAS_THIS_CUTSCENE_LOADED("family_5_mcs_5")
			OR HAS_CUTSCENE_LOADED()
			OR IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
				#ENDIF
			ENDIF
		
		ENDIF

	ENDIF
											
	SWITCH iStageProgress
				
		CASE 0
		
			IF 	NOT ASSISTED_MOVEMENT_IS_ROUTE_LOADED("family5d")
			AND NOT ASSISTED_MOVEMENT_IS_ROUTE_LOADED("mansion_1")
			AND NOT ASSISTED_MOVEMENT_IS_ROUTE_LOADED("f5_jimmy1")
				ASSISTED_MOVEMENT_REQUEST_ROUTE("family5d")
				ASSISTED_MOVEMENT_REQUEST_ROUTE("mansion_1")
				ASSISTED_MOVEMENT_REQUEST_ROUTE("f5_jimmy1")
			ENDIF
		
			REQUEST_VEHICLE_ASSET(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK	
				IF ( bPlayerPedScriptTaskInterrupted = FALSE )
					IF IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_IN()
						IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
						OR (ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) * 255.0) < -20 OR ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) * 255.0) > 20)
						OR (ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) * 255.0) < -20 OR ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) * 255.0) > 20)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							REPLAY_RECORD_BACK_FOR_TIME(0.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
							bPlayerPedScriptTaskInterrupted = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)

				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
					
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					
						IF NOT IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)

							//if player keeps driving away from stage start while Jimmy still runs behind the car
							IF ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), vsMichaelsCar.vPosition) > ABANDON_BUDDY_FAIL_RANGE )
								bJimmyLeftBehindSprinting = TRUE
							ENDIF
							
						ENDIF
					
					ENDIF
					
					IF IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_TO_JIMMY")
						STOP_AUDIO_SCENE("FAMILY_5_GO_TO_JIMMY")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_TO_BURGERSHOT")
						START_AUDIO_SCENE("FAMILY_5_GO_TO_BURGERSHOT")
					ENDIF
				
				ELSE
				
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly, TRUE)
				
				ENDIF
			ENDIF

			IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData, << -1175.67, -888.60, 12.87 >>, << 5.0, 3.5, LOCATE_SIZE_HEIGHT >>,
														   TRUE, psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, "", "CMN_JLEAVE",
														   PICK_STRING(IS_SUBTITLE_PREFERENCE_SWITCHED_ON(), "", sLabelMGETIN),
														   "CMN_GENGETBCKY", FALSE, TRUE)

				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
					IF IS_VEHICLE_ON_ALL_WHEELS(vsMichaelsCar.VehicleIndex)
			
						bConversationsFinished = TRUE
			
						KILL_ANY_CONVERSATION()
						
						CLEANUP_MISSION_OBJECT_GROUP(osYogaMats, TRUE)
						CLEANUP_MISSION_OBJECT(osGamepad, TRUE)
						CLEANUP_MISSION_OBJECT(osHeadset, TRUE)
						SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_DESTROYED)
						
						REPLAY_RECORD_BACK_FOR_TIME(12.0, 1.0, REPLAY_IMPORTANCE_HIGHEST)
						
						bLeadInTriggered = TRUE
						
						IF ( GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON )
							bIgnoreLeadInFocusPush = TRUE
						ENDIF
					
						iStageProgress++
						
					ENDIF
				ENDIF
		
			ENDIF
			
			IF 	NOT HAS_LABEL_BEEN_TRIGGERED(sLabelMGETIN)
			AND NOT IS_STRING_NULL_OR_EMPTY(sLabelMGETIN)
				IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
					IF ( bInitialConversationFinished = TRUE )
						PRINT_GOD_TEXT_ADVANCED(sLabelMGETIN)
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_PRINT_BEING_DISPLAYED(sLabelMGETIN)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
						CLEAR_PRINTS()
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_GT3")
			
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF HAS_LABEL_BEEN_TRIGGERED("FAM5_SON3")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							PRINT_GOD_TEXT_ADVANCED("FAM5_GT3")
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
			
				IF IS_THIS_PRINT_BEING_DISPLAYED("FAM5_GT3")
					IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						CLEAR_PRINTS()
					ENDIF
				ENDIF
			
			ENDIF
			
			//handle removing prop objects from previous stage
			IF DOES_ENTITY_EXIST(osYogaMats[0].ObjectIndex)
			OR DOES_ENTITY_EXIST(osYogaMats[1].ObjectIndex)
			OR DOES_ENTITY_EXIST(osYogaMats[2].ObjectIndex)
			OR DOES_ENTITY_EXIST(osGamepad.ObjectIndex)
			OR DOES_ENTITY_EXIST(osHeadset.ObjectIndex)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), MissionPosition.vStageStart) > 75.0
					CLEANUP_MISSION_OBJECT_GROUP(osYogaMats, TRUE)
					CLEANUP_MISSION_OBJECT(osGamepad, TRUE)
					CLEANUP_MISSION_OBJECT(osHeadset, TRUE)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_DESTROYED)
				ENDIF
			ENDIF

		BREAK
		
		CASE 1
		
			IF ( bIgnoreLeadInFocusPush = FALSE )
				MANAGE_FOCUS_PUSH()
			ENDIF
				
			IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vsMichaelsCar.VehicleIndex, 2.5, 1, 0.75)
		
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF 	bLeadInStartCutscene = TRUE AND bCutsceneAssetsRequested = TRUE
						RETURN TRUE
					ENDIF
				//ENDIF
				
			ENDIF
		
		BREAK
				
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_BURGER_SHOT_COMPLETED(INT &iStageProgress)

	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLeadOutSceneID)

		IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
			IF IS_ENTITY_PLAYING_ANIM(vsMichaelsCar.VehicleIndex, "missfam5leadinoutmcs_5", "leadout_car_door")			
				DISABLE_PLAYER_VEHICLE_DRIVING_CONTROLS_THIS_FRAME()
			ENDIF
		ENDIF

		DISABLE_PLAYER_VEHICLE_MISC_CONTROLS_THIS_FRAME()
		
		IF (bLeadOutCameraCancelled = FALSE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5leadinoutmcs_5", "leadout_mic")	//when player is playing leadout anim
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)						//disable vehicle turing controls
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)						//to stop extra movement on secondary anim
				ENDIF
			ENDIF
		ENDIF

		IF DOES_CAM_EXIST(ScriptedCamera)

			IF (bLeadOutCameraCancelled = FALSE)

				IF SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(DEFAULT, DEFAULT, TRUE)						//display help text for cinematic camera
					IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HCAM")	
						PRINT_HELP("FAM5_HCAM", DEFAULT_HELP_TEXT_TIME)
						SET_LABEL_AS_TRIGGERED("FAM5_HCAM", TRUE)
					ENDIF
				ENDIF

				IF SHOULD_CONTROL_CHASE_HINT_CAM(sCinematicCamStruct, DEFAULT, DEFAULT, TRUE)		//control cinematic camera toggle
					IF NOT IS_CAM_RENDERING(ScriptedCamera)
						DISPLAY_RADAR(FALSE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_TAKE_JIMMY_FOCUS_CAM")
							STOP_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY")
							START_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY_FOCUS_CAM")
						ENDIF
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FAM5_HCAM")
							CLEAR_HELP()
						ENDIF
					ENDIF
				ELSE

					IF IS_CAM_RENDERING(ScriptedCamera)
						DISPLAY_RADAR(TRUE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						IF IS_AUDIO_SCENE_ACTIVE("FAMILY_5_TAKE_JIMMY_FOCUS_CAM")
							STOP_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY_FOCUS_CAM")
							START_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ( bLeadOutCameraCancelled = FALSE )
			IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				IF NOT IS_ENTITY_PLAYING_ANIM(vsMichaelsCar.VehicleIndex, "missfam5leadinoutmcs_5", "leadout_car_door")
					IF NOT IS_VEHICLE_STOPPED(vsMichaelsCar.VehicleIndex)
						IF DOES_CAM_EXIST(ScriptedCamera)
							IF IS_CAM_RENDERING(ScriptedCamera)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							ENDIF
						ENDIF
						CLEAR_HELP(TRUE)
						DISPLAY_RADAR(TRUE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS(PLAYER_PED_ID())	//clear ped tasks when quitting the scripted leadout, fix for B*2079938
						ENDIF
						KILL_CHASE_HINT_CAM(sCinematicCamStruct)
						bLeadOutCameraCancelled = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cancelling animated leadout camera due to player driving the car.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF

	SWITCH iStageProgress
		
		CASE 0

			IF HAS_REQUESTED_CUTSCENE_LOADED("family_5_mcs_5")
		
				KILL_ANY_CONVERSATION()
				KILL_FACE_TO_FACE_CONVERSATION()
		
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psJimmy.PedIndex, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, psJimmy.ModelName)
				ENDIF
				
				REGISTER_ENTITY_FOR_CUTSCENE(osNecklace.ObjectIndex, "Jimmy_necklace", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, P_JIMMYNECK_03_S)
				
				IF DOES_ENTITY_EXIST(osMethBag.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osMethBag.ObjectIndex)
						DETACH_ENTITY(osMethBag.ObjectIndex, FALSE, TRUE)
						REGISTER_ENTITY_FOR_CUTSCENE(osMethBag.ObjectIndex, "Meth_Bag", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, osMethBag.ModelName)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(psDealer.PedIndex)
					TASK_CLEAR_LOOK_AT(psDealer.PedIndex)
					REGISTER_ENTITY_FOR_CUTSCENE(psDealer.PedIndex, "Burgershot_drugdealer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				
					IF IS_VEHICLE_DOOR_DAMAGED(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_LEFT)
						SET_VEHICLE_DOOR_CONTROL(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
					ENDIF
					IF IS_VEHICLE_DOOR_DAMAGED(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_RIGHT)
						SET_VEHICLE_DOOR_CONTROL(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0.0)
					ENDIF
				
					REGISTER_ENTITY_FOR_CUTSCENE(vsMichaelsCar.VehicleIndex, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				STOP_AUDIO_SCENE("FAMILY_5_GO_TO_BURGERSHOT")

				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					ENDIF
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", psJimmy.PedIndex)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_BERD, 1, 0)
					ENDIF
					IF NOT IS_PED_INJURED(psDealer.PedIndex)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Burgershot_drugdealer", psDealer.PedIndex)
					ENDIF
					SET_CUTSCENE_PED_PROP_VARIATION("Burgershot_drugdealer", ANCHOR_HEAD, 0, 0)
					SET_CUTSCENE_PED_PROP_VARIATION("Burgershot_drugdealer", ANCHOR_EYES, 0, 0)
				ENDIF
			
         	ENDIF
			
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				STOP_GAMEPLAY_HINT(TRUE)
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			
				IF DOES_ENTITY_EXIST(osCiggy.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osCiggy.ObjectIndex)
						IF IS_ENTITY_ATTACHED(osCiggy.ObjectIndex)
							DETACH_ENTITY(osCiggy.ObjectIndex, FALSE)
						ENDIF
						DELETE_OBJECT(osCiggy.ObjectIndex)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
					SET_VEHICLE_DOOR_CONTROL(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0.0)
					SET_VEHICLE_DOOR_SHUT(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_RIGHT, TRUE)
				ENDIF
			
				CLEAR_AREA(<< -1176.3639, -887.5740, 12.8652 >>, 6.0, TRUE, TRUE)
				CLEAR_AREA_OF_VEHICLES(<< -1176.3639, -887.5740, 12.8652 >>, 6.0, TRUE)
			
				CREATE_MODEL_HIDE(osBSDoor.vPosition, 1.0, osBSDoor.ModelName, FALSE)
				
				SET_SRL_POST_CUTSCENE_CAMERA(<< -1179.80, -882.04, 14.75 >>, << 0.53, -0.85, -0.04 >>)	//gameplay cam position and direction after cutscene
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				SETTIMERA(0)
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			REQUEST_MODEL(osCup.ModelName)
			REQUEST_MODEL(osBSDoorDummy.ModelName)
			
			REQUEST_ANIM_DICT("missfam5_drink")
			REQUEST_ANIM_DICT("missfam5leadinoutmcs_5")
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_MCS5LO_P")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRELOAD_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_MCS5LO", CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("FAM5_MCS5LO_P", TRUE)
				ENDIF
			ENDIF

			IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				SET_VEHICLE_BRAKE_LIGHTS(vsMichaelsCar.VehicleIndex, FALSE)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(osNecklace.ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Jimmy_necklace")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy_necklace"))
					
						osNecklace.ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy_necklace"))
					
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Grabbed Jimmy_necklace object from the cutscene.")
						#ENDIF

					ENDIF
				ENDIF
			ENDIF	
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_CAMERA() returned true for camera.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERA(): ", TIMERA(), ".")
				#ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF
				
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy", psJimmy.ModelName)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERA(): ", TIMERA(), ".")
				#ENDIF
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					SET_PED_COMPONENT_VARIATION(psJimmy.PedIndex, PED_COMP_BERD, 3, 0, 0) //(berd)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy_necklace")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Jimmy_necklace.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERA(): ", TIMERA(), ".")
				#ENDIF
				IF DOES_ENTITY_EXIST(osNecklace.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osNecklace.ObjectIndex)
						DELETE_OBJECT(osNecklace.ObjectIndex)
					ENDIF
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERA(): ", TIMERA(), ".")
				#ENDIF
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michaels_car.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERA(): ", TIMERA(), ".")
				#ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Burgershot_drugdealer")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Burgershot_drugdealer.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERA(): ", TIMERA(), ".")
				#ENDIF
				
				IF ( bCutsceneSkipped = FALSE )
				
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLeadOutSceneID)	//set up synchronised scene for lead out
								
						iLeadOutSceneID = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), GET_ENTITY_ROTATION(vsMichaelsCar.VehicleIndex))
						
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iLeadOutSceneID, TRUE)
						
						DESTROY_ALL_CAMS()
						ScriptedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iLeadOutSceneID, "leadout_cam", "missfam5leadinoutmcs_5")
						
						SET_VEHICLE_DOOR_LATCHED(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_RIGHT, TRUE, TRUE, FALSE)
						PLAY_ENTITY_ANIM(vsMichaelsCar.VehicleIndex, "leadout_car_door", "missfam5leadinoutmcs_5", INSTANT_BLEND_IN, FALSE, FALSE, FALSE, 0, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
	
						IF HAS_MISSION_OBJECT_BEEN_CREATED(osBSDoorDummy)
							FREEZE_ENTITY_POSITION(osBSDoorDummy.ObjectIndex, FALSE)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(osBSDoorDummy.ObjectIndex, iLeadOutSceneID, "leadout_door", "missfam5leadinoutmcs_5", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
						ENDIF
						
						IF DOES_ENTITY_EXIST(psJimmy.PedIndex)
							IF NOT IS_ENTITY_DEAD(psJimmy.PedIndex)
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLeadOutSceneJimmyID)
								
									iLeadOutSceneJimmyID = CREATE_SYNCHRONIZED_SCENE(GET_WORLD_POSITION_OF_ENTITY_BONE(vsMichaelsCar.VehicleIndex,
																					 GET_ENTITY_BONE_INDEX_BY_NAME(vsMichaelsCar.VehicleIndex, "seat_pside_f")),
																					 GET_ENTITY_ROTATION(vsMichaelsCar.VehicleIndex))
								
									CLEAR_PED_TASKS_IMMEDIATELY(psJimmy.PedIndex)
									TASK_SYNCHRONIZED_SCENE(psJimmy.PedIndex, iLeadOutSceneJimmyID, "missfam5leadinoutmcs_5", "leadout_alt_jimmy",
															INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex, TRUE)
									
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(psDealer.PedIndex)
							IF NOT IS_ENTITY_DEAD(psDealer.PedIndex)
								CLEAR_PED_TASKS_IMMEDIATELY(psDealer.PedIndex)
								TASK_SYNCHRONIZED_SCENE(psDealer.PedIndex, iLeadOutSceneID, "missfam5leadinoutmcs_5", "leadout_dealer", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psDealer.PedIndex)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
								TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5leadinoutmcs_5", "leadout_mic", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
						ENDIF

					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
				
				START_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY")
				SET_PARTICLE_FX_CAM_INSIDE_VEHICLE(FALSE)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				IF ( bCutsceneSkipped = TRUE )	//cutscene was skipped
				
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
						SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
					ENDIF
				
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						SET_PED_INTO_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, VS_FRONT_RIGHT)
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
					ENDIF
					
					IF DOES_ENTITY_EXIST(psDealer.PedIndex)
						DELETE_PED(psDealer.PedIndex)
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					
					iStageProgress = 4			//go to last stage
					
				ELSE							//cutscene not skipped

					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLeadOutSceneID)

						iStageProgress++
						
					ENDIF

				ENDIF
				
			ELSE
			
				//if cutscene is skipped, fade out and remain cutscene on faded out screen
				IF ( bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
					
			ENDIF
			
		BREAK
		
		CASE 3
				
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLeadOutSceneID)
			
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_MCS5LO")
					IF HAS_LABEL_BEEN_TRIGGERED("FAM5_MCS5LO_P")
						//IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_MCS5LO", CONV_PRIORITY_MEDIUM)
						//IF PRELOAD_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_MCS5LO", CONV_PRIORITY_MEDIUM)
							BEGIN_PRELOADED_CONVERSATION()
							SET_LABEL_AS_TRIGGERED("FAM5_MCS5LO", TRUE)
						//ENDIF
					ENDIF
				ENDIF
			
				IF NOT DOES_ENTITY_EXIST(osCup.ObjectIndex)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadOutSceneID) > 0.09
						IF HAS_MISSION_OBJECT_BEEN_CREATED(osCup)
							IF DOES_ENTITY_EXIST(psDealer.PedIndex)
								IF NOT IS_ENTITY_DEAD(psDealer.PedIndex)
									ATTACH_ENTITY_TO_ENTITY(osCup.ObjectIndex, psDealer.PedIndex,
															GET_PED_BONE_INDEX(psDealer.PedIndex, BONETAG_PH_R_HAND),
															<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF ( bCupPassed = FALSE )
					IF DOES_ENTITY_EXIST(osCup.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osCup.ObjectIndex)
							IF DOES_ENTITY_EXIST(psDealer.PedIndex)
								IF NOT IS_ENTITY_DEAD(psDealer.PedIndex)
									IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadOutSceneID) > 0.285
										DETACH_ENTITY(osCup.ObjectIndex, FALSE)
										ATTACH_ENTITY_TO_ENTITY(osCup.ObjectIndex, psJimmy.PedIndex,
																GET_PED_BONE_INDEX(psJimmy.PedIndex, BONETAG_PH_L_HAND),
																<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
										bCupPassed = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadOutSceneID) > 0.575	//when Jimmy has entered the car, closed doors and is sitting in car
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						IF IS_ENTITY_PLAYING_ANIM(vsMichaelsCar.VehicleIndex, "missfam5leadinoutmcs_5", "leadout_car_door")
							
							STOP_ENTITY_ANIM(vsMichaelsCar.VehicleIndex, "leadout_car_door", "missfam5leadinoutmcs_5", SLOW_BLEND_OUT)
							SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
							
							CLEAR_PED_TASKS(psJimmy.PedIndex)													//stop synched scene on Jimmy
							SET_PED_INTO_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, VS_FRONT_RIGHT)	//play in car anim on Jimmy
							TASK_PLAY_ANIM(psJimmy.PedIndex, "missfam5leadinoutmcs_5", "leadout_alt_jimmy", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_HOLD_LAST_FRAME, GET_SYNCHRONIZED_SCENE_PHASE(iLeadOutSceneID))
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadOutSceneID) >= 1.0 //> 0.99
			
					IF DOES_CAM_EXIST(ScriptedCamera)
						IF IS_CAM_RENDERING(ScriptedCamera)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						ENDIF
					ENDIF
			
					DISPLAY_RADAR(TRUE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_CAM(ScriptedCamera)
					DESTROY_ALL_CAMS()
					
					KILL_CHASE_HINT_CAM(sCinematicCamStruct)

					IF DOES_ENTITY_EXIST(psDealer.PedIndex)
						IF NOT IS_ENTITY_DEAD(psDealer.PedIndex)
							SET_ENTITY_INVINCIBLE(psDealer.PedIndex, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psDealer.PedIndex, TRUE)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(psJimmy.PedIndex)
						IF NOT IS_ENTITY_DEAD(psJimmy.PedIndex)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							IF NOT IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
								SET_PED_INTO_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, VS_FRONT_RIGHT)
							ENDIF
							TASK_PLAY_ANIM(psJimmy.PedIndex, "missfam5_drink", "Drink_Michael&Jimmy_JimmyIdle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
					
					iStageProgress++

				ENDIF
			
			ENDIF
				
		BREAK
		
		CASE 4

			IF IS_AUDIO_SCENE_ACTIVE("FAMILY_5_TAKE_JIMMY_FOCUS_CAM")
				STOP_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY_FOCUS_CAM")
			ENDIF

			IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_TAKE_JIMMY")
				START_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY")
			ENDIF

			SET_MODEL_AS_NO_LONGER_NEEDED(osCup.ModelName)
			SET_MODEL_AS_NO_LONGER_NEEDED(osBSDoor.ModelName)
			SET_MODEL_AS_NO_LONGER_NEEDED(osBSDoorDummy.ModelName)
			
			REMOVE_ANIM_DICT("missfam5leadinoutmcs_5")
		
			DISPLAY_RADAR(TRUE)
			
			KILL_CHASE_HINT_CAM(sCinematicCamStruct)
		
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			
			iStageProgress++
			
		BREAK
		
		CASE 5
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
				IF DOES_ENTITY_EXIST(psDealer.PedIndex)
					DELETE_PED(psDealer.PedIndex)
				ENDIF
				
				REPLAY_STOP_EVENT()
			
				RETURN TRUE
			
			ENDIF
		
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_DURING_RIDE_HOME(INT &iProgress, INT &iTimer, STRING sRoot1, STRING sRoot2, STRING sRoot3)

	IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
	AND NOT IS_PED_INJURED(psJimmy.PedIndex)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())

		IF ( bConversationsFinished = FALSE )		//random conversations allowed
			
			IF ( iProgress > 0 )
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				
					
					//resume paused conversations when player gets back in the car
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					ENDIF

				
				ELSE //locates header blip does not exist, player is out of the car or wanted
					
					
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						//don't pause the conversations when wanted
					ELSE
						//pause the conversations when not wanted
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ENDIF

				ENDIF
				
			ENDIF
			
			SWITCH iProgress

				CASE 0

						IF 	IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
						AND IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
				
							IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_SON6")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_SON6", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("FAM5_SON6", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF HAS_LABEL_BEEN_TRIGGERED(sLabelOBJ)		//check if objective label has been printed or triggered
									OR IS_STRING_NULL_OR_EMPTY(sLabelOBJ)
										iTimer = GET_GAME_TIMER()
										iProgress++
									ENDIF
								ENDIF					
							ENDIF
							
						ENDIF

					BREAK
					
				CASE 1

						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1184.633423,-874.228088,17.855732>>, <<-1168.236450,-901.460083,7.064990>>, 35.0)
					
							IF 	IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
							AND IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
					
								IF NOT HAS_LABEL_BEEN_TRIGGERED(sRoot1)
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sRoot1, CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED(sRoot1, TRUE)
												iProgress++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						
						ELSE
						
							IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_DRIVEJ")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF 	IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
										AND IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
											IF HAS_TIME_PASSED(15000, iTimer)
												IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_DRIVEJ", CONV_PRIORITY_MEDIUM)
													iDriveReminders++
													iTimer = GET_GAME_TIMER()
													
													IF ( iDriveReminders = 2 )
														SET_LABEL_AS_TRIGGERED("FAM5_DRIVEJ", TRUE)
													ENDIF
													
												ENDIF
											ENDIF
										ENDIF	
									ENDIF
								ENDIF
							ENDIF
						
						ENDIF
					BREAK
					
				CASE 2
				
						IF ( bPreDrinkConversationFinished = FALSE )								//check if a different conversation
							IF HAS_LABEL_BEEN_TRIGGERED(sRoot1)										//is ongoing or queued
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									bPreDrinkConversationFinished = TRUE
								ENDIF
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							//after Root2 was triggered
								    TEXT_LABEL txtRoot
									txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								    IF NOT ARE_STRINGS_EQUAL(sRoot1, txtRoot)						//that means Root2 was finished
								    	bPreDrinkConversationFinished = TRUE
								    ENDIF
								ENDIF
							ENDIF
						ENDIF
				

						IF 	IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
						AND IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
						
							IF ( bDrinkAnimationFinished = TRUE )
				
								IF NOT HAS_LABEL_BEEN_TRIGGERED(sRoot2)
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sRoot2, CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED(sRoot2, TRUE)
												
												iTimer = GET_GAME_TIMER()
												
												iProgress++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
							
						ENDIF
					BREAK
					
				CASE 3
					
						IF ( bPostDrinkConversationFinished = FALSE )								//check if a different conversation
							IF HAS_LABEL_BEEN_TRIGGERED(sRoot2)										//is ongoing or queued
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							//after Root2 was triggered
								    TEXT_LABEL txtRoot
									txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								    IF NOT ARE_STRINGS_EQUAL(sRoot2, txtRoot)						//that means Root2 was finished
								    	bPostDrinkConversationFinished = TRUE
								    ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						IF 	IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
						AND IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
				
							IF HAS_TIME_PASSED(6000, iTimer)
				
								IF NOT HAS_LABEL_BEEN_TRIGGERED(sRoot3)
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", sRoot3, CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED(sRoot3, TRUE)
												iTimer = 0
												iProgress++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
							
						ENDIF
							
					BREAK
					
				CASE 4	//random lines and kick out of car conversations

					IF 	IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
					AND IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
					
						IF GET_GAME_TIMER() - iTimer > 0
				
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
									SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
								
										CASE 0
								
											IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_DRUNKM")
												IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_DRUNKM", CONV_PRIORITY_MEDIUM)
													
													iMichaelDrunkConversationCounter++
													iTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 10000)
													
													IF ( iMichaelDrunkConversationCounter >= 9 )
														SET_LABEL_AS_TRIGGERED("FAM5_DRUNKM", TRUE)
													ENDIF
												ENDIF
											ENDIF
										
										BREAK
										
										CASE 1
								
											IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_DRUNKJ")
												IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_DRUNKJ", CONV_PRIORITY_MEDIUM)
												
													iJimmyDrunkConversationCounter++
													iTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 10000)
													
													IF ( iJimmyDrunkConversationCounter >= 3 )
														SET_LABEL_AS_TRIGGERED("FAM5_DRUNKJ", TRUE)
													ENDIF
												ENDIF
											ENDIF
										
										BREAK
										
									ENDSWITCH
									
								ENDIF
							ENDIF

						ENDIF
					
					ENDIF
				
				BREAK
				
			ENDSWITCH
		
		ELSE									//random conversations not allowed, kick out of car scene has started
		
			//play stop conversation
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_DSTOP")
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					ENDIF
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_DSTOP", CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("FAM5_DSTOP", TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_CAR")
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
							IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_CAR", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("FAM5_CAR", TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF

	ENDIF

ENDPROC

PROC MANAGE_ANIMATIONS_AND_SFX_DURING_RIDE_HOME(INT &iProgress, STRING sRoot1, STRING sRoot2)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_drink", "Drink_Michael&Jimmy_Plyer0")
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF

	SWITCH iProgress
	
		CASE 0	//play Jimmy's idle in car animation
		
			IF NOT IS_PED_INJURED(psJimmy.PedIndex)
				IF NOT IS_ENTITY_PLAYING_ANIM(psJimmy.PedIndex, "missfam5_drink", "Drink_Michael&Jimmy_JimmyIdle")
					TASK_PLAY_ANIM(psJimmy.PedIndex, "missfam5_drink", "Drink_Michael&Jimmy_JimmyIdle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psJimmy.PedIndex, TRUE)
			ENDIF
			
			//start loading the drink sfx
			LOAD_MISSION_SFX(MISSION_SFX_DRINK)
			
			iProgress++
		
		BREAK
		
		CASE 1	//play Jimmy's and Michael's in car drink pass animation
		
			IF 	IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
			AND NOT IS_PED_INJURED(psJimmy.PedIndex)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		
				//start the anim after "You want a drink?" conversation has been started
				IF HAS_LABEL_BEEN_TRIGGERED(sRoot1)
				
					IF ( IS_THIS_CONVERSATION_PLAYING(sRoot1) AND GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 5 )
					OR ( bPreDrinkConversationFinished = TRUE ) 
			
						IF 	IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
						AND IS_PED_SITTING_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)

							DISABLE_CELLPHONE_THIS_FRAME_ONLY()

							SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, FALSE)
							SET_VEHICLE_DOORS_LOCKED(vsMichaelsCar.VehicleIndex, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
						
							TASK_PLAY_ANIM(psJimmy.PedIndex, "missfam5_drink", "Drink_Michael&Jimmy_Jimmy")
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5_drink", "Drink_Michael&Jimmy_Plyer0", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
		
							
		
							iProgress++
		
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 2
						
			//Play drink sfx
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_drink", "Drink_Michael&Jimmy_Plyer0")
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missfam5_drink", "Drink_Michael&Jimmy_Plyer0") > 0.389
				OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("Drink_Sfx"))
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Drink_Sfx animation event fired.")
					#ENDIF
				
					PLAY_MISSION_SFX(MISSION_SFX_DRINK)
					ANIMPOSTFX_PLAY("DrugsDrivingIn", 0, FALSE)
					
					iProgress++
					
				ENDIF
				
			ENDIF
						
		BREAK
		
		CASE 3
		
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_drink", "Drink_Michael&Jimmy_Plyer0")
			
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missfam5_drink", "Drink_Michael&Jimmy_Plyer0") > 0.703
				OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("Interrupt"))
			
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Interrupt animation event fired.")
					#ENDIF
				
					bDrinkAnimationFinished = TRUE
				
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND NOT IS_PED_INJURED(psJimmy.PedIndex)
					AND IS_PED_IN_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vsMichaelsCar.VehicleIndex)
					
						STOP_ANIM_PLAYBACK(PLAYER_PED_ID(), AF_PRIORITY_HIGH, TRUE)
						CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
						
						STOP_ANIM_PLAYBACK(psJimmy.PedIndex)
						
						TASK_PLAY_ANIM(psJimmy.PedIndex, "missfam5_drink", "Drink_Michael&Jimmy_JimmyIdle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					
						SET_VEHICLE_DOORS_LOCKED(vsMichaelsCar.VehicleIndex, VEHICLELOCK_UNLOCKED)
						
						iProgress++
					
					ENDIF
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 4
		
			IF HAS_LABEL_BEEN_TRIGGERED(sRoot2)
				
				IF ( IS_THIS_CONVERSATION_PLAYING(sRoot2) AND( GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 4 ) )
				OR ( bPostDrinkConversationFinished = TRUE )
				
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						MAKE_PED_DRUNK_CONSTANT(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Making player ped drunk constant.")
						#ENDIF
					ENDIF
					
					DISABLE_CELLPHONE(TRUE)

					PLAY_MISSION_SFX(MISSION_SFX_HEARTBEAT)
					
					STOP_AUDIO_SCENE("FAMILY_5_TAKE_JIMMY")
					
					START_AUDIO_SCENE("FAMILY_5_YOGA_ON_DRUGS")
					
					START_AUDIO_SCENE("FAMILY_5_DRUGS")
					
					SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)		//block all shops when drunk
					
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN, FALSE)
					
					iKickOutDelayTimer = GET_GAME_TIMER()
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting iKickOutDelayTimer.")
					#ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iProgress++
					
				ENDIF
				
			ENDIF
		
		BREAK
		
	ENDSWITCH

ENDPROC

PROC UPDATE_DRUNK_CAMERA(FLOAT fStartShake = 0.0, FLOAT fStartBlur = 0.0, INT iTimeDelay = 3000, FLOAT fMaxShake = 3.0, FLOAT fMaxBlur = 3.0)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF Is_Ped_Drunk(PLAYER_PED_ID())
		
			IF NOT IS_GAMEPLAY_CAM_SHAKING()
				
				fDrunkCameraShake = fStartShake
				fDrunkCameraBlur = fStartBlur
				
				Activate_Drunk_Camera_Constant(DRUNK_DEFAULT_TIMEOUT_msec, fDrunkCameraShake, fDrunkCameraBlur)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Activating drunk camera with amplitude equal ", fDrunkCameraShake, " and blur equal ", fDrunkCameraBlur, ".")
				#ENDIF
				
				SETTIMERA(0)

			ELSE
			
				fDrunkCameraShake = CLAMP(fDrunkCameraShake +@0.1, fStartShake, fMaxShake)
				fDrunkCameraBlur = CLAMP(fDrunkCameraBlur +@0.12, fStartBlur, fMaxBlur)
						
				IF (( fDrunkCameraShake < fMaxShake ) OR ( fDrunkCameraBlur < fMaxBlur))
				AND ( TIMERA() > iTimeDelay )
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting drunk camera shake to ", fDrunkCameraShake, " and blur to ", fDrunkCameraBlur, ".")
					#ENDIF

					Update_Drunk_Camera_ParamDesiredAmplitude(fDrunkCameraShake)
					Update_Drunk_Camera_ParamStrength(fDrunkCameraBlur)
					
					SETTIMERA(0)
					
				ENDIF
			
			ENDIF
		
		ENDIF
	
	ENDIF

ENDPROC

PROC RUN_VEHICLE_AND_CAMERA_CHECK()

	IF DOES_CAM_EXIST(ScriptedCamera)
		IF IS_CAM_RENDERING(ScriptedCamera)
			IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex) AND NOT IS_PED_INJURED(psJimmy.PedIndex)
				IF 	NOT IS_ENTITY_PLAYING_ANIM(psJimmy.PedIndex, "missfam5mcs_6", "push_michael_out_jimmy")
				AND NOT IS_ENTITY_PLAYING_ANIM(vsMichaelsCar.VehicleIndex, "missfam5mcs_6", "Push_Michael_Out_Car")
			
					IF IS_POINT_IN_ANGLED_AREA(GET_CAM_COORD(ScriptedCamera), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, << 0.0, 2.65, -0.65>>),
													 						  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, << 0.0, -2.65, 1.0>>), 2.25)
						
						IF IS_ENTITY_VISIBLE(vsMichaelsCar.VehicleIndex)
						
							SET_ENTITY_VISIBLE(vsMichaelsCar.VehicleIndex, FALSE)
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Scripted camera inside angled area around michael's car. Setting michael's car invisible")
							#ENDIF
							
						ENDIF
					
					ELSE
					
						IF NOT IS_ENTITY_VISIBLE(vsMichaelsCar.VehicleIndex)
						
							SET_ENTITY_VISIBLE(vsMichaelsCar.VehicleIndex, TRUE)
					
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Scripted camera inside angled area around michael's car. Setting michael's car visible")
							#ENDIF
							
						ENDIF
					
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_DRIVE_HOME_COMPLETED(INT &iStageProgress)
	
	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
	SET_BIT(sLocatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
	SET_BIT(sLocatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)	
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF

	MANAGE_ANIMATIONS_AND_SFX_DURING_RIDE_HOME(psJimmy.iProgress, PICK_STRING(bStageReplayInProgress, "FAM5_SON7b", "FAM5_SON7"),
																  PICK_STRING(bStageReplayInProgress, "FAM5_SON8b", "FAM5_SON8"))
	MANAGE_CONVERSATIONS_DURING_RIDE_HOME(iConversationProgress, psJimmy.iPedConversationTimer,
										  PICK_STRING(bStageReplayInProgress, "FAM5_SON7b", "FAM5_SON7"),
										  PICK_STRING(bStageReplayInProgress, "FAM5_SON8b", "FAM5_SON8"),
										  PICK_STRING(bStageReplayInProgress, "FAM5_SON9b", "FAM5_SON9"))
																			
	UPDATE_DRUNK_CAMERA(0.5, 1.0, 2500, 2.0, 2.0)
	UPDATE_TRIGGERED_LABEL(sLabelOBJ)
	
	BOOL bTimePassed
	
	IF DOES_ENTITY_EXIST(osBSDoorDummy.ObjectIndex)
		IF NOT IS_ENTITY_DEAD(osBSDoorDummy.ObjectIndex)
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), osBSDoorDummy.ObjectIndex) > 15.0
				IF NOT IS_ENTITY_ON_SCREEN(osBSDoorDummy.ObjectIndex)
					DELETE_OBJECT(osBSDoorDummy.ObjectIndex)
					REMOVE_MODEL_HIDE(osBSDoor.vPosition, 1.0, osBSDoor.ModelName)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF 	IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
	AND NOT IS_PED_INJURED(psJimmy.PedIndex)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		SWITCH iStageProgress
		
			CASE 0
			
				sLabelOBJ = "FAM5_GT4"
				
				IF HAS_LABEL_BEEN_TRIGGERED("FAM5_SON6")
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF ( GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 2 )
							iStageProgress++
						ENDIF
					ELSE
						iStageProgress++
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 1
			
				REQUEST_ANIM_DICT("missfam5mcs_6")
			
				//display the loctates header objective text and blip		
				IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData, vMichaelsHouseCoords, g_vAnyMeansLocate , FALSE, psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, sLabelOBJ, "CMN_JLEAVE", sLabelMGETIN, "CMN_GENGETBCKY", FALSE, TRUE)
			
				IF ( iKickOutDelayTimer <> 0 )
					
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsMichaelsCar.VehicleIndex, GET_STRING_FROM_INT(CLAMP_INT(iKickOutDelayTime - ( GET_GAME_TIMER() - iKickOutDelayTimer ), 0, iKickOutDelayTime)), 2.25, 0, 255, 0)
					#ENDIF
				
					IF  HAS_TIME_PASSED(iKickOutDelayTime, iKickOutDelayTimer)
						bTimePassed = TRUE	
					ENDIF
				ENDIF					
				
				IF ARE_PEDS_IN_VEHICLE_AT_SAFE_POSITION_FOR_INGAME_CUTSCENE(PLAYER_PED_ID(), psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
					IF ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMichaelsHouseCoords) < fKickAnimationTriggerDistance )
					OR ( bTimePassed = TRUE )

						bConversationsFinished = TRUE
						
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							ENDIF
						ENDIF

						CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
													
						RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX_DRINK)
						
						iStageProgress++
						
					ENDIF
				ENDIF

			BREAK
			
			CASE 2

				REQUEST_ANIM_DICT("missfam5mcs_6")
				
				SET_IN_VEHICLE_CAM_STATE_THIS_UPDATE(vsMichaelsCar.VehicleIndex, CAM_INSIDE_VEHICLE)
				
				IF HAS_ANIM_DICT_LOADED("missfam5mcs_6")

					IF IS_VEHICLE_ON_ALL_WHEELS(vsMichaelsCar.VehicleIndex)
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vsMichaelsCar.VehicleIndex, 5.0)
							
							IF 	IS_VEHICLE_ON_ALL_WHEELS(vsMichaelsCar.VehicleIndex)
							AND NOT IS_ENTITY_ON_FIRE(vsMichaelsCar.VehicleIndex)

								INT		iTotalLanes
								FLOAT	fHeading
								FLOAT	fVehicleHeading
								VECTOR	vVehiclePosition
								VECTOR 	vClosestNodeToVehicle
							
								vVehiclePosition 	= GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex)
								fVehicleHeading		= GET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex)
							
								IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vVehiclePosition, 1, vClosestNodeToVehicle, fHeading, iTotalLanes, NF_NONE)
							
									SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vClosestNodeToVehicle)
									SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, fVehicleHeading)
									SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
							
								ENDIF

								CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 2.0)
								CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 4.0)
								CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 3.0)
								
								//remove cars behind michael's car in case any cars don't manage to stop in time and drive into the scene
								CLEAR_AREA_OF_VEHICLES(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<0.00000, -4.000, 0.1689>>), 3.5)
							
								iSpeedZone = ADD_ROAD_NODE_SPEED_ZONE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 15.0, 0.0)
								
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
									
									iKickOutSceneID = CREATE_SYNCHRONIZED_SCENE(<< 0.0, 0.0,0.0>>, << 0.0, 0.0,0.0>>)
										
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iKickOutSceneID, vsMichaelsCar.VehicleIndex, 0)
							
									DESTROY_ALL_CAMS()
									ScriptedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
									PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iKickOutSceneID, "Push_Michael_Out_Cam", "missfam5mcs_6")
							
									DISPLAY_HUD(FALSE)
									DISPLAY_RADAR(FALSE)
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
									PLAY_SYNCHRONIZED_ENTITY_ANIM(vsMichaelsCar.VehicleIndex, iKickOutSceneID, "Push_Michael_Out_Car", "missfam5mcs_6", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
									SET_ENTITY_COLLISION(vsMichaelsCar.VehicleIndex, FALSE)
									
									CLEAR_PED_TASKS_IMMEDIATELY(psJimmy.PedIndex)
									TASK_SYNCHRONIZED_SCENE(psJimmy.PedIndex, iKickOutSceneID, "missfam5mcs_6", "push_michael_out_jimmy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_EXPLOSION)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
									SET_RAGDOLL_BLOCKING_FLAGS(psJimmy.PedIndex, RBF_EXPLOSION)
									
									IF DOES_ENTITY_EXIST(osCup.ObjectIndex)
										IF NOT IS_ENTITY_DEAD(osCup.ObjectIndex)
											IF IS_ENTITY_ATTACHED(osCup.ObjectIndex)
												DETACH_ENTITY(osCup.ObjectIndex, FALSE, TRUE)
											ENDIF
											ATTACH_ENTITY_TO_ENTITY(osCup.ObjectIndex, psJimmy.PedIndex, GET_PED_BONE_INDEX(psJimmy.PedIndex, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
										ENDIF
									ENDIF
									
									MAKE_PED_SOBER(PLAYER_PED_ID())
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iKickOutSceneID, "missfam5mcs_6", "push_michael_out_player0",
															INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_EXPLOSION, INSTANT_BLEND_IN, AIK_DISABLE_LEG_IK)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_EXPLOSION)
									
									psJimmy.bHasTask 				= FALSE
									bAnimationForced				= FALSE
									bAnimationsSwitched				= FALSE
									bPlayerRagdolledEarly 			= FALSE
									bCleanupVehiclesForCamera		= FALSE
									bCleanupVehiclesForCameraAgain	= FALSE
									
									SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)
									
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
									
									//Activate multihead blinders
									SET_MULTIHEAD_SAFE(TRUE,FALSE)
									
									iStageProgress++
									
								ENDIF
							
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			
			BREAK

			CASE 3
			
				RUN_VEHICLE_AND_CAMERA_CHECK()
			
				REQUEST_ANIM_DICT("missfam5mcs_6drag")
			
				IF NOT IS_PED_INJURED(psJimmy.PedIndex)
					SET_PED_RESET_FLAG(psJimmy.PedIndex, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
				ENDIF

				IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)

					DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
					DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) <= 0.630
						SET_IN_VEHICLE_CAM_STATE_THIS_UPDATE(vsMichaelsCar.VehicleIndex, CAM_INSIDE_VEHICLE)
					ENDIF
					
					SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vsMichaelsCar.VehicleIndex, TRUE)
				
					IF HAS_ANIM_EVENT_FIRED(psJimmy.PedIndex, GET_HASH_KEY("Detach"))
						IF DOES_ENTITY_EXIST(osCup.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osCup.ObjectIndex)
								IF IS_ENTITY_ATTACHED(osCup.ObjectIndex)
									DETACH_ENTITY(osCup.ObjectIndex, FALSE, TRUE)
								ENDIF
								DELETE_OBJECT(osCup.ObjectIndex)
							ENDIF
						ENDIF
					ENDIF

					IF DOES_CAM_EXIST(ScriptedCamera)
					
						IF NOT IS_CAM_RENDERING(ScriptedCamera)
							DISPLAY_HUD(FALSE)
							DISPLAY_RADAR(FALSE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_DRUGS_FOCUS_CAM")
								STOP_AUDIO_SCENE("FAMILY_5_DRUGS")
								START_AUDIO_SCENE("FAMILY_5_DRUGS_FOCUS_CAM")
							ENDIF
						ELSE
							
							IF ( bCleanupVehiclesForCamera = FALSE )//cleanup vehicles near the camera and michael's animation positions
								CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-0.7309, 2.7140, 0.4813>>), 3.0)
								CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-3.6668, 0.1784, 0.4859>>), 3.0)
								CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-2.1213, 0.5363, 0.4975>>), 3.0)
								CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-1.4263, 1.9211, 4.8319>>), 3.0)
								CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-3.342, -0.523, -0.1449>>), 3.0)
								CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<0.0000, 4.5000, 0.25000>>), 3.0)
								CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-2.0859, 4.1187, 0.1689>>), 3.0)
								CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<0.00000, -4.250, 0.1689>>), 3.0)
								bCleanupVehiclesForCamera = TRUE
							ENDIF
							
						ENDIF
						
					ENDIF				
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) > 0.75
							
						IF ( psJimmy.bHasTask = FALSE )
							
							KILL_CHASE_HINT_CAM(sCinematicCamStruct)
						
							STOP_SYNCHRONIZED_ENTITY_ANIM(vsMichaelsCar.VehicleIndex, INSTANT_BLEND_OUT, TRUE)
							SET_ENTITY_COLLISION(vsMichaelsCar.VehicleIndex, TRUE)
							
							CLEAR_PED_TASKS(psJimmy.PedIndex)
							SET_PED_INTO_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, VS_DRIVER)
							TASK_VEHICLE_MISSION_PED_TARGET(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, PLAYER_PED_ID(), MISSION_FLEE, 25.0, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_SteerAroundPeds , 25.0, 2.0)
							psJimmy.iTimer = GET_GAME_TIMER()
							
							FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 			= FALSE
							FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= FALSE
							FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE] 	= FALSE
							
							psJimmy.bHasTask = TRUE
							
						ENDIF
					
					ELSE
					
						//trigger another vehicle cleanup around the car
						IF bCleanupVehiclesForCameraAgain = FALSE
							IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) >= 0.350
								bCleanupVehiclesForCamera = FALSE
								bCleanupVehiclesForCameraAgain = TRUE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting flag bCleanupVehiclesForCamera to FALSE for another vehicle cleanup to run.")
							ENDIF
						ENDIF
						
					ENDIF
										
				ENDIF
				
				IF ( bAnimationsSwitched = FALSE )
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) > 0.6275
							DETACH_SYNCHRONIZED_SCENE(iKickOutSceneID)
							SET_SYNCHRONIZED_SCENE_ORIGIN(iKickOutSceneID, GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), GET_ENTITY_ROTATION(vsMichaelsCar.VehicleIndex))
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5mcs_6", "push_michael_out_player0", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_ACTIVATE_RAGDOLL_ON_COLLISION, GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID), FALSE, AIK_DISABLE_LEG_IK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							bAnimationsSwitched = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF ( bAnimationForced = FALSE )
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) >= fAnimationForcePhase
							
							IF ( bPlayerRagdolledEarly = TRUE )
							
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iKickOutSceneID, "missfam5mcs_6", "push_michael_out_player0",
														INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_VEHICLE_IMPACT)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								
								CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1.0)
								REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								bAnimationForced = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ( bPlayerRagdolledEarly = FALSE )					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)							
						IF IS_PED_RAGDOLL(PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player ped is ragdolling out of the synched scene early.")
							#ENDIF
							SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 10000, 10000, TASK_RELAX)
							bPlayerRagdolledEarly = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_GETTING_UP(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Making player ped ragdoll as player ped was getting up.")
					#ENDIF
					SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 20000, 20000, TASK_RELAX)
				ENDIF
				
				IF ( bPlayerRagdolledEarly = FALSE )
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) > 0.99
							IF IS_PED_RAGDOLL(PLAYER_PED_ID())
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Kick out of car scene finished, player ragdolling normally.")
								#ENDIF
								REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								iStageProgress++
							ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Kick out of car scene finished, player not ragdolling.")
								#ENDIF
								REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
				
				ELIF ( bPlayerRagdolledEarly = TRUE )
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) > 0.99
							iStageProgress++
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Kick out of car scene finished with player ragdolling early.")
						#ENDIF
						iStageProgress++
					ENDIF
				
				ENDIF

			BREAK
			
			CASE 4
			
				RUN_VEHICLE_AND_CAMERA_CHECK()
			
				iKickOutSceneID = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_ROTATION(PLAYER_PED_ID()))
				
				SET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID, 0.0)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iKickOutSceneID, TRUE)
				
				CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1.5)
				CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1.5)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iKickOutSceneID, "missfam5mcs_6drag", "fam_5_mcs_6_drag_michael",
											INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT,
											RBF_VEHICLE_IMPACT, INSTANT_BLEND_IN, AIK_DISABLE_LEG_IK)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				ENDIF
				
				PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iKickOutSceneID, "fam_5_mcs_6_drag_cam", "missfam5mcs_6drag")
				
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			BREAK
			
			CASE 5
			
				RUN_VEHICLE_AND_CAMERA_CHECK()
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
				
					IF NOT DOES_CAM_EXIST(UndergroundCamera)
						UndergroundCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
						SET_CAM_PARAMS(UndergroundCamera, <<-20.194494,-219.796371,37.580395>>, <<-81.945038,-1.126925,-11.828329>>, 29.0)
					ENDIF
				
					IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) >= 0.99
						
						IF DOES_CAM_EXIST(UndergroundCamera)
							SET_CAM_ACTIVE(UndergroundCamera, TRUE)
						ENDIF
						
						iStageProgress++
						
					ENDIF
				ENDIF
			
			BREAK
			
			CASE 6

				RUN_VEHICLE_AND_CAMERA_CHECK()
				
				IF DOES_CAM_EXIST(UndergroundCamera) AND IS_CAM_RENDERING(UndergroundCamera)
							
					iStageProgress++

				ENDIF
			
			BREAK
				
			CASE 7
			
				RUN_VEHICLE_AND_CAMERA_CHECK()
				
				IF DOES_CAM_EXIST(UndergroundCamera) AND IS_CAM_RENDERING(UndergroundCamera)
				
					SET_CINEMATIC_MODE_ACTIVE(FALSE)
					KILL_CHASE_HINT_CAM(sCinematicCamStruct)
				
					REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedZone)
				
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(psJimmy.PedIndex)
					
					DELETE_PED(psJimmy.PedIndex)
					DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
					
					STOP_AUDIO_SCENE("FAMILY_5_DRUGS")
					STOP_AUDIO_SCENE("FAMILY_5_DRUGS_FOCUS_CAM")
					
					REMOVE_ANIM_DICT("missfam5mcs_6")
					REMOVE_ANIM_DICT("missfam5_drink")
					REMOVE_ANIM_DICT("missfam5mcs_6drag")
					
					ANIMPOSTFX_STOP("DrugsDrivingIn")
					ANIMPOSTFX_PLAY("DMT_flight", 0, TRUE)
					
					QUIT_DRUNK_CAMERA_IMMEDIATELY()
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE)
					
					RETURN TRUE
				
				ENDIF			
			BREAK
			
		ENDSWITCH
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_BLACKOUT_COMPLETED(INT &iStageProgress)
	
	SET_CLOCK_TIME(22, 0, 0)
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	SWITCH iStageProgress
	
		CASE 0

			MAKE_PED_SOBER(PLAYER_PED_ID())
			QUIT_DRUNK_CAMERA_IMMEDIATELY()
						
			WARP_PED(PLAYER_PED_ID(), MissionPosition.vStageStart, MissionPosition.fStartHeading, FALSE, TRUE, FALSE)
			
			SET_WEATHER_TYPE_NOW_PERSIST("CLOUDS")
			
			iStageProgress++
			
		BREAK
		
		CASE 1
		
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
							
				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20.0)
				
				iLoadSceneTimer = GET_GAME_TIMER()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
				#ENDIF
				
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Load scene timer: ", GET_GAME_TIMER() - iLoadSceneTimer, ".")
			#ENDIF
			
			IF IS_NEW_LOAD_SCENE_LOADED()
			OR HAS_TIME_PASSED(10000, iLoadSceneTimer)

					#IF IS_DEBUG_BUILD
						IF HAS_TIME_PASSED(10000, iLoadSceneTimer)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to iLoadSceneTimer timeout.")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to IS_NEW_LOAD_SCENE_LOADED() returning TRUE.")
						ENDIF
					#ENDIF
				
				NEW_LOAD_SCENE_STOP()
				
				SET_CUTSCENE_AUDIO_OVERRIDE("_CUSTOM")	//needed for B*1572859
						
				iStageProgress++
			
			ENDIF
		
		BREAK
	
		CASE 2
			
			REQUEST_IPL("SpaceInterior")
			
			REQUEST_CUTSCENE("fam_5_mcs_6")
		
			//IF HAS_REQUESTED_CUTSCENE_LOADED("fam_5_mcs_6")	//don't want fade out here
			IF HAS_THIS_CUTSCENE_LOADED("fam_5_mcs_6")
						
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT,FALSE)
				
				ANIMPOSTFX_STOP("DMT_flight")
				SET_TIMECYCLE_MODIFIER("stoned_monkeys")
				
				START_CUTSCENE(CUTSCENE_CREATE_OBJECTS_AT_SCENE_ORIGIN | CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien", PED_COMP_HEAD, 		0, 0, S_M_M_MOVALIEN_01)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien", PED_COMP_TORSO, 	0, 0, S_M_M_MOVALIEN_01)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien", PED_COMP_LEG, 		0, 0, S_M_M_MOVALIEN_01)
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^1", PED_COMP_HEAD, 	0, 0, S_M_M_MOVALIEN_01)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^1", PED_COMP_TORSO, 	0, 0, S_M_M_MOVALIEN_01)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^1", PED_COMP_LEG, 	0, 0, S_M_M_MOVALIEN_01)
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^2", PED_COMP_HEAD, 	0, 0, S_M_M_MOVALIEN_01)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^2", PED_COMP_TORSO, 	0, 0, S_M_M_MOVALIEN_01)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^2", PED_COMP_LEG, 	0, 0, S_M_M_MOVALIEN_01)
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^3", PED_COMP_HEAD, 	0, 0, S_M_M_MOVALIEN_01)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^3", PED_COMP_TORSO, 	0, 0, S_M_M_MOVALIEN_01)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Movie_Alien^3", PED_COMP_LEG, 	0, 0, S_M_M_MOVALIEN_01)
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 3
		
			IF IS_CUTSCENE_PLAYING()
			
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				NEW_LOAD_SCENE_START_SPHERE(<< 41.1, -779.8, 837.0 >>, 20.0)

				CLEAR_AREA(MissionPosition.vStageStart, 25.0, TRUE)
				CLEAR_AREA_OF_PEDS(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_OBJECTS(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_PROJECTILES(MissionPosition.vStageStart, 25.0)
				REMOVE_PARTICLE_FX_IN_RANGE(MissionPosition.vStageStart, 25.0)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 4
		
			REQUEST_ANIM_DICT("missfam5_flying")
			REQUEST_SCRIPT_AUDIO_BANK("FAM5_MICHAEL_LONG_SCREAM")
			
			IF IS_CUTSCENE_PLAYING()
				SWITCH iVFXProgress
					CASE 0
						IF GET_CUTSCENE_TIME() >= 30465
							CLEAR_TIMECYCLE_MODIFIER()
							SET_TIMECYCLE_MODIFIER("stoned_aliens")
							iVFXProgress++
						ENDIF
					BREAK
					CASE 1
						IF GET_CUTSCENE_TIME() >= 48565
							CLEAR_TIMECYCLE_MODIFIER()
							ANIMPOSTFX_PLAY("DMT_flight_intro", 0, FALSE)
							iVFXProgress++
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF GET_CUTSCENE_TIME() >= 28500
					IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BARECHEST_BOXERS)
						SET_PLAYER_PED_OUTFIT(MO_UNDERWEAR)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0, 0)
				
					WARP_PED(PLAYER_PED_ID(), << -2100.000, 700.325, 1200.000 >>, 128.0, FALSE, FALSE, FALSE)
						
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFlyingSceneID)
			
						iFlyingSceneID = CREATE_SYNCHRONIZED_SCENE(<< -900.000, 100.325, 800.000 >>, << 0.0, 0.0, -139.210 >>)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iFlyingSceneID, "missfam5_flying", "falling_to_skydive", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						
						DESTROY_ALL_CAMS()
						AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						PLAY_SYNCHRONIZED_CAM_ANIM(AnimatedCamera, iFlyingSceneID, "falling_to_skydive_cam", "missfam5_flying")
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
					ENDIF

				ENDIF
				
			ENDIF
		
			IF HAS_CUTSCENE_FINISHED()
				
				NEW_LOAD_SCENE_STOP()
				REMOVE_IPL("SpaceInterior")
				
				CLEAR_TIMECYCLE_MODIFIER()
				
				IF ( bCutsceneSkipped = TRUE )
					IF REQUEST_SCRIPT_AUDIO_BANK("FAM5_MICHAEL_LONG_SCREAM")
						PLAY_SOUND_FRONTEND(-1, "MICHAEL_LONG_SCREAM", "FAMILY_5_SOUNDS")
					ENDIF
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)

				REPLAY_STOP_EVENT()

				RETURN TRUE
				
			ELSE
			
				IF ( bCutsceneSkipped = FALSE ) 
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	//if cutscene was skipped, fade out and don't fade in
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
		
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC CONVERGE_TIME_SCALE_VALUE(FLOAT &fCurrentValue, FLOAT fDesiredValue, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = FALSE)

	IF fCurrentValue != fDesiredValue
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = 0.0 +@ (fAmountToConverge * 30.0)
		ENDIF
	
		IF fCurrentValue - fDesiredValue > fConvergeAmountThisFrame
			fCurrentValue -= fConvergeAmountThisFrame
		ELIF fCurrentValue - fDesiredValue < -fConvergeAmountThisFrame
			fCurrentValue += fConvergeAmountThisFrame
		ELSE
			fCurrentValue = fDesiredValue
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_TIME_SCALE_VALUE()

	CONVERGE_TIME_SCALE_VALUE(fCurrentTimeScale, fDesiredTimeScale, fDesiredTimeScaleSpeed, TRUE)
	SET_TIME_SCALE(fCurrentTimeScale)
	
	IF GET_GAME_TIMER() - iTimeScaleChangeTimer > 0
	
		fDesiredTimeScale		= GET_RANDOM_FLOAT_IN_RANGE(0.5, 0.75)	//imran wanted 0.60 - 0.85
		fDesiredTimeScaleSpeed	= GET_RANDOM_FLOAT_IN_RANGE(0.01, 0.001)
		iTimeScaleChangeTimer 	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
	
	ENDIF

ENDPROC

PROC UPDATE_FLIGHT_CONTROLS()

	FLOAT	fPitchNormalValue	= GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_PARACHUTE_PITCH_UD)
	VECTOR	vPlayerPosition		= GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	
	FLOAT 	fForceFrameModifier = 0.0 +@ (fForceBaseModifier * 30.0)

	//add small boost upwards for holding stick down
	IF ( fPitchNormalValue > 0.5 )
		vForceValue.z = BASE_Z_FORCE_VALUE + 10
	ELSE
		vForceValue.z = BASE_Z_FORCE_VALUE
	ENDIF
	
	//get dive boost time
	IF ( fPitchNormalValue < -0.5 )
	
		IF ( iDiveStartTime = 0 )
			iDiveStartTime 	= GET_GAME_TIMER()
		ELSE
			iDiveTimer = GET_GAME_TIMER() - iDiveStartTime
		ENDIF
	
	ELSE
	
		IF ( iBoostTime = 0 )
			iBoostTime		= iDiveTimer
			iBoostTimer		= GET_GAME_TIMER()
		ENDIF
	
		iDiveTimer 		= 0
		iDiveStartTime	= 0
	
	ENDIF
	
	//apply dive boost for specified time
	IF ( iBoostTime != 0 )
		IF NOT HAS_TIME_PASSED(iBoostTime, iBoostTimer)
			IF ( fPitchNormalValue > 0.5 )
				vForceValue.y = 25
				vForceValue.z = 400
			ENDIF
		ELSE
			iBoostTime	= 0
			iBoostTimer = 0
			vForceValue.y = 150
			vForceValue.z = 50
		ENDIF
	ENDIF
	
	//don't apply any force up when over 700 m
	IF ( vPlayerPosition.z > 700.0 )
		vForceValue.z = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD	
		IF bDrawDebugLinesAndSpheres = TRUE
			DRAW_DEBUG_TEXT_2D("DISTANCE FROM ORIGIN:", << 0.1, 0.70, 0.0 >>) 	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << 0.0, 0.0, 0.0 >>)), 	<< 0.2, 0.70, 0.0 >>)
			DRAW_DEBUG_TEXT_2D("PITCH NORMAL VALUE:", 	<< 0.1, 0.72, 0.0 >>) 	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fPitchNormalValue), 	<< 0.2, 0.72, 0.0 >>)
			DRAW_DEBUG_TEXT_2D("HEIGHT:", 				<< 0.1, 0.74, 0.0 >>) 	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(vPlayerPosition.z), 	<< 0.2, 0.74, 0.0 >>)
			DRAW_DEBUG_TEXT_2D("HEIGHT ABOVE GROUND:", 	<< 0.1, 0.76, 0.0 >>) 	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID())), 	<< 0.2, 0.76, 0.0 >>)
			DRAW_DEBUG_TEXT_2D("DIVE TIMER:", 			<< 0.1, 0.78, 0.0 >>) 	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iDiveTimer), 			<< 0.2, 0.78, 0.0 >>)
			DRAW_DEBUG_TEXT_2D("BOOST TIME:", 			<< 0.1, 0.80, 0.0 >>) 	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iBoostTime), 			<< 0.2, 0.80, 0.0 >>)	IF ( iBoostTime != 0 ) DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iBoostTime - (GET_GAME_TIMER() - iBoostTimer )), 			<< 0.3, 0.80, 0.0 >>) ENDIF
			DRAW_DEBUG_TEXT_2D("FORCE APPLIED:", 		<< 0.1, 0.88, 0.0 >>) 	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(vForceValue.x), 		<< 0.2, 0.88, 0.0 >>)	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(vForceValue.y), 		<< 0.3, 0.88, 0.0 >>)	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(vForceValue.z), 		<< 0.4, 0.88, 0.0 >>)
		ENDIF
	#ENDIF

	IF IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
		APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(), APPLY_TYPE_IMPULSE,
							  << vForceValue.x * fForceFrameModifier, vForceValue.y * fForceFrameModifier, vForceValue.z * fForceFrameModifier >>,
							  vForceOffset, 0, TRUE, FALSE, FALSE)
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_FLIGHT_COMPLETED(INT &iStageProgress) 

	SET_CLOCK_TIME(22, 0, 0)
	
	DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()	//fix for B*2008355
	SET_FOLLOW_PED_CAM_THIS_UPDATE("FOLLOW_PED_SKY_DIVING_FAMILY5_CAMERA", 0)

	SWITCH iStageProgress
	
		CASE 0
		
			STAT_DISABLE_STATS_TRACKING()
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, TRUE, FALSE)
				
				//B* 2222737: Reduce exposure brightness on SLI rigs
				IF IS_PC_VERSION()
					FORCE_EXPOSURE_READBACK(TRUE)
				ENDIF
				
				CLEAR_TIMECYCLE_MODIFIER()
				ANIMPOSTFX_STOP("DMT_flight_intro")
				ANIMPOSTFX_PLAY("DMT_flight", 0, TRUE)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
				IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_flying", "falling_to_skydive")
				
					QUIT_DRUNK_CAMERA_IMMEDIATELY()
					STOP_GAMEPLAY_CAM_SHAKING(TRUE)
					MAKE_PED_SOBER(PLAYER_PED_ID())
					
					WARP_PED(PLAYER_PED_ID(), << -2100.000, 700.325, 1200.000 >>, 128.0, FALSE, FALSE, FALSE)
					
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					
					iStageProgress++
						
				ELSE
					
					iStageProgress++
					
				ENDIF
			ENDIF

		BREAK
		
		CASE 1
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFlyingSceneID)
				
					REQUEST_ANIM_DICT("missfam5_flying")
		
					IF HAS_ANIM_DICT_LOADED("missfam5_flying")
					
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0, 0)
						WARP_PED(PLAYER_PED_ID(), << -2100.000, 700.325, 1200.000 >>, 128.0, FALSE, FALSE, FALSE)
						
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFlyingSceneID)
				
							iFlyingSceneID = CREATE_SYNCHRONIZED_SCENE(<< -900.000, 100.325, 800.000 >>, << 0.0, 0.0, -139.210 >>)
							
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iFlyingSceneID, "missfam5_flying", "falling_to_skydive", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
							
							DESTROY_ALL_CAMS()
							AnimatedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
							PLAY_SYNCHRONIZED_CAM_ANIM(AnimatedCamera, iFlyingSceneID, "falling_to_skydive_cam", "missfam5_flying")
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
						ENDIF
						
						iStageProgress++
						
					ENDIF
					
				ELSE
		
					iStageProgress++
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missfam5_flying", "falling_to_skydive")
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(fRelativePitch)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.35)
			ENDIF
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iFlyingSceneID)
				
					DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
				
					IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_FLYING")
						START_AUDIO_SCENE("FAMILY_5_FLYING")							
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					ENDIF
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(iFlyingSceneID) >= 0.715
						IF bFlightStreamTriggered = FALSE
							IF LOAD_STREAM("FLYING_STREAM", "FAMILY_5_SOUNDS")
								SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
								PLAY_STREAM_FRONTEND()
								iStreamPlayTime			= 0
								bFlightStreamTriggered	= TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF bAnimatedCameraStopped = FALSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(iFlyingSceneID) >= fAnimatedCameraStopPhase
							
							STOP_GAMEPLAY_CAM_SHAKING(TRUE)
							SHAKE_GAMEPLAY_CAM("FAMILY5_DRUG_TRIP_SHAKE", 1.0)
							RENDER_SCRIPT_CAMS(FALSE, TRUE, iAnimatedCameraInterpTime, FALSE)
							
							bAnimatedCameraStopped = TRUE
						ENDIF
					ENDIF
				
					IF GET_SYNCHRONIZED_SCENE_PHASE(iFlyingSceneID) >= fFallAnimationStopPhase
				
						TASK_SKY_DIVE(PLAYER_PED_ID())
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0, 0)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_PARACHUTING, FALSE, FAUS_DEFAULT)
						
						IF DOES_CAM_EXIST(UndergroundCamera)
							DESTROY_CAM(UndergroundCamera)
						ENDIF
						
						fCurrentTimeScale		= 1.0
						fDesiredTimeScale		= 1.0
						fDesiredTimeScaleSpeed	= 0.01
					
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_MICHAEL_LONG_SCREAM")
						
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						iStageProgress++
						
					ENDIF
				ENDIF
			ENDIF

		BREAK
	
		CASE 3
		
			IF IS_STREAM_PLAYING()
				iStreamPlayTime = GET_STREAM_PLAY_TIME()
				SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
			ENDIF
		
			REQUEST_SCRIPT_AUDIO_BANK("FAM5_DRUG_FLIGHT_END")

			UPDATE_FLIGHT_CONTROLS()
			UPDATE_TIME_SCALE_VALUE()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0, 0)
		
			VECTOR vTestCoordsFront
			VECTOR vTestCoordsDown
		
			vTestCoordsFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 10.0, 0.0 >>)
			vTestCoordsDown  = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 5.0, -5.0 >>)
			
			IF iStreamPlayTime >= FLIGHT_TIME_MAX OR NOT IS_STREAM_PLAYING()
			OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(PLAYER_PED_ID()) OR IS_ENTITY_IN_WATER(PLAYER_PED_ID())
			OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << 0.0, 0.0, 0.0 >>) > 8000.0
			OR NOT IS_SHAPE_TEST_CAPSULE_CLEAR(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()), 3.0 #IF IS_DEBUG_BUILD, "PLAYER" #ENDIF)
			OR NOT IS_SHAPE_TEST_CAPSULE_CLEAR(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTestCoordsFront, 1.0 #IF IS_DEBUG_BUILD, "FRONT" #ENDIF)
			OR NOT IS_SHAPE_TEST_CAPSULE_CLEAR(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTestCoordsDown, 1.0 #IF IS_DEBUG_BUILD, "DOWN" #ENDIF)

				PLAY_SOUND_FRONTEND(-1, "FLYING_STREAM_END_INSTANT", "FAMILY_5_SOUNDS")

				bCollisionDetected = TRUE
				
			ENDIF
			
			IF ( bCollisionDetected = TRUE )
						
				UndergroundCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				SET_CAM_PARAMS(UndergroundCamera, <<-967.576538,309.460022,65.552261>>,<<-89.499146,0.013776,37.204556>>,40.0)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				iStageProgress++

			ENDIF
		
		BREAK
		
		CASE 4

			IF DOES_CAM_EXIST(UndergroundCamera) AND IS_CAM_RENDERING(UndergroundCamera)
			
				SET_TIME_SCALE(1.0)
				
				STOP_STREAM()
				STOP_AUDIO_SCENE("FAMILY_5_FLYING")
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_DRUG_FLIGHT_END")
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_MICHAEL_LONG_SCREAM")
				
				REMOVE_ANIM_DICT("missfam5_flying")
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				ENDIF
				
				STOP_GAMEPLAY_CAM_SHAKING(TRUE)
				
				REPLAY_RECORD_BACK_FOR_TIME(15.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
				
				bBlackoutStagesActive = FALSE
				
				//B* 2222737: Adjust exposure to work on SLI rigs
				IF IS_PC_VERSION()
					FORCE_EXPOSURE_READBACK(FALSE)
				ENDIF
				
				STAT_ENABLE_STATS_TRACKING()
			
				RETURN TRUE
				
			ENDIF
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_AT_HOME()

	IF ( bLeadInTriggered = FALSE )

		//Michael enters main hall of the house, says 'Hello?'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME1")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.746826,183.296768,70.847786>>, <<-816.380859,178.671936,75.153091>>, 5.5)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME1", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME1", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//Michael walks up the lower stairs, says 'Hey, I'm home.'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME2")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-805.310608,184.709808,69.002808>>, <<-810.909058,182.608139,75.588348>>, 2.4)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME2", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME2", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//Michael walks up the upper stairs, says 'Anyone here?'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME3")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-805.989319,183.167130,73.502525>>, <<-804.606812,179.573715,78.740730>>, 2.35)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME3", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME3", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//Michael enters living room area, says 'I'm back.'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME9")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.945618,181.749252,66.152946>>, <<-801.842285,168.541428,75.334709>>, 7.6)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME9", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME9", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//Michael enters kitchen and dining room, says 'Hello?'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME5")
			//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.412415,187.048843,71.605469>>, <<-796.639465,177.333191,75.334709>>, 8.0)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-797.724304,183.222839,71.744682>>, <<-795.584778,177.697128,74.834709>>, 6.0) //dining room only
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME5", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME5", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//Michael enters upper hall, says 'Kids?'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME8")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.397156,179.202255,75.240730>>, <<-809.928223,175.562454,78.740730>>, 4.0)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME8", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME8", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Michael enters his bedroom
		IF  NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME6")	//says 'Amanda?'
		AND NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME10")	//says 'Honey?'
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.094360,180.614655,75.240730>>, <<-816.723267,177.806808,78.862457>>, 5.0)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
							CASE 0
								IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME6", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("FAM5_HOME6", TRUE)
								ENDIF
							BREAK
							CASE 1
								IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME10", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("FAM5_HOME10", TRUE)
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Michael enters Jimmy's room, says 'Ho Jimmy'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME4")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.125183,173.548157,75.740074>>, <<-806.509033,166.364822,78.740730>>, 4.5)	//Jimmy's room
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME4", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME4", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Michael enters Tracy's room, says 'Tracey? Trace?'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME7")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.726013,176.068436,75.240730>>, <<-799.839966,168.956497,78.740730>>, 3.5)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME7", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME7", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Michael enters the back garden, says 'Anyone out here?'
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_HOME11")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-793.242004,180.620438,68.834671>>, <<-775.615662,187.148071,74.834671>>, 24.0)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-795.846069,177.214935,74.835037>>, <<-792.377319,167.718445,69.388756>>, 8.0)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_HOME11", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("FAM5_HOME11", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF

ENDPROC

PROC MANAGE_AMBIENT_PEDS_SPEAKING_TO_PED(PED_INDEX PedIndex, INT &iTimer)

	PED_INDEX pedArray[16]
	
	INT i = 0
	
	IF ( iTimer = 0)
	
		iTimer = GET_GAME_TIMER()
	
	ELSE
	
		IF HAS_TIME_PASSED(15000, iTimer)
	
			IF NOT IS_PED_INJURED(PedIndex)
			
				GET_PED_NEARBY_PEDS(PedIndex, pedArray, PEDTYPE_ANIMAL)
				
				FOR i = 0 TO ( COUNT_OF(pedArray) - 1 )
				
					IF DOES_ENTITY_EXIST(pedArray[i])
					
						IF NOT IS_PED_INJURED(pedArray[i])
						
							IF NOT IS_PED_IN_ANY_VEHICLE(pedArray[i])
							
								IF GET_DISTANCE_BETWEEN_ENTITIES(pedArray[i], PedIndex) < 7.5
								
									VECTOR 	vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(pedArray[i], GET_ENTITY_COORDS(PedIndex))
									
									IF ( vOffset.y > 1.0 )	//if ped is in front of the ambient ped
								
										IF NOT IS_AMBIENT_SPEECH_PLAYING(pedArray[i])
											
											PLAY_PED_AMBIENT_SPEECH(pedArray[i], "SEE_WEIRDO", SPEECH_PARAMS_FORCE)
											
											TASK_LOOK_AT_ENTITY(pedArray[i], PedIndex, 1000)
											
											#IF IS_DEBUG_BUILD
												DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i]), 255, 0, 255)
											#ENDIF
											
											iTimer = GET_GAME_TIMER()
											
										ENDIF
										
									ENDIF
								
								ENDIF
							
							ENDIF
						
						ENDIF
						
					ENDIF
					
				ENDFOR
				
			ENDIF
		
		ENDIF
		
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_GO_HOME_WASTED_COMPLETED(INT &iStageProgress)

	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	MANAGE_CONVERSATIONS_AT_HOME()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND IS_VEHICLE_A_CYCLE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
		SET_PLAYER_SPRINT(PLAYER_ID(), FALSE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN_ONLY)
	ELSE
		SET_PLAYER_SPRINT(PLAYER_ID(), TRUE)
	ENDIF

	SWITCH iStageProgress
	
		CASE 0	//create scripted camera for wake up wasted

			WARP_PED(PLAYER_PED_ID(), MissionPosition.vStageStart, MissionPosition.fStartHeading, FALSE, TRUE, FALSE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				REMOVE_DECALS_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				MAKE_PED_SOBER(PLAYER_PED_ID())
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)		//unblock all shops when drunk and blackout is over
			
			INSTANTLY_FILL_VEHICLE_POPULATION()

			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)

			iStageProgress++			
			
		BREAK
		
		CASE 1
		
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
							
				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.0)
				
				iLoadSceneTimer = GET_GAME_TIMER()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
				#ENDIF
				
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Load scene timer: ", GET_GAME_TIMER() - iLoadSceneTimer, ".")
			#ENDIF
			
			IF IS_NEW_LOAD_SCENE_LOADED()
			OR HAS_TIME_PASSED(10000, iLoadSceneTimer)

					#IF IS_DEBUG_BUILD
						IF HAS_TIME_PASSED(10000, iLoadSceneTimer)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to iLoadSceneTimer timeout.")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to IS_NEW_LOAD_SCENE_LOADED() returning TRUE.")
						ENDIF
					#ENDIF
				
				NEW_LOAD_SCENE_STOP()
						
				iStageProgress++
			
			ENDIF
		
		BREAK
				
		CASE 2

			REQUEST_PTFX_ASSET()
			
			REQUEST_ANIM_DICT("missfam5_blackout")
			
			REQUEST_ANIM_DICT("move_m@drunk@verydrunk")
			
			LOAD_MISSION_SFX(MISSION_SFX_VOMIT)
			
			IF 	HAS_PTFX_ASSET_LOADED()
			AND HAS_ANIM_DICT_LOADED("missfam5_blackout")
			AND HAS_ANIM_DICT_LOADED("move_m@drunk@verydrunk")

				iStageProgress++
			
			ENDIF
			
		BREAK
		
		CASE 3

			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iVomitSceneID)
				
					iVomitSceneID = CREATE_SYNCHRONIZED_SCENE(<< -966.935, 309.252, 69.350 >>, << 0.0, 0.0, -90.0 >>)
					
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
					
					SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iVomitSceneID, "missfam5_blackout", "vomit", INSTANT_BLEND_IN, -2)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					DESTROY_ALL_CAMS()
					ScriptedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iVomitSceneID, "vomit_cam", "missfam5_blackout")
					
					DISPLAY_HUD(FALSE)
					DISPLAY_RADAR(FALSE)			
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					MAKE_PED_DRUNK(PLAYER_PED_ID(), 50000)
					STOP_MISSION_SFX(MISSION_SFX_HEARTBEAT)
					SET_AUDIO_SPECIAL_EFFECT_MODE(AUDIO_SPECIAL_EFFECT_MODE_NORMAL)
					
					STOP_AUDIO_SCENE("FAMILY_5_YOGA_ON_DRUGS")
					START_AUDIO_SCENE("FAMILY_5_WAKE_UP_VOMIT")
					
					ANIMPOSTFX_STOP("DMT_flight")
					ANIMPOSTFX_PLAY("DrugsDrivingOut", 0, FALSE)
					
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iStageProgress++
					
				ENDIF
				
			ENDIF

		BREAK
		
		CASE 4
		
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iVomitSceneID)
				
					IF DOES_CAM_EXIST(UndergroundCamera)
						DESTROY_CAM(UndergroundCamera)
					ENDIF
				
					FLOAT fAnimTime
					
					fAnimTime = GET_SYNCHRONIZED_SCENE_PHASE(iVomitSceneID)
					
					IF ( fAnimTime < 0.95 )
						
					
						IF 	(fAnimTime > iSTART_VOMITING_TIME)
						AND (fAnimTime < iSTOP_VOMITING_TIME)
						
							IF (ptfxPuke = NULL)
						
								PLAY_MISSION_SFX(MISSION_SFX_VOMIT)
						
								ptfxPuke = START_PARTICLE_FX_LOOPED_ON_PED_BONE("scr_trev_puke", PLAYER_PED_ID(),
																				<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, BONETAG_HEAD, 1)
							ENDIF
						
						ELSE
						
							IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxPuke)
							
								STOP_PARTICLE_FX_LOOPED(ptfxPuke)
								
								ptfxPuke = NULL
								
							ENDIF
						
						ENDIF
						
						IF ( bFirstPersonFlashPlayed = FALSE )
							IF ( fAnimTime > iSTART_FP_FLASH_TIME )
								IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
									ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
									PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
									bFirstPersonFlashPlayed = TRUE
								ENDIF
							ENDIF
						ENDIF
					
					ELSE

						CLEAR_PED_TASKS(PLAYER_PED_ID())
						
						REMOVE_ANIM_DICT("missfam5_blackout")

						iStageProgress++
						
					ENDIF
				
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 5
		
			IF ( bCutsceneSkipped = TRUE )
				WAIT(500)
			ENDIF
		
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		
			RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX_HEARTBEAT)
			RELEASE_MISSION_SFX_AUDIO_BANK(MISSION_SFX_VOMIT)
			
			REMOVE_PTFX_ASSET()
		
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			
			//destroy scripted camera
			IF DOES_CAM_EXIST(ScriptedCamera)
				SET_CAM_ACTIVE(ScriptedCamera, FALSE)
				IF 	GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ELSE
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				ENDIF
				DESTROY_CAM(ScriptedCamera)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Destroying scripted camera.")
				#ENDIF
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FAM5_HOME_TIME) 
		
			iStageProgress++
		
		BREAK
		
		CASE 6
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FAM5_PUKE")
				IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_PUKE", CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("FAM5_PUKE", TRUE)
				ENDIF
			ENDIF
		
			//disable player camera control when camera is interpolating from script camera to gameplay camera
			IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_BEHIND)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
			ENDIF

			MANAGE_AMBIENT_PEDS_SPEAKING_TO_PED(PLAYER_PED_ID(), iAmbientSpeechTimer)

			//display locates information, blip, god text etc, take care of cops
			IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData, vKitchenCoords, << 0.25, 0.25, LOCATE_SIZE_HEIGHT >>, TRUE, sLabelOBJ, TRUE)

			//handle requestting mocap cutscene 
			IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			OR IS_PLAYER_CHANGING_CLOTHES()
                  
				IF HAS_THIS_CUTSCENE_LOADED("family_5_mcs_5_p4")
				OR HAS_CUTSCENE_LOADED()
				OR IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
					#ENDIF
				ENDIF
				                        
			ELSE
			
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vKitchenCoords) < DEFAULT_CUTSCENE_LOAD_DIST / 2
			
					REQUEST_CUTSCENE("family_5_mcs_5_p4")
				
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())	//Michael's variations
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene ped Michael from player ped.")
						#ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(osNote.ObjectIndex)
						IF HAS_MISSION_OBJECT_BEEN_CREATED(osNote, TRUE)
							FREEZE_ENTITY_POSITION(osNote.ObjectIndex, TRUE)
							SET_ENTITY_INVINCIBLE(osNote.ObjectIndex, TRUE)
						ENDIF
					ENDIF

				ELSE
				
					IF HAS_THIS_CUTSCENE_LOADED("family_5_mcs_5_p4")
					OR HAS_CUTSCENE_LOADED()
					OR IS_CUTSCENE_ACTIVE()
						REMOVE_CUTSCENE()
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
						#ENDIF
					ENDIF

				ENDIF
					
			ENDIF

			IF IS_AUDIO_SCENE_ACTIVE("FAMILY_5_WAKE_UP_VOMIT")			//FAMILY_5_WAKE_UP_VOMIT 
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())			//starts as soon as vomit scene fades in
					IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())	//stops as soon as player stars to enter any vehicle
					OR GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) <> NULL					
						STOP_AUDIO_SCENE("FAMILY_5_WAKE_UP_VOMIT")
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_HOME")			//FAMILY_5_GO_HOME
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())				//starts when player gets into any vehicle
					START_AUDIO_SCENE("FAMILY_5_GO_HOME")				//stops when player exist vehicle after arriving at Michael's house
				ENDIF
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_HOME")
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF GET_DISTANCE_BETWEEN_COORDS(vKitchenCoords, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20.0
						STOP_AUDIO_SCENE("FAMILY_5_WAKE_UP_VOMIT")
						STOP_AUDIO_SCENE("FAMILY_5_GO_HOME")
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_HOME_INT")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.520630,187.521057,71.105469>>, <<-795.681763,177.692230,74.834709>>, 6.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.700134,185.895218,71.105469>>, <<-800.207031,176.436371,74.834709>>, 4.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.559814,168.317978,71.334709>>, <<-805.525635,178.113297,74.834709>>, 8.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.901978,185.396698,73.504105>>, <<-804.658386,179.531998,78.740730>>, 2.5)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.397156,179.202255,75.240730>>, <<-809.928223,175.562454,78.740730>>, 4.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.917725,176.369095,70.834709>>, <<-808.785706,178.632828,74.653091>>, 2.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.726013,176.068436,75.240730>>, <<-799.839966,168.956497,78.740730>>, 3.5)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.094360,180.614655,75.240730>>, <<-816.723267,177.806808,78.862457>>, 8.0)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-810.866333,175.499969,75.240730>>, <<-814.803284,173.922989,78.740730>>, 3.6)
				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-802.712769,167.797989,75.240730>>, <<-805.571716,175.191330,78.740730>>, 3.0)
					STOP_AUDIO_SCENE("FAMILY_5_WAKE_UP_VOMIT")
					STOP_AUDIO_SCENE("FAMILY_5_GO_HOME")
					START_AUDIO_SCENE("FAMILY_5_GO_HOME_INT")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting audio scene FAMILY_5_GO_HOME_INT, player is in the house.")
					#ENDIF
				ENDIF
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("FAMILY_5_GO_HOME_INT")
				IF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.520630,187.521057,71.105469>>, <<-795.681763,177.692230,74.834709>>, 6.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.700134,185.895218,71.105469>>, <<-800.207031,176.436371,74.834709>>, 4.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.559814,168.317978,71.334709>>, <<-805.525635,178.113297,74.834709>>, 8.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.901978,185.396698,73.504105>>, <<-804.658386,179.531998,78.740730>>, 2.5)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.397156,179.202255,75.240730>>, <<-809.928223,175.562454,78.740730>>, 4.0)
				AND NOT	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.917725,176.369095,70.834709>>, <<-808.785706,178.632828,74.653091>>, 2.0)
				AND NOT	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.726013,176.068436,75.240730>>, <<-799.839966,168.956497,78.740730>>, 3.5)
				AND NOT	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.094360,180.614655,75.240730>>, <<-816.723267,177.806808,78.862457>>, 8.0)
				AND NOT	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-810.866333,175.499969,75.240730>>, <<-814.803284,173.922989,78.740730>>, 3.6)
				AND NOT	IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-802.712769,167.797989,75.240730>>, <<-805.571716,175.191330,78.740730>>, 3.0)				
					STOP_AUDIO_SCENE("FAMILY_5_GO_HOME_INT")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping audio scene FAMILY_5_GO_HOME_INT, player is not in the house.")
					#ENDIF
				ENDIF
			ENDIF
					
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -801.42, 182.86, 73.10 >>, << 2.25, 2.25, 2.25 >>)
					
						KILL_ANY_CONVERSATION()
						KILL_FACE_TO_FACE_CONVERSATION()
						CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)

						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						
						bLeadInTriggered = TRUE	//cutscene is about to start

						iStageProgress++
						
					ENDIF
					
				ELSE
				
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -800.80, 183.71 ,73.50 >>, << 1.5, 1.25, 2.0 >>)
				
						KILL_ANY_CONVERSATION()
						KILL_FACE_TO_FACE_CONVERSATION()
						CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)

						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						
						bLeadInTriggered = TRUE	//cutscene is about to start

						iStageProgress++
					
					ENDIF
					
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 7
		
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 2.0)
					
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
		
					SETTIMERA(0)
		
					iStageProgress++
					
				ENDIF
			ELSE

				SETTIMERA(0)

				iStageProgress++			
				
			ENDIF
		
		BREAK
		
		CASE 8
		
			IF 	NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_GETTING_UP(PLAYER_PED_ID())
			AND	NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			
				IF ( TIMERA() > 5000 )
				OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
					
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
					
					RETURN TRUE
					
				ENDIF
			ENDIF
		
		BREAK
			
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_END_1_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
			
			IF HAS_REQUESTED_CUTSCENE_LOADED("family_5_mcs_5_p4")
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF DOES_ENTITY_EXIST(vsParkedVehicles[0].VehicleIndex)
							IF NOT IS_ENTITY_DEAD(vsParkedVehicles[0].VehicleIndex)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsParkedVehicles[0].VehicleIndex)
									bCutsceneTriggeredOnBike = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					MAKE_PED_SOBER(PLAYER_PED_ID())
					REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(osNote.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osNote.ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osNote.ObjectIndex, "Amanda_Note", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, osNote.ModelName)
					ENDIF
				ENDIF
				
				STOP_AUDIO_SCENE("FAMILY_5_GO_HOME_INT")
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)

				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					//Michael's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
				
				IF ( bCutsceneTriggeredOnBike = TRUE )
					IF DOES_ENTITY_EXIST(vsParkedVehicles[0].VehicleIndex)
						IF NOT IS_ENTITY_DEAD(vsParkedVehicles[0].VehicleIndex)
							SET_ENTITY_COORDS(vsParkedVehicles[0].VehicleIndex, <<-824.9539, 174.8554, 69.8377>>)
							SET_ENTITY_HEADING(vsParkedVehicles[0].VehicleIndex, 157.3586)
							SET_VEHICLE_ON_GROUND_PROPERLY(vsParkedVehicles[0].VehicleIndex)
						ENDIF
					ENDIF
				ENDIF
				
				VEHICLE_INDEX LastPlayerVehicleIndex
				
				LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
				
				IF DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
					IF NOT IS_ENTITY_DEAD(LastPlayerVehicleIndex)
					
						IF GET_DISTANCE_BETWEEN_COORDS(vKitchenCoords, GET_ENTITY_COORDS(LastPlayerVehicleIndex)) < 20.0
						OR IS_ENTITY_AT_COORD(LastPlayerVehicleIndex, << -826.78, 177.64, 72.13 >>,<< 12.0, 12.0, 8.0>>) //try 16.0 for bigger area
						
							IF NOT IS_VEHICLE_IN_PLAYERS_GARAGE(LastPlayerVehicleIndex, CHAR_MICHAEL, TRUE)
							
								IF NOT IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
									SET_ENTITY_AS_MISSION_ENTITY(LastPlayerVehicleIndex)
								ENDIF
								
								STOP_VEHICLE_FIRE(LastPlayerVehicleIndex)
							
								SET_ENTITY_COORDS(LastPlayerVehicleIndex, << -825.8718, 157.3143, 69.4619 >>)
								SET_ENTITY_HEADING(LastPlayerVehicleIndex, 90.0)
								//SET_ENTITY_COORDS(LastPlayerVehicleIndex, sVehicleGenDataStruct.coords)
								//SET_ENTITY_HEADING(LastPlayerVehicleIndex, sVehicleGenDataStruct.heading)
								SET_VEHICLE_ON_GROUND_PROPERLY(LastPlayerVehicleIndex)
								SET_VEHICLE_DOORS_SHUT(LastPlayerVehicleIndex, TRUE)
								SET_VEHICLE_ENGINE_ON(LastPlayerVehicleIndex, FALSE, TRUE)
								SET_MISSION_VEHICLE_GEN_VEHICLE(LastPlayerVehicleIndex, << -825.8718, 157.3143, 69.4619 >>, 90.0)
								
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//fix for B*1711336, comment out clear are and clear objects as it is causing
				//props on the kitchen counter to clear and pop back into the world
				//CLEAR_AREA(MissionPosition.vStageStart, 25.0, TRUE)
				//CLEAR_AREA_OF_OBJECTS(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_PEDS(MissionPosition.vStageStart, 25.0)
				STOP_FIRE_IN_RANGE(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_PROJECTILES(MissionPosition.vStageStart, 25.0)
				REMOVE_PARTICLE_FX_IN_RANGE(MissionPosition.vStageStart, 25.0)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK

		CASE 2
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF

			IF HAS_CUTSCENE_FINISHED()

				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				REPLAY_STOP_EVENT()
				iStageProgress++
		
			ENDIF
			
		BREAK
		
		CASE 3
		
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
			TRIGGER_MISSION_STATS_UI(TRUE, TRUE)
			
			iStageProgress++
		
		BREAK
		
		CASE 4
		
			IF ( g_bResultScreenDisplaying = TRUE )
			
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 5
		
			IF NOT IS_REPEAT_PLAY_ACTIVE()
		
				RETURN TRUE
			
			ELSE
			
				IF ( g_bResultScreenDisplaying = FALSE )
				
					RETURN TRUE
				
				ENDIF
			
			ENDIF
		
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_END_2_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
				SET_PLAYER_PED_OUTFIT(MO_MISSION_END)
				
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				RESTORE_PLAYER_PED_TATTOOS(PLAYER_PED_ID())
				
				WARP_PED(PLAYER_PED_ID(), MissionPosition.vStageStart, MissionPosition.fStartHeading, FALSE, FALSE, FALSE)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

			ENDIF
			
			iStageProgress++
			
		BREAK
		
		CASE 1
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				
					iStageProgress++
				
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
			
			IF HAS_REQUESTED_CUTSCENE_LOADED("family_5_mcs_5_p5") AND g_bResultScreenDisplaying = FALSE
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)

				START_CUTSCENE()
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					//Michael's variations
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 3	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
				
				CLEAR_AREA(MissionPosition.vStageStart, 25.0, TRUE)
				CLEAR_AREA_OF_PEDS(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_OBJECTS(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_VEHICLES(MissionPosition.vStageStart, 25.0)
				CLEAR_AREA_OF_PROJECTILES(MissionPosition.vStageStart, 25.0)
				REMOVE_PARTICLE_FX_IN_RANGE(MissionPosition.vStageStart, 25.0)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK

		CASE 4
		
			VEHICLE_INDEX LastPlayerVehicleIndex
				
			LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
			
			IF DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
				IF NOT IS_ENTITY_DEAD(LastPlayerVehicleIndex)
					IF IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
						IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(LastPlayerVehicleIndex)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(LastPlayerVehicleIndex)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				RETURN TRUE
		
			ENDIF
			
		BREAK
		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

#IF IS_DEBUG_BUILD

	FUNC BOOL IS_MISSION_STAGE_TEST_COMPLETED(INT &iStageProgress)

		BOOL	bTimePassed

		IF 	IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
		AND NOT IS_PED_INJURED(psJimmy.PedIndex)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())

			SWITCH iStageProgress
			 
				CASE 0
				
					REQUEST_ANIM_DICT("missfam5mcs_6")
				
					IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData, vMichaelsHouseCoords, g_vAnyMeansLocate , FALSE, psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, sLabelOBJ, "CMN_JLEAVE", sLabelMGETIN, "CMN_GENGETBCKY", FALSE, TRUE)
			
					IF ( iKickOutDelayTimer <> 0 )
					
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsMichaelsCar.VehicleIndex, GET_STRING_FROM_INT(CLAMP_INT(iKickOutDelayTime - ( GET_GAME_TIMER() - iKickOutDelayTimer ), 0, iKickOutDelayTime)), 2.25, 0, 255, 0)
					
						IF  HAS_TIME_PASSED(iKickOutDelayTime, iKickOutDelayTimer)
							bTimePassed = TRUE	
						ENDIF
					ENDIF					
				
					IF 	ARE_PEDS_IN_VEHICLE_AT_SAFE_POSITION_FOR_INGAME_CUTSCENE(PLAYER_PED_ID(), psJimmy.PedIndex, vsMichaelsCar.VehicleIndex)
					OR ( bCutsceneToggle = TRUE )
						IF ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMichaelsHouseCoords) < fKickAnimationTriggerDistance )
						OR ( bTimePassed = TRUE )
						OR ( bCutsceneToggle = TRUE )
					
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							IF CREATE_CONVERSATION(Family5Conversation, "FAM5AUD", "FAM5_DSTOP", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("FAM5_DSTOP", TRUE)
							ENDIF

							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
																		
							bCutsceneToggle = FALSE
							
							iStageProgress++
							
						ENDIF
					ENDIF

				BREAK
				
				CASE 1

					REQUEST_ANIM_DICT("missfam5mcs_6")
					
					SET_IN_VEHICLE_CAM_STATE_THIS_UPDATE(vsMichaelsCar.VehicleIndex, CAM_INSIDE_VEHICLE)
					
					IF HAS_ANIM_DICT_LOADED("missfam5mcs_6")

						IF IS_VEHICLE_ON_ALL_WHEELS(vsMichaelsCar.VehicleIndex)
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vsMichaelsCar.VehicleIndex, 5.0)
								
								IF 	IS_VEHICLE_ON_ALL_WHEELS(vsMichaelsCar.VehicleIndex)
								AND NOT IS_ENTITY_ON_FIRE(vsMichaelsCar.VehicleIndex)
								
									INT		iTotalLanes
									FLOAT	fHeading
									FLOAT	fVehicleHeading
									VECTOR	vVehiclePosition
									VECTOR 	vClosestNodeToVehicle
								
									vVehiclePosition 	= GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex)
									fVehicleHeading		= GET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex)
								
									IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vVehiclePosition, 1, vClosestNodeToVehicle, fHeading, iTotalLanes, NF_NONE)
								
										SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vClosestNodeToVehicle)
										SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, fVehicleHeading)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								
									ENDIF

									CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 3.5)
									CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 3.0)
								
									iSpeedZone = ADD_ROAD_NODE_SPEED_ZONE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 15.0, 0.0)
									
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
										
										iKickOutSceneID = CREATE_SYNCHRONIZED_SCENE(<< 0.0, 0.0,0.0>>, << 0.0, 0.0,0.0>>)
											
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iKickOutSceneID, vsMichaelsCar.VehicleIndex, 0)
								
										DESTROY_ALL_CAMS()
										ScriptedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
										PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iKickOutSceneID, "Push_Michael_Out_Cam", "missfam5mcs_6")
								
										DISPLAY_HUD(FALSE)
										DISPLAY_RADAR(FALSE)
										RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
										PLAY_SYNCHRONIZED_ENTITY_ANIM(vsMichaelsCar.VehicleIndex, iKickOutSceneID, "Push_Michael_Out_Car", "missfam5mcs_6", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
										SET_ENTITY_COLLISION(vsMichaelsCar.VehicleIndex, FALSE)
										
										CLEAR_PED_TASKS_IMMEDIATELY(psJimmy.PedIndex)
										TASK_SYNCHRONIZED_SCENE(psJimmy.PedIndex, iKickOutSceneID, "missfam5mcs_6", "push_michael_out_jimmy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(psJimmy.PedIndex)
										
										IF DOES_ENTITY_EXIST(osCup.ObjectIndex)
											IF NOT IS_ENTITY_DEAD(osCup.ObjectIndex)
												IF IS_ENTITY_ATTACHED(osCup.ObjectIndex)
													DETACH_ENTITY(osCup.ObjectIndex, FALSE, TRUE)
												ENDIF
												ATTACH_ENTITY_TO_ENTITY(osCup.ObjectIndex, psJimmy.PedIndex, GET_PED_BONE_INDEX(psJimmy.PedIndex, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
											ENDIF
										ENDIF
										
										MAKE_PED_SOBER(PLAYER_PED_ID())
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
										TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iKickOutSceneID, "missfam5mcs_6", "push_michael_out_player0",
																INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_LEG_IK)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
										
										psJimmy.bHasTask 			= FALSE
										bAnimationForced			= FALSE
										bAnimationsSwitched			= FALSE
										bPlayerRagdolledEarly 		= FALSE
										bCleanupVehiclesForCamera	= FALSE
										
										SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)
										
										SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
										
										iStageProgress++
										
									ENDIF
								
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				
				BREAK

				CASE 2
				
					RUN_VEHICLE_AND_CAMERA_CHECK()
				
					REQUEST_ANIM_DICT("missfam5mcs_6drag")
				
					IF NOT IS_PED_INJURED(psJimmy.PedIndex)
						SET_PED_RESET_FLAG(psJimmy.PedIndex, PRF_ForceInstantSteeringWheelIkBlendIn, TRUE)
					ENDIF

					IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)

						DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
						DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) <= 0.630
							SET_IN_VEHICLE_CAM_STATE_THIS_UPDATE(vsMichaelsCar.VehicleIndex, CAM_INSIDE_VEHICLE)
						ENDIF
						
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vsMichaelsCar.VehicleIndex, TRUE)
					
						IF HAS_ANIM_EVENT_FIRED(psJimmy.PedIndex, GET_HASH_KEY("Detach"))
							IF DOES_ENTITY_EXIST(osCup.ObjectIndex)
								IF NOT IS_ENTITY_DEAD(osCup.ObjectIndex)
									IF IS_ENTITY_ATTACHED(osCup.ObjectIndex)
										DETACH_ENTITY(osCup.ObjectIndex, FALSE, TRUE)
									ENDIF
									DELETE_OBJECT(osCup.ObjectIndex)
								ENDIF
							ENDIF
						ENDIF

						IF DOES_CAM_EXIST(ScriptedCamera)
						
							IF NOT IS_CAM_RENDERING(ScriptedCamera)
								DISPLAY_HUD(FALSE)
								DISPLAY_RADAR(FALSE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_5_DRUGS_FOCUS_CAM")
									STOP_AUDIO_SCENE("FAMILY_5_DRUGS")
									START_AUDIO_SCENE("FAMILY_5_DRUGS_FOCUS_CAM")
								ENDIF
							ELSE
								
								IF ( bCleanupVehiclesForCamera = FALSE )//cleanup vehicles near the camera and michael's animation positions
									CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-0.7309, 2.7140, 0.4813>>), 2.5)
									CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-3.6668, 0.1784, 0.4859>>), 2.5)
									CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-2.1213, 0.5363, 0.4975>>), 2.5)
									CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-1.4263, 1.9211, 4.8319>>), 2.5)
									CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-3.342, -0.523, -0.1449>>), 2.5)
									CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<0.0000, 4.5000, 0.25000>>), 2.5)
									CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<-2.0859, 4.1187, 0.1689>>), 2.5)
									CLEANUP_VEHICLE_WITH_PEDS_NEAR_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vsMichaelsCar.VehicleIndex, <<0.00000, -4.000, 0.1689>>), 3.0)
									bCleanupVehiclesForCamera = TRUE
								ENDIF
								
							ENDIF
							
						ENDIF				
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) > 0.75
								
							IF ( psJimmy.bHasTask = FALSE )
								
								KILL_CHASE_HINT_CAM(sCinematicCamStruct)
							
								STOP_SYNCHRONIZED_ENTITY_ANIM(vsMichaelsCar.VehicleIndex, INSTANT_BLEND_OUT, TRUE)
								SET_ENTITY_COLLISION(vsMichaelsCar.VehicleIndex, TRUE)
								
								CLEAR_PED_TASKS(psJimmy.PedIndex)
								SET_PED_INTO_VEHICLE(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, VS_DRIVER)
								TASK_VEHICLE_MISSION_PED_TARGET(psJimmy.PedIndex, vsMichaelsCar.VehicleIndex, PLAYER_PED_ID(), MISSION_FLEE, 25.0, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_SteerAroundPeds , 25.0, 2.0)
								psJimmy.iTimer = GET_GAME_TIMER()
								
								FailFlags[MISSION_FAIL_JIMMY_LEFT_BEHIND] 			= FALSE
								FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= FALSE
								FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVEABLE] 	= FALSE
								
								psJimmy.bHasTask = TRUE
								
							ENDIF
							
						ENDIF
											
					ENDIF
					
					IF ( bAnimationsSwitched = FALSE )
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) > 0.6275
								DETACH_SYNCHRONIZED_SCENE(iKickOutSceneID)
								SET_SYNCHRONIZED_SCENE_ORIGIN(iKickOutSceneID, GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), GET_ENTITY_ROTATION(vsMichaelsCar.VehicleIndex))
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								TASK_PLAY_ANIM(PLAYER_PED_ID(), "missfam5mcs_6", "push_michael_out_player0", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_ACTIVATE_RAGDOLL_ON_COLLISION, GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID), FALSE, AIK_DISABLE_LEG_IK)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								bAnimationsSwitched = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF ( bAnimationForced = FALSE )
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
						
							IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) >= fAnimationForcePhase //0.780
								
								IF ( bPlayerRagdolledEarly = TRUE )
								
									TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iKickOutSceneID, "missfam5mcs_6", "push_michael_out_player0",
															INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_VEHICLE_IMPACT)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									
									CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1.0)
									
									bAnimationForced = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF ( bPlayerRagdolledEarly = FALSE )					
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)							
							IF IS_PED_RAGDOLL(PLAYER_PED_ID())
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player ped is ragdolling out of the synched scene early.")
								#ENDIF
								SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 10000, 10000, TASK_RELAX)
								bPlayerRagdolledEarly = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_GETTING_UP(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Making player ped ragdoll as player ped was getting up.")
						#ENDIF
						SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 20000, 20000, TASK_RELAX)
					ENDIF
					
					IF ( bPlayerRagdolledEarly = FALSE )
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) > 0.99
								IF IS_PED_RAGDOLL(PLAYER_PED_ID())
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Kick out of car scene finished, player ragdolling normally.")
									#ENDIF
									iStageProgress++
								ELSE
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Kick out of car scene finished, player not ragdolling.")
									#ENDIF
									iStageProgress++
								ENDIF
							ENDIF
						ENDIF
					
					ELIF ( bPlayerRagdolledEarly = TRUE )
					
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) > 0.99
								iStageProgress++
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Kick out of car scene finished with player ragdolling early.")
							#ENDIF
							iStageProgress++
						ENDIF
					
					ENDIF

				BREAK
				
				CASE 3
				
					RUN_VEHICLE_AND_CAMERA_CHECK()
				
					iKickOutSceneID = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_ROTATION(PLAYER_PED_ID()))
					
					SET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID, 0.0)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iKickOutSceneID, TRUE)
					
					CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1.5)
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iKickOutSceneID, "missfam5mcs_6drag", "fam_5_mcs_6_drag_michael",
												INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT,
												RBF_VEHICLE_IMPACT, INSTANT_BLEND_IN, AIK_DISABLE_LEG_IK)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iKickOutSceneID, "fam_5_mcs_6_drag_cam", "missfam5mcs_6drag")
					
					iStageProgress++
				
				BREAK
				
				CASE 4
				
					RUN_VEHICLE_AND_CAMERA_CHECK()
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iKickOutSceneID)
					
						IF NOT DOES_CAM_EXIST(UndergroundCamera)
							UndergroundCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
							SET_CAM_PARAMS(UndergroundCamera, <<-20.194494,-219.796371,37.580395>>, <<-81.945038,-1.126925,-11.828329>>, 29.0)
						ENDIF
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(iKickOutSceneID) >= 0.99
							
							IF DOES_CAM_EXIST(UndergroundCamera)
								SET_CAM_ACTIVE(UndergroundCamera, TRUE)
							ENDIF
							
							iStageProgress++
							
						ENDIF
					ENDIF
				
				BREAK
				
				CASE 5

					RUN_VEHICLE_AND_CAMERA_CHECK()
					
					IF DOES_CAM_EXIST(UndergroundCamera) AND IS_CAM_RENDERING(UndergroundCamera)
								
						iStageProgress++

					ENDIF
				
				BREAK
					
				CASE 6
				
					RUN_VEHICLE_AND_CAMERA_CHECK()
					
					IF DOES_CAM_EXIST(UndergroundCamera) AND IS_CAM_RENDERING(UndergroundCamera)
					
						SET_CINEMATIC_MODE_ACTIVE(FALSE)
						KILL_CHASE_HINT_CAM(sCinematicCamStruct)
					
						REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedZone)
					
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(psJimmy.PedIndex)
						
						DELETE_PED(psJimmy.PedIndex)
						DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
						
						STOP_AUDIO_SCENE("FAMILY_5_DRUGS")
						STOP_AUDIO_SCENE("FAMILY_5_DRUGS_FOCUS_CAM")
						
						REMOVE_ANIM_DICT("missfam5mcs_6")
						REMOVE_ANIM_DICT("missfam5_drink")
						REMOVE_ANIM_DICT("missfam5mcs_6drag")
						
						ANIMPOSTFX_STOP("DrugsDrivingIn")
						//ANIMPOSTFX_PLAY("DMT_flight", 0, TRUE)
						
						QUIT_DRUNK_CAMERA_IMMEDIATELY()
						
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE)
						
						DESTROY_ALL_CAMS()
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						//RETURN TRUE
						iStageProgress++
						
					
					ENDIF			
				BREAK
				
			ENDSWITCH
			
		ENDIF

		RETURN FALSE

	ENDFUNC

#ENDIF

PROC MISSION_CLEANUP()
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting mission cleanup.")
		DELETE_DEBUG_WIDGETS()
	#ENDIF

	//remove vehicles and peds
	IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
		IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsMichaelsCar.VehicleIndex, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vsMichaelsCar.VehicleIndex, VEHICLELOCK_UNLOCKED)
		ENDIF
		IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
			IF 	IS_ENTITY_A_MISSION_ENTITY(vsMichaelsCar.VehicleIndex)
			AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vsMichaelsCar.VehicleIndex)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
			ENDIF
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
		ENDIF
	ENDIF
	
	CLEANUP_PED(psAmanda, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psFabien, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psJimmy, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_PED(psDealer, IS_SCREEN_FADED_OUT(), FALSE)
		
	IF DOES_ENTITY_EXIST(vsParkedVehicles[0].VehicleIndex)
		IF NOT IS_ENTITY_DEAD(vsParkedVehicles[0].VehicleIndex)
			IF 	IS_ENTITY_A_MISSION_ENTITY(vsParkedVehicles[0].VehicleIndex)
			AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vsParkedVehicles[0].VehicleIndex)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vsParkedVehicles[0].VehicleIndex)
			ENDIF
		ENDIF
	ENDIF

	//remove objects
	IF DOES_ENTITY_EXIST(osBSDoorDummy.ObjectIndex)
		REMOVE_MODEL_HIDE(osBSDoor.vPosition, 1.0, osBSDoor.ModelName, NOT IS_SCREEN_FADED_OUT())
	ENDIF
	
	IF DOES_ENTITY_EXIST(osLeftDoorDummy.ObjectIndex)
		REMOVE_MODEL_HIDE(osLeftDoorDummy.vPosition, 1.0, osLeftDoorDummy.ModelName)
	ENDIF

	IF DOES_ENTITY_EXIST(osRightDoorDummy.ObjectIndex)
		REMOVE_MODEL_HIDE(osRightDoorDummy.vPosition, 1.0, osRightDoorDummy.ModelName)
	ENDIF
	
	CLEANUP_MISSION_OBJECT(osNecklace, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osGamepad, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osHeadset, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osCup, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osNote, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osCiggy, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osMethBag, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osBSDoor, FALSE)
	CLEANUP_MISSION_OBJECT(osBSDoorDummy, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osLeftDoorDummy, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT(osRightDoorDummy, IS_SCREEN_FADED_OUT())
	CLEANUP_MISSION_OBJECT_GROUP(osYogaMats,  IS_SCREEN_FADED_OUT())
	
	REMOVE_ANIM_DICT("missfam5_yoga")
	REMOVE_ANIM_DICT("missfam5mcs_6")
	REMOVE_ANIM_DICT("missfam5_drink")
	REMOVE_ANIM_DICT("missfam5_flying")
	REMOVE_ANIM_DICT("missfam5mcs_6drag")
	REMOVE_ANIM_DICT("missfam5_wet_walk")
	REMOVE_ANIM_DICT("missfam5_blackout")
	REMOVE_ANIM_DICT("missfam2mcs_intp1")
	
	REMOVE_ANIM_DICT("missfam5mcs_4leadin")
	REMOVE_ANIM_DICT("missfam5leadinoutmcs_5")
	
	KillAllConversations()
	
	SET_MAX_WANTED_LEVEL(5)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_CREATE_RANDOM_COPS(TRUE)
	
	SET_PED_POPULATION_BUDGET(3)
	SET_VEHICLE_POPULATION_BUDGET(3)
	
	SET_PED_MODEL_IS_SUPPRESSED(U_M_Y_BURGERDRUG_01, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICE, FALSE)
	
	REMOVE_VEHICLE_ASSET(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
	
	//enable the phone 
	DISABLE_CELLPHONE(FALSE)
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	DISPLAY_RADAR(TRUE)
	
	QUIT_DRUNK_CAMERA_IMMEDIATELY()
	STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	STOP_CUTSCENE_CAM_SHAKING(TRUE)
	
	IF DOES_CAM_EXIST(ScriptedCamera)
		SET_CAM_ACTIVE(ScriptedCamera, FALSE)
		DESTROY_CAM(ScriptedCamera)
	ENDIF
	
	IF DOES_CAM_EXIST(UndergroundCamera)
		SET_CAM_ACTIVE(UndergroundCamera, FALSE)
		DESTROY_CAM(UndergroundCamera)
	ENDIF
	
	IF DOES_CAM_EXIST(AnimatedCamera)
		SET_CAM_ACTIVE(AnimatedCamera, FALSE)
		DESTROY_CAM(AnimatedCamera)
	ENDIF
	
	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	
	REMOVE_PTFX_ASSET()
	
	//stop all audio sfx
	STOP_STREAM()
	
	STOP_MISSION_SFX(MISSION_SFX_TAP)
	STOP_MISSION_SFX(MISSION_SFX_VOMIT)
	STOP_MISSION_SFX(MISSION_SFX_DRINK)
	STOP_MISSION_SFX(MISSION_SFX_CHIMPS)
	STOP_MISSION_SFX(MISSION_SFX_ALIENS)
	STOP_MISSION_SFX(MISSION_SFX_HEARTBEAT)
	RELEASE_SFX_SOUND_IDS()
	RELEASE_SCRIPT_AUDIO_BANK()
	UNREGISTER_SCRIPT_WITH_AUDIO()
	STOP_AUDIO_SCENES()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_DRUG_FLIGHT_END")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("FAM5_MICHAEL_LONG_SCREAM")
	SET_AUDIO_SPECIAL_EFFECT_MODE(AUDIO_SPECIAL_EFFECT_MODE_NORMAL)
	
	SET_STATIC_EMITTER_ENABLED("SE_MICHAELS_HOUSE_RADIO", TRUE)	
	
	SET_TIME_SCALE(1.0)
	CLEAR_TIMECYCLE_MODIFIER()
	CLEAR_WEATHER_TYPE_PERSIST()
	ANIMPOSTFX_STOP("DMT_flight")
	ANIMPOSTFX_STOP("DrugsDrivingIn")
	ANIMPOSTFX_STOP("DrugsDrivingOut")
	SET_PARTICLE_FX_CAM_INSIDE_VEHICLE(FALSE)
	
	//stop all music
	IF ( bYogaMusicPlaying = TRUE )
		TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_ENDS")
		bYogaMusicPlaying = FALSE
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event FAM5_YOGA_MUSIC_ENDS.")
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping yoga music.")
		#ENDIF
	ENDIF
	CANCEL_MUSIC_EVENT("FAM5_YOGA_MOVE_START")
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cancelling music event FAM5_YOGA_MOVE_START.")
	#ENDIF
	
	MAKE_PED_SOBER(PLAYER_PED_ID())
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Making player ped sober on mission cleanup.")
	#ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_SCREEN_FADED_OUT()
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			IF IS_PED_RAGDOLL(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
		ENDIF
		SET_ENTITY_PROOFS(PLAYER_PED_ID(),FALSE, FALSE, FALSE, FALSE, FALSE)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2))
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_L2), DOORSTATE_UNLOCKED, FALSE, TRUE)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2))
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_R_R2), DOORSTATE_UNLOCKED, FALSE, TRUE)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
		REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_UNLOCKED, FALSE, TRUE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
	ENDIF
	
	//enable the car gen on mission cleanup
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	
	DISABLE_TV_CONTROLS(TV_LOC_JIMMY_BEDROOM, FALSE)
	
	DISABLE_TAXI_HAILING(FALSE)
	
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN, FALSE)
	
	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedZone)
	
	SET_TV_VOLUME(0)
	
	SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE)
	
	ASSISTED_MOVEMENT_REMOVE_ROUTE("mansion_1")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("family5b")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("family5c")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("family5d")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("f5_jimmy1")
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxPuke)
		STOP_PARTICLE_FX_LOOPED(ptfxPuke)
	ENDIF
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
	
	REMOVE_IPL("SpaceInterior")
	
	STAT_ENABLE_STATS_TRACKING()
	SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission cleanup.")
	#ENDIF
		
ENDPROC

//|============================= END MISSION STAGES FUNCTIONS ============================|

//|=================================== MAIN SCRIPT LOOP ==================================|

SCRIPT

    SET_MISSION_FLAG (TRUE)
	
	//check for death or arrest
    IF HAS_FORCE_CLEANUP_OCCURRED() 

		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		
		STOP_MISSION_SFX(MISSION_SFX_HEARTBEAT)														//stop heartbeat, takes a while to fade out
		
        MISSION_CLEANUP()
		
		SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_DESTROYED, TRUE)				//hide yoga props
				
		IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_DESTROYED )
		OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_DESTROYED )
		OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_DESTROYED )
			SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_NORMAL, TRUE)		//unhide amanda's stuff
			SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_NORMAL, TRUE)		//unhide jimmy's stuff
			SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_NORMAL, TRUE)			// Flowers are ok.
			SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_NORMAL, TRUE)//hide burger shot stuff
		ENDIF
		
		TERMINATE_THIS_THREAD()
		
    ENDIF
	
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_MISSION_STAGE_MENU()
		CREATE_DEBUG_WIDGETS()
	#ENDIF
	
	IF IS_REPLAY_IN_PROGRESS()
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission replay is in progress.")
		#ENDIF
	
		SET_MISSION_STAGE_FOR_REPLAY(eMissionStage, Get_Replay_Mid_Mission_Stage())
		
		bReplayFlag = TRUE
	
		//handle shitskip message
		IF ( g_bShitSkipAccepted = TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip accepted by the player.")
			#ENDIF
		
			eMissionStage = GET_NEXT_MISSION_STAGE_FOR_SHITSKIP(eMissionStage)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip changed mission stage to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
			#ENDIF
		
		ELSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip not triggered.")
			#ENDIF
		
		ENDIF

	ELSE
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_YOGA_WARRIOR), "YOGA - WARRIOR", FALSE)
	ENDIF
	
	
	IF IS_REPEAT_PLAY_ACTIVE()
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active.")
		#ENDIF

		IF NOT IS_REPLAY_IN_PROGRESS()
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active and mission replay is not in progress.")
			#ENDIF
		
			eMissionStage = MISSION_STAGE_CUTSCENE_INTRO
		
			bReplayFlag = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting local replay flag to TRUE for stage loading.")
			#ENDIF
			
			SET_CLOCK_TIME(IDEAL_MISSION_START_TIME, 0, 0)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting clock time to ", IDEAL_MISSION_START_TIME, ".")
			#ENDIF
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission stage is ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), " for repeat play and no mission replay.")
			#ENDIF
		
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active and mission replay is in progress.")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission stage is ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), " for repeat play and mission replay.")
			#ENDIF
		
		ENDIF

	ENDIF
	
	INIT_PC_SCRIPTED_CONTROLS("Yoga")
	bSouthpaw = FALSE

	WHILE TRUE
	
		//check each frame if mission is failed due to fail conditions
		//check unpinning interiors when far away from them
		//check only if mission stage loading has finished
		IF ( bLoadingFlag = TRUE )
			RUN_FAIL_CHECKS(eMissionStage, eMissionFail)
			RUN_INTERIOR_UNPINNING_CHECK(vInteriorPosition, 50.0)
		ENDIF
		
		//handle loading of sfx
		RUN_MISSION_SFX_LOADING(eMissionSfx)
		
		UPDATE_DRUGS_AUDIO_SCENE()
		
		IF ( bBlackoutStagesActive = TRUE )
			HIDE_LOADING_ON_FADE_THIS_FRAME()
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_DidSomebodySayYoga")

		SWITCH eMissionStage
		
			CASE MISSION_STAGE_PRE_TIMELAPSE
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_PRE_TIMELAPSE_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()	
						eMissionStage = MISSION_STAGE_TIMELAPSE
					ENDIF
				ENDIF
			BREAK
		
			CASE MISSION_STAGE_TIMELAPSE
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_TIMELAPSE_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()	
						eMissionStage = MISSION_STAGE_CUTSCENE_INTRO
					ENDIF
				ENDIF
			BREAK
		
			CASE MISSION_STAGE_CUTSCENE_INTRO
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)		
					IF IS_MISSION_STAGE_CUTSCENE_INTRO_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_YOGA_WARRIOR
					ENDIF
				ENDIF
			BREAK

			CASE MISSION_STAGE_YOGA_WARRIOR
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					SET_CLOCK_TIME(iStageTimeHours, iStageTimeMinutes, iStageTimeSeconds)
					IF IS_MISSION_STAGE_YOGA_WARRIOR_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_TRIANGLE
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_TRIANGLE
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)				
					IF IS_MISSION_STAGE_CUTSCENE_TRIANGLE_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_YOGA_TRIANGLE
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_YOGA_TRIANGLE
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					SET_CLOCK_TIME(iStageTimeHours, iStageTimeMinutes, iStageTimeSeconds)
					IF IS_MISSION_STAGE_YOGA_TRIANGLE_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_SUNSALUTATION
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_SUNSALUTATION
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_SUNSALUTATION_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_YOGA_SUNSALUTATION
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_YOGA_SUNSALUTATION
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					SET_CLOCK_TIME(iStageTimeHours, iStageTimeMinutes, iStageTimeSeconds)
					IF IS_MISSION_STAGE_YOGA_SUNSALUTATION_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						SHUTDOWN_PC_SCRIPTED_CONTROLS()
						eMissionStage = MISSION_STAGE_CUTSCENE_POOL
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_POOL
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_POOL_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_GOTO_JIMMYS_ROOM
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_GOTO_JIMMYS_ROOM			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GOTO_JIMMYS_ROOM_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_JIMMYS_ROOM
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_JIMMYS_ROOM_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_GOTO_BURGER_SHOT
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_GOTO_BURGER_SHOT
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GOTO_BURGER_SHOT_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_BURGER_SHOT
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BURGER_SHOT
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.75)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_BURGER_SHOT_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_DRIVE_HOME
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_DRIVE_HOME
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.75)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_DRIVE_HOME_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_BLACKOUT
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_BLACKOUT
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_BLACKOUT_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_FLIGHT
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_FLIGHT
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_FLIGHT_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_GO_HOME_WASTED
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_GO_HOME_WASTED
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GO_HOME_WASTED_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_END_1
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_END_1
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_END_1_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()						
						IF NOT IS_REPEAT_PLAY_ACTIVE()
							eMissionStage = MISSION_STAGE_CUTSCENE_END_2
						ELSE
							eMissionStage = MISSION_STAGE_PASSED
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_END_2
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_END_2_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()						
						eMissionStage = MISSION_STAGE_PASSED
					ENDIF
				ENDIF
			BREAK
			
			#IF IS_DEBUG_BUILD
				CASE MISSION_STAGE_TEST
					IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
						IF IS_MISSION_STAGE_TEST_COMPLETED(iMissionStageProgress)
							RESET_MISSION_FLAGS()
							eMissionStage = MISSION_STAGE_PASSED
						ENDIF
					ENDIF
				BREAK
			#ENDIF
			
			CASE MISSION_STAGE_PASSED
			
				Mission_Flow_Mission_Passed()
				
				MISSION_CLEANUP()
				
				SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_DESTROYED)						//hide yoga props
				
				IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_NORMAL )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_NORMAL )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_DESTROYED)			//hide amanda's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_DESTROYED)				//hide jimmy's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_DESTROYED)			// Flowers are ok.
					SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_DESTROYED)	//unhide burger shot stuff
				ENDIF
				
				TERMINATE_THIS_THREAD()
				
			BREAK
			
			CASE MISSION_STAGE_FAILED
			
				SET_MISSION_FAILED_WITH_REASON(eMissionFail)
				
				CLEAR_HELP()
				CLEAR_PRINTS()
				KILL_ANY_CONVERSATION()
				
				STOP_MISSION_SFX(MISSION_SFX_HEARTBEAT)														//stop heartbeat, takes a while to fade out
				
				TRIGGER_MUSIC_EVENT("FAM5_YOGA_MUSIC_ENDS")

				WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
					WAIT(0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for GET_MISSION_FLOW_SAFE_TO_CLEANUP() to return TRUE.")
					#ENDIF
				ENDWHILE
				
				IF ( eMissionFail = MISSION_FAIL_YOGA_FAILED )
					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-791.1766, 185.8434, 71.8349>>, 188.7433)			//on yoga fail position the player in the back garden
				ENDIF

				IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_YOGA)
					SET_PLAYER_PED_OUTFIT(MO_YOGA_FLIP_FLOPS)
				ENDIF

				MISSION_CLEANUP()
				
				SET_BUILDING_STATE(BUILDINGNAME_IPL_YOGA_GAME, BUILDINGSTATE_DESTROYED, NOT IS_SCREEN_FADED_OUT())				//hide yoga props
				
				IF ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_JIMMYS_STUFF] 	= BUILDINGSTATE_DESTROYED )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_MIC_AMANDAS_STUFF] = BUILDINGSTATE_DESTROYED )
				OR ( g_eCurrentBuildingState[BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT] = BUILDINGSTATE_DESTROYED )
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_JIMMYS_STUFF, BUILDINGSTATE_NORMAL, NOT IS_SCREEN_FADED_OUT())				//unhide amanda's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_AMANDAS_STUFF, BUILDINGSTATE_NORMAL, NOT IS_SCREEN_FADED_OUT())				//unhide jimmy's stuff
					SET_BUILDING_STATE(BUILDINGNAME_ES_MIC_DINING_ROOM_DEAD_FLOWERS, BUILDINGSTATE_NORMAL, NOT IS_SCREEN_FADED_OUT())	// Flowers are ok.
					SET_BUILDING_STATE(BUILDINGNAME_ES_FAMILY5_KITCHEN_BURGERSHOT, BUILDINGSTATE_NORMAL, NOT IS_SCREEN_FADED_OUT())//hide burger shot stuff
				ENDIF

				TERMINATE_THIS_THREAD()
			BREAK
			
		ENDSWITCH

		#IF IS_DEBUG_BUILD																					//handle debug functionality
			RUN_DEBUG_PASS_AND_FAIL_CHECK(eMissionStage, eMissionFail)										//handle key_f and key_s presses
			RUN_DEBUG_MISSION_STAGE_MENU(iReturnStage, eMissionStage, bSkipFlag, bStageResetFlag)			//handle z-skip menu
			RUN_DEBUG_SKIPS(eMissionStage, bSkipFlag, bStageResetFlag)										//handle j-skip and p-skip
			RUN_DEBUG_WIDGETS()																				//handle RAG widgets
		#ENDIF

		WAIT(0)
	
    ENDWHILE
	
ENDSCRIPT
