//|=======================================================================================|
//|                 Author:  Lukasz Bogaj / Jim McMahon	 	Date: 09/01/2012     		  |
//|=======================================================================================|
//|                                  	  CARSTEAL3.sc	                              	  |
//|                            		 	  DEEP INSIDE                          			  |
//|                             									                      |
//|=======================================================================================|


/*| Security peds ambient speech

	//generic
	
	PROVOKE_TRESPASS
	CHALLENGE_THREATEN
	CHALLENGE_ACCEPTED_GENERIC
	PROVOKE_STARING
	GENERIC_HI
	GENERIC_HOWS_IT_GOING
	OVER_THERE

	//mission specific
	
	MOVE_AWAY_WARNING
	DRAW_GUN
	STOP_THE_CAR
	MOVE_AWAY_FROM_THE_CAR
	GET_OUT_OF_THE_CAR
	FREEZE
	COVER_ME
	NEEDED_ON_SET
	GENERIC_SHOCKED_HIGH
	
	//mission voice names
	
	A_M_M_GENERICMALE_01_WHITE_MINI_01
	A_M_M_GENERICMALE_01_WHITE_MINI_02
	A_M_M_GENERICMALE_01_WHITE_MINI_03
	A_M_M_GENERICMALE_01_WHITE_MINI_04

*/

//|==================================== INCLUDE FILES ====================================|

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

//Commands headers
USING "commands_pad.sch"
USING "commands_fire.sch"
USING "commands_misc.sch"
USING "commands_clock.sch"
USING "commands_script.sch"
USING "commands_object.sch"
USING "commands_player.sch"
USING "commands_entity.sch"
USING "commands_camera.sch"
USING "commands_graphics.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_shapetest.sch"
USING "commands_recording.sch"

//Script headers
USING "script_ped.sch"
USING "script_misc.sch"
USING "script_blips.sch"
USING "script_player.sch"

//Public headers
USING "shop_public.sch"
USING "replay_public.sch"
USING "taxi_functions.sch"
USING "locates_public.sch"
USING "dialogue_public.sch"
USING "selector_public.sch"
USING "cutscene_public.sch"
USING "carsteal_public.sch"
USING "weapons_public.sch"
USING "cellphone_public.sch"
USING "mission_stat_public.sch"
USING "building_control_public.sch"
USING "flow_public_core.sch"
USING "flow_public_core_override.sch"

//Other headers
USING "area_checks.sch"
USING "emergency_call.sch"
USING "jb700_gadgets.sch"

//Debug headers
#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//|===================================== ENUMS & STRUCTS ==================================|

/// PURPOSE: Specifies stages of the mission.
ENUM MISSION_STAGES
	MISSION_STAGE_GET_TO_ACTOR					= 0,	//Franklin knocks out movie star to steal his suit or steals the car
	MISSION_STAGE_CUTSCENE_TRAILER				= 1,	//Franklin drags the body of the movie star into the trailer
	MISSION_STAGE_GET_TO_CAR					= 2,	//Franklin gets to the target car and steals it
	MISSION_STAGE_ESCAPE_FILM_SET				= 3,	//Franklin drives the stolen car out of the film set and loses the security
	MISSION_STAGE_DELIVER_CAR					= 4,	//Franklin drives the stolen car to the garage
	MISSION_STAGE_CUTSCENE_END					= 5,	//Franklin delivers the stolen car to the garage and meets Devin
	MISSION_STAGE_EJECTOR_TEST					= 6,
	
	
	MISSION_STAGE_PASSED,								//Mission passed
	MISSION_STAGE_FAILED								//Mission failed
ENDENUM

/// PURPOSE: Specifies mission fail reasons.
ENUM MISSION_FAILS
	MISSION_FAIL_EMPTY							= 0,	//Generic fail
	MISSION_FAIL_CAR_DEAD,								//Mission car destroyed
	MISSION_FAIL_CAR_UNDRIVABLE,						//Mission car undrivable
	MISSION_FAIL_CAR_LEFT_BEHIND,						//Mission car left behind
	MISSION_FAIL_STUDIO_LEFT_BEHIND,					//Film studio left behind
	MISSION_FAIL_COPS_AT_GARAGE,						//Player arrived at the garage with cops around
	MISSION_FAIL_ACTRESS_AT_GARAGE,						//Player arrived at the garage with actress in the mission car
	MISSION_FAIL_SECURITY_AT_GARAGE,					//Player arrived at the garage security around
	MISSION_FAIL_DEVIN_DEAD,							//Devin died
	MISSION_FAIL_DEVIN_ATTACKED,						//Devin attacked
	MISSION_FAIL_FORCE_FAIL,							//Debug forced fail
	
	MAX_MISSION_FAILS
ENDENUM

/// PURPOSE: Specifies mid-mission checkpoints for mission retry.
ENUM MID_MISSION_STAGES
	MID_MISSION_STAGE_GET_TO_ACTOR				= 0,	//Checkpoint at MISSION_STAGE_GET_TO_ACTOR
	MID_MISSION_STAGE_GET_TO_CAR				= 1,	//Checkpoint at MISSION_STAGE_GET_TO_CAR
	MID_MISSION_STAGE_ESCAPE_FILM_SET			= 2,	//Checkpoint at MISSION_STAGE_ESCAPE_FILM_SET
	MID_MISSION_STAGE_DELIVER_CAR				= 3		//Checkpoint at MISSION_STAGE_DELIVER_CAR
ENDENUM

/// PURPOSE: Specifies ped behaviour states.
ENUM PED_STATES
	PED_STATE_IDLE								= 0,
	PED_STATE_DEAD,
	PED_STATE_ARGUING,
	PED_STATE_MOVING_TO_LOCATION,
	PED_STATE_STANDING_AT_LOCATION,
	PED_STATE_PLAYING_ANIM_1,
	PED_STATE_PLAYING_ANIM_2,
	PED_STATE_PLAYING_ANIM_3,
	PED_STATE_PLAYING_ANIM_4,
	PED_STATE_STARTING_SCENARIO,
	PED_STATE_STARTING_SCENARIO_WARP,
	PED_STATE_USING_SCENARIO,
	PED_STATE_BLOCKING_PLAYER,
	PED_STATE_JACKING_PLAYER,
	PED_STATE_WATCHING_PLAYER,
	PED_STATE_STUNNED,
	PED_STATE_SITTING_IN_CAR,
	PED_STATE_BEING_EJECTED,
	PED_STATE_BEING_STEALTH_KILLED,
	PED_STATE_FLEEING,
	PED_STATE_IMMOBILIZED,
	PED_STATE_INTIMIDATION,
	PED_STATE_COMBAT_AIMING,
	PED_STATE_COMBAT_AIMING_PED,
	PED_STATE_COMBAT_ON_FOOT,
	PED_STATE_COMBAT_DEFENSIVE,
	PED_STATE_ENTERING_VEHICLE,
	PED_STATE_COMBAT_FROM_CAR_SHOOTING,
	PED_STATE_COMBAT_FROM_CAR_NO_SHOOTING
ENDENUM

ENUM PED_CLEANUP_REASONS
	PED_CLEANUP_NO_CLEANUP						= 0,
	PED_CLEANUP_LOST							= 1,
	PED_CLEANUP_KILLED_BY_PLAYER_STEALTH		= 2,
	PED_CLEANUP_KILLED_BY_PLAYER_TAKEDOWN		= 3,
	PED_CLEANUP_KILLED_BY_PLAYER_LETHAL			= 4,
	PED_CLEANUP_KILLED_BY_PLAYER_VEHICLE		= 5,
	PED_CLEANUP_KILLED_BY_SCRIPT				= 6
ENDENUM

ENUM FILM_CREW_PEDS
	FCP_DIRECTOR								= 0,	//Film director
	FCP_GRIP_1									= 1,	//Grip next to the director
	FCP_GRIP_2									= 2,	
	FCP_GRIP_3									= 3,
	FCP_GRIP_4									= 4,
	FCP_GRIP_5									= 5,
	
	MAX_FILM_CREW_PEDS
ENDENUM

ENUM MELTDOWN_SCENE_PEDS
	MSP_ACTORA									= 0,
	MSP_ACTORB									= 1,
	MSP_CAMERAMAN								= 2,
	MSP_BOOMMAN									= 3,
	MSP_DIRECTOR								= 4,
	
	MAX_MELTDOWN_SCENE_PEDS
ENDENUM

HASH_ENUM STUDIO_GATES_HASH_ENUM
	DOORHASH_STUDIO_SIDE_GATE_L,
	DOORHASH_STUDIO_SIDE_GATE_R,
	DOORHASH_GARAGE_FRONT
ENDENUM

/// PURPOSE: Specifies details of mission peds.
STRUCT PED_STRUCT
	PED_INDEX				PedIndex					//Ped Index
	BLIP_INDEX				BlipIndex					//Ped's Blip Index
	VECTOR					vPosition					//Ped's start location
	FLOAT					fHeading					//Ped's start heading
	MODEL_NAMES				ModelName					//Ped's model
	INT						iConversationTimer			//Ped's conversation timer
	INT						iConversationCounter
	INT						iProgress
	INT						iTimer						//Ped's timer
	INT						iLOSTimer
	INT						iCleanupAttempts
	BOOL					bCanSeeTargetPed
	BOOL					bHasTask
	PED_STATES				eState
	PED_CLEANUP_REASONS		eCleanupReason
	FLOAT					fDistanceToTarget
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31		sDebugName
	#ENDIF
ENDSTRUCT

/// PURPOSE: Specifies details of mission vehicles.
STRUCT VEH_STRUCT 
	VEHICLE_INDEX			VehicleIndex				//Vehicle index
	BLIP_INDEX				BlipIndex					//Vehicle blip
	VECTOR					vPosition					//Vehicle postion
	FLOAT					fHeading					//Vehicle heading
	MODEL_NAMES				ModelName					//Vehicle model
	INT 					iProgress
	INT						iTimer
	VECTOR					vOffset
	VECTOR					vTargetPosition
	FLOAT					fTargetHeading
	BOOL					bImmobilized
	BOOL					bAutoDriveActive
	BOOL					bLineOfSightToTarget
ENDSTRUCT

/// PURPOSE: Specifies details of mission objects
STRUCT OBJECT_STRUCT
	OBJECT_INDEX			ObjectIndex					//Object index
	VECTOR					vPosition					//Object position
	VECTOR					vRotation					//Object rotation
	MODEL_NAMES				ModelName					//Object model
ENDSTRUCT

/// PURPOSE: Specifies ped postion and heading
STRUCT LOCATION_STRUCT		
	VECTOR 					vPosition				
	FLOAT 					fHeading
ENDSTRUCT

//|======================================= CONSTANTS =====================================|

CONST_INT 					MAX_MODELS 							8
CONST_INT 					MAX_CAR_RECORDINGS					6		
CONST_INT 					MAX_ANIMATIONS   					2

CONST_INT					ABANDON_STUDIO_FAIL_RANGE			225
CONST_INT					ABANDON_VEHICLE_FAIL_RANGE			100

TWEAK_FLOAT					BLIP_SLIDE_AMOUNT					1.0

TWEAK_FLOAT					fEjectScenePhase					0.575

MODEL_NAMES					mnSecurityCar						= SCHAFTER2
MODEL_NAMES					mnSecurityPed						= S_M_M_SECURITY_01
MODEL_NAMES					mnSpike								= PROP_TYRE_SPIKE_01

VECTOR 						vInteriorPosition					= << 479.88, -1318.57, 28.20 >>//<< 480.51, -1318.31, 28.20 >>			//garage interior coordinates
VECTOR						vFilmSetCenterPosition				= << -1187.46, -501.22, 35.42 >>		//film set center coordinates

VECTOR						vFilmCrewPedsScenePosition			= << -1178.52, -509.19, 34.56 >>
VECTOR						vFilmCrewPedsSceneRotation			= << 0.0, 0.0, 3.24 >>

VECTOR						vEjectorSeatSecondaryForce			= << 0.0, -2.5, 1.0 >>

VECTOR						vMeltdownScenePosition				= << -1130.828, -451.637, 34.620 >>
VECTOR						vMeltdownSceneRotation				= << 1.000, -0.000, -128.000 >>

VECTOR						vEjectorSeatPtfxOffset				= << 0.0, -0.4, 0.75 >>
VECTOR						vEjectorSeatPtfxRotation			= << 0.0, 0.0, 0.0 >>

TEXT_LABEL					sLabelDCAR							= "CAR4_DCAR"
TEXT_LABEL					sLabelGENGETIN						= "CMN_GENGETIN"
TEXT_LABEL					sLabelGENGETBCK						= "CMN_GENGETBCK"

STRING						SECURITY1VOICENAME					= "A_M_M_GENERICMALE_01_WHITE_MINI_01"
STRING						SECURITY2VOICENAME					= "A_M_M_GENERICMALE_01_WHITE_MINI_02"
STRING						SECURITY3VOICENAME					= "A_M_M_GENERICMALE_01_WHITE_MINI_03"
STRING						SECURITY4VOICENAME					= "A_M_M_GENERICMALE_01_WHITE_MINI_04"

///used for reading bits from INT  g_iCarSteal3AgentBitSet from gta5/script/dev/singleplayer/include/globals/mission_globals.sch
CONST_INT 					PLAYER_GOT_TUXEDO					0
CONST_INT 					ACTOR_KILLED    					1
CONST_INT 					ACTOR_STEALTH_KILLED    			2
CONST_INT 					ACTOR_IGNORED    					3
CONST_INT					ACTRESS_FLED_CAR					4

//|======================================= VARIABLES =====================================|

MISSION_STAGES 				eMissionStage 						= MISSION_STAGE_GET_TO_ACTOR
MISSION_FAILS				eMissionFail 						= MISSION_FAIL_EMPTY

//relationship groups
REL_GROUP_HASH 				rgFilmCrew


//mission peds
PED_STRUCT					psActor
PED_STRUCT					psDevin
PED_STRUCT					psToilet
PED_STRUCT					psActress
PED_STRUCT					psAssistant

//mission vehicles
VEH_STRUCT					vsPlayersCar

//mission objects
OBJECT_STRUCT				osPropLamp
OBJECT_STRUCT				osPropChair
OBJECT_STRUCT				osTrailerDoor
OBJECT_STRUCT				osPropToilet
OBJECT_STRUCT				osPropClipboard
OBJECT_STRUCT				osPropGreenScreen
OBJECT_STRUCT				osPropCamera
OBJECT_STRUCT				osPropMicrophone

//player's mission location
LOCATION_STRUCT				sMissionPosition

//locates header data
LOCATES_HEADER_DATA 		sLocatesData

JB700_GADGET_DATA			CarGadgetData

//conversation struct
structPedsForConversation 	CST4Conversation

BLIP_INDEX					DestinationBlip

//progress counters and flags for loading, skipping and replays
INT							iStageSetupProgress
INT 						iMissionStageProgress
BOOL 						bSkipFlag
BOOL 						bReplayFlag
BOOL 						bLoadingFinishedFlag

INT 						iModelsRequested
INT 						iAnimationsRequested

BOOL						bEndCallMessy
BOOL						bCutsceneSkipped
BOOL						bSecurityAlerted
BOOL						bSpawnActorSecurity
BOOL						bDoorBargePlayed
BOOL						bDoorBargeInterrupted
BOOL						bRepositionActor
BOOL						bRepositionAssistant
BOOL						bPlayerWearingTuxedo
BOOL						bAlienWalkDisrupted
BOOL						bActorSceneDisrupted
BOOL						bMeltdownSceneDisrupted
BOOL						bMeltdownSceneWalkedInto
BOOL						bFilmCrewGripAlerted
BOOL						bDirectorSceneDisrupted				//has player disrupted the director and grip animations at film set
BOOL						bPlayerInsideFilmStudio
BOOL						bPlayerEnteredTargetVehicle
BOOL 						bHasOutfitChangedBeforeMission    	//variable for storing the state of “player has changed outfit”
BOOL						bFilmStudioAmbientPedsBlocked

BOOL						CST4_DSC_TRIGGERED
BOOL						CST4_DSG_TRIGGERED
BOOL						CST4_DSW_TRIGGERED
BOOL						CST4_DSG_PLAYING_TRIGGERED

BOOL						CST4_DBRAN_1_TRIGGERED
BOOL						CST4_DBRAN_2_TRIGGERED
BOOL						CST4_DBRAN_3_TRIGGERED
BOOL						CST4_DBRAN_4_TRIGGERED
BOOL						CST4_DBRAN_5_TRIGGERED

BOOL						CST4_DSCAR_TRIGGERED
BOOL						CST4_DSSET1_TRIGGERED
BOOL						CST4_DSSET2_TRIGGERED
BOOL						CST4_DCHAT1_TRIGGERED
BOOL						CST4_DCHAT2_TRIGGERED

FLOAT 						fDistanceFromPlayerToTargetVehicle

INT							iFilmCrewProgress
//INT							iAlienSceneProgress
INT							iDeadActorProgress
INT							iToiletSceneProgress
INT							iMeltdownSceneProgress
INT							iMeltdownDialogueProgress
INT							iWalkingAliensProgress
INT							iSetSecurityProgress
INT							iSetSecurityAlertTimer
INT							iGateSecurityProgress
INT							iActorSecurityProgress
INT							iMeltdownSecurityProgress
INT							iDirectorAndGripProgress
INT							iDirectorFleeShoutsCounter
INT							iDirectorFranklinShoutsCounter

BOOL						bChaseStarted
BOOL 						bChaseFinished
BOOL						bChaseInProgress

INT							iSecurityChaseProgress
INT							iSecurityChaseDelayTimer
BOOL						bSecurityInVehiclesAllowed
BOOL						bStartSecurityChaseTimerDelay

VECTOR						vClosestRoadNode
FLOAT						fClosestRoadNodeHeading

INT							iLeadInProgress
BOOL						bLeadInTriggered

BOOL						bSpikesAllowed
BOOL						bSpikesUnlocked
INT							iSpikesDelayTime
INT							iSpikesDelayTimer
INT							iCarStoppedTimer

//BOOL						bEndPhoneCallFinished
INT							iPhoneCallToDevinDelayTimer

INT							iPlayerLethalKills
INT							iPlayerStealthKills
INT							iPlayerTakedownKills
INT							iTotalSpikeHits
INT							iSecurityCarsImmobilized
INT							iSuccessfullSpikesHits
BOOL						bCarSecurityAggressive

BOOL						bActressEjected
BOOL						bEjectorSeatActive
BOOL						bEjectorSeatAllowed
INT							iEjectorSeatTimer
INT							iEjectorSeatProgress

INT							iSteerBiasTimer
FLOAT						fSteerBiasValue
BOOL						bSteerBiasActive

BOOL						bRandomConversationsAllowed
INT							iActressShootConversationCounter
INT							iActressSecurityConversationCounter

BOOL						bMusicEventMissionStartTriggered
BOOL						bMusicEventTrailerTriggered
BOOL						bMusicEventDropTriggered
BOOL						bMusicEventDeliverTriggered
BOOL						bMusicEventSetAlertTriggered

INT							iEjectSceneID		= -1
INT							iCarTrackedPoint 	= -1
INT							iActorTrackedPoint 	= -1
INT							iMeltdownSceneID	= -1
INT							iDeadActorSceneID	= -1

INT							iEjectorScreamSoundID

STRING						sDirectorAnimName
WEAPON_TYPE					ActorCauseOfDeathWeapon

SCENARIO_BLOCKING_INDEX		ScenarioBlockingArea1
SCENARIO_BLOCKING_INDEX		ScenarioBlockingArea2

BOOL						bInteriorLoadSceneLoaded
BOOL						bReplayRecordBackFortimeTriggered

//|========================================= ARRAYS ======================================|

BOOL						FailFlags[MAX_MISSION_FAILS]

MODEL_NAMES 				MissionModels[MAX_MODELS]
STRING						MissionAnimations[MAX_ANIMATIONS]

PED_STRUCT					psAliens[2]
PED_STRUCT					psSetSecurity[5]
PED_STRUCT					psCarSecurity[2]
PED_STRUCT					psActorSecurity[3]
PED_STRUCT					psMeltdownSecurity[1]
PED_STRUCT					psNorthGateSecurity[1]
PED_STRUCT					psSouthGateSecurity[1]
PED_STRUCT					psFilmCrew[MAX_FILM_CREW_PEDS]
PED_STRUCT					psMeltdownCrew[5]

VEH_STRUCT					vsSecurityCars[2]

BOOL						SecuritySpawned[8]
LOCATION_STRUCT				SecuritySpawnPoints[8]

INT							iNumTextHashesStored
INT 						iTriggeredTextHashes[50]

STRING						EndCallCleanRoots[9]
STRING						EndCallCleanLines[9]

STRING						EndCallMessyRoots[12]
STRING						EndCallMessyLines[12]

//|========================= MISCELLANEOUS PROCEDURES & FUNCTIONS ========================|

/// PURPOSE:
///    Resets all mission flags and progress counters to their default values.
PROC RESET_MISSION_FLAGS()

	//reset setup progress to 1, so that iSetupProgress = 0 only runs once, on initial mission load.
	iStageSetupProgress 				= 1
	iMissionStageProgress				= 0
	
	bEndCallMessy						= FALSE
	bCutsceneSkipped					= FALSE
	bDoorBargePlayed					= FALSE
	bDoorBargeInterrupted				= FALSE
	bPlayerWearingTuxedo				= FALSE
	bAlienWalkDisrupted					= FALSE
	bActorSceneDisrupted				= FALSE
	bFilmCrewGripAlerted				= FALSE
	bMeltdownSceneDisrupted				= FALSE
	bMeltdownSceneWalkedInto			= FALSE
	bDirectorSceneDisrupted				= FALSE
	
	psActor.iProgress					= 0

	
	iFilmCrewProgress					= 0
	//iAlienSceneProgress				= 0
	iDeadActorProgress					= 0
	iToiletSceneProgress				= 0
	iWalkingAliensProgress				= 0
	iMeltdownSceneProgress				= 0
	iMeltdownDialogueProgress			= 0
	iSetSecurityProgress				= 0
	iSetSecurityAlertTimer				= 0
	iGateSecurityProgress				= 0
	iActorSecurityProgress				= 0
	iMeltdownSecurityProgress			= 0
	iDirectorAndGripProgress			= 0
	
	bChaseStarted						= FALSE
	bChaseFinished						= FALSE
	bChaseInProgress					= FALSE

	iLeadInProgress						= 0
	bLeadInTriggered					= FALSE
	
	bSpikesAllowed						= FALSE
	bSpikesUnlocked						= FALSE
	iSpikesDelayTime					= 0
	iSpikesDelayTimer					= 0
	iCarStoppedTimer					= 0
	
	iTotalSpikeHits						= 0
	iSecurityCarsImmobilized			= 0
	iSuccessfullSpikesHits				= 0

	bRepositionActor					= FALSE
	bRepositionAssistant				= FALSE
	bSpawnActorSecurity					= FALSE
	
	bActressEjected						= FALSE
	bEjectorSeatActive					= FALSE
	bEjectorSeatAllowed					= FALSE
	iEjectorSeatTimer					= 0
	iEjectorSeatProgress				= 0
	
	iSteerBiasTimer						= 0
	fSteerBiasValue						= 0.0
	bSteerBiasActive					= FALSE

	iSecurityChaseProgress				= 0
	iSecurityChaseDelayTimer			= 0
	bSecurityInVehiclesAllowed			= FALSE
	bStartSecurityChaseTimerDelay		= FALSE
	
//	bEndPhoneCallFinished				= FALSE
	iPhoneCallToDevinDelayTimer			= 0
	
	iActressShootConversationCounter	= 0
	iActressSecurityConversationCounter	= 0
	
	bMusicEventMissionStartTriggered	= FALSE
	bMusicEventTrailerTriggered			= FALSE
	bMusicEventDropTriggered			= FALSE
	bMusicEventDeliverTriggered			= FALSE
	bMusicEventSetAlertTriggered		= FALSE
	
	bInteriorLoadSceneLoaded			= FALSE
	bReplayRecordBackFortimeTriggered	= FALSE

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission flags and progress counters are reset.")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets all fail flags to specified boolean value.
/// PARAMS:
///    bNewBool - Boolean value to set all flags to TRUE or FALSE.
PROC SET_FAIL_FLAGS(BOOL bValue)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(FailFlags) - 1 )
		FailFlags[i] = bValue
	ENDFOR

ENDPROC

PROC KEEP_GARAGE_DOOR_SHUT()

	UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_HAYES_GARAGE, PLAYER_PED_ID())
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID )
		g_sAutoDoorData[AUTODOOR_HAYES_GARAGE].currentOpenRatio = 0.0
		DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, g_sAutoDoorData[AUTODOOR_HAYES_GARAGE].currentOpenRatio, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
	ENDIF

ENDPROC

PROC REGISTER_STUDIO_GATES()

	//register side gate left
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L))
		ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), PROP_BH1_03_GATE_L, <<-965.1991, -504.0431, 37.9792>>, FALSE, FALSE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding door DOORHASH_STUDIO_SIDE_GATE_L with int ", ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), " to system.")
		#ENDIF
	ENDIF
	
	//register side gate right
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R))
		ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), PROP_BH1_03_GATE_R, <<-962.2961, -509.5990, 37.9779>>, FALSE, FALSE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding door DOORHASH_STUDIO_SIDE_GATE_R with int ", ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), " to system.")
		#ENDIF
	ENDIF
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_GARAGE_FRONT))
		ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_GARAGE_FRONT), PROP_SC1_06_GATE_R, <<500.1759, -1320.5450, 28.2499>>)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding door DOORHASH_GARAGE_FRONT with int ", ENUM_TO_INT(DOORHASH_GARAGE_FRONT), " to system.")
		#ENDIF
	ENDIF
	
ENDPROC

PROC UNREGISTER_STUDIO_GATES()

	//unregister side gate left
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L))
		REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L))
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing door DOORHASH_STUDIO_SIDE_GATE_L with int ", ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), " from system.")
		#ENDIF
	ENDIF
	
	//unregister side gate right
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R))
		REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R))
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing door DOORHASH_STUDIO_SIDE_GATE_R with int ", ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), " from system.")
		#ENDIF
	ENDIF
	
	//unregister garage main front gate
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_GARAGE_FRONT))
		REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_GARAGE_FRONT))
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing door DOORHASH_GARAGE_FRONT with int ", ENUM_TO_INT(DOORHASH_GARAGE_FRONT), " from system.")
		#ENDIF
	ENDIF

ENDPROC

PROC LOCK_STUDIO_GATES()

	//lock north gate barrier leading out
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT), DOORSTATE_LOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Locking door DOORHASH_STUDIO_NORTH_GATE_OUT with int ", ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT), ".")
		#ENDIF
	ENDIF
	
	//lock north gate barrier leading in
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN), DOORSTATE_LOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Locking door DOORHASH_STUDIO_NORTH_GATE_IN with int ", ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN), ".")
		#ENDIF
	ENDIF

	//lock side gate left
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), DOORSTATE_LOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Locking door DOORHASH_STUDIO_SIDE_GATE_L with int ", ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), ".")
		#ENDIF
	ENDIF
	
	//lock side gate right
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), DOORSTATE_LOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Locking door DOORHASH_STUDIO_SIDE_GATE_R with int ", ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), ".")
		#ENDIF
	ENDIF

ENDPROC

PROC UNLOCK_STUDIO_GATES()

	//unlock north gate barrier leading out
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT), DOORSTATE_UNLOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unlocking door DOORHASH_STUDIO_NORTH_GATE_OUT with int ", ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_OUT), ".")
		#ENDIF
	ENDIF
	
	//unlock north gate barrier leading in
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN), DOORSTATE_UNLOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unlocking door DOORHASH_STUDIO_NORTH_GATE_IN with int ", ENUM_TO_INT(DOORHASH_STUDIO_NORTH_GATE_IN), ".")
		#ENDIF
	ENDIF
	
	//unlock side gate left
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), DOORSTATE_UNLOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unlocking door DOORHASH_STUDIO_SIDE_GATE_L with int ", ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_L), ".")
		#ENDIF
	ENDIF
	
	//unlock side gate right
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unlocking door DOORHASH_STUDIO_SIDE_GATE_R with int ", ENUM_TO_INT(DOORHASH_STUDIO_SIDE_GATE_R), ".")
		#ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Warps specified ped to new coordinates and sets specified ped's new heading.
/// PARAMS:
///    ped - PED_INDEX to warp.
///    vNewPosition - VECTOR coordinates specifying new ped's position.
///    fNewHeading - FLOAT specifying new ped's heading.
///    bKeepVehicle - Specifies if ped should keep vehicle they are currently in.
///    bResetGameplayCamera - Specifies if gameplay camera should be positioned behind player's back. Works only for PLAYER_PED_ID().
///    bLoadScene - Specifies if scene at new provided coordinates should be loaded. Works only for PLAYER_PED_ID().
PROC WARP_PED(PED_INDEX ped, VECTOR vNewPosition, FLOAT fNewHeading, BOOL bKeepVehicle, BOOL bResetGameplayCamera, BOOL bLoadScene)

	IF NOT IS_PED_INJURED(ped)
	
		IF ( bKeepVehicle = TRUE )
			SET_PED_COORDS_KEEP_VEHICLE(ped, vNewPosition)
		ELIF ( bKeepVehicle = FALSE ) 
			SET_ENTITY_COORDS(ped, vNewPosition)
		ENDIF
		
		SET_ENTITY_HEADING(ped, fNewHeading)
		
		IF ( ped = PLAYER_PED_ID())
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
			ENDIF
		
			IF ( bResetGameplayCamera = TRUE)
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
			ENDIF
			
			IF ( bLoadScene = TRUE )
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", vNewPosition, ". This might take a while to load.")
				#ENDIF
			
				LOAD_SCENE(vNewPosition)
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_PED_DRIVING_VEHICLE(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex)

	IF NOT IS_PED_INJURED(PedIndex)
		IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
			IF IS_PED_SITTING_IN_VEHICLE(PedIndex, VehicleIndex)
				IF NOT IS_VEHICLE_STOPPED(VehicleIndex)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_PLAYER_PED_OUTFIT_TO_TUXEDO(BOOL bSetTuxedo)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		IF ( bSetTuxedo = TRUE )	
		
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_TUXEDO, FALSE)

			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN)	//set that the player has changed clothes on mission

			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped outfit to OUTFIT_P1_WHITE_TUXEDO.")
			#ENDIF
			
			CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
			RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing player ped blood damage and visible damage.")
			#ENDIF
			
		ELIF ( bSetTuxedo = FALSE )
		
			IF NOT IS_REPEAT_PLAY_ACTIVE()
		
				RESTORE_MISSION_START_OUTFIT()
				
				RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN, bHasOutfitChangedBeforeMission) //reset that the player has changed outfit on mission to how it was at the start of the mission
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring player ped mission start outfit.")
				#ENDIF
				
			ELSE
			
				IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_TUXEDO)
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player current outfit is OUTFIT_P1_WHITE_TUXEDO on repeat play.")
					#ENDIF

					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped outfit to OUTFIT_P1_DEFAULT.")
					#ENDIF
					
				ENDIF
				
			ENDIF

		ENDIF
		
	ENDIF

ENDPROC

PROC CREATE_PED_NON_CREATION_AREA(VECTOR vPosition, VECTOR vSize)
	SET_PED_NON_CREATION_AREA(<< vPosition.x - vSize.x, vPosition.y - vSize.y, vPosition.z - vSize.z >>,
							  << vPosition.x + vSize.x, vPosition.y + vSize.y, vPosition.z + vSize.z >>)
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating ped non creation area at coords ", vPosition, " with size ", vSize, ".")
	#ENDIF
ENDPROC

FUNC SCENARIO_BLOCKING_INDEX CREATE_SCENARIO_BLOCKING_AREA(VECTOR vPosition, VECTOR vSize)

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating scenario blocking area at coords ", vPosition, " with size ", vSize, ".")
	#ENDIF

	RETURN ADD_SCENARIO_BLOCKING_AREA(<< vPosition.x - vSize.x, vPosition.y - vSize.y, vPosition.z - vSize.z >>,
									  << vPosition.x + vSize.x, vPosition.y + vSize.y, vPosition.z + vSize.z >>)
ENDFUNC

FUNC BOOL IS_ENTITY_IN_FILM_STUDIO(ENTITY_INDEX EntityIndex)

	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
		
			IF IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1205.642822,-531.999146,25.343176>>, <<-1074.152588,-462.691223,67.819405>>, 102.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1200.559082,-581.003723,25.353422>>, <<-1072.527832,-462.328857,67.838409>>, 104.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1242.769531,-537.128479,26.775068>>, <<-1177.131470,-481.667603,67.787155>>, 82.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1182.017822,-463.759888,29.759914>>, <<-1220.528931,-491.323792,67.660599>>, 58.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1040.278931,-524.405273,34.038719>>, <<-973.457825,-488.391327,68.321289>>, 54.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1063.079834,-546.856323,33.529106>>, <<-1045.254883,-485.873199,68.180908>>, 55.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1182.097900,-587.645691,25.248831>>, <<-1004.804077,-489.698608,71.056602>>, 32.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1070.755127,-551.146790,32.089046>>, <<-1027.902588,-536.425659,67.631119>>, 28.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1093.414185,-560.927551,31.959763>>, <<-1070.760376,-551.153198,67.088013>>, 28.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1155.982300,-594.167358,25.453455>>, <<-1093.336792,-560.974243,66.957108>>, 28.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1118.298340,-439.908905,34.260933>>, <<-1141.630127,-442.671783,67.496567>>, 29.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-996.248474,-533.809570,34.814335>>, <<-1021.386475,-504.800110,67.985336>>, 22.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1014.585815,-545.387634,33.798470>>, <<-1039.673828,-487.312805,68.180908>>, 22.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(EntityIndex, <<-1030.172974,-483.188324,35.181511>>, <<-1017.088745,-507.492035,68.150673>>, 20.000000)
			
				RETURN TRUE
				
			ENDIF
		
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

PROC RUN_INTERIOR_PINNING_AND_UNPINNING_CHECK(VECTOR vInteriorCoords, FLOAT fRadius)

	IF ( GET_DISTANCE_BETWEEN_COORDS(vInteriorCoords, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE) > fRadius )
	
		//unpin the interior if it is pinned and player is away from it
		IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorCoords))

			UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorCoords))
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unpinning interior at coordinates ", vInteriorCoords, " in memory.")
			#ENDIF
			
		ENDIF
		
	ELSE
	
		//unpin the interior if it is pinned and player is away from it
		IF NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorCoords))
		
			PIN_INTERIOR_IN_MEMORY(GET_INTERIOR_AT_COORDS(vInteriorCoords))
					
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Pinning interior at coordinates ", vInteriorCoords, " in memory.")
			#ENDIF
		
		ENDIF
		
	ENDIF
		
ENDPROC

PROC CREATE_TRACKED_POINT_FOR_COORD(INT &iTrackedPoint, VECTOR vPosition, FLOAT fRadius)

	IF ( iTrackedPoint = -1  )
		iTrackedPoint = CREATE_TRACKED_POINT()
		SET_TRACKED_POINT_INFO(iTrackedPoint, vPosition, fRadius)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating tracked point with id ", iTrackedPoint, " for coord ", vPosition, " and radius ", fRadius, ".")
		#ENDIF
	ENDIF

ENDPROC

PROC CLEANUP_TRACKED_POINT(INT &iTrackedPoint)

	IF ( iTrackedPoint != -1  )
		DESTROY_TRACKED_POINT(iTrackedPoint)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Destroying tracked point with id ", iTrackedPoint, ".")
		#ENDIF
		iTrackedPoint = -1
	ENDIF

ENDPROC

FUNC BOOL DOES_TRACKED_POINT_EXIST(INT iTrackedPoint)

	RETURN iTrackedPoint != -1
	
ENDFUNC

PROC CLEANUP_OBJECT(OBJECT_INDEX &ObjectIndex, BOOL bDelete = FALSE, BOOL bUnfreezeObjectOnCleanup = TRUE)

	IF DOES_ENTITY_EXIST(ObjectIndex)
		IF IS_ENTITY_ATTACHED(ObjectIndex)
			IF IS_SCREEN_FADED_OUT()
				DETACH_ENTITY(ObjectIndex)
			ENDIF
		ENDIF

		IF bUnfreezeObjectOnCleanup = TRUE
			FREEZE_ENTITY_POSITION(ObjectIndex, FALSE)
		ENDIF

		IF ( bDelete = TRUE )
			DELETE_OBJECT(ObjectIndex)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectIndex)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CLEANUP_PED(PED_STRUCT &ped, BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	IF ( bDelete = FALSE )
	
		IF DOES_ENTITY_EXIST(ped.PedIndex)
			
			IF ( bDeathOnly = TRUE )
		
				IF IS_PED_INJURED(ped.PedIndex)

					IF IS_ENTITY_DEAD(ped.PedIndex)
						REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
					ENDIF
					
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF

				ENDIF
				
			ELIF ( bDeathOnly = FALSE )

				IF NOT IS_PED_INJURED(ped.PedIndex)
				
					SET_PED_KEEP_TASK(ped.PedIndex, bKeepTask)
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF
					
				ENDIF

			ENDIF
		
		ENDIF
	
	ELIF ( bDelete = TRUE )

		IF DOES_ENTITY_EXIST(ped.PedIndex)

			IF IS_PED_INJURED(ped.PedIndex)
			OR NOT IS_PED_INJURED(ped.PedIndex)

				IF IS_ENTITY_DEAD(ped.PedIndex)
				OR NOT IS_ENTITY_DEAD(ped.PedIndex)
					REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
				ENDIF
				
				DELETE_PED(ped.PedIndex)
				
				IF DOES_BLIP_EXIST(ped.BlipIndex)
					REMOVE_BLIP(ped.BlipIndex)
				ENDIF
				
			ENDIF

		ENDIF
				
		ped.iTimer							= 0
		ped.iProgress 						= 0
		ped.iCleanupAttempts				= 0
		ped.bHasTask						= FALSE
		ped.bCanSeeTargetPed				= FALSE
		ped.eState							= PED_STATE_IDLE
		ped.eCleanupReason					= PED_CLEANUP_NO_CLEANUP
		
	ENDIF

ENDPROC

PROC CLEANUP_PED_GROUP(PED_STRUCT &array[], BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		CLEANUP_PED(array[i], bDelete, bDeathOnly, bKeepTask)
	ENDFOR

ENDPROC

FUNC PED_CLEANUP_REASONS GET_PED_CLEANUP_REASON(PED_STRUCT &ped)
	
	IF DOES_ENTITY_EXIST(ped.PedIndex)
	
		IF IS_ENTITY_DEAD(ped.PedIndex)
	
			ENTITY_INDEX SourceOfDeathEntity
						
			SourceOfDeathEntity = GET_PED_SOURCE_OF_DEATH(ped.PedIndex)
							
			IF DOES_ENTITY_EXIST(SourceOfDeathEntity)

				IF IS_ENTITY_A_PED(SourceOfDeathEntity)

					PED_INDEX SourceOfDeathPed
					
					SourceOfDeathPed = GET_PED_INDEX_FROM_ENTITY_INDEX(SourceOfDeathEntity)
					
					IF ( SourceOfDeathPed = PLAYER_PED_ID() )

						IF WAS_PED_KILLED_BY_STEALTH(ped.PedIndex)
						
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_PLAYER_STEALTH.")
							#ENDIF
							RETURN PED_CLEANUP_KILLED_BY_PLAYER_STEALTH
						
						ELIF WAS_PED_KILLED_BY_TAKEDOWN(ped.PedIndex)

							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_PLAYER_TAKEDOWN.")
							#ENDIF
							RETURN PED_CLEANUP_KILLED_BY_PLAYER_TAKEDOWN
						
						ELSE

							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_PLAYER_LETHAL.")
							#ENDIF
							RETURN PED_CLEANUP_KILLED_BY_PLAYER_LETHAL
							
						ENDIF

					ENDIF
				
				ELIF IS_ENTITY_A_VEHICLE(SourceOfDeathEntity)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_PLAYER_VEHICLE.")
					#ENDIF
					
					RETURN PED_CLEANUP_KILLED_BY_PLAYER_VEHICLE
						
				ELSE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_LOST.")
					#ENDIF
					
					RETURN PED_CLEANUP_LOST

				ENDIF
			
			ELSE
			
				IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(ped.PedIndex, WEAPONTYPE_STUNGUN)
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " was killed with weapon ", GET_WEAPON_NAME(WEAPONTYPE_STUNGUN), ".")
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_PLAYER_STEALTH.")
					#ENDIF
					RETURN PED_CLEANUP_KILLED_BY_PLAYER_STEALTH
					
				ENDIF
			

				ped.iCleanupAttempts++
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " source of death does not exist.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " had ", ped.iCleanupAttempts, " cleanup attempts .")
				#ENDIF

				IF ( ped.iCleanupAttempts = 10 )
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_LOST due to cleanup attempts running out.")
					#ENDIF
					
					RETURN PED_CLEANUP_LOST
				
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_NO_CLEANUP.")
	#ENDIF
	
	RETURN PED_CLEANUP_NO_CLEANUP
	
ENDFUNC

PROC CLEANUP_VEHICLE(VEH_STRUCT &vehicle, BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE)

	IF ( bDelete = FALSE )
	
		IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)
			
			IF ( bDeathOnly = TRUE )
			
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)	
				OR IS_ENTITY_DEAD(vehicle.VehicleIndex)	
					REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle.VehicleIndex)
				ENDIF
				
				IF DOES_BLIP_EXIST(vehicle.BlipIndex)
					REMOVE_BLIP(vehicle.BlipIndex)
				ENDIF
				
			ELIF ( bDeathOnly = FALSE )
			
				IF NOT IS_ENTITY_DEAD(vehicle.VehicleIndex)
					REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle.VehicleIndex)
				ENDIF
				
				IF DOES_BLIP_EXIST(vehicle.BlipIndex)
					REMOVE_BLIP(vehicle.BlipIndex)
				ENDIF
			
			ENDIF
		
		ENDIF
	
	ELIF ( bDelete = TRUE )

		IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)

			IF IS_ENTITY_A_MISSION_ENTITY(vehicle.VehicleIndex)
				IF IS_ENTITY_DEAD(vehicle.VehicleIndex)
				OR NOT IS_ENTITY_DEAD(vehicle.VehicleIndex)
					REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
					DELETE_VEHICLE(vehicle.VehicleIndex)
				ENDIF
			ENDIF
			
		ENDIF
		
		IF DOES_BLIP_EXIST(vehicle.BlipIndex)
			REMOVE_BLIP(vehicle.BlipIndex)
		ENDIF
		
		vehicle.iProgress 				= 0
		vehicle.bImmobilized			= FALSE
		vehicle.bAutoDriveActive		= FALSE
		vehicle.bLineOfSightToTarget	= FALSE
		
	ENDIF

ENDPROC

PROC CLEANUP_VEHICLE_GROUP(VEH_STRUCT &vsArray[], BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE)
	
	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(vsArray) - 1 )
		CLEANUP_VEHICLE(vsArray[i], bDelete, bDeathOnly)
	ENDFOR
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_CONTROL_OF_VEHICLE(VEHICLE_INDEX VehicleIndex)

	IF IS_VEHICLE_DRIVEABLE(VehicleIndex)

		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), VehicleIndex)
	
			INT iXValue, iYValue
			
			iXValue = ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_VEH_MOVE_LR) * 255.0)
			iYValue = ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_VEH_MOVE_UD) * 255.0)

			IF (iXValue < -50 OR iXValue > 50)
			OR (iYValue < -50 OR iYValue > 50)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_DUCK)
			
				RETURN TRUE

			ENDIF
		
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks if the specified mocap cutscene has loaded.
///    Removes any other loaded mocap cutscene and requests the specified cutscene until it is loaded.
/// PARAMS:
///    sSceneName - Mocap cutscene name to check for being loaded.
/// RETURNS:
///    TRUE if the specified cutscene has loaded, FALSE if otherwise.
FUNC BOOL HAS_REQUESTED_CUTSCENE_LOADED(STRING sSceneName)

	//check if the specified cutscene has loaded first
	IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE(sSceneName)
	
		RETURN TRUE
	
	ELSE
		
		//if the expected reuqested cutscene has not loaded, but there already is a loaded cutscene
		//then remove it and keep requesting specified mocap cutscene
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
			IF NOT HAS_THIS_CUTSCENE_LOADED(sSceneName)
			
				REMOVE_CUTSCENE()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": HAS_REQUESTED_CUTSCENE_LOADED() Removing cutscene from memory, because cutscene ", sSceneName, " was expected to be loaded.")
				#ENDIF
				
			ENDIF
			
		ENDIF
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading.")
		#ENDIF
		
		//if the cutscene is not loaded, keep requesting it
		REQUEST_CUTSCENE(sSceneName)
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, ".")
		#ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
			RETURN TRUE
	    ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC REMOVE_LABEL_ARRAY_SPACES()
	INT iArrayLength = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT (iArrayLength - 1) i
		IF iTriggeredTextHashes[i] = 0
			IF iTriggeredTextHashes[i+1] != 0
				iTriggeredTextHashes[i] = iTriggeredTextHashes[i+1]
				iTriggeredTextHashes[i+1] = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_LABEL_INDEX(INT iLabelHash)
	INT i = 0
	
	REPEAT iNumTextHashesStored i
		IF iTriggeredTextHashes[i] = iLabelHash
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF GET_LABEL_INDEX(iHash) != -1
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bTrigger)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF bTrigger
		IF NOT HAS_LABEL_BEEN_TRIGGERED(strLabel)
			INT iArraySize = COUNT_OF(iTriggeredTextHashes)
		
			IF iNumTextHashesStored < iArraySize
				iTriggeredTextHashes[iNumTextHashesStored] = iHash
				iNumTextHashesStored++
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", strLabel, " TRUE.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", iNumTextHashesStored, "/", COUNT_OF(iTriggeredTextHashes), ".")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Label array is full.")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		INT iIndex = GET_LABEL_INDEX(iHash)
		IF iIndex != -1
			iTriggeredTextHashes[iIndex] = 0
			REMOVE_LABEL_ARRAY_SPACES()
			iNumTextHashesStored--
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", strLabel, " FALSE.")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", iNumTextHashesStored, "/", COUNT_OF(iTriggeredTextHashes), ".")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iArraySize i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
	
	iNumTextHashesStored = 0
ENDPROC

PROC UPDATE_TRIGGERED_LABEL(TEXT_LABEL &sLabel)

	IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)					//if the label is not empty, which means not yet printed by locates header
	
		IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)				//if it was not set as triggered by script

			IF IS_THIS_PRINT_BEING_DISPLAYED(sLabel)		//if the label was displayed on screen

				SET_LABEL_AS_TRIGGERED(sLabel, TRUE)		//mark it as triggered

			ENDIF
			
		ELSE												//if the label was set as triggered by script,
															//which means it was printed by locates header
			IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sLabel)	//but is not currently being printed, cleared off screen or timed out
			
				sLabel = ""									//mark is as empty
			
			ENDIF
		
		ENDIF
		
		
	ENDIF
	
ENDPROC

PROC PRINT_GOD_TEXT_ADVANCED(STRING sLabel, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)
		PRINT(sLabel, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Printing god text ", sLabel, ".")
		#ENDIF
	ENDIF
ENDPROC

PROC PRINT_HELP_ADVANCED(STRING pTextLabel, BOOL bDisplayForever = FALSE, BOOL bPlaySound = TRUE, INT iDuration = DEFAULT_HELP_TEXT_TIME, BOOL bSetLabelAsTriggered = TRUE)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(pTextLabel)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, bDisplayForever, bPlaySound, iDuration)
	SET_LABEL_AS_TRIGGERED(pTextLabel, bSetLabelAsTriggered)
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Printing help text ", pTextLabel, ".")
	#ENDIF
ENDPROC

/// PURPOSE:
///    Check is the specified time has passed using specified timer.
/// PARAMS:
///    iTimeAmount - Time to check.
///    iTimer - Timer to use.
/// RETURNS:
///    True is the specified amount of time has passed for the specified timer.
FUNC BOOL HAS_TIME_PASSED(INT iTimeAmount, INT iTimer)

	INT iCurrentTime
	INT iTimeDifference
	
	iCurrentTime = GET_GAME_TIMER()
	
	iTimeDifference = iCurrentTime - iTimeAmount
	
	IF iTimeDifference > iTimer
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Returns the next mission stage after the specified mission stage.
/// PARAMS:
///    eStage - Mission stage to get the next mission stage.
/// RETURNS:
///    Mission stage that is next after the specified mission stage.
FUNC MISSION_STAGES GET_NEXT_MISSION_STAGE(MISSION_STAGES eStage)

	IF eStage = MISSION_STAGE_CUTSCENE_END
		RETURN MISSION_STAGE_CUTSCENE_END
	ENDIF
	
	MISSION_STAGES eNextStage
	
	INT iNextStage = ENUM_TO_INT(eStage) + 1
	
	eNextStage = INT_TO_ENUM(MISSION_STAGES, iNextStage)
	
	RETURN eNextStage

ENDFUNC

/// PURPOSE:
///    Checks for various mission fail conditions. Redirects mission flow to MISSION_STAGE_FAILED when one of fail conditions is true.
/// PARAMS:
///    eStage - Mission stage variable.
///    eFail - Mission fail reason.
PROC RUN_FAIL_CHECKS(MISSION_STAGES &eStage, MISSION_FAILS &eFail)

	IF 	eStage <> MISSION_STAGE_PASSED
	AND eStage <> MISSION_STAGE_FAILED
	
		IF NOT IS_CUTSCENE_PLAYING()
		
			//check mission car for being destroyed, undriveable, abandoned or lost
			IF DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
				
				IF ( FailFlags[MISSION_FAIL_CAR_UNDRIVABLE] = TRUE )
					IF IS_VEHICLE_PERMANENTLY_STUCK(vsPlayersCar.VehicleIndex)
						eFail = MISSION_FAIL_CAR_UNDRIVABLE
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_CAR_DEAD] = TRUE )
					IF NOT IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
					OR IS_ENTITY_ON_FIRE(vsPlayersCar.VehicleIndex)
						eFail = MISSION_FAIL_CAR_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_STUDIO_LEFT_BEHIND] = TRUE )
					IF ( bPlayerInsideFilmStudio = FALSE )
						IF fDistanceFromPlayerToTargetVehicle > ABANDON_STUDIO_FAIL_RANGE
							eFail 	= MISSION_FAIL_CAR_LEFT_BEHIND
							eStage 	= MISSION_STAGE_FAILED
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_CAR_LEFT_BEHIND] = TRUE )
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
							IF ( bPlayerInsideFilmStudio = FALSE )
								IF fDistanceFromPlayerToTargetVehicle > ABANDON_VEHICLE_FAIL_RANGE
									eFail 	= MISSION_FAIL_CAR_LEFT_BEHIND
									eStage 	= MISSION_STAGE_FAILED
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_SECURITY_AT_GARAGE] = TRUE )
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						IF ( bChaseInProgress = TRUE AND bChaseFinished = FALSE )
							IF IS_ENTITY_AT_COORD(vsPlayersCar.VehicleIndex, vInteriorPosition, << 20.0, 16.0, 3.0 >>)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInteriorPosition, << 20.0, 16.0, 3.0 >>)
								
									PED_INDEX PedIndex
										
									PedIndex = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(PLAYER_PED_ID(), RELGROUPHASH_SECURITY_GUARD, 0, FALSE)
								
									IF DOES_ENTITY_EXIST(PedIndex)
										IF NOT IS_ENTITY_DEAD(PedIndex)
											IF IS_ENTITY_AT_COORD(PedIndex, vInteriorPosition, << 20.0, 16.0, 3.0 >>)
												eFail 	= MISSION_FAIL_SECURITY_AT_GARAGE
												eStage 	= MISSION_STAGE_FAILED
											ENDIF
										ENDIF
									ENDIF

								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_ACTRESS_AT_GARAGE] = TRUE )
					IF DOES_ENTITY_EXIST(psActress.PedIndex)
						IF NOT IS_PED_INJURED(psActress.PedIndex)
							IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
								IF IS_PED_IN_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
									IF IS_ENTITY_AT_COORD(psActress.PedIndex, vInteriorPosition, << 20.0, 16.0, 3.0 >>)
										eFail 	= MISSION_FAIL_ACTRESS_AT_GARAGE
										eStage 	= MISSION_STAGE_FAILED
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_COPS_AT_GARAGE] = TRUE )
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						IF IS_ENTITY_AT_COORD(vsPlayersCar.VehicleIndex, vInteriorPosition, << 20.0, 16.0, 3.0 >>)
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInteriorPosition, << 20.0, 16.0, 3.0 >>)
								IF IS_PLAYER_PLAYING(PLAYER_ID())
									IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
									
										PED_INDEX PedIndex
										
										PedIndex = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(PLAYER_PED_ID(), RELGROUPHASH_COP, 0, FALSE)
									
										IF DOES_ENTITY_EXIST(PedIndex)
											IF NOT IS_ENTITY_DEAD(PedIndex)
												IF IS_ENTITY_AT_COORD(PedIndex, vInteriorPosition, << 20.0, 16.0, 3.0 >>)
													eFail 	= MISSION_FAIL_COPS_AT_GARAGE
													eStage 	= MISSION_STAGE_FAILED
												ENDIF
											ENDIF
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_DEVIN_DEAD] = TRUE )
					IF DOES_ENTITY_EXIST(psDevin.PedIndex)
						IF IS_PED_INJURED(psDevin.PedIndex)
						OR IS_ENTITY_DEAD(psDevin.PedIndex)
							eFail 	= MISSION_FAIL_DEVIN_DEAD
							eStage 	= MISSION_STAGE_FAILED
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_DEVIN_ATTACKED] = TRUE )
					IF DOES_ENTITY_EXIST(psDevin.PedIndex)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psDevin.PedIndex, PLAYER_PED_ID(), TRUE)
						OR ( IS_ENTITY_TOUCHING_ENTITY(psDevin.PedIndex, PLAYER_PED_ID()) AND IS_PED_RAGDOLL(psDevin.PedIndex))
							eFail 	= MISSION_FAIL_DEVIN_ATTACKED
							eStage 	= MISSION_STAGE_FAILED
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Sets mission as failed and provides mission flow with reason to print on screen.
/// PARAMS:
///    eFailReason - One of MISSION_FAILS values.
PROC SET_MISSION_FAILED_WITH_REASON(MISSION_FAILS &eFailReason)   

	SWITCH eFailReason
	
		CASE MISSION_FAIL_CAR_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_GENDEST")	//Mission car is destroyed
		BREAK
		CASE MISSION_FAIL_CAR_UNDRIVABLE
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CAR4_CUND")	//Mission car is undrivable or stuck
		BREAK
		CASE MISSION_FAIL_CAR_LEFT_BEHIND
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CAR4_CLEFT")	//Mission car is left behind
		BREAK
		CASE MISSION_FAIL_ACTRESS_AT_GARAGE
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CAR4_GGAR")	//Player drove to the garage with the actress in the car
		BREAK
		CASE MISSION_FAIL_SECURITY_AT_GARAGE
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CAR4_SGAR")	//Player drove to the garage tailed by security
		BREAK
		CASE MISSION_FAIL_COPS_AT_GARAGE
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CAR4_CGAR")	//Player drove to the garage tailed by cops
		BREAK
		CASE MISSION_FAIL_DEVIN_DEAD
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CAR4_DDEAD")
		BREAK
		CASE MISSION_FAIL_DEVIN_ATTACKED
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CAR4_DATTA")
		BREAK
		CASE MISSION_FAIL_EMPTY
		CASE MISSION_FAIL_FORCE_FAIL
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CAR4_FAIL")	//Debug force fail
		BREAK
		
	ENDSWITCH
	
ENDPROC

//|======================= ASSET REQUESTING PROCEDURES & FUNCTIONS =======================|

/// PURPOSE:
///    Adds a model to model request array.
/// PARAMS:
///    ModelName - Model name being requested.
///    array - MODEL_NAMES array to add the requested model name to.
///    iArrayLength - Maximum length of the array.
///    iModelsAlreadyRequested - Number of models already requested in this array.
PROC ADD_MODEL_REQUEST_TO_ARRAY(MODEL_NAMES ModelName, MODEL_NAMES &array[], INT iArrayLength, INT &iModelsAlreadyRequested)

	INT i = 0
	
	BOOL bModelAlreadyRequested = FALSE
	
	IF ( iModelsAlreadyRequested >= 0 )
	
		IF ( iModelsAlreadyRequested <= ( iArrayLength - 1 ) )
		
			//loop through the array (from 0 to number of models already requested)
			//to see if the array already contains the model name being requested
			//modify the boolean flag if the model was found in array, otherwise leave the flag unchanged
			FOR i = 0 TO ( iModelsAlreadyRequested )
			
				IF ( array[i] = ModelName )
				
					bModelAlreadyRequested = TRUE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " is already in model array.")
					#ENDIF
					
				ENDIF
				
			ENDFOR
			
			IF ( bModelAlreadyRequested = FALSE )
			
				REQUEST_MODEL(ModelName)
				
				//iModelsAlreadyRequested will be the model array index of the model being requested
				array[iModelsAlreadyRequested] = ModelName
				
				iModelsAlreadyRequested++
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " and added to model array.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Number of models requested: ", iModelsAlreadyRequested, ".")
				#ENDIF
				
			ENDIF
		
		ELSE
		
			SCRIPT_ASSERT("Number of requested models is greater than the length of the model array.")
		
		ENDIF
	
	ELSE
	
		SCRIPT_ASSERT("Number of requested models is less than 0.")
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if all requested models from model array are loaded into memory.
/// PARAMS:
///    array - MODEL_NAMES array holding the models.
///    iModelsAlreadyRequested - Number of already requested models. Should be greated than 0, since 0 means no models have been requested.
/// RETURNS:
///    TRUE if all models from model array are loaded into memory. FALSE if otherwise.
FUNC BOOL ARE_REQUESTED_MODELS_LOADED(MODEL_NAMES &array[], INT &iModelsAlreadyRequested) 

	INT i = 0
	
	IF ( iModelsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iModelsAlreadyRequested - 1 )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model in array[", i, "]: ", GET_MODEL_NAME_FOR_DEBUG(array[i]), ".")
			#ENDIF
		
			IF NOT HAS_MODEL_LOADED(array[i])
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), " is loading.")
				#ENDIF
				
				//if the model is not loaded keep requesting it
				REQUEST_MODEL(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), ".")
				#ENDIF
				
				RETURN FALSE
				
			ENDIF
			
		ENDFOR
		
	ENDIF	
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Models from model array loaded. Number of models loaded: ", iModelsAlreadyRequested, ".")
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleans model array from requested models.
/// PARAMS:
///    array - MODEL_NAMES array.
///    iModelsAlreadyRequested - Number of already requested models. Should be greated than 0, since 0 means no models have been requested.
PROC CLEANUP_MODEL_ARRAY(MODEL_NAMES &array[], INT &iModelsAlreadyRequested)
	
	INT i = 0
	
	IF ( iModelsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iModelsAlreadyRequested - 1 )
		
			IF array[i] <> DUMMY_MODEL_FOR_SCRIPT
			
				SET_MODEL_AS_NO_LONGER_NEEDED(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), " cleaned up from model array by setting it as no longer needed.")
				#ENDIF
				
				array[i] = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
			
		ENDFOR
		
	ENDIF
	
	iModelsAlreadyRequested = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model array cleaned up. Number of requested models set to 0.")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Adds animation dictionary to animation request array.
/// PARAMS:
///    sAnimName - Name of the animation dictionary to request.
///    array - STRING array containing requested animations.
///    iArrayLength - Maximum length of the array
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
PROC ADD_ANIMATION_REQUEST_TO_ARRAY(STRING sAnimName, STRING &array[], INT iArrayLength, INT &iAnimationsAlreadyRequested)

	INT i = 0
	
	BOOL bAnimationRequested
	
	IF ( iAnimationsAlreadyRequested >= 0 )
	
		IF ( iAnimationsAlreadyRequested <= (iArrayLength - 1 ) )
	
		    FOR i = 0 TO ( iAnimationsAlreadyRequested )
			
				IF 	IS_STRING_NULL_OR_EMPTY(sAnimName)
				AND	IS_STRING_NULL_OR_EMPTY(array[i])
				AND	ARE_STRINGS_EQUAL(sAnimName, array[i])
				
				    bAnimationRequested = TRUE
					
				    #IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested animation ", sAnimName, " is already in animation array.")
					#ENDIF
					
				ENDIF
				
		    ENDFOR
			
		    IF ( bAnimationRequested = FALSE )
			
				REQUEST_ANIM_DICT(sAnimName)
			
				array[iAnimationsAlreadyRequested] = sAnimName

				iAnimationsAlreadyRequested++

				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested animation ", sAnimName, " and added to animation array.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Number of animations requested: ", iAnimationsAlreadyRequested, ".")
				#ENDIF
				  
		    ENDIF
		
		ELSE
		
			SCRIPT_ASSERT("Number of requested animations is greater than the length of the animation array.")
			
		ENDIF
	
	ELSE
	
		SCRIPT_ASSERT("Number of requested animations is less than 0.")
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if all requested animations are loaded into memory.
/// PARAMS:
///    array - STRING array containing requested animations.
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
/// RETURNS:
///    TRUE if all requested animations from animation array are loaded into memory. FALSE if otherwise.
FUNC BOOL ARE_REQUESTED_ANIMATIONS_LOADED(STRING &array[], INT &iAnimationsAlreadyRequested) 
	
	INT i = 0
	
	IF ( iAnimationsAlreadyRequested > 0 )
	
	    FOR i = 0 TO ( iAnimationsAlreadyRequested - 1 )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary in array[", i, "]: ", array[i], ".")
			#ENDIF
		
			IF NOT HAS_ANIM_DICT_LOADED(array[i])
			
			    #IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary ", array[i], " is loading.")
				#ENDIF
				
				//if the animation dictionary is not loaded keep requesting it
				REQUEST_ANIM_DICT(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting animation dictionary ", array[i], ".")
				#ENDIF
				
				
			    RETURN FALSE
				
			ENDIF
			
	    ENDFOR
		
	ENDIF   
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animations from animation array loaded. Number of animations loaded: ", iAnimationsAlreadyRequested, ".")
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleans animation array from requested animations.
/// PARAMS:
///    array - STRING array of animations to clean.
///    iAnimationsAlreadyRequested - Number of animations already requested in this array.
PROC CLEANUP_ANIMATION_ARRAY(STRING &array[], INT &iAnimationsAlreadyRequested)
	
	INT i = 0
	
	STRING sNull = NULL
	
	IF ( iAnimationsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iAnimationsAlreadyRequested - 1 )
			
			//brute force remove animation dictionary from memory
		
			REMOVE_ANIM_DICT(array[i])
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation dictionary ", array[i], " removed from memory and cleaned up from animation array.")
			#ENDIF
			
			array[i] = sNull

		ENDFOR
	ENDIF
	
	iAnimationsAlreadyRequested = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Animation array cleaned up. Number of requested animations set to 0.")
	#ENDIF
	
ENDPROC

//|===================== END ASSET REQUESTING PROCEDURES & FUNCTIONS =====================|

//|======================= DEBUG VARIABLES, PROCEDURES & FUNCTIONS =======================|

#IF IS_DEBUG_BUILD

	//mission skip menu and widget variables
	MissionStageMenuTextStruct	SkipMenu[7]
	INT 	iReturnStage = 0
	BOOL	bStageResetFlag
	BOOL	bPrintDebugOutput
	BOOL	bDrawDebugLinesAndSpheres
	BOOL	bDrawDebugStates
	BOOL	bDrawDebugSpawnpoints
	BOOL	bDrawDebugVehicleDamageInfo	

	//debug widgets variables
	WIDGET_GROUP_ID CarSteal4Widgets

	/// PURPOSE:
	///    Returns string name of the specified mission stage.
	/// PARAMS:
	///    eStage - Mission stage to get string name for.
	/// RETURNS:
	///    String name of the specified mission stage.
	FUNC STRING GET_MISSION_STAGE_NAME_FOR_DEBUG(MISSION_STAGES eStage)
	
		SWITCH eStage

			CASE MISSION_STAGE_GET_TO_ACTOR
				RETURN "MISSION_STAGE_GET_TO_ACTOR"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_TRAILER
				RETURN "MISSION_STAGE_CUTSCENE_TRAILER"
			BREAK
			CASE MISSION_STAGE_GET_TO_CAR
				RETURN "MISSION_STAGE_GET_TO_CAR"
			BREAK
			CASE MISSION_STAGE_ESCAPE_FILM_SET
				RETURN "MISSION_STAGE_ESCAPE_FILM_SET"
			BREAK
			CASE MISSION_STAGE_DELIVER_CAR
				RETURN "MISSION_STAGE_DELIVER_CAR"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_END
				RETURN "MISSION_STAGE_CUTSCENE_END"
			BREAK
			
		ENDSWITCH
		
		RETURN "INCORRECT_MISSION_STAGE"
	
	ENDFUNC
	
	/// PURPOSE:
	///    Creates debug widgets for the mission.
	PROC CREATE_DEBUG_WIDGETS()
	
		CarSteal4Widgets = START_WIDGET_GROUP("Mission: Car Steal 3")
		
			ADD_WIDGET_FLOAT_SLIDER("BLIP_SLIDE_AMOUNT", BLIP_SLIDE_AMOUNT, 0.0, 256.0, 1.0)
		
			ADD_WIDGET_BOOL("Print debug output", bPrintDebugOutput)
			
			START_WIDGET_GROUP("Ejector Seat")
				
				ADD_WIDGET_FLOAT_SLIDER("fEjectScenePhase", fEjectScenePhase, 0.0, 1.0, 0.05)
				ADD_WIDGET_VECTOR_SLIDER("vEjectorSeatSecondaryForce", vEjectorSeatSecondaryForce, -100.0, 100.0, 1.0)
				ADD_WIDGET_VECTOR_SLIDER("vEjectorSeatPtfxOffset", vEjectorSeatPtfxOffset, -10.0, 10.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vEjectorSeatPtfxRotation", vEjectorSeatPtfxRotation, -360.0, 360.0, 1.0)
				
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
		SET_LOCATES_HEADER_WIDGET_GROUP(CarSteal4Widgets)	
	
	ENDPROC
	
	/// PURPOSE:
	///    Deletes debug widgets for the mission.
	PROC DELETE_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(CarSteal4Widgets)
			DELETE_WIDGET_GROUP(CarSteal4Widgets)
		ENDIF
	ENDPROC
	
	PROC RUN_DEBUG_WIDGETS()
	
		IF ( bDrawDebugSpawnpoints = TRUE )
		
			INT i
				
			REPEAT COUNT_OF(SecuritySpawnPoints) i
				DRAW_DEBUG_SPHERE(SecuritySpawnPoints[i].vPosition, 0.15, 255, 0, 0, 255)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(SecuritySpawnPoints[i].vPosition, GET_STRING_FROM_INT(i), 0.15)
			ENDREPEAT
		
		ENDIF	
	
	ENDPROC
	
	/// PURPOSE:
	///    Performs a screen fade in with a wait for a specified time.
	/// PARAMS:
	///    iTime - Duration of the fade in miliseconds.
	PROC DO_SAFE_SCREEN_FADE_IN(INT iTime)

		IF NOT IS_SCREEN_FADED_IN()
		
			DO_SCREEN_FADE_IN(iTime)
			
			WHILE NOT IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
			
		ENDIF

	ENDPROC

	/// PURPOSE:
	///    Performs a screen fade out with a wait for a specified time.
	/// PARAMS:
	///    iTime - Duration of the fade in miliseconds.
	PROC DO_SAFE_SCREEN_FADE_OUT(INT iTime)

		IF NOT IS_SCREEN_FADED_OUT()
		
			DO_SCREEN_FADE_OUT(iTime)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT(0)
			ENDWHILE
			
		ENDIF

	ENDPROC

	/// PURPOSE:
	///    Stops an active mocap cutscene and waits until it has finished.
	PROC DO_SAFE_STOP_CUTSCENE()

		IF IS_CUTSCENE_PLAYING()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			STOP_CUTSCENE()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)	//needs to be called to make sure all scripted systems work correctly when cutscene is skipped		
		ENDIF

		WHILE NOT HAS_CUTSCENE_FINISHED()
			WAIT(0)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for the cutscene to finish.")
			#ENDIF
		ENDWHILE
		
	ENDPROC
	
	PROC RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()

		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting mission stage cleanup.")
		#ENDIF

		KILL_ANY_CONVERSATION()
		HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
		
		REMOVE_CUTSCENE()
		STOP_AUDIO_SCENES()
		TRIGGER_MUSIC_EVENT("CAR3_MISSION_FAIL")
		
		//cleanup mission peds
		CLEANUP_PED(psActor, TRUE)
		CLEANUP_PED(psDevin, TRUE)
		CLEANUP_PED(psToilet, TRUE)
		CLEANUP_PED(psActress, TRUE)
		CLEANUP_PED(psAssistant, TRUE)
		CLEANUP_PED_GROUP(psAliens, TRUE)
		CLEANUP_PED_GROUP(psFilmCrew, TRUE)
		CLEANUP_PED_GROUP(psMeltdownCrew, TRUE)
		
		CLEANUP_PED_GROUP(psSetSecurity, TRUE)
		CLEANUP_PED_GROUP(psCarSecurity, TRUE)
		CLEANUP_PED_GROUP(psActorSecurity, TRUE)
		CLEANUP_PED_GROUP(psMeltdownSecurity, TRUE)
		CLEANUP_PED_GROUP(psNorthGateSecurity, TRUE)
		CLEANUP_PED_GROUP(psSouthGateSecurity, TRUE)
		
		IF DOES_ENTITY_EXIST(osPropToilet.ObjectIndex)
			REMOVE_MODEL_HIDE(osPropToilet.vPosition, 1.0, osPropToilet.ModelName)
		ENDIF
				
		//cleanup mission objects
		CLEANUP_OBJECT(osPropLamp.ObjectIndex, TRUE)
		CLEANUP_OBJECT(osPropChair.ObjectIndex, TRUE)
		CLEANUP_OBJECT(osTrailerDoor.ObjectIndex, TRUE)
		CLEANUP_OBJECT(osPropToilet.ObjectIndex, TRUE)
		CLEANUP_OBJECT(osPropClipboard.ObjectIndex, TRUE)
		CLEANUP_OBJECT(osPropCamera.ObjectIndex, TRUE)
		CLEANUP_OBJECT(osPropMicrophone.ObjectIndex, TRUE)
		CLEANUP_OBJECT(osPropGreenScreen.ObjectIndex, TRUE, FALSE)
		
		//cleanup mission vehicles
		CLEANUP_VEHICLE(vsPlayersCar, TRUE)
		CLEANUP_VEHICLE_GROUP(vsSecurityCars, TRUE)
		
		//cleanup mission blips		
		IF DOES_BLIP_EXIST(DestinationBlip)
			REMOVE_BLIP(DestinationBlip)
		ENDIF
		
		CLEANUP_TRACKED_POINT(iCarTrackedPoint)
		CLEANUP_TRACKED_POINT(iActorTrackedPoint)
		
		//cleanup mission text labels
		CLEAR_TRIGGERED_LABELS()
		
		//cleanup animation dictionaries
		REMOVE_ANIM_DICT("dead")
		REMOVE_ANIM_DICT("misscarsteal4@actor")
		REMOVE_ANIM_DICT("misscarsteal4@aliens")
		REMOVE_ANIM_DICT("misscarsteal4@meltdown")
		REMOVE_ANIM_DICT("missarmenian3_tryopendoor")
		REMOVE_ANIM_DICT("misscarsteal4leadinoutcar_4_ext")
		
		//cleanup jb700 spikes and reset data structs
		RESET_JB700_GADGET_DATA(CarGadgetData)
		CLEANUP_JB700_SPIKES(CarGadgetData, TRUE)
		
		UNLOCK_STUDIO_GATES()
		
		//cleanup locates header data
		CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
		
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		
		REMOVE_PTFX_ASSET()
		
		DESTROY_ALL_CAMS()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission stage cleanup.")
		#ENDIF
		
	ENDPROC

	/// PURPOSE:
	///    Creates debug mission stage menu.
	PROC CREATE_DEBUG_MISSION_STAGE_MENU()

		SkipMenu[0].sTxtLabel		= "GET TO ACTOR"
		SkipMenu[1].sTxtLabel		= "CUT - TRAILER - CAR_4_MCS_1"
		SkipMenu[2].sTxtLabel		= "GET TO CAR"
		SkipMenu[3].sTxtLabel		= "ESCAPE FILM SET"
		SkipMenu[4].sTxtLabel		= "DELIVER CAR"
		SkipMenu[5].sTxtLabel		= "CUT - END - CAR_4_EXT"
		SkipMenu[6].sTxtLabel		= "DEBUG EJECTOR SEAT TEST"
		
	ENDPROC

	/// PURPOSE:
	///    Checks if any of the debug pass or fail keys has been pressed to force mission end.
	/// PARAMS:
	///    eStage - Mission stage variable
	///    eFailReason - Mission fail variable.
	PROC RUN_DEBUG_PASS_AND_FAIL_CHECK(MISSION_STAGES &eStage, MISSION_FAILS &eFailReason)
	
		IF IS_KEYBOARD_KEY_PRESSED (KEY_S)
			eStage = MISSION_STAGE_PASSED
	    ENDIF
		
	    IF IS_KEYBOARD_KEY_PRESSED (KEY_F)
			eStage = MISSION_STAGE_FAILED
			eFailReason = MISSION_FAIL_FORCE_FAIL
	    ENDIF
	
	ENDPROC
	
	/// PURPOSE:
	///    Controls running the debug mission stage menu.
	/// PARAMS:
	///    iSelectedStage - Variable that stores int value returned from the debug mission stage menu.
	///    eStage - Mission stage variable.
	///    bSkipActive - Skip flag.
	PROC RUN_DEBUG_MISSION_STAGE_MENU(INT &iSelectedStage, MISSION_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenu, iSelectedStage, ENUM_TO_INT(eStage), FALSE, "CAR STEAL 3")
		
			DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			
			DO_SAFE_STOP_CUTSCENE()
			
			CLEAR_PRINTS()
			
			CLEAR_HELP()
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
			
			eStage = INT_TO_ENUM(MISSION_STAGES, iSelectedStage)
			
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via mission stage menu.")
			
			bResetStage = FALSE
			
			bSkipActive = TRUE
			
			RESET_MISSION_FLAGS()
		
		ENDIF
	
	ENDPROC
	
	PROC RUN_DEBUG_SKIPS(MISSION_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		INT iCurrentStage = ENUM_TO_INT(eStage)
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
			IF eStage <> MISSION_STAGE_CUTSCENE_END
		
				DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				
				DO_SAFE_STOP_CUTSCENE()
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
				
				iCurrentStage++
				
				eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)
								
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via J skip.")
				
				bResetStage = FALSE
				
				bSkipActive = TRUE
				
				RESET_MISSION_FLAGS()
				
			ENDIF
			
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)

				DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				
				DO_SAFE_STOP_CUTSCENE()
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MISSION_STAGE_CLEANUP_FOR_DEBUG()
				
				IF ( eStage <> MISSION_STAGE_GET_TO_ACTOR )
				
					IF ( bResetStage = FALSE )
				
						iCurrentStage--
						
						eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)			
						
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via P skip.")
					
					ELIF ( bResetStage = TRUE )
					
						eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)			
						
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " reset via P skip.")
					
					ENDIF
					
				ELIF ( eStage = MISSION_STAGE_GET_TO_ACTOR )
				
					eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)			
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " reset via P skip.")
				
				ENDIF
				
				bResetStage = FALSE
				
				bSkipActive = TRUE
				
				RESET_MISSION_FLAGS()
			
		ENDIF
	
	ENDPROC
	
	FUNC STRING GET_PED_STATE_NAME_FOR_DEBUG(PED_STATES eState)
	
		SWITCH eState
		
			CASE PED_STATE_IDLE
				RETURN "PED_STATE_IDLE"
			BREAK
			CASE PED_STATE_DEAD
				RETURN "PED_STATE_DEAD"
			BREAK
			CASE PED_STATE_ARGUING
				RETURN "PED_STATE_ARGUING"
			BREAK
			CASE PED_STATE_MOVING_TO_LOCATION
				RETURN "PED_STATE_MOVING_TO_LOCATION"
			BREAK
			CASE PED_STATE_STANDING_AT_LOCATION
				RETURN "PED_STATE_STANDING_AT_LOCATION"
			BREAK
			CASE PED_STATE_PLAYING_ANIM_1
				RETURN "PED_STATE_PLAYING_ANIM_1"
			BREAK
			CASE PED_STATE_PLAYING_ANIM_2
				RETURN "PED_STATE_PLAYING_ANIM_2"
			BREAK
			CASE PED_STATE_PLAYING_ANIM_3
				RETURN "PED_STATE_PLAYING_ANIM_3"
			BREAK
			CASE PED_STATE_PLAYING_ANIM_4
				RETURN "PED_STATE_PLAYING_ANIM_4"
			BREAK
			CASE PED_STATE_STARTING_SCENARIO
				RETURN "PED_STATE_STARTING_SCENARIO"
			BREAK
			CASE PED_STATE_STARTING_SCENARIO_WARP
				RETURN "PED_STATE_STARTING_SCENARIO_WARP"
			BREAK
			CASE PED_STATE_USING_SCENARIO
				RETURN "PED_STATE_USING_SCENARIO"
			BREAK
			CASE PED_STATE_BLOCKING_PLAYER
				RETURN "PED_STATE_BLOCKING_PLAYER"
			BREAK
			CASE PED_STATE_JACKING_PLAYER
				RETURN "PED_STATE_JACKING_PLAYER"
			BREAK
			CASE PED_STATE_WATCHING_PLAYER
				RETURN "PED_STATE_WATCHING_PLAYER"
			BREAK
			CASE PED_STATE_STUNNED
				RETURN "PED_STATE_STUNNED"
			BREAK
			CASE PED_STATE_FLEEING
				RETURN "PED_STATE_FLEEING"
			BREAK
			CASE PED_STATE_SITTING_IN_CAR
				RETURN "PED_STATE_SITTING_IN_CAR"
			BREAK
			CASE PED_STATE_BEING_EJECTED
				RETURN "PED_STATE_BEING_EJECTED"
			BREAK
			CASE PED_STATE_BEING_STEALTH_KILLED
				RETURN "PED_STATE_BEING_STEALTH_KILLED"
			BREAK
			CASE PED_STATE_IMMOBILIZED
				RETURN "PED_STATE_IMMOBILIZED"
			BREAK
			CASE PED_STATE_INTIMIDATION
				RETURN "PED_STATE_INTIMIDATION"
			BREAK
			CASE PED_STATE_COMBAT_AIMING
				RETURN "PED_STATE_COMBAT_AIMING"
			BREAK
			CASE PED_STATE_COMBAT_AIMING_PED
				RETURN "PED_STATE_COMBAT_AIMING_PED"
			BREAK
			CASE PED_STATE_COMBAT_ON_FOOT
				RETURN "PED_STATE_COMBAT_ON_FOOT"
			BREAK
			CASE PED_STATE_COMBAT_DEFENSIVE
				RETURN "PED_STATE_COMBAT_DEFENSIVE"
			BREAK
			CASE PED_STATE_ENTERING_VEHICLE
				RETURN "PED_STATE_ENTERING_VEHICLE"
			BREAK
			CASE PED_STATE_COMBAT_FROM_CAR_SHOOTING
				RETURN "PED_STATE_COMBAT_FROM_CAR_SHOOTING"
			BREAK
			CASE PED_STATE_COMBAT_FROM_CAR_NO_SHOOTING
				RETURN "PED_STATE_COMBAT_FROM_CAR_NO_SHOOTING"
			BREAK
			
			DEFAULT
				RETURN "INVALID_PED_STATE"
			BREAK
			
		ENDSWITCH
		
		RETURN "INVALID_PED_STATE"
	
	ENDFUNC
	
	PROC RUN_DEBUG_INFORMATION_DISPLAY()
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			IF ( bDrawDebugLinesAndSpheres = FALSE)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				bDrawDebugLinesAndSpheres = TRUE
			ELSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
				bDrawDebugLinesAndSpheres = FALSE
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
			IF ( bDrawDebugStates = FALSE)
				bDrawDebugStates = TRUE
			ELSE
				bDrawDebugStates = FALSE
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
			IF ( bDrawDebugSpawnpoints = FALSE)
				bDrawDebugSpawnpoints = TRUE
			ELSE
				bDrawDebugSpawnpoints = FALSE
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD7)
			IF ( bDrawDebugVehicleDamageInfo = FALSE)
				bDrawDebugVehicleDamageInfo = TRUE
			ELSE
				bDrawDebugVehicleDamageInfo = FALSE
			ENDIF
		ENDIF
	
	ENDPROC
	
	PROC PRINT_GLOBAL_BIT_SET()
	
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": ******************************** GLOBAL BIT SET *********************************")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": PLAYER_GOT_TUXEDO: ", IS_BIT_SET(g_iCarSteal3AgentBitSet, PLAYER_GOT_TUXEDO), ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": ACTOR_KILLED: ", IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTOR_KILLED), ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": ACTOR_STEALTH_KILLED: ", IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTOR_STEALTH_KILLED), ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": ACTOR_IGNORED: ", IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTOR_IGNORED), ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": ACTRESS_FLED_CAR: ", IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTRESS_FLED_CAR), ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": **********************************************************************************")

	ENDPROC
	
#ENDIF

//|===================== END DEBUG VARIABLES, PROCEDURES & FUNCTIONS =====================|

FUNC BOOL CAN_GET_CLOSEST_NTH_VEHICLE_NODE_TO_ENTITY_BETWEEN_ANGLES(ENTITY_INDEX EntityIndex, VECTOR &vCurrentClosestNodePosition,
																	FLOAT &fOutHeading, INT iNodeNumber, FLOAT fMinAngle,
																	FLOAT fMaxAngle, NODE_FLAGS eNodeFlags = NF_NONE)

	///
	///     Angles
	///     use 0 - 90 for vehicle nodes in front of the entity
	///     use 90 - 180 for vehicle nodes behind the player
		
	IF DOES_ENTITY_EXIST(EntityIndex)
	
		IF NOT IS_ENTITY_DEAD(EntityIndex)

			VECTOR 	vEntityPosition
			VECTOR 	vEntityForwardVector
			VECTOR	vVectorFromNodeToEntity
			VECTOR 	vClosestNodePosition
			FLOAT	fClosestNodeHeading
			FLOAT	fAngle
			INT		iTotalLanes
			
			vEntityPosition 		= GET_ENTITY_COORDS(EntityIndex)
			vEntityForwardVector 	= GET_ENTITY_FORWARD_VECTOR(EntityIndex)
			
			IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vEntityPosition, iNodeNumber, vClosestNodePosition, fClosestNodeHeading, iTotalLanes, eNodeFlags)
			
				vVectorFromNodeToEntity = vClosestNodePosition - vEntityPosition
				
				fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vVectorFromNodeToEntity.X, vVectorFromNodeToEntity.Y, vEntityForwardVector.X, vEntityForwardVector.Y)				

				IF ( fAngle > fMinAngle AND fAngle < fMaxAngle )
					
					//IF NOT ARE_VECTORS_EQUAL(vCurrentClosestNodePosition, vClosestNodePosition) //not sure why this was here
					
						vCurrentClosestNodePosition = vClosestNodePosition
						
						fOutHeading = fClosestNodeHeading
												
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), "************* CAN_GET_CLOSEST_NTH_VEHICLE_NODE_TO_VEHICLE_BETWEEN_ANGLES **************")
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Closest vehicle node found at ", vClosestNodePosition, " and heading ", fClosestNodeHeading , ".")
							PRINTLN(GET_THIS_SCRIPT_NAME(), "***************************************************************************************")
							DRAW_DEBUG_SPHERE(vClosestNodePosition, 1.0)
						#ENDIF

						RETURN TRUE

					//ENDIF
					
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF ( bDrawDebugStates = TRUE )
						DRAW_DEBUG_SPHERE(vClosestNodePosition, 0.5, 255, 0, 0, 64)
						DRAW_DEBUG_SPHERE(vCurrentClosestNodePosition, 1.0, 0, 0, 255, 64)
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(EntityIndex, GET_STRING_FROM_FLOAT(fAngle), 1.5)
					ENDIF
				#ENDIF
				
			ENDIF

		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC STORE_ENTITY_POSITION_AND_HEADING(ENTITY_INDEX EntityIndex, VECTOR &vPosition, FLOAT &fHeading)

	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
			vPosition 	= GET_ENTITY_COORDS(EntityIndex)
			fHeading 	= GET_ENTITY_HEADING(EntityIndex)
		ENDIF
	ENDIF

ENDPROC

PROC RESET_GLOBAL_BIT_SET()
	
	g_iCarSteal3AgentBitSet = 0
	
	#IF IS_DEBUG_BUILD
		PRINT_GLOBAL_BIT_SET()
	#ENDIF
	
ENDPROC

PROC SET_GLOBAL_BIT_SET(INT iBit)
	
	SET_BIT(g_iCarSteal3AgentBitSet, iBit)
	
	#IF IS_DEBUG_BUILD
		PRINT_GLOBAL_BIT_SET()
	#ENDIF

ENDPROC

PROC CLEAR_GLOBAL_BIT_SET(INT iBit)

	CLEAR_BIT(g_iCarSteal3AgentBitSet, iBit)

	#IF IS_DEBUG_BUILD
		PRINT_GLOBAL_BIT_SET()
	#ENDIF

ENDPROC

PROC UPDATE_PLAYER_KILL_COUNTERS(PED_CLEANUP_REASONS eCleanupReason, INT &iLethalKills, INT &iStealthKills, INT &iTakedownKills)

	SWITCH eCleanupReason
		CASE PED_CLEANUP_KILLED_BY_PLAYER_LETHAL
			iLethalKills++
		BREAK
		CASE PED_CLEANUP_KILLED_BY_PLAYER_STEALTH
			iStealthKills++
		BREAK
		CASE PED_CLEANUP_KILLED_BY_PLAYER_TAKEDOWN
			iTakedownKills++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": *********************** UPDATE_PLAYER_AND_BUDDY_KILL_COUNTERS *************************")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player lethal kills: ", iLethalKills, ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player stealth kills: ", iStealthKills, ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player takedown kills: ", iTakedownKills, ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": ***************************************************************************************")
	#ENDIF	

ENDPROC

PROC UPDATE_JB700_SPIKE_HIT_COUNTERS(JB700_GADGET_DATA sGadgetData, VEH_STRUCT &array[])

	IF sGadgetData.bSpikesHit = TRUE
	
		IF ( iTotalSpikeHits != sGadgetData.iSpikesHits )
	
			INT i = 0

			REPEAT COUNT_OF(array) i
			
				IF ( sGadgetData.SpikesData[sGadgetData.iSpikeHit].VehicleIndex = array[i].VehicleIndex )
				
					array[i].bImmobilized = TRUE
															
					iSecurityCarsImmobilized++
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": iSecurityCarsImmobilized: ", iSecurityCarsImmobilized, ".")
					#ENDIF

				ENDIF
			
			ENDREPEAT
			
			iTotalSpikeHits = sGadgetData.iSpikesHits
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": iTotalSpikeHits: ", iTotalSpikeHits, ".")
			#ENDIF
	
		ENDIF
	
	ENDIF

ENDPROC

PROC UPDATE_JB700_PED_DRIVE_TASK(JB700_GADGET_DATA sGadgetData, PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex, BOOL &bAutoDrive, INT &iTimer)

	IF ( bAutoDrive = FALSE )
	
		IF ( sGadgetData.bSpikesHit = TRUE )
		
			IF IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_CAMERA_MAN)
		
				IF DOES_ENTITY_EXIST(sGadgetData.SpikesData[sGadgetData.iSpikeHit].VehicleIndex)
				
					VECTOR vFleeCoords
					
					vFleeCoords = GET_ENTITY_COORDS(sGadgetData.SpikesData[sGadgetData.iSpikeHit].VehicleIndex, FALSE)
					
					IF IS_PED_SITTING_IN_THIS_VEHICLE(PedIndex, VehicleIndex)
					
						TASK_VEHICLE_MISSION_COORS_TARGET(PedIndex, VehicleIndex, vFleeCoords, MISSION_FLEE, 25.0,
														  DF_SteerAroundObjects | DF_SteerAroundPeds | DF_SteerAroundStationaryCars |
														  DF_ChangeLanesAroundObstructions | DF_ForceJoinInRoadDirection |
														  DF_SwerveAroundAllCars, 500.0, 10.0, FALSE)
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting auto drive.")
						#ENDIF
						
						iTimer		= 0
						bAutoDrive 	= TRUE
					
					ENDIF

				ENDIF
				
			ENDIF
			
		ENDIF
					
	ELSE
	
		#IF IS_DEBUG_BUILD
			IF ( bDrawDebugStates = TRUE )
				IF ( bAutoDrive = TRUE )
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, "AUTO DRIVE", 0.0)
				ENDIF
			ENDIF
		#ENDIF
	
		IF NOT IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_CAMERA_MAN)
		
			IF ( iTimer = 0 )
				iTimer = GET_GAME_TIMER()
			ENDIF
		
			IF ( IS_PLAYER_IN_CONTROL_OF_VEHICLE(VehicleIndex) )
			OR ( iTimer != 0 AND HAS_TIME_PASSED(3000, iTimer) )
			
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				bAutoDrive = FALSE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping auto drive.")
				#ENDIF
				
			ENDIF
		
		ENDIF
		
	ENDIF

ENDPROC

PROC GO_TO_PED_STATE(PED_STRUCT &ped, PED_STATES eState)

	ped.bHasTask 	= FALSE
	ped.eState 		= eState
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(ped.sDebugName)
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Sending ped with debug name ", ped.sDebugName, " to state ", GET_PED_STATE_NAME_FOR_DEBUG(eState), ".")
		ENDIF
	#ENDIF
	
ENDPROC

PROC CHECK_PED_LINE_OF_SIGHT_TO_PED(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, BOOL &bLOSFlag, INT &iTimer, INT iTime)

	IF HAS_TIME_PASSED(iTime, iTimer)
		IF 	DOES_ENTITY_EXIST(PedIndex)
		AND DOES_ENTITY_EXIST(TargetPedIndex)
			IF 	NOT IS_ENTITY_DEAD(PedIndex)
			AND NOT IS_ENTITY_DEAD(TargetPedIndex)
				
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(PedIndex, TargetPedIndex)
				
					bLOSFlag = TRUE
					
					#IF IS_DEBUG_BUILD
						IF ( bDrawDebugLinesAndSpheres = TRUE )
							DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(TargetPedIndex), 255, 0, 0)
						ENDIF
					#ENDIF
					
				ELSE
				
					bLOSFlag = FALSE
					
				ENDIF

				iTimer = GET_GAME_TIMER()

			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MISSION_CLEANUP()

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting mission cleanup.")
		DELETE_DEBUG_WIDGETS()
	#ENDIF
	
	REMOVE_CUTSCENE()
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	KILL_ANY_CONVERSATION()
	DISABLE_CELLPHONE(FALSE)
	DISABLE_TAXI_HAILING(FALSE)
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	CLEANUP_MODEL_ARRAY(MissionModels, iModelsRequested)
	CLEANUP_ANIMATION_ARRAY(MissionAnimations, iAnimationsRequested)

	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
	ENDIF

	//cleanup mission peds
	CLEANUP_PED(psActor, FALSE, FALSE)
	CLEANUP_PED(psDevin, FALSE, FALSE)
	CLEANUP_PED(psToilet, FALSE, FALSE)
	CLEANUP_PED(psActress, FALSE, FALSE)
	CLEANUP_PED(psAssistant, FALSE, FALSE)
	CLEANUP_PED_GROUP(psAliens, FALSE, FALSE)
	CLEANUP_PED_GROUP(psFilmCrew, FALSE, FALSE)
	CLEANUP_PED_GROUP(psMeltdownCrew, FALSE, FALSE)
	
	CLEANUP_PED_GROUP(psSetSecurity, FALSE, FALSE)
	CLEANUP_PED_GROUP(psCarSecurity, FALSE, FALSE)
	CLEANUP_PED_GROUP(psActorSecurity, FALSE, FALSE)
	CLEANUP_PED_GROUP(psMeltdownSecurity, FALSE, FALSE)
	CLEANUP_PED_GROUP(psNorthGateSecurity, FALSE, FALSE)
	CLEANUP_PED_GROUP(psSouthGateSecurity, FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(osPropToilet.ObjectIndex)
		REMOVE_MODEL_HIDE(osPropToilet.vPosition, 1.0, osPropToilet.ModelName)
	ENDIF
	
	//cleanup mission objects
	CLEANUP_OBJECT(osPropLamp.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osPropChair.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osTrailerDoor.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osPropToilet.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osPropClipboard.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osPropCamera.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osPropMicrophone.ObjectIndex, FALSE)
	CLEANUP_OBJECT(osPropGreenScreen.ObjectIndex, FALSE, FALSE)
	
	//cleanup mission vehicles
	CLEANUP_VEHICLE(vsPlayersCar, FALSE, FALSE)
	CLEANUP_VEHICLE_GROUP(vsSecurityCars, FALSE, FALSE)

	//cleanup mission blips	
	IF DOES_BLIP_EXIST(DestinationBlip)
		REMOVE_BLIP(DestinationBlip)
	ENDIF
	
	CLEANUP_TRACKED_POINT(iCarTrackedPoint)
	CLEANUP_TRACKED_POINT(iActorTrackedPoint)
	

	//reset relationship for SECURITY_GUARD group
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_SECURITY_GUARD, RELGROUPHASH_PLAYER)	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_SECURITY_GUARD, rgFilmCrew)
	
	//remove relationship groups
	REMOVE_RELATIONSHIP_GROUP(rgFilmCrew)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(JB700, FALSE)			
	SET_VEHICLE_MODEL_IS_SUPPRESSED(mnSecurityCar, FALSE)	
	
	SET_PED_MODEL_IS_SUPPRESSED(mnSecurityPed, FALSE)		
	SET_PED_MODEL_IS_SUPPRESSED(U_M_M_SPYACTOR, FALSE)		
	SET_PED_MODEL_IS_SUPPRESSED(U_F_Y_SPYACTRESS, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(U_M_M_FILMDIRECTOR, FALSE)	

	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
		
	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	
	
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	SET_IGNORE_NO_GPS_FLAG(FALSE)
	
	
	

	REMOVE_PTFX_ASSET()

	
	STOP_AUDIO_SCENES()
	RELEASE_SCRIPT_AUDIO_BANK()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("CAR_THEFT_MOVIE_LOT")
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	SET_SCENARIO_GROUP_ENABLED("MOVIE_STUDIO_SECURITY", TRUE)
	
	UNLOCK_STUDIO_GATES()
	UNREGISTER_STUDIO_GATES()
	
	SET_MAX_WANTED_LEVEL(5)													//restore default cop and wanted level settings
	SET_CREATE_RANDOM_COPS(TRUE)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)							//set all shops available
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, FALSE)			//set mod shops available
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, FALSE)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, FALSE)

	CLEAR_WEATHER_TYPE_PERSIST()
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CHOPSHOP, TRUE)
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	
	SET_STUNT_JUMPS_CAN_TRIGGER(TRUE)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission cleanup.")
	#ENDIF
	
ENDPROC

//|========================== MISSION STAGE LOADING PROCEDURES ==========================|

PROC SET_MISSION_PED_PROPERTIES(PED_INDEX PedIndex, REL_GROUP_HASH relGroupHash, BOOL bCanFlyThroughWindscreen, BOOL bKeepRelGroupOnCleanup,
								BOOL bCanBeTargetted, BOOL bIsEnemy)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_PED_INJURED(PedIndex)
		
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_WillFlyThroughWindscreen, bCanFlyThroughWindscreen)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, bKeepRelGroupOnCleanup)
			SET_PED_CAN_BE_TARGETTED(PedIndex, bCanBeTargetted)
			SET_PED_AS_ENEMY(PedIndex, bIsEnemy)
			SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, relGroupHash)
		
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates a ped or player ped to be used by player and returns TRUE if such pad was created successfully.
/// PARAMS:
///    psPed - PED_STRUCT containg ped details, like PED_INDEX, blip, coordinates, heading, model etc.
///    bPlayerPed - Boolean indicating if a player ped or NPC ped should be created.
///    relGroupHash - Relationship group hash for the created ped.
///    bCreateBlip - Boolean indicating if a blip for this ped should be created.
///    eCharacter - Enum specifying character from story characters list. Use NO_CHARACTER for characters not stored in that list and provide a MODEL_NAMES in PED_STRUCT.
///    bCanFlyThroughWindscreen - Sets if ped can fly through windscreen when car crashes.
///    bCanBeTargetted - Sets if ped can be targetted by player.
///    bIsEnemy - Sets if ped is considered an enemy.
///    VehicleIndex - Specify a vehicle index if ped should be created inside a vehicle. IF not use NULL.
///    eVehicleSeat - Vehicle seat enum for peds created in vehicles.
/// RETURNS:
///    TRUE if ped was created successfully, FALSE if otherwise.
FUNC BOOL HAS_MISSION_PED_BEEN_CREATED(PED_STRUCT &psPed, BOOL bPlayerPed, REL_GROUP_HASH relGroupHash, BOOL bCreateBlip, enumCharacterList eCharacter,
									   BOOL bCanFlyThroughWindscreen = FALSE, BOOL bCanBeTargetted = FALSE, BOOL bIsEnemy = FALSE, VEHICLE_INDEX VehicleIndex = NULL,
									   VEHICLE_SEAT eVehicleSeat = VS_DRIVER, BOOL bKeepRelGroupOnCleanup = TRUE #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	IF ( bPlayerPed = FALSE )	//create non player ped
		
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
		
			REQUEST_MODEL(psPed.ModelName)
			
			IF HAS_MODEL_LOADED(psPed.ModelName)
		
				IF ( VehicleIndex = NULL )	//create ped outside of vehicle
				
					IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT if no character was specified
					
						psPed.PedIndex = CREATE_PED(PEDTYPE_MISSION, psPed.ModelName, psPed.vPosition, psPed.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
					
					ELSE								//use character name to define the ped MODEL_NAMES
					
						IF CREATE_NPC_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
						ENDIF
					
					ENDIF
					
					IF NOT IS_PED_INJURED(psPed.PedIndex)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
												   
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
					
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF
					#ENDIF
					
				ELIF ( VehicleIndex != NULL )	//create ped in a vehicle
				
					IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
						IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT
					
							psPed.PedIndex = CREATE_PED_INSIDE_VEHICLE(VehicleIndex, PEDTYPE_MISSION, psPed.ModelName, eVehicleSeat)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							
						ELSE								//use character name to define the ped MODEL_NAMES
						
							IF CREATE_NPC_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat)
								SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							ENDIF
						
						ENDIF
						
						IF NOT IS_PED_INJURED(psPed.PedIndex)
						
							SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
													   bCanBeTargetted, bIsEnemy)
							
							IF ( bCreateBlip = TRUE )
								psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
							ENDIF
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ENDIF
						#ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ELSE
		
			//if the ped already exists return true
			RETURN TRUE
		
		ENDIF
		
		
	ELIF ( bPlayerPed = TRUE )	//create player ped, for example a ped that player can hotswap to and take control of
	
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
	
			IF ( VehicleIndex = NULL )	//create player ped outside of vehicle
	
				IF CREATE_PLAYER_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading, TRUE)
				
					SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
					
					IF ( bCreateBlip = TRUE )
						psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF	
					#ENDIF
					
					RETURN TRUE
					
				ENDIF
				
			ELIF ( VehicleIndex != NULL )	//create player ped in a vehicle
			
				IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
					IF CREATE_PLAYER_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat, TRUE)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
						
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
						
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped  in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ENDIF
						#ENDIF
						
						RETURN TRUE
					
					ENDIF
				
				ENDIF
			
			ENDIF
		
		ELSE
		
			//if the ped already exists, return true
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Creates vehicle to be used on a mission by player and returns TRUE if such vehicle was created successfully.
/// PARAMS:
///    vsVehicle - VEH_STRUCT containing model, position and heading of the vehicle.
///    bPlayerVehicle - If set to TRUE will create player specific vehicles using CREATE_PLAYER_VEHICLE()
///    bCarStealVehicle - If set to TRUE will create car steal strand vehicle with a specific command. Works when bPlayerVehicle is FALSE.
///    eCharacter - Character enum indicating which character specific vehicle to create.
///    bMissionCritical - Sets the vehicle to be unable to leak oil/petrol and break off doors. Set to TRUE to stop these damage types to vehicle.
///    iColourCombination - Colour combination of the vehicle.
///    iColour1 - Colour 1 of the vehicle.
///    iColour2 - Colour 2 of the vehicle.
/// RETURNS:
///    TRUE if vehicle was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_VEHICLE_BEEN_CREATED(VEH_STRUCT &vsVehicle, BOOL bPlayerVehicle = FALSE, BOOL bCarStealVehicle = FALSE, enumCharacterList eCharacter = CHAR_MICHAEL,
										   BOOL bMissionCritical = TRUE, INT iColourCombination = -1, INT iColour1 = -1, INT iColour2 = -1,
										   INT iColour3 = 0, INT iColour4 = 0 #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	//create any vehicle that can be used on a mission by player
	IF ( bPlayerVehicle = FALSE )
	
		IF NOT DOES_ENTITY_EXIST(vsVehicle.VehicleIndex)
		
			REQUEST_MODEL(vsVehicle.ModelName)
			
			IF HAS_MODEL_LOADED(vsVehicle.ModelName)

				IF( bCarStealVehicle = TRUE )
				
					vsVehicle.VehicleIndex = CREATE_CAR_STEAL_STRAND_CAR(vsVehicle.ModelName, vsVehicle.vPosition, vsVehicle.fHeading)
					
					SET_HORN_ENABLED(vsVehicle.VehicleIndex, FALSE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
					
				ELSE
				
					IF ( eCharacter = NO_CHARACTER )
					
						vsVehicle.VehicleIndex = CREATE_VEHICLE(vsVehicle.ModelName, vsVehicle.vPosition, vsVehicle.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
					
					ELSE								//create npc vehicle based on the character enum specified
					
						IF CREATE_NPC_VEHICLE(vsVehicle.VehicleIndex, eCharacter, vsVehicle.vPosition, vsVehicle.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
						ENDIF
					
					ENDIF
					
				ENDIF
				
				
				
				IF IS_VEHICLE_DRIVEABLE(vsVehicle.VehicleIndex)
				
					//set vehicle colours, if they are provided
					IF 	( iColour1 != -1)
					AND ( iColour2 != -1 )
						SET_VEHICLE_COLOURS(vsVehicle.VehicleIndex, iColour1, iColour2)
					ENDIF
					
					//set vehicle colour combination, if it is provided
					IF ( iColourCombination != -1 )
						SET_VEHICLE_COLOUR_COMBINATION(vsVehicle.VehicleIndex, iColourCombination)
					ENDIF
					
					SET_VEHICLE_EXTRA_COLOURS(vsVehicle.VehicleIndex, iColour3, iColour4)
					
					SET_VEHICLE_HAS_STRONG_AXLES(vsVehicle.VehicleIndex, bMissionCritical)
					
					//limit damage that can be done to the vehicle
					SET_VEHICLE_CAN_LEAK_OIL(vsVehicle.VehicleIndex, NOT bMissionCritical)
					SET_VEHICLE_CAN_LEAK_PETROL(vsVehicle.VehicleIndex, NOT bMissionCritical)
					
					//make the vehicle not attach to tow truck if mission critical
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsVehicle.VehicleIndex, NOT bMissionCritical)
					
					IF IS_THIS_MODEL_A_CAR(vsVehicle.ModelName)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
					ENDIF
					
					SET_VEHICLE_ON_GROUND_PROPERLY(vsVehicle.VehicleIndex)
					
				ENDIF

				#IF IS_DEBUG_BUILD
					IF NOT Is_String_Null_Or_Empty(sDebugName)
						SET_VEHICLE_NAME_DEBUG(vsVehicle.VehicleIndex, sDebugName)
						IF ( bCarStealVehicle = TRUE )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created car steal strand vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ENDIF
					ELSE		
						IF ( bCarStealVehicle = TRUE )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created car steal strand vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ENDIF
					ENDIF
				#ENDIF
				
				RETURN TRUE
			
			ENDIF
		
		ELSE	//if the vehicle exists, return true
		
			RETURN TRUE
			
		ENDIF
		
	//create player specific vehicle
	ELIF ( bPlayerVehicle = TRUE )
	
		IF NOT DOES_ENTITY_EXIST(vsVehicle.VehicleIndex)
	
			//call the player vehicle creation until it returns true
			IF CREATE_PLAYER_VEHICLE(vsVehicle.VehicleIndex, eCharacter, vsVehicle.vPosition, vsVehicle.fHeading, TRUE)
			
				SET_VEHICLE_HAS_STRONG_AXLES(vsVehicle.VehicleIndex, bMissionCritical)
			
				//limit damage that can be done to the vehicle
				SET_VEHICLE_CAN_LEAK_OIL(vsVehicle.VehicleIndex, NOT bMissionCritical)
				SET_VEHICLE_CAN_LEAK_PETROL(vsVehicle.VehicleIndex, NOT bMissionCritical)
				
				//make the vehicle not attach to tow truck if mission critical
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsVehicle.VehicleIndex, NOT bMissionCritical)
				
				IF IS_THIS_MODEL_A_CAR(vsVehicle.ModelName)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
				ENDIF
			
				#IF IS_DEBUG_BUILD
					IF NOT Is_String_Null_Or_Empty(sDebugName)
						SET_VEHICLE_NAME_DEBUG(vsVehicle.VehicleIndex, sDebugName)
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
					ELSE
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
					ENDIF	
				#ENDIF
				
				RETURN TRUE
			
			ENDIF
		
		ELSE	//if the vehicle exists, return true
		
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Creates an object and returns TRUE if such object was created successfully.
/// PARAMS:
///    osObject - OBJECT_STRUCT containing model, position and rotation of the object.
///    bFreezeObject - Specify if this object's position should be frozen.
/// RETURNS:
///    TRUE if object was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_OBJECT_BEEN_CREATED(OBJECT_STRUCT &osObject, BOOL bFreezeObject = FALSE)

	IF NOT DOES_ENTITY_EXIST(osObject.ObjectIndex)
		
		REQUEST_MODEL(osObject.ModelName)
		
		IF HAS_MODEL_LOADED(osObject.ModelName)

			osObject.ObjectIndex = CREATE_OBJECT(osObject.ModelName, osObject.vPosition)
			
			SET_ENTITY_INVINCIBLE(osObject.ObjectIndex, TRUE)
			SET_ENTITY_ROTATION(osObject.ObjectIndex, osObject.vRotation)
			SET_ENTITY_COORDS_NO_OFFSET(osObject.ObjectIndex, osObject.vPosition)
			
			IF ( bFreezeObject = TRUE )
				FREEZE_ENTITY_POSITION(osObject.ObjectIndex, bFreezeObject)
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(osObject.ModelName)
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object with model ", GET_MODEL_NAME_FOR_DEBUG(osObject.ModelName), " at coordinates ", osObject.vPosition, ".")
			#ENDIF
		
		ENDIF
	
	ENDIF
	
	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)
	
		RETURN TRUE
	
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC PED_INDEX CREATE_ENEMY_PED(VECTOR vPosition, FLOAT fHeading, MODEL_NAMES ModelName, REL_GROUP_HASH RelHashGroup, INT iHealth = 200,
								WEAPON_TYPE eWeaponType = WEAPONTYPE_UNARMED #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)
		
	PED_INDEX PedIndex = CREATE_PED(PEDTYPE_MISSION, ModelName, vPosition, fHeading)
	
	SET_ENTITY_HEALTH(PedIndex, iHealth)
	SET_PED_MAX_HEALTH(PedIndex, iHealth)
	SET_PED_DIES_WHEN_INJURED(PedIndex, TRUE)
	
	GIVE_WEAPON_TO_PED(PedIndex, eWeaponType, INFINITE_AMMO, FALSE)
	SET_PED_INFINITE_AMMO(PedIndex, TRUE, eWeaponType)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, RelHashGroup)
	
	SET_PED_TARGET_LOSS_RESPONSE(PedIndex, TLR_NEVER_LOSE_TARGET)
	
	SET_PED_CONFIG_FLAG(PedIndex, PCF_RunFromFiresAndExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedIndex, PCF_AlwaysRespondToCriesForHelp, TRUE)
	SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	
	SET_COMBAT_FLOAT(PedIndex, CCF_TIME_TO_INVALIDATE_INJURED_TARGET, 30.0)

	SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(PedIndex)

	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)

	#IF IS_DEBUG_BUILD
		IF NOT Is_String_Null_Or_Empty(sDebugName)
			SET_PED_NAME_DEBUG(PedIndex, sDebugName)
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy ped with model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " and debug name ", sDebugName, " at coordinates ", vPosition, ".")
		ELSE
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy ped with model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " at coordinates ", vPosition, ".")
		ENDIF
	#ENDIF
	
	RETURN PedIndex
								
ENDFUNC

FUNC PED_INDEX CREATE_ENEMY_PED_IN_VEHICLE(VEHICLE_INDEX VehicleIndex, MODEL_NAMES ModelName, REL_GROUP_HASH RelHashGroup,
										   VEHICLE_SEAT eVehicleSeat, INT iHealth = 200, WEAPON_TYPE eWeaponType = WEAPONTYPE_UNARMED
										   #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)
	
	IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
	
		PED_INDEX PedIndex = CREATE_PED_INSIDE_VEHICLE(VehicleIndex, PEDTYPE_MISSION, ModelName, eVehicleSeat)
		
		SET_PED_MAX_HEALTH(PedIndex, iHealth)
		SET_ENTITY_HEALTH(PedIndex, iHealth)
		SET_PED_DIES_WHEN_INJURED(PedIndex, TRUE)
		
		GIVE_WEAPON_TO_PED(PedIndex, eWeaponType, INFINITE_AMMO, FALSE)
		SET_PED_INFINITE_AMMO(PedIndex, TRUE, eWeaponType)
		
		SET_PED_AS_ENEMY(PedIndex, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, RelHashGroup)
		
		SET_PED_TARGET_LOSS_RESPONSE(PedIndex, TLR_NEVER_LOSE_TARGET)
		
		SET_PED_CONFIG_FLAG(PedIndex, PCF_DisableHurt, TRUE)
		SET_PED_CONFIG_FLAG(PedIndex, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		SET_PED_CONFIG_FLAG(PedIndex, PCF_AllowToBeTargetedInAVehicle, TRUE)
		
		SET_COMBAT_FLOAT(PedIndex, CCF_TIME_TO_INVALIDATE_INJURED_TARGET, 30.0)
		
		SET_ENTITY_IS_TARGET_PRIORITY(PedIndex, TRUE)

		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
	
		#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sDebugName)
				SET_PED_NAME_DEBUG(PedIndex, sDebugName)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy ped in vehicle ", sDebugName, ".")
			ELSE
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy ped in vehicle.")
			ENDIF
		#ENDIF
		
		RETURN PedIndex

	ENDIF

	RETURN NULL

ENDFUNC


/// PURPOSE:
///    Sets mission stage enum for replay.
/// PARAMS:
///    eStage - Mission stage enum variable.
///    iValue - Mid-mission replay value returned by the script when replaying the mission.
PROC SET_MISSION_STAGE_FOR_REPLAY(MISSION_STAGES &eStage, INT iValue)

	IF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_ACTOR)
	
		eStage = MISSION_STAGE_GET_TO_ACTOR
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_CAR)
	
		eStage = MISSION_STAGE_GET_TO_CAR
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_ESCAPE_FILM_SET)
	
		eStage = MISSION_STAGE_ESCAPE_FILM_SET
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_DELIVER_CAR)
	
		eStage = MISSION_STAGE_DELIVER_CAR
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission is being replayed at stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

ENDPROC

PROC INITIALISE_ARRAYS_FOR_FILM_CREW()

	osPropLamp.vPosition					= <<-1169.7400, -512.6960, 34.7427>>
	osPropLamp.vRotation					= <<-0.0000, 0.0000, -122.4000>>
	osPropLamp.ModelName					= PROP_STUDIO_LIGHT_02

	osPropChair.vPosition 					= << -1178.42, -511.08, 35.26 >>				//<< -1185.21, -506.609, 35.1495 >>
	osPropChair.vRotation					= << 0.000, 0.000, 180.000 >>					//<< 0.0, 0.0, 0.0 >>
	osPropChair.ModelName 					= PROP_DIRECT_CHAIR_02
	
	osPropClipboard.vPosition				= << -1180.00, -511.25, 34.57 >>
	osPropClipboard.vRotation				= << 0.0, 0.0, 0.0 >>
	osPropClipboard.ModelName				= P_CS_CLIPBOARD
	
	osPropToilet.vPosition					= << -1160.7360, -528.9149, 31.5855 >>
	osPropToilet.vRotation					= << 0.0, 0.0, 152.3009 >>
	osPropToilet.ModelName					= PROP_PORTALOO_01A
	
	osPropGreenScreen.vPosition				= <<-1130.5870, -453.2327, 36.1750>>
	osPropGreenScreen.vRotation				= <<-0.8000, 1.0000, -160.0000>>
	osPropGreenScreen.ModelName				= PROP_LD_GREENSCREEN_01
	
	osPropCamera.vPosition					= << -1132.19, -449.93, 34.61 >>
	osPropCamera.vRotation					= << 0.0, 0.0, 0.0 >>
	osPropCamera.ModelName					= PROP_V_CAM_01
	
	osPropMicrophone.vPosition				= << -1132.75, -451.21, 34.57 >>
	osPropMicrophone.vRotation				= << 0.0, 0.0, 0.0 >>
	osPropMicrophone.ModelName				= PROP_V_BMIKE_01

	psFilmCrew[FCP_DIRECTOR].vPosition		= << -1178.1136, -510.3467, 34.5669 >> 			//film director
	psFilmCrew[FCP_DIRECTOR].fHeading		= 27.1460
	psFilmCrew[FCP_DIRECTOR].ModelName		= U_M_M_FILMDIRECTOR 
	
	psFilmCrew[FCP_GRIP_1].vPosition		= << -1179.1171, -510.1808, 34.5669 >> 			//grip who stands next to director
	psFilmCrew[FCP_GRIP_1].fHeading			= 350.5527
	psFilmCrew[FCP_GRIP_1].ModelName		= S_M_Y_GRIP_01
	
	psFilmCrew[FCP_GRIP_2].vPosition		= <<-1170.0442, -511.5512, 34.5666>>//<<-1171.0500, -512.6278, 34.5666>>//<<-1173.4337, -505.6174, 34.5665>> 			//grip who stands in front of the car
	psFilmCrew[FCP_GRIP_2].fHeading			= 203.5958//246.1889//219.8001
	psFilmCrew[FCP_GRIP_2].ModelName		= S_M_Y_GRIP_01
	
	psFilmCrew[FCP_GRIP_3].vPosition		= <<-1185.9601, -506.3779, 34.5794>> 
	psFilmCrew[FCP_GRIP_3].fHeading			= 65.6138
	psFilmCrew[FCP_GRIP_3].ModelName		= S_M_Y_GRIP_01

	psFilmCrew[FCP_GRIP_4].vPosition		= <<-1177.0441, -488.3724, 34.5519>> 
	psFilmCrew[FCP_GRIP_4].fHeading			= 82.4244
	psFilmCrew[FCP_GRIP_4].ModelName		= S_M_Y_GRIP_01

	psFilmCrew[FCP_GRIP_5].vPosition		= <<-1176.1688, -482.1744, 34.7701>> 
	psFilmCrew[FCP_GRIP_5].fHeading			= 201.0676
	psFilmCrew[FCP_GRIP_5].ModelName		= S_M_Y_GRIP_01
	
	psAliens[0].vPosition					= <<-1140.1760, -472.6156, 33.9209>> 
	psAliens[0].fHeading					= 37.5363
	psAliens[0].ModelName					= S_M_M_MOVALIEN_01
	#IF IS_DEBUG_BUILD
		psAliens[0].sDebugName				= "Alien 0"
	#ENDIF
	
	psAliens[1].vPosition					= <<-1139.2887, -471.7373, 33.9631>>
	psAliens[1].fHeading					= 23.6991
	psAliens[1].ModelName					= S_M_M_MOVALIEN_01
	#IF IS_DEBUG_BUILD
		psAliens[1].sDebugName				= "Alien 1"
	#ENDIF
	
	psActress.vPosition						= << -1181.3146, -506.7412, 34.5669 >>
	psActress.fHeading						= 253.9266
	psActress.ModelName						= U_F_Y_SPYACTRESS
	#IF IS_DEBUG_BUILD
		psActress.sDebugName				= "Actress"
	#ENDIF
	
	psToilet.vPosition						= <<-1159.3536, -527.1266, 31.5364>>
	psToilet.fHeading						= 137.4430
	psToilet.ModelName						= S_M_Y_GRIP_01
	#IF IS_DEBUG_BUILD
		psToilet.sDebugName					= "Toliet"
	#ENDIF
	
	psMeltdownCrew[MSP_ACTORA].vPosition	= <<-1130.6631, -450.2912, 34.6384>> 
	psMeltdownCrew[MSP_ACTORA].fHeading		= 170.2838
	psMeltdownCrew[MSP_ACTORA].ModelName	= IG_MILTON
	#IF IS_DEBUG_BUILD
		psMeltdownCrew[MSP_ACTORA].sDebugName = "MSP_ACTORA"
	#ENDIF
	
	psMeltdownCrew[MSP_ACTORB].vPosition	= <<-1128.7494, -449.8770, 34.6903>> 
	psMeltdownCrew[MSP_ACTORB].fHeading		= 173.7335
	psMeltdownCrew[MSP_ACTORB].ModelName	= S_M_M_MOVALIEN_01//S_M_Y_GRIP_01
	#IF IS_DEBUG_BUILD
		psMeltdownCrew[MSP_ACTORB].sDebugName = "MSP_ACTORB"
	#ENDIF
	
	psMeltdownCrew[MSP_CAMERAMAN].vPosition	= <<-1132.4835, -449.4888, 34.6125>> 
	psMeltdownCrew[MSP_CAMERAMAN].fHeading	= 207.1287
	psMeltdownCrew[MSP_CAMERAMAN].ModelName	= S_M_Y_GRIP_01
	#IF IS_DEBUG_BUILD
		psMeltdownCrew[MSP_CAMERAMAN].sDebugName = "MSP_CAMERAMAN"
	#ENDIF
	
	psMeltdownCrew[MSP_BOOMMAN].vPosition	= <<-1133.2305, -451.1367, 34.5651>>
	psMeltdownCrew[MSP_BOOMMAN].fHeading	= 222.7364
	psMeltdownCrew[MSP_BOOMMAN].ModelName	= S_M_Y_GRIP_01
	#IF IS_DEBUG_BUILD
		psMeltdownCrew[MSP_BOOMMAN].sDebugName	= "MSP_BOOMMAN"
	#ENDIF
	
	psMeltdownCrew[MSP_DIRECTOR].vPosition	= <<-1128.5414, -447.9520, 34.7552>>
	psMeltdownCrew[MSP_DIRECTOR].fHeading	= 157.4622
	psMeltdownCrew[MSP_DIRECTOR].ModelName	= U_M_Y_ANTONB
	#IF IS_DEBUG_BUILD
		psMeltdownCrew[MSP_DIRECTOR].sDebugName	= "MSP_DIRECTOR"
	#ENDIF
	
ENDPROC

PROC INITIALISE_ARRAYS_FOR_SECURITY()

	psSetSecurity[0].vPosition				= << -1175.2550, -501.9372, 34.5669 >>
	psSetSecurity[0].fHeading				= 138.1011
	psSetSecurity[0].iConversationTimer		= -1
	psSetSecurity[0].ModelName				= mnSecurityPed
	#IF IS_DEBUG_BUILD
		psSetSecurity[0].sDebugName			= "Set 0"
	#ENDIF
	
	psSetSecurity[1].vPosition				= << -1199.8624, -493.9804, 34.5414 >> 
	psSetSecurity[1].fHeading				= 219.7226
	psSetSecurity[1].iConversationTimer		= -1
	psSetSecurity[1].ModelName				= mnSecurityPed
	#IF IS_DEBUG_BUILD
		psSetSecurity[1].sDebugName			= "Set 1"
	#ENDIF
	
	psSetSecurity[2].vPosition				= << -1188.6659, -510.1270, 34.6740 >> 
	psSetSecurity[2].fHeading				= 317.9273
	psSetSecurity[2].iConversationTimer		= -1
	psSetSecurity[2].ModelName				= mnSecurityPed
	#IF IS_DEBUG_BUILD
		psSetSecurity[2].sDebugName			= "Set 2"
	#ENDIF

	psSetSecurity[3].vPosition				= << -1180.6191, -522.2653, 29.3807 >> 
	psSetSecurity[3].fHeading				= 238.8610
	psSetSecurity[3].iConversationTimer		= -1
	psSetSecurity[3].ModelName				= mnSecurityPed
	#IF IS_DEBUG_BUILD
		psSetSecurity[3].sDebugName			= "Set 3"
	#ENDIF
	
	psSetSecurity[4].vPosition				= <<-1149.7146, -512.0564, 32.6042>>
	psSetSecurity[4].fHeading				= 240.8322
	psSetSecurity[4].iConversationTimer		= -1
	psSetSecurity[4].ModelName				= mnSecurityPed
	#IF IS_DEBUG_BUILD
		psSetSecurity[4].sDebugName			= "Set 4"
	#ENDIF
	
	psNorthGateSecurity[0].vPosition		= <<-1066.0760, -477.5667, 35.6377>>
	psNorthGateSecurity[0].fHeading			= 209.0050
	psNorthGateSecurity[0].ModelName		= mnSecurityPed
	#IF IS_DEBUG_BUILD
		psNorthGateSecurity[0].sDebugName	= "NorthGate 0"
	#ENDIF
	
	psSouthGateSecurity[0].vPosition		= <<-1209.3595, -569.9016, 26.3524>>
	psSouthGateSecurity[0].fHeading			= 222.0723
	psSouthGateSecurity[0].ModelName		= mnSecurityPed
	#IF IS_DEBUG_BUILD
		psSouthGateSecurity[0].sDebugName	= "SouthGate 0"
	#ENDIF
	
	psMeltdownSecurity[0].vPosition			= <<-1118.7767, -440.0241, 35.2380>>
	psMeltdownSecurity[0].fHeading			= 120.6108
	psMeltdownSecurity[0].ModelName			= mnSecurityPed
	#IF IS_DEBUG_BUILD
		psMeltdownSecurity[0].sDebugName	= "Meltdown 0"
	#ENDIF

ENDPROC

PROC INITIALISE_ARRAYS_FOR_SECURITY_SPAWNPOINTS()

	SecuritySpawned[0]						= FALSE
	SecuritySpawnPoints[0].vPosition		= <<-1098.1554, -540.3402, 34.3502>>
	SecuritySpawnPoints[0].fHeading			= 297.8740
	
	SecuritySpawned[1]						= FALSE
	SecuritySpawnPoints[1].vPosition		= <<-1095.3468, -492.3910, 35.0424>> 
	SecuritySpawnPoints[1].fHeading			= 205.2367
	
	SecuritySpawned[2]						= FALSE
	SecuritySpawnPoints[2].vPosition		= <<-1124.9238, -499.1039, 33.7600>> 
	SecuritySpawnPoints[2].fHeading			= 121.9869
	
	SecuritySpawned[3]						= FALSE
	SecuritySpawnPoints[3].vPosition		= <<-1068.7140, -503.8521, 35.1105>>
	SecuritySpawnPoints[3].fHeading			= 28.3751
	
	SecuritySpawned[4]						= FALSE
	SecuritySpawnPoints[4].vPosition		= <<-1104.8634, -485.5148, 35.0221>>
	SecuritySpawnPoints[4].fHeading			= 299.0859 
	
	SecuritySpawned[5]						= FALSE
	SecuritySpawnPoints[5].vPosition		= <<-1091.2770, -549.1374, 33.9233>> 
	SecuritySpawnPoints[5].fHeading			= 304.0113
	
	SecuritySpawned[6]						= FALSE
	SecuritySpawnPoints[6].vPosition		= <<-1134.6398, -530.8317, 31.9306>>  
	SecuritySpawnPoints[6].fHeading			= 120.3262
	
	SecuritySpawned[7]						= FALSE
	SecuritySpawnPoints[7].vPosition		= <<-1124.9128, -504.2563, 33.6356>>  
	SecuritySpawnPoints[7].fHeading			= 299.8388

ENDPROC

/// PURPOSE:
///    Populates structs and arrays with data such as vectors, model names etc for a specified mission stage.
/// PARAMS:
///    eStage - Mission stage to initialise arrays for.
PROC INITIALISE_ARRAYS_FOR_MISSION_STAGE(MISSION_STAGES eStage)

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Initialising arrays for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
	INITIALISE_ARRAYS_FOR_SECURITY()
	INITIALISE_ARRAYS_FOR_FILM_CREW()
	INITIALISE_ARRAYS_FOR_SECURITY_SPAWNPOINTS()
	
	SWITCH eStage
	 
		CASE MISSION_STAGE_GET_TO_ACTOR
		
			sMissionPosition.vPosition				= << -1085.6110, -428.5754, 35.6276 >> 
			sMissionPosition.fHeading				= 118.9753
		
			vsPlayersCar.vPosition					= << -1180.1277, -505.9280, 34.5664 >>    
			vsPlayersCar.fHeading					= 246.5268
			vsPlayersCar.ModelName					= JB700
			
			psActor.vPosition						= << -1109.36, -503.38, 34.26 >>
			psActor.fHeading						= 116.5653
			psActor.ModelName						= U_M_M_SPYACTOR
			
			psAssistant.vPosition					= << -1112.3136, -504.5007, 34.0635 >> //aligned << -1112.14, -504.91, 34.0635 >>
			psAssistant.fHeading					= 296.0018
			psAssistant.ModelName					= A_F_Y_HIPSTER_04
						
			osTrailerDoor.vPosition					= << -1113.92, -502.832, 35.882 >>
			osTrailerDoor.vRotation					= << 0.0, 0.0, -84.1778 >>				//trailer door in open rotation
			osTrailerDoor.ModelName					= PROP_A_TRAILER_DOOR_01

		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_TRAILER

			sMissionPosition.vPosition				= << -1109.6000, -502.7238, 34.2594 >> 
			sMissionPosition.fHeading				= 113.6969
			
			vsPlayersCar.vPosition					= << -1180.1277, -505.9280, 34.5664 >>    
			vsPlayersCar.fHeading					= 246.5268
			vsPlayersCar.ModelName					= JB700
			
			psActor.vPosition						= << -1105.2350, -491.2045, 34.8387 >>	//coords will place the ped inside a building 
			psActor.fHeading						= 206.1382								//this is fine as he has to be hidden
			psActor.ModelName						= U_M_M_SPYACTOR
			
			osTrailerDoor.vPosition					= << -1113.92, -502.832, 35.882 >>
			osTrailerDoor.vRotation					= << 0.0, 0.0, -84.1778 >>				//trailer door in open rotation
			osTrailerDoor.ModelName					= PROP_A_TRAILER_DOOR_01
			
		BREAK
		
		CASE MISSION_STAGE_GET_TO_CAR
		
			sMissionPosition.vPosition				= << -1115.4556, -505.5281, 33.9062 >>
			sMissionPosition.fHeading				= 117.7305
		
			vsPlayersCar.vPosition					= << -1180.1277, -505.9280, 34.5664 >>    
			vsPlayersCar.fHeading					= 246.5268
			vsPlayersCar.ModelName					= JB700
			
			psActor.vPosition						= <<-1116.6180, -502.8903, 34.8063>>
			psActor.fHeading						= 304.2335
			psActor.ModelName						= U_M_M_SPYACTOR
			
			osTrailerDoor.vPosition					= <<-1113.9050, -502.8200, 35.882>>
			osTrailerDoor.vRotation					= << 0.0, 0.0, 27 >>					//trailer door in closed rotation
			osTrailerDoor.ModelName					= PROP_A_TRAILER_DOOR_01
		
		BREAK

		CASE MISSION_STAGE_ESCAPE_FILM_SET

			sMissionPosition.vPosition				= << -1180.1184, -504.4346, 34.5669 >> 
			sMissionPosition.fHeading				= 252.0904
		
			vsPlayersCar.vPosition					= << -1180.1277, -505.9280, 34.5664 >>    
			vsPlayersCar.fHeading					= 246.5268
			vsPlayersCar.ModelName					= JB700
			
			psActor.vPosition						= << -1105.23, -491.20, 34.83 >>	//coords inside a building 
			psActor.fHeading						= 206.1382							//this is fine as ped has to be hidden
			psActor.ModelName						= U_M_M_SPYACTOR					//ped will play animation afterwards
						
			osTrailerDoor.vPosition					= << -1113.92, -502.832, 35.882 >>
			osTrailerDoor.vRotation					= << 0.0, 0.0, -84.1778 >>				//trailer door in open rotation
			osTrailerDoor.ModelName					= PROP_A_TRAILER_DOOR_01
			
		BREAK
		
		CASE MISSION_STAGE_DELIVER_CAR

			sMissionPosition.vPosition				= << 126.2967, -912.3267, 29.0396 >>
			sMissionPosition.fHeading				= 160.9179
		
			vsPlayersCar.vPosition					= << 124.8845, -913.1312, 28.9710 >>  
			vsPlayersCar.fHeading					= 161.6850
			vsPlayersCar.ModelName					= JB700
			
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_END
		
			sMissionPosition.vPosition				= << 481.8158, -1318.6210, 28.2052 >>
			sMissionPosition.fHeading				= 119.3001
		
			vsPlayersCar.vPosition					= << 480.7779, -1317.7502, 28.2059 >>
			vsPlayersCar.fHeading					= 116.0750
			vsPlayersCar.ModelName					= JB700
		
		BREAK
		
		#IF IS_DEBUG_BUILD
		
			CASE MISSION_STAGE_EJECTOR_TEST
	
				sMissionPosition.vPosition				= << 126.2967, -912.3267, 29.0396 >>
				sMissionPosition.fHeading				= 160.9179
			
				vsPlayersCar.vPosition					= << 124.8845, -913.1312, 28.9710 >>  
				vsPlayersCar.fHeading					= 161.6850
				vsPlayersCar.ModelName					= JB700
				
				psActress.vPosition						= << -1181.3146, -506.7412, 34.5669 >>
				psActress.fHeading						= 253.9266
				psActress.ModelName						= U_F_Y_SPYACTRESS
			
			BREAK
	
		#ENDIF
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initialising arrays for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

ENDPROC

PROC BUILD_REQUEST_BANK_FOR_MISSION_STAGE(MISSION_STAGES eStage)

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Building request bank for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

	SWITCH eStage

		CASE MISSION_STAGE_GET_TO_ACTOR
			IF NOT DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsPlayersCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psActor.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psActor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osTrailerDoor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_TRAILER
			IF NOT DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsPlayersCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(psActor.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psActor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osTrailerDoor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_GET_TO_CAR
			IF NOT DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsPlayersCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osTrailerDoor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_ESCAPE_FILM_SET
			IF NOT DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsPlayersCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(osTrailerDoor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_MODEL_REQUEST_TO_ARRAY(mnSpike, MissionModels, MAX_MODELS, iModelsRequested)		
		BREAK
		
		CASE MISSION_STAGE_DELIVER_CAR
			IF NOT DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsPlayersCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_MODEL_REQUEST_TO_ARRAY(mnSpike, MissionModels, MAX_MODELS, iModelsRequested)		
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_END
			IF NOT DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsPlayersCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished building request bank for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
ENDPROC

FUNC BOOL IS_MISSION_STAGE_LOADED(MISSION_STAGES eStage, INT &iSetupProgress, BOOL &bStageLoaded, BOOL &bStageSkippedTo, BOOL &bStageReplayed)

	//handle initial mission setup
	//only run this once, when the mission first loads
	IF ( iSetupProgress = 0 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting initial mission loading.")
		#ENDIF
		
		CLEAR_HELP()
		CLEAR_PRINTS()
		
		REQUEST_ADDITIONAL_TEXT("CAR4", MISSION_TEXT_SLOT)
		REQUEST_ADDITIONAL_TEXT("CST4AUD", MISSION_DIALOGUE_TEXT_SLOT)
		
		IF NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
			WAIT(0)
		ENDIF
		
		REGISTER_SCRIPT_WITH_AUDIO(TRUE)
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(JB700, TRUE)			
		SET_VEHICLE_MODEL_IS_SUPPRESSED(mnSecurityCar, TRUE)	
		
		SET_PED_MODEL_IS_SUPPRESSED(mnSecurityPed, TRUE)		
		SET_PED_MODEL_IS_SUPPRESSED(U_M_M_SPYACTOR, TRUE)		
		SET_PED_MODEL_IS_SUPPRESSED(U_F_Y_SPYACTRESS, TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(U_M_M_FILMDIRECTOR, TRUE)
		
		//setup relationship groups
		ADD_RELATIONSHIP_GROUP("FilmCrew", rgFilmCrew)

		//film crew
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,		rgFilmCrew, rgFilmCrew)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,		rgFilmCrew, RELGROUPHASH_COP)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,		rgFilmCrew, RELGROUPHASH_SECURITY_GUARD)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,	rgFilmCrew, RELGROUPHASH_PLAYER)
		
		//global SECURITY_GUARD relationship group
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,		RELGROUPHASH_SECURITY_GUARD, rgFilmCrew)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE,	RELGROUPHASH_SECURITY_GUARD, RELGROUPHASH_PLAYER)
	
		SET_IGNORE_NO_GPS_FLAG(TRUE)
		SET_GPS_FLAGS(GPS_FLAG_NONE, 15.0)
	
		bHasOutfitChangedBeforeMission = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN)  //ensure bChange is set to the state of “player has changed outfit” at the start of the mission
				
		SUPPRESS_EMERGENCY_CALLS()
		DISABLE_TAXI_HAILING(TRUE)
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO)
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID)
			ADD_DOOR_TO_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID,
							   g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].model,
							   g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].coords)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding door AUTODOOR_HAYES_GARAGE to system.")
			#ENDIF
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID)
			SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, TRUE)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_SCRIPT_UPDATE_DOOR_AUDIO for AUTODOOR_HAYES_GARAGE.")
			#ENDIF
		ENDIF
		
		IF IS_REPEAT_PLAY_ACTIVE()
			bStageReplayed = TRUE
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		ELSE
			SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 15.0)
		ENDIF
		
		iEjectorScreamSoundID = GET_SOUND_ID()
		
		//fix for B*2066186, some poor soul decided to add a van vehicle generator exactly where the green screen scene takes place, block it on mission
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1130.463867 - 6.0, -455.577698 - 6.0, 36.564621 - 3.0 >>, << -1130.463867 + 6.0, -455.577698 + 6.0, 36.564621 + 3.0 >>, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< -1130.463867 - 6.0, -455.577698 - 6.0, 36.564621 - 3.0 >>, << -1130.463867 + 6.0, -455.577698 + 6.0, 36.564621 + 3.0 >>)
		
		SET_STUNT_JUMPS_CAN_TRIGGER(FALSE)
		
		#IF IS_DEBUG_BUILD
			PRINT_GLOBAL_BIT_SET()
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initial mission loading.")
		#ENDIF
		
		iSetupProgress++
	
	ENDIF
	
	//run this each time new mission stage needs to be loaded
	IF ( iSetupProgress = 1 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Started mission stage loading for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded = FALSE
		
		//initialise arrays for peds, vehicles and objects
		INITIALISE_ARRAYS_FOR_MISSION_STAGE(eStage)
		
		//cleanup the asset arrays so that they are ready to be populated with assets needed for the stage being loaded
		CLEANUP_MODEL_ARRAY(MissionModels, iModelsRequested)
		CLEANUP_ANIMATION_ARRAY(MissionAnimations, iAnimationsRequested)
		
		//clear triggered text labels
		CLEAR_TRIGGERED_LABELS()

		iSetupProgress++
	
	ENDIF
	
	//handle player positioning when current stage is being skipped to or played normally
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 2 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to new position for mission stage being loaded due to skipping or replay.")
			#ENDIF
			
			//handle resetting flags and the global bit set variable depending on mission stage skipped to
			SWITCH eStage
				CASE MISSION_STAGE_GET_TO_ACTOR
					bPlayerEnteredTargetVehicle = FALSE
					RESET_GLOBAL_BIT_SET()
				BREAK
				CASE MISSION_STAGE_CUTSCENE_TRAILER
					bPlayerEnteredTargetVehicle = FALSE
				BREAK
				CASE MISSION_STAGE_GET_TO_CAR
					bPlayerEnteredTargetVehicle = FALSE
				BREAK
				CASE MISSION_STAGE_ESCAPE_FILM_SET
					bPlayerEnteredTargetVehicle = TRUE
				BREAK
				CASE MISSION_STAGE_DELIVER_CAR
					bPlayerEnteredTargetVehicle = TRUE
				BREAK
				CASE MISSION_STAGE_CUTSCENE_END
					bPlayerEnteredTargetVehicle = TRUE
				BREAK
			ENDSWITCH
			
			WARP_PED(PLAYER_PED_ID(), sMissionPosition.vPosition, sMissionPosition.fHeading, FALSE, TRUE, FALSE)

			IF ( bStageSkippedTo = TRUE )
				//pin the interior at player start position for stages starting in interiors or requiring interiors
				IF ( eStage = MISSION_STAGE_CUTSCENE_END )
				
					WHILE NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorPosition))
						
						PIN_INTERIOR_IN_MEMORY(GET_INTERIOR_AT_COORDS(vInteriorPosition))
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Pinning interior at coordinates ", vInteriorPosition, " in memory.")
						#ENDIF
						
						WAIT(0)
					
					ENDWHILE
				
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_TATTOOS(PLAYER_PED_ID())

				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				
				STOP_FIRE_IN_RANGE(sMissionPosition.vPosition, 1000.0)
				REMOVE_DECALS_IN_RANGE(sMissionPosition.vPosition, 1000.0)
				REMOVE_PARTICLE_FX_IN_RANGE(sMissionPosition.vPosition, 1000.0)
				
				CLEAR_AREA(sMissionPosition.vPosition, 1000.0, TRUE)
				CLEAR_AREA_OF_VEHICLES(sMissionPosition.vPosition, 1000.0)
				
				CLEAR_AREA_OF_OBJECTS(<< -1051.45, -477.70, 35.99 >>, 20.0, CLEAROBJ_FLAG_FORCE | CLEAROBJ_FLAG_INCLUDE_DOORS)	//north gate barriers
				CLEAR_AREA_OF_OBJECTS(<< -1209.42, -577.77, 26.67 >>, 20.0, CLEAROBJ_FLAG_FORCE | CLEAROBJ_FLAG_INCLUDE_DOORS)	//south gate barriers
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
			ENDIF
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
			
			bSecurityAlerted					= FALSE				//reset security alerted flag
			iPlayerLethalKills					= 0					//reset player kill counters
			iPlayerStealthKills					= 0
			iPlayerTakedownKills				= 0
			fDistanceFromPlayerToTargetVehicle 	= 0					//reset the distance check on skips
			
			sLabelDCAR							= "CAR4_DCAR"			
			sLabelGENGETIN						= "CMN_GENGETIN"
			sLabelGENGETBCK						= "CMN_GENGETBCK"
			
			CST4_DSC_TRIGGERED					= FALSE
			CST4_DSG_TRIGGERED					= FALSE
			CST4_DSW_TRIGGERED					= FALSE
			CST4_DBRAN_1_TRIGGERED				= FALSE
			CST4_DBRAN_2_TRIGGERED				= FALSE
			CST4_DBRAN_3_TRIGGERED				= FALSE
			CST4_DBRAN_4_TRIGGERED				= FALSE
			CST4_DBRAN_5_TRIGGERED				= FALSE
			CST4_DSCAR_TRIGGERED				= FALSE
			CST4_DSSET1_TRIGGERED 				= FALSE
			CST4_DSSET2_TRIGGERED 				= FALSE
			CST4_DCHAT1_TRIGGERED				= FALSE
			CST4_DCHAT2_TRIGGERED				= FALSE
			CST4_DSG_PLAYING_TRIGGERED			= FALSE
			
		ENDIF
		
		SWITCH eStage
		
			CASE MISSION_STAGE_GET_TO_ACTOR
			CASE MISSION_STAGE_CUTSCENE_TRAILER
			CASE MISSION_STAGE_GET_TO_CAR
			CASE MISSION_STAGE_ESCAPE_FILM_SET
				IF ( bFilmStudioAmbientPedsBlocked = FALSE )
				
					SET_SCENARIO_GROUP_ENABLED("MOVIE_STUDIO_SECURITY", FALSE)															//disable ambient security guards
					
					CREATE_PED_NON_CREATION_AREA(<< -1149.78, -508.89, 40.84 >>, << 72.0, 52.0, 16.0 >>)								//block peds arounds film set
					
					IF ( ScenarioBlockingArea1 = NULL )
						ScenarioBlockingArea1 = CREATE_SCENARIO_BLOCKING_AREA(<< -1149.78, -508.89, 40.84 >>, << 72.0, 52.0, 16.0 >>) 	//big area
					ENDIF
					
					IF ( ScenarioBlockingArea2 = NULL )
						ScenarioBlockingArea2 = CREATE_SCENARIO_BLOCKING_AREA(<< -1073.38, -476.93, 37.63 >>, << 20.0, 20.0, 4.0>>) 	//north gate
					ENDIF
					
					CLEAR_AREA_OF_PEDS(<< -1149.78, -508.89, 40.84 >>, 90.0)															//big area
					CLEAR_AREA_OF_PEDS(<< -1080.52, -479.68, 35.69>>, 20.0)																//north gate
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Added film studio scenario blocking areas and non ped creation areas.")
					#ENDIF
					
					bFilmStudioAmbientPedsBlocked = TRUE
				ENDIF
			BREAK
			
			DEFAULT
				//
			BREAK
				
		ENDSWITCH
		
		IF ( bStageReplayed = TRUE )
			START_REPLAY_SETUP(sMissionPosition.vPosition, sMissionPosition.fHeading)
		ENDIF
		
		//request assets needed for current stage
		BUILD_REQUEST_BANK_FOR_MISSION_STAGE(eStage)
		
		iSetupProgress++
	
	ENDIF
	
	//handle loading of mission models that have been requested in the previous stage in BUILD_REQUEST_BANK_FOR_MISSION_STAGE()
	//handle loading of mission animations that been requested in the previous stage in BUILD_REQUEST_BANK_FOR_MISSION_STAGE()
	IF ( iSetupProgress = 3)
	
		IF ARE_REQUESTED_MODELS_LOADED(MissionModels, iModelsRequested)
			IF ARE_REQUESTED_ANIMATIONS_LOADED(MissionAnimations, iAnimationsRequested)
				iSetupProgress++
			ENDIF
		ENDIF
	
	ENDIF
	
	//handle creating vehicles/peds and setting up fail flags
	//run this each time new mission stage needs to be loaded
	//THIS SETUP SECTION ASSUMES THAT ALL REQUIRED ASSETS HAVE BEEN ALREADY LOADED TO MEMORY IN PREVIOUS SETUP SECTIONS
	IF ( iSetupProgress = 4 )
	
		SET_FAIL_FLAGS(FALSE)
	
		SWITCH eStage

			CASE MISSION_STAGE_GET_TO_ACTOR
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsPlayersCar, FALSE, TRUE, NO_CHARACTER)
					
					FailFlags[MISSION_FAIL_CAR_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_CAR_UNDRIVABLE] 		= TRUE
					FailFlags[MISSION_FAIL_STUDIO_LEFT_BEHIND] 	= TRUE
					 
					SET_VEHICLE_AS_RESTRICTED(vsPlayersCar.VehicleIndex, 0)
					 
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						SET_VEHICLE_STRONG(vsPlayersCar.VehicleIndex, TRUE)
						SET_VEHICLE_TYRES_CAN_BURST(vsPlayersCar.VehicleIndex, FALSE)
					ENDIF
					
					IF 	HAS_MISSION_PED_BEEN_CREATED(psActor, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
					AND	HAS_MISSION_PED_BEEN_CREATED(psAssistant, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
					AND HAS_MISSION_PED_BEEN_CREATED(psActress, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE, FALSE, vsPlayersCar.VehicleIndex, VS_FRONT_RIGHT)
					
						IF NOT IS_PED_INJURED(psActor.PedIndex)
							SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_LEG, 	0, 0, 0)
							SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_TORSO,	0, 0, 0)
						ENDIF
						
						IF NOT IS_PED_INJURED(psAssistant.PedIndex)
							SET_PED_COMPONENT_VARIATION(psAssistant.PedIndex, PED_COMP_HEAD, 	1, 1, 0) //(head)
							SET_PED_COMPONENT_VARIATION(psAssistant.PedIndex, PED_COMP_HAIR, 	1, 0, 0) //(hair)
							SET_PED_COMPONENT_VARIATION(psAssistant.PedIndex, PED_COMP_TORSO, 	0, 2, 0) //(uppr)
							SET_PED_COMPONENT_VARIATION(psAssistant.PedIndex, PED_COMP_LEG, 	0, 0, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(psAssistant.PedIndex, PED_COMP_HAND, 	0, 0, 0) //(hand)
						ENDIF
						
						IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osTrailerDoor, TRUE)
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osPropLamp, FALSE)
						
							IF DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
								IF NOT IS_ENTITY_DEAD(osTrailerDoor.ObjectIndex)
									SET_ENTITY_INVINCIBLE(osTrailerDoor.ObjectIndex, TRUE)
								ENDIF
							ENDIF
							
							//handle player's mission start vehicle
							
							VEHICLE_INDEX LastPlayerVehicleIndex

							LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
							
							IF IS_REPLAY_START_VEHICLE_AVAILABLE()
							
								IF 	DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
								AND IS_VEHICLE_DRIVEABLE(LastPlayerVehicleIndex)
								
									IF NOT IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
										SET_ENTITY_AS_MISSION_ENTITY(LastPlayerVehicleIndex)
									ENDIF
									
									TRACK_VEHICLE_FOR_IMPOUND(LastPlayerVehicleIndex)
									
									SET_VEHICLE_AS_NO_LONGER_NEEDED(LastPlayerVehicleIndex)
								
								ENDIF
							
							ENDIF
						
							iSetupProgress++
							
						ENDIF

					ENDIF

				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_TRAILER
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsPlayersCar, FALSE, TRUE, NO_CHARACTER)
					
					FailFlags[MISSION_FAIL_CAR_DEAD] 		= TRUE
					FailFlags[MISSION_FAIL_CAR_UNDRIVABLE] 	= TRUE
					FailFlags[MISSION_FAIL_CAR_LEFT_BEHIND] = TRUE
					
					SET_VEHICLE_AS_RESTRICTED(vsPlayersCar.VehicleIndex, 0)
					
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						SET_VEHICLE_STRONG(vsPlayersCar.VehicleIndex, TRUE)
						SET_VEHICLE_TYRES_CAN_BURST(vsPlayersCar.VehicleIndex, FALSE)						
					ENDIF
					
					IF HAS_MISSION_PED_BEEN_CREATED(psActor, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
					
						IF NOT IS_PED_INJURED(psActor.PedIndex)
							SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_LEG, 0, 0, 0)
							SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_TORSO, 0, 0, 0)
						ENDIF

						IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osTrailerDoor, TRUE)
						AND HAS_MISSION_OBJECT_BEEN_CREATED(osPropLamp, FALSE)
						
							IF DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
								IF NOT IS_ENTITY_DEAD(osTrailerDoor.ObjectIndex)
									SET_ENTITY_INVINCIBLE(osTrailerDoor.ObjectIndex, TRUE)
								ENDIF
							ENDIF
						
							iSetupProgress++
							
						ENDIF

					ENDIF

				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_GET_TO_CAR
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsPlayersCar, FALSE, TRUE, NO_CHARACTER)
					
					FailFlags[MISSION_FAIL_CAR_DEAD] 		= TRUE
					FailFlags[MISSION_FAIL_CAR_UNDRIVABLE] 	= TRUE
					FailFlags[MISSION_FAIL_CAR_LEFT_BEHIND] = TRUE
					
					SET_VEHICLE_AS_RESTRICTED(vsPlayersCar.VehicleIndex, 0)
					
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						SET_VEHICLE_STRONG(vsPlayersCar.VehicleIndex, TRUE)
						SET_VEHICLE_TYRES_CAN_BURST(vsPlayersCar.VehicleIndex, FALSE)
						SET_VEHICLE_ENGINE_ON(vsPlayersCar.VehicleIndex, TRUE, TRUE)
					ENDIF
					
					SET_RADIO_TO_STATION_INDEX(255)
					
					IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osTrailerDoor, TRUE)
					AND HAS_MISSION_OBJECT_BEEN_CREATED(osPropLamp, FALSE)
					
						IF DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osTrailerDoor.ObjectIndex)
								SET_ENTITY_INVINCIBLE(osTrailerDoor.ObjectIndex, TRUE)
							ENDIF
						ENDIF
						
						IF HAS_MISSION_PED_BEEN_CREATED(psActor, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
					
							IF NOT IS_PED_INJURED(psActor.PedIndex)
								SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_LEG, 	1, 0, 0)
								SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_TORSO, 	1, 0, 0)
							ENDIF
						
							IF HAS_MISSION_PED_BEEN_CREATED(psActress, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE, FALSE, vsPlayersCar.VehicleIndex, VS_FRONT_RIGHT)
							
								GO_TO_PED_STATE(psActress, PED_STATE_SITTING_IN_CAR)
											
								iSetupProgress++
								
							ENDIF
						ENDIF
					ENDIF

				ENDIF

			BREAK
			
			CASE MISSION_STAGE_ESCAPE_FILM_SET

				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsPlayersCar, FALSE, TRUE, NO_CHARACTER)
				
					FailFlags[MISSION_FAIL_CAR_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_CAR_UNDRIVABLE] 		= TRUE
					FailFlags[MISSION_FAIL_CAR_LEFT_BEHIND] 	= TRUE
					
					FailFlags[MISSION_FAIL_COPS_AT_GARAGE] 		= TRUE
					FailFlags[MISSION_FAIL_ACTRESS_AT_GARAGE] 	= TRUE
					FailFlags[MISSION_FAIL_SECURITY_AT_GARAGE] 	= TRUE
					
					SET_VEHICLE_AS_RESTRICTED(vsPlayersCar.VehicleIndex, 0)
					
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						SET_VEHICLE_STRONG(vsPlayersCar.VehicleIndex, TRUE)
						SET_VEHICLE_TYRES_CAN_BURST(vsPlayersCar.VehicleIndex, FALSE)
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
					ENDIF
							
					IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osTrailerDoor, TRUE)
					AND HAS_MISSION_OBJECT_BEEN_CREATED(osPropLamp, FALSE)
					
						IF DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osTrailerDoor.ObjectIndex)
								SET_ENTITY_INVINCIBLE(osTrailerDoor.ObjectIndex, TRUE)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTRESS_FLED_CAR)
					
							iSetupProgress++
							
						ELSE
						
							IF HAS_MISSION_PED_BEEN_CREATED(psActress, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE, FALSE, vsPlayersCar.VehicleIndex, VS_FRONT_RIGHT)
					
								GO_TO_PED_STATE(psActress, PED_STATE_SITTING_IN_CAR)
								
								iSetupProgress++
							
							ENDIF
						
						ENDIF
						
					ENDIF

				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_DELIVER_CAR
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsPlayersCar, FALSE, TRUE, NO_CHARACTER)
					
					FailFlags[MISSION_FAIL_CAR_DEAD] 		= TRUE
					FailFlags[MISSION_FAIL_CAR_UNDRIVABLE] 	= TRUE
					FailFlags[MISSION_FAIL_CAR_LEFT_BEHIND] = TRUE
					
					SET_VEHICLE_AS_RESTRICTED(vsPlayersCar.VehicleIndex, 0)
					
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						SET_VEHICLE_STRONG(vsPlayersCar.VehicleIndex, FALSE)
						SET_VEHICLE_TYRES_CAN_BURST(vsPlayersCar.VehicleIndex, FALSE)
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
					ENDIF
					
					FailFlags[MISSION_FAIL_DEVIN_DEAD]			= TRUE
					FailFlags[MISSION_FAIL_DEVIN_ATTACKED]		= TRUE
					FailFlags[MISSION_FAIL_COPS_AT_GARAGE] 		= TRUE
					
					iSetupProgress++
					
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_END
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsPlayersCar, FALSE, TRUE, NO_CHARACTER)
					
					FailFlags[MISSION_FAIL_CAR_DEAD] = TRUE
					FailFlags[MISSION_FAIL_CAR_UNDRIVABLE] = TRUE
					FailFlags[MISSION_FAIL_CAR_LEFT_BEHIND] = TRUE
					
					SET_VEHICLE_AS_RESTRICTED(vsPlayersCar.VehicleIndex, 0)
					
					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						SET_VEHICLE_STRONG(vsPlayersCar.VehicleIndex, FALSE)
						SET_VEHICLE_TYRES_CAN_BURST(vsPlayersCar.VehicleIndex, FALSE)
					ENDIF
					
					iSetupProgress++
					
				ENDIF
				
			BREAK
			
			#IF IS_DEBUG_BUILD
			
				CASE MISSION_STAGE_EJECTOR_TEST
				
					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsPlayersCar, FALSE, TRUE, NO_CHARACTER)
					
						IF HAS_MISSION_PED_BEEN_CREATED(psActress, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE, FALSE, vsPlayersCar.VehicleIndex, VS_FRONT_RIGHT)
						
							iSetupProgress++
							
						ENDIF
						
					ENDIF
				
				BREAK
			
			#ENDIF
		
		ENDSWITCH
		
	ENDIF
	
	//handle player's outfit
	IF ( iSetupProgress = 5 )
	
		SWITCH eStage

			CASE MISSION_STAGE_GET_TO_ACTOR
			CASE MISSION_STAGE_CUTSCENE_TRAILER
			
				IF IS_REPEAT_PLAY_ACTIVE()
				OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_TUXEDO)
					SET_PLAYER_PED_OUTFIT_TO_TUXEDO(FALSE)
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GET_TO_CAR
			
				bPlayerWearingTuxedo = TRUE
				SET_PLAYER_PED_OUTFIT_TO_TUXEDO(TRUE)
				
				IF NOT IS_BIT_SET(g_iCarSteal3AgentBitSet, PLAYER_GOT_TUXEDO)
					SET_GLOBAL_BIT_SET(PLAYER_GOT_TUXEDO)
				ENDIF
				
			BREAK
			
			DEFAULT
				IF IS_BIT_SET(g_iCarSteal3AgentBitSet, PLAYER_GOT_TUXEDO)
				
					bPlayerWearingTuxedo = TRUE
					SET_PLAYER_PED_OUTFIT_TO_TUXEDO(TRUE)
					
				ELSE
				
					bPlayerWearingTuxedo = FALSE
					SET_PLAYER_PED_OUTFIT_TO_TUXEDO(FALSE)
				ENDIF
			BREAK
			
		ENDSWITCH
		
		iSetupProgress++
	
	ENDIF
		
	//handle adding mission peds for dialogue depending on mission stage
	IF ( iSetupProgress = 6 )
		
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 0)	//Devin/Toilet ped
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 1)	//Franklin
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 2)	//Molly
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 3)	//Actress
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 6)	//Actor
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 7)	//Film director
		
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 4)	//Meltdown scene actor A
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 5)	//Meltdown scene actor B
		REMOVE_PED_FOR_DIALOGUE(CST4Conversation, 8)	//Meltodown scene director
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(CST4Conversation, 1, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF
		
		SWITCH eStage

			CASE MISSION_STAGE_GET_TO_ACTOR
				
				ADD_PED_FOR_DIALOGUE(CST4Conversation, 2, NULL, "MOLLY")
				
				IF NOT IS_PED_INJURED(psActor.PedIndex)
					ADD_PED_FOR_DIALOGUE(CST4Conversation, 6, psActor.PedIndex, "CST4ACTOR")
				ENDIF

			BREAK
			
			CASE MISSION_STAGE_GET_TO_CAR
			
				IF NOT IS_PED_INJURED(psActress.PedIndex)
					ADD_PED_FOR_DIALOGUE(CST4Conversation, 3, psActress.PedIndex, "CST4ACTRESS")
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_ESCAPE_FILM_SET
			
				ADD_PED_FOR_DIALOGUE(CST4Conversation, 2, NULL, "MOLLY")
				
				IF NOT IS_PED_INJURED(psActress.PedIndex)
					ADD_PED_FOR_DIALOGUE(CST4Conversation, 3, psActress.PedIndex, "CST4ACTRESS")
				ENDIF
				
				IF NOT IS_PED_INJURED( psFilmCrew[FCP_DIRECTOR].PedIndex)
					ADD_PED_FOR_DIALOGUE(CST4Conversation, 7, psFilmCrew[FCP_DIRECTOR].PedIndex, "CST4DIRECTOR")
				ENDIF
				
			BREAK
			
			#IF IS_DEBUG_BUILD
				
				CASE MISSION_STAGE_EJECTOR_TEST
				
					IF NOT IS_PED_INJURED(psActress.PedIndex)
						ADD_PED_FOR_DIALOGUE(CST4Conversation, 3, psActress.PedIndex, "CST4ACTRESS")
					ENDIF
				
				BREAK
				
			#ENDIF

		ENDSWITCH
		
		iSetupProgress++	
		
	ENDIF
	
	IF ( iSetupProgress = 7 )
		
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)					//all shops available by default
		
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, 	TRUE)	//mod shops are not available on this mission
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
		
		REGISTER_STUDIO_GATES()
		LOCK_STUDIO_GATES()
		
		SET_MAX_WANTED_LEVEL(5)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_CREATE_RANDOM_COPS(TRUE)
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		
		SWITCH eStage
		
			CASE MISSION_STAGE_GET_TO_ACTOR			
			CASE MISSION_STAGE_CUTSCENE_TRAILER
			CASE MISSION_STAGE_GET_TO_CAR
				SET_IGNORE_NO_GPS_FLAG(TRUE)
				
				SET_MAX_WANTED_LEVEL(0)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_CREATE_RANDOM_COPS(FALSE)
				
			BREAK
			
			CASE MISSION_STAGE_ESCAPE_FILM_SET
				SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)						//all shops are unavailable
				
				SET_IGNORE_NO_GPS_FLAG(TRUE)
				
				SET_MAX_WANTED_LEVEL(0)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_CREATE_RANDOM_COPS(FALSE)
				
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				
			BREAK
			
			CASE MISSION_STAGE_DELIVER_CAR
			CASE MISSION_STAGE_CUTSCENE_END
				SET_IGNORE_NO_GPS_FLAG(FALSE)
			BREAK
				
		ENDSWITCH
		
		//handle garage doors
		SWITCH eStage
			CASE MISSION_STAGE_GET_TO_ACTOR
			CASE MISSION_STAGE_CUTSCENE_TRAILER
			CASE MISSION_STAGE_GET_TO_CAR
				SET_DOOR_STATE(DOORNAME_CARSTEAL_GARAGE_S, DOORSTATE_LOCKED)
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID )
					SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID , TRUE)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_SCRIPT_UPDATE_DOOR_AUDIO for AUTODOOR_HAYES_GARAGE.")
					#ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_ESCAPE_FILM_SET
				SET_DOOR_STATE(DOORNAME_CARSTEAL_GARAGE_S, DOORSTATE_LOCKED)
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID )
					SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID , TRUE)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_SCRIPT_UPDATE_DOOR_AUDIO for AUTODOOR_HAYES_GARAGE.")
					#ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_DELIVER_CAR
			CASE MISSION_STAGE_CUTSCENE_END
				SET_DOOR_STATE(DOORNAME_CARSTEAL_GARAGE_S, DOORSTATE_LOCKED)
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID )
					SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID , TRUE)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_SCRIPT_UPDATE_DOOR_AUDIO for AUTODOOR_HAYES_GARAGE.")
					#ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		iSetupProgress++
		
	ENDIF
	
	//handle initial setup for skipping to or retrying specific stages
	IF ( iSetupProgress = 8 )
	
		IF 	( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
		
			SWITCH eStage
			
				CASE MISSION_STAGE_ESCAPE_FILM_SET
				
					INT i
				
					REQUEST_CLIP_SET("move_injured_generic")
					REQUEST_ANIM_DICT("misscarsteal4@director_grip")
					
					IF	HAS_CLIP_SET_LOADED("move_injured_generic")
					AND	HAS_ANIM_DICT_LOADED("misscarsteal4@director_grip")
					
						IF	HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_GRIP_1], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
						AND HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_GRIP_2], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
						AND HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_DIRECTOR], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
						
							IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osPropChair)
							AND HAS_MISSION_OBJECT_BEEN_CREATED(osPropClipboard)
							
								IF NOT IS_PED_INJURED(psFilmCrew[FCP_DIRECTOR].PedIndex)
									SET_PED_PROP_INDEX(psFilmCrew[FCP_DIRECTOR].PedIndex, ANCHOR_EYES, 0, 0)
									ADD_PED_FOR_DIALOGUE(CST4Conversation, 7, psFilmCrew[FCP_DIRECTOR].PedIndex, "CST4DIRECTOR")
								ENDIF
								
								IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_1].PedIndex)						
									SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_1].PedIndex, PED_COMP_HEAD, 	1, 1, 0) //(head)
									SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_1].PedIndex, PED_COMP_TORSO, 	1, 1, 0) //(uppr)
									SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_1].PedIndex, PED_COMP_LEG, 		0, 1, 0) //(lowr)
									SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_1].PedIndex, PED_COMP_SPECIAL, 	2, 0, 0) //(accs)
								ENDIF
							
								IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_2].PedIndex)
									SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_2].PedIndex, PED_COMP_HEAD, 	0, 1, 0) //(head)
									SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_2].PedIndex, PED_COMP_TORSO, 	0, 2, 0) //(uppr)
									SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_2].PedIndex, PED_COMP_LEG, 		0, 2, 0) //(lowr)
									SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_2].PedIndex, PED_COMP_SPECIAL, 	0, 0, 0) //(accs)
								ENDIF
							
								IF DOES_ENTITY_EXIST(osPropClipboard.ObjectIndex)
									IF NOT IS_ENTITY_DEAD(osPropClipboard.ObjectIndex)
										IF NOT IS_ENTITY_ATTACHED(osPropClipboard.ObjectIndex)
											IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_1].PedIndex)
												ATTACH_ENTITY_TO_ENTITY(osPropClipboard.ObjectIndex, psFilmCrew[FCP_GRIP_1].PedIndex,
																		GET_PED_BONE_INDEX(psFilmCrew[FCP_GRIP_1].PedIndex, BONETAG_PH_L_HAND),
																		<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF DOES_ENTITY_EXIST(psFilmCrew[FCP_GRIP_1].PedIndex)
									IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_1].PedIndex)
										IF NOT IS_ENTITY_PLAYING_ANIM(psFilmCrew[FCP_GRIP_1].PedIndex, "misscarsteal4@director_grip", "end_loop_grip")
											TASK_PLAY_ANIM_ADVANCED(psFilmCrew[FCP_GRIP_1].PedIndex, "misscarsteal4@director_grip", "end_loop_grip",
																	osPropChair.vPosition, osPropChair.vRotation, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1,
																	AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
																	AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 
																	0, EULER_YXZ, AIK_DISABLE_ARM_IK)
										ENDIF
									ENDIF
								ENDIF

								IF DOES_ENTITY_EXIST(psFilmCrew[FCP_GRIP_2].PedIndex)
									IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_2].PedIndex)
										IF NOT IS_ENTITY_PLAYING_ANIM(psFilmCrew[FCP_GRIP_2].PedIndex, "misscarsteal4@director_grip", "mcs_2_loop_grip1")
											TASK_PLAY_ANIM_ADVANCED(psFilmCrew[FCP_GRIP_2].PedIndex, "misscarsteal4@director_grip", "mcs_2_loop_grip1", vFilmCrewPedsScenePosition,
																	vFilmCrewPedsSceneRotation, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET)
										ENDIF
									ENDIF
								ENDIF
								
								IF DOES_ENTITY_EXIST(psFilmCrew[FCP_DIRECTOR].PedIndex)
									IF NOT IS_PED_INJURED(psFilmCrew[FCP_DIRECTOR].PedIndex)
										SET_PED_PROP_INDEX(psFilmCrew[FCP_DIRECTOR].PedIndex, ANCHOR_EYES, 0, 0)
										IF NOT IS_ENTITY_PLAYING_ANIM(psFilmCrew[FCP_DIRECTOR].PedIndex, "misscarsteal4@director_grip", "end_loop_director")
											TASK_PLAY_ANIM_ADVANCED(psFilmCrew[FCP_DIRECTOR].PedIndex, "misscarsteal4@director_grip", "end_loop_director",
																	osPropChair.vPosition, osPropChair.vRotation, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1,
																	AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
																	AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 
																	0, EULER_YXZ, AIK_DISABLE_LEG_IK)					
										ENDIF
									ENDIF
								ENDIF
								
								GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_1], PED_STATE_PLAYING_ANIM_3)
								GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_2], PED_STATE_BLOCKING_PLAYER)
								GO_TO_PED_STATE(psFilmCrew[FCP_DIRECTOR], PED_STATE_PLAYING_ANIM_3)
								
								IF 	DOES_ENTITY_EXIST(psSetSecurity[0].PedIndex)
								AND DOES_ENTITY_EXIST(psSetSecurity[1].PedIndex)
								AND DOES_ENTITY_EXIST(psSetSecurity[2].PedIndex)
								AND DOES_ENTITY_EXIST(psSetSecurity[3].PedIndex)
								
									REPEAT 4 i
										IF NOT IS_ENTITY_DEAD(psSetSecurity[i].PedIndex)
											TASK_AIM_GUN_AT_ENTITY(psSetSecurity[i].PedIndex, PLAYER_PED_ID(), -1, TRUE)
											GO_TO_PED_STATE(psSetSecurity[i], PED_STATE_COMBAT_AIMING)
										ENDIF
									ENDREPEAT
								
									iSetupProgress++
								
								ELSE
								
									REPEAT COUNT_OF(psSetSecurity) i
										IF NOT DOES_ENTITY_EXIST(psSetSecurity[i].PedIndex)
											REQUEST_MODEL(mnSecurityPed)
											IF HAS_MODEL_LOADED(mnSecurityPed)
												psSetSecurity[i].PedIndex = CREATE_ENEMY_PED(psSetSecurity[i].vPosition, psSetSecurity[i].fHeading, mnSecurityPed,
																							 RELGROUPHASH_SECURITY_GUARD, 200, WEAPONTYPE_PISTOL 
																							 #IF IS_DEBUG_BUILD, psSetSecurity[i].sDebugName #ENDIF)
												GO_TO_PED_STATE(psSetSecurity[i], PED_STATE_IDLE)
												SET_MODEL_AS_NO_LONGER_NEEDED(mnSecurityPed)
											ENDIF
										ENDIF
									ENDREPEAT

								ENDIF
							
							ENDIF
						
						ENDIF
					
					ENDIF
				
				BREAK
			
				DEFAULT
					iSetupProgress++
				BREAK
			
			ENDSWITCH
		
		ELSE
		
			iSetupProgress++
		
		ENDIF
	
	ENDIF
	
	//handle player positioning when current stage is being skipped to or played normally
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 9 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
								
			IF ( bStageReplayed = TRUE )
		
				END_REPLAY_SETUP(NULL, DEFAULT, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling END_REPLAY_SETUP().")
				#ENDIF
				
			ELSE
			
				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20.0)
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START_SPHERE() at coordinates ", sMissionPosition.vPosition, " for mission replay or stage skip. This might take a while to load.")
				#ENDIF
				
				INT iLoadSceneTimer = GET_GAME_TIMER() + 15000

				WHILE IS_NEW_LOAD_SCENE_ACTIVE()
				AND NOT IS_NEW_LOAD_SCENE_LOADED()
				AND GET_GAME_TIMER() < iLoadSceneTimer
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Load scene timer: ", iLoadSceneTimer - GET_GAME_TIMER(), ".")
					#ENDIF

					WAIT(0)
					  
				ENDWHILE

				NEW_LOAD_SCENE_STOP()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP().")
				#ENDIF
			
			ENDIF

			WAIT(500)	//add wait to hide any load scene freezes
			
			//put player in car when required during the skip
			SWITCH eStage
			
				CASE MISSION_STAGE_ESCAPE_FILM_SET
				CASE MISSION_STAGE_DELIVER_CAR
				CASE MISSION_STAGE_CUTSCENE_END
				#IF IS_DEBUG_BUILD
					CASE MISSION_STAGE_EJECTOR_TEST
				#ENDIF

					IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex, VS_DRIVER)
							SET_VEHICLE_ON_GROUND_PROPERLY(vsPlayersCar.VehicleIndex)
							SET_VEHICLE_ENGINE_ON(vsPlayersCar.VehicleIndex, TRUE, TRUE)
							
							ACTIVATE_PHYSICS(vsPlayersCar.VehicleIndex)
							SET_ENTITY_DYNAMIC(vsPlayersCar.VehicleIndex, TRUE)
							
						ENDIF
					ENDIF
				
				BREAK
			
			ENDSWITCH
		
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			
			SWITCH eStage								//handle music for skips/retry/replay of mission stages
				CASE MISSION_STAGE_CUTSCENE_TRAILER
					TRIGGER_MUSIC_EVENT("CAR3_TRAILER_RESTART")
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_3_INSIDE_STUDIO")
						START_AUDIO_SCENE("CAR_3_INSIDE_STUDIO")
					ENDIF
				BREAK
				CASE MISSION_STAGE_GET_TO_CAR
					TRIGGER_MUSIC_EVENT("CAR3_CAR_RESTART")
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_3_INSIDE_STUDIO")
						START_AUDIO_SCENE("CAR_3_INSIDE_STUDIO")
					ENDIF
				BREAK
				CASE MISSION_STAGE_ESCAPE_FILM_SET
					TRIGGER_MUSIC_EVENT("CAR3_ESCAPE_RESTART")
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_3_ESCAPE_SECURITY")
						START_AUDIO_SCENE("CAR_3_ESCAPE_SECURITY")
					ENDIF
				BREAK
				CASE MISSION_STAGE_DELIVER_CAR
					TRIGGER_MUSIC_EVENT("CAR3_DELIVER_RESTART")
				BREAK
			ENDSWITCH

			SWITCH eStage								//handle fade in for each mission stage
			
				CASE MISSION_STAGE_CUTSCENE_TRAILER		//do not fade in, mission stage will handle a fade in from fade out
				CASE MISSION_STAGE_CUTSCENE_END
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Halting loading screen fade in for skip to a mission stage that will fade the screen in.")
					#ENDIF
					
				BREAK
			
				DEFAULT									//fade in by default for other stages
								
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling a loading screen fade in.")
					#ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				
				BREAK

			ENDSWITCH
				
		ENDIF
		
		iSetupProgress++
	
	ENDIF
	
	
	//handle setting of mid-mission replay checkpoints
	IF ( iSetupProgress = 10 )
	
		SWITCH eStage
		
			CASE MISSION_STAGE_GET_TO_ACTOR
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_ACTOR), "GET TO ACTOR", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_ACTOR), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GET_TO_CAR
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_CAR), "GET TO CAR", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_CAR), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_ESCAPE_FILM_SET
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_ESCAPE_FILM_SET), "ESCAPE FILM SET", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_ESCAPE_FILM_SET), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_DELIVER_CAR
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_DELIVER_CAR), "DELIVER CAR", TRUE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_DELIVER_CAR), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
		ENDSWITCH
	
		iSetupProgress++
	
	ENDIF
	
	//print the message only once per mission stage
	//handle fades
	IF ( iSetupProgress = 11 )
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission stage loading for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		//reset the stage reset flag
		#IF IS_DEBUG_BUILD
			bStageResetFlag = FALSE
		#ENDIF
		
		bStageLoaded 	= TRUE
		bStageReplayed 	= FALSE
		bStageSkippedTo = FALSE
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		iSetupProgress++
		
	ENDIF
	
	IF ( iSetupProgress = 12 )

		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE	

ENDFUNC

//|======================== END MISSION STAGE LOADING PROCEDURES ========================|

//|=============================== MISSION STAGES FUNCTIONS ==============================|

FUNC BOOL CAN_PED_HEAR_PED_ON_FOOT(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, FLOAT fDistanceBetweenPeds)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT IS_PED_INJURED(TargetPedIndex)
					IF NOT IS_PED_IN_ANY_VEHICLE(TargetPedIndex)
					
						IF CAN_PED_HEAR_PLAYER(PLAYER_ID(), PedIndex)
						
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped heard player heard on foot.")
							#ENDIF
						
							IF IS_PED_RUNNING(TargetPedIndex)
							OR IS_PED_SPRINTING(TargetPedIndex)
								IF fDistanceBetweenPeds <= 5.0
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped heard player heard on foot running.")
									#ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
							
							IF IS_PED_WALKING(TargetPedIndex)
								IF fDistanceBetweenPeds <= 3.0
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped heard player heard on foot walking.")
									#ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

							

ENDFUNC

FUNC BOOL CAN_PED_HEAR_PED_IN_VEHICLE(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, FLOAT fDistanceBetweenPeds)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT IS_PED_INJURED(TargetPedIndex)
					IF IS_PED_IN_ANY_VEHICLE(TargetPedIndex)
						
						IF ( fDistanceBetweenPeds < 20.0 )
							IF IS_HORN_ACTIVE(GET_VEHICLE_PED_IS_USING(TargetPedIndex))
							OR IS_VEHICLE_SIREN_ON(GET_VEHICLE_PED_IS_USING(TargetPedIndex))
							OR IS_VEHICLE_ALARM_ACTIVATED(GET_VEHICLE_PED_IS_USING(TargetPedIndex))
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player heard in vehicle with horn, siren or alarm active.")
								#ENDIF
								RETURN TRUE
							ENDIF
						
							IF IS_VEHICLE_IN_BURNOUT(GET_VEHICLE_PED_IS_USING(TargetPedIndex))
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player heard in vehicle doing burnout.")
								#ENDIF
								RETURN TRUE
							ENDIF
						ENDIF
						
						IF ( fDistanceBetweenPeds < 15.0 )
							IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_USING(TargetPedIndex)) AND GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_USING(TargetPedIndex)) > 5.0
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player heard in vehicle crashing.")
								#ENDIF
								RETURN TRUE
							ENDIF
						
							IF CAN_PED_HEAR_PLAYER(PLAYER_ID(), PedIndex)
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player heard in vehicle.")
								#ENDIF
								RETURN TRUE
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_SECURITY_PED_ALERTED_BY_PED_VEHICLE_ACTIONS(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, FLOAT fDistanceBetweenPeds, BOOL bCanSeePed)
	
	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT IS_PED_INJURED(TargetPedIndex)
					IF IS_PED_IN_ANY_VEHICLE(TargetPedIndex)
						
						IF ( fDistanceBetweenPeds < 40.0 )
							IF IS_VEHICLE_ALARM_ACTIVATED(GET_VEHICLE_PED_IS_USING(TargetPedIndex))
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped alerted due to player using vehicle with alarm active.")
								#ENDIF
								RETURN TRUE
							ENDIF
						ENDIF
						
						IF ( fDistanceBetweenPeds < 5.0 )					
							IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_USING(TargetPedIndex)) AND GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_USING(TargetPedIndex)) > 5.0
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped alerted due to player crashing a vehicle nearby ped.")
								#ENDIF
								RETURN TRUE
							ENDIF
						ENDIF
						
						IF ( bPlayerInsideFilmStudio = TRUE )
							IF ( bCanSeePed = TRUE )
								IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(TargetPedIndex)))
								OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(TargetPedIndex)))
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped alerted due to player seen using helicopter or plane inside film studio.")
									#ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_ALERTED_BY_PED_ACTIONS(PED_INDEX PedIndex, PED_INDEX TargetPedIndex)

	IF 	NOT IS_PED_INJURED(PedIndex)
	AND NOT IS_PED_INJURED(TargetPedIndex)
	
		VECTOR PedCoords = GET_ENTITY_COORDS(PedIndex)
	
		IF HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_GUN_AIMED_AT)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_POTENTIAL_BLAST)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
		
		OR IS_BULLET_IN_AREA(PedCoords, 16.0, TRUE)
		OR IS_BULLET_IN_AREA(PedCoords, 16.0, FALSE)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, PedCoords, 16.0)
		
		OR IS_ENTITY_TOUCHING_ENTITY(PedIndex, TargetPedIndex)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(PedIndex, TargetPedIndex)
		
			RETURN TRUE
	
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(PED_INDEX PedIndex, PED_INDEX TargetPedIndex)

	IF 	NOT IS_PED_INJURED(PedIndex)
	AND NOT IS_PED_INJURED(TargetPedIndex)
	
		VECTOR PedCoords = GET_ENTITY_COORDS(PedIndex)
	
		IF HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_GUN_AIMED_AT)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_POTENTIAL_BLAST)
		
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOT_FIRED)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
		
		OR IS_BULLET_IN_AREA(PedCoords, 16.0, TRUE)
		OR IS_BULLET_IN_AREA(PedCoords, 16.0, FALSE)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, PedCoords, 16.0)
		
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(PedIndex, TargetPedIndex)
		
			RETURN TRUE
	
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_SECURITY_PED_ALERTED_BY_PED_ACTIONS(PED_INDEX PedIndex, PED_INDEX TargetPedIndex)

	IF 	NOT IS_PED_INJURED(PedIndex)
	AND NOT IS_PED_INJURED(TargetPedIndex)
	
		VECTOR PedCoords = GET_ENTITY_COORDS(PedIndex)
	
		IF HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_MELEE_ACTION)
	
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_GUN_AIMED_AT)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_POTENTIAL_BLAST)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_POTENTIAL_GET_RUN_OVER)
		
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_EXPLOSION)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOCKING_EXPLOSION)
		
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOCKING_CAR_ALARM)
		
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOT_FIRED)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
		OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
		
		OR IS_BULLET_IN_AREA(PedCoords, 20.0, TRUE)
		OR IS_BULLET_IN_AREA(PedCoords, 20.0, FALSE)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, PedCoords, 20.0)
		
		OR IS_ENTITY_TOUCHING_ENTITY(PedIndex, TargetPedIndex)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(PedIndex, TargetPedIndex)
		
			RETURN TRUE
	
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_PED_SEE_PED_WEAPON(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, FLOAT fDistanceBetweenPeds, BOOL bCanSeePed)

	IF 	NOT IS_PED_INJURED(PedIndex)
	AND NOT IS_PED_INJURED(TargetPedIndex)
	
		IF ( fDistanceBetweenPeds < 20.0 AND bCanSeePed = TRUE )
		
			WEAPON_TYPE eCurrentPlayerWeapon
			 
			IF IS_PED_SHOOTING(TargetPedIndex)
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), PedIndex)
			OR HAS_PED_RECEIVED_EVENT(PedIndex, EVENT_GUN_AIMED_AT)
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), PedIndex)
			OR GET_CURRENT_PED_WEAPON(TargetPedIndex, eCurrentPlayerWeapon)
				
				RETURN TRUE
			
			ENDIF
		
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, FLOAT fDistanceBetweenPeds, BOOL bCanSeePed)

	IF 	NOT IS_PED_INJURED(PedIndex)
	AND NOT IS_PED_INJURED(TargetPedIndex)
	
		IF ( fDistanceBetweenPeds < 25.0 AND bCanSeePed = TRUE )
			IF NOT IS_PED_DEAD_OR_DYING(PedIndex)
				IF IS_PED_PERFORMING_MELEE_ACTION(TargetPedIndex)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_PED_SEE_PED_DAMAGING_VEHICLE(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, VEHICLE_INDEX TargetVehicleIndex, BOOL bCanSeePed)

	IF 	NOT IS_PED_INJURED(PedIndex)
	AND NOT IS_PED_INJURED(TargetPedIndex)
		IF 	DOES_ENTITY_EXIST(TargetVehicleIndex)
		AND NOT IS_ENTITY_DEAD(TargetVehicleIndex)
		
			IF ( bCanSeePed = TRUE )
				
				IF IS_PED_ON_SPECIFIC_VEHICLE(TargetPedIndex, TargetVehicleIndex)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(TargetVehicleIndex, TargetPedIndex)
					RETURN TRUE
				ENDIF
				
			ENDIF
		
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_PED_SEE_PED_IN_STEALTH_MOVEMENT(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, FLOAT fDistanceBetweenPeds, BOOL bCanSeePed)

	IF 	NOT IS_PED_INJURED(PedIndex)
	AND NOT IS_PED_INJURED(TargetPedIndex)
	
		IF ( fDistanceBetweenPeds < 10.0 AND bCanSeePed = TRUE )
			
			IF GET_PED_STEALTH_MOVEMENT(TargetPedIndex)
				
				RETURN TRUE
				
			ENDIF
			
		ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MANAGE_TOILET_PED(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
	
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
		
		#IF IS_DEBUG_BUILD
			ped.sDebugName = "Toilet"
			IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				SET_PED_NAME_DEBUG(ped.PedIndex, "Toilet")
			ENDIF
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
							
				VECTOR	vAnimPosition	= << -1160.548, -528.440, 31.585 >>
				VECTOR	vAnimRotation	= << 0.000, 0.000, 133.920 >>
				
				IF ( ped.eState != PED_STATE_FLEEING )
					IF IS_ENTITY_TOUCHING_ENTITY(ped.PedIndex, TargetPedIndex)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped.PedIndex, TargetPedIndex)			
					OR HAS_PED_RECEIVED_EVENT(ped.PedIndex, EVENT_SHOT_FIRED)
					OR HAS_PED_RECEIVED_EVENT(ped.PedIndex, EVENT_GUN_AIMED_AT)
					OR HAS_PED_RECEIVED_EVENT(ped.PedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(ped.PedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
						GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
					ENDIF
				ENDIF
				
				SWITCH ped.eState
				
					CASE PED_STATE_IDLE
					
						IF ( ped.bHasTask = FALSE )
							//ad initial tasks and settings here
							ped.bHasTask = TRUE
						ENDIF

						GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_1)
					
					BREAK
				
					CASE PED_STATE_PLAYING_ANIM_1
					CASE PED_STATE_PLAYING_ANIM_2
					CASE PED_STATE_PLAYING_ANIM_3
					CASE PED_STATE_PLAYING_ANIM_4
					
						IF ( ped.bHasTask = FALSE )
						
							SWITCH ped.eState
							
								CASE PED_STATE_PLAYING_ANIM_1
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_base_idle")						
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
										TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_base_idle",
															vAnimPosition, vAnimRotation, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
															AF_EXTRACT_INITIAL_OFFSET | AF_HOLD_LAST_FRAME)
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_2
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_a")	
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
										TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_a",
															vAnimPosition, vAnimRotation, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
															AF_EXTRACT_INITIAL_OFFSET | AF_HOLD_LAST_FRAME)
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_3
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_b")
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
										TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_b",
																vAnimPosition, vAnimRotation, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
																AF_EXTRACT_INITIAL_OFFSET | AF_HOLD_LAST_FRAME)
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_4
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_c")
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
										TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_c",
																vAnimPosition, vAnimRotation, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
																AF_EXTRACT_INITIAL_OFFSET | AF_HOLD_LAST_FRAME)
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								BREAK
							
							ENDSWITCH
							
						ENDIF						
						
						
						IF ( ped.bHasTask = TRUE )
						
							WEAPON_TYPE TargetPedWeaponType
						
							IF ( eMissionStage = MISSION_STAGE_GET_TO_ACTOR )
							AND GET_CURRENT_PED_WEAPON(TargetPedIndex, TargetPedWeaponType)
							AND TargetPedWeaponType = WEAPONTYPE_STUNGUN
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
							ELSE
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							ENDIF
						
							SWITCH ped.eState
							
								CASE PED_STATE_PLAYING_ANIM_1
									IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_base_idle")
										IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_base_idle") >= 1.0
											SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
												CASE 0
													GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_2)
												BREAK
												CASE 1
													GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_3)
												BREAK
												CASE 2
													GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_4)
												BREAK
											ENDSWITCH
										ENDIF
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_2
								CASE PED_STATE_PLAYING_ANIM_3
								CASE PED_STATE_PLAYING_ANIM_4
									IF ( IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_a") AND GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_a") >= 1.0 )
									OR ( IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_b") AND GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_b") >= 1.0 )
									OR ( IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_c") AND GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_c") >= 1.0 )
										GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_1)
									ENDIF
								BREAK
							
							ENDSWITCH
							
							SWITCH ped.eState
								CASE PED_STATE_PLAYING_ANIM_1
									IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_base_idle")
										IF HAS_LABEL_BEEN_TRIGGERED("CST4_TOILET")
											SET_LABEL_AS_TRIGGERED("CST4_TOILET", FALSE)
										ENDIF
									ENDIF
								BREAK
								CASE PED_STATE_PLAYING_ANIM_2
								CASE PED_STATE_PLAYING_ANIM_3
								CASE PED_STATE_PLAYING_ANIM_4
									IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_a")
									OR IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_b")
									OR IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@toilet", "desperate_toilet_idle_c")									
										IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_TOILET")
											IF IS_ENTITY_AT_COORD(TargetPedIndex, << -1151.53, -535.51, 42.63 >>, << 20.0, 16.0, 16.0 >>) AND ( ped.fDistanceToTarget < 20.0 )
												SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
													CASE 0
														IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
															IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_TOILET", "CST4_TOILET_1", CONV_PRIORITY_MEDIUM)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ELSE
															IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
																PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "CST4_BSAA", "CST4RUNNER3", SPEECH_PARAMS_FORCE_FRONTEND)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ENDIF
													BREAK													
													CASE 1
														IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
															IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_TOILET", "CST4_TOILET_3", CONV_PRIORITY_MEDIUM)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ELSE
															IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
																PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "CST4_BSAB",  "CST4RUNNER3", SPEECH_PARAMS_FORCE_FRONTEND)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ENDIF
													BREAK
													CASE 2
														IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
															IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_TOILET", "CST4_TOILET_5", CONV_PRIORITY_MEDIUM)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ELSE
															IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
																PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "CST4_BSAC",  "CST4RUNNER3", SPEECH_PARAMS_FORCE_FRONTEND)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ENDIF
													BREAK
													CASE 3
														IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
															IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_TOILET", "CST4_TOILET_7", CONV_PRIORITY_MEDIUM)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ELSE
															IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
																PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "CST4_BSAD",  "CST4RUNNER3", SPEECH_PARAMS_FORCE_FRONTEND)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ENDIF
													BREAK
													CASE 4
														IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
															IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_TOILET", "CST4_TOILET_9", CONV_PRIORITY_MEDIUM)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ELSE
															IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
																PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "CST4_BSAE",  "CST4RUNNER3", SPEECH_PARAMS_FORCE_FRONTEND)
																SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
															ENDIF
														ENDIF
													BREAK
												ENDSWITCH
											ELSE
												SET_LABEL_AS_TRIGGERED("CST4_TOILET", TRUE)
											ENDIF
										ELSE
											IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF IS_SCRIPTED_SPEECH_PLAYING(ped.PedIndex)
													IF IS_PED_DEAD_OR_DYING(ped.PedIndex)
														KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						
						ENDIF
					
					BREAK
					
					CASE PED_STATE_FLEEING
						
						IF ( ped.bHasTask = FALSE )
						
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
							
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
							
							IF NOT IS_PED_FLEEING(ped.PedIndex)
								CLEAR_PED_TASKS(ped.PedIndex)
								TASK_SMART_FLEE_PED(ped.PedIndex, TargetPedIndex, 300, -1)
								SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
							ENDIF
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iConversationTimer = 0
							ped.bHasTask = TRUE
						ENDIF
						
						IF GET_GAME_TIMER() - ped.iConversationTimer > 0
							IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "SCREAM_PANIC", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
								ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 7000)
							ENDIF
						ENDIF
						
					BREAK
					
				ENDSWITCH
			
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING	//cleanup ped when ped is not dead or fleeing
		
			IF ( bPlayerInsideFilmStudio = FALSE )
				IF ( ped.fDistanceToTarget > 200.0 )
					
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					
					REMOVE_ANIM_DICT("misscarsteal4@toilet")
					
				ENDIF
			ENDIF
		
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING		//cleanup ped when ped is dead or fleeing
		
			IF IS_ENTITY_DEAD(ped.PedIndex)
			
				ped.eState = PED_STATE_DEAD
			
				IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
					ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
					UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
				ENDIF
			ENDIF
		
			IF ( ped.fDistanceToTarget > 200.0 )
			
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
				
				REMOVE_ANIM_DICT("misscarsteal4@toilet")
				
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MANAGE_TOILET_SCENE(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			iProgress++
		
		BREAK
		
		CASE 1

			REQUEST_MODEL(osPropToilet.ModelName)
			REQUEST_ANIM_DICT("misscarsteal4@toilet")

			IF 	HAS_MODEL_LOADED(osPropToilet.ModelName)
			AND HAS_ANIM_DICT_LOADED("misscarsteal4@toilet")
		
				IF	HAS_MISSION_PED_BEEN_CREATED(psToilet, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
				
					IF NOT IS_PED_INJURED(psToilet.PedIndex)
						ADD_PED_FOR_DIALOGUE(CST4Conversation, 0, psToilet.PedIndex, "CST4RUNNER3")
					ENDIF
					
					CREATE_MODEL_HIDE(osPropToilet.vPosition, 1.0, osPropToilet.ModelName, FALSE)
					
					IF HAS_MISSION_OBJECT_BEEN_CREATED(osPropToilet, TRUE)					
						//
					ENDIF
				
					GO_TO_PED_STATE(psToilet, PED_STATE_IDLE)
				
					iProgress++
				
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			MANAGE_TOILET_PED(psToilet, PLAYER_PED_ID())
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC BLOCK_WEAPON_FIRE_FOR_AIMING_SECURITY_GROUP(PED_STRUCT &array[])

	INT i
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		IF DOES_ENTITY_EXIST(array[i].PedIndex)
			IF NOT IS_ENTITY_DEAD(array[i].PedIndex)
				IF array[i].eState = PED_STATE_COMBAT_AIMING
				OR array[i].eState = PED_STATE_COMBAT_AIMING_PED
					SET_PED_RESET_FLAG(array[i].PedIndex, PRF_BlockWeaponFire, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC MANAGE_SECURITY_PED_AT_FILM_SET(PED_STRUCT &ped, PED_INDEX TargetPedIndex, VEHICLE_INDEX VehicleIndex, STRING sVoiceName,
									 VECTOR vDefensiveSpherePosition, FLOAT fDefensiveSphereRadius, INT iCarAimingTime,
									 INT iPedAimingTime, INT iLOSTime, FLOAT fWalkWhenStrafingInCombatAimingChance,
									 BOOL bEntryGuard)

	IF DOES_ENTITY_EXIST(ped.PedIndex)

		IF 	NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
		
				#IF IS_DEBUG_BUILD
					IF ( bDrawDebugStates = TRUE )
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
					ENDIF
				#ENDIF
				
				ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
				CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, iLOSTime)
				
				SWITCH ped.eState
				
					CASE PED_STATE_IDLE
					
						GO_TO_PED_STATE(ped, PED_STATE_STANDING_AT_LOCATION)
						
					BREAK
					
					CASE PED_STATE_STANDING_AT_LOCATION
					
						IF ( ped.bHasTask = FALSE )
						
							TASK_START_SCENARIO_IN_PLACE(ped.PedIndex, "WORLD_HUMAN_GUARD_STAND")
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iConversationTimer = 0
							
							ped.bHasTask = TRUE
						ENDIF
					
						IF ( bEntryGuard = TRUE )
							
							WEAPON_TYPE TargetPedWeaponType
							
							IF ( eMissionStage = MISSION_STAGE_GET_TO_ACTOR )
							AND GET_CURRENT_PED_WEAPON(TargetPedIndex, TargetPedWeaponType)
							AND TargetPedWeaponType = WEAPONTYPE_STUNGUN
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
							ELSE
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							ENDIF
						ENDIF

						IF ( ped.bCanSeeTargetPed = TRUE )

							IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
								IF IS_ENTITY_AT_ENTITY(TargetPedIndex, VehicleIndex, << 2.5, 2.5, 2.5 >>, FALSE, TRUE, TM_ON_FOOT)
									GO_TO_PED_STATE(ped, PED_STATE_INTIMIDATION)
								ENDIF
							ENDIF
							
						ENDIF
					
						IF ( bSecurityAlerted = TRUE )
						OR ( bFilmCrewGripAlerted = TRUE)
						OR ( bDirectorSceneDisrupted = TRUE)
						OR IS_PED_IN_COMBAT(ped.PedIndex)
						OR IS_SECURITY_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_DAMAGING_VEHICLE(ped.PedIndex, TargetPedIndex, VehicleIndex, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR IS_SECURITY_PED_ALERTED_BY_PED_VEHICLE_ACTIONS(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
						ENDIF
						
						IF ( CST4_DSG_PLAYING_TRIGGERED = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_AIMING_PED)
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
						
							IF ( bEntryGuard = TRUE )
								SET_PED_CAN_PLAY_AMBIENT_ANIMS(ped.PedIndex, FALSE)
							ENDIF
						
							IF GET_GAME_TIMER() - ped.iConversationTimer > 0
								IF ( ped.fDistanceToTarget < 12.0 )
									IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
										IF ( bPlayerWearingTuxedo = TRUE )
											IF ( bEntryGuard = TRUE )
												TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 6000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "NEEDED_ON_SET", sVoiceName, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
												ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 15000)
											ENDIF
										ELSE
											TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 6000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "MOVE_AWAY_WARNING", sVoiceName, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
											ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 15000)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ELSE
						
							IF ( bEntryGuard = TRUE )
								SET_PED_CAN_PLAY_AMBIENT_ANIMS(ped.PedIndex, TRUE)
							ENDIF
													
						ENDIF

					BREAK

					CASE PED_STATE_INTIMIDATION
					
						IF ( ped.bHasTask = FALSE )
						
							REQUEST_ANIM_DICT("move_m@intimidation@cop@unarmed")
							REQUEST_ANIM_DICT("reaction@intimidation@cop@unarmed")
							
							IF 	HAS_ANIM_DICT_LOADED("move_m@intimidation@cop@unarmed")
							AND HAS_ANIM_DICT_LOADED("reaction@intimidation@cop@unarmed")
							
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, TargetPedIndex)
									TASK_LOOK_AT_ENTITY(NULL, TargetPedIndex, 100, SLF_FAST_TURN_RATE)
									TASK_PLAY_ANIM(NULL, "reaction@intimidation@cop@unarmed", "intro", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
									TASK_PLAY_ANIM(NULL, "move_m@intimidation@cop@unarmed", "idle", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								
								ped.iTimer = GET_GAME_TIMER()
								ped.bHasTask = TRUE
								
							ENDIF
							
						ENDIF

						IF ( bSecurityAlerted = TRUE )
						OR ( bFilmCrewGripAlerted = TRUE)
						OR ( bDirectorSceneDisrupted = TRUE)
						OR IS_PED_IN_COMBAT(ped.PedIndex)
						OR IS_SECURITY_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_DAMAGING_VEHICLE(ped.PedIndex, TargetPedIndex, VehicleIndex, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR IS_SECURITY_PED_ALERTED_BY_PED_VEHICLE_ACTIONS(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
						ENDIF
						
						IF ( CST4_DSG_PLAYING_TRIGGERED = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_AIMING_PED)
						ENDIF
					
					BREAK
					
					CASE PED_STATE_COMBAT_AIMING_PED
					
						SET_PED_RESET_FLAG(ped.PedIndex, PRF_BlockWeaponFire, TRUE)	//block weapon fire in aiming state each frame
					
						IF ( ped.bHasTask = FALSE )
						
							SET_PED_COMBAT_ABILITY(ped.PedIndex, CAL_PROFESSIONAL)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
							
							SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)
							
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iTimer = GET_GAME_TIMER()
							ped.iConversationTimer = 0
							
							ped.bHasTask = TRUE
							
						ENDIF
						
						IF ( ped.bHasTask = TRUE )
						
							IF ( bSecurityAlerted = TRUE )
							OR ( bFilmCrewGripAlerted = TRUE)
							OR ( bDirectorSceneDisrupted = TRUE)
							OR HAS_TIME_PASSED(iPedAimingTime, ped.iTimer)
							OR IS_SECURITY_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
							OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							OR CAN_PED_SEE_PED_DAMAGING_VEHICLE(ped.PedIndex, TargetPedIndex, VehicleIndex, ped.bCanSeeTargetPed)
							OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
								
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
								
							ENDIF
							
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
						
							IF GET_GAME_TIMER() - ped.iConversationTimer > 0
								IF ( ped.fDistanceToTarget < 12.0 )
									IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
										SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
											CASE 0
												TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "MOVE_AWAY_FROM_THE_CAR", sVoiceName, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
												ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											BREAK
											CASE 1
												TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "FREEZE", sVoiceName, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
												ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
											BREAK
										ENDSWITCH
									ENDIF
								ENDIF
							ENDIF
													
						ENDIF

					BREAK
					
					CASE PED_STATE_COMBAT_AIMING
					
						SET_PED_RESET_FLAG(ped.PedIndex, PRF_BlockWeaponFire, TRUE)	//block weapon fire in aiming state each frame
					
						IF ( ped.bHasTask = FALSE )
						
							REMOVE_ANIM_DICT("move_m@intimidation@cop@unarmed")
							REMOVE_ANIM_DICT("reaction@intimidation@cop@unarmed")
							
							SET_PED_COMBAT_ABILITY(ped.PedIndex, CAL_PROFESSIONAL)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
							
							SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
							SET_PED_SPHERE_DEFENSIVE_AREA(ped.PedIndex, vDefensiveSpherePosition, fDefensiveSphereRadius)
							
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, fWalkWhenStrafingInCombatAimingChance)
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iTimer = GET_GAME_TIMER()
							ped.iConversationTimer = 0
							ped.bHasTask = TRUE
						ENDIF
						
						IF ( ped.bHasTask = TRUE )
						
							IF ( bSecurityAlerted = TRUE )
							OR ( bFilmCrewGripAlerted = TRUE)
							OR ( bDirectorSceneDisrupted = TRUE)
							OR HAS_TIME_PASSED(iCarAimingTime, ped.iTimer)
							OR IS_SECURITY_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
							OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							OR CAN_PED_SEE_PED_DAMAGING_VEHICLE(ped.PedIndex, TargetPedIndex, VehicleIndex, ped.bCanSeeTargetPed)
							OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
								
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
								
							ENDIF
							
							IF GET_GAME_TIMER() - ped.iConversationTimer > 0
								IF ( ped.fDistanceToTarget < 15.0 )
									IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
										TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GET_OUT_OF_THE_CAR", sVoiceName, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
										ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					
					BREAK
					
					CASE PED_STATE_COMBAT_ON_FOOT
					
						IF ( ped.bHasTask = FALSE )
						
							REMOVE_ANIM_DICT("move_m@intimidation@cop@unarmed")
							REMOVE_ANIM_DICT("reaction@intimidation@cop@unarmed")
						
							REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
							
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)
							
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHASE_TARGET_ON_FOOT, TRUE)
							
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							IF ( ped.fDistanceToTarget < 25.0 )
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "FREEZE", sVoiceName, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
							ENDIF
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							bSecurityAlerted = TRUE
							
							ped.bHasTask = TRUE
						ENDIF
									
					BREAK
				
				ENDSWITCH
		
			ENDIF
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex)
			
			ped.eState = PED_STATE_DEAD
		
			IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
				ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
				UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
			ENDIF
			
		ENDIF
	
	ENDIF

ENDPROC

PROC MANAGE_SECURITY_PED_AT_GATE(PED_STRUCT &ped, STRING sVoiceName, PED_INDEX TargetPedIndex, VEHICLE_INDEX TargetVehicleIndex,
								 VECTOR vGateAreaPosition1, VECTOR vGateAreaPosition2, FLOAT fGateAreaWidth,
								 VECTOR vDefensiveAreaPosition1, VECTOR vDefensiveAreaPosition2, FLOAT fDefensiveAreaWidth, INT iLOSTime)

	IF DOES_ENTITY_EXIST(ped.PedIndex)

		IF 	NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
		
				#IF IS_DEBUG_BUILD
					IF ( bDrawDebugStates = TRUE )
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
					ENDIF
				#ENDIF
				
				ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
				CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, iLOSTime)
				
				SWITCH ped.eState
				
					CASE PED_STATE_IDLE
					
						GO_TO_PED_STATE(ped, PED_STATE_STANDING_AT_LOCATION)
						
					BREAK
					
					CASE PED_STATE_STANDING_AT_LOCATION
					
						IF ( ped.bHasTask = FALSE )
						
							TASK_START_SCENARIO_IN_PLACE(ped.PedIndex, "WORLD_HUMAN_GUARD_STAND")
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iConversationTimer = 0
							
							ped.bHasTask = TRUE
						ENDIF
										
						IF ( bSecurityAlerted = TRUE )
						OR IS_PED_IN_COMBAT(ped.PedIndex)
						OR IS_SECURITY_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR IS_SECURITY_PED_ALERTED_BY_PED_VEHICLE_ACTIONS(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
						
							IF IS_ENTITY_IN_ANGLED_AREA(TargetPedIndex, vGateAreaPosition1, vGateAreaPosition2, fGateAreaWidth)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
							
							IF GET_GAME_TIMER() - ped.iConversationTimer > 0
								IF ( bPlayerInsideFilmStudio = FALSE AND ped.fDistanceToTarget < 15.0 )
									IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
										TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "MOVE_AWAY_WARNING", sVoiceName, SPEECH_PARAMS_FORCE_NORMAL)
										ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 15000)
									ENDIF
								ENDIF
							ENDIF

						ENDIF
						
					BREAK
					
					CASE PED_STATE_COMBAT_DEFENSIVE
						
						IF ( ped.bHasTask = FALSE )

							SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_DEFENSIVE)
							SET_PED_COMBAT_ABILITY(ped.PedIndex, CAL_PROFESSIONAL)
							
							SET_PED_ANGLED_DEFENSIVE_AREA(ped.PedIndex, vDefensiveAreaPosition1, vDefensiveAreaPosition2, fDefensiveAreaWidth)
							
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
						
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.5)
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iConversationTimer = 0
							
							ped.bHasTask = TRUE
							
						ENDIF
					
						IF ( ped.bCanSeeTargetPed = TRUE )
							IF IS_PED_IN_THIS_VEHICLE(TargetPedIndex, TargetVehicleIndex)
								IF ( ped.fDistanceToTarget < 20.0 )
									IF GET_GAME_TIMER() - ped.iConversationTimer > 0
										IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
											IF IS_VEHICLE_STOPPED(TargetVehicleIndex)
												TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GET_OUT_OF_THE_CAR", sVoiceName, SPEECH_PARAMS_FORCE_NORMAL)
												ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 15000)
											ELSE
												TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "STOP_THE_CAR", sVoiceName, SPEECH_PARAMS_FORCE_NORMAL)
												ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 15000)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
					BREAK

					CASE PED_STATE_COMBAT_ON_FOOT
					
						IF ( ped.bHasTask = FALSE )
						
							REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
							
							SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)
							SET_PED_COMBAT_ABILITY(ped.PedIndex, CAL_PROFESSIONAL)
							
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
						
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.2)
							
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "FREEZE", sVoiceName, SPEECH_PARAMS_FORCE_NORMAL)
							TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
							
							IF NOT IS_PED_IN_COMBAT(ped.PedIndex, TargetPedIndex)
								TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
							ENDIF
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							bSecurityAlerted = TRUE
							ped.bHasTask = TRUE
							
						ENDIF
					
					BREAK
					
				ENDSWITCH
				
			ENDIF
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex)
			
			ped.eState = PED_STATE_DEAD
		
			IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
				ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
				UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_SECURITY_PED_AROUND_ACTOR(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)

		IF 	NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
		
				#IF IS_DEBUG_BUILD
					IF ( bDrawDebugStates = TRUE )
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
					ENDIF
				#ENDIF
				
				SWITCH ped.eState
				
					CASE PED_STATE_IDLE
					
						GO_TO_PED_STATE(ped, PED_STATE_COMBAT_AIMING)
						
					BREAK
					
					CASE PED_STATE_COMBAT_AIMING
					
						IF ( ped.bHasTask = FALSE )
							
							SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)
							SET_PED_COMBAT_ABILITY(ped.PedIndex, CAL_PROFESSIONAL)
							
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
							
							//CA_JUST_FOLLOW_VEHICLE - might be useful for chasers
							//CA_USE_VEHICLE_ATTACK - might be useful for chasers

							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iTimer = GET_GAME_TIMER()
							ped.bHasTask = TRUE
						ENDIF
					
						SET_PED_RESET_FLAG(ped.PedIndex, PRF_BlockWeaponFire, TRUE)	//block weapon fire in aiming state each frame
				
						IF ( ped.bHasTask = TRUE )
							IF HAS_TIME_PASSED(5000, ped.iTimer)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
						ENDIF
					
						//add other checks here, like player aiming
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped.PedIndex, TargetPedIndex)			
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
						ENDIF
					
					BREAK
					
					CASE PED_STATE_COMBAT_ON_FOOT
					
						IF ( ped.bHasTask = FALSE )
						
							REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
							
							SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)
							SET_PED_COMBAT_ABILITY(ped.PedIndex, CAL_PROFESSIONAL)
							
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
						
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.2)
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							bSecurityAlerted = TRUE
							ped.bHasTask = TRUE
							
						ENDIF
					
					BREAK
					
				ENDSWITCH
				
			ENDIF
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex)
			
			ped.eState = PED_STATE_DEAD
		
			IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
				ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
				UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_SECURITY_PEDS_AT_FILM_SET(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			iProgress++
		
		BREAK
		
		CASE 1
		
			INT i
			
			IF 	DOES_ENTITY_EXIST(psSetSecurity[0].PedIndex)
			AND DOES_ENTITY_EXIST(psSetSecurity[1].PedIndex)
			AND DOES_ENTITY_EXIST(psSetSecurity[2].PedIndex)
			AND	DOES_ENTITY_EXIST(psSetSecurity[3].PedIndex)
			AND	DOES_ENTITY_EXIST(psSetSecurity[4].PedIndex)
				
				REPEAT COUNT_OF(psSetSecurity) i
					SWITCH eMissionStage
					
						CASE MISSION_STAGE_GET_TO_ACTOR
						CASE MISSION_STAGE_GET_TO_CAR
						
							//
							
						BREAK
						
						CASE MISSION_STAGE_ESCAPE_FILM_SET
						
							IF NOT IS_ENTITY_DEAD(psSetSecurity[i].PedIndex)
								IF 	psSetSecurity[i].eState != PED_STATE_DEAD
								AND psSetSecurity[i].eState != PED_STATE_COMBAT_AIMING
								AND psSetSecurity[i].eState != PED_STATE_COMBAT_ON_FOOT
									GO_TO_PED_STATE(psSetSecurity[i], PED_STATE_COMBAT_AIMING)
								ENDIF
							ENDIF

						BREAK
						
					ENDSWITCH
				ENDREPEAT

				iProgress++
			
			ELSE
			
				REPEAT COUNT_OF(psSetSecurity) i
					IF NOT DOES_ENTITY_EXIST(psSetSecurity[i].PedIndex)
						REQUEST_MODEL(mnSecurityPed)
						IF HAS_MODEL_LOADED(mnSecurityPed)
							psSetSecurity[i].PedIndex = CREATE_ENEMY_PED(psSetSecurity[i].vPosition, psSetSecurity[i].fHeading, mnSecurityPed,
																		 RELGROUPHASH_SECURITY_GUARD, 200, WEAPONTYPE_PISTOL 
																		 #IF IS_DEBUG_BUILD, psSetSecurity[i].sDebugName #ENDIF)							
							GO_TO_PED_STATE(psSetSecurity[i], PED_STATE_IDLE)
							SET_MODEL_AS_NO_LONGER_NEEDED(mnSecurityPed)
						ENDIF
					ENDIF
				ENDREPEAT
			
			ENDIF
			
		BREAK
		
		CASE 2
			
			IF ( bSecurityAlerted = FALSE )
				IF ( iSetSecurityAlertTimer != 0 )
					IF HAS_TIME_PASSED(20000, iSetSecurityAlertTimer)
						bSecurityAlerted = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			MANAGE_SECURITY_PED_AT_FILM_SET(psSetSecurity[0], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
											SECURITY1VOICENAME, << -1177.71, -503.58, 34.57 >>, 0.75, 10000, 6000, 1050, 1.0, FALSE)
			MANAGE_SECURITY_PED_AT_FILM_SET(psSetSecurity[1], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
											SECURITY2VOICENAME, << -1189.43, -506.21, 34.57 >>, 0.75, 20000, 5500, 1000, 0.0, FALSE)
			MANAGE_SECURITY_PED_AT_FILM_SET(psSetSecurity[2], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
											SECURITY3VOICENAME, << -1182.29, -507.45, 34.57 >>, 0.75, 12000, 6500, 950, 1.0, FALSE)
			MANAGE_SECURITY_PED_AT_FILM_SET(psSetSecurity[3], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
											SECURITY4VOICENAME, << -1171.85, -512.33, 34.57 >>, 0.75, 15000, 5000, 1100, 0.0, TRUE)
			MANAGE_SECURITY_PED_AT_FILM_SET(psSetSecurity[4], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
											SECURITY2VOICENAME, << -1158.05, -513.84, 34.02 >>, 0.75, 16000, 6000, 900, 0.0, TRUE)
			
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_SECURITY_PEDS_AT_GATES(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			iProgress++
		
		BREAK
		
		CASE 1
			
			IF 	DOES_ENTITY_EXIST(psNorthGateSecurity[0].PedIndex)
			AND DOES_ENTITY_EXIST(psSouthGateSecurity[0].PedIndex)
				
				SWITCH eMissionStage
				
					CASE MISSION_STAGE_GET_TO_ACTOR
					CASE MISSION_STAGE_GET_TO_CAR
					
						//
						
					BREAK
					
					CASE MISSION_STAGE_ESCAPE_FILM_SET
					
						IF NOT IS_ENTITY_DEAD(psNorthGateSecurity[0].PedIndex)
							IF 	psNorthGateSecurity[0].eState != PED_STATE_DEAD
							AND psNorthGateSecurity[0].eState != PED_STATE_COMBAT_ON_FOOT
							AND psNorthGateSecurity[0].eState != PED_STATE_COMBAT_DEFENSIVE
								WARP_PED(psNorthGateSecurity[0].PedIndex, <<-1055.8082, -485.2950, 35.6577>>, 117.3792, FALSE, FALSE, FALSE)
								GO_TO_PED_STATE(psNorthGateSecurity[0], PED_STATE_COMBAT_DEFENSIVE)
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(psSouthGateSecurity[0].PedIndex)
							IF 	psSouthGateSecurity[0].eState != PED_STATE_DEAD
							AND psSouthGateSecurity[0].eState != PED_STATE_COMBAT_ON_FOOT
							AND psSouthGateSecurity[0].eState != PED_STATE_COMBAT_DEFENSIVE
								WARP_PED(psSouthGateSecurity[0].PedIndex, <<-1206.0895, -571.6317, 26.2291>>, 303.6333, FALSE, FALSE, FALSE)
								GO_TO_PED_STATE(psSouthGateSecurity[0], PED_STATE_COMBAT_DEFENSIVE)
							ENDIF
						ENDIF

						//maybe have 2 more guards when escaping without tuxedo?

					BREAK
					
				ENDSWITCH

				iProgress++
			
			ELSE
			
				IF NOT DOES_ENTITY_EXIST(psNorthGateSecurity[0].PedIndex)
					REQUEST_MODEL(mnSecurityPed)
					IF HAS_MODEL_LOADED(mnSecurityPed)
						psNorthGateSecurity[0].PedIndex = CREATE_ENEMY_PED(psNorthGateSecurity[0].vPosition, psNorthGateSecurity[0].fHeading, mnSecurityPed,
																	 	   RELGROUPHASH_SECURITY_GUARD, 200, WEAPONTYPE_PISTOL 
																	 	   #IF IS_DEBUG_BUILD, psNorthGateSecurity[0].sDebugName #ENDIF)
						GO_TO_PED_STATE(psNorthGateSecurity[0], PED_STATE_IDLE)
						SET_MODEL_AS_NO_LONGER_NEEDED(mnSecurityPed)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(psSouthGateSecurity[0].PedIndex)
					REQUEST_MODEL(mnSecurityPed)
					IF HAS_MODEL_LOADED(mnSecurityPed)
						psSouthGateSecurity[0].PedIndex = CREATE_ENEMY_PED(psSouthGateSecurity[0].vPosition, psSouthGateSecurity[0].fHeading, mnSecurityPed,
																	 	   RELGROUPHASH_SECURITY_GUARD, 200, WEAPONTYPE_PISTOL 
																	 	   #IF IS_DEBUG_BUILD, psSouthGateSecurity[0].sDebugName #ENDIF)
						GO_TO_PED_STATE(psSouthGateSecurity[0], PED_STATE_IDLE)
						SET_MODEL_AS_NO_LONGER_NEEDED(mnSecurityPed)
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
		
		CASE 2
		
			MANAGE_SECURITY_PED_AT_GATE(psNorthGateSecurity[0], SECURITY1VOICENAME, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
										<<-1044.182617,-485.863373,34.623894>>, <<-1061.653687,-472.510498,43.651306>>, 3.0,
										<<-1063.403076,-471.480377,35.153713>>, <<-1043.490601,-486.186920,39.613873>>, 10.0, 650)
			MANAGE_SECURITY_PED_AT_GATE(psSouthGateSecurity[0], SECURITY2VOICENAME, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
										<<-1203.236206,-582.197266,25.330757>>, <<-1212.639648,-571.008911,32.306335>>, 1.5,
										<<-1202.559204,-583.141479,24.330172>>, <<-1213.059326,-570.624268,30.307667>>, 20.0, 750)
																				 
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_SECURITY_PEDS_AROUND_ACTOR(INT &iProgress)

	INT i, k

	SWITCH iProgress
	
		CASE 0
		
			IF ( bSpawnActorSecurity = TRUE )
			
				REQUEST_MODEL(mnSecurityPed)
				
				IF HAS_MODEL_LOADED(mnSecurityPed)
					
					iProgress++
					
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1
		
			REPEAT COUNT_OF(psActorSecurity) i
				IF NOT DOES_ENTITY_EXIST(psActorSecurity[i].PedIndex)
					IF HAS_MODEL_LOADED(mnSecurityPed)

						k = GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(SecuritySpawnPoints))
											
						IF 	NOT IS_SPHERE_VISIBLE(SecuritySpawnPoints[k].vPosition, 2.0) AND NOT SecuritySpawned[k]
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), SecuritySpawnPoints[k].vPosition) > 10.0
					
							#IF IS_DEBUG_BUILD
								psActorSecurity[i].sDebugName = "Actor "
								psActorSecurity[i].sDebugName += i
							#ENDIF
							
							SecuritySpawned[k] 			= TRUE
							psActorSecurity[i].PedIndex = CREATE_ENEMY_PED(SecuritySpawnPoints[k].vPosition, SecuritySpawnPoints[k].fHeading, mnSecurityPed,
																		   RELGROUPHASH_SECURITY_GUARD, 200, WEAPONTYPE_PISTOL 
																		   #IF IS_DEBUG_BUILD, psActorSecurity[i].sDebugName #ENDIF)
																		   
							GO_TO_PED_STATE(psActorSecurity[i], PED_STATE_IDLE)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF 	DOES_ENTITY_EXIST(psActorSecurity[0].PedIndex)
			AND	DOES_ENTITY_EXIST(psActorSecurity[1].PedIndex)
			AND DOES_ENTITY_EXIST(psActorSecurity[2].PedIndex)
	
				SET_MODEL_AS_NO_LONGER_NEEDED(mnSecurityPed)
				
				iProgress++
			
			ENDIF
			
		BREAK
		
		CASE 2	//update spawned security

			REPEAT COUNT_OF(psActorSecurity) i
				MANAGE_SECURITY_PED_AROUND_ACTOR(psActorSecurity[i], PLAYER_PED_ID())
			ENDREPEAT
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_SECURITY_PED_FOR_MELTDOWN(PED_STRUCT &ped, STRING sVoiceName, PED_INDEX TargetPedIndex, INT iLOSTime)

	IF DOES_ENTITY_EXIST(ped.PedIndex)

		IF 	NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
		
				#IF IS_DEBUG_BUILD
					IF ( bDrawDebugStates = TRUE )
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
					ENDIF
				#ENDIF
				
				ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
				CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, iLOSTime)
				
				SWITCH ped.eState
				
					CASE PED_STATE_IDLE
					
						GO_TO_PED_STATE(ped, PED_STATE_STANDING_AT_LOCATION)
						
					BREAK
					
					CASE PED_STATE_STANDING_AT_LOCATION
					
						IF ( ped.bHasTask = FALSE )
						
							TASK_START_SCENARIO_IN_PLACE(ped.PedIndex, "WORLD_HUMAN_GUARD_STAND")
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iConversationTimer = 0
							
							ped.bHasTask = TRUE
						ENDIF
				
						IF ( bSecurityAlerted = TRUE )
						OR ( bMeltdownSceneDisrupted = TRUE)
						OR IS_PED_IN_COMBAT(ped.PedIndex)
						OR IS_SECURITY_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR IS_SECURITY_PED_ALERTED_BY_PED_VEHICLE_ACTIONS(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
							IF IS_ENTITY_IN_ANGLED_AREA(TargetPedIndex, <<-1143.180786,-428.372925,35.001019>>, <<-1125.562012,-426.191284,40.545696>>, 1.0)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
						ENDIF
						
						IF ( bMeltdownSceneWalkedInto = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_AIMING_PED)
						ENDIF

					BREAK
					
					CASE PED_STATE_COMBAT_AIMING_PED
					
						SET_PED_RESET_FLAG(ped.PedIndex, PRF_BlockWeaponFire, TRUE)	//block weapon fire in aiming state each frame
					
						IF ( ped.bHasTask = FALSE )
						
							SET_PED_COMBAT_ABILITY(ped.PedIndex, CAL_PROFESSIONAL)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, FALSE)
							
							SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)
							
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							ped.iTimer = GET_GAME_TIMER()
							ped.iConversationTimer = 0
							
							ped.bHasTask = TRUE
							
						ENDIF
						
						IF ( bSecurityAlerted = TRUE )
						OR ( bMeltdownSceneDisrupted = TRUE)
						OR HAS_TIME_PASSED(5000, ped.iTimer)
						OR IS_SECURITY_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR IS_SECURITY_PED_ALERTED_BY_PED_VEHICLE_ACTIONS(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
						
							IF GET_GAME_TIMER() - ped.iConversationTimer > 0
								IF ( ped.fDistanceToTarget < 30.0 )
									IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
										TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "FREEZE", sVoiceName, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
										ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 9000)
									ENDIF
								ENDIF
							ENDIF
													
						ENDIF

					BREAK
					
					CASE PED_STATE_COMBAT_ON_FOOT
					
						IF ( ped.bHasTask = FALSE )
						
							REMOVE_PED_DEFENSIVE_AREA(ped.PedIndex)
							
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)
							
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_FLANK, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_CAN_CHASE_TARGET_ON_FOOT, TRUE)
							
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							IF ( ped.fDistanceToTarget < 25.0 )
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "FREEZE", sVoiceName, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 1000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE | SLF_USE_TORSO, SLF_LOOKAT_VERY_HIGH)
							ENDIF
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							bSecurityAlerted = TRUE
							
							ped.bHasTask = TRUE
						ENDIF
									
					BREAK
				
				ENDSWITCH
		
			ENDIF
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex)
			
			ped.eState = PED_STATE_DEAD
		
			IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
				ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
				UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
			ENDIF
			
		ENDIF
	
	ENDIF

ENDPROC

PROC MANAGE_SECURITY_PEDS_FOR_MELTDOWN(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			iProgress++
		
		BREAK
		
		CASE 1
			
			IF 	DOES_ENTITY_EXIST(psMeltdownSecurity[0].PedIndex)
				
				SWITCH eMissionStage
				
					CASE MISSION_STAGE_GET_TO_ACTOR
					CASE MISSION_STAGE_GET_TO_CAR
					
						//
						
					BREAK
					
					CASE MISSION_STAGE_ESCAPE_FILM_SET
					
					BREAK
					
				ENDSWITCH
				
				iProgress++
			
			ELSE
			
				IF NOT DOES_ENTITY_EXIST(psMeltdownSecurity[0].PedIndex)
					REQUEST_MODEL(mnSecurityPed)
					IF HAS_MODEL_LOADED(mnSecurityPed)
						psMeltdownSecurity[0].PedIndex = CREATE_ENEMY_PED(psMeltdownSecurity[0].vPosition, psMeltdownSecurity[0].fHeading, mnSecurityPed,
																	 	   RELGROUPHASH_SECURITY_GUARD, 200, WEAPONTYPE_PISTOL 
																	 	   #IF IS_DEBUG_BUILD, psMeltdownSecurity[0].sDebugName #ENDIF)
						GO_TO_PED_STATE(psMeltdownSecurity[0], PED_STATE_IDLE)
						SET_MODEL_AS_NO_LONGER_NEEDED(mnSecurityPed)
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
		
		CASE 2
		
			MANAGE_SECURITY_PED_FOR_MELTDOWN(psMeltdownSecurity[0], SECURITY3VOICENAME, PLAYER_PED_ID(), 750)
																				 
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_DEVIN(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInteriorPosition) < 150.0
			
				psDevin.vPosition	= <<475.4521, -1313.7129, 28.2059>>
				psDevin.fHeading	= 233.8940
				psDevin.ModelName	= IG_DEVIN
			
				iProgress++
				
			ENDIF
		
		BREAK
		
		CASE 1
		
			REQUEST_MODEL(psDevin.ModelName)
			REQUEST_ANIM_DICT("misscarsteal4leadinoutcar_4_ext")
		
			IF 	HAS_MODEL_LOADED(psDevin.ModelName)
			AND	HAS_ANIM_DICT_LOADED("misscarsteal4leadinoutcar_4_ext")
		
				IF HAS_MISSION_PED_BEEN_CREATED(psDevin, FALSE, RELGROUPHASH_PLAYER, FALSE, CHAR_DEVIN)
					IF NOT IS_ENTITY_DEAD(psDevin.PedIndex)
					
						SET_FORCE_FOOTSTEP_UPDATE(psDevin.PedIndex, TRUE)
						ADD_PED_FOR_DIALOGUE(CST4Conversation, 0, psDevin.PedIndex, "DEVIN")
						
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_HEAD, 		0, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_HAIR, 		0, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_TORSO, 		0, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_LEG, 		0, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_HAND,		0, 0, 0) //(hand)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_FEET, 		0, 0, 0) //(feet)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_TEETH, 		0, 0, 0) //(feet)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_SPECIAL, 	2, 0, 0) //(accs)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_SPECIAL2, 	0, 0, 0) //(task)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_DECL, 		0, 0, 0) //(decl)
						SET_PED_COMPONENT_VARIATION(psDevin.PedIndex, PED_COMP_JBIB, 		0, 0, 0) //(jbib)						
						
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(psDevin.ModelName)
					
					psDevin.bHasTask 	= FALSE
					psDevin.iProgress	= 0
			
					iProgress++
					
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			VECTOR 	vScenePosition, vSceneRotation	
		
			vSceneRotation	= << 0.0, 0.0, 116.8 >> 
			vScenePosition 	= << 473.682, -1314.547, 29.244 >>
			
			IF DOES_ENTITY_EXIST(psDevin.PedIndex)
				IF NOT IS_ENTITY_DEAD(psDevin.PedIndex)
				
					IF IS_ENTITY_PLAYING_ANIM(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_loop_devin")
					OR IS_ENTITY_PLAYING_ANIM(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_action_devin")
						SET_PED_CAPSULE(psDevin.PedIndex, 0.5)
						SET_PED_RESET_FLAG(psDevin.PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
					ENDIF
				
					SWITCH psDevin.iProgress
						
						CASE 0

							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psDevin.PedIndex, TRUE)
							SET_RAGDOLL_BLOCKING_FLAGS(psDevin.PedIndex, RBF_PLAYER_IMPACT)
							TASK_PLAY_ANIM_ADVANCED(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_loop_devin",
													vScenePosition, vSceneRotation, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1,
													AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | 
													AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)

							psDevin.bHasTask = FALSE
							psDevin.iProgress++
							
						BREAK
						
						CASE 1
						
							IF ( bLeadInTriggered = TRUE )
								
								IF IS_ENTITY_PLAYING_ANIM(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_loop_devin")
									IF GET_ENTITY_ANIM_CURRENT_TIME(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_loop_devin") > 0.93
									
										TASK_LOOK_AT_ENTITY(psDevin.PedIndex, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
										TASK_PLAY_ANIM_ADVANCED(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_action_devin",
																vScenePosition, vSceneRotation, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,
																AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE |AF_USE_KINEMATIC_PHYSICS |
																AF_HOLD_LAST_FRAME, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
										psDevin.bHasTask = FALSE
										psDevin.iProgress++
										
									ENDIF
								ENDIF
								
							ENDIF
						
						BREAK
						
						CASE 2
						
							IF IS_ENTITY_PLAYING_ANIM(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_action_devin")
							
								SET_ENTITY_ANIM_SPEED(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_action_devin", 1.0)
							
								IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_EXT_LI")
									IF GET_ENTITY_ANIM_CURRENT_TIME(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_action_devin") >= 0.225
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_EXT_LI", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_EXT_LI", TRUE)
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF

						BREAK
						
					ENDSWITCH
				
				ENDIF
			ENDIF
		
		BREAK
		
	ENDSWITCH

ENDPROC

PROC MANAGE_EJECTOR_SEAT(PED_INDEX PedIndex, PED_INDEX TargetPedIndex, VEHICLE_INDEX VehicleIndex,
						INT &iProgress, INT &iTimer, BOOL &bEjectorActivated, BOOL &bPedEjected)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
		
			IF 	NOT IS_CELLPHONE_CAMERA_IN_USE()
			AND	NOT IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_CAMERA_MAN)

				SWITCH iProgress
				
					CASE 0
					
						IF 	NOT IS_ENTITY_UPSIDEDOWN(VehicleIndex)
						AND	IS_PED_SITTING_IN_THIS_VEHICLE(PedIndex, VehicleIndex)
						AND IS_PED_SITTING_IN_THIS_VEHICLE(TargetPedIndex, VehicleIndex)
						
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
							
							REQUEST_PTFX_ASSET()
							REQUEST_SCRIPT_AUDIO_BANK("CAR_STEAL_3_SCREAM")
							REQUEST_AMBIENT_AUDIO_BANK("CAR_THEFT_MOVIE_LOT")
							
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
							
								CLEAR_HELP()
								CLEAR_PRINTS()
								DISABLE_CELLPHONE(TRUE)
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							
								CLEAR_PED_TASKS(PedIndex)
								SET_ENTITY_INVINCIBLE(PedIndex, TRUE)
								
								IF IS_PLAYER_PLAYING(PLAYER_ID())
									SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
									ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
								ENDIF
				
								START_AUDIO_SCENE("CAR_3_ACTIVATE_EJECTOR_SEAT")
								
								SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", TRUE)
								SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)
								SET_AUDIO_FLAG("DisableAbortConversationForDeathAndInjury", TRUE)

								TASK_LOOK_AT_ENTITY(TargetPedIndex, PedIndex, 2000, SLF_WHILE_NOT_IN_FOV)

								iTimer 				= GET_GAME_TIMER()
								bEjectorActivated 	= TRUE
								
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
																	
								iProgress++
							
							ENDIF
						
						ENDIF
					
					BREAK
					
					CASE 1
			
						DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)

						IF HAS_TIME_PASSED(20, iTimer)

							IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
								POP_OFF_VEHICLE_ROOF_WITH_IMPULSE(VehicleIndex, << 0.0, -9.0, 10.0>>)
							ENDIF
							
							PLAY_SOUND_FROM_ENTITY(-1, "CAR_THEFT_MOVIE_LOT_EJECT_SEAT", VehicleIndex)
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_carsteal3_eject", VehicleIndex, vEjectorSeatPtfxOffset, vEjectorSeatPtfxRotation)
							
							iTimer = GET_GAME_TIMER()
							
							iProgress++
						
						ENDIF
						
					BREAK
					
					CASE 2
			
						DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)

						IF HAS_TIME_PASSED(150, iTimer)

							IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
								
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iEjectSceneID)
								
									iEjectSceneID = CREATE_SYNCHRONIZED_SCENE(<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
									
									SET_SYNCHRONIZED_SCENE_LOOPED(iEjectSceneID, FALSE)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iEjectSceneID, FALSE)
									
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iEjectSceneID, VehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(VehicleIndex, "seat_pside_f"))
									
									CLEAR_PED_TASKS_IMMEDIATELY(PedIndex)
									SET_ENTITY_NO_COLLISION_ENTITY(PedIndex, VehicleIndex, TRUE)
									TASK_SYNCHRONIZED_SCENE(PedIndex, iEjectSceneID, "misscarsteal4@actress", "eject_girl",
															INSTANT_BLEND_IN, INSTANT_BLEND_OUT,
															SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_PRESERVE_VELOCITY,
															RBF_FALLING)
															
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
								
								ENDIF
								
								PLAY_SOUND_FROM_ENTITY(iEjectorScreamSoundID, "Ejector_Scream", PedIndex, "CAR_STEAL_3_AGENT")
								
								INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CS3_EJECTOR_SEAT_USED)
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0, REPLAY_IMPORTANCE_HIGH)
								
								iTimer = GET_GAME_TIMER()
								
								iProgress++
							
							ENDIF
						ENDIF
					
					BREAK
					
					CASE 3
			
						DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iEjectSceneID)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iEjectSceneID) >= fEjectScenePhase

								CLEAR_PED_TASKS(PedIndex)
								SET_PED_TO_RAGDOLL_WITH_FALL(PedIndex, 1000, 30000, TYPE_FROM_HIGH, << 0.0, 0.0, 1.0 >>, 0.0, << 0.0, 0.0, 0.0>>, << 0.0, 0.0, 0.0>>)
								APPLY_FORCE_TO_ENTITY(PedIndex, APPLY_TYPE_IMPULSE, vEjectorSeatSecondaryForce, <<0.0,0.0,0.0>>,
												      ENUM_TO_INT(RAGDOLL_SPINE3), TRUE, TRUE, TRUE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PedIndex)
							ENDIF
						ENDIF
					
						IF HAS_TIME_PASSED(1000, iTimer)
					
							SET_ENTITY_HEALTH(PedIndex, 101)
							SET_PED_DIES_WHEN_INJURED(PedIndex, TRUE)
							SET_ENTITY_INVINCIBLE(PedIndex, FALSE)
							
							IF IS_PLAYER_PLAYING(PLAYER_ID())
								ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
							ENDIF
							
							DISABLE_CELLPHONE(FALSE)
							
							bPedEjected = TRUE
							
							STOP_AUDIO_SCENE("CAR_3_ACTIVATE_EJECTOR_SEAT")
							
							SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", FALSE)
							SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE)
							SET_AUDIO_FLAG("DisableAbortConversationForDeathAndInjury", FALSE)
							
							IF IS_THIS_PRINT_BEING_DISPLAYED("CAR4_EGIRL")
								CLEAR_PRINTS()
							ENDIF
							
							IF ( bMusicEventDropTriggered = FALSE )
								TRIGGER_MUSIC_EVENT("CAR3_DROP")
								bMusicEventDropTriggered = TRUE
							ENDIF
							
							iProgress++
							
						ENDIF
					
					BREAK
					
					CASE 4
					
						IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(PedIndex)
						
							APPLY_DAMAGE_TO_PED(PedIndex, 10, TRUE)
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Applying damage to ejected ped due to collision.")
							#ENDIF
							
							iProgress++
						
						ENDIF
					
					BREAK
				
				ENDSWITCH
				
			ENDIF
		
		ENDIF
		
		IF NOT HAS_SOUND_FINISHED(iEjectorScreamSoundID)
			IF IS_ENTITY_DEAD(PedIndex)
				STOP_SOUND(iEjectorScreamSoundID)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_STEER_BIAS(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex, BOOL &bActive, INT &iTimer, INT &iTime, FLOAT &fValue,
					   INT iBaseTime = 3500, INT iBaseSteerTime = 500, FLOAT fBaseValue = 0.3, FLOAT fAddedValue = 0.5,
					   INT iSteerAddedTimeMin = 500, INT iSteerAddedTimeMax = 2500, INT iBaseTimeMin = 1500, INT iBaseTimeMax = 3000)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
		
			IF IS_PED_SITTING_IN_THIS_VEHICLE(PedIndex, VehicleIndex)

				IF NOT IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_CAMERA_MAN)
				
					IF ( iTimer = 0 )			//initialise the timers
					
						iTimer 	= GET_GAME_TIMER()
						iTime 	= iBaseTime
						bActive = FALSE
					
					ELSE						//run timers
					
						IF NOT IS_VEHICLE_STOPPED(VehicleIndex)
								
							IF ( bActive = FALSE )
							
								IF HAS_TIME_PASSED(iTime, iTimer)
									
									fValue 	= GET_RANDOM_FLOAT_IN_RANGE(-fAddedValue, fAddedValue)	//calculate new value of the steer bias
									
									IF ( fValue >= 0.0 )
										fValue = fValue + fBaseValue
									ELIF ( fValue < 0.0 )
										fValue = fValue - fBaseValue
									ENDIF
									
									iTimer 	= GET_GAME_TIMER()
									iTime 	= iBaseSteerTime + GET_RANDOM_INT_IN_RANGE(iSteerAddedTimeMin, iSteerAddedTimeMax)
									
									IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, "misscarsteal4@actress", "car_fight_girl")
										TASK_PLAY_ANIM(PedIndex, "misscarsteal4@actress", "car_fight_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
									ENDIF
									
									bActive = TRUE

								ENDIF
								
							ELIF ( bActive = TRUE )
							
								IF HAS_TIME_PASSED(iTime, iTimer)	//if it is time to stop applying steer bias
										
									iTimer 	= GET_GAME_TIMER()
									iTime 	= iBaseTime + GET_RANDOM_INT_IN_RANGE(iBaseTimeMin, iBaseTimeMax)
								
									IF IS_ENTITY_PLAYING_ANIM(PedIndex, "misscarsteal4@actress", "car_fight_girl")
										CLEAR_PED_TASKS(PedIndex)
									ENDIF
									
									bActive = FALSE
								
								ELSE								//keep applying steer bias						
								
									SET_VEHICLE_STEER_BIAS(VehicleIndex, fValue)
								
								ENDIF
							
							ENDIF
							
						ENDIF

					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MANAGE_ACTRESS(PED_STRUCT &ped, PED_INDEX TargetPedIndex, VEHICLE_INDEX TargetVehicleIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
	
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
		
		#IF IS_DEBUG_BUILD
			ped.sDebugName = "Actress"
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
			
				IF ( bEjectorSeatAllowed = TRUE )
					MANAGE_EJECTOR_SEAT(ped.PedIndex, TargetPedIndex, TargetVehicleIndex, iEjectorSeatProgress,
										iEjectorSeatTimer, bEjectorSeatActive, bActressEjected)
				ENDIF
			
				SWITCH ped.eState
				
					CASE PED_STATE_IDLE
						
						GO_TO_PED_STATE(ped, PED_STATE_SITTING_IN_CAR)
						
					BREAK
				
					CASE PED_STATE_SITTING_IN_CAR
					
						IF ( ped.bHasTask = FALSE )
							REQUEST_ANIM_DICT("misscarsteal4@actress")
							IF HAS_ANIM_DICT_LOADED("misscarsteal4@actress")
							
								SET_PED_CONFIG_FLAG(ped.PedIndex, PCF_FallsOutOfVehicleWhenKilled, TRUE)
								
								TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actress", "sit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
								
								IF ( bPlayerEnteredTargetVehicle = FALSE )
								
									TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
								
								ENDIF
								
								SET_PED_VISUAL_FIELD_PROPERTIES(ped.PedIndex, 30.0, 5.0, 90.0)
								ped.bHasTask = TRUE
							ENDIF
						ENDIF
						
						IF ARE_PEDS_IN_THE_SAME_VEHICLE(ped.PedIndex, TargetPedIndex)
							IF HAS_LABEL_BEEN_TRIGGERED("CST4_APULL")
								MANAGE_STEER_BIAS(ped.PedIndex, TargetVehicleIndex, bSteerBiasActive, psActress.iTimer,
												  iSteerBiasTimer, fSteerBiasValue, 3500, 500, 0.05, 0.035, 250, 1250)
							ENDIF
						ENDIF

						IF ( bPlayerEnteredTargetVehicle = TRUE )
						
							IF IS_PED_IN_THIS_VEHICLE(ped.PedIndex, TargetVehicleIndex)
						
								IF NOT IS_PED_IN_VEHICLE(TargetPedIndex, TargetVehicleIndex)
									GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
								ENDIF
								
								IF 	IS_VEHICLE_STOPPED(TargetVehicleIndex)
								AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(ped.PedIndex, TargetVehicleIndex, VS_FRONT_RIGHT)
									IF ( iCarStoppedTimer = 0 )
										iCarStoppedTimer = GET_GAME_TIMER()
									ELSE
										IF HAS_TIME_PASSED(PICK_INT(bPlayerInsideFilmStudio, 12000, 8000), iCarStoppedTimer)
											GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
										ENDIF
									ENDIF
								ELSE
									iCarStoppedTimer = 0
								ENDIF
								
							ENDIF
						
							IF ( bChaseInProgress = FALSE )
								IF IS_VEHICLE_DRIVEABLE(TargetVehicleIndex)
									IF IS_PED_SITTING_IN_VEHICLE(ped.PedIndex, TargetVehicleIndex)
										IF IS_VEHICLE_STOPPED(TargetVehicleIndex)
											IF NOT IS_PED_IN_VEHICLE(TargetPedIndex, TargetVehicleIndex)
											OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 16.0, FALSE)
											OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ped.PedIndex), 16.0, TRUE)
												GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
					
						ELSE
						
							IF ( bSecurityAlerted = TRUE )
							OR IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedindex)
								GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
							ENDIF							
							
						ENDIF
						
						
						IF IS_PED_SITTING_IN_VEHICLE(TargetPedIndex, TargetVehicleIndex)
							IF ( bSteerBiasActive = FALSE )
								IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actress", "car_panic_girl")
									REQUEST_ANIM_DICT("misscarsteal4@actress")
									IF HAS_ANIM_DICT_LOADED("misscarsteal4@actress")
										TASK_CLEAR_LOOK_AT(ped.PedIndex)
										TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actress", "car_panic_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedindex)
								GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
							ENDIF
						ENDIF
						
						
						IF NOT IS_PED_IN_VEHICLE(ped.PedIndex, TargetVehicleIndex)
						OR ( IS_PED_BEING_JACKED(ped.PedIndex) AND IS_PED_JACKING(TargetPedIndex) )					
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
						
						IF ( bEjectorSeatActive = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_BEING_EJECTED)
						ENDIF

					BREAK
					
					CASE PED_STATE_BEING_EJECTED
					
						IF ( ped.bHasTask = FALSE )
							ped.bHasTask = TRUE
						ENDIF
						
						IF ( bActressEjected = TRUE )
							IF ( GET_ENTITY_HEALTH(ped.PedIndex) < 101 )
								SET_ENTITY_HEALTH(ped.PedIndex, 0)
							ENDIF
						ENDIF

					BREAK
					
					CASE PED_STATE_FLEEING
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_PED_RAGDOLL(ped.PedIndex)
								CLEAR_PED_TASKS(ped.PedIndex)
							ENDIF
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_USE_VEHICLE, FALSE)
							TASK_SMART_FLEE_PED(ped.PedIndex, TargetPedIndex, 300, -1)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
							SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
							ped.bHasTask = TRUE
						ENDIF
						
					BREAK
				
				ENDSWITCH

			ENDIF
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING
		
			IF ( ped.fDistanceToTarget > 150.0 )
			
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
				
				REMOVE_ANIM_DICT("misscarsteal4@actress")	
				
			ENDIF
		
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_CONVERSATIONS_FOR_DIRECTOR(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
		
			TEXT_LABEL_23 CurrentConversationRoot
		
			IF NOT CST4_DSCAR_TRIGGERED
				IF ( ped.fDistanceToTarget < 30.0 )
					IF 	( ped.eState != PED_STATE_FLEEING )
					AND ( ped.eState != PED_STATE_MOVING_TO_LOCATION )
						IF IS_PED_IN_VEHICLE(TargetPedIndex, vsPlayersCar.VehicleIndex, TRUE)
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
								TEXT_LABEL_23 LocalConversationRoot
								
								LocalConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								
								IF NOT IS_STRING_NULL_OR_EMPTY(LocalConversationRoot)
									IF 	NOT ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_DSW")
									AND	NOT ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_DSG")
									AND NOT ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_DSC")
									AND NOT ARE_STRINGS_EQUAL(LocalConversationroot, "CST4_DSSET1")
									AND NOT ARE_STRINGS_EQUAL(LocalConversationroot, "CST4_DSSET2")
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DSCAR", CONV_PRIORITY_MEDIUM)
									
									CST4_DSCAR_TRIGGERED 	= TRUE
									
									CST4_DBRAN_1_TRIGGERED 	= TRUE
									CST4_DBRAN_2_TRIGGERED 	= TRUE
									CST4_DBRAN_3_TRIGGERED 	= TRUE
									CST4_DBRAN_4_TRIGGERED 	= TRUE
									CST4_DBRAN_5_TRIGGERED 	= TRUE
									
									CST4_DSSET1_TRIGGERED 	= TRUE
									CST4_DSSET2_TRIGGERED 	= TRUE
									CST4_DCHAT1_TRIGGERED	= TRUE
									CST4_DCHAT2_TRIGGERED	= TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT CST4_DSW_TRIGGERED
				IF ( ped.fDistanceToTarget < 20.0 AND ped.bCanSeeTargetPed = TRUE )
				
					WEAPON_TYPE eCurrentPlayerWeapon
	 
					IF IS_PED_SHOOTING(TargetPedIndex)
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped.PedIndex)
					OR HAS_PED_RECEIVED_EVENT(ped.PedIndex, EVENT_GUN_AIMED_AT)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped.PedIndex)
					OR GET_CURRENT_PED_WEAPON(TargetPedIndex, eCurrentPlayerWeapon)
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DSW", CONV_PRIORITY_MEDIUM)
							
								CST4_DSW_TRIGGERED 			= TRUE
								
								CST4_DSG_TRIGGERED 			= TRUE
								CST4_DSCAR_TRIGGERED 		= TRUE
								CST4_DSSET1_TRIGGERED 		= TRUE
								CST4_DSSET2_TRIGGERED 		= TRUE
								CST4_DCHAT1_TRIGGERED		= TRUE
								CST4_DCHAT2_TRIGGERED		= TRUE
								CST4_DSG_PLAYING_TRIGGERED	= TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT CST4_DBRAN_1_TRIGGERED
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF ( bPlayerWearingTuxedo = TRUE AND ped.fDistanceToTarget < 30.0 )
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DBRAN", "CST4_DBRAN_1", CONV_PRIORITY_MEDIUM)
							ped.iConversationTimer	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000, 10000)
							CST4_DBRAN_1_TRIGGERED 	= TRUE
						ENDIF	
					ENDIF
				ENDIF
			ELSE
				IF NOT CST4_DBRAN_2_TRIGGERED
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF ( bPlayerWearingTuxedo = TRUE AND ped.fDistanceToTarget < 30.0 )
							IF GET_GAME_TIMER() - ped.iConversationTimer > 0
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DBRAN", "CST4_DBRAN_2", CONV_PRIORITY_MEDIUM)
									ped.iConversationTimer	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 12000)
									CST4_DBRAN_2_TRIGGERED 	= TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT CST4_DBRAN_3_TRIGGERED
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF ( bPlayerWearingTuxedo = TRUE AND ped.fDistanceToTarget < 30.0 )
								IF GET_GAME_TIMER() - ped.iConversationTimer > 0
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DBRAN", "CST4_DBRAN_3", CONV_PRIORITY_MEDIUM)
										ped.iConversationTimer	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000, 12000)
										CST4_DBRAN_3_TRIGGERED 	= TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT CST4_DBRAN_4_TRIGGERED
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF ( bPlayerWearingTuxedo = TRUE AND ped.fDistanceToTarget < 30.0 )
									IF GET_GAME_TIMER() - ped.iConversationTimer > 0
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DBRAN", "CST4_DBRAN_4", CONV_PRIORITY_MEDIUM)
											ped.iConversationTimer	= GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(9000, 12000)
											CST4_DBRAN_4_TRIGGERED 	= TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT CST4_DBRAN_5_TRIGGERED
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF ( bPlayerWearingTuxedo = TRUE AND ped.fDistanceToTarget < 30.0 )
										IF GET_GAME_TIMER() - ped.iConversationTimer > 0
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DBRAN", "CST4_DBRAN_5", CONV_PRIORITY_MEDIUM)
												CST4_DBRAN_5_TRIGGERED 	= TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF CST4_DBRAN_1_TRIGGERED
			OR CST4_DBRAN_2_TRIGGERED
			OR CST4_DBRAN_3_TRIGGERED
			OR CST4_DBRAN_4_TRIGGERED
			OR CST4_DBRAN_5_TRIGGERED
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF ( ped.fDistanceToTarget > 35.0 )
						CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						
						IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot) AND ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DBRAN")
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT CST4_DCHAT1_TRIGGERED
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF ( bPlayerWearingTuxedo = FALSE AND ped.fDistanceToTarget < 25.0 )
						IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DCHAT1", CONV_PRIORITY_MEDIUM)
							CST4_DCHAT1_TRIGGERED = TRUE
						ENDIF	
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF ( ped.fDistanceToTarget > 35.0 )
						CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						
						IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot) AND ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DCHAT1")
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT CST4_DCHAT2_TRIGGERED
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF ( bPlayerWearingTuxedo = FALSE AND ped.fDistanceToTarget < 25.0 )
						IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DCHAT2", CONV_PRIORITY_MEDIUM)
							CST4_DCHAT2_TRIGGERED = TRUE
						ENDIF	
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF ( ped.fDistanceToTarget > 35.0 )
						CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						
						IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot) AND ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DCHAT2")
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			IF 	NOT CST4_DSSET1_TRIGGERED 
			AND NOT CST4_DSSET2_TRIGGERED
				
				IF ( ped.fDistanceToTarget < 20.0 AND ped.bCanSeeTargetPed = TRUE )
						
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					
						IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
							IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DBRAN")
							OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DCHAT1")
							OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DCHAT2")
							
								CST4_DBRAN_1_TRIGGERED	= TRUE
								CST4_DBRAN_2_TRIGGERED	= TRUE
								CST4_DBRAN_3_TRIGGERED	= TRUE
								CST4_DBRAN_4_TRIGGERED	= TRUE
								CST4_DBRAN_5_TRIGGERED	= TRUE
								
								CST4_DCHAT1_TRIGGERED	= TRUE
								CST4_DCHAT2_TRIGGERED	= TRUE
								
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF

					ENDIF
					
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										
						IF ( bPlayerWearingTuxedo = TRUE )
				
							IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DSSET1", CONV_PRIORITY_MEDIUM)
							
								TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 6000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE, SLF_LOOKAT_VERY_HIGH)
							
								iDirectorFranklinShoutsCounter++
								IF (  iDirectorFranklinShoutsCounter >= 2 )
									CST4_DSSET1_TRIGGERED = TRUE
								ENDIF
							ENDIF
				
						ELSE
						
							IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DSSET2", CONV_PRIORITY_MEDIUM)
							
								TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, 6000, SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE, SLF_LOOKAT_VERY_HIGH)
							
								iDirectorFranklinShoutsCounter++
								IF (  iDirectorFranklinShoutsCounter >= 2 )
									CST4_DSSET2_TRIGGERED = TRUE
								ENDIF
							ENDIF
						
						ENDIF
			
					ENDIF
				ENDIF				
			ENDIF
			
			IF NOT CST4_DSG_TRIGGERED
				IF ( ped.fDistanceToTarget < 30.0 )
					IF 	( ped.eState != PED_STATE_FLEEING )
					AND ( ped.eState != PED_STATE_MOVING_TO_LOCATION )
						IF CST4_DSCAR_TRIGGERED
						OR CST4_DSSET1_TRIGGERED
						OR CST4_DSSET2_TRIGGERED
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DSG", CONV_PRIORITY_MEDIUM)
									CST4_DSG_TRIGGERED = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TEXT_LABEL_23 LocalConversationRoot
					
					LocalConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					
					IF NOT IS_STRING_NULL_OR_EMPTY(LocalConversationRoot)
						IF ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_DSG")
							IF IS_SCRIPTED_SPEECH_PLAYING(ped.PedIndex)
								CST4_DSG_PLAYING_TRIGGERED = TRUE
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			IF NOT CST4_DSC_TRIGGERED
				IF ( ped.eState = PED_STATE_FLEEING )
				OR ( ped.eState = PED_STATE_MOVING_TO_LOCATION )
					IF ( ped.fDistanceToTarget < 25.0 ) 
						IF IS_PED_RUNNING(ped.PedIndex) OR IS_PED_SPRINTING(ped.PedIndex)
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_DSC", CONV_PRIORITY_MEDIUM)
									iDirectorFleeShoutsCounter++
									IF (  iDirectorFleeShoutsCounter >= 3 )
										CST4_DSC_TRIGGERED = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF ( ped.eState = PED_STATE_FLEEING )
			OR ( ped.eState = PED_STATE_MOVING_TO_LOCATION )
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					CurrentConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
					IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
						IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DBRAN")
						OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DCHAT1")
						OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "CST4_DCHAT2")
						
							CST4_DBRAN_1_TRIGGERED	= TRUE
							CST4_DBRAN_2_TRIGGERED	= TRUE
							CST4_DBRAN_3_TRIGGERED	= TRUE
							CST4_DBRAN_4_TRIGGERED	= TRUE
							CST4_DBRAN_5_TRIGGERED	= TRUE
								
							CST4_DCHAT1_TRIGGERED	= TRUE
							CST4_DCHAT2_TRIGGERED	= TRUE
							
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF

				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
ENDPROC

PROC MANAGE_DIRECTOR(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
	
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
		
		CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, 1000)
		
		#IF IS_DEBUG_BUILD
			ped.sDebugName = "FCP_DIRECTOR"
			IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				SET_PED_NAME_DEBUG(ped.PedIndex, "FCP_DIRECTOR")
			ENDIF
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
		
		MANAGE_CONVERSATIONS_FOR_DIRECTOR(ped, TargetPedIndex)
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)

				SET_PED_RESET_FLAG(ped.PedIndex, PRF_ForceInjuryAfterStunned, TRUE)

				SWITCH ped.eState
				
					CASE PED_STATE_PLAYING_ANIM_1
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "beginning_loop_director")
						
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@director_grip", "beginning_loop_director",
														osPropChair.vPosition, osPropChair.vRotation,
														INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
														AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 
														0, EULER_YXZ, AIK_DISABLE_LEG_IK)
														
							ELSE
							
								SET_ENTITY_HEALTH(ped.PedIndex, 105)
								
								SET_PED_DIES_WHEN_INJURED(ped.PedIndex, TRUE)
							
								SET_ENTITY_NO_COLLISION_ENTITY(ped.PedIndex, osPropChair.ObjectIndex, TRUE)
				
								ped.bHasTask = TRUE
								
							ENDIF
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "beginning_loop_director")
						
							FLOAT fAnimationPhase
						
							SET_PED_CAPSULE(ped.PedIndex, 0.6)
							SET_PED_RESET_FLAG(ped.PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
							
							fAnimationPhase = GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@director_grip", "beginning_loop_director")
							
							IF HAS_LABEL_BEEN_TRIGGERED("CST4_DBRAN")
							//OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1187.17, -501.91, 33.41 >>, << -1172.83, -508.60, 37.56 >>, 12.0)
							
								IF ( fAnimationPhase <= 0.06 OR fAnimationPhase >= 0.99 )
									
									GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_2)
									
								ENDIF
							
							ENDIF

						ENDIF
						
						IF ( CST4_DSW_TRIGGERED = TRUE )
						OR ( bSecurityAlerted = TRUE )
						OR ( bDirectorSceneDisrupted = TRUE )
						OR IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						
							IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "beginning_loop_director")
								GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_4)
							ELSE
								GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
							ENDIF
							
						ENDIF
						
					BREAK
					
					CASE PED_STATE_PLAYING_ANIM_2
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "react_director")
								
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@director_grip", "react_director",
														osPropChair.vPosition, osPropChair.vRotation,
														NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1,
														AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME, 
														0, EULER_YXZ, AIK_DISABLE_LEG_IK)
														
							ELSE
							
								SET_ENTITY_NO_COLLISION_ENTITY(ped.PedIndex, osPropChair.ObjectIndex, TRUE)
				
								ped.bHasTask = TRUE
								
							ENDIF
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "react_director")
						
							SET_PED_CAPSULE(ped.PedIndex, 0.6)
							SET_PED_RESET_FLAG(ped.PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
							
							IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@director_grip", "react_director") >= 0.999
							
								GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_3)
							
							ENDIF

						ENDIF
						
						IF ( CST4_DSW_TRIGGERED = TRUE )
						OR ( bSecurityAlerted = TRUE )
						OR ( bDirectorSceneDisrupted = TRUE )
						OR IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)

							IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "react_director")
							AND GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@director_grip", "react_director") <= 0.118
								GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_4)
							ELSE
								GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
							ENDIF
							
						ENDIF
					
					BREAK
										
					CASE PED_STATE_PLAYING_ANIM_3
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "end_loop_director")
								
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@director_grip", "end_loop_director",
														osPropChair.vPosition, osPropChair.vRotation,
														NORMAL_BLEND_IN, WALK_BLEND_OUT, -1,
														AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 
														0, EULER_YXZ, AIK_DISABLE_LEG_IK)
														
							ELSE
							
								SET_ENTITY_NO_COLLISION_ENTITY(ped.PedIndex, osPropChair.ObjectIndex, TRUE)
				
								ped.bHasTask = TRUE
								
							ENDIF
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "end_loop_director")
						
							SET_PED_CAPSULE(ped.PedIndex, 0.6)
							SET_PED_RESET_FLAG(ped.PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)

						ENDIF
						
						IF ( CST4_DSW_TRIGGERED = TRUE )
						OR ( bSecurityAlerted = TRUE )
						OR ( bDirectorSceneDisrupted = TRUE )
						OR IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						
							SET_PED_MOVEMENT_CLIPSET(ped.PedIndex, "move_injured_generic")
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
							
						ENDIF
					
					BREAK
					
					CASE PED_STATE_PLAYING_ANIM_4
					
						IF ( ped.bHasTask = FALSE )
						
							IF NOT IS_PED_RAGDOLL(ped.PedIndex)
						
								IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "flee_exit_fwd_director")
									
									TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@director_grip", "flee_exit_fwd_director",
															osPropChair.vPosition, osPropChair.vRotation,
															SLOW_BLEND_IN, WALK_BLEND_OUT, -1,
															AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
															AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME, 
															0, EULER_YXZ, AIK_DISABLE_LEG_IK)
															
								ELSE
								
									SET_ENTITY_NO_COLLISION_ENTITY(ped.PedIndex, osPropChair.ObjectIndex, TRUE)
					
									ped.bHasTask = TRUE
									
								ENDIF
								
							ELSE
							
								GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
								
							ENDIF
						ENDIF
						
						IF ( ped.bHasTask = TRUE )
						
							IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "flee_exit_fwd_director")
								
								IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@director_grip", "flee_exit_fwd_director") >= 0.725
								
									GO_TO_PED_STATE(ped, PED_STATE_MOVING_TO_LOCATION)
								
								ENDIF
								
							ENDIF
						
						ENDIF
					
					BREAK
					
					CASE PED_STATE_MOVING_TO_LOCATION

						IF ( ped.bHasTask = FALSE )
							
							CLEAR_PED_TASKS(ped.PedIndex)
							
							/*
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
							
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
							
							SEQUENCE_INDEX SequenceIndex
							
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							OPEN_SEQUENCE_TASK(SequenceIndex)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1178.16, -499.45, 34.57 >>, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1188.00, -496.22, 34.57 >>, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1198.91, -495.79, 34.49 >>, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
								TASK_SMART_FLEE_PED(NULL, TargetPedIndex, 300, -1)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							*/
							
							TASK_COWER(ped.PedIndex, 7000)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)//TRUE)
							//SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
							
							bDirectorSceneDisrupted = TRUE
							
							ped.iTimer	= GET_GAME_TIMER()
							ped.bHasTask = TRUE
						ENDIF
						
						IF ( ped.bHasTask = TRUE )
						
							IF HAS_TIME_PASSED(5000, ped.iTimer)
								GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
							ENDIF
						
							/*
							IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
								IF IS_PED_FLEEING(ped.PedIndex)
								OR GET_SEQUENCE_PROGRESS(ped.PedIndex) = 3
									GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
								ENDIF
							ENDIF
							*/
						
						ENDIF
					
					BREAK
					
					CASE PED_STATE_FLEEING
					
						IF ( ped.bHasTask = FALSE )
						
							bDirectorSceneDisrupted = TRUE
			
							IF 	NOT IS_PED_RAGDOLL(ped.PedIndex)
								IF NOT IS_PED_FLEEING(ped.PedIndex)
								
									CLEAR_PED_TASKS(ped.PedIndex)

									CLEAR_RAGDOLL_BLOCKING_FLAGS(ped.PedIndex, RBF_MELEE | RBF_PLAYER_IMPACT)
									
									SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
									
									SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
									SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
									SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
									
									TASK_SMART_FLEE_PED(ped.PedIndex, TargetPedIndex, 300, -1)
									
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
									SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
									
								ELSE
								
									ped.bHasTask = TRUE
									
								ENDIF
							ENDIF
							
						ENDIF
					
					BREAK
					
				ENDSWITCH
			
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING	//cleanup ped when ped is not dead or fleeing
		
			IF ( bPlayerInsideFilmStudio = FALSE )
				IF ( ped.fDistanceToTarget > 200.0 )
					
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
				
					REMOVE_CLIP_SET("move_injured_generic")
					REMOVE_ANIM_DICT("misscarsteal4@director_grip")
					
					CLEANUP_OBJECT(osPropChair.ObjectIndex, FALSE)				//cleanup prop that this ped is using
					
				ENDIF
			ENDIF
		
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING		//cleanup ped when ped is dead or fleeing
		
			IF IS_ENTITY_DEAD(ped.PedIndex)
			
				ped.eState = PED_STATE_DEAD
			
				IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
					ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
					UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
				ENDIF
			ENDIF
		
			IF ( ped.fDistanceToTarget > 200.0 )
			
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
				
				REMOVE_CLIP_SET("move_injured_generic")
				REMOVE_ANIM_DICT("misscarsteal4@director_grip")
				
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_GRIP_1(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF 	DOES_ENTITY_EXIST(ped.PedIndex)
	
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
	
		#IF IS_DEBUG_BUILD
			ped.sDebugName = "FCP_GRIP_1"
			IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				SET_PED_NAME_DEBUG(ped.PedIndex, "FCP_GRIP_1")
			ENDIF
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
			
				IF ( ped.eState != PED_STATE_FLEEING )
					IF ( CST4_DSW_TRIGGERED = TRUE )
					OR ( bSecurityAlerted = TRUE )
					OR ( bDirectorSceneDisrupted = TRUE )
					OR IS_PED_DEAD_OR_DYING(ped.PedIndex)
					OR IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)					
						GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
					ENDIF
				ENDIF
			
				SWITCH ped.eState
				
					CASE PED_STATE_PLAYING_ANIM_1
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "beginning_loop_grip")
							
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@director_grip", "beginning_loop_grip",
														osPropChair.vPosition, osPropChair.vRotation,
														INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
														AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 
														0, EULER_YXZ, AIK_DISABLE_ARM_IK)
														
							ELSE
				
								ped.bHasTask = TRUE
								
							ENDIF
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "beginning_loop_grip")
						
							SET_PED_CAPSULE(ped.PedIndex, 0.6)
							SET_PED_RESET_FLAG(ped.PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
							
							IF (psFilmCrew[FCP_DIRECTOR].eState = PED_STATE_PLAYING_ANIM_2)
								GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_2)
							ENDIF
							
						ENDIF
						
					BREAK
					
					CASE PED_STATE_PLAYING_ANIM_2
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "react_grip")
								
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@director_grip", "react_grip",
														osPropChair.vPosition, osPropChair.vRotation,
														NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1,
														AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME, 
														0, EULER_YXZ, AIK_DISABLE_ARM_IK)
														
							ELSE

								ped.bHasTask = TRUE
								
							ENDIF
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "react_grip")
						
							FLOAT fAnimationPhase
						
							SET_PED_CAPSULE(ped.PedIndex, 0.6)
							SET_PED_RESET_FLAG(ped.PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
							
							fAnimationPhase = GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@director_grip", "react_grip")
						
							IF DOES_ENTITY_EXIST(osPropClipboard.ObjectIndex)
								IF NOT IS_ENTITY_DEAD(osPropClipboard.ObjectIndex)
									IF IS_ENTITY_ATTACHED(osPropClipboard.ObjectIndex)
										IF ( fAnimationPhase >= 0.6)
											DETACH_ENTITY(osPropClipboard.ObjectIndex, FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF

							IF (psFilmCrew[FCP_DIRECTOR].eState = PED_STATE_PLAYING_ANIM_3)
								GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_3)
							ENDIF
							
						ENDIF
					
					BREAK
					
					CASE PED_STATE_PLAYING_ANIM_3
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "end_loop_grip")
								
								TASK_PLAY_ANIM_ADVANCED(ped.PedIndex, "misscarsteal4@director_grip", "end_loop_grip",
														osPropChair.vPosition, osPropChair.vRotation,
														NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
														AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET |
														AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 
														0, EULER_YXZ, AIK_DISABLE_ARM_IK)
														
							ELSE
				
								ped.bHasTask = TRUE
								
							ENDIF
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "end_loop_grip")
						
							SET_PED_CAPSULE(ped.PedIndex, 0.6)
							SET_PED_RESET_FLAG(ped.PedIndex, PRF_ExpandPedCapsuleFromSkeleton, TRUE)

						ENDIF
					
					BREAK
					
					CASE PED_STATE_FLEEING
					
						IF ( ped.bHasTask = FALSE )
						
							bDirectorSceneDisrupted = TRUE
						
							IF NOT IS_PED_RAGDOLL(ped.PedIndex)
					
								CLEAR_PED_TASKS(ped.PedIndex)
								
								SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
								
								SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
								SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
								SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
						
								IF DOES_ENTITY_EXIST(osPropClipboard.ObjectIndex)
									IF NOT IS_ENTITY_DEAD(osPropClipboard.ObjectIndex)
										IF IS_ENTITY_ATTACHED(osPropClipboard.ObjectIndex)
											DETACH_ENTITY(osPropClipboard.ObjectIndex, FALSE)
										ENDIF
									ENDIF
								ENDIF
								
								TASK_SMART_FLEE_PED(ped.PedIndex, TargetPedIndex, 300, -1)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
								
								ped.bHasTask = TRUE
							ENDIF
							
						ENDIF
					
					BREAK
				
				ENDSWITCH
			
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING	//cleanup ped when ped is not dead or fleeing
		
			IF ( bPlayerInsideFilmStudio = FALSE )
				IF ( ped.fDistanceToTarget > 200.0 )
					
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)

					REMOVE_ANIM_DICT("misscarsteal4@director_grip")
					
					CLEANUP_OBJECT(osPropClipboard.ObjectIndex, FALSE)			//cleanup prop that this ped is using
					
				ENDIF
			ENDIF
		
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING
		
			IF IS_ENTITY_DEAD(ped.PedIndex)
			
				IF DOES_ENTITY_EXIST(osPropClipboard.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osPropClipboard.ObjectIndex)
						IF IS_ENTITY_ATTACHED(osPropClipboard.ObjectIndex)
							DETACH_ENTITY(osPropClipboard.ObjectIndex, FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				ped.eState = PED_STATE_DEAD
			
				IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
					ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
					UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
				ENDIF
			ENDIF
		
			IF ( ped.fDistanceToTarget > 200.0 )
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
				CLEANUP_OBJECT(osPropClipboard.ObjectIndex, FALSE)			//cleanup prop that this ped is using
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_FILM_CREW_PED(PED_STRUCT &ped, PED_INDEX TargetPedIndex, VEHICLE_INDEX TargetVehicleIndex,
						  VECTOR vScenarioPosition, STRING sScenarioName, BOOL bShouldBlockPlayer #IF IS_DEBUG_BUILD, STRING DebugName #ENDIF)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
	
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
	
		#IF IS_DEBUG_BUILD
			ped.sDebugName = DebugName
			IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				SET_PED_NAME_DEBUG(ped.PedIndex, DebugName)
			ENDIF
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
			
				CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, 1000)
			
				SWITCH ped.eState
				
					CASE PED_STATE_STARTING_SCENARIO_WARP

						IF ( ped.bHasTask = FALSE )
							IF NOT IS_PED_USING_SCENARIO(ped.PedIndex, sScenarioName)
								IF DOES_SCENARIO_EXIST_IN_AREA(vScenarioPosition, 1.5, FALSE)
									CLEAR_PED_TASKS(ped.PedIndex)
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(ped.PedIndex, vScenarioPosition, 1.5)
								ENDIF
								ped.iTimer = GET_GAME_TIMER()
								ped.bHasTask = TRUE
							ENDIF
						ENDIF
						
						IF ( ped.bHasTask = TRUE )
							IF 	IS_PED_USING_SCENARIO(ped.PedIndex, sScenarioName)
							AND IS_PED_ACTIVE_IN_SCENARIO(ped.PedIndex)
								GO_TO_PED_STATE(ped, PED_STATE_USING_SCENARIO)
							ELSE
								IF HAS_TIME_PASSED(3000, ped.iTimer)
									GO_TO_PED_STATE(ped, PED_STATE_STARTING_SCENARIO_WARP)
								ENDIF
							ENDIF
						ENDIF
						
						IF ( CST4_DSW_TRIGGERED = TRUE )
						OR ( bDirectorSceneDisrupted = TRUE )
						OR IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
						
					BREAK
					
					CASE PED_STATE_USING_SCENARIO
				
						IF ( ped.bHasTask = FALSE )
							//
							ped.bHasTask = TRUE
						ENDIF
						
						IF NOT IS_PED_USING_SCENARIO(ped.PedIndex, sScenarioName)
							GO_TO_PED_STATE(ped, PED_STATE_STARTING_SCENARIO_WARP)
						ENDIF
						
						IF ( CST4_DSW_TRIGGERED = TRUE )
						OR ( bDirectorSceneDisrupted = TRUE )
						OR IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE AND ped.fDistanceToTarget < 15.0 )
							WEAPON_TYPE eCurrentPlayerWeapon
						
							IF IS_PED_SHOOTING(TargetPedIndex)
							OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped.PedIndex)
							OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped.PedIndex)
							OR GET_CURRENT_PED_WEAPON(TargetPedIndex, eCurrentPlayerWeapon)
								GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
							ENDIF
						ENDIF
						
						IF ( bShouldBlockPlayer = TRUE )
							IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_USING(TargetPedIndex))
								IF GET_VEHICLE_PED_IS_USING(TargetPedIndex) = TargetVehicleIndex
									IF GET_DISTANCE_BETWEEN_ENTITIES(TargetPedIndex, TargetVehicleIndex) < 2.0
										GO_TO_PED_STATE(ped, PED_STATE_MOVING_TO_LOCATION)
									ENDIF
								ENDIF
							ENDIF
						
							IF IS_PED_IN_THIS_VEHICLE(TargetPedIndex, TargetVehicleIndex, TRUE)
								GO_TO_PED_STATE(ped, PED_STATE_MOVING_TO_LOCATION)
							ENDIF
						ENDIF
						
					BREAK
					
					CASE PED_STATE_MOVING_TO_LOCATION
				
						IF ( ped.bHasTask = FALSE )
						
							REQUEST_ANIM_DICT("misscarsteal4@director_grip")
						
							IF HAS_ANIM_DICT_LOADED("misscarsteal4@director_grip")
						
								SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(ped.PedIndex)
								
								CLEAR_PED_TASKS(ped.PedIndex)
								SET_ENTITY_HEALTH(ped.PedIndex, 105)
								
								VECTOR vDestination, vRotation
								
								vDestination = GET_ANIM_INITIAL_OFFSET_POSITION("misscarsteal4@director_grip", "mcs_2_loop_grip1", vFilmCrewPedsScenePosition,
																				vFilmCrewPedsSceneRotation)
															
								vRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("misscarsteal4@director_grip", "mcs_2_loop_grip1", vFilmCrewPedsScenePosition,
																			 vFilmCrewPedsSceneRotation)


								vDestination = << -1175.94, -508.15, 34.57 >>

								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(ped.PedIndex), vDestination) < 7.5
					
									SEQUENCE_INDEX SequenceIndex
									CLEAR_SEQUENCE_TASK(SequenceIndex)
									OPEN_SEQUENCE_TASK(SequenceIndex)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, TargetPedIndex)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDestination, PEDMOVE_RUN/*1.35*/, DEFAULT_TIME_BEFORE_WARP * 2, 0.0, ENAV_ACCURATE_WALKRUN_START | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, vRotation.z)
									CLOSE_SEQUENCE_TASK(SequenceIndex)
									TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
									CLEAR_SEQUENCE_TASK(SequenceIndex)
																		
									ped.bHasTask = TRUE
									
								ELSE
								
									GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
								
								ENDIF
								
							ENDIF
							
						ENDIF
						
						IF ( ped.bHasTask = TRUE )
						
							IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(ped.PedIndex), << -1175.56, -507.77, 34.57 >>) < 1.5
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "mcs_2_loop_grip1")
										TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "mcs_2_loop_grip1", WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
										TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
									ENDIF
								ENDIF
							ENDIF
							
							IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
								IF IS_PED_IN_THIS_VEHICLE(TargetPedIndex, TargetVehicleIndex)
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vsPlayersCar.VehicleIndex), vsPlayersCar.vPosition) > 1.75
										GO_TO_PED_STATE(ped, PED_STATE_JACKING_PLAYER)
									ENDIF
								ENDIF
							ENDIF
							
							IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GENERIC_FRIGHTENED_HIGH", "A_M_Y_Vinewood_03_White_FULL_01 ", SPEECH_PARAMS_FORCE_SHOUTED)
								GO_TO_PED_STATE(ped, PED_STATE_BLOCKING_PLAYER)
							ENDIF
						
						ENDIF
						
					BREAK
					
					CASE PED_STATE_BLOCKING_PLAYER
				
						IF ( ped.bHasTask = FALSE )
							SET_ENTITY_HEALTH(ped.PedIndex, 105)
							SET_PED_CAN_BE_TARGETTED(ped.PedIndex, TRUE)
							TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "mcs_2_loop_grip1", WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_LOOPING)
							TASK_LOOK_AT_ENTITY(ped.PedIndex, TargetPedIndex, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)						
							ped.iTimer = GET_GAME_TIMER()
							ped.iProgress = 0
							ped.bHasTask = TRUE
						ENDIF
						
						IF ped.iProgress = 0
							IF HAS_TIME_PASSED(250, ped.iTimer)
								CLEAR_PED_SECONDARY_TASK(ped.PedIndex)
								ped.iProgress = 1
							ENDIF
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@director_grip", "mcs_2_loop_grip1")
							IF HAS_TIME_PASSED(5000, ped.iTimer)
								IF IS_VEHICLE_DRIVEABLE(TargetVehicleIndex)
									GO_TO_PED_STATE(ped, PED_STATE_JACKING_PLAYER)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(TargetVehicleIndex)
							IF NOT IS_PED_IN_VEHICLE(TargetPedIndex, TargetVehicleIndex)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ELSE
								IF ( ped.fDistanceToTarget < 2.25 )
									GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
								ENDIF
							ENDIF
						ENDIF
					
					BREAK
					
					CASE PED_STATE_JACKING_PLAYER
				
						IF ( ped.bHasTask = FALSE )
						
							TASK_CLEAR_LOOK_AT(ped.PedIndex)
						
							TASK_ENTER_VEHICLE(ped.PedIndex, TargetVehicleIndex, -1, VS_DRIVER, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY | ECF_RESUME_IF_INTERRUPTED | ECF_JUST_PULL_PED_OUT | ECF_JACK_ANYONE | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
							
							ped.bHasTask = TRUE
						ENDIF
					
						IF IS_VEHICLE_DRIVEABLE(TargetVehicleIndex)
							IF NOT IS_PED_IN_VEHICLE(TargetPedIndex, TargetVehicleIndex)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
						ENDIF
					
					BREAK
					
					CASE PED_STATE_COMBAT_ON_FOOT
						IF ( ped.bHasTask = FALSE )
						
							TASK_CLEAR_LOOK_AT(ped.PedIndex)
						
							CLEAR_PED_SECONDARY_TASK(ped.PedIndex)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, TRUE)
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_FIGHT_PROFICIENCY, 1.0)
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
							ped.bHasTask = TRUE
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
							IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped.PedIndex)
							OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped.PedIndex)						
								WEAPON_TYPE eCurrentWeapon
								IF GET_CURRENT_PED_WEAPON(TargetPedIndex, eCurrentWeapon)
									IF ( eCurrentWeapon <> WEAPONTYPE_UNARMED)
										GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					BREAK
				
					CASE PED_STATE_FLEEING
						IF ( ped.bHasTask = FALSE )
													
							CLEAR_PED_TASKS(ped.PedIndex)							
							
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
							
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
							
							IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "SCREAM_PANIC", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE_SHOUTED)
							ENDIF
							
							TASK_SMART_FLEE_PED(ped.PedIndex, TargetPedIndex, 300, -1)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
							
							bFilmCrewGripAlerted = TRUE
							
							ped.bHasTask = TRUE
						ENDIF
					
					BREAK
				
				ENDSWITCH
			
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
		
			IF ( bPlayerInsideFilmStudio = FALSE )
				IF ( ped.fDistanceToTarget > 300.0 )
					GO_TO_PED_STATE(ped, PED_STATE_DEAD)
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting ped with debug name ", ped.sDebugName, " as no longer needed due to player distance.")
					#ENDIF
				ENDIF
			ENDIF
			
			IF ( ped.eState = PED_STATE_FLEEING )
				IF ( ped.fDistanceToTarget > 200.0 )
					GO_TO_PED_STATE(ped, PED_STATE_DEAD)
					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting ped with debug name ", ped.sDebugName, " as no longer needed due to fleeing distance.")
					#ENDIF
				ENDIF
			ENDIF
		
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex)
					
			ped.eState = PED_STATE_DEAD
		
			IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
				ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
				UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
			ENDIF
		
			IF ( ped.fDistanceToTarget > 200.0 )
				GO_TO_PED_STATE(ped, PED_STATE_DEAD)
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting ped with debug name ", ped.sDebugName, " as no longer needed due to dead distance.")
				#ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_DIRECTOR_AND_GRIP(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			iProgress++
		
		BREAK
		
		CASE 1

			REQUEST_CLIP_SET("move_injured_generic")
			REQUEST_ANIM_DICT("misscarsteal4@director_grip")
		
			IF 	HAS_CLIP_SET_LOADED("move_injured_generic")
			AND HAS_ANIM_DICT_LOADED("misscarsteal4@director_grip")

				IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osPropChair)
				AND HAS_MISSION_OBJECT_BEEN_CREATED(osPropClipboard)
				
					IF	HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_GRIP_1], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
					AND HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_DIRECTOR], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
					
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_DIRECTOR].PedIndex)
							SET_PED_PROP_INDEX(psFilmCrew[FCP_DIRECTOR].PedIndex, ANCHOR_EYES, 0, 0)
							ADD_PED_FOR_DIALOGUE(CST4Conversation, 7, psFilmCrew[FCP_DIRECTOR].PedIndex, "CST4DIRECTOR")
						ENDIF
						
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_1].PedIndex)						
							SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_1].PedIndex, PED_COMP_HEAD, 	1, 1, 0) //(head)
							SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_1].PedIndex, PED_COMP_TORSO, 	1, 1, 0) //(uppr)
							SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_1].PedIndex, PED_COMP_LEG, 		0, 1, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_1].PedIndex, PED_COMP_SPECIAL, 	2, 0, 0) //(accs)
						ENDIF
					
						IF DOES_ENTITY_EXIST(osPropClipboard.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osPropClipboard.ObjectIndex)
								IF NOT IS_ENTITY_ATTACHED(osPropClipboard.ObjectIndex)
									IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_1].PedIndex)
										ATTACH_ENTITY_TO_ENTITY(osPropClipboard.ObjectIndex, psFilmCrew[FCP_GRIP_1].PedIndex,
																GET_PED_BONE_INDEX(psFilmCrew[FCP_GRIP_1].PedIndex, BONETAG_PH_L_HAND),
																<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_1], 	PED_STATE_PLAYING_ANIM_1)
						GO_TO_PED_STATE(psFilmCrew[FCP_DIRECTOR], 	PED_STATE_PLAYING_ANIM_1)
					
						SET_PED_VISUAL_FIELD_PROPERTIES(psFilmCrew[FCP_GRIP_1].PedIndex, 30.0, 5.0, 90.0)
						SET_PED_VISUAL_FIELD_PROPERTIES(psFilmCrew[FCP_DIRECTOR].PedIndex, 30.0, 5.0, 90.0)
					
						iProgress++
					
					ENDIF
					
				ENDIF
			ENDIF

		BREAK
		
		CASE 2
		
			MANAGE_DIRECTOR(psFilmCrew[FCP_DIRECTOR], PLAYER_PED_ID())
			MANAGE_GRIP_1(psFilmCrew[FCP_GRIP_1], PLAYER_PED_ID())

		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_FILM_CREW(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
			
			iProgress++
			
		BREAK
		
		CASE 1
		
			//this needs a better check
			//what if ped is killed or flees in the get to car stage and the player returns to the set and moves the mission on
			//it would probably recreate the ped again
		
			IF 	( psFilmCrew[FCP_GRIP_2].eState = PED_STATE_DEAD OR HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_GRIP_2], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE) )
			AND ( psFilmCrew[FCP_GRIP_3].eState = PED_STATE_DEAD OR HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_GRIP_3], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE) )
			AND ( psFilmCrew[FCP_GRIP_4].eState = PED_STATE_DEAD OR HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_GRIP_4], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE) )
			AND ( psFilmCrew[FCP_GRIP_5].eState = PED_STATE_DEAD OR HAS_MISSION_PED_BEEN_CREATED(psFilmCrew[FCP_GRIP_5], FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE) )
	
				IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_2].PedIndex)
					SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_2].PedIndex, PED_COMP_HEAD, 	0, 1, 0) //(head)
					SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_2].PedIndex, PED_COMP_TORSO, 	0, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_2].PedIndex, PED_COMP_LEG, 		0, 2, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(psFilmCrew[FCP_GRIP_2].PedIndex, PED_COMP_SPECIAL, 	0, 0, 0) //(accs)
				ENDIF
				
				SWITCH eMissionStage
				
					CASE MISSION_STAGE_GET_TO_ACTOR
					CASE MISSION_STAGE_GET_TO_CAR
					
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_2].PedIndex)
							GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_2], PED_STATE_STARTING_SCENARIO_WARP)
						ENDIF
						
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_3].PedIndex)
							GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_3], PED_STATE_STARTING_SCENARIO_WARP)
						ENDIF
						
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_4].PedIndex)
							GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_4], PED_STATE_STARTING_SCENARIO_WARP)
						ENDIF
						
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_5].PedIndex)
							GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_5], PED_STATE_STARTING_SCENARIO_WARP)
						ENDIF
						
					BREAK
					
					CASE MISSION_STAGE_ESCAPE_FILM_SET
					
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_2].PedIndex)
							IF 	psFilmCrew[FCP_GRIP_2].eState != PED_STATE_DEAD
							AND psFilmCrew[FCP_GRIP_2].eState != PED_STATE_FLEEING
							AND psFilmCrew[FCP_GRIP_2].eState != PED_STATE_MOVING_TO_LOCATION
							AND psFilmCrew[FCP_GRIP_2].eState != PED_STATE_BLOCKING_PLAYER
								GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_2], PED_STATE_MOVING_TO_LOCATION)
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_3].PedIndex)
							IF 	psFilmCrew[FCP_GRIP_3].eState != PED_STATE_DEAD
							AND psFilmCrew[FCP_GRIP_3].eState != PED_STATE_FLEEING
								GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_3], PED_STATE_STARTING_SCENARIO_WARP)
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_4].PedIndex)
							IF 	psFilmCrew[FCP_GRIP_4].eState != PED_STATE_DEAD
							AND psFilmCrew[FCP_GRIP_4].eState != PED_STATE_FLEEING
								GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_4], PED_STATE_STARTING_SCENARIO_WARP)
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(psFilmCrew[FCP_GRIP_5].PedIndex)
							IF 	psFilmCrew[FCP_GRIP_5].eState != PED_STATE_DEAD
							AND psFilmCrew[FCP_GRIP_5].eState != PED_STATE_FLEEING
								GO_TO_PED_STATE(psFilmCrew[FCP_GRIP_5], PED_STATE_STARTING_SCENARIO_WARP)
							ENDIF
						ENDIF
					
					BREAK
					
				ENDSWITCH
					
				iProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			MANAGE_FILM_CREW_PED(psFilmCrew[FCP_GRIP_2], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
								 <<-1170.0442, -511.5512, 34.5666>>/*<< -1172.57, -506.53, 34.57 >>*/, "PROP_HUMAN_MOVIE_STUDIO_LIGHT", TRUE
								 #IF IS_DEBUG_BUILD, "FCP_GRIP_2" #ENDIF)
			MANAGE_FILM_CREW_PED(psFilmCrew[FCP_GRIP_3], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
								 << -1186.86, -505.80, 34.58 >>, "PROP_HUMAN_MOVIE_STUDIO_LIGHT", FALSE
								 #IF IS_DEBUG_BUILD, "FCP_GRIP_3" #ENDIF)
			MANAGE_FILM_CREW_PED(psFilmCrew[FCP_GRIP_4], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
								 << -1178.57, -488.17, 34.55 >>, "PROP_HUMAN_MOVIE_BULB", FALSE
								 #IF IS_DEBUG_BUILD, "FCP_GRIP_4" #ENDIF)
			MANAGE_FILM_CREW_PED(psFilmCrew[FCP_GRIP_5], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex,
								 << -1175.81, -483.17, 34.54 >>, "PROP_HUMAN_MOVIE_STUDIO_LIGHT", FALSE
								 #IF IS_DEBUG_BUILD, "FCP_GRIP_5" #ENDIF)
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_CONVERSATIONS_FOR_ACTOR(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)

			SWITCH ped.eState

				CASE PED_STATE_ARGUING
				CASE PED_STATE_PLAYING_ANIM_1
				CASE PED_STATE_PLAYING_ANIM_2
				CASE PED_STATE_PLAYING_ANIM_3
				CASE PED_STATE_PLAYING_ANIM_4
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSARG")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_berating_assistant")
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSARG", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("CST4_MSARG", TRUE)
									SET_LABEL_AS_TRIGGERED("CST4_MSVOC1", FALSE)
									SET_LABEL_AS_TRIGGERED("CST4_MSVOC2", FALSE)
									SET_LABEL_AS_TRIGGERED("CST4_MSVOC3", FALSE)
									SET_LABEL_AS_TRIGGERED("CST4_MSVOC4", FALSE)									
									REPLAY_RECORD_BACK_FOR_TIME(3.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)									
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSVOC1_1")
						IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1")
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1") >= 0.1
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1") <= 0.375
							IF ( ped.fDistanceToTarget < 35.0 )
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC1", CONV_PRIORITY_MEDIUM)
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC1", "CST4_MSVOC1_1", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGH)
											
										ENDIF				
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSVOC1_2")
						IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1")
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1") >= 0.430
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1") <= 0.880
							IF ( ped.fDistanceToTarget < 35.0 )
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC1", CONV_PRIORITY_MEDIUM)
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC1", "CST4_MSVOC1_2", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGH)
											
										ENDIF				
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
							ENDIF
						ENDIF
					ENDIF
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSVOC2_1")
						IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2")
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2") >= 0.105
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2") <= 0.375
							IF ( ped.fDistanceToTarget < 35.0 )
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC2", CONV_PRIORITY_MEDIUM)
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC2", "CST4_MSVOC2_1", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGH)
											
										ENDIF				
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSVOC2_2")
						IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2")
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2") >= 0.390
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2") <= 0.840
							IF ( ped.fDistanceToTarget < 35.0 )
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC2", CONV_PRIORITY_MEDIUM)
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC2", "CST4_MSVOC2_2", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGH)
											
										ENDIF				
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
							ENDIF
						ENDIF
					ENDIF
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSVOC3_1")
						IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3")
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3") >= 0.100
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3") <= 0.362
							IF ( ped.fDistanceToTarget < 35.0 )
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC3", CONV_PRIORITY_MEDIUM)
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC3", "CST4_MSVOC3_1", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGH)
											
										ENDIF				
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSVOC3_2")
						IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3")
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3") >= 0.452
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3") <= 0.900
							IF ( ped.fDistanceToTarget < 35.0 )
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC3", CONV_PRIORITY_MEDIUM)
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC3", "CST4_MSVOC3_2", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGH)
											
										ENDIF				
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
							ENDIF
						ENDIF
					ENDIF
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSVOC4_1")
						IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4")
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4") >= 0.100
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4") <= 0.365
							IF ( ped.fDistanceToTarget < 35.0 )
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC4", CONV_PRIORITY_MEDIUM)
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC4", "CST4_MSVOC4_1", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGH)
											
										ENDIF				
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSVOC4_2")
						IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4")
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4") >= 0.460
						AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4") <= 0.828
							IF ( ped.fDistanceToTarget < 35.0 )
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										//IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC4", CONV_PRIORITY_MEDIUM)
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSVOC4", "CST4_MSVOC4_2", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", TRUE)
											SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", TRUE)
											
											REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGH)
											
										ENDIF				
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC1_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC2_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_1", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC3_2", FALSE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_1", TRUE)
								SET_LABEL_AS_TRIGGERED("CST4_MSVOC4_2", TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_FLEEING(ped.PedIndex)
					OR IS_PED_IN_COMBAT(ped.PedIndex)
					OR IS_PED_RESPONDING_TO_EVENT(ped.PedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped.PedIndex, TargetPedIndex)
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_SCRIPTED_SPEECH_PLAYING(ped.PedIndex)
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
						ENDIF
						IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "SCREAM_PANIC", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
						ENDIF
					ENDIF
					
				BREAK
				
				CASE PED_STATE_INTIMIDATION
				
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_SCRIPTED_SPEECH_PLAYING(ped.PedIndex)
							
							TEXT_LABEL_23 ConversationRoot
							ConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							
							IF NOT IS_STRING_NULL_OR_EMPTY(ConversationRoot)
								IF 	NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSSEC")
								AND NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSSPO")
								AND NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSFIG")
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSSPO")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSSPO", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("CST4_MSSPO", TRUE)
								ped.iTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE PED_STATE_COMBAT_ON_FOOT
				
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_SCRIPTED_SPEECH_PLAYING(ped.PedIndex)
							
							TEXT_LABEL_23 ConversationRoot
							ConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							
							IF NOT IS_STRING_NULL_OR_EMPTY(ConversationRoot)
								IF 	NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSSEC")
								AND NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSSPO")
								AND NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSFIG")
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_MSFIG")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSFIG", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("CST4_MSFIG", TRUE)
								ped.iConversationTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ELSE
					
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_PED_IN_COMBAT(ped.PedIndex)
								IF ( ped.fDistanceToTarget < 30.0 )
									IF HAS_TIME_PASSED(5000, ped.iConversationTimer)
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSSEC", CONV_PRIORITY_MEDIUM)
												ped.iConversationTimer = GET_GAME_TIMER()
											ENDIF
										ELSE
											IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "CST4_AYAA", "CST4ACTOR", SPEECH_PARAMS_FORCE)
												bSpawnActorSecurity = TRUE
												ped.iConversationTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF IS_SCRIPTED_SPEECH_PLAYING(ped.PedIndex)
								
									TEXT_LABEL_23 LocalConversationRoot
									LocalConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
									
									IF NOT IS_STRING_NULL_OR_EMPTY(LocalConversationRoot)
										IF ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_MSSEC")
											bSpawnActorSecurity = TRUE	//spawn securirty when this converstion root is playing
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
						
					ENDIF

				BREAK
				
				CASE PED_STATE_FLEEING
				
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_SCRIPTED_SPEECH_PLAYING(ped.PedIndex)
							
							TEXT_LABEL_23 ConversationRoot
							ConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							
							IF NOT IS_STRING_NULL_OR_EMPTY(ConversationRoot)
								IF 	NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSSEC")
								AND NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSSPO")
								AND NOT ARE_STRINGS_EQUAL(ConversationRoot, "CST4_MSFIG")
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
					IF IS_PED_FLEEING(ped.PedIndex)
					OR IS_PED_IN_COMBAT(ped.PedIndex)
					
						IF ( ped.fDistanceToTarget < 30.0 )
							IF HAS_TIME_PASSED(3000, ped.iConversationTimer)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MSSEC", CONV_PRIORITY_MEDIUM)
											bSpawnActorSecurity = TRUE
											ped.iConversationTimer = GET_GAME_TIMER()
										ENDIF
									ELSE
										IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "CST4_AYAA", "CST4ACTOR", SPEECH_PARAMS_FORCE)
											bSpawnActorSecurity = TRUE
											ped.iConversationTimer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				BREAK
				
			ENDSWITCH
			
			IF IS_PED_BEING_STUNNED(ped.PedIndex)
			OR IS_PED_DEAD_OR_DYING(ped.PedIndex)
			OR IS_PED_BEING_STEALTH_KILLED(ped.PedIndex)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_PED_IN_CURRENT_CONVERSATION(ped.PedIndex)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
			ENDIF

		ENDIF
	ENDIF
	
ENDPROC

PROC MANAGE_ACTOR_DURING_GET_TO_ACTOR(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)

		#IF IS_DEBUG_BUILD
			ped.sDebugName = "Actor"
			IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				SET_PED_NAME_DEBUG(ped.PedIndex, "Actor")
			ENDIF
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
		
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
		
		CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, 750)
	
		MANAGE_CONVERSATIONS_FOR_ACTOR(ped, TargetPedIndex)
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)

				SET_PED_RESET_FLAG(ped.PedIndex, PRF_ForceInjuryAfterStunned, TRUE)
				SET_PED_RESET_FLAG(ped.PedIndex, PRF_PreventFailedMeleeTakedowns, TRUE)

				SET_PED_RESET_FLAG(TargetPedIndex, PRF_SuppressLethalMeleeActions, TRUE)	//suppress player ped from lethal melee actions
				
				IF ped.eState != PED_STATE_FLEEING
					IF ( bSecurityAlerted = TRUE AND ped.eState != PED_STATE_COMBAT_ON_FOOT )
					OR IS_PED_FLEEING(ped.PedIndex) 
						GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
					ENDIF
				ENDIF

				SWITCH ped.eState
				
					CASE PED_STATE_IDLE
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_berating_loop")
								REQUEST_ANIM_DICT("misscarsteal4@actor")
								IF HAS_ANIM_DICT_LOADED("misscarsteal4@actor")
									TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_berating_loop",
												   SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
												   
									SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
												   
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
								ENDIF
								
							ELSE
								
								CREATE_TRACKED_POINT_FOR_COORD(iActorTrackedPoint, GET_PED_BONE_COORDS(ped.PedIndex, BONETAG_HEAD, << 0.0, 0.0, 0.0 >>), 0.5)
							
								SET_PED_VISUAL_FIELD_PROPERTIES(ped.PedIndex, 25.0, 5.0, 90.0, -45.0, 45.0)
								
								SET_ENTITY_HEALTH(ped.PedIndex, 150)
								
								SET_PED_CONFIG_FLAG(ped.PedIndex, PCF_DisableHurt, TRUE)
								SET_PED_CONFIG_FLAG(ped.PedIndex, PCF_AvoidTearGas, TRUE)
								SET_PED_CONFIG_FLAG(ped.PedIndex, PCF_AlwaysSeeApproachingVehicles, TRUE)
								SET_PED_CONFIG_FLAG(ped.PedIndex, PCF_DisableGoToWritheWhenInjured, TRUE)								
				
								ped.bHasTask = TRUE
							ENDIF
						ENDIF
						
						IF ( bRepositionActor = FALSE )
							IF IS_ENTITY_AT_COORD(TargetPedIndex, << -1132.33, -490.46, 41.16 >>, << 8.0, 8.0, 8.0>>)
							OR IS_ENTITY_IN_ANGLED_AREA(TargetPedIndex, << -1118.82, -504.05, 22.36 >>, << -1207.15, -576.01, 44.20 >>, 30.0) 
								WARP_PED(ped.PedIndex, << -1109.36, -503.38, 34.26 >>, 292.4256, FALSE, FALSE, FALSE)
								bRepositionActor = TRUE
							ENDIF
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(TargetPedIndex, <<-1141.908203,-523.151123,27.854542>>, <<-1069.740967,-490.765381,49.649647>>, 46.0)
						OR IS_ENTITY_AT_COORD(TargetPedIndex, << -1132.34, -490.47, 41.16 >>, << 8.0, 8.0, 8.0>>)							//alley behind the trailer
							START_AUDIO_SCENE("CAR_3_TAKE_OUT_ACTOR")
							GO_TO_PED_STATE(ped, PED_STATE_ARGUING)
						ENDIF
						
					BREAK
					
					CASE PED_STATE_ARGUING
				
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_berating_assistant")
								IF HAS_ANIM_DICT_LOADED("misscarsteal4@actor")
									IF NOT IS_PED_INJURED(psAssistant.PedIndex)
										TASK_LOOK_AT_ENTITY(ped.PedIndex, psAssistant.PedIndex, 15000, SLF_WHILE_NOT_IN_FOV)
									ENDIF
									TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_berating_assistant", SLOW_BLEND_IN, SLOW_BLEND_OUT)
								ELSE
									REQUEST_ANIM_DICT("misscarsteal4@actor")
								ENDIF
							ELSE
								ped.bHasTask = TRUE
							ENDIF
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_berating_assistant")
							IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_berating_assistant") > 0.995

								SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
									CASE 0
										GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_1)
									BREAK
									CASE 1
										GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_2)
									BREAK
									CASE 2
										GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_3)
									BREAK
									CASE 3
										GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_4)
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
						
						IF bActorSceneDisrupted = TRUE
						OR IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_IN_STEALTH_MOVEMENT(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF

						IF CAN_PED_HEAR_PED_IN_VEHICLE(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget)
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
						
						IF CAN_PED_HEAR_PED_ON_FOOT(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget)
							IF ( ped.bCanSeeTargetPed = FALSE )
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
							IF ( ped.fDistanceToTarget < 6.25 )
								GO_TO_PED_STATE(ped, PED_STATE_INTIMIDATION)
							ENDIF
						ENDIF
												
					BREAK
					
					CASE PED_STATE_PLAYING_ANIM_1
					CASE PED_STATE_PLAYING_ANIM_2
					CASE PED_STATE_PLAYING_ANIM_3
					CASE PED_STATE_PLAYING_ANIM_4
				
						IF ( ped.bHasTask = FALSE )
						
							SWITCH ped.eState
							
								CASE PED_STATE_PLAYING_ANIM_1
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1")						
										TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1",
													   SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_2
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2")						
										TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2",
													   SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_3
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3")						
										TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3",
													   SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_4
									IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4")						
										TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4",
													   SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									ELSE
										ped.bHasTask = TRUE
									ENDIF
								BREAK
							
							ENDSWITCH
							
						ENDIF
					
						IF ( ped.bHasTask = TRUE )
						
							SWITCH ped.eState
							
								CASE PED_STATE_PLAYING_ANIM_1
									IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1")
									AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_1") >= 0.988
										SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
											CASE 0
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_2)
											BREAK
											CASE 1
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_3)
											BREAK
											CASE 2
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_4)
											BREAK
										ENDSWITCH
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_2
									IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2")
									AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_2") >= 0.988
										SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
											CASE 0
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_1)
											BREAK
											CASE 1
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_3)
											BREAK
											CASE 2
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_4)
											BREAK
										ENDSWITCH
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_3
									IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3")
									AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_3") >= 0.988
										SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
											CASE 0
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_1)
											BREAK
											CASE 1
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_2)
											BREAK
											CASE 2
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_4)
											BREAK
										ENDSWITCH
									ENDIF
								BREAK
								
								CASE PED_STATE_PLAYING_ANIM_4
									IF 	IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4")
									AND	GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "actor_warming_up_loop_4") >= 0.988
										SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
											CASE 0
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_1)
											BREAK
											CASE 1
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_2)
											BREAK
											CASE 2
												GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_3)
											BREAK
										ENDSWITCH
									ENDIF
								BREAK
							
							ENDSWITCH
						
						ENDIF

						IF bActorSceneDisrupted = TRUE
						OR IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_IN_STEALTH_MOVEMENT(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF

						IF CAN_PED_HEAR_PED_IN_VEHICLE(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget)
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
						
						IF CAN_PED_HEAR_PED_ON_FOOT(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget)
							IF ( ped.bCanSeeTargetPed = FALSE )
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
							IF ( ped.fDistanceToTarget < 6.25 )
								GO_TO_PED_STATE(ped, PED_STATE_INTIMIDATION)
							ENDIF
						ENDIF

					BREAK
					
					CASE PED_STATE_INTIMIDATION
					
						IF ( ped.bHasTask = FALSE )
							
							CLEAR_PED_TASKS(ped.PedIndex)
							
							SEQUENCE_INDEX SequenceIndex
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							OPEN_SEQUENCE_TASK(SequenceIndex)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, TargetPedIndex)
								TASK_LOOK_AT_ENTITY(NULL, TargetPedIndex, 3000, SLF_FAST_TURN_RATE)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							
							ped.iTimer = GET_GAME_TIMER()
							ped.bHasTask = TRUE
						ENDIF
						
						IF HAS_LABEL_BEEN_TRIGGERED("CST4_MSSPO")
							IF HAS_TIME_PASSED(3500, ped.iTimer)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
						ENDIF
						
						IF IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_IN_STEALTH_MOVEMENT(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
					
					BREAK
										
					CASE PED_STATE_FLEEING
					
						IF ( ped.bHasTask = FALSE )
						
							IF DOES_BLIP_EXIST(ped.BlipIndex)
								REMOVE_BLIP(ped.BlipIndex)
							ENDIF
							
							SET_GLOBAL_BIT_SET(ACTOR_IGNORED)

							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLRETREAT)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, FALSE)
							
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
							
							IF NOT IS_PED_FLEEING(ped.PedIndex)		
								CLEAR_PED_TASKS(ped.PedIndex)
								TASK_SMART_FLEE_PED(ped.PedIndex, TargetPedIndex, 300, -1)
							ENDIF
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
							
							bActorSceneDisrupted = FALSE
							
							ped.iConversationTimer = 0
							ped.bHasTask = TRUE
						ENDIF
					
					BREAK
					
					CASE PED_STATE_COMBAT_ON_FOOT
					
						IF ( ped.bHasTask = FALSE )
						
							IF DOES_BLIP_EXIST(ped.BlipIndex)
								REMOVE_BLIP(ped.BlipIndex)
							ENDIF
						
							SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)

							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, TRUE)
							
							SET_COMBAT_FLOAT(ped.PedIndex, CCF_FIGHT_PROFICIENCY, 1.0)
							
							TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex, COMBAT_PED_PREVENT_CHANGING_TARGET, TASK_THREAT_RESPONSE_NONE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
							
							//bSpawnActorSecurity 	= TRUE	//actor will call security when his security dialogue plays
							bActorSceneDisrupted 	= TRUE
							
							ped.iConversationTimer = 0
							ped.bHasTask = TRUE
							
						ENDIF
					
					BREAK
				
				ENDSWITCH

			ENDIF
		
			
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING		//cleanup ped when ped is dead or fleeing
		
			IF IS_ENTITY_DEAD(ped.PedIndex)

				IF DOES_BLIP_EXIST(ped.BlipIndex)
					REMOVE_BLIP(ped.BlipIndex)			
				ENDIF
				
				IF ( ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP )
				
					ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
					
					ActorCauseOfDeathWeapon = GET_PED_CAUSE_OF_DEATH(ped.PedIndex)
									
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", ped.sDebugName, " was killed with weapon ", GET_WEAPON_NAME(GET_PED_CAUSE_OF_DEATH(ped.PedIndex)), ".")
					#ENDIF				
					
					UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
					
				ENDIF
				
			ENDIF
			
			IF ( ped.fDistanceToTarget > 200.0 )			
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
				REMOVE_ANIM_DICT("misscarsteal4@actor")
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_ASSISTANT_PED_DURING_GET_TO_ACTOR(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
	
		#IF IS_DEBUG_BUILD
			ped.sDebugName = "Assistant"
			IF NOT IS_ENTITY_DEAD(ped.PedIndex)
				SET_PED_NAME_DEBUG(ped.PedIndex, "Assistant")
			ENDIF
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
	
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, TargetPedIndex)
		
		CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, 500)
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
			IF NOT IS_PED_INJURED(TargetPedIndex)
			
				IF ( ped.eState != PED_STATE_FLEEING )
					IF bSecurityAlerted = TRUE
					OR IS_PED_FLEEING(ped.PedIndex)
						GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
					ENDIF
				ENDIF

				SWITCH ped.eState
				
					CASE PED_STATE_IDLE
				
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "assistant_loop")
								REQUEST_ANIM_DICT("misscarsteal4@actor")
								IF HAS_ANIM_DICT_LOADED("misscarsteal4@actor")
								
									SET_PED_VISUAL_FIELD_PROPERTIES(ped.PedIndex, 20.0, 5.0, 90.0)
									
									SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FIGHT, FALSE)

									TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actor", "assistant_loop",
												   SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
									
								ENDIF
							ELSE
								ped.bHasTask = TRUE
							ENDIF
						ENDIF
						
						IF ( bRepositionAssistant = FALSE )
							IF IS_ENTITY_AT_COORD(TargetPedIndex, << -1132.33, -490.46, 41.16 >>, << 8.0, 8.0, 8.0 >>)
							OR IS_ENTITY_IN_ANGLED_AREA(TargetPedIndex, << -1118.82, -504.05, 22.36 >>, << -1207.15, -576.01, 44.20 >>, 30.0)
								WARP_PED(ped.PedIndex, << -1109.1906, -503.3141, 34.26 >>, 120.0023, FALSE, FALSE, FALSE)
								bRepositionAssistant = TRUE
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(psActor.PedIndex)
							IF NOT IS_ENTITY_DEAD(psActor.PedIndex)
								IF psActor.eState = PED_STATE_ARGUING
									GO_TO_PED_STATE(ped, PED_STATE_ARGUING)
								ENDIF
							ENDIF
						ENDIF
						
						IF ( bActorSceneDisrupted = TRUE )
						OR IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							bSpawnActorSecurity 	= TRUE
							bActorSceneDisrupted 	= TRUE
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
							IF DOES_ENTITY_EXIST(psActor.PedIndex)
								IF IS_ENTITY_DEAD(psActor.PedIndex)
									bSpawnActorSecurity		= TRUE
									bActorSceneDisrupted 	= TRUE
									GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
								ELSE
									IF IS_PED_BEING_STUNNED(psActor.PedIndex)
									OR IS_PED_DEAD_OR_DYING(psActor.PedIndex)
									OR IS_PED_BEING_STEALTH_KILLED(psActor.PedIndex)
									OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psActor.PedIndex, TargetPedIndex)
									OR IS_ENTITY_TOUCHING_ENTITY(psActor.PedIndex, TargetPedIndex)
									OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), psActor.PedIndex)
									OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), psActor.PedIndex)
										bSpawnActorSecurity		= TRUE
										bActorSceneDisrupted 	= TRUE
										GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF

					BREAK
				
					CASE PED_STATE_ARGUING
					
						IF ( ped.bHasTask = FALSE )
							IF NOT IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "assistant_berated")
								TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@actor", "assistant_berated",
											   SLOW_BLEND_IN, WALK_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							ELSE
								ped.bHasTask = TRUE
							ENDIF
						ENDIF
					
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@actor", "assistant_berated")
							SET_ENTITY_ANIM_SPEED(ped.PedIndex, "misscarsteal4@actor", "assistant_berated", 1.125)
							IF ( GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@actor", "assistant_berated") > 0.995 )
								GO_TO_PED_STATE(ped, PED_STATE_MOVING_TO_LOCATION)
							ENDIF
						ENDIF
						
						IF ( bActorSceneDisrupted = TRUE )
						OR IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
						OR CAN_PED_SEE_PED_IN_STEALTH_MOVEMENT(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							bSpawnActorSecurity 	= TRUE
							bActorSceneDisrupted 	= TRUE
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
						
						IF ( ped.bCanSeeTargetPed = TRUE )
							IF DOES_ENTITY_EXIST(psActor.PedIndex)
								IF IS_ENTITY_DEAD(psActor.PedIndex)
									bSpawnActorSecurity		= TRUE
									bActorSceneDisrupted 	= TRUE
									GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
								ELSE
									IF IS_PED_BEING_STUNNED(psActor.PedIndex)
									OR IS_PED_DEAD_OR_DYING(psActor.PedIndex)
									OR IS_PED_BEING_STEALTH_KILLED(psActor.PedIndex)
									OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psActor.PedIndex, TargetPedIndex)
									OR IS_ENTITY_TOUCHING_ENTITY(psActor.PedIndex, TargetPedIndex)
									OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), psActor.PedIndex)
									OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), psActor.PedIndex)
										bSpawnActorSecurity		= TRUE
										bActorSceneDisrupted 	= TRUE
										GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
												
					BREAK
					
					CASE PED_STATE_MOVING_TO_LOCATION 
				
						IF ( ped.bHasTask = FALSE )
							CLEAR_PED_TASKS(ped.PedIndex)
							FORCE_PED_MOTION_STATE(ped.PedIndex, MS_ON_FOOT_RUN, FALSE)
							IF ( bRepositionAssistant = TRUE )
								TASK_FOLLOW_NAV_MESH_TO_COORD(ped.PedIndex, << -1075.86, -468.95, 35.64 >>, PEDMOVE_RUN, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 309.4294)
							ELSE
								TASK_FOLLOW_NAV_MESH_TO_COORD(ped.PedIndex, << -1155.5319, -521.8391, 31.5830 >>, PEDMOVE_RUN, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 74.1956)
							ENDIF
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
							ped.bHasTask = TRUE
						ENDIF
						
						IF ( GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK )
							GO_TO_PED_STATE(ped, PED_STATE_STANDING_AT_LOCATION)
						ENDIF
						
						IF IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							bSecurityAlerted		= TRUE
							//bSpawnActorSecurity 	= TRUE
							//bActorSceneDisrupted 	= TRUE
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF

						IF ( ped.bCanSeeTargetPed = TRUE )
							IF DOES_ENTITY_EXIST(psActor.PedIndex)
								IF IS_ENTITY_DEAD(psActor.PedIndex)
									bSecurityAlerted		= TRUE
									//bSpawnActorSecurity	= TRUE
									//bActorSceneDisrupted 	= TRUE
									GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
								ELSE
									IF IS_PED_BEING_STUNNED(psActor.PedIndex)
									OR IS_PED_DEAD_OR_DYING(psActor.PedIndex)
									OR IS_PED_BEING_STEALTH_KILLED(psActor.PedIndex)
									OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psActor.PedIndex, TargetPedIndex)
									OR IS_ENTITY_TOUCHING_ENTITY(psActor.PedIndex, TargetPedIndex)
									OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), psActor.PedIndex)
									OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), psActor.PedIndex)
										bSecurityAlerted		= TRUE
										//bSpawnActorSecurity	= TRUE
										//bActorSceneDisrupted 	= TRUE
										GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
					BREAK
					
					CASE PED_STATE_STANDING_AT_LOCATION
				
						IF ( ped.bHasTask = FALSE )
							TASK_START_SCENARIO_IN_PLACE(ped.PedIndex, "WORLD_HUMAN_STAND_IMPATIENT")
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							ped.bHasTask = TRUE
						ENDIF
							
						WEAPON_TYPE TargetPedWeaponType
						
						IF ( eMissionStage = MISSION_STAGE_GET_TO_ACTOR )
						AND GET_CURRENT_PED_WEAPON(TargetPedIndex, TargetPedWeaponType)
						AND TargetPedWeaponType = WEAPONTYPE_STUNGUN
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
						ELSE
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
						ENDIF
						
						IF IS_FILM_CREW_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
						OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
							bSecurityAlerted		= TRUE
							//bSpawnActorSecurity 	= TRUE
							//bActorSceneDisrupted 	= TRUE
							GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
						ENDIF
					
					BREAK
				
					CASE PED_STATE_FLEEING
					
						IF ( ped.bHasTask = FALSE )
													
							CLEAR_PED_TASKS(ped.PedIndex)
							
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "SCREAM_PANIC", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
							
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
							
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
							SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
							
							TASK_SMART_FLEE_PED(ped.PedIndex, TargetPedIndex, 300, -1)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
							
							SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
							
							ped.iConversationTimer = GET_GAME_TIMER()
							ped.bHasTask = TRUE
						ENDIF
						
						IF ( ped.bHasTask = TRUE )
							
							IF HAS_TIME_PASSED(3000, ped.iConversationTimer)
								IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GENERIC_FRIGHTENED_HIGH", "A_F_Y_VINEWOOD_01_WHITE_FULL_01", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
									ped.iConversationTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
							
						ENDIF
					
					BREAK
				
				ENDSWITCH
			
			ENDIF
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING		//cleanup ped when ped is dead or fleeing
		
			IF IS_ENTITY_DEAD(ped.PedIndex)

				IF ( ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP )
					ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)					
					UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
				ENDIF
				
			ENDIF
			
			IF ( ped.fDistanceToTarget > 200.0 )			
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

FUNC BOOL IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	
		TEXT_LABEL_23 LocalConversationRoot
		
		LocalConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF NOT IS_STRING_NULL_OR_EMPTY(LocalConversationRoot)
			IF ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_MELT")
			OR ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_CUT")
			OR ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_AWFUL")
			OR ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_AGAIN")
			OR ARE_STRINGS_EQUAL(LocalConversationRoot, "CST4_ACTION")
				
				RETURN TRUE
				
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MANAGE_MELTDOWN_DIALOGUE()

	IF bMeltdownSceneDisrupted = FALSE

		SWITCH iMeltdownDialogueProgress
		
			CASE 0
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.040 AND GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) < 0.500
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MELT", "CST4_MELT_1", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 1
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 1
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.100 AND GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) < 0.500
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MELT", "CST4_MELT_2", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 2
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 2
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.235 AND GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) < 0.500
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MELT", "CST4_MELT_4", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 3
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 3
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.310 AND GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) < 0.500
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MELT", "CST4_MELT_5", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 4
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 4
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.375
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MELT", "CST4_MELT_6", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 5
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 5
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.425
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MELT", "CST4_MELT_7", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 6
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 6
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 6
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.520
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MELT", "CST4_MELT_8", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 7
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 7
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 7
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.600
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_MELT", "CST4_MELT_9", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 8
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 8
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 8
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.640
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_CUT", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 9
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 9
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 9
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.685
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_AWFUL", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 10
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 10
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 10
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.825
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_AGAIN", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 11
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 11
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 11
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.900
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMeltdownScenePosition) <= 25.0
							IF NOT IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ACTION", CONV_PRIORITY_MEDIUM)
									iMeltdownDialogueProgress = 12
								ENDIF
							ENDIF
						ELSE
							iMeltdownDialogueProgress = 12
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 12
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.99				
						iMeltdownDialogueProgress = 0
					ENDIF
				ENDIF
			
			BREAK
		
		ENDSWITCH
		
	ELSE
	
		IF IS_ANY_MELTDOWN_CONVERSATION_ONGOING_OR_QUEUED()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	
	ENDIF

ENDPROC

PROC MANAGE_MELTDOWN_DIRECTOR_PED(PED_STRUCT &ped, PED_INDEX TargetPedIndex)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
			
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, PLAYER_PED_ID())
		CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, 950)
			
		#IF IS_DEBUG_BUILD
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
		
			IF ( ped.eState != PED_STATE_FLEEING )
				IF ( bSecurityAlerted = TRUE )
				OR ( bMeltdownSceneDisrupted = TRUE )
				OR IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
				OR CAN_PED_HEAR_PED_IN_VEHICLE(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget)
				OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
				OR CAN_PED_SEE_PED_IN_STEALTH_MOVEMENT(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
				OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
					GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
				ENDIF
			ENDIF
		
			SWITCH ped.eState
										
				CASE PED_STATE_PLAYING_ANIM_1
					
					IF ( ped.bHasTask = FALSE )
						TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@aliens", "rehearsal_base_idle_director", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
						ped.iTimer 	= GET_GAME_TIMER()
						ped.bHasTask = TRUE
					ENDIF
					
					IF ( ped.bHasTask = TRUE )
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) >= 0.645 AND GET_SYNCHRONIZED_SCENE_PHASE(iMeltdownSceneID) < 0.700
								GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_2)
							ENDIF
						ENDIF
						
					ENDIF
					
					IF ( ped.bCanSeeTargetPed = TRUE )
						IF IS_ENTITY_AT_COORD(TargetPedIndex, <<-1130.014404,-451.218109,35.503246>>,<<2.000000,3.000000,1.350000>>)
						OR IS_ENTITY_IN_ANGLED_AREA(TargetPedIndex, <<-1131.502075,-450.782013,34.109997>>, <<-1130.051880,-454.864288,36.793358>>, 4.5)
							GO_TO_PED_STATE(ped, PED_STATE_INTIMIDATION)
						ENDIF
					ENDIF

				
				BREAK
				
				CASE PED_STATE_PLAYING_ANIM_2
				
					IF ( ped.bHasTask = FALSE )

						SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
							CASE 0
								sDirectorAnimName	= "rehearsal_idle_a_director"
							BREAK
							CASE 1
								sDirectorAnimName	= "rehearsal_idle_b_director"
							BREAK
							CASE 2
								sDirectorAnimName	= "rehearsal_idle_c_director"
							BREAK
						ENDSWITCH
					
						TASK_PLAY_ANIM(ped.PedIndex, "misscarsteal4@aliens", sDirectorAnimName, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						
						ped.bHasTask = TRUE
					ENDIF
					
					IF ( ped.bHasTask = TRUE )
						IF IS_ENTITY_PLAYING_ANIM(ped.PedIndex, "misscarsteal4@aliens", sDirectorAnimName)
							IF GET_ENTITY_ANIM_CURRENT_TIME(ped.PedIndex, "misscarsteal4@aliens", sDirectorAnimName) >= 0.99
								GO_TO_PED_STATE(ped, PED_STATE_PLAYING_ANIM_1)
							ENDIF
						ENDIF
					ENDIF
					
					IF ( ped.bCanSeeTargetPed = TRUE )
						IF IS_ENTITY_AT_COORD(TargetPedIndex, <<-1130.014404,-451.218109,35.503246>>,<<2.000000,3.000000,1.350000>>)
						OR IS_ENTITY_IN_ANGLED_AREA(TargetPedIndex, <<-1131.502075,-450.782013,34.109997>>, <<-1130.051880,-454.864288,36.793358>>, 4.5)
							GO_TO_PED_STATE(ped, PED_STATE_INTIMIDATION)
						ENDIF
					ENDIF
				
				BREAK
				
				CASE PED_STATE_INTIMIDATION
				
					IF ( ped.bHasTask = FALSE )

						CLEAR_PED_TASKS(ped.PedIndex)
						
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, TargetPedIndex)
							TASK_LOOK_AT_ENTITY(NULL, TargetPedIndex, 5000, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV | SLF_USE_TORSO, SLF_LOOKAT_HIGH)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(ped.PedIndex, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						
						IF IS_SCRIPTED_SPEECH_PLAYING(ped.PedIndex)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						
						ped.iProgress = 0
						ped.bHasTask = TRUE
					ENDIF
					
					IF ( ped.bHasTask = TRUE )
						
						SWITCH ped.iProgress
							CASE 0
								IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)								
									SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
										CASE 0
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GENERIC_CURSE_HIGH", "ANTON", SPEECH_PARAMS_FORCE_NORMAL)
										BREAK
										CASE 1
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "AIMED_AT_BY_PLAYER", "ANTON", SPEECH_PARAMS_FORCE_NORMAL)
										BREAK
										CASE 2
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GENERIC_FRIGHTENED_HIGH", "ANTON", SPEECH_PARAMS_FORCE_NORMAL)
										BREAK
										CASE 3
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GENERIC_SHOCKED_MED", "ANTON", SPEECH_PARAMS_FORCE_NORMAL)
										BREAK
										CASE 4
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "CRASH_GENERIC_INTERRUPT", "ANTON", SPEECH_PARAMS_FORCE_NORMAL)
										BREAK
										CASE 5
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GENERIC_FRIGHTENED_HIGH", "ANTON", SPEECH_PARAMS_FORCE_NORMAL)
										BREAK
										CASE 6
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "GENERIC_INTERUPT_HIGH", "ANTON", SPEECH_PARAMS_FORCE_NORMAL)
										BREAK
									ENDSWITCH
								ELSE
									ped.iTimer 		= GET_GAME_TIMER()
									ped.iProgress 	= 1
								ENDIF
							BREAK
							CASE 1
								IF HAS_TIME_PASSED(2000, ped.iTimer)
									bMeltdownSceneWalkedInto = TRUE
								ENDIF
							BREAK
						ENDSWITCH
						
					ENDIF
				
				BREAK
				
				CASE PED_STATE_FLEEING
				
					IF ( ped.bHasTask = FALSE )
					
						CLEAR_PED_TASKS(ped.PedIndex)
						
						SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
						
						SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
						SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
						SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
						
						TASK_SMART_FLEE_PED(ped.PedIndex, PLAYER_PED_ID(), 300, -1)
						
						PLAY_PAIN(ped.PedIndex, AUD_DAMAGE_REASON_SCREAM_TERROR)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
						SET_PED_KEEP_TASK(ped.PedIndex, TRUE)

						bMeltdownSceneDisrupted = TRUE
						
						ped.iConversationTimer = 0
						ped.bHasTask = TRUE
					ENDIF
					
					IF GET_GAME_TIMER() - ped.iConversationTimer > 0
						IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "SCREAM_PANIC", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
							ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 7000)
						ENDIF
					ENDIF
					
				BREAK
			
			ENDSWITCH
		
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING
		
			IF IS_ENTITY_DEAD(ped.PedIndex)
			
				IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
					ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
					UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
				ENDIF
			ENDIF
		
			IF ( ped.fDistanceToTarget > 200.0 )
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_MELTDOWN_PED(PED_STRUCT &ped, PED_INDEX TargetPedIndex, INT iLOSTime)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
			
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(ped.PedIndex, PLAYER_PED_ID())
		CHECK_PED_LINE_OF_SIGHT_TO_PED(ped.PedIndex, TargetPedIndex, ped.bCanSeeTargetPed, ped.iLOSTimer, iLOSTime)
			
		#IF IS_DEBUG_BUILD
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
	
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
		
			IF ( ped.eState != PED_STATE_FLEEING )
				IF ( bSecurityAlerted = TRUE )
				OR ( bMeltdownSceneDisrupted = TRUE )
				OR IS_PED_ALERTED_BY_PED_ACTIONS(ped.PedIndex, TargetPedIndex)
				OR CAN_PED_HEAR_PED_IN_VEHICLE(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget)
				OR CAN_PED_SEE_PED_WEAPON(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
				OR CAN_PED_SEE_PED_IN_STEALTH_MOVEMENT(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
				OR CAN_PED_SEE_PED_PERFORMING_MELEE_ACTION(ped.PedIndex, TargetPedIndex, ped.fDistanceToTarget, ped.bCanSeeTargetPed)
					GO_TO_PED_STATE(ped, PED_STATE_FLEEING)
				ENDIF
			ENDIF
		
			SWITCH ped.eState
										
				CASE PED_STATE_PLAYING_ANIM_1
					
					IF ( ped.bHasTask = FALSE )
						//
						ped.bHasTask = TRUE
					ENDIF
				
				BREAK
				
				CASE PED_STATE_FLEEING
				
					IF ( ped.bHasTask = FALSE )
					
						CLEAR_PED_TASKS(ped.PedIndex)
						
						SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_ALWAYS_FLEE, TRUE)
						
						SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_CAN_SCREAM, TRUE)
						SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_COWER, TRUE)
						SET_PED_FLEE_ATTRIBUTES(ped.PedIndex, FA_DISABLE_HANDS_UP, TRUE)
						
						IF DOES_ENTITY_EXIST(osPropCamera.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osPropCamera.ObjectIndex)
								IF IS_ENTITY_ATTACHED_TO_ENTITY(osPropCamera.ObjectIndex, ped.PedIndex)
									DETACH_ENTITY(osPropCamera.ObjectIndex, FALSE)
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(osPropMicrophone.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osPropMicrophone.ObjectIndex)
								IF IS_ENTITY_ATTACHED_TO_ENTITY(osPropMicrophone.ObjectIndex, ped.PedIndex)
									DETACH_ENTITY(osPropMicrophone.ObjectIndex, FALSE)
								ENDIF
							ENDIF
						ENDIF
						
						TASK_SMART_FLEE_PED(ped.PedIndex, PLAYER_PED_ID(), 300, -1)
						
						PLAY_PAIN(ped.PedIndex, AUD_DAMAGE_REASON_SCREAM_TERROR)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, FALSE)
						SET_PED_KEEP_TASK(ped.PedIndex, TRUE)

						bMeltdownSceneDisrupted = TRUE
						ped.iConversationTimer = 0
						ped.bHasTask = TRUE
					ENDIF
					
					IF GET_GAME_TIMER() - ped.iConversationTimer > 0
						IF NOT IS_AMBIENT_SPEECH_PLAYING(ped.PedIndex)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped.PedIndex, "SCREAM_PANIC", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
							ped.iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2500, 9000)
						ENDIF
					ENDIF
					
				BREAK
			
			ENDSWITCH
		
		ENDIF
		
		IF IS_ENTITY_DEAD(ped.PedIndex) OR ped.eState = PED_STATE_FLEEING
		
			IF IS_ENTITY_DEAD(ped.PedIndex)
			
				IF (ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP)
					ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)
					UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
				ENDIF
			ENDIF
		
			IF ( ped.fDistanceToTarget > 200.0 )
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

PROC MANAGE_MELTDOWN_SCENE(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
			
			iProgress++
		
		BREAK
		
		CASE 1
		
			REQUEST_ANIM_DICT("misscarsteal4@aliens")
			REQUEST_ANIM_DICT("misscarsteal4@meltdown")
			
			REQUEST_MODEL(osPropCamera.ModelName)
			REQUEST_MODEL(osPropMicrophone.ModelName)
			REQUEST_MODEL(osPropGreenScreen.ModelName)
		
			IF 	HAS_ANIM_DICT_LOADED("misscarsteal4@aliens")
			AND HAS_ANIM_DICT_LOADED("misscarsteal4@meltdown")
			
				IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osPropGreenScreen)//, TRUE)
				AND HAS_MISSION_PED_BEEN_CREATED(psMeltdownCrew[MSP_ACTORA], FALSE, rgFilmCrew, FALSE, NO_CHARACTER)
				AND	HAS_MISSION_PED_BEEN_CREATED(psMeltdownCrew[MSP_ACTORB], FALSE, rgFilmCrew, FALSE, NO_CHARACTER)
				AND HAS_MISSION_PED_BEEN_CREATED(psMeltdownCrew[MSP_CAMERAMAN], FALSE, rgFilmCrew, FALSE, NO_CHARACTER)
				AND HAS_MISSION_PED_BEEN_CREATED(psMeltdownCrew[MSP_BOOMMAN], FALSE, rgFilmCrew, FALSE, NO_CHARACTER)
				AND HAS_MISSION_PED_BEEN_CREATED(psMeltdownCrew[MSP_DIRECTOR], FALSE, rgFilmCrew, FALSE, NO_CHARACTER)
				
					IF 	HAS_MISSION_OBJECT_BEEN_CREATED(osPropCamera)
					AND HAS_MISSION_OBJECT_BEEN_CREATED(osPropMicrophone)
					
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iMeltdownSceneID)
					
							iMeltdownSceneID = CREATE_SYNCHRONIZED_SCENE(vMeltdownScenePosition, vMeltdownSceneRotation)
							
							SET_SYNCHRONIZED_SCENE_LOOPED(iMeltdownSceneID, TRUE)
							SET_SYNCHRONIZED_SCENE_RATE(iMeltdownSceneID, 0.80)
							
							IF NOT IS_PED_INJURED(psMeltdownCrew[MSP_ACTORA].PedIndex)
							
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORA].PedIndex, PED_COMP_HEAD, 	0, 0, 0) //(head)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORA].PedIndex, PED_COMP_HAIR, 	0, 0, 0) //(hair)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORA].PedIndex, PED_COMP_TORSO, 	1, 0, 0) //(uppr)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORA].PedIndex, PED_COMP_LEG, 		1, 0, 0) //(lowr)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORA].PedIndex, PED_COMP_HAND, 	0, 0, 0) //(hand)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORA].PedIndex, PED_COMP_FEET, 	0, 0, 0) //(feet)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORA].PedIndex, PED_COMP_TEETH,	0, 0, 0) //(teef)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORA].PedIndex, PED_COMP_JBIB, 	1, 0, 0) //(jbib)
							
								TASK_SYNCHRONIZED_SCENE(psMeltdownCrew[MSP_ACTORA].PedIndex, iMeltdownSceneID,
														"misscarsteal4@meltdown", "_rehearsal_actor_a",
														INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psMeltdownCrew[MSP_ACTORA].PedIndex)
							ENDIF
							
							IF NOT IS_PED_INJURED(psMeltdownCrew[MSP_ACTORB].PedIndex)
							
								//S_M_M_MOVALIEN_01 variations
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORB].PedIndex, PED_COMP_HEAD, 	1, 1, 0) //(head)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORB].PedIndex, PED_COMP_TORSO, 	1, 0, 0) //(uppr)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORB].PedIndex, PED_COMP_LEG, 	1, 0, 0) //(lowr)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORB].PedIndex, PED_COMP_HAND, 	1, 0, 0) //(hand)
							
								//S_M_Y_GRIP_01 varitions
								//SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORB].PedIndex, PED_COMP_HEAD, 	0, 0, 0) //(head)
								//SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORB].PedIndex, PED_COMP_TORSO, 	0, 0, 0) //(uppr)
								//SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORB].PedIndex, PED_COMP_LEG, 		0, 1, 0) //(lowr)
								//SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_ACTORB].PedIndex, PED_COMP_SPECIAL, 	2, 0, 0) //(accs)
							
								TASK_SYNCHRONIZED_SCENE(psMeltdownCrew[MSP_ACTORB].PedIndex, iMeltdownSceneID,
														"misscarsteal4@meltdown", "_rehearsal_actor_b",
														INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psMeltdownCrew[MSP_ACTORB].PedIndex)
							ENDIF
							
							IF NOT IS_PED_INJURED(psMeltdownCrew[MSP_CAMERAMAN].PedIndex)
								IF DOES_ENTITY_EXIST(osPropCamera.ObjectIndex) AND NOT IS_ENTITY_DEAD(osPropCamera.ObjectIndex)
									IF NOT IS_ENTITY_ATTACHED(osPropCamera.ObjectIndex)
										ATTACH_ENTITY_TO_ENTITY(osPropCamera.ObjectIndex, psMeltdownCrew[MSP_CAMERAMAN].PedIndex,
																GET_PED_BONE_INDEX( psMeltdownCrew[MSP_CAMERAMAN].PedIndex, BONETAG_PH_R_HAND),
												    			<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
									ENDIF
								ENDIF
								
								TASK_SYNCHRONIZED_SCENE(psMeltdownCrew[MSP_CAMERAMAN].PedIndex, iMeltdownSceneID,
														"misscarsteal4@meltdown", "_rehearsal_camera_man",
														INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psMeltdownCrew[MSP_CAMERAMAN].PedIndex)
							ENDIF
							
							IF NOT IS_PED_INJURED(psMeltdownCrew[MSP_BOOMMAN].PedIndex)
								IF DOES_ENTITY_EXIST(osPropMicrophone.ObjectIndex) AND NOT IS_ENTITY_DEAD(osPropMicrophone.ObjectIndex)
									IF NOT IS_ENTITY_ATTACHED(osPropMicrophone.ObjectIndex)
										ATTACH_ENTITY_TO_ENTITY(osPropMicrophone.ObjectIndex, psMeltdownCrew[MSP_BOOMMAN].PedIndex,
																GET_PED_BONE_INDEX( psMeltdownCrew[MSP_BOOMMAN].PedIndex, BONETAG_PH_R_HAND),
												    			<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
									ENDIF
								ENDIF
								
								TASK_SYNCHRONIZED_SCENE(psMeltdownCrew[MSP_BOOMMAN].PedIndex, iMeltdownSceneID,
														"misscarsteal4@meltdown", "_rehearsal_boom_op",
														INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psMeltdownCrew[MSP_BOOMMAN].PedIndex)
							ENDIF
							
							IF NOT IS_PED_INJURED(psMeltdownCrew[MSP_DIRECTOR].PedIndex)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_DIRECTOR].PedIndex, PED_COMP_HEAD, 	0, 0, 0) //(head)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_DIRECTOR].PedIndex, PED_COMP_TORSO, 	0, 0, 0) //(uppr)
								SET_PED_COMPONENT_VARIATION(psMeltdownCrew[MSP_DIRECTOR].PedIndex, PED_COMP_LEG, 	0, 0, 0) //(lowr)
							ENDIF
							
							ADD_PED_FOR_DIALOGUE(CST4Conversation, 4, psMeltdownCrew[MSP_ACTORA].PedIndex, "DYLAN")
							ADD_PED_FOR_DIALOGUE(CST4Conversation, 5, psMeltdownCrew[MSP_ACTORB].PedIndex, "FITCH")
							ADD_PED_FOR_DIALOGUE(CST4Conversation, 8, psMeltdownCrew[MSP_DIRECTOR].PedIndex, "ANTON")
							
							SET_MODEL_AS_NO_LONGER_NEEDED(osPropCamera.ModelName)
							SET_MODEL_AS_NO_LONGER_NEEDED(osPropMicrophone.ModelName)
							SET_MODEL_AS_NO_LONGER_NEEDED(osPropGreenScreen.ModelName)
							
							GO_TO_PED_STATE(psMeltdownCrew[MSP_ACTORA], PED_STATE_PLAYING_ANIM_1)
							GO_TO_PED_STATE(psMeltdownCrew[MSP_ACTORB], PED_STATE_PLAYING_ANIM_1)
							GO_TO_PED_STATE(psMeltdownCrew[MSP_BOOMMAN], PED_STATE_PLAYING_ANIM_1)
							GO_TO_PED_STATE(psMeltdownCrew[MSP_DIRECTOR], PED_STATE_PLAYING_ANIM_1)
							GO_TO_PED_STATE(psMeltdownCrew[MSP_CAMERAMAN], PED_STATE_PLAYING_ANIM_1)
							
							iMeltdownDialogueProgress = 0
							
							iProgress++
						
						ENDIF
					
					ENDIF
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 2
		
			MANAGE_MELTDOWN_DIALOGUE()
			MANAGE_MELTDOWN_PED(psMeltdownCrew[MSP_ACTORA], PLAYER_PED_ID(), 900)
			MANAGE_MELTDOWN_PED(psMeltdownCrew[MSP_ACTORB], PLAYER_PED_ID(), 1000)
			MANAGE_MELTDOWN_PED(psMeltdownCrew[MSP_BOOMMAN], PLAYER_PED_ID(), 1250)
			MANAGE_MELTDOWN_PED(psMeltdownCrew[MSP_CAMERAMAN], PLAYER_PED_ID(), 950)
			MANAGE_MELTDOWN_DIRECTOR_PED(psMeltdownCrew[MSP_DIRECTOR], PLAYER_PED_ID())
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_WALKING_ALIENS(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			IF ( bSecurityAlerted = FALSE )
		
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1139.96, -452.77, 40.31 >>, << 22.0, 22.0, 8.0 >>)
			
					iProgress++
					
				ENDIF
				
			ENDIF

		BREAK
		
		CASE 1
		
			IF 	HAS_MISSION_PED_BEEN_CREATED(psAliens[0], FALSE, rgFilmCrew, FALSE, NO_CHARACTER)
			AND HAS_MISSION_PED_BEEN_CREATED(psAliens[1], FALSE, rgFilmCrew, FALSE, NO_CHARACTER)
			
				GO_TO_PED_STATE(psAliens[0], PED_STATE_MOVING_TO_LOCATION)
				GO_TO_PED_STATE(psAliens[1], PED_STATE_MOVING_TO_LOCATION)
			
				iProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF DOES_ENTITY_EXIST(psAliens[0].PedIndex)
			
				psAliens[0].fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(psAliens[0].PedIndex, PLAYER_PED_ID())
			
				#IF IS_DEBUG_BUILD
					psAliens[0].sDebugName = "Alien0"
					IF NOT IS_ENTITY_DEAD(psAliens[0].PedIndex)
						SET_PED_NAME_DEBUG(psAliens[0].PedIndex, "Alien0")
					ENDIF
					IF ( bDrawDebugStates = TRUE )
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(psAliens[0].PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(psAliens[0].eState), 1.25)
					ENDIF
				#ENDIF

				IF NOT IS_ENTITY_DEAD(psAliens[0].PedIndex)
				
					IF ( psAliens[0].eState != PED_STATE_FLEEING )
						IF ( bAlienWalkDisrupted = TRUE )
						OR IS_ENTITY_TOUCHING_ENTITY(psAliens[0].PedIndex, PLAYER_PED_ID())
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psAliens[0].PedIndex, PLAYER_PED_ID())		
						OR HAS_PED_RECEIVED_EVENT(psAliens[0].PedIndex, EVENT_SHOT_FIRED)
						OR HAS_PED_RECEIVED_EVENT(psAliens[0].PedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
						OR HAS_PED_RECEIVED_EVENT(psAliens[0].PedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
							GO_TO_PED_STATE(psAliens[0], PED_STATE_FLEEING)
						ENDIF
					ENDIF
				
					SWITCH psAliens[0].eState
					
						CASE PED_STATE_MOVING_TO_LOCATION
						
							IF ( psAliens[0].bHasTask = FALSE )
						
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1143.49, -464.56, 33.96 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1143.91, -458.29, 34.08 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1150.15, -455.65, 33.91 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1162.95, -461.74, 33.30 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1166.33, -459.13, 33.27 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1183.96, -467.70, 32.30 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1204.53, -480.80, 31.49 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_WANDER_IN_AREA(NULL, << -1223.41, -496.62, 30.31 >>, 10.0)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(psAliens[0].PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
				
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psAliens[0].PedIndex, TRUE)
								psAliens[0].bHasTask 	= TRUE
							ENDIF
						
						BREAK
						
						CASE PED_STATE_FLEEING
						
							IF ( psAliens[0].bHasTask = FALSE )
							
								CLEAR_PED_TASKS(psAliens[0].PedIndex)
								
								SET_PED_COMBAT_ATTRIBUTES(psAliens[0].PedIndex, CA_ALWAYS_FLEE, TRUE)
								
								SET_PED_FLEE_ATTRIBUTES(psAliens[0].PedIndex, FA_CAN_SCREAM, TRUE)
								SET_PED_FLEE_ATTRIBUTES(psAliens[0].PedIndex, FA_DISABLE_COWER, TRUE)
								SET_PED_FLEE_ATTRIBUTES(psAliens[0].PedIndex, FA_DISABLE_HANDS_UP, TRUE)
								
								TASK_SMART_FLEE_PED(psAliens[0].PedIndex, PLAYER_PED_ID(), 300, -1)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psAliens[0].PedIndex, FALSE)
								SET_PED_KEEP_TASK(psAliens[0].PedIndex, TRUE)

								bAlienWalkDisrupted = TRUE
								
								psAliens[0].iConversationTimer = 0
								psAliens[0].bHasTask = TRUE
							ENDIF
							
							IF GET_GAME_TIMER() - psAliens[0].iConversationTimer > 0
								IF NOT IS_AMBIENT_SPEECH_PLAYING(psAliens[0].PedIndex)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psAliens[0].PedIndex, "SCREAM_PANIC", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
									psAliens[0].iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
								ENDIF
							ENDIF
							
						BREAK
					
					ENDSWITCH
					
				ENDIF
				
				IF IS_ENTITY_DEAD(psAliens[0].PedIndex) OR psAliens[0].eState = PED_STATE_FLEEING
				
					IF IS_ENTITY_DEAD(psAliens[0].PedIndex)
					
						IF (psAliens[0].eCleanupReason = PED_CLEANUP_NO_CLEANUP)
							psAliens[0].eCleanupReason = GET_PED_CLEANUP_REASON(psAliens[0])
							UPDATE_PLAYER_KILL_COUNTERS(psAliens[0].eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
						ENDIF
					ENDIF
				
					IF ( psAliens[0].fDistanceToTarget > 200.0 )
						SET_PED_AS_NO_LONGER_NEEDED(psAliens[0].PedIndex)
					ENDIF
					
				ENDIF
				
			ENDIF
			
			IF DOES_ENTITY_EXIST(psAliens[1].PedIndex)
			
				psAliens[1].fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(psAliens[1].PedIndex, PLAYER_PED_ID())
			
				#IF IS_DEBUG_BUILD
					psAliens[1].sDebugName = "Alien1"
					IF NOT IS_ENTITY_DEAD(psAliens[1].PedIndex)
						SET_PED_NAME_DEBUG(psAliens[1].PedIndex, "Alien1")
					ENDIF
					IF ( bDrawDebugStates = TRUE )
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(psAliens[1].PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(psAliens[1].eState), 1.25)
					ENDIF
				#ENDIF
			
				IF NOT IS_ENTITY_DEAD(psAliens[1].PedIndex)
				
					IF ( psAliens[1].eState != PED_STATE_FLEEING )
						IF ( bAlienWalkDisrupted = TRUE )
						OR IS_ENTITY_TOUCHING_ENTITY(psAliens[1].PedIndex, PLAYER_PED_ID())
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psAliens[1].PedIndex, PLAYER_PED_ID())		
						OR HAS_PED_RECEIVED_EVENT(psAliens[1].PedIndex, EVENT_SHOT_FIRED)
						OR HAS_PED_RECEIVED_EVENT(psAliens[1].PedIndex, EVENT_SHOT_FIRED_WHIZZED_BY)
						OR HAS_PED_RECEIVED_EVENT(psAliens[1].PedIndex, EVENT_SHOT_FIRED_BULLET_IMPACT)
							GO_TO_PED_STATE(psAliens[1], PED_STATE_FLEEING)
						ENDIF
					ENDIF
					
					SWITCH psAliens[1].eState
					
						CASE PED_STATE_MOVING_TO_LOCATION
					
							IF ( psAliens[1].bHasTask = FALSE )
							
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1142.27, -462.21, 34.07 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1143.37, -457.43, 34.19 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1152.27, -454.78, 33.86 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1164.64, -460.87, 33.27 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1167.92, -458.11, 33.237 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1184.64, -465.70, 32.47 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1206.56, -478.80, 31.57 >>, 0.8, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									TASK_WANDER_IN_AREA(NULL, << -1223.41, -496.62, 30.31 >>, 10.0)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(psAliens[1].PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
				
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psAliens[1].PedIndex, TRUE)
								psAliens[1].bHasTask 	= TRUE
							
							ENDIF
							
						BREAK
						
						CASE PED_STATE_FLEEING
						
							IF ( psAliens[1].bHasTask = FALSE )
							
								CLEAR_PED_TASKS(psAliens[1].PedIndex)
								
								SET_PED_COMBAT_ATTRIBUTES(psAliens[1].PedIndex, CA_ALWAYS_FLEE, TRUE)
								
								SET_PED_FLEE_ATTRIBUTES(psAliens[1].PedIndex, FA_CAN_SCREAM, TRUE)
								SET_PED_FLEE_ATTRIBUTES(psAliens[1].PedIndex, FA_DISABLE_COWER, TRUE)
								SET_PED_FLEE_ATTRIBUTES(psAliens[1].PedIndex, FA_DISABLE_HANDS_UP, TRUE)
								
								TASK_SMART_FLEE_PED(psAliens[1].PedIndex, PLAYER_PED_ID(), 300, -1)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psAliens[1].PedIndex, FALSE)
								SET_PED_KEEP_TASK(psAliens[1].PedIndex, TRUE)

								bAlienWalkDisrupted = TRUE
								
								psAliens[1].iConversationTimer = 0
								psAliens[1].bHasTask = TRUE
							ENDIF
							
							IF GET_GAME_TIMER() - psAliens[1].iConversationTimer > 0
								IF NOT IS_AMBIENT_SPEECH_PLAYING(psAliens[1].PedIndex)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psAliens[1].PedIndex, "SCREAM_PANIC", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
									psAliens[1].iConversationTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 8000)
								ENDIF
							ENDIF
							
						BREAK
						
					ENDSWITCH
					
				ENDIF
				
				IF IS_ENTITY_DEAD(psAliens[1].PedIndex) OR psAliens[1].eState = PED_STATE_FLEEING
				
					IF IS_ENTITY_DEAD(psAliens[1].PedIndex)
					
						IF (psAliens[1].eCleanupReason = PED_CLEANUP_NO_CLEANUP)
							psAliens[1].eCleanupReason = GET_PED_CLEANUP_REASON(psAliens[1])
							UPDATE_PLAYER_KILL_COUNTERS(psAliens[1].eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
						ENDIF
					ENDIF
				
					IF ( psAliens[1].fDistanceToTarget > 200.0 )
						SET_PED_AS_NO_LONGER_NEEDED(psAliens[1].PedIndex)
					ENDIF
					
				ENDIF
				
			ENDIF
		
			
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC RUN_DOOR_BARGE_CHECK()

	IF ( bDoorBargePlayed = FALSE )

		REQUEST_ANIM_DICT("missarmenian3_tryopendoor")
		
		IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1089.873169,-432.288513,35.528690>>, <<-1088.673828,-431.657532,38.127274>>, 1.7)		
			IF 	IS_PED_WALKING(PLAYER_PED_ID())
			AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())
			
			
				VECTOR vVelocity
				
				vVelocity = GET_ENTITY_SPEED_VECTOR(PLAYER_PED_ID(), TRUE)

				IF 	( vVelocity.y > 0.0 )
				AND ( GET_ENTITY_HEADING(PLAYER_PED_ID()) < 130.0 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > 90.0 )
			
					IF NOT IS_PHONE_ONSCREEN()
						IF HAS_ANIM_DICT_LOADED("missarmenian3_tryopendoor")
						
							IF NOT IS_AREA_OCCUPIED(<<-1088.917603 - 2.0, -431.848083 - 2.0, 37.127144 - 1.5 >>,
													<<-1088.917603 + 2.0, -431.848083 + 2.0, 37.127144 + 1.5 >>,
													FALSE, TRUE, FALSE, FALSE, FALSE, PLAYER_PED_ID())
						
								VECTOR vAnimationPosition, vAnimationRotation
							
								vAnimationPosition = GET_ANIM_INITIAL_OFFSET_POSITION("missarmenian3_tryopendoor", "lockeddoor_tryopen", << -1090.100, -431.850, 35.620 >>, << 0.000, 0.000, 120.000 >>)
								vAnimationRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("missarmenian3_tryopendoor", "lockeddoor_tryopen", << -1090.100, -431.850, 35.620 >>, << 0.000, 0.000, 120.000 >>)
							
								SEQUENCE_INDEX SequenceIndex
								OPEN_SEQUENCE_TASK(SequenceIndex)
									TASK_GO_STRAIGHT_TO_COORD(NULL, vAnimationPosition, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, vAnimationRotation.Z)
									TASK_PLAY_ANIM_ADVANCED(NULL, "missarmenian3_tryopendoor", "lockeddoor_tryopen", << -1090.10, -431.85, 35.62 >>,
															<< 0.0, 0.0, 120.0 >>, WALK_BLEND_IN, WALK_BLEND_OUT, -1,
															AF_EXTRACT_INITIAL_OFFSET | AF_IGNORE_GRAVITY | AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXIT_AFTER_INTERRUPTED)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
								
								bDoorBargePlayed 		= TRUE
								bDoorBargeInterrupted 	= FALSE
								
							ELSE
								
								REMOVE_ANIM_DICT("missarmenian3_tryopendoor")
								
								bDoorBargePlayed 		= TRUE
								bDoorBargeInterrupted 	= TRUE
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ( bPlayerInsideFilmStudio = TRUE OR bSecurityAlerted = TRUE )	//if player gets into film studio, don't attempt to play animation again
			bDoorBargePlayed = TRUE
		ENDIF
	
	ELSE
	
		IF ( bDoorBargeInterrupted = FALSE )
		
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missarmenian3_tryopendoor", "lockeddoor_tryopen")
			
				IF IS_PED_RAGDOLL(PLAYER_PED_ID())
				OR IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
				
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
				
					CLEAR_PED_TASKS(PLAYER_PED_ID())
			
					bDoorBargeInterrupted = TRUE
			
				ENDIF
			
			ENDIF
		
		ENDIF
		
	ENDIF
	
	IF ( bPlayerInsideFilmStudio = TRUE )
		REMOVE_ANIM_DICT("missarmenian3_tryopendoor")
	ENDIF

ENDPROC

PROC MANAGE_REQUESTING_TRAILER_CUTSCENE()

	IF DOES_ENTITY_EXIST(psActor.PedIndex)

		IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		OR IS_PLAYER_CHANGING_CLOTHES()
		OR ( bSecurityAlerted = TRUE )

			IF HAS_THIS_CUTSCENE_LOADED("car_4_mcs_1")
			OR HAS_CUTSCENE_LOADED()
			OR IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes or security is alerted. Removing cutscene.")
				#ENDIF
			ENDIF
			                        
		ELSE

			IF 	( bPlayerInsideFilmStudio = TRUE )
			AND ( psActor.fDistanceToTarget < 50.0 )

				REQUEST_CUTSCENE("car_4_mcs_1")
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())
				ENDIF
							
			ELSE
			
				IF HAS_THIS_CUTSCENE_LOADED("car_4_mcs_1")
				OR HAS_CUTSCENE_LOADED()
				OR IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
					#ENDIF
				ENDIF
			
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

BOOL bFirstTimePlayerEntered

FUNC BOOL IS_MISSION_STAGE_GET_TO_ACTOR_COMPLETED(INT &iStageProgress)

	IF bPlayerInsideFilmStudio
		IF bFirstTimePlayerEntered = FALSE
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0, REPLAY_IMPORTANCE_NORMAL)
			bFirstTimePlayerEntered = TRUE
		ENDIF
	ENDIF

	MANAGE_FILM_CREW(iFilmCrewProgress)
	MANAGE_DIRECTOR_AND_GRIP(iDirectorAndGripProgress)
	MANAGE_ACTRESS(psActress, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)

	MANAGE_TOILET_SCENE(iToiletSceneProgress)
	MANAGE_MELTDOWN_SCENE(iMeltdownSceneProgress)
	MANAGE_WALKING_ALIENS(iWalkingAliensProgress)
	MANAGE_ACTOR_DURING_GET_TO_ACTOR(psActor, PLAYER_PED_ID())
	MANAGE_ASSISTANT_PED_DURING_GET_TO_ACTOR(psAssistant, PLAYER_PED_ID())

	MANAGE_SECURITY_PEDS_AT_GATES(iGateSecurityProgress)
	MANAGE_SECURITY_PEDS_AT_FILM_SET(iSetSecurityProgress)
	MANAGE_SECURITY_PEDS_AROUND_ACTOR(iActorSecurityProgress)
	MANAGE_SECURITY_PEDS_FOR_MELTDOWN(iMeltdownSecurityProgress)
	
	RUN_DOOR_BARGE_CHECK()
	MANAGE_REQUESTING_TRAILER_CUTSCENE()
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_SCALL")
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF PLAYER_CALL_CHAR_CELLPHONE(CST4Conversation, CHAR_MOLLY, "CST4AUD", "CST4_SCALL", CONV_PRIORITY_VERY_HIGH)
				SET_LABEL_AS_TRIGGERED("CST4_SCALL", TRUE)
			ENDIF
		ENDIF
	ENDIF

	IF ( bMusicEventMissionStartTriggered = FALSE )
		IF HAS_LABEL_BEEN_TRIGGERED("CST4_SCALL")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF 	NOT IS_MOBILE_PHONE_CALL_ONGOING()
				AND NOT IS_CALLING_CONTACT(CHAR_MOLLY)
				AND NOT IS_CONTACT_IN_CONNECTED_CALL(CHAR_MOLLY)
					IF TRIGGER_MUSIC_EVENT("CAR3_MISSION_START")
						bMusicEventMissionStartTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_3_INSIDE_STUDIO")
		IF ( bPlayerInsideFilmStudio = TRUE )
			START_AUDIO_SCENE("CAR_3_INSIDE_STUDIO")
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_CDOOR")
		IF HAS_LABEL_BEEN_TRIGGERED("CST4_SCALL")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF 	NOT IS_MOBILE_PHONE_CALL_ONGOING()
				AND NOT IS_CALLING_CONTACT(CHAR_MOLLY)
				AND NOT IS_CONTACT_IN_CONNECTED_CALL(CHAR_MOLLY)
					IF ( bPlayerInsideFilmStudio = FALSE )
						IF NOT DOES_BLIP_EXIST(DestinationBlip)
							DestinationBlip = ADD_BLIP_FOR_COORD(<< -1091.00, -433.06, 36.63 >>)
							PRINT_GOD_TEXT_ADVANCED("CAR4_CDOOR", DEFAULT_GOD_TEXT_TIME, TRUE)
						ENDIF
					ELSE
						SET_LABEL_AS_TRIGGERED("CAR4_CDOOR", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_HALERT")
		IF bMusicEventMissionStartTriggered
			IF ( bSecurityAlerted = TRUE )
			
				IF DOES_BLIP_EXIST(DestinationBlip)
					REMOVE_BLIP(DestinationBlip)
				ENDIF
				
				IF DOES_BLIP_EXIST(psActor.BlipIndex)
					REMOVE_BLIP(psActor.BlipIndex)
				ENDIF
				
				IF ( bMusicEventSetAlertTriggered = FALSE )
					IF TRIGGER_MUSIC_EVENT("CAR3_SET_ALERT")
						bMusicEventSetAlertTriggered = TRUE
					ENDIF
				ENDIF
			
				PRINT_HELP_ADVANCED("CAR4_HALERT", FALSE, TRUE, DEFAULT_HELP_TEXT_TIME, TRUE)
				
				IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex, TRUE)
					IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
						CLEAR_PRINTS()
					ENDIF
					PRINT_GOD_TEXT_ADVANCED("CAR4_LCAR", DEFAULT_GOD_TEXT_TIME, TRUE)
					CLEANUP_TRACKED_POINT(iActorTrackedPoint)
					CREATE_TRACKED_POINT_FOR_COORD(iCarTrackedPoint, vsPlayersCar.vPosition, 3.0)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("CMN_GENGETIN")
		IF HAS_LABEL_BEEN_TRIGGERED("CAR4_LCAR")
			IF DOES_TRACKED_POINT_EXIST(iCarTrackedPoint)
				IF IS_TRACKED_POINT_VISIBLE(iCarTrackedPoint)
				
					IF DOES_BLIP_EXIST(DestinationBlip)
						REMOVE_BLIP(DestinationBlip)
					ENDIF
				
					IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
						CLEAR_PRINTS()
					ENDIF
					
					CLEANUP_TRACKED_POINT(iCarTrackedPoint)
					PRINT_GOD_TEXT_ADVANCED("CMN_GENGETIN", DEFAULT_GOD_TEXT_TIME, TRUE)
					
					IF NOT DOES_BLIP_EXIST(vsPlayersCar.BlipIndex)
						vsPlayersCar.BlipIndex = CREATE_BLIP_FOR_VEHICLE(vsPlayersCar.VehicleIndex)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_HKO")
		IF DOES_ENTITY_EXIST(psActor.PedIndex)
			IF NOT IS_ENTITY_DEAD(psActor.PedIndex)
				IF ( psActor.eState != PED_STATE_FLEEING )
					IF DOES_BLIP_EXIST(psActor.BlipIndex)
						IF DOES_TRACKED_POINT_EXIST(iActorTrackedPoint)
							IF IS_TRACKED_POINT_VISIBLE(iActorTrackedPoint)
								PRINT_HELP_ADVANCED("CAR4_HKO", FALSE, TRUE, DEFAULT_HELP_TEXT_TIME, TRUE)
								CLEANUP_TRACKED_POINT(iActorTrackedPoint)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SWITCH iStageProgress
	
		CASE 0
			
			IF DOES_ENTITY_EXIST(psActor.PedIndex)
			
				IF NOT IS_ENTITY_DEAD(psActor.PedIndex)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_TMSTA")
						IF NOT DOES_BLIP_EXIST(psActor.BlipIndex)
							IF ( bPlayerInsideFilmStudio = TRUE AND bSecurityAlerted = FALSE )
								IF HAS_LABEL_BEEN_TRIGGERED("CST4_SCALL")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF 	NOT IS_MOBILE_PHONE_CALL_ONGOING()
										AND NOT IS_CALLING_CONTACT(CHAR_MOLLY)
										AND NOT IS_CONTACT_IN_CONNECTED_CALL(CHAR_MOLLY)
											CLEAR_PRINTS()
											IF DOES_BLIP_EXIST(DestinationBlip)
												REMOVE_BLIP(DestinationBlip)
											ENDIF
											psActor.BlipIndex = CREATE_BLIP_FOR_PED(psActor.PedIndex, TRUE)
											PRINT_GOD_TEXT_ADVANCED("CAR4_TMSTA", DEFAULT_GOD_TEXT_TIME, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_ENTITY_DEAD(psActor.PedIndex)
				
					IF ( psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_PLAYER_STEALTH )
					OR ( psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_PLAYER_TAKEDOWN )
					OR ( psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_PLAYER_LETHAL )
					
						IF ( psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_PLAYER_STEALTH )
						
							SET_GLOBAL_BIT_SET(ACTOR_STEALTH_KILLED)
							CLEAR_GLOBAL_BIT_SET(ACTOR_KILLED)

						ELIF ( psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_PLAYER_TAKEDOWN )
						
							SET_GLOBAL_BIT_SET(ACTOR_STEALTH_KILLED)
						
							SWITCH ActorCauseOfDeathWeapon
								CASE WEAPONTYPE_KNIFE
								CASE WEAPONTYPE_DLC_BOTTLE
								CASE WEAPONTYPE_DLC_DAGGER
									SET_GLOBAL_BIT_SET(ACTOR_KILLED)
								BREAK
								DEFAULT
									CLEAR_GLOBAL_BIT_SET(ACTOR_KILLED)
								BREAK
							ENDSWITCH
							
						ELIF ( psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_PLAYER_LETHAL )
							
							CLEAR_GLOBAL_BIT_SET(ACTOR_STEALTH_KILLED)
							SET_GLOBAL_BIT_SET(ACTOR_KILLED)
							
						ENDIF
						
						IF NOT IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTOR_KILLED)
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CS3_ACTOR_KNOCKOUT)
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Informing stat system that bool stat CS3_ACTOR_KNOCKOUT happened.")
							#ENDIF
						ENDIF
					
						STOP_AUDIO_SCENE("CAR_3_TAKE_OUT_ACTOR")
						
						REPLAY_RECORD_BACK_FOR_TIME(6.0, 0.0, REPLAY_IMPORTANCE_HIGH)
					
						iStageProgress++
						
					ENDIF
				
				ENDIF
			ENDIF

		BREAK
		
		CASE 1
		
			IF 	( bSecurityAlerted = FALSE AND bSpawnActorSecurity = FALSE )
		
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					IF ( psActor.fDistanceToTarget < 5.0)
					
						CLEANUP_TRACKED_POINT(iCarTrackedPoint)
						CLEANUP_TRACKED_POINT(iActorTrackedPoint)
					
						SET_PED_AS_NO_LONGER_NEEDED(psActor.PedIndex)
								
						RETURN TRUE
						
					ENDIF
					
				ENDIF
				
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
	
		IF NOT IS_PED_IN_THIS_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
			SET_GLOBAL_BIT_SET(ACTRESS_FLED_CAR)
		ENDIF
	
		IF IS_THIS_PRINT_BEING_DISPLAYED("CMN_GENGETIN")
			CLEAR_PRINTS()
		ENDIF
	
		IF DOES_BLIP_EXIST(vsPlayersCar.BlipIndex)
			REMOVE_BLIP(vsPlayersCar.BlipIndex)
		ENDIF
	
		IF DOES_BLIP_EXIST(psActor.BlipIndex)
			REMOVE_BLIP(psActor.BlipIndex)
		ENDIF
		
		IF DOES_BLIP_EXIST(DestinationBlip)
			REMOVE_BLIP(DestinationBlip)
		ENDIF
		
		IF DOES_ENTITY_EXIST(psActor.PedIndex)
			IF NOT IS_ENTITY_DEAD(psActor.PedIndex)
				SET_GLOBAL_BIT_SET(ACTOR_IGNORED)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(psActor.PedIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(psAssistant.PedIndex)
			SET_PED_AS_NO_LONGER_NEEDED(psAssistant.PedIndex)
		ENDIF
		
		REMOVE_ANIM_DICT("misscarsteal4@actor")
		REMOVE_ANIM_DICT("misscarsteal4@aliens")
		REMOVE_ANIM_DICT("misscarsteal4@meltdown")
		CLEANUP_PED_GROUP(psAliens, FALSE, FALSE, FALSE)
		CLEANUP_PED_GROUP(psMeltdownCrew,FALSE, FALSE, FALSE)
	
		CLEANUP_OBJECT(osPropCamera.ObjectIndex, FALSE)
		CLEANUP_OBJECT(osPropMicrophone.ObjectIndex, FALSE)
		CLEANUP_OBJECT(osPropGreenScreen.ObjectIndex, FALSE, FALSE)
	
		CLEANUP_TRACKED_POINT(iCarTrackedPoint)
		CLEANUP_TRACKED_POINT(iActorTrackedPoint)
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_3_INSIDE_STUDIO")
			STOP_AUDIO_SCENE("CAR_3_INSIDE_STUDIO")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_3_TAKE_OUT_ACTOR")
			STOP_AUDIO_SCENE("CAR_3_TAKE_OUT_ACTOR")
		ENDIF
			
		IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_3_ESCAPE_SECURITY")
			START_AUDIO_SCENE("CAR_3_ESCAPE_SECURITY")
		ENDIF
		
		bPlayerEnteredTargetVehicle = TRUE
		
		TRIGGER_MUSIC_EVENT("CAR3_DRIVE")
	
		REMOVE_CUTSCENE()
		
		RETURN TRUE
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_TRAILER_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
		
		CASE 0
      
      		IF HAS_REQUESTED_CUTSCENE_LOADED("car_4_mcs_1")
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
              		REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
				ENDIF
				
				IF NOT IS_PED_INJURED(psActor.PedIndex)
              		REGISTER_ENTITY_FOR_CUTSCENE(psActor.PedIndex, "spy_actor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, U_M_M_SPYACTOR)
				ENDIF
				
				IF DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osTrailerDoor.ObjectIndex)
						REGISTER_ENTITY_FOR_CUTSCENE(osTrailerDoor.ObjectIndex, "Actors_trailer_door", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, osTrailerDoor.ModelName)
					ENDIF
				ENDIF
				
				CLEAR_HELP()
				CLEAR_PRINTS()
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++

         	ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				REMOVE_ANIM_DICT("misscarsteal4@toilet")
				REMOVE_ANIM_DICT("misscarsteal4@aliens")
				REMOVE_ANIM_DICT("misscarsteal4@meltdown")
				
				CLEANUP_PED(psToilet, TRUE)
				CLEANUP_PED(psAssistant, TRUE)
				CLEANUP_PED_GROUP(psAliens, TRUE)
				CLEANUP_PED_GROUP(psMeltdownCrew, TRUE)
				
				CLEANUP_OBJECT(osPropCamera.ObjectIndex, TRUE)
				CLEANUP_OBJECT(osPropMicrophone.ObjectIndex, TRUE)
				CLEANUP_OBJECT(osPropGreenScreen.ObjectIndex, TRUE, FALSE)
				
				IF DOES_ENTITY_EXIST(osPropToilet.ObjectIndex)
					REMOVE_MODEL_HIDE(osPropToilet.vPosition, 1.0, osPropToilet.ModelName)
					CLEANUP_OBJECT(osPropToilet.ObjectIndex, TRUE)
				ENDIF
				
				IF DOES_BLIP_EXIST(DestinationBlip)
					REMOVE_BLIP(DestinationBlip)
				ENDIF
				
				CLEAR_AREA_OF_PEDS(sMissionPosition.vPosition, 25.0)
				CLEAR_AREA_OF_VEHICLES(sMissionPosition.vPosition, 25.0)
				CLEAR_AREA_OF_OBJECTS(sMissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_PROJECTILES(sMissionPosition.vPosition, 25.0)
				REMOVE_PARTICLE_FX_IN_RANGE(sMissionPosition.vPosition, 25.0)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++

			ENDIF
		
		BREAK
		
		CASE 2
		
			REQUEST_ANIM_DICT("dead")
		
			IF IS_CUTSCENE_PLAYING()
				IF GET_CUTSCENE_TIME() >= 17000.0
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)		//disable cutscene skip when player starts final walk
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
			
			IF ( bPlayerWearingTuxedo = FALSE )
				IF IS_CUTSCENE_PLAYING()
					IF ( GET_CUTSCENE_TIME() > 10000 )
					
						SET_PLAYER_PED_OUTFIT_TO_TUXEDO(TRUE)
						
						IF DOES_CUTSCENE_ENTITY_EXIST("spy_actor")
							SET_CUTSCENE_PED_COMPONENT_VARIATION("spy_actor", PED_COMP_LEG, 	1, 0)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("spy_actor", PED_COMP_TORSO, 	1, 0)
						ENDIF
						
						bPlayerWearingTuxedo = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Franklin.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1500)
				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("spy_actor")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for spy_actor.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psActor.PedIndex)
					IF HAS_ANIM_DICT_LOADED("dead")
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iDeadActorSceneID)
					
							iDeadActorSceneID = CREATE_SYNCHRONIZED_SCENE(<< -1114.511, -501.777, 35.810 >>, << -0.000, 0.000, 69.480 >>)
							
							TASK_SYNCHRONIZED_SCENE(psActor.PedIndex, iDeadActorSceneID, "dead", "dead_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psActor.PedIndex)
						
							STOP_PED_SPEAKING(psActor.PedIndex, TRUE)
							SET_ENTITY_INVINCIBLE(psActor.PedIndex, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psActor.PedIndex, TRUE)
							
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Actors_trailer_door")
				IF DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osTrailerDoor.ObjectIndex)
						FREEZE_ENTITY_POSITION(osTrailerDoor.ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF ( bMusicEventTrailerTriggered = FALSE )
				IF IS_CUTSCENE_PLAYING()
					IF GET_CUTSCENE_TIME() > 10500
						IF TRIGGER_MUSIC_EVENT("CAR3_TRAILER")
							bMusicEventTrailerTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF HAS_CUTSCENE_FINISHED()
			
				REPLAY_STOP_EVENT()
				
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1500)
				ENDIF
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				SET_GLOBAL_BIT_SET(PLAYER_GOT_TUXEDO)
								
				RETURN TRUE

			ELSE
			
				//if cutscene is skipped, fade out and remain cutscene on faded out screen
				IF ( bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						bCutsceneSkipped = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mocap cutscene was skipped by the player.")
						#ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC MANAGE_ACTOR_DURING_GET_TO_CAR(INT &iProgress)

	IF DOES_ENTITY_EXIST(psActor.PedIndex)
		IF NOT IS_ENTITY_DEAD(psActor.PedIndex)
		
			SWITCH iProgress

				CASE 0
		
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iDeadActorSceneID)
			
						REQUEST_ANIM_DICT("dead")
						
						IF HAS_ANIM_DICT_LOADED("dead")
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iDeadActorSceneID)
							
								iDeadActorSceneID = CREATE_SYNCHRONIZED_SCENE(<< -1114.511, -501.777, 35.810 >>, << -0.000, 0.000, 69.480 >>)
								
								TASK_SYNCHRONIZED_SCENE(psActor.PedIndex, iDeadActorSceneID, "dead", "dead_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psActor.PedIndex)
							
								STOP_PED_SPEAKING(psActor.PedIndex, TRUE)
								SET_ENTITY_INVINCIBLE(psActor.PedIndex, TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psActor.PedIndex, TRUE)
							
							ENDIF						
						ENDIF
					
					ELSE
					
						SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_FIRE)
						SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_MELEE)
						SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_EXPLOSION)
						SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_PLAYER_BUMP)
						SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_BULLET_IMPACT)
						SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_PLAYER_IMPACT)
						SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_IMPACT_OBJECT)
						
						iProgress++
					
					ENDIF
					
				BREAK
				
			ENDSWITCH
		
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_GET_TO_CAR_COMPLETED(INT &iStageProgress)
	
	UPDATE_TRIGGERED_LABEL(sLabelGENGETIN)

	IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
		IF NOT IS_VEHICLE_SEAT_FREE(vsPlayersCar.VehicleIndex, VS_FRONT_RIGHT)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly, TRUE)
		ENDIF
	ENDIF
	
	MANAGE_FILM_CREW(iFilmCrewProgress)
	MANAGE_ACTOR_DURING_GET_TO_CAR(iDeadActorProgress)
	MANAGE_DIRECTOR_AND_GRIP(iDirectorAndGripProgress)
	MANAGE_ACTRESS(psActress, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
	
	MANAGE_SECURITY_PEDS_AT_GATES(iGateSecurityProgress)
	MANAGE_SECURITY_PEDS_AT_FILM_SET(iSetSecurityProgress)
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_HALERT")
		IF ( bSecurityAlerted = TRUE )
			PRINT_HELP_ADVANCED("CAR4_HALERT", FALSE, TRUE, DEFAULT_HELP_TEXT_TIME, TRUE)
			IF ( bMusicEventSetAlertTriggered = FALSE )
				IF TRIGGER_MUSIC_EVENT("CAR3_SET_ALERT")
					bMusicEventSetAlertTriggered = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
			IF NOT DOES_BLIP_EXIST(vsPlayersCar.BlipIndex)
				vsPlayersCar.BlipIndex = CREATE_BLIP_FOR_VEHICLE(vsPlayersCar.VehicleIndex)
				PRINT_GOD_TEXT_ADVANCED(sLabelGENGETIN, DEFAULT_GOD_TEXT_TIME, TRUE)
			ENDIF
			
			bPlayerEnteredTargetVehicle = FALSE
						
			iStageProgress++
		
		BREAK
		
		CASE 1
		
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1187.176270,-501.910004,33.418343>>, <<-1172.835205,-508.601990,37.566460>>, 12.0)
			
				IF ( bMusicEventSetAlertTriggered = FALSE )
					IF TRIGGER_MUSIC_EVENT("CAR3_SET_ALERT")
						bMusicEventSetAlertTriggered = TRUE
					ENDIF
				ENDIF

				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)

					REPLAY_RECORD_BACK_FOR_TIME(8.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)

					IF DOES_BLIP_EXIST(vsPlayersCar.BlipIndex)
						REMOVE_BLIP(vsPlayersCar.BlipIndex)
					ENDIF
					
					IF NOT IS_PED_IN_THIS_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
						SET_GLOBAL_BIT_SET(ACTRESS_FLED_CAR)
					ENDIF

					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
					
					TRIGGER_MUSIC_EVENT("CAR3_DRIVE")
					
					IF IS_AUDIO_SCENE_ACTIVE("CAR_3_INSIDE_STUDIO")
						STOP_AUDIO_SCENE("CAR_3_INSIDE_STUDIO")
					ENDIF
						
					IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_3_ESCAPE_SECURITY")
						START_AUDIO_SCENE("CAR_3_ESCAPE_SECURITY")
					ENDIF
					
					CLEANUP_PED(psActor, TRUE)
					REMOVE_ANIM_DICT("dead")

					bPlayerEnteredTargetVehicle = TRUE

					RETURN TRUE
					
				ENDIF

			ENDIF
		
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC CLEANUP_PED_AND_VEHICLE(PED_STRUCT &ped, VEH_STRUCT &vehicle, FLOAT fCleanupDistanceInVehicle = 300.0, FLOAT fCleanupDistanceOnFoot = 150.0)
	
	IF DOES_ENTITY_EXIST(ped.PedIndex)
	
		IF IS_PED_INJURED(ped.PedIndex)
		
			IF ( ped.eCleanupReason = PED_CLEANUP_NO_CLEANUP )	//ped has no cleanup reason
		
				ped.eCleanupReason = GET_PED_CLEANUP_REASON(ped)

				UPDATE_PLAYER_KILL_COUNTERS(ped.eCleanupReason, iPlayerLethalKills, iPlayerStealthKills, iPlayerTakedownKills)
				
				IF DOES_BLIP_EXIST(ped.BlipIndex)
					REMOVE_BLIP(ped.BlipIndex)
				ENDIF
				
			ELSE												//ped has a valid cleanup reason
			
				SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
				
			ENDIF
		
		ELSE
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ped.PedIndex)
							
				IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped.PedIndex) > fCleanupDistanceOnFoot )
					
					ped.eCleanupReason = PED_CLEANUP_LOST		//flag ped cleanup reason as lost
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped on foot ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_LOST.")
					#ENDIF

					SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)

					IF DOES_BLIP_EXIST(ped.BlipIndex)
						REMOVE_BLIP(ped.BlipIndex)
					ENDIF
				
				ENDIF
				
			ELSE
		
				IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)
		
					IF IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
					
						IF IS_PED_IN_VEHICLE(ped.PedIndex, vehicle.VehicleIndex)
												
							IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped.PedIndex) > fCleanupDistanceInVehicle )
					
								ped.eCleanupReason = PED_CLEANUP_LOST		//flag ped cleanup reason as lost
								
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped in vehicle ", ped.sDebugName, " cleaned up with reason PED_CLEANUP_LOST.")
								#ENDIF
					
								SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)

								IF DOES_BLIP_EXIST(ped.BlipIndex)
									REMOVE_BLIP(ped.BlipIndex)
								ENDIF
															
							ENDIF

						ENDIF
					
					ENDIF
					
				ENDIF
			
			ENDIF
		
		ENDIF
	
	ENDIF
			
	IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)
	
		IF IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
			
			IF 	DOES_ENTITY_EXIST(ped.PedIndex)
			AND	NOT IS_ENTITY_DEAD(ped.PedIndex)
			
				IF ( GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicle.VehicleIndex) > fCleanupDistanceInVehicle )
					
					REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
					REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle.VehicleIndex)
			
					IF DOES_BLIP_EXIST(vehicle.BlipIndex)
						REMOVE_BLIP(vehicle.BlipIndex)
					ENDIF
												
				ENDIF
				
			ELSE
			
				REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
				REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
					
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle.VehicleIndex)
			
				IF DOES_BLIP_EXIST(vehicle.BlipIndex)
					REMOVE_BLIP(vehicle.BlipIndex)
				ENDIF
			
			ENDIF
		
		ELSE
			
			REMOVE_VEHICLE_STUCK_CHECK(vehicle.VehicleIndex)
			REMOVE_VEHICLE_UPSIDEDOWN_CHECK(vehicle.VehicleIndex)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle.VehicleIndex)
			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle.VehicleIndex)
			
			IF DOES_BLIP_EXIST(vehicle.BlipIndex)
				REMOVE_BLIP(vehicle.BlipIndex)
			ENDIF
		
		ENDIF
	
	ENDIF

ENDPROC

PROC UPDATE_BLIPS_FOR_PED_AND_VEHCILE(PED_STRUCT &ped, VEH_STRUCT &vehicle)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
	
		IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)
			
			IF IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
			
				IF ( ped.eState = PED_STATE_IMMOBILIZED )
				
					IF DOES_BLIP_EXIST(vehicle.BlipIndex)
						REMOVE_BLIP(vehicle.BlipIndex)
					ENDIF
				
					IF NOT DOES_BLIP_EXIST(ped.BlipIndex)
						ped.BlipIndex = CREATE_BLIP_FOR_PED(ped.PedIndex, TRUE)
					ENDIF
				
				ELSE

					IF NOT IS_PED_INJURED(ped.PedIndex)
		
						IF IS_PED_IN_VEHICLE(ped.PedIndex, vehicle.VehicleIndex)
					
							IF DOES_BLIP_EXIST(ped.BlipIndex)
								REMOVE_BLIP(ped.BlipIndex)
							ENDIF
					
							IF NOT DOES_BLIP_EXIST(vehicle.BlipIndex)
								vehicle.BlipIndex = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehicle.VehicleIndex))
								SET_BLIP_COLOUR(vehicle.BlipIndex, BLIP_COLOUR_RED)
								SET_BLIP_PRIORITY(vehicle.BlipIndex, BLIPPRIORITY_HIGHEST)
								SET_BLIP_NAME_FROM_TEXT_FILE(vehicle.BlipIndex, "BLIP_VEH")
							ENDIF
							
							IF DOES_BLIP_EXIST(vehicle.BlipIndex)
							
								VECTOR vBlipPosition 	= GET_BLIP_COORDS(vehicle.BlipIndex)
								VECTOR vVehiclePosition = GET_ENTITY_COORDS(vehicle.VehicleIndex)									

								vBlipPosition.X = vBlipPosition.X +@((vVehiclePosition.X - vBlipPosition.X) / BLIP_SLIDE_AMOUNT)
								vBlipPosition.Y = vBlipPosition.Y +@((vVehiclePosition.Y - vBlipPosition.Y) / BLIP_SLIDE_AMOUNT)
								vBlipPosition.Z = vBlipPosition.Z +@((vVehiclePosition.Z - vBlipPosition.Z) / BLIP_SLIDE_AMOUNT)

								SET_BLIP_COORDS(vehicle.BlipIndex, vBlipPosition)
							ENDIF
							
						ELSE	//driver out of the car
						
							IF DOES_BLIP_EXIST(vehicle.BlipIndex)
								REMOVE_BLIP(vehicle.BlipIndex)
							ENDIF
						
							IF NOT DOES_BLIP_EXIST(ped.BlipIndex)
								ped.BlipIndex = CREATE_BLIP_FOR_PED(ped.PedIndex, TRUE)
							ENDIF
							
						ENDIF
					
					ENDIF

				ENDIF
			
			ELSE	//vehicle not driveable, diplay the driver blip then
			
				IF NOT IS_PED_INJURED(ped.PedIndex)
				
					IF DOES_BLIP_EXIST(vehicle.BlipIndex)
						REMOVE_BLIP(vehicle.BlipIndex)
					ENDIF
				
					IF NOT DOES_BLIP_EXIST(ped.BlipIndex)
						ped.BlipIndex = CREATE_BLIP_FOR_PED(ped.PedIndex, TRUE)
					ENDIF
				
				ENDIF

			ENDIF
		
		ELSE	//vehicle does not exist, display driver blip
			
			IF NOT IS_PED_INJURED(ped.PedIndex)
				
				IF DOES_BLIP_EXIST(vehicle.BlipIndex)
					REMOVE_BLIP(vehicle.BlipIndex)
				ENDIF
			
				IF NOT DOES_BLIP_EXIST(ped.BlipIndex)
					ped.BlipIndex = CREATE_BLIP_FOR_PED(ped.PedIndex, TRUE)
				ENDIF
			
			ENDIF
			
		ENDIF
	
	ENDIF

ENDPROC

PROC RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(VEHICLE_INDEX VehicleIndex, VEHICLE_INDEX TargetVehicleIndex, VECTOR &vWarpPosition,
												   FLOAT &fWarpHeading, VECTOR &vLastTargetPosition, FLOAT &fLastTargetHeading,
												   INT &iWarpTimer, VECTOR vWarpOffset, INT iWarpTimeDelay, FLOAT fWarpDistance)

	IF DOES_ENTITY_EXIST(VehicleIndex)
		IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
					
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vLastTargetPosition) > 20.0
				
				vWarpPosition 	= vLastTargetPosition	//store last target vehicle positon as the new warp position
				fWarpHeading 	= fLastTargetHeading	//store last target vehicle heading as the new warp heading
				
				vWarpPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarpPosition, fWarpHeading, vWarpOffset)
				
				STORE_ENTITY_POSITION_AND_HEADING(TargetVehicleIndex, vLastTargetPosition, fLastTargetHeading)
				
			ENDIF
			
			IF IS_ENTITY_ON_SCREEN(VehicleIndex)
			
			      iWarpTimer = GET_GAME_TIMER()
				  
			ELIF GET_GAME_TIMER() - iWarpTimer > iWarpTimeDelay
			
				IF 	GET_DISTANCE_BETWEEN_ENTITIES(VehicleIndex, TargetVehicleIndex) > fWarpDistance
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TargetVehicleIndex), vWarpPosition) > 20.0
				
					IF NOT IS_VECTOR_ZERO(vWarpPosition)
						IF NOT IS_SPHERE_VISIBLE(vWarpPosition, 3.0)
						  
							CLEAR_AREA_OF_PEDS(vWarpPosition, 1.5)
					        CLEAR_AREA_OF_VEHICLES(vWarpPosition, 3.0)
							
							SET_ENTITY_COORDS(VehicleIndex, vWarpPosition)
							SET_ENTITY_HEADING(VehicleIndex, fWarpHeading)
				            SET_VEHICLE_ON_GROUND_PROPERLY(VehicleIndex)
				            
				            SET_VEHICLE_FORWARD_SPEED(VehicleIndex, 10.0)
				            
				            SET_VEHICLE_ENGINE_ON(VehicleIndex, TRUE, TRUE)
				            
				            iWarpTimer = GET_GAME_TIMER()
							
						ENDIF
				    ENDIF
					  
				ENDIF
			ENDIF

		
		
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_SECURITY_PED_IN_VEHICLE(PED_STRUCT &ped, VEH_STRUCT &vehicle, PED_INDEX TargetPedIndex, VEHICLE_INDEX TargetVehicleIndex)

	IF 	DOES_ENTITY_EXIST(ped.PedIndex)
	AND	NOT IS_ENTITY_DEAD(ped.PedIndex)
	AND NOT IS_ENTITY_DEAD(TargetPedIndex)
	AND NOT IS_ENTITY_DEAD(TargetVehicleIndex)
	
		#IF IS_DEBUG_BUILD
			IF ( bDrawDebugStates = TRUE )
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(ped.PedIndex, GET_PED_STATE_NAME_FOR_DEBUG(ped.eState), 1.25)
			ENDIF
		#ENDIF
		
		ped.fDistanceToTarget = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(ped.PedIndex), GET_ENTITY_COORDS(TargetPedIndex))
	
		IF ( bCarSecurityAggressive = FALSE )
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped.PedIndex, TargetPedIndex, TRUE)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(ped.PedIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(ped.PedIndex)
					bCarSecurityAggressive = TRUE
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)
				IF NOT IS_ENTITY_DEAD(vehicle.VehicleIndex)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle.VehicleIndex, TargetPedIndex, TRUE)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vehicle.VehicleIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehicle.VehicleIndex)
							bCarSecurityAggressive = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		SWITCH ped.eState

			CASE PED_STATE_COMBAT_ON_FOOT

				IF ( ped.bHasTask = FALSE )
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, FALSE)
					TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
					ped.bHasTask = TRUE
				ENDIF
				
				IF IS_PED_DRIVING_VEHICLE(TargetPedIndex, TargetVehicleIndex)
				
					IF ( ped.fDistanceToTarget > 20.0)
					
						IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)
							IF 	IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
							AND NOT IS_VEHICLE_STUCK_ON_ROOF(vehicle.VehicleIndex)
							AND NOT IS_VEHICLE_PERMANENTLY_STUCK(vehicle.VehicleIndex)
							
								IF NOT IS_PED_IN_VEHICLE(ped.PedIndex, vehicle.VehicleIndex)
							
									IF IS_VEHICLE_SEAT_FREE(vehicle.VehicleIndex, VS_DRIVER)
										GO_TO_PED_STATE(ped, PED_STATE_ENTERING_VEHICLE)
									ENDIF
									
								ELSE
									
									IF ( bCarSecurityAggressive = TRUE )
										GO_TO_PED_STATE(ped, PED_STATE_COMBAT_FROM_CAR_SHOOTING)
									ELSE
										GO_TO_PED_STATE(ped, PED_STATE_COMBAT_FROM_CAR_NO_SHOOTING)
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF

				ENDIF
				
			BREAK
			
			CASE PED_STATE_COMBAT_FROM_CAR_NO_SHOOTING
			
				IF ( ped.bHasTask = FALSE )
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DO_DRIVEBYS, FALSE)
					TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
					ped.iTimer 		= GET_GAME_TIMER()
					ped.bHasTask 	= TRUE
				ENDIF
				
				SET_PED_RESET_FLAG(ped.PedIndex, PRF_BlockWeaponFire, TRUE)	//block weapon fire in no shooting state each frame
				
				IF ( bCarSecurityAggressive = TRUE )
					GO_TO_PED_STATE(ped, PED_STATE_COMBAT_FROM_CAR_SHOOTING)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
					IF IS_PED_SITTING_IN_VEHICLE(ped.PedIndex, vehicle.VehicleIndex)
						IF ( ped.fDistanceToTarget < 17.5 )
							IF NOT IS_PED_DRIVING_VEHICLE(TargetPedIndex, TargetVehicleIndex)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
						ENDIF
					ENDIF
					RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehicle.VehicleIndex, TargetVehicleIndex,
															      vehicle.vPosition, vehicle.fHeading,
																  vehicle.vTargetPosition, vehicle.fTargetHeading,
																  ped.iTimer, vehicle.vOffset, 3000, 40.0)
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
					GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
				ENDIF
				
				IF ( vehicle.bImmobilized = TRUE )
					GO_TO_PED_STATE(ped, PED_STATE_IMMOBILIZED)
				ENDIF
			
			BREAK
			
			CASE PED_STATE_COMBAT_FROM_CAR_SHOOTING
			
				IF ( ped.bHasTask = FALSE )
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_DO_DRIVEBYS, TRUE)
					TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
					ped.iTimer 		= GET_GAME_TIMER()
					ped.bHasTask 	= TRUE
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
					IF IS_PED_SITTING_IN_VEHICLE(ped.PedIndex, vehicle.VehicleIndex)
						IF ( ped.fDistanceToTarget < 17.5 )
							IF NOT IS_PED_DRIVING_VEHICLE(TargetPedIndex, TargetVehicleIndex)
								GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
							ENDIF
						ENDIF
					ENDIF
					RUN_VEHICLE_WARP_CHECK_BASED_ON_TARGET_VEHICLE(vehicle.VehicleIndex, TargetVehicleIndex,
															      vehicle.vPosition, vehicle.fHeading,
																  vehicle.vTargetPosition, vehicle.fTargetHeading,
																  ped.iTimer, vehicle.vOffset, 3000, 40.0)
				ENDIF
			
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
					GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
				ENDIF
				
				IF ( vehicle.bImmobilized = TRUE )
					GO_TO_PED_STATE(ped, PED_STATE_IMMOBILIZED)
				ENDIF
			
			BREAK
			
			CASE PED_STATE_ENTERING_VEHICLE
			
				IF ( ped.bHasTask = FALSE )
					IF DOES_ENTITY_EXIST(vehicle.VehicleIndex)
						IF IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, FALSE)
							TASK_ENTER_VEHICLE(ped.PedIndex, vehicle.VehicleIndex, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JACK_ANYONE)
						ENDIF
					ENDIF
					ped.bHasTask = TRUE
				ENDIF
											
				IF	DOES_ENTITY_EXIST(vehicle.VehicleIndex)
				AND IS_VEHICLE_DRIVEABLE(vehicle.VehicleIndex)
				
					IF IS_PED_IN_VEHICLE(ped.PedIndex, vehicle.VehicleIndex)
					
						IF ( bCarSecurityAggressive = TRUE )
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_FROM_CAR_SHOOTING)
						ELSE
							GO_TO_PED_STATE(ped, PED_STATE_COMBAT_FROM_CAR_NO_SHOOTING)
						ENDIF
						
					ENDIF
					
				ELSE
				
					GO_TO_PED_STATE(ped, PED_STATE_COMBAT_ON_FOOT)
					
				ENDIF
			
			BREAK
			
			CASE PED_STATE_IMMOBILIZED

				IF ( ped.bHasTask = FALSE )
					SET_VEHICLE_UNDRIVEABLE(vehicle.VehicleIndex, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_LEAVE_VEHICLES, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(ped.PedIndex, CA_USE_VEHICLE, FALSE)
					SET_PED_COMBAT_MOVEMENT(ped.PedIndex, CM_WILLADVANCE)
					SET_PED_COMBAT_ABILITY(ped.PedIndex, CAL_AVERAGE)
					SET_PED_COMBAT_RANGE(ped.PedIndex, CR_NEAR)
					SET_PED_SHOOT_RATE(ped.PedIndex, 20)
					SET_PED_ACCURACY(ped.PedIndex, 20)
					TASK_COMBAT_PED(ped.PedIndex, TargetPedIndex)
					ped.bHasTask = TRUE
				ENDIF

			BREAK
			
		ENDSWITCH
	
	ENDIF

ENDPROC

PROC MANAGE_SECURITY_PEDS_IN_VEHICLES(INT &iProgress)

	IF ( bSecurityInVehiclesAllowed = FALSE )
	
		IF ( bPlayerInsideFilmStudio = FALSE )
			
			bStartSecurityChaseTimerDelay = TRUE
			
			IF ( iSecurityChaseDelayTimer = 0 )
				IF ( bStartSecurityChaseTimerDelay = TRUE )
					iSecurityChaseDelayTimer = GET_GAME_TIMER()
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting security chase delay timer.")
					#ENDIF
				ENDIF
			ELSE
				//if the player stops the car outside the film studio, spawn the security cars sooner
				IF HAS_TIME_PASSED(PICK_INT(IS_VEHICLE_STOPPED(vsPlayersCar.VehicleIndex), 5000, 10000), iSecurityChaseDelayTimer)
					bSecurityInVehiclesAllowed = TRUE
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security in vehicles are allowed to spawn due to timer.")
					#ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(vsPlayersCar.VehicleIndex, << -840.78, -345.37, 35.68 >>, << -806.72, -359.60, 44.10 >>, 32.0)
				
				vsSecurityCars[0].vPosition			= << -940.7892, -425.8869, 36.6582 >>
				vsSecurityCars[0].fHeading			= 299.7635

				vsSecurityCars[1].vPosition			= << -914.2634, -410.5154, 36.5965 >>
				vsSecurityCars[1].fHeading			= 295.6050
				
				bSecurityInVehiclesAllowed = TRUE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security in vehicles are allowed to spawn due to player reaching locate 1.")
				#ENDIF
				
			ELIF IS_ENTITY_IN_ANGLED_AREA(vsPlayersCar.VehicleIndex, <<-922.50, -462.98, 34.38 >>, << -958.13, -481.30, 44.20 >>, 32.0)
			
				vsSecurityCars[0].vPosition			= << -990.2645, -451.9471, 36.5288 >>
				vsSecurityCars[0].fHeading			= 298.9762
				
				vsSecurityCars[1].vPosition			= << -924.7919, -408.3496, 36.5208 >>
				vsSecurityCars[1].fHeading			= 118.9813
				
				bSecurityInVehiclesAllowed = TRUE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security in vehicles are allowed to spawn due to player reaching locate 2.")
				#ENDIF
			
			ELIF IS_ENTITY_IN_ANGLED_AREA(vsPlayersCar.VehicleIndex, << -940.00, -366.51, 35.96 >>, << -1011.40, -402.69, 46.61 >>, 32.0)
			
				vsSecurityCars[0].vPosition			= << -1043.2084, -376.0805, 36.8852 >>
				vsSecurityCars[0].fHeading			= 298.4807
				
				vsSecurityCars[1].vPosition			= << -945.8993, -313.9898, 37.9018 >>
				vsSecurityCars[1].fHeading			= 119.5346
				
				bSecurityInVehiclesAllowed = TRUE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security in vehicles are allowed to spawn due to player reaching locate 3.")
				#ENDIF
			
			ELIF IS_ENTITY_IN_ANGLED_AREA(vsPlayersCar.VehicleIndex, << -1043.86, -410.67, 34.58 >>, << -1088.86, -433.64, 51.62 >>, 32.0)
			
				vsSecurityCars[0].vPosition			= << -1037.3854, -309.7007, 36.7360 >>
				vsSecurityCars[0].fHeading			= 188.3013
				
				vsSecurityCars[1].vPosition			= << -1169.5857, -422.5981, 33.9917 >> 
				vsSecurityCars[1].fHeading			= 276.9465
				
				bSecurityInVehiclesAllowed = TRUE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security in vehicles are allowed to spawn due to player reaching locate 4.")
				#ENDIF
			
			ELIF IS_ENTITY_IN_ANGLED_AREA(vsPlayersCar.VehicleIndex, << -1206.13, -646.00, 20.83 >>, << -1175.31, -618.72, 31.01 >>, 32.0)
			
				vsSecurityCars[0].vPosition			= << -1246.7711, -612.4629, 26.1706 >>
				vsSecurityCars[0].fHeading			= 308.9866
				
				vsSecurityCars[1].vPosition			= << -1137.7020, -615.8666, 20.7124 >>
				vsSecurityCars[1].fHeading			= 126.5517
				
				bSecurityInVehiclesAllowed = TRUE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security in vehicles are allowed to spawn due to player reaching locate 5.")
				#ENDIF
			
			ELIF IS_ENTITY_IN_ANGLED_AREA(vsPlayersCar.VehicleIndex, << -1248.37, -625.58, 24.17 >>, << -1263.54, -606.61, 34.16 >>, 32.0)
			
				vsSecurityCars[0].vPosition			= << -1273.5568, -554.0495, 29.3703 >>
				vsSecurityCars[0].fHeading			= 219.1150
				
				vsSecurityCars[1].vPosition			= << -1188.8282, -635.2723, 22.7021 >>
				vsSecurityCars[1].fHeading			= 41.9663
				
				bSecurityInVehiclesAllowed = TRUE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security in vehicles are allowed to spawn due to player reaching locate 6.")
				#ENDIF
			
			ELIF IS_ENTITY_IN_ANGLED_AREA(vsPlayersCar.VehicleIndex, << -1291.04, -558.91, 28.71 >>, << -1254.28, -527.38, 38.07 >>, 32.0)
			
				vsSecurityCars[0].vPosition			= << -1255.1533, -618.7726, 26.1000 >>
				vsSecurityCars[0].fHeading			= 303.7104
				
				vsSecurityCars[1].vPosition			= << -1263.9615, -464.1984, 32.5328 >>
				vsSecurityCars[1].fHeading			= 141.2234
				
				bSecurityInVehiclesAllowed = TRUE
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security in vehicles are allowed to spawn due to player reaching locate 7.")
				#ENDIF
			
			ENDIF
		
		ENDIF
	ENDIF

	SWITCH iProgress
	
		CASE 0	//request models
		
			REQUEST_MODEL(mnSecurityCar)
			REQUEST_MODEL(mnSecurityPed)
		
			IF 	HAS_MODEL_LOADED(mnSecurityCar)
			AND HAS_MODEL_LOADED(mnSecurityPed)
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Models ", GET_MODEL_NAME_FOR_DEBUG(mnSecurityCar), " and ", GET_MODEL_NAME_FOR_DEBUG(mnSecurityPed), " are loaded.")
				#ENDIF
				
				vsSecurityCars[0].vPosition			= << 0.0, 0.0, 0.0 >>
				vsSecurityCars[0].fHeading			= 0.0
				vsSecurityCars[0].vOffset			= << -2.15, -3.0, 0.0 >>
				vsSecurityCars[0].ModelName			= mnSecurityCar
				
				vsSecurityCars[1].vPosition			= << 0.0, 0.0, 0.0 >>
				vsSecurityCars[1].fHeading			= 0.0
				vsSecurityCars[1].vOffset			= << 2.15, 3.0, 0.0 >>
				vsSecurityCars[1].ModelName			= mnSecurityCar
			
				iProgress++
				
			ENDIF
		
		BREAK
		
		CASE 1
		
			IF ( bSecurityInVehiclesAllowed = TRUE )
				
				IF IS_VECTOR_ZERO(vsSecurityCars[0].vPosition)
				OR IS_VECTOR_ZERO(vsSecurityCars[1].vPosition)
				
					VECTOR vPlayerPosition
					VECTOR vClosestNodeToPlayer
					
					vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
					IF GET_CLOSEST_VEHICLE_NODE(vPlayerPosition, vClosestNodeToPlayer, NF_NONE)
					
						IF ( GET_DISTANCE_BETWEEN_COORDS(vPlayerPosition, vClosestNodeToPlayer) < 7.0 )
														
							IF CAN_GET_CLOSEST_NTH_VEHICLE_NODE_TO_ENTITY_BETWEEN_ANGLES(PLAYER_PED_ID(), vClosestRoadNode, fClosestRoadNodeHeading, 20,
																						 PICK_FLOAT(IS_VEHICLE_STOPPED(vsPlayersCar.VehicleIndex), 0.0, 120.0),
																						 PICK_FLOAT(IS_VEHICLE_STOPPED(vsPlayersCar.VehicleIndex), 180.0, 180.0))
																					
								IF NOT IS_SPHERE_VISIBLE(vClosestRoadNode, 3.0)
							
									INT 	i
									FLOAT 	fHeadingDifference
									FLOAT 	fPlayerHeading
									FLOAT	fSpawnHeading
							
									fPlayerHeading 		= GET_ENTITY_HEADING(PLAYER_PED_ID())
									fHeadingDifference 	= ABSF(fPlayerHeading - fClosestRoadNodeHeading)
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), " fPlayerHeading: ", fPlayerHeading,
																		" fClosestRoadNodeHeading: ", fClosestRoadNodeHeading,
																		" fHeadingDifference: ", fHeadingDifference, ".")
									#ENDIF
									
									IF ( fHeadingDifference > 180.0 )
										fHeadingDifference = ABSF(fHeadingDifference - 360.0)
									ENDIF
									
									IF ( fHeadingDifference < 90.0 )
										fSpawnHeading = fClosestRoadNodeHeading
									ELSE
										fSpawnHeading = fClosestRoadNodeHeading + 180
									ENDIF
									
									FOR i = 0 TO ( COUNT_OF(vsSecurityCars) - 1 )
									
										vsSecurityCars[i].vPosition 				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vClosestRoadNode, fClosestRoadNodeHeading, vsSecurityCars[i].vOffset)
										vsSecurityCars[i].fHeading 					= fSpawnHeading
										vsSecurityCars[i].bLineOfSightToTarget		= FALSE

										CLEAR_AREA_OF_VEHICLES(vsSecurityCars[i].vPosition, 3.0)
										
									ENDFOR
																	
									iProgress++
								
								ENDIF
								
							ENDIF
							
						ENDIF
						
					ENDIF
				
				ELSE
				
					iProgress++
					
				ENDIF
				
			ENDIF
		
		BREAK
				
		CASE 2	//create security cars
		
			REQUEST_MODEL(mnSecurityCar)
			REQUEST_MODEL(mnSecurityPed)
			
			IF  HAS_MODEL_LOADED(mnSecurityCar)
			AND HAS_MODEL_LOADED(mnSecurityPed)
			
				IF 	HAS_MISSION_VEHICLE_BEEN_CREATED(vsSecurityCars[0], FALSE, FALSE, NO_CHARACTER, FALSE, -1, 0, 0, 0, 0 #IF IS_DEBUG_BUILD, "Car 0" #ENDIF)
				AND HAS_MISSION_VEHICLE_BEEN_CREATED(vsSecurityCars[1], FALSE, FALSE, NO_CHARACTER, FALSE, -1, 0, 0, 0, 0 #IF IS_DEBUG_BUILD, "Car 1" #ENDIF)

					INT i
					
					REPEAT 2 i
					
						psCarSecurity[i].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(vsSecurityCars[i].VehicleIndex, mnSecurityPed,
																	   			RELGROUPHASH_SECURITY_GUARD, VS_DRIVER, 200, WEAPONTYPE_PISTOL
																	   			#IF IS_DEBUG_BUILD, GET_STRING_FROM_INT(i) #ENDIF)
							
						SET_VEHICLE_ENGINE_ON(vsSecurityCars[i].VehicleIndex, TRUE, TRUE)
						SET_VEHICLE_FORWARD_SPEED(vsSecurityCars[i].VehicleIndex, 20.0)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vsSecurityCars[i].VehicleIndex, TRUE)
						ADD_VEHICLE_UPSIDEDOWN_CHECK(vsSecurityCars[i].VehicleIndex)
						ADD_VEHICLE_STUCK_CHECK_WITH_WARP(vsSecurityCars[i].VehicleIndex, 10, 1000, FALSE, FALSE, FALSE, -1)
						
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vsSecurityCars[i].VehicleIndex, "CAR_3_SEC_CARS")
						
						STORE_ENTITY_POSITION_AND_HEADING(vsPlayersCar.VehicleIndex, vsSecurityCars[i].vTargetPosition, vsSecurityCars[i].fTargetHeading)
						
						vsSecurityCars[i].bImmobilized = FALSE
						
						psCarSecurity[i].bHasTask	= FALSE
						psCarSecurity[i].eState 	= PED_STATE_COMBAT_FROM_CAR_NO_SHOOTING
						
					ENDREPEAT
				
					SET_MODEL_AS_NO_LONGER_NEEDED(mnSecurityCar)
					SET_MODEL_AS_NO_LONGER_NEEDED(mnSecurityPed)

					bChaseInProgress		= TRUE
					bChaseStarted			= TRUE
					bChaseFinished 			= FALSE
					
					#IF IS_DEBUG_BUILD
						psCarSecurity[0].sDebugName	= "CarSecurity 0"
						psCarSecurity[1].sDebugName	= "CarSecurity 1"
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security chase started.")
					#ENDIF

					iProgress++
					
				ENDIF
			
			ENDIF

		BREAK
		
		CASE 3	//keep updating the security peds in security vehicles
	
			INT i
					
			REPEAT 2 i
	
				UPDATE_BLIPS_FOR_PED_AND_VEHCILE(psCarSecurity[i], vsSecurityCars[i])
				CLEANUP_PED_AND_VEHICLE(psCarSecurity[i], vsSecurityCars[i], 320.0, 150.0)
				MANAGE_SECURITY_PED_IN_VEHICLE(psCarSecurity[i], vsSecurityCars[i], PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
			ENDREPEAT
										   
			IF ( vsSecurityCars[0].bLineOfSightToTarget = FALSE AND vsSecurityCars[1].bLineOfSightToTarget = FALSE )	//check enemy cars line of sight
				vsSecurityCars[0].bLineOfSightToTarget = HAS_ENTITY_CLEAR_LOS_TO_ENTITY(vsSecurityCars[0].VehicleIndex, vsPlayersCar.VehicleIndex)
				vsSecurityCars[1].bLineOfSightToTarget = HAS_ENTITY_CLEAR_LOS_TO_ENTITY(vsSecurityCars[1].VehicleIndex, vsPlayersCar.VehicleIndex)
			ENDIF
			
			IF 	NOT DOES_ENTITY_EXIST(psCarSecurity[0].PedIndex)
			AND NOT DOES_ENTITY_EXIST(psCarSecurity[1].PedIndex)
			AND NOT DOES_ENTITY_EXIST(vsSecurityCars[0].VehicleIndex)
			AND NOT DOES_ENTITY_EXIST(vsSecurityCars[1].VehicleIndex)
			
				bChaseFinished 			= TRUE
				bChaseInProgress 		= FALSE
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR4_HCARS")
					CLEAR_HELP()
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
					SET_VEHICLE_STRONG(vsPlayersCar.VehicleIndex, FALSE)
				ENDIF
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Security chase finished.")
				#ENDIF
				
				iProgress++

			ENDIF

		BREAK

	ENDSWITCH

ENDPROC

PROC MANAGE_ACTOR_DURING_ESCAPE_FILM_SET()

	SWITCH psActor.iProgress
	
		CASE 0
		
			IF 	NOT IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTOR_KILLED)
			AND	NOT IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTOR_IGNORED)
				
				psActor.iProgress++
				
			ELSE
			
				IF 	IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTOR_KILLED)
				AND IS_BIT_SET(g_iCarSteal3AgentBitSet, PLAYER_GOT_TUXEDO)
				
					psActor.iProgress = 10
				
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 1
		
			psActor.vPosition	= << -1105.23, -491.20, 34.83 >>	//coords inside a building 
			psActor.fHeading	= 206.1382							//this is fine as ped has to be hidden
			psActor.ModelName	= U_M_M_SPYACTOR					//ped will play animation afterwards
			
			psActor.iProgress++
		
		BREAK
		
		CASE 2
		
			IF HAS_MISSION_PED_BEEN_CREATED(psActor, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
			
				IF NOT IS_PED_INJURED(psActor.PedIndex)
				
					IF IS_BIT_SET(g_iCarSteal3AgentBitSet, PLAYER_GOT_TUXEDO)
						SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_LEG, 1, 0, 0)
						SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_TORSO, 1, 0, 0)
					ELSE
						SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_LEG, 0, 0, 0)
						SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_TORSO, 0, 0, 0)
					ENDIF
					
				ENDIF
				
				REQUEST_ANIM_DICT("misscarsteal4@actor")
				
				#IF IS_DEBUG_BUILD
					psActor.sDebugName = "Actor"
				#ENDIF
			
				psActor.eCleanupReason		= PED_CLEANUP_NO_CLEANUP	//reset actor ped cleanup reason
				psActor.iCleanupAttempts 	= 0 
				psActor.iProgress++
			
			ENDIF
		
		BREAK
		
		CASE 3
		
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1177.465454,-508.063934,36.566887>>, <<28.0, 16.0, 8.0 >>)
			
				IF DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osTrailerDoor.ObjectIndex)
						SET_ENTITY_ROTATION(osTrailerDoor.ObjectIndex, osTrailerDoor.vRotation)
					ENDIF
				ENDIF
			
				IF NOT IS_PED_INJURED(psActor.PedIndex)
			
					SET_ENTITY_HEALTH(psActor.PedIndex, 101)

					SEQUENCE_INDEX SequenceIndex

					OPEN_SEQUENCE_TASK(SequenceIndex)
						TASK_PLAY_ANIM_ADVANCED(NULL, "misscarsteal4@actor", "stumble", << -1111.76, -505.90, 35.035 >>, << 0.0, 0.0, -164.0 >>,
												INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
												AF_EXIT_AFTER_INTERRUPTED | AF_EXTRACT_INITIAL_OFFSET, 0.1)
						TASK_PLAY_ANIM(NULL, "misscarsteal4@actor", "dazed_idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
									   AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_EXTRACT_INITIAL_OFFSET)
					CLOSE_SEQUENCE_TASK(SequenceIndex)
					TASK_PERFORM_SEQUENCE(psActor.PedIndex, SequenceIndex)
					CLEAR_SEQUENCE_TASK(SequenceIndex)
					
				ENDIF
				
				psActor.iTimer = 0
				psActor.iProgress++
				
			ENDIF
			
		BREAK
		
		CASE 4
		
			IF DOES_ENTITY_EXIST(psActor.PedIndex)
			
				psActor.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(psActor.PedIndex, PLAYER_PED_ID())
			
				IF NOT IS_ENTITY_DEAD(psActor.PedIndex)
				
					IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
				
						IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psActor.PedIndex, vsPlayersCar.VehicleIndex, FALSE)
							IF IS_ENTITY_TOUCHING_ENTITY(vsPlayersCar.VehicleIndex, psActor.PedIndex)
							
								IF ( psActor.iTimer = 0 )
								
									psActor.iTimer = GET_GAME_TIMER()
									
								ELSE
								
									IF HAS_TIME_PASSED(300, psActor.iTimer)
																		
										SET_PED_TO_RAGDOLL(psActor.PedIndex, 10000, 20000, TASK_RELAX)
										APPLY_DAMAGE_TO_PED(psActor.PedIndex, 10, TRUE)
										
										psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_SCRIPT
									
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", psActor.sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_SCRIPT.")
										#ENDIF
									
									ENDIF
								
								ENDIF
							ENDIF
						ENDIF
				
					ELSE
					
						IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psActor.PedIndex, PLAYER_PED_ID(), FALSE)
							IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), psActor.PedIndex)
								IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
								
									SET_PED_TO_RAGDOLL(psActor.PedIndex, 10000, 20000, TASK_RELAX)
									APPLY_DAMAGE_TO_PED(psActor.PedIndex, 10, TRUE)
									
									psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_SCRIPT
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", psActor.sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_SCRIPT.")
									#ENDIF

								ENDIF
							ENDIF
						ENDIF
					
					ENDIF

				ELSE	//actor ped is dead
				
					IF ( psActor.eCleanupReason = PED_CLEANUP_NO_CLEANUP )			//ped has no cleanup reason
					
						psActor.eCleanupReason = GET_PED_CLEANUP_REASON(psActor)	//call this until cleanup reason is returned
						
						IF ( psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_PLAYER_VEHICLE )
						
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CS3_RAN_OVER_ACTOR_AGAIN)
							REPLAY_RECORD_BACK_FOR_TIME(3, 0.0, REPLAY_IMPORTANCE_HIGH)
							
						ENDIF
					
					ENDIF

				ENDIF
				
				IF ( psActor.fDistanceToTarget > 200.0 )
					IF ( bPlayerInsideFilmStudio = FALSE)			
			
						REMOVE_ANIM_DICT("misscarsteal4@actor")
						SET_PED_AS_NO_LONGER_NEEDED(psActor.PedIndex)

					ENDIF
				ENDIF
				
				
			ENDIF
			
		BREAK
		
		CASE 10
		
			IF 	IS_BIT_SET(g_iCarSteal3AgentBitSet, ACTOR_KILLED)
			AND IS_BIT_SET(g_iCarSteal3AgentBitSet, PLAYER_GOT_TUXEDO)
				
				IF HAS_MISSION_PED_BEEN_CREATED(psActor, FALSE, rgFilmCrew, FALSE, NO_CHARACTER, FALSE, TRUE)
			
					IF NOT IS_PED_INJURED(psActor.PedIndex)
					
						IF IS_BIT_SET(g_iCarSteal3AgentBitSet, PLAYER_GOT_TUXEDO)
							SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_LEG, 1, 0, 0)
							SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_TORSO, 1, 0, 0)
						ELSE
							SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_LEG, 0, 0, 0)
							SET_PED_COMPONENT_VARIATION(psActor.PedIndex, PED_COMP_TORSO, 0, 0, 0)
						ENDIF
						
						IF DOES_ENTITY_EXIST(osTrailerDoor.ObjectIndex)
							IF NOT IS_ENTITY_DEAD(osTrailerDoor.ObjectIndex)
								SET_ENTITY_ROTATION(osTrailerDoor.ObjectIndex, << 0.0, 0.0, 27 >>)
							ENDIF
						ENDIF
						
						REMOVE_ANIM_DICT("misscarsteal4@actor")
						
						iDeadActorProgress 	= 0
						psActor.iProgress	= 11
						
					ENDIF
					
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 11
		
			IF DOES_ENTITY_EXIST(psActor.PedIndex)
			
				psActor.fDistanceToTarget = GET_DISTANCE_BETWEEN_ENTITIES(psActor.PedIndex, PLAYER_PED_ID())
			
				IF NOT IS_ENTITY_DEAD(psActor.PedIndex)
				
					SWITCH iDeadActorProgress

						CASE 0
				
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iDeadActorSceneID)
					
								REQUEST_ANIM_DICT("dead")
								
								IF HAS_ANIM_DICT_LOADED("dead")
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iDeadActorSceneID)
									
										iDeadActorSceneID = CREATE_SYNCHRONIZED_SCENE(<< -1114.511, -501.777, 35.810 >>, << -0.000, 0.000, 69.480 >>)
										
										TASK_SYNCHRONIZED_SCENE(psActor.PedIndex, iDeadActorSceneID, "dead", "dead_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(psActor.PedIndex)
									
										STOP_PED_SPEAKING(psActor.PedIndex, TRUE)
										SET_ENTITY_INVINCIBLE(psActor.PedIndex, TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psActor.PedIndex, TRUE)
									
									ENDIF						
								ENDIF
							
							ELSE
							
								SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_FIRE)
								SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_MELEE)
								SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_EXPLOSION)
								SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_PLAYER_BUMP)
								SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_BULLET_IMPACT)
								SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_PLAYER_IMPACT)
								SET_RAGDOLL_BLOCKING_FLAGS(psActor.PedIndex, RBF_IMPACT_OBJECT)
								
								iDeadActorProgress++
							
							ENDIF
							
						BREAK
						
						CASE 1
						
							IF ( psActor.fDistanceToTarget > 200.0 )
								IF ( bPlayerInsideFilmStudio = FALSE)			
						
									REMOVE_ANIM_DICT("dead")
									REMOVE_ANIM_DICT("misscarsteal4@actor")
									SET_PED_AS_NO_LONGER_NEEDED(psActor.PedIndex)

								ENDIF
							ENDIF
						
						BREAK
						
					ENDSWITCH
				
				ENDIF
			ENDIF
		
		BREAK
		
	ENDSWITCH

ENDPROC

PROC SETUP_LABELS_FOR_END_PHONE_CALL()

	IF ( iPlayerLethalKills > 0 )								//decide if clean or messy end call should be triggered
		bEndCallMessy = TRUE
	ELSE
		bEndCallMessy = FALSE
	ENDIF


	IF ( bEndCallMessy = FALSE )								//if end call clean should be triggered
	
		EndCallCleanRoots[0] 			= "CST4_ECALLC"			//build the start of end call clean
		EndCallCleanLines[0] 			= "CST4_ECALLC_1"
		
		EndCallCleanRoots[1] 			= "CST4_ECALLC"
		EndCallCleanLines[1] 			= "CST4_ECALLC_2"
		
		EndCallCleanRoots[2] 			= "CST4_ECALLC"
		EndCallCleanLines[2] 			= "CST4_ECALLC_3"
		
		EndCallCleanRoots[3] 			= "CST4_ECALLC"
		EndCallCleanLines[3] 			= "CST4_ECALLC_4"
		
		EndCallCleanRoots[4] 			= "CST4_ECALLC"
		EndCallCleanLines[4] 			= "CST4_ECALLC_5"
		
		EndCallCleanRoots[5] 			= "CST4_ECALLC"
		EndCallCleanLines[5] 			= "CST4_ECALLC_6"
		
		IF ( bActressEjected = FALSE )							//build the dynamic part of the end call clean
			IF ( CarGadgetData.iTimesSpikesDropped > 0 )
				EndCallCleanRoots[6] 	= "CST4_ECALL1"
				EndCallCleanLines[6] 	= "CST4_ECALL1_1"
			ELSE
				EndCallCleanRoots[6] 	= "CST4_ECALL3"
				EndCallCleanLines[6] 	= "CST4_ECALL3_1"
			ENDIF
		ELSE
			IF ( CarGadgetData.iTimesSpikesDropped > 0 )
				EndCallCleanRoots[6] 	= "CST4_ECALL2"
				EndCallCleanLines[6] 	= "CST4_ECALL2_1"
			ELSE
				EndCallCleanRoots[6] 	= "CST4_ECALL4"
				EndCallCleanLines[6] 	= "CST4_ECALL4_1"
			ENDIF
		ENDIF
		
		EndCallCleanRoots[7] 			= "CST4_ECALLE"			//build the end of the end call clean
		EndCallCleanLines[7]			= "CST4_ECALLE_1"
		
		EndCallCleanRoots[8] 			= "CST4_ECALLE"
		EndCallCleanLines[8]			= "CST4_ECALLE_2"
	
	ELSE														//if end call messy should be triggered
	
		EndCallMessyRoots[0] 			= "CST4_ECALLM"			//build the start of end call messy
		EndCallMessyLines[0] 			= "CST4_ECALLM_1"
		
		EndCallMessyRoots[1] 			= "CST4_ECALLM"
		EndCallMessyLines[1] 			= "CST4_ECALLM_2"
		
		EndCallMessyRoots[2] 			= "CST4_ECALLM"
		EndCallMessyLines[2] 			= "CST4_ECALLM_3"
		
		EndCallMessyRoots[3] 			= "CST4_ECALLM"
		EndCallMessyLines[3] 			= "CST4_ECALLM_4"
		
		EndCallMessyRoots[4] 			= "CST4_ECALLM"
		EndCallMessyLines[4] 			= "CST4_ECALLM_5"
		
		EndCallMessyRoots[5] 			= "CST4_ECALLM"
		EndCallMessyLines[5] 			= "CST4_ECALLM_6"
		
		EndCallMessyRoots[6] 			= "CST4_ECALLM"
		EndCallMessyLines[6] 			= "CST4_ECALLM_7"
		
		EndCallMessyRoots[7] 			= "CST4_ECALLM"
		EndCallMessyLines[7] 			= "CST4_ECALLM_8"
		
		EndCallMessyRoots[8] 			= "CST4_ECALLM"
		EndCallMessyLines[8] 			= "CST4_ECALLM_9"
		
		IF ( bActressEjected = FALSE )							//build the dynamic part of the end call messy
			IF ( CarGadgetData.iTimesSpikesDropped > 0 )
				EndCallMessyRoots[9] 	= "CST4_ECALL1"
				EndCallMessyLines[9] 	= "CST4_ECALL1_1"
			ELSE
				EndCallMessyRoots[9] 	= "CST4_ECALL3"
				EndCallMessyLines[9] 	= "CST4_ECALL3_1"
			ENDIF
		ELSE
			IF ( CarGadgetData.iTimesSpikesDropped > 0 )
				EndCallMessyRoots[9] 	= "CST4_ECALL2"
				EndCallMessyLines[9] 	= "CST4_ECALL2_1"
			ELSE
				EndCallMessyRoots[9] 	= "CST4_ECALL4"
				EndCallMessyLines[9] 	= "CST4_ECALL4_1"
			ENDIF
		ENDIF
		
		EndCallMessyRoots[10] 			= "CST4_ECALLE"			//build the end of the end call messy
		EndCallMessyLines[10]			= "CST4_ECALLE_1"
		
		EndCallMessyRoots[11] 			= "CST4_ECALLE"
		EndCallMessyLines[11]			= "CST4_ECALLE_2"
	
	ENDIF
	
ENDPROC

PROC MANAGE_CONVERSATIONS_DURING_ESCAPE_FILM_SET()
	
	IF DOES_ENTITY_EXIST(psActress.PedIndex)										//play actress's fleeing conversations
		IF NOT IS_ENTITY_DEAD(psActress.PedIndex)
			IF 	NOT IS_PED_IN_ANY_VEHICLE(psActress.PedIndex)						//play conversation for actress out of the car
			AND NOT IS_PED_FALLING(psActress.PedIndex)
			AND NOT IS_PED_RAGDOLL(psActress.PedIndex)
			AND HAS_SOUND_FINISHED(iEjectorScreamSoundID)
			AND NOT IS_ENTITY_PLAYING_ANIM(psActress.PedIndex, "misscarsteal4@actress", "eject_girl")
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							//kill any ongoing conversations with Franklin
					
					TEXT_LABEL_23 CurrentRoot 
		
					CurrentRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
					IF NOT IS_STRING_NULL_OR_EMPTY(CurrentRoot)
						IF ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ACHAT1")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ACHAT2")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ACHAT3")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ACHAT4")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY1")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY2")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY3")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY4")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY5")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY6")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY7")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY8")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY9")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_ANNOY10")
						OR ARE_STRINGS_EQUAL(CurrentRoot, "CST4_AWARN")
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
					
				ENDIF
			
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_AFLEE1")						//play conversation for actress getting out of the car
				
					IF ( psActress.fDistanceToTarget < 35.0 )
				
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_AFLEE1", CONV_PRIORITY_MEDIUM)
									psActress.iConversationCounter 	= 0					//reset conversation counter
									psActress.iConversationTimer 	= GET_GAME_TIMER()	//reset conversation timer
									SET_LABEL_AS_TRIGGERED("CST4_AFLEE1", TRUE)
								ENDIF
							ELSE
								IF NOT IS_AMBIENT_SPEECH_PLAYING(psActress.PedIndex)	//maybe play conversation with no subs instead?
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psActress.PedIndex, "CST4_CDAA", "CST4ACTRESS", SPEECH_PARAMS_FORCE_NORMAL)
									psActress.iConversationCounter 	= 0					//reset conversation counter
									psActress.iConversationTimer 	= GET_GAME_TIMER()	//reset conversation timer
									SET_LABEL_AS_TRIGGERED("CST4_AFLEE1", TRUE)
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing ped ambient speech CST4_AFLEE1.")
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ELSE
					
						SET_LABEL_AS_TRIGGERED("CST4_AFLEE1", TRUE)
					
					ENDIF
					
				ELSE
				
					IF IS_PED_FLEEING(psActress.PedIndex)
					
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_AFLEE2")				//play conversation for actrees fleeing on foot
					
							IF HAS_TIME_PASSED(15000, psActress.iConversationTimer)
							
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									
									IF ( psActress.fDistanceToTarget < 30.0 )
								
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											
											IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_AFLEE2", CONV_PRIORITY_MEDIUM)
											
												psActress.iConversationCounter++					//increment conversation counter
												psActress.iConversationTimer = GET_GAME_TIMER()		//reset conversation timer
												
												IF ( psActress.iConversationCounter = 6 )
													SET_LABEL_AS_TRIGGERED("CST4_AFLEE2", TRUE)
												ENDIF

											ENDIF
											
										ELSE
										
											IF NOT IS_AMBIENT_SPEECH_PLAYING(psActress.PedIndex)
											
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psActress.PedIndex, "CST4_DGAA", "CST4ACTRESS", SPEECH_PARAMS_FORCE)
												
												psActress.iConversationCounter++					//increment conversation counter
												psActress.iConversationTimer = GET_GAME_TIMER()		//reset conversation timer
												
												IF ( psActress.iConversationCounter = 6 )
													SET_LABEL_AS_TRIGGERED("CST4_AFLEE2", TRUE)
												ENDIF
												
											ENDIF
										ENDIF
										
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psActress.PedIndex)				//Franklin and actress are in the same car
	
		IF NOT IS_PED_INJURED(psActress.PedIndex)										//play actress screams when bullets are near her
			IF 	NOT IS_AMBIENT_SPEECH_PLAYING(psActress.PedIndex)
			AND	NOT IS_SCRIPTED_SPEECH_PLAYING(psActress.PedIndex)
				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(psActress.PedIndex), 3.0, TRUE)
				OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(psActress.PedIndex), 3.0, FALSE)
					PLAY_PAIN(psActress.PedIndex, AUD_DAMAGE_REASON_SCREAM_SCARED)
				ENDIF
			ENDIF
		ENDIF
		
		IF ( bPlayerInsideFilmStudio = FALSE )											//and they have left the studio
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_AKDNP")								//play conversation for actress asking if this is a kidnapping
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_AKDNP", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("CST4_AKDNP", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_APULL")					//play conversation for actress trying to pull the car over	
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_APULL", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("CST4_APULL", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF

	ENDIF
	
	IF ( bChaseFinished = TRUE )
	
		IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_BUTTON1")
			IF ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psActress.PedIndex)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF ( bSteerBiasActive = FALSE )
							IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_BUTTON1", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("CST4_BUTTON1", TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ENDCALL")
			
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex) 
				IF NOT IS_PED_IN_THIS_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
					IF ( bPlayerInsideFilmStudio = FALSE )

						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							
								IF ( iPhoneCallToDevinDelayTimer = 0 )
									iPhoneCallToDevinDelayTimer = GET_GAME_TIMER()
								ELSE
							
									IF HAS_TIME_PASSED(2000, iPhoneCallToDevinDelayTimer)

										SETUP_LABELS_FOR_END_PHONE_CALL()
										
										IF ( bEndCallMessy = FALSE )	//trigger clean version of end call
										
											IF PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES(9, CST4Conversation, CHAR_MOLLY, "CST4AUD",
																								 EndCallCleanRoots, EndCallCleanLines,
																							 	 CONV_PRIORITY_HIGH)
												SET_LABEL_AS_TRIGGERED("CST4_ENDCALL", TRUE)
											ENDIF
										
										ELSE							//trigger messy version of end call
										
											IF PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES(12, CST4Conversation, CHAR_MOLLY, "CST4AUD",
																								 EndCallMessyRoots, EndCallMessyLines,
																							 	 CONV_PRIORITY_HIGH)
												SET_LABEL_AS_TRIGGERED("CST4_ENDCALL", TRUE)
											ENDIF
										
										ENDIF

									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
//		ELSE
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				bEndPhoneCallFinished = TRUE
//			ENDIF	
		ENDIF
		
	ENDIF

	IF ( bChaseStarted = TRUE )
	
		IF 	NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ASSC")	//play conversation for spotting security
		AND	NOT HAS_LABEL_BEEN_TRIGGERED("CST4_FSSC")
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					
					IF NOT IS_VEHICLE_SEAT_FREE(vsPlayersCar.VehicleIndex, VS_FRONT_RIGHT)	//actress is sitting in the car
					
						IF DOES_ENTITY_EXIST(psActress.PedIndex)
							IF NOT IS_ENTITY_DEAD(psActress.PedIndex)
								IF IS_PED_SITTING_IN_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
									IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ASSC", CONV_PRIORITY_MEDIUM)
										psActress.iConversationTimer = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("CST4_ASSC", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ELSE																	//actress is not sitting in the car
					
						IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_FSSC", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("CST4_FSSC", TRUE)
						ENDIF
						
					ENDIF
					
				ENDIF
			ENDIF
			
		ELSE	//conversation for security being spotted has been triggered

			IF HAS_LABEL_BEEN_TRIGGERED("CAR4_LSEC")	//lose the security objective has been printed
			
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ASSCF")				//play conversation for Franklin responding to  
					IF HAS_LABEL_BEEN_TRIGGERED("CST4_ASSC")				//actress's conversation for spotting the security cars arrive
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF 	NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "CST4_DDAA", "FRANKLIN", SPEECH_PARAMS_FORCE)
								SET_LABEL_AS_TRIGGERED("CST4_ASSCF", TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)	//play spikes related conversations

					IF ( bSpikesAllowed = FALSE )

						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_FSPB")
							
							IF ( bSpikesUnlocked = TRUE )
						
								IF 	NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_FSPB", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_FSPB", TRUE)
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
							
						ENDIF
					
					ELSE

						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_FSPDS")

							IF ( iSuccessfullSpikesHits <> iSecurityCarsImmobilized )
							
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
									IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_FSPDS", CONV_PRIORITY_MEDIUM)
										iSuccessfullSpikesHits = iSecurityCarsImmobilized
										IF ( iSecurityCarsImmobilized = 2 )
											SET_LABEL_AS_TRIGGERED("CST4_FSPDS", TRUE)
										ENDIF
									ENDIF
								
								ELSE
								
									INTERRUPT_CONVERSATION(PLAYER_PED_ID(), "CST4_CFAA", "FRANKLIN")
									iSuccessfullSpikesHits = iSecurityCarsImmobilized
									IF ( iSecurityCarsImmobilized = 2 )
										SET_LABEL_AS_TRIGGERED("CST4_FSPDS", TRUE)
									ENDIF
									
									
									//IF NOT IS_THIS_CONVERSATION_PLAYING("CST4_FSPDS")
									//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									//ENDIF
								
								ENDIF
						
							ENDIF
						ENDIF
					ENDIF

				ENDIF
				
				IF ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), psActress.PedIndex)

					IF ( bEjectorSeatActive = FALSE )

						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_SHOOT")			//play conversation for actress reacting to player shooting from car
							
							IF HAS_TIME_PASSED(10000, psActress.iConversationTimer)

								IF IS_PED_SHOOTING(PLAYER_PED_ID())
								
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
											IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_SHOOT", CONV_PRIORITY_MEDIUM)
											
												iActressShootConversationCounter++
												psActress.iConversationTimer = GET_GAME_TIMER()
												
												IF ( iActressShootConversationCounter = 4 )
													SET_LABEL_AS_TRIGGERED("CST4_SHOOT", TRUE)
												ENDIF
												
											ENDIF
										ELSE
											IF NOT IS_AMBIENT_SPEECH_PLAYING(psActress.PedIndex)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(psActress.PedIndex, "CST4_DCAA", "CST4ACTRESS", SPEECH_PARAMS_FORCE)
												
												iActressShootConversationCounter++
												psActress.iConversationTimer = GET_GAME_TIMER()
												
												IF ( iActressShootConversationCounter = 4 )
													SET_LABEL_AS_TRIGGERED("CST4_SHOOT", TRUE)
												ENDIF
												
											ENDIF
										ENDIF
									ENDIF
								ENDIF					
								
							ENDIF
						ENDIF

						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ACHAT1")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
									IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ACHAT1", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("CST4_ACHAT1", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ACHAT2")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
									IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ACHAT2", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("CST4_ACHAT2", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ACHAT3")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
									IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ACHAT3", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("CST4_ACHAT3", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ACHAT4")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
									IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ACHAT4", CONV_PRIORITY_MEDIUM)
										psActress.iConversationTimer = GET_GAME_TIMER()
										bRandomConversationsAllowed = TRUE
										SET_LABEL_AS_TRIGGERED("CST4_ACHAT4", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF ( bRandomConversationsAllowed = TRUE )
					
						IF ( bEjectorSeatActive = FALSE )
					
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
									
									IF HAS_TIME_PASSED(12500, psActress.iConversationTimer)
									
										IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_SECSHT")
									
											PED_INDEX ClosestPedIndex
											
											ClosestPedIndex = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(psActress.PedIndex, RELGROUPHASH_SECURITY_GUARD)
											
											IF DOES_ENTITY_EXIST(ClosestPedIndex)
												IF NOT IS_ENTITY_DEAD(ClosestPedIndex)
													IF GET_DISTANCE_BETWEEN_PEDS(psActress.PedIndex, ClosestPedIndex) < 10.0
														
														IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_SECSHT", CONV_PRIORITY_MEDIUM)
															
															iActressSecurityConversationCounter++
															psActress.iConversationTimer = GET_GAME_TIMER()
														
															IF ( iActressSecurityConversationCounter = 5 )
																SET_LABEL_AS_TRIGGERED("CST4_SECSHT", TRUE)
															ENDIF
															
														ENDIF
														
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									
											SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
												CASE 0
													IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_STOP", CONV_PRIORITY_MEDIUM)
														psActress.iConversationTimer = GET_GAME_TIMER()
													ENDIF
												BREAK
												CASE 1
													IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_SCREAM", CONV_PRIORITY_MEDIUM)
														psActress.iConversationTimer = GET_GAME_TIMER()
													ENDIF
												BREAK
												CASE 2
													IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ASTOP", CONV_PRIORITY_MEDIUM)
														psActress.iConversationTimer = GET_GAME_TIMER()
													ENDIF
												BREAK
											ENDSWITCH
										ENDIF
										
									ENDIF
									
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY1")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY1", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY1", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY2")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY2", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY2", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY3")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY3", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY3", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY4")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY4", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY4", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY5")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY5", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY5", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY6")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY6", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY6", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY7")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY7", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY7", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY8")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY8", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY8", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY9")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY9", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY9", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_ANNOY10")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_ANNOY10", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_ANNOY10", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_AWARN")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
										IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_AWARN", CONV_PRIORITY_MEDIUM)
											psActress.iConversationTimer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("CST4_AWARN", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF

						ENDIF

					ENDIF
					
				ENDIF
			
			ENDIF

		ENDIF
	
	ELSE	//chase not started
	
		IF 	NOT HAS_LABEL_BEEN_TRIGGERED("CST4_AMSNO")		//play conversation for actress seeing the dazed actor
		AND NOT HAS_LABEL_BEEN_TRIGGERED("CST4_AMSRO")
		
			IF DOES_ENTITY_EXIST(psActor.PedIndex)
				IF NOT IS_ENTITY_DEAD(psActor.PedIndex)
					IF IS_ENTITY_PLAYING_ANIM(psActor.PedIndex, "misscarsteal4@actor", "stumble")
					OR IS_ENTITY_PLAYING_ANIM(psActor.PedIndex, "misscarsteal4@actor", "dazed_idle")
								
						CHECK_PED_LINE_OF_SIGHT_TO_PED(psActress.PedIndex, psActor.PedIndex, psActress.bCanSeeTargetPed, psActress.iTimer, 950)
					
						IF ( psActress.bCanSeeTargetPed = TRUE )
						
							IF ( GET_DISTANCE_BETWEEN_ENTITIES(psActor.PedIndex, psActress.PedIndex) < 30.0)
						
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_AMSNO", CONV_PRIORITY_HIGH)
										SET_LABEL_AS_TRIGGERED("CST4_AMSNO", TRUE)
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF

		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("CST4_AMSRO")		//play conversation for actress reacting to player running over the actor
		
			IF IS_PED_SITTING_IN_THIS_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
		
				IF ( psActress.bCanSeeTargetPed = TRUE )
			
					IF DOES_ENTITY_EXIST(psActor.PedIndex)
						IF IS_ENTITY_DEAD(psActor.PedIndex)
						
							IF ( psActor.eCleanupReason = PED_CLEANUP_KILLED_BY_PLAYER_VEHICLE )
					
								//HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(psActor.PedIndex, PLAYER_PED_ID(), TRUE)
					
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(CST4Conversation, "CST4AUD", "CST4_AMSRO", CONV_PRIORITY_HIGH)
										SET_LABEL_AS_TRIGGERED("CST4_AMSRO", TRUE)
									ENDIF
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								
							ENDIF
							
						ENDIF
					ENDIF
				
				ENDIF
				
			ENDIF

		ENDIF

	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_ESCAPE_FILM_SET_COMPLETED(INT &iStageProgress)

	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	IF ( bPlayerInsideFilmStudio = FALSE )
		SET_IGNORE_NO_GPS_FLAG(FALSE)
	ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	
	KEEP_GARAGE_DOOR_SHUT()
	
	UPDATE_TRIGGERED_LABEL(sLabelDCAR)
	UPDATE_TRIGGERED_LABEL(sLabelGENGETIN)
	UPDATE_TRIGGERED_LABEL(sLabelGENGETBCK)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	
	MANAGE_FILM_CREW(iFilmCrewProgress)
	MANAGE_GRIP_1(psFilmCrew[FCP_GRIP_1], PLAYER_PED_ID())
	MANAGE_DIRECTOR(psFilmCrew[FCP_DIRECTOR], PLAYER_PED_ID())
	
	MANAGE_ACTOR_DURING_ESCAPE_FILM_SET()
	MANAGE_ACTRESS(psActress, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
	
	MANAGE_SECURITY_PEDS_AT_GATES(iGateSecurityProgress)
	MANAGE_SECURITY_PEDS_AT_FILM_SET(iSetSecurityProgress)
	MANAGE_SECURITY_PEDS_IN_VEHICLES(iSecurityChaseProgress)
	
	MANAGE_CONVERSATIONS_DURING_ESCAPE_FILM_SET()
	
	IF 	IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
	AND	IS_PED_IN_THIS_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	ENDIF
	
	IF IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_CAMERA_MAN)
		IF ( bReplayRecordBackFortimeTriggered = FALSE )
			REPLAY_RECORD_BACK_FOR_TIME(3, 0.0, REPLAY_IMPORTANCE_HIGH)
			bReplayRecordBackFortimeTriggered = TRUE
		ENDIF
	ELSE
		bReplayRecordBackFortimeTriggered = FALSE
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
			IF ( bChaseInProgress = FALSE )		//security chase not in progress
			
				IF ( bChaseFinished = FALSE )	//security chase not finished
					
					IS_PLAYER_AT_ANGLED_AREA_IN_VEHICLE(sLocatesData, vInteriorPosition, << 483.30, -1316.18, 27.70 >>, << 477.64 , -1319.01, 31.20 >>, 5.0, TRUE, vsPlayersCar.VehicleIndex, sLabelDCAR, sLabelGENGETIN, sLabelGENGETBCK, TRUE)
				
				ELSE							//security chase finished, security lost or killed
				
					IF ( bEjectorSeatAllowed = FALSE )
						IF HAS_LABEL_BEEN_TRIGGERED("CST4_BUTTON1")
							bEjectorSeatAllowed = TRUE
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ejector seat allowed.")
							#ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_IN_THIS_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
			
						IF HAS_LABEL_BEEN_TRIGGERED("CST4_BUTTON1")
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_EGIRL")
								IF 	( bEjectorSeatActive = FALSE )
								AND	NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									PRINT_GOD_TEXT_ADVANCED("CAR4_EGIRL", DEFAULT_GOD_TEXT_TIME, TRUE)
								ENDIF
							ENDIF
						ENDIF
				
					ELSE
						
						IF ( bMusicEventDropTriggered = FALSE )
							IF HAS_LABEL_BEEN_TRIGGERED("CST4_ENDCALL")
								IF IS_CALLING_CONTACT(CHAR_MOLLY)
								OR IS_CONTACT_IN_CONNECTED_CALL(CHAR_MOLLY)
									TRIGGER_MUSIC_EVENT("CAR3_DROP")										//trigger music event
									bMusicEventDropTriggered = TRUE											//if not triggered via ejector seat
								ENDIF
							ENDIF
						ENDIF
					
						IF 	IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)				//if player is in the car
						AND NOT IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_CAMERA_MAN)								//and spikes camera is not active
							
							IF HAS_LABEL_BEEN_TRIGGERED("CST4_ENDCALL")
								IF IS_CALLING_CONTACT(CHAR_MOLLY)
								OR IS_CONTACT_IN_CONNECTED_CALL(CHAR_MOLLY)
								
									IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > 0
				
										STOP_AUDIO_SCENE("CAR_3_ESCAPE_SECURITY")
				
										RETURN TRUE
									ENDIF
									
								ENDIF
								
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									
									STOP_AUDIO_SCENE("CAR_3_ESCAPE_SECURITY")
			
									RETURN TRUE
								ENDIF	
								
							ENDIF
							
						ENDIF
						
					ENDIF
				
				ENDIF

			ELSE								//security chase in progress
			
				IF ( bSpikesAllowed = FALSE )
					IF ( iSpikesDelayTimer = 0 )
					
						IF IS_PED_IN_THIS_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
							iSpikesDelayTime = 22000
						ELSE
							iSpikesDelayTime = 16000
						ENDIF
					
						iSpikesDelayTimer = GET_GAME_TIMER()
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting spikes delay timer with delay time ", iSpikesDelayTime, "ms.")
						#ENDIF
					ELSE
						IF HAS_TIME_PASSED(iSpikesDelayTime, iSpikesDelayTimer)
							bSpikesUnlocked	= TRUE
							IF HAS_LABEL_BEEN_TRIGGERED("CST4_FSPB")
								bSpikesAllowed = TRUE
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Spikes allowed.")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
				OR DOES_BLIP_EXIST(sLocatesData.LocationBlip)	//clear locates header blip and text when chase starts
		
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)

					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
					ENDIF
				
				ENDIF
							
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_LSEC")
				
					IF HAS_LABEL_BEEN_TRIGGERED("CST4_ASSC")
					OR HAS_LABEL_BEEN_TRIGGERED("CST4_FSSC")
							
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
							PRINT_GOD_TEXT_ADVANCED("CAR4_LSEC", DEFAULT_GOD_TEXT_TIME, TRUE)
						ENDIF

					ENDIF
					
				ENDIF

			ENDIF
			
			IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)				//handle target vehicle blip display
				IF 	NOT DOES_BLIP_EXIST(vsPlayersCar.BlipIndex)
				AND NOT DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
					vsPlayersCar.BlipIndex = CREATE_BLIP_FOR_VEHICLE(vsPlayersCar.VehicleIndex)
				ENDIF
				IF DOES_BLIP_EXIST(vsPlayersCar.BlipIndex)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabelGENGETBCK) AND NOT IS_STRING_NULL_OR_EMPTY(sLabelGENGETBCK)
						IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							CLEAR_PRINTS()
							PRINT_GOD_TEXT_ADVANCED(sLabelGENGETBCK, DEFAULT_GOD_TEXT_TIME, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE	//player is in the car
				IF DOES_BLIP_EXIST(vsPlayersCar.BlipIndex)
					IF IS_THIS_PRINT_BEING_DISPLAYED(sLabelGENGETBCK)
						CLEAR_PRINTS()
					ENDIF
					REMOVE_BLIP(vsPlayersCar.BlipIndex)
				ENDIF
			ENDIF

			IF 	IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)					//handle help text display
			AND NOT IS_CELLPHONE_CAMERA_IN_USE()

				IF NOT IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_CAMERA_MAN)

					IF ( bSpikesAllowed = TRUE )
						IF ( iSecurityCarsImmobilized = 0)
							IF ( bChaseInProgress = TRUE )
								IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_HCARS")
									PRINT_HELP_ADVANCED("CAR4_HCARS", TRUE, TRUE)					//handle spikes help text
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF ( bEjectorSeatAllowed = TRUE )
						IF ( bEjectorSeatActive = FALSE )
							IF IS_PED_IN_THIS_VEHICLE(psActress.PedIndex, vsPlayersCar.VehicleIndex)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR4_HCARE")
									PRINT_HELP_ADVANCED("CAR4_HCARE", TRUE, TRUE)					//handle ejector seat help text
								ELSE
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR4_HCARE")
										SET_LABEL_AS_TRIGGERED("CAR4_HCARE", FALSE)
									ENDIF
								ENDIF
							ELSE
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR4_HCARE")
									CLEAR_HELP()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
			
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR4_HCARE")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR4_HCARS")
					CLEAR_HELP()
				ENDIF
		
			ENDIF
			
			IF NOT IS_CELLPHONE_CAMERA_IN_USE()
		
				IF ( bSpikesAllowed = TRUE )
					UPDATE_JB700_SPIKES_DROPPING(vsPlayersCar.VehicleIndex, CarGadgetData)
					UPDATE_JB700_SPIKES_COLLISIONS(vsPlayersCar.VehicleIndex, CarGadgetData)
					IF NOT IS_CALLING_CONTACT(CHAR_MOLLY) AND NOT IS_CONTACT_IN_CONNECTED_CALL(CHAR_MOLLY)
						UPDATE_JB700_SPIKES_CUTSCENE(vsPlayersCar.VehicleIndex, CarGadgetData, "CAR_3_TYRE_BURST_CAM")						
					ELSE
						RESET_JB700_SPIKES_CUTSCENE_TIMERS(CarGadgetData)
					ENDIF
					UPDATE_JB700_SPIKE_HIT_COUNTERS(CarGadgetData, vsSecurityCars)
					UPDATE_JB700_PED_DRIVE_TASK(CarGadgetData, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex, vsPlayersCar.bAutoDriveActive, vsPlayersCar.iTimer)
				ENDIF
	
			ENDIF
					
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_DELIVER_CAR_COMPLETED(INT &iStageProgress)

	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)

	MANAGE_DEVIN(iLeadInProgress)
	MANAGE_ACTRESS(psActress, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
	MANAGE_DIRECTOR(psFilmCrew[FCP_DIRECTOR], PLAYER_PED_ID())
	MANAGE_GRIP_1(psFilmCrew[FCP_GRIP_1], PLAYER_PED_ID())
	
	IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
		IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_3_GO_TO_GARAGE")
			START_AUDIO_SCENE("CAR_3_GO_TO_GARAGE")
		ENDIF
	ENDIF

	IF NOT DOES_ENTITY_EXIST(psDevin.PedIndex)
	OR NOT bInteriorLoadSceneLoaded
		KEEP_GARAGE_DOOR_SHUT()
	ENDIF

	IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID)
		SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID , TRUE)
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInteriorPosition) < 50.0
	
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
	
			NEW_LOAD_SCENE_START_SPHERE(vInteriorPosition, 5.25, NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR)
			
			bInteriorLoadSceneLoaded = FALSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting new load scene at coords ", vInteriorPosition, ".")
			#ENDIF
			
		ELSE
		
			IF IS_NEW_LOAD_SCENE_LOADED()
			
				//NEW_LOAD_SCENE_STOP()
			
				bInteriorLoadSceneLoaded = TRUE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": New load scene at coords ", vInteriorPosition, " loaded.")
				#ENDIF
			
			ENDIF
			
		ENDIF
	
	ELSE
	
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInteriorPosition) > 75.0
	
			IF IS_NEW_LOAD_SCENE_ACTIVE()
			
				NEW_LOAD_SCENE_STOP()
				
				bInteriorLoadSceneLoaded = FALSE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping new load scene at coords ", vInteriorPosition, ".")
				#ENDIF
			
			ENDIF
			
		ENDIF
	
	ENDIF
	
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_PLAYER_CHANGING_CLOTHES()

		IF HAS_THIS_CUTSCENE_LOADED("car_4_ext")
		OR HAS_CUTSCENE_LOADED()
		OR IS_CUTSCENE_ACTIVE()
			REMOVE_CUTSCENE()
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
			#ENDIF
		ENDIF
		                        
	ELSE

		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInteriorPosition) < 15.0 //DEFAULT_CUTSCENE_LOAD_DIST

			REQUEST_CUTSCENE("car_4_ext")

			//fix for streaming of the gates, see B*1843796
			REQUEST_MODEL(PROP_SC1_06_GATE_L)
			REQUEST_MODEL(PROP_SC1_06_GATE_R)
			
			REMOVE_ANIM_DICT("move_m@generic")
			REMOVE_ANIM_DICT("move_m@intimidation@cop@unarmed")
			REMOVE_ANIM_DICT("reaction@intimidation@cop@unarmed")
			
			CLEANUP_OBJECT(osPropLamp.ObjectIndex, FALSE)
			CLEANUP_PED_GROUP(psSetSecurity, FALSE, FALSE)
			CLEANUP_PED_GROUP(psActorSecurity, FALSE, FALSE)
			CLEANUP_PED_GROUP(psMeltdownSecurity, FALSE, FALSE)
			CLEANUP_PED_GROUP(psNorthGateSecurity, FALSE, FALSE)
			CLEANUP_PED_GROUP(psSouthGateSecurity, FALSE, FALSE)
		
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())			//request variations for Franklin
				
				IF DOES_ENTITY_EXIST(psDevin.PedIndex)												//request variations for Devin
				
					IF NOT IS_ENTITY_DEAD(psDevin.PedIndex)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Devin", psDevin.PedIndex)	//from ped if Devin exists
					ENDIF
					
				ELSE																				//and if Devin does not exist
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_HEAD, 		0, 0) //(head)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_HAIR, 		0, 0) //(hair)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_TORSO, 		0, 0) //(uppr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_LEG, 		0, 0) //(lowr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_HAND,		0, 0) //(hand)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_FEET, 		0, 0) //(feet)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_TEETH, 		0, 0) //(feet)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_SPECIAL, 	2, 0) //(accs)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_SPECIAL2, 	0, 0) //(task)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_DECL, 		0, 0) //(decl)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_JBIB, 		0, 0) //(jbib)
					
				ENDIF
			ENDIF
			
			IF ( bMusicEventDeliverTriggered = FALSE )
				IF TRIGGER_MUSIC_EVENT("CAR3_DELIVER")
					bMusicEventDeliverTriggered = TRUE
				ENDIF
			ENDIF
			
		ELSE
		
			IF HAS_THIS_CUTSCENE_LOADED("car_4_ext")
			OR HAS_CUTSCENE_LOADED()
			OR IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SC1_06_GATE_L)
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SC1_06_GATE_R)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
				#ENDIF
			ENDIF
		
		ENDIF
		
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
			
			UPDATE_JB700_SPIKES_DROPPING(vsPlayersCar.VehicleIndex, CarGadgetData)
			UPDATE_JB700_SPIKES_COLLISIONS(vsPlayersCar.VehicleIndex, CarGadgetData)
			IF NOT IS_CALLING_CONTACT(CHAR_MOLLY) AND NOT IS_CONTACT_IN_CONNECTED_CALL(CHAR_MOLLY)
				UPDATE_JB700_SPIKES_CUTSCENE(vsPlayersCar.VehicleIndex, CarGadgetData, "CAR_3_TYRE_BURST_CAM")						
			ELSE
				RESET_JB700_SPIKES_CUTSCENE_TIMERS(CarGadgetData)
			ENDIF
			UPDATE_JB700_SPIKE_HIT_COUNTERS(CarGadgetData, vsSecurityCars)
			UPDATE_JB700_PED_DRIVE_TASK(CarGadgetData, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex, vsPlayersCar.bAutoDriveActive, vsPlayersCar.iTimer)
					
			//if the player reaches this stage while not being in car and the blip was displayed
			//make sure the car's blip is cleaned up
			IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
					IF DOES_BLIP_EXIST(vsPlayersCar.BlipIndex)
						REMOVE_BLIP(vsPlayersCar.BlipIndex)
					ENDIF
				ENDIF
			ENDIF
			
			IF ( bFilmStudioAmbientPedsBlocked = TRUE )
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
		
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vFilmSetCenterPosition) > 500.0
		
						CLEAR_PED_NON_CREATION_AREA()
						SET_SCENARIO_GROUP_ENABLED("MOVIE_STUDIO_SECURITY", TRUE)								

						IF ( ScenarioBlockingArea1 != NULL )
							REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlockingArea1)
						ENDIF
						
						IF ( ScenarioBlockingArea2 != NULL )
							REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlockingArea2)
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removed film studio scenario blocking areas and non ped creation areas.")
						#ENDIF
						
						bFilmStudioAmbientPedsBlocked = FALSE
					ENDIF
				ENDIF
			ENDIF

			IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, vInteriorPosition, DEFAULT_LOCATES_SIZE_VECTOR(), TRUE, vsPlayersCar.VehicleIndex, sLabelDCAR, sLabelGENGETIN, sLabelGENGETBCK, TRUE)
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF 	( IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 483.30, -1316.18, 27.70 >>, << 477.64 , -1319.01, 31.20 >>, 5.0)
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vInteriorPosition) < CLAMP(GET_ENTITY_SPEED(PLAYER_PED_ID()), 1.25, 3.5) )
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<474.036987,-1316.961304,28.106997>>, <<481.344360,-1311.562744,31.202971>>, 6.0)
				
					TRIGGER_MUSIC_EVENT("CAR3_STOP_TRACK")
				
					CLEANUP_JB700_SPIKES(CarGadgetData, FALSE)
				
					CLEANUP_OBJECT(osPropChair.ObjectIndex, TRUE)
					CLEANUP_OBJECT(osTrailerDoor.ObjectIndex, TRUE)
					CLEANUP_OBJECT(osPropClipboard.ObjectIndex, TRUE)
					
					IF IS_PHONE_ONSCREEN()
						HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					ENDIF
					
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), psDevin.PedIndex, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
				
					bLeadInTriggered = TRUE
					
					iStageProgress++
				ENDIF
			ENDIF

		BREAK
		
		CASE 1
		
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		
			IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vsPlayersCar.VehicleIndex, 2.5, 1, 0.75)
			
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
				iStageProgress++

			ENDIF
		
		BREAK
		
		CASE 2
		
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_PLAYER_VEHICLE_CONTROLS_THIS_FRAME()
			
			BOOL bProgress
			bProgress = FALSE
			IF DOES_ENTITY_EXIST(psDevin.PedIndex)
				IF NOT IS_ENTITY_DEAD(psDevin.PedIndex)
					IF IS_ENTITY_PLAYING_ANIM(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_action_devin")
						IF GET_ENTITY_ANIM_CURRENT_TIME(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_action_devin") > 0.875
						
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								bProgress = TRUE
							ENDIF
						
						ENDIF
					ELSE
						IF NOT IS_ENTITY_PLAYING_ANIM(psDevin.PedIndex, "misscarsteal4leadinoutcar_4_ext", "leadin_loop_devin")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								bProgress = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bProgress
				CLEANUP_PED(psActor)
				REMOVE_ANIM_DICT("dead")
				REMOVE_ANIM_DICT("misscarsteal4@actor")
				REMOVE_ANIM_DICT("misscarsteal4leadinoutcar_4_ext")
			
				IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
					SET_VEHICLE_ENGINE_ON(vsPlayersCar.VehicleIndex, FALSE, FALSE)
				ENDIF
				
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				
				SET_FORCE_FOOTSTEP_UPDATE(psDevin.PedIndex, FALSE)
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
			
				RETURN TRUE			
			ENDIF
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_END_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
			
		CASE 0
      
          	IF HAS_REQUESTED_CUTSCENE_LOADED("car_4_ext")

				IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
				
					IF IS_VEHICLE_DOOR_DAMAGED(vsPlayersCar.VehicleIndex, SC_DOOR_FRONT_LEFT)
						SET_VEHICLE_DOOR_CONTROL(vsPlayersCar.VehicleIndex, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
					ENDIF
				
					SET_VEHICLE_ENGINE_ON(vsPlayersCar.VehicleIndex, FALSE, FALSE)
					REGISTER_ENTITY_FOR_CUTSCENE(vsPlayersCar.VehicleIndex, "movie_car", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF DOES_ENTITY_EXIST(psDevin.PedIndex)
					IF NOT IS_ENTITY_DEAD(psDevin.PedIndex)
						SET_FORCE_FOOTSTEP_UPDATE(psDevin.PedIndex, FALSE)
						REGISTER_ENTITY_FOR_CUTSCENE(psDevin.PedIndex, "Devin", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
				
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_HEAD, 		0, 0) //(head)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_HAIR, 		0, 0) //(hair)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_TORSO, 		0, 0) //(uppr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_LEG, 		0, 0) //(lowr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_HAND,		0, 0) //(hand)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_FEET, 		0, 0) //(feet)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_TEETH, 		0, 0) //(feet)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_SPECIAL, 	2, 0) //(accs)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_SPECIAL2, 	0, 0) //(task)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_DECL, 		0, 0) //(decl)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Devin", PED_COMP_JBIB, 		0, 0) //(jbib)
				ENDIF
				
         	ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				STOP_AUDIO_SCENE("CAR_3_GO_TO_GARAGE")
				
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SC1_06_GATE_L)
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SC1_06_GATE_R)
				
				CLEANUP_JB700_SPIKES(CarGadgetData, TRUE)
				CLEAR_AREA_OF_OBJECTS(<< 484.687, -1315.512, 28.202 >>, 4.5)
				
				CLEAR_AREA_OF_PEDS(sMissionPosition.vPosition, 100.0)
				CLEAR_AREA_OF_VEHICLES(sMissionPosition.vPosition, 100.0)
				
				STOP_FIRE_IN_RANGE(sMissionPosition.vPosition, 20.0)
				REMOVE_DECALS_IN_RANGE(sMissionPosition.vPosition, 20.0)
				CLEAR_AREA_OF_PROJECTILES(sMissionPosition.vPosition, 20.0)
				REMOVE_PARTICLE_FX_IN_RANGE(sMissionPosition.vPosition, 20.0)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF IS_CUTSCENE_PLAYING()
				KEEP_GARAGE_DOOR_SHUT()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				//should fix pop at end of cutscene
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			
				IF ( bCutsceneSkipped = TRUE )
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_GARAGE_FRONT))
			            DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_GARAGE_FRONT), -1.0, FALSE, FALSE)
			            DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_GARAGE_FRONT), DOORSTATE_FORCE_OPEN_THIS_FRAME, FALSE, TRUE)
					ENDIF					
				ENDIF
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
					WARP_PED(PLAYER_PED_ID(), << 493.4449, -1313.0740, 28.2614 >>, 300.9095, FALSE, TRUE, FALSE)
					
					//fix for B*2025342, put Franklin in motion at the end of the cutscene
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
				ENDIF				
			ENDIF
		
			IF HAS_CUTSCENE_FINISHED()

				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)

				REPLAY_STOP_EVENT()
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF

				RETURN TRUE
				
			ELSE
			
				//if cutscene is skipped, fade out and remain cutscene on faded out screen
				IF ( bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						bCutsceneSkipped = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mocap cutscene was skipped by the player.")
						#ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

#IF IS_DEBUG_BUILD

	FUNC BOOL IS_MISSION_STAGE_EJECTOR_TEST_COMPLETED(INT &iStageProgress)
	
		SWITCH iStageProgress
		
			CASE 0
			
				MANAGE_EJECTOR_SEAT(psActress.PedIndex, PLAYER_PED_ID(), vsPlayersCar.VehicleIndex, iEjectorSeatProgress,
									iEjectorSeatTimer, bEjectorSeatActive, bActressEjected)
			
			BREAK
		
		ENDSWITCH
	
		RETURN FALSE
	
	ENDFUNC

#ENDIF



//|============================= END MISSION STAGES FUNCTIONS ============================|

//|=================================== MAIN SCRIPT LOOP ==================================|

SCRIPT

	SET_MISSION_FLAG(TRUE)
	
	//check for death or arrest
    IF HAS_FORCE_CLEANUP_OCCURRED() 
	
		CLEAR_HELP()
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		
		TRIGGER_MUSIC_EVENT("CAR3_MISSION_FAIL")
	
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		MISSION_CLEANUP()
		TERMINATE_THIS_THREAD()
    ENDIF
	
	INIT_PC_SCRIPTED_CONTROLS( "Carsteal4_spycar" )
	
	SET_INTERIOR_CAPPED(INTERIOR_V_CHOPSHOP, FALSE)
	
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_MISSION_STAGE_MENU()
		CREATE_DEBUG_WIDGETS()
	#ENDIF
	
	IF IS_REPLAY_IN_PROGRESS()
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission replay is in progress.")
		#ENDIF
	
		SET_MISSION_STAGE_FOR_REPLAY(eMissionStage, Get_Replay_Mid_Mission_Stage())
		
		bReplayFlag = TRUE
	
		//handle shitskip message
		IF ( g_bShitSkipAccepted = TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip accepted by the player.")
			#ENDIF
		
			eMissionStage = GET_NEXT_MISSION_STAGE(eMissionStage)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip changed mission stage to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
			#ENDIF
		
		ELSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip not triggered.")
			#ENDIF
		
		ENDIF

	ELSE
	
		RESET_GLOBAL_BIT_SET()
	
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_ACTOR), "GET TO ACTOR", FALSE)
	ENDIF
	
	WHILE TRUE
		
		//calculate distance from player ped to target vehicle
		//then use the variable rather than running another distance check
		IF DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
			IF IS_VEHICLE_DRIVEABLE(vsPlayersCar.VehicleIndex)
				fDistanceFromPlayerToTargetVehicle = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
			ENDIF
		ENDIF
		
		bPlayerInsideFilmStudio = IS_ENTITY_IN_FILM_STUDIO(PLAYER_PED_ID())
		
		//check each frame if mission is failed due to fail conditions
		//check only if mission stage loading has finished
		IF ( bLoadingFinishedFlag = TRUE )
			RUN_FAIL_CHECKS(eMissionStage, eMissionFail)
			RUN_INTERIOR_PINNING_AND_UNPINNING_CHECK(vInteriorPosition, 150.0)
		ENDIF
		
		//run this check to stop security peds in the film set
		//from firing random bullet when the PRF_BlockWeaponFire is not set for a frame
		BLOCK_WEAPON_FIRE_FOR_AIMING_SECURITY_GROUP(psSetSecurity)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_DeepInside")

		SWITCH eMissionStage
		
			CASE MISSION_STAGE_GET_TO_ACTOR
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
			
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2175707
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFinishedFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GET_TO_ACTOR_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsPlayersCar.VehicleIndex)
							eMissionStage = MISSION_STAGE_ESCAPE_FILM_SET
						ELSE
							eMissionStage = MISSION_STAGE_CUTSCENE_TRAILER
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_TRAILER
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.1)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.1)
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFinishedFlag, bSkipFlag, bReplayFlag)		
					IF IS_MISSION_STAGE_CUTSCENE_TRAILER_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_GET_TO_CAR
					ENDIF
				ENDIF
			BREAK

			CASE MISSION_STAGE_GET_TO_CAR
			
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.1)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.1)
			
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2175707
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFinishedFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GET_TO_CAR_COMPLETED(iMissionStageProgress) 
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_ESCAPE_FILM_SET
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_ESCAPE_FILM_SET
			
				IF ( bChaseFinished = FALSE )
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.4)
				ENDIF
			
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2175707
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFinishedFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_ESCAPE_FILM_SET_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_DELIVER_CAR
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_DELIVER_CAR			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFinishedFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_DELIVER_CAR_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_END
					ENDIF
				ENDIF
			BREAK

			CASE MISSION_STAGE_CUTSCENE_END
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFinishedFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_END_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						//lock the main garage door after the end cutscene
						KEEP_GARAGE_DOOR_SHUT()
						SET_DOOR_STATE(DOORNAME_CARSTEAL_GARAGE_S, DOORSTATE_LOCKED)
						eMissionStage = MISSION_STAGE_PASSED
					ENDIF
				ENDIF
			BREAK
			
			#IF IS_DEBUG_BUILD
				CASE MISSION_STAGE_EJECTOR_TEST
					IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFinishedFlag, bSkipFlag, bReplayFlag)
						IF IS_MISSION_STAGE_EJECTOR_TEST_COMPLETED(iMissionStageProgress)
							RESET_MISSION_FLAGS()
							eMissionStage = MISSION_STAGE_PASSED
						ENDIF
					ENDIF
				BREAK
			#ENDIF

			CASE MISSION_STAGE_PASSED
			
				MISSION_FLOW_MISSION_PASSED()
				
				MISSION_CLEANUP()
				
				TERMINATE_THIS_THREAD()
				
			BREAK
			
			CASE MISSION_STAGE_FAILED

				SET_MISSION_FAILED_WITH_REASON(eMissionFail)   
				
				CLEAR_HELP()
				CLEAR_PRINTS()
				KILL_ANY_CONVERSATION()
				
				TRIGGER_MUSIC_EVENT("CAR3_MISSION_FAIL")
				
				WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
					WAIT(0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for GET_MISSION_FLOW_SAFE_TO_CLEANUP() to return TRUE.")
					#ENDIF
				ENDWHILE
				
				IF ( bPlayerInsideFilmStudio = TRUE )
					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-1021.8750, -475.0034, 36.0445>>, 82.2636)
				ENDIF
				
				IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_TUXEDO)
					SET_PLAYER_PED_OUTFIT_TO_TUXEDO(FALSE)
				ENDIF

				MISSION_CLEANUP()
				
				TERMINATE_THIS_THREAD()
				
			BREAK
			
		ENDSWITCH
		
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), CS3_DAMAGE)
		
		IF DOES_ENTITY_EXIST(vsPlayersCar.VehicleIndex)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vsPlayersCar.VehicleIndex, CS3_NO_SCRATCH)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vsPlayersCar.VehicleIndex, CS3_FASTEST_SPEED)
		ENDIF
		
		#IF IS_DEBUG_BUILD																			//handle debug functionality
			RUN_DEBUG_PASS_AND_FAIL_CHECK(eMissionStage, eMissionFail)								//handle key_f and key_s presses
			RUN_DEBUG_MISSION_STAGE_MENU(iReturnStage, eMissionStage, bSkipFlag, bStageResetFlag)	//handle z-skip menu
			RUN_DEBUG_SKIPS(eMissionStage, bSkipFlag, bStageResetFlag)								//handle j-skip and p-skip
			RUN_DEBUG_WIDGETS()																		//handle rag widgets
			RUN_DEBUG_INFORMATION_DISPLAY()															//handle drawing of debug information		
		#ENDIF

		WAIT(0)
	
    ENDWHILE

ENDSCRIPT
