
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "Flow_Mission_Data_Public.sch"
USING "Commands_streaming.sch"
USING "commands_clock.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_object.sch"
USING "selector_public.sch"
USING "commands_fire.sch"
USING "commands_event.sch"
using "commands_misc.sch"
USING "commands_graphics.sch"
using "dialogue_public.sch"
//USING "cutscene_builder.sch"
USING "script_blips.sch"
USING "replay_public.sch"
USING "locates_public.sch"
#IF IS_DEBUG_BUILD
USING "scripted_cam_editor_public.sch"
#ENDIF
USING "mission_stat_public.sch"
using "help_at_location.sch"
USING "CompletionPercentage_public.sch"
USING "CompletionPercentage_public.sch"
using "clearmissionarea.sch"
USING "commands_recording.sch"
USING "player_ped_public.sch"

CONST_INT ENABLE_ENHANCED_HUD_FEATURES 1
CONST_INT MAX_SCANNABLE_PEDS 10
CONST_INT MAX_HELIHUD_COORD_COUNT 5
CONST_INT MAX_MESSAGES 1
CONST_INT MAX_PED_NAMES 11
CONST_INT MAX_BRAIN_TRACKERS 1

USING "helicopterhud.sch"
//USING "beam_effect.sch"
USING "clearmissionarea.sch"
USING "timelapse.sch"
USING "shop_public.sch"
USING "spline_cam_edit.sch"
USING "taxi_functions.sch"
USING "push_in_public.sch"
USING "script_misc.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 13

	
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      

#ENDIF

//WIDGET_GROUP_ID widWidgets

ENUM CARSTEAL2_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE,
	SWITCH_CAM_START_SPLINE1,
	SWITCH_CAM_PLAYING_SPLINE1,
	SWITCH_CAM_WAITING_FOR_PLAYER_CONTROL,
	SWITCH_CAM_RETURN_TO_GAMEPLAY
ENDENUM
CARSTEAL2_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE
SWITCH_CAM_STRUCT scsCarsteal2

//first person push-ins

PUSH_IN_DATA s_cs2_pushin

INTERIOR_INSTANCE_INDEX intHangar

int iConvoAttempts
int iConvoLastAttmpetTime

bool bDoReplayToHere
BOOL bTrevorHeadTrackStarted = FALSE
FLOAT fTrevorHeadTrackPhase = 0.0

FLOAT fFranklinAimingHeading = 145.4926

BOOL bFranklinAnimStarted = FALSE
FLOAT fFranklinAnimStartPhase = 0.37
float fFranklinAnimTalkPhase = 0.4

BOOL bPlayerControlStarted = FALSE

INT iWhooshSound,iWhooshSoundMid
BOOL bPlayerinFirstPerson
BOOL bWhooshStarted = FALSE
BOOL bWhooshFinished = FALSE
FLOAT fWhooshStartPhase = 0.278269
float fWhooshMidAudioPhase = 0.345958
FLOAT fWhooshStopPhase = 0.95


bool bMidWhooshPlayed = FALSE


CONST_INT MAX_PED_COUNT 18
CONST_INT MAX_VEH_COUNT 14
CONST_INT MAX_ASSETS 44
CONST_INT MAX_EVENTS 20
CONST_INT MAX_ACTIONS 21

CONST_INT MAX_DIALOGUE 25

string sVehicleRecordingLibrary
int iStoreStatBit

enum enumFailState
	MISSION_STATE_SKIP,
	MISSION_STATE_FAILED,
	MISSION_STATE_DEATH_ARREST,
	MISSION_STATE_PASSED
endenum

enum enumAction
	action_null
endenum

ENUM enumMissionStage
	STAGE_STARTUP,						//0
	STAGE_TREVOR_GET_CHOPPER,			//1
	STAGE_FRANKLIN_AWAITS_TREVOR,		//2
	STAGE_LEARN_TO_SCAN,				//3
	STAGE_APPROACH_SCAN_AREA_ONE,		//4
	STAGE_APPROACH_SCAN_AREA_THREE,		//5
	STAGE_CHASE_BEGINS,					//6
	STAGE_CARPARK,						//7
	STAGE_SCAN_CARPARK_PEDS,			//8
	STAGE_TAKE_ZTYPE,					//9
	STAGE_DRIVE_VEHICLE_TO_OBJECTIVE,	//10
	STAGE_APPROACH_AIRPORT_GATES,		//11
	STAGE_FINAL_CUT,					//12
	STAGE_LEAVE_AIRPORT,				//13
	STAGE_GAME_OVER,					//14
	STAGE_CLEANUP,						//15
	STAGE_NULL							//16
ENDENUM

enumMissionStage missionProgress

enum andOrEnum
	cFORCEtrue,
	cIGNORE,
	cIF,
	cIFnot,
	cAND,
	cAndNOT,
	cOR,
	cWasTrueNowFalse,
	cORbracket
endenum

bool conditionsTrue
bool bracketOpen

//Activities
enum enumFails
	FAIL_NULL,
	FAIL_PLAYER_HAS_WEAPON_DRAWN_IN_POL_DEPT,
	FAIL_HAS_WANTED_LEVEL_IN_POLICE_DEPT,
	FAIL_COP_DIES,
	FAIL_PLAYER_SHOOTING_NEAR_POLICE_DEPT,
	FAIL_PLAYER_CREATED_DISTURBANCE_NEAR_POL_DEPT,
	FAIL_COPS_IN_COMBAT,
	FAIL_CHOPPER_DESTROYED,
	FAIL_PLAYER_WANDERS_AWAY_FROM_POLICE_DEPT,
	FAIL_PILOT_FLEW_AWAY,
	FAIL_ZTYPE_DAMAGED,
	FAIL_PLAYER_LEAVES_ZTYPE,
	FAIL_CHAD_ESCAPES_WITH_ZTYPE,
	FAIL_ZTYPE_STUCK,
	FAIL_DEVIN_KILLED,
	FAIL_DEVIN_SCARED_OFF,
	FAIL_COPS_AT_DEVIN,
	FAIL_FLEW_TOO_FAR_AWAY,
	fail_devins_plane_destroyed,
	fail_attacked_devins_entourage
endenum

enumFails failReason
int failDelay

// Conditions setup
ENUM enumconditions
	COND_NULL,						
	COND_FAILING,
	COND_COP_WALKING_TO_STAIRS,		
	COND_CUTSCENE_ENDED,				
	COND_PLAYER_ENTERED_RECEPTION,		
	COND_COP_IN_CORRIDOR,			
	COND_PLAYER_ANYWHERE_IN_STAIRWALL,
	COND_PLAYER_LAGS_BEHIND_OR_WANDERS,
	COND_PLAYER_UP_STAIRWELL,
	COND_PLAYER_IN_CORRIDOR,
	COND_PLAYER_OUT_OF_BOUNDS,
	COND_PLAYER_NEARBY,
	COND_COPS_ALERTED_CONDITION,
	COND_COP_IN_STAIRWELL,
	COND_COP_FINISHED_LEADING_PLAYER,
	COND_CAN_SPAWN_CHOPPER,
	COND_PLAYER_NOT_FOLLOWING_COP,
	COND_DIA_COP_LEADING,
	COND_PLAYER_ON_ROOF,
	COND_PLAYER_TRYS_TO_ENTER_CHOPPER,
	COND_PLAYER_IN_LOCKER_ROOM,
	COND_PLAYER_RUNS_AHEAD,
	COND_TREVOR_TELLS_PILOT_TO_TAKE_OFF,
	COND_TREVOR_TELLS_PILOT_TO_ACTIVATE_SCANNER,
	COND_READY_TO_TURN_ON_SCANNER,
	COND_DIA_BACK_TO_FRONT_DESK,
	COND_DIA_GET_IN,
	COND_PLAYER_IN_CHOPPER,
	COND_COP_LEADING_PLAYER,
	COND_PLAYER_SPOTTED_BY_PILOT, 
	
	COND_START_FRANKLIN,
	COND_PLAYER_LEAVING_TREVOR,
	COND_END_FRANKLIN,
	
	//learn to scan
	COND_SCAN_STAGE_START,
	COND_STARTED_AS_TREVOR,
	COND_FAR_ENOUGH_FROM_FRANKLIN_TO_PLAY_CONVO,
	COND_LEARNT_TO_SCAN,
	COND_BOOTING_UP_LINE_PLAYED,
	COND_FRANKLIN_SCANNED,
	COND_DIA_SCANNED_CONVO_FINISHED,
	COND_DIA_SCAN_MY_BUDDY,
	COND_TREV_SAID_TO_FIND_FRANKLIN,
	COND_TOLD_TO_SCAN_FRANKLIN,
	COND_OUTSIDE_SAFE_ZONE,
	COND_FLEW_IN_TO_FAIL_ZONE,
	COND_APPROACHING_FRANKLIN,
	COND_SCAN_STAGE_END,
	
	//scan area 1
	COND_START_SCAN_AREA_1,
	COND_POSTAL_BEING_OBSERVED,
	COND_PERVERT_BEING_OBSERVED,
	COND_AREA1_PED_JUST_SCANNED,
	COND_SCANNED_PED_WAS_A_WOMAN,
	COND_DIA_COME_IN_FRANKLIN_PLAYED,
	COND_CHOPPER_IN_VIEWING_RANGE_OF_AREA1,
	COND_START_SCAN_AREA_3,
	
	COND_LISTENING_IN_ON_DIALOGUE,
		
	COND_END_SCAN_AREA_1,
	
	//scan area 3	
	COND_AREA3_PED_JUST_SCANNED,
	COND_PROSIE_BEING_OBSERVED,
	COND_CHAD_BEING_OBSERVED,
	COND_CHAD_WAS_SCANNED,
	COND_CHAD_HIDDEN,
	COND_CHAD_ON_SCREEN,
	COND_CHAD_OFFSCREEN_FOR_4_SECONDS,
	COND_CHAD_BY_GARAGES,
	COND_CHAD_IN_APARTMENT_LOCATE,
	COND_GARAGE_DOOR_OPENING,
	COND_END_SCAN_AREA_3,
	
	//CAR PARK CONDITONS
	COND_CARPARK_STARTS, //21
	COND_CHOPPER_DESCENDING,
	COND_FRANKLIN_STOPPED_HIS_CAR,
	COND_FRANKLIN_HAS_STOPPED_RUNNING,
	COND_A_LITTLE_AFTER_EXITS_CLEAR_DIA,
	COND_DIA_TAKE_US_DOWN,
	COND_DIA_THERMAL_VISION,	
	COND_DIA_CAN_YOU_SEE_FRANKLIN,	
	COND_DIA_ANY_OTHER_HEAT_SOURCES,
	COND_DIA_LOOK_FOR_MORE_HEAT_PLAYED,
	COND_DIA_FRANKLIN_SEES_PISSER,
	COND_DIA_FRANKLIN_SEES_FIXING_MAN,
	COND_DIA_FRANKLIN_SEES_CHAD,
	COND_DIA_TREVOR_SEE_FRANKLIN_ENDED,
	COND_CHOPPER_AT_CAR_PARK_LEVEL,
	COND_THERMAL_TURNED_ON,
	COND_FRANKLIN_OBSERVED_WITH_THERMAL,
	COND_SEE_WANKER,
	COND_SEE_FIXING_MAN,
	COND_SEE_CHAD,
	COND_SEE_LEANING_MAN,
	COND_SEE_PHONE_CAR_MAN,	
	COND_FRANKLIN_AT_PHONE_MAN,
	COND_FRANKLIN_AT_CHAD,
	COND_THERMAL_READY,
	COND_DIA_TREVOR_SEES_CHAD_ENDED,
	COND_SWITCH_BEGUN,
	COND_BLOCK_LISTENING,
	COND_FRANKLIN_SAW_WRONG_GUY,
	COND_DIA_FROM_FRANKLIN_SEEING_CHAD,
	COND_FRANKLIN_RUNS_TO_CAR,
	COND_WAITING_FOR_NEXT_TARGET,
	COND_NEXT_TARGET_FOUND,
	COND_INVESTIGATING_NEXT_TARGET,	
	COND_CAR_PARK_ENDS, 
	
	//ztype drive conditions
	COND_ZTYPE_STARTS,
	COND_CHAD_INTERRUPTED,
	COND_CHAD_CAN_BACK_OFF,
	COND_GUN_AIMED_AT_CHAD,
	COND_CHAD_DAMAGED_BY_PLAYER,
	COND_CHAD_CAN_RUN,
	COND_PLAYER_AT_SECURITY,
	COND_PLAYER_200M_FROM_SECURITY,
	COND_CHAD_PLAYER_IN_GARAGE,
	COND_CHAD_RAGDOLL,
	COND_PLAYER_GOT_IN_CAR,
	COND_PROCEED_TO_HANGAR_CONDITIONS_MET,
	COND_PHONE_CALL_MADE,
	COND_NO_DIALOGUE_FOR_TWO_SECONDS,
	COND_CHAD_DEAD,
	COND_CHAD_PLAYER_100m_FROM_CAR_PARK,
	COND_PLAYER_30m_from_garage,
	COND_PLAYER_200m_from_garage,
	COND_PLAYER_300m_from_garage,
	COND_PLAYER_DEFAULT_LOAD_CUT_RANGE_FROM_HANGAR,
	COND_WANTED,
	COND_HIT_BLIP,
	COND_PLAYER_IN_ZTYPE,
	COND_DRIVE_TO_INSTRUCTIONS_GIVEN,
	COND_CHAD_ESCAPED_IN_ZTYPE,
	COND_PLAYER_SHOOTING_NEAR_CHAD,
	COND_DEVIN_WALKING_IN_TO_ZTYPE,
	COND_CHOPPER_DAMAGED_BY_PLAYER,
	COND_DAMAGED_ZTYPE,
	COND_PLAYER_7M_FROM_CHAD,
	COND_ZTYPE_IN_HANGAR,
	COND_ZTYPE_IN_CAR_STOP_RANGE,

	COND_ZTYPE_IS_STOPPED,
	COND_STOP_FOR_DEVIN,
	COND_STOP_FOR_CORRECT,
	COND_ZTYPE_ENDS,
	
	COND_EXIT_AIRPORT_START,
	COND_PLAY_IN_ZTYPE,
	COND_EXIT_AIRPORT_END

ENDENUM

//ACTION SETUP
ENUM enumActions
	ACT_NULL,
	
	
	
	ACT_SPAWN_COPS,
	ACT_INTRO_CUTSCENE,
	ACT_LOOK_AT_PLAYER_ENTRY,
	ACT_POINT_TO_STAITRS,
	ACT_WALK_TO_STAIRS,
	ACT_COPS_ALERTED,
	ACT_SPAWN_CHOPPER,
	ACT_PED_AT_VENDING_MACHINE_LEAVES,
	ACT_COP_RETURNS_TO_FRONT_DESK,
	ACT_PLAYER_GETS_IN_CHOPPER,
	ACT_PILOT_FLIES_OFF,
	ACT_PILOT_LOOKS_AT_TREVOR,
	ACT_COP_WAITS_FOR_PLAYER,
	ACT_LOCK_GUN_ROOM,
	ACT_DO_TREVOR_STAND,
	
	//get chopper as Franklin
	ACT_CLEAR_SWITCH_INSTRUCTIONS,
	ACT_LOAD_STAND_ANIM,
	ACT_BLOCK_CAM_MODE_CHANGE,

	
	//learn to scan
	ACT_TURN_ON_SCANNER,
	ACT_MUSIC_SCANNER_ON,
	ACT_FRANKLIN_LOOKS_AT_CHOPPER,	
	ACT_FRANKLIN_REACTS_TO_TREVOR_CONVERSATION,
	ACT_FRAKNLIN_GETS_IN_CAR,
	ACT_LOAD_PATH_NODES,
	ACT_CREATE_PLAYER_CAR,
	ACT_CHANGE_FRANKLIN_LOD_LEVEL_UP,
	ACT_CHANGE_FRANKLIN_LOD_LEVEL_DOWN,
	
	//scan area 1
	ACT_STOP_AUDIO_CAR_2_SCAN_FRANKLIN,
	ACT_SCANNING_AUDIO_SCENE,
	ACT_STOP_CAR_2_SCAN_THE_SUSPECTS,
	ACT_DELETE_FRANKLIN,
	ACT_GARAGE_OPENING_SOUND,
	
	//scan area 3
	ACT_RELEASE_AREA_1_ASSETS,
	ACT_SPAWN_ZTYPE,
	ACT_DOG_BARKS,
	ACT_TARGET_LOST_SOUND,
	
	//chase stage
	ACT_block_peds_on_chase_route,
	ACT_AUDIO_SCENE_TRANSITION,
	action_load_audio_scene,
	act_update_target_car_audio_level,
	act_remove_trees,

	
	//Car Park stage
	ACT_CHOPPER_CONTROL,
	ACT_CAR_HAS_PISS,
	ACT_BONNET_SEX,
	ACT_CAR_FIXER,
	ACT_ON_PHONE_IN_CAR,
	ACT_LEANING_ON_CAR,
	ACT_FRANKLIN_RUNS_TO_ROOF,
	ACT_FRANKLIN_WAVES_AT_CHOPPER_CAMERA,
	ACT_FRANKLIN_RUNS_TO_BONNET_CAR,
	ACT_FRANKLIN_RUNS_TO_PISSER_CAR,
	ACT_FRANKLIN_RUNS_TO_CHAD_CAR,
	ACT_FRANKLIN_RUNS_TO_LEANING_MAN,
	ACT_FRANKLIN_RUNS_TO_PHONE_MAN,
	ACT_FRANKLIN_REACTS_TO_WRONG_CAR,
	ACT_CHAD_LOOKS_ABOUT,
	ACT_CHAD_EXITS_CAR,
	ACT_MAKE_FRANKLIN_WALK_ON_SWITCH,
	ACT_PREP_SWITCH,
	ACT_DO_SWITCH,
	//take ztype
	ACT_PREP_STAGE,
	ACT_START_AUDIO_CAR_2_DRIVE_BACK_TO_GARAGE,
	ACT_STOP_AUDIO_CAR_2_STEAL_THE_CAR,
	ACT_CHAD_RUNS_OFF,
	ACT_CHAD_TRIES_TO_CREEP_AWAY,
	ACT_REACTS_TO_PLAYER_IN_CAR,
	ACT_LOAD_END_CUTSCENE,
	ACT_CHAD_CALLS_COPS,
	ACT_FLY_TO_GARAGE,
	ACT_REMOVE_CAR_PARK_ASSETS,
	ACT_SET_UP_DEVIN_LEADIN,
	ACT_SPAWN_SECURITY_GUARD,
	ACT_OPEN_GATES,
	ACT_CONTROL_CHOPPER,
	ACT_STOP_ZTYPE_IN_HANGAR,
	ACT_RETURN_CONTROL_TO_PLAYER,
	ACT_STAT_CS2_SCANMAN,
	
	ACT_DEVIN_DRIVES_OFF_ZTYPE,
	ACT_MAKE_PLAYER_WALK,
	ACT_OPEN_EXIT_GATE,
	ACT_TURN_PLANE_ENGINE_ON
ENDENUM
// Set up instructions
ENUM enumInstructions
	INS_NULL,
	INS_GET_TO_RECEPTION,
	INS_FOLLOW_COP,
	INS_GET_TO_ROOF,
	INS_GET_IN_CHOPPER,
	
	INS_ABANDON_TREVOR,
	
	//learn to scan
	INS_TEACH_TO_SCAN,
	INS_SCAN_FRANKLIN,
	INS_SCAN_COMPLETE,
	INS_GET_BACK_TO_FRANKLIN,
	
	//scan area 3
	INS_FOLLOW_CHAD,
	INS_SHOW_HIDDEN_HELP,
		
	// ==== Car Park Instructions ====
	INS_TURN_ON_THERMAL_VISION,
	INS_ZOOM_IN_ON_FRANKLIN,
	INS_LOOK_FOR_A_HEAT_SOURCES,
	INS_LOOK_FOR_ANOTHER_HEAT_SOURCES,
	INS_SWITCH_TO_FRANKLIN,
	
	//====== take ztype =======
	INS_GET_IN_THE_ZTYPE,
	INS_DRIVE_TO_AIRPORT,
	INS_DRIVE_TO_DESTINATION,
	INS_WARNING_WANTED_LEVEL,
	INS_DRIVE_IN_TO_GARAGE,
	
	//exit airport
	INS_EXIT_AIRPORT,
	INS_RETURN_ZTYPE
ENDENUM

enum enumDialogue
	DIA_NULL,
	DIA_CAN_I_HELP,
	DIA_INTEROGATE_COP,
	DIA_COP_WALK_CHAT,
	DIA_COME_ON,
	DIA_COP_RETURNS_TO_DESK,
	DIA_PLAYER_GETS_IN_CHOPPER,
	DIA_PLAYER_LAGS_OR_LEAVES,
	DIA_PLAYER_THREAT_IN_POL_DEPT,
	DIA_PILOT_SEES_PLAYER,
	DIA_PLAYER_ENTERS_LOCKER_ROOM,
	DIA_WRONG_WAY,
	DIA_RESTART_CONV,
	
	dia_player_doesnt_switch,
	
	//learn to scan
	DIA_BOOTING_UP,
	DIA_SCAN_BUDDY_NEARBY,
	DIA_CONV_ON_WAY_TO_FRANKLIN,
	DIA_SCAN_MY_BUDDY,
	DIA_FRANKLIN_SCANNED,
	DIA_GOING_TO_FIND_BUDDY,
	DIA_ACTOR_LINE_PLAYS,
	DIA_FLYING_AWAY,
	DIA_WAITING_TO_SCAN,
	
	//scan area 1
	DIA_TREVOR_COMMENTS_ON_POSTAL,
	DIA_TREVOR_COMMENTS_ON_PERVERT,
	DIA_TREVOR_COMMENTS_ON_PROSTITUTE,
	DIA_TREVOR_COMMENTS_ON_CHADGIRL,
	DIA_TREVOR_MAKES_SCAN_COMPLETE_COMMENT,
	
	//scan area 3
	DIA_SEEN_CHAD,
	DIA_CHAD_HIDDEN,
	DIA_CHAD_WALKING_THROUGH_APARTMENTS,
	DIA_CHAD_OFF_SCREEN,
	DIA_FRANKLIN_FIND_HIM,
	DIA_FOUND_CHAD,
	DIA_TREVOR_BANTER_DURING_CHAD_WALK,
	DIA_CHAD_AT_GARAGE,
	
	//car park dialogue
	DIA_TAKE_US_DOWN,
	DIA_TREVOR_SCARED,
	DIA_EXITS_CLEAR,
	DIA_THERMAL_VISION,
	DIA_TREVOR_REACTION_TO_THERMAL_VISION_ON,
	DIA_FRANKLIN_ASKS_IF_PLAYER_CAN_SEE_HIM,
	DIA_TREVOR_SEE_FRANKLIN,
	DIA_ANY_OTHER_HEAT_SOURCES,
	DIA_TREVOR_SEES_PISSER,
	DIA_TREVOR_SEES_FIXING_MAN,
	DIA_TREVOR_SEES_CHAD,
	DIA_TREVOR_SEES_LEANING_MAN,
	DIA_TREVOR_SEES_PHONE_CAR_MAN,
	DIA_FRANKLIN_SEES_PISSER,
	DIA_FRANKLIN_SEES_FIXING_MAN,
	DIA_FRANKLIN_SEES_ZTYPE,
	DIA_FRANKLIN_SEES_LEANING_MAN,
	DIA_FRANKLIN_SEES_PHONE_MAN,
	DIA_OVERHEAR_FUCKERS_WITH_SCANNER,
	DIA_OVERHEAR_CAR_FIXER_WITH_SCANNER,
	DIA_OVERHEAR_PISSER_WITH_SCANNER,
	DIA_OVERHEAR_CHAD_WITH_SCANNER,
	DIA_OVERHEAR_CAR_LEANING_GUY,
	DIA_OVERHEAR_CAR_ON_PHONE,
	DIA_TREVOR_RANTS_IN_SWITCH,
	DIA_PLAYER_SHOOTING_CHOPPER,
	DIA_DRIVING_BACK,
	DIA_CRASH_ZTYPE,
	DIA_WHERES_THE_CAR,
	
	//collect car
	DIA_BEG_TO_LET_GO,
	DIA_DONT_TAKE_CAR,
	DIA_FLEE_PANIC,
	DIA_FLEE_PANIC_NOT_HIT,
	DIA_PLAYER_GOT_IN_CAR,
	DIA_DRIVING_AWAY,
	DIA_WARNING_COPS,
	DIA_KILLED_CHAD,	
	DIA_CALL_DEVIN,
	DIA_OPEN_GATES,
	DIA_FRANKLIN_THREATENS_CHAD,
	DIA_PLAYER_RUNS_OFF
ENDENUM

//conversation types
ENUM enum_conv_types
	CONVTYPE_NULL,	
	CONVTYPE_UNIMPORTANT,
	CONVTYPE_CHOPPER_CAM,
	CONVTYPE_BANTER,
	CONVTYPE_GAMEPLAY
ENDENUM

enum_conv_types currentConvType

//events
enum enumEvents
	EVENTS_REMOVE_INTRO_CUT_ASSETS,			//0
	EVENTS_CHOPPER_LANDS,					//1
	EVENTS_LOAD_FIRST_LOCATION_PEDS,		//2
	EVENTS_CLEAR_FIRST_LOCATION,			//3
	EVENTS_LOAD_SECOND_LOCATION_PEDS,		//4
	EVENTS_CLEAR_SECOND_LOCATION,			//5
	EVENTS_LOAD_FINAL_LOCATION_PEDS,		//6
	EVENTS_CLEAR_FINAL_LOCATION,			//7
	EVENTS_REMOVE_FRANKLIN,					//8
	EVENTS_CHOPPER_BLADES,					//9
	events_control_playback,				//10
	events_control_golf_playback,			//11
	EVENTS_GOLF_COURSE,						//12
	EVENTS_HOVER_CHOPPER,					//13
	EVENTS_FILL_CARPARK,					//14
	EVENTS_CHOPPER_CARPARK,					//15
	EVENTS_CONTROL_INFRARED,				//16
	EVENTS_TREVOR_DRIVE_TO_CARPARK,			//17
	EVENTS_CONVERSATION_ON_ROUTE,			//18
	EVENTS_CHASE_CHATTER,					//19
	EVENTS_CONTROL_GOLF_CONVERSATION,		//20
	EVENTS_CARPARK_CONVERSATION,			//21
	EVENTS_KEEP_CHAD_SAFE,					//22
	EVENTS_REMOVE_CARPARK_ASSETS,						//23
	EVENTS_ENABLE_CHOPPER_BEAM,					//24
	EVENTS_REMOVE_CARPARK_CARS,				//25	
	EVENTS_CHECK_COLLISION,					//26
	EVENTS_SPEED_UP_CHOPPER,				//27
	EVENTS_PRINT_CHADBUST_MESSAGE,			//28
	EVENTS_TRY_HEIST_VEHICLE,				//29
	events_disable_franklin_car,			//30
	EVENTS_FRANKLIN_ARRIVE,					//31
	EVENTS_DRIVE_OFF,						//32
	EVENTS_REPOSITION_HEIST_CAR,			//33
	events_chopper_push,					//34
	EVENTS_FRANKLIN_INVESTIGATES_PEDS,		//35	
	events_look_for_another_heat_source,	//36
	EVENTS_GETCHAD_INST,					//37
	EVENTS_KILL_CONVERSATION_WHEN_LEAVING_CAR,	//38
	events_blow_up_cars,					//39
	EVENTS_CHOPPER_TRACKS_FRANKLIN,			//40
	EVENTS_CHOPPER_HOVER,					//41
	EVENTS_DEAL_WITH_PLAYER_CAR,			//42
	events_change_playback_speed,			//43
	EVENTS_OPEN_GARAGE,						//44
	EVENTS_CHOPPER_AT_START,				//45
	EVENTS_CHAD_COWER,						//46
	EVENTS_FAIL_CHASE,						//47
	EVENTS_LOAD_COLLISION_AROUND_CHAD,		//48
	EVENTS_STREAM_ALLEY,					//49
	events_scene_mugging,					//50
	EVENTS_SCENE_BURY_BODY,					//51
	EVENTS_SCENE_PERVERT,					//52
	events_scene_chadGirl,					//53
	events_scene_pimp,						//54
	EVENTS_SCENE_CHADJACK,					//55
	EVENTS_SCENE_CHAD_EXITS_CAR,			//56
	EVENTS_SCENE_CAR_FUCK,					//57
	events_scene_car_wank,					//58	
	events_freeze_car,						//59
	events_witness_mugging,					//60
	events_witness_pervert,					//61
	events_witness_pimp,					//62
	events_witness_chadgirl,				//63	
	events_show_goto_instructions,			//64
	EVENTS_REMOVE_SCAN_BOXES,				//65
	events_hover_over_first_location,		//66
	events_hover_over_second_location,		//67
	events_pimp_achievement,				//68
	EVENTS_UPSIDE_DOWN,						//69
	events_set_player_ids,					//71
	events_create_franklin_and_car,			//72
	events_Franklin_enters_carpark,			//73
	events_switch_to_franklin,				//74
	events_listen_help,						//75
	EVENTS_QUAD_BLOCKING,					//76
	EVENTS_CHAD_DIALOGUE,					//77
	EVENTS_REMOVE_RANDOM_CARS,				//78
	EVENTS_ADDITIONAL_ZOOM_HELP,			//79
	events_waypoint_through_police_dept,	//80
	EVENTS_STEALTH_REMOVE_TRAFFIC,			//81
	events_is_chad_hidden,
	events_create_chopper_pilot,
	events_spawn_ztype,
	events_bum_on_chad_walk, 
	events_dog_walks_by,
	events_load_cutscene,
	events_garage_beep,
	events_scan_music_change,
	events_chad_car_brief_hint,
	EVENTS_MISSION_FAILS,					//82	
	EVENTS_NULL								//83
ENDENUM

// =============================================== ASSETS

enum eMissionAssets
	ASSET_Police_DEPT,
	ASSET_CHOPPER,
	ASSET_PILOT_AT_START,
	ASSET_AREA_1_ASSETS,
	ASSET_AREA_2_ASSETS,
	ASSET_CHAD,
	ASSET_ZYPE,
	ASSET_CARPARK,
	ASSET_DEVIN,
	ASSET_CHASE_AUDIO
endenum

enum eAssetStage
	ASSETS_STAGE_STARTUP,	
	ASSETS_STAGE_POLICE_STATION,
	ASSETS_STAGE_AFTER_LEADIN,
	ASSETS_STAGE_IN_CHOPPER,
	ASSETS_STAGE_PREP_AREA_1,
	ASSETS_STAGE_PREP_AREA_2,
	ASSETS_STAGE_RELEASE_AREA_1,
	ASSETS_STAGE_LOAD_ZTYPE,
	ASSETS_STAGE_RELEASE_AREA_2,
	ASSETS_STAGE_PREP_CAR_PARK,
	ASSETS_STAGE_END_CAR_CHASE,
	ASSETS_STAGE_RELEASE_CAR_PARK,
	ASSETS_STAGE_FINAL_CUTSCENE,
	ASSETS_STAGE_GAME_OVER
endenum



struct structAssets
	eAssetStage loadFor
	eAssetStage releaseOn
	bool loaded
endstruct

structAssets ASSETS[count_of(eMissionAssets)]
eAssetStage currentAssetStage
bool bAssetChageInProgress
vector devinCoord = << -993.41, -3023.94, 12.94 >>
float fDevinHeading = -20.61
vector carDropoffCoord = <<-990.14, -3016.53, 12.94>>
vector airportBarrier = <<-967.8815, -2796.7524, 12.9648>>

// =============================================== END ASSETS

/*
enum enumAction
	action_none,
	action_at_front_desk,	
	action_guarding_door,
	action_lead_player,
	action_sitting_in_chopper,
	action_player_threat
endenum
*/
//special switch cases that it's best to leave fixed rather than have numerixc
CONST_INT CASE_CHECK_PLAYER_ON_ROOF 10000
CONST_INT CASE_SWITCH_BEGUN 10001
CONST_INT CASE_SWITCH_IN_PROGRESS 10002

USING "BoltsCommonCommands.sch"
INT iReturnStage
//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:		KB							Date:		02/06/2010	│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│																				│
//│							Car Steal Heist - Setup 5 							│
//│																				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

structHelicopterHUDVars HUD
helihud_marker destMarker[5]

//car recs
vector vStartPos,vStartRot,vTargetBlendPos,vTargetBlendRot
float fBlendTime

//CONSTS
bool bWaveformDisplayingQuiet,bWaveformDisplayingLoud
bool bplayVehRecs
bool playAsTrevor
//peds
//0 = player
CONST_INT ped_trevor 		1
CONST_INT ped_pilot 		2
CONST_INT ped_chad			3
CONST_INT ped_franklin		4
CONST_INT ped_courier		5
CONST_INT ped_mugger		6
CONST_INT ped_husband		7
CONST_INT ped_pervert		8
CONST_INT ped_wife			9
CONST_INT ped_chadGirl		10
CONST_INT ped_pimp			11
CONST_INT ped_punter		12
CONST_INT ped_hooker		13
CONST_INT ped_bum			14
CONST_INT ped_dog_walk		15
CONST_INT ped_dog			16

CONST_INT ped_wrong_wanker		5
CONST_INT ped_wrong_fucker		6
CONST_INT ped_wrong_phone		7
CONST_INT ped_wrong_lean		8

CONST_INT ped_killer 5
CONST_INT ped_body 6

CONST_INT ped_cop1 5
CONST_INT ped_cop2 6
CONST_INT ped_cop3 7
CONST_INT ped_cop4 8

CONST_INT ped_civ1 9
CONST_INT ped_civ2 10
CONST_INT ped_civ3 11
CONST_INT ped_civ4 12
CONST_INT ped_civ5 13

CONST_INT ped_busDriver 6

ped_index pedDevin
ped_index pedList[16]
	int iStoredLastReplay									

//============================== sync scenes ===================================
int iDevinSyncSCene,iDevinSyncSCeneB=-1
CONST_INT MAX_TIMELINES 33

enum enumSceneName
	SCENENAME_POSTAL,
	SCENENAME_ROOF,
	SCENENAME_PIMP,
	SCENENAME_CHAD
endenum

enum sceneEventEnum	
	SCENE_INIT,
	SCENE_TRIGGER,
	SCENE_LISTEN_LINE,
	SCENE_ACTION,
	SCENE_LOOPED_DIALOGUE,
	SCENE_DEBUG
endenum

struct sceneTimelineStruct
	sceneEventEnum event
	int triggerTime
endstruct

struct viewedScenedStruct
	int syncSceneID[2]
	vector pos
	vector rot
	sceneTimelineStruct timeLine[MAX_TIMELINES]
	int triggerTime = 0
	int nextAction
	int dialogueLine
	int nextEvent
	int listenDia
ENDSTRUCT

viewedScenedStruct viewScene[4]

INTERIOR_INSTANCE_INDEX int_garage

INT sceneId[8] //7 = blend out chadgirl
OBJECT_INDEX prop[3]
object_index fakeGarageDoor,realGarageDoor

int scene_chad_reacts_to_franklin_jack
int scene_franklin_tries_to_jack_chad
int scene_chad_leaves_car
int scene_car_fuck, scene_car_phone, scene_car_lean

bool bPauseListenerDialogue
//int scene_handjob

//========================= stream vol id
STREAMVOL_ID sVol


//====================== bezier ==================
//=====chopper beam=======

struct bezier
	float fTime
	vector vcontrol
	vector vPos
	vector Rot
endstruct

// ============================= TOD ==========================
// TIME OF DAY VARIABLES
structTimelapse cs2Timelapse
int iTrevBlocker
SCENARIO_BLOCKING_INDEX copScenarioBlocker
// ===========================================help timer for learning to use chopper cam
int helpTimer
int timeAllowingProgress
int iMovingStickCount
bool bMovingStick

//============================Natural motion grab
#IF IS_DEBUG_BUILD
vector vgrab1 = << -0, 2.057, 0.218 >>
vector vgrab2 = << 0.308, 2.064, 0.2655 >>
vector vgrab3 = << -0.344, 2.072, 0.2755 >>
vector vgrab4 = << -0.0585, 2.059, 0.2705 >>
vector vgrab5 = << 0.935, -1.59, 0.358 >>
vector vgrab6 = << 0.945, -1.862, 0.378 >>
vector vgrab7 = << 0.9335, -1.3155, 0.248 >>
vector vgrab8 = << 0.9515, -1.5915, 0.388 >>

	bool bShowGrab
	bool bPrintGrab
	bool bSkipSwitch = false
#ENDIF
//scripted cut


#if IS_DEBUG_BUILD model_names model_franklin #endif
MODEL_NAMES model_pilot

MODEL_NAMES model_chad

//locates navigation
LOCATES_HEADER_DATA sLocatesData

//Enums
// ======= bools =======


//bool bBeamOn
//bool bAimAtCar
bool infraRed=FALSE
#IF IS_DEBUG_BUILD
bool bFrameTimer
bool bScreenConditions
bool bscreenDialogue
bool bScreenActions,bScreenActionsB
bool bScreenInstructions

bool bResetScene,bSceneExport
int iDebugScene,iLastDebugScene
int iDebugEvent,iLastDebugEvent
int iDebugTimer,iLastDebugTimer
#endif
//chase dialogue

struct chaseDialogueStruct
	int askTime
	int replyByTime
	int TrevorResponseTime
	int chadRecordingToCheck
	string conv_root
	int conv_line
	bool askForDirections
	bool mustBeSeen
	bool FailForNotSeeing
endstruct

chaseDialogueStruct chaseDialogue[17]


//int iTrack

//chopper breeze

//float fBreezeMultiplier
float fPlaybackSpeed
//Vehicle Recording Struct
//navdata nv

CONST_INT MAX_CAR_RECS 72
STRUCT StructRecordingData
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VEHICLE_INDEX vehicleID	
	int startPlaybackTime
	int Recording=-1
	bool playing
	bool ended
	bool isHeistCar
	bool isChaseCar
ENDSTRUCT

StructRecordingData recordingData[MAX_CAR_RECS]

//vehicles
VEHICLE_INDEX carParkVehicles[35]


int carParkCounter

// SCENARIO VECTORS
//vector vStartPosition

//SCENARIO_BLOCKING_INDEX scenario_block_spawn_area
//recordings


//locations


//cutscenes

//chopper bez timer

float fOpenRatio

//blips
BLIP_INDEX blipTarget	
	
//cams


// hotswap
//SELECTOR_PED_STRUCT sSelectorPeds
//SELECTOR_CAM_STRUCT sCamDetails


//timers
//int itimer

//BITS
//int bitStoreEntry //stores bits for ensuring no random dialogue entries are repeated.

//INTS
//int pedBeingScanned
int pedScanned
int scanPedToRemove
int pedRemoveTimer

//int iCamTimer

//ped variables

//PED_INDEX pedTrevor

//vehicle variables

//car park variables
//int icarParkLevel = 1
int StaticSoundID = -1
int listenDialogue = -1

CONST_INT vehChopper 0
CONST_INT vehFranklin 1
CONST_INT vehHeist 2
CONST_INT player_vehicle 3
CONST_INT vehDeliveryVan 4
CONST_INT veh_postie 5
CONST_INT veh_camper 6
CONST_INT veh_vanhide 7
CONST_INT veh_wrong1 8
CONST_INT veh_wrong2 9
CONST_INT vehJet 5
CONST_INT veh_phoneChat 10
CONST_INT veh_lean 11

CONST_INT vehSec1 4
CONST_INT vehSec2 12
CONST_INT vehSec3 13

//chopper beam
//float fBeamSpread = 4.0
//float fBeamDrawRange = 250.0
//float fInBeamTime

INTERIOR_INSTANCE_INDEX int_police

#IF IS_DEBUG_BUILD

//widget
WIDGET_GROUP_ID carStealWidget

bool bSkipChopper


bool bWidget_skipChadOnWaypoint

int iMissionProg
//int iSkipPos
//debug

// vehicle removal vars
//chopper sound volumes


#ENDIF
int audio_level_flag
vehicle_index DelCar 
int iFoundCars


TEXT_LABEL_23 restartLine,restartRoot,crashRestartLine
TEXT_LABEL_23 bumRestartLine

func bool IS_CONV_ROOT_PLAYING(string conv_root_to_check)
	TEXT_LABEL_23 txt
	txt = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

	IF ARE_STRINGS_EQUAL(txt,conv_root_to_check)
	OR ARE_STRINGS_EQUAL(restartRoot,conv_root_to_check)
		RETURN TRUE		
	ENDIF
	RETURN FALSE
endfunc

PROC SAFEWAIT(int waittime=0)
	#if IS_DEBUG_BUILD
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		TERMINATE_THIS_THREAD()
	ENDIF
	#endif
	WAIT(waittime)
ENDPROC

// PC CONTROL FOR HELI CAMERA
BOOL bPCCOntrolsSetup = FALSE

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("CARSTEAL2 HELICAM")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

func bool SC2_PLAYER_IN_FIRST_PERSON()
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
	OR GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_VEHICLE ) = CAM_VIEW_MODE_FIRST_PERSON
		RETURN TRUE
	ENDIF
	RETURN FALSE
endfunc

func string GET_INSTRUCTION_STRING(enumInstructions thisIn)
	SWITCH thisIn
		CASE	INS_NULL	RETURN "INS_NULL" BREAK
		CASE	INS_GET_TO_RECEPTION	RETURN "INS_GET_TO_RECEPTION" BREAK
		CASE	INS_FOLLOW_COP	RETURN "INS_FOLLOW_COP" BREAK
		CASE	INS_GET_TO_ROOF	RETURN "INS_GET_TO_ROOF" BREAK
		CASE	INS_GET_IN_CHOPPER	RETURN "INS_GET_IN_CHOPPER" BREAK
		CASE	INS_ABANDON_TREVOR	RETURN "INS_ABANDON_TREVOR" BREAK
		CASE	INS_TEACH_TO_SCAN	RETURN "INS_TEACH_TO_SCAN" BREAK
		CASE	INS_SCAN_FRANKLIN	RETURN "INS_SCAN_FRANKLIN" BREAK
		CASE	INS_SCAN_COMPLETE	RETURN "INS_SCAN_COMPLETE" BREAK
		CASE	INS_GET_BACK_TO_FRANKLIN	RETURN "INS_GET_BACK_TO_FRANKLIN" BREAK
		CASE	INS_FOLLOW_CHAD	RETURN "INS_FOLLOW_CHAD" BREAK
		CASE	INS_SHOW_HIDDEN_HELP	RETURN "INS_SHOW_HIDDEN_HELP" BREAK
		CASE	INS_TURN_ON_THERMAL_VISION	RETURN "INS_TURN_ON_THERMAL_VISION" BREAK
		CASE	INS_ZOOM_IN_ON_FRANKLIN	RETURN "INS_ZOOM_IN_ON_FRANKLIN" BREAK
		CASE	INS_LOOK_FOR_A_HEAT_SOURCES	RETURN "INS_LOOK_FOR_A_HEAT_SOURCES" BREAK
		CASE	INS_LOOK_FOR_ANOTHER_HEAT_SOURCES	RETURN "INS_LOOK_FOR_ANOTHER_HEAT_SOURCES" BREAK
		CASE	INS_SWITCH_TO_FRANKLIN	RETURN "INS_SWITCH_TO_FRANKLIN" BREAK
		CASE	INS_GET_IN_THE_ZTYPE	RETURN "INS_GET_IN_THE_ZTYPE" BREAK
		CASE	INS_DRIVE_TO_AIRPORT	RETURN "INS_DRIVE_TO_AIRPORT" BREAK
		CASE	INS_DRIVE_TO_DESTINATION	RETURN "INS_DRIVE_TO_DESTINATION" BREAK
		CASE	INS_WARNING_WANTED_LEVEL	RETURN "INS_WARNING_WANTED_LEVEL" BREAK
		CASE	INS_DRIVE_IN_TO_GARAGE	RETURN "INS_DRIVE_IN_TO_GARAGE" BREAK
		CASE	INS_EXIT_AIRPORT	RETURN "INS_EXIT_AIRPORT" BREAK
		CASE	INS_RETURN_ZTYPE	RETURN "INS_RETURN_ZTYPE" BREAK
	ENDSWITCH

	RETURN "NO DATA"
ENDFUNC

#IF IS_DEBUG_BUILD

PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS(WIDGET_GROUP_ID widParentGroup)
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")
	
	//INT i
	//TEXT_LABEL_15 txtLabel
	
	SET_CURRENT_WIDGET_GROUP(widParentGroup)
	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables -")
		
		
		ADD_WIDGET_FLOAT_SLIDER("Trevor Start Head Track Phase", fTrevorHeadTrackPhase, 0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Franklin Start Anim Phase", fFranklinAnimStartPhase, 0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Franklin Aim Heading", fFranklinAimingHeading, -360.0, 360.0, 0.1)
		
		START_WIDGET_GROUP("Whoosh Sound")
			ADD_WIDGET_FLOAT_SLIDER("Whoosh Start Phase", fWhooshStartPhase, 0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Whoosh Stop Phase", fWhooshStopPhase, 0, 1.0, 0.01)
		STOP_WIDGET_GROUP()

	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(widParentGroup)
	
ENDPROC

PROC DO_INSTANT_SWITCH(ped_index this_franklin,ped_index this_trevor)
	sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = this_franklin
	sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = this_trevor
	
	prep_selector_ped(SELECTOR_PED_FRANKLIN)
	prep_selector_ped(SELECTOR_PED_TREVOR)
	
	TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds,true,true)		
ENDPROC

PROC UPDATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS
	//CDEBUG3LN(DEBUG_MISSION, "UPDATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")
	
ENDPROC

#ENDIF

PROC SETUP_SPLINE_CAM_NODE_ARRAY_CHOPPER_TO_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX viHeli, PED_INDEX piFranklin)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_CHOPPER_TO_FRANKLIN")
	
	//IF NOT thisSwitchCam.bInitialized
	
	
		//--- Start of Cam Data ---

/*
		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-2.4172, 4.2304, 0.1681>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].vNodeDir = <<0.9921, 1.0670, 0.0872>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 45.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = TRUE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 2000
		thisSwitchCam.nodes[1].vNodePos = <<-2.4172, 4.2304, 0.1681>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].vNodeDir = <<0.9921, 1.0670, 0.0872>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 50.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = TRUE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		//thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 1600
		
		thisSwitchCam.nodes[2].vNodePos =  <<0,-3.2,0.27>> //<<1.072,1.92,0.27>> //<<-0.0618, 1.8094, 0.2695>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].vNodeDir = <<-4.0088, 0.0236, 146.3507>>
		
		thisSwitchCam.nodes[2].bPointAtEntity = FALSE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 45.0000
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.1000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[3].iNodeTime = 2200
		thisSwitchCam.nodes[3].vNodePos =  <<0,-3.2,0.27>> //<<1.072,1.92,0.27>> //<<-0.0618, 1.8094, 0.2695>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].vNodeDir = <<-4.0088, 0.0236, 146.3507>>
		thisSwitchCam.nodes[3].bPointAtEntity = FALSE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].fNodeFOV = 45.0000
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[3].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 0
		thisSwitchCam.nodes[3].iRampDownDuration = 0
		thisSwitchCam.nodes[3].iHoldDuration = 0
		thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 500
		thisSwitchCam.nodes[4].vNodePos = <<0.5885, 1.4540, 0.6298>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].vNodeDir = <<-0.7384, 0.0000, 143.2135>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 45.0000
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[4].iCamEaseType = 0
		thisSwitchCam.nodes[4].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.iNumNodes = 4
		thisSwitchCam.iCamSwitchFocusNode = 2
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
		thisSwitchCam.iGameplayNodeBlendDuration = 1000

*/
		//--- End of Cam Data ---

		IF NOT bPlayerinFirstPerson
				
				thisSwitchCam.nodes[0].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-2.4172, 4.2304, 0.1681>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].vNodeDir = <<0.9921, 1.0670, 0.0872>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 45.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = TRUE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1800
		thisSwitchCam.nodes[1].vNodePos = <<-2.4172, 4.2304, 0.1681>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].vNodeDir = <<0.9921, 1.0670, 0.0872>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 50.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = TRUE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000


		
		

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = TRUE
		thisSwitchCam.nodes[2].iNodeTime = 1600
		thisSwitchCam.nodes[2].vNodePos = <<0,0,0>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].vNodeDir = <<0,0,0>>
		thisSwitchCam.nodes[2].bPointAtEntity = FALSE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].fNodeFOV = 45.0000
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.1000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000
/*
		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[3].iNodeTime = 1000
		thisSwitchCam.nodes[3].vNodePos =  <<0,-3.2,0.27>> //<<-0.0618, 1.8094, 0.2695>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].vNodeDir = <<-4.0088, 0.0236, 151.3507>>
		thisSwitchCam.nodes[3].bPointAtEntity = FALSE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].fNodeFOV = 45.0000
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[3].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 0
		thisSwitchCam.nodes[3].iRampDownDuration = 0
		thisSwitchCam.nodes[3].iHoldDuration = 0
		thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000*/

		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = TRUE
		thisSwitchCam.nodes[3].iNodeTime = 2000
		thisSwitchCam.nodes[3].vNodePos = <<0,0,0>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].vNodeDir = <<0,0,0>>
		thisSwitchCam.nodes[3].bPointAtEntity = FALSE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].fNodeFOV = 45.0000
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[3].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 0
		thisSwitchCam.nodes[3].iRampDownDuration = 0
		thisSwitchCam.nodes[3].iHoldDuration = 0
		thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.iNumNodes = 4
		thisSwitchCam.iCamSwitchFocusNode = 2
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
		thisSwitchCam.iGameplayNodeBlendDuration = 1000
			ELSE
			
				CPRINTLN(DEBUG_TREVOR3,"rEVISE SWITCH FOR FIRST PERSON")
				thisSwitchCam.nodes[0].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
				thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
				thisSwitchCam.nodes[0].iNodeTime = 0
				thisSwitchCam.nodes[0].vNodePos = <<-2.4172, 4.2304, 0.1681>>
				thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
				thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
				thisSwitchCam.nodes[0].vNodeDir = <<0.9921, 1.0670, 0.0872>>
				thisSwitchCam.nodes[0].bPointAtEntity = TRUE
				thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
				thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
				thisSwitchCam.nodes[0].fNodeFOV = 45.0000
				thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
				thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
				thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
				thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
				thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
				thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
				thisSwitchCam.nodes[0].iCamEaseType = 0
				thisSwitchCam.nodes[0].fCamEaseScaler = 1.0000
				thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
				thisSwitchCam.nodes[0].bCamEaseForceLevel = TRUE
				thisSwitchCam.nodes[0].fTimeScale = 1.0000
				thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
				thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 1.0000
				thisSwitchCam.nodes[0].bFlashEnabled = FALSE
				thisSwitchCam.nodes[0].fMinExposure = 0.0000
				thisSwitchCam.nodes[0].fMaxExposure = 0.0000
				thisSwitchCam.nodes[0].iRampUpDuration = 0
				thisSwitchCam.nodes[0].iRampDownDuration = 0
				thisSwitchCam.nodes[0].iHoldDuration = 0
				thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
				thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
				thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
				thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
				thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
				thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

				thisSwitchCam.nodes[1].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
				thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
				thisSwitchCam.nodes[1].iNodeTime = 1800
				thisSwitchCam.nodes[1].vNodePos = <<-2.4172, 4.2304, 0.1681>>
				thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
				thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
				thisSwitchCam.nodes[1].vNodeDir = <<0.9921, 1.0670, 0.0872>>
				thisSwitchCam.nodes[1].bPointAtEntity = TRUE
				thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
				thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
				thisSwitchCam.nodes[1].fNodeFOV = 50.0000
				thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
				thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
				thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
				thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
				thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
				thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
				thisSwitchCam.nodes[1].iCamEaseType = 0
				thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
				thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
				thisSwitchCam.nodes[1].bCamEaseForceLevel = TRUE
				thisSwitchCam.nodes[1].fTimeScale = 1.0000
				thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
				thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
				thisSwitchCam.nodes[1].bFlashEnabled = FALSE
				thisSwitchCam.nodes[1].fMinExposure = 0.0000
				thisSwitchCam.nodes[1].fMaxExposure = 0.0000
				thisSwitchCam.nodes[1].iRampUpDuration = 0
				thisSwitchCam.nodes[1].iRampDownDuration = 0
				thisSwitchCam.nodes[1].iHoldDuration = 0
				thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
				thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
				thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
				thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
				thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
				thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

				thisSwitchCam.nodes[2].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
				//thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_WORLD_POS
				thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
				thisSwitchCam.nodes[2].iNodeTime = 1600
				
				thisSwitchCam.nodes[2].vNodePos =  <<0,-3.2,0.67>> //<<1.072,1.92,0.27>> //<<-0.0618, 1.8094, 0.2695>>
				thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
				thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
				thisSwitchCam.nodes[2].vNodeDir = <<-4, 0.0236, 145.3507>>
				
				thisSwitchCam.nodes[2].bPointAtEntity = FALSE
				thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
				thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
				thisSwitchCam.nodes[2].fNodeFOV = 45.0000
				thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
				thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
				thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
				thisSwitchCam.nodes[2].fNodeMotionBlur = 0.1000
				thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
				thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
				thisSwitchCam.nodes[2].iCamEaseType = 0
				thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
				thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
				thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
				thisSwitchCam.nodes[2].fTimeScale = 1.0000
				thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
				thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
				thisSwitchCam.nodes[2].bFlashEnabled = FALSE
				thisSwitchCam.nodes[2].fMinExposure = 0.0000
				thisSwitchCam.nodes[2].fMaxExposure = 0.0000
				thisSwitchCam.nodes[2].iRampUpDuration = 0
				thisSwitchCam.nodes[2].iRampDownDuration = 0
				thisSwitchCam.nodes[2].iHoldDuration = 0
				thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
				thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
				thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
				thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
				thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
				thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

				thisSwitchCam.nodes[3].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
				thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
				thisSwitchCam.nodes[3].iNodeTime = 2000
				thisSwitchCam.nodes[3].vNodePos =  <<0,-3.2,0.67>> //<<1.072,1.92,0.27>> //<<-0.0618, 1.8094, 0.2695>>
				thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
				thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
				thisSwitchCam.nodes[3].vNodeDir = <<-4.0088, 0.0236, 145.3507>>
				thisSwitchCam.nodes[3].bPointAtEntity = FALSE
				thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = FALSE
				thisSwitchCam.nodes[3].bAttachOffsetIsRelative = TRUE
				thisSwitchCam.nodes[3].fNodeFOV = 45.0000
				thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
				thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
				thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
				thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
				thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_MEDIUM
				thisSwitchCam.nodes[3].fNodeCamShake = 0.0000
				thisSwitchCam.nodes[3].iCamEaseType = 0
				thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
				thisSwitchCam.nodes[3].bCamEaseForceLinear = TRUE
				thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
				thisSwitchCam.nodes[3].fTimeScale = 1.0000
				thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
				thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
				thisSwitchCam.nodes[3].bFlashEnabled = FALSE
				thisSwitchCam.nodes[3].fMinExposure = 0.0000
				thisSwitchCam.nodes[3].fMaxExposure = 0.0000
				thisSwitchCam.nodes[3].iRampUpDuration = 0
				thisSwitchCam.nodes[3].iRampDownDuration = 0
				thisSwitchCam.nodes[3].iHoldDuration = 0
				thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
				thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
				thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
				thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
				thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
				thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

				

				thisSwitchCam.iNumNodes = 4
				thisSwitchCam.iCamSwitchFocusNode = 2
				thisSwitchCam.bSplineNoSmoothing = FALSE
				thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
				thisSwitchCam.iGameplayNodeBlendDuration = 1000
				cprintln(debug_trevor3,"first person switch")			
			ENDIF

		

		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_CarSteal2_HeliToFranklin.txt"
		thisSwitchCam.strXMLFileName = "CameraInfo_CarSteal2_HeliToFranklin.xml"
		
		thisSwitchCam.bInitialized = TRUE
//	ENDIF
	
	thisSwitchCam.viVehicles[0] = viHeli
	thisSwitchCam.piPeds[1] = piFranklin
ENDPROC

enum eGarageDoorState
	GD_NONE,
	GD_CREATE_AND_LOCK,
	GD_LOCKING,
	GD_OPEN,
	GD_NO_LONGER_NEEDED,
	GD_RESET
endenum

eGarageDoorState eCurrentDoorState

PROC UPDATE_GARAGE_DOOR_STATE()
	SWITCH eCurrentDoorState
		CASE GD_NONE
		BREAK
		CASE GD_CREATE_AND_LOCK
			
			//set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,true,0)
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<201.400284,-153.364517,57.848885>>,1.0,PROP_GAR_DOOR_05)
				realGarageDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<201.400284,-153.364517,57.848885>>,1.0,PROP_GAR_DOOR_05)
				set_entity_coords(realGarageDoor,<<201.400284,-153.364517,47.848885>>)
				fakeGarageDoor = CREATE_OBJECT_NO_OFFSET(PROP_GAR_DOOR_05,<<201.400284,-153.364517,57.848885>>)
				set_entity_rotation(fakeGarageDoor,<<-1.441772,-0.000000,-20.000166>>)
				SET_ENTITY_LOD_DIST(fakeGarageDoor,400)
				eCurrentDoorState = GD_LOCKING
				cprintln(debug_trevor3,"DOOR NOW CREATED")
			ENDIF												
		BREAK
		
		CASE GD_LOCKING
			IF set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,TRUE,-2)
				cprintln(debug_trevor3,"DOOR NOW LOCKED")
				eCurrentDoorState = GD_NONE
			ENDIF
		BREAK
		CASE GD_OPEN
			set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,false,1.0) //just unlock it and let it open itself
			eCurrentDoorState = GD_NONE
		BREAK
		CASE GD_NO_LONGER_NEEDED
			cprintln(debug_trevor3,"SET DOOR AS NO LONGER NEEDED")
			IF DOES_ENTITY_EXIST(realGarageDoor)
				cprintln(debug_trevor3,"TRYING TO RESET REAL DOOR")
				IF DOES_ENTITY_EXIST(fakeGarageDoor)
					DELETE_OBJECT(fakeGarageDoor)
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_GAR_DOOR_05)
				ENDIF
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<201.400284,-153.364517,57.848885>>,1.0,PROP_GAR_DOOR_05)
					set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,47.8>>,false,1.0)										
					set_entity_coords(realGarageDoor,<<201.400284,-153.364517,57.848885>>)										
				ENDIF
				eCurrentDoorState = GD_NONE
			ENDIF
		BREAK
		CASE GD_RESET
			IF DOES_ENTITY_EXIST(fakeGarageDoor)
				DELETE_OBJECT(fakeGarageDoor)
			ENDIF
			IF DOES_ENTITY_EXIST(realGarageDoor)
				set_entity_coords(realGarageDoor,<<201.400284,-153.364517,57.848885>>)
			ENDIF
			eCurrentDoorState = GD_NONE
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_GARAGE_DOOR_STATE(eGarageDoorState eNewDoorState)
	eCurrentDoorState = eNewDoorState
ENDPROC

FUNC BOOL IS_PLAYER_PRESSING_A_CONTROL_BUTTON()
	IF GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_MOVE_LR) != 127
	OR GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_MOVE_UD) != 127
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_ATTACK)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SELECT_CHARACTER_TREVOR)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_AIM)
	OR GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_SCALED_LOOK_UD) != 127
	OR GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_SCALED_LOOK_LR) != 127
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// =============================================== Asssets

FUNC BOOL IS_ASSET_READY(eMissionAssets ethisAsset,bool bReadyIfLoadedOnly = false)
	int thisAsset = enum_to_int(ethisAsset)
	IF currentAssetStage >= assets[thisAsset].loadFor 
	AND currentAssetStage < assets[thisAsset].releaseOn			
		SWITCH int_to_enum(eMissionAssets,thisAsset)
			CASE ASSET_Police_DEPT 
				IF HAS_MODEL_LOADED(S_M_Y_COP_01)


				AND HAS_MODEL_LOADED(A_M_Y_GenStreet_02)
				AND HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@start_idle")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2officer")
				AND HAS_ANIM_DICT_LOADED("reaction@points@")
				AND HAS_ANIM_DICT_LOADED("switch@trevor@head_in_sink")
					RETURN TRUE
				ENDIF
			BREAK		
			CASE ASSET_CHOPPER IF HAS_MODEL_LOADED(POLMAV) RETURN TRUE ENDIF BREAK
			
			CASE ASSET_PILOT_AT_START 
				IF HAS_MODEL_LOADED(model_pilot)
					RETURN TRUE
				ENDIF
			BREAK
			CASE ASSET_AREA_1_ASSETS

				IF HAS_MODEL_LOADED(PROP_CS_PACKAGE_01)
				AND HAS_MODEL_LOADED(BOXVILLE2)			
				AND HAS_MODEL_LOADED(s_f_y_hooker_01)
				AND HAS_MODEL_LOADED(S_M_M_janitor)
				AND HAS_MODEL_LOADED(A_M_Y_BEACH_02)
				AND HAS_ANIM_DICT_LOADED("misscarsteal2MUGGING")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2PERVERT")
					RETURN TRUE
				ENDIF
			BREAK
			CASE ASSET_AREA_2_ASSETS
				IF HAS_MODEL_LOADED(s_f_y_hooker_02)
				AND HAS_MODEL_LOADED(s_f_y_hooker_01) //pimp scene
				AND HAS_MODEL_LOADED(U_M_Y_FIBMugger_01)
				AND HAS_MODEL_LOADED(A_M_M_OG_BOSS_01)
				AND HAS_MODEL_LOADED(A_C_ROTTWEILER) //dog walker
				AND HAS_MODEL_LOADED(A_M_Y_BEACH_02)
				AND HAS_MODEL_LOADED(PROP_GAR_DOOR_05)
				AND HAS_ANIM_DICT_LOADED("misscarsteal2PIMPSEX")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2CHAD_GOODBYE")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2_bum")
				AND HAS_ANIM_DICT_LOADED("CREATURES@ROTTWEILER@AMB@WORLD_DOG_BARKING@idle_a")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102,"CS2")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(103,"CS2")
				AND GET_IS_WAYPOINT_RECORDING_LOADED("cs2_01")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE ASSET_CHAD IF HAS_MODEL_LOADED(model_chad) RETURN TRUE ENDIF BREAK
			CASE ASSET_ZYPE IF HAS_MODEL_LOADED(ZTYPE) RETURN TRUE ENDIF cprintln(DEBUG_TREVOR3,"HAS ZTYPE LOADED") BREAK
			CASE ASSET_CARPARK
				IF HAS_MODEL_LOADED(BURRITO) //car park
				AND HAS_MODEL_LOADED(DOMINATOR)
				AND HAS_MODEL_LOADED(HABANERO)
				AND HAS_MODEL_LOADED(DUBSTA)
				AND HAS_MODEL_LOADED(model_pilot)
				AND HAS_MODEL_LOADED(A_M_Y_GENSTREET_01)
				AND HAS_ANIM_DICT_LOADED("misscarsteal2peeing")
				AND HAS_ANIM_DICT_LOADED("misscarstealfinalecar_5_ig_1")
				AND HAS_ANIM_DICT_LOADED("missarmenian2lamar_idles")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2Chad_waiting")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2CAR_STOLEN")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2CHAD_GARAGE")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2switch")
				AND HAS_ANIM_DICT_LOADED("misscarsteal2fixer")
				AND GET_IS_WAYPOINT_RECORDING_LOADED("cs2_10")
					RETURN TRUE
				ENDIF
			BREAK		
			CASE ASSET_DEVIN 
				IF HAS_MODEL_LOADED(IG_DEVIN) 
				AND HAS_MODEL_LOADED(S_M_Y_DEVINSEC_01)
				AND HAS_MODEL_LOADED(S_M_M_Security_01)
				AND HAS_MODEL_LOADED(CS_MOLLY)
				AND HAS_MODEL_LOADED(SHAMAL)
				AND HAS_MODEL_LOADED(TAILGATER)
				AND HAS_MODEL_LOADED(S_M_M_PILOT_01)
				AND HAS_ANIM_DICT_LOADED("misscarsteal2leadinoutcar_2_mcs_1")
					RETURN TRUE
				ENDIF 
			BREAK
			CASE ASSET_CHASE_AUDIO				
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		IF bReadyIfLoadedOnly
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	//cprintln(debug_trevor3,"Asset not ready: ",ethisAsset)
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ASSET_LOADED(eMissionAssets ethisAsset)
	IF IS_ASSET_READY(ethisAsset,TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNc

FUNC BOOL IS_ASSET_STAGE_READY(eAssetStage thisStage)
	IF NOT bAssetChageInProgress
	AND currentAssetStage = thisStage
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_ASSETS()
	
	IF bAssetChageInProgress
		int i
		REPEAT COUNT_OF(ASSETS) i
			IF NOT IS_ASSET_READY(INT_TO_ENUM(eMissionAssets,i))
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF
	bAssetChageInProgress = FALSE
	RETURN TRUE
ENDFUNC

PROC LOAD_AND_RELEASE_ASSETS(eAssetStage newAssetStage)
	//cprintln(debug_Trevor3,"LOAD_AND_RELEASE_ASSETS: ",newAssetStage)
	int iAssetArray
	
	//remove models first
	REPEAT COUNT_OF(assets) iAssetArray
			IF newAssetStage >= assets[iAssetArray].loadFor
			AND newAssetStage < assets[iAssetArray].releaseOn
			ELSE
				IF assets[iAssetArray].loaded
					SWITCH int_to_enum(eMissionAssets,iAssetArray)
						CASE ASSET_Police_DEPT 
							SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01) 
							REMOVE_ANIM_DICT("misscarsteal2officer") 	
							REMOVE_ANIM_DICT("missheistdockssetup1ig_13@start_idle")
							REMOVE_ANIM_DICT("reaction@points@")
							REMOVE_ANIM_DICT("switch@trevor@head_in_sink")
						
							SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_GenStreet_02)
						BREAK
						CASE ASSET_CHOPPER SET_MODEL_AS_NO_LONGER_NEEDED(POLMAV)  BREAK
					
						CASE ASSET_PILOT_AT_START SET_MODEL_AS_NO_LONGER_NEEDED(model_pilot) BREAK
						CASE ASSET_AREA_1_ASSETS
							
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_PACKAGE_01)
							SET_MODEL_AS_NO_LONGER_NEEDED(BOXVILLE2)																	
							SET_MODEL_AS_NO_LONGER_NEEDED(s_f_y_hooker_01)
							SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_janitor)
							SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_BEACH_02)
							REMOVE_ANIM_DICT("misscarsteal2MUGGING")
							REMOVE_ANIM_DICT("misscarsteal2PERVERT")
								
							
						BREAK
						CASE ASSET_AREA_2_ASSETS
							SET_MODEL_AS_NO_LONGER_NEEDED(s_f_y_hooker_02)
							SET_MODEL_AS_NO_LONGER_NEEDED(s_f_y_hooker_01) //pimp scene
							SET_MODEL_AS_NO_LONGER_NEEDED(U_M_Y_FIBMugger_01)
							SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_OG_BOSS_01)
							SET_MODEL_AS_NO_LONGER_NEEDED(A_C_ROTTWEILER) //dog walker
							SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_BEACH_02)
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_GAR_DOOR_05)
							REMOVE_ANIM_DICT("misscarsteal2PIMPSEX")
							REMOVE_ANIM_DICT("misscarsteal2CHAD_GOODBYE")
							REMOVE_ANIM_DICT("misscarsteal2_bum")
							REMOVE_ANIM_DICT("CREATURES@ROTTWEILER@AMB@WORLD_DOG_BARKING@idle_a")
							REMOVE_VEHICLE_RECORDING(102,"CS2")							
							REMOVE_VEHICLE_RECORDING(103,"CS2")	
							REMOVE_WAYPOINT_RECORDING("cs2_01")
							RELEASE_SCRIPT_AUDIO_BANK()
						BREAK
						
						CASE ASSET_CHAD SET_MODEL_AS_NO_LONGER_NEEDED(model_chad)  BREAK
						CASE ASSET_ZYPE SET_MODEL_AS_NO_LONGER_NEEDED(ZTYPE) cprintln(DEBUG_TREVOR3,"REMOVe ZTYPE") BREAK
						CASE ASSET_CARPARK
							SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO) //car park
							SET_MODEL_AS_NO_LONGER_NEEDED(DOMINATOR)
							SET_MODEL_AS_NO_LONGER_NEEDED(HABANERO)
							SET_MODEL_AS_NO_LONGER_NEEDED(DUBSTA)
							SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_GENSTREET_01)
							SET_MODEL_AS_NO_LONGER_NEEDED(model_pilot)
							REMOVE_ANIM_DICT("misscarsteal2peeing")
							REMOVE_ANIM_DICT("misscarstealfinalecar_5_ig_1")
							REMOVE_ANIM_DICT("missarmenian2lamar_idles")
							REMOVE_ANIM_DICT("misscarsteal2")
							REMOVE_ANIM_DICT("misscarsteal2Chad_waiting")
							REMOVE_ANIM_DICT("misscarsteal2CAR_STOLEN")
							REMOVE_ANIM_DICT("misscarsteal2CHAD_GARAGE")
							REMOVE_ANIM_DICT("misscarsteal2switch")
							REMOVE_ANIM_DICT("misscarsteal2fixer")
							REMOVE_WAYPOINT_RECORDING("cs2_10")
						BREAK
						CASE ASSET_DEVIN 
							SET_MODEL_AS_NO_LONGER_NEEDED(IG_DEVIN) 
							SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
							SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_DEVINSEC_01)
							SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_Security_01)
							SET_MODEL_AS_NO_LONGER_NEEDED(CS_MOLLY)
							SET_MODEL_AS_NO_LONGER_NEEDED(TAILGATER)
							SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
							REMOVE_ANIM_DICT("misscarsteal2leadinoutcar_2_mcs_1")
						BREAK
						CASE ASSET_CHASE_AUDIO
							RELEASE_NAMED_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
						BREAK
					ENDSWITCH
					assets[iAssetArray].loaded = FALSE
				ENDIF
			ENDIF
	ENDREPEAT
	
	//load assets
	REPEAT COUNT_OF(assets) iAssetArray
			IF newAssetStage >= assets[iAssetArray].loadFor
			AND newAssetStage < assets[iAssetArray].releaseOn
				//cprintln(debug_Trevor3,"LOAD ASSETS: ",iAssetArray)
				//load assets
				SWITCH int_to_enum(eMissionAssets,iAssetArray)
					CASE ASSET_Police_DEPT 
						REQUEST_MODEL(S_M_Y_COP_01) 
					
				
						REQUEST_MODEL(A_M_Y_GenStreet_02)
						REQUEST_ANIM_DICT("misscarsteal2officer") 
						REQUEST_ANIM_DICT("reaction@points@")
						REQUEST_ANIM_DICT("missheistdockssetup1ig_13@start_idle")
						REQUEST_ANIM_DICT("switch@trevor@head_in_sink")
					BREAK
					CASE ASSET_CHOPPER REQUEST_MODEL(POLMAV)  BREAK
			
					CASE ASSET_PILOT_AT_START REQUEST_MODEL(model_pilot) BREAK
					CASE ASSET_AREA_1_ASSETS
						REQUEST_MODEL(PROP_CS_PACKAGE_01)
						REQUEST_MODEL(BOXVILLE2)						
						REQUEST_MODEL(s_f_y_hooker_01)
						REQUEST_MODEL(S_M_M_janitor)
						REQUEST_MODEL(A_M_Y_BEACH_02)
						REQUEST_ANIM_DICT("misscarsteal2MUGGING")
						REQUEST_ANIM_DICT("misscarsteal2PERVERT")							
					BREAK
					CASE ASSET_AREA_2_ASSETS
						REQUEST_MODEL(s_f_y_hooker_02)
						REQUEST_MODEL(s_f_y_hooker_01) //pimp scene
						REQUEST_MODEL(U_M_Y_FIBMugger_01)
						REQUEST_MODEL(A_M_M_OG_BOSS_01)
						REQUEST_MODEL(A_C_ROTTWEILER) //dog walker
						REQUEST_MODEL(A_M_Y_BEACH_02)
						REQUEST_MODEL(PROP_GAR_DOOR_05)
						REQUEST_ANIM_DICT("misscarsteal2PIMPSEX")
						REQUEST_ANIM_DICT("misscarsteal2CHAD_GOODBYE")
						REQUEST_ANIM_DICT("misscarsteal2_bum")		
						REQUEST_ANIM_DICT("CREATURES@ROTTWEILER@AMB@WORLD_DOG_BARKING@idle_a")	
						REQUEST_VEHICLE_RECORDING(102,"CS2")
						REQUEST_VEHICLE_RECORDING(103,"CS2")
						REQUEST_WAYPOINT_RECORDING("cs2_01")
					BREAK
					
					CASE ASSET_CHAD REQUEST_MODEL(model_chad)  BREAK
					CASE ASSET_ZYPE REQUEST_MODEL(ZTYPE) cprintln(DEBUG_TREVOR3,"LOAD ZTYPE") BREAK
					CASE ASSET_CARPARK
						REQUEST_MODEL(BURRITO) //car park
						REQUEST_MODEL(DOMINATOR)
						REQUEST_MODEL(HABANERO)
						REQUEST_MODEL(DUBSTA)
						REQUEST_MODEL(model_pilot)
						REQUEST_MODEL(A_M_Y_GENSTREET_01)
						REQUEST_ANIM_DICT("misscarsteal2peeing")
						REQUEST_ANIM_DICT("misscarstealfinalecar_5_ig_1")
						REQUEST_ANIM_DICT("missarmenian2lamar_idles")
						REQUEST_ANIM_DICT("misscarsteal2")
						REQUEST_ANIM_DICT("misscarsteal2Chad_waiting")
						REQUEST_ANIM_DICT("misscarsteal2CAR_STOLEN")
						REQUEST_ANIM_DICT("misscarsteal2CHAD_GARAGE")	
						REQUEST_ANIM_DICT("misscarsteal2switch")
						REQUEST_ANIM_DICT("misscarsteal2fixer")
						REQUEST_WAYPOINT_RECORDING("cs2_10")						
					BREAK
					CASE ASSET_DEVIN 
						REQUEST_MODEL(IG_DEVIN)
						REQUEST_MODEL(SHAMAL)
						REQUEST_MODEL(S_M_Y_DEVINSEC_01)
						REQUEST_MODEL(S_M_M_Security_01)
						REQUEST_MODEL(CS_MOLLY)
						REQUEST_MODEL(TAILGATER)
						REQUEST_MODEL(S_M_M_PILOT_01)
						REQUEST_ANIM_DICT("misscarsteal2leadinoutcar_2_mcs_1")
					BREAK
					CASE ASSET_CHASE_AUDIO
						REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
					BREAK
				
				ENDSWITCH
				assets[iAssetArray].loaded = TRUE

			ENDIF
		endrepeat
ENDPROC

PROC SET_ASSET_STAGE(eAssetStage newAssetStage)
	//cprintln(debug_Trevor3,"SET_ASSET_STAGE: ",newAssetStage)
	bAssetChageInProgress = TRUE
	currentAssetStage = newAssetStage
	LOAD_AND_RELEASE_ASSETS(newAssetStage)
ENDPROC

PROC FORCE_ASSET_STAGE(eAssetStage newAssetStage)
	//cprintln(debug_Trevor3,"FORCE_ASSET_STAGE: ",newAssetStage)
	bAssetChageInProgress = TRUE
	currentAssetStage = newAssetStage
	LOAD_AND_RELEASE_ASSETS(newAssetStage)
	WHILE bAssetChageInProgress
		HANDLE_ASSETS()
		safewait()
	ENDWHILE
ENDPROC



PROC PREP_ASSET(eMissionAssets assetType, eAssetStage startAt, eAssetStage endOn)
	
	ASSETS[enum_to_int(assetType)].loadFor = startAt
	ASSETS[enum_to_int(assetType)].releaseOn = endOn
ENDPROC

//end assets

//set up heist steal vehicle
PROC initHeistVehicle(vector vPos,float fHeading)
	initVehicle(vehHeist,ztype,vPos,fHeading)	
	SET_VEHICLE_COLOURS(vehicle[vehHeist].id, CARSTEAL_COLOURS_ZTYPE, CARSTEAL_COLOURS_ZTYPE)
	SET_VEHICLE_EXTRA_COLOURS(vehicle[vehHeist].id, CARSTEAL_COLOURS_ZTYPE, CARSTEAL_COLOURS_ZTYPE)
	SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehicle[vehHeist].id,FALSE)
	SET_VEHICLE_HAS_STRONG_AXLES(vehicle[vehHeist].id,TRUE)
	SET_ENTITY_HEALTH(vehicle[vehHeist].id,1000)
	SET_VEHICLE_AS_RESTRICTED(vehicle[vehHeist].id, 0)
ENDPROC

FUNC STRING GET_FAIL_STRING(enumFails failCondition)
	SWITCH failCondition
		CASE FAIL_NULL RETURN "FAIL_NULL" BREAK
		CASE FAIL_PLAYER_HAS_WEAPON_DRAWN_IN_POL_DEPT RETURN "FAIL_PLAYER_HAS_WEAPON_DRAWN_IN_POL_DEPT" BREAK
		CASE FAIL_HAS_WANTED_LEVEL_IN_POLICE_DEPT RETURN "FAIL_HAS_WANTED_LEVEL_IN_POLICE_DEPT" BREAK
		CASE FAIL_COP_DIES RETURN "FAIL_COP_DIES" BREAK
		CASE FAIL_PLAYER_SHOOTING_NEAR_POLICE_DEPT RETURN "FAIL_PLAYER_SHOOTING_NEAR_POLICE_DEPT" BREAK
		CASE FAIL_PLAYER_CREATED_DISTURBANCE_NEAR_POL_DEPT RETURN "FAIL_PLAYER_CREATED_DISTURBANCE_NEAR_POL_DEPT" BREAK
		CASE FAIL_COPS_IN_COMBAT RETURN "FAIL_COPS_IN_COMBAT" BREAK
		CASE FAIL_CHOPPER_DESTROYED RETURN "FAIL_CHOPPER_DESTROYED" BREAK
		CASE FAIL_PLAYER_WANDERS_AWAY_FROM_POLICE_DEPT RETURN "FAIL_PLAYER_WANDERS_AWAY_FROM_POLICE_DEPT" BREAK
		CASE FAIL_PILOT_FLEW_AWAY RETURN "FAIL_PILOT_FLEW_AWAY" BREAK
		CASE FAIL_ZTYPE_DAMAGED RETURN "FAIL_ZTYPE_DAMAGED" BREAK
		CASE FAIL_PLAYER_LEAVES_ZTYPE RETURN "FAIL_PLAYER_LEAVES_ZTYPE" BREAK
		CASE FAIL_CHAD_ESCAPES_WITH_ZTYPE RETURN "FAIL_CHAD_ESCAPES_WITH_ZTYPE" BREAK
		CASE FAIL_ZTYPE_STUCK RETURN "FAIL_ZTYPE_STUCK" BREAK
		CASE FAIL_FLEW_TOO_FAR_AWAY RETURN "FAIL_FLEW_TOO_FAR_AWAY" BREAK
	ENDSWITCH
	RETURN "NULL"
ENDFUNC

FUNC STRING GET_CONDITION_STRING(enumconditions thisCond)
	SWITCH thisCond
		CASE	COND_NULL RETURN	"	COND_NULL RETURN	" BREAK
CASE	COND_FAILING RETURN	"	COND_FAILING RETURN	" BREAK
CASE	COND_COP_WALKING_TO_STAIRS RETURN	"	COND_COP_WALKING_TO_STAIRS RETURN	" BREAK
CASE	COND_CUTSCENE_ENDED RETURN	"	COND_CUTSCENE_ENDED RETURN	" BREAK
CASE	COND_PLAYER_ENTERED_RECEPTION RETURN	"	COND_PLAYER_ENTERED_RECEPTION RETURN	" BREAK
CASE	COND_COP_IN_CORRIDOR RETURN	"	COND_COP_IN_CORRIDOR RETURN	" BREAK
CASE	COND_PLAYER_ANYWHERE_IN_STAIRWALL RETURN	"	COND_PLAYER_ANYWHERE_IN_STAIRWALL RETURN	" BREAK
CASE	COND_PLAYER_LAGS_BEHIND_OR_WANDERS RETURN	"	COND_PLAYER_LAGS_BEHIND_OR_WANDERS RETURN	" BREAK
CASE	COND_PLAYER_UP_STAIRWELL RETURN	"	COND_PLAYER_UP_STAIRWELL RETURN	" BREAK
CASE	COND_PLAYER_IN_CORRIDOR RETURN	"	COND_PLAYER_IN_CORRIDOR RETURN	" BREAK
CASE	COND_PLAYER_OUT_OF_BOUNDS RETURN	"	COND_PLAYER_OUT_OF_BOUNDS RETURN	" BREAK
CASE	COND_PLAYER_NEARBY RETURN	"	COND_PLAYER_NEARBY RETURN	" BREAK
CASE	COND_COPS_ALERTED_CONDITION RETURN	"	COND_COPS_ALERTED_CONDITION RETURN	" BREAK
CASE	COND_COP_IN_STAIRWELL RETURN	"	COND_COP_IN_STAIRWELL RETURN	" BREAK
CASE	COND_COP_FINISHED_LEADING_PLAYER RETURN	"	COND_COP_FINISHED_LEADING_PLAYER RETURN	" BREAK
CASE	COND_CAN_SPAWN_CHOPPER RETURN	"	COND_CAN_SPAWN_CHOPPER RETURN	" BREAK
CASE	COND_PLAYER_NOT_FOLLOWING_COP RETURN	"	COND_PLAYER_NOT_FOLLOWING_COP RETURN	" BREAK
CASE	COND_DIA_COP_LEADING RETURN	"	COND_DIA_COP_LEADING RETURN	" BREAK
CASE	COND_PLAYER_ON_ROOF RETURN	"	COND_PLAYER_ON_ROOF RETURN	" BREAK
CASE	COND_PLAYER_TRYS_TO_ENTER_CHOPPER RETURN	"	COND_PLAYER_TRYS_TO_ENTER_CHOPPER RETURN	" BREAK
CASE	COND_PLAYER_IN_LOCKER_ROOM RETURN	"	COND_PLAYER_IN_LOCKER_ROOM RETURN	" BREAK
CASE	COND_PLAYER_RUNS_AHEAD RETURN	"	COND_PLAYER_RUNS_AHEAD RETURN	" BREAK
CASE	COND_TREVOR_TELLS_PILOT_TO_TAKE_OFF RETURN	"	COND_TREVOR_TELLS_PILOT_TO_TAKE_OFF RETURN	" BREAK
CASE	COND_TREVOR_TELLS_PILOT_TO_ACTIVATE_SCANNER RETURN	"	COND_TREVOR_TELLS_PILOT_TO_ACTIVATE_SCANNER RETURN	" BREAK
CASE	COND_READY_TO_TURN_ON_SCANNER RETURN	"	COND_READY_TO_TURN_ON_SCANNER RETURN	" BREAK
CASE	COND_DIA_BACK_TO_FRONT_DESK RETURN	"	COND_DIA_BACK_TO_FRONT_DESK RETURN	" BREAK
CASE	COND_DIA_GET_IN RETURN	"	COND_DIA_GET_IN RETURN	" BREAK
CASE	COND_PLAYER_IN_CHOPPER RETURN	"	COND_PLAYER_IN_CHOPPER RETURN	" BREAK
CASE	COND_COP_LEADING_PLAYER RETURN	"	COND_COP_LEADING_PLAYER RETURN	" BREAK
CASE	COND_PLAYER_SPOTTED_BY_PILOT RETURN	"	COND_PLAYER_SPOTTED_BY_PILOT RETURN	" BREAK
CASE	COND_START_FRANKLIN RETURN	"	COND_START_FRANKLIN RETURN	" BREAK
CASE	COND_PLAYER_LEAVING_TREVOR RETURN	"	COND_PLAYER_LEAVING_TREVOR RETURN	" BREAK
CASE	COND_END_FRANKLIN RETURN	"	COND_END_FRANKLIN RETURN	" BREAK
CASE	COND_SCAN_STAGE_START RETURN	"	COND_SCAN_STAGE_START RETURN	" BREAK
CASE	COND_STARTED_AS_TREVOR RETURN	"	COND_STARTED_AS_TREVOR RETURN	" BREAK
CASE	COND_FAR_ENOUGH_FROM_FRANKLIN_TO_PLAY_CONVO RETURN	"	COND_FAR_ENOUGH_FROM_FRANKLIN_TO_PLAY_CONVO RETURN	" BREAK
CASE	COND_LEARNT_TO_SCAN RETURN	"	COND_LEARNT_TO_SCAN RETURN	" BREAK
CASE	COND_BOOTING_UP_LINE_PLAYED RETURN	"	COND_BOOTING_UP_LINE_PLAYED RETURN	" BREAK
CASE	COND_FRANKLIN_SCANNED RETURN	"	COND_FRANKLIN_SCANNED RETURN	" BREAK
CASE	COND_DIA_SCANNED_CONVO_FINISHED RETURN	"	COND_DIA_SCANNED_CONVO_FINISHED RETURN	" BREAK
CASE	COND_DIA_SCAN_MY_BUDDY RETURN	"	COND_DIA_SCAN_MY_BUDDY RETURN	" BREAK
CASE	COND_TREV_SAID_TO_FIND_FRANKLIN RETURN	"	COND_TREV_SAID_TO_FIND_FRANKLIN RETURN	" BREAK
CASE	COND_TOLD_TO_SCAN_FRANKLIN RETURN	"	COND_TOLD_TO_SCAN_FRANKLIN RETURN	" BREAK
CASE	COND_OUTSIDE_SAFE_ZONE RETURN	"	COND_OUTSIDE_SAFE_ZONE RETURN	" BREAK
CASE	COND_FLEW_IN_TO_FAIL_ZONE RETURN	"	COND_FLEW_IN_TO_FAIL_ZONE RETURN	" BREAK
CASE	COND_SCAN_STAGE_END RETURN	"	COND_SCAN_STAGE_END RETURN	" BREAK
CASE	COND_START_SCAN_AREA_1 RETURN	"	COND_START_SCAN_AREA_1 RETURN	" BREAK
CASE	COND_POSTAL_BEING_OBSERVED RETURN	"	COND_POSTAL_BEING_OBSERVED RETURN	" BREAK
CASE	COND_PERVERT_BEING_OBSERVED RETURN	"	COND_PERVERT_BEING_OBSERVED RETURN	" BREAK
CASE	COND_AREA1_PED_JUST_SCANNED RETURN	"	COND_AREA1_PED_JUST_SCANNED RETURN	" BREAK
CASE	COND_SCANNED_PED_WAS_A_WOMAN RETURN	"	COND_SCANNED_PED_WAS_A_WOMAN RETURN	" BREAK
CASE	COND_DIA_COME_IN_FRANKLIN_PLAYED RETURN	"	COND_DIA_COME_IN_FRANKLIN_PLAYED RETURN	" BREAK
CASE	COND_CHOPPER_IN_VIEWING_RANGE_OF_AREA1 RETURN	"	COND_CHOPPER_IN_VIEWING_RANGE_OF_AREA1 RETURN	" BREAK
CASE	COND_START_SCAN_AREA_3 RETURN	"	COND_START_SCAN_AREA_3 RETURN	" BREAK
CASE	COND_LISTENING_IN_ON_DIALOGUE RETURN	"	COND_LISTENING_IN_ON_DIALOGUE RETURN	" BREAK
CASE	COND_END_SCAN_AREA_1 RETURN	"	COND_END_SCAN_AREA_1 RETURN	" BREAK
CASE	COND_AREA3_PED_JUST_SCANNED RETURN	"	COND_AREA3_PED_JUST_SCANNED RETURN	" BREAK
CASE	COND_PROSIE_BEING_OBSERVED RETURN	"	COND_PROSIE_BEING_OBSERVED RETURN	" BREAK
CASE	COND_CHAD_BEING_OBSERVED RETURN	"	COND_CHAD_BEING_OBSERVED RETURN	" BREAK
CASE	COND_CHAD_WAS_SCANNED RETURN	"	COND_CHAD_WAS_SCANNED RETURN	" BREAK
CASE	COND_CHAD_HIDDEN RETURN	"	COND_CHAD_HIDDEN RETURN	" BREAK
CASE	COND_CHAD_ON_SCREEN RETURN	"	COND_CHAD_ON_SCREEN RETURN	" BREAK
CASE	COND_CHAD_OFFSCREEN_FOR_4_SECONDS RETURN	"	COND_CHAD_OFFSCREEN_FOR_4_SECONDS RETURN	" BREAK
CASE	COND_CHAD_BY_GARAGES RETURN	"	COND_CHAD_BY_GARAGES RETURN	" BREAK
CASE	COND_CHAD_IN_APARTMENT_LOCATE RETURN	"	COND_CHAD_IN_APARTMENT_LOCATE RETURN	" BREAK
CASE	COND_GARAGE_DOOR_OPENING RETURN	"	COND_GARAGE_DOOR_OPENING RETURN	" BREAK
CASE	COND_END_SCAN_AREA_3 RETURN	"	COND_END_SCAN_AREA_3 RETURN	" BREAK
CASE	COND_CARPARK_STARTS RETURN	"	COND_CARPARK_STARTS RETURN	" BREAK
CASE	COND_CHOPPER_DESCENDING RETURN	"	COND_CHOPPER_DESCENDING RETURN	" BREAK
CASE	COND_FRANKLIN_STOPPED_HIS_CAR RETURN	"	COND_FRANKLIN_STOPPED_HIS_CAR RETURN	" BREAK
CASE	COND_FRANKLIN_HAS_STOPPED_RUNNING RETURN	"	COND_FRANKLIN_HAS_STOPPED_RUNNING RETURN	" BREAK
CASE	COND_A_LITTLE_AFTER_EXITS_CLEAR_DIA RETURN	"	COND_A_LITTLE_AFTER_EXITS_CLEAR_DIA RETURN	" BREAK
CASE	COND_DIA_TAKE_US_DOWN RETURN	"	COND_DIA_TAKE_US_DOWN RETURN	" BREAK
CASE	COND_DIA_THERMAL_VISION RETURN	"	COND_DIA_THERMAL_VISION RETURN	" BREAK
CASE	COND_DIA_CAN_YOU_SEE_FRANKLIN RETURN	"	COND_DIA_CAN_YOU_SEE_FRANKLIN RETURN	" BREAK
CASE	COND_DIA_ANY_OTHER_HEAT_SOURCES RETURN	"	COND_DIA_ANY_OTHER_HEAT_SOURCES RETURN	" BREAK
CASE	COND_DIA_LOOK_FOR_MORE_HEAT_PLAYED RETURN	"	COND_DIA_LOOK_FOR_MORE_HEAT_PLAYED RETURN	" BREAK
CASE	COND_DIA_FRANKLIN_SEES_PISSER RETURN	"	COND_DIA_FRANKLIN_SEES_PISSER RETURN	" BREAK
CASE	COND_DIA_FRANKLIN_SEES_FIXING_MAN RETURN	"	COND_DIA_FRANKLIN_SEES_FIXING_MAN RETURN	" BREAK
CASE	COND_DIA_FRANKLIN_SEES_CHAD RETURN	"	COND_DIA_FRANKLIN_SEES_CHAD RETURN	" BREAK
CASE	COND_DIA_TREVOR_SEE_FRANKLIN_ENDED RETURN	"	COND_DIA_TREVOR_SEE_FRANKLIN_ENDED RETURN	" BREAK
CASE	COND_CHOPPER_AT_CAR_PARK_LEVEL RETURN	"	COND_CHOPPER_AT_CAR_PARK_LEVEL RETURN	" BREAK
CASE	COND_THERMAL_TURNED_ON RETURN	"	COND_THERMAL_TURNED_ON RETURN	" BREAK
CASE	COND_FRANKLIN_OBSERVED_WITH_THERMAL RETURN	"	COND_FRANKLIN_OBSERVED_WITH_THERMAL RETURN	" BREAK
CASE	COND_SEE_WANKER RETURN	"	COND_SEE_WANKER RETURN	" BREAK
CASE	COND_SEE_FIXING_MAN RETURN	"	COND_SEE_FIXING_MAN RETURN	" BREAK
CASE	COND_SEE_CHAD RETURN	"	COND_SEE_CHAD RETURN	" BREAK
CASE	COND_SEE_LEANING_MAN RETURN	"	COND_SEE_LEANING_MAN RETURN	" BREAK
CASE	COND_SEE_PHONE_CAR_MAN RETURN	"	COND_SEE_PHONE_CAR_MAN RETURN	" BREAK
CASE	COND_FRANKLIN_AT_PHONE_MAN RETURN	"	COND_FRANKLIN_AT_PHONE_MAN RETURN	" BREAK
CASE	COND_FRANKLIN_AT_CHAD RETURN	"	COND_FRANKLIN_AT_CHAD RETURN	" BREAK
CASE	COND_THERMAL_READY RETURN	"	COND_THERMAL_READY RETURN	" BREAK
CASE	COND_DIA_TREVOR_SEES_CHAD_ENDED RETURN	"	COND_DIA_TREVOR_SEES_CHAD_ENDED RETURN	" BREAK
CASE	COND_SWITCH_BEGUN RETURN	"	COND_SWITCH_BEGUN RETURN	" BREAK
CASE	COND_BLOCK_LISTENING RETURN	"	COND_BLOCK_LISTENING RETURN	" BREAK
CASE	COND_FRANKLIN_SAW_WRONG_GUY RETURN	"	COND_FRANKLIN_SAW_WRONG_GUY RETURN	" BREAK
CASE	COND_DIA_FROM_FRANKLIN_SEEING_CHAD RETURN	"	COND_DIA_FROM_FRANKLIN_SEEING_CHAD RETURN	" BREAK
CASE	COND_FRANKLIN_RUNS_TO_CAR RETURN	"	COND_FRANKLIN_RUNS_TO_CAR RETURN	" BREAK
CASE	COND_WAITING_FOR_NEXT_TARGET RETURN	"	COND_WAITING_FOR_NEXT_TARGET RETURN	" BREAK
CASE	COND_NEXT_TARGET_FOUND RETURN	"	COND_NEXT_TARGET_FOUND RETURN	" BREAK
CASE	COND_INVESTIGATING_NEXT_TARGET RETURN	"	COND_INVESTIGATING_NEXT_TARGET RETURN	" BREAK
CASE	COND_CAR_PARK_ENDS RETURN	"	COND_CAR_PARK_ENDS RETURN	" BREAK
CASE	COND_ZTYPE_STARTS RETURN	"	COND_ZTYPE_STARTS RETURN	" BREAK
CASE	COND_CHAD_INTERRUPTED RETURN	"	COND_CHAD_INTERRUPTED RETURN	" BREAK
CASE	COND_CHAD_CAN_BACK_OFF RETURN	"	COND_CHAD_CAN_BACK_OFF RETURN	" BREAK
CASE	COND_GUN_AIMED_AT_CHAD RETURN	"	COND_GUN_AIMED_AT_CHAD RETURN	" BREAK
CASE	COND_CHAD_DAMAGED_BY_PLAYER RETURN	"	COND_CHAD_DAMAGED_BY_PLAYER RETURN	" BREAK
CASE	COND_CHAD_CAN_RUN RETURN	"	COND_CHAD_CAN_RUN RETURN	" BREAK
CASE	COND_PLAYER_AT_SECURITY RETURN	"	COND_PLAYER_AT_SECURITY RETURN	" BREAK
CASE	COND_PLAYER_200M_FROM_SECURITY RETURN	"	COND_PLAYER_200M_FROM_SECURITY RETURN	" BREAK
CASE	COND_CHAD_PLAYER_IN_GARAGE RETURN	"	COND_CHAD_PLAYER_IN_GARAGE RETURN	" BREAK
CASE	COND_CHAD_RAGDOLL RETURN	"	COND_CHAD_RAGDOLL RETURN	" BREAK
CASE	COND_PLAYER_GOT_IN_CAR RETURN	"	COND_PLAYER_GOT_IN_CAR RETURN	" BREAK
CASE	COND_PROCEED_TO_HANGAR_CONDITIONS_MET RETURN	"	COND_PROCEED_TO_HANGAR_CONDITIONS_MET RETURN	" BREAK
CASE	COND_PHONE_CALL_MADE RETURN	"	COND_PHONE_CALL_MADE RETURN	" BREAK
CASE	COND_NO_DIALOGUE_FOR_TWO_SECONDS RETURN	"	COND_NO_DIALOGUE_FOR_TWO_SECONDS RETURN	" BREAK
CASE	COND_CHAD_DEAD RETURN	"	COND_CHAD_DEAD RETURN	" BREAK
CASE	COND_CHAD_PLAYER_100m_FROM_CAR_PARK RETURN	"	COND_CHAD_PLAYER_100m_FROM_CAR_PARK RETURN	" BREAK
CASE	COND_PLAYER_30m_from_garage RETURN	"	COND_PLAYER_30m_from_garage RETURN	" BREAK
CASE	COND_PLAYER_200m_from_garage RETURN	"	COND_PLAYER_200m_from_garage RETURN	" BREAK
CASE	COND_PLAYER_300m_from_garage RETURN	"	COND_PLAYER_300m_from_garage RETURN	" BREAK
CASE	COND_PLAYER_DEFAULT_LOAD_CUT_RANGE_FROM_HANGAR RETURN	"	COND_PLAYER_DEFAULT_LOAD_CUT_RANGE_FROM_HANGAR RETURN	" BREAK
CASE	COND_WANTED RETURN	"	COND_WANTED RETURN	" BREAK
CASE	COND_HIT_BLIP RETURN	"	COND_HIT_BLIP RETURN	" BREAK
CASE	COND_PLAYER_IN_ZTYPE RETURN	"	COND_PLAYER_IN_ZTYPE RETURN	" BREAK
CASE	COND_DRIVE_TO_INSTRUCTIONS_GIVEN RETURN	"	COND_DRIVE_TO_INSTRUCTIONS_GIVEN RETURN	" BREAK
CASE	COND_CHAD_ESCAPED_IN_ZTYPE RETURN	"	COND_CHAD_ESCAPED_IN_ZTYPE RETURN	" BREAK
CASE	COND_PLAYER_SHOOTING_NEAR_CHAD RETURN	"	COND_PLAYER_SHOOTING_NEAR_CHAD RETURN	" BREAK
CASE	COND_DEVIN_WALKING_IN_TO_ZTYPE RETURN	"	COND_DEVIN_WALKING_IN_TO_ZTYPE RETURN	" BREAK
CASE	COND_CHOPPER_DAMAGED_BY_PLAYER RETURN	"	COND_CHOPPER_DAMAGED_BY_PLAYER RETURN	" BREAK
CASE	COND_DAMAGED_ZTYPE RETURN	"	COND_DAMAGED_ZTYPE RETURN	" BREAK
CASE	COND_PLAYER_7M_FROM_CHAD RETURN	"	COND_PLAYER_7M_FROM_CHAD RETURN	" BREAK
CASE	COND_ZTYPE_IN_HANGAR RETURN	"	COND_ZTYPE_IN_HANGAR RETURN	" BREAK
CASE	COND_ZTYPE_IN_CAR_STOP_RANGE RETURN	"	COND_ZTYPE_IN_CAR_STOP_RANGE RETURN	" BREAK
CASE	COND_ZTYPE_IS_STOPPED RETURN	"	COND_ZTYPE_IS_STOPPED RETURN	" BREAK
CASE	COND_ZTYPE_ENDS RETURN	"	COND_ZTYPE_ENDS RETURN	" BREAK
CASE	COND_EXIT_AIRPORT_START RETURN	"	COND_EXIT_AIRPORT_START RETURN	" BREAK
CASE	COND_PLAY_IN_ZTYPE RETURN	"	COND_PLAY_IN_ZTYPE RETURN	" BREAK
CASE	COND_EXIT_AIRPORT_END RETURN	"	COND_EXIT_AIRPORT_END RETURN	" BREAK


	ENDSWITCH
	RETURN "NO DATA"
ENDFUNC

// ===================================== music ========================================


enum MusicEvents
	mus_null,
	mus_init_scanner, 
	mus_scan_first_ped,
	mus_reload_scan_area_1,
	mus_chad_sees_franklin,
	mus_reload_scan_area_2,
	mus_car_chase_begins,
	mus_reload_chase_Stage,
	mus_chad_drives_down_alley,
	mus_reload_car_park,
	mus_Chad_found,
	mus_switch_to_franklin,
	mus_reload_collec_car_stage,
	mus_reload_drive_car_stage,
	mus_get_car_to_objective,
	mus_stop,
	mus_game_over
endenum

FUNC STRING GET_MUSIC_STRING(MusicEvents eventToPlay)
	SWITCH eventToPlay
		CASE mus_null RETURN "" BREAK
		CASE mus_init_scanner RETURN "CAR2_MISSION_START" BREAK
		CASE mus_scan_first_ped RETURN "CAR2_SCAN_1" BREAK
		CASE mus_reload_scan_area_1 RETURN "car2_scan_2_restart" BREAK
		CASE mus_chad_sees_franklin RETURN "CAR2_GUN" BREAK
		CASE mus_reload_scan_area_2 RETURN "car2_scan_2_restart" BREAK
		CASE mus_car_chase_begins RETURN "CAR2_CHASE_START" BREAK
		CASE mus_reload_chase_Stage RETURN "CAR2_CHASE_RESTART" BREAK
		CASE mus_chad_drives_down_alley RETURN "CAR2_ALLEY" BREAK
		CASE mus_reload_car_park RETURN "CAR2_SWITCH_1_RESTART" BREAK
		CASE mus_Chad_found RETURN "CAR2_CHAD_FOUND" BREAK
		CASE mus_switch_to_franklin RETURN "CAR2_SWITCH_1" BREAK
		CASE mus_reload_collec_car_stage RETURN "CAR2_SWITCH_1_RESTART" BREAK
		CASE mus_reload_drive_car_stage RETURN "CAR2_DRIVE_RESTART" BREAK 
		CASE mus_get_car_to_objective RETURN "CAR2_RADIO_1" BREAK
		case mus_stop RETURN "CAR2_STOP" BREAK
		CASE mus_game_over RETURN "CAR2_MISSION_FAIL" BREAK
	endswitch
	RETURN ""
ENDFUNC

bool bIsThisAMusicChange
bool bMusicChange
string sMusicChange
string sNextMusic
int iControlMusicFlag

PROC PlAY_MUSIC(MusicEvents MusicToPlayNow, MusicEvents nextMusicChangeEvent)
	bMusicChange = TRUe
	iControlMusicFlag=0
	bIsThisAMusicChange = FALSE
	SWITCH MusicToPlayNow
		CASE mus_scan_first_ped FALLTHRU
		CASE mus_chad_sees_franklin FALLTHRU
		CASE mus_car_chase_begins FALLTHRU
		CASE mus_chad_drives_down_alley FALLTHRU
		CASE mus_get_car_to_objective
			bIsThisAMusicChange = TRUE
		BREAK
	ENDSWITCH
	sMusicChange = GET_MUSIC_STRING(MusicToPlayNow)
	sNextMusic = GET_MUSIC_STRING(nextMusicChangeEvent)
ENDPROC

PROC CONTROL_MUSIC()
	IF bMusicChange
		SWITCH iControlMusicFlag
			CASE 0
				IF bIsThisAMusicChange							
					IF PREPARE_MUSIC_EVENT(sMusicChange)
						IF TRIGGER_MUSIC_EVENT(sMusicChange)	
							iControlMusicFlag++
						ENDIF
					ENDIF
				ELSE
					IF TRIGGER_MUSIC_EVENT(sMusicChange)
						iControlMusicFlag++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_STRING_NULL_OR_EMPTY(sNextMusic)
					IF NOT IS_MUSIC_ONESHOT_PLAYING()
						PREPARE_MUSIC_EVENT(sNextMusic)	
						bMusicChange = FALSE
					ENDIF
				ELSE
					bMusicChange = FALSE
				ENDIF							
			BREAK
		ENDSWITCH	
	ELSE
		iControlMusicFlag = 0
	ENDIF	
ENDPROC

PROC CLEANUP_MUSIC()
	bMusicChange = FALSE
	
	string cancelMusicString
	int iMusics
	REPEAT count_of(MusicEvents) iMusics
		cancelMusicString = GET_MUSIC_STRING(int_to_enum(MusicEvents,iMusics))
		IF NOT IS_STRING_NULL_OR_EMPTY(cancelMusicString)
			CANCEL_MUSIC_EVENT(cancelMusicString)
		ENDIF
	endrepeat
ENDPROC

// ================================================ unsepecific functions
func bool is_ped_in_sight(int pedID)
	float tx,ty
	if not IS_PED_INJURED(ped[pedID].id)
		if GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(ped[pedID].id),tx,ty)
			if tx > 0.3 and tx < 0.7 and ty > 0.3 and ty < 0.7
				IF HUD.fFov < 15
					return true
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC

PROC CREATE_CHOPPER_PILOT(bool waitingAtPoliceDept = FALSE)
	IF waitingAtPoliceDept
		initPed(ped_pilot,model_pilot,<<444.9502, -975.7095, 42.6919>>, 233.9611,NULL,VS_DRIVER,pedrole_no_ai)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_pilot].id,TRUE)
		SET_PED_PROP_INDEX(ped[ped_pilot].id,ANCHOR_HEAD,0,0)
		SET_PED_CAN_BE_TARGETTED(ped[ped_pilot].id,FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped[ped_pilot].id,RELGROUPHASH_PLAYER)
	ELSE
		ped_index pedInSeat
		IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
			pedInSeat = GET_PED_IN_VEHICLE_SEAT(vehicle[vehChopper].id,VS_DRIVER)
			IF NOT (IS_PED_INJURED(ped[ped_pilot].id) AND pedInSeat != ped[ped_pilot].id)
			OR NOT DOES_ENTITY_EXIST(ped[ped_pilot].id)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					IF NOT IS_PED_INJURED(pedInSeat)
						SET_PED_INTO_VEHICLE(pedInSeat,vehicle[vehChopper].id,VS_FRONT_RIGHT)
					ENDIF
					initPed(ped_pilot,model_pilot,vNull,0,vehicle[vehChopper].id,VS_DRIVER,pedrole_no_ai)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_pilot].id,TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(ped[ped_pilot].id,RELGROUPHASH_PLAYER)
					SET_PED_PROP_INDEX(ped[ped_pilot].id,ANCHOR_HEAD,0,0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

sequencE_index sequence
proc seq()
	OPEN_SEQUENCE_TASK(sequence)
ENDPROC

proc endSeq(PED_INDEX aPed, bool MakeRepeat=FALSE)
	
	if MakeRepeat
		SET_SEQUENCE_TO_REPEAT(sequence,REPEAT_FOREVER)
	ENDIF
	CLOSE_SEQUENCE_TASK(sequence)
	if not IS_PED_INJURED(aPed)
		TASK_PERFORM_SEQUENCE(aPed,sequence)		
	ENDIF
	CLEAR_SEQUENCE_TASK(sequence)
ENDPROC

bool bMissionMustFail
string sFairReason

PROC Tell_mission_to_fail(string thisFailReason)
	bMissionMustFail = TRUE
	sFairReason = thisFailReason
ENDPROC

FUNC float GET_RELATIVE_ANGLE_FROM_ENTITY_TO_COORD(ENTITY_INDEX ent, vector lookAtCoord)
	if DOES_ENTITY_EXIST(ent)
		if not IS_ENTITY_DEAD(ent)
			float fHeading = GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(ent,FALSE),lookAtCoord)
			RETURN fHeading-GET_ENTITY_HEADING(ent)	
		endif
	ELSE
		SCRIPT_ASSERT("GET_RELATIVE_ANGLE_FROM_ENTITY_TO_COORD(), entity does not exist.")
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC float GET_RELATIVE_ANGLE_FROM_ENTITY_TO_ENTITY(ENTITY_INDEX ent, ENTITY_INDEX entLookAt)
	if DOES_ENTITY_EXIST(ent) and DOES_ENTITY_EXIST(entLookAt)
		RETURN GET_RELATIVE_ANGLE_FROM_ENTITY_TO_COORD(ent,GET_ENTITY_COORDS(entLookAt,FALSE))
	ENDIF
	RETURN 0.0
ENDFUNC

// ============================================= Handling dialogue ===========================================

ped_index pedInSlot[10]
bool pedInSlotSpeaking[10]

func PED_INDEX ped_in_slot(int slot)
	return pedInSlot[slot]
ENDFUNC

bool bLastConvoWithoutSubtitles

struct convStoreStruct
	int speakerNo
	ped_index pedIndex
	string speakerLabel
endstruct
convStoreStruct convStore[4]

proc update_active_speaking_peds(ped_index pedSpeakerOne,ped_index pedSpeakerTwo,ped_index pedSpeakerThree)
	int IJ
	repeat count_of(pedInSlot) iJ
		if pedInSlot[iJ] = pedSpeakerOne or pedInSlot[iJ] = pedSpeakerTwo or pedInSlot[iJ] = pedSpeakerThree			
			pedInSlotSpeaking[iJ] = TRUE
		else
			pedInSlotSpeaking[iJ] = FALSE
		endif
	endrepeat
endproc

PROC CHECK_DIALOGUE_KILL_AT_RANGE()
	int iJ
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		IF HAS_CELLPHONE_CALL_FINISHED()
			repeat count_of(pedInSlot) iJ
				IF pedInSlotSpeaking[iJ] = TRUE
					IF NOT IS_PED_INJURED(pedInSlot[iJ])
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),pedInSlot[iJ]) > 40.0
							//is player making a phone call???
							
						ENDIF
					ENDIF		
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC



PROC KILL_FACE_TO_FACE_CONVERSATION_EXTRA(bool finishLastLine=true)
	//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		
		bLastConvoWithoutSubtitles = FALSE	
		restartLine = ""
		restartRoot = ""
		IF finishLastLine
			//cprintln(debug_trevor3,"ax")
			KILL_FACE_TO_FACE_CONVERSATION()
		ELSE
			//cprintln(debug_trevor3,"bx")
			cprintln(debug_trevor3,"kill conv B ")
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			KILL_ANY_CONVERSATION()
		ENDIF
	//ENDIF	
ENDPROC

FUNC BOOL ADD_PED_FOR_DIALOGUE_EXTRA(int speakerIndex, ped_index speaker, string speakerLabel)
	IF speaker != null
		IF IS_PED_INJURED(speaker)
			//SCRIPT_ASSERT("Speaker is dead")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF speaker !=  MyLocalPedStruct.PedInfo[speakerIndex].Index
		REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,speakerIndex)			
	//	cprintln(debug_Trevor3,"Cleared speaker ",speakerIndex," and added new speaker")
	ENDIF
	
	cprintln(debug_Trevor3,speakerIndex," and added new speaker")
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,speakerIndex,speaker, speakerLabel)

	RETURN TRUE
ENDFUNC

string remlastConv
PROC REMOVE_INACTIVE_SPEAKERS(string thisConv, int iSp1,int iSp2, int iSp3, int iSp4)
	cprintln(debug_trevor3, isp1," ",isp2," ",isp3," ",isp4)
	int i
	IF IS_STRING_NULL_OR_EMPTY(remlastConv)
	OR NOT ARE_STRINGS_EQUAL(remlastConv,thisConv)
		remlastConv = thisConv
		FOR i = 0 to 15
			IF MyLocalPedStruct.PedInfo[i].ActiveInConversation
				IF i != iSp1
				AND i != iSp2
				AND i != iSp3
				AND i != isp4
					CPRINTLN(DEBUG_TREVOR3,"remove pexd at dialogue: ",I," label: ",MyLocalPedStruct.PedInfo[i].VoiceID)
					REMOVE_PED_FOR_DIALOGUE(MyLocalPedStruct,i)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

func bool CREATE_CONVERSATION_EXTRA(enum_conv_types convType, string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, int speakerFour=-1, ped_index pedSpeakerFour=null, string speakerFourLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)
	

		
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
	OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND convType > currentConvType
		convStore[0].speakerNo = speakerOne
		convStore[0].pedIndex = pedSpeakerOne
		convStore[0].speakerLabel = speakerOneLabel
		convStore[1].speakerNo = speakerTwo
		convStore[1].pedIndex = pedSpeakerTwo
		convStore[1].speakerLabel = speakerTwoLabel
		convStore[2].speakerNo = speakerThree
		convStore[2].pedIndex = pedSpeakerThree
		convStore[2].speakerLabel = speakerThreeLabel
		convStore[3].speakerNo = speakerFour
		convStore[3].pedIndex = pedSpeakerFour
		convStore[3].speakerLabel = speakerFourLabel
		
		REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,speakerFour)
				
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
	
		if speakerTwo != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
		if speakerThree != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
		if speakerFour != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
		ENDIF

		IF currentConvType < convType
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF currentConvType = CONVTYPE_UNIMPORTANT
					KILL_FACE_TO_FACE_CONVERSATION()
				ELSE
					cprintln(debug_trevor3,"kill conv D ")
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				currentConvType = CONVTYPE_NULL
			ENDIF
		ENDIF

		IF IS_MESSAGE_BEING_DISPLAYED()
		ANd IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
			cprintln(debug_trevor3,"Dia no subs")
			if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, Label, convPriority,DO_NOT_DISPLAY_SUBTITLES)
				bLastConvoWithoutSubtitles = TRUE
				currentConvType = convType
				iConvoAttempts = 0
				return true
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		else
			//cprintln(debug_trevor3,"Dia subs")
			cprintln(debug_trevor3,"Dia with subs")
			if CREATE_CONVERSATION(MyLocalPedStruct, convBlock, Label, convPriority)
				bLastConvoWithoutSubtitles = FALSE
				restartRoot = ""
				restartLine = ""
				currentConvType = convType
				iConvoAttempts = 0
				return true
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						iConvoAttempts = 0
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		endif
	ENDIF
	return FALSE
ENDFUNC

func bool CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(enum_conv_types convType,string Label, string line, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, int speakerFour=-1, ped_index pedSpeakerFour=null, string speakerFourLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_MEDIUM)	
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
	OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND convType > currentConvType
	//IF NOT IS_MESSAGE_BEING_DISPLAYED()
		REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,speakerFour)
		
		
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
	
		if speakerTwo != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
		if speakerThree != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
		if speakerFour != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
		ENDIF
		
		IF currentConvType < convType
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				cprintln(debug_trevor3,"kill conv E ")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				currentConvType = CONVTYPE_NULL
			ENDIF
		ENDIF

		IF IS_MESSAGE_BEING_DISPLAYED()
		ANd IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct,convBlock,Label,line,convPriority,DO_NOT_DISPLAY_SUBTITLES)		
				currentConvType = convType
				iConvoAttempts = 0
				RETURN TRUE
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct,convBlock,Label,line,convPriority)		
				currentConvType = convType
				iConvoAttempts = 0
				RETURN TRUE
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
endfunc

func bool PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(enum_conv_types convType,string Label, string line, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, int speakerFour=-1, ped_index pedSpeakerFour=null, string speakerFourLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_MEDIUM)
	/*//
	IF IS_THIS_CONVERSATION_ROOT_PLAYING(Label)
	IF line
		RETURN TRUE
	ENDIF*/
	//IF NOT IS_MESSAGE_BEING_DISPLAYED()
	//OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
	OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND convType > currentConvType
		REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,speakerFour)
		
		
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		
		if speakerTwo != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
		if speakerThree != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
		if speakerFour != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
		ENDIF
		
		IF currentConvType < convType
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				cprintln(debug_trevor3,"kill conv F ")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				currentConvType = CONVTYPE_NULL
			ENDIF
		ENDIF
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct,convBlock,Label,line,convPriority)		
				currentConvType = convType
				iConvoAttempts = 0
				RETURN TRUE
			ELSE //backup method in case dialogue just fails to start
				IF iConvoLastAttmpetTime != GET_GAME_TIMER()
					iConvoAttempts++
					iConvoLastAttmpetTime = GET_GAME_TIMER()
					IF iConvoAttempts >= 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	return false
ENDFUNC


PROC ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA(enum_conv_types convType,string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_MEDIUM, enumSubtitlesState subtitlesState = DISPLAY_SUBTITLES)
	if ped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
		IF NOT ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
			EXIT
		ENDIF
	ENDIF
	if speakerTwo != -1
		if ped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
			IF NOT ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	if speakerThree != -1
		if ped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
			IF NOT ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF currentConvType < convType
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			cprintln(debug_trevor3,"kill conv G ")
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			currentConvType = CONVTYPE_NULL
		ENDIF
	ENDIF
	
	currentConvType = convType
	ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER( MyLocalPedStruct, convBlock, Label, convPriority, subtitlesState)
endproc

func bool PLAYER_CALL_CHAR_CELLPHONE_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null)
	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
		if ped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		ENDIF
		if speakerTwo != -1
			if ped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
			ENDIF
		ENDIF
		if speakerThree != -1
			if ped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
			ENDIF
		ENDIF

		//IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct,CHAR_MOLLY,convBlock,Label,CONV_PRIORITY_VERY_HIGH)
		IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct,CHAR_MOLLY,convBlock,Label,CONV_PRIORITY_VERY_HIGH)
			return TRUE
		ENDIF
	ENDIF
	return FALSE
endfunc



// ============================================= SETUP FAILS ============================================



float failTimer
enumFails missionFailing = FAIL_NULL
bool bDelayFail = FALSE
string sFailText


PROC RESET_FAILS()
	bDelayFail = FALSE
	sFailText = ""
	missionFailing = FAIL_NULL
ENDPROC

// =================================== SET UP CONDITIONS ========================================


struct structInstructions
	enumInstructions ins = INS_NULL
	bool bCompleted
	int flag
	int intA
endstruct

structInstructions instr[6]

struct structConditions
	enumconditions condition = COND_NULL
	bool active
	bool returns
	bool wasTrue
	int flag
	int intA
	int intB
endstruct

CONST_INT MAX_CONDITIONS 36
structConditions conditions[MAX_CONDITIONS]

PROC RESET_CONDITIONS()
	int i
	REPEAT COUNT_OF(conditions) i
		conditions[i].condition = COND_NULL
		conditions[i].active = FALSE
		conditions[i].returns = FALSE
		conditions[i].wasTrue = FALSE
		conditions[i].flag = 0
		conditions[i].intA = 0
		conditions[i].intB = 0
	ENDREPEAT
	
ENDPROC



PROC FORCE_INSTRUCTION_STATE(int thisI, enumInstructions thisInstruction, bool setCompleted=TRUE)
	instr[thisI].bCompleted = setCompleted
	instr[thisI].ins = thisInstruction
ENDPROC

FUNC BOOL IS_CONDITION_TRUE(enumconditions conditiontoCheck)


	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 
	
	IF iArray >= 0 and iArray < count_of(conditions)
	
		IF conditions[iArray].condition = conditiontoCheck
			IF conditions[iArray].active
			AND conditions[iArray].returns
				RETURN TRUE
			ENDIF
		ELSE
			
			TEXT_LABEL_63 txt
			txt = ""
			txt += "IS_COND_TRUE() fail:"
			txt += GET_CONDITION_STRING(conditiontoCheck)
			
			SCRIPT_ASSERT(txt)
			
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CONDITION_FALSE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		IF conditions[iArray].active
		AND NOT conditions[iArray].returns
			RETURN TRUE
		ENDIF
	ELSE
		
		SCRIPT_ASSERT("SCRIPT: IS_CONDITION_TRUE() : condition stored in array is not equal to value stored in conditionArrayEntry[]")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL WAS_CONDITION_TRUE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		IF conditions[iArray].active
		AND conditions[iArray].wasTrue
			RETURN TRUE
		ENDIF
	ELSE		
		SCRIPT_ASSERT("SCRIPT: IS_CONDITION_TRUE() : condition stored in array is not equal to value stored in conditionArrayEntry[]")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CONDITION_WAS_TRUE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		conditions[iArray].wasTrue = TRUE		
	ELSE		
		SCRIPT_ASSERT("SCRIPT: IS_CONDITION_TRUE() : condition stored in array is not equal to value stored in conditionArrayEntry[]")
	ENDIF
	
ENDPROC

//======================================================= VARIABLES FOR NEW TEST ON TREVOR GET IN CHOPPER STAGE =============================

// ================================================ Setup Dialogue ============================================

TEXT_LABEL_23 currentConv,lastConv 



struct structDialogue
	enumDialogue dial = DIA_NULL
	bool bCompleted
	bool bStarted
	int flag
	int intA
endstruct

structDialogue dia[MAX_DIALOGUE]

FUNC BOOL HAS_DIALOGUE_FINISHED(int iEntry,enumDialogue thisDia)
	IF dia[iEntry].dial = thisDia
		IF dia[iEntry].bCompleted
			RETURN TRUE
		ENDIF
	ELSE
		IF dia[iEntry].dial != DIA_NULL
			SCRIPT_ASSERT("HAS_DIALOGUE_FINISHED() has wrong entry value")
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// ====================================== ACTION SETUP ====================================



ENUM enumACTIONplayout
	PLAYOUT_ON_TRIGGER,
	PLAYOUT_ON_COND,
	SKIP_SET_TO_COMPLETE
endenum

STRUCT structactions
	enumActions action = ACT_NULL
	bool active
	bool ongoing
	bool completed
	bool needsCleanup
	bool trackCondition
	int flag
	int intA,intB
	float floatA
ENDSTRUCT




structactions actions[MAX_ACTIONS]




// ====================================== ACTION CHECK ========================================

// ================================================ Conditions ==================================

FUNC BOOL SET_CONDITION_STATE(enumconditions thisCondition, bool setReturns, int flag=-1)
	int j

	REPEAT COUNT_OF(conditions) j		
		IF thisCondition = conditions[j].condition
			conditions[j].returns = setReturns				
			
			IF flag != -1
				conditions[j].flag = flag
			ENDIF						
			
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ACTION_COMPLETE(int iEntry, enumActions actionToCheck)
	IF actions[iEntry].action = actionToCheck
		IF actions[iEntry].completed = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ACTION_ONGOING(enumActions actionToCheck)
	int i
	REPEAT COUNT_OF(actions) i
		IF actions[i].action = actionToCheck
			IF actions[i].active = TRUE
			AND actions[i].ongoing = TRUE
				IF actions[i].completed = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC FORCE_END_ACTION(enumActions actionToEnd)
	int i
	REPEAT COUNT_OF(actions) i
		IF actions[i].action = actionToEnd
			IF actions[i].active = TRUE
				actions[i].completed = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_ACTION_NEEDS_CLEANUP(int iEntry)
	actions[iEntry].needsCleanup = TRUE
ENDPROC

PROC SET_ACTION_FLAG(int iArray, enumActions actionToSet, int iNewFlagValue)

	IF actions[iArray].action = actionToSet
		actions[iArray].flag = iNewFlagValue
	ELSE
		TEXT_LABEL_63 txt
		txt = ""
		txt += "SET_ACTION_FLAG() fail:"
	//	txt += GET_ACTION_STRING(actionToSet)
		SCRIPT_ASSERT(txt)
	ENDIF
	
ENDPROC

FUNC INT GET_ACTION_FLAG(int iArray, enumActions actionToSet)

	IF actions[iArray].action = actionToSet
		RETURN actions[iArray].flag
	ELSE
		TEXT_LABEL_63 txt
		txt = ""
		txt += "SET_ACTION_FLAG() fail:"
	//	txt += GET_ACTION_STRING(actionToSet)
		SCRIPT_ASSERT(txt)
	ENDIF
	RETURN -1
	
ENDFUNC

PROC FORCE_ACTION_STATE(int thisI, enumActions thisAction, bool setCompleted=TRUE)
	
	actions[thisI].action = thisAction
	actions[thisI].active = TRUE
	actions[thisI].completed = FALSE		
	actions[thisI].flag = 0
	actions[thisI].needsCleanup = FALSE
	actions[thisI].intA = 0
	actions[thisI].ongoing = FALSE
	actions[thisI].intB = 0
	actions[thisI].floatA = 0
	
	IF setCompleted
		actions[thisI].completed = TRUE
	ENDIF
ENDPROC

FUNC bool andOrReturns(bool &pconditionsTrue, bool &pbracketOpen, andorEnum andOr, enumconditions cond)
	//IF NOT pbracketOpen 
	//OR (NOT pconditionsTrue AND pbracketOpen) 
		SWITCH andOr
			CASE cFORCEtrue
				pconditionsTrue = TRUE
			BREAK
			CASE cIGNORE
				RETURN FALSE
			BREAK
			CASE cIF
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cIFnot
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_FALSE(cond)))
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cOR
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cORbracket
				IF pconditionsTrue
					RETURN FALSE
				ELSE
					pbracketOpen = TRUE
					IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))
						pconditionsTrue = TRUE
					ENDIF
				ENDIF
			BREAK
			CASE cAND
				IF (cond != COND_NULL AND IS_CONDITION_FALSE(cond))
					pconditionsTrue = FALSE
				ENDIF
			BREAK
			CASE cANDNOT
				IF (cond != COND_NULL AND IS_CONDITION_TRUE(cond))
					pconditionsTrue = FALSE
				ENDIF
			BREAK
			CASE cWasTrueNowFalse
				IF WAS_CONDITION_TRUE(cond)
					IF IS_CONDITION_FALSE(COND)
						pconditionsTrue = TRUE
					ENDIF
				ELSE
					IF IS_CONDITION_TRUE(cond)
						SET_CONDITION_WAS_TRUE(COND)
						pconditionsTrue = FALSE
					ENDIF
				ENDIF
			BREAK		
		ENDSWITCH
	
	RETURN TRUE
ENDFUNC



FUNC bool checkANDOR(andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL)
	conditionsTrue = FALSE
	bracketOpen = FALSE
	
	IF andOrReturns(conditionsTrue,bracketOpen,andOr1,cond1)
		IF andOrReturns(conditionsTrue,bracketOpen,andOr2,cond2)
			IF andOrReturns(conditionsTrue,bracketOpen,andOr3,cond3) 
				IF andOrReturns(conditionsTrue,bracketOpen,andOr4,cond4) 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF conditionsTrue
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC



int iPlaybackTime

const_int max_road_blocks 15
struct structRoadBlocks
	vector v1,v2
	float width
	bool on = false
	bool entered
endstruct

structRoadBlocks sroadblocks[max_road_blocks]

proc set_road_blocks(int iStart,int iEnd,bool on=TRUE)
	int iR
	cprintln(debug_trevor3,"set road blocks ",iStart," to ", iEnd, " as ",on)
	FOr iR = iStart to iEnd
		IF iR < count_of(sroadblocks)
			SET_ROADS_IN_ANGLED_AREA(sroadblocks[iR].v1,sroadblocks[iR].v2,sroadblocks[iR].width,FALSE,on)
		ENDIF
	ENDfor
endproc

FUNC BOOL HANDLE_SWITCH_HELI_TO_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam)
	//CDEBUG3LN(DEBUG_MISSION, "HANDLE_SWITCH_HELI_TO_FRANKLIN")

	INT iCurrentNode
	FLOAT fCamPhase
	
	
	
	SWITCH eSwitchCamState
		
		CASE SWITCH_CAM_IDLE
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_START_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE1")
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) 
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 25, TRUE)
				ENDIF
			ENDIF

			IF DOES_ENTITY_EXIST(vehicle[vehHeist].id) 
				IF NOT IS_ENTITY_DEAD(vehicle[vehHeist].id)
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
						SET_ENTITY_INVINCIBLE(vehicle[vehHeist].id, FALSE)
						SET_ENTITY_HEALTH(vehicle[vehHeist].id, 1000)
						SET_VEHICLE_DIRT_LEVEL(vehicle[vehHeist].id, 0.0)	
						SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehicle[vehHeist].id, TRUE)
						SET_VEHICLE_ENGINE_ON(vehicle[vehHeist].id, TRUE, TRUE)
						SET_VEHICLE_DOOR_OPEN(vehicle[vehHeist].id, SC_DOOR_FRONT_LEFT)
					ENDIF
				ENDIF
			ENDIF
	
			
	
			SETUP_SPLINE_CAM_NODE_ARRAY_CHOPPER_TO_FRANKLIN(thisSwitchCam, vehicle[vehChopper].id, ped[ped_franklin].id)
			CREATE_SPLINE_CAM(thisSwitchCam)
			
			SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			bTrevorHeadTrackStarted = FALSE
			bFranklinAnimStarted = FALSE
			bPlayerControlStarted = FALSE
			bWhooshStarted = FALSE
			bWhooshFinished = FALSE
			bMidWhooshPlayed = FALSE
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
			
			
			 

			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
		FALLTHRU

		CASE SWITCH_CAM_PLAYING_SPLINE1
			
		
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")

			iCurrentNode = iCurrentNode
			iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam)
			fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline)
			
			
			
			IF NOT bWhooshStarted
				IF fCamPhase > fWhooshStartPhase
					CDEBUG3LN(DEBUG_MISSION, "Whoosh Starting...")
					ANIMPOSTFX_PLAY("SwitchShortTrevorIn", 0, FALSE) 
					iWhooshSound = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iWhooshSound, "HIT_OUT", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					SET_FORCE_FOOTSTEP_UPDATE (player_ped_id(), true)
					START_AUDIO_SCENE("CAR_2_SWITCH_TO_FRANLIN_IN_GARAGE")
					

					bWhooshStarted = TRUE	
				ENDIF
			ELSE
				IF fCamPhase > fWhooshMidAudioPhase
				AND NOT bMidWhooshPlayed
					bMidWhooshPlayed = TRUE
					PLAY_SOUND_FRONTEND(iWhooshSoundMid, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				ENDIf
			ENDIF
			
			IF NOT bWhooshFinished
				IF fCamPhase > fWhooshStopPhase
					CDEBUG3LN(DEBUG_MISSION, "Whoosh Stopping...")
					STOP_SOUND(iWhooshSound)
					
					IF bPlayerinFirstPerson
						PLAY_SOUND_FRONTEND(iWhooshSound, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					ENDIF
					bWhooshFinished = TRUE	
				ENDIF
			ENDIF
			
			IF NOT bTrevorHeadTrackStarted
				IF fCamPhase > fTrevorHeadTrackPhase
					CDEBUG3LN(DEBUG_MISSION, "Starting Trevor Head Track...")
	
					IF DOES_ENTITY_EXIST(ped[ped_trevor].id)
					AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(ped[ped_trevor].id)
						AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(ped[ped_trevor].id)
							AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
								TASK_LOOK_AT_ENTITY(ped[ped_trevor].id, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							ENDIF
						ENDIF
					ENDIF
					bTrevorHeadTrackStarted = TRUE
				ENDIF
			ENDIF
			
			IF fCamPhase > fFranklinAnimTalkPhase
				SET_CONDITION_STATE(COND_DIA_FROM_FRANKLIN_SEEING_CHAD, TRUE)
			ENDIF
			
			IF NOT bFranklinAnimStarted
				IF fCamPhase > fFranklinAnimStartPhase
					CDEBUG3LN(DEBUG_MISSION, "Starting Franklin Anim...")
					
					
					STOP_AUDIO_SCENE("CAR_2_USE_INFRARED")
					
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())										
								SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinAimingHeading)
								
								SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
								SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
								SET_PLAYER_SIMULATE_AIMING(PLAYER_ID(), TRUE)

								TASK_PLAY_ANIM(PLAYER_PED_ID(), "misscarsteal2switch", "_ground_franklin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
							ENDIF
						ENDIF
					ENDIF
					bFranklinAnimStarted = TRUE
				ENDIF
			ENDIF
			
			IF NOT bPlayerControlStarted
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "misscarsteal2switch", "_ground_franklin")
								IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "misscarsteal2switch", "_ground_franklin") >= 0.99
									cprintln(debug_trevor3, "Starting Player Control b...")																		
									
									//CLEAR_PED_TASKS(PLAYER_PED_ID())
									
									IF bPlayerinFirstPerson
										TASK_AIM_GUN_AT_ENTITY(PLAYER_PED_ID(), ped[ped_chad].id, INFINITE_TASK_TIME, TRUE)
									ELSE
										TASK_AIM_GUN_AT_COORD(PLAYER_PED_ID(), <<-1320.3032, -219.4609, 50.5926>>, INFINITE_TASK_TIME, TRUE)
									ENDIF
									
									
									FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING, TRUE, DEFAULT, TRUE)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									
									DISPLAY_HUD(TRUE)
									DISPLAY_RADAR(TRUE)
									SET_WIDESCREEN_BORDERS(FALSE, 500)	
									//cprintln(debug_trevor3,"PLAYER HAS THE BRIDGE")
																	
									bPlayerControlStarted = TRUE
									/*
									STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
									IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
										IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
											DESTROY_CAM(thisSwitchCam.ciSpline)
										ENDIF
									ENDIF
									DESTROY_ALL_CAMS()
								
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
									*/
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bPlayerControlStarted
				SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
			ENDIF
			
		//	IF fCamPhase >= 0.9
		//		cprintln(debug_trevor3,"PUSH IN ",fCamPhase)
		//		HANDLE_PUSH_IN(s_cs2_pushin)
		//	ENDIF
			
			cprintln(debug_trevor3,"switch phase = ",fCamPhase)
			
			bool bEndSwitch
			
			IF bPlayerinFirstPerson
				cprintln(debug_trevor3,"player in first person")
				IF fCamPhase >= 0.8
					IF HANDLE_PUSH_IN(s_cs2_pushin)
						bEndSwitch = TRUE
					ENDIF
				ENDIF
			ELSE
				cprintln(debug_trevor3,"player not in first person")
				IF fCamPhase >= 1.0
				OR bPlayerControlStarted
					bEndSwitch = TRUE
				ENDIF
			ENDIF
			
			IF bEndSwitch = TRUE
				cprintln(debug_trevor3,"handle push in complete")
				SET_TIME_SCALE(1.0)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				IF NOT bPlayerinFirstPerson
					ANIMPOSTFX_PLAY("CamPushInFranklin", 0, FALSE) //previously SwitchSceneFranklin
					PLAY_SOUND_FRONTEND(iWhooshSound, "Hit_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")		
				ENDIF
						
				IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
					IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
						DESTROY_CAM(thisSwitchCam.ciSpline)
					ENDIF
				ENDIF
				DESTROY_ALL_CAMS()
			
				RENDER_SCRIPT_CAMS(FALSE, TRUE)																								
				SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
				SET_FORCE_FOOTSTEP_UPDATE (player_ped_id(), FALSE)
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				SET_WIDESCREEN_BORDERS(FALSE, 500)
				
				STOP_AUDIO_SCENE("CAR_2_SWITCH_TO_FRANLIN_IN_GARAGE")
				eSwitchCamState = SWITCH_CAM_IDLE
				RESET_PUSH_IN(s_cs2_pushin)
				RETURN TRUE
			ELSE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			ENDIF
		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC ACTION(int thisI, enumActions thisAction, enumACTIONplayout playout=PLAYOUT_ON_TRIGGER, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL)			
	#if IS_DEBUG_BUILD
		IF thisI >= MAX_ACTIONS
			SCRIPT_ASSERT("SCRIPT: INCREASE MAX_ACTIONS")
		ENDIF
	#endif
	IF actions[thisI].action != thisAction
	OR actions[thisI].active = FALSE
		actions[thisI].action = thisAction
		actions[thisI].active = TRUE
		actions[thisI].completed = FALSE		
		actions[thisI].flag = 0
		actions[thisI].ongoing = FALSE
		actions[thisI].needsCleanup = FALSE
		actions[thisI].intA = 0
	ENDIF
	bool actionConditionCheck
	
	IF actions[thisI].ongoing
	AND playout = PLAYOUT_ON_TRIGGER
		actionConditionCheck = TRUE
	ELSE
		actionConditionCheck = checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3,andOr4,cond4)
	ENDIF
	
	IF playout = PLAYOUT_ON_COND
	AND NOT actionConditionCheck
		actions[thisI].ongoing = FALSE
	ENDIF
	
		
	IF NOT bMakeMissionFail
	OR actions[thisI].flag = CLEANUP
		
		IF actions[thisI].completed = FALSE
		OR actions[thisI].flag = CLEANUP
			IF actionConditionCheck = TRUE
				IF NOT actions[thisI].ongoing
					actions[thisI].ongoing = TRUE
				ENDIF
				SWITCH actions[thisI].action	
				
					
				
					CASE ACT_SPAWN_COPS
						SWITCH actions[thisI].flag
							CASE CLEANUP
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
							/*
								IF NOT IS_PED_INJURED(ped[ped_cop1].id)
									SET_PED_AS_NO_LONGER_NEEDED(ped[ped_cop1].id)
								ENDIF
								IF NOT IS_PED_INJURED(ped[ped_cop2].id)
									SET_PED_AS_NO_LONGER_NEEDED(ped[ped_cop2].id)
								ENDIF
								IF NOT IS_PED_INJURED(ped[ped_cop3].id)
									SET_PED_AS_NO_LONGER_NEEDED(ped[ped_cop3].id)
								ENDIF
								IF NOT IS_PED_INJURED(ped[ped_cop4].id)
									SET_PED_AS_NO_LONGER_NEEDED(ped[ped_cop4].id)
								ENDIF
								*/
							BREAK
							case 100 //this is triggered from the replay function to let this action know that a replay is in progress.
								ped[ped_cop1].id = g_sTriggerSceneAssets.ped[0]
								ped[ped_cop2].id = g_sTriggerSceneAssets.ped[1]
								ped[ped_cop3].id = g_sTriggerSceneAssets.ped[2]
								ped[ped_cop4].id = g_sTriggerSceneAssets.ped[6]
								ped[ped_civ1].id = g_sTriggerSceneAssets.ped[3]
								ped[ped_civ2].id = g_sTriggerSceneAssets.ped[4]
								ped[ped_civ3].id = g_sTriggerSceneAssets.ped[5]
								
								SET_ENTITY_AS_MISSION_ENTITY(ped[ped_cop1].id,TRUE,TRUE)
								SET_ENTITY_AS_MISSION_ENTITY(ped[ped_cop2].id,TRUE,TRUE)
								SET_ENTITY_AS_MISSION_ENTITY(ped[ped_cop3].id,TRUE,TRUE)
								SET_ENTITY_AS_MISSION_ENTITY(ped[ped_cop4].id,TRUE,TRUE)
								SET_ENTITY_AS_MISSION_ENTITY(ped[ped_civ1].id,TRUE,TRUE)
								SET_ENTITY_AS_MISSION_ENTITY(ped[ped_civ2].id,TRUE,TRUE)
								SET_ENTITY_AS_MISSION_ENTITY(ped[ped_civ3].id,TRUE,TRUE)
								cprintln(debug_trevor3,"Flag = 100?")
								
							break
							CASE 0
								SET_ACTION_NEEDS_CLEANUP(thisI)
								actions[thisI].flag++
							BREAK
							
							CASE 1
								int_police = GET_INTERIOR_AT_COORDS(<<441.02, -978.93, 30.69>>)
									
								IF IS_VALID_INTERIOR(int_police)
									PIN_INTERIOR_IN_MEMORY(int_police)
									actions[thisI].flag++
								endif
							BREAK
							
							case 2
								IF IS_INTERIOR_READY(int_police)			
									SET_INTERIOR_ACTIVE(int_police, TRUE)	
									actions[thisI].flag++
								ENDIF
							BREAK
							
							case 3
									IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
										CPRINTLN(debug_trevor3,"peds taken from lead_in script")
										ped[ped_cop1].id = g_sTriggerSceneAssets.ped[0]
										ped[ped_cop2].id = g_sTriggerSceneAssets.ped[1]
										ped[ped_cop3].id = g_sTriggerSceneAssets.ped[2]
										ped[ped_cop4].id = g_sTriggerSceneAssets.ped[6]
										ped[ped_civ1].id = g_sTriggerSceneAssets.ped[3]
										ped[ped_civ2].id = g_sTriggerSceneAssets.ped[4]
										ped[ped_civ3].id = g_sTriggerSceneAssets.ped[5]
										
										SET_ENTITY_AS_MISSION_ENTITY(ped[ped_cop1].id,TRUE,TRUE)
										SET_ENTITY_AS_MISSION_ENTITY(ped[ped_cop2].id,TRUE,TRUE)
										SET_ENTITY_AS_MISSION_ENTITY(ped[ped_cop3].id,TRUE,TRUE)
										SET_ENTITY_AS_MISSION_ENTITY(ped[ped_cop4].id,TRUE,TRUE)
										SET_ENTITY_AS_MISSION_ENTITY(ped[ped_civ1].id,TRUE,TRUE)
										SET_ENTITY_AS_MISSION_ENTITY(ped[ped_civ2].id,TRUE,TRUE)
										SET_ENTITY_AS_MISSION_ENTITY(ped[ped_civ3].id,TRUE,TRUE)
									ELSE
					
										CPRINTLN(debug_trevor3,"Mission made peds")
										ped[ped_cop1].id = CREATE_PED(PEDTYPE_COP,S_M_Y_COP_01,<<441.0267, -978.2040, 29.6895>>,192)
										ped[ped_cop2].id = CREATE_PED(PEDTYPE_COP,S_M_Y_COP_01,<< 440.2506, -975.6328, 29.6895 >>,356)
										ped[ped_cop3].id = CREATE_PED(PEDTYPE_COP,S_M_Y_COP_01,<<454.1487, -979.8940, 29.6896>>, 105.1729)
										ped[ped_cop4].id = CREATE_PED(PEDTYPE_COP,S_M_Y_COP_01,<<450.2071, -992.9072, 29.6896>>, 316.4481)
										ped[ped_civ1].id = CREATE_PED(PEDTYPE_CIVMALE,A_M_Y_GenStreet_02,<<436.9079, -986.8186, 29.6895>>, 71.5386)
										ped[ped_civ2].id = CREATE_PED(PEDTYPE_CIVMALE,A_M_Y_GenStreet_02,<<443.4680, -981.7770, 29.6895>>,30)
										ped[ped_civ3].id = CREATE_PED(PEDTYPE_CIVMALE,A_M_Y_GenStreet_02,<<444.9140, -988.1146, 29.6895>>, 71.5386)
									ENDIF
									
									SET_PED_COMPONENT_VARIATION(ped[ped_cop1].id, INT_TO_ENUM(PED_COMPONENT,0), 2, 1, 0) //(head)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop1].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop1].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop1].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop1].id, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop1].id, INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)
									
									SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
							
									SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
									SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)
									
									SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_cop4].id)
									SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ped[ped_cop1].id,FALSE)
							
									STOP_PED_SPEAKING(ped[ped_cop1].id,TRUE)
									STOP_PED_SPEAKING(ped[ped_cop2].id,TRUE)
									STOP_PED_SPEAKING(ped[ped_cop3].id,TRUE)
									STOP_PED_SPEAKING(ped[ped_cop4].id,TRUE)
							
									GIVE_WEAPON_TO_PED(ped[ped_cop1].id,WEAPONTYPE_PISTOL,infinite_ammo)
									GIVE_WEAPON_TO_PED(ped[ped_cop2].id,WEAPONTYPE_PISTOL,infinite_ammo)
									GIVE_WEAPON_TO_PED(ped[ped_cop3].id,WEAPONTYPE_PISTOL,infinite_ammo)
									GIVE_WEAPON_TO_PED(ped[ped_cop4].id,WEAPONTYPE_PISTOL,infinite_ammo)
									TASK_START_SCENARIO_IN_PLACE(ped[ped_cop1].id,"WORLD_HUMAN_HANG_OUT_STREET")
									TASK_START_SCENARIO_IN_PLACE(ped[ped_cop2].id,"WORLD_HUMAN_CLIPBOARD")
									TASK_START_SCENARIO_IN_PLACE(ped[ped_cop3].id,"WORLD_HUMAN_CLIPBOARD")
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_cop1].id)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_cop2].id)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_cop3].id)
									//create bums
						
					
									//REQUEST_MODEL(A_M_Y_GenStreet_02)
						
									
							
									
									TASK_START_SCENARIO_IN_PLACE(ped[ped_civ1].id,"WORLD_HUMAN_BUM_STANDING")
									TASK_START_SCENARIO_IN_PLACE(ped[ped_civ2].id,"WORLD_HUMAN_STAND_IMPATIENT")							
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_civ1].id)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_civ2].id)
									
									
									INT scenei
									scenei = CREATE_SYNCHRONIZED_SCENE(<< 447.140, -988.574, 29.688 >>, << -0.000, 0.000, -80.150 >>)
									TASK_SYNCHRONIZED_SCENE (ped[ped_civ3].id, scenei, "missheistdockssetup1ig_13@start_idle", "guard_beatup_startidle_dockworker", INSTANT_BLEND_IN, -2,SYNCED_SCENE_USE_PHYSICS )
									TASK_SYNCHRONIZED_SCENE (ped[ped_cop4].id, scenei, "missheistdockssetup1ig_13@start_idle", "guard_beatup_startidle_guard1", INSTANT_BLEND_IN, -2,SYNCED_SCENE_USE_PHYSICS )
									SET_SYNCHRONIZED_SCENE_LOOPED(scenei,true)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_civ3].id)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_cop4].id)
																		
									SET_PED_CAN_RAGDOLL(ped[ped_civ3].id,FALSE)
									
									actions[thisI].completed = TRUE
							
							BREAK
						ENDSWITCH							
					BREAK
					
					
					CASE ACT_LOOK_AT_PLAYER_ENTRY
						IF NOT IS_PED_INJURED(ped[ped_cop1].id)
							TASK_LOOK_AT_ENTITY(ped[ped_cop1].id,player_ped_id(),14000)
							TASK_LOOK_AT_ENTITY(player_ped_id(),ped[ped_cop1].id,14000)
						ENDIF
						actions[thisI].completed = TRUE
					BREAK
					
					CASE ACT_POINT_TO_STAITRS
						SWITCH actions[thisI].flag
							CASE 0						
								actions[thisI].flag++
							BREAK
							CASE 1
								IF IS_CONV_ROOT_PLAYING("cs2_cop1")
									IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() = 0
										REPLAY_RECORD_BACK_FOR_TIME(7.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
										actions[thisI].flag++
									ENDIF
								ENDIF
							BREAK
							case 2
								IF NOT IS_PED_INJURED(ped[ped_cop1].id)
									CLEAR_PED_TASKS(ped[ped_cop1].id)
									seq()
									TASK_PLAY_ANIM(null,"misscarsteal2officer","officer_point")
									TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_HANG_OUT_STREET")
									endseq(ped[ped_cop1].id)
									actions[thisI].completed = TRUE
								ENDIF
							BREAK
						
						ENDSWITCH
					BREAK
					
					CASE ACT_WALK_TO_STAIRS
						SWITCH actions[thisI].flag
							CASE 0
								IF NOT IS_PED_INJURED(ped[ped_cop1].id)
									CLEAR_PED_TASKS(ped[ped_cop1].id)
									seq()
									TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 464.1900, -985.8955, 29.6897 >>,pedmove_walk,40000)								 
									TASK_LOOK_AT_ENTITY(null,player_ped_id(),10000)
									TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),5000)
									
									endseq(ped[ped_cop1].id)
									
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
								IF IS_CONDITION_TRUE(COND_PLAYER_IN_LOCKER_ROOM)
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 2
								IF IS_CONDITION_FALSE(COND_PLAYER_IN_LOCKER_ROOM)
									actions[thisI].flag=0
								ENDIF
							BREAK
						
						ENDSWITCH
					BREAK
					
					CASE ACT_COPS_ALERTED
						FORCE_END_ACTION(ACT_WALK_TO_STAIRS)
						SET_WANTED_LEVEL_MULTIPLIER(1.0)
						SET_MAX_WANTED_LEVEL(5)
						SET_PLAYER_WANTED_LEVEL(player_id(),3)
						SET_PLAYER_WANTED_LEVEL_NOW(player_id())
						IF NOT IS_PED_INJURED(ped[ped_cop1].id)
							SET_PED_AS_NO_LONGER_NEEDED(ped[ped_cop1].id)
						ENDIF
						actions[thisI].completed = TRUE
					BREAK
					
					CASE ACT_SPAWN_CHOPPER
						switch actions[thisI].flag
							case 0
								request_model(polmav)
								request_model(model_pilot)								
								actions[thisI].flag++
							BREAK
							case 1
								if HAS_MODEL_LOADED(POLMAV)
								and HAS_MODEL_LOADED(model_pilot)
									REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
									vehicle[vehChopper].id = create_Vehicle(polmav,<< 449.0350, -982.4875, 42.6919 >>, 357.5347)
									SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
									SET_VEHICLE_ENGINE_ON(vehicle[vehChopper].id,false,true)
									SET_VEHICLE_DOORS_LOCKED(vehicle[vehChopper].id,VEHICLELOCK_UNLOCKED)
									SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)		
									SET_VEHICLE_ON_GROUND_PROPERLY(vehicle[vehChopper].id)
									CREATE_CHOPPER_PILOT(TRUE)
									seq()
									TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_AA_SMOKE")
									TASK_LOOK_AT_COORD(null,<< 453.9144, -948.0502, 44.2760 >>,-1)
									TASK_PAUSE(null,99999999)							
									endseq(ped[ped_pilot].id)
									actions[thisI].completed = TRUE							
								endif
							break
						ENDSWITCH
					BREAK
					
					CASE ACT_PED_AT_VENDING_MACHINE_LEAVES
						IF NOT IS_PED_INJURED(ped[ped_civ1].id) //vending machine ped
							IF g_bCurrentlyUsingVendingMachine
								IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_civ1].id,player_ped_id()) < 5.0
									
									TASK_USE_NEAREST_SCENARIO_TO_COORD(ped[ped_civ1].id,GET_ENTITY_COORDS(ped[ped_civ1].id),30.0)
									//TASK_WANDER_STANDARD(ped[ped_civ1].id)
									actions[thisI].completed = TRUE
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					
					
					CASE ACT_COP_RETURNS_TO_FRONT_DESK	
						
						seq()
							TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 441.2560, -977.9714, 29.6895 >>,pedmove_walk)
							TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_HANG_OUT_STREET")
						endseq(ped[ped_cop1].id)
						actions[thisI].completed = TRUE	
						FORCE_ACTION_STATE(3,ACT_WALK_TO_STAIRS)
						
					BREAK									
					
					CASE ACT_PLAYER_GETS_IN_CHOPPER
						switch actions[thisI].flag	
							case 0
								cprintln(debug_trevor3,"Trevor gets in chopper. Awesome")
								REPLAY_RECORD_BACK_FOR_TIME(8.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								SET_PLAYER_CONTROL(PLAYER_ID(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
								TASK_ENTER_VEHICLE(player_ped_id(),vehicle[vehChopper].id,DEFAULT_TIME_BEFORE_WARP,VS_FRONT_RIGHT,pedmove_run,ECF_USE_RIGHT_ENTRY)
								IF DOES_BLIP_EXIST(blipTarget)
									remove_blip(blipTarget)
								ENDIF
								CLEAR_PRINTS()
								IF NOT IS_PED_INJURED(ped[ped_pilot].id)
									TASK_LOOK_AT_ENTITY(ped[ped_pilot].id,PLAYER_PED_ID(),6000)
								ENDIF
								DISABLE_CELLPHONE(TRUE) 
								actions[thisI].flag++
							BREAK
							CASE 1
								//added to fix bug 2027162
								IF GET_SCRIPT_TASK_STATUS(player_ped_id(),SCRIPT_TASK_ENTER_VEHICLE) = FINISHED_TASK
								OR GET_SCRIPT_TASK_STATUS(player_ped_id(),SCRIPT_TASK_ENTER_VEHICLE) = DORMANT_TASK
									TASK_ENTER_VEHICLE(player_ped_id(),vehicle[vehChopper].id,DEFAULT_TIME_BEFORE_WARP,VS_FRONT_RIGHT,pedmove_run,ECF_USE_RIGHT_ENTRY)
								ENDIF
								
								IF GET_SCRIPT_TASK_STATUS(player_ped_id(),SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
									IF IS_PED_IN_ANY_VEHICLE(player_ped_id(),TRUE)
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH								
					BREAK
					
					CASE ACT_PILOT_FLIES_OFF
						cprintln(debug_trevor3,"HELI TEST")
						IF NOT IS_PED_INJURED(PED[ped_pilot].id)
						AND IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
							seq()							
							TASK_HELI_MISSION(null,vehicle[vehChopper].id,null,null,<< 447.0187, -982.2263, 73.6343 >>,MISSION_GOTO,15.0,1.0,0,130,30)							
							TASK_HELI_MISSION(null,vehicle[vehChopper].id,null,null,<< 727.9601, -1085.0781, 131.2213 >>,MISSION_CIRCLE,15.0,1.0,0,130,30)
							endseq(PED[ped_pilot].id)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_PILOT_LOOKS_AT_TREVOR
						IF NOT IS_PED_INJURED(PED[ped_pilot].id)
						AND IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
							seq()
								TASK_LOOK_AT_ENTITY(null,player_ped_id(),15000)
								TASK_PAUSE(null,1000)
								TASK_ENTER_VEHICLE(null,vehicle[vehChopper].id,DEFAULT_TIME_BEFORE_WARP,vs_driver,PEDMOVE_WALK)
								TASK_HELI_MISSION(null,vehicle[vehChopper].id,null,null,GET_ENTITY_COORDS(vehicle[vehChopper].id),MISSION_LAND_AND_WAIT,0,5,0,0,0)
							endseq(PED[ped_pilot].id)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					 
					CASE ACT_COP_WAITS_FOR_PLAYER
						
						IF NOT IS_PED_INJURED(PED[ped_cop1].id)
							seq()
							TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<453.0859, -985.3833, 29.6896>>,pedmove_run)
							TASK_TURN_PED_TO_FACE_ENTITY(null,player_peD_id(),0)
							endseq(PED[ped_cop1].id)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
			
					
					CASE ACT_CLEAR_SWITCH_INSTRUCTIONS
						IF switchState = SWITCH_SWAP_PLAYER_PEDS
							CLEAR_PRINTS()
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_DO_TREVOR_STAND
						switch actions[thisI].flag	
							case 0
								
								IF HAS_ANIM_DICT_LOADED("switch@trevor@head_in_sink")
									actions[thisI].flag=2
								ENDIF
							BREAK
							CASE 3
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								SET_PLAYER_CONTROL(player_id(),false)
								DESTROY_ALL_CAMS()
								
									CAMERA_INDEX splineCamera
									splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
									ADD_CAM_SPLINE_NODE(splineCamera,<<417.4168, -964.0216, 31.2312>>, <<22.2372, -0.0000, -121.6787>>,6500)
									ADD_CAM_SPLINE_NODE(splineCamera,<<417.3173, -965.3885, 31.2487>>, <<-17.7868, -0.0000, -164.7233>>,4500)
									SET_CAM_SPLINE_SMOOTHING_STYLE(splineCamera,CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
									SET_CAM_ACTIVE(splineCamera, TRUE)
									SET_CAM_FOV(splinecamera,46.25)
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
									
									
									actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 419.776, -969.354, 29.529 >>,<< -0.000, 0.000, -108.720 >>)//<< 1408.917, -2045.323, 51.496 >>, << 0.000, 0.000, 144.500 >>)
									TASK_SYNCHRONIZED_SCENE (player_ped_id(), actions[thisI].intA, "switch@trevor@head_in_sink", "trev_sink_exit", INSTANT_BLEND_IN, -2)
									SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intA,FALSE)
									SET_SYNCHRONIZED_SCENE_RATE(actions[thisI].intA,0)
									CLEAR_area(<<418.929260,-969.588623,29.411676>>,4,true)
									
								//	SET_GAMEPLAY_CAM_RELATIVE_HEADING(-45)
								//	SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
									
									actions[thisI].flag=4
									actions[thisI].intB = GET_GAME_TIMER() + 1800
									RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<408.865997,-964.756531,26.657490>>, <<407.229065,-989.141602,34.579247>>, 24.812500, <<407.1000, -983.7553, 28.2668>>, 233.2444,<<4,10,6>>)
							BREAK						
							
							CASE 4
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
								IF GET_GAME_TIMER() > actions[thisI].intB
								//OR IS_PLAYER_PRESSING_A_CONTROL_BUTTON()								
									
									SET_SYNCHRONIZED_SCENE_RATE(actions[thisI].intA,1.0)	
									actions[thisI].flag++
							
									//IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)								
									//	SET_GAMEPLAY_ENTITY_HINT(vehicle[vehChopper].id,<<0,0,0>>,TRUE,10000,10000)								
									//ENDIF
								ENDIF
							BREAK
							
							
							CASE 5
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
								IF (IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA) AND GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intA) > 0.67 AND IS_PLAYER_PRESSING_A_CONTROL_BUTTON())
								OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
										
										CLEAR_PED_TASKS(player_ped_id())
										SET_PLAYER_CONTROL(player_id(),true)
										STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
										DESTROY_ALL_CAMS()
										IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)								
											SET_GAMEPLAY_ENTITY_HINT(vehicle[vehChopper].id,<<0,0,-7>>,TRUE,6000,5000,4000)								
										ENDIF
										actions[thisI].completed = TRUE
										
									
								ENDIF
							BREAK
							CASE 6
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
									REMOVE_ANIM_DICT("switch@trevor@head_in_sink")
								ENDIF
							break
							
						ENDSWITCH
					BREAK
					CASE ACT_LOAD_STAND_ANIM
						
						DISABLE_CONTROL_ACTION(CAMERA_CONTROL,INPUT_NEXT_CAMERA)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_NEXT_CAMERA)
						//DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_NEXT_CAMERA)
						///DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_SELECT)
						switch actions[thisI].flag
							case 0
								REQUEST_ANIM_DICT("misscarsteal2franklin_on_bench")
								REQUEST_ANIM_DICT("AMB@PROP_HUMAN_SEAT_CHAIR@MALE@LEFT_ELBOW_ON_KNEE@BASE")
								REQUEST_MODEL(model_pilot)
								REQUEST_MODEL(polmav)
								actions[thisI].flag++
							BREAK
							CASE 1
								IF HAS_ANIM_DICT_LOADED("misscarsteal2franklin_on_bench")
								AND HAS_ANIM_DICT_LOADED("AMB@PROP_HUMAN_SEAT_CHAIR@MALE@LEFT_ELBOW_ON_KNEE@BASE")
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 3
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
								SET_PLAYER_CONTROL(player_id(),false)
								DESTROY_ALL_CAMS()
								
									CAMERA_INDEX splineCamera
									splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	//CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<< 1390.3833, 1163.0212, 113.8014 >>, << -4.1252, 0.0000, -113.4193 >>,65,true)						
									ADD_CAM_SPLINE_NODE(splineCamera,<<1411.6434, -2045.3735, 52.0791>>, <<-4.6833, 0.1650, 106.0461>>,6500)
									ADD_CAM_SPLINE_NODE(splineCamera,<<1411.8749, -2045.2078, 51.9994>>, <<7.3850, 0.1650, 120.4620>>,4000)
									SET_CAM_SPLINE_SMOOTHING_STYLE(splineCamera,CAM_SPLINE_SLOW_OUT_SMOOTH)
									SET_CAM_ACTIVE(splineCamera, TRUE)
									SET_CAM_FOV(splinecamera,50)
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
									
	
									actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 1409.681, -2045.772, 51.500 >>, << 0.000, 0.000, 144.500 >>)//<< 1408.917, -2045.323, 51.496 >>, << 0.000, 0.000, 144.500 >>)
									TASK_SYNCHRONIZED_SCENE (player_ped_id(), actions[thisI].intA, "AMB@PROP_HUMAN_SEAT_CHAIR@MALE@LEFT_ELBOW_ON_KNEE@BASE", "base", INSTANT_BLEND_IN, SLOW_BLEND_OUT)
									SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intA,TRUE)
									
								//	SET_GAMEPLAY_CAM_RELATIVE_HEADING(-45)
								//	SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
									
									actions[thisI].flag=4
									actions[thisI].intB = GET_GAME_TIMER() + 2000
								
							BREAK
							
							case 35
								IF DOES_ENTITY_EXIST(vehicle[vehChopper].id)
									SET_GAMEPLAY_ENTITY_HINT(vehicle[vehChopper].id,<<0,0,0>>,true,10000,6000)
									actions[thisI].flag=4
								ENDIF
							BREAK
							
							CASE 4
								DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
								IF GET_GAME_TIMER() > actions[thisI].intB
								OR IS_PLAYER_PRESSING_A_CONTROL_BUTTON()								
									actions[thisI].intB = CREATE_SYNCHRONIZED_SCENE(<< 1409.681, -2045.772, 51.500 >>, << 0.000, 0.000, 144.500 >>)//<< 1408.917, -2045.323, 51.496 >>, << 0.000, 0.000, 144.500 >>)
									TASK_SYNCHRONIZED_SCENE (player_ped_id(), actions[thisI].intB, "misscarsteal2franklin_on_bench", "exit_forward", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_ON_ABORT_STOP_SCENE)	
									actions[thisI].flag++
							
									//IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)								
									//	SET_GAMEPLAY_ENTITY_HINT(vehicle[vehChopper].id,<<0,0,0>>,TRUE,10000,10000)								
									//ENDIF
								ENDIF
							BREAK							
							
							CASE 5
								DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
								IF (IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intB) AND GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intB) > 0.97)
								OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intB)
										
										CLEAR_PED_TASKS(player_ped_id())
										SET_PLAYER_CONTROL(player_id(),true)
										actions[thisI].flag++
									
								ENDIF
							BREAK
							CASE 6
								//IF GET_GAME_TIMER() > actions[thisI].intA
							//	IF IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_LR)
							//	OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_UD)
								DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
					
							
								IF IS_PLAYER_PRESSING_A_CONTROL_BUTTON()
										STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
										DESTROY_ALL_CAMS()
										IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)								
											SET_GAMEPLAY_ENTITY_HINT(vehicle[vehChopper].id,<<0,0,-7>>,TRUE,6000,5000,4000)								
										ENDIF
										actions[thisI].completed = TRUE
								
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
				
					
					// ===================================== LEARN TO SCAN ===================================
					
					CASE ACT_TURN_ON_SCANNER
						SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[vehChopper].id,TRUE, vehicle[vehChopper].id)
						SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",TRUE)
						DISABLE_SELECTOR()
						IF DOES_ENTITY_EXIST(ped[ped_pilot].id)
							DELETE_PED(ped[ped_pilot].id)
							SET_MODEL_AS_NO_LONGER_NEEDED(model_pilot)
						ENDIF
						
						if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
							FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,FALSE)
							SET_VEHICLE_ENGINE_ON(vehicle[vehChopper].id,true,true)									
							SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)							
						ENDIF

/*
						IF playAsTrevor
							POINT_CHOPPER_CAM_AT_COORD(HUD,<< 1353.0531, -2064.5781, 54.0574 >>)
						ELSE
							POINT_CHOPPER_CAM_AT_COORD(HUD,<< -212.2150, 639.5228, 191.7123 >>)
						ENDIF*/
						
						HUD.bMoveChopper = TRUE
						
						SET_ASSET_STAGE(ASSETS_STAGE_IN_CHOPPER)
						START_AUDIO_SCENE("CAR_2_HELI_CAM_TUTORIAL")
						kill_event(events_chopper_at_Start)
						actions[thisI].completed = TRUE
					BREAK
					
					CASE ACT_MUSIC_SCANNER_ON
						PLAY_MUSIC(mus_init_scanner,mus_scan_first_ped)
						actions[thisI].completed = TRUE
					BREAK
					
					CASE ACT_FRANKLIN_LOOKS_AT_CHOPPER
						if not IS_PED_INJURED(ped[ped_franklin].id)
						OR actions[thisI].flag = CLEANUP
							SWITCH actions[thisI].flag
								case CLEANUP
									REMOVE_ANIM_DICT("misscarsteal2")
									actions[thisI].completed = TRUE
								BREAK
								case 0
									REQUEST_ANIM_DICT("misscarsteal2")
									actions[thisI].flag++
								break
								case 1
									if HAS_ANIM_DICT_LOADED("misscarsteal2")
										actions[thisI].flag++
									ENDIF
								break
								case 2
									if IS_PED_IN_ANY_VEHICLE(ped[ped_franklin].id) or IS_PED_SITTING_IN_ANY_VEHICLE(ped[ped_franklin].id)
										IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,script_TASK_LEAVE_ANY_VEHICLE) = FINISHED_TASK
											TASK_LEAVE_ANY_VEHICLE(ped[ped_franklin].id)
										ENDIF
									ELSE						
										
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_franklin].id,<<1390.7333, -2063.3943, 50.9983>>) < 5
											if not IS_PED_INJURED(ped[ped_franklin].id)
												IF ABSF(GET_RELATIVE_ANGLE_FROM_ENTITY_TO_ENTITY(ped[ped_franklin].id,PLAYER_PED_ID())) > 45
													IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
														TASK_TURN_PED_TO_FACE_ENTITY(ped[ped_franklin].id,player_ped_id(),2000)
													ENDIF
												ELSE
													IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
													AND GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = FINISHED_TASK
													
														seq()
														TASK_LOOK_AT_ENTITY(null,player_ped_id(),5000,SLF_DEFAULT)													
														endseq(ped[ped_franklin].id)
													ENDIF
												ENDIF
											
											ENDIF	
										ELSE
											TASK_FOLLOW_NAV_MESH_TO_COORD(ped[ped_franklin].id,<<1390.7333, -2063.3943, 50.9983>>,pedmove_run)
											actions[thisI].flag++
										ENDIF
									ENDIF
								BREAK
								case 3
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_franklin].id,<<1390.7333, -2063.3943, 50.9983>>) < 5
										actions[thisI].flag = 2
									ENDIF
								break
							ENDSWITCH
						ENDIF
					BREAK
					
					CASE ACT_FRANKLIN_REACTS_TO_TREVOR_CONVERSATION
						SWITCH actions[thisI].flag
							CASE 0
								if not IS_PED_INJURED(ped[ped_franklin].id)		
								AND IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
									TASK_SWEEP_AIM_ENTITY(ped[ped_franklin].id, "missCarsteal2", "sweep_high", "sweep_high", "sweep_high", -1, vehicle[vehChopper].id,1.57,1)									
									actions[thisI].flag++
								endif
							BREAK
							CASE 1
								if not IS_PED_INJURED(ped[ped_franklin].id)		
								AND IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
									IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_GENERAL_SWEEP) = PERFORMING_TASK
										UPDATE_TASK_SWEEP_AIM_ENTITY(ped[ped_franklin].id,vehicle[vehChopper].id)
									ELSE
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE ACT_LOAD_PATH_NODES
						REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(1598.6936, -2350.9961,-200.3946, 368.2026)
						IF NOT DOES_ENTITY_EXIST(ped[ped_franklin].id)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_CREATE_PLAYER_CAR
						IF actions[thisI].flag = 0
							
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)								
								IF GET_ENTITY_MODEL(vehicle[vehFranklin].id) != BUFFALO2							
									vehicle[vehFranklin].id = null
							//	ELSE
								//	//cprintln(debug_trevor3,"player's last car is a Buffalo? ")
								ENDIF
						//	ELSE
							//	//cprintln(debug_trevor3,"IS player#s last car driveable? No")
							ENDIF
							
							IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
							OR (IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[vehFranklin].id,<<1380.7723, -2066.0107, 50.9983>>) > 60)
								IF (NOT IS_SPHERE_VISIBLE(<<1388.4817, -2043.4288, 50.9985>>,5)
								AND  NOT IS_ANY_VEHICLE_NEAR_POINT(<<1388.4817, -2043.4288, 50.9985>>,5))
									actions[thisI].flag = 1
									
								ELIF (NOT IS_SPHERE_VISIBLE(<<1408.5426, -2057.7336, 50.9983>>,5)
								AND  NOT IS_ANY_VEHICLE_NEAR_POINT(<<1408.5426, -2057.7336, 50.9983>>,5))
									actions[thisI].flag = 2
								ELIF (NOT IS_SPHERE_VISIBLE(<<1375.7686, -2080.0642, 50.9983>>,5)
								AND  NOT IS_ANY_VEHICLE_NEAR_POINT(<<1375.7686, -2080.0642, 50.9983>>,5))
									actions[thisI].flag = 3								
								ENDIF															
							ENDIF
						ENDIF
						
						IF actions[thisI].flag > 0
							SWITCH actions[thisI].flag
								CASE 1
									IF CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id,CHAR_FRANKLIN,<<1388.4817, -2043.4288, 50.9985>>, 135.7372,TRUE,VEHICLE_TYPE_CAR)
										actions[thisI].completed = TRUE
									ENDIF
								BREAK
								CASE 2
									IF CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id,CHAR_FRANKLIN,<<1408.5426, -2057.7336, 50.9983>>, 108.4516,TRUE,VEHICLE_TYPE_CAR)
										actions[thisI].completed = TRUE
									ENDIF
								BREAK
								CASE 3
									IF CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id,CHAR_FRANKLIN,<<1375.7686, -2080.0642, 50.9983>>, 312.5410,TRUE,VEHICLE_TYPE_CAR)
										actions[thisI].completed = TRUE
									ENDIF
								BREAK
							ENDSWITCH
						ELSE
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_CHANGE_FRANKLIN_LOD_LEVEL_UP
						IF NOT IS_PED_INJURED(ped[ped_Franklin].id)
							SET_PED_LOD_MULTIPLIER(ped[ped_Franklin].id,1.7)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_CHANGE_FRANKLIN_LOD_LEVEL_DOWN
						IF NOT IS_PED_INJURED(ped[ped_Franklin].id)
							SET_PED_LOD_MULTIPLIER(ped[ped_Franklin].id,1.0)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_FRAKNLIN_GETS_IN_CAR
						if not IS_PED_INJURED(ped[ped_franklin].id)	
							SWITCH actions[thisI].flag
								case CLEANUP
									REMOVE_ANIM_DICT("misscarsteal2")
									actions[thisI].completed = TRUE
								BREAK
								case 0
									
									
									
										//TASK_GO_TO_COORD_ANY_MEANS(ped[ped_franklin].id,<< -93.9606, -68.0128, 55.7683 >>,pedmove_run,null)
														
										IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
											seq()
											TASK_ENTER_VEHICLE(null,vehicle[vehFranklin].id)
											//TASK_VEHICLE_DRIVE_TO_COORD(null,vehicle[vehFranklin].id,<< -93.9606, -68.0128, 55.7683 >>,18.0,DRIVINGSTYLE_NORMAL,DUMMY_MODEL_FOR_SCRIPT,DRIVINGMODE_STOPFORCARS,5,5)
											TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(null,vehicle[vehFranklin].id,<< -93.9606, -68.0128, 55.7683 >>,18.0,DRIVINGMODE_STOPFORCARS,5)
											endseq(ped[ped_franklin].id)
										endif
										
										REMOVE_ANIM_DICT("misscarsteal2")
										FORCE_ACTION_STATE(2,ACT_FRANKLIN_LOOKS_AT_CHOPPER) //stop player forced to look at chopper
										actions[thisI].flag++
									
								BREAK
								CASE 1
									if IS_PED_IN_ANY_VEHICLE(ped[ped_franklin].id)									
										IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)																					
											DETACH_VEHICLE_FROM_ANY_TOW_TRUCK(vehicle[vehFranklin].id)												
											actions[thisI].completed = TRUE
										ENDIF
									endif
								BREAK
							endswitch
						endif
					BREAK
					
					//scan area 1
					CASE ACT_STOP_AUDIO_CAR_2_SCAN_FRANKLIN
						IF IS_POINT_VISIBLE(<<-10.7022, -33.1513, 69.2777>>,30,500) 
							IF IS_SPHERE_VISIBLE(<<-27.9336, -35.7344, 88.8509>>,10)
								
					
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF
					BREAK
					
					CASE ACT_SCANNING_AUDIO_SCENE
						SWITCH actions[thisI].flag
							CASE 0
								IF IS_AUDIO_SCENE_ACTIVE("CAR_2_SCANNING_TARGET")
									STOP_AUDIO_SCENE("CAR_2_SCANNING_TARGET")
								ENDIF
								IF HUD.scanning = TRUE
									START_AUDIO_SCENE("CAR_2_SCANNING_TARGET")
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
								IF HUD.scanning = FALSE
									STOP_AUDIO_SCENE("CAR_2_SCANNING_TARGET")
									actions[thisI].flag=0
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE ACT_STOP_CAR_2_SCAN_THE_SUSPECTS
						IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id)
							STOP_AUDIO_SCENE("CAR_2_SCAN_THE_SUSPECTS")
							START_AUDIO_SCENE("CAR_2_FOLLOW_CHAD_ON_FOOT")
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_DELETE_FRANKLIN
						If NOT IS_ENTITY_ON_SCREEN(ped[ped_Franklin].id)
							actions[thisI].floatA += TIMESTEP()
							IF actions[thisI].floatA > 10000
								IF NOT IS_PED_INJURED(ped[ped_Franklin].id)
									DELETE_PED(ped[ped_Franklin].id)
								ENDIF
								IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
									IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[vehFranklin].id)
										SET_ENTITY_AS_MISSION_ENTITY(vehicle[vehFranklin].id,true,true)
										DELETE_VEHICLE(vehicle[vehFranklin].id)
									ENDIF
								ENDIF
								
								actions[thisI].completed = TRUE
							ENDIF
						ELSE
							actions[thisI].floatA = 0
						ENDIF
					BREAK
					
					CASE ACT_GARAGE_OPENING_SOUND
						
						PLAY_SOUND_FROM_ENTITY(-1,"Garage_Open",ped[ped_chad].id,"CAR_STEAL_2_SOUNDSET")						
						
						actions[thisI].completed = TRUE
					BREAK
					
					// scan area 3
					CASE ACT_RELEASE_AREA_1_ASSETS
						IF NOT IS_POINT_VISIBLE(<<-10.7022, -33.1513, 69.2777>>,30,500)
						AND NOT IS_POINT_VISIBLE(<<-30.3868, -90.2560, 59.0222>>,30,500)
							KILL_EVENT(EVENTS_SCENE_PERVERT)
							KILL_EVENT(events_scene_mugging)
							SET_ASSET_STAGE(ASSETS_STAGE_RELEASE_AREA_1)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_SPAWN_ZTYPE
						IF IS_ASSET_LOADED(ASSET_ZYPE)
							initHeistVehicle(<< 202.7272, -149.7968, 56.1760 >>,160)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_DOG_BARKS
						
						IF listenDialogue = 5
						OR actions[thisI].flag = 0
							////cprintln(debug_Trevor3,"PLAYING DOG BARKS ACTION")
							//the anim has been tagged up so this should work without needing to call it now (url:bugstar:1245077)
							SWITCH actions[thisI].flag
								case 0
									IF REQUEST_SCRIPT_AUDIO_BANK("CAR_STEAL2_DISTANT_DOG")
										actions[thisI].intB = GET_SOUND_ID()
										actions[thisI].flag++
									ENDIF
								BREAK
								
								CASE 2
									cprintln(debug_Trevor3,"ACT_DOG_BARKS: flag = 2")
									IF NOT IS_PED_INJURED(ped[ped_dog].id)
										cprintln(debug_Trevor3,"ACT_DOG_BARKS: dog alive")
										IF IS_ENTITY_PLAYING_ANIM(ped[ped_dog].id,"CREATURES@ROTTWEILER@AMB@WORLD_DOG_BARKING@idle_a","idle_a")
											
											float fAnimTime
											fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_dog].id,"CREATURES@ROTTWEILER@AMB@WORLD_DOG_BARKING@idle_a","idle_a")
											//cprintln(debug_Trevor3,"DOG BARK ANIM PLAYING ",fAnimTime)
											If (fAnimTime >= 0.0 and fAnimTime < 0.02)
											OR (fAnimTime >= 0.173 and fAnimTime < 0.193)
											OR (fAnimTime >= 0.348 and fAnimTime < 0.368)
											OR (fAnimTime >= 0.528 and fAnimTime < 0.548)
											OR (fAnimTime >= 0.633 and fAnimTime < 0.653)
											OR (fAnimTime >= 0.818 and fAnimTime < 0.838)
												cprintln(debug_Trevor3,"BARK BARK")
													
												PLAY_SOUND_FRONTEND(actions[thisI].intB,"DISTANT_DOG_BARK","CAR_STEAL_2_SOUNDSET")
												actions[thisI].flag++
												actions[thisI].intA = GET_GAME_TIMER() + 500
											ENDIF
											
										ENDIF
									ENDIF
								BREAK
								CASE 3
									IF GET_GAME_TIMER() > actions[thisI].intA
										actions[thisI].flag = 2
									ENDIF
								BREAK
								CASE 10
									actions[thisI].completed = TRUE
								BREAK
							ENDSWITCH
								/*
									IF NOT IS_PED_INJURED(ped[ped_chad].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,ped[ped_dog].id) < 4
											actions[thisI].flag = 3
										ENDIF
									ENDIF
									IF GET_GAME_TIMER() > actions[thisI].intA
										IF GET_GAME_TIMER() - actions[thisI].intA > 1000
											actions[thisI].flag = 1
										ELSE
											IF NOT IS_PED_INJURED(ped[ped_dog].id)
												PLAY_SOUND_FRONTEND(actions[thisI].intB,"DISTANT_DOG_BARK","CAR_STEAL_2_SOUNDSET")
												//PLAY_ANIMAL_VOCALIZATION(ped[ped_dog].id,AUD_ANIMAL_DOG,"BARK")
											ENDIf
											//cprintln(debug_trevor3,"woof")
											actions[thisI].flag = 1										
											
										ENDIF
									ENDIF
								BREAK		
								case 3		
									IF actions[thisI].intA - GET_GAME_TIMER() > 900
										actions[thisI].intA = GET_GAME_TIMER()
									ENDIF
									IF GET_GAME_TIMER() > actions[thisI].intA
										IF NOT IS_PED_INJURED(ped[ped_dog].id)
											IF GET_RANDOM_INT_IN_RANGE(0,3) = 0
												PLAY_SOUND_FRONTEND(actions[thisI].intB,"DISTANT_DOG_GROWL","CAR_STEAL_2_SOUNDSET")	
												actions[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(600,900)
											ELSE
												PLAY_SOUND_FRONTEND(actions[thisI].intB,"DISTANT_DOG_BARK","CAR_STEAL_2_SOUNDSET")									
												actions[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(300,600)
											ENDIF
											actions[thisI].flag=2
										ENDIf
									ENDIF
									
								break
							ENDSWITCH*/
						ENDIF
					BREAK
					
					CASE ACT_TARGET_LOST_SOUND
						SWITCH actions[thisI].flag
							CASE 0
								IF HUD.targetIsLost
									actions[thisI].intA = GET_SOUND_ID()
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
								IF HUD.targetIsLost
									PLAY_SOUND_FRONTEND(StaticSoundID,"Lost_Target","POLICE_CHOPPER_CAM_SOUNDS")
									actions[thisI].flag++								
								ENDIF									
							BREAK
							CASE 2
								IF NOT HUD.targetIsLost
									PLAY_SOUND_FRONTEND(StaticSoundID,"Found_Target","POLICE_CHOPPER_CAM_SOUNDS")
									actions[thisI].flag=1
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					// ==== chase stage ====
					CASE ACT_block_peds_on_chase_route
						SET_PED_PATHS_IN_AREA(<<-290.3812, -100.4234, 46.3507>>-<<10,10,10>>,<<-290.3812, -100.4234, 46.3507>>+<<10,10,10>>,FALSE)
						actions[thisI].completed = TRUE
					BREAK
					
					CASE ACT_AUDIO_SCENE_TRANSITION
						SWITCH 	actions[thisI].flag
							CASE 0
								IF IS_CONV_ROOT_PLAYING("cs2_chase2")
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
								IF NOT IS_CONV_ROOT_PLAYING("cs2_chase2")
									STOP_AUDIO_SCENE("CAR_2_CAR_CHASE_START")
									START_AUDIO_SCENE("CAR_2_CAR_CHASE_CONTINUED")
									actions[thisI].completed = TRUE
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					case action_load_audio_scene
						IF REQUEST_SCRIPT_AUDIO_BANK("Car_Steal_2_Chase_Skids_01")
						AND REQUEST_SCRIPT_AUDIO_BANK("Car_Steal_2_Chase_Skids_02")
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								SET_AUDIO_VEHICLE_PRIORITY(vehicle[vehHeist].id,AUDIO_VEHICLE_PRIORITY_MAX)							
							ENDIF
							actions[thisI].completed = TRUE
						ENDIF
					break
					
					case act_update_target_car_audio_level						
						IF DOES_ENTITY_EXIST(HUD.camEntity)
						AND DOES_ENTITY_EXIST(vehicle[vehHeist].id)
							vector vCamRot
							vCamRot = GET_CAM_ROT(HUD.camChopper) 
							
							vector vCarRot
							float p1,p2,p3
							
							GET_PITCH_AND_HEADING_FROM_COORDS(GET_ENTITY_COORDS(HUD.camEntity,false),GET_ENTITY_COORDS(vehicle[vehHeist].id,false),vCarRot.x,vCarRot.z)
																
							p1 = cos(vCamRot.z-vCarRot.z)
							p2 = cos(vCamRot.x-vCarRot.x)
							p3 = cos(vCamRot.x+vCarRot.x)

							float sphericalRange
							sphericalRange = acos(((p1*(p2+p3))+(p2-p3))/2)
							
							SET_AUDIO_SCENE_VARIABLE("CAR_2_Z_TYPE_ENGINE_BOOST","TargetCarVisibility", sphericalRange)
							////cprintln(debug_trevor3,"dist: ",sphericalRange)
						ENDIF
					break
					
					CASE act_remove_trees
					
							CREATE_MODEL_HIDE(<<-111.8944,-188.65,46.02>>,1.0,prop_tree_birch_04,TRUE)						
							CREATE_MODEL_HIDE(<<-109.937,-179.954,47.62>>,1.0,prop_tree_birch_04,TRUE)
							CREATE_MODEL_HIDE(<<-108.3848,-171.323,49.34>>,1.0,prop_tree_birch_04,TRUE)
									
											
							actions[thisI].completed = TRUE
						
					BREAK
					
					
					
					// ===================================== CAR PARK ACTIONNS ===================================
					CASE ACT_CHOPPER_CONTROL
						SWITCH 	actions[thisI].flag
							CASE 0
								actions[thisI].intA = GET_GAME_TIMER() + 2000
								actions[thisI].flag++
							BREAK
							CASE 1
								IF GET_GAME_TIMER() > actions[thisI].intA
									add_event(EVENTS_CHOPPER_CARPARK)
									actions[thisI].completed = TRUE
								ENDIF
							BREAK
						ENDSWITCH						
					BREAK
					
					CASE ACT_CAR_HAS_PISS
						SWITCH 	actions[thisI].flag
							case cleanup
								if DOES_ENTITY_EXIST(ped[ped_wrong_wanker].id)
									if not IS_PED_INJURED(ped[ped_wrong_wanker].id) 
										IF IS_VEHICLE_DRIVEABLE(vehicle[veh_wrong1].id)
											SET_PED_INTO_VEHICLE(ped[ped_wrong_wanker].id,vehicle[veh_wrong1].id)
											seq()
											
											TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped[ped_wrong_wanker].id,<<-5,5,0>>),15000)
											TASK_PAUSE(null,15000)
											TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped[ped_wrong_wanker].id,<<5,5,0>>),15000)
											TASK_PAUSE(null,15000)
											endseq(ped[ped_wrong_wanker].id,true)
										ENDIF
										SET_PED_KEEP_TASK(ped[ped_wrong_wanker].id,TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_wrong_wanker].id,FALSE)
										SET_PED_AS_NO_LONGER_NEEDED(ped[ped_wrong_wanker].id)
									ENDIF
								ENDIF	
								
								if DOES_ENTITY_EXIST(vehicle[veh_wrong1].id)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_wrong1].id)
								ENDIF
																																																
							break
							
							case 0		
								IF IS_ASSET_LOADED(ASSET_CARPARK)
									actions[thisI].flag =100
									actions[thisI].intA = GET_GAME_TIMER() + 1000
								ENDIF
							break
							case 100 //timer added to prevent load_scene removed by request model assert.
								if GET_GAME_TIMER() > actions[thisI].intA

									SET_ACTION_NEEDS_CLEANUP(thisI)
							
									actions[thisI].flag=1
								ENDIF								
							BREAK
							case 1								
								initVehicle(veh_wrong1,DOMINATOR,<<-1269.4034, -212.2201, 50.8255>>,-56.5256)
								initPedInVehicle(ped_wrong_wanker,A_M_Y_GENSTREET_01,vehicle[veh_wrong1].id)					
								actions[thisI].flag++
							BREAK
							CASE 2
								IF IS_CONDITION_TRUE(COND_DIA_LOOK_FOR_MORE_HEAT_PLAYED)
								//IF HAS_DIALOGUE_FINISHED(7,DIA_TREVOR_SEES_PISSER)
									seq()
										TASK_LEAVE_ANY_VEHICLE(null)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-1269.9102, -210.0006, 50.5499>>,pedmove_walk)
										TASK_PLAY_ANIM(null,"misscarsteal2peeing","peeing_intro")
										TASK_PLAY_ANIM(null,"misscarsteal2peeing","peeing_loop",NORMAL_BLEND_IN,normal_blend_out,-1,AF_LOOPING)
									endseq(ped[ped_wrong_wanker].id)
									actions[thisI].flag++
								ENDIF
							break
							case 3
								IF HAS_DIALOGUE_FINISHED(15,DIA_OVERHEAR_PISSER_WITH_SCANNER)
									actions[thisI].intA = GET_GAME_TIMER() + 6000
									actions[thisI].flag++
								ENDIF								
							BREAK
							CASE 4
								IF  GET_GAME_TIMER() > actions[thisI].intA
									IF IS_VEHICLE_DRIVEABLE(vehicle[veh_wrong1].id)
										seq()
											TASK_PLAY_ANIM(null,"misscarsteal2peeing","peeing_outro")
											TASK_ENTER_VEHICLE(null,vehicle[veh_wrong1].id,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_WALK)										
										endseq(ped[ped_wrong_wanker].id)
										REMOVE_ANIM_DICT("misscarsteal2peeing")									
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF
							break
						ENDSWITCH
					BREAK
					
					

					CASE ACT_CAR_FIXER
						SWITCH 	actions[thisI].flag
							case cleanup																
								if DOES_ENTITY_EXIST(ped[ped_wrong_fucker].id)
									if not IS_PED_INJURED(ped[ped_wrong_fucker].id) 
										IF IS_VEHICLE_DRIVEABLE(vehicle[veh_wrong2].id)
											SET_PED_INTO_VEHICLE(ped[ped_wrong_fucker].id,vehicle[veh_wrong2].id)
											seq()
											
											TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped[ped_wrong_fucker].id,<<-5,5,0>>),15000)
											TASK_PAUSE(null,15000)
											TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped[ped_wrong_fucker].id,<<5,5,0>>),15000)
											TASK_PAUSE(null,15000)
											endseq(ped[ped_wrong_fucker].id,true)
										ENDIF
										SET_PED_KEEP_TASK(ped[ped_wrong_fucker].id,TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_wrong_fucker].id,FALSE)
										SET_PED_AS_NO_LONGER_NEEDED(ped[ped_wrong_fucker].id)
									ENDIF																		
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehicle[veh_wrong2].id)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_wrong2].id)
								ENDIF
															
								
							break
							case 0								
								
								SET_ACTION_NEEDS_CLEANUP(thisI)
								actions[thisI].flag++
							break
							case 1							
								IF IS_ASSET_LOADED(ASSET_CARPARK)
									initVehicle(veh_wrong2,DOMINATOR,<<-1260.3215, -244.8830, 50.7755>>,29.36)		
									SET_VEHICLE_DOOR_OPEN(vehicle[veh_wrong2].id,SC_DOOR_BONNET)
									initPedOnFoot(ped_wrong_fucker,A_M_Y_GENSTREET_01,<< -1260.095, -244.781, 51.224 >>, 34.6180,pedrole_civilian)									
									scene_car_fuck = CREATE_SYNCHRONIZED_SCENE(<< -1260.660, -244.607, 50.957 >>,<< -0.000, 0.000, 32.400 >>)									
								
									TASK_SYNCHRONIZED_SCENE (ped[ped_wrong_fucker].id, scene_car_fuck, "misscarsteal2fixer", "confused_a", INSTANT_BLEND_IN, slow_BLEND_OUT )																
									SET_SYNCHRONIZED_SCENE_LOOPED(scene_car_fuck,TRUE)
									
									actions[thisI].flag++
								ENDIF
							break						
						ENDSWITCH
					BREAK
					
					CASE ACT_ON_PHONE_IN_CAR
						SWITCH 	actions[thisI].flag
							case cleanup																
								if DOES_ENTITY_EXIST(ped[ped_wrong_phone].id)
									if not IS_PED_INJURED(ped[ped_wrong_phone].id) 
										IF IS_VEHICLE_DRIVEABLE(vehicle[veh_phoneChat].id)
											SET_PED_INTO_VEHICLE(ped[ped_wrong_phone].id,vehicle[veh_phoneChat].id)
											seq()
											
											TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped[ped_wrong_phone].id,<<-5,5,0>>),15000)
											TASK_PAUSE(null,15000)
											TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped[ped_wrong_phone].id,<<5,5,0>>),15000)
											TASK_PAUSE(null,15000)
											endseq(ped[ped_wrong_phone].id,true)
										ENDIF
										SET_PED_KEEP_TASK(ped[ped_wrong_phone].id,TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_wrong_phone].id,FALSE)
										SET_PED_AS_NO_LONGER_NEEDED(ped[ped_wrong_phone].id)
									ENDIF																		
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehicle[veh_phoneChat].id)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_phoneChat].id)
								ENDIF
								
							break
							case 0								
								SET_ACTION_NEEDS_CLEANUP(thisI)
								actions[thisI].flag++
							break
							case 1							
								if IS_ASSET_LOADED(ASSET_CARPARK)
									initVehicle(veh_phoneChat,HABANERO,<<-1260.8893, -226.0429, 50.5499>>, 303.0480)		
									SET_VEHICLE_DOOR_OPEN(vehicle[veh_phoneChat].id,SC_DOOR_FRONT_LEFT)
									initPedOnFoot(ped_wrong_phone,A_M_Y_GENSTREET_01,<< -1260.095, -244.781, 51.224 >>, 34.6180,pedrole_civilian)									
									scene_car_phone = CREATE_SYNCHRONIZED_SCENE(<< -1261.042, -225.594, 51.120 >>,<< -0.000, 0.000, -53.640 >>)									
								
									TASK_SYNCHRONIZED_SCENE (ped[ped_wrong_phone].id, scene_car_phone, "misscarsteal2", "wrong_house_dave_dave", INSTANT_BLEND_IN, slow_BLEND_OUT )																
									SET_SYNCHRONIZED_SCENE_LOOPED(scene_car_phone,TRUE)
									SET_SYNCHRONIZED_SCENE_PHASE(scene_car_phone,0.41)
									actions[thisI].flag++
								ENDIF
							break	
							case 2
								IF GET_SYNCHRONIZED_SCENE_PHASE(scene_car_phone) > 0.835
									SET_SYNCHRONIZED_SCENE_PHASE(scene_car_phone,0.460)
								ENDIF
							break
						ENDSWITCH
					BREAK
					
					CASE ACT_LEANING_ON_CAR
						SWITCH 	actions[thisI].flag
							case cleanup																
								if DOES_ENTITY_EXIST(ped[ped_wrong_lean].id)
									if not IS_PED_INJURED(ped[ped_wrong_lean].id) 
										IF IS_VEHICLE_DRIVEABLE(vehicle[veh_lean].id)
											SET_PED_INTO_VEHICLE(ped[ped_wrong_lean].id,vehicle[veh_lean].id)
											seq()
											
											TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped[ped_wrong_lean].id,<<-5,5,0>>),15000)
											TASK_PAUSE(null,15000)
											TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped[ped_wrong_lean].id,<<5,5,0>>),15000)
											TASK_PAUSE(null,15000)
											endseq(ped[ped_wrong_lean].id,true)
										ENDIF
										SET_PED_KEEP_TASK(ped[ped_wrong_lean].id,TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_wrong_lean].id,FALSE)
										SET_PED_AS_NO_LONGER_NEEDED(ped[ped_wrong_lean].id)
									ENDIF																		
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehicle[veh_lean].id)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_lean].id)
								ENDIF
															
								
							break
							case 0								
								
								SET_ACTION_NEEDS_CLEANUP(thisI)
								actions[thisI].flag++
							break
							case 1							
								if IS_ASSET_LOADED(ASSET_CARPARK)
								//	initVehicle(veh_phoneChat,DUBSTA,<<-1292.8159, -185.4189, 50.5497>>, 33.0002)		
									
									initPedOnFoot(ped_wrong_lean,A_M_Y_GENSTREET_01,<< -1260.095, -244.781, 51.224 >>, 34.6180,pedrole_civilian)									
									scene_car_lean = CREATE_SYNCHRONIZED_SCENE(<< -1293.832, -184.969, 50.870 >>,<< -0.000, 0.000, 180.0 >>)									
								
									TASK_SYNCHRONIZED_SCENE (ped[ped_wrong_lean].id, scene_car_lean, "misscarstealfinalecar_5_ig_1", "waitloop_lamar", INSTANT_BLEND_IN, slow_BLEND_OUT )																
									SET_SYNCHRONIZED_SCENE_LOOPED(scene_car_lean,TRUE)
									actions[thisI].flag++
								ENDIF
							break						
						ENDSWITCH
					BREAK
					
					CASE ACT_FRANKLIN_RUNS_TO_ROOF
						SWITCH actions[thisI].flag
							CASE 0
								IF NOT IS_PED_INJURED(ped[ped_franklin].id)
									IF IS_PED_IN_ANY_VEHICLE(ped[ped_franklin].id)
									//	VEHICLE_INDEX vehTemp
									//	vehTemp = GET_VEHICLE_PED_IS_IN(ped[ped_franklin].id)
									//	IF IS_VEHICLE_DRIVEABLE(vehtemp)
									//	AND IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
									//		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTemp)
												seq()
													TASK_LEAVE_ANY_VEHICLE(null)		
													TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-1273.9879, -221.8100, 50.5498>>,pedmove_run)
													TASK_TURN_PED_TO_FACE_COORD(null,<< -1314.7947, -243.3134, 55.9067 >>,3000)
												endseq(ped[ped_franklin].id)
												actions[thisI].flag++
												REQUEST_ANIM_DICT("missarmenian2lamar_idles")
												REQUEST_ANIM_DICT("misscarsteal2")
									//		ENDIF
									//	ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 1
								IF NOT IS_PED_INJURED(ped[ped_franklin].id)
									IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
										actions[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(500,2500)
										actions[thisI].flag++								
									ENDIF
								ENDIF
							BREAK
							CASE 2
								if IS_CONV_ROOT_PLAYING("cs2_onme")
									actions[thisI].flag++
								ENDIF
								
								IF GET_GAME_TIMER() > actions[thisI].intA
									IF NOT IS_PED_INJURED(ped[ped_franklin].id)
										IF NOT IS_PED_IN_ANY_VEHICLE(ped[ped_franklin].id)										
											IF HAS_ANIM_DICT_LOADED("missarmenian2lamar_idles")
												int iIdle
												iIdle = GET_RANDOM_INT_IN_RANGE(0,7)
												IF iIdle = actions[thisI].intB
													iIdle += 1
													IF iIdle = 7 iIdle = 0 ENDIF
												ENDIF
														
												if iIdle = 0
													TASK_PLAY_ANIM(ped[ped_franklin].id,"missarmenian2lamar_idles","idle_a",SLOW_BLEND_IN,SLOW_BLEND_OUT)										
												ELIF iIdle = 1
													TASK_PLAY_ANIM(ped[ped_franklin].id,"missarmenian2lamar_idles","idle_b",SLOW_BLEND_IN,SLOW_BLEND_OUT)
												ELIF iIdle = 2
													TASK_PLAY_ANIM(ped[ped_franklin].id,"missarmenian2lamar_idles","idle_c",SLOW_BLEND_IN,SLOW_BLEND_OUT)
												ELIF iIdle = 3
													TASK_PLAY_ANIM(ped[ped_franklin].id,"missarmenian2lamar_idles","idle_d",SLOW_BLEND_IN,SLOW_BLEND_OUT)
												ELIF iIdle = 4
													TASK_PLAY_ANIM(ped[ped_franklin].id,"missarmenian2lamar_idles","idle_e",SLOW_BLEND_IN,SLOW_BLEND_OUT)
												ELIF iIdle = 5
													TASK_PLAY_ANIM(ped[ped_franklin].id,"missarmenian2lamar_idles","idle_f",SLOW_BLEND_IN,SLOW_BLEND_OUT)
												ELIF iIdle = 6
													TASK_PLAY_ANIM(ped[ped_franklin].id,"missarmenian2lamar_idles","idle_look_right",SLOW_BLEND_IN,SLOW_BLEND_OUT)
												ENDIF
												actions[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500,5000)												
												actions[thisI].intB = iIdle
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							case 3
								IF NOT IS_PED_INJURED(ped[ped_franklin].id)
									if IS_sphere_VISIBLE(GET_ENTITY_COORDS(ped[ped_franklin].id),1.5)
										TASK_PLAY_ANIM(ped[ped_franklin].id,"misscarsteal2","COME_HERE_IDLE_C",SLOW_BLEND_IN,SLOW_BLEND_OUT)
										actions[thisI].intA = GET_GAME_TIMER() + 7000
										actions[thisI].flag++
									ENDIF
								ENDIF
							break
							CASE 4
								if get_game_timer() > actions[thisI].intA
								AND NOT IS_CONV_ROOT_PLAYING("cs2_onme")
									actions[thisI].flag = 2
								ENDIF
							BREAK
						ENDSWITCH	
					BREAK
					
					CASE ACT_FRANKLIN_WAVES_AT_CHOPPER_CAMERA
					/* //gestures@male removed
						if IS_CONV_ROOT_PLAYING("cs2_looknow")
							actions[thisI].flag = cleanup
						endif
						SWITCH actions[thisI].flag
							case cleanup
								REMOVE_ANIM_DICT("gestures@male")							
							break
							case 0
								REQUEST_ANIM_DICT("gestures@male")
								TASK_TURN_PED_TO_FACE_ENTITY(ped[ped_franklin].id,player_ped_id(),1000)
								SET_ACTION_NEEDS_CLEANUP(thisI)
								actions[thisI].flag++
							BREAK
							CASE 1
								if IS_CONDITION_TRUE(COND_DIA_TREVOR_SEE_FRANKLIN_ENDED)
									actions[thisI].flag = cleanup
								else
									IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = FINISHED_TASK
										IF HAS_ANIM_DICT_LOADED("gestures@male")
											actions[thisI].intA = GET_RANDOM_INT_IN_RANGE(0,4)
											switch actions[thisI].intA
												CASE 0 TASK_PLAY_ANIM(ped[ped_franklin].id,"gestures@male","raise_hands",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,af_Default,0.27) BREAK
												CASE 1 TASK_PLAY_ANIM(ped[ped_franklin].id,"gestures@male","amazing",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,af_Default,0.27) BREAK
												CASE 2 TASK_PLAY_ANIM(ped[ped_franklin].id,"gestures@male","bring_it_on",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,af_Default,0.27) BREAK
												CASE 3 TASK_PLAY_ANIM(ped[ped_franklin].id,"gestures@male","give_me_a_break",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,af_Default,0.27) BREAK
											ENDSWITCH
																				
											actions[thisI].flag++
										ENDIF
									ENDIF
								endif
							BREAK
							CASE 2
								switch actions[thisI].intA
									CASE 0 IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_franklin].id,"gestures@male","raise_hands") > 0.7 actions[thisI].flag=1 ENDIF BREAK
									CASE 1 IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_franklin].id,"gestures@male","amazing") > 0.7 actions[thisI].flag=1 ENDIF  BREAK
									CASE 2 IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_franklin].id,"gestures@male","bring_it_on") > 0.7 actions[thisI].flag=1 ENDIF  BREAK
									CASE 3 IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_franklin].id,"gestures@male","give_me_a_break") > 0.7 actions[thisI].flag=1 ENDIF  BREAK
								ENDSWITCH							
							BREAK
						ENDSWITCH
						*/
					BREAK
					
					case ACT_FRANKLIN_RUNS_TO_BONNET_CAR
						IF NOT IS_PED_INJURED(ped[ped_franklin].id)
							SWITCH actions[thisI].flag
								CASE 0
									seq()	
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< -1263.6390, -239.6366, 50.5499 >>,PEDMOVE_RUN)
										IF NOT IS_PED_INJURED(ped[ped_wrong_fucker].id)
											TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_wrong_fucker].id,5000)
										ENDIF
										TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),3000)	
									endseq(ped[ped_franklin].id)
									 actions[thisI].flag++
								BREAK
								CASE 1
									IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
											IF GET_SEQUENCE_PROGRESS(ped[ped_franklin].id) >= 1
												actions[thisI].completed = TRUe
											ENDIF
										ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					break
					
					CASE ACT_FRANKLIN_RUNS_TO_PISSER_CAR	
						IF NOT IS_PED_INJURED(ped[ped_franklin].id)
							SWITCH actions[thisI].flag
								CASE 0
									seq()	
									TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< -1273.5055, -212.8070, 50.5499 >>,PEDMOVE_RUN,DEFAULT_TIME_BEFORE_WARP,1.2,ENAV_DEFAULT,300)
									
									endseq(ped[ped_franklin].id)
								
									 actions[thisI].flag++
								BREAK
								CASE 1
									IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
//										IF GET_SEQUENCE_PROGRESS(ped[ped_franklin].id) >= 1
											actions[thisI].completed = TRUe
//										ENDIF
									ENDIF
								BREAK
							ENDSWITCH					
						ENDIF
					BREAK
	
					CASE ACT_FRANKLIN_RUNS_TO_CHAD_CAR
						IF NOT IS_PED_INJURED(ped[ped_franklin].id)
							SWITCH actions[thisI].flag
								CASE 0
								
									TASK_FOLLOW_NAV_MESH_TO_COORD(ped[ped_franklin].id,<<-1301.0939, -208.0075, 50.5498>>,PEDMOVE_RUN,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,141.1114)
								
									 actions[thisI].flag++
								BREAK
								CASE 1
									//cprintln(DEBUG_TREVOR3,GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD))
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_franklin].id,<<-1301.0939, -208.0075, 50.5498>>) < 5									
										IF NOT IS_PED_INJURED(ped[ped_franklin].id)
										AND NOT IS_PED_INJURED(ped[ped_chad].id)
											GIVE_WEAPON_TO_PED(ped[ped_franklin].id,WEAPONTYPE_PISTOL,50,true)
											SET_CURRENT_PED_WEAPON(ped[ped_franklin].id,WEAPONTYPE_PISTOL,TRUE)
											TASK_AIM_GUN_AT_ENTITY(ped[ped_franklin].id,ped[ped_chad].id,INFINITE_TASK_TIME)										
										ENDIF
										actions[thisI].completed = TRUe
									ELIF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
										
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					BREAK
					
					CASE ACT_FRANKLIN_RUNS_TO_LEANING_MAN
						IF NOT IS_PED_INJURED(ped[ped_franklin].id)
							SWITCH actions[thisI].flag
								CASE 0
									seq()
									TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-1293.6746, -193.4069, 50.5497>>,PEDMOVE_RUN)
									IF NOT IS_PED_INJURED(ped[ped_wrong_lean].id)
										TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_wrong_lean].id,5000)
									ENDIF
									TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),3000)	
									endseq(ped[ped_franklin].id)
									 actions[thisI].flag++
								BREAK
								CASE 1
									IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
										IF GET_SEQUENCE_PROGRESS(ped[ped_franklin].id) >= 1
											actions[thisI].completed = TRUe
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH	
						ENDIF
					BREAK
	
					CASE ACT_FRANKLIN_RUNS_TO_PHONE_MAN
						IF NOT IS_PED_INJURED(ped[ped_franklin].id)
							SWITCH actions[thisI].flag
								CASE 0
									seq()
									TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-1265.9111, -225.6303, 50.5499>>,PEDMOVE_RUN,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,300)
									
									endseq(ped[ped_franklin].id)
										
							
									 actions[thisI].flag++
								BREAK
								CASE 1
									IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
								//		IF GET_SEQUENCE_PROGRESS(ped[ped_franklin].id) >= 1
											actions[thisI].completed = TRUe
								//		ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					BREAK
		
					
					CASE ACT_MAKE_FRANKLIN_WALK_ON_SWITCH
						SWITCH actions[thisI].flag
							case 0
								IF iFlag=CASE_SWITCH_IN_PROGRESS
									actions[thisI].intA = GET_GAME_TIMER() + 2500
									IF NOT IS_PED_INJURED(ped[ped_franklin].id)
									AND NOT IS_PED_INJURED(ped[ped_chad].id)
										TASK_AIM_GUN_AT_ENTITY(ped[ped_franklin].id,ped[ped_chad].id,3000)
									ENDIF
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
								IF GET_GAME_TIMER() > actions[thisI].intA
									IF NOT IS_PED_INJURED(ped[ped_franklin].id)
										SET_PED_MIN_MOVE_BLEND_RATIO(ped[ped_franklin].id,pedmove_walk)
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE ACT_PREP_SWITCH
						SWITCH actions[thisI].flag
							case 0
							
								
								
								actions[thisI].flag++
							BREAK
							CASE 1
								actions[thisI].completed = TRUE						
							BREAK
						ENDSWITCH
					BREAK
					
					CASE ACT_DO_SWITCH
						//IF not IS_CONV_ROOT_PLAYING("CS2_manc1")
							
							SWITCH actions[thisI].flag
							
								
								case 0
									
									
									
									HIDE_HUD_AND_RADAR_THIS_FRAME()
									DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
																
									CLEAR_PRINTS()
									PLAY_MUSIC(mus_switch_to_franklin,mus_get_car_to_objective)	
									SET_INFRARED(FALSE,TRUE)										
									
									HUD.bDisplayHUD = FALSE
									SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[vehChopper].id, FALSE, vehicle[vehChopper].id)
									
									SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl", FALSE)
									STOP_AUDIO_SCENE("CAR_2_CAR_ENTERS_GARAGE")
									
									//CLEAR_TIMECYCLE_MODIFIER()
									kill_event(EVENTS_CHOPPER_CARPARK)
									kill_event(EVENTS_CONTROL_INFRARED)	
																																					
									SET_CONDITION_STATE(COND_SWITCH_BEGUN,TRUE)

									PED_INDEX deleteThisPed
									deleteThisPed = PLAYER_PED_ID()
									
									UNREGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS(CHAR_TREVOR, ped[ped_trevor].id) 

									IF NOT IS_PED_INJURED(ped[ped_franklin].id)
										CHANGE_PLAYER_PED(PLAYER_ID(), ped[ped_franklin].id)
										REGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS(CHAR_FRANKLIN, ped[ped_franklin].id) 
										//DO_INSTANT_SWITCH(ped[ped_franklin].id,ped[ped_trevor].id)
									ENDIF
									
									
									
									IF NOT IS_PED_INJURED(ped[ped_chad].id) //added to try and fix bug 1524679
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_chad].id,TRUE)
									ENDIF
									
									SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1299.6785, -205.9114, 50.5498>>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), 143.87)

									ped[ped_trevor].id = deleteThisPed
									
									IF bPlayerinFirstPerson
										RESET_PUSH_IN(s_cs2_pushin)
										FILL_PUSH_IN_DATA(s_cs2_pushin,ped[ped_franklin].id,CHAR_FRANKLIN,2.5,1200,1200,900,PUSH_IN_SPEED_UP_TIME)										
									ENDIF
									
									IF DOES_ENTITY_EXIST(vehicle[vehChopper].id)
										IF NOT IS_ENTITY_DEAD(vehicle[vehChopper].id)
											
											SET_PED_INTO_VEHICLE(ped[ped_trevor].id,vehicle[vehChopper].id,VS_FRONT_RIGHT)
											
										//	CREATE_PLAYER_PED_INSIDE_VEHICLE(ped[ped_trevor].id, CHAR_TREVOR, vehicle[vehChopper].id, VS_FRONT_RIGHT)
											IF DOES_ENTITY_EXIST(ped[ped_trevor].id)
												IF NOT IS_ENTITY_DEAD(ped[ped_trevor].id)
													IF NOT IS_PED_INJURED(ped[ped_trevor].id)
														SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_trevor].id, TRUE)
														TASK_PLAY_ANIM(ped[ped_trevor].id, "misscarsteal2switch", "_heli_trevor", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
														FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_trevor].id)
													ENDIF
												ENDIF
											ENDIF
											
											ped[ped_pilot].id = CREATE_PED_INSIDE_VEHICLE(vehicle[vehChopper].id, PEDTYPE_MISSION, model_pilot, VS_DRIVER)
											IF DOES_ENTITY_EXIST(ped[ped_pilot].id)
												IF NOT IS_ENTITY_DEAD(ped[ped_pilot].id)
													IF NOT IS_PED_INJURED(ped[ped_pilot].id)
														SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_pilot].id, TRUE)
														FORCE_PED_AI_AND_ANIMATION_UPDATE(ped[ped_pilot].id)
													ENDIF
												ENDIF
											ENDIF
											
										ENDIF
									ENDIF
									
									#IF IS_DEBUG_BUILD
									IF bSkipSwitch
										//CLEAR_TIMECYCLE_MODIFIER()
										if not IS_PED_INJURED(ped[ped_franklin].id)		
										and not is_ped_injured(ped[ped_chad].id)
											GIVE_WEAPON_TO_PED(ped[ped_franklin].id,WEAPONTYPE_PISTOL,25,true)
											task_play_anim(player_ped_id(),"misscarsteal2switch","_ground_franklin",instant_blend_in,1)

											//TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(ped[ped_franklin].id,<<-1302.1127, -212.9030, 50.5500>>,<<-1307.9683, -216.9170, 51.9251>>,pedmove_walk,false)
										endif
										STOP_AUDIO_SCENE("CAR_2_USE_INFRARED")				
										IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
											SET_ENTITY_HEALTH(vehicle[vehHeist].id,1000)
											SET_VEHICLE_DIRT_LEVEL(vehicle[vehHeist].id,0.0)	
											SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehicle[vehHeist].id,true)
											SET_VEHICLE_ENGINE_ON(vehicle[vehHeist].id,true,true)
											SET_VEHICLE_DOOR_OPEN(vehicle[vehHeist].id,SC_DOOR_FRONT_LEFT)
										ENDIF

										IF	IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
											SET_ENTITY_INVINCIBLE(vehicle[vehHeist].id,FALSE)
										ENDIF
										actions[thisI].flag++
										
									else
									#ENDIF
									
										DISPLAY_HUD(FALSE)
										DISPLAY_RADAR(FALSE)
										eSwitchCamState = SWITCH_CAM_START_SPLINE1
										
										actions[thisI].flag++
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
								FALLTHRU
								
								case 1
									
									#IF IS_DEBUG_BUILD
									IF bSkipSwitch
										actions[thisI].completed = TRUE
									ELSE
									#ENDIF
										IF HANDLE_SWITCH_HELI_TO_FRANKLIN(scsCarsteal2)
											actions[thisI].completed = TRUE
										ENDIF
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
								break
								
							endswitch
						//ENDIF					
					BREAK
	
					
					CASE ACT_CHAD_LOOKS_ABOUT
						SWITCH actions[thisI].flag
							case 0
								REQUEST_ANIM_DICT("misscarsteal2Chad_waiting")
								actions[thisI].flag++															
							BREAK
							case 1				
								
							
								if HAS_ANIM_DICT_LOADED("misscarsteal2Chad_waiting")
									IF DOES_ENTITY_EXIST(ped[ped_chad].id)
										IF NOT IS_PED_INJURED(ped[ped_chad].id)
											TASK_PLAY_ANIM(ped[ped_chad].id,"misscarsteal2Chad_waiting","Sat_in_Car_Lookaround",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
										ENDIF
									
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF
								
							break
						ENDSWITCH
					BREAK
					
					CASE ACT_CHAD_EXITS_CAR
						switch actions[thisi].flag
							case CLEANUP
								REMOVE_ANIM_DICT("misscarsteal2CAR_STOLEN")
								REMOVE_ANIM_DICT("misscarsteal2CHAD_GARAGE")
								events[thisi].active = FALSE
							break

							case 0
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_chad].id,TRUE)
								ENDIF
								REQUEST_ANIM_DICT("misscarsteal2CAR_STOLEN")
								REQUEST_ANIM_DICT("misscarsteal2CHAD_GARAGE")
								actions[thisi].flag++
							BREAK
							case 1
								if HAS_ANIM_DICT_LOADED("misscarsteal2CAR_STOLEN")
								and HAS_ANIM_DICT_LOADED("misscarsteal2CHAD_GARAGE")
									actions[thisi].flag++
								ENDIF
							break																												
							case 2		
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
								AND IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									scene_chad_leaves_car = CREATE_SYNCHRONIZED_SCENE(<< -1308.275, -222.337, 50.563 >>, << -0.000, 0.000, 35.500 >>)
								
									SET_ENTITY_COORDS_NO_OFFSET(ped[ped_chad].id,<< -1308.275, -222.337, 50.563 >>)
									TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, scene_chad_leaves_car, "misscarsteal2CHAD_GARAGE", "chad_parking_garage_chad", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE) 
									SET_VEHICLE_DOOR_OPEN(vehicle[vehHeist].id,SC_DOOR_FRONT_LEFT,true,true)
									actions[thisi].flag++
								ENDIF
							BREAK 
							case 3
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(scene_chad_leaves_car)
										IF GET_SYNCHRONIZED_SCENE_PHASE(scene_chad_leaves_car) > 0.1
											TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, scene_chad_leaves_car, "misscarsteal2CHAD_GARAGE", "chad_parking_garage_chad", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
											TASK_LOOK_AT_ENTITY(ped[ped_chad].id,player_ped_id(),-1,SLF_EXTEND_YAW_LIMIT)
											SET_ENTITY_COLLISION(ped[ped_chad].id,TRUE)
											actions[thisi].flag++
										ENDIF
									ENDIF
								ENDIF
							break
							case 4
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
								AND IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(scene_chad_leaves_car)
										IF GET_SYNCHRONIZED_SCENE_PHASE(scene_chad_leaves_car) > 0.87
											actions[thisi].completed = TRUE/*
											seq()
										//	TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-1307.1735, -216.8293, 50.5498>>,PEDMOVE_RUN,DEFAULT_TIME_BEFORE_WARP,0.2)									
											TASK_FOLLOW_WAYPOINT_RECORDING(null,"cs2_10",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
											TASK_ENTER_VEHICLE(null,vehicle[vehHeist].id)
											TASK_VEHICLE_DRIVE_TO_COORD(null,vehicle[vehHeist].id,<<-1263.1799, -307.1262, 36.0240>>,20.0,DRIVINGSTYLE_NORMAL,ztype,DRIVINGMODE_AVOIDCARS_RECKLESS,5.0,5.0)
											endseq(ped[ped_chad].id)
											actions[thisi].flag++*/
										ENDIF
									ENDIF
								ENDIF
							break
							case 5
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
								AND IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									actions[thisi].completed = TRUE
								ENDIF
							break
								
						ENDSWITCH
					BREAK
					
					CASE ACT_PREP_STAGE
						SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
						kill_event(EVENTS_KEEP_CHAD_SAFE)
						//SET_INITIAL_PLAYER_STATION("RADIO_09_HIPHOP_OLD")
						//FREEZE_RADIO_STATION("RADIO_09_HIPHOP_OLD")
						//SET_RADIO_AUTO_UNFREEZE(FALSE)
						//SET_RADIO_TRACK("RADIO_09_HIPHOP_OLD", "CAR2_RADIO")
						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-961.907654,-3006.614502,21.694874>>-<<41.187500,41.500000,11.125000>>,<<-961.907654,-3006.614502,21.694874>>+<<41.187500,41.500000,11.125000>>,false) //airport hanagar
						SET_WANTED_LEVEL_MULTIPLIER(1.0)
						SET_MAX_WANTED_LEVEL(5)
						DISABLE_CELLPHONE(FALSE)
						SET_IGNORE_NO_GPS_FLAG(TRUE)
						set_road_blocks(0,15,true)
						kill_event(EVENTS_STEALTH_REMOVE_TRAFFIC)
						SET_ROADS_IN_ANGLED_AREA(<<-1314.045654,-183.052002,40.465984>>, <<-1256.618408,-261.595306,68.179886>>, 52.250000,FALSE,FALSE)
						SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
						IF NOT IS_PED_INJURED(ped[ped_chad].id)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_chad].id,TRUE)
							TASK_LOOK_AT_ENTITY(ped[ped_chad].id,player_ped_id(),-1)
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
							FREEZE_ENTITY_POSITION(vehicle[vehHeist].id,FALSE)
							SET_VEHICLE_ENGINE_ON(vehicle[vehHeist].id,TRUE,TRUE)
							SET_VEHICLE_LIGHTS(vehicle[vehHeist].id,NO_VEHICLE_LIGHT_OVERRIDE)
						ENDIF
						
				
						
						START_AUDIO_SCENE("CAR_2_STEAL_THE_CAR")
						actions[thisI].completed = TRUE
					BREAK
					
					CASE ACT_STOP_AUDIO_CAR_2_STEAL_THE_CAR
						IF DOES_ENTITY_EXIST(ped[ped_chad].id)
							IF IS_PED_INJURED(ped[ped_chad].id)
								STOP_AUDIO_SCENE("CAR_2_STEAL_THE_CAR")
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
							IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehHeist].id)
								STOP_AUDIO_SCENE("CAR_2_STEAL_THE_CAR")
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF							
					BREAK
					
					CASE ACT_START_AUDIO_CAR_2_DRIVE_BACK_TO_GARAGE
						IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
							IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehHeist].id)
								START_AUDIO_SCENE("CAR_2_DRIVE_BACK_TO_GARAGE")
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF	
					BREAK
					
					CASE ACT_CHAD_TRIES_TO_CREEP_AWAY
					//	IF actions[thisI].flag > 0
					//		IF NOT IS_ENTITY_PLAYING_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_GARAGE", "chad_parking_garage_handsuploop_chad")
							//	SET_CONDITION_STATE(COND_CHAD_CAN_RUN,TRUE)
					//			actions[thisI].completed = TRUE
					//		ENDIF
					//	ENDIF
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
							switch actions[thisI].flag
								case 0
								//	seq()
										TASK_PLAY_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_GARAGE", "chad_parking_garage_handsuploop_chad",SLOW_BLEND_IN, NORMAL_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING | AF_NOT_INTERRUPTABLE)									
										
								//	endseq(ped[ped_chad].id)
									//actions[thisI].intA = get_Game_timer() + 1000
									actions[thisI].flag++
								BREAK
								case 1
									//IF GET_GAME_TIMER() > actions[thisI].intA
										//TASK_TURN_PED_TO_FACE_ENTITY(ped[ped_chad].id,player_ped_id(),15000)
										seq()
										TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),2000)
										TASK_PLAY_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_GARAGE", "chad_parking_garage_handsuploop_chad",SLOW_BLEND_IN, NORMAL_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING | AF_NOT_INTERRUPTABLE)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null,<<-1310.3997, -218.5188, 50.5498>>,player_ped_id(),pedmove_walk,false)
										TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),-1)
										endseq(ped[ped_chad].id)
										actions[thisI].intA = get_Game_timer() + 13000
										actions[thisI].flag++
									//ENDIF
								BREAK
								case 2
									/*IF GET_GAME_TIMER() > actions[thisI].intA
										SET_CONDITION_STATE(COND_CHAD_CAN_RUN,TRUE)
										actions[thisI].completed = TRUE
										actions[thisI].flag++
									ENDIF*/
								break
								case 3
								
									IF NOT IS_PED_INJURED(ped[ped_chad].id)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_chad].id,<<-1287.6219, -251.0352, 46.0453>>) < 2.0
											actions[thisI].completed = TRUE
										ENDIF
									ENDIF
								break
							endswitch						
						ELSE
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_CHAD_RUNS_OFF
						switch actions[thisI].flag
							case 0
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
									IF IS_ENTITY_PLAYING_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_GARAGE", "chad_parking_garage_handsuploop_chad") //
										STOP_ANIM_TASK(ped[ped_chad].id,"misscarsteal2CHAD_GARAGE", "chad_parking_garage_handsuploop_chad")																												
									endif
									CLEAR_PED_TASKS(ped[ped_chad].id)
									seq()
									TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-1287.6219, -251.0352, 46.0453>>,pedmove_run)
									TASK_COWER(null,-1)
									endseq(ped[ped_chad].id)
									actions[thisI].flag++
								endif
							break
							case 1
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_chad].id,<<-1287.6219, -251.0352, 46.0453>>) < 2.0
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE ACT_REACTS_TO_PLAYER_IN_CAR	
						IF NOT IS_PED_INJURED(ped[ped_chad].id)
							switch actions[thisI].flag
								case 0
									
										seq()
											TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
											TASK_PLAY_ANIM(null,"misscarsteal2CHAD_GARAGE", "chad_parking_garage_handsuploop_chad",SLOW_BLEND_IN,SLOW_BLEND_OUT,30000,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)																				
											TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),30000)
										endseq(ped[ped_chad].id)
										REQUEST_ANIM_DICT("misscarsteal2car_stolen")
										actions[thisI].intA = get_game_timer() + 25000
										actions[thisI].flag++
									
								break
								case 1
								
										IF HAS_ANIM_DICT_LOADED("misscarsteal2car_stolen")										
											CLEAR_PED_SECONDARY_TASK(ped[ped_chad].id)
											seq()
											TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
											TASK_PLAY_ANIM(null,"misscarsteal2car_stolen","chad_car_stolen_reaction",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1) 
											TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,false)
											endSeq(ped[ped_chad].id)
											actions[thisI].flag++
										ENDIF
								
								break
								case 2
									if GET_SCRIPT_TASK_STATUS(	ped[ped_chad].id,script_task_play_anim) = FINISHED_TASK
									OR GET_SCRIPT_TASK_STATUS(	ped[ped_chad].id,script_task_play_anim) = DORMANT_TASK
										actions[thisI].completed = TRUE
									endif
								break
							endswitch
						ENDIF
					BREAK
					
					case ACT_CHAD_CALLS_COPS
						switch actions[thisI].flag
							case 0
								IF NOT IS_PED_INJURED(ped[ped_chad].id)									
									TASK_USE_MOBILE_PHONE(ped[ped_chad].id,true)
									actions[thisI].flag++
								ENDIF
							break
							case 1
							/*
								IF IS_DIALOGUE_COMPLETE(DIA_CALL_COPS)
									seq()
									TASK_USE_MOBILE_PHONE(null,false)
									TASK_COWER(null,-1)
									endseq(ped[ped_chad].id)
								ENDIF
							break*/
							break
						endswitch
					break
					
					case ACT_FLY_TO_GARAGE
						IF NOT IS_PED_INJURED(ped[ped_pilot].id)
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
								IF IS_PED_IN_VEHICLE(ped[ped_pilot].id, vehicle[vehChopper].id)								
									TASK_HELI_MISSION(ped[ped_pilot].id, vehicle[vehChopper].id,null,null,<<500.9223, -1314.2908, 73.6306>>,MISSION_GOTO,20,10,0,100,60)
									
									actions[thisI].completed = TRUE
								ENDIF
							ENDIF
						ENDIF
					break
					
					CASE ACT_REMOVE_CAR_PARK_ASSETS
						int iTemp
						repeat COUNT_OF(carParkVehicles) iTemp
							SET_VEHICLE_AS_NO_LONGER_NEEDED(carParkVehicles[itemp])
						ENDREPEAT
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_postie].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_camper].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_phoneChat].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_vanhide].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_wrong2].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[vehFranklin].id)		
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_lean].id)	
						SET_ped_AS_NO_LONGER_NEEDED(ped[ped_chad].id)
						REMOVE_VEHICLE_RECORDING(301,sVehicleRecordingLibrary)
						SET_ASSET_STAGE(ASSETS_STAGE_RELEASE_CAR_PARK)
						SET_ROADS_IN_ANGLED_AREA(<<-1314.045654,-183.052002,40.465984>>, <<-1256.618408,-261.595306,68.179886>>, 52.250000,FALSE,TRUE)
						actions[thisI].completed = TRUE
					BREAK
					
					CASE ACT_SPAWN_SECURITY_GUARD
						switch actions[thisI].flag
							case 0
								IF IS_ASSET_LOADED(ASSET_DEVIN)
									ADD_SCENARIO_BLOCKING_AREA(<<-964.12, -2799.27, 13.24>>-<<10,10,10>>,<<-964.12, -2799.27, 13.24>>+<<10,10,10>>)
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
								ped[ped_cop1].id = CREATE_PED(PEDTYPE_MISSION,S_M_M_Security_01,<<-966.6412, -2800.2471, 12.9648>>, 19.0574)
								SET_PED_CAN_EVASIVE_DIVE(ped[ped_cop1].id,false)
								actions[thisI].flag++
							BREAK
							CASE 2
								IF NOT IS_PED_INJURED(ped[ped_cop1].id)
									//IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-966.2216, -2800.1692, 12.9648>>,"WORLD_HUMAN_GUARD_STAND",3,TRUE)
										//TASK_START_SCENARIO_IN_PLACE(ped[ped_cop1].id,"WORLD_HUMAN_GUARD_STAND")
										TASK_START_SCENARIO_AT_POSITION(ped[ped_cop1].id,"WORLD_HUMAN_GUARD_STAND",<<-966.9684, -2799.9868, 12.9648>>, 19.0574)
										actions[thisI].flag++
									//ENDIF
								ENDIF
							BREAK
							CASE  3
								IF NOT IS_PED_INJURED(ped[ped_cop1].id)
									IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_cop1].id) < 30
										TASK_LOOK_AT_ENTITY(ped[ped_cop1].id,player_ped_id(),-1)
										actions[thisI].completed = TRUE
									ENDIF
								ENDIF								
							BREAK
						ENDSWITCH
					BREAK
					
					CASE ACT_OPEN_GATES						
						IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_IN, PLAYER_PED_ID())
							REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_IN, PLAYER_PED_ID())
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_SET_UP_DEVIN_LEADIN
						switch actions[thisI].flag
							case 0
								IF IS_ASSET_LOADED(ASSET_DEVIN)
									
									actions[thisI].flag=2
								ENDIF
							BREAK
						
							
							CASE 2
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),devinCoord) < 300									
									actions[thisI].flag=3
								endif
							BREAK
						
							
							CASE 3
								
								
									//IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( vehicle[vehHeist].id, carDropoffCoord) < 30
									 
									intHangar = GET_INTERIOR_AT_COORDS(<<-960.7767, -3016.6912, 12.9451>>)	
									PIN_INTERIOR_IN_MEMORY(intHangar)
									actions[thisI].flag++
							
							BREAK
							CASE 4
								IF IS_INTERIOR_READY(intHangar)																													
									IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
										IF CREATE_NPC_PED_ON_FOOT(pedDevin,CHAR_DEVIN,devinCoord,fDevinHeading)
										AND CREATE_NPC_PED_ON_FOOT(ped[ped_cop4].id,CHAR_MOLLY,<<-992.12, -3024.18, 12.94>>,42.6)
											
											SET_PED_DEFAULT_COMPONENT_VARIATION(pedDevin)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
											SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
											//SET_PED_CONFIG_FLAG(pedDevin, PCF_AlwaysSeeApproachingVehicles,false)
											SET_PED_CAN_EVASIVE_DIVE(pedDevin,false)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDevin,TRUE)
											vehicle[vehJet].id = CREATE_VEHICLE(SHAMAL,<<-981.02, -3011.31, 14.55>>,59.25)
											SET_VEHICLE_DOORS_LOCKED(vehicle[vehJet].id,VEHICLELOCK_LOCKED)
											SET_VEHICLE_LIVERY(vehicle[vehJet].id,1)
											iDevinSyncSCene = CREATE_SYNCHRONIZED_SCENE(devinCoord+<<0,0,1>>,<<0,0,fDevinHeading>>)
										 
											TASK_SYNCHRONIZED_SCENE (pedDevin, iDevinSyncSCene, "misscarsteal2leadinoutcar_2_mcs_1", "_leadin_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
											SET_SYNCHRONIZED_SCENE_LOOPED(iDevinSyncSCene,TRUE)
											
											
											//create security
											ped[ped_cop2].id = create_ped(PEDTYPE_MISSION,S_M_Y_DEVINSEC_01,<<-989.96, -3022.82, 13.91>>,91.12)
											ped[ped_cop3].id = create_ped(PEDTYPE_MISSION,S_M_Y_DEVINSEC_01,<<-990.51, -3023.46, 13.93>>,11.9)
											
											GIVE_WEAPON_TO_PED(ped[ped_cop2].id,WEAPONTYPE_MICROSMG,1000)
											GIVE_WEAPON_TO_PED(ped[ped_cop3].id,WEAPONTYPE_MICROSMG,1000)
											
											SET_PED_PROP_INDEX(ped[ped_cop4].id, ANCHOR_EYES, 0, 0)
											
											SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_cop2].id)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
											
											SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_cop3].id)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
											SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)

											
											TASK_START_SCENARIO_IN_PLACE(ped[ped_cop2].id,"WORLD_HUMAN_GUARD_STAND")
											TASK_START_SCENARIO_IN_PLACE(ped[ped_cop3].id,"WORLD_HUMAN_GUARD_STAND")
											TASK_START_SCENARIO_IN_PLACE(ped[ped_cop4].id,"WORLD_HUMAN_STAND_MOBILE_UPRIGHT")
											SET_PED_CAN_EVASIVE_DIVE(ped[ped_cop2].id,false)
											SET_PED_CAN_EVASIVE_DIVE(ped[ped_cop3].id,false)
											SET_PED_CAN_EVASIVE_DIVE(ped[ped_cop4].id,false)
											vehicle[vehSec1].id = CREATE_VEHICLE(TAILGATER,<<-984.58, -2998.80, 13.58>>,22.68)
											vehicle[vehSec2].id = CREATE_VEHICLE(TAILGATER,<<-992.74, -3031.48, 13.58>>,18.53)
											vehicle[vehSec3].id = CREATE_VEHICLE(TAILGATER,<<-990.95, -2997.46, 13.56>>,24.38)
											
											SET_FORCE_HD_VEHICLE(vehicle[vehSec1].id,TRUE)
											SET_FORCE_HD_VEHICLE(vehicle[vehSec3].id,true)
											
											
											
											
											IF missionProgress = STAGE_FINAL_CUT
												actions[thisI].completed = TRUE
											ELSE
												actions[thisI].flag++
											ENDIF
										ENDIF
										
										
										
									ENDIF
									
									//endif
								endif
							break
							
							case 5
								//IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<482.108490,-1312.055298,27.827969>>, <<482.908966,-1313.624878,30.640469>>, 0.937500)
									//actions[thisI].flag=5
								//ENDIF
//								
								//IF DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_F)) > 0.5
									//actions[thisI].flag=5
								//ENDIF
								
								IF IS_CONDITION_TRUE(COND_STOP_FOR_CORRECT)
								AND NOT IS_CONDITION_TRUE(COND_WANTED)
								
									actions[thisI].flag=6
								ENDIF
								//if car not outside or inside garage
								/*	IF car is outside or inside garage
										IF IS_ENTITY_IN_ANGLED_AREA() //car outside garage, player inside
											IF player in garage
												actions[thisI].flag = 2
											endif
										ENDIF
										
										IF garage door opening
											actions[thisI].flag = 2
										endif
									endif*/
								//ENDIF
								////cprintln(debug_Trevor3,"F: ",DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_F)) )
								////cprintln(debug_Trevor3,"S: ",DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_CARSTEAL_GARAGE_S)) )
							BREAK
							case 6
								IF NOT IS_PED_INJURED(pedDevin)
									IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
										SET_VEHICLE_DOORS_LOCKED(vehicle[vehHeist].id,VEHICLELOCK_LOCKED_PLAYER_INSIDE)
									ENDIF
									
									IF ( GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON )
										SET_GAMEPLAY_ENTITY_HINT(pedDevin,<<0,0,0.5>>,TRUE, -1, 7000)
						                SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.510)
						                SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.4)					                        
						                SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0)					                        
						                SET_GAMEPLAY_HINT_FOV(35.00)
									ENDIF
									
									iDevinSyncSCeneB = CREATE_SYNCHRONIZED_SCENE(devinCoord+<<0,0,1>>,<<0,0,fDevinHeading>>)
									TASK_SYNCHRONIZED_SCENE (pedDevin, iDevinSyncSCeneB, "misscarsteal2leadinoutcar_2_mcs_1", "_leadin_action", SLOW_BLEND_IN, INSTANT_BLEND_OUT )
									actions[thisI].flag++
								ENDIF
							break
							case 7
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CAR2_MCS1_LI",0,pedDevin,"Devin")
									SET_PLAYER_CONTROL(player_id(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
									actions[thisI].flag++
									actions[thisI].inta = GET_GAME_TIMER() + 2000
								ENDIF
							break
							case 8
								//IF IS_SYNCHRONIZED_SCENE_RUNNING(iDevinSyncSCeneB)
									//IF GET_SYNCHRONIZED_SCENE_PHASE(iDevinSyncSCeneB) > 0.8
									IF GET_GAME_TIMER() > actions[thisI].inta
										seq()
											TASK_LEAVE_ANY_VEHICLE(null)
											TASK_TURN_PED_TO_FACE_ENTITY(null,pedDevin,-1)
										endseq(player_ped_id())
											actions[thisI].flag++
									endif
									
							break
						
						ENDSWITCH
					BREAK
					
					CASE ACT_CONTROL_CHOPPER
						IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
						AND NOT IS_PED_INJURED(ped[ped_pilot].id)
							switch actions[thisI].flag
								case 0
									
									FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,FALSE)
								
									TASK_HELI_MISSION(ped[ped_pilot].id, vehicle[vehChopper].id,null,null,<<-1316.7277, -233.5309, 53.7461>>,MISSION_GOTO,4,3,-54,20,20)
									actions[thisI].flag++
								BREAK
								case 1
									IF IS_CONDITION_TRUE(COND_CHAD_PLAYER_100m_FROM_CAR_PARK)
										TASK_HELI_MISSION(ped[ped_pilot].id, vehicle[vehChopper].id,null,null,<<-1067.6364, -2946.3630, 70.9524>>,MISSION_GOTO,20,10,0,100,60)
										actions[thisI].completed = TRUE
									ENDIF
								break
							ENDSWITCH
						ENDIF								
					BREAK
					
					CASE ACT_STOP_ZTYPE_IN_HANGAR
						IF actions[thisI].flag = 0
							TRIGGER_MUSIC_EVENT("CAR2_STOP")
							actions[thisI].flag = 1
						ENDIF
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehicle[vehHeist].id,6.0,2)
							FORCE_INSTRUCTION_STATE(3,INS_DRIVE_TO_DESTINATION)
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							
							REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
					CASE ACT_RETURN_CONTROL_TO_PLAYER
						IF NOT actions[thisI].completed
						//	SET_PLAYER_CONTROL(player_id(),TRUE)
						//		actions[thisI].completed = TRUE
							IF IS_PLAYER_PRESSING_A_CONTROL_BUTTON()
								CLEAR_PED_TASKS(player_ped_id())
								SET_PLAYER_CONTROL(player_id(),TRUE)
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF
					BREAK
					
					CASE ACT_STAT_CS2_SCANMAN
						IF NOT IS_CONDITION_TRUE(COND_SEE_WANKER)
						AND NOT IS_CONDITION_TRUE(COND_SEE_FIXING_MAN)
						AND NOT IS_CONDITION_TRUE(COND_SEE_LEANING_MAN)
						AND NOT IS_CONDITION_TRUE(COND_SEE_PHONE_CAR_MAN)
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CS2_SCANMAN)
						ENDIF
						actions[thisI].completed = TRUE
					BREAK
					
					case ACT_LOAD_END_CUTSCENE
						switch actions[thisI].flag
							case 0							
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),devinCoord) < DEFAULT_CUTSCENE_LOAD_DIST
									IF NOT IS_PED_INJURED(pedDevin)
									AND NOT IS_PED_INJURED(ped[ped_cop2].id)
									AND NOT IS_PED_INJURED(ped[ped_cop3].id)
									//cprintln(debug_trevor3,"REQUEST CUT A")
									//REQUEST_CUTSCENE("Car_2_mcs_1")
										REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("Car_2_mcs_1", CS_SECTION_2)
									ENDIF

									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
							
								IF NOT IS_PED_INJURED(pedDevin)
								AND NOT IS_PED_INJURED(ped[ped_cop2].id)
								AND NOT IS_PED_INJURED(ped[ped_cop3].id)
									IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()				
										//cprintln(debug_trevor3,"CUT A")
										SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Devin",pedDevin)				
										SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())
										SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Car_2_Security", ped[ped_cop2].id)
										SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Car_2_Security^1", ped[ped_cop3].id)
									ENDIF
								ENDIF
							
								IF HAS_CUTSCENE_LOADED()
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 2
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),devinCoord) > DEFAULT_CUTSCENE_LOAD_DIST + 100
									REMOVE_CUTSCENE()
									actions[thisI].flag = 0
								ENDIF
							BREAK
						ENDSWITCH
					break
					
				
					
					CASE ACT_MAKE_PLAYER_WALK
						SWITCH actions[thisI].flag
							CASE 0
								IF IS_CUTSCENE_PLAYING()
									actions[thisI].flag++
								ENDIF
							BREAK
							CASE 1
							
								if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin") OR HAS_CUTSCENE_FINISHED() //for shit skip to this stage	
								
									IF NOT IS_CUTSCENE_PLAYING() //if player shit skipped here
								
										FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK)
									ELSE
									
										FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
									ENDIF
									SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
									
									//TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),<< -1123.6796, -1596.0865, 3.3602 >>,pedmove_run)
									actions[thisI].flag=2
									actions[thisI].intA = GET_GAME_TIMER() + 1000
								ENDIF
							BREAK
							CASE 2
				
								if GET_GAME_TIMER() < actions[thisI].intA
									SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)								
								ELSE
									actions[thisI].completed = TRUE
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE ACT_OPEN_EXIT_GATE
						IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_OUT, PLAYER_PED_ID()) 
                        	REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_OUT, PLAYER_PED_ID())  
							actions[thisI].completed = TRUE
                        ENDIF												
					BREAK
					
					CASE ACT_TURN_PLANE_ENGINE_ON
						IF IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)
							SET_VEHICLE_ENGINE_ON(vehicle[vehJet].id,TRUE,FALSE)
							actions[thisI].completed = TRUE
						ENDIF
					BREAK
					
				ENDSWITCH
				
				IF actions[thisI].flag = CLEANUP
					actions[thisI].completed = TRUE
					actions[thisI].flag = 0
					actions[thisI].active = FALSE
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC ACTION_ON_DIALOGUE(int thisI, enumActions thisAction, int diaEntry, enumDialogue thisDia)
	IF actions[thisI].action != thisAction
	OR actions[thisI].active = FALSE
		actions[thisI].action = thisAction
		actions[thisI].active = TRUE
		actions[thisI].completed = FALSE		
		actions[thisI].flag = 0
		actions[thisI].ongoing = FALSE
		actions[thisI].needsCleanup = FALSE
		actions[thisI].intA = 0
	ENDIF
	
	IF actions[thisI].completed = FALSE
		IF dia[diaEntry].bCompleted
			thisDia=thisDia
			action(thisI,thisAction)
		ENDIF
	ENDIF
ENDPROC

PROC RESET_ACTIONS(enumActions persistThisAction=ACT_NULL)
	int i
	REPEAT COUNT_OF(actions) i
		IF actions[i].action != persistThisAction
		OR actions[i].action = ACT_NULL
			//tell action to play its cleanup flag
			IF actions[i].needsCleanup
				actions[i].active = TRUE
				actions[i].flag = CLEANUP
				ACTION(i,actions[i].action)
			ENDIF
			
			//reset action
			actions[i].action = ACT_NULL
			actions[i].active = FALSE
			actions[i].ongoing = FALSE
			actions[i].completed = FALSE
			actions[i].flag = 0
			actions[i].needsCleanup = FALSE
			actions[i].intA = 0
			actions[i].intB = 0
			actions[i].trackCondition = FALSE
			actions[i].floatA = 0
		ENDIF
	ENDREPEAT
ENDPROC





// ================================================ SETUP Instructions ========================================





PROC RESET_INSTRUCTIONS()
	int i
	REPEAT COUNT_OF(instr) i
		instr[i].ins = INS_NULL
		instr[i].bCompleted = FALSE
		instr[i].flag = 0
		instr[i].intA = 0		
	ENDREPEAT
ENDPROC



PROC SET_INSTRUCTION_FLAG(int thisI, enumInstructions thisInstruction, int iFlagValue)
	IF instr[thisI].ins = thisInstruction
		instr[thisI].flag = iFlagValue		
	ELSE
		IF instr[thisI].ins != INS_NULL
			TEXT_LABEL_63 txt
			txt = ""
			txt += "IS_INS_COM() fail:"
			txt += GET_INSTRUCTION_STRING(thisInstruction)
			
			SCRIPT_ASSERT(txt)
			//cprintln(debug_trevor3,"FAIL: instr[thisI].ins = ",GET_INSTRUCTION_STRING(instr[thisI].ins))
		ENDIF		
	ENDIF
ENDPROC

FUNC INT GET_INSTRUCTION_FLAG(int thisI, enumInstructions thisInstruction)
	IF instr[thisI].ins = thisInstruction
		RETURN instr[thisI].flag
	ELSE
		IF instr[thisI].ins != INS_NULL
			TEXT_LABEL_63 txt
			txt = ""
			txt += "IS_INS_COM() fail:"
			txt += GET_INSTRUCTION_STRING(thisInstruction)
			
			SCRIPT_ASSERT(txt)
			//cprintln(debug_trevor3,"FAIL: instr[thisI].ins = ",GET_INSTRUCTION_STRING(instr[thisI].ins))
		ENDIF		
	ENDIF
	RETURN 0
ENDFUNC

PROC FORCE_DIALOGUE_STATE(int thisI, enumDialogue thisDialogue, bool setCompleted=TRUE)
	dia[thisI].bCompleted = setCompleted
	dia[thisI].dial = thisDialogue
ENDPROC


FUNC BOOL IS_INSTRUCTION_COMPLETE(int thisI, enumInstructions thisInstruction)
	IF instr[thisI].ins = thisInstruction
		IF instr[thisI].bCompleted
			RETURN TRUE
		ENDIF
	ELSE
		IF instr[thisI].ins != INS_NULL
			TEXT_LABEL_63 txt
			txt = ""
			txt += "IS_INS_COM() fail:"
			txt += GET_INSTRUCTION_STRING(thisInstruction)
			
			SCRIPT_ASSERT(txt)
			//cprintln(debug_trevor3,"FAIL: instr[thisI].ins = ",GET_INSTRUCTION_STRING(instr[thisI].ins))
		ENDIF		
	ENDIF
	RETURN FALSE
ENDFUNC



//======== dialogue =================

PROC RESET_DIALOGUE()
	int i
	REPEAT COUNT_OF(dia) i
		dia[i].dial = DIA_NULL
		dia[i].bCompleted = FALSE
		dia[i].bStarted = FALSE
		dia[i].flag = 0
		dia[i].intA = 0
	ENDREPEAT
ENDPROC


FUNC BOOL IS_DIALOGUE_COMPLETE(int iEntry, enumDialogue thisDia)
	
	IF dia[iEntry].dial = thisDia	
		IF dia[iEntry].bCompleted
			RETURN TRUE
		ENDIF
	ENDIF		

	RETURN FALSE
ENDFUNC





// =================================================== MAIN PROCS START HERE ========================================

func bool isalive(int pedID)
	if not IS_PED_INJURED(ped[pedID].id)
		return TRUE
	ENDIF
	return FALSE
ENDFUNC



bool bDisplayBeam





//check if a sync scene is in view




// ********************************** overhear scene audio from chopper ****************************************

enum scene_ID
	scene_mugging,
	scene_chadgirl
endenum

struct sceneAudioStruct
	bool active
	vector location
	int line = -1
endstruct

sceneAudioStruct sceneAudio[2]

FUNC int get_start_line(scene_ID thisScene)
	int iReturnLine = -1
	int ij
	float fTimings[24]
	float fSyncTime = -1.0
	int iLastLine = -1
	
	repeat count_of(fTimings) ij
		fTimings[ij] = 0
	ENDREPEAT
	switch thisScene
		case scene_mugging
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId[1])				
				fSyncTime= GET_SYNCHRONIZED_SCENE_PHASE(sceneId[1])
				fTimings[0] = 0.191
				fTimings[1] = 0.21
				fTimings[2] = 0.23
				fTimings[3] = 0.25
				fTimings[4] = 0.3
				fTimings[5] = 0.334
				fTimings[6] = 0.363
				fTimings[7] = 0.391
				fTimings[8] = 0.417				
			ENDIF
			iLastLine = 8
		break
		case scene_chadgirl
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId[3])				
				fSyncTime= GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3])
				fTimings[0] = 0.1 //I know you're going off to work with all those pretty woman...but.
				fTimings[1] = 0.15 //Mhmm.  At the brothel.
				fTimings[2] = 0.175 //You can't be thinking about any of them but me.
				fTimings[3] = 0.21 //I'll see you later okay?  I love you.
				fTimings[4] = 0.241 //Okay...wait...no come back...
				fTimings[5] = 0.284 //Tell me something nice about myself.
				fTimings[6] = 0.319 //Your eyes almost go together perfectly...Almost
				fTimings[7] = 0.37 //Thats...thats all you got?
				fTimings[8] = 0.42 //I love you.
				fTimings[9] = 0.46 //But just wait...Make it a little...say that with a smile.
				fTimings[10] = 0.522 //You look like my Grandma did in 1920.  I love you
				fTimings[11] = 0.567 //I dont know what to make of that one but....
				fTimings[12] = 0.614 //I really appreciate the thought.
				fTimings[13] = 0.65 //Okay.
				fTimings[14] = 0.68 //Okay.
				fTimings[15] = 0.73 //Alright.
				fTimings[16] = 0.76 //Alright.
				fTimings[17] = 0.794 //Come here. 0.794
				fTimings[18] = 0.821 //See you later alright?
				fTimings[19] = 0.85 //Have a great day.
				fTimings[20] = 0.868 //Take it easy baby.
				fTimings[21] = 0.89 //Okay
				fTimings[22] = 0.931//Keep it tight.
				fTimings[23] = 0.956 //Eeeww.  Get the fuck out of here.  Jerk.
			ENDIF
			iLastLine = 23
		break
	endswitch
	
	repeat count_of(fTimings) ij
		IF fTimings[ij] != 0 OR (ij = 0)
			IF fSyncTime >= fTimings[ij]
			and fSyncTime <= fTimings[ij] + 0.02
				iReturnLine = ij
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iLastLine = -1
		SCRIPT_ASSERT("get_start_line() iLastLine not set")
		return -1
	ENDIF
	
	IF fSyncTime = -1.0
		return -1
	ENDIF
	
	IF fSyncTime > fTimings[iLastLine] + 0.02
		return -2 //the dialogue can no longer play
	ELSE
		return iReturnLine
	ENDIF
endfunc

func int get_scene_index(scene_ID thisScene)
	switch thisScene
		case scene_mugging
			return 0
		break
		case scene_chadgirl
			return 1
		break
	ENDSWITCH
	SCRIPT_ASSERT("get_scene_index() : scene not assigned here")
	return 0
endfunc



func text_label_23 buildConvLine(string convRoot,int iLineToStartAt)
	text_label_23 txt = ""
	txt += convRoot
	txt += "_"
	txt += iLineToStartAt
	return txt
endfunc

func bool has_scene_audio_finished(scene_ID thisScene)
	if get_start_line(thisScene) = -2
		return true
	endif
	return FALSE
endfunc

//AUDIO FOR SCENES HEARD THROUGH CHOPPER CAM

vector chopperListeningLoc[7]

FUNC INT ADD_CHOPPER_LISTENING_LOCATION(int iEntry, vector vLocation)
	chopperListeningLoc[iEntry] = vLocation
	RETURN iEntry
ENDFUNC

PROC REMOVE_CHOPPER_LISTENING_LOCATION(int iEntry)
	chopperListeningLoc[iEntry] = <<0,0,0>>
ENDPROC


PROC CHOPPER_LISTENING()
	IF IS_HELIHUD_ACTIVE(HUD)
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()	//B* 2446396
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RB)
		
			IF currentConvType = CONVTYPE_CHOPPER_CAM				
				
				IF listenDialogue != -1
					IF missionProgress < STAGE_CHASE_BEGINS
						IF NOT IS_BIT_SET(iStoreStatBit,listenDialogue)
							//cprintln(debug_Trevor3,"CONVO STAT: ",listenDialogue)
							SET_BIT(iStoreStatBit,listenDialogue)							
							INFORM_MISSION_STATS_OF_INCREMENT(CS2_EAVESDROPPER)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iStoreStatBit,listenDialogue+7)
							SET_BIT(iStoreStatBit,listenDialogue+7)							
							INFORM_MISSION_STATS_OF_INCREMENT(CS2_EAVESDROPPER)
							//cprintln(debug_Trevor3,"CONVO STAT: ",listenDialogue)
						ENDIF
					ENDIF
				ENDIF
			
				IF NOT bWaveformDisplayingLoud
					BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_AUDIO_STATES")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(false)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(false)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(true)
					END_SCALEFORM_MOVIE_METHOD()
					bWaveformDisplayingQuiet = FALSE
					bWaveformDisplayingLoud = TRUE																				
				ENDIF		
			ENDIF
			
			
			If NOT bWaveformDisplayingQuiet
				IF currentConvType != CONVTYPE_CHOPPER_CAM
					BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_AUDIO_STATES")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(false)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(true)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(false)
					END_SCALEFORM_MOVIE_METHOD()
					bWaveformDisplayingQuiet = TRUE
					bWaveformDisplayingLoud = FALSE	
				ENDIF
			ENDIF
			
			IF StaticSoundID = -1 
				StaticSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(StaticSoundID,"Microphone","POLICE_CHOPPER_CAM_SOUNDS")
				START_AUDIO_SCENE("CAR_2_USE_MICROPHONE")
			ENDIF
			
			int j
		//	vector vPoint
		//	float fDistToDialogue
			float fStaticVolume
			float closestListenerRange = 1000000
			listenDialogue = -1
			
			int bestListenLoc
			int ListenCount
			
			REPEAT COUNT_OF(chopperListeningLoc) j
				//vector vExtended = HUD.vAimAt + ((HUD.vAimAt - GET_ENTITY_COORDS(HUD.vehChopper,FALSE)) * 10.0)
				//GET_CLOSEST_POINT_ON_LINE(vPoint,chopperListeningLoc[j],GET_ENTITY_COORDS(HUD.vehChopper,FALSE),vExtended)
				IF NOT IS_VECTOR_ZERO(chopperListeningLoc[j])
					IF IS_SPHERE_VISIBLE(chopperListeningLoc[j],5)									
						IF HUD.fFOV < 15
							int xi,yi
							ListenCount = 0
							for xi = -1 to 1
								for yi = -1 to 1
									IF IS_SPHERE_VISIBLE(chopperListeningLoc[j]+<<xi * 15, 0, yi*15>>,5)	
										ListenCount++
									ENDIF								
								endfor
							endfor
							
							If ListenCount > bestListenLoc
								listenDialogue = j
								bestListenLoc = ListenCount
							ENDIF
						ENDIF
					ENDIF
				ENDIF			
			ENDREPEAT
			
			
	
			IF closestListenerRange !=  1000000
				fStaticVolume = closestListenerRange/20.0
			ELSE
				fStaticVolume = 1.0
			ENDIF
			//float fDistToDialogue = GET_DISTANCE_BETWEEN_COORDS(vPoint,chopperListeningLoc[j])				
			
			
			
			clamp(fStaticVolume,0,1)
			SET_VARIABLE_ON_SOUND(StaticSoundID,"Ctrl", fStaticVolume)
		ELSE
			If bWaveformDisplayingLoud
			OR bWaveformDisplayingQuiet
				BEGIN_SCALEFORM_MOVIE_METHOD(mov,"SET_AUDIO_STATES")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(true)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(false)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(false)
				END_SCALEFORM_MOVIE_METHOD()
				bWaveformDisplayingLoud = FALSE			
				bWaveformDisplayingQuiet = FALSE
			ENDIF
			
			If currentConvType = CONVTYPE_CHOPPER_CAM
				cprintln(debug_trevor3,"kill conv H ")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				currentConvType = CONVTYPE_NULL
			ENDIF
			
			IF StaticSoundID != -1 
				STOP_SOUND(StaticSoundID)
				STOP_AUDIO_SCENE("CAR_2_USE_MICROPHONE")
				StaticSoundID = -1 
			ENDIF
			listenDialogue = -1
			
		ENDIF
	ELSE
		IF StaticSoundID != -1 
			STOP_SOUND(StaticSoundID)
			STOP_AUDIO_SCENE("CAR_2_USE_MICROPHONE")
			StaticSoundID = -1 
		ENDIF
	ENDIF
ENDPROC



PROC play_scene_audio(scene_ID thisScene)
	int scene = get_scene_index(thisScene)
	bool sceneIsActive = FALSE

	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RB)
		IF StaticSoundID = -1 
			StaticSoundID = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(StaticSoundID,"Microphone","POLICE_CHOPPER_CAM_SOUNDS")
		ENDIF
		
		
		float fDistToDialogue = GET_DISTANCE_BETWEEN_COORDS(HUD.vAimAt,sceneAudio[scene].location)				
		float zoomStaticVolumeAdjust = hud.fFOV / 20
		float fStaticVolume = 0.0 + (fDistToDialogue/20.0) + zoomStaticVolumeAdjust
		
		clamp(fStaticVolume,0,1)
		SET_VARIABLE_ON_SOUND(StaticSoundID,"Ctrl", fStaticVolume)
		
		if hud.fFOV < 13.4
			
		
			if fDistToDialogue < 20.0
				IF NOT sceneAudio[scene].active
					IF currentConvType = CONVTYPE_CHOPPER_CAM
						KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)	
						KILL_ANY_CONVERSATION()						
					ENDIF
					sceneAudio[scene].active = TRUE
					sceneIsActive = TRUE
				ELSE
					sceneIsActive = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//find line to start dialogue
						int iLineToStartAt
						iLineToStartAt = get_start_line(thisScene)
						IF iLineToStartAt > sceneAudio[scene].line
							TEXT_LABEL_23 convLine
							convLine = ""
							switch thisScene
								case scene_mugging
									convLine = buildConvLine("CAR_2_IG_4",iLineToStartAt+1)
									If PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"CAR_2_IG_4",convLine,5,ped[ped_mugger].id,"cs2_mugger")
									//if CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA("CAR_2_IG_4",convLine,5,ped[ped_mugger].id,"cs2_mugger")
										sceneAudio[scene].active = TRUE
										sceneIsActive = TRUE
									ENDIf					
								break
								case scene_chadgirl
									convLine = buildConvLine("CAR_2_IG_7",iLineToStartAt+1)
									if PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"CAR_2_IG_7",convLine,3,ped[ped_chad].id,"Chad",4,ped[ped_chadGirl].id,"cs2_girlfriend")
										sceneAudio[scene].active = TRUE
										sceneIsActive = TRUE
									ENDIf
								break
							ENDSWITCH
						endif		
					endif
				ENDIF
			endif
		ENDIF
	ELSE
		IF StaticSoundID != -1 
			STOP_SOUND(StaticSoundID)
			StaticSoundID = -1 
		ENDIF
	ENDIF
	
	//track dialogue
	switch thisScene
		case scene_mugging
			IF IS_CONV_ROOT_PLAYING("CAR_2_IG_4")
				sceneAudio[scene].line = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
			ENDIF
		break
		case scene_chadgirl
			IF IS_CONV_ROOT_PLAYING("CAR_2_IG_7")
				sceneAudio[scene].line = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
			ENDIF
		break
	ENDSWITCH
	
	IF NOT sceneIsActive
		IF sceneAudio[scene].active
			sceneAudio[scene].active = FALSE
			IF currentConvType = CONVTYPE_CHOPPER_CAM
				KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)
			ENDIF
		ENDIF		
	ENDIF

ENDPROC


float fSpeed
proc SET_CHOPPER_TO_TARGET(vector vTarget, vector vRotation, float maxSpeed=12.0)	
	if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)	
		
		SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
		vector vPos = GET_ENTITY_COORDS(vehicle[vehChopper].id)
		vector vPosDiff = vTarget - vPos
		float ftargetSpeed = CLAMP(GET_DISTANCE_BETWEEN_COORDS(vTarget,vPos) / 12.0,maxSpeed*-1.0,maxSpeed)
		fSpeed += (ftargetSpeed-fSpeed)*TIMESTEP()
		vPosDiff = GET_VECTOR_OF_LENGTH(vPosDiff,fSpeed)
		SET_ENTITY_VELOCITY(vehicle[vehChopper].id,vPosDiff)
		vector vDiff
		if IS_VECTOR_ZERO(vRotation)
			float rotFromVel = GET_HEADING_FROM_VECTOR_2D(vPosDiff.x,vPosDiff.y)
			vector vrotFromVel = <<0,0,rotFromVel>>

			//vector vChopperRot = GET_ENTITY_ROTATION(vehicle[vehChopper].id)

			vDiff = vrotFromVel-GET_ENTITY_ROTATION(vehicle[vehChopper].id)
			if vDiff.x < -180.0 vDiff.x += 360.0 ENDIF
			if vDiff.x > 180.0 vDiff.x -= 360.0 ENDIF
			if vDiff.y < -180.0 vDiff.y += 360.0 ENDIF
			if vDiff.y > 180.0 vDiff.y -= 360.0 ENDIF
			if vDiff.z < -180.0 vDiff.z += 360.0 ENDIF
			if vDiff.z > 180.0 vDiff.z -= 360.0 ENDIF
			

			
			SET_ENTITY_ROTATION(vehicle[vehChopper].id,GET_ENTITY_ROTATION(vehicle[vehChopper].id)+(vDiff*TIMESTEP()))
		ELSE
			vDiff = vRotation-GET_ENTITY_ROTATION(vehicle[vehChopper].id)
			if vDiff.x < -180.0 vDiff.x += 360.0 ENDIF
			if vDiff.x > 180.0 vDiff.x -= 360.0 ENDIF
			if vDiff.y < -180.0 vDiff.y += 360.0 ENDIF
			if vDiff.y > 180.0 vDiff.y -= 360.0 ENDIF
			if vDiff.z < -180.0 vDiff.z += 360.0 ENDIF
			if vDiff.z > 180.0 vDiff.z -= 360.0 ENDIF
			SET_ENTITY_ROTATION(vehicle[vehChopper].id,GET_ENTITY_ROTATION(vehicle[vehChopper].id)+(vDiff*TIMESTEP()))
		ENDIF
		
	ENDIF
ENDPROC

proc set_bezier_node(bezier &aBez, float time, vector vPos, vector vControl, vector Rot)
	aBez.fTime = time
	aBez.vPos = vpos
	aBez.vcontrol = vControl
	aBez.Rot = rot
endproc



proc getBezCoord(float time, bezier bez1, bezier bez2, vector &vReturn)  //vector v0, vector v1, vector v2, vector v3, float t)	
	//outputDebugVector("v0:",bez1.vPos)
	//outputDebugVector("v1:",bez2.vPos)
	vector v0,v1,v2,v3,vR		
	float t=time-to_float(floor(time))	
	if not are_vectors_equal(bez1.vpos,bez2.vpos)		
		v0 = bez1.vPos
		v1 = bez1.vPos + bez1.vControl //bezVcontrol[bezNode]
		v2 = bez2.vPos - bez2.vControl
		v3 = bez2.vPos
			
		vR.x = (((1-t)*(1-t)*(1-t))*v0.x) + (3*((1-t)*(1-t))*t*v1.x) + (3*(1-t)*(t*t)*v2.x) + ((t*t*t)*v3.x)
		vR.y = (((1-t)*(1-t)*(1-t))*v0.y) + (3*((1-t)*(1-t))*t*v1.y) + (3*(1-t)*(t*t)*v2.y) + ((t*t*t)*v3.y)
		vR.z = (((1-t)*(1-t)*(1-t))*v0.z) + (3*((1-t)*(1-t))*t*v1.z) + (3*(1-t)*(t*t)*v2.z) + ((t*t*t)*v3.z)			
		vReturn =  vR		
	ELSE	

		vReturn = bez1.vpos			
	ENDIF
endproc

proc GET_BEZIER_ROT(float fT, bezier bez1, bezier bez2, vector &vReturn)	
	float t=ft-to_float(floor(ft))
	vector fr0,fr1,fr2,fr3
	vector nc1	
	float rDamp=0.3	
	float fFactorLength = 1.0
		
	fr0 = bez1.rot
	fr3 = bez2.rot
		

	Nc1 = ((bez2.rot - bez1.rot) / 2.0) //+ (bezRot[node] - bezRot[node+1])
	fr2 = fr3 - (Nc1 * rDamp)
	fr1 = fr0 + ((((bez2.rot - bez1.rot)*2.0) - Nc1) * rDamp * fFactorLength)

		
	vReturn = (((1-t)*(1-t)*(1-t))*fr0) + (3*((1-t)*(1-t))*t*fr1) + (3*(1-t)*(t*t)*fr2) + ((t*t*t)*fr3)

ENDPROC

int iEntry=0
proc add_to_vehicle_recorder(int recording=-1, int iStartTime=0, MODEL_NAMES modelName=DUMMY_MODEL_FOR_SCRIPT, bool bIsHeistCar=FALSE, bool bIsChaseCar=FALSE)
	recordingData[iEntry].model = modelName	
	recordingData[iEntry].startPlaybackTime = iStartTime
	recordingData[iEntry].isHeistCar = bIsHeistCar
	recordingData[iEntry].isChaseCar = bIsChaseCar
	recordingData[iEntry].recording=recording
	iEntry++
ENDPROC

proc prepCarRecData()

	add_to_vehicle_recorder(400,0,ztype,true,FALSE)
	add_to_vehicle_recorder(401,25200,ztype,true,FALSE)
	add_to_vehicle_recorder(402,44199,ztype,true,FALSE)
	add_to_vehicle_recorder(3,1495,futo)
	add_to_vehicle_recorder(4,1500,sentinel)
	add_to_vehicle_recorder(5,2000,bison)
	add_to_vehicle_recorder(6,1517,baller)
	add_to_vehicle_recorder(7,0,sentinel)
	add_to_vehicle_recorder(8,3494,sentinel)
	add_to_vehicle_recorder(9,4555,GRESLEY)
	add_to_vehicle_recorder(10,7849,bison)
	add_to_vehicle_recorder(11,4765,boxville2)
	add_to_vehicle_recorder(12,12735,sentinel)
	add_to_vehicle_recorder(13,11335,baller)
	add_to_vehicle_recorder(14,12165,bison)
	add_to_vehicle_recorder(15,10775,bus)
	add_to_vehicle_recorder(16,770,sentinel)
	add_to_vehicle_recorder(17,4245,GRESLEY)
	add_to_vehicle_recorder(18,9620,futo)
	add_to_vehicle_recorder(19,12525,bison)
	add_to_vehicle_recorder(20,16675,sentinel)
	add_to_vehicle_recorder(21,5000,GRESLEY)
	add_to_vehicle_recorder(22,6986,futo)
	add_to_vehicle_recorder(23,5535,sentinel)
	add_to_vehicle_recorder(24,31515,sentinel)
	add_to_vehicle_recorder(25,33480,futo)
	add_to_vehicle_recorder(26,26530,bus)
	add_to_vehicle_recorder(27,29065,GRESLEY)
	add_to_vehicle_recorder(28,28000,baller)
	add_to_vehicle_recorder(29,29708,boxville2)
	add_to_vehicle_recorder(30,32755,sentinel)
	add_to_vehicle_recorder(31,49000,sentinel)
	add_to_vehicle_recorder(32,26918,benson)
	add_to_vehicle_recorder(33,40450,futo)
	add_to_vehicle_recorder(34,43000,granger)
	add_to_vehicle_recorder(35,41520,sentinel)
	add_to_vehicle_recorder(36,45560,sentinel)
	add_to_vehicle_recorder(37,34500,sentinel)
	add_to_vehicle_recorder(39,52835,benson)
	add_to_vehicle_recorder(40,51905,sentinel)
	add_to_vehicle_recorder(41,51850,granger)
	add_to_vehicle_recorder(42,51000,granger)
	add_to_vehicle_recorder(43,54000,boxville2)
	add_to_vehicle_recorder(44,54250,baller)
	add_to_vehicle_recorder(45,54750,futo) //crossing
	add_to_vehicle_recorder(46,51600,sentinel) //test
	add_to_vehicle_recorder(47,53320,benson)
	add_to_vehicle_recorder(48,53390,sentinel)
	add_to_vehicle_recorder(49,65630,bus)
	add_to_vehicle_recorder(50,65475,bus)
	add_to_vehicle_recorder(51,65100,GRESLEY)
	add_to_vehicle_recorder(52,65895,sentinel)
	add_to_vehicle_recorder(53,64880,baller)
	add_to_vehicle_recorder(54,65720,baller)
	add_to_vehicle_recorder(55,65950,futo)
	add_to_vehicle_recorder(56,65785,boxville2)
	add_to_vehicle_recorder(57,71795,granger)
	add_to_vehicle_recorder(58,69245,sentinel)
	add_to_vehicle_recorder(59,75870,sentinel)
	add_to_vehicle_recorder(60,78885,bus)
	add_to_vehicle_recorder(61,78480,baller)
	add_to_vehicle_recorder(62,80050,baller)
	add_to_vehicle_recorder(63,79685,futo)
	add_to_vehicle_recorder(64,76030,bus)
	add_to_vehicle_recorder(65,76463,GRESLEY)
	add_to_vehicle_recorder(66,77085,futo)
	add_to_vehicle_recorder(67,80240,futo)
	add_to_vehicle_recorder(68,94190,sentinel)
	add_to_vehicle_recorder(69,89525,bus)
	add_to_vehicle_recorder(70,89525,bison)
	add_to_vehicle_recorder(71,94110,boxville2)
	add_to_vehicle_recorder(100,6500,buffalo2,FALSE,true)
ENDPROC

bool bRecordingsLoaded
func bool loadCarRecData()
//	//cprintln(debug_trevor3,"load car recs")
	int i
	if not is_ped_injured(ped[ped_chad].id)
		if (missionProgress > STAGE_APPROACH_SCAN_AREA_THREE AND missionProgress < STAGE_CARPARK)
		OR (missionProgress = STAGE_APPROACH_SCAN_AREA_THREE AND IS_ENTITY_PLAYING_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad"))
		//	//cprintln(debug_trevor3,"a")
			if bRecordingsLoaded = FALSE
				cprintln(debug_trevor3,"Load veh recordings")
				REPEAT COUNT_OF(recordingData) i
					If i >= 0
					//	//cprintln(debug_trevor3,"aaa",i)
						IF recordingData[i].Recording >= 1
							REQUEST_VEHICLE_RECORDING(recordingData[i].Recording,sVehicleRecordingLibrary)
						ENDIF
					ENDIF
				ENDREPEAT
				
				REPEAT COUNT_OF(recordingData) i
					If i > 0
				//		//cprintln(debug_trevor3,"aab",i)
						IF recordingData[i].Recording >= 1
							if not HAS_VEHICLE_RECORDING_BEEN_LOADED(recordingData[i].Recording,sVehicleRecordingLibrary)
								//cprintln(debug_trevor3,"not loaded car rec: ",recordingData[i].Recording)
								return FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
						
				bRecordingsLoaded=true
				RETURN TRUE
			ELSE
				RETURN TRUE
			ENDIF
		ELSe
		//	//cprintln(debug_trevor3,"b")
			IF bRecordingsLoaded
				IF missionProgress < STAGE_APPROACH_SCAN_AREA_THREE
				OR missionProgress > STAGE_CARPARK
		
					cprintln(debug_trevor3,"Release veh recordings")
					REPEAT COUNT_OF(recordingData) i
						If i > 0
							IF recordingData[i].Recording >= 1
								if HAS_VEHICLE_RECORDING_BEEN_LOADED(recordingData[i].Recording,sVehicleRecordingLibrary)
									REMOVE_VEHICLE_RECORDING(recordingData[i].Recording,sVehicleRecordingLibrary)
								endif
							ENDIF
						ENDIF
					ENDREPEAT
					bRecordingsLoaded=FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC



proc addToChaseDialogue(int iThisEntry, int askTime, int replyByTime, int TrevorResponseTime, int chadRecordingToCheck, string conv_root, int conv_line, bool askForDirections, bool triggeredIfChadOnScreen, bool failForNotSeeing)
	chaseDialogue[iThisEntry].askTime = askTime
	chaseDialogue[iThisEntry].replyByTime = replyByTime
	chaseDialogue[iThisEntry].TrevorResponseTime = TrevorResponseTime
	chaseDialogue[iThisEntry].chadRecordingToCheck = chadRecordingToCheck
	chaseDialogue[iThisEntry].conv_root = conv_root
	chaseDialogue[iThisEntry].conv_line = conv_line
	chaseDialogue[iThisEntry].askForDirections = askForDirections
	chaseDialogue[iThisEntry].mustBeSeen = triggeredIfChadOnScreen
	chaseDialogue[iThisEntry].FailForNotSeeing = failForNotSeeing	
endproc



PROC INIT_ASSETS()
	INIT_REL_GROUPS()
	sVehicleRecordingLibrary = "CS2"
	convBlock = "CST2AUD"
	

//	model_trevor  = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
	#if IS_DEBUG_BUILD model_franklin = GET_PLAYER_PED_MODEL(CHAR_FRANKLIN) #endif
	model_chad = U_M_Y_FIBMugger_01
	model_pilot = S_M_Y_Pilot_01

	
	prepCarRecData()
	
	

	//prep chase dialogue data
	
	addToChaseDialogue(0,0,0,2000,400,"cs2_chase1",0,false,false,false)
	addToChaseDialogue(1,0,0,11000,400,"cs2_tmp18",1,false,true,false)
	addToChaseDialogue(2,0,0,16000,400,"cs2_tmp18",2,false,true,false)
	addToChaseDialogue(3,18302,22807,2000,401,"cs2_tmp18",3,true,false,true) //North up S La Conaga Blvd
	addToChaseDialogue(4,20000,0,0,0,"cs2_chase2",0,false,false,false) //banter
	addToChaseDialogue(5,37168,42022,2871,402,"cs2_tmp18",6,true,false,true) //Through parking lot alley
	addToChaseDialogue(6,46000,0,0,0,"cs2_chase3",0,false,false,false) //police intercom
	addToChaseDialogue(7,55000,0,0,0,"cs2_chase5",0,false,true,false) //nearly totalled
	addToChaseDialogue(8,59000,63008,17736,402,"cs2_tmp18",9,true,false,true) //north on rockford
	addToChaseDialogue(9,63519,0,0,0,"cs2_frksee",0,false,false,false) //I see him
	addToChaseDialogue(10,71500,0,0,0,"cs2_anger",0,false,false,false) //Motherfucker (weaves cars)
	addToChaseDialogue(11,73870,79000,36056,402,"cs2_tmp18",11,true,false,true) //santa monica blvd
	addToChaseDialogue(12,80000,0,0,0,"cs2_hurry",0,false,true,false)
	addToChaseDialogue(13,83101,85543,46911,402,"cs2_tmp18",12,true,false,true)
	addToChaseDialogue(14,91000,0,0,0,"cs2_chase4",0,false,true,false)
	addToChaseDialogue(15,91000,94000,57965,402,"cs2_tmp18",13,true,false,true)
	addToChaseDialogue(16,0,0,65698,402,"cs2_atend",0,false,true,false)
	
	PREP_ASSET(ASSET_Police_DEPT,ASSETS_STAGE_POLICE_STATION,ASSETS_STAGE_IN_CHOPPER)
	PREP_ASSET(ASSET_CHOPPER,ASSETS_STAGE_AFTER_LEADIN,ASSETS_STAGE_GAME_OVER)

	PREP_ASSET(ASSET_PILOT_AT_START,ASSETS_STAGE_AFTER_LEADIN,ASSETS_STAGE_IN_CHOPPER)
	PREP_ASSET(ASSET_AREA_1_ASSETS,ASSETS_STAGE_PREP_AREA_1,ASSETS_STAGE_RELEASE_AREA_1)
	PREP_ASSET(ASSET_AREA_2_ASSETS,ASSETS_STAGE_PREP_AREA_2,ASSETS_STAGE_RELEASE_AREA_2)
	PREP_ASSET(ASSET_CHAD,ASSETS_STAGE_PREP_AREA_2,ASSETS_STAGE_RELEASE_CAR_PARK)
	PREP_ASSET(ASSET_ZYPE,ASSETS_STAGE_PREP_AREA_2,ASSETS_STAGE_GAME_OVER)
	PREP_ASSET(ASSET_CARPARK,ASSETS_STAGE_PREP_CAR_PARK,ASSETS_STAGE_RELEASE_CAR_PARK)
	PREP_ASSET(ASSET_DEVIN,ASSETS_STAGE_RELEASE_CAR_PARK,ASSETS_STAGE_GAME_OVER)
	PREP_ASSET(ASSET_CHASE_AUDIO,ASSETS_STAGE_LOAD_ZTYPE,ASSETS_STAGE_END_CAR_CHASE)
	

		

		
	//***************************************** WHO ARE WE PLAYING AS? *******************************************
	
	IF GET_ENTITY_MODEL(player_ped_id()) = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_MAX_WANTED_LEVEL(0)
		playAsTrevor = TRUE
	ELSE
		playAsTrevor = FALSE
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_MAX_WANTED_LEVEL(0)
	ENDIF
	
	IF playAsTrevor = TRUE
		FORCE_ASSET_STAGE(ASSETS_STAGE_POLICE_STATION)
	ELSE
		FORCE_ASSET_STAGE(ASSETS_STAGE_STARTUP)
	ENDIF
		
	#if is_debug_build
	IF playAsTrevor = TRUE
		SkipMenuStruct[0].sTxtLabel = "Startup"  // Stage 0 Name                
		SkipMenuStruct[1].sTxtLabel = "Get police Chopper"  // Stage 3 Name
		SkipMenuStruct[2].sTxtLabel = "Learn to scan"  // Stage 4 Name
		SkipMenuStruct[3].sTxtLabel = "Approach scan area 1"  
		SkipMenuStruct[4].sTxtLabel = "Approach scan area 2"  
		SkipMenuStruct[5].sTxtLabel = "Chase begins" 
		SkipMenuStruct[6].sTxtLabel = "Car Park" 
		SkipMenuStruct[7].sTxtLabel = "Scan car park peds" 
		SkipMenuStruct[8].sTxtLabel = "Collect Chad's vehicle" 
		SkipMenuStruct[9].sTxtLabel = "Take Chad's vehicle to obj" 
		SkipMenuStruct[10].sTxtLabel = "Approach airport gates" 
		SkipMenuStruct[11].sTxtLabel = "Final Cutscene (car_2_mcs_1)" 
		SkipMenuStruct[12].sTxtLabel = "Game Over"
	ELSE //playing as Franklin
		

		SkipMenuStruct[0].sTxtLabel = "Startup"  // Stage 0 Name                
		SkipMenuStruct[1].sTxtLabel = "Franklin waits for Trevor"  // Stage 3 Name
		SkipMenuStruct[2].sTxtLabel = "Learn to scan"  // Stage 4 Name
		SkipMenuStruct[3].sTxtLabel = "Approach scan area 1"  
		SkipMenuStruct[4].sTxtLabel = "Approach scan area 2"  
		SkipMenuStruct[5].sTxtLabel = "Chase begins" 
		SkipMenuStruct[6].sTxtLabel = "Car Park" 
		SkipMenuStruct[7].sTxtLabel = "Scan car park peds" 
		SkipMenuStruct[8].sTxtLabel = "Collect Chad's vehicle" 
		SkipMenuStruct[9].sTxtLabel = "Take Chad's vehicle to obj" 
		SkipMenuStruct[10].sTxtLabel = "Approach airport gates" 
		SkipMenuStruct[11].sTxtLabel = "Final Cutscene (car_2_mcs_1)" 
		SkipMenuStruct[12].sTxtLabel = "Game Over"
	
	ENDIF
	#endif
	
	//set up scene audi data
	sceneAudio[0].active = FALSE
	sceneAudio[0].location = << -29.6222, -87.8161, 56.3652 >>
	sceneAudio[0].line = -1
	sceneAudio[1].active = FALSE
	sceneAudio[1].location = << 206.3808, -110.3547, 67.8803 >>
	sceneAudio[1].line = -1
	
//	FIX_CHOPPER_HUD_TIME(HUD,21,04)
	
ENDPROC




#IF IS_DEBUG_BUILD
proc setWidget()	
	carStealWidget = START_WIDGET_GROUP("Car Steal 5")
		ADD_WIDGET_BOOL("Display frame timer",bFrameTimer)
		ADD_WIDGET_BOOL("Condtions On Screen",bScreenConditions)
		ADD_WIDGET_BOOL("Dialogue On Screen",bscreenDialogue)
		ADD_WIDGET_BOOL("Actions 0-15",bScreenActions)
		ADD_WIDGET_BOOL("Actions 16+",bScreenActionsB)
		ADD_WIDGET_BOOL("Instructions On Screen",bScreenInstructions)
		ADD_WIDGET_INT_READ_ONLY("missionProgress",iMissionProg)
		ADD_WIDGET_INT_SLIDER("Event Flag",iFlag,0,9999,1)
		START_WIDGET_GROUP("Extra skips")
			ADD_WIDGET_BOOL("Skip chad along waypoint",bWidget_skipChadOnWaypoint)
			ADD_WIDGET_BOOL("skip chopper at carpark",bSkipChopper)
			ADD_WIDGET_BOOL("skip point in area check",bSkipcheck)
		STOP_WIDGET_GROUP()				
	
		
	STOP_WIDGET_GROUP()
	
	IF DOES_WIDGET_GROUP_EXIST(carStealWidget)
		SET_CURRENT_WIDGET_GROUP(carStealWidget)
		START_WIDGET_GROUP("Scenes")
			ADD_WIDGET_BOOL("Export",bSceneExport)
			ADD_WIDGET_BOOL("Reset scene",bResetScene)
			ADD_WIDGET_INT_SLIDER("Scene",iDebugScene,0,4,1)
			ADD_WIDGET_INT_SLIDER("Event",iDebugEvent,0,MAX_TIMELINES,1)
			ADD_WIDGET_INT_SLIDER("Event Time",iDebugTimer,0,240000,1)
		STOP_WIDGET_GROUP()	
		CLEAR_CURRENT_WIDGET_GROUP(carStealWidget)
	ENDIF
	
	setUpWidgets(HUD,carStealWidget)
	boltDebug(carStealWidget)
	SET_CURRENT_WIDGET_GROUP(carStealWidget)
	add_widget_bool("Skip Carpark Switch",bSkipSwitch)
	STOP_WIDGET_GROUP()
ENDPROC



PROC PRINT_CONDITION_DATA(int iCon)
	string sCond
	enumConditions thisCond = conditions[iCon].condition
	
	sCond = GET_CONDITION_STRING(thisCond)
	
		
	SET_TEXT_SCALE(0.25,0.25)
	IF conditions[iCon].returns		
		SET_TEXT_COLOUR(0,0,255,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iCon*0.02),"STRING",sCond)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.25,0.25)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iCon*0.02),"STRING","TRUE")
	ELSE
		
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iCon*0.02),"STRING",sCond)
		SET_TEXT_COLOUR(255,0,0,255)
		SET_TEXT_SCALE(0.25,0.25)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iCon*0.02),"STRING","FALSE")
	ENDIF

ENDPROC

PROC PRINT_DIALOGUE_DATA(int iDia, bool toDebugTextOnly = FALSE)
	string sDia
	enumDialogue thisDia = dia[iDia].dial
	SWITCH thisDia
		 
		CASE	DIA_NULL	sDia = "	DIA_NULL	" BREAK
CASE	DIA_CAN_I_HELP	sDia = "	DIA_CAN_I_HELP	" BREAK
CASE	DIA_INTEROGATE_COP	sDia = "	DIA_INTEROGATE_COP	" BREAK
CASE	DIA_COP_WALK_CHAT	sDia = "	DIA_COP_WALK_CHAT	" BREAK
CASE	DIA_COME_ON	sDia = "	DIA_COME_ON	" BREAK
CASE	DIA_COP_RETURNS_TO_DESK	sDia = "	DIA_COP_RETURNS_TO_DESK	" BREAK
CASE	DIA_PLAYER_GETS_IN_CHOPPER	sDia = "	DIA_PLAYER_GETS_IN_CHOPPER	" BREAK
CASE	DIA_PLAYER_LAGS_OR_LEAVES	sDia = "	DIA_PLAYER_LAGS_OR_LEAVES	" BREAK
CASE	DIA_PLAYER_THREAT_IN_POL_DEPT	sDia = "	DIA_PLAYER_THREAT_IN_POL_DEPT	" BREAK
CASE	DIA_PILOT_SEES_PLAYER	sDia = "	DIA_PILOT_SEES_PLAYER	" BREAK
CASE	DIA_PLAYER_ENTERS_LOCKER_ROOM	sDia = "	DIA_PLAYER_ENTERS_LOCKER_ROOM	" BREAK
CASE	DIA_WRONG_WAY	sDia = "	DIA_WRONG_WAY	" BREAK
CASE	DIA_RESTART_CONV	sDia = "	DIA_RESTART_CONV	" BREAK
CASE	dia_player_doesnt_switch	sDia = "	dia_player_doesnt_switch	" BREAK
CASE	DIA_BOOTING_UP	sDia = "	DIA_BOOTING_UP	" BREAK
CASE	DIA_SCAN_BUDDY_NEARBY	sDia = "	DIA_SCAN_BUDDY_NEARBY	" BREAK
CASE	DIA_CONV_ON_WAY_TO_FRANKLIN	sDia = "	DIA_CONV_ON_WAY_TO_FRANKLIN	" BREAK
CASE	DIA_SCAN_MY_BUDDY	sDia = "	DIA_SCAN_MY_BUDDY	" BREAK
CASE	DIA_FRANKLIN_SCANNED	sDia = "	DIA_FRANKLIN_SCANNED	" BREAK
CASE	DIA_GOING_TO_FIND_BUDDY	sDia = "	DIA_GOING_TO_FIND_BUDDY	" BREAK
CASE	DIA_ACTOR_LINE_PLAYS	sDia = "	DIA_ACTOR_LINE_PLAYS	" BREAK
CASE	DIA_FLYING_AWAY	sDia = "	DIA_FLYING_AWAY	" BREAK
CASE	DIA_WAITING_TO_SCAN	sDia = "	DIA_WAITING_TO_SCAN	" BREAK
CASE	DIA_TREVOR_COMMENTS_ON_POSTAL	sDia = "	DIA_TREVOR_COMMENTS_ON_POSTAL	" BREAK
CASE	DIA_TREVOR_COMMENTS_ON_PERVERT	sDia = "	DIA_TREVOR_COMMENTS_ON_PERVERT	" BREAK
CASE	DIA_TREVOR_COMMENTS_ON_PROSTITUTE	sDia = "	DIA_TREVOR_COMMENTS_ON_PROSTITUTE	" BREAK
CASE	DIA_TREVOR_COMMENTS_ON_CHADGIRL	sDia = "	DIA_TREVOR_COMMENTS_ON_CHADGIRL	" BREAK
CASE	DIA_TREVOR_MAKES_SCAN_COMPLETE_COMMENT	sDia = "	DIA_TREVOR_MAKES_SCAN_COMPLETE_COMMENT	" BREAK
CASE	DIA_SEEN_CHAD	sDia = "	DIA_SEEN_CHAD	" BREAK
CASE	DIA_CHAD_HIDDEN	sDia = "	DIA_CHAD_HIDDEN	" BREAK
CASE	DIA_CHAD_WALKING_THROUGH_APARTMENTS	sDia = "	DIA_CHAD_WALKING_THROUGH_APARTMENTS	" BREAK
CASE	DIA_CHAD_OFF_SCREEN	sDia = "	DIA_CHAD_OFF_SCREEN	" BREAK
CASE	DIA_FRANKLIN_FIND_HIM	sDia = "	DIA_FRANKLIN_FIND_HIM	" BREAK
CASE	DIA_FOUND_CHAD	sDia = "	DIA_FOUND_CHAD	" BREAK
CASE	DIA_TREVOR_BANTER_DURING_CHAD_WALK	sDia = "	DIA_TREVOR_BANTER_DURING_CHAD_WALK	" BREAK
CASE	DIA_CHAD_AT_GARAGE	sDia = "	DIA_CHAD_AT_GARAGE	" BREAK
CASE	DIA_TAKE_US_DOWN	sDia = "	DIA_TAKE_US_DOWN	" BREAK
CASE	DIA_TREVOR_SCARED	sDia = "	DIA_TREVOR_SCARED	" BREAK
CASE	DIA_EXITS_CLEAR	sDia = "	DIA_EXITS_CLEAR	" BREAK
CASE	DIA_THERMAL_VISION	sDia = "	DIA_THERMAL_VISION	" BREAK
CASE	DIA_TREVOR_REACTION_TO_THERMAL_VISION_ON	sDia = "	DIA_TREVOR_REACTION_TO_THERMAL_VISION_ON	" BREAK
CASE	DIA_FRANKLIN_ASKS_IF_PLAYER_CAN_SEE_HIM	sDia = "	DIA_FRANKLIN_ASKS_IF_PLAYER_CAN_SEE_HIM	" BREAK
CASE	DIA_TREVOR_SEE_FRANKLIN	sDia = "	DIA_TREVOR_SEE_FRANKLIN	" BREAK
CASE	DIA_ANY_OTHER_HEAT_SOURCES	sDia = "	DIA_ANY_OTHER_HEAT_SOURCES	" BREAK
CASE	DIA_TREVOR_SEES_PISSER	sDia = "	DIA_TREVOR_SEES_PISSER	" BREAK
CASE	DIA_TREVOR_SEES_FIXING_MAN	sDia = "	DIA_TREVOR_SEES_FIXING_MAN	" BREAK
CASE	DIA_TREVOR_SEES_CHAD	sDia = "	DIA_TREVOR_SEES_CHAD	" BREAK
CASE	DIA_TREVOR_SEES_LEANING_MAN	sDia = "	DIA_TREVOR_SEES_LEANING_MAN	" BREAK
CASE	DIA_TREVOR_SEES_PHONE_CAR_MAN	sDia = "	DIA_TREVOR_SEES_PHONE_CAR_MAN	" BREAK
CASE	DIA_FRANKLIN_SEES_PISSER	sDia = "	DIA_FRANKLIN_SEES_PISSER	" BREAK
CASE	DIA_FRANKLIN_SEES_FIXING_MAN	sDia = "	DIA_FRANKLIN_SEES_FIXING_MAN	" BREAK
CASE	DIA_FRANKLIN_SEES_ZTYPE	sDia = "	DIA_FRANKLIN_SEES_ZTYPE	" BREAK
CASE	DIA_FRANKLIN_SEES_LEANING_MAN	sDia = "	DIA_FRANKLIN_SEES_LEANING_MAN	" BREAK
CASE	DIA_FRANKLIN_SEES_PHONE_MAN	sDia = "	DIA_FRANKLIN_SEES_PHONE_MAN	" BREAK
CASE	DIA_OVERHEAR_FUCKERS_WITH_SCANNER	sDia = "	DIA_OVERHEAR_FUCKERS_WITH_SCANNER	" BREAK
CASE	DIA_OVERHEAR_CAR_FIXER_WITH_SCANNER	sDia = "	DIA_OVERHEAR_CAR_FIXER_WITH_SCANNER	" BREAK
CASE	DIA_OVERHEAR_PISSER_WITH_SCANNER	sDia = "	DIA_OVERHEAR_PISSER_WITH_SCANNER	" BREAK
CASE	DIA_OVERHEAR_CHAD_WITH_SCANNER	sDia = "	DIA_OVERHEAR_CHAD_WITH_SCANNER	" BREAK
CASE	DIA_OVERHEAR_CAR_LEANING_GUY	sDia = "	DIA_OVERHEAR_CAR_LEANING_GUY	" BREAK
CASE	DIA_OVERHEAR_CAR_ON_PHONE	sDia = "	DIA_OVERHEAR_CAR_ON_PHONE	" BREAK
CASE	DIA_TREVOR_RANTS_IN_SWITCH	sDia = "	DIA_TREVOR_RANTS_IN_SWITCH	" BREAK
CASE	DIA_PLAYER_SHOOTING_CHOPPER	sDia = "	DIA_PLAYER_SHOOTING_CHOPPER	" BREAK
CASE	DIA_DRIVING_BACK	sDia = "	DIA_DRIVING_BACK	" BREAK
CASE	DIA_CRASH_ZTYPE	sDia = "	DIA_CRASH_ZTYPE	" BREAK
CASE	DIA_WHERES_THE_CAR	sDia = "	DIA_WHERES_THE_CAR	" BREAK
CASE	DIA_BEG_TO_LET_GO	sDia = "	DIA_BEG_TO_LET_GO	" BREAK
CASE	DIA_DONT_TAKE_CAR	sDia = "	DIA_DONT_TAKE_CAR	" BREAK
CASE	DIA_FLEE_PANIC	sDia = "	DIA_FLEE_PANIC	" BREAK
CASE	DIA_FLEE_PANIC_NOT_HIT	sDia = "	DIA_FLEE_PANIC_NOT_HIT	" BREAK
CASE	DIA_PLAYER_GOT_IN_CAR	sDia = "	DIA_PLAYER_GOT_IN_CAR	" BREAK
CASE	DIA_DRIVING_AWAY	sDia = "	DIA_DRIVING_AWAY	" BREAK
CASE	DIA_WARNING_COPS	sDia = "	DIA_WARNING_COPS	" BREAK
CASE	DIA_KILLED_CHAD	sDia = "	DIA_KILLED_CHAD	" BREAK
CASE	DIA_CALL_DEVIN	sDia = "	DIA_CALL_DEVIN	" BREAK
CASE	DIA_OPEN_GATES	sDia = "	DIA_OPEN_GATES	" BREAK
CASE	DIA_FRANKLIN_THREATENS_CHAD	sDia = "	DIA_FRANKLIN_THREATENS_CHAD	" BREAK
CASE	DIA_PLAYER_RUNS_OFF	sDia = "	DIA_PLAYER_RUNS_OFF	" BREAK


		DEFAULT sDia = " -NOT DEFINED-" BREAK
	ENDSWITCH
	
	IF NOT toDebugTextOnly
		SET_TEXT_SCALE(0.35,0.35)
		IF dia[iDia].bCompleted		
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iDia*0.03),"STRING",sDia)
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.1+(iDia*0.03),"STRING","TRUE")
		ELSE
			
			SET_TEXT_COLOUR(0,0,255,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iDia*0.03),"STRING",sDia)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iDia*0.03),"STRING","FALSE")
		ENDIF
		
		IF dia[iDia].bStarted
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iDia*0.03),"STRING","TRUE")
		else
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iDia*0.03),"STRING","FALSE")
		endif
		
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,0,255)
		showIntOnScreen(0.5,0.1+(iDia*0.03),dia[iDia].flag,"",0.35)
	ELSe
		PRINTLN("DIALOGUE trying to play: ",sDia)
	ENDIf
	
ENDPROC

PROC PRINT_ACTION_DATA(int iData,int offset=0)
	string sAct
	enumActions thisAct = actions[iData].action
	SWITCH thisAct
		CASE ACT_NULL  sAct = "ACT_NULL" BREAK
		CASE ACT_SPAWN_COPS  sAct = "ACT_SPAWN_COPS" BREAK
		CASE ACT_INTRO_CUTSCENE  sAct = "ACT_INTRO_CUTSCENE" BREAK
		CASE ACT_LOOK_AT_PLAYER_ENTRY  sAct = "ACT_LOOK_AT_PLAYER_ENTRY" BREAK
		CASE ACT_WALK_TO_STAIRS  sAct = "ACT_WALK_TO_STAIRS" BREAK
		CASE ACT_COPS_ALERTED  sAct = "ACT_COPS_ALERTED" BREAK
		CASE ACT_SPAWN_CHOPPER  sAct = "ACT_SPAWN_CHOPPER" BREAK
		CASE ACT_COP_RETURNS_TO_FRONT_DESK  sAct = "ACT_COP_RETURNS_TO_FRONT_DESK" BREAK
		CASE ACT_PLAYER_GETS_IN_CHOPPER  sAct = "ACT_PLAYER_GETS_IN_CHOPPER" BREAK
		CASE ACT_PILOT_FLIES_OFF  sAct = "ACT_PILOT_FLIES_OFF" BREAK
	
		//Car Park stage
		CASE ACT_CHOPPER_CONTROL  sAct = "ACT_CHOPPER_CONTROL" BREAK
		CASE ACT_CAR_HAS_PISS sAct = "ACT_CAR_HAS_PISS" BREAK
		CASE ACT_BONNET_SEX sAct = "ACT_BONNET_SEX" BREAK
		CASE ACT_CAR_FIXER sAct = "ACT_CAR_FIXER" BREAK
		CASE ACT_ON_PHONE_IN_CAR sAct = "ACT_ON_PHONE_IN_CAR" BREAK
		CASE ACT_LEANING_ON_CAR sAct = "ACT_LEANING_ON_CAR" BREAK
		CASE ACT_FRANKLIN_RUNS_TO_ROOF sAct = "ACT_FRANKLIN_RUNS_TO_ROOF" BREAK
		CASE ACT_FRANKLIN_WAVES_AT_CHOPPER_CAMERA sAct = "ACT_FRANKLIN_WAVES_AT_CHOPPER_CAMERA" BREAK
		CASE ACT_FRANKLIN_RUNS_TO_BONNET_CAR sAct = "ACT_FRANKLIN_RUNS_TO_BONNET_CAR" BREAK
		CASE ACT_FRANKLIN_RUNS_TO_PISSER_CAR sAct = "ACT_FRANKLIN_RUNS_TO_PISSER_CAR" BREAK
		CASE ACT_FRANKLIN_RUNS_TO_CHAD_CAR sAct = "ACT_FRANKLIN_RUNS_TO_CHAD_CAR" BREAK
		CASE ACT_FRANKLIN_RUNS_TO_LEANING_MAN sAct = "ACT_FRANKLIN_RUNS_TO_LEANING_MAN" BREAK
		CASE ACT_FRANKLIN_RUNS_TO_PHONE_MAN sAct = "ACT_FRANKLIN_RUNS_TO_PHONE_MAN" BREAK
		CASE ACT_FRANKLIN_REACTS_TO_WRONG_CAR sAct = "ACT_FRANKLIN_REACTS_TO_WRONG_CAR" BREAK
		CASE ACT_CHAD_LOOKS_ABOUT sAct = "ACT_CHAD_LOOKS_ABOUT" BREAK
		CASE ACT_CHAD_EXITS_CAR sAct = "ACT_CHAD_EXITS_CAR" BREAK
		
		//get car in car park
		CASE ACT_PREP_STAGE  sAct = "ACT_PREP_STAGE" BREAK
		CASE ACT_START_AUDIO_CAR_2_DRIVE_BACK_TO_GARAGE  sAct = "ACT_START_AUDIO_CAR_2_DRIVE_BACK_TO_GARAGE" BREAK
		CASE ACT_STOP_AUDIO_CAR_2_STEAL_THE_CAR  sAct = "ACT_STOP_AUDIO_CAR_2_STEAL_THE_CAR" BREAK
		CASE ACT_CHAD_RUNS_OFF  sAct = "ACT_CHAD_RUNS_OFF" BREAK
		CASE ACT_CHAD_TRIES_TO_CREEP_AWAY  sAct = "ACT_CHAD_TRIES_TO_CREEP_AWAY" BREAK
		CASE ACT_REACTS_TO_PLAYER_IN_CAR  sAct = "ACT_REACTS_TO_PLAYER_IN_CAR" BREAK
		CASE ACT_LOAD_END_CUTSCENE  sAct = "ACT_LOAD_END_CUTSCENE" BREAK
		CASE ACT_CHAD_CALLS_COPS  sAct = "ACT_CHAD_CALLS_COPS" BREAK
		CASE ACT_FLY_TO_GARAGE  sAct = "ACT_FLY_TO_GARAGE" BREAK
		CASE ACT_REMOVE_CAR_PARK_ASSETS  sAct = "ACT_REMOVE_CAR_PARK_ASSETS" BREAK
		CASE ACT_SET_UP_DEVIN_LEADIN  sAct = "ACT_SET_UP_DEVIN_LEADIN" BREAK
		CASE ACT_CONTROL_CHOPPER  sAct = "ACT_CONTROL_CHOPPER" BREAK
		DEFAULT sAct = " -NOT DEFINED-" BREAK
	ENDSWITCH
	
	float shiftY = 0.0
	shiftY = offset * -0.03
	IF actions[iData].ongoing
		SET_TEXT_COLOUR(255,0,0,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",sAct)
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.1+(iData*0.03)+shiftY,"STRING","TRUE")		
	ELSE
		
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",sAct)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.1+(iData*0.03)+shiftY,"STRING","FALSE")
	ENDIF
	
	IF actions[iData].completed
		SET_TEXT_COLOUR(255,0,0,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",sAct)
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.1+(iData*0.03)+shiftY,"STRING","TRUE")		
	ELSE
		
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",sAct)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.1+(iData*0.03)+shiftY,"STRING","FALSE")
	ENDIF
		
	showIntOnScreen(0.57,0.1+(iData*0.03)+shiftY,actions[iData].flag,"",0.35)
	showIntOnScreen(0.62,0.1+(iData*0.03)+shiftY,actions[iData].intA,"",0.35)
	
	
ENDPROc



PROC PRINT_INSTRUCTION_DATA(int iData)
	string sInst
	enumInstructions thisInst = instr[iData].ins
	sINst= GET_INSTRUCTION_STRING(thisInst)
		
	IF instr[iData].bCompleted
		SET_TEXT_COLOUR(255,0,0,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03),"STRING",sInst)
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.1+(iData*0.03),"STRING","TRUE")		
	ELSE		
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03),"STRING",sInst)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.1+(iData*0.03),"STRING","FALSE")
	ENDIF
		
	showIntOnScreen(0.5,0.1+(iData*0.03),instr[iData].flag,"",0.35)
	showIntOnScreen(0.55,0.1+(iData*0.03),instr[iData].intA,"",0.35)	
ENDPROC

PROC resetSCENE(viewedScenedStruct &thisScene)
	thisScene.nextEvent = 0
	thisScene.timeLine[0].event = SCENE_DEBUG
	thisScene.dialogueLine = 0
	thisScene.nextAction = 0
	KILL_ANY_CONVERSATION()
ENDPROC

PROC debugStuff()
	if bWidget_skipChadOnWaypoint = TRUE
		bWidget_skipChadOnWaypoint = FALSE
		if not IS_PED_INJURED(ped[ped_chad].id)
			CLEAR_PED_TASKS_IMMEDIATELY(ped[ped_chad].id)	
			initPedOnFoot(ped_chad,model_chad,<< 209.1343, -153.3794, 57.6580 >>, 159.6702,pedrole_void)				
											
			//brainWander(ped_chadGirl)
			if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
				SET_ENTITY_COORDS(vehicle[vehChopper].id,<< 259.1425, -189.8652, 163.2856 >>)
				POINT_CHOPPER_CAM_AT_COORD(HUD,<< 209.1343, -153.3794, 57.6580 >>)				
			ENDIF
			iFlag=15
		ENDIF
		
		REQUEST_ANIM_DICT("misscarsteal2CHAD_HOLDUP")
		WHILE NOT HAS_ANIM_DICT_LOADED("misscarsteal2CHAD_HOLDUP")
			safewait(0)
		ENDWHILE
		
		set_event_flag(events_scene_chadGirl,CLEANUP)
		add_event(EVENTS_SCENE_CHADJACK)
		set_event_flag(EVENTS_SCENE_CHADJACK,4)
		SET_PED_IN_SCAN_LIST_AS_SCANNED(HUD,ped[ped_chad].id)
		
		SET_GARAGE_DOOR_STATE(GD_CREATE_AND_LOCK)
		
		//set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,TRUE,fOpenRatio)
	ENDIF	
	
	IF bSkipcheck
		bSkipcheck=false
		if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
			SET_ENTITY_COORDS(vehicle[vehHeist].id, <<501.9428, -1311.0804, 28.1913>>)
			SET_ENTITY_HEADING(vehicle[vehHeist].id,86)			
		ENDIF
	ENDIF
	
	if bSkipChopper
		bSkipChopper=FALSE
		add_event(EVENTS_CHOPPER_CARPARK)
	
		infraRed = TRUE
		set_event_flag(EVENTS_CARPARK_CONVERSATION,8)
		add_event(EVENTS_CONTROL_INFRARED)


		if not IS_PED_INJURED(ped[ped_franklin].id)
			initPedOnFoot(ped_franklin,model_franklin,<< -1275.8275, -223.6033, 50.5498 >>, 341.7638)
			CLEAR_PED_TASKS_IMMEDIATELY(ped[ped_franklin].id)
		ENDIF
		CLEAR_PRINTS()
	ENDIF
	
	IF bResetScene
		bResetScene = FALSE
		resetSCENE(viewScene[iDebugScene])
		
	ENDIF
	
	IF bSceneExport
		bSceneExport=FALSE
		int jj
		for jj = 0 to MAX_TIMELINES-1
			println(viewScene[iDebugScene].timeLine[jj].triggerTime)
		endfor
	ENDIF
	
	IF iDebugScene != iLastDebugScene
	
		iLastDebugScene = iDebugScene
		iDebugEvent = 0
		iLastDebugEvent = 0
		iDebugTimer = viewScene[iDebugScene].timeLine[iDebugEvent].triggerTime

		iLastDebugTimer =iDebugTimer
	ENDIF
	
	IF iDebugEvent != iLastDebugEvent
		iDebugTimer = viewScene[iDebugScene].timeLine[iDebugEvent].triggerTime
		
		iLastDebugTimer =iDebugTimer
		iLastDebugEvent = iDebugEvent
	ENDIF
	
	If iDebugTimer != iLastDebugTimer
		viewScene[iDebugScene].timeLine[iDebugEvent].triggerTime = iDebugTimer
		
		iLastDebugTimer = iDebugTimer
	ENDIF

		
		 
		 
	

	IF bShowGrab
		if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicle[vehHeist].id,vgrab1),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicle[vehHeist].id,vgrab3))
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicle[vehHeist].id,vgrab2),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicle[vehHeist].id,vgrab4))
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicle[vehHeist].id,vgrab5),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicle[vehHeist].id,vgrab7))
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicle[vehHeist].id,vgrab6),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicle[vehHeist].id,vgrab8))

		ENDIF
	ENDIF
	
	if bPrintGrab
		bPrintGrab=false
		PRINTLN("vector vgrab1 = ",vgrab1)
		PRINTLN("vector vgrab2 = ",vgrab2)
		PRINTLN("vector vgrab3 = ",vgrab3)
		PRINTLN("vector vgrab4 = ",vgrab4)		
		PRINTLN("vector vgrab5 = ",vgrab5)
		PRINTLN("vector vgrab6 = ",vgrab6)
		PRINTLN("vector vgrab7 = ",vgrab7)
		PRINTLN("vector vgrab8 = ",vgrab8)	
	ENDIF	
	int iR
	IF bFrameTimer
		PRINTLN("timer: ",GET_GAME_TIMER())
	ENDIF
	
	IF bScreenConditions
		bscreenDialogue = FALSE
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.05,"STRING","Returns")
		
		REPEAT COUNT_OF(conditions) iR
			IF conditions[iR].active
				PRINT_CONDITION_DATA(iR)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bscreenDialogue
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.05,"STRING","Ongoing")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.05,"STRING","Complete")
		
		REPEAT COUNT_OF(dia) iR
			PRINT_DIALOGUE_DATA(iR)
		ENDREPEAT
	ENDIF
	
	IF bScreenActions
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.05,"STRING","Ongoing")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.05,"STRING","Complete")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.57,0.05,"STRING","Flag")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.62,0.05,"STRING","IntA")
		
		FOR iR = 0 to 15
			IF actions[iR].active	
				PRINT_ACTION_DATA(iR)
			ENDIF
		ENDFOR
	ENDIF
	
	IF bScreenActionsB
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.05,"STRING","Ongoing")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.05,"STRING","Complete")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.57,0.05,"STRING","Flag")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.62,0.05,"STRING","IntA")
		
		FOR iR = 15 to COUNT_OF(actions)-1
			IF actions[iR].active	
				PRINT_ACTION_DATA(iR,15)
			ENDIF
		ENDFOR
	ENDIF
	
	IF bScreenInstructions
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.05,"STRING","Complete")
		
		REPEAT COUNT_OF(instr) iR
			PRINT_INSTRUCTION_DATA(iR)
		ENDREPEAT
	endif
ENDPROC
#ENDIF

proc force_ped_plays_anims(int pedID)
	if DOES_ENTITY_EXIST(ped[pedID].id) 
		if not is_ped_injured(ped[pedID].id)			
			SET_PED_RESET_FLAG(ped[pedID].id,PRF_AllowUpdateIfNoCollisionLoaded,true)
			SET_PED_RESET_FLAG(ped[pedID].id,INT_TO_ENUM(PED_RESET_FLAGS,50),true) //THIS IS A HACK FIX FOR BUG 286194
		ENDIF
	ENDIF
ENDPROC


proc set_road_block(int iNextblock,vector v1, vector v2, float fWidth)
	sroadblocks[iNextblock].v1 = v1
	sroadblocks[iNextblock].v2 = v2
	sroadblocks[iNextblock].width = fWidth
	sroadblocks[iNextblock].on = false
endproc



PROC controlCarRecs()
	int i
	int iSkiptime


	If bplayVehRecs = TRUE
		if missionProgress = STAGE_CHASE_BEGINS
		OR missionProgress = STAGE_CARPARK
			IF NOT IS_PC_VERSION()
				iPlaybackTime += floor((TIMESTEP() * fPlaybackSpeed) * 1000.0) //= GET_GAME_TIMER() - iStartPlaybackTime
			ELSE
				iPlaybackTime += floor((TIMESTEP() * fPlaybackSpeed) * 1000.0 + 0.5) //= GET_GAME_TIMER() - iStartPlaybackTime
			ENDIF
			
			REPEAT COUNT_OF(recordingData) i	
				if recordingData[i].Recording >= 1
				
					if recordingData[i].playing = FALSE and recordingData[i].ended = FALSE
						//load models in advance
						if recordingData[i].startPlaybackTime -5000 < iPlaybackTime
							if recordingData[i].isHeistCar = FALSE and recordingData[i].isChaseCar = FALSE
								REQUEST_MODEL(recordingData[i].model)
							ENDIF
						ENDIF
					
						if i!=31 and iPlaybackTime > recordingData[i].startPlaybackTime
						OR i=31 and iPlaybackTime > recordingData[i].startPlaybackTime - 15000
							if recordingData[i].isHeistCar = TRUE
								recordingData[i].vehicleID = vehicle[vehHeist].id
								vStartPos = GET_ENTITY_COORDS(recordingData[i].vehicleID) //+<<-0.01,-0.1,0.0054>>
								vStartRot = GET_ENTITY_ROTATION(recordingData[i].vehicleID)
								//cprintln(debug_trevor3,"pos: ",vStartPos," vTargetBlendPos: ",vTargetBlendPos)
							ELIF recordingData[i].isChaseCar = TRUE
								if not IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
									if not is_ped_injured(PED[ped_franklin].id)
										if IS_PED_IN_ANY_VEHICLE(PED[ped_franklin].id)
											vehicle[vehFranklin].id = GET_VEHICLE_PED_IS_IN(PED[ped_franklin].id,true)
										endif
									endif
								endif
										
								recordingData[i].vehicleID = vehicle[vehFranklin].id
							ELSE
								IF HAS_MODEL_LOADED(recordingData[i].model)
									IF NOT IS_VEHICLE_DRIVEABLE(recordingData[i].vehicleID)
										IF HAS_VEHICLE_RECORDING_BEEN_LOADED(recordingData[i].Recording,sVehicleRecordingLibrary)
											recordingData[i].vehicleID = CREATE_VEHICLE(recordingData[i].model,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(recordingData[i].Recording,0.0,sVehicleRecordingLibrary))															
											SET_VEHICLE_LIGHTS(recordingData[i].vehicleID,SET_VEHICLE_LIGHTS_ON)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(recordingData[i].vehicleID)
								FREEZE_ENTITY_POSITION(recordingData[i].vehicleID,FALSE)								
								//cprintln(debug_Trevor3,"UnFreeze car: ",i)
								IF recordingData[i].isHeistCar = TRUE OR recordingData[i].isChaseCar = TRUE
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(recordingData[i].vehicleID)
										vStartPos = GET_ENTITY_COORDS(recordingData[i].vehicleID) +<<-0.01,-0.1,0.0054>>
										vStartRot = GET_ENTITY_ROTATION(recordingData[i].vehicleID)
										//cprintln(debug_trevor3,"pos: ",vStartPos," vTargetBlendPos: ",vTargetBlendPos)
										STOP_PLAYBACK_RECORDED_VEHICLE(recordingData[i].vehicleID)
										recordingData[i-1].ended = FALSE
									ENDIF
								ENDIF
								
								IF recordingData[i].Recording = 100//franklin chase rec
									STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[vehFranklin].id,1000.0,TRUE)
									SET_VEHICLE_DOOR_SHUT(vehicle[vehFranklin].id,SC_DOOR_FRONT_LEFT,TRUE)
								ENDIF
								
								
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(recordingData[i].vehicleID)
									bool bStartedARecording
									IF recordingData[i].isHeistCar = TRUE
										IF recordingData[i].Recording = 400
											START_PLAYBACK_RECORDED_VEHICLE(recordingData[i].vehicleID,recordingData[i].Recording,sVehicleRecordingLibrary,FALSE)
											bStartedARecording=TRUE
										ELSE
											fBlendTime = 0.0
//											vStartPos = GET_ENTITY_COORDS(recordingData[i].vehicleID)
//											vStartRot = GET_ENTITY_ROTATION(recordingData[i].vehicleID)
											vTargetBlendPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(recordingData[i].Recording,1000.0,"cs2")
											vTargetBlendRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(recordingData[i].Recording,1000.0,"cs2")
											
										ENDIF
										//START_PLAYBACK_RECORDED_VEHICLE(recordingData[i].vehicleID,recordingData[i].Recording,sVehicleRecordingLibrary,FALSE)
									ELSE
										START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(recordingData[i].vehicleID,recordingData[i].Recording,sVehicleRecordingLibrary,ENUM_TO_INT(CONTINUE_RECORDING_IF_CAR_DESTROYED))
										bStartedARecording=TRUE
									ENDIF
									
									IF bStartedARecording
										FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(recordingData[i].vehicleID,false)																
										iSkiptime = iPlaybackTime - recordingData[i].startPlaybackTime								
										SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(recordingData[i].vehicleID,TO_FLOAT(iSkiptime))									
									ENDIF
									recordingData[i].playing = true	
								ENDIF
							ENDIF
						ENDIF																					
					ENDIF	
					IF recordingData[i].playing = true and recordingData[i].ended = FALSE
						IF IS_VEHICLE_DRIVEABLE(recordingData[i].vehicleID)
						
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(recordingData[i].vehicleID)
								if recordingData[i].isHeistCar != TRUE and recordingData[i].isChaseCar != TRUE 									
									FREEZE_ENTITY_POSITION(recordingData[i].vehicleID,TRUE) //added due to bug 894717
									//recordingData[i].ended = TRUE
									cprintln(debug_trevor3,"Recording: ",i," is frozen E")
								else								
									IF recordingData[i].isHeistCar
										IF recordingData[i].Recording > 400
											IF iPlaybackTime - recordingData[i].startPlaybackTime < 2000
												vector vNew
												vector vRotNew
												fBlendTime += timestep()
												float fAdvanceTime
												fAdvanceTime = 1 - (cos(fBlendTime * 90))
												IF fBlendTime < 1.0
													vNew = vStartPos + ((vTargetBlendPos - vStartPos) * (fAdvanceTime))
													vRotNew = vStartRot + ((vTargetBlendRot - vStartRot) * (fAdvanceTime)) 
													FREEZE_ENTITY_POSITION(recordingData[i].vehicleID,TRUE)
													SET_ENTITY_COORDS(recordingData[i].vehicleID,vNew)
													SET_ENTITY_ROTATION(recordingData[i].vehicleID,vRotNew)
													cprintln(debug_trevor3,"Recording: ",i," is frozen C")
												ELSE
													vNew = vStartPos + ((vTargetBlendPos - vStartPos) * (fAdvanceTime))
													vRotNew = vStartRot + ((vTargetBlendRot - vStartRot) * (fAdvanceTime)) 
													SET_ENTITY_COORDS_NO_OFFSET(recordingData[i].vehicleID,vNew)
													SET_ENTITY_ROTATION(recordingData[i].vehicleID,vRotNew)
													FREEZE_ENTITY_POSITION(recordingData[i].vehicleID,FALSE)
													START_PLAYBACK_RECORDED_VEHICLE(recordingData[i].vehicleID,recordingData[i].Recording,sVehicleRecordingLibrary,FALSE)
																												
													iSkiptime = iPlaybackTime - recordingData[i].startPlaybackTime								
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(recordingData[i].vehicleID,TO_FLOAT(iSkiptime))
													FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(recordingData[i].vehicleID,false)	
													cprintln(debug_trevor3,"Recording: ",i," is frozen D")
												ENDIF
											ENDIF
										ELSE
											cprintln(debug_trevor3,"Recording: ",i," is frozen B")
											FREEZE_ENTITY_POSITION(recordingData[i].vehicleID,TRUE)
										ENDIF
									ENDIF
								endif
							ENDIF
							
							IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(recordingData[i].Recording,sVehicleRecordingLibrary)
								cprintln(debug_trevor3,"Recording: ",recordingData[i].Recording," is not loaded.")
							ENDIF
							
							if HAS_VEHICLE_RECORDING_BEEN_LOADED(recordingData[i].Recording,sVehicleRecordingLibrary)
								if iPlaybackTime < recordingData[i].startPlaybackTime + (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(recordingData[i].Recording,sVehicleRecordingLibrary))
		
								ELSE			
									if recordingData[i].isHeistCar != TRUE and recordingData[i].isChaseCar != TRUE 
										
										FREEZE_ENTITY_POSITION(recordingData[i].vehicleID,TRUE)
										cprintln(debug_trevor3,"Recording: ",i," is frozen A")
									ENDIF
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(recordingData[i].vehicleID)
										IF GET_CURRENT_PLAYBACK_FOR_VEHICLE(recordingData[i].vehicleID) = GET_VEHICLE_RECORDING_ID(recordingData[i].Recording, sVehicleRecordingLibrary)
											STOP_PLAYBACK_RECORDED_VEHICLE(recordingData[i].vehicleID)
											cprintln(debug_trevor3,"Recording: ",i," is stopped")
										ENDIF
									ENDIF
									
									if recordingData[i].isHeistCar != TRUE and recordingData[i].isChaseCar != TRUE 															
										SET_VEHICLE_AS_NO_LONGER_NEEDED(recordingData[i].vehicleID)									
										SET_MODEL_AS_NO_LONGER_NEEDED(recordingData[i].model)
									ENDIF
									
									if recordingData[i].isHeistCar = TRUE
										vStartPos = GET_ENTITY_COORDS(recordingData[i].vehicleID) +<<-0.01,-0.1,0.0054>>
										vStartRot = GET_ENTITY_ROTATION(recordingData[i].vehicleID)
									ENDIF
									
									recordingData[i].ended = TRUE 
								ENDIF
							endif
						ENDIF
					ENDIF
					/*
					IF recordingData[i].Recording = 400
						IF recordingData[i].playing = TRUE
							showIntOnScreen(0.1,0.1,400,"playing")
						ENDIF
						
						IF recordingData[i].ended = TRUE
							showIntOnScreen(0.2,0.1,400,"ended")
						ENDIF
					ENDIF
					
					IF recordingData[i].Recording = 401
						IF recordingData[i].playing = TRUE
							showIntOnScreen(0.1,0.14,401,"playing")
						ENDIF
						
						IF recordingData[i].ended = TRUE
							showIntOnScreen(0.2,0.14,401,"ended")
						ENDIF
						
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehHeist].id)
							showIntOnScreen(0.3,0.14,401,"rec playing")
						ENDIF
					ENDIF*/
				ENDIF
				
				
			ENDREPEAT
			
			
		ENDIF	
	ENDIF
ENDPROC



// ==============================================================================
//		Termination
// ==============================================================================

// ------------------------------------------------------------------------------
//		Mission Cleanup
// ------------------------------------------------------------------------------

int iChaseCV
int iNotSeenCount
int iFindByTime
int iLastSeenLocationLine
float fObserveTime
float fFranklineTime,fChadTime
enum chaseDialogueStateEnum
	DIA_NONE,
	DIA_QUERIED,
	DIA_SPOTTED_WAIT,
	DIA_SPOTTED,
	DIA_NOTSEEN,
	DIA_SO_FIND_HIM,
	DIA_FIND_OR_FAIL,
	DIA_FOUND_CHAD_LOCATION,
	DIA_ENDING,
	DIA_FAIL_DIALOGUE,
	DIA_FAIL
endenum

chaseDialogueStateEnum chaseDialogueEnum = DIA_NONE




FUNC BOOL HAS_CHAD_BEEN_OBSERVED(bool bDoInstantCheck = FALSE)
	float fScreenCheckSize = 0.15 + (((45 - HUD.fFOV)/30.0) * 0.35)
	IF fScreenCheckSize < 0.1 fScreenCheckSize = 0.1 ENDIF
	IF fScreenCheckSize > 0.5 fScreenCheckSize = 0.5 ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
		IF is_point_in_screeen_area(GET_ENTITY_COORDS(vehicle[vehHeist].id),0.5-fScreenCheckSize,0.5-fScreenCheckSize,0.5+fScreenCheckSize,0.5+fScreenCheckSize)
			IF bDoInstantCheck
				RETURN TRUE
			ELSE
				fObserveTime+=TIMESTEP()			
				IF fObserveTime > 0.3
					fObserveTime = 0.0
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_LINE_FOR_CHAD_LOCATION(float iFranklinTime)
	IF iFranklinTime < 8000
		return 1
	ELIF iFranklinTime < 15800
		return 2
	ELIF iFranklinTime < 23500
		return 3
	ELIF iFranklinTime < 31500
		return 4
	ELIF iFranklinTime < 37600
		return 5
	ELIF iFranklinTime < 44500
		return 6
	ELIF iFranklinTime < 50000
		return 7
	ELIF iFranklinTime < 53000
		return 8
	ELIF iFranklinTime < 56000
		return 9
	ELIF iFranklinTime < 68500
		return 10
	ELIF iFranklinTime < 83500
		return 11
	ELIF iFranklinTime < 85300
		return 12
	ELIF iFranklinTime < 101500
		return 13
	ENDIF
	return 13	
ENDFUNC

#if IS_DEBUG_BUILD
FUNC STRING GET_CASE_DIALOGUE_FLAG()
	switch chaseDialogueEnum
		CASE DIA_NONE RETURN "DIA_NONE" BREAK
		CASE DIA_QUERIED RETURN "DIA_QUERIED" BREAK
		CASE DIA_SPOTTED_WAIT RETURN "DIA_SPOTTED_WAIT" BREAK
		CASE DIA_SPOTTED RETURN "DIA_SPOTTED" BREAK
		CASE DIA_NOTSEEN RETURN "DIA_NOTSEEN" BREAK
		CASE DIA_SO_FIND_HIM RETURN "DIA_SO_FIND_HIM" BREAK
		CASE DIA_FIND_OR_FAIL RETURN "DIA_FIND_OR_FAIL" BREAK
		CASE DIA_FOUND_CHAD_LOCATION RETURN "DIA_FOUND_CHAD_LOCATION" BREAK
		CASE DIA_ENDING RETURN "DIA_ENDING" BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC
#endif



FUNC BOOL CREATE_TREVOR(vector vCoord, float fHeading=0.0, vehicle_index inVehicle = null, VEHICLE_SEAT vs=VS_DRIVER)
	IF inVehicle = NULL		
		IF NOT CREATE_PLAYER_PED_ON_FOOT(ped[ped_trevor].id,CHAR_TREVOR,vCoord,fHeading)
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_VEHICLE_DRIVEABLE(inVehicle)
			IF IS_VEHICLE_SEAT_FREE(inVehicle,vs)				
				IF NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(ped[ped_trevor].id,CHAR_TREVOR,inVehicle,VS)
					RETURN FALSE
				ENDIF
			ELSE
				IF NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(ped[ped_trevor].id,CHAR_TREVOR,inVehicle,VS_FRONT_RIGHT)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(ped[ped_trevor].id)
		SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_trevor].id)
		SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_trevor].id,FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped[ped_trevor].id,CA_LEAVE_VEHICLES,FALSE)
		
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_trevor].id,TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_FRANKLIN(vector vCoord, float fHeading=0.0, vehicle_index inVehicle = null)
	IF inVehicle = NULL
		 IF CREATE_PLAYER_PED_ON_FOOT(ped[ped_franklin].id,CHAR_FRANKLIN,vCoord,fHeading)
		 	SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_franklin].id,FALSE)
			RETURN TRUE
		 ENDIF
	ELSE
		IF IS_VEHICLE_DRIVEABLE(inVehicle)
			IF IS_VEHICLE_SEAT_FREE(inVehicle,VS_DRIVER)
				IF CREATE_PLAYER_PED_INSIDE_VEHICLE(ped[ped_franklin].id,CHAR_FRANKLIN,inVehicle)
					SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_franklin].id,FALSE)
					RETURN TRUE
				ENDIF						
			ELSE
				IF CREATE_PLAYER_PED_INSIDE_VEHICLE(ped[ped_franklin].id,CHAR_FRANKLIN,inVehicle,VS_FRONT_RIGHT)
					SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_franklin].id,FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_franklin].id)
	RETURN FALSE
ENDFUNC


// ============================================ Scenes observed from chopper ============================
FUNC BOOL IS_SCENE_BEING_OBSERVED(viewedScenedStruct thisScene)
	//if (is_point_in_screeen_area(<< -29.8705, -86.0999, 57.4479 >>,0.2,0.2,0.8,0.8) and HUD.fFOV < 30) or IS_PED_BEING_SCANNED(HUD,ped[ped_courier].id) or IS_PED_BEING_SCANNED(HUD,ped[ped_mugger].id)
	IF IS_POINT_VISIBLE(thisScene.pos,10,300) and HUD.fFOV < 12
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_EVENT_DATA(viewedScenedStruct &thisScene, sceneEventEnum eventType,int iTime)
	int iThis
	for iThis = 1 to MAX_TIMELINES-1
		IF thisScene.timeLine[iThis].event = SCENE_INIT
	 		thisScene.timeLine[iThis].event = eventType
			thisScene.timeLine[iThis].triggerTime = iTime
			iThis = MAX_TIMELINES
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_PLAYER_LISTENING_TO_DIALOGUE(viewedScenedStruct &thisScene)		
	IF listenDialogue!= -1
		IF thisScene.listenDia != -1 
			IF thisScene.listenDia = listenDialogue				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

proc PAUSE_scene_timer(viewedScenedStruct &thisScene)
	thisScene.triggerTime += floor(TIMESTEP() * 1000)
endproc

PROC SET_LISTENER_DIALOGUE_TO_PAUSE(bool setPause)
	IF setPause
		bPauseListenerDialogue = TRUE
	ELSE
		bPauseListenerDialogue = FALSE
	ENDIF
ENDPROC

FUNC bool triggerSceneEvent(viewedScenedStruct &thisScene,enumSceneName sceneName, bool justSkipIt = FALSE)
	

	
	IF bPauseListenerDialogue justSkipIt = TRUE ENDIF
	IF thisScene.nextEvent != 0 and thisScene.timeLine[thisScene.nextEvent].event != SCENE_INIT
	OR thisScene.nextEvent = 0
		
		SWITCH sceneName
			CASE SCENENAME_POSTAL
				SWITCH thisScene.timeLine[thisScene.nextEvent].event
					CASE SCENE_INIT				
						//set up events
						thisScene.timeLine[0].event = SCENE_INIT
						thisScene.timeLine[1].event = SCENE_TRIGGER
						thisScene.timeLine[2].event = SCENE_LISTEN_LINE
						thisScene.timeLine[2].triggerTime = 2000
						thisScene.timeLine[3].event = SCENE_LISTEN_LINE
						thisScene.timeLine[3].triggerTime = 5000
						thisScene.timeLine[4].event = SCENE_ACTION
						thisScene.timeLine[4].triggerTime = 6000
						thisScene.timeLine[5].event = SCENE_LISTEN_LINE
						thisScene.timeLine[5].triggerTime = 8000						
						thisScene.timeLine[6].event = SCENE_LISTEN_LINE
						thisScene.timeLine[6].triggerTime = 13000
						thisScene.timeLine[7].event = SCENE_LISTEN_LINE
						thisScene.timeLine[7].triggerTime = 18000
						thisScene.timeLine[8].event = SCENE_LISTEN_LINE
						thisScene.timeLine[8].triggerTime = 25057
						thisScene.timeLine[9].event = SCENE_LISTEN_LINE
						thisScene.timeLine[9].triggerTime = 30000
						thisScene.timeLine[10].event = SCENE_LISTEN_LINE
						thisScene.timeLine[10].triggerTime = 33300
						thisScene.timeLine[11].event = SCENE_ACTION
						thisScene.timeLine[11].triggerTime = 34000
						thisScene.timeLine[12].event = SCENE_ACTION
						thisScene.timeLine[12].triggerTime = 40000
						thisScene.timeLine[13].event = SCENE_ACTION
						thisScene.timeLine[13].triggerTime = 40000
						thisScene.timeLine[14].event = SCENE_LISTEN_LINE
						thisScene.timeLine[14].triggerTime = 40000
					
						vehicle[vehDeliveryVan].id = CREATE_VEHICLE(BOXVILLE2,<<-25.625967,-85.980637,57.145386>>, -45.000004)
						
					//	SET_ENTITY_COLLISION(vehicle[vehDeliveryVan].id,FALSE)
					//	FREEZE_ENTITY_POSITION(vehicle[vehDeliveryVan].id,TRUE)
						ped[ped_courier].id = CREATE_PED(PEDTYPE_MISSION,S_M_M_janitor,<<-29.4751, -90.5675, 56.2545>>,140)
						
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_courier].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_courier].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_courier].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_courier].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_courier].id, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
						SET_PED_COMPONENT_VARIATION(ped[ped_courier].id, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
						SET_PED_PROP_INDEX(ped[ped_courier].id, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
						
						remove_helihud_marker(HUD,destMarker[0])

						ADD_PED_TO_SCAN_LIST(HUD,ped[ped_courier].id,true,HUD_UNKNOWN,FALSE,true,true,true)									
						ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_courier].id,"CH_NAME1",8)	
						SET_VEHICLE_DOOR_OPEN(vehicle[vehDeliveryVan].id,SC_DOOR_REAR_LEFT)
						SET_VEHICLE_DOOR_OPEN(vehicle[vehDeliveryVan].id,SC_DOOR_REAR_RIGHT)
						//SET_PED_RESET_FLAG(ped[ped_courier].id,PRF_AllowUpdateIfNoCollisionLoaded,true)		
						//SET_PED_RESET_FLAG(ped[ped_courier].id,INT_TO_ENUM(PED_RESET_FLAGS,50),true)
						
														
						thisScene.pos = << -28.850, -87.939, 56.622 >>
						thisScene.rot = << -0.000, -0.000, 45.000 >>
						thisScene.listenDia = ADD_CHOPPER_LISTENING_LOCATION(0,thisScene.pos)					
						thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(<< -28.850, -87.939, 56.622 >>, << -0.000, -0.000, 45.000 >>)
						SET_SYNCHRONIZED_SCENE_LOOPED(thisScene.syncSceneID[0],TRUE)																							
						IF NOT IS_PED_INJURED(ped[ped_courier].id)						
							TASK_SYNCHRONIZED_SCENE(ped[ped_courier].id, thisScene.syncSceneID[0], "misscarsteal2MUGGING", "mugging_outro_mugger", INSTANT_BLEND_IN, -0.5 )
						ENDIF
						thisScene.nextEvent++
					BREAK
					CASE SCENE_TRIGGER
						
						IF IS_SCENE_BEING_OBSERVED(thisScene)
							
							//cprintln(debug_Trevor3,"OBSERVED")
							thisScene.triggerTime = GET_GAME_TIMER()
							thisScene.nextEvent++
						ENDIF
					BREAK
					CASE SCENE_LISTEN_LINE
						IF justSkipIt
							thisScene.dialogueLine++
							thisScene.nextEvent++
						ELSe
							IF IS_PLAYER_LISTENING_TO_DIALOGUE(thisScene)
								IF thisScene.dialogueLine = 8
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"cs2_pub",5,ped[ped_courier].id,"cs2_courier")
										thisScene.dialogueLine++
										thisScene.nextEvent++
									ENDIF
								ELSE
									//cprintln(debug_Trevor3,"LISTEN")
									TEXT_LABEL_23 txt
									txt = "CAR_2_IG_4_"
									txt += thisScene.dialogueLine+1
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"CAR_2_IG_4",txt,5,ped[ped_courier].id,"cs2_courier")
										thisScene.dialogueLine++
										thisScene.nextEvent++
									ENDIF
								ENDIF
							ELSE
								thisScene.dialogueLine++
								thisScene.nextEvent++
							ENDIf		
						ENDIF
					BREAK
					CASE SCENE_ACTION
						IF justSkipIt
							thisScene.dialogueLine++
							thisScene.nextAction++
						ELSe
							SWITCH thisScene.nextAction
								CASE 0 //Throw boxes from van
									thisScene.syncSceneID[1] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)
									
									
									if not IS_PED_INJURED(ped[ped_courier].id)
										TASK_SYNCHRONIZED_SCENE (ped[ped_courier].id, thisScene.syncSceneID[1], "misscarsteal2MUGGING", "mugging_action_mugger", 0.5, -0.5 )									
									ENDIF
									
									SET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[1],0.0)
									
									prop[1] = CREATE_OBJECT(PROP_CS_PACKAGE_01,<< -28.850, -87.939, 56.622 >>)
									prop[2] = CREATE_OBJECT(PROP_CS_PACKAGE_01,<< -28.850, -87.939, 57.622 >>)	
																				
									SET_ENTITY_COLLISION(prop[1],FALSE)
									SET_ENTITY_COLLISION(prop[2],FALSE)
																																		
									PLAY_SYNCHRONIZED_ENTITY_ANIM( prop[1], thisScene.syncSceneID[1], "mugging_action_box1","misscarsteal2MUGGING" , INSTANT_BLEND_IN )
									PLAY_SYNCHRONIZED_ENTITY_ANIM( prop[2], thisScene.syncSceneID[1], "mugging_action_box2","misscarsteal2MUGGING" , INSTANT_BLEND_IN )
									
									
									//turn off nodes on main road
									SET_ROADS_IN_ANGLED_AREA(<<-22.509262,-110.788269,54.817471>>, <<-32.304104,-135.421097,64.824463>>, 29.375000,FALSE,FALSE)
									SET_ROADS_IN_ANGLED_AREA(<<19.846016,-142.186356,53.802525>>, <<65.391869,-160.891373,62.862186>>, 29.375000,FALSE,FALSE)
									
									thisScene.nextEvent++	
									thisScene.nextAction++
									//cprintln(debug_Trevor3,"AAA")
								BREAK
								CASE 1 //Goes to get a beer
									PAUSE_scene_timer(thisScene)
									//cprintln(debug_Trevor3,"BBB")
									IF GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[1]) > 0.99
									OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[1])
										//cprintln(debug_Trevor3,"CCC")
										thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(<< -28.850, -87.939, 56.622 >>, << -0.000, -0.000, 45.000 >>)
										SET_SYNCHRONIZED_SCENE_LOOPED(thisScene.syncSceneID[0],TRUE)																							
										IF NOT IS_PED_INJURED(ped[ped_courier].id)						
											TASK_SYNCHRONIZED_SCENE(ped[ped_courier].id, thisScene.syncSceneID[0], "misscarsteal2MUGGING", "mugging_outro_mugger", INSTANT_BLEND_IN, -0.5 )
										ENDIF
										thisScene.nextEvent++	
										thisScene.nextAction++
									ENDIF
										
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_courier].id)
									OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_courier].id)
										IF NOT IS_PED_INJURED(ped[ped_courier].id)		
											seq()
											TASK_ACHIEVE_HEADING(null,139)
											TASK_GO_STRAIGHT_TO_COORD(null,<<-47.9225, -135.5877, 56.6118>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
											TASK_WANDER_STANDARD(null)
											endseq(ped[ped_courier].id)
											thisScene.nextEvent++	
											thisScene.nextAction++
										ENDIF
									endif
								BREAK
								CASE 2
									PAUSE_scene_timer(thisScene)
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_courier].id)
									OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_courier].id)
										IF NOT IS_PED_INJURED(ped[ped_courier].id)		
											seq()
											TASK_ACHIEVE_HEADING(null,139)
											TASK_GO_STRAIGHT_TO_COORD(null,<<-47.9225, -135.5877, 56.6118>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
											TASK_WANDER_STANDARD(null)
											endseq(ped[ped_courier].id)
											thisScene.nextEvent++	
											thisScene.nextAction++
										ENDIF
									endif																
								BREAK
							ENDSWITCH
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SCENENAME_ROOF
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[0])
					IF GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0]) >= 0.436
						thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)
						IF NOT IS_PED_INJURED(ped[ped_husband].id)
						AND NOT IS_PED_INJURED(ped[ped_pervert].id)
						AND NOT IS_PED_INJURED(ped[ped_wife].id) 
							TASK_SYNCHRONIZED_SCENE (ped[ped_husband].id, thisScene.syncSceneID[0], "misscarsteal2PERVERT", "pervert_husband", 0.4, -0.4, SYNCED_SCENE_NONE, RBF_NONE, 0.4 )								
							TASK_SYNCHRONIZED_SCENE (ped[ped_pervert].id, thisScene.syncSceneID[0], "misscarsteal2PERVERT", "pervert_perv", 0.4, -0.4, SYNCED_SCENE_NONE, RBF_NONE, 0.4 )
							TASK_SYNCHRONIZED_SCENE (ped[ped_wife].id, thisScene.syncSceneID[0], "misscarsteal2PERVERT", "pervert_wife", 0.4, -0.4, SYNCED_SCENE_NONE, RBF_NONE, 0.4 )

							SET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0],0.11)
						ENDIF
					ENDIF
				ENDIF
			
				SWITCH thisScene.timeLine[thisScene.nextEvent].event
					CASE SCENE_INIT				
						//set up events
						thisScene.timeLine[0].event = SCENE_INIT
						thisScene.timeLine[1].event = SCENE_TRIGGER
						thisScene.timeLine[2].event = SCENE_ACTION
						thisScene.timeLine[2].triggerTime = 0
						thisScene.timeLine[3].event = SCENE_ACTION
						thisScene.timeLine[3].triggerTime = 0
					
						ped[ped_husband].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_BEACH_02,<< -9.518, -31.102, 68.097 >>,140)
						ped[ped_pervert].id = CREATE_PED(PEDTYPE_MISSION,S_M_M_janitor,<< -9.518, -31.102, 68.097 >>,140)
						ped[ped_wife].id = CREATE_PED(PEDTYPE_MISSION,s_f_y_hooker_01,<< -9.518, -31.102, 68.097 >>,140)
						
						//husband
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_husband].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_husband].id, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_husband].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_husband].id, INT_TO_ENUM(PED_COMPONENT,4), 1, 3, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_husband].id, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
						SET_PED_PROP_INDEX(ped[ped_husband].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
						
						//wife
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_wife].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_wife].id, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_wife].id, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(ped[ped_wife].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_wife].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_wife].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
						
						//pervert
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_pervert].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_pervert].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_pervert].id, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_pervert].id, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_pervert].id, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
						SET_PED_COMPONENT_VARIATION(ped[ped_pervert].id, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
						
						remove_helihud_marker(HUD,destMarker[1])
						remove_helihud_marker(HUD,destMarker[2])
						remove_helihud_marker(HUD,destMarker[3])
												
						ADD_PED_TO_SCAN_LIST(HUD,ped[ped_wife].id,true,HUD_UNKNOWN,FALSE,true,true,true)	
						ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_wife].id,"CH_NAME5",4)	
						ADD_PED_TO_SCAN_LIST(HUD,ped[ped_pervert].id,true,HUD_UNKNOWN,FALSE,true,true,true)									
						ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_pervert].id,"CH_GREG",6)	
						ADD_PED_TO_SCAN_LIST(HUD,ped[ped_husband].id,true,HUD_UNKNOWN,FALSE,true,true,true)			
						ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_husband].id,"CH_NAME3",4)	//loitering with intent									
						
						thisScene.pos = << -9.518, -31.102, 68.097 >>
						thisScene.rot =  << -0.000, 0.000, 69.250 >>
						thisScene.listenDia = ADD_CHOPPER_LISTENING_LOCATION(1,thisScene.pos)					
						thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)
						
						TASK_SYNCHRONIZED_SCENE (ped[ped_husband].id, thisScene.syncSceneID[0], "misscarsteal2PERVERT", "pervert_husband", 0.4, -0.4, SYNCED_SCENE_NONE, RBF_NONE)								
						TASK_SYNCHRONIZED_SCENE (ped[ped_pervert].id, thisScene.syncSceneID[0], "misscarsteal2PERVERT", "pervert_perv", 0.4, -0.4, SYNCED_SCENE_NONE, RBF_NONE)
						TASK_SYNCHRONIZED_SCENE (ped[ped_wife].id, thisScene.syncSceneID[0], "misscarsteal2PERVERT", "pervert_wife", 0.4, -0.4, SYNCED_SCENE_NONE, RBF_NONE)

						thisScene.nextEvent++
					BREAK
					CASE SCENE_TRIGGER
						
						IF IS_SCENE_BEING_OBSERVED(thisScene)
							thisScene.triggerTime = GET_GAME_TIMER()
							thisScene.nextEvent++
						ENDIF
					BREAK
					CASE SCENE_LISTEN_LINE
				
					BREAK
					CASE SCENE_ACTION
					
						SWITCH thisScene.nextAction
							CASE 0 //play normal dialogue		
								
								IF thisScene.dialogueLine <  20
							
									IF IS_PLAYER_LISTENING_TO_DIALOGUE(thisScene)
										TEXT_LABEL_23 txt
										txt = "IG_5_a_"
										txt += thisScene.dialogueLine+1
									
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"IG_5_a",txt,4,ped[ped_husband].id,"cs2_roof_husband",5,ped[ped_pervert].id,"cs2_roof_pervert",6,ped[ped_wife].id,"cs2_roof_wife")
											thisScene.dialogueLine++											
										ENDIF
									ENDIF
								ELSE //play a random line
									thisScene.dialogueLine = GET_RANDOM_INT_IN_RANGE(0,3)
									thisScene.nextEvent++
									thisScene.nextAction++
								ENDIF
							BREAK	
							
							CASE 1 //play random dialogue
								IF IS_PLAYER_LISTENING_TO_DIALOGUE(thisScene)
									SWITCH thisScene.dialogueLine
										CASE 0
											IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"IG_5_b",4,ped[ped_husband].id,"cs2_roof_husband")
												thisScene.dialogueLine = GET_RANDOM_INT_IN_RANGE(0,3)
												thisScene.nextAction++
												thisScene.timeLine[0].triggerTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,5000)
											ENDIF
										BREAK
										CASE 1
											IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"IG_5_c",5,ped[ped_pervert].id,"cs2_roof_pervert")
												thisScene.nextAction++
												thisScene.dialogueLine = GET_RANDOM_INT_IN_RANGE(0,3)
												thisScene.timeLine[0].triggerTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,5000)
											ENDIF
										BREAK
										CASE 2
											IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"IG_5_d",6,ped[ped_wife].id,"cs2_roof_wife")
												thisScene.nextAction++
												thisScene.dialogueLine = GET_RANDOM_INT_IN_RANGE(0,3)
												thisScene.timeLine[0].triggerTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500,5000)
											ENDIF
										BREAK
									ENDSWITCH
								ENDIF								
							BREAK
							CASE 2
								IF GET_GAME_TIMER() > thisScene.timeLine[0].triggerTime
									thisScene.nextAction = 1
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
			CASE SCENENAME_PIMP
				
				SWITCH thisScene.timeLine[thisScene.nextEvent].event
					CASE SCENE_DEBUG
					
						IF DOES_ENTITY_EXIST(ped[ped_pimp].id) DELETE_PED(ped[ped_pimp].id) ENDIF //debug 
						IF DOES_ENTITY_EXIST(ped[ped_punter].id) DELETE_PED(ped[ped_punter].id) ENDIF //debug 
						IF DOES_ENTITY_EXIST(ped[ped_hooker].id) DELETE_PED(ped[ped_hooker].id) ENDIF //debug 
						
						ped[ped_pimp].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_OG_BOSS_01,<<180.0352, -153.7189, 55.2982>>,73.5737)
						ped[ped_punter].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_BevHills_02,<<188.5162, -157.9683, 55.3856>>,203.2963)
						ped[ped_hooker].id = CREATE_PED(PEDTYPE_MISSION,S_F_Y_HOOKER_01,<<189.1093, -159.2958, 55.3431>>,359.9998)
						
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_pimp].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_pimp].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_pimp].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_pimp].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_pimp].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)	
						
						#IF IS_JAPANESE_BUILD
							TASK_STAND_STILL(ped[ped_pimp].id, -1)
							TASK_STAND_STILL(ped[ped_punter].id, -1)
							TASK_STAND_STILL(ped[ped_hooker].id, -1)
						#ENDIF
						
						
						#IF NOT IS_JAPANESE_BUILD
							thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)							
													
							TASK_SYNCHRONIZED_SCENE (ped[ped_pimp].id,thisScene.syncSceneID[0], "misscarsteal2PIMPSEX", "pimpsex_pimp", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )								
							TASK_SYNCHRONIZED_SCENE (ped[ped_punter].id,thisScene.syncSceneID[0], "misscarsteal2PIMPSEX", "pimpsex_punter", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							TASK_SYNCHRONIZED_SCENE (ped[ped_hooker].id,thisScene.syncSceneID[0], "misscarsteal2PIMPSEX", "pimpsex_hooker", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
						#ENDIF 

						thisScene.nextEvent++
					BREAK
					CASE SCENE_INIT		
						
						thisScene.timeLine[0].event = SCENE_INIT
						thisScene.timeLine[1].event = SCENE_TRIGGER


						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,100)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,6800)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,10000)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,12126)						
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,15824)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,18920)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,25570)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,28014)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,29714)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,31041)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,33706)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,35577)						
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,40437)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,45579)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,46000)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,49139)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,51600)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,54318)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,58432)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,61640)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,64000)
					//	SET_EVENT_DATA(thisScene,SCENE_LOOPED_DIALOGUE,65000)

					
						IF DOES_ENTITY_EXIST(ped[ped_pimp].id) DELETE_PED(ped[ped_pimp].id) ENDIF //debug 
						IF DOES_ENTITY_EXIST(ped[ped_punter].id) DELETE_PED(ped[ped_pimp].id) ENDIF //debug 
						IF DOES_ENTITY_EXIST(ped[ped_hooker].id) DELETE_PED(ped[ped_pimp].id) ENDIF //debug 
						
						ped[ped_pimp].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_OG_BOSS_01,<<180.0352, -153.7189, 55.2982>>,73.5737)
						ped[ped_punter].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_BEACH_02,<<188.5162, -157.9683, 55.3856>>,203.2963)
						ped[ped_hooker].id = CREATE_PED(PEDTYPE_MISSION,s_f_y_hooker_02,<<189.1093, -159.2958, 55.3431>>,359.9998)
						
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_pimp].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_pimp].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_pimp].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_pimp].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_pimp].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
						SET_PED_PROP_INDEX(ped[ped_pimp].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)												
						
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_punter].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_punter].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_punter].id, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_punter].id, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_punter].id, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
						SET_PED_PROP_INDEX(ped[ped_punter].id, INT_TO_ENUM(PED_PROP_POSITION,1), 1, 1)
						
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_hooker].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_hooker].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_hooker].id, INT_TO_ENUM(PED_COMPONENT,2), 1, 1, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(ped[ped_hooker].id, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_hooker].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_hooker].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
						
						thisScene.pos = << 189.080, -159.296, 55.330 >>
						thisScene.rot = << -0.000, 0.000, -15.390 >>
						thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)
						thisScene.listenDia = ADD_CHOPPER_LISTENING_LOCATION(2,thisScene.pos)	
						
						
						#IF IS_JAPANESE_BUILD
							TASK_STAND_STILL(ped[ped_pimp].id, -1)
							TASK_STAND_STILL(ped[ped_punter].id, -1)
							TASK_STAND_STILL(ped[ped_hooker].id, -1)
						#ENDIF
						
						#IF NOT IS_JAPANESE_BUILD
							TASK_SYNCHRONIZED_SCENE (ped[ped_pimp].id,thisScene.syncSceneID[0], "misscarsteal2PIMPSEX", "pimpsex_pimp", INSTANT_BLEND_IN, -1 )								
							TASK_SYNCHRONIZED_SCENE (ped[ped_punter].id,thisScene.syncSceneID[0], "misscarsteal2PIMPSEX", "pimpsex_punter", INSTANT_BLEND_IN, -1 )
							TASK_SYNCHRONIZED_SCENE (ped[ped_hooker].id,thisScene.syncSceneID[0], "misscarsteal2PIMPSEX", "pimpsex_hooker", INSTANT_BLEND_IN, -1 )
						#ENDIF
						
						add_event(events_pimp_achievement)

						thisScene.nextEvent++
					BREAK
					CASE SCENE_TRIGGER
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[0])
							SET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0],0.2)
						ENDIF
						IF IS_SCENE_BEING_OBSERVED(thisScene)
							thisScene.triggerTime = GET_GAME_TIMER()
							thisScene.nextEvent++
						ENDIF
					BREAK
					CASE SCENE_LISTEN_LINE
				
						IF justSkipIt
							thisScene.dialogueLine++
							thisScene.nextEvent++
						ELSe
						
							IF IS_PLAYER_LISTENING_TO_DIALOGUE(thisScene)
					
								#IF NOT IS_JAPANESE_BUILD	
								TEXT_LABEL_23 txt
								txt = "CAR_2_IG_6_"
								IF thisScene.dialogueLine <= 2
									txt += thisScene.dialogueLine+1
								ELIF thisScene.dialogueLine <= 10
									txt += thisScene.dialogueLine+2									
								ELSE
									txt += thisScene.dialogueLine+4
								ENDIF
								//IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() != thisScene.dialogueLine+1
								//	IF currentConvType = CONVTYPE_CHOPPER_CAM
									//	KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)
								//	ENDIF
								//ENDIF
								
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"CAR_2_IG_6",txt,4,ped[ped_punter].id,"cs2_punter",5,ped[ped_pimp].id,"cs2_pimp",6,ped[ped_hooker].id,"cs2_whore")
								
									thisScene.dialogueLine++
									thisScene.nextEvent++
								ENDIF
								#endif
							ELSE
								thisScene.dialogueLine++
								thisScene.nextEvent++
							ENDIf		
						ENDIF
					BREAK
					CASE SCENE_LOOPED_DIALOGUE
						IF thisScene.dialogueLine >= 0 //using this var as a counter for the random lines
							thisScene.dialogueLine = -1
						ENDIf
						IF thisScene.dialogueLine > -12
							IF IS_PLAYER_LISTENING_TO_DIALOGUE(thisScene)	
								#IF NOT IS_JAPANESE_BUILD
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"IG_6_a",5,ped[ped_pimp].id,"cs2_pimp")
									thisScene.triggerTime += 4000							
									thisScene.dialogueLine-- //using this var as a counter for random lines
								ENDIF
								#endif
							ELSE
								PAUSE_scene_timer(thisScene)
							ENDIF
						ENDIF
					BREAK
					
					CASE SCENE_ACTION
						SWITCH thisScene.nextAction
							CASE 0
								IF NOT IS_PED_INJURED(ped[ped_pimp].id)
									GIVE_WEAPON_TO_PED(ped[ped_pimp].id,WEAPONTYPE_PISTOL,10,TRUE)
								ENDIF
								thisScene.nextEvent++
								thisScene.nextAction++
							BREAK
							CASE 1
								IF NOT IS_PED_INJURED(ped[ped_pimp].id)
									#IF NOT IS_JAPANESE_BUILD
									IF IS_PLAYER_LISTENING_TO_DIALOGUE(thisScene)
										PLAY_SOUND_FROM_ENTITY(-1,"Pimp_Gunshot_Mic",ped[ped_pimp].id,"CAR_STEAL_2_SOUNDSET")
											cprintln(debug_trevor3,"kill conv I ")
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ELSE
										PLAY_SOUND_FROM_ENTITY(-1,"Pimp_Gunshot",ped[ped_pimp].id,"CAR_STEAL_2_SOUNDSET")
									ENDIF
									
									thisScene.nextEvent++
									thisScene.nextAction++
									#endif //prevent jap version going beyond this point
								ENDIF
							BREAK
							CASE 2
								IF NOT IS_PED_INJURED(ped[ped_pimp].id)
									SET_CURRENT_PED_WEAPON(ped[ped_pimp].id,WEAPONTYPE_UNARMED,TRUE)
								ENDIF
								thisScene.nextEvent++
								thisScene.nextAction++
							BREAK
							CASE 3
								IF NOT IS_PED_INJURED(ped[ped_pimp].id)
									SET_CURRENT_PED_WEAPON(ped[ped_pimp].id,WEAPONTYPE_PISTOL,TRUE)
								ENDIF
								ADD_NAVMESH_REQUIRED_REGION(187,-156,20)
								thisScene.nextEvent++
								thisScene.nextAction++
							BREAK
							CASE 4
								IF NOT IS_PED_INJURED(ped[ped_pimp].id)
									SET_CURRENT_PED_WEAPON(ped[ped_pimp].id,WEAPONTYPE_UNARMED,TRUE)
									
									//CLEAR_PED_TASKS(ped[ped_pimp].id)
									SEQ()										
										TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_hooker].id,13000)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<176.4232, -153.2906, 55.2087>>,pedmove_walk)
										TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_SMOKING")
									endseq(ped[ped_pimp].id)
									//SET_PED_KEEP_TASK(ped[ped_pimp].id,TRUE)
									//SET_PED_AS_NO_LONGER_NEEDED(ped[ped_pimp].id)
									
									IF NOT IS_PED_INJURED(ped[ped_hooker].id)
										//CLEAR_PED_TASKS(ped[ped_hooker].id)
										SEQ()
										TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_hooker].id,6000)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<231.9157, -173.5117, 55.9517>>,pedmove_walk,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS)
										TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_PROSTITUTE_LOW_CLASS")
									
										endseq(ped[ped_hooker].id)
									//	SET_PED_KEEP_TASK(ped[ped_hooker].id,TRUE)
										//SET_PED_AS_NO_LONGER_NEEDED(ped[ped_hooker].id)
									ENDIF
								ENDIF
								thisScene.nextEvent++
								thisScene.nextAction++
							BREAK
							CASE 5
							/*
								thisScene.pos = << 189.080, -159.296, 55.330 >>
								thisScene.rot = << -0.000, 0.000, -15.390 >>
								thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)
								
								
								#IF IS_JAPANESE_BUILD
									TASK_STAND_STILL(ped[ped_pimp].id, -1)
									TASK_STAND_STILL(ped[ped_hooker].id, -1)
								#ENDIF
								
								#IF NOT IS_JAPANESE_BUILD
									SET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0],0)
									SET_SYNCHRONIZED_SCENE_LOOPED(thisScene.syncSceneID[0],TRUE)
									TASK_SYNCHRONIZED_SCENE (ped[ped_pimp].id,thisScene.syncSceneID[0], "misscarsteal2PIMPSEX", "ShagLoop_PIMP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )								
									TASK_SYNCHRONIZED_SCENE (ped[ped_hooker].id,thisScene.syncSceneID[0], "misscarsteal2PIMPSEX", "ShagLoop_hooker", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
								#ENDIF
								*/
								thisScene.nextEvent++
								thisScene.nextAction++
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH			
			BREAK
			CASE SCENENAME_CHAD

				SWITCH thisScene.timeLine[thisScene.nextEvent].event
					CASE SCENE_DEBUG
						
						IF DOES_ENTITY_EXIST(ped[ped_chad].id) DELETE_PED(ped[ped_chad].id) ENDIF //debug 
						IF DOES_ENTITY_EXIST(ped[ped_chadGirl].id) DELETE_PED(ped[ped_chadGirl].id) ENDIF //debug 
						
						
						initPed(ped_chad,model_chad,<< 205.723, -110.766, 67.755 >>,140,null,vs_driver,pedrole_enemyUnaware)
						STOP_PED_SPEAKING(ped[ped_chad].id,TRUE)
						ped[ped_chadGirl].id = CREATE_PED(PEDTYPE_MISSION,s_f_y_hooker_01,<< 205.723, -110.766, 67.755 >>,140)
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_chadGirl].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0) //(accs)
						SET_ENTITY_LOAD_COLLISION_FLAG(ped[ped_chad].id,FALSE)			
						
						thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)
						thisScene.syncSceneID[1] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)
						
					
					//	TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_chad", INSTANT_BLEND_IN, -0.4)								
					//	TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_girl", INSTANT_BLEND_IN, -0.4)																														
						SET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0] ,0.04)
						thisScene.nextEvent++
						thisScene.triggerTime = GET_GAME_TIMER()
						thisScene.nextAction = 0
					BREAK
					CASE SCENE_INIT		
				
						thisScene.timeLine[0].event = SCENE_INIT
						thisScene.timeLine[1].event = SCENE_TRIGGER
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	2500	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	6200	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	8000	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	10000	)						
						SET_EVENT_DATA(thisScene,SCENE_ACTION,11500) 
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	14000	)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,16500) 
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	18000	)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,18000)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	22500	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	28300	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	32000	)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,33500)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,35000)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	36500	)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,41000)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	43300	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	48900	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	51500	)
						SET_EVENT_DATA(thisScene,SCENE_ACTION,51500)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	56000	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	58500	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	60500	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	62000	)						
						SET_EVENT_DATA(thisScene,SCENE_ACTION,65500) //26
						SET_EVENT_DATA(thisScene,SCENE_ACTION,67700)	
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	66700	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	70500	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	71500	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	76000	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	77500	)
						SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,	78800	)


						SET_EVENT_DATA(thisScene,SCENE_ACTION,0)

						//SET_EVENT_DATA(thisScene,SCENE_LISTEN_LINE,100)
						
						IF DOES_ENTITY_EXIST(ped[ped_chad].id) DELETE_PED(ped[ped_chad].id) ENDIF //debug 
						IF DOES_ENTITY_EXIST(ped[ped_chadGirl].id) DELETE_PED(ped[ped_chadGirl].id) ENDIF //debug 
						
						
						initPed(ped_chad,model_chad,<< 205.723, -110.766, 67.755 >>,140,null,vs_driver,pedrole_enemyUnaware)
						STOP_PED_SPEAKING(ped[ped_chad].id,TRUE)
						ped[ped_chadGirl].id = CREATE_PED(PEDTYPE_MISSION,s_f_y_hooker_01,<< 205.723, -110.766, 67.755 >>,140)
						
						SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_chadGirl].id)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(ped[ped_chadGirl].id, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0) //(accs)
						
						SET_ENTITY_LOAD_COLLISION_FLAG(ped[ped_chad].id,FALSE)			
						thisScene.pos = << 205.723, -110.766, 67.755 >>
						thisScene.rot = << -0.000, 0.000, -39.430 >>
						thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)
						SET_SYNCHRONIZED_SCENE_LOOPED(thisScene.syncSceneID[0],TRUE)				
						
						TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_idle_chad", INSTANT_BLEND_IN, -0.4)								
						TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_idle_girl", INSTANT_BLEND_IN, -0.4)																														

						thisScene.listenDia = ADD_CHOPPER_LISTENING_LOCATION(3,thisScene.pos)	
						thisScene.triggerTime = GET_GAME_TIMER()
						thisScene.nextEvent++
						
						//cprintln(debug_Trevor3,"HERE")
						IF bDoReplayToHere
						//OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_chad].id)
							//cprintln(debug_Trevor3,"HERE B")
							thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)		
							TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "Chad_Dip_chad", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in )								
							TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "Chad_Dip_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in)	
							thisScene.triggerTime = GET_GAME_TIMER() - thisScene.timeLine[thisScene.nextEvent].triggerTime
							thisScene.nextEvent=27
							thisScene.nextAction=9							
						ENDIF
					BREAK
					CASE SCENE_TRIGGER
						
						IF IS_CONDITION_TRUE(COND_CHAD_BEING_OBSERVED)
								//cprintln(DEBUG_TREVOR3,"CHAD BEING OBSERVED")
						//IF IS_SCENE_BEING_OBSERVED(thisScene)							
							thisScene.nextEvent++
							thisScene.nextAction=0	
							thisScene.triggerTime = GET_GAME_TIMER() //THIS LINE WAS MISSING CAUSING DIALOGUE TO START EARLY
						ENDIF
					BREAK
					CASE SCENE_LISTEN_LINE
						
						
						IF justSkipIt
							thisScene.dialogueLine++
							thisScene.nextEvent++
						ELSe
							IF IS_PLAYER_LISTENING_TO_DIALOGUE(thisScene)
								
								TEXT_LABEL_23 txt
								txt = "CAR_2_IG_7_"
								int iLine
								
								IF thisScene.dialogueLine < 9
									iLine = (thisScene.dialogueLine*2)+1
								ELIF thisScene.dialogueLine > 8 and thisScene.dialogueLine < 12
									iLine = (thisScene.dialogueLine*2)
								elif thisScene.dialogueLine = 12
									iLine = 23
								elif thisScene.dialogueLine = 13
									iLine = 24
								elif thisScene.dialogueLine = 14
									iLine = 26
								else
									iline = (thisScene.dialogueLine*2) - 3
								endif
									
							
								txt += iline
								
								//cprintln(debug_trevor3,"Line: ",txt)
								
								IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() != iline
									IF currentConvType = CONVTYPE_CHOPPER_CAM
								//		KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)
									ENDIF
								ENDIF
									
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"CAR_2_IG_7",txt,3,ped[ped_chad].id,"Chad",4,ped[ped_chadGirl].id,"cs2_girlfriend")
									thisScene.dialogueLine++
									thisScene.nextEvent++
								ENDIF
							ELSE
								thisScene.dialogueLine++
								thisScene.nextEvent++
							ENDIf		
						ENDIF
					BREAK
					CASE SCENE_ACTION
						SWITCH thisScene.nextAction
							case 0 			
							FALLTHRU
							CASE 4
											
								//cprintln(debug_Trevor3,"HERE C")
								thisScene.syncSceneID[1] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)			
								
								TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "Chad_Pullback_chad", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in )								
								TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "Chad_Pullback_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in)	
								thisScene.nextEvent++
								thisScene.nextAction++
								
							BREAK
							case 1 		
							FALLTHRU
							CASE 3
							FALLTHRU
							CASE 5
							FALLTHRU
							CASE 7
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id)
								OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_chad].id)
									thisScene.nextEvent=27
									thisScene.nextAction=8
									thisScene.triggerTime = GET_GAME_TIMER() - thisScene.timeLine[thisScene.nextEvent].triggerTime
								ELSE								
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[0])
										thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)			
										SET_SYNCHRONIZED_SCENE_LOOPED(thisScene.syncSceneID[0],true)
										TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_idle_chad", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in )								
										TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_idle_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in)											 										
									ENDIF
									thisScene.nextEvent++
									thisScene.nextAction++
								ENDIF
								
							BREAK
							CASE 2
							FALLTHRU
							CASE 6
								thisScene.syncSceneID[1] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)			
								
								TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "Chad_Armsaround_chad", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in )								
								TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "Chad_Armsaround_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in)	
								thisScene.nextEvent++
								thisScene.nextAction++
							BREAK													
							
							case 8			
							
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id)
								OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_chad].id)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[0])
									
										thisScene.syncSceneID[1] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)			
										
										TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "Chad_Dip_chad", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in )								
										TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "Chad_Dip_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in)	
										thisScene.nextAction++
									ELIF (IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[1]) AND GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[1]) >= 0.99)
										thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)			
										
										TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "Chad_Dip_chad", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in )								
										TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "Chad_Dip_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT,synced_scene_none,rbf_none,slow_blend_in)	
										thisScene.nextAction++
									ENDIF
									
									//thisScene.nextEvent++
									//thisScene.nextAction++
										
									
								ENDIF
							BREAK
							CASE 9
								IF (IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[1]) AND GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[1]) > 0.97)
								OR (IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[0]) AND GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0]) > 0.97)
									seq()
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 224.1202, -119.9946, 68.6437 >>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.5,ENAV_NO_STOPPING) 
										TASK_FOLLOW_WAYPOINT_RECORDING(null,"CS2_01",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
									endseq(ped[ped_chad].id)
									
									seq()
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 195.4088, -96.7522, 66.7088 >>,pedmove_walk)
										TASK_WANDER_STANDARD(null)
									endseq(ped[ped_chadGirl].id)
									kill_event(events_hover_over_second_location)
									add_event(EVENTS_SCENE_CHADJACK)
									cprintln(debug_Trevor3,"Start EVENTS_SCENE_CHADJACK")
									add_event(events_speed_up_chopper,STAGE_CARPARK)
									
								
									add_event(EVENTS_STEALTH_REMOVE_TRAFFIC,STAGE_CARPARK)
							
									RETURN TRUE
								ENDIF
							BREAK
							/*CASE 0
								IF NOT HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id)
								AND IS_PED_IN_SCAN_LIST(HUD,ped[ped_chad].id)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[1])
										IF GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[1]) > 0.797
											IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[0])
												thisScene.syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)											
												TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_chad", 0.4, -0.4,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE,0.4)								
												TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_girl", 0.4, -0.4,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE,0.4)
												SET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0] ,0.736)																		
												thisScene.triggerTime = GET_GAME_TIMER() - 47100
											ENDIF
										ENDIF
									ENDIF
									IF IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[0])
										IF GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0]) > 0.797
											IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[1])
												thisScene.syncSceneID[1] = CREATE_SYNCHRONIZED_SCENE(thisScene.pos, thisScene.rot)											
												TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, thisScene.syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_chad", 0.4, -0.4,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE,0.4)
												TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, thisScene.syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_girl", 0.4, -0.4,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE,0.4)
												SET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[1] ,0.736)									
												thisScene.triggerTime = GET_GAME_TIMER() - 47100
												
											ENDIF
										ENDIF
									ENDIF
								ELSE
									kill_event(events_scene_pimp)
									thisScene.nextEvent++
									thisScene.nextAction++
								ENDIF
							BREAK
							CASE 1 //skip forward in animation?
								
								IF (IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[0]) AND GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[0]) > 0.97)
								OR (IS_SYNCHRONIZED_SCENE_RUNNING(thisScene.syncSceneID[1]) AND GET_SYNCHRONIZED_SCENE_PHASE(thisScene.syncSceneID[1]) > 0.97)
									seq()
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 224.1202, -119.9946, 68.6437 >>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.5,ENAV_NO_STOPPING) 
										TASK_FOLLOW_WAYPOINT_RECORDING(null,"CS2_01",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
									endseq(ped[ped_chad].id)
									
									seq()
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 195.4088, -96.7522, 66.7088 >>,pedmove_walk)
										TASK_WANDER_STANDARD(null)
									endseq(ped[ped_chadGirl].id)
									kill_event(events_hover_over_second_location)
									add_event(EVENTS_SCENE_CHADJACK)
									add_event(events_speed_up_chopper,STAGE_CARPARK)
									
								
									add_event(EVENTS_STEALTH_REMOVE_TRAFFIC,STAGE_CARPARK)
							
									RETURN TRUE
								ENDIF
							BREAK*/
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC bool do_Scene(enumSceneName sceneName)

	int id = ENUM_TO_INT(sceneName)
	
	
	
	IF viewScene[id].triggerTime != 0
		IF viewScene[id].nextEvent < MAX_TIMELINES
	
			IF GET_GAME_TIMER() - viewScene[id].triggerTime > viewScene[id].timeLine[viewScene[id].nextEvent].triggerTime or (sceneName = SCENENAME_CHAD AND bDoReplayToHere)
				IF GET_GAME_TIMER() - viewScene[id].triggerTime < viewScene[id].timeLine[viewScene[id].nextEvent].triggerTime + 1000 or (sceneName = SCENENAME_CHAD AND bDoReplayToHere)
					IF triggerSceneEvent(viewScene[id],sceneName)
						//cprintln(debug_Trevor3,"ABORT ABORT")
						RETURN TRUE
					ENDIF
				ELSE
					IF triggerSceneEvent(viewScene[id],sceneName,TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIf
	ELSE
		
		IF triggerSceneEvent(viewScene[id],sceneName)
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	//viewScene_postal
	RETURN FALSE
endFUNC



proc fillSlots(float fx1, float fy1,float fx2, float fy2, float z,float heading,int maxslots, int slotsToFill, int avoidSlotA=-1, int avoidSlotB = -1, int avoidSlotC = -1, MODEL_NAMES avoidModel=DUMMY_MODEL_FOR_SCRIPT, MODEL_NAMES avoidModel2=DUMMY_MODEL_FOR_SCRIPT)
	int i
	bool slotFull[12]
	MODEL_NAMES thisModel
	bool bSkip
	
	for i = 0 to slotstoFill-1
		int slot = GET_RANDOM_INT_IN_RANGE(0,maxslots)
		IF slotFull[slot] = TRUE 
			bSkip = TRUE
		ENDIF
		
		if avoidSlotA != -1
			if slot = avoidSlotA
				bSkip = TRUE
			ENDIF
		ENDIF
		
		if avoidSlotB != -1
			if slot = avoidSlotB
				bSkip = TRUE
			ENDIF
		ENDIF
		
		if avoidSlotC != -1
			if slot = avoidSlotC
				bSkip = TRUE
			ENDIF
		ENDIF
		
		while bSkip = TRUE
			slot++
			
			bSkip=FALSE
			
			IF slot >= maxSlots
				slot=0
			ENDIF
			
			if avoidSlotA != -1
				if slot = avoidSlotA
					bSkip = true
				ENDIF
			ENDIF
			
			if avoidSlotB != -1
				if slot = avoidSlotB
					bSkip = true
				ENDIF
			ENDIF
			
			if avoidSlotC != -1
				if slot = avoidSlotC
					bSkip = true
				ENDIF
			ENDIF
				
			if bSkip = FALSE
				IF slotFull[slot] = true
					bSkip = true
				ENDIF
			ENDIF
		ENDWHILE			
		
		
		slotFull[slot] = TRUE
		
	//	SAVE_NEWLINE_TO_DEBUG_FILE()
		//SAVE_INT_TO_DEBUG_FILE(i)
		//SAVE_STRING_TO_DEBUG_FILE(":")
		//SAVE_INT_TO_DEBUG_FILE(slot)
		
	
		switch GET_RANDOM_INT_IN_RANGE(0,4)
			
			case 0
				thisModel = BURRITO
			BREAK
			case 1
				thisModel = DOMINATOR
				
			BREAK
			case 2
				thisModel = HABANERO
			BREAK
			case 3
				thismodel = DUBSTA
			BREAK			
		ENDSWITCH
		
		if avoidModel = thisMOdel
		or avoidModel2 = thisMOdel
			thisModel = HABANERO
		ENDIF
		
	
		carParkVehicles[carParkCounter] = CREATE_VEHICLE(thisModel,<<fx1+(((fx2-fx1)/to_float(maxSlots-1)) *to_float(slot)),fy1+(((fy2-fy1)/to_float(maxSlots-1)) *to_float(slot)),z>>,heading)
	
		
	//	SET_VEHICLE_DOORS_LOCKED(carParkVehicles[carParkCounter],VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		carParkCounter++

		
	ENDFOR
ENDPROC



/*
PROC checkBeam()
	if bBeamOn = TRUE		
		if not IS_ENTITY_DEAD(vehicle[vehChopper].id) and not IS_ENTITY_DEAD(vehicle[vehHeist].id)
			if bAimAtCar = TRUE				
				if DOES_ENTITY_EXIST(ped[ped_chad].id)
					if not IS_PED_INJURED(ped[ped_chad].id)
						vector vBeamRot = GET_ROTATION_BETWEEN_VECTORS(GET_ENTITY_COORDS(vehicle[vehChopper].id),GET_ENTITY_COORDS(ped[ped_chad].id))
						DISPLAY_BEAM(GET_ENTITY_COORDS(vehicle[vehChopper].id)+<<0,0,-1.0>>,<<vBeamRot.x,vBeamRot.y,vBeamRot.z*-1>>,fBeamDrawRange,fBeamSpread)
					ENDIF
				ENDIF
			ELSE
				camera_index renCam = GET_RENDERING_CAM()
				if DOES_CAM_EXIST(renCam)
					IF IS_CAM_RENDERING(renCam)
						DISPLAY_BEAM(GET_ENTITY_COORDS(vehicle[vehChopper].id)+<<0,0,-1.0>>,GET_CAM_ROT(renCam),fBeamDrawRange,fBeamSpread)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
*/



/*

int exCar2, iFocusFlag2
proc explodeCar(int &iCar,int &trackerFlag, int timer, int recording,int timeForFocus,int timeToStopPlayback,int timeToExplode)	
	int iTemp
	switch trackerFlag
		case 0
			if timer > timeForFocus
				if iCar = 0		
					REPEAT COUNT_OF(recordingData) itemp
						if recordingData[itemp].recording = recording
							iCar = itemp
							trackerFlag = 1
						//	outputDebugData("trackerFlag = 1")
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		BREAK
		case 1
			if IS_VEHICLE_DRIVEABLE(recordingData[iCar].vehicleID)
				//SET_ENTITY_LOAD_COLLISION_FLAG(recordingData[iCar].vehicleID,true)
				//SET_FOCUS_ENTITY(recordingData[iCar].vehicleID)
				trackerFlag = 2
				//outputDebugData("trackerFlag = 2")				
			ENDIF
		break
		case 2
			if IS_VEHICLE_DRIVEABLE(recordingData[iCar].vehicleID)	
				if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(recordingData[iCar].vehicleID)				
					if timer > timeToStopPlayback
						STOP_PLAYBACK_RECORDED_VEHICLE(recordingData[iCar].vehicleID)
						trackerFlag = 3
						//outputDebugData("trackerFlag = 3")
					ENDIF
				ENDIF
			ELSE
				trackerFlag = 0
				//outputDebugData("trackerFlag = ob")
			ENDIF
		break
		case 3
			if IS_VEHICLE_DRIVEABLE(recordingData[iCar].vehicleID)	
				if timer > timeToExplode					
					EXPLODE_VEHICLE(recordingData[iCar].vehicleID)
					//CLEAR_FOCUS()
					trackerFlag = 0
					//outputDebugData("trackerFlag = 0c")
				ENDIF
			ELSE
				trackerFlag = 0
				//outputDebugData("trackerFlag = 0d")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
*/
func bool do_fade(bool doFadeOut=true)
	if doFadeOut = true
		if not IS_SCREEN_FADED_OUT() and not IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(250)
		ENDIF
		
		if IS_SCREEN_FADED_OUT()
			return TRUE
		ENDIF
	ELSE
		if not IS_SCREEN_FADED_IN() and not IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(250)
		ENDIF
		
		if IS_SCREEN_FADED_IN()
			return TRUE
		ENDIF
	ENDIF
	return FALSE
ENDFUNC
/*
proc display_input_values()
	int i
	float fy=0.1, fx=0.1
	for i = 0 to 82
		if fy > 0.9
			fy=0.1
			fx+=0.1
		ENDIF
		fy+=0.04
		showIntOnScreen(fx,fy,i)	
		showIntOnScreen(fx+0.04,fy,GET_CONTROL_VALUE(player_control,INT_TO_ENUM(CONTROL_ACTION,i)))		
	ENDFOR	
ENDPROC
*/

func text_label get_text_label()
	TEXT_LABEL txt="d"
	return txt
endfunc



//================================================= PED ACTIONS ===================================


	



/*				
FUNC STRING GET_CONTROL_VALUE_STRING(CONTROL_ACTION actionToCheck)
	//PRINTLN("GET CONTROL VALUE")
	SWITCH actionToCheck
		CASE INPUT_NEXT_CAMERA RETURN "INPUT_NEXT_CAMERA" BREAK
		CASE INPUT_SPRINT RETURN "INPUT_SPRINT" BREAK
		CASE INPUT_JUMP RETURN "INPUT_JUMP" BREAK
		CASE INPUT_ENTER RETURN "INPUT_ENTER" BREAK
		CASE INPUT_ATTACK RETURN "INPUT_ATTACK" BREAK
		CASE INPUT_AIM RETURN "INPUT_AIM" BREAK
		CASE INPUT_LOOK_BEHIND RETURN "INPUT_LOOK_BEHIND" BREAK
		CASE INPUT_PHONE RETURN "INPUT_PHONE" BREAK
		CASE INPUT_NEXT_WEAPON RETURN "INPUT_NEXT_WEAPON" BREAK
		CASE INPUT_PREV_WEAPON RETURN "INPUT_PREV_WEAPON" BREAK
		CASE INPUT_SPECIAL_ABILITY RETURN "INPUT_SPECIAL_ABILITY" BREAK
		CASE INPUT_SPECIAL_ABILITY_SECONDARY RETURN "INPUT_SPECIAL_ABILITY_SECONDARY" BREAK
		CASE INPUT_MOVE_LR RETURN "INPUT_MOVE_LR" BREAK
		CASE INPUT_MOVE_UD RETURN "INPUT_MOVE_UD" BREAK
		CASE INPUT_LOOK_LR RETURN "INPUT_LOOK_LR" BREAK
		CASE INPUT_LOOK_UD RETURN "INPUT_LOOK_UD" BREAK
		CASE INPUT_DUCK RETURN "INPUT_DUCK" BREAK
		CASE INPUT_SELECT_WEAPON RETURN "INPUT_SELECT_WEAPON" BREAK
		CASE INPUT_PICKUP RETURN "INPUT_PICKUP" BREAK
		CASE INPUT_SNIPER_ZOOM RETURN "INPUT_SNIPER_ZOOM" BREAK
		CASE INPUT_COVER RETURN "INPUT_COVER" BREAK
		CASE INPUT_RELOAD RETURN "INPUT_RELOAD" BREAK
		CASE INPUT_TALK RETURN "INPUT_TALK" BREAK
		CASE INPUT_DETONATE RETURN "INPUT_DETONATE" BREAK
		CASE INPUT_RAPPEL_JUMP RETURN "INPUT_RAPPEL_JUMP" BREAK
		CASE INPUT_RAPPEL_SMASH_WINDOW RETURN "INPUT_RAPPEL_SMASH_WINDOW" BREAK

		// in vehicle controls
		CASE INPUT_VEH_MOVE_LR RETURN "INPUT_VEH_MOVE_LR" BREAK
		CASE INPUT_VEH_MOVE_UD RETURN "INPUT_VEH_MOVE_UD" BREAK
		CASE INPUT_VEH_GUN_LR RETURN "INPUT_VEH_GUN_LR" BREAK
		CASE INPUT_VEH_GUN_UD RETURN "INPUT_VEH_GUN_UD" BREAK
		CASE INPUT_VEH_ATTACK RETURN "INPUT_VEH_ATTACK" BREAK
		CASE INPUT_VEH_ATTACK2 RETURN "INPUT_VEH_ATTACK2" BREAK
		CASE INPUT_VEH_ACCELERATE RETURN "INPUT_VEH_ACCELERATE" BREAK
		CASE INPUT_VEH_BRAKE RETURN "INPUT_VEH_BRAKE" BREAK
		CASE INPUT_VEH_HEADLIGHT RETURN "INPUT_VEH_HEADLIGHT" BREAK
		CASE INPUT_VEH_EXIT RETURN "INPUT_VEH_EXIT" BREAK
		CASE INPUT_VEH_HANDBRAKE RETURN "INPUT_VEH_HANDBRAKE" BREAK
		CASE INPUT_VEH_DUCK RETURN "INPUT_VEH_DUCK" BREAK
		CASE INPUT_VEH_HOTWIRE_LEFT RETURN "INPUT_VEH_HOTWIRE_LEFT" BREAK
		CASE INPUT_VEH_HOTWIRE_RIGHT RETURN "INPUT_VEH_HOTWIRE_RIGHT" BREAK
		CASE INPUT_VEH_LOOK_BEHIND RETURN "INPUT_VEH_LOOK_BEHIND" BREAK
		CASE INPUT_VEH_CIN_CAM RETURN "INPUT_VEH_CIN_CAM" BREAK
		CASE INPUT_VEH_NEXT_RADIO RETURN "INPUT_VEH_NEXT_RADIO" BREAK
		CASE INPUT_VEH_PREV_RADIO RETURN "INPUT_VEH_PREV_RADIO" BREAK
		CASE INPUT_VEH_HORN RETURN "INPUT_VEH_HORN" BREAK
		CASE INPUT_VEH_FLY_THROTTLE_UP RETURN "INPUT_VEH_FLY_THROTTLE_UP" BREAK
		CASE INPUT_VEH_FLY_THROTTLE_DOWN RETURN "INPUT_VEH_FLY_THROTTLE_DOWN" BREAK
		CASE INPUT_VEH_FLY_YAW_LEFT RETURN "INPUT_VEH_FLY_YAW_LEFT" BREAK
		CASE INPUT_VEH_FLY_YAW_RIGHT RETURN "INPUT_VEH_FLY_YAW_RIGHT" BREAK

		// melee combat controls
		CASE INPUT_MELEE_ATTACK_LIGHT RETURN "INPUT_MELEE_ATTACK_LIGHT" BREAK
		CASE INPUT_MELEE_ATTACK_HEAVY RETURN "INPUT_MELEE_ATTACK_HEAVY" BREAK
		CASE INPUT_MELEE_BLOCK RETURN "INPUT_MELEE_BLOCK" BREAK
		
		// Direct controls. Used by frontend only.
		CASE INPUT_FRONTEND_DOWN RETURN "INPUT_FRONTEND_DOWN" BREAK
		CASE INPUT_FRONTEND_UP RETURN "INPUT_FRONTEND_UP" BREAK
		CASE INPUT_FRONTEND_LEFT RETURN "INPUT_FRONTEND_LEFT" BREAK
		CASE INPUT_FRONTEND_RIGHT RETURN "INPUT_FRONTEND_RIGHT" BREAK
		CASE INPUT_FRONTEND_RDOWN RETURN "INPUT_FRONTEND_RDOWN" BREAK
		CASE INPUT_FRONTEND_RUP RETURN "INPUT_FRONTEND_RUP" BREAK
		CASE INPUT_FRONTEND_RLEFT RETURN "INPUT_FRONTEND_RLEFT" BREAK
		CASE INPUT_FRONTEND_RRIGHT RETURN "INPUT_FRONTEND_RRIGHT" BREAK
		CASE INPUT_FRONTEND_AXIS_X RETURN "INPUT_FRONTEND_AXIS_X" BREAK
		CASE INPUT_FRONTEND_AXIS_Y RETURN "INPUT_FRONTEND_AXIS_Y" BREAK
		CASE INPUT_FRONTEND_RIGHT_AXIS_X RETURN "INPUT_FRONTEND_RIGHT_AXIS_X" BREAK
		CASE INPUT_FRONTEND_RIGHT_AXIS_Y RETURN "INPUT_FRONTEND_RIGHT_AXIS_Y" BREAK
		CASE INPUT_FRONTEND_PAUSE RETURN "INPUT_FRONTEND_PAUSE" BREAK
		CASE INPUT_FRONTEND_ACCEPT RETURN "INPUT_FRONTEND_ACCEPT" BREAK
		CASE INPUT_FRONTEND_CANCEL RETURN "INPUT_FRONTEND_CANCEL" BREAK
		CASE INPUT_FRONTEND_X RETURN "INPUT_FRONTEND_X" BREAK
		CASE INPUT_FRONTEND_Y RETURN "INPUT_FRONTEND_Y" BREAK
		CASE INPUT_FRONTEND_LB RETURN "INPUT_FRONTEND_LB" BREAK
		CASE INPUT_FRONTEND_RB RETURN "INPUT_FRONTEND_RB" BREAK
		CASE INPUT_FRONTEND_LT RETURN "INPUT_FRONTEND_LT" BREAK 
		CASE INPUT_FRONTEND_RT RETURN "INPUT_FRONTEND_RT" BREAK
		CASE INPUT_FRONTEND_SELECT RETURN "INPUT_FRONTEND_SELECT" BREAK
		CASE INPUT_ATTACK2 RETURN "INPUT_ATTACK2" BREAK 
		CASE INPUT_MELEE_ATTACK1 RETURN "INPUT_MELEE_ATTACK1" BREAK
		CASE INPUT_MELEE_ATTACK2 RETURN "INPUT_MELEE_ATTACK2" BREAK
		CASE INPUT_WHISTLE RETURN "INPUT_WHISTLE" BREAK
		CASE INPUT_MOVE_LEFT_ONLY RETURN "INPUT_MOVE_LEFT_ONLY" BREAK
		CASE INPUT_MOVE_RIGHT_ONLY RETURN "INPUT_MOVE_RIGHT_ONLY" BREAK
		CASE INPUT_MOVE_UP_ONLY RETURN "INPUT_MOVE_UP_ONLY" BREAK
		CASE INPUT_MOVE_DOWN_ONLY RETURN "INPUT_MOVE_DOWN_ONLY" BREAK
		CASE INPUT_LOOK_LEFT RETURN "INPUT_LOOK_LEFT" BREAK
		CASE INPUT_LOOK_RIGHT RETURN "INPUT_LOOK_RIGHT" BREAK
		CASE INPUT_LOOK_UP RETURN "INPUT_LOOK_UP" BREAK
		CASE INPUT_LOOK_DOWN RETURN "INPUT_LOOK_DOWN" BREAK
		CASE INPUT_SNIPER_ZOOM_IN RETURN "INPUT_SNIPER_ZOOM_IN" BREAK
		CASE INPUT_SNIPER_ZOOM_OUT RETURN "INPUT_SNIPER_ZOOM_OUT" BREAK
		CASE INPUT_SNIPER_ZOOM_IN_ALTERNATE RETURN "INPUT_SNIPER_ZOOM_IN_ALTERNATE" BREAK
		CASE INPUT_SNIPER_ZOOM_OUT_ALTERNATE RETURN "INPUT_SNIPER_ZOOM_OUT_ALTERNATE" BREAK
		CASE INPUT_VEH_MOVE_LEFT RETURN "INPUT_VEH_MOVE_LEFT" BREAK
		CASE INPUT_VEH_MOVE_RIGHT RETURN "INPUT_VEH_MOVE_RIGHT" BREAK
		CASE INPUT_VEH_MOVE_UP RETURN "INPUT_VEH_MOVE_UP" BREAK
		CASE INPUT_VEH_MOVE_DOWN RETURN "INPUT_VEH_MOVE_DOWN" BREAK
		CASE INPUT_VEH_GUN_LEFT RETURN "INPUT_VEH_GUN_LEFT" BREAK
		CASE INPUT_VEH_GUN_RIGHT RETURN "INPUT_VEH_GUN_RIGHT" BREAK
		CASE INPUT_VEH_GUN_UP RETURN "INPUT_VEH_GUN_UP" BREAK
		CASE INPUT_VEH_GUN_DOWN RETURN "INPUT_VEH_GUN_DOWN" BREAK
		CASE INPUT_VEH_LOOK_LEFT RETURN "INPUT_VEH_LOOK_LEFT" BREAK
		CASE INPUT_VEH_LOOK_RIGHT RETURN "INPUT_VEH_LOOK_RIGHT" BREAK
	ENDSWITCH
	RETURN "UNKNOWN"
ENDFUNC

proc FIND_CONTROL_INPUT()
	//PRINTLN("FIND CON")
	int i
	CONTROL_ACTION thisAction
	WHILE INT_TO_ENUM(CONTROL_ACTION,i) != MAX_INPUTS
		thisAction = INT_TO_ENUM(CONTROL_ACTION,i)
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL,thisAction)
			//PRINTLN("PLAYER_CONTROL: ",GET_CONTROL_VALUE_STRING(thisAction))
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,thisAction)
			PRINTLN("FRONTEND_CONTROL: ",GET_CONTROL_VALUE_STRING(thisAction))
		ENDIF
		IF IS_CONTROL_PRESSED(camera_control,thisAction)		
			PRINTLN("camera_control: ",GET_CONTROL_VALUE_STRING(thisAction))
		ENDIF
		i++
	ENDWHILE		
ENDPROC
		*/		

FUNC INT GET_EVENTS_FLAG(EnumEvents eventToCheck)
	int iE
	REPEAT COUNT_OF(events) iE
		if events[iE].active = TRUE
			if events[iE].thisEvent = eventToCheck
				return events[iE].flag
			ENDIF
		ENDIF
	ENDREPEAT
	return 0
ENDFUNC

PROC SET_EVENTS_FLAG(EnumEvents eventToCheck, int iFlagValue)
	int iE
	REPEAT COUNT_OF(events) iE
		if events[iE].active = TRUE
			if events[iE].thisevent = eventToCheck
				events[iE].flag = iFlagValue
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

proc control_events(enumEvents specificEventToRun = EVENTS_NULL, bool bPauseFailDuringSkip = FALSE)
	int i,iTemp
	//float fTemp, fTemp2

	//float fTemp
	

	

	if not IS_ENTITY_DEAD(PLAYER_PED_ID())
		for i = 0 to MAX_EVENTS-1
			if events[i].active = TRUE
				IF specificEventToRun = EVENTS_NULL OR events[i].thisEvent = specificEventToRun
					//if mission has progressed past this event's defined final mission stage, clean it up.
					if enum_to_int(missionProgress) >= enum_to_int(events[i].ForceCleanupStage)
						events[i].flag = CLEANUP
					ENDIF
					
					IF events[i].flag = CLEANUP
						events[i].active = FALSE
					ENDIF
				
					switch events[i].thisEvent														
						case EVENTS_CHOPPER_LANDS
							IF DOES_ENTITY_EXIST(vehicle[vehChopper].id)
								IF NOT IS_ENTITY_DEAD(vehicle[vehChopper].id)
									IF events[i].flag = 0
										IF not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehChopper].id)
											FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,true)
											events[i].flag = 1
											
										ENDIF
									ELSE
										IF IS_VEHICLE_SEAT_FREE(vehicle[vehChopper].id,VS_DRIVER)
											SET_VEHICLE_DOORS_LOCKED(vehicle[vehChopper].id,VEHICLELOCK_UNLOCKED)
											events[i].active = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						break
						
						CASE EVENTS_REMOVE_FRANKLIN
							if not IS_PED_INJURED(ped[ped_trevor].id)	
								if not IS_ENTITY_DEAD(PLAYER_PED_ID())	
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(ped[ped_trevor].id),FALSE) > 200.0								
										DELETE_PED(ped[ped_trevor].id)
										if DOES_ENTITY_EXIST(vehicle[player_vehicle].id)
											DELETE_VEHICLE(vehicle[player_vehicle].id)
										ENDIF
										events[i].active = FALSE
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE EVENTS_LOAD_FIRST_LOCATION_PEDS
							switch events[i].flag
								case 0
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),destMarker[0].coord,FALSE) < 950.0
										SET_ASSET_STAGE(ASSETS_STAGE_PREP_AREA_1)
										set_chopper_speed_limits(HUD,0,50)
										events[i].flag++
									ENDIF
								BREAK
								case 1
									IF IS_ASSET_STAGE_READY(ASSETS_STAGE_PREP_AREA_1)
										add_event(events_scene_mugging)
										add_event(EVENTS_SCENE_PERVERT)										
									//	add_event(events_witness_pervert)
										events[i].flag++
									ENDIF
								break
								CASE 2
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),destMarker[0].coord,FALSE) < 750.0
										KILL_FACE_TO_FACE_CONVERSATION()
										events[i].flag++
									endif
								break
								case 3
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								//	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),destMarker[0].coord,FALSE) < 550.0
										add_event(events_hover_over_first_location,STAGE_APPROACH_SCAN_AREA_THREE)
										//add_event(EVENTS_STREAM_ALLEY)									
										set_chopper_speed_limits(HUD,0,25)
										events[i].flag++
									ENDIF
								BREAK
								case 4
							
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_arrive",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")	
									
										START_AUDIO_SCENE("CAR_2_SCAN_THE_SUSPECTS")
										events[i].flag = 2001
									ENDIF
								break
								CASE 2001
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),destMarker[0].coord,FALSE) < 350.0
										set_chopper_speed_limits(HUD,0,25)
										events[i].flag = 2002
									ENDIF
								BREAK
								CASE 2002				
									if not IS_ENTITY_DEAD(PLAYER_PED_ID())							
										IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),destMarker[0].coord,FALSE) < 250.0
											//HUD.speedMultiplier = 1.0
											events[i].flag = 2003
										ENDIF
									ENDIF
								BREAK
								CASE 2003
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_courier].id)									
									OR HAS_PED_BEEN_SCANNED(HUD,ped[ped_wife].id) 
									OR HAS_PED_BEEN_SCANNED(HUD,ped[ped_husband].id) 
									OR HAS_PED_BEEN_SCANNED(HUD,ped[ped_pervert].id) 
										add_event(events_listen_help)
										events[i].flag = 5
									ENDIF
									
									
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),destMarker[0].coord,FALSE) < 220.0
									
										if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()																				
											add_event(events_listen_help)
											events[i].flag = 5
										ENDIF
									ENDIF
								BREAK
								CASE 5
							
									iTemp = 0
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_husband].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_husband].id) iTemp++ ENDIF
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_pervert].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_pervert].id) iTemp++ ENDIF
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_wife].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_wife].id) iTemp++ ENDIF
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_courier].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_courier].id) iTemp++ ENDIF

									IF iTemp = 4				
										INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										events[i].active = FALSE
									ENDIF
								BREAK
							ENDSWITCH
						break
													
						CASE EVENTS_LOAD_FINAL_LOCATION_PEDS
							SWITCH events[i].flag
								CASE 0
									SET_ASSET_STAGE(ASSETS_STAGE_PREP_AREA_2)
									IF NOT IS_CONV_ROOT_PLAYING("CS2_scan_wom")
									AND NOT IS_CONV_ROOT_PLAYING("CS2_scanning")
										IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_tryNext",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")											
											
											events[i].flag = 1
										ENDIF
									ENDIF
								BREAK
								CASE 1
									IF IS_THIS_CONVERSATION_ROOT_PLAYING("CS2_tryNext")
										events[i].flag = 2
									ENDIF
								BREAK
								CASE 2
									IF IS_ASSET_STAGE_READY(ASSETS_STAGE_PREP_AREA_2)
									OR IS_ASSET_STAGE_READY(ASSETS_STAGE_RELEASE_AREA_1)
										add_event(events_scene_chadGirl)										
										add_event(events_scene_pimp)
										add_event(events_bum_on_chad_walk)
										add_event(events_dog_walks_by)
										events[i].flag++
									ENDIF
								BREAK
								
								CASE  3
									IF NOT IS_THIS_CONVERSATION_ROOT_PLAYING("CS2_tryNext")
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_husband].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_wife].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_pervert].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_courier].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_mugger].id)	

										events[i].flag++
									ENDIF
								BREAK
								CASE 4
									if not IS_ENTITY_DEAD(PLAYER_PED_ID())																	
										
											//add_event(EVENTS_LOAD_COLLISION_AROUND_CHAD,STAGE_TAKE_ZTYPE)	
											add_event(events_hover_over_second_location,STAGE_CHASE_BEGINS)

											events[i].flag++
									ENDIF
								BREAK
								CASE 5
									IF NOT IS_PED_INJURED(ped[ped_franklin].id)
										delete_ped(ped[ped_franklin].id)
									ENDIF
									IF DOES_ENTITY_EXIST(vehicle[vehFranklin].id)	
										IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[vehFranklin].id,FALSE)
											SET_ENTITY_AS_MISSION_ENTITY(vehicle[vehFranklin].id,TRUE,TRUE)
										ENDIF
										delete_vehicle(vehicle[vehFranklin].id)
									ENDIF
									IF CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN,  << -93.4581, -66.9989, 55.8005 >>,345.5313,true,VEHICLE_TYPE_CAR)
										IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
											SET_ENTITY_INVINCIBLE(vehicle[vehFranklin].id,true)
											SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehicle[vehFranklin].id,false)
										ENDIF
															
										events[i].flag++
									ENDIF
								BREAK
								CASE 6
									IF CREATE_PLAYER_PED_INSIDE_VEHICLE(ped[ped_franklin].id,CHAR_FRANKLIN,vehicle[vehFranklin].id)
										//ped[ped_chadGirl].id = CREATE_PED (PEDTYPE_MISSION ,U_F_Y_JEWELASS_01,<< 205.9948, -112.6291, 67.8807 >>, 238.3422)
										if IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
											if IS_VEHICLE_SEAT_FREE(vehicle[vehFranklin].id)
												SET_PED_INTO_VEHICLE(ped[ped_franklin].id,vehicle[vehFranklin].id)									
											ENDIF
										
											START_PLAYBACK_RECORDED_VEHICLE(vehicle[vehFranklin].id,102,sVehicleRecordingLibrary)																				
											SET_ROADS_IN_ANGLED_AREA(<<-48.346008,-109.031822,82.549194>>, <<195.550781,-197.283768,-38.864712>>, 155.812500,FALSE,FALSE)
										ENDIF
										events[i].flag++
									ENDIF
								BREAK
								CASE 7
									if DOES_ENTITY_EXIST(ped[ped_chad].id) and DOES_ENTITY_EXIST(ped[ped_chadGirl].id) and DOES_ENTITY_EXIST(ped[ped_punter].id) and DOES_ENTITY_EXIST(ped[ped_hooker].id) and DOES_ENTITY_EXIST(ped[ped_pimp].id)
									AND DOES_ENTITY_EXIST(ped[ped_dog_walk].id)	AND DOES_ENTITY_EXIST(ped[ped_bum].id)								
										PRINT_NOW("CH_INS12C",6000,1)
										EMPTY_SCAN_LIST(HUD)
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_chad].id,true,HUD_FOE,FALSE,true,true,true)																		
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_chad].id,"CH_CHAD",8)	
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_chadGirl].id,true,HUD_UNKNOWN,FALSE,true,true,true)
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_chadGirl].id,"CH_NAME9",7)	
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_punter].id,true,HUD_UNKNOWN,FALSE,true,true,true)
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_punter].id,"CH_NAME7",6)	
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_hooker].id,true,HUD_UNKNOWN,FALSE,true,true,true)
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_hooker].id,"CH_NAME8",1)	
										set_ped_scan_crime(HUD,ped[ped_hooker].id,0)
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_pimp].id,true,HUD_UNKNOWN,FALSE,true,true,true)
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_pimp].id,"CH_NAME6",2)	
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_dog_walk].id,true,HUD_UNKNOWN,FALSE,true,true,true)
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_dog_walk].id,"CH_NAME10",5)
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_bum].id,true,HUD_UNKNOWN,FALSE,true,true,true)
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_bum].id,"CH_NAME11",3)
										events[i].flag++
									ENDIF
								BREAK
								CASE 8
												
									if HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id) 
										KILL_EVENT(events_scene_pimp)
										KILL_EVENT(EVENTS_SCENE_PERVERT)
										KILL_EVENT(events_scene_mugging)
										
										IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_chadGirl].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_chadGirl].id)
											INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										ENDIF
										
										IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_punter].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_punter].id)
											INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										ENDIF
										
										IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_hooker].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_hooker].id)
											INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										ENDIF
										
										IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_pimp].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_pimp].id)
											INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										ENDIF
										
										IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_dog_walk].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_dog_walk].id)
											INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										ENDIF
										
										IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_bum].id) OR NOT IS_PED_IN_SCAN_LIST(HUD,ped[ped_bum].id)
											INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED)
										ENDIF										
										
										INFORM_MISSION_STATS_OF_INCREMENT(CS2_TARGETS_SCANNED) //chad
										
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_chadGirl].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_punter].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_hooker].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_pimp].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_dog_walk].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_bum].id)
										events[i].active = FALSE
									ENDIF		
								BREAK
							ENDSWITCH
						break
						
						
						case EVENTS_CLEAR_FINAL_LOCATION
							
							REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_chadGirl].id)	
							if not IS_PED_INJURED(ped[ped_chadGirl].id)
								SET_PED_KEEP_TASK(ped[ped_chadGirl].id,true)
								SET_PED_AS_NO_LONGER_NEEDED(ped[ped_chadGirl].id)
							ENDIF
							
							events[i].active = FALSE
						break
						
						CASE EVENTS_CHOPPER_BLADES
							if DOES_ENTITY_EXIST(vehicle[vehChopper].id)
								if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
									IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
										SET_VEHICLE_ENGINE_ON(vehicle[vehChopper].id,true,true)									
										SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
									ENDIF							
								ENDIF
							ENDIF
						BREAK
					
						
						
						CASE events_disable_franklin_car
							if IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
								SET_VEHICLE_ENGINE_ON(vehicle[vehFranklin].id,FALSE,FALSE)
								SET_VEHICLE_ENGINE_HEALTH(vehicle[vehFranklin].id,100)
							ENDIF
						BREAK
						
						
						
						CASE EVENTS_HOVER_CHOPPER
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								NAVIGATE_TO_POINT(HUD,vehicle[vehChopper].id,GET_ENTITY_COORDS(vehicle[vehHeist].id))
							ENDIF
						BREAK
						
						CASE EVENTS_FILL_CARPARK
							SWITCH events[i].flag
								CASE CLEANUP								
									events[i].active = FALSE
								BREAK
								CASE 0
									IF NOT IS_ASSET_STAGE_READY(ASSETS_STAGE_PREP_CAR_PARK)
										SET_ASSET_STAGE(ASSETS_STAGE_PREP_CAR_PARK)
									ENDIF
									
								//	add_event(events_scene_car_fuck)
								//	add_event(events_scene_car_wank)
									events[i].flag++
								BREAK
								CASE 1
									IF IS_ASSET_STAGE_READY(ASSETS_STAGE_PREP_CAR_PARK)
										events[i].flag++
									ENDIF
								BREAK
								CASE 2
									initVehicle(veh_camper,BURRITO,<< -1306.9370, -218.7608, 50.5497 >>, 303.7934)
									initVehicle(veh_postie,BURRITO,<< -1281.5746, -223.2485, 50.5497 >>, 35.5040)					
									initVehicle(veh_vanhide,BURRITO,<< -1264.3513, -245.3335, 50.5499 >>, 214.0007)
									initVehicle(veh_phoneChat,HABANERO,<<-1260.8893, -226.0429, 50.5499>>, 303.0007)
									initVehicle(veh_lean,DUBSTA,<<-1292.8159, -185.4189, 50.5497>>, 33.0002)
									SET_VEHICLE_LIVERY(vehicle[veh_camper].id,1)
									SET_VEHICLE_LIVERY(vehicle[veh_postie].id,2)
									SET_VEHICLE_LIVERY(vehicle[veh_vanhide].id,3)
									SET_VEHICLE_DOOR_OPEN(vehicle[veh_phoneChat].id,SC_DOOR_FRONT_LEFT)
									SET_VEHICLE_DOORS_LOCKED(vehicle[veh_camper].id,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
									//SET_VEHICLE_DOORS_LOCKED(vehicle[veh_postie].id,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
									//SET_VEHICLE_DOORS_LOCKED(vehicle[veh_wrong1].id,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
									SET_VEHICLE_DOORS_LOCKED(vehicle[veh_vanhide].id,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehicle[veh_vanhide].id, FALSE)
									
									
									//initped(ped_wrong_wanker,S_M_M_JANITOR,vNull,0.0,vehicle[veh_wrong1].id,VS_DRIVER,pedrole_civilian)
									//brainPlayAnim()
									events[i].flag++
								BREAK
								CASE 3										
									fillSlots(-1311.5907, -212.4394,-1289.1437, -244.9437, 50.5497,125,11,4,1,2,-1,dubsta,BURRITO)
									IF IS_REPLAY_BEING_SET_UP()
										fillSlots(-1277.0620, -255.4843,-1254.7090, -239.9274, 50.5497,215,8,2,4,5)
										fillSlots(-1258.6924, -228.6362,-1283.9841, -193.4677, 50.5497,306.5,12,3,5,4,1)
										fillSlots(-1292.8164, -185.4186,-1315.0455, -200.8566, 50.5497,33,8,3,0)
										fillSlots(-1310.9569, -211.8995,-1288.5760, -243.4570, 55.1017,125,11,3)
										fillSlots(-1277.0620, -255.4843,-1254.7090, -239.9274, 55.1017,215,8,2)
										fillSlots(-1258.6924, -228.6362,-1283.9841, -193.4677, 55.1017,306.5,12,4)
										fillSlots(-1292.8164, -185.4186,-1315.0455, -200.8566, 55.1017,33,8,2)
										events[i].flag = cleanup
									ELSE
										events[i].flag = 4
										events[i].timer = GET_GAME_TIMER() + 500
									ENDIF
									
								BREAK
								CASE 4
									IF GET_GAME_TIMER() > events[i].timer																				
										fillSlots(-1277.0620, -255.4843,-1254.7090, -239.9274, 50.5497,215,8,2,4,5)
										events[i].timer = GET_GAME_TIMER() + 500
										events[i].flag = 5
									ENDIF
								BREAK
								CASE 5
									IF GET_GAME_TIMER() > events[i].timer								
										fillSlots(-1258.6924, -228.6362,-1283.9841, -193.4677, 50.5497,306.5,12,3,5,4,1)
										events[i].timer = GET_GAME_TIMER() + 500
										events[i].flag = 6
									ENDIF
								BREAK
								CASE 6
									IF GET_GAME_TIMER() > events[i].timer									
										fillSlots(-1292.8164, -185.4186,-1315.0455, -200.8566, 50.5497,33,8,3,0)
										events[i].timer = GET_GAME_TIMER() + 500
										events[i].flag = 7
									ENDIF	
								BREAK
								CASE 7
									IF GET_GAME_TIMER() > events[i].timer
										fillSlots(-1310.9569, -211.8995,-1288.5760, -243.4570, 55.1017,125,11,3)
										events[i].timer = GET_GAME_TIMER() + 500
										events[i].flag = 8
									ENDIF
								BREAK
								CASE 8
									IF GET_GAME_TIMER() > events[i].timer
										fillSlots(-1277.0620, -255.4843,-1254.7090, -239.9274, 55.1017,215,8,2)
										events[i].timer = GET_GAME_TIMER() + 500
										events[i].flag = 9
									ENDIF
								BREAK
								CASE 9
									IF GET_GAME_TIMER() > events[i].timer
										fillSlots(-1258.6924, -228.6362,-1283.9841, -193.4677, 55.1017,306.5,12,4)
										events[i].timer = GET_GAME_TIMER() + 500
										events[i].flag = 10
									ENDIF
								BREAK
								CASE 10
									IF GET_GAME_TIMER() > events[i].timer
										fillSlots(-1292.8164, -185.4186,-1315.0455, -200.8566, 55.1017,33,8,2)
										events[i].flag = cleanup							
									ENDIF								
								BREAK
							ENDSWITCH
						BREAK									
						
						
						
						
						
			
						/*
						case EVENTS_GETCHAD_INST
							if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehicle[vehHeist].id)
										PRINT("CH_INS24",DEFAULT_GOD_TEXT_TIME,1)									
									ENDIF
								ENDIF							
								events[i].active=FALSE	
							ENDIF
						BREAK
						*/
						CASE EVENTS_KILL_CONVERSATION_WHEN_LEAVING_CAR
							If events[i].flag = 0
								if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									if IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehicle[vehHeist].id)
										events[i].flag = 1
									ENDIF
								ENDIF
							ELIF events[i].flag = 1
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
										if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehicle[vehHeist].id)
							
											events[i].active = FALSE
										ENDIF
									ENDIF
								ELSE
									events[i].active = FALSE
								ENDIF
							ENDIF
						BREAK
						
						
						
						case events_blow_up_cars
						
							//	explodeCar(exCar1,iFocusFlag1,iPlaybackTime, 40,55920,65400,65400)
							//	explodeCar(exCar2,iFocusFlag2,iPlaybackTime,51,70000,77900,77900)
						
							/*
							if events[i].flag = 0
								events[i].timer = iStartPlaybackTime
								events[i].flag = 1
							ELIF events[i].flag = 1
								if GET_GAME_TIMER() - events[i].timer > 43920
									SET_FOCUS_ENTITY()
								if GET_GAME_TIMER() - events[i].timer > 63920
									REPEAT COUNT_OF(recordingData) itemp
										if recordingData[itemp].recording = 40
											if IS_VEHICLE_DRIVEABLE(recordingData[itemp].vehicleID)
												if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(recordingData[itemp].vehicleID)
													STOP_PLAYBACK_RECORDED_VEHICLE(recordingData[itemp].vehicleID)
													APPLY_FORCE_TO_ENTITY(recordingData[itemp].vehicleID,APPLY_TYPE_IMPULSE,<< 1,-7,2 >>,<<-1,1,0>>,0,FALSE,true,true)
													//EXPLODE_VEHICLE(recordingData[itemp].vehicleID,TRUE)
													itemp = 500
													events[i].flag = 2
												ENDIF
											ENDIF
										ENDIF
									ENDREPEAT
								ENDIF
							ELIF events[i].flag = 2
								if GET_GAME_TIMER() - events[i].timer > 78034
									REPEAT COUNT_OF(recordingData) itemp
										if recordingData[itemp].recording = 51
											if IS_VEHICLE_DRIVEABLE(recordingData[itemp].vehicleID)
												if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(recordingData[itemp].vehicleID)
													STOP_PLAYBACK_RECORDED_VEHICLE(recordingData[itemp].vehicleID)
													APPLY_FORCE_TO_ENTITY(recordingData[itemp].vehicleID,APPLY_TYPE_IMPULSE,<< -5,-8,2 >>,<<0,0,0>>,0,FALSE,true,true)
													//EXPLODE_VEHICLE(recordingData[itemp].vehicleID,TRUE)
													itemp = 500
													events[i].active = FALSE
												ENDIF
											ENDIF
										ENDIF
									ENDREPEAT
								ENDIF
							ENDIF
							*/
						BREAK
						
						CASE EVENTS_CHOPPER_TRACKS_FRANKLIN
							if events[i].flag = 0
								if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
									FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,FALSE)				
								ENDIF
								
								bDisplayBeam=FALSE
							
								events[i].flag = 1
							ELIF events[i].flag = 1
								if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
									SET_CHOPPER_TO_TARGET(<< -1307.6479, -244.7984, 145.0250 >>,<<0,0,-50.0>>)
								ENDIF
								if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									if IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehicle[vehHeist].id)
										events[i].flag = 2
									ENDIF
								ENDIF
							ELIF events[i].flag = 2
								if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									SET_CHOPPER_TO_TARGET(GET_ENTITY_COORDS(vehicle[vehHeist].id)+<<0,0,120>>,<<0,0,0>>)
								ENDIF
							ENDIF
						break
							
			
						
						CASE EVENTS_CHOPPER_CARPARK
						
							switch events[i].flag
								case cleanup
									clearHoverInArea(hud)
									SET_CHOPPER_HUD_ACTIVE(hud,vehicle[vehChopper].id,false, vehicle[vehChopper].id)
									SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",FALSE)
									ENABLE_SELECTOR()
								break
								case 0
									set_chopper_speed_limits(hud,0,9.0)
									clearHoverInArea(hud)
									events[i].flag++
								break
								case 1
									if GET_TO_POINT(hud,<< -1275.1460, -277.1535, 54 >>,5.0)//<< -1292.4032, -283.9487, 89.8351 >>,30.0)
										set_chopper_speed_limits(hud,0.3,6.0)
										fixChopperHeading(hud,-55.6696)
										events[i].flag=4
									ENDIF
								BREAK
								
								case 2
									IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
										if GET_DISTANCE_BETWEEN_COORDS(<< -1309.6600, -241.7653, 54 >>,GET_ENTITY_COORDS(vehicle[vehChopper].id)) < 7.0
											set_chopper_speed_limits(hud,0.3,1.0)
										ENDIF
										if GET_TO_POINT(hud,<< -1298.6008, -248.6288, 54 >>,4.0)																		
											set_chopper_speed_limits(hud,0.3,6.0)
											events[i].flag=4
										ENDIF
									ENDIF
								BREAK
								
								case 3
									IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
										if GET_DISTANCE_BETWEEN_COORDS(<< -1305.5187, -238.6272, 54 >>,GET_ENTITY_COORDS(vehicle[vehChopper].id)) < 7.0
											set_chopper_speed_limits(hud,0.3,1.0)
										ENDIF
										if GET_TO_POINT(hud,<< -1309.1571, -240.8424, 54 >>,2.0)//<< -1305.5187, -238.6272, 54.0552 >>,2.0)
											set_chopper_speed_limits(hud,0.3,0.7)									
											events[i].flag++
										ENDIF
									ENDIF
								break
								
								case 4
									if GET_DISTANCE_BETWEEN_COORDS(<< -1301.2561, -253.3784, 54 >>,GET_ENTITY_COORDS(vehicle[vehChopper].id)) < 5.0
										set_chopper_speed_limits(hud,0.3,0.7)
									ENDIF
									if GET_TO_POINT(hud,<< -1301.2561, -253.3784, 54 >>,2.0)
										set_chopper_speed_limits(hud,0.3,0.7)
										events[i].flag++
									ENDIF
								BREAK
								
								case 5
								//	if GET_TO_POINT(hud,<< -1309.6600, -241.7653, 53.5238 >>,2.0)
									if GET_TO_POINT(hud,<< -1309.1571, -240.8424, 54 >>,2.0)
										set_chopper_speed_limits(hud,0.3,0.7)
										events[i].flag=4
									ENDIF
								BREAK
															
							ENDSWITCH
						BREAK
						
						case EVENTS_CHOPPER_HOVER
							SET_CHOPPER_TO_TARGET(<< -1310.6067, -240.0043, 57.9243 >>+<<GET_RANDOM_FLOAT_IN_RANGE(-0.5,0.5),GET_RANDOM_FLOAT_IN_RANGE(-0.5,0.5),GET_RANDOM_FLOAT_IN_RANGE(-0.5,0.5)>>,<<0,0,-50>>)
						BREAK
						
						CASE EVENTS_CONTROL_INFRARED
						
							IF infraRed = true
								REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
							ENDIF
						
							switch events[i].flag
								case cleanup								
									infraRed = FALSE
								break
								
								case 0
							//PRINTLN("TEST")
							//FIND_CONTROL_INPUT()
									IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT) 
										
										IF infraRed = true
											SET_INFRARED(FALSE)
											infraRed = FALSE
											PLAY_SOUND_FRONTEND(-1,"Thermal_Off","CAR_STEAL_2_SOUNDSET")
											STOP_AUDIO_SCENE("CAR_2_USE_INFRARED")
											//bDisplayBeam = true
											
										ELSE
											infraRed = true
									
										//	bDisplayBeam = FALSE
										
											SET_INFRARED(TRUE)
											START_AUDIO_SCENE("CAR_2_USE_INFRARED")
											PLAY_SOUND_FRONTEND(-1,"Thermal_On","CAR_STEAL_2_SOUNDSET")
										ENDIF
										events[i].flag++
										events[i].timer = GET_GAME_TIMER() + 1000
									ENDIF
									
									IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS30")
										If infraRed = true
											CLEAR_PRINTS()
										endif
									ENDIF
								break
								case 1
									IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS30")
										If infraRed = true
											CLEAR_PRINTS()
										endif
									ENDIF
									IF GET_GAME_TIMER() > events[i].timer
										events[i].flag = 0
									ENDIF
								break
							ENDSWITCH
						BREAK
						
						
						
						CASE EVENTS_CONVERSATION_ON_ROUTE
							IF events[i].flag = 0
								events[i].timer = get_game_timer() + 6000
								events[i].flag = 1
							ELIF events[i].flag = 1
								If GET_GAME_TIMER() > events[i].timer								
									ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA(CONVTYPE_GAMEPLAY,"CS2_explain",0,ped[ped_trevor].id,"Trevor",2,ped[ped_pilot].id,"ChopperPilot")
									events[i].flag = 2
								ENDIF
							ELIF events[i].flag = 2
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									events[i].active = FALSE
								ENDIF
							ENDIF
						BREAK
						
					
						
						CASE EVENTS_KEEP_CHAD_SAFE										
							IF events[i].flag = 0
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
								//	SET_PED_CAN_BE_TARGETTED(ped[ped_chad].id,FALSE) //1063801
									SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_chad].id,FALSE)
									SET_PED_CAN_BE_SHOT_IN_VEHICLE(ped[ped_chad].id,FALSE)
									SET_PED_SUFFERS_CRITICAL_HITS(ped[ped_chad].id,FALSE)								
									SET_PED_COMBAT_ATTRIBUTES(ped[ped_chad].id,CA_ALWAYS_FLEE,FALSE)
									SET_PED_COMBAT_ATTRIBUTES(ped[ped_chad].id,CA_FLEE_WHILST_IN_VEHICLE,FALSE)								
									SET_PED_COMBAT_ATTRIBUTES(ped[ped_chad].id,CA_USE_VEHICLE,FALSE)
									events[i].flag = 1
								ENDIF
							ELIF events[i].flag = 1
								IF NOT DOES_ENTITY_EXIST(ped[ped_chad].id)
									events[i].flag = 0
								ENDIF
							ELIF events[i].flag = CLEANUP
								IF DOES_ENTITY_EXIST(ped[ped_chad].id)
									SET_PED_CAN_BE_TARGETTED(ped[ped_chad].id,TRUE)
									SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_chad].id,TRUE)
									SET_PED_CAN_BE_SHOT_IN_VEHICLE(ped[ped_chad].id,TRUE)
									SET_PED_SUFFERS_CRITICAL_HITS(ped[ped_chad].id,TRUE)
								ENDIF
							ENDIF
						BREAK
					
						
						CASE EVENTS_REMOVE_CARPARK_ASSETS
							IF DOES_ENTITY_EXIST(ped[ped_chad].id)
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
									IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,PLAYER_PED_ID()) > 100.0
										SET_ped_AS_NO_LONGER_NEEDED(ped[ped_chad].id)
										SET_MODEL_AS_NO_LONGER_NEEDED(model_chad)
										IF DOES_ENTITY_EXIST(vehicle[vehFranklin].id)
											SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[vehFranklin].id)
										ENDIF
										
										REMOVE_ANIM_DICT("misscarsteal2car_stolen")
										REMOVE_ANIM_DICT("misscarsteal2chad_Garage")
										REMOVE_VEHICLE_RECORDING(301,sVehicleRecordingLibrary)
										events[i].active = FALSE
									ENDIF																	
								ENDIF
							ENDIF							
						BREAK
											
						CASE EVENTS_REMOVE_CARPARK_CARS
						//	IF GET_DISTANCE_BETWEEN_COORDS(<< -1291.3961, -215.8462, 38.4461 >>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 150.0
								repeat COUNT_OF(carParkVehicles) iTemp
									SET_VEHICLE_AS_NO_LONGER_NEEDED(carParkVehicles[itemp])
								ENDREPEAT
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_postie].id)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_camper].id)
							//	SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_wrong1].id)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_vanhide].id)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[veh_wrong2].id)
								events[i].active = FALSE
						//	ENDIF						
						BREAK
						
					
						
						CASE EVENTS_SPEED_UP_CHOPPER	
							//SET_CHOPPER_TO_TARGET(<<263.9558, -203.4580, 128.9033>>,<<0,0,0.0>>,5.0)																		
							if events[i].flag = 0
								IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
									IF IS_ENTITY_IN_ANGLED_AREA( vehicle[vehChopper].id, <<198.493317,-177.111450,53.297363>>, <<257.798859,-198.757294,210.079193>>, 115.937500)
										set_chopper_speed_limits(HUD,0,1.5)
									ELSe
										set_chopper_speed_limits(HUD,0,3.5)
									ENDIF
								ENDIF
								
								vector vTemp
								vTemp = GET_ENTITY_COORDS(vehicle[vehChopper].id)
								
								IF vTemp.x < 205
									if GET_TO_POINT(HUD,<< 155.1646, -181.1793, 160.3576 >>,15.0)
										createHoverInArea(HUD,<< 247.38, -186.19, 135 >>,7.0,6.0,FALSE)
										events[i].flag = 1
									ENDIF
								ELSE
									createHoverInArea(HUD,<< 247.38, -186.19, 135 >>,7.0,6.0,FALSE)
									events[i].flag = 1
								ENDIF
							elif events[i].flag = 1
								IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
									IF IS_ENTITY_IN_ANGLED_AREA( vehicle[vehChopper].id, <<198.493317,-177.111450,53.297363>>, <<257.798859,-198.757294,210.079193>>, 115.937500)
										set_chopper_speed_limits(HUD,0,1.5)
									ELSe
										set_chopper_speed_limits(HUD,0,3.5)
									ENDIF
								ENDIF
							ELIF events[i].flag = 2
								events[i].flag = 3
								clearHoverInArea(HUD)
							ELIF events[i].flag = 3
								if not IS_SPHERE_VISIBLE(<< 199.8876, -157.8262, 57.4081 >>,10.0)								
									set_chopper_speed_limits(HUD)
									events[i].active=FALSE
								ENDIF
							ENDIF
						BREAK
						
						CASE EVENTS_PRINT_CHADBUST_MESSAGE
							if events[i].flag = 0
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									TEXT_LABEL_15 sTr
									sTr = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
									if are_strings_equal(sTr,"cs2_seeChad3")
										events[i].flag = 1
									ENDIF
								ENDIF
							ELIF events[i].flag = 1							
								IF not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									PRINT("CH_INS14",6000,1)
									events[i].active = FALSE
								ENDIF
							ENDIF
						BREAK
						
						
						
						CASE EVENTS_FRANKLIN_ARRIVE
							switch events[i].flag
								case 0													
									IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)	
										SET_VEHICLE_LIGHTS(vehicle[vehFranklin].id,FORCE_VEHICLE_LIGHTS_ON)
								
										START_PLAYBACK_RECORDED_VEHICLE(vehicle[vehFranklin].id,103,sVehicleRecordingLibrary)	
										REQUEST_VEHICLE_RECORDING(100,"cs2")
									ENDIF
									events[i].flag++
								break
								case 1
						
									if IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
										if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehFranklin].id)
											events[i].flag++
										ENDIF
									ENDIF
								BREAK
								CASE 2
							
									if IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
										if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehFranklin].id)										
											START_PLAYBACK_RECORDED_VEHICLE(vehicle[vehFranklin].id,100,"cs2")
											PAUSE_PLAYBACK_RECORDED_VEHICLE(vehicle[vehFranklin].id)
											
											events[i].active=FALSE
										ENDIF
									ENDIF
								BREAK							
							ENDSWITCH						
						BREAK
						
						CASE EVENTS_DRIVE_OFF
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								if events[i].flag = 0	
									if not IS_PED_INJURED(ped[ped_chad].id)
										CLEAR_PED_TASKS_IMMEDIATELY(ped[ped_chad].id)
									ENDIF
											
									TASK_EXTEND_ROUTE(<<202.16, -154.30, 56.13>>)
									TASK_EXTEND_ROUTE(<<202.16, -154.30, 56.13>>)
									
									TASK_FOLLOW_POINT_ROUTE(ped[ped_chad].id,pedmove_run,TICKET_SINGLE)
									
									events[i].flag = 1
									events[i].timer = GET_GAME_TIMER()+3000
									
									//iStartPlaybackTime = GET_GAME_TIMER() + 2000
									
									
									
								elif events[i].flag = 1
									IF GET_GAME_TIMER() > events[i].timer
										if not IS_PED_INJURED(ped[ped_chad].id)
											SET_PED_INTO_VEHICLE(ped[ped_chad].id,vehicle[vehHeist].id)
										ENDIF
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_chad].id)
										bplayVehRecs = true
										iPlaybackTime = 0
										//add_event(events_chase_chatter)
									
										add_event(events_blow_up_cars)
										events[i].active = FALSE
												
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
					
						
						CASE EVENTS_REPOSITION_HEIST_CAR						
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								IF not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehHeist].id)								
									SET_VEHICLE_LIGHTS(vehicle[vehHeist].id,FORCE_VEHICLE_LIGHTS_OFF)
									SET_ENTITY_COORDS(vehicle[vehHeist].id,<< -1309.5819, -215.2148, 50.5497 >>)
									SET_ENTITY_HEADING(vehicle[vehHeist].id,122.987)
									events[i].active = FALSE
								ENDIF
							ENDIF
							
							if events[i].flag = CLEANUP
								IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									FREEZE_ENTITY_POSITION(vehicle[vehHeist].id,FALSE)
								ENDIF
							ENDIF
						BREAK
						
						case events_chopper_push
						
							if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
							
								set_chopper_speed_limits(hud,0,GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehicle[vehHeist].id,FALSE)/5.0)
							
								if IS_ENTITY_AT_COORD(vehicle[vehHeist].id,<< 21.5236, -217.2817, 53.0341 >>,<<150,150,400>>)
									if events[i].flag = 0
										events[i].flag = 1
									ENDIF
								ENDIF
								
								if IS_ENTITY_AT_COORD(vehicle[vehHeist].id,<< -269.0054, -103.5025, 47.5659 >>,<<200,200,400>>)											
									if events[i].flag < 4
										events[i].flag = 4
									ENDIF
								ENDIF
								
								if IS_ENTITY_AT_COORD(vehicle[vehHeist].id,<< -1110.0933, -264.0582, 40.8865 >>,<<100,100,400>>)											
									if events[i].flag < 7
										events[i].flag = 7
									ENDIF
								ENDIF
							ENDIF
						
							SWITCH events[i].flag
								case 1
									if IS_SPHERE_VISIBLE(<< 21.5236, -217.2817, 53.0341 >>,50.0)									
										events[i].flag=2
									ENDIF
								break
								case 2
									if GET_TO_POINT(hud,<< 10.4487, -206.1065, -1.0 >>,25)
										events[i].flag=3
									ENDIF
								BREAK
								case 4
									if IS_SPHERE_VISIBLE(<< -269.0054, -103.5025, 47.5659 >>,50.0)
										events[i].flag++
									ENDIF
								break
								case 5
									if GET_TO_POINT(hud,<< -281.4530, -137.3323, 205.2054 >>,15)									
										events[i].flag=6
									ENDIF
								BREAK
								case 7																
									createHoverInArea(hud,<< -1157.7358, -312.9930, 170.3401 >>,10.0)
									events[i].flag++								
								break						
							ENDSWITCH 
						BREAK
						
						CASE EVENTS_DEAL_WITH_PLAYER_CAR	
							If events[i].flag = 0
								//what to do if player turns up in a car / his car / on foot
								RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<1403.3, -2059.55, 52>>, 32.6853,true,VEHICLE_TYPE_CAR)
								
								events[i].flag = 1
							elif events[i].flag = 1
								IF CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN,  << 1394.1700, -2064.9812, 50.9997 >>, 65.3178,true,VEHICLE_TYPE_CAR)									
									events[i].active = FALSE
								ENDIF 
							ENDIF
						
						BREAK	

						case events_change_playback_speed
						fPlaybackSpeed=1.0
						
							if events[i].flag=0
								fPlaybackSpeed = 0.7
								events[i].flag=1
							ELIF events[i].flag=1
								if iPlaybackTime > 15000
									fPlaybackSpeed += (TIMESTEP() * 0.01)
									if fPlaybackSpeed >= 1.25
										fPlaybackSpeed = 1.25
										//events[i].flag=2
										events[i].active = FALSE
									ENDIF
								ENDIF
							ENDIF
							
						BREAK
						
						CASE EVENTS_OPEN_GARAGE
							IF events[i].flag = CLEANUP
								//set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,FALSE,-2)
								SET_GARAGE_DOOR_STATE(GD_RESET)
								CLEAR_FOCUS()
							endif
							SWITCH events[i].flag
								CASE 0
								
									if not IS_PED_INJURED(ped[ped_chad].id)
										SET_FOCUS_ENTITY(ped[ped_chad].id)								
									ENDIF
								/*	fOpenRatio=0
									object_index obja
									//set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,true,0)
									IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<201.400284,-153.364517,57.848885>>,1.0,PROP_GAR_DOOR_05)
										obja = GET_CLOSEST_OBJECT_OF_TYPE(<<201.400284,-153.364517,57.848885>>,1.0,PROP_GAR_DOOR_05)
									//	SET_ENTITY_VISIBLE(obja,false)
										set_entity_coords(obja,<<201.400284,-153.364517,47.848885>>)
									ENDIF
									
									fakeGarageDoor = CREATE_OBJECT_NO_OFFSET(PROP_GAR_DOOR_05,<<201.400284,-153.364517,57.848885>>)
									set_entity_rotation(fakeGarageDoor,<<-1.441772,-0.000000,-20.000166>>)
									*/
									SET_GARAGE_DOOR_STATE(GD_OPEN)
									//set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,false,fOpenRatio)
									events[i].flag = 10
								BREAK
								CASE 1
									//set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,true,fOpenRatio)
									
									events[i].flag = 3
									/*
									fOpenRatio += 0.15*TIMESTEP()							
									if fOpenRatio >= 1.0
										fOpenRatio=1.0
										set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,true,fOpenRatio)																
									ELSE
										//cprintln(debug_trevor3,"Open ratio: ",fOpenRatio)
										set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,true,fOpenRatio)
									ENDIF			
									IF NOT IS_ANY_VEHICLE_NEAR_POINT(<<201.4,-153.4,57.8>>,10.0)
									AND DOES_ENTITY_EXIST(vehicle[vehHeist].id)
										events[i].flag = 2																													
									ENDIF*/
								BREAK
								CASE 2
									fOpenRatio -= 0.15*TIMESTEP()							
									if fOpenRatio <= 0.0
										fOpenRatio=0.0
										set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,false,fOpenRatio)																
										CLEAR_FOCUS()
										events[i].active = FALSE
									ELSE
										set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,FALSE,fOpenRatio)
									ENDIF		
								BREAK
								case 10
									IF missionProgress = STAGE_CHASE_BEGINS
										IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
											IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehHeist].id)
												IF GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehicle[vehHeist].id) = GET_VEHICLE_RECORDING_ID(401, "cs2")
													SET_GARAGE_DOOR_STATE(GD_NO_LONGER_NEEDED)
													CLEAR_FOCUS()
													events[i].active = FALSE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								break
		
							ENDSWITCH
						BREAK
				
						case EVENTS_CHOPPER_AT_START
							SWITCH events[i].flag
								CASE CLEANUP
									if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
										if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehChopper].id)									
											STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[vehChopper].id)
										ENDIF
									ENDIF
								BREAK
								CASE 0
									if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
										IF NOT IS_PED_INJURED(ped[ped_pilot].id)
											SET_ENTITY_VELOCITY(vehicle[vehChopper].id,<<0,15,0>>)
											TASK_HELI_MISSION(ped[ped_pilot].id,vehicle[vehChopper].id,null,null,<<1359.7988, -2074.8599, 74.8290>>,MISSION_GOTO,15,10,-1,0,20)
											events[i].flag++
										ENDIF
									ENDIF
								BREAK							
							ENDSWITCH

						BREAK
						
						CASE EVENTS_CHAD_COWER
							if events[i].flag = 0
								events[i].timer = GET_ENTITY_HEALTH(ped[ped_chad].id)
								events[i].flag = 1
							ELIF events[i].flag = 1
								if not IS_PED_INJURED(ped[ped_chad].id)
									if GET_ENTITY_HEALTH(ped[ped_chad].id) < events[i].timer
										CLEAR_PED_TASKS(ped[ped_chad].id)
										seq()
											TASK_COWER(null,-1)
											TASK_PLAY_ANIM(null,"misscarsteal2CHAD_GARAGE", "chad_parking_garage_handsuploop_chad",2,normal_blend_out,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
										endseq(ped[ped_chad].id)
										events[i].active = FALSE
									ENDIF
									if IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),ped[ped_chad].id)
										if HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped[ped_chad].id,PLAYER_PED_ID())
											CLEAR_PED_TASKS(ped[ped_chad].id)
											TASK_COWER(ped[ped_chad].id,-1)
											events[i].active = FALSE
										ENDIF
									ENDIF
								ELSE
									events[i].active = FALSE
								ENDIF
							ENDIF
						BREAK
						
						CASE EVENTS_FAIL_CHASE
							if events[i].flag = 0
								if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									//PRINTLN(GET_DISTANCE_BETWEEN_ENTITIES(vehicle[vehHeist].id,player_ped_id(),FALSE))
									if GET_DISTANCE_BETWEEN_ENTITIES(vehicle[vehHeist].id,player_ped_id(),FALSE) > 400.0
										//if not IS_ENTITY_ON_SCREEN(vehicle[vehHeist].id)
										//	if TIMERB() > 2000
										if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											Tell_mission_to_fail("CH_F20")	
										endif
												//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("cs2_lostcar",0,ped[ped_Trevor].id,"Trevor")
										//		IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_lostcar",0,ped[ped_Trevor].id,"Trevor")
										//			events[i].flag = 1
										//		ENDIF
										//	ENDIF
										//ELSE
										//	SETTIMERB(0)
										//ENDIF
									//ELSE
									//	SETTIMERB(0)
									ENDIF
								ENDIF
							ELIF events[i].flag = 1
								TEXT_LABEL_23 tl
								tl= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								if ARE_STRINGS_EQUAL(tl,"cs2_lostcar")
									events[i].flag = 2
								ENDIF
							ELIF events[i].flag = 2
								if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									//Mission_Failed("CH_F08")
									Tell_mission_to_fail("CH_F20")
								ENDIF
							ENDIF
							
						BREAK
						
						CASE EVENTS_LOAD_COLLISION_AROUND_CHAD
							
							if not IS_PED_INJURED(ped[ped_chad].id)
								if not IS_PED_IN_ANY_VEHICLE(ped[ped_chad].id)
									SET_ENTITY_LOAD_COLLISION_FLAG(ped[ped_chad].id,true)
									//REQUEST_COLLISION_AT_COORD(GET_ENTITY_COORDS(ped[ped_chad].id))
								ELSE
									SET_ENTITY_LOAD_COLLISION_FLAG(ped[ped_chad].id,FALSE)
									events[i].active=FALSE
								ENDIF
							ENDIF
						break
						
						
						
						case EVENTS_STREAM_ALLEY
							events[i].active=FALSE
							/* //commented out due to memory issues
							if events[i].flag = cleanup
								if STREAMVOL_IS_VALID(sVol)
									//PRINTLN("****************** STREAM OUT ****************************")
									STREAMVOL_DELETE(sVol)
								ENDIF
								events[i].active=FALSE
							ENDIF
							if events[i].flag = 0
								//PRINTLN("****************** STREAM IN ****************************")
								 sVol = STREAMVOL_CREATE_SPHERE(<< -30.5561, -89.9123, 57.0928 >>,10.0,FLAG_MAPDATA)
								 events[i].flag = 1
							ENDIF */
								 
						BREAK
						
						case events_scene_mugging
							if is_event_complete(EVENTS_LOAD_FIRST_LOCATION_PEDS)
								if not is_point_in_screeen_area(<< -29.8705, -86.0999, 57.4479 >>,0,0,1,1)
									events[i].timer += floor(TIMESTEP() * 1000)
									if events[i].timer > 5000
										events[i].flag = CLEANUP
									ENDIF
								ENDIF
							ENDIF
							
							//force_ped_plays_anims(ped_courier)
							//force_ped_plays_anims(ped_mugger)
							

							
							switch events[i].flag
								case CLEANUP
						
									if does_entity_exist(vehicle[vehDeliveryVan].id)
										SET_vehicle_AS_NO_LONGER_NEEDED(vehicle[vehDeliveryVan].id)
									ENDIF
									
									if does_entity_exist(ped[ped_courier].id)
										IF NOT IS_PED_INJURED(ped[ped_courier].id)
											SET_PED_KEEP_TASK(ped[ped_courier].id,TRUE)
											SET_PED_AS_NO_LONGER_NEEDED(ped[ped_courier].id)
										ENDIF
									ENDIF
									
									if does_entity_exist(ped[ped_busDriver].id)
										IF NOT IS_PED_INJURED(ped[ped_busDriver].id)
											SET_PED_KEEP_TASK(ped[ped_busDriver].id,TRUE)
											SET_PED_AS_NO_LONGER_NEEDED(ped[ped_busDriver].id)
										ENDIF
									ENDIF
									
									
								//	if does_entity_exist(ped[ped_mugger].id)
								//		SET_PED_AS_NO_LONGER_NEEDED(ped[ped_mugger].id)
								//	ENDIF
									
									if does_entity_exist(prop[0])
										SET_OBJECT_AS_NO_LONGER_NEEDED(prop[0])
									ENDIF
									
									if does_entity_exist(prop[1])
										SET_OBJECT_AS_NO_LONGER_NEEDED(prop[1])
									ENDIF
									
									if does_entity_exist(prop[2])
										SET_OBJECT_AS_NO_LONGER_NEEDED(prop[2])
									ENDIF
																							
																
								
								
									
									events[i].active=FALSE
								BREAK
								case 0
							
									
																					
									//viewScene_postal.timeLine[0].type = ANIM
									
									events[i].flag=333
								BREAK
								case 333
							
								//	and HAS_MODEL_LOADED(PROP_LD_FIREAXE)									
						
										events[i].flag=334
								//	endif
								break
								case 334
									do_scene(SCENENAME_POSTAL)
								break
								
							ENDSWITCH
						BREAK
						
						CASE EVENTS_SCENE_PERVERT
							if is_event_complete(EVENTS_LOAD_FIRST_LOCATION_PEDS)
								if not is_point_in_screeen_area(<< -13.6117, -34.6633, 74.2783 >>,0,0,1,1)						
									events[i].timer += floor(TIMESTEP() * 1000)
									if events[i].timer > 5000
										events[i].flag = CLEANUP
									ENDIF
								ENDIF
							ENDIF
							
							//force_ped_plays_anims(ped_husband)
							//force_ped_plays_anims(ped_pervert)
							//force_ped_plays_anims(ped_wife)
							
							switch events[i].flag
								case CLEANUP								
									if does_entity_exist(ped[ped_husband].id)
										IF NOT IS_PED_INJURED(ped[ped_husband].id)
											SET_PED_KEEP_TASK(ped[ped_husband].id,TRUE)
											SET_PED_AS_NO_LONGER_NEEDED(ped[ped_husband].id)
										ENDIF
									ENDIF
									
									if does_entity_exist(ped[ped_pervert].id)
										IF NOT IS_PED_INJURED(ped[ped_pervert].id)
											SET_PED_KEEP_TASK(ped[ped_pervert].id,TRUE)
											SET_PED_AS_NO_LONGER_NEEDED(ped[ped_pervert].id)
										ENDIF
									ENDIF
									
									if does_entity_exist(ped[ped_wife].id)
										IF NOT IS_PED_INJURED(ped[ped_wife].id)
											SET_PED_KEEP_TASK(ped[ped_wife].id,TRUE)
											SET_PED_AS_NO_LONGER_NEEDED(ped[ped_wife].id)
										ENDIF
									ENDIF
											
							
						
									
									events[i].active=FALSE
								BREAK
								case 0
							
									events[i].flag++
								BREAK
								case 1
									if HAS_MODEL_LOADED(A_M_Y_BEACH_02)
									and HAS_MODEL_LOADED(S_M_M_JANITOR)
									and HAS_MODEL_LOADED(s_f_y_hooker_01)	
									and HAS_ANIM_DICT_LOADED("misscarsteal2PERVERT")
										events[i].flag++
									endif
								BREAK
								case 2
										do_scene(SCENENAME_ROOF)
									
										
										
										/*
										ped[ped_husband].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_BEACH_02,<< -9.518, -31.102, 68.097 >>,140)
										ped[ped_pervert].id = CREATE_PED(PEDTYPE_MISSION,S_M_M_JANITOR,<< -9.518, -31.102, 68.097 >>,140)
										ped[ped_wife].id = CREATE_PED(PEDTYPE_MISSION,A_F_Y_Yoga_01,<< -9.518, -31.102, 68.097 >>,140)
										
										int ir
										ir = 0
										repeat count_of(PED_COMPONENT) ir
											SET_PED_COMPONENT_VARIATION(ped[ped_wife].id,INT_TO_ENUM(PED_COMPONENT,ir),0,0)
										ENDREPEAT
										SET_PED_COMPONENT_VARIATION(ped[ped_wife].id,INT_TO_ENUM(PED_COMPONENT,0),1,0)
										SET_PED_COMPONENT_VARIATION(ped[ped_wife].id,INT_TO_ENUM(PED_COMPONENT,2),2,0)
										SET_PED_COMPONENT_VARIATION(ped[ped_wife].id,INT_TO_ENUM(PED_COMPONENT,3),0,5)
										SET_PED_COMPONENT_VARIATION(ped[ped_wife].id,INT_TO_ENUM(PED_COMPONENT,4),0,5)
										SET_PED_COMPONENT_VARIATION(ped[ped_wife].id,INT_TO_ENUM(PED_COMPONENT,8),1,0)
										
									
										
									
										remove_helihud_marker(HUD,destMarker[2])
										remove_helihud_marker(HUD,destMarker[3])
										remove_helihud_marker(HUD,destMarker[4])
																
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_wife].id,true,HUD_UNKNOWN,FALSE,true,true,true)	
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_wife].id,"CH_NAME5")	
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_pervert].id,true,HUD_UNKNOWN,FALSE,true,true,true)									
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_pervert].id,"CH_GREG",6)	
										ADD_PED_TO_SCAN_LIST(HUD,ped[ped_husband].id,true,HUD_UNKNOWN,FALSE,true,true,true)			
										ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_husband].id,"CH_NAME3")	//loitering with intent
													
										scenePosition[2] = << -9.518, -31.102, 68.097 >>
										sceneRotation[2] = << -0.000, 0.000, 69.250 >>
										sceneId[2] = CREATE_SYNCHRONIZED_SCENE(scenePosition[2], sceneRotation[2])																									
										
										
										TASK_SYNCHRONIZED_SCENE (ped[ped_husband].id, sceneId[2], "misscarsteal2PERVERT", "pervert_husband", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )								
										TASK_SYNCHRONIZED_SCENE (ped[ped_pervert].id, sceneId[2], "misscarsteal2PERVERT", "pervert_perv", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
										TASK_SYNCHRONIZED_SCENE (ped[ped_wife].id, sceneId[2], "misscarsteal2PERVERT", "pervert_wife", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )

										events[i].flag++
										*/
									
								BREAK
								/*
								case 2
									if is_point_in_screeen_area(<< -13.6117, -34.6633, 74.2783 >>,0.2,0.2,0.8,0.8) and hud.fFOV < 25 
										events[i].flag++
									ELSE
										SET_SYNCHRONIZED_SCENE_PHASE(sceneId[2],0.3)
									endif
								break
								case 3								
									
									if GET_SYNCHRONIZED_SCENE_PHASE(sceneId[2]) = 1.0
										SET_SYNCHRONIZED_SCENE_PHASE(sceneId[2],0.62)
										events[i].flag++
									ENDIF
									
								break
								*/
							ENDSWITCH
						BREAK
						
						case events_scene_chadGirl
							
					//		force_ped_plays_anims(ped_chad)
					//		force_ped_plays_anims(ped_chadGirl)						
					//		play_scene_audio(scene_chadgirl)
							
							switch events[i].flag
								case CLEANUP	
									
									if does_entity_exist(ped[ped_chadGirl].id)
										IF NOT IS_PED_INJURED(ped[ped_chadGirl].id)
											SET_PED_KEEP_TASK(ped[ped_chadGirl].id,true)
										ENDIF
										SET_PED_AS_NO_LONGER_NEEDED(ped[ped_chadGirl].id)
									ENDIF								
																	
									events[i].active=FALSE
								BREAK
								case 0								
									events[i].flag++
								BREAK
								case 1
									if HAS_MODEL_LOADED(model_chad)
									and HAS_MODEL_LOADED(s_f_y_hooker_01)
									and HAS_ANIM_DICT_LOADED("misscarsteal2CHAD_GOODBYE")
										events[i].flag++
									endif
								break
								case 2
									IF DOES_ENTITY_EXIST(ped[ped_chad].id)
										if IS_PED_IN_SCAN_LIST(hud,ped[ped_chad].id)
											events[i].flag++
										endif
									endif
									
									IF do_scene(SCENENAME_CHAD)
										events[i].flag = CLEANUP
									ENDIF
									
								break
								case 3
								
									IF DOES_ENTITY_EXIST(ped[ped_chad].id)
									/*	if HAS_PED_BEEN_SCANNED(hud,ped[ped_chad].id)
										or not IS_PED_IN_SCAN_LIST(hud,ped[ped_chad].id)
												
											int id 
											id = ENUM_TO_INT(SCENENAME_CHAD)
											IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(viewScene[id].syncSceneID[1])
												
												viewScene[id].nextEvent = 24
												viewScene[id].triggerTime = GET_GAME_TIMER() - viewScene[id].timeLine[viewScene[id].nextEvent].triggerTime
												events[i].flag++
											ENDIF*/
											/*
											IF IS_SYNCHRONIZED_SCENE_RUNNING(viewScene[id].syncSceneID[0])
												IF (GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[0]) >= 0.063 AND GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[0]) < 0.201)
												OR (GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[0]) >= 0.296 AND GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[0]) < 0.415)
												OR (GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[0]) >= 0.528 AND GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[0]) < 0.799)
													events[i].flag++													
												ENDIF
											ELIF IS_SYNCHRONIZED_SCENE_RUNNING(viewScene[id].syncSceneID[1]) 
												IF (GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[1]) >= 0.063 AND GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[1]) < 0.201)
												OR (GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[1]) >= 0.296 AND GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[1]) < 0.415)
												OR (GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[1]) >= 0.528 AND GET_SYNCHRONIZED_SCENE_PHASE(viewScene[id].syncSceneID[1]) < 0.799)
													events[i].flag++													
												ENDIF
											ENDIF
											*/
										//endif
										//scene skipping takes place at SCENENAME_CHAD
									endif
								
									IF do_scene(SCENENAME_CHAD)
										events[i].flag = CLEANUP
									ENDIF
								break
								case 4
									IF do_scene(SCENENAME_CHAD)
										events[i].flag = CLEANUP
									ENDIF
								/*
									int idb
									idb  = ENUM_TO_INT(SCENENAME_CHAD) 
									IF IS_SYNCHRONIZED_SCENE_RUNNING(viewScene[idb].syncSceneID[0])
										viewScene[idb].syncSceneID[1] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)
																																
										IF NOT IS_PED_INJURED(ped[ped_chad].id)
										AND NOT IS_PED_INJURED(ped[ped_chadGirl].id)
											TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, viewScene[idb].syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_chad", 0.4, -0.5,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE,0.4)								
											TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, viewScene[idb].syncSceneID[1], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_girl", 0.4, -0.5,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE,0.4)																																																															
										ENDIF
										SET_SYNCHRONIZED_SCENE_PHASE(viewScene[idb].syncSceneID[1] ,0.736)																		
										viewScene[idb].triggerTime = GET_GAME_TIMER() - 47100
										
									ELIF IS_SYNCHRONIZED_SCENE_RUNNING(viewScene[idb].syncSceneID[1]) 
										viewScene[idb].syncSceneID[0] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)
										IF NOT IS_PED_INJURED(ped[ped_chad].id)
										AND NOT IS_PED_INJURED(ped[ped_chadGirl].id)
											TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, viewScene[idb].syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_chad", 0.4, -0.5,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE,0.4)								
											TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, viewScene[idb].syncSceneID[0], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_girl", 0.4, -0.5,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE,0.4)																																																															
										ENDIF
										SET_SYNCHRONIZED_SCENE_PHASE(viewScene[idb].syncSceneID[0] ,0.736)																		
										viewScene[idb].triggerTime = GET_GAME_TIMER() - 47100
									ENDIF
									ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,0,<<59.341049,-72.708755,80.553650>>,3.125)
									do_scene(SCENENAME_CHAD)
									events[i].flag++*/
								break
								
								case 5
									
									IF do_scene(SCENENAME_CHAD)
									
										events[i].flag = CLEANUP
									ENDIF
								break
										/*
										ped[ped_chad].id = CREATE_PED(PEDTYPE_MISSION,model_chad,<< 205.723, -110.766, 67.755 >>,140)
										ped[ped_chadGirl].id = CREATE_PED(PEDTYPE_MISSION,U_F_Y_JEWELASS_01,<< 205.723, -110.766, 67.755 >>,140)
										SET_ENTITY_LOAD_COLLISION_FLAG(ped[ped_chad].id,FALSE)			
										scenePosition[3] = << 205.723, -110.766, 67.755 >>
										sceneRotation[3] = << -0.000, 0.000, -39.430 >>
										sceneId[3] = CREATE_SYNCHRONIZED_SCENE(scenePosition[3], sceneRotation[3])
										add_event(events_witness_chadgirl,STAGE_CHASE_BEGINS)
										
										TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, sceneId[3], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_chad", INSTANT_BLEND_IN, -0.4)								
										TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, sceneId[3], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_girl", INSTANT_BLEND_IN, -0.4)																														

										events[i].flag++
									ENDIF
								BREAK
								
								case 2
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId[3])
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) >= 0.921
											SET_SYNCHRONIZED_SCENE_PHASE(sceneId[3],0.063)
										ENDIF
									ENDIF
									
									if HAS_PED_BEEN_SCANNED(hud,ped[ped_chad].id)
										IF NOT IS_PED_INJURED(ped[ped_chad].id)
											add_event(events_speed_up_chopper,STAGE_CARPARK)
											SET_VEHICLE_DENSITY_MULTIPLIER(0.0)
											add_event(EVENTS_STEALTH_REMOVE_TRAFFIC,STAGE_CARPARK)
										//	sceneId[6] = CREATE_SYNCHRONIZED_SCENE(scenePosition[3], sceneRotation[3]) //blend out with chad
										//	SET_SYNCHRONIZED_SCENE_PHASE(sceneId[6],0.9)
										//	TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, sceneId[6], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_chad", 0.5, SLOW_BLEND_OUT,SYNCED_SCENE_TAG_SYNC_OUT )								
										//	TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, sceneId[6], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_girl", 0.5, SLOW_BLEND_OUT,SYNCED_SCENE_TAG_SYNC_OUT )
											
											
											events[i].flag++
										endif
									endif
								//	if is_point_in_screeen_area(<< 205.723, -110.766, 67.755 >>,0.2,0.2,0.8,0.8) and hud.fFOV < 25 
										//check if sync scene can be skipped forward. if not wait until a ghood time to do so
										
									//	events[i].flag++
								//	ELSE									
									//	SET_SYNCHRONIZED_SCENE_PHASE(sceneId[3],0.0)
								//	endif
								break
								
								case 3
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId[3])
										IF (GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) >= 0.063 AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) < 0.201)
										OR (GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) >= 0.296 AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) < 0.415)
										OR (GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) >= 0.528 AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) < 0.799)
										
											sceneId[7] = CREATE_SYNCHRONIZED_SCENE(<< 205.723, -110.766, 67.755 >>, << -0.000, 0.000, -39.430 >>)
										
											
											
											IF NOT IS_PED_INJURED(ped[ped_chad].id)
											AND NOT IS_PED_INJURED(ped[ped_chadGirl].id)
												TASK_SYNCHRONIZED_SCENE (ped[ped_chad].id, sceneId[7], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_chad", 0.4, -0.5,SYNCED_SCENE_TAG_SYNC_OUT,RBF_NONE)								
												TASK_SYNCHRONIZED_SCENE (ped[ped_chadGirl].id, sceneId[7], "misscarsteal2CHAD_GOODBYE", "chad_goodbye_girl", 0.4, -0.5,SYNCED_SCENE_TAG_SYNC_OUT)																														
											
												ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,0,<<59.341049,-72.708755,80.553650>>,3.125)
												events[i].flag++
											ENDIF
											SET_SYNCHRONIZED_SCENE_PHASE(sceneId[7],0.799)
										ENDIF
									ENDIF																				
								BREAK
								

								
								case 4						
									
									if GET_SYNCHRONIZED_SCENE_PHASE(sceneId[7]) > 0.97
										//if not IS_PED_INJURED(ped[ped_chad].id)
											//CLEAR_PED_TASKS(ped[ped_chad].id)
										//ENDIF
										
										//
										
										
									//	vStartPosition = GET_ANIM_INITIAL_OFFSET_POSITION("misscarsteal2pimpsex","pimpsex_punter",<< 224.589, -117.188, 68.649 >>, << 0.000, 0.000, 66.451 >>, 0.0)
									//	vStartRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("misscarsteal2pimpsex","pimpsex_punter",<< 224.589, -117.188, 68.649 >>, << 0.000, 0.000, 66.451 >>, 0.0)
										
										
									//	nv.m_fSlideToCoordHeading = vStartRotation.z
										seq()
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 224.1202, -119.9946, 68.6437 >>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.5,ENAV_NO_STOPPING) 
										TASK_FOLLOW_WAYPOINT_RECORDING(null,"CS2_01",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
										endseq(ped[ped_chad].id)
									//	TASK_FOLLOW_NAV_MESH_TO_COORD(ped[ped_chad].id,vStartPosition,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.5,ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END,vStartRotation.z)
										
										seq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 195.4088, -96.7522, 66.7088 >>,pedmove_walk)
											TASK_WANDER_STANDARD(null)
										endseq(ped[ped_chadGirl].id)
									//	brainGotoCoord(ped_chad,<< 225.1114, -120.9105, 68.6479 >>,MV_STRLINE)
									//	brainUseWaypoint(ped_chad,"CS2_01")									
										
									//	brainWander(ped_chadGirl)	
										add_event(EVENTS_SCENE_CHADJACK)
										events[i].flag = CLEANUP
										//events[i].flag++
									ENDIF								
								BREAK
								case 5
									if not IS_PED_INJURED(ped[ped_chad].id)
									and not IS_PED_INJURED(ped[ped_bikini].id)
										IF GET_SCRIPT_TASK_STATUS(ped[ped_chad].id,script_TASK_ANY) != PERFORMING_TASK
											scenePosition[3] = << 224.589, -117.188, 68.649 >>
											sceneRotation[3] = << 0.000, 0.000, 66.451 >>
											sceneId[3] = CREATE_SYNCHRONIZED_SCENE(scenePosition[3], sceneRotation[3])
											
											IF NOT IS_PED_INJURED(ped[ped_chad].id)
											AND NOT IS_PED_INJURED(ped[ped_bikini].id)
												502,"cs2"HRONIZED_SCENE(ped[ped_chad].id,sceneId[3],"misscarsteal2pimpsex","pimpsex_punter",0.4,0.4)
												502,"cs2"HRONIZED_SCENE(ped[ped_bikini].id,sceneId[3],"misscarsteal2pimpsex","pimpsex_hooker",0.4,1.0)
											ENDIF
											SET_SYNCHRONIZED_SCENE_PHASE(sceneId[3],0.25)
											events[i].flag++
										ENDIf
									ENDIF
								break
								case 6
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId[3])
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) > 0.4
											TASK_FOLLOW_WAYPOINT_RECORDING(ped[ped_chad].id,"CS2_01",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
											add_event(EVENTS_SCENE_CHADJACK)
											events[i].flag++											
										ENDIF
										
									ENDIF
								break
								case 7
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneId[3])
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId[3]) > 0.45
											SET_EVENTS_FLAG(events_bikini_girl,3)
											events[i].flag = CLEANUP
										ENDIF
									ENDIF
								break*/
							ENDSWITCH
						break
						
						case events_scene_pimp
							
						//	force_ped_plays_anims(ped_pimp)
						//	force_ped_plays_anims(ped_hooker)
						//	force_ped_plays_anims(ped_punter)					
							
							switch events[i].flag
								case CLEANUP		
									IF DOES_ENTITY_EXIST(ped[ped_pimp].id)
										IF NOT IS_PED_INJURED(ped[ped_pimp].id)
											SET_PED_KEEP_TASK(ped[ped_pimp].id,true)
										ENDIF
										SET_PED_AS_NO_LONGER_NEEDED(ped[ped_pimp].id)
									ENDIF
									
									IF DOES_ENTITY_EXIST(ped[ped_punter].id)
										IF NOT IS_PED_INJURED(ped[ped_punter].id)
											SET_PED_KEEP_TASK(ped[ped_punter].id,true)
										ENDIF
										SET_PED_AS_NO_LONGER_NEEDED(ped[ped_punter].id)
									ENDIF
									IF DOES_ENTITY_EXIST(ped[ped_hooker].id)
										IF NOT IS_PED_INJURED(ped[ped_hooker].id)
											SET_PED_KEEP_TASK(ped[ped_hooker].id,true)
										ENDIF
										SET_PED_AS_NO_LONGER_NEEDED(ped[ped_hooker].id)
									ENDIF
									cprintln(debug_Trevor3,"REMOVE PIMP")
									events[i].active=FALSE
								BREAK
								case 0

									if HAS_MODEL_LOADED(S_F_Y_HOOKER_02)
									and HAS_MODEL_LOADED(A_M_M_OG_BOSS_01)
									and HAS_MODEL_LOADED(A_M_Y_BEACH_02)
									and HAS_ANIM_DICT_LOADED("misscarsteal2PIMPSEX")
									/*
										ped[ped_pimp].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_OG_BOSS_01,<< 189.080, -159.296, 55.330 >>,140)
										ped[ped_punter].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_BevHills_02,<< 189.080, -159.296, 55.330 >>,140)
										ped[ped_hooker].id = CREATE_PED(PEDTYPE_MISSION,S_F_Y_HOOKER_01,<< 189.080, -159.296, 55.330 >>,140)
																
										scenePosition[4] = << 189.080, -159.296, 55.330 >>
										sceneRotation[4] = << -0.000, 0.000, -15.390 >>
										sceneId[4] = CREATE_SYNCHRONIZED_SCENE(scenePosition[4], sceneRotation[4])
										
										502,"cs2"HRONIZED_SCENE (ped[ped_pimp].id, sceneId[4], "misscarsteal2PIMPSEX", "pimpsex_pimp", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )								
										TASK_SYNCHRONIZED_SCENE (ped[ped_punter].id, sceneId[4], "misscarsteal2PIMPSEX", "pimpsex_punter", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
										TASK_SYNCHRONIZED_SCENE (ped[ped_hooker].id, sceneId[4], "misscarsteal2PIMPSEX", "pimpsex_hooker", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
										add_event(events_pimp_achievement)
										*/
										events[i].flag=2
									ENDIF
								BREAK
								
								case 2
									do_scene(SCENENAME_PIMP)
										
									/*
									if HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id) 	
										events[i].flag++
									ELSE
										if is_point_in_screeen_area(<< 189.080, -159.296, 55.330 >>,0.2,0.2,0.8,0.8) and hud.fFOV < 25 
											SET_SYNCHRONIZED_SCENE_PHASE(sceneId[4],0.3)
											events[i].flag++
										ELSE									
											SET_SYNCHRONIZED_SCENE_PHASE(sceneId[4],0.0)
										endif
									ENDIF
								break
								case 3										
									if is_event_complete(EVENTS_LOAD_FINAL_LOCATION_PEDS)	
										if not 	is_point_in_screeen_area(<< 189.080, -159.296, 55.330 >>,0,0,1,1) 
											events[i].flag = CLEANUP
											brainWander(ped_hooker)
											brainWander(ped_pimp)
											
											if not IS_PED_INJURED(ped[ped_hooker].id)
												SET_PED_KEEP_TASK(ped[ped_hooker].id,true)
												SET_PED_AS_NO_LONGER_NEEDED(ped[ped_hooker].id)
											ENDIF
											if not IS_PED_INJURED(ped[ped_pimp].id)
												SET_PED_KEEP_TASK(ped[ped_pimp].id,true)
												SET_PED_AS_NO_LONGER_NEEDED(ped[ped_pimp].id)
											ENDIF
											if not IS_PED_INJURED(ped[ped_punter].id)
												DELETE_PED(ped[ped_punter].id)
											ENDIF
										ENDIF
									ENDIF*/
								BREAK														
							ENDSWITCH
						break
						
						case EVENTS_SCENE_CHADJACK
							vector pedStartPosition
							//vector pedStartRotation
							if events[i].flag >= 1
							//	force_ped_plays_anims(ped_chad)
							//	force_ped_plays_anims(ped_franklin)
							ENDIF
							switch events[i].flag
								case CLEANUP
									REMOVE_ANIM_DICT("misscarsteal2CHAD_HOLDUP")
									events[i].active = false
								BREAK
								case 0								
									if not IS_PED_INJURED(ped[ped_chad].id)
										if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ped[ped_chad].id)
											cprintln(debug_trevor3,"First call to door lock state")
											SET_GARAGE_DOOR_STATE(GD_CREATE_AND_LOCK)
											//set_door_lock(PROP_GAR_DOOR_05,<<201.4,-153.4,57.8>>,TRUE,fOpenRatio)
											
											events[i].flag++
										ENDIF
									ENDIF
								BREAK
								case 1
									REQUEST_ANIM_DICT("misscarsteal2CHAD_HOLDUP")
									events[i].flag++
								BREAK
								case 2
									if HAS_ANIM_DICT_LOADED("misscarsteal2CHAD_HOLDUP")
										add_event(events_garage_beep)
										events[i].flag++
									ENDIF
								BREAK
								case 3
									if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ped[ped_chad].id)																		
										if GET_PED_WAYPOINT_PROGRESS(ped[ped_chad].id) >= 31
											events[i].flag++
										ENDIF
									ELSE
										events[i].flag++
									ENDIF
								BREAK
								case 4
									IF NOT IS_PED_INJURED(ped[ped_chad].id)
										IF IS_ENTITY_ON_SCREEN(ped[ped_chad].id)
											IF GET_SCRIPT_TASK_STATUS(ped[ped_chad].id,SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) != PERFORMING_TASK
												TASK_FOLLOW_WAYPOINT_RECORDING(ped[ped_chad].id,"CS2_01",28,EWAYPOINT_START_FROM_CLOSEST_POINT) 
											ENDIF
											events[i].flag++
										ELSE										
											IF GET_SCRIPT_TASK_STATUS(ped[ped_chad].id,SCRIPT_TASK_ANY) = PERFORMING_TASK											
												CLEAR_PED_TASKS_IMMEDIATELY(ped[ped_chad].id)
											ENDIF
										ENDIF
										if not IS_PED_INJURED(ped[ped_chad].id)
											SET_FOCUS_ENTITY(ped[ped_chad].id)								
										ENDIF
									ENDIF
								break
								case 5
									//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("CS2_seeChad3",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor")
									
									
								//	kill_event(events_is_chad_hidden)
									
									pedStartPosition = GET_ANIM_INITIAL_OFFSET_POSITION("misscarsteal2CHAD_HOLDUP","chad_holdup_chad",<<198.93, -152.54, 56.18>> /*<< 199.656, -152.833, 56.174 >>*/, << -0.000, 0.000, -20.500 >>, 0.0)
									//pedStartRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("misscarsteal2CHAD_HOLDUP","chad_holdup_chad",<< 199.656, -152.833, 56.174 >>, << -0.000, 0.000, -20.500 >>, 0.0)
									
									//TASK_FOLLOW_NAV_MESH_TO_COORD(ped[ped_chad].id,pedStartPosition,PEDMOVE_WALK,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, pedStartRotation.z)
									events[i].flag++
								BREAK 
								case 6
									IF NOT IS_PED_INJURED(ped[ped_chad].id)
										pedStartPosition = GET_ANIM_INITIAL_OFFSET_POSITION("misscarsteal2CHAD_HOLDUP","chad_holdup_chad",<<198.93, -152.54, 56.18>> /*<< 199.656, -152.833, 56.174 >>*/, << -0.000, 0.000, -20.500 >>, 0.0)
										

										if IS_ENTITY_AT_COORD(ped[ped_chad].id,pedStartPosition,<<0.3,0.3,1.0>>)
											scene_chad_reacts_to_franklin_jack = CREATE_SYNCHRONIZED_SCENE(<<198.93, -152.54, 56.18>> /*<< 199.656, -152.833, 56.174 >>*/, << -0.000, 0.000, -20.500 >>)
											//TASK_SYNCHRONIZED_SCENE(ped[ped_chad].id, scene_chad_reacts_to_franklin_jack, "misscarsteal2CHAD_HOLDUP", "chad_holdup_chad", WALK_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_NONE,RBF_NONE,WALK_BLEND_IN)																				
											TASK_PLAY_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad",WALK_BLEND_IN,SLOW_BLEND_OUT,-1,AF_TAG_SYNC_IN | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)// | AF_OVERRIDE_PHYSICS) 
											
											events[i].flag++
											events[i].timer = GET_GAME_TIMER() + 2000
										ENDIF
									ENDIF
								break
								
								//events_garage_beep
								case 7
							//		SET_PED_MIN_MOVE_BLEND_RATIO(ped[ped_chad].id,pedmove_walk)
									if GET_GAME_TIMER() > events[i].timer	
										IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)	
											FREEZE_ENTITY_POSITION(vehicle[vehFranklin].id,FALSE)
											IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehFranklin].id)
												STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[vehFranklin].id)
											ENDIF
											SET_VEHICLE_LIGHTS(vehicle[vehFranklin].id,FORCE_VEHICLE_LIGHTS_ON)
											SET_ENTITY_LOD_DIST(vehicle[vehFranklin].id,400)
											
											START_PLAYBACK_RECORDED_VEHICLE(vehicle[vehFranklin].id,103,sVehicleRecordingLibrary)	
											//add_event(EVENTS_FRANKLIN_ARRIVE)
											
											
											events[i].timer = GET_GAME_TIMER() + 500
											events[i].flag++
										ENDIF
									ENDIF								
								BREAK
								
								case 8
									//if GET_GAME_TIMER() > events[i].timer									
										add_event(EVENTS_OPEN_GARAGE)
										SET_CONDITION_STATE(COND_GARAGE_DOOR_OPENING,TRUE)
										events[i].flag++
									//ENDIF
								break
								
								case 9
									if not IS_ENTITY_DEAD(vehicle[vehFranklin].id)
									and not IS_PED_INJURED(ped[ped_chad].id)
										//if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehFranklin].id)	
										
										if IS_SYNCHRONIZED_SCENE_RUNNING(scene_chad_reacts_to_franklin_jack)
											if GET_SYNCHRONIZED_SCENE_PHASE(scene_chad_reacts_to_franklin_jack) >= 0.401
												events[i].flag++
											endif
										else
											if IS_ENTITY_PLAYING_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad")
												
												IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad") > 0.401
													events[i].flag++
												endif
											endif
										endif
									endif
								break
								
								case 10
									if not IS_ENTITY_DEAD(vehicle[vehFranklin].id)
									and not IS_PED_INJURED(ped[ped_chad].id)		
										scene_franklin_tries_to_jack_chad = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vehicle[vehFranklin].id),GET_ENTITY_ROTATION(vehicle[vehFranklin].id))//<< 193.617, -150.932, 55.833 >>, << -0.000, 0.000, -16.500 >>)
										STOP_AUDIO_SCENE("CAR_2_FOLLOW_CHAD_ON_FOOT")	
										START_AUDIO_SCENE("CAR_2_FRANKLIN_ARRIVES")
										
										if not IS_PED_INJURED(ped[ped_franklin].id) and IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
											IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehFranklin].id)
												STOP_PLAYBACK_RECORDED_VEHICLE(vehicle[vehFranklin].id)
											ENDIF
											SET_ENTITY_COORDS_NO_OFFSET(ped[ped_franklin].id,GET_ENTITY_COORDS(vehicle[vehFranklin].id)+<<0,0,2.0>>)
											TASK_SYNCHRONIZED_SCENE (ped[ped_franklin].id, scene_franklin_tries_to_jack_chad, "misscarsteal2CHAD_HOLDUP", "chad_holdup_franklin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
													
											GIVE_WEAPON_TO_PED(ped[ped_franklin].id,WEAPONTYPE_PISTOL,50,true)
													
											PLAY_SYNCHRONIZED_ENTITY_ANIM(vehicle[vehFranklin].id,scene_franklin_tries_to_jack_chad,"chad_holdup_buffalo","misscarsteal2CHAD_HOLDUP",1000.0)
											SET_SYNCHRONIZED_SCENE_PHASE(scene_franklin_tries_to_jack_chad,0.371) //GET_SYNCHRONIZED_SCENE_PHASE(scene_chad_reacts_to_franklin_jack))
										ENDIF
										events[i].flag++

									ENDIF
								break
								case 11
									if IS_SYNCHRONIZED_SCENE_RUNNING(scene_chad_reacts_to_franklin_jack)
										if GET_SYNCHRONIZED_SCENE_PHASE(scene_chad_reacts_to_franklin_jack) >= 0.488
											events[i].flag++
										endif
									else
										IF not IS_PED_INJURED(ped[ped_chad].id)
											if IS_ENTITY_PLAYING_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad")												
												IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad") > 0.504
													events[i].flag++											
												endif
											endif
										endif
									endif
								break
								case 12
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CAR_2_IG_8",3,ped[ped_franklin].id,"Franklin",4,ped[ped_chad].id,"Chad")
										PLAY_MUSIC(mus_chad_sees_franklin,mus_chad_drives_down_alley)
										events[i].flag++
									ENDIF
								BREAK
								case 13
									if not IS_PED_INJURED(ped[ped_chad].id)	
										if IS_SYNCHRONIZED_SCENE_RUNNING(scene_franklin_tries_to_jack_chad)
											if GET_SYNCHRONIZED_SCENE_PHASE(scene_franklin_tries_to_jack_chad) >= 0.855
												events[i].flag++
											endif
										else
											if IS_ENTITY_PLAYING_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad")												
												IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad") > 0.855
													events[i].flag++												
												endif
											endif
										endif
									endif
								BREAK
								CASE 14
									if not IS_PED_INJURED(ped[ped_chad].id)		
								
										initHeistVehicle(<< 202.7272, -149.7968, 56.1760 >>,160)
										SET_PED_INTO_VEHICLE(ped[ped_chad].id,vehicle[vehHeist].id)
										
										IF Get_Fails_Count_Without_Progress_For_This_Mission_Script() < 2	
											REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_chad].id)
										ENDIF
										bplayVehRecs = true
										iPlaybackTime = 0
									//	add_event(events_chase_chatter)										
									//	add_event(events_blow_up_cars)
										
										STOP_AUDIO_SCENE("CAR_2_FRANKLIN_ARRIVES")
										START_AUDIO_SCENE("CAR_2_CAR_CHASE_START")
										START_AUDIO_SCENE("CAR_2_Z_TYPE_ENGINE_BOOST")
										ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehicle[vehHeist].id,"CAR_2_Z-TYPE")
										
										events[i].flag++												
									ENDIF										
								BREAK
								case 15
									if not IS_PED_INJURED(ped[ped_franklin].id)	
										if IS_SYNCHRONIZED_SCENE_RUNNING(scene_franklin_tries_to_jack_chad)
											if GET_SYNCHRONIZED_SCENE_PHASE(scene_franklin_tries_to_jack_chad) >= 1.0
												//STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[vehFranklin].id,1.0,TRUE)
												SET_PED_INTO_VEHICLE(ped[ped_franklin].id,vehicle[vehFranklin].id)																						
												events[i].flag = CLEANUP //this wasn't set to cleanup meaning the dict was never removed.
											endif
										endif
										/*	if IS_ENTITY_PLAYING_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad")												
												IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP", "chad_holdup_chad") >= 1.0
													STOP_SYNCHRONIZED_ENTITY_ANIM(vehicle[vehFranklin].id,1.0,TRUE)
													SET_PED_INTO_VEHICLE(ped[ped_franklin].id,vehicle[vehFranklin].id)																						
													events[i].flag = CLEANUP
												endif
											endif
										endif*/
									endif
									
								BREAK
							ENDSWITCH
						BREAK
						
						
						
						
						
						
										

						
						case events_freeze_car
							if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								if SET_VEHICLE_ON_GROUND_PROPERLY(vehicle[vehHeist].id)
									FREEZE_ENTITY_POSITION(vehicle[vehHeist].id,true)
									events[i].active=FALSE
								ENDIF
							ENDIF									
						BREAK
						
					
						
					
						
						
						
						
						
						case events_show_goto_instructions
							if events[i].flag = CLEANUP
								CLEAR_PRINTS()
							elif events[i].flag = 0
								PRINT("CH_INS42",DEFAULT_GOD_TEXT_TIME,1)
							ENDIF
						BREAK										
						
						CASE EVENTS_REMOVE_SCAN_BOXES
							
							pedScanned = 0
							//pedBeingScanned = 0
							for itemp = ped_courier to ped_dog_walk
								if isalive(itemp)
									IF IS_PED_IN_SCAN_LIST(HUD,ped[itemp].id)
										IF HAS_PED_BEEN_SCANNED(HUD,ped[itemp].id)
											pedScanned = itemp
										ENDIF
									ENDIF
								ENDIF
							ENDFOR	
							/*
							IF pedScanned != 0 and pedBeingScanned != pedScanned and pedBeingScanned != 0
								REMOVE_PED_FROM_SCAN_LIST(HUD,ped[pedScanned].id)
								if scanPedToRemove = pedScanned
									scanPedToRemove = 0
									pedRemoveTimer = 0
								ENDIF
								pedScanned = 0
							ENDIF
							*/
							IF pedScanned != 0 and scanPedToRemove = 0
								pedRemoveTimer = GET_GAME_TIMER() + 3000
								scanPedToRemove = pedScanned
							ENDIF
							
							if pedRemoveTimer != 0
								if GET_GAME_TIMER() > pedRemoveTimer
									IF IS_PED_IN_SCAN_LIST(HUD,ped[scanPedToRemove].id)
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[pedScanned].id)
									ENDIF
									scanPedToRemove = 0
									pedRemoveTimer = 0
								ENDIF
							ENDIF
							
						BREAK
						
						case events_hover_over_first_location	
							if events[i].flag = CLEANUP
								clearHoverInArea(HUD)
							ELSE
								events[i].flag = 0
							
								
								if IS_SPHERE_VISIBLE(<< -13.6185, -34.6559, 74.1984 >>,15.0)
									//if IS_PED_IN_SCAN_LIST(HUD,ped[ped_wife].id) or IS_PED_IN_SCAN_LIST(HUD,ped[ped_husband].id) or IS_PED_IN_SCAN_LIST(HUD,ped[ped_pervert].id)
										events[i].flag = 2								
									//ENDIF
								ENDIF
							
							
								if IS_SPHERE_VISIBLE(<< -31.0612, -87.8128, 57.1910 >>,10.0)
									//if IS_PED_IN_SCAN_LIST(HUD,ped[ped_mugger].id) or IS_PED_IN_SCAN_LIST(HUD,ped[ped_courier].id)																
										events[i].flag = 1								
									//ENDIF
								ENDIF
																				
								if events[i].flag = 0
									if events[i].timer != 0
										clearHoverInArea(HUD)	
										events[i].timer = 0
									ENDIF
								ELIF events[i].flag = 1
									if events[i].timer != 1
										createHoverInArea(hud,<<-0.9629, -152.3349, 180.6798>>,10.0)							
										events[i].timer = 1
									ENDIF
								ELSE
									if events[i].timer != 2
										events[i].timer = 2
										createHoverInArea(hud,<< 5.2097, -122.5330, 172.3606 >>,10.0)
									ENDIF
								ENDIF
							ENDIF

						BREAK
						
						case events_hover_over_second_location	
							if events[i].flag = CLEANUP
								clearHoverInArea(HUD)
							ELSE
								events[i].flag = 0
							
								
								if IS_SPHERE_VISIBLE(<< 205.7967, -110.9667, 69.9306 >>,20.0) or IS_SPHERE_VISIBLE(<< 182.7890, -157.1740, 58.2470 >>,25.0) or IS_SPHERE_VISIBLE(<< 209.1109, -148.8647, 61.6233 >>,25.0)
									//if IS_PED_IN_SCAN_LIST(HUD,ped[ped_wife].id) or IS_PED_IN_SCAN_LIST(HUD,ped[ped_husband].id) or IS_PED_IN_SCAN_LIST(HUD,ped[ped_pervert].id)
										events[i].flag = 1						
									//ENDIF
								ENDIF
							
																				
								if events[i].flag = 0
									if events[i].timer != 0
										clearHoverInArea(HUD)	
										events[i].timer = 0
									ENDIF
								ELIF events[i].flag = 1
									if events[i].timer != 1
										createHoverInArea(hud,<<165.0758, -118.7167,172.5288>>,10.0)							
										events[i].timer = 1
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
					
						
						case events_pimp_achievement
							IF DOES_ENTITY_EXIST(ped[ped_pimp].id)
								if HAS_PED_BEEN_SCANNED(HUD,ped[ped_pimp].id)
									//INFORM_STAT_CAR_STEAL_TWO_PIMP_HAND_SEEN()
									events[i].active = false
								endif
							else
								events[i].active = false
							endif
						break
						
						case events_upside_Down
							IF events[i].flag = 0
								IF DOES_ENTITY_EXIST(vehicle[vehHeist].id)
									IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
										ADD_VEHICLE_UPSIDEDOWN_CHECK(vehicle[vehHeist].id)																		
										events[i].flag = 1
										
									ENDIF
								ENDIF
							ELSE
								IF NOT DOES_ENTITY_EXIST(vehicle[vehHeist].id)							
									events[i].flag = 0
								ENDIF
							ENDIF
						BREAK	
						
						
						case EVENTS_SET_PLAYER_IDS
							IF native_to_int(ped[ped_trevor].id) = 0 OR NOT DOES_ENTITY_EXIST(ped[ped_trevor].id)
								IF GET_PLAYER_PED_MODEL(CHAR_TREVOR) = GET_ENTITY_MODEL(player_ped_id())
									ped[ped_trevor].id = player_ped_id()
								ENDIF
							ENDIF
							IF native_to_int(ped[ped_franklin].id) = 0 OR NOT DOES_ENTITY_EXIST(ped[ped_franklin].id)
								IF GET_PLAYER_PED_MODEL(CHAR_FRANKLIN) = GET_ENTITY_MODEL(player_ped_id())
									ped[ped_franklin].id = player_ped_id()
								ENDIF
							ENDIF
						BREAK
							
						case events_create_franklin_and_car //create car when playing as trevor at start
							IF CREATE_FRANKLIN(<<1390.7333, -2063.3943, 50.9983>>,128)
						
								events[i].active = FALSE
							ENDIF							
						break
							
						
						
						case events_switch_to_franklin
							IF missionProgress != STAGE_CARPARK
							OR switchState = SWITCH_SWAP_PLAYER_PEDS
								IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS39")
									CLEAR_PRINTS()
								ENDIF
								events[i].active = false
							endif
							
							IF events[i].active
								if events[i].flag = 0
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									AND NOT IS_MESSAGE_BEING_DISPLAYED()
										PRINT("CH_INS39",DEFAULT_GOD_TEXT_TIME,1)
										events[i].flag = 1
									ENDIF
								endif
							ENDIF
						break
						
						case events_listen_help
							IF events[i].active
								SWITCH events[i].flag
									CASE 0							
										PRINT_HELP("CH_HLP01")
										events[i].flag = 1
										events[i].timer = get_game_timer() + 20000
									BREAK
									CASe 1
										IF get_game_timer() > events[i].timer
											PRINT_HELP("CH_HLP01")
											events[i].active = FALSE
										ENDIF
										IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RB)
											events[i].active = FALSE
										ENDIF
									BREAK
								ENDSWITCH
								
								if missionProgress >= STAGE_CHASE_BEGINS					
									events[i].active = FALSE
								endif								
							endif
						break
						
				
						
						case EVENTS_QUAD_BLOCKING
							ped_index pClose
							pClose = null
							IF missionProgress = STAGE_APPROACH_SCAN_AREA_ONE
								IF GET_HELIHUD_SELECTED_PED(HUD,pClose)
									IF pClose = ped[ped_mugger].id 
									OR pClose = ped[ped_courier].id 
										If events[i].flag != 3	
											events[i].flag = 1
										ENDIF
									ELSE
										If events[i].flag != 4
											events[i].flag = 2
										endif
									ENDIF														
								ENDIF
								
								switch events[i].flag
									case 1 //add quad blocking for courier
										ADD_BLOCKING_PLANE(0,<<-28.2320, -89.2847, 56.8547>>,<<-27.6147, -88.9500, 56.8797>>,<<-27.5647, -88.8999, 58.6297>>,<<-28.2629, -89.2600, 58.6297>>)
										ADD_BLOCKING_PLANE(1,<<-28.6441, -104.0011, 64.2778>>,<<-10.0603, -110.8698, 64.2778>>,<<-2.0177, -88.4428, 64.2778>>,<<-20.5835, -81.8591, 64.2778>>)
										ADD_BLOCKING_PLANE(2,<<-40.2599, -98.5964, 56.7374>>,<<-34.2282, -82.2685, 56.2530>>,<<-34.3367, -82.2011, 62.7623>>,<<-40.4784, -98.5622, 63.1072>>)
										ADD_BLOCKING_PLANE(3,<<-28.8646, -88.6101, 56.9044>>,<<-28.8646, -88.6101, 58.6794>>,<<-28.5948, -87.9370, 58.6542>>,<<-28.5950, -87.9370, 56.9542>>)
										ADD_BLOCKING_PLANE(4,<<-25.2983, -84.2270, 58.9288>>,<<-28.7960, -87.7316, 58.9290>>,<<-27.4239, -89.1142, 58.9290>>,<<-23.9171, -85.5873, 58.9290>>)
										ADD_BLOCKING_PLANE(5,<<-24.8034, -83.7998, 56.5789>>,<<-28.7721, -87.7740, 56.5792>>,<<-28.7721, -87.7740, 58.9792>>,<<-25.2034, -84.1998, 58.9289>>)
										events[i].flag = 3
									break
									CASE 2
										ADD_BLOCKING_PLANE(0,<<-8.9480, -52.3802, 78.6554>>,<<-1.3360, -55.0471, 80.4048>>,<<1.6459, -46.7435, 80.3990>>,<<-5.9165, -43.9649, 78.6554>>)
										ADD_BLOCKING_PLANE(1,<<-5.5538, -43.9854, 78.2118>>,<<9.5382, -49.4824, 78.2118>>,<<12.9347, -39.8749, 78.2118>>,<<-1.4279, -32.9156, 78.2118>>)
										ADD_BLOCKING_PLANE(2,<<-3.4200, -30.8691, 77.1564>>,<<6.6288, -34.5387, 77.1564>>,<<16.0751, -9.0404, 77.4075>>,<<5.8839, -5.2851, 77.4124>>)
										ADD_BLOCKING_PLANE(3,<<-2.1643, -22.0938, 77.0106>>,<<-5.1205, -30.2703, 77.0106>>,<<-3.4104, -30.8594, 77.1564>>,<<-0.5185, -22.9861, 77.3823>>)
										ADD_BLOCKING_PLANE(4,<<-19.4109, -42.2933, 75.9532>>,<<-13.2283, -25.0136, 75.9532>>,<<-36.1206, -16.5541, 75.9532>>,<<-42.8010, -33.4459, 76.4495>>)
										ADD_BLOCKING_PLANE(5,<<-4.0202, -18.9672, 82.5066>>,<<1.7875, -2.3476, 82.5066>>,<<-3.8792, -0.4543, 82.5066>>,<<-10.4238, -16.7885, 82.5066>>)
										ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,0,<<9.469584,-35.123260,95.209671>>,3.750000)
										ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,1,<<-8.538630,-21.224758,80.733902>>,3.375000)
										ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,2,<<-43.042824,-39.191010,99.565697>>,3.687500)
										events[i].flag = 4
									BREAK
								ENDSWITCH
							ELSE
								IF GET_HELIHUD_SELECTED_PED(HUD,pClose)
									IF pClose = ped[ped_pimp].id 
									OR pClose = ped[ped_punter].id 
									OR pClose = ped[ped_hooker].id 
										If events[i].flag != 6
											events[i].flag = 5
										ENDIF
									ELSE
										IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id)
											If events[i].flag != 10
												events[i].flag = 9
											endif
										ELSE
											If events[i].flag != 8
												events[i].flag = 7
											endif
										ENDIF
									ENDIF														
								ENDIF
								
								switch events[i].flag
									case 5 //add quad blocking for pimp sex
										ADD_BLOCKING_PLANE(0,<<200.6473, -168.1464, 55.3268>>,<<182.4492, -161.5087, 55.3836>>,<<182.4593, -161.5123, 62.1720>>,<<200.6783, -168.1942, 62.1804>>)
										ADD_BLOCKING_PLANE(1,<<181.6328, -163.3893, 62.1529>>,<<181.6413, -163.3530, 55.3168>>,<<175.6814, -161.2399, 55.4279>>,<<181.9644, -163.5368, 62.1804>>)
										ADD_BLOCKING_PLANE(2,<<188.7882, -160.1965, 55.3177>>,<<187.6117, -163.3776, 55.3177>>,<<187.6955, -163.3199, 59.7359>>,<<188.8773, -160.1298, 59.7265>>)
										ADD_BLOCKING_PLANE(3,<<182.0581, -146.8758, 56.5077>>,<<200.6405, -153.1321, 56.5318>>,<<200.1214, -152.4480, 72.1690>>,<<182.3946, -146.4140, 72.3024>>)
										ADD_BLOCKING_PLANE(4,<<182.3946, -146.4140, 72.3024>>,<<200.5840, -152.8908, 72.7951>>,<<206.1236, -137.4284, 72.7951>>,<<186.9452, -130.9248, 72.3515>>)
										ADD_BLOCKING_PLANE(5,<<171.8139, -174.8890, 65.9972>>,<<182.9264, -167.7773, 66.0955>>,<<183.1790, -167.6078, 71.3906>>,<<171.6981, -174.9631, 71.1046>>)
										//ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,0,<<175.778687,-158.134827,62.814491>>,3.5625)
										//ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,1,<<153.480743,-159.753113,80.045670>>,3.8125)
										//ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,2,<<181.433578,-145.485672,76.155899>>,3.8125)
										//ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,3,<<211.149414,-156.867844,73.933311>>,3.8125)

										events[i].flag = 6
										
									break
									CASE 7
									//	ADD_BLOCKING_PLANE(0,<<209.1507, -111.6366, 68.8296>>,<<210.2542, -106.3744, 68.8296>>,<<210.5292, -108.3744, 83.1796>>,<<210.7257, -110.4866, 82.0796>>)
									//	ADD_BLOCKING_PLANE(1,<<207.7449, -100.0619, 82.4365>>,<<215.7615, -102.8439, 80.2823>>,<<218.4723, -94.5033, 80.5969>>,<<210.7405, -91.7269, 82.5138>>)
									//	ADD_BLOCKING_PLANE(2,<<210.7405, -91.7269, 82.5138>>,<<203.0201, -88.8654, 80.5969>>,<<199.8377, -97.1021, 80.5969>>,<<207.7089, -100.0680, 82.5113>>)
									//	ADD_BLOCKING_PLANE(3,<<200.4286, -97.0117, 72.4746>>,<<214.9580, -102.8432, 72.7187>>,<<215.5957, -102.6127, 80.1636>>,<<200.2995, -96.9636, 80.2670>>)
									//	ADD_BLOCKING_PLANE(4,<<214.1890, -114.6139, 68.6245>>,<<213.1701, -114.5938, 76.8409>>,<<217.9960, -102.0663, 76.9621>>,<<219.7616, -99.2012, 68.6237>>)
									//	ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,0,<<208.825806,-118.482643,77.625267>>,3.0625)
									
										events[i].flag = 8
									BREAK
									CASE 9
									//	ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,0,<<208.825806,-118.482643,77.625267>>,1.5625)
							
									//	ADD_HUD_VIEW_SPHERE_BLOCKING(HUD,2,<<212.447754,-143.031982,79.264145>> ,1.525)
										ADD_BLOCKING_PLANE(0,<<216.37, -123.66, 70.79>>,<<210.93, -121.62, 70.76>>,<<204.26, -134.66, 70.78>>,<<211.44, -137.29, 70.78>>)
										ADD_BLOCKING_PLANE(1,<<216.55, -126.74, 69.39>>,<<215.58, -126.21, 70.58>>,<<212.17, -134.51, 66.84>>,<<213.49, -134.99, 66.96>>)										
										events[i].flag = 10
									BREAK
								ENDSWITCH
								
							ENDIF
									
						break
						
						CASE EVENTS_CHAD_DIALOGUE
							switch events[i].flag								
								case 0
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_gotChad",3,ped[ped_chad].id,"Chad")
										//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA("CS2_gotChad",3,ped[ped_chad].id,"Chad",-1,null,"",-1,null,"",CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)			
										events[i].flag++
									ENDIF
								BREAK
								CASE 1
									IF NOT is_event_complete(EVENTS_SCENE_CHAD_EXITS_CAR)
										IF NOT IS_THIS_CONVERSATION_ROOT_PLAYING("CS2_gotChad")
											CLEAR_PRINTS()
											IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_gotChad1",3,ped[ped_chad].id,"Chad")
												events[i].active = FALSE
											ENDIF
										ENDIF
									ENDIF
								BREAK

							ENDSWITCH
							
							IF is_event_complete(EVENTS_SCENE_CHAD_EXITS_CAR)
								events[i].active = FALSE
							ENDIF							
						BREAK
						
						
						CASE EVENTS_ADDITIONAL_ZOOM_HELP	
							switch events[i].flag
								case 0									
									IF NOT HAS_PED_BEEN_SCANNED(HUD,ped[ped_franklin].id)
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()										
											PRINT_HELP("CH_INS4")
											
											events[i].flag = 1
										ENDIF
									ELSE
										events[i].active = FALSE
									ENDIF
								BREAK
								CASE 1
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_franklin].id)
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_INS4")
										
											CLEAR_HELP()
											events[i].active = FALSE
										ELSE
											events[i].active = FALSE
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
						
						CASE events_waypoint_through_police_dept
							switch events[i].flag
								case cleanup
									REMOVE_WAYPOINT_RECORDING("cs2_06")
									//REMOVE_WAYPOINT_RECORDING("cs2_07")
									REMOVE_WAYPOINT_RECORDING("cs2_08")
									REMOVE_WAYPOINT_RECORDING("cs2_09")
								break
								case 0
									REQUEST_WAYPOINT_RECORDING("cs2_06")
									//REQUEST_WAYPOINT_RECORDING("cs2_07")
									REQUEST_WAYPOINT_RECORDING("cs2_08")
									REQUEST_WAYPOINT_RECORDING("cs2_09")
									events[i].flag++
								break
								case 1
									IF GET_IS_WAYPOINT_RECORDING_LOADED("cs2_06")								
									//AND GET_IS_WAYPOINT_RECORDING_LOADED("cs2_07")
									AND GET_IS_WAYPOINT_RECORDING_LOADED("cs2_08")
									AND GET_IS_WAYPOINT_RECORDING_LOADED("cs2_09")
										USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("cs2_06",TRUE,0.5,1.0)
										//USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("cs2_07",TRUE,0.5,1.0)
										USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("cs2_08",TRUE,0.5,1.0)
										USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("cs2_09",TRUE,0.5,1.0)
										events[i].flag++
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
					
						CASE EVENTS_STEALTH_REMOVE_TRAFFIC
							SWITCH events[i].flag
								case cleanup
									SET_ROAD_BLOCKS(0,max_road_blocks,true)
									SET_ROADS_IN_ANGLED_AREA(<<-1314.045654,-183.052002,40.465984>>, <<-1256.618408,-261.595306,68.179886>>, 52.250000,FALSE,FALSE)
								break
								CASE 0									
									SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
									SET_ROADS_IN_ANGLED_AREA(<<-1314.045654,-183.052002,40.465984>>, <<-1256.618408,-261.595306,68.179886>>, 52.250000,FALSE,FALSE)
									SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1284.524292,-212.007370,50.633434>>-<<39.000000,54.750000,14.000000>>,<<-1284.524292,-212.007370,50.633434>>+<<39.000000,54.750000,14.000000>>,false)
									DelCar = GET_CLOSEST_VEHICLE(<< 145.7978, -188.0687, 54.0219 >>,1000,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) 
									iFoundCars = 0
									IF DOES_ENTITY_EXIST(DelCar)
										iFoundCars++
										IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(DelCar,FALSE),10.0)
											CLEAR_AREA(GET_ENTITY_COORDS(delcar,false),10.0,TRUE)			
										ENDIF
									ENDIF
									
									DelCar = GET_CLOSEST_VEHICLE(<< 200.1302, -53.9908, 67.8874 >>,1000,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) 
									IF DOES_ENTITY_EXIST(DelCar)
										iFoundCars++
										IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(DelCar,FALSE),10.0)
											CLEAR_AREA(GET_ENTITY_COORDS(delcar,false),10.0,TRUE)
										ENDIF
									ENDIF
									
									DelCar = GET_CLOSEST_VEHICLE(<< 276.3881, -84.4989, 69.6961 >>,1000,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) 
									IF DOES_ENTITY_EXIST(DelCar)
										iFoundCars++
										IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(DelCar,FALSE),10.0)
											CLEAR_AREA(GET_ENTITY_COORDS(delcar,false),10.0,TRUE)
										ENDIF
									ENDIF
									
									DelCar = GET_CLOSEST_VEHICLE(<< 228.0034, -221.5665, 53.3967 >>,1000,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) 
									IF DOES_ENTITY_EXIST(DelCar)
										iFoundCars++
										IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(DelCar,FALSE),10.0)
											CLEAR_AREA(GET_ENTITY_COORDS(delcar,false),10.0,TRUE)
										ENDIF
									ENDIF
							
									IF iFoundCars = 0
									OR missionProgress = STAGE_CHASE_BEGINS
										set_road_block(0,<<83.446854,-254.758499,44.383682>>, <<157.401337,-45.130379,77.731789>>, 239.630005)
										set_road_block(1,<<-48.491924,-238.547638,34.424400>>, <<7.273878,-90.352585,68.342606>>, 202.437500)
										set_road_block(2,<<-659.643433,-260.605347,24.287605>>, <<-441.786926,-147.829559,51.771347>>, 136.250000)
										set_road_block(3,<<-220.672134,111.042793,30.804962>>, <<-282.125732,-163.065765,105.533096>>, 37.875000)
										set_road_block(4,<<-473.940186,-15.623335,33.462952>>, <<-470.748322,117.273026,77.999863>>, 185.625000)
										set_road_block(5,<<-88.164024,-105.620598,44.246861>>, <<-386.206970,-2.748743,60.812332>>, 37.250000)
										set_road_block(6,<<-461.786530,-60.158344,34.427547>>, <<-288.607391,-106.343079,70.048027>>, 113.750000)
										set_road_block(7,<<-547.271606,-160.380173,22.270775>>, <<-647.416992,32.620464,62.944546>>, 194.750000)
										set_road_block(8,<<-769.867188,-83.208847,21.516926>>, <<-707.259094,-49.819843,61.394394>>, 73.000000)
										set_road_block(9,<<-756.991577,38.100735,30.295971>>, <<-635.053284,68.894981,78.633743>>, 112.312500)
										set_road_block(10,<<-766.287537,-101.407005,21.633743>>, <<-831.088623,4.667232,49.371185>>, 17.750000)
										set_road_block(11,<<-1015.547363,-185.572388,21.749935>>, <<-777.231628,-60.651215,44.710403>>, 83.500000)
										set_road_block(12,<<-809.419739,-311.399017,20.875179>>, <<-1320.396606,-53.602451,55.817036>>, 83.500000)
										set_road_block(13,<<-869.279724,-163.038208,21.764233>>, <<-976.067810,-215.705780,44.715263>>, 13.000000)
										set_road_block(14,<<-979.920105,-193.357071,21.569519>>, <<-1287.616577,-351.481201,48.365097>>, 219.250000)
										SET_ROAD_BLOCKS(0,max_road_blocks,false)
										
										events[i].flag = 1
									ENDIF
								BREAK
								CASE 1							
									IF missionProgress = STAGE_CHASE_BEGINS
										events[i].flag = 2
									ENDIF
								BREAK
								CASE 2
									IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
										int iR
										repeat count_of(sroadblocks) iR
											IF sroadblocks[iR].on
												IF NOT sroadblocks[iR].entered
													IF IS_ENTITY_IN_ANGLED_AREA(vehicle[vehFranklin].id,sroadblocks[iR].v1,sroadblocks[iR].v2,sroadblocks[iR].width)
														sroadblocks[iR].entered = TRUE
													ENDIF
												ELSE
													IF NOT IS_ENTITY_IN_ANGLED_AREA(vehicle[vehFranklin].id,sroadblocks[iR].v1,sroadblocks[iR].v2,sroadblocks[iR].width)
														sroadblocks[iR].entered = TRUE
														sroadblocks[iR].on = false
														SET_ROAD_BLOCKS(iR,iR,true)
													ENDIF
												ENDIF
											endif
										endrepeat
									ENDIF
								BREAK
							ENDSWITCH
							
						BREAK
					
						CASE events_create_chopper_pilot
							SWITCH events[i].flag
								case 0
									REQUEST_MODEL(model_pilot)
									events[i].flag++
								break
								case 1
									IF HAS_MODEL_LOADED(model_pilot)
										CREATE_CHOPPER_PILOT()
										events[i].active = FALSE
									ENDIF
								BREAK
							ENDSWITCH					
						BREAK
						
						
						
						CASE events_bum_on_chad_walk
							
							SWITCH events[i].flag
								CASE CLEANUP
									SET_PED_AS_NO_LONGER_NEEDED(ped[ped_bum].id)
									cprintln(debug_Trevor3,"REMOVE bum")
									//cprintln(debug_trevor3,"BUM RELEASED")
								BREAK
								case 0
									REQUEST_ANIM_DICT("misscarsteal2_bum")
									
									ADD_CHOPPER_LISTENING_LOCATION(4,<< 209.650, -149.125, 60.612 >>)
									events[i].flag++
								BREAK
								CASE 1
									IF HAS_ANIM_DICT_LOADED("misscarsteal2_bum")							
									AND HAS_MODEL_LOADED(A_M_M_OG_BOSS_01)
										ped[ped_bum].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_OG_BOSS_01,<< 209.650, -149.125, 60.612 >>)
										ped[ped_bum].intA = CREATE_SYNCHRONIZED_SCENE(<< 209.650, -149.125, 60.612 >>, << 0.000, 0.000, -103.680 >>)
										
										SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_bum].id)
										SET_PED_COMPONENT_VARIATION(ped[ped_bum].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
										SET_PED_COMPONENT_VARIATION(ped[ped_bum].id, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
										SET_PED_COMPONENT_VARIATION(ped[ped_bum].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
										SET_PED_COMPONENT_VARIATION(ped[ped_bum].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
										SET_PED_PROP_INDEX(ped[ped_bum].id, INT_TO_ENUM(PED_PROP_POSITION,0), 1, 0)

										
										TASK_SYNCHRONIZED_SCENE (ped[ped_bum].id, ped[ped_bum].intA, "misscarsteal2_bum", "base", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
										SET_SYNCHRONIZED_SCENE_LOOPED(ped[ped_bum].intA,TRUE)
										events[i].flag++
										bumRestartLine = "START"
									ENDIF
								BREAK
								CASE 2
									IF NOT IS_PED_INJURED(ped[ped_bum].id)
									AND NOT IS_PED_INJURED(ped[ped_chad].id) 
										IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,ped[ped_bum].id) < 6.0
											TASK_LOOK_AT_ENTITY(ped[ped_chad].id,ped[ped_bum].id,4000,SLF_DEFAULT)
											events[i].flag++
										ELSE
											IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,ped[ped_bum].id) > 20.0
												IF listenDialogue = 4
													IF GET_GAME_TIMER() > events[i].timer
														IF NOT IS_CONV_ROOT_PLAYING("LisBum")
															IF NOT are_strings_equal(bumRestartLine,"NULL") 
																IF are_strings_equal(bumRestartLine,"START")
																	IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisBum",8,ped[ped_bum].id,"cs2_bum")																
																	ENDIF													
																ELSE
																	IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(CONVTYPE_CHOPPER_CAM,"LisBum",bumRestartLine,8,ped[ped_bum].id,"cs2_bum")															
																	ENDIF
																ENDIF
															ENDIF
														ELSE
															bumRestartLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
														ENDIF
													ENDIF
												ELSE
													IF IS_CONV_ROOT_PLAYING("LisBum")
														IF currentConvType = CONVTYPE_CHOPPER_CAM
														cprintln(debug_trevor3,"kill conv J ")
															KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
														ENDIF
														events[i].timer = GET_GAME_TIMER() + 2000
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 3
									IF NOT IS_PED_INJURED(ped[ped_bum].id)
										IF listenDialogue = 4
										AND GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,ped[ped_bum].id) < 6.0	
											events[i].flag++											
										ELSE
											IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,ped[ped_bum].id) < 3.0
												events[i].flag=5
											ENDIF
										ENDIF		
									ENDIF
								BREAK
								case 4
									IF listenDialogue = 4
										IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisChadBum",3,ped[ped_chad].id,"CHAD",8,ped[ped_bum].id,"cs2_bum")
											events[i].flag++
										ENDIF
									ENDIF
								break
								CASE 5
									IF listenDialogue != 4
									AND IS_CONV_ROOT_PLAYING("LisChadBum")
										IF currentConvType = CONVTYPE_CHOPPER_CAM
											KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)										
										ENDIF
									ENDIF
							
									IF NOT IS_PED_INJURED(ped[ped_chad].id)
									AND NOT IS_PED_INJURED(ped[ped_bum].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,ped[ped_bum].id) < 4.0
											events[i].timer = CREATE_SYNCHRONIZED_SCENE(<< 209.650, -149.125, 60.612 >>, << 0.000, 0.000, -103.680 >>)											
											TASK_SYNCHRONIZED_SCENE (ped[ped_bum].id, events[i].timer, "misscarsteal2_bum", "exit",NORMAL_BLEND_IN, NORMAL_BLEND_OUT )											
											events[i].flag++
										ENDIF
									ENDIF
								BREAK
								CASE 6
									IF NOT IS_PED_INJURED(ped[ped_bum].id)
										IF listenDialogue != 4
										AND IS_CONV_ROOT_PLAYING("LisChadBum")
											IF currentConvType = CONVTYPE_CHOPPER_CAM
												KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)										
											ENDIF
										ENDIF
										
										IF GET_SYNCHRONIZED_SCENE_PHASE(events[i].timer) = 1.0
											SET_ENTITY_LOAD_COLLISION_FLAG(ped[ped_bum].id,TRUE)
											TASK_WANDER_STANDARD(ped[ped_bum].id)
											SET_PED_KEEP_TASK(ped[ped_bum].id,TRUE)
											SET_PED_AS_NO_LONGER_NEEDED(ped[ped_bum].id)
											REMOVE_ANIM_DICT("misscarsteal2_bum")											
//											events[i].active = FALSE //removed to allow event to clean up
										ENDIf
										events[i].flag++
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
						
						CASE events_dog_walks_by
							IF events[i].flag < 10
							and events[i].flag > 1
								IF IS_PED_IN_SCAN_LIST(HUD,ped[ped_chad].id)
									IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id)
										events[i].flag = 10
									ENDIF
								ENDIF
							ENDIF
							
							
							SWITCH events[i].flag
								CASE CLEANUP
									cprintln(debug_Trevor3,"REMOVE DOG WALKS")
									SET_PED_AS_NO_LONGER_NEEDED(ped[ped_dog_walk].id)
									SET_PED_AS_NO_LONGER_NEEDED(ped[ped_dog].id)									
								BREAK
								case 0														
									events[i].flag++
								BREAK
								CASE 1
									IF HAS_MODEL_LOADED(A_C_ROTTWEILER)
									AND HAS_MODEL_LOADED(A_M_Y_BEACH_02)
									
										ped[ped_dog_walk].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_BEACH_02,<< 228.4758, -115.5210, 68.8577 >>,83.9800)										
										SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_dog_walk].id)
										SET_PED_COMPONENT_VARIATION(ped[ped_dog_walk].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
										SET_PED_COMPONENT_VARIATION(ped[ped_dog_walk].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
										SET_PED_COMPONENT_VARIATION(ped[ped_dog_walk].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
										SET_PED_COMPONENT_VARIATION(ped[ped_dog_walk].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
										SET_PED_PROP_INDEX(ped[ped_dog_walk].id, INT_TO_ENUM(PED_PROP_POSITION,0), 1, 2)
										TASK_START_SCENARIO_IN_PLACE(ped[ped_dog_walk].id,"WORLD_HUMAN_STAND_MOBILE_UPRIGHT")
									//	TASK_USE_MOBILE_PHONE(ped[ped_dog_walk].id,TRUE)
										
									
										ped[ped_dog].id = CREATE_PED(PEDTYPE_MISSION,A_C_ROTTWEILER,<< 225.5398, -114.8174, 68.7156 >>,253.0000)										
										SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_dog].id)
										seq()
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_DOG_SITTING_RETRIEVER",10000)
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<222.7919, -102.9722, 68.7838>>,PEDMOVE_RUN)
											TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_dog_walk].id,2000)
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_DOG_SITTING_RETRIEVER",5000)
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 225.5398, -114.8174, 68.7156 >>,PEDMOVE_RUN)
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_DOG_SITTING_RETRIEVER",10000)
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<224.4595, -108.3759, 68.8405>>,PEDMOVE_RUN)
											TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_dog_walk].id,2000)
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_DOG_SITTING_RETRIEVER",5000)
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 225.5398, -114.8174, 68.7156 >>,PEDMOVE_RUN)
										endseq(ped[ped_dog].id,TRUE)
										
																				
										ADD_CHOPPER_LISTENING_LOCATION(5,<< 228.4758, -115.5210, 68.8577 >>)
										events[i].flag++
										events[i].timer = GET_GAME_TIMER() + 10000
									ENDIF
								BREAK
								CASE 2
									IF NOT IS_PED_INJURED(ped[ped_dog_walk].id)
										IF listenDialogue = 5											
											IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisDogMan",8,ped[ped_dog_walk].id,"cs2_dogMan")
												events[i].flag++
											ENDIF
										ENDIF
									ENDIF
								BREAK
								CASE 3
									IF IS_CONV_ROOT_PLAYING("LisDogMan")
										IF listenDialogue != 5
											IF currentConvType = CONVTYPE_CHOPPER_CAM
												restartLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
												restartRoot = "LisDogMan"
												cprintln(debug_trevor3,"kill conv K ")
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												events[i].timer = GET_GAME_TIMER() + 2500
												events[i].flag++
											ENDIF
										ENDIF
									ELSE
										CLEAR_PED_TASKS(ped[ped_dog_walk].id)
										//TASK_USE_MOBILE_PHONE(ped[ped_dog_walk].id,FALSE)
										events[i].flag=5
									ENDIF
								BREAK
								CASE 4
									IF NOT IS_PED_INJURED(ped[ped_dog_walk].id)
										IF listenDialogue = 5
											IF GET_GAME_TIMER() > events[i].timer
												IF NOT ARE_STRINGS_EQUAL(restartLine,"NULL")
													IF ARE_STRINGS_EQUAL(restartRoot,"LisDogMan")
														IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(CONVTYPE_CHOPPER_CAM,"LisDogMan",restartLine,8,ped[ped_dog_walk].id,"cs2_dogMan")
															events[i].flag=3
														ENDIF
													ENDIF
												ELSE
													TASK_USE_MOBILE_PHONE(ped[ped_dog_walk].id,FALSE)
													events[i].flag=5
												ENDIF
											ENDIF
										ELSE
											TASK_USE_MOBILE_PHONE(ped[ped_dog_walk].id,FALSE)
											events[i].flag=5
										ENDIF
									ENDIF
								BREAK
								CASE 5
									IF NOT IS_PED_INJURED(ped[ped_dog_walk].id)
										IF GET_SCRIPT_TASK_STATUS(ped[ped_dog_walk].id,SCRIPT_TASK_USE_MOBILE_PHONE) = FINISHED_TASK
											TASK_START_SCENARIO_IN_PLACE(ped[ped_dog_walk].id,"WORLD_HUMAN_STAND_MOBILE")
											events[i].flag=6
										ENDIF
									ENDIF
								BREAK
								
								//chad walks by
								case 10
									IF NOT IS_PED_INJURED(ped[ped_chad].id)
										IF IS_ENTITY_IN_ANGLED_AREA( ped[ped_chad].id, <<211.113678,-114.131859,64.811371>>, <<224.620483,-119.230583,73.213097>>, 6.875000)
											IF NOT IS_PED_INJURED(ped[ped_dog].id)
											//	TASK_GO_TO_ENTITY(ped[ped_dog].id,ped[ped_chad].id)
												TASK_FOLLOW_TO_OFFSET_OF_ENTITY(ped[ped_dog].id,ped[ped_chad].id,<<-3.0,5.0,0>>,pedmove_run,-1,0.1,true)												
												events[i].flag++
											ENDIF
										ENDIF	
									ENDIF
								break
								
								case 11
									IF NOT IS_PED_INJURED(ped[ped_dog].id)
									AND NOT IS_PED_INJURED(ped[ped_chad].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_dog].id,ped[ped_chad].id) < 5.0
											ADD_CHOPPER_LISTENING_LOCATION(5,GET_ENTITY_COORDS(ped[ped_chad].id))
											REMOVE_CHOPPER_LISTENING_LOCATION(3)
											SET_ACTION_FLAG(2,ACT_DOG_BARKS,2)
											
											seq()
											TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_chad].id,1500)
											TASK_PLAY_ANIM(null,"CREATURES@ROTTWEILER@AMB@WORLD_DOG_BARKING@idle_a","idle_a",normal_blend_in,normal_blend_out,3200)
											TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_chad].id,1500)
											TASK_PLAY_ANIM(null,"CREATURES@ROTTWEILER@AMB@WORLD_DOG_BARKING@idle_a","idle_a",normal_blend_in,normal_blend_out,3500)
											endseq(ped[ped_dog].id)
											events[i].timer = GET_GAME_TIMER()
											events[i].flag++
										ENDIF
									ENDIF
								break
								
								case 12							
									IF NOT IS_PED_INJURED(ped[ped_dog].id)
									AND NOT IS_PED_INJURED(ped[ped_chad].id)
										IF GET_GAME_TIMER() - events[i].timer < 1000
											IF listenDialogue = 5
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisChadDog","LisChadDog_1",8,ped[ped_dog_walk].id,"cs2_dogMan",3,ped[ped_chad].id,"chad")
													TASK_LOOK_AT_ENTITY(ped[ped_chad].id,ped[ped_dog].id,5000)
													events[i].flag++
												ENDIF																							
											ENDIF
										ELSE
											TASK_LOOK_AT_ENTITY(ped[ped_chad].id,ped[ped_dog].id,5000)
											events[i].flag++
										ENDIF		
									ENDIF
								BREAK
								
								case 13		
									IF NOT IS_PED_INJURED(ped[ped_dog].id)
									AND NOT IS_PED_INJURED(ped[ped_chad].id)
									AND NOT IS_PED_INJURED(ped[ped_dog_walk].id)
										IF GET_GAME_TIMER() - events[i].timer > 4000
											IF GET_GAME_TIMER() - events[i].timer < 5000
												IF listenDialogue = 5
													IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisChadDog","LisChadDog_2",8,ped[ped_dog_walk].id,"cs2_dogMan",3,ped[ped_chad].id,"chad")
														TASK_TURN_PED_TO_FACE_ENTITY(ped[ped_chad].id,ped[ped_dog].id,5000)
													//	CLEAR_PED_TASKS(ped[ped_dog_walk].id)
														
													//	TASK_TURN_PED_TO_FACE_ENTITY(ped[ped_dog_walk].id,ped[ped_dog].id,15000)														
														
														events[i].flag++
													ENDIF																							
												ENDIF									
											ELSE			
												TASK_TURN_PED_TO_FACE_ENTITY(ped[ped_chad].id,ped[ped_dog].id,5000)
												//CLEAR_PED_TASKS(ped[ped_dog_walk].id)												
												//TASK_TURN_PED_TO_FACE_ENTITY(ped[ped_dog_walk].id,ped[ped_dog].id,5000)												
												events[i].flag++
											ENDIf
										ENDIF	
									ENDIF
								BREAK
								
								case 14							
									IF GET_GAME_TIMER() - events[i].timer > 7000
										IF GET_GAME_TIMER() - events[i].timer < 8000
											IF listenDialogue = 5
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisChadDog","LisChadDog_3",8,ped[ped_dog_walk].id,"cs2_dogMan",3,ped[ped_chad].id,"chad")
													events[i].flag++
												ENDIF																							
											ENDIF
										ELSE
											events[i].flag++
										ENDIF
									ENDIF									
								BREAK
								
								case 15			
									IF NOT IS_PED_INJURED(ped[ped_dog].id)
									AND NOT IS_PED_INJURED(ped[ped_dog_walk].id)
										
										seq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<232.5028, -103.9169, 69.0993>>,pedmove_walk,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_NO_STOPPING) 
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<232.1583, -81.7946, 68.6269>>,pedmove_walk,DEFAULT_TIME_NEVER_WARP)
											TASK_WANDER_STANDARD(null)
										endseq(ped[ped_dog_walk].id)
										//SET_PED_KEEP_TASK(ped[ped_dog].id,TRUE)
										//SET_PED_KEEP_TASK(ped[ped_dog_walk].id,TRUE)
										//SET_PED_AS_NO_LONGER_NEEDED(ped[ped_dog].id)
										//SET_PED_AS_NO_LONGER_NEEDED(ped[ped_dog_walk].id)
										events[i].flag++
									ENDIF
								BREAK
								
								CASE 16			
									IF NOT IS_PED_INJURED(ped[ped_chad].id)
										IF GET_GAME_TIMER() - events[i].timer > 10000
											IF GET_GAME_TIMER() - events[i].timer < 11000
												IF listenDialogue = 5
													IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisChadDog","LisChadDog_4",8,ped[ped_dog_walk].id,"cs2_dogMan",3,ped[ped_chad].id,"chad")
														seq()
														TASK_PAUSE(null,250)
														IF IS_ENTITY_IN_ANGLED_AREA( ped[ped_chad].id, <<228.998688,-119.637054,73.218834>>, <<205.348007,-111.020775,67.255005>>, 6.062500)
															TASK_FOLLOW_WAYPOINT_RECORDING(null,"CS2_01",0,EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
														ELSE
															TASK_FOLLOW_WAYPOINT_RECORDING(null,"CS2_01",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
														ENDIF
														endseq(ped[ped_chad].id)
														
														seq()
															TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-2197.3071, -2648.1526, -59.3432>>,pedmove_run,DEFAULT_TIME_NEVER_WARP)
															TASK_FOLLOW_TO_OFFSET_OF_ENTITY(null,ped[ped_dog_walk].id,<<3,1,0>>,PEDMOVE_RUN)
															//TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_dog_walk].id)
															//TASK_START_SCENARIO_IN_PLACE(null,"WORLD_DOG_SITTING_RETRIEVER",5000)									
														endseq(ped[ped_dog].id)
														events[i].flag++
													ENDIF																							
												ENDIF
											ELSE
												IF IS_ENTITY_IN_ANGLED_AREA( ped[ped_chad].id, <<228.998688,-119.637054,73.218834>>, <<205.348007,-111.020775,67.255005>>, 6.062500)
													TASK_FOLLOW_WAYPOINT_RECORDING(ped[ped_chad].id,"CS2_01",0,EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
												ELSE
													TASK_FOLLOW_WAYPOINT_RECORDING(ped[ped_chad].id,"CS2_01",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
												ENDIF
												
												seq()
													TASK_FOLLOW_TO_OFFSET_OF_ENTITY(null,ped[ped_dog_walk].id,<<3,1,0>>,PEDMOVE_RUN)
													//TASK_TURN_PED_TO_FACE_ENTITY(null,ped[ped_dog_walk].id)
													//TASK_START_SCENARIO_IN_PLACE(null,"WORLD_DOG_SITTING_RETRIEVER",5000)									
												endseq(ped[ped_dog].id)
												events[i].flag++
											ENDIF																		
										ENDIF	
									ENDIF
								BREAK
								
								case 17					
									IF GET_GAME_TIMER() - events[i].timer > 13000
										IF GET_GAME_TIMER() - events[i].timer < 14000
											IF listenDialogue = 5
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisChadDog","LisChadDog_5",8,ped[ped_dog_walk].id,"cs2_dogMan",3,ped[ped_chad].id,"chad")
													events[i].flag++
												ENDIF																							
											ENDIF										
										ELSE
											events[i].flag++
										ENDIF									
									ENDIF
								BREAK
								
								
							
									/*
									IF NOT IS_PED_INJURED(ped[ped_dog_walk].id)
									AND NOT IS_PED_INJURED(ped[ped_chad].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,ped[ped_dog_walk].id) < 6.0
											TASK_LOOK_AT_ENTITY(ped[ped_chad].id,ped[ped_dog_walk].id,4000,SLF_DEFAULT)
											events[i].flag++
										ENDIF
									ENDIF
								
								CASE 3
									IF NOT IS_PED_INJURED(ped[ped_dog_walk].id)
										IF listenDialogue = 5
										AND GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_chad].id,ped[ped_dog_walk].id) < 5.0	
											IF CREATE_CONVERSATION_EXTRA("PROST",4,ped[ped_chad].id,"CHAD",5,ped[ped_dog_walk].id,"cs2_whoreB")
												events[i].flag++
											ENDIF
										ELSE
											IF missionProgress = STAGE_CHASE_BEGINS
												events[i].flag++
											ENDIF
										ENDIF		
									ENDIF
								BREAK
								CASE 4
									IF listenDialogue != 5
									AND IS_CONV_ROOT_PLAYING("BUM")
										KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)										
									ENDIF
									
									IF missionProgress = STAGE_CHASE_BEGINS
										IF NOT IS_PED_INJURED(ped[ped_dog_walk].id)
											SET_PED_AS_NO_LONGER_NEEDED(ped[ped_dog_walk].id)
										ENDIF
										IF NOT IS_PED_INJURED(ped[ped_dog].id)
											SET_PED_AS_NO_LONGER_NEEDED(ped[ped_dog].id)
										ENDIF	
										events[i].active = FALSE
									ENDIF
								BREAK*/
							ENDSWITCH
						BREAK
						
						CASE events_garage_beep
							SWITCH events[i].flag
								CASE 0
									ADD_CHOPPER_LISTENING_LOCATION(6,<<201.4053, -154.2533, 57.8284>>)								
									events[i].flag++
								BREAK
								CASE 1						
									IF listenDialogue = 6
										IF not IS_PED_INJURED(ped[ped_chad].id)
											IF IS_ENTITY_PLAYING_ANIM(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP","chad_holdup_chad")
												IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP","chad_holdup_chad") > 0.103
													IF GET_ENTITY_ANIM_CURRENT_TIME(ped[ped_chad].id,"misscarsteal2CHAD_HOLDUP","chad_holdup_chad") < 0.112
														
														int tempSound
														tempSound = GET_SOUND_ID()
														PLAY_SOUND(tempSound,"Garage_Open","CAR_STEAL_2_SOUNDSET")
														//PLAY_SOUND_FROM_COORD(tempSound,"Garage_Open",<<201.4053, -154.2533, 57.8284>>,"CAR_STEAL_2_SOUNDSET")
														
														events[i].active = false
													ELSE
														events[i].active = false
													ENDIF
												ENDIF									
											ENDIF
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH							
						BREAK
						
				
						
						CASE events_scan_music_change
							IF HUD.lastScannedPed != null
							AND HUD.lastScannedPed != ped[ped_franklin].id
								PLAY_MUSIC(mus_scan_first_ped,mus_chad_sees_franklin)
								events[i].active = FALSE
							ENDIF
						break
						
						CASE events_chad_car_brief_hint
							SWITCH events[i].flag
								case 0
									ADD_PED_TO_SCAN_LIST(HUD,ped[ped_chad].id,true,HUD_FOE,true,true,true,true)
									ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_chad].id,"CH_CHAD",8)
									events[i].timer = get_Game_timer() + 2000
									events[i].flag++
								BREAK
								CASE 1
									IF get_Game_timer() > events[i].timer
										REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_chad].id)
									endif	
								BREAK
							ENDSWITCH
						BREAK
						
						CASE EVENTS_MISSION_FAILS
							IF NOT bPauseFailDuringSkip
								IF missionProgress = STAGE_FRANKLIN_AWAITS_TREVOR
									if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< 1372.8740, -2071.8774, 51.1031 >>) > 130.0
										Tell_mission_to_fail("CH_F10")
									endif
								endif
								
								IF missionProgress < STAGE_FINAL_CUT //prevent fail for pilot dying
									IF DOES_ENTITY_EXIST(vehicle[vehChopper].id)
										IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
											Tell_mission_to_fail("CH_F01")
										ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(ped[ped_trevor].id)
										IF IS_PED_INJURED(ped[ped_trevor].id)
											Tell_mission_to_fail("CH_F02")
										ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(vehicle[vehHeist].id)
										IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
											Tell_mission_to_fail("CH_F04")
										ELSE
										/*	IF GET_EVENTS_FLAG(events_upside_Down) = 1									
												IF IS_VEHICLE_STUCK_ON_ROOF(vehicle[vehHeist].id)
													Tell_mission_to_fail("CH_F12")									
												ENDIF
											ENDIF*/
										ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(ped[ped_pilot].id)
										IF IS_PED_INJURED(ped[ped_pilot].id)
											Tell_mission_to_fail("CH_F05")
										ENDIF
									ENDIF
									
									if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
										if missionProgress < STAGE_LEARN_TO_SCAN
											if GET_ENTITY_HEALTH(vehicle[vehChopper].id) < 500								
												Tell_mission_to_fail("CH_F06")								
											ENDIF
										ENDIF
									ENDIF
									
									IF missionProgress != STAGE_TREVOR_GET_CHOPPER
										if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)	
											IF not IS_PED_INJURED(ped[ped_trevor].id)
												if not IS_PED_IN_VEHICLE(ped[ped_trevor].id,vehicle[vehChopper].id)
													Tell_mission_to_fail("CH_F09")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF missionProgress > STAGE_LEARN_TO_SCAN
										if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)	
											IF not IS_PED_INJURED(player_ped_id())
												if IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)
													vector vtempC
													vTempC = GET_ENTITY_COORDS(player_ped_id())
													if vTempC.x < -2000 Or vTempC.x > 2000 or vTempc.y > 1365 or vTempc.y < -3000
														Tell_mission_to_fail("CH_F11")
													endif
												ENDIF
											ENDIF
										ENDIF
									endif
									
									if missionProgress = STAGE_TREVOR_GET_CHOPPER
										
										
										/*
										weapon_type playerWep
										playerWep=WEAPONTYPE_UNARMED
										IF GET_CURRENT_PED_WEAPON(player_ped_id(),playerWep)									
											IF playerWep != WEAPONTYPE_UNARMED
												IF GET_INTERIOR_AT_COORDS(<< 438.8143, -984.8113, 31.3253 >>) = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
													IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_EXTSTRS_ROOM002")
														Mission_Failed("CH_F15")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										IF IS_PED_SHOOTING(player_ped_id())
											IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<443.060303,-943.353638,12.574972>>, <<441.947815,-1061.535400,38.027191>>, 130.812500)
												SET_WANTED_LEVEL_MULTIPLIER(1.0)
												SET_PLAYER_WANTED_LEVEL(player_id(),3)
												
												SET_PLAYER_WANTED_LEVEL_NO_DROP(player_id(),3)
												SET_PLAYER_WANTED_LEVEL_NOW(player_id())
												Mission_Failed("CH_F13")
											ENDIF
										ENDIF
	*/

									ENDIF
									
									/*
									IF DOES_ENTITY_EXIST(vehicle[vehFranklin].id)
										IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
											Tell_mission_to_fail("CMN_FDEST")
										ENDIF
									ENDIF
									*/
									
									if DOES_ENTITY_EXIST(ped[ped_franklin].id)
										if IS_PED_INJURED(ped[ped_franklin].id)
											Tell_mission_to_fail("CMN_FDIED")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK

						
					ENDSWITCH
					
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC DELETE_EVERYTHING(enumFailState failState)
	int i
		WHILE IS_CUTSCENE_ACTIVE()	
		  IF IS_CUTSCENE_PLAYING()
		      STOP_CUTSCENE(TRUE)		 
		  ELSE
		      REMOVE_CUTSCENE ()
		   ENDIF
		   SAFEWAIT(0)
		ENDWHILE
		
		SET_SCENARIO_GROUP_ENABLED("MP_POLICE",TRUE)
		RENDER_SCRIPT_CAMS(false,FALSE)
		//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP() //COMMENTING OUT DUE TO BUG url:bugstar:1541155.
		IF IS_GAMEPLAY_HINT_ACTIVE()
			STOP_GAMEPLAY_HINT(true)
		ENDIF
		
		WHILE NOT TRIGGER_MUSIC_EVENT("CAR2_MISSION_FAIL")
			safewait()
		ENDWHILE
		
		HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(FALSE)
		SET_WIDESCREEN_BORDERS(FALSE, 0) 
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)

		CLEAR_HELP()
		CLEAR_PRINTS()
		CLEAR_FOCUS()
		if STREAMVOL_IS_VALID(sVol)
			STREAMVOL_DELETE(sVol)
		ENDIF
		audio_level_flag  = 0
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_HELI_FILTERING")
			STOP_AUDIO_SCENE("CAR_2_HELI_FILTERING")
		ENDIF
		//SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
		SET_IGNORE_NO_GPS_FLAG(FALSE)

		REMOVE_MODEL_HIDE(<<-111.8944,-188.65,46.02>>,1.0,prop_tree_birch_04)						
		REMOVE_MODEL_HIDE(<<-109.937,-179.954,47.62>>,1.0,prop_tree_birch_04)
		REMOVE_MODEL_HIDE(<<-108.3848,-171.323,49.34>>,1.0,prop_tree_birch_04)	
		
		IF IS_HELIHUD_ACTIVE(HUD)			
			SET_CHOPPER_HUD_ACTIVE(hud,vehicle[vehChopper].id,false, vehicle[vehChopper].id)
		ENDIF
		REMOVE_SCENARIO_BLOCKING_AREAS()
		IF failState = MISSION_STATE_PASSED
		OR failState = MISSION_STATE_FAILED
		OR failState = MISSION_STATE_DEATH_ARREST
			DISABLE_CELLPHONE(FALSE)
			SET_PED_PATHS_BACK_TO_ORIGINAL(<<-290.3812, -100.4234, 46.3507>>-<<10,10,10>>,<<-290.3812, -100.4234, 46.3507>>+<<10,10,10>>)
			SET_ROADS_IN_ANGLED_AREA(<<-48.346008,-109.031822,82.549194>>, <<195.550781,-197.283768,-38.864712>>, 155.812500,FALSE,true)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1284.524292,-212.007370,50.633434>>-<<39.000000,54.750000,14.000000>>,<<-1284.524292,-212.007370,50.633434>>+<<39.000000,54.750000,14.000000>>,true)
			SET_ROADS_IN_ANGLED_AREA(<<1413.366333,-2037.311035,49.111671>>, <<1345.929077,-2093.547363,57.623245>>, 96.500000,FALSE,true)
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<514.633423,-1321.991821,25.631521>>, <<502.822418,-1297.020508,37.444073>>, 19.312500)
			//REMOVE_SCENARIO_BLOCKING_AREA(scenario_block_spawn_area)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-961.907654,-3006.614502,21.694874>>-<<41.187500,41.500000,11.125000>>,<<-961.907654,-3006.614502,21.694874>>+<<41.187500,41.500000,11.125000>>,true) //airport hanagar
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1382.831177,-2064.168701,56.185715>>,<<41.250000,44.750000,5.750000>>,TRUE)
			CLEAR_PED_NON_CREATION_AREA()
			SET_PED_PATHS_IN_AREA(<<1382.831177,-2064.168701,56.185715>>,<<41.250000,44.750000,5.750000>>,TRUE)
			
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
			SET_FRONTEND_RADIO_ACTIVE(TRUE)
			
			if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehicle[vehHeist].id,true)
			ENDIF
			
			if IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehicle[vehFranklin].id,true)
			ENDIF		
			
			SET_VEHICLE_MODEL_IS_SUPPRESSED(BLIMP, FALSE)
			
		ENDIF
		
		HUD.bBlockTargetLost = FALSE
		
		//remove peds
		REPEAT COUNT_OF(ped) i
			//if i > 0		
				if DOES_ENTITY_EXIST(ped[i].id)	
					IF ped[i].id != player_ped_id()
						IF failState = MISSION_STATE_FAILED
						OR failState = MISSION_STATE_SKIP
							SET_ENTITY_AS_MISSION_ENTITY(ped[i].id)
							DELETE_PED(ped[i].id)
						ELSE
							IF NOT IS_PED_INJURED(ped[i].id)				
								IF NOT IS_PED_A_PLAYER(ped[i].id)	
									if i = ped_pilot							
										if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
											ped_index temp_ped
											temp_ped = GET_PED_IN_VEHICLE_SEAT(vehicle[vehChopper].id,VS_FRONT_RIGHT)
											if not IS_PED_INJURED(temp_ped)
												SET_ENTITY_HEALTH(ped[ped_pilot].id,100)
											ELSE
												if not IS_PED_INJURED(ped[ped_pilot].id)
													TASK_HELI_MISSION(ped[ped_pilot].id,vehicle[vehChopper].id,null,null,<< -1372.4413, -738.7798, 177.5559 >>,MISSION_GOTO,20.0,10.0,-1,150,150)
												ENDIF
											ENDIF
											IF not IS_PED_INJURED(PLAYER_PED_ID())
												if IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id)																																					
													TASK_PAUSE(PLAYER_PED_ID(),4000)																											
												ENDIF
											ENDIF
										ENDIF	
										IF NOT IS_PED_INJURED(ped[ped_pilot].id)
											SET_PED_KEEP_TASK(ped[ped_pilot].id,true)
										ENDIf
									ENDIF
									
							
									if i = ped_trevor
										if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
											IF not IS_PED_INJURED(PLAYER_PED_ID())
												if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id) 
													if not IS_PED_INJURED(ped[ped_trevor].id)
														if IS_PED_IN_VEHICLE(ped[ped_trevor].id,vehicle[vehChopper].id) 
															seq()
															//	TASK_PAUSE(null,4000)
														//		TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(null,vehicle[vehChopper].id)	
																TASK_HELI_MISSION(null,vehicle[vehChopper].id,null,null,<< -1372.4413, -738.7798, 177.5559 >>,MISSION_GOTO,20.0,10.0,-1,150,150)
															endseq(ped[ped_trevor].id)
															
															SET_PED_KEEP_TASK(ped[ped_trevor].id,true)											
														endif									
													endif
												endif
											ENDIF
										endif
									endif
									
									IF failState = MISSION_STATE_PASSED
									AND i != ped_cop1
										DELETE_PED(ped[i].id)
									ELSE
										IF IS_ENTITY_A_MISSION_ENTITY(ped[i].id)
											SET_PED_AS_NO_LONGER_NEEDED(ped[i].id)
										ENDIF							
									ENDIF
								ENDIF
							ENDIF								
						ENDIF
					ENDIF
				ENDIF
			//
		ENDREPEAT
		
		//remove nearby peds for fail test check
		REPEAT COUNT_OF(pedList) i	
				if DOES_ENTITY_EXIST(pedList[i])	
					IF pedList[i] != player_ped_id()
						IF failState = MISSION_STATE_FAILED
						OR failState = MISSION_STATE_SKIP
							SET_ENTITY_AS_MISSION_ENTITY(pedList[i])
							DELETE_PED(pedList[i])
						ENDIF
					ENDIF	
				ENDIF
		ENDREPEAT
		
	//	IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
	//	AND NOT IS_PED_INJURED(player_ped_id())
	//		IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)
			//	TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(player_ped_id(),vehicle[vehChopper].id)
	//			SET_PED_KEEP_TASK(player_ped_id(),TRUE)
	//		ENDIF
	//	ENDIF
		
		IF IS_VALID_INTERIOR(int_police)
			UNPIN_INTERIOR(int_police)
		ENDIF
		
		//lock airport gate
		IF IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_IN, PLAYER_PED_ID())
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_IN, PLAYER_PED_ID())
		ENDIF
		
		//lock
		IF IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_OUT, PLAYER_PED_ID()) 
        	UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_OUT, PLAYER_PED_ID())  
		ENDIF
		
		IF failState != MISSION_STATE_DEATH_ARREST
		AND failState != MISSION_STATE_PASSED
			IF GET_PLAYER_MODEL() = PLAYER_TWO
				SET_ENTITY_COORDS(player_ped_id(),<<392.4630, -969.8661, 28.4456>>)
				SET_ENTITY_HEADING(player_ped_id(), 263.8383)	
//				LOAD_SCENE(<<392.4630, -969.8661, 28.4456>>) //[MF] Fix for B* 1694492
			ELSE
				IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(player_ped_ID()),AC_AIRPORT_AIRSIDE)
				//	SET_ENTITY_COORDS(player_ped_id(),<<-955.2151, -2761.3064, 12.9145>>)
				//	SET_ENTITY_HEADING(player_ped_id(),70)	
				ENDIF
			ENDIF
		ENDIF
		
		

		
		REPEAT COUNT_OF(vehicle) i
			if DOES_ENTITY_EXIST(vehicle[i].id)
				if i = vehHeist	
					IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[i].id,FALSE)
						SET_ENTITY_AS_MISSION_ENTITY(vehicle[i].id,TRUE,TRUE)
					ENDIF
					IF failState = MISSION_STATE_DEATH_ARREST
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[i].id)
					ELSE
						IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[i].id)
							vector vSafe
							vSafe = <<0,0,0>>
							IF GET_SAFE_COORD_FOR_PED(GET_ENTITY_COORDS(player_ped_id()),false,vSafe)
								SET_ENTITY_COORDS(player_ped_id(),vSafe)
							ELSE
								SET_ENTITY_COORDS(player_ped_id(),GET_ENTITY_COORDS(player_ped_id()))
							ENDIF
						ENDIF
						DELETE_VEHICLE(vehicle[i].id)
					ENDIF
				else
					IF failState = MISSION_STATE_FAILED
					OR failState = MISSION_STATE_SKIP
					//	if i = vehChopper //was causing vehicles to not be removed on fail/skip
							IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[i].id)
								SET_ENTITY_COORDS(player_ped_id(),GET_ENTITY_COORDS(player_ped_id()))
							ENDIF
				
							if NOT IS_ENTITY_A_MISSION_ENTITY(vehicle[i].id)
							OR NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[i].id,FALSE)
								SET_ENTITY_AS_MISSION_ENTITY(vehicle[i].id,true,true)
							ENDIF
				
							IF IS_VEHICLE_DRIVEABLE(vehicle[i].id)												
								DELETE_VEHICLE(vehicle[i].id)
							ENDIF
							
					//	endif
					ELSE
						
						IF IS_VEHICLE_DRIVEABLE(vehicle[i].id)
							IF failState = MISSION_STATE_PASSED
							and i = vehJet
								DELETE_VEHICLE(vehicle[i].id)
							ELSE
								if NOT IS_ENTITY_A_MISSION_ENTITY(vehicle[i].id)
								OR NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehicle[i].id,FALSE)
									SET_ENTITY_AS_MISSION_ENTITY(vehicle[i].id,true)
								ENDIF
								SET_ENTITY_HAS_GRAVITY(vehicle[i].id,TRUE)
									SET_VEHICLE_GRAVITY(vehicle[i].id,TRUE)
								FREEZE_ENTITY_POSITION(vehicle[i].id,FALSE)										
								SET_ENTITY_INVINCIBLE(vehicle[i].id,FALSE)	
								SET_FORCE_HD_VEHICLE(vehicle[i].id,FALSE)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicle[i].id)
							ENDIF
						ENDIF
					ENDIF
				endif
			endif
		ENDrepeat

		IF carParkCounter > 0
			REPEAT COUNT_OF(carParkVehicles) i			
				IF failState = MISSION_STATE_FAILED
				OR failState = MISSION_STATE_SKIP
					DELETE_VEHICLE(carParkVehicles[i])
				ELSE
					SET_VEHICLE_AS_NO_LONGER_NEEDED(carParkVehicles[i])
				ENDIF
			ENDREPEAT
		 ENDIF
		carParkCounter = 0
		
		kill_all_events(true)

	
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iTrevBlocker)
			REMOVE_NAVMESH_BLOCKING_OBJECT(iTrevBlocker)
		ENDIF

		CLEAR_SCAN_LIST(HUD)
		REMOVE_ALL_HELIHUD_MARKERS(HUD)
		SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[player_vehicle].id,FALSE, vehicle[vehChopper].id)
		SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",FALSE)
		ENABLE_SELECTOR()
		remove_helihud_marker(HUD,destMarker[0])
		remove_helihud_marker(HUD,destMarker[1])
		remove_helihud_marker(HUD,destMarker[2])
		remove_helihud_marker(HUD,destMarker[3])
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)
		CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
		clearHoverInArea(hud)
		//SwitchPreped = FALSE
	

	
		failDelay = 0
		failReason = FAIL_NULL
	
		if DOES_ENTITY_EXIST(pedDevin)
			IF failState = MISSION_STATE_DEATH_ARREST
			OR failState = MISSION_STATE_SKIP
				DELETE_PED(pedDevin)
			ELSE
				IF NOT IS_PED_INJURED(pedDevin)
					SET_PED_KEEP_TASK(pedDevin,TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(pedDevin)
				ENDIF
			ENDIF
		ENDIF
	
		repeat count_of(prop) i
			IF DOES_ENTITY_EXIST(prop[i])
				DELETE_OBJECT(prop[i])
			ENDIF
		endrepeat
		
		IF DOES_ENTITY_EXIST(fakeGarageDoor)
			DELETE_OBJECT(fakeGarageDoor)
		ENDIF
		
/*		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<201.400284,-153.364517,47.848885>>,1.0,PROP_GAR_DOOR_05)
			object_index objB
			objB = GET_CLOSEST_OBJECT_OF_TYPE(<<201.400284,-153.364517,47.848885>>,1.0,PROP_GAR_DOOR_05)
			SET_ENTITY_VISIBLE(objB,true)
			set_entity_coords(objB,<<201.400284,-153.364517,57.848885>>)
		ENDIF*/
	
		IF failState != MISSION_STATE_SKIP
			#IF IS_DEBUG_BUILD
				if DOES_WIDGET_GROUP_EXIST(carStealWidget)
					DELETE_WIDGET_GROUP(carStealWidget)
				ENDIF
			#ENDIF
		ENDIF
	
		IF IS_VALID_INTERIOR(int_garage)
			UNPIN_INTERIOR(int_garage)
		ENDIF
		
		IF IS_VALID_INTERIOR(intHangar)
			UNPIN_INTERIOR(intHangar)
		ENDIF
		
		
		
		CLEANUP_MUSIC()
		iChaseCV=0
		iNotSeenCount=0
		chaseDialogueEnum = DIA_NONE
		bplayVehRecs = FALSE
		set_chopper_speed_limits(HUD)

		SET_INFRARED(FALSE,true)
		infrared = FALSE
		CLEAR_FOCUS()
		bDisplayBeam = FALSE
		
		helpTimer = 0
		timeAllowingProgress = 0
		iMovingStickCount = 0
		bMovingStick = FALSE
		
		//pedBeingScanned = 0
		pedScanned = 0
		scanPedToRemove = 0
		pedRemoveTimer = 0
		
		HUD.fChopperSpeed = 0
		HUD.actualSpeed = 0
		
		
		bLastConvoWithoutSubtitles = FALSE
		restartRoot = ""
		restartLine = ""
		
		REPEAT COUNT_OF(recordingData) i
			recordingData[i].playing = FALSE
			recordingData[i].ended = FALSE
			IF DOES_ENTITY_EXIST(recordingData[i].vehicleID)
				if IS_ENTITY_A_MISSION_ENTITY(recordingData[i].vehicleID)
					DELETE_VEHICLE(recordingData[i].vehicleID)
				ENDIF
				//SET_VEHICLE_AS_NO_LONGER_NEEDED(recordingData[i].vehicleID)
			ENDIF
		ENDREPEAT
		
		//reset scene zoom dialogue
		repeat count_of(sceneAudio) i
			sceneAudio[i].active = FALSE
			sceneAudio[i].line = -1
		ENDREPEAT
		
		REPEAT COUNT_OF(viewscene) i
			viewscene[i].nextAction = 0
			viewscene[i].triggerTime = 0
			viewscene[i].dialogueLine = 0
			viewscene[i].nextEvent = 0
			viewscene[i].listenDia = -1
		ENDREPEAT
		bPauseListenerDialogue = FALSE
		
		REPEAT COUNT_OF(chopperListeningLoc) i
			chopperListeningLoc[i] = <<0,0,0>>
		endrepeat
		IF DOES_BLIP_EXIST(blipTarget)
			REMOVE_BLIP(blipTarget)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			missionProgress = INT_TO_ENUM(enumMissionStage,iReturnStage)
			If playAsTrevor = FALSE
				IF missionProgress = STAGE_TREVOR_GET_CHOPPER
					missionProgress = STAGE_FRANKLIN_AWAITS_TREVOR
				ENDIF
			ENDIF
		#ENDIF
		
		IF failState != MISSION_STATE_PASSED
		AND failState != MISSION_STATE_DEATH_ARREST
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),200,true)
		ENDIF
	
		
		switchState = SWITCH_SETUP
	
		SET_EVENTS_FLAG(events_upside_Down,0)
		control_events(EVENTS_NULL,TRUE)
		if STREAMVOL_IS_VALID(sVol)
			STREAMVOL_DELETE(sVol)
		ENDIF
		
		//CLEAR_TIMECYCLE_MODIFIER()
		KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)
		KILL_ANY_CONVERSATION()
		lastConv = ""
		currentConv = ""
		currentConvType = CONVTYPE_NULL
		RESET_CONDITIONS()
		RESET_ACTIONS()
		RESET_INSTRUCTIONS()
		RESET_DIALOGUE()
		RESET_FAILS()
		
		
		
		bWaveformDisplayingQuiet = FALSE
		bWaveformDisplayingLoud = FALSE
		
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE) //added because of bug 527011
		
	
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_HELI_CAM_TUTORIAL")
			STOP_AUDIO_SCENE("CAR_2_HELI_CAM_TUTORIAL")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_SCAN_FRANKLIN")
			STOP_AUDIO_SCENE("CAR_2_SCAN_FRANKLIN")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_SCANNING_TARGET")
			STOP_AUDIO_SCENE("CAR_2_SCANNING_TARGET")
		ENDIF
		
		
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_SCAN_THE_SUSPECTS")
			STOP_AUDIO_SCENE("CAR_2_SCAN_THE_SUSPECTS")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_USE_MICROPHONE")
			STOP_AUDIO_SCENE("CAR_2_USE_MICROPHONE")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_FOLLOW_CHAD_ON_FOOT")
			STOP_AUDIO_SCENE("CAR_2_FOLLOW_CHAD_ON_FOOT")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_FRANKLIN_ARRIVES")
			STOP_AUDIO_SCENE("CAR_2_FRANKLIN_ARRIVES")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_CAR_CHASE_START")
			STOP_AUDIO_SCENE("CAR_2_CAR_CHASE_START")
		ENDIf
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_Z_TYPE_ENGINE_BOOST")
			STOP_AUDIO_SCENE("CAR_2_Z_TYPE_ENGINE_BOOST")
		ENDIf

	
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_CAR_CHASE_CONTINUED")
			STOP_AUDIO_SCENE("CAR_2_CAR_CHASE_CONTINUED")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_CAR_IN_SIGHT")
			STOP_AUDIO_SCENE("CAR_2_CAR_IN_SIGHT")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_CAR_ENTERS_GARAGE")
			STOP_AUDIO_SCENE("CAR_2_CAR_ENTERS_GARAGE")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_USE_INFRARED")
			STOP_AUDIO_SCENE("CAR_2_USE_INFRARED")
		ENDIf
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_STEAL_THE_CAR")
			STOP_AUDIO_SCENE("CAR_2_STEAL_THE_CAR")
		ENDIf
		IF IS_AUDIO_SCENE_ACTIVE("CAR_2_DRIVE_BACK_TO_GARAGE")
			STOP_AUDIO_SCENE("CAR_2_DRIVE_BACK_TO_GARAGE")
		ENDIf
		
		IF IS_AUDIO_SCENE_ACTIVE("Car_Steal_2_Chase_Skids_01")
			STOP_AUDIO_SCENE("Car_Steal_2_Chase_Skids_01")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("Car_Steal_2_Chase_Skids_02")
			STOP_AUDIO_SCENE("Car_Steal_2_Chase_Skids_02")
		ENDIF
					
ENDPROC

PROC Mission_Cleanup(enumFailState failState)	
	DELETE_EVERYTHING(failState)
	CLEANUP_PC_CONTROLS()
	TERMINATE_THIS_THREAD() 
ENDPROC



// ------------------------------------------------------------------------------
//		Mission Pass
// ------------------------------------------------------------------------------

PROC mission_Passed()
	CLEAR_PRINTS()
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED)
	//PRINT("CHPASSED",DEFAULT_GOD_TEXT_TIME,1) //PRINT("S3_PASS", DEFAULT_GOD_TEXT_TIME, 1)
	
	Mission_Flow_Mission_Passed()

	MISSION_CLEANUP(MISSION_STATE_PASSED)
ENDPROC

PROC Mission_Failed(String failText=null)
	IF IS_STRING_NULL(failText) failText = "CHFAILED" ENDIF

	CLEAR_PRINTS()
	
	CLEAR_HELP()
	
	WHILE NOT TRIGGER_MUSIC_EVENT("CAR2_MISSION_FAIL")
		safewait()
	ENDWHILE
	


	MISSION_FLOW_MISSION_FAILED_WITH_REASON(failText)    
	
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here

	Mission_Cleanup(MISSION_STATE_FAILED) // must only take 1 frame and terminate the thread 
ENDPROC


FUNC BOOL CONTROL_CHASE_CHATTER()
	IF bplayVehRecs
		IF iChaseCV = count_of(chaseDialogue)
			RETURN TRUE
		ENDIF
		TEXT_LABEL_23 sLineToPlay
		IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehFranklin].id)
				fFranklineTime = GET_TIME_POSITION_IN_RECORDING(vehicle[vehFranklin].id)
			ENDIF
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehicle[vehheist].id)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehheist].id)
				//IF GET_PLAYBACK_NUMBER_FOR_VEHICLE_VIA_RECID(vehicle[vehheist].id) = chaseDialogue[iChaseCV].chadRecordingToCheck				
					fChadTime = GET_TIME_POSITION_IN_RECORDING(vehicle[vehheist].id)				
				//ELSE				
				//	fChadTime = -1
				//ENDIF
			ELSE
				fChadTime = -1
			ENDIF
		ENDIF

		IF chaseDialogue[iChaseCV].askForDirections
			SWITCH chaseDialogueEnum
				CASE DIA_NONE
					IF fFranklineTime >= chaseDialogue[iChaseCV].askTime
						IF fFranklineTime < chaseDialogue[iChaseCV].askTime + 500
							cprintln(debug_trevor3,"Trying to play dialogue: ",iChaseCV)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_tmp16",1,null,"Franklin")
									chaseDialogueEnum = DIA_QUERIED
								ENDIF
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_whatsee",1,null,"Franklin")
									chaseDialogueEnum = DIA_QUERIED
								ENDIF
								
							ENDIF
						ELSE
							chaseDialogueEnum = DIA_ENDING
						ENDIF
					ENDIF
				BREAK
				CASE DIA_QUERIED
					//check if Chad is on screen for more than a second
					IF fFranklineTime < chaseDialogue[iChaseCV].replyByTime
						IF HAS_CHAD_BEEN_OBSERVED()
							iNotSeenCount = 0
							chaseDialogueEnum = DIA_SPOTTED_WAIT
						ENDIF
					ELSE
						//FAIL FOR NOT SPOTTING CHAD
						//Only fail if not seen for second time.
						IF iNotSeenCount = 0
							iNotSeenCount++
							chaseDialogueEnum = DIA_NOTSEEN
						ELSE
							Mission_Failed("CH_F20")
						ENDIF
					ENDIF
				BREAK
				CASE DIA_SPOTTED_WAIT
					iLastSeenLocationLine = GET_LINE_FOR_CHAD_LOCATION(fFranklineTime)
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						chaseDialogueEnum = DIA_SPOTTED
					ENDIF
				BREAK
				CASE DIA_SPOTTED
				//	IF fChadTime > chaseDialogue[iChaseCV].TrevorResponseTime	
						sLineToPlay = buildConvLine(chaseDialogue[iChaseCV].conv_root,iLastSeenLocationLine)
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,chaseDialogue[iChaseCV].conv_root,sLineToPlay,0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
							chaseDialogueEnum = DIA_ENDING
						ENDIF
				//	ENDIF
				BREAK
				CASE DIA_NOTSEEN
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_tmp36",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
						chaseDialogueEnum = DIA_SO_FIND_HIM
					ENDIF
				BREAK
				
				CASE DIA_SO_FIND_HIM
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_find",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
						chaseDialogueEnum = DIA_FIND_OR_FAIL
						iFindByTime = get_game_timer() + 4000
					ENDIF				
				BREAK
				
				CASE DIA_FIND_OR_FAIL
					
					IF HAS_CHAD_BEEN_OBSERVED(TRUE)
						chaseDialogueEnum = DIA_FOUND_CHAD_LOCATION
					ELSE
						IF get_game_timer() > iFindByTime
							Mission_Failed("CH_F20")
						ENDIF
					ENDIF
				BREAK
				
				CASE DIA_FOUND_CHAD_LOCATION
					sLineToPlay = buildConvLine("cs2_tmp18",GET_LINE_FOR_CHAD_LOCATION(fFranklineTime))
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_tmp18",sLineToPlay,0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
						chaseDialogueEnum = DIA_ENDING
						iNotSeenCount = 0
					ENDIF
				BREAK
				
				CASE DIA_ENDING
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						chaseDialogueEnum = DIA_NONE
						iChaseCV++
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF chaseDialogue[iChaseCV].mustBeSeen
			
				SWITCH chaseDialogueEnum
					CASE DIA_NONE
						IF chaseDialogue[iChaseCV].askTime != 0 AND fFranklineTime >= chaseDialogue[iChaseCV].askTime
						OR chaseDialogue[iChaseCV].TrevorResponseTime != 0 AND fChadTime >= chaseDialogue[iChaseCV].TrevorResponseTime
							IF chaseDialogue[iChaseCV].askTime != 0 AND fFranklineTime < chaseDialogue[iChaseCV].askTime + 500
							OR chaseDialogue[iChaseCV].TrevorResponseTime != 0 AND fChadTime < chaseDialogue[iChaseCV].TrevorResponseTime + 500
								IF HAS_CHAD_BEEN_OBSERVED(TRUE)
									chaseDialogueEnum = DIA_SPOTTED
								ELSE
									//if dialogue for a location of Chad, play not seen dialogue instead
									IF chaseDialogue[iChaseCV].FailForNotSeeing
										//shit we lost him.
										chaseDialogueEnum = DIA_FAIL_DIALOGUE
									ELSE
										IF ARE_STRINGS_EQUAL(chaseDialogue[iChaseCV].conv_root,"cs2_tmp18")
											chaseDialogueEnum = DIA_NOTSEEN
										ELSE
											chaseDialogueEnum = DIA_NONE
											iChaseCV++
										ENDIF	
									ENDIF
								ENDIF
							ELSE
								chaseDialogueEnum = DIA_NONE
								iChaseCV++
							ENDIF
						ENDIF
					BREAK
					CASE DIA_SPOTTED
						IF chaseDialogue[iChaseCV].conv_line != 0
							sLineToPlay = buildConvLine(chaseDialogue[iChaseCV].conv_root,chaseDialogue[iChaseCV].conv_line)
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_BANTER,chaseDialogue[iChaseCV].conv_root,sLineToPlay,0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
								chaseDialogueEnum = DIA_ENDING
							ENDIF
						ELSE
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,chaseDialogue[iChaseCV].conv_root,0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
								chaseDialogueEnum = DIA_ENDING
							ENDIF
						ENDIF
					BREAK
					CASE DIA_NOTSEEN
						IF CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_tmp36",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
							iNotSeenCount++
							chaseDialogueEnum = DIA_ENDING
						ENDIF
					BREAK				
					CASE DIA_ENDING
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							chaseDialogueEnum = DIA_NONE
							iChaseCV++
						ENDIF
					BREAK
					CASE DIA_FAIL_DIALOGUE
					//	IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY, "cs2_lostcar",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
							chaseDialogueEnum = DIA_FAIL
					//	ENDIF
					BREAK
					CASE DIA_FAIL
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							Mission_Failed("CH_F20")
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				SWITCH chaseDialogueEnum
					CASE DIA_NONE
						IF chaseDialogue[iChaseCV].askTime != 0 AND fFranklineTime >= chaseDialogue[iChaseCV].askTime
						OR chaseDialogue[iChaseCV].TrevorResponseTime != 0 AND fChadTime >= chaseDialogue[iChaseCV].TrevorResponseTime
					
							IF chaseDialogue[iChaseCV].conv_line != 0
							
								sLineToPlay = buildConvLine(chaseDialogue[iChaseCV].conv_root,chaseDialogue[iChaseCV].conv_line)
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_BANTER,chaseDialogue[iChaseCV].conv_root,sLineToPlay,0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
									chaseDialogueEnum = DIA_ENDING
								ENDIF
							ELSE
								
								IF ARE_STRINGS_EQUAL(chaseDialogue[iChaseCV].conv_root,"cs2_chase3")
									
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,chaseDialogue[iChaseCV].conv_root,0,null,"Trevor",3,null,"cs2_copRadio")
										chaseDialogueEnum = DIA_ENDING
									ENDIF
								ELSE
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,chaseDialogue[iChaseCV].conv_root,0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
										chaseDialogueEnum = DIA_ENDING
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE DIA_ENDING
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							chaseDialogueEnum = DIA_NONE
							iChaseCV++
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC




PROC checkConditions(enumconditions firstCondition, enumconditions lastCondition)
	//IF SIZE_OF(enumconditions) > NO_OF_CONDITIONS
		//SCRIPT_ASSERT("INCREASE CONST_INT NO_OF_CONDITIONS")
	//ENDIF
	int iLastArray = enum_to_int(lastCondition) - enum_to_int(firstCondition)
	IF conditions[0].condition != firstCondition
	AND conditions[iLastArray].condition != lastCondition
		int j
		int enumCounter = 0
		REPEAT COUNT_OF(conditions) j
			IF j <= iLastArray
				conditions[j].condition = int_to_enum(enumconditions,enum_to_int(firstCondition) + enumCounter)
				conditions[j].active = TRUE
				conditions[j].returns = FALSE
				conditions[j].wasTrue = FALSE
				enumCounter++
			ELSE
				conditions[j].active = FALSE
			ENDIF
		ENDREPEAT
		//reset which condition goes in to which array.
		
	ENDIF

	int i
	REPEAT COUNT_OF(conditions) i
		IF conditions[i].active
			SWITCH conditions[i].condition
				CASE COND_FAILING
					IF missionFailing != fail_null
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				CASE COND_COP_WALKING_TO_STAIRS
					IF IS_ACTION_ONGOING(ACT_WALK_TO_STAIRS)
						conditions[i].returns = TRUE
					ELSE
						conditions[i].returns = FALSE
					ENDIF
				BREAK
				CASE COND_CUTSCENE_ENDED
					conditions[i].returns = TRUE
					/*
					SWITCH conditions[i].flag
						CASE 0
							IF IS_CUTSCENE_PLAYING()
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_CUTSCENE_PLAYING()
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH*/
				BREAK

				CASE COND_DIA_COP_LEADING
			
					IF ARE_STRINGS_EQUAL("cs2_cop5",lastConv)
						conditions[i].returns = TRUE
					ENDIF
				BREAK				
				
				CASE COND_PLAYER_ENTERED_RECEPTION
					IF NOT conditions[i].returns
					//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<437.452118,-988.347717,29.385637>>, <<437.466064,-977.924683,32.189537>>, 2.437500)
						IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<434.832092,-983.039307,29.314508>>, <<450.191742,-983.240356,32.814507>>, 11.000000)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				CASE COND_COP_IN_CORRIDOR
					IF NOT conditions[i].returns
						IF NOT IS_PED_INJURED(ped[ped_cop1].id)
							IF GET_ROOM_KEY_FROM_ENTITY(ped[ped_cop1].id) = GET_HASH_KEY("PH_LOCKERS_ROOM")
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PLAYER_ANYWHERE_IN_STAIRWALL
					IF NOT conditions[i].returns
						IF NOT IS_PED_INJURED(player_ped_id())
							IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("PH_EXTSTRS_ROOM001")
							OR GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("PH_EXTSTRS_ROOM002")
							OR GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("PH_EXTSTRS_ROOM003")
							OR GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("PH_EXTSTRS_ROOM004")
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF					
				BREAK
				
				CASE COND_PLAYER_LAGS_BEHIND_OR_WANDERS
					IF NOT IS_PED_INJURED(ped[ped_cop1].id)																
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_cop1].id) > 13
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PLAYER_UP_STAIRWELL
					IF NOT conditions[i].returns
						IF NOT IS_PED_INJURED(player_ped_id())
							IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("PH_EXTSTRS_ROOM002")
							OR GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("PH_EXTSTRS_ROOM003")
							OR GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("PH_EXTSTRS_ROOM004")
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF					
				BREAK
				
				CASE COND_PLAYER_IN_CORRIDOR					
					IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("PH_LOCKERS_ROOM")						
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_OUT_OF_BOUNDS
					/*
					conditions[i].returns = FALSE
					IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_LOCKERS_ROOM")
					AND GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_EXTSTRS_ROOM001")
					AND GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_EXTSTRS_ROOM002")
					AND GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_EXTSTRS_ROOM003")
					AND GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_EXTSTRS_ROOM004")
					AND GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_LOBBY_ROOM")
						conditions[i].returns = TRUE
					ENDIF*/
					
					IF IS_ENTITY_AT_COORD(player_ped_id(), <<454.225983,-982.579773,30.564594>>,<<1.000000,1.000000,1.062500>>)
						conditions[i].returns = TRUE
					ENDIf
				BREAK
				
				
				CASE COND_PLAYER_NEARBY
					IF NOT IS_PED_INJURED(ped[ped_cop1].id)
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),ped[ped_cop1].id) < 5.0
							conditions[i].returns = TRUE
						ELSE
							conditions[i].returns = FALSE
						ENDIF
					ENDIF
				BREAK
				CASE COND_COPS_ALERTED_CONDITION
					conditions[i].returns = FALSE
					IF IS_VALID_INTERIOR(int_police)
						IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) = int_police
							//IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_LOBBY_ROOM")
							//GET_ROOM_NAME_FROM_PED_DEBUG(player_ped_id()))
								/*IF IS_CONDITION_TRUE(COND_CUTSCENE_ENDED)
									IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("PH_LOCKERS_ROOM")
										IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<460.894409,-990.826416,27.877041>>, <<449.021454,-990.770142,33.752041>>, 6.375000)
											
											conditions[i].returns = TRUE
										ENDIF
									ELSE
										IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != GET_HASH_KEY("PH_EXTSTRS_ROOM001")
										AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != GET_HASH_KEY("PH_EXTSTRS_ROOM002")
										AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != GET_HASH_KEY("PH_EXTSTRS_ROOM003")
										AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != GET_HASH_KEY("PH_EXTSTRS_ROOM004")
										
											conditions[i].returns = TRUE
										ENDIF
									ENDIF
								ELSE
									
									conditions[i].returns = TRUE
								ENDIF*/
							//ENDIF
							//IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
								//conditions[i].returns = TRUE
							//ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE COND_COP_IN_STAIRWELL	
					conditions[i].returns = FALSE
					IF DOES_ENTITY_EXIST(ped[ped_cop1].id)
						IF IS_VALID_INTERIOR(int_police)
							IF NOT IS_PED_INJURED(ped[ped_cop1].id)
								IF GET_INTERIOR_FROM_ENTITY(ped[ped_cop1].id) = int_police
									IF GET_ROOM_KEY_FROM_ENTITY(ped[ped_cop1].id) = GET_HASH_KEY("PH_EXTSTRS_ROOM001")
									OR GET_ROOM_KEY_FROM_ENTITY(ped[ped_cop1].id) = GET_HASH_KEY("PH_EXTSTRS_ROOM002") 							
										conditions[i].returns = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
		
				CASE COND_CAN_SPAWN_CHOPPER
					IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<465.032135,-985.680969,38.766777>>, <<465.031616,-981.791077,42.766624>>, 8.000000)
						conditions[i].returns = TRUE
					ENDIF
					IF IS_CONV_ROOT_PLAYING("cs2_cop5")
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				CASE COND_PLAYER_NOT_FOLLOWING_COP
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_cop1].id)
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_cop1].id,TRUE) > 8.0
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
							
				CASE COND_PLAYER_ON_ROOF
					conditions[i].returns = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<476.378693,-987.339050,42.320747>>, <<430.220062,-986.779541,47.257648>>, 31.125000)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				CASE COND_PLAYER_TRYS_TO_ENTER_CHOPPER
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
						IF IS_PED_GETTING_INTO_A_VEHICLE(player_ped_id())
							IF GET_VEHICLE_PED_IS_USING(player_ped_id()) = vehicle[vehChopper].id
								conditions[i].returns = TRUE
							ENDIF
						ELIF IS_CONTROL_JUST_PRESSED(player_control,INPUT_ENTER) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehicle[vehChopper].id) < 9
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PLAYER_IN_LOCKER_ROOM
					conditions[i].returns = FALSE
					IF IS_ENTITY_AT_COORD(player_ped_id(), <<455.441803,-991.061951,31.064594>>,<<5.312500,3.625000,1.500000>>)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_TREVOR_TELLS_PILOT_TO_TAKE_OFF
					IF IS_CONV_ROOT_PLAYING("cs2_cop7")
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_RUNS_AHEAD
					IF IS_CONDITION_TRUE(COND_COP_LEADING_PLAYER)
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_cop1].id) > 5
							IF IS_CONDITION_TRUE(COND_PLAYER_IN_CORRIDOR)
							AND NOT IS_CONDITION_TRUE(COND_COP_IN_CORRIDOR)
							AND NOT IS_CONDITION_TRUE(COND_COP_IN_CORRIDOR)
								conditions[i].returns = TRUE
							ENDIF
							
							IF IS_CONDITION_TRUE(COND_COP_IN_STAIRWELL)
							AND IS_CONDITION_TRUE(COND_PLAYER_ANYWHERE_IN_STAIRWALL)
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_TREVOR_TELLS_PILOT_TO_ACTIVATE_SCANNER
					SWITCH conditions[i].flag
						CASE 0 
							IF IS_CONV_ROOT_PLAYING("cs2_cop7")
								conditions[i].flag = 1
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_CONV_ROOT_PLAYING("cs2_cop7")							
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE COND_READY_TO_TURN_ON_SCANNER
					IF IS_CONDITION_TRUE(COND_TREVOR_TELLS_PILOT_TO_ACTIVATE_SCANNER)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				CASE COND_DIA_BACK_TO_FRONT_DESK				
					IF IS_CONV_ROOT_PLAYING("cs2_cop6")
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				CASE COND_DIA_GET_IN				
					//IF ARE_STRINGS_EQUAL("cs2_cop10",lastConv)
					IF IS_CONV_ROOT_PLAYING("cs2_cop10")
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				CASE COND_PLAYER_IN_CHOPPER
					conditions[i].returns = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)	
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
						//OR IS_PED_GETTING_INTO_A_VEHICLE(player_ped_id())
							IF GET_VEHICLE_PED_IS_in(player_ped_id()) = vehicle[vehChopper].id
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
						
					ENDIF
				BREAK
				CASE COND_COP_LEADING_PLAYER
					conditions[i].returns = FALSE
					IF IS_ACTION_ONGOING(ACT_WALK_TO_STAIRS)					
					AND NOT IS_ACTION_COMPLETE(6,ACT_COP_RETURNS_TO_FRONT_DESK)
						conditions[i].returns = TRUE
					ENDIF							 
				BREAK
				CASE COND_PLAYER_SPOTTED_BY_PILOT
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_pilot].id)
						//IF IS_PED_IN_VIEWING_ARC_FROM_PED(ped[ped_pilot].id,player_ped_id(),180,13)
						IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<464.015961,-984.180542,42.066925>>, <<437.531738,-984.932678,47.191925>>, 28.062500)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF				
				BREAK
				
				CASE COND_PLAYER_LEAVING_TREVOR
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_trevor].id)
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<1381.1898, -2065.8499, 50.9983>>) > 80
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				//learn to scan
				CASE COND_STARTED_AS_TREVOR
					IF playAsTrevor
						conditions[i].returns = TRUE
					ELSE
						conditions[i].returns = FALSE
					ENDIF
				BREAK
				
				CASE COND_FAR_ENOUGH_FROM_FRANKLIN_TO_PLAY_CONVO
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_franklin].id)
						IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_franklin].id) > 600
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_LEARNT_TO_SCAN
					IF NOT conditions[i].returns = TRUE
						IF IS_INSTRUCTION_COMPLETE(0,INS_TEACH_TO_SCAN)
						
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_BOOTING_UP_LINE_PLAYED
					IF NOT conditions[i].returns
						IF IS_CONV_ROOT_PLAYING("CS2_inst2")
							IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 1
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_FRANKLIN_SCANNED
					IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_franklin].id)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_DIA_SCANNED_CONVO_FINISHED
					IF HAS_DIALOGUE_FINISHED(4,DIA_FRANKLIN_SCANNED)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_DIA_SCAN_MY_BUDDY
				
					IF HAS_DIALOGUE_FINISHED(5,DIA_GOING_TO_FIND_BUDDY)
					OR HAS_DIALOGUE_FINISHED(1,DIA_SCAN_BUDDY_NEARBY)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_TREV_SAID_TO_FIND_FRANKLIN
					IF HAS_DIALOGUE_FINISHED(5,DIA_GOING_TO_FIND_BUDDY)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_TOLD_TO_SCAN_FRANKLIN
					SWITCH conditions[i].flag
						CASE 0
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS7")
								conditions[i].intA = GET_GAME_TIMER() + 3000
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > conditions[i].intA								
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_OUTSIDE_SAFE_ZONE
					conditions[i].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<483.12451, -1176.62402, 40.71120>>) > 2250
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_FLEW_IN_TO_FAIL_ZONE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<483.12451, -1176.62402, 40.71120>>) > 2650
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_APPROACHING_FRANKLIN
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<1377.20, -2066.32, 51.00>>) < 350
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				//scan area 1
				CASE COND_POSTAL_BEING_OBSERVED		
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_courier].id)
						IF GET_OBSERVED_HELIHUD_PED(HUD) = ped[ped_courier].id
							IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_courier].id) < 300
								conditions[i].returns = TRUE						
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PERVERT_BEING_OBSERVED
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_pervert].id)
					AND NOT IS_PED_INJURED(ped[ped_wife].id)
					AND NOT IS_PED_INJURED(ped[ped_husband].id) 
						IF GET_OBSERVED_HELIHUD_PED(HUD) = ped[ped_pervert].id
						OR GET_OBSERVED_HELIHUD_PED(HUD) = ped[ped_wife].id
						OR GET_OBSERVED_HELIHUD_PED(HUD) = ped[ped_husband].id
							IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_wife].id) < 300
								conditions[i].returns = TRUE						
							ENDIF	
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_AREA1_PED_JUST_SCANNED
					SWITCH conditions[i].flag
						CASE 0				
							IF NOT IS_BIT_SET(conditions[i].intA,0)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_courier].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,FALSE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,0)
									conditions[i].intB = GET_GAME_TIMER() + 2000
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(conditions[i].intA,1)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_pervert].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,FALSE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,1)
									conditions[i].intB = GET_GAME_TIMER() + 2000
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(conditions[i].intA,2)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_husband].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,FALSE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,2)
									conditions[i].intB = GET_GAME_TIMER() + 2000
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(conditions[i].intA,3)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_wife].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,TRUE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,3)
									conditions[i].intB = GET_GAME_TIMER() + 2000
								ENDIF
							ENDIF
							IF conditions[i].returns
								IF IS_BIT_SET(conditions[i].intA,0)
								AND IS_BIT_SET(conditions[i].intA,1)
								AND IS_BIT_SET(conditions[i].intA,2)
								AND IS_BIT_SET(conditions[i].intA,3)
									conditions[i].returns = FALSE
									conditions[i].flag=10 //don't do any more checks
								ENDIF
							ENDIF
						BREAK
						CASE 1
							
							IF IS_AMBIENT_SPEECH_PLAYING(ped[ped_trevor].id)
							OR GET_GAME_TIMER() > conditions[i].intB
								conditions[i].returns = FALSE
								conditions[i].flag=2
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_AMBIENT_SPEECH_PLAYING(ped[ped_trevor].id)
							OR GET_GAME_TIMER() > conditions[i].intB
								conditions[i].flag=0
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_DIA_COME_IN_FRANKLIN_PLAYED
					SWITCH conditions[i].flag
						CASE 0	
							IF IS_CONV_ROOT_PLAYING("CS2_arrive")
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_CONV_ROOT_PLAYING("CS2_arrive")
								conditions[i].flag++
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_CHOPPER_IN_VIEWING_RANGE_OF_AREA1
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-21.36739, -54.06999, 62.39728>>,FALSE) < 180
						conditions[i].returns = TRUE
					ELSE
						conditions[i].returns = FALSE
					ENDIF
				BREAK
				
				CASE COND_AREA3_PED_JUST_SCANNED
					SWITCH conditions[i].flag
						CASE 0				
							IF NOT IS_BIT_SET(conditions[i].intA,0)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_hooker].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,TRUE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,0)
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(conditions[i].intA,1)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_punter].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,FALSE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,1)
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(conditions[i].intA,2)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_pimp].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,FALSE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,2)
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(conditions[i].intA,3)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_bum].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,FALSE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,3)
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(conditions[i].intA,4)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_dog_walk].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,FALSE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,4)
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(conditions[i].intA,5)
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_chadGirl].id)
									SET_CONDITION_STATE(COND_SCANNED_PED_WAS_A_WOMAN,TRUE)
									conditions[i].returns = TRUE
									conditions[i].flag=1
									SET_BIT(conditions[i].intA,5)
								ENDIF
							ENDIF
							IF conditions[i].returns
								IF IS_BIT_SET(conditions[i].intA,0)
								AND IS_BIT_SET(conditions[i].intA,1)
								AND IS_BIT_SET(conditions[i].intA,2)
								AND IS_BIT_SET(conditions[i].intA,3)
								AND IS_BIT_SET(conditions[i].intA,4)
								AND IS_BIT_SET(conditions[i].intA,5)
									conditions[i].returns = FALSE
									conditions[i].flag=10 //don't do any more checks
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF IS_AMBIENT_SPEECH_PLAYING(ped[ped_trevor].id)
								conditions[i].returns = FALSE
								conditions[i].flag=2
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_AMBIENT_SPEECH_PLAYING(ped[ped_trevor].id)
								conditions[i].flag=0
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_PROSIE_BEING_OBSERVED
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_hooker].id)
						IF GET_OBSERVED_HELIHUD_PED(HUD) = ped[ped_hooker].id
							IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_hooker].id) < 300
								conditions[i].returns = TRUE						
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_CHAD_BEING_OBSERVED
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						IF GET_OBSERVED_HELIHUD_PED(HUD) = ped[ped_chad].id
							IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_chad].id) < 300
								conditions[i].returns = TRUE						
							ENDIF					
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_LISTENING_IN_ON_DIALOGUE
					conditions[i].returns = FALSE
					IF listenDialogue != -1
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				//scan area 3
				
				CASE COND_CHAD_HIDDEN
					conditions[i].returns = FALSE
					IF IS_CONDITION_TRUE(COND_CHAD_WAS_SCANNED)
						if HUD.targetIsLost
							IF conditions[i].intA = 0
								conditions[i].intA = GET_GAME_TIMER() + 1000
							ENDIF
							IF GET_GAME_TIMER() > conditions[i].intA
								conditions[i].returns = TRUE
							ENDIF
						ELSE
							conditions[i].intA = 0			
						endif
					ENDIF
				BREAK
				
				CASE COND_CHAD_ON_SCREEN
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						if IS_ENTITY_ON_SCREEN(ped[ped_chad].id)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_CHAD_OFFSCREEN_FOR_4_SECONDS
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						SWITCH conditions[i].flag
							CASE 0
								conditions[i].returns = FALSE
								IF NOT IS_ENTITY_ON_SCREEN(ped[ped_chad].id)
									conditions[i].intA = GET_GAME_TIMER()
									conditions[i].flag++
								ENDIF
							BREAK
							CASE 1
								IF NOT IS_ENTITY_ON_SCREEN(ped[ped_chad].id)
									IF GET_GAME_TIMER() - conditions[i].intA > 4000
										conditions[i].returns = TRUE
									ENDIF
								ELSE
									conditions[i].flag=0
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE COND_CHAD_BY_GARAGES
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
					IF NOT conditions[i].returns
						IF IS_ENTITY_IN_ANGLED_AREA(ped[ped_chad].id, <<193.618256,-152.232742,55.388981>>, <<213.026962,-159.061966,61.175438>>, 6.500000)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
					ENDIF
				BREAK
				
				CASE COND_CHAD_IN_APARTMENT_LOCATE
				
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
					IF IS_ENTITY_IN_ANGLED_AREA(ped[ped_chad].id, <<207.965149,-151.093109,59.599060>>, <<215.810669,-151.350418,76.994919>>, 20.750000)
						conditions[i].returns = TRUE
					ENDIF
					ENDIF
				BREAK
				
				CASE COND_GARAGE_DOOR_OPENING
					IF fOpenRatio > 0.05
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				// ============================ CAR PARK CASE CONDITIONS ==============================
				CASE COND_CARPARK_STARTS
					conditions[i].returns = TRUE					
				BREAK
				
				CASE COND_CHOPPER_DESCENDING
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
						vector vTemp					
						vTemp = GET_ENTITY_COORDS(vehicle[vehChopper].id)
						IF vTemp.z < 100.0
							conditions[i].returns = TRUE
						ENDIF					
					ENDIF
				BREAK
				
				CASE COND_FRANKLIN_STOPPED_HIS_CAR
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehFranklin].id)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_FRANKLIN_HAS_STOPPED_RUNNING
					IF NOT conditions[i].returns
						IF NOT IS_PED_INJURED(ped[ped_franklin].id)
							IF IS_ENTITY_IN_ANGLED_AREA( ped[ped_franklin].id, <<-1273.912354,-226.118301,49.924622>>, <<-1276.218140,-222.831039,52.174622>>, 4.812500)
								IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ped[ped_franklin].id)
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_A_LITTLE_AFTER_EXITS_CLEAR_DIA
					IF NOT conditions[i].returns
						SWITCH conditions[i].flag
							CASE 0
								IF ARE_STRINGS_EQUAL(lastConv,"cs2_clear")
									conditions[i].intA = GET_GAME_TIMER() + 3000
									conditions[i].flag++
								ENDIF
							BREAK
							CASE 1
								IF GET_GAME_TIMER() > conditions[i].intA
									conditions[i].returns = TRUE
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE COND_DIA_TAKE_US_DOWN
					SWITCH conditions[i].flag
						CASE 0
							IF IS_CONV_ROOT_PLAYING("cs2_flylow")					
								conditions[i].flag++					
							ENDIf
						BREAK
						CASE 1
							IF NOT IS_CONV_ROOT_PLAYING("cs2_flylow")					
								conditions[i].returns = TRUE					
							ENDIf
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_DIA_THERMAL_VISION
					IF IS_CONV_ROOT_PLAYING("CS2_Carpark3")
						IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 1
							conditions[i].returns = TRUE
						ENDIF
					ENDIF 
				BREAK
				
				CASE COND_DIA_CAN_YOU_SEE_FRANKLIN
					IF ARE_STRINGS_EQUAL(lastConv,"cs2_onme")
						conditions[i].returns = TRUE
					ENDIF 
				BREAK
				
				CASE COND_DIA_ANY_OTHER_HEAT_SOURCES
					IF HAS_DIALOGUE_FINISHED(6,DIA_ANY_OTHER_HEAT_SOURCES)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_DIA_LOOK_FOR_MORE_HEAT_PLAYED
					if not conditions[i].returns				
					
						IF ARE_STRINGS_EQUAL(lastConv,"cs2_looknow")
							conditions[i].returns = true
						ENDIF						
					ENDIF
				BREAK
				

				
				CASE COND_DIA_TREVOR_SEES_CHAD_ENDED
					IF ARE_STRINGS_EQUAL(lastConv,"CS2_manc1")
						conditions[i].returns = TRUE
					ENDIF
				BREAK								
				
				CASE COND_DIA_FRANKLIN_SEES_PISSER
					IF ARE_STRINGS_EQUAL(lastConv,"cs2_mana3")
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_DIA_FRANKLIN_SEES_FIXING_MAN
					IF ARE_STRINGS_EQUAL(lastConv,"cs2_mand2b")
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_DIA_FRANKLIN_SEES_CHAD
					SWITCH conditions[i].flag
						CASE 0							
							IF IS_DIALOGUE_COMPLETE(12,DIA_FRANKLIN_SEES_ZTYPE)
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_CONV_ROOT_PLAYING("cs2_manc2")
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_DIA_TREVOR_SEE_FRANKLIN_ENDED
					IF ARE_STRINGS_EQUAL(lastConv,"cs2_seen")
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_CHOPPER_AT_CAR_PARK_LEVEL
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
						IF IS_ENTITY_IN_ANGLED_AREA( vehicle[vehChopper].id, <<-1324.085449,-217.174683,42.192139>>, <<-1297.745728,-256.196350,62.782513>>, 19.187500)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				CASE COND_THERMAL_TURNED_ON
					IF infraRed
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				CASE COND_FRANKLIN_OBSERVED_WITH_THERMAL
					IF NOT conditions[i].returns
						IF infraRed
							IF NOT IS_PED_INJURED(ped[ped_franklin].id)
								if is_point_in_screeen_area(GET_ENTITY_COORDS(ped[ped_franklin].id,FALSE),0,0,1,1)
									if hud.fFOV < 25															
										conditions[i].returns = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE COND_SEE_WANKER	
					IF NOT 	conditions[i].returns
							IF IS_CONDITION_TRUE(COND_WAITING_FOR_NEXT_TARGET)					
							AND IS_CONDITION_TRUE(COND_THERMAL_READY)
								IF is_ped_in_sight(ped_wrong_wanker)
								AND NOT is_ped_in_sight(ped_wrong_fucker)
								AND NOT is_ped_in_sight(ped_franklin)
								AND NOT is_ped_in_sight(ped_chad)
								AND NOT is_ped_in_sight(ped_wrong_lean)
								AND NOT is_ped_in_sight(ped_wrong_phone)
									conditions[i].returns = TRUE
									conditions[i].flag++
								ENDIF		
							ENDIF
					ENDIF	
				BREAK
				CASE COND_SEE_FIXING_MAN	
					IF NOT 	conditions[i].returns
							IF IS_CONDITION_TRUE(COND_WAITING_FOR_NEXT_TARGET)					
							AND IS_CONDITION_TRUE(COND_THERMAL_READY)
								IF is_ped_in_sight(ped_wrong_fucker)
								AND NOT is_ped_in_sight(ped_wrong_wanker)
								AND NOT is_ped_in_sight(ped_franklin)
								AND NOT is_ped_in_sight(ped_chad)
								AND NOT is_ped_in_sight(ped_wrong_lean)
								AND NOT is_ped_in_sight(ped_wrong_phone)
									conditions[i].returns = TRUE	
									conditions[i].flag++
								ENDIF	
							ENDIF
						ENDIF
				BREAK
				CASE COND_SEE_CHAD
					IF NOT 	conditions[i].returns
							IF IS_CONDITION_TRUE(COND_WAITING_FOR_NEXT_TARGET)					
							AND IS_CONDITION_TRUE(COND_THERMAL_READY)
								IF is_ped_in_sight(ped_chad)
								AND NOT is_ped_in_sight(ped_wrong_wanker)
								AND NOT is_ped_in_sight(ped_wrong_fucker)
								AND NOT is_ped_in_sight(ped_franklin)
								AND NOT is_ped_in_sight(ped_wrong_lean)
								AND NOT is_ped_in_sight(ped_wrong_phone)
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
						ENDIF
				
				BREAK
				
				CASE COND_SEE_LEANING_MAN
					IF NOT 	conditions[i].returns
							IF IS_CONDITION_TRUE(COND_WAITING_FOR_NEXT_TARGET)					
							AND IS_CONDITION_TRUE(COND_THERMAL_READY)
								IF is_ped_in_sight(ped_wrong_lean)
								AND NOT is_ped_in_sight(ped_wrong_wanker)
								AND NOT is_ped_in_sight(ped_wrong_fucker)
								AND NOT is_ped_in_sight(ped_franklin)
								AND NOT is_ped_in_sight(ped_wrong_phone)
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
						ENDIF
				BREAK
				
				CASE COND_SEE_PHONE_CAR_MAN
					IF NOT 	conditions[i].returns
							IF IS_CONDITION_TRUE(COND_WAITING_FOR_NEXT_TARGET)					
							AND IS_CONDITION_TRUE(COND_THERMAL_READY)
								IF is_ped_in_sight(ped_wrong_phone)
								AND NOT is_ped_in_sight(ped_wrong_lean)
								AND NOT is_ped_in_sight(ped_wrong_wanker)
								AND NOT is_ped_in_sight(ped_wrong_fucker)
								AND NOT is_ped_in_sight(ped_franklin)
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
						ENDIF
				BREAK
				
				CASE COND_NEXT_TARGET_FOUND
					IF IS_CONDITION_TRUE(COND_WAITING_FOR_NEXT_TARGET)
						int iTargetsSeen
						iTargetsSeen=0
						IF IS_CONDITION_TRUE(COND_SEE_CHAD) iTargetsSeen++ ENDIF
						IF IS_CONDITION_TRUE(COND_SEE_FIXING_MAN) iTargetsSeen++ ENDIF
						IF IS_CONDITION_TRUE(COND_SEE_WANKER) iTargetsSeen++ ENDIF
						IF IS_CONDITION_TRUE(COND_SEE_LEANING_MAN) iTargetsSeen++ ENDIF
						IF IS_CONDITION_TRUE(COND_SEE_PHONE_CAR_MAN) iTargetsSeen++ ENDIF
						IF iTargetsSeen > conditions[i].intA
							conditions[i].intA = iTargetsSeen
							conditions[i].returns = TRUE		
							
						ENDIF
					ENDIF
					
					IF IS_CONDITION_TRUE(COND_INVESTIGATING_NEXT_TARGET)
						conditions[i].returns = FALSE
					ENDIF
				BREAK
				
				CASE COND_INVESTIGATING_NEXT_TARGET
					SWITCH conditions[i].flag
						CASE 0																				
							IF IS_CONV_ROOT_PLAYING("cs2_manb1")
							OR IS_CONV_ROOT_PLAYING("cs2_manb1b")
							OR IS_CONV_ROOT_PLAYING("cs2_manc1")
							OR IS_CONV_ROOT_PLAYING("cs2_mana1")
							OR IS_CONV_ROOT_PLAYING("cs2_mane1")
								conditions[i].returns = TRUE
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_mana3")
							OR IS_CONV_ROOT_PLAYING("cs2_mand2b")
							OR IS_CONV_ROOT_PLAYING("cs2_mane2")
							OR IS_CONV_ROOT_PLAYING("cs2_manb2b")
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("cs2_mana3")
							AND NOT IS_CONV_ROOT_PLAYING("cs2_mand2b")
							AND NOT IS_CONV_ROOT_PLAYING("cs2_mane2")
							AND NOT IS_CONV_ROOT_PLAYING("cs2_manb2b")
								conditions[i].returns = FALSE
								SET_INSTRUCTION_FLAG(3,INS_LOOK_FOR_ANOTHER_HEAT_SOURCES,0)
								conditions[i].flag = 0
							ENDIF
						BREAK
					ENDSWITCH
				
						//this is set back to flase elsewhere in script
																
				BREAK/*
				CASE COND_FRANKLIN_AT_PISSER
					SWITCH conditions[i].flag
						CASE 0
							//IF IS_ACTION_ONGOING(
						BREAK
					ENDSWITCH
							
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_franklin].id)
						IF NOT ARE_STRINGS_EQUAL(lastConv,"cs2_mana3")
						AND NOT IS_CONDITION_TRUE(COND_DIA_FRANKLIN_SEES_PISSER)
							IF IS_ENTITY_AT_COORD(ped[ped_franklin].id,pedWankPosition,<<2,2,2>>)
								IF GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				/*
				CASE COND_FRANKLIN_AT_FIXING_MAN
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_franklin].id)
						IF NOT ARE_STRINGS_EQUAL(lastConv,"cs2_manc2")
						AND NOT IS_CONDITION_TRUE(COND_DIA_FRANKLIN_SEES_FIXING_MAN)
							IF IS_ENTITY_AT_COORD(ped[ped_franklin].id,<< -1262.5160, -241.0382, 50.6831 >>,<<2,2,2>>)
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK*/
				
				CASE COND_BLOCK_LISTENING
					/*conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_franklin].id)
						IF (GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_ANY) = performing_TASK 
						AND GET_SCRIPT_TASK_STATUS(ped[ped_franklin].id,SCRIPT_TASK_PLAY_ANIM) != performing_Task)
						OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								conditions[i].returns = TRUE						
						ENDIF
					ENDIF*/
				BREAK
				
				CASE COND_FRANKLIN_SAW_WRONG_GUY
					SWITCH conditions[i].flag
						CASE 0	
							IF IS_CONDITION_TRUE(COND_INVESTIGATING_NEXT_TARGET)
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_CONDITION_TRUE(COND_INVESTIGATING_NEXT_TARGET)
								conditions[i].returns = TRUE
								//set back to false elsewhere in script
								conditions[i].flag++
							ENDIF
						BREAK

					ENDSWITCH
				BREAK
				
				CASE COND_FRANKLIN_RUNS_TO_CAR
					IF IS_ENTITY_PLAYING_ANIM(player_ped_id(),"misscarsteal2switch","_ground_franklin")
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				/*
				CASE COND_CAN_SEE_UNSCANNED_PED	
					IF IS_CONDITION_TRUE(COND_SEE_CHAD)
					OR IS_CONDITION_TRUE(COND_SEE_FIXING_MAN)
					OR IS_CONDITION_TRUE(COND_SEE_WANKER)
					OR IS_CONDITION_TRUE(COND_SEE_LEANING_MAN)
					OR IS_CONDITION_TRUE(COND_SEE_PHONE_CAR_MAN)
					ENDIF
				BREAK
				*/
				CASE COND_WAITING_FOR_NEXT_TARGET
					SWITCH conditions[i].flag
						CASE 0
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS32b")						
								conditions[i].intA = GET_GAME_TIMER() + 2000
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > conditions[i].intA
								conditions[i].flag++
								conditions[i].returns = TRUE
							ENDIF
						BREAK
						CASE 2
							IF IS_CONDITION_TRUE(COND_NEXT_TARGET_FOUND)
								conditions[i].returns = FALSE
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 3
							IF IS_CONDITION_TRUE(COND_INVESTIGATING_NEXT_TARGET)
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 4
							IF IS_CONDITION_FALSE(COND_INVESTIGATING_NEXT_TARGET)
							AND IS_CONDITION_FALSE(COND_DIA_FROM_FRANKLIN_SEEING_CHAD)
								conditions[i].returns = TRUE
								conditions[i].flag = 2
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				
				
				CASE COND_THERMAL_READY
				//	IF NOT conditions[i].returns	//bug 1746477					
						IF HAS_DIALOGUE_FINISHED(6,DIA_ANY_OTHER_HEAT_SOURCES)
							IF infraRed
								conditions[i].returns = TRUE
							ELSE
								conditions[i].returns = FALSE
							ENDIF
						ENDIF							
				//	ENDIF
				BREAK
				/*
				CASE COND_FRANKLIN_SEES_WANKER_AT_IT
					IF NOT IS_PED_INJURED(ped[ped_franklin].id)
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_franklin].id,<< -1273.6810, -213.1801, 50.5499 >>) < 3.0
							conditions[i].returns = TRUE							
						ENDIF
					ENDIF					
				BREAK*/
				
				CASE COND_CHAD_INTERRUPTED
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						IF GET_SCRIPT_TASK_STATUS(ped[ped_chad].id,SCRIPT_TASK_SYNCHRONIZED_SCENE) = DORMANT_TASK
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_CHAD_CAN_BACK_OFF
					
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(scene_chad_leaves_car) AND GET_SYNCHRONIZED_SCENE_PHASE(scene_chad_leaves_car) > 0.766)
					OR IS_CONDITION_TRUE(COND_GUN_AIMED_AT_CHAD)
					OR IS_CONDITION_TRUE(COND_CHAD_INTERRUPTED)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_GUN_AIMED_AT_CHAD
										
						IF NOT conditions[i].returns
							IF GET_GAME_TIMER() > conditions[i].intA
								IF NOT IS_PED_INJURED(ped[ped_chad].id)
									IF IS_PLAYER_FREE_AIMING_AT_ENTITY(GET_PLAYER_INDEX(), ped[ped_chad].id)
									OR IS_PLAYER_TARGETTING_ENTITY(GET_PLAYER_INDEX(), ped[ped_chad].id)
										IF ((IS_SYNCHRONIZED_SCENE_RUNNING(scene_chad_leaves_car) aND GET_SYNCHRONIZED_SCENE_PHASE(scene_chad_leaves_car) > 0.35)
										OR GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_chad].id) < 4.0)
											IF NOT IS_PED_IN_ANY_VEHICLE(ped[ped_chad].id) //so chad won't flee when back in car
												//make sure Chad's exit anim has played out enough.
												conditions[i].returns = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
				BREAK
				
				CASE COND_CHAD_CAN_RUN
					IF NOT IS_CONDITION_TRUE(COND_CHAD_ESCAPED_IN_ZTYPE)
						//IF IS_DIALOGUE_COMPLETE(1,DIA_BEG_TO_LET_GO)
						//OR IS_DIALOGUE_COMPLETE(12,DIA_PLAYER_RUNS_OFF)
						IF IS_CONDITION_TRUE(COND_CHAD_DAMAGED_BY_PLAYER)	
						//OR IS_ACTION_COMPLETE(8,ACT_REACTS_TO_PLAYER_IN_CAR)
						//OR IS_ACTION_COMPLETE(11,ACT_CHAD_EXITS_CAR)
						OR IS_CONDITION_TRUE(COND_PLAYER_SHOOTING_NEAR_CHAD)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PLAYER_200M_FROM_SECURITY
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),airportBarrier) < 200
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_AT_SECURITY
					IF IS_INSTRUCTION_COMPLETE(1,INS_DRIVE_TO_AIRPORT)
					AND GET_INSTRUCTION_FLAG(1,INS_DRIVE_TO_AIRPORT) = 3
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_CHAD_PLAYER_IN_GARAGE
					conditions[i].returns = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<-1307.389160,-189.894394,38.635483>>, <<-1263.044800,-251.514374,64.209320>>, 41.312500)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_CHAD_DAMAGED_BY_PLAYER
					SWITCH conditions[i].flag
						CASE 0
							IF NOT IS_PED_INJURED(ped[ped_chad].id)
								conditions[i].intA = GET_ENTITY_HEALTH(ped[ped_chad].id)
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_PED_INJURED(ped[ped_chad].id)
								IF GET_ENTITY_HEALTH(ped[ped_chad].id) < conditions[i].intA
									conditions[i].returns = TRUE
								ENDIF
							ELSE
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_PLAYER_SHOOTING_NEAR_CHAD
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						IF NOT conditions[i].returns
							IF (IS_PED_SHOOTING(player_ped_id()) 
							AND GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped[ped_chad].id) < 20)
							OR HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(ped[ped_chad].id),10)
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_CHAD_RAGDOLL
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						IF IS_PED_RAGDOLL(ped[ped_chad].id)
							conditions[i].returns = TRUE
						ENDIf
					ENDIF
				BREAK
				
				CASE COND_PLAYER_GOT_IN_CAR
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
						IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehHeist].id,TRUE)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				//cs2_call
				CASE COND_PHONE_CALL_MADE
					IF NOT conditions[i].returns
						IF conditions[i].intB = 0
							IF IS_PHONE_ONSCREEN()
							OR IS_CONV_ROOT_PLAYING("cs2_call")
								conditions[i].intB = 1							
							ENDIF
						ELSE
							IF NOT IS_CONV_ROOT_PLAYING("cs2_call")
							AND NOT IS_PHONE_ONSCREEN()
							
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
						
						IF IS_DIALOGUE_COMPLETE(8,DIA_CALL_DEVIN)						
							IF conditions[i].intA = 0
								IF IS_CONV_ROOT_PLAYING("cs2_call") AND GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 3
									conditions[i].intA = GET_GAME_TIMER() + 3000
								ENDIF
							ELSE
								IF GET_GAME_TIMER() > conditions[i].intA
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
								
						ENDIF	
					ENDIF
				BREAK
				
				CASE COND_PROCEED_TO_HANGAR_CONDITIONS_MET
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
					//	IF GET_PLAYER_WANTED_LEVEL(player_id()) = 0
							IF IS_DIALOGUE_COMPLETE(4,DIA_OPEN_GATES)
							OR IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(vehicle[vehHeist].id),AC_AIRPORT_AIRSIDE)
							OR IS_ENTITY_IN_ANGLED_AREA( vehicle[vehHeist].id, <<-1007.525696,-2845.613525,11.766135>>, <<-968.910645,-2777.963379,20.382439>>, 12.312500)
								conditions[i].returns = TRUE
							ENDIF
					//	ENDIF
					ENDIF
				BREAK
				
				CASE COND_NO_DIALOGUE_FOR_TWO_SECONDS
					SWITCH conditions[i].flag
						CASE 0
							IF IS_CONDITION_TRUE(COND_PLAYER_GOT_IN_CAR)
								conditions[i].flag++
								conditions[i].intA = GET_GAME_TIMER() + 5000
							ENDIF
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > conditions[i].intA
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_CHAD_DEAD
					IF DOES_ENTITY_EXIST(ped[ped_chad].id)
						IF IS_PED_DEAD_OR_DYING(ped[ped_chad].id)
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN, TRUE)
							conditions[i].returns = TRUE
						ELSE
							//This in theory shouldn't be needed but is in as a safety measure in case the flag has been left on from a previous playthrough.
							IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN)
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					//If we retried the mission and Chad was killed previously then also flag Chad as dead.
					IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_CHAD_PLAYER_100m_FROM_CAR_PARK
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< -1285.9066, -225.3947, 48.2642 >>) > 100
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_30m_from_garage
					conditions[i].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),carDropoffCoord) < 30
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_200m_from_garage
					conditions[i].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),carDropoffCoord) < 200
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_300m_from_garage
					conditions[i].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),devinCoord) < 300
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_DEFAULT_LOAD_CUT_RANGE_FROM_HANGAR
					conditions[i].returns = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),devinCoord) < DEFAULT_CUTSCENE_LOAD_DIST
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_WANTED
					conditions[i].returns = FALSE
					IF GET_PLAYER_WANTED_LEVEL(player_id()) != 0
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
			
				
				
				CASE COND_PLAYER_IN_ZTYPE
					conditions[i].returns = FALSE
					IF NOT IS_PED_INJURED(player_ped_id())
						IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
							IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehHeist].id)
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_DRIVE_TO_INSTRUCTIONS_GIVEN
					SWITCH conditions[i].flag
						CASE 0
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS53")
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("CH_INS53")
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_CHAD_ESCAPED_IN_ZTYPE
					IF IS_ACTION_COMPLETE(11,ACT_CHAD_EXITS_CAR)
						IF NOT IS_PED_INJURED(ped[ped_chad].id)
						AND IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
							IF IS_PED_IN_VEHICLE(ped[ped_chad].id,vehicle[vehHeist].id)							
								IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),vehicle[vehHeist].id) > 20
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_DEVIN_WALKING_IN_TO_ZTYPE
					IF NOT IS_PED_INJURED(pedDevin)
					AND IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
						vector vTemp
						vTemp = GET_ENTITY_COORDS(pedDevin)
						
						IF DOES_VEHICLE_OVERLAP_ANGLED_AREA(vehicle[vehHeist].id,vTemp-<<0.5,0.5,1>>,vTemp+<<0.5,0.5,1>>,1.0)
						OR GET_DISTANCE_BETWEEN_ENTITIES(pedDevin,vehicle[vehHeist].id) < 3.5
							//cprintln(debug_trevor3,"Devin is overlapping the ztype. Devin coords: ",vtemp, "heist car coords: ",vtempb)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_CHOPPER_DAMAGED_BY_PLAYER
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
						SWITCH conditions[i].flag
							CASE 0
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle[vehChopper].id,player_ped_id())
									conditions[i].intA = GET_GAME_TIMER() + 3000
									conditions[i].returns = TRUE
									conditions[i].flag++
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehicle[vehChopper].id)
								ENDIF
							BREAK
							CASE 1
								IF GET_GAME_TIMER() > conditions[i].intA
									conditions[i].flag = 0
									conditions[i].returns = FALSE
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE COND_PLAY_IN_ZTYPE
					conditions[i].returns = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
						IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehHeist].id)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_DAMAGED_ZTYPE
					SWITCH conditions[i].flag
						CASE 0
							IF HAS_DIALOGUE_FINISHED(10,DIA_DRIVING_BACK)
								IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									conditions[i].intA = GET_ENTITY_HEALTH(vehicle[vehHeist].id)
									conditions[i].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF GET_ENTITY_HEALTH(vehicle[vehHeist].id) < conditions[i].intA - 50
								conditions[i].returns = TRUE	
								
							ENDIF
						
						BREAK
					ENDSWITCH								
				BREAK
				
				CASE COND_PLAYER_7M_FROM_CHAD
					IF NOT conditions[i].returns
						//IF NOT IS_PED_INJURED(ped[ped_chad].id)
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-991.5975, -3022.6431, 12.9451>>) < 7
								conditions[i].returns = TRUE
							ENDIF
						//ENDIF
					ENDIF
				BREAK
				
				CASE COND_ZTYPE_IN_CAR_STOP_RANGE
					IF NOT conditions[i].returns
						IF IS_CONDITION_FALSE(COND_WANTED)
							IF IS_CONDITION_TRUE(COND_ZTYPE_IN_HANGAR)
								IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								
									float fZSpeed
									fZSpeed = GET_ENTITY_SPEED(vehicle[vehHeist].id)
									bool bInDevinStop, bInCorrectStop, bPointingAtCorrect
								
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[vehHeist].id,<<-990.65, -3016.24, 12.94>>) < 4.5 //correct stop area
										bInCorrectStop = TRUE
									ENDIF
								
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[vehHeist].id,<<-991.17, -3023.55, 12.94>>) < 4.5 + (fZSpeed*0.25) //Devin stop area
										bInDevinStop = TRUE
									ENDIF
								
									IF bInCorrectStop OR bInDevinStop
										//find out if car pointing towards correct stop
										float adj,opp, ang, carAng
										adj = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[vehHeist].id,<<-990.65, -3016.24, 12.94>>)
										opp = 4.5
										ang = atan(opp/adj)
										carAng = absf(GET_ENTITY_HEADING(vehicle[vehHeist].id) - GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(vehicle[vehHeist].id),<<-990.65, -3016.24, 12.94>>))
										IF carAng > 180 carAng = absf(carAng-360) ENDIF
										
										IF carAng < ang
											bPointingAtCorrect = TRUE
										ENDIF
										
										cprintln(debug_Trevor3,"hit ang = ",ang," actual ang = ",carAng)
										
										IF bInDevinStop AND NOT bPointingAtCorrect
											cprintln(debug_Trevor3,"Stop for Devin")
											SET_CONDITION_STATE(COND_STOP_FOR_DEVIN,TRUE)
											REMOVE_CUTSCENE()										
											REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("Car_2_mcs_1", CS_SECTION_1 | CS_SECTION_2)
											SET_ACTION_FLAG(7,ACT_LOAD_END_CUTSCENE,1) //reload cutscene variations
											conditions[i].returns = TRUE	
										ELIF bInCorrectStop
											cprintln(debug_Trevor3,"Stop for Correct")
											SET_CONDITION_STATE(COND_STOP_FOR_CORRECT,TRUE)
											conditions[i].returns = TRUE	
										ENDIF
										
									ENDIF
									
								
																	
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				BREAK
				
				
				CASE COND_ZTYPE_IS_STOPPED
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
						IF IS_CONDITION_TRUE(COND_ZTYPE_IN_CAR_STOP_RANGE)
							IF IS_CONDITION_FALSE(COND_WANTED)
								IF GET_ENTITY_SPEED(vehicle[vehHeist].id) < 0.1
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_ZTYPE_IN_HANGAR
					conditions[i].returns = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[vehHeist].id,<<-978.445923,-3028.967041,12.944874>>) < 30
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				
			ENDSWITCH
		ENDIF
	ENDREPEAT
	

ENDPROC



// ================================================ Instructions ========================================



PROC INSTRUCTIONS(int thisI, enumInstructions eInstruction,  andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL)
	//reset if instructino is new
	IF instr[thisI].ins != eInstruction
		instr[thisI].bCompleted = FALSE
		instr[thisI].ins = eInstruction
	ENDIF
	
	IF NOT instr[thisI].bCompleted
		IF checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3)
			SWITCH instr[thisI].ins
				CASE INS_GET_TO_RECEPTION
					PRINT("CH_POL1",DEFAULT_GOD_TEXT_TIME,1)
					blipTarget = CREATE_BLIP_FOR_COORD(<<441.02, -978.93, 30.69>>)
					instr[thisI].bCompleted = TRUE
				BREAK
				CASE INS_FOLLOW_COP
					SWITCH instr[thisI].flag
						CASE 0	
					
				
							IF DOES_BLIP_EXIST(blipTarget)
								REMOVE_BLIP(blipTarget)
							ENDIF
							IF NOT IS_PED_INJURED(ped[ped_cop1].id)
								blipTarget = CREATE_BLIP_FOR_PED(ped[ped_cop1].id)
							ENDIF
							instr[thisI].flag++
						break
						case 1
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PRINT("CH_POL2",DEFAULT_GOD_TEXT_TIME,1)
								instr[thisI].bCompleted = TRUE
							ENDIF						
						break
					ENDSWITCH					
				BREAK
				CASE INS_GET_TO_ROOF
					SWITCH instr[thisI].flag
						CASE 0					
							IF DOES_BLIP_EXIST(blipTarget)
								REMOVE_BLIP(blipTarget)
							ENDIF
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_POL2")
								clear_prints()
							ENDIF
							FORCE_INSTRUCTION_STATE(1,INS_FOLLOW_COP)
							blipTarget = CREATE_BLIP_FOR_COORD(<< 463.5469, -984.1166, 42.6919 >>)
							instr[thisI].flag++
						BREAK
						CASE 1
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PRINT("CH_POL3",DEFAULT_GOD_TEXT_TIME,1)
								instr[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE INS_GET_IN_CHOPPER
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
						SWITCH instr[thisI].flag
							 CASE 0
							 	if DOES_BLIP_EXIST(blipTarget)
									remove_blip(blipTarget)
								endif
								IF IS_VALID_INTERIOR(int_police)
									UNPIN_INTERIOR(int_police)
								ENDIF
								blipTarget = CREATE_BLIP_FOR_ENTITY(vehicle[vehChopper].id)
								instr[thisI].flag++
							BREAK
							CASE 1
								
									IF NOT IS_PED_GETTING_INTO_A_VEHICLE(player_ped_id())
									AND NOT IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF IS_THIS_PRINT_BEING_DISPLAYED("CH_POL3")
												CLEAR_PRINTS()
											ENDIF
											PRINT("CH_INS1",1,1)																			
										ENDIF
									ELSE
										IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)
											if DOES_BLIP_EXIST(blipTarget)
												remove_blip(blipTarget)
												instr[thisI].flag++
											endif
										ENDIF
									ENDIF							
														
							BREAK
							CASE 2
								IF NOT IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)
									blipTarget = CREATE_BLIP_FOR_ENTITY(vehicle[vehChopper].id)
									instr[thisI].flag++
								ENDIF
							BREAK
							CASE 3
								IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)
									
									remove_blip(blipTarget)
									instr[thisI].flag=2
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE INS_ABANDON_TREVOR
					PRINT("CH_INS50",DEFAULT_GOD_TEXT_TIME,1)
					instr[thisI].bCompleted = TRUE					
				BREAK
				
				// learn to scan
				CASE INS_TEACH_TO_SCAN
					SWITCH instr[thisI].flag
						 CASE 0
						 	PRINT_HELP_FOREVER("CH_INS3")	
							instr[thisI].intA = GET_GAME_TIMER() + 8000
							instr[thisI].flag++
						BREAK
						CASE 1
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_INS3")
								IF timeAllowingProgress = 0
									timeAllowingProgress = get_game_timer() + 3000
								ENDIF
								//GET_POSITION_OF_ANALOGUE_STICKS(PAD1,iLeftX,iLeftY,iRightX,iRightY) //depricated
								IF ABSF(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD)) > 0.1
								OR ABSF(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR)) > 0.1
									helpTimer+=floor(1000*TIMESTEP())
									IF NOT bMovingStick
										bMovingStick = TRUE
										iMovingStickCount++
									ENDIF
								ELSE
									bMovingStick = FALSE
								ENDIF
								if helpTimer>3000	
								or iMovingStickCount >= 2
								OR GET_GAME_TIMER() > instr[thisI].intA
									IF get_game_timer() > timeAllowingProgress
										bMovingStick = FALSE
										iMovingStickCount = 0
										timeAllowingProgress = 0
										helpTimer=0
										instr[thisI].flag++
										CLEAR_HELP()
									ENDIF
								ENDIF
							ELSE
								CLEAR_HELP()
								instr[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							PRINT_HELP_FOREVER("CH_INS4")	
							instr[thisI].flag++
						BREAK
						CASE 3
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_INS4")
								IF timeAllowingProgress = 0
									timeAllowingProgress = get_game_timer() + 3000
									instr[thisI].intA = GET_GAME_TIMER() + 8000
								ENDIF
								IF ABSF(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SNIPER_ZOOM)) >= 0.05
									helpTimer+=floor(1000*TIMESTEP())
									IF NOT bMovingStick
										bMovingStick = TRUE
										iMovingStickCount++
									ENDIF
								ELSE
									bMovingStick = FALSE
								ENDIF
								if helpTimer>3000	
								or iMovingStickCount >= 2
								OR GET_GAME_TIMER() > instr[thisI].intA
									IF get_game_timer() > timeAllowingProgress
										bMovingStick = FALSE
										iMovingStickCount = 0
										timeAllowingProgress = 0
										helpTimer=0
										CLEAR_HELP()
										instr[thisI].flag=6
									ENDIF
								ENDIF
							ELSE
								instr[thisI].flag=6
							ENDIF
						BREAK
						CASE 4
							PRINT_HELP("CH_INS4")	
							instr[thisI].flag=6
						BREAK
						CASE 5
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_INS5")	
								if hud.fFOV < 20.0
									CLEAR_HELP()
									instr[thisI].flag++
								ENDIF
							ELSE
								instr[thisI].flag++
							ENDIF
						BREAK
						CASE 6
							PRINT_HELP("CH_INS6d",5500)	
							instr[thisI].flag++
						BREAK
						CASE 7
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_INS6d")			
								instr[thisI].bCompleted = TRUE
							ENDIF
							/*
								if is_point_in_screeen_area(<< 1270.127, -1769.433, 48.311 >>,0.1,0.1,0.9,0.9)								
									helpTimer+=floor(1000*TIMESTEP())
									if helpTimer > DEFAULT_GOD_TEXT_TIME					
										helpTimer = 0
										CLEAR_HELP()
										instr[thisI].bCompleted = TRUE
									ENDIF
								ENDIF
							ELSE							
								instr[thisI].bCompleted = TRUE
							ENDIF		*/	
						BREAK
					ENDSWITCH
				BREAK
				
				CASE INS_SCAN_FRANKLIN
					IF not is_ped_injured(ped[ped_franklin].id)
						SWITCH instr[thisI].flag
							 CASE 0
								PRINT_HELP("CH_INS7b")
								PRINT("CH_INS7",DEFAULT_GOD_TEXT_TIME,1)
								REMOVE_ALL_HELIHUD_MARKERS(HUD)
								ADD_PED_TO_SCAN_LIST(HUD,ped[ped_franklin].id,true,HUD_UNKNOWN,FALSE,TRUE,FALSE,true)				
								ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_franklin].id,"CH_NAME",3)
								instr[thisI].flag++							
							BREAK
							CASE 1
								
								if IS_ENTITY_ON_SCREEN(ped[ped_franklin].id)				
									instr[thisI].intA+=floor(1000*TIMESTEP())
									if instr[thisI].intA > 5000
										helpTimer=0
										CLEAR_HELP()
									ENDIF
								ENDIF
								
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP("CH_INS8c",5000)
									instr[thisI].intA=0
									instr[thisI].flag++
								ENDIF
								
							BREAK
							CASE 2
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_INS8c")
									if IS_ENTITY_ON_SCREEN(ped[ped_franklin].id)
										if HUD.scanning=TRUE or HAS_PED_BEEN_SCANNED(HUD,ped[ped_franklin].id)
											instr[thisI].intA+=floor(1000*TIMESTEP())
											if instr[thisI].intA > 5000
												helpTimer=0
												CLEAR_HELP()
												//PRINT_HELP("CH_INS7c")
												instr[thisI].flag++
											ENDIF
										ENDIF
									ENDIF
								ELSE
									ped_index aPed
									IF GET_HELIHUD_SELECTED_PED(HUD,aPED)
										instr[thisI].flag=5
									ELSE
										instr[thisI].flag++
									ENDIF
									instr[thisI].intA = GET_GAME_TIMER() + 3000
									PRINT_HELP("CH_INS7c",DEFAULT_HELP_TEXT_TIME+3000)
								ENDIF
							BREAK
							CASE 3
							FALLTHRU
							CASE 4
							FALLTHRU
							CASE 5
								IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_Franklin].id)
								OR HUD.scanning = TRUE
									instr[thisI].bCompleted=TRUE
								ELSE
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF GET_GAME_TIMER() > instr[thisI].intA
										
											IF GET_DISTANCE_BETWEEN_ENTITIES(player_peD_id(),ped[ped_franklin].id,FALSE) < 550.0
												ped_index aPEd
												IF GET_HELIHUD_SELECTED_PED(HUD,aPED)
													IF instr[thisI].flag < 5
														CLEAR_HELP()
														PRINT_HELP("CH_INS7c")
														instr[thisI].flag = 5
													ENDIF
												ELSE
													IF instr[thisI].flag < 4
														PRINT_HELP("CH_INS4")
														instr[thisI].flag = 4														
													ENDIF
												ENDIF
											ENDIF
										
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					endif
				BREAK
				
				CASE INS_SCAN_COMPLETE
				//	PRINT_HELP("CH_INS8c")
				//	CLEAR_PRINTS()
					instr[thisI].bCompleted=TRUE
				BREAK
				
				CASE INS_GET_BACK_TO_FRANKLIN
					
				BREAK
				
				CASE INS_FOLLOW_CHAD
					PRINT_NOW("CH_INS13",6000,1)	
					instr[thisI].bCompleted=TRUE
				BREAK
				
				CASE INS_SHOW_HIDDEN_HELP
					PRINT_HELP("CH_INS41")
					instr[thisI].bCompleted=TRUE
				BREAK
						
				
				// CAR PARK INSTRUCTION
				CASE INS_TURN_ON_THERMAL_VISION
					SWITCH instr[thisI].flag
						CASE 0								
							PRINT_HELP_FOREVER("CH_INS31")
							add_event(EVENTS_CONTROL_INFRARED)
							instr[thisI].flag++							
						BREAK
						CASE 1
							IF infraRed = true
								CLEAR_HELP()
								PRINT_HELP("CH_INS32")
								instr[thisI].flag++	
								
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_CONDITION_TRUE(COND_THERMAL_TURNED_ON)
									PRINT("CH_INS30",DEFAULT_GOD_TEXT_TIME,1)
									instr[thisI].bCompleted = TRUE
								ELSE
									instr[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE INS_ZOOM_IN_ON_FRANKLIN
					SWITCH instr[thisI].flag
						CASE 0
							PRINT("CH_ZOOM",DEFAULT_GOD_TEXT_TIME,1)
							instr[thisI].flag=2
						BREAK
						
						case 2
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("CH_ZOOM")
								instr[thisI].intA = GET_GAME_TIMER() + 1500
								instr[thisI].flag++
							ENDIF
						break
						CASE 3
							IF GET_GAME_TIMER() > instr[thisI].intA
								PRINT("CH_ZOOMC",DEFAULT_GOD_TEXT_TIME,1)
								instr[thisI].bCompleted = TRUE
							ENDIf
						BREAK
					ENDSWITCH
				BREAK
				CASE INS_LOOK_FOR_A_HEAT_SOURCES
					SWITCH instr[thisI].flag
						CASE 0
							PRINT("CH_INS32b",DEFAULT_GOD_TEXT_TIME,1)						
							PRINT_HELP("CH_ZOOMb")
							
							instr[thisI].flag++		
							instr[thisI].intA = get_game_timer() + 2500
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > instr[thisI].intA
								instr[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF IS_CONDITION_TRUE(COND_NEXT_TARGET_FOUND)
								IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS32b")
									CLEAR_PRINTS()										
								ENDIF
								instr[thisI].bCompleted = TRUE
							ENDIF																				
						BREAK
					ENDSWITCH						
				BREAK
				CASE INS_LOOK_FOR_ANOTHER_HEAT_SOURCES
					
					SWITCH instr[thisI].flag
						CASE 0
							IF IS_CONDITION_TRUE(COND_WAITING_FOR_NEXT_TARGET)
								instr[thisI].intA = GET_GAME_TIMER() + 1000
								instr[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > instr[thisI].intA
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									PRINT("CH_INS32c",DEFAULT_GOD_TEXT_TIME,1)
									instr[thisI].flag++										
								ENDIF
							ELSE
								IF IS_CONDITION_TRUE(COND_NEXT_TARGET_FOUND)
									instr[thisI].flag++	
								ENDIF
							ENDIF
						BREAK
						
					ENDSWITCH
				BREAK
			
				CASE INS_SWITCH_TO_FRANKLIN
					PRINT("CH_INS39",DEFAULT_GOD_TEXT_TIME,1)
					instr[thisI].bCompleted = TRUE
				BREAK
				
				//take the ztype
				CASE INS_GET_IN_THE_ZTYPE
					switch instr[thisI].flag
						case 0
							PRINT("CH_GET",DEFAULT_GOD_TEXT_TIME,1)
							blipTarget = CREATE_BLIP_FOR_ENTITY(vehicle[vehHeist].id,FALSE)
							instr[thisI].flag++
						break
						case 1
							IF IS_CONDITION_TRUE(COND_PLAYER_GOT_IN_CAR)
							AND IS_CONDITION_TRUE(COND_PHONE_CALL_MADE)
								IF IS_THIS_PRINT_BEING_DISPLAYED("CH_GET")
									CLEAR_PRINTS()
								ENDIF
								
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								
								instr[thisI].bCompleted = TRUE
							ELSE
								IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehHeist].id)
										IF IS_THIS_PRINT_BEING_DISPLAYED("CH_GET")
											CLEAR_PRINTS()
										ENDIF
										IF DOES_BLIP_EXIST(blipTarget)
											REMOVE_BLIP(blipTarget)
										ENDIF
									ELSE
										IF NOT DOES_BLIP_EXIST(blipTarget)
											blipTarget = CREATE_BLIP_FOR_ENTITY(vehicle[vehHeist].id,FALSE)
										ENDIf
									ENDIF
								ENDIF
							ENDIF
							
							
						break
					ENDSWITCH
				BREAK
				CASE INS_DRIVE_TO_AIRPORT
					switch instr[thisI].flag
						case 0
							CLEAR_PRINTS()
							IF DOES_BLIP_EXIST(blipTarget)
								remove_blip(blipTarget)								
							endif
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehicle[vehHeist].id)
								INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehicle[vehHeist].id)								
							ENDIF
							 instr[thisI].flag++
						BREAK
						CASE 1
							IF IS_CONDITION_TRUE(COND_PHONE_CALL_MADE)
								
							//	PRINT("CH_INS25",DEFAULT_GOD_TEXT_TIME,1)						
								SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
								PLAY_MUSIC(mus_get_car_to_objective,mus_null)														
																
								
								instr[thisI].flag++
								instr[thisI].intA = GET_GAME_TIMER() + 5500
								SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE) 
							ENDIF
						break
						case 2
							IF instr[thisI].intA != 0
								IF GET_GAME_TIMER() > instr[thisI].intA
									instr[thisI].intA = 0
									SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
								ENDIF
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								float fStopRange
								float fZSpeed
								fZSpeed = GET_ENTITY_SPEED(vehicle[vehHeist].id)
								fStopRange = fZSpeed * 0.2
								IF fStopRange < 3
									fStopRange = 3
								ENDIF
								
								fStopRange += 2
													
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehicle[vehHeist].id,<<-968.4945, -2798.2935, 12.9648>>) < fStopRange
								OR IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,airportBarrier,<<2,2,5>>,TRUE,vehicle[vehHeist].id,"CH_INS53","CMN_GENGETIN","CMN_GENGETBCK",TRUE,0,TRUE)								
									IF GET_PLAYER_WANTED_LEVEL(player_id()) = 0
										instr[thisI].flag++
									ENDIF
								ELIF IS_CONDITION_TRUE(COND_PROCEED_TO_HANGAR_CONDITIONS_MET)
										instr[thisI].bCompleted = TRUE
									
								ENDIF
							ENDIF
						break
						case 3
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehicle[vehHeist].id,3,2)
								instr[thisI].bCompleted = TRUE
							ENDIF
						break
					endswitch
				BREAK
				CASE INS_DRIVE_TO_DESTINATION
					switch instr[thisI].flag
						case 0
							IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
								//IF GET_PLAYER_WANTED_LEVEL(player_id()) = 0
								IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,carDropoffCoord,<<2.0,2.0,2.0>>,TRUE,vehicle[vehHeist].id,"CH_INS25","CMN_GENGETIN","CMN_GENGETBCK",TRUE,0,TRUE)
									instr[thisI].bCompleted = TRUE
									//ENDIF
								ENDIF
							ENDIF
						break
					endswitch
				BREAK
				CASE INS_WARNING_WANTED_LEVEL
					switch instr[thisI].flag
						case 0
							IF IS_DIALOGUE_COMPLETE(5,DIA_WARNING_COPS)
								PRINT("CH_COPS",DEFAULT_GOD_TEXT_TIME,1)
								instr[thisI].flag++
								instr[thisI].intA = GET_GAME_TIMER() + 12000
							ENDIF
						break
						case 1
							if not IS_THIS_PRINT_BEING_DISPLAYED("CH_COPS")
								PRINT("CH_COPS2",DEFAULT_GOD_TEXT_TIME,1)
								
								instr[thisI].flag++
							ENDIF							
						break
						CASE 2
							IF GET_GAME_TIMER() > instr[thisI].intA
							AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),carDropoffCoord) > 300
								IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
									//IF GET_ENTITY_SPEED(vehicle[vehHeist].id) < 15.0
										SET_MAX_WANTED_LEVEL(5)
										SET_PLAYER_WANTED_LEVEL(player_id(),2)
										SET_PLAYER_WANTED_LEVEL_NOW(player_id())
										
										instr[thisI].flag++
										instr[thisI].intA = GET_GAME_TIMER() + 2000
								//	ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 3
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_CAR_STEAL_2_01", 0.0)
								instr[thisI].bCompleted = TRUE	
							ENDIF
						BREAK
					endswitch
				BREAK
				
				CASE INS_DRIVE_IN_TO_GARAGE
					PRINT("CH_INS38",DEFAULT_GOD_TEXT_TIME,1)
					instr[thisI].bCompleted = TRUE
				BREAK
				
				CASE INS_EXIT_AIRPORT
					switch instr[thisI].flag
						case 0
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
							instr[1].flag=0
							instr[thisI].flag++
						BREAK
						CASE 1
							IF (IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData,<<-951.5104, -2785.9827, 12.9648>>,g_vAnyMeansLocate,TRUE,"CH_INS51")
							OR NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(player_ped_ID()),AC_AIRPORT_AIRSIDE))
							AND NOT IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1000.981567,-2863.637939,12.027286>>, <<-962.783813,-2797.050537,21.385738>>, 41.750000)
								instr[thisI].bCompleted = TRUE
								CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE INS_RETURN_ZTYPE
					switch instr[thisI].flag
						case 0
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
							instr[0].flag=0
							instr[thisI].flag++
						BREAK
						CASE 1
							IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,carDropoffCoord,g_vAnyMeansLocate,TRUE,vehicle[vehHeist].id,"CH_INS52","CMN_GENGETIN","CMN_GENGETBCK")
								instr[thisI].bCompleted = TRUE
								CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF			
ENDPROC

PROC INSTRUCTIONS_ON_DIALOGUE(int thisI, enumInstructions eInstruction, int diaEntry, enumDialogue thisDia,andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL)
	IF instr[thisI].bCompleted = FALSE
		IF dia[diaEntry].bCompleted
			thisDia=thisDia
			INSTRUCTIONS(thisI,eInstruction,andOr1,cond1,andOr2,cond2)
		ENDIF
	ENDIF
ENDPROC

PROC INSTRUCTIONS_ON_ACTION(int thisI, enumInstructions eInstruction, int actEntry, enumActions thisAct,andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL)
	IF instr[thisI].bCompleted = FALSE
		IF actions[actEntry].completed
			thisAct=thisAct
			INSTRUCTIONS(thisI,eInstruction,andOr1,cond1,andOr2,cond2)
		ENDIF
	ENDIF
ENDPROC


// ================================================ Dialogue ============================================



PROC DIALOGUE(int thisI, enumDialogue thisDia, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL)
	IF dia[thisI].dial != thisDia
		dia[thisI].bCompleted = FALSE
		dia[thisI].dial = thisDia
	ENDIF		
	

	
	IF NOT dia[thisI].bCompleted
	//AND NOT bLastConvoWithoutSubtitles

		IF checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3,andOr4,cond4)
		AND NOT bMakeMissionFail

			IF NOT dia[thisI].bStarted dia[thisI].bStarted=TRUE ENDIF
				
			SWITCH dia[thisI].dial
				CASE DIA_CAN_I_HELP
					SWITCH dia[thisI].flag
						case 0
							ADD_PED_FOR_DIALOGUE_EXTRA(0,player_ped_id(),"Trevor")
							ADD_PED_FOR_DIALOGUE_EXTRA(4,ped[ped_cop1].id,"cs2_copA")
							IF PRELOAD_CONVERSATION(MyLocalPedStruct, "CST2AUD","cs2_cop1",CONV_PRIORITY_MEDIUM)
								dia[thisI].flag++
							ENDIF
						break
						CASE 1
							IF IS_CONDITION_TRUE(COND_PLAYER_ENTERED_RECEPTION)
								CLEAR_PRINTS()
								BEGIN_PRELOADED_CONVERSATION()
								IF NOT IS_PED_INJURED(ped[ped_cop1].id)
									TASK_LOOK_AT_ENTITY(ped[ped_cop1].id,player_ped_id(),16000)
									TASK_LOOK_AT_ENTITY(player_ped_id(),ped[ped_cop1].id,16000)
								ENDIF
								dia[thisI].flag++
							ENDIF			
						BREAK
						CASE 2
							IF IS_CONV_ROOT_PLAYING("cs2_cop1")
							//	IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 1
							//		KILL_FACE_TO_FACE_CONVERSATION()
									dia[thisI].bCompleted = TRUE
							//	ENDIF
							ENDIF
						BREAK
						CASE 3
							IF NOT IS_CONV_ROOT_PLAYING("cs2_cop1")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DIA_COP_WALK_CHAT					
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cop5",4,PED[ped_cop1].id,"cs2_cop1",0,PED[ped_trevor].id,"Trevor")
						dia[thisI].bCompleted = TRUE
					ENDIF				
				BREAK
				
				CASE DIA_INTEROGATE_COP
		
					switch dia[thisI].flag					
						case 0
							SWITCH dia[thisI].intA
								CASE 0
								FALLTHRU
								CASE 2
								FALLTHRU
								CASE 4
								FALLTHRU
								CASE 6			
							
									IF NOT IS_PED_INJURED(PED[ped_cop4].id)
							
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),PED[ped_cop4].id) < 5																			
					
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(ped[ped_cop4].id, "CHALLENGE_THREATEN", "S_M_Y_Cop_01_WHITE_FULL_02","SPEECH_PARAMS_FORCE")
											dia[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,4000)																			
										ENDIF
									ENDIF
								BREAK
								CASE 8
									IF NOT IS_PED_INJURED(PED[ped_cop4].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),PED[ped_cop4].id) < 6
											TASK_TURN_PED_TO_FACE_ENTITY(PED[ped_cop4].id,player_ped_id(),-1)
											dia[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,3000)	
											IF NOT IS_PED_INJURED(PED[ped_civ3].id)
												TASK_LOOK_AT_ENTITY(PED[ped_civ3].id,player_ped_id(),-1)
											ENDIF
										ENDIF
									ENDIF
									
								BREAK
								CASE 9
									IF NOT IS_PED_INJURED(PED[ped_cop4].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),PED[ped_cop4].id) < 6																		
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(ped[ped_cop4].id, "PROVOKE_STARING", "S_M_Y_Cop_01_WHITE_FULL_02","SPEECH_PARAMS_FORCE")
											dia[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000,6000)																				
											
										ENDIF
									ENDIF
									
								BREAK
								CASE 10
									IF NOT IS_PED_INJURED(PED[ped_cop4].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),PED[ped_cop4].id) < 6																		
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(ped[ped_cop4].id, "PROVOKE_STARING", "S_M_Y_Cop_01_WHITE_FULL_02","SPEECH_PARAMS_FORCE")
											dia[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000,5000)																				
											
										ENDIF
									ENDIF									
								BREAK
								CASE 11
									IF NOT IS_PED_INJURED(PED[ped_cop4].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),PED[ped_cop4].id) < 6	
											seq()
												TASK_AIM_GUN_AT_ENTITY(null,player_ped_id(),2000)
												TASK_COMBAT_PED(null,player_ped_id())																			
											endseq(PED[ped_cop4].id)
											dia[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,5000)	
										ENDIF
									ENDIF									
								BREAK
								
								
								CASE 1
								FALLTHRU
								CASE 3
								FALLTHRU
								CASE 5
									IF NOT IS_PED_INJURED(PED[ped_civ3].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),PED[ped_civ3].id) < 5
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(ped[ped_civ3].id, "GUN_BEG", "A_M_Y_BevHills_02_Black_FULL_01","SPEECH_PARAMS_FORCE")
											dia[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,5000)									
										ENDIF
									ENDIF
								BREAK
								CASE 7
									IF NOT IS_PED_INJURED(PED[ped_civ3].id)
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),PED[ped_civ3].id) < 5
											PLAY_PAIN(PED[ped_civ3].id, AUD_DAMAGE_REASON_COWER )
											dia[thisI].flag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,3000)									
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
						DEFAULT
							IF GET_GAME_TIMER() > dia[thisI].flag
								dia[thisI].intA++
								dia[thisI].flag = 0								
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_COME_ON
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cop8",4,PED[ped_cop1].id,"cs2_cop1",0,PED[ped_trevor].id,"Trevor")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_COP_RETURNS_TO_DESK
					switch dia[thisI].flag
					
						case 0
							KILL_FACE_TO_FACE_CONVERSATION()
							dia[thisI].flag++				
						BREAK
						CASE 1
							If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cop6",4,PED[ped_cop1].id,"cs2_cop1")
								dia[thisI].bCompleted = TRUE
							ENDIF	
						BREAK
						
					ENDSWITCH
				BREAK
				CASE DIA_PLAYER_GETS_IN_CHOPPER
					switch dia[thisI].flag					
						case 0
							dia[thisI].intA = GET_GAME_TIMER() + 100
							dia[thisI].flag++								
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > dia[thisI].intA
								If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cop7",2,PED[ped_pilot].id,"chopperPilot",0,PED[ped_trevor].id,"Trevor")
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DIA_PLAYER_LAGS_OR_LEAVES
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cop8",4,PED[ped_cop1].id,"cs2_cop1")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				CASE DIA_PLAYER_THREAT_IN_POL_DEPT
					switch dia[thisI].flag					
						case 0						
							KILL_FACE_TO_FACE_CONVERSATION_EXTRA(FALSE)
							IF NOT IS_PED_INJURED(PED[ped_cop3].id)
								TASK_ARREST_PED(PED[ped_cop3].id,player_ped_id())
							ENDIF
							dia[thisI].flag++
						BREAK
						CASE 1
							If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CST2_NOGO",7,PED[ped_cop3].id,"cs2_cop4")								
								dia[thisI].flag++
								SET_WANTED_LEVEL_MULTIPLIER(1.0)
									SET_MAX_WANTED_LEVEL(5)
									SET_PLAYER_WANTED_LEVEL(player_id(),3)
									SET_PLAYER_WANTED_LEVEL_NOW(player_id())
								
								dia[thisI].intA = GET_GAME_TIMER() + 2000
							ENDIF
						BREAK
						CASE 2
							If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CST2_GUN",7,PED[ped_cop3].id,"cs2_cop4")
							OR GET_GAME_TIMER() > dia[thisI].intA
								
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DIA_PILOT_SEES_PLAYER
					CLEAR_PRINTS()
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cop10",2,PED[ped_pilot].id,"chopperPilot")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_PLAYER_ENTERS_LOCKER_ROOM
				
					KILL_FACE_TO_FACE_CONVERSATION()
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CST2_SEES",5,PED[ped_cop4].id,"cs2_cop1")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_WRONG_WAY

					//check
					IF IS_CONV_ROOT_PLAYING("cs2_cop5")
						restartLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cop8",4,PED[ped_cop1].id,"cs2_cop1")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_RESTART_CONV
					IF not IS_STRING_NULL_OR_EMPTY(restartLine)
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cop5",restartLine,4,PED[ped_cop1].id,"cs2_cop1",0,PED[ped_trevor].id,"Trevor")
							dia[thisI].bCompleted = TRUE
						ENDIF
					else
						dia[thisI].bCompleted = TRUE
					ENDIF										
				BREAK
				
				
				CASE dia_player_doesnt_switch
					switch dia[thisI].flag					
						case 0
							IF IS_CONV_ROOT_PLAYING("CS2_chopper")
								dia[thisI].flag++								
							ENDIF
						BREAK
						CASE 1
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS40")
								dia[thisI].intA = GET_GAME_TIMER() + 10000
								dia[thisI].flag++
							ENDIF
						BREAK
						DEFAULT
							IF GET_GAME_TIMER() > dia[thisI].intA
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND NOT IS_MESSAGE_BEING_DISPLAYED()
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_switch",0,PED[ped_trevor].id,"Trevor")
										dia[thisI].flag++
										dia[thisI].intA = GET_GAME_TIMER() + 5000 + (dia[thisI].flag * 2000)
										IF dia[thisI].flag >= 5
											dia[thisI].bCompleted = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				// ================ LEarn to scan ===========================
				CASE DIA_BOOTING_UP
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_inst2",2,null,"chopperPilot")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK	
				
				CASE DIA_SCAN_BUDDY_NEARBY
					SWITCH dia[thisI].flag
						CASE 0
							If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_friend",2,PED[ped_pilot].id,"chopperPilot",0,PED[ped_trevor].id,"Trevor")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_friend")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("cs2_friend")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
												
				CASE DIA_CONV_ON_WAY_TO_FRANKLIN
					SWITCH dia[thisI].flag
						CASE 0
							If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_chat1",0,PED[ped_trevor].id,"Trevor")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_chat1")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
								If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_chat2",2,null,"chopperPilot",0,PED[ped_trevor].id,"Trevor")
									dia[thisI].flag++
								ENDIF
						
						BREAK
						CASE 3
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_chat3",2,null,"chopperPilot",0,PED[ped_trevor].id,"Trevor")
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 4
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_chat4",2,null,"chopperPilot",0,PED[ped_trevor].id,"Trevor")
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_SCAN_MY_BUDDY								
					SWITCH dia[thisI].flag
						CASE 0
							If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_seefr",2,PED[ped_pilot].id,"chopperPilot",0,PED[ped_trevor].id,"Trevor")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_seefr")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("cs2_seefr")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_FRANKLIN_SCANNED
					SWITCH dia[thisI].flag
						CASE 0
							dia[thisI].intA = GET_GAME_TIMER()+1000
							dia[thisI].flag++
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > dia[thisI].intA
								STOP_AUDIO_SCENE("CAR_2_HELI_CAM_TUTORIAL")
								START_AUDIO_SCENE("CAR_2_SCAN_FRANKLIN")
								If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_teach",1,PED[ped_franklin].id,"Franklin",0,PED[ped_trevor].id,"Trevor")
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF IS_CONV_ROOT_PLAYING("cs2_teach")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 3
							IF NOT IS_CONV_ROOT_PLAYING("cs2_teach")
								STOP_AUDIO_SCENE("CAR_2_SCAN_FRANKLIN")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_GOING_TO_FIND_BUDDY
					
					SWITCH dia[thisI].flag
						CASE 0
							If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_tresp",0,PED[ped_trevor].id,"Trevor")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_tresp")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("cs2_tresp")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_ACTOR_LINE_PLAYS
					SWITCH dia[thisI].flag
						CASE 0
							If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_actor",0,ped[ped_Trevor].id,"Trevor",2,null,"ChopperPilot")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_actor")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("cs2_actor")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_FLYING_AWAY
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_wrong",2,null,"ChopperPilot")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_WAITING_TO_SCAN
					SWITCH dia[thisI].flag
						CASE 0
							dia[thisI].INTA = GET_GAME_TIMER() + 10000
							dia[thisI].flag++
						BREAK
						DEFAULT
							IF GET_GAME_TIMER() > dia[thisI].INTA
								IF NOT IS_PED_INJURED(ped[ped_franklin].id)
									IF NOT IS_ENTITY_ON_SCREEN(ped[ped_franklin].id)
										If CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_noscan",0,ped[ped_Trevor].id,"Trevor")
											dia[thisI].INTA = GET_GAME_TIMER() + 10000
											dia[thisI].flag++
											IF dia[thisI].flag >= 6
												dia[thisI].bCompleted = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_TREVOR_COMMENTS_ON_POSTAL
					If CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(CONVTYPE_BANTER,"cs2_seetheft","cs2_seetheft_2",0,ped[ped_Trevor].id,"Trevor",2,null,"ChopperPilot")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_TREVOR_COMMENTS_ON_PERVERT
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_seeRoof",0,ped[ped_Trevor].id,"Trevor",2,null,"ChopperPilot")
						dia[thisI].bCompleted = TRUE
		
					ENDIF
				BREAK
				
				CASE DIA_TREVOR_MAKES_SCAN_COMPLETE_COMMENT
					IF IS_CONDITION_TRUE(COND_SCANNED_PED_WAS_A_WOMAN)
					//	IF PLAY_AMBIENT_DIALOGUE_LINE(MyLocalPedStruct,ped[ped_Trevor].id,"CST2AUD","CST2_AIAA")
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped[ped_Trevor].id, "CST2_AIAA", "Trevor", SPEECH_PARAMS_FORCE_FRONTEND)
						If missionProgress = STAGE_APPROACH_SCAN_AREA_ONE
							SET_CONDITION_STATE(COND_AREA1_PED_JUST_SCANNED,FALSE,0)
						ELSE
							SET_CONDITION_STATE(COND_AREA3_PED_JUST_SCANNED,FALSE,0)
						ENDIF
						//If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"CS2_scan_wom",0,ped[ped_Trevor].id,"Trevor")
							dia[thisI].INTA++
							IF dia[thisI].INTA = 5
								dia[thisI].bCompleted = TRUE
							ENDIF
						
					ELSE
						//IF PLAY_AMBIENT_DIALOGUE_LINE(MyLocalPedStruct,ped[ped_Trevor].id,"CST2AUD","cCST2_AHAA")
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped[ped_Trevor].id, "CST2_AHAA", "Trevor", SPEECH_PARAMS_FORCE_FRONTEND)
						If missionProgress = STAGE_APPROACH_SCAN_AREA_ONE
							SET_CONDITION_STATE(COND_AREA1_PED_JUST_SCANNED,FALSE,0)
						ELSE
							SET_CONDITION_STATE(COND_AREA3_PED_JUST_SCANNED,FALSE,0)
						ENDIF
						//If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_scanning",0,ped[ped_Trevor].id,"Trevor")
							dia[thisI].INTA++
							IF dia[thisI].INTA = 5
								dia[thisI].bCompleted = TRUE
							ENDIF
						
					ENDIF
				BREAK
				
				CASE DIA_TREVOR_COMMENTS_ON_PROSTITUTE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped[ped_hooker].id,<<187.93819, -160.18086, 55.31756>>,false) < 1.5
						//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(ped[ped_Trevor].id, "CST2_CIAA", "Trevor", SPEECH_PARAMS_FORCE_FRONTEND)
						If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_posie",0,ped[ped_Trevor].id,"Trevor",2,null,"ChopperPilot")
							dia[thisI].bCompleted = TRUE
						ENDIF
					ELSE
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_TREVOR_COMMENTS_ON_CHADGIRL
					If CREATE_CONVERSATION_EXTRA(CONVTYPE_BANTER,"cs2_lookChad",0,ped[ped_Trevor].id,"Trevor")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				// scan area 3
				CASE DIA_SEEN_CHAD
					SWITCH dia[thisI].flag
						CASE 0
							dia[thisI].intA = GET_GAME_TIMER()+1800
							dia[thisI].flag++
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > dia[thisI].intA
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_seeChad",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("CS2_seeChad")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_CHAD_HIDDEN
					KILL_FACE_TO_FACE_CONVERSATION()
					if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_nochad",0,ped[ped_Trevor].id,"Trevor",2,ped[ped_pilot].id,"ChopperPilot")				
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_CHAD_WALKING_THROUGH_APARTMENTS
					if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_seeChad2",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_CHAD_OFF_SCREEN
					KILL_FACE_TO_FACE_CONVERSATION()
					if PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_tmp36","CS2_tmp36_4",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")
						dia[thisI].bCompleted = TRUE
					ENDIF						
				BREAK
				
				CASE DIA_FRANKLIN_FIND_HIM
					IF IS_CONDITION_TRUE(COND_CHAD_OFFSCREEN_FOR_4_SECONDS)
						if PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_find","CS2_find_5",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")
							dia[thisI].bCompleted = TRUE
						ENDIF
					ELSE
						//cancel playing as Chad was found during the previous conversation
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_FOUND_CHAD
					if PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_found","CS2_found_3",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_TREVOR_BANTER_DURING_CHAD_WALK
					SWITCH dia[thisI].flag
						CASE 0
							dia[thisI].intA = GET_GAME_TIMER() + 2000
							dia[thisI].flag++
						BREAK
						CASE 1
							IF GET_GAME_TIMER() < dia[thisI].intA				
								IF listenDialogue = -1
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
										if CREATE_CONVERSATION_EXTRA(CONVTYPE_UNIMPORTANT,"cs2_ChadView",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")
											dia[thisI].bCompleted = TRUE
										ENDIF
										
									ENDIF
								ENDIF
							ELSE
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_CHAD_AT_GARAGE
					if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_seeChad3",1,null,"Franklin",0,ped[ped_Trevor].id,"Trevor")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				// ================ car park dialogue ===========================
				CASE DIA_TAKE_US_DOWN
					SWITCH dia[thisI].flag
						CASE 0
							dia[thisI].intA = GET_GAME_TIMER() + 1700
							dia[thisI].flag++
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > dia[thisI].intA
								if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_flylow",0,ped[ped_Trevor].id,"Trevor")
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
						CASE 2
							//this below plays elsewhere
							//IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_carpark2",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
							//IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cp2c",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
								dia[thisI].bCompleted = TRUE
							//ENDIF
						BREAK
					ENDSWITCH
					
				BREAK
				
				CASE DIA_TREVOR_SCARED
					if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_down",0,ped[ped_Trevor].id,"Trevor",2,null,"chopperPilot")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_EXITS_CLEAR
					if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_clear",0,ped[ped_Trevor].id,"Trevor",2,null,"chopperPilot")	
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK

				CASE DIA_THERMAL_VISION //CS2_Carpark3
					if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_Carpark3",0,ped[ped_Trevor].id,"Trevor",2,null,"ChopperPilot")
						dia[thisI].bCompleted = TRUE
					ENDIF				
				BREAK
				
				CASE DIA_TREVOR_REACTION_TO_THERMAL_VISION_ON //CS2_carpark4
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_Carpark4",0,ped[ped_Trevor].id,"Trevor")
							dia[thisI].bCompleted = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE DIA_FRANKLIN_ASKS_IF_PLAYER_CAN_SEE_HIM//cs2_onme
				//	IF NOT IS_MESSAGE_BEING_DISPLAYED()
						if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_onme",1,ped[ped_franklin].id,"Franklin")
							dia[thisI].bCompleted = TRUE
						ENDIF
				//	ENDIF
				BREAK
				
				CASE DIA_TREVOR_SEE_FRANKLIN
					//CLEAR_PRINTS()
					if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_seen",0,ped[ped_Trevor].id,"Trevor")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_ANY_OTHER_HEAT_SOURCES //cs2_looknow
					SWITCH dia[thisI].flag
						CASE 0
							if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_looknow",1,ped[ped_franklin].id,"Franklin")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_looknow")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_CONV_ROOT_PLAYING("cs2_looknow")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_TREVOR_SEES_PISSER //cs2_mana1
				
					SWITCH dia[thisI].flag
						CASE 0
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS32c")
								CLEAR_PRINTS()
							ENDIF
							SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_manb1",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor") //that's the motherfucker.
								dia[thisI].flag++								
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_manb1")
								IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 1
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
								
				CASE DIA_TREVOR_SEES_CHAD //cs2_manc1
					IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS32c")
						CLEAR_PRINTS()
					ENDIF
					SWITCH dia[thisI].flag
						CASE 0
				//	IF actions[20].flag > 0
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_manc1",0,ped[ped_trevor].id,"Trevor",1,ped[ped_franklin].id,"Franklin")																			
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("CS2_manc1")
								IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 1
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				//	ENDIF
				BREAK
				
				CASE DIA_TREVOR_RANTS_IN_SWITCH
					SWITCH dia[thisI].flag
						CASE 0
							
							//ADD_PED_FOR_DIALOGUE_EXTRA(0,null,"Trevor")
							//ADD_PED_FOR_DIALOGUE_EXTRA(1,null,"Franklin")
							//IF PRELOAD_CONVERSATION(MyLocalPedStruct, "CST2AUD","cs2_IG15",CONV_PRIORITY_MEDIUM)
							//	BEGIN_PRELOADED_CONVERSATION()								
							//	dia[thisI].intA = GET_GAME_TIMER() + 200
							//	dia[thisI].flag++
							//ENDIF
						
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_IG15","cs2_IG15_1",0,null,"Trevor",1,null,"Franklin")
								dia[thisI].intA = GET_GAME_TIMER() + 200
							//	dia[thisI].flag++
								dia[thisI].flag = 1						
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_IG15")
								IF GET_GAME_TIMER() > dia[thisI].intA
							//	IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() = 1
									dia[thisI].bCompleted = TRUE
							//	ENDIF
								ENDIF
							ENDIF
								//ENDIF
							//	KILL_FACE_TO_FACE_CONVERSATION()
								
						//	ENDIF
						BREAK
						//CASE 1
						//	IF GET_GAME_TIMER() > dia[thisI].intA
						//		KILL_FACE_TO_FACE_CONVERSATION()
						//		dia[thisI].bCompleted = TRUE
						//	ENDIF
						//BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_PLAYER_SHOOTING_CHOPPER
					IF GET_GAME_TIMER() > dia[thisI].intA
						IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_franksh",2,ped[ped_pilot].id,"chopperPilot")
							dia[thisI].intA = GET_GAME_TIMER() + 10000
							dia[thisI].flag++
							IF dia[thisI].flag = 3
								dia[thisI].bCompleted = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE DIA_DRIVING_BACK
	
					SWITCH dia[thisI].flag
						CASE 0
							//cprintln(debug_Trevor3,"playing")
							//cs2_radio
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS53")
								dia[thisI].flag++
							ENDIF
								
						BREAK
						CASE 1
							IF NOT IS_CONDITION_TRUE(COND_WANTED)
								dia[thisI].flag++
							ELSE
								FORCE_DIALOGUE_STATE(5,DIA_WARNING_COPS) //stop getting wanted level if already have one
							ENDIF
						BREAK
						CASE 2
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_franwh",1,player_ped_id(),"Franklin") //that's the motherfucker.
								dia[thisI].flag++								
							ENDIF							
						BREAK
						
						CASE 3
						
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_radio",0,null,"Trevor",1,player_ped_id(),"Franklin") //that's the motherfucker.
									dia[thisI].flag++							
								ENDIF
						
						BREAK
						CASE 4
							IF IS_CONV_ROOT_PLAYING("cs2_radio")
								dia[thisI].flag++	
							ENDIF
						BREAK
						CASE 5
							IF NOT IS_CONV_ROOT_PLAYING("cs2_radio")
								dia[thisI].bCompleted = TRUE	
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				//cs2_crash1
				CASE DIA_CRASH_ZTYPE
					SWITCH dia[thisI].flag
						CASE 0
							IF IS_CONV_ROOT_PLAYING("cs2_radio")
								//PAUSE_SCRIPTED_CONVERSATION(TRUE)
								crashrestartLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								cprintln(debug_trevor3,"kill conv L ")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								SET_BIT(dia[thisI].intA,0)
								dia[thisI].flag=1								
							ELSE
								CLEAR_BIT(dia[thisI].intA,0)
								dia[thisI].flag=1
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_BIT_SET(dia[thisI].intA,1)
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_crash1",0,null,"Trevor",1,ped[ped_franklin].id,"Franklin") //that's the motherfucker.
									SET_BIT(dia[thisI].intA,1)
									dia[thisI].flag=2
								ENDIF
							ELIF NOT IS_BIT_SET(dia[thisI].intA,2)
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_crash2",0,null,"Trevor",1,ped[ped_franklin].id,"Franklin") //that's the motherfucker.
									SET_BIT(dia[thisI].intA,2)
									dia[thisI].flag=2									
								ENDIF
							ELIF NOT IS_BIT_SET(dia[thisI].intA,3)
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_crash3",0,null,"Trevor",1,ped[ped_franklin].id,"Franklin") //that's the motherfucker.
									SET_BIT(dia[thisI].intA,3)
									dia[thisI].flag=2									
								ENDIF
							ENDIF
						BREAK
						CASE 2
							
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF IS_BIT_SET(dia[thisI].intA,0)
								and not IS_STRING_NULL_OR_EMPTY(crashrestartLine)
									IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(CONVTYPE_GAMEPLAY,"cs2_radio",crashrestartLine,0,null,"Trevor",1,player_ped_id(),"Franklin") //that's the motherfucker.
										CLEAR_BIT(dia[thisI].intA,0)
										dia[thisI].flag=0
										SET_CONDITION_STATE(COND_DAMAGED_ZTYPE,FALSE,0)							
									ENDIF								
								ELSE
									IF NOT IS_BIT_SET(dia[thisI].intA,3)
										dia[thisI].flag=0
										SET_CONDITION_STATE(COND_DAMAGED_ZTYPE,FALSE,0)
									ELSE
										dia[thisI].bCompleted = TRUE
									ENDIF
								ENDIF
								
								
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_WHERES_THE_CAR
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_where",7,ped[ped_cop2].id,"cs2_cop1")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_TREVOR_SEES_FIXING_MAN
				
					SWITCH dia[thisI].flag
						CASE 0
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS32c")
								CLEAR_PRINTS()
							ENDIF
							SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_mana1",0,ped[ped_trevor].id,"Trevor",1,ped[ped_franklin].id,"Franklin") //that's the motherfucker.
								dia[thisI].flag++								
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_mana1")
								IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 1
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_TREVOR_SEES_PHONE_CAR_MAN
					SWITCH dia[thisI].flag
						CASE 0
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS32c")
								CLEAR_PRINTS()
							ENDIF
							SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_mane1",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor") //that's the motherfucker.
								dia[thisI].flag++								
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_mane1")
								IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 1
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DIA_TREVOR_SEES_LEANING_MAN
					SWITCH dia[thisI].flag
						CASE 0
							IF IS_THIS_PRINT_BEING_DISPLAYED("CH_INS32c")
								CLEAR_PRINTS()
							ENDIF
							SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_manb1b",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor") //that's the motherfucker.
								dia[thisI].flag++								
							ENDIF
						BREAK
						CASE 1
							IF IS_CONV_ROOT_PLAYING("cs2_manb1b")
								IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 1
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				
				CASE DIA_FRANKLIN_SEES_PISSER //cs2_mana3
					//SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_mana3",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor")
						SET_CONDITION_STATE(COND_BLOCK_LISTENING,FALSE)
				
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_FRANKLIN_SEES_FIXING_MAN
					//SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_mand2b",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor") //fuck you, like you couldn't see
						SET_CONDITION_STATE(COND_BLOCK_LISTENING,FALSE)
					
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_FRANKLIN_SEES_ZTYPE
					//SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_manc2",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor") //that's the motherfucker.
						PLAY_MUSIC(mus_Chad_found,mus_get_car_to_objective)
						SET_CONDITION_STATE(COND_BLOCK_LISTENING,FALSE)						
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_FRANKLIN_SEES_LEANING_MAN
					//SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_manb2b",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor") //that's the motherfucker.
						//SET_CONDITION_STATE(COND_BLOCK_LISTENING,FALSE)
					
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_FRANKLIN_SEES_PHONE_MAN
					//SET_CONDITION_STATE(COND_BLOCK_LISTENING,TRUE)
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_mane2",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor") //that's the motherfucker.
						SET_CONDITION_STATE(COND_BLOCK_LISTENING,FALSE)
					
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				
				
				CASE DIA_OVERHEAR_FUCKERS_WITH_SCANNER
					
					IF (listenDialogue = 0
					OR dia[thisI].flag = 0)
					AND IS_CONDITION_FALSE(COND_BLOCK_LISTENING)
						SWITCH dia[thisI].flag
							CASE 0 //grunt
								ADD_CHOPPER_LISTENING_LOCATION(0,<< -1260.095, -244.781, 51.224 >>)
								dia[thisI].flag++
							BREAK
							CASE 1
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"lisFuckb",5,ped[ped_wrong_fucker].id,"cs2_carfuck_man",6,ped[ped_chadGirl].id,"cs2_carfuck_woman") //that's the motherfucker.								
									dia[thisI].flag++
								ENDIF
							BREAK
							CASE 2 //grunt
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"lisFuckb",5,ped[ped_wrong_fucker].id,"cs2_carfuck_man",6,ped[ped_chadGirl].id,"cs2_carfuck_woman") //that's the motherfucker.
									IF dia[thisI].intA < 4
										dia[thisI].flag++
									ENDIF
								ENDIF
							BREAK
							CASE 3 //man sex comment
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"lisFuck",5,ped[ped_wrong_fucker].id,"cs2_carfuck_man") //that's the motherfucker.
									dia[thisI].intA++
									dia[thisI].flag=1
								ENDIF
							BREAK
						ENDSWITCH
					ELSE
						IF IS_CONV_ROOT_PLAYING("lisFuckb")
						OR IS_CONV_ROOT_PLAYING("lisFuck")
							cprintln(debug_trevor3,"kill conv M ")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF					
				BREAK
				
				CASE DIA_OVERHEAR_CAR_FIXER_WITH_SCANNER
					IF (listenDialogue = 0
					OR dia[thisI].flag = 0)					
					AND IS_CONDITION_FALSE(COND_BLOCK_LISTENING)
						SWITCH dia[thisI].flag
							CASE 0 //grunt
								ADD_CHOPPER_LISTENING_LOCATION(0,<< -1260.095, -244.781, 51.224 >>)
								dia[thisI].flag++								
							BREAK
							CASE 1
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"lisCarFix",6,ped[ped_wrong_fucker].id,"cs2_carfix") //that's the motherfucker.									
										
										dia[thisI].bCompleted = TRUE
								
								ENDIF
							BREAK
						ENDSWITCH
					ELSE
						IF IS_CONV_ROOT_PLAYING("lisCarFix")
							IF currentConvType = CONVTYPE_CHOPPER_CAM	
								cprintln(debug_trevor3,"kill conv N ")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							
							ENDIF
						ENDIF
					ENDIF								
				BREAK
				
				CASE DIA_OVERHEAR_CAR_LEANING_GUY
					IF (listenDialogue = 3
					OR dia[thisI].flag = 0)
					AND IS_CONDITION_FALSE(COND_BLOCK_LISTENING)
						SWITCH dia[thisI].flag
							CASE 0 //grunt
								ADD_CHOPPER_LISTENING_LOCATION(3,<<-1295.2588, -185.2966, 51.7873>>)
								dia[thisI].flag++								
							BREAK
							CASE 1								
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"lisManLean",6,ped[ped_wrong_lean].id,"cs2_carlean") //that's the motherfucker.									
									dia[thisI].flag++								
								ENDIF
							BREAK		
							CASE 2
								IF dia[thisI].intA != 0									
									TEXT_LABEL_23 txt
									txt = "lisManLean"
									txt += dia[thisI].intA
									IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(CONVTYPE_CHOPPER_CAM,"lisManLean",txt,6,ped[ped_wrong_lean].id,"cs2_carlean")
										dia[thisI].flag=2
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELSE
						//IF IS_CONV_ROOT_PLAYING("lisManLean")
							//IF currentConvType = CONVTYPE_CHOPPER_CAM
							//	dia[thisI].intA = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
							//	dia[thisI].flag = 3
							//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							//ENDIF
						//ENDIF
					ENDIF	
				BREAK
				
				CASE DIA_OVERHEAR_CAR_ON_PHONE
					IF (listenDialogue = 4
					OR dia[thisI].flag = 0)
					AND IS_CONDITION_FALSE(COND_BLOCK_LISTENING)
						SWITCH dia[thisI].flag
							CASE 0 //grunt
								ADD_CHOPPER_LISTENING_LOCATION(4,<<-1260.2112, -224.6728, 51.5658>>)
								dia[thisI].flag=1								
							BREAK
							CASE 1				
								IF IS_DIALOGUE_COMPLETE(22,DIA_FRANKLIN_SEES_PHONE_MAN)
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"lisOnPhn",6,ped[ped_wrong_phone].id,"cs2_carphn") //that's the motherfucker.																
										dia[thisI].flag=2
									ENDIF
								ENDIF
							BREAK
							CASE 2
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									dia[thisI].flag=3
								ENDIF
							BREAK
							CASE 3							
								IF dia[thisI].intA != 0									
									TEXT_LABEL_23 txt
									txt = "lisOnPhn_"
									txt += dia[thisI].intA
									IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(CONVTYPE_CHOPPER_CAM,"lisOnPhn",txt,6,ped[ped_wrong_phone].id,"cs2_carphn")
										dia[thisI].flag=2
									ENDIF
								ELSE
									dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,5000)
									dia[thisI].flag=4
								ENDIF
							BREAK
							CASE 4
								IF GET_GAME_TIMER() > dia[thisI].intA
									IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"lisOnPhnB",6,ped[ped_wrong_phone].id,"cs2_carphn") //that's the motherfucker.																
										dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,5000)								
									ENDIF
								ENDIF
							BREAK
								
						ENDSWITCH
					ELSE
						IF IS_CONV_ROOT_PLAYING("lisOnPhn")						
							IF currentConvType = CONVTYPE_CHOPPER_CAM
								dia[thisI].intA = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
								dia[thisI].flag = 3
								cprintln(debug_trevor3,"kill conv A ")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE DIA_OVERHEAR_PISSER_WITH_SCANNER
					IF (listenDialogue = 1
					OR dia[thisI].flag = 0)
					AND (IS_CONDITION_FALSE(COND_BLOCK_LISTENING) OR (IS_CONDITION_TRUE(COND_BLOCK_LISTENING) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND GET_DISTANCE_BETWEEN_ENTITIES(ped[ped_franklin].id,ped[ped_wrong_wanker].id) > 15))
						SWITCH dia[thisI].flag
							CASE 0 //grunt
								ADD_CHOPPER_LISTENING_LOCATION(1,<< -1270.3787, -210.2109, 50.5499 >>)
								dia[thisI].flag++
							BREAK
							CASE 1
								IF NOT IS_PED_INJURED(ped[ped_wrong_wanker].id)
									IF IS_ENTITY_PLAYING_ANIM(ped[ped_wrong_wanker].id,"misscarsteal2peeing","peeing_intro")
									OR IS_ENTITY_PLAYING_ANIM(ped[ped_wrong_wanker].id,"misscarsteal2peeing","peeing_loop")
										dia[thisI].flag++
									ENDIF
								ENDIF
							BREAK
							CASE 2 //grunt
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"lisManPeeing",7,ped[ped_wrong_wanker].id,"cs2_pissing_man") //that's the motherfucker.									
									dia[thisI].flag++									
									dia[thisI].bCompleted = TRUE
								ENDIF
							BREAK							
						ENDSWITCH
					ELSE
						IF IS_CONV_ROOT_PLAYING("lisManPeeing")
							IF currentConvType = CONVTYPE_CHOPPER_CAM
								IF listenDialogue != 1
									cprintln(debug_trevor3,"kill conv O ")
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
							ENDIF						
						ENDIF
					ENDIF			
				BREAK
				
				CASE DIA_OVERHEAR_CHAD_WITH_SCANNER
					IF (listenDialogue = 2
					OR dia[thisI].flag = 0)
					AND IS_CONDITION_FALSE(COND_BLOCK_LISTENING)
						SWITCH dia[thisI].flag
							CASE 0 //grunt
								ADD_CHOPPER_LISTENING_LOCATION(2,<< -1309.563, -215.238, 50.993 >>)
								dia[thisI].flag++
							BREAK
							CASE 1
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_CHOPPER_CAM,"LisChadWhimp",3,ped[ped_chad].id,"Chad")								
									dia[thisI].flag++									
									dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
								ENDIF
							BREAK
							CASE 2 //grunt
								IF GET_GAME_TIMER() > dia[thisI].intA
									dia[thisI].flag = 1
								ENDIF
							BREAK							
						ENDSWITCH
					ELSE
						IF IS_CONV_ROOT_PLAYING("LisChadWhimp")
							IF currentConvType = CONVTYPE_CHOPPER_CAM
								cprintln(debug_trevor3,"kill conv P ")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
						ENDIF
					ENDIF			
				BREAK
				
				// get car in car park
				CASE DIA_BEG_TO_LET_GO
					switch dia[thisI].flag
						case 0
							KILL_FACE_TO_FACE_CONVERSATION_EXTRA(FALSE)
							
							dia[thisI].flag++
						break
						case 1
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"gunAtChad",3,ped[ped_chad].id,"Chad")
								dia[thisI].flag++
							ENDIF
						break
						case 2
				
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								dia[thisI].bCompleted = TRUE
							ENDIF
						break
					endswitch
				BREAK
				CASE DIA_DONT_TAKE_CAR
					switch dia[thisI].flag
						case 0
						//	IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_gotChad",3,ped[ped_chad].id,"Chad")
								dia[thisI].flag++
						//	ENDIF
						BREAK
						CASE 1
							IF IS_CONDITION_TRUE(COND_PLAYER_GOT_IN_CAR)
								dia[thisI].bCompleted = TRUE
							ELSE
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_gotChad1",3,ped[ped_chad].id,"Chad")
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE DIA_FLEE_PANIC
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						switch dia[thisI].flag
							case 0
								cprintln(debug_trevor3,"kill conv Q ")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								FORCE_dIALOGUE_STATE(13,DIA_FLEE_PANIC_NOT_HIT,TRUE)
								dia[thisI].flag++
							BREAK
							CASE 1
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"chadfear",3,ped[ped_chad].id,"Chad")
									dia[thisI].bCompleted = TRUE
								ENDIF
							BREAK
						ENDSWITCH		
					ENDIF
				BREAK
				CASE DIA_FLEE_PANIC_NOT_HIT
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						switch dia[thisI].flag
							case 0
								cprintln(debug_trevor3,"kill conv R ")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								FORCE_dIALOGUE_STATE(2,DIA_FLEE_PANIC,TRUE)
								dia[thisI].flag++
							BREAK
							CASE 1
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"chadbad",3,ped[ped_chad].id,"Chad")
									dia[thisI].bCompleted = TRUE
								ENDIF
							BREAK
						ENDSWITCH		
					ENDIF
				BREAK
				CASE DIA_PLAYER_GOT_IN_CAR
					switch dia[thisI].flag
						case 0
							KILL_FACE_TO_FACE_CONVERSATION_EXTRA(FALSE)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								dia[thisI].flag++
							ENDIF
						break
						case 1
							IF NOT IS_PED_INJURED(ped[ped_chad].id)
								IF IS_DIALOGUE_COMPLETE(1,DIA_BEG_TO_LET_GO)
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_tmp53","cs2_tmp53_2",3,ped[ped_chad].id,"Chad")
									//IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_tmp53",3,ped[ped_chad].id,"Chad")
										dia[thisI].flag++
									ENDIF
								ELSE
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_tmp53","cs2_tmp53_1",3,ped[ped_chad].id,"Chad")
										dia[thisI].flag++
									ENDIF
								ENDIF
							ELSE
								dia[thisI].flag++
							ENDIF
						break
						case 2
				
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								dia[thisI].bCompleted = TRUE
							ENDIF
						break
					endswitch
				BREAK
				
				CASE DIA_DRIVING_AWAY
					
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_chad3",0,ped[ped_trevor].id,"Trevor",1,ped[ped_franklin].id,"Franklin")
						dia[thisI].bCompleted = TRUE
					ENDIF					
				BREAK
				
				CASE DIA_WARNING_COPS
					switch dia[thisI].flag
						case 0
							IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"trvcops",0,ped[ped_trevor].id,"Trevor",1,ped[ped_franklin].id,"Franklin")
								SET_MAX_WANTED_LEVEL(5)
								SET_PLAYER_WANTED_LEVEL(player_id(),2)
								SET_PLAYER_WANTED_LEVEL_NOW(player_id())
								
								dia[thisI].flag++
							ENDIF
						break
						case 1
							IF NOT IS_CONV_ROOT_PLAYING("trvcops")
								PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_CAR_STEAL_2_01", 0.0)
								dia[thisI].bCompleted = TRUE
							ENDIF
						break
					endswitch
				BREAK
				
				CASE DIA_KILLED_CHAD
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"trvkill",0,ped[ped_trevor].id,"Trevor",1,ped[ped_franklin].id,"Franklin")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_CALL_DEVIN	
				
							if PLAYER_CALL_CHAR_CELLPHONE_EXTRA("cs2_call",1,player_ped_id(),"Franklin",2,null,"Molly")													
								dia[thisI].bCompleted = TRUE
							ENDIF
						
			
					/*
					SWITCH dia[thisI].flag
						case 0							
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2442.55, 4967.51, 54.35>>) < 1300
							OR IS_CALLING_CONTACT(CHAR_ONEIL)
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2442.55, 4967.51, 54.35>>) > 500
								//If IS_MESSAGE_BEING_DISPLAYED()							
									events[iE].flag++																	
								ENDIF
							ENDIF
						BREAK
						case 1
							//If not IS_MESSAGE_BEING_DISPLAYED()		
								//need to set up speakers for this
								//////cprintln(DEBUG_TREVOR3,"LOADING PHONE CALL")
								if PLAYER_CALL_CHAR_CELLPHONE_EXTRA("cs2_call",1,player_ped_id(),"Franklin",2,null,"Devin")								
									//////cprintln(DEBUG_TREVOR3,"PHONE CALL PLAYING")
									events[iE].active = FALSE
								ENDIF
							//ENDIF
						BREAK*/
				BREAK
				
				CASE DIA_OPEN_GATES
					SWITCH dia[thisI].flag
						CASE 0
							IF IS_CONDITION_TRUE(COND_PLAYER_IN_ZTYPE)
								IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_gate",1,ped[ped_franklin].id,"Franklin",7,ped[ped_cop1].id,"cs2_cop1")
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_CONV_ROOT_PLAYING("cs2_gate")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH						
				BREAK
					
				CASE DIA_FRANKLIN_THREATENS_CHAD
					IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_franch",1,ped[ped_franklin].id,"Franklin")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE DIA_PLAYER_RUNS_OFF
					SWITCH dia[thisI].flag
						CASE 0
							cprintln(debug_trevor3,"kill conv S ")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()			
							 dia[thisI].flag++
						BREAK
						CASE 1
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_gotChad1","CS2_gotChad1_11",3,ped[ped_chad].id,"Chad")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			
			ENDSWITCH			
		ENDIF
	ENDIF
	
	
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		currentConv = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	ELSE
		IF NOT bLastConvoWithoutSubtitles
			lastConv = currentConv
		ENDIF
	ENDIF
ENDPROC


PROC DIALOGUE_playout(int thisI, enumDialogue thisDia, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL)
	IF dia[thisI].dial != thisDia
		dia[thisI].bCompleted = FALSE
		dia[thisI].dial = thisDia
	ENDIF		
	
	IF NOT dia[thisI].bCompleted
	//AND NOT bLastConvoWithoutSubtitles
		
		IF dia[thisI].bStarted
			DIALOGUE(thisI, thisDia)
		ELSE
			DIALOGUE(thisI,thisDia,andOr1,cond1,andOr2,cond2,andOr3,cond3,andOr4,cond4)
		ENDIF
	ENDIF
endproc

PROC DIALOGUE_ON_ACTION(int thisI, enumDialogue thisDia, int actEntry, enumActions checkAction)
	IF dia[thisI].dial != thisDia
		dia[thisI].bCompleted = FALSE
		dia[thisI].dial = thisDia
	ENDIF
	
	IF actions[actEntry].action = checkAction
		IF NOT dia[thisI].bCompleted
			IF actions[actEntry].completed			
				DIALOGUE(thisI,thisDia)				
			ENDIF
		ENDIF
	ELSE
		IF actions[actEntry].action != ACT_NULL
			TEXT_LABEL_63 txt
			txt = ""
			txt += "DIALOGUE_ON_ACTION():"
			txt += actEntry		
			SCRIPT_ASSERT(txt)
		ENDIF
	ENDIF
ENDPROC

PROC DIALOGUE_ON_DIALOGUE(int thisI, enumDialogue thisDia, int diaEntry, enumDialogue checkDia, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL)
	IF dia[thisI].dial != thisDia
		dia[thisI].bCompleted = FALSE
		dia[thisI].dial = thisDia
	ENDIF
	
	IF NOT dia[thisI].bCompleted
		IF HAS_DIALOGUE_FINISHED(diaEntry,checkDia)
			DIALOGUE(thisI,thisDia,andOr1,cond1,andOr2,cond2,andOr3,cond3)
		ENDIF
	ENDIF
ENDPROC

// ===================================================== FAILS =====================================



PROC SET_MISSION_TO_FAIL(enumFails thisFailReason, string failText, bool delayFail=FALSE)
	//cprintln(debug_trevor3,"Fail condition: ",GET_FAIL_STRING(thisFailReason))
	IF NOT IS_CONV_ROOT_PLAYING("cs2_cop5")		
		KILL_FACE_TO_FACE_CONVERSATION_EXTRA(false)
		
	ENDIF
	IF NOT bDelayFail
		missionFailing = thisFailReason
		sFailText = failText
		bDelayFail = delayFail
	ENDIF
ENDPROC



PROC FAIL(enumFails thisFailToCheck)
	INTERIOR_INSTANCE_INDEX thisInt
	
	
	//cprintln(debug_trevor3,"FAIL: ",enum_to_int(thisFailToCheck))
	
	SWITCH thisFailToCheck
		CASE FAIL_PLAYER_HAS_WEAPON_DRAWN_IN_POL_DEPT
			thisInt = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 442.1297, -982.7592, 31.4630 >>, "v_policehub")
			IF IS_VALID_INTERIOR(thisInt)
				IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) = thisInt
					weapon_type playerWep
					playerWep=WEAPONTYPE_UNARMED
					IF GET_CURRENT_PED_WEAPON(player_ped_id(),playerWep)						
						IF playerWep != WEAPONTYPE_UNARMED						
							IF NOT IS_PHONE_ONSCREEN()
				
								IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_EXTSTRS_ROOM001")
								AND GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) != GET_HASH_KEY("PH_EXTSTRS_ROOM002")								
									SET_WANTED_LEVEL_MULTIPLIER(1.0)
									SET_MAX_WANTED_LEVEL(5)
									SET_PLAYER_WANTED_LEVEL(player_id(),3)
									SET_PLAYER_WANTED_LEVEL_NOW(player_id())
									cprintln(debug_trevor3,"FAIL_PLAYER_HAS_WEAPON_DRAWN_IN_POL_DEPT A")
									SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F15",TRUE)								
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_HAS_WANTED_LEVEL_IN_POLICE_DEPT
			IF GET_PLAYER_WANTED_LEVEL(player_id()) != 0
				
				thisInt = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 442.1297, -982.7592, 31.4630 >>, "v_policehub")
				IF IS_VALID_INTERIOR(thisInt)
					IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) = thisInt
						IF failDelay = 0
							failDelay = GET_GAME_TIMER() + 2700
							failReason = FAIL_HAS_WANTED_LEVEL_IN_POLICE_DEPT													
							
						ENDIf
					ENDIF
				ENDIF
			ENDIF
			
			IF failReason = FAIL_HAS_WANTED_LEVEL_IN_POLICE_DEPT
				IF GET_GAME_TIMER() >  failDelay
					
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F13",TRUE)
				ENDIF
			ENDIF
		BREAK
		
			
		
		CASE FAIL_COP_DIES
			IF DOES_ENTITY_EXIST(ped[ped_cop1].id)
				IF IS_PED_INJURED(ped[ped_cop1].id)
					IF NOT IS_CONDITION_TRUE(COND_COP_IN_STAIRWELL)
						SET_WANTED_LEVEL_MULTIPLIER(1.0)
								SET_MAX_WANTED_LEVEL(5)
								SET_PLAYER_WANTED_LEVEL(player_id(),3)
								SET_PLAYER_WANTED_LEVEL_NOW(player_id())
						
						SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F13",TRUE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_PLAYER_SHOOTING_NEAR_POLICE_DEPT
			IF IS_PED_SHOOTING(player_ped_id())
				IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<443.060303,-943.353638,12.574972>>, <<441.947815,-1061.535400,55.027191>>, 130.812500)				
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					SET_PLAYER_WANTED_LEVEL(player_id(),3)
					SET_PLAYER_WANTED_LEVEL_NOW(player_id())
				
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F13",TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_PLAYER_CREATED_DISTURBANCE_NEAR_POL_DEPT
			//explosions
			IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,<<443.060303,-943.353638,12.574972>>, <<441.947815,-1061.535400,55.027191>>, 130.812500)
				entity_index aEnt
				aEnt = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,<<443.060303,-943.353638,12.574972>>, <<441.947815,-1061.535400,55.027191>>, 130.812500)
				
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(aEnt) = PLAYER_PED_ID()
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					SET_PLAYER_WANTED_LEVEL(player_id(),3)

					SET_PLAYER_WANTED_LEVEL_NOW(player_id())				
					
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F13",TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_COPS_IN_COMBAT

			
			GET_PED_NEARBY_PEDS(player_ped_id(),pedList)
			int iJ
			REPEAT COUNT_OF(pedList) ij
				IF DOES_ENTITY_EXIST(pedList[ij])
					IF NOT IS_PED_INJURED(pedList[ij])
						IF IS_PED_IN_COMBAT(pedList[ij],player_ped_id())
						OR IS_PED_RAGDOLL(pedList[ij])
							IF GET_INTERIOR_FROM_ENTITY(pedList[ij]) = int_police						
				
								SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F13",TRUE)
							ENDIF
						ENDIF
					ELSE
						IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) = int_police
		
							SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F13",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		
		CASE FAIL_CHOPPER_DESTROYED
			IF DOES_ENTITY_EXIST(vehicle[vehChopper].id)
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F01",TRUE)					
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_PLAYER_WANDERS_AWAY_FROM_POLICE_DEPT
			if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<< 422,-978,30 >>) > 100.0
			and (NOT IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id) OR (IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id) and not IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)))
				SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F14")
			ENDIF			
		BREAK
		
		CASE FAIL_PILOT_FLEW_AWAY
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
				IF NOT IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)
					IF IS_ENTITY_IN_ANGLED_AREA( vehicle[vehChopper].id, <<476.378693,-987.339050,45.758247>>, <<430.220062,-986.779541,79.070145>>, 31.125000)						
						SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F18")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_ZTYPE_DAMAGED
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
				IF GET_ENTITY_HEALTH(vehicle[vehHeist].id) < 820
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F07")
				ENDIF
				IF IS_VEHICLE_DOOR_DAMAGED(vehicle[vehHeist].id,SC_DOOR_FRONT_LEFT)
				OR IS_VEHICLE_DOOR_DAMAGED(vehicle[vehHeist].id,SC_DOOR_FRONT_RIGHT)
				OR IS_VEHICLE_DOOR_DAMAGED(vehicle[vehHeist].id,SC_DOOR_REAR_LEFT)
				OR IS_VEHICLE_DOOR_DAMAGED(vehicle[vehHeist].id,SC_DOOR_REAR_RIGHT)
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F07")
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_PLAYER_LEAVES_ZTYPE
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)								
				IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),vehicle[vehHeist].id) > 150					
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F08") //ztype was lost
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_CHAD_ESCAPES_WITH_ZTYPE
			IF IS_CONDITION_TRUE(COND_CHAD_ESCAPED_IN_ZTYPE)
				SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F20")
			ENDIF
		BREAK
		
		CASE FAIL_FLEW_TOO_FAR_AWAY
			IF IS_CONDITION_TRUE(COND_FLEW_IN_TO_FAIL_ZONE)
				SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F19")
			ENDIF
		BREAK
		
		CASE fail_devins_plane_destroyed
			
			IF DOES_ENTITY_EXIST(vehicle[vehJet].id)
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F21")
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle[vehJet].id,player_ped_id())
						SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F23")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE fail_attacked_devins_entourage
			IF (DOES_ENTITY_EXIST(ped[ped_cop2].id) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped[ped_cop2].id,player_ped_id()))
			OR (DOES_ENTITY_EXIST(ped[ped_cop3].id) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped[ped_cop3].id,player_ped_id()))
			OR (DOES_ENTITY_EXIST(ped[ped_cop4].id) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped[ped_cop4].id,player_ped_id()))
			OR (DOES_ENTITY_EXIST(ped[ped_civ1].id) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped[ped_civ1].id,player_ped_id()))
				SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F24")
			ENDIF
		BREAK
		
		CASE FAIL_ZTYPE_STUCK
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
				IF IS_VEHICLE_STUCK_TIMER_UP(vehicle[vehHeist].id, VEH_STUCK_JAMMED, JAMMED_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehicle[vehHeist].id, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehicle[vehHeist].id, VEH_STUCK_ON_ROOF, ROOF_TIME)
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F12")		
				ENDIF		
			ENDIF
		BREAK
		
		CASE FAIL_DEVIN_KILLED
			IF DOES_ENTITY_EXIST(pedDevin)
				IF IS_PED_INJURED(pedDevin)
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F21")
				ENDIF
			ENDIF
		BREAK
		
		CASE FAIL_COPS_AT_DEVIN
			IF IS_CONDITION_TRUE(COND_ZTYPE_IN_HANGAR)
				IF GET_PLAYER_WANTED_LEVEL(player_id()) > 0
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F25")
				ENDIF
			ENDIF
		BREAK
		
		
		CASE FAIL_DEVIN_SCARED_OFF
			bool bScareOff
			
			IF IS_CONDITION_TRUE(COND_PLAYER_300m_from_garage)
				IF DOES_ENTITY_EXIST(peddevin)
					IF NOT IS_PED_INJURED(peddevin)					
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peddevin,PLAYER_PED_ID())								
						OR GET_SCRIPT_TASK_STATUS(peddevin,SCRIPT_TASK_SYNCHRONIZED_SCENE) = DORMANT_TASK	
							bScareOff=TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(ped[ped_cop2].id)
					IF IS_PED_INJURED(ped[ped_cop2].id)
						bScareOff=TRUE
					ELSE
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped[ped_cop2].id,PLAYER_PED_ID())								
						OR GET_SCRIPT_TASK_STATUS(ped[ped_cop2].id,SCRIPT_TASK_START_SCENARIO_IN_PLACE) = DORMANT_TASK	
							bScareOff=TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(ped[ped_cop3].id)
					IF IS_PED_INJURED(ped[ped_cop3].id)
						bScareOff=TRUE
					ELSE
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped[ped_cop3].id,PLAYER_PED_ID())
						OR GET_SCRIPT_TASK_STATUS(ped[ped_cop3].id,SCRIPT_TASK_START_SCENARIO_IN_PLACE) = DORMANT_TASK					
							bScareOff=TRUE
						ENDIF
						
						//cprintln(debug_trevor3,ENUM_TO_INT(GET_SCRIPT_TASK_STATUS(ped[ped_cop3].id,SCRIPT_TASK_START_SCENARIO_IN_PLACE)))
					ENDIF
				ENDIF
				
			
				
				IF DOES_ENTITY_EXIST(ped[ped_cop4].id)
					IF IS_PED_INJURED(ped[ped_cop4].id)
						bScareOff=TRUE
					ELSE
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped[ped_cop4].id,PLAYER_PED_ID())
						OR GET_SCRIPT_TASK_STATUS(ped[ped_cop4].id,SCRIPT_TASK_START_SCENARIO_IN_PLACE) = DORMANT_TASK	
							bScareOff=TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehicle[vehJet].id)
					IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)
						bScareOff=TRUE
					ELSE
						//IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle[vehJet].id,PLAYER_PED_ID())
						IF GET_ENTITY_HEALTH(vehicle[vehJet].id) < 1000
							bScareOff=TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehicle[vehsec1].id)
					IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehsec1].id)
						bScareOff=TRUE
					ELSE
						//IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle[vehsec1].id,PLAYER_PED_ID())
						IF GET_ENTITY_HEALTH(vehicle[vehsec1].id) < 1000
							bScareOff=TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehicle[vehsec2].id)
					IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehsec2].id)
						bScareOff=TRUE
					ELSE
						//IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle[vehsec2].id,PLAYER_PED_ID())
						IF GET_ENTITY_HEALTH(vehicle[vehsec1].id) < 1000
							bScareOff=TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehicle[vehsec3].id)
					IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehsec3].id)
						bScareOff=TRUE
					ELSE
						//IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle[vehsec3].id,PLAYER_PED_ID())						
						IF GET_ENTITY_HEALTH(vehicle[vehsec3].id) < 1000
							bScareOff=TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF bScareOff
				and missionFailing = FAIL_NULL
					SET_MISSION_TO_FAIL(thisFailToCheck,"CH_F22",true)
					IF NOT IS_PED_INJURED(ped[ped_cop4].id)	
						TASK_SMART_FLEE_PED(ped[ped_cop4].id,player_ped_id(),100,-1)
						SET_PED_KEEP_TASK(ped[ped_cop4].id,TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(peddevin)	
						TASK_SMART_FLEE_PED(peddevin,player_ped_id(),100,-1)
						SET_PED_KEEP_TASK(peddevin,TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(ped[ped_cop2].id)	
						GIVE_WEAPON_TO_PED(ped[ped_cop2].id,WEAPONTYPE_MICROSMG,1000)
						TASK_COMBAT_PED(ped[ped_cop2].id,player_ped_id())
						SET_PED_KEEP_TASK(ped[ped_cop2].id,TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(ped[ped_cop3].id)	
						GIVE_WEAPON_TO_PED(ped[ped_cop3].id,WEAPONTYPE_MICROSMG,1000)
						TASK_COMBAT_PED(ped[ped_cop3].id,player_ped_id())
						SET_PED_KEEP_TASK(ped[ped_cop3].id,TRUE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
	
		
	ENDSWITCH
	
	IF missionFailing != FAIL_NULL		
		IF bDelayFail
		AND failTimer < 2.0
			
			failTimer += TIMESTEP()			
		ELSE
		
			Mission_Failed(sFailText)
		ENDIF		
	ENDIF
	
ENDPROC


PROC Check_For_Skip(bool bDoSkip = FALSE,enumMissionStage skipToStage=STAGE_NULL,int newFlagValue=0,bool bIsReplay=FALSE)

	
	#IF IS_DEBUG_BUILD
	IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct,iReturnStage,iReturnStage)		
	
		
		IF iReturnStage = 1
			If playAsTrevor = FALSE
				iReturnStage = 2
			ENDIF
		ELIF iReturnStage > 1
			iReturnStage++
		ENDIF

		bDoSkip = true
	ELSE
		
		If IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			iReturnStage = enum_to_int(missionProgress)
			bDoSkip = true
			If playAsTrevor = TRUE
				IF iReturnStage = 3
					iReturnStage = 1
				ELSE
					iReturnStage--
				endif
			else
				IF iReturnStage = 2
					iReturnStage = 0
				ELSE
					iReturnStage--					
				endif
			endif			
		ENDIF
		
		If IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			iReturnStage = enum_to_int(missionProgress)						
			If iReturnStage = 1				
				IF iStoredLastReplay = 0
					newFlagValue = 1
				ELSE		
					iReturnStage = 3
				ENDIF
			else
				iReturnStage++
			endif
			bDoSkip = true
			
		ENDIF
	ENDIF	
	#ENDIF
	
	bool bDoNotFade
	If bDoSkip = true
		DELETE_EVERYTHING(MISSION_STATE_SKIP)
		IF skipToStage != STAGE_NULL
			missionProgress=skipToStage
		ENDIF
		iFlag = newFlagValue
		
		switch missionProgress
			CASE STAGE_STARTUP		
				REQUEST_SCALEFORM_MOVIE("heli_cam")
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
				FORCE_ASSET_STAGE(ASSETS_STAGE_STARTUP)
				IF playAsTrevor
					CHANGE_PLAYER(SELECTOR_PED_TREVOR)
					SET_ENTITY_COORDS(player_ped_id(),<< 424.5670, -979.8093, 29.7108 >>)
					SET_ENTITY_HEADING(player_ped_id(),256.3022)
				ELSE
					CHANGE_PLAYER(SELECTOR_PED_FRANKLIN)
				ENDIF
				DISABLE_CELLPHONE(FALSE)
			BREAK
			
			CASE STAGE_TREVOR_GET_CHOPPER
				
				If iFlag = 1
					IF bIsReplay
						START_REPLAY_SETUP(<<461.8770, -984.3531, 42.6920>>,81.1471)
					ELSE
						SET_ENTITY_COORDS(player_ped_id(),<<461.8770, -984.3531, 42.6920>>)
						SET_ENTITY_HEADING(player_ped_id(),81.1471)
						LOAD_SCENE(<<461.8770, -984.3531, 42.6920>>)
					ENDIF
					REQUEST_SCALEFORM_MOVIE("heli_cam")
					REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")	
					CHANGE_PLAYER(SELECTOR_PED_TREVOR)
														
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					SET_PED_RESET_FLAG(player_ped_id(),PRF_DisableAgitation,TRUE)					
					
					add_event(events_create_Franklin_and_car)
					add_event(events_waypoint_through_police_dept,STAGE_LEARN_TO_SCAN)
					//cprintln(debug_trevor3,"GOt a")
					safewait(2)
					FORCE_ASSET_STAGE(ASSETS_STAGE_POLICE_STATION)
					//cprintln(debug_trevor3,"GOt b")
					DISABLE_CELLPHONE(FALSE)
					FORCE_INSTRUCTION_STATE(0,INS_GET_TO_RECEPTION)
					FORCE_INSTRUCTION_STATE(1,INS_FOLLOW_COP)
					FORCE_INSTRUCTION_STATE(2,INS_GET_TO_ROOF)
					FORCE_DIALOGUE_STATE(0,DIA_CAN_I_HELP)
					FORCE_DIALOGUE_STATE(1,DIA_COP_WALK_CHAT)
					FORCE_DIALOGUE_STATE(2,DIA_COME_ON)
					FORCE_DIALOGUE_STATE(4,DIA_PLAYER_ENTERS_LOCKER_ROOM)
					FORCE_DIALOGUE_STATE(6,DIA_COP_RETURNS_TO_DESK)	
					FORCE_DIALOGUE_STATE(7,DIA_RESTART_CONV)
					FORCE_DIALOGUE_STATE(8,DIA_PLAYER_THREAT_IN_POL_DEPT)					
				
					FORCE_ACTION_STATE(2,ACT_LOOK_AT_PLAYER_ENTRY)
					FORCE_ACTION_STATE(3,ACT_WALK_TO_STAIRS)					
					FORCE_ACTION_STATE(6,ACT_COP_RETURNS_TO_FRONT_DESK)
					FORCE_ACTION_STATE(9,ACT_COP_WAITS_FOR_PLAYER)					
					FORCE_ACTION_STATE(0,ACT_SPAWN_COPS,false)
					
				//	SET_ACTION_FLAG(0,ACT_SPAWN_COPS,100)
					
					WHILE NOT IS_ACTION_COMPLETE(5,ACT_SPAWN_CHOPPER)
					OR NOT IS_ACTION_COMPLETE(0,ACT_SPAWN_COPS)					
						ACTION(5,ACT_SPAWN_CHOPPER)
						ACTION(0,ACT_SPAWN_COPS)
						SAFEWAIT()
					ENDWHILE
					//cprintln(debug_trevor3,"GOt d")
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
					iFlag = 2
				else
					IF bIsReplay
						START_REPLAY_SETUP(<< 422.6565, -978.2415, 29.7089 >>,252)
						SET_CLOCK_TIME(21,00,00)
					ELSE
						SET_ENTITY_COORDS(player_ped_id(),<< 422.6565, -978.2415, 29.7089 >>)
						SET_ENTITY_HEADING(player_ped_id(),252)
						LOAD_SCENE(<< 422.6565, -978.2415, 29.7089 >>)
						SAFEWAIT(1)
					ENDIF
				//	CLEAR_AREA(<< 422.6565, -978.2415, 29.7089 >>,30,true) //to clear up remaining cops
					
					
					CLEAR_AREA(<<441.0267, -978.2040, 29.6895>>,2,TRUE)
					CLEAR_AREA(<< 440.2506, -975.6328, 29.6895 >>,2,TRUE)
					CLEAR_AREA(<<454.1487, -979.8940, 29.6896>>,2,TRUE)
					CLEAR_AREA(<<450.2071, -992.9072, 29.6896>>,2,TRUE)
					
					
					if not is_ped_injured(ped[ped_cop4].id) cprintln(debug_Trevor3,"cop 4 alive A") ENDIF
					CHANGE_PLAYER(SELECTOR_PED_TREVOR)					
					DISABLE_CELLPHONE(FALSE)					
				endif
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
				
				FORCE_ASSET_STAGE(ASSETS_STAGE_POLICE_STATION)
				WHILE NOT IS_ACTION_COMPLETE(0,ACT_SPAWN_COPS)					
					ACTION(0,ACT_SPAWN_COPS)
					SAFEWAIT()
				ENDWHILE
				
			BREAK

			case STAGE_FRANKLIN_AWAITS_TREVOR	
				CHANGE_PLAYER(SELECTOR_PED_FRANKLIN)
				IF bIsReplay
					START_REPLAY_SETUP(<<1390.5972, -2058.0562, 51.0442>>, 129.5256)
				ELSE
					SET_ENTITY_COORDS(player_ped_id(),<<1390.5972, -2058.0562, 51.0442>>)
					SET_ENTITY_HEADING(player_ped_id(),129.5256)
					LOAD_SCENE(<<1390.5972, -2058.0562, 51.0442>>)
					SAFEWAIT(1)
				ENDIF
				
				REQUEST_SCALEFORM_MOVIE("heli_cam")
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")

				FORCE_ASSET_STAGE(ASSETS_STAGE_STARTUP)
				
				DISABLE_CELLPHONE(FALSE)
				
				//CREATE_VEHICLE_FOR_REPLAY(vehicle[vehFranklin].id, <<1388.0295, -2067.3911, 50.9981>>, 73.6419,FALSE,FALSE,true,true,true) //
				
				//removed due to bug 1760749. Was creeating two franklin cars and it was defaulting to the Asterope anyway.
				
				cprintln(debug_Trevor3,"Make car")
				
				WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN,  << 1388.0295, -2067.3911, 50.9981 >>,0,true,VEHICLE_TYPE_CAR)
					WAIT(0)
				ENDWHILE
				
				
				
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
										
			break	
							
			CASE STAGE_LEARN_TO_SCAN
				//cprintln(debug_trevor3,"SKIPPY ME LOO")
				FORCE_ASSET_STAGE(ASSETS_STAGE_IN_CHOPPER)
				PLAY_MUSIC(mus_init_scanner,mus_scan_first_ped)
				CHANGE_PLAYER(SELECTOR_PED_TREVOR)
								
				REQUEST_SCALEFORM_MOVIE("heli_cam")
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
				vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< 1374.2163, -2074.3337, 58.9988 >>,28.3)
				SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
				SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
				SET_VEHICLE_DOORS_LOCKED(vehicle[vehChopper].id,VEHICLELOCK_LOCKED)
				WHILE NOT CREATE_FRANKLIN(<< 1380.8173, -2065.9912, 50.9988 >>,128)
					WAIT(0)
				ENDWHILE
								
				FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,TRUE)			
				
				if not IS_ENTITY_DEAD(PLAYER_PED_ID())
					if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id,VS_DRIVER)
					ENDIF
				ENDIF
				
				CLEAR_PRINTS()
				
				while not  SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[vehChopper].id,TRUE, vehicle[vehChopper].id)								
					WAIT(0)
				ENDWHILE
				SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",TRUE)
				DISABLE_SELECTOR()
				WHILE NOT HUD.bIsCamAttached
					
					RUN_HELICOPTER_HUD(HUD)
					SAFEWAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehicle[vehFranklin].id)
					WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN,  << 1388.0295, -2067.3911, 50.9981 >>,0,true,VEHICLE_TYPE_CAR)
						WAIT(0)
					ENDWHILE
				ENDIF
				RUN_HELICOPTER_HUD(HUD)
				
				
				
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,FALSE)	
				ENDIF
				
				HUD.bMoveChopper = true
				HUD.fTargetHeight = 100.0
				POINT_CHOPPER_CAM_AT_COORD(HUD,<< -212.2150, 639.5228, 191.7123 >>)
			
			
				
				iFlag=999
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
			break
			
			case STAGE_APPROACH_SCAN_AREA_ONE //skip to flying towards first destination
				IF bIsReplay
					START_REPLAY_SETUP(<< 131.4066, -341.6778, 177.6481 >>,60.0)
				ELSE
					SET_ENTITY_COORDS(player_ped_id(),<< 131.4066, -341.6778, 177.6481 >>)
					SET_ENTITY_HEADING(player_ped_id(),60)
					LOAD_SCENE(<< 131.4066, -341.6778, 177.6481 >>)
					SAFEWAIT(1)
				ENDIF
				REQUEST_SCALEFORM_MOVIE("heli_cam")
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
				FORCE_ASSET_STAGE(ASSETS_STAGE_PREP_AREA_1)
				PLAY_MUSIC(mus_reload_scan_area_1,mus_scan_first_ped)
				CHANGE_PLAYER(SELECTOR_PED_TREVOR)			
				if IS_PHONE_ONSCREEN()
					HANG_UP_AND_PUT_AWAY_PHONE()
				ENDIF
				HUD.fTargetHeight = 100.0
				
				vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< 131.4066, -341.6778, 177.6481 >>,60.0)
				SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
				SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id,VS_DRIVER)
				ENDIF
				
				IF NOT IS_HELIHUD_ACTIVE(HUD)
					SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[vehChopper].id,true, vehicle[vehChopper].id)
					DISABLE_SELECTOR()
				ENDIF
				SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",TRUE)
				WHILE NOT HUD.bIsCamAttached
					RUN_HELICOPTER_HUD(HUD)
					SAFEWAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				ENDIF		
				//if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
				//	SET_ENTITY_COORDS(vehicle[vehChopper].id,<< 253.5414, -503.7481, 230.8252 >>)
				//	SET_ENTITY_HEADING(vehicle[vehChopper].id,0)
				//	FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,FALSE)
					HUD.bMoveChopper = true
				//ENDIF
				if DOES_BLIP_EXIST(ped[ped_trevor].blip)
					REMOVE_BLIP(ped[ped_trevor].blip)
				ENDIF
				if DOES_BLIP_EXIST(blipTarget)
					REMOVE_BLIP(blipTarget)
				ENDIF
	

				//CREATE_CHOPPER_PILOT()
			
				
				POINT_CHOPPER_CAM_AT_COORD(HUD,<< -23.5265, -44.4256, 87.3915 >>)
				add_event(EVENTS_LOAD_FIRST_LOCATION_PEDS)
				add_event(EVENTS_QUAD_BLOCKING,STAGE_CHASE_BEGINS)

				
				//initVehicle(vehFranklin,BUFFALO,<< 187.2789, -153.2764, 55.6541 >>, 254.3027)
				WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN,  << -93.4581, -66.9989, 55.8005 >>,345.5313,true,VEHICLE_TYPE_CAR)
					WAIT(0)
				ENDWHILE
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
					SET_ENTITY_INVINCIBLE(vehicle[vehFranklin].id,true)
				ENDIF
				WHILE NOT CREATE_FRANKLIN(vNull,0,vehicle[vehFranklin].id)
					WAIT(0)
				ENDWHILE
				if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
				//	SET_ENTITY_COORDS(vehicle[vehChopper].id,<< 247.4768, -190.9185, 150.9918 >>)
				//	SET_ENTITY_HEADING(vehicle[vehChopper].id,90.0)
					POINT_CHOPPER_CAM_AT_COORD(HUD,<< 200.2920, -156.3387, 58.0269 >>)
				ENDIF
				
			
				iFlag=9
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
			break
			
			case STAGE_APPROACH_SCAN_AREA_THREE //skip to flying towards first destination
				IF bIsReplay
					START_REPLAY_SETUP(<< 82.3613, -72.5265, 233.7927 >>,-114.3)
				ELSE
					SET_ENTITY_COORDS(player_ped_id(),<< 82.3613, -72.5265, 233.7927 >>)
					SET_ENTITY_HEADING(player_ped_id(),60)
					LOAD_SCENE(<< 82.3613, -72.5265, 233.7927 >>)
					SAFEWAIT(1)
				ENDIF
				
				REQUEST_SCALEFORM_MOVIE("heli_cam")
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
				FORCE_ASSET_STAGE(ASSETS_STAGE_PREP_AREA_2)
				
				CHANGE_PLAYER(SELECTOR_PED_TREVOR)
				add_event(EVENTS_QUAD_BLOCKING,STAGE_CHASE_BEGINS)
			
				if IS_PHONE_ONSCREEN()
					HANG_UP_AND_PUT_AWAY_PHONE()
				ENDIF

				
				vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< 82.3613, -72.5265, 233.7927 >>,-114.3)
				SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
				
				SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id,VS_DRIVER)
				ENDIF
				
				IF NOT IS_HELIHUD_ACTIVE(HUD)					
					SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[vehChopper].id,true, vehicle[vehChopper].id)		
					DISABLE_SELECTOR()
				ENDIF
				SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",TRUE)
				WHILE NOT HUD.bIsCamAttached
					RUN_HELICOPTER_HUD(HUD)
					SAFEWAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				ENDIF
				
				//CREATE_CHOPPER_PILOT()
		
				HUD.bMoveChopper = true
				PRINT_NOW("CH_INS12C",6000,1)
				POINT_CHOPPER_CAM_AT_COORD(HUD,<< 215.9081, -130.7865, 63.6567 >>)
			
			
				//initVehicle(vehFranklin,BUFFALO,<< 187.2789, -153.2764, 55.6541 >>, 254.3027)
				WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN, << -93.4581, -66.9989, 55.8005 >>,345.5313,true,VEHICLE_TYPE_CAR)
					WAIT(0)
				ENDWHILE
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
					SET_ENTITY_INVINCIBLE(vehicle[vehFranklin].id,true)
				ENDIF
				WHILE NOT CREATE_FRANKLIN(vNull,0,vehicle[vehFranklin].id)
					WAIT(0)
				ENDWHILE
				if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
				//	SET_ENTITY_COORDS(vehicle[vehChopper].id,<< 247.4768, -190.9185, 150.9918 >>)
				//	SET_ENTITY_HEADING(vehicle[vehChopper].id,90.0)
					POINT_CHOPPER_CAM_AT_COORD(HUD,<< 200.2920, -156.3387, 58.0269 >>)
				ENDIF
				
				add_event(EVENTS_LOAD_FINAL_LOCATION_PEDS)
				set_event_flag(EVENTS_LOAD_FINAL_LOCATION_PEDS,2)
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
				
				START_AUDIO_SCENE("CAR_2_SCAN_THE_SUSPECTS")
				PLAY_MUSIC(mus_reload_scan_area_2,mus_chad_sees_franklin)
			break

			case STAGE_CHASE_BEGINS //skip to chad waiting by car
			
				IF bIsReplay
					START_REPLAY_SETUP(<< 247.4768, -190.9185, 150.9918 >>,130.3)
				ELSE
					SET_ENTITY_COORDS(player_ped_id(),<< 247.4768, -190.9185, 150.9918 >>)
					SET_ENTITY_HEADING(player_ped_id(),130.3)
					LOAD_SCENE(<< 247.4768, -190.9185, 150.9918 >>)
					SAFEWAIT(1)
				ENDIF
			
				REQUEST_SCALEFORM_MOVIE("heli_cam")
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
				HUD.bBlockTargetLost = TRUE
				
				START_AUDIO_SCENE("CAR_2_CAR_CHASE_START")
				START_AUDIO_SCENE("CAR_2_Z_TYPE_ENGINE_BOOST")
				

				FORCE_ASSET_STAGE(ASSETS_STAGE_RELEASE_AREA_2)
				PLAY_MUSIC(mus_reload_chase_Stage,mus_chad_drives_down_alley)
				REQUEST_MODEL(model_chad)
				WHILE NOT HAS_MODEL_LOADED(model_chad)
					SAFEWAIT(0)
				ENDWHILE
				CHANGE_PLAYER(SELECTOR_PED_TREVOR)
				add_event(EVENTS_SPEED_UP_CHOPPER)
				add_event(EVENTS_STEALTH_REMOVE_TRAFFIC,STAGE_CARPARK)
				SET_ROADS_IN_ANGLED_AREA(<<-1314.045654,-183.052002,40.465984>>, <<-1256.618408,-261.595306,68.179886>>, 52.250000,FALSE,FALSE)

				
				vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< 247.4768, -190.9185, 150.9918 >>,130.3)
				SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
				SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id,VS_DRIVER)
				ENDIF
				
				IF NOT IS_HELIHUD_ACTIVE(HUD)
					SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[vehChopper].id,true, vehicle[vehChopper].id)
					DISABLE_SELECTOR()
					hud.fChopperSpeed=0.0
				ENDIF
				SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",TRUE)
				
				WHILE NOT HUD.bIsCamAttached
					RUN_HELICOPTER_HUD(HUD)
					SAFEWAIT(0)
				ENDWHILE
		
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				ENDIF
				//delete all peds (except pilot)
				
				
				//vehicle[vehHeist].id = CREATE_VEHICLE(ZTYPE,<< 202.7272, -149.7968, 56.1760 >>,160)	
				initHeistVehicle(<< 202.7272, -149.7968, 56.1760 >>,160)
			//	initVehicle(vehFranklin,BUFFALO,<< 187.2789, -153.2764, 55.6541 >>, 254.3027)
				WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN, << 187.2789, -153.2764, 55.6541 >>, 254.3027,true,VEHICLE_TYPE_CAR)
					SAFEWAIT(0)
				ENDWHILE
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
					SET_ENTITY_INVINCIBLE(vehicle[vehFranklin].id,true)
				ENDIF
				WHILE NOT CREATE_FRANKLIN(vNull,0,vehicle[vehFranklin].id)
					SAFEWAIT(0)
				ENDWHILE
				
				
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehicle[vehHeist].id,"CAR_2_Z-TYPE")
				if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
					SET_ENTITY_COORDS(vehicle[vehChopper].id,<< 247.4768, -190.9185, 150.9918 >>)
					SET_ENTITY_HEADING(vehicle[vehChopper].id,90.0)
					POINT_CHOPPER_CAM_AT_COORD(HUD,<<212.3564, -180.0367, 63.6800>>)
				ENDIF
													
				//CREATE_CHOPPER_PILOT()
				initPedInVehicle(ped_chad,model_chad,vehicle[vehHeist].id)
				//add_event(EVENTS_FRANKLIN_ARRIVE)
				HUD.bMoveChopper = true
				
				
				kill_event(EVENTS_SPEED_UP_CHOPPER)
											
				bplayVehRecs = true
				

				while bRecordingsLoaded = FALSE
					loadCarRecData()
					safewait()
				endwhile
				iPlaybackTime = 0
			//	add_event(events_chase_chatter)										
			//	add_event(events_blow_up_cars)
				
			//	PRINTLN("FAIL COUNT:",Get_Fails_Count_Without_Progress_For_This_Mission_Script())
		//		IF Get_Fails_Count_Without_Progress_For_This_Mission_Script() >= 2	
		//			ADD_PED_TO_SCAN_LIST(HUD,ped[ped_chad].id,true,HUD_FOE,true,true,true,true)
		//			ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_chad].id,"CH_CHAD",8)	
		//		ELSE
					ADD_EVENT(events_chad_car_brief_hint)
		//		ENDIF
				
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
				
			break
			
			
			CASE STAGE_CARPARK
				START_AUDIO_SCENE("CAR_2_CAR_ENTERS_GARAGE")
				FORCE_ASSET_STAGE(ASSETS_STAGE_PREP_CAR_PARK)
				REQUEST_SCALEFORM_MOVIE("heli_cam")
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
				PLAY_MUSIC(mus_reload_car_park,mus_get_car_to_objective)
				CHANGE_PLAYER(SELECTOR_PED_TREVOR)
				REQUEST_MODEL(model_chad)
				SET_ROADS_IN_ANGLED_AREA(<<-1314.045654,-183.052002,40.465984>>, <<-1256.618408,-261.595306,68.179886>>, 52.250000,FALSE,FALSE)
				//vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< -1199.8082, -295.1306, 142.2804 >>,90.3)
				initVehicle(vehChopper,POLMAV,<< -1199.8082, -295.1306, 142.2804 >>,90.3)
				SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
				SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id,VS_DRIVER)
				ENDIF
				
			//	ped[ped_trevor].id = player_ped_id()
				
				IF NOT IS_HELIHUD_ACTIVE(HUD)
					SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[vehChopper].id,true, vehicle[vehChopper].id)
					DISABLE_SELECTOR()
				ENDIF
				SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",TRUE)
				WHILE NOT HUD.bIsCamAttached
					RUN_HELICOPTER_HUD(HUD)
					SAFEWAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				ENDIF
				
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1284.524292,-212.007370,50.633434>>-<<39.000000,54.750000,14.000000>>,<<-1284.524292,-212.007370,50.633434>>+<<39.000000,54.750000,14.000000>>,false)
				//delete all peds (except pilot)
				

				initHeistVehicle(<< -1309.563, -215.238, 50.993 >>, 123.0209)
			//	initVehicle(vehFranklin,BUFFALO,<< -1188.8595, -294.5421, 36.8739 >>,223.0)
				WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN, << -1275.8739, -222.6858, 50.5496 >>,218.0,true,VEHICLE_TYPE_CAR)
					SAFEWAIT(0)
				ENDWHILE
			//	vehicle[vehFranklin].id = CREATE_VEHICLE(BUFFALO,<< -1188.8595, -294.5421, 36.8739 >>,223.0)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
					SET_ENTITY_INVINCIBLE(vehicle[vehFranklin].id,true)
				ENDIF
				//START_PLAYBACK_RECORDED_VEHICLE(vehicle[vehHeist].id,206,sVehicleRecordingLibrary)
				//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehicle[vehHeist].id,7000)
			
				
				
				WHILE NOT HAS_MODEL_LOADED(model_chad)
					SAFEWAIT(0)
				ENDWHILE
				
				
				initped(ped_chad,model_chad,vNull,0.0,vehicle[vehHeist].id,VS_DRIVER,pedrole_enemyUnaware)	
				
				
				STOP_PED_SPEAKING(ped[ped_chad].id,TRUE)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
					POINT_CHOPPER_CAM_AT_COORD(HUD,GET_ENTITY_COORDS(vehicle[vehHeist].id))
				ENDIF
								//ped[ped_chad].id = CREATE_PED_INSIDE_VEHICLE(vehicle[vehHeist].id,PEDTYPE_MISSION,model_chad)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
					WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(ped[ped_franklin].id,CHAR_FRANKLIN,vehicle[vehFranklin].id)
						safewait()
					ENDWHILE
				ENDIF
		
				add_event(EVENTS_FILL_CARPARK,STAGE_DRIVE_VEHICLE_TO_OBJECTIVE)
				WHILE NOT is_event_complete(EVENTS_FILL_CARPARK)	
					control_events(EVENTS_FILL_CARPARK)
					safewait()
				ENDWHILE
				//CREATE_CHOPPER_PILOT()
								
				if not DOES_BLIP_EXIST(ped[ped_chad].blip)
				//	ped[ped_chad].blip = CREATE_BLIP_FOR_PED(ped[ped_chad].id,true)
				ENDIF
				
				iFlag=0
				
				HUD.bMoveChopper = true
			BREAK
			
			CASE STAGE_SCAN_CARPARK_PEDS
				START_AUDIO_SCENE("CAR_2_CAR_ENTERS_GARAGE")
				FORCE_ASSET_STAGE(ASSETS_STAGE_PREP_CAR_PARK)
				PLAY_MUSIC(mus_reload_car_park,mus_get_car_to_objective)
				CHANGE_PLAYER(SELECTOR_PED_TREVOR)
				REQUEST_MODEL(model_chad)
				REQUEST_SCALEFORM_MOVIE("heli_cam")
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
				//vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< -1199.8082, -295.1306, 142.2804 >>,90.3)
				initVehicle(vehChopper,POLMAV,<< -1301.2561, -253.3784, 54 >>,-49.3)
				SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
				SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id,VS_DRIVER)
				ENDIF
				
				SET_ROADS_IN_ANGLED_AREA(<<-1314.045654,-183.052002,40.465984>>, <<-1256.618408,-261.595306,68.179886>>, 52.250000,FALSE,FALSE)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1284.524292,-212.007370,50.633434>>-<<39.000000,54.750000,14.000000>>,<<-1284.524292,-212.007370,50.633434>>+<<39.000000,54.750000,14.000000>>,false)
				
			//	ped[ped_trevor].id = player_ped_id()
				
				IF NOT IS_HELIHUD_ACTIVE(HUD)
					SET_CHOPPER_HUD_ACTIVE(HUD,vehicle[vehChopper].id,true, vehicle[vehChopper].id)
					DISABLE_SELECTOR()
				ENDIF
				SET_AUDIO_FLAG("AllowPoliceScannerWhenPlayerHasNoControl",TRUE)
				WHILE NOT HUD.bIsCamAttached
					RUN_HELICOPTER_HUD(HUD)
					SAFEWAIT(0)
				ENDWHILE
				
				set_chopper_speed_limits(hud,0.3,0.7)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_ENTITY_COORDS(vehicle[vehChopper].id,<< -1301.2561, -253.3784, 54 >>)
					SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				ENDIF
				
				
				//delete all peds (except pilot)
				

				initHeistVehicle(<< -1309.563, -215.238, 50.993 >>, 123.0209)
			//	initVehicle(vehFranklin,BUFFALO,<< -1188.8595, -294.5421, 36.8739 >>,223.0)
				WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN, << -1275.8739, -222.6858, 50.5496 >>,218.0,true,VEHICLE_TYPE_CAR)
					SAFEWAIT(0)
				ENDWHILE
			//	vehicle[vehFranklin].id = CREATE_VEHICLE(BUFFALO,<< -1188.8595, -294.5421, 36.8739 >>,223.0)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
					SET_ENTITY_INVINCIBLE(vehicle[vehFranklin].id,true)
				ENDIF
				//START_PLAYBACK_RECORDED_VEHICLE(vehicle[vehHeist].id,206,sVehicleRecordingLibrary)
				//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehicle[vehHeist].id,7000)
			
				
				
				WHILE NOT HAS_MODEL_LOADED(model_chad)
					SAFEWAIT(0)
				ENDWHILE
				
				
				initped(ped_chad,model_chad,vNull,0.0,vehicle[vehHeist].id,VS_DRIVER,pedrole_enemyUnaware)	
				STOP_PED_SPEAKING(ped[ped_chad].id,TRUE)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
					POINT_CHOPPER_CAM_AT_COORD(HUD,<<-1308.4099, -217.4297, 52.3787>>)
				ENDIF
								//ped[ped_chad].id = CREATE_PED_INSIDE_VEHICLE(vehicle[vehHeist].id,PEDTYPE_MISSION,model_chad)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(ped[ped_franklin].id,CHAR_FRANKLIN,<<-1274.7087, -216.8189, 50.5498>>, 142.5829)
						safewait()
					ENDWHILE
				ENDIF
		
				add_event(EVENTS_FILL_CARPARK,STAGE_DRIVE_VEHICLE_TO_OBJECTIVE)
				WHILE NOT is_event_complete(EVENTS_FILL_CARPARK)	
					control_events(EVENTS_FILL_CARPARK)
					safewait()
				ENDWHILE
				//CREATE_CHOPPER_PILOT()
								
				
				
				iFlag=0
				
				HUD.bMoveChopper = true
				
				FORCE_ACTION_STATE(0,ACT_CHOPPER_CONTROL,true)
				FORCE_ACTION_STATE(3,ACT_FRANKLIN_RUNS_TO_ROOF,false)
				SET_ACTION_FLAG(3,ACT_FRANKLIN_RUNS_TO_ROOF,2)
	
				
				checkConditions(COND_CARPARK_STARTS,COND_CAR_PARK_ENDS)
				
				SET_CONDITION_STATE(COND_CHOPPER_DESCENDING,TRUE)
				SET_CONDITION_STATE(COND_FRANKLIN_STOPPED_HIS_CAR,TRUE)
				SET_CONDITION_STATE(COND_FRANKLIN_HAS_STOPPED_RUNNING,TRUE)
				SET_CONDITION_STATE(COND_A_LITTLE_AFTER_EXITS_CLEAR_DIA,TRUE)
				SET_CONDITION_STATE(COND_DIA_TAKE_US_DOWN,TRUE)
				SET_CONDITION_STATE(COND_DIA_THERMAL_VISION,TRUE)
				SET_CONDITION_STATE(COND_DIA_CAN_YOU_SEE_FRANKLIN,TRUE)		
				SET_CONDITION_STATE(COND_DIA_TREVOR_SEE_FRANKLIN_ENDED,TRUE)
				SET_CONDITION_STATE(COND_CHOPPER_AT_CAR_PARK_LEVEL,TRUE)
				SET_CONDITION_STATE(COND_THERMAL_TURNED_ON,TRUE)
				SET_CONDITION_STATE(COND_FRANKLIN_OBSERVED_WITH_THERMAL,TRUE)
				SET_CONDITION_STATE(COND_THERMAL_READY,TRUE)
				
				FORCE_INSTRUCTION_STATE(0,INS_TURN_ON_THERMAL_VISION)
				FORCE_INSTRUCTION_STATE(1,INS_ZOOM_IN_ON_FRANKLIN)
			
				
				FORCE_DIALOGUE_STATE(0,DIA_TAKE_US_DOWN)
				FORCE_DIALOGUE_STATE(1,DIA_TREVOR_SCARED)
				FORCE_DIALOGUE_STATE(2,DIA_EXITS_CLEAR)
				FORCE_DIALOGUE_STATE(3,DIA_THERMAL_VISION)
				FORCE_DIALOGUE_STATE(4,DIA_TREVOR_REACTION_TO_THERMAL_VISION_ON)
				FORCE_DIALOGUE_STATE(5,DIA_FRANKLIN_ASKS_IF_PLAYER_CAN_SEE_HIM)
				FORCE_DIALOGUE_STATE(13,DIA_TREVOR_SEE_FRANKLIN)
				
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_ENTITY_COORDS(vehicle[vehChopper].id,<< -1301.2561, -253.3784, 54 >>)					
				ENDIF
				
				add_event(EVENTS_CONTROL_INFRARED)
				add_event(EVENTS_CHOPPER_CARPARK)
				set_event_flag(EVENTS_CHOPPER_CARPARK,4)
				control_events(EVENTS_CHOPPER_CARPARK)
				
			
				
				clearHoverInArea(hud)
				
				fixChopperHeading(hud,-55.6696)
			BREAK
			
			CASE STAGE_TAKE_ZTYPE	
			
				IF bIsReplay
					START_REPLAY_SETUP(<< -1300.4332, -211.6004, 50.6831 >>,125.0)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -1300.4332, -211.6004, 50.6831 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),125.4926)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					LOAD_SCENE(<< -1300.4332, -211.6004, 50.6831 >>)
					SAFEWAIT(1)
				ENDIF
					
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1284.524292,-212.007370,50.633434>>-<<39.000000,54.750000,14.000000>>,<<-1284.524292,-212.007370,50.633434>>+<<39.000000,54.750000,14.000000>>,false)
				FORCE_ASSET_STAGE(ASSETS_STAGE_PREP_CAR_PARK)
				
				REQUEST_MODEL(model_pilot)
				REQUEST_MODEL(model_chad)
				WHILE NOT HAS_MODEL_LOADED(model_pilot)
				OR nOT HAS_MODEL_LOADED(model_chad)
					SAFEWAIT(0)
				ENDWHILE
				SET_ROADS_IN_ANGLED_AREA(<<-1314.045654,-183.052002,40.465984>>, <<-1256.618408,-261.595306,68.179886>>, 52.250000,FALSE,FALSE)
				PLAY_MUSIC(mus_reload_collec_car_stage,mus_get_car_to_objective)
				
				CHANGE_PLAYER(SELECTOR_PED_FRANKLIN)				
	
		//		CLEAR_AREA(<< -1308.9067, -215.0462, 50.5497 >>,150.0,true)
				initVehicle(vehChopper,POLMAV,<< -1318.8483, -231.4252, 57.5876 >>,-62.3)
				SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
				SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
				SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,TRUE)
			//	add_event(EVENTS_CHOPPER_HOVER)
				
				
				

				initHeistVehicle(<< -1309.5819, -215.2148, 50.5497 >>, 123.0209)

				
				initped(ped_chad,model_chad,vNull,0.0,vehicle[vehHeist].id,VS_DRIVER,pedrole_enemyUnaware)		
				STOP_PED_SPEAKING(ped[ped_chad].id,TRUE)
			
				

				CREATE_CHOPPER_PILOT()

				WHILE NOT CREATE_TREVOR(vNull,0,vehicle[vehChopper].id,VS_FRONT_RIGHT)		
					safewait()
				ENDWHILE
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_PISTOL,25,true)
				SET_PED_COMP_ITEM_CURRENT_SP(ped[ped_trevor].id, COMP_TYPE_PROPS, PROPS_P2_PILOT_HEADPHONES,FALSE)
			
				ACTION(11,ACT_CHAD_EXITS_CAR)
				add_event(EVENTS_FILL_CARPARK,STAGE_DRIVE_VEHICLE_TO_OBJECTIVE)
				WHILE NOT is_event_complete(EVENTS_FILL_CARPARK)	
					control_events(EVENTS_FILL_CARPARK)	
					SAFEWAIT(0)
				ENDWHILE
	
			
				bDisplayBeam = TRUE
			
				iFlag=0
				
				FREEZE_ENTITY_POSITION(vehicle[vehHeist].id,TRUE)
				WHILE NOT IS_SYNCHRONIZED_SCENE_RUNNING(scene_chad_leaves_car)
					ACTION(11,ACT_CHAD_EXITS_CAR)				
					SAFEWAIT(0)
				ENDWHILE
				
		//		LOAD_SCENE(<< -1300.4332, -211.6004, 50.6831 >>)
				
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,FALSE)
					SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				endif
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
					FREEZE_ENTITY_POSITION(vehicle[vehHeist].id,FALSE)
					SET_VEHICLE_DOOR_OPEN(vehicle[vehHeist].id,SC_DOOR_FRONT_LEFT,true,true)
				endif
				
					
				IF IS_SYNCHRONIZED_SCENE_RUNNING(scene_chad_leaves_car)
					SET_SYNCHRONIZED_SCENE_PHASE(scene_chad_leaves_car,0.06)
				ENDIF
				//GIVE_WEAPON_TO_PED(ped[ped_franklin].id,WEAPONTYPE_PISTOL,25,true)
	
				TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(player_ped_id(),<<-1302.1127, -212.9030, 50.5500>>,<<-1307.9683, -216.9170, 51.9251>>,pedmove_walk,false)
				
				DISABLE_CELLPHONE(FALSE)
				
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
			BREAK
			
			case STAGE_DRIVE_VEHICLE_TO_OBJECTIVE //drive heist vehicle to end
				//cprintln(debug_trevor3,"a")
				IF bIsReplay
					START_REPLAY_SETUP(<< -1320.8483, -231.4252, 57.4876 >>,125.0)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -1320.8483, -231.4252, 57.4876 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),125.4926)
					LOAD_SCENE(<< -1320.8483, -231.4252, 57.4876 >>)
					SAFEWAIT(1)
				ENDIF
				//cprintln(debug_trevor3,"a")
				FORCE_ASSET_STAGE(ASSETS_STAGE_PREP_CAR_PARK)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1284.524292,-212.007370,50.633434>>-<<39.000000,54.750000,14.000000>>,<<-1284.524292,-212.007370,50.633434>>+<<39.000000,54.750000,14.000000>>,false)
				
				CHANGE_PLAYER(SELECTOR_PED_FRANKLIN)	
				REQUEST_MODEL(model_pilot)
				REQUEST_MODEL(model_chad)
				WHILE NOT HAS_MODEL_LOADED(model_pilot)
				OR nOT HAS_MODEL_LOADED(model_chad)
					SAFEWAIT(0)
				ENDWHILE
				
				//cprintln(debug_trevor3,"a")
				//vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< -1318.8483, -231.4252, 57.4876 >>,-62.3)
				initVehicle(vehChopper,POLMAV,<< -1318.8483, -231.4252, 57.4876 >>,-62.3)
				SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
				SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
				WHILE NOT CREATE_TREVOR(vNull,0,vehicle[vehChopper].id,VS_FRONT_RIGHT)
					safewait()
				ENDWHILE
				CREATE_CHOPPER_PILOT()
				//delete all peds (except pilot)
				//cprintln(debug_trevor3,"a")
				//vehicle[vehHeist].id = CREATE_VEHICLE(ztype,<< -1309.1268, -215.1889, 50.5497 >>,125)
				initHeistVehicle(<< -1309.1268, -215.1889, 50.5497 >>,125)
			
				FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,TRUE)
			
				IF NOT IS_PED_INJURED(player_ped_id())
				AND IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicle[vehHeist].id)
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				//vehicle[vehGolfCart].id = CREATE_VEHICLE(CADDY,<< -1324.5869, -211.3875, 42.4008 >>,305.0)				
				//cprintln(debug_trevor3,"a")
				REQUEST_MODEL(model_chad)
				WHILE NOT HAS_MODEL_LOADED(model_chad)
					safewait()
				ENDWHILE
				
				IF g_numEnemyAlive = 1				
					CPRINTLN(debug_Trevor3,"replay Chad alive a")
					initPedOnFoot(ped_chad,model_chad,<< -1309.1848, -217.6889, 50.5497 >>,328,pedrole_no_ai)												
				ELSE
					CPRINTLN(debug_Trevor3,"replay Chad dead a")
				ENDIF
				
				add_event(EVENTS_FILL_CARPARK)
				
				WHILE NOT CREATE_PLAYER_VEHICLE(vehicle[vehFranklin].id, CHAR_FRANKLIN, << -1275.8739, -222.6858, 50.5496 >>,218.0,true,VEHICLE_TYPE_CAR)
				OR NOT is_event_complete(EVENTS_FILL_CARPARK)	
					control_events(EVENTS_FILL_CARPARK)	
					WAIT(0)
				ENDWHILE
				
				//cprintln(debug_trevor3,"a")							
		
				PLAY_MUSIC(mus_reload_drive_car_stage,mus_null)												
		
				bDisplayBeam = TRUE
		
				iFlag=0
				//LOAD_SCENE(<< -1309.1268, -215.1889, 50.5497 >>)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					FREEZE_ENTITY_POSITION(vehicle[vehChopper].id,FALSE)
					SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
				ENDIF
				DISABLE_CELLPHONE(FALSE)
				
				checkConditions(COND_ZTYPE_STARTS,COND_ZTYPE_ENDS)
				FORCE_INSTRUCTION_STATE(0,INS_GET_IN_THE_ZTYPE,TRUE)
				FORCE_ACTION_STATE(11,ACT_CHAD_EXITS_CAR)
				//cprintln(debug_trevor3,"a")
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
				//cprintln(debug_trevor3,"a")
			break
			
			CASE STAGE_APPROACH_AIRPORT_GATES
				IF bIsReplay
					START_REPLAY_SETUP(<<-918.2530, -2698.2292, 12.7509>>, 152.7001 )
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-918.2530, -2698.2292, 12.7509>>  )
					SET_ENTITY_HEADING(PLAYER_PED_ID(),152.7001)
					LOAD_SCENE(<< -918.2530, -2698.2292, 12.7509 >>)
					SAFEWAIT(1)
				ENDIF
			
				FORCE_ASSET_STAGE(ASSETS_STAGE_RELEASE_CAR_PARK)
				
				CHANGE_PLAYER(SELECTOR_PED_FRANKLIN)	
				
				initHeistVehicle(<<-918.2530, -2698.2292, 12.7509>>, 152.7001)
				SET_PED_INTO_VEHICLE(player_ped_id(),vehicle[vehheist].id)
			
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				
				PLAY_MUSIC(mus_reload_drive_car_stage,mus_null)												
		
				DISABLE_CELLPHONE(FALSE)
				
				checkConditions(COND_ZTYPE_STARTS,COND_ZTYPE_ENDS)
				
				FORCE_INSTRUCTION_STATE(0,INS_GET_IN_THE_ZTYPE,TRUE)
			//	FORCE_INSTRUCTION_STATE(2,INS_WARNING_WANTED_LEVEL)
				FORCE_ACTION_STATE(11,ACT_CHAD_EXITS_CAR)
			
				FORCE_ACTION_STATE(1,ACT_FLY_TO_GARAGE)
				FORCE_ACTION_STATE(2,ACT_REMOVE_CAR_PARK_ASSETS)
				FORCE_ACTION_STATE(8,ACT_REACTS_TO_PLAYER_IN_CAR)
				FORCE_ACTION_STATE(9,ACT_CHAD_TRIES_TO_CREEP_AWAY)
				FORCE_ACTION_STATE(10,ACT_CHAD_RUNS_OFF)
				FORCE_ACTION_STATE(11,ACT_CHAD_EXITS_CAR)
				FORCE_ACTION_STATE(14,ACT_CONTROL_CHOPPER)
				FORCE_ACTION_STATE(16,ACT_RETURN_CONTROL_TO_PLAYER)
				
				
				FORCE_DIALOGUE_STATE(7,DIA_FRANKLIN_THREATENS_CHAD)
				FORCE_DIALOGUE_STATE(0,DIA_DONT_TAKE_CAR)
				FORCE_DIALOGUE_STATE(1,DIA_BEG_TO_LET_GO)
				FORCE_DIALOGUE_STATE(2,DIA_FLEE_PANIC)
				FORCE_dIALOGUE_STATE(13,DIA_FLEE_PANIC_NOT_HIT)
				FORCE_DIALOGUE_STATE(3,DIA_PLAYER_GOT_IN_CAR)
				FORCE_DIALOGUE_STATE(8,DIA_CALL_DEVIN)
				FORCE_DIALOGUE_STATE(5,DIA_WARNING_COPS)
				FORCE_DIALOGUE_STATE(6,DIA_KILLED_CHAD)
				FORCE_DIALOGUE_STATE(9,DIA_PLAYER_SHOOTING_CHOPPER)
				FORCE_DIALOGUE_STATE(10,DIA_DRIVING_BACK)
				FORCE_DIALOGUE_STATE(11,DIA_CRASH_ZTYPE)
				FORCE_DIALOGUE_STATE(12,DIA_PLAYER_RUNS_OFF)
				
				SET_CONDITION_STATE(COND_PHONE_CALL_MADE,TRUE)
				
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
			BREAK
			
			CASE STAGE_FINAL_CUT
				IF bIsReplay
					START_REPLAY_SETUP(<< -991.8787, -3006.2463, 12.9449 >>,125.0)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -991.8787, -3006.2463, 12.9449 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),125.4926)
					LOAD_SCENE(<< -991.8787, -3006.2463, 12.9449 >>)
					SAFEWAIT(1)
				ENDIF
				FORCE_ASSET_STAGE(ASSETS_STAGE_FINAL_CUTSCENE)
				CHANGE_PLAYER(SELECTOR_PED_FRANKLIN)	
			
				
				//vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< 515.5120, -1333.8617, 74.3550 >>,95.3)
				initHeistVehicle(<< -991.8787, -3006.2463, 12.9449 >>,125)
				
				
				
			//	WHILE NOT CREATE_NPC_PED_ON_FOOT(pedDevin,CHAR_DEVIN,<<-991.8787, -3006.2463, 12.9449>>,-125.28)
			//		safewait()
				//endwhile
				
				
				ACTION(3,ACT_SET_UP_DEVIN_LEADIN)
				WHILE NOT IS_ACTION_COMPLETE(3,ACT_SET_UP_DEVIN_LEADIN)
					ACTION(3,ACT_SET_UP_DEVIN_LEADIN)
					safewait(0)
				ENDWHILE
											
				DISABLE_CELLPHONE(FALSE)
				bDoNotFade = TRUE
				SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE) 
				
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
			BREAK
			
			CASE STAGE_LEAVE_AIRPORT
				IF bIsReplay
					START_REPLAY_SETUP(<< -991.8787, -3006.2463, 12.9449 >>,15.0)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -991.8787, -3006.2463, 12.9449 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),15.4926)
					LOAD_SCENE(<< -991.8787, -3006.2463, 12.9449 >>)
					SAFEWAIT(1)
				ENDIF
				
				FORCE_ASSET_STAGE(ASSETS_STAGE_FINAL_CUTSCENE)
				CHANGE_PLAYER(SELECTOR_PED_FRANKLIN)	
				
				vehicle[vehJet].id = CREATE_VEHICLE(SHAMAL,<<-981.02, -3011.31, 14.55>>,59.25)
				SET_VEHICLE_DOORS_LOCKED(vehicle[vehJet].id,VEHICLELOCK_LOCKED)
				SET_VEHICLE_LIVERY(vehicle[vehJet].id,1)
				
			
				vehicle[vehSec1].id = CREATE_VEHICLE(TAILGATER,<<-984.58, -2998.80, 13.58>>,22.68)
				vehicle[vehSec2].id = CREATE_VEHICLE(TAILGATER,<<-992.74, -3031.48, 13.58>>,18.53)
				vehicle[vehSec3].id = CREATE_VEHICLE(TAILGATER,<<-990.95, -2997.46, 13.56>>,24.38)
										
			
	
				WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedDevin,CHAR_DEVIN,vehicle[vehJet].id,VS_BACK_RIGHT)
				OR NOT CREATE_NPC_PED_INSIDE_VEHICLE(ped[ped_cop4].id,CHAR_MOLLY,vehicle[vehJet].id,VS_BACK_LEFT)
					wait(0)
				ENDWHILE
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDevin,TRUE)								
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_cop4].id,TRUE)
				
				ped[ped_cop2].id = CREATE_PED_INSIDE_VEHICLE(vehicle[vehJet].id,PEDTYPE_MISSION,S_M_Y_DEVINSEC_01,VS_EXTRA_LEFT_2)
				ped[ped_cop3].id = CREATE_PED_INSIDE_VEHICLE(vehicle[vehJet].id,PEDTYPE_MISSION,S_M_Y_DEVINSEC_01,VS_EXTRA_LEFT_3)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_cop2].id,TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_cop3].id,TRUE)
				
				SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_cop2].id)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop2].id, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
				
				SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_cop3].id)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(ped[ped_cop3].id, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
				
				SET_PED_DEFAULT_COMPONENT_VARIATION(pedDevin)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
				SET_PED_COMPONENT_VARIATION(pedDevin, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)	
				
				SET_PED_PROP_INDEX(ped[ped_cop4].id, ANCHOR_EYES, 0, 0)
				
				ped[ped_civ1].id = CREATE_PED_INSIDE_VEHICLE(vehicle[vehJet].id,PEDTYPE_MISSION,S_M_M_PILOT_01,VS_DRIVER)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_civ1].id,TRUE)
				
				
				SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE) 
				
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
			BREAK
			
			case STAGE_GAME_OVER
				IF bIsReplay
					START_REPLAY_SETUP(<<-960.0240, -2757.6130, 12.9018>>,56.0)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-960.0240, -2757.6130, 12.9018>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),56.4926)
					LOAD_SCENE(<< -960.0240, -2757.6130, 12.9018 >>)
					SAFEWAIT(1)
				ENDIF
				FORCE_ASSET_STAGE(ASSETS_STAGE_GAME_OVER)
				CHANGE_PLAYER(SELECTOR_PED_FRANKLIN)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				
				DISABLE_CELLPHONE(FALSE)
				IF bIsReplay
					END_REPLAY_SETUP()
				ENDIF
			break
		ENDSWITCH
		if not IS_PED_INJURED(PLAYER_PED_ID())
			//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(),FALSE)
		ENDIF
		WAIT(0)
		
		IF NOT bDoNotFade
			do_fade(FALSE)
		ENDIF
		/*
		if IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(1000)
			while not IS_SCREEN_FADED_IN()
				WAIT(0)
			ENDWHILE
		ENDIF
		*/
		
	ENDIF
	
ENDPROC

/*
proc skipCutscene()
	if not DOES_ENTITY_EXIST(vehicle[vehChopper].id)
		vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<< -179.6574, 640.2711, 195.4092 >>,122.3)
		
		START_PLAYBACK_RECORDED_VEHICLE(vehicle[vehChopper].id,21,sVehicleRecordingLibrary)
		while not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehChopper].id)
			WAIT(0)
		ENDWHILE
		SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehicle[vehChopper].id,46500)					
	ELSE
		if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehChopper].id)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehicle[vehChopper].id,46500 - GET_TIME_POSITION_IN_RECORDING(vehicle[vehChopper].id))				
			ENDIF
		ENDIF
	ENDIF
	if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
		if not DOES_ENTITY_EXIST(ped[ped_trevor].id)
			ped[ped_trevor].id = CREATE_PED_INSIDE_VEHICLE(vehicle[vehChopper].id,PEDTYPE_MISSION,model_trevor)
	
		ENDIF
		if not DOES_ENTITY_EXIST(ped[ped_pilot].id)
			CREATE_CHOPPER_PILOT()
			SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_pilot].id,FALSE)
			//ped[ped_pilot].id = CREATE_PED_INSIDE_VEHICLE(vehicle[vehChopper].id,PEDTYPE_MISSION,model_pilot,VS_FRONT_RIGHT)
		ENDIF
	ENDIF
	if DOES_BLIP_EXIST(blipTarget)
		REMOVE_BLIP(blipTarget)
	ENDIF
	if not IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),<< -182.1347, 634.8387, 190.7370 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(),345)
		SET_PLAYER_CONTROL(PLAYER_ID(),true)
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)	
	missionProgress = STAGE_FRANKLIN_AWAITS_TREVOR
	iFlag = 0
	//END_CUTSCENE(FALSE)
ENDPROC
*/
//RUN_CAM_SPLINE_FROM_PLAYER_TO_CAM
//RUN_CAM_SPLINE_FROM_PLAYER_TO_PED
//SELECTOR_CAM_SHORT_SPLINE

//selector_Cam_Struct hotswapCam


func bool TrevorGetChopper()	
	
	ACTION(1,ACT_DO_TREVOR_STAND)
	SET_PED_RESET_FLAG(player_ped_id(),PRF_DisableAgitation,TRUE)

	

	SWITCH iFlag
	
		case 0 //get to the front desk
			SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_UNARMED, TRUE)
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			SET_MAX_WANTED_LEVEL(5)
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_DOWNTOWN_POLICE, TRUE)
			
			
			
			add_event(events_create_Franklin_and_car)
			add_event(events_waypoint_through_police_dept,STAGE_LEARN_TO_SCAN)

			IF GET_CLOCK_HOURS() < 21
			AND GET_CLOCK_HOURS() > 4
				REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				iflag = 1001
			ELSE
				FORCE_ACTION_STATE(1,ACT_DO_TREVOR_STAND,TRUE)
				iflag = 2
			ENDIF

		BREAK
		case 1001
			if REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				cs2Timelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
				ADD_CAM_SPLINE_NODE(cs2Timelapse.splineCamera,<<416.8213, -963.6541, 30.9451>>, <<22.2372, -0.0000, -121.6787>>,5000)
				ADD_CAM_SPLINE_NODE(cs2Timelapse.splineCamera,<<417.4168, -964.0216, 31.2312>>, <<22.2372, -0.0000, -121.6787>>,4500)

				SET_CAM_FOV(cs2Timelapse.splineCamera,46.25)
				SET_CAM_ACTIVE(cs2Timelapse.splineCamera,TRUE)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-4)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()

				iWhooshSound = GET_SOUND_ID() 
	            PLAY_SOUND_FRONTEND(iWhooshSound, "TIME_LAPSE_MASTER") 
	            START_AUDIO_SCENE("TOD_SHIFT_SCENE")
				iTrevBlocker = ADD_NAVMESH_BLOCKING_OBJECT(<<418.929260,-969.588623,29.411676>>,<<2.312500,3.437500,1.000000>>,0)
				ADD_SCENARIO_BLOCKING_AREA(<<418.929260,-969.588623,29.411676>>-<<2.312500,3.437500,1.000000>>,<<418.929260,-969.588623,29.411676>>+<<2.312500,3.437500,1.000000>>)
				
				CLEAR_AREA_OF_PEDS(<<420.26, -978.76, 28.83>>,15.625)
				copScenarioBlocker = ADD_SCENARIO_BLOCKING_AREA(<<417.65, -979.87, 28.43>>-<<16.625000,18.437500,7.875000>>,<<417.65, -979.87, 28.43>>+<<16.625000,18.437500,7.875000>>)
				
				iflag=1
			endif 
		break
		CASE 1
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_UNARMED, TRUE)
			IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(21, 00, "","", cs2Timelapse) 
			AND actions[1].flag = 2 //Trevor waiting scenario active			
				actions[1].flag = 3 //set running of Franklin sitting down anim			
				STOP_SOUND(iWhooshSound)
				STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				REMOVE_SCENARIO_BLOCKING_AREA(copScenarioBlocker)
				SET_ASSET_STAGE(ASSETS_STAGE_AFTER_LEADIN)
				iflag=2
			ENDIF
		BREAK
		CASE 2				
			checkConditions(COND_FAILING,COND_PLAYER_SPOTTED_BY_PILOT) //first condition, last condition
			
			INSTRUCTIONS_ON_ACTION(0,INS_GET_TO_RECEPTION,1,ACT_DO_TREVOR_STAND)
			//INSTRUCTIONS(1,INS_FOLLOW_COP,cIF,COND_COP_WALKING_TO_STAIRS,cAND,COND_CUTSCENE_ENDED)
			INSTRUCTIONS_ON_DIALOGUE(2,INS_GET_TO_ROOF,0,DIA_CAN_I_HELP)
			INSTRUCTIONS_ON_DIALOGUE(3,INS_GET_IN_CHOPPER,5,DIA_PILOT_SEES_PLAYER)
						 
			DIALOGUE(0,DIA_CAN_I_HELP)
			DIALOGUE(1,DIA_INTEROGATE_COP,cIF,COND_PLAYER_ENTERED_RECEPTION,cANDNOT,COND_PLAYER_ON_ROOF)
		//	DIALOGUE(1,DIA_COP_WALK_CHAT,cIF,COND_COP_IN_CORRIDOR,cAND,COND_PLAYER_NEARBY,cANDNOT,COND_PLAYER_IN_LOCKER_ROOM,cAND,COND_COP_LEADING_PLAYER)
		//	DIALOGUE_ON_DIALOGUE(2,DIA_COME_ON,4,DIA_PLAYER_ENTERS_LOCKER_ROOM,cIF,COND_PLAYER_IN_LOCKER_ROOM,cAND,COND_COP_LEADING_PLAYER)
			DIALOGUE_ON_ACTION(3,DIA_PLAYER_GETS_IN_CHOPPER,7,ACT_PLAYER_GETS_IN_CHOPPER)
		//	DIALOGUE(4,DIA_PLAYER_ENTERS_LOCKER_ROOM,cIF,COND_PLAYER_IN_LOCKER_ROOM)
			DIALOGUE(5,DIA_PILOT_SEES_PLAYER,cIF,COND_PLAYER_SPOTTED_BY_PILOT,cANDNOT,COND_PLAYER_IN_CHOPPER,cAND,COND_PLAYER_ON_ROOF)
		//	DIALOGUE(6,DIA_COP_RETURNS_TO_DESK,cIF,COND_PLAYER_RUNS_AHEAD)	
		//	DIALOGUE(7,DIA_RESTART_CONV,cIF,COND_COP_LEADING_PLAYER,cWasTrueNowFalse,COND_PLAYER_IN_LOCKER_ROOM,cAND,COND_COP_LEADING_PLAYER)
			DIALOGUE(8,DIA_PLAYER_THREAT_IN_POL_DEPT,cIF,COND_PLAYER_OUT_OF_BOUNDS)
			 
			ACTION(0,ACT_SPAWN_COPS) //now loads in lead-in or mission reload stage
			ACTION(2,ACT_LOOK_AT_PLAYER_ENTRY,playout_on_trigger,cIF,COND_PLAYER_ENTERED_RECEPTION)
			ACTION(3,ACT_POINT_TO_STAITRS)
			ACTION(4,ACT_COPS_ALERTED,playout_on_trigger,cIF,COND_COPS_ALERTED_CONDITION)
			ACTION(5,ACT_SPAWN_CHOPPER,playout_on_trigger,cIF,COND_CAN_SPAWN_CHOPPER)
			ACTION(6,ACT_PED_AT_VENDING_MACHINE_LEAVES)
			ACTION(7,ACT_PLAYER_GETS_IN_CHOPPER,playout_on_trigger,cIF,COND_PLAYER_TRYS_TO_ENTER_CHOPPER)
			ACTION(8,ACT_PILOT_FLIES_OFF,playout_on_trigger,cIF,COND_TREVOR_TELLS_PILOT_TO_TAKE_OFF,cAND,COND_PLAYER_IN_CHOPPER)
			//ACTION(9,ACT_COP_WAITS_FOR_PLAYER,playout_on_cond,cIF,COND_PLAYER_IN_LOCKER_ROOM,cAND,COND_COP_LEADING_PLAYER)
			ACTION(10,ACT_LOCK_GUN_ROOM)
			ACTION_ON_DIALOGUE(12,ACT_PILOT_LOOKS_AT_TREVOR,5,DIA_PILOT_SEES_PLAYER)
			
			FAIL(FAIL_CHOPPER_DESTROYED)
			FAIL(FAIL_PLAYER_HAS_WEAPON_DRAWN_IN_POL_DEPT)
			FAIL(FAIL_HAS_WANTED_LEVEL_IN_POLICE_DEPT)
			FAIL(FAIL_COP_DIES)
			FAIL(FAIL_PLAYER_SHOOTING_NEAR_POLICE_DEPT)
			FAIL(FAIL_PLAYER_CREATED_DISTURBANCE_NEAR_POL_DEPT)
			FAIL(FAIL_COPS_IN_COMBAT)
			
			FAIL(FAIL_PLAYER_WANDERS_AWAY_FROM_POLICE_DEPT)
			FAIL(FAIL_PILOT_FLEW_AWAY)
				
			IF IS_CONDITION_TRUE(COND_TREVOR_TELLS_PILOT_TO_ACTIVATE_SCANNER)
			
				//remove cops and stuff
				IF DOES_ENTITY_EXIST(ped[ped_civ1].id) DELETE_PED(ped[ped_civ1].id) ENDIF
				IF DOES_ENTITY_EXIST(ped[ped_civ2].id) DELETE_PED(ped[ped_civ2].id) ENDIF
				IF DOES_ENTITY_EXIST(ped[ped_civ3].id) DELETE_PED(ped[ped_civ3].id) ENDIF
				IF DOES_ENTITY_EXIST(ped[ped_civ4].id) DELETE_PED(ped[ped_civ4].id) ENDIF
				IF DOES_ENTITY_EXIST(ped[ped_civ5].id) DELETE_PED(ped[ped_civ5].id) ENDIF
				IF DOES_ENTITY_EXIST(ped[ped_cop1].id) DELETE_PED(ped[ped_cop1].id) ENDIF
				IF DOES_ENTITY_EXIST(ped[ped_cop2].id) DELETE_PED(ped[ped_cop2].id) ENDIF
				IF DOES_ENTITY_EXIST(ped[ped_cop3].id) DELETE_PED(ped[ped_cop3].id) ENDIF
				IF DOES_ENTITY_EXIST(ped[ped_cop4].id) DELETE_PED(ped[ped_cop4].id) ENDIF
				
			
				IF IS_VALID_INTERIOR(int_police)
					UNPIN_INTERIOR(int_police)
				ENDIF
				
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_MAX_WANTED_LEVEL(0)
				RETURN TRUE
			ENDIF		
		BREAK
	ENDSWITCH


	return false
endfunc

func bool getInChopper()	

	checkConditions(COND_START_FRANKLIN,COND_END_FRANKLIN)

	ACTION(0,ACT_CLEAR_SWITCH_INSTRUCTIONS)
	ACTION(1,ACT_LOAD_STAND_ANIM)
	ACTION(2,ACT_BLOCK_CAM_MODE_CHANGE)
	dialogue(0,dia_player_doesnt_switch)
	
	INSTRUCTIONS(0,INS_ABANDON_TREVOR,cIF,COND_PLAYER_LEAVING_TREVOR)

	SWITCH iFlag
		case 0
		
			IF GET_CLOCK_HOURS() < 21
			AND GET_CLOCK_HOURS() > 4
				if REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
					DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					cs2Timelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
					ADD_CAM_SPLINE_NODE(cs2Timelapse.splineCamera,<<1393.3628, -2052.5813, 65.4054>>, <<2.5107, -0.0000, 51.1167>>,5000)
					ADD_CAM_SPLINE_NODE(cs2Timelapse.splineCamera,<<1393.4564, -2052.6563, 68.1460>>, <<2.5107, -0.0000, 51.1167>>,6000)
					SET_CAM_FOV(cs2Timelapse.splineCamera,35.9859)
					SET_CAM_ACTIVE(cs2Timelapse.splineCamera,TRUE)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)

					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-4)
					
					iWhooshSound = GET_SOUND_ID() 
	                PLAY_SOUND_FRONTEND(iWhooshSound, "TIME_LAPSE_MASTER") 
	                START_AUDIO_SCENE("TOD_SHIFT_SCENE")
					
					REQUEST_MODEL(polmav)
					REQUEST_MODEL(model_pilot)
					REQUEST_SCALEFORM_MOVIE("heli_cam")
					REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
					iflag++
				endif
			ELSE
				FORCE_ACTION_STATE(1,ACT_LOAD_STAND_ANIM,TRUE)
				REQUEST_MODEL(polmav)
				REQUEST_MODEL(model_pilot)
				iflag = 2
			ENDIF
			
		break
		case 1
			SKIP_TO_TIME_DURING_SPLINE_CAMERA(21, 00, "","", cs2Timelapse) 
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			IF NOT IS_CAM_INTERPOLATING(cs2Timelapse.splineCamera)
			AND actions[1].flag = 2 //Franklin seated anims loaded		
				actions[1].flag = 3 //set running of Franklin sitting down anim			
				STOP_SOUND(iWhooshSound)
				stop_audio_scene("TOD_SHIFT_SCENE")
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
				iflag=3
			ENDIF
		break
		case 2
			IF HAS_MODEL_LOADED(model_pilot)
			AND HAS_MODEL_LOADED(polmav)
				iflag++
			ENDIf
		break
		case 3
			add_event(EVENTS_KEEP_CHAD_SAFE,STAGE_GAME_OVER,true)
			//SET_GAMEPLAY_ENTITY_HINT()
			
			//find safe coord to spawn chopper
			IF NOT IS_SPHERE_VISIBLE(<<1365.1155, -2147.4495, 85.3603>>,10)			
				vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<<1365.1155, -2147.4495, 75.3603>>,-8.3)			
			ELIF NOT IS_SPHERE_VISIBLE(<<1289.7468, -2068.5752, 88.1654>>,10)
				vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<<1289.7468, -2068.5752, 78.1654>>,-93)
			ELIF NOT IS_SPHERE_VISIBLE(<<1289.7468, -2068.5752, 88.1654>>,10)
				vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<<1443.8966, -2067.7009, 78.8426>>,86)
			ELSE
				vehicle[vehChopper].id = CREATE_VEHICLE(POLMAV,<<1406.2607, -2003.5839, 74.7528>>,158)
			ENDIF
			SET_VEHICLE_LIVERY(vehicle[vehChopper].id,0)
			SET_ENTITY_VELOCITY(vehicle[vehChopper].id,<<0,15,0>>)
											
			SET_VEHICLE_RADIO_ENABLED(vehicle[vehChopper].id,FALSE)
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
				SET_HELI_BLADES_FULL_SPEED(vehicle[vehChopper].id)
			ENDIF
			
			//add_event(events_chopper_at_Start)

			CREATE_CHOPPER_PILOT()
			
			SET_VEHICLE_DOORS_LOCKED(vehicle[vehChopper].id,VEHICLELOCK_FORCE_SHUT_DOORS)
			SET_PED_COMBAT_ATTRIBUTES(ped[ped_pilot].id,CA_LEAVE_VEHICLES,FALSE)
			
			TASK_HELI_MISSION(ped[ped_pilot].id,vehicle[vehChopper].id,null,null,<<1359.7988, -2074.8599, 74.8290>>,MISSION_GOTO,15,10,-1,0,20)
		//	CREATE_TREVOR(<< 1376.0630, -2069.8489, 50.9988 >>,340,vehicle[vehChopper].id,VS_FRONT_RIGHT)
			
			
			TASK_LOOK_AT_ENTITY(player_ped_id(),vehicle[vehChopper].id,10000)
			
			ped[ped_franklin].id = PLAYER_PED_ID()					
			
			IF NOT IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
				vehicle[vehFranklin].id = GET_PLAYERS_LAST_VEHICLE()
			ENDIF
			
			//cprintln(debug_trevor3,"PLayer's last car = ",native_to_int(vehicle[vehFranklin].id))
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
				//cprintln(debug_trevor3,"PLayer's last car is driveable")
				SET_ENTITY_AS_MISSION_ENTITY(vehicle[vehFranklin].id,TRUE,TRUE) //if player drove in in a random car, do this to ensure it doesn't get cleared up.
			ENDIF
			
			SET_PED_CAN_BE_DRAGGED_OUT(ped[ped_pilot].id,FALSE)
			IF IS_ACTION_COMPLETE(1,ACT_LOAD_STAND_ANIM)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)								
					SET_GAMEPLAY_ENTITY_HINT(vehicle[vehChopper].id,<<0,0,-7>>,TRUE,6000,5000,5000)						
				ENDIF
			ENDIF
			iFlag=4
		BREAK
		CASE 4
		//	initVehicle(vehFranklin,BUFFALO,<< 1388.0295, -2067.3911, 50.9981 >>,0)										
			if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)		
				IF CREATE_PLAYER_PED_INSIDE_VEHICLE(ped[ped_trevor].id,CHAR_TREVOR,vehicle[vehChopper].id,VS_FRONT_RIGHT)				
					TASK_LOOK_AT_ENTITY(ped[ped_trevor].id,player_ped_id(),60000)
									
					iFlag=5
				ENDIF											
			ENDIF			
		break
		case 5
			IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_chopper",1,ped[ped_franklin].id,"Franklin",0,ped[ped_Trevor].id,"Trevor")			
				iflag = 21
			ENDIF
		BREAK
		CASE 21
//			IF IS_THIS_CONVERSATION_ROOT_PLAYING("CS2_chopper")
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iFlag=6
				
			ENDIF
		break
		case 6	
			//if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				if get_print_status("CH_INS40") = PRN_NEVER_DISPLAYED
					settimerB(0)
					PRINT_NOW("CH_INS40",DEFAULT_GOD_TEXT_TIME,1)
				ENDIF
				SET_SELECTOR_PED_PRIORITY(sSelectorPeds,selector_ped_trevor,SELECTOR_PED_FRANKLIN,SELECTOR_PED_MICHAEL)
				
				bool bForceSwitch
				IF timerb() > 10000
				
					
					bForceSwitch = true
				endif
				
				
				
				if do_switch(ped[ped_franklin].id,ped[ped_trevor].id,null,SELECTOR_PED_TREVOR,SWITCH_TYPE_SHORT,bForceSwitch)
				
					
				
					UPDATE_PLAYER_PED_BLIP_NAME()
					SET_PLAYER_CONTROL(PLAYER_ID(),false,SPC_LEAVE_CAMERA_CONTROL_ON)															
					clear_prints()
					
					IF IS_PED_IN_ANY_VEHICLE(ped[ped_Franklin].id)
						vehicle_index aVeh
						aVeh = get_Vehicle_ped_is_using(ped[ped_Franklin].id)
						IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(aVeh))
						OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(aVeh))
							IF IS_ENTITY_IN_AIR(aVeh)
								SET_ENTITY_COORDS(aVeh,<<1371.3750, -2068.7754, 50.9982>>)
								SET_ENTITY_HEADING(aVeh,124)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
						SET_ENTITY_HEALTH(vehicle[vehChopper].id,1000)
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-6)
					return true
				ENDIF
				
				IF switchState = SWITCH_SWAP_PLAYER_PEDS
					
					IF IS_GAMEPLAY_HINT_ACTIVE()
						STOP_GAMEPLAY_HINT(true)
					ENDIF
				ENDIF
				
		//	ENDIF
		break
		
		/*
		case 3
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				return true
			ENDIF
		BREAK
		*/
		/*
		case 3
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				if not IS_ENTITY_DEAD(vehicle[vehChopper].id) and not IS_ENTITY_DEAD(PLAYER_PED_ID())
					if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id)
						if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
							PRINT_NOW("CH_INS1",6000,1)
						ENDIF
					ELSE
						return TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		*/
	ENDSWITCH
	return FALSE
ENDFUNC

func bool learnToScan()
	//ped_index pedscanned
	//int iTemp
	//vector vTemp
	//float tx,ty
	checkConditions(COND_SCAN_STAGE_START,COND_SCAN_STAGE_END)
	
	DIALOGUE(0,DIA_BOOTING_UP)
	DIALOGUE(1,DIA_SCAN_BUDDY_NEARBY,cIFNOT,COND_STARTED_AS_TREVOR,cAND,COND_LEARNT_TO_SCAN)
	DIALOGUE_ON_DIALOGUE(2,DIA_CONV_ON_WAY_TO_FRANKLIN,6,DIA_ACTOR_LINE_PLAYS,cIF,COND_STARTED_AS_TREVOR,cAND,COND_FAR_ENOUGH_FROM_FRANKLIN_TO_PLAY_CONVO)	
	DIALOGUE_ON_DIALOGUE(3,DIA_SCAN_MY_BUDDY,5,DIA_GOING_TO_FIND_BUDDY,cIF,COND_LEARNT_TO_SCAN,cAND,COND_STARTED_AS_TREVOR,cAND,COND_APPROACHING_FRANKLIN)
	DIALOGUE(4,DIA_FRANKLIN_SCANNED,cIF,COND_FRANKLIN_SCANNED)
	DIALOGUE(5,DIA_GOING_TO_FIND_BUDDY,cIF,COND_STARTED_AS_TREVOR)
	DIALOGUE(6,DIA_ACTOR_LINE_PLAYS,cIF,COND_STARTED_AS_TREVOR)
	DIALOGUE(7,DIA_FLYING_AWAY,cIF,COND_TOLD_TO_SCAN_FRANKLIN,cAND,COND_OUTSIDE_SAFE_ZONE)
	DIALOGUE(8,DIA_WAITING_TO_SCAN,cIF,COND_DIA_SCAN_MY_BUDDY,cANDNOT,COND_FRANKLIN_SCANNED,cANDNOT,COND_FAR_ENOUGH_FROM_FRANKLIN_TO_PLAY_CONVO)

	ACTION(0,ACT_TURN_ON_SCANNER,PLAYOUT_ON_TRIGGER,cIF,COND_BOOTING_UP_LINE_PLAYED)
	ACTION(1,ACT_MUSIC_SCANNER_ON,PLAYOUT_ON_TRIGGER,cIF,COND_BOOTING_UP_LINE_PLAYED)
	ACTION(2,ACT_FRANKLIN_LOOKS_AT_CHOPPER)//,PLAYOUT_ON_COND,cIFNOT,COND_FRANKLIN_SCANNED)
	ACTION(3,ACT_FRANKLIN_REACTS_TO_TREVOR_CONVERSATION,PLAYOUT_ON_COND,cIF,COND_FRANKLIN_SCANNED,cANDNOT,COND_DIA_SCANNED_CONVO_FINISHED)
	ACTION_ON_DIALOGUE(4,ACT_FRAKNLIN_GETS_IN_CAR,4,DIA_FRANKLIN_SCANNED)
	ACTION(5,ACT_SCANNING_AUDIO_SCENE)
	//ACTION(6,ACT_LOAD_PATH_NODES)
	ACTION(7,ACT_CREATE_PLAYER_CAR)
	action(8,ACT_CHANGE_FRANKLIN_LOD_LEVEL_UP)

	
	INSTRUCTIONS(0,INS_TEACH_TO_SCAN,cIF,COND_BOOTING_UP_LINE_PLAYED)
	INSTRUCTIONS(1,INS_SCAN_FRANKLIN,cIF,COND_DIA_SCAN_MY_BUDDY)
	INSTRUCTIONS(2,INS_SCAN_COMPLETE,cIF,COND_FRANKLIN_SCANNED)
	
	FAIL(FAIL_FLEW_TOO_FAR_AWAY)
	
	IF HAS_DIALOGUE_FINISHED(4,DIA_FRANKLIN_SCANNED)
		REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_franklin].id)
		return true
	ENDIF

	return FALSE
ENDFUNC

func bool scanFirstArea()

	checkConditions(COND_START_SCAN_AREA_1,COND_END_SCAN_AREA_1)
	ACTION(0,ACT_STOP_AUDIO_CAR_2_SCAN_FRANKLIN)
	ACTION(1,ACT_SCANNING_AUDIO_SCENE)
	ACTION(2,ACT_DELETE_FRANKLIN)
	action(3,ACT_CHANGE_FRANKLIN_LOD_LEVEL_DOWN,PLAYOUT_ON_TRIGGER,cIF,COND_CHOPPER_IN_VIEWING_RANGE_OF_AREA1)
	//ACTION(3,ACT_LOAD_PATH_NODES)
	
	DIALOGUE(0,DIA_TREVOR_COMMENTS_ON_POSTAL,cIF,COND_POSTAL_BEING_OBSERVED,cANDNOT,COND_LISTENING_IN_ON_DIALOGUE,cAND,COND_DIA_COME_IN_FRANKLIN_PLAYED,cAND,COND_CHOPPER_IN_VIEWING_RANGE_OF_AREA1)
	DIALOGUE(1,DIA_TREVOR_COMMENTS_ON_PERVERT,cIF,COND_PERVERT_BEING_OBSERVED,cANDNOT,COND_LISTENING_IN_ON_DIALOGUE,cAND,COND_DIA_COME_IN_FRANKLIN_PLAYED,cAND,COND_CHOPPER_IN_VIEWING_RANGE_OF_AREA1)
	DIALOGUE(2,DIA_TREVOR_MAKES_SCAN_COMPLETE_COMMENT,cIF,COND_AREA1_PED_JUST_SCANNED)
	
	SWITCH iFlag
			
		CASE 0												
			if CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_letsGo",0,ped[ped_Trevor].id,"Trevor",2,null,"ChopperPilot")
			//	destMarker[0] = ADD_HELIHUD_MARKER_FOR_COORD(HUD, << -33.9675, -85.8839, 57.0174 >>,hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b,MARKER_BOX)			
				destMarker[0] = ADD_HELIHUD_MARKER_FOR_COORD(HUD, << -28.7640, -88.8056, 57.2592 >>,hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b,MARKER_BOX)			
				destMarker[1] = ADD_HELIHUD_MARKER_FOR_COORD(HUD, << -7.1245, -29.9020, 69.0970 >>,hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b,MARKER_BOX)			
				destMarker[2] = ADD_HELIHUD_MARKER_FOR_COORD(HUD, << -17.5966, -36.9055, 76.1669 >>,hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b,MARKER_BOX)			
				destMarker[3] = ADD_HELIHUD_MARKER_FOR_COORD(HUD, << -17.8836, -36.5907, 76.3757 >>,hud.boxColourUnscanned.r,hud.boxColourUnscanned.g,hud.boxColourUnscanned.b,MARKER_BOX)			
				
				set_chopper_speed_limits(HUD)					
				add_event(EVENTS_LOAD_FIRST_LOCATION_PEDS)
				add_event(EVENTS_QUAD_BLOCKING,STAGE_CHASE_BEGINS)
				add_event(EVENTS_SCAN_MUSIC_CHANGE)
				
				iFlag = 1
			ENDIF
		BREAK
		
		case 1
			IF IS_CONV_ROOT_PLAYING("CS2_letsGo")
				iFlag = 2
			ENDIF
		BREAK
		
		CASE 2
			if not IS_CONV_ROOT_PLAYING("CS2_letsGo")
				PRINT_NOW("CH_INS12",DEFAULT_GOD_TEXT_TIME,1)
				iFlag = 3
			ENDIF
		BREAK
		
		
		case 3
			if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_INS12")
				add_event(EVENTS_CONVERSATION_ON_ROUTE)
				iFlag = 9
			ENDIF
		BREAK
		
		case 9

			if  is_event_complete(EVENTS_LOAD_FIRST_LOCATION_PEDS)
				kill_event(EVENTS_STREAM_ALLEY)
				
				SET_ROADS_IN_ANGLED_AREA(<<-48.346008,-109.031822,82.549194>>, <<195.550781,-197.283768,-38.864712>>, 155.812500,FALSE,FALSE)
				//if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//REMOVE_BLIP(blipTarget)
					
				return true
				//ENDIF
			ENDIF
		break
		

	ENDSWITCH
	return FALSE
ENDFUNC

func bool scanThirdArea()

	checkConditions(COND_START_SCAN_AREA_3,COND_END_SCAN_AREA_3)
	
	action(0,ACT_RELEASE_AREA_1_ASSETS)
	//action(1,ACT_SPAWN_ZTYPE)
	action(2,ACT_DOG_BARKS)
	ACTION(3,ACT_SCANNING_AUDIO_SCENE)
	ACTION(4,ACT_STOP_CAR_2_SCAN_THE_SUSPECTS)
	action(5,ACT_GARAGE_OPENING_SOUND,PLAYOUT_ON_TRIGGER,cIF,COND_GARAGE_DOOR_OPENING)
	action(6,ACT_TARGET_LOST_SOUND)
	
	DIALOGUE(0,DIA_SEEN_CHAD,cIF,COND_CHAD_WAS_SCANNED)
	DIALOGUE(1,DIA_CHAD_HIDDEN,cIF,COND_CHAD_HIDDEN,cANDNOT,COND_CHAD_BY_GARAGES,cAND,COND_CHAD_WAS_SCANNED)
	DIALOGUE_ON_DIALOGUE(4,DIA_FRANKLIN_FIND_HIM,3,DIA_CHAD_OFF_SCREEN)
	DIALOGUE_ON_DIALOGUE(5,DIA_FOUND_CHAD,3,DIA_CHAD_OFF_SCREEN,cIFNOT,COND_CHAD_OFFSCREEN_FOR_4_SECONDS)
	DIALOGUE(2,DIA_CHAD_WALKING_THROUGH_APARTMENTS,cIF,COND_CHAD_WAS_SCANNED,cANDNOT,COND_CHAD_HIDDEN,CAND,COND_CHAD_IN_APARTMENT_LOCATE,cAND,COND_CHAD_ON_SCREEN)
	DIALOGUE(3,DIA_CHAD_OFF_SCREEN,cIF,COND_CHAD_OFFSCREEN_FOR_4_SECONDS,cAND,COND_CHAD_WAS_SCANNED)
	
	DIALOGUE_ON_DIALOGUE(6,DIA_TREVOR_BANTER_DURING_CHAD_WALK,0,DIA_SEEN_CHAD,cIFNOT,COND_CHAD_OFFSCREEN_FOR_4_SECONDS)
	DIALOGUE(7,DIA_CHAD_AT_GARAGE,cIF,COND_GARAGE_DOOR_OPENING)
	
	DIALOGUE(8,DIA_TREVOR_COMMENTS_ON_PROSTITUTE,cIF,COND_PROSIE_BEING_OBSERVED,cANDNOT,COND_LISTENING_IN_ON_DIALOGUE,cANDNOT,COND_CHAD_WAS_SCANNED)
	DIALOGUE(9,DIA_TREVOR_COMMENTS_ON_CHADGIRL,cIF,COND_CHAD_BEING_OBSERVED,cANDNOT,COND_LISTENING_IN_ON_DIALOGUE,cANDNOT,COND_CHAD_WAS_SCANNED)
	DIALOGUE(10,DIA_TREVOR_MAKES_SCAN_COMPLETE_COMMENT,cIF,COND_AREA3_PED_JUST_SCANNED)
	

	
	INSTRUCTIONS_ON_DIALOGUE(0,INS_FOLLOW_CHAD,0,DIA_SEEN_CHAD)
	instructions(1,INS_SHOW_HIDDEN_HELP,cIF,COND_CHAD_HIDDEN,cANDNOT,COND_CHAD_BY_GARAGES,cAND,COND_CHAD_WAS_SCANNED)

	SWITCH iFlag
		case 0
		//	destMarker[0] = ADD_HELIHUD_MARKER_FOR_COORD(HUD, << 200.3937, -141.4813, 80.8976 >>)
			add_event(EVENTS_LOAD_FINAL_LOCATION_PEDS)		
			//PRINT_NOW("CH_INS12C",6000,1)
			iFlag =1	
		break
		
		case 1
			if is_event_complete(EVENTS_LOAD_FINAL_LOCATION_PEDS)
				if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					fSpeed = GET_ENTITY_SPEED(vehicle[vehChopper].id)
				ENDIF
				
				//add_event(events_is_chad_hidden)
			
				KILL_FACE_TO_FACE_CONVERSATION_EXTRA(true)
				SET_CONDITION_STATE(COND_CHAD_WAS_SCANNED,TRUE)
				iFlag =15
			ENDIF
		break
	
		case 15
			//check ped is on screen when he reaches car
			/*if get_print_status("CH_INS13") = PRN_FINISHED
				if get_conversation_status("CS2_seeChad2") = PRN_NEVER_DISPLAYED
			
					iFlag=16
				ENDIF
			ENDIF*/
			
			if not IS_PED_INJURED(ped[ped_chad].id)
				if IS_ENTITY_IN_ANGLED_AREA( ped[ped_chad].id, <<191.634933,-195.268723,43.702320>>, <<215.426270,-130.483856,91.142548>>, 31.625000)
				
					//he's heading through appartment blocks
					SET_ASSET_STAGE(ASSETS_STAGE_LOAD_ZTYPE)
					iFlag=16
				ENDIF
			ENDIF
		BREAK
		
		case 16 
			if bplayVehRecs = true
				SET_ASSET_STAGE(ASSETS_STAGE_RELEASE_AREA_2)
				set_event_flag(EVENTS_SPEED_UP_CHOPPER,2)
				kill_event(events_scene_chadGirl)										
				kill_event(events_scene_pimp)
				kill_event(events_bum_on_chad_walk)
				kill_event(events_dog_walks_by)
				return TRUE
			ENDIF
		break
		
	ENDSWITCH
	return FALSE
ENDFUNC


func bool chaseChad()	
	//SEQUENCE_INDEX seq
	
	action(0,act_block_peds_on_chase_route)
	action(1,ACT_AUDIO_SCENE_TRANSITION)
	action(2,action_load_audio_scene)
	action(3,act_update_target_car_audio_level)
	action(4,act_remove_trees)


//	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0,0)
	
	SWITCH iFlag
		CASE 0																														
			
			HUD.bBlockTargetLost = TRUE
			HUD.fTargetHeight = 90
			set_chopper_speed_limits(HUD,0,38)
			add_event(EVENTS_PRINT_CHADBUST_MESSAGE,STAGE_CARPARK)
		
			add_event(events_chopper_push,STAGE_CARPARK)
			add_event(events_change_playback_speed)
			add_event(EVENTS_FAIL_CHASE,STAGE_CARPARK)
	
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
				SET_ENTITY_ALWAYS_PRERENDER(vehicle[vehFranklin].id,TRUE)
			ENDIF
	
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
				IF IS_VEHICLE_SEAT_FREE(vehicle[vehChopper].id,VS_DRIVER)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehicle[vehChopper].id,VS_DRIVER)
				ENDIf
			ENDIF
			if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
				SET_ENTITY_INVINCIBLE(vehicle[vehHeist].id,true)
			ENDIF
			PRINT_HELP("CH_HLP02")
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			iFlag=200
		BREAK
			
		case 200
			CONTROL_CHASE_CHATTER()
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehHeist].id)
					IF GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehicle[vehHeist].id) = GET_VEHICLE_RECORDING_ID(402, "cs2")					
						IF GET_TIME_POSITION_IN_RECORDING(vehicle[vehheist].id) >= 43913
							add_event(EVENTS_FILL_CARPARK,STAGE_DRIVE_VEHICLE_TO_OBJECTIVE)
							iflag=2
						endif
					endif
				endif
			endif
		break
		case 2
			CONTROL_CHASE_CHATTER()
			IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle[vehHeist].id)
					IF GET_TIME_POSITION_IN_RECORDING(vehicle[vehheist].id) >= 70913
						IF HAS_CHAD_BEEN_OBSERVED()
							iFlag=6
							SETTIMERA(0)
						ELSE
							SETTIMERA(0)
							iFlag=3
						ENDIF
						PLAY_MUSIC(mus_chad_drives_down_alley,mus_get_car_to_objective)
					ENDIF
				ENDIF
			ENDIF
		break
		
		case 3 //chad not seen entering car park
			IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_tmp16",1,null,"Franklin") //where is he man?			
				STOP_AUDIO_SCENE("CAR_2_CAR_CHASE_CONTINUED")
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle[vehHeist].id)
				STOP_AUDIO_SCENE("CAR_2_Z_TYPE_ENGINE_BOOST")
				
				START_AUDIO_SCENE("CAR_2_CAR_ENTERS_GARAGE")
				SETTIMERA(0)
				iFlag=4
			ENDIF
		BREAK
		
		CASE 4 //give player five seconds to observe parking garage
			IF TIMERA() > 5000
				Mission_Failed("CH_F20")
			ELSE
				IF IS_POINT_VISIBLE(<<-1246.2971, -239.1974, 42.4411>>,50,300)
					SETTIMERA(0)
					iFlag=5
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_cp2b",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")															
				SET_ASSET_STAGE(ASSETS_STAGE_END_CAR_CHASE)
				RETURN TRUE
			ENDIF
		BREAK
		
		case 6 //suspect sighted entering parking garage
			IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"CS2_carpark2",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")			
				STOP_AUDIO_SCENE("CAR_2_CAR_CHASE_CONTINUED")
				STOP_AUDIO_SCENE("CAR_2_Z_TYPE_ENGINE_BOOST")
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehicle[vehHeist].id)
				START_AUDIO_SCENE("CAR_2_CAR_ENTERS_GARAGE")
				SET_ASSET_STAGE(ASSETS_STAGE_END_CAR_CHASE)
				
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehFranklin].id)
					SET_ENTITY_ALWAYS_PRERENDER(vehicle[vehFranklin].id,FALSE)
				ENDIF
				
				RETURN TRUE
			ENDIF
		break
		
		case 7
		//	IF CREATE_CONVERSATION_EXTRA(CONVTYPE_GAMEPLAY,"cs2_cp2c",0,ped[ped_trevor].id,"Trevor",1,null,"Franklin")
		//		RETURN TRUE
		//	ENDIF
		break

	ENDSWITCH
	
	return FALSE
ENDFUNC
//vector vChopRot, vChopPos

func bool carPark()
	//float sx,sy
	checkConditions(COND_CARPARK_STARTS,COND_CAR_PARK_ENDS)
	
	
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0,0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
	INSTRUCTIONS(0,INS_TURN_ON_THERMAL_VISION,cIF,COND_DIA_THERMAL_VISION)
	INSTRUCTIONS(1,INS_ZOOM_IN_ON_FRANKLIN,cIF,COND_DIA_CAN_YOU_SEE_FRANKLIN,cANDNOT,COND_FRANKLIN_OBSERVED_WITH_THERMAL)
	INSTRUCTIONS(2,INS_LOOK_FOR_A_HEAT_SOURCES,cIF,COND_DIA_ANY_OTHER_HEAT_SOURCES)
	INSTRUCTIONS(3,INS_LOOK_FOR_ANOTHER_HEAT_SOURCES,cIF,COND_WAITING_FOR_NEXT_TARGET)
	//INSTRUCTIONS(4,INS_LOOK_FOR_ANOTHER_HEAT_SOURCES,cIF,COND_DIA_FRANKLIN_SEES_FIXING_MAN)
	//INSTRUCTIONS(5,INS_SWITCH_TO_FRANKLIN,cIF,COND_DIA_FRANKLIN_SEES_CHAD)
 
	DIALOGUE(0,DIA_TAKE_US_DOWN,cIF,COND_CARPARK_STARTS)
	DIALOGUE(1,DIA_TREVOR_SCARED,cIF,COND_CHOPPER_DESCENDING)
	DIALOGUE(2,DIA_EXITS_CLEAR,cIF,COND_CHOPPER_AT_CAR_PARK_LEVEL)
	DIALOGUE(3,DIA_THERMAL_VISION,cIF,COND_A_LITTLE_AFTER_EXITS_CLEAR_DIA) //CS2_Carpark3
	DIALOGUE(4,DIA_TREVOR_REACTION_TO_THERMAL_VISION_ON,cIF,COND_THERMAL_TURNED_ON) //CS2_carpark4
	DIALOGUE_ON_DIALOGUE(5,DIA_FRANKLIN_ASKS_IF_PLAYER_CAN_SEE_HIM,4,DIA_TREVOR_REACTION_TO_THERMAL_VISION_ON) //cs2_onme
	DIALOGUE(6,DIA_ANY_OTHER_HEAT_SOURCES,cIF,COND_DIA_TREVOR_SEE_FRANKLIN_ENDED) //cs2_looknow
	
	DIALOGUE_Playout(7,DIA_TREVOR_SEES_PISSER,cIF,COND_SEE_WANKER)
	DIALOGUE_Playout(8,DIA_TREVOR_SEES_FIXING_MAN,cIF,COND_SEE_FIXING_MAN) //cs2_mana1
	DIALOGUE_Playout(9,DIA_TREVOR_SEES_CHAD,cIF,COND_SEE_CHAD) //cs2_manc1	
	DIALOGUE_Playout(19,DIA_TREVOR_SEES_LEANING_MAN,cIF,COND_SEE_LEANING_MAN)
	DIALOGUE_Playout(20,DIA_TREVOR_SEES_PHONE_CAR_MAN,cIF,COND_SEE_PHONE_CAR_MAN)
	
	DIALOGUE_ON_ACTION(10,DIA_FRANKLIN_SEES_PISSER,6,ACT_FRANKLIN_RUNS_TO_PISSER_CAR) 
	DIALOGUE_ON_ACTION(11,DIA_FRANKLIN_SEES_FIXING_MAN,5,ACT_FRANKLIN_RUNS_TO_BONNET_CAR)
	DIALOGUE_ON_ACTION(21,DIA_FRANKLIN_SEES_LEANING_MAN,14,ACT_FRANKLIN_RUNS_TO_LEANING_MAN)
	DIALOGUE_ON_ACTION(22,DIA_FRANKLIN_SEES_PHONE_MAN,15,ACT_FRANKLIN_RUNS_TO_PHONE_MAN)	
	DIALOGUE(12,DIA_FRANKLIN_SEES_ZTYPE,cIF,COND_DIA_FROM_FRANKLIN_SEEING_CHAD)
	
	DIALOGUE(13,DIA_TREVOR_SEE_FRANKLIN,cIF,COND_FRANKLIN_OBSERVED_WITH_THERMAL)
	DIALOGUE(14,DIA_OVERHEAR_CAR_FIXER_WITH_SCANNER)
	DIALOGUE(15,DIA_OVERHEAR_PISSER_WITH_SCANNER)
	DIALOGUE(16,DIA_OVERHEAR_CHAD_WITH_SCANNER)
	DIALOGUE(17,DIA_OVERHEAR_CAR_LEANING_GUY)
	DIALOGUE(18,DIA_OVERHEAR_CAR_ON_PHONE)
	
	DIALOGUE_ON_DIALOGUE(23,DIA_TREVOR_RANTS_IN_SWITCH,9,DIA_TREVOR_SEES_CHAD) //,cif,COND_SWITCH_BEGUN)

	ACTION(0,ACT_CHOPPER_CONTROL,PLAYOUT_ON_TRIGGER,cIF,COND_DIA_TAKE_US_DOWN)	
	ACTION(1,ACT_CAR_HAS_PISS,PLAYOUT_ON_TRIGGER,cIF,COND_CARPARK_STARTS)	
	ACTION(2,ACT_CAR_FIXER,PLAYOUT_ON_TRIGGER,cIF,COND_CARPARK_STARTS)
	ACTION(3,ACT_FRANKLIN_RUNS_TO_ROOF,PLAYOUT_ON_COND,cIF,COND_FRANKLIN_STOPPED_HIS_CAR,cANDNOT,COND_INVESTIGATING_NEXT_TARGET)
	//ACTION(4,ACT_FRANKLIN_WAVES_AT_CHOPPER_CAMERA,PLAYOUT_ON_TRIGGER,cIF,COND_FRANKLIN_HAS_STOPPED_RUNNING,cANDNOT,COND_DIA_LOOK_FOR_MORE_HEAT_PLAYED)
	
	ACTION(4,ACT_STAT_CS2_SCANMAN,PLAYOUT_ON_TRIGGER,cIF,COND_SEE_CHAD)
	
	ACTION_ON_DIALOGUE(5,ACT_FRANKLIN_RUNS_TO_BONNET_CAR,8,DIA_TREVOR_SEES_FIXING_MAN)
	ACTION_ON_DIALOGUE(6,ACT_FRANKLIN_RUNS_TO_PISSER_CAR,7,DIA_TREVOR_SEES_PISSER)
	//ACTION_ON_DIALOGUE(7,ACT_FRANKLIN_RUNS_TO_CHAD_CAR,9,DIA_TREVOR_SEES_CHAD)
	ACTION_ON_DIALOGUE(14,ACT_FRANKLIN_RUNS_TO_LEANING_MAN,19,DIA_TREVOR_SEES_LEANING_MAN)
	ACTION_ON_DIALOGUE(15,ACT_FRANKLIN_RUNS_TO_PHONE_MAN,20,DIA_TREVOR_SEES_PHONE_CAR_MAN)
	
	//ACTION_ON_DIALOGUE(8,ACT_FRANKLIN_REACTS_TO_WRONG_CAR,10,DIA_FRANKLIN_SEES_PISSER)
	//ACTION_ON_DIALOGUE(9,ACT_FRANKLIN_REACTS_TO_WRONG_CAR,11,DIA_FRANKLIN_SEES_FIXING_MAN)
	//ACTION_ON_DIALOGUE(16,ACT_FRANKLIN_REACTS_TO_WRONG_CAR,21,DIA_FRANKLIN_SEES_LEANING_MAN)
	//ACTION_ON_DIALOGUE(17,ACT_FRANKLIN_REACTS_TO_WRONG_CAR,22,DIA_FRANKLIN_SEES_PHONE_MAN)
	
	ACTION(10,ACT_CHAD_LOOKS_ABOUT,PLAYOUT_ON_TRIGGER,cIF,COND_CARPARK_STARTS)
	ACTION(11,ACT_CHAD_EXITS_CAR,PLAYOUT_ON_TRIGGER,cIF,COND_FRANKLIN_RUNS_TO_CAR)
	ACTION(12,ACT_ON_PHONE_IN_CAR,PLAYOUT_ON_TRIGGER,cIF,COND_CARPARK_STARTS)
	ACTION(13,ACT_LEANING_ON_CAR,PLAYOUT_ON_TRIGGER,cIF,COND_CARPARK_STARTS)
	
	//ACTION(18,ACT_MAKE_FRANKLIN_WALK_ON_SWITCH)
	//ACTION(19,ACT_PREP_SWITCH)
	ACTION_ON_DIALOGUE(20,ACT_DO_SWITCH,23,DIA_TREVOR_RANTS_IN_SWITCH)//,PLAYOUT_ON_TRIGGER,cIF,COND_SEE_CHAD,cANDNOT,COND_INVESTIGATING_NEXT_TARGET,cAND,COND_THERMAL_READY,cAND,COND_DIA_LOOK_FOR_MORE_HEAT_PLAYED)

	//16
	
	//FAIL(FAIL_PLAYER_HAS_WEAPON_DRAWN_IN_POL_DEPT)

	SWITCH iFlag

		case 0
					SET_ROADS_IN_ANGLED_AREA(<<-48.346008,-109.031822,82.549194>>, <<195.550781,-197.283768,-38.864712>>, 155.812500,FALSE,true)
					//set up cars in car park
					
					IF missionProgress != STAGE_SCAN_CARPARK_PEDS
						REMOVE_PED_FROM_SCAN_LIST(HUD,ped[ped_chad].id)													
						add_event(EVENTS_REPOSITION_HEIST_CAR)									
						createHoverInArea(hud,<< -1157.7358, -312.9930, 170.3401 >>,10.0)
					ENDIF
					
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P2_PILOT_HEADPHONES,FALSE) 
					
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
						SET_AUDIO_VEHICLE_PRIORITY(vehicle[vehHeist].id,AUDIO_VEHICLE_PRIORITY_NORMAL)							
					ENDIF

					if not IS_PED_INJURED(ped[ped_franklin].id)
						SET_ENTITY_LOAD_COLLISION_FLAG(ped[ped_franklin].id,true)
					ENDIF

					
					iFlag=4
				
		BREAK
		case 4
			IF IS_ACTION_COMPLETE(20,ACT_DO_SWITCH)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					SET_ENTITY_HEALTH(vehicle[vehChopper].id,1000)
				ENDIF
				return true
			ENDIF
		break
	
	
	ENDSWITCH
	return FALSE
ENDFUNC

			

func bool getCar()



	checkConditions(COND_ZTYPE_STARTS,COND_ZTYPE_ENDS)
	
	INSTRUCTIONS(0,INS_GET_IN_THE_ZTYPE,cIFnot,COND_PLAYER_GOT_IN_CAR)
	INSTRUCTIONS(1,INS_DRIVE_TO_AIRPORT,cIF,COND_PLAYER_GOT_IN_CAR)
	INSTRUCTIONS(3,INS_DRIVE_TO_DESTINATION,cIF,COND_PROCEED_TO_HANGAR_CONDITIONS_MET)
//	INSTRUCTIONS(2,INS_WARNING_WANTED_LEVEL,cIF,COND_CHAD_PLAYER_100m_FROM_CAR_PARK,cANDNOT,COND_CHAD_DEAD,cAND,COND_PLAYER_IN_ZTYPE)
	//INSTRUCTIONS(3,INS_DRIVE_IN_TO_GARAGE,cIF,COND_PLAYER_30m_from_garage,cANDNOT,COND_WANTED,CAND,COND_PLAYER_IN_ZTYPE)



	DIALOGUE(0,DIA_DONT_TAKE_CAR,cIFNOT,COND_CHAD_INTERRUPTED,cANDNOT,COND_CHAD_RAGDOLL,cANDNOT,COND_GUN_AIMED_AT_CHAD,cANDNOT,COND_CHAD_CAN_RUN)
	DIALOGUE(1,DIA_BEG_TO_LET_GO,cIF,COND_GUN_AIMED_AT_CHAD,cOR,COND_CHAD_INTERRUPTED,cANDNOT,COND_CHAD_RAGDOLL,cANDNOT,COND_CHAD_CAN_RUN)
	DIALOGUE(2,DIA_FLEE_PANIC,cIF,COND_CHAD_DAMAGED_BY_PLAYER,cAND,COND_CHAD_PLAYER_IN_GARAGE)
	DIALOGUE(13,DIA_FLEE_PANIC_NOT_HIT,cIFNOT,COND_CHAD_DAMAGED_BY_PLAYER,cAND,COND_PLAYER_SHOOTING_NEAR_CHAD,CAND,COND_CHAD_PLAYER_IN_GARAGE)
	DIALOGUE(3,DIA_PLAYER_GOT_IN_CAR,cIF,COND_PLAYER_GOT_IN_CAR,cANDNOT,COND_CHAD_CAN_RUN)
	DIALOGUE(4,DIA_OPEN_GATES,cIF,COND_PLAYER_AT_SECURITY)
	DIALOGUE(8,DIA_CALL_DEVIN,cIF,COND_PLAYER_GOT_IN_CAR,cAND,COND_NO_DIALOGUE_FOR_TWO_SECONDS)
	DIALOGUE_ON_DIALOGUE(5,DIA_WARNING_COPS,10,DIA_DRIVING_BACK,cIF,COND_CHAD_PLAYER_100m_FROM_CAR_PARK,cANDNOT,COND_CHAD_DEAD,cAND,COND_PLAYER_IN_ZTYPE)
	DIALOGUE(6,DIA_KILLED_CHAD,cIF,COND_CHAD_DEAD,cANDNOT,COND_CHAD_PLAYER_100m_FROM_CAR_PARK) //,cAND,COND_NO_DIALOGUE_FOR_TWO_SECONDS)
	DIALOGUE(9,DIA_PLAYER_SHOOTING_CHOPPER,cIF,COND_CHOPPER_DAMAGED_BY_PLAYER)
	DIALOGUE_ON_DIALOGUE(10,DIA_DRIVING_BACK,8,DIA_CALL_DEVIN)
	DIALOGUE_ON_DIALOGUE(11,DIA_CRASH_ZTYPE,10,DIA_DRIVING_BACK,cIF,COND_DAMAGED_ZTYPE)
	DIALOGUE(12,DIA_WHERES_THE_CAR,cIF,COND_PLAYER_7M_FROM_CHAD,cANDNOT,COND_PLAYER_IN_ZTYPE,cANDNOT,COND_STOP_FOR_CORRECT)
	
	
	ACTION(0,ACT_PREP_STAGE)	
	ACTION(1,ACT_FLY_TO_GARAGE,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_ZTYPE)
	ACTION(2,ACT_REMOVE_CAR_PARK_ASSETS,PLAYOUT_ON_TRIGGER,cIF,COND_CHAD_PLAYER_100m_FROM_CAR_PARK)
	ACTION(3,ACT_SET_UP_DEVIN_LEADIN,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_300m_from_garage)
	ACTION(4,ACT_SPAWN_SECURITY_GUARD,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_200M_FROM_SECURITY)
	ACTION_ON_DIALOGUE(5,ACT_OPEN_GATES,4,DIA_OPEN_GATES)
	ACTION(7,ACT_LOAD_END_CUTSCENE)
	
	ACTION(8,ACT_REACTS_TO_PLAYER_IN_CAR,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_GOT_IN_CAR,cANDNOT,COND_CHAD_CAN_RUN)
	ACTION(9,ACT_CHAD_TRIES_TO_CREEP_AWAY,PLAYOUT_ON_COND,cIF,COND_CHAD_CAN_BACK_OFF,cANDNOT,COND_CHAD_RAGDOLL,cANDNOT,COND_CHAD_CAN_RUN)
	ACTION(10,ACT_CHAD_RUNS_OFF,PLAYOUT_ON_TRIGGER,cIF,COND_CHAD_CAN_RUN)
	ACTION(11,ACT_CHAD_EXITS_CAR,PLAYOUT_ON_COND,cIFnot,COND_CHAD_INTERRUPTED,cANDNOT,COND_GUN_AIMED_AT_CHAD) //continued from last stage
	
	ACTION(12,ACT_START_AUDIO_CAR_2_DRIVE_BACK_TO_GARAGE)
	ACTION(13,ACT_STOP_AUDIO_CAR_2_STEAL_THE_CAR)
	ACTION(14,ACT_CONTROL_CHOPPER)
	ACTION(15,ACT_STOP_ZTYPE_IN_HANGAR,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_ZTYPE,cAND,COND_ZTYPE_IN_CAR_STOP_RANGE,cANDNOT,COND_WANTED)
	ACTION(16,ACT_RETURN_CONTROL_TO_PLAYER)
	
	//Set veh density to
	
	FAIL(FAIL_ZTYPE_DAMAGED)
	FAIL(FAIL_PLAYER_LEAVES_ZTYPE)
	FAIL(FAIL_CHAD_ESCAPES_WITH_ZTYPE)
	FAIL(FAIL_ZTYPE_STUCK)
	FAIL(FAIL_DEVIN_KILLED)
	FAIL(FAIL_DEVIN_SCARED_OFF)
	FAIL(FAIL_COPS_AT_DEVIN)
	
	IF missionFailing = FAIL_NULL
		IF IS_CONDITION_TRUE(COND_PLAYER_200m_from_garage)
		AND NOT IS_CONDITION_TRUE(COND_WANTED)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iDevinSyncSCeneB)				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iDevinSyncSCeneB) > 0.685				
					//cprintln(debug_trevor3,"cut started cos Devin sync scene B is done")
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						TRIGGER_MUSIC_EVENT("CAR2_STOP")
						RETURN TRUE
					ENDIF
				endif			
			ENDIF
			
			if IS_CONDITION_TRUE(COND_DEVIN_WALKING_IN_TO_ZTYPE)
				//cprintln(debug_trevor3,"cut started cos Devin walking in to Ztype")
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					TRIGGER_MUSIC_EVENT("CAR2_STOP")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_CONDITION_TRUE(COND_STOP_FOR_DEVIN)

					IF IS_CONDITION_TRUE(COND_ZTYPE_IS_STOPPED)
						IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
							TRIGGER_MUSIC_EVENT("CAR2_STOP")
							RETURN TRUE
						ENDIF
					ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
	
	//IF IS_ACTION_COMPLETE(15,ACT_STOP_ZTYPE_IN_HANGAR)
	//	return true
	//ENDIF
	return false
ENDFUNC



//INTERIOR_INSTANCE_INDEX int_garage
//int iroomkey
func bool final_cutscene()

//	ACTION(0,ACT_DEVIN_DRIVES_OFF_ZTYPE)
//	ACTION(1,ACT_MAKE_PLAYER_WALK)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
	switch iFlag
		case 0
			SET_ROADS_IN_ANGLED_AREA(<<514.633423,-1321.991821,25.631521>>, <<502.822418,-1297.020508,37.444073>>, 19.312500,FALSE,FALSE)
			SET_ASSET_STAGE(ASSETS_STAGE_FINAL_CUTSCENE)
			
			clear_help_text()
			IF NOT HAS_CUTSCENE_LOADED() //this should only call if skipping to this stage
				cprintln(debug_trevor3,"RELOAD CUT")
				//cprintln(debug_trevor3,"REQUEST CUT B")
				REQUEST_CUTSCENE("Car_2_mcs_1")
			ENDIf
			iFlag=1
		BREAK
		case 1
			IF NOT IS_PED_INJURED(pedDevin)
			AND NOT IS_PED_INJURED(ped[ped_cop2].id)
			AND NOT IS_PED_INJURED(ped[ped_cop3].id)
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()				
					//cprintln(debug_trevor3,"CUT B")
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Devin",pedDevin)				
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Car_2_Security", ped[ped_cop2].id)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Car_2_Security^1", ped[ped_cop3].id)
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()	
				
				if IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
					SET_ENTITY_INVINCIBLE(vehicle[vehHeist].id,TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(vehicle[vehHeist].id,"Car2_ztype",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY,ztype)
					SET_VEHICLE_RADIO_ENABLED(vehicle[vehHeist].id,FALSE)
				ENDIF	
				
				if IS_VEHICLE_DRIVEABLE(vehicle[vehSec3].id)					
					REGISTER_ENTITY_FOR_CUTSCENE(vehicle[vehSec3].id,"Car_2_Tailgater",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,tailgater)		
				ENDIF
				
									
				REGISTER_ENTITY_FOR_CUTSCENE(ped[ped_civ1].id,"Car_2_Pilot",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,S_M_M_PILOT_01)		
			
				
				if IS_VEHICLE_DRIVEABLE(vehicle[vehSec2].id)					
					REGISTER_ENTITY_FOR_CUTSCENE(vehicle[vehSec2].id,"Car_2_Tailgater^1",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,tailgater)		
				ENDIF
				
				if IS_VEHICLE_DRIVEABLE(vehicle[vehSec1].id)					
					REGISTER_ENTITY_FOR_CUTSCENE(vehicle[vehSec1].id,"Car_2_Tailgater^2",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,tailgater)		
				ENDIF
				
				if IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)					
					REGISTER_ENTITY_FOR_CUTSCENE(vehicle[vehJet].id,"Car_2_Shamal",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,shamal)		
				ENDIF	
				
				if not IS_PED_INJURED(ped[ped_cop2].id)			
					REGISTER_ENTITY_FOR_CUTSCENE(ped[ped_cop2].id,"Car_2_Security",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,S_M_Y_DEVINSEC_01)
				endif
				
				if not IS_PED_INJURED(ped[ped_cop3].id)			
					REGISTER_ENTITY_FOR_CUTSCENE(ped[ped_cop3].id,"Car_2_Security^1",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,S_M_Y_DEVINSEC_01)
				endif
				
				if not IS_PED_INJURED(ped[ped_cop4].id)			
					REGISTER_ENTITY_FOR_CUTSCENE(ped[ped_cop4].id,"Molly",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_MOLLY)
				endif
				
				if not IS_PED_INJURED(pedDevin)
					//cprintln(debug_trevor3,"CUT C")
					REGISTER_ENTITY_FOR_CUTSCENE(pedDevin,"Devin",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_DEVIN)
				endif
				
				if not IS_PED_INJURED(ped[ped_trevor].id)
					delete_ped(ped[ped_trevor].id)
				endif
				
				if not IS_PED_INJURED(ped[ped_pilot].id)
					delete_ped(ped[ped_pilot].id)
				endif
				
				if IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
					DELETE_VEHICLE(vehicle[vehChopper].id)
				ENDIF
				
				
				
				
			
				
				clear_area(GET_ENTITY_COORDS(PLAYER_PED_ID()),50,TRUE)
				kill_event(EVENTS_CHOPPER_TRACKS_FRANKLIN)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()					
				ENDIF
				START_CUTSCENE()			
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				CLEAR_AREA_OF_PEDS(<< 482.2777, -1315.6163, 29.9710 >>,50.0)
				
				SET_FRONTEND_RADIO_ACTIVE(FALSE)
				STOP_AUDIO_SCENE("CAR_2_STEAL_THE_CAR")
				iFlag=2
			ENDIF
		BREAK
		case 2
			if IS_CUTSCENE_ACTIVE()			
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
					SET_VEHICLE_TYRE_FIXED(vehicle[vehHeist].id,SC_WHEEL_CAR_FRONT_LEFT)
					SET_VEHICLE_TYRE_FIXED(vehicle[vehHeist].id,SC_WHEEL_CAR_FRONT_RIGHT)
					SET_VEHICLE_TYRE_FIXED(vehicle[vehHeist].id,SC_WHEEL_CAR_REAR_LEFT)
					SET_VEHICLE_TYRE_FIXED(vehicle[vehHeist].id,SC_WHEEL_CAR_REAR_RIGHT)
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					do_fade(FALSE)
				ENDIF
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED)
				iFlag=3
			ENDIF
		break
		case 3
			
			IF NOT DOES_ENTITY_EXIST(ped[ped_civ1].id)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Car_2_Pilot"))
					ped[ped_civ1].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Car_2_Pilot"))
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Car2_ztype",ztype)
				IF IS_VEHICLE_DRIVEABLE(vehicle[vehHeist].id)
					DELETE_VEHICLE(vehicle[vehHeist].id)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Devin")
				IF NOT IS_PED_INJURED(pedDevin)
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)
						SET_PED_INTO_VEHICLE(pedDevin,vehicle[vehJet].id,VS_BACK_RIGHT)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDevin,TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Molly")
				IF NOT IS_PED_INJURED(ped[ped_cop4].id)
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)
						SET_PED_INTO_VEHICLE(ped[ped_cop4].id,vehicle[vehJet].id,VS_BACK_LEFT) 
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_cop4].id,TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Car_2_Security")
				IF NOT IS_PED_INJURED(ped[ped_cop2].id)
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)
						SET_PED_INTO_VEHICLE(ped[ped_cop2].id,vehicle[vehJet].id,VS_EXTRA_LEFT_2) 
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_cop2].id,TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Car_2_Security^1")
				IF NOT IS_PED_INJURED(ped[ped_cop3].id)
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)
						SET_PED_INTO_VEHICLE(ped[ped_cop3].id,vehicle[vehJet].id,VS_EXTRA_LEFT_3) 
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_cop3].id,TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Car_2_Pilot")
				IF NOT IS_PED_INJURED(ped[ped_civ1].id)
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)
						SET_PED_INTO_VEHICLE(ped[ped_civ1].id,vehicle[vehJet].id,VS_DRIVER) 
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped[ped_civ1].id,TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			
		
			IF HAS_CUTSCENE_FINISHED()
			
				REPLAY_STOP_EVENT()
				
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0)
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				REMOVE_CUTSCENE()
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED)
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				SET_FRONTEND_RADIO_ACTIVE(TRUE)
				if IS_VEHICLE_DRIVEABLE(vehicle[vehJet].id)					
					SET_VEHICLE_DOORS_LOCKED(vehicle[vehJet].id,VEHICLELOCK_LOCKED)
					SET_VEHICLE_DOORS_SHUT(vehicle[vehJet].id,TRUE)
				ENDIF	
				
				IF IS_VALID_INTERIOR(intHangar)
					UNPIN_INTERIOR(intHangar)
				ENDIF
			//	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			//	SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
				
			//	IF DOES_ENTITY_EXIST(vehicle[VEH_OBJECTIVE].id)
			//		DELETE_VEHICLE(vehicle[VEH_OBJECTIVE].id)
			//	ENDIF
			/* //done in cleanup
				IF NOT IS_PED_INJURED(ped[ped_Trevor].id)
					IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
						TASK_HELI_MISSION(ped[ped_Trevor].id,vehicle[vehChopper].id,null,null,<< -1372.4413, -738.7798, 177.5559 >>,MISSION_GOTO,20.0,10.0,0.0,150,150)
						SET_PED_KEEP_TASK(ped[ped_Trevor].id,TRUE)
					ENDIF
				ENDIF
			*/
				RETURN TRUE
				
				//removing creation of Franklin's car due to bug 388809
				//IF CREATE_PLAYER_VEHICLE(vehicle[VEH_lamar].id, CHAR_FRANKLIN, << -4.1427, -1446.2357, 29.5720 >>,171)	
				//	return true
				//ENDIF
			ENDIF
		break					
	ENDSWITCH
	
/*	IF IS_ACTION_COMPLETE(1,ACT_MAKE_PLAYER_WALK)
		IF IS_VALID_INTERIOR(intHangar)
			UNPIN_INTERIOR(intHangar)
		ENDIF
		RETURN TRUE
	ENDIF*/
	return false
ENDFUNC

FUNC BOOL leave_airport
	checkConditions(COND_EXIT_AIRPORT_START,COND_EXIT_AIRPORT_END)
	
	ACTION(0,ACT_OPEN_EXIT_GATE)
	ACTION(1,ACT_TURN_PLANE_ENGINE_ON)
	
	INSTRUCTIONS(0,INS_EXIT_AIRPORT)	
	//INSTRUCTIONS(1,INS_RETURN_ZTYPE,cIFNOT,COND_ZTYPE_IN_HANGAR)
	FAIL(fail_devins_plane_destroyed)
	FAIL(fail_attacked_devins_entourage)
	IF IS_INSTRUCTION_COMPLETE(0,INS_EXIT_AIRPORT)
		return TRUE
	ENDIF
	return false
ENDFUNC

PROC advanceStage(enumMissionStage skiptoStage=stage_null)
	int i
	i = ENUM_TO_INT(missionProgress)
	
	i++
	if missionProgress = STAGE_TREVOR_GET_CHOPPER
		i++ //skip passed franklin stage
	endif
	
	IF skiptoStage = stage_null
		missionProgress = INT_TO_ENUM(enumMissionStage,i)
	ELSE
		missionProgress = skiptoStage
	ENDIF
	
	RESET_CONDITIONS()
	RESET_ACTIONS(ACT_CHAD_EXITS_CAR)
	RESET_INSTRUCTIONS()
	RESET_DIALOGUE()
	RESET_FAILS()		

	iFlag = 0
ENDPROC

PROC Mission_Progress()
	//Main mission Loop
	#IF IS_DEBUG_BUILD
		Check_For_Skip()	
		iMissionProg = ENUM_TO_INT(missionProgress)
	#ENDIF
			
	SWITCH missionProgress
			
			CASE STAGE_STARTUP						//0
				If playAsTrevor = FALSE
					
					missionProgress=STAGE_FRANKLIN_AWAITS_TREVOR
				else
					SET_ASSET_STAGE(ASSETS_STAGE_POLICE_STATION)
					missionProgress=STAGE_TREVOR_GET_CHOPPER
				endif
			BREAK

			CASE STAGE_TREVOR_GET_CHOPPER			//1
				IF TrevorGetChopper()
					advanceStage()	
				ENDIF
			BREAK
			
			CASE STAGE_FRANKLIN_AWAITS_TREVOR				//1
				if getInChopper()
					advanceStage()	
				ENDIF
			BREAK
			
			CASE STAGE_LEARN_TO_SCAN				//2
				if learnToScan()
					advanceStage()
				ENDIF
			BREAK
			
			CASE STAGE_APPROACH_SCAN_AREA_ONE		//3
				if scanFirstArea()
					advanceStage()
				ENDIF
			BREAK
			
			CASE STAGE_APPROACH_SCAN_AREA_THREE		//4
				if scanThirdArea()
					advanceStage()
				ENDIF
			BREAK
			
			CASE STAGE_CHASE_BEGINS					//5
				if chaseChad()
					advanceStage()
				ENDIF
			BREAK

			CASE STAGE_CARPARK
				if carPark()
					advanceStage(STAGE_TAKE_ZTYPE)
				ENDIF
			break
			
			CASE STAGE_SCAN_CARPARK_PEDS
				if carPark()
					advanceStage(STAGE_TAKE_ZTYPE)
				ENDIF
			break
			
			CASE STAGE_TAKE_ZTYPE
				IF getCar()
					advanceStage(STAGE_FINAL_CUT)
				ENDIF
			BREAK
			
			CASE STAGE_APPROACH_AIRPORT_GATES
			FALLTHRU
			CASE STAGE_DRIVE_VEHICLE_TO_OBJECTIVE					//10
				if getCar()
					advanceStage(STAGE_FINAL_CUT)
				ENDIF
			BREAK
			
			CASE STAGE_FINAL_CUT
				if final_cutscene()
					advanceStage()
				ENDIF
			BREAK
			
			CASE STAGE_LEAVE_AIRPORT
				if leave_airport()
					advanceStage()
				ENDIF
			BREAK
			/*
			CASE STAGE_USE_SEARCHLIGHT				//9
				if useSearchlight()
					advanceStage()
				ENDIF
			BREAK
			*/
			/*
			CASE STAGE_TAKE_VEHICLE					//10
				if takeVehicle()
					advanceStage()
				ENDIF
			BREAK
			
			CASE STAGE_DRIVE_VEHICLE_TO_OBJECTIVE	//11
				if dropOffVehicle()
					advanceStage()
				ENDIF
			BREAK
			*/
			CASE STAGE_GAME_OVER					//12
				mission_Passed()
			BREAK

		
	ENDSWITCH
	
	IF missionProgress >= STAGE_TAKE_ZTYPE
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
	ENDIF
	
	
	RUN_HELICOPTER_HUD(HUD)
	
	controlCarRecs()	
	//checkBeam()	

	bDisplayBeam=bDisplayBeam
	
	// rob - 2123293
	IF missionProgress >= STAGE_LEARN_TO_SCAN
	AND missionProgress <= STAGE_SCAN_CARPARK_PEDS
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	
	/*
	if bDisplayBeam = TRUE
	
		if not IS_ENTITY_DEAD(vehicle[vehChopper].id)
			if DOES_CAM_EXIST(HUD.camChopper)				
				DISPLAY_BEAM(vehicle[vehChopper].id)//,GET_CAM_ROT(HUD.camChopper))			
			ELSE
				If missionProgress >= STAGE_TAKE_ZTYPE and missionProgress < STAGE_DRIVE_VEHICLE_TO_OBJECTIVE
					DISPLAY_BEAM_WITH_ROTATION(vehicle[vehChopper].id,GET_ROTATION_BETWEEN_VECTORS(GET_ENTITY_COORDS(vehicle[vehChopper].id),<< -1279.0254, -222.1808, 56.2743 >>)*<<1.0,1.0,-1.0>>)				
				ELSE
					DISPLAY_BEAM_WITH_ROTATION(vehicle[vehChopper].id,GET_ROTATION_BETWEEN_VECTORS(GET_ENTITY_COORDS(vehicle[vehChopper].id),<< 488.0179, -1314.8793, 28.2556 >>)*<<1.0,1.0,-1.0>>)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		DISPLAY_BEAM(vehicle[vehChopper].id,FALSE)
	ENDIF
	*/
	#IF IS_DEBUG_BUILD
	debugStuff()
	#ENDIF

ENDPROC


PROC SET_REPLAY_STAGE()
	switch iStoredLastReplay
		CASE 0
			IF GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_FRANKLIN
				
				iStoredLastReplay = 1
			ELSE
				IF missionProgress = STAGE_TREVOR_GET_CHOPPER and IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<476.378693,-987.339050,42.320747>>, <<430.220062,-986.779541,47.257648>>, 31.125000) //player on roof
				OR missionProgress > STAGE_TREVOR_GET_CHOPPER
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Trevor on roof")
					iStoredLastReplay++
				ENDIF
			ENDIF				
		BREAK
		CASE 1
			IF missionProgress >= STAGE_APPROACH_SCAN_AREA_THREE
				IF HAS_PED_BEEN_SCANNED(HUD,ped[ped_chad].id)
				OR missionProgress > STAGE_APPROACH_SCAN_AREA_THREE
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Follow Chad on foot")
					iStoredLastReplay++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF missionProgress >= STAGE_CHASE_BEGINS
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Follow Chad in car")
				iStoredLastReplay++
			ENDIF
		BREAK
		CASE 3
			IF missionProgress = STAGE_TAKE_ZTYPE					
			//	IF IS_CONDITION_TRUE(COND_PLAYER_IN_ZTYPE)
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Franklin gets Z-Type")
					IF NOT IS_PED_INJURED(ped[ped_chad].id)
						g_numEnemyAlive = 1
						CPRINTLN(debug_Trevor3,"Chad alive a")
					ELSE
						g_numEnemyAlive = 0
						CPRINTLN(debug_Trevor3,"Chad dead a")
					ENDIF
					iStoredLastReplay++
			//	ENDIF
			ELIF missionProgress >= STAGE_DRIVE_VEHICLE_TO_OBJECTIVE
				iStoredLastReplay++
			ENDIF
		BREAK
		case 4
			if (missionProgress >= STAGE_TAKE_ZTYPE AND IS_CONDITION_TRUE(COND_PLAYER_IN_ZTYPE))			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5,"Drive Z-type back to garage")
				IF NOT IS_PED_INJURED(ped[ped_chad].id)
					g_numEnemyAlive = 1
					CPRINTLN(debug_Trevor3,"Chad alive b")
				ELSE
					g_numEnemyAlive = 0
					CPRINTLN(debug_Trevor3,"Chad dead b")
				ENDIF
				iStoredLastReplay++
			elif missionProgress > STAGE_DRIVE_VEHICLE_TO_OBJECTIVE
				iStoredLastReplay++
			ENDIF
		BREAK
		case 5
			if missionProgress >= STAGE_LEAVE_AIRPORT
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Leave the airport",TRUE)
				iStoredLastReplay++
			ENDIF
		BREAK
		
	ENDSWITCH		
ENDPROC


PROC CONTROL_CHOPPER_AUDIO_LEVEL()
	
	IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
		IF IS_PED_IN_VEHICLE(player_ped_id(),vehicle[vehChopper].id)
			SWITCH audio_level_flag
				CASE 0
					IF IS_HELIHUD_ACTIVE(HUD)
						IF START_AUDIO_SCENE("CAR_2_HELI_FILTERING")
							audio_level_flag++
						ENDIF
					ENDIF
				BREAK
				CASE 1					
					SET_AUDIO_SCENE_VARIABLE("CAR_2_HELI_FILTERING","HeliFiltering",GET_ENTITY_SPEED(vehicle[vehChopper].id))
				BREAK
			ENDSWITCH
		ELSE
			IF audio_level_flag != 99
			AND audio_level_flag > 0
				STOP_AUDIO_SCENE("CAR_2_HELI_FILTERING")
				audio_level_flag = 99
			ENDIF
		ENDIF
	ELSE
		IF audio_level_flag != 99
		AND audio_level_flag > 0
			STOP_AUDIO_SCENE("CAR_2_HELI_FILTERING")
			audio_level_flag = 99
		ENDIF
	ENDIF
ENDPROC


PROC CONTROL_CONVERSATION_SUBTITLES()
	IF currentConvType != CONVTYPE_NULL
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			currentConvType = CONVTYPE_NULL
		ENDIF
	ENDIF
					 
	If bLastConvoWithoutSubtitles
		IF IS_STRING_NULL_OR_EMPTY(restartRoot)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_SCRIPTED_CONVERSATION_ONGOING()
					IF NOT IS_MESSAGE_BEING_DISPLAYED()															
						restartLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						restartRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						restartRoot = ""
						if not is_string_void(restartLine)
							restartRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							KILL_FACE_TO_FACE_CONVERSATION()
						ELSE
							bLastConvoWithoutSubtitles = FALSE
							restartRoot = ""
							restartLine = ""
						ENDIF		
					ENDIF
				ENDIF
			ELSE
				bLastConvoWithoutSubtitles = FALSE
				restartRoot = ""
				restartLine = ""
			ENDIF
		ELSE		
			IF IS_SCRIPTED_CONVERSATION_ONGOING()
				IF NOT IS_CONV_ROOT_PLAYING(restartRoot)
					bLastConvoWithoutSubtitles = FALSE
					restartRoot = ""
					restartLine = ""
				ENDIF
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(restartRoot) 
				
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(currentConvType,restartRoot,restartLine,convStore[0].speakerNo,convStore[0].pedIndex,convStore[0].speakerLabel,					
					convStore[1].speakerNo,convStore[1].pedIndex,convStore[1].speakerLabel,
					convStore[2].speakerNo,convStore[2].pedIndex,convStore[2].speakerLabel,
					convStore[3].speakerNo,convStore[3].pedIndex,convStore[3].speakerLabel)
				
					restartRoot = ""
					restartLine = ""
					bLastConvoWithoutSubtitles = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

bool bStatTrackingVehicle
proc stat_tracking()
	IF NOT IS_PED_INJURED(ped[ped_chad].id)
		bool bInFOV
		if not IS_ENTITY_DEAD(ped[ped_chad].id) and not IS_ENTITY_DEAD(HUD.camEntity)
			IF IS_ENTITY_ON_SCREEN(ped[ped_chad].id)
				float fWidth = TAN(HUD.fFov/2.0) * GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(HUD.camEntity),GET_WORLD_POSITION_OF_ENTITY_BONE(ped[ped_chad].id,0))
				if fWidth < HUD.fCheckHUDFocusRange
					bInFOV = TRUE
				ENDIF						
			ENDIF
		ENDIF
		
		IF bInFOV
			IF NOT bStatTrackingVehicle
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(CS2_CHAD_FOCUS_TIME)
				bStatTrackingVehicle = TRUE
			ENDIF
		ELSE
			IF bStatTrackingVehicle
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				bStatTrackingVehicle = FALSE
			ENDIF			
		ENDIF
	ENDIF
endproc
// ==============================================================================
//		Script Loop
// ==============================================================================

//int frameCount



SCRIPT
	DISABLE_TAXI_HAILING(true)
	bPlayersUseHeadsets = TRUE
	SET_MISSION_FLAG(TRUE)
	//INFORM_MISSION_STATS_OF_MISSION_START_CAR_STEAL_TWO()
	
	IF HAS_FORCE_CLEANUP_OCCURRED()		
		
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup(MISSION_STATE_DEATH_ARREST)
	ENDIF
	
	INIT_ASSETS()
	DISABLE_TAXI_HAILING(TRUE)
	SET_SCENARIO_GROUP_ENABLED("MP_POLICE",FALSE)
	//BLOCK FRANKLIN START AREA FROM SPAWNING CARS OR PEDS AND STUFF
	SET_ROADS_IN_ANGLED_AREA(<<1413.366333,-2037.311035,49.111671>>, <<1345.929077,-2093.547363,57.623245>>, 96.500000,FALSE,FALSE)
	ADD_SCENARIO_BLOCKING_AREA(<<1382.831177,-2064.168701,56.185715>>-<<41.250000,44.750000,5.750000>>,<<1382.831177,-2064.168701,56.185715>>+<<41.250000,44.750000,5.750000>>)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1382.831177,-2064.168701,56.185715>>,<<41.250000,44.750000,5.750000>>,FALSE)
	SET_PED_NON_CREATION_AREA(<<1382.831177,-2064.168701,56.185715>>-<<41.250000,44.750000,5.750000>>,<<1382.831177,-2064.168701,56.185715>>+<<41.250000,44.750000,5.750000>>)
	SET_PED_PATHS_IN_AREA(<<1382.831177,-2064.168701,56.185715>>-<<41.250000,44.750000,5.750000>>,<<1382.831177,-2064.168701,56.185715>>+<<41.250000,44.750000,5.750000>>,FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BLIMP, TRUE)
	
	SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY",50.0)
	SET_MISSION_VEHICLE_GEN_VEHICLE(GET_PLAYERS_LAST_VEHICLE(),<<0,0,0>>,0)
	
	SETUP_PC_CONTROLS()
	
	If Is_Replay_In_Progress()
	    // Your mission is being replayed
        INT myStage = Get_Replay_Mid_Mission_Stage()
		//cprintln(debug_Trevor3,"replay stage = ",myStage)
		IF g_bShitskipAccepted = TRUE
			IF NOT playAsTrevor
				IF myStage = 0
        			myStage = 2
				ELSE
					myStage++
				ENDIF
			ELSE
				myStage++
			ENDIF
			//cprintln(debug_Trevor3,"shit skip to stage = ",myStage)
        ENDIF
		
		IF not is_ped_injured(player_ped_id())
			CLEAR_AREA(GET_ENTITY_COORDS(player_ped_id()),100.0,TRUE)
		ENDIF
		SWITCH myStage
			case 0 //skip to start location		
				
				IF playAsTrevor
					Check_For_Skip(TRUE,STAGE_TREVOR_GET_CHOPPER,0,TRUE)
				ELSE					
					Check_For_Skip(TRUE,STAGE_FRANKLIN_AWAITS_TREVOR,0,TRUE)
				ENDIF
			BREAK
			case 1 //Trevor on roof of pol dept
				Check_For_Skip(TRUE,STAGE_TREVOR_GET_CHOPPER,1,TRUE)
			break
			case 2 //skip to sitting in chopper	
				SET_ENTITY_INVINCIBLE(player_ped_id(),TRUE)
				bDoReplayToHere = TRUE
				Check_For_Skip(TRUE,STAGE_APPROACH_SCAN_AREA_THREE,1,TRUE)
				CLEAR_PRINTS()
				add_event(EVENTS_LOAD_FINAL_LOCATION_PEDS)
				control_events(EVENTS_LOAD_FINAL_LOCATION_PEDS)
				kill_event(EVENTS_LOAD_FINAL_LOCATION_PEDS)
			
				WHILE NOT DOES_ENTITY_EXIST(ped[ped_chad].id)
					control_events(events_scene_chadGirl)
					RUN_HELICOPTER_HUD(HUD)
					wait(0)
				ENDWHILE
				
				ADD_PED_TO_SCAN_LIST(HUD,ped[ped_chad].id,true,HUD_FOE,true,true,true,true)
				ADD_SPECIAL_NAME_CASE_TO_SCAN_LIST(HUD,ped[ped_chad].id,"CH_CHAD",8)	
			
				//cprintln(debug_Trevor3,"Add ped to scan list")
			
				KILL_EVENT(events_witness_pimp)
				
				DO_SCREEN_FADE_IN(0)
			BREAK
			case 3 					
				Check_For_Skip(TRUE,STAGE_CHASE_BEGINS,0,TRUE)
				WHILE NOT loadCarRecData()
					RUN_HELICOPTER_HUD(HUD)
					safewait()
				ENDWHILE
			BREAK
			CASE 4
				Check_For_Skip(TRUE,STAGE_TAKE_ZTYPE,0,TRUE)
			BREAK
			CASE 5
				Check_For_Skip(TRUE,STAGE_DRIVE_VEHICLE_TO_OBJECTIVE,0,TRUE)
			BREAK
			case 6
				IF g_bShitskipAccepted = TRUE
					Check_For_Skip(TRUE,STAGE_FINAL_CUT,0,TRUE)
				ELSE
					Check_For_Skip(TRUE,STAGE_LEAVE_AIRPORT,0,TRUE)
				ENDIF
			break
			case 7
				Check_For_Skip(TRUE,STAGE_GAME_OVER,0,TRUE)
			break
		ENDSWITCH
	ELIF IS_REPEAT_PLAY_ACTIVE()
		If playAsTrevor
			Check_For_Skip(TRUE,STAGE_TREVOR_GET_CHOPPER,0,FALSE)
		ELSE
			Check_For_Skip(TRUE,STAGE_FRANKLIN_AWAITS_TREVOR,0,FALSE)
		ENDIF
		DO_SCREEN_FADE_IN(1000)
//	ELSE
//		REQUEST_MODEL(model_pilot)
//		WHILE NOT HAS_MODEL_LOADED(model_pilot)
//			WAIT(0)
//		ENDWHILE
	ENDIF		       
	/*	
	IF IS_SCREEN_FADED_OUT()				
		DO_SCREEN_FADE_IN(500)
	ENDIF
*/


	iReturnStage = iReturnStage
	#IF IS_DEBUG_BUILD
		setWidget()
		SET_LOCATES_HEADER_WIDGET_GROUP(carStealWidget)
	#ENDIF

	REQUEST_ADDITIONAL_TEXT("CHHEIST", MISSION_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE

	add_event(EVENTS_MISSION_FAILS,STAGE_GAME_OVER,TRUE)
	//add_event(EVENTS_DEAL_WITH_PLAYER_CAR,STAGE_LEARN_TO_SCAN)
	add_event(EVENTS_REMOVE_SCAN_BOXES,STAGE_CARPARK,true)
	add_event(events_upside_Down,STAGE_GAME_OVER,TRUE)
	add_event(events_set_player_ids,STAGE_GAME_OVER,TRUE)
	
/*#IF IS_DEBUG_BUILD
	widWidgets = START_WIDGET_GROUP("MISSION DEBUG")
	STOP_WIDGET_GROUP()
	SETUP_SPLINE_CAM_NODE_ARRAY_CHOPPER_TO_FRANKLIN(scsCarsteal2, INT_TO_NATIVE(VEHICLE_INDEX, 0), INT_TO_NATIVE(PED_INDEX, 0))
	CREATE_SPLINE_CAM_WIDGETS(scsCarsteal2, "Heli", "Franklin", widWidgets)	
	CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS(widWidgets)
#ENDIF*/						
														
	WHILE (TRUE)
	
		// rob - force searchlight off - 1247680
		IF IS_VEHICLE_DRIVEABLE(vehicle[vehChopper].id)
			SET_VEHICLE_SEARCHLIGHT(vehicle[vehChopper].id, FALSE)		
		ENDIF
	
		IF NOT IS_HELIHUD_ACTIVE(HUD)
		AND missionProgress != STAGE_CARPARK
		AND missionProgress != STAGE_SCAN_CARPARK_PEDS
		
			IF SC2_PLAYER_IN_FIRST_PERSON()
				bPlayerinFirstPerson = TRUE
				//cprintln(debug_trevor3,"Player in first person")
			ELSE
				bPlayerinFirstPerson = FALSE
				//cprintln(debug_trevor3,"Player NOT in first person")
			ENDIF
		ENDIF
	
/*#IF IS_DEBUG_BUILD
		UPDATE_SPLINE_CAM_WIDGETS(scsCarsteal2)
		UPDATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
#ENDIF*/
								
		HANDLE_ASSETS()
		loadCarRecData()			
		Mission_Progress()				
		control_events()		
		controlBlips()			
		displayHelpText()
		SET_REPLAY_STAGE()
		CHOPPER_LISTENING()
		CONTROL_MUSIC()
		CONTROL_CONVERSATION_SUBTITLES()
		CONTROL_CHOPPER_AUDIO_LEVEL()
		stat_tracking()
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("m_EyeInTheSky")
		UPDATE_GARAGE_DOOR_STATE()
		
		IF bMissionMustFail
			mission_failed(sFairReason)
		ENDIF

		
		#IF IS_DEBUG_BUILD
			ScriptManagement(carStealWidget)
		
		
						
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			Mission_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Mission_Failed()
		ENDIF
		#ENDIF
	
		WAIT(0)
	ENDWHILE

	currentConvType=currentConvType //temp

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
//#ENDIF
