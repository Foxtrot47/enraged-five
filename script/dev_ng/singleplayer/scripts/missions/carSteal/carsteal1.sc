//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║				Author: Alan Litobarski				Date: 28/05/2010		 	║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║																				║
//║ 			Street Race Police Bait - (carsteal3.sc)						║
//║ 																			║
//║ 			Pull-up alongside the owner of an expensive sports car			║
//║ 			and challenge him to a street race. Half way through you		║
//║ 			have your buddies pretending to be cops pull you over,			║
//║ 			and you jack his car.											║
//║																				║
//╚═════════════════════════════════════════════════════════════════════════════╝

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//═════════════════════════════════╡ HEADERS ╞═══════════════════════════════════

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_cutscene.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_entity.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_physics.sch"

USING "dialogue_public.sch"
USING "flow_public_core_override.sch"
USING "script_blips.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "chase_hint_cam.sch"
USING "locates_public.sch"
USING "selector_public.sch"
USING "replay_public.sch"
USING "comms_control_public.sch"
USING "cam_recording_public.sch"
USING "help_at_location.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "script_ped.sch"
USING "CompletionPercentage_public.sch"
USING "carsteal_public.sch"
USING "clearmissionarea.sch"
USING "building_control_public.sch"
USING "commands_recording.sch"
USING "push_in_public.sch"

USING "emergency_call.sch"
USING "spline_cam_edit.sch"

// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					340	//125
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					25	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				60	

// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		12  
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		15					

// this should be fine
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		10

USING "traffic.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//Debug Variables
//#IF IS_DEBUG_BUILD
//	INT iCinCamID, iCinCamIDLast
//	BOOL bCinCamSave
//	BOOL bCinCamOutput
//	BOOL bCinCamDebug
//	BOOL bCinCamLink, bCinCamLinkLast
//	
//	VECTOR vCinCamPos, vCinCamPosLast
//	VECTOR vCinCamRot, vCinCamRotLast
//	FLOAT fCinCamFov, fCinCamFovLast
//	VECTOR vCinCamTanIn, vCinCamTanInLast
//	FLOAT fCinCamTanInMag, fCinCamTanInMagLast
//	VECTOR vCinCamTanOut, vCinCamTanOutLast
//	FLOAT fCinCamTanOutMag, fCinCamTanOutMagLast
//	FLOAT fCinCamRec, fCinCamRecLast
//#ENDIF

//Integers
CONST_INT MAX_CAR_HEALTH 2000

INT iCutsceneStage

INT iDialogueStage = 0
INT iDialogueLineCount[5]
INT iDialogueTimer[3]

CONST_INT iTotalEffects 8

INT iGameTimer

ENUM CHASE_TIMER_INDEX
	CHASE_TIMER_F620,
	CHASE_TIMER_ENTITYXF,
	CHASE_TIMER_CHEETAH
ENDENUM

INT iRoadNodeTimer
INT iChaseTimer[COUNT_OF(CHASE_TIMER_INDEX)]
VECTOR vCurrentPoint[COUNT_OF(CHASE_TIMER_INDEX)], vChasePoint[COUNT_OF(CHASE_TIMER_INDEX)]
FLOAT fCurrentPoint[COUNT_OF(CHASE_TIMER_INDEX)], fChasePoint[COUNT_OF(CHASE_TIMER_INDEX)]

INT iInterruptTimer

ENUM CHASE_CARS
	CHASE_CAR_F620,
	CHASE_CAR_ENTITYXF,
	CHASE_CAR_CHEETAH
ENDENUM

INT iCarHealth[COUNT_OF(CHASE_CARS)]
FLOAT fCarGasTank[COUNT_OF(CHASE_CARS)]
FLOAT fCarEngineHealth[COUNT_OF(CHASE_CARS)]

//Bools
BOOL bVideoRecording

BOOL bInitStage
BOOL bCleanupStage
BOOL bRadar = TRUE

#IF IS_DEBUG_BUILD //Debug
	BOOL bAutoSkipping
#ENDIF

BOOL bSkipped //Used for J-skipping and Mid Mission Replay

BOOL bPreloaded //For Preloaded Cutscene

BOOL bReplaySkip

BOOL bCutsceneSkipped	//Tracks if WAS_CUTSCENE_SKIPPED() returns TRUE

BOOL bPassed	//Passed the mission

BOOL bDelayHUD = FALSE

BOOL bPinnedChopShop

BOOL bSetUncapped

BOOL bHasChanged	//Variable for storing the state of “player has changed outfit”

//BOOL bSparks[iTotalEffects]

BOOL bAbilityUsed

BOOL bWaitForPlayer = TRUE

#IF IS_DEBUG_BUILD
BOOL bDebugAudio
#ENDIF

//Vectors and Floats
VECTOR VECTOR_ZERO = <<0.0, 0.0, 0.0>>

FLOAT fModifyTopSpeed = 0.0

VECTOR vPlayerStart = <<117.1050, -414.6006, 40.1527>>
CONST_FLOAT fPlayerStart 268.8413

VECTOR vRival1Start = <<2577.2300, 360.6015, 107.8564>>
CONST_FLOAT fRival1Start 0.0

VECTOR vRival2Start = <<2583.5542, 360.2650, 107.7638>>
CONST_FLOAT fRival2Start -1.3639

VECTOR vPoliceStart1 = <<2682.7749, 5131.3569, 43.8418>>
CONST_FLOAT fPoliceStart1 147.9465
//VECTOR vPoliceRotate1 = <<0.0, -9.1609, 147.9465>>

VECTOR vPoliceStart2 = <<2684.9312, 5131.9438, 43.8593>>
CONST_FLOAT fPoliceStart2 135.0532
//VECTOR vPoliceRotate2 = <<-0.0799, -9.6398, 135.0532>>

VECTOR vTown = <<2571.36, 360.45, 107.4569>>	//<<2577.3052, 364.9556, 107.4569>>
VECTOR vGarage = <<488.8870, -1323.6843, 28.1722>>

//VECTOR vCinematicCam

//FLOAT fForceBoost //Boost for bike

//Text Labels

//Audio

//Strings
STRING sCarrecGasStation = "ALgasstation"
STRING sCarrecSetup = "ALubersetup"
STRING sCarrecCop = "ALubercop"
STRING sCarrecGarage = "ALgarage"

//Playback
FLOAT fPlaybackSpeed = 1.0

//Anim Dicts
STRING sAnimDictCar3 = "misscarsteal3"
STRING sAnimDictCar3PullOver = "misscarsteal3pullover"
//STRING sAnimDictPhoneUse = "veh@low@front_ds@base"

//═════════════════════════════════╡ INDEXES ╞═══════════════════════════════════

//Ped
PED_INDEX pedRival1
PED_INDEX pedRival2
PED_INDEX pedDevin
PED_INDEX pedMolly
PED_INDEX pedWorker1
PED_INDEX pedWorker2

//Vehicle
VEHICLE_INDEX vehPlayer
VEHICLE_INDEX vehRival1
VEHICLE_INDEX vehRival2
VEHICLE_INDEX vehPolice1
VEHICLE_INDEX vehPolice2
VEHICLE_INDEX vehDevin
VEHICLE_INDEX vehCutscene
VEHICLE_INDEX vehTrailer[5]

//Blip
BLIP_INDEX blipStart
BLIP_INDEX blipPlayer
BLIP_INDEX blipRival1
BLIP_INDEX blipRival2
BLIP_INDEX blipPolice
BLIP_INDEX blipPolice2
BLIP_INDEX blipGarage

//Camera
CAMERA_INDEX camMain
CAMERA_INDEX camCinematic
//CAMERA_INDEX camSwitch
//CAMERA_INDEX camGameplay

//Sequence
SEQUENCE_INDEX seqMain

//Interior
INTERIOR_INSTANCE_INDEX intChopShop

//Streaming Volume
STREAMVOL_ID svolCarStealOutro

//Object
OBJECT_INDEX objID
OBJECT_INDEX objRamp

//Doors
HASH_ENUM STUDIO_GATES_HASH_ENUM
	DOORHASH_GARAGE_FRONT
ENDENUM

INT iSlidingGarageDoor = ENUM_TO_INT(DOORHASH_GARAGE_FRONT)

//Rope
//ROPE_INDEX ropeChain

//Particles
//PTFX_ID ptfxSparks[iTotalEffects]
//PTFX_ID ptfxFric[iTotalEffects]
//PTFX_ID ptfxSmoke[iTotalEffects]

//Weapons
//WEAPON_TYPE wtUnarmed = WEAPONTYPE_UNARMED

//Relationship Groups
REL_GROUP_HASH relGroupBuddy
REL_GROUP_HASH relGroupEnemy

//Locates Struct
LOCATES_HEADER_DATA sLocatesData

//Hint Cam Struct
CHASE_HINT_CAM_STRUCT localChaseHintCamStruct

//Push In Cam Struct
PUSH_IN_DATA pushInData

//Dialogue
structPedsForConversation sPedsForConversation

INT i_CST3_MCS1_AnimStartTime = 0
INT i_CST3_MCS1_DialogueDelay = 0

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widGroup
#ENDIF

//══════════════════════════════════╡ ENUMS ╞════════════════════════════════════

ENUM MissionObjective
	initMission,
	cutIntro,
	stageArrive,
	cutRace,
	stageRace,
	cutSwitch,
	stageChase,
	cutBridge,
	stageDriveHome,
	cutHome,
	passMission,
	failMission
ENDENUM

MissionObjective eMissionObjective = initMission

#IF IS_DEBUG_BUILD

MissionObjective eMissionObjectiveAutoJSkip = initMission

#ENDIF

ENUM MissionFail
	failDefault,
	failPlayerDied,
	failMichaelDied,
	failFranklinDied,
	failTrevorDied,
	failTrevorShot,
	failDevinDied,
	failDevinShot,
	failDevinCarDestroyed,
	failDevinCarDamaged,
	failRacerDied,
	failCarDestroyed,
	failBikeDestroyed,
	failBikeDestroyedTrevor,
	failBikeDamagedTrevor,
	failLeftBehind,
	failAbandonedBike,
	failAbandonedCar,
	failRacerShot,
	failRacerSpooked,
	failRacerLeft,
	failDamagedCar,
	failStuckCar,
	failRaceDisrupted
ENDENUM

MissionFail eMissionFail = failDefault

//Stage Selector
#IF IS_DEBUG_BUILD

INT iStageSkipMenu

CONST_INT MAX_SKIP_MENU_LENGTH COUNT_OF(MissionObjective) - 2 //Number of stages in mission minus Pass/Fail

MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]

PROC MissionNames()
	SkipMenuStruct[0].sTxtLabel = "initMission"
	SkipMenuStruct[1].sTxtLabel = "cutIntro - (CAR_1_INT_CONCAT)"
	SkipMenuStruct[2].sTxtLabel = "stageArrive"
	SkipMenuStruct[3].sTxtLabel = "cutRace - (Car_steal_3_mcs_1)"
	SkipMenuStruct[4].sTxtLabel = "stageRace"
	SkipMenuStruct[5].sTxtLabel = "cutSwitch - (Car_steal_3_mcs_2)"
	SkipMenuStruct[6].sTxtLabel = "stageChase"
	SkipMenuStruct[7].sTxtLabel = "cutBridge - (Car_steal_3_mcs_3)"
	SkipMenuStruct[8].sTxtLabel = "stageDriveHome"
	SkipMenuStruct[9].sTxtLabel = "cutHome"
ENDPROC

#ENDIF

//Text
STRING sConversationBlock = "CST3AUD"

INT iTriggeredTextHashes[100]
INT iNumTextHashesStored = 0

PROC REMOVE_LABEL_ARRAY_SPACES()
	INT iArrayLength = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT (iArrayLength - 1) i
		IF iTriggeredTextHashes[i] = 0
			IF iTriggeredTextHashes[i+1] != 0
				iTriggeredTextHashes[i] = iTriggeredTextHashes[i+1]
				iTriggeredTextHashes[i+1] = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_LABEL_INDEX(INT iLabelHash)
	INT i = 0
	
	REPEAT iNumTextHashesStored i
		IF iTriggeredTextHashes[i] = iLabelHash
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF GET_LABEL_INDEX(iHash) != -1
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bTrigger)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF bTrigger
		IF NOT HAS_LABEL_BEEN_TRIGGERED(strLabel)
			INT iArraySize = COUNT_OF(iTriggeredTextHashes)
		
			IF iNumTextHashesStored < iArraySize
				iTriggeredTextHashes[iNumTextHashesStored] = iHash
				iNumTextHashesStored++
			ELSE
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Label array is full.")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		INT iIndex = GET_LABEL_INDEX(iHash)
		IF iIndex != -1
			iTriggeredTextHashes[iIndex] = 0
			REMOVE_LABEL_ARRAY_SPACES()
			iNumTextHashesStored--
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iArraySize i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
	
	iNumTextHashesStored = 0
ENDPROC

//Models
INT iLoadedModelHashes[25]

FUNC BOOL HAS_MODEL_BEEN_LOADED(MODEL_NAMES mnModel)
	INT iHash = ENUM_TO_INT(mnModel)
	
	INT i
	
	REPEAT COUNT_OF(iLoadedModelHashes) i
		IF iLoadedModelHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_MODEL_AS_LOADED(MODEL_NAMES mnModel, BOOL bIsLoaded)
	INT iHash = ENUM_TO_INT(mnModel)
	
	INT i = 0
	
	BOOL bQuitLoop = FALSE
	
	WHILE i < COUNT_OF(iLoadedModelHashes) AND NOT bQuitLoop
		IF bIsLoaded
			IF iLoadedModelHashes[i] = 0
				iLoadedModelHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iLoadedModelHashes[i] = iHash
				iLoadedModelHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_MODELS()
	INT i = 0
	
	REPEAT COUNT_OF(iLoadedModelHashes) i
		iLoadedModelHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_MODEL_LOADED_CHECK(MODEL_NAMES mnModel)
	IF NOT HAS_MODEL_BEEN_LOADED(mnModel)
		REQUEST_MODEL(mnModel)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING MODEL #", ENUM_TO_INT(mnModel))	#ENDIF
		
		IF HAS_MODEL_LOADED(mnModel)
			SET_MODEL_AS_LOADED(mnModel, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("MODEL #", ENUM_TO_INT(mnModel), " LOADED")	#ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Recordings
INT iLoadedRecordingHashes[30]

FUNC BOOL HAS_RECORDING_BEEN_LOADED(INT iFileNumber, STRING sRecordingName)
	TEXT_LABEL txtHash
	txtHash = iFileNumber
	txtHash += sRecordingName
	
	INT iHash = GET_HASH_KEY(txtHash)
	
	INT i
	
	REPEAT COUNT_OF(iLoadedRecordingHashes) i
		IF iLoadedRecordingHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_RECORDING_AS_LOADED(INT iFileNumber, STRING sRecordingName, BOOL bIsLoaded)
	TEXT_LABEL txtHash
	txtHash = iFileNumber
	txtHash += sRecordingName
	
	INT iHash = GET_HASH_KEY(txtHash)
	
	INT i = 0
	
	BOOL bQuitLoop = FALSE
	
	WHILE i < COUNT_OF(iLoadedRecordingHashes) AND NOT bQuitLoop
		IF bIsLoaded
			IF iLoadedRecordingHashes[i] = 0
				iLoadedRecordingHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iLoadedRecordingHashes[i] = iHash
				iLoadedRecordingHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_RECORDINGS()
	INT i = 0
	
	REPEAT COUNT_OF(iLoadedRecordingHashes) i
		iLoadedRecordingHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_RECORDING_LOADED_CHECK(INT iFileNumber, STRING sRecordingName)
	IF NOT HAS_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
		REQUEST_VEHICLE_RECORDING(iFileNumber, sRecordingName)
		
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
			SET_RECORDING_AS_LOADED(iFileNumber, sRecordingName, TRUE)
			PRINTSTRING("RECORDING ") PRINTINT(iFileNumber) PRINTSTRING(" ") PRINTSTRING(sRecordingName) PRINTSTRING(" LOADED")
			PRINTNL()
		ELSE
			PRINTSTRING("LOADING RECORDING ") PRINTINT(iFileNumber) PRINTSTRING(" ") PRINTSTRING(sRecordingName)
			PRINTNL()
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNLOAD_RECORDING(INT iFileNumber, STRING sRecordingName)
	IF HAS_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
		REMOVE_VEHICLE_RECORDING(iFileNumber, sRecordingName)
		SET_RECORDING_AS_LOADED(iFileNumber, sRecordingName, FALSE)
	ENDIF
ENDPROC

//Animation Dictionaries
INT iAnimDictHashes[20]

FUNC BOOL HAS_ANIM_DICT_BEEN_LOADED(STRING strAnimDict)
	INT iHash = GET_HASH_KEY(strAnimDict)
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		IF iAnimDictHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_ANIM_DICT_AS_LOADED(STRING strAnimDict, BOOL bIsLoaded)
	INT iHash = GET_HASH_KEY(strAnimDict)
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	BOOL bQuitLoop = FALSE
	
	WHILE i < iNumHashes AND NOT bQuitLoop
		IF bIsLoaded
			IF iAnimDictHashes[i] = 0
				iAnimDictHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iAnimDictHashes[i] = iHash
				iAnimDictHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_ANIM_DICTS()
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		iAnimDictHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_ANIM_DICT_LOADED_CHECK(STRING strAnimDict)
	IF NOT HAS_ANIM_DICT_BEEN_LOADED(strAnimDict)
		REQUEST_ANIM_DICT(strAnimDict)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING ANIM DICT ", strAnimDict)	#ENDIF
		
		IF HAS_ANIM_DICT_LOADED(strAnimDict)
			SET_ANIM_DICT_AS_LOADED(strAnimDict, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("ANIM DICT ", strAnimDict, " LOADED")	#ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNLOAD_ANIM_DICT(STRING strAnimDict)
	IF HAS_ANIM_DICT_BEEN_LOADED(strAnimDict)
		REMOVE_ANIM_DICT(strAnimDict)
		SET_ANIM_DICT_AS_LOADED(strAnimDict, FALSE)
	ENDIF
ENDPROC

//Audio
ENUM AUDIO_TRACK
	NO_AUDIO,
	CAR1_MISSION_START,
	CAR1_MISSION_RESTART,
	CAR1_COP_BIKES,
	CAR1_COPS_RESTART,
	CAR1_APPROACH,
	CAR1_CHASE_START,
	CAR1_PULL_OVER,
	CAR1_CHASE_RESTART,
	CAR1_BRIDGE,
	CAR1_MISSION_FAIL
ENDENUM

AUDIO_TRACK ePrepAudioTrack = NO_AUDIO
AUDIO_TRACK ePlayAudioTrack = NO_AUDIO

//════════════════════════════════╡ STRUCTURES ╞═════════════════════════════════

//Switch
SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sCamDetails

//════════════════════════════════╡ FUNCTIONS ╞══════════════════════════════════

BOOL bPlayerControl = TRUE

PROC SAFE_SET_PLAYER_CONTROL(PLAYER_INDEX iPlayerIndex, BOOL bSetControlOn, SET_PLAYER_CONTROL_FLAGS iFlags = 0)
	SET_PLAYER_CONTROL(iPlayerIndex, bSetControlOn, iFlags)
	bPlayerControl = bSetControlOn
ENDPROC

FUNC BOOL SAFE_IS_PLAYER_CONTROL_ON()
	IF bPlayerControl
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADVANCE_CUTSCENE()
	SETTIMERA(0)
	
	iCutsceneStage++
ENDPROC

PROC ADVANCE_STAGE()
	bCleanupStage = TRUE
ENDPROC

FUNC BOOL SKIPPED_STAGE()
	IF bSkipped = TRUE
		bSkipped = FALSE
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL INIT_STAGE()
	IF bInitStage = FALSE
		SETTIMERA(0)
		
		bInitStage = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL CLEANUP_STAGE()
	IF bCleanupStage = TRUE
		SETTIMERA(0)
		
		bInitStage = FALSE
		bCleanupStage = FALSE
		iCutsceneStage = 0
		
		iDialogueStage = 0
		INT i
		REPEAT COUNT_OF(iDialogueLineCount) i
			iDialogueLineCount[i] = -1
		ENDREPEAT
		REPEAT COUNT_OF(iDialogueTimer) i
			iDialogueTimer[i] = 0
		ENDREPEAT
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC CLEAR_TEXT()
	CLEAR_PRINTS()
	CLEAR_HELP(TRUE)
	CLEAR_ALL_FLOATING_HELP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
ENDPROC

PROC PRINT_ADV(STRING sPrint, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
		PRINT_NOW(sPrint, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
	ENDIF
ENDPROC

PROC PRINT_HELP_ADV(STRING sPrint, BOOL bOnce = TRUE, INT iOverrideTime = -1)
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
			PRINT_HELP(sPrint, iOverrideTime)
			SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
			PRINTLN("PRINTHELP ", sPrint)
		ENDIF
	ENDIF
ENDPROC

PROC PRINT_HELP_ADV_POS(STRING sPrint, VECTOR vLocation, ENTITY_INDEX entIndex = NULL, BOOL bOnce = TRUE, eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, INT iDuration = 7500)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
		IF NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
		AND vLocation.Z != 0.0
		AND entIndex = NULL
			HELP_AT_LOCATION(sPrint, vLocation, eArrow, iDuration)
		ELIF NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
		AND vLocation.Z = 0.0
		AND entIndex = NULL
			HELP_AT_SCREEN_LOCATION(sPrint, vLocation.X, vLocation.Y, eArrow, iDuration)
		ELIF entIndex != NULL
		AND ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
			HELP_AT_ENTITY(sPrint, entIndex, eArrow, iDuration)
		ELIF entIndex != NULL
		AND NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
			HELP_AT_ENTITY_OFFSET(sPrint, entIndex, vLocation, eArrow, iDuration)
		ENDIF
		SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
	ENDIF
ENDPROC

FUNC PED_INDEX PLAYER_PED(enumCharacterList CHAR_TYPE)
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TYPE
		RETURN PLAYER_PED_ID()
	ELSE
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_TYPE)]
	ENDIF
ENDFUNC

PROC SET_PED_POSITION(PED_INDEX pedIndex, VECTOR vCoords, FLOAT fHeading, BOOL bKeepVehicle = TRUE)
	IF bKeepVehicle = TRUE
		SET_PED_COORDS_KEEP_VEHICLE(pedIndex, vCoords)
	ELSE
		SET_ENTITY_COORDS(pedIndex, vCoords)
	ENDIF
	
	SET_ENTITY_HEADING(pedIndex, fHeading)
ENDPROC

PROC SET_VEHICLE_POSITION(VEHICLE_INDEX vehicleIndex, VECTOR vCoords, FLOAT fHeading)
	SET_ENTITY_COORDS(vehicleIndex, vCoords)
	SET_ENTITY_HEADING(vehicleIndex, fHeading)
ENDPROC

PROC SPAWN_PLAYER(PED_INDEX &pedIndex, enumCharacterList eChar, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = FALSE)
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedIndex, eChar, vStart, fStart)
			WAIT(0)
		ENDWHILE
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_PED(PED_INDEX &pedIndex, MODEL_NAMES eModel, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		pedIndex = CREATE_PED(PEDTYPE_MISSION, eModel, vStart, fStart)
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedIndex, TRUE)
	ENDIF
ENDPROC

PROC SPAWN_VEHICLE(VEHICLE_INDEX &vehIndex, MODEL_NAMES eModel, VECTOR vStart, FLOAT fStart = 0.0, INT iColour = -1, FLOAT fDirt = 0.0)
	IF NOT DOES_ENTITY_EXIST(vehIndex)
		vehIndex = CREATE_VEHICLE(eModel, vStart, fStart)
		IF iColour >= 0
			SET_VEHICLE_COLOURS(vehIndex, iColour, iColour)
			SET_VEHICLE_EXTRA_COLOURS(vehIndex, iColour, iColour)
		ENDIF
		SET_VEHICLE_DIRT_LEVEL(vehIndex, fDirt)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehIndex, TRUE)
	ENDIF
ENDPROC

FUNC BOOL SAFE_DEATH_CHECK_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		IF IS_ENTITY_DEAD(pedIndex)
		OR IS_PED_INJURED(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_DEATH_CHECK_VEHICLE(VEHICLE_INDEX &vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
		IF IS_ENTITY_DEAD(vehIndex)
		OR NOT IS_VEHICLE_DRIVEABLE(vehIndex)
		OR IS_VEHICLE_PERMANENTLY_STUCK(vehIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SAFE_ADD_BLIP_LOCATION(BLIP_INDEX &blipIndex, VECTOR vCoords, BOOL bRoute = FALSE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		blipIndex = CREATE_BLIP_FOR_COORD(vCoords, bRoute)
		SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGHEST)
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_PED(BLIP_INDEX &blipIndex, PED_INDEX &pedIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(pedIndex)
			IF NOT IS_PED_INJURED(pedIndex)
				blipIndex = CREATE_BLIP_FOR_PED(pedIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_VEHICLE(BLIP_INDEX &blipIndex, VEHICLE_INDEX &vehIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(vehIndex)
			IF NOT IS_ENTITY_DEAD(vehIndex)
				blipIndex = CREATE_BLIP_FOR_VEHICLE(vehIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_REMOVE_BLIP(BLIP_INDEX &blipIndex)
	IF DOES_BLIP_EXIST(blipIndex)
		REMOVE_BLIP(blipIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		DELETE_PED(pedIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_VEHICLE(VEHICLE_INDEX &vehicleIndex)
	IF DOES_ENTITY_EXIST(vehicleIndex)
		DELETE_VEHICLE(vehicleIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_OBJECT(OBJECT_INDEX &objectIndex)
	IF DOES_ENTITY_EXIST(objectIndex)
		DELETE_OBJECT(objectIndex)
	ENDIF
ENDPROC

PROC SAFE_FREEZE_ENTITY_POSITION(ENTITY_INDEX EntityIndex, BOOL FrozenByScriptFlag)
	IF NOT IS_ENTITY_ATTACHED(EntityIndex)
		IF (IS_ENTITY_A_PED(EntityIndex)
		AND NOT IS_PED_IN_ANY_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(EntityIndex)))
		OR NOT IS_ENTITY_A_PED(EntityIndex)
			FREEZE_ENTITY_POSITION(EntityIndex, FrozenByScriptFlag)
		ENDIF
	ENDIF
ENDPROC

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &vehIndex, FLOAT fTime)
	IF IS_VEHICLE_DRIVEABLE(vehIndex)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehIndex)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehIndex, fTime - GET_TIME_POSITION_IN_RECORDING(vehIndex))
		ENDIF
	ENDIF
ENDPROC

//Borrowed from locates_private.sch!
PROC EMPTY_VEHICLE_OF_PEDS(VEHICLE_INDEX inVehicle)
    PED_INDEX pedTemp
    INT i
    INT iMaxNumberOfPassengers
	
    //Clear tasks of any passengers or drivers
    IF DOES_ENTITY_EXIST(inVehicle)
        IF IS_VEHICLE_DRIVEABLE(inVehicle)
            //Driver
            pedTemp = GET_PED_IN_VEHICLE_SEAT(inVehicle)  
            IF DOES_ENTITY_EXIST(pedTemp)
                IF NOT IS_PED_INJURED(pedTemp)
                    CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
                ENDIF
            ENDIF
            
            iMaxNumberOfPassengers = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(inVehicle) 
			
            //Passengers
            REPEAT iMaxNumberOfPassengers i
                IF NOT IS_VEHICLE_SEAT_FREE(inVehicle, INT_TO_ENUM(VEHICLE_SEAT, i))
                    pedTemp = GET_PED_IN_VEHICLE_SEAT(inVehicle, INT_TO_ENUM(VEHICLE_SEAT, i)) 
                    IF DOES_ENTITY_EXIST(pedTemp)
                        IF NOT IS_PED_INJURED(pedTemp)
                            CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
                        ENDIF
                    ENDIF
                ENDIF
            ENDREPEAT
        ENDIF
    ENDIF
ENDPROC

//═══════════════════════════╡ CAR STEAL SWITCH CAM ╞════════════════════════════



ENUM CAR_STEAL_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE,
	SWITCH_CAM_START_SPLINE1,
	SWITCH_CAM_PLAYING_SPLINE1,
	SWITCH_CAM_START_CUTSCENE,
	SWITCH_CAM_WAITING_FOR_CUTSCENE,
	SWITCH_CAM_PLAYING_CUTSCENE,
	SWITCH_CAM_START_SPLINE2,
	SWITCH_CAM_WAITING_FOR_PLAYER_CONTROL,
	SWITCH_CAM_RETURN_TO_GAMEPLAY
ENDENUM
CAR_STEAL_SWITCH_CAM_STATE eCarStealSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsSwitchCam1
SWITCH_CAM_STRUCT scsSwitchCam2

STRING strAnimDictCar3LeadInOut = "misscarsteal3leadinout"
STRING strAnimDictBikeIdle = "veh@bike@quad@front@base"
STRING strAnimDictCamShake = "shake_cam_all@"

STRING strCustomSwitchSRL = "carsteal1_customswitch"
FLOAT fCustomSwitchSRLTime = 0.0

FLOAT fEndSceneDistanceToBlend = 3.80

INT iSyncedScene

FLOAT fTrevorRecSkipTime = 0.0
BOOL bStoppedTrevRec = FALSE
FLOAT fStopTrevRecTime = 4500.000

BOOL bStartedTrevRec2 = FALSE
FLOAT fTrevRecSkipTime2 = 0.0
INT iTrevRecRevertTime = 5000

INT iPlayerControlDelay = 800

VECTOR vTrevorStartPos = <<2686.8792, 5131.9077, 43.8515>>
FLOAT fTrevorStartHeading = 151.6033
VECTOR vMichaelStartPos = <<2682.7749, 5131.3569, 43.8418>>
FLOAT fMichaelStartHeading = 147.9465

PED_INDEX piSceneTrevor
PED_INDEX piSceneMichael

VEHICLE_INDEX viSceneBike1
VEHICLE_INDEX viSceneBike2

VECTOR vCarStartPos = <<2801.3, 4435.5, 47.9>>
FLOAT fCarStartHeading = 14.29

OBJECT_INDEX oiDonut1
VECTOR vDonut1Pos = <<2683.3, 5132.5, 43.8>>
VECTOR vDonut1AttachPos
VECTOR vDonut1AttachRot

OBJECT_INDEX oiDonut2
VECTOR vDonut2Pos = <<2683.0, 5130.8, 43.9>>
VECTOR vDonut2AttachPos
VECTOR vDonut2AttachRot

OBJECT_INDEX oiCellphone
VECTOR vCellphonePos = <<2683.3, 5132.5, 44.8>>
VECTOR vCellphoneAttachPos
VECTOR vCellphoneAttachRot
BOOL bCellphoneReattach = FALSE

BOOL bSceneAnimsStarted = FALSE

FLOAT fFranklinRecordingSkipTime = 30000.0

INT iWooshIn_ID = -1
INT iWooshing_ID = -1
INT iWooshOut_ID = -1

BOOL bStartWooshIn = FALSE
BOOL bStopWooshIn = FALSE

BOOL bStartWooshing = FALSE
BOOL bStopWooshing = FALSE

FLOAT fCustomCamFX1StartPhase = 0.54
FLOAT fCustomCamFX2StartPhase = 0.63

BOOL bCustomCamFX1Started = FALSE
BOOL bCustomCamFX2Started = FALSE

FLOAT fStartWhooshInSplinePhase = 0.54
FLOAT fStopWhooshInSplinePhase = 0.78

FLOAT fStartWhooshingSplinePhase = 0.57
FLOAT fStopWhooshingSplinePhase = 0.78

//BOOL bSpeechStarted
structPedsForConversation splineCamSpeech1
//structPedsForConversation splineCamSpeech2

#IF IS_DEBUG_BUILD
BOOL bSwitchCamDebugScenarioEnabled = FALSE
#ENDIF
BOOL bResetDebugScenario = FALSE

BOOL bDebugStartWhooshIn = FALSE
BOOL bDebugStopWhooshIn = FALSE
BOOL bDebugStartWhooshOut = FALSE
BOOL bDebugStopWhooshOut = FALSE

PROC SETUP_SPLINE_CAM_1_NODE_ARRAY(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX &vi1, VEHICLE_INDEX &vi2)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_1_NODE_ARRAY")
	
	IF NOT thisSwitchCam.bInitialized

		//Camera swith from Franklin to Bike Cops (ends when the cutscene starts)

		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-2.3643, -0.5889, 0.4792>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<0.0183, 0.0447, 0.3025>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].fNodeFOV = 40.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[0].fNodeCamShake = 1.0
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 2900
		thisSwitchCam.nodes[1].vNodePos = <<-1.9817, -0.5034, 0.4696>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<0.0203, 0.0344, 0.2803>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].fNodeFOV = 40.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[1].fNodeCamShake = 1.0
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-7.6252, 24.0532, 0.3440>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<-4.9335, 0.0000, -72.5827>>
		thisSwitchCam.nodes[2].bPointAtEntity = FALSE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].fNodeFOV = 40.0000
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[2].fNodeCamShake = 1.0
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<2.7024, -65.6113, 3.9747>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<-2.9265, 0.0274, 3.5865>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 30.0000
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 2.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 1
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 600
		thisSwitchCam.nodes[5].vNodePos = <<-0.8088, -7.2793, 0.9790>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<1.1810, 0.6552, 0.4279>>
		thisSwitchCam.nodes[5].bPointAtEntity = TRUE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 30.0000
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 900
		thisSwitchCam.nodes[6].vNodePos = <<-0.8560, -6.5132, 0.9411>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<1.1810, 0.6530, 0.4270>>
		thisSwitchCam.nodes[6].bPointAtEntity = TRUE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 30.0000
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[6].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000


		thisSwitchCam.iNumNodes = 7
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---



		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_CarSteal_CarToBike.txt"
		thisSwitchCam.strXMLFileName = "CameraInfo_CarSteal_CarToBike.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.viVehicles[0] = vi1
	thisSwitchCam.viVehicles[1] = vi2

ENDPROC

PROC SETUP_SPLINE_CAM_2_NODE_ARRAY(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX &viVehicle)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_2_NODE_ARRAY")
	
	IF NOT thisSwitchCam.bInitialized
		
		
		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<2.5180, 2.8530, 0.2460>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<0.6690, -0.0850, 0.2480>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].fNodeFOV = 0.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 1
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.8000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 5000
		thisSwitchCam.nodes[1].vNodePos = <<2.1235, 2.5186, 0.2460>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<0.6300, -0.2496, 0.2500>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].fNodeFOV = 0.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000


		thisSwitchCam.iNumNodes = 2
		thisSwitchCam.iCamSwitchFocusNode = 0
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_CarSteal_EndCam.txt"
		thisSwitchCam.strXMLFileName = "CameraInfo_CarSteal_EndCam.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF

	thisSwitchCam.viVehicles[0] = viVehicle

ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS(WIDGET_GROUP_ID wgidWidget)
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")
	
	SET_CURRENT_WIDGET_GROUP(wgidWidget)
	
	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables -")
		
		ADD_WIDGET_BOOL("Restart Debug Scenario", bResetDebugScenario)

		START_WIDGET_GROUP("Misc")
			
			ADD_WIDGET_INT_SLIDER("Player Control Delay", iPlayerControlDelay, 0, 10000, 100)
			ADD_WIDGET_FLOAT_SLIDER("EndScene distance scale", fEndSceneDistanceToBlend, 0.0, 20.0, 0.5)
			
			START_WIDGET_GROUP("Trevor's Recording Tunables")
				ADD_WIDGET_FLOAT_SLIDER("Skip Time - Recording 1", fTrevorRecSkipTime, 0, 999999.9, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Stop Time - Recording 1", fStopTrevRecTime, 0.0, 999999.9, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Skip Time - Recording 2", fTrevRecSkipTime2, 0.0, 999999.9, 100.0)
				ADD_WIDGET_INT_SLIDER("Revert back to Rec Time - Recording 2", iTrevRecRevertTime, 0, 999999, 100)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Donuts Tunables")
				ADD_WIDGET_VECTOR_SLIDER("Donut 1 Pos", vDonut1Pos, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Donut 1 Attach Pos", vDonut1AttachPos, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Donut 1 Attach Rot", vDonut1AttachRot, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Donut 2 Pos", vDonut2Pos, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Donut 2 Attach Pos", vDonut2AttachPos, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Donut 2 Attach Rot", vDonut2AttachRot, -10000.0, 10000.0, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Cellphone Prop Tunables")
				ADD_WIDGET_BOOL("Reattach Cellphone", bCellphoneReattach)
				ADD_WIDGET_VECTOR_SLIDER("Spawn Pos", vCellphonePos, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Attach Pos", vCellphoneAttachPos, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Attach Rot", vCellphoneAttachRot, -10000.0, 10000.0, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Whoosh Sounds")
				ADD_WIDGET_FLOAT_SLIDER("Start Whoosh Sound 'In' Spline Phase", fStartWhooshInSplinePhase, 0.0, 1.0, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("Stop Whoosh Sound 'In' Spline Phase", fStopWhooshInSplinePhase, 0.0, 1.0, 0.05)
				//ADD_WIDGET_FLOAT_SLIDER("Start Whoosh Sound 'Out' Spline Phase", fStartWhooshOutSplinePhase, 0.0, 1.0, 0.05)
				ADD_WIDGET_BOOL("Start Woosh In", bDebugStartWhooshIn)
				ADD_WIDGET_BOOL("Stop Woosh In", bDebugStopWhooshIn)
				ADD_WIDGET_BOOL("Start Woosh Out", bDebugStartWhooshOut)
				ADD_WIDGET_BOOL("Stop Woosh Out", bDebugStopWhooshOut)
			STOP_WIDGET_GROUP()
			
//			START_WIDGET_GROUP("Michaels response to Franklin's cellphone call")
//				ADD_WIDGET_FLOAT_SLIDER("Spline Phase Start", fStartSpeech2SplinePhase, 0.0, 1.0, 0.05)
//			STOP_WIDGET_GROUP()

		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
	CLEAR_CURRENT_WIDGET_GROUP(wgidWidget)
	
ENDPROC
#ENDIF

FUNC BOOL HANDLE_SWITCH_CINEMATIC(SWITCH_CAM_STRUCT &thisSwitchCam1, SWITCH_CAM_STRUCT &thisSwitchCam2)
	//CDEBUG3LN(DEBUG_MISSION, "HANDLE_SWITCH_CINEMATIC")
	
	INT iCurrentNode
	FLOAT fCamPhase
	
	VECTOR vSyncScenePos = <<2683.80, 5131.16, 43.901>>
	VECTOR vSyncSceneRot = <<0.0, 0.0, -4.680>>
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
		
	RESET_ADAPTATION()
	
	SWITCH eCarStealSwitchCamState
		
		CASE SWITCH_CAM_START_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_START_SPLINE1")
			
			SET_GAME_PAUSES_FOR_STREAMING(FALSE)
			
			DESTROY_ALL_CAMS()
			
			SETUP_SPLINE_CAM_1_NODE_ARRAY(thisSwitchCam1, vehPlayer, vehPolice2)
			CREATE_SPLINE_CAM(thisSwitchCam1)

			SET_CAM_ACTIVE(thisSwitchCam1.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			CLEAR_AREA(<<2716.9924, 4787.3481, 43.5334>>, 1000.0, TRUE)
			
			IF DOES_ENTITY_EXIST(vehPlayer)
				IF NOT IS_ENTITY_DEAD(vehPlayer)
					START_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 600, "ALuberSetup")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPlayer, fFranklinRecordingSkipTime)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehPlayer)
				ENDIF
			ENDIF
			
			BEGIN_SRL()
			fCustomSwitchSRLTime = -9999.0
			
			//To hide the cellphone and stop it ringing
			HIDE_ACTIVE_PHONE(TRUE)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					STOP_PED_RINGTONE(PLAYER_PED_ID()) 
					CLEAR_BIT(BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)
				ENDIF
			ENDIF

			bStartWooshIn = FALSE
			bStopWooshIn = FALSE
			bCustomCamFX1Started = FALSE
			bCustomCamFX2Started = FALSE
			bSceneAnimsStarted = FALSE
			
			eCarStealSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
		BREAK
		
		CASE SWITCH_CAM_PLAYING_SPLINE1
			//CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
			
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			
			fCustomSwitchSRLTime += (GET_FRAME_TIME() * 1000.0)
			IF fCustomSwitchSRLTime < 0.0
				fCustomSwitchSRLTime = 0.0
			ENDIF
			CDEBUG3LN(DEBUG_MISSION, "SRL Time = ", fCustomSwitchSRLTime)
			SET_SRL_TIME(fCustomSwitchSRLTime)
			
			IF IS_CAM_ACTIVE(thisSwitchCam1.ciSpline)
			
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam1)
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam1.ciSpline)
				
				IF bStartWooshIn = FALSE
					IF fCamPhase >= fStartWhooshInSplinePhase
						iWooshIn_ID = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(iWooshIn_ID , "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bStartWooshIn = TRUE
					ENDIF
				ENDIF
				
				IF bStopWooshIn = FALSE
					IF fCamPhase >= fStopWhooshInSplinePhase
						STOP_SOUND(iWooshIn_ID)
						bStopWooshIn = TRUE
					ENDIF
				ENDIF
				
				IF bStartWooshing = FALSE
					IF fCamPhase >= fStartWhooshingSplinePhase
						iWooshing_ID = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(iWooshing_ID , "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bStartWooshing = TRUE
					ENDIF
				ENDIF
				
				IF bStopWooshing = FALSE
					IF fCamPhase >= fStopWhooshingSplinePhase
						STOP_SOUND(iWooshing_ID)
						bStopWooshing = TRUE
					ENDIF
				ENDIF
				
				IF NOT bCustomCamFX1Started
					IF fCamPhase >= fCustomCamFX1StartPhase
						CDEBUG3LN(DEBUG_MISSION, "FX1 Started...")
						ANIMPOSTFX_PLAY("SwitchShortFranklinIn", 0, FALSE)
						bCustomCamFX1Started = TRUE
					ENDIF
				ENDIF
				
				IF NOT bCustomCamFX2Started
					IF fCamPhase >= fCustomCamFX2StartPhase
						CDEBUG3LN(DEBUG_MISSION, "FX2 Started...")
						ANIMPOSTFX_PLAY("SwitchShortMichaelMid", 0, FALSE)
						bCustomCamFX2Started = TRUE
					ENDIF
				ENDIF
				
				//play animations
				IF NOT bSceneAnimsStarted
					IF iCurrentNode > 0
						IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_MICHAEL))
						AND NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_TREVOR))
							iSyncedScene = CREATE_SYNCHRONIZED_SCENE(vSyncScenePos, vSyncSceneRot)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_MICHAEL), iSyncedScene, strAnimDictCar3LeadInOut, "byBike_idle_Mic", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_ARM_IK)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_TREVOR), iSyncedScene, strAnimDictCar3LeadInOut, "byBike_idle_Trev", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_ARM_IK)
							SET_SYNCHRONIZED_SCENE_LOOPED(iSyncedScene, TRUE)
							
							IF DOES_ENTITY_EXIST(vehPolice1)
								IF NOT IS_ENTITY_DEAD(vehPolice1)
									SET_ENTITY_COORDS(vehPolice1, vPoliceStart1)
									SET_ENTITY_HEADING(vehPolice1, fPoliceStart1)
									SET_VEHICLE_ON_GROUND_PROPERLY(vehPolice1)
									FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehPolice1)
								ENDIF
							ENDIF
							IF DOES_ENTITY_EXIST(vehPolice2)
								IF NOT IS_ENTITY_DEAD(vehPolice2)
									SET_ENTITY_COORDS(vehPolice2, vPoliceStart2)
									SET_ENTITY_HEADING(vehPolice2, fPoliceStart2)
									SET_VEHICLE_ON_GROUND_PROPERLY(vehPolice2)
									FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehPolice2)
								ENDIF
							ENDIF							
						ENDIF
						bSceneAnimsStarted = TRUE
					ENDIF
				ENDIF
				
//				IF NOT bForcedCellStarted
//					IF iCurrentNode >= thisSwitchCam1.iCamSwitchFocusNode
//						CDEBUG3LN(DEBUG_MISSION, "Disabling Cellphone...")
//						KILL_ANY_CONVERSATION()
//						bForcedCellStarted = TRUE
//					ENDIF
//				ENDIF
//				
//				IF NOT bSpeech2Started
//					IF GET_CAM_SPLINE_PHASE(thisSwitchCam1.ciSpline) >= fStartSpeech2SplinePhase
//						CDEBUG3LN(DEBUG_MISSION, "Starting Speech 2...")
//						PLAY_SINGLE_LINE_FROM_CONVERSATION(splineCamSpeech2, "CST3AUD", "CST3_2secs", "CST3_2secs_3", CONV_PRIORITY_VERY_HIGH)
//						bSpeech2Started = TRUE
//					ENDIF
//				ENDIF

				IF fCamPhase >= 1.0
//				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					END_SRL()
					
					eCarStealSwitchCamState = SWITCH_CAM_START_CUTSCENE
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
		FALLTHRU
		
		CASE SWITCH_CAM_START_CUTSCENE
			CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_START_CUTSCENE")
			
			IF NOT HAS_CUTSCENE_LOADED()
				SCRIPT_ASSERT("Trying to play a cutscene that is not loaded (Car_steal_3_mcs_2)")
			ENDIF
			
			STOP_SOUND(iWooshOut_ID)
			
			IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_MAIN")
				STOP_AUDIO_SCENE("CAR_1_RACE_MAIN")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("CAR_1_FRANKLIN_CALLS_MICHAEL")
				STOP_AUDIO_SCENE("CAR_1_FRANKLIN_CALLS_MICHAEL")
			ENDIF
			
			FREEZE_ENTITY_POSITION(vehPolice1, FALSE)
			FREEZE_ENTITY_POSITION(vehPolice2, FALSE)
			
			REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED(CHAR_TREVOR), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED(CHAR_MICHAEL), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(vehPolice1, "Trevors_police_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(vehPolice2, "Michaels_Police_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
				
				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ENDIF
			
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			
			START_CUTSCENE()
			
			CLEAR_TEXT()
			
			HIDE_ACTIVE_PHONE(FALSE)
			
			eCarStealSwitchCamState = SWITCH_CAM_WAITING_FOR_CUTSCENE
			CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_PLAYING_CUTSCENE")
		BREAK
		
		CASE SWITCH_CAM_WAITING_FOR_CUTSCENE
			//CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_PLAYING_CUTSCENE")
			
			IF IS_CUTSCENE_PLAYING()
				
				IF DOES_CAM_EXIST(thisSwitchCam1.ciSpline)
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedScene)
						DETACH_SYNCHRONIZED_SCENE(iSyncedScene)
					ENDIF
					
					SET_TIME_SCALE(1.0)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_CAM(thisSwitchCam1.ciSpline)
					
					//Remove Franklins vehicle recording for switch cam
					REMOVE_VEHICLE_RECORDING(600, "ALuberSetup")
					
					//Removing Michaels attached cellphone prop
					IF DOES_ENTITY_EXIST(oiCellPhone)
						DETACH_ENTITY(oiCellPhone)
						SAFE_DELETE_OBJECT(oiCellPhone)
					ENDIF
					
					//DELETING DONUTS
					IF DOES_ENTITY_EXIST(oiDonut1)
						DETACH_ENTITY(oiDonut1)
						SAFE_DELETE_OBJECT(oiDonut1)
					ENDIF
					IF DOES_ENTITY_EXIST(oiDonut2)
						DETACH_ENTITY(oiDonut2)
						SAFE_DELETE_OBJECT(oiDonut2)
					ENDIF
					
				ENDIF
				
				eCarStealSwitchCamState = SWITCH_CAM_PLAYING_CUTSCENE
				CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_PLAYING_CUTSCENE")
			ENDIF
		BREAK
		
		CASE SWITCH_CAM_PLAYING_CUTSCENE
			//CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_PLAYING_CUTSCENE")
			
//			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//				SETUP_SPLINE_CAM_2_NODE_ARRAY(thisSwitchCam2, vehPolice2)
//				CREATE_SPLINE_CAM(thisSwitchCam2)
//				
//				SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, TRUE)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				RETURN FALSE
			ELSE
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) != CAM_VIEW_MODE_FIRST_PERSON
				OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) != CAM_VIEW_MODE_FIRST_PERSON
					SETUP_SPLINE_CAM_2_NODE_ARRAY(thisSwitchCam2, vehPolice2)
					CREATE_SPLINE_CAM(thisSwitchCam2)
					PRINTLN("Spline created")
					SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
				
				//SPAWN DONUTS
				SAFE_DELETE_OBJECT(oiDonut1)
				oiDonut1 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02B, <<2683.87842, 5130.14893, 43.85238>>, FALSE, FALSE)
				IF DOES_ENTITY_EXIST(oiDonut1)
					PLACE_OBJECT_ON_GROUND_PROPERLY(oiDonut1)
				ENDIF
				
				SAFE_DELETE_OBJECT(oiDonut2)
				oiDonut2 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02B, <<2683.29028, 5128.59229, 43.85796>>, FALSE, FALSE)
				IF DOES_ENTITY_EXIST(oiDonut2)
					PLACE_OBJECT_ON_GROUND_PROPERLY(oiDonut2)
				ENDIF
				
				SETTIMERA(0)
				
				eCarStealSwitchCamState = SWITCH_CAM_IDLE
				RETURN TRUE
				
				//eCarStealSwitchCamState = SWITCH_CAM_START_SPLINE2
			ENDIF
			
		FALLTHRU
		
		CASE SWITCH_CAM_START_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_START_SPLINE2")
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			ENDIF
			
			//SPAWN DONUTS
//			SAFE_DELETE_OBJECT(oiDonut1)
//			oiDonut1 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02B, vDonut1Pos, FALSE, FALSE)
//			IF DOES_ENTITY_EXIST(oiDonut1)
//				PLACE_OBJECT_ON_GROUND_PROPERLY(oiDonut1)
//			ENDIF
//			
//			SAFE_DELETE_OBJECT(oiDonut2)
//			oiDonut2 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02B, vDonut2Pos, FALSE, FALSE)
//			IF DOES_ENTITY_EXIST(oiDonut2)
//				PLACE_OBJECT_ON_GROUND_PROPERLY(oiDonut2)
//			ENDIF
			
//			SETUP_SPLINE_CAM_2_NODE_ARRAY(thisSwitchCam2, vehPolice2)
//			CREATE_SPLINE_CAM(thisSwitchCam2)
//			
//			SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, TRUE)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SETTIMERA(0)
			
			eCarStealSwitchCamState = SWITCH_CAM_WAITING_FOR_PLAYER_CONTROL
			CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_WAITING_FOR_PLAYER_CONTROL")
		BREAK

		CASE SWITCH_CAM_WAITING_FOR_PLAYER_CONTROL
			//CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_WAITING_FOR_PLAYER_CONTROL")
			
			iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam2)
			
			IF TIMERA() > iPlayerControlDelay
				eCarStealSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
			ENDIF
		BREAK
		
		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
			CDEBUG3LN(DEBUG_MISSION, "eCarStealSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
			
//			INT iLeftX, iLeftY, iRightX, iRightY
//			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
			
			IF IS_CAM_ACTIVE(thisSwitchCam2.ciSpline)
			
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam2)
				
//				IF GET_CAM_SPLINE_PHASE(thisSwitchCam2.ciSpline) >= 1.0
//				OR iLeftX <> 0 OR iLeftY <> 0 OR iRightX <> 0 OR iRightY <> 0
//				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
//				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
//					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, fEndSceneDistanceToBlend, CAM_SPLINE_SLOW_IN_SMOOTH)
//					SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, FALSE)
//					
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					IF DOES_CAM_EXIST(thisSwitchCam2.ciSpline)
//						DESTROY_CAM(thisSwitchCam2.ciSpline)
//					ENDIF
					

					SETTIMERA(0)
					
					eCarStealSwitchCamState = SWITCH_CAM_IDLE
					RETURN TRUE
//				ENDIF
			ENDIF
			
		BREAK

	ENDSWITCH
		
	RETURN FALSE
	
ENDFUNC

PROC UPDATE_SPLINE_CAM_HELPERS()
	//CDEBUG3LN(DEBUG_MISSION, "UPDATE_SPLINE_CAM_HELPERS")
	
	IF bResetDebugScenario
	
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		DO_SCREEN_FADE_OUT(0)
		
		DESTROY_ALL_CAMS()
		
		//Audio
		REGISTER_SCRIPT_WITH_AUDIO()
		
		SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		//Vehicle
		SET_VEHICLE_MODEL_IS_SUPPRESSED(F620, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(ENTITYXF, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(CHEETAH, TRUE)
		
		//Relationships
		REMOVE_RELATIONSHIP_GROUP(relGroupBuddy)
		REMOVE_RELATIONSHIP_GROUP(relGroupEnemy)
	
		ADD_RELATIONSHIP_GROUP("BUDDIES", relGroupBuddy)
		ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemy)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBuddy, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupBuddy)
		
		CLEAR_AREA(vCarStartPos, 100.0, TRUE)
		CLEAR_AREA_OF_VEHICLES(vCarStartPos, 100.0)
		CLEAR_AREA_OF_COPS(vCarStartPos, 100.0)
		
		SWITCH_ALL_RANDOM_CARS_OFF()
		SUPPRESS_EMERGENCY_CALLS()
		
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		
		//Set Player
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
				CDEBUG3LN(DEBUG_MISSION, "Waiting for Franklin...")
				WAIT(0)
			ENDWHILE
		ENDIF
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vCarStartPos, TRUE, TRUE)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fCarStartHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			ENDIF
		ENDIF
		
		
		//Request Additional Text
		REQUEST_ADDITIONAL_TEXT("H3HEIST", MISSION_TEXT_SLOT)
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			CDEBUG3LN(DEBUG_MISSION, "Waiting for mission text...")
			WAIT(0)
		ENDWHILE
	
		//REQUEST
		HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
		HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
		HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		HAS_MODEL_LOADED_CHECK(F620)
		HAS_MODEL_LOADED_CHECK(POLICEB)
		HAS_MODEL_LOADED_CHECK(PROP_PLAYER_PHONE_01)
		HAS_MODEL_LOADED_CHECK(PROP_DONUT_02)
		HAS_MODEL_LOADED_CHECK(PROP_DONUT_02B)
		HAS_ANIM_DICT_LOADED_CHECK(strAnimDictCar3LeadInOut)
		HAS_ANIM_DICT_LOADED_CHECK(strAnimDictBikeIdle)
		HAS_RECORDING_LOADED_CHECK(500, sCarrecCop)	//sCarrecChase)
//		HAS_RECORDING_LOADED_CHECK(499, sCarrecChase)
		
		CDEBUG3LN(DEBUG_MISSION, "Waiting for Assets...")
		
		//CHECK FOR LOADED
		BOOL bAssetsLoaded = FALSE
		WHILE NOT bAssetsLoaded
			IF HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			AND HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			AND HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				CDEBUG3LN(DEBUG_MISSION, "Ped Models loaded...")
				IF HAS_MODEL_LOADED_CHECK(F620)
				AND HAS_MODEL_LOADED_CHECK(POLICEB)
					CDEBUG3LN(DEBUG_MISSION, "Vehicle Models loaded...")
					IF HAS_MODEL_LOADED_CHECK(PROP_PLAYER_PHONE_01)
					AND HAS_MODEL_LOADED_CHECK(PROP_DONUT_02)
					AND HAS_MODEL_LOADED_CHECK(PROP_DONUT_02B)
						CDEBUG3LN(DEBUG_MISSION, "Prop Models loaded...")
						IF HAS_ANIM_DICT_LOADED_CHECK(strAnimDictCar3LeadInOut)
						AND HAS_ANIM_DICT_LOADED_CHECK(strAnimDictBikeIdle)
							CDEBUG3LN(DEBUG_MISSION, "Animations loaded...")
							IF HAS_RECORDING_LOADED_CHECK(500, sCarrecCop)	//sCarrecChase)
//								AND HAS_RECORDING_LOADED_CHECK(499, sCarrecChase)
								CDEBUG3LN(DEBUG_MISSION, "Recordings loaded...")
								bAssetsLoaded = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			WAIT(0)
		ENDWHILE
		
		
		//TEMP LOADING OF AREA TO GET BIKES IN THE CORRECT POSITION
		
		CLEAR_AREA(vPoliceStart1, 100.0, TRUE)
		CLEAR_AREA_OF_VEHICLES(vPoliceStart1, 100.0)
		CLEAR_AREA_OF_COPS(vPoliceStart1, 100.0)
		
		VECTOR vTempPos = <<2683.1, 5122.2, 43.8374>>
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vTempPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
			ENDIF
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		
		LOAD_SCENE(vPoliceStart1)
		
		//DO_SCREEN_FADE_IN(0)
		
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		
		//SPAWN TREVOR'S BIKE
		SAFE_DELETE_VEHICLE(vehPolice1)
		SPAWN_VEHICLE(vehPolice1, POLICEB, vPoliceStart1, fPoliceStart1)
		IF DOES_ENTITY_EXIST(vehPolice1)
			IF NOT IS_ENTITY_DEAD(vehPolice1)
				SET_ENTITY_COORDS(vehPolice1, vPoliceStart1)
				SET_ENTITY_HEADING(vehPolice1, fPoliceStart1)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPolice1)
				SET_ENTITY_VISIBLE(vehPolice1, FALSE)
			ENDIF
		ENDIF
		
		//SPAWN MICHAEL'S BIKE
		SAFE_DELETE_VEHICLE(vehPolice2)
		SPAWN_VEHICLE(vehPolice2, POLICEB, vPoliceStart2, fPoliceStart2)
		IF DOES_ENTITY_EXIST(vehPolice2)
			IF NOT IS_ENTITY_DEAD(vehPolice2)
				SET_ENTITY_COORDS(vehPolice2, vPoliceStart2)
				SET_ENTITY_HEADING(vehPolice2, fPoliceStart2)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPolice2)
				SET_ENTITY_VISIBLE(vehPolice2, FALSE)
			ENDIF
		ENDIF
		
		WAIT(0)
		
		IF DOES_ENTITY_EXIST(vehPolice1)
			IF NOT IS_ENTITY_DEAD(vehPolice1)
				//SET_ENTITY_COLLISION(vehPolice1, FALSE)
				FREEZE_ENTITY_POSITION(vehPolice1, TRUE)
			ENDIF
		ENDIF

		IF DOES_ENTITY_EXIST(vehPolice2)
			IF NOT IS_ENTITY_DEAD(vehPolice2)
				//SET_ENTITY_COLLISION(vehPolice2, FALSE)
				FREEZE_ENTITY_POSITION(vehPolice2, TRUE)
			ENDIF
		ENDIF
		
		//SPAWN TREVOR
		SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorStartPos, fTrevorStartHeading)
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
				SET_ENTITY_COLLISION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)

				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_WillFlyThroughWindscreen, FALSE)
				IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
					SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
				ENDIF
				GIVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, PV_FLAG_SCRIPT_HELMET)
				
				IF DOES_ENTITY_EXIST(vehPolice1)
					IF NOT IS_ENTITY_DEAD(vehPolice1)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
						SET_VEHICLE_ENGINE_ON(vehPolice1, FALSE, TRUE)
						FREEZE_ENTITY_POSITION(vehPolice1, FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(vehPolice1, 500, sCarrecCop)	//499, sCarrecChase)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPolice1, fTrevorRecSkipTime)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehPolice1)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
		//SPAWN MICHAEL
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStartPos, fMichaelStartHeading)
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
				SET_ENTITY_COLLISION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
				
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_WillFlyThroughWindscreen, FALSE)
				IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 4)
					SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 0)
				ENDIF
				GIVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, PV_FLAG_SCRIPT_HELMET)
				
				IF DOES_ENTITY_EXIST(vehPolice2)
					IF NOT IS_ENTITY_DEAD(vehPolice2)
						FREEZE_ENTITY_POSITION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPolice2)
						SET_VEHICLE_ENGINE_ON(vehPolice2, FALSE, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//WAIT(2000)
		//DO_SCREEN_FADE_OUT(0)
		
		//SPAWN FRANKLIN (current player)
		SAFE_DELETE_VEHICLE(vehPlayer)
		SPAWN_VEHICLE(vehPlayer, F620, vCarStartPos, fCarStartHeading, 0)
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF NOT IS_ENTITY_DEAD(vehPlayer)
				SET_ENTITY_COORDS(vehPlayer, vCarStartPos, TRUE, TRUE)
				SET_ENTITY_HEADING(vehPlayer, fCarStartHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				FREEZE_ENTITY_POSITION(vehPlayer, TRUE)
				SET_VEHICLE_COLOURS(vehPlayer, 43, 43)
				SET_VEHICLE_EXTRA_COLOURS(vehPlayer, 48, 48)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND DOES_ENTITY_EXIST(vehPlayer)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(vehPlayer)		
				sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = PLAYER_PED_ID()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				IF DOES_ENTITY_EXIST(vehPlayer)
					IF NOT IS_ENTITY_DEAD(vehPlayer)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		//SPAWN SCENE TREVOR & BIKE
		SAFE_DELETE_VEHICLE(viSceneBike1)
		SPAWN_VEHICLE(viSceneBike1, POLICEB, vPoliceStart1, fPoliceStart1)
		IF DOES_ENTITY_EXIST(viSceneBike1)
			IF NOT IS_ENTITY_DEAD(viSceneBike1)
				SET_VEHICLE_ON_GROUND_PROPERLY(viSceneBike1)
				SET_VEHICLE_ENGINE_ON(viSceneBike1, FALSE, TRUE)
				SET_ENTITY_COLLISION(viSceneBike1, FALSE)
				FREEZE_ENTITY_POSITION(viSceneBike1, TRUE)
			ENDIF
		ENDIF
		SAFE_DELETE_PED(piSceneTrevor)
		SPAWN_PLAYER(piSceneTrevor, CHAR_TREVOR, vTrevorStartPos, fTrevorStartHeading)
		IF DOES_ENTITY_EXIST(piSceneTrevor)
			IF NOT IS_ENTITY_DEAD(piSceneTrevor)
				SET_PED_COMP_ITEM_CURRENT_SP(piSceneTrevor, COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(piSceneTrevor, relGroupBuddy)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piSceneTrevor, TRUE)
				
				IF NOT IS_PED_WEARING_HELMET(piSceneTrevor)
					SET_PED_HELMET_PROP_INDEX(piSceneTrevor, 5)
					SET_PED_HELMET_TEXTURE_INDEX(piSceneTrevor, 0)
				ENDIF
				GIVE_PED_HELMET(piSceneTrevor, TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
		ENDIF
		
		//SPAWN SCENE MICHAEL & BIKE
		SAFE_DELETE_VEHICLE(viSceneBike2)
		SPAWN_VEHICLE(viSceneBike2, POLICEB, vPoliceStart2, fPoliceStart2)
		IF DOES_ENTITY_EXIST(viSceneBike2)
			IF NOT IS_ENTITY_DEAD(viSceneBike2)
				SET_VEHICLE_ON_GROUND_PROPERLY(viSceneBike2)
				SET_VEHICLE_ENGINE_ON(viSceneBike2, FALSE, TRUE)
				SET_ENTITY_COLLISION(viSceneBike2, FALSE)
				FREEZE_ENTITY_POSITION(viSceneBike2, TRUE)
			ENDIF
		ENDIF
		SAFE_DELETE_PED(piSceneMichael)
		SPAWN_PLAYER(piSceneMichael, CHAR_MICHAEL, vMichaelStartPos, fMichaelStartHeading)
		IF DOES_ENTITY_EXIST(piSceneMichael)
			IF NOT IS_ENTITY_DEAD(piSceneMichael)
				SET_PED_COMP_ITEM_CURRENT_SP(piSceneMichael, COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(piSceneMichael, relGroupBuddy)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piSceneMichael, TRUE)
				
				IF NOT IS_PED_WEARING_HELMET(piSceneMichael)
					SET_PED_HELMET_PROP_INDEX(piSceneMichael, 4)
					SET_PED_HELMET_TEXTURE_INDEX(piSceneMichael, 0)
				ENDIF
				GIVE_PED_HELMET(piSceneMichael, TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
		ENDIF
		
		//SPAWN ATTACHED DONUTS
		SAFE_DELETE_OBJECT(oiDonut1)
		oiDonut1 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02B, vDonut1Pos, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(oiDonut1)
			IF DOES_ENTITY_EXIST(piSceneMichael)
				IF NOT IS_ENTITY_DEAD(piSceneMichael)
					ATTACH_ENTITY_TO_ENTITY(oiDonut1, piSceneMichael,  GET_PED_BONE_INDEX(piSceneMichael, BONETAG_PH_R_HAND), vDonut1AttachPos, vDonut1AttachRot)
				ENDIF
			ENDIF
		ENDIF
		SAFE_DELETE_OBJECT(oiDonut2)
		oiDonut2 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02, vDonut2Pos, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(oiDonut2)
			IF DOES_ENTITY_EXIST(piSceneTrevor)
				IF NOT IS_ENTITY_DEAD(piSceneTrevor)
					ATTACH_ENTITY_TO_ENTITY(oiDonut2, piSceneTrevor,  GET_PED_BONE_INDEX(piSceneTrevor, BONETAG_PH_R_HAND), vDonut2AttachPos, vDonut2AttachRot)
				ENDIF
			ENDIF
		ENDIF
		
		//CELLPHONE
		SAFE_DELETE_OBJECT(oiCellPhone)
		oiCellPhone = CREATE_OBJECT_NO_OFFSET(PROP_PLAYER_PHONE_01, vCellphonePos, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(oiCellPhone)
			IF DOES_ENTITY_EXIST(piSceneMichael)
				IF NOT IS_ENTITY_DEAD(piSceneMichael)
					ATTACH_ENTITY_TO_ENTITY(oiCellPhone, piSceneMichael,  GET_PED_BONE_INDEX(piSceneMichael, BONETAG_PH_L_HAND), vCellphoneAttachPos, vCellphoneAttachRot)
				ENDIF
			ENDIF
		ENDIF
		
		
		WAIT(0)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
		
		WAIT(0)
		
		CLEAR_AREA(vCarStartPos, 100.0, TRUE)
		CLEAR_AREA_OF_VEHICLES(vCarStartPos, 100.0)
		CLEAR_AREA_OF_COPS(vCarStartPos, 100.0)
		
		LOAD_SCENE(vCarStartPos)
		
		//Fade In
		IF IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(0)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_FRANKLIN))
		AND NOT IS_ENTITY_DEAD(vehPlayer)
			FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
		ENDIF
		
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		VECTOR vPoint1 = <<2825.1, 4647.5, 40.1>>
		VECTOR vPoint2 = <<2660.4, 4641.2, 51.5>>
		
		WHILE NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayer, vPoint1, vPoint2, 10.0, TRUE, TRUE, TM_IN_VEHICLE) 
			CDEBUG3LN(DEBUG_MISSION, "Waiting for player to drive to trigger...")
			WAIT(0)
		ENDWHILE
		
		DISPLAY_RADAR(FALSE)
		DISPLAY_HUD(FALSE)
		
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		VECTOR vDriveTarget = <<2602.3, 5299.4, 44.4>>
		IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_FRANKLIN))
		AND NOT IS_ENTITY_DEAD(vehPlayer)
			CLEAR_PED_TASKS(PLAYER_PED(CHAR_FRANKLIN))
			TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehPlayer, vDriveTarget, 200.0, DRIVINGSTYLE_ACCURATE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5.0, 2.0 )
			
			ADD_PED_FOR_DIALOGUE(splineCamSpeech1, 3, PLAYER_PED_ID(), "FRANKLIN")
			ADD_PED_FOR_DIALOGUE(splineCamSpeech1, 1, piSceneMichael, "MICHAEL")
			REMOVE_PED_FOR_DIALOGUE(splineCamSpeech1, 1)
			PLAYER_CALL_CHAR_CELLPHONE(splineCamSpeech1, CHAR_MICHAEL, "CST3AUD", "CST3_2secs", CONV_PRIORITY_HIGH)
		ENDIF
		
		REQUEST_CUTSCENE("Car_steal_3_mcs_2", CUTSCENE_REQUESTED_FROM_Z_SKIP)
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED(CHAR_TREVOR))
		ENDIF
		
		REQUEST_ANIM_DICT(strAnimDictCamShake)
		
		WAIT(5000)
		
		bStoppedTrevRec = FALSE
		bStartedTrevRec2 = FALSE

		eCarStealSwitchCamState = SWITCH_CAM_START_SPLINE1
		
		bResetDebugScenario = FALSE
	ENDIF
	
	IF HANDLE_SWITCH_CINEMATIC(scsSwitchCam1, scsSwitchCam2)
		
	ENDIF
	
	IF NOT bStoppedTrevRec
		IF DOES_ENTITY_EXIST(vehPolice1)
			IF NOT IS_ENTITY_DEAD(vehPolice1)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
					CDEBUG2LN(DEBUG_MISSION, "Time of Trev's recording: ", GET_TIME_POSITION_IN_RECORDING(vehPolice1))
					IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) >= fStopTrevRecTime
					
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
						START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehPolice1, 500, sCarrecCop /*sCarrecChase*/, 10.0, DRIVINGMODE_AVOIDCARS)
						SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(vehPolice1, iTrevRecRevertTime, DRIVINGMODE_AVOIDCARS, TRUE)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPolice1, fTrevRecSkipTime2)
						
						bStartedTrevRec2 = bStartedTrevRec2
						
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						
						bStoppedTrevRec = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bCellphoneReattach
		IF DOES_ENTITY_EXIST(oiCellPhone)
		AND DOES_ENTITY_EXIST(PLAYER_PED_ID())//,sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			DETACH_ENTITY(oiCellPhone)
			ATTACH_ENTITY_TO_ENTITY(oiCellPhone, PLAYER_PED_ID(),  GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), vCellphoneAttachPos, vCellphoneAttachRot)
		ENDIF
		bCellphoneReattach = FALSE
	ENDIF
	
	IF bDebugStartWhooshIn
		STOP_SOUND(iWooshIn_ID)
		iWooshIn_ID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iWooshIn_ID , "IN", "SHORT_PLAYER_SWITCH_SOUND_SET")		
		bDebugStartWhooshIn = FALSE
	ENDIF
	
	IF bDebugStopWhooshIn
		STOP_SOUND(iWooshIn_ID)
		bDebugStopWhooshIn = FALSE
	ENDIF
	
	IF bDebugStartWhooshOut
		STOP_SOUND(iWooshOut_ID)
		iWooshOut_ID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iWooshOut_ID, "OUT", "SHORT_PLAYER_SWITCH_SOUND_SET")
		bDebugStartWhooshOut = FALSE			
	ENDIF
	
	IF bDebugStopWhooshOut
		STOP_SOUND(iWooshOut_ID)
		bDebugStopWhooshOut = FALSE
	ENDIF

	
ENDPROC


//CONST_INT iCinCams 250
//
//STRUCT CamInterpData
//	VECTOR vPos
//	VECTOR vRot
//	FLOAT fFov
//	VECTOR vTanIn
//	VECTOR vTanOut
//	FLOAT fRec
//	BOOL bLink
//ENDSTRUCT
//
//CamInterpData sCamIntData[iCinCams]
//
//PROC INIT_CINEMATIC_CAMS()
//	sCamIntData[0].vPos = <<185.1290, 6594.1812, 31.9177>>
//	sCamIntData[0].vRot = <<-10.8216, 0.0000, 10.9492>>
//	sCamIntData[0].fFov = 45.0000
//	sCamIntData[0].vTanIn = <<0.0000, 0.0000, 0.0000>>
//	sCamIntData[0].vTanOut = <<0.0000, 0.0000, 0.0000>>
//	sCamIntData[0].fRec = 0.0000
//	sCamIntData[0].bLink = TRUE
//
//	sCamIntData[1].vPos = <<179.5871, 6583.5903, 31.7940>>
//	sCamIntData[1].vRot = <<-10.2048, 0.0000, -22.1628>>
//	sCamIntData[1].fFov = 45.0000
//	sCamIntData[1].vTanIn = <<-6.4000, -13.0000, 0.0000>>
//	sCamIntData[1].vTanOut = <<0.0000, 0.0000, 0.0000>>
//	sCamIntData[1].fRec = 2014.4010
//	sCamIntData[1].bLink = TRUE
//
//	sCamIntData[2].vPos = <<172.6011, 6564.0415, 31.7822>>
//	sCamIntData[2].vRot = <<-13.1517, 0.0000, 63.5101>>
//	sCamIntData[2].fFov = 45.0000
//	sCamIntData[2].vTanIn = <<-4.5000, -7.5000, 0.0000>>
//	sCamIntData[2].vTanOut = <<0.0000, 0.0000, 0.0000>>
//	sCamIntData[2].fRec = 3439.5959
//	sCamIntData[2].bLink = TRUE
//
//	sCamIntData[3].vPos = <<169.0506, 6556.9600, 31.7018>>
//	sCamIntData[3].vRot = <<-4.3687, 0.0000, 136.7127>>
//	sCamIntData[3].fFov = 36.5556
//	sCamIntData[3].vTanIn = <<-2.9000, -3.7000, 0.0000>>
//	sCamIntData[3].vTanOut = <<0.0000, 0.0000, 0.0000>>
//	sCamIntData[3].fRec = 4045.9939
//	sCamIntData[3].bLink = TRUE
//ENDPROC

FUNC FLOAT GET_INTERP_POINT_FLOAT(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC

FUNC VECTOR GET_INTERP_POINT_VECTOR(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN <<GET_INTERP_POINT_FLOAT(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC

//This function will return the value of a number after it has been raised to a power
FUNC FLOAT TPO(FLOAT fNumber, INT iPower)
	INT iLoopControl				//This will act as a counter to control how long the loop runs
	FLOAT fInitialValue = fNumber	//This will be used in the loop to multiply the new value
								//by its initial value
	FLOAT fResult = 0			//This is the value that the function returns
	
	//Use an if statement to determine how to handle the power function
	//This loop will determine how many times to multiply the number by itself
	IF iPower = 0
		fResult = 1 //Note n^0 = 1
	ELIF iPower = 1
		fResult = fNumber
	ELIF iPower < 0
		iLoopControl = -iPower + 1
		WHILE iLoopControl > 0
			fNumber *= fInitialValue
			iLoopControl--
		ENDWHILE
		fResult = 1 / fNumber
	ELIF iPower > 1
		iLoopControl = iPower - 1
		WHILE iLoopControl > 0
			fNumber *= fInitialValue
			iLoopControl--
		ENDWHILE
		fResult = fNumber
	ENDIF
	
	RETURN fResult
ENDFUNC

FUNC VECTOR HERMITE_CURVE(VECTOR vStartPoint, VECTOR vStartTangent, VECTOR vEndPoint, VECTOR vEndTangent, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	FLOAT fScale, H1, H2, H3, H4
	VECTOR vPoint
	
	fScale = fPointTime / (fEndTime - fStartTime)
	H1 = 2 * TPO(fScale, 3) - 3 * TPO(fScale, 2) + 1
	H2 = -2 * TPO(fScale, 3) + 3 * TPO(fScale, 2)
	H3 = TPO(fScale, 3) - 2 * TPO(fScale, 2) + fScale
	H4 = TPO(fScale, 3) - TPO(fScale, 2)
	vPoint = H1 * vStartPoint + H2 * vEndPoint + H3 * vStartTangent + H4 * vEndTangent
	RETURN vPoint
ENDFUNC

//FUNC VECTOR GET_START_TANGENT_WITH_ADJUST(INT i)
//	//N is the number of frames (seconds, whatever) between two keypoints.
//	//
//	//                    2 * N
//	//                        i-1
//	//TD  =  TD *     ---------------       adjustment of outgoing tangent
//	// i      i           N    + N
//	//                    i-1    i
//	
//	RETURN sCamIntData[i].vTanIn * ((2 * (sCamIntData[i].fRec - sCamIntData[CLAMP_INT(i - 1, 0, iCinCams - 1)].fRec)) / ((sCamIntData[i].fRec - sCamIntData[CLAMP_INT(i - 1, 0, iCinCams - 1)].fRec) + (sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec - sCamIntData[i].fRec)))
//ENDFUNC

//FUNC VECTOR GET_END_TANGENT_WITH_ADJUST(INT i)
//	//N is the number of frames (seconds, whatever) between two keypoints.
//	//
//	//                    2 * N
//	//                        i
//	//TS  =  TS *     ---------------       adjustment of incomming tangent
//	// i      i           N    + N
//	//                    i-1    i
//	
//	RETURN sCamIntData[i].vTanOut * ((2 * (sCamIntData[i].fRec - sCamIntData[CLAMP_INT(i - 1, 0, iCinCams - 1)].fRec)) / ((sCamIntData[i].fRec - sCamIntData[CLAMP_INT(i - 1, 0, iCinCams - 1)].fRec) + (sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec - sCamIntData[i].fRec)))
//ENDFUNC

//VECTOR vCamCurrentPos, vCamTargetPos, vCamCurrentRot, vCamTargetRot
//FLOAT fShakeTime
//
//PROC CAM_SHAKE(CAMERA_INDEX camShake)
//	IF ARE_VECTORS_EQUAL(vCamCurrentPos, VECTOR_ZERO)
//		vCamCurrentPos = GET_CAM_COORD(camShake)
//		vCamTargetPos = GET_CAM_COORD(camShake)
//	ENDIF
//	
//	IF ARE_VECTORS_EQUAL(vCamCurrentRot, VECTOR_ZERO)
//		vCamCurrentRot = GET_CAM_ROT(camShake)
//		vCamTargetRot = GET_CAM_ROT(camShake)
//	ENDIF
//	
//	IF NOT ARE_VECTORS_EQUAL(vCamCurrentPos, VECTOR_ZERO)
//	AND NOT ARE_VECTORS_EQUAL(vCamCurrentRot, VECTOR_ZERO)
//		IF ARE_VECTORS_EQUAL(vCamCurrentPos, vCamTargetPos)
//		AND ARE_VECTORS_EQUAL(vCamCurrentRot, vCamTargetRot)
//			vCamTargetPos += <<GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1), GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1), GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1)>>
//			vCamTargetRot += <<GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1), GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1), GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1)>>
//			fShakeTime = 0.0
//		ELSE
//			vCamCurrentPos = GET_INTERP_POINT_VECTOR(vCamCurrentPos, vCamTargetPos, 0.0, 10.0, fShakeTime)
//			vCamCurrentRot = GET_INTERP_POINT_VECTOR(vCamCurrentRot, vCamTargetRot, 0.0, 10.0, fShakeTime)
//			
//			fShakeTime += 1.0
//		ENDIF
//	ENDIF
//	
//	SET_CAM_PARAMS(camShake,
//					GET_CAM_COORD(camShake) + vCamCurrentPos,
//					GET_CAM_COORD(camShake) + vCamCurrentRot,
//					GET_CAM_FOV(camShake))
//ENDPROC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.Z) * COS(vRot.X), COS(vRot.Z) * COS(vRot.X), SIN(vRot.X)>>
ENDFUNC

FUNC VECTOR INVERT_VECTOR(VECTOR vVector)
	vVector.X = -vVector.X
	vVector.Y = -vVector.Y
	vVector.Z = -vVector.Z
	
	RETURN vVector
ENDFUNC

FUNC FLOAT GET_HEADING_BETWEEN_VECTORS(VECTOR vVector1, VECTOR vVector2)
	VECTOR vTemp = vVector2 - vVector1
	
	RETURN GET_HEADING_FROM_VECTOR_2D(vTemp.X, vTemp.Y)
ENDFUNC

//OBJECT_INDEX objDummy
//
//PROC DRAW_CAMERA(VECTOR vCoords, VECTOR vRot, FLOAT fFov, FLOAT fScale = 1.0, INT iR = 255, INT iG = 0, INT iB = 0, INT iA = 255)
//	fFov = fFov
//	
//	IF NOT DOES_ENTITY_EXIST(objDummy)
//		objDummy = CREATE_OBJECT(PROP_POOL_CUEBALL, <<1.0, 1.0, 1.0>>)
//	ENDIF
//	
//	SET_ENTITY_COORDS(objDummy, vCoords)
//	SET_ENTITY_ROTATION(objDummy, vRot)
//	
//	FLOAT fWidth = 0.15 * fScale
//	FLOAT fDepth = 0.3 * fScale
//	
//	//	Front Poly
//	//Vertices
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, -fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, -fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, -fWidth>>), iR, iG, iB, iA)
//	//Triangles
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	//	Back Poly
//	//Vertices
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, -fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, -fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, -fWidth>>), iR, iG, iB, iA)
//	//Triangles
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	//	Mid Poly
//	//Vertices
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, -fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, fWidth>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, -fWidth>>), iR, iG, iB, iA)
//	//Triangles
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, -fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, -fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, -fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<fWidth, fDepth, -fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, -fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, -fDepth, -fWidth>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-fWidth, fDepth, -fWidth>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	//	Cone Front Poly
//	//Vertices
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), iR, iG, iB, iA)
//	//Triangles
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	//	Cone Back Poly
//	//Vertices
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), iR, iG, iB, iA)
//	
//	//	Cone Mid Poly
//	//Vertices
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), iR, iG, iB, iA)
//	DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), iR, iG, iB, iA)
//	//Triangles
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, ((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//	DRAW_DEBUG_POLY(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 2), fDepth, -((fWidth / 4) * 2)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), ((fWidth / 4) * 3)>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDummy, <<-((fWidth / 4) * 3), fDepth + (fDepth / 2), -((fWidth / 4) * 3)>>), iR, iG, iB, CLAMP_INT(iA - 150, 0, 255))
//ENDPROC

//PROC CINEMATIC_CAMERA()
//	//Make the camera
//	IF NOT DOES_CAM_EXIST(camCinematic)
//		camCinematic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//		
//		SET_CAM_NEAR_CLIP(camCinematic, 0.01)
//		SET_CAM_FAR_DOF(camCinematic, 10.0)
//		
//		#IF IS_DEBUG_BUILD
//			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//		#ENDIF
//		
//		INIT_CINEMATIC_CAMS()
//	ELSE
//		IF DOES_ENTITY_EXIST(vehRival1)
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
//				//Get the time in the recording
//				FLOAT fRecTime
//				
//				fRecTime = GET_TIME_POSITION_IN_RECORDING(vehRival1)
//				
//				INT i
//				
//				#IF IS_DEBUG_BUILD
//					//Debug Line
//					FLOAT ii
//				#ENDIF
//				
//				REPEAT iCinCams i
//					IF sCamIntData[i].fFov > 1.0
//						#IF IS_DEBUG_BUILD
//							IF bCinCamDebug = TRUE
////								REPEAT ROUND(sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec - sCamIntData[i].fRec) / 100 ii
////									DRAW_DEBUG_LINE(HERMITE_CURVE(sCamIntData[i].vPos, sCamIntData[i].vTanIn, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, sCamIntData[i].vTanOut, sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, TO_FLOAT(ii * 100)),
////													HERMITE_CURVE(sCamIntData[i].vPos, sCamIntData[i].vTanIn, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, sCamIntData[i].vTanOut, sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, TO_FLOAT((ii + 1) * 100)))
////								ENDREPEAT
//								
//								ii = 0.0
//								FLOAT fStep = 0.04
//								
//								VECTOR vColour = <<0.0, 0.0, 0.0>>
//								
//								WHILE ii < 1.0
//									IF sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fFov != 0
//										vColour.X = 255.0
//										
//										IF sCamIntData[i].bLink = FALSE
//											vColour.Y = CLAMP(255.0 - (255.0 / (GET_DISTANCE_BETWEEN_COORDS(HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_END_TANGENT_WITH_ADJUST(i), 0.0, 1.0, ii), 
//																											HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_END_TANGENT_WITH_ADJUST(i), 0.0, 1.0, CLAMP(ii + fStep, 0.0, 1.0))) * 5.0)), 
//															0.0, 
//															255.0)
//											
//											IF vColour.Y >= 255.0
//												vColour.X = CLAMP(255.0 - (255.0 / ((GET_DISTANCE_BETWEEN_COORDS(HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_END_TANGENT_WITH_ADJUST(i), 0.0, 1.0, ii), 
//																												HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_END_TANGENT_WITH_ADJUST(i), 0.0, 1.0, CLAMP(ii + fStep, 0.0, 1.0))) * 5.0) - 255.0)), 
//																0.0, 
//																255.0)
//											ENDIF
//										ELSE
//											vColour.Y = CLAMP(255.0 - (255.0 / (GET_DISTANCE_BETWEEN_COORDS(HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_START_TANGENT_WITH_ADJUST(CLAMP_INT(i + 1, 0, iCinCams - 1)), 0.0, 1.0, ii), 
//																											HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_START_TANGENT_WITH_ADJUST(CLAMP_INT(i + 1, 0, iCinCams - 1)), 0.0, 1.0, CLAMP(ii + fStep, 0.0, 1.0))) * 5.0)), 
//															0.0, 
//															255.0)
//											
//											IF vColour.Y >= 255.0
//												vColour.X = CLAMP(255.0 - (255.0 / ((GET_DISTANCE_BETWEEN_COORDS(HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_START_TANGENT_WITH_ADJUST(CLAMP_INT(i + 1, 0, iCinCams - 1)), 0.0, 1.0, ii), 
//																												HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_START_TANGENT_WITH_ADJUST(CLAMP_INT(i + 1, 0, iCinCams - 1)), 0.0, 1.0, CLAMP(ii + fStep, 0.0, 1.0))) * 5.0) - 255.0)), 
//																0.0, 
//																255.0)
//											ENDIF
//										ENDIF
//										
//										IF sCamIntData[i].bLink = FALSE
//											DRAW_DEBUG_LINE(HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_END_TANGENT_WITH_ADJUST(i), 0.0, 1.0, ii),
//															HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_END_TANGENT_WITH_ADJUST(i), 0.0, 1.0, CLAMP(ii + fStep, 0.0, 1.0)),
//															ROUND(vColour.X),
//															ROUND(vColour.Y),
//															ROUND(vColour.Z))
//										ELSE
//											DRAW_DEBUG_LINE(HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_START_TANGENT_WITH_ADJUST(CLAMP_INT(i + 1, 0, iCinCams - 1)), 0.0, 1.0, ii),
//															HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_START_TANGENT_WITH_ADJUST(CLAMP_INT(i + 1, 0, iCinCams - 1)), 0.0, 1.0, CLAMP(ii + fStep, 0.0, 1.0)),
//															ROUND(vColour.X),
//															ROUND(vColour.Y),
//															ROUND(vColour.Z))
//										ENDIF
//									ENDIF
//									
//									ii += fStep
//								ENDWHILE
//								
//								IF i = iCinCamID
//									DRAW_CAMERA(sCamIntData[i].vPos, sCamIntData[i].vRot, sCamIntData[i].fFov, 1.0, 255, 0, 255)
//									DRAW_DEBUG_SPHERE(sCamIntData[i].vPos, 0.025, 255, 0, 255)
//									
//									DRAW_DEBUG_LINE(sCamIntData[i].vPos, sCamIntData[i].vPos + sCamIntData[i].vTanIn, 255, 0, 255)
//									
//									//DRAW_DEBUG_SPHERE(sCamIntData[i].vPos + sCamIntData[i].vTanIn, 0.1, 255, 0, 255)
//									DRAW_DEBUG_POLY_WITH_THREE_COLOURS(sCamIntData[i].vPos + sCamIntData[i].vTanIn,
//																		GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sCamIntData[i].vPos + sCamIntData[i].vTanIn, GET_HEADING_BETWEEN_VECTORS(sCamIntData[i].vPos, sCamIntData[i].vPos + sCamIntData[i].vTanIn), <<0.5, 0.5, 0.0>>),
//																		GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sCamIntData[i].vPos + sCamIntData[i].vTanIn, GET_HEADING_BETWEEN_VECTORS(sCamIntData[i].vPos, sCamIntData[i].vPos + sCamIntData[i].vTanIn), <<-0.5, 0.5, 0.0>>),
//																		255, 0, 255, 255,
//																		255, 0, 255, 0,
//																		255, 0, 255, 0)
//									
//									IF sCamIntData[i].bLink = FALSE
//										DRAW_DEBUG_LINE(sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos + sCamIntData[i].vTanOut, 255, 0, 255)
//										
//										//DRAW_DEBUG_SPHERE(sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos + sCamIntData[i].vTanOut, 0.1, 255, 0, 255)
//										DRAW_DEBUG_POLY_WITH_THREE_COLOURS(sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos + sCamIntData[i].vTanOut,
//																			GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos + sCamIntData[i].vTanOut, GET_HEADING_BETWEEN_VECTORS(sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos + sCamIntData[i].vTanOut), <<0.5, 0.5, 0.0>>),
//																			GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos + sCamIntData[i].vTanOut, GET_HEADING_BETWEEN_VECTORS(sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos + sCamIntData[i].vTanOut), <<-0.5, 0.5, 0.0>>),
//																			255, 0, 255, 255,
//																			255, 0, 255, 0,
//																			255, 0, 255, 0)
//									ENDIF
//								ELIF i = CLAMP_INT(iCinCamID + 1, 0, iCinCams - 1)
//								AND sCamIntData[i].bLink = FALSE
//									DRAW_CAMERA(sCamIntData[i].vPos, sCamIntData[i].vRot, sCamIntData[i].fFov, 1.0, 255, 0, 255)
//									DRAW_DEBUG_SPHERE(sCamIntData[i].vPos, 0.025, 255, 0, 255)
//								ELSE
//									DRAW_CAMERA(sCamIntData[i].vPos, sCamIntData[i].vRot, sCamIntData[i].fFov, 1.0, ROUND(vColour.X), ROUND(vColour.Y), ROUND(vColour.Z))
//									DRAW_DEBUG_SPHERE(sCamIntData[i].vPos, 0.025, ROUND(vColour.X), ROUND(vColour.Y), ROUND(vColour.Z))
//								ENDIF
//							ENDIF
//						#ENDIF
//						
//						IF sCamIntData[i].fRec < fRecTime
//							IF sCamIntData[i].bLink = FALSE
//								SET_CAM_PARAMS(camCinematic,
//								/*Pos*/		HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_END_TANGENT_WITH_ADJUST(i), sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, fRecTime - sCamIntData[i].fRec),
//								/*Rot*/		HERMITE_CURVE(sCamIntData[i].vRot, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vRot, GET_END_TANGENT_WITH_ADJUST(i), sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, fRecTime - sCamIntData[i].fRec),	//GET_INTERP_POINT_VECTOR(sCamIntData[i].vRot, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vRot, sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, fRecTime),
//								/*Fov*/		CLAMP(GET_INTERP_POINT_FLOAT(sCamIntData[i].fFov, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fFov, sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, fRecTime), 1.0, 130.0))
//							ELSE
//								SET_CAM_PARAMS(camCinematic,
//								/*Pos*/		HERMITE_CURVE(sCamIntData[i].vPos, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vPos, GET_START_TANGENT_WITH_ADJUST(CLAMP_INT(i + 1, 0, iCinCams - 1)), sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, fRecTime - sCamIntData[i].fRec),
//								/*Rot*/		HERMITE_CURVE(sCamIntData[i].vRot, GET_START_TANGENT_WITH_ADJUST(i), sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vRot, GET_START_TANGENT_WITH_ADJUST(CLAMP_INT(i + 1, 0, iCinCams - 1)), sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, fRecTime - sCamIntData[i].fRec),	//GET_INTERP_POINT_VECTOR(sCamIntData[i].vRot, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].vRot, sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, fRecTime),
//								/*Fov*/		CLAMP(GET_INTERP_POINT_FLOAT(sCamIntData[i].fFov, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fFov, sCamIntData[i].fRec, sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec, fRecTime), 1.0, 130.0))
//							ENDIF
//							
//							#IF IS_DEBUG_BUILD
//								IF bCinCamDebug = TRUE
//									IF fRecTime > sCamIntData[i].fRec
//									AND fRecTime < sCamIntData[CLAMP_INT(i + 1, 0, iCinCams - 1)].fRec
//										DRAW_CAMERA(GET_CAM_COORD(camCinematic), GET_CAM_ROT(camCinematic), GET_CAM_FOV(camCinematic), 1.2, 255, 0, 0)
//										DRAW_DEBUG_SPHERE(GET_CAM_COORD(camCinematic), 0.025, 255, 0, 0)
//									ENDIF
//								ENDIF
//							#ENDIF
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				
//				#IF IS_DEBUG_BUILD
//					IF bCinCamSave = TRUE
//						//Get the time in the recording
//						CAMERA_INDEX camTemp
//						camTemp = GET_DEBUG_CAM()
//						
//						sCamIntData[iCinCamID].vPos = GET_CAM_COORD(camTemp)
//						sCamIntData[iCinCamID].vRot = GET_CAM_ROT(camTemp)
//						sCamIntData[iCinCamID].fFov = GET_CAM_FOV(camTemp)
//						sCamIntData[iCinCamID].vTanIn = VECTOR_ZERO
//						sCamIntData[iCinCamID].vTanOut = VECTOR_ZERO
//						sCamIntData[iCinCamID].fRec = fRecTime
//						sCamIntData[iCinCamID].bLink = FALSE
//						
//						vCinCamPos = GET_CAM_COORD(camTemp)
//						vCinCamRot = GET_CAM_ROT(camTemp)
//						fCinCamFov = GET_CAM_FOV(camTemp)
//						vCinCamTanIn = VECTOR_ZERO
//						vCinCamTanOut = VECTOR_ZERO
//						fCinCamRec = fRecTime
//						bCinCamLink = FALSE
//						
//						iCinCamID++
//						
//						bCinCamSave = FALSE
//					ENDIF
//					
//					IF iCinCamIDLast != iCinCamID
//						vCinCamPos = sCamIntData[iCinCamID].vPos
//						vCinCamRot = sCamIntData[iCinCamID].vRot
//						fCinCamFov = sCamIntData[iCinCamID].fFov
//						vCinCamTanIn = sCamIntData[iCinCamID].vTanIn
//						vCinCamTanOut = sCamIntData[iCinCamID].vTanOut
//						fCinCamRec = sCamIntData[iCinCamID].fRec
//						bCinCamLink = sCamIntData[iCinCamID].bLink
//						
//						iCinCamIDLast = iCinCamID
//					ENDIF
//										
//					IF NOT ARE_VECTORS_EQUAL(vCinCamPosLast, vCinCamPos)
//						sCamIntData[iCinCamID].vPos = vCinCamPos
//						
//						vCinCamPosLast = vCinCamPos
//					ENDIF
//					
//					IF NOT ARE_VECTORS_EQUAL(vCinCamRotLast, vCinCamRot)
//						sCamIntData[iCinCamID].vRot = vCinCamRot
//						
//						vCinCamRotLast = vCinCamRot
//					ENDIF
//					
//					IF fCinCamFovLast != fCinCamFov
//						sCamIntData[iCinCamID].fFov = fCinCamFov
//						
//						fCinCamFovLast = fCinCamFov
//					ENDIF
//					
//					IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
//					AND NOT IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN)
//						VECTOR vMouse
//						
//						vMouse = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
//						PRINTVECTOR(vMouse)
//						PRINTNL()
//						
//						vCinCamTanIn.X = vMouse.X - sCamIntData[iCinCamID].vPos.X
//						vCinCamTanIn.Y = vMouse.Y - sCamIntData[iCinCamID].vPos.Y
//						
//						//GET_MOUSE_POSITION(vMouse.X, vMouse.Y)
//						
//						//vCinCamTanIn.X = (vMouse.X - 0.5) * 10.0
//						//vCinCamTanIn.Y = (vMouse.Y - 0.5) * 10.0
//					ENDIF
//					
//					IF NOT ARE_VECTORS_EQUAL(vCinCamTanInLast, vCinCamTanIn)
//						sCamIntData[iCinCamID].vTanIn = vCinCamTanIn
//						
//						fCinCamTanInMag = SQRT(TPO(vCinCamTanIn.X, 2) + TPO(vCinCamTanIn.Y, 2) + TPO(vCinCamTanIn.Z, 2))
//						
//						vCinCamTanInLast = vCinCamTanIn
//					ENDIF
//					
//					IF fCinCamTanInMagLast != fCinCamTanInMag
//						VECTOR vUnit = vCinCamTanIn / VMAG(vCinCamTanIn)
//						
//						vCinCamTanIn = vUnit * fCinCamTanInMag
//						
//						fCinCamTanInMagLast = fCinCamTanInMag
//					ENDIF
//					
//					IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
//					AND IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN)
//						VECTOR vMouse
//						
//						vMouse = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
//						PRINTVECTOR(vMouse)
//						PRINTNL()
//						
//						vCinCamTanOut.X = vMouse.X - sCamIntData[CLAMP_INT(iCinCamID + 1, 0, iCinCams - 1)].vPos.X
//						vCinCamTanOut.Y = vMouse.Y - sCamIntData[CLAMP_INT(iCinCamID + 1, 0, iCinCams - 1)].vPos.Y
//						
//						//GET_MOUSE_POSITION(vMouse.X, vMouse.Y)
//						
//						//vCinCamTanOut.X = (vMouse.X - 0.5) * 10.0
//						//vCinCamTanOut.Y = (vMouse.Y - 0.5) * 10.0
//					ENDIF
//					
//					IF NOT ARE_VECTORS_EQUAL(vCinCamTanOutLast, vCinCamTanOut)
//						sCamIntData[iCinCamID].vTanOut = vCinCamTanOut
//						
//						fCinCamTanOutMag = SQRT(TPO(vCinCamTanOut.X, 2) + TPO(vCinCamTanOut.Y, 2) + TPO(vCinCamTanOut.Z, 2))
//						
//						vCinCamTanOutLast = vCinCamTanOut
//					ENDIF
//					
//					IF fCinCamTanOutMagLast != fCinCamTanOutMag
//						VECTOR vUnit = vCinCamTanOut / VMAG(vCinCamTanOut)
//						
//						vCinCamTanOut = vUnit * fCinCamTanOutMag
//						
//						fCinCamTanOutMagLast = fCinCamTanOutMag
//					ENDIF
//					
//					IF fCinCamRecLast != fCinCamRec
//						sCamIntData[iCinCamID].fRec = fCinCamRec
//						
//						fCinCamRecLast = fCinCamRec
//					ENDIF
//					
//					IF bCinCamLinkLast != bCinCamLink
//						sCamIntData[iCinCamID].bLink = bCinCamLink
//						
//						bCinCamLinkLast = bCinCamLink
//					ENDIF
//					
//					IF bCinCamOutput = TRUE
//						INT iCamIndex
//						
//						OPEN_DEBUG_FILE()
//						
//						REPEAT iCinCams iCamIndex
//							IF NOT ARE_VECTORS_EQUAL(sCamIntData[iCamIndex].vPos, VECTOR_ZERO)
//								SAVE_STRING_TO_DEBUG_FILE("sCamIntData[")
//								SAVE_INT_TO_DEBUG_FILE(iCamIndex)
//								SAVE_STRING_TO_DEBUG_FILE("].vPos = <<")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vPos.X)
//								SAVE_STRING_TO_DEBUG_FILE(", ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vPos.Y)
//								SAVE_STRING_TO_DEBUG_FILE(", ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vPos.Z)
//								SAVE_STRING_TO_DEBUG_FILE(">>")
//								SAVE_NEWLINE_TO_DEBUG_FILE()
//								SAVE_STRING_TO_DEBUG_FILE("sCamIntData[")
//								SAVE_INT_TO_DEBUG_FILE(iCamIndex)
//								SAVE_STRING_TO_DEBUG_FILE("].vRot = <<")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vRot.X)
//								SAVE_STRING_TO_DEBUG_FILE(", ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vRot.Y)
//								SAVE_STRING_TO_DEBUG_FILE(", ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vRot.Z)
//								SAVE_STRING_TO_DEBUG_FILE(">>")
//								SAVE_NEWLINE_TO_DEBUG_FILE()
//								SAVE_STRING_TO_DEBUG_FILE("sCamIntData[")
//								SAVE_INT_TO_DEBUG_FILE(iCamIndex)
//								SAVE_STRING_TO_DEBUG_FILE("].fFov = ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].fFov)
//								SAVE_NEWLINE_TO_DEBUG_FILE()
//								SAVE_STRING_TO_DEBUG_FILE("sCamIntData[")
//								SAVE_INT_TO_DEBUG_FILE(iCamIndex)
//								SAVE_STRING_TO_DEBUG_FILE("].vTanIn = <<")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vTanIn.X)
//								SAVE_STRING_TO_DEBUG_FILE(", ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vTanIn.Y)
//								SAVE_STRING_TO_DEBUG_FILE(", ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vTanIn.Z)
//								SAVE_STRING_TO_DEBUG_FILE(">>")
//								SAVE_NEWLINE_TO_DEBUG_FILE()
//								SAVE_STRING_TO_DEBUG_FILE("sCamIntData[")
//								SAVE_INT_TO_DEBUG_FILE(iCamIndex)
//								SAVE_STRING_TO_DEBUG_FILE("].vTanOut = <<")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vTanOut.X)
//								SAVE_STRING_TO_DEBUG_FILE(", ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vTanOut.Y)
//								SAVE_STRING_TO_DEBUG_FILE(", ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].vTanOut.Z)
//								SAVE_STRING_TO_DEBUG_FILE(">>")
//								SAVE_NEWLINE_TO_DEBUG_FILE()
//								SAVE_STRING_TO_DEBUG_FILE("sCamIntData[")
//								SAVE_INT_TO_DEBUG_FILE(iCamIndex)
//								SAVE_STRING_TO_DEBUG_FILE("].fRec = ")
//								SAVE_FLOAT_TO_DEBUG_FILE(sCamIntData[iCamIndex].fRec)
//								SAVE_NEWLINE_TO_DEBUG_FILE()
//								SAVE_STRING_TO_DEBUG_FILE("sCamIntData[")
//								SAVE_INT_TO_DEBUG_FILE(iCamIndex)
//								SAVE_STRING_TO_DEBUG_FILE("].bLink = ")
//								IF sCamIntData[iCamIndex].bLink = TRUE
//									SAVE_STRING_TO_DEBUG_FILE("TRUE")
//								ELSE
//									SAVE_STRING_TO_DEBUG_FILE("FALSE")
//								ENDIF
//								SAVE_NEWLINE_TO_DEBUG_FILE()
//								SAVE_NEWLINE_TO_DEBUG_FILE()
//							ENDIF
//						ENDREPEAT
//						
//						CLOSE_DEBUG_FILE()
//						
//						bCinCamOutput = FALSE
//					ENDIF
//				#ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

FUNC FLOAT GET_HEADING_FROM_VECTOR(VECTOR vVector)
	RETURN vVector.Z
ENDFUNC

STRING sAudioEvent
INT iAudioPrepareTimer	//This is an override in case an audio event is prepared but never used

PROC AUDIO_CONTROLLER()	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("<<<<<<<< AUDIO_CONTROLLER >>>>>>>> (sAudioEvent = ", sAudioEvent, ")")	ENDIF	#ENDIF
	IF ePlayAudioTrack = NO_AUDIO
	AND (NOT IS_MUSIC_ONESHOT_PLAYING()
	OR GET_GAME_TIMER() > iAudioPrepareTimer)
		SWITCH ePrepAudioTrack
			CASE CAR1_MISSION_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - CAR1_MISSION_START")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "CAR1_MISSION_START")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "CAR1_MISSION_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("CAR1_MISSION_START")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - CAR1_MISSION_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE CAR1_CHASE_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - CAR1_CHASE_START")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "CAR1_CHASE_START")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "CAR1_CHASE_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("CAR1_CHASE_START")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - CAR1_CHASE_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH ePlayAudioTrack
		CASE CAR1_MISSION_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_MISSION_START")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "CAR1_MISSION_START")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "CAR1_MISSION_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("CAR1_MISSION_START")
					IF TRIGGER_MUSIC_EVENT("CAR1_MISSION_START")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_MISSION_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE CAR1_MISSION_RESTART	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_MISSION_RESTART")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("CAR1_MISSION_RESTART")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_MISSION_RESTART")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE CAR1_COP_BIKES	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_COP_BIKES")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("CAR1_COP_BIKES")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_COP_BIKES")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE CAR1_COPS_RESTART	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_COPS_RESTART")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("CAR1_COPS_RESTART")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_COPS_RESTART")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE CAR1_APPROACH	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_APPROACH")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("CAR1_APPROACH")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_APPROACH")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE CAR1_CHASE_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_CHASE_START")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "CAR1_CHASE_START")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "CAR1_CHASE_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("CAR1_CHASE_START")
					IF TRIGGER_MUSIC_EVENT("CAR1_CHASE_START")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_CHASE_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE CAR1_PULL_OVER	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_PULL_OVER")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("CAR1_PULL_OVER")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_PULL_OVER")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE CAR1_CHASE_RESTART	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_CHASE_RESTART")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("CAR1_CHASE_RESTART")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_CHASE_RESTART")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE CAR1_BRIDGE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_BRIDGE")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("CAR1_BRIDGE")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_BRIDGE")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE CAR1_MISSION_FAIL	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - CAR1_MISSION_FAIL")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("CAR1_MISSION_FAIL")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - CAR1_MISSION_FAIL")	ENDIF	#ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC LOAD_AUDIO(AUDIO_TRACK eSetAudioTrack)
	ePrepAudioTrack = eSetAudioTrack	
	
	AUDIO_CONTROLLER()
ENDPROC

PROC PLAY_AUDIO(AUDIO_TRACK eSetAudioTrack)
	ePlayAudioTrack = eSetAudioTrack
	
	AUDIO_CONTROLLER()
ENDPROC

//════════════════════════════╡ CONTROL PROCEDURES ╞═════════════════════════════

//Cleanup
PROC MISSION_CLEANUP(BOOL bRestart = FALSE, BOOL bDelayCleanup = FALSE)
	IF bVideoRecording
		REPLAY_STOP_EVENT()
		
		bVideoRecording = FALSE
	ENDIF
	
	RESET_PUSH_IN(pushInData)
	
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	CLEAR_HELP()
	
	ENABLE_PROCOBJ_CREATION()
	
	IF HAS_LABEL_BEEN_TRIGGERED("SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER")
		SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER(1.0)
		
		SET_LABEL_AS_TRIGGERED("SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER", FALSE)
	ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_SCREEN_FADED_OUT()
			IF bRestart
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_PED_POSITION(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE)
			ELSE
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ENDIF
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
		CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EARS)
		CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
		REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
		SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), -1)
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
		IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_MICHAEL))
			SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_PhoneDisableTalkingAnimations, FALSE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_TREVOR))
		IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_TREVOR))
			SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_TREVOR), PCF_PhoneDisableTalkingAnimations, FALSE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_FRANKLIN))
		IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_FRANKLIN))
			SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_PhoneDisableTalkingAnimations, FALSE)
		ENDIF
	ENDIF
	
	//Outfits/Weapons
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)  //reset that the player has changed outfit on mission to how it was at the start of the mission
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR, bHasChanged)
	
	IF bPassed = FALSE
		RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	ENDIF
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	SAFE_REMOVE_BLIP(blipStart)
	SAFE_REMOVE_BLIP(blipPlayer)
	SAFE_REMOVE_BLIP(blipRival1)
	SAFE_REMOVE_BLIP(blipRival2)
	SAFE_REMOVE_BLIP(blipPolice)
	SAFE_REMOVE_BLIP(blipGarage)
	
	//Car Recordings
	IF IS_SCREEN_FADED_OUT()
		IF DOES_ENTITY_EXIST(vehPolice1)
		AND NOT IS_ENTITY_DEAD(vehPolice1)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehPlayer)
		AND NOT IS_ENTITY_DEAD(vehPlayer)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehRival1)
		AND NOT IS_ENTITY_DEAD(vehRival1)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehRival1)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehRival2)
		AND NOT IS_ENTITY_DEAD(vehRival2)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehRival2)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bDelayCleanup	//Force Cleanup (Death/Arrest/Multiplayer)
		#IF IS_DEBUG_BUILD	IF bAutoSkipping	#ENDIF	//Z-skips cleanup
		//Delete all peds, vehicles and blips
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		SAFE_DELETE_PED(pedRival1)
		SAFE_DELETE_PED(pedRival2)
		SAFE_DELETE_PED(pedDevin)
		SAFE_DELETE_PED(pedMolly)
		SAFE_DELETE_PED(pedWorker1)
		SAFE_DELETE_PED(pedWorker2)
		
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
				SAFE_DELETE_VEHICLE(vehPlayer)
			ENDIF
		ENDIF
		SAFE_DELETE_VEHICLE(vehRival1)
		SAFE_DELETE_VEHICLE(vehPolice1)
		SAFE_DELETE_VEHICLE(vehDevin)
		SAFE_DELETE_VEHICLE(vehCutscene)
		#IF IS_DEBUG_BUILD	ENDIF	#ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ENDIF
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		ENDIF
		IF DOES_ENTITY_EXIST(pedRival1)
			SET_PED_AS_NO_LONGER_NEEDED(pedRival1)
		ENDIF
		IF DOES_ENTITY_EXIST(pedRival2)
			SET_PED_AS_NO_LONGER_NEEDED(pedRival2)
		ENDIF
		IF DOES_ENTITY_EXIST(pedDevin)
			SET_PED_AS_NO_LONGER_NEEDED(pedDevin)
		ENDIF
		IF DOES_ENTITY_EXIST(pedMolly)
			SET_PED_AS_NO_LONGER_NEEDED(pedMolly)
		ENDIF
		IF DOES_ENTITY_EXIST(pedWorker1)
			SET_PED_AS_NO_LONGER_NEEDED(pedWorker1)
		ENDIF
		IF DOES_ENTITY_EXIST(pedWorker2)
			SET_PED_AS_NO_LONGER_NEEDED(pedWorker2)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayer)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(vehRival1)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehRival1)
		ENDIF
		IF DOES_ENTITY_EXIST(vehPolice1)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPolice1)
		ENDIF
		IF DOES_ENTITY_EXIST(vehDevin)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehDevin)
		ENDIF
		IF DOES_ENTITY_EXIST(vehCutscene)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCutscene)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			TASK_LEAVE_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			TASK_LEAVE_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			TASK_LEAVE_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlayer)
	AND NOT IS_ENTITY_DEAD(vehPlayer)
		IF IS_ENTITY_A_MISSION_ENTITY(vehPlayer) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehPlayer)
			SET_VEHICLE_INDICATOR_LIGHTS(vehPlayer, FALSE, FALSE)
			SET_ENTITY_COLLISION(vehPlayer, TRUE)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayer)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehRival1)
	AND NOT IS_ENTITY_DEAD(vehRival1)
		IF IS_ENTITY_A_MISSION_ENTITY(vehRival1) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehRival1)
			SET_VEHICLE_INDICATOR_LIGHTS(vehRival1, FALSE, FALSE)
			SET_ENTITY_COLLISION(vehRival1, TRUE)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehRival1)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehRival2)
	AND NOT IS_ENTITY_DEAD(vehRival2)
		IF IS_ENTITY_A_MISSION_ENTITY(vehRival2) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehRival2)
			SET_VEHICLE_INDICATOR_LIGHTS(vehRival2, FALSE, FALSE)
			SET_ENTITY_COLLISION(vehRival2, TRUE)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehRival2)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiCellphone)
		SET_OBJECT_AS_NO_LONGER_NEEDED(oiCellphone)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiDonut1)
		SET_OBJECT_AS_NO_LONGER_NEEDED(oiDonut1)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiDonut2)
		SET_OBJECT_AS_NO_LONGER_NEEDED(oiDonut2)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objID)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objID)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objRamp)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objRamp)
	ENDIF
	
	//Vehicle Assets
	REMOVE_VEHICLE_ASSET(CHEETAH)
	REMOVE_VEHICLE_ASSET(F620)
	REMOVE_VEHICLE_ASSET(POLICEB)
	
	//Model Hides
	IF HAS_LABEL_BEEN_TRIGGERED("ChopShopDoors")
		REMOVE_MODEL_HIDE(<<498.7252, -1317.7551, 28.2534>>, 1.0, PROP_SC1_06_GATE_L, FALSE)
		REMOVE_MODEL_HIDE(<<494.6904, -1312.0663, 28.2534>>, 1.0, PROP_SC1_06_GATE_R, FALSE)
		
		SET_LABEL_AS_TRIGGERED("ChopShopDoors", FALSE)
	ENDIF
	
	//Phone
	HIDE_ACTIVE_PHONE(FALSE)
	
	//Radio
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	
	//Cams
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	DESTROY_ALL_CAMS()
	
	STOP_GAMEPLAY_HINT(TRUE)
	
	SET_GAME_PAUSES_FOR_STREAMING(TRUE)
	
	END_SRL()
	
	//New Load Scene
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	//Stream Volume
	IF STREAMVOL_IS_VALID(svolCarStealOutro)
		STREAMVOL_DELETE(svolCarStealOutro)
	ENDIF
	
	//Reset Variables
	CLEAR_TRIGGERED_LABELS()
	CLEAR_LOADED_MODELS()
	CLEAR_LOADED_RECORDINGS()
	CLEAR_LOADED_ANIM_DICTS()
	
	SETTIMERA(0)
	
	iCutsceneStage = 0
	
	iDialogueStage = 0
	INT i
	REPEAT COUNT_OF(iDialogueLineCount) i
		iDialogueLineCount[i] = -1
	ENDREPEAT
	REPEAT COUNT_OF(iDialogueTimer) i
		iDialogueTimer[i] = 0
	ENDREPEAT
	
	bInitStage = FALSE
	
	bReplaySkip = FALSE
	
	bCutsceneSkipped = FALSE
	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
	
	fModifyTopSpeed = 0.0
	
	//Uber
	CLEANUP_UBER_PLAYBACK(TRUE)
	
	//Wanted
	SET_MAX_WANTED_LEVEL(6)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	//Emergency Calls
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	//Ambulances etc.
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	
	SET_ROADS_BACK_TO_ORIGINAL(<<-1928.3759, 4597.9775, 56.0588>> - <<1000.0, 1000.0, 1000.0>>, <<-1928.3759, 4597.9775, 56.0588>> + <<1000.0, 1000.0, 1000.0>>)
	
	SET_ROADS_BACK_TO_ORIGINAL(<<2581.105713, 361.805481, 116.468567>> - <<25.0, 25.0, 10.0>>, <<2581.105713, 361.805481, 116.468567>> + <<25.0, 25.0, 10.0>>)
	
	//Ped
	SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_HWAYCOP_01, FALSE)
	
	//Vehicle
	SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICEB, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(F620, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(ENTITYXF, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(CHEETAH, FALSE)
	
	//Relationship Groups
	REMOVE_RELATIONSHIP_GROUP(relGroupBuddy)
	REMOVE_RELATIONSHIP_GROUP(relGroupEnemy)
	
	//SWITCH
	SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
	SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
	SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
	
	//Audio Scenes
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_DEVIN_DRIVES_AWAY")
		STOP_AUDIO_SCENE("CAR_1_DEVIN_DRIVES_AWAY")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_GET_TO_RACE")
		STOP_AUDIO_SCENE("CAR_1_GET_TO_RACE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_MAIN")
		STOP_AUDIO_SCENE("CAR_1_RACE_MAIN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_SKIDDING_CARS")
		STOP_AUDIO_SCENE("CAR_1_RACE_SKIDDING_CARS")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_CARS_FOCUS_CAM")
		STOP_AUDIO_SCENE("CAR_1_RACE_CARS_FOCUS_CAM")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_FRANKLIN_CALLS_MICHAEL")
		STOP_AUDIO_SCENE("CAR_1_FRANKLIN_CALLS_MICHAEL")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_CHASE_MAIN")
		STOP_AUDIO_SCENE("CAR_1_BIKE_CHASE_MAIN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_ENTER_TUNNEL")
		STOP_AUDIO_SCENE("CAR_1_BIKE_ENTER_TUNNEL")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_PASS_THE_LOST")
		STOP_AUDIO_SCENE("CAR_1_BIKE_PASS_THE_LOST")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_GET_TO_GARAGE")
		STOP_AUDIO_SCENE("CAR_1_GET_TO_GARAGE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CAR_1_GARAGE_ARRIVAL")
		STOP_AUDIO_SCENE("CAR_1_GARAGE_ARRIVAL")
	ENDIF
	
	//Mix Groups
	IF DOES_ENTITY_EXIST(vehPlayer)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPlayer)
	ENDIF
	IF DOES_ENTITY_EXIST(vehRival1)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival1)
	ENDIF
	IF DOES_ENTITY_EXIST(vehRival2)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival2)
	ENDIF
	IF DOES_ENTITY_EXIST(vehPolice1)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPolice1)
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[3])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[3])
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[4])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[4])
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[5])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[5])
	ENDIF
	
	//Audio
	PLAY_AUDIO(CAR1_MISSION_FAIL)
	
	STOP_STREAM()
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	#IF IS_DEBUG_BUILD	IF bAutoSkipping	#ENDIF	//Z-skips cleanup
	//Text
	CLEAR_TEXT()
	#IF IS_DEBUG_BUILD	ENDIF	#ENDIF
	
	//Interface
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	//Cutscene State
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	CLEANUP_UBER_PLAYBACK()
	
	eCarStealSwitchCamState = SWITCH_CAM_IDLE
	
	bPassed = FALSE	//Reset the bool
	
	IF bRestart = FALSE
		SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CHOPSHOP, TRUE)
	ELSE
		SET_INTERIOR_CAPPED(INTERIOR_V_CHOPSHOP, TRUE)
	ENDIF
	
	IF bRestart = FALSE
		// url:bugstar:2106915
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iSlidingGarageDoor)
			REMOVE_DOOR_FROM_SYSTEM(iSlidingGarageDoor)
		ENDIF
	
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

//Mission Passed
PROC missionPassed()
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("MollyContact")
		enumPhonebookPresence ePhonebookPresence
		
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL
				ePhonebookPresence = MICHAEL_BOOK
			BREAK
			CASE CHAR_FRANKLIN
				ePhonebookPresence = FRANKLIN_BOOK
			BREAK
			CASE CHAR_TREVOR
				ePhonebookPresence = TREVOR_BOOK
			BREAK
		ENDSWITCH
		
		//Add Molly to the phonebook.
		ADD_CONTACT_TO_PHONEBOOK(CHAR_MOLLY, ePhonebookPresence, TRUE)
	ENDIF
	
	CLEAR_TEXT()
	
	Mission_Flow_Mission_Passed()
	
	bPassed = TRUE
	
	MISSION_CLEANUP()
ENDPROC

//Mission Failed
PROC missionFailed()
	CLEAR_TEXT()
	
	IF DOES_ENTITY_EXIST(pedRival1)
	AND NOT IS_PED_INJURED(pedRival1)
		IF GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_REACT_AND_FLEE_PED) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
			CLEAR_PED_TASKS(pedRival1)
			
			IF DOES_ENTITY_EXIST(vehRival1)
			AND NOT IS_ENTITY_DEAD(vehRival1)
			AND IS_PED_IN_VEHICLE(pedRival1, vehRival1)
				IF DOES_ENTITY_EXIST(vehPlayer)
				AND NOT IS_ENTITY_DEAD(vehPlayer)
					TASK_VEHICLE_MISSION(pedRival1, vehRival1, vehPlayer, MISSION_FLEE, 50.0, DRIVINGMODE_PLOUGHTHROUGH, 1.0, 1.0)
				ENDIF
			ELSE
				TASK_REACT_AND_FLEE_PED(pedRival1, PLAYER_PED_ID())
			ENDIF
			
			SET_PED_KEEP_TASK(pedRival1, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedRival2)
	AND NOT IS_PED_INJURED(pedRival2)
		IF GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_REACT_AND_FLEE_PED) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
			CLEAR_PED_TASKS(pedRival2)
			
			IF DOES_ENTITY_EXIST(vehRival2)
			AND NOT IS_ENTITY_DEAD(vehRival2)
			AND IS_PED_IN_VEHICLE(pedRival2, vehRival2)
				IF DOES_ENTITY_EXIST(vehPlayer)
				AND NOT IS_ENTITY_DEAD(vehPlayer)
					TASK_VEHICLE_MISSION(pedRival2, vehRival2, vehPlayer, MISSION_FLEE, 50.0, DRIVINGMODE_PLOUGHTHROUGH, 1.0, 1.0)
				ENDIF
			ELSE
				TASK_REACT_AND_FLEE_PED(pedRival2, PLAYER_PED_ID())
			ENDIF
			
			SET_PED_KEEP_TASK(pedRival2, TRUE)
		ENDIF
	ENDIF
	
	//Audio
	PLAY_AUDIO(CAR1_MISSION_FAIL)
	
	IF HAS_LABEL_BEEN_TRIGGERED("MollyContact")
		//Remove Molly from the phonebook
		REMOVE_CONTACT_FROM_ALL_PHONEBOOKS(CHAR_MOLLY)
	ENDIF
	
	// hang up phone instantly on fail - for 1643745
	IF eMissionObjective = cutSwitch
		KILL_ANY_CONVERSATION()
		HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	ENDIF
	
	SWITCH eMissionFail
		CASE failDefault
			MISSION_FLOW_SET_FAIL_REASON("S3_FAIL")
		BREAK
		CASE failPlayerDied
			MISSION_FLOW_SET_FAIL_REASON("S3_FAIL")
		BREAK
		CASE failMichaelDied
			MISSION_FLOW_SET_FAIL_REASON("CMN_MDIED")
		BREAK
		CASE failFranklinDied
			MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
		BREAK
		CASE failTrevorDied
			MISSION_FLOW_SET_FAIL_REASON("CMN_TDIED")
		BREAK
		CASE failTrevorShot
			MISSION_FLOW_SET_FAIL_REASON("S3_FSHOTTREVOR")
		BREAK
		CASE failDevinDied
			MISSION_FLOW_SET_FAIL_REASON("S3_FDEVINDIED")
		BREAK
		CASE failDevinShot
			MISSION_FLOW_SET_FAIL_REASON("S3_FSHOTDEVIN")
		BREAK
		CASE failDevinCarDestroyed
			MISSION_FLOW_SET_FAIL_REASON("S3_FDEVINCAR")
		BREAK
		CASE failDevinCarDamaged
			MISSION_FLOW_SET_FAIL_REASON("S3_FSHOTDEVCAR")
		BREAK
		CASE failRacerDied
			MISSION_FLOW_SET_FAIL_REASON("S3_FRACERDIED")
		BREAK
		CASE failCarDestroyed
			MISSION_FLOW_SET_FAIL_REASON("CMN_GENDEST")
		BREAK
		CASE failBikeDestroyed
			MISSION_FLOW_SET_FAIL_REASON("S3_FBIKEDEST")
		BREAK
		CASE failBikeDestroyedTrevor
			MISSION_FLOW_SET_FAIL_REASON("S3_FBIKEDESTT")
		BREAK
		CASE failBikeDamagedTrevor
			MISSION_FLOW_SET_FAIL_REASON("S3_FBIKEDAMT")
		BREAK
		CASE failLeftBehind
			MISSION_FLOW_SET_FAIL_REASON("S3_FLOSTRACE")
		BREAK
		CASE failAbandonedBike
			MISSION_FLOW_SET_FAIL_REASON("S3_FBIKEABAN")
		BREAK
		CASE failAbandonedCar
			MISSION_FLOW_SET_FAIL_REASON("S3_FCARABAN")
		BREAK
		CASE failRacerShot
			MISSION_FLOW_SET_FAIL_REASON("S3_FSHOTRACER")
		BREAK
		CASE failRacerSpooked
			MISSION_FLOW_SET_FAIL_REASON("S3_FSPKDRACER")
		BREAK
		CASE failRacerLeft
			MISSION_FLOW_SET_FAIL_REASON("S3_FRACERLEFT")
		BREAK
		CASE failDamagedCar
			MISSION_FLOW_SET_FAIL_REASON("S3_FDMGCAR")
		BREAK
		CASE failStuckCar
			MISSION_FLOW_SET_FAIL_REASON("S3_FSTUCKCAR")
		BREAK
		CASE failRaceDisrupted
			MISSION_FLOW_SET_FAIL_REASON("S3_FDISRACE")
		BREAK
	ENDSWITCH
	
	Mission_Flow_Mission_Failed()    

	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread 
ENDPROC

PROC DEATH_CHECKS()
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		eMissionFail = failDefault
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		eMissionFail = failMichaelDied
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		eMissionFail = failTrevorDied
		
		missionFailed()
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF eMissionObjective = stageChase
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), TRUE)
				eMissionFail = failTrevorShot
				
				missionFailed()
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehPolice1)
				IF NOT IS_ENTITY_DEAD(vehPolice1)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
						IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
							eMissionFail = failTrevorShot
							
							missionFailed()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		eMissionFail = failFranklinDied
		
		missionFailed()
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedDevin)
	AND NOT IS_PED_INJURED(pedDevin)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDevin, PLAYER_PED_ID())
			eMissionFail = failDevinShot
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(pedDevin)
		eMissionFail = failDevinDied
		
		missionFailed()
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehDevin)
	AND NOT IS_ENTITY_DEAD(vehDevin)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehDevin, PLAYER_PED_ID())
			eMissionFail = failDevinCarDamaged
			
			missionFailed()
		ENDIF
	ENDIF
	
	SAFE_DEATH_CHECK_PED(pedMolly)
	SAFE_DEATH_CHECK_PED(pedWorker1)
	SAFE_DEATH_CHECK_PED(pedWorker2)
	
	IF SAFE_DEATH_CHECK_VEHICLE(vehDevin)
		eMissionFail = failDevinCarDestroyed
		
		missionFailed()
	ENDIF
	
	IF eMissionObjective <= cutBridge
		IF SAFE_DEATH_CHECK_PED(pedRival1)
		OR SAFE_DEATH_CHECK_PED(pedRival2)
			#IF IS_DEBUG_BUILD
			VECTOR vDebugVector
			vDebugVector = GET_ENTITY_COORDS(pedRival1, FALSE)
			PRINTLN("Racer 1 Injured[", IS_ENTITY_DEAD(pedRival1), "] at coords <<", vDebugVector.X, ", ", vDebugVector.Y, ", ", vDebugVector.Z, ">>")
			vDebugVector = GET_ENTITY_COORDS(pedRival2, FALSE)
			PRINTLN("Racer 2 Injured[", IS_ENTITY_DEAD(pedRival2), "] at coords <<", vDebugVector.X, ", ", vDebugVector.Y, ", ", vDebugVector.Z, ">>")
			#ENDIF
			
			eMissionFail = failRacerDied
			
			missionFailed()
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRival1)
		AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRival1, PLAYER_PED_ID(), TRUE)
			eMissionFail = failRacerShot
			
			missionFailed()
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRival1)
		AND (IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedRival1), 10.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedRival1), 20.0)
		OR IS_PED_BEING_JACKED(pedRival1)
		OR DOES_ENTITY_EXIST(vehRival1)
		AND (IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
		AND GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vehRival1)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRival1, PLAYER_PED_ID(), TRUE)
		OR (DOES_ENTITY_EXIST(vehRival1) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehRival1, PLAYER_PED_ID(), FALSE))
		OR (IS_PED_SHOOTING(PLAYER_PED_ID())
		AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedRival1)) < 25.0))
			eMissionFail = failRacerSpooked
			
			missionFailed()
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRival2)
		AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRival2, PLAYER_PED_ID(), TRUE)
			eMissionFail = failRacerShot
			
			missionFailed()
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRival2)
		AND (IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedRival2), 10.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedRival2), 20.0)
		OR IS_PED_BEING_JACKED(pedRival2)
		OR DOES_ENTITY_EXIST(vehRival2)
		AND (IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
		AND GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vehRival2)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRival2, PLAYER_PED_ID(), TRUE)
		OR (DOES_ENTITY_EXIST(vehRival2) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehRival2, PLAYER_PED_ID(), FALSE))
		OR (IS_PED_SHOOTING(PLAYER_PED_ID())
		AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedRival2)) < 25.0))
			eMissionFail = failRacerSpooked
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_VEHICLE(vehPlayer)
		IF IS_VEHICLE_PERMANENTLY_STUCK(vehPlayer)
			eMissionFail = failStuckCar
		ELSE
			eMissionFail = failCarDestroyed
		ENDIF
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_VEHICLE(vehRival1)
		IF IS_VEHICLE_PERMANENTLY_STUCK(vehRival1)
			eMissionFail = failStuckCar
		ELSE
			eMissionFail = failCarDestroyed
		ENDIF
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_VEHICLE(vehRival2)
		IF IS_VEHICLE_PERMANENTLY_STUCK(vehRival2)
			eMissionFail = failStuckCar
		ELSE
			eMissionFail = failCarDestroyed
		ENDIF
		
		missionFailed()
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlayer)
	AND ((HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehPlayer, PLAYER_PED_ID(), TRUE)
	AND GET_ENTITY_HEALTH(vehPlayer) < 700)
	OR (IS_VEHICLE_TYRE_BURST(vehPlayer, SC_WHEEL_CAR_FRONT_LEFT)
	OR IS_VEHICLE_TYRE_BURST(vehPlayer, SC_WHEEL_CAR_FRONT_RIGHT)
	OR IS_VEHICLE_TYRE_BURST(vehPlayer, SC_WHEEL_CAR_REAR_LEFT)
	OR IS_VEHICLE_TYRE_BURST(vehPlayer, SC_WHEEL_CAR_REAR_RIGHT)))
		eMissionFail = failDamagedCar
		
		missionFailed()
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehRival1)
	AND ((HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehRival1, PLAYER_PED_ID(), TRUE)
	AND GET_ENTITY_HEALTH(vehRival1) < 700)
	OR (IS_VEHICLE_TYRE_BURST(vehRival1, SC_WHEEL_CAR_FRONT_LEFT)
	OR IS_VEHICLE_TYRE_BURST(vehRival1, SC_WHEEL_CAR_FRONT_RIGHT)
	OR IS_VEHICLE_TYRE_BURST(vehRival1, SC_WHEEL_CAR_REAR_LEFT)
	OR IS_VEHICLE_TYRE_BURST(vehRival1, SC_WHEEL_CAR_REAR_RIGHT)))
		eMissionFail = failDamagedCar
		
		missionFailed()
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehRival2)
	AND ((HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehRival2, PLAYER_PED_ID(), TRUE)
	AND GET_ENTITY_HEALTH(vehRival2) < 700)
	OR (IS_VEHICLE_TYRE_BURST(vehRival2, SC_WHEEL_CAR_FRONT_LEFT)
	OR IS_VEHICLE_TYRE_BURST(vehRival2, SC_WHEEL_CAR_FRONT_RIGHT)
	OR IS_VEHICLE_TYRE_BURST(vehRival2, SC_WHEEL_CAR_REAR_LEFT)
	OR IS_VEHICLE_TYRE_BURST(vehRival2, SC_WHEEL_CAR_REAR_RIGHT)))
		eMissionFail = failDamagedCar
		
		missionFailed()
	ENDIF
	
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	
//	IF DOES_ENTITY_EXIST(vehRival1)
//		IF NOT IS_ENTITY_DEAD(vehRival1)
//			DRAW_DEBUG_LINE_WITH_TWO_COLOURS(GET_ENTITY_COORDS(vehRival1), GET_ENTITY_COORDS(vehRival1) + <<0.0, 0.0, 2.0>>, 0, 0, 255, 255, 0, 0, 0, 0)
//			TEXT_LABEL txtLabel
//			txtLabel = "vehRival1"
//			DRAW_DEBUG_TEXT(txtLabel, GET_ENTITY_COORDS(vehRival1) + <<0.0, 0.0, 2.5>>)
//			TEXT_LABEL txtLabel2
//			txtLabel2 = GET_ENTITY_HEALTH(vehRival1)
//			txtLabel2 += "/"
//			txtLabel2 += ROUND(GET_VEHICLE_ENGINE_HEALTH(vehRival1))
//			txtLabel2 += "/"
//			txtLabel2 += ROUND(GET_VEHICLE_PETROL_TANK_HEALTH(vehRival1))
//			DRAW_DEBUG_TEXT(txtLabel2, GET_ENTITY_COORDS(vehRival1) + <<0.0, 0.0, 2.25>>)
//		ENDIF
//	ENDIF
//	
//	IF DOES_ENTITY_EXIST(vehRival2)
//		IF NOT IS_ENTITY_DEAD(vehRival2)
//			DRAW_DEBUG_LINE_WITH_TWO_COLOURS(GET_ENTITY_COORDS(vehRival2), GET_ENTITY_COORDS(vehRival2) + <<0.0, 0.0, 2.0>>, 0, 0, 255, 255, 0, 0, 0, 0)
//			TEXT_LABEL txtLabel
//			txtLabel = "vehRival2"
//			DRAW_DEBUG_TEXT(txtLabel, GET_ENTITY_COORDS(vehRival2) + <<0.0, 0.0, 2.5>>)
//			TEXT_LABEL txtLabel2
//			txtLabel2 = GET_ENTITY_HEALTH(vehRival2)
//			txtLabel2 += "/"
//			txtLabel2 += ROUND(GET_VEHICLE_ENGINE_HEALTH(vehRival2))
//			txtLabel2 += "/"
//			txtLabel2 += ROUND(GET_VEHICLE_PETROL_TANK_HEALTH(vehRival2))
//			DRAW_DEBUG_TEXT(txtLabel2, GET_ENTITY_COORDS(vehRival2) + <<0.0, 0.0, 2.25>>)
//		ENDIF
//	ENDIF
	
	IF SAFE_DEATH_CHECK_VEHICLE(vehPolice1)
		IF eMissionObjective < stageDriveHome
			eMissionFail = failBikeDestroyedTrevor
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPolice1)
	AND NOT IS_ENTITY_DEAD(vehPolice1)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehPolice1, PLAYER_PED_ID(), FALSE)
			eMissionFail = failBikeDamagedTrevor
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_VEHICLE(vehPolice2)
		IF eMissionObjective < stageDriveHome
			eMissionFail = failBikeDestroyed
			
			missionFailed()
		ENDIF
	ENDIF
	
	//Left Behind
	IF eMissionObjective = stageArrive
		IF IS_SCREEN_FADED_IN()
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayer)) >= 150
				eMissionFail = failAbandonedCar
				
				missionFailed()
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionObjective = stageRace
		IF DOES_ENTITY_EXIST(vehRival1)
		AND DOES_ENTITY_EXIST(vehRival2)
			IF IS_SCREEN_FADED_IN()
				IF (((IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				AND GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK)
				AND GET_TIME_POSITION_IN_RECORDING(vehRival1) < 47936.000)
				AND ((IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
				AND GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK)
				AND GET_TIME_POSITION_IN_RECORDING(vehRival2) < 70000.000)
				AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) >= 300
				OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) >= 300))
					eMissionFail = failLeftBehind
					
					missionFailed()
				ENDIF
				
				IF (GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) >= 600)
				OR (GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) >= 600)
					eMissionFail = failLeftBehind
					
					missionFailed()
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayer)) >= 75
					eMissionFail = failAbandonedCar
					
					missionFailed()
				ENDIF
				
				UPDATE_CHASE_BLIP(blipRival1, vehRival1, 250.0)
				UPDATE_CHASE_BLIP(blipRival2, vehRival2, 250.0)
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionObjective = stageChase
		IF DOES_ENTITY_EXIST(vehPlayer)
		AND DOES_ENTITY_EXIST(vehRival1)
		AND DOES_ENTITY_EXIST(vehRival2)
		AND DOES_ENTITY_EXIST(vehPolice1)
		AND DOES_ENTITY_EXIST(vehPolice2)
			IF IS_SCREEN_FADED_IN()
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
				AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1))
				OR HAS_LABEL_BEEN_TRIGGERED("S3_STOP")
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) >= 600
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) >= 600
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPolice1)) >= 300
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPolice2)) >= 300
						eMissionFail = failLeftBehind
						
						missionFailed()
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehPolice2)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPolice2)) >= 75
							eMissionFail = failAbandonedBike
							
							missionFailed()
						ENDIF
					ENDIF
				ENDIF
				
				UPDATE_CHASE_BLIP(blipPlayer, vehPlayer, 500.0)
				UPDATE_CHASE_BLIP(blipRival1, vehRival1, 500.0)
				UPDATE_CHASE_BLIP(blipRival2, vehRival2, 500.0)
				UPDATE_CHASE_BLIP(blipPolice, vehPolice1, 250.0)
				UPDATE_CHASE_BLIP(blipPolice2, vehPolice2, 250.0)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC WAIT_WITH_DEATH_CHECKS(INT iWait = 0)
	INT iGameTime = GET_GAME_TIMER() + iWait	
	
	WHILE GET_GAME_TIMER() <= iGameTime
		WAIT(0)
		
		//Video Recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_IFoughtTheLaw")
		
		DEATH_CHECKS()
	ENDWHILE
ENDPROC

PROC LOAD_SCENE_ADV(VECTOR vCoords, FLOAT fRadius = 20.0)
	NEW_LOAD_SCENE_START_SPHERE(vCoords, fRadius)
	
	INT iTimeOut = GET_GAME_TIMER() + 20000
	
	WHILE IS_NEW_LOAD_SCENE_ACTIVE()
	AND NOT IS_NEW_LOAD_SCENE_LOADED()
	AND GET_GAME_TIMER() < iTimeOut
		#IF IS_DEBUG_BUILD
		PRINTLN("NEW_LOAD_SCENE_START_SPHERE(", vCoords.X, ", ", vCoords.Y, ", ", vCoords.Z, ", ", fRadius, ")...")
		#ENDIF
		
		PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iTimeOut)
		
		WAIT_WITH_DEATH_CHECKS(0)
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	IF GET_GAME_TIMER() > iTimeOut
		SCRIPT_ASSERT("NEW_LOAD_SCENE_START_SPHERE timed out, see log for details.")
	ENDIF
	#ENDIF
	
	NEW_LOAD_SCENE_STOP()
ENDPROC

PROC CREATE_CONVERSATION_ADV(STRING sLabel, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE, enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)
		WHILE NOT CREATE_CONVERSATION(sPedsForConversation, sConversationBlock, sLabel, eConvPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(" CONVERSATION")
			PRINTNL()
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
	ENDIF
ENDPROC

PROC CREATE_CONVERSATION_FROM_SPECIFIC_LINE_ADV(STRING sLabel, STRING sLabelSub, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabelSub)
		WHILE NOT CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sPedsForConversation, sConversationBlock, sLabel, sLabelSub, eConvPriority)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(" CONVERSATION")
			PRINTNL()
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
	ENDIF
ENDPROC

PROC PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(STRING sLabel, STRING sLabelSub, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabelSub)
		WHILE NOT PLAY_SINGLE_LINE_FROM_CONVERSATION(sPedsForConversation, sConversationBlock, sLabel, sLabelSub, eConvPriority)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT_WITH_DEATH_CHECKS(0)
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(", ") PRINTSTRING(sLabelSub) PRINTSTRING(" CONVERSATION")
			PRINTNL()
		ENDWHILE
		SET_LABEL_AS_TRIGGERED(sLabelSub, bOnce)
	ENDIF
ENDPROC

//Mid Mission Replay
ENUM MissionReplay
	replayStart,
//	replayDriveTo,
	replayGarage,
	replayChase,
	replayDriveHome,
	replayPass
ENDENUM

PROC MISSION_REPLAY(MissionReplay eMissionReplay = replayStart)
	//-	Replay Specific Position
	VECTOR vStartReplay
	FLOAT fStartReplay
	
	SWITCH eMissionReplay
		CASE replayStart
			vStartReplay = vPlayerStart
			fStartReplay = fPlayerStart
		BREAK
		CASE replayGarage
			vStartReplay = <<2570.5767, 364.8711, 107.4569>>
			fStartReplay = 357.8433
		BREAK
		CASE replayChase
			vStartReplay = <<2686.8792, 5131.9077, 43.8515>>
			fStartReplay = 151.6033
		BREAK
		CASE replayDriveHome
			vStartReplay = <<-1923.0222, 4605.4956, 56.0558>>
			fStartReplay = 135.0792
		BREAK
		CASE replayPass
			vStartReplay = <<492.7253, -1311.5626, 28.2627>>
			fStartReplay = 293.8439
		BREAK
	ENDSWITCH
	
	START_REPLAY_SETUP(vStartReplay, fStartReplay, FALSE)
	
	END_REPLAY_SETUP(NULL, VS_DRIVER, FALSE)
	
	SWITCH eMissionReplay
		CASE replayStart
			CLEAR_AREA(vPlayerStart, 100.0, TRUE)
			SET_PED_POSITION(PLAYER_PED_ID(), vPlayerStart, fPlayerStart)			
			
			//Load Scene
			//LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			
			eMissionObjective = stageArrive
		BREAK
//		CASE replayDriveTo
//			SET_PED_POSITION(PLAYER_PED_ID(), <<1801.1298, 1915.6359, 77.7007>>, 348.5779)
//			SET_VEHICLE_FORWARD_SPEED(vehPlayer, 20.0)
//			SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
//			
//			eMissionObjective = stageArrive
//		BREAK
		CASE replayGarage
			//Outfits/Weapons
			SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
			
			EMPTY_VEHICLE_OF_PEDS(vehPlayer)
			
			SAFE_DELETE_VEHICLE(vehPlayer)
			
			IF GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = F620
				CREATE_VEHICLE_FOR_REPLAY(vehPlayer, <<2570.5767, 364.8711, 107.4569>>, 357.8433, TRUE, TRUE, FALSE, FALSE, TRUE, F620, 0)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayer, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
			ELSE
				REQUEST_MODEL(F620)
				
				WHILE NOT HAS_MODEL_LOADED(F620)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SPAWN_VEHICLE(vehPlayer, F620, <<2570.5767, 364.8711, 107.4569>>, 357.8433, 0)
				SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
				SET_VEHICLE_COLOURS(vehPlayer, 43, 43)
				SET_VEHICLE_EXTRA_COLOURS(vehPlayer, 48, 48)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(F620)
			ENDIF
			
			//vehRival1 - ENTITYXF
			REQUEST_MODEL(ENTITYXF)
			
			WHILE NOT HAS_MODEL_LOADED(ENTITYXF)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			vehRival1 = CREATE_CAR_STEAL_STRAND_CAR(ENTITYXF, vRival1Start, fRival1Start)	//SPAWN_VEHICLE(vehRival1, ENTITYXF, vRival1Start, fRival1Start, CARSTEAL_COLOURS_ENTITYXF)
			SET_VEHICLE_AS_RESTRICTED(vehRival1, 0)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival1, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival1, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehRival1, VEHICLELOCK_LOCKED)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
			
			//pedRival1 - IG_CAR3GUY1
			REQUEST_MODEL(IG_CAR3GUY1)
			
			WHILE NOT HAS_MODEL_LOADED(IG_CAR3GUY1)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PED(pedRival1, IG_CAR3GUY1, GET_ENTITY_COORDS(vehRival1))
			SET_PED_INTO_VEHICLE(pedRival1, vehRival1, VS_DRIVER)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedRival1, relGroupEnemy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival1, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY1)
			
			//vehRival2 - CHEETAH
			REQUEST_MODEL(CHEETAH)
			
			WHILE NOT HAS_MODEL_LOADED(CHEETAH)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			vehRival2 = CREATE_CAR_STEAL_STRAND_CAR(CHEETAH, vRival2Start, fRival2Start)	//SPAWN_VEHICLE(vehRival2, CHEETAH, vRival2Start, fRival2Start, CARSTEAL_COLOURS_CHEETAH)
			SET_VEHICLE_AS_RESTRICTED(vehRival2, 1)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival2, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival2, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehRival2, VEHICLELOCK_LOCKED)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
			
			//pedRival2 - IG_CAR3GUY2
			REQUEST_MODEL(IG_CAR3GUY2)
			
			WHILE NOT HAS_MODEL_LOADED(IG_CAR3GUY2)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PED(pedRival2, IG_CAR3GUY2, GET_ENTITY_COORDS(vehRival2))
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_HEAD, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_LEG, 0, 0)
			SET_PED_PROP_INDEX(pedRival2, ANCHOR_EYES, 0)
			SET_PED_INTO_VEHICLE(pedRival2, vehRival2, VS_DRIVER)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedRival2, relGroupEnemy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival2, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY2)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedRival1, "CST3RACER1")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedRival2, "CST3RACER2")
			
			IF g_bShitskipAccepted = TRUE
				eMissionObjective = cutRace
			ELSE
				eMissionObjective = stageRace
			ENDIF
		BREAK
		CASE replayChase
			//Outfits/Weapons
			SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
			
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EARS)
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
			SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), -1)
			
			EMPTY_VEHICLE_OF_PEDS(vehPlayer)
			
			SAFE_DELETE_VEHICLE(vehPlayer)
			
			IF GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = F620
				CREATE_VEHICLE_FOR_REPLAY(vehPlayer, <<2716.9924, 4787.3481, 43.5334>>, 11.8873, TRUE, TRUE, FALSE, FALSE, TRUE, F620, 0)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayer, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
			ELSE
				REQUEST_MODEL(F620)
				
				WHILE NOT HAS_MODEL_LOADED(F620)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SPAWN_VEHICLE(vehPlayer, F620, <<2716.9924, 4787.3481, 43.5334>>, 11.8873, 0)
				SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
				SET_VEHICLE_COLOURS(vehPlayer, 43, 43)
				SET_VEHICLE_EXTRA_COLOURS(vehPlayer, 48, 48)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(F620)
			ENDIF
			
			//vehRival1 - ENTITYXF
			REQUEST_MODEL(ENTITYXF)
			
			WHILE NOT HAS_MODEL_LOADED(ENTITYXF)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			vehRival1 = CREATE_CAR_STEAL_STRAND_CAR(ENTITYXF, vRival1Start, fRival1Start)	//SPAWN_VEHICLE(vehRival1, ENTITYXF, vRival1Start, fRival1Start, CARSTEAL_COLOURS_ENTITYXF)
			SET_VEHICLE_AS_RESTRICTED(vehRival1, 0)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival1, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival1, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehRival1, VEHICLELOCK_LOCKED)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
			
			//pedRival1 - IG_CAR3GUY1
			REQUEST_MODEL(IG_CAR3GUY1)
			
			WHILE NOT HAS_MODEL_LOADED(IG_CAR3GUY1)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PED(pedRival1, IG_CAR3GUY1, GET_ENTITY_COORDS(vehRival1))
			SET_PED_INTO_VEHICLE(pedRival1, vehRival1, VS_DRIVER)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedRival1, relGroupEnemy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival1, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY1)
			
			//vehRival2 - CHEETAH
			REQUEST_MODEL(CHEETAH)
			
			WHILE NOT HAS_MODEL_LOADED(CHEETAH)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			vehRival2 = CREATE_CAR_STEAL_STRAND_CAR(CHEETAH, vRival2Start, fRival2Start)	//SPAWN_VEHICLE(vehRival2, CHEETAH, vRival2Start, fRival2Start, CARSTEAL_COLOURS_CHEETAH)
			SET_VEHICLE_AS_RESTRICTED(vehRival2, 1)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival2, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival2, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehRival2, VEHICLELOCK_LOCKED)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
			
			//pedRival2 - IG_CAR3GUY2
			REQUEST_MODEL(IG_CAR3GUY2)
			
			WHILE NOT HAS_MODEL_LOADED(IG_CAR3GUY2)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PED(pedRival2, IG_CAR3GUY2, GET_ENTITY_COORDS(vehRival2))
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_HEAD, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_LEG, 0, 0)
			SET_PED_PROP_INDEX(pedRival2, ANCHOR_EYES, 0)
			SET_PED_INTO_VEHICLE(pedRival2, vehRival2, VS_DRIVER)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedRival2, relGroupEnemy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival2, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY2)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedRival1, "CST3RACER1")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedRival2, "CST3RACER2")
			
			//vehPolice1 - POLICEB
			REQUEST_MODEL(POLICEB)
			
			WHILE NOT HAS_MODEL_LOADED(POLICEB)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SAFE_DELETE_VEHICLE(vehPolice1)
			SPAWN_VEHICLE(vehPolice1, POLICEB, vPoliceStart1, fPoliceStart1)
			SET_VEHICLE_AS_RESTRICTED(vehPolice1, 2)
			SET_VEHICLE_HAS_STRONG_AXLES(vehPolice1, TRUE)
			
			//sSelectorPeds.pedID[SELECTOR_PED_TREVOR] - GET_PLAYER_PED_MODEL(CHAR_TREVOR)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorStartPos, fTrevorStartHeading)
			//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD, 5)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAIR, 3, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 3, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 3, 0)
			IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
				SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
			ENDIF
			WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL, FALSE)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)	//Set that the player has changed clothes on mission
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_WillFlyThroughWindscreen, FALSE)
			
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			
			//vehPolice2 - POLICEB
			SAFE_DELETE_VEHICLE(vehPolice2)
			SPAWN_VEHICLE(vehPolice2, POLICEB, vPoliceStart2, fPoliceStart2)
			SET_VEHICLE_AS_RESTRICTED(vehPolice2, 3)
			SET_VEHICLE_HAS_STRONG_AXLES(vehPolice2, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(POLICEB)
			
			//sSelectorPeds.pedID[SELECTOR_PED_TREVOR] - GET_PLAYER_PED_MODEL(CHAR_TREVOR)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStartPos, fMichaelStartHeading)
			//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ANCHOR_HEAD, 4)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 6, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_LEG, 6, 0)
			WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)	//Set that the player has changed clothes on mission
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_WillFlyThroughWindscreen, FALSE)
			
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPolice2)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPolice2)
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			
			IF g_bShitskipAccepted = TRUE
				eMissionObjective = cutSwitch
			ELSE
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				
				WHILE NOT TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				eMissionObjective = stageChase
			ENDIF
		BREAK
		CASE replayDriveHome
			//Outfits/Weapons
			SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
			
			EMPTY_VEHICLE_OF_PEDS(vehPlayer)
			
			SAFE_DELETE_VEHICLE(vehPlayer)
			
			IF GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = F620
				CREATE_VEHICLE_FOR_REPLAY(vehPlayer, <<-1912.547, 4616.3615, 56.0427>>, 134.6697, TRUE, TRUE, FALSE, FALSE, TRUE, F620, 0)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayer, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
			ELSE
				REQUEST_MODEL(F620)
				
				WHILE NOT HAS_MODEL_LOADED(F620)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SPAWN_VEHICLE(vehPlayer, F620, <<-1912.547, 4616.3615, 56.0427>>, 134.6697, 0)
				SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
				SET_VEHICLE_COLOURS(vehPlayer, 43, 43)
				SET_VEHICLE_EXTRA_COLOURS(vehPlayer, 48, 48)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(F620)
			ENDIF
			
			//Request Models
			REQUEST_MODEL(POLICEB)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			REQUEST_MODEL(ENTITYXF)
			REQUEST_MODEL(IG_CAR3GUY1)
			REQUEST_MODEL(CHEETAH)
			REQUEST_MODEL(IG_CAR3GUY2)
			
			//vehPolice1 - POLICEB
			WHILE NOT HAS_MODEL_LOADED(POLICEB)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_VEHICLE(vehPolice1, POLICEB, vPoliceStart1, fPoliceStart1)
			SET_VEHICLE_AS_RESTRICTED(vehPolice1, 2)
			SET_VEHICLE_HAS_STRONG_AXLES(vehPolice1, TRUE)
			
			//sSelectorPeds.pedID[SELECTOR_PED_TREVOR] - GET_PLAYER_PED_MODEL(CHAR_TREVOR)
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorStartPos, fTrevorStartHeading)
			//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD, 5)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAIR, 3, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 3, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 3, 0)
			IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
				SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
			ENDIF
			WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL, FALSE)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)	//Set that the player has changed clothes on mission
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_WillFlyThroughWindscreen, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			
			//vehPolice2 - POLICEB
			SPAWN_VEHICLE(vehPolice2, POLICEB, vPoliceStart2, fPoliceStart2)
			SET_VEHICLE_AS_RESTRICTED(vehPolice2, 3)
			SET_VEHICLE_HAS_STRONG_AXLES(vehPolice2, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(POLICEB)
			
			//sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] - GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<-1921.7319, 4604.9102, 56.0562>> + <<0.36, -0.37, -0.0>>, 135.0260)
			
			CLEAR_AREA(<<-1921.7319, 4604.9102, 56.0562>>, 200.0, TRUE)
			
			//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ANCHOR_HEAD, 4)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 6, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_LEG, 6, 0)
			WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)	//Set that the player has changed clothes on mission
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_WillFlyThroughWindscreen, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			
			//vehRival1 - ENTITYXF
			WHILE NOT HAS_MODEL_LOADED(ENTITYXF)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			vehRival1 = CREATE_CAR_STEAL_STRAND_CAR(ENTITYXF, <<-1923.0222, 4605.4929, 56.0440>>, 135.0786)	//SPAWN_VEHICLE(vehRival1, ENTITYXF, vRival1Start, fRival1Start, CARSTEAL_COLOURS_ENTITYXF)
			SET_VEHICLE_AS_RESTRICTED(vehRival1, 0)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival1, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival1, TRUE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
			
			//pedRival1 - IG_CAR3GUY1
			WHILE NOT HAS_MODEL_LOADED(IG_CAR3GUY1)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PED(pedRival1, IG_CAR3GUY1, <<-1915.625732, 4600.510254, 56.0301>>, 141.4507)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(pedRival1, relGroupEnemy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival1, TRUE)
			
			TASK_SMART_FLEE_PED(pedRival1, PLAYER_PED_ID(), 500.0, -1)
			FORCE_PED_MOTION_STATE(pedRival1, MS_ON_FOOT_RUN)
			SET_PED_KEEP_TASK(pedRival1, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY1)
			
			//vehRival2 - CHEETAH
			WHILE NOT HAS_MODEL_LOADED(CHEETAH)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			vehRival2 = CREATE_CAR_STEAL_STRAND_CAR(CHEETAH, <<-1919.3284, 4609.285, 56.0532>>, 135.0284)	//SPAWN_VEHICLE(vehRival2, CHEETAH, vRival2Start, fRival2Start, CARSTEAL_COLOURS_CHEETAH)
			SET_VEHICLE_AS_RESTRICTED(vehRival2, 1)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival2, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival2, TRUE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
			
			//pedRival2 - IG_CAR3GUY2
			WHILE NOT HAS_MODEL_LOADED(IG_CAR3GUY2)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PED(pedRival2, IG_CAR3GUY2, <<-1921.257690, 4590.634766, 56.0301>>, 141.4507)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_HEAD, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_LEG, 0, 0)
			SET_PED_PROP_INDEX(pedRival2, ANCHOR_EYES, 0)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(pedRival2, relGroupEnemy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival2, TRUE)
			
			TASK_SMART_FLEE_PED(pedRival2, PLAYER_PED_ID(), 500.0, -1)
			FORCE_PED_MOTION_STATE(pedRival2, MS_ON_FOOT_RUN)
			SET_PED_KEEP_TASK(pedRival2, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY2)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedRival1, "CST3RACER1")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedRival2, "CST3RACER2")
			
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			
			WHILE NOT TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			IF g_bShitskipAccepted = TRUE
				eMissionObjective = cutBridge
			ELSE
				eMissionObjective = stageDriveHome
			ENDIF
		BREAK
		CASE replayPass
			//Outfits/Weapons
			SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
			
			EMPTY_VEHICLE_OF_PEDS(vehPlayer)
			
			SAFE_DELETE_VEHICLE(vehPlayer)
			
			IF GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() = F620
				CREATE_VEHICLE_FOR_REPLAY(vehPlayer, <<485.3990, -1332.9005, 28.3095>>, 292.4873, TRUE, TRUE, FALSE, FALSE, TRUE, F620, 0)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayer, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
			ELSE
				REQUEST_MODEL(F620)
				
				WHILE NOT HAS_MODEL_LOADED(F620)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SPAWN_VEHICLE(vehPlayer, F620, <<485.3990, -1332.9005, 28.3095>>, 292.4873, 0)
				SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
				SET_VEHICLE_COLOURS(vehPlayer, 43, 43)
				SET_VEHICLE_EXTRA_COLOURS(vehPlayer, 48, 48)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(F620)
			ENDIF
			
			//sSelectorPeds.pedID[SELECTOR_PED_TREVOR] - GET_PLAYER_PED_MODEL(CHAR_TREVOR)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<485.5920, -1330.7214, 28.2454>>, 296.1122)
			//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD, 5)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAIR, 3, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 3, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 3, 0)
			IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
				SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
			ENDIF
			WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL, FALSE)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)	//Set that the player has changed clothes on mission
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_WillFlyThroughWindscreen, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			
			//sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] - GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<485.9252, -1330.5171, 28.2488>>, 293.9519)
			//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ANCHOR_HEAD, 4)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 6, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_LEG, 6, 0)
			WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)	//Set that the player has changed clothes on mission
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_WillFlyThroughWindscreen, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			
			//vehRival1 - ENTITYXF
			REQUEST_MODEL(ENTITYXF)
			
			WHILE NOT HAS_MODEL_LOADED(ENTITYXF)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			vehRival1 = CREATE_CAR_STEAL_STRAND_CAR(ENTITYXF, <<485.9952, -1335.4487, 28.2862>>, 286.6455)
			SET_VEHICLE_AS_RESTRICTED(vehRival1, 0)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival1, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival1, TRUE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
			
			//vehRival2 - CHEETAH
			REQUEST_MODEL(CHEETAH)
			
			WHILE NOT HAS_MODEL_LOADED(CHEETAH)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			vehRival2 = CREATE_CAR_STEAL_STRAND_CAR(CHEETAH, <<487.1696, -1337.8451, 28.2688>>, 293.0351)
			SET_VEHICLE_AS_RESTRICTED(vehRival2, 1)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival2, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival2, TRUE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
			
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			
			WHILE NOT TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			SET_PED_POSITION(PLAYER_PED_ID(), <<492.7253, -1311.5626, 28.2627>>, 293.8439, FALSE)
			
			//LOAD_SCENE_ADV(<<492.7253, -1311.5626, 28.2627>>)
			
			eMissionObjective = cutHome
		BREAK
	ENDSWITCH
	
	bSkipped = TRUE
	bReplaySkip = TRUE
	
//	IF IS_SCREEN_FADED_OUT()
//		DO_SCREEN_FADE_IN(1000)
//	ENDIF
ENDPROC

PROC LOAD_UNLOAD_ASSETS()
	//Stages Reference
		//stageArrive
		//cutRace
		//stageRace
		//cutSwitch
		//stageChase
		//cutBridge
		//stageDriveHome
		//passMission
		//failMission
	
	//Mission Peds:
		//GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
		IF eMissionObjective = cutSwitch
			HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
		ENDIF
		
		//GET_PLAYER_PED_MODEL(CHAR_FRANKLIN)
		IF eMissionObjective = initMission
			HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
		ENDIF
		
		//GET_PLAYER_PED_MODEL(CHAR_TREVOR)
		IF eMissionObjective = cutSwitch
			HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		ENDIF
		
		//IG_CAR3GUY2
		IF eMissionObjective = cutRace
			HAS_MODEL_LOADED_CHECK(IG_CAR3GUY2)
		ENDIF
		
		//IG_CAR3GUY1
		IF eMissionObjective = cutRace
			HAS_MODEL_LOADED_CHECK(IG_CAR3GUY1)
		ENDIF
	
	//Mission Cars:
		//F620
		IF eMissionObjective = initMission
			HAS_MODEL_LOADED_CHECK(F620)
		ENDIF
		
		//CHEETAH
		IF eMissionObjective = cutRace
			HAS_MODEL_LOADED_CHECK(CHEETAH)
		ENDIF
		
		//ENTITYXF
		IF eMissionObjective = cutRace
			HAS_MODEL_LOADED_CHECK(ENTITYXF)
		ENDIF
		
		//ASTEROPE
		IF eMissionObjective = stageRace
			HAS_MODEL_LOADED_CHECK(ASTEROPE)
		ENDIF
		
		//TRAILERS2
		IF eMissionObjective = stageRace
			HAS_MODEL_LOADED_CHECK(TRAILERS)
			HAS_MODEL_LOADED_CHECK(TRAILERS2)
		ENDIF
		
		//POLICEB
		IF eMissionObjective = cutSwitch
			HAS_MODEL_LOADED_CHECK(POLICEB)
		ENDIF
		
		//PROP_PLAYER_PHONE_01
		IF eMissionObjective >= stageRace AND eMissionObjective <= cutSwitch
			HAS_MODEL_LOADED_CHECK(PROP_PLAYER_PHONE_01)
		ENDIF
		
		//PROP_DONUT_02
		IF eMissionObjective >= stageRace AND eMissionObjective <= cutSwitch
			HAS_MODEL_LOADED_CHECK(PROP_DONUT_02)
		ENDIF
		
		//PROP_DONUT_02B
		IF eMissionObjective >= stageRace AND eMissionObjective <= cutSwitch
			HAS_MODEL_LOADED_CHECK(PROP_DONUT_02B)
		ENDIF
	
	//Vehicle Recordings
		
		IF eMissionObjective = stageArrive
			//Arrives normal direction
			HAS_RECORDING_LOADED_CHECK(001, sCarrecGasStation)
			
			//Arrives wrong direction
			HAS_RECORDING_LOADED_CHECK(002, sCarrecGasStation)
		ELSE
			UNLOAD_RECORDING(001, sCarrecGasStation)
			UNLOAD_RECORDING(002, sCarrecGasStation)
		ENDIF
		
		IF eMissionObjective >= cutRace AND eMissionObjective <= stageRace
			//Rival 1
			HAS_RECORDING_LOADED_CHECK(500, sCarrecSetup)	//sCarrecRace)
			
			//Rival 2
			HAS_RECORDING_LOADED_CHECK(501, sCarrecSetup)	//sCarrecRace)
			
			//Trailer Hauler
			HAS_RECORDING_LOADED_CHECK(401, sCarrecSetup)	//345, sCarrecRace)
			
			//Trailer Packer
			HAS_RECORDING_LOADED_CHECK(403, sCarrecSetup)	//346, sCarrecRace)
		ELIF eMissionObjective != stageArrive
			UNLOAD_RECORDING(500, sCarrecSetup)	//sCarrecRace)
			UNLOAD_RECORDING(501, sCarrecSetup)	//sCarrecRace)
			
			UNLOAD_RECORDING(401, sCarrecSetup)	//345, sCarrecRace)
			UNLOAD_RECORDING(403, sCarrecSetup)	//346, sCarrecRace)
		ENDIF
		
		IF eMissionObjective >= cutSwitch AND eMissionObjective <= stageChase
			//Police Bike 1
			HAS_RECORDING_LOADED_CHECK(500, sCarrecCop)	//sCarrecChase)
			
			//Player
			HAS_RECORDING_LOADED_CHECK(400, sCarrecCop)	//300, sCarrecChase)
			
			//Rival 1
			HAS_RECORDING_LOADED_CHECK(401, sCarrecCop)	//301, sCarrecChase)
			
			//Rival 2
			HAS_RECORDING_LOADED_CHECK(402, sCarrecCop)	//302, sCarrecChase)
		ELSE
			UNLOAD_RECORDING(500, sCarrecCop)	//sCarrecChase)
			UNLOAD_RECORDING(400, sCarrecCop)	//300, sCarrecChase)
			UNLOAD_RECORDING(401, sCarrecCop)	//301, sCarrecChase)
			UNLOAD_RECORDING(402, sCarrecCop)	//302, sCarrecChase)
		ENDIF
		
//		IF eMissionObjective >= cutBridge AND eMissionObjective <= stageDriveHome
//			//Rival 2
//			HAS_RECORDING_LOADED_CHECK(001, sCarrecHome)
//			
//			//Rival 1
//			HAS_RECORDING_LOADED_CHECK(002, sCarrecHome)
//			
//			//Player
//			HAS_RECORDING_LOADED_CHECK(003, sCarrecHome)
//		ELSE
//			UNLOAD_RECORDING(001, sCarrecHome)
//			UNLOAD_RECORDING(002, sCarrecHome)
//			UNLOAD_RECORDING(003, sCarrecHome)
//		ENDIF
		
		IF eMissionObjective >= stageDriveHome AND eMissionObjective <= cutHome
			//ENTITYXF - Michael
			HAS_RECORDING_LOADED_CHECK(001, sCarrecGarage)
			
			//CHEETAH - Trevor
			HAS_RECORDING_LOADED_CHECK(002, sCarrecGarage)
			
			//F620 - Franklin
			HAS_RECORDING_LOADED_CHECK(003, sCarrecGarage)
		ELSE
			UNLOAD_RECORDING(001, sCarrecGarage)
			UNLOAD_RECORDING(002, sCarrecGarage)
			UNLOAD_RECORDING(003, sCarrecGarage)
		ENDIF
	
	//Waypoint Recordings
	
	//Animation Dictionaries
		IF eMissionObjective >= stageArrive AND eMissionObjective <= stageChase
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictCar3)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictCar3)
		ENDIF
		
		IF eMissionObjective >= stageArrive AND eMissionObjective <= stageChase
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictCar3PullOver)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictCar3PullOver)
		ENDIF
		
		IF eMissionObjective >= stageRace AND eMissionObjective <= cutSwitch
			HAS_ANIM_DICT_LOADED_CHECK(strAnimDictCar3LeadInOut)
		ELSE
			UNLOAD_ANIM_DICT(strAnimDictCar3LeadInOut)
		ENDIF
		
		IF eMissionObjective >= stageRace AND eMissionObjective <= cutSwitch
			HAS_ANIM_DICT_LOADED_CHECK(strAnimDictBikeIdle)
		ELSE
			UNLOAD_ANIM_DICT(strAnimDictBikeIdle)
		ENDIF
		
//		IF eMissionObjective >= stageRace AND eMissionObjective <= cutSwitch
//			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPhoneUse)
//		ELSE
//			UNLOAD_ANIM_DICT(sAnimDictPhoneUse)
//		ENDIF
ENDPROC

PROC PRELOAD_ALL_CHASE_RECORDINGS(FLOAT fPlaybackTime, STRING sCarrec)
    INT i = 0
    INT iSetpieceSize = COUNT_OF(SetPieceCarID)
    INT iTrafficSize = COUNT_OF(TrafficCarID)
    
    REPEAT iSetpieceSize i
        IF SetPieceCarRecording[i] > 0
            IF SetPieceCarStartime[i] > fPlaybackTime 
                //IF SetPieceCarStartime[i] - f_playback_time < 50000.0
                    REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[i], sCarrec)
                //ENDIF
//            ELIF fPlaybackTime > SetPieceCarStartime[i] + 10000
//                IF NOT IS_VEHICLE_DRIVEABLE(SetPieceCarID[i])
//                    REMOVE_VEHICLE_RECORDING(SetPieceCarRecording[i], sCarrec)
//                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    REPEAT iTrafficSize i 
        IF TrafficCarRecording[i] > 0
            IF TrafficCarStartime[i] > fPlaybackTime
                //IF TrafficCarStartime[i] - f_playback_time < 50000.0
                    REQUEST_VEHICLE_RECORDING(TrafficCarRecording[i], sCarrec)
                //ENDIF
//            ELIF fPlaybackTime > TrafficCarStartime[i] + 10000
//                IF NOT IS_VEHICLE_DRIVEABLE(TrafficCarID[i])
//                    REMOVE_VEHICLE_RECORDING(TrafficCarRecording[i], sCarrec)
//                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
ENDPROC

FLOAT fCurrentSpeed = 1.0
FLOAT fDesiredSpeed = 1.0

PROC RUBBER_BANDING(FLOAT fRecordingTime, STRING sCarrec)
	IF ARE_STRINGS_EQUAL(sCarrec, sCarrecSetup)	//sCarrecRace)
		IF fRecordingTime < 2000.0
			fCurrentSpeed = 1.0
		ELIF fRecordingTime < 5000.0
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredSpeed, vehPlayer, vehRival2, 2.5, 5.0, 25.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE)	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival2, PLAYER_PED_ID(), fPlaybackSpeed, 8.0, 5.0, 20.0, 0.6, 1.0, 0.8, FALSE)
		ELIF fRecordingTime < 15000.0
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredSpeed, vehPlayer, vehRival2, 5.0, 10.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE)	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival2, PLAYER_PED_ID(), fPlaybackSpeed, 10.0 + 0.1, 7.0 +  0.1, 25.0 +  0.1, 0.6 + 0.1, 2.0 + 0.1, 0.8 + 0.1, FALSE)
//		ELIF fRecordingTime < 20000.0
//			CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival2, PLAYER_PED_ID(), fPlaybackSpeed, 15.0 + 2.5, 10.0 + 2.5, 25.0 + 2.5, 0.8 + 0.1, 2.0 + 0.1, 1.0 + 0.1, FALSE)
//		ELIF fRecordingTime < 25000.0	//42000.0
//			CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival2, PLAYER_PED_ID(), fPlaybackSpeed, 10.0 + 4.5, 7.0 + 4.5, 25.0 + 4.5, 0.6 + 0.2, 2.0 + 0.2, 0.8 + 0.2, FALSE)
//		ELIF fRecordingTime < 52000.0	//47000.0
//			CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival2, PLAYER_PED_ID(), fPlaybackSpeed, GET_INTERP_POINT_FLOAT(10.0, 20.0, 25000.0, 45000.0, fRecordingTime), GET_INTERP_POINT_FLOAT(7.0, 15.0, 25000.0, 45000.0, fRecordingTime), GET_INTERP_POINT_FLOAT(25.0, 35.0, 25000.0, 45000.0, fRecordingTime), GET_INTERP_POINT_FLOAT(0.6, 1.2, 25000.0, 45000.0, fRecordingTime), 2.0, GET_INTERP_POINT_FLOAT(0.8, 1.5, 25000.0, 45000.0, fRecordingTime), FALSE)
//		ELIF fRecordingTime < 60000.0
//			CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival2, PLAYER_PED_ID(), fPlaybackSpeed, 10.0 + 4.5, 7.0 + 4.5, 25.0 + 4.5, 0.6 + 0.2, 2.0 + 0.2, 0.8 + 0.2, FALSE)
		ELIF fRecordingTime < 86000.0
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredSpeed, vehPlayer, vehRival2, 5.0, 10.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE)	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival2, PLAYER_PED_ID(), fPlaybackSpeed, 10.0 + 4.5, 7.0 + 4.5, 25.0 + 4.5, 0.6 + 0.2, 2.0 + 0.2, 0.8 + 0.2, FALSE)
		ELIF fRecordingTime < 102000.0	//98000.0
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehRival2)
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
				TASK_VEHICLE_MISSION_COORS_TARGET(pedRival2, vehRival2, <<2336.7979, 5885.9106, 46.6832>>, MISSION_GOTO_RACING, 60.0, DRIVINGMODE_AVOIDCARS, 1.0, 1.0, TRUE)
			ENDIF
			
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredSpeed, vehPlayer, vehRival1, 5.0, 10.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE)	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival1, PLAYER_PED_ID(), fPlaybackSpeed, 10.0, 7.0, 25.0, 0.5, 2.0, 0.8, FALSE)
		ELIF fRecordingTime < GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrec)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehRival1)
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
				TASK_VEHICLE_MISSION_COORS_TARGET(pedRival1, vehRival1, <<2336.7979, 5885.9106, 46.6832>>, MISSION_GOTO_RACING, 60.0, DRIVINGMODE_AVOIDCARS, 1.0, 1.0, TRUE)
			ENDIF
			
			//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehRival2, PLAYER_PED_ID(), fPlaybackSpeed, 10.0, 7.0, 25.0, 0.6, 2.0, 0.8, FALSE)
		ENDIF
		
		UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(fCurrentSpeed, fDesiredSpeed)
		
		fPlaybackSpeed = fCurrentSpeed
	ELIF ARE_STRINGS_EQUAL(sCarrec, sCarrecCop)	//sCarrecChase)
		IF fRecordingTime < 5000.0
			fCurrentSpeed = 1.0
		ELIF fRecordingTime < 128000.0
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredSpeed, vehPolice2, vehPolice1, 5.0, 10.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE)	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehPolice1, PLAYER_PED_ID(), fPlaybackSpeed, 10.0, 5.0, 15.0, 0.6, 2.0, 0.8, FALSE)
		ELIF fRecordingTime < 135000.0
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredSpeed, vehPolice2, vehPolice1, 10.0, 15.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE)	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehPolice1, PLAYER_PED_ID(), fPlaybackSpeed, 20.0, 15.0, 30.0, 0.8, 2.0, 1.0, FALSE)
		ELIF fRecordingTime < 140000.0
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredSpeed, vehPolice2, vehPolice1, 5.0, 10.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE)	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehPolice1, PLAYER_PED_ID(), fPlaybackSpeed, 10.0, 5.0, 15.0, 0.6, 2.0, 0.8, FALSE)
		ELIF fRecordingTime < 153107.800
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredSpeed, vehPolice2, vehPolice1, 5.0, 10.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE)	//CALCULATE_PLAYBACK_SPEED_FROM_CHAR(vehPolice1, PLAYER_PED_ID(), fPlaybackSpeed, 2.5, 0.0, 5.0, 0.4, 1.5, 0.6, FALSE)
		ELIF fRecordingTime > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrec) - 100
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
		ELIF fRecordingTime < GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrec)
			IF fCurrentSpeed < 1.0
				fCurrentSpeed = fCurrentSpeed +@ 0.1
			ELIF fCurrentSpeed > 1.0
				fCurrentSpeed = fCurrentSpeed -@ 0.1
			ENDIF
			PRINTLN("fCurrentSpeed = ", fCurrentSpeed)
		ENDIF
		
		UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(fCurrentSpeed, fDesiredSpeed)
		
		fPlaybackSpeed = fCurrentSpeed
	ENDIF
ENDPROC

FUNC FLOAT GET_GROUND_Z(VECTOR vCoords)
	FLOAT fGroundZ
	
	GET_GROUND_Z_FOR_3D_COORD(vCoords, fGroundZ)
	
	RETURN fGroundZ
ENDFUNC

INT iTimeSinceLastHorn
INT iClosestVehicleCheckTimer

PROC DO_CHASE_HORNS(VEHICLE_INDEX vehClosest)
	//General horns for all vehicles: get the nearest vehicle and play horn
	IF GET_GAME_TIMER() - iTimeSinceLastHorn > 2000
		IF vehClosest != vehRival1
		AND vehClosest != vehRival2
			IF IS_VEHICLE_DRIVEABLE(vehClosest)
				PED_INDEX pedClosest = GET_PED_IN_VEHICLE_SEAT(vehClosest)
				
				IF NOT IS_PED_INJURED(pedClosest)
					INT iRand
					
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					
					IF iRand = 0
						START_VEHICLE_HORN(vehClosest, 2000)
					ELIF iRand = 1
						PLAY_PED_AMBIENT_SPEECH(pedClosest, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_NORMAL)
					ELIF iRand = 2
						START_VEHICLE_HORN(vehClosest, 2000)
						PLAY_PED_AMBIENT_SPEECH(pedClosest, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_NORMAL)
					ENDIF
					
					iTimeSinceLastHorn = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_TRAFFIC_VEHICLE_SEARCH_FLAGS()
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED)
ENDFUNC

//═════════════════════════════╡ SETUP PROCEDURES ╞══════════════════════════════

//═══════════════════════════════════╡ UBER ╞════════════════════════════════════

PROC UberSetupVariables()
	TrafficCarPos[0] = <<2531.8367, 567.2463, 111.2710>>
	TrafficCarQuatX[0] = 0.0104
	TrafficCarQuatY[0] = 0.0256
	TrafficCarQuatZ[0] = 0.9981
	TrafficCarQuatW[0] = -0.0547
	TrafficCarRecording[0] = 1
	TrafficCarStartime[0] = 3432.0000
	TrafficCarModel[0] = Mule

	TrafficCarPos[1] = <<2528.3938, 602.4405, 108.8434>>
	TrafficCarQuatX[1] = 0.0095
	TrafficCarQuatY[1] = 0.0302
	TrafficCarQuatZ[1] = 0.9960
	TrafficCarQuatW[1] = -0.0832
	TrafficCarRecording[1] = 2
	TrafficCarStartime[1] = 4884.0000
	TrafficCarModel[1] = minivan

	TrafficCarPos[2] = <<2523.9231, 590.3826, 109.1960>>
	TrafficCarQuatX[2] = -0.0180
	TrafficCarQuatY[2] = 0.0280
	TrafficCarQuatZ[2] = 0.9992
	TrafficCarQuatW[2] = -0.0234
	TrafficCarRecording[2] = 3
	TrafficCarStartime[2] = 5214.0000
	TrafficCarModel[2] = minivan

	TrafficCarPos[3] = <<2518.8372, 628.9053, 107.6059>>
	TrafficCarQuatX[3] = -0.0193
	TrafficCarQuatY[3] = 0.0300
	TrafficCarQuatZ[3] = 0.9973
	TrafficCarQuatW[3] = -0.0645
	TrafficCarRecording[3] = 4
	TrafficCarStartime[3] = 5676.0000
	TrafficCarModel[3] = Mule

	TrafficCarPos[4] = <<2517.6628, 641.6564, 106.3506>>
	TrafficCarQuatX[4] = -0.0138
	TrafficCarQuatY[4] = 0.0317
	TrafficCarQuatZ[4] = 0.9955
	TrafficCarQuatW[4] = -0.0884
	TrafficCarRecording[4] = 5
	TrafficCarStartime[4] = 7194.0000
	TrafficCarModel[4] = minivan

	TrafficCarPos[5] = <<2511.8474, 706.9466, 102.3872>>
	TrafficCarQuatX[5] = 0.0098
	TrafficCarQuatY[5] = 0.0355
	TrafficCarQuatZ[5] = 0.9951
	TrafficCarQuatW[5] = -0.0922
	TrafficCarRecording[5] = 6
	TrafficCarStartime[5] = 8118.0000
	TrafficCarModel[5] = Mule

	TrafficCarPos[6] = <<2500.5420, 736.9729, 99.4502>>
	TrafficCarQuatX[6] = -0.0158
	TrafficCarQuatY[6] = 0.0340
	TrafficCarQuatZ[6] = 0.9966
	TrafficCarQuatW[6] = -0.0739
	TrafficCarRecording[6] = 7
	TrafficCarStartime[6] = 8910.0000
	TrafficCarModel[6] = emperor

	TrafficCarPos[7] = <<2504.5508, 746.0255, 99.1176>>
	TrafficCarQuatX[7] = 0.0095
	TrafficCarQuatY[7] = 0.0367
	TrafficCarQuatZ[7] = 0.9902
	TrafficCarQuatW[7] = -0.1342
	TrafficCarRecording[7] = 8
	TrafficCarStartime[7] = 9240.0000
	TrafficCarModel[7] = minivan

	TrafficCarPos[8] = <<2490.1902, 789.2353, 96.4467>>
	TrafficCarQuatX[8] = -0.0208
	TrafficCarQuatY[8] = 0.0316
	TrafficCarQuatZ[8] = 0.9940
	TrafficCarQuatW[8] = -0.1028
	TrafficCarRecording[8] = 9
	TrafficCarStartime[8] = 10164.0000
	TrafficCarModel[8] = Mule

	TrafficCarPos[9] = <<2488.1423, 798.9224, 95.2954>>
	TrafficCarQuatX[9] = -0.0210
	TrafficCarQuatY[9] = 0.0298
	TrafficCarQuatZ[9] = 0.9967
	TrafficCarQuatW[9] = -0.0728
	TrafficCarRecording[9] = 10
	TrafficCarStartime[9] = 10362.0000
	TrafficCarModel[9] = minivan

	TrafficCarPos[10] = <<2487.1033, 803.4872, 95.1668>>
	TrafficCarQuatX[10] = -0.0199
	TrafficCarQuatY[10] = 0.0299
	TrafficCarQuatZ[10] = 0.9966
	TrafficCarQuatW[10] = -0.0736
	TrafficCarRecording[10] = 11
	TrafficCarStartime[10] = 10890.0000
	TrafficCarModel[10] = minivan

	TrafficCarPos[11] = <<2474.4858, 851.2122, 92.7213>>
	TrafficCarQuatX[11] = -0.0209
	TrafficCarQuatY[11] = 0.0219
	TrafficCarQuatZ[11] = 0.9875
	TrafficCarQuatW[11] = -0.1549
	TrafficCarRecording[11] = 12
	TrafficCarStartime[11] = 11616.0000
	TrafficCarModel[11] = Mule

	TrafficCarPos[12] = <<2471.3564, 860.8902, 91.7206>>
	TrafficCarQuatX[12] = -0.0232
	TrafficCarQuatY[12] = 0.0194
	TrafficCarQuatZ[12] = 0.9796
	TrafficCarQuatW[12] = -0.1984
	TrafficCarRecording[12] = 13
	TrafficCarStartime[12] = 11946.0000
	TrafficCarModel[12] = minivan

	TrafficCarPos[13] = <<2462.5959, 902.4520, 89.8936>>
	TrafficCarQuatX[13] = 0.0072
	TrafficCarQuatY[13] = 0.0228
	TrafficCarQuatZ[13] = 0.9802
	TrafficCarQuatW[13] = -0.1965
	TrafficCarRecording[13] = 14
	TrafficCarStartime[13] = 12738.0000
	TrafficCarModel[13] = minivan

	TrafficCarPos[14] = <<2456.2698, 917.0245, 89.0053>>
	TrafficCarQuatX[14] = 0.0082
	TrafficCarQuatY[14] = 0.0213
	TrafficCarQuatZ[14] = 0.9690
	TrafficCarQuatW[14] = -0.2462
	TrafficCarRecording[14] = 15
	TrafficCarStartime[14] = 13068.0000
	TrafficCarModel[14] = emperor

	TrafficCarPos[15] = <<2479.7439, 924.9675, 86.5518>>
	TrafficCarQuatX[15] = 0.0025
	TrafficCarQuatY[15] = -0.0175
	TrafficCarQuatZ[15] = 0.2824
	TrafficCarQuatW[15] = 0.9591
	TrafficCarRecording[15] = 16
	TrafficCarStartime[15] = 13068.0000
	TrafficCarModel[15] = minivan

	TrafficCarPos[16] = <<2442.2642, 946.6888, 88.5245>>
	TrafficCarQuatX[16] = 0.0092
	TrafficCarQuatY[16] = 0.0222
	TrafficCarQuatZ[16] = 0.9713
	TrafficCarQuatW[16] = -0.2368
	TrafficCarRecording[16] = 17
	TrafficCarStartime[16] = 13662.0000
	TrafficCarModel[16] = Mule

	TrafficCarPos[17] = <<2450.3679, 964.5439, 85.8815>>
	TrafficCarQuatX[17] = 0.0010
	TrafficCarQuatY[17] = -0.0181
	TrafficCarQuatZ[17] = 0.3259
	TrafficCarQuatW[17] = 0.9452
	TrafficCarRecording[17] = 18
	TrafficCarStartime[17] = 13992.0000
	TrafficCarModel[17] = minivan

	TrafficCarPos[18] = <<2430.6428, 968.1787, 87.0446>>
	TrafficCarQuatX[18] = 0.0072
	TrafficCarQuatY[18] = 0.0228
	TrafficCarQuatZ[18] = 0.9626
	TrafficCarQuatW[18] = -0.2698
	TrafficCarRecording[18] = 19
	TrafficCarStartime[18] = 14190.0000
	TrafficCarModel[18] = minivan

	TrafficCarPos[19] = <<2428.4163, 971.8596, 86.6286>>
	TrafficCarQuatX[19] = 0.0082
	TrafficCarQuatY[19] = 0.0227
	TrafficCarQuatZ[19] = 0.9740
	TrafficCarQuatW[19] = -0.2251
	TrafficCarRecording[19] = 20
	TrafficCarStartime[19] = 14454.0000
	TrafficCarModel[19] = emperor

	TrafficCarPos[20] = <<2425.7302, 1005.2106, 85.5479>>
	TrafficCarQuatX[20] = -0.0130
	TrafficCarQuatY[20] = -0.0026
	TrafficCarQuatZ[20] = 0.3390
	TrafficCarQuatW[20] = 0.9407
	TrafficCarRecording[20] = 21
	TrafficCarStartime[20] = 14916.0000
	TrafficCarModel[20] = minivan

	TrafficCarPos[21] = <<2404.2795, 1007.8649, 85.5689>>
	TrafficCarQuatX[21] = -0.0026
	TrafficCarQuatY[21] = 0.0243
	TrafficCarQuatZ[21] = 0.9489
	TrafficCarQuatW[21] = -0.3145
	TrafficCarRecording[21] = 22
	TrafficCarStartime[21] = 15114.0000
	TrafficCarModel[21] = Mule

	TrafficCarPos[22] = <<2381.6492, 1026.7485, 83.5159>>
	TrafficCarQuatX[22] = -0.0257
	TrafficCarQuatY[22] = 0.0190
	TrafficCarQuatZ[22] = 0.9447
	TrafficCarQuatW[22] = -0.3263
	TrafficCarRecording[22] = 23
	TrafficCarStartime[22] = 15708.0000
	TrafficCarModel[22] = minivan

//	TrafficCarPos[23] = <<2398.5208, 1039.1782, 83.7117>>
//	TrafficCarQuatX[23] = -0.0240
//	TrafficCarQuatY[23] = -0.0019
//	TrafficCarQuatZ[23] = 0.3131
//	TrafficCarQuatW[23] = 0.9494
//	TrafficCarRecording[23] = 24
//	TrafficCarStartime[23] = 15774.0000
//	TrafficCarModel[23] = minivan

//	TrafficCarPos[24] = <<2398.7788, 1029.6384, 83.7781>>
//	TrafficCarQuatX[24] = -0.0193
//	TrafficCarQuatY[24] = -0.0154
//	TrafficCarQuatZ[24] = 0.3084
//	TrafficCarQuatW[24] = 0.9509
//	TrafficCarRecording[24] = 25
//	TrafficCarStartime[24] = 16170.0000
//	TrafficCarModel[24] = emperor

	TrafficCarPos[25] = <<2357.7251, 1055.2380, 81.2879>>
	TrafficCarQuatX[25] = -0.0317
	TrafficCarQuatY[25] = 0.0111
	TrafficCarQuatZ[25] = 0.9215
	TrafficCarQuatW[25] = -0.3869
	TrafficCarRecording[25] = 26
	TrafficCarStartime[25] = 16434.0000
	TrafficCarModel[25] = emperor

//	TrafficCarPos[26] = <<2354.1641, 1066.6754, 80.8054>>
//	TrafficCarQuatX[26] = 0.0087
//	TrafficCarQuatY[26] = 0.0264
//	TrafficCarQuatZ[26] = 0.9356
//	TrafficCarQuatW[26] = -0.3520
//	TrafficCarRecording[26] = 27
//	TrafficCarStartime[26] = 16632.0000
//	TrafficCarModel[26] = minivan

	TrafficCarPos[27] = <<2353.7114, 1066.8109, 80.6639>>
	TrafficCarQuatX[27] = 0.0105
	TrafficCarQuatY[27] = 0.0261
	TrafficCarQuatZ[27] = 0.9482
	TrafficCarQuatW[27] = -0.3164
	TrafficCarRecording[27] = 28
	TrafficCarStartime[27] = 16962.0000
	TrafficCarModel[27] = asterope

//	TrafficCarPos[28] = <<2344.4421, 1099.4828, 79.5341>>
//	TrafficCarQuatX[28] = -0.0205
//	TrafficCarQuatY[28] = 0.0066
//	TrafficCarQuatZ[28] = 0.3597
//	TrafficCarQuatW[28] = 0.9328
//	TrafficCarRecording[28] = 29
//	TrafficCarStartime[28] = 17424.0000
//	TrafficCarModel[28] = asterope

	TrafficCarPos[29] = <<2322.8469, 1102.3051, 79.3780>>
	TrafficCarQuatX[29] = 0.0049
	TrafficCarQuatY[29] = 0.0175
	TrafficCarQuatZ[29] = 0.9272
	TrafficCarQuatW[29] = -0.3742
	TrafficCarRecording[29] = 30
	TrafficCarStartime[29] = 17688.0000
	TrafficCarModel[29] = minivan

//	TrafficCarPos[30] = <<2313.7671, 1124.9960, 79.2767>>
//	TrafficCarQuatX[30] = -0.0047
//	TrafficCarQuatY[30] = -0.0230
//	TrafficCarQuatZ[30] = 0.3577
//	TrafficCarQuatW[30] = 0.9335
//	TrafficCarRecording[30] = 31
//	TrafficCarStartime[30] = 18150.0000
//	TrafficCarModel[30] = Mule

	TrafficCarPos[31] = <<2382.2473, 1099.5322, 69.9259>>
	TrafficCarQuatX[31] = -0.0722
	TrafficCarQuatY[31] = -0.0345
	TrafficCarQuatZ[31] = 0.3048
	TrafficCarQuatW[31] = 0.9490
	TrafficCarRecording[31] = 32
	TrafficCarStartime[31] = 18282.0000
	TrafficCarModel[31] = minivan

//	TrafficCarPos[32] = <<2300.4705, 1147.4011, 77.9743>>
//	TrafficCarQuatX[32] = -0.0155
//	TrafficCarQuatY[32] = 0.0106
//	TrafficCarQuatZ[32] = 0.3904
//	TrafficCarQuatW[32] = 0.9204
//	TrafficCarRecording[32] = 33
//	TrafficCarStartime[32] = 18678.0000
//	TrafficCarModel[32] = emperor

//	TrafficCarPos[33] = <<2304.0510, 1144.5635, 78.0309>>
//	TrafficCarQuatX[33] = -0.0148
//	TrafficCarQuatY[33] = 0.0117
//	TrafficCarQuatZ[33] = 0.3121
//	TrafficCarQuatW[33] = 0.9499
//	TrafficCarRecording[33] = 34
//	TrafficCarStartime[33] = 19140.0000
//	TrafficCarModel[33] = minivan

//	TrafficCarPos[34] = <<2285.7996, 1164.7573, 77.5157>>
//	TrafficCarQuatX[34] = -0.0141
//	TrafficCarQuatY[34] = 0.0118
//	TrafficCarQuatZ[34] = 0.3843
//	TrafficCarQuatW[34] = 0.9230
//	TrafficCarRecording[34] = 35
//	TrafficCarStartime[34] = 19470.0000
//	TrafficCarModel[34] = minivan

	TrafficCarPos[35] = <<2296.1975, 1131.9489, 78.3355>>
	TrafficCarQuatX[35] = 0.0125
	TrafficCarQuatY[35] = 0.0162
	TrafficCarQuatZ[35] = 0.9365
	TrafficCarQuatW[35] = -0.3500
	TrafficCarRecording[35] = 36
	TrafficCarStartime[35] = 19602.0000
	TrafficCarModel[35] = minivan

	TrafficCarPos[36] = <<2260.1240, 1173.1749, 77.3797>>
	TrafficCarQuatX[36] = 0.0113
	TrafficCarQuatY[36] = 0.0126
	TrafficCarQuatZ[36] = 0.9313
	TrafficCarQuatW[36] = -0.3639
	TrafficCarRecording[36] = 37
	TrafficCarStartime[36] = 19602.0000
	TrafficCarModel[36] = minivan

	TrafficCarPos[37] = <<2254.7627, 1178.7247, 76.9189>>
	TrafficCarQuatX[37] = 0.0120
	TrafficCarQuatY[37] = 0.0128
	TrafficCarQuatZ[37] = 0.9249
	TrafficCarQuatW[37] = -0.3797
	TrafficCarRecording[37] = 38
	TrafficCarStartime[37] = 20196.0000
	TrafficCarModel[37] = asterope

//	TrafficCarPos[38] = <<2244.7361, 1213.0354, 76.5186>>
//	TrafficCarQuatX[38] = -0.0114
//	TrafficCarQuatY[38] = 0.0100
//	TrafficCarQuatZ[38] = 0.3397
//	TrafficCarQuatW[38] = 0.9404
//	TrafficCarRecording[38] = 39
//	TrafficCarStartime[38] = 20592.0000
//	TrafficCarModel[38] = minivan

//	TrafficCarPos[39] = <<2236.6230, 1222.0437, 76.6346>>
//	TrafficCarQuatX[39] = -0.0120
//	TrafficCarQuatY[39] = 0.0108
//	TrafficCarQuatZ[39] = 0.3771
//	TrafficCarQuatW[39] = 0.9260
//	TrafficCarRecording[39] = 40
//	TrafficCarStartime[39] = 20856.0000
//	TrafficCarModel[39] = minivan

	TrafficCarPos[40] = <<2254.0083, 1178.5549, 77.0527>>
	TrafficCarQuatX[40] = 0.0076
	TrafficCarQuatY[40] = 0.0111
	TrafficCarQuatZ[40] = 0.9422
	TrafficCarQuatW[40] = -0.3348
	TrafficCarRecording[40] = 41
	TrafficCarStartime[40] = 21186.0000
	TrafficCarModel[40] = emperor

	TrafficCarPos[41] = <<2191.6226, 1245.7930, 75.9200>>
	TrafficCarQuatX[41] = -0.0131
	TrafficCarQuatY[41] = 0.0023
	TrafficCarQuatZ[41] = 0.9440
	TrafficCarQuatW[41] = -0.3296
	TrafficCarRecording[41] = 42
	TrafficCarStartime[41] = 21648.0000
	TrafficCarModel[41] = asterope

	TrafficCarPos[42] = <<2186.2471, 1285.2190, 75.6404>>
	TrafficCarQuatX[42] = -0.0070
	TrafficCarQuatY[42] = 0.0073
	TrafficCarQuatZ[42] = 0.3289
	TrafficCarQuatW[42] = 0.9443
	TrafficCarRecording[42] = 43
	TrafficCarStartime[42] = 22176.0000
	TrafficCarModel[42] = minivan

	TrafficCarPos[43] = <<2177.3938, 1287.1149, 75.2953>>
	TrafficCarQuatX[43] = 0.0020
	TrafficCarQuatY[43] = -0.0179
	TrafficCarQuatZ[43] = 0.3596
	TrafficCarQuatW[43] = 0.9329
	TrafficCarRecording[43] = 44
	TrafficCarStartime[43] = 22440.0000
	TrafficCarModel[43] = emperor

//	TrafficCarPos[44] = <<2176.6436, 1297.3920, 75.6882>>
//	TrafficCarQuatX[44] = -0.0085
//	TrafficCarQuatY[44] = 0.0143
//	TrafficCarQuatZ[44] = 0.3292
//	TrafficCarQuatW[44] = 0.9441
//	TrafficCarRecording[44] = 45
//	TrafficCarStartime[44] = 22638.0000
//	TrafficCarModel[44] = minivan

	TrafficCarPos[45] = <<2168.6582, 1284.1532, 76.0682>>
	TrafficCarQuatX[45] = 0.0078
	TrafficCarQuatY[45] = 0.0062
	TrafficCarQuatZ[45] = 0.9487
	TrafficCarQuatW[45] = -0.3161
	TrafficCarRecording[45] = 46
	TrafficCarStartime[45] = 22836.0000
	TrafficCarModel[45] = Mule

	TrafficCarPos[46] = <<2159.5466, 1320.8896, 75.0434>>
	TrafficCarQuatX[46] = -0.0036
	TrafficCarQuatY[46] = 0.0046
	TrafficCarQuatZ[46] = 0.2774
	TrafficCarQuatW[46] = 0.9607
	TrafficCarRecording[46] = 47
	TrafficCarStartime[46] = 23034.0000
	TrafficCarModel[46] = asterope

	TrafficCarPos[47] = <<2155.5037, 1317.0106, 75.3680>>
	TrafficCarQuatX[47] = 0.0021
	TrafficCarQuatY[47] = -0.0150
	TrafficCarQuatZ[47] = 0.2680
	TrafficCarQuatW[47] = 0.9633
	TrafficCarRecording[47] = 48
	TrafficCarStartime[47] = 23232.0000
	TrafficCarModel[47] = minivan

	TrafficCarPos[48] = <<2125.9438, 1334.5380, 75.2456>>
	TrafficCarQuatX[48] = -0.0170
	TrafficCarQuatY[48] = -0.0040
	TrafficCarQuatZ[48] = 0.9526
	TrafficCarQuatW[48] = -0.3038
	TrafficCarRecording[48] = 49
	TrafficCarStartime[48] = 23694.0000
	TrafficCarModel[48] = minivan

	TrafficCarPos[49] = <<2118.6013, 1355.4376, 74.9437>>
	TrafficCarQuatX[49] = 0.0123
	TrafficCarQuatY[49] = 0.0044
	TrafficCarQuatZ[49] = 0.9453
	TrafficCarQuatW[49] = -0.3260
	TrafficCarRecording[49] = 50
	TrafficCarStartime[49] = 24024.0000
	TrafficCarModel[49] = minivan

	TrafficCarPos[50] = <<2111.5498, 1365.3160, 74.9957>>
	TrafficCarQuatX[50] = 0.0127
	TrafficCarQuatY[50] = 0.0030
	TrafficCarQuatZ[50] = 0.9686
	TrafficCarQuatW[50] = -0.2483
	TrafficCarRecording[50] = 51
	TrafficCarStartime[50] = 24288.0000
	TrafficCarModel[50] = emperor

//	TrafficCarPos[51] = <<2112.5137, 1380.6727, 75.7430>>
//	TrafficCarQuatX[51] = 0.0026
//	TrafficCarQuatY[51] = -0.0084
//	TrafficCarQuatZ[51] = 0.2792
//	TrafficCarQuatW[51] = 0.9602
//	TrafficCarRecording[51] = 52
//	TrafficCarStartime[51] = 25080.0000
//	TrafficCarModel[51] = Mule

	TrafficCarPos[52] = <<2114.0325, 1361.7507, 74.9520>>
	TrafficCarQuatX[52] = 0.0198
	TrafficCarQuatY[52] = 0.0097
	TrafficCarQuatZ[52] = 0.9551
	TrafficCarQuatW[52] = -0.2954
	TrafficCarRecording[52] = 53
	TrafficCarStartime[52] = 25476.0000
	TrafficCarModel[52] = minivan

	TrafficCarPos[53] = <<2094.2156, 1411.4084, 75.2758>>
	TrafficCarQuatX[53] = 0.0052
	TrafficCarQuatY[53] = -0.0165
	TrafficCarQuatZ[53] = 0.2652
	TrafficCarQuatW[53] = 0.9640
	TrafficCarRecording[53] = 54
	TrafficCarStartime[53] = 25608.0000
	TrafficCarModel[53] = minivan

	TrafficCarPos[54] = <<2078.2864, 1449.0529, 74.9826>>
	TrafficCarQuatX[54] = -0.0015
	TrafficCarQuatY[54] = 0.0112
	TrafficCarQuatZ[54] = 0.2564
	TrafficCarQuatW[54] = 0.9665
	TrafficCarRecording[54] = 55
	TrafficCarStartime[54] = 26466.0000
	TrafficCarModel[54] = asterope

	TrafficCarPos[55] = <<2070.5757, 1419.4891, 75.2672>>
	TrafficCarQuatX[55] = -0.0106
	TrafficCarQuatY[55] = -0.0032
	TrafficCarQuatZ[55] = 0.9545
	TrafficCarQuatW[55] = -0.2979
	TrafficCarRecording[55] = 56
	TrafficCarStartime[55] = 26664.0000
	TrafficCarModel[55] = asterope

	TrafficCarPos[56] = <<2074.4333, 1445.0652, 75.0597>>
	TrafficCarQuatX[56] = 0.0051
	TrafficCarQuatY[56] = -0.0166
	TrafficCarQuatZ[56] = 0.2513
	TrafficCarQuatW[56] = 0.9677
	TrafficCarRecording[56] = 57
	TrafficCarStartime[56] = 26730.0000
	TrafficCarModel[56] = emperor

	TrafficCarPos[57] = <<2034.1135, 1491.6438, 75.4180>>
	TrafficCarQuatX[57] = -0.0116
	TrafficCarQuatY[57] = -0.0029
	TrafficCarQuatZ[57] = 0.9807
	TrafficCarQuatW[57] = -0.1953
	TrafficCarRecording[57] = 58
	TrafficCarStartime[57] = 27192.0000
	TrafficCarModel[57] = minivan

	TrafficCarPos[58] = <<2031.2903, 1498.9364, 75.1537>>
	TrafficCarQuatX[58] = -0.0155
	TrafficCarQuatY[58] = -0.0053
	TrafficCarQuatZ[58] = 0.9681
	TrafficCarQuatW[58] = -0.2499
	TrafficCarRecording[58] = 59
	TrafficCarStartime[58] = 27456.0000
	TrafficCarModel[58] = minivan

//	TrafficCarPos[59] = <<2039.0902, 1517.1097, 75.9982>>
//	TrafficCarQuatX[59] = -0.0025
//	TrafficCarQuatY[59] = -0.0023
//	TrafficCarQuatZ[59] = 0.2142
//	TrafficCarQuatW[59] = 0.9768
//	TrafficCarRecording[59] = 60
//	TrafficCarStartime[59] = 27522.0000
//	TrafficCarModel[59] = Mule

	TrafficCarPos[60] = <<2039.6882, 1528.8265, 75.4200>>
	TrafficCarQuatX[60] = -0.0045
	TrafficCarQuatY[60] = -0.0085
	TrafficCarQuatZ[60] = 0.2030
	TrafficCarQuatW[60] = 0.9791
	TrafficCarRecording[60] = 61
	TrafficCarStartime[60] = 27984.0000
	TrafficCarModel[60] = minivan

	TrafficCarPos[61] = <<2017.7032, 1529.8983, 75.0193>>
	TrafficCarQuatX[61] = 0.0036
	TrafficCarQuatY[61] = 0.0035
	TrafficCarQuatZ[61] = 0.9739
	TrafficCarQuatW[61] = -0.2269
	TrafficCarRecording[61] = 62
	TrafficCarStartime[61] = 28050.0000
	TrafficCarModel[61] = emperor

	TrafficCarPos[62] = <<2039.0934, 1530.8571, 75.1213>>
	TrafficCarQuatX[62] = -0.0058
	TrafficCarQuatY[62] = -0.0118
	TrafficCarQuatZ[62] = 0.1708
	TrafficCarQuatW[62] = 0.9852
	TrafficCarRecording[62] = 63
	TrafficCarStartime[62] = 28512.0000
	TrafficCarModel[62] = minivan

	TrafficCarPos[63] = <<2003.0155, 1606.6510, 73.6440>>
	TrafficCarQuatX[63] = -0.0180
	TrafficCarQuatY[63] = -0.0027
	TrafficCarQuatZ[63] = 0.1751
	TrafficCarQuatW[63] = 0.9844
	TrafficCarRecording[63] = 64
	TrafficCarStartime[63] = 29502.0000
	TrafficCarModel[63] = minivan

	TrafficCarPos[64] = <<2007.3918, 1610.4432, 73.2368>>
	TrafficCarQuatX[64] = -0.0188
	TrafficCarQuatY[64] = -0.0044
	TrafficCarQuatZ[64] = 0.1347
	TrafficCarQuatW[64] = 0.9907
	TrafficCarRecording[64] = 65
	TrafficCarStartime[64] = 29832.0000
	TrafficCarModel[64] = asterope

	TrafficCarPos[65] = <<1961.8286, 1653.6876, 72.4539>>
	TrafficCarQuatX[65] = -0.0041
	TrafficCarQuatY[65] = 0.0207
	TrafficCarQuatZ[65] = 0.9811
	TrafficCarQuatW[65] = -0.1924
	TrafficCarRecording[65] = 66
	TrafficCarStartime[65] = 30756.0000
	TrafficCarModel[65] = minivan

	TrafficCarPos[66] = <<1979.6003, 1672.9697, 70.8238>>
	TrafficCarQuatX[66] = -0.0266
	TrafficCarQuatY[66] = -0.0043
	TrafficCarQuatZ[66] = 0.1589
	TrafficCarQuatW[66] = 0.9869
	TrafficCarRecording[66] = 67
	TrafficCarStartime[66] = 30954.0000
	TrafficCarModel[66] = Mule

//	TrafficCarPos[67] = <<1987.3289, 1666.5441, 70.5441>>
//	TrafficCarQuatX[67] = -0.0266
//	TrafficCarQuatY[67] = -0.0033
//	TrafficCarQuatZ[67] = 0.1300
//	TrafficCarQuatW[67] = 0.9912
//	TrafficCarRecording[67] = 68
//	TrafficCarStartime[67] = 31152.0000
//	TrafficCarModel[67] = emperor

	TrafficCarPos[68] = <<1946.9937, 1689.0887, 70.3624>>
	TrafficCarQuatX[68] = -0.0058
	TrafficCarQuatY[68] = 0.0249
	TrafficCarQuatZ[68] = 0.9723
	TrafficCarQuatW[68] = -0.2323
	TrafficCarRecording[68] = 69
	TrafficCarStartime[68] = 31548.0000
	TrafficCarModel[68] = minivan

	TrafficCarPos[69] = <<1934.6880, 1715.0862, 68.7313>>
	TrafficCarQuatX[69] = -0.0040
	TrafficCarQuatY[69] = 0.0275
	TrafficCarQuatZ[69] = 0.9850
	TrafficCarQuatW[69] = -0.1705
	TrafficCarRecording[69] = 70
	TrafficCarStartime[69] = 32142.0000
	TrafficCarModel[69] = asterope

	TrafficCarPos[70] = <<1957.8663, 1743.5569, 66.1376>>
	TrafficCarQuatX[70] = -0.0282
	TrafficCarQuatY[70] = -0.0039
	TrafficCarQuatZ[70] = 0.1453
	TrafficCarQuatW[70] = 0.9890
	TrafficCarRecording[70] = 71
	TrafficCarStartime[70] = 32472.0000
	TrafficCarModel[70] = minivan

	TrafficCarPos[71] = <<1925.9292, 1749.3240, 67.6292>>
	TrafficCarQuatX[71] = -0.0055
	TrafficCarQuatY[71] = 0.0275
	TrafficCarQuatZ[71] = 0.9804
	TrafficCarQuatW[71] = -0.1948
	TrafficCarRecording[71] = 72
	TrafficCarStartime[71] = 32736.0000
	TrafficCarModel[71] = Mule

	TrafficCarPos[72] = <<1952.0720, 1761.2147, 64.8320>>
	TrafficCarQuatX[72] = -0.0270
	TrafficCarQuatY[72] = -0.0033
	TrafficCarQuatZ[72] = 0.1259
	TrafficCarQuatW[72] = 0.9917
	TrafficCarRecording[72] = 73
	TrafficCarStartime[72] = 32802.0000
	TrafficCarModel[72] = minivan

	TrafficCarPos[73] = <<1918.1613, 1753.9640, 66.4773>>
	TrafficCarQuatX[73] = -0.0044
	TrafficCarQuatY[73] = 0.0278
	TrafficCarQuatZ[73] = 0.9868
	TrafficCarQuatW[73] = -0.1597
	TrafficCarRecording[73] = 74
	TrafficCarStartime[73] = 32934.0000
	TrafficCarModel[73] = emperor

//	TrafficCarPos[74] = <<1947.2626, 1797.7180, 63.1835>>
//	TrafficCarQuatX[74] = -0.0251
//	TrafficCarQuatY[74] = -0.0033
//	TrafficCarQuatZ[74] = 0.1393
//	TrafficCarQuatW[74] = 0.9899
//	TrafficCarRecording[74] = 75
//	TrafficCarStartime[74] = 33462.0000
//	TrafficCarModel[74] = minivan

	TrafficCarPos[75] = <<1939.9493, 1804.6766, 62.5258>>
	TrafficCarQuatX[75] = -0.0236
	TrafficCarQuatY[75] = -0.0021
	TrafficCarQuatZ[75] = 0.0882
	TrafficCarQuatW[75] = 0.9958
	TrafficCarRecording[75] = 76
	TrafficCarStartime[75] = 33726.0000
	TrafficCarModel[75] = emperor

//	TrafficCarPos[76] = <<1938.7435, 1830.9276, 62.0945>>
//	TrafficCarQuatX[76] = -0.0206
//	TrafficCarQuatY[76] = -0.0026
//	TrafficCarQuatZ[76] = 0.1272
//	TrafficCarQuatW[76] = 0.9917
//	TrafficCarRecording[76] = 77
//	TrafficCarStartime[76] = 34122.0000
//	TrafficCarModel[76] = Mule

	TrafficCarPos[77] = <<1910.1921, 1924.5275, 57.8338>>
	TrafficCarQuatX[77] = -0.0158
	TrafficCarQuatY[77] = -0.0019
	TrafficCarQuatZ[77] = 0.1139
	TrafficCarQuatW[77] = 0.9934
	TrafficCarRecording[77] = 78
	TrafficCarStartime[77] = 35970.0000
	TrafficCarModel[77] = asterope

//	TrafficCarPos[78] = <<1909.1331, 1958.2784, 57.1949>>
//	TrafficCarQuatX[78] = -0.0143
//	TrafficCarQuatY[78] = -0.0021
//	TrafficCarQuatZ[78] = 0.0890
//	TrafficCarQuatW[78] = 0.9959
//	TrafficCarRecording[78] = 79
//	TrafficCarStartime[78] = 36564.0000
//	TrafficCarModel[78] = minivan

	TrafficCarPos[79] = <<1906.6332, 1942.2339, 57.3684>>
	TrafficCarQuatX[79] = -0.0149
	TrafficCarQuatY[79] = -0.0021
	TrafficCarQuatZ[79] = 0.1458
	TrafficCarQuatW[79] = 0.9892
	TrafficCarRecording[79] = 80
	TrafficCarStartime[79] = 36696.0000
	TrafficCarModel[79] = minivan

	TrafficCarPos[80] = <<1838.2122, 1987.8311, 56.5357>>
	TrafficCarQuatX[80] = -0.0010
	TrafficCarQuatY[80] = 0.0096
	TrafficCarQuatZ[80] = 0.9949
	TrafficCarQuatW[80] = -0.1009
	TrafficCarRecording[80] = 81
	TrafficCarStartime[80] = 37620.0000
	TrafficCarModel[80] = emperor

//	TrafficCarPos[81] = <<1891.1013, 2015.4900, 55.7017>>
//	TrafficCarQuatX[81] = -0.0106
//	TrafficCarQuatY[81] = -0.0011
//	TrafficCarQuatZ[81] = 0.1004
//	TrafficCarQuatW[81] = 0.9949
//	TrafficCarRecording[81] = 82
//	TrafficCarStartime[81] = 37686.0000
//	TrafficCarModel[81] = minivan

	TrafficCarPos[82] = <<1893.2684, 2037.7450, 55.7725>>
	TrafficCarQuatX[82] = -0.0090
	TrafficCarQuatY[82] = -0.0005
	TrafficCarQuatZ[82] = 0.0559
	TrafficCarQuatW[82] = 0.9984
	TrafficCarRecording[82] = 83
	TrafficCarStartime[82] = 38082.0000
	TrafficCarModel[82] = Mule

//	TrafficCarPos[83] = <<1888.2937, 2066.3066, 54.5632>>
//	TrafficCarQuatX[83] = -0.0075
//	TrafficCarQuatY[83] = -0.0008
//	TrafficCarQuatZ[83] = 0.1141
//	TrafficCarQuatW[83] = 0.9934
//	TrafficCarRecording[83] = 84
//	TrafficCarStartime[83] = 38610.0000
//	TrafficCarModel[83] = emperor

//	TrafficCarPos[84] = <<1884.1357, 2061.0784, 54.8804>>
//	TrafficCarQuatX[84] = -0.0076
//	TrafficCarQuatY[84] = -0.0022
//	TrafficCarQuatZ[84] = 0.1108
//	TrafficCarQuatW[84] = 0.9938
//	TrafficCarRecording[84] = 85
//	TrafficCarStartime[84] = 38940.0000
//	TrafficCarModel[84] = minivan

	TrafficCarPos[85] = <<1875.6396, 2133.1733, 53.9558>>
	TrafficCarQuatX[85] = -0.0011
	TrafficCarQuatY[85] = -0.0002
	TrafficCarQuatZ[85] = 0.0396
	TrafficCarQuatW[85] = 0.9992
	TrafficCarRecording[85] = 86
	TrafficCarStartime[85] = 39864.0000
	TrafficCarModel[85] = asterope

//	TrafficCarPos[86] = <<1880.5902, 2142.5889, 54.0465>>
//	TrafficCarQuatX[86] = -0.0000
//	TrafficCarQuatY[86] = 0.0003
//	TrafficCarQuatZ[86] = 0.0704
//	TrafficCarQuatW[86] = 0.9975
//	TrafficCarRecording[86] = 87
//	TrafficCarStartime[86] = 40062.0000
//	TrafficCarModel[86] = minivan

	TrafficCarPos[87] = <<1836.6387, 2125.5872, 70.6828>>
	TrafficCarQuatX[87] = -0.0345
	TrafficCarQuatY[87] = -0.0056
	TrafficCarQuatZ[87] = -0.0538
	TrafficCarQuatW[87] = 0.9979
	TrafficCarRecording[87] = 88
	TrafficCarStartime[87] = 40260.0000
	TrafficCarModel[87] = Mule

	TrafficCarPos[88] = <<1837.8618, 2177.1838, 66.2470>>
	TrafficCarQuatX[88] = -0.0378
	TrafficCarQuatY[88] = 0.0030
	TrafficCarQuatZ[88] = -0.0833
	TrafficCarQuatW[88] = 0.9958
	TrafficCarRecording[88] = 89
	TrafficCarStartime[88] = 40920.0000
	TrafficCarModel[88] = asterope

	TrafficCarPos[89] = <<1843.8552, 2182.2559, 65.5808>>
	TrafficCarQuatX[89] = -0.0391
	TrafficCarQuatY[89] = 0.0042
	TrafficCarQuatZ[89] = -0.1129
	TrafficCarQuatW[89] = 0.9928
	TrafficCarRecording[89] = 90
	TrafficCarStartime[89] = 41118.0000
	TrafficCarModel[89] = emperor

	TrafficCarPos[90] = <<1846.6605, 2198.6279, 64.5254>>
	TrafficCarQuatX[90] = -0.0382
	TrafficCarQuatY[90] = 0.0036
	TrafficCarQuatZ[90] = -0.1322
	TrafficCarQuatW[90] = 0.9905
	TrafficCarRecording[90] = 91
	TrafficCarStartime[90] = 41316.0000
	TrafficCarModel[90] = minivan

	TrafficCarPos[91] = <<1877.0269, 2216.1968, 54.8171>>
	TrafficCarQuatX[91] = -0.0001
	TrafficCarQuatY[91] = 0.0000
	TrafficCarQuatZ[91] = -0.0436
	TrafficCarQuatW[91] = 0.9991
	TrafficCarRecording[91] = 92
	TrafficCarStartime[91] = 41382.0000
	TrafficCarModel[91] = Mule

//	TrafficCarPos[92] = <<1883.6604, 2229.7732, 54.3213>>
//	TrafficCarQuatX[92] = -0.0007
//	TrafficCarQuatY[92] = -0.0014
//	TrafficCarQuatZ[92] = -0.0133
//	TrafficCarQuatW[92] = 0.9999
//	TrafficCarRecording[92] = 93
//	TrafficCarStartime[92] = 41646.0000
//	TrafficCarModel[92] = minivan

	TrafficCarPos[93] = <<1878.1968, 2232.2417, 54.0437>>
	TrafficCarQuatX[93] = -0.0000
	TrafficCarQuatY[93] = -0.0000
	TrafficCarQuatZ[93] = -0.0100
	TrafficCarQuatW[93] = 0.9999
	TrafficCarRecording[93] = 94
	TrafficCarStartime[93] = 42108.0000
	TrafficCarModel[93] = minivan

	TrafficCarPos[94] = <<1866.3765, 2284.5105, 58.3637>>
	TrafficCarQuatX[94] = -0.0299
	TrafficCarQuatY[94] = 0.0084
	TrafficCarQuatZ[94] = -0.0900
	TrafficCarQuatW[94] = 0.9955
	TrafficCarRecording[94] = 95
	TrafficCarStartime[94] = 42702.0000
	TrafficCarModel[94] = minivan

	TrafficCarPos[95] = <<1859.9976, 2279.6716, 58.5171>>
	TrafficCarQuatX[95] = -0.0314
	TrafficCarQuatY[95] = -0.0016
	TrafficCarQuatZ[95] = -0.0917
	TrafficCarQuatW[95] = 0.9953
	TrafficCarRecording[95] = 96
	TrafficCarStartime[95] = 43098.0000
	TrafficCarModel[95] = emperor

	TrafficCarPos[96] = <<1889.9025, 2301.9653, 54.3177>>
	TrafficCarQuatX[96] = -0.0000
	TrafficCarQuatY[96] = -0.0000
	TrafficCarQuatZ[96] = -0.0948
	TrafficCarQuatW[96] = 0.9955
	TrafficCarRecording[96] = 97
	TrafficCarStartime[96] = 43494.0000
	TrafficCarModel[96] = minivan

//	TrafficCarPos[97] = <<1909.4022, 2372.5049, 54.8150>>
//	TrafficCarQuatX[97] = -0.0005
//	TrafficCarQuatY[97] = 0.0000
//	TrafficCarQuatZ[97] = -0.1526
//	TrafficCarQuatW[97] = 0.9883
//	TrafficCarRecording[97] = 98
//	TrafficCarStartime[97] = 44418.0000
//	TrafficCarModel[97] = Mule

	TrafficCarPos[98] = <<1914.8075, 2371.7500, 54.0472>>
	TrafficCarQuatX[98] = -0.0000
	TrafficCarQuatY[98] = -0.0002
	TrafficCarQuatZ[98] = -0.1181
	TrafficCarQuatW[98] = 0.9930
	TrafficCarRecording[98] = 99
	TrafficCarStartime[98] = 44550.0000
	TrafficCarModel[98] = minivan

	TrafficCarPos[99] = <<1919.3309, 2385.4546, 54.0774>>
	TrafficCarQuatX[99] = -0.0003
	TrafficCarQuatY[99] = 0.0002
	TrafficCarQuatZ[99] = -0.1213
	TrafficCarQuatW[99] = 0.9926
	TrafficCarRecording[99] = 100
	TrafficCarStartime[99] = 45012.0000
	TrafficCarModel[99] = emperor

	TrafficCarPos[100] = <<1881.0796, 2420.8135, 53.9370>>
	TrafficCarQuatX[100] = -0.0009
	TrafficCarQuatY[100] = -0.0044
	TrafficCarQuatZ[100] = 0.9667
	TrafficCarQuatW[100] = 0.2560
	TrafficCarRecording[100] = 101
	TrafficCarStartime[100] = 45606.0000
	TrafficCarModel[100] = asterope

	TrafficCarPos[101] = <<1942.5637, 2449.5076, 53.9608>>
	TrafficCarQuatX[101] = 0.0000
	TrafficCarQuatY[101] = -0.0000
	TrafficCarQuatZ[101] = -0.2572
	TrafficCarQuatW[101] = 0.9664
	TrafficCarRecording[101] = 102
	TrafficCarStartime[101] = 45936.0000
	TrafficCarModel[101] = asterope

	TrafficCarPos[102] = <<1919.2661, 2471.7690, 55.0404>>
	TrafficCarQuatX[102] = -0.0002
	TrafficCarQuatY[102] = -0.0007
	TrafficCarQuatZ[102] = 0.9583
	TrafficCarQuatW[102] = 0.2858
	TrafficCarRecording[102] = 103
	TrafficCarStartime[102] = 46332.0000
	TrafficCarModel[102] = Mule

	TrafficCarPos[103] = <<1959.3350, 2493.4661, 54.0472>>
	TrafficCarQuatX[103] = -0.0000
	TrafficCarQuatY[103] = -0.0002
	TrafficCarQuatZ[103] = -0.2531
	TrafficCarQuatW[103] = 0.9674
	TrafficCarRecording[103] = 104
	TrafficCarStartime[103] = 46860.0000
	TrafficCarModel[103] = minivan

	TrafficCarPos[104] = <<1922.3114, 2504.7466, 54.3150>>
	TrafficCarQuatX[104] = 0.0003
	TrafficCarQuatY[104] = -0.0003
	TrafficCarQuatZ[104] = 0.9483
	TrafficCarQuatW[104] = 0.3174
	TrafficCarRecording[104] = 105
	TrafficCarStartime[104] = 46926.0000
	TrafficCarModel[104] = minivan

	TrafficCarPos[105] = <<1923.5302, 2514.6523, 54.2393>>
	TrafficCarQuatX[105] = -0.0009
	TrafficCarQuatY[105] = -0.0014
	TrafficCarQuatZ[105] = 0.9517
	TrafficCarQuatW[105] = 0.3071
	TrafficCarRecording[105] = 106
	TrafficCarStartime[105] = 47124.0000
	TrafficCarModel[105] = asterope

	TrafficCarPos[106] = <<1973.2681, 2514.6934, 54.3217>>
	TrafficCarQuatX[106] = 0.0003
	TrafficCarQuatY[106] = -0.0001
	TrafficCarQuatZ[106] = -0.3002
	TrafficCarQuatW[106] = 0.9539
	TrafficCarRecording[106] = 107
	TrafficCarStartime[106] = 47322.0000
	TrafficCarModel[106] = minivan

	TrafficCarPos[107] = <<1980.1942, 2510.0784, 54.7849>>
	TrafficCarQuatX[107] = -0.0006
	TrafficCarQuatY[107] = -0.0000
	TrafficCarQuatZ[107] = -0.3186
	TrafficCarQuatW[107] = 0.9479
	TrafficCarRecording[107] = 108
	TrafficCarStartime[107] = 47586.0000
	TrafficCarModel[107] = Mule

	TrafficCarPos[108] = <<1927.3007, 2511.6277, 54.0738>>
	TrafficCarQuatX[108] = -0.0006
	TrafficCarQuatY[108] = -0.0019
	TrafficCarQuatZ[108] = 0.9542
	TrafficCarQuatW[108] = 0.2992
	TrafficCarRecording[108] = 109
	TrafficCarStartime[108] = 47718.0000
	TrafficCarModel[108] = emperor

	TrafficCarPos[109] = <<1978.6078, 2570.1250, 54.0438>>
	TrafficCarQuatX[109] = 0.0003
	TrafficCarQuatY[109] = 0.0009
	TrafficCarQuatZ[109] = 0.9199
	TrafficCarQuatW[109] = 0.3921
	TrafficCarRecording[109] = 110
	TrafficCarStartime[109] = 48444.0000
	TrafficCarModel[109] = emperor

	TrafficCarPos[110] = <<1978.6371, 2577.6123, 54.2875>>
	TrafficCarQuatX[110] = 0.0017
	TrafficCarQuatY[110] = 0.0005
	TrafficCarQuatZ[110] = 0.9266
	TrafficCarQuatW[110] = 0.3761
	TrafficCarRecording[110] = 111
	TrafficCarStartime[110] = 48510.0000
	TrafficCarModel[110] = minivan

	TrafficCarPos[111] = <<2025.8209, 2557.6570, 54.3789>>
	TrafficCarQuatX[111] = -0.0047
	TrafficCarQuatY[111] = -0.0141
	TrafficCarQuatZ[111] = -0.3716
	TrafficCarQuatW[111] = 0.9283
	TrafficCarRecording[111] = 112
	TrafficCarStartime[111] = 48708.0000
	TrafficCarModel[111] = minivan

	TrafficCarPos[112] = <<2044.8674, 2576.5210, 53.6568>>
	TrafficCarQuatX[112] = -0.0123
	TrafficCarQuatY[112] = 0.0049
	TrafficCarQuatZ[112] = -0.4343
	TrafficCarQuatW[112] = 0.9007
	TrafficCarRecording[112] = 113
	TrafficCarStartime[112] = 49104.0000
	TrafficCarModel[112] = asterope

	TrafficCarPos[113] = <<2035.2405, 2575.0334, 53.9560>>
	TrafficCarQuatX[113] = -0.0042
	TrafficCarQuatY[113] = 0.0054
	TrafficCarQuatZ[113] = -0.3938
	TrafficCarQuatW[113] = 0.9192
	TrafficCarRecording[113] = 114
	TrafficCarStartime[113] = 49302.0000
	TrafficCarModel[113] = minivan

	TrafficCarPos[114] = <<2034.0126, 2613.8088, 53.9144>>
	TrafficCarQuatX[114] = -0.0026
	TrafficCarQuatY[114] = 0.0158
	TrafficCarQuatZ[114] = 0.9120
	TrafficCarQuatW[114] = 0.4100
	TrafficCarRecording[114] = 115
	TrafficCarStartime[114] = 49632.0000
	TrafficCarModel[114] = Mule

	TrafficCarPos[115] = <<2082.6130, 2618.1206, 52.2736>>
	TrafficCarQuatX[115] = -0.0150
	TrafficCarQuatY[115] = 0.0061
	TrafficCarQuatZ[115] = -0.4216
	TrafficCarQuatW[115] = 0.9066
	TrafficCarRecording[115] = 116
	TrafficCarStartime[115] = 50226.0000
	TrafficCarModel[115] = minivan

	TrafficCarPos[116] = <<2074.0278, 2642.6331, 51.7169>>
	TrafficCarQuatX[116] = 0.0077
	TrafficCarQuatY[116] = 0.0159
	TrafficCarQuatZ[116] = 0.9037
	TrafficCarQuatW[116] = 0.4278
	TrafficCarRecording[116] = 117
	TrafficCarStartime[116] = 50556.0000
	TrafficCarModel[116] = emperor

	TrafficCarPos[117] = <<2100.8926, 2629.0205, 51.3350>>
	TrafficCarQuatX[117] = -0.0147
	TrafficCarQuatY[117] = 0.0073
	TrafficCarQuatZ[117] = -0.2968
	TrafficCarQuatW[117] = 0.9548
	TrafficCarRecording[117] = 118
	TrafficCarStartime[117] = 51150.0000
	TrafficCarModel[117] = minivan

	TrafficCarPos[118] = <<2102.9087, 2675.5833, 50.9682>>
	TrafficCarQuatX[118] = 0.0123
	TrafficCarQuatY[118] = 0.0138
	TrafficCarQuatZ[118] = 0.9176
	TrafficCarQuatW[118] = 0.3970
	TrafficCarRecording[118] = 119
	TrafficCarStartime[118] = 51348.0000
	TrafficCarModel[118] = Mule

	TrafficCarPos[119] = <<2096.3523, 2662.0562, 50.7263>>
	TrafficCarQuatX[119] = 0.0060
	TrafficCarQuatY[119] = 0.0159
	TrafficCarQuatZ[119] = 0.9315
	TrafficCarQuatW[119] = 0.3634
	TrafficCarRecording[119] = 120
	TrafficCarStartime[119] = 51546.0000
	TrafficCarModel[119] = asterope

	TrafficCarPos[120] = <<2139.4839, 2709.6846, 48.5264>>
	TrafficCarQuatX[120] = 0.0083
	TrafficCarQuatY[120] = 0.0169
	TrafficCarQuatZ[120] = 0.9252
	TrafficCarQuatW[120] = 0.3790
	TrafficCarRecording[120] = 121
	TrafficCarStartime[120] = 52338.0000
	TrafficCarModel[120] = asterope

	TrafficCarPos[121] = <<2158.1047, 2682.1785, 48.5176>>
	TrafficCarQuatX[121] = -0.0233
	TrafficCarQuatY[121] = -0.0056
	TrafficCarQuatZ[121] = -0.4217
	TrafficCarQuatW[121] = 0.9064
	TrafficCarRecording[121] = 122
	TrafficCarStartime[121] = 52338.0000
	TrafficCarModel[121] = emperor

	TrafficCarPos[122] = <<2150.6326, 2676.2463, 49.1083>>
	TrafficCarQuatX[122] = -0.0175
	TrafficCarQuatY[122] = 0.0035
	TrafficCarQuatZ[122] = -0.3898
	TrafficCarQuatW[122] = 0.9207
	TrafficCarRecording[122] = 123
	TrafficCarStartime[122] = 52536.0000
	TrafficCarModel[122] = minivan

	TrafficCarPos[123] = <<2157.3115, 2720.8997, 47.7763>>
	TrafficCarQuatX[123] = 0.0129
	TrafficCarQuatY[123] = 0.0152
	TrafficCarQuatZ[123] = 0.9163
	TrafficCarQuatW[123] = 0.4000
	TrafficCarRecording[123] = 124
	TrafficCarStartime[123] = 52668.0000
	TrafficCarModel[123] = minivan

	TrafficCarPos[124] = <<2161.8943, 2732.9780, 47.1336>>
	TrafficCarQuatX[124] = 0.0040
	TrafficCarQuatY[124] = 0.0190
	TrafficCarQuatZ[124] = 0.9056
	TrafficCarQuatW[124] = 0.4237
	TrafficCarRecording[124] = 125
	TrafficCarStartime[124] = 52998.0000
	TrafficCarModel[124] = asterope

	TrafficCarPos[125] = <<2165.2139, 2735.3638, 46.9517>>
	TrafficCarQuatX[125] = 0.0054
	TrafficCarQuatY[125] = 0.0190
	TrafficCarQuatZ[125] = 0.9231
	TrafficCarQuatW[125] = 0.3839
	TrafficCarRecording[125] = 126
	TrafficCarStartime[125] = 53328.0000
	TrafficCarModel[125] = emperor

	TrafficCarPos[126] = <<2168.5723, 2731.1460, 47.1710>>
	TrafficCarQuatX[126] = 0.0160
	TrafficCarQuatY[126] = 0.0146
	TrafficCarQuatZ[126] = 0.9366
	TrafficCarQuatW[126] = 0.3497
	TrafficCarRecording[126] = 127
	TrafficCarStartime[126] = 53460.0000
	TrafficCarModel[126] = minivan

//	TrafficCarPos[127] = <<2204.8955, 2736.4832, 45.7908>>
//	TrafficCarQuatX[127] = -0.0174
//	TrafficCarQuatY[127] = 0.0086
//	TrafficCarQuatZ[127] = -0.4340
//	TrafficCarQuatW[127] = 0.9007
//	TrafficCarRecording[127] = 128
//	TrafficCarStartime[127] = 53724.0000
//	TrafficCarModel[127] = asterope

	TrafficCarPos[128] = <<2204.3203, 2729.6499, 46.0701>>
	TrafficCarQuatX[128] = -0.0151
	TrafficCarQuatY[128] = 0.0134
	TrafficCarQuatZ[128] = -0.4697
	TrafficCarQuatW[128] = 0.8826
	TrafficCarRecording[128] = 129
	TrafficCarStartime[128] = 53856.0000
	TrafficCarModel[128] = asterope

	TrafficCarPos[129] = <<2223.9238, 2745.0034, 44.8964>>
	TrafficCarQuatX[129] = -0.0148
	TrafficCarQuatY[129] = 0.0123
	TrafficCarQuatZ[129] = -0.4397
	TrafficCarQuatW[129] = 0.8980
	TrafficCarRecording[129] = 130
	TrafficCarStartime[129] = 54252.0000
	TrafficCarModel[129] = minivan

	TrafficCarPos[130] = <<2235.4163, 2785.0027, 43.9190>>
	TrafficCarQuatX[130] = 0.0114
	TrafficCarQuatY[130] = 0.0146
	TrafficCarQuatZ[130] = 0.9048
	TrafficCarQuatW[130] = 0.4254
	TrafficCarRecording[130] = 131
	TrafficCarStartime[130] = 54516.0000
	TrafficCarModel[130] = minivan

//	TrafficCarPos[131] = <<2255.6169, 2777.0356, 43.2626>>
//	TrafficCarQuatX[131] = -0.0177
//	TrafficCarQuatY[131] = 0.0032
//	TrafficCarQuatZ[131] = -0.4113
//	TrafficCarQuatW[131] = 0.9113
//	TrafficCarRecording[131] = 132
//	TrafficCarStartime[131] = 54780.0000
//	TrafficCarModel[131] = minivan

	TrafficCarPos[132] = <<2245.4656, 2799.8049, 43.1371>>
	TrafficCarQuatX[132] = 0.0068
	TrafficCarQuatY[132] = 0.0152
	TrafficCarQuatZ[132] = 0.9105
	TrafficCarQuatW[132] = 0.4132
	TrafficCarRecording[132] = 133
	TrafficCarStartime[132] = 54912.0000
	TrafficCarModel[132] = asterope

	TrafficCarPos[133] = <<2266.8672, 2784.8850, 42.9903>>
	TrafficCarQuatX[133] = -0.0177
	TrafficCarQuatY[133] = 0.0036
	TrafficCarQuatZ[133] = -0.4714
	TrafficCarQuatW[133] = 0.8817
	TrafficCarRecording[133] = 134
	TrafficCarStartime[133] = 55374.0000
	TrafficCarModel[133] = asterope

	TrafficCarPos[134] = <<2283.8840, 2797.6458, 42.0955>>
	TrafficCarQuatX[134] = -0.0178
	TrafficCarQuatY[134] = -0.0002
	TrafficCarQuatZ[134] = -0.4409
	TrafficCarQuatW[134] = 0.8974
	TrafficCarRecording[134] = 135
	TrafficCarStartime[134] = 55506.0000
	TrafficCarModel[134] = emperor

	TrafficCarPos[135] = <<2266.4207, 2815.7029, 42.4122>>
	TrafficCarQuatX[135] = 0.0006
	TrafficCarQuatY[135] = 0.0160
	TrafficCarQuatZ[135] = 0.8986
	TrafficCarQuatW[135] = 0.4385
	TrafficCarRecording[135] = 136
	TrafficCarStartime[135] = 55638.0000
	TrafficCarModel[135] = minivan

	TrafficCarPos[136] = <<2278.1692, 2793.7195, 42.5558>>
	TrafficCarQuatX[136] = -0.0181
	TrafficCarQuatY[136] = -0.0022
	TrafficCarQuatZ[136] = -0.4076
	TrafficCarQuatW[136] = 0.9130
	TrafficCarRecording[136] = 137
	TrafficCarStartime[136] = 55770.0000
	TrafficCarModel[136] = minivan

	TrafficCarPos[137] = <<2288.3352, 2826.1553, 41.7135>>
	TrafficCarQuatX[137] = 0.0093
	TrafficCarQuatY[137] = 0.0108
	TrafficCarQuatZ[137] = 0.8790
	TrafficCarQuatW[137] = 0.4765
	TrafficCarRecording[137] = 138
	TrafficCarStartime[137] = 55968.0000
	TrafficCarModel[137] = minivan

	TrafficCarPos[138] = <<2307.5479, 2840.0693, 40.9374>>
	TrafficCarQuatX[138] = 0.0082
	TrafficCarQuatY[138] = 0.0077
	TrafficCarQuatZ[138] = 0.9129
	TrafficCarQuatW[138] = 0.4081
	TrafficCarRecording[138] = 139
	TrafficCarStartime[138] = 56364.0000
	TrafficCarModel[138] = asterope

	TrafficCarPos[139] = <<2347.1609, 2876.9775, 40.2483>>
	TrafficCarQuatX[139] = -0.0009
	TrafficCarQuatY[139] = 0.0054
	TrafficCarQuatZ[139] = 0.9061
	TrafficCarQuatW[139] = 0.4231
	TrafficCarRecording[139] = 140
	TrafficCarStartime[139] = 57420.0000
	TrafficCarModel[139] = minivan

	TrafficCarPos[140] = <<2353.2888, 2843.3313, 40.1427>>
	TrafficCarQuatX[140] = -0.0013
	TrafficCarQuatY[140] = 0.0130
	TrafficCarQuatZ[140] = -0.4505
	TrafficCarQuatW[140] = 0.8927
	TrafficCarRecording[140] = 141
	TrafficCarStartime[140] = 57420.0000
	TrafficCarModel[140] = emperor

	TrafficCarPos[141] = <<2345.4990, 2845.3132, 40.1960>>
	TrafficCarQuatX[141] = -0.0105
	TrafficCarQuatY[141] = -0.0046
	TrafficCarQuatZ[141] = -0.4457
	TrafficCarQuatW[141] = 0.8951
	TrafficCarRecording[141] = 142
	TrafficCarStartime[141] = 58080.0000
	TrafficCarModel[141] = minivan

	TrafficCarPos[142] = <<2393.7900, 2875.5859, 39.6648>>
	TrafficCarQuatX[142] = 0.0016
	TrafficCarQuatY[142] = 0.0069
	TrafficCarQuatZ[142] = -0.4279
	TrafficCarQuatW[142] = 0.9038
	TrafficCarRecording[142] = 143
	TrafficCarStartime[142] = 58146.0000
	TrafficCarModel[142] = minivan

	TrafficCarPos[143] = <<2376.0623, 2899.5327, 39.7828>>
	TrafficCarQuatX[143] = -0.0036
	TrafficCarQuatY[143] = 0.0024
	TrafficCarQuatZ[143] = 0.9093
	TrafficCarQuatW[143] = 0.4160
	TrafficCarRecording[143] = 144
	TrafficCarStartime[143] = 58212.0000
	TrafficCarModel[143] = emperor

	TrafficCarPos[144] = <<2431.8914, 2860.4575, 48.4824>>
	TrafficCarQuatX[144] = 0.0078
	TrafficCarQuatY[144] = 0.0040
	TrafficCarQuatZ[144] = -0.4613
	TrafficCarQuatW[144] = 0.8872
	TrafficCarRecording[144] = 145
	TrafficCarStartime[144] = 58476.0000
	TrafficCarModel[144] = emperor

	TrafficCarPos[145] = <<2437.6814, 2853.6606, 48.6681>>
	TrafficCarQuatX[145] = -0.0023
	TrafficCarQuatY[145] = 0.0016
	TrafficCarQuatZ[145] = 0.7530
	TrafficCarQuatW[145] = -0.6581
	TrafficCarRecording[145] = 146
	TrafficCarStartime[145] = 58542.0000
	TrafficCarModel[145] = minivan

//	TrafficCarPos[146] = <<2424.0815, 2899.4736, 40.0331>>
//	TrafficCarQuatX[146] = 0.0047
//	TrafficCarQuatY[146] = 0.0064
//	TrafficCarQuatZ[146] = -0.4331
//	TrafficCarQuatW[146] = 0.9013
//	TrafficCarRecording[146] = 147
//	TrafficCarStartime[146] = 58806.0000
//	TrafficCarModel[146] = minivan
//
//	TrafficCarPos[147] = <<2429.8965, 2911.9617, 39.8643>>
//	TrafficCarQuatX[147] = -0.0006
//	TrafficCarQuatY[147] = -0.0049
//	TrafficCarQuatZ[147] = -0.4606
//	TrafficCarQuatW[147] = 0.8876
//	TrafficCarRecording[147] = 148
//	TrafficCarStartime[147] = 59070.0000
//	TrafficCarModel[147] = emperor

	TrafficCarPos[148] = <<2423.1223, 2899.4880, 39.7634>>
	TrafficCarQuatX[148] = 0.0048
	TrafficCarQuatY[148] = 0.0070
	TrafficCarQuatZ[148] = -0.3871
	TrafficCarQuatW[148] = 0.9220
	TrafficCarRecording[148] = 149
	TrafficCarStartime[148] = 59334.0000
	TrafficCarModel[148] = minivan

	TrafficCarPos[149] = <<2443.4436, 2922.7126, 40.1022>>
	TrafficCarQuatX[149] = -0.0115
	TrafficCarQuatY[149] = -0.0064
	TrafficCarQuatZ[149] = -0.3552
	TrafficCarQuatW[149] = 0.9347
	TrafficCarRecording[149] = 150
	TrafficCarStartime[149] = 59598.0000
	TrafficCarModel[149] = asterope

	TrafficCarPos[150] = <<2461.1191, 2971.0920, 40.4965>>
	TrafficCarQuatX[150] = -0.0027
	TrafficCarQuatY[150] = -0.0056
	TrafficCarQuatZ[150] = 0.9087
	TrafficCarQuatW[150] = 0.4173
	TrafficCarRecording[150] = 151
	TrafficCarStartime[150] = 60192.0000
	TrafficCarModel[150] = asterope

//	TrafficCarPos[151] = <<2500.0269, 2965.3242, 40.9798>>
//	TrafficCarQuatX[151] = 0.0070
//	TrafficCarQuatY[151] = -0.0001
//	TrafficCarQuatZ[151] = -0.4009
//	TrafficCarQuatW[151] = 0.9161
//	TrafficCarRecording[151] = 152
//	TrafficCarStartime[151] = 60654.0000
//	TrafficCarModel[151] = minivan

	TrafficCarPos[152] = <<2478.9807, 2986.5122, 40.8035>>
	TrafficCarQuatX[152] = -0.0028
	TrafficCarQuatY[152] = -0.0060
	TrafficCarQuatZ[152] = 0.9128
	TrafficCarQuatW[152] = 0.4083
	TrafficCarRecording[152] = 153
	TrafficCarStartime[152] = 60786.0000
	TrafficCarModel[152] = asterope

	TrafficCarPos[153] = <<2526.0459, 2987.5046, 41.3395>>
	TrafficCarQuatX[153] = 0.0115
	TrafficCarQuatY[153] = -0.0008
	TrafficCarQuatZ[153] = -0.4057
	TrafficCarQuatW[153] = 0.9139
	TrafficCarRecording[153] = 154
	TrafficCarStartime[153] = 61380.0000
	TrafficCarModel[153] = asterope

	TrafficCarPos[154] = <<2541.4512, 3001.8245, 41.8341>>
	TrafficCarQuatX[154] = 0.0160
	TrafficCarQuatY[154] = -0.0028
	TrafficCarQuatZ[154] = -0.4384
	TrafficCarQuatW[154] = 0.8986
	TrafficCarRecording[154] = 155
	TrafficCarStartime[154] = 61710.0000
	TrafficCarModel[154] = minivan

//	TrafficCarPos[155] = <<2546.9021, 3015.0061, 42.5364>>
//	TrafficCarQuatX[155] = 0.0149
//	TrafficCarQuatY[155] = -0.0086
//	TrafficCarQuatZ[155] = -0.3904
//	TrafficCarQuatW[155] = 0.9205
//	TrafficCarRecording[155] = 156
//	TrafficCarStartime[155] = 61908.0000
//	TrafficCarModel[155] = minivan

	TrafficCarPos[156] = <<2488.6902, 2995.3376, 40.9617>>
	TrafficCarQuatX[156] = -0.0026
	TrafficCarQuatY[156] = -0.0060
	TrafficCarQuatZ[156] = 0.9098
	TrafficCarQuatW[156] = 0.4149
	TrafficCarRecording[156] = 157
	TrafficCarStartime[156] = 61974.0000
	TrafficCarModel[156] = asterope

//	TrafficCarPos[157] = <<2545.3433, 3012.3589, 42.1836>>
//	TrafficCarQuatX[157] = 0.0123
//	TrafficCarQuatY[157] = -0.0105
//	TrafficCarQuatZ[157] = -0.4339
//	TrafficCarQuatW[157] = 0.9008
//	TrafficCarRecording[157] = 158
//	TrafficCarStartime[157] = 62304.0000
//	TrafficCarModel[157] = emperor

	TrafficCarPos[158] = <<2592.3665, 3056.7212, 45.2098>>
	TrafficCarQuatX[158] = 0.0221
	TrafficCarQuatY[158] = -0.0095
	TrafficCarQuatZ[158] = -0.3915
	TrafficCarQuatW[158] = 0.9199
	TrafficCarRecording[158] = 159
	TrafficCarStartime[158] = 63096.0000
	TrafficCarModel[158] = minivan

	TrafficCarPos[159] = <<2622.8523, 3091.0662, 47.3566>>
	TrafficCarQuatX[159] = 0.0239
	TrafficCarQuatY[159] = -0.0086
	TrafficCarQuatZ[159] = -0.3428
	TrafficCarQuatW[159] = 0.9391
	TrafficCarRecording[159] = 160
	TrafficCarStartime[159] = 64020.0000
	TrafficCarModel[159] = asterope

	TrafficCarPos[160] = <<2610.3125, 3112.6821, 47.6941>>
	TrafficCarQuatX[160] = -0.0083
	TrafficCarQuatY[160] = -0.0242
	TrafficCarQuatZ[160] = 0.9453
	TrafficCarQuatW[160] = 0.3251
	TrafficCarRecording[160] = 161
	TrafficCarStartime[160] = 64152.0000
	TrafficCarModel[160] = emperor

	TrafficCarPos[161] = <<2646.1172, 3116.8943, 49.0079>>
	TrafficCarQuatX[161] = 0.0228
	TrafficCarQuatY[161] = -0.0081
	TrafficCarQuatZ[161] = -0.3662
	TrafficCarQuatW[161] = 0.9302
	TrafficCarRecording[161] = 162
	TrafficCarStartime[161] = 64680.0000
	TrafficCarModel[161] = minivan

	TrafficCarPos[162] = <<2637.1482, 3137.2964, 49.7730>>
	TrafficCarQuatX[162] = -0.0085
	TrafficCarQuatY[162] = -0.0225
	TrafficCarQuatZ[162] = 0.9470
	TrafficCarQuatW[162] = 0.3205
	TrafficCarRecording[162] = 163
	TrafficCarStartime[162] = 64878.0000
	TrafficCarModel[162] = minivan

//	TrafficCarPos[163] = <<2661.3721, 3128.3782, 50.1556>>
//	TrafficCarQuatX[163] = 0.0220
//	TrafficCarQuatY[163] = -0.0072
//	TrafficCarQuatZ[163] = -0.3245
//	TrafficCarQuatW[163] = 0.9456
//	TrafficCarRecording[163] = 164
//	TrafficCarStartime[163] = 65076.0000
//	TrafficCarModel[163] = minivan

//	TrafficCarPos[164] = <<2645.6206, 3108.5300, 48.6881>>
//	TrafficCarQuatX[164] = 0.0241
//	TrafficCarQuatY[164] = -0.0044
//	TrafficCarQuatZ[164] = -0.3356
//	TrafficCarQuatW[164] = 0.9417
//	TrafficCarRecording[164] = 165
//	TrafficCarStartime[164] = 65274.0000
//	TrafficCarModel[164] = emperor

	TrafficCarPos[165] = <<2665.6265, 3175.4021, 51.7303>>
	TrafficCarQuatX[165] = -0.0060
	TrafficCarQuatY[165] = -0.0194
	TrafficCarQuatZ[165] = 0.9567
	TrafficCarQuatW[165] = 0.2902
	TrafficCarRecording[165] = 166
	TrafficCarStartime[165] = 65934.0000
	TrafficCarModel[165] = asterope

	TrafficCarPos[166] = <<2668.3545, 3180.0718, 51.9595>>
	TrafficCarQuatX[166] = -0.0054
	TrafficCarQuatY[166] = -0.0191
	TrafficCarQuatZ[166] = 0.9583
	TrafficCarQuatW[166] = 0.2850
	TrafficCarRecording[166] = 167
	TrafficCarStartime[166] = 66396.0000
	TrafficCarModel[166] = asterope

	TrafficCarPos[167] = <<2704.1785, 3199.5823, 53.4408>>
	TrafficCarQuatX[167] = 0.0155
	TrafficCarQuatY[167] = -0.0043
	TrafficCarQuatZ[167] = -0.2745
	TrafficCarQuatW[167] = 0.9615
	TrafficCarRecording[167] = 168
	TrafficCarStartime[167] = 66792.0000
	TrafficCarModel[167] = minivan

	TrafficCarPos[168] = <<2696.9309, 3187.3430, 52.7233>>
	TrafficCarQuatX[168] = 0.0165
	TrafficCarQuatY[168] = -0.0042
	TrafficCarQuatZ[168] = -0.2499
	TrafficCarQuatW[168] = 0.9681
	TrafficCarRecording[168] = 169
	TrafficCarStartime[168] = 66990.0000
	TrafficCarModel[168] = emperor

	TrafficCarPos[169] = <<2745.0408, 3260.3135, 55.0390>>
	TrafficCarQuatX[169] = 0.0089
	TrafficCarQuatY[169] = 0.0007
	TrafficCarQuatZ[169] = -0.2473
	TrafficCarQuatW[169] = 0.9689
	TrafficCarRecording[169] = 170
	TrafficCarStartime[169] = 68112.0000
	TrafficCarModel[169] = asterope

	TrafficCarPos[170] = <<2766.0090, 3302.2466, 55.5060>>
	TrafficCarQuatX[170] = 0.0044
	TrafficCarQuatY[170] = -0.0010
	TrafficCarQuatZ[170] = -0.1799
	TrafficCarQuatW[170] = 0.9837
	TrafficCarRecording[170] = 171
	TrafficCarStartime[170] = 69234.0000
	TrafficCarModel[170] = minivan

	TrafficCarPos[171] = <<2773.7832, 3320.9487, 55.8819>>
	TrafficCarQuatX[171] = 0.0016
	TrafficCarQuatY[171] = -0.0002
	TrafficCarQuatZ[171] = -0.2109
	TrafficCarQuatW[171] = 0.9775
	TrafficCarRecording[171] = 172
	TrafficCarStartime[171] = 69498.0000
	TrafficCarModel[171] = minivan

//	TrafficCarPos[172] = <<2766.6729, 3316.8103, 55.6176>>
//	TrafficCarQuatX[172] = 0.0019
//	TrafficCarQuatY[172] = -0.0005
//	TrafficCarQuatZ[172] = -0.2454
//	TrafficCarQuatW[172] = 0.9694
//	TrafficCarRecording[172] = 173
//	TrafficCarStartime[172] = 69762.0000
//	TrafficCarModel[172] = emperor

//	TrafficCarPos[173] = <<2782.7380, 3339.6160, 55.7284>>
//	TrafficCarQuatX[173] = 0.0000
//	TrafficCarQuatY[173] = 0.0002
//	TrafficCarQuatZ[173] = -0.1704
//	TrafficCarQuatW[173] = 0.9854
//	TrafficCarRecording[173] = 174
//	TrafficCarStartime[173] = 70224.0000
//	TrafficCarModel[173] = asterope

	TrafficCarPos[174] = <<2806.5112, 3409.5461, 55.3729>>
	TrafficCarQuatX[174] = -0.0039
	TrafficCarQuatY[174] = 0.0009
	TrafficCarQuatZ[174] = -0.2034
	TrafficCarQuatW[174] = 0.9791
	TrafficCarRecording[174] = 175
	TrafficCarStartime[174] = 71412.0000
	TrafficCarModel[174] = asterope

//	TrafficCarPos[175] = <<2812.9497, 3408.1135, 55.5505>>
//	TrafficCarQuatX[175] = -0.0038
//	TrafficCarQuatY[175] = 0.0009
//	TrafficCarQuatZ[175] = -0.2035
//	TrafficCarQuatW[175] = 0.9791
//	TrafficCarRecording[175] = 176
//	TrafficCarStartime[175] = 71412.0000
//	TrafficCarModel[175] = minivan

	TrafficCarPos[176] = <<2812.2185, 3421.8411, 55.2114>>
	TrafficCarQuatX[176] = -0.0040
	TrafficCarQuatY[176] = 0.0008
	TrafficCarQuatZ[176] = -0.1992
	TrafficCarQuatW[176] = 0.9799
	TrafficCarRecording[176] = 177
	TrafficCarStartime[176] = 71676.0000
	TrafficCarModel[176] = emperor

	TrafficCarPos[177] = <<2789.7988, 3433.5957, 55.2778>>
	TrafficCarQuatX[177] = 0.0014
	TrafficCarQuatY[177] = 0.0052
	TrafficCarQuatZ[177] = 0.9741
	TrafficCarQuatW[177] = 0.2262
	TrafficCarRecording[177] = 178
	TrafficCarStartime[177] = 71742.0000
	TrafficCarModel[177] = asterope

	TrafficCarPos[178] = <<2821.1392, 3430.9651, 55.0884>>
	TrafficCarQuatX[178] = -0.0039
	TrafficCarQuatY[178] = 0.0004
	TrafficCarQuatZ[178] = -0.1594
	TrafficCarQuatW[178] = 0.9872
	TrafficCarRecording[178] = 179
	TrafficCarStartime[178] = 71940.0000
	TrafficCarModel[178] = minivan

	TrafficCarPos[179] = <<2814.3210, 3478.4307, 54.8139>>
	TrafficCarQuatX[179] = 0.0019
	TrafficCarQuatY[179] = 0.0068
	TrafficCarQuatZ[179] = 0.9771
	TrafficCarQuatW[179] = 0.2126
	TrafficCarRecording[179] = 180
	TrafficCarStartime[179] = 72798.0000
	TrafficCarModel[179] = minivan

	TrafficCarPos[180] = <<2803.9612, 3466.0779, 54.7513>>
	TrafficCarQuatX[180] = 0.0016
	TrafficCarQuatY[180] = 0.0066
	TrafficCarQuatZ[180] = 0.9723
	TrafficCarQuatW[180] = 0.2335
	TrafficCarRecording[180] = 181
	TrafficCarStartime[180] = 73128.0000
	TrafficCarModel[180] = minivan

	TrafficCarPos[181] = <<2847.7361, 3505.2705, 54.2615>>
	TrafficCarQuatX[181] = -0.0076
	TrafficCarQuatY[181] = 0.0013
	TrafficCarQuatZ[181] = -0.1921
	TrafficCarQuatW[181] = 0.9814
	TrafficCarRecording[181] = 182
	TrafficCarStartime[181] = 73854.0000
	TrafficCarModel[181] = minivan

//	TrafficCarPos[182] = <<2857.5210, 3516.4604, 53.7994>>
//	TrafficCarQuatX[182] = -0.0078
//	TrafficCarQuatY[182] = 0.0016
//	TrafficCarQuatZ[182] = -0.1957
//	TrafficCarQuatW[182] = 0.9806
//	TrafficCarRecording[182] = 183
//	TrafficCarStartime[182] = 74250.0000
//	TrafficCarModel[182] = emperor

	TrafficCarPos[183] = <<2877.0220, 3588.1187, 52.8030>>
	TrafficCarQuatX[183] = -0.0076
	TrafficCarQuatY[183] = 0.0011
	TrafficCarQuatZ[183] = -0.1616
	TrafficCarQuatW[183] = 0.9868
	TrafficCarRecording[183] = 184
	TrafficCarStartime[183] = 75240.0000
	TrafficCarModel[183] = minivan

	TrafficCarPos[184] = <<2879.7188, 3577.6648, 52.7755>>
	TrafficCarQuatX[184] = -0.0077
	TrafficCarQuatY[184] = 0.0015
	TrafficCarQuatZ[184] = -0.1889
	TrafficCarQuatW[184] = 0.9820
	TrafficCarRecording[184] = 185
	TrafficCarStartime[184] = 75570.0000
	TrafficCarModel[184] = asterope

	TrafficCarPos[185] = <<2865.0088, 3612.8206, 52.2994>>
	TrafficCarQuatX[185] = 0.0008
	TrafficCarQuatY[185] = 0.0041
	TrafficCarQuatZ[185] = 0.9899
	TrafficCarQuatW[185] = 0.1414
	TrafficCarRecording[185] = 186
	TrafficCarStartime[185] = 75636.0000
	TrafficCarModel[185] = asterope

	TrafficCarPos[186] = <<2858.7224, 3611.3376, 52.5330>>
	TrafficCarQuatX[186] = -0.0042
	TrafficCarQuatY[186] = 0.0082
	TrafficCarQuatZ[186] = 0.9904
	TrafficCarQuatW[186] = 0.1376
	TrafficCarRecording[186] = 187
	TrafficCarStartime[186] = 75768.0000
	TrafficCarModel[186] = minivan

//	TrafficCarPos[187] = <<2888.3115, 3603.8743, 52.2723>>
//	TrafficCarQuatX[187] = -0.0066
//	TrafficCarQuatY[187] = 0.0015
//	TrafficCarQuatZ[187] = -0.1903
//	TrafficCarQuatW[187] = 0.9817
//	TrafficCarRecording[187] = 188
//	TrafficCarStartime[187] = 76098.0000
//	TrafficCarModel[187] = minivan

	TrafficCarPos[188] = <<2871.3333, 3659.2446, 52.2244>>
	TrafficCarQuatX[188] = 0.0001
	TrafficCarQuatY[188] = -0.0000
	TrafficCarQuatZ[188] = 0.9890
	TrafficCarQuatW[188] = 0.1480
	TrafficCarRecording[188] = 189
	TrafficCarStartime[188] = 76626.0000
	TrafficCarModel[188] = asterope

	TrafficCarPos[189] = <<2875.5686, 3652.1323, 52.2426>>
	TrafficCarQuatX[189] = 0.0002
	TrafficCarQuatY[189] = -0.0000
	TrafficCarQuatZ[189] = 0.9862
	TrafficCarQuatW[189] = 0.1656
	TrafficCarRecording[189] = 190
	TrafficCarStartime[189] = 76890.0000
	TrafficCarModel[189] = asterope

	TrafficCarPos[190] = <<2905.4819, 3668.8877, 52.2389>>
	TrafficCarQuatX[190] = 0.0000
	TrafficCarQuatY[190] = 0.0000
	TrafficCarQuatZ[190] = -0.1334
	TrafficCarQuatW[190] = 0.9911
	TrafficCarRecording[190] = 191
	TrafficCarStartime[190] = 76956.0000
	TrafficCarModel[190] = asterope

//	TrafficCarPos[191] = <<2902.5149, 3679.3806, 52.2028>>
//	TrafficCarQuatX[191] = 0.0009
//	TrafficCarQuatY[191] = -0.0001
//	TrafficCarQuatZ[191] = -0.0748
//	TrafficCarQuatW[191] = 0.9972
//	TrafficCarRecording[191] = 192
//	TrafficCarStartime[191] = 77880.0000
//	TrafficCarModel[191] = Vader

//	TrafficCarPos[192] = <<2914.3647, 3719.2368, 52.1600>>
//	TrafficCarQuatX[192] = -0.0005
//	TrafficCarQuatY[192] = 0.0000
//	TrafficCarQuatZ[192] = -0.1458
//	TrafficCarQuatW[192] = 0.9893
//	TrafficCarRecording[192] = 193
//	TrafficCarStartime[192] = 77946.0000
//	TrafficCarModel[192] = emperor

//	TrafficCarPos[193] = <<2903.5320, 3753.4880, 52.1987>>
//	TrafficCarQuatX[193] = -0.0000
//	TrafficCarQuatY[193] = 0.0009
//	TrafficCarQuatZ[193] = 0.9940
//	TrafficCarQuatW[193] = 0.1092
//	TrafficCarRecording[193] = 194
//	TrafficCarStartime[193] = 78606.0000
//	TrafficCarModel[193] = Vader

//	TrafficCarPos[194] = <<2909.9712, 3703.8149, 52.4099>>
//	TrafficCarQuatX[194] = -0.0002
//	TrafficCarQuatY[194] = -0.0003
//	TrafficCarQuatZ[194] = -0.1378
//	TrafficCarQuatW[194] = 0.9905
//	TrafficCarRecording[194] = 195
//	TrafficCarStartime[194] = 78804.0000
//	TrafficCarModel[194] = minivan

	TrafficCarPos[195] = <<2903.8784, 3758.2183, 52.4160>>
	TrafficCarQuatX[195] = 0.0001
	TrafficCarQuatY[195] = 0.0004
	TrafficCarQuatZ[195] = 0.9907
	TrafficCarQuatW[195] = 0.1359
	TrafficCarRecording[195] = 196
	TrafficCarStartime[195] = 78936.0000
	TrafficCarModel[195] = minivan

	TrafficCarPos[196] = <<2921.5320, 3747.8635, 52.4151>>
	TrafficCarQuatX[196] = -0.0004
	TrafficCarQuatY[196] = 0.0014
	TrafficCarQuatZ[196] = -0.1704
	TrafficCarQuatW[196] = 0.9854
	TrafficCarRecording[196] = 197
	TrafficCarStartime[196] = 79068.0000
	TrafficCarModel[196] = minivan

//	TrafficCarPos[197] = <<2933.3706, 3798.5608, 52.3470>>
//	TrafficCarQuatX[197] = -0.0011
//	TrafficCarQuatY[197] = 0.0002
//	TrafficCarQuatZ[197] = -0.0827
//	TrafficCarQuatW[197] = 0.9966
//	TrafficCarRecording[197] = 198
//	TrafficCarStartime[197] = 79596.0000
//	TrafficCarModel[197] = asterope

	TrafficCarPos[198] = <<2910.4014, 3810.9268, 52.1188>>
	TrafficCarQuatX[198] = 0.0001
	TrafficCarQuatY[198] = 0.0013
	TrafficCarQuatZ[198] = 0.9992
	TrafficCarQuatW[198] = 0.0405
	TrafficCarRecording[198] = 199
	TrafficCarStartime[198] = 79794.0000
	TrafficCarModel[198] = minivan

	TrafficCarPos[199] = <<2916.3674, 3820.1096, 52.1236>>
	TrafficCarQuatX[199] = -0.0001
	TrafficCarQuatY[199] = 0.0021
	TrafficCarQuatZ[199] = 0.9999
	TrafficCarQuatW[199] = 0.0160
	TrafficCarRecording[199] = 200
	TrafficCarStartime[199] = 80124.0000
	TrafficCarModel[199] = emperor

	TrafficCarPos[200] = <<2942.5574, 3832.1980, 52.3031>>
	TrafficCarQuatX[200] = -0.0019
	TrafficCarQuatY[200] = 0.0001
	TrafficCarQuatZ[200] = -0.0657
	TrafficCarQuatW[200] = 0.9978
	TrafficCarRecording[200] = 201
	TrafficCarStartime[200] = 81444.0000
	TrafficCarModel[200] = minivan

	TrafficCarPos[201] = <<2939.4863, 3896.6113, 51.8115>>
	TrafficCarQuatX[201] = -0.0032
	TrafficCarQuatY[201] = -0.0000
	TrafficCarQuatZ[201] = 0.0027
	TrafficCarQuatW[201] = 1.0000
	TrafficCarRecording[201] = 202
	TrafficCarStartime[201] = 81510.0000
	TrafficCarModel[201] = asterope

//	TrafficCarPos[202] = <<2945.4500, 3887.4250, 51.8027>>
//	TrafficCarQuatX[202] = -0.0032
//	TrafficCarQuatY[202] = -0.0000
//	TrafficCarQuatZ[202] = -0.0393
//	TrafficCarQuatW[202] = 0.9992
//	TrafficCarRecording[202] = 203
//	TrafficCarStartime[202] = 81708.0000
//	TrafficCarModel[202] = emperor

//	TrafficCarPos[203] = <<2940.0327, 3891.1279, 52.0222>>
//	TrafficCarQuatX[203] = -0.0033
//	TrafficCarQuatY[203] = 0.0000
//	TrafficCarQuatZ[203] = 0.0003
//	TrafficCarQuatW[203] = 1.0000
//	TrafficCarRecording[203] = 204
//	TrafficCarStartime[203] = 81906.0000
//	TrafficCarModel[203] = minivan

//	TrafficCarPos[204] = <<2939.9668, 3889.7473, 51.0329>>
//	TrafficCarQuatX[204] = -0.0316
//	TrafficCarQuatY[204] = 0.0318
//	TrafficCarQuatZ[204] = 0.0000
//	TrafficCarQuatW[204] = 0.9990
//	TrafficCarRecording[204] = 205
//	TrafficCarStartime[204] = 81972.0000
//	TrafficCarModel[204] = minivan

//	TrafficCarPos[205] = <<2938.9258, 3971.4851, 51.2215>>
//	TrafficCarQuatX[205] = -0.0039
//	TrafficCarQuatY[205] = -0.0004
//	TrafficCarQuatZ[205] = 0.0573
//	TrafficCarQuatW[205] = 0.9983
//	TrafficCarRecording[205] = 206
//	TrafficCarStartime[205] = 83028.0000
//	TrafficCarModel[205] = Vader

	TrafficCarPos[206] = <<2941.1116, 3958.0115, 51.5513>>
	TrafficCarQuatX[206] = -0.0038
	TrafficCarQuatY[206] = -0.0016
	TrafficCarQuatZ[206] = 0.0433
	TrafficCarQuatW[206] = 0.9991
	TrafficCarRecording[206] = 207
	TrafficCarStartime[206] = 83292.0000
	TrafficCarModel[206] = minivan

//	TrafficCarPos[207] = <<2930.9578, 4019.9880, 50.7709>>
//	TrafficCarQuatX[207] = -0.0042
//	TrafficCarQuatY[207] = -0.0005
//	TrafficCarQuatZ[207] = 0.1100
//	TrafficCarQuatW[207] = 0.9939
//	TrafficCarRecording[207] = 208
//	TrafficCarStartime[207] = 84018.0000
//	TrafficCarModel[207] = minivan

//	TrafficCarPos[208] = <<2932.2856, 4013.7095, 50.9161>>
//	TrafficCarQuatX[208] = -0.0043
//	TrafficCarQuatY[208] = -0.0002
//	TrafficCarQuatZ[208] = 0.0534
//	TrafficCarQuatW[208] = 0.9986
//	TrafficCarRecording[208] = 209
//	TrafficCarStartime[208] = 84348.0000
//	TrafficCarModel[208] = asterope

//	TrafficCarPos[209] = <<2926.6328, 4039.8433, 50.8801>>
//	TrafficCarQuatX[209] = -0.0039
//	TrafficCarQuatY[209] = -0.0004
//	TrafficCarQuatZ[209] = 0.1037
//	TrafficCarQuatW[209] = 0.9946
//	TrafficCarRecording[209] = 210
//	TrafficCarStartime[209] = 84348.0000
//	TrafficCarModel[209] = minivan

	TrafficCarPos[210] = <<2926.0823, 4017.5310, 51.0611>>
	TrafficCarQuatX[210] = -0.0042
	TrafficCarQuatY[210] = -0.0018
	TrafficCarQuatZ[210] = 0.0771
	TrafficCarQuatW[210] = 0.9970
	TrafficCarRecording[210] = 211
	TrafficCarStartime[210] = 84480.0000
	TrafficCarModel[210] = minivan

	TrafficCarPos[211] = <<2925.8696, 4016.1045, 50.8968>>
	TrafficCarQuatX[211] = -0.0042
	TrafficCarQuatY[211] = -0.0003
	TrafficCarQuatZ[211] = 0.1139
	TrafficCarQuatW[211] = 0.9935
	TrafficCarRecording[211] = 212
	TrafficCarStartime[211] = 85140.0000
	TrafficCarModel[211] = asterope

	TrafficCarPos[212] = <<2902.5908, 4129.4810, 50.1421>>
	TrafficCarQuatX[212] = -0.0017
	TrafficCarQuatY[212] = -0.0004
	TrafficCarQuatZ[212] = 0.1585
	TrafficCarQuatW[212] = 0.9874
	TrafficCarRecording[212] = 213
	TrafficCarStartime[212] = 86262.0000
	TrafficCarModel[212] = asterope

	TrafficCarPos[213] = <<2872.5837, 4122.1616, 49.9296>>
	TrafficCarQuatX[213] = -0.0004
	TrafficCarQuatY[213] = 0.0031
	TrafficCarQuatZ[213] = 0.9907
	TrafficCarQuatW[213] = -0.1358
	TrafficCarRecording[213] = 214
	TrafficCarStartime[213] = 86262.0000
	TrafficCarModel[213] = emperor

	TrafficCarPos[214] = <<2882.5388, 4110.0171, 50.2840>>
	TrafficCarQuatX[214] = 0.0007
	TrafficCarQuatY[214] = 0.0034
	TrafficCarQuatZ[214] = 0.9832
	TrafficCarQuatW[214] = -0.1824
	TrafficCarRecording[214] = 215
	TrafficCarStartime[214] = 86526.0000
	TrafficCarModel[214] = minivan

//	TrafficCarPos[215] = <<2887.2168, 4169.7148, 49.7477>>
//	TrafficCarQuatX[215] = -0.0005
//	TrafficCarQuatY[215] = 0.0001
//	TrafficCarQuatZ[215] = 0.1842
//	TrafficCarQuatW[215] = 0.9829
//	TrafficCarRecording[215] = 216
//	TrafficCarStartime[215] = 87054.0000
//	TrafficCarModel[215] = Vader

	TrafficCarPos[216] = <<2838.9204, 4220.8018, 49.8474>>
	TrafficCarQuatX[216] = -0.0005
	TrafficCarQuatY[216] = -0.0008
	TrafficCarQuatZ[216] = 0.9795
	TrafficCarQuatW[216] = -0.2013
	TrafficCarRecording[216] = 217
	TrafficCarStartime[216] = 88374.0000
	TrafficCarModel[216] = minivan

	TrafficCarPos[217] = <<2838.7207, 4233.9038, 49.6966>>
	TrafficCarQuatX[217] = 0.0003
	TrafficCarQuatY[217] = -0.0010
	TrafficCarQuatZ[217] = 0.9882
	TrafficCarQuatW[217] = -0.1534
	TrafficCarRecording[217] = 218
	TrafficCarStartime[217] = 88638.0000
	TrafficCarModel[217] = asterope

	TrafficCarPos[218] = <<2861.1074, 4254.6597, 49.6824>>
	TrafficCarQuatX[218] = 0.0013
	TrafficCarQuatY[218] = 0.0001
	TrafficCarQuatZ[218] = 0.1495
	TrafficCarQuatW[218] = 0.9888
	TrafficCarRecording[218] = 219
	TrafficCarStartime[218] = 88836.0000
	TrafficCarModel[218] = asterope

	TrafficCarPos[219] = <<2854.5920, 4258.1792, 49.6395>>
	TrafficCarQuatX[219] = 0.0015
	TrafficCarQuatY[219] = 0.0001
	TrafficCarQuatZ[219] = 0.1592
	TrafficCarQuatW[219] = 0.9872
	TrafficCarRecording[219] = 220
	TrafficCarStartime[219] = 88968.0000
	TrafficCarModel[219] = emperor

	TrafficCarPos[220] = <<2823.9988, 4256.2808, 49.9823>>
	TrafficCarQuatX[220] = 0.0063
	TrafficCarQuatY[220] = -0.0003
	TrafficCarQuatZ[220] = 0.9811
	TrafficCarQuatW[220] = -0.1932
	TrafficCarRecording[220] = 221
	TrafficCarStartime[220] = 89166.0000
	TrafficCarModel[220] = minivan

	TrafficCarPos[221] = <<2831.4854, 4251.9546, 49.7750>>
	TrafficCarQuatX[221] = -0.0035
	TrafficCarQuatY[221] = -0.0023
	TrafficCarQuatZ[221] = 0.9817
	TrafficCarQuatW[221] = -0.1904
	TrafficCarRecording[221] = 222
	TrafficCarStartime[221] = 89430.0000
	TrafficCarModel[221] = asterope

	TrafficCarPos[222] = <<2849.7661, 4287.1689, 49.6947>>
	TrafficCarQuatX[222] = 0.0007
	TrafficCarQuatY[222] = -0.0002
	TrafficCarQuatZ[222] = 0.1893
	TrafficCarQuatW[222] = 0.9819
	TrafficCarRecording[222] = 223
	TrafficCarStartime[222] = 89496.0000
	TrafficCarModel[222] = minivan

	TrafficCarPos[223] = <<2853.8042, 4277.0708, 49.9440>>
	TrafficCarQuatX[223] = 0.0019
	TrafficCarQuatY[223] = -0.0014
	TrafficCarQuatZ[223] = 0.1232
	TrafficCarQuatW[223] = 0.9924
	TrafficCarRecording[223] = 224
	TrafficCarStartime[223] = 89628.0000
	TrafficCarModel[223] = minivan

//	TrafficCarPos[224] = <<2807.2744, 4319.2813, 49.6917>>
//	TrafficCarQuatX[224] = -0.0046
//	TrafficCarQuatY[224] = 0.0027
//	TrafficCarQuatZ[224] = 0.9770
//	TrafficCarQuatW[224] = -0.2131
//	TrafficCarRecording[224] = 225
//	TrafficCarStartime[224] = 90420.0000
//	TrafficCarModel[224] = Vader

	TrafficCarPos[225] = <<2826.7432, 4358.8252, 49.4738>>
	TrafficCarQuatX[225] = -0.0056
	TrafficCarQuatY[225] = -0.0011
	TrafficCarQuatZ[225] = 0.1658
	TrafficCarQuatW[225] = 0.9861
	TrafficCarRecording[225] = 226
	TrafficCarStartime[225] = 91014.0000
	TrafficCarModel[225] = asterope

	TrafficCarPos[226] = <<2790.5518, 4346.2715, 49.4405>>
	TrafficCarQuatX[226] = -0.0012
	TrafficCarQuatY[226] = 0.0065
	TrafficCarQuatZ[226] = 0.9817
	TrafficCarQuatW[226] = -0.1902
	TrafficCarRecording[226] = 227
	TrafficCarStartime[226] = 91014.0000
	TrafficCarModel[226] = asterope

	TrafficCarPos[227] = <<2798.8682, 4337.9961, 49.7406>>
	TrafficCarQuatX[227] = -0.0030
	TrafficCarQuatY[227] = 0.0040
	TrafficCarQuatZ[227] = 0.9768
	TrafficCarQuatW[227] = -0.2140
	TrafficCarRecording[227] = 228
	TrafficCarStartime[227] = 91344.0000
	TrafficCarModel[227] = minivan

	TrafficCarPos[228] = <<2790.3762, 4346.4023, 49.3544>>
	TrafficCarQuatX[228] = -0.0015
	TrafficCarQuatY[228] = 0.0069
	TrafficCarQuatZ[228] = 0.9810
	TrafficCarQuatW[228] = -0.1940
	TrafficCarRecording[228] = 229
	TrafficCarStartime[228] = 91608.0000
	TrafficCarModel[228] = emperor

	TrafficCarPos[229] = <<2805.9817, 4392.1289, 48.9902>>
	TrafficCarQuatX[229] = -0.0079
	TrafficCarQuatY[229] = -0.0017
	TrafficCarQuatZ[229] = 0.1483
	TrafficCarQuatW[229] = 0.9889
	TrafficCarRecording[229] = 230
	TrafficCarStartime[229] = 91806.0000
	TrafficCarModel[229] = minivan

	TrafficCarPos[230] = <<2827.9146, 4412.4312, 48.4923>>
	TrafficCarQuatX[230] = -0.0010
	TrafficCarQuatY[230] = 0.0052
	TrafficCarQuatZ[230] = -0.3744
	TrafficCarQuatW[230] = 0.9273
	TrafficCarRecording[230] = 231
	TrafficCarStartime[230] = 92334.0000
	TrafficCarModel[230] = emperor

	TrafficCarPos[231] = <<2796.7185, 4464.0962, 47.6579>>
	TrafficCarQuatX[231] = -0.0100
	TrafficCarQuatY[231] = -0.0019
	TrafficCarQuatZ[231] = 0.1384
	TrafficCarQuatW[231] = 0.9903
	TrafficCarRecording[231] = 232
	TrafficCarStartime[231] = 93192.0000
	TrafficCarModel[231] = minivan

	TrafficCarPos[232] = <<2763.4312, 4396.7793, 48.5387>>
	TrafficCarQuatX[232] = -0.0067
	TrafficCarQuatY[232] = -0.0059
	TrafficCarQuatZ[232] = -0.5931
	TrafficCarQuatW[232] = 0.8051
	TrafficCarRecording[232] = 233
	TrafficCarStartime[232] = 93390.0000
	TrafficCarModel[232] = asterope

	TrafficCarPos[233] = <<2796.6157, 4463.2529, 47.4005>>
	TrafficCarQuatX[233] = -0.0099
	TrafficCarQuatY[233] = -0.0015
	TrafficCarQuatZ[233] = 0.1715
	TrafficCarQuatW[233] = 0.9851
	TrafficCarRecording[233] = 234
	TrafficCarStartime[233] = 93588.0000
	TrafficCarModel[233] = minivan

//	TrafficCarPos[234] = <<2758.1426, 4480.5522, 47.0032>>
//	TrafficCarQuatX[234] = -0.0043
//	TrafficCarQuatY[234] = 0.0104
//	TrafficCarQuatZ[234] = 0.9845
//	TrafficCarQuatW[234] = -0.1750
//	TrafficCarRecording[234] = 235
//	TrafficCarStartime[234] = 93720.0000
//	TrafficCarModel[234] = Vader

	TrafficCarPos[235] = <<2750.0212, 4510.1553, 46.5679>>
	TrafficCarQuatX[235] = -0.0052
	TrafficCarQuatY[235] = 0.0092
	TrafficCarQuatZ[235] = 0.9893
	TrafficCarQuatW[235] = -0.1454
	TrafficCarRecording[235] = 236
	TrafficCarStartime[235] = 94314.0000
	TrafficCarModel[235] = minivan

	TrafficCarPos[236] = <<2769.1023, 4543.7563, 45.7959>>
	TrafficCarQuatX[236] = -0.0094
	TrafficCarQuatY[236] = -0.0010
	TrafficCarQuatZ[236] = 0.1361
	TrafficCarQuatW[236] = 0.9907
	TrafficCarRecording[236] = 237
	TrafficCarStartime[236] = 94842.0000
	TrafficCarModel[236] = asterope

//	TrafficCarPos[237] = <<2775.7061, 4538.3184, 45.8946>>
//	TrafficCarQuatX[237] = -0.0093
//	TrafficCarQuatY[237] = -0.0014
//	TrafficCarQuatZ[237] = 0.1701
//	TrafficCarQuatW[237] = 0.9854
//	TrafficCarRecording[237] = 238
//	TrafficCarStartime[237] = 95172.0000
//	TrafficCarModel[237] = minivan

//	TrafficCarPos[238] = <<2769.8945, 4560.9517, 45.6520>>
//	TrafficCarQuatX[238] = -0.0092
//	TrafficCarQuatY[238] = 0.0017
//	TrafficCarQuatZ[238] = 0.1337
//	TrafficCarQuatW[238] = 0.9910
//	TrafficCarRecording[238] = 239
//	TrafficCarStartime[238] = 95172.0000
//	TrafficCarModel[238] = minivan

	TrafficCarPos[239] = <<2733.6995, 4556.1299, 45.3717>>
	TrafficCarQuatX[239] = -0.0009
	TrafficCarQuatY[239] = 0.0091
	TrafficCarQuatZ[239] = 0.9966
	TrafficCarQuatW[239] = -0.0817
	TrafficCarRecording[239] = 240
	TrafficCarStartime[239] = 95304.0000
	TrafficCarModel[239] = minivan

//	TrafficCarPos[240] = <<2762.9197, 4565.1895, 45.3244>>
//	TrafficCarQuatX[240] = -0.0085
//	TrafficCarQuatY[240] = -0.0011
//	TrafficCarQuatZ[240] = 0.1278
//	TrafficCarQuatW[240] = 0.9918
//	TrafficCarRecording[240] = 241
//	TrafficCarStartime[240] = 95634.0000
//	TrafficCarModel[240] = emperor

	TrafficCarPos[241] = <<2735.2793, 4578.1475, 44.9966>>
	TrafficCarQuatX[241] = -0.0005
	TrafficCarQuatY[241] = 0.0079
	TrafficCarQuatZ[241] = 0.9990
	TrafficCarQuatW[241] = -0.0442
	TrafficCarRecording[241] = 242
	TrafficCarStartime[241] = 95700.0000
	TrafficCarModel[241] = minivan

//	TrafficCarPos[242] = <<2763.8918, 4582.4990, 45.0513>>
//	TrafficCarQuatX[242] = -0.0093
//	TrafficCarQuatY[242] = 0.0028
//	TrafficCarQuatZ[242] = 0.1685
//	TrafficCarQuatW[242] = 0.9857
//	TrafficCarRecording[242] = 243
//	TrafficCarStartime[242] = 95898.0000
//	TrafficCarModel[242] = minivan

	TrafficCarPos[243] = <<2733.3506, 4558.6138, 45.5368>>
	TrafficCarQuatX[243] = 0.0003
	TrafficCarQuatY[243] = 0.0089
	TrafficCarQuatZ[243] = 0.9931
	TrafficCarQuatW[243] = -0.1167
	TrafficCarRecording[243] = 244
	TrafficCarStartime[243] = 96030.0000
	TrafficCarModel[243] = minivan

	TrafficCarPos[244] = <<2753.3953, 4625.8643, 44.4231>>
	TrafficCarQuatX[244] = -0.0065
	TrafficCarQuatY[244] = -0.0008
	TrafficCarQuatZ[244] = 0.1180
	TrafficCarQuatW[244] = 0.9930
	TrafficCarRecording[244] = 245
	TrafficCarStartime[244] = 96558.0000
	TrafficCarModel[244] = minivan

	TrafficCarPos[245] = <<2756.6841, 4587.3096, 44.9834>>
	TrafficCarQuatX[245] = -0.0078
	TrafficCarQuatY[245] = -0.0012
	TrafficCarQuatZ[245] = 0.1280
	TrafficCarQuatW[245] = 0.9917
	TrafficCarRecording[245] = 246
	TrafficCarStartime[245] = 97086.0000
	TrafficCarModel[245] = asterope

	TrafficCarPos[246] = <<2745.1133, 4663.1567, 44.1967>>
	TrafficCarQuatX[246] = -0.0036
	TrafficCarQuatY[246] = -0.0003
	TrafficCarQuatZ[246] = 0.1046
	TrafficCarQuatW[246] = 0.9945
	TrafficCarRecording[246] = 247
	TrafficCarStartime[246] = 97614.0000
	TrafficCarModel[246] = asterope

//	TrafficCarPos[247] = <<2740.1965, 4680.7329, 43.9248>>
//	TrafficCarQuatX[247] = -0.0020
//	TrafficCarQuatY[247] = 0.0003
//	TrafficCarQuatZ[247] = 0.2092
//	TrafficCarQuatW[247] = 0.9779
//	TrafficCarRecording[247] = 248
//	TrafficCarStartime[247] = 98010.0000
//	TrafficCarModel[247] = Vader

//	TrafficCarPos[248] = <<2729.3479, 4701.9292, 43.8695>>
//	TrafficCarQuatX[248] = 0.0001
//	TrafficCarQuatY[248] = 0.0002
//	TrafficCarQuatZ[248] = 0.1131
//	TrafficCarQuatW[248] = 0.9936
//	TrafficCarRecording[248] = 249
//	TrafficCarStartime[248] = 98340.0000
//	TrafficCarModel[248] = minivan

//	TrafficCarPos[249] = <<2719.0593, 4750.0059, 43.9539>>
//	TrafficCarQuatX[249] = 0.0015
//	TrafficCarQuatY[249] = 0.0002
//	TrafficCarQuatZ[249] = 0.1147
//	TrafficCarQuatW[249] = 0.9934
//	TrafficCarRecording[249] = 250
//	TrafficCarStartime[249] = 98934.0000
//	TrafficCarModel[249] = emperor

	TrafficCarPos[250] = <<2699.8525, 4746.0195, 43.9265>>
	TrafficCarQuatX[250] = 0.0002
	TrafficCarQuatY[250] = -0.0005
	TrafficCarQuatZ[250] = 0.9962
	TrafficCarQuatW[250] = -0.0871
	TrafficCarRecording[250] = 251
	TrafficCarStartime[250] = 98934.0000
	TrafficCarModel[250] = minivan

//	TrafficCarPos[251] = <<2723.2102, 4762.0459, 44.0218>>
//	TrafficCarQuatX[251] = 0.0014
//	TrafficCarQuatY[251] = 0.0001
//	TrafficCarQuatZ[251] = 0.1044
//	TrafficCarQuatW[251] = 0.9945
//	TrafficCarRecording[251] = 252
//	TrafficCarStartime[251] = 99396.0000
//	TrafficCarModel[251] = minivan

//	TrafficCarPos[252] = <<2713.8896, 4776.2275, 44.0996>>
//	TrafficCarQuatX[252] = 0.0030
//	TrafficCarQuatY[252] = 0.0004
//	TrafficCarQuatZ[252] = 0.1103
//	TrafficCarQuatW[252] = 0.9939
//	TrafficCarRecording[252] = 253
//	TrafficCarStartime[252] = 99660.0000
//	TrafficCarModel[252] = asterope

//	TrafficCarPos[253] = <<2714.2168, 4796.4341, 44.0916>>
//	TrafficCarQuatX[253] = 0.0007
//	TrafficCarQuatY[253] = 0.0001
//	TrafficCarQuatZ[253] = 0.1276
//	TrafficCarQuatW[253] = 0.9918
//	TrafficCarRecording[253] = 254
//	TrafficCarStartime[253] = 99858.0000
//	TrafficCarModel[253] = minivan

	TrafficCarPos[254] = <<2698.0276, 4733.7168, 44.1639>>
	TrafficCarQuatX[254] = -0.0027
	TrafficCarQuatY[254] = -0.0010
	TrafficCarQuatZ[254] = 0.9943
	TrafficCarQuatW[254] = -0.1066
	TrafficCarRecording[254] = 255
	TrafficCarStartime[254] = 100188.0000
	TrafficCarModel[254] = minivan

//	TrafficCarPos[255] = <<2709.7654, 4819.0278, 44.1546>>
//	TrafficCarQuatX[255] = 0.0007
//	TrafficCarQuatY[255] = 0.0001
//	TrafficCarQuatZ[255] = 0.1469
//	TrafficCarQuatW[255] = 0.9891
//	TrafficCarRecording[255] = 256
//	TrafficCarStartime[255] = 100584.0000
//	TrafficCarModel[255] = asterope

	TrafficCarPos[256] = <<2682.8777, 4826.2163, 44.3648>>
	TrafficCarQuatX[256] = 0.0005
	TrafficCarQuatY[256] = -0.0007
	TrafficCarQuatZ[256] = 0.9942
	TrafficCarQuatW[256] = -0.1075
	TrafficCarRecording[256] = 257
	TrafficCarStartime[256] = 100782.0000
	TrafficCarModel[256] = minivan

	TrafficCarPos[257] = <<2671.1245, 4851.1870, 44.1884>>
	TrafficCarQuatX[257] = -0.0018
	TrafficCarQuatY[257] = -0.0008
	TrafficCarQuatZ[257] = 0.9956
	TrafficCarQuatW[257] = -0.0933
	TrafficCarRecording[257] = 258
	TrafficCarStartime[257] = 101178.0000
	TrafficCarModel[257] = minivan

	TrafficCarPos[258] = <<2670.6460, 4856.5493, 44.3695>>
	TrafficCarQuatX[258] = -0.0002
	TrafficCarQuatY[258] = -0.0006
	TrafficCarQuatZ[258] = 0.9887
	TrafficCarQuatW[258] = -0.1497
	TrafficCarRecording[258] = 259
	TrafficCarStartime[258] = 101574.0000
	TrafficCarModel[258] = asterope

//	TrafficCarPos[259] = <<2691.9001, 4876.1128, 44.2642>>
//	TrafficCarQuatX[259] = 0.0005
//	TrafficCarQuatY[259] = -0.0001
//	TrafficCarQuatZ[259] = 0.0691
//	TrafficCarQuatW[259] = 0.9976
//	TrafficCarRecording[259] = 260
//	TrafficCarStartime[259] = 101772.0000
//	TrafficCarModel[259] = asterope

//	TrafficCarPos[260] = <<2692.8052, 4892.2969, 44.4566>>
//	TrafficCarQuatX[260] = 0.0007
//	TrafficCarQuatY[260] = 0.0004
//	TrafficCarQuatZ[260] = 0.1139
//	TrafficCarQuatW[260] = 0.9935
//	TrafficCarRecording[260] = 261
//	TrafficCarStartime[260] = 101838.0000
//	TrafficCarModel[260] = minivan

	TrafficCarPos[261] = <<2667.8579, 4869.2661, 44.2200>>
	TrafficCarQuatX[261] = 0.0001
	TrafficCarQuatY[261] = -0.0005
	TrafficCarQuatZ[261] = 0.9908
	TrafficCarQuatW[261] = -0.1351
	TrafficCarRecording[261] = 262
	TrafficCarStartime[261] = 101970.0000
	TrafficCarModel[261] = minivan

//	TrafficCarPos[262] = <<2682.1086, 4919.7715, 44.2625>>
//	TrafficCarQuatX[262] = 0.0000
//	TrafficCarQuatY[262] = -0.0000
//	TrafficCarQuatZ[262] = 0.0996
//	TrafficCarQuatW[262] = 0.9950
//	TrafficCarRecording[262] = 263
//	TrafficCarStartime[262] = 102762.0000
//	TrafficCarModel[262] = minivan

//	TrafficCarPos[263] = <<2682.4604, 4915.1680, 44.1997>>
//	TrafficCarQuatX[263] = 0.0000
//	TrafficCarQuatY[263] = 0.0000
//	TrafficCarQuatZ[263] = 0.1082
//	TrafficCarQuatW[263] = 0.9941
//	TrafficCarRecording[263] = 264
//	TrafficCarStartime[263] = 103290.0000
//	TrafficCarModel[263] = minivan

	TrafficCarPos[264] = <<2654.7310, 4947.6592, 44.4515>>
	TrafficCarQuatX[264] = -0.0000
	TrafficCarQuatY[264] = -0.0006
	TrafficCarQuatZ[264] = 0.9925
	TrafficCarQuatW[264] = -0.1222
	TrafficCarRecording[264] = 265
	TrafficCarStartime[264] = 103422.0000
	TrafficCarModel[264] = asterope

//	TrafficCarPos[265] = <<2672.1680, 4985.8140, 44.3148>>
//	TrafficCarQuatX[265] = 0.0008
//	TrafficCarQuatY[265] = -0.0000
//	TrafficCarQuatZ[265] = 0.1008
//	TrafficCarQuatW[265] = 0.9949
//	TrafficCarRecording[265] = 266
//	TrafficCarStartime[265] = 103554.0000
//	TrafficCarModel[265] = Vader

//	TrafficCarPos[266] = <<2676.0173, 4972.1226, 44.3164>>
//	TrafficCarQuatX[266] = 0.0005
//	TrafficCarQuatY[266] = 0.0000
//	TrafficCarQuatZ[266] = 0.0798
//	TrafficCarQuatW[266] = 0.9968
//	TrafficCarRecording[266] = 267
//	TrafficCarStartime[266] = 103818.0000
//	TrafficCarModel[266] = minivan

	TrafficCarPos[267] = <<2650.3118, 4943.5879, 44.5048>>
	TrafficCarQuatX[267] = 0.0013
	TrafficCarQuatY[267] = -0.0007
	TrafficCarQuatZ[267] = 0.9957
	TrafficCarQuatW[267] = -0.0931
	TrafficCarRecording[267] = 268
	TrafficCarStartime[267] = 103950.0000
	TrafficCarModel[267] = minivan

//	TrafficCarPos[268] = <<2669.3022, 5003.4873, 44.3213>>
//	TrafficCarQuatX[268] = 0.0004
//	TrafficCarQuatY[268] = -0.0000
//	TrafficCarQuatZ[268] = 0.1029
//	TrafficCarQuatW[268] = 0.9947
//	TrafficCarRecording[268] = 269
//	TrafficCarStartime[268] = 104148.0000
//	TrafficCarModel[268] = emperor

//	TrafficCarPos[269] = <<2621.1787, 5045.5127, 44.5638>>
//	TrafficCarQuatX[269] = -0.0001
//	TrafficCarQuatY[269] = -0.0005
//	TrafficCarQuatZ[269] = 0.9922
//	TrafficCarQuatW[269] = -0.1249
//	TrafficCarRecording[269] = 270
//	TrafficCarStartime[269] = 104874.0000
//	TrafficCarModel[269] = minivan

//	TrafficCarPos[270] = <<2652.0811, 5061.4380, 44.3571>>
//	TrafficCarQuatX[270] = 0.0000
//	TrafficCarQuatY[270] = -0.0000
//	TrafficCarQuatZ[270] = 0.0947
//	TrafficCarQuatW[270] = 0.9955
//	TrafficCarRecording[270] = 271
//	TrafficCarStartime[270] = 105072.0000
//	TrafficCarModel[270] = minivan

//	TrafficCarPos[271] = <<2623.9983, 5058.8535, 44.3565>>
//	TrafficCarQuatX[271] = 0.0001
//	TrafficCarQuatY[271] = -0.0000
//	TrafficCarQuatZ[271] = 0.9892
//	TrafficCarQuatW[271] = -0.1466
//	TrafficCarRecording[271] = 272
//	TrafficCarStartime[271] = 105666.0000
//	TrafficCarModel[271] = minivan

//	TrafficCarPos[272] = <<2656.6990, 5107.9023, 44.2505>>
//	TrafficCarQuatX[272] = -0.0027
//	TrafficCarQuatY[272] = -0.0018
//	TrafficCarQuatZ[272] = -0.1337
//	TrafficCarQuatW[272] = 0.9910
//	TrafficCarRecording[272] = 273
//	TrafficCarStartime[272] = 105930.0000
//	TrafficCarModel[272] = minivan

//	TrafficCarPos[273] = <<2626.8604, 5182.9043, 44.5148>>
//	TrafficCarQuatX[273] = -0.0007
//	TrafficCarQuatY[273] = 0.0001
//	TrafficCarQuatZ[273] = 0.1292
//	TrafficCarQuatW[273] = 0.9916
//	TrafficCarRecording[273] = 274
//	TrafficCarStartime[273] = 107448.0000
//	TrafficCarModel[273] = minivan

	TrafficCarPos[274] = <<2618.7942, 5190.9868, 44.4486>>
	TrafficCarQuatX[274] = -0.0007
	TrafficCarQuatY[274] = -0.0001
	TrafficCarQuatZ[274] = 0.1434
	TrafficCarQuatW[274] = 0.9897
	TrafficCarRecording[274] = 275
	TrafficCarStartime[274] = 107646.0000
	TrafficCarModel[274] = asterope

	TrafficCarPos[275] = <<2620.9553, 5181.9199, 44.3305>>
	TrafficCarQuatX[275] = -0.0006
	TrafficCarQuatY[275] = 0.0002
	TrafficCarQuatZ[275] = 0.1561
	TrafficCarQuatW[275] = 0.9877
	TrafficCarRecording[275] = 276
	TrafficCarStartime[275] = 107844.0000
	TrafficCarModel[275] = asterope

	TrafficCarPos[276] = <<2587.1160, 5198.9702, 44.2932>>
	TrafficCarQuatX[276] = -0.0000
	TrafficCarQuatY[276] = 0.0002
	TrafficCarQuatZ[276] = 0.9877
	TrafficCarQuatW[276] = -0.1562
	TrafficCarRecording[276] = 277
	TrafficCarStartime[276] = 107976.0000
	TrafficCarModel[276] = minivan

//	TrafficCarPos[277] = <<2619.5605, 5205.5767, 44.2791>>
//	TrafficCarQuatX[277] = -0.0006
//	TrafficCarQuatY[277] = -0.0002
//	TrafficCarQuatZ[277] = 0.1082
//	TrafficCarQuatW[277] = 0.9941
//	TrafficCarRecording[277] = 278
//	TrafficCarStartime[277] = 108240.0000
//	TrafficCarModel[277] = minivan

//	TrafficCarPos[278] = <<2621.1724, 5200.2778, 44.2766>>
//	TrafficCarQuatX[278] = 0.0006
//	TrafficCarQuatY[278] = 0.0001
//	TrafficCarQuatZ[278] = 0.2128
//	TrafficCarQuatW[278] = 0.9771
//	TrafficCarRecording[278] = 279
//	TrafficCarStartime[278] = 108372.0000
//	TrafficCarModel[278] = Vader

	TrafficCarPos[279] = <<2600.9653, 5267.5176, 44.1361>>
	TrafficCarQuatX[279] = -0.0006
	TrafficCarQuatY[279] = -0.0000
	TrafficCarQuatZ[279] = 0.1429
	TrafficCarQuatW[279] = 0.9897
	TrafficCarRecording[279] = 280
	TrafficCarStartime[279] = 109164.0000
	TrafficCarModel[279] = minivan

//	TrafficCarPos[280] = <<2595.7542, 5267.8159, 44.2360>>
//	TrafficCarQuatX[280] = -0.0006
//	TrafficCarQuatY[280] = 0.0001
//	TrafficCarQuatZ[280] = 0.1417
//	TrafficCarQuatW[280] = 0.9899
//	TrafficCarRecording[280] = 281
//	TrafficCarStartime[280] = 109164.0000
//	TrafficCarModel[280] = asterope

//	TrafficCarPos[281] = <<2604.3191, 5256.8315, 44.4257>>
//	TrafficCarQuatX[281] = -0.0006
//	TrafficCarQuatY[281] = -0.0015
//	TrafficCarQuatZ[281] = 0.1864
//	TrafficCarQuatW[281] = 0.9825
//	TrafficCarRecording[281] = 282
//	TrafficCarStartime[281] = 109362.0000
//	TrafficCarModel[281] = minivan

//	TrafficCarPos[282] = <<2578.0525, 5324.3140, 44.1364>>
//	TrafficCarQuatX[282] = -0.0006
//	TrafficCarQuatY[282] = -0.0001
//	TrafficCarQuatZ[282] = 0.1569
//	TrafficCarQuatW[282] = 0.9876
//	TrafficCarRecording[282] = 283
//	TrafficCarStartime[282] = 110352.0000
//	TrafficCarModel[282] = minivan

	TrafficCarPos[283] = <<2553.7939, 5321.5039, 44.5224>>
	TrafficCarQuatX[283] = -0.0002
	TrafficCarQuatY[283] = 0.0005
	TrafficCarQuatZ[283] = 0.9943
	TrafficCarQuatW[283] = -0.1065
	TrafficCarRecording[283] = 284
	TrafficCarStartime[283] = 110484.0000
	TrafficCarModel[283] = minivan

//	TrafficCarPos[284] = <<2583.3596, 5308.2412, 44.1476>>
//	TrafficCarQuatX[284] = 0.0006
//	TrafficCarQuatY[284] = 0.0001
//	TrafficCarQuatZ[284] = 0.2116
//	TrafficCarQuatW[284] = 0.9774
//	TrafficCarRecording[284] = 285
//	TrafficCarStartime[284] = 110616.0000
//	TrafficCarModel[284] = Vader

//	TrafficCarPos[285] = <<2572.3564, 5342.5728, 44.3177>>
//	TrafficCarQuatX[285] = -0.0002
//	TrafficCarQuatY[285] = -0.0001
//	TrafficCarQuatZ[285] = 0.1478
//	TrafficCarQuatW[285] = 0.9890
//	TrafficCarRecording[285] = 286
//	TrafficCarStartime[285] = 110946.0000
//	TrafficCarModel[285] = minivan

//	TrafficCarPos[286] = <<2558.2329, 5395.3130, 44.0749>>
//	TrafficCarQuatX[286] = -0.0002
//	TrafficCarQuatY[286] = 0.0000
//	TrafficCarQuatZ[286] = 0.1390
//	TrafficCarQuatW[286] = 0.9903
//	TrafficCarRecording[286] = 287
//	TrafficCarStartime[286] = 111738.0000
//	TrafficCarModel[286] = minivan

	TrafficCarPos[287] = <<2528.2549, 5399.9287, 44.2416>>
	TrafficCarQuatX[287] = 0.0002
	TrafficCarQuatY[287] = 0.0002
	TrafficCarQuatZ[287] = 0.9906
	TrafficCarQuatW[287] = -0.1368
	TrafficCarRecording[287] = 288
	TrafficCarStartime[287] = 112002.0000
	TrafficCarModel[287] = asterope

//	TrafficCarPos[288] = <<2553.2566, 5408.7754, 44.0636>>
//	TrafficCarQuatX[288] = 0.0008
//	TrafficCarQuatY[288] = 0.0000
//	TrafficCarQuatZ[288] = 0.2430
//	TrafficCarQuatW[288] = 0.9700
//	TrafficCarRecording[288] = 289
//	TrafficCarStartime[288] = 112200.0000
//	TrafficCarModel[288] = Vader

//	TrafficCarPos[289] = <<2548.7327, 5422.6401, 44.2893>>
//	TrafficCarQuatX[289] = 0.0005
//	TrafficCarQuatY[289] = 0.0001
//	TrafficCarQuatZ[289] = 0.1532
//	TrafficCarQuatW[289] = 0.9882
//	TrafficCarRecording[289] = 290
//	TrafficCarStartime[289] = 112398.0000
//	TrafficCarModel[289] = minivan

	TrafficCarPos[290] = <<2534.5610, 5394.7261, 44.0934>>
	TrafficCarQuatX[290] = -0.0002
	TrafficCarQuatY[290] = 0.0006
	TrafficCarQuatZ[290] = 0.9907
	TrafficCarQuatW[290] = -0.1359
	TrafficCarRecording[290] = 291
	TrafficCarStartime[290] = 113124.0000
	TrafficCarModel[290] = minivan

	TrafficCarPos[291] = <<2531.4963, 5407.4761, 44.0435>>
	TrafficCarQuatX[291] = 0.0000
	TrafficCarQuatY[291] = -0.0001
	TrafficCarQuatZ[291] = 0.9919
	TrafficCarQuatW[291] = -0.1270
	TrafficCarRecording[291] = 292
	TrafficCarStartime[291] = 113256.0000
	TrafficCarModel[291] = emperor

//	TrafficCarPos[292] = <<2511.7451, 5470.1875, 44.3447>>
//	TrafficCarQuatX[292] = -0.0012
//	TrafficCarQuatY[292] = -0.0015
//	TrafficCarQuatZ[292] = 0.9853
//	TrafficCarQuatW[292] = -0.1709
//	TrafficCarRecording[292] = 293
//	TrafficCarStartime[292] = 113520.0000
//	TrafficCarModel[292] = minivan

//	TrafficCarPos[293] = <<2550.2617, 5437.9927, 44.1226>>
//	TrafficCarQuatX[293] = 0.0007
//	TrafficCarQuatY[293] = 0.0002
//	TrafficCarQuatZ[293] = 0.1533
//	TrafficCarQuatW[293] = 0.9882
//	TrafficCarRecording[293] = 294
//	TrafficCarStartime[293] = 113850.0000
//	TrafficCarModel[293] = asterope

//	TrafficCarPos[294] = <<2507.9319, 5479.8594, 44.1506>>
//	TrafficCarQuatX[294] = 0.0001
//	TrafficCarQuatY[294] = -0.0013
//	TrafficCarQuatZ[294] = 0.9873
//	TrafficCarQuatW[294] = -0.1591
//	TrafficCarRecording[294] = 295
//	TrafficCarStartime[294] = 114048.0000
//	TrafficCarModel[294] = minivan

//	TrafficCarPos[295] = <<2514.7593, 5461.7959, 44.1486>>
//	TrafficCarQuatX[295] = 0.0001
//	TrafficCarQuatY[295] = -0.0004
//	TrafficCarQuatZ[295] = 0.9855
//	TrafficCarQuatW[295] = -0.1697
//	TrafficCarRecording[295] = 296
//	TrafficCarStartime[295] = 114378.0000
//	TrafficCarModel[295] = asterope

//	TrafficCarPos[296] = <<2516.3423, 5521.9912, 44.2608>>
//	TrafficCarQuatX[296] = 0.0013
//	TrafficCarQuatY[296] = 0.0001
//	TrafficCarQuatZ[296] = 0.1739
//	TrafficCarQuatW[296] = 0.9848
//	TrafficCarRecording[296] = 297
//	TrafficCarStartime[296] = 114840.0000
//	TrafficCarModel[296] = minivan

//	TrafficCarPos[297] = <<2480.4070, 5556.0698, 44.2801>>
//	TrafficCarQuatX[297] = 0.0002
//	TrafficCarQuatY[297] = -0.0012
//	TrafficCarQuatZ[297] = 0.9812
//	TrafficCarQuatW[297] = -0.1929
//	TrafficCarRecording[297] = 298
//	TrafficCarStartime[297] = 115236.0000
//	TrafficCarModel[297] = minivan

//	TrafficCarPos[298] = <<2497.7844, 5571.8809, 44.5875>>
//	TrafficCarQuatX[298] = 0.0008
//	TrafficCarQuatY[298] = 0.0002
//	TrafficCarQuatZ[298] = 0.1861
//	TrafficCarQuatW[298] = 0.9825
//	TrafficCarRecording[298] = 299
//	TrafficCarStartime[298] = 115434.0000
//	TrafficCarModel[298] = minivan

//	TrafficCarPos[299] = <<2471.8467, 5562.0718, 44.3712>>
//	TrafficCarQuatX[299] = 0.0001
//	TrafficCarQuatY[299] = -0.0013
//	TrafficCarQuatZ[299] = 0.9733
//	TrafficCarQuatW[299] = -0.2293
//	TrafficCarRecording[299] = 300
//	TrafficCarStartime[299] = 115698.0000
//	TrafficCarModel[299] = minivan

//	TrafficCarPos[300] = <<2503.5378, 5570.7871, 44.3415>>
//	TrafficCarQuatX[300] = 0.0010
//	TrafficCarQuatY[300] = 0.0005
//	TrafficCarQuatZ[300] = 0.2245
//	TrafficCarQuatW[300] = 0.9745
//	TrafficCarRecording[300] = 301
//	TrafficCarStartime[300] = 115830.0000
//	TrafficCarModel[300] = emperor

//	TrafficCarPos[301] = <<2496.2344, 5590.6104, 44.4156>>
//	TrafficCarQuatX[301] = 0.0012
//	TrafficCarQuatY[301] = 0.0001
//	TrafficCarQuatZ[301] = 0.1792
//	TrafficCarQuatW[301] = 0.9838
//	TrafficCarRecording[301] = 302
//	TrafficCarStartime[301] = 115896.0000
//	TrafficCarModel[301] = minivan

//	TrafficCarPos[302] = <<2491.3748, 5601.3467, 44.4328>>
//	TrafficCarQuatX[302] = 0.0020
//	TrafficCarQuatY[302] = 0.0004
//	TrafficCarQuatZ[302] = 0.2585
//	TrafficCarQuatW[302] = 0.9660
//	TrafficCarRecording[302] = 303
//	TrafficCarStartime[302] = 116556.0000
//	TrafficCarModel[302] = Vader

//	TrafficCarPos[303] = <<2474.6465, 5641.5903, 44.5806>>
//	TrafficCarQuatX[303] = 0.0014
//	TrafficCarQuatY[303] = 0.0004
//	TrafficCarQuatZ[303] = 0.1967
//	TrafficCarQuatW[303] = 0.9805
//	TrafficCarRecording[303] = 304
//	TrafficCarStartime[303] = 116952.0000
//	TrafficCarModel[303] = asterope

//	TrafficCarPos[304] = <<2443.4148, 5630.0596, 44.5461>>
//	TrafficCarQuatX[304] = 0.0002
//	TrafficCarQuatY[304] = -0.0014
//	TrafficCarQuatZ[304] = 0.9723
//	TrafficCarQuatW[304] = -0.2339
//	TrafficCarRecording[304] = 305
//	TrafficCarStartime[304] = 117018.0000
//	TrafficCarModel[304] = minivan

//	TrafficCarPos[305] = <<2471.0015, 5636.2915, 44.5357>>
//	TrafficCarQuatX[305] = 0.0013
//	TrafficCarQuatY[305] = 0.0001
//	TrafficCarQuatZ[305] = 0.2363
//	TrafficCarQuatW[305] = 0.9717
//	TrafficCarRecording[305] = 306
//	TrafficCarStartime[305] = 117282.0000
//	TrafficCarModel[305] = minivan

//	TrafficCarPos[306] = <<2459.7236, 5675.4243, 44.8480>>
//	TrafficCarQuatX[306] = 0.0017
//	TrafficCarQuatY[306] = 0.0004
//	TrafficCarQuatZ[306] = 0.2092
//	TrafficCarQuatW[306] = 0.9779
//	TrafficCarRecording[306] = 307
//	TrafficCarStartime[306] = 117678.0000
//	TrafficCarModel[306] = minivan

//	TrafficCarPos[307] = <<2464.1621, 5665.6514, 44.7753>>
//	TrafficCarQuatX[307] = 0.0012
//	TrafficCarQuatY[307] = 0.0004
//	TrafficCarQuatZ[307] = 0.1711
//	TrafficCarQuatW[307] = 0.9853
//	TrafficCarRecording[307] = 308
//	TrafficCarStartime[307] = 118140.0000
//	TrafficCarModel[307] = asterope

//	TrafficCarPos[308] = <<2442.6357, 5700.7183, 44.7607>>
//	TrafficCarQuatX[308] = 0.0024
//	TrafficCarQuatY[308] = 0.0006
//	TrafficCarQuatZ[308] = 0.2199
//	TrafficCarQuatW[308] = 0.9755
//	TrafficCarRecording[308] = 309
//	TrafficCarStartime[308] = 118272.0000
//	TrafficCarModel[308] = minivan

//	TrafficCarPos[309] = <<2428.5857, 5738.9170, 45.0313>>
//	TrafficCarQuatX[309] = 0.0034
//	TrafficCarQuatY[309] = 0.0008
//	TrafficCarQuatZ[309] = 0.2295
//	TrafficCarQuatW[309] = 0.9733
//	TrafficCarRecording[309] = 310
//	TrafficCarStartime[309] = 119130.0000
//	TrafficCarModel[309] = asterope

//	TrafficCarPos[310] = <<2413.8796, 5758.8765, 45.1997>>
//	TrafficCarQuatX[310] = 0.0027
//	TrafficCarQuatY[310] = 0.0007
//	TrafficCarQuatZ[310] = 0.2451
//	TrafficCarQuatW[310] = 0.9695
//	TrafficCarRecording[310] = 311
//	TrafficCarStartime[310] = 119658.0000
//	TrafficCarModel[310] = minivan

//	TrafficCarPos[311] = <<2426.1887, 5733.7778, 45.3610>>
//	TrafficCarQuatX[311] = 0.0034
//	TrafficCarQuatY[311] = 0.0008
//	TrafficCarQuatZ[311] = 0.2244
//	TrafficCarQuatW[311] = 0.9745
//	TrafficCarRecording[311] = 312
//	TrafficCarStartime[311] = 120186.0000
//	TrafficCarModel[311] = minivan

//	TrafficCarPos[312] = <<2379.2861, 5829.6538, 46.0483>>
//	TrafficCarQuatX[312] = 0.0063
//	TrafficCarQuatY[312] = 0.0020
//	TrafficCarQuatZ[312] = 0.2709
//	TrafficCarQuatW[312] = 0.9626
//	TrafficCarRecording[312] = 313
//	TrafficCarStartime[312] = 121176.0000
//	TrafficCarModel[312] = minivan

//	TrafficCarPos[313] = <<2390.9995, 5809.2119, 45.7082>>
//	TrafficCarQuatX[313] = 0.0062
//	TrafficCarQuatY[313] = 0.0020
//	TrafficCarQuatZ[313] = 0.3054
//	TrafficCarQuatW[313] = 0.9522
//	TrafficCarRecording[313] = 314
//	TrafficCarStartime[313] = 121308.0000
//	TrafficCarModel[313] = emperor

//	TrafficCarPos[314] = <<2335.8091, 5881.2896, 47.1471>>
//	TrafficCarQuatX[314] = 0.0096
//	TrafficCarQuatY[314] = 0.0033
//	TrafficCarQuatZ[314] = 0.3279
//	TrafficCarQuatW[314] = 0.9446
//	TrafficCarRecording[314] = 315
//	TrafficCarStartime[314] = 122562.0000
//	TrafficCarModel[314] = Vader

//	TrafficCarPos[315] = <<2308.3513, 5926.8325, 48.0219>>
//	TrafficCarQuatX[315] = 0.0078
//	TrafficCarQuatY[315] = 0.0010
//	TrafficCarQuatZ[315] = 0.3165
//	TrafficCarQuatW[315] = 0.9486
//	TrafficCarRecording[315] = 316
//	TrafficCarStartime[315] = 123618.0000
//	TrafficCarModel[315] = minivan

//	TrafficCarPos[316] = <<2315.8203, 5916.2373, 48.1020>>
//	TrafficCarQuatX[316] = 0.0100
//	TrafficCarQuatY[316] = -0.0114
//	TrafficCarQuatZ[316] = 0.3397
//	TrafficCarQuatW[316] = 0.9404
//	TrafficCarRecording[316] = 317
//	TrafficCarStartime[316] = 123882.0000
//	TrafficCarModel[316] = asterope

//	TrafficCarPos[317] = <<2263.7161, 5985.3062, 50.0417>>
//	TrafficCarQuatX[317] = 0.0118
//	TrafficCarQuatY[317] = 0.0032
//	TrafficCarQuatZ[317] = 0.2993
//	TrafficCarQuatW[317] = 0.9541
//	TrafficCarRecording[317] = 318
//	TrafficCarStartime[317] = 125202.0000
//	TrafficCarModel[317] = minivan
	
// ****  UBER RECORDED SET PIECE CARS  ****
//	SetPieceCarPos[0] = <<2583.5540, 360.2650, 107.7638>>
//	SetPieceCarQuatX[0] = -0.0032
//	SetPieceCarQuatY[0] = 0.0001
//	SetPieceCarQuatZ[0] = -0.0147
//	SetPieceCarQuatW[0] = 0.9999
//	SetPieceCarRecording[0] = 501
//	SetPieceCarStartime[0] = 0.0000
//	SetPieceCarRecordingSpeed[0] = 1.0000
//	SetPieceCarModel[0] = cheetah
	
	SetPieceCarPos[1] = <<2297.0762, 1144.4797, 78.6363>>
	SetPieceCarQuatX[1] = 0.0023
	SetPieceCarQuatY[1] = -0.0176
	SetPieceCarQuatZ[1] = 0.3532
	SetPieceCarQuatW[1] = 0.9354
	SetPieceCarRecording[1] = 400
	SetPieceCarStartime[1] = 16000.0000
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = Hauler
	
	//Recording 401 is for trailer
	
	SetPieceCarPos[2] = <<2284.0776, 1167.9307, 78.0065>>
	SetPieceCarQuatX[2] = -0.0169
	SetPieceCarQuatY[2] = 0.0226
	SetPieceCarQuatZ[2] = 0.3469
	SetPieceCarQuatW[2] = 0.9375
	SetPieceCarRecording[2] = 402
	SetPieceCarStartime[2] = 17000.0000
	SetPieceCarRecordingSpeed[2] = 1.0000
	SetPieceCarModel[2] = Packer
	
	//Recording 403 is for trailer
	
	SetPieceCarPos[3] = <<1886.9763, 2084.2434, 55.6819>>
	SetPieceCarQuatX[3] = -0.0082
	SetPieceCarQuatY[3] = 0.0048
	SetPieceCarQuatZ[3] = 0.0651
	SetPieceCarQuatW[3] = 0.9978
	SetPieceCarRecording[3] = 404
	SetPieceCarStartime[3] = 36000.0000
	SetPieceCarRecordingSpeed[3] = 1.0000
	SetPieceCarModel[3] = coach
	
	SetPieceCarPos[4] = <<1881.8495, 2073.8083, 54.9369>>
	SetPieceCarQuatX[4] = -0.0051
	SetPieceCarQuatY[4] = -0.0067
	SetPieceCarQuatZ[4] = 0.0702
	SetPieceCarQuatW[4] = 0.9975
	SetPieceCarRecording[4] = 405
	SetPieceCarStartime[4] = 36000.0000
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = BUS
	
	SetPieceCarPos[5] = <<2893.2883, 3642.3274, 52.8881>>
	SetPieceCarQuatX[5] = 0.0015
	SetPieceCarQuatY[5] = -0.0036
	SetPieceCarQuatZ[5] = -0.1382
	SetPieceCarQuatW[5] = 0.9904
	SetPieceCarRecording[5] = 406
	SetPieceCarStartime[5] = 72000.0000
	SetPieceCarRecordingSpeed[5] = 1.0000
	SetPieceCarModel[5] = Mule
ENDPROC

PROC UberCopVariables()
	TrafficCarPos[0] = <<2651.6782, 5083.7163, 44.2504>>
	TrafficCarQuatX[0] = 0.0013
	TrafficCarQuatY[0] = 0.0025
	TrafficCarQuatZ[0] = 0.0925
	TrafficCarQuatW[0] = 0.9957
	TrafficCarRecording[0] = 1
	TrafficCarStartime[0] = 2009284.0000
	TrafficCarModel[0] = asterope

	TrafficCarPos[1] = <<2578.6765, 5206.6890, 44.1481>>
	TrafficCarQuatX[1] = 0.0007
	TrafficCarQuatY[1] = -0.0009
	TrafficCarQuatZ[1] = 0.9908
	TrafficCarQuatW[1] = -0.1355
	TrafficCarRecording[1] = 2
	TrafficCarStartime[1] = 4224.0000
	TrafficCarModel[1] = emperor

	TrafficCarPos[2] = <<2564.1345, 5263.9688, 44.0797>>
	TrafficCarQuatX[2] = 0.0000
	TrafficCarQuatY[2] = 0.0007
	TrafficCarQuatZ[2] = 0.9896
	TrafficCarQuatW[2] = -0.1436
	TrafficCarRecording[2] = 3
	TrafficCarStartime[2] = 4422.0000
	TrafficCarModel[2] = emperor

	TrafficCarPos[3] = <<2571.2625, 5256.6226, 44.1779>>
	TrafficCarQuatX[3] = 0.0001
	TrafficCarQuatY[3] = 0.0007
	TrafficCarQuatZ[3] = 0.9922
	TrafficCarQuatW[3] = -0.1243
	TrafficCarRecording[3] = 4
	TrafficCarStartime[3] = 4620.0000
	TrafficCarModel[3] = journey

//	TrafficCarPos[4] = <<2590.1956, 5305.0815, 44.2940>>
//	TrafficCarQuatX[4] = -0.0008
//	TrafficCarQuatY[4] = 0.0001
//	TrafficCarQuatZ[4] = 0.1422
//	TrafficCarQuatW[4] = 0.9898
//	TrafficCarRecording[4] = 5
//	TrafficCarStartime[4] = 4950.0000
//	TrafficCarModel[4] = SURFER

	TrafficCarPos[5] = <<2567.1211, 5272.3496, 44.3493>>
	TrafficCarQuatX[5] = -0.0003
	TrafficCarQuatY[5] = 0.0005
	TrafficCarQuatZ[5] = 0.9923
	TrafficCarQuatW[5] = -0.1239
	TrafficCarRecording[5] = 6
	TrafficCarStartime[5] = 5280.0000
	TrafficCarModel[5] = asterope

	TrafficCarPos[6] = <<2581.3538, 5314.8896, 44.0127>>
	TrafficCarQuatX[6] = -0.0004
	TrafficCarQuatY[6] = -0.0005
	TrafficCarQuatZ[6] = 0.1847
	TrafficCarQuatW[6] = 0.9828
	TrafficCarRecording[6] = 7
	TrafficCarStartime[6] = 5610.0000
	TrafficCarModel[6] = emperor

	TrafficCarPos[7] = <<2548.6987, 5322.8833, 44.3774>>
	TrafficCarQuatX[7] = -0.0001
	TrafficCarQuatY[7] = 0.0006
	TrafficCarQuatZ[7] = 0.9920
	TrafficCarQuatW[7] = -0.1263
	TrafficCarRecording[7] = 8
	TrafficCarStartime[7] = 6006.0000
	TrafficCarModel[7] = journey

	TrafficCarPos[8] = <<2553.1313, 5327.9746, 44.2761>>
	TrafficCarQuatX[8] = 0.0001
	TrafficCarQuatY[8] = 0.0007
	TrafficCarQuatZ[8] = 0.9910
	TrafficCarQuatW[8] = -0.1340
	TrafficCarRecording[8] = 9
	TrafficCarStartime[8] = 6072.0000
	TrafficCarModel[8] = asterope

//	TrafficCarPos[9] = <<2581.7786, 5313.4814, 44.1521>>
//	TrafficCarQuatX[9] = -0.0006
//	TrafficCarQuatY[9] = 0.0002
//	TrafficCarQuatZ[9] = 0.1848
//	TrafficCarQuatW[9] = 0.9828
//	TrafficCarRecording[9] = 10
//	TrafficCarStartime[9] = 6138.0000
//	TrafficCarModel[9] = minivan

	TrafficCarPos[10] = <<2593.3813, 5293.0142, 44.3104>>
	TrafficCarQuatX[10] = -0.0007
	TrafficCarQuatY[10] = 0.0001
	TrafficCarQuatZ[10] = 0.1465
	TrafficCarQuatW[10] = 0.9892
	TrafficCarRecording[10] = 11
	TrafficCarStartime[10] = 6468.0000
	TrafficCarModel[10] = asterope

	TrafficCarPos[11] = <<2548.9211, 5343.3643, 44.1531>>
	TrafficCarQuatX[11] = 0.0001
	TrafficCarQuatY[11] = 0.0007
	TrafficCarQuatZ[11] = 0.9962
	TrafficCarQuatW[11] = -0.0873
	TrafficCarRecording[11] = 12
	TrafficCarStartime[11] = 6930.0000
	TrafficCarModel[11] = asterope

	TrafficCarPos[12] = <<2539.3406, 5358.3770, 43.9659>>
	TrafficCarQuatX[12] = 0.0002
	TrafficCarQuatY[12] = 0.0002
	TrafficCarQuatZ[12] = 0.9915
	TrafficCarQuatW[12] = -0.1300
	TrafficCarRecording[12] = 13
	TrafficCarStartime[12] = 7326.0000
	TrafficCarModel[12] = emperor

//	TrafficCarPos[13] = <<2562.9797, 5398.0762, 43.9368>>
//	TrafficCarQuatX[13] = -0.0001
//	TrafficCarQuatY[13] = 0.0000
//	TrafficCarQuatZ[13] = 0.1453
//	TrafficCarQuatW[13] = 0.9894
//	TrafficCarRecording[13] = 14
//	TrafficCarStartime[13] = 8316.0000
//	TrafficCarModel[13] = emperor

	TrafficCarPos[14] = <<2547.9246, 5346.3848, 44.3941>>
	TrafficCarQuatX[14] = -0.0001
	TrafficCarQuatY[14] = 0.0004
	TrafficCarQuatZ[14] = 0.9940
	TrafficCarQuatW[14] = -0.1090
	TrafficCarRecording[14] = 15
	TrafficCarStartime[14] = 8646.0000
	TrafficCarModel[14] = minivan

	TrafficCarPos[15] = <<2554.2639, 5406.5571, 44.3017>>
	TrafficCarQuatX[15] = -0.0015
	TrafficCarQuatY[15] = -0.0006
	TrafficCarQuatZ[15] = 0.1376
	TrafficCarQuatW[15] = 0.9905
	TrafficCarRecording[15] = 16
	TrafficCarStartime[15] = 9174.0000
	TrafficCarModel[15] = journey

	TrafficCarPos[16] = <<2556.2463, 5399.6890, 44.2142>>
	TrafficCarQuatX[16] = -0.0001
	TrafficCarQuatY[16] = 0.0001
	TrafficCarQuatZ[16] = 0.1420
	TrafficCarQuatW[16] = 0.9899
	TrafficCarRecording[16] = 17
	TrafficCarStartime[16] = 10164.0000
	TrafficCarModel[16] = asterope

//	TrafficCarPos[17] = <<2542.4026, 5445.0840, 44.1067>>
//	TrafficCarQuatX[17] = 0.0002
//	TrafficCarQuatY[17] = 0.0001
//	TrafficCarQuatZ[17] = 0.1531
//	TrafficCarQuatW[17] = 0.9882
//	TrafficCarRecording[17] = 18
//	TrafficCarStartime[17] = 10230.0000
//	TrafficCarModel[17] = minivan

	TrafficCarPos[18] = <<2546.3093, 5449.5732, 44.2454>>
	TrafficCarQuatX[18] = -0.0001
	TrafficCarQuatY[18] = 0.0001
	TrafficCarQuatZ[18] = 0.1520
	TrafficCarQuatW[18] = 0.9884
	TrafficCarRecording[18] = 19
	TrafficCarStartime[18] = 10890.0000
	TrafficCarModel[18] = asterope

	TrafficCarPos[19] = <<2512.8413, 5449.7285, 44.3785>>
	TrafficCarQuatX[19] = -0.0000
	TrafficCarQuatY[19] = -0.0002
	TrafficCarQuatZ[19] = 0.9875
	TrafficCarQuatW[19] = -0.1577
	TrafficCarRecording[19] = 20
	TrafficCarStartime[19] = 10956.0000
	TrafficCarModel[19] = minivan

	TrafficCarPos[20] = <<2536.3279, 5478.3750, 43.9978>>
	TrafficCarQuatX[20] = 0.0006
	TrafficCarQuatY[20] = 0.0001
	TrafficCarQuatZ[20] = 0.1958
	TrafficCarQuatW[20] = 0.9806
	TrafficCarRecording[20] = 21
	TrafficCarStartime[20] = 11484.0000
	TrafficCarModel[20] = emperor

	TrafficCarPos[21] = <<2493.3911, 5506.5771, 44.1779>>
	TrafficCarQuatX[21] = 0.0001
	TrafficCarQuatY[21] = -0.0016
	TrafficCarQuatZ[21] = 0.9845
	TrafficCarQuatW[21] = -0.1754
	TrafficCarRecording[21] = 22
	TrafficCarStartime[21] = 12144.0000
	TrafficCarModel[21] = journey

	TrafficCarPos[22] = <<2509.6421, 5537.3931, 44.1639>>
	TrafficCarQuatX[22] = 0.0009
	TrafficCarQuatY[22] = 0.0002
	TrafficCarQuatZ[22] = 0.1787
	TrafficCarQuatW[22] = 0.9839
	TrafficCarRecording[22] = 23
	TrafficCarStartime[22] = 12606.0000
	TrafficCarModel[22] = emperor

	TrafficCarPos[23] = <<2503.3555, 5556.0645, 44.3684>>
	TrafficCarQuatX[23] = 0.0009
	TrafficCarQuatY[23] = 0.0003
	TrafficCarQuatZ[23] = 0.1796
	TrafficCarQuatW[23] = 0.9837
	TrafficCarRecording[23] = 24
	TrafficCarStartime[23] = 13398.0000
	TrafficCarModel[23] = asterope

//	TrafficCarPos[24] = <<2509.5007, 5555.1055, 44.4747>>
//	TrafficCarQuatX[24] = 0.0008
//	TrafficCarQuatY[24] = 0.0004
//	TrafficCarQuatZ[24] = 0.1768
//	TrafficCarQuatW[24] = 0.9843
//	TrafficCarRecording[24] = 25
//	TrafficCarStartime[24] = 13794.0000
//	TrafficCarModel[24] = asterope

//	TrafficCarPos[25] = <<2496.7876, 5572.8652, 44.5145>>
//	TrafficCarQuatX[25] = 0.0009
//	TrafficCarQuatY[25] = 0.0003
//	TrafficCarQuatZ[25] = 0.1841
//	TrafficCarQuatW[25] = 0.9829
//	TrafficCarRecording[25] = 26
//	TrafficCarStartime[25] = 13926.0000
//	TrafficCarModel[25] = SURFER

//	TrafficCarPos[26] = <<2485.2317, 5602.1167, 44.5838>>
//	TrafficCarQuatX[26] = 0.0009
//	TrafficCarQuatY[26] = 0.0002
//	TrafficCarQuatZ[26] = 0.1912
//	TrafficCarQuatW[26] = 0.9816
//	TrafficCarRecording[26] = 27
//	TrafficCarStartime[26] = 14520.0000
//	TrafficCarModel[26] = asterope

//	TrafficCarPos[27] = <<2455.7356, 5612.8857, 44.4778>>
//	TrafficCarQuatX[27] = 0.0002
//	TrafficCarQuatY[27] = -0.0012
//	TrafficCarQuatZ[27] = 0.9812
//	TrafficCarQuatW[27] = -0.1932
//	TrafficCarRecording[27] = 28
//	TrafficCarStartime[27] = 15114.0000
//	TrafficCarModel[27] = minivan

	TrafficCarPos[28] = <<2494.8794, 5594.3193, 44.4232>>
	TrafficCarQuatX[28] = 0.0012
	TrafficCarQuatY[28] = 0.0000
	TrafficCarQuatZ[28] = 0.1843
	TrafficCarQuatW[28] = 0.9829
	TrafficCarRecording[28] = 29
	TrafficCarStartime[28] = 15708.0000
	TrafficCarModel[28] = minivan

	TrafficCarPos[29] = <<2445.1016, 5638.9839, 44.4275>>
	TrafficCarQuatX[29] = 0.0004
	TrafficCarQuatY[29] = -0.0014
	TrafficCarQuatZ[29] = 0.9702
	TrafficCarQuatW[29] = -0.2423
	TrafficCarRecording[29] = 30
	TrafficCarStartime[29] = 15840.0000
	TrafficCarModel[29] = emperor

	TrafficCarPos[30] = <<2461.4565, 5658.5557, 44.8311>>
	TrafficCarQuatX[30] = 0.0013
	TrafficCarQuatY[30] = 0.0003
	TrafficCarQuatZ[30] = 0.2075
	TrafficCarQuatW[30] = 0.9782
	TrafficCarRecording[30] = 31
	TrafficCarStartime[30] = 16302.0000
	TrafficCarModel[30] = journey

	TrafficCarPos[31] = <<2427.9761, 5673.8774, 44.6726>>
	TrafficCarQuatX[31] = 0.0004
	TrafficCarQuatY[31] = -0.0017
	TrafficCarQuatZ[31] = 0.9732
	TrafficCarQuatW[31] = -0.2300
	TrafficCarRecording[31] = 32
	TrafficCarStartime[31] = 17094.0000
	TrafficCarModel[31] = journey

//	TrafficCarPos[32] = <<2425.5408, 5665.1904, 44.6124>>
//	TrafficCarQuatX[32] = 0.0006
//	TrafficCarQuatY[32] = -0.0015
//	TrafficCarQuatZ[32] = 0.9816
//	TrafficCarQuatW[32] = -0.1911
//	TrafficCarRecording[32] = 33
//	TrafficCarStartime[32] = 17622.0000
//	TrafficCarModel[32] = asterope

	TrafficCarPos[33] = <<2400.9226, 5713.9766, 45.1135>>
	TrafficCarQuatX[33] = 0.0007
	TrafficCarQuatY[33] = -0.0030
	TrafficCarQuatZ[33] = 0.9697
	TrafficCarQuatW[33] = -0.2445
	TrafficCarRecording[33] = 34
	TrafficCarStartime[33] = 18084.0000
	TrafficCarModel[33] = SURFER

	TrafficCarPos[34] = <<2393.4893, 5737.1675, 45.0127>>
	TrafficCarQuatX[34] = 0.0010
	TrafficCarQuatY[34] = -0.0043
	TrafficCarQuatZ[34] = 0.9713
	TrafficCarQuatW[34] = -0.2379
	TrafficCarRecording[34] = 35
	TrafficCarStartime[34] = 18810.0000
	TrafficCarModel[34] = emperor

	TrafficCarPos[35] = <<2424.1030, 5737.5229, 45.1532>>
	TrafficCarQuatX[35] = 0.0035
	TrafficCarQuatY[35] = 0.0008
	TrafficCarQuatZ[35] = 0.2295
	TrafficCarQuatW[35] = 0.9733
	TrafficCarRecording[35] = 36
	TrafficCarStartime[35] = 18876.0000
	TrafficCarModel[35] = asterope

	TrafficCarPos[36] = <<2390.8848, 5743.3213, 45.2131>>
	TrafficCarQuatX[36] = 0.0011
	TrafficCarQuatY[36] = -0.0043
	TrafficCarQuatZ[36] = 0.9535
	TrafficCarQuatW[36] = -0.3014
	TrafficCarRecording[36] = 37
	TrafficCarStartime[36] = 19338.0000
	TrafficCarModel[36] = minivan

	TrafficCarPos[37] = <<2412.9656, 5770.1772, 45.1403>>
	TrafficCarQuatX[37] = 0.0039
	TrafficCarQuatY[37] = 0.0009
	TrafficCarQuatZ[37] = 0.2406
	TrafficCarQuatW[37] = 0.9706
	TrafficCarRecording[37] = 38
	TrafficCarStartime[37] = 19404.0000
	TrafficCarModel[37] = emperor

	TrafficCarPos[38] = <<2386.7268, 5749.7593, 45.3162>>
	TrafficCarQuatX[38] = 0.0011
	TrafficCarQuatY[38] = -0.0046
	TrafficCarQuatZ[38] = 0.9670
	TrafficCarQuatW[38] = -0.2546
	TrafficCarRecording[38] = 39
	TrafficCarStartime[38] = 19734.0000
	TrafficCarModel[38] = asterope

	TrafficCarPos[39] = <<2379.0435, 5762.4819, 45.2977>>
	TrafficCarQuatX[39] = 0.0015
	TrafficCarQuatY[39] = -0.0046
	TrafficCarQuatZ[39] = 0.9501
	TrafficCarQuatW[39] = -0.3120
	TrafficCarRecording[39] = 40
	TrafficCarStartime[39] = 20328.0000
	TrafficCarModel[39] = emperor

	TrafficCarPos[40] = <<2341.2683, 5808.5879, 46.1418>>
	TrafficCarQuatX[40] = -0.0018
	TrafficCarQuatY[40] = -0.0084
	TrafficCarQuatZ[40] = 0.9447
	TrafficCarQuatW[40] = -0.3278
	TrafficCarRecording[40] = 41
	TrafficCarStartime[40] = 21120.0000
	TrafficCarModel[40] = journey

	TrafficCarPos[41] = <<2334.0640, 5818.2275, 46.4928>>
	TrafficCarQuatX[41] = -0.0019
	TrafficCarQuatY[41] = -0.0082
	TrafficCarQuatZ[41] = 0.9489
	TrafficCarQuatW[41] = -0.3156
	TrafficCarRecording[41] = 42
	TrafficCarStartime[41] = 21450.0000
	TrafficCarModel[41] = asterope

	TrafficCarPos[42] = <<2341.3201, 5808.5200, 46.3244>>
	TrafficCarQuatX[42] = -0.0015
	TrafficCarQuatY[42] = -0.0085
	TrafficCarQuatZ[42] = 0.9358
	TrafficCarQuatW[42] = -0.3523
	TrafficCarRecording[42] = 43
	TrafficCarStartime[42] = 21648.0000
	TrafficCarModel[42] = SURFER

//	TrafficCarPos[43] = <<2317.0034, 5848.4966, 46.8214>>
//	TrafficCarQuatX[43] = 0.0030
//	TrafficCarQuatY[43] = -0.0086
//	TrafficCarQuatZ[43] = 0.9356
//	TrafficCarQuatW[43] = -0.3530
//	TrafficCarRecording[43] = 44
//	TrafficCarStartime[43] = 22308.0000
//	TrafficCarModel[43] = emperor

	TrafficCarPos[44] = <<2356.7549, 5852.9966, 46.8044>>
	TrafficCarQuatX[44] = 0.0077
	TrafficCarQuatY[44] = 0.0023
	TrafficCarQuatZ[44] = 0.2911
	TrafficCarQuatW[44] = 0.9567
	TrafficCarRecording[44] = 45
	TrafficCarStartime[44] = 22440.0000
	TrafficCarModel[44] = minivan

	TrafficCarPos[45] = <<2306.9895, 5851.7124, 47.3458>>
	TrafficCarQuatX[45] = 0.0013
	TrafficCarQuatY[45] = -0.0100
	TrafficCarQuatZ[45] = 0.9375
	TrafficCarQuatW[45] = -0.3479
	TrafficCarRecording[45] = 46
	TrafficCarStartime[45] = 22638.0000
	TrafficCarModel[45] = journey

//	TrafficCarPos[46] = <<2308.6541, 5858.1802, 47.0598>>
//	TrafficCarQuatX[46] = 0.0039
//	TrafficCarQuatY[46] = -0.0095
//	TrafficCarQuatZ[46] = 0.9207
//	TrafficCarQuatW[46] = -0.3901
//	TrafficCarRecording[46] = 47
//	TrafficCarStartime[46] = 22836.0000
//	TrafficCarModel[46] = emperor

//	TrafficCarPos[47] = <<2313.1885, 5853.4268, 47.2034>>
//	TrafficCarQuatX[47] = 0.0034
//	TrafficCarQuatY[47] = -0.0093
//	TrafficCarQuatZ[47] = 0.9349
//	TrafficCarQuatW[47] = -0.3547
//	TrafficCarRecording[47] = 48
//	TrafficCarStartime[47] = 23892.0000
//	TrafficCarModel[47] = SURFER

	TrafficCarPos[48] = <<2305.7908, 5853.1348, 47.0949>>
	TrafficCarQuatX[48] = 0.0004
	TrafficCarQuatY[48] = -0.0102
	TrafficCarQuatZ[48] = 0.9353
	TrafficCarQuatW[48] = -0.3536
	TrafficCarRecording[48] = 49
	TrafficCarStartime[48] = 24024.0000
	TrafficCarModel[48] = journey

	TrafficCarPos[49] = <<2264.7849, 5905.3477, 48.6000>>
	TrafficCarQuatX[49] = 0.0056
	TrafficCarQuatY[49] = -0.0124
	TrafficCarQuatZ[49] = 0.9091
	TrafficCarQuatW[49] = -0.4164
	TrafficCarRecording[49] = 50
	TrafficCarStartime[49] = 24222.0000
	TrafficCarModel[49] = asterope

	TrafficCarPos[50] = <<2276.7534, 5892.9897, 48.1198>>
	TrafficCarQuatX[50] = 0.0046
	TrafficCarQuatY[50] = -0.0105
	TrafficCarQuatZ[50] = 0.9142
	TrafficCarQuatW[50] = -0.4052
	TrafficCarRecording[50] = 51
	TrafficCarStartime[50] = 24354.0000
	TrafficCarModel[50] = emperor

	TrafficCarPos[51] = <<2294.0850, 5934.6953, 48.3150>>
	TrafficCarQuatX[51] = 0.0128
	TrafficCarQuatY[51] = 0.0035
	TrafficCarQuatZ[51] = 0.3199
	TrafficCarQuatW[51] = 0.9473
	TrafficCarRecording[51] = 52
	TrafficCarStartime[51] = 24618.0000
	TrafficCarModel[51] = minivan

//	TrafficCarPos[52] = <<2276.6106, 5885.2847, 48.2309>>
//	TrafficCarQuatX[52] = 0.0000
//	TrafficCarQuatY[52] = -0.0121
//	TrafficCarQuatZ[52] = 0.9275
//	TrafficCarQuatW[52] = -0.3737
//	TrafficCarRecording[52] = 53
//	TrafficCarStartime[52] = 25410.0000
//	TrafficCarModel[52] = asterope

//	TrafficCarPos[53] = <<2237.6968, 5931.9976, 49.8146>>
//	TrafficCarQuatX[53] = 0.0009
//	TrafficCarQuatY[53] = -0.0120
//	TrafficCarQuatZ[53] = 0.9183
//	TrafficCarQuatW[53] = -0.3956
//	TrafficCarRecording[53] = 54
//	TrafficCarStartime[53] = 25806.0000
//	TrafficCarModel[53] = asterope

//	TrafficCarPos[54] = <<2227.3838, 5941.6563, 50.2338>>
//	TrafficCarQuatX[54] = 0.0008
//	TrafficCarQuatY[54] = -0.0123
//	TrafficCarQuatZ[54] = 0.9111
//	TrafficCarQuatW[54] = -0.4119
//	TrafficCarRecording[54] = 55
//	TrafficCarStartime[54] = 26202.0000
//	TrafficCarModel[54] = journey

	TrafficCarPos[55] = <<2245.7124, 5916.9033, 49.1332>>
	TrafficCarQuatX[55] = 0.0036
	TrafficCarQuatY[55] = -0.0122
	TrafficCarQuatZ[55] = 0.9221
	TrafficCarQuatW[55] = -0.3867
	TrafficCarRecording[55] = 56
	TrafficCarStartime[55] = 26334.0000
	TrafficCarModel[55] = emperor

	TrafficCarPos[56] = <<2194.0139, 5963.9224, 50.5311>>
	TrafficCarQuatX[56] = 0.0019
	TrafficCarQuatY[56] = -0.0040
	TrafficCarQuatZ[56] = 0.9006
	TrafficCarQuatW[56] = -0.4346
	TrafficCarRecording[56] = 57
	TrafficCarStartime[56] = 26532.0000
	TrafficCarModel[56] = journey

//	TrafficCarPos[57] = <<2189.9368, 5967.4883, 50.7580>>
//	TrafficCarQuatX[57] = 0.0020
//	TrafficCarQuatY[57] = -0.0042
//	TrafficCarQuatZ[57] = 0.8992
//	TrafficCarQuatW[57] = -0.4376
//	TrafficCarRecording[57] = 58
//	TrafficCarStartime[57] = 26664.0000
//	TrafficCarModel[57] = SURFER

	TrafficCarPos[58] = <<2225.7893, 5935.9399, 49.7968>>
	TrafficCarQuatX[58] = 0.0087
	TrafficCarQuatY[58] = -0.0080
	TrafficCarQuatZ[58] = 0.9007
	TrafficCarQuatW[58] = -0.4343
	TrafficCarRecording[58] = 59
	TrafficCarStartime[58] = 26928.0000
	TrafficCarModel[58] = emperor

//	TrafficCarPos[59] = <<2173.9656, 5987.0151, 51.0622>>
//	TrafficCarQuatX[59] = 0.0010
//	TrafficCarQuatY[59] = -0.0011
//	TrafficCarQuatZ[59] = 0.9169
//	TrafficCarQuatW[59] = -0.3992
//	TrafficCarRecording[59] = 60
//	TrafficCarStartime[59] = 27324.0000
//	TrafficCarModel[59] = minivan

//	TrafficCarPos[60] = <<2196.2571, 5969.2012, 50.4885>>
//	TrafficCarQuatX[60] = 0.0010
//	TrafficCarQuatY[60] = -0.0046
//	TrafficCarQuatZ[60] = 0.8977
//	TrafficCarQuatW[60] = -0.4406
//	TrafficCarRecording[60] = 61
//	TrafficCarStartime[60] = 27918.0000
//	TrafficCarModel[60] = emperor

	TrafficCarPos[61] = <<2151.9482, 6004.3169, 50.8595>>
	TrafficCarQuatX[61] = -0.0007
	TrafficCarQuatY[61] = 0.0013
	TrafficCarQuatZ[61] = 0.8772
	TrafficCarQuatW[61] = -0.4801
	TrafficCarRecording[61] = 62
	TrafficCarStartime[61] = 27918.0000
	TrafficCarModel[61] = asterope

	TrafficCarPos[62] = <<2143.4595, 6010.6953, 50.9416>>
	TrafficCarQuatX[62] = -0.0006
	TrafficCarQuatY[62] = 0.0012
	TrafficCarQuatZ[62] = 0.8935
	TrafficCarQuatW[62] = -0.4490
	TrafficCarRecording[62] = 63
	TrafficCarStartime[62] = 28248.0000
	TrafficCarModel[62] = asterope

//	TrafficCarPos[63] = <<2170.3572, 5983.4600, 50.6688>>
//	TrafficCarQuatX[63] = 0.0008
//	TrafficCarQuatY[63] = -0.0015
//	TrafficCarQuatZ[63] = 0.8815
//	TrafficCarQuatW[63] = -0.4721
//	TrafficCarRecording[63] = 64
//	TrafficCarStartime[63] = 28644.0000
//	TrafficCarModel[63] = emperor

//	TrafficCarPos[64] = <<2129.6682, 6013.3916, 50.6789>>
//	TrafficCarQuatX[64] = -0.0003
//	TrafficCarQuatY[64] = 0.0048
//	TrafficCarQuatZ[64] = 0.9053
//	TrafficCarQuatW[64] = -0.4248
//	TrafficCarRecording[64] = 65
//	TrafficCarStartime[64] = 29172.0000
//	TrafficCarModel[64] = asterope

//	TrafficCarPos[65] = <<2122.8330, 6026.0000, 50.7882>>
//	TrafficCarQuatX[65] = -0.0033
//	TrafficCarQuatY[65] = 0.0040
//	TrafficCarQuatZ[65] = 0.8944
//	TrafficCarQuatW[65] = -0.4472
//	TrafficCarRecording[65] = 66
//	TrafficCarStartime[65] = 29634.0000
//	TrafficCarModel[65] = asterope

	TrafficCarPos[66] = <<2128.2231, 6021.9575, 50.5741>>
	TrafficCarQuatX[66] = -0.0034
	TrafficCarQuatY[66] = 0.0038
	TrafficCarQuatZ[66] = 0.8762
	TrafficCarQuatW[66] = -0.4820
	TrafficCarRecording[66] = 67
	TrafficCarStartime[66] = 30096.0000
	TrafficCarModel[66] = emperor

//	TrafficCarPos[67] = <<2079.1140, 6059.2349, 49.0249>>
//	TrafficCarQuatX[67] = -0.0062
//	TrafficCarQuatY[67] = 0.0124
//	TrafficCarQuatZ[67] = 0.9006
//	TrafficCarQuatW[67] = -0.4343
//	TrafficCarRecording[67] = 68
//	TrafficCarStartime[67] = 30228.0000
//	TrafficCarModel[67] = journey

	TrafficCarPos[68] = <<2106.0881, 6030.7827, 50.0777>>
	TrafficCarQuatX[68] = -0.0059
	TrafficCarQuatY[68] = 0.0126
	TrafficCarQuatZ[68] = 0.9114
	TrafficCarQuatW[68] = -0.4112
	TrafficCarRecording[68] = 69
	TrafficCarStartime[68] = 30690.0000
	TrafficCarModel[68] = emperor

//	TrafficCarPos[69] = <<2080.8591, 6057.7676, 49.0091>>
//	TrafficCarQuatX[69] = -0.0061
//	TrafficCarQuatY[69] = 0.0132
//	TrafficCarQuatZ[69] = 0.9050
//	TrafficCarQuatW[69] = -0.4251
//	TrafficCarRecording[69] = 70
//	TrafficCarStartime[69] = 30888.0000
//	TrafficCarModel[69] = emperor

//	TrafficCarPos[70] = <<2077.8420, 6060.2080, 49.3048>>
//	TrafficCarQuatX[70] = -0.0055
//	TrafficCarQuatY[70] = 0.0132
//	TrafficCarQuatZ[70] = 0.9220
//	TrafficCarQuatW[70] = -0.3870
//	TrafficCarRecording[70] = 71
//	TrafficCarStartime[70] = 31152.0000
//	TrafficCarModel[70] = minivan

	TrafficCarPos[71] = <<2077.3833, 6053.5493, 49.1698>>
	TrafficCarQuatX[71] = -0.0056
	TrafficCarQuatY[71] = 0.0132
	TrafficCarQuatZ[71] = 0.9220
	TrafficCarQuatW[71] = -0.3870
	TrafficCarRecording[71] = 72
	TrafficCarStartime[71] = 31482.0000
	TrafficCarModel[71] = asterope

//	TrafficCarPos[72] = <<2071.9292, 6065.4019, 48.6676>>
//	TrafficCarQuatX[72] = -0.0064
//	TrafficCarQuatY[72] = 0.0128
//	TrafficCarQuatZ[72] = 0.8953
//	TrafficCarQuatW[72] = -0.4451
//	TrafficCarRecording[72] = 73
//	TrafficCarStartime[72] = 31812.0000
//	TrafficCarModel[72] = emperor

	TrafficCarPos[73] = <<2025.2740, 6101.4082, 47.5248>>
	TrafficCarQuatX[73] = -0.0080
	TrafficCarQuatY[73] = 0.0081
	TrafficCarQuatZ[73] = 0.9381
	TrafficCarQuatW[73] = -0.3461
	TrafficCarRecording[73] = 74
	TrafficCarStartime[73] = 31878.0000
	TrafficCarModel[73] = SURFER

	TrafficCarPos[74] = <<2056.4045, 6071.6440, 48.3698>>
	TrafficCarQuatX[74] = -0.0049
	TrafficCarQuatY[74] = 0.0120
	TrafficCarQuatZ[74] = 0.9275
	TrafficCarQuatW[74] = -0.3735
	TrafficCarRecording[74] = 75
	TrafficCarStartime[74] = 32208.0000
	TrafficCarModel[74] = minivan

	TrafficCarPos[75] = <<2016.8806, 6117.5591, 47.1699>>
	TrafficCarQuatX[75] = -0.0044
	TrafficCarQuatY[75] = 0.0108
	TrafficCarQuatZ[75] = 0.9266
	TrafficCarQuatW[75] = -0.3760
	TrafficCarRecording[75] = 76
	TrafficCarStartime[75] = 32274.0000
	TrafficCarModel[75] = asterope

	TrafficCarPos[76] = <<2064.5444, 6124.3882, 49.5683>>
	TrafficCarQuatX[76] = -0.0121
	TrafficCarQuatY[76] = -0.0050
	TrafficCarQuatZ[76] = 0.4074
	TrafficCarQuatW[76] = 0.9131
	TrafficCarRecording[76] = 77
	TrafficCarStartime[76] = 32274.0000 - 1000.0
	TrafficCarModel[76] = journey

//	TrafficCarPos[77] = <<2049.0120, 6085.9404, 48.1355>>
//	TrafficCarQuatX[77] = -0.0046
//	TrafficCarQuatY[77] = 0.0104
//	TrafficCarQuatZ[77] = 0.9194
//	TrafficCarQuatW[77] = -0.3932
//	TrafficCarRecording[77] = 78
//	TrafficCarStartime[77] = 32670.0000
//	TrafficCarModel[77] = asterope

	TrafficCarPos[78] = <<2011.3207, 6123.3882, 46.7588>>
	TrafficCarQuatX[78] = -0.0037
	TrafficCarQuatY[78] = 0.0097
	TrafficCarQuatZ[78] = 0.9278
	TrafficCarQuatW[78] = -0.3729
	TrafficCarRecording[78] = 79
	TrafficCarStartime[78] = 32868.0000
	TrafficCarModel[78] = asterope

	TrafficCarPos[79] = <<2022.5339, 6104.1489, 47.5842>>
	TrafficCarQuatX[79] = -0.0083
	TrafficCarQuatY[79] = 0.0076
	TrafficCarQuatZ[79] = 0.9083
	TrafficCarQuatW[79] = -0.4181
	TrafficCarRecording[79] = 80
	TrafficCarStartime[79] = 33264.0000
	TrafficCarModel[79] = minivan

	TrafficCarPos[80] = <<1988.7322, 6150.8906, 45.9257>>
	TrafficCarQuatX[80] = -0.0041
	TrafficCarQuatY[80] = 0.0108
	TrafficCarQuatZ[80] = 0.9411
	TrafficCarQuatW[80] = -0.3378
	TrafficCarRecording[80] = 81
	TrafficCarStartime[80] = 33396.0000
	TrafficCarModel[80] = emperor

	TrafficCarPos[81] = <<1985.0082, 6155.7656, 45.9220>>
	TrafficCarQuatX[81] = -0.0028
	TrafficCarQuatY[81] = 0.0112
	TrafficCarQuatZ[81] = 0.9647
	TrafficCarQuatW[81] = -0.2630
	TrafficCarRecording[81] = 82
	TrafficCarStartime[81] = 33792.0000
	TrafficCarModel[81] = minivan

	TrafficCarPos[82] = <<1955.8132, 6202.2827, 44.6369>>
	TrafficCarQuatX[82] = -0.0026
	TrafficCarQuatY[82] = 0.0127
	TrafficCarQuatZ[82] = 0.9876
	TrafficCarQuatW[82] = -0.1563
	TrafficCarRecording[82] = 83
	TrafficCarStartime[82] = 34650.0000
	TrafficCarModel[82] = asterope

	TrafficCarPos[83] = <<1951.5308, 6214.4453, 44.0232>>
	TrafficCarQuatX[83] = -0.0024
	TrafficCarQuatY[83] = 0.0129
	TrafficCarQuatZ[83] = 0.9803
	TrafficCarQuatW[83] = -0.1973
	TrafficCarRecording[83] = 84
	TrafficCarStartime[83] = 36498.0000
	TrafficCarModel[83] = emperor

	TrafficCarPos[84] = <<1941.9100, 6251.7842, 43.0887>>
	TrafficCarQuatX[84] = -0.0024
	TrafficCarQuatY[84] = 0.0126
	TrafficCarQuatZ[84] = 0.9769
	TrafficCarQuatW[84] = -0.2135
	TrafficCarRecording[84] = 85
	TrafficCarStartime[84] = 37224.0000
	TrafficCarModel[84] = journey

//	TrafficCarPos[85] = <<1899.3508, 6304.0464, 41.7675>>
//	TrafficCarQuatX[85] = -0.0045
//	TrafficCarQuatY[85] = 0.0089
//	TrafficCarQuatZ[85] = 0.9075
//	TrafficCarQuatW[85] = -0.4200
//	TrafficCarRecording[85] = 86
//	TrafficCarStartime[85] = 37488.0000
//	TrafficCarModel[85] = asterope

	TrafficCarPos[86] = <<1895.5120, 6313.5635, 41.4656>>
	TrafficCarQuatX[86] = -0.0063
	TrafficCarQuatY[86] = 0.0114
	TrafficCarQuatZ[86] = 0.8745
	TrafficCarQuatW[86] = -0.4849
	TrafficCarRecording[86] = 87
	TrafficCarStartime[86] = 37686.0000
	TrafficCarModel[86] = asterope

	TrafficCarPos[87] = <<1862.3741, 6328.8042, 40.3474>>
	TrafficCarQuatX[87] = -0.0113
	TrafficCarQuatY[87] = 0.0158
	TrafficCarQuatZ[87] = 0.8149
	TrafficCarQuatW[87] = -0.5793
	TrafficCarRecording[87] = 88
	TrafficCarStartime[87] = 38346.0000
	TrafficCarModel[87] = SURFER

	TrafficCarPos[88] = <<1891.9431, 6349.6519, 41.5862>>
	TrafficCarQuatX[88] = -0.0107
	TrafficCarQuatY[88] = -0.0078
	TrafficCarQuatZ[88] = 0.5670
	TrafficCarQuatW[88] = 0.8236
	TrafficCarRecording[88] = 89
	TrafficCarStartime[88] = 38412.0000
	TrafficCarModel[88] = minivan

	TrafficCarPos[89] = <<1846.8118, 6363.4038, 40.1519>>
	TrafficCarQuatX[89] = -0.0114
	TrafficCarQuatY[89] = -0.0087
	TrafficCarQuatZ[89] = 0.6037
	TrafficCarQuatW[89] = 0.7971
	TrafficCarRecording[89] = 90
	TrafficCarStartime[89] = 39204.0000
	TrafficCarModel[89] = emperor

	TrafficCarPos[90] = <<1784.3910, 6370.2109, 37.9927>>
	TrafficCarQuatX[90] = -0.0174
	TrafficCarQuatY[90] = -0.0171
	TrafficCarQuatZ[90] = 0.6835
	TrafficCarQuatW[90] = 0.7295
	TrafficCarRecording[90] = 91
	TrafficCarStartime[90] = 41778.0000
	TrafficCarModel[90] = asterope

	ParkedCarPos[0] = <<1732.1554, 6403.5503, 34.4566>>
	ParkedCarQuatX[0] = -0.0159
	ParkedCarQuatY[0] = -0.0254
	ParkedCarQuatZ[0] = -0.2550
	ParkedCarQuatW[0] = 0.9665
	ParkedCarModel[0] = minivan

	TrafficCarPos[91] = <<1773.8611, 6343.8247, 36.3343>>
	TrafficCarQuatX[91] = -0.0130
	TrafficCarQuatY[91] = 0.0190
	TrafficCarQuatZ[91] = 0.7427
	TrafficCarQuatW[91] = -0.6692
	TrafficCarRecording[91] = 92
	TrafficCarStartime[91] = 41910.0000
	TrafficCarModel[91] = journey

	TrafficCarPos[92] = <<1725.8148, 6352.4946, 33.9923>>
	TrafficCarQuatX[92] = -0.0156
	TrafficCarQuatY[92] = 0.0212
	TrafficCarQuatZ[92] = 0.7943
	TrafficCarQuatW[92] = -0.6070
	TrafficCarRecording[92] = 93
	TrafficCarStartime[92] = 42108.0000
	TrafficCarModel[92] = asterope

	TrafficCarPos[93] = <<1693.9230, 6380.9585, 31.6176>>
	TrafficCarQuatX[93] = -0.0288
	TrafficCarQuatY[93] = -0.0220
	TrafficCarQuatZ[93] = 0.6188
	TrafficCarQuatW[93] = 0.7847
	TrafficCarRecording[93] = 94
	TrafficCarStartime[93] = 42702.0000
	TrafficCarModel[93] = asterope

	TrafficCarPos[94] = <<1675.1924, 6368.1792, 31.2057>>
	TrafficCarQuatX[94] = -0.0170
	TrafficCarQuatY[94] = 0.0230
	TrafficCarQuatZ[94] = 0.8127
	TrafficCarQuatW[94] = -0.5819
	TrafficCarRecording[94] = 95
	TrafficCarStartime[94] = 42900.0000
	TrafficCarModel[94] = asterope

	TrafficCarPos[95] = <<1648.9746, 6371.3037, 29.7276>>
	TrafficCarQuatX[95] = -0.0172
	TrafficCarQuatY[95] = 0.0237
	TrafficCarQuatZ[95] = 0.8060
	TrafficCarQuatW[95] = -0.5912
	TrafficCarRecording[95] = 96
	TrafficCarStartime[95] = 43428.0000
	TrafficCarModel[95] = SURFER

	TrafficCarPos[96] = <<1621.5427, 6382.0889, 27.9963>>
	TrafficCarQuatX[96] = -0.0179
	TrafficCarQuatY[96] = 0.0260
	TrafficCarQuatZ[96] = 0.8220
	TrafficCarQuatW[96] = -0.5686
	TrafficCarRecording[96] = 97
	TrafficCarStartime[96] = 44088.0000
	TrafficCarModel[96] = journey

	TrafficCarPos[97] = <<1613.9684, 6404.0195, 26.7857>>
	TrafficCarQuatX[97] = -0.0289
	TrafficCarQuatY[97] = -0.0185
	TrafficCarQuatZ[97] = 0.5576
	TrafficCarQuatW[97] = 0.8294
	TrafficCarRecording[97] = 98
	TrafficCarStartime[97] = 44418.0000
	TrafficCarModel[97] = emperor

	TrafficCarPos[98] = <<1594.1198, 6392.8965, 25.6806>>
	TrafficCarQuatX[98] = -0.0173
	TrafficCarQuatY[98] = 0.0277
	TrafficCarQuatZ[98] = 0.8492
	TrafficCarQuatW[98] = -0.5271
	TrafficCarRecording[98] = 99
	TrafficCarStartime[98] = 44814.0000
	TrafficCarModel[98] = emperor

	TrafficCarPos[99] = <<1593.0610, 6413.1421, 25.3120>>
	TrafficCarQuatX[99] = -0.0286
	TrafficCarQuatY[99] = -0.0196
	TrafficCarQuatZ[99] = 0.5784
	TrafficCarQuatW[99] = 0.8150
	TrafficCarRecording[99] = 100
	TrafficCarStartime[99] = 44946.0000
	TrafficCarModel[99] = minivan

//	TrafficCarPos[100] = <<1584.9420, 6403.1611, 25.1363>>
//	TrafficCarQuatX[100] = -0.0179
//	TrafficCarQuatY[100] = 0.0270
//	TrafficCarQuatZ[100] = 0.8338
//	TrafficCarQuatW[100] = -0.5511
//	TrafficCarRecording[100] = 101
//	TrafficCarStartime[100] = 45078.0000
//	TrafficCarModel[100] = asterope

//	ParkedCarPos[1] = <<1597.6619, 6417.5078, 24.9339>>
//	ParkedCarQuatX[1] = -0.0000
//	ParkedCarQuatY[1] = 0.0000
//	ParkedCarQuatZ[1] = 0.5357
//	ParkedCarQuatW[1] = 0.8444
//	ParkedCarModel[1] = Packer

	TrafficCarPos[101] = <<1588.1694, 6421.3286, 24.7918>>
	TrafficCarQuatX[101] = -0.0301
	TrafficCarQuatY[101] = -0.0185
	TrafficCarQuatZ[101] = 0.5236
	TrafficCarQuatW[101] = 0.8512
	TrafficCarRecording[101] = 102
	TrafficCarStartime[101] = 45342.0000
	TrafficCarModel[101] = asterope

	TrafficCarPos[102] = <<1587.3392, 6396.2217, 25.2695>>
	TrafficCarQuatX[102] = -0.0181
	TrafficCarQuatY[102] = 0.0275
	TrafficCarQuatZ[102] = 0.8375
	TrafficCarQuatW[102] = -0.5455
	TrafficCarRecording[102] = 103
	TrafficCarStartime[102] = 45474.0000
	TrafficCarModel[102] = journey

	TrafficCarPos[103] = <<1542.3279, 6415.5874, 23.3908>>
	TrafficCarQuatX[103] = -0.0083
	TrafficCarQuatY[103] = 0.0124
	TrafficCarQuatZ[103] = 0.8169
	TrafficCarQuatW[103] = -0.5765
	TrafficCarRecording[103] = 104
	TrafficCarStartime[103] = 46068.0000
	TrafficCarModel[103] = emperor

	ParkedCarPos[2] = <<1576.5372, 6451.1187, 24.5210>>
	ParkedCarQuatX[2] = 0.0361
	ParkedCarQuatY[2] = -0.0126
	ParkedCarQuatZ[2] = -0.2494
	ParkedCarQuatW[2] = 0.9676
	ParkedCarModel[2] = minivan

	ParkedCarPos[3] = <<1532.3318, 6408.2344, 23.1721>>
	ParkedCarQuatX[3] = 0.0000
	ParkedCarQuatY[3] = 0.0000
	ParkedCarQuatZ[3] = 0.8056
	ParkedCarQuatW[3] = -0.5924
	ParkedCarModel[3] = emperor

//	TrafficCarPos[104] = <<1546.6086, 6418.7231, 23.5173>>
//	TrafficCarQuatX[104] = -0.0077
//	TrafficCarQuatY[104] = 0.0122
//	TrafficCarQuatZ[104] = 0.8514
//	TrafficCarQuatW[104] = -0.5243
//	TrafficCarRecording[104] = 105
//	TrafficCarStartime[104] = 46596.0000
//	TrafficCarModel[104] = asterope

//	TrafficCarPos[105] = <<1524.1237, 6441.6055, 23.2660>>
//	TrafficCarQuatX[105] = -0.0145
//	TrafficCarQuatY[105] = -0.0099
//	TrafficCarQuatZ[105] = 0.5570
//	TrafficCarQuatW[105] = 0.8303
//	TrafficCarRecording[105] = 106
//	TrafficCarStartime[105] = 46728.0000
//	TrafficCarModel[105] = Packer

	TrafficCarPos[106] = <<1531.3542, 6419.3477, 23.3128>>
	TrafficCarQuatX[106] = -0.0092
	TrafficCarQuatY[106] = 0.0122
	TrafficCarQuatZ[106] = 0.8038
	TrafficCarQuatW[106] = -0.5947
	TrafficCarRecording[106] = 107
	TrafficCarStartime[106] = 46992.0000
	TrafficCarModel[106] = SURFER

	TrafficCarPos[107] = <<1487.2659, 6441.6406, 21.9444>>
	TrafficCarQuatX[107] = -0.0092
	TrafficCarQuatY[107] = 0.0102
	TrafficCarQuatZ[107] = 0.7991
	TrafficCarQuatW[107] = -0.6010
	TrafficCarRecording[107] = 108
	TrafficCarStartime[107] = 47520.0000
	TrafficCarModel[107] = journey

//	TrafficCarPos[108] = <<1440.0983, 6458.7432, 20.7191>>
//	TrafficCarQuatX[108] = -0.0077
//	TrafficCarQuatY[108] = 0.0073
//	TrafficCarQuatZ[108] = 0.8126
//	TrafficCarQuatW[108] = -0.5827
//	TrafficCarRecording[108] = 109
//	TrafficCarStartime[108] = 48576.0000
//	TrafficCarModel[108] = minivan

	TrafficCarPos[109] = <<1406.0055, 6480.3774, 19.6421>>
	TrafficCarQuatX[109] = -0.0031
	TrafficCarQuatY[109] = -0.0048
	TrafficCarQuatZ[109] = 0.6208
	TrafficCarQuatW[109] = 0.7840
	TrafficCarRecording[109] = 110
	TrafficCarStartime[109] = 49368.0000
	TrafficCarModel[109] = emperor

	TrafficCarPos[110] = <<1394.3269, 6471.6846, 19.6279>>
	TrafficCarQuatX[110] = -0.0010
	TrafficCarQuatY[110] = 0.0034
	TrafficCarQuatZ[110] = 0.7756
	TrafficCarQuatW[110] = -0.6312
	TrafficCarRecording[110] = 111
	TrafficCarStartime[110] = 49566.0000
	TrafficCarModel[110] = journey

	TrafficCarPos[111] = <<1373.0330, 6476.1318, 19.4375>>
	TrafficCarQuatX[111] = 0.0019
	TrafficCarQuatY[111] = 0.0044
	TrafficCarQuatZ[111] = 0.7449
	TrafficCarQuatW[111] = -0.6672
	TrafficCarRecording[111] = 112
	TrafficCarStartime[111] = 50028.0000
	TrafficCarModel[111] = emperor

	TrafficCarPos[112] = <<1372.0283, 6482.6143, 19.6764>>
	TrafficCarQuatX[112] = -0.0018
	TrafficCarQuatY[112] = -0.0012
	TrafficCarQuatZ[112] = 0.6428
	TrafficCarQuatW[112] = 0.7660
	TrafficCarRecording[112] = 113
	TrafficCarStartime[112] = 50094.0000
	TrafficCarModel[112] = asterope

	TrafficCarPos[113] = <<1385.5012, 6480.0137, 19.7448>>
	TrafficCarQuatX[113] = -0.0020
	TrafficCarQuatY[113] = -0.0017
	TrafficCarQuatZ[113] = 0.6064
	TrafficCarQuatW[113] = 0.7951
	TrafficCarRecording[113] = 114
	TrafficCarStartime[113] = 50292.0000
	TrafficCarModel[113] = SURFER

	TrafficCarPos[114] = <<1383.4406, 6473.8906, 19.8693>>
	TrafficCarQuatX[114] = 0.0022
	TrafficCarQuatY[114] = 0.0043
	TrafficCarQuatZ[114] = 0.7876
	TrafficCarQuatW[114] = -0.6162
	TrafficCarRecording[114] = 115
	TrafficCarStartime[114] = 50358.0000
	TrafficCarModel[114] = journey

	TrafficCarPos[115] = <<1322.9041, 6483.0718, 19.6677>>
	TrafficCarQuatX[115] = 0.0041
	TrafficCarQuatY[115] = 0.0020
	TrafficCarQuatZ[115] = 0.7191
	TrafficCarQuatW[115] = -0.6949
	TrafficCarRecording[115] = 116
	TrafficCarStartime[115] = 51084.0000
	TrafficCarModel[115] = asterope

	TrafficCarPos[116] = <<1203.2847, 6495.1777, 20.7136>>
	TrafficCarQuatX[116] = 0.0013
	TrafficCarQuatY[116] = 0.0012
	TrafficCarQuatZ[116] = 0.7118
	TrafficCarQuatW[116] = 0.7024
	TrafficCarRecording[116] = 117
	TrafficCarStartime[116] = 54120.0000
	TrafficCarModel[116] = minivan

	ParkedCarPos[4] = <<1167.2759, 6510.2671, 20.4517>>
	ParkedCarQuatX[4] = -0.0056
	ParkedCarQuatY[4] = 0.0116
	ParkedCarQuatZ[4] = 0.6716
	ParkedCarQuatW[4] = 0.7408
	ParkedCarModel[4] = minivan

	TrafficCarPos[117] = <<1155.1647, 6482.5327, 20.7270>>
	TrafficCarQuatX[117] = -0.0035
	TrafficCarQuatY[117] = -0.0023
	TrafficCarQuatZ[117] = -0.7037
	TrafficCarQuatW[117] = 0.7105
	TrafficCarRecording[117] = 118
	TrafficCarStartime[117] = 55110.0000
	TrafficCarModel[117] = SURFER

	ParkedCarPos[5] = <<1140.6067, 6511.6465, 20.5916>>
	ParkedCarQuatX[5] = -0.0319
	ParkedCarQuatY[5] = 0.0231
	ParkedCarQuatZ[5] = 0.6637
	ParkedCarQuatW[5] = 0.7469
	ParkedCarModel[5] = minivan

	TrafficCarPos[118] = <<1124.6927, 6494.4053, 20.5125>>
	TrafficCarQuatX[118] = 0.0029
	TrafficCarQuatY[118] = -0.0031
	TrafficCarQuatZ[118] = 0.7094
	TrafficCarQuatW[118] = 0.7048
	TrafficCarRecording[118] = 119
	TrafficCarStartime[118] = 55704.0000
	TrafficCarModel[118] = emperor

	TrafficCarPos[119] = <<1119.5255, 6488.2949, 20.7457>>
	TrafficCarQuatX[119] = 0.0031
	TrafficCarQuatY[119] = -0.0033
	TrafficCarQuatZ[119] = 0.7145
	TrafficCarQuatW[119] = 0.6997
	TrafficCarRecording[119] = 120
	TrafficCarStartime[119] = 56034.0000
	TrafficCarModel[119] = asterope

	TrafficCarPos[120] = <<1106.9222, 6481.5542, 20.5177>>
	TrafficCarQuatX[120] = 0.0030
	TrafficCarQuatY[120] = 0.0031
	TrafficCarQuatZ[120] = 0.7152
	TrafficCarQuatW[120] = -0.6989
	TrafficCarRecording[120] = 121
	TrafficCarStartime[120] = 56166.0000
	TrafficCarModel[120] = asterope

	ParkedCarPos[6] = <<1112.5037, 6510.3462, 20.6561>>
	ParkedCarQuatX[6] = -0.0000
	ParkedCarQuatY[6] = 0.0000
	ParkedCarQuatZ[6] = 0.6373
	ParkedCarQuatW[6] = 0.7707
	ParkedCarModel[6] = asterope

	TrafficCarPos[121] = <<1089.8398, 6482.0918, 20.4654>>
	TrafficCarQuatX[121] = -0.0021
	TrafficCarQuatY[121] = -0.0042
	TrafficCarQuatZ[121] = -0.6711
	TrafficCarQuatW[121] = 0.7414
	TrafficCarRecording[121] = 122
	TrafficCarStartime[121] = 56562.0000
	TrafficCarModel[121] = emperor

	TrafficCarPos[122] = <<1064.2640, 6481.4595, 20.5162>>
	TrafficCarQuatX[122] = 0.0020
	TrafficCarQuatY[122] = 0.0035
	TrafficCarQuatZ[122] = 0.7168
	TrafficCarQuatW[122] = -0.6973
	TrafficCarRecording[122] = 123
	TrafficCarStartime[122] = 57156.0000
	TrafficCarModel[122] = journey

	TrafficCarPos[123] = <<1060.3815, 6493.4580, 20.7409>>
	TrafficCarQuatX[123] = 0.0021
	TrafficCarQuatY[123] = -0.0037
	TrafficCarQuatZ[123] = 0.6976
	TrafficCarQuatW[123] = 0.7165
	TrafficCarRecording[123] = 124
	TrafficCarStartime[123] = 57156.0000
	TrafficCarModel[123] = asterope

	TrafficCarPos[124] = <<1048.4277, 6481.5498, 20.7796>>
	TrafficCarQuatX[124] = 0.0036
	TrafficCarQuatY[124] = 0.0026
	TrafficCarQuatZ[124] = 0.7133
	TrafficCarQuatW[124] = -0.7008
	TrafficCarRecording[124] = 125
	TrafficCarStartime[124] = 57486.0000
	TrafficCarModel[124] = journey

	ParkedCarPos[7] = <<1038.6754, 6504.0908, 20.5573>>
	ParkedCarQuatX[7] = 0.0135
	ParkedCarQuatY[7] = -0.0175
	ParkedCarQuatZ[7] = 0.7902
	ParkedCarQuatW[7] = 0.6125
	ParkedCarModel[7] = minivan

	TrafficCarPos[125] = <<1029.0808, 6488.5986, 20.3729>>
	TrafficCarQuatX[125] = 0.0001
	TrafficCarQuatY[125] = -0.0004
	TrafficCarQuatZ[125] = 0.6854
	TrafficCarQuatW[125] = 0.7282
	TrafficCarRecording[125] = 126
	TrafficCarStartime[125] = 57882.0000
	TrafficCarModel[125] = emperor

	TrafficCarPos[126] = <<1013.0278, 6481.3428, 20.3730>>
	TrafficCarQuatX[126] = 0.0031
	TrafficCarQuatY[126] = 0.0031
	TrafficCarQuatZ[126] = 0.7262
	TrafficCarQuatW[126] = -0.6875
	TrafficCarRecording[126] = 127
	TrafficCarStartime[126] = 58278.0000
	TrafficCarModel[126] = emperor

	TrafficCarPos[127] = <<1002.9684, 6481.7998, 20.7867>>
	TrafficCarQuatX[127] = -0.0033
	TrafficCarQuatY[127] = -0.0029
	TrafficCarQuatZ[127] = -0.6788
	TrafficCarQuatW[127] = 0.7343
	TrafficCarRecording[127] = 128
	TrafficCarStartime[127] = 58476.0000
	TrafficCarModel[127] = minivan

	TrafficCarPos[128] = <<987.0549, 6481.9800, 20.6444>>
	TrafficCarQuatX[128] = -0.0032
	TrafficCarQuatY[128] = -0.0033
	TrafficCarQuatZ[128] = -0.7041
	TrafficCarQuatW[128] = 0.7101
	TrafficCarRecording[128] = 129
	TrafficCarStartime[128] = 58872.0000
	TrafficCarModel[128] = SURFER

	TrafficCarPos[129] = <<978.0116, 6482.1367, 20.6604>>
	TrafficCarQuatX[129] = 0.0037
	TrafficCarQuatY[129] = 0.0022
	TrafficCarQuatZ[129] = 0.7221
	TrafficCarQuatW[129] = -0.6918
	TrafficCarRecording[129] = 130
	TrafficCarStartime[129] = 59598.0000
	TrafficCarModel[129] = asterope

	TrafficCarPos[130] = <<934.9140, 6495.5503, 20.5344>>
	TrafficCarQuatX[130] = 0.0025
	TrafficCarQuatY[130] = -0.0002
	TrafficCarQuatZ[130] = 0.6975
	TrafficCarQuatW[130] = 0.7165
	TrafficCarRecording[130] = 131
	TrafficCarStartime[130] = 60060.0000
	TrafficCarModel[130] = emperor

	TrafficCarPos[131] = <<932.5633, 6483.0703, 20.9136>>
	TrafficCarQuatX[131] = 0.0052
	TrafficCarQuatY[131] = 0.0011
	TrafficCarQuatZ[131] = 0.7226
	TrafficCarQuatW[131] = -0.6913
	TrafficCarRecording[131] = 132
	TrafficCarStartime[131] = 60390.0000
	TrafficCarModel[131] = journey

	TrafficCarPos[132] = <<909.9576, 6483.8462, 20.7867>>
	TrafficCarQuatX[132] = 0.0069
	TrafficCarQuatY[132] = -0.0002
	TrafficCarQuatZ[132] = 0.7339
	TrafficCarQuatW[132] = -0.6792
	TrafficCarRecording[132] = 133
	TrafficCarStartime[132] = 60984.0000
	TrafficCarModel[132] = journey

	TrafficCarPos[133] = <<888.3078, 6491.0879, 21.3140>>
	TrafficCarQuatX[133] = 0.0049
	TrafficCarQuatY[133] = 0.0028
	TrafficCarQuatZ[133] = 0.6906
	TrafficCarQuatW[133] = 0.7232
	TrafficCarRecording[133] = 134
	TrafficCarStartime[133] = 61116.0000
	TrafficCarModel[133] = minivan

	TrafficCarPos[134] = <<889.8680, 6484.9717, 21.2800>>
	TrafficCarQuatX[134] = 0.0071
	TrafficCarQuatY[134] = -0.0009
	TrafficCarQuatZ[134] = 0.7250
	TrafficCarQuatW[134] = -0.6887
	TrafficCarRecording[134] = 135
	TrafficCarStartime[134] = 61446.0000
	TrafficCarModel[134] = journey

	TrafficCarPos[135] = <<842.5891, 6488.0615, 22.1128>>
	TrafficCarQuatX[135] = 0.0123
	TrafficCarQuatY[135] = -0.0070
	TrafficCarQuatZ[135] = 0.7344
	TrafficCarQuatW[135] = -0.6786
	TrafficCarRecording[135] = 136
	TrafficCarStartime[135] = 62172.0000
	TrafficCarModel[135] = journey

	TrafficCarPos[136] = <<841.4916, 6494.0601, 21.7476>>
	TrafficCarQuatX[136] = 0.0096
	TrafficCarQuatY[136] = 0.0076
	TrafficCarQuatZ[136] = 0.6762
	TrafficCarQuatW[136] = 0.7366
	TrafficCarRecording[136] = 137
	TrafficCarStartime[136] = 62172.0000
	TrafficCarModel[136] = emperor

	TrafficCarPos[137] = <<822.6181, 6489.5981, 22.7336>>
	TrafficCarQuatX[137] = 0.0142
	TrafficCarQuatY[137] = -0.0097
	TrafficCarQuatZ[137] = 0.7634
	TrafficCarQuatW[137] = -0.6457
	TrafficCarRecording[137] = 138
	TrafficCarStartime[137] = 62634.0000
	TrafficCarModel[137] = minivan

	TrafficCarPos[138] = <<798.2071, 6491.5229, 23.6704>>
	TrafficCarQuatX[138] = 0.0171
	TrafficCarQuatY[138] = -0.0136
	TrafficCarQuatZ[138] = 0.7653
	TrafficCarQuatW[138] = -0.6434
	TrafficCarRecording[138] = 139
	TrafficCarStartime[138] = 63228.0000
	TrafficCarModel[138] = minivan

	TrafficCarPos[139] = <<795.2281, 6497.8896, 23.5202>>
	TrafficCarQuatX[139] = 0.0163
	TrafficCarQuatY[139] = 0.0138
	TrafficCarQuatZ[139] = 0.6766
	TrafficCarQuatW[139] = 0.7361
	TrafficCarRecording[139] = 140
	TrafficCarStartime[139] = 63228.0000
	TrafficCarModel[139] = minivan

	TrafficCarPos[140] = <<797.3790, 6503.7246, 23.3211>>
	TrafficCarQuatX[140] = 0.0169
	TrafficCarQuatY[140] = 0.0141
	TrafficCarQuatZ[140] = 0.6396
	TrafficCarQuatW[140] = 0.7684
	TrafficCarRecording[140] = 141
	TrafficCarStartime[140] = 63558.0000
	TrafficCarModel[140] = emperor

	TrafficCarPos[141] = <<747.9111, 6498.4053, 26.0047>>
	TrafficCarQuatX[141] = 0.0165
	TrafficCarQuatY[141] = -0.0131
	TrafficCarQuatZ[141] = 0.7684
	TrafficCarQuatW[141] = -0.6397
	TrafficCarRecording[141] = 142
	TrafficCarStartime[141] = 64350.0000
	TrafficCarModel[141] = journey

	TrafficCarPos[142] = <<713.0914, 6505.1797, 26.9262>>
	TrafficCarQuatX[142] = 0.0099
	TrafficCarQuatY[142] = -0.0055
	TrafficCarQuatZ[142] = 0.7799
	TrafficCarQuatW[142] = -0.6259
	TrafficCarRecording[142] = 143
	TrafficCarStartime[142] = 65142.0000
	TrafficCarModel[142] = journey

//	TrafficCarPos[143] = <<711.7022, 6512.8169, 26.9004>>
//	TrafficCarQuatX[143] = 0.0126
//	TrafficCarQuatY[143] = 0.0045
//	TrafficCarQuatZ[143] = 0.6277
//	TrafficCarQuatW[143] = 0.7784
//	TrafficCarRecording[143] = 144
//	TrafficCarStartime[143] = 65208.0000
//	TrafficCarModel[143] = emperor

	TrafficCarPos[144] = <<725.2426, 6509.2402, 26.9223>>
	TrafficCarQuatX[144] = 0.0136
	TrafficCarQuatY[144] = 0.0054
	TrafficCarQuatZ[144] = 0.6585
	TrafficCarQuatW[144] = 0.7525
	TrafficCarRecording[144] = 145
	TrafficCarStartime[144] = 65472.0000
	TrafficCarModel[144] = minivan

//	TrafficCarPos[145] = <<716.8861, 6511.1299, 27.0243>>
//	TrafficCarQuatX[145] = 0.0130
//	TrafficCarQuatY[145] = 0.0043
//	TrafficCarQuatZ[145] = 0.5981
//	TrafficCarQuatW[145] = 0.8013
//	TrafficCarRecording[145] = 146
//	TrafficCarStartime[145] = 65604.0000
//	TrafficCarModel[145] = SURFER

//	TrafficCarPos[146] = <<702.9258, 6519.6206, 27.8427>>
//	TrafficCarQuatX[146] = 0.0037
//	TrafficCarQuatY[146] = -0.0028
//	TrafficCarQuatZ[146] = 0.6318
//	TrafficCarQuatW[146] = 0.7751
//	TrafficCarRecording[146] = 147
//	TrafficCarStartime[146] = 65934.0000
//	TrafficCarModel[146] = Packer

	TrafficCarPos[147] = <<669.4201, 6515.0186, 27.8855>>
	TrafficCarQuatX[147] = 0.0052
	TrafficCarQuatY[147] = 0.0006
	TrafficCarQuatZ[147] = 0.7765
	TrafficCarQuatW[147] = -0.6301
	TrafficCarRecording[147] = 148
	TrafficCarStartime[147] = 66396.0000
	TrafficCarModel[147] = journey

	TrafficCarPos[148] = <<648.7714, 6519.7339, 28.0324>>
	TrafficCarQuatX[148] = 0.0045
	TrafficCarQuatY[148] = 0.0014
	TrafficCarQuatZ[148] = 0.7660
	TrafficCarQuatW[148] = -0.6428
	TrafficCarRecording[148] = 149
	TrafficCarStartime[148] = 67188.0000
	TrafficCarModel[148] = minivan

	TrafficCarPos[149] = <<617.5839, 6538.9854, 27.6398>>
	TrafficCarQuatX[149] = 0.0007
	TrafficCarQuatY[149] = -0.0028
	TrafficCarQuatZ[149] = 0.6262
	TrafficCarQuatW[149] = 0.7797
	TrafficCarRecording[149] = 150
	TrafficCarStartime[149] = 67584.0000
	TrafficCarModel[149] = emperor

	TrafficCarPos[150] = <<595.9203, 6531.1943, 27.9013>>
	TrafficCarQuatX[150] = 0.0016
	TrafficCarQuatY[150] = 0.0049
	TrafficCarQuatZ[150] = 0.7801
	TrafficCarQuatW[150] = -0.6256
	TrafficCarRecording[150] = 151
	TrafficCarStartime[150] = 68112.0000
	TrafficCarModel[150] = journey

	TrafficCarPos[151] = <<573.7428, 6536.1211, 27.4576>>
	TrafficCarQuatX[151] = 0.0012
	TrafficCarQuatY[151] = 0.0059
	TrafficCarQuatZ[151] = 0.7853
	TrafficCarQuatW[151] = -0.6191
	TrafficCarRecording[151] = 152
	TrafficCarStartime[151] = 68442.0000
	TrafficCarModel[151] = journey

	TrafficCarPos[152] = <<569.4676, 6549.8774, 27.7749>>
	TrafficCarQuatX[152] = -0.0003
	TrafficCarQuatY[152] = -0.0058
	TrafficCarQuatZ[152] = 0.6257
	TrafficCarQuatW[152] = 0.7800
	TrafficCarRecording[152] = 153
	TrafficCarStartime[152] = 68706.0000
	TrafficCarModel[152] = minivan

	TrafficCarPos[153] = <<553.1826, 6540.7139, 27.5681>>
	TrafficCarQuatX[153] = 0.0006
	TrafficCarQuatY[153] = 0.0064
	TrafficCarQuatZ[153] = 0.7703
	TrafficCarQuatW[153] = -0.6376
	TrafficCarRecording[153] = 154
	TrafficCarStartime[153] = 69366.0000
	TrafficCarModel[153] = journey

//	TrafficCarPos[154] = <<552.5774, 6547.6406, 27.4334>>
//	TrafficCarQuatX[154] = -0.0025
//	TrafficCarQuatY[154] = -0.0043
//	TrafficCarQuatZ[154] = 0.6047
//	TrafficCarQuatW[154] = 0.7964
//	TrafficCarRecording[154] = 155
//	TrafficCarStartime[154] = 69564.0000
//	TrafficCarModel[154] = SURFER

//	TrafficCarPos[155] = <<501.2111, 6551.8574, 27.2862>>
//	TrafficCarQuatX[155] = 0.0010
//	TrafficCarQuatY[155] = 0.0058
//	TrafficCarQuatZ[155] = 0.7784
//	TrafficCarQuatW[155] = -0.6278
//	TrafficCarRecording[155] = 156
//	TrafficCarStartime[155] = 70158.0000
//	TrafficCarModel[155] = Packer

	TrafficCarPos[156] = <<490.8492, 6565.3628, 26.8575>>
	TrafficCarQuatX[156] = -0.0004
	TrafficCarQuatY[156] = -0.0041
	TrafficCarQuatZ[156] = 0.6584
	TrafficCarQuatW[156] = 0.7526
	TrafficCarRecording[156] = 157
	TrafficCarStartime[156] = 70488.0000
	TrafficCarModel[156] = minivan

	TrafficCarPos[157] = <<504.1685, 6557.9175, 26.9448>>
	TrafficCarQuatX[157] = -0.0045
	TrafficCarQuatY[157] = -0.0036
	TrafficCarQuatZ[157] = 0.6266
	TrafficCarQuatW[157] = 0.7793
	TrafficCarRecording[157] = 158
	TrafficCarStartime[157] = 70686.0000
	TrafficCarModel[157] = journey

	TrafficCarPos[158] = <<468.7714, 6557.2080, 26.8139>>
	TrafficCarQuatX[158] = 0.0029
	TrafficCarQuatY[158] = 0.0027
	TrafficCarQuatZ[158] = 0.7754
	TrafficCarQuatW[158] = -0.6314
	TrafficCarRecording[158] = 159
	TrafficCarStartime[158] = 71016.0000
	TrafficCarModel[158] = minivan

	TrafficCarPos[159] = <<443.9303, 6561.5098, 26.5272>>
	TrafficCarQuatX[159] = 0.0017
	TrafficCarQuatY[159] = -0.0019
	TrafficCarQuatZ[159] = 0.7477
	TrafficCarQuatW[159] = -0.6641
	TrafficCarRecording[159] = 160
	TrafficCarStartime[159] = 71544.0000
	TrafficCarModel[159] = journey

	TrafficCarPos[160] = <<410.8601, 6564.7383, 27.1720>>
	TrafficCarQuatX[160] = 0.0061
	TrafficCarQuatY[160] = -0.0049
	TrafficCarQuatZ[160] = 0.7329
	TrafficCarQuatW[160] = -0.6803
	TrafficCarRecording[160] = 161
	TrafficCarStartime[160] = 72336.0000
	TrafficCarModel[160] = journey

	TrafficCarPos[161] = <<405.5365, 6576.0762, 27.3538>>
	TrafficCarQuatX[161] = 0.0096
	TrafficCarQuatY[161] = 0.0013
	TrafficCarQuatZ[161] = 0.6791
	TrafficCarQuatW[161] = 0.7339
	TrafficCarRecording[161] = 162
	TrafficCarStartime[161] = 72468.0000
	TrafficCarModel[161] = minivan

//	TrafficCarPos[162] = <<397.8140, 6565.3525, 27.6450>>
//	TrafficCarQuatX[162] = 0.0085
//	TrafficCarQuatY[162] = -0.0095
//	TrafficCarQuatZ[162] = 0.7374
//	TrafficCarQuatW[162] = -0.6753
//	TrafficCarRecording[162] = 163
//	TrafficCarStartime[162] = 72600.0000
//	TrafficCarModel[162] = Packer

	TrafficCarPos[163] = <<395.5341, 6577.0586, 27.2913>>
	TrafficCarQuatX[163] = 0.0078
	TrafficCarQuatY[163] = 0.0014
	TrafficCarQuatZ[163] = 0.6561
	TrafficCarQuatW[163] = 0.7546
	TrafficCarRecording[163] = 164
	TrafficCarStartime[163] = 72798.0000
	TrafficCarModel[163] = SURFER

	TrafficCarPos[164] = <<348.5958, 6567.6582, 28.2457>>
	TrafficCarQuatX[164] = -0.0079
	TrafficCarQuatY[164] = 0.0079
	TrafficCarQuatZ[164] = -0.6893
	TrafficCarQuatW[164] = 0.7244
	TrafficCarRecording[164] = 165
	TrafficCarStartime[164] = 73788.0000
	TrafficCarModel[164] = minivan

	TrafficCarPos[165] = <<341.6452, 6578.4043, 28.4158>>
	TrafficCarQuatX[165] = 0.0112
	TrafficCarQuatY[165] = 0.0051
	TrafficCarQuatZ[165] = 0.7104
	TrafficCarQuatW[165] = 0.7037
	TrafficCarRecording[165] = 166
	TrafficCarStartime[165] = 73986.0000
	TrafficCarModel[165] = journey

//	TrafficCarPos[166] = <<341.9621, 6566.7886, 28.6901>>
//	TrafficCarQuatX[166] = 0.0102
//	TrafficCarQuatY[166] = -0.0102
//	TrafficCarQuatZ[166] = 0.7127
//	TrafficCarQuatW[166] = -0.7013
//	TrafficCarRecording[166] = 167
//	TrafficCarStartime[166] = 74514.0000
//	TrafficCarModel[166] = Packer

//	TrafficCarPos[167] = <<311.6325, 6578.1865, 29.2086>>
//	TrafficCarQuatX[167] = 0.0125
//	TrafficCarQuatY[167] = 0.0062
//	TrafficCarQuatZ[167] = 0.6970
//	TrafficCarQuatW[167] = 0.7169
//	TrafficCarRecording[167] = 168
//	TrafficCarStartime[167] = 74646.0000
//	TrafficCarModel[167] = minivan

	ParkedCarPos[8] = <<301.0602, 6596.7490, 29.7845>>
	ParkedCarQuatX[8] = -0.0241
	ParkedCarQuatY[8] = 0.0122
	ParkedCarQuatZ[8] = -0.1432
	ParkedCarQuatW[8] = 0.9893
	ParkedCarModel[8] = SURFER

//	TrafficCarPos[168] = <<311.7135, 6577.5947, 29.0619>>
//	TrafficCarQuatX[168] = 0.0120
//	TrafficCarQuatY[168] = 0.0065
//	TrafficCarQuatZ[168] = 0.7500
//	TrafficCarQuatW[168] = 0.6614
//	TrafficCarRecording[168] = 169
//	TrafficCarStartime[168] = 75108.0000
//	TrafficCarModel[168] = SURFER

	TrafficCarPos[169] = <<289.2193, 6565.6675, 29.8009>>
	TrafficCarQuatX[169] = -0.0105
	TrafficCarQuatY[169] = 0.0097
	TrafficCarQuatZ[169] = -0.6653
	TrafficCarQuatW[169] = 0.7465
	TrafficCarRecording[169] = 170
	TrafficCarStartime[169] = 75570.0000
	TrafficCarModel[169] = minivan

	TrafficCarPos[170] = <<280.0489, 6564.2744, 30.5158>>
	TrafficCarQuatX[170] = -0.0102
	TrafficCarQuatY[170] = 0.0098
	TrafficCarQuatZ[170] = -0.6946
	TrafficCarQuatW[170] = 0.7193
	TrafficCarRecording[170] = 171
	TrafficCarStartime[170] = 75768.0000
	TrafficCarModel[170] = Mule

	TrafficCarPos[171] = <<223.4678, 6553.7607, 31.5455>>
	TrafficCarQuatX[171] = -0.0027
	TrafficCarQuatY[171] = 0.0055
	TrafficCarQuatZ[171] = -0.6048
	TrafficCarQuatW[171] = 0.7963
	TrafficCarRecording[171] = 172
	TrafficCarStartime[171] = 76626.0000
	TrafficCarModel[171] = journey

//	TrafficCarPos[172] = <<220.9848, 6566.5981, 31.6423>>
//	TrafficCarQuatX[172] = 0.0081
//	TrafficCarQuatY[172] = 0.0032
//	TrafficCarQuatZ[172] = 0.7852
//	TrafficCarQuatW[172] = 0.6192
//	TrafficCarRecording[172] = 173
//	TrafficCarStartime[172] = 76692.0000
//	TrafficCarModel[172] = minivan

	TrafficCarPos[173] = <<204.3794, 6547.8735, 31.7291>>
	TrafficCarQuatX[173] = -0.0001
	TrafficCarQuatY[173] = 0.0024
	TrafficCarQuatZ[173] = -0.5380
	TrafficCarQuatW[173] = 0.8430
	TrafficCarRecording[173] = 174
	TrafficCarStartime[173] = 77220.0000
	TrafficCarModel[173] = minivan

	TrafficCarPos[174] = <<191.1950, 6542.2598, 31.7253>>
	TrafficCarQuatX[174] = 0.0012
	TrafficCarQuatY[174] = -0.0008
	TrafficCarQuatZ[174] = -0.5002
	TrafficCarQuatW[174] = 0.8659
	TrafficCarRecording[174] = 175
	TrafficCarStartime[174] = 77880.0000
	TrafficCarModel[174] = minivan

//	TrafficCarPos[175] = <<163.6469, 6542.5317, 31.6838>>
//	TrafficCarQuatX[175] = 0.0014
//	TrafficCarQuatY[175] = -0.0066
//	TrafficCarQuatZ[175] = 0.8820
//	TrafficCarQuatW[175] = 0.4713
//	TrafficCarRecording[175] = 176
//	TrafficCarStartime[175] = 78078.0000
//	TrafficCarModel[175] = minivan

	ParkedCarPos[9] = <<150.8109, 6597.5127, 30.8449>>
	ParkedCarQuatX[9] = -0.0000
	ParkedCarQuatY[9] = 0.0000
	ParkedCarQuatZ[9] = -0.0089
	ParkedCarQuatW[9] = 1.0000
	ParkedCarModel[9] = Mule

	ParkedCarPos[10] = <<146.6424, 6579.6548, 30.8164>>
	ParkedCarQuatX[10] = -0.0000
	ParkedCarQuatY[10] = 0.0000
	ParkedCarQuatZ[10] = -0.6845
	ParkedCarQuatW[10] = 0.7290
	ParkedCarModel[10] = Mule

	TrafficCarPos[176] = <<136.9821, 6540.7783, 31.3868>>
	TrafficCarQuatX[176] = -0.0180
	TrafficCarQuatY[176] = -0.0020
	TrafficCarQuatZ[176] = 0.9253
	TrafficCarQuatW[176] = -0.3787
	TrafficCarRecording[176] = 177
	TrafficCarStartime[176] = 78672.0000
	TrafficCarModel[176] = minivan

	TrafficCarPos[177] = <<88.7146, 6597.5522, 31.3270>>
	TrafficCarQuatX[177] = -0.0033
	TrafficCarQuatY[177] = 0.0080
	TrafficCarQuatZ[177] = 0.3907
	TrafficCarQuatW[177] = 0.9205
	TrafficCarRecording[177] = 178
	TrafficCarStartime[177] = 79794.0000
	TrafficCarModel[177] = minivan

	TrafficCarPos[178] = <<62.4602, 6447.9053, 31.1752>>
	TrafficCarQuatX[178] = 0.0041
	TrafficCarQuatY[178] = -0.0017
	TrafficCarQuatZ[178] = 0.9220
	TrafficCarQuatW[178] = 0.3872
	TrafficCarRecording[178] = 179
	TrafficCarStartime[178] = 81906.0000
	TrafficCarModel[178] = minivan

	ParkedCarPos[11] = <<40.8196, 6439.5845, 30.3269>>
	ParkedCarQuatX[11] = 0.0000
	ParkedCarQuatY[11] = 0.0000
	ParkedCarQuatZ[11] = 0.9229
	ParkedCarQuatW[11] = 0.3850
	ParkedCarModel[11] = Mule

	TrafficCarPos[179] = <<95.0551, 6464.8789, 31.4468>>
	TrafficCarQuatX[179] = 0.0059
	TrafficCarQuatY[179] = -0.0024
	TrafficCarQuatZ[179] = -0.3842
	TrafficCarQuatW[179] = 0.9232
	TrafficCarRecording[179] = 180
	TrafficCarStartime[179] = 82104.0000
	TrafficCarModel[179] = Mule

	TrafficCarPos[180] = <<35.1529, 6421.5063, 31.1415>>
	TrafficCarQuatX[180] = 0.0036
	TrafficCarQuatY[180] = -0.0021
	TrafficCarQuatZ[180] = 0.9210
	TrafficCarQuatW[180] = 0.3895
	TrafficCarRecording[180] = 181
	TrafficCarStartime[180] = 82302.0000
	TrafficCarModel[180] = journey

	TrafficCarPos[181] = <<54.4397, 6424.6313, 30.9632>>
	TrafficCarQuatX[181] = -0.0006
	TrafficCarQuatY[181] = -0.0000
	TrafficCarQuatZ[181] = -0.3767
	TrafficCarQuatW[181] = 0.9263
	TrafficCarRecording[181] = 182
	TrafficCarStartime[181] = 82434.0000
	TrafficCarModel[181] = SURFER

	TrafficCarPos[182] = <<44.9629, 6415.1523, 31.1338>>
	TrafficCarQuatX[182] = -0.0006
	TrafficCarQuatY[182] = -0.0000
	TrafficCarQuatZ[182] = -0.4186
	TrafficCarQuatW[182] = 0.9082
	TrafficCarRecording[182] = 183
	TrafficCarStartime[182] = 82566.0000
	TrafficCarModel[182] = minivan

//	TrafficCarPos[183] = <<45.2923, 6431.6035, 31.6324>>
//	TrafficCarQuatX[183] = 0.0042
//	TrafficCarQuatY[183] = -0.0013
//	TrafficCarQuatZ[183] = 0.9173
//	TrafficCarQuatW[183] = 0.3981
//	TrafficCarRecording[183] = 184
//	TrafficCarStartime[183] = 82632.0000
//	TrafficCarModel[183] = Mule

	TrafficCarPos[184] = <<24.5200, 6394.3013, 31.1302>>
	TrafficCarQuatX[184] = -0.0004
	TrafficCarQuatY[184] = 0.0000
	TrafficCarQuatZ[184] = -0.4210
	TrafficCarQuatW[184] = 0.9071
	TrafficCarRecording[184] = 185
	TrafficCarStartime[184] = 82962.0000
	TrafficCarModel[184] = minivan

//	TrafficCarPos[185] = <<10.2365, 6396.2900, 31.4119>>
//	TrafficCarQuatX[185] = -0.0013
//	TrafficCarQuatY[185] = -0.0031
//	TrafficCarQuatZ[185] = 0.9279
//	TrafficCarQuatW[185] = 0.3729
//	TrafficCarRecording[185] = 186
//	TrafficCarStartime[185] = 83094.0000
//	TrafficCarModel[185] = Mule

	TrafficCarPos[186] = <<47.3566, 6417.5459, 31.4162>>
	TrafficCarQuatX[186] = -0.0047
	TrafficCarQuatY[186] = 0.0019
	TrafficCarQuatZ[186] = -0.3832
	TrafficCarQuatW[186] = 0.9237
	TrafficCarRecording[186] = 187
	TrafficCarStartime[186] = 83424.0000
	TrafficCarModel[186] = Mule

	TrafficCarPos[187] = <<9.9551, 6396.0161, 31.1338>>
	TrafficCarQuatX[187] = 0.0002
	TrafficCarQuatY[187] = -0.0006
	TrafficCarQuatZ[187] = 0.9382
	TrafficCarQuatW[187] = 0.3461
	TrafficCarRecording[187] = 188
	TrafficCarStartime[187] = 83622.0000
	TrafficCarModel[187] = minivan

	TrafficCarPos[188] = <<13.5893, 6399.6494, 31.5825>>
	TrafficCarQuatX[188] = 0.0002
	TrafficCarQuatY[188] = 0.0004
	TrafficCarQuatZ[188] = 0.9260
	TrafficCarQuatW[188] = 0.3776
	TrafficCarRecording[188] = 189
	TrafficCarStartime[188] = 83886.0000
	TrafficCarModel[188] = Mule

//	TrafficCarPos[189] = <<-21.2510, 6364.5601, 31.0957>>
//	TrafficCarQuatX[189] = -0.0001
//	TrafficCarQuatY[189] = -0.0007
//	TrafficCarQuatZ[189] = 0.9272
//	TrafficCarQuatW[189] = 0.3745
//	TrafficCarRecording[189] = 190
//	TrafficCarStartime[189] = 84150.0000
//	TrafficCarModel[189] = journey

	TrafficCarPos[190] = <<-2.7613, 6366.9287, 31.1338>>
	TrafficCarQuatX[190] = -0.0006
	TrafficCarQuatY[190] = -0.0000
	TrafficCarQuatZ[190] = -0.4186
	TrafficCarQuatW[190] = 0.9082
	TrafficCarRecording[190] = 191
	TrafficCarStartime[190] = 84216.0000
	TrafficCarModel[190] = minivan

//	TrafficCarPos[191] = <<-33.7674, 6352.7920, 31.1266>>
//	TrafficCarQuatX[191] = -0.0004
//	TrafficCarQuatY[191] = -0.0007
//	TrafficCarQuatZ[191] = 0.9040
//	TrafficCarQuatW[191] = 0.4275
//	TrafficCarRecording[191] = 192
//	TrafficCarStartime[191] = 84942.0000
//	TrafficCarModel[191] = minivan

	TrafficCarPos[192] = <<-4.7622, 6364.7031, 31.5749>>
	TrafficCarQuatX[192] = -0.0012
	TrafficCarQuatY[192] = 0.0005
	TrafficCarQuatZ[192] = -0.3836
	TrafficCarQuatW[192] = 0.9235
	TrafficCarRecording[192] = 193
	TrafficCarStartime[192] = 85206.0000
	TrafficCarModel[192] = Mule

//	TrafficCarPos[193] = <<-51.5284, 6333.8555, 31.1273>>
//	TrafficCarQuatX[193] = -0.0002
//	TrafficCarQuatY[193] = -0.0007
//	TrafficCarQuatZ[193] = 0.9352
//	TrafficCarQuatW[193] = 0.3542
//	TrafficCarRecording[193] = 194
//	TrafficCarStartime[193] = 85602.0000
//	TrafficCarModel[193] = minivan

	TrafficCarPos[194] = <<-46.7229, 6323.0806, 30.9614>>
	TrafficCarQuatX[194] = -0.0005
	TrafficCarQuatY[194] = 0.0002
	TrafficCarQuatZ[194] = -0.3384
	TrafficCarQuatW[194] = 0.9410
	TrafficCarRecording[194] = 195
	TrafficCarStartime[194] = 86064.0000
	TrafficCarModel[194] = SURFER

//	TrafficCarPos[195] = <<-61.5199, 6323.9727, 31.5788>>
//	TrafficCarQuatX[195] = -0.0016
//	TrafficCarQuatY[195] = -0.0034
//	TrafficCarQuatZ[195] = 0.9254
//	TrafficCarQuatW[195] = 0.3791
//	TrafficCarRecording[195] = 196
//	TrafficCarStartime[195] = 86790.0000
//	TrafficCarModel[195] = Mule

	TrafficCarPos[196] = <<-57.0975, 6312.9463, 31.1302>>
	TrafficCarQuatX[196] = -0.0010
	TrafficCarQuatY[196] = 0.0002
	TrafficCarQuatZ[196] = -0.3729
	TrafficCarQuatW[196] = 0.9279
	TrafficCarRecording[196] = 197
	TrafficCarStartime[196] = 86922.0000
	TrafficCarModel[196] = minivan

	TrafficCarPos[197] = <<-118.5549, 6259.6343, 30.9714>>
	TrafficCarQuatX[197] = 0.0034
	TrafficCarQuatY[197] = -0.0035
	TrafficCarQuatZ[197] = -0.3943
	TrafficCarQuatW[197] = 0.9190
	TrafficCarRecording[197] = 198
	TrafficCarStartime[197] = 87450.0000
	TrafficCarModel[197] = minivan

	ParkedCarPos[12] = <<-139.0886, 6276.7036, 31.0148>>
	ParkedCarQuatX[12] = -0.0000
	ParkedCarQuatY[12] = 0.0000
	ParkedCarQuatZ[12] = 0.4019
	ParkedCarQuatW[12] = 0.9157
	ParkedCarModel[12] = minivan

	ParkedCarPos[13] = <<-104.6942, 6263.7051, 30.2543>>
	ParkedCarQuatX[13] = -0.0000
	ParkedCarQuatY[13] = 0.0000
	ParkedCarQuatZ[13] = -0.3802
	ParkedCarQuatW[13] = 0.9249
	ParkedCarModel[13] = Mule

	TrafficCarPos[198] = <<-129.7646, 6239.8472, 30.9729>>
	TrafficCarQuatX[198] = -0.0003
	TrafficCarQuatY[198] = 0.0004
	TrafficCarQuatZ[198] = -0.3557
	TrafficCarQuatW[198] = 0.9346
	TrafficCarRecording[198] = 199
	TrafficCarStartime[198] = 87978.0000
	TrafficCarModel[198] = minivan

	ParkedCarPos[14] = <<-131.4290, 6282.8022, 31.0148>>
	ParkedCarQuatX[14] = 0.0000
	ParkedCarQuatY[14] = 0.0000
	ParkedCarQuatZ[14] = 0.9305
	ParkedCarQuatW[14] = -0.3662
	ParkedCarModel[14] = minivan

	TrafficCarPos[199] = <<-143.0423, 6242.1479, 30.9584>>
	TrafficCarQuatX[199] = 0.0007
	TrafficCarQuatY[199] = 0.0017
	TrafficCarQuatZ[199] = 0.9282
	TrafficCarQuatW[199] = 0.3721
	TrafficCarRecording[199] = 200
	TrafficCarStartime[199] = 88110.0000
	TrafficCarModel[199] = journey

	ParkedCarPos[15] = <<-142.2711, 6252.6636, 31.2133>>
	ParkedCarQuatX[15] = 0.0000
	ParkedCarQuatY[15] = 0.0000
	ParkedCarQuatZ[15] = 0.7589
	ParkedCarQuatW[15] = -0.6512
	ParkedCarModel[15] = minivan

	TrafficCarPos[200] = <<-149.4187, 6219.9175, 31.4495>>
	TrafficCarQuatX[200] = -0.0013
	TrafficCarQuatY[200] = 0.0007
	TrafficCarQuatZ[200] = -0.3658
	TrafficCarQuatW[200] = 0.9307
	TrafficCarRecording[200] = 201
	TrafficCarStartime[200] = 88638.0000
	TrafficCarModel[200] = Mule

	TrafficCarPos[201] = <<-159.8186, 6226.0146, 31.0039>>
	TrafficCarQuatX[201] = 0.0001
	TrafficCarQuatY[201] = -0.0003
	TrafficCarQuatZ[201] = 0.9112
	TrafficCarQuatW[201] = 0.4121
	TrafficCarRecording[201] = 202
	TrafficCarStartime[201] = 88704.0000
	TrafficCarModel[201] = minivan

	TrafficCarPos[202] = <<-156.5760, 6212.6206, 31.2925>>
	TrafficCarQuatX[202] = -0.0032
	TrafficCarQuatY[202] = 0.0011
	TrafficCarQuatZ[202] = -0.3874
	TrafficCarQuatW[202] = 0.9219
	TrafficCarRecording[202] = 203
	TrafficCarStartime[202] = 89232.0000
	TrafficCarModel[202] = Mule

	TrafficCarPos[203] = <<-177.8305, 6207.8335, 30.8371>>
	TrafficCarQuatX[203] = -0.0005
	TrafficCarQuatY[203] = -0.0003
	TrafficCarQuatZ[203] = 0.9093
	TrafficCarQuatW[203] = 0.4161
	TrafficCarRecording[203] = 204
	TrafficCarStartime[203] = 89298.0000
	TrafficCarModel[203] = SURFER

	ParkedCarPos[16] = <<-197.4503, 6228.7759, 30.9489>>
	ParkedCarQuatX[16] = -0.0000
	ParkedCarQuatY[16] = 0.0000
	ParkedCarQuatZ[16] = 0.4072
	ParkedCarQuatW[16] = 0.9133
	ParkedCarModel[16] = minivan

	TrafficCarPos[204] = <<-200.7223, 6184.8062, 31.0163>>
	TrafficCarQuatX[204] = 0.0039
	TrafficCarQuatY[204] = -0.0021
	TrafficCarQuatZ[204] = 0.9257
	TrafficCarQuatW[204] = 0.3782
	TrafficCarRecording[204] = 205
	TrafficCarStartime[204] = 90024.0000
	TrafficCarModel[204] = journey

	ParkedCarPos[17] = <<-238.7583, 6196.2285, 31.1881>>
	ParkedCarQuatX[17] = -0.0000
	ParkedCarQuatY[17] = 0.0000
	ParkedCarQuatZ[17] = -0.3988
	ParkedCarQuatW[17] = 0.9170
	ParkedCarModel[17] = minivan

//	TrafficCarPos[205] = <<-192.7801, 6176.6592, 31.0585>>
//	TrafficCarQuatX[205] = -0.0019
//	TrafficCarQuatY[205] = -0.0038
//	TrafficCarQuatZ[205] = -0.3478
//	TrafficCarQuatW[205] = 0.9375
//	TrafficCarRecording[205] = 206
//	TrafficCarStartime[205] = 90750.0000
//	TrafficCarModel[205] = minivan

	TrafficCarPos[206] = <<-200.2096, 6169.2295, 31.5074>>
	TrafficCarQuatX[206] = -0.0012
	TrafficCarQuatY[206] = -0.0042
	TrafficCarQuatZ[206] = -0.3741
	TrafficCarQuatW[206] = 0.9274
	TrafficCarRecording[206] = 207
	TrafficCarStartime[206] = 91080.0000
	TrafficCarModel[206] = Mule

	TrafficCarPos[207] = <<-219.4954, 6166.0654, 31.0585>>
	TrafficCarQuatX[207] = 0.0039
	TrafficCarQuatY[207] = -0.0023
	TrafficCarQuatZ[207] = 0.9082
	TrafficCarQuatW[207] = 0.4186
	TrafficCarRecording[207] = 208
	TrafficCarStartime[207] = 91278.0000
	TrafficCarModel[207] = minivan

	TrafficCarPos[208] = <<-242.7871, 6133.8745, 31.0087>>
	TrafficCarQuatX[208] = -0.0016
	TrafficCarQuatY[208] = -0.0034
	TrafficCarQuatZ[208] = -0.3642
	TrafficCarQuatW[208] = 0.9313
	TrafficCarRecording[208] = 209
	TrafficCarStartime[208] = 91542.0000
	TrafficCarModel[208] = minivan

	ParkedCarPos[18] = <<-227.0850, 6120.9082, 31.4034>>
	ParkedCarQuatX[18] = 0.0000
	ParkedCarQuatY[18] = 0.0000
	ParkedCarQuatZ[18] = 0.9135
	ParkedCarQuatW[18] = 0.4068
	ParkedCarModel[18] = minivan

	TrafficCarPos[209] = <<-273.2502, 6094.9175, 31.3158>>
	TrafficCarQuatX[209] = -0.0014
	TrafficCarQuatY[209] = -0.0017
	TrafficCarQuatZ[209] = -0.3911
	TrafficCarQuatW[209] = 0.9203
	TrafficCarRecording[209] = 210
	TrafficCarStartime[209] = 92664.0000
	TrafficCarModel[209] = Mule

//	TrafficCarPos[210] = <<-281.2550, 6102.5186, 31.0241>>
//	TrafficCarQuatX[210] = 0.0035
//	TrafficCarQuatY[210] = -0.0031
//	TrafficCarQuatZ[210] = 0.9364
//	TrafficCarQuatW[210] = 0.3509
//	TrafficCarRecording[210] = 211
//	TrafficCarStartime[210] = 92664.0000
//	TrafficCarModel[210] = minivan

	ParkedCarPos[19] = <<-268.2806, 6066.7236, 31.4731>>
	ParkedCarQuatX[19] = -0.0000
	ParkedCarQuatY[19] = 0.0000
	ParkedCarQuatZ[19] = -0.5672
	ParkedCarQuatW[19] = 0.8236
	ParkedCarModel[19] = minivan

	TrafficCarPos[211] = <<-320.4204, 6065.1772, 31.4226>>
	TrafficCarQuatX[211] = 0.0022
	TrafficCarQuatY[211] = -0.0010
	TrafficCarQuatZ[211] = 0.9238
	TrafficCarQuatW[211] = 0.3829
	TrafficCarRecording[211] = 212
	TrafficCarStartime[211] = 93918.0000
	TrafficCarModel[211] = Mule

	ParkedCarPos[20] = <<-343.3658, 6076.2744, 31.2703>>
	ParkedCarQuatX[20] = 0.0000
	ParkedCarQuatY[20] = 0.0000
	ParkedCarQuatZ[20] = 0.9055
	ParkedCarQuatW[20] = -0.4243
	ParkedCarModel[20] = minivan

	TrafficCarPos[212] = <<-342.7433, 6026.7881, 30.9903>>
	TrafficCarQuatX[212] = -0.0027
	TrafficCarQuatY[212] = -0.0012
	TrafficCarQuatZ[212] = -0.3927
	TrafficCarQuatW[212] = 0.9197
	TrafficCarRecording[212] = 213
	TrafficCarStartime[212] = 94908.0000
	TrafficCarModel[212] = minivan

	TrafficCarPos[213] = <<-345.5276, 6024.0957, 30.8184>>
	TrafficCarQuatX[213] = -0.0024
	TrafficCarQuatY[213] = 0.0009
	TrafficCarQuatZ[213] = -0.3320
	TrafficCarQuatW[213] = 0.9433
	TrafficCarRecording[213] = 214
	TrafficCarStartime[213] = 95238.0000
	TrafficCarModel[213] = SURFER

	TrafficCarPos[214] = <<-366.1345, 6019.9106, 31.0597>>
	TrafficCarQuatX[214] = 0.0047
	TrafficCarQuatY[214] = 0.0006
	TrafficCarQuatZ[214] = 0.9468
	TrafficCarQuatW[214] = 0.3219
	TrafficCarRecording[214] = 215
	TrafficCarStartime[214] = 95436.0000
	TrafficCarModel[214] = journey

	TrafficCarPos[215] = <<-386.3470, 5988.0854, 31.3453>>
	TrafficCarQuatX[215] = -0.0057
	TrafficCarQuatY[215] = -0.0060
	TrafficCarQuatZ[215] = -0.3475
	TrafficCarQuatW[215] = 0.9376
	TrafficCarRecording[215] = 216
	TrafficCarStartime[215] = 96228.0000
	TrafficCarModel[215] = minivan

	TrafficCarPos[216] = <<-397.9482, 6021.1909, 31.1729>>
	TrafficCarQuatX[216] = -0.0166
	TrafficCarQuatY[216] = -0.0055
	TrafficCarQuatZ[216] = 0.9252
	TrafficCarQuatW[216] = -0.3791
	TrafficCarRecording[216] = 217
	TrafficCarStartime[216] = 96426.0000
	TrafficCarModel[216] = minivan

	TrafficCarPos[217] = <<-396.4938, 5985.0698, 31.6773>>
	TrafficCarQuatX[217] = 0.0010
	TrafficCarQuatY[217] = 0.0024
	TrafficCarQuatZ[217] = 0.9301
	TrafficCarQuatW[217] = 0.3674
	TrafficCarRecording[217] = 218
	TrafficCarStartime[217] = 96492.0000
	TrafficCarModel[217] = Mule

	TrafficCarPos[218] = <<-385.9991, 5979.5903, 31.8351>>
	TrafficCarQuatX[218] = -0.0034
	TrafficCarQuatY[218] = 0.0014
	TrafficCarQuatZ[218] = -0.3756
	TrafficCarQuatW[218] = 0.9268
	TrafficCarRecording[218] = 219
	TrafficCarStartime[218] = 96558.0000
	TrafficCarModel[218] = Mule

	TrafficCarPos[219] = <<-425.9279, 5933.4028, 32.0897>>
	TrafficCarQuatX[219] = -0.0076
	TrafficCarQuatY[219] = 0.0026
	TrafficCarQuatZ[219] = -0.2957
	TrafficCarQuatW[219] = 0.9553
	TrafficCarRecording[219] = 220
	TrafficCarStartime[219] = 97812.0000
	TrafficCarModel[219] = minivan

//	TrafficCarPos[220] = <<-440.7476, 5932.4507, 32.1078>>
//	TrafficCarQuatX[220] = -0.0058
//	TrafficCarQuatY[220] = 0.0100
//	TrafficCarQuatZ[220] = 0.9463
//	TrafficCarQuatW[220] = 0.3230
//	TrafficCarRecording[220] = 221
//	TrafficCarStartime[220] = 98076.0000
//	TrafficCarModel[220] = journey

	TrafficCarPos[221] = <<-450.5932, 5901.5835, 32.5169>>
	TrafficCarQuatX[221] = -0.0070
	TrafficCarQuatY[221] = 0.0019
	TrafficCarQuatZ[221] = -0.2731
	TrafficCarQuatW[221] = 0.9620
	TrafficCarRecording[221] = 222
	TrafficCarStartime[221] = 98736.0000
	TrafficCarModel[221] = SURFER

//	TrafficCarPos[222] = <<-468.5921, 5896.4658, 32.9075>>
//	TrafficCarQuatX[222] = 0.0021
//	TrafficCarQuatY[222] = 0.0068
//	TrafficCarQuatZ[222] = 0.9483
//	TrafficCarQuatW[222] = 0.3173
//	TrafficCarRecording[222] = 223
//	TrafficCarStartime[222] = 99066.0000
//	TrafficCarModel[222] = minivan

//	TrafficCarPos[223] = <<-463.1865, 5903.2178, 33.0637>>
//	TrafficCarQuatX[223] = 0.0014
//	TrafficCarQuatY[223] = 0.0042
//	TrafficCarQuatZ[223] = 0.9487
//	TrafficCarQuatW[223] = 0.3161
//	TrafficCarRecording[223] = 224
//	TrafficCarStartime[223] = 99264.0000
//	TrafficCarModel[223] = Mule

	TrafficCarPos[224] = <<-451.5227, 5899.9282, 33.1721>>
	TrafficCarQuatX[224] = -0.0071
	TrafficCarQuatY[224] = 0.0025
	TrafficCarQuatZ[224] = -0.3279
	TrafficCarQuatW[224] = 0.9447
	TrafficCarRecording[224] = 225
	TrafficCarStartime[224] = 99264.0000
	TrafficCarModel[224] = Mule

//	TrafficCarPos[225] = <<-494.0117, 5858.7041, 33.5587>>
//	TrafficCarQuatX[225] = 0.0022
//	TrafficCarQuatY[225] = 0.0078
//	TrafficCarQuatZ[225] = 0.9588
//	TrafficCarQuatW[225] = 0.2840
//	TrafficCarRecording[225] = 226
//	TrafficCarStartime[225] = 100122.0000
//	TrafficCarModel[225] = journey

//	TrafficCarPos[226] = <<-492.1231, 5861.6958, 32.9334>>
//	TrafficCarQuatX[226] = -0.0199
//	TrafficCarQuatY[226] = -0.0957
//	TrafficCarQuatZ[226] = 0.9547
//	TrafficCarQuatW[226] = 0.2812
//	TrafficCarRecording[226] = 227
//	TrafficCarStartime[226] = 100320.0000
//	TrafficCarModel[226] = minivan

	TrafficCarPos[227] = <<-494.7882, 5834.9268, 33.9774>>
	TrafficCarQuatX[227] = -0.0086
	TrafficCarQuatY[227] = 0.0026
	TrafficCarQuatZ[227] = -0.2869
	TrafficCarQuatW[227] = 0.9579
	TrafficCarRecording[227] = 228
	TrafficCarStartime[227] = 100584.0000
	TrafficCarModel[227] = minivan

	TrafficCarPos[228] = <<-512.1516, 5804.1235, 34.6696>>
	TrafficCarQuatX[228] = -0.0113
	TrafficCarQuatY[228] = -0.0016
	TrafficCarQuatZ[228] = -0.2395
	TrafficCarQuatW[228] = 0.9708
	TrafficCarRecording[228] = 229
	TrafficCarStartime[228] = 101310.0000
	TrafficCarModel[228] = journey

//	TrafficCarPos[229] = <<-533.5082, 5786.8188, 35.2332>>
//	TrafficCarQuatX[229] = 0.0049
//	TrafficCarQuatY[229] = 0.0109
//	TrafficCarQuatZ[229] = 0.9775
//	TrafficCarQuatW[229] = 0.2106
//	TrafficCarRecording[229] = 230
//	TrafficCarStartime[229] = 101970.0000
//	TrafficCarModel[229] = minivan

	TrafficCarPos[230] = <<-522.2422, 5783.0591, 35.6767>>
	TrafficCarQuatX[230] = -0.0124
	TrafficCarQuatY[230] = -0.0021
	TrafficCarQuatZ[230] = -0.1982
	TrafficCarQuatW[230] = 0.9801
	TrafficCarRecording[230] = 231
	TrafficCarStartime[230] = 102366.0000
	TrafficCarModel[230] = Mule

	TrafficCarPos[231] = <<-544.5030, 5760.5815, 35.6938>>
	TrafficCarQuatX[231] = 0.0025
	TrafficCarQuatY[231] = 0.0112
	TrafficCarQuatZ[231] = 0.9733
	TrafficCarQuatW[231] = 0.2292
	TrafficCarRecording[231] = 232
	TrafficCarStartime[231] = 102630.0000
	TrafficCarModel[231] = SURFER

	TrafficCarPos[232] = <<-540.8187, 5768.0986, 35.9515>>
	TrafficCarQuatX[232] = 0.0016
	TrafficCarQuatY[232] = 0.0081
	TrafficCarQuatZ[232] = 0.9812
	TrafficCarQuatW[232] = 0.1929
	TrafficCarRecording[232] = 233
	TrafficCarStartime[232] = 102828.0000
	TrafficCarModel[232] = Mule

	TrafficCarPos[233] = <<-540.1239, 5740.1914, 36.2707>>
	TrafficCarQuatX[233] = -0.0113
	TrafficCarQuatY[233] = 0.0020
	TrafficCarQuatZ[233] = -0.1930
	TrafficCarQuatW[233] = 0.9811
	TrafficCarRecording[233] = 234
	TrafficCarStartime[233] = 104346.0000
	TrafficCarModel[233] = minivan

	TrafficCarPos[234] = <<-567.8047, 5677.1108, 37.7006>>
	TrafficCarQuatX[234] = -0.0100
	TrafficCarQuatY[234] = -0.0015
	TrafficCarQuatZ[234] = -0.2147
	TrafficCarQuatW[234] = 0.9766
	TrafficCarRecording[234] = 235
	TrafficCarStartime[234] = 104676.0000
	TrafficCarModel[234] = journey

	TrafficCarPos[235] = <<-578.9499, 5679.6934, 38.2365>>
	TrafficCarQuatX[235] = 0.0060
	TrafficCarQuatY[235] = 0.0086
	TrafficCarQuatZ[235] = 0.9778
	TrafficCarQuatW[235] = 0.2094
	TrafficCarRecording[235] = 236
	TrafficCarStartime[235] = 104676.0000
	TrafficCarModel[235] = Mule

	TrafficCarPos[236] = <<-582.9236, 5643.6528, 38.4277>>
	TrafficCarQuatX[236] = -0.0074
	TrafficCarQuatY[236] = -0.0028
	TrafficCarQuatZ[236] = -0.2457
	TrafficCarQuatW[236] = 0.9693
	TrafficCarRecording[236] = 237
	TrafficCarStartime[236] = 105534.0000
	TrafficCarModel[236] = minivan

	TrafficCarPos[237] = <<-603.3911, 5611.9170, 38.8401>>
	TrafficCarQuatX[237] = -0.0034
	TrafficCarQuatY[237] = -0.0034
	TrafficCarQuatZ[237] = -0.3058
	TrafficCarQuatW[237] = 0.9521
	TrafficCarRecording[237] = 238
	TrafficCarStartime[237] = 106458.0000
	TrafficCarModel[237] = minivan

	TrafficCarPos[238] = <<-617.3054, 5612.0728, 39.1315>>
	TrafficCarQuatX[238] = 0.0027
	TrafficCarQuatY[238] = 0.0034
	TrafficCarQuatZ[238] = 0.9518
	TrafficCarQuatW[238] = 0.3068
	TrafficCarRecording[238] = 239
	TrafficCarStartime[238] = 106524.0000
	TrafficCarModel[238] = Mule

	TrafficCarPos[239] = <<-631.8846, 5579.8950, 38.7442>>
	TrafficCarQuatX[239] = 0.0031
	TrafficCarQuatY[239] = -0.0014
	TrafficCarQuatZ[239] = -0.4165
	TrafficCarQuatW[239] = 0.9091
	TrafficCarRecording[239] = 240
	TrafficCarStartime[239] = 107382.0000
	TrafficCarModel[239] = journey

	TrafficCarPos[240] = <<-652.1945, 5563.6494, 38.9956>>
	TrafficCarQuatX[240] = 0.0059
	TrafficCarQuatY[240] = -0.0028
	TrafficCarQuatZ[240] = -0.4297
	TrafficCarQuatW[240] = 0.9029
	TrafficCarRecording[240] = 241
	TrafficCarStartime[240] = 107910.0000
	TrafficCarModel[240] = Mule

//	TrafficCarPos[241] = <<-675.7839, 5560.0620, 38.2339>>
//	TrafficCarQuatX[241] = -0.0041
//	TrafficCarQuatY[241] = -0.0078
//	TrafficCarQuatZ[241] = 0.8853
//	TrafficCarQuatW[241] = 0.4650
//	TrafficCarRecording[241] = 242
//	TrafficCarStartime[241] = 108306.0000
//	TrafficCarModel[241] = minivan

	TrafficCarPos[242] = <<-681.4705, 5543.0757, 37.9190>>
	TrafficCarQuatX[242] = 0.0095
	TrafficCarQuatY[242] = -0.0100
	TrafficCarQuatZ[242] = -0.4741
	TrafficCarQuatW[242] = 0.8803
	TrafficCarRecording[242] = 243
	TrafficCarStartime[242] = 108702.0000
	TrafficCarModel[242] = journey

	TrafficCarPos[243] = <<-698.1723, 5532.0845, 37.6632>>
	TrafficCarQuatX[243] = 0.0090
	TrafficCarQuatY[243] = -0.0100
	TrafficCarQuatZ[243] = -0.4885
	TrafficCarQuatW[243] = 0.8724
	TrafficCarRecording[243] = 244
	TrafficCarStartime[243] = 109164.0000
	TrafficCarModel[243] = Mule

	TrafficCarPos[244] = <<-723.4635, 5515.5347, 36.1629>>
	TrafficCarQuatX[244] = 0.0138
	TrafficCarQuatY[244] = -0.0127
	TrafficCarQuatZ[244] = -0.4765
	TrafficCarQuatW[244] = 0.8790
	TrafficCarRecording[244] = 245
	TrafficCarStartime[244] = 111276.0000
	TrafficCarModel[244] = SURFER

	TrafficCarPos[245] = <<-771.7394, 5485.4854, 34.3527>>
	TrafficCarQuatX[245] = 0.0092
	TrafficCarQuatY[245] = -0.0046
	TrafficCarQuatZ[245] = -0.4793
	TrafficCarQuatW[245] = 0.8776
	TrafficCarRecording[245] = 246
	TrafficCarStartime[245] = 111408.0000
	TrafficCarModel[245] = journey

	TrafficCarPos[246] = <<-780.1580, 5479.9624, 34.1691>>
	TrafficCarQuatX[246] = 0.0085
	TrafficCarQuatY[246] = -0.0063
	TrafficCarQuatZ[246] = -0.4854
	TrafficCarQuatW[246] = 0.8742
	TrafficCarRecording[246] = 247
	TrafficCarStartime[246] = 111606.0000
	TrafficCarModel[246] = minivan

	TrafficCarPos[247] = <<-751.5074, 5512.3467, 35.3637>>
	TrafficCarQuatX[247] = -0.0086
	TrafficCarQuatY[247] = -0.0153
	TrafficCarQuatZ[247] = 0.8750
	TrafficCarQuatW[247] = 0.4838
	TrafficCarRecording[247] = 248
	TrafficCarStartime[247] = 111870.0000
	TrafficCarModel[247] = minivan

	TrafficCarPos[248] = <<-782.3395, 5478.5220, 34.5592>>
	TrafficCarQuatX[248] = 0.0088
	TrafficCarQuatY[248] = -0.0068
	TrafficCarQuatZ[248] = -0.4877
	TrafficCarQuatW[248] = 0.8729
	TrafficCarRecording[248] = 249
	TrafficCarStartime[248] = 111936.0000
	TrafficCarModel[248] = Mule

	TrafficCarPos[249] = <<-797.4579, 5469.0874, 33.8033>>
	TrafficCarQuatX[249] = 0.0048
	TrafficCarQuatY[249] = -0.0049
	TrafficCarQuatZ[249] = -0.4470
	TrafficCarQuatW[249] = 0.8945
	TrafficCarRecording[249] = 250
	TrafficCarStartime[249] = 112134.0000
	TrafficCarModel[249] = minivan

	TrafficCarPos[250] = <<-802.7859, 5465.8076, 33.6970>>
	TrafficCarQuatX[250] = 0.0022
	TrafficCarQuatY[250] = -0.0034
	TrafficCarQuatZ[250] = -0.4630
	TrafficCarQuatW[250] = 0.8864
	TrafficCarRecording[250] = 251
	TrafficCarStartime[250] = 112530.0000
	TrafficCarModel[250] = journey

//	TrafficCarPos[251] = <<-838.0173, 5458.9814, 33.7862>>
//	TrafficCarQuatX[251] = 0.0082
//	TrafficCarQuatY[251] = 0.0014
//	TrafficCarQuatZ[251] = 0.8736
//	TrafficCarQuatW[251] = 0.4865
//	TrafficCarRecording[251] = 252
//	TrafficCarStartime[251] = 112860.0000
//	TrafficCarModel[251] = minivan

//	TrafficCarPos[252] = <<-833.3596, 5462.4761, 34.0031>>
//	TrafficCarQuatX[252] = 0.0028
//	TrafficCarQuatY[252] = 0.0006
//	TrafficCarQuatZ[252] = 0.8788
//	TrafficCarQuatW[252] = 0.4771
//	TrafficCarRecording[252] = 253
//	TrafficCarStartime[252] = 113190.0000
//	TrafficCarModel[252] = Mule

	TrafficCarPos[253] = <<-859.1169, 5432.9985, 34.5357>>
	TrafficCarQuatX[253] = -0.0169
	TrafficCarQuatY[253] = 0.0056
	TrafficCarQuatZ[253] = -0.5386
	TrafficCarQuatW[253] = 0.8424
	TrafficCarRecording[253] = 254
	TrafficCarStartime[253] = 113652.0000
	TrafficCarModel[253] = minivan

//	TrafficCarPos[254] = <<-857.1628, 5447.7319, 33.8215>>
//	TrafficCarQuatX[254] = 0.0118
//	TrafficCarQuatY[254] = 0.0106
//	TrafficCarQuatZ[254] = 0.8537
//	TrafficCarQuatW[254] = 0.5206
//	TrafficCarRecording[254] = 255
//	TrafficCarStartime[254] = 113718.0000
//	TrafficCarModel[254] = emperor

	ParkedCarPos[21] = <<-860.6396, 5415.5566, 34.4756>>
	ParkedCarQuatX[21] = 0.0733
	ParkedCarQuatY[21] = 0.0470
	ParkedCarQuatZ[21] = 0.7841
	ParkedCarQuatW[21] = -0.6145
	ParkedCarModel[21] = emperor

	TrafficCarPos[255] = <<-886.9130, 5421.8223, 35.2647>>
	TrafficCarQuatX[255] = -0.0193
	TrafficCarQuatY[255] = 0.0078
	TrafficCarQuatZ[255] = -0.5541
	TrafficCarQuatW[255] = 0.8322
	TrafficCarRecording[255] = 256
	TrafficCarStartime[255] = 114312.0000
	TrafficCarModel[255] = emperor

	TrafficCarPos[256] = <<-902.9290, 5422.2612, 36.2460>>
	TrafficCarQuatX[256] = -0.0162
	TrafficCarQuatY[256] = 0.0127
	TrafficCarQuatZ[256] = -0.6064
	TrafficCarQuatW[256] = 0.7949
	TrafficCarRecording[256] = 257
	TrafficCarStartime[256] = 114708.0000
	TrafficCarModel[256] = journey

	TrafficCarPos[257] = <<-915.9521, 5418.4751, 36.7077>>
	TrafficCarQuatX[257] = -0.0183
	TrafficCarQuatY[257] = 0.0146
	TrafficCarQuatZ[257] = -0.6257
	TrafficCarQuatW[257] = 0.7797
	TrafficCarRecording[257] = 258
	TrafficCarStartime[257] = 114972.0000
	TrafficCarModel[257] = SURFER

	TrafficCarPos[258] = <<-928.0029, 5414.9702, 37.9183>>
	TrafficCarQuatX[258] = -0.0186
	TrafficCarQuatY[258] = 0.0139
	TrafficCarQuatZ[258] = -0.6015
	TrafficCarQuatW[258] = 0.7985
	TrafficCarRecording[258] = 259
	TrafficCarStartime[258] = 115236.0000
	TrafficCarModel[258] = Mule

	TrafficCarPos[259] = <<-933.9056, 5421.6489, 37.2376>>
	TrafficCarQuatX[259] = 0.0170
	TrafficCarQuatY[259] = 0.0160
	TrafficCarQuatZ[259] = 0.7839
	TrafficCarQuatW[259] = 0.6205
	TrafficCarRecording[259] = 260
	TrafficCarStartime[259] = 115236.0000
	TrafficCarModel[259] = emperor

//	TrafficCarPos[260] = <<-985.4686, 5400.2017, 40.2372>>
//	TrafficCarQuatX[260] = 0.0153
//	TrafficCarQuatY[260] = 0.0172
//	TrafficCarQuatZ[260] = 0.8716
//	TrafficCarQuatW[260] = 0.4897
//	TrafficCarRecording[260] = 261
//	TrafficCarStartime[260] = 116490.0000
//	TrafficCarModel[260] = minivan

	TrafficCarPos[261] = <<-1004.6003, 5376.7871, 41.1807>>
	TrafficCarQuatX[261] = -0.0222
	TrafficCarQuatY[261] = 0.0058
	TrafficCarQuatZ[261] = -0.4104
	TrafficCarQuatW[261] = 0.9116
	TrafficCarRecording[261] = 262
	TrafficCarStartime[261] = 117150.0000
	TrafficCarModel[261] = emperor

//	TrafficCarPos[262] = <<-1026.2605, 5369.4600, 42.4221>>
//	TrafficCarQuatX[262] = 0.0116
//	TrafficCarQuatY[262] = 0.0169
//	TrafficCarQuatZ[262] = 0.9287
//	TrafficCarQuatW[262] = 0.3703
//	TrafficCarRecording[262] = 263
//	TrafficCarStartime[262] = 117612.0000
//	TrafficCarModel[262] = journey

	TrafficCarPos[263] = <<-1064.3765, 5337.0894, 44.8801>>
	TrafficCarQuatX[263] = 0.0202
	TrafficCarQuatY[263] = 0.0325
	TrafficCarQuatZ[263] = 0.8517
	TrafficCarQuatW[263] = 0.5226
	TrafficCarRecording[263] = 264
	TrafficCarStartime[263] = 118800.0000
	TrafficCarModel[263] = emperor

	TrafficCarPos[264] = <<-1033.5538, 5346.6406, 43.3626>>
	TrafficCarQuatX[264] = -0.0208
	TrafficCarQuatY[264] = 0.0066
	TrafficCarQuatZ[264] = -0.3975
	TrafficCarQuatW[264] = 0.9173
	TrafficCarRecording[264] = 265
	TrafficCarStartime[264] = 119394.0000
	TrafficCarModel[264] = minivan

	TrafficCarPos[265] = <<-1098.0574, 5324.0342, 47.8281>>
	TrafficCarQuatX[265] = 0.0294
	TrafficCarQuatY[265] = 0.0288
	TrafficCarQuatZ[265] = 0.8094
	TrafficCarQuatW[265] = 0.5858
	TrafficCarRecording[265] = 266
	TrafficCarStartime[265] = 119592.0000
	TrafficCarModel[265] = emperor

//	TrafficCarPos[266] = <<-1113.1876, 5305.8076, 50.4049>>
//	TrafficCarQuatX[266] = -0.0359
//	TrafficCarQuatY[266] = 0.0175
//	TrafficCarQuatZ[266] = -0.5069
//	TrafficCarQuatW[266] = 0.8611
//	TrafficCarRecording[266] = 267
//	TrafficCarStartime[266] = 120120.0000
//	TrafficCarModel[266] = Mule

//	TrafficCarPos[267] = <<-1133.8778, 5303.2563, 51.0682>>
//	TrafficCarQuatX[267] = 0.0152
//	TrafficCarQuatY[267] = 0.0316
//	TrafficCarQuatZ[267] = 0.8913
//	TrafficCarQuatW[267] = 0.4520
//	TrafficCarRecording[267] = 268
//	TrafficCarStartime[267] = 120582.0000
//	TrafficCarModel[267] = SURFER

	TrafficCarPos[268] = <<-1146.1438, 5276.4282, 53.3106>>
	TrafficCarQuatX[268] = -0.0174
	TrafficCarQuatY[268] = 0.0065
	TrafficCarQuatZ[268] = -0.3905
	TrafficCarQuatW[268] = 0.9204
	TrafficCarRecording[268] = 269
	TrafficCarStartime[268] = 121176.0000
	TrafficCarModel[268] = Mule

	TrafficCarPos[269] = <<-1167.2081, 5261.1929, 53.1992>>
	TrafficCarQuatX[269] = 0.0181
	TrafficCarQuatY[269] = -0.0049
	TrafficCarQuatZ[269] = -0.4946
	TrafficCarQuatW[269] = 0.8689
	TrafficCarRecording[269] = 270
	TrafficCarStartime[269] = 121770.0000
	TrafficCarModel[269] = minivan

	TrafficCarPos[270] = <<-1181.1008, 5255.3423, 52.4125>>
	TrafficCarQuatX[270] = 0.0222
	TrafficCarQuatY[270] = -0.0182
	TrafficCarQuatZ[270] = -0.5681
	TrafficCarQuatW[270] = 0.8225
	TrafficCarRecording[270] = 271
	TrafficCarStartime[270] = 122166.0000
	TrafficCarModel[270] = journey

//	TrafficCarPos[271] = <<-1189.4590, 5260.8638, 51.5821>>
//	TrafficCarQuatX[271] = -0.0215
//	TrafficCarQuatY[271] = -0.0235
//	TrafficCarQuatZ[271] = 0.7644
//	TrafficCarQuatW[271] = 0.6440
//	TrafficCarRecording[271] = 272
//	TrafficCarStartime[271] = 122232.0000
//	TrafficCarModel[271] = emperor

//	TrafficCarPos[272] = <<-1180.9889, 5262.6802, 53.0019>>
//	TrafficCarQuatX[272] = -0.0253
//	TrafficCarQuatY[272] = -0.0201
//	TrafficCarQuatZ[272] = 0.8120
//	TrafficCarQuatW[272] = 0.5828
//	TrafficCarRecording[272] = 273
//	TrafficCarStartime[272] = 122364.0000
//	TrafficCarModel[272] = Mule

	TrafficCarPos[273] = <<-1194.3643, 5265.0225, 51.7508>>
	TrafficCarQuatX[273] = -0.0139
	TrafficCarQuatY[273] = -0.0305
	TrafficCarQuatZ[273] = 0.7712
	TrafficCarQuatW[273] = 0.6357
	TrafficCarRecording[273] = 274
	TrafficCarStartime[273] = 122628.0000
	TrafficCarModel[273] = minivan

	TrafficCarPos[274] = <<-1231.9467, 5253.2534, 49.5905>>
	TrafficCarQuatX[274] = -0.0040
	TrafficCarQuatY[274] = 0.0105
	TrafficCarQuatZ[274] = 0.7549
	TrafficCarQuatW[274] = -0.6557
	TrafficCarRecording[274] = 275
	TrafficCarStartime[274] = 123222.0000
	TrafficCarModel[274] = emperor

	TrafficCarPos[275] = <<-1240.9534, 5255.0864, 49.8296>>
	TrafficCarQuatX[275] = 0.0025
	TrafficCarQuatY[275] = 0.0004
	TrafficCarQuatZ[275] = 0.7601
	TrafficCarQuatW[275] = -0.6498
	TrafficCarRecording[275] = 276
	TrafficCarStartime[275] = 123486.0000
	TrafficCarModel[275] = journey

	TrafficCarPos[276] = <<-1243.1129, 5255.4033, 49.8544>>
	TrafficCarQuatX[276] = -0.0025
	TrafficCarQuatY[276] = 0.0006
	TrafficCarQuatZ[276] = 0.7863
	TrafficCarQuatW[276] = -0.6178
	TrafficCarRecording[276] = 277
	TrafficCarStartime[276] = 124014.0000
	TrafficCarModel[276] = minivan

//	TrafficCarPos[277] = <<-1297.2975, 5227.6362, 54.4575>>
//	TrafficCarQuatX[277] = -0.0542
//	TrafficCarQuatY[277] = 0.0066
//	TrafficCarQuatZ[277] = -0.1993
//	TrafficCarQuatW[277] = 0.9784
//	TrafficCarRecording[277] = 278
//	TrafficCarStartime[277] = 124938.0000
//	TrafficCarModel[277] = journey

//	TrafficCarPos[278] = <<-1309.1455, 5214.5718, 55.7857>>
//	TrafficCarQuatX[278] = -0.0035
//	TrafficCarQuatY[278] = 0.0500
//	TrafficCarQuatZ[278] = 0.9921
//	TrafficCarQuatW[278] = 0.1149
//	TrafficCarRecording[278] = 279
//	TrafficCarStartime[278] = 125334.0000
//	TrafficCarModel[278] = emperor

//	TrafficCarPos[279] = <<-1307.0697, 5223.1758, 55.2818>>
//	TrafficCarQuatX[279] = 0.0056
//	TrafficCarQuatY[279] = 0.0488
//	TrafficCarQuatZ[279] = 0.9784
//	TrafficCarQuatW[279] = 0.2010
//	TrafficCarRecording[279] = 280
//	TrafficCarStartime[279] = 125532.0000
//	TrafficCarModel[279] = minivan

//	TrafficCarPos[280] = <<-1378.4095, 5100.0117, 60.9502>>
//	TrafficCarQuatX[280] = -0.0027
//	TrafficCarQuatY[280] = -0.0020
//	TrafficCarQuatZ[280] = 0.8906
//	TrafficCarQuatW[280] = 0.4548
//	TrafficCarRecording[280] = 281
//	TrafficCarStartime[280] = 128964.0000
//	TrafficCarModel[280] = journey

	TrafficCarPos[281] = <<-1395.8094, 5080.6880, 61.3721>>
	TrafficCarQuatX[281] = -0.0031
	TrafficCarQuatY[281] = -0.0056
	TrafficCarQuatZ[281] = -0.4942
	TrafficCarQuatW[281] = 0.8693
	TrafficCarRecording[281] = 282
	TrafficCarStartime[281] = 129228.0000
	TrafficCarModel[281] = Mule

	TrafficCarPos[282] = <<-1344.0863, 5116.7720, 60.9809>>
	TrafficCarQuatX[282] = 0.0056
	TrafficCarQuatY[282] = -0.0028
	TrafficCarQuatZ[282] = -0.4235
	TrafficCarQuatW[282] = 0.9059
	TrafficCarRecording[282] = 283
	TrafficCarStartime[282] = 129426.0000
	TrafficCarModel[282] = emperor

	TrafficCarPos[283] = <<-1410.3035, 5072.4902, 60.5543>>
	TrafficCarQuatX[283] = -0.0072
	TrafficCarQuatY[283] = -0.0075
	TrafficCarQuatZ[283] = -0.4753
	TrafficCarQuatW[283] = 0.8798
	TrafficCarRecording[283] = 284
	TrafficCarStartime[283] = 131208.0000
	TrafficCarModel[283] = emperor

//	TrafficCarPos[284] = <<-1456.3480, 5039.3125, 62.0023>>
//	TrafficCarQuatX[284] = -0.0119
//	TrafficCarQuatY[284] = 0.0006
//	TrafficCarQuatZ[284] = -0.4251
//	TrafficCarQuatW[284] = 0.9050
//	TrafficCarRecording[284] = 285
//	TrafficCarStartime[284] = 131604.0000
//	TrafficCarModel[284] = Mule

	TrafficCarPos[285] = <<-1465.6652, 5046.7910, 61.7431>>
	TrafficCarQuatX[285] = 0.0046
	TrafficCarQuatY[285] = 0.0100
	TrafficCarQuatZ[285] = 0.8994
	TrafficCarQuatW[285] = 0.4369
	TrafficCarRecording[285] = 286
	TrafficCarStartime[285] = 131934.0000
	TrafficCarModel[285] = minivan

//	TrafficCarPos[286] = <<-1505.3987, 4991.6504, 62.1293>>
//	TrafficCarQuatX[286] = 0.0030
//	TrafficCarQuatY[286] = -0.0057
//	TrafficCarQuatZ[286] = -0.3628
//	TrafficCarQuatW[286] = 0.9319
//	TrafficCarRecording[286] = 287
//	TrafficCarStartime[286] = 132924.0000
//	TrafficCarModel[286] = emperor

	TrafficCarPos[287] = <<-1526.0546, 4986.1719, 62.3233>>
	TrafficCarQuatX[287] = 0.0010
	TrafficCarQuatY[287] = -0.0075
	TrafficCarQuatZ[287] = 0.9346
	TrafficCarQuatW[287] = 0.3556
	TrafficCarRecording[287] = 288
	TrafficCarStartime[287] = 133320.0000
	TrafficCarModel[287] = journey

//	TrafficCarPos[288] = <<-1522.6311, 4973.4961, 62.0929>>
//	TrafficCarQuatX[288] = 0.0056
//	TrafficCarQuatY[288] = -0.0066
//	TrafficCarQuatZ[288] = -0.3607
//	TrafficCarQuatW[288] = 0.9326
//	TrafficCarRecording[288] = 289
//	TrafficCarStartime[288] = 133452.0000
//	TrafficCarModel[288] = emperor

	TrafficCarPos[289] = <<-1522.0673, 4982.6128, 61.9106>>
	TrafficCarQuatX[289] = 0.0017
	TrafficCarQuatY[289] = -0.0088
	TrafficCarQuatZ[289] = 0.9479
	TrafficCarQuatW[289] = 0.3184
	TrafficCarRecording[289] = 290
	TrafficCarStartime[289] = 133716.0000
	TrafficCarModel[289] = emperor

//	TrafficCarPos[290] = <<-1523.0508, 4973.2485, 62.6510>>
//	TrafficCarQuatX[290] = 0.0057
//	TrafficCarQuatY[290] = -0.0070
//	TrafficCarQuatZ[290] = -0.3752
//	TrafficCarQuatW[290] = 0.9269
//	TrafficCarRecording[290] = 291
//	TrafficCarStartime[290] = 133848.0000
//	TrafficCarModel[290] = Mule

//	TrafficCarPos[291] = <<-1552.1281, 4940.7471, 61.3977>>
//	TrafficCarQuatX[291] = 0.0020
//	TrafficCarQuatY[291] = -0.0083
//	TrafficCarQuatZ[291] = -0.3482
//	TrafficCarQuatW[291] = 0.9374
//	TrafficCarRecording[291] = 292
//	TrafficCarStartime[291] = 134640.0000
//	TrafficCarModel[291] = emperor

	ParkedCarPos[22] = <<-1545.1647, 4929.5005, 61.3231>>
	ParkedCarQuatX[22] = -0.0000
	ParkedCarQuatY[22] = 0.0255
	ParkedCarQuatZ[22] = -0.4983
	ParkedCarQuatW[22] = 0.8666
	ParkedCarModel[22] = minivan

	ParkedCarPos[23] = <<-1533.8358, 4941.9458, 61.2712>>
	ParkedCarQuatX[23] = 0.0113
	ParkedCarQuatY[23] = -0.0309
	ParkedCarQuatZ[23] = 0.9258
	ParkedCarQuatW[23] = -0.3765
	ParkedCarModel[23] = emperor

//	TrafficCarPos[292] = <<-1576.2004, 4923.4380, 60.8458>>
//	TrafficCarQuatX[292] = 0.0026
//	TrafficCarQuatY[292] = -0.0045
//	TrafficCarQuatZ[292] = 0.9049
//	TrafficCarQuatW[292] = 0.4255
//	TrafficCarRecording[292] = 293
//	TrafficCarStartime[292] = 135234.0000
//	TrafficCarModel[292] = emperor

//	TrafficCarPos[293] = <<-1584.5472, 4914.5571, 61.1758>>
//	TrafficCarQuatX[293] = 0.0027
//	TrafficCarQuatY[293] = -0.0050
//	TrafficCarQuatZ[293] = 0.9102
//	TrafficCarQuatW[293] = 0.4142
//	TrafficCarRecording[293] = 294
//	TrafficCarStartime[293] = 135564.0000
//	TrafficCarModel[293] = minivan

	TrafficCarPos[294] = <<-1586.3225, 4902.9268, 61.0933>>
	TrafficCarQuatX[294] = -0.0001
	TrafficCarQuatY[294] = -0.0062
	TrafficCarQuatZ[294] = -0.2908
	TrafficCarQuatW[294] = 0.9568
	TrafficCarRecording[294] = 295
	TrafficCarStartime[294] = 135762.0000
	TrafficCarModel[294] = journey

//	TrafficCarPos[295] = <<-1582.8475, 4924.0020, 61.1598>>
//	TrafficCarQuatX[295] = 0.0032
//	TrafficCarQuatY[295] = -0.0048
//	TrafficCarQuatZ[295] = 0.9398
//	TrafficCarQuatW[295] = 0.3416
//	TrafficCarRecording[295] = 296
//	TrafficCarStartime[295] = 135828.0000
//	TrafficCarModel[295] = emperor

	TrafficCarPos[296] = <<-1594.7332, 4892.0879, 60.9430>>
	TrafficCarQuatX[296] = 0.0014
	TrafficCarQuatY[296] = -0.0052
	TrafficCarQuatZ[296] = -0.3516
	TrafficCarQuatW[296] = 0.9361
	TrafficCarRecording[296] = 297
	TrafficCarStartime[296] = 136158.0000
	TrafficCarModel[296] = emperor

//	TrafficCarPos[297] = <<-1596.9127, 4907.2891, 60.9464>>
//	TrafficCarQuatX[297] = 0.0028
//	TrafficCarQuatY[297] = -0.0044
//	TrafficCarQuatZ[297] = 0.9364
//	TrafficCarQuatW[297] = 0.3509
//	TrafficCarRecording[297] = 298
//	TrafficCarStartime[297] = 136422.0000
//	TrafficCarModel[297] = SURFER

	TrafficCarPos[298] = <<-1623.1863, 4865.9243, 60.3676>>
	TrafficCarQuatX[298] = 0.0022
	TrafficCarQuatY[298] = -0.0013
	TrafficCarQuatZ[298] = -0.3832
	TrafficCarQuatW[298] = 0.9236
	TrafficCarRecording[298] = 299
	TrafficCarStartime[298] = 137082.0000
	TrafficCarModel[298] = emperor

	TrafficCarPos[299] = <<-1624.2852, 4863.7334, 60.7330>>
	TrafficCarQuatX[299] = 0.0023
	TrafficCarQuatY[299] = -0.0010
	TrafficCarQuatZ[299] = -0.4036
	TrafficCarQuatW[299] = 0.9149
	TrafficCarRecording[299] = 300
	TrafficCarStartime[299] = 137412.0000
	TrafficCarModel[299] = journey

//	TrafficCarPos[300] = <<-1654.8507, 4851.4946, 60.6231>>
//	TrafficCarQuatX[300] = 0.0006
//	TrafficCarQuatY[300] = -0.0030
//	TrafficCarQuatZ[300] = 0.9218
//	TrafficCarQuatW[300] = 0.3875
//	TrafficCarRecording[300] = 301
//	TrafficCarStartime[300] = 137808.0000
//	TrafficCarModel[300] = minivan

	TrafficCarPos[301] = <<-1657.7526, 4832.5718, 60.5535>>
	TrafficCarQuatX[301] = 0.0013
	TrafficCarQuatY[301] = -0.0017
	TrafficCarQuatZ[301] = -0.4077
	TrafficCarQuatW[301] = 0.9131
	TrafficCarRecording[301] = 302
	TrafficCarStartime[301] = 138336.0000
	TrafficCarModel[301] = minivan

//	TrafficCarPos[302] = <<-1653.9990, 4853.1523, 60.5032>>
//	TrafficCarQuatX[302] = -0.0003
//	TrafficCarQuatY[302] = -0.0027
//	TrafficCarQuatZ[302] = 0.9110
//	TrafficCarQuatW[302] = 0.4124
//	TrafficCarRecording[302] = 303
//	TrafficCarStartime[302] = 139260.0000
//	TrafficCarModel[302] = emperor

//	TrafficCarPos[303] = <<-1709.2064, 4806.2900, 59.3238>>
//	TrafficCarQuatX[303] = -0.0066
//	TrafficCarQuatY[303] = -0.0229
//	TrafficCarQuatZ[303] = 0.8920
//	TrafficCarQuatW[303] = 0.4514
//	TrafficCarRecording[303] = 304
//	TrafficCarStartime[303] = 139722.0000
//	TrafficCarModel[303] = emperor

	TrafficCarPos[304] = <<-1673.6324, 4818.7017, 60.5406>>
	TrafficCarQuatX[304] = 0.0021
	TrafficCarQuatY[304] = -0.0057
	TrafficCarQuatZ[304] = -0.4375
	TrafficCarQuatW[304] = 0.8992
	TrafficCarRecording[304] = 305
	TrafficCarStartime[304] = 139986.0000
	TrafficCarModel[304] = minivan

	TrafficCarPos[305] = <<-1719.6976, 4782.9854, 57.8986>>
	TrafficCarQuatX[305] = 0.0181
	TrafficCarQuatY[305] = -0.0128
	TrafficCarQuatZ[305] = -0.4471
	TrafficCarQuatW[305] = 0.8942
	TrafficCarRecording[305] = 306
	TrafficCarStartime[305] = 140184.0000
	TrafficCarModel[305] = emperor

	TrafficCarPos[306] = <<-1729.2485, 4775.8354, 57.8030>>
	TrafficCarQuatX[306] = 0.0131
	TrafficCarQuatY[306] = -0.0119
	TrafficCarQuatZ[306] = -0.4648
	TrafficCarQuatW[306] = 0.8853
	TrafficCarRecording[306] = 307
	TrafficCarStartime[306] = 140778.0000
	TrafficCarModel[306] = minivan

//	TrafficCarPos[307] = <<-1761.1912, 4763.4185, 56.9793>>
//	TrafficCarQuatX[307] = 0.0005
//	TrafficCarQuatY[307] = -0.0087
//	TrafficCarQuatZ[307] = 0.9178
//	TrafficCarQuatW[307] = 0.3969
//	TrafficCarRecording[307] = 308
//	TrafficCarStartime[307] = 141042.0000
//	TrafficCarModel[307] = journey

	TrafficCarPos[308] = <<-1761.8721, 4747.9673, 56.8266>>
	TrafficCarQuatX[308] = -0.0002
	TrafficCarQuatY[308] = -0.0081
	TrafficCarQuatZ[308] = -0.4103
	TrafficCarQuatW[308] = 0.9119
	TrafficCarRecording[308] = 309
	TrafficCarStartime[308] = 141306.0000
	TrafficCarModel[308] = emperor

	TrafficCarPos[309] = <<-1771.1117, 4740.0356, 56.8407>>
	TrafficCarQuatX[309] = -0.0029
	TrafficCarQuatY[309] = -0.0035
	TrafficCarQuatZ[309] = -0.4186
	TrafficCarQuatW[309] = 0.9081
	TrafficCarRecording[309] = 310
	TrafficCarStartime[309] = 141570.0000
	TrafficCarModel[309] = minivan

	TrafficCarPos[310] = <<-1771.1819, 4739.9648, 56.7243>>
	TrafficCarQuatX[310] = -0.0029
	TrafficCarQuatY[310] = -0.0037
	TrafficCarQuatZ[310] = -0.4187
	TrafficCarQuatW[310] = 0.9081
	TrafficCarRecording[310] = 311
	TrafficCarStartime[310] = 141834.0000
	TrafficCarModel[310] = emperor

//	TrafficCarPos[311] = <<-1789.5768, 4735.5669, 56.4492>>
//	TrafficCarQuatX[311] = 0.0040
//	TrafficCarQuatY[311] = -0.0013
//	TrafficCarQuatZ[311] = 0.9312
//	TrafficCarQuatW[311] = 0.3644
//	TrafficCarRecording[311] = 312
//	TrafficCarStartime[311] = 141966.0000
//	TrafficCarModel[311] = emperor

//	TrafficCarPos[312] = <<-1802.6720, 4708.2441, 56.8245>>
//	TrafficCarQuatX[312] = -0.0020
//	TrafficCarQuatY[312] = -0.0039
//	TrafficCarQuatZ[312] = -0.3900
//	TrafficCarQuatW[312] = 0.9208
//	TrafficCarRecording[312] = 313
//	TrafficCarStartime[312] = 142626.0000
//	TrafficCarModel[312] = journey

	TrafficCarPos[313] = <<-1815.7078, 4695.4819, 56.7461>>
	TrafficCarQuatX[313] = -0.0019
	TrafficCarQuatY[313] = -0.0041
	TrafficCarQuatZ[313] = -0.3735
	TrafficCarQuatW[313] = 0.9276
	TrafficCarRecording[313] = 314
	TrafficCarStartime[313] = 142956.0000
	TrafficCarModel[313] = emperor

//	TrafficCarPos[314] = <<-1823.1193, 4702.2739, 56.8611>>
//	TrafficCarQuatX[314] = 0.0040
//	TrafficCarQuatY[314] = -0.0017
//	TrafficCarQuatZ[314] = 0.9306
//	TrafficCarQuatW[314] = 0.3661
//	TrafficCarRecording[314] = 315
//	TrafficCarStartime[314] = 143022.0000
//	TrafficCarModel[314] = minivan

//	TrafficCarPos[315] = <<-1824.1984, 4686.8857, 56.4565>>
//	TrafficCarQuatX[315] = -0.0016
//	TrafficCarQuatY[315] = -0.0040
//	TrafficCarQuatZ[315] = -0.3850
//	TrafficCarQuatW[315] = 0.9229
//	TrafficCarRecording[315] = 316
//	TrafficCarStartime[315] = 143220.0000
//	TrafficCarModel[315] = emperor

//	TrafficCarPos[316] = <<-1844.0956, 4665.9521, 56.7460>>
//	TrafficCarQuatX[316] = -0.0019
//	TrafficCarQuatY[316] = -0.0038
//	TrafficCarQuatZ[316] = -0.4051
//	TrafficCarQuatW[316] = 0.9143
//	TrafficCarRecording[316] = 317
//	TrafficCarStartime[316] = 143880.0000
//	TrafficCarModel[316] = emperor

//	TrafficCarPos[317] = <<-1856.3444, 4668.7070, 56.8597>>
//	TrafficCarQuatX[317] = 0.0039
//	TrafficCarQuatY[317] = -0.0015
//	TrafficCarQuatZ[317] = 0.9290
//	TrafficCarQuatW[317] = 0.3701
//	TrafficCarRecording[317] = 318
//	TrafficCarStartime[317] = 144078.0000
//	TrafficCarModel[317] = minivan

	TrafficCarPos[318] = <<-1862.8253, 4649.1343, 56.8577>>
	TrafficCarQuatX[318] = -0.0019
	TrafficCarQuatY[318] = -0.0041
	TrafficCarQuatZ[318] = -0.3536
	TrafficCarQuatW[318] = 0.9354
	TrafficCarRecording[318] = 319
	TrafficCarStartime[318] = 144540.0000
	TrafficCarModel[318] = minivan

	TrafficCarPos[319] = <<-1864.9409, 4646.2070, 56.5935>>
	TrafficCarQuatX[319] = -0.0015
	TrafficCarQuatY[319] = -0.0043
	TrafficCarQuatZ[319] = -0.3406
	TrafficCarQuatW[319] = 0.9402
	TrafficCarRecording[319] = 320
	TrafficCarStartime[319] = 144738.0000
	TrafficCarModel[319] = minivan

//	TrafficCarPos[320] = <<-1884.5511, 4641.9819, 56.7505>>
//	TrafficCarQuatX[320] = 0.0039
//	TrafficCarQuatY[320] = -0.0016
//	TrafficCarQuatZ[320] = 0.9200
//	TrafficCarQuatW[320] = 0.3919
//	TrafficCarRecording[320] = 321
//	TrafficCarStartime[320] = 145266.0000
//	TrafficCarModel[320] = emperor

//	TrafficCarPos[321] = <<-1892.1720, 4618.9746, 56.8667>>
//	TrafficCarQuatX[321] = -0.0023
//	TrafficCarQuatY[321] = -0.0038
//	TrafficCarQuatZ[321] = -0.4186
//	TrafficCarQuatW[321] = 0.9082
//	TrafficCarRecording[321] = 322
//	TrafficCarStartime[321] = 145530.0000
//	TrafficCarModel[321] = minivan

//	TrafficCarPos[322] = <<-1899.8138, 4611.4746, 56.5926>>
//	TrafficCarQuatX[322] = -0.0025
//	TrafficCarQuatY[322] = -0.0037
//	TrafficCarQuatZ[322] = -0.3437
//	TrafficCarQuatW[322] = 0.9391
//	TrafficCarRecording[322] = 323
//	TrafficCarStartime[322] = 145794.0000
//	TrafficCarModel[322] = minivan

	TrafficCarPos[323] = <<-1909.1554, 4602.0088, 56.8270>>
	TrafficCarQuatX[323] = -0.0017
	TrafficCarQuatY[323] = -0.0009
	TrafficCarQuatZ[323] = -0.3819
	TrafficCarQuatW[323] = 0.9242
	TrafficCarRecording[323] = 324
	TrafficCarStartime[323] = 146190.0000
	TrafficCarModel[323] = journey

//	TrafficCarPos[324] = <<-1927.3397, 4598.3667, 56.7548>>
//	TrafficCarQuatX[324] = 0.0034
//	TrafficCarQuatY[324] = -0.0024
//	TrafficCarQuatZ[324] = 0.9210
//	TrafficCarQuatW[324] = 0.3896
//	TrafficCarRecording[324] = 325
//	TrafficCarStartime[324] = 146520.0000
//	TrafficCarModel[324] = emperor

//	TrafficCarPos[325] = <<-1922.5496, 4589.5356, 56.8579>>
//	TrafficCarQuatX[325] = -0.0018
//	TrafficCarQuatY[325] = -0.0040
//	TrafficCarQuatZ[325] = -0.3583
//	TrafficCarQuatW[325] = 0.9336
//	TrafficCarRecording[325] = 326
//	TrafficCarStartime[325] = 146916.0000
//	TrafficCarModel[325] = minivan

//	TrafficCarPos[326] = <<-1950.9838, 4561.3110, 56.7415>>
//	TrafficCarQuatX[326] = -0.0016
//	TrafficCarQuatY[326] = -0.0040
//	TrafficCarQuatZ[326] = -0.3763
//	TrafficCarQuatW[326] = 0.9265
//	TrafficCarRecording[326] = 327
//	TrafficCarStartime[326] = 147774.0000
//	TrafficCarModel[326] = emperor

//	TrafficCarPos[327] = <<-1971.0903, 4554.0884, 56.7414>>
//	TrafficCarQuatX[327] = 0.0040
//	TrafficCarQuatY[327] = -0.0015
//	TrafficCarQuatZ[327] = 0.9250
//	TrafficCarQuatW[327] = 0.3800
//	TrafficCarRecording[327] = 328
//	TrafficCarStartime[327] = 148368.0000
//	TrafficCarModel[327] = emperor

//	TrafficCarPos[328] = <<-1967.7822, 4543.7969, 56.8219>>
//	TrafficCarQuatX[328] = -0.0020
//	TrafficCarQuatY[328] = -0.0036
//	TrafficCarQuatZ[328] = -0.3683
//	TrafficCarQuatW[328] = 0.9297
//	TrafficCarRecording[328] = 329
//	TrafficCarStartime[328] = 148500.0000
//	TrafficCarModel[328] = journey

	TrafficCarPos[329] = <<-1984.5195, 4527.2192, 56.8599>>
	TrafficCarQuatX[329] = -0.0017
	TrafficCarQuatY[329] = -0.0037
	TrafficCarQuatZ[329] = -0.3489
	TrafficCarQuatW[329] = 0.9372
	TrafficCarRecording[329] = 330
	TrafficCarStartime[329] = 149226.0000
	TrafficCarModel[329] = minivan

//	TrafficCarPos[330] = <<-1987.1052, 4524.0400, 56.5935>>
//	TrafficCarQuatX[330] = -0.0016
//	TrafficCarQuatY[330] = -0.0043
//	TrafficCarQuatZ[330] = -0.3461
//	TrafficCarQuatW[330] = 0.9382
//	TrafficCarRecording[330] = 331
//	TrafficCarStartime[330] = 149490.0000
//	TrafficCarModel[330] = minivan

//	TrafficCarPos[331] = <<-2001.7291, 4524.9360, 56.7503>>
//	TrafficCarQuatX[331] = 0.0040
//	TrafficCarQuatY[331] = -0.0016
//	TrafficCarQuatZ[331] = 0.9212
//	TrafficCarQuatW[331] = 0.3890
//	TrafficCarRecording[331] = 332
//	TrafficCarStartime[331] = 149688.0000
//	TrafficCarModel[331] = emperor

//	TrafficCarPos[332] = <<-1994.0558, 4517.0898, 56.8285>>
//	TrafficCarQuatX[332] = -0.0023
//	TrafficCarQuatY[332] = -0.0040
//	TrafficCarQuatZ[332] = -0.3662
//	TrafficCarQuatW[332] = 0.9305
//	TrafficCarRecording[332] = 333
//	TrafficCarStartime[332] = 149886.0000
//	TrafficCarModel[332] = journey

	TrafficCarPos[333] = <<-2026.8411, 4484.1152, 56.8037>>
	TrafficCarQuatX[333] = -0.0035
	TrafficCarQuatY[333] = -0.0029
	TrafficCarQuatZ[333] = -0.4273
	TrafficCarQuatW[333] = 0.9041
	TrafficCarRecording[333] = 334
	TrafficCarStartime[333] = 151800.0000
	TrafficCarModel[333] = minivan

//	TrafficCarPos[334] = <<-2038.8058, 4475.1045, 57.0356>>
//	TrafficCarQuatX[334] = -0.0109
//	TrafficCarQuatY[334] = 0.0029
//	TrafficCarQuatZ[334] = -0.3906
//	TrafficCarQuatW[334] = 0.9205
//	TrafficCarRecording[334] = 335
//	TrafficCarStartime[334] = 153318.0000
//	TrafficCarModel[334] = minivan

//	TrafficCarPos[335] = <<-2037.0986, 4476.1309, 56.9395>>
//	TrafficCarQuatX[335] = -0.0116
//	TrafficCarQuatY[335] = 0.0027
//	TrafficCarQuatZ[335] = -0.4029
//	TrafficCarQuatW[335] = 0.9152
//	TrafficCarRecording[335] = 336
//	TrafficCarStartime[335] = 155166.0000
//	TrafficCarModel[335] = journey

// ****  UBER RECORDED SET PIECE CARS  ****

//	SetPieceCarPos[0] = <<2641.4314, 5104.0015, 44.1941>>
//	SetPieceCarQuatX[0] = 0.0034
//	SetPieceCarQuatY[0] = -0.0012
//	SetPieceCarQuatZ[0] = 0.1110
//	SetPieceCarQuatW[0] = 0.9938
//	SetPieceCarRecording[0] = 400
//	SetPieceCarStartime[0] = 0.0000
//	SetPieceCarRecordingSpeed[0] = 1.0000
//	SetPieceCarModel[0] = entityxf
//
//	SetPieceCarPos[1] = <<2651.3657, 5090.0205, 44.1038>>
//	SetPieceCarQuatX[1] = -0.0000
//	SetPieceCarQuatY[1] = 0.0000
//	SetPieceCarQuatZ[1] = 0.1193
//	SetPieceCarQuatW[1] = 0.9929
//	SetPieceCarRecording[1] = 401
//	SetPieceCarStartime[1] = 0.0000
//	SetPieceCarRecordingSpeed[1] = 1.0000
//	SetPieceCarModel[1] = cheetah
//	
//	SetPieceCarPos[2] = <<2650.3530, 5066.5088, 44.3731>>
//	SetPieceCarQuatX[2] = 0.0065
//	SetPieceCarQuatY[2] = -0.0003
//	SetPieceCarQuatZ[2] = 0.1173
//	SetPieceCarQuatW[2] = 0.9931
//	SetPieceCarRecording[2] = 402
//	SetPieceCarStartime[2] = 0.0000
//	SetPieceCarRecordingSpeed[2] = 1.0000
//	SetPieceCarModel[2] = f620

	SetPieceCarPos[3] = <<-526.2304, 5788.8130, 34.7703>>
	SetPieceCarQuatX[3] = 0.0075
	SetPieceCarQuatY[3] = 0.0489
	SetPieceCarQuatZ[3] = 0.9749
	SetPieceCarQuatW[3] = 0.2171
	SetPieceCarRecording[3] = 403
	SetPieceCarStartime[3] = 102000.000
	SetPieceCarRecordingSpeed[3] = 1.0000
	SetPieceCarModel[3] = hexer

	SetPieceCarPos[4] = <<-523.5488, 5790.6362, 34.7089>>
	SetPieceCarQuatX[4] = 0.0081
	SetPieceCarQuatY[4] = 0.0484
	SetPieceCarQuatZ[4] = 0.9763
	SetPieceCarQuatW[4] = 0.2109
	SetPieceCarRecording[4] = 404
	SetPieceCarStartime[4] = 102000.000
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = hexer

	SetPieceCarPos[5] = <<-527.1961, 5790.4072, 34.7552>>
	SetPieceCarQuatX[5] = 0.0115
	SetPieceCarQuatY[5] = 0.0382
	SetPieceCarQuatZ[5] = 0.9777
	SetPieceCarQuatW[5] = 0.2060
	SetPieceCarRecording[5] = 405
	SetPieceCarStartime[5] = 102000.000
	SetPieceCarRecordingSpeed[5] = 1.0000
	SetPieceCarModel[5] = hexer
ENDPROC

//═══════════════════════════╡ OBJECTIVE PROCEDURES ╞════════════════════════════

PROC initialiseMission()
	IF INIT_STAGE()
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		SET_INTERIOR_CAPPED(INTERIOR_V_CHOPSHOP, FALSE)
		
		//INFORM_MISSION_STATS_OF_MISSION_START_CAR_STEAL_THREE()
		
		//Request Additional Text
		REQUEST_ADDITIONAL_TEXT("CRSTL3", MISSION_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
		
		//Audio
		REGISTER_SCRIPT_WITH_AUDIO()
		
		//Ped
		SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_HWAYCOP_01, TRUE)
		
		//Vehicle
		SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICEB, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(F620, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(ENTITYXF, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(CHEETAH, TRUE)
		
		//Set Player
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
			bHasChanged = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(GET_CURRENT_PLAYER_PED_ENUM())  //ensure bChange is set to the state of “player has changed outfit” at the start of the mission
			
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
				WAIT(0)
			ENDWHILE
		ENDIF
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		
		//Widgets
		#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widGroup)
				widGroup = START_WIDGET_GROUP("Car Steal 3")
					START_WIDGET_GROUP("Debug Print")
						ADD_WIDGET_BOOL("Audio", bDebugAudio)
					STOP_WIDGET_GROUP()
					
//					START_WIDGET_GROUP("Cinematic Camera Capture")
//						ADD_WIDGET_INT_SLIDER("Camera Index", iCinCamID, 0, iCinCams - 1, 1)
//						
//						ADD_WIDGET_BOOL("Capture Debug Camera as Node", bCinCamSave)
//						
//						ADD_WIDGET_BOOL("Link Tangent to Next Node", bCinCamLink)
//						
//						ADD_WIDGET_STRING("")
//						
//						//ADD_WIDGET_VECTOR_SLIDER("Camera Position", vCinCamPos, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Position X", vCinCamPos.X, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Position Y", vCinCamPos.Y, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Position Z", vCinCamPos.Z, -10000.0, 10000.0, 0.1)
//						
//						ADD_WIDGET_STRING("")
//						
//						//ADD_WIDGET_VECTOR_SLIDER("Camera Rotation", vCinCamRot, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Rotation X", vCinCamRot.X, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Rotation Y", vCinCamRot.Y, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Rotation Z", vCinCamRot.Z, -10000.0, 10000.0, 0.1)
//						
//						ADD_WIDGET_STRING("")
//						
//						ADD_WIDGET_FLOAT_SLIDER("Camera FOV", fCinCamFov, 0.0, 130.0, 0.1)
//						
//						ADD_WIDGET_STRING("")
//						
//						//ADD_WIDGET_VECTOR_SLIDER("Camera Tangent In", vCinCamTanIn, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Tangent In X", vCinCamTanIn.X, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Tangent In Y", vCinCamTanIn.Y, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Tangent In Z", vCinCamTanIn.Z, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Magnitude", fCinCamTanInMag, 0.0, 100000.0, 1.0)
//						
//						ADD_WIDGET_STRING("")
//						
//						//ADD_WIDGET_VECTOR_SLIDER("Camera Tangent Out", vCinCamTanOut, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Tangent Out X", vCinCamTanOut.X, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Tangent Out Y", vCinCamTanOut.Y, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Camera Tangent Out Z", vCinCamTanOut.Z, -10000.0, 10000.0, 0.1)
//						ADD_WIDGET_FLOAT_SLIDER("Magnitude", fCinCamTanOutMag, 0.0, 100000.0, 1.0)
//											
//						ADD_WIDGET_STRING("")
//						
//						ADD_WIDGET_FLOAT_SLIDER("Camera Recording Time", fCinCamRec, 0.0, 100000.0, 1.0)
//						
//						ADD_WIDGET_STRING("")
//						
//						ADD_WIDGET_BOOL("Display Debug Lines", bCinCamDebug)
//						
//						ADD_WIDGET_BOOL("Output Camera Data", bCinCamOutput)
//				    STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				
				SET_UBER_PARENT_WIDGET_GROUP(widGroup)
				
				SET_LOCATES_HEADER_WIDGET_GROUP(widGroup)
			ENDIF
		#ENDIF
		
		//Relationships
		ADD_RELATIONSHIP_GROUP("BUDDIES", relGroupBuddy)
		ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemy)
		
		//Buddy
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBuddy, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupBuddy)
		
		//Spawn Car
		IF NOT DOES_ENTITY_EXIST(vehPlayer)
			SPAWN_VEHICLE(vehPlayer, F620, vPlayerStart, fPlayerStart, 0)
			SET_VEHICLE_HAS_STRONG_AXLES(vehPlayer, TRUE)
			SET_VEHICLE_COLOURS(vehPlayer, 43, 43)
			SET_VEHICLE_EXTRA_COLOURS(vehPlayer, 48, 48)
		ENDIF
//		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF DOES_ENTITY_EXIST(vehPlayer)
//					IF NOT IS_ENTITY_DEAD(vehPlayer)
//						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(F620)
		
		//Dialogue
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, PLAYER_PED_ID(), "FRANKLIN")
		
		// Fix possible bad weather
		IF Is_Replay_In_Progress()
			// On replay - set weather now
			SET_WEATHER_TYPE_PERSIST("EXTRASUNNY")
		ELSE
			// Change to sunny quickly
			SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 60.0)
		ENDIF
		
		IF SKIPPED_STAGE()
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		ADVANCE_STAGE()
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Your mission is being replayed
		IF IS_REPLAY_IN_PROGRESS()
			IF g_bShitskipAccepted = TRUE
	            MISSION_REPLAY(INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE() + 1))
            ELSE
            	MISSION_REPLAY(INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE()))
            ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayStart), "initMission")
	ENDIF
ENDPROC

PROC CutsceneIntro()
	IF INIT_STAGE()
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Radar
		bRadar = FALSE
		
		//Cutscene
		IF bReplaySkip = FALSE
		AND bPreloaded = FALSE
			REQUEST_CUTSCENE("CAR_1_INT_CONCAT", CUTSCENE_REQUESTED_FROM_Z_SKIP)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			IF NOT DOES_ENTITY_EXIST(vehDevin)
				REQUEST_MODEL(ADDER)
				
				WHILE NOT HAS_MODEL_LOADED(ADDER)
					WAIT(0)
				ENDWHILE
				
				SPAWN_VEHICLE(vehDevin, ADDER, <<119.2013, -398.4865, 40.1208>>, -171.4098)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehDevin, "vehDevin")	#ENDIF
				
				SET_VEHICLE_COLOURS(vehDevin, 0, 0)
				SET_VEHICLE_EXTRA_COLOURS(vehDevin, 0, 0)
				SET_VEHICLE_DOORS_LOCKED(vehDevin, VEHICLELOCK_CANNOT_ENTER)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehDevin, FALSE)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedDevin)
				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_DEVIN))
				
				WHILE NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_DEVIN))
					WAIT(0)
				ENDWHILE
				
				WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedDevin, CHAR_DEVIN, vehDevin)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_TORSO, 2, 0)
				SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_LEG, 2, 0)
				SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_DECL, 1, 0)
				
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDevin)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SET_PED_RELATIONSHIP_GROUP_HASH(pedDevin, relGroupBuddy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDevin, TRUE)
				TASK_VEHICLE_DRIVE_WANDER(pedDevin, vehDevin, 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				SET_PED_KEEP_TASK(pedDevin, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DEVIN))
			ENDIF
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
		ENDIF
	ELSE
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
		
		IF bReplaySkip = FALSE
		AND bPreloaded = FALSE
			//Request Cutscene Variations - CAR_1_INT_CONCAT
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED(CHAR_TREVOR))
				ENDIF
				SET_CUTSCENE_PED_COMPONENT_VARIATION("DEVIN", PED_COMP_TORSO, 2, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("DEVIN", PED_COMP_LEG, 2, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("DEVIN", PED_COMP_DECL, 1, 0)
			ENDIF
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF HAS_THIS_CUTSCENE_LOADED("CAR_1_INT_CONCAT")
				OR bReplaySkip = TRUE
				OR bPreloaded = TRUE
					IF bReplaySkip = FALSE
					AND bPreloaded = FALSE
					
						THEFEED_FLUSH_QUEUE()
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						IF DOES_ENTITY_EXIST(vehPlayer)
						AND NOT IS_ENTITY_DEAD(vehPlayer)
							REGISTER_ENTITY_FOR_CUTSCENE(vehPlayer, "Franklins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedDevin)
						AND NOT IS_PED_INJURED(pedDevin)
							REGISTER_ENTITY_FOR_CUTSCENE(pedDevin, "DEVIN", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_DEVIN))
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "DEVIN", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_DEVIN))
						ENDIF
						
						IF DOES_ENTITY_EXIST(vehDevin)
						AND NOT IS_ENTITY_DEAD(vehDevin)
							REGISTER_ENTITY_FOR_CUTSCENE(vehDevin, "Devins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, ADDER)
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Devins_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, ADDER)
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedMolly)
						AND NOT IS_ENTITY_DEAD(pedMolly)
							REGISTER_ENTITY_FOR_CUTSCENE(pedMolly, "MOLLY", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF DOES_ENTITY_EXIST(vehCutscene)
						AND NOT IS_ENTITY_DEAD(vehCutscene)
							REGISTER_ENTITY_FOR_CUTSCENE(vehCutscene, "Car_1_Felon", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						AND NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						WAIT_WITH_DEATH_CHECKS(0)
						
						DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
						DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_TREVOR)
						
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<110.811684, -421.141571, 36.196766>>, <<126.111618, -380.102112, 57.261459>>, 40.0, <<120.5374, -429.8324, 40.0967>>, 289.2337, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
						
						CLEAR_AREA(<<115.56944, -400.46335, 40.26558>>, 20.0, TRUE)
						
						DISABLE_PROCOBJ_CREATION()
						
						STOP_GAMEPLAY_HINT(TRUE)
						
						CLEAR_TEXT()
					ENDIF
					
					//Fade In
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF NOT DOES_ENTITY_EXIST(pedDevin)
					ENTITY_INDEX entityDevin
					entityDevin = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("DEVIN")
					
					IF DOES_ENTITY_EXIST(entityDevin)
						pedDevin = GET_PED_INDEX_FROM_ENTITY_INDEX(entityDevin)
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedDevin, relGroupBuddy)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDevin, TRUE)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehDevin)
					IF NOT DOES_ENTITY_EXIST(vehDevin)
						ENTITY_INDEX entityDevin
						entityDevin = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Devins_car")
						
						IF DOES_ENTITY_EXIST(entityDevin)
							vehDevin = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityDevin)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehDevin, "vehDevin")	#ENDIF
							
							SET_VEHICLE_COLOURS(vehDevin, 0, 0)
							SET_VEHICLE_EXTRA_COLOURS(vehDevin, 0, 0)
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					
					REPLAY_STOP_EVENT()
					
					//TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 172.2073)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklins_car")
					
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("DEVIN")
					IF DOES_ENTITY_EXIST(vehDevin)
					AND NOT IS_ENTITY_DEAD(vehDevin)
						IF DOES_ENTITY_EXIST(vehDevin)
						AND NOT IS_PED_INJURED(pedDevin)
							SET_PED_INTO_VEHICLE(pedDevin, vehDevin, VS_DRIVER)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedDevin, relGroupBuddy)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDevin, TRUE)
							TASK_VEHICLE_DRIVE_WANDER(pedDevin, vehDevin, 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
							SET_PED_KEEP_TASK(pedDevin, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MOLLY")
					
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Devins_car")
					SET_VEHICLE_DOORS_LOCKED(vehDevin, VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehDevin, FALSE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
					
					ENABLE_PROCOBJ_CREATION()
				ENDIF
				
				IF WAS_CUTSCENE_SKIPPED()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
				ENDIF
				
				IF bReplaySkip = TRUE
				OR HAS_CUTSCENE_FINISHED()
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					IF NOT DOES_ENTITY_EXIST(vehDevin)
						REQUEST_MODEL(ADDER)
						
						WHILE NOT HAS_MODEL_LOADED(ADDER)
							WAIT(0)
						ENDWHILE
						
						SPAWN_VEHICLE(vehDevin, ADDER, <<119.2013, -398.4865, 40.1208>>, -171.4098)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehDevin, "vehDevin")	#ENDIF
						
						SET_VEHICLE_COLOURS(vehDevin, 0, 0)
						SET_VEHICLE_EXTRA_COLOURS(vehDevin, 0, 0)
						SET_VEHICLE_DOORS_LOCKED(vehDevin, VEHICLELOCK_CANNOT_ENTER)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehDevin, FALSE)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(pedDevin)
						REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_DEVIN))
						
						WHILE NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_DEVIN))
							WAIT(0)
						ENDWHILE
						
						WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedDevin, CHAR_DEVIN, vehDevin)
							WAIT_WITH_DEATH_CHECKS(0)
						ENDWHILE
						
						SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_TORSO, 2, 0)
						SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_LEG, 2, 0)
						SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_DECL, 1, 0)
						
						WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDevin)
							WAIT_WITH_DEATH_CHECKS(0)
						ENDWHILE
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedDevin, relGroupBuddy)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDevin, TRUE)
						TASK_VEHICLE_DRIVE_WANDER(pedDevin, vehDevin, 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
						SET_PED_KEEP_TASK(pedDevin, TRUE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DEVIN))
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC Arrive()
	IF INIT_STAGE()
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Outfits/Weapons
		SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Set radar	
		bRadar = TRUE
		
		//Blips
		//SAFE_ADD_BLIP_LOCATION(blipStart, vTown, TRUE)
		
		ADD_SCENARIO_BLOCKING_AREA(<<2581.105713, 361.805481, 116.468567>> - <<25.0, 25.0, 10.0>>, <<2581.105713, 361.805481, 116.468567>> + <<25.0, 25.0, 10.0>>)
		
		SET_ROADS_IN_AREA(<<2581.105713, 361.805481, 116.468567>> - <<25.0, 25.0, 10.0>>, <<2581.105713, 361.805481, 116.468567>> + <<25.0, 25.0, 10.0>>, FALSE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED_ID(), <<117.2437, -405.8348, 40.2589>>, 172.2073, FALSE)
			
			IF NOT DOES_ENTITY_EXIST(vehDevin)
				REQUEST_MODEL(ADDER)
				
				WHILE NOT HAS_MODEL_LOADED(ADDER)
					WAIT(0)
				ENDWHILE
				
				SPAWN_VEHICLE(vehDevin, ADDER, <<119.2013, -398.4865, 40.1208>>, -171.4098)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehDevin, "vehDevin")	#ENDIF
				
				SET_VEHICLE_COLOURS(vehDevin, 0, 0)
				SET_VEHICLE_EXTRA_COLOURS(vehDevin, 0, 0)
				SET_VEHICLE_DOORS_LOCKED(vehDevin, VEHICLELOCK_CANNOT_ENTER)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehDevin, FALSE)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedDevin)
				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_DEVIN))
				
				WHILE NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_DEVIN))
					WAIT(0)
				ENDWHILE
				
				WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedDevin, CHAR_DEVIN, vehDevin)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_TORSO, 2, 0)
				SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_LEG, 2, 0)
				SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_DECL, 1, 0)
				
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDevin)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SET_PED_RELATIONSHIP_GROUP_HASH(pedDevin, relGroupBuddy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDevin, TRUE)
				TASK_VEHICLE_DRIVE_WANDER(pedDevin, vehDevin, 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				SET_PED_KEEP_TASK(pedDevin, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DEVIN))
			ENDIF
			
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			WAIT_WITH_DEATH_CHECKS(10)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID)
			ADD_DOOR_TO_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].model, g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].coords)
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID)
			IF DOOR_SYSTEM_GET_OPEN_RATIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID) <> 0.0
			OR DOOR_SYSTEM_GET_DOOR_STATE(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
				DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
		
		SET_DOOR_STATE(DOORNAME_CARSTEAL_GARAGE_S, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
		
		SWITCH iCutsceneStage
			CASE 0
				//Audio Scene
				IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_DEVIN_DRIVES_AWAY")
					START_AUDIO_SCENE("CAR_1_DEVIN_DRIVES_AWAY")
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehDevin)
				AND NOT IS_ENTITY_DEAD(vehDevin)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR_1_DEVINS_CAR_GROUP")
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehDevin, "CAR_1_DEVINS_CAR_GROUP")
						
						SET_LABEL_AS_TRIGGERED("CAR_1_DEVINS_CAR_GROUP", TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Phone")
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2597.918945, 361.818237, 106.427612>>) < 1500.0
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2597.918945, 361.818237, 106.427612>>) > 200.0
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, NULL, "MICHAEL")
						
						PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_MICHAEL, "CST3AUD", "CST3_Phone", CONV_PRIORITY_HIGH)
						
						SET_LABEL_AS_TRIGGERED("CST3_Phone", TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("LeaveVehicle")
					#IF IS_DEBUG_BUILD	DONT_DO_J_SKIP(sLocatesData)	#ENDIF
					IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, vTown, <<0.0, 0.0, LOCATE_SIZE_HEIGHT>>, TRUE, vehPlayer, "S3_GOTO", "CMN_GENGETIN", "CMN_GENGETBCK", TRUE)
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_GET_TO_RACE")
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
						START_AUDIO_SCENE("CAR_1_GET_TO_RACE")
					ENDIF
				ENDIF
				
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2571.36, 360.45, 108.05>>, <<2.5, 3.0, 3.0>>)
//				AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//					IF CAN_PLAYER_START_CUTSCENE()
//						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//					ENDIF
//				ENDIF
//				
//				IF NOT SAFE_IS_PLAYER_CONTROL_ON()
//				AND GET_ENTITY_SPEED(vehPlayer) < 1.0
//				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2571.36, 360.45, 108.05>>, <<50.0, 50.0, 5.0>>)
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("LeaveVehicle")
//						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//						
//						SET_LABEL_AS_TRIGGERED("LeaveVehicle", TRUE)
//					ENDIF
//					
//					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					AND GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) != vehPlayer
//						ADVANCE_STAGE()
//					ENDIF
//				ENDIF
//				
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("SAFE_ENTRY")
//				AND NOT HAS_LABEL_BEEN_TRIGGERED("NOT_SAFE_ENTRY")
//					IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2571.565186, 363.774963, 112.456970>>, <<2570.273682, 331.501129, 107.452881>>, 20.0)
//					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2571.565186, 363.774963, 112.456970>>, <<2588.722900, 332.148132, 107.191933>>, 20.0)
//					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2571.565186, 363.774963, 112.456970>>, <<2558.238525, 333.626129, 107.205437>>, 15.0))
//					AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//					AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
//						IF CAN_PLAYER_START_CUTSCENE()
//							IF GET_ENTITY_HEADING(PLAYER_PED_ID()) > WRAP(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2581.719727, 336.251709, 113.443291>>) - 90.0, 0.0, 360.0)
//							AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < WRAP(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2581.719727, 336.251709, 113.443291>>) + 90.0, 0.0, 360.0)
//								SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//								
//								TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehPlayer, <<2571.3267, 360.4416, 107.4570>>, 20.0, DRIVINGSTYLE_ACCURATE, F620, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 1.0, 1.0)
//								
//								SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
//								SET_GAMEPLAY_ENTITY_HINT(vehRival1, <<0.0, 0.0, 1.0>>, DEFAULT, -1, DEFAULT_INTERP_IN_TIME * 2)
//								SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.70)
//								SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(-90.0)
//								//SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.030)
//								SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(-0.02)
//								SET_GAMEPLAY_HINT_FOV(30.0)
//								
//								SET_LABEL_AS_TRIGGERED("SAFE_ENTRY", TRUE)
//							ELSE
//								SET_LABEL_AS_TRIGGERED("NOT_SAFE_ENTRY", TRUE)
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("SAFE_ENTRY")
//				AND HAS_LABEL_BEEN_TRIGGERED("NOT_SAFE_ENTRY")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2558.925537, 355.287384, 107.121086>>, <<2598.119873, 353.765930, 121.994392>>, 35.0)
					AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
					AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						IF CAN_PLAYER_START_CUTSCENE()
							IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
								NEW_LOAD_SCENE_START_SPHERE(<<2580.91138, 354.02069, 107.45697>>, 50.0)
							ENDIF
							
							SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
							
							IF DOES_ENTITY_EXIST(pedRival1)
								CLEAR_PED_TASKS_IMMEDIATELY(pedRival1)
								TASK_PLAY_ANIM_ADVANCED(pedRival1, sAnimDictCar3, "racer_argue_action_01", <<2581.546, 358.981, 107.662>> + <<-0.45, 0.0, 0.0>>, <<0.0, 0.0, 90.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_EXIT_AFTER_INTERRUPTED | AF_USE_MOVER_EXTRACTION, 0.7)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedRival1)
							ENDIF
							
							IF DOES_ENTITY_EXIST(pedRival2)
								CLEAR_PED_TASKS_IMMEDIATELY(pedRival2)
								TASK_PLAY_ANIM_ADVANCED(pedRival2, sAnimDictCar3, "racer_argue_action_02", <<2581.546, 358.981, 107.662>>, <<0.0, 0.0, 90.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_EXIT_AFTER_INTERRUPTED | AF_USE_MOVER_EXTRACTION, 0.7)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedRival2)
							ENDIF
							
							REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0)
							

							START_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 001, sCarrecGasStation)
							
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehPlayer)
							
							SET_PLAYBACK_SPEED(vehPlayer, 0.75)
							
//							WAIT_WITH_DEATH_CHECKS(0)
//							
//							IF DOES_ENTITY_EXIST(pedRival1)
//								IF IS_ENTITY_PLAYING_ANIM(pedRival1, sAnimDictCar3, "racer_argue_action_01")
//									//SET_ENTITY_ANIM_CURRENT_TIME(pedRival1, sAnimDictCar3, "racer_argue_action_01", CLAMP((1.0 / GET_ENTITY_ANIM_TOTAL_TIME(pedRival1, sAnimDictCar3, "racer_argue_action_01")) * (GET_ENTITY_ANIM_TOTAL_TIME(pedRival1, sAnimDictCar3, "racer_argue_action_01") - GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGasStation)), 0.0, 1.0))
//									SET_ENTITY_ANIM_CURRENT_TIME(pedRival1, sAnimDictCar3, "racer_argue_action_01", 0.7)
//								ENDIF
//							ENDIF
//							
//							IF DOES_ENTITY_EXIST(pedRival2)
//								IF IS_ENTITY_PLAYING_ANIM(pedRival2, sAnimDictCar3, "racer_argue_action_02")
//									//SET_ENTITY_ANIM_CURRENT_TIME(pedRival2, sAnimDictCar3, "racer_argue_action_02", CLAMP((1.0 / GET_ENTITY_ANIM_TOTAL_TIME(pedRival2, sAnimDictCar3, "racer_argue_action_02")) * (GET_ENTITY_ANIM_TOTAL_TIME(pedRival2, sAnimDictCar3, "racer_argue_action_02") - GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGasStation)), 0.0, 1.0))
//									SET_ENTITY_ANIM_CURRENT_TIME(pedRival2, sAnimDictCar3, "racer_argue_action_02", 0.7)
//								ENDIF
//							ENDIF
							
							IF NOT DOES_CAM_EXIST(camMain)
								camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							ENDIF
							
							SET_CAM_PARAMS(camMain, <<2564.086914, 365.741150, 110.564148>>, <<-5.489840, 0.0, -143.291901>>, 27.323626)
							SET_CAM_PARAMS(camMain, <<2564.047852, 365.790039, 109.911957>>, <<-10.087912, 0.0, -122.954353>>, 27.323626, 3500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
							
							SHAKE_CAM(camMain, "HAND_SHAKE", 0.4)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							CLEAR_PRINTS()
							CLEAR_HELP(TRUE)
							
							CLEAR_AREA(<<2570.5767, 364.8711, 107.4569>>, 1000.0, TRUE)
							
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							
							bRadar = FALSE
							
							SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
							
							SET_LABEL_AS_TRIGGERED("FranklinExitCar", TRUE)
							
							//Audio Scene
							IF IS_AUDIO_SCENE_ACTIVE("CAR_1_GET_TO_RACE")
								STOP_AUDIO_SCENE("CAR_1_GET_TO_RACE")
							ENDIF
							
							IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_PARK_UP_CUTSCENE")
								START_AUDIO_SCENE("CAR_1_PARK_UP_CUTSCENE")
							ENDIF
							
							ADVANCE_CUTSCENE()
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2559.318604, 370.344788, 107.121025>>, <<2598.887451, 368.674652, 122.002274>>, 35.0)
					AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
					AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						IF CAN_PLAYER_START_CUTSCENE()
							IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
								NEW_LOAD_SCENE_START_SPHERE(<<2580.91138, 354.02069, 107.45697>>, 50.0)
							ENDIF
							
							SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
							
							START_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 002, sCarrecGasStation)
							
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehPlayer)
							
							SET_PLAYBACK_SPEED(vehPlayer, 0.75)
							
//							WAIT_WITH_DEATH_CHECKS(0)
							
							IF NOT DOES_CAM_EXIST(camMain)
								camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							ENDIF
							
							SET_CAM_PARAMS(camMain, <<2595.212891, 341.750702, 110.446648>>, <<9.125167, 0.100839, 16.448170>>, 39.313477)
							SET_CAM_PARAMS(camMain, <<2593.473633, 340.792969, 109.082886>>, <<2.509332, 0.100839, 29.033203>>, 39.313477, ROUND(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGasStation)))
							
							SHAKE_CAM(camMain, "HAND_SHAKE", 0.4)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							CLEAR_PRINTS()
							CLEAR_HELP(TRUE)
							
							CLEAR_AREA(<<2570.5767, 364.8711, 107.4569>>, 1000.0, TRUE)
							
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							
							bRadar = FALSE
							
							DISPLAY_RADAR(FALSE)
							DISPLAY_HUD(FALSE)
							
							SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
							
							//Audio Scene
							IF IS_AUDIO_SCENE_ACTIVE("CAR_1_GET_TO_RACE")
								STOP_AUDIO_SCENE("CAR_1_GET_TO_RACE")
							ENDIF
							
							IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_PARK_UP_CUTSCENE")
								START_AUDIO_SCENE("CAR_1_PARK_UP_CUTSCENE")
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_MCS1_LI")	//Fix for bug 1813722
								IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedRival1, "CST3RACER1")
									ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedRival2, "CST3RACER2")
									i_CST3_MCS1_DialogueDelay = 0
									i_CST3_MCS1_AnimStartTime = 0
									
									CREATE_CONVERSATION_ADV("CST3_MCS1_LI", DEFAULT, DEFAULT, DO_NOT_DISPLAY_SUBTITLES)	//CST3_MCS3_LI
								ENDIF
							ENDIF
							
							WAIT_WITH_DEATH_CHECKS(ROUND(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGasStation)))
							
							STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
							
							IF DOES_ENTITY_EXIST(pedRival1)
								CLEAR_PED_TASKS_IMMEDIATELY(pedRival1)
								TASK_PLAY_ANIM_ADVANCED(pedRival1, sAnimDictCar3, "racer_argue_action_01", <<2581.546, 358.981, 107.662>> + <<-0.45, 0.0, 0.0>>, <<0.0, 0.0, 90.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_EXIT_AFTER_INTERRUPTED | AF_USE_MOVER_EXTRACTION)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedRival1)
							ENDIF
							
							IF DOES_ENTITY_EXIST(pedRival2)
								CLEAR_PED_TASKS_IMMEDIATELY(pedRival2)
								TASK_PLAY_ANIM_ADVANCED(pedRival2, sAnimDictCar3, "racer_argue_action_02", <<2581.546, 358.981, 107.662>>, <<0.0, 0.0, 90.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_EXIT_AFTER_INTERRUPTED | AF_USE_MOVER_EXTRACTION)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedRival2)
							ENDIF
							
							START_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 001, sCarrecGasStation)
							
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 1000)
							
							SET_PLAYBACK_SPEED(vehPlayer, 0.75)
							
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehPlayer)
							
							SET_CAM_PARAMS(camMain, <<2564.070313, 365.760864, 110.288490>>, <<-7.603287, -0.187008, -134.635147>>, 27.323626)
							SET_CAM_PARAMS(camMain, <<2564.047852, 365.790039, 109.911957>>, <<-10.087912, 0.0, -122.954353>>, 27.323626, 2500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
							
							WAIT_WITH_DEATH_CHECKS(0)
							
							IF DOES_ENTITY_EXIST(pedRival1)
								IF IS_ENTITY_PLAYING_ANIM(pedRival1, sAnimDictCar3, "racer_argue_action_01")
									//SET_ENTITY_ANIM_CURRENT_TIME(pedRival1, sAnimDictCar3, "racer_argue_action_01", CLAMP((1.0 / GET_ENTITY_ANIM_TOTAL_TIME(pedRival1, sAnimDictCar3, "racer_argue_action_01")) * (GET_ENTITY_ANIM_TOTAL_TIME(pedRival1, sAnimDictCar3, "racer_argue_action_01") - GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGasStation)), 0.0, 1.0))
									SET_ENTITY_ANIM_CURRENT_TIME(pedRival1, sAnimDictCar3, "racer_argue_action_01", 0.7)
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(pedRival2)
								IF IS_ENTITY_PLAYING_ANIM(pedRival2, sAnimDictCar3, "racer_argue_action_02")
									//SET_ENTITY_ANIM_CURRENT_TIME(pedRival2, sAnimDictCar3, "racer_argue_action_02", CLAMP((1.0 / GET_ENTITY_ANIM_TOTAL_TIME(pedRival2, sAnimDictCar3, "racer_argue_action_02")) * (GET_ENTITY_ANIM_TOTAL_TIME(pedRival2, sAnimDictCar3, "racer_argue_action_02") - GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGasStation)), 0.0, 1.0))
									SET_ENTITY_ANIM_CURRENT_TIME(pedRival2, sAnimDictCar3, "racer_argue_action_02", 0.7)
								ENDIF
							ENDIF
							
							SET_LABEL_AS_TRIGGERED("FranklinExitCar", TRUE)
							
							ADVANCE_CUTSCENE()
						ENDIF
					ENDIF
//				ELSE
//					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2571.3267, 360.4416, 107.4570>>, <<3.0, 3.0, 3.0>>)
//						IF IS_VEHICLE_STOPPED(vehPlayer)
//						OR GET_ENTITY_SPEED(vehPlayer) < 1.0
//							SET_LABEL_AS_TRIGGERED("FranklinExitCar", TRUE)
//							
//							ADVANCE_CUTSCENE()
//						ENDIF
//					ENDIF
//				ENDIF
			BREAK
			CASE 1
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				OR (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				AND GET_TIME_POSITION_IN_RECORDING(vehPlayer) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehPlayer)) / 100) * 90)
					IF HAS_LABEL_BEEN_TRIGGERED("FranklinExitCar")
						TASK_LEAVE_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehPlayer, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
						
						WAIT_WITH_DEATH_CHECKS(1000)
					ENDIF
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF DOES_ENTITY_EXIST(pedRival1)
		AND NOT IS_PED_INJURED(pedRival1)
		AND DOES_ENTITY_EXIST(pedRival2)
		AND NOT IS_PED_INJURED(pedRival2)
			PRINTLN("GET_ENTITY_HEADING(PLAYER_PED_ID()) = ", GET_ENTITY_HEADING(PLAYER_PED_ID()))
			PRINTLN("GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2581.719727, 336.251709, 113.443291>>) = ", GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2581.719727, 336.251709, 113.443291>>))
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_MCS1_LI")
				IF IS_ENTITY_PLAYING_ANIM(pedRival1, sAnimDictCar3, "racer_argue_action_01")
				OR IS_ENTITY_PLAYING_ANIM(pedRival2, sAnimDictCar3, "racer_argue_action_02")
					IF GET_GAME_TIMER() - i_CST3_MCS1_AnimStartTime > i_CST3_MCS1_DialogueDelay
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2581.719727, 336.251709, 112.443291>>, <<25.0, 35.0, 8.0>>) AND GET_ENTITY_SPEED(vehPlayer) <= 25.0
							OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2581.719727, 336.251709, 112.443291>>, <<35.0, 60.0, 8.0>>) AND GET_ENTITY_SPEED(vehPlayer) > 25.0)
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedRival1, "CST3RACER1")
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedRival2, "CST3RACER2")
								i_CST3_MCS1_DialogueDelay = 0
								i_CST3_MCS1_AnimStartTime = 0
								
								CREATE_CONVERSATION_ADV("CST3_MCS1_LI", DEFAULT, DEFAULT, DO_NOT_DISPLAY_SUBTITLES)	//CST3_MCS3_LI
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) <> CAM_VIEW_MODE_FIRST_PERSON
				IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2581.719727, 336.251709, 112.443291>>, <<25.0, 35.0, 8.0>>) AND GET_ENTITY_SPEED(vehPlayer) <= 25.0
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2581.719727, 336.251709, 112.443291>>, <<35.0, 60.0, 8.0>>) AND GET_ENTITY_SPEED(vehPlayer) > 25.0)
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > WRAP(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2581.719727, 336.251709, 113.443291>>) - 30.0, 0.0, 360.0)
				AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < WRAP(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2581.719727, 336.251709, 113.443291>>) + 30.0, 0.0, 360.0)
					IF NOT IS_GAMEPLAY_HINT_ACTIVE()
//						SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
						SET_GAMEPLAY_COORD_HINT(GET_ENTITY_COORDS(vehRival1), -1, 1500, DEFAULT, HINTTYPE_NO_FOV)
//						SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.70)
//						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.030)
//						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(-0.02)
//						SET_GAMEPLAY_HINT_FOV(30.0)
					ENDIF
				ELSE
					IF IS_GAMEPLAY_HINT_ACTIVE()
						STOP_GAMEPLAY_HINT(FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedDevin)
		AND NOT IS_PED_INJURED(pedDevin)
		AND DOES_ENTITY_EXIST(vehDevin)
		AND NOT IS_ENTITY_DEAD(vehDevin)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedDevin)) > 200.0
				SET_PED_AS_NO_LONGER_NEEDED(pedDevin)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehDevin)
			ENDIF
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2597.918945, 361.818237, 106.427612>>) < DEFAULT_CUTSCENE_LOAD_DIST
		AND NOT IS_PLAYER_CHANGING_CLOTHES()
			IF DOES_ENTITY_EXIST(vehRival1)
			AND DOES_ENTITY_EXIST(pedRival1)
			AND DOES_ENTITY_EXIST(vehRival2)
			AND DOES_ENTITY_EXIST(pedRival2)
				//Cutscene
				REQUEST_CUTSCENE("Car_steal_3_mcs_1")
				
				//Request Cutscene Variations - Car_steal_3_mcs_1
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Racer_that_dies", pedRival1)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Racer_that_dies", PED_COMP_SPECIAL, 1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Racer_that_runsaway", pedRival2)
				ENDIF
			ENDIF
		ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2597.918945, 361.818237, 106.427612>>) > DEFAULT_CUTSCENE_UNLOAD_DIST
		OR IS_PLAYER_CHANGING_CLOTHES()
			IF HAS_CUTSCENE_LOADED()
				REMOVE_CUTSCENE()
			ENDIF
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2597.918945, 361.818237, 106.427612>>) < 300.0
			IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictCar3)
				IF HAS_MODEL_LOADED_CHECK(ENTITYXF)
					IF NOT DOES_ENTITY_EXIST(vehRival1)
						//Spawn Rival1
						vehRival1 = CREATE_CAR_STEAL_STRAND_CAR(ENTITYXF, vRival1Start, fRival1Start)	//SPAWN_VEHICLE(vehRival1, ENTITYXF, vRival1Start, fRival1Start, CARSTEAL_COLOURS_ENTITYXF)
						SET_VEHICLE_AS_RESTRICTED(vehRival1, 0)
						SET_VEHICLE_HAS_STRONG_AXLES(vehRival1, TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival1, TRUE)
						SET_VEHICLE_DOORS_LOCKED(vehRival1, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)	//VEHICLELOCK_LOCKED)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_LEFT, FALSE)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_RIGHT, FALSE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
					ENDIF
				ENDIF
				
				IF HAS_MODEL_LOADED_CHECK(IG_CAR3GUY1)
					IF NOT DOES_ENTITY_EXIST(pedRival1)
						SPAWN_PED(pedRival1, IG_CAR3GUY1, <<2581.546, 358.981, 107.662>>)
						//SET_PED_INTO_VEHICLE(pedRival1, vehRival1, VS_DRIVER)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedRival1, relGroupEnemy)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival1, TRUE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY1)
						
						TASK_PLAY_ANIM_ADVANCED(pedRival1, sAnimDictCar3, "racer_argue_01", <<2581.546, 358.981, 107.662>>, <<0.0, 0.0, 90.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
						
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedRival1, "CST3RACER1")
					ENDIF
				ENDIF
				
				IF HAS_MODEL_LOADED_CHECK(CHEETAH)
					IF NOT DOES_ENTITY_EXIST(vehRival2)
						//Spawn Rival2
						vehRival2 = CREATE_CAR_STEAL_STRAND_CAR(CHEETAH, vRival2Start, fRival2Start)	//SPAWN_VEHICLE(vehRival2, CHEETAH, vRival2Start, fRival2Start, CARSTEAL_COLOURS_CHEETAH)
						SET_VEHICLE_AS_RESTRICTED(vehRival2, 1)
						SET_VEHICLE_HAS_STRONG_AXLES(vehRival2, TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival2, TRUE)
						SET_VEHICLE_DOORS_LOCKED(vehRival2, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)	//VEHICLELOCK_LOCKED)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_LEFT, FALSE)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_RIGHT, FALSE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
					ENDIF
				ENDIF
				
				IF HAS_MODEL_LOADED_CHECK(IG_CAR3GUY2)
					IF NOT DOES_ENTITY_EXIST(pedRival2)
						SPAWN_PED(pedRival2, IG_CAR3GUY2, <<2581.546, 358.981, 107.662>>)
						SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_HEAD, 1, 0)
						SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_LEG, 0, 0)
						SET_PED_PROP_INDEX(pedRival2, ANCHOR_EYES, 0)
						//SET_PED_INTO_VEHICLE(pedRival2, vehRival2, VS_DRIVER)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedRival2, relGroupEnemy)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival2, TRUE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY2)
						
						TASK_PLAY_ANIM_ADVANCED(pedRival2, sAnimDictCar3, "racer_argue_02", <<2581.546, 358.981, 107.662>>, <<0.0, 0.0, 90.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
						
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedRival2, "CST3RACER2")
					ENDIF
				ENDIF
			ENDIF
			
			HAS_RECORDING_LOADED_CHECK(500, sCarrecSetup)
			HAS_RECORDING_LOADED_CHECK(501, sCarrecSetup)
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2597.918945, 361.818237, 106.427612>>) < 100.0
			IF DOES_ENTITY_EXIST(vehRival1)
			AND NOT IS_ENTITY_DEAD(vehRival1)
			AND DOES_ENTITY_EXIST(vehRival2)
			AND NOT IS_ENTITY_DEAD(vehRival2)
			AND DOES_ENTITY_EXIST(pedRival1)
			AND NOT IS_PED_INJURED(pedRival1)
			AND DOES_ENTITY_EXIST(pedRival2)
			AND NOT IS_PED_INJURED(pedRival2)
				IF HAS_COLLISION_LOADED_AROUND_ENTITY(vehRival1)
				AND HAS_COLLISION_LOADED_AROUND_ENTITY(vehRival2)
				AND HAS_COLLISION_LOADED_AROUND_ENTITY(pedRival1)
				AND HAS_COLLISION_LOADED_AROUND_ENTITY(pedRival2)
					IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
					AND (GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vehRival1
					OR GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vehRival2)
					OR (NOT IS_ENTITY_DEAD(vehRival1)
					AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehRival1, PLAYER_PED_ID()))
					OR (NOT IS_ENTITY_DEAD(vehRival2)
					AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehRival2, PLAYER_PED_ID()))
					OR (DOES_ENTITY_EXIST(vehRival1)
					AND NOT IS_ENTITY_AT_COORD(vehRival1, vRival1Start, <<0.1, 0.1, 2.0>>)
					OR (IS_PED_ON_VEHICLE(PLAYER_PED_ID())
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival1, <<0.0, 2.15, -2.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival1, <<0.0, -2.07, 5.0>>), 2.0)))
					OR (DOES_ENTITY_EXIST(vehRival2)
					AND NOT IS_ENTITY_AT_COORD(vehRival2, vRival2Start, <<0.1, 0.1, 2.0>>)
					OR (IS_PED_ON_VEHICLE(PLAYER_PED_ID())
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival2, <<0.0, 2.63, -2.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival2, <<0.0, -2.14, 5.0>>), 2.0)))
					OR (DOES_ENTITY_EXIST(pedRival1)
					AND NOT IS_ENTITY_PLAYING_ANIM(pedRival1, sAnimDictCar3, "racer_argue_01")
					AND NOT IS_ENTITY_PLAYING_ANIM(pedRival1, sAnimDictCar3, "racer_argue_action_01")
					AND IS_PED_RAGDOLL(pedRival1))
					OR (DOES_ENTITY_EXIST(pedRival2)
					AND NOT IS_ENTITY_PLAYING_ANIM(pedRival2, sAnimDictCar3, "racer_argue_02")
					AND NOT IS_ENTITY_PLAYING_ANIM(pedRival2, sAnimDictCar3, "racer_argue_action_02")
					AND IS_PED_RAGDOLL(pedRival2))
						IF DOES_ENTITY_EXIST(pedRival1)
						AND NOT IS_PED_INJURED(pedRival1)
						AND (GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
						OR (GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
						AND GET_SEQUENCE_PROGRESS(pedRival1) = 2))
						OR DOES_ENTITY_EXIST(pedRival2)
						AND NOT IS_PED_INJURED(pedRival2)
						AND (GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
						OR (GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
						AND GET_SEQUENCE_PROGRESS(pedRival2) = 2))
							eMissionFail = failRacerLeft
						ELSE
							eMissionFail = failRacerSpooked
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedRival1)
						AND NOT IS_PED_INJURED(pedRival1)
							IF GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_REACT_AND_FLEE_PED) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
								CLEAR_PED_TASKS(pedRival1)
								
								IF DOES_ENTITY_EXIST(vehRival1)
								AND NOT IS_ENTITY_DEAD(vehRival1)
								AND IS_PED_IN_VEHICLE(pedRival1, vehRival1)
									TASK_VEHICLE_MISSION(pedRival1, vehRival1, vehPlayer, MISSION_FLEE, 50.0, DRIVINGMODE_PLOUGHTHROUGH, 1.0, 1.0)
								ELSE
									TASK_REACT_AND_FLEE_PED(pedRival1, PLAYER_PED_ID())
								ENDIF
								
								SET_PED_KEEP_TASK(pedRival1, TRUE)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedRival2)
						AND NOT IS_PED_INJURED(pedRival2)
							IF GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_REACT_AND_FLEE_PED) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
								CLEAR_PED_TASKS(pedRival2)
								
								IF DOES_ENTITY_EXIST(vehRival2)
								AND NOT IS_ENTITY_DEAD(vehRival2)
								AND IS_PED_IN_VEHICLE(pedRival2, vehRival2)
									TASK_VEHICLE_MISSION(pedRival2, vehRival2, vehPlayer, MISSION_FLEE, 50.0, DRIVINGMODE_PLOUGHTHROUGH, 1.0, 1.0)
								ELSE
									TASK_REACT_AND_FLEE_PED(pedRival2, PLAYER_PED_ID())
								ENDIF
								
								SET_PED_KEEP_TASK(pedRival2, TRUE)
							ENDIF
						ENDIF
						
						WAIT_WITH_DEATH_CHECKS(500)
						
						missionFailed()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2578.794678, 398.877350, 106.457253>>, <<2576.777100, 331.041382, 117.452728>>, 75.0)	//<<2597.918945, 361.818237, 106.427612>>, <<2558.556152, 363.878754, 112.656685>>, 30.0)					
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//			IF CAN_PLAYER_START_CUTSCENE()
//				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 20.0)
//					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				ENDIF
//			ENDIF
			
			IF DOES_ENTITY_EXIST(pedRival1)
				IF IS_ENTITY_PLAYING_ANIM(pedRival1, sAnimDictCar3, "racer_argue_01")
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictCar3, "racer_argue_action_01", <<2581.546, 358.981, 107.662>>, <<0.0, 0.0, 90.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_EXIT_AFTER_INTERRUPTED)
						TASK_ENTER_VEHICLE(NULL, vehRival1, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK)
						TASK_VEHICLE_DRIVE_WANDER(NULL, vehRival1, 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
					CLOSE_SEQUENCE_TASK(seqMain)
					i_CST3_MCS1_AnimStartTime = GET_GAME_TIMER()
					i_CST3_MCS1_DialogueDelay = 7500
					TASK_PERFORM_SEQUENCE(pedRival1, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedRival2)
				IF IS_ENTITY_PLAYING_ANIM(pedRival2, sAnimDictCar3, "racer_argue_02")
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictCar3, "racer_argue_action_02", <<2581.546, 358.981, 107.662>>, <<0.0, 0.0, 90.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_EXIT_AFTER_INTERRUPTED)
						TASK_ENTER_VEHICLE(NULL, vehRival2, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK)
						TASK_VEHICLE_DRIVE_WANDER(NULL, vehRival2, 30.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedRival2, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					i_CST3_MCS1_AnimStartTime = GET_GAME_TIMER()
					i_CST3_MCS1_DialogueDelay = 7500
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_GET_TO_RACE")
			STOP_AUDIO_SCENE("CAR_1_GET_TO_RACE")
		ENDIF
					
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_PARK_UP_CUTSCENE")
			STOP_AUDIO_SCENE("CAR_1_PARK_UP_CUTSCENE")
		ENDIF
		
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		
		KILL_PHONE_CONVERSATION()
		
		CLEAR_PRINTS()
		CLEAR_HELP(TRUE)
		KILL_FACE_TO_FACE_CONVERSATION()
		
		SAFE_REMOVE_BLIP(blipStart)
		
		SAFE_DELETE_PED(pedDevin)
		SAFE_DELETE_PED(pedMolly)
		SAFE_DELETE_VEHICLE(vehDevin)
		SAFE_DELETE_VEHICLE(vehCutscene)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC CutsceneRace()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayGarage), "stageRace", FALSE, FALSE, PLAYER_PED(CHAR_FRANKLIN))
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		IF NOT DOES_ENTITY_EXIST(vehRival1)
			//Spawn Rival1
			vehRival1 = CREATE_CAR_STEAL_STRAND_CAR(ENTITYXF, vRival1Start, fRival1Start)	//SPAWN_VEHICLE(vehRival1, ENTITYXF, vRival1Start, fRival1Start, CARSTEAL_COLOURS_ENTITYXF)
			SET_VEHICLE_AS_RESTRICTED(vehRival1, 0)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival1, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival1, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehRival1, VEHICLELOCK_LOCKED)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival1, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(pedRival1)	
			SPAWN_PED(pedRival1, IG_CAR3GUY1, GET_ENTITY_COORDS(vehRival1))
			SET_PED_INTO_VEHICLE(pedRival1, vehRival1, VS_DRIVER)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedRival1, relGroupEnemy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival1, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY1)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehRival2)
			//Spawn Rival2
			vehRival2 = CREATE_CAR_STEAL_STRAND_CAR(CHEETAH, vRival2Start, fRival2Start)	//SPAWN_VEHICLE(vehRival2, CHEETAH, vRival2Start, fRival2Start, CARSTEAL_COLOURS_CHEETAH)
			SET_VEHICLE_AS_RESTRICTED(vehRival2, 1)
			SET_VEHICLE_HAS_STRONG_AXLES(vehRival2, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival2, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehRival2, VEHICLELOCK_LOCKED)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_LEFT, FALSE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehRival2, SC_DOOR_FRONT_RIGHT, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(pedRival2)	
			SPAWN_PED(pedRival2, IG_CAR3GUY2, GET_ENTITY_COORDS(vehRival2))
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_HEAD, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedRival2, PED_COMP_LEG, 0, 0)
			SET_PED_PROP_INDEX(pedRival2, ANCHOR_EYES, 0)
			SET_PED_INTO_VEHICLE(pedRival2, vehRival2, VS_DRIVER)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedRival2, relGroupEnemy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRival2, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_CAR3GUY2)
		ENDIF
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedRival1, "CST3RACER1")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedRival2, "CST3RACER2")
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		fPlaybackSpeed = 1.0
		
		//Radar
		bRadar = FALSE
				
		//Cutscene
		REQUEST_CUTSCENE("Car_steal_3_mcs_1", CUTSCENE_REQUESTED_FROM_Z_SKIP)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		//Audio
		LOAD_AUDIO(CAR1_MISSION_START)
		
		IF SKIPPED_STAGE()
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - Car_steal_3_mcs_1
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Racer_that_dies", pedRival1)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Racer_that_dies", PED_COMP_SPECIAL, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Racer_that_runsaway", pedRival2)
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					CLEAR_PRINTS()
					CLEAR_HELP(TRUE)
					KILL_FACE_TO_FACE_CONVERSATION()
					
//					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "PLAYER_TWO", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehPlayer, "Franklins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehRival1, "Car_Racer_runsaway", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehRival2, "Car_Racer_dies", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedRival1, "Racer_that_dies", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedRival2, "Racer_that_runsaway", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_VEHICLE_DOOR_CONTROL(vehPlayer, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					//SET_ROADS_IN_AREA(<<925.3424, 52.7882, 79.7647>> - <<1000.0, 1000.0, 1000.0>>, <<925.3424, 52.7882, 79.7647>> + <<1000.0, 1000.0, 1000.0>>, FALSE)
					
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
//					IF NOT bVideoRecording
//						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
//						
//						bVideoRecording = TRUE
//					ENDIF
					
					STOP_GAMEPLAY_HINT(TRUE)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					CLEAR_AREA(<<2570.5767, 364.8711, 107.4569>>, 1000.0, TRUE)
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF IS_CUTSCENE_PLAYING()
					SET_SRL_POST_CUTSCENE_CAMERA(<<2571.300049, 354.893677, 110.035751>>, NORMALISE_VECTOR(<<2215.980591, 0.427734, -0.092331>>))	//Rotation <<-12.179845, 0.0, -0.274823>>
				ENDIF
				
				//Traffic
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklins_car")
					//IF WAS_CUTSCENE_SKIPPED()
					//	SET_VEHICLE_POSITION(vehPlayer, <<2570.5767, 364.8711, 107.4569>>, 357.8433)
					//ENDIF
					
					REPLAY_STOP_EVENT()
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
					ENDIF
					
					SET_VEHICLE_DOORS_SHUT(vehPlayer, TRUE)
					
					//SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
					//
					//ACTIVATE_PHYSICS(vehPlayer)
					
					SET_LABEL_AS_TRIGGERED("WheelCompression[vehPlayer](Car_steal_3_mcs_1)", TRUE)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("WheelCompression[vehPlayer](Car_steal_3_mcs_1)")
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehPlayer)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("RacerRev")
					IF REQUEST_SCRIPT_AUDIO_BANK("RACE_IGNITIONS")
						IF GET_CUTSCENE_TIME() > ROUND(12.384283 * 1000.0)
							PLAY_SOUND_FROM_ENTITY(-1, "CAR_STEAL_1_IGNITIONS", vehRival1, "CAR_STEAL_1_SOUNDSET")
							
							SET_LABEL_AS_TRIGGERED("RacerRev", TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Car_Racer_runsaway")
					START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 500, sCarrecSetup)
					SET_PLAYBACK_SPEED(vehRival1, 0.0)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehRival1)
					
					SET_VEHICLE_DOORS_SHUT(vehRival1, TRUE)
					
					SET_LABEL_AS_TRIGGERED("Car_steal_3_mcs_1:Car_Racer_runsaway", TRUE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Car_Racer_dies")
					START_PLAYBACK_RECORDED_VEHICLE(vehRival2, 501, sCarrecSetup)
					SET_PLAYBACK_SPEED(vehRival2, 0.0)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehRival2)
					
					SET_VEHICLE_DOORS_SHUT(vehRival2, TRUE)
					
					SET_LABEL_AS_TRIGGERED("Car_steal_3_mcs_1:Car_Racer_dies", TRUE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Racer_that_runsaway")
					SET_LABEL_AS_TRIGGERED("Car_steal_3_mcs_1:Racer_that_runsaway", TRUE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Racer_that_dies")
					SET_LABEL_AS_TRIGGERED("Car_steal_3_mcs_1:Racer_that_dies", TRUE)
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("Car_steal_3_mcs_1:Car_Racer_runsaway")
				AND HAS_LABEL_BEEN_TRIGGERED("Car_steal_3_mcs_1:Racer_that_runsaway")
					IF NOT IS_PED_IN_VEHICLE(pedRival1, vehRival1)
						SET_PED_INTO_VEHICLE(pedRival1, vehRival1)
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("Car_steal_3_mcs_1:Car_Racer_dies")
				AND HAS_LABEL_BEEN_TRIGGERED("Car_steal_3_mcs_1:Racer_that_dies")
					IF NOT IS_PED_IN_VEHICLE(pedRival2, vehRival2)
						SET_PED_INTO_VEHICLE(pedRival2, vehRival2)
					ENDIF
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					IF bVideoRecording
						REPLAY_STOP_EVENT()
						
						bVideoRecording = FALSE
					ENDIF
					
					//Audio
					PLAY_AUDIO(CAR1_MISSION_START)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		//SET_ROADS_BACK_TO_ORIGINAL(<<925.3424, 52.7882, 79.7647>> - <<1000.0, 1000.0, 1000.0>>, <<925.3424, 52.7882, 79.7647>> + <<1000.0, 1000.0, 1000.0>>)
		
		//CLEAR_AREA(<<925.3424, 52.7882, 79.7647>>, 1000.0, TRUE)
		
		STOP_GAMEPLAY_HINT(TRUE)
		
		CLEAR_TEXT()
		
		bRadar = TRUE
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC Race()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayGarage), "stageRace", FALSE, FALSE, PLAYER_PED(CHAR_FRANKLIN))
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Uber Playback
		fPlaybackSpeed = 1.0
		
		INITIALISE_UBER_PLAYBACK(sCarrecSetup, 500)
		
		UberSetupVariables()
		
		SET_VEHICLE_STRONG(vehPlayer, TRUE)
		SET_VEHICLE_STRONG(vehRival1, TRUE)
		SET_VEHICLE_STRONG(vehRival2, TRUE)
		
		SET_VEHICLE_DOORS_SHUT(vehPlayer, TRUE)
		SET_VEHICLE_DOORS_SHUT(vehRival1, TRUE)
		SET_VEHICLE_DOORS_SHUT(vehRival2, TRUE)
		
		SET_ENTITY_COLLISION(vehPlayer, TRUE)
		SET_ENTITY_COLLISION(vehRival1, TRUE)
		SET_ENTITY_COLLISION(vehRival2, TRUE)
		
		SET_VEHICLE_IS_RACING(vehPlayer, TRUE)
		SET_VEHICLE_IS_RACING(vehRival1, TRUE)
		SET_VEHICLE_IS_RACING(vehRival2, TRUE)
		
		IF DOES_ENTITY_EXIST(pedRival1)
			IF NOT IS_PED_IN_VEHICLE(pedRival1, vehRival1)
				SET_PED_INTO_VEHICLE(pedRival1, vehRival1)
			ENDIF
			
			SET_DRIVER_RACING_MODIFIER(pedRival1, 1.0)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRival2)
			IF NOT IS_PED_IN_VEHICLE(pedRival2, vehRival2)
				SET_PED_INTO_VEHICLE(pedRival2, vehRival2)
			ENDIF
			
			SET_DRIVER_RACING_MODIFIER(pedRival2, 1.0)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehRival1, "CAR_1_RACERS")
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehRival2, "CAR_1_RACERS")
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_MAIN")
			START_AUDIO_SCENE("CAR_1_RACE_MAIN")
		ENDIF
		
		PRINT_ADV("S3_RACE")
		
		IF NOT DOES_CAM_EXIST(camCinematic)
			camCinematic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		ENDIF
		
		bRadar = TRUE
		
		bAbilityUsed = FALSE
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//Audio
		PLAY_AUDIO(CAR1_MISSION_START)
		
		IF SKIPPED_STAGE()
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(pedRival1, vehRival1)
				SET_PED_INTO_VEHICLE(pedRival1, vehRival1)
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(pedRival2, vehRival2)
				SET_PED_INTO_VEHICLE(pedRival2, vehRival2)
			ENDIF
			
			SET_VEHICLE_POSITION(vehPlayer, <<2571.3267, 360.4416, 107.4570>>, 359.7253)
			
			SET_VEHICLE_POSITION(vehRival1, vRival1Start, fRival1Start)
			
			SET_VEHICLE_POSITION(vehRival2, vRival2Start, fRival2Start)
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<2570.5767, 364.8711, 107.4569>>, 50.0)
			ENDIF
			
			INT iTimeOut = GET_GAME_TIMER() + 5000
			
			WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("RACE_IGNITIONS")
			AND GET_GAME_TIMER() < iTimeOut
				WAIT_WITH_DEATH_CHECKS(0)	#IF IS_DEBUG_BUILD	PRINTLN("REQUEST_SCRIPT_AUDIO_BANK('RACE_IGNITIONS')")	#ENDIF
				
				PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iTimeOut)
			ENDWHILE
			
			#IF IS_DEBUG_BUILD
			IF GET_GAME_TIMER() > iTimeOut
				SCRIPT_ASSERT("REQUEST_SCRIPT_AUDIO_BANK('RACE_IGNITIONS') timed out, see log for details.")
			ENDIF
			#ENDIF
			
			PLAY_SOUND_FROM_ENTITY(-1, "CAR_STEAL_1_IGNITIONS", vehRival1, "CAR_STEAL_1_SOUNDSET")
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
			
			ACTIVATE_PHYSICS(vehPlayer)
			
			START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 500, sCarrecSetup)
			SET_PLAYBACK_SPEED(vehRival1, 0.0)
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehRival1)
			
			START_PLAYBACK_RECORDED_VEHICLE(vehRival2, 501, sCarrecSetup)
			SET_PLAYBACK_SPEED(vehRival2, 0.0)
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehRival2)
			
			//Audio
			IF IS_REPLAY_IN_PROGRESS()
				PLAY_AUDIO(CAR1_MISSION_RESTART)
			ENDIF
			
			WAIT_WITH_DEATH_CHECKS(500)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehRival1, "S3_HELP2")
		
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2197058
		
		//Audio Scene
		IF IS_GAMEPLAY_HINT_ACTIVE()
			IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_CARS_FOCUS_CAM")
				START_AUDIO_SCENE("CAR_1_RACE_CARS_FOCUS_CAM")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_CARS_FOCUS_CAM")
				STOP_AUDIO_SCENE("CAR_1_RACE_CARS_FOCUS_CAM")
			ENDIF
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
				SET_VEHICLE_ENGINE_ON(vehRival1, TRUE, FALSE)
				SET_VEHICLE_ENGINE_ON(vehRival2, TRUE, FALSE)
				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
					START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 500, sCarrecSetup)
				ELSE
					SET_PLAYBACK_SPEED(vehRival1, 1.0)
				ENDIF
				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
					START_PLAYBACK_RECORDED_VEHICLE(vehRival2, 501, sCarrecSetup)
				ELSE
					SET_PLAYBACK_SPEED(vehRival2, 1.0)
				ENDIF
				
//				SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival1, 1000.0)
//				SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival2, 1000.0)
//				SET_UBER_PLAYBACK_TO_TIME_NOW(vehRival1, 1000.0)
				
				ADVANCE_CUTSCENE()
			BREAK
		ENDSWITCH
		
		IF TIMERA() >= 500
			IF NOT SAFE_IS_PLAYER_CONTROL_ON()
				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			IF DOES_BLIP_EXIST(blipPlayer)
				CLEAR_PRINTS()
				PRINT_ADV("S3_RACE")
				
				REMOVE_BLIP(blipPlayer)
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("S3_HELP1")
				CLEAR_HELP()
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(blipPlayer)
				CLEAR_PRINTS()
				PRINT_ADV("CMN_GENGETIN")
				
				SAFE_REMOVE_BLIP(blipRival1)
				SAFE_REMOVE_BLIP(blipRival2)
				
				blipPlayer = ADD_BLIP_FOR_ENTITY(vehPlayer)
				SET_BLIP_AS_FRIENDLY(blipPlayer, TRUE)
			ENDIF
		ENDIF
		
		//Horn
		VEHICLE_INDEX vehClosest
		
		IF GET_GAME_TIMER() - iClosestVehicleCheckTimer > 200
			vehClosest = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10.0, DUMMY_MODEL_FOR_SCRIPT, GET_TRAFFIC_VEHICLE_SEARCH_FLAGS())
			iClosestVehicleCheckTimer = GET_GAME_TIMER()
		ENDIF
		
		DO_CHASE_HORNS(vehClosest)
		
		//Trailers
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
			IF DOES_ENTITY_EXIST(SetPieceCarID[1])
				IF NOT DOES_ENTITY_EXIST(vehTrailer[0])
					SPAWN_VEHICLE(vehTrailer[0], TRAILERS2, GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(401, "ALubersetup"), 0.0), 0.0)
					SET_ENTITY_ROTATION(vehTrailer[0], GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(401, "ALubersetup"), 0.0))
					SET_ENTITY_NO_COLLISION_ENTITY(vehTrailer[0], SetPieceCarID[1], FALSE)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehTrailer[0])
				IF NOT IS_ENTITY_DEAD(vehTrailer[0])
					IF DOES_ENTITY_EXIST(SetPieceCarID[1])
						IF NOT IS_ENTITY_DEAD(SetPieceCarID[1])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[1])
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[0])
									START_PLAYBACK_RECORDED_VEHICLE(vehTrailer[0], 401, "ALubersetup")
								ENDIF
							ENDIF
							
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[0])
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[1])
									SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTrailer[0], GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[1]))
								ENDIF
							ELSE
								IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(SetPieceCarID[1])
									ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[1], vehTrailer[0])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(SetPieceCarID[2])
				IF NOT DOES_ENTITY_EXIST(vehTrailer[1])
					SPAWN_VEHICLE(vehTrailer[1], TRAILERS, GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(403, "ALubersetup"), 0.0), 0.0)
					SET_ENTITY_ROTATION(vehTrailer[1], GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(403, "ALubersetup"), 0.0))
					SET_ENTITY_NO_COLLISION_ENTITY(vehTrailer[1], SetPieceCarID[2], FALSE)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehTrailer[1])
				IF NOT IS_ENTITY_DEAD(vehTrailer[1])
					IF DOES_ENTITY_EXIST(SetPieceCarID[2])
						IF NOT IS_ENTITY_DEAD(SetPieceCarID[2])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[2])
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[1])
									START_PLAYBACK_RECORDED_VEHICLE(vehTrailer[1], 403, "ALubersetup")
								ENDIF
							ENDIF
							
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[1])
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[2])
									SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTrailer[1], GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[2]))
								ENDIF
							ELSE
								IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(SetPieceCarID[2])
									ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[2], vehTrailer[1])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehTrailer[0])
			AND DOES_ENTITY_EXIST(vehTrailer[1])
				IF NOT HAS_LABEL_BEEN_TRIGGERED("BetweenTrucks")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehTrailer[0], FALSE) - <<0.0, 0.0, 5.0>>, GET_ENTITY_COORDS(vehTrailer[1], FALSE) + <<0.0, 0.0, 5.0>>, 8.0)
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CS1_DROVE_BETWEEN_TRUCKS)
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						
						IF DOES_ENTITY_EXIST(SetPieceCarID[2])
							PLAY_SOUND_FROM_ENTITY(-1, "TRUCK_HORN", SetPieceCarID[2], "CAR_STEAL_1_SOUNDSET")	PRINTLN("PLAY_SOUND_FROM_ENTITY(-1, 'TRUCK_HORN', SetPieceCarID[2], 'CAR_STEAL_1_SOUNDSET')")
						ENDIF
						
						IF NOT bVideoRecording
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
							
							bVideoRecording = TRUE
						ENDIF
						
						SET_LABEL_AS_TRIGGERED("BetweenTrucks", TRUE)
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED("BetweenBuses")
					IF bVideoRecording
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehTrailer[0], FALSE) - <<0.0, 0.0, 5.0>>, GET_ENTITY_COORDS(vehTrailer[1], FALSE) + <<0.0, 0.0, 5.0>>, 8.0)
							REPLAY_STOP_EVENT()
							
							bVideoRecording = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(SetPieceCarID[3])
			AND DOES_ENTITY_EXIST(SetPieceCarID[4])
				IF NOT HAS_LABEL_BEEN_TRIGGERED("BetweenBuses")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_ENTITY_COORDS(SetPieceCarID[3], FALSE) - <<0.0, 0.0, 5.0>>, GET_ENTITY_COORDS(SetPieceCarID[4], FALSE) + <<0.0, 0.0, 5.0>>, 8.0)
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CS1_DROVE_BETWEEN_BUSES)
						
						PLAY_SOUND_FROM_ENTITY(-1, "BUS_HORN", SetPieceCarID[3], "CAR_STEAL_1_SOUNDSET")	PRINTLN("PLAY_SOUND_FROM_ENTITY(-1, 'BUS_HORN', SetPieceCarID[3], 'CAR_STEAL_1_SOUNDSET')")
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						
						IF NOT bVideoRecording
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
							
							bVideoRecording = TRUE
						ENDIF
						
						SET_LABEL_AS_TRIGGERED("BetweenBuses", TRUE)
					ENDIF
				ELSE
					IF bVideoRecording
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_ENTITY_COORDS(SetPieceCarID[3], FALSE) - <<0.0, 0.0, 5.0>>, GET_ENTITY_COORDS(SetPieceCarID[4], FALSE) + <<0.0, 0.0, 5.0>>, 8.0)
							REPLAY_STOP_EVENT()
							
							bVideoRecording = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(TrafficCarID[5])
			AND NOT IS_ENTITY_DEAD(TrafficCarID[5])
			AND DOES_ENTITY_EXIST(TrafficCarID[6])
			AND NOT IS_ENTITY_DEAD(TrafficCarID[6])
			AND DOES_ENTITY_EXIST(TrafficCarID[7])
			AND NOT IS_ENTITY_DEAD(TrafficCarID[7])
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR_1_RACE_SKIDDING_CARS")
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TrafficCarID[5]), GET_ENTITY_COORDS(vehPlayer)) < 20.0
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TrafficCarID[6]), GET_ENTITY_COORDS(vehPlayer)) < 20.0
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TrafficCarID[7]), GET_ENTITY_COORDS(vehPlayer)) < 20.0						
						//Audio Scene
						IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_SKIDDING_CARS")
							START_AUDIO_SCENE("CAR_1_RACE_SKIDDING_CARS")
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						
						SET_LABEL_AS_TRIGGERED("CAR_1_RACE_SKIDDING_CARS", TRUE)
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR_1_BRAKING_CARS_1")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TrafficCarID[5]), GET_ENTITY_COORDS(vehPlayer)) < 20.0
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(TrafficCarID[5], "CAR_1_SKIDDING_CARS_GROUP")
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							SET_LABEL_AS_TRIGGERED("CAR_1_BRAKING_CARS_1", TRUE)
						ENDIF
					ELSE
						IF GET_ENTITY_SPEED(TrafficCarID[5]) < 1.0
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(TrafficCarID[5])
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR_1_BRAKING_CARS_2")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TrafficCarID[6]), GET_ENTITY_COORDS(vehPlayer)) < 20.0
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(TrafficCarID[6], "CAR_1_SKIDDING_CARS_GROUP")
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							SET_LABEL_AS_TRIGGERED("CAR_1_BRAKING_CARS_2", TRUE)
						ENDIF
					ELSE
						IF GET_ENTITY_SPEED(TrafficCarID[6]) < 1.0
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(TrafficCarID[6])
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CAR_1_BRAKING_CARS_3")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TrafficCarID[7]), GET_ENTITY_COORDS(vehPlayer)) < 20.0
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(TrafficCarID[7], "CAR_1_SKIDDING_CARS_GROUP")
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							SET_LABEL_AS_TRIGGERED("CAR_1_BRAKING_CARS_3", TRUE)
						ENDIF
					ELSE
						IF GET_ENTITY_SPEED(TrafficCarID[7]) < 1.0
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(TrafficCarID[7])
						ENDIF
					ENDIF
					
					IF HAS_LABEL_BEEN_TRIGGERED("CAR_1_BRAKING_CARS_1")
					AND HAS_LABEL_BEEN_TRIGGERED("CAR_1_BRAKING_CARS_2")
					AND HAS_LABEL_BEEN_TRIGGERED("CAR_1_BRAKING_CARS_3")
						IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_SKIDDING_CARS")
							STOP_AUDIO_SCENE("CAR_1_RACE_SKIDDING_CARS")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
//				IF GET_TIME_POSITION_IN_RECORDING(vehRival1) < 60000.0
//					IF DOES_ENTITY_EXIST(SetPieceCarID[8])
//						IF NOT DOES_ENTITY_EXIST(vehTrailer[2])
//							IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(SetPieceCarID[8])
//								vehTrailer[2] = CREATE_VEHICLE(TR2, <<1901.4882, 1997.4597, 56.5473>>, 0.0)
//								SET_ENTITY_QUATERNION(vehTrailer[2], -0.0024, 0.0062, 0.0958, 0.9954)
//	//							ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[8], vehTrailer[2])
//								vehVehicles[0] = CREATE_VEHICLE(ASTEROPE, <<1899.9705, 2005.2631, 56.4645>>, 0.0)
//								ATTACH_ENTITY_TO_ENTITY(vehVehicles[0], vehTrailer[2], 0, <<-0.0100, 4.9100, 3.0000>>, <<3.4000, 0.0000, 0.0000>>, FALSE, FALSE, TRUE)
//								vehVehicles[1] = CREATE_VEHICLE(ASTEROPE, <<1899.9705, 2005.2631, 56.4645>>, 0.0)
//								ATTACH_ENTITY_TO_ENTITY(vehVehicles[1], vehTrailer[2], 0, <<-0.0300, 0.2600, 3.0800>>, <<-4.8700, 0.0000, 0.0000>>, FALSE, FALSE, TRUE)
//								vehVehicles[2] = CREATE_VEHICLE(ASTEROPE, <<1899.9705, 2005.2631, 56.4645>>, 0.0)
//								ATTACH_ENTITY_TO_ENTITY(vehVehicles[2], vehTrailer[2], 0, <<0.0400, -4.7700, 3.3000>>, <<0.2300, 0.0000, 0.0000>>, FALSE, FALSE, TRUE)
//								vehVehicles[3] = CREATE_VEHICLE(ASTEROPE, <<1899.9705, 2005.2631, 56.4645>>, 0.0)
//								ATTACH_ENTITY_TO_ENTITY(vehVehicles[3], vehTrailer[2], 0, <<0.0000, 5.2200, 1.0300>>, <<0.0900, 0.0000, 0.0000>>, FALSE, FALSE, TRUE)
//								vehVehicles[4] = CREATE_VEHICLE(ASTEROPE, <<1899.9705, 2005.2631, 56.4645>>, 0.0)
//								ATTACH_ENTITY_TO_ENTITY(vehVehicles[4], vehTrailer[2], 0, <<-0.0400, 0.3100, 1.1300>>, <<-2.6000, 0.0000, 0.0000>>, FALSE, FALSE, TRUE)
//								vehVehicles[5] = CREATE_VEHICLE(ASTEROPE, <<1899.9705, 2005.2631, 56.4645>>, 0.0)
//								ATTACH_ENTITY_TO_ENTITY(vehVehicles[5], vehTrailer[2], 0, <<-0.0400, -4.6300, 1.1100>>, <<4.6000, 0.0000, 0.0000>>, FALSE, FALSE, TRUE)
//							ENDIF
//						ENDIF
//					ENDIF
//					
//					IF DOES_ENTITY_EXIST(vehTrailer[2])
//						IF NOT IS_ENTITY_DEAD(vehTrailer[2])
//							IF DOES_ENTITY_EXIST(SetPieceCarID[8])
//								IF NOT IS_ENTITY_DEAD(SetPieceCarID[8])
//									IF GET_TIME_POSITION_IN_RECORDING(vehRival1) < 48436.000
//										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
//											IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[2])
//												START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehTrailer[2], 313, sCarrecRace, ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY) | ENUM_TO_INT(SWITCH_ON_PLAYER_VEHICLE_IMPACT))
//											ENDIF
//										ENDIF
//									ENDIF
//									
//									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[2])
//										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
//											SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTrailer[2], GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[8])) //SET_PLAYBACK_SPEED(vehTrailer[2], fPlaybackSpeed)
//										ENDIF
//									ELSE
//										IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(SetPieceCarID[8])
//											ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[8], vehTrailer[2])
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//					IF HAS_LABEL_BEEN_TRIGGERED("NoCollision")
//						IF DOES_ENTITY_EXIST(SetPieceCarID[9])
//							IF NOT IS_ENTITY_DEAD(SetPieceCarID[9])
//								IF DOES_ENTITY_EXIST(vehTrailer[2])
//									IF NOT IS_ENTITY_DEAD(vehTrailer[2])
//										SET_ENTITY_NO_COLLISION_ENTITY(SetPieceCarID[9], vehTrailer[2], FALSE)
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(vehVehicles[0])
//									IF NOT IS_ENTITY_DEAD(vehVehicles[0])
//										SET_ENTITY_NO_COLLISION_ENTITY(SetPieceCarID[9], vehVehicles[0], FALSE)
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(vehVehicles[1])
//									IF NOT IS_ENTITY_DEAD(vehVehicles[1])
//										SET_ENTITY_NO_COLLISION_ENTITY(SetPieceCarID[9], vehVehicles[1], FALSE)
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(vehVehicles[2])
//									IF NOT IS_ENTITY_DEAD(vehVehicles[2])
//										SET_ENTITY_NO_COLLISION_ENTITY(SetPieceCarID[9], vehVehicles[2], FALSE)
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(vehVehicles[3])
//									IF NOT IS_ENTITY_DEAD(vehVehicles[3])
//										SET_ENTITY_NO_COLLISION_ENTITY(SetPieceCarID[9], vehVehicles[3], FALSE)
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(vehVehicles[4])
//									IF NOT IS_ENTITY_DEAD(vehVehicles[4])
//										SET_ENTITY_NO_COLLISION_ENTITY(SetPieceCarID[9], vehVehicles[4], FALSE)
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(vehVehicles[5])
//									IF NOT IS_ENTITY_DEAD(vehVehicles[5])
//										SET_ENTITY_NO_COLLISION_ENTITY(SetPieceCarID[9], vehVehicles[5], FALSE)
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(SetPieceCarID[9])
//								AND DOES_ENTITY_EXIST(vehTrailer[2])
//								AND DOES_ENTITY_EXIST(vehVehicles[0])
//								AND DOES_ENTITY_EXIST(vehVehicles[1])
//								AND DOES_ENTITY_EXIST(vehVehicles[2])
//								AND DOES_ENTITY_EXIST(vehVehicles[3])
//								AND DOES_ENTITY_EXIST(vehVehicles[4])
//								AND DOES_ENTITY_EXIST(vehVehicles[5])
//									SET_LABEL_AS_TRIGGERED("NoCollision", TRUE)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//					//Packer goes to AI
////					IF (DOES_ENTITY_EXIST(vehVehicles[5])
////					AND NOT IS_ENTITY_DEAD(vehVehicles[5]))
////					AND (DOES_ENTITY_EXIST(vehTrailer[2])
////					AND NOT IS_ENTITY_DEAD(vehVehicles[2]))
////					AND NOT DOES_ROPE_EXIST(ropeChain)
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
////							IF GET_TIME_POSITION_IN_RECORDING(vehRival1) > 53660.320 - 2500.000
////								IF DOES_ENTITY_EXIST(SetPieceCarID[8])
////									IF NOT IS_ENTITY_DEAD(SetPieceCarID[8])
////										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
////											SET_PLAYBACK_TO_USE_AI(SetPieceCarID[8])
////										ENDIF
////									ENDIF
////								ENDIF
////								
////								IF DOES_ENTITY_EXIST(vehTrailer[2])
////									IF NOT IS_ENTITY_DEAD(vehTrailer[2])
////										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[2])
////											STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailer[2])	//SET_PLAYBACK_TO_USE_AI(vehTrailer[2])
////										ENDIF
////									ENDIF
////								ENDIF
////							ENDIF
////						ENDIF
////					ENDIF
//					
//					//Rope Car
//					IF (DOES_ENTITY_EXIST(vehVehicles[5])
//					AND NOT IS_ENTITY_DEAD(vehVehicles[5]))
//					AND (DOES_ENTITY_EXIST(vehTrailer[2])
//					AND NOT IS_ENTITY_DEAD(vehVehicles[2]))
//					AND NOT DOES_ROPE_EXIST(ropeChain)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
//							IF GET_TIME_POSITION_IN_RECORDING(vehRival1) > 48436.000
//							AND GET_TIME_POSITION_IN_RECORDING(vehRival1) < 62000.0
//								IF DOES_ENTITY_EXIST(SetPieceCarID[8])
//									IF NOT IS_ENTITY_DEAD(SetPieceCarID[8])
//										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
//											SET_PLAYBACK_TO_USE_AI(SetPieceCarID[8])
//										ENDIF
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(vehTrailer[2])
//									IF NOT IS_ENTITY_DEAD(vehTrailer[2])
//										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[2])
//											STOP_PLAYBACK_RECORDED_VEHICLE(vehTrailer[2])	//SET_PLAYBACK_TO_USE_AI(vehTrailer[2])
//										ENDIF
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(SetPieceCarID[9])
//									IF NOT IS_ENTITY_DEAD(SetPieceCarID[9])
//										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[9])
//											SET_PLAYBACK_TO_USE_AI(SetPieceCarID[9])
//										ENDIF
//									ENDIF
//								ENDIF
//								
//								IF DOES_ENTITY_EXIST(vehVehicles[5])
//									IF NOT IS_ENTITY_DEAD(vehVehicles[5])
//										IF DOES_ENTITY_EXIST(vehTrailer[2])
//											IF NOT IS_ENTITY_DEAD(vehTrailer[2])
//												SET_ENTITY_NO_COLLISION_ENTITY(vehTrailer[2], vehVehicles[5], FALSE)
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//								
//								//SET_VEHICLE_DOOR_CONTROL(vehTrailer[2], SC_DOOR_FRONT_RIGHT, DT_DOOR_MISSING, 1.0)
//								SET_VEHICLE_DOOR_OPEN(vehTrailer[2], SC_DOOR_BOOT)
//								
//								VECTOR vTrailerRope = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrailer[2], <<0.0, -5.5 - 1.85, 0.475>>)
//								VECTOR vCarRope = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehVehicles[5], <<0.0, 2.0, 0.0>>)
//								
//								ropeChain = ADD_ROPE(vTrailerRope, <<0.0, 0.0, 0.0>>, 10.0, PHYSICS_ROPE_DEFAULT)
//								
//								ATTACH_ENTITIES_TO_ROPE(ropeChain, vehVehicles[5], vehTrailer[2], vCarRope, vTrailerRope, 10.0, 0, 0)
//								
//								DETACH_ENTITY(vehVehicles[5])
//								
//								SET_ENTITY_PROOFS(vehVehicles[5], FALSE, FALSE, TRUE, FALSE, FALSE)
//								
//								//Fudge fix for deadline: the trailer vehicle was updated with proper collision, so the car could get trapped inside. For now warp it out.
//								//SET_ENTITY_COORDS_NO_OFFSET(vehVehicles[5], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrailer[2], <<-0.0400, -7.6300, 1.1100>>))
//								
//								APPLY_FORCE_TO_ENTITY(vehVehicles[5], APPLY_TYPE_FORCE, <<0.0, -40.0, -5.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
//								
//								SET_VEHICLE_TYRE_BURST(vehVehicles[5], SC_WHEEL_CAR_FRONT_LEFT)
//								SET_VEHICLE_TYRE_BURST(vehVehicles[5], SC_WHEEL_CAR_FRONT_RIGHT)
//								SET_VEHICLE_TYRE_BURST(vehVehicles[5], SC_WHEEL_CAR_REAR_LEFT)
//								SET_VEHICLE_TYRE_BURST(vehVehicles[5], SC_WHEEL_CAR_REAR_RIGHT)
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
//				IF GET_TIME_POSITION_IN_RECORDING(vehRival1) > 47936.000
//				AND GET_TIME_POSITION_IN_RECORDING(vehRival1) < 60000.0
//					IF DOES_ENTITY_EXIST(vehVehicles[5])
//						IF NOT IS_ENTITY_DEAD(vehVehicles[5])
//							REQUEST_PTFX_ASSET()
//							
////							SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//
//							INT i
//							
//							VECTOR vSparkPoint[iTotalEffects], vSparkPointOffset[iTotalEffects]
//							
//							//Offsets
//							vSparkPointOffset[0] = << 0.5,	 1.5,  1.0>>
//							vSparkPointOffset[1] = << 0.5,	 2.0, -0.5>>
//							vSparkPointOffset[2] = <<-0.5,	 1.5,  1.0>>
//							vSparkPointOffset[3] = <<-0.5,	 2.0, -0.5>>
//							vSparkPointOffset[4] = << 0.5,	-1.5,  1.0>>
//							vSparkPointOffset[5] = << 0.5,	-2.0, -0.5>>
//							vSparkPointOffset[6] = <<-0.5,	-1.5,  1.0>>
//							vSparkPointOffset[7] = <<-0.5,	-2.0, -0.5>>
//							
//							IF HAS_PTFX_ASSET_LOADED()
//								REPEAT iTotalEffects i
//									vSparkPoint[i] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehVehicles[5], vSparkPointOffset[i])
//									
//									IF vSparkPoint[i].Z - GET_GROUND_Z(vSparkPoint[i]) < 0.3
//										IF bSparks[i] = FALSE
//											ptfxSparks[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_carsteal3_packer_scrapes", vehVehicles[5], vSparkPointOffset[i], VECTOR_ZERO, 1.2)
//											//ptfxFric[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY("wheel_fric_hard", vehVehicles[5], vSparkPointOffset[i], VECTOR_ZERO, 1.2)
//											//ptfxSmoke[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY("wheel_burnout", vehVehicles[5], vSparkPointOffset[i], VECTOR_ZERO, 1.2)
//											bSparks[i] = TRUE
//										ENDIF
//									ELSE
//										IF bSparks[i] = TRUE
//											STOP_PARTICLE_FX_LOOPED(ptfxSparks[i])
//											//STOP_PARTICLE_FX_LOOPED(ptfxFric[i])
//											//STOP_PARTICLE_FX_LOOPED(ptfxSmoke[i])
//											bSparks[i] = FALSE
//										ENDIF
//									ENDIF
//									
////									IF bSparks[i] = TRUE
////										DRAW_DEBUG_SPHERE(vSparkPoint[i], 0.05, 0, 255, 0)
////										DRAW_DEBUG_LINE_WITH_TWO_COLOURS(vSparkPoint[i], <<vSparkPoint[i].X, vSparkPoint[i].Y, GET_GROUND_Z(vSparkPoint[i])>>, 0, 255, 0, 255, 255, 0, 0, 255)
////									ELSE
////										DRAW_DEBUG_SPHERE(vSparkPoint[i], 0.05)
////										DRAW_DEBUG_LINE_WITH_TWO_COLOURS(vSparkPoint[i], <<vSparkPoint[i].X, vSparkPoint[i].Y, GET_GROUND_Z(vSparkPoint[i])>>, 0, 0, 255, 255, 255, 0, 0, 255)
////									ENDIF
////									DRAW_DEBUG_SPHERE(<<vSparkPoint[i].X, vSparkPoint[i].Y, GET_GROUND_Z(vSparkPoint[i])>>, 0.05, 255, 0, 0)
//								ENDREPEAT
//							ENDIF
//							
//							//Flip Car
//							IF GET_TIME_POSITION_IN_RECORDING(vehRival1) > 49936.0
//								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1899.71, 2327.25, 53.59>>) < 40.0
//									CREATE_CONVERSATION_ADV("CST3_Crash")
//								ENDIF
//								
////								IF NOT IS_ENTITY_UPSIDEDOWN(vehVehicles[5])
////									IF GET_TIME_POSITION_IN_RECORDING(vehRival1) % 4000 < 2000
////										APPLY_FORCE_TO_ENTITY(vehVehicles[5], APPLY_TYPE_FORCE, <<-60.0, 0.0, 0.0>>, <<-1.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
////									ELSE
////										APPLY_FORCE_TO_ENTITY(vehVehicles[5], APPLY_TYPE_FORCE, <<60.0, 0.0, 0.0>>, <<1.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
////									ENDIF
////									
////									IF GET_TIME_POSITION_IN_RECORDING(vehRival1) % 2000 < 1000
////										APPLY_FORCE_TO_ENTITY(vehVehicles[5], APPLY_TYPE_FORCE, <<0.0, 0.0, 15.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
////									ELSE
////										APPLY_FORCE_TO_ENTITY(vehVehicles[5], APPLY_TYPE_FORCE, <<0.0, 0.0, -20.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
////									ENDIF
//									
//									//APPLY_FORCE_TO_ENTITY(vehVehicles[5], APPLY_TYPE_FORCE, <<0.0, 0.0, -1.0>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
////								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				IF GET_TIME_POSITION_IN_RECORDING(vehRival1) > 62000.0
//					IF DOES_ENTITY_EXIST(vehVehicles[5])
//					AND DOES_ENTITY_EXIST(vehTrailer[2])
//						IF NOT IS_ENTITY_DEAD(vehVehicles[5])
//						AND NOT IS_ENTITY_DEAD(vehTrailer[2])
//							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehVehicles[5])) > 50.0
//								IF DOES_ROPE_EXIST(ropeChain)
//									DETACH_ROPE_FROM_ENTITY(ropeChain, vehVehicles[5])
//									DETACH_ROPE_FROM_ENTITY(ropeChain, vehTrailer[2])
//									DELETE_ROPE(ropeChain)
//								ENDIF
//								
//								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehVehicles[5])
//								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTrailer[2])
//							ENDIF
//						ENDIF
//					ENDIF				
//				ENDIF
			
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				//Cutscene Peds
				IF GET_TIME_POSITION_IN_RECORDING(vehRival1) > 100000.000
					//Spawn Police
					IF NOT DOES_ENTITY_EXIST(vehPolice1)
						IF HAS_MODEL_LOADED_CHECK(POLICEB)
							SPAWN_VEHICLE(vehPolice1, POLICEB, vPoliceStart1, fPoliceStart1)
							SET_VEHICLE_AS_RESTRICTED(vehPolice1, 2)
							SET_VEHICLE_HAS_STRONG_AXLES(vehPolice1, TRUE)
						ENDIF
					ELSE
						IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							IF HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
								PRINTLN("SPAWN_PLAYER TREVOR")
								SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorStartPos, fTrevorStartHeading)
								//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD, 5)
//								SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAIR, 3, 0)
//								SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 3, 0)
//								SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 3, 0)
								IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
									SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
								ENDIF
								SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL, FALSE)
								SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)	//Set that the player has changed clothes on mission
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
								SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_WillFlyThroughWindscreen, FALSE)
							ENDIF
						ELSE
							IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
								SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(vehPolice2)
						IF HAS_MODEL_LOADED_CHECK(POLICEB)
							SPAWN_VEHICLE(vehPolice2, POLICEB, vPoliceStart2, fPoliceStart2)
							SET_VEHICLE_AS_RESTRICTED(vehPolice2, 3)
							SET_VEHICLE_HAS_STRONG_AXLES(vehPolice2, TRUE)
						ENDIF
					ELSE
						IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
								PRINTLN("SPAWN_PLAYER MICHAEL")
								SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStartPos, fMichaelStartHeading)
								//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ANCHOR_HEAD, 4)
//								SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 6, 0)
//								SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_LEG, 6, 0)
								SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
								SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)	//Set that the player has changed clothes on mission
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
								SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_WillFlyThroughWindscreen, FALSE)
							ENDIF
						ELSE
							IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPolice2)
								SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPolice2)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			AND DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < DEFAULT_CUTSCENE_LOAD_DIST
					//Cutscene
					REQUEST_CUTSCENE("Car_steal_3_mcs_2")
					REQUEST_ANIM_DICT(strAnimDictCamShake)
					
					//Request Cutscene Variations - Car_steal_3_mcs_2
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED(CHAR_TREVOR))
					ENDIF
					
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("S3_OVER")
				IF NOT bAbilityUsed
					IF GET_TIME_POSITION_IN_RECORDING(vehRival1) > 23000.000
						IF NOT IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								PRINT_HELP_ADV("S3_OVER_KM", TRUE, 12000)
							ELSE
								PRINT_HELP_ADV("S3_OVER", TRUE, 12000)
							ENDIF
						ELSE
							SET_LABEL_AS_TRIGGERED("S3_OVER", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("S3_OVER")
					CLEAR_HELP()
				ENDIF
				
				IF IS_PC_VERSION()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("S3_OVER_KM")
						CLEAR_HELP()
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
		
		IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
		AND GET_TIME_POSITION_IN_RECORDING(vehRival2) > 86000.0 - 3000.0)
		OR IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehRival2)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehRival2, TRUE)
		ENDIF
		
		IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
		AND GET_TIME_POSITION_IN_RECORDING(vehRival1) > 102000.0 - 3000.0)
		OR IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehRival1)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehRival1, TRUE)
		ENDIF
		
//		IF GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
//			IF IS_ENTITY_ON_SCREEN(vehRival1)
//				iChaseTimer[CHASE_TIMER_ENTITYXF] = GET_GAME_TIMER() + 1500
//			ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_ENTITYXF]
//			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) > 30.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<2716.9924, 4787.3481, 43.5334>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2716.9924, 4787.3481, 43.5334>>)
//					VECTOR vWarpCoord = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 500, sCarrecRace) - 1000, sCarrecRace)
//					
//					IF NOT IS_SPHERE_VISIBLE(vWarpCoord, 5.0)
//						CLEAR_AREA(vWarpCoord, 5.0, TRUE)
//						
//						SET_VEHICLE_POSITION(vehRival1, vWarpCoord, GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 500, sCarrecRace) - 1000, sCarrecRace)))
//						
//						SET_VEHICLE_FORWARD_SPEED(vehRival1, 20.0)
//						
//						SET_VEHICLE_ENGINE_ON(vehRival1, TRUE, TRUE)
//						
//						iChaseTimer[CHASE_TIMER_ENTITYXF] = GET_GAME_TIMER() + (3000 - CLAMP_INT(ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) * GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))), 0, 1500))
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
//			IF IS_ENTITY_ON_SCREEN(vehRival2)
//				iChaseTimer[CHASE_TIMER_CHEETAH] = GET_GAME_TIMER() + 1500
//			ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_CHEETAH]
//			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) > 30.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<2716.9924, 4787.3481, 43.5334>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2716.9924, 4787.3481, 43.5334>>)
//					VECTOR vWarpCoord = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(501, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 501, sCarrecRace) - 1000, sCarrecRace)
//					
//					IF NOT IS_SPHERE_VISIBLE(vWarpCoord, 5.0)
//						CLEAR_AREA(vWarpCoord, 5.0, TRUE)
//						
//						SET_VEHICLE_POSITION(vehRival2, vWarpCoord, GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(501, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 501, sCarrecRace) - 1000, sCarrecRace)))
//						
//						SET_VEHICLE_FORWARD_SPEED(vehRival2, 20.0)
//						
//						SET_VEHICLE_ENGINE_ON(vehRival2, TRUE, TRUE)
//						
//						iChaseTimer[CHASE_TIMER_CHEETAH] = GET_GAME_TIMER() + (3000 - CLAMP_INT(ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) * GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))), 0, 1500))
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF GET_SCRIPT_TASK_STATUS(pedRival1, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
			IF IS_ENTITY_ON_SCREEN(vehRival1)
				iChaseTimer[CHASE_TIMER_ENTITYXF] = GET_GAME_TIMER() + 1500
			ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_ENTITYXF]
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) > 30.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<2391.1492, 5804.0825, 45.1577>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2391.1492, 5804.0825, 45.1577>>)
					VECTOR vWarpCoord = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 500, sCarrecSetup) - 1000, sCarrecSetup)
					
					IF NOT IS_SPHERE_VISIBLE(vWarpCoord, 5.0)
						CLEAR_AREA(vWarpCoord, 5.0, TRUE)
						
						SET_VEHICLE_POSITION(vehRival1, vWarpCoord, GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 500, sCarrecSetup) - 1000, sCarrecSetup)))
						
						SET_VEHICLE_FORWARD_SPEED(vehRival1, GET_ENTITY_SPEED(vehPlayer))
						
						SET_VEHICLE_ENGINE_ON(vehRival1, TRUE, TRUE)
						
						iChaseTimer[CHASE_TIMER_ENTITYXF] = GET_GAME_TIMER() + (3000 - CLAMP_INT(ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) * GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))), 0, 1500))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_SCRIPT_TASK_STATUS(pedRival2, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
			IF IS_ENTITY_ON_SCREEN(vehRival2)
				iChaseTimer[CHASE_TIMER_CHEETAH] = GET_GAME_TIMER() + 1500
			ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_CHEETAH]
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) > 30.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<2391.1492, 5804.0825, 45.1577>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2391.1492, 5804.0825, 45.1577>>)
					VECTOR vWarpCoord = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(501, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 501, sCarrecSetup) - 1000, sCarrecSetup)
					
					IF NOT IS_SPHERE_VISIBLE(vWarpCoord, 5.0)
						CLEAR_AREA(vWarpCoord, 5.0, TRUE)
						
						SET_VEHICLE_POSITION(vehRival2, vWarpCoord, GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(501, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 501, sCarrecSetup) - 1000, sCarrecSetup)))
						
						SET_VEHICLE_FORWARD_SPEED(vehRival2, GET_ENTITY_SPEED(vehPlayer))
						
						SET_VEHICLE_ENGINE_ON(vehRival2, TRUE, TRUE)
						
						iChaseTimer[CHASE_TIMER_CHEETAH] = GET_GAME_TIMER() + (3000 - CLAMP_INT(ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) * GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))), 0, 1500))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			IF IS_PAUSE_MENU_ACTIVE()
				IF DOES_BLIP_EXIST(blipRival1)
					SET_BLIP_COORDS(blipRival1, GET_ENTITY_COORDS(vehRival1))
				ENDIF
				
				IF DOES_BLIP_EXIST(blipRival2)
					SET_BLIP_COORDS(blipRival2, GET_ENTITY_COORDS(vehRival2))
				ENDIF
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(blipRival1)
				blipRival1 = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehRival1))
				SET_BLIP_COLOUR(blipRival1, BLIP_COLOUR_BLUE)
				SET_BLIP_PRIORITY(blipRival1, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipRival1, "S3_BLIPVEH")
			ENDIF
			
			//Racer Blips
			VECTOR vRacerChase
			VECTOR vBlipChase
			
			FLOAT fModifier
			
			//Rival 1
			vRacerChase = GET_ENTITY_COORDS(vehRival1)
			vBlipChase = GET_BLIP_COORDS(blipRival1)
			
			fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) / 4), 0.0, 17.5)
			
			vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
			vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
			vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
			
			SET_BLIP_COORDS(blipRival1, vBlipChase)
			
			IF NOT DOES_BLIP_EXIST(blipRival2)
				blipRival2 = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehRival2))
				SET_BLIP_COLOUR(blipRival2, BLIP_COLOUR_BLUE)
				SET_BLIP_PRIORITY(blipRival2, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipRival2, "S3_BLIPVEH")
			ENDIF
			
			//Rival 2
			vRacerChase = GET_ENTITY_COORDS(vehRival2)
			vBlipChase = GET_BLIP_COORDS(blipRival2)
			
			fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) / 4), 0.0, 17.5)
			
			vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
			vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
			vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
			
			SET_BLIP_COORDS(blipRival2, vBlipChase)
		ELSE
			SAFE_REMOVE_BLIP(blipRival1)
			SAFE_REMOVE_BLIP(blipRival2)
		ENDIF
		
		//Road Nodes
		#IF IS_DEBUG_BUILD	BOOL bRoadNodes = #ENDIF REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(1651.787842, 3131.189209, 3216.789307, 6348.194336)
		#IF IS_DEBUG_BUILD	IF NOT bRoadNodes PRINTLN("REQUEST_PATH_NODES_IN_AREA_THIS_FRAME = ", bRoadNodes) ENDIF #ENDIF
		
		//Progress Stage
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2479.381104, 4728.353027, 23.323929>>, <<3015.382324, 4846.076172, 153.630157>>, 450.0)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			IF CAN_PLAYER_START_CUTSCENE()
				ADVANCE_STAGE()
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_SKIDDING_CARS")
			STOP_AUDIO_SCENE("CAR_1_RACE_SKIDDING_CARS")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_CARS_FOCUS_CAM")
			STOP_AUDIO_SCENE("CAR_1_RACE_CARS_FOCUS_CAM")
		ENDIF
		
		SET_ENTITY_LOAD_COLLISION_FLAG(vehRival1, FALSE)
		SET_ENTITY_LOAD_COLLISION_FLAG(vehRival2, FALSE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS)
		SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERS2)
		
		SAFE_DELETE_VEHICLE(vehTrailer[0])
		SAFE_DELETE_VEHICLE(vehTrailer[1])
		
		REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC CutsceneSwitch()
	IF INIT_STAGE()
		//Checkpoint
		//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayChase), "stageChase", FALSE, FALSE, PLAYER_PED(CHAR_FRANKLIN))
		
		//SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		//Spawn Police
		IF NOT DOES_ENTITY_EXIST(vehPolice1)
			SPAWN_VEHICLE(vehPolice1, POLICEB, vPoliceStart1, fPoliceStart1)
			SET_VEHICLE_AS_RESTRICTED(vehPolice1, 2)
			SET_VEHICLE_HAS_STRONG_AXLES(vehPolice1, TRUE)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehPolice2)
			SPAWN_VEHICLE(vehPolice2, POLICEB, vPoliceStart2, fPoliceStart2)
			SET_VEHICLE_AS_RESTRICTED(vehPolice2, 3)
			SET_VEHICLE_HAS_STRONG_AXLES(vehPolice2, TRUE)
		ENDIF
		
		FREEZE_ENTITY_POSITION(vehPolice1, TRUE)
		FREEZE_ENTITY_POSITION(vehPolice2, TRUE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(POLICEB)
		
		IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorStartPos, fTrevorStartHeading)
			//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD, 5)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAIR, 3, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 3, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 3, 0)
			IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
				SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
			ENDIF
			WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL, FALSE)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)	//Set that the player has changed clothes on mission
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_WillFlyThroughWindscreen, FALSE)
		ENDIF
		
		IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		
		IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStartPos, fMichaelStartHeading)
			//SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ANCHOR_HEAD, 4)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 6, 0)
//			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_LEG, 6, 0)
			WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)	//Set that the player has changed clothes on mission
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PCF_WillFlyThroughWindscreen, FALSE)
		ENDIF
		
		IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPolice2)
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPolice2)
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
		
		//Restore Engine/Petrol Health
		SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1000.0)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1000.0)
		
		SET_VEHICLE_ENGINE_HEALTH(vehRival1, 1000.0)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehRival1, 1000.0)
		
		SET_VEHICLE_ENGINE_HEALTH(vehRival2, 1000.0)
		SET_VEHICLE_PETROL_TANK_HEALTH(vehRival2, 1000.0)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		bRadar = FALSE
		
		//Cutscene
		REQUEST_CUTSCENE("Car_steal_3_mcs_2", CUTSCENE_REQUESTED_FROM_Z_SKIP)
		
		REQUEST_ANIM_DICT(strAnimDictCamShake)
		
		//Audio
		PLAY_AUDIO(CAR1_COP_BIKES)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//NEW_LOAD_SCENE_START((<<2684.2, 5118.4, 45.3>>), (<<-1.3, 0.0, 2.1>>), 20.0)	
		
		IF SKIPPED_STAGE()
			//Audio
			PLAY_AUDIO(CAR1_COPS_RESTART)
			
			CLEAR_AREA(<<2716.9924, 4787.3481, 43.5334>>, 1000.0, TRUE)
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			
			SET_PED_POSITION(PLAYER_PED_ID(), <<2716.9924, 4787.3481, 43.5334>>, 11.8873)
			
			SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - Car_steal_3_mcs_2
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED(CHAR_TREVOR))
		ENDIF
		
		IF IS_PAUSE_MENU_ACTIVE()
			IF DOES_BLIP_EXIST(blipRival1)
				SET_BLIP_COORDS(blipRival1, GET_ENTITY_COORDS(vehRival1))
			ENDIF
			
			IF DOES_BLIP_EXIST(blipRival2)
				SET_BLIP_COORDS(blipRival2, GET_ENTITY_COORDS(vehRival2))
			ENDIF
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(blipRival1)
			blipRival1 = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehRival1))
			SET_BLIP_COLOUR(blipRival1, BLIP_COLOUR_BLUE)
			SET_BLIP_PRIORITY(blipRival1, BLIPPRIORITY_HIGHEST)
			SET_BLIP_NAME_FROM_TEXT_FILE(blipRival1, "S3_BLIPVEH")
		ENDIF
		
		//Racer Blips
		VECTOR vRacerChase
		VECTOR vBlipChase
		
		FLOAT fModifier
		
		//Rival 1
		vRacerChase = GET_ENTITY_COORDS(vehRival1)
		vBlipChase = GET_BLIP_COORDS(blipRival1)
		
		fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) / 4), 0.0, 17.5)
		
		vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
		vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
		vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
		
		SET_BLIP_COORDS(blipRival1, vBlipChase)
		
		IF NOT DOES_BLIP_EXIST(blipRival2)
			blipRival2 = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehRival2))
			SET_BLIP_COLOUR(blipRival2, BLIP_COLOUR_BLUE)
			SET_BLIP_PRIORITY(blipRival2, BLIPPRIORITY_HIGHEST)
			SET_BLIP_NAME_FROM_TEXT_FILE(blipRival2, "S3_BLIPVEH")
		ENDIF
		
		//Rival 2
		vRacerChase = GET_ENTITY_COORDS(vehRival2)
		vBlipChase = GET_BLIP_COORDS(blipRival2)
		
		fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) / 4), 0.0, 17.5)
		
		vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
		vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
		vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
		
		SET_BLIP_COORDS(blipRival2, vBlipChase)
		
		SWITCH iCutsceneStage
			CASE 0
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
				IF HAS_CUTSCENE_LOADED()
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
					//Fix for 179276: If the cinematic cam was active when the cutscene triggered, deactivate it before starting the mocap.
					IF DOES_CAM_EXIST(camCinematic)
						IF IS_CAM_ACTIVE(camCinematic)
							SET_CAM_ACTIVE(camCinematic, FALSE)
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
						ENDIF
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					// added by Rob to try and fix 881875
					SET_ENTITY_INVINCIBLE(vehRival1, TRUE)
					SET_ENTITY_INVINCIBLE(vehRival2, TRUE)
					SET_ENTITY_INVINCIBLE(vehPlayer, TRUE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehPlayer, FALSE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehRival1, FALSE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehRival2, FALSE)
					
					//SPAWN ATTACHED DONUTS
					SAFE_DELETE_OBJECT(oiDonut1)
					oiDonut1 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02, vDonut1Pos, FALSE, FALSE)
					IF DOES_ENTITY_EXIST(oiDonut1)
						IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
							IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_MICHAEL))
								ATTACH_ENTITY_TO_ENTITY(oiDonut1, PLAYER_PED(CHAR_MICHAEL),  GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_PH_R_HAND), vDonut1AttachPos, vDonut1AttachRot)
							ENDIF
						ENDIF
					ENDIF
					SAFE_DELETE_OBJECT(oiDonut2)
					oiDonut2 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02B, vDonut2Pos, FALSE, FALSE)
					IF DOES_ENTITY_EXIST(oiDonut2)
						IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_TREVOR))
							IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_TREVOR))
								ATTACH_ENTITY_TO_ENTITY(oiDonut2, PLAYER_PED(CHAR_TREVOR),  GET_PED_BONE_INDEX(PLAYER_PED(CHAR_TREVOR), BONETAG_PH_R_HAND), vDonut2AttachPos, vDonut2AttachRot)
							ENDIF
						ENDIF
					ENDIF
					
					//CELLPHONE
					SAFE_DELETE_OBJECT(oiCellPhone)
					oiCellPhone = CREATE_OBJECT_NO_OFFSET(PROP_PLAYER_PHONE_01, vCellphonePos, FALSE, FALSE)
					IF DOES_ENTITY_EXIST(oiCellPhone)
						IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
							IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_MICHAEL))
								ATTACH_ENTITY_TO_ENTITY(oiCellPhone, PLAYER_PED(CHAR_MICHAEL),  GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_PH_L_HAND), vCellphoneAttachPos, vCellphoneAttachRot)
							ENDIF
						ENDIF
					ENDIF
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, TRUE)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_FRANKLIN))
					AND NOT IS_ENTITY_DEAD(vehPlayer)
						CLEAR_PED_TASKS(PLAYER_PED(CHAR_FRANKLIN))
						SET_VEHICLE_FORWARD_SPEED(vehPlayer, GET_ENTITY_SPEED(vehPlayer))
						SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
						TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED(CHAR_FRANKLIN), vehPlayer, <<2602.3, 5299.4, 44.4>>, 200.0, DRIVINGSTYLE_ACCURATE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5.0, 2.0)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						
//						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
//							TASK_PLAY_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictPhoneUse, "change_station", DEFAULT, DEFAULT, DEFAULT, AF_UPPERBODY | AF_SECONDARY)
//							PRINTLN("First Person Phone Anim Played...")
//						ENDIF
						
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
						
						PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_MICHAEL, "CST3AUD", "CST3_2secs", CONV_PRIORITY_HIGH)
					ENDIF
					
					eCarStealSwitchCamState = SWITCH_CAM_START_SPLINE1
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					PREFETCH_SRL(strCustomSwitchSRL)
					SET_SRL_READAHEAD_TIMES(3, 3, 3, 3)
					SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				REQUEST_VEHICLE_RECORDING(600, "ALuberSetup")
				REQUEST_VEHICLE_ASSET(POLICEB, ENUM_TO_INT(VRF_REQUEST_ALL_ANIMS))
				
				IF NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayer, <<2751.025879, 4623.216309, 33.912022>>, <<2621.143066, 5207.858887, 63.722816>>, 40.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayer, <<2624.585449, 5179.363281, 33.771980>>, <<2512.105713, 5542.450684, 63.772625>>, 40.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayer, <<2742.847656, 4658.283691, 33.534836>>, <<2804.21216, 4427.76123, 66.249695>>, 40.0)
					eMissionFail = failLeftBehind
					
					missionFailed()
				ENDIF
				
				IF TIMERA() > 5000
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(600, "ALuberSetup")
				AND HAS_VEHICLE_ASSET_LOADED(POLICEB)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_MICHAEL))
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_TREVOR))
				//AND IS_SRL_LOADED()
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayChase), "stageChase", FALSE, FALSE, PLAYER_PED(CHAR_FRANKLIN))
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_FRANKLIN_CALLS_MICHAEL")
						START_AUDIO_SCENE("CAR_1_FRANKLIN_CALLS_MICHAEL")
					ENDIF
					
					IF NOT bVideoRecording
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						bVideoRecording = TRUE
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER")
						SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER(0.0)
						
						SET_LABEL_AS_TRIGGERED("SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER", TRUE)
					ENDIF
					
					//Push In
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
//						FILL_PUSH_IN_DATA(pushInData, PLAYER_PED(CHAR_MICHAEL), CHAR_MICHAEL, 2.0, 550, 550, 305)
						//SET_PUSH_IN_DIRECTION_MODIFIER(pushInData, <<0.0, 0.0, -20.0>>)
						
						FILL_PUSH_IN_DATA(pushInData, PLAYER_PED(CHAR_MICHAEL), CHAR_MICHAEL, 2.0, GET_CUTSCENE_TOTAL_DURATION() - ROUND(3.274930 * 1000.0), GET_CUTSCENE_TOTAL_DURATION() - ROUND(3.274930 * 1000.0), GET_CUTSCENE_TOTAL_DURATION() - ROUND(3.274930 * 1000.0) - 300, ((GET_CUTSCENE_TOTAL_DURATION() - ROUND(3.274930 * 1000.0)) / 3) * 2, PUSH_IN_SPEED_UP_PROPORTION)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				LOAD_STREAM("CAR_STEAL_1_PASSBY", "CAR_STEAL_1_SOUNDSET")
				//Push In
				//3.786076
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					IF IS_CUTSCENE_PLAYING()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("PushInMichael")
							IF GET_CUTSCENE_TIME() > ROUND(3.274930 * 1000.0)	//2.432000 * 1000.0)
								BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
								IF HANDLE_PUSH_IN(pushInData)	//, DEFAULT, DEFAULT, FALSE, DEFAULT, FALSE)
									SET_LABEL_AS_TRIGGERED("PushInMichael", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_CUTSCENE_PLAYING()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("HereTheyCome")
						IF GET_CUTSCENE_TIME() > ROUND(21.285322 * 1000.0)
							//Audio
							PLAY_AUDIO(CAR1_APPROACH)
							
							LOAD_AUDIO(CAR1_CHASE_START)
							
							SET_LABEL_AS_TRIGGERED("HereTheyCome", TRUE)
						ENDIF
					ENDIF
				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
//				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
//					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
//						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
//					ENDIF
//					
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//					
//					IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//						SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
//						SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
//					ENDIF
//					GIVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, PV_FLAG_SCRIPT_HELMET)
//				ENDIF
//				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehPolice2)
//						SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehPolice2)
//					ENDIF
//					
//					IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
//						SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), 4)
//						SET_PED_HELMET_TEXTURE_INDEX(PLAYER_PED_ID(), 0)
//					ENDIF
//					GIVE_PED_HELMET(PLAYER_PED_ID(), TRUE, PV_FLAG_SCRIPT_HELMET)
//					
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
//					
//					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
//					SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)	//Set that the player has changed clothes on mission
//				ENDIF
//				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_police_bike")
////					SET_VEHICLE_POSITION(vehPolice1, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, sCarrecChase), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, sCarrecChase)))
////					SET_ENTITY_ROTATION(vehPolice1, GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.1, sCarrecChase))
//					
//					SET_VEHICLE_ENGINE_ON(vehPolice1, TRUE, TRUE)
//					
//					IF HAS_RECORDING_LOADED_CHECK(500, sCarrecCop)	//sCarrecChase)
//						START_PLAYBACK_RECORDED_VEHICLE(vehPolice1, 500, sCarrecCop)	//sCarrecChase)
//						SET_PLAYBACK_SPEED(vehPolice1, 0.0)
//						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehPolice1, FALSE)
//					ENDIF
//				ENDIF
//				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_Police_bike")
//					SET_VEHICLE_POSITION(vehPolice2, <<2686.8740, 5131.9189, 44.3182>>, 150.224)
//					SET_ENTITY_ROTATION(vehPolice2, <<0.541, -9.993, 150.224>>)
//					
//					SET_VEHICLE_ENGINE_ON(vehPolice2, TRUE, TRUE)
//					
//					ACTIVATE_PHYSICS(vehPolice2)
//				ENDIF
				
//				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
//					IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//						
//					ENDIF
//				ENDIF
				
				IF HANDLE_SWITCH_CINEMATIC(scsSwitchCam1, scsSwitchCam2)	//HAS_CUTSCENE_FINISHED()
					//---
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
					ENDIF
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					
					IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
						SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
					ENDIF
					GIVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, PV_FLAG_SCRIPT_HELMET)
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehPolice2)
						SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehPolice2)
					ENDIF
					
					IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
						SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), 4)
						SET_PED_HELMET_TEXTURE_INDEX(PLAYER_PED_ID(), 0)
					ENDIF
					GIVE_PED_HELMET(PLAYER_PED_ID(), TRUE, PV_FLAG_SCRIPT_HELMET)
					
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
					SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)	//Set that the player has changed clothes on mission
					
					SET_VEHICLE_ENGINE_ON(vehPolice1, TRUE, TRUE)
					
					IF HAS_RECORDING_LOADED_CHECK(500, sCarrecCop)	//sCarrecChase)
						START_PLAYBACK_RECORDED_VEHICLE(vehPolice1, 500, sCarrecCop)	//sCarrecChase)
						SET_PLAYBACK_SPEED(vehPolice1, 0.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehPolice1, FALSE)
					ENDIF
					
					SET_VEHICLE_POSITION(vehPolice2, <<2686.8625, 5131.9277, 43.8426>>, 150.224)
					SET_ENTITY_ROTATION(vehPolice2, <<0.541, -9.993, 150.224>>)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL), TRUE)
					
					SET_VEHICLE_ENGINE_ON(vehPolice2, TRUE, TRUE)
					
					ACTIVATE_PHYSICS(vehPolice2)
					//---
					
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						RESET_PUSH_IN(pushInData)
						PRINTLN("First Person Push In Reset...")
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					REMOVE_ANIM_DICT(strAnimDictCamShake)
					ADVANCE_STAGE()
				ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		IF HAS_LABEL_BEEN_TRIGGERED("SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER")
			SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER(1.0)
			
			SET_LABEL_AS_TRIGGERED("SET_CAR_HIGH_SPEED_BUMP_SEVERITY_MULTIPLIER", FALSE)
		ENDIF
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		IF bVideoRecording
			REPLAY_STOP_EVENT()
			
			bVideoRecording = FALSE
		ENDIF
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_MAIN")
			STOP_AUDIO_SCENE("CAR_1_RACE_MAIN")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_FRANKLIN_CALLS_MICHAEL")
			STOP_AUDIO_SCENE("CAR_1_FRANKLIN_CALLS_MICHAEL")
		ENDIF
		
		IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
			IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_MICHAEL))
				SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_PhoneDisableTalkingAnimations, FALSE)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_TREVOR))
			IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_TREVOR))
				SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_TREVOR), PCF_PhoneDisableTalkingAnimations, FALSE)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_FRANKLIN))
			IF NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_FRANKLIN))
				SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_PhoneDisableTalkingAnimations, FALSE)
			ENDIF
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PLAYER_PHONE_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_DONUT_02)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_DONUT_02B)
		
		SET_ENTITY_VISIBLE(vehPlayer, TRUE)
		SET_ENTITY_VISIBLE(vehRival1, TRUE)
		SET_ENTITY_VISIBLE(vehRival2, TRUE)
		
		FREEZE_ENTITY_POSITION(vehPolice1, FALSE)
		FREEZE_ENTITY_POSITION(vehPolice2, FALSE)
		
		SAFE_REMOVE_BLIP(blipRival1)
		SAFE_REMOVE_BLIP(blipRival2)
		
		SAFE_DELETE_OBJECT(oiCellPhone)
		
		SET_ROADS_BACK_TO_ORIGINAL(<<-1928.3759, 4597.9775, 56.0588>> - <<1000.0, 1000.0, 1000.0>>, <<-1928.3759, 4597.9775, 56.0588>> + <<1000.0, 1000.0, 1000.0>>)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC Chase()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayChase), "stageChase", FALSE, FALSE, PLAYER_PED(CHAR_FRANKLIN))
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		//Radar
		bRadar = TRUE
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					
			//SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 4)
			//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 6, 0)
			//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 6, 0)
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)	//Set that the player has changed clothes on mission
			
			SET_VEHICLE_POSITION(vehPolice1, vPoliceStart1, fPoliceStart1)
			SET_VEHICLE_POSITION(vehPolice2, <<2686.8792, 5131.9077, 43.8515>>, 151.6033)
			SET_ENTITY_ROTATION(vehPolice2, <<-0.3083, -9.4703, 151.2340>>)
			
			IF NOT IS_VEHICLE_EMPTY(vehPolice2)
				IF GET_PED_IN_VEHICLE_SEAT(vehPolice2) != PLAYER_PED_ID()
					CLEAR_PED_TASKS_IMMEDIATELY(GET_PED_IN_VEHICLE_SEAT(vehPolice2))
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
					SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPolice1)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPolice2)
					SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				ENDIF
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPolice2)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPolice2)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
		ENDIF
		
		SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
		SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], relGroupBuddy)
		
		IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
			SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), 4)
			SET_PED_HELMET_TEXTURE_INDEX(PLAYER_PED_ID(), 0)
		ENDIF
		GIVE_PED_HELMET(PLAYER_PED_ID(), TRUE, PV_FLAG_SCRIPT_HELMET)
		
		IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
			SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
		ENDIF
		GIVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, PV_FLAG_SCRIPT_HELMET)
		
		//STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
		
		STOP_PLAYBACK_RECORDED_VEHICLE(vehRival1)
		STOP_PLAYBACK_RECORDED_VEHICLE(vehRival2)
		STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
		
		CLEANUP_UBER_PLAYBACK(TRUE)
		
		fPlaybackSpeed = 1.0

		INITIALISE_UBER_PLAYBACK(sCarrecCop, 500)	//sCarrecChase, 500)
		
		UberCopVariables()
		
		bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
		
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			START_PLAYBACK_RECORDED_VEHICLE(vehPolice1, 500, sCarrecCop) //sCarrecChase)	#IF IS_DEBUG_BUILD	PRINTLN("START_PLAYBACK_RECORDED_VEHICLE(vehPolice1, 500, sCarrecChase)")	#ENDIF
		ELSE
			SET_PLAYBACK_SPEED(vehPolice1, 1.0)
		ENDIF
		
		START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 400, sCarrecCop) //sCarrecChase)	#IF IS_DEBUG_BUILD	PRINTLN("START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 300, sCarrecChase)")	#ENDIF
		START_PLAYBACK_RECORDED_VEHICLE(vehRival2, 401, sCarrecCop) //sCarrecChase)	#IF IS_DEBUG_BUILD	PRINTLN("START_PLAYBACK_RECORDED_VEHICLE(vehRival2, 301, sCarrecChase)")	#ENDIF
		START_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 402, sCarrecCop) //sCarrecChase)	#IF IS_DEBUG_BUILD	PRINTLN("START_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 302, sCarrecChase)")	#ENDIF
		
//		SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPolice1, 1000.0)
//		SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival1, 1000.0)
//		SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival2, 1000.0)
//		SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 1000.0)
//		SET_UBER_PLAYBACK_TO_TIME_NOW(vehPolice1, 1000.0)
		
		// added by Rob to try and fix 881875
		SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehPlayer, TRUE)
		SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehRival1, TRUE)
		SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehRival2, TRUE)
		
		SET_ENTITY_INVINCIBLE(vehRival1, TRUE)
		SET_ENTITY_INVINCIBLE(vehRival2, TRUE)
		SET_ENTITY_INVINCIBLE(vehPlayer, TRUE)
		
		SET_ENTITY_COLLISION(vehRival1, FALSE)
		SET_ENTITY_COLLISION(vehRival2, FALSE)
		SET_ENTITY_COLLISION(vehPlayer, FALSE)
		
		SET_VEHICLE_IS_RACING(vehPlayer, TRUE)
		SET_VEHICLE_IS_RACING(vehRival1, TRUE)
		SET_VEHICLE_IS_RACING(vehRival2, TRUE)
		SET_VEHICLE_IS_RACING(vehPolice1, TRUE)
		SET_VEHICLE_IS_RACING(vehPolice2, TRUE)
		
		fPlaybackSpeed = 1.0
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
			ENDIF
		ENDIF
		
		SET_VEHICLE_ENGINE_ON(vehPolice1, TRUE, TRUE)
		SET_VEHICLE_ENGINE_ON(vehPolice2, TRUE, TRUE)
		
		SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_HARD)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_CHASE_MAIN")
			START_AUDIO_SCENE("CAR_1_BIKE_CHASE_MAIN")
		ENDIF
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehPolice1, "CAR_1_TREVORS_BIKE")
		
		//Radar
		bRadar = TRUE
		
		PRINT_ADV("S3_COP", 6000)
		
		//Delay player control so Trevor gets a head start
		iPlayerControlDelay = GET_GAME_TIMER() + 1000
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//Audio
		PLAY_AUDIO(CAR1_CHASE_START)
		
		IF SKIPPED_STAGE()
			//Pause Cars
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival1)
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival2)
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_MICHAEL))
			OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_TREVOR))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE

			
			INT iTimeOut = GET_GAME_TIMER() + 5000
			
			WHILE NOT LOAD_STREAM("CAR_STEAL_1_PASSBY", "CAR_STEAL_1_SOUNDSET")
			AND GET_GAME_TIMER() < iTimeOut
				WAIT_WITH_DEATH_CHECKS(0)	#IF IS_DEBUG_BUILD	PRINTLN("LOAD_STREAM('CAR_STEAL_1_PASSBY', 'CAR_STEAL_1_SOUNDSET')")	#ENDIF
				
				PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iTimeOut)
			ENDWHILE
			
			#IF IS_DEBUG_BUILD
			IF GET_GAME_TIMER() > iTimeOut
				SCRIPT_ASSERT("LOAD_STREAM('CAR_STEAL_1_PASSBY', 'CAR_STEAL_1_SOUNDSET') timed out, see log for details.")
			ENDIF
			#ENDIF
			
			SAFE_DELETE_OBJECT(oiDonut1)
			oiDonut1 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02B, <<2683.87842, 5130.14893, 43.85238>>, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(oiDonut1)
				PLACE_OBJECT_ON_GROUND_PROPERLY(oiDonut1)
			ENDIF
			
			SAFE_DELETE_OBJECT(oiDonut2)
			oiDonut2 = CREATE_OBJECT_NO_OFFSET(PROP_DONUT_02B, <<2683.29028, 5128.59229, 43.85796>>, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(oiDonut2)
				PLACE_OBJECT_ON_GROUND_PROPERLY(oiDonut2)
			ENDIF
			
			//Audio
			PLAY_AUDIO(CAR1_CHASE_RESTART)
			
			SET_VEHICLE_POSITION(vehPolice2, <<2686.8792, 5131.9077, 43.8515>>, 151.6033)
			SET_ENTITY_ROTATION(vehPolice2, <<-0.3083, -9.4703, 151.2340>>)
			
			IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), 5)
				SET_PED_HELMET_TEXTURE_INDEX(PLAYER_PED_ID(), 0)
			ENDIF
			GIVE_PED_HELMET(PLAYER_PED_ID(), TRUE, PV_FLAG_SCRIPT_HELMET)
			
			IF NOT IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_HELMET_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 5)
				SET_PED_HELMET_TEXTURE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 0)
			ENDIF
			GIVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, PV_FLAG_SCRIPT_HELMET)
			
			WAIT_WITH_DEATH_CHECKS(500)
			
			//Unpause Cars
			UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
			UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival1)
			UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival2)
			UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
			
			//Spline Cam Skip
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) != CAM_VIEW_MODE_FIRST_PERSON
			OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) != CAM_VIEW_MODE_FIRST_PERSON
				SETUP_SPLINE_CAM_2_NODE_ARRAY(scsSwitchCam2, vehPolice2)
				CREATE_SPLINE_CAM(scsSwitchCam2)
				
				SET_CAM_ACTIVE(scsSwitchCam2.ciSpline, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			//Delay player control so Trevor gets a head start
			iPlayerControlDelay = GET_GAME_TIMER() + 1000
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		IF GET_GAME_TIMER() < iPlayerControlDelay
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
		ENDIF
		
		CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehRival1, "S3_HELP2")
		
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2197058
		
		//Audio Scene
		IF IS_GAMEPLAY_HINT_ACTIVE()
			IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_CARS_FOCUS_CAM")
				START_AUDIO_SCENE("CAR_1_RACE_CARS_FOCUS_CAM")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_CARS_FOCUS_CAM")
				STOP_AUDIO_SCENE("CAR_1_RACE_CARS_FOCUS_CAM")
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2370.747314, 5771.054688, 45.033119>>, <<2119.455566, 6025.142090, 60.062992>>, 50.0)
			IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_ENTER_TUNNEL")
				START_AUDIO_SCENE("CAR_1_BIKE_ENTER_TUNNEL")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_ENTER_TUNNEL")
				STOP_AUDIO_SCENE("CAR_1_BIKE_ENTER_TUNNEL")
			ENDIF
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF LOAD_STREAM("CAR_STEAL_1_PASSBY", "CAR_STEAL_1_SOUNDSET")
					PLAY_STREAM_FRONTEND()
				ENDIF
				
				CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 500.0)	//Rogue vehicles...
				
				ADVANCE_CUTSCENE()
			BREAK
			CASE 1
				IF TIMERA() >= 1000
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				IF TIMERA() >= 1000
					SET_VEHICLE_SIREN(vehPolice1, TRUE)
					//SET_SIREN_WITH_NO_DRIVER(vehPolice1, TRUE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				IF TIMERA() >= 2000
					SET_VEHICLE_SIREN(vehPolice2, TRUE)
					//SET_SIREN_WITH_NO_DRIVER(vehPolice2, TRUE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPolice2)
		AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_TREVOR), vehPolice1)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
				IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 8000.000
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						CREATE_CONVERSATION_ADV("CST3_Chase")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Tunnel")
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
				IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 37000.000
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						
						CREATE_CONVERSATION_ADV("CST3_Tunnel")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 15000.000
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Lag")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) > 120.0
							CREATE_CONVERSATION_ADV("CST3_Lag")
							
							iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Catchup")
						CREATE_CONVERSATION_ADV("CST3_Catchup")
						
						iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 40000.000 AND GET_TIME_POSITION_IN_RECORDING(vehPolice1) < 110000.000
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 30.0
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_GAME_TIMER() > iDialogueTimer[0]
							IF iDialogueLineCount[0] = -1
								iDialogueLineCount[0] = 5
							ELIF iDialogueLineCount[1] = -1
								iDialogueLineCount[1] = 5
							ENDIF
							
							IF iDialogueLineCount[iDialogueStage] > 0
								IF iDialogueStage = 0
									CREATE_CONVERSATION_ADV("CST3_Stay", CONV_PRIORITY_LOW, FALSE)
								ELIF iDialogueStage = 1
									CREATE_CONVERSATION_ADV("CST3_LetsGo", CONV_PRIORITY_LOW, FALSE)
								ENDIF
								
								iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
								
								iDialogueLineCount[iDialogueStage]--
							ENDIF
							
							IF iDialogueStage < 1
								iDialogueStage++
							ELSE
								iDialogueStage = 0
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 78000.000
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Bay")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 30.0
							CREATE_CONVERSATION_ADV("CST3_Bay")
							
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
							
							iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 93000.000
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_FRAN")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 30.0
							CREATE_CONVERSATION_ADV("CST3_FRAN")
							
							iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 14000.000
			AND GET_TIME_POSITION_IN_RECORDING(vehPolice1) < 30000.000
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 30.0
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Split")
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							CLEAR_PED_SECONDARY_TASK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							
//							OPEN_SEQUENCE_TASK(seqMain)
//								TASK_PLAY_ANIM(NULL, sAnimDictCar3PullOver, "pull_over_left", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
//								TASK_PLAY_ANIM(NULL, sAnimDictCar3, "point_left", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
//							CLOSE_SEQUENCE_TASK(seqMain)
//							TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
//							CLEAR_SEQUENCE_TASK(seqMain)
							
							TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_left", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START | AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
							PRINTLN("Car Steal 1 - TASK_PLAY_ANIM 'point_left' on Trevor")
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							
							CREATE_CONVERSATION_ADV("CST3_Split")
							
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
							
							SETTIMERB(0)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_left")
					IF TIMERB() < 1500
						IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_left") > 0.55
							SET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_left", 0.55)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 106000.000
			AND GET_TIME_POSITION_IN_RECORDING(vehPolice1) < 109000.000
				IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_PASS_THE_LOST")
					START_AUDIO_SCENE("CAR_1_BIKE_PASS_THE_LOST")
				ENDIF
			ENDIF
			
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 106000.000
			AND GET_TIME_POSITION_IN_RECORDING(vehPolice1) < 108000.000
				IF DOES_ENTITY_EXIST(SetPieceCarID[3])
				AND NOT IS_ENTITY_DEAD(SetPieceCarID[3])
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[3], "CAR_1_THE_LOST")
				ENDIF
				IF DOES_ENTITY_EXIST(SetPieceCarID[4])
				AND NOT IS_ENTITY_DEAD(SetPieceCarID[4])
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[4], "CAR_1_THE_LOST")
				ENDIF
				IF DOES_ENTITY_EXIST(SetPieceCarID[5])
				AND NOT IS_ENTITY_DEAD(SetPieceCarID[5])
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[5], "CAR_1_THE_LOST")
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Epic")
				IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 90000.000
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV("CST3_Epic")
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						
						iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 105000.000
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Bikers")
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV("CST3_Bikers")
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						
						iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
					ENDIF
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_PASS_THE_LOST")
					IF ((DOES_ENTITY_EXIST(SetPieceCarID[3])
					AND NOT IS_ENTITY_DEAD(SetPieceCarID[3])
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(SetPieceCarID[3], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50.0)
					OR NOT DOES_ENTITY_EXIST(SetPieceCarID[3]))
					AND ((DOES_ENTITY_EXIST(SetPieceCarID[4])
					AND NOT IS_ENTITY_DEAD(SetPieceCarID[4])
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(SetPieceCarID[4], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50.0)
					OR NOT DOES_ENTITY_EXIST(SetPieceCarID[4]))
					AND ((DOES_ENTITY_EXIST(SetPieceCarID[5])
					AND NOT IS_ENTITY_DEAD(SetPieceCarID[5])
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(SetPieceCarID[5], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50.0)
					OR NOT DOES_ENTITY_EXIST(SetPieceCarID[5]))
						STOP_AUDIO_SCENE("CAR_1_BIKE_PASS_THE_LOST")
						
						IF DOES_ENTITY_EXIST(SetPieceCarID[3])
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[3])
						ENDIF
						IF DOES_ENTITY_EXIST(SetPieceCarID[4])
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[4])
						ENDIF
						IF DOES_ENTITY_EXIST(SetPieceCarID[5])
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[5])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 120000.000
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Caught")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 30.0
							CREATE_CONVERSATION_ADV("CST3_Caught")
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_MGood")
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 30.0
							CREATE_CONVERSATION_ADV("CST3_MGood")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 110000.000 AND GET_TIME_POSITION_IN_RECORDING(vehPolice1) < 133000.000
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 30.0
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_PullT")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_PullLas")
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF GET_GAME_TIMER() > iDialogueTimer[0]
								iDialogueStage = 2
								
								IF iDialogueLineCount[iDialogueStage] = -1 
									 iDialogueLineCount[iDialogueStage] = 7
								ELIF iDialogueLineCount[iDialogueStage] > 0
									CREATE_CONVERSATION_ADV("CST3_PullT", CONV_PRIORITY_LOW, FALSE)
									
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
									
									iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
									
									iDialogueLineCount[iDialogueStage]--
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 120000.000 AND GET_TIME_POSITION_IN_RECORDING(vehPolice1) < 143000.000
					IF GET_GAME_TIMER() > iDialogueTimer[1]
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_1")
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_1")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_2")
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_2")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_3")
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_3")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_4")
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_4")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_5")
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_5")
						ENDIF
						
						iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 133000.000
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < 30.0
					IF NOT HAS_LABEL_BEEN_TRIGGERED("PullOverTwo")
						CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						CLEAR_PED_SECONDARY_TASK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						
//						OPEN_SEQUENCE_TASK(seqMain)
//							TASK_PLAY_ANIM(NULL, sAnimDictCar3, "point_right_trev", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
//							TASK_PLAY_ANIM(NULL, sAnimDictCar3PullOver, "pull_over_right", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
//							TASK_PLAY_ANIM(NULL, sAnimDictCar3, "point_right_micheal", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
//						CLOSE_SEQUENCE_TASK(seqMain)
//						TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
//						CLEAR_SEQUENCE_TASK(seqMain)
						
						TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_right_micheal", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START | AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
						TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayer, 5000)
						PRINTLN("Car Steal 1 - TASK_PLAY_ANIM 'point_right_micheal' on Trevor")
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						
						SETTIMERB(2000)
						
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_1")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_1")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_2")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_2")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_3")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_3")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_4")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_4")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Pull_5")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("CST3_Pull", "CST3_Pull_5")
							ENDIF
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						
						SET_LABEL_AS_TRIGGERED("PullOverTwo", TRUE)
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
						IF GET_TIME_POSITION_IN_RECORDING(vehPlayer) > 135000.000
							SET_VEHICLE_INDICATOR_LIGHTS(vehPlayer, FALSE, TRUE)
						ENDIF
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
						IF GET_TIME_POSITION_IN_RECORDING(vehRival1) > 145000.000
							SET_VEHICLE_INDICATOR_LIGHTS(vehRival1, FALSE, TRUE)
						ENDIF
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
						IF GET_TIME_POSITION_IN_RECORDING(vehRival2) > 146000.000
							SET_VEHICLE_INDICATOR_LIGHTS(vehRival2, FALSE, TRUE)
						ENDIF
					ENDIF
					
					IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 143000.000
						IF NOT HAS_LABEL_BEEN_TRIGGERED("PullOverTwo")
							CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							CLEAR_PED_SECONDARY_TASK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							
//							OPEN_SEQUENCE_TASK(seqMain)
//								TASK_PLAY_ANIM(NULL, sAnimDictCar3PullOver, "pull_over_right", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
//								TASK_PLAY_ANIM(NULL, sAnimDictCar3, "point_right_micheal", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
//							CLOSE_SEQUENCE_TASK(seqMain)
//							TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
//							CLEAR_SEQUENCE_TASK(seqMain)
							
							TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_right_micheal", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START | AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
							TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayer, 5000)
							PRINTLN("Car Steal 1 - TASK_PLAY_ANIM 'point_right_micheal' on Trevor")
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							
							SETTIMERB(0)
							
							SET_LABEL_AS_TRIGGERED("PullOverTwo", TRUE)
						ENDIF
						
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CREATE_CONVERSATION_ADV("CST3_PullLas")
						ENDIF
					ENDIF					
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_right_micheal")
					IF TIMERB() < 4000
						IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_right_micheal") > 0.55
							SET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "point_right_micheal", 0.55)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF HAS_LABEL_BEEN_TRIGGERED("CST3_PullLas")
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Over")
					CREATE_CONVERSATION_ADV("CST3_Over")
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_About")
					CREATE_CONVERSATION_ADV("CST3_About")
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("ThroughTunnel")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2122.70, 6032.38, 48.97>>, <<2113.73, 6019.76, 55.97>>, 5.0)
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CS1_DROVE_THROUGH_TUNNEL) 
				
				CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				CLEAR_PED_SECONDARY_TASK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				
				TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictCar3, "punch_air", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START | AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED(CHAR_TREVOR), "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				ENDIF
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 2.0)
				
				SET_LABEL_AS_TRIGGERED("ThroughTunnel", TRUE)
			ENDIF
		ENDIF
		
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
//			//Trailers
//			IF NOT DOES_ENTITY_EXIST(vehTrailer[3])
//				IF DOES_ENTITY_EXIST(SetPieceCarID[33])
//					IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(SetPieceCarID[33])
//						vehTrailer[3] = CREATE_VEHICLE(TRAILERS2, <<313.3170, 6578.9199, 29.2749>>, 0.0)
//						ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[33], vehTrailer[3])
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF NOT DOES_ENTITY_EXIST(vehTrailer[4])
//				IF DOES_ENTITY_EXIST(SetPieceCarID[34])
//					IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(SetPieceCarID[34])
//						vehTrailer[4] = CREATE_VEHICLE(TRAILERS2, <<198.9357, 6544.1406, 31.9323>>, 0.0)
//						ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[34], vehTrailer[4])
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPolice2)
			IF NOT DOES_BLIP_EXIST(blipPolice)
				SAFE_ADD_BLIP_VEHICLE(blipPolice, vehPolice1, FALSE)
				
				SET_BLIP_SCALE(blipPolice, 0.5)
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(blipPlayer)
				SAFE_ADD_BLIP_VEHICLE(blipPlayer, vehPlayer, FALSE)
				
				SET_BLIP_SCALE(blipPlayer, 0.5)
			ENDIF
			
			SAFE_ADD_BLIP_VEHICLE(blipRival1, vehRival1, FALSE)
			
			SAFE_ADD_BLIP_VEHICLE(blipRival2, vehRival2, FALSE)
			
			IF DOES_BLIP_EXIST(blipPolice2)
				CLEAR_PRINTS()
				PRINT_ADV("S3_COP")
				
				SAFE_REMOVE_BLIP(blipPolice2)
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("S3_HELP2")
				CLEAR_HELP()
			ENDIF
			
			SAFE_REMOVE_BLIP(blipPolice)
			SAFE_REMOVE_BLIP(blipPlayer)
			SAFE_REMOVE_BLIP(blipRival1)
			SAFE_REMOVE_BLIP(blipRival2)
			
			IF NOT DOES_BLIP_EXIST(blipPolice2)
				CLEAR_PRINTS()
				PRINT_ADV("CMN_GENGETBCKBK")
				
				SAFE_ADD_BLIP_VEHICLE(blipPolice2, vehPolice2, FALSE)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1765.3965, 4732.7046, 50.9022>>, <<-1786.8444, 4754.6094, 60.9306>>, 80.0, FALSE, TRUE) 
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPolice2)
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_ADV("S3_STOP")
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1778.072510,4740.481445,56.039520>>, <<-1953.011108,4565.767578,66.024292>>, 20.5)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPolice2)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPolice2), <<-1906.9209, 4621.7510, 56.0440>>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPolice1), <<-1906.9209, 4621.7510, 56.0440>>) + CLAMP(GET_ENTITY_SPEED(vehPolice1), 0.0, 7.5)
			//AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPolice2), <<-1906.9209, 4621.7510, 56.0440>>) > 10.0
				IF fModifyTopSpeed > -50.0
					fModifyTopSpeed = fModifyTopSpeed -@ 25.0
				ENDIF
				
				IF GET_ENTITY_SPEED(vehPolice2) > 7.5
				AND GET_ENTITY_SPEED(vehPolice2) > GET_ENTITY_SPEED(vehPolice1)
					PRINTLN("GET_ENTITY_SPEED(vehPolice2) -@ 25.0 = ", GET_ENTITY_SPEED(vehPolice2) -@ 25.0)
					
					VECTOR vVelocity
					vVelocity = GET_ENTITY_VELOCITY(vehPolice2)
					SET_ENTITY_VELOCITY(vehPolice2, <<vVelocity.X / 1.05, vVelocity.Y / 1.05, vVelocity.Z>>)
					
					//SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.9)
					//TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), vehPolice2, TEMPACT_BRAKE, 1)
				ENDIF
			ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPolice2), <<-1906.9209, 4621.7510, 56.0440>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPolice1), <<-1906.9209, 4621.7510, 56.0440>>) + CLAMP(GET_ENTITY_SPEED(vehPolice1) + 2.5, 0.0, 10.0)
			//OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPolice2), <<-1906.9209, 4621.7510, 56.0440>>) < 10.0
				IF fModifyTopSpeed < 0.0
					fModifyTopSpeed = fModifyTopSpeed +@ 25.0
				ENDIF
			ENDIF
			
			MODIFY_VEHICLE_TOP_SPEED(vehPolice2, fModifyTopSpeed)	PRINTLN("fModifyTopSpeed = ", fModifyTopSpeed)
			
			VECTOR vSteerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPolice1, GET_ENTITY_COORDS(vehPolice2))
			PRINTLN("vSteerOffset = <<", vSteerOffset.X, ", ", vSteerOffset.Y, ", ", vSteerOffset.Z, ">>")
			
			IF GET_ENTITY_HEADING(vehPolice2) > 121.7529 AND GET_ENTITY_HEADING(vehPolice2) < 223.1123
			AND IS_ENTITY_IN_ANGLED_AREA(vehPolice2, <<-1956.187134,4560.509766,56.030418>>, <<-1799.060913,4717.293457,66.006142>>, 16.0)
				SET_VEHICLE_STEER_BIAS(vehPolice2, CLAMP(vSteerOffset.X / 4.0, -0.05, 0.0))
			ELIF GET_ENTITY_HEADING(vehPolice2) > 43.7849 AND GET_ENTITY_HEADING(vehPolice2) < 224.4444
			AND IS_ENTITY_IN_ANGLED_AREA(vehPolice2, <<-1805.951538,4724.200195,56.044353>>, <<-1963.063354,4567.359863,66.069725>>, 3.0)
				SET_VEHICLE_STEER_BIAS(vehPolice2, CLAMP(vSteerOffset.X / 4.0, 0.0, 0.05))
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("BridgeSlowDown")
				//TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehPolice2, <<-1906.9209, 4621.7510, 56.0440>>, 10.0, DRIVINGSTYLE_ACCURATE, POLICEB, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 1.0, 1.0)
				
				//Audio
				PLAY_AUDIO(CAR1_BRIDGE)
				
				SET_LABEL_AS_TRIGGERED("BridgeSlowDown", TRUE)
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPolice2)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1912.8, 4602.7, 56.01148>>, <<-1944.62061, 4574.32129, 66.013885>>, 21.5)	//<<-1917.27649, 4601.47168, 56.01148>>, <<-1944.62061, 4574.32129, 66.013885>>, 21.5)
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPolice2, DEFAULT_VEH_STOPPING_DISTANCE, 1, 0.5)
					IF GET_ENTITY_SPEED(vehPolice1) < 0.5
					AND GET_ENTITY_SPEED(vehPolice1) < 0.5
						//IF CAN_PLAYER_START_CUTSCENE()
							SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
							WAIT_WITH_DEATH_CHECKS(100)
							
							ADVANCE_STAGE()
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Bikers
		IF NOT HAS_LABEL_BEEN_TRIGGERED("BikerSpawn1")
			SET_LABEL_AS_TRIGGERED("BikerSpawn1", OVERRIDE_VEHICLE_DRIVER_MODEL(SetPieceCarID[3], G_M_Y_LOST_01))
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("BikerSpawn2")
			SET_LABEL_AS_TRIGGERED("BikerSpawn2", OVERRIDE_VEHICLE_DRIVER_MODEL(SetPieceCarID[4], G_M_Y_LOST_01))
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("BikerSpawn3")
			SET_LABEL_AS_TRIGGERED("BikerSpawn3", OVERRIDE_VEHICLE_DRIVER_MODEL(SetPieceCarID[5], G_M_Y_LOST_01))
		ENDIF
		
		IF DOES_ENTITY_EXIST(SetPieceCarID[3])
		AND NOT IS_ENTITY_DEAD(SetPieceCarID[3])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[3])
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]))
					IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]))
					OR NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]))
					AND GET_ENTITY_HEALTH(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3])) < 200
					OR IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), GET_ENTITY_COORDS(SetPieceCarID[3]) - <<50.0, 50.0, 50.0>>, GET_ENTITY_COORDS(SetPieceCarID[3]) + <<25.0, 25.0, 25.0>>, FALSE)
						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[3])
						
						IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]))
						AND NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]))
							IF NOT IS_PED_IN_COMBAT(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]))
								GIVE_WEAPON_TO_PED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]), WEAPONTYPE_PISTOL, INFINITE_AMMO)
								
								SET_PED_KEEP_TASK(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]), TRUE)
								
								TASK_COMBAT_PED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[3]), PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(SetPieceCarID[4])
		AND NOT IS_ENTITY_DEAD(SetPieceCarID[4])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[4])
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]))
					IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]))
					OR NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]))
					AND GET_ENTITY_HEALTH(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4])) < 200
					OR IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), GET_ENTITY_COORDS(SetPieceCarID[4]) - <<50.0, 50.0, 50.0>>, GET_ENTITY_COORDS(SetPieceCarID[4]) + <<25.0, 25.0, 25.0>>, FALSE)
						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[4])
						
						IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]))
						AND NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]))
							IF NOT IS_PED_IN_COMBAT(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]))
								GIVE_WEAPON_TO_PED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]), WEAPONTYPE_SMG, INFINITE_AMMO)
								
								SET_PED_KEEP_TASK(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]), TRUE)
								
								TASK_COMBAT_PED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[4]), PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(SetPieceCarID[5])
		AND NOT IS_ENTITY_DEAD(SetPieceCarID[5])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[5])
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]))
					IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]))
					OR NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]))
					AND GET_ENTITY_HEALTH(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5])) < 200
					OR IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), GET_ENTITY_COORDS(SetPieceCarID[5]) - <<50.0, 50.0, 50.0>>, GET_ENTITY_COORDS(SetPieceCarID[5]) + <<25.0, 25.0, 25.0>>, FALSE)
						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[5])
						
						IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]))
						AND NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]))
							IF NOT IS_PED_IN_COMBAT(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]))
								GIVE_WEAPON_TO_PED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]), WEAPONTYPE_SAWNOFFSHOTGUN, INFINITE_AMMO)
								
								SET_PED_KEEP_TASK(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]), TRUE)
								
								TASK_COMBAT_PED(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[5]), PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Cutscene
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
			IF GET_TIME_POSITION_IN_RECORDING(vehPolice1) > 130000.0
				REQUEST_CUTSCENE("Car_steal_3_mcs_3")
				
				//Request Cutscene Variations - Car_steal_3_mcs_3
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED(CHAR_TREVOR))
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Racer_that_dies", pedRival1)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Racer_that_runsaway", pedRival2)
				ENDIF
			ENDIF
		ENDIF
		
		//Anim Flag
		SET_PED_RESET_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PRF_AllowUpdateIfNoCollisionLoaded, TRUE)
		
		//Collision
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
		OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
		OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
			SET_ENTITY_COLLISION(vehRival1, TRUE)
			SET_ENTITY_COLLISION(vehRival2, TRUE)
			SET_ENTITY_COLLISION(vehPlayer, TRUE)
			
			SET_ENTITY_INVINCIBLE(vehRival1, FALSE)
			SET_ENTITY_INVINCIBLE(vehRival2, FALSE)
			SET_ENTITY_INVINCIBLE(vehPlayer, FALSE)
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		PLAY_AUDIO(CAR1_BRIDGE)
		
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_CHASE_MAIN")
			STOP_AUDIO_SCENE("CAR_1_BIKE_CHASE_MAIN")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_RACE_CARS_FOCUS_CAM")
			STOP_AUDIO_SCENE("CAR_1_RACE_CARS_FOCUS_CAM")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_ENTER_TUNNEL")
			STOP_AUDIO_SCENE("CAR_1_BIKE_ENTER_TUNNEL")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_BIKE_PASS_THE_LOST")
			STOP_AUDIO_SCENE("CAR_1_BIKE_PASS_THE_LOST")
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehPolice1)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPolice1)
		ENDIF
		
		IF DOES_ENTITY_EXIST(SetPieceCarID[3])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[3])
		ENDIF
		
		IF DOES_ENTITY_EXIST(SetPieceCarID[4])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[4])
		ENDIF
		
		IF DOES_ENTITY_EXIST(SetPieceCarID[5])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[5])
		ENDIF
		
		CLEAR_HELP()
		
		SAFE_REMOVE_BLIP(blipRival1)
		SAFE_REMOVE_BLIP(blipRival2)
		SAFE_REMOVE_BLIP(blipPolice)
		SAFE_REMOVE_BLIP(blipPolice2)
		
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
//			STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
//		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehRival1)
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehRival2)
		ENDIF
		
		SET_VEHICLE_INDICATOR_LIGHTS(vehPlayer, FALSE, FALSE)
		SET_VEHICLE_INDICATOR_LIGHTS(vehRival1, FALSE, FALSE)
		SET_VEHICLE_INDICATOR_LIGHTS(vehRival2, FALSE, FALSE)
		
		CLEANUP_UBER_PLAYBACK(FALSE)
		
		SET_ENTITY_COLLISION(vehRival1, TRUE)
		SET_ENTITY_COLLISION(vehRival2, TRUE)
		SET_ENTITY_COLLISION(vehPlayer, TRUE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC CutsceneBridge()
	IF INIT_STAGE()
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		SET_VEHICLE_SIREN(vehPolice1, TRUE)
		//SET_SIREN_WITH_NO_DRIVER(vehPolice1, TRUE)
		
		SET_VEHICLE_SIREN(vehPolice2, TRUE)
		//SET_SIREN_WITH_NO_DRIVER(vehPolice2, TRUE)
		
		SET_VEHICLE_DOORS_LOCKED(vehRival1, VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_DOORS_LOCKED(vehRival2, VEHICLELOCK_UNLOCKED)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
		
		IF DOES_ENTITY_EXIST(vehPlayer)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPlayer)
		ENDIF
		IF DOES_ENTITY_EXIST(pedRival1)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival1)
		ENDIF
		IF DOES_ENTITY_EXIST(pedRival2)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival2)
		ENDIF
		
		//Radar
		bRadar = FALSE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cutscene
		REQUEST_CUTSCENE("Car_steal_3_mcs_3", CUTSCENE_REQUESTED_FROM_Z_SKIP)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			//Audio
			PLAY_AUDIO(CAR1_BRIDGE)
			
			//Warp and load area
			SET_PED_POSITION(PLAYER_PED_ID(), <<-1921.7319, 4604.9102, 56.0562>> + <<0.36, -0.37, -0.0>>, 135.0260, FALSE)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<-1917.8722, 4608.7822, 56.0547>>, 50.0)
			ENDIF
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - Car_steal_3_mcs_3
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED(CHAR_TREVOR))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Racer_that_dies", pedRival1)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Racer_that_runsaway", pedRival2)
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					REQUEST_VEHICLE_ASSET(CHEETAH, ENUM_TO_INT(VRF_REQUEST_ENTRY_ANIMS))
					
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED(CHAR_FRANKLIN), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED(CHAR_TREVOR), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedRival1, "Racer_that_dies", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedRival2, "Racer_that_runsaway", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehPlayer, "Franklins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehRival1, "Car_Racer_runsaway", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DEFAULT, CEO_IS_CASCADE_SHADOW_FOCUS_ENTITY_DURING_EXIT)
					REGISTER_ENTITY_FOR_CUTSCENE(vehRival2, "Car_Racer_dies", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehPolice1, "Trevors_police_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehPolice2, "Michaels_Police_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					SET_ROADS_IN_AREA(<<-1928.3759, 4597.9775, 56.0588>> - <<1000.0, 1000.0, 1000.0>>, <<-1928.3759, 4597.9775, 56.0588>> + <<1000.0, 1000.0, 1000.0>>, FALSE)
					
					CLEAR_AREA(<<-1928.3759, 4597.9775, 56.0588>>, 2000.0, TRUE)
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
					ENDIF
					
					SET_VEHICLE_ENGINE_ON(vehPlayer, FALSE, TRUE)
					SET_VEHICLE_ENGINE_ON(vehRival1, FALSE, TRUE)
					SET_VEHICLE_ENGINE_ON(vehRival2, FALSE, TRUE)
					SET_VEHICLE_ENGINE_ON(vehPolice1, FALSE, TRUE)
					SET_VEHICLE_ENGINE_ON(vehPolice2, FALSE, TRUE)
					
					REQUEST_VEHICLE_ASSET(F620)
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				SET_FORCE_HD_VEHICLE(vehRival1, TRUE)
				SET_FORCE_HD_VEHICLE(vehRival2, TRUE)
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Racer_that_dies")
					SET_PED_POSITION(pedRival1, <<-1915.625732, 4600.510254, 56.0301>>, 141.4507, FALSE)
					
					TASK_SMART_FLEE_PED(pedRival1, PLAYER_PED_ID(), 500.0, -1)
					
					FORCE_PED_MOTION_STATE(pedRival1, MS_ON_FOOT_RUN)
					
					SET_PED_KEEP_TASK(pedRival1, TRUE)
					
					//SET_PED_AS_NO_LONGER_NEEDED(pedRival1)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Racer_that_runsaway")
					SET_PED_POSITION(pedRival2, <<-1921.257690, 4590.634766, 56.0301>>, 141.4507, FALSE)
					
					TASK_SMART_FLEE_PED(pedRival2, PLAYER_PED_ID(), 500.0, -1)
					
					FORCE_PED_MOTION_STATE(pedRival2, MS_ON_FOOT_RUN)
					
					SET_PED_KEEP_TASK(pedRival2, TRUE)
					
					//SET_PED_AS_NO_LONGER_NEEDED(pedRival2)
				ENDIF
				
				IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) <> -1
				OR IS_PED_WEARING_HELMET(PLAYER_PED_ID())
					IF GET_CUTSCENE_TIME() > ROUND(46.687057 * 1000.0)
						CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					//SET_PED_POSITION(PLAYER_PED_ID(), <<-1917.8722, 4608.7822, 56.0547>>, 135.0620, FALSE)
					
					CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
					REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)	//CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
					
					//TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehRival1, -1, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_ENTRY_POINT)
					
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival1)
					
					//Camera Behind Player
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(141.1 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					//SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], <<-1910.9945, 4615.9185, 56.0528>>, 23.9654, FALSE)
					
					//TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer, -1, VS_DRIVER, PEDMOVE_WALK)
					
					SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					//SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<-1921.7319, 4604.9102, 56.0562>> + <<0.36, -0.37, -0.0>>, 135.0260, FALSE)
					
					CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD)
					REMOVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)	//CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD)
					
//					OPEN_SEQUENCE_TASK(seqMain)
//						TASK_PAUSE(NULL, 1000)
//						TASK_ENTER_VEHICLE(NULL, vehRival2, -1, VS_DRIVER, PEDMOVE_WALK)
//					CLOSE_SEQUENCE_TASK(seqMain)
//					TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
//					CLEAR_SEQUENCE_TASK(seqMain)
					
					//TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2, -1, VS_DRIVER, PEDMOVE_WALK)
					
					SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklins_car")
					//SET_VEHICLE_FIXED(vehPlayer)
					
					SET_ENTITY_INVINCIBLE(vehPlayer, FALSE)
					
					//SET_VEHICLE_POSITION(vehPlayer, <<-1912.547, 4616.3615, 56.0427>>, 134.6697)
					
					SET_VEHICLE_DOORS_SHUT(vehPlayer)
					
					SET_LABEL_AS_TRIGGERED("WheelCompression[vehPlayer](Car_steal_3_mcs_3)", TRUE)	//SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("WheelCompression[vehPlayer](Car_steal_3_mcs_3)")
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehPlayer)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Car_Racer_runsaway")
					//SET_VEHICLE_FIXED(vehRival1)
					
					SET_ENTITY_INVINCIBLE(vehRival1, FALSE)
					
					//SET_VEHICLE_POSITION(vehRival1, <<-1923.0222, 4605.4929, 56.0440>>, 135.0284)
					
					SET_VEHICLE_DOORS_SHUT(vehRival1)
					
					SET_LABEL_AS_TRIGGERED("WheelCompression[vehRival1](Car_steal_3_mcs_3)", TRUE)	//SET_VEHICLE_ON_GROUND_PROPERLY(vehRival1)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("WheelCompression[vehRival1](Car_steal_3_mcs_3)")
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehRival1)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Car_Racer_dies")
					//SET_VEHICLE_FIXED(vehRival2)
					
					SET_ENTITY_INVINCIBLE(vehRival2, FALSE)
					
					//SET_VEHICLE_POSITION(vehRival2, <<-1919.3284, 4609.285, 56.0532>>, 135.0786)
					
					SET_VEHICLE_DOORS_SHUT(vehRival2)
					
					SET_LABEL_AS_TRIGGERED("WheelCompression[vehRival2](Car_steal_3_mcs_3)", TRUE)	//SET_VEHICLE_ON_GROUND_PROPERLY(vehRival2)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("WheelCompression[vehRival2](Car_steal_3_mcs_3)")
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehRival2)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_police_bike")
					SET_VEHICLE_POSITION(vehPolice1, <<-1909.1063, 4619.5996, 56.0440>>, 135.7088)
					
					SET_LABEL_AS_TRIGGERED("WheelCompression[vehPolice1](Car_steal_3_mcs_3)", TRUE)	//SET_VEHICLE_ON_GROUND_PROPERLY(vehPolice1)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("WheelCompression[vehPolice1](Car_steal_3_mcs_3)")
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehPolice1)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_Police_bike")
					SET_VEHICLE_POSITION(vehPolice2, <<-1907.5037, 4618.1211, 56.0526>>, 137.0636)
					
					SET_LABEL_AS_TRIGGERED("WheelCompression[vehPolice2](Car_steal_3_mcs_3)", TRUE)	//SET_VEHICLE_ON_GROUND_PROPERLY(vehPolice2)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("WheelCompression[vehPolice2](Car_steal_3_mcs_3)")
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehPolice2)
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		//TEMP FOR REGISTERING ENTITY BUG!
//		SET_ENTITY_INVINCIBLE(vehPlayer, FALSE)
//		SET_ENTITY_PROOFS(vehPlayer, FALSE, FALSE, FALSE, FALSE, FALSE)
//		SET_ENTITY_INVINCIBLE(vehRival1, FALSE)
//		SET_ENTITY_PROOFS(vehRival1, FALSE, FALSE, FALSE, FALSE, FALSE)
//		SET_ENTITY_INVINCIBLE(vehRival2, FALSE)
//		SET_ENTITY_PROOFS(vehRival2, FALSE, FALSE, FALSE, FALSE, FALSE)
//		SET_ENTITY_INVINCIBLE(vehPolice1, FALSE)
//		SET_ENTITY_PROOFS(vehPolice1, FALSE, FALSE, FALSE, FALSE, FALSE)
//		SET_ENTITY_INVINCIBLE(vehPolice2, FALSE)
//		SET_ENTITY_PROOFS(vehPolice2, FALSE, FALSE, FALSE, FALSE, FALSE)
		
		//SAFE_DELETE_PED(pedRival1)
		//SAFE_DELETE_PED(pedRival2)
		
		CLEAR_AREA(<<-1907.4462, 4621.8071, 56.0429>>, 1000.0, TRUE)
		
		SAFE_REMOVE_BLIP(blipPlayer)
		SAFE_REMOVE_BLIP(blipRival1)
		SAFE_REMOVE_BLIP(blipRival2)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

FUNC VECTOR ADD_VECTORS(VECTOR vVector1, VECTOR vVector2)
	RETURN <<vVector1.X + vVector2.X, vVector1.Y + vVector2.Y, vVector1.Z + vVector2.Z>>
ENDFUNC

FUNC FLOAT CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(VECTOR vPoint, INT iRecordingNumber, STRING sRecordingName = NULL, INT iIterations = 10)
	FLOAT fSeek
	FLOAT fRange
	
	fSeek = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNumber, sRecordingName) / 2
	fRange = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNumber, sRecordingName) / 2
	
	INT i
	
	FOR i = 0 TO iIterations
		IF GET_DISTANCE_BETWEEN_COORDS(vPoint, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, CLAMP(fSeek - fRange, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNumber, sRecordingName)), sRecordingName)) < GET_DISTANCE_BETWEEN_COORDS(vPoint, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, CLAMP(fSeek + fRange, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNumber, sRecordingName)), sRecordingName))
			fRange /= 2
			fSeek -= fRange
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(vPoint, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, CLAMP(fSeek - fRange, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNumber, sRecordingName)), sRecordingName)) > GET_DISTANCE_BETWEEN_COORDS(vPoint, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, CLAMP(fSeek + fRange, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNumber, sRecordingName)), sRecordingName))
			fRange /= 2
			fSeek += fRange
		ENDIF
	ENDFOR
	
	RETURN fSeek
ENDFUNC

VEHICLE_INDEX vehSwitch
VECTOR vSwitchCoords
VECTOR vSwitchRot
FLOAT fSwitchSpeed

BOOL bFranklinFinishFirst, bTrevorFinishFirst, bMichaelFinishFirst
BOOL bFranklinFinishSecond, bTrevorFinishSecond, bMichaelFinishSecond

PROC DriveHome()
	IF INIT_STAGE()
		THEFEED_PAUSE()
		
		//Audio
		PLAY_AUDIO(CAR1_BRIDGE)
		
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayDriveHome), "stageDriveHome", TRUE, FALSE, PLAYER_PED(CHAR_FRANKLIN))
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
				
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_PED_MICHAEL, SELECTOR_PED_FRANKLIN)
				
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
		
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
		
		SET_VEHICLE_STRONG(vehPlayer, TRUE)
		SET_VEHICLE_STRONG(vehRival1, TRUE)
		SET_VEHICLE_STRONG(vehRival2, TRUE)
		
		SET_VEHICLE_IS_RACING(vehPlayer, TRUE)
		SET_VEHICLE_IS_RACING(vehRival1, TRUE)
		SET_VEHICLE_IS_RACING(vehRival2, TRUE)
		
		SET_VEHICLE_USE_ALTERNATE_HANDLING(vehPlayer, TRUE)
		SET_VEHICLE_USE_ALTERNATE_HANDLING(vehRival1, TRUE)
		SET_VEHICLE_USE_ALTERNATE_HANDLING(vehRival2, TRUE)
		
		SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehPlayer, TRUE)
		SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehRival1, TRUE)
		SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehRival2, TRUE)
		
		iCarHealth[CHASE_CAR_F620] = GET_ENTITY_HEALTH(vehPlayer)
		fCarGasTank[CHASE_CAR_F620] = GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer)
		fCarEngineHealth[CHASE_CAR_F620] = GET_VEHICLE_ENGINE_HEALTH(vehPlayer)
		
		iCarHealth[CHASE_CAR_ENTITYXF] = GET_ENTITY_HEALTH(vehRival1)
		fCarGasTank[CHASE_CAR_ENTITYXF] = GET_VEHICLE_PETROL_TANK_HEALTH(vehRival1)
		fCarEngineHealth[CHASE_CAR_ENTITYXF] = GET_VEHICLE_ENGINE_HEALTH(vehRival1)
		
		iCarHealth[CHASE_CAR_CHEETAH] = GET_ENTITY_HEALTH(vehRival2)
		fCarGasTank[CHASE_CAR_CHEETAH] = GET_VEHICLE_PETROL_TANK_HEALTH(vehRival2)
		fCarEngineHealth[CHASE_CAR_CHEETAH] = GET_VEHICLE_ENGINE_HEALTH(vehRival2)
		
		//SET_VEH_RADIO_STATION(vehPlayer, )
		SET_VEH_RADIO_STATION(vehRival1, "RADIO_02_POP")
		SET_VEHICLE_RADIO_ENABLED(vehRival1, TRUE)
		SET_VEH_RADIO_STATION(vehRival2, "RADIO_09_HIPHOP_OLD")
		SET_VEHICLE_RADIO_ENABLED(vehRival2, TRUE)
		
		//SET_PED_PROP_INDEX(PLAYER_PED(CHAR_MICHAEL), ANCHOR_EARS, 0)
		//SET_PED_PROP_INDEX(PLAYER_PED(CHAR_FRANKLIN), ANCHOR_EARS, 0)
		//SET_PED_PROP_INDEX(PLAYER_PED(CHAR_TREVOR), ANCHOR_EARS, 0)
		
		SAFE_DELETE_OBJECT(objID)
		objID = CREATE_OBJECT_NO_OFFSET(P_LD_ID_CARD_01, <<-1918.7059, 4604.1597, 56.035>>, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(objID)
			//PLACE_OBJECT_ON_GROUND_PROPERLY(objID)
			SET_ENTITY_ROTATION(objID, <<-90.0, 0.0, 12.6>>)
		ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(P_LD_ID_CARD_01)
		
		IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlayer, TRUE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival1, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival2, FALSE)
		ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlayer, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival1, TRUE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival2, FALSE)
		ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlayer, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival1, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival2, TRUE)
		ENDIF
		
		SET_LABEL_AS_TRIGGERED("CMN_GENGETIN", FALSE)
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_GET_TO_GARAGE")
			START_AUDIO_SCENE("CAR_1_GET_TO_GARAGE")
		ENDIF
		
		bRadar = FALSE
		
		PRINT_ADV("S3_DRIVE")
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_2)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SAVING_GAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL_STATS)
		
		bDelayHUD = TRUE
		
		bSetUncapped = TRUE
		
		SETTIMERB(0)
		
		IF NOT DOES_BLIP_EXIST(blipGarage)
			SAFE_ADD_BLIP_LOCATION(blipGarage, vGarage, TRUE)
		ENDIF
		
		bWaitForPlayer = TRUE
		
		iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(15000, 25000)	//Make sure you don't taunt them for overtaking immediately!
		
		bFranklinFinishFirst = FALSE
		bTrevorFinishFirst = FALSE
		bMichaelFinishFirst = FALSE
		
		bFranklinFinishSecond = FALSE
		bTrevorFinishSecond = FALSE
		bMichaelFinishSecond = FALSE
		
		CLEAR_AREA(<<-1907.4462, 4621.8071, 56.0429>>, 200.0, TRUE)
		
		SET_ROADS_BACK_TO_ORIGINAL(<<-1928.3759, 4597.9775, 56.0588>> - <<1000.0, 1000.0, 1000.0>>, <<-1928.3759, 4597.9775, 56.0588>> + <<1000.0, 1000.0, 1000.0>>)
		
		IF SKIPPED_STAGE()
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD)
			
			SET_PED_POSITION(PLAYER_PED_ID(), <<-1921.7319, 4604.9102, 56.0562>> + <<0.36, -0.37, -0.0>>, 135.0260, FALSE)
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], <<-1910.9945, 4615.9185, 56.0528>>, 23.9654, FALSE)
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<-1917.8722, 4608.7822, 56.0547>>, 135.0620, FALSE)
			
			//SET_VEHICLE_FIXED(vehPlayer)
			//SET_VEHICLE_FIXED(vehRival1)
			//SET_VEHICLE_FIXED(vehRival2)
			
			SET_ENTITY_INVINCIBLE(vehRival1, FALSE)
			SET_ENTITY_INVINCIBLE(vehRival2, FALSE)
			SET_ENTITY_INVINCIBLE(vehPlayer, FALSE)
			
			SET_VEHICLE_POSITION(vehPolice1, <<-1909.1063, 4619.5996, 56.0440>>, 135.7088)
			SET_VEHICLE_POSITION(vehPolice2, <<-1907.5037, 4618.1211, 56.0526>>, 137.0636)
			SET_VEHICLE_POSITION(vehPlayer, <<-1912.547, 4616.3615, 56.0427>>, 134.6697)
			SET_VEHICLE_POSITION(vehRival1, <<-1923.0222, 4605.4929, 56.0440>>, 135.0786)
			SET_VEHICLE_POSITION(vehRival2, <<-1919.3284, 4609.285, 56.0532>>, 135.0284)
			
			SET_VEHICLE_SIREN(vehPolice1, TRUE)
			//SET_SIREN_WITH_NO_DRIVER(vehPolice1, TRUE)
			
			SET_VEHICLE_SIREN(vehPolice2, TRUE)
			//SET_SIREN_WITH_NO_DRIVER(vehPolice2, TRUE)
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<-1917.8722, 4608.7822, 56.0547>>, 50.0)
			ENDIF
			
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPolice1)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPolice2)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehRival1)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehRival2)
			
			REQUEST_VEHICLE_ASSET(CHEETAH, ENUM_TO_INT(VRF_REQUEST_ENTRY_ANIMS))
			
			WHILE NOT HAS_VEHICLE_ASSET_LOADED(CHEETAH)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_MICHAEL))
			OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_TREVOR))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			CLEAR_AREA(<<-1917.8722, 4608.7822, 56.0547>>, 200.0, TRUE)
			
//			IF IS_VEHICLE_SEAT_FREE(vehPlayer)
//				TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer, -1, VS_DRIVER, PEDMOVE_WALK)
//			ENDIF
			
//			TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehRival1, -1, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_ENTRY_POINT)
			
//			IF IS_VEHICLE_SEAT_FREE(vehRival2)
//				OPEN_SEQUENCE_TASK(seqMain)
//					TASK_PAUSE(NULL, 1000)
//					TASK_ENTER_VEHICLE(NULL, vehRival2, -1, VS_DRIVER, PEDMOVE_WALK)
//				CLOSE_SEQUENCE_TASK(seqMain)
//				TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
//				CLEAR_SEQUENCE_TASK(seqMain)
//			ENDIF
			
			SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehPlayer)
			SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehRival1)
			SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_TREVOR), vehRival2)
			
			IF DOES_ENTITY_EXIST(pedRival1)
			AND NOT IS_PED_INJURED(pedRival1)
				SET_PED_POSITION(pedRival1, <<-1915.625732, 4600.510254, 56.0301>>, 141.4507, FALSE)
				
				TASK_SMART_FLEE_PED(pedRival1, PLAYER_PED_ID(), 500.0, -1)
				
				FORCE_PED_MOTION_STATE(pedRival1, MS_ON_FOOT_RUN)
				
				SET_PED_KEEP_TASK(pedRival1, TRUE)
				
				SET_PED_AS_NO_LONGER_NEEDED(pedRival1)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedRival2)
			AND NOT IS_PED_INJURED(pedRival2)
				SET_PED_POSITION(pedRival2, <<-1921.257690, 4590.634766, 56.0301>>, 141.4507, FALSE)
				
				TASK_SMART_FLEE_PED(pedRival2, PLAYER_PED_ID(), 500.0, -1)
				
				FORCE_PED_MOTION_STATE(pedRival2, MS_ON_FOOT_RUN)
				
				SET_PED_KEEP_TASK(pedRival2, TRUE)
				
				SET_PED_AS_NO_LONGER_NEEDED(pedRival2)
			ENDIF
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(141.1 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			WAIT_WITH_DEATH_CHECKS(1000)
			
			WHILE NOT IS_VEHICLE_HIGH_DETAIL(vehRival1)
			OR NOT IS_VEHICLE_HIGH_DETAIL(vehRival2)
				SET_FORCE_HD_VEHICLE(vehRival1, TRUE)
				SET_FORCE_HD_VEHICLE(vehRival2, TRUE)
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2197058
		
		IF TIMERB() < 800
		AND NOT IS_SELECTOR_ONSCREEN(FALSE)
		AND NOT IS_SELECTOR_ONSCREEN(TRUE)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_2)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SAVING_GAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL_STATS)
		ELSE
			IF bDelayHUD
				THEFEED_RESUME()
				
				bRadar = TRUE
				
				bDelayHUD = FALSE
			ENDIF
		ENDIF
		
		IF IS_VECTOR_ZERO(vCurrentPoint[CHASE_TIMER_F620])
			vCurrentPoint[CHASE_TIMER_F620] = <<100.0, 0.0, 0.0>>
		ENDIF
		
		IF IS_VECTOR_ZERO(vCurrentPoint[CHASE_TIMER_CHEETAH])
			vCurrentPoint[CHASE_TIMER_CHEETAH] = <<0.0, 100.0, 0.0>>
		ENDIF
		
		IF IS_VECTOR_ZERO(vCurrentPoint[CHASE_TIMER_ENTITYXF])
			vCurrentPoint[CHASE_TIMER_ENTITYXF] = <<0.0, 0.0, 100.0>>
		ENDIF
		
		//Peds running away
		IF DOES_ENTITY_EXIST(pedRival1)
		AND NOT IS_PED_INJURED(pedRival1)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedRival1), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 200.0
				SET_PED_AS_NO_LONGER_NEEDED(pedRival1)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRival2)
		AND NOT IS_PED_INJURED(pedRival2)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedRival2), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 200.0
				SET_PED_AS_NO_LONGER_NEEDED(pedRival2)
			ENDIF
		ENDIF
		
		//Bikes
		IF DOES_ENTITY_EXIST(vehPolice1)
		AND NOT IS_ENTITY_DEAD(vehPolice1)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPolice1), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 200.0
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPolice1)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehPolice2)
		AND NOT IS_ENTITY_DEAD(vehPolice2)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPolice2), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 200.0
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPolice2)
			ENDIF
		ENDIF
		
		//Traffic
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1919.3295, 4609.2861, 56.0538 - 1.0>>, <<10.0, 10.0, 10.0>>)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		ELSE
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
		ENDIF
		
		//Roads
		IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-387.447174, -2211.639893, -200.0>>, <<1478.488525, -552.179993, 1500.0>>)
			REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(-387.447174, -2211.639893, 1478.488525, -552.179993)
		ELIF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-3624.216553, -3439.023682, -200.0>>, <<1435.432129, 967.935364, 1500.0>>)
			REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(-3624.216553, -3439.023682, 1435.432129, 967.935364)
		ELIF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-3624.216553, -3439.023682, -200.0>>, <<1435.432129, 2366.105469, 1500.0>>)
			REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(-3624.216553, -3439.023682, 1435.432129, 2366.105469)
		ELSE
			REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(-3624.993164, -3439.023682, 1435.432129, 5642.218750)
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("LOAD_ALL_PATH_NODES")
			IF NOT ARE_NODES_LOADED_FOR_AREA(-387.447174, -2211.639893, 1478.488525, -552.179993)
			AND NOT ARE_NODES_LOADED_FOR_AREA(-3624.216553, -3439.023682, 1435.432129, 967.935364)
			AND NOT ARE_NODES_LOADED_FOR_AREA(-3624.216553, -3439.023682, 1435.432129, 2366.105469)
			AND NOT ARE_NODES_LOADED_FOR_AREA(-3624.993164, -3439.023682, 1435.432129, 5642.218750)
				PRINTLN("LOADING PATH NODES...")
			ELSE
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayer, FALSE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival1, FALSE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehRival2, FALSE)
				
				SET_LABEL_AS_TRIGGERED("LOAD_ALL_PATH_NODES", TRUE)
			ENDIF
		ENDIF
		
		INT i
		
		VECTOR vClosestRoad
		FLOAT fClosestRoad
		INT iClosestRoad
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
		AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
		AND NOT IS_ENTITY_IN_WATER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		AND GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 5.0
			IF GET_GAME_TIMER() > iRoadNodeTimer
				GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1, vClosestRoad, fClosestRoad, iClosestRoad, NF_NONE)
				
				iRoadNodeTimer = GET_GAME_TIMER() + 500
			ENDIF
			
			REPEAT COUNT_OF(CHASE_TIMER_INDEX) i
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCurrentPoint[i]) > 40.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vClosestRoad) < (5.0 + iClosestRoad)
					
					vChasePoint[i] = vCurrentPoint[i]
					fChasePoint[i] = fCurrentPoint[i]
					
					vCurrentPoint[i] = GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					fCurrentPoint[i] = GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF TIMERA() > 1000
		IF NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayer, <<481.813293, -1306.417114, 28.200811>>, <<497.217743, -1328.954590, 34.341835>>, 15.0)
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
						IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer, -1, VS_DRIVER, PEDMOVE_WALK)
						ENDIF
						
						IF NOT IS_ENTITY_ON_SCREEN(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						AND NOT IS_ENTITY_ON_SCREEN(vehPlayer)
							SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
						ENDIF
					ELSE
						IF HAS_LABEL_BEEN_TRIGGERED("LOAD_ALL_PATH_NODES")
							IF bWaitForPlayer = FALSE
								IF NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], <<496.1170, -1323.8239, 28.8653>>, <<20.0, 20.0, 5.0>>)
									IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
										TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer, <<496.1170, -1323.8239, 28.8653>>, 100.0, DRIVINGMODE_AVOIDCARS, 1.0)
										//TASK_VEHICLE_MISSION_COORS_TARGET(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer, <<496.1170, -1323.8239, 28.8653>>, MISSION_GOTO_RACING, 60.0, DRIVINGMODE_AVOIDCARS, 1.0, 1.0, TRUE)
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) = PERFORMING_TASK
										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
										
										TASK_VEHICLE_DRIVE_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer, <<496.1170, -1323.8239, 28.8653>>, 20.0, DRIVINGSTYLE_ACCURATE, F620, DRIVINGMODE_STOPFORCARS, 2.0, 4.0)
									ENDIF
								ENDIF
								
								IF (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
								OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
								OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival2))
									IF IS_ENTITY_ON_SCREEN(vehPlayer)
										iChaseTimer[CHASE_TIMER_F620] = GET_GAME_TIMER() + 3000
									ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_F620]
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayer)) > 50.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vChasePoint[CHASE_TIMER_F620]) > 40.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
										IF NOT IS_VECTOR_ZERO(vChasePoint[CHASE_TIMER_F620])
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_F620], GET_ENTITY_COORDS(vehRival1)) > 10.0
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_F620], GET_ENTITY_COORDS(vehRival2)) > 10.0
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_F620], <<493.6570, -1319.0879, 28.5502>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<496.1170, -1323.8239, 28.8653>>)
										AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<493.6570, -1319.0879, 28.5502>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<496.1170, -1323.8239, 28.8653>>)
											CLEAR_AREA(vChasePoint[CHASE_TIMER_F620], 3.0, TRUE)
											
											SET_VEHICLE_POSITION(vehPlayer, vChasePoint[CHASE_TIMER_F620], fChasePoint[CHASE_TIMER_F620])
											
											SET_VEHICLE_FORWARD_SPEED(vehPlayer, 10.0)
											
											SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
											
											iChaseTimer[CHASE_TIMER_F620] = GET_GAME_TIMER() + (6000 - CLAMP_INT(ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) * GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))), 0, 3000))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				INT iTolerance
				
				iTolerance = 16
				
				INT iLTSx, iLTSy, iRTSx, iRTSy
				
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
				
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) = PERFORMING_TASK
				AND GET_RENDERING_CAM() != sCamDetails.camID
				AND (GET_GAME_TIMER() > iGameTimer
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				OR (iLTSx < -iTolerance OR iLTSx > iTolerance))
					PRINTLN("Stopping vehPlayer Drive Task, ", GET_GAME_TIMER(), "/", iGameTimer)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
					
//					SET_VEHICLE_FORWARD_SPEED(vehPlayer, 30.0)
				ENDIF
			ENDIF
		ELSE
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			
			IF bTrevorFinishFirst = FALSE AND bMichaelFinishFirst = FALSE
				bFranklinFinishFirst = TRUE
			ELIF (bTrevorFinishFirst = TRUE AND bMichaelFinishSecond = FALSE)
			OR (bMichaelFinishFirst = TRUE AND bTrevorFinishSecond = FALSE)
				bFranklinFinishSecond = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_IN_ANGLED_AREA(vehRival2, <<481.813293, -1306.417114, 28.200811>>, <<497.217743, -1328.954590, 34.341835>>, 15.0)
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2)
						IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2, -1, VS_DRIVER, PEDMOVE_WALK)
						ENDIF
						
						IF NOT IS_ENTITY_ON_SCREEN(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						AND NOT IS_ENTITY_ON_SCREEN(vehRival2)
							SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2)
						ENDIF
					ELSE
						IF HAS_LABEL_BEEN_TRIGGERED("LOAD_ALL_PATH_NODES")
							IF bWaitForPlayer = FALSE
								IF NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<490.83542, -1311.98657, 28.2584>>, <<20.0, 20.0, 5.0>>)
									IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
										TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2, <<490.83542, -1311.98657, 28.2584>>, 100.0, DRIVINGMODE_AVOIDCARS, 1.0)
//										TASK_VEHICLE_MISSION_COORS_TARGET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2, <<490.83542, -1311.98657, 28.2584>>, MISSION_GOTO_RACING, 60.0, DRIVINGMODE_AVOIDCARS, 1.0, 1.0, TRUE)
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) = PERFORMING_TASK
										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
										
										TASK_VEHICLE_DRIVE_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2, <<490.83542, -1311.98657, 28.2584>>, 10.0, DRIVINGSTYLE_ACCURATE, CHEETAH, DRIVINGMODE_STOPFORCARS, 2.0, 4.0)
									ENDIF
								ENDIF
								
								IF (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
								OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
								OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival2))
									IF IS_ENTITY_ON_SCREEN(vehRival2)
										iChaseTimer[CHASE_TIMER_CHEETAH] = GET_GAME_TIMER() + 3000
									ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_CHEETAH]
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) > 50.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vChasePoint[CHASE_TIMER_CHEETAH]) > 40.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
										IF NOT IS_VECTOR_ZERO(vChasePoint[CHASE_TIMER_CHEETAH])
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_CHEETAH], GET_ENTITY_COORDS(vehPlayer)) > 10.0
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_CHEETAH], GET_ENTITY_COORDS(vehRival1)) > 10.0
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_CHEETAH], <<490.83542, -1311.98657, 28.2584>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<490.83542, -1311.98657, 28.2584>>)
										AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<490.83542, -1311.98657, 28.2584>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<490.83542, -1311.98657, 28.2584>>)
											CLEAR_AREA(vChasePoint[CHASE_TIMER_CHEETAH], 3.0, TRUE)
											
											SET_VEHICLE_POSITION(vehRival2, vChasePoint[CHASE_TIMER_CHEETAH], fChasePoint[CHASE_TIMER_CHEETAH])
											
											SET_VEHICLE_FORWARD_SPEED(vehRival2, 10.0)
											
											SET_VEHICLE_ENGINE_ON(vehRival2, TRUE, TRUE)
											
											iChaseTimer[CHASE_TIMER_CHEETAH] = GET_GAME_TIMER() + (6000 - CLAMP_INT(ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) * GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))), 0, 3000))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				INT iTolerance
				
				iTolerance = 16
				
				INT iLTSx, iLTSy, iRTSx, iRTSy
				
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
				
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) = PERFORMING_TASK
				AND GET_RENDERING_CAM() != sCamDetails.camID
				AND (GET_GAME_TIMER() > iGameTimer
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				OR (iLTSx < -iTolerance OR iLTSx > iTolerance))
					PRINTLN("Stopping Playback vehRival2, ", GET_GAME_TIMER(), "/", iGameTimer)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					SET_VEHICLE_ENGINE_ON(vehRival2, TRUE, TRUE)
					
//					SET_VEHICLE_FORWARD_SPEED(vehRival2, 30.0)
				ENDIF
			ENDIF
		ELSE
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR)
			
			IF bFranklinFinishFirst = FALSE AND bMichaelFinishFirst = FALSE
				bTrevorFinishFirst = TRUE
			ELIF (bFranklinFinishFirst = TRUE AND bMichaelFinishSecond = FALSE)
			OR (bMichaelFinishFirst = TRUE AND bFranklinFinishSecond = FALSE)
				bTrevorFinishSecond = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_IN_ANGLED_AREA(vehRival1, <<481.813293, -1306.417114, 28.200811>>, <<497.217743, -1328.954590, 34.341835>>, 15.0)
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehRival1)
						IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehRival1, -1, VS_DRIVER, PEDMOVE_WALK)
						ENDIF
						
						IF NOT IS_ENTITY_ON_SCREEN(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						AND NOT IS_ENTITY_ON_SCREEN(vehRival1)
							SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehRival1)
						ENDIF
					ELSE
						IF HAS_LABEL_BEEN_TRIGGERED("LOAD_ALL_PATH_NODES")
							IF bWaitForPlayer = FALSE
								IF NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], <<489.1545, -1312.9886, 28.6664>>, <<20.0, 20.0, 5.0>>)
									IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
										TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehRival1, <<489.1545, -1312.9886, 28.6664>>, 100.0, DRIVINGMODE_AVOIDCARS, 1.0)
//										TASK_VEHICLE_MISSION_COORS_TARGET(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehRival1, <<489.1545, -1312.9886, 28.6664>>, MISSION_GOTO_RACING, 60.0, DRIVINGMODE_AVOIDCARS, 1.0, 1.0, TRUE)
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) = PERFORMING_TASK
										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
										
										TASK_VEHICLE_DRIVE_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehRival1, <<489.1545, -1312.9886, 28.6664>>, 20.0, DRIVINGSTYLE_ACCURATE, ENTITYXF, DRIVINGMODE_STOPFORCARS, 2.0, 4.0)
									ENDIF
								ENDIF
								
								IF (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
								OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
								OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival2))
									IF IS_ENTITY_ON_SCREEN(vehRival1)
										iChaseTimer[CHASE_TIMER_ENTITYXF] = GET_GAME_TIMER() + 3000
									ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_ENTITYXF]
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) > 50.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vChasePoint[CHASE_TIMER_ENTITYXF]) > 40.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
										IF NOT IS_VECTOR_ZERO(vChasePoint[CHASE_TIMER_ENTITYXF])
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_ENTITYXF], GET_ENTITY_COORDS(vehPlayer)) > 10.0
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_ENTITYXF], GET_ENTITY_COORDS(vehRival2)) > 10.0
										AND GET_DISTANCE_BETWEEN_COORDS(vChasePoint[CHASE_TIMER_ENTITYXF], <<493.6570, -1319.0879, 28.5502>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<489.1545, -1312.9886, 28.6664>>)
										AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<493.6570, -1319.0879, 28.5502>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<496.1170, -1323.8239, 28.8653>>)
											CLEAR_AREA(vChasePoint[CHASE_TIMER_ENTITYXF], 3.0, TRUE)
											
											SET_VEHICLE_POSITION(vehRival1, vChasePoint[CHASE_TIMER_ENTITYXF], fChasePoint[CHASE_TIMER_ENTITYXF])
											
											SET_VEHICLE_FORWARD_SPEED(vehRival1, 10.0)
											
											SET_VEHICLE_ENGINE_ON(vehRival1, TRUE, TRUE)
											
											iChaseTimer[CHASE_TIMER_ENTITYXF] = GET_GAME_TIMER() + (6000 - CLAMP_INT(ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) * GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))), 0, 3000))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				INT iTolerance
				
				iTolerance = 16
				
				INT iLTSx, iLTSy, iRTSx, iRTSy
				
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
				
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) = PERFORMING_TASK
				AND GET_RENDERING_CAM() != sCamDetails.camID
				AND (GET_GAME_TIMER() > iGameTimer
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				OR (iLTSx < -iTolerance OR iLTSx > iTolerance))
					PRINTLN("Stopping Playback vehRival1, ", GET_GAME_TIMER(), "/", iGameTimer)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					SET_VEHICLE_ENGINE_ON(vehRival1, TRUE, TRUE)
					
//					SET_VEHICLE_FORWARD_SPEED(vehRival1, 30.0)
				ENDIF
			ENDIF
		ELSE
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL)
			
			IF bFranklinFinishFirst = FALSE AND bTrevorFinishFirst = FALSE
				bMichaelFinishFirst = TRUE
			ELIF (bFranklinFinishFirst = TRUE AND bTrevorFinishSecond = FALSE)
			OR (bTrevorFinishFirst = TRUE AND bFranklinFinishSecond = FALSE)
				bMichaelFinishSecond = TRUE
			ENDIF
		ENDIF
		ENDIF
		
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF HAS_LABEL_BEEN_TRIGGERED("SwitchOnce")
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<492.65005, -1317.19214, 28.25825>>) > 500.0
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_GoHomeM")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_GoHomeT")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_GoHomeF")
						IF NOT HAS_LABEL_BEEN_TRIGGERED("MollyDialogue")
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, NULL, "Molly")
							
							SET_LABEL_AS_TRIGGERED("MollyDialogue", TRUE)
						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							IF PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_MOLLY, sConversationBlock, "CST3_GoHomeM", CONV_PRIORITY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
								SET_LABEL_AS_TRIGGERED("CST3_GoHomeM", TRUE)
							ENDIF
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							IF PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_MOLLY, sConversationBlock, "CST3_GoHomeT", CONV_PRIORITY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
								SET_LABEL_AS_TRIGGERED("CST3_GoHomeT", TRUE)
							ENDIF
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							IF PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_MOLLY, sConversationBlock, "CST3_GoHomeF", CONV_PRIORITY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
								SET_LABEL_AS_TRIGGERED("CST3_GoHomeF", TRUE)
							ENDIF
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MollyContact")
						enumPhonebookPresence ePhonebookPresence
						
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL
								ePhonebookPresence = MICHAEL_BOOK
							BREAK
							CASE CHAR_FRANKLIN
								ePhonebookPresence = FRANKLIN_BOOK
							BREAK
							CASE CHAR_TREVOR
								ePhonebookPresence = TREVOR_BOOK
							BREAK
						ENDSWITCH
						
						//Add Molly to the phonebook.
						ADD_CONTACT_TO_PHONEBOOK(CHAR_MOLLY, ePhonebookPresence, TRUE)
						
						SET_LABEL_AS_TRIGGERED("MollyContact", TRUE)
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_MollyM")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_MollyF")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_MollyT")
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION_ADV("CST3_MollyM")
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION_ADV("CST3_MollyF")
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION_ADV("CST3_MollyT")
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_ShitM")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_ShitF")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_ShitT")
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION_ADV("CST3_ShitT")
							
							iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(15000, 25000)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION_ADV("CST3_ShitM")
							
							iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(15000, 25000)
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION_ADV("CST3_ShitF")
							
							iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(15000, 25000)
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_RAD1")
						IF GET_GAME_TIMER() > iDialogueTimer[1]
							CREATE_CONVERSATION_ADV("CST3_RAD1")
							
							iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(40000, 55000)
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_RAD2")
						IF GET_GAME_TIMER() > iDialogueTimer[1]
							CREATE_CONVERSATION_ADV("CST3_RAD2")
							
							iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(40000, 55000)
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_RAD3")
						IF GET_GAME_TIMER() > iDialogueTimer[1]
							CREATE_CONVERSATION_ADV("CST3_RAD3")
							
							iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(40000, 55000)
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_RAD4")
						IF GET_GAME_TIMER() > iDialogueTimer[1]
							CREATE_CONVERSATION_ADV("CST3_RAD4")
							
							iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(40000, 55000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Damages Cars (Crashes)
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
				IF DOES_ENTITY_EXIST(vehPlayer) AND NOT IS_ENTITY_DEAD(vehPlayer)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehPlayer, PLAYER_PED_ID())
						CREATE_CONVERSATION_ADV("CST3_CrashF")
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				IF DOES_ENTITY_EXIST(vehRival1) AND NOT IS_ENTITY_DEAD(vehRival1)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehRival1, PLAYER_PED_ID())
						CREATE_CONVERSATION_ADV("CST3_CrashM")
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				IF DOES_ENTITY_EXIST(vehRival2) AND NOT IS_ENTITY_DEAD(vehRival2)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehRival2, PLAYER_PED_ID())
						CREATE_CONVERSATION_ADV("CST3_CrashT")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_ENTITY_HEALTH(vehPlayer) < iCarHealth[CHASE_CAR_F620]
		OR GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < fCarGasTank[CHASE_CAR_F620]
		OR GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < fCarEngineHealth[CHASE_CAR_F620]
			IF IS_ENTITY_ON_SCREEN(vehPlayer)
			AND NOT IS_ENTITY_OCCLUDED(vehPlayer)
				IF GET_GAME_TIMER() > iInterruptTimer
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_StackF")
							CREATE_CONVERSATION_ADV("CST3_StackF")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_StackM")
							CREATE_CONVERSATION_ADV("CST3_StackM")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			iCarHealth[CHASE_CAR_F620] = GET_ENTITY_HEALTH(vehPlayer)
			fCarGasTank[CHASE_CAR_F620] = GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer)
			fCarEngineHealth[CHASE_CAR_F620] = GET_VEHICLE_ENGINE_HEALTH(vehPlayer)
			
			iInterruptTimer = GET_GAME_TIMER() + 7500
		ENDIF
		
		IF GET_ENTITY_HEALTH(vehRival1) < iCarHealth[CHASE_CAR_ENTITYXF]
		OR GET_VEHICLE_PETROL_TANK_HEALTH(vehRival1) < fCarGasTank[CHASE_CAR_ENTITYXF]
		OR GET_VEHICLE_ENGINE_HEALTH(vehRival1) < fCarEngineHealth[CHASE_CAR_ENTITYXF]
			IF IS_ENTITY_ON_SCREEN(vehRival1)
			AND NOT IS_ENTITY_OCCLUDED(vehRival1)
				IF GET_GAME_TIMER() > iInterruptTimer
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_StackM")
							CREATE_CONVERSATION_ADV("CST3_StackM")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_StackT")
							CREATE_CONVERSATION_ADV("CST3_StackT")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			iCarHealth[CHASE_CAR_ENTITYXF] = GET_ENTITY_HEALTH(vehRival1)
			fCarGasTank[CHASE_CAR_ENTITYXF] = GET_VEHICLE_PETROL_TANK_HEALTH(vehRival1)
			fCarEngineHealth[CHASE_CAR_ENTITYXF] = GET_VEHICLE_ENGINE_HEALTH(vehRival1)
			
			iInterruptTimer = GET_GAME_TIMER() + 7500
		ENDIF
		
		IF GET_ENTITY_HEALTH(vehRival2) < iCarHealth[CHASE_CAR_CHEETAH]
		OR GET_VEHICLE_PETROL_TANK_HEALTH(vehRival2) < fCarGasTank[CHASE_CAR_CHEETAH]
		OR GET_VEHICLE_ENGINE_HEALTH(vehRival2) < fCarEngineHealth[CHASE_CAR_CHEETAH]
			IF IS_ENTITY_ON_SCREEN(vehRival2)
			AND NOT IS_ENTITY_OCCLUDED(vehRival2)
				IF GET_GAME_TIMER() > iInterruptTimer
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_StackT")
							CREATE_CONVERSATION_ADV("CST3_StackT")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_StackF")
							CREATE_CONVERSATION_ADV("CST3_StackF")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			iCarHealth[CHASE_CAR_CHEETAH] = GET_ENTITY_HEALTH(vehRival2)
			fCarGasTank[CHASE_CAR_CHEETAH] = GET_VEHICLE_PETROL_TANK_HEALTH(vehRival2)
			fCarEngineHealth[CHASE_CAR_CHEETAH] = GET_VEHICLE_ENGINE_HEALTH(vehRival2)
			
			iInterruptTimer = GET_GAME_TIMER() + 7500
		ENDIF
		
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF HAS_LABEL_BEEN_TRIGGERED("SwitchOnce")
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				IF bFranklinFinishFirst OR bFranklinFinishSecond
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_FinishF")
						IF bFranklinFinishFirst
							IF IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehPlayer)
							AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehRival1)
							AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_TREVOR), vehRival2)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), GET_ENTITY_COORDS(vehRival1)) < 100.0
								OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), GET_ENTITY_COORDS(vehRival2)) < 100.0
									CREATE_CONVERSATION_ADV("CST3_JUSTF")
								ELSE
									CREATE_CONVERSATION_ADV("CST3_FinishF")
								ENDIF
							ENDIF
						ENDIF
						SET_LABEL_AS_TRIGGERED("CST3_FinishF", TRUE)
					ENDIF
				ENDIF
				
				IF bTrevorFinishFirst OR bTrevorFinishSecond
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_FinishT")
						IF bTrevorFinishFirst
							IF IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehPlayer)
							AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehRival1)
							AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_TREVOR), vehRival2)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), GET_ENTITY_COORDS(vehPlayer)) < 100.0
								OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), GET_ENTITY_COORDS(vehRival1)) < 100.0
									CREATE_CONVERSATION_ADV("CST3_JUSTT")
								ELSE
									CREATE_CONVERSATION_ADV("CST3_FinishT")
								ENDIF
							ENDIF
						ENDIF
						SET_LABEL_AS_TRIGGERED("CST3_FinishT", TRUE)
					ENDIF
				ENDIF
				
				IF bMichaelFinishFirst OR bMichaelFinishSecond
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_FinishM")
						IF bMichaelFinishFirst
							IF IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehPlayer)
							AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehRival1)
							AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_TREVOR), vehRival2)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), GET_ENTITY_COORDS(vehPlayer)) < 100.0
								OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), GET_ENTITY_COORDS(vehRival2)) < 100.0
									CREATE_CONVERSATION_ADV("CST3_JUSTM")
								ELSE
									CREATE_CONVERSATION_ADV("CST3_FinishM")
								ENDIF
							ENDIF
						ENDIF
						SET_LABEL_AS_TRIGGERED("CST3_FinishM", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ((GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) != CHAR_FRANKLIN
		AND NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayer, <<509.486908, -1338.530884, 27.325581>>, <<475.685364, -1289.889771, 44.556633>>, 55.0))
		OR (GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) != CHAR_MICHAEL
		AND NOT IS_ENTITY_IN_ANGLED_AREA(vehRival1, <<509.486908, -1338.530884, 27.325581>>, <<475.685364, -1289.889771, 44.556633>>, 55.0))
		OR (GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) != CHAR_TREVOR
		AND NOT IS_ENTITY_IN_ANGLED_AREA(vehRival2, <<509.486908, -1338.530884, 27.325581>>, <<475.685364, -1289.889771, 44.556633>>, 55.0)))
			IF NOT sCamDetails.bRun
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchOnce")
					IF TIMERA() < 20000
						IF HAS_LABEL_BEEN_TRIGGERED("CST3_Return")
							PRINT_HELP_ADV("S3_RACEHOME")
						ENDIF
						
						IF UPDATE_SELECTOR_HUD(sSelectorPeds) //Returns TRUE when the player has made a selection
							IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								
								//Stats
								INFORM_MISSION_STATS_OF_INCREMENT(CS1_SWITCHES)
								
								sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
								sCamDetails.bRun  = TRUE
								
								vehSwitch = GET_VEHICLE_PED_IS_USING(sCamDetails.pedTo)
								
								vSwitchCoords = VECTOR_ZERO
								vSwitchRot = VECTOR_ZERO
								fSwitchSpeed = 0.0
								
								IF DOES_ENTITY_EXIST(vehSwitch)
								AND NOT IS_ENTITY_DEAD(vehSwitch)
									vSwitchCoords = GET_ENTITY_COORDS(vehSwitch)
									vSwitchRot = GET_ENTITY_ROTATION(vehSwitch)
									fSwitchSpeed = GET_ENTITY_SPEED(vehSwitch)
								ENDIF
								
								SET_LABEL_AS_TRIGGERED("SwitchOnce", TRUE)
							ENDIF
						ENDIF
					ELSE
						SET_LABEL_AS_TRIGGERED("SwitchOnce", TRUE)
					ENDIF
				ENDIF
			ELSE
				IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails)	//RUN_CAM_SPLINE_FROM_PLAYER_TO_PED_RACE(sCamDetails) //Returns FALSE when the camera spline is complete
					IF sCamDetails.bOKToSwitchPed
						IF NOT sCamDetails.bPedSwitched
							IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
								iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 10000)
								
								iGameTimer = GET_GAME_TIMER() + 5000
								
								PRINTLN("iGameTimer = ", iGameTimer)
								
								IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
									SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlayer, TRUE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival1, FALSE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival2, FALSE)
								ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
									SET_VEHICLE_ENGINE_ON(vehRival1, TRUE, TRUE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlayer, FALSE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival1, TRUE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival2, FALSE)
								ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
									SET_VEHICLE_ENGINE_ON(vehRival2, TRUE, TRUE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlayer, FALSE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival1, FALSE)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehRival2, TRUE)
								ENDIF
								
								sCamDetails.bPedSwitched = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT ARE_VECTORS_EQUAL(vSwitchCoords, VECTOR_ZERO)
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			AND GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
				SWITCH_STATE switchState = GET_PLAYER_SWITCH_STATE()
				
				PRINTLN("switchState = ", switchState)
				
				IF DOES_ENTITY_EXIST(vehSwitch)
				AND NOT IS_ENTITY_DEAD(vehSwitch)
					IF switchState > SWITCH_STATE_JUMPCUT_ASCENT
						CLEAR_AREA(vSwitchCoords, 15.0, TRUE)
						
						IF vehPlayer != vehSwitch
							IF IS_ENTITY_AT_COORD(vehPlayer, GET_ENTITY_COORDS(vehSwitch) - <<0.0, 0.0, 1.0>>, <<4.0, 4.0, 4.0>>)
								SET_ENTITY_COORDS(vehPlayer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSwitch, <<0.0, -10.0, 1.0>>))
								SET_ENTITY_ROTATION(vehPlayer, GET_ENTITY_ROTATION(vehPlayer))
								SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
							ENDIF
						ENDIF
						
						IF vehRival1 != vehSwitch
							IF IS_ENTITY_AT_COORD(vehRival1, GET_ENTITY_COORDS(vehSwitch) - <<0.0, 0.0, 1.0>>, <<4.0, 4.0, 4.0>>)
								SET_ENTITY_COORDS(vehRival1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSwitch, <<0.0, -10.0, 1.0>>))
								SET_ENTITY_ROTATION(vehRival1, GET_ENTITY_ROTATION(vehRival1))
								SET_VEHICLE_ON_GROUND_PROPERLY(vehRival1)
							ENDIF
						ENDIF
						
						IF vehRival2 != vehSwitch
							IF IS_ENTITY_AT_COORD(vehRival2, GET_ENTITY_COORDS(vehSwitch) - <<0.0, 0.0, 1.0>>, <<4.0, 4.0, 4.0>>)
								SET_ENTITY_COORDS(vehRival2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehSwitch, <<0.0, -10.0, 1.0>>))
								SET_ENTITY_ROTATION(vehRival2, GET_ENTITY_ROTATION(vehRival2))
								SET_VEHICLE_ON_GROUND_PROPERLY(vehRival2)
							ENDIF
						ENDIF
						
						SET_ENTITY_COORDS(vehSwitch, vSwitchCoords)
						SET_ENTITY_ROTATION(vehSwitch, vSwitchRot)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehSwitch)
						
						IF switchState > SWITCH_STATE_JUMPCUT_DESCENT
							SET_VEHICLE_FORWARD_SPEED(vehSwitch, fSwitchSpeed)
							
							vSwitchCoords = VECTOR_ZERO
							vSwitchRot = VECTOR_ZERO
							fSwitchSpeed = 0.0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Helmet
		IF NOT HAS_LABEL_BEEN_TRIGGERED("MichaelHelmet")
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_MICHAEL))
				CLEAR_PED_PROP(PLAYER_PED(CHAR_MICHAEL), ANCHOR_HEAD)
				REMOVE_PED_HELMET(PLAYER_PED(CHAR_MICHAEL), TRUE)
				SET_LABEL_AS_TRIGGERED("MichaelHelmet", TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TrevorHelmet")
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_TREVOR))
				CLEAR_PED_PROP(PLAYER_PED(CHAR_TREVOR), ANCHOR_HEAD)
				REMOVE_PED_HELMET(PLAYER_PED(CHAR_TREVOR), TRUE)
				SET_LABEL_AS_TRIGGERED("TrevorHelmet", TRUE)
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehRival1)
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival2)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehRival2)
		ENDIF
		
		//Overtaking
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			IF bWaitForPlayer = FALSE
				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
						IF GET_GAME_TIMER() > iDialogueTimer[0]
							IF IS_ENTITY_IN_ANGLED_AREA(vehPlayer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival1, <<-10.0, 0.0, -5.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival1, <<10.0, 10.0, 5.0>>), 10.0)
							OR IS_ENTITY_IN_ANGLED_AREA(vehPlayer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival2, <<-10.0, 0.0, -5.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival2, <<10.0, 10.0, 5.0>>), 10.0)
								iDialogueStage = 0
								
								IF iDialogueLineCount[iDialogueStage] = -1 
									 iDialogueLineCount[iDialogueStage] = 3
								ELIF iDialogueLineCount[iDialogueStage] > 0
									CREATE_CONVERSATION_ADV("CST3_OverF", CONV_PRIORITY_MEDIUM, FALSE)
									
									iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(15000, 25000)
									
									iDialogueLineCount[iDialogueStage]--
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
						IF GET_GAME_TIMER() > iDialogueTimer[0]
							IF IS_ENTITY_IN_ANGLED_AREA(vehRival1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayer, <<-10.0, 0.0, -5.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayer, <<10.0, 10.0, 5.0>>), 10.0)
							OR IS_ENTITY_IN_ANGLED_AREA(vehRival1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival2, <<-10.0, 0.0, -5.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival2, <<10.0, 10.0, 5.0>>), 10.0)
								iDialogueStage = 1
								
								IF iDialogueLineCount[iDialogueStage] = -1 
									 iDialogueLineCount[iDialogueStage] = 3
								ELIF iDialogueLineCount[iDialogueStage] > 0
									CREATE_CONVERSATION_ADV("CST3_OverM", CONV_PRIORITY_MEDIUM, FALSE)
									
									iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(15000, 25000)
									
									iDialogueLineCount[iDialogueStage]--
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival2)
						IF GET_GAME_TIMER() > iDialogueTimer[0]
							IF IS_ENTITY_IN_ANGLED_AREA(vehRival2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayer, <<-10.0, 0.0, -5.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayer, <<10.0, 10.0, 5.0>>), 10.0)
							OR IS_ENTITY_IN_ANGLED_AREA(vehRival2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival1, <<-10.0, 0.0, -5.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehRival1, <<10.0, 10.0, 5.0>>), 10.0)
								iDialogueStage = 2
								
								IF iDialogueLineCount[iDialogueStage] = -1 
									 iDialogueLineCount[iDialogueStage] = 3
								ELIF iDialogueLineCount[iDialogueStage] > 0
									CREATE_CONVERSATION_ADV("CST3_OverT", CONV_PRIORITY_MEDIUM, FALSE)
									
									iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(15000, 25000)
									
									iDialogueLineCount[iDialogueStage]--
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Far Ahead
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			IF HAS_LABEL_BEEN_TRIGGERED("SwitchOnce")
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_ShitM")
				OR NOT HAS_LABEL_BEEN_TRIGGERED("CST3_ShitF")
				OR NOT HAS_LABEL_BEEN_TRIGGERED("CST3_ShitT")
					IF TIMERA() > 60000
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_FR_M")
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<492.937805, -1317.739258, 29.251940 - 1.0>>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<492.937805, -1317.739258, 29.251940 - 1.0>>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
								CREATE_CONVERSATION_ADV("CST3_FR_M")
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_CATCHF")
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<492.937805, -1317.739258, 29.251940 - 1.0>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
									CREATE_CONVERSATION_ADV("CST3_CATCHF")
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_CATCHT")
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<492.937805, -1317.739258, 29.251940 - 1.0>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
									CREATE_CONVERSATION_ADV("CST3_CATCHT")
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_FR_F")
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<492.937805, -1317.739258, 29.251940 - 1.0>>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<492.937805, -1317.739258, 29.251940 - 1.0>>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
								CREATE_CONVERSATION_ADV("CST3_FR_F")
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_CATCHM")
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<492.937805, -1317.739258, 29.251940 - 1.0>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
									CREATE_CONVERSATION_ADV("CST3_CATCHM")
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_CATCHT")
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<492.937805, -1317.739258, 29.251940 - 1.0>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
									CREATE_CONVERSATION_ADV("CST3_CATCHT")
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_FR_T")
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<492.937805, -1317.739258, 29.251940 - 1.0>>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<492.937805, -1317.739258, 29.251940 - 1.0>>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
								CREATE_CONVERSATION_ADV("CST3_FR_T")
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_CATCHM")
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<492.937805, -1317.739258, 29.251940 - 1.0>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival1), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
									CREATE_CONVERSATION_ADV("CST3_CATCHM")
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_CATCHF")
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehRival2), <<492.937805, -1317.739258, 29.251940 - 1.0>>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayer), <<492.937805, -1317.739258, 29.251940 - 1.0>>) - 300.0
									CREATE_CONVERSATION_ADV("CST3_CATCHF")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Ramp
		IF NOT DOES_ENTITY_EXIST(objRamp)
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-2575.948730, 3186.570313, 71.914772>>, <<200.0, 200.0, 50.0>>)
				IF HAS_MODEL_LOADED_CHECK(PROP_MP_RAMP_03)
					objRamp = CREATE_OBJECT_NO_OFFSET(PROP_MP_RAMP_03, <<-2589.899902, 3126.100098, 32.580002>>)
					SET_ENTITY_ROTATION(objRamp, <<0.0, 0.0, 170.739975>>)
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_MP_RAMP_03)
				ENDIF
			ENDIF
		ENDIF
		
		//Keep Player in the correct car
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
		OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival2)
		OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			IF bWaitForPlayer
				VEHICLE_INDEX vehUsing = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				
				IF (DOES_ENTITY_EXIST(vehUsing)
				AND NOT IS_ENTITY_DEAD(vehUsing)
				AND GET_ENTITY_SPEED(vehUsing) > 5.0)
				OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1908.292236, 4619.039551, 55.051991>>, <<-1924.485962, 4602.391602, 60.057453>>, 10.0)
					bWaitForPlayer = FALSE
				ENDIF
			ENDIF
			
			VECTOR vRacerChase
			VECTOR vBlipChase
			
			FLOAT fModifier
			
			IF GET_BLIP_FROM_ENTITY(vehPlayer) = blipPlayer
				SAFE_REMOVE_BLIP(blipPlayer)
			ENDIF
			
			IF GET_BLIP_FROM_ENTITY(vehRival1) = blipRival1
				SAFE_REMOVE_BLIP(blipRival1)
			ENDIF
			
			IF GET_BLIP_FROM_ENTITY(vehRival2) = blipRival2
				SAFE_REMOVE_BLIP(blipRival2)
			ENDIF
			
			IF IS_PAUSE_MENU_ACTIVE()
				IF DOES_BLIP_EXIST(blipPlayer)
					SET_BLIP_COORDS(blipPlayer, GET_ENTITY_COORDS(vehPlayer))
				ENDIF
				
				IF DOES_BLIP_EXIST(blipRival1)
					SET_BLIP_COORDS(blipRival1, GET_ENTITY_COORDS(vehRival1))
				ENDIF
				
				IF DOES_BLIP_EXIST(blipRival2)
					SET_BLIP_COORDS(blipRival2, GET_ENTITY_COORDS(vehRival2))
				ENDIF
			ENDIF
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
				//Mix Group
				IF DOES_ENTITY_EXIST(vehPlayer)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPlayer)
				ENDIF
				IF DOES_ENTITY_EXIST(vehRival2)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival2)
				ENDIF
				
				SAFE_REMOVE_BLIP(blipRival1)
				
				IF NOT DOES_BLIP_EXIST(blipRival2)
					blipRival2 = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehRival2))
					SET_BLIP_COLOUR(blipRival2, BLIP_COLOUR_BLUE)
					SET_BLIP_PRIORITY(blipRival2, BLIPPRIORITY_HIGHEST)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipRival2, "S3_BLIPVEH")					
				ENDIF
				
				vRacerChase = GET_ENTITY_COORDS(vehRival2)
				vBlipChase = GET_BLIP_COORDS(blipRival2)
				
				fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) / 4), 0.0, 17.5)
				
				vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
				vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
				vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
				
				SET_BLIP_COORDS(blipRival2, vBlipChase)
				
				IF NOT DOES_BLIP_EXIST(blipPlayer)
					blipPlayer = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehPlayer))
					SET_BLIP_COLOUR(blipPlayer, BLIP_COLOUR_BLUE)
					SET_BLIP_PRIORITY(blipPlayer, BLIPPRIORITY_HIGHEST)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipPlayer, "S3_BLIPVEH")
				ENDIF
				
				vRacerChase = GET_ENTITY_COORDS(vehPlayer)
				vBlipChase = GET_BLIP_COORDS(blipPlayer)
				
				fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayer)) / 4), 0.0, 17.5)
				
				vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
				vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
				vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
				
				SET_BLIP_COORDS(blipPlayer, vBlipChase)
			ELIF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival2)
				//Mix Group
				IF DOES_ENTITY_EXIST(vehPlayer)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPlayer)
				ENDIF
				IF DOES_ENTITY_EXIST(vehRival1)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival1)
				ENDIF
				SAFE_REMOVE_BLIP(blipRival2)
				
				IF NOT DOES_BLIP_EXIST(blipRival1)
					blipRival1 = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehRival1))
					SET_BLIP_COLOUR(blipRival1, BLIP_COLOUR_BLUE)
					SET_BLIP_PRIORITY(blipRival1, BLIPPRIORITY_HIGHEST)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipRival1, "S3_BLIPVEH")
				ENDIF
				
				vRacerChase = GET_ENTITY_COORDS(vehRival1)
				vBlipChase = GET_BLIP_COORDS(blipRival1)
				
				fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) / 4), 0.0, 17.5)
				
				vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
				vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
				vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
				
				SET_BLIP_COORDS(blipRival1, vBlipChase)
				
				IF NOT DOES_BLIP_EXIST(blipPlayer)
					blipPlayer = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehPlayer))
					SET_BLIP_COLOUR(blipPlayer, BLIP_COLOUR_BLUE)
					SET_BLIP_PRIORITY(blipPlayer, BLIPPRIORITY_HIGHEST)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipPlayer, "S3_BLIPVEH")
				ENDIF
				
				vRacerChase = GET_ENTITY_COORDS(vehPlayer)
				vBlipChase = GET_BLIP_COORDS(blipPlayer)
				
				fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayer)) / 4), 0.0, 17.5)
				
				vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
				vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
				vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
				
				SET_BLIP_COORDS(blipPlayer, vBlipChase)
			ELIF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
				//Mix Group
				IF DOES_ENTITY_EXIST(vehRival1)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival1)
				ENDIF
				IF DOES_ENTITY_EXIST(vehRival2)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival2)
				ENDIF
				
				SAFE_REMOVE_BLIP(blipPlayer)
				
				IF NOT DOES_BLIP_EXIST(blipRival1)
					blipRival1 = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehRival1))
					SET_BLIP_COLOUR(blipRival1, BLIP_COLOUR_BLUE)
					SET_BLIP_PRIORITY(blipRival1, BLIPPRIORITY_HIGHEST)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipRival1, "S3_BLIPVEH")
				ENDIF
				
				vRacerChase = GET_ENTITY_COORDS(vehRival1)
				vBlipChase = GET_BLIP_COORDS(blipRival1)
				
				fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) / 4), 0.0, 17.5)
				
				vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
				vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
				vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
				
				SET_BLIP_COORDS(blipRival1, vBlipChase)
				
				IF NOT DOES_BLIP_EXIST(blipRival2)
					blipRival2 = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehRival2))
					SET_BLIP_COLOUR(blipRival2, BLIP_COLOUR_BLUE)
					SET_BLIP_PRIORITY(blipRival2, BLIPPRIORITY_HIGHEST)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipRival2, "S3_BLIPVEH")
				ENDIF
				
				vRacerChase = GET_ENTITY_COORDS(vehRival2)
				vBlipChase = GET_BLIP_COORDS(blipRival2)
				
				fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) / 4), 0.0, 17.5)
				
				vBlipChase.X = vBlipChase.X +@ (((vRacerChase.X - vBlipChase.X) / fModifier) * 15)
				vBlipChase.Y = vBlipChase.Y +@ (((vRacerChase.Y - vBlipChase.Y) / fModifier) * 15)
				vBlipChase.Z = vBlipChase.Z +@ (((vRacerChase.Z - vBlipChase.Z) / fModifier) * 15)
				
				SET_BLIP_COORDS(blipRival2, vBlipChase)
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(blipGarage)
				CLEAR_PRINTS()
				PRINT_ADV("S3_DRIVE")
				SAFE_ADD_BLIP_LOCATION(blipGarage, vGarage, TRUE)
			ENDIF
			
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_Return")
					CREATE_CONVERSATION_ADV("CST3_Return")
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CST3_RaceM")
				AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_RaceF")
				AND NOT HAS_LABEL_BEEN_TRIGGERED("CST3_RaceT")
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CREATE_CONVERSATION_ADV("CST3_RaceM")
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						CREATE_CONVERSATION_ADV("CST3_RaceF")
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CREATE_CONVERSATION_ADV("CST3_RaceT")
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<492.937805, -1317.739258, 29.251940 - 1.0>>) <= 50.0
				HAS_MODEL_LOADED_CHECK(FELON)
				HAS_MODEL_LOADED_CHECK(S_M_Y_DWSERVICE_02)
				HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_MOLLY))
				HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_DEVIN))
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("svolCarStealOutro")
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<492.937805, -1317.739258, 29.251940 - 1.0>>) <= 250.0
					svolCarStealOutro = STREAMVOL_CREATE_FRUSTUM(<<498.099976, -1334.323486, 31.278627>>, NORMALISE_VECTOR(<<-0.022675, 0.266968, -0.017954>>), 300.0, FLAG_MAPDATA)
					//source <<498.100739,-1334.334229,31.117319>>
					//dest <<498.078064,-1334.067261,31.099365>>
					
					//Pin Interior
					IF bPinnedChopShop = FALSE
						intChopShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<481.1439, -1314.4697, 28.2017>>, "v_chopshop")
						
						PIN_INTERIOR_IN_MEMORY(intChopShop)
					ENDIF
										
					SET_LABEL_AS_TRIGGERED("svolCarStealOutro", TRUE)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF NOT STREAMVOL_HAS_LOADED(svolCarStealOutro)
					PRINTLN("Waiting for STREAMVOL_HAS_LOADED(svolCarStealOutro) ...")
				ENDIF
				
				IF NOT IS_INTERIOR_READY(intChopShop)
					PRINTLN("PINNING INTERIOR...")
				ENDIF
				#ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("car_1_ext_concat")
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<492.937805, -1317.739258, 29.251940 - 1.0>>) < DEFAULT_CUTSCENE_LOAD_DIST * 2
					REQUEST_CUTSCENE("car_1_ext_concat")
					
					SET_LABEL_AS_TRIGGERED("car_1_ext_concat", TRUE)
				ENDIF
			ELSE
				//Request Cutscene Variations - car_1_ext_concat
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED(CHAR_TREVOR))
					SET_CUTSCENE_PED_PROP_VARIATION("MOLLY", ANCHOR_EYES, 0, 0)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<492.937805, -1317.739258, 29.251940 - 1.0>>, <<0.0, 0.0, LOCATE_SIZE_HEIGHT>>, TRUE)
			ENDIF
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<501.685455, -1324.219482, 27.310808>>, <<488.542847, -1304.170776, 32.355415>>, 20.0)
				ADVANCE_STAGE()
			ENDIF
			
			SET_LABEL_AS_TRIGGERED("GetInCar", FALSE)
		ELSE
			IF bWaitForPlayer
				SETTIMERA(0)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipGarage)
				CLEAR_PRINTS()
				SAFE_REMOVE_BLIP(blipGarage)
			ENDIF
			
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("GetInCar")
					IF NOT HAS_LABEL_BEEN_TRIGGERED("CMN_GENGETIN")
						SET_LABEL_AS_TRIGGERED("CMN_GENGETIN", TRUE)	//PRINT_ADV("CMN_GENGETIN")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("CMN_GENGETBCK")
						PRINT_ADV("CMN_GENGETBCK")
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("GetInCar", TRUE)
				ENDIF
			ENDIF
			
			IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
				SAFE_ADD_BLIP_VEHICLE(blipRival1, vehRival1, FALSE)
				SAFE_REMOVE_BLIP(blipRival2)
				SAFE_REMOVE_BLIP(blipPlayer)
			ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
				SAFE_ADD_BLIP_VEHICLE(blipRival2, vehRival2, FALSE)
				SAFE_REMOVE_BLIP(blipRival1)
				SAFE_REMOVE_BLIP(blipPlayer)
			ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
				SAFE_ADD_BLIP_VEHICLE(blipPlayer, vehPlayer, FALSE)
				SAFE_REMOVE_BLIP(blipRival1)
				SAFE_REMOVE_BLIP(blipRival2)
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayer)) >= 75
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival1)) >= 75
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehRival2)) >= 75
				eMissionFail = failAbandonedCar
				
				missionFailed()
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_GET_TO_GARAGE")
			STOP_AUDIO_SCENE("CAR_1_GET_TO_GARAGE")
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehPlayer)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPlayer)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehRival1)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival1)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehRival2)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehRival2)
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehRival1)
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehRival2)
		ENDIF
		
//		//Franklin
//		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
//			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//				EMPTY_VEHICLE_OF_PEDS(vehPlayer)
//				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer)
//			ENDIF
//		ELSE
//			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
//				EMPTY_VEHICLE_OF_PEDS(vehPlayer)
//				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
//				TASK_LEAVE_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//			ENDIF
//		ENDIF
//		
//		IF NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayer, <<475.399536, -1320.267090, 27.207529>>, <<492.851624, -1311.241821, 31.264484>>, 10.0)
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
//				STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
//			ENDIF
//			
//			SET_VEHICLE_POSITION(vehPlayer, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrecHome), sCarrecHome), GET_HEADING_BETWEEN_VECTORS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrecHome) / 1000 * 999), sCarrecHome), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrecHome), sCarrecHome)))
//		ENDIF
//		
//		//Trevor
//		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
//			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival2)
//				EMPTY_VEHICLE_OF_PEDS(vehRival2)
//				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival2)
//			ENDIF
//		ELSE
//			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2)
//				EMPTY_VEHICLE_OF_PEDS(vehRival2)
//				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2)
//				TASK_LEAVE_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//			ENDIF
//		ENDIF
//		
//		IF NOT IS_ENTITY_IN_ANGLED_AREA(vehRival2, <<475.399536, -1320.267090, 27.207529>>, <<492.851624, -1311.241821, 31.264484>>, 10.0)
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
//				STOP_PLAYBACK_RECORDED_VEHICLE(vehRival2)
//			ENDIF
//			
//			SET_VEHICLE_POSITION(vehRival2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecHome), sCarrecHome), GET_HEADING_BETWEEN_VECTORS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecHome) / 1000 * 999), sCarrecHome), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecHome), sCarrecHome)))
//		ENDIF
//		
//		//Michael
//		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
//			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
//				EMPTY_VEHICLE_OF_PEDS(vehRival1)
//				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival1)
//			ENDIF
//		ELSE
//			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehRival1)
//				EMPTY_VEHICLE_OF_PEDS(vehRival1)
//				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehRival1)
//				TASK_LEAVE_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//			ENDIF
//		ENDIF
//		
//		IF NOT IS_ENTITY_IN_ANGLED_AREA(vehRival1, <<475.399536, -1320.267090, 27.207529>>, <<492.851624, -1311.241821, 31.264484>>, 10.0)
//			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
//				STOP_PLAYBACK_RECORDED_VEHICLE(vehRival1)
//			ENDIF
//			
//			SET_VEHICLE_POSITION(vehRival1, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecHome), sCarrecHome), GET_HEADING_BETWEEN_VECTORS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecHome) / 1000 * 999), sCarrecHome), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecHome), sCarrecHome)))
//		ENDIF
		
		SAFE_DELETE_PED(pedRival1)
		SAFE_DELETE_PED(pedRival2)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC CutsceneHome()
	IF INIT_STAGE()
		//Radar
		bRadar = FALSE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cutscene
		REQUEST_CUTSCENE("car_1_ext_concat")
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("CAR_1_GARAGE_ARRIVAL")
			START_AUDIO_SCENE("CAR_1_GARAGE_ARRIVAL")
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_HEAD)
			
			SET_PED_POSITION(PLAYER_PED_ID(), <<485.9252, -1330.5171, 28.2488>>, 293.9519, FALSE)
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], <<486.2881, -1331.0845, 28.2850>>, 296.1121, FALSE)
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<485.5920, -1330.7214, 28.2454>>, 296.1122, FALSE)
			
			//SET_VEHICLE_FIXED(vehPlayer)
			//SET_VEHICLE_FIXED(vehRival1)
			//SET_VEHICLE_FIXED(vehRival2)
			
			SET_VEHICLE_POSITION(vehPlayer, <<485.3990, -1332.9005, 28.3095>>, 292.4873)
			SET_VEHICLE_POSITION(vehRival1, <<485.9952, -1335.4487, 28.2862>>, 286.6455)
			SET_VEHICLE_POSITION(vehRival2, <<487.1696, -1337.8451, 28.2688>>, 293.0351)
			
			FREEZE_ENTITY_POSITION(vehPlayer, TRUE)
			FREEZE_ENTITY_POSITION(vehRival1, TRUE)
			FREEZE_ENTITY_POSITION(vehRival2, TRUE)
			
			//Pin Interior
			IF bPinnedChopShop = FALSE
				intChopShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<481.1439, -1314.4697, 28.2017>>, "v_chopshop")
				
				PIN_INTERIOR_IN_MEMORY(intChopShop)
				
				WHILE NOT IS_INTERIOR_READY(intChopShop)
					PRINTLN("PINNING INTERIOR...")
					
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				bPinnedChopShop = TRUE
			ENDIF
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			
			FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
			FREEZE_ENTITY_POSITION(vehRival1, FALSE)
			FREEZE_ENTITY_POSITION(vehRival2, FALSE)
			
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehRival1)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehRival2)
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehRival1)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival1)
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehPlayer)
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehRival2)
			ENDIF
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - car_1_ext_concat
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED(CHAR_TREVOR))
			SET_CUTSCENE_PED_PROP_VARIATION("MOLLY", ANCHOR_EYES, 0, 0)
		ENDIF
		
		//Traffic
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
		FLOAT fOpenRatio = 1.0
		
		IF iCutsceneStage < 4
			IF (NOT IS_PED_INJURED(pedWorker1) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWorker1))
			AND (NOT IS_PED_INJURED(pedWorker2) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWorker2))
			AND (NOT IS_PED_INJURED(pedMolly) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedMolly))
			AND (NOT IS_PED_INJURED(pedDevin) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDevin))
				fOpenRatio = 0.5
				
				SETTIMERB(0)
				
				SET_LABEL_AS_TRIGGERED("WaitingForPeds", FALSE)
			ELSE
				fOpenRatio = 0.0
				
				SETTIMERB(0)
				
				SET_LABEL_AS_TRIGGERED("WaitingForPeds", TRUE)
			ENDIF
		ELIF iCutsceneStage = 4
			IF (NOT IS_PED_INJURED(pedWorker1) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWorker1))
			AND (NOT IS_PED_INJURED(pedWorker2) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWorker2))
			AND (NOT IS_PED_INJURED(pedMolly) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedMolly))
			AND (NOT IS_PED_INJURED(pedDevin) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDevin))
				IF NOT HAS_LABEL_BEEN_TRIGGERED("WaitingForPeds")
					fOpenRatio = 0.5 + GET_INTERP_POINT_FLOAT(0.0, 0.5, 0, 3000.0, TO_FLOAT(TIMERA()))
				ELSE
					fOpenRatio = 0.0 + GET_INTERP_POINT_FLOAT(0.0, 1.0, 0, 6000.0, TO_FLOAT(TIMERB()))
				ENDIF
			ELSE
				fOpenRatio = 0.0
				
				SETTIMERB(0)
				
				SET_LABEL_AS_TRIGGERED("WaitingForPeds", TRUE)
			ENDIF
		ELSE
//			IF NOT HAS_LABEL_BEEN_TRIGGERED("RemoveCopUniform")
//				fOpenRatio = 1.0
//			ELSE
//				fOpenRatio = 0.0
//			ENDIF
			
			fOpenRatio = 1.0
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID)
			ADD_DOOR_TO_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].model, g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].coords)
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID)
			IF DOOR_SYSTEM_GET_OPEN_RATIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID) <> fOpenRatio
			OR DOOR_SYSTEM_GET_DOOR_STATE(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
				DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, fOpenRatio, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iSlidingGarageDoor)
			ADD_DOOR_TO_SYSTEM(iSlidingGarageDoor, PROP_SC1_06_GATE_R, <<500.1759, -1320.5450, 28.2499>>)
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iSlidingGarageDoor)
			IF DOOR_SYSTEM_GET_OPEN_RATIO(iSlidingGarageDoor) <> -1.0
			OR DOOR_SYSTEM_GET_DOOR_STATE(iSlidingGarageDoor) != DOORSTATE_FORCE_OPEN_THIS_FRAME
				DOOR_SYSTEM_SET_OPEN_RATIO(iSlidingGarageDoor, -1.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(iSlidingGarageDoor, DOORSTATE_FORCE_OPEN_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
		
		BOOL bPausedMichael, bPausedTrevor, bPausedFranklin
		
		SWITCH iCutsceneStage
			CASE 0
				IF NOT HAS_LABEL_BEEN_TRIGGERED("svolCarStealOutro")
					svolCarStealOutro = STREAMVOL_CREATE_FRUSTUM(<<498.099976, -1334.323486, 31.278627>>, NORMALISE_VECTOR(<<-0.022675, 0.266968, -0.017954>>), 300.0, FLAG_MAPDATA)
					//source <<498.100739,-1334.334229,31.117319>>
					//dest <<498.078064,-1334.067261,31.099365>>
					SET_LABEL_AS_TRIGGERED("svolCarStealOutro", TRUE)
				ENDIF
				
				//Pin Interior
				IF bPinnedChopShop = FALSE
					intChopShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<481.1439, -1314.4697, 28.2017>>, "v_chopshop")
					
					PIN_INTERIOR_IN_MEMORY(intChopShop)
				ENDIF
				
				iDialogueTimer[0] = GET_GAME_TIMER() + 20000	//Saving variables by reusing iDialogueTimer[0]
				
				ADVANCE_CUTSCENE()
			BREAK
			CASE 1
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<482.356537, -1307.464233, 27.706671>>, <<497.543457, -1328.467651, 33.341789>>, 10.0)
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF NOT STREAMVOL_HAS_LOADED(svolCarStealOutro)
					PRINTLN("Waiting for STREAMVOL_HAS_LOADED(svolCarStealOutro) ...")
				ENDIF
				
				IF NOT IS_INTERIOR_READY(intChopShop)
					PRINTLN("PINNING INTERIOR...")
				ENDIF
				
				PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iDialogueTimer[0])
				#ENDIF
				
//				IF STREAMVOL_HAS_LOADED(svolCarStealOutro)
//				OR GET_GAME_TIMER() > iDialogueTimer[0]
					#IF IS_DEBUG_BUILD
//					IF GET_GAME_TIMER() > iDialogueTimer[0]
						IF NOT STREAMVOL_HAS_LOADED(svolCarStealOutro)
							PRINTLN("STREAMVOL_HAS_LOADED(svolCarStealOutro) did not load in time.")
//							SCRIPT_ASSERT("STREAMVOL_HAS_LOADED(svolCarStealOutro) did not load in time.")	//timed out, see log for details.")
						ENDIF
//					ENDIF
					#ENDIF
					
					//F620 - 003
					//003: 0.0
					//002: 0.275
					//001: 0.2
					//
					//ENTITYXF - 001
					//003: -0.150
					//002: 0.100
					//001: 0.0
					//
					//CHEETAH - 002
					//003: -0.225
					//002: 0.0
					//001: -0.050
					
					bPinnedChopShop = TRUE
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					
					FLOAT fCurrentDist, fTotalDist, fTotalTime, fTimeCap, fSetTime
					
					VECTOR vFranklinPos, vMichaelPos
					
					VEHICLE_INDEX vehIndex
					
					//Franklin - 003 - Player pulls in
					vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
					PRINTLN("======== Franklin - 003 ========")
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehIndex), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrecGarage), sCarrecGarage))
					fTotalDist = GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, 0.0, sCarrecGarage), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrecGarage), sCarrecGarage))
					fTotalTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrecGarage)
					PRINTLN("fCurrentDist = ", fCurrentDist)
					PRINTLN("fTotalDist = ", fTotalDist)
					PRINTLN("fTotalTime = ", fTotalTime)
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex)
						fTimeCap = 80.0
					ELSE
						fTimeCap = 100.0
					ENDIF
					PRINTLN("fTimeCap = ", fTimeCap)
					
					fSetTime = CLAMP((fTotalTime / fTotalDist) * (fTotalDist - fCurrentDist), 0.0, (fTotalTime / 100.0) * fTimeCap)
					PRINTLN("fSetTime = ", fSetTime)
					
					START_PLAYBACK_RECORDED_VEHICLE(vehIndex, 003, sCarrecGarage)
					
					IF GET_ENTITY_MODEL(vehIndex) = CHEETAH
						SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehIndex, <<0.0, 0.0, -0.225>>)
					ELIF GET_ENTITY_MODEL(vehIndex) = ENTITYXF
						SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehIndex, <<0.0, 0.0, -0.150>>)
					ENDIF
					
					IF fSetTime < (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrecGarage) / 100.0) * 40
						fSetTime = (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrecGarage) / 100.0) * 40
					ENDIF
					
					SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehIndex, fSetTime)
					
					vFranklinPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, fSetTime, sCarrecGarage)
					PRINTLN("================================")
					
					//Michael - 001 - Buddy comes next
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
						vehIndex = vehPlayer
					ELIF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
						vehIndex = vehRival1
					ENDIF
					
					PRINTLN("======== Michael - 001 =========")
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehIndex), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecGarage), sCarrecGarage))
					fTotalDist = GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, 0.0, sCarrecGarage), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecGarage), sCarrecGarage))
					fTotalTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecGarage)
					PRINTLN("fCurrentDist = ", fCurrentDist)
					PRINTLN("fTotalDist = ", fTotalDist)
					PRINTLN("fTotalTime = ", fTotalTime)
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex)
						fTimeCap = 80.0
					ELSE
						fTimeCap = 100.0
					ENDIF
					PRINTLN("fTimeCap = ", fTimeCap)
					
					fSetTime = CLAMP((fTotalTime / fTotalDist) * (fTotalDist - fCurrentDist), 0.0, (fTotalTime / 100.0) * fTimeCap)
					PRINTLN("fSetTime = ", fSetTime)
					
					//Michael must be further than 5.0 metres from Franklin (unless it's the player!)
					IF fSetTime < (fTotalTime / 100.0) * 95.0
						IF GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, fSetTime, sCarrecGarage), vFranklinPos) <= 10.0
						AND GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, fSetTime, sCarrecGarage), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecGarage), sCarrecGarage)) > 5.0
							fSetTime = CLAMP(((fTotalTime / fTotalDist) * (fTotalDist - fCurrentDist)) - 1500.0, 0.0, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecGarage) / 100.0) * fTimeCap)
							PRINTLN("fSetTime = ", fSetTime)
						ENDIF
					ENDIF
					
					START_PLAYBACK_RECORDED_VEHICLE(vehIndex, 001, sCarrecGarage)
					
					IF GET_ENTITY_MODEL(vehIndex) = F620
						SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehIndex, <<0.0, 0.0, 0.2>>)
					ELIF GET_ENTITY_MODEL(vehIndex) = CHEETAH
						SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehIndex, <<0.0, 0.0, -0.050>>)
					ENDIF
					
					IF fSetTime < (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecGarage) / 100.0) * 40
						fSetTime = (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, sCarrecGarage) / 100.0) * 40
					ENDIF
					
					SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehIndex, fSetTime)
					
					vMichaelPos = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, fSetTime, sCarrecGarage)
					PRINTLN("================================")
					
					//Trevor - 002 - Last buddy
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						vehIndex = vehRival2
					ELIF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
						vehIndex = vehRival1
					ENDIF
					
					PRINTLN("========= Trevor - 002 =========")
					fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehIndex), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage), sCarrecGarage))
					fTotalDist = GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, 0.0, sCarrecGarage), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage), sCarrecGarage))
					fTotalTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage)
					PRINTLN("fCurrentDist = ", fCurrentDist)
					PRINTLN("fTotalDist = ", fTotalDist)
					PRINTLN("fTotalTime = ", fTotalTime)
					
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex)
						fTimeCap = 80.0
					ELSE
						fTimeCap = 100.0
					ENDIF
					PRINTLN("fTimeCap = ", fTimeCap)
					
					fSetTime = CLAMP((fTotalTime / fTotalDist) * (fTotalDist - fCurrentDist), 0.0, (fTotalTime / 100.0) * fTimeCap)
					PRINTLN("fSetTime = ", fSetTime)
					
					//Trevor must be further than 5.0 metres from Franklin (unless it's the player!)
					IF fSetTime < (fTotalTime / 100.0) * 95.0
						IF GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, fSetTime, sCarrecGarage), vFranklinPos) <= 10.0
						AND GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, fSetTime, sCarrecGarage), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage), sCarrecGarage)) > 5.0
							fSetTime = CLAMP(((fTotalTime / fTotalDist) * (fTotalDist - fCurrentDist)) - 1500.0, 0.0, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage) / 100.0) * fTimeCap)
							PRINTLN("fSetTime = ", fSetTime)
						ENDIF
					ENDIF
					
					//Trevor must be further than 5.0 metres from Michael (unless it's the player!)
					IF fSetTime < (fTotalTime / 100.0) * 95.0
						IF GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, fSetTime, sCarrecGarage), vMichaelPos) <= 10.0
						AND GET_DISTANCE_BETWEEN_COORDS(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, fSetTime, sCarrecGarage), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(002, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage), sCarrecGarage)) > 5.0
							fSetTime = CLAMP(((fTotalTime / fTotalDist) * (fTotalDist - fCurrentDist)) - 1500.0, 0.0, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage) / 100.0) * fTimeCap)
							PRINTLN("fSetTime = ", fSetTime)
						ENDIF
					ENDIF
					
					START_PLAYBACK_RECORDED_VEHICLE(vehIndex, 002, sCarrecGarage)
					
					IF GET_ENTITY_MODEL(vehIndex) = F620
						SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehIndex, <<0.0, 0.0, 0.275>>)
					ELIF GET_ENTITY_MODEL(vehIndex) = ENTITYXF
						SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehIndex, <<0.0, 0.0, 0.100>>)
					ENDIF
					
					IF fSetTime < (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage) / 100.0) * 40
						fSetTime = (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(002, sCarrecGarage) / 100.0) * 40
					ENDIF
					
					SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehIndex, fSetTime)
					PRINTLN("================================")
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_MICHAEL), TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_TREVOR), TRUE)
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehPlayer)
						SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehPlayer)
					ENDIF
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehRival1)
						SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehRival1)
					ENDIF
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_TREVOR), vehRival2)
						SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_TREVOR), vehRival2)
					ENDIF
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					CLEAR_AREA(<<485.3990, -1332.9005, 28.3095>>, 500.0, TRUE)
					
					IF NOT DOES_CAM_EXIST(camMain)
						camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					ENDIF
					
					INT iCameraTime
					
					iCameraTime = 0
					
					vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehIndex)
						IF ROUND(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehIndex)) - GET_TIME_POSITION_IN_RECORDING(vehIndex)) > iCameraTime
							iCameraTime = ROUND(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehIndex)) - GET_TIME_POSITION_IN_RECORDING(vehIndex))
						ENDIF
					ENDIF
					
					SET_CAM_PARAMS(camMain, <<490.698425, -1337.069702, 30.857084>>, <<-0.619130, 0.022362, -15.042570>>, 25.977287)
					SET_CAM_PARAMS(camMain, <<491.126160, -1337.160400, 30.104757>>, <<-0.619130, 0.022362, -16.546909>>, 25.977287, iCameraTime + 500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					
					SHAKE_CAM(camMain, "HAND_SHAKE", 0.2)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					OVERRIDE_LODSCALE_THIS_FRAME(1.0)
					
					SET_FRONTEND_RADIO_ACTIVE(FALSE)
					REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					CLEAR_TEXT()
					
					//Fade In
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					iDialogueTimer[0] = GET_GAME_TIMER() + 20000	//Saving variables by reusing iDialogueTimer[0]
					
					ADVANCE_CUTSCENE()
//				ENDIF
			BREAK
			CASE 2
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				
				#IF IS_DEBUG_BUILD
				IF NOT IS_INTERIOR_READY(intChopShop)
					PRINTLN("PINNING INTERIOR...")
				ENDIF
				
				PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iDialogueTimer[0])
				#ENDIF
				
				bPausedMichael = TRUE
				bPausedTrevor = TRUE
				bPausedFranklin = TRUE
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				AND GET_TIME_POSITION_IN_RECORDING(vehPlayer) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehPlayer)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
				ELSE
					bPausedFranklin = FALSE
				ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				AND GET_TIME_POSITION_IN_RECORDING(vehRival1) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival1)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival1)
				ELSE
					bPausedMichael = FALSE
				ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
				AND GET_TIME_POSITION_IN_RECORDING(vehRival2) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival2)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival2)
				ELSE
					bPausedTrevor = FALSE
				ENDIF
				
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_MICHAEL), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_TREVOR), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_FRANKLIN), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				
				IF (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN AND (bPausedFranklin OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)))
				OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL AND (bPausedMichael OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)))
				OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR AND (bPausedTrevor OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)))
					SPAWN_VEHICLE(vehCutscene, FELON, <<481.3185, -1317.7887, 28.2023>>, -63.4145)
					
					SPAWN_PED(pedWorker1, S_M_Y_DWSERVICE_02, <<480.6057, -1317.0214, 28.2028>>, 75.2925)
					SET_PED_COMPONENT_VARIATION(pedWorker1, PED_COMP_HEAD, 1, 0)
					
					SPAWN_PED(pedWorker2, S_M_Y_DWSERVICE_02, <<478.7872, -1317.6216, 28.2028>>, -56.3168)
					SET_PED_COMPONENT_VARIATION(pedWorker2, PED_COMP_HEAD, 2, 0)
					
					CREATE_NPC_PED_ON_FOOT(pedMolly, CHAR_MOLLY, <<479.5576, -1315.9417, 28.2028>>, -66.9277)
					SET_PED_PROP_INDEX(pedMolly, ANCHOR_EYES, 0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedMolly, TRUE)
					
					CREATE_NPC_PED_ON_FOOT(pedDevin, CHAR_DEVIN, <<479.9050, -1314.8568, 28.2028>>, 170.8332)
					SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_TORSO, 2, 0)
					SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_LEG, 2, 0)
					SET_PED_COMPONENT_VARIATION(pedDevin, PED_COMP_DECL, 1, 0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedDevin, TRUE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				
				#IF IS_DEBUG_BUILD
				IF NOT IS_INTERIOR_READY(intChopShop)
					PRINTLN("PINNING INTERIOR...")
				ENDIF
				
				PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iDialogueTimer[0])
				#ENDIF
				
				bPausedMichael = TRUE
				bPausedTrevor = TRUE
				bPausedFranklin = TRUE
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				AND GET_TIME_POSITION_IN_RECORDING(vehPlayer) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehPlayer)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
				ELSE
					bPausedFranklin = FALSE
				ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				AND GET_TIME_POSITION_IN_RECORDING(vehRival1) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival1)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival1)
				ELSE
					bPausedMichael = FALSE
				ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
				AND GET_TIME_POSITION_IN_RECORDING(vehRival2) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival2)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival2)
				ELSE
					bPausedTrevor = FALSE
				ENDIF
				
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_MICHAEL), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_TREVOR), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_FRANKLIN), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				
				IF bPausedFranklin OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED(CHAR_FRANKLIN), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_LEAVE_ANY_VEHICLE(NULL, 400)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<479.9050, -1314.8568, 29.1361>>, PEDMOVE_WALK)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_FRANKLIN), seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
				ENDIF
				
				IF bPausedMichael OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED(CHAR_MICHAEL), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_LEAVE_ANY_VEHICLE(NULL, 0)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<479.9050, -1314.8568, 29.1361>>, PEDMOVE_WALK)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_MICHAEL), seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
				ENDIF
				
				IF bPausedTrevor OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED(CHAR_TREVOR), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_LEAVE_ANY_VEHICLE(NULL, 150)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<479.9050, -1314.8568, 29.1361>>, PEDMOVE_WALK)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_TREVOR), seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
				ENDIF
				
				IF TIMERA() > 500
					SET_CAM_PARAMS(camMain, <<506.658447, -1312.660767, 29.656441>>, <<1.058747, 0.013282, 96.031349>>, 36.686596)
					SET_CAM_PARAMS(camMain, <<505.959290, -1312.438965, 29.668831>>, <<1.058747, 0.013282, 96.031349>>, 36.686596, 6000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					AND GET_TIME_POSITION_IN_RECORDING(vehPlayer) < (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehPlayer)) / 100.0) * 80
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPlayer, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehPlayer)) / 100.0) * 80)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehPlayer)
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
					AND GET_TIME_POSITION_IN_RECORDING(vehRival1) < (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival1)) / 100.0) * 80
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival1, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival1)) / 100.0) * 80)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehRival1)
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
					AND GET_TIME_POSITION_IN_RECORDING(vehRival2) < (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival2)) / 100.0) * 80
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival2, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival2)) / 100.0) * 80)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehRival2)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 4
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				
				#IF IS_DEBUG_BUILD
				IF NOT IS_INTERIOR_READY(intChopShop)
					PRINTLN("PINNING INTERIOR...")
				ENDIF
				
				PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iDialogueTimer[0])
				#ENDIF
				
				bPausedMichael = TRUE
				bPausedTrevor = TRUE
				bPausedFranklin = TRUE
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				AND GET_TIME_POSITION_IN_RECORDING(vehPlayer) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehPlayer)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
				ELSE
					bPausedFranklin = FALSE
				ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				AND GET_TIME_POSITION_IN_RECORDING(vehRival1) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival1)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival1)
				ELSE
					bPausedMichael = FALSE
				ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
				AND GET_TIME_POSITION_IN_RECORDING(vehRival2) >= (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehRival2)) / 100.0) * 99
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehRival2)
				ELSE
					bPausedTrevor = FALSE
				ENDIF
				
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_MICHAEL), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_TREVOR), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_FRANKLIN), PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				
				IF bPausedFranklin OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED(CHAR_FRANKLIN), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_LEAVE_ANY_VEHICLE(NULL, 400)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<479.9050, -1314.8568, 29.1361>>, PEDMOVE_WALK)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_FRANKLIN), seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
				ENDIF
				
				IF bPausedMichael OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED(CHAR_MICHAEL), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_LEAVE_ANY_VEHICLE(NULL, 0)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<479.9050, -1314.8568, 29.1361>>, PEDMOVE_WALK)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_MICHAEL), seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
				ENDIF
				
				IF bPausedTrevor OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED(CHAR_TREVOR), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_LEAVE_ANY_VEHICLE(NULL, 150)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<479.9050, -1314.8568, 29.1361>>, PEDMOVE_WALK)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_TREVOR), seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
				ENDIF
				
				IF ((NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				OR bPausedFranklin)
				AND (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
				OR bPausedMichael)
				AND (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
				OR bPausedTrevor)
				AND (TIMERA() > 3000
				AND HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("car_1_ext_concat"))
				AND (HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWorker1)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWorker2)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedMolly)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDevin)))
//				AND IS_INTERIOR_READY(intChopShop) - should be bunched with HAS_THIS_CUTSCENE_LOADED
				OR GET_GAME_TIMER() > iDialogueTimer[0]
					#IF IS_DEBUG_BUILD
					IF GET_GAME_TIMER() > iDialogueTimer[0]
						IF NOT IS_INTERIOR_READY(intChopShop)
							SCRIPT_ASSERT("IS_INTERIOR_READY(intChopShop) timed out, see log for details.")
						ENDIF
					ENDIF
					#ENDIF
					
					//Stream Volume
					IF STREAMVOL_IS_VALID(svolCarStealOutro)
						STREAMVOL_DELETE(svolCarStealOutro)
					ENDIF
					
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
					
					PED_INDEX pedLastPlayer
					pedLastPlayer = PLAYER_PED_ID()
					
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
						
						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(pedLastPlayer)
					ENDIF
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED(CHAR_MICHAEL), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED(CHAR_FRANKLIN), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED(CHAR_TREVOR), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(vehCutscene)
						REGISTER_ENTITY_FOR_CUTSCENE(vehCutscene, "Car_in_garage", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(vehPlayer, "Franklins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehRival1, "Car_Racer_runsaway", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehRival2, "Car_Racer_dies", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedMolly, "MOLLY", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedDevin, "DEVIN", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					IF NOT IS_ENTITY_DEAD(pedWorker1)
						REGISTER_ENTITY_FOR_CUTSCENE(pedWorker1, "Car_1_guy", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedWorker2)
						REGISTER_ENTITY_FOR_CUTSCENE(pedWorker2, "Car_1_guy_2", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					START_CUTSCENE()
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SET_FRONTEND_RADIO_ACTIVE(TRUE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 5
				IF WAS_CUTSCENE_SKIPPED()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					
					bCutsceneSkipped = TRUE
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("CutsceneFadeOut")
					IF GET_CUTSCENE_TIME() > ROUND(108.000000 * 1000.0)
						IF NOT bCutsceneSkipped
							IF NOT HAS_LABEL_BEEN_TRIGGERED("RemoveCopUniform")
								RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)  //reset that the player has changed outfit on mission to how it was at the start of the mission
								RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR, bHasChanged)
								
								RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED(CHAR_MICHAEL))
								
								SET_LABEL_AS_TRIGGERED("RemoveCopUniform", TRUE)
							ENDIF
						ENDIF
						
						//Also hide the cars
						SET_ENTITY_VISIBLE(vehPlayer, FALSE)
						SET_ENTITY_VISIBLE(vehRival1, FALSE)
						SET_ENTITY_VISIBLE(vehRival2, FALSE)
						SET_VEHICLE_ENGINE_ON(vehPlayer, FALSE, TRUE)
						SET_VEHICLE_ENGINE_ON(vehRival1, FALSE, TRUE)
						SET_VEHICLE_ENGINE_ON(vehRival2, FALSE, TRUE)
						
						IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
							SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
							SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
							SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
						ENDIF
						
						REMOVE_MODEL_HIDE(<<498.7252, -1317.7551, 28.2534>>, 1.0, PROP_SC1_06_GATE_L, FALSE)
						REMOVE_MODEL_HIDE(<<494.6904, -1312.0663, 28.2534>>, 1.0, PROP_SC1_06_GATE_R, FALSE)
						
						SET_LABEL_AS_TRIGGERED("CutsceneFadeOut", TRUE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
					
					FORCE_PED_MOTION_STATE(PLAYER_PED(CHAR_MICHAEL), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
				ENDIF
				
				IF bCutsceneSkipped
					IF NOT HAS_LABEL_BEEN_TRIGGERED("ForceFadeOut")
						IF IS_SCREEN_FADED_OUT()
							SET_LABEL_AS_TRIGGERED("ForceFadeOut", TRUE)
						ENDIF
					ELSE
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
				
					REPLAY_STOP_EVENT()
				
					IF bCutsceneSkipped
						LOAD_SCENE_ADV(<<498.7252, -1317.7551, 28.2534>>, 75.0)
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("RemoveCopUniform")
							RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)  //reset that the player has changed outfit on mission to how it was at the start of the mission
							RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR, bHasChanged)
							
							RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED(CHAR_MICHAEL))
						ENDIF
						
						INT iTimeOut
						
						iTimeOut = GET_GAME_TIMER() + 10000
						
						WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_MICHAEL))
						AND GET_GAME_TIMER() < iTimeOut
							WAIT_WITH_DEATH_CHECKS(0)	PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iTimeOut)
						ENDWHILE
						
						SET_PED_POSITION(PLAYER_PED(CHAR_MICHAEL), <<480.9356, -1315.8055, 28.2023>>, 270.8615, FALSE)
						
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
						
						FORCE_PED_MOTION_STATE(PLAYER_PED(CHAR_MICHAEL), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
						
						WAIT_WITH_DEATH_CHECKS(100)
						
						DO_SCREEN_FADE_IN(1000)	#IF IS_DEBUG_BUILD	PRINTLN("DO_SCREEN_FADE_IN")	#ENDIF
					ENDIF
					
					bCutsceneSkipped = FALSE
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SAFE_DELETE_VEHICLE(vehCutscene)
					SAFE_DELETE_VEHICLE(vehPlayer)
					SAFE_DELETE_VEHICLE(vehRival1)
					SAFE_DELETE_VEHICLE(vehRival2)
					
					SAFE_DELETE_PED(pedMolly)
					SAFE_DELETE_PED(pedDevin)
					SAFE_DELETE_PED(pedWorker1)
					SAFE_DELETE_PED(pedWorker2)
					
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
						SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
						SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					ENDIF
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Stream Volume
		IF STREAMVOL_IS_VALID(svolCarStealOutro)
			STREAMVOL_DELETE(svolCarStealOutro)
		ENDIF
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("CAR_1_GARAGE_ARRIVAL")
			STOP_AUDIO_SCENE("CAR_1_GARAGE_ARRIVAL")
		ENDIF
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

//══════════════════════════════════╡ DEBUG ╞════════════════════════════════════

#IF IS_DEBUG_BUILD

PROC debugRoutine()
	IF bAutoSkipping = TRUE
		IF ENUM_TO_INT(eMissionObjective) >= ENUM_TO_INT(eMissionObjectiveAutoJSkip)
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//				
//				WHILE IS_SCREEN_FADING_IN()
//					WAIT(0)
//				ENDWHILE
//			ENDIF
			
			eMissionObjectiveAutoJSkip = initMission
			
			bAutoSkipping = FALSE
			bSkipped = TRUE
		ENDIF
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		missionPassed()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		missionFailed()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	OR ENUM_TO_INT(eMissionObjective) < ENUM_TO_INT(eMissionObjectiveAutoJSkip)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			bSkipped = TRUE
		ENDIF
		
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
		ENDIF
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		ADVANCE_STAGE()
		
		TEXT_LABEL txtLabel
		
		txtLabel = ENUM_TO_INT(eMissionObjective)
		
		PRINTSTRING(txtLabel)
		PRINTNL()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
	OR LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iStageSkipMenu, ENUM_TO_INT(eMissionObjective))
		IF bAutoSkipping = FALSE
			bAutoSkipping = TRUE
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				eMissionObjectiveAutoJSkip = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) - 1)
			ELSE
				eMissionObjectiveAutoJSkip = INT_TO_ENUM(MissionObjective, iStageSkipMenu)
			ENDIF
			
			eMissionObjective = initMission
			
			DO_SCREEN_FADE_OUT(500)
			
			WHILE IS_SCREEN_FADING_OUT()
				WAIT(0)
			ENDWHILE
			
			//Stop recordings
			IF DOES_ENTITY_EXIST(vehPlayer)
				IF NOT IS_ENTITY_DEAD(vehPlayer)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehRival1)
				IF NOT IS_ENTITY_DEAD(vehRival1)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehRival1)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehRival2)
				IF NOT IS_ENTITY_DEAD(vehRival2)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehRival2)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehPolice1)
				IF NOT IS_ENTITY_DEAD(vehPolice1)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice1)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehPolice2)
				IF NOT IS_ENTITY_DEAD(vehPolice2)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice2)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice2)
					ENDIF
				ENDIF
			ENDIF
			
			CLEANUP_UBER_PLAYBACK()
			
			//Cutscene
			WHILE NOT HAS_CUTSCENE_FINISHED()
				STOP_CUTSCENE(TRUE)
				
				PRINTLN("STOPPING CUTSCENE...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			REMOVE_CUTSCENE()
			
			WHILE HAS_CUTSCENE_LOADED()
				PRINTLN("REMOVING CUTSCENE...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			MISSION_CLEANUP(TRUE)
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10000.0, TRUE)
		ENDIF
	ENDIF
ENDPROC 

#ENDIF

//══════════════════════════════╡ MISSION SCRIPT ╞═══════════════════════════════

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Flow_Mission_Force_Cleanup()
		
		MISSION_CLEANUP(FALSE, TRUE)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
//═══════════════════════════════╡ MISSION LOOP ╞════════════════════════════════
	
	#IF IS_DEBUG_BUILD
		MissionNames()
	#ENDIF
	
//	REQUEST_MODEL(ENTITYXF)
//	REQUEST_MODEL(CHEETAH)
//	REQUEST_MODEL(TRAILERS)
//	REQUEST_MODEL(TRAILERS2)
//	
//	WHILE NOT HAS_MODEL_LOADED(ENTITYXF)
//	OR NOT HAS_MODEL_LOADED(CHEETAH)
//	OR NOT HAS_MODEL_LOADED(TRAILERS)
//	OR NOT HAS_MODEL_LOADED(TRAILERS2)
//		WAIT(0)
//	ENDWHILE
//	
//	REQUEST_VEHICLE_RECORDING(500, "ALubersetup")
////	REQUEST_VEHICLE_RECORDING(501, "ALubersetup")
//	
//	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(500, "ALubersetup")
////	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(501, "ALubersetup")
//		WAIT(0)
//	ENDWHILE
//	
//	SPAWN_VEHICLE(vehRival1, ENTITYXF, vRival1Start, fRival1Start)
//	SPAWN_VEHICLE(vehRival2, CHEETAH, vRival2Start, fRival2Start)
//	
//	SAFE_ADD_BLIP_VEHICLE(blipRival1, vehRival1)
//	SAFE_ADD_BLIP_VEHICLE(blipRival2, vehRival2)
//	
//	SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival2)
//	
//	SET_VEHICLE_POPULATION_BUDGET(0)
//	
//	CLEAR_AREA(vRival1Start, 1000.0, TRUE)
//	
//	LOAD_SCENE(vRival1Start)
//	
//	//INIT_UBER_RECORDING("ALubersetup")
//	
//	INITIALISE_UBER_PLAYBACK("ALubersetup", 500)
//	
//	UberSetupVariables()
//	
//	iDontSwitchThisSetpieceRecordingToAI = 1
//	iDontSwitchThisSetpieceRecordingToAI2 = 2
//	
//	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
//	
//	WAIT(1000)
//	
//	START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 500, "ALubersetup")
////	START_PLAYBACK_RECORDED_VEHICLE(vehRival2, 501, "ALubersetup")
//	
//	VEHICLE_INDEX vehClosestCars[10]
//	
//	SET_PED_POPULATION_BUDGET(0)
//	SET_VEHICLE_POPULATION_BUDGET(0)
//	
//	WHILE(TRUE)
//		//UPDATE_UBER_RECORDING()
//		
//		bTrafficDebugOn = TRUE
//		bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
//		fUberPlaybackDensitySwitchOffRange = 300
//		PRELOAD_ALL_CHASE_RECORDINGS(GET_TIME_POSITION_IN_RECORDING(vehRival1), "ALubersetup")
//		
//		CREATE_ALL_WAITING_UBER_CARS()
//		
//		UPDATE_UBER_PLAYBACK(vehRival1, fPlaybackSpeed)
//		
////		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
////			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
////				IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(vehRival2)
////					START_RECORDING_VEHICLE(vehRival2, 599, "ALubersetup", TRUE)
////				ENDIF
////			ELSE
////				IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehRival2)
////					STOP_RECORDING_VEHICLE(vehRival2)
////				ENDIF
////			ENDIF
////		ENDIF
//		
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
////			IF DOES_ENTITY_EXIST(vehRival2)
////				IF NOT IS_ENTITY_DEAD(vehRival2)
////					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
////						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival2, GET_TIME_POSITION_IN_RECORDING(vehRival1))
////					ENDIF
////				ENDIF
////			ENDIF
//			
//			IF DOES_ENTITY_EXIST(SetPieceCarID[1])
//				IF NOT DOES_ENTITY_EXIST(vehTrailer[0])
//					SPAWN_VEHICLE(vehTrailer[0], TRAILERS2, GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(401, "ALubersetup"), 0.0), 0.0)
//					SET_ENTITY_ROTATION(vehTrailer[0], GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(401, "ALubersetup"), 0.0))
//					SET_ENTITY_NO_COLLISION_ENTITY(vehTrailer[0], SetPieceCarID[1], TRUE)
//				ENDIF
//			ENDIF
//			
//			IF DOES_ENTITY_EXIST(vehTrailer[0])
//				IF NOT IS_ENTITY_DEAD(vehTrailer[0])
//					IF DOES_ENTITY_EXIST(SetPieceCarID[1])
//						IF NOT IS_ENTITY_DEAD(SetPieceCarID[1])
//							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[1])
//								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[0])
//									START_PLAYBACK_RECORDED_VEHICLE(vehTrailer[0], 401, "ALubersetup")
//								ENDIF
//							ENDIF
//							
//							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[0])
//								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[1])
//									SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTrailer[0], GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[1]))
//								ENDIF
//							ELSE
//								IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(SetPieceCarID[1])
//									ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[1], vehTrailer[0])
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF DOES_ENTITY_EXIST(SetPieceCarID[2])
//				IF NOT DOES_ENTITY_EXIST(vehTrailer[1])
//					SPAWN_VEHICLE(vehTrailer[1], TRAILERS, GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(403, "ALubersetup"), 0.0), 0.0)
//					SET_ENTITY_ROTATION(vehTrailer[1], GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(403, "ALubersetup"), 0.0))
//					SET_ENTITY_NO_COLLISION_ENTITY(vehTrailer[1], SetPieceCarID[2], TRUE)
//				ENDIF
//			ENDIF
//			
//			IF DOES_ENTITY_EXIST(vehTrailer[1])
//				IF NOT IS_ENTITY_DEAD(vehTrailer[1])
//					IF DOES_ENTITY_EXIST(SetPieceCarID[2])
//						IF NOT IS_ENTITY_DEAD(SetPieceCarID[2])
//							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[2])
//								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[1])
//									START_PLAYBACK_RECORDED_VEHICLE(vehTrailer[1], 403, "ALubersetup")
//								ENDIF
//							ENDIF
//							
//							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrailer[1])
//								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[2])
//									SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTrailer[1], GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[2]))
//								ENDIF
//							ELSE
//								IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(SetPieceCarID[2])
//									ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[2], vehTrailer[1])
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			VEHICLE_INDEX vehPlayerRecord = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
//			
//			IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehPlayerRecord)
//				VEHICLE_INDEX vehTrailerRecord
//				GET_VEHICLE_TRAILER_VEHICLE(vehPlayerRecord, vehTrailerRecord)
//				
//				IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehPlayerRecord)
//					START_RECORDING_VEHICLE(vehTrailerRecord, 450, "ALubersetup", TRUE)
//				ELSE
//					IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehTrailerRecord)
//						STOP_RECORDING_VEHICLE(vehTrailerRecord)
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF IS_BUTTON_PRESSED(PAD1, CIRCLE)
//				//SET_VEHICLE_FORWARD_SPEED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) + 1.0)
//				MODIFY_VEHICLE_TOP_SPEED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 20.0)
//			ELSE
//				MODIFY_VEHICLE_TOP_SPEED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 0.0)
//			ENDIF
//		ENDIF
//		
//		IF IS_BUTTON_PRESSED(PAD1, DPADRIGHT)
//			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10000.0, TRUE)
//		ENDIF
//		
//		IF IS_BUTTON_PRESSED(PAD1, SQUARE)
//			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehClosestCars)
//			
//			INT i
//			
//			REPEAT COUNT_OF(vehClosestCars) i
//				IF DOES_ENTITY_EXIST(vehClosestCars[i])
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehClosestCars[i])
//						IF IS_ENTITY_IN_ANGLED_AREA(vehClosestCars[i], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.5, -2.0, -1.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.5, 20.0, 5.0>>), 5.0)
//							SET_ENTITY_COORDS(vehClosestCars[i], GET_ENTITY_COORDS(vehClosestCars[i]) - <<0.0, 0.0, 10.0>>)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
//			
////			CLEAR_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 4.0, 0.0>>), 3.0, TRUE)
////			CLEAR_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 6.0, 0.0>>), 3.0, TRUE)
////			CLEAR_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 8.0, 0.0>>), 3.0, TRUE)
////			CLEAR_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 10.0, 0.0>>), 3.0, TRUE)
//		ENDIF
//		
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
//		
//		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//		
//		WAIT(0)
//	ENDWHILE
	
//////	REQUEST_MODEL(TRAILERS)
//////	
//////	WHILE NOT HAS_MODEL_LOADED(TRAILERS)
//////		WAIT(0)
//////	ENDWHILE
//////	
//////	REQUEST_VEHICLE_RECORDING(403, "ALubersetup")
//////	
//////	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(403, "ALubersetup")
//////		WAIT(0)
//////	ENDWHILE
//////	
//////	//SPAWN_VEHICLE(vehRival1, ENTITYXF, vRival1Start, fRival1Start)
//////	
//////	//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival1)
//////	
//////	SET_PED_POSITION(PLAYER_PED_ID(), GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(403, "ALubersetup"), 0.0) + <<5.0, 5.0, 0.0>>, 0.0, FALSE)
//////
//////	LOAD_SCENE(GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(403, "ALubersetup"), 0.0))
//////
//////	WAIT(500)
//////
//////	SPAWN_VEHICLE(vehRival1, TRAILERS, GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(403, "ALubersetup"), 0.0), 0.0)
//////	
//////	//Uber Playback
//////	fPlaybackSpeed = 1.0
//////	
//////	//INITIALISE_UBER_PLAYBACK(sCarrecSetup, 500)
//////	//
//////	//UberSetupVariables()
//////	
//////	START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 403, "ALubersetup")
//////	
//////	FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehRival1)
//////	
//////	START_RECORDING_VEHICLE(vehRival1, 403, "ALubersetup", TRUE)
//////	
//////	SAFE_ADD_BLIP_VEHICLE(blipRival1, vehRival1, TRUE)
//////	
//////	DISABLE_CELLPHONE(TRUE)
//////	
//////	CLEAR_AREA(GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(403, "ALubersetup"), 0.0), 100.0, TRUE)
//////	
//////	WHILE IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
//////		TEXT_LABEL txtLabel
//////		txtLabel = "REC"
//////		SET_TEXT_SCALE(0.5, 0.5)
//////		SET_TEXT_CENTRE(TRUE)
//////		SET_TEXT_COLOUR(255, 0, 0, 255)
//////		DISPLAY_TEXT(0.5, 0.1, txtLabel)
//////		
//////		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//////		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//////		
//////		WAIT(0)
//////	ENDWHILE
//////	
//////	STOP_RECORDING_VEHICLE(vehRival1)
//////	
//////	WHILE(TRUE)
////////		bTrafficDebugOn = TRUE
////////		bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
////////		
////////		UPDATE_UBER_PLAYBACK(vehRival1, fPlaybackSpeed)
////////		
////////		UPDATE_UBER_RECORDING()
//////		
//////		WAIT(0)
//////	ENDWHILE	//56806.890

//SET_PED_POPULATION_BUDGET(0)
//SET_VEHICLE_POPULATION_BUDGET(0)
//
//REQUEST_MODEL(ENTITYXF)
//
//WHILE NOT HAS_MODEL_LOADED(ENTITYXF)
//	WAIT(0)
//ENDWHILE
//
//REQUEST_VEHICLE_RECORDING(500, "ALubersetup")
//
//WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(500, "ALubersetup")
//	WAIT(0)
//ENDWHILE
//
//SPAWN_VEHICLE(vehRival1, ENTITYXF, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, "ALubersetup"), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, "ALubersetup")))
//
//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival1)
//
//LOAD_SCENE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, "ALubersetup"))
//	
//SAFE_ADD_BLIP_VEHICLE(blipRival1, vehRival1, TRUE)
//
//DISABLE_CELLPHONE(TRUE)
//	
//INITIALISE_UBER_PLAYBACK("ALubersetup", 500)
//	
//UberSetupVariables()
//
//SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
//
//WAIT(1000)
//
//START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 500, "ALubersetup")
//
//WHILE(TRUE)
//	UPDATE_UBER_PLAYBACK(vehRival1, 1.0)
//	
//	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//	
//	WAIT(0)
//ENDWHILE

////	REQUEST_MODEL(POLICEB)
////	REQUEST_MODEL(ENTITYXF)
////	REQUEST_MODEL(CHEETAH)
////	REQUEST_MODEL(F620)
////	
////	WHILE NOT HAS_MODEL_LOADED(POLICEB)
////	OR NOT HAS_MODEL_LOADED(ENTITYXF)
////	OR NOT HAS_MODEL_LOADED(CHEETAH)
////	OR NOT HAS_MODEL_LOADED(F620)
////		WAIT(0)
////	ENDWHILE
////	
////	REQUEST_VEHICLE_RECORDING(500, "ALubercop")
////	//REQUEST_VEHICLE_RECORDING(402, "ALubercop")
////	
////	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(500, "ALubercop")
////	//OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(402, "ALubercop")
////		WAIT(0)
////	ENDWHILE
////	
////	SPAWN_VEHICLE(vehRival1, POLICEB, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, "ALubercop"), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, "ALubercop")))
////	SPAWN_VEHICLE(vehRival2, POLICEB, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop"), "ALubercop"), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop"), "ALubercop")))
////
////	SPAWN_VEHICLE(vehPlayer, F620, <<-1929.9939, 4599.4263, 56.0583>>, 136.1435)	//GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(402, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(402, "ALubercop"), "ALubercop"), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(402, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(402, "ALubercop"), "ALubercop")))
////	SET_ENTITY_COLLISION(vehPlayer, FALSE)
////	FREEZE_ENTITY_POSITION(vehPlayer, TRUE)
////	
////	SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival1)
////	
//////	WHILE NOT CAN_CREATE_RANDOM_DRIVER()
//////		WAIT(0)
//////	ENDWHILE
//////	
//////	CREATE_RANDOM_PED_AS_DRIVER(vehRival2)
////////	CREATE_RANDOM_PED_AS_DRIVER(vehPlayer)
////	
////	LOAD_SCENE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, "ALubercop"))
////	
////////	//Uber Playback
//////////	fPlaybackSpeed = 0.0
////	
////	SAFE_ADD_BLIP_VEHICLE(blipRival1, vehRival1, TRUE)
////	
////	DISABLE_CELLPHONE(TRUE)
////	
////	//SET_ENTITY_COLLISION(vehRival1, FALSE)
////	SET_ENTITY_COLLISION(vehRival2, FALSE)
////	FREEZE_ENTITY_POSITION(vehRival2, TRUE)
////	
////	SET_PED_HELMET(PLAYER_PED_ID(), FALSE)
////	
////	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
////	
//////	INIT_UBER_RECORDING("ALubercop")
////	
////	INITIALISE_UBER_PLAYBACK("ALubercop", 500)
////	
////	UberCopVariables()
////	
////	VEHICLE_INDEX vehClosestCars[10]
////	
////	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
////	
////	WAIT(1000)
////	
////	START_PLAYBACK_RECORDED_VEHICLE(vehRival1, 500, "ALubercop")
////	
////	INT iDistCheck = 7
////	
////	WHILE(TRUE)
////		IF DOES_ENTITY_EXIST(SetPieceCarID[2])
////			SAFE_ADD_BLIP_VEHICLE(blipRival2, SetPieceCarID[2], FALSE)
////		ENDIF
////		
//////		UPDATE_UBER_RECORDING()
////		
////		bTrafficDebugOn = TRUE
////		bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
////		fUberPlaybackDensitySwitchOffRange = 300
////		PRELOAD_ALL_CHASE_RECORDINGS(GET_TIME_POSITION_IN_RECORDING(vehRival1), "ALubercop")
////		
////		CREATE_ALL_WAITING_UBER_CARS()
////		
////		UPDATE_UBER_PLAYBACK(vehRival1, fPlaybackSpeed)
////		
////		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
////		
////		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + 500.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.5, 255, 0, 0, 255)
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + 1000.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.45, 255, 0, 0, 225)
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + 1500.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.4, 0, 0, 255, 200)
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + 2000.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.35, 255, 0, 255, 175)
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + 2500.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.3, 255, 0, 0, 150)
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + 3000.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.25, 255, 0, 0, 125)
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + 3500.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.2, 255, 0, 0, 100)
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + 4000.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.15, 255, 0, 0, 75)
////			
////			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival1) + (500.0 + (500.0 * iDistCheck)), 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, "ALubercop")), "ALubercop"), 0.5, 0, 255, 0, 255)
////		ENDIF
////		
////		IF IS_BUTTON_PRESSED(PAD1, SQUARE)
////			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehClosestCars)
////			
////			INT i
////			
////			REPEAT COUNT_OF(vehClosestCars) i
////				IF DOES_ENTITY_EXIST(vehClosestCars[i])
////					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehClosestCars[i])
////						IF IS_ENTITY_IN_ANGLED_AREA(vehClosestCars[i], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.5, -2.0, -1.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.5, 20.0, 5.0>>), 5.0)
////							SET_ENTITY_COORDS(vehClosestCars[i], GET_ENTITY_COORDS(vehClosestCars[i]) - <<0.0, 0.0, 10.0>>)
////						ENDIF
////					ENDIF
////				ENDIF
////			ENDREPEAT
////		ENDIF
////		
////		IF IS_BUTTON_PRESSED(PAD1, CIRCLE)
////			MODIFY_VEHICLE_TOP_SPEED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 50.0)
////		ELSE
////			MODIFY_VEHICLE_TOP_SPEED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 0.0)
////		ENDIF
////		
////		IF IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
////			IF iDistCheck > 0
////				iDistCheck--
////			ENDIF
////		ELIF IS_BUTTON_JUST_PRESSED(PAD1, DPADRIGHT)
////			IF iDistCheck < 7
////				iDistCheck++
////			ENDIF
////		ENDIF
////		
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
////		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
////		
////		WAIT(0)
////	ENDWHILE

//Reference
//TrafficCarPos[0] = <<2531.8367, 567.2463, 111.2710>>
//TrafficCarQuatX[0] = 0.0104
//TrafficCarQuatY[0] = 0.0256
//TrafficCarQuatZ[0] = 0.9981
//TrafficCarQuatW[0] = -0.0547
//TrafficCarRecording[0] = 1
//TrafficCarStartime[0] = 3432.0000
//TrafficCarModel[0] = Mule

//SET_PED_POPULATION_BUDGET(0)
//SET_VEHICLE_POPULATION_BUDGET(0)
//
//UberCopVariables()
//
//BOOL bRunTool = FALSE
//
//INT iCarIndex = 0
//
//SET_PED_POSITION(PLAYER_PED_ID(), TrafficCarPos[iCarIndex], 0.0, FALSE)
//
//LOAD_SCENE(TrafficCarPos[iCarIndex])
//
//WAIT(500)
//
//WHILE(TRUE)
//	IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
//		bRunTool = !bRunTool
//	ENDIF
//	
//	IF bRunTool AND TrafficCarRecording[iCarIndex] <> 0 AND iCarIndex < TOTAL_NUMBER_OF_TRAFFIC_CARS - 1
//		IF iCarIndex < TOTAL_NUMBER_OF_TRAFFIC_CARS - 1
//			SET_PED_POSITION(PLAYER_PED_ID(), TrafficCarPos[iCarIndex], 0.0, FALSE)
//			
//			SAFE_DELETE_VEHICLE(vehRival1)
//			SAFE_DELETE_VEHICLE(vehRival2)
//			
//			LOAD_SCENE_ADV(TrafficCarPos[iCarIndex])
//			
//			CLEAR_AREA(TrafficCarPos[iCarIndex], 100.0, TRUE)
//			
//			REQUEST_MODEL(TrafficCarModel[iCarIndex])
//			
//			WHILE NOT HAS_MODEL_LOADED(TrafficCarModel[iCarIndex])
//				WAIT(0)
//			ENDWHILE
//			
//			REQUEST_VEHICLE_RECORDING(TrafficCarRecording[iCarIndex], "ALubercop")
//			
//			WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(TrafficCarRecording[iCarIndex], "ALubercop")
//				WAIT(0)
//			ENDWHILE
//			
//			SPAWN_VEHICLE(vehRival1, TrafficCarModel[iCarIndex], TrafficCarPos[iCarIndex], 0.0)
//			
//			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival1)
//			
//			SPAWN_VEHICLE(vehRival2, TrafficCarModel[iCarIndex], TrafficCarPos[iCarIndex], 0.0)
//			
//			SET_ENTITY_NO_COLLISION_ENTITY(vehRival1, vehRival2, FALSE)
//			
//			START_PLAYBACK_RECORDED_VEHICLE(vehRival1, TrafficCarRecording[iCarIndex], "ALubercop")
//			
//			SET_PLAYBACK_SPEED(vehRival1, 0.0)
//			
//			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehRival1)
//			
//			SET_ENTITY_COORDS(vehRival2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(TrafficCarRecording[iCarIndex], 0.0, "ALubercop"))
//			SET_ENTITY_ROTATION(vehRival2, GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(TrafficCarRecording[iCarIndex], 0.0, "ALubercop"))
//			SET_VEHICLE_ON_GROUND_PROPERLY(vehRival2)		
//			
//			WAIT(0)
//			
//			ACTIVATE_PHYSICS(vehRival2)
//			
//			WAIT(1000)
//			
//			VECTOR vCoord1 = GET_ENTITY_COORDS(vehRival1)
//			VECTOR vCoord2 = GET_ENTITY_COORDS(vehRival2)
//			FLOAT fOffset = vCoord1.Z - vCoord2.Z
//			
//			SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehRival1, <<0.0, 0.0, -fOffset>>)	//SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK()
//			
//			WAIT(0)
//			
//			START_RECORDING_VEHICLE(vehRival1, TrafficCarRecording[iCarIndex], "ALubercopX", TRUE)
//			
//			SET_PLAYBACK_SPEED(vehRival1, 1.0)
//			
//			WHILE IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
//				TEXT_LABEL txtLabel
//				txtLabel = "REC "
//				txtLabel += TrafficCarRecording[iCarIndex]
//				txtLabel += " "
//				txtLabel += GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehRival1)
//				SET_TEXT_SCALE(0.5, 0.5)
//				SET_TEXT_CENTRE(TRUE)
//				SET_TEXT_COLOUR(255, 0, 0, 255)
//				DISPLAY_TEXT(0.5, 0.1, txtLabel)
//				
//				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//				
//				WAIT(0)
//			ENDWHILE
//			
//			STOP_RECORDING_VEHICLE(vehRival1)
//			
//			iCarIndex++
//		ENDIF
//	ELSE
//		TEXT_LABEL txtLabel
//		IF iCarIndex < TOTAL_NUMBER_OF_TRAFFIC_CARS - 1
//			txtLabel = "REC "
//			txtLabel += TrafficCarRecording[iCarIndex]
//			IF DOES_ENTITY_EXIST(vehRival1)
//				txtLabel += " "
//				txtLabel += GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehRival1)
//			ENDIF
//		ELSE
//			txtLabel = "END"
//		ENDIF
//		SET_TEXT_SCALE(0.5, 0.5)
//		SET_TEXT_CENTRE(TRUE)
//		SET_TEXT_COLOUR(0, 255, 0, 255)
//		DISPLAY_TEXT(0.5, 0.1, txtLabel)
//		
//		IF iCarIndex < TOTAL_NUMBER_OF_TRAFFIC_CARS - 1
//			IF TrafficCarRecording[iCarIndex] = 0
//				iCarIndex++
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
//	
//	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//	
//	WAIT(0)
//ENDWHILE

//	CLEANUP_UBER_PLAYBACK(TRUE)
//	
//	fPlaybackSpeed = 1.0
//	
//	INITIALISE_UBER_PLAYBACK(sCarrecChase, 500)
//	
//	UberCopVariables()
//	
//	bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
//	
//	BOOL bOnce = FALSE
//	INT iCountDown = -1
////	BOOL bVisible = TRUE
//	
////	FLOAT fStart, fEnd
////	VECTOR vStartPos, vStartRot, vEndPos, vEndRot
////	
//	WHILE(TRUE)
//		TEXT_LABEL txtLabel
//		txtLabel = "Time: "
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
//			txtLabel += ROUND(GET_TIME_POSITION_IN_RECORDING(vehRival2))
//		ELIF GET_GAME_TIMER() < iCountDown
//			SET_TEXT_COLOUR(255, 255, 0, 255)
//			txtLabel += iCountDown - GET_GAME_TIMER()
//		ENDIF
//		SET_TEXT_SCALE(0.5, 0.5)
//		SET_TEXT_CENTRE(TRUE)
//		IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehRival1)
//			SET_TEXT_COLOUR(255, 0, 0, 255)
//		ENDIF
//		DISPLAY_TEXT(0.5, 0.1, txtLabel)
//		
//		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//		
//		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
//			FREEZE_ENTITY_POSITION(vehRival2, TRUE)
//		ELSE
//			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival2) - 500.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrecChase)), sCarrecChase), 0.5, 255, 0, 0, 255)
//			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival2) - 1000.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrecChase)), sCarrecChase), 0.25, 255, 0, 0, 175)
//			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival2) - 1500.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrecChase)), sCarrecChase), 0.1, 255, 0, 0, 100)
//			
//			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival2) + 500.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrecChase)), sCarrecChase), 0.5, 255, 0, 0, 255)
//			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival2) + 1000.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrecChase)), sCarrecChase), 0.25, 255, 0, 0, 175)
//			DRAW_DEBUG_SPHERE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, CLAMP(GET_TIME_POSITION_IN_RECORDING(vehRival2) + 1500.0, 0.0, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(500, sCarrecChase)), sCarrecChase), 0.1, 255, 0, 0, 100)
//		ENDIF
//		
//		CLEAR_AREA(GET_ENTITY_COORDS(vehRival1), 10.0, TRUE)
//		
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
//		
//		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
//		
//		IF IS_BUTTON_JUST_PRESSED(PAD1, TRIANGLE)
//			IF bOnce = TRUE
//				IF GET_GAME_TIMER() > iCountDown
//					CLEAR_AREA(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, sCarrecChase), 500.0, TRUE)
//					
//					LOAD_SCENE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, sCarrecChase))
//					
//					CLEANUP_UBER_PLAYBACK(TRUE)
//					
//					fPlaybackSpeed = 1.0
//					
//					INITIALISE_UBER_PLAYBACK(sCarrecChase, 500)
//					
//					UberCopVariables()
//					
//					bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
//					
//					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehRival1)
//					
//					SET_VEHICLE_POSITION(vehRival1, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, sCarrecChase), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, 0.0, sCarrecChase)))
//					
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//					
//					SET_VEHICLE_ENGINE_ON(vehRival1, TRUE, TRUE)
//					
//					SET_PED_HELMET(PLAYER_PED_ID(), FALSE)
//					
//					iCountDown = GET_GAME_TIMER() + 2000
//				ENDIF
//			ELSE
//				STOP_RECORDING_VEHICLE(vehRival1)
//				
//				STOP_PLAYBACK_RECORDED_VEHICLE(vehRival2)
//				
//				iCountDown = -1
//				
//				bOnce = TRUE
//			ENDIF
//		ENDIF
//		
//		IF IS_BUTTON_PRESSED(PAD1, SQUARE)
//			SET_VEHICLE_FORWARD_SPEED(vehRival1, GET_ENTITY_SPEED(vehRival1) + 1.0)
//		ENDIF
//		
//		IF iCountDown != -1
//			IF bOnce = TRUE
//				IF GET_GAME_TIMER() > iCountDown
//					FREEZE_ENTITY_POSITION(vehRival2, FALSE)
//					
//					START_PLAYBACK_RECORDED_VEHICLE(vehRival2, 500, sCarrecChase)
//					
//					START_RECORDING_VEHICLE(vehRival1, 999, sCarrecChase, TRUE)
//					
//					bOnce = FALSE
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
//			SET_PLAYBACK_SPEED(vehRival2, fPlaybackSpeed)
//			bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
//			PRELOAD_ALL_CHASE_RECORDINGS(GET_TIME_POSITION_IN_RECORDING(vehRival2), sCarrecChase)
//			CREATE_ALL_WAITING_UBER_CARS()
//			UPDATE_UBER_PLAYBACK(vehRival2, fPlaybackSpeed)
//			fUberPlaybackDensitySwitchOffRange = 300
//		ENDIF
//		
////		SET_PLAYBACK_SPEED(vehRival1, fPlaybackSpeed)
////		
////		IF IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
////			IF fPlaybackSpeed > -1.0
////				fPlaybackSpeed -= 1.0
////			ENDIF
////		ELIF IS_BUTTON_JUST_PRESSED(PAD1, DPADRIGHT)
////			IF fPlaybackSpeed < 1.0
////				fPlaybackSpeed += 1.0
////			ENDIF
////		ELIF IS_BUTTON_JUST_PRESSED(PAD1, DPADUP)
////			fStart = GET_TIME_POSITION_IN_RECORDING(vehRival1)
////			vStartPos = GET_ENTITY_COORDS(vehRival1)
////			vStartRot = GET_ENTITY_ROTATION(vehRival1)
////		ELIF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
////			fEnd = GET_TIME_POSITION_IN_RECORDING(vehRival1)
////			vEndPos = GET_ENTITY_COORDS(vehRival1)
////			vEndRot = GET_ENTITY_ROTATION(vehRival1)
////		ENDIF
////		
////		IF GET_TIME_POSITION_IN_RECORDING(vehRival1) <= fStart
////		OR GET_TIME_POSITION_IN_RECORDING(vehRival1) >= fEnd
////			SET_ENTITY_COORDS(vehRival2, GET_ENTITY_COORDS(vehRival1))
////			SET_ENTITY_ROTATION(vehRival2, GET_ENTITY_ROTATION(vehRival1))
////		ELSE
////			//SET_ENTITY_COORDS(vehRival2, GET_INTERP_POINT_VECTOR(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, fStart, sCarrecRace), GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(500, fEnd, sCarrecRace), fStart, fEnd, GET_TIME_POSITION_IN_RECORDING(vehRival1)))
////			//SET_ENTITY_ROTATION(vehRival2, GET_INTERP_POINT_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, fStart, sCarrecRace), GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(500, fEnd, sCarrecRace), fStart, fEnd, GET_TIME_POSITION_IN_RECORDING(vehRival1)))
////			SET_ENTITY_COORDS(vehRival2, GET_INTERP_POINT_VECTOR(vStartPos, vEndPos, 0.0, 1.0, (1.0 / (fEnd - fStart)) * (GET_TIME_POSITION_IN_RECORDING(vehRival1) - fStart)))
////			SET_ENTITY_ROTATION(vehRival2, GET_INTERP_POINT_VECTOR(vStartRot, vEndRot, 0.0, 1.0, (1.0 / (fEnd - fStart)) * (GET_TIME_POSITION_IN_RECORDING(vehRival1) - fStart)))
////		ENDIF
//		
//	//		IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
//	//			IF bVisible = TRUE
//	//				SET_ENTITY_VISIBLE(vehRival1, FALSE)
//	//				SET_ENTITY_VISIBLE(vehPlayer, FALSE)
//	//				
//	//				bVisible = FALSE
//	//			ELSE
//	//				SET_ENTITY_VISIBLE(vehRival1, TRUE)
//	//				SET_ENTITY_VISIBLE(vehPlayer, TRUE)
//	//				
//	//				bVisible = TRUE
//	//			ENDIF
//	//		ENDIF
//		
////		IF bOnce = FALSE
////			IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(vehRival1)
////				SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival1, 0.0)
////				
//////				fPlaybackSpeed = 1.0
////				
////				START_RECORDING_VEHICLE(vehRival1, 999, sCarrecChase, TRUE)
////			ELSE
////				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
////					SET_ENTITY_COORDS(vehPlayer, GET_ENTITY_COORDS(vehRival1))
////					SET_ENTITY_ROTATION(vehPlayer, GET_ENTITY_ROTATION(vehRival1))
////					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
////					
////					VECTOR vCoords1 = GET_ENTITY_COORDS(vehRival1)
////					VECTOR vCoords2 = GET_ENTITY_COORDS(vehPlayer)
////					
////					PRINTLN("vCoords2 - vCoords1 = ", vCoords2.Z - vCoords1.Z)
////					
////					SET_ENTITY_COORDS(vehRival2, GET_ENTITY_COORDS(vehRival1) - <<0.0, 0.0, vCoords2.Z - vCoords1.Z>>)
////					SET_ENTITY_ROTATION(vehRival2, GET_ENTITY_ROTATION(vehRival1))
////					SET_VEHICLE_ENGINE_ON(vehRival2, TRUE, FALSE)
////					ACTIVATE_PHYSICS(vehRival2)
////				ELSE
////					STOP_RECORDING_VEHICLE(vehRival2)
////					bOnce = TRUE
////				ENDIF
////				
//////				fPlaybackSpeed = 0.0
////			ENDIF
////		ENDIF
////		ENDIF
//		
//		WAIT(0)
//	ENDWHILE
	
	//Fetch vehPlayer from carsteal3_launcher
//	IF NOT DOES_ENTITY_EXIST(vehPlayer)
//		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF DOES_ENTITY_EXIST(vehPlayer)
//			IF NOT IS_VEHICLE_MODEL(vehPlayer, F620)
//			OR NOT IS_VEHICLE_DRIVEABLE(vehPlayer)
//				vehPlayer = NULL
//			ELSE
//				SET_ENTITY_AS_MISSION_ENTITY(vehPlayer)
//			ENDIF
//		ENDIF
//	ENDIF

	IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(VECTOR_ZERO, TRUE)
		SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<135.7141, -432.9040, 40.0295>>, 162.7001, FALSE)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])	//Franklin
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[3])
				//Transfer the global index to a local index and grab ownership of the entity.
				sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = g_sTriggerSceneAssets.ped[3]
				SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE, TRUE)
				
				PED_INDEX pedLastPlayer
				pedLastPlayer = PLAYER_PED_ID()
				
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
				
				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(pedLastPlayer)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])	//Car (FELON)
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[0])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehCutscene = g_sTriggerSceneAssets.veh[0]
			SET_ENTITY_AS_MISSION_ENTITY(vehCutscene, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])	//Player Car (F620)
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[1])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehPlayer = g_sTriggerSceneAssets.veh[1]
			SET_ENTITY_AS_MISSION_ENTITY(vehPlayer, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])	//Devin
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			//Transfer the global index to a local index and grab ownership of the entity.
			pedDevin = g_sTriggerSceneAssets.ped[0]
			SET_ENTITY_AS_MISSION_ENTITY(pedDevin, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])	//Molly
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
			//Transfer the global index to a local index and grab ownership of the entity.
			pedMolly = g_sTriggerSceneAssets.ped[1]
			SET_ENTITY_AS_MISSION_ENTITY(pedMolly, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])	//Devin's Car (ADDER)
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[2])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehDevin = g_sTriggerSceneAssets.veh[2]
			SET_ENTITY_AS_MISSION_ENTITY(vehDevin, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])	//Trevor
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[4])
			//Transfer the global index to a local index and grab ownership of the entity.
			sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = g_sTriggerSceneAssets.ped[4]
			SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_REPLAY_IN_PROGRESS()
		IF HAS_THIS_CUTSCENE_LOADED("CAR_1_INT_CONCAT")
			
			THEFEED_FLUSH_QUEUE()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			IF DOES_ENTITY_EXIST(vehPlayer)
			AND NOT IS_ENTITY_DEAD(vehPlayer)
				REGISTER_ENTITY_FOR_CUTSCENE(vehPlayer, "Franklins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedDevin)
			AND NOT IS_ENTITY_DEAD(pedDevin)
				REGISTER_ENTITY_FOR_CUTSCENE(pedDevin, "DEVIN", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedMolly)
			AND NOT IS_ENTITY_DEAD(pedMolly)
				REGISTER_ENTITY_FOR_CUTSCENE(pedMolly, "MOLLY", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehDevin)
			AND NOT IS_ENTITY_DEAD(vehDevin)
				REGISTER_ENTITY_FOR_CUTSCENE(vehDevin, "Devins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehCutscene)
			AND NOT IS_ENTITY_DEAD(vehCutscene)
				REGISTER_ENTITY_FOR_CUTSCENE(vehCutscene, "Car_1_Felon", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			AND NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			START_CUTSCENE()
			
			WAIT(0)
			
			DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
			DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_TREVOR)
			
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<110.811684, -421.141571, 36.196766>>, <<126.111618, -380.102112, 57.261459>>, 40.0, <<120.5374, -429.8324, 40.0967>>, 289.2337, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
			
			CLEAR_AREA(<<115.56944, -400.46335, 40.26558>>, 20.0, TRUE)
			
			DISABLE_PROCOBJ_CREATION()
			
			STOP_GAMEPLAY_HINT(TRUE)
			
			CLEAR_TEXT()
			
			bPreloaded = TRUE
		ENDIF
	ENDIF
	
	//Buddy peds shouldn't exit here
	SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	
	MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_CARSTEAL_1)

#IF IS_DEBUG_BUILD
	IF bSwitchCamDebugScenarioEnabled
		SETUP_SPLINE_CAM_1_NODE_ARRAY(scsSwitchCam1, vehPlayer, viSceneBike2)
		SETUP_SPLINE_CAM_2_NODE_ARRAY(scsSwitchCam2, vehPolice2)
		widGroup = START_WIDGET_GROUP("MISSION DEBUG")
		STOP_WIDGET_GROUP()
		CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam1, "Car", "Bike", widGroup)
		CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam2, "Bike", "Game", widGroup)
		CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS(widGroup)
		WHILE(TRUE)
			UPDATE_SPLINE_CAM_HELPERS()
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam1)
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam2)
			WAIT(0)
		ENDWHILE
	ENDIF
#ENDIF

	WHILE(TRUE)
		//Video Recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_IFoughtTheLaw")
		
		//Fail Checks
		DEATH_CHECKS()
		
		//Uber
		IF eMissionObjective <= stageRace
			IF DOES_ENTITY_EXIST(vehRival1)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
					RUBBER_BANDING(GET_TIME_POSITION_IN_RECORDING(vehRival1), sCarrecSetup)
					SET_PLAYBACK_SPEED(vehRival1, fPlaybackSpeed)
					bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
					CREATE_ALL_WAITING_UBER_CARS()
					UPDATE_UBER_PLAYBACK(vehRival1, fPlaybackSpeed)
					fUberPlaybackDensitySwitchOffRange = 300
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehRival2)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
					SET_PLAYBACK_SPEED(vehRival2, fPlaybackSpeed)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehPlayer)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					SET_PLAYBACK_SPEED(vehPlayer, fPlaybackSpeed * 1.25)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehPolice1)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
					SET_PLAYBACK_SPEED(vehPolice1, fPlaybackSpeed * 1.25)
				ENDIF
			ENDIF
		ELIF eMissionObjective = stageChase
		AND bInitStage
			IF DOES_ENTITY_EXIST(vehPolice1)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
					RUBBER_BANDING(GET_TIME_POSITION_IN_RECORDING(vehPolice1), sCarrecCop)
					SET_PLAYBACK_SPEED(vehPolice1, fPlaybackSpeed)
					bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
					CREATE_ALL_WAITING_UBER_CARS()
					UPDATE_UBER_PLAYBACK(vehPolice1, fPlaybackSpeed)
					fUberPlaybackDensitySwitchOffRange = 300
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehRival1)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
					SET_PLAYBACK_SPEED(vehRival1, fPlaybackSpeed)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehRival1)
			AND DOES_ENTITY_EXIST(vehRival2)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival2)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRival1)
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehRival2, GET_TIME_POSITION_IN_RECORDING(vehRival1))
					ELSE
						SET_PLAYBACK_SPEED(vehRival2, fPlaybackSpeed)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehPlayer)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					IF DOES_ENTITY_EXIST(vehPolice1)
					AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice1)
					AND GET_TIME_POSITION_IN_RECORDING(vehPolice1) < 153107.800
						SET_PLAYBACK_SPEED(vehPlayer, fPlaybackSpeed)
					ELSE
						SET_PLAYBACK_SPEED(vehPlayer, 0.7)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Temp fix to force the interior to uncap when you're close to it.
		IF bSetUncapped
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),<<479.0568, -1316.8253, 28.2038>>) < 240.0
				SET_INTERIOR_CAPPED(INTERIOR_V_CHOPSHOP, FALSE)
				bSetUncapped = FALSE
			ENDIF
		ENDIF
		
		//Chop Shop
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<528.999939, -1417.176270, 27.145472>>, <<424.988098, -1212.274902, 139.672485>>, 150.0)
		OR eMissionObjective = cutHome
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ChopShopDoors")
				CREATE_MODEL_HIDE(<<498.7252, -1317.7551, 28.2534>>, 1.0, PROP_SC1_06_GATE_L, TRUE)
				CREATE_MODEL_HIDE(<<494.6904, -1312.0663, 28.2534>>, 1.0, PROP_SC1_06_GATE_R, TRUE)
				
				SET_LABEL_AS_TRIGGERED("ChopShopDoors", TRUE)
			ENDIF
			
			IF intChopShop = NULL
				intChopShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<481.1439, -1314.4697, 28.2017>>, "v_chopshop")
			ELSE
				IF bPinnedChopShop = FALSE
					PIN_INTERIOR_IN_MEMORY(intChopShop)
					
					IF NOT IS_INTERIOR_READY(intChopShop)
						PRINTLN("PINNING INTERIOR...")
					ELSE
						bPinnedChopShop = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<528.999939, -1417.176270, 27.145472>>, <<424.988098, -1212.274902, 139.672485>>, 150.0)
			IF HAS_LABEL_BEEN_TRIGGERED("ChopShopDoors")
				REMOVE_MODEL_HIDE(<<498.7252, -1317.7551, 28.2534>>, 1.0, PROP_SC1_06_GATE_L, FALSE)
				REMOVE_MODEL_HIDE(<<494.6904, -1312.0663, 28.2534>>, 1.0, PROP_SC1_06_GATE_R, FALSE)
				
				SET_LABEL_AS_TRIGGERED("ChopShopDoors", FALSE)
			ENDIF
			
			IF intChopShop <> NULL
				IF bPinnedChopShop = TRUE
					UNPIN_INTERIOR(intChopShop)
					
					bPinnedChopShop = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		//Cinematic Camera
//		CINEMATIC_CAMERA()
		
		//Debug Routine
		#IF IS_DEBUG_BUILD
			debugRoutine()
		#ENDIF
		
		//Hide HUD/Radar
		IF bRadar = FALSE
		//OR IS_PHONE_ONSCREEN()
			//HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF IS_RADAR_PREFERENCE_SWITCHED_ON()
				DISPLAY_RADAR(FALSE)
			ENDIF
			
			IF IS_HUD_PREFERENCE_SWITCHED_ON()
				DISPLAY_HUD(FALSE)
			ENDIF
		ELSE
			IF IS_RADAR_PREFERENCE_SWITCHED_ON()
				DISPLAY_RADAR(TRUE)
			ENDIF
			
			IF IS_HUD_PREFERENCE_SWITCHED_ON()
				DISPLAY_HUD(TRUE)
			ENDIF
		ENDIF
		
		//Special Ability
		IF NOT bAbilityUsed
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				bAbilityUsed = TRUE
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CS1_SPECIAL_USED)
			ENDIF
		ENDIF
		
		//Cam Switch
		IF eMissionObjective = stageChase
			IF DOES_CAM_EXIST(scsSwitchCam2.ciSpline)
			AND IS_CAM_ACTIVE(scsSwitchCam2.ciSpline)
				INT iLeftX = 0
				INT iLeftY = 0
				INT iRightX = 0
				INT iRightY = 0
				
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
				
				UPDATE_SPLINE_CAM(scsSwitchCam2)
				
				IF GET_CAM_SPLINE_PHASE(scsSwitchCam2.ciSpline) >= 1.0
				OR iLeftX <> 0 OR iLeftY <> 0 OR iRightX <> 0 OR iRightY <> 0
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) <> CAM_VIEW_MODE_FIRST_PERSON
						STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, fEndSceneDistanceToBlend, CAM_SPLINE_SLOW_IN_SMOOTH)
					ENDIF
					SET_CAM_ACTIVE(scsSwitchCam2.ciSpline, FALSE)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					IF DOES_CAM_EXIST(scsSwitchCam2.ciSpline)
//						DESTROY_CAM(scsSwitchCam2.ciSpline)
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Stats
		IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) != NULL
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
		ENDIF
		
		//Stop peds attacking player
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		
		LOAD_UNLOAD_ASSETS()
		
		AUDIO_CONTROLLER()
		
		//Instance Priority
		IF eMissionObjective >= stageRace AND eMissionObjective <= stageDriveHome
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
		ELSE
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		ENDIF
		
		//Objective switch
		SWITCH eMissionObjective 
			CASE initMission
				IF HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
				AND HAS_MODEL_LOADED_CHECK(F620)
					initialiseMission()
				ENDIF
			BREAK
			CASE cutIntro
				CutsceneIntro()
			BREAK
			CASE stageArrive
				IF HAS_RECORDING_LOADED_CHECK(001, sCarrecGasStation)
				AND HAS_RECORDING_LOADED_CHECK(002, sCarrecGasStation)
					Arrive()
				ENDIF
			BREAK
			CASE cutRace
				IF HAS_MODEL_LOADED_CHECK(IG_CAR3GUY2)
				AND HAS_MODEL_LOADED_CHECK(IG_CAR3GUY1)
				AND HAS_MODEL_LOADED_CHECK(CHEETAH)
				AND HAS_MODEL_LOADED_CHECK(ENTITYXF)
				AND HAS_RECORDING_LOADED_CHECK(500, sCarrecSetup)
				AND HAS_RECORDING_LOADED_CHECK(501, sCarrecSetup)
					CutsceneRace()
				ENDIF
			BREAK
			CASE stageRace
				IF HAS_RECORDING_LOADED_CHECK(500, sCarrecSetup)
				AND HAS_RECORDING_LOADED_CHECK(501, sCarrecSetup)
				AND HAS_RECORDING_LOADED_CHECK(401, sCarrecSetup)
				AND HAS_RECORDING_LOADED_CHECK(403, sCarrecSetup)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictCar3PullOver)
					Race()
				ENDIF
			BREAK
			CASE cutSwitch
				IF HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				AND HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				AND HAS_MODEL_LOADED_CHECK(POLICEB)
				AND HAS_MODEL_LOADED_CHECK(PROP_PLAYER_PHONE_01)
				AND HAS_MODEL_LOADED_CHECK(PROP_DONUT_02)
				AND HAS_MODEL_LOADED_CHECK(PROP_DONUT_02B)
				AND HAS_RECORDING_LOADED_CHECK(600, "ALuberSetup")
				AND HAS_ANIM_DICT_LOADED_CHECK(strAnimDictCar3LeadInOut)
				AND HAS_ANIM_DICT_LOADED_CHECK(strAnimDictBikeIdle)
//				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPhoneUse)
					CutsceneSwitch()
				ENDIF
			BREAK
			CASE stageChase
				IF HAS_RECORDING_LOADED_CHECK(500, sCarrecCop)
				AND HAS_RECORDING_LOADED_CHECK(400, sCarrecCop)
				AND HAS_RECORDING_LOADED_CHECK(401, sCarrecCop)
				AND HAS_RECORDING_LOADED_CHECK(402, sCarrecCop)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictCar3)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictCar3PullOver)
					Chase()
				ENDIF
			BREAK
			CASE cutBridge
				CutsceneBridge()
			BREAK
			CASE stageDriveHome
				IF HAS_MODEL_LOADED_CHECK(P_LD_ID_CARD_01)
					DriveHome()
				ENDIF
			BREAK
			CASE cutHome
				IF HAS_MODEL_LOADED_CHECK(FELON)
				AND HAS_MODEL_LOADED_CHECK(S_M_Y_DWSERVICE_02)
				AND HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_MOLLY))
				AND HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_DEVIN))
				AND HAS_RECORDING_LOADED_CHECK(001, sCarrecGarage)
				AND HAS_RECORDING_LOADED_CHECK(002, sCarrecGarage)
				AND HAS_RECORDING_LOADED_CHECK(003, sCarrecGarage)
					CutsceneHome()
				ENDIF
			BREAK
			CASE passMission
				missionPassed()
			BREAK
			CASE failMission
				missionFailed()
			BREAK
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
	
//Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
