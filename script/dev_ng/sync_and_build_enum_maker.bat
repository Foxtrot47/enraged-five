@echo off

echo.Syncing Launcher Files
p4 sync %RS_SCRIPTBRANCH%/GrabLatestMapSceneXmlFiles.bat
p4 sync %RS_SCRIPTBRANCH%/enum_maker.txt
p4 sync %RS_SCRIPTBRANCH%/enum_maker.bat
p4 sync %RS_TOOLSROOT%/config.xml

echo.Syncing Generation Files
call %RS_SCRIPTBRANCH%/GrabLatestMapSceneXmlFiles.bat -nopause

echo.Running Enum Maker
call %RS_SCRIPTBRANCH%/enum_maker.bat
