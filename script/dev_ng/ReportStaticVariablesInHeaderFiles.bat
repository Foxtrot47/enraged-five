@echo off

rem call setenv.bat

rem --list_sc_files
%PYTHONHOME%\python.exe ReportStaticVariablesInHeaderFiles.py X:\gta5\script\dev_ng\singleplayer\sco\DEBUG X:\gta5\script\dev_ng\singleplayer\sco\DEBUG\statics_in_headers.txt

if errorlevel 1 (
	echo ReportStaticVariablesInHeaderFiles.py failed
) else (
	echo ReportStaticVariablesInHeaderFiles.py succeeded
)

rem pause
