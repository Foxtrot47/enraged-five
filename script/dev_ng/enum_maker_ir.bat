@ECHO OFF
REM enum_maker_ir.bat
REM Description : Enum maker converted to iron ruby
REM Author : Liam Anderson <liam.anderson@rockstarnorth.com>
REM Date : 20 May 2014

call setenv

SET CALLER=%0 
SET PROCESS=%RS_TOOLSROOT%/ironlib/util/enum_maker.rb

if "%1"=="-cruisecontrol" GOTO :CRUISECONTROL
if "%1"=="-local" GOTO :LOCAL
GOTO :SCM


:CRUISECONTROL
SET ADDITIONAL=-scm -submit
ECHO Cruise Control Mode this will create and submit a change list at the end of the job
GOTO :START

:LOCAL
SET ADDITIONAL=
ECHO Local Mode this will only generate the file
GOTO :START

:SCM
SET ADDITIONAL=-scm
ECHO SCM this will create a change list but not submit it
GOTO :START

REM Main for this script
:START
SET ARGS=%ADDITIONAL% %RS_SCRIPTBRANCH%\core\game\data\model_enums.sch %RS_SCRIPTBRANCH%\enum_maker.txt 

ECHO *************************************************************************************
ECHO %CALLER% 
ECHO %PROCESS% %ARGS%
ECHO start %date% %time%
ECHO *************************************************************************************
ECHO.

call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSROOT%\bin\ironruby\bin\ir64.exe %PROCESS% %ARGS%
SET RETCODE=%ERRORLEVEL%
ECHO %PROCESS% RETCODE: %RETCODE%

ECHO.
ECHO completed %date% %time%
EXIT /B %RETCODE%