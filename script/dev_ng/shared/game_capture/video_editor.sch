// *****************************************************************************************
//
//      SCRIPT NAME    	:   video_editor.sch
//      AUTHOR          :	James Strain
//      DESCRIPTION     :   Script functions used for initializing/accessing the video editor
//
// *****************************************************************************************
USING "types.sch"
USING "commands_replay.sch"

PROC Register_All_ReplayEditor_Fx()

	// These should ideally match the same FX used in appCamera.sc
	REGISTER_EFFECT_FOR_REPLAY_EDITOR( "No_Filter", TRUE )
	REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam1" , TRUE )
    REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam2" , TRUE )
    REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam3" , TRUE )
    REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam4" , TRUE )

    REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam5" , TRUE )
    REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam6" , TRUE )
    REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam7" , TRUE )
    REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam9" , TRUE )
    REGISTER_EFFECT_FOR_REPLAY_EDITOR( "phone_cam12" , FALSE )

ENDPROC
