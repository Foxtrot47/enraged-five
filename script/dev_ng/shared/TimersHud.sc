

USING "Shared_hud_displays.sch"
USING "Net_mission_details_hud.sch"
USING "Transition_Invites.sch"
USING "Transition_Common.sch"
USING "Transition_Saving.sch"


FLOAT AlignX
FLOAT AlignY
FLOAT SIZEX = 0.952
FLOAT SIZEY = 0.949

#IF IS_DEBUG_BUILD
BOOL bCreateWidgets 

#ENDIF

FUNC BOOL SHOULD_DISPLAY_OVER_PAUSE_MENU()

	IF IS_THIS_PLAYER_ACTIVE_IN_CORONA(PLAYER_ID()) 
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


PROC DISPLAY_OVER_PAUSE_MENU(BOOL isActive)

	IF SHOULD_DISPLAY_OVER_PAUSE_MENU()
		IF isActive
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		ELSE
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
		ENDIF
	ENDIF

ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_TIMERHUD_WIDGETS()

	IF bCreateWidgets = FALSE
		START_WIDGET_GROUP("TIMER HUD")
	
			START_WIDGET_GROUP("Inner workings")
			
			ADD_WIDGET_BOOL("MPGlobalsHud.g_bHasAnythingChangedTimerHud", MPGlobalsHud.g_bHasAnythingChangedTimerHud)
			
			
			STOP_WIDGET_GROUP()
	
	
			START_WIDGET_GROUP("Individual Drawing")
				ADD_WIDGET_FLOAT_SLIDER("AlignX", AlignX, -2, 2, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("AlignY", AlignY, -2, 2, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("SIZEX", SIZEX, -2, 2, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("SIZEY", SIZEY, -2, 2, 0.001)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()	
	
		bCreateWidgets = TRUE
		
	ENDIF
	
	
	

ENDPROC

#ENDIF

PROC RUN_TIMERHUD()

	INT I , J , K 
		
	BOOL SpaceTaken[NUMBER_SPACES_FOR_HUD_ELEMENTS]

	IF BUSYSPINNER_IS_DISPLAYING()
		IF MPGlobalsScoreHud.bNumberOfInstructionalButtonsRowsUnderHud = 0
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		ENDIF
	ENDIF
	
	EXTEND_ALL_HUD_ELEMENTS()




	IF CAN_INGAME_HUD_ELEMENTS_DISPLAY() 

			#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("CAN_INGAME_HUD_ELEMENTS_DISPLAY")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("RUN_TIMERHUD - TOP ")NET_NL()
			ENDIF
		#ENDIF
		
		
//		HIDE_ALL_BOTTOM_RIGHT_HUD()
		
		// Update fixed order every 100 frames for spectators 
		IF GET_FRAME_COUNT() % 100 = 0
			IF IS_PLAYER_SPECTATING(PLAYER_ID())
				FORCE_TIMER_ORDER_REFRESH()
			ENDIF
		ENDIF
		
		IF HAS_ANY_PROGRESSHUD_ACTIVATION_CHANGED()
			MPGlobalsHud.g_bHasAnythingChangedTimerHud = TRUE
		ENDIF	
		
		INT iCount
		
		FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
			iCount = 0 //Timers
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_TIMER, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] 
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker PROGRESSHUD_TIMER being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_TIMER:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 1 //Single Numbers
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SINGLE_NUMBER, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I] 
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_SINGLENUMBER being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_SINGLE_NUMBER:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 2 
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I] 
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_DOUBLENUMBER being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_DOUBLE_NUMBER:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 3 
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I]
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_DOUBLENUMPLACE being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_DOUBLE_NUMBER_PLACE:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 4 
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_CHECKPOINT, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I] 
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_CHECKPOINT being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_CHECKPOINT:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 5 
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_METER, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I]  
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_METER being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_METER:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 6 
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SCORE, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] 
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_SCORE being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_SCORE:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 7 
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_ELIMINATION, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I]
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_ELIMINATION being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_ELIMINATION:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 8 
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_WINDMETER, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I]
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_WIND being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_WINDMETER:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 9 
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_BIG_RACE_POSITION, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I]
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_BIGRACEPOSITION being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_BIG_RACE_POSITION:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 10
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SPRITE_METER, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I]
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_SPRITEMETER being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_SPRITE_METER:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 11
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FOUR_ICON_BAR, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I]
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_FOURICONBAR being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_FOUR_ICON_BAR:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 12
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FIVE_ICON_SCORE_BAR, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I]
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_FIVEICONSCOREBAR being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_FIVE_ICON_SCORE_BAR:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 13
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SIX_ICON_BAR, I)
				MPGlobalsHud.OrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I]
				
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL() NET_PRINT("OrderTracker ElementHud_SIXICONBAR being called =  ") NET_PRINT_INT(I) NET_NL()
				ENDIF
				
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_SIX_ICON_BAR:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
			iCount = 14
			IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_TEXT, I)
				MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I]
				#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("OrderTracker ElementHud_DOUBLETEXT being called =  ")NET_PRINT_INT(I) NET_NL()
				ENDIF
				IF g_bHUDOrderPrints
					PRINTLN("HUD_ORDER_PRINTS - -OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_DOUBLE_TEXT:")
					PRINTLN("HUD_ORDER_PRINTS - I: ", I)
					PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[iCount][",I,"]: ", MPGlobalsHud.OrderTracker[iCount][I])
					PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I])
					PRINTLN("HUD_ORDER_PRINTS - -/OrderTracker-")
					PRINTLN("HUD_ORDER_PRINTS - ")
				ENDIF
				#ENDIF
			ENDIF
		ENDFOR
		iCount = 0
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("loop 1")
		#ENDIF
		#ENDIF
		



		IF 	MPGlobalsHud.g_bHasAnythingChangedTimerHud = TRUE
		
			#IF IS_DEBUG_BUILD
				IF g_DisplayTimersDisplaying
					NET_NL()NET_PRINT("MPGlobalsHud.g_bHasAnythingChangedTimerHud = TRUE ")NET_NL()
				ENDIF
			#ENDIF
		
						
		//	INT iSlot = 0
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1

				iCount = 0 //Timers
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_TIMER, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[I] 
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[I] <> HUDORDER_DONTCARE))		
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[I] <> HUDORDER_TOP))		
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_TIMER Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_TIMER:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF
						ENDIF 
					ENDIF
				ENDIF
				
				iCount = 1 //Single Numbers
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SINGLE_NUMBER, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I] 
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I] <> HUDORDER_DONTCARE))	
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I] <> HUDORDER_TOP))	
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I]
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_SINGLENUMBER Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_SINGLE_NUMBER:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF
						ENDIF 
					ENDIF
				ENDIF
				
				iCount = 2 //Double Numbers
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I] 
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I]
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I]
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_DOUBLENUMBER Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_DOUBLE_NUMBER:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF 
					ENDIF
				ENDIF
				
				iCount = 3 //Double Numbers Place
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_NUMBER_PLACE, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I]
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_DOUBLENUMPLACE Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_DOUBLE_NUMBER_PLACE:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF
						ENDIF 
					ENDIF
				ENDIF
				
				iCount = 4 //Checkpoints
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_CHECKPOINT, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I] 
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I] <> HUDORDER_DONTCARE))		
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I] <> HUDORDER_TOP))		
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_CHECKPOINT Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_CHECKPOINT:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF 
					ENDIF
				ENDIF
				
				iCount = 5 //Meter
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_METER, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I] 
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I] <> HUDORDER_DONTCARE))	
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_METER Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_METER:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_METER.GenericMeter_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF 
					ENDIF
				ENDIF
				
				iCount = 6 //Score
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SCORE, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] 
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_SCORE Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_SCORE:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF
					ENDIF
				ENDIF
				
				iCount = 7 //Elimination
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_ELIMINATION, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I]
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_ELIMINATION Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_ELIMINATION:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF
					ENDIF
				ENDIF
				
				iCount = 8 //Windmeter
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_WINDMETER, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I]
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_WIND Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_WINDMETER:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_WIND.eGenericMeter_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF
					ENDIF
				ENDIF
				
				iCount = 9 //Big Race Position
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_BIG_RACE_POSITION, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I]
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_BIGRACEPOSITION Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_BIG_RACE_POSITION:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF
					ENDIF
				ENDIF
				iCount = 10 //Sprite Meter
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SPRITE_METER, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I]
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I] 
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_SPRITEMETER Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_SPRITE_METER:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF
					ENDIF
				ENDIF
				iCount = 11 //Four Icon Bar
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FOUR_ICON_BAR, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I]
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I]
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_FOURICONBAR Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_FOUR_ICON_BAR:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF
					ENDIF
				ENDIF
				iCount = 12 //Five Icon Score Bar
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_FIVE_ICON_SCORE_BAR, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I]
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I]
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_FIVEICONSCOREBAR Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_FIVE_ICON_SCORE_BAR:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF
					ENDIF
				ENDIF
				iCount = 13 //Four Icon Bar
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_SIX_ICON_BAR, I)
					MPGlobalsHud.OrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I]
					
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I]
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I]
							
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_SIXICONBAR Numbers being called =  ") NET_PRINT_INT(I) NET_NL()
							ENDIF
							
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_SIX_ICON_BAR:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericFourIconBar_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				iCount = 14 //Double Text
				IF IS_PROGRESSHUD_ACTIVATION_ON_INDEXED(PROGRESSHUD_DOUBLE_TEXT, I)
					MPGlobalsHud.OrderTracker[iCount][I] =  MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I]
					IF MPGlobalsHud.OrderTracker[iCount][I] <> HUDORDER_FREEROAM
						IF MPGlobalsHud.FixedOrderTracker[iCount][I] <> MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I] 
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I] = HUDORDER_DONTCARE)
						OR (MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I] <> HUDORDER_DONTCARE))
						AND ((MPGlobalsHud.FixedOrderTracker[iCount][I] = HUDORDER_NOTDISPLAYING AND MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I] = HUDORDER_TOP)
						OR (MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I] <> HUDORDER_TOP))
							MPGlobalsHud.FixedOrderTracker[iCount][I] = MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I]
							#IF IS_DEBUG_BUILD
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("ElementHud_DOUBLETEXT Numbers being called =  ")NET_PRINT_INT(I) NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - PROGRESSHUD_DOUBLE_TEXT:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - iCount: ", iCount)
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[iCount][",I,"]: ", MPGlobalsHud.FixedOrderTracker[iCount][I])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[",I,"]: ", MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_HUDOrder[I])
								PRINTLN("HUD_ORDER_PRINTS - -/FIXED OrderTracker-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
							#ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("loop 2")
			#ENDIF
			#ENDIF
		
	//	ADD_SCRIPT_PROFILE_MARKER(SPData, "AFTER Anything changed", 0)
		
		
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				FOR J = 0 TO NUMBER_OF_DIFFERENT_HUD_ELEMENTS-1
					IF MPGlobalsHud.OrderTracker[J][I] <> MPGlobalsHud.FixedOrderTracker[J][I]
						MPGlobalsHud.FixedOrderTracker[J][I] = HUDORDER_NOTDISPLAYING	
					ENDIF
				ENDFOR
			ENDFOR
			
			FOR K = 0 TO NUMBER_SPACES_FOR_HUD_ELEMENTS-1
				SpaceTaken[K] = FALSE
			ENDFOR
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("loop 3")
			#ENDIF
			#ENDIF
		
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
				FOR J = 0 TO NUMBER_OF_DIFFERENT_HUD_ELEMENTS-1
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_BOTTOM

						#IF IS_DEBUG_BUILD
							IF SpaceTaken[0] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_BOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[0] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_BOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[0]: ", SpaceTaken[0])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[0] = TRUE

					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_SECONDBOTTOM

						#IF IS_DEBUG_BUILD
							IF SpaceTaken[1] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_SECONDBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[1] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_SECONDBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[1]: ", SpaceTaken[1])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[1] = TRUE
					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_THIRDBOTTOM

						#IF IS_DEBUG_BUILD
							IF SpaceTaken[2] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_THIRDBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[2] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_THIRDBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[2]: ", SpaceTaken[2])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[2] = TRUE
	
					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_FOURTHBOTTOM
						
						#IF IS_DEBUG_BUILD
							IF SpaceTaken[3] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_FOURTHBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[3] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_FOURTHBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[3]: ", SpaceTaken[3])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[3] = TRUE
						
					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_FIFTHBOTTOM
						#IF IS_DEBUG_BUILD
							IF SpaceTaken[4] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_FIFTHBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[4] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_FIFTHBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[4]: ", SpaceTaken[4])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[4] = TRUE
					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_SIXTHBOTTOM

						#IF IS_DEBUG_BUILD
							IF SpaceTaken[5] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_SIXTHBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[5] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_SIXTHBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[5]: ", SpaceTaken[5])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[5] = TRUE
					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_SEVENTHBOTTOM
						#IF IS_DEBUG_BUILD
							IF SpaceTaken[6] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_SEVENTHBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[6] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_SEVENTHBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[6]: ", SpaceTaken[6])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[6] = TRUE
					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_EIGHTHBOTTOM
						#IF IS_DEBUG_BUILD
							IF SpaceTaken[7] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_EIGHTHBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[7] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_EIGHTHBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[7]: ", SpaceTaken[7])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[7] = TRUE
					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_NINETHBOTTOM
						#IF IS_DEBUG_BUILD
							IF SpaceTaken[8] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_NINETHBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[8] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_NINETHBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[8]: ", SpaceTaken[8])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[8] = TRUE
					ENDIF
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_TENTHBOTTOM
						#IF IS_DEBUG_BUILD
							IF SpaceTaken[9] = TRUE
								NET_NL()NET_PRINT("HUD DISPLAY: HUDORDER_TENTHBOTTOM SPACE ALREADY TAKEN")
							ENDIF
							IF g_DisplayTimersDisplaying
								NET_NL()NET_PRINT("SpaceTaken[9] = TRUE ")NET_NL()
							ENDIF
							IF g_bHUDOrderPrints
								PRINTLN("HUD_ORDER_PRINTS - -Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - HUDORDER_TENTHBOTTOM:")
								PRINTLN("HUD_ORDER_PRINTS - I: ", I)
								PRINTLN("HUD_ORDER_PRINTS - J: ", J)
								PRINTLN("HUD_ORDER_PRINTS - SpaceTaken[9]: ", SpaceTaken[9])
								PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
								PRINTLN("HUD_ORDER_PRINTS - -/Double Loop-")
								PRINTLN("HUD_ORDER_PRINTS - ")
							ENDIF
						#ENDIF
						SpaceTaken[9] = TRUE
					ENDIF
				ENDFOR
			ENDFOR

			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("loop 4")
			#ENDIF
			#ENDIF
			
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
				FOR J = 0 TO NUMBER_OF_DIFFERENT_HUD_ELEMENTS-1
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_DONTCARE
						FOR K = 0 TO NUMBER_SPACES_FOR_HUD_ELEMENTS-1
							IF SpaceTaken[K] = FALSE
								MPGlobalsHud.OrderTracker[J][I] = INT_TO_ENUM(HUDORDER, K+ENUM_TO_INT(HUDORDER_BOTTOM))
								SpaceTaken[K] = TRUE
								MPGlobalsHud.FixedOrderTracker[J][I] = MPGlobalsHud.OrderTracker[J][I] 
								#IF IS_DEBUG_BUILD
								IF g_bHUDOrderPrints
									PRINTLN("HUD_ORDER_PRINTS - ---Double Loop---")
									PRINTLN("HUD_ORDER_PRINTS - HUDORDER_DONTCARE:")
									PRINTLN("HUD_ORDER_PRINTS - I: ", I)
									PRINTLN("HUD_ORDER_PRINTS - J: ", J)
									PRINTLN("HUD_ORDER_PRINTS - K: ", K)
									PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
									PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[",J,"][",I,"]: ", MPGlobalsHud.FixedOrderTracker[J][I])
									PRINTLN("HUD_ORDER_PRINTS - ---/Double Loop---")
									PRINTLN("HUD_ORDER_PRINTS - ")
								ENDIF
								#ENDIF
								K = NUMBER_SPACES_FOR_HUD_ELEMENTS
							ENDIF
						ENDFOR
					ENDIF
				ENDFOR
			ENDFOR
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("loop 5")
			#ENDIF
			#ENDIF
		
			
			FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
				FOR J = 0 TO NUMBER_OF_DIFFERENT_HUD_ELEMENTS-1
					IF MPGlobalsHud.OrderTracker[J][I] = HUDORDER_TOP
						FOR K = NUMBER_SPACES_FOR_HUD_ELEMENTS-1 TO 1 STEP -1
							IF SpaceTaken[K] = FALSE
							AND SpaceTaken[K-1] = TRUE
								MPGlobalsHud.OrderTracker[J][I] = INT_TO_ENUM(HUDORDER, (K)+ENUM_TO_INT(HUDORDER_BOTTOM))
								SpaceTaken[K] = TRUE
								MPGlobalsHud.FixedOrderTracker[J][I] = MPGlobalsHud.OrderTracker[J][I] 
								#IF IS_DEBUG_BUILD
								IF g_bHUDOrderPrints
									PRINTLN("HUD_ORDER_PRINTS - ---Double Loop---")
									PRINTLN("HUD_ORDER_PRINTS - HUDORDER_TOP [0]:")
									PRINTLN("HUD_ORDER_PRINTS - I: ", I)
									PRINTLN("HUD_ORDER_PRINTS - J: ", J)
									PRINTLN("HUD_ORDER_PRINTS - K: ", K)
									PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
									PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[",J,"][",I,"]: ", MPGlobalsHud.FixedOrderTracker[J][I])
									PRINTLN("HUD_ORDER_PRINTS - ---/Double Loop---")
									PRINTLN("HUD_ORDER_PRINTS - ")
								ENDIF
								#ENDIF
								K = 0
							ELSE
								IF K = 1
								AND SpaceTaken[0] = FALSE
									MPGlobalsHud.OrderTracker[J][I] = INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_BOTTOM))
									SpaceTaken[0] = TRUE
									MPGlobalsHud.FixedOrderTracker[J][I] = MPGlobalsHud.OrderTracker[J][I] 
									#IF IS_DEBUG_BUILD
									IF g_bHUDOrderPrints
										PRINTLN("HUD_ORDER_PRINTS - ---Double Loop---")
										PRINTLN("HUD_ORDER_PRINTS - HUDORDER_TOP [1]:")
										PRINTLN("HUD_ORDER_PRINTS - I: ", I)
										PRINTLN("HUD_ORDER_PRINTS - J: ", J)
										PRINTLN("HUD_ORDER_PRINTS - K: ", K)
										PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.OrderTracker[",J,"][",I,"]: ", MPGlobalsHud.OrderTracker[J][I])
										PRINTLN("HUD_ORDER_PRINTS - MPGlobalsHud.FixedOrderTracker[",J,"][",I,"]: ", MPGlobalsHud.FixedOrderTracker[J][I])
										PRINTLN("HUD_ORDER_PRINTS - ---/Double Loop---")
										PRINTLN("HUD_ORDER_PRINTS - ")
									ENDIF
									#ENDIF
									K = 0
								ENDIF
							ENDIF
						ENDFOR
//						FOR K = 0 TO NUMBER_SPACES_FOR_HUD_ELEMENTS-1
//							IF SpaceTaken[K] = FALSE
//								MPGlobalsHud.OrderTracker[J][I] = INT_TO_ENUM(HUDORDER, K+ENUM_TO_INT(HUDORDER_BOTTOM))
//								SpaceTaken[K] = TRUE
//								MPGlobalsHud.FixedOrderTracker[J][I] = MPGlobalsHud.OrderTracker[J][I] 
//								K = NUMBER_SPACES_FOR_HUD_ELEMENTS
//							ENDIF
//						ENDFOR
					ENDIF
				ENDFOR
			ENDFOR
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("loop 6")
			#ENDIF
			#ENDIF
			
	//		ELSE
			MPGlobalsHud.g_bHasAnythingChangedTimerHud = FALSE 
		ENDIF
		
	//		IF MPGlobalsHud.g_bHasAnythingChangedTimerHud = FALSE 

		UI_ALIGNMENT TopOrBottom
		IF MPGlobalsScoreHud.bTopRightHud
			TopOrBottom = UI_ALIGN_TOP
		ELSE
			TopOrBottom = UI_ALIGN_BOTTOM
		ENDIF

			
		IF CAN_INGAME_HUD_ELEMENTS_DISPLAY() 	
			
			FOR J = 0 TO NUMBER_OF_DIFFERENT_HUD_ELEMENTS
				IF IS_PRIORITY_HUD_ELEMENT_VALID(HUD_PRIORITY_FIRST)
					IF IS_PROGRESSHUD_ACTIVATION_ON(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_FIRST))
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							PRINTLN("*FIRST PRIORITY HUD ELEMENT* - IS_PROGRESSHUD_ACTIVATION_ON(", GET_HUD_ELEMENT_STRING(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_FIRST)), ") = TRUE J = ")
						ENDIF
						#ENDIF
						
						FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
							INT iOrderIndex = GET_HUD_ELEMENT_ORDER_INDEX(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_FIRST))
							IF IS_HUD_ELEMENT_ORDER_INDEX_VALID(iOrderIndex)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									PRINTLN("				- MPGlobalsHud.FixedOrderTracker[", iOrderIndex, "][I] = ", ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[iOrderIndex][I]))
									PRINTLN("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ", (J+ENUM_TO_INT(HUDORDER_BOTTOM)))
								ENDIF
								#ENDIF
								
								IF MPGlobalsHud.FixedOrderTracker[iOrderIndex][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
								OR MPGlobalsHud.OrderTracker[iOrderIndex][I] = HUDORDER_FREEROAM
									IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[iOrderIndex][I])
										DISPLAY_OVER_PAUSE_MENU(TRUE)
										IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE
											SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
											SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
										ENDIF
										#IF IS_DEBUG_BUILD
										IF g_DisplayTimersDisplaying
											PRINTLN(GET_HUD_ELEMENT_ACTUALLY_DRAW_STRING(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_FIRST)), " I = ", I)
										ENDIF
										#ENDIF
										
										ACTUALLY_DRAW_GENERAL_HUD_ELEMENT(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_FIRST), I)
										
										IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE
											RESET_SCRIPT_GFX_ALIGN()	
										ENDIF
										DISPLAY_OVER_PAUSE_MENU(FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				IF IS_PRIORITY_HUD_ELEMENT_VALID(HUD_PRIORITY_SECOND)
					IF IS_PROGRESSHUD_ACTIVATION_ON(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_SECOND))
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							PRINTLN("*SECOND PRIORITY HUD ELEMENT* - IS_PROGRESSHUD_ACTIVATION_ON(", GET_HUD_ELEMENT_STRING(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_SECOND)), ") = TRUE J = ")
						ENDIF
						#ENDIF
						
						FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
							INT iOrderIndex = GET_HUD_ELEMENT_ORDER_INDEX(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_SECOND))
							IF IS_HUD_ELEMENT_ORDER_INDEX_VALID(iOrderIndex)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									PRINTLN("				- MPGlobalsHud.FixedOrderTracker[", iOrderIndex, "][I] = ", ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[iOrderIndex][I]))
									PRINTLN("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ", (J+ENUM_TO_INT(HUDORDER_BOTTOM)))
								ENDIF
								#ENDIF
								
								IF MPGlobalsHud.FixedOrderTracker[iOrderIndex][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
								OR MPGlobalsHud.OrderTracker[iOrderIndex][I] = HUDORDER_FREEROAM
									IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[iOrderIndex][I])
										DISPLAY_OVER_PAUSE_MENU(TRUE)
										IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE
											SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
											SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
										ENDIF
										#IF IS_DEBUG_BUILD
										IF g_DisplayTimersDisplaying
											PRINTLN(GET_HUD_ELEMENT_ACTUALLY_DRAW_STRING(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_SECOND)), " I = ", I)
										ENDIF
										#ENDIF
										
										ACTUALLY_DRAW_GENERAL_HUD_ELEMENT(GET_PRIORITY_HUD_ELEMENT(HUD_PRIORITY_SECOND), I)
										
										IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE
											RESET_SCRIPT_GFX_ALIGN()	
										ENDIF
										DISPLAY_OVER_PAUSE_MENU(FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_TIMER)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_TIMER)
					
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_TIMER) = TRUE J = ")NET_PRINT_INT(J)NET_NL()
					
					ENDIF
					#ENDIF
						
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[0][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[0][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
					
						IF MPGlobalsHud.FixedOrderTracker[0][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[0][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[0][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE
									SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
									SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								ENDIF
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_TIME_TIMER I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_GENERAL_TIME_TIMER(I, MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_Timer[I], 
																MPGlobalsScoreHud.ElementHud_TIMER.sGenericTimer_TimerTitle[I], MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_ExtraTime[I], 
																MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_MedalDisplay[I], MPGlobalsScoreHud.ElementHud_TIMER.bGenericTimer_TimerStyle[I], 
																MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_Colour[I], MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_FlashTimer[I], 
																MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_TitleNumber[I], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bIsPlayer[I],
																MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_ColourFlashType[I], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_ColourFlash[I], 
																MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bDisplayAsDashes[I], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_TitleColour[I], 
																MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bIsLiteral[I], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_FleckColour[I],
																MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_Powerup[I], MPGlobalsScoreHud.ElementHud_TIMER.GenericTimer_bHideUnusedZeros[I])
								IF MPGlobalsScoreHud.bCoronaUnderHud = FALSE
									RESET_SCRIPT_GFX_ALIGN()	
								ENDIF
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SINGLE_NUMBER)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_SINGLE_NUMBER)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SINGLE_NUMBER) = TRUE ")NET_NL()

					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[1][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[1][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
					
						IF MPGlobalsHud.FixedOrderTracker[1][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[1][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[1][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_SINGLE_BIG_NUMBER I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								ACTUALLY_DRAW_GENERAL_SINGLE_BIG_NUMBER(I, MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_Number[I], 
																		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_NumberTitle[I], 
																		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_Colour[I], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_FlashTimer[I], 
																		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.iGenericNumber_TitleNumber[I], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_bIsPlayer[I], 
																		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_NumberString[I], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.sGenericNumber_TitleColour[I], 
																		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.bGenericNumber_DrawInfinity[I] ,  MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_ColourFlashType[I], 
																		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_ColourFlash[I], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_FleckColour[I],
																		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_EnablePulsing[I], MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_PulseColour[I], 
																		MPGlobalsScoreHud.ElementHud_SINGLENUMBER.GenericNumber_PulseTime[I])
								RESET_SCRIPT_GFX_ALIGN()
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR	
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_DOUBLE_NUMBER)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER) = TRUE ")NET_NL()
					
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[2][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[2][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
					
						IF MPGlobalsHud.FixedOrderTracker[2][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[2][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[2][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER(I, MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_Number[I], 
																	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_NumberTwo[I], 
																	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.sGenericDoubleNumber_Title[I], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_COLOUR[I], 
																	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_FlashTimer[I], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.iGenericDoubleNumber_TitleNumber[I], 
																	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bIsPlayer[I], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_ColourFlashType[I], 
																	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_ColourFlash[I],  MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bDisplayWarning[I], 
																	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bUseNonPlayerFont[I], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_TitleCOLOUR[I],
																	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_FleckColour[I], MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_iAlpha[I],
																	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER.GenericDoubleNumber_bFlashTitle[I])
								RESET_SCRIPT_GFX_ALIGN()
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER_PLACE)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_DOUBLE_NUMBER_PLACE)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_NUMBER_PLACE) = TRUE ")NET_NL()
				
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[3][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[3][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
					
						IF MPGlobalsHud.FixedOrderTracker[3][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[3][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[3][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER_PLACE I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								ACTUALLY_DRAW_GENERAL_DOUBLE_BIG_NUMBER_PLACE(I, MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_Number[I], 
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_NumberTwo[I], 
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.sGenericDoubleNumberPlace_Title[I],
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_COLOUR[I], 
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_FlashTimer[I], 
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.iGenericDoubleNumberPlace_TitleNumber[I],
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_bIsPlayer[I] , 
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_ColourFlashType[I], 
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_ColourFlash[I], 
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_TitleCOLOUR[I],
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_FleckColour[I],
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_bCustomFont[I],
																			MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE.GenericDoubleNumberPlace_eCustomFont[I])
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_CHECKPOINT)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_CHECKPOINT)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_CHECKPOINT) = TRUE ")NET_NL()
					
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[4][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[4][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
					
						IF MPGlobalsHud.FixedOrderTracker[4][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[4][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[4][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_CHECKPOINT I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								ACTUALLY_DRAW_GENERAL_CHECKPOINT(I, MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_Number[I], MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_MaxNumber[I], 
																MPGlobalsScoreHud.ElementHud_CHECKPOINT.sGenericCheckpoint_Title[I], MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_Colour[I],
																MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_TitleNumber[I], MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_bIsPlayer[I],
																MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_FlashTimer[I], MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FreeRoamPos[I].x,
																MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FreeRoamPos[I].y, MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_ColourFlashType[I], 
																MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_ColourFlash[I], MPGlobalsScoreHud.ElementHud_CHECKPOINT.iGenericCheckpoint_iInBuiltMultiplyer[I], 
																#IF USE_TU_CHANGES GenericCheckpoint_Cross0[I], GenericCheckpoint_Cross1[I], GenericCheckpoint_Cross2[I], GenericCheckpoint_Cross3[I], GenericCheckpoint_Cross4[I], 
																GenericCheckpoint_Cross5[I], GenericCheckpoint_Cross6[I], GenericCheckpoint_Cross7[I], #ENDIF 
																MPGlobalsScoreHud.ElementHud_CHECKPOINT.GenericCheckpoint_FleckColour[I])
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_METER)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_METER)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_METER) = TRUE ")NET_NL()
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
						
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[5][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[5][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
						
						IF MPGlobalsHud.FixedOrderTracker[5][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[5][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[5][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_METER I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								ACTUALLY_DRAW_GENERAL_METER(I, MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_Number[I], MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_MaxNumber[I], 
															MPGlobalsScoreHud.ElementHud_METER.sGenericMeter_Title[I],  MPGlobalsScoreHud.ElementHud_METER.GenericMeter_Colour[I], 
															MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_FlashTimer[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FreeRoamPos[I].x,
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FreeRoamPos[I].y, MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bIsPlayer[I], 
															MPGlobalsScoreHud.ElementHud_METER.iGenericMeter_TitleNumber[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bOnlyZeroIsEmpty[I], 
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_ColourFlashType[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_ColourFlash[I], 
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bBigMeter[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iDrawRedDangerZonePercent[I], 
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bIsLiteralString[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_PercentageLine[I],
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_FleckColour[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_TextColour[I],
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bDrawLineUnderName[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_LineUnderNameColour[I],
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_MakeBarUrgent[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iUrgentPercentage[I],
															MPGlobalsScoreHud.ElementHud_METER.PulseToColour[I], MPGlobalsScoreHud.ElementHud_METER.iPulseTime[I], MPGlobalsScoreHud.ElementHud_METER.bUseScoreTitle[I],
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fNumber[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fMaxNumber[I],
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bUseSecondaryBar[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_eSecondaryBarColour[I], 
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fSecondaryBarPercentage[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bTransparentSecBarIntersectingMainBar[I],
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_eSecBarPulseToColour[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iSecBarPulseTime[I],
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_fSecBarStartPercentage[I], MPGlobalsScoreHud.ElementHud_METER.GenericMeter_iGFXDrawOrder[I],
															MPGlobalsScoreHud.ElementHud_METER.GenericMeter_bCapMaxPercentage[I])
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
		//				IF AreAnyOfTheseElementsOnScreen[6] = TRUE
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SCORE)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_SCORE)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SCORE) = TRUE ")NET_NL()
					
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[6][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[6][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
					
						IF MPGlobalsHud.FixedOrderTracker[6][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[6][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[6][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_SINGLE_SCORE I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_GENERAL_SINGLE_SCORE(I, MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_Number[I], MPGlobalsScoreHud.ElementHud_SCORE.sGenericScore_Title[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Colour[I], 
																	MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_FlashTimer[I], MPGlobalsScoreHud.ElementHud_SCORE.iGenericScore_TitleNumber[I], 
																	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bIsPlayer[I], MPGlobalsScoreHud.ElementHud_SCORE.sGenericScore_NumberString[I], MPGlobalsScoreHud.ElementHud_SCORE.bGenericScore_isFloat[I], 
																	MPGlobalsScoreHud.ElementHud_SCORE.bGenericScore_FloatValue[I], 
																	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_ColourFlashType[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_ColourFlash[I],MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_TitleColour[I], 
																	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_DisplayWarning[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_MaxNumber[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_DrawInfinity[I], 
																	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Powerup[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_Style[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bIsLiteralTitle[I],
																	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_FleckColour[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_iAlpha[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bDisplayBlankScore[I],
																	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_pPlayerID[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bFlashTitle[I], MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_bDrawLineUnderName[I],
																	MPGlobalsScoreHud.ElementHud_SCORE.GenericScore_LineUnderNameColour[I])
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_ELIMINATION)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_ELIMINATION)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_ELIMINATION) = TRUE ")NET_NL()
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[7][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[7][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
					
						IF MPGlobalsHud.FixedOrderTracker[7][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[7][I] = HUDORDER_FREEROAM //FreeRoaming Bar
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[7][I]) 
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_ELIMINATION I = ")NET_PRINT_INT(I) NET_NL()

									
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_GENERAL_ELIMINATION(I, MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_MaxNumber[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.sGenericElimination_Title[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFirst[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourSecond[I],
															MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_VisibleBoxes[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive1[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive2[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive3[I], 
															MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive4[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive5[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive6[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive7[I], 
															MPGlobalsScoreHud.ElementHud_ELIMINATION.bGenericElimination_IsActive8[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_FlashTimer[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.iGenericElimination_TitleNumber[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_bIsPlayer[I], 
															MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FreeRoamPos[I].x,MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FreeRoamPos[I].y,MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box1Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box2Colour[I],
															MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box3Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box4Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box5Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box6Colour[I],
															MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box7Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box8Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box1Colour_InActive[I], 
															MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box2Colour_InActive[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box3Colour_InActive[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box4Colour_InActive[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box5Colour_InActive[I], 
															MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box6Colour_InActive[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box7Colour_InActive[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Box8Colour_InActive[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFlashType[I], 
															MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_ColourFlash[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_TitleColour[I] #IF USE_TU_CHANGES , GenericElimination_Cross0[I], GenericElimination_Cross1[I],GenericElimination_Cross2[I],GenericElimination_Cross3[I],GenericElimination_Cross4[I],GenericElimination_Cross5[I],
															GenericElimination_Cross6[I],GenericElimination_Cross7[I] #ENDIF, MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_bUseNonPlayerFont[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_FleckColour[I] #IF USE_TU_CHANGES , MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross0Colour[I],
															MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross1Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross2Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross3Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross4Colour[I],
															MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross5Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross6Colour[I], MPGlobalsScoreHud.ElementHud_ELIMINATION.GenericElimination_Cross7Colour[I] #ENDIF)
								RESET_SCRIPT_GFX_ALIGN()	
								
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_WINDMETER)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_WINDMETER)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_WINDMETER) = TRUE ")NET_NL()
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[8][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[8][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))
						ENDIF
						#ENDIF
					
						IF MPGlobalsHud.FixedOrderTracker[8][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[8][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[8][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_WINDMETER I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_GENERAL_WINDMETER(I, MPGlobalsScoreHud.ElementHud_WIND.sGenericMeter_Title[I], 
										MPGlobalsScoreHud.ElementHud_WIND.fGenericMeter_Heading[I],
										MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_WindSpeed[I],
										MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_RedComponent[I],
										MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_BlueComponent[I],
										MPGlobalsScoreHud.ElementHud_WIND.iGenericMeter_GreenComponent[I])
										
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_BIG_RACE_POSITION)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_BIG_RACE_POSITION)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_BIG_RACE_POSITION) = TRUE ")NET_NL()
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[9][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[9][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))
						ENDIF
						#ENDIF
						
						IF MPGlobalsHud.FixedOrderTracker[9][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[9][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[9][I])
								
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_BIG_RACE_POSITION I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_GENERAL_BIG_RACE_POSITION(I,
										MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_iRacePosition[I],
										MPGlobalsScoreHud.ElementHud_BIGRACEPOSITION.eGenericBigRacePos_eRacePositionHUDColour[I])
										
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SPRITE_METER)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_SPRITE_METER)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SPRITE_METER) = TRUE ")NET_NL()
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1 
						
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[10][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[5][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))

							
						ENDIF
						#ENDIF
						
						IF MPGlobalsHud.FixedOrderTracker[10][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[10][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[10][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_SPRITE_METER I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								ACTUALLY_DRAW_GENERAL_SPRITE_METER(I, MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_Number[I], MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_MaxNumber[I], 
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.sGenericMeter_Title[I],  MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_Colour[I], 
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_FlashTimer[I],
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bIsPlayer[I], 
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.iGenericMeter_TitleNumber[I], MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bOnlyZeroIsEmpty[I], 
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_ColourFlashType[I], 
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_ColourFlash[I], 
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_SpriteName[I],
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.sGenericMeter_DictionaryName, 
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_bIsLiteralString[I],
															MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_FleckColour[I], MPGlobalsScoreHud.ElementHud_SPRITEMETER.GenericMeter_TextColour[I])
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_FOUR_ICON_BAR)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_FOUR_ICON_BAR)
					
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_FOUR_ICON_BAR) = TRUE ")NET_NL()
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[11][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[11][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))
						ENDIF
						#ENDIF
						
						IF MPGlobalsHud.FixedOrderTracker[11][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[11][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[11][I])
								
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_FOUR_ICON_BAR I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_GENERAL_FOUR_ICON_BAR(I, MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_TitleColour[I], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerOne[I],
																	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerTwo[I], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerThree[I], 
																	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_pPlayerFour[I], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupOne[I],
																	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupTwo[I], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupThree[I],
																	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_aPowerupFour[I], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconOne[I],
																	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconTwo[I], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconThree[I],
																	MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_bFlashIconFour[I], MPGlobalsScoreHud.ElementHud_FOURICONBAR.eGenericFourIconBar_iFlashTime[I])
															
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_FIVE_ICON_SCORE_BAR)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_FIVE_ICON_SCORE_BAR)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_FIVE_ICON_SCORE_BAR) = TRUE ")NET_NL()
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[12][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[12][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))
						ENDIF
						#ENDIF
						
						IF MPGlobalsHud.FixedOrderTracker[12][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[12][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[12][I])
								
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_FIVE_ICON_SCORE_BAR I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_GENERAL_FIVE_ICON_SCORE_BAR(I, MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_Number[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_FloatValue[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_NumberString[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_isFloat[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_MaXNumber[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_DrawInfinity[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_TitleColour[I], 
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerOne[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerTwo[I], 
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerThree[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerFour[I], 
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerFive[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupOne[I], 
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupTwo[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupThree[I], 
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupFour[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_aPowerupFive[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pPlayerToHighlight[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bEnablePlayerHighlight[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupOneColour[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupTwoColour[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupThreeColour[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupFourColour[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_ePowerupFiveColour[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iInstanceToHighlight[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bPulseHighlight[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iPulseTime[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_pAvatarToFlash[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_bFlashAvatar[I],
																		MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iAvatarFlashTime[I], MPGlobalsScoreHud.ElementHud_FIVEICONSCOREBAR.eGenericFiveIconScoreBar_iAvatarSlotToFlash[I])
															
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SIX_ICON_BAR)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_SIX_ICON_BAR)
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL() NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_SIX_ICON_BAR) = TRUE") NET_NL()
					ENDIF
					#ENDIF
					
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT - 1
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[13][I] = ") NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[13][I]))
							NET_NL()NET_PRINT("				- J + ENUM_TO_INT(HUDORDER_BOTTOM) = ") NET_PRINT_INT(J + ENUM_TO_INT(HUDORDER_BOTTOM))
						ENDIF
						#ENDIF
						
						IF MPGlobalsHud.FixedOrderTracker[13][I] = INT_TO_ENUM(HUDORDER, J + ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[13][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[13][I])
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_GENERAL_SIX_ICON_BAR I = ") NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_GENERAL_SIX_ICON_BAR(I, MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_TitleColour[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerOne[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerTwo[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerThree[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerFour[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerFive[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_pPlayerSix[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupOne[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupTwo[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupThree[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupFour[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupFive[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_aPowerupSix[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconOne[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconTwo[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconThree[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconFour[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconFive[I],
																	MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_bFlashIconSix[I], MPGlobalsScoreHud.ElementHud_SIXICONBAR.eGenericSixIconBar_iFlashTime[I])
								
								RESET_SCRIPT_GFX_ALIGN()
								
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
				IF IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_TEXT)
				AND NOT IS_HUD_ELEMENT_A_PRIORITY(PROGRESSHUD_DOUBLE_TEXT)
				
					#IF IS_DEBUG_BUILD
					IF g_DisplayTimersDisplaying
						NET_NL()NET_PRINT("IS_PROGRESSHUD_ACTIVATION_ON(PROGRESSHUD_DOUBLE_TEXT) = TRUE ")NET_NL()
					ENDIF
					#ENDIF
				
					FOR I = 0 TO MAX_NUMBER_HUD_ELEMENT-1
					
						#IF IS_DEBUG_BUILD
						IF g_DisplayTimersDisplaying
							NET_NL()NET_PRINT("				- MPGlobalsHud.FixedOrderTracker[14][I] = ")NET_PRINT_INT(ENUM_TO_INT(MPGlobalsHud.FixedOrderTracker[14][I]))
							NET_NL()NET_PRINT("				- J+ENUM_TO_INT(HUDORDER_BOTTOM) = ")NET_PRINT_INT(J+ENUM_TO_INT(HUDORDER_BOTTOM))
						ENDIF
						#ENDIF
						
						IF MPGlobalsHud.FixedOrderTracker[14][I] = INT_TO_ENUM(HUDORDER, J+ENUM_TO_INT(HUDORDER_BOTTOM))
						OR MPGlobalsHud.OrderTracker[14][I] = HUDORDER_FREEROAM
							IF CAN_INGAME_HUD_DISPLAY_WITH_PHONE(MPGlobalsHud.OrderTracker[14][I])
								
								DISPLAY_OVER_PAUSE_MENU(TRUE)
								SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, TopOrBottom)
								SET_SCRIPT_GFX_ALIGN_PARAMS(AlignX, AlignY, SIZEX, SIZEY)
								
								#IF IS_DEBUG_BUILD
								IF g_DisplayTimersDisplaying
									NET_NL()NET_PRINT("ACTUALLY_DRAW_DOUBLE_TEXT_BAR I = ")NET_PRINT_INT(I) NET_NL()
								ENDIF
								#ENDIF
								
								ACTUALLY_DRAW_DOUBLE_TEXT_BAR(I, MPGlobalsScoreHud.ElementHud_DOUBLETEXT.sGenericDoubleText_TitleLeft[I], MPGlobalsScoreHud.ElementHud_DOUBLETEXT.sGenericDoubleText_TitleRight[I], 
															  MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bTitleLeftLiteral[I], MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bTitleRightLiteral[I],
										 					  MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_TitleCOLOUR[I], MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_bCustomFont[I],
															  MPGlobalsScoreHud.ElementHud_DOUBLETEXT.GenericDoubleText_eCustomFont[I])
															
								RESET_SCRIPT_GFX_ALIGN()	
								DISPLAY_OVER_PAUSE_MENU(FALSE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("[BCTIMERS] RUN_TIMERHUD - BOTTOM ")NET_NL()
			ENDIF
		#ENDIF
		
		
		UPDATE_ALL_PROGRESSHUD_LAST_FRAME()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("[BCTIMERS] UPDATE_ALL_PROGRESSHUD_LAST_FRAME")
		#ENDIF
		#ENDIF	
	ELSE	
		#IF IS_DEBUG_BUILD
			IF g_DisplayTimersDisplaying
				NET_NL()NET_PRINT("[BCTIMERS] RUN_TIMERHUD - HIDE but keep active ")NET_NL()
			ENDIF
		#ENDIF
	ENDIF
ENDPROC




SCRIPT


	
	// This script needs to cleanup only when the game runs the magdemo
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_MAGDEMO))
		PRINTSTRING("...event_controller.sc has been forced to cleanup (MAGDEMO)")
		PRINTNL()
		
		TERMINATE_THIS_THREAD()
	ENDIF
	
	SCRIPT_TIMER TimersTimer
	

	
//	BREAK_ON_NATIVE_COMMAND("SWITCH_TO_MULTI_SECONDPART", FALSE)	
		
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		

	NET_NL()NET_PRINT("[BCTIMERS] ------ START TIMERSHUD -------- ")




	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	CREATE_SCRIPT_PROFILER_WIDGET()
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		CREATE_TIMERHUD_WIDGETS()
	#ENDIF


	// The main mission loop 
	WHILE TRUE 	
	
		IF HAS_NET_TIMER_EXPIRED(TimersTimer, 5)
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
			#ENDIF
			#ENDIF
			
			//Need this running all the time for the singleplayer hud displays. Added by BC 22/08/11
			RUN_TIMERHUD()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_HUD_DISPLAYS")
			#ENDIF
			#ENDIF
			
			

		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
			#ENDIF
		ENDIF
		
		
		BOOL Terminate
		IF MPGlobalsScoreHud.isSomethingDisplaying = FALSE
			Terminate	= TRUE
		ENDIF
		
		DISABLE_YACHT_INFO_THIS_FRAME() 
		
		RESET_PROGRESSHUD_BITSET()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RESET_PROGRESSHUD_BITSET")
		#ENDIF
		#ENDIF
		
		RESET_ALL_HUD_ELEMENTS_PRIVATE()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RESET_ALL_HUD_ELEMENTS")
		#ENDIF
		#ENDIF
		
			
			
				
			
		
			
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("loop 7")
		#ENDIF
		#ENDIF
			
			
		IF Terminate
		
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("timerbar_lines")		
			
			RESET_MP_GLOBALS_SCORE_HUD() 
			
			NET_NL()NET_PRINT("[BCTIMERS] ---  KILL TIMERSHUD ---- ")
			TERMINATE_THIS_THREAD()
		
		ENDIF
	
		
	
		WAIT(0)
		
	ENDWHILE
	
	
ENDSCRIPT



