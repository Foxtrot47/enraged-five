/***********************************
*	Name: PauseMenu_MP_Map.sc
*	Author: James Adwick
*	Date: 05/11/2012
*	Purpose: Script to run while players 
*			are on map tab to display
*			mission details
***********************************/

USING "globals.sch"
USING "PauseMenu_MP_Public.sch"
USING "PauseMenu_SP_Public.sch"
USING "Flow_Mission_Data_Public.sch"
USING "shop_public.sch"
USING "fmmc_cloud_loader.sch"
USING "mp_scaleform_functions_xml.sch"
USING "transition_common.sch"
USING "fm_quickmatch_header.sch"
USING "net_time_trials.sch"
USING "net_rc_time_trials.sch"
#IF FEATURE_GEN9_EXCLUSIVE
USING "net_hsw_time_trials.sch"
#ENDIF 
BLIP_INDEX mapBlip
BLIP_INDEX mapBlipLastFrame
VECTOR			vLocation			//The location of the blip
TEXT_LABEL_63 	tl63Creator			//The creator
TEXT_LABEL_63 	tl31MissionName		//The mission name
TEXT_LABEL_63 	tl63Description 	//The mission description
INT 			iMinPlayers 		//The min number of players
INT 			iMaxPlayers 		//The max number of players
INT 			iMissionType 		//The mission type.
INT 			iMissionSubType 	//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
INT 			iMissionBitSet		//The bitset for the mission (let us know subtype of Capture mode)
INT 			iMissionBitSetTwo	//The bitset for the mission (let us know subtype of Capture mode)
INT				iRank				//The rank the player must be at
INT 			iRating				//The mission rating
BOOL			bVerified			//The mission is R* verified
BOOL			bHasPhoto			//Has the missiong got a photo
BOOL 			bRockstarCreated
TEXT_LABEL_23 	tlFileName
INT				iDecHash
INT 			iMaxTeams
BOOL			bAllowStartJob		// KEITH 20/3/14: TRUE if the 'start mission' option should be allowed for this blip, FALSE if not (added to disable this for remote player's UGC missions)
INT 			iPhotoVersion
INT 			iPhotoPath
DOWNLOAD_PHOTO_DATA DLPhotoData
STRUCT_DL_PHOTO_VARS_LITE sDownloadPhotoData

BOOL 			bJoinJobWarning
BOOL 			bCantJoinJobWarning
BOOL 			bCleanupJobCall
BOOL			bPushbikeOnly
INT 			iRootContentIDHash
INT 			iAdversaryModeType
SCRIPT_TIMER	iGarageCheck
TEXT_LABEL_31 	tlJoinMissionName
INT 			iJoinMissionType
INT 			iJoinMaxPlayers
INT 			iJoinAvailable

BOOL bGetDescription
CACHED_MISSION_DESCRIPTION_LOAD_VARS sCMDLvars
	
//SP_MISSIONS eMission				//Single player mission ID.

//SHOP_NAME_ENUM eShop				//shop id

//PURPOSE: Controls loading and displaying the mission photo
PROC CONTROL_MISSION_PHOTO_DISPLAY()
	IF DLPhotoData.bLoadMissionPhoto = TRUE
		//Initialisation
		IF DLPhotoData.bInitLoadMissionPhoto = FALSE
			RESET_STRUCT_DL_PHOTO_VARS_LITE(sDownloadPhotoData)		//CLEAN_UP_PHOTO_FOR_FMMC(sDownloadPhotoData)
			DLPhotoData.bInitLoadMissionPhoto = TRUE
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] CONTROL_MISSION_PHOTO_DISPLAY - INIT")
		ENDIF
			
		IF sDownloadPhotoData.bSucess = FALSE
			//Load Photo
			//IF ARE_STRINGS_EQUAL(DLPhotoData.sFileName, "City")
			//OR ARE_STRINGS_EQUAL(DLPhotoData.sFileName, "Country_side")
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] CONTROL_MISSION_PHOTO_DISPLAY - START LOADING")
			IF DOWNLOAD_PHOTO_FOR_FMMC_LITE(sDownloadPhotoData, tlFileName, 0, iPhotoVersion, iPhotoPath)
			OR iMissionType = FMMC_TYPE_TIME_TRIAL
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PAUSE MAP - CONTROL_MISSION_PHOTO_DISPLAY - A - tlFileName = ", tlFileName)
			//OR DOWNLOAD_PHOTO_FOR_FMMC(sDownloadPhotoData, DLPhotoData.sCreator, DLPhotoData.sFileName)	//"RSN_BobbyW_1", "513f98028fc43216549b3416")	//DLPhotoData.sCreator, DLPhotoData.sFileName)	//GET_MISSION_CREATOR_PM_STAT(),  GET_MISSION_FILENAME_PM_STAT())
				//Failsafe Photo
				TEXT_LABEL_31 strTxd
	            TEXT_LABEL_31 strName
	            INT iLoadInt
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PAUSE MAP - CONTROL_MISSION_PHOTO_DISPLAY - A - sFileName = ", DLPhotoData.sFileName)
				DLPhotoData.sFileName = TEXTURE_DOWNLOAD_GET_NAME(sDownloadPhotoData.iTextureDownloadHandle)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PAUSE MAP - CONTROL_MISSION_PHOTO_DISPLAY - B - sFileName = ", DLPhotoData.sFileName)
				
	            IF NOT IS_STRING_NULL_OR_EMPTY(DLPhotoData.sFileName)
				AND sDownloadPhotoData.bSucess = TRUE
	                strTxd = DLPhotoData.sFileName
	                strName = DLPhotoData.sFileName
					iLoadInt = 0
	            ELSE
	                iLoadInt = 1
					IF iMissionType = FMMC_TYPE_TIME_TRIAL
						strTxd = "PM_TT_"
						strTxd += GET_CURRENT_TIME_TRIAL_OLD_MAPPING(GET_CURRENT_ACTIVE_FM_TIME_TRIAL())
						strName = "TTSHOT"	
						strName += GET_CURRENT_TIME_TRIAL_OLD_MAPPING(GET_CURRENT_ACTIVE_FM_TIME_TRIAL())
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PAUSE MAP - CONTROL_MISSION_PHOTO_DISPLAY - strTxd = ",strTxd, " - strName = ",strName )
					ELIF iMissionType = FMMC_TYPE_RC_TIME_TRIAL
						strTxd = "PM_RCTT_"
						strTxd += GET_CURRENT_RC_TIME_TRIAL_OLD_MAPPING(GET_CURRENT_ACTIVE_FM_RC_TIME_TRIAL())
						strName = "RCTTSHOT"	
						strName += GET_CURRENT_RC_TIME_TRIAL_OLD_MAPPING(GET_CURRENT_ACTIVE_FM_RC_TIME_TRIAL())
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PAUSE MAP - CONTROL_MISSION_PHOTO_DISPLAY - strTxd = ",strTxd, " - strName = ",strName )
					#IF FEATURE_GEN9_EXCLUSIVE
					ELIF iMissionType = FMMC_TYPE_HSW_TIME_TRIAL
						strTxd = "PM_HSWTT_"
						strTxd += ENUM_TO_INT(GET_CURRENT_HSW_TIME_TRIAL())
						strName = "HSWTTSHOT"	
						strName += ENUM_TO_INT(GET_CURRENT_HSW_TIME_TRIAL())
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PAUSE MAP - CONTROL_MISSION_PHOTO_DISPLAY - strTxd = ",strTxd, " - strName = ",strName )
					#ENDIF
					ELSE
						IF GET_HASH_OF_MAP_AREA_AT_COORDS(vLocation) = MAP_AREA_CITY
							strTxd = "MPCarHUD"
							strName = "City"
						ELSE
							strTxd = "MPCarHUD"
							strName = "Country_side"
						ENDIF
					ENDIF
	            ENDIF
				
				INT iJobType = 0
				/*IF bVerified	//DON'T DISPLAY VERIFIED OR ROCKSTAR  - 1614275 and 1614277
					iJobType = 1
				ELIF bRockstarCreated
					iJobType = 2
				ENDIF*/
				
				//IF iJobType = 0
					TAKE_CONTROL_OF_FRONTEND()
//						//PM_SET_COLUMN_TITLE(DLPhotoData.iColumn, GET_MISSION_DETAIL_STRING_FOR_PAUSE_MENU(DLPhotoData.iMissionType), DLPhotoData.tl31MissionName, DLPhotoData.bVerified, strTxd, strName)	//"513f98028fc43216549b3416", "513f98028fc43216549b3416")//DLPhotoData.sFileName, DLPhotoData.sFileName)
//						//PM_SET_COLUMN_TITLE(DLPhotoData.iColumn, GET_MISSION_DETAIL_STRING_FOR_PAUSE_MENU(DLPhotoData.iMissionType), DLPhotoData.tl31MissionName, DLPhotoData.bVerified)
						SET_FRONTEND_DETAILS_TITLE(DLPhotoData.iColumn, " ", DLPhotoData.tl31MissionName, iJobType, strTxd, strName, TRUE, iLoadInt, iMissionType, iMissionSubType, tlFileName, bPushbikeOnly, iRootContentIDHash, iAdversaryModeType)
					RELEASE_CONTROL_OF_FRONTEND()
				//ENDIF
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PAUSE MAP - CONTROL_MISSION_PHOTO_DISPLAY - SET_FRONTEND_DETAILS_TITLE called with ", strTxd, " and ", strName, " LoadInt = ", iLoadInt, " iJobType = ", iJobType)
				sDownloadPhotoData.bSucess = TRUE
			ENDIF
		ENDIF
		
	ELSE
		//Cleanup
		IF DLPhotoData.bInitLoadMissionPhoto = TRUE
			RESET_STRUCT_DL_PHOTO_VARS_LITE(sDownloadPhotoData)		//CLEAN_UP_PHOTO_FOR_FMMC(sDownloadPhotoData)
			DOWNLOAD_PHOTO_DATA emptyData
			DLPhotoData = emptyData
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] CONTROL_MISSION_PHOTO_DISPLAY - CLEAN UP")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Gets the cached Mission Description and once done displays the Mission Details box
PROC GET_CACHED_DESCRIPTION(BOOL bShowStartMission)
	IF bGetDescription = TRUE
		
		IF REQUEST_AND_LOAD_CACHED_DESCRIPTION(iDecHash, sCMDLvars)
			// Update right hand column and display - uses code-cached long description
			DISPLAY_MISSION_DETAILS(1, ENUM_TO_INT(MENU_UNIQUE_ID_MISSION_CREATOR_LISTITEM), vLocation, iRating, tl63Creator, tl31MissionName, UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(iDecHash, 500), 
			iRank, iMinPlayers, iMaxPlayers, iMissionType, iMissionSubType, iMissionBitSet, iMissionBitSetTwo, bVerified, tlFileName, DLPhotoData, bHasPhoto, bRockstarCreated, iMaxTeams,  DEFAULT,DEFAULT,DEFAULT, iRootContentIDHash, iAdversaryModeType)
			
			IF (bAllowStartJob AND bShowStartMission)
				SHOW_START_MISSION_INSTRUCTIONAL_BUTTON(TRUE)
			ENDIF
			SHOW_PM_COLUMN(1)
			bGetDescription = FALSE
			CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - REQUEST_AND_LOAD_CACHED_DESCRIPTION done")
		#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - waiting on REQUEST_AND_LOAD_CACHED_DESCRIPTION")
		#ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the player joining the currently selected mission
PROC PROCESS_JOIN_MISSION()
	IF bJoinJobWarning = FALSE
		//Join Job Check
		IF bCantJoinJobWarning = FALSE
			IF DOES_BLIP_EXIST(mapBlipLastFrame)
				IF IS_MISSION_CREATOR_BLIP(mapBlipLastFrame)
					TAKE_CONTROL_OF_FRONTEND()
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
						IF (bAllowStartJob)
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							iJoinAvailable = CAN_DO_QUICK_MATCH(iGarageCheck, iMissionType)
							CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_JOIN_MISSION - CAN_DO_QUICK_MATCH = ", iJoinAvailable)
							IF iJoinAvailable = 0
								
								CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_JOIN_MISSION - bJoinJobWarning = TRUE - Name = ", tl31MissionName)
								
								tlJoinMissionName 	= tlFileName
								iJoinMissionType	= iMissionType
								iJoinMaxPlayers		= iMaxPlayers
								
								IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SPECIAL_VEHICLE_RACE_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_SPECIAL_VEHICLE_RACE_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_BUNKER_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_BUNKER_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_TRANSFORM_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_TRANSFORM_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_TARGET_ASSAULT_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_TARGET_ASSAULT_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HOTRING_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_HOTRING_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_ARENA_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_RACE_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_RACE_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SURVIVAL_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_SURVIVAL_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_OPEN_WHEEL_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_OPEN_WHEEL_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_STREET_RACE_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_STREET_RACE_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_PURSUIT_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_PURSUIT_SERIES
								#IF FEATURE_GEN9_EXCLUSIVE
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HSW_RACE_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_HSW_RACE_SERIES
								#ENDIF
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_COMMUNITY_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_COMMUNITY_SERIES
								ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(iRootContentIDHash)
									iJoinMissionType = FMMC_TYPE_CAYO_PERICO_SERIES
								ELIF iJoinMissionType = FMMC_TYPE_MISSION
								AND CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ADVERSARY_SERIES_JOB(iRootContentIDHash)
								AND NOT g_sMPTunables.bDisableStuntSeriesBucket
									iJoinMissionType = CV2_GET_ADVERSARY_SERIES_TYPE_FROM_ROOT_CONTENT_ID(iRootContentIDHash)
								ELIF iJoinMissionType = FMMC_TYPE_MISSION
									IF IS_THIS_AN_ADVERSARY_MODE_MISSION(iRootContentIDHash, iAdversaryModeType)
										iJoinMissionType = FMMC_TYPE_MISSION_NEW_VS
									ELIF iMissionSubType = FMMC_MISSION_TYPE_CTF
										iJoinMissionType = FMMC_TYPE_MISSION_CTF
									ELIF iMissionSubType = FMMC_MISSION_TYPE_VERSUS
										iJoinMissionType = FMMC_TYPE_MISSION_VS
									ELIF iMissionSubType = FMMC_MISSION_TYPE_LTS
										iJoinMissionType = FMMC_TYPE_MISSION_LTS
									ELIF iMissionSubType = FMMC_MISSION_TYPE_COOP
										iJoinMissionType = FMMC_TYPE_MISSION_COOP
									ENDIF
								ELIF iJoinMissionType = FMMC_TYPE_RACE
								AND  CV2_IS_THIS_ROOT_CONTENT_ID_A_CORONA_FLOW_JOB(iRootContentIDHash)
								AND NOT g_sMPTunables.bDisableStuntSeriesBucket
									iJoinMissionType = FMMC_TYPE_RACE_STUNT_FOR_QM
								ENDIF
								bJoinJobWarning = TRUE
							ELSE
								CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_JOIN_MISSION - bCantJoinJobWarning = TRUE - Name = ", tl31MissionName)
								bCantJoinJobWarning = TRUE
							ENDIF
						ENDIF
					ENDIF
					RELEASE_CONTROL_OF_FRONTEND()
				ENDIF
			ENDIF
			
		//Can't Join Job Warning Message
		ELSE
			TAKE_CONTROL_OF_FRONTEND()
			TEXT_LABEL_15 sBody
			sBody = "PM_INF_QMF"
			sBody += iJoinAvailable
			SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", sBody, FE_WARNING_CANCEL)
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_JOIN_MISSION - bCantJoinJobWarning = FALSE - Name = ", tl31MissionName)
				bCantJoinJobWarning = FALSE
			ENDIF
			RELEASE_CONTROL_OF_FRONTEND()
		ENDIF
	
	//Confirm Join Job
	ELSE
		TAKE_CONTROL_OF_FRONTEND()
		SET_WARNING_MESSAGE_WITH_HEADER("PM_QUIT_K1", "PM_QUIT_WARN7", FE_WARNING_OKCANCEL)
		
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR bCleanupJobCall
			IF NOT bCleanupJobCall 
				SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() 
				bCleanupJobCall = TRUE 
			ENDIF 
			IF !AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_JOIN_MISSION - ACCEPT - Name = ", tl31MissionName)
				//PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				SET_FRONTEND_ACTIVE(FALSE)
				IF DOES_BLIP_EXIST(mapBlipLastFrame)	
					BLIP_SPRITE blipSprite = GET_BLIP_SPRITE(mapBlipLastFrame)
					IF blipSprite = RADAR_TRACE_STUNT_PREMIUM
						SET_TRANSITION_SESSIONS_LAUNCHING_PROFESSIONAL_RACE()
						SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
					ELIF blipSprite = RADAR_TRACE_STUNT
						g_iCV2TelemetryLaunchCorona = 100
						SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
					ELIF blipSprite = RADAR_TRACE_STEERINGWHEEL
					OR blipSprite = RADAR_TRACE_TROPHY
					OR CV2_IS_BLIP_ADVERSARY_SERIES_BLIP(blipSprite)
					OR blipSprite = RADAR_TRACE_ADVERSARY_BUNKER
					OR blipSprite = RADAR_TRACE_RACE_TF
					OR blipSprite = RADAR_TRACE_ACSR_RACE_TARGET
					OR blipSprite = RADAR_TRACE_ACSR_RACE_HOTRING
					OR blipSprite = RADAR_TRACE_ARENA_SERIES
					OR blipSprite = RADAR_TRACE_STREET_RACE_SERIES
					OR blipSprite = RADAR_TRACE_PURSUIT_SERIES
					OR blipSprite = RADAR_TRACE_FEATURED_SERIES
					OR blipSprite = RADAR_TRACE_RACE_OPEN_WHEEL
					OR ((blipSprite = RADAR_TRACE_RACE_LAND) AND (GET_BLIP_COLOUR(mapBlipLastFrame) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_PINKLIGHT)))
					OR ((blipSprite = RADAR_TRACE_HORDE) AND (GET_BLIP_COLOUR(mapBlipLastFrame) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_ORANGE)))
					#IF FEATURE_GEN9_EXCLUSIVE
					OR blipSprite = RADAR_TRACE_HSW_RACE_SERIES
					#ENDIF
					OR blipSprite = RADAR_TRACE_COMMUNITY_SERIES
					OR blipSprite = RADAR_TRACE_CAYO_SERIES
						SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
					ENDIF
				ENDIF
				SET_PLAYER_LEAVING_CORONA_VECTOR(GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()))
				
				//Set that we picked a specific job
				IF NOT IS_STRING_NULL_OR_EMPTY(tlJoinMissionName)
				AND NOT TRANSITION_SESSIONS_SKIP_JOB_WARNING()
					SET_TRANSITION_SESSIONS_PICKED_SPECIFIC_JOB()
				ENDIF
				
				SET_MY_TRANSITION_SESSION_CONTENT_ID(tlJoinMissionName)
				SET_TRANSITION_SESSIONS_STARTING_QUICK_MATCH()	
				
				SET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE(iJoinMissionType)
				
				SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_HOST_PAUSE_MENU)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
				SET_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA()
				CLEAR_PAUSE_MENU_IS_USING_UGC()
				SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(iJoinMaxPlayers)
				SET_TRANSITION_SESSIONS_NEED_TO_WARP_TO_START_SKYCAM()
				
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
				bJoinJobWarning = FALSE
			ENDIF
			
		ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_JOIN_MISSION - CANCEL - Name = ", tl31MissionName)
			//PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			bJoinJobWarning = FALSE
			bCleanupJobCall = FALSE
		ENDIF
		
		RELEASE_CONTROL_OF_FRONTEND()
	ENDIF
ENDPROC

PROC PROCESS_DLC_CONTENT_CALL()

	IF g_sMPTunables.bENABLE_DLC_CONTENT_CONTROLLER = FALSE
		EXIT
	ENDIF

	IF g_sDLCContentContollerStruct.iTriggerBitset = 0
		IF DOES_BLIP_EXIST(mapBlipLastFrame)
			IF IS_MISSION_CREATOR_BLIP(mapBlipLastFrame)
			AND IS_DLC_CONTENT_BLIP(mapBlipLastFrame)
				DLC_CONTENT eContent = GET_DLC_CONTENT_FROM_BLIP(mapBlipLastFrame)
				IF eContent != eDLCCONTENT_INVALID
				AND DOES_DLC_CONTENT_HAVE_CORRESPONDANCE(eContent)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
						TAKE_CONTROL_OF_FRONTEND()
						PRINTLN("[DLC_CONTENT] - PROCESS_DLC_CONTENT_CALL - Selected to trigger correspondance from the pause menu, force close pause menu and trigger.")
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						SET_BIT(g_sDLCContentContollerStruct.iTriggerBitset, ENUM_TO_INT(eContent))
						SET_FRONTEND_ACTIVE(FALSE)
						RELEASE_CONTROL_OF_FRONTEND()
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL GET_TIME_TRIAL_INFO_FROM_VECTOR()
	VECTOR vEnd, vDir
	FLOAT fStart, fEnd
	IF GET_TIME_TRIAL_LOCATION_INFO(GET_CURRENT_ACTIVE_FM_TIME_TRIAL(), vLocation, fStart, vEnd, fEnd, vDir)
		IF ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(mapBlip), vLocation)
			iRating = -1
			tl63Creator = "NULL"
			tl31MissionName = GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_TIME_TRIAL_ROUTE_STRING(GET_CURRENT_ACTIVE_FM_TIME_TRIAL()))
			CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - GET_TIME_TRIAL_INFO_FROM_VECTOR - tl31MissionName = ", tl31MissionName)
			tl63Description = GET_TIME_TRIAL_DESCRIPTION_STRING(GET_CURRENT_ACTIVE_FM_TIME_TRIAL())
			CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - GET_TIME_TRIAL_DESCRIPTION_STRING - tl63Description = ", tl63Description)
			iRank = 3
			iMinPlayers = 1 
			iMaxPlayers = 1 
			iMissionType = FMMC_TYPE_TIME_TRIAL 
			iMissionSubType = 0 
			iMissionBitSet = 0
			iMissionBitSetTwo = 0
			bVerified = FALSE 
			iRootContentIDHash = 0
			iAdversaryModeType = 0
			tlFileName = "NULL"
			bHasPhoto = FALSE
			bRockstarCreated = TRUE
			iMaxTeams = 1 
			bPushbikeOnly = FALSE
			DLPhotoData.sFileName = "NULL"
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_RC_TIME_TRIAL_INFO_FROM_VECTOR()

		
	//IF GET_TIME_TRIAL_LOCATION_INFO(GET_CURRENT_ACTIVE_FM_TIME_TRIAL(), vLocation, fStart, vEnd, fEnd, vDir)
		IF ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(mapBlip), AMRCTT_GET_CORONA_LOCATION(GET_CURRENT_ACTIVE_FM_RC_TIME_TRIAL()))
			iRating = -1
			tl63Creator = "NULL"
			tl31MissionName = GET_FILENAME_FOR_AUDIO_CONVERSATION(AMRCTT_GET_VARIATION_NAME_STRING(GET_CURRENT_ACTIVE_FM_RC_TIME_TRIAL()))
			CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - GET_RC_TIME_TRIAL_INFO_FROM_VECTOR - tl31MissionName = ", tl31MissionName)
			tl63Description = AMRCTT_GET_VARIATION_Description_STRING(GET_CURRENT_ACTIVE_FM_RC_TIME_TRIAL())
			CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - GET_RC_TIME_TRIAL_INFO_FROM_VECTOR - tl63Description = ", tl63Description)
			iRank = 3
			iMinPlayers = 1 
			iMaxPlayers = 1 
			iMissionType = FMMC_TYPE_RC_TIME_TRIAL 
			iMissionSubType = 0 
			iMissionBitSet = 0
			iMissionBitSetTwo = 0
			bVerified = FALSE 
			iRootContentIDHash = 0
			iAdversaryModeType = 0
			tlFileName = "NULL"
			bHasPhoto = FALSE
			bRockstarCreated = TRUE
			iMaxTeams = 1 
			bPushbikeOnly = FALSE
			DLPhotoData.sFileName = "NULL"
			RETURN TRUE
		ENDIF
	//ENDIF
	RETURN FALSE
ENDFUNC

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL GET_HSW_TIME_TRIAL_INFO_FROM_VECTOR()
	//IF GET_TIME_TRIAL_LOCATION_INFO(GET_CURRENT_ACTIVE_FM_TIME_TRIAL(), vLocation, fStart, vEnd, fEnd, vDir)
		IF ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(mapBlip), GET_HSW_TIME_TRIAL_START_LOCATION(GET_CURRENT_HSW_TIME_TRIAL()))
			iRating = -1
			tl63Creator = "NULL"
			tl31MissionName = GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_HSW_TIME_TRIAL_ROUTE_NAME_STRING(GET_CURRENT_HSW_TIME_TRIAL()))
			CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - GET_HSW_TIME_TRIAL_INFO_FROM_VECTOR - tl31MissionName = ", tl31MissionName)
			tl63Description = GET_HSW_TIME_TRIAL_ROUTE_DESCRIPTION_STRING(GET_CURRENT_HSW_TIME_TRIAL())
			CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - GET_HSW_TIME_TRIAL_INFO_FROM_VECTOR - tl63Description = ", tl63Description)
			iRank = 3
			iMinPlayers = 1 
			iMaxPlayers = 1 
			iMissionType = FMMC_TYPE_HSW_TIME_TRIAL
			iMissionSubType = 0 
			iMissionBitSet = 0
			iMissionBitSetTwo = 0
			bVerified = FALSE 
			iRootContentIDHash = 0
			iAdversaryModeType = 0
			tlFileName = "NULL"
			bHasPhoto = FALSE
			bRockstarCreated = TRUE
			iMaxTeams = 1 
			bPushbikeOnly = FALSE
			DLPhotoData.sFileName = "NULL"
			RETURN TRUE
		ENDIF
	//ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF 


PROC SET_GLOBAL_FOR_PREMIUM_RACE_BLIP(BLIP_INDEX tempBlip)
	//CPRINTLN(DEBUG_AMBIENT, "SET_GLOBAL_FOR_PREMIUM_RACE_BLIP")
	
	IF IS_HOVERING_OVER_MISSION_CREATOR_BLIP()
		// 2919150 - set Premium Blip global if currently highlighted.
		IF DOES_BLIP_EXIST(tempBlip)
			IF GET_BLIP_SPRITE(tempBlip) = RADAR_TRACE_STUNT_PREMIUM
				g_bV2CoronaMapBlipIsPremiumRace = TRUE
				//SCRIPT_ASSERT("g_bV2CoronaMapBlipIsPremiumRace = TRUE")
			ENDIF
		ENDIF	
	ELSE
		g_bV2CoronaMapBlipIsPremiumRace = FALSE
	ENDIF
	
	//CPRINTLN(DEBUG_AMBIENT, "SET_GLOBAL_FOR_PREMIUM_RACE_BLIP - g_bV2CoronaMapBlipIsPremiumRace = ", g_bV2CoronaMapBlipIsPremiumRace)
ENDPROC

// ************************************************************
// ******************** MAIN SCRIPT LOOP **********************
// ************************************************************

SCRIPT( PAUSE_MENU_LAUNCH_DATA args ) 

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Map pausemenu script launched with args: ", args.operation, ", Menu: ", args.MenuScreenId, ", Prev:", args.PreviousId, ", Unique: ", args.UniqueIdentifier)
	#ENDIF
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	

	// KEITH 3/5/13: Added to retrieve code-cached long mission descriptions if required
//	STRING			cachedDescription
//	UGC_DESCRIPTION	cachedDescAsTLs
//	INT				cachedDescTypeID	= 0
	INT				iCreatorID			= ILLEGAL_CREATOR_ID
	INT				iCachedDescID		= ILLEGAL_ARRAY_POSITION
	
	g_bHasPauseMapBeenAccessed = TRUE
	BOOL bShowStartMission 
	
	SWITCH args.operation
		
		CASE kUpdate
			WHILE(TRUE)

				WAIT(0)	// One and only wait in MP script.
				
				// Is code ready for script to take control
				IF IS_FRONTEND_READY_FOR_CONTROL()
					
					bShowStartMission = TRUE
					
					REQUEST_MAP_INFOCARD_TXDS()
					
					mapBlip = GET_NEW_SELECTED_MISSION_CREATOR_BLIP()
					
					IF IS_HOVERING_OVER_MISSION_CREATOR_BLIP()
						IF DOES_BLIP_EXIST(mapBlip)
							IF mapBlipLastFrame != mapBlip
								IF DOES_BLIP_EXIST(mapBlipLastFrame)
									IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
									ENDIF END_SCALEFORM_MOVIE_METHOD()	
								ENDIF
								mapBlipLastFrame = NULL
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(mapBlipLastFrame)
							IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
							ENDIF END_SCALEFORM_MOVIE_METHOD()	
						ENDIF
						mapBlipLastFrame = NULL
						bGetDescription = FALSE
					ENDIF
					
					SET_GLOBAL_FOR_PREMIUM_RACE_BLIP(mapBlip)
					
					IF IS_ARENA_CREATOR()
						PROCESS_ARENA_CREATOR_MAP()
					ENDIF
					
					//**TWH - CMcM - #1396679 - Checked that the blip exists before checking against or storing mapBlipLastFrame. Checks were getting skipped the other way.
					// Is the blip index valid
					IF DOES_BLIP_EXIST(mapBlip)
					
						//Is this a new blip that we haven't processed on a previous frame?
						IF mapBlip != mapBlipLastFrame

							// Is this a blip we specified as mission creator
							IF IS_MISSION_CREATOR_BLIP(mapBlip)

								//Are we in MP or SP?
								IF g_bInMultiplayer
								
									// MP. Are mission details available
									IF GET_MISSION_INFO_FROM_VECTOR(mapBlip, vLocation, tl63Creator, tl31MissionName, tl63Description, iMinPlayers, iMaxPlayers, iMissionType, iMissionSubType, iMissionBitSet, iMissionBitSetTwo,
									iRank, iRating, bVerified, iCreatorID, iCachedDescID, bHasPhoto, bRockstarCreated, tlFileName, iDecHash, iMaxTeams, bAllowStartJob,bPushbikeOnly, iPhotoVersion, iPhotoPath, iRootContentIDHash, iAdversaryModeType)
										
										CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Valid MP blip selected by player, display column")
											
										IF g_sMPTunables.bENABLE_SERIES_INFOCARDS
										AND GET_SERIES_FROM_BLIP(mapBlip) != ciCV2_SERIES_INVALID
										#IF FEATURE_GEN9_EXCLUSIVE
										AND NOT IS_PLAYER_ON_MP_INTRO()
										#ENDIF
										
											INT iSeries
											iSeries = GET_SERIES_FROM_BLIP(mapBlip)
											
											IF REQUEST_AND_LOAD_SERIES_BLIP_INFORMATION(iSeries)
											
												DISPLAY_SERIES_BLIP_INFORMATION(iSeries)
												
											ENDIF
											
										ELSE
											IF REQUEST_AND_LOAD_CACHED_DESCRIPTION(iDecHash, sCMDLvars)
												// Update right hand column and display - uses code-cached long description
												DISPLAY_MISSION_DETAILS(1, ENUM_TO_INT(MENU_UNIQUE_ID_MISSION_CREATOR_LISTITEM), vLocation, iRating, tl63Creator, tl31MissionName, UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(iDecHash, 500), 
												iRank, iMinPlayers, iMaxPlayers, iMissionType, iMissionSubType, iMissionBitSet, iMissionBitSetTwo, bVerified, tlFileName, DLPhotoData, bHasPhoto, bRockstarCreated, iMaxTeams, FALSE, bPushbikeOnly, DEFAULT, iRootContentIDHash, iAdversaryModeType)
												
												IF (bAllowStartJob)
													SHOW_START_MISSION_INSTRUCTIONAL_BUTTON(TRUE)
												ENDIF
												SHOW_PM_COLUMN(1)
												CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - REQUEST_AND_LOAD_CACHED_DESCRIPTION done")
											ELSE
												bGetDescription = TRUE
												CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - bGetDescription = TRUE")
												CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - waiting on REQUEST_AND_LOAD_CACHED_DESCRIPTION")
											ENDIF
										ENDIF
									ELSE
									
										IF GET_TIME_TRIAL_INFO_FROM_VECTOR()
										OR  GET_RC_TIME_TRIAL_INFO_FROM_VECTOR()
										#IF FEATURE_GEN9_EXCLUSIVE
										OR GET_HSW_TIME_TRIAL_INFO_FROM_VECTOR()
										#ENDIF 
											CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - Found a Time Trial blip.")
											
											DISPLAY_MISSION_DETAILS(1, ENUM_TO_INT(MENU_UNIQUE_ID_MISSION_CREATOR_LISTITEM), vLocation, iRating, tl63Creator, tl31MissionName, tl63Description, 
											iRank, iMinPlayers, iMaxPlayers, iMissionType, iMissionSubType, iMissionBitSet, iMissionBitSetTwo, bVerified, tlFileName, DLPhotoData, bHasPhoto, bRockstarCreated, iMaxTeams, FALSE, bPushbikeOnly, FALSE, iRootContentIDHash, iAdversaryModeType)
											
											
										ENDIF
										
										IF g_sMPTunables.bENABLE_DLC_CONTENT_CONTROLLER
										AND IS_DLC_CONTENT_BLIP(mapBlip)
										#IF FEATURE_GEN9_EXCLUSIVE
										AND NOT IS_PLAYER_ON_MP_INTRO()
										#ENDIF
											DLC_CONTENT eContent
											eContent = GET_DLC_CONTENT_FROM_BLIP(mapBlip)
											
											IF REQUEST_AND_LOAD_DLC_CONTENT_BLIP_INFORMATION(eContent)
												
												CPRINTLN(DEBUG_PAUSE_MENU, " ---> PAUSE_MENU_MAP - Displaying data for a DLC blip")
																								
												DISPLAY_DLC_CONTENT_BLIP_INFORMATION(eContent, bShowStartMission, DLPhotoData)
											
											ENDIF
										ENDIF
										
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
						//**TWH - CMcM - #1396679 - Checked that the blip exists before checking against or storing mapBlipLastFrame. Checks were getting skipped the other way.
						//Store the blip we hovered over this frame.
						mapBlipLastFrame = mapBlip
					ENDIF
					
					// Kenneth R.
					IF g_sVehicleGenNSData.bInGarage
						IF IS_PAUSEMAP_IN_INTERIOR_MODE() 
							HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
						ELSE
							HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
							SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(g_sVehicleGenNSData.vGarageEntryCoords.x, g_sVehicleGenNSData.vGarageEntryCoords.y)
						ENDIF
					ENDIF
					
					
					IF g_bInMultiplayer
						GET_CACHED_DESCRIPTION(bShowStartMission)
						PROCESS_JOIN_MISSION()
						CONTROL_MISSION_PHOTO_DISPLAY()
						PROCESS_DLC_CONTENT_CALL()
					ENDIF
				ENDIF
			ENDWHILE
		BREAK
		
	ENDSWITCH
	g_bV2CoronaMapBlipIsPremiumRace = FALSE
	RELEASE_MAP_INFOCARD_TXDS()
	TERMINATE_THIS_THREAD()

ENDSCRIPT

