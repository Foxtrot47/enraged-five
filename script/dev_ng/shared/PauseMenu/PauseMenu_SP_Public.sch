//╒═════════════════════════════════════════════════════════════════════════════╕
//│						 SP Specific Pause Menu	Scripts							│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│						AUTHOR:			Ben Rollinson							│
//│						DATE:			28/11/12								│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "PauseMenu_Public.sch"
USING "commands_zone.sch"


/// PURPOSE:
///    Sets up a row of data to be shown on the pause menu map overlay column.
///    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    strLabelA - 	Stringtable identifier for the first string element in the data slot.
///    strLabelB - 	Stringtable identifier for the second string element in the data slot.
PROC PM_SET_DATA_SLOT_FOR_SP_MAP(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING strLabelA, STRING strLabelB)
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // appear active
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strLabelA) // text label
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strLabelB) // text label
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE: Sets the scaleform data slots to display the mission details
PROC PM_DISPLAY_SP_MAP_MISSION_DETAILS(INT iColumn, STRING strMissionNameLabel, STRING strMissionTypeLabel, STRING sTextDict, STRING sTextName)
	
	TAKE_CONTROL_OF_FRONTEND()
		
		IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_TITLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)						// column, 			this is usually 1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strMissionNameLabel)		// title, 			mission name
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strMissionTypeLabel)		// overlay, 		let's write the mission type here for now
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)						// rockstar logo, 	not necessary here
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextDict)		// txDict
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextName)		// txName
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)								// loadtype, 		0 from cloud, 1 from disk
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)								// iDisplayType, 	1 is for stores, 0 is for missions
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		//Ensure the second panel is blank.
		//TODO: Really need a way to hide it completely.
		PM_SET_DATA_SLOT_FOR_SP_MAP(1, 0, 0, 0, "", "")
		
		PM_DISPLAY_DATA_SLOT(iColumn)
							
	RELEASE_CONTROL_OF_FRONTEND()

ENDPROC

/// PURPOSE:
///    Displaying scaleform of store logos in the pause menu
/// PARAMS:
///    sTxDict - text label for the logo texture
///    sDescription - text label for the shop description
///    iColumn - which column, probably 0, 1, or 2
PROC PM_DISPLAY_SP_MAP_STORE_DETAILS(STRING sTxDict, STRING sDescription, INT iColumn = 1, INT iShopR = 0, INT iShopG = 0, INT iShopB = 0)
	
	TAKE_CONTROL_OF_FRONTEND()
		
		PM_RESET_ALL_DATA_SLOTS(iColumn)
		
		IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_TITLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)					// column, 			this is usually 1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")						// title, 			not necessary here
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")						// overlay, 		not necessary here
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)					// rockstar logo, 	not necessary here
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTxDict)	// txDict, 			logo texture
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTxDict)	// txName, 			same as txDict
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)							// loadtype, 		0 from cloud, 1 from disk
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)							// iDisplayType, 	1 is for stores, 0 is for missions
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iShopR)					// iShopR, 			red
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iShopG)					// iShopG, 			green
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iShopB)					// iShopB, 			blue
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		PM_SET_DATA_SLOT_FOR_SP_MAP(1, 0, 0, 0, sDescription, "")
		
		PM_DISPLAY_DATA_SLOT(iColumn)
							
	RELEASE_CONTROL_OF_FRONTEND()

ENDPROC

