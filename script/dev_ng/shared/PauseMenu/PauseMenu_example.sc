USING "PauseMenu_public.sch"

CONST_INT MIDDLE_COLUMN 1

CONST_INT MY_MENU HASH("PM_REPLAY")
CONST_INT SUBMENU HASH("PM_REPLAY_LIST")

// as an example, these ids are non-contiguous
CONST_INT ID_AUDIO		25
CONST_INT ID_DISPLAY  	50

SCRIPT( PAUSE_MENU_LAUNCH_DATA args ) 

	PRINTLN("SCRIPT LAUNCHED WITH OP: ", args.operation, ", Menu: ", args.MenuScreenId, ", Prev:", args.PreviousId, ", Unique: ", args.UniqueIdentifier)
	PRINTLN("MY MENU HASH: ", 	MY_MENU)
	PRINTLN("MY SUBMENU HASH: ", SUBMENU)
	
	SWITCH args.operation
	
		// Probably won't be used, but this operation (which is dependent on the xml having continual=true on)
		// is updated as long as the menu it belongs to is active and on screen.
		CASE kUpdate
			WHILE TRUE
				PRINTLN("Just Chillin' in Update()")
				// perform any/all querying stuff here.
				// not shown because I don't like it as much as the below stuff
				WAIT(5000)
			ENDWHILE
		BREAK
		
		/// ----------------------------------------------------------------
		// Triggered whenever a menu needs to be filled out
		// switch off args.MenuScreenId to know which to fill out
		CASE kFill
		CASE kLayoutChange
			IF args.MenuScreenId = MY_MENU
				PRINTLN("Acknowledged layout Changed!")
				PM_SET_DATA_SLOT(MIDDLE_COLUMN,0,SUBMENU+PREF_OPTIONS_THRESHOLD,  	ID_AUDIO,	TRUE, "PM_PANE_AUD")
				PM_SET_DATA_SLOT(MIDDLE_COLUMN,1,SUBMENU+PREF_OPTIONS_THRESHOLD,	ID_DISPLAY,	TRUE, "PM_PANE_DIS")
				PM_DISPLAY_DATA_SLOT(MIDDLE_COLUMN)
			ENDIF
			
			IF args.MenuScreenId = SUBMENU
				// FILL OUT CRAP IN NEXT COLUMN (once it exists)
			ENDIF
		BREAK


		// Is triggered when a menuitem is selected
		// Use to launch options when selected.
		CASE kTriggerEvent
			PRINTLN("Event Triggered for ", args.MenuScreenId, " and ", args.UniqueIdentifier)
			SWITCH args.MenuScreenId
				// -----------------------------------------
				CASE MY_MENU
					PRINTLN("I'm intercepting this, but don't need to DO anything, because scaleform handled it for me; UniqueID:", args.UniqueIdentifier)
				BREAK
				
				// -----------------------------------------
			
				CASE SUBMENU
					SWITCH args.UniqueIdentifier
						CASE ID_AUDIO 	PRINTLN("You pressed the AUDIO button!")	BREAK
						CASE ID_DISPLAY	PRINTLN("You pressed the DISPLAY button!")	BREAK
					ENDSWITCH
				BREAK
				
				
			ENDSWITCH
		BREAK

	ENDSWITCH
	
	
	


	TERMINATE_THIS_THREAD()

ENDSCRIPT

