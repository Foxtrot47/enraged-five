/***********************************
*	Name: SP_MenuPed.sc
*	Author: Brenda Carey
*	Date: 15/10/2012
***********************************/

USING "PauseMenu_public.sch"
USING "net_include.sch"
USING "Screens_Header.sch"

// ************************************************************
// ******************** MAIN SCRIPT LOOP **********************
// ************************************************************

FUNC BOOL HAS_SP_STATS_BEEN_SET(PED_VARIATION_STRUCT& sTempVariations)

	IF sTempVariations.iTextureVariation[0] = 0
	AND sTempVariations.iTextureVariation[1] = 0
	AND sTempVariations.iTextureVariation[2] = 0
	AND sTempVariations.iTextureVariation[3] = 0
	AND sTempVariations.iTextureVariation[4] = 0
	AND sTempVariations.iTextureVariation[5] = 0
	AND sTempVariations.iTextureVariation[6] = 0
	AND sTempVariations.iTextureVariation[7] = 0
	AND sTempVariations.iTextureVariation[8] = 0
	AND sTempVariations.iTextureVariation[9] = 0
	AND sTempVariations.iTextureVariation[10] = 0
	AND sTempVariations.iTextureVariation[11] = 0
	
	AND sTempVariations.iDrawableVariation[0] = 0
	AND sTempVariations.iDrawableVariation[1] = 0
	AND sTempVariations.iDrawableVariation[2] = 0
	AND sTempVariations.iDrawableVariation[3] = 0
	AND sTempVariations.iDrawableVariation[4] = 0
	AND sTempVariations.iDrawableVariation[5] = 0
	AND sTempVariations.iDrawableVariation[6] = 0
	AND sTempVariations.iDrawableVariation[7] = 0
	AND sTempVariations.iDrawableVariation[8] = 0
	AND sTempVariations.iDrawableVariation[9] = 0
	AND sTempVariations.iDrawableVariation[10] = 0
	AND sTempVariations.iDrawableVariation[11] = 0
	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


FUNC BOOL HAS_ANY_SP_SAVE_HAPPENED_TO_LOCAL_PLAYER()
	
	INT SPTimestamp 
	STAT_GET_INT(_SaveSpTimestamp, SPTimestamp)
	IF SPTimestamp = 0
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC
FUNC BOOL HAS_ANY_SP_SAVE_HAPPENED_TO_GAMER()
	
	INT SPTimestamp 
	GET_MENU_PED_INT_STAT(_SaveSpTimestamp, SPTimestamp)
	
	
	NET_NL()NET_PRINT("SP_MENUPED: HAS_ANY_SP_SAVE_HAPPENED_TO_GAMER: SPTimestamp = ")NET_PRINT_INT(SPTimestamp)

	IF SPTimestamp = 0
	
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL DRESS_PLAYER_IN_PROGLOGUE_GEAR()
	
	INT Arm1_progress	
	GET_MENU_PED_INT_STAT(FL_CO_ARM1, Arm1_progress)
	
	NET_NL()NET_PRINT("SP_MENUPED: DRESS_PLAYER_IN_PROGLOGUE_GEAR: Arm1_progress = ")NET_PRINT_INT(Arm1_progress)

	
	IF Arm1_progress = 0
		RETURN TRUE
	ENDIF
	
	
	
	BOOL bInitialClothesStored
	GET_MENU_PED_BOOL_STAT(CLO_STORED_INITIAL, bInitialClothesStored)
//	
	NET_NL()NET_PRINT("SP_MENUPED: DRESS_PLAYER_IN_PROGLOGUE_GEAR: bInitialClothesStored = ")NET_PRINT_BOOL(bInitialClothesStored)
//	
//	IF NOT bInitialClothesStored
//		RETURN TRUE
//	ENDIF
	

	
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DRESS_PLAYER_IN_DEFAULT_FRANKLIN_GEAR()
	
	INT Arm1_progress
	INT Arm2_Progress 
	GET_MENU_PED_INT_STAT(FL_CO_ARM1, Arm1_progress)
	GET_MENU_PED_INT_STAT(FL_CO_ARM2, Arm2_Progress)
	
	NET_NL()NET_PRINT("SP_MENUPED: DRESS_PLAYER_IN_DEFAULT_FRANKLIN_GEAR: Arm1_progress = ")NET_PRINT_INT(Arm1_progress)
	NET_NL()NET_PRINT("SP_MENUPED: DRESS_PLAYER_IN_DEFAULT_FRANKLIN_GEAR: Arm2_Progress = ")NET_PRINT_INT(Arm2_Progress)

	
	IF Arm1_progress = 2 
	AND Arm2_Progress = 0
		RETURN TRUE
	ENDIF
	
	
	
	BOOL bInitialClothesStored
	GET_MENU_PED_BOOL_STAT(CLO_STORED_INITIAL, bInitialClothesStored)
	NET_NL()NET_PRINT("SP_MENUPED: DRESS_PLAYER_IN_DEFAULT_FRANKLIN_GEAR: bInitialClothesStored = ")NET_PRINT_BOOL(bInitialClothesStored)	
//	IF NOT bInitialClothesStored
//		RETURN TRUE
//	ENDIF
		
	
	RETURN FALSE
	
		
	
ENDFUNC



/// PURPOSE: Sets the ped component and prop variations
PROC SET_PLAYER_PED_VARIATION_STATS_SP(PED_INDEX ped, INT iSlot = 0)

	IF IS_PED_INJURED(ped)
		PRINTLN("SP_MENUPED: SET_PLAYER_PED_VARIATION_STATS - Ped is dead")
		EXIT
	ENDIF
	
	MODEL_NAMES ModelCompare
	IF GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot) = ENUM_TO_INT(CHAR_MICHAEL)
		ModelCompare = PLAYER_ZERO
		NET_NL()NET_PRINT("SP_MENUPED: ModelCompare = CHAR_MICHAEL  ")
	ELIF GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot) = ENUM_TO_INT(CHAR_FRANKLIN)
		ModelCompare = PLAYER_ONE
		NET_NL()NET_PRINT("SP_MENUPED: ModelCompare = CHAR_FRANKLIN  ")
	ELIF  GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot) = ENUM_TO_INT(CHAR_TREVOR)
		ModelCompare = PLAYER_TWO
		NET_NL()NET_PRINT("SP_MENUPED: ModelCompare = CHAR_TREVOR  ")
	ENDIF

	
	IF (GET_ENTITY_MODEL(ped)) != ModelCompare
	
			NET_NL()NET_PRINT("SP_MENUPED: PLAYER_ZERO = ")NET_PRINT_INT(ENUM_TO_INT(PLAYER_ZERO))
			NET_NL()NET_PRINT("SP_MENUPED: PLAYER_ONE = ")NET_PRINT_INT(ENUM_TO_INT(PLAYER_ONE))
			NET_NL()NET_PRINT("SP_MENUPED: PLAYER_TWO = ")NET_PRINT_INT(ENUM_TO_INT(PLAYER_TWO))
			
			NET_NL()NET_PRINT("SP_MENUPED: GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot) = ")NET_PRINT_INT(GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot))
	
			IF ModelCompare = (PLAYER_ZERO)
				NET_NL()NET_PRINT("SP_MENUPED: GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot) = PLAYER_ZERO")
			ELIF ModelCompare = (PLAYER_ONE)
				NET_NL()NET_PRINT("SP_MENUPED: GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot) = PLAYER_ONE")
			ELIF ModelCompare = (PLAYER_TWO)
				NET_NL()NET_PRINT("SP_MENUPED: GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot) = PLAYER_TWO")
			ENDIF

			
			IF GET_ENTITY_MODEL(ped) = PLAYER_ZERO
				NET_NL()NET_PRINT("SP_MENUPED: GET_ENTITY_MODEL(ped) = PLAYER_ZERO")
			ELIF GET_ENTITY_MODEL(ped) = PLAYER_ONE
				NET_NL()NET_PRINT("SP_MENUPED: GET_ENTITY_MODEL(ped) = PLAYER_ONE")
			ELIF GET_ENTITY_MODEL(ped) = PLAYER_TWO
				NET_NL()NET_PRINT("SP_MENUPED: GET_ENTITY_MODEL(ped) = PLAYER_TWO")
			ENDIF

	
		PRINTLN("SP_MENUPED: SET_PLAYER_PED_VARIATION_STATS - Ped doesn't match last player model")
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTLN("SP_MENUPED: SET_PLAYER_PED_VARIATION_STATS - Setting player ped varitions from stats")
	#ENDIF
	
	PED_VARIATION_STRUCT sTempVariations
	
	// Store the peds variations in the packed stats for use with the front end menu.
	sTempVariations.iTextureVariation[0] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HEAD, iSlot)
	sTempVariations.iTextureVariation[1] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_BERD, iSlot)
	sTempVariations.iTextureVariation[2] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HAIR, iSlot)
	sTempVariations.iTextureVariation[3] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_TORSO, iSlot)
	sTempVariations.iTextureVariation[4] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_LEG, iSlot)
	sTempVariations.iTextureVariation[5] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HAND, iSlot)
	sTempVariations.iTextureVariation[6] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_FEET, iSlot)
	sTempVariations.iTextureVariation[7] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_TEETH, iSlot)
	sTempVariations.iTextureVariation[8] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_SPECIAL, iSlot)
	sTempVariations.iTextureVariation[9] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_SPECIAL2, iSlot)
	sTempVariations.iTextureVariation[10] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_DECL, iSlot)
	sTempVariations.iTextureVariation[11] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_TEXTVAR_JBIB, iSlot)
	
	sTempVariations.iDrawableVariation[0] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HEAD, iSlot)
	sTempVariations.iDrawableVariation[1] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_BERD, iSlot)
	sTempVariations.iDrawableVariation[2] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HAIR, iSlot)
	sTempVariations.iDrawableVariation[3] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_TORSO, iSlot)
	sTempVariations.iDrawableVariation[4] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_LEG, iSlot)
	sTempVariations.iDrawableVariation[5] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HAND, iSlot)
	sTempVariations.iDrawableVariation[6] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_FEET, iSlot)
	sTempVariations.iDrawableVariation[7] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_TEETH, iSlot)
	sTempVariations.iDrawableVariation[8] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_SPECIAL, iSlot)
	sTempVariations.iDrawableVariation[9] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_SPECIAL2, iSlot)
	sTempVariations.iDrawableVariation[10] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_DECL, iSlot)
	sTempVariations.iDrawableVariation[11] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_DRAWVAR_JBIB, iSlot)
	
	sTempVariations.iPaletteVariation[0] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_HEAD, iSlot)
	sTempVariations.iPaletteVariation[1] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_BERD, iSlot)
	sTempVariations.iPaletteVariation[2] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_HAIR, iSlot)
	sTempVariations.iPaletteVariation[3] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_TORSO, iSlot)
	sTempVariations.iPaletteVariation[4] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_LEG, iSlot)
	sTempVariations.iPaletteVariation[5] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_HAND, iSlot)
	sTempVariations.iPaletteVariation[6] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_FEET, iSlot)
	sTempVariations.iPaletteVariation[7] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_TEETH, iSlot)
	sTempVariations.iPaletteVariation[8] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_SPECIAL, iSlot)
	sTempVariations.iPaletteVariation[9] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_SPECIAL2, iSlot)
	sTempVariations.iPaletteVariation[10] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_DECL, iSlot)
	sTempVariations.iPaletteVariation[11] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PALVAR_JBIB, iSlot)
	
	sTempVariations.iPropIndex[0] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_0, iSlot)
	sTempVariations.iPropIndex[1] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_1, iSlot)
	sTempVariations.iPropIndex[2] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_2, iSlot)
	sTempVariations.iPropIndex[3] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_3, iSlot)
	sTempVariations.iPropIndex[4] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_4, iSlot)
	sTempVariations.iPropIndex[5] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_5, iSlot)
	sTempVariations.iPropIndex[6] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_6, iSlot)
	sTempVariations.iPropIndex[7] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_7, iSlot)
	sTempVariations.iPropIndex[8] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPINDX_8, iSlot)
	
	sTempVariations.iPropTexture[0] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_0, iSlot)
	sTempVariations.iPropTexture[1] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_1, iSlot)
	sTempVariations.iPropTexture[2] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_2, iSlot)
	sTempVariations.iPropTexture[3] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_3, iSlot)
	sTempVariations.iPropTexture[4] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_4, iSlot)
	sTempVariations.iPropTexture[5] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_5, iSlot)
	sTempVariations.iPropTexture[6] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_6, iSlot)
	sTempVariations.iPropTexture[7] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_7, iSlot)
	sTempVariations.iPropTexture[8] = GET_PACKED_MENU_STAT_INT(PACKED_SP_CLTH_PROPTEXT_8, iSlot)
	
	#IF IS_DEBUG_BUILD
	
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation SLOT = ")PRINTINT(iSlot)PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[0])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[0])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[1])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[1])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[2])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[2])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[3])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[3])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[4])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[4])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[5])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[5])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[6])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[6])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[7])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[7])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[8])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[8])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[9])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[9])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[10])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[10])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU SP COMPONENT variation [")PRINTINT(sTempVariations.iDrawableVariation[11])PRINTSTRING(",")PRINTINT(sTempVariations.iTextureVariation[11])PRINTSTRING("]")PRINTNL()

	
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation SLOT = ")PRINTINT(iSlot)PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[0])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[0])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[1])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[1])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[2])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[2])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[3])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[3])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[4])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[4])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[5])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[5])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[6])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[6])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[7])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[7])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("SP_MENUPED: ...LOADING MENU PROP SP variation [")PRINTINT(sTempVariations.iPropIndex[8])PRINTSTRING(",")PRINTINT(sTempVariations.iPropTexture[8])PRINTSTRING("]")PRINTNL()
	
	#ENDIF
	
	
	IF HAS_SP_STATS_BEEN_SET(sTempVariations) = FALSE
	
		NET_NL()NET_PRINT("SP_MENUPED: HAS_SP_STATS_BEEN_SET(sTempVariations) = FALSE so calling RESTORE_PLAYER_PED_VARIATIONS ")
	
		RESTORE_PLAYER_PED_VARIATIONS(ped)

	ELSE
		NET_NL()NET_PRINT("SP_MENUPED: HAS_SP_STATS_BEEN_SET(sTempVariations) = FALSE so calling SET_PED_VARIATIONS")

		SET_PED_VARIATIONS(ped, sTempVariations)
	ENDIF
ENDPROC









SCRIPT( PAUSE_MENU_LAUNCH_DATA args ) 

	#IF IS_DEBUG_BUILD
		PRINTLN("SP_MENUPED Select Menu Launched with args: ", args.operation, ", Menu: ", args.MenuScreenId, ", Prev:", args.PreviousId, ", Unique: ", args.UniqueIdentifier)
	#ENDIF
	
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	

	
	IF args.operation = kPopulatePeds
	
		PED_INDEX FinalPed 
		MODEL_NAMES FinalPedModel
		
			
		IF HAS_ANY_SP_SAVE_HAPPENED_TO_GAMER() = FALSE	
			NET_NL()NET_PRINT("SP_MENUPED: GENERATE_PAUSE_MENU_PED: HAS_ANY_SP_SAVE_HAPPENED_TO_GAMER() = FALSE")
		
			REQUEST_MENU_PED_MODEL(PLAYER_ZERO)
			WHILE HAS_MODEL_LOADED(PLAYER_ZERO) = FALSE
				WAIT(0)
			ENDWHILE
			NET_NL()NET_PRINT("SP_MENUPED: GENERATE_PAUSE_MENU_PED: MICHAEL OUTFIT_P0_PROLOGUE ")

			FinalPed = CREATE_PED(PEDTYPE_SPECIAL, PLAYER_ZERO, GET_FINAL_RENDERED_CAM_COORD(), 0.0, FALSE, FALSE)
			
			//#2304090 - Hardcoded data look up command call to save memory.
			//SET_PED_COMP_ITEM_CURRENT_SP(FinalPed, COMP_TYPE_OUTFIT, OUTFIT_P0_PROLOGUE, FALSE)
			//										Component			Draw	Texture
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_HAIR, 		0, 		0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL, 	5, 		0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_TORSO, 	31, 	0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_LEG, 		26, 	0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_FEET, 		14, 	0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_HAND, 		5, 		0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL, 	0, 		0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL2, 	0, 		0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_DECL, 		0, 		0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_BERD, 		0, 		0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_TEETH, 	0, 		0		)
			SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_JBIB, 		0, 		0		)
			
			FREEZE_ENTITY_POSITION(FinalPed, TRUE)

			GIVE_PED_TO_PAUSE_MENU(FinalPed)
			
			
			
		ELSE
	
			NET_NL()NET_PRINT("SP_MENUPED: GENERATE_PAUSE_MENU_PED: HAS_ANY_SP_SAVE_HAPPENED_TO_GAMER() = TRUE")


			IF DRESS_PLAYER_IN_PROGLOGUE_GEAR()
				
				REQUEST_MENU_PED_MODEL(PLAYER_ZERO)
				WHILE HAS_MODEL_LOADED(PLAYER_ZERO) = FALSE
					WAIT(0)
				ENDWHILE
				NET_NL()NET_PRINT("SP_MENUPED: GENERATE_PAUSE_MENU_PED: MICHAEL OUTFIT_P0_DEFAULT DRESS_PLAYER_IN_PROGLOGUE_GEAR() ")

				FinalPed = CREATE_PED(PEDTYPE_SPECIAL, PLAYER_ZERO, GET_FINAL_RENDERED_CAM_COORD(), 0.0, FALSE, FALSE)
				
				//#2304090 - Hardcoded data look up command call to save memory.
				//SET_PED_COMP_ITEM_CURRENT_SP(FinalPed, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				//										Component			Draw	Texture
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_HAIR, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL, 	0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_TORSO, 	0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_LEG, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_FEET, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_HAND, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL, 	0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL2, 	0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_DECL, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_BERD, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_TEETH, 	0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_JBIB, 		0, 		0		)

				FREEZE_ENTITY_POSITION(FinalPed, TRUE)

				FINALIZE_HEAD_BLEND(FinalPed)

				GIVE_PED_TO_PAUSE_MENU(FinalPed)
				
			ELIF DRESS_PLAYER_IN_DEFAULT_FRANKLIN_GEAR()
				
				NET_NL()NET_PRINT("SP_MENUPED: GENERATE_PAUSE_MENU_PED: DRESS_PLAYER_IN_DEFAULT_FRANKLIN_GEAR ")
				
				REQUEST_MENU_PED_MODEL(PLAYER_ONE)
				WHILE HAS_MODEL_LOADED(PLAYER_ONE) = FALSE
					WAIT(0)
				ENDWHILE

				FinalPed = CREATE_PED(PEDTYPE_SPECIAL, PLAYER_ONE, GET_FINAL_RENDERED_CAM_COORD(), 0.0, FALSE, FALSE)
				
				//#2304090 - Hardcoded data look up command call to save memory.
				//SET_PED_COMP_ITEM_CURRENT_SP(FinalPed, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
				//										Component			Draw	Texture
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_HAIR, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL, 	0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_TORSO, 	8, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_LEG, 		8, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_FEET, 		6, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_HAND, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL, 	14, 	0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_SPECIAL2, 	0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_DECL, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_BERD, 		0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_TEETH, 	0, 		0		)
				SET_PED_COMPONENT_VARIATION(FinalPed, 	PED_COMP_JBIB, 		0, 		0		)

				FREEZE_ENTITY_POSITION(FinalPed, TRUE)

				FINALIZE_HEAD_BLEND(FinalPed)

				GIVE_PED_TO_PAUSE_MENU(FinalPed)
				
				
			ELSE

				INT iSlot = 0
				VECTOR Position = GET_FINAL_RENDERED_CAM_COORD()

				
				FinalPedModel = GET_PLAYER_PED_MODEL(INT_TO_ENUM(enumCharacterList, GET_PACKED_MENU_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot)))

				REQUEST_MENU_PED_MODEL(FinalPedModel)
					
				WHILE HAS_MODEL_LOADED(FinalPedModel) = FALSE
					WAIT(0)
				ENDWHILE
				
				FinalPed = CREATE_PED(PEDTYPE_SPECIAL, FinalPedModel, Position, 0, FALSE, FALSE)
	//			CLEAR_PED_TASKS_IMMEDIATELY(FinalPed)
				SET_PED_DESIRED_HEADING(FinalPed, GET_ENTITY_HEADING(FinalPed))
				SET_ENTITY_COLLISION(FinalPed, FALSE)
				SET_ENTITY_VISIBLE(FinalPed, FALSE)
				
				SET_PLAYER_PED_VARIATION_STATS_SP(FinalPed, iSlot)
				
				FINALIZE_HEAD_BLEND(FinalPed)
				
				FREEZE_ENTITY_POSITION(FinalPed, TRUE)
				
				NET_NL()NET_PRINT("SP_MENUPED: CREATED PED for slot ")NET_PRINT_INT(iSlot)NET_NL()
				
				GIVE_PED_TO_PAUSE_MENU(FinalPed)
				
				NET_NL()NET_PRINT("SP_MENUPED: WHILE args.operation = kPopulatePeds ")
			ENDIF
		ENDIF
					
	ENDIF
	
	NET_NL()NET_PRINT("SP_MENUPED: TERMINATE THREAD - args.operation <> kPopulatePeds ")NET_NL()

	TERMINATE_THIS_THREAD()
	
	
	
	
	
	

ENDSCRIPT

