/***********************************
*	Name: PauseMenu_Multiplayer_Playlists
*	Author: James Adwick
*	Date: 24/11/2012
*	Purpose: Handles playlists in Pause Menu
***********************************/

USING "PauseMenu_Multiplayer_Private.sch"
USING "FMMC_header.sch"

CONST_INT CREATED_PLAYLIST_LENGTH FMMC_MAX_PLAY_LIST_LENGTH

CONST_INT PLAYLIST_SAVE_NAME	0
CONST_INT PLAYLIST_SAVING		1
CONST_INT PLAYLIST_PUBLISH		2
CONST_INT PLAYLIST_SAVED		3

INT iPlaylistSavingState
INT iPlaylistDeleteState
OSK_STATUS playlistOSKStatus
INT iPlaylistKBStage, iPlaylistProfanityToken


CONST_INT CHALLENGE_SAVE_NAME	0
CONST_INT CHALLENGE_SAVING		1
CONST_INT CHALLENGE_PUBLISH		2
CONST_INT CHALLENGE_SAVED		3

INT iChallengeSavingState
OSK_STATUS ChallengeOSKStatus
INT iChallengeKBStage, iChallengeProfanityToken
INT iRockstarCreatedCount

SCRIPT_TIMER iQuickMatchTimer
UGC_PATHS_STRUCT szFilePaths
CACHED_MISSION_DESCRIPTION_LOAD_VARS sCMDLvars

BOOL bDoUpdateOnly = FALSE, bDisplayEditingPlaylistMissionDetails = FALSE, bMyPlaylistDataAvailable, bShowError, bZeroLengthPlaylist

FUNC BOOL SKIP_RANK_CHECK_PAUSE_MENU_MISSIONS(INT rootidhash)
	
	IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_SHORT_TRIP(rootidhash) 
	AND g_sMPTunables.bENABLE_FIXER_COOP_PAUSEMENU
		PRINTLN("SKIP_RANK_CHECK_PAUSE_MENU_MISSIONS True. This is a Short trip and g_sMPTunables.bENABLE_FIXER_COOP_PAUSEMENU = ", g_sMPTunables.bENABLE_FIXER_COOP_PAUSEMENU)
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(rootidhash)
	AND HAS_LOCAL_PLAYER_COMPLETED_ALL_ULP_MISSIONS_AS_LEADER()
		PRINTLN("SKIP_RANK_CHECK_PAUSE_MENU_MISSIONS True. This is a ULP mission and HAS_LOCAL_PLAYER_COMPLETED_ALL_ULP_MISSIONS_AS_LEADER")
		RETURN TRUE
	ENDIF
	#ENDIF

	#IF FEATURE_DLC_2_2022
	IF IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_PART_ONE_MISSION(rootidhash)
	AND HAS_LOCAL_PLAYER_COMPLETED_ALL_XMAS22_STORY_PART_ONE_MISSIONS_AS_LEADER()
		PRINTLN("SKIP_RANK_CHECK_PAUSE_MENU_MISSIONS True. This is a Xmas Story Part 1 mission and HAS_LOCAL_PLAYER_COMPLETED_ALL_XMAS_STORY_PART_ONE_MISSIONS_AS_LEADER")
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_PART_TWO_MISSION(rootidhash)
	AND HAS_LOCAL_PLAYER_COMPLETED_ALL_XMAS22_STORY_MISSIONS_AS_LEADER()
		PRINTLN("SKIP_RANK_CHECK_PAUSE_MENU_MISSIONS True. This is a Xmas Story Part 2 mission and HAS_LOCAL_PLAYER_COMPLETED_ALL_XMAS_STORY_MISSIONS_AS_LEADER")
		RETURN TRUE
	ENDIF
	#ENDIF

    RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_ALL_MISSIONS_OF_TYPE(INT iMissionType, BOOL &bDataAvailable)
	BOOL bIsLTS, bIsCTF, bIsMission, bIgnoreMissions, bIsStuntRace, bisArenaWars, bisTargetAssualt, biskingofthehill, bissurvivalseries
	BOOL bisdeathmatch
//	IF HAVE_ROCKSTAR_CREATED_MISSIONS_LOADED()
//	AND HAVE_ROCKSTAR_VERIFIED_MISSIONS_LOADED()

		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - displaying missions of type: ", iMissionType)
		CLEAR_PM_COLUMN(THIRD_COLUMN)
		PAUSE_MENU_UPDATE_COLUMN(THIRD_COLUMN)
		
		
		CLEAR_ALL_CAPTURE_BUTTON_CONTEXTS()
		CLEAR_ALL_RACE_BUTTON_CONTEXTS()
		CLEAR_ALL_DEATHMATCH_BUTTON_CONTEXTS()
		IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
			bIsCTF = TRUE
			CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - bIsCTF")
			SET_CAPTURE_BUTTON_CONTEXTS()
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
			bIsLTS = TRUE
			CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - bIsLTS")
//		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_VS
//			bIsVS = TRUE
//			CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - bIsVS")
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
			bIsMission = TRUE
			CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - bIsMission")
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
			SET_RACE_BUTTON_CONTEXTS()
			bIgnoreMissions = TRUE
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
			SET_STUNT_RACE_BUTTON_CONTEXTS()
			bIgnoreMissions = TRUE
			bIsStuntRace = TRUE
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT
			SET_TARGET_ASSAULT_RACE_BUTTON_CONTEXTS()
			//bIgnoreMissions = TRUE
			bisTargetAssualt = TRUE
			bIgnoreMissions = FALSE
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
			SET_ARENA_WARS_BUTTON_CONTEXTS()
			bIgnoreMissions = FALSE
			bisArenaWars = TRUE
			iMissionType = -1
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
			SET_DEATHMATCH_BUTTON_CONTEXTS()
			bIgnoreMissions = TRUE
			bisdeathmatch = TRUE
		ELIF pauseMenuRoute[2] =PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
		//	bIgnoreMissions = FALSE
			biskingofthehill = TRUE
			iMissionType = -1
			//SET_DEATHMATCH_BUTTON_CONTEXTS()
			bIgnoreMissions = TRUE
		ELSE
		 	bIgnoreMissions = TRUE
		ENDIF
		
		
		
		IF NOT bDoUpdateOnly
			CLEAR_PM_COLUMN(FIRST_COLUMN)
			
			//Set Column Title
			IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
				PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_RC_STUNT", "")
			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
				IF currentlyShowingRaces = SHOWING_ALL_RACES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_RC_ALL", "")
				ELIF currentlyShowingRaces = SHOWING_LAND_RACES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_RC_LAND", "")
				ELIF currentlyShowingRaces = SHOWING_BIKE_RACES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_RC_BIKE", "")
				ELIF currentlyShowingRaces = SHOWING_WATER_RACES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_RC_BOAT", "")
				ELIF currentlyShowingRaces = SHOWING_AIR_RACES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_RC_AIR", "")
				ELIF currentlyShowingRaces = SHOWING_STUNT_RACES 	
					IF g_sMPTunables.bEnableStuntRaces
						PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_RC_STUNT", "")
					ELSE
						PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_RC_ALL", "")
					ENDIF
				ELIF currentlyShowingRaces = SHOWING_TARGET_ASSAULT_RACES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "RC_TARGASSAULT", "")
				
				ELIF currentlyShowingRaces = SHOWING_ARENA_RACES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_ARENA", "")
				
				ENDIF
			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
				IF currentlyShowingDeathmatches = SHOWING_ALL_DEATHMATCHES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_DM_ALL", "")
				ELIF currentlyShowingDeathmatches = SHOWING_DEATHMATCHES
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_DM_FFA", "")
				ELIF currentlyShowingDeathmatches = SHOWING_TEAM_DEATHMATCHES
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_DM_TEAM", "")
				ELIF currentlyShowingDeathmatches = SHOWING_VEHICLE_DEATHMATCHES
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_DM_VEH", "")
				ELIF currentlyShowingDeathmatches =  SHOWING_ARENA_DEATHMATCHES
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_ARENA", "")
				ENDIF
			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_TSURV", "")
			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_TBSJUMP", "")
			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
				IF currentlyShowingCaptures = SHOWING_ALL_CAPTURES 	
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_CAP_ALL", "")
				ELIF currentlyShowingCaptures = SHOWING_GTA_CAPTURES
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_CAP_GTA", "")
				ELIF currentlyShowingCaptures = SHOWING_HOLD_CAPTURES
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_CAP_HOLD", "")
				ELIF currentlyShowingCaptures = SHOWING_RAID_CAPTURES
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_CAP_RAID", "")
				ELIF currentlyShowingCaptures = SHOWING_CONTEND_CAPTURES
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_CAP_CONT", "")
				ENDIF
			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_TLTS", "")
			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_TMISSIONS", "")
			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
				PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "RC_TARGASSAULT", "")
//			ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_VS
//					PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_TVS", "")
			ELIF  pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
				PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_ARENA", "")
			ELIF  pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
				PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "KOTH_MN_STND", "")
			ENDIF
		ENDIF
		INT i
		INT iCount
		INT iPlayerRank = GET_PLAYER_RANK(PLAYER_ID())
		
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - Begin. Player Rank: ", iPlayerRank)
				
		FOR i = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED - 1)
			IF i=0
				iPlaylistCreatedMission = 0
			ENDIF
			//IF g_FMMC_ROCKSTAR_CREATED.bInUse[i]
			IF IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
			AND (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iRank < 9999 OR SKIP_RANK_CHECK_PAUSE_MENU_MISSIONS(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash))
				IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType = iMissionType 
					OR (bisArenaWars AND  IS_THIS_AN_ARENA_WAR_ACTIVITY(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iAdversaryModeType))
					OR (biskingofthehill AND IS_CONTENT_KING_OF_THE_HILL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType ,  g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType)))
				AND (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iContactCharEnum = 0 OR CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash))
				//AND NOT IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_CONTACT_MISSION)
				AND NOT IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END)
				AND NOT IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_HEIST_PLANNING)		// Keith 4/3/14: To discount new Heist Planning missions
				AND NOT IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT)
				AND (iPlayerRank >= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iRank OR SKIP_RANK_CHECK_PAUSE_MENU_MISSIONS(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash))
				AND !CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_PROFESSIONAL_JOB(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash)
				AND !SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash)
				AND !(bisdeathmatch AND IS_CONTENT_KING_OF_THE_HILL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType ,  g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType))
				AND !IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash) 
				AND !IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash) 
				#IF FEATURE_TUNER
				AND !IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash)
				#ENDIF
				#IF FEATURE_FIXER
				AND  (!IS_THIS_ROOT_CONTENT_ID_A_FIXER_SHORT_TRIP(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash) OR g_sMPTunables.bENABLE_FIXER_COOP_PAUSEMENU)
				AND !(IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash))
				#ENDIF
					IF (bIsLTS AND IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_LTS))
					OR (bIsCTF AND IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_CTF))
					//OR (bIsVS AND IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS))
					OR (bIsMission 
						AND !IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_LTS) 
						AND !IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_CTF))
					OR (bIsStuntRace AND g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType = FMMC_TYPE_RACE AND (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_STUNT OR g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_STUNT_P2P OR g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET OR g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET_P2P))
					OR (bIgnoreMissions AND !IS_THIS_AN_ARENA_WAR_ACTIVITY(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iAdversaryModeType))
					OR (bisTargetAssualt  AND g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType = FMMC_TYPE_RACE AND (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET OR g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET_P2P))
					OR (bisArenaWars AND  IS_THIS_AN_ARENA_WAR_ACTIVITY(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iAdversaryModeType))
					OR (biskingofthehill AND IS_CONTENT_KING_OF_THE_HILL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType ,  g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType))
					OR (bissurvivalseries	AND (IS_THIS_A_NEW_SURVIVAL_MISSION(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iAdversaryModeType) OR g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType = FMMC_TYPE_SURVIVAL ))
						iPlaylistCreatedMission = (i+1)
						IF DOES_JOB_SUBTYPE_MATCH_FILTER(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType, 
														 g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType, 
														 g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, 
														 g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSetTwo, 
														 g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iAdversaryModeType)
							IF bDoUpdateOnly
								MP_UPDATE_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iLesRating,#ENDIF FIRST_COLUMN, iCount, CREATOR_MENU_ID, i, 
								CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iMinPlayers, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iMaxPlayers, DEFAULT, DEFAULT, DEFAULT, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].tlName), g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].tlMissionName, 
								TRUE, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType, IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), DEFAULT, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iAdversaryModeType) // First i, may need to be a count. Whereas second i should be index in array
							ELSE
								MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iLesRating,#ENDIF FIRST_COLUMN, iCount, CREATOR_MENU_ID, i, 
								CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iMinPlayers, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iMaxPlayers, DEFAULT, DEFAULT, DEFAULT, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].tlName), g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].tlMissionName, 
								TRUE, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iSubType, IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED),DEFAULT,  g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iAdversaryModeType)
								IF iCount = 0
									pauseMenuRoute[3] = i
									iPauseMenuItemSelected = i
								ENDIF
							ENDIF
							
							CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - CHECK MMM - Count = ", 
									iCount, " i = ", i, ". LastAdded: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].tlMissionName, 
									", Rank: [", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iRank, "]")
							
							IF i = iStoredIndex
								CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] A - iStoredHighlight = ", iCount)
								//SET_PM_HIGHLIGHT(FIRST_COLUMN, iCount, FALSE, TRUE)
								CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - DO HIGHLIGHT - CHECK QQQ")
								iStoredHighlight = iCount
							ENDIF
							
							iCount++
							iRockstarCreatedCount = iCount
						ELSE
							CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - Subtype filter is gubbed for mission: ", 
											g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].tlMissionName)		
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - iPlaylistCreatedMission = ", iPlaylistCreatedMission)
		
		
		FOR i = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED - 1)
			IF IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
			//IF g_FMMC_ROCKSTAR_VERIFIED.bInUse[i]
				IF (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType = iMissionType OR (bisArenaWars AND IS_THIS_AN_ARENA_WAR_ACTIVITY(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType)) )  
				AND g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iContactCharEnum = 0
				AND NOT IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash) 
				AND NOT IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash) 
				#IF FEATURE_TUNER
				AND NOT IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash)
				#ENDIF
				#IF FEATURE_FIXER
				AND (!IS_THIS_ROOT_CONTENT_ID_A_FIXER_SHORT_TRIP(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash) OR g_sMPTunables.bENABLE_FIXER_COOP_PAUSEMENU)
				AND !(IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[i].iRootContentIdHash))
				#ENDIF
					IF (bIsLTS AND IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_LTS))
					OR (bIsCTF AND IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_CTF))
					//OR (bIsVS AND IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS))
					OR (bIsMission 
						AND !IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_LTS) 
						AND !IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_CTF))
					OR (bIsStuntRace AND g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType = FMMC_TYPE_RACE AND (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_STUNT OR g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_STUNT_P2P OR g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET OR g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET_P2P))
					OR (bIgnoreMissions AND !IS_THIS_AN_ARENA_WAR_ACTIVITY(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType))
					OR (bisTargetAssualt  AND g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType = FMMC_TYPE_RACE AND (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET OR g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET_P2P))
					OR (bisArenaWars AND  IS_THIS_AN_ARENA_WAR_ACTIVITY(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType))
					OR (biskingofthehill AND IS_CONTENT_KING_OF_THE_HILL(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType ,  g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType))
					OR (bissurvivalseries	AND (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType = FMMC_TYPE_SURVIVAL OR IS_THIS_A_NEW_SURVIVAL_MISSION(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType)))
						IF DOES_JOB_SUBTYPE_MATCH_FILTER(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, 
														 g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType, 
														 g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, 
														 g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSetTwo, 0)
							IF bDoUpdateOnly
								MP_UPDATE_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iLesRating,#ENDIF FIRST_COLUMN, iCount, CREATOR_MENU_ID, (i + iPlaylistCreatedMission), 
								CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iMinPlayers, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iMaxPlayers, DEFAULT, DEFAULT, DEFAULT, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].tlName), g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].tlMissionName, 
								FALSE, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType, IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), TRUE, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType) // First i, may need to be a count. Whereas second i should be index in array
							ELSE
								MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iLesRating,#ENDIF FIRST_COLUMN, iCount, CREATOR_MENU_ID, (i + iPlaylistCreatedMission), 
								CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iMinPlayers, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iMaxPlayers, DEFAULT, DEFAULT, DEFAULT, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].tlName), g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].tlMissionName, 
								FALSE, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType, IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), TRUE, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType)
							ENDIF
							
							iCount++
							
							//CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] ADD RACES - CHECK QQQ - Count = ", iCount, " i = ", i)
							
							IF (i+iRockstarCreatedCount) = iStoredIndex
								CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] B - iStoredHighlight = ", iCount)
								//SET_PM_HIGHLIGHT(FIRST_COLUMN, iCount, FALSE, TRUE)
								//CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] ADD RACES - DO HIGHLIGHT - CHECK RRR")
								
								iStoredHighlight = iCount
							ENDIF
							
							IF iCount = 0
								CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Z2 - SET iPauseMenuItemSelected to ", (i + iPlaylistCreatedMission), " from ", iPauseMenuItemSelected)
								iPauseMenuItemSelected = (i + iPlaylistCreatedMission)
								pauseMenuRoute[3] = (i + iPlaylistCreatedMission)
								CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] A - pauseMenuRoute[3] = ", pauseMenuRoute[3])
							ENDIF
						ELSE
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed DOES_JOB_SUBTYPE_MATCH_FILTER g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].tlName = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].tlName)
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed DOES_JOB_SUBTYPE_MATCH_FILTER g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType)
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed DOES_JOB_SUBTYPE_MATCH_FILTER g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType)
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed DOES_JOB_SUBTYPE_MATCH_FILTER g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType)
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed DOES_JOB_SUBTYPE_MATCH_FILTER IS_THIS_AN_ARENA_WAR_ACTIVITY = ", IS_THIS_AN_ARENA_WAR_ACTIVITY(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType))
//							
						
						ENDIF
					ELSE
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed IS_THIS_AN_ARENA_WAR_ACTIVITY g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].tlName = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].tlName)
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed IS_THIS_AN_ARENA_WAR_ACTIVITY g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType)
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed IS_THIS_AN_ARENA_WAR_ACTIVITY g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType)
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed IS_THIS_AN_ARENA_WAR_ACTIVITY g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType)
//							PRINTLN("[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE failed IS_THIS_AN_ARENA_WAR_ACTIVITY IS_THIS_AN_ARENA_WAR_ACTIVITY = ", IS_THIS_AN_ARENA_WAR_ACTIVITY(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iSubType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[i].iAdversaryModeType))
//							
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		IF iCount > 0
			iStoredHighlight = -1
			bDataAvailable = TRUE
		ENDIF
		
	
		IF iStoredIndex != -1
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Z3 - SET iPauseMenuItemSelected to ", iStoredIndex, " from ", iPauseMenuItemSelected)
			iPauseMenuItemSelected = iStoredIndex
			iStoredIndex = -1
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_ALL_MISSIONS_OF_TYPE - CHECK SSS - iPauseMenuItemSelected = ", iPauseMenuItemSelected)
		ENDIF
		
		RETURN TRUE
//	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE when all downloaded missions have been loaded. Updates bDataAvailable if data is available to display  
FUNC BOOL UPDATE_RECENTLY_PLAYED_JOBS(BOOL &bDataAvailable, INT iColumn = SECOND_COLUMN, INT iMissionType = -1, BOOL bIsLTSMission = FALSE, BOOL bAllowAll=FALSE, BOOL bIsCTFMission = FALSE, BOOL bIsVSMission = FALSE, BOOL bIsNewVSMission = FALSE, BOOL bIsCoOpMission = FALSE, BOOL bIsStuntRace = FALSE, BOOL bIsArenaWar = FALSE, BOOL bIsKingOfTheHill = FALSE)
//	IF HAVE_DOWNLOADED_MISSIONS_LOADED()
	
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_RECENTLY_PLAYED_JOBS - Updating second column to show downloaded missions")

		INT iMenuID = CREATOR_LIST_ID
		IF iColumn = FIRST_COLUMN
			iMenuID = CREATOR_MENU_ID
			CLEAR_PM_COLUMN(FIRST_COLUMN)
		ELSE
			CLEAR_MISSION_CREATOR_COLUMNS()
		ENDIF
		
		BOOL bAddColumnTitle, bActiveMission
		IF pauseMenuRoute[0] = ENUM_TO_INT(PM_D1_PLAYLISTS)
		AND pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_CREATE_PLAYLIST)
			PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_TRECENT", "")
		ELIF pauseMenuRoute[0] = ENUM_TO_INT(PM_D1_ALL_JOBS)
			IF pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_AVAILABLE_JOBS)
				bAddColumnTitle = TRUE	
			ENDIF
		ENDIF
		
		IF g_sRECENT_MISSION_HISTORY.iNumberOfMissions > 0
		AND HAVE_RECENTLY_PLAYED_JOBS_LOADED()
			iRecentlyPlayedJobCount = 0
			INT i

			CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] UPDATE_RECENTLY_PLAYED_JOBS - g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.iNumberOfMissions = ", g_sRECENT_MISSION_HISTORY.iNumberOfMissions)
			FOR i = 0  TO (g_sRECENT_MISSION_HISTORY.iNumberOfMissions - 1)
				bActiveMission = FALSE
				IF iMissionType = -1
				OR (g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType = iMissionType  AND !IS_THIS_AN_ARENA_WAR_ACTIVITY(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iAdversaryModeType) 
					 AND !IS_CONTENT_KING_OF_THE_HILL(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType))
				
				
					IF IS_FM_TYPE_UNLOCKED(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType)
					//AND g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iContactCharEnum = 0
					AND IS_SELECT_AVAILABLE() = 0
					AND RUN_NETWORK_CONTENT_CHECK()
					AND !(VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_sRECENT_MISSION_HISTORY.iRootContentIdHash[i]) AND NOT HAS_LOCAL_PLAYER_UNLOCKED_VCM_REPLAY())	
						IF (bIsLTSMission AND g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_LTS AND !bAllowAll)
						OR (bIsCTFMission AND g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_CTF AND !bAllowAll)
						OR (bIsVSMission AND g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_VERSUS AND !bAllowAll AND !IS_NEW_VERSUS_MISSION(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitset, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSetTwo, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iAdversaryModeType))
						OR (bIsNewVSMission AND IS_NEW_VERSUS_MISSION(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitset, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSetTwo, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iAdversaryModeType) AND !bAllowAll)
						OR (bIsCoOpMission AND g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_COOP AND !bAllowAll)
						OR (!bIsNewVSMission AND !bIsLTSMission AND !bAllowAll AND !bIsCTFMission AND !bIsVSMission AND !bIsCoOpMission AND !bIsStuntRace AND !bIsKingOfTheHill)
						OR (bIsStuntRace AND g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType = FMMC_TYPE_RACE AND (g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_STUNT OR g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_STUNT_P2P OR g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET OR g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_RACE_TYPE_TARGET_P2P) )
						OR (bIsArenaWar AND IS_THIS_AN_ARENA_WAR_ACTIVITY(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iAdversaryModeType))  
						OR (bIsKingOfTheHill AND IS_CONTENT_KING_OF_THE_HILL(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType))   
						//
						OR bAllowAll
							IF DOES_JOB_SUBTYPE_MATCH_FILTER(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSetTwo, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iAdversaryModeType)
								IF !IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT)
								AND !IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END)
								AND !IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_HEIST_PLANNING)
								AND !IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_sRECENT_MISSION_HISTORY.iRootContentIdHash[i])
								AND !IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_sRECENT_MISSION_HISTORY.iRootContentIdHash[i]) 
								#IF FEATURE_TUNER
								AND !IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_sRECENT_MISSION_HISTORY.iRootContentIdHash[i])
								#ENDIF
								#IF FEATURE_FIXER
								AND (!IS_THIS_ROOT_CONTENT_ID_A_FIXER_SHORT_TRIP(g_sRECENT_MISSION_HISTORY.iRootContentIdHash[i]) OR g_sMPTunables.bENABLE_FIXER_COOP_PAUSEMENU)
								AND !(IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION(g_sRECENT_MISSION_HISTORY.iRootContentIdHash[i]))
								#ENDIF
									IF NOT bAllowAll
										IF (bIsLTSMission)
										
											IF IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_LTS)
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be LTS and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be LTS but it's bit isn't set.")
											ENDIF
											
											IF g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_LTS
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be LTS and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be LTS but it's subtype isn't set.")
											ENDIF
										ELIF (bIsCTFMission)
										
											IF IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_CTF)
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be CTF and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be CTF but it's bit isn't set.")
											ENDIF
											
											IF g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_CTF
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be CTF and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be CTF but it's subtype isn't set.")
											ENDIF
										ELIF (bIsVSMission)
										
											IF IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS)
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be VS and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be VS but it's bit isn't set.")
											ENDIF
											
											IF g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_VERSUS
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be VS and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be VS but it's subtype isn't set.")
											ENDIF
										ELIF (bIsNewVSMission)
										
											IF IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS)
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be VS and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be VS but it's bit isn't set.")
											ENDIF
											
											IF g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_VERSUS
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be VS and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be VS but it's subtype isn't set.")
											ENDIF
										ELIF (bIsCoOpMission)
										
											IF IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_COOP)
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be Co-op and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be Co-op but it's bit isn't set.")
											ENDIF
											
											IF g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_COOP
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be Co-op and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be Co-op but it's subtype isn't set.")
											ENDIF
										ELIF (bIsArenaWar)
										
											IF IS_THIS_AN_ARENA_WAR_ACTIVITY(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iAdversaryModeType)
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be Arena War and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be Arena War but it's bit isn't set.")
											ENDIF
											
											IF g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType = FMMC_TYPE_ARENA_SERIES
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be Arena War and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be Arena War but it's subtype isn't set.")
											ENDIF
										ELIF (bIsKingOfTheHill)
										
											IF IS_CONTENT_KING_OF_THE_HILL(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType)
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be King Of The Hill and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job should be King Of The Hill but it's bit isn't set.")
											ENDIF
											
											IF g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType = FMMC_KING_OF_THE_HILL
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be King Of The Hill and it is!!!!! Success!")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job should be  King Of The Hill but it's subtype isn't set.")
											ENDIF
										ELSE
										
											IF IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet, ciMISSION_OPTION_BS_LTS)
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job shouldn't be LTS but it's bit is set.")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job shouldn't be LTS and isn't. Success!!!")
											ENDIF
											
											IF g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType = FMMC_MISSION_TYPE_LTS
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job shouldn't be LTS but it's bit is set.")
											ELSE
												CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] SUBTYPE! Job shouldn't be LTS and isn't. Success!!!")
											ENDIF
										ENDIF
									ENDIF

								// Need new verified/rockstar created nCategory for Bookmarked Jobs from Bobby.
									IF CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iMinPlayers, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iMaxPlayers, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iRank, pauseMenuRoute[0] = ENUM_TO_INT(PM_D1_PLAYLISTS), DEFAULT, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].tlName)
										CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] This Job should be active.")
										bActiveMission = TRUE
									ELSE
										CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] This Job should be INactive.")
										bActiveMission = FALSE
									ENDIF
									MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF iColumn, iRECENTLYPLAYEDJobCount, iMenuID, i, 
									bActiveMission, 
									g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].tlMissionName, GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i), g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iSubType, IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iBitSet , ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED),
									GET_MISSION_VERIFIED_PM_STAT(i),
									g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iAdversaryModeType
									
									)
									IF iRecentlyPlayedJobCount = 0
										CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] N - SET iPauseMenuItemSelected to ", i, " from ", iPauseMenuItemSelected)
										iPauseMenuItemSelected = i
									ENDIF
									iRecentlyPlayedJobCount++
								ELSE
									CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Job is either a Random Event or Contact Mission. Denied!")
								ENDIF
							ENDIF
						ELSE	
							CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Blocked by checks. C")	
						ENDIF
					ELSE	
						CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Blocked by checks. B")
						CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Blocked by checks. B, ", g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].tlMissionName)
						CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Blocked by checks. B, IS_SELECT_AVAILABLE() = ", IS_SELECT_AVAILABLE())
						CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Blocked by checks. B, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iContactCharEnum = ", g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iContactCharEnum)
						
					ENDIF
				ELSE
					CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Blocked by checks. A")
					CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Blocked by checks. A, ", g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].tlMissionName)
					CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] Blocked by checks. A iMissionType = ", iMissionType, ", g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType = ", g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[i].iType)
				ENDIF
			ENDFOR
			
			IF iRecentlyPlayedJobCount = 0
				CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] iRecentlyPlayedJobCount is 0")
				iRecentlyPlayedJobCount = -1
				HIDE_PM_COLUMN(THIRD_COLUMN)
				//ADD_JOB_CONTEXTS()
				bDataAvailable = FALSE
				RETURN TRUE
			ENDIF
			
			IF iRecentlyPlayedJobCount > 16
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] ADD_SCALEFORMXML_INIT_COLUMN_SCROLL_GEN - B")
				ADD_SCALEFORMXML_INIT_COLUMN_SCROLL_GEN(1, 1, 1, 1, 0)
				ADD_SCALEFORMXML_SET_COLUMN_SCROLL(0, iPauseMenuItemSelected, g_sRECENT_MISSION_HISTORY.iNumberOfMissions)
			ENDIF
			
			IF bAddColumnTitle
				CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] UPDATE_RECENTLY_PLAYED_JOBS - ADD_JOB_COLUMN_TITLE")
				//ADD_JOB_COLUMN_TITLE()
			ENDIF
			bDataAvailable = TRUE
			SET_BIT(iBS_DataAvailable, RECENTLY_PLAYED_AVAILABLE)
			DISPLAY_PAUSE_MENU_COLUMN(iColumn)
			RETURN TRUE
		ELSE
			bDataAvailable = FALSE
			RETURN TRUE
		ENDIF
		
		
//	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_RECENTLY_PLAYED_MISSIONS(BOOL &bDataAvailable, INT iColumn = FIRST_COLUMN)
	BOOL bVerified, bCreated
	
	
	IF g_sRECENT_MISSION_HISTORY.iNumberOfMissions > 0
				
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_RECENTLY_PLAYED - called...")
		
		CLEAR_PM_COLUMN(iColumn)
		//Set Column Title
		PM_SET_COLUMN_TITLE_ONLY(iColumn, "PM_TRECENT", "")
		
		INT i
		INT iTotal = (g_sRECENT_MISSION_HISTORY.iNumberOfMissions - 1)
		FOR i = 0 TO iTotal
			IF GET_MISSION_VERIFIED_PM_STAT(iTotal-i)
				bVerified = TRUE
			ELSE
				bVerified = FALSE
			ENDIF
			IF GET_MISSION_ROCKSTAR_CREATED_PM_STAT(iTotal-i)
				bCreated = TRUE
			ELSE
				bCreated = FALSE
			ENDIF
			// Will need nCategory for Recently Played Jobs.
			IF bDoUpdateOnly
				CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU UPDATE_RECENTLY_PLAYED_MISSIONS - bDoUpdateOnly")
				MP_UPDATE_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF iColumn, (iTotal-i), CREATOR_MENU_ID, (iTotal-i), 
				CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iMinPlayers, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iMaxPlayers, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iRank, DEFAULT, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iSubType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].tlName), 
				g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].tlMissionName, bCreated, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iSubType, IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), bVerified)
			ELSE
				PRINTLN("CMcM@PAUSEMENU UPDATE_RECENTLY_PLAYED_MISSIONS - NOT bDoUpdateOnly")
				MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF iColumn, (iTotal-i), CREATOR_MENU_ID, (iTotal-i), 
				CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iMinPlayers, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iMaxPlayers, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iRank, DEFAULT, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iSubType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].tlName), 
				g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].tlMissionName, bCreated, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iSubType, IS_BIT_SET(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), bVerified,
				g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[(iTotal-i)].iAdversaryModeType)
			ENDIF

		ENDFOR
		
					
		
		bDoUpdateOnly = FALSE
		bDataAvailable = TRUE
		DISPLAY_PAUSE_MENU_COLUMN(iColumn)
		IF iColumn = SECOND_COLUMN
			SET_PAUSE_MENU_MP_COLUMN_STATE(PAUSE_MENU_COLUMN_STATE_SECOND_COLUMN)
//			PM_MENU_SHIFT_DEPTH(1)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Z4a - SET iPauseMenuItemSelected to ", 0, " from ", iPauseMenuItemSelected)	
			iPauseMenuItemSelected = 0
		ELSE
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Z4b - SET iPauseMenuItemSelected to ", 0, " from ", iPauseMenuItemSelected)	
			iPauseMenuItemSelected = 0
		ENDIF
		SET_BIT(iBS_DataAvailable, RECENTLY_PLAYED_AVAILABLE)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC UPDATE_PLAYLIST_COLUMN(INT iColumn = SECOND_COLUMN, INT iPickedIndex = -1)	
	
	INT i
	INT iLength
	BOOL bVerified, bCreated
	IF iColumn = SECOND_COLUMN
		PM_SET_COLUMN_TITLE_ONLY(iColumn, "PM_TPLAYL", "")
	ENDIF
	
	REPEAT CREATED_PLAYLIST_LENGTH i
		
		IF g_sMenuPlayListDetails.sLoadedMissionDetails[i].bInUse
					
			iLength++
			
			TEXT_LABEL_63 tlTemp
			tlTemp = ""
			IF IS_BIT_SET(iBS_General, biEDIT_PLAYLIST_PICK)
				IF i = iPickedIndex
					tlTemp = "(*) - "
				ENDIF
			ENDIF
			tlTemp += g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName
			
			CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - UPDATE_PLAYLIST_COLUMN - GET_MISSION_VERIFIED_PM_STAT(i) = ", GET_MISSION_VERIFIED_PM_STAT(i), " - GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i) = ", GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i, iColumn))
			
			IF GET_MISSION_VERIFIED_PM_STAT(i)
				bVerified = TRUE
			ELSE
				bVerified = FALSE
			ENDIF
			IF GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i, iColumn)
				bCreated = TRUE
			ELSE
				bCreated = FALSE
			ENDIF
			MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF iColumn, i, CREATOR_LIST_ID, i, TRUE, tlTemp,  bCreated, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iType, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iSubType, IS_BIT_SET(g_sMenuPlayListDetails.sLoadedMissionDetails[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), bVerified, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iAdversaryModeType)
		ENDIF
		
	ENDREPEAT
	
	g_sMenuPlayListDetails.iLength = iLength
	
	DISPLAY_PAUSE_MENU_COLUMN(iColumn)

ENDPROC

PROC SAVE_SELECTED_MISSION_TO_PLAYLIST()

	INT i = 0
	INT index = -1
	
	// Find the next available slot 
	REPEAT CREATED_PLAYLIST_LENGTH i
		
		IF NOT g_sMenuPlayListDetails.sLoadedMissionDetails[i].bInUse
			index = i
			i = CREATED_PLAYLIST_LENGTH
		ENDIF
	
	ENDREPEAT
	
	IF index != -1
		

		INT iArrayIndex
		BOOL bCreated
		FMMC_MISSION_DETAILS_STRUCT missionDetails
		
		IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
		OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
				iArrayIndex = (pauseMenuRoute[3] - iPlaylistCreatedMission)//(iPauseMenuItemSelected - iRockstarCreatedCount)	//(iPauseMenuItemSelected - iPlaylistCreatedMission)
				bCreated = iArrayIndex < 0
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SAVE_SELECTED_MISSION_TO_PLAYLIST - iArrayIndex = ", iArrayIndex, ", iPlaylistCreatedMission = ", iPlaylistCreatedMission, ", pauseMenuRoute[3] = ", pauseMenuRoute[3])
				
				IF bCreated
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SAVE_SELECTED_MISSION_TO_PLAYLIST - bCreated is TRUE.")
					IF CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iMinPlayers, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iMaxPlayers, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iRank, DEFAULT, DEFAULT, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].tlName)
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SAVE_SELECTED_MISSION_TO_PLAYLIST - saving R* created mission to playlist")
					
						missionDetails.tlName 				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].tlName	//iPauseMenuItemSelected]
						missionDetails.tlOwner 				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].tlOwner
						missionDetails.tlMissionName 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].tlMissionName
						missionDetails.tlMissionDec	 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].tlMissionDec
						missionDetails.vStartPos 			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].vStartPos
						missionDetails.iType 				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iType
						missionDetails.iSubType 			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iSubType
						missionDetails.iMinPlayers 			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iMinPlayers
						missionDetails.iRank 				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iRank
						missionDetails.iMaxPlayers 			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iMaxPlayers
						missionDetails.iRating 				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iRating
						missionDetails.bInUse 				= TRUE
						missionDetails.iMaxNumberOfTeams	= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[pauseMenuRoute[3]].iMaxNumberOfTeams
						missionDetails.nCategory			= UGC_CATEGORY_ROCKSTAR_CREATED
						missionDetails.iAdversaryModeType   = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iAdversaryModeType
					ENDIF
				
				ELSE
					IF CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iType, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iMinPlayers, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iMaxPlayers, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iRank, DEFAULT, DEFAULT, g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].tlName)
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SAVE_SELECTED_MISSION_TO_PLAYLIST - saving R* Verified mission to playlist. pauseMenuRoute[3] = ", pauseMenuRoute[3], ", iArrayIndex = ", iArrayIndex)
					
						missionDetails.tlName 				= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].tlName
						missionDetails.tlOwner 				= g_FMMC_ROCKSTAR_VERIFIED.tlOwnerPretty[iArrayIndex] //g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].tlOwner
						missionDetails.tlMissionName 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].tlMissionName
						missionDetails.tlMissionDec	 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].tlMissionDec
						missionDetails.vStartPos 			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].vStartPos
						missionDetails.iType 				= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iType
						missionDetails.iSubType 			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iSubType
						missionDetails.iMinPlayers 			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iMinPlayers
						missionDetails.iRank 				= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iRank
						missionDetails.iMaxPlayers 			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iMaxPlayers
						missionDetails.iRating 				= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iRating
						missionDetails.bInUse 				= TRUE
						missionDetails.iMaxNumberOfTeams	= g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[iArrayIndex].iMaxNumberOfTeams
						missionDetails.nCategory			= UGC_CATEGORY_ROCKSTAR_VERIFIED
						missionDetails.iAdversaryModeType   = g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iAdversaryModeType
					ENDIF
				ENDIF
			
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
				IF CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iType, g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers, g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers, g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iRank, DEFAULT, DEFAULT, g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlName)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SAVE_SELECTED_MISSION_TO_PLAYLIST - saving my mission to playlist")		
					missionDetails.tlName 				= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlName
					missionDetails.tlOwner 				= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlOwner
					missionDetails.tlMissionName 		= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlMissionName
					missionDetails.tlMissionDec	 		= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlMissionDec
					missionDetails.vStartPos 			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].vStartPos
					missionDetails.iType 				= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iType
					missionDetails.iSubType 			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iSubType
					missionDetails.iMinPlayers 			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers
					missionDetails.iRank 				= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iRank
					missionDetails.iMaxPlayers 			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers
					missionDetails.iRating 				= 0
					missionDetails.bInUse 				= TRUE
					missionDetails.iMaxNumberOfTeams	= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxNumberOfTeams
					missionDetails.nCategory			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].nCategory
				    missionDetails.iAdversaryModeType   = g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iAdversaryModeType
				ENDIF
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
				IF CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iType, g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers, g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers, g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iRank, DEFAULT, DEFAULT, g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlName)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SAVE_SELECTED_MISSION_TO_PLAYLIST - saving downloaded mission to playlist")
					missionDetails.tlName 				= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlName
					missionDetails.tlOwner 				= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlOwner
					missionDetails.tlMissionName	 	= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlMissionName
					missionDetails.tlMissionDec		 	= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlMissionDec
					missionDetails.vStartPos 			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].vStartPos
					missionDetails.iType 				= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iType
					missionDetails.iSubType 			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iSubType
					missionDetails.iMinPlayers 			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers
					missionDetails.iRank 				= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iRank
					missionDetails.iMaxPlayers 			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers
					missionDetails.iRating 				= 0
					missionDetails.bInUse 				= TRUE
					missionDetails.iMaxNumberOfTeams	= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxNumberOfTeams
					missionDetails.nCategory			= g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].nCategory
					missionDetails.iAdversaryModeType   = g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iAdversaryModeType
				ENDIF
		ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
				IF CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iRank, DEFAULT, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iSubType, g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlName)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SAVE_SELECTED_MISSION_TO_PLAYLIST - saving recent mission to playlist")
					missionDetails.tlName 				= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlName
					missionDetails.tlOwner 				= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlOwner
					missionDetails.tlMissionName 		= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlMissionName
					missionDetails.tlMissionDec		 	= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlMissionDec
					missionDetails.vStartPos 			= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].vStartPos
					missionDetails.iType 				= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iType
					missionDetails.iSubType				= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iSubType
					missionDetails.iMinPlayers 			= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers
					missionDetails.iRank 				= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iRank
					missionDetails.iMaxPlayers 			= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers
					missionDetails.bInUse 				= TRUE
					missionDetails.iMaxNumberOfTeams	= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMaxNumberOfTeams
					missionDetails.nCategory			= g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].nCategory
				    missionDetails.iAdversaryModeType   = g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iAdversaryModeType
				ENDIF
		ENDIF
		
		IF missionDetails.bInUse
			g_sMenuPlayListDetails.tl31ContentIDs.szContentID[index] = missionDetails.tlName
			g_sMenuPlayListDetails.sLoadedMissionDetails[index] = missionDetails
		
		
			//ALSO DO FOR DELETE!!!
			UPDATE_PLAYLIST_NEW_MIN_AND_MAX(missionDetails.iMinPlayers, missionDetails.iMaxPlayers)
			PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
			CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU SAVE_SELECTED_MISSION_TO_PLAYLIST - bDoUpdateOnly = TRUE")
			bDoUpdateOnly = TRUE
			
			UPDATE_PLAYLIST_COLUMN()
			
			IF g_sMenuPlayListDetails.iLength = FMMC_MAX_PLAY_LIST_LENGTH
				PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
				CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU SAVE_SELECTED_MISSION_TO_PLAYLIST - bDoUpdateOnly = TRUE")
				bDoUpdateOnly = TRUE
			ENDIF
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			PAUSE_MENU_SET_WARN_ON_TAB_CHANGE(TRUE)

			SET_BIT(iBS_General, biWARN_ON_TAB_CHANGE_SET)
		ENDIF
	ELSE
		PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
		CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU SAVE_SELECTED_MISSION_TO_PLAYLIST - bDoUpdateOnly = TRUE")
		bDoUpdateOnly = TRUE
		//PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
ENDPROC

FUNC VECTOR GET_MISSION_AREA_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].vStartPos//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].vStartPos
			ENDIF
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].vStartPos
			
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].vStartPos
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].vStartPos

	ENDIF
	
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC INT GET_MISSION_RATING_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iRating//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iRating
			ENDIF						
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iRating
			
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iRating
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iRating
			
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC STRING GET_MISSION_CREATOR_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN PM_CONVERT_TL_TO_STRING(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].tlOwner)//iPauseMenuItemSelected])
			ELSE
				RETURN PM_CONVERT_TL_TO_STRING(g_FMMC_ROCKSTAR_VERIFIED.tlOwnerPretty[iArrayIndex])
			ENDIF
						
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN PM_CONVERT_TL_TO_STRING(g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlOwner)
			
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN PM_CONVERT_TL_TO_STRING(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlOwner)
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN PM_CONVERT_TL_TO_STRING(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlOwner)

	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_MISSION_NAME_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN PM_CONVERT_TL_TO_STRING(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].tlMissionName)//iPauseMenuItemSelected])
			ELSE
				RETURN PM_CONVERT_TL_TO_STRING(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].tlMissionName)
			ENDIF
						
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN PM_CONVERT_TL_TO_STRING(g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlMissionName)
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN PM_CONVERT_TL_TO_STRING(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlMissionName)
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_RECENT
			RETURN PM_CONVERT_TL_TO_STRING(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlMissionName)

	ENDIF
	
	RETURN ""
ENDFUNC

FUNC INT GET_MISSION_DESC_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - GET_MISSION_DESC_PM_STAT_PLAYLIST - iArrayIndex = ", iArrayIndex)
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iMissionDecHash//iPauseMenuItemSelected])
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iMissionDecHash
			ENDIF
						
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMissionDecHash
			
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMissionDecHash
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMissionDecHash

	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_MISSION_RANK_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iRank//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iRank
			ENDIF
						
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iRank
			
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iRank
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iRank

	ENDIF
	
	RETURN 1
ENDFUNC

FUNC INT GET_MISSION_MAX_TEAMS_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)

	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[pauseMenuRoute[3]].iMaxNumberOfTeams
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[iArrayIndex].iMaxNumberOfTeams
			ENDIF
						
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxNumberOfTeams
			
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxNumberOfTeams
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMaxNumberOfTeams

	ENDIF
	
	RETURN 1
ENDFUNC

FUNC BOOL GET_MISSION_CREATOR_USES_SC_NICK_PLAYLIST(BOOL bCreated, INT iArrayIndex)

	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN FALSE
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.bScNickName[iArrayIndex]
			ENDIF
						
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].bScNickName
			
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].bScNickName
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].bScNickName

	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MISSION_MIN_PLAYERS_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iMinPlayers//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iMinPlayers
			ENDIF
						
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers
			
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMinPlayers

	ENDIF
	
	RETURN 1
ENDFUNC

FUNC INT GET_MISSION_MAX_PLAYERS_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iMaxPlayers//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iMaxPlayers
			ENDIF
						
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers
			
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iMaxPlayers

	ENDIF
	
	RETURN 1
ENDFUNC

FUNC INT GET_MISSION_TYPE_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iType//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iType
			ENDIF
						
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iType
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iType
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iType

	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_MISSION_SUBTYPE_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iSubType//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iSubType
			ENDIF
						
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iSubType
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iSubType
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iSubType

	ENDIF
	
	RETURN 0
ENDFUNC


FUNC INT GET_MISSION_BITSET_TWO_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iBitSetTwo//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iBitSetTwo
			ENDIF
						
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iBitSetTwo
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iBitSetTwo
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iBitSetTwo

	ENDIF
	
	RETURN 0
ENDFUNC
FUNC INT GET_MISSION_BITSET_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iBitSet//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iBitSet
			ENDIF
						
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iBitSet
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iBitSet
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iBitSet

	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_MISSION_ADVERSARY_MODE_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].iAdversaryModeType//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].iAdversaryModeType
			ENDIF
						
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].iAdversaryModeType
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].iAdversaryModeType
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].iAdversaryModeType

	ENDIF
	
	RETURN 0
ENDFUNC


FUNC TEXT_LABEL_23 GET_MISSION_FILENAME_PM_STAT_PLAYLIST(BOOL bCreated, INT iArrayIndex)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[pauseMenuRoute[3]].tlName//iPauseMenuItemSelected]
			ELSE
				RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].tlName
			ENDIF
						
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MY_MISSION
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlName
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlName
		
	ELIF pauseMenuRoute[2] =  PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlName

	ENDIF
	
	TEXT_LABEL_23 tlEmpty
	RETURN tlEmpty
ENDFUNC

FUNC BOOL GET_MISSION_VERIFIED_PM_STAT_PLAYLIST(BOOL bCreated)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN FALSE
			ELSE
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_MISSION_VERIFIED_PM_STAT_PLAYLIST - g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayIndex].nCategory = UGC_CATEGORY_ROCKSTAR_VERIFIED")
				RETURN TRUE
			ENDIF
			
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].nCategory = UGC_CATEGORY_ROCKSTAR_VERIFIED
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].nCategory = UGC_CATEGORY_ROCKSTAR_VERIFIED
	
						
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MISSION_ROCKSTAR_CREATED_PM_STAT_PLAYLIST(BOOL bCreated)
	
	IF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DEATHMATCH
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_SURVIVAL
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_BASE_JUMP
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_MISSION
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_CTF
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_LTS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_TARGET_ASSAULT	
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_ARENA_WARS
	OR pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_KING_OF_THE_HILL
			IF bCreated
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_DOWNLOADED
			RETURN g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].nCategory = UGC_CATEGORY_ROCKSTAR_CREATED
		
	ELIF pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_RECENT
			RETURN g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].nCategory = UGC_CATEGORY_ROCKSTAR_CREATED
			
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DISPLAY_MISSION_DETAILS_FOR_PLAYLIST(BOOL bCanDisplay=TRUE)
	
	
	INT iArrayIndex = (pauseMenuRoute[3] - iPlaylistCreatedMission)	//(iPauseMenuItemSelected - iPlaylistCreatedMission)
	BOOL bCreated = iArrayIndex < 0
	
	
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] pauseMenuRoute[3] = ", pauseMenuRoute[3], " iPlaylistCreatedMission = ", iPlaylistCreatedMission, " iArrayIndex = ", iArrayIndex)
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_MISSION_DETAILS - G - DISPLAY_MISSION_DETAILS_FOR_PLAYLIST")
	IF bCanDisplay								
		IF REQUEST_AND_LOAD_CACHED_DESCRIPTION(GET_MISSION_DESC_PM_STAT_PLAYLIST(bCreated, iArrayIndex), sCMDLvars)								
		DISPLAY_MISSION_DETAILS(THIRD_COLUMN, CREATOR_STAT_ID, 
			GET_MISSION_AREA_PM_STAT_PLAYLIST(bCreated, iArrayIndex), GET_MISSION_RATING_PM_STAT_PLAYLIST(bCreated, iArrayIndex), 
			GET_MISSION_CREATOR_PM_STAT_PLAYLIST(bCreated, iArrayIndex), GET_MISSION_NAME_PM_STAT_PLAYLIST(bCreated, iArrayIndex), 
			UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(GET_MISSION_DESC_PM_STAT_PLAYLIST(bCreated, iArrayIndex), 500), 
			GET_MISSION_RANK_PM_STAT_PLAYLIST(bCreated, iArrayIndex), GET_MISSION_MIN_PLAYERS_PM_STAT_PLAYLIST(bCreated, iArrayIndex),
			GET_MISSION_MAX_PLAYERS_PM_STAT_PLAYLIST(bCreated, iArrayIndex), GET_MISSION_TYPE_PM_STAT_PLAYLIST(bCreated, iArrayIndex), GET_MISSION_SUBTYPE_PM_STAT_PLAYLIST(bCreated, iArrayIndex), GET_MISSION_BITSET_PM_STAT_PLAYLIST(bCreated, iArrayIndex),GET_MISSION_BITSET_TWO_PM_STAT_PLAYLIST(bCreated, iArrayIndex),
			GET_MISSION_VERIFIED_PM_STAT_PLAYLIST(bCreated), GET_MISSION_FILENAME_PM_STAT_PLAYLIST(bCreated, iArrayIndex), DLPhotoData, 
			HAS_MISSION_GOT_PHOTO(), GET_MISSION_ROCKSTAR_CREATED_PM_STAT_PLAYLIST(bCreated), GET_MISSION_MAX_TEAMS_PM_STAT_PLAYLIST(bCreated, iArrayIndex), GET_MISSION_CREATOR_USES_SC_NICK_PLAYLIST(bCreated, iArrayIndex),DEFAULT,DEFAULT, DEFAULT, GET_MISSION_ADVERSARY_MODE_PLAYLIST(bCreated, iArrayIndex))
			UGC_RELEASE_CACHED_DESCRIPTION(sCMDLvars.nHashOld)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: return true when the keybord has finished
/// PARAMS:
///    oskStatus - the current status
///    iCurrentstatus - control int
/// RETURNS:
///    
FUNC BOOL RUN_PLAYLIST_ON_SCREEN_KEYBOARD(OSK_STATUS &oskStatus, INT &iProfanityToken, INT &iCurrentstatus)
	STRING sTitle
	STRING strName
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
		RETURN FALSE
	ENDIF
	INT iTextLength = 15
	#IF IS_DEBUG_BUILD
		iTextLength = 30
	#ENDIF
	//Set up the keyyboard and then wait for it's return status
	SWITCH iCurrentstatus
		//Set up
		CASE 0
			
			IF USE_SHORT_OBJECTIVE_TEXT()
				iTextLength = 10
			ENDIF
			
			IF iProfanityToken = 0
				sTitle = "PM_NAME_PLIST"
			ELSE
				sTitle = "PM_NAME_PLISTF"
			ENDIF
			
			eKeyboardType keyType
			IF IS_PC_VERSION()
				keyType = ONSCREEN_KEYBOARD_FILENAME
			ELSE
	            IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH
	                keyType = ONSCREEN_KEYBOARD_FILENAME
	            ELSE
	                keyType = ONSCREEN_KEYBOARD_LOCALISED
	            ENDIF
			ENDIF
			
			IF sPublishPlaylistVars.bUpdate
				IF IS_PLAYSTATION_PLATFORM()
				OR IS_PC_VERSION()
					DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, "", g_sMenuPlayListDetails.tl31PlaylistName, "", "", "", iTextLength)
				ELSE
					DISPLAY_ONSCREEN_KEYBOARD(keyType, "FMMC_MPM_NA", sTitle, g_sMenuPlayListDetails.tl31PlaylistName, "", "", "", iTextLength)
				ENDIF
			ELSE
				IF IS_PLAYSTATION_PLATFORM()
				OR IS_PC_VERSION()
					DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, "", "", "", "", "", iTextLength)
				ELSE
					DISPLAY_ONSCREEN_KEYBOARD(keyType, "FMMC_MPM_NA", sTitle, "", "", "", "", iTextLength)
				ENDIF
			ENDIF
			
			
			iProfanityToken = 0
			iCurrentstatus = 1
			
			IF iTextLength > 15
				CPRINTLN(DEBUG_PAUSE_MENU, "RUN_ON_SCREEN_KEYBOARD iTextLength > 15, this is for DEV only.")
			ENDIF
			
		BREAK
		//Wait for return status
		CASE 1
		CASE 2
			oskStatus = UPDATE_ONSCREEN_KEYBOARD()
			SWITCH oskStatus
				//Still on screen
				CASE OSK_PENDING
					RETURN FALSE
					
				//Failed just return false and try again
				CASE OSK_FAILED
					CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_SAVE - iPlaylistKBStage reset 3")
					iCurrentstatus	= 0					
					oskStatus 		= OSK_PENDING
					RETURN FALSE
				
				//If the keyboard was canceled then return
				CASE OSK_CANCELLED
					CPRINTLN(DEBUG_PAUSE_MENU, "OSK_CANCELLED")
					RETURN FALSE					
				
				//Accepted the input
				CASE OSK_SUCCESS
					CPRINTLN(DEBUG_PAUSE_MENU, "RUN_ON_SCREEN_KEYBOARD")
					iCurrentstatus 		= OSK_STAGE_FAILED
					strName = GET_ONSCREEN_KEYBOARD_RESULT()
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					OR IS_STRING_NULL_OR_EMPTY(strName)
						iCurrentstatus	= OSK_STAGE_SET_UP					
						oskStatus 		= OSK_CANCELLED
						RETURN FALSE
					ENDIF
					
					IF !PAUSE_MENU_IS_CONTEXT_ACTIVE(HASH("SupressSelectPM"))
						PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SupressSelectPM"))
						CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - PAUSE_MENU_ACTIVATE_CONTEXT SupressSelectPM 2")
						PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
					ENDIF
					
					IF SC_PROFANITY_GET_CHECK_IS_VALID(iProfanityToken)
						IF NOT SC_PROFANITY_GET_CHECK_IS_PENDING(iProfanityToken)
							BUSYSPINNER_OFF()
							ePROFANITY_CHECK_STATUS status 
							status = SC_PROFANITY_GET_STRING_STATUS(iProfanityToken)
							SWITCH(status)
							    CASE PROFANITY_RESULT_OK
								  	CPRINTLN(DEBUG_PAUSE_MENU, "Profanity token ", iProfanityToken, " PASSED")
								  	iProfanityToken = 0
									RETURN TRUE
								BREAK
								CASE PROFANITY_RESULT_FAILED
								  CPRINTLN(DEBUG_PAUSE_MENU, "Profanity token ", iProfanityToken, " FAILED")
									iCurrentstatus	= OSK_STAGE_SET_UP					
									oskStatus 		= OSK_PENDING
									RETURN FALSE
								BREAK
								CASE PROFANITY_RESULT_ERROR
								  	CPRINTLN(DEBUG_PAUSE_MENU, "Profanity token ", iProfanityToken, " ERROR")
								  	//iCurrentstatus	= OSK_STAGE_SET_UP					
									oskStatus 		= OSK_PENDING
									SET_PLAYER_KICKED_TO_GO_OFFLINE_FROM_KEYBOARD(TRUE) //This is a sweeping change, only going into NG to give enough time to test. BC: 1839562 but this will happen throughout the game. 
									//added this for 2131642
									IF IS_PAUSE_MENU_ACTIVE()
										SET_FRONTEND_ACTIVE(FALSE)
										NET_NL()NET_PRINT("RUN_PLAYLIST_ON_SCREEN_KEYBOARD: PROFANITY_RESULT_ERROR: SET_FRONTEND_ACTIVE(FALSE)")
									ENDIF
									RETURN FALSE
                                BREAK
                            ENDSWITCH
							
//							IF SC_PROFANITY_GET_STRING_PASSED(iProfanityToken)
//								CPRINTLN(DEBUG_SYSTEM, "Profanity token ", iProfanityToken, " PASSED")
//								iProfanityToken = 0
//								RETURN TRUE
//							ELSE //Failed
//								CPRINTLN(DEBUG_SYSTEM, "Profanity token ", iProfanityToken, " FAILED")
//								iCurrentstatus	= 0					
//								oskStatus 		= OSK_PENDING
//								RETURN FALSE
//							ENDIF
							
							
						ELSE //Still pending
							CPRINTLN(DEBUG_PAUSE_MENU, "Profanity token ", iProfanityToken, " still pending")
							BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
                    		END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER))
						ENDIF
					ELSE
						CPRINTLN(DEBUG_PAUSE_MENU, "Profanity token ", iProfanityToken, " not yet valid.")
						SC_PROFANITY_CHECK_STRING_UGC(GET_ONSCREEN_KEYBOARD_RESULT(), iProfanityToken)
						BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("ERROR_CHECKPROFANITY")
						END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER)) 
					ENDIF
				BREAK
					
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL GET_CREATED_PLAYLIST_NAME_FROM_KEYBOARD(OSK_STATUS& Playlist_oskStatus, INT &iProfanityToken, INT& iKeyboardStage, BOOL& QuitOut, TEXT_LABEL_31 &strName)

	IF RUN_PLAYLIST_ON_SCREEN_KEYBOARD(Playlist_oskStatus, iProfanityToken, iKeyboardStage)
		iKeyboardStage = 0
		strName = GET_ONSCREEN_KEYBOARD_RESULT()
		
		IF IS_STRING_NULL_OR_EMPTY(strName) // Added below because this doesn't work.
		OR ARE_STRINGS_EQUAL("                              ", strName)
		OR ARE_STRINGS_EQUAL("                             ", strName)
		OR ARE_STRINGS_EQUAL("                            ", strName)
		OR ARE_STRINGS_EQUAL("                           ", strName)
		OR ARE_STRINGS_EQUAL("                          ", strName)
		OR ARE_STRINGS_EQUAL("                         ", strName)
		OR ARE_STRINGS_EQUAL("                        ", strName)
		OR ARE_STRINGS_EQUAL("                       ", strName)
		OR ARE_STRINGS_EQUAL("                      ", strName)
		OR ARE_STRINGS_EQUAL("                     ", strName)
		OR ARE_STRINGS_EQUAL("                    ", strName)
		OR ARE_STRINGS_EQUAL("                   ", strName)
		OR ARE_STRINGS_EQUAL("                  ", strName)
		OR ARE_STRINGS_EQUAL("                 ", strName)
		OR ARE_STRINGS_EQUAL("                ", strName)
		OR ARE_STRINGS_EQUAL("               ", strName)
		OR ARE_STRINGS_EQUAL("              ", strName)
		OR ARE_STRINGS_EQUAL("             ", strName)
		OR ARE_STRINGS_EQUAL("            ", strName)
		OR ARE_STRINGS_EQUAL("           ", strName)
		OR ARE_STRINGS_EQUAL("          ", strName)
		OR ARE_STRINGS_EQUAL("         ", strName)
		OR ARE_STRINGS_EQUAL("        ", strName)
		OR ARE_STRINGS_EQUAL("       ", strName)
		OR ARE_STRINGS_EQUAL("      ", strName)
		OR ARE_STRINGS_EQUAL("     ", strName)
		OR ARE_STRINGS_EQUAL("    ", strName)
		OR ARE_STRINGS_EQUAL("   ", strName)
		OR ARE_STRINGS_EQUAL("  ", strName)
		OR ARE_STRINGS_EQUAL(" ", strName)
		OR ARE_STRINGS_EQUAL("", strName)
			QuitOut = TRUE
			iKeyboardStage = -1
			Playlist_oskStatus = OSK_PENDING
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_PAUSE_MENU, "Name is Empty - playlistOSKStatus = OSK_PENDING")
			#ENDIF
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		NET_PRINT("playlist name accepted as [")
		NET_PRINT(strName)NET_PRINT("]")
		NET_PRINT("playlistOSKStatus = OSK_SUCCESS")
		#ENDIF
		iKeyboardStage = 0
		Playlist_oskStatus = OSK_PENDING
		RETURN TRUE
	ENDIF
	IF Playlist_oskStatus = OSK_CANCELLED

		QuitOut = TRUE
		//iKeyboardStage = 0
		Playlist_oskStatus = OSK_PENDING
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_PAUSE_MENU, "playlistOSKStatus = OSK_CANCELLED")
		#ENDIF
		
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL RUN_CHALLENGE_ON_SCREEN_KEYBOARD(OSK_STATUS &oskStatus, INT &iProfanityToken, INT &iCurrentstatus)
	STRING sTitle
	STRING strName
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
		RETURN FALSE
	ENDIF
	
	INT iTextLength = 15
	
	//Set up the keyyboard and then wait for it's return status
	SWITCH iCurrentstatus
		//Set up
		CASE 0
			
			IF USE_SHORT_OBJECTIVE_TEXT()
				iTextLength = 10
			ENDIF
			
			IF iProfanityToken = 0
				sTitle = "PM_NAME_CHALL"
			ELSE
				sTitle = "PM_NAME_CHALLF"
			ENDIF
			
			eKeyboardType keyType
            IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH
                keyType = ONSCREEN_KEYBOARD_ENGLISH
            ELSE
                keyType = ONSCREEN_KEYBOARD_LOCALISED
            ENDIF
			
			IF IS_PC_VERSION()
			OR IS_PLAYSTATION_PLATFORM()
				DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, "", "", "", "", "", iTextLength)	
			ELSE
				DISPLAY_ONSCREEN_KEYBOARD(keyType, "FMMC_MPM_NA", sTitle, "", "", "", "", iTextLength)	
			ENDIF
			
			
			iProfanityToken = 0
			iCurrentstatus = 1
			
			
		BREAK
		//Wait for return status
		CASE 1
			oskStatus = UPDATE_ONSCREEN_KEYBOARD()
			SWITCH oskStatus
				//Still on screen
				CASE OSK_PENDING
					RETURN FALSE
					
				//Failed just return false and try again
				CASE OSK_FAILED
					iCurrentstatus	= 0					
					oskStatus 		= OSK_PENDING
					RETURN FALSE
				
				//If the keyboard was canceled then return
				CASE OSK_CANCELLED
				
					CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - OSK_CANCELLED")
					RETURN FALSE					
				
				//Accepted the input
				CASE OSK_SUCCESS
					strName = GET_ONSCREEN_KEYBOARD_RESULT()
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					OR IS_STRING_NULL_OR_EMPTY(strName)
						iCurrentstatus	= OSK_STAGE_SET_UP					
						oskStatus 		= OSK_CANCELLED
						RETURN FALSE
					ENDIF
					CPRINTLN(DEBUG_PAUSE_MENU, "RUN_ON_SCREEN_KEYBOARD")
					IF SC_PROFANITY_GET_CHECK_IS_VALID(iProfanityToken)
						IF NOT SC_PROFANITY_GET_CHECK_IS_PENDING(iProfanityToken)
							ePROFANITY_CHECK_STATUS status 
							BUSYSPINNER_OFF()
							status = SC_PROFANITY_GET_STRING_STATUS(iProfanityToken)
							SWITCH(status)
							    CASE PROFANITY_RESULT_OK
								  	CPRINTLN(DEBUG_SYSTEM, "Profanity token ", iProfanityToken, " PASSED")
								  	iProfanityToken = 0
									RETURN TRUE
								BREAK
								CASE PROFANITY_RESULT_FAILED
								  CPRINTLN(DEBUG_SYSTEM, "Profanity token ", iProfanityToken, " FAILED")
									iCurrentstatus	= OSK_STAGE_SET_UP					
									oskStatus 		= OSK_PENDING
									RETURN FALSE
								BREAK
								CASE PROFANITY_RESULT_ERROR
								  	CPRINTLN(DEBUG_SYSTEM, "Profanity token ", iProfanityToken, " ERROR")
								  	iCurrentstatus	= OSK_STAGE_SET_UP					
									oskStatus 		= OSK_PENDING
									SET_PLAYER_KICKED_TO_GO_OFFLINE_FROM_KEYBOARD(TRUE) //This is a sweeping change, only going into NG to give enough time to test. BC: 1839562 but this will happen throughout the game. 
									//added this for 2131642
									IF IS_PAUSE_MENU_ACTIVE()
										SET_FRONTEND_ACTIVE(FALSE)
										NET_NL()NET_PRINT("RUN_CHALLENGE_ON_SCREEN_KEYBOARD: PROFANITY_RESULT_ERROR: SET_FRONTEND_ACTIVE(FALSE)")
									ENDIF
									RETURN FALSE
								BREAK
						   	ENDSWITCH
						ELSE //Still pending
							CPRINTLN(DEBUG_SYSTEM, "Profanity token ", iProfanityToken, " still pending")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_SYSTEM, "Profanity token ", iProfanityToken, " not longer valid")
						SC_PROFANITY_CHECK_STRING_UGC(GET_ONSCREEN_KEYBOARD_RESULT(), iProfanityToken)
						BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("ERROR_CHECKPROFANITY")
						END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER)) 
					ENDIF
				BREAK
					
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL GET_CHALLENGE_NAME_FROM_KEYBOARD(OSK_STATUS& Playlist_oskStatus, INT &iProfanityToken, INT& iKeyboardStage, BOOL& QuitOut, TEXT_LABEL_31 &strName)
	CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - GET_CHALLENGE_NAME_FROM_KEYBOARD")
	IF RUN_CHALLENGE_ON_SCREEN_KEYBOARD(Playlist_oskStatus, iProfanityToken, iKeyboardStage)
		iKeyboardStage = 0
		strName = GET_ONSCREEN_KEYBOARD_RESULT()
		
		IF IS_STRING_NULL_OR_EMPTY(strName)
		OR ARE_STRINGS_EQUAL("                              ", strName)
		OR ARE_STRINGS_EQUAL("                             ", strName)
		OR ARE_STRINGS_EQUAL("                            ", strName)
		OR ARE_STRINGS_EQUAL("                           ", strName)
		OR ARE_STRINGS_EQUAL("                          ", strName)
		OR ARE_STRINGS_EQUAL("                         ", strName)
		OR ARE_STRINGS_EQUAL("                        ", strName)
		OR ARE_STRINGS_EQUAL("                       ", strName)
		OR ARE_STRINGS_EQUAL("                      ", strName)
		OR ARE_STRINGS_EQUAL("                     ", strName)
		OR ARE_STRINGS_EQUAL("                    ", strName)
		OR ARE_STRINGS_EQUAL("                   ", strName)
		OR ARE_STRINGS_EQUAL("                  ", strName)
		OR ARE_STRINGS_EQUAL("                 ", strName)
		OR ARE_STRINGS_EQUAL("                ", strName)
		OR ARE_STRINGS_EQUAL("               ", strName)
		OR ARE_STRINGS_EQUAL("              ", strName)
		OR ARE_STRINGS_EQUAL("             ", strName)
		OR ARE_STRINGS_EQUAL("            ", strName)
		OR ARE_STRINGS_EQUAL("           ", strName)
		OR ARE_STRINGS_EQUAL("          ", strName)
		OR ARE_STRINGS_EQUAL("         ", strName)
		OR ARE_STRINGS_EQUAL("        ", strName)
		OR ARE_STRINGS_EQUAL("       ", strName)
		OR ARE_STRINGS_EQUAL("      ", strName)
		OR ARE_STRINGS_EQUAL("     ", strName)
		OR ARE_STRINGS_EQUAL("    ", strName)
		OR ARE_STRINGS_EQUAL("   ", strName)
		OR ARE_STRINGS_EQUAL("  ", strName)
		OR ARE_STRINGS_EQUAL(" ", strName)
		OR ARE_STRINGS_EQUAL("", strName)
		
			QuitOut = TRUE
			iKeyboardStage = -1
			Playlist_oskStatus = OSK_PENDING
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_PAUSE_MENU, "Name is Empty - playlistOSKStatus = OSK_CANCELLED")
			#ENDIF
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		NET_PRINT("playlist name accepted as [")
		NET_PRINT(strName)NET_PRINT("]")
		NET_PRINT("playlistOSKStatus = OSK_SUCCESS")
		#ENDIF

		iKeyboardStage = 0
		Playlist_oskStatus = OSK_PENDING
		RETURN TRUE
	ENDIF
	IF Playlist_oskStatus = OSK_CANCELLED

		QuitOut = TRUE
		iKeyboardStage = 0
		Playlist_oskStatus = OSK_PENDING
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - playlistOSKStatus = OSK_CANCELLED")
		#ENDIF
		
	ENDIF
	RETURN FALSE

ENDFUNC

PROC PAUSE_MENU_CLEAR_CREATED_PLAYLIST()
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PAUSE_MENU_CLEAR_CREATED_PLAYLIST - clearing playlist created struct")

	INT i
	REPEAT CREATED_PLAYLIST_LENGTH i
		g_sMenuPlayListDetails.sLoadedMissionDetails[i].bInUse = FALSE
	ENDREPEAT
	
	g_sMenuPlayListDetails.tl31PlaylistName = ""
	
	PAUSE_MENU_SET_WARN_ON_TAB_CHANGE(FALSE)
	CLEAR_BIT(iBS_General, biWARN_ON_TAB_CHANGE_SET)
	
	CLEAR_PM_COLUMN(SECOND_COLUMN)
	CLEAR_PM_COLUMN(THIRD_COLUMN)
ENDPROC

PROC SWAP_ITEMS(INT iOld, INT iNew)
	FMMC_MISSION_DETAILS_STRUCT storedMissionDetails 	= g_sMenuPlayListDetails.sLoadedMissionDetails[iOld]
	g_sMenuPlayListDetails.sLoadedMissionDetails[iOld] 	= g_sMenuPlayListDetails.sLoadedMissionDetails[iNew]
	g_sMenuPlayListDetails.sLoadedMissionDetails[iNew] 	= storedMissionDetails
	
	//Do the same with the content ID
	TEXT_LABEL_23 szContentID									= g_sMenuPlayListDetails.tl31ContentIDs.szContentID[iOld]
	g_sMenuPlayListDetails.tl31ContentIDs.szContentID[iOld]  	= g_sMenuPlayListDetails.tl31ContentIDs.szContentID[iNew]
	g_sMenuPlayListDetails.tl31ContentIDs.szContentID[iNew] 	= szContentID
ENDPROC

//PURPOSE: Controls editing the currently selected playlist.
PROC PROCESS_PAUSE_MENU_PLAYLIST_EDIT()
	INT iOldIndex
	IF bDisplayEditingPlaylistMissionDetails
		IF iPauseMenuItemSelected < 16
			IF REQUEST_AND_LOAD_CACHED_DESCRIPTION(g_sMenuPlayListDetails.sLoadedMissionDetails[iPauseMenuItemSelected].iMissionDecHash, sCMDLvars)
				DISPLAY_MISSION_DETAILS(THIRD_COLUMN, CREATOR_STAT_ID, 
							GET_MISSION_AREA_PM_STAT(), GET_MISSION_RATING_PM_STAT(), 
							GET_MISSION_CREATOR_PM_STAT(), GET_MISSION_NAME_PM_STAT(), UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_sMenuPlayListDetails.sLoadedMissionDetails[iPauseMenuItemSelected].iMissionDecHash, 500), 
							GET_MISSION_RANK_PM_STAT(), GET_MISSION_MIN_PLAYERS_PM_STAT(),
							GET_MISSION_MAX_PLAYERS_PM_STAT(), GET_MISSION_TYPE_PM_STAT(), GET_MISSION_SUBTYPE_PM_STAT(), GET_MISSION_BITSET_PM_STAT(), GET_MISSION_BITSET_TWO_PM_STAT(), GET_MISSION_VERIFIED_PM_STAT(), GET_MISSION_FILENAME_PM_STAT(), DLPhotoData, 
							TRUE, GET_MISSION_ROCKSTAR_CREATED_PM_STAT(), GET_MISSION_MAX_TEAMS_PM_STAT(),DEFAULT, DEFAULT,DEFAULT, DEFAULT, GET_MISSION_ADVERSARY_MODE_TYPE())
				bDisplayEditingPlaylistMissionDetails = FALSE
			ENDIF
		ENDIF
	ENDIF
	//INITIALISE	- //DeletePlaylist	//EditPlaylistPick	//EditPlaylistDrop	 //EditPlaylist
	IF NOT IS_BIT_SET(iBS_General, biEDIT_PLAYLIST_INIT)
//		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("StartHeadToHead"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylistDrop"))
		PAUSE_MENU_ACTIVATE_CONTEXT(HASH("EditPlaylistPick"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
		PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DeletePlaylist"))
		PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		
		IF IS_BIT_SET(iBS_General, biSHOW_SIDE_COLUMN_WARNING)
			MP_SET_COLUMN_WARNING_MESSAGE(FALSE, SECOND_COLUMN, 2, "PM_SOC_PLY", "PM_SOC_SGN", 0, "", "")
			SHOW_PM_COLUMN(FIRST_COLUMN)
			SHOW_PM_COLUMN(SECOND_COLUMN)
			SHOW_PM_COLUMN(THIRD_COLUMN)
			CLEAR_BIT(iBS_General, biSHOW_COLUMN_WARNING)
			CLEAR_BIT(iBS_General, biSHOW_SIDE_COLUMN_WARNING)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - MP_SET_COLUMN_WARNING_MESSAGE - Cancel")
		ENDIF
		
		iEditIndex = 0
		iStoredIndex = iPauseMenuItemSelected
		//iStoredHighlight = iPauseMenuItemSelected
		iPauseMenuItemSelected = iEditIndex
		
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DISPLAY_MISSION_DETAILS - A")
		bDisplayEditingPlaylistMissionDetails = TRUE				
		CLEAR_PM_COLUMN(FIRST_COLUMN)
		UPDATE_PLAYLIST_COLUMN(FIRST_COLUMN)
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SET_PM_HIGHLIGHT - I")
		SET_PM_HIGHLIGHT(FIRST_COLUMN, iEditIndex)
		
		HIDE_PAUSE_MENU_HELP_TEXT()
		HIDE_PM_COLUMN(SECOND_COLUMN)
		//HIDE_PM_COLUMN(THIRD_COLUMN)
		PAUSE_MENU_UPDATE_COLUMN(THIRD_COLUMN)
		
		PAUSE_MENU_SET_WARN_ON_TAB_CHANGE(TRUE)
		SET_BIT(iBS_General, biWARN_ON_TAB_CHANGE_SET)

		SET_BIT(iBS_General, biEDIT_PLAYLIST_INIT)
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - biEDIT_PLAYLIST_INIT - A")
	ENDIF
	
	// if the warning screen is up, just bail outright
	IF IS_BIT_SET(iBS_General, biWARN_ON_TAB_CHANGE_SET) AND IS_WARNING_MESSAGE_ACTIVE()
		EXIT
	ENDIF
	
	//Check for Down		 - Do Highlight - SET_PM_HIGHLIGHT()
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) OR IS_ANALOGUE_STICK_MOVED(INPUT_FRONTEND_DOWN, MPGlobalsHud.iDelayInt, FALSE)
		IF HAS_NET_TIMER_EXPIRED(iMenuMoveTimer, iMenuMoveTimerDelay)
			IF g_sMenuPlayListDetails.iLength > 1
		
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				iOldIndex = iEditIndex
				BOOL bWrapped = FALSE
				IF iEditIndex < (g_sMenuPlayListDetails.iLength-1)
					iEditIndex++
				ELSE
					iEditIndex = 0
					bWrapped = TRUE
				ENDIF
				
				//Check if should move Mission (i.e. it has been picked up)
				IF IS_BIT_SET(iBS_General, biEDIT_PLAYLIST_PICK)
					IF bWrapped
						INT iItem
					 	// from the end of the list to the second-to-last item (because inclusive FOR)
						FOR iItem = g_sMenuPlayListDetails.iLength-1 TO 1 STEP -1
							// bubble them all up
							SWAP_ITEMS(iItem, iItem-1)
						ENDFOR
					ELSE
						SWAP_ITEMS(iOldIndex, iEditIndex)
					ENDIF
					
				
					CLEAR_PM_COLUMN(FIRST_COLUMN)
					UPDATE_PLAYLIST_COLUMN(FIRST_COLUMN, iEditIndex)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - MOVED MISSION - DOWN")
				ENDIF
				
				iPauseMenuItemSelected = iEditIndex
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DISPLAY_MISSION_DETAILS - B")
				bDisplayEditingPlaylistMissionDetails = TRUE
							
				PAUSE_MENU_UPDATE_COLUMN(THIRD_COLUMN)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SET_PM_HIGHLIGHT - J")
				SET_PM_HIGHLIGHT(FIRST_COLUMN, iEditIndex, FALSE, TRUE)
				
				iMenuMoveTimerDelay = MENU_MOVE_TIME
				RESET_NET_TIMER(iMenuMoveTimer)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - MOVED HIGHLIGHT - DOWN - ", iEditIndex)
			ENDIF
		ENDIF
		
	//Check for Up 	
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) OR IS_ANALOGUE_STICK_MOVED(INPUT_FRONTEND_UP, MPGlobalsHud.iDelayInt, FALSE)
		IF HAS_NET_TIMER_EXPIRED(iMenuMoveTimer, iMenuMoveTimerDelay)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			iOldIndex = iEditIndex
			BOOL bWrapped = FALSE
			IF iEditIndex > 0
				iEditIndex--
			ELSE
				iEditIndex = (g_sMenuPlayListDetails.iLength-1)
				bWrapped = TRUE
			ENDIF
			
			//Check if should move Mission (i.e. it has been picked up)
			IF IS_BIT_SET(iBS_General, biEDIT_PLAYLIST_PICK)
				IF bWrapped
					INT iItem
				 	// from the first item on the list to the second-to-last (because +1)
					FOR iItem = 0 TO g_sMenuPlayListDetails.iLength-2
						// bubble them all down
						SWAP_ITEMS(iItem, iItem+1)
					ENDFOR
				ELSE
					SWAP_ITEMS(iOldIndex, iEditIndex)
				ENDIF
				
				
								
				CLEAR_PM_COLUMN(FIRST_COLUMN)
				UPDATE_PLAYLIST_COLUMN(FIRST_COLUMN, iEditIndex)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - MOVED MISSION - UP - ")
			ENDIF
			
			iPauseMenuItemSelected = iEditIndex
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DISPLAY_MISSION_DETAILS - V")
			bDisplayEditingPlaylistMissionDetails = TRUE
						
			PAUSE_MENU_UPDATE_COLUMN(THIRD_COLUMN)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SET_PM_HIGHLIGHT - K")
			SET_PM_HIGHLIGHT(FIRST_COLUMN, iEditIndex, FALSE, TRUE)
			
			iMenuMoveTimerDelay = MENU_MOVE_TIME
			RESET_NET_TIMER(iMenuMoveTimer)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - MOVED HIGHLIGHT - UP ", iEditIndex)
		ENDIF
		
		
		
	//Check for Pick/Drop
	ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			IF NOT IS_BIT_SET(iBS_General, biEDIT_PLAYLIST_PICK)
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				//STORE PICKED UP MISSION DATA??
				
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylistPick"))
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("EditPlaylistDrop"))
				PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
				
				SET_BIT(iBS_General, biEDIT_PLAYLIST_PICK)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - PICK")
				
				CLEAR_PM_COLUMN(FIRST_COLUMN)
				UPDATE_PLAYLIST_COLUMN(FIRST_COLUMN, iEditIndex)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SET_PM_HIGHLIGHT - L")
				SET_PM_HIGHLIGHT(FIRST_COLUMN, iEditIndex)
				SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				//CLEAR PICKED UP MISSION DATA??
				
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylistDrop"))
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("EditPlaylistPick"))
				PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
				
				CLEAR_BIT(iBS_General, biEDIT_PLAYLIST_PICK)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DROP")
				
				CLEAR_PM_COLUMN(FIRST_COLUMN)
				UPDATE_PLAYLIST_COLUMN(FIRST_COLUMN)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SET_PM_HIGHLIGHT - M")
				SET_PM_HIGHLIGHT(FIRST_COLUMN, iEditIndex)
				SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			ENDIF
		ELSE
			IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)")
				CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			ENDIF
		ENDIF
	
	//Check for Delete
	ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)
			PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DELETE PRESSED - CHECK A") NET_PRINT_TIME() NET_NL()
			
			//LOOP THROUGH LOWER MISSIONS - MOVING THEM ALL UP ONE - REDUCE TOTAL MISSIONS BY 1
			//iOldIndex = iEditIndex
			
			FMMC_MISSION_DETAILS_STRUCT emptyData
					
			//Check if should move Mission (i.e. it has been picked up)
			BOOL bLastItem = TRUE
			INT i = iEditIndex
			FOR i = iEditIndex TO (g_sMenuPlayListDetails.iLength-1)
				IF i < (g_sMenuPlayListDetails.iLength-1)
					g_sMenuPlayListDetails.sLoadedMissionDetails[i] = g_sMenuPlayListDetails.sLoadedMissionDetails[i+1]
					g_sMenuPlayListDetails.tl31ContentIDs.szContentID[i] = g_sMenuPlayListDetails.sLoadedMissionDetails[i+1].tlName
					bLastItem = FALSE
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DELETE - MOVE DOWN MISSION ", i)
				ELSE
					g_sMenuPlayListDetails.sLoadedMissionDetails[i] = emptyData
					g_sMenuPlayListDetails.tl31ContentIDs.szContentID[i] = ""
					g_sMenuPlayListDetails.iLength--
					IF bLastItem = TRUE
					AND iEditIndex > 0
						iEditIndex--
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - bLastItem = TRUE")
					ENDIF
					
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DELETE LAST MISSION ON LIST ", i)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - g_sMenuPlayListDetails.iLength = ", g_sMenuPlayListDetails.iLength)
				ENDIF
			ENDFOR
			
			IF g_sMenuPlayListDetails.iLength = 0
				PAUSE_MENU_SET_WARN_ON_TAB_CHANGE(FALSE)
				CLEAR_BIT(iBS_General, biWARN_ON_TAB_CHANGE_SET)
			ENDIF
			
			//Make sure Playlist Min and Max is okay
			RESET_PLAYLIST_MIN_AND_MAX()
			FOR i = 0 TO (g_sMenuPlayListDetails.iLength-1)
				UPDATE_PLAYLIST_NEW_MIN_AND_MAX(g_sMenuPlayListDetails.sLoadedMissionDetails[i].iMinPlayers, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iMaxPlayers)
			ENDFOR
			
			iPauseMenuItemSelected = iEditIndex
			
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylistDrop"))
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("EditPlaylistPick"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
			
			CLEAR_BIT(iBS_General, biEDIT_PLAYLIST_PICK)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DROP")
			
			CLEAR_PM_COLUMN(FIRST_COLUMN)
			UPDATE_PLAYLIST_COLUMN(FIRST_COLUMN, iEditIndex)
			
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] SET_PM_HIGHLIGHT - N")
			SET_PM_HIGHLIGHT(FIRST_COLUMN, iEditIndex)
			PAUSE_MENU_UPDATE_COLUMN(THIRD_COLUMN)
			
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DISPLAY_MISSION_DETAILS - D")
			bDisplayEditingPlaylistMissionDetails = TRUE
			
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DELETE - DONE")
			SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)
		ELSE
			IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)")
				CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)")
			CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
		ENDIF
		IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)")
			CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)
		ENDIF
	ENDIF
	
	//Check for Back
	BOOL bVerified, bCreated
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR g_sMenuPlayListDetails.iLength = 0
		PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		//iStoredHighlight = iStoredHighlight
		//iPauseMenuItemSelected = iStoredIndex
		//SET_PM_HIGHLIGHT(FIRST_COLUMN, iStoredHighlight, FALSE, TRUE)
		
		CLEAR_BIT(iBS_General, biEDIT_PLAYLIST_PICK)
		
		PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
		IF g_sMenuPlayListDetails.iLength > 0
			CLEAR_PM_COLUMN(SECOND_COLUMN)
						
			PM_SET_COLUMN_TITLE_ONLY(SECOND_COLUMN, "PM_TPLAYL", "")
			
			INT i
			FOR i = 0 TO (g_sMenuPlayListDetails.iLength-1)			
				IF NOT IS_STRING_NULL_OR_EMPTY(g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - displaying your playlist in Second Column - B")
					CPRINTLN(DEBUG_PAUSE_MENU, "thisPlaylistData.sMissionNames[", pauseMenuRoute[2], "].tl31MissionName[", i, "] = ", g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName, ", GET_MISSION_VERIFIED_PM_STAT(i) = ", GET_MISSION_VERIFIED_PM_STAT(i), ", GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i) = ", GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i))
					// How do I check if it's verified?
					IF GET_MISSION_VERIFIED_PM_STAT(i)
						bVerified = TRUE
					ELSE
						bVerified = FALSE
					ENDIF
					IF GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i)
						bCreated = TRUE
					ELSE
						bCreated = FALSE
					ENDIF
					MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF SECOND_COLUMN, i, CREATOR_MENU_ID, i, TRUE, g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName, bCreated, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iType, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iSubType, IS_BIT_SET(g_sMenuPlayListDetails.sLoadedMissionDetails[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), bVerified, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iAdversaryModeType) // First i, may need to be a count. Whereas second i should be index in array
				ENDIF
			ENDFOR	
			
			PM_DISPLAY_DATA_SLOT(SECOND_COLUMN)
			UPDATE_PLAYLIST_COLUMN(SECOND_COLUMN)
			PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
		ENDIF
		
		pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_CREATE_PLAYLIST) //PM_D2_PLAYLISTS
		pauseMenuRoute[2] = PM_D3_PLAYLIST_ADD_STUNT_RACE//PM_D3_PLAYLIST_EDIT_PLAYLIST
		iPauseMenuDepth = 2
		iPauseMenuItemStored = PM_D3_PLAYLIST_EDIT_PLAYLIST
		
		IF iPauseMenuDepth = 3
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - DISPLAY_MISSION_DETAILS - E")
			bDisplayEditingPlaylistMissionDetails = TRUE
							
			PAUSE_MENU_UPDATE_COLUMN(THIRD_COLUMN)
		ELSE
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - CLEAR MISSION DETAILS - C")
			CLEAR_PM_COLUMN(THIRD_COLUMN)
			HIDE_PM_COLUMN(THIRD_COLUMN)
			PAUSE_MENU_COLUMN_UPDATED(THIRD_COLUMN)
		ENDIF
		
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Z5 - SET iPauseMenuItemSelected to ", iPauseMenuItemStored, " from ", iPauseMenuItemSelected)			
		iPauseMenuItemSelected = iPauseMenuItemStored
		
		CLEANUP_TEMP_PASSIVE_MODE()
		CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] CLEANUP_TEMP_PASSIVE_MODE()")
		
		SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
		
		//Clear Instructional Buttons
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylistPick"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylistDrop"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
		
		IF pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_CREATE_PLAYLIST)
			IF iPauseMenuDepth = 3
				IF IS_GAME_LINKED_TO_SOCIAL_CLUB()
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("EditPlaylist"))
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] EditPlaylist - CHECK C")
				ENDIF
			ENDIF
		ENDIF

		PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		
		iMenuMoveTimerDelay = -1
		
		CLEAR_BIT(iBS_General, biEDIT_PLAYLIST_INIT)
		CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
		CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_DELETE)
		SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
		
		PAUSE_MENU_SET_WARN_ON_TAB_CHANGE(FALSE)
		CLEAR_BIT(iBS_General, biWARN_ON_TAB_CHANGE_SET)

		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_EDIT - BACK")
	ENDIF
	IF IS_CLOUD_DOWN_CLOUD_LOADER()
		SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_CLOUD_WARNING_MESSAGE)
	ENDIF
ENDPROC

PROC RESET_PUBLISH_PLAYLIST_VARS()
	sPublishPlaylistVars.iSwitchingInt = 0
	sPublishPlaylistVars.iPublishStage = 0
	sPublishPlaylistVars.bPublish = FALSE
	sPublishPlaylistVars.iButtonBitSetPassed = 0
	sPublishPlaylistVars.tl23FileAndPath = ""
	szFilePaths.tl63Path[0] = ""
	szFilePaths.tl63Path[1] = ""
ENDPROC

BOOL bStoredQuit
PROC PROCESS_PAUSE_MENU_PLAYLIST_SAVE()

	SWITCH iPlaylistSavingState
	
		CASE PLAYLIST_SAVE_NAME
			TEXT_LABEL_31 strPlaylistName
			BOOL bQuit
			
			IF GET_CREATED_PLAYLIST_NAME_FROM_KEYBOARD(playlistOSKStatus, iPlaylistProfanityToken, iPlaylistKBStage, bQuit, strPlaylistName)
				
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_SAVE - Saving playlist with name: ", strPlaylistName)
				
				INT i
				INT iMissions
				REPEAT CREATED_PLAYLIST_LENGTH i
					IF g_sMenuPlayListDetails.sLoadedMissionDetails[i].bInUse
						iMissions++
						CPRINTLN(DEBUG_PAUSE_MENU, "	Mission ", i, " : ", g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName)
					ENDIF
				ENDREPEAT

				//STRUCT_CLOUD_ROCKSTAR_CREATED_DATA emptyData				
				//pmRockstarCreatedData = emptyData
				
				g_sMenuPlayListDetails.tl31PlaylistName = strPlaylistName
				g_sMenuPlayListDetails.iLength = iMissions

				iPlaylistSavingState++
				//IF sPublishPlaylistVars.bUpdate 
				//	sPublishPlaylistVars.tl23FileAndPath = g_sMenuPlayListDetails.tl31szContentID
				//ELSE
				//	sPublishPlaylistVars.tl23FileAndPath = ""
				//ENDIF
				CPRINTLN(DEBUG_PAUSE_MENU, "sPublishPlaylistVars.tl23FileAndPath = ", sPublishPlaylistVars.tl23FileAndPath)
			ELSE
				//CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_SAVE - iPlaylistKBStage = ", iPlaylistKBStage)
				IF bQuit
				OR bStoredQuit
					IF iPlaylistKBStage = -1
						bStoredQuit = TRUE
						
						SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", "PM_PL_NM_AL", FE_WARNING_OK)
						IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
							IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
								iPlaylistKBStage = 0
								bStoredQuit = FALSE
							ENDIF
						ENDIF
						
					ENDIF
					IF iPlaylistKBStage = OSK_STAGE_FAILED
						bStoredQuit = TRUE
						
						SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", "PM_PL_NM_FL", FE_WARNING_OK)
						IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
							IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
								iPlaylistKBStage = 0
								bStoredQuit = FALSE
							ENDIF
						ENDIF
						
					ENDIF
					IF NOT bStoredQuit
						iPlaylistKBStage = 0
						iPlaylistSavingState = 0
						SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
						RELEASE_CONTROL_OF_FRONTEND()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYLIST_SAVING
			PAUSE_MENU_SET_COLUMN_BUSY(1, FALSE)
			RESET_PUBLISH_PLAYLIST_VARS()
			SET_WARNING_MESSAGE_WITH_HEADER("PM_WAIT", "PM_SAVING", FE_WARNING_SPINNER_ONLY)
			//IF SAVEOUT_PLAYLIST_DETAILS_TO_CLOUD(pmRockstarCreatedData, g_sMenuPlayListDetails, sPublishPlaylistVars.tl23FileAndPath)			
				iPlaylistSavingState++
			//ENDIF
		BREAK
		
		CASE PLAYLIST_PUBLISH
			SET_WARNING_MESSAGE_WITH_HEADER("PM_WAIT", "PM_SAVING", FE_WARNING_SPINNER_ONLY)
			IF DEAL_WITH_PLAYER_SELECTING_PUBLISH(	sPublishPlaylistVars.iSwitchingInt, 
													sPublishPlaylistVars.iPublishStage, 
													sPublishPlaylistVars.bSucess,
													sPublishPlaylistVars.bPublish, 
													sPublishPlaylistVars.iButtonBitSetPassed, 
													sPublishPlaylistVars.tl23FileAndPath, 
													szFilePaths,
													tl23ReturnedContexID, 
													sPublishPlaylistVars.bUpdate,  
													UGC_TYPE_GTA5_MISSION_PLAYLIST, 
													g_sMenuPlayListDetails.tl31PlaylistName, 
													sPublishPlaylistVars.tl23SelectedPlaylist)
				
				RESET_PUBLISH_PLAYLIST_VARS()
				sPublishPlaylistVars.bUpdate = FALSE									
				PAUSE_MENU_CLEAR_CREATED_PLAYLIST()
				CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, PLAYLISTS_LOADED)
				iPlaylistSavingState++
			ENDIF
		BREAK
		
		CASE PLAYLIST_SAVED
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_WARNING_MESSAGE SET_WARNING_MESSAGE_WITH_HEADER")
			IF sPublishPlaylistVars.bSucess
				SET_WARNING_MESSAGE_WITH_HEADER("PM_COMPL", "PM_SAVED1", FE_WARNING_OK)
			ELSE
				SET_WARNING_MESSAGE_WITH_HEADER("PM_INF_QMFT", "PM_SAVED1N", FE_WARNING_OK)
			ENDIF
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_WARNING_MESSAGE Accept pressed")
					
					RESET_PLAYLIST_MIN_AND_MAX()		
					RESET_LOADED_PLAYLIST_VARS(g_sMenuPlayListDetails)
				
					iPlaylistSavingState = 0
					
					PAUSE_MENU_CLEAR_CREATED_PLAYLIST()
				
					iPauseMenuDepth--
					IF iPauseMenuItemStored = ENUM_TO_INT(PM_D3_PLAYLIST_EDIT_PLAYLIST)
						pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_PLAYLISTS)
						iPauseMenuItemStored = -1
					ELSE
						pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_CREATE_PLAYLIST)
					ENDIF
								
					PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
					PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
					SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
					CLEANUP_TEMP_PASSIVE_MODE()
					
					
					
					
					RELEASE_CONTROL_OF_FRONTEND()
					
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	IF IS_CLOUD_DOWN_CLOUD_LOADER()
		SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_CLOUD_WARNING_MESSAGE)
	ENDIF
ENDPROC



PROC PROCESS_PAUSE_MENU_PLAYLIST_NAME_CHALLENGE()

	SWITCH iChallengeSavingState
	
		CASE PLAYLIST_SAVE_NAME
			TEXT_LABEL_31 strChallengeName
			BOOL bQuit
			
			IF GET_CHALLENGE_NAME_FROM_KEYBOARD(challengeOSKStatus, iChallengeProfanityToken, iChallengeKBStage, bQuit, strChallengeName)
				
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_NAME_CHALLENGE - Saving Challenge with name: ", strChallengeName)

				g_sMenuPlayListDetails.tl31ChallengeName = strChallengeName
				
				iChallengeSavingState = CHALLENGE_SAVED

			ELSE
				IF bQuit
				OR bStoredQuit
					IF iChallengeKBStage = -1
						bStoredQuit = TRUE
						SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", "PM_CH_NM_AL", FE_WARNING_OK)
						IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
							//IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
								iChallengeKBStage = 0
								bStoredQuit = FALSE
								bSetCrewChallenge = FALSE
								bDoCrewHeadToHead = FALSE
							//ENDIF
						ENDIF
					ENDIF
					IF NOT bStoredQuit
						iChallengeSavingState = 0
						iChallengeKBStage = 0
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_NAME_CHALLENGE - bQuit")
						SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
						RELEASE_CONTROL_OF_FRONTEND()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CHALLENGE_SAVED
			SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_WARNING_MESSAGE)
			iChallengeSavingState = 0
			iChallengeKBStage = 0
		BREAK
		
	ENDSWITCH
ENDPROC


CONST_INT PLAYLIST_ARE_YOU_SURE_YOU_WANT_TO_DELETE	0
CONST_INT PLAYLIST_DELETE							1
CONST_INT PLAYLIST_DELETE_CONFIRMATION				2

PROC PROCESS_PAUSE_MENU_PLAYLIST_DELETE()

	SWITCH iPlaylistDeleteState
		
		CASE PLAYLIST_ARE_YOU_SURE_YOU_WANT_TO_DELETE
		
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELETEY", FE_WARNING_YESNO)
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					PAUSE_MENU_SET_COLUMN_BUSY(1, FALSE)
					RESET_UGC_LOAD_VARS(sGetUGC_content)
					UGC_CANCEL_QUERY()
					CLEAR_BIT(iBS_General, biMY_PLAYLISTS_LOADED)
					
					iPlaylistDeleteState = PLAYLIST_DELETE
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE")
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ENDIF			
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
					SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYLIST_DELETE
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELETED", FE_WARNING_SPINNER_ONLY)
			IF DELETE_UGC_BY_CONTENT_ID(sGetUGC_content, g_sLoadedPlaylistDetails.tl31szContentID[pauseMenuRoute[2]], UGC_TYPE_GTA5_MISSION_PLAYLIST)
				CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, PLAYLISTS_LOADED)
				iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION
				sPublishPlaylistVars.bUpdate = FALSE
				RESET_UGC_LOAD_VARS(sGetUGC_content)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION")
			ENDIF
		BREAK
		
		CASE PLAYLIST_DELETE_CONFIRMATION
		
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELETEYY", FE_WARNING_OK)
									
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_WARNING_MESSAGE Accept pressed")
				
					CLEAR_PM_COLUMN(FIRST_COLUMN)
					CLEAR_PM_COLUMN(SECOND_COLUMN)
					
					IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
						RESET_PLAYLIST_MIN_AND_MAX()			
						RESET_LOADED_PLAYLIST_VARS(g_sMenuPlayListDetails)
					ENDIF
					
					iPlaylistDeleteState = 0
					IF pauseMenuRoute[iPauseMenuDepth] > 0
						pauseMenuRoute[iPauseMenuDepth]--
					ENDIF
					
					iPauseMenuDepth--
										
					CLEAR_BIT(iBS_General, biMY_PLAYLISTS_LOADED)
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, PLAYLISTS_LOADED)		
					CLEAR_BIT(iBS_DataAvailable, PLAYLISTS_AVAILABLE)
					
					PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
					PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
					SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
					RELEASE_CONTROL_OF_FRONTEND()
					
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
					PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
					
					RESET_UGC_LOAD_VARS(sGetUGC_content)
					
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_DELETE - FINISHED")
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	IF IS_CLOUD_DOWN_CLOUD_LOADER()
		SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_CLOUD_WARNING_MESSAGE)
	ENDIF
ENDPROC

PROC PROCESS_PAUSE_MENU_PLAYLIST_DELETE_BOOKMARK()

	SWITCH iPlaylistDeleteState
		
		CASE PLAYLIST_ARE_YOU_SURE_YOU_WANT_TO_DELETE
		
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELETEY", FE_WARNING_YESNO)
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					PAUSE_MENU_SET_COLUMN_BUSY(1, FALSE)
					RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedPlaylist)
					UGC_CANCEL_QUERY()
					CLEAR_BIT(iBS_General, biMY_PLAYLISTS_LOADED)
					
					iPlaylistDeleteState = PLAYLIST_DELETE
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE")
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ENDIF			
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
					SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYLIST_DELETE
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELETED", FE_WARNING_SPINNER_ONLY)
			IF UGC_SET_BOOKMARKED(g_sMenuPlayListDetails.tl31szContentID, FALSE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION_PLAYLIST))
				CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, BOOKMARKED_PLAYLISTS_LOADED)
				iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION
				RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedPlaylist)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION")
			ENDIF
		BREAK
		
		CASE PLAYLIST_DELETE_CONFIRMATION
		
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELETEYY", FE_WARNING_OK)
									
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_WARNING_MESSAGE Accept pressed")
				
					CLEAR_PM_COLUMN(FIRST_COLUMN)
					CLEAR_PM_COLUMN(SECOND_COLUMN)
					
					IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
						RESET_PLAYLIST_MIN_AND_MAX()			
						RESET_LOADED_PLAYLIST_VARS(g_sMenuPlayListDetails)
					ENDIF
					
					iPlaylistDeleteState = 0
					IF pauseMenuRoute[iPauseMenuDepth] > 0
						pauseMenuRoute[iPauseMenuDepth]--
					ENDIF
					
					iPauseMenuDepth--
										
					CLEAR_BIT(iBS_General, biMY_PLAYLISTS_LOADED)
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, BOOKMARKED_PLAYLISTS_LOADED)		
					CLEAR_BIT(iBS_DataAvailable, BOOKMARKED_PLAYLISTS_AVAILABLE)
					
					PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
					PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
					SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
					RELEASE_CONTROL_OF_FRONTEND()
					
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
					PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
					
					RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedPlaylist)
					
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_PLAYLIST_DELETE - FINISHED")
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	IF IS_CLOUD_DOWN_CLOUD_LOADER()
		SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_CLOUD_WARNING_MESSAGE)
	ENDIF
ENDPROC

PROC PROCESS_PAUSE_MENU_BOOKMARK_ADD()
	IF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_RECENTLY_PLAYED)	
		IF UGC_SET_BOOKMARKED(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlName, TRUE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
			SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)	
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("BookmarkJob"))
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("RemoveBookmark"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		ENDIF
	ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_ROCKSTAR_CREATED)
		IF UGC_SET_BOOKMARKED(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iPauseMenuItemSelected].tlName, TRUE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
			SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)	
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("BookmarkJob"))
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("RemoveBookmark"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		ENDIF
	ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_ROCKSTAR_VERIFIED)
		IF UGC_SET_BOOKMARKED(g_FMMC_ROCKSTAR_VERIFIED_PAUSE_MENU.sMissionHeaderVars[iPauseMenuItemSelected].tlName, TRUE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
			SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)	
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("BookmarkJob"))
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("RemoveBookmark"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		ENDIF
	ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_COMMUNITY_JOBS)
		IF UGC_SET_BOOKMARKED(g_FMMC_ROCKSTAR_VERIFIED_PAUSE_MENU.sMissionHeaderVars[iPauseMenuItemSelected].tlName, TRUE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
			SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)	
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("BookmarkJob"))
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("RemoveBookmark"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PAUSE_MENU_BOOKMARK_DELETE()
	SWITCH iPlaylistDeleteState
		
		CASE PLAYLIST_ARE_YOU_SURE_YOU_WANT_TO_DELETE
		
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELBOOKY", FE_WARNING_YESNO)
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					PAUSE_MENU_SET_COLUMN_BUSY(1, FALSE)
					IF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_RECENTLY_PLAYED)	
						RESET_UGC_LOAD_VARS(sGetUGC_contentRecent)
						UGC_CANCEL_QUERY()
						CLEAR_BIT(iBS_DataAvailable, RECENTLY_PLAYED_AVAILABLE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
						CLEAR_BIT(iBS_DataAvailable, BOOKMARKED_JOBS_AVAILABLE)
						g_sRECENT_MISSION_HISTORY.bBookmarked[iPauseMenuItemSelected] = FALSE
					ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_ROCKSTAR_CREATED)
						RESET_UGC_LOAD_VARS(sGetUGC_content)
						UGC_CANCEL_QUERY()
						CLEAR_BIT(iBS_DataAvailable, ROCKSTAR_CREATED_AVAILABLE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
						CLEAR_BIT(iBS_DataAvailable, BOOKMARKED_JOBS_AVAILABLE)
						g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iPauseMenuItemSelected].bBookMarked = FALSE
					ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_ROCKSTAR_VERIFIED)
						RESET_UGC_LOAD_VARS(sGetUGC_contentVerified)
						UGC_CANCEL_QUERY()
						CLEAR_BIT(iBS_DataAvailable, ROCKSTAR_VERIFIED_AVAILABLE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
						CLEAR_BIT(iBS_DataAvailable, BOOKMARKED_JOBS_AVAILABLE)
						g_FMMC_ROCKSTAR_VERIFIED_PAUSE_MENU.sDefaultCoronaOptions[iPauseMenuItemSelected].bBookMarked = FALSE
					ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_COMMUNITY_JOBS)
						RESET_UGC_LOAD_VARS(sGetUGC_contentVerified)
						UGC_CANCEL_QUERY()
						CLEAR_BIT(iBS_DataAvailable2, COMMUNITY_JOBS_AVAILABLE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
						CLEAR_BIT(iBS_DataAvailable2, BOOKMARKED_JOBS_AVAILABLE)
						g_FMMC_ROCKSTAR_VERIFIED_PAUSE_MENU.sDefaultCoronaOptions[iPauseMenuItemSelected].bBookMarked = FALSE
					ELSE
						CLEAR_BIT(iBS_DataAvailable, BOOKMARKED_JOBS_AVAILABLE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
						CLEAR_BIT(iBS_DataAvailable, RECENTLY_PLAYED_AVAILABLE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentRecent)
						CLEAR_BIT(iBS_DataAvailable, ROCKSTAR_VERIFIED_AVAILABLE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentVerified)
						CLEAR_BIT(iBS_DataAvailable2, COMMUNITY_JOBS_AVAILABLE)
						CLEAR_BIT(iBS_DataAvailable, ROCKSTAR_CREATED_AVAILABLE)
						RESET_UGC_LOAD_VARS(sGetUGC_content)
						UGC_CANCEL_QUERY()
						
					ENDIF
					
					iPlaylistDeleteState = PLAYLIST_DELETE
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_BOOKMARK_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE")
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ENDIF			
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
					SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_CANCEL)
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYLIST_DELETE
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELBOOKD", FE_WARNING_SPINNER_ONLY)
			IF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_RECENTLY_PLAYED)	
				IF UGC_SET_BOOKMARKED(g_sRECENT_MISSION_HISTORY.sRecentMissionHeaderVars[iPauseMenuItemSelected].tlName, FALSE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, BOOKMARKED_JOBS_LOADED)
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RECENTLY_PLAYED_LOADED)
					iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION
					RESET_UGC_LOAD_VARS(sGetUGC_contentRecent)
					RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_BOOKMARK_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION")
				ENDIF
			ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_ROCKSTAR_CREATED)
				IF UGC_SET_BOOKMARKED(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iPauseMenuItemSelected].tlName, FALSE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, BOOKMARKED_JOBS_LOADED)
					iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION
					RESET_UGC_LOAD_VARS(sGetUGC_content)
					RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_BOOKMARK_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION")
				ENDIF
			ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_ROCKSTAR_VERIFIED)
				IF UGC_SET_BOOKMARKED(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iPauseMenuItemSelected].tlName, FALSE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, BOOKMARKED_JOBS_LOADED)
					iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION
					RESET_UGC_LOAD_VARS(sGetUGC_content)
					RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_BOOKMARK_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION")
				ENDIF
			ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_COMMUNITY_JOBS)
				IF UGC_SET_BOOKMARKED(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iPauseMenuItemSelected].tlName, FALSE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, BOOKMARKED_JOBS_LOADED)
					iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION
					RESET_UGC_LOAD_VARS(sGetUGC_content)
					RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_BOOKMARK_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION")
				ENDIF
			ELSE
				IF UGC_SET_BOOKMARKED(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS.sMyMissionHeaderVars[iPauseMenuItemSelected].tlName, FALSE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, BOOKMARKED_JOBS_LOADED)
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RECENTLY_PLAYED_LOADED)
					iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION
					RESET_UGC_LOAD_VARS(sGetUGC_contentRecent)
					RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_BOOKMARK_DELETE - iPlaylistDeleteState = PLAYLIST_DELETE_CONFIRMATION")
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYLIST_DELETE_CONFIRMATION
		
			SET_WARNING_MESSAGE_WITH_HEADER("PM_DELETE", "PM_DELBOOKYY", FE_WARNING_OK)
									
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
			
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_WARNING_MESSAGE Accept pressed")
				
					CLEAR_PM_COLUMN(FIRST_COLUMN)
					CLEAR_PM_COLUMN(SECOND_COLUMN)
					CLEAR_PM_COLUMN(THIRD_COLUMN)
					
					iPlaylistDeleteState = 0

					CLEAR_PAUSE_MENU_BUSY_COLUMNS()
					LOCK_PM_COLUMN(SECOND_COLUMN, FALSE)
					PM_MENU_SHIFT_DEPTH(-1)
					
					PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
					PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
					SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_DISPLAYING)
					SET_PAUSE_MENU_MP_COLUMN_STATE(PAUSE_MENU_COLUMN_STATE_FIRST_COLUMN)
					
					IF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_RECENTLY_PLAYED)
						PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("RemoveBookmark"))
						CLEAN_RECENT_HISTORY_HEADER_DATA(g_sRECENT_MISSION_HISTORY, TRUE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentRecent)
						CLEAN_HEADER_DATA(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS, TRUE, TRUE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_ROCKSTAR_CREATED)
						PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("RemoveBookmark"))
						RESET_UGC_LOAD_VARS(sGetUGC_content)
						CLEAN_HEADER_DATA(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS, TRUE, TRUE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_ROCKSTAR_VERIFIED)
						PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("RemoveBookmark"))
						RESET_UGC_LOAD_VARS(sGetUGC_contentVerified)
						CLEAN_HEADER_DATA(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS, TRUE, TRUE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					ELIF pauseMenuRoute[2] = ENUM_TO_INT(PM_D3_JOBS_COMMUNITY_JOBS)
						PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("RemoveBookmark"))
						RESET_UGC_LOAD_VARS(sGetUGC_contentVerified)
						CLEAN_HEADER_DATA(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS, TRUE, TRUE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					ELSE
						PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
						CLEAN_RECENT_HISTORY_HEADER_DATA(g_sRECENT_MISSION_HISTORY, TRUE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentRecent)
						CLEAN_HEADER_DATA(g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS, TRUE, TRUE)
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedJobs)
					ENDIF
					PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
					SET_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PROCESS_PAUSE_MENU_BOOKMARK_DELETE - FINISHED")
				ENDIF
			ELSE
				IF IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CLEAR_BIT(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	IF IS_CLOUD_DOWN_CLOUD_LOADER()
		SET_PAUSE_MENU_MP_GLOBAL_STATE(PAUSE_MENU_GLOBAL_STATE_CLOUD_WARNING_MESSAGE)
	ENDIF
ENDPROC


/// PURPOSE: Returns TRUE if player is creating a playlist of some length
FUNC BOOL IS_PLAYER_CREATING_PLAYLIST()

	SWITCH INT_TO_ENUM(ePM_D1_CATEGORIES, pauseMenuRoute[0])
		CASE PM_D1_PLAYLISTS
			SWITCH INT_TO_ENUM(ePM_D2_PLAYLIST_TYPE, pauseMenuRoute[1])
				CASE PM_D2_CREATE_PLAYLIST
					IF iPauseMenuDepth = 2
						IF g_sMenuPlayListDetails.iLength > 0
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC


CONST_INT ciPLAYLIST_FAIL_1  1
CONST_INT ciPLAYLIST_FAIL_2  2
CONST_INT ciPLAYLIST_FAIL_3  3
CONST_INT ciPLAYLIST_FAIL_4  4
CONST_INT ciPLAYLIST_FAIL_5  5
CONST_INT ciPLAYLIST_FAIL_6  6
CONST_INT ciPLAYLIST_FAIL_PLAYER_MISSMATCH 7 


FUNC BOOL GET_UGC_FROM_CURRENT_TYPE_ROCKSTAR(PLAY_LIST_HEADER_DETAILS_ROCKSTAR &thisPlaylistData, BOOL &bAvailable, INT &iFailReason)
	SWITCH INT_TO_ENUM(ePM_D2_PLAYLIST_TYPE, pauseMenuRoute[1])
		CASE PM_D2_PLAYLISTS
			sGetUGC_content.bFrontEndRequest = TRUE
			IF (GET_UGC_BY_CONTENT_ID(sGetUGC_content, thisPlaylistData.tl31szContentID[pauseMenuRoute[2]], UGC_TYPE_GTA5_MISSION_PLAYLIST))
				IF sGetUGC_content.bSuccess
					bAvailable = TRUE
					RETURN TRUE
				ELSE
					bAvailable = FALSE
					IF sGetUGC_content.bCantLoadFromUser
						IF sGetUGC_content.bCanGetFriend
							iFailReason = ciPLAYLIST_FAIL_1
						ELSE
							iFailReason = ciPLAYLIST_FAIL_2
						ENDIF
					ELIF sGetUGC_content.bPlayListPlayerMissMatch 
						iFailReason = ciPLAYLIST_FAIL_PLAYER_MISSMATCH
					ELSE
						iFailReason = ciPLAYLIST_FAIL_3
					ENDIF
					PRINTLN("GET_UGC_FROM_CURRENT_TYPE_ROCKSTAR - iFailReason = ", iFailReason)
					
					RETURN TRUE
					
				ENDIF
			ENDIF
		BREAK
		CASE PM_D2_DOWNLOADED_PLAYLISTS
			sGetUGC_contentBookmarkedPlaylist.bFrontEndRequest = TRUE
			IF (GET_UGC_BY_CONTENT_ID(sGetUGC_contentBookmarkedPlaylist, thisPlaylistData.tl31szContentID[pauseMenuRoute[2]], UGC_TYPE_GTA5_MISSION_PLAYLIST))
				IF sGetUGC_contentBookmarkedPlaylist.bSuccess
					bAvailable = TRUE
					RETURN TRUE
				ELSE
					bAvailable = FALSE
					IF sGetUGC_contentBookmarkedPlaylist.bCantLoadFromUser
						IF sGetUGC_contentBookmarkedPlaylist.bCanGetFriend
							iFailReason = ciPLAYLIST_FAIL_1
						ELSE
							iFailReason = ciPLAYLIST_FAIL_2
						ENDIF
					ELIF sGetUGC_contentBookmarkedPlaylist.bPlayListPlayerMissMatch 
						iFailReason = ciPLAYLIST_FAIL_PLAYER_MISSMATCH
					ELSE
						iFailReason = ciPLAYLIST_FAIL_4
					ENDIF
					PRINTLN("GET_UGC_FROM_CURRENT_TYPE_ROCKSTAR - iFailReason = ", iFailReason)
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE PM_D2_ROCKSTAR_VERIFIED_PLAYLISTS
		CASE PM_D2_ROCKSTAR_PLAYLISTS
			sGetUGC_contentRockstarPlaylist.bFrontEndRequest = TRUE
			IF (GET_UGC_BY_CONTENT_ID(sGetUGC_contentRockstarPlaylist, thisPlaylistData.tl31szContentID[pauseMenuRoute[2]], UGC_TYPE_GTA5_MISSION_PLAYLIST))
				IF sGetUGC_contentRockstarPlaylist.bSuccess
					bAvailable = TRUE
					RETURN TRUE
				ELSE
					bAvailable = FALSE
					IF sGetUGC_contentRockstarPlaylist.bCantLoadFromUser
						IF sGetUGC_contentRockstarPlaylist.bCanGetFriend
							iFailReason = ciPLAYLIST_FAIL_1
						ELSE
							iFailReason = ciPLAYLIST_FAIL_2
						ENDIF
					ELIF sGetUGC_contentRockstarPlaylist.bPlayListPlayerMissMatch 
						iFailReason = ciPLAYLIST_FAIL_PLAYER_MISSMATCH
					ELSE
						iFailReason = ciPLAYLIST_FAIL_5
					ENDIF
					PRINTLN("GET_UGC_FROM_CURRENT_TYPE_ROCKSTAR - iFailReason = ", iFailReason)
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_UGC_FROM_CURRENT_TYPE(PLAY_LIST_HEADER_DETAILS &thisPlaylistData, BOOL &bAvailable, INT &iFailReason)
	
	DEBUG_PRINTCALLSTACK()
	
	SWITCH INT_TO_ENUM(ePM_D2_PLAYLIST_TYPE, pauseMenuRoute[1])
		CASE PM_D2_PLAYLISTS
			sGetUGC_content.bFrontEndRequest = TRUE
			IF (GET_UGC_BY_CONTENT_ID(sGetUGC_content, thisPlaylistData.tl31szContentID[pauseMenuRoute[2]], UGC_TYPE_GTA5_MISSION_PLAYLIST))
				IF sGetUGC_content.bSuccess
					bAvailable = TRUE
					PRINTLN("TEMP - GET_UGC_FROM_CURRENT_TYPE A - ", iFailReason)
					
					RETURN TRUE
				ELSE
					bAvailable = FALSE
					IF sGetUGC_content.bZeroLengthPlaylist
						iFailReason = ciPLAYLIST_FAIL_6
					ELSE
						IF sGetUGC_content.bCantLoadFromUser
							IF sGetUGC_content.bCanGetFriend
								iFailReason = ciPLAYLIST_FAIL_1
							ELSE
								iFailReason = ciPLAYLIST_FAIL_2
							ENDIF
						ELIF sGetUGC_content.bPlayListPlayerMissMatch 
							iFailReason = ciPLAYLIST_FAIL_PLAYER_MISSMATCH
						ELSE
							iFailReason = ciPLAYLIST_FAIL_3
						ENDIF
					ENDIF
					PRINTLN("TEMP - GET_UGC_FROM_CURRENT_TYPE B - ", iFailReason)
					
					RETURN TRUE
					
				ENDIF
			ENDIF
		BREAK
		CASE PM_D2_DOWNLOADED_PLAYLISTS
			sGetUGC_contentBookmarkedPlaylist.bFrontEndRequest = TRUE
			IF (GET_UGC_BY_CONTENT_ID(sGetUGC_contentBookmarkedPlaylist, thisPlaylistData.tl31szContentID[pauseMenuRoute[2]], UGC_TYPE_GTA5_MISSION_PLAYLIST))
				IF sGetUGC_contentBookmarkedPlaylist.bSuccess
					bAvailable = TRUE
				
					PRINTLN("TEMP - GET_UGC_FROM_CURRENT_TYPE C - ", iFailReason)
						
					RETURN TRUE
				ELSE
					bAvailable = FALSE
					IF sGetUGC_contentBookmarkedPlaylist.bCantLoadFromUser
						IF sGetUGC_contentBookmarkedPlaylist.bCanGetFriend
							iFailReason = ciPLAYLIST_FAIL_1
						ELSE
							iFailReason = ciPLAYLIST_FAIL_2
						ENDIF
					ELIF sGetUGC_contentBookmarkedPlaylist.bPlayListPlayerMissMatch 
						iFailReason = ciPLAYLIST_FAIL_PLAYER_MISSMATCH
					ELSE
						iFailReason = ciPLAYLIST_FAIL_4
					ENDIF
					
					PRINTLN("TEMP - GET_UGC_FROM_CURRENT_TYPE D - ", iFailReason)
										
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE PM_D2_ROCKSTAR_VERIFIED_PLAYLISTS
		CASE PM_D2_ROCKSTAR_PLAYLISTS
			sGetUGC_contentRockstarPlaylist.bFrontEndRequest = TRUE
			IF (GET_UGC_BY_CONTENT_ID(sGetUGC_contentRockstarPlaylist, thisPlaylistData.tl31szContentID[pauseMenuRoute[2]], UGC_TYPE_GTA5_MISSION_PLAYLIST))
				IF sGetUGC_contentRockstarPlaylist.bSuccess
					bAvailable = TRUE
					
					PRINTLN("TEMP - GET_UGC_FROM_CURRENT_TYPE E - ", iFailReason)
										
					RETURN TRUE
				ELSE
					bAvailable = FALSE
					IF sGetUGC_contentRockstarPlaylist.bCantLoadFromUser
						IF sGetUGC_contentRockstarPlaylist.bCanGetFriend
							iFailReason = ciPLAYLIST_FAIL_1
						ELSE
							iFailReason = ciPLAYLIST_FAIL_2
						ENDIF
					ELIF sGetUGC_contentRockstarPlaylist.bPlayListPlayerMissMatch 
						iFailReason = ciPLAYLIST_FAIL_PLAYER_MISSMATCH
					ELSE
						iFailReason = ciPLAYLIST_FAIL_5
					ENDIF
					
					PRINTLN("TEMP - GET_UGC_FROM_CURRENT_TYPE F - ", iFailReason)
					
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_PLAYLIST_CONTEXTS()
	BOOL bIsDebug
	#IF IS_DEBUG_BUILD
		bIsDebug = TRUE
	#ENDIF
	
	IF CAN_DO_QUICK_MATCH(iGarageCheck) = 0
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SupressSelectPM"))
	ENDIF
	
	SWITCH INT_TO_ENUM(ePM_D2_PLAYLIST_TYPE, pauseMenuRoute[1])
		CASE PM_D2_PLAYLISTS
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - Playlist is challenge compatible. UPDATE BUTTONS - A")
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DeletePlaylist"))
			IF IS_GAME_LINKED_TO_SOCIAL_CLUB()
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("EditPlaylist"))
			ENDIF
//			IF IS_BIT_SET(g_sLoadedPlaylistDetails.iChallengeCompatableBitSet, pauseMenuRoute[2])
//			AND IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			AND NETWORK_GET_VC_WALLET_BALANCE() >= 1000
//				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
//			ELSE
//				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
//			ENDIF
			IF IS_THIS_A_SOLO_SESSION()
				IF NOT IS_BIT_SET(g_sLoadedPlaylistDetails.iChallengeCompatableBitSet, pauseMenuRoute[2])
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SupressSelectPM"))
					CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - PAUSE_MENU_ACTIVATE_CONTEXT SupressSelectPM 3")
				ENDIF
			ENDIF
			IF bZeroLengthPlaylist
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SupressSelectPM"))
			ENDIF
			IF NOT g_sMPTunables.bToggleHeadtoheadoff 
			OR bIsDebug
				IF IS_BIT_SET(g_sLoadedPlaylistDetails.iHead2HeadCompatableBitSet, pauseMenuRoute[2])
				AND NETWORK_GET_VC_WALLET_BALANCE() >= 1000
				AND IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
				AND !IS_THIS_A_SOLO_SESSION()
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("StartHeadToHead"))
				ELSE
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("StartHeadToHead"))
				ENDIF
			ENDIF
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DeletePlaylist"))
			IF IS_GAME_LINKED_TO_SOCIAL_CLUB()
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("EditPlaylist"))
			ENDIF
			
			#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_XBOX_PLATFROM() 
					IF NOT bDisplayXboxPrompt
						IF IS_ANY_MP_CONTENT_REDUCED_DUE_TO_PERMISSIONS() = TRUE
							NETWORK_RESOLVE_PRIVILEGE_USER_CONTENT()
							bDisplayXboxPrompt = TRUE
							PRINTLN("SET_PLAYLIST_CONTEXTS bDisplayXboxPrompt = TRUE")
						ENDIF
					ENDIF
				ENDIF
			#ENDIF 
			
			
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		BREAK
		CASE PM_D2_DOWNLOADED_PLAYLISTS
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - Playlist is challenge compatible. UPDATE BUTTONS - B")
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DeletePlaylist"))
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
			IF IS_THIS_A_SOLO_SESSION()
				IF NOT IS_BIT_SET(g_sLoadedPlaylistDetails.iChallengeCompatableBitSet, pauseMenuRoute[2])
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SupressSelectPM"))
					CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - PAUSE_MENU_ACTIVATE_CONTEXT SupressSelectPM 4")
				ENDIF
			ENDIF
//			IF IS_BIT_SET(g_sLoadedBookMarkedPlaylistDetails.iChallengeCompatableBitSet, pauseMenuRoute[2])
//			AND IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			AND NETWORK_GET_VC_WALLET_BALANCE() >= 1000
//				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
//			ELSE
//				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
//			ENDIF
			IF NOT g_sMPTunables.bToggleHeadtoheadoff 
			OR bIsDebug
				IF IS_BIT_SET(g_sLoadedBookMarkedPlaylistDetails.iHead2HeadCompatableBitSet, pauseMenuRoute[2])
				AND NETWORK_GET_VC_WALLET_BALANCE() >= 1000
				AND IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
				AND !IS_THIS_A_SOLO_SESSION()
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("StartHeadToHead"))
				ELSE
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("StartHeadToHead"))
				ENDIF
			ENDIF
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		BREAK
		CASE PM_D2_ROCKSTAR_PLAYLISTS
		CASE PM_D2_ROCKSTAR_VERIFIED_PLAYLISTS
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - Playlist is challenge compatible. UPDATE BUTTONS - C")
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
//			IF IS_BIT_SET(g_sLoadedRockstarPlaylistDetails.iChallengeCompatableBitSet, pauseMenuRoute[2])
//			AND IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			AND NETWORK_GET_VC_WALLET_BALANCE() >= 1000
//				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
//			ELSE
//				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
//			ENDIF
			IF IS_THIS_A_SOLO_SESSION()
				IF NOT IS_BIT_SET(g_sLoadedPlaylistDetails.iChallengeCompatableBitSet, pauseMenuRoute[2])
					CPRINTLN(DEBUG_PAUSE_MENU, "CMcM@PAUSEMENU - PAUSE_MENU_ACTIVATE_CONTEXT SupressSelectPM 1")
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SupressSelectPM")) 
				ENDIF
			ENDIF
			IF NOT g_sMPTunables.bToggleHeadtoheadoff 
			OR bIsDebug
				IF IS_BIT_SET(g_sLoadedRockstarPlaylistDetails.iHead2HeadCompatableBitSet, pauseMenuRoute[2])
				AND NETWORK_GET_VC_WALLET_BALANCE() >= 1000
				AND IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
				AND !IS_THIS_A_SOLO_SESSION()
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("StartHeadToHead"))
				ELSE
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("StartHeadToHead"))
				ENDIF
			ENDIF
			//PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SupressSelectPM"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		BREAK
	ENDSWITCH

ENDPROC

PROC RESET_UNUSED_MISSION_VARS()
	INT iCount
	FMMC_MISSION_DETAILS_STRUCT sLoadedMissionDetailsBlank
	FOR iCount = g_sMenuPlayListDetails.iLength TO (COUNT_OF(g_sMenuPlayListDetails.sLoadedMissionDetails) - 1)
		g_sMenuPlayListDetails.sLoadedMissionDetails[iCount]	= sLoadedMissionDetailsBlank
	ENDFOR
ENDPROC
INT iPlaylistFailReason

//CONST_INT PLAYER_PLAYLIST_TYPE		0
//CONST_INT ROCKSTAR_PLAYLIST_TYPE	1
FUNC BOOL UPDATE_PLAYER_PLAYLISTS(BOOL &bDataAvailable, PLAY_LIST_HEADER_DETAILS &thisPlaylistData)
	BOOL bPlaylistsLoaded, bVerified, bCanReturn = TRUE, bCreated
	STRING sColumnTitle, sPLWarn, sErrorMessage
	
	IF NOT bShowError
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - reset bShowError = FALSE")
		iPlaylistFailReason = 0
	ENDIF
	
	//INT iFailReason
	//Reset and prepare to load if we looking at differen playlists
	SWITCH INT_TO_ENUM(ePM_D2_PLAYLIST_TYPE, pauseMenuRoute[1])
		CASE PM_D2_PLAYLISTS
			sColumnTitle = "PM_TMYPLL"
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
			IF NOT IS_BIT_SET(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
				RESET_PLAYLIST_MIN_AND_MAX()			
				RESET_UNUSED_MISSION_VARS()
				SET_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - RESET_PLAYLISTS set - A")
			ENDIF
			IF HAVE_PLAYLISTS_LOADED()
				sPLWarn = "PM_PL_MY_AL"
				bPlaylistsLoaded = TRUE
			ENDIF
		BREAK
		CASE PM_D2_DOWNLOADED_PLAYLISTS
			sColumnTitle = "PM_TBMPLL"
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
			IF NOT IS_BIT_SET(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
				RESET_PLAYLIST_MIN_AND_MAX()			
				RESET_UNUSED_MISSION_VARS()
				SET_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - RESET_BOOKMARKED_PLAYLISTS set - A1")
			ENDIF
			IF HAVE_BOOKMARKED_PLAYLISTS_LOADED()
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
				sPLWarn = "PM_PL_BM_AL"
				bPlaylistsLoaded = TRUE
			ENDIF
		BREAK
		CASE PM_D2_ROCKSTAR_PLAYLISTS
			sColumnTitle = "PM_TRSPLL"
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
			IF NOT IS_BIT_SET(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
				RESET_PLAYLIST_MIN_AND_MAX()			
				RESET_UNUSED_MISSION_VARS()
				SET_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - RESET_ROCKSTAR_PLAYLISTS set - A2")
			ENDIF
			IF HAVE_ROCKSTAR_PLAYLISTS_LOADED()
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
				bPlaylistsLoaded = TRUE
			ENDIF
		BREAK
		CASE PM_D2_ROCKSTAR_VERIFIED_PLAYLISTS
			sColumnTitle = "PM_R_V_PLIST"
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
			IF NOT IS_BIT_SET(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
				RESET_PLAYLIST_MIN_AND_MAX()			
				RESET_UNUSED_MISSION_VARS()
				SET_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - RESET_ROCKSTAR_PLAYLISTS set - A3")
			ENDIF
			IF HAVE_ROCKSTAR_VERIFIED_PLAYLISTS_LOADED()
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
				bPlaylistsLoaded = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bPlaylistsLoaded	= TRUE //HAVE_PLAYLISTS_LOADED()
		
		IF NOT IS_BIT_SET(iBS_General, biMY_PLAYLISTS_LOADED)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - A - thisPlaylistData.iTotalPlaylists = ", thisPlaylistData.iTotalPlaylists)
			IF thisPlaylistData.iTotalPlaylists > 0
				CLEAR_PM_COLUMN(FIRST_COLUMN)
				CLEAR_PM_COLUMN(SECOND_COLUMN)
				
				
				
				PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, sColumnTitle, "")
				
				INT iLoop
				FOR iLoop = 0 TO (thisPlaylistData.iTotalPlaylists - 1)
					IF IS_BIT_SET(thisPlaylistData.iChallengeCompatableBitSet, iLoop)
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - TRUE - thisPlaylistData.iChallengeCompatableBitSet = ", iLoop)
					ELSE
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - thisPlaylistData.iChallengeCompatableBitSet = ", iLoop)
					ENDIF
					MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF FIRST_COLUMN, iLoop, CREATOR_MENU_ID, iLoop, TRUE, thisPlaylistData.tl31PlaylistName[iLoop])		// Replace with actual playlist name
				ENDFOR
				
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Z6 - SET iPauseMenuItemSelected to ", 0, " from ", iPauseMenuItemSelected)	
				iPauseMenuItemSelected = 0
				
				bDataAvailable = TRUE
				SET_BIT(iBS_DataAvailable, PLAYLISTS_AVAILABLE)
				
				SET_BIT(iBS_General, biMY_PLAYLISTS_LOADED)
				
				SHOW_PM_COLUMN(FIRST_COLUMN)
				//PAUSE_MENU_COLUMN_UPDATED(FIRST_COLUMN)
				//CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] CHECK bb")
				
				PM_DISPLAY_DATA_SLOT(FIRST_COLUMN)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				
				
				
			ELSE
				IF pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_PLAYLISTS)
				OR pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_DOWNLOADED_PLAYLISTS)
					
					SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", sPLWarn, FE_WARNING_BACK)
					bCanReturn = FALSE
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
						//IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
							PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SupressSelectPM"))
							PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
							iPauseMenuDepth--
							PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
							PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
							bCanReturn = TRUE
						//ENDIF
					ENDIF
				ENDIF
				IF bCanReturn
					bDataAvailable = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iBS_General, biMY_PLAYLISTS_CANCEL_QUERY)
				SWITCH INT_TO_ENUM(ePM_D2_PLAYLIST_TYPE, pauseMenuRoute[1])
					CASE PM_D2_PLAYLISTS
						RESET_UGC_LOAD_VARS(sGetUGC_content)
					BREAK
					CASE PM_D2_DOWNLOADED_PLAYLISTS
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedPlaylist)
					BREAK
					CASE PM_D2_ROCKSTAR_PLAYLISTS
					CASE PM_D2_ROCKSTAR_VERIFIED_PLAYLISTS
						RESET_UGC_LOAD_VARS(sGetUGC_contentRockstarPlaylist)
					BREAK
				ENDSWITCH
				
				UGC_CANCEL_QUERY()
				//SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				SET_BIT(iBS_General, biMY_PLAYLISTS_CANCEL_QUERY)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - UGC_CANCEL_QUERY - SET A")
				bDataAvailable = FALSE
			ELIF NOT bShowError
			
					IF GET_UGC_FROM_CURRENT_TYPE(thisPlaylistData, bDataAvailable, iPlaylistFailReason)
						
						
						
						IF bDataAvailable
							CLEAR_PM_COLUMN(SECOND_COLUMN)
							
							PM_SET_COLUMN_TITLE_ONLY(SECOND_COLUMN, "PM_TPLAYL", "")
							
							INT i
							FOR i = 0 TO (g_sMenuPlayListDetails.iLength-1)			
								IF NOT IS_STRING_NULL_OR_EMPTY(g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName)
									CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS - displaying your playlist in Second Column - B")
									CPRINTLN(DEBUG_PAUSE_MENU, "thisPlaylistData.sMissionNames[", pauseMenuRoute[2], "].tl31MissionName[", i, "] = ", g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName, ", GET_MISSION_VERIFIED_PM_STAT(i) = ", GET_MISSION_VERIFIED_PM_STAT(i), ", GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i) = ", GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i))
									// How do I check if it's verified?
									IF GET_MISSION_VERIFIED_PM_STAT(i)
										bVerified = TRUE
									ELSE
										bVerified = FALSE
									ENDIF
									IF GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i)
										bCreated = TRUE
									ELSE
										bCreated = FALSE
									ENDIF
									MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF SECOND_COLUMN, i, CREATOR_MENU_ID, i, TRUE, g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName, bCreated, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iType, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iSubType, IS_BIT_SET(g_sMenuPlayListDetails.sLoadedMissionDetails[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), bVerified, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iAdversaryModeType) // First i, may need to be a count. Whereas second i should be index in array
								ENDIF
							ENDFOR	
							
							PM_DISPLAY_DATA_SLOT(SECOND_COLUMN)
							
							CLEAR_BIT(iBS_General, biMY_PLAYLISTS_LOADED)	//CLEAR READY FOR NEXT TIME
							CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS A - displaying your playlist in Second Column - DONE - B")
							
							CLEAR_BIT(iBS_General, biMY_PLAYLISTS_CANCEL_QUERY)

							CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
							CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
							CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
							CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
							
							SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
							RESET_UNUSED_MISSION_VARS()
							SET_PLAYLIST_CONTEXTS()
							bShowError = FALSE						
							CPRINTLN(DEBUG_PAUSE_MENU, " bShowError = false, GET_UGC_FROM_CURRENT_TYPE")
							RETURN TRUE
						ELSE
							bShowError = TRUE
							CPRINTLN(DEBUG_PAUSE_MENU, " bShowError, UPDATE_PLAYER_PLAYLISTS")
						ENDIF
						
						
						
					ELSE
						bDataAvailable = FALSE
					ENDIF
				
			ENDIF
		ENDIF
		
		//PRINTLN("TEMP - UPDATE_PLAYER_PLAYLISTS - bShowError = ", bShowError, 
		//" bDataAvailable = ", bDataAvailable, " iPlaylistFailReason = ", iPlaylistFailReason, " bPlaylistsLoaded = ", bPlaylistsLoaded)
		
		IF bShowError
		
			IF iPlaylistFailReason = 1
				sErrorMessage = GET_CANT_LOAD_FROM_USER_FAIL_REASON()//"CONT_FRIEN"
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_content - GET_CANT_LOAD_FROM_USER_FAIL_REASON = ", GET_CANT_LOAD_FROM_USER_FAIL_REASON())
			ELIF iPlaylistFailReason = 2
				sErrorMessage = GET_CANT_LOAD_FROM_USER_FAIL_REASON() //"CONT_NONE"
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_content - GET_CANT_LOAD_FROM_USER_FAIL_REASON = ", GET_CANT_LOAD_FROM_USER_FAIL_REASON())
			ELIF iPlaylistFailReason = 3 
				sErrorMessage = GET_UGC_FAIL_MESSAGE(sGetUGC_content.eError)
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_content - GET_UGC_FAIL_MESSAGE = ", ENUM_TO_INT(sGetUGC_content.eError))
			ELIF iPlaylistFailReason = 4 
				sErrorMessage = GET_UGC_FAIL_MESSAGE(sGetUGC_contentBookmarkedPlaylist.eError)
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_contentBookmarkedPlaylist - GET_UGC_FAIL_MESSAGE = ", ENUM_TO_INT(sGetUGC_contentBookmarkedPlaylist.eError))	
			ELIF iPlaylistFailReason = 5
				sErrorMessage = GET_UGC_FAIL_MESSAGE(sGetUGC_contentRockstarPlaylist.eError)
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_contentRockstarPlaylist - GET_UGC_FAIL_MESSAGE = ", ENUM_TO_INT(sGetUGC_contentRockstarPlaylist.eError))	
			ELIF iPlaylistFailReason = 6
				sErrorMessage = "UGC_FL_PL_EMPTY"
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_contentRockstarPlaylist - GET_UGC_FAIL_MESSAGE = UGC_FL_PL_EMPTY ", ENUM_TO_INT(sGetUGC_contentRockstarPlaylist.eError))	
			ELIF iPlaylistFailReason = ciPLAYLIST_FAIL_PLAYER_MISSMATCH 
				sErrorMessage = "UGC_FL_PLMISMAT"
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_contentRockstarPlaylist - GET_UGC_FAIL_MESSAGE = iPLAYLIST_FAIL_PLAYER_MISSMATCH ")
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(sErrorMessage)
				SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", sErrorMessage, FE_WARNING_CANCEL)
			ENDIF
			bCanReturn = FALSE
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				//IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
				
				IF iPlaylistFailReason = 6
				OR iPlaylistFailReason = ciPLAYLIST_FAIL_PLAYER_MISSMATCH 
					CLEAR_PM_COLUMN(SECOND_COLUMN)
							
					PM_SET_COLUMN_TITLE_ONLY(SECOND_COLUMN, "PM_TPLAYL", "")
					
					PM_DISPLAY_DATA_SLOT(SECOND_COLUMN)
					
					CLEAR_BIT(iBS_General, biMY_PLAYLISTS_LOADED)	//CLEAR READY FOR NEXT TIME
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS C - displaying your playlist in Second Column - DONE - B")
					
					CLEAR_BIT(iBS_General, biMY_PLAYLISTS_CANCEL_QUERY)

					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
					CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
					
					SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
					RESET_UNUSED_MISSION_VARS()
					bZeroLengthPlaylist = TRUE
										
					SET_PLAYLIST_CONTEXTS()
					iPlaylistFailReason = 0
					bDataAvailable = TRUE
					bShowError = FALSE
			
					RETURN TRUE
				ELSE				
					PM_MENU_SHIFT_DEPTH(-1) 
					iPauseMenuDepth--
					PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
					PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
					bCanReturn = TRUE
				ENDIF
				
				bShowError = FALSE
				CPRINTLN(DEBUG_PAUSE_MENU, " bShowError = false, UPDATE_PLAYER_PLAYLISTS")
				iPlaylistFailReason = 0
				//ENDIF
			ENDIF
				
			
			IF bCanReturn
				bDataAvailable = FALSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL UPDATE_PLAYER_PLAYLISTS_ROCKSTAR(BOOL &bDataAvailable, PLAY_LIST_HEADER_DETAILS_ROCKSTAR &thisPlaylistData)
	BOOL bPlaylistsLoaded, bVerified, bCanReturn = TRUE, bCreated
	STRING sColumnTitle, sPLWarn, sErrorMessage
	INT iFailReason
	//Reset and prepare to load if we looking at differen playlists
	SWITCH INT_TO_ENUM(ePM_D2_PLAYLIST_TYPE, pauseMenuRoute[1])
		CASE PM_D2_PLAYLISTS
			sColumnTitle = "PM_TMYPLL"
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
			IF NOT IS_BIT_SET(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
				RESET_PLAYLIST_MIN_AND_MAX()			
				//RESET_LOADED_PLAYLIST_VARS(g_sMenuPlayListDetails)
				SET_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - RESET_PLAYLISTS set - A")
			ENDIF
			IF HAVE_PLAYLISTS_LOADED()
				IF bMyPlaylistDataAvailable
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DeletePlaylist"))
					IF IS_GAME_LINKED_TO_SOCIAL_CLUB()
						PAUSE_MENU_ACTIVATE_CONTEXT(HASH("EditPlaylist"))
					ENDIF
					sPLWarn = "PM_PL_MY_AL"
					bPlaylistsLoaded = TRUE
				ELSE
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
				ENDIF
			ENDIF
		BREAK
		CASE PM_D2_DOWNLOADED_PLAYLISTS
			sColumnTitle = "PM_TBMPLL"
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
			IF NOT IS_BIT_SET(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
				RESET_PLAYLIST_MIN_AND_MAX()			
				//RESET_LOADED_PLAYLIST_VARS(g_sMenuPlayListDetails)
				SET_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - RESET_BOOKMARKED_PLAYLISTS set - A1")
			ENDIF
			IF HAVE_BOOKMARKED_PLAYLISTS_LOADED()
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
				sPLWarn = "PM_PL_BM_AL"
				bPlaylistsLoaded = TRUE
			ENDIF
		BREAK
		CASE PM_D2_ROCKSTAR_PLAYLISTS
			sColumnTitle = "PM_TRSPLL"
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
			IF NOT IS_BIT_SET(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
				RESET_PLAYLIST_MIN_AND_MAX()			
				//RESET_LOADED_PLAYLIST_VARS(g_sMenuPlayListDetails)
				SET_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - RESET_ROCKSTAR_PLAYLISTS set - A2")
			ENDIF
			IF HAVE_ROCKSTAR_PLAYLISTS_LOADED()
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
				bPlaylistsLoaded = TRUE
			ENDIF
		BREAK
		CASE PM_D2_ROCKSTAR_VERIFIED_PLAYLISTS
			sColumnTitle = "PM_R_V_PLIST"
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
			CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
			IF NOT IS_BIT_SET(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
				RESET_PLAYLIST_MIN_AND_MAX()			
				//RESET_LOADED_PLAYLIST_VARS(g_sMenuPlayListDetails)
				SET_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - RESET_ROCKSTAR_PLAYLISTS set - A3")
			ENDIF
			IF HAVE_ROCKSTAR_VERIFIED_PLAYLISTS_LOADED()
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
				bPlaylistsLoaded = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bPlaylistsLoaded	= TRUE //HAVE_PLAYLISTS_LOADED()
		
		IF NOT IS_BIT_SET(iBS_General, biMY_PLAYLISTS_LOADED)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - A - thisPlaylistData.iTotalPlaylists = ", thisPlaylistData.iTotalPlaylists)
			IF thisPlaylistData.iTotalPlaylists > 0
				CLEAR_PM_COLUMN(FIRST_COLUMN)
				CLEAR_PM_COLUMN(SECOND_COLUMN)
				
				
				
				PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, sColumnTitle, "")
				
				INT iLoop
				FOR iLoop = 0 TO (thisPlaylistData.iTotalPlaylists - 1)
					IF IS_BIT_SET(thisPlaylistData.iChallengeCompatableBitSet, iLoop)
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - TRUE - thisPlaylistData.iChallengeCompatableBitSet = ", iLoop)
					ELSE
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - thisPlaylistData.iChallengeCompatableBitSet = ", iLoop)
					ENDIF
					MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF FIRST_COLUMN, iLoop, CREATOR_MENU_ID, iLoop, TRUE, thisPlaylistData.tl31PlaylistName[iLoop] )		// Replace with actual playlist name
				ENDFOR
				
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Z6 - SET iPauseMenuItemSelected to ", 0, " from ", iPauseMenuItemSelected)	
				iPauseMenuItemSelected = 0
				
				bDataAvailable = TRUE
				SET_BIT(iBS_DataAvailable, PLAYLISTS_AVAILABLE)
				
				SET_BIT(iBS_General, biMY_PLAYLISTS_LOADED)
				
				SHOW_PM_COLUMN(FIRST_COLUMN)
				//PAUSE_MENU_COLUMN_UPDATED(FIRST_COLUMN)
				//CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] CHECK bb")
				
				PM_DISPLAY_DATA_SLOT(FIRST_COLUMN)
				SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				
				
				
			ELSE
				IF pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_PLAYLISTS)
				OR pauseMenuRoute[1] = ENUM_TO_INT(PM_D2_DOWNLOADED_PLAYLISTS)
					
					SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", sPLWarn, FE_WARNING_BACK)
					bCanReturn = FALSE
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
						//IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
							iPauseMenuDepth--
							PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
							PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
							bCanReturn = TRUE
						//ENDIF
					ENDIF
				ENDIF
				IF bCanReturn
					bDataAvailable = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iBS_General, biMY_PLAYLISTS_CANCEL_QUERY)
				SWITCH INT_TO_ENUM(ePM_D2_PLAYLIST_TYPE, pauseMenuRoute[1])
					CASE PM_D2_PLAYLISTS
						RESET_UGC_LOAD_VARS(sGetUGC_content)
					BREAK
					CASE PM_D2_DOWNLOADED_PLAYLISTS
						RESET_UGC_LOAD_VARS(sGetUGC_contentBookmarkedPlaylist)
					BREAK
					CASE PM_D2_ROCKSTAR_PLAYLISTS
					CASE PM_D2_ROCKSTAR_VERIFIED_PLAYLISTS
						RESET_UGC_LOAD_VARS(sGetUGC_contentRockstarPlaylist)
					BREAK
				ENDSWITCH
				
				UGC_CANCEL_QUERY()
				//SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
				SET_BIT(iBS_General, biMY_PLAYLISTS_CANCEL_QUERY)
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - UGC_CANCEL_QUERY - SET A")
			ELIF NOT bShowError
				
				IF GET_UGC_FROM_CURRENT_TYPE_ROCKSTAR(thisPlaylistData, bDataAvailable, iFailReason)
					IF bDataAvailable
						CLEAR_PM_COLUMN(SECOND_COLUMN)
						
						PM_SET_COLUMN_TITLE_ONLY(SECOND_COLUMN, "PM_TPLAYL", "")
						
						INT i
						FOR i = 0 TO (g_sMenuPlayListDetails.iLength-1)			
							IF NOT IS_STRING_NULL_OR_EMPTY(g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName)
								CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - displaying your playlist in Second Column - B")
								CPRINTLN(DEBUG_PAUSE_MENU, "thisPlaylistData.sMissionNames[", pauseMenuRoute[2], "].tl31MissionName[", i, "] = ", g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName, ", GET_MISSION_VERIFIED_PM_STAT(i) = ", GET_MISSION_VERIFIED_PM_STAT(i), ", GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i) = ", GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i))
								// How do I check if it's verified?
								IF GET_MISSION_VERIFIED_PM_STAT(i)
									bVerified = TRUE
								ELSE
									bVerified = FALSE
								ENDIF
								IF GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i)
									bCreated = TRUE
								ELSE
									bCreated = FALSE
								ENDIF
								MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF SECOND_COLUMN, i, CREATOR_MENU_ID, i, TRUE, g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName, bCreated, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iType, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iSubType, IS_BIT_SET(g_sMenuPlayListDetails.sLoadedMissionDetails[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), bVerified, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iAdversaryModeType) // First i, may need to be a count. Whereas second i should be index in array
							ENDIF
						ENDFOR	
						
						PM_DISPLAY_DATA_SLOT(SECOND_COLUMN)
						
						CLEAR_BIT(iBS_General, biMY_PLAYLISTS_LOADED)	//CLEAR READY FOR NEXT TIME
						CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_PLAYER_PLAYLISTS_ROCKSTAR - displaying your playlist in Second Column - DONE - B")
						
						CLEAR_BIT(iBS_General, biMY_PLAYLISTS_CANCEL_QUERY)

						CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_PLAYLISTS)
						CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_PLAYLISTS)
						CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_BOOKMARKED_PLAYLISTS)
						CLEAR_BIT(g_iMpPauseMenuBS_LoadedMissions, RESET_ROCKSTAR_VERIFIED_PLAYLISTS)
						
						SET_PM_HIGHLIGHT(FIRST_COLUMN, pauseMenuRoute[2], FALSE, TRUE)
						
						SET_PLAYLIST_CONTEXTS()
						bShowError = FALSE						
						CPRINTLN(DEBUG_PAUSE_MENU, " bShowError = false, GET_UGC_FROM_CURRENT_TYPE_ROCKSTAR")
						RETURN TRUE
					ELSE
						PRINTLN("GET_UGC_FROM_CURRENT_TYPE_ROCKSTAR - bShowError = TRUE")
						bShowError = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF bShowError
			IF iFailReason = ciPLAYLIST_FAIL_1
				sErrorMessage = GET_CANT_LOAD_FROM_USER_FAIL_REASON()//"CONT_FRIEN"
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_content - GET_CANT_LOAD_FROM_USER_FAIL_REASON = ", GET_CANT_LOAD_FROM_USER_FAIL_REASON())
			ELIF iFailReason = ciPLAYLIST_FAIL_2
				sErrorMessage = GET_CANT_LOAD_FROM_USER_FAIL_REASON() //"CONT_NONE"
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_content - GET_CANT_LOAD_FROM_USER_FAIL_REASON = ", GET_CANT_LOAD_FROM_USER_FAIL_REASON())
			ELIF iFailReason = ciPLAYLIST_FAIL_3 
			
				sErrorMessage = GET_UGC_FAIL_MESSAGE(sGetUGC_content.eError)
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_content - GET_UGC_FAIL_MESSAGE = ", ENUM_TO_INT(sGetUGC_content.eError))
				
			ELIF iFailReason = ciPLAYLIST_FAIL_4 
			
				sErrorMessage = GET_UGC_FAIL_MESSAGE(sGetUGC_contentBookmarkedPlaylist.eError)
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_contentBookmarkedPlaylist - GET_UGC_FAIL_MESSAGE = ", ENUM_TO_INT(sGetUGC_contentBookmarkedPlaylist.eError))	
			ELSE // 5
			
				sErrorMessage = GET_UGC_FAIL_MESSAGE(sGetUGC_contentRockstarPlaylist.eError)
				CPRINTLN(DEBUG_PAUSE_MENU, "GET_UGC_FAIL_MESSAGE - sGetUGC_contentRockstarPlaylist - GET_UGC_FAIL_MESSAGE = ", ENUM_TO_INT(sGetUGC_contentRockstarPlaylist.eError))	
			ENDIF
			
			
			#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_XBOX_PLATFORM() 
					IF NOT bDisplayXboxPrompt
						IF IS_ANY_MP_CONTENT_REDUCED_DUE_TO_PERMISSIONS() = TRUE
							NETWORK_RESOLVE_PRIVILEGE_USER_CONTENT()
							bDisplayXboxPrompt = TRUE
							PRINTLN("UPDATE_PLAYER_PLAYLISTS_ROCKSTAR bDisplayXboxPrompt = TRUE")
						ELSE
							PRINTLN("UPDATE_PLAYER_PLAYLISTS_ROCKSTAR bDisplayXboxPrompt do nothing IS_ANY_MP_CONTENT_REDUCED_DUE_TO_PERMISSIONS = FALSE")
						ENDIF
					ELSE
						PRINTLN("UPDATE_PLAYER_PLAYLISTS_ROCKSTAR bDisplayXboxPrompt already played message")
					ENDIF
				ELSE
					PRINTLN("UPDATE_PLAYER_PLAYLISTS_ROCKSTAR bDisplayXboxPrompt NOT IS_XBOX_PLATFORM")
				ENDIF
			#ENDIF 
			
			
			
			CPRINTLN(DEBUG_PAUSE_MENU, "SET_WARNING_MESSAGE_WITH_HEADER - sErrorMessage = ", sErrorMessage)
			IF NOT IS_STRING_NULL_OR_EMPTY(sErrorMessage)
				SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", sErrorMessage, FE_WARNING_CANCEL)
			ENDIF
			bCanReturn = FALSE
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
				//IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
					CPRINTLN(DEBUG_PAUSE_MENU, " bShowError IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)")
				
					PM_MENU_SHIFT_DEPTH(-1)
					iPauseMenuDepth--
					PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
					PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
					bCanReturn = TRUE
					bShowError = FALSE
				//ENDIF
			ENDIF
			IF bCanReturn
				bDataAvailable = FALSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL UPDATE_CHOSEN_PLAYLIST(BOOL &bDataAvailable, GET_UGC_CONTENT_STRUCT &thisUGC_content, INT iType)
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_CHOSEN_PLAYLIST")
	// Will need to figure out which playlist to show
	BOOL bLoaded, bVerified, bCreated//, bAvailable
	TEXT_LABEL_23 tlContentId
	IF iType = ENUM_TO_INT(PM_D2_ROCKSTAR_PLAYLISTS)
		tlContentId = g_sLoadedRockstarPlaylistDetails.tl31szContentID[pauseMenuRoute[2]]
		bLoaded = HAVE_ROCKSTAR_PLAYLISTS_LOADED()//UPDATE_PLAYER_PLAYLISTS(bAvailable, g_sLoadedRockstarPlaylistDetails)
	ELIF iType = ENUM_TO_INT(PM_D2_ROCKSTAR_VERIFIED_PLAYLISTS)
		tlContentId = g_sLoadedRockstarPlaylistDetails.tl31szContentID[pauseMenuRoute[2]]
		bLoaded = HAVE_ROCKSTAR_VERIFIED_PLAYLISTS_LOADED()
	ELIF iType = ENUM_TO_INT(PM_D2_DOWNLOADED_PLAYLISTS)
		tlContentId = g_sLoadedBookmarkedPlaylistDetails.tl31szContentID[pauseMenuRoute[2]]
		bLoaded = HAVE_BOOKMARKED_PLAYLISTS_LOADED()//UPDATE_PLAYER_PLAYLISTS(bAvailable, g_sLoadedBookMarkedPlaylistDetails)
		
	ELSE
		tlContentId = g_sLoadedPlaylistDetails.tl31szContentID[pauseMenuRoute[2]]
		bLoaded = HAVE_PLAYLISTS_LOADED()//UPDATE_PLAYER_PLAYLISTS(bAvailable, g_sLoadedPlaylistDetails)//
		
	ENDIF
	
	
	INT i
	thisUGC_content.bFrontEndRequest = TRUE	
	IF bLoaded
		IF GET_UGC_BY_CONTENT_ID(thisUGC_content, tlContentId, UGC_TYPE_GTA5_MISSION_PLAYLIST)
		OR NETWORK_CHECK_USER_CONTENT_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE)
			IF thisUGC_content.bSuccess
				CLEAR_PM_COLUMN(FIRST_COLUMN)
				CLEAR_PM_COLUMN(SECOND_COLUMN)
				
				PM_SET_COLUMN_TITLE_ONLY(FIRST_COLUMN, "PM_TPLAYL", "")
				
				CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Z7 - SET iPauseMenuItemSelected to ", 0, " from ", iPauseMenuItemSelected)			
				iPauseMenuItemSelected = 0
				FOR i = 0 TO (g_sMenuPlayListDetails.iLength-1)			
					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] UPDATE_CHOSEN_PLAYLIST - displaying your playlist")
					bDataAvailable = TRUE
					IF GET_MISSION_VERIFIED_PM_STAT(i)
						bVerified = TRUE
					ELSE
						bVerified = FALSE
					ENDIF
					
					IF GET_MISSION_ROCKSTAR_CREATED_PM_STAT(i)
						bCreated = TRUE
					ELSE
						bCreated = FALSE
					ENDIF
					MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD 0,#ENDIF FIRST_COLUMN, i, CREATOR_MENU_ID, i, TRUE, g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlMissionName, bCreated, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iType, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iSubType, IS_BIT_SET(g_sMenuPlayListDetails.sLoadedMissionDetails[i].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED), bVerified, g_sMenuPlayListDetails.sLoadedMissionDetails[i].iAdversaryModeType) // First i, may need to be a count. Whereas second i should be index in array
				ENDFOR	
				
				IF g_sMenuPlayListDetails.iLength > 0
					bDataAvailable = TRUE
				ENDIF
				
				PAUSE_MENU_UPDATE_COLUMN(THIRD_COLUMN)
				
//				IF g_sMenuPlayListDetails.bChallengeCompatable
//				
//					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Playlist is challenge compatible. UPDATE BUTTONS - B")
//					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
//					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
//					IF CAN_DO_QUICK_MATCH(iQuickMatchTimer) = 0
//						IF NETWORK_GET_VC_WALLET_BALANCE() >= 1000
//							IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//								PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
//							ENDIF
//							IF g_sMenuPlayListDetails.bHead2HeadCompatable
//								PAUSE_MENU_ACTIVATE_CONTEXT(HASH("StartHeadToHead"))
//							ENDIF
//						ENDIF
//					ENDIF
//					PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
//				ELSE
//					CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Playlist is challenge compatible. UPDATE BUTTONS - C")
//					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("EditPlaylist"))
//					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SetCrewChallenge"))
//					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DeletePlaylist"))
//					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("StartHeadToHead"))
//					PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
//				ENDIF
				
				RETURN TRUE
			ELSE
			
				IF thisUGC_content.bCantLoadFromUser
					IF thisUGC_content.bCanGetFriend
						SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", "CONT_FRIEN", FE_WARNING_OK)	
					ELSE
						SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", "CONT_NONE", FE_WARNING_OK)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] UPDATE_CHOSEN_PLAYLIST - GET_UGC_FAIL_MESSAGE = ", GET_UGC_FAIL_MESSAGE(sGetUGC_content.eError))
					SET_WARNING_MESSAGE_WITH_HEADER("PCARD_SYNC_ERROR_TITLE", GET_UGC_FAIL_MESSAGE(sGetUGC_content.eError), FE_WARNING_OK)
				ENDIF
				BOOL bCanReturn = FALSE
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				OR NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE()
					//IF NOT IS_BIT_SET(iBS_ButtonPressed, PAUSE_MENU_MP_SELECT)
						iPauseMenuDepth--
						PAUSE_MENU_UPDATE_COLUMN(FIRST_COLUMN)
						PAUSE_MENU_UPDATE_COLUMN(SECOND_COLUMN)
						bCanReturn = TRUE
					//ENDIF
				ENDIF
				IF bCanReturn
					bDataAvailable = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
//		
	RETURN FALSE
ENDFUNC


