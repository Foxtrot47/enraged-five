
USING "commands_debug.sch"
USING "commands_script.sch"
USING "commands_graphics.sch"
USING "mp_icons.sch"

/// PURPOSE: An offset to differentiate menu options from menus
///    Is outmoded, and hopefully will go away
CONST_INT PREF_OPTIONS_THRESHOLD 1000



// MUST BE KEPT IN SYNC FROM CODE in ScriptMenu.cpp!
ENUM eOPERATION_TYPES
	// Fire-and-die types
	// These types should have NO WAITS; just instantly fall to the end of the block and die.
	
						//	Args filled out:		MenuScreenId	PreviousId	UniqueIdentifier
	kFill = 0, 			// Fill up the entire menu		-				-			-
	kLayoutChange = 1,	// Layout was changed			X				X			X
	kTriggerEvent = 2,	// Button was pressed			X				-			X
	kPrepareButtons = 4,// New menu item was selected	X				-			X
						// needs buttons filled out
	
	// Continual update type
	// Just run the script normally, full control
	// including WAIT(0) and stuff
	kUpdate = 3,			//		
	
	kPopulatePeds = 100,
	
	kUpdateInput = 5
	//-				-			-
ENDENUM



// MUST BE KEPT IN SYNC FROM CODE in ScriptMenu.h!
STRUCT PAUSE_MENU_LAUNCH_DATA
	eOPERATION_TYPES	operation			// operation being performed
	INT					MenuScreenId		// ID of the active menu
	INT					PreviousId			// Menu previously active
	INT					UniqueIdentifier 	// an identifier for the menu item
ENDSTRUCT


#IF IS_DEBUG_BUILD
FUNC STRING PM_DEBUG_GET_OPERATION_STRING(eOPERATION_TYPES eOperation)
	SWITCH eOperation
		CASE kFill
			RETURN "DATA-FILL"
		BREAK
		CASE kLayoutChange
			RETURN "LAYOUT-CHANGE"
		BREAK
		CASE kTriggerEvent
			RETURN "TRIGGER-EVENT"
		BREAK
		CASE kUpdate
			RETURN "UPDATE"
		BREAK
	ENDSWITCH
	SCRIPT_ASSERT("PM_DEBUG_GET_OPERATION_STRING: Invalid operation type passed.")
	RETURN "INVALID"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Sets up a column to be shown in the PauseMenu Scaleform
///    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    pszLabel - 	Stringtable identifier to display.
///    eMedalColour - what medal to display for this mission
PROC PM_SET_DATA_SLOT_WITH_MEDAL(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel, HUD_COLOURS eMedalColour )
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // active or not
		ENDIF

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel) // text label
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eMedalColour)) // medal colour
				
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Clear the data in a single data slot.
///
PROC PM_RESET_DATA_SLOT(INT iMenuIndex, INT iColumn = 1)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Brute force reset of all data slot items. Current max is 6, i'm not sure what the actual max is.
///    
PROC PM_RESET_ALL_DATA_SLOTS(INT iColumn = 1)
	INT iTemp, iSlotsMax
	iSlotsMax = 6
	REPEAT iSlotsMax iTemp
		PM_RESET_DATA_SLOT(iTemp, iColumn)
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Sets up a column to be shown in the PauseMenu Scaleform
///    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    pszLabel - 	Stringtable identifier to display.
PROC PM_SET_DATA_SLOT(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel, BOOL bLiteralString = FALSE, INT iOverrideType = 0)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOverrideType) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // active or not
		ENDIF
		
		IF bLiteralString = FALSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel) // text label
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(pszLabel) // literal string
		ENDIF
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up a column to be shown in the PauseMenu Scaleform with literal string
///    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    pszLabel - 	Stringtable identifier to display.
PROC PM_SET_DATA_SLOT_WITH_STRING(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel )
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // active or not
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(pszLabel) // text label
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Once PM_SET_DATA_SLOT has been called for a column, displays all the information
/// PARAMS:
///    iColumn - Which column this is for, probably 0,1,2
PROC PM_DISPLAY_DATA_SLOT(INT iColumn)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("DISPLAY_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets if a column can jump
///    This is used to block the pause menu from advancing into another column.
/// PARAMS:
///    iColumn - the column we are blocking / allowing to jump
///    bCanJump - TRUE if it can jump, false if we want to block it
PROC PM_SET_COLUMN_CAN_JUMP(INT iColumn, BOOL bCanJump)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_CAN_JUMP")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanJump)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Highlights the item at specified index in specified column
/// PARAMS:
///    iColumn - the column the item is in
///    iHighlightIndex - the item we want to highlight
PROC PM_SET_COLUMN_HIGHLIGHT(INT iColumn, 	INT iHighlightIndex)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_HIGHLIGHT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHighlightIndex)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Highlights the item at specified index in specified column and moves focus
/// PARAMS:
///    iColumn - the column the item is in
PROC PM_SET_COLUMN_FOCUS(INT iColumn)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_FOCUS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC
