//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 SP Mission Repeat Pause Menu Manager						│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		Launched by the pause menu to manage mission repeat data population		│
//│		and repeat launching. This thraed does not persist but runs when 		│
//│		fill, layout-change, or trigger events are fired by the menu and 		│
//│		then terminates after performing the required operation.				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "PauseMenu_public.sch"
USING "mission_repeat_data.sch"
USING "mission_stat_public.sch"
USING "commands_hud.sch"
USING "commands_replay.sch"

#IF  USE_CLF_DLC
USING "shop_public.sch"
#ENDIF

#IF USE_NRM_DLC
USING "shop_public.sch"
#ENDIF

#IF NOT USE_SP_DLC
USING "heist_end_screen.sch"
#ENDIF

//Columns.
CONST_INT REPEAT_COLUMN_MAIN_MENU	0
CONST_INT REPEAT_COLUMN_MISSIONS 	1
CONST_INT REPEAT_COLUMN_STATS 		2

//Menu IDs.
CONST_INT REPEAT_MENU_ID_REPLAY_MISSION		MENU_UNIQUE_ID_REPLAY_MISSION
CONST_INT REPEAT_MENU_ID_REPLAY_RC			MENU_UNIQUE_ID_REPLAY_RANDOM
CONST_INT REPEAT_MENU_ID_GAME				MENU_UNIQUE_ID_GAME
CONST_INT REPEAT_MENU_ID_LOAD_GAME			MENU_UNIQUE_ID_LOAD_GAME
CONST_INT REPEAT_MENU_ID_SAVE_GAME_LIST		MENU_UNIQUE_ID_SAVE_GAME_LIST
CONST_INT REPEAT_MENU_ID_NEW_GAME			MENU_UNIQUE_ID_NEW_GAME
CONST_INT REPEAT_MENU_ID_MISSIONS			MENU_UNIQUE_ID_REPLAY_MISSION_LIST	
CONST_INT REPEAT_MENU_ID_STATS				MENU_UNIQUE_ID_REPLAY_MISSION_ACTIVITY

CONST_INT REPEAT_MENU_ID_PROCESS_SAVEGAME		MENU_UNIQUE_ID_PROCESS_SAVEGAME		 // Upload
CONST_INT REPEAT_MENU_ID_PROCESS_SAVEGAME_LIST	MENU_UNIQUE_ID_PROCESS_SAVEGAME_LIST // Upload list


ENUM REPLAY_BLOCK_REASON
	RBR_NONE = -1,

	RBR_ON_MISSION = 0,
	RBR_IN_CREATOR,
	RBR_IN_CUTSCENE,
	RBR_IN_SHOP,
	RBR_IN_LOCATION,
	RBR_IN_DIRECTOR_MODE
ENDENUM

// Debug
TEXT_LABEL_7 tMissionName

//Operations.
// kFill 		 = 0 	- Fill up the entire menu.
// kLayoutChange = 1	- Layout was changed.
// kTriggerEvent = 2	- Button was pressed.

RepeatPlayData sMissionRepeatData[SP_MISSION_MAX]

// ----------------Repeat Play Pause Menu ---------------------------------------------------------------

/// PURPOSE:
///    Returns whether the player has completed any missions
/// RETURNS:
///    TRUE if a mission has been completed. FALSE otherwise
FUNC BOOL HAVE_ANY_MISSIONS_BEEN_COMPLETED()
	IF g_bLoadedClifford
		IF g_savedGlobalsClifford.sFlowCustom.iMissionsCompleted > 0
			RETURN TRUE
		ENDIF
		RETURN FALSE
		
	ELIF g_bLoadedNorman
		IF g_savedGlobalsnorman.sFlowCustom.iMissionsCompleted > 0
			RETURN TRUE
		ENDIF
		RETURN FALSE
		
	ELIF g_savedGlobals.sFlowCustom.iMissionsCompleted > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns whether the player has completed any RC missions
/// RETURNS:
///    TRUE if an RC mission has been completed. FALSE otherwise
FUNC BOOL HAVE_ANY_RC_MISSIONS_BEEN_COMPLETED()
	IF g_savedGlobals.sRandomChars.iRCMissionsCompleted > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ANY_MISSIONS_OF_TYPE_BEEN_COMPLETED(enumGrouping eMissionType)
	SWITCH eMissionType
		CASE CP_GROUP_MISSIONS
			RETURN HAVE_ANY_MISSIONS_BEEN_COMPLETED()
		BREAK
		
		CASE CP_GROUP_RANDOMCHARS
			RETURN HAVE_ANY_RC_MISSIONS_BEEN_COMPLETED()
		BREAK
		DEFAULT
			CPRINTLN(DEBUG_PAUSE_MENU, "HAVE_ANY_MISSIONS_OF_TYPE_BEEN_COMPLETED invalid eMissionType")
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns whether repeat play is currently blocked.
///    By being on mission, waiting for pass screen, being in barber shop chair,
///    Getting private dance in stripclub, being on a repeat play already
/// RETURNS:
///    
FUNC REPLAY_BLOCK_REASON IS_REPEAT_PLAY_BLOCKED()

	IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			CPRINTLN(DEBUG_REPEAT, "RP blocked: Director mode is running.")
			RETURN RBR_IN_DIRECTOR_MODE
		ENDIF
		
		CPRINTLN(DEBUG_REPEAT, "RP blocked: on mission")
		RETURN RBR_ON_MISSION
	ENDIF
	
	IF g_bResultScreenDisplaying = TRUE
		CPRINTLN(DEBUG_REPEAT, "RP blocked: results screen on")
		RETURN RBR_ON_MISSION
	ENDIF
	
	IF g_bLoadedClifford
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchCLF")) > 0 
			CPRINTLN(DEBUG_REPEAT, "RP blocked: mission stat watcher is running.")
			RETURN RBR_ON_MISSION
			// this was added to cover the gap between being on mission + the pass screen displaying
		ENDIF
	ELIF g_bLoadedNorman
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchNRM")) > 0 
			CPRINTLN(DEBUG_REPEAT, "RP blocked: mission stat watcher is running.")
			RETURN RBR_ON_MISSION
			// this was added to cover the gap between being on mission + the pass screen displaying
		ENDIF
	ELSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) > 0 
			CPRINTLN(DEBUG_REPEAT, "RP blocked: mission stat watcher is running.")
			RETURN RBR_ON_MISSION
			// this was added to cover the gap between being on mission + the pass screen displaying
		ENDIF
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("creator")) > 0 
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Creator is running.")
		RETURN RBR_IN_CREATOR
		// this was added to cover the gap between being on mission + the pass screen displaying
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_REPEAT, "RP blocked: already on repeat play.")
		RETURN RBR_ON_MISSION
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(g_StripclubGlobals.iStripclubFlags, GSF_GETTING_LAP_DANCE)
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Player gettting a lapdance.")
		RETURN RBR_IN_CUTSCENE
	ENDIF
	
	IF IS_PLAYER_IN_BARBER_CHAIR_OR_DOING_BARBER_INTRO()
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Player gettting haircut (or in barber intro).")
		RETURN RBR_IN_CUTSCENE
	ENDIF
	IF IS_PLAYER_BROWSING_MOD_SHOP()
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Player is inside a mod shop.")
		RETURN RBR_IN_SHOP
	ENDIF
	
	IF IS_PLAYER_BROWSING_TATTOO_SHOP()
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Player browsing tattoo shop.")
		RETURN RBR_IN_SHOP
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Player browsing in a shop.")
		RETURN RBR_IN_SHOP
	ENDIF
	
	IF IS_ANY_SHOP_INTRO_RUNNING()
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Shop intro is running.")
		RETURN RBR_IN_SHOP
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
	OR Is_Player_Timetable_Scene_In_Progress()
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Player switch is running.")
		RETURN RBR_IN_CUTSCENE
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<207.433578,-1019.795410,-100.472763>>, <<189.933777,-1019.623474,-95.568832>>, 17.187500)
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)	
				CPRINTLN(DEBUG_REPEAT, "RP blocked: Player in vehicle in garage.")
				RETURN RBR_IN_SHOP
			ENDIF
		ENDIF
	ENDIF
	
	IF g_bScriptsSetSafeForCutscene
		CPRINTLN(DEBUG_REPEAT, "RP blocked: Scripts are set safe for cutscene.")
		RETURN RBR_IN_CUTSCENE
	ENDIF
	
	IF g_sVehicleGenNSData.bInGarage
		CPRINTLN(DEBUG_REPEAT, "RP blocked: The player is in a special garage.")
		RETURN RBR_IN_LOCATION
	ENDIF
	
	RETURN RBR_NONE
ENDFUNC

/// PURPOSE:
///    Updates the last selected pause menu item so we can show correct missions stats when re-entering menu
/// PARAMS:
///    iNewValue - what we want to set this to
PROC UPDATE_LAST_SELECTED_ITEM(INT iNewValue)
	g_iLastSelectedPauseMenuMission = iNewValue
	CPRINTLN(DEBUG_PAUSE_MENU, "g_iLastSelectedPauseMenuMission = ", iNewValue)
ENDPROC


/// PURPOSE:
///    Initialises a scroll indicator for the missions column
PROC PM_INIT_SCROLL_COLUMN()
	CPRINTLN(DEBUG_PAUSE_MENU, "Init scroll column.")
	BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("INIT_COLUMN_SCROLL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(REPEAT_COLUMN_MISSIONS) // column
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE) // visible
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // column span
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // arrow type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2) // arrow positioning
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // allow override
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // column offset
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) // force invisible
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Displays a scroll indicator for the missions column
PROC PM_DISPLAY_SCROLL_COLUMN(INT iCurrentPos, INT iMaxPos)
	CPRINTLN(DEBUG_PAUSE_MENU, "Display scroll column. iCurrentPos =", iCurrentPos," iMaxPos=",  iMaxPos)
	BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_SCROLL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(REPEAT_COLUMN_MISSIONS) // column
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentPos) // current position in list
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMaxPos) // max position in list
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) // max visible -1 uses default
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Clears the scroll indicator for the missions column
PROC PM_CLEAR_SCROLL_COLUMN()
	CPRINTLN(DEBUG_PAUSE_MENU, "Clear scroll column")
	BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_SCROLL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(REPEAT_COLUMN_MISSIONS) // column
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) // current position in list
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) // max position in list
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) // max visible -1 uses default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Clears all data entries in the specified column
/// PARAMS:
///    iColumn - which column we are clearing (REPEAT_COLUMN_MISSIONS or REPEAT_COLUMN_STATS)
PROC PM_CLEAR_MENU_COLUMN(INT iColumn)
	CPRINTLN(DEBUG_PAUSE_MENU, "Clearing menu column: ", iColumn)
	BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
	END_SCALEFORM_MOVIE_METHOD()
	
	// clear the scroll column whenever we clear the mission column
	IF iColumn = REPEAT_COLUMN_MISSIONS
		PM_CLEAR_SCROLL_COLUMN()
	ENDIF
ENDPROC

/// PURPOSE:
///    Shows or hides the help screen (Select a Mission to replay.)
/// PARAMS:
///    bShow - if true the screen is shown. if false the screen is hidden
///    eMissionType - story or RCs
PROC PM_SHOW_REPEAT_PLAY_HELP(BOOL bShow, enumGrouping eMissionType)
	BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SHOW_WARNING_MESSAGE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bShow)
	
	STRING sMsg
	STRING sTitle
	INT iColIndex
	
	REPLAY_BLOCK_REASON eBlockReason = IS_REPEAT_PLAY_BLOCKED()
	
	IF eBlockReason != RBR_NONE
	
		iColIndex= 1
		sTitle=	"PM_RP_HELPT"
		
		//Story mission block messages.
		IF eMissionType = CP_GROUP_MISSIONS
			SWITCH eBlockReason
				CASE RBR_ON_MISSION			sMsg = "PM_RPB_SM_1"		BREAK
				CASE RBR_IN_CUTSCENE		sMsg = "PM_RPB_SM_2"		BREAK
				CASE RBR_IN_SHOP			sMsg = "PM_RPB_SM_3"		BREAK
				CASE RBR_IN_CREATOR			sMsg = "PM_RPB_SM_4"		BREAK
				CASE RBR_IN_LOCATION		sMsg = "PM_RPB_SM_5"		BREAK
				CASE RBR_IN_DIRECTOR_MODE	sMsg = "PM_RPB_SM_6"		BREAK
				DEFAULT
					SCRIPT_ASSERT("PM_SHOW_REPEAT_PLAY_HELP: Message text missing for repeat play block reason. Bug BenR.")
				BREAK
			ENDSWITCH
			
		//Strange and freak block messages.
		ELSE
			SWITCH eBlockReason
				CASE RBR_ON_MISSION			sMsg = "PM_RPB_RC_1"		BREAK
				CASE RBR_IN_CUTSCENE		sMsg = "PM_RPB_RC_2"		BREAK
				CASE RBR_IN_SHOP			sMsg = "PM_RPB_RC_3"		BREAK
				CASE RBR_IN_CREATOR			sMsg = "PM_RPB_RC_4"		BREAK
				CASE RBR_IN_LOCATION		sMsg = "PM_RPB_RC_5"		BREAK
				CASE RBR_IN_DIRECTOR_MODE	sMsg = "PM_RPB_RC_6"		BREAK
				DEFAULT
					SCRIPT_ASSERT("PM_SHOW_REPEAT_PLAY_HELP: Message text missing for repeat play block reason. Bug BenR.")
				BREAK
			ENDSWITCH
		ENDIF
		
	ELIF HAVE_ANY_MISSIONS_OF_TYPE_BEEN_COMPLETED(eMissionType)

		// set title + help to display
		iColIndex= 1
		IF eMissionType = CP_GROUP_MISSIONS
			sTitle=	"PM_RP_HELPT" // Replay
			sMsg = "PM_RP_HELP" // Replay Missions you've passed and try to reach the Gold standard for each. Aim to reach Gold in a single attempt, or work towards it one objective at a time.~n~~n~If you are a Social Club member, Mission stats are recorded on the Leaderboards on the Social Club website where you can see how you stack up against friends and Crewmates. Visit ~HUD_COLOUR_SOCIAL_CLUB~rockstargames.com/socialclub~s~ to view Leaderboards and in-depth career stats.
		ELSE
			sTitle=	"PM_RP_HELPT3" // Replay
			sMsg = "PM_RP_HELP3" // Select an encounter with a Random Character to replay.~n~~n~If you are a Social Club member, you can see which Random Characters you are yet to meet and see how you stack up against your friends and Crewmates. Visit ~HUD_COLOUR_SOCIAL_CLUB~rockstargames.com/socialclub~s~ for more information and to view in-depth career stats.
		ENDIF
	ELSE
		// set title + help to display
		iColIndex = 1
		IF eMissionType = CP_GROUP_MISSIONS
			sTitle=	"PM_RP_HELPT1" // Replay
			sMsg = "PM_RP_HELP1" // There are currently no Missions available to replay. Once you have played some Missions you will be able to try them again and aim to reach the Gold standard in each.~n~~n~If you are a Social Club member, Mission stats are recorded on the Leaderboards on the Social Club website where you can see how you stack up against friends and Crewmates. Visit ~HUD_COLOUR_SOCIAL_CLUB~rockstargames.com/socialclub~s~ to view Leaderboards and in-depth career stats.
		ELSE
			sTitle=	"PM_RP_HELPT3" // Replay
			sMsg = "PM_RP_HELP4" // There are currently no encounters with Random Characters available to replay.~n~~n~If you are a Social Club member, you can see which Random Characters you are yet to meet and see how you stack up against your friends and Crewmates. Visit ~HUD_COLOUR_SOCIAL_CLUB~rockstargames.com/socialclub~s~ for more information and to view in-depth career stats.
		ENDIF
	ENDIF
	
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3-iColIndex)	
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sMsg)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitle) // title text
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // tiling type
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("") // no header TXD
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("") // no header TXN
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Shows or hides a column.
/// PARAMS:
///    iColumn - the column we want to alter
///    bShow - TRUE = show column FALSE = hide column
PROC PM_SHOW_COLUMN(INT iColumn, BOOL bShow)
	IF bShow = TRUE
		CPRINTLN(DEBUG_PAUSE_MENU, "Show column: ", iColumn)

		// hide help
		PM_SHOW_REPEAT_PLAY_HELP(FALSE, CP_GROUP_MISSIONS)
	ELSE
		CPRINTLN(DEBUG_PAUSE_MENU, "Hide column: ", iColumn)
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SHOW_COLUMN")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bShow)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Shows or hides the mission list and stats.
/// PARAMS:
///    bShow - if TRUE we show the list and stats, otherwise we hide them
PROC PM_SHOW_MISSION_LIST_AND_STATS(BOOL bShow)
	PM_SHOW_COLUMN(REPEAT_COLUMN_MISSIONS, bShow)
	PM_SHOW_COLUMN(REPEAT_COLUMN_STATS, bShow)
ENDPROC

/// PURPOSE:
///    Makes a column show the "busy" spinner (or removes it)
/// PARAMS:
///    iColumn - the column that is busy (or no longer busy)
///    bBusy - is this column busy?
PROC PM_SET_COLUMN_BUSY(INT iColumn, BOOL bBusy)
	PAUSE_MENU_SET_BUSY_SPINNER(bBusy, iColumn)
ENDPROC

/// PURPOSE:
///    Request menu text. returns TRUE if text loaded
/// PARAMS:
///    iColumn - the column we want to show the "busy" spinner in
FUNC BOOL PM_REQUEST_TEXT(INT iColumn)

	REQUEST_ADDITIONAL_TEXT("MISHSTA", MP_STATS_TEXT_SLOT)
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(MP_STATS_TEXT_SLOT)
		CPRINTLN(DEBUG_PAUSE_MENU, "PauseMenu: Waiting for text to load")
		//PM_SET_COLUMN_BUSY(iColumn, TRUE)
		RETURN FALSE
	ENDIF
	//PM_SET_COLUMN_BUSY(iColumn, FALSE)
	iColumn = iColumn+ 1
	RETURN TRUE
ENDFUNC

PROC GET_RAMPAGE_MEDAL(INT iMissionID, INT &overrideMedal)
	// rampages weren't hooked up into the mission stats system, so if we're checking a rampage
	// just get the medal value from the saved global rampage scores and override it
	IF g_eMenuMissionType = CP_GROUP_RANDOMCHARS
		g_eRC_MissionIDs iRCMission
		iRCMission = INT_TO_ENUM(g_eRC_MissionIDs, iMissionID)
		//CPRINTLN(DEBUG_PAUSE_MENU, "GET_RAMPAGE_MEDAL check mission index: ", iMissionID)
		SWITCH iRCMission
			CASE RC_RAMPAGE_1
				//CPRINTLN(DEBUG_PAUSE_MENU, "rampage1, saved medal index is: ", g_savedGlobals.sRampageData.playerData[0].iMedalIndex)
				overrideMedal = g_savedGlobals.sRampageData.playerData[0].iMedalIndex
				IF overrideMedal = 0 
					overrideMedal =1 // fix for debug skipping past rampages- set bronze
				ENDIF
			BREAK
			CASE RC_RAMPAGE_2
				//CPRINTLN(DEBUG_PAUSE_MENU, "rampage2, saved medal index is: ", g_savedGlobals.sRampageData.playerData[1].iMedalIndex)
				overrideMedal = g_savedGlobals.sRampageData.playerData[1].iMedalIndex
				IF overrideMedal = 0 
					overrideMedal =1 // fix for debug skipping past rampages- set bronze
				ENDIF
			BREAK
			CASE RC_RAMPAGE_3
				//CPRINTLN(DEBUG_PAUSE_MENU, "rampage3, saved medal index is: ", g_savedGlobals.sRampageData.playerData[2].iMedalIndex)
				overrideMedal = g_savedGlobals.sRampageData.playerData[2].iMedalIndex
				IF overrideMedal = 0 
					overrideMedal =1 // fix for debug skipping past rampages- set bronze
				ENDIF
			BREAK
			CASE RC_RAMPAGE_4
				//CPRINTLN(DEBUG_PAUSE_MENU, "rampage4, saved medal index is: ", g_savedGlobals.sRampageData.playerData[3].iMedalIndex)
				overrideMedal = g_savedGlobals.sRampageData.playerData[3].iMedalIndex
				IF overrideMedal = 0 
					overrideMedal =1 // fix for debug skipping past rampages- set bronze
				ENDIF
			BREAK
			CASE RC_RAMPAGE_5
				//CPRINTLN(DEBUG_PAUSE_MENU, "rampage5, saved medal index is: ", g_savedGlobals.sRampageData.playerData[4].iMedalIndex)
				overrideMedal = g_savedGlobals.sRampageData.playerData[4].iMedalIndex
				IF overrideMedal = 0 
					overrideMedal =1 // fix for debug skipping past rampages- set bronze
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up the stat info for a story mission
/// PARAMS:
///    iStoryMissionIndex - the story mission we are showing the stat info for
PROC PM_SET_MISSION_DATA(INT iMissionIndex)
	tMissionName = GetRepeatPlayMissionName(iMissionIndex, g_eMenuMissionType)
	INT iStatIndex
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_PAUSE_MENU, "Repopulating mission stats panel for mission ", GET_STRING_FROM_TEXT_FILE(tMissionName), ".") 
		CPRINTLN(DEBUG_PAUSE_MENU, "iMissionIndex = ", iMissionIndex) 
	#ENDIF
				
	IF NOT PM_REQUEST_TEXT(REPEAT_COLUMN_STATS)
		CPRINTLN(DEBUG_PAUSE_MENU, "PM_SET_MISSION_DATA text not loaded, exit") 
		EXIT
	ENDIF
	PM_CLEAR_MENU_COLUMN(REPEAT_COLUMN_STATS)
	
	//Set the mission title.
	BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(REPEAT_COLUMN_STATS)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MISSTA")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tMissionName)
	END_SCALEFORM_MOVIE_METHOD()

	// normal story or RC mission
	
	INT overridePerc = -333
	INT overrideMedal = -333
	
	//Pass data in for each of this mission's stats.
	INT iTotalStats = 0
	INT iPassedStats = 0
	REPEAT GetRepeatPlayMissionStatCount(iMissionIndex, g_eMenuMissionType) iStatIndex
		
		ENUM_MISSION_STATS eMissionStat = GetRepeatPlayMissionStat(iMissionIndex, g_eMenuMissionType, iStatIndex)
		
		BOOL bDoThisOne = TRUE

		SWITCH eMissionStat
	
			CASE ASS1_MIRROR_MEDAL								
			CASE ASS2_MIRROR_MEDAL										
			CASE ASS4_MIRROR_MEDAL
			CASE ASS3_MIRROR_MEDAL									    
			CASE ASS5_MIRROR_MEDAL	
				bDoThisOne = FALSE
				overrideMedal = GET_MISSION_STAT_FRIENDLY_VALUE(eMissionStat)
				//Give bronze medal in case of shitskip
				if overrideMedal >= HIGHEST_INT
					overrideMedal = 1
				endif
				BREAK
				
			CASE ASS4_MIRROR_PERCENTAGE		    		    		    
			CASE ASS5_MIRROR_PERCENT	
			CASE ASS3_MIRROR_PERCENTAGE	
			CASE ASS1_MIRROR_PERCENT
			CASE ASS2_MIRROR_PERCENT
				bDoThisOne = FALSE
				overridePerc = GET_MISSION_STAT_FRIENDLY_VALUE(eMissionStat)
				//Give 50% CP in case of shitskip
				if overridePerc >= HIGHEST_INT
					overridePerc = 50
				endif
				BREAK

		ENDSWITCH
		
		IF (NOT g_MissionStatTrackingPrototypes[eMissionStat].bHidden)
			AND bDoThisOne
			IF SET_SCALEFORM_DATA_FOR_MISSION_STAT(eMissionStat, iTotalStats)
				++iPassedStats
			ENDIF
			++iTotalStats
		ENDIF
		
	ENDREPEAT
	
	GET_RAMPAGE_MEDAL(iMissionIndex, overrideMedal)
	
	//CPRINTLN(DEBUG_PAUSE_MENU, "Total passed  = : ", iPassedStats)
	//CPRINTLN(DEBUG_PAUSE_MENU, "Total attempted  = : ", iTotalStats)
	
	SET_SCALEFORM_DATA_FOR_MISSION_STAT_TOTALS(iPassedStats, iTotalStats, FALSE, overridePerc,overrideMedal)
	
	PM_DISPLAY_DATA_SLOT(REPEAT_COLUMN_STATS)
ENDPROC


/// PURPOSE:
///    Fills the repeat play array so we know which missions where completed in which order
PROC POPULATE_REPEAT_PLAY_ARRAY()

	//Get list of repeatable missions.
	SWITCH g_eMenuMissionType
					
		CASE CP_GROUP_MISSIONS
			CPRINTLN(DEBUG_PAUSE_MENU, "Populate Story mission list.")
			g_iNoRepeatableMissions = PopulateRepeatPlayArray(sMissionRepeatData, TRUE, FALSE, TRUE)
		BREAK
		
		CASE CP_GROUP_RANDOMCHARS
			CPRINTLN(DEBUG_PAUSE_MENU, "Populate RC mission list.")
			g_iNoRepeatableMissions = PopulateRepeatPlayArray(sMissionRepeatData, FALSE, TRUE, TRUE)
		BREAK
		
		DEFAULT 
			SCRIPT_ASSERT("POPULATE_REPEAT_PLAY_ARRAY tried to select invalid mission type.")
		BREAK
	ENDSWITCH
	CPRINTLN(DEBUG_PAUSE_MENU, "g_iNoRepeatableMissions = ", g_iNoRepeatableMissions)
ENDPROC


/// PURPOSE:
///    Finds the specified mission in the repeat play array
/// PARAMS:
///    iMissionToFind - the int value of the mission enum of the mission we're looking for
/// RETURNS:
///    INT the position in the repeat play array (used to update scroll indicator)
FUNC INT GET_POSITION_IN_REPEAT_PLAY_ARRAY(INT iMissionToFind)
	INT iMission
	FOR iMission = 0 TO ENUM_TO_INT(SP_MISSION_MAX) -1
		IF sMissionRepeatData[iMission].iMissionIndex = iMissionToFind
			RETURN iMission
		ENDIF
	ENDFOR
	RETURN -1 // should never happen
ENDFUNC


/// PURPOSE:
///    Populates the list of available repeat play misisons for selected mission type
PROC PM_POPULATE_REPEAT_PLAY_LIST()

	// Update list of repeatable missions.
	POPULATE_REPEAT_PLAY_ARRAY()
	
	// show or clear the scroll column
	IF g_iNoRepeatableMissions > 16
		PM_INIT_SCROLL_COLUMN()
		PM_DISPLAY_SCROLL_COLUMN(0, g_iNoRepeatableMissions)
	ELSE
		PM_CLEAR_SCROLL_COLUMN()
	ENDIF
	CPRINTLN(DEBUG_PAUSE_MENU, "iNoRepeatableMissions= ", g_iNoRepeatableMissions)
	
	//For each mission add a menu item.
	INT iRepeatIndex
	INT iMissionDataIndex
	
	INT iTotalStats = 0
	INT iPassedStats = 0
	ENUM_MISSION_STATS eMissionStat
	FLOAT fPercentage 
	INT iMedal
	INT iStatIndex
	
	REPEAT g_iNoRepeatableMissions iRepeatIndex
		iTotalStats = 0
	 	iPassedStats = 0
		INT overrideMedal = -1
		iMissionDataIndex = sMissionRepeatData[iRepeatIndex].iMissionIndex
		tMissionName = GetRepeatPlayMissionName(iMissionDataIndex, g_eMenuMissionType)
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PAUSE_MENU, "Adding repeatable mission item: [", iRepeatIndex, ", ", iMissionDataIndex, ", ", GET_STRING_FROM_TEXT_FILE(tMissionName), "].")
		#ENDIF
		REPEAT GetRepeatPlayMissionStatCount(iMissionDataIndex, g_eMenuMissionType) iStatIndex
			eMissionStat = GetRepeatPlayMissionStat(iMissionDataIndex, g_eMenuMissionType, iStatIndex)
			IF NOT g_MissionStatTrackingPrototypes[eMissionStat].bHidden
				IF GET_MISSION_STAT_PASS_STATUS(eMissionStat) =1
					++iPassedStats
				ENDIF
				SWITCH eMissionStat
					CASE ASS1_MIRROR_MEDAL								
					CASE ASS2_MIRROR_MEDAL										
					CASE ASS4_MIRROR_MEDAL
					CASE ASS3_MIRROR_MEDAL									    
					CASE ASS5_MIRROR_MEDAL	
						overrideMedal = GET_MISSION_STAT_FRIENDLY_VALUE(eMissionStat)
						BREAK
				ENDSWITCH
				
				++iTotalStats
			ENDIF
		ENDREPEAT
		
		GET_RAMPAGE_MEDAL(iMissionDataIndex, overrideMedal)
		
		fPercentage = GET_MISSION_STAT_PERCENTAGE(iPassedStats, iTotalStats)
		iMedal = GET_STAT_MEDAL_VALUE(fPercentage)
		IF overrideMedal > -1
			iMedal = overrideMedal
		ENDIF
		
		// debug info
		CPRINTLN(DEBUG_REPEAT, "----------------------")
		CPRINTLN(DEBUG_REPEAT, "tMissionName = : ", tMissionName)
		CPRINTLN(DEBUG_REPEAT, "Total passed  = : ", iPassedStats)
		CPRINTLN(DEBUG_REPEAT, "Total attempted  = : ", iTotalStats)
		CPRINTLN(DEBUG_REPEAT, "fPercentage  = : ", fPercentage)
		CPRINTLN(DEBUG_REPEAT, "iMedal = : ", iMedal)
		
		// set scaleform info
		PM_SET_DATA_SLOT_WITH_MEDAL(REPEAT_COLUMN_MISSIONS, iRepeatIndex, REPEAT_MENU_ID_MISSIONS+PREF_OPTIONS_THRESHOLD, iMissionDataIndex, TRUE, tMissionName, GET_MEDAL_HUD_COLOUR(iMedal))
	ENDREPEAT
	
	PM_DISPLAY_DATA_SLOT(REPEAT_COLUMN_MISSIONS)
ENDPROC

/// PURPOSE:
///    Hides or unhides the select option
/// PARAMS:
///    bShow - if true A: Select gets shown. If False it is hidden
PROC SHOW_SELECT_OPTION(BOOL bShow)
	IF bShow = TRUE
		// show select option
		CPRINTLN(DEBUG_PAUSE_MENU, "RepeatPlayMenu. Show Select option")
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("HIDE_ACCEPTBUTTON"))
	ELSE
		// hide select option	
		CPRINTLN(DEBUG_PAUSE_MENU, "RepeatPlayMenu. Hide Select option")
		PAUSE_MENU_ACTIVATE_CONTEXT(HASH("HIDE_ACCEPTBUTTON"))
	ENDIF
	PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
ENDPROC

/// PURPOSE:
///    Populates stats display for this mission.
/// PARAMS:
///    iMission - which mission we're now highlighting
///    bListAlreadyPopulated - if already populated this frame, don't do it again!
PROC UPDATE_CURRENTLY_HIGHLIGHTED_MISSION(INT iMission, BOOL bListAlreadyPopulated = FALSE)
	// Update stats panel for an individual repeatable mission.
	CPRINTLN(DEBUG_PAUSE_MENU, 	"UPDATE_CURRENTLY_HIGHLIGHTED_MISSION. ", iMission)
	
	IF IS_REPEAT_PLAY_BLOCKED() = RBR_NONE
		// only show missions list if we're not on a mission
		
		IF HAVE_ANY_MISSIONS_OF_TYPE_BEEN_COMPLETED(g_eMenuMissionType)
			CPRINTLN(DEBUG_PAUSE_MENU, "UPDATE_CURRENTLY_HIGHLIGHTED_MISSION updating")
			
			// update last selected mission + show stats
			UPDATE_LAST_SELECTED_ITEM(iMission)
			PM_SET_MISSION_DATA(iMission)
			
			// show the scroll column if needed
			IF g_iNoRepeatableMissions > 16
				// Update list of repeatable missions.
				IF  bListAlreadyPopulated = FALSE
					POPULATE_REPEAT_PLAY_ARRAY()
				ENDIF
				PM_DISPLAY_SCROLL_COLUMN(GET_POSITION_IN_REPEAT_PLAY_ARRAY(iMission), g_iNoRepeatableMissions)
			ENDIF
		ELSE
			CPRINTLN(DEBUG_PAUSE_MENU, "UPDATE_CURRENTLY_HIGHLIGHTED_MISSION : None available. don't do anything.")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_PAUSE_MENU, "UPDATE_CURRENTLY_HIGHLIGHTED_MISSION : on mission. don't do anything.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Called when the player selects Replay mission or Replay RC from the menu
///    Populates the mission list, highlights 1st mission. Shows stats for it.
///    Or shows help message on why repeat play is unavailable
/// PARAMS:
///    eMissionType - Story or RC
///    bListAlreadyPopulated - if already populated this frame, don't do it again!
PROC ENTER_REPEAT_PLAY_MENU(enumGrouping eMissionType, BOOL bListAlreadyPopulated = FALSE)
	
	IF IS_REPEAT_PLAY_BLOCKED() = RBR_NONE
		// only show missions list if we're not on a mission
		IF HAVE_ANY_MISSIONS_OF_TYPE_BEEN_COMPLETED(eMissionType)
			CPRINTLN(DEBUG_PAUSE_MENU, "Showing mission list. eMissionType= ", eMissionType)
			PM_REQUEST_TEXT(REPEAT_COLUMN_STATS)
			UPDATE_CURRENTLY_HIGHLIGHTED_MISSION(g_iLastSelectedPauseMenuMission, bListAlreadyPopulated)
			PM_SHOW_MISSION_LIST_AND_STATS(TRUE)
		ELSE
			CPRINTLN(DEBUG_PAUSE_MENU, "Not completed any missions- don't display mission list.")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_PAUSE_MENU, "On mission- don't display mission list.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Shows the Main entry screen for this repeat play type
///    (The one with the main help: Replay mission you've passed and try to reach gold standard etc)
/// PARAMS:
///    eMissionType - story or RC missions
///    bDoHideSelectCheck - if TRUE we hide the A: Select option if Repeat Play is unavailable
///   bForceRepopulate - if true we will repopulate the mission list (otherwise this is done if missiontype <> g_eMenuMissionType)
PROC SHOW_ENTRY_SCREEN(enumGrouping eMissionType, BOOL bDoHideSelectCheck, BOOL bForceRepopulate = FALSE)
	CPRINTLN(DEBUG_PAUSE_MENU, "SHOW_ENTRY_SCREEN being called now.") 
	
	BOOL bRepopulate = FALSE
	
	// set repeat play mission type
	IF g_eMenuMissionType <> eMissionType
	OR bForceRepopulate = TRUE
		CPRINTLN(DEBUG_PAUSE_MENU, "SHOW_ENTRY_SCREEN missionType changed. repopulate needed") 
		bRepopulate = TRUE
	ENDIF
	g_eMenuMissionType = eMissionType
	CPRINTLN(DEBUG_PAUSE_MENU, "SHOW_ENTRY_SCREEN g_eMenuMissionType = ", g_eMenuMissionType) 
	
	// request text, so it is loaded when we need to show the missions + stats
	PM_REQUEST_TEXT(REPEAT_COLUMN_STATS)

	IF IS_REPEAT_PLAY_BLOCKED() != RBR_NONE
	OR NOT HAVE_ANY_MISSIONS_OF_TYPE_BEEN_COMPLETED(eMissionType)
		// repeat play is not available
		CPRINTLN(DEBUG_PAUSE_MENU, "SHOW_ENTRY_SCREEN. Repeat play not available.")
		
		// See if we need to hide the "Select" button prompt
		IF bDoHideSelectCheck = TRUE
			SHOW_SELECT_OPTION(FALSE)
		ENDIF
		
		// hide stats + mission columns, and show the help instead
		PM_SHOW_MISSION_LIST_AND_STATS(FALSE)
		PM_SHOW_REPEAT_PLAY_HELP(TRUE, eMissionType)
	ELSE
		// repeat play is available
		CPRINTLN(DEBUG_PAUSE_MENU, "SHOW_ENTRY_SCREEN. Repeat play is available.")
		
		// clear the mission + stats columns + repopulate if needed
		IF bRepopulate = TRUE
			PM_SHOW_COLUMN(REPEAT_COLUMN_MISSIONS, FALSE)
			PM_SHOW_COLUMN(REPEAT_COLUMN_STATS, FALSE)
			PM_CLEAR_MENU_COLUMN(REPEAT_COLUMN_MISSIONS)
			PM_CLEAR_MENU_COLUMN(REPEAT_COLUMN_STATS)
			PM_POPULATE_REPEAT_PLAY_LIST()	
			CPRINTLN(DEBUG_PAUSE_MENU, "SHOW_ENTRY_SCREEN. Clear + update last selected item.")
			UPDATE_LAST_SELECTED_ITEM(sMissionRepeatData[0].iMissionIndex)
		ENDIF
		
		// See if we need to show the "Select" button prompt
		IF bDoHideSelectCheck = TRUE
			SHOW_SELECT_OPTION(TRUE)
		ENDIF
		
		// populate stats column, show mission and stats columns, hide help
		ENTER_REPEAT_PLAY_MENU(eMissionType, bRepopulate)
	ENDIF
ENDPROC

SCRIPT( PAUSE_MENU_LAUNCH_DATA args )

	//Load spPacks verions of this script
	IF g_bLoadedClifford 
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PauseMenu_SP_Repeat_CLF")) = 0
			REQUEST_SCRIPT_WITH_NAME_HASH(HASH("PauseMenu_SP_Repeat_CLF"))
			WHILE NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("PauseMenu_SP_Repeat_CLF"))
				WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(HASH("PauseMenu_SP_Repeat_CLF"), args, SIZE_OF(args), DEFAULT_STACK_SIZE)
		ENDIF
		
		//Shutdown bed save script since we're switching to AGT
		TERMINATE_THIS_THREAD()
	ELIF g_bLoadedNorman
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("PauseMenu_SP_Repeat_NRM")) = 0
			REQUEST_SCRIPT_WITH_NAME_HASH(HASH("PauseMenu_SP_Repeat_NRM"))
			WHILE NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("PauseMenu_SP_Repeat_NRM"))
				WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(HASH("PauseMenu_SP_Repeat_NRM"), args, SIZE_OF(args), DEFAULT_STACK_SIZE)
		ENDIF
		
		//Shutdown bed save script since we're switching to AGT
		TERMINATE_THIS_THREAD()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_PAUSE_MENU, 	"Repeat Play menu script launching. Op:", 
									PM_DEBUG_GET_OPERATION_STRING(args.operation), 
									" MenuID:", args.MenuScreenId, 
									" PrevID:", args.PreviousId, 
									" UniqueID:", args.UniqueIdentifier)
	#ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		CPRINTLN(DEBUG_PAUSE_MENU, 	"Repeat menu script terminated due to force cleanup.")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	SWITCH args.operation

		CASE kLayoutChange
			
			SWITCH args.MenuScreenId

				// ------------Player currently has "Replay Mission" highlighted in the left column of menu
				CASE REPEAT_MENU_ID_REPLAY_MISSION
					IF args.PreviousId = REPEAT_MENU_ID_GAME
						// if player uses triggers to scroll left / right through menu
						// we don't need to hide the Select button yet
						// also clear scroll indicator
						CPRINTLN(DEBUG_PAUSE_MENU, 	"Replay mission now highlighted via scroll through Pause menu.")
						SHOW_ENTRY_SCREEN(CP_GROUP_MISSIONS, FALSE, TRUE)
						PM_CLEAR_SCROLL_COLUMN() 

					ELIF args.PreviousId = REPEAT_MENU_ID_MISSIONS
						// backed out of mission list. just clear scroll indicator
						CPRINTLN(DEBUG_PAUSE_MENU, 	"Replay mission now highlighted via backing out of mission list.")
						PM_CLEAR_SCROLL_COLUMN()
					ELSE
						// otherwise we need to check whether to hide it
						CPRINTLN(DEBUG_PAUSE_MENU, 	"Replay mission now highlighted via scroll in Game menu.")
						SHOW_ENTRY_SCREEN(CP_GROUP_MISSIONS, TRUE)
					ENDIF
				BREAK
				
				// ------------Player currently has "Replay Random Character" highlighted in the left column of menu
				CASE REPEAT_MENU_ID_REPLAY_RC
					IF args.PreviousId = REPEAT_MENU_ID_MISSIONS
						// backed out of mission list. just clear scroll indicator
						CPRINTLN(DEBUG_PAUSE_MENU, 	"Replay mission now highlighted via backing out of mission list.")
						PM_CLEAR_SCROLL_COLUMN()
					ELSE
						CPRINTLN(DEBUG_PAUSE_MENU, 	"Replay RC now highlighted.")
						SHOW_ENTRY_SCREEN(CP_GROUP_RANDOMCHARS, TRUE)
					ENDIF
				BREAK

				// -------------Player is scrolling through the mission list------------
				CASE REPEAT_MENU_ID_MISSIONS
					CPRINTLN(DEBUG_PAUSE_MENU, 	"Player scrolling through missions.")
					UPDATE_CURRENTLY_HIGHLIGHTED_MISSION(args.UniqueIdentifier)
				BREAK
				
				CASE REPEAT_MENU_ID_SAVE_GAME_LIST
					IF args.PreviousId = REPEAT_MENU_ID_SAVE_GAME_LIST
						SET_SAVEGAME_LIST_UNIQUE_ID(args.UniqueIdentifier)
					ENDIF
				BREAK
				
				CASE REPEAT_MENU_ID_PROCESS_SAVEGAME_LIST
					IF args.PreviousId = REPEAT_MENU_ID_PROCESS_SAVEGAME_LIST
						SET_SAVEGAME_LIST_UNIQUE_ID(args.UniqueIdentifier)
					ENDIF
				BREAK

				// ---------------We have moved off the replay entry item --------------------------
				DEFAULT
					CPRINTLN(DEBUG_PAUSE_MENU, 	"Exited repeat play menu.")

					// show the select option again
					SHOW_SELECT_OPTION(TRUE)
					
					// check if we should show or hide the repeat play help
					IF args.MenuScreenId = REPEAT_MENU_ID_LOAD_GAME
					OR args.MenuScreenId = REPEAT_MENU_ID_NEW_GAME
					OR args.MenuScreenId = REPEAT_MENU_ID_PROCESS_SAVEGAME
						// hide help if going to new game, load game or Upload Savegame
						CPRINTLN(DEBUG_PAUSE_MENU, 	"Gone to new / load game.")
						PM_SHOW_REPEAT_PLAY_HELP(FALSE, CP_GROUP_MISSIONS)
						
						// hide stats 
						PM_SHOW_COLUMN(REPEAT_COLUMN_STATS, FALSE)
						
						// hide scroll bar
						PM_CLEAR_SCROLL_COLUMN()
						
						// reset the mission type selection as we're no longer in the main section of the menu
						g_eMenuMissionType = CP_GROUP_NO_GROUP
						UPDATE_LAST_SELECTED_ITEM(-1)
					ELSE
						IF args.MenuScreenId = REPEAT_MENU_ID_GAME
							IF args.PreviousId = REPEAT_MENU_ID_REPLAY_MISSION
							OR args.PreviousId = REPEAT_MENU_ID_REPLAY_RC
								// leaving replay mission menu to the Game menu
								// nothing to be done
							ELSE
								// clear help if going to game menu from New Game or Load Game
								PM_SHOW_REPEAT_PLAY_HELP(FALSE, CP_GROUP_MISSIONS)
							ENDIF
						ELSE
							// hide help if leaving the game menu
							PM_SHOW_REPEAT_PLAY_HELP(FALSE, CP_GROUP_MISSIONS)
							
							// hide stats and missions
							PM_SHOW_COLUMN(REPEAT_COLUMN_STATS, FALSE)
							IF NOT (args.MenuScreenId = REPEAT_MENU_ID_SAVE_GAME_LIST)
							AND NOT (args.MenuScreenId = REPEAT_MENU_ID_PROCESS_SAVEGAME_LIST)
								PM_SHOW_COLUMN(REPEAT_COLUMN_MISSIONS, FALSE)
							ENDIF
							
							// reset the mission type selection as we're no longer in the main section of the menu
							g_eMenuMissionType = CP_GROUP_NO_GROUP
							UPDATE_LAST_SELECTED_ITEM(-1)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK

		// Selection events
		CASE kTriggerEvent
			//CPRINTLN(DEBUG_PAUSE_MENU, "Event Triggered for ", args.MenuScreenId, " and ", args.UniqueIdentifier)
			SWITCH args.MenuScreenId
			
				// --------------Player is coming from top level "Game" menu -----------
				CASE REPEAT_MENU_ID_GAME
					SWITCH g_eMenuMissionType

						// ------------Player currently has "Replay Mission" highlighted in the left column of menu
						CASE CP_GROUP_MISSIONS
							CPRINTLN(DEBUG_PAUSE_MENU, 	"Replay mission highlighted coming from top level Game menu.")
							SHOW_ENTRY_SCREEN(CP_GROUP_MISSIONS, TRUE)
						BREAK
						
						// ------------Player currently has "Replay Random Character" highlighted in the left column of menu
						CASE CP_GROUP_RANDOMCHARS
							CPRINTLN(DEBUG_PAUSE_MENU, 	"Replay RC highlighted coming from top level Game menu.")
							SHOW_ENTRY_SCREEN(CP_GROUP_RANDOMCHARS, TRUE)
						BREAK		
					ENDSWITCH
				BREAK

				// --------------Player has selected "Replay Mission" from left column of menu -----------
				CASE REPEAT_MENU_ID_REPLAY_MISSION
					CPRINTLN(DEBUG_PAUSE_MENU, 	"Entered repeat play mission menu.")
					ENTER_REPEAT_PLAY_MENU(CP_GROUP_MISSIONS)
				BREAK
				
				// --------------Player has selected "Replay Random Character" from left column of menu -----------
				CASE REPEAT_MENU_ID_REPLAY_RC
					CPRINTLN(DEBUG_PAUSE_MENU, 	"Entered repeat play RC menu.")
					ENTER_REPEAT_PLAY_MENU(CP_GROUP_RANDOMCHARS)
				BREAK
			
				// --------Player has selected mission to replay ----------
				CASE REPEAT_MENU_ID_MISSIONS
					IF g_iLastSelectedPauseMenuMission <> -1
						CPRINTLN(DEBUG_PAUSE_MENU, 	"Selected a mission to repeat play!")
						
						//Mission item selected. Trigger a repeat play.
						#IF IS_DEBUG_BUILD
							tMissionName = GetRepeatPlayMissionName(g_iLastSelectedPauseMenuMission, g_eMenuMissionType)
							CPRINTLN(DEBUG_PAUSE_MENU, "Selected to launch mission repeat for mission: [", g_iLastSelectedPauseMenuMission, ", ", GET_STRING_FROM_TEXT_FILE(tMissionName), "].")
						#ENDIF
						
						//Tell the mission repeat controller to start a repeat.
						LaunchRepeatPlay(g_iLastSelectedPauseMenuMission, g_eMenuMissionType)
						UPDATE_LAST_SELECTED_ITEM(-1)
						
						//Close the pause menu to let the repeat trigger.
						SET_FRONTEND_ACTIVE(FALSE) 
						DO_SCREEN_FADE_OUT(0)
						REPLAY_CONTROL_SHUTDOWN()
						
						// store vehicle velocity before pause so we can restore it after repeat play
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								g_vRPVelocity = GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							ENDIF
						ENDIF
						// make the player invincible so he doesn't die during the 
						// few frames that we're waiting for repeat play to launch
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_PAUSE_MENU, 	"Ignoring attempt to repeat play mission -1!")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	TERMINATE_THIS_THREAD()
ENDSCRIPT
