/***********************************
*	Name: PauseMenu_MP_Public.sch
*	Author: James Adwick
*	Date: 05/11/2012
*	Purpose: Common functions for 
*			MP pause menu screens
***********************************/

USING "globals.sch"
USING "PauseMenu_public.sch"
USING "fmmc_cloud_loader.sch"
USING "commands_zone.sch"
USING "mp_scaleform_functions_xml.sch"
USING "net_time_trials.sch"
USING "DLC_Content_Controller.sch"
#IF FEATURE_GEN9_EXCLUSIVE
USING "net_hsw_time_trials.sch"
#ENDIF 

ENUM PAUSE_MENU_MP_GLOBAL_STATE
	PAUSE_MENU_GLOBAL_STATE_INIT = 0,
	PAUSE_MENU_GLOBAL_STATE_DISPLAYING,
	PAUSE_MENU_GLOBAL_STATE_LOAD_PLAYLIST_FOR_EDIT,
	PAUSE_MENU_GLOBAL_STATE_EDIT_PLAYLIST,
	PAUSE_MENU_GLOBAL_STATE_SAVE_PLAYLIST,
	PAUSE_MENU_GLOBAL_STATE_DELETE_PLAYLIST,
	PAUSE_MENU_GLOBAL_STATE_DELETE_PLAYLIST_BOOKMARK,
	PAUSE_MENU_GLOBAL_STATE_CLOUD_WARNING_MESSAGE,
	PAUSE_MENU_GLOBAL_STATE_WARNING_MESSAGE,
	PAUSE_MENU_GLOBAL_STATE_COLUMN_WARNING_MESSAGE,
	PAUSE_MENU_GLOBAL_STATE_PUBLISH_MISSION,
	PAUSE_MENU_GLOBAL_STATE_NOT_AVAILABLE,
	PAUSE_MENU_GLOBAL_STATE_QUICK_PLAY_JOIN,
	PAUSE_MENU_GLOBAL_STATE_QUALIFYING_PLAYLIST_JOIN,
	PAUSE_MENU_GLOBAL_STATE_EVENT_PLAYLIST_JOIN,
	PAUSE_MENU_GLOBAL_STATE_COLUMN_NAME_CHALLENGE,
	PAUSE_MENU_GLOBAL_STATE_ADD_BOOKMARK,
	PAUSE_MENU_GLOBAL_STATE_DELETE_BOOKMARK,
	PAUSE_MENU_GLOBAL_STATE_REPORT_UGC,
	PAUSE_MENU_GLOBAL_STATE_CLEANUP,
	PAUSE_MENU_GLOBAL_STATE_ACCOUNT_PICKER,
	PAUSE_MENU_GLOBAL_STATE_MIGRATION_STATUS
//	PAUSE_MENU_RUNNING_STARTER_PACK_BROWSER
ENDENUM

PAUSE_MENU_MP_GLOBAL_STATE pauseMenuGlobalState


// *********** SCALEFORM FUNCTIONS ***********

/// PURPOSE:
///    Sets up a column to be shown in the PauseMenu Scaleform
///    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    pszLabel - 	Stringtable identifier to display.
PROC MP_SET_DATA_SLOT(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel, BOOL bRockstarVerified = FALSE, INT iMissionType = 0, BOOL bPlayed = FALSE)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)	//-1) // active or not
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel) // text label

		IF bRockstarVerified
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // rockstar verified or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // rockstar verified or not
		ENDIF
		
		IF bPlayed
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(21) // VALID ICON
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bPlayed)
			
		ENDIF

		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMissionType) // mission type

		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] MP_SET_DATA_SLOT - Column ", iColumn, " Index ", iMenuIndex)
ENDPROC


/// PURPOSE: Set up a row in pause menu with setting options
PROC MP_SET_DATA_SLOT_SETTING(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING pszLabel, STRING tlOption)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlOption)
	
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

/// PURPOSE: Update a row in the pause menu for a setting option
PROC MP_UPDATE_DATA_SLOT_SETTING(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING pszLabel, STRING tlOption)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("UPDATE_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlOption)
	
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

/// PURPOSE: Set up a row in pause menu with number options
PROC MP_SET_DATA_SLOT_SETTING_INT(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING pszLabel, INT iNumber, BOOL bActive = TRUE)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		
		//**TWH - CMcM - #1416411 - Greyed out and disabled Crew options when the player didn’t have a crew. Had to update MP_SET_DATA_SLOT_SETTING_INT to allow inactive entries.
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)	//-1) // active or not
		ENDIF	
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	
		TEXT_LABEL_15 tlNumber = "NUMBER"
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlNumber)
			ADD_TEXT_COMPONENT_INTEGER(iNumber)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

/// PURPOSE: Update a row in pause menu with number options
PROC MP_UPDATE_DATA_SLOT_SETTING_INT(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING pszLabel, INT iNumber)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("UPDATE_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
		TEXT_LABEL_15 tlNumber = "NUMBER"
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlNumber)
			ADD_TEXT_COMPONENT_INTEGER(iNumber)
		END_TEXT_COMMAND_SCALEFORM_STRING()

		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

/// PURPOSE: Set up a row in pause menu with setting options
PROC MP_SET_DATA_SLOT_SETTING_PANEL(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, INT iType, STRING pszLabel, STRING tlOption, BOOL bActive = TRUE, FLOAT fOptionFloat=-1.0)

	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] MP_SET_DATA_SLOT_SETTING_PANEL - iMenuIndex ", iMenuIndex)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iType) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2) // active or not
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel)
		IF NOT IS_STRING_NULL_OR_EMPTY(tlOption)
			IF DOES_TEXT_LABEL_EXIST(tlOption)
				PRINTLN("MP_UPDATE_DATA_SLOT_SETTING_PANEL exists tlOption = ", tlOption)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlOption)
					IF fOptionFloat>-1.0
						IF ARE_STRINGS_EQUAL(tlOption, "PM_CASH")
							ADD_TEXT_COMPONENT_FLOAT(fOptionFloat, 0)
						ELSE
							ADD_TEXT_COMPONENT_FLOAT(fOptionFloat, 2)
						ENDIF
					ENDIF
				END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
			ELSE
				PRINTLN("MP_UPDATE_DATA_SLOT_SETTING_PANEL tlOption = ", tlOption)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRTNM1")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlOption)
				END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
			ENDIF
		ENDIF
	
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

/// PURPOSE: Update a row in the pause menu for a setting option
PROC MP_UPDATE_DATA_SLOT_SETTING_PANEL(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, INT iType, STRING pszLabel, STRING tlOption, BOOL bActive = TRUE, FLOAT fOptionFloat=-1.0)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("UPDATE_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iType) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) // active or not
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel)
		IF NOT IS_STRING_NULL_OR_EMPTY(tlOption)
			IF DOES_TEXT_LABEL_EXIST(tlOption)
				PRINTLN("MP_UPDATE_DATA_SLOT_SETTING_PANEL exists tlOption = ", tlOption)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlOption)
					IF fOptionFloat>-1.0
						IF ARE_STRINGS_EQUAL(tlOption, "PM_CASH")
							ADD_TEXT_COMPONENT_FLOAT(fOptionFloat, 0)
						ELSE
							ADD_TEXT_COMPONENT_FLOAT(fOptionFloat, 2)
						ENDIF
					ENDIF
				END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
			ELSE
				PRINTLN("MP_UPDATE_DATA_SLOT_SETTING_PANEL tlOption = ", tlOption)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRTNM1")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlOption)	
				END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
			ENDIF
		ENDIF
	
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

/// PURPOSE:
///    Updates a column to be shown in the PauseMenu Scaleform
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    pszLabel - 	Stringtable identifier to display.
PROC MP_UPDATE_DATA_SLOT(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel, BOOL bRockstarVerified = FALSE, INT iMissionType = 0)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("UPDATE_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)	//-1) // active or not
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel) // text label
		
		IF bRockstarVerified
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // rockstar verified or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // rockstar verified or not
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMissionType) // mission type
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


FUNC INT MP_GET_MISSION_ICON_FOR_PAUSE_MENU(INT iMissionType, INT iMissionSubType, INT iAdversaryModeType)
	INT iMissionTypeIcon = iMissionType
	IF iMissionTypeIcon = -1
		iMissionTypeIcon = 0	
	ELIF iMissionTypeIcon = FMMC_TYPE_BASE_JUMP
		iMissionTypeIcon = 9
	ELIF iMissionTypeIcon = FMMC_TYPE_MISSION_CTF
		iMissionTypeIcon = 5
	ELIF iMissionTypeIcon = FMMC_TYPE_SURVIVAL
		iMissionTypeIcon = 4
	ELIF iMissionTypeIcon = FMMC_TYPE_MISSION
		IF iMissionSubType = FMMC_MISSION_TYPE_LTS //FMMC_TYPE_MISSION_LTS
			iMissionTypeIcon = 16
		ELIF iMissionSubType = FMMC_MISSION_TYPE_CTF //FMMC_TYPE_MISSION_CTF
			iMissionTypeIcon = 17
		ELSE
			iMissionTypeIcon = 1
		ENDIF
	ELIF iMissionTypeIcon = FMMC_TYPE_DEATHMATCH
		IF iMissionSubType = FMMC_DEATHMATCH_TEAM
			iMissionTypeIcon = 5
		ELIF iMissionSubType = FMMC_DEATHMATCH_VEHICLE
			iMissionTypeIcon = 10
		ELIF iMissionSubType = FMMC_KING_OF_THE_HILL
		OR iMissionSubType = FMMC_KING_OF_THE_HILL_TEAM
			iMissionTypeIcon = 24 // itempiconforpausemenu // itempiconforpausemenu
		ELSE
			iMissionTypeIcon = 2
		ENDIF
	ELIF iMissionTypeIcon = FMMC_TYPE_RACE
		
		IF iMissionSubType = FMMC_RACE_TYPE_STANDARD
		OR iMissionSubType = FMMC_RACE_TYPE_P2P
			iMissionTypeIcon = 11
		ELIF iMissionSubType = FMMC_RACE_TYPE_STUNT
		OR iMissionSubType = FMMC_RACE_TYPE_STUNT_P2P
			iMissionTypeIcon = 20
			
		ELIF iMissionSubType = FMMC_RACE_TYPE_TARGET
		OR iMissionSubType = FMMC_RACE_TYPE_TARGET_P2P
			iMissionTypeIcon = 22
		ELIF iMissionSubType = FMMC_RACE_TYPE_ON_FOOT
		OR iMissionSubType = FMMC_RACE_TYPE_ON_FOOT_P2P
			iMissionTypeIcon = 12
		ELIF iMissionSubType = FMMC_RACE_TYPE_BIKE_AND_CYCLE
		OR iMissionSubType = FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P
			iMissionTypeIcon = 13
		ELIF iMissionSubType = FMMC_RACE_TYPE_BOAT
		OR iMissionSubType = FMMC_RACE_TYPE_BOAT_P2P
			iMissionTypeIcon = 14
		ELIF iMissionSubType = FMMC_RACE_TYPE_AIR
		OR iMissionSubType = FMMC_RACE_TYPE_AIR_P2P
			iMissionTypeIcon = 15
		ELIF iMissionSubType = FMMC_RACE_TYPE_OPEN_WHEEL
		OR iMissionSubType = FMMC_RACE_TYPE_OPEN_WHEEL_P2P
			iMissionTypeIcon = 26
		ELIF iMissionSubType = FMMC_RACE_TYPE_PURSUIT
			iMissionTypeIcon = 30
		ELIF iMissionSubType = FMMC_RACE_TYPE_STREET
			iMissionTypeIcon = 31
		#IF FEATURE_GEN9_EXCLUSIVE
		ELIF iMissionSubType = FMMC_RACE_TYPE_HSW
		OR iMissionSubType = FMMC_RACE_TYPE_HSW_P2P
			iMissionTypeIcon = 11 //to be updated if/when we get a new icon
		#ENDIF
		ELSE
			iMissionTypeIcon = 3
		ENDIF
	ENDIF

	BOOL bisArenawar  = IS_THIS_AN_ARENA_WAR_ACTIVITY( iMissionType, iMissionSubType, iAdversaryModeType)

	IF bisArenawar
		iMissionTypeIcon = 23
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF itempiconforpausemenu != 0
			iMissionTypeIcon = itempiconforpausemenu
		ENDIF
	#ENDIF

	RETURN iMissionTypeIcon
ENDFUNC

/// PURPOSE:
///    Sets up a column to be shown in the PauseMenu Scaleform with literal string
///    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    pszLabel - 	Stringtable identifier to display.
PROC MP_SET_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD INT iLesRating ,#ENDIF INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel, BOOL bRockstarCreated = FALSE, INT iMissionType = -1, INT iMissionSubType = -1, BOOL bIsMissionComplete = FALSE, BOOL bRockstarVerified = FALSE, INT iAdversaryModeType = 0)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)	//-1) // active or not
		ENDIF
		
		TEXT_LABEL_63 tlTemp
		#IF IS_DEBUG_BUILD
		IF iLesRating > 0
			tlTemp = "("
			tlTemp += iLesRating
			tlTemp += ") "
		ENDIF
		#ENDIF
		
		tlTemp += pszLabel
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlTemp) // text label
		
		//bRockstarVerified = TRUE
		
		IF bRockstarCreated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // rockstar verified or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // rockstar verified or not
		ENDIF
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_SET_DATA_SLOT_WITH_STRING pszLabel = ", pszLabel)
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_SET_DATA_SLOT_WITH_STRING bRockstarVerified = ", bRockstarVerified)
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_SET_DATA_SLOT_WITH_STRING iMissionType = ", iMissionType)
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_SET_DATA_SLOT_WITH_STRING iMissionSubType = ", iMissionSubType)
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_SET_DATA_SLOT_WITH_STRING iAdversaryModeType = ", iAdversaryModeType)
		
		INT iMissionTypeIcon = MP_GET_MISSION_ICON_FOR_PAUSE_MENU(iMissionType, iMissionSubType, iAdversaryModeType)
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_SET_DATA_SLOT_WITH_STRING iMissionTypeIcon = ", iMissionTypeIcon)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMissionTypeIcon) // mission type
	
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		IF bRockstarVerified OR bRockstarCreated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_BLUE))
		ELSE
			IF iMissionType = -1
			AND iMissionSubType = -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_PURE_WHITE))
			ENDIF
		ENDIF

		
		// ** THESE PARAMS ARE UNUSED FOR THIS TYPE OF MENU ITEM **
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		// *************************************************
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsMissionComplete)

		
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up a column to be shown in the PauseMenu Scaleform with literal string
///    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    pszLabel - 	Stringtable identifier to display.
PROC MP_UPDATE_DATA_SLOT_WITH_STRING(#IF IS_DEBUG_BUILD INT iLesRating ,#ENDIF INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel, BOOL bRockstarCreated = FALSE, INT iMissionType = -1, INT iMissionSubType = -1, BOOL bIsMissionComplete = FALSE, BOOL bRockstarVerified = FALSE, INT iAdversaryModeType = 0)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("UPDATE_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)	//-1) // active or not
		ENDIF
		
		TEXT_LABEL_63 tlTemp
		#IF IS_DEBUG_BUILD
		IF iLesRating > 0
			tlTemp = "("
			tlTemp += iLesRating
			tlTemp += ") "
		ENDIF
		#ENDIF
		
		tlTemp += pszLabel
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlTemp) // text label
		
		IF bRockstarCreated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // rockstar verified or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // rockstar verified or not
		ENDIF
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_UPDATE_DATA_SLOT_WITH_STRING pszLabel = ", pszLabel)
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_UPDATE_DATA_SLOT_WITH_STRING bRockstarVerified = ", bRockstarVerified)
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_UPDATE_DATA_SLOT_WITH_STRING iMissionType = ", iMissionType)
		
		INT iMissionTypeIcon = MP_GET_MISSION_ICON_FOR_PAUSE_MENU(iMissionType, iMissionSubType, iAdversaryModeType)
		CPRINTLN(DEBUG_PAUSE_MENU, "------------------->>>>>>>>>>MP_UPDATE_DATA_SLOT_WITH_STRING iMissionTypeIcon = ", iMissionTypeIcon)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMissionTypeIcon) // mission type
	
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		IF bRockstarVerified OR bRockstarCreated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_BLUE))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_PURE_WHITE))
		ENDIF

		
		// ** THESE PARAMS ARE UNUSED FOR THIS TYPE OF MENU ITEM **
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		// *************************************************
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsMissionComplete)
		
		
		
		
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up mission stat (string format) for third column of pause menu
/// PARAMS:
///    iColumn - 		Which column, probably 0,1,2
///    iMenuIndex - 	Which item in the list, probably 0-9
///    iUniqueId - 		Identifier for this menuitem (can be anything, really)
///    bRating - 		TRUE if stat is a rating for a mission (shows thumb)
///    stStatTitle - 	Text label for name of stat
///    stStatValue - 	Text lable of stat value
///    fRating - 		Rating value of mission
///    bRealName - 		If stat is a literal string rather than text label
PROC PM_SET_DATA_SLOT_FOR_STAT(	INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bRating, STRING stStatTitle, STRING stStatValue, 
								INT iRating = 0, BOOL bRealName = FALSE, BOOL bCreator = FALSE, BOOL bSCNickname = FALSE)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		
		IF bRating
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // Special case (thumb)
		ELSE
			IF bCreator
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)		// Correct font
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			ENDIF
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) // stat name
				
		IF bRealName
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(stStatValue)	// Literal string
		ELSE
			IF bRating
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(stStatValue) // stat value is float for rating
					ADD_TEXT_COMPONENT_INTEGER(iRating)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatValue) // stat value is just text label
			ENDIF
		ENDIF
		
		BOOL bCrewTag
		IF bCreator
		//	TEXT_LABEL_15 tlCreatorCrewTag
			IF ARE_STRINGS_EQUAL(stStatValue, GET_ROCKSTAR_CREATED_USER_STRING())
				PRINTLN("PM_SET_DATA_SLOT_FOR_STAT - ARE_STRINGS_EQUAL = ", stStatValue)
			ENDIF
			IF ARE_STRINGS_EQUAL(stStatValue, GET_ROCKSTAR_VERIFIED_USER_STRING())
				PRINTLN("PM_SET_DATA_SLOT_FOR_STAT - ARE_STRINGS_EQUAL = ", stStatValue)
			ENDIF
			IF ARE_STRINGS_EQUAL(stStatValue, "Verified")
				PRINTLN("PM_SET_DATA_SLOT_FOR_STAT - ARE_STRINGS_EQUAL = ", stStatValue)
			ENDIF
			IF COMPARE_STRINGS(stStatValue, GET_ROCKSTAR_VERIFIED_USER_STRING(), FALSE, 10) = 0
				PRINTLN("PM_SET_DATA_SLOT_FOR_STAT - COMPARE_STRINGS = ", stStatValue)
			ENDIF
			IF IS_STRING_EMPTY_HUD(stStatValue)
				PRINTLN("PM_SET_DATA_SLOT_FOR_STAT - IS_STRING_EMPTY_HUD = ", stStatValue)
			ENDIF
//			IF NOT ARE_STRINGS_EQUAL(stStatValue, GET_ROCKSTAR_CREATED_USER_STRING())
//			AND NOT ARE_STRINGS_EQUAL(stStatValue, GET_ROCKSTAR_VERIFIED_USER_STRING())
//			AND NOT ARE_STRINGS_EQUAL(stStatValue, "Verified")
//			AND COMPARE_STRINGS(stStatValue, "Verified", FALSE, 8) != 0
//			AND COMPARE_STRINGS(stStatValue, GET_ROCKSTAR_VERIFIED_USER_STRING(), FALSE, 10) != 0
//			//**TWH - CMcM - #1394125 - I think this assert is because stStatValue is an empty string. I'm not sure, though. 
//			AND NOT IS_STRING_EMPTY_HUD(stStatValue)
//			
//				PRINTLN("PM_SET_DATA_SLOT_FOR_STAT - stStatValue = ", stStatValue)
//				IF !IS_PC_VERSION() AND !IS_PLAYSTATION_PLATFORM()
//					GAMER_HANDLE thisHandle
//					thisHandle = GET_GAMER_HANDLE_USER(stStatValue)
//					IF IS_GAMER_HANDLE_VALID(thisHandle)
//						IF IS_PLAYER_IN_ACTIVE_CLAN(thisHandle)
//							GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(thisHandle, tlCreatorCrewTag)
//							//SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(tlCreatorCrewTag)							// Crewtag
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlCreatorCrewTag)
//							bCrewTag = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//				
//			ENDIF
		ENDIF
		
		IF NOT bCrewTag
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bSCNickname)
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets up mission stat (string format) for third column of pause menu
/// PARAMS:
///    iColumn - 		Which column, probably 0,1,2
///    iMenuIndex - 	Which item in the list, probably 0-9
///    iUniqueId - 		Identifier for this menuitem (can be anything, really)
///    bRating - 		TRUE if stat is a rating for a mission (shows thumb)
///    stStatTitle - 	Text label for name of stat
///    stStatValue - 	Text lable of stat value
///    fRating - 		Rating value of mission
///    bRealName - 		If stat is a literal string rather than text label
PROC PM_SET_DATA_SLOT_FOR_TYPE_STAT(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING stStatTitle, STRING stStatValue, INT iMissionType, INT iIconColour, BOOL bPlayed)
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) 		// depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) 	// imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)		// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) 	// unique id
		
		//IF iMissionType = FMMC_TYPE_DEATHMATCH
		//OR iMissionType = FMMC_TYPE_RACE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)		// Type has a valid icon
		//ELSE
		//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		//ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) // stat name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatValue) // stat value is just text label
		
		/*IF iMissionType = FMMC_TYPE_DEATHMATCH
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)		// DM type
		ELIF iMissionType = FMMC_TYPE_RACE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)		// Race type
		ENDIF*/
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMissionType)
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PM_SET_DATA_SLOT_FOR_TYPE_STAT - iMissionType ", iMissionType)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconColour)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bPlayed)
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets up a mission stat containing an int value for third column
/// PARAMS:
///    iColumn - 		Which column, probably 0,1,2
///    iMenuIndex - 	Which item in the list, probably 0-9
///    iUniqueId - 		Identifier for this menuitem (can be anything, really)
///    stStatTitle - 	Text label for name of stat
///    stStatText - 	Text label of stat value (must contain ~1~)
///    iInt - 			Value of stat
PROC PM_SET_DATA_SLOT_FOR_STAT_WITH_NUMBER(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING stStatTitle, STRING stStatText, INT iInt)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) // stat name
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(stStatText) // Show min - max style
			ADD_TEXT_COMPONENT_INTEGER(iInt)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up a mission stat containing a time value for third column
/// PARAMS:
///    iColumn - 		Which column, probably 0,1,2
///    iMenuIndex - 	Which item in the list, probably 0-9
///    iUniqueId - 		Identifier for this menuitem (can be anything, really)
///    stStatTitle - 	Text label for name of stat
///    stStatText - 	Text label of stat value (must contain ~a~)
///    iInt - 			Value of stat
PROC PM_SET_DATA_SLOT_FOR_STAT_WITH_TIME(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING stStatTitle, STRING stStatText, INT iInt)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) // stat name
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(stStatText) // Show min - max style
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(iInt, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER|TEXT_FORMAT_HIDE_MILLISECONDS_UNITS_DIGIT)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC



PROC PM_SET_DATA_SLOT_FOR_STAT_WITH_TWO_NUMBERS(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING stStatTitle, STRING stStatText, INT iInt, INT iInt2)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) // stat name
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(stStatText) // Show min - max style
			ADD_TEXT_COMPONENT_INTEGER(iInt)
			ADD_TEXT_COMPONENT_INTEGER(iInt2)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC MP_SET_DATA_SLOT_SETTING_YES_NO(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING stStatTitle, BOOL bYes, BOOL bUnlocks = FALSE)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) 		// depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) 	// imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)		// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) 	// unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) 	// stat name
		
		IF bYes = TRUE
			IF bUnlocks = FALSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PM_UJOB_YES") 	// Yes
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PM_UCON_ULK") 	// Unlocked
			ENDIF
		ELSE
			IF bUnlocks = FALSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PM_UJOB_NO") 	// No
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PM_UCON_LCK") 	// Unlocked
			ENDIF
		ENDIF
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets number of players stat for mission details in third column
/// PARAMS:
///    iColumn - 		Which column, probably 0,1,2
///    iMenuIndex - 	Which item in the list, probably 0-9
///    iUniqueId - 		Identifier for this menuitem (can be anything, really)
///    stStatTitle - 	Text label for name of stat
///    iMinPlayers - 	Value of the minimum players
///    iMaxPlayers - 	Value of the maximum players
PROC PM_SET_DATA_SLOT_FOR_PLAYERS(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING stStatTitle, INT iMinPlayers, INT iMaxPlayers)
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) // stat name

		IF iMinPlayers != iMaxPlayers
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_PLAYERS_V") // Show min - max style
				ADD_TEXT_COMPONENT_INTEGER(iMinPlayers)
				ADD_TEXT_COMPONENT_INTEGER(iMaxPlayers)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER") // They are the same so just set to number of players
				ADD_TEXT_COMPONENT_INTEGER(iMaxPlayers)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets up a warning message in the columns.
/// PARAMS:
///    iColumn - 	Which column, probably 0,1,2
///    iMenuIndex - Which item in the list, probably 0-9
///    iMenuId - 	Which menu this is considered
///    iUniqueId - 	Identifier for this menuitem (can be anything, really)
///    bActive - 	Appears grayed out or not
///    pszLabel - 	Stringtable identifier to display.
PROC MP_SET_COLUMN_WARNING_MESSAGE(BOOL bShow, INT iColumnIndex, INT iColumnSpan, STRING sBody, STRING sTitle, INT iBackground, STRING sTextureDict, STRING sTextureName, INT iInsertionInt = -1, INT ilayout = 0, BOOL bUseliteralstring = FALSE )
	DEBUG_PRINTCALLSTACK()
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SHOW_WARNING_MESSAGE")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bShow)						// Show
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumnIndex) 					// Start Column
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumnSpan) 					// Column Span
		
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sBody) 					// Body Text
		

		
		IF bUseliteralstring
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sBody)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sTitle) 
			
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sBody)
				IF iInsertionInt > -1
					ADD_TEXT_COMPONENT_INTEGER(iInsertionInt)
				ENDIF
			END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitle) 					// Title Text
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBackground) 					// Background Type (0 = Rockstar, 1 = Social Club)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureDict) 	// Texture Dictionary
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureName) 	// Texture Name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ilayout) 					// ilayout
		IF bUseliteralstring
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING("") 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)	
		ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	SET_PM_WARNINGSCREEN_ACTIVE(bShow)
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] MP_SET_COLUMN_WARNING_MESSAGE - CALLED - ", bShow, " - Title ", sTitle)
ENDPROC

// Returns text label based on type of mission
FUNC STRING GET_MISSION_STRING_FOR_PAUSEMENU(INT iMissionType)
	
	IF iMissionType = FMMC_TYPE_DEATHMATCH
		RETURN "FMMC_MPM_TY1"
	ELIF iMissionType = FMMC_TYPE_RACE
		RETURN "FMMC_MPM_TY2"
	ELIF iMissionType = FMMC_TYPE_GANGHIDEOUT
		RETURN "FMMC_MPM_TY5"
	ELIF iMissionType = FMMC_TYPE_SURVIVAL
		RETURN "FMMC_MPM_TY4"
	ELIF iMissionType = FMMC_TYPE_BASE_JUMP
		RETURN "FMMC_MPM_TY8"
	ENDIF
	
	RETURN "FMMC_MPM_TY0"	//Mission
ENDFUNC


PROC PM_MENU_SHIFT_DEPTH(INT iDirection, BOOL bAllowExitIfAtTop = FALSE)

	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PM_MENU_SHIFT_DEPTH, iDirection = ", iDirection, ", bAllowExitIfAtTop = ", bAllowExitIfAtTop)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("MENU_SHIFT_DEPTH")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDirection)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(!bAllowExitIfAtTop)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

// Scaleform functions to set the column title and show / hide columns
PROC PM_SET_COLUMN_TITLE(INT iColumn, STRING sTitle, STRING sMissionName, BOOL bVerified, STRING sTxdDict = NULL, STRING sTxdName = NULL, INT iLoadInt = -1)
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sMissionName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVerified)
		IF NOT IS_STRING_NULL_OR_EMPTY(sTxdDict)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTxdDict)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("")
		ENDIF
		IF NOT IS_STRING_NULL_OR_EMPTY(sTxdName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTxdName)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("")
		ENDIF
		IF iLoadInt != -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLoadInt)
		ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	//CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PM_SET_COLUMN_TITLE - sTxdDict = ", sTxdDict)
	//CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PM_SET_COLUMN_TITLE - sTxdName = ", sTxdName)
ENDPROC

// Scaleform functions to set the column title and show / hide columns
PROC PM_SET_COLUMN_TITLE_ONLY(INT iColumn, STRING sTitle, STRING sInsertText)
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PM_SET_COLUMN_TITLE_ONLY: ", sTitle)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sTitle)
			IF NOT ARE_STRINGS_EQUAL(sInsertText, " ")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sInsertText)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC PM_ALLOW_CLICK_FROM_COLUMN(INT iColumn, BOOL bCanEnterViaColumnToColumn)
	
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PM_ALLOW_CLICK_FROM_COLUMN, iColumn = ", iColumn, ", bCanEnterViaColumnToColumn = ", PICK_STRING(bCanEnterViaColumnToColumn, "TRUE", "FALSE"))

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("ALLOW_CLICK_FROM_COLUMN")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanEnterViaColumnToColumn)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC

PROC CLEAR_MISSION_CREATOR_COLUMNS()
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
	ENDIF END_SCALEFORM_MOVIE_METHOD()
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF END_SCALEFORM_MOVIE_METHOD()	
ENDPROC

PROC CLEAR_PM_COLUMN(INT iColumn)

	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Clearing column: ", iColumn)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
	ENDIF END_SCALEFORM_MOVIE_METHOD()	
ENDPROC

PROC CLEAR_MISSIONS_COLUMN(INT iColumn)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
	ENDIF END_SCALEFORM_MOVIE_METHOD()	
ENDPROC

PROC CLEAR_MISSION_DETAILS_COLUMN()
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF END_SCALEFORM_MOVIE_METHOD()	
ENDPROC

PROC PRIVATE_SHOW_PM_COLUMN(INT iColumn, BOOL bShow)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SHOW_COLUMN")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bShow)		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF iColumn = 2//THIRD_COLUMN
		IF bShow
			CLEAR_BIT(iBS_PM_ColumnHidden, iColumn)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PRIVATE_SHOW_PM_COLUMN - iBS_PM_ColumnHidden - TRUE - ", iColumn)
		ELSE
			SET_BIT(iBS_PM_ColumnHidden, iColumn)
			CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] PRIVATE_SHOW_PM_COLUMN - iBS_PM_ColumnHidden - FALSE - ", iColumn)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PAUSE_MENU_COLUMN_HIDDEN(INT iColumn)
	RETURN IS_BIT_SET(iBS_PM_ColumnHidden, iColumn)
ENDFUNC
	
	
/// PURPOSE: Locks column in pause menu so highlight won't move to another column
PROC LOCK_PM_COLUMN(INT iColumn, BOOL bLock)

	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] LOCK_PM_COLUMN col ", iColumn, " bLock ", bLock)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_CAN_JUMP")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(!bLock)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC SHOW_PM_COLUMN(INT iColumn)
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Show column ", iColumn)
	DEBUG_PRINTCALLSTACK() 

	PRIVATE_SHOW_PM_COLUMN(iColumn, TRUE)
ENDPROC

PROC HIDE_PM_COLUMN(INT iColumn)
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] Hide column ", iColumn)
	DEBUG_PRINTCALLSTACK()
	
	PRIVATE_SHOW_PM_COLUMN(iColumn, FALSE)
ENDPROC

/// PURPOSE: Sets a highlight of a select
PROC SET_PM_HIGHLIGHT(INT iColumn, INT iIndex = 0, BOOL bAlwayaFalse = FALSE, BOOL bSetFocus = FALSE)
	CPRINTLN(DEBUG_PAUSE_MENU, "SET_PM_HIGHLIGHT iColumn = ", iColumn, " iIndex = ", iIndex)

	IF iIndex = -1
		CPRINTLN(DEBUG_PAUSE_MENU, "BAILED ON IT BECAUSE IT'S -1!")
		EXIT
	ENDIF
	
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_HIGHLIGHT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIndex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAlwayaFalse)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bSetFocus)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

// Returns text label based on type of mission
FUNC STRING GET_MISSION_DETAIL_STRING_FOR_PAUSE_MENU(INT iMissionType, INT iMissionSubType = -1)
	IF iMissionType = FMMC_TYPE_DEATHMATCH
		IF (iMissionSubType = FMMC_KING_OF_THE_HILL OR iMissionSubType = FMMC_KING_OF_THE_HILL_TEAM)
			RETURN "PM_DETAILS_Q"
		ELSE
			RETURN "PM_DETAILS_D"
		ENDIF		
	ELIF iMissionType = FMMC_TYPE_RACE
		RETURN "PM_DETAILS_R"
	ELIF iMissionType = FMMC_TYPE_SURVIVAL
		RETURN "PM_DETAILS_S"
	ELIF iMissionType = FMMC_TYPE_GANGHIDEOUT
		RETURN "PM_DETAILS_G"
	ELIF iMissionType = FMMC_TYPE_MISSION
		RETURN "PM_DETAILS_M"
	ELIF iMissionType = FMMC_TYPE_BASE_JUMP
		RETURN "PM_DETAILS_P"
	ENDIF
	
	RETURN "PM_DETAILS"
ENDFUNC



FUNC STRING GET_CRIMINAL_ENTERPRISE_STARTER_PACK_WHERE_TO_PURCHASE(INT pauseMenuRoute1, INT menuindex)
	SWITCH pauseMenuRoute1
	
	
		CASE 0 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_ALL
			SWITCH menuindex
				CASE 0		RETURN    "PM_PREMWHR_DYN8"	BREAK   //Maze Bank West Executive Office
				CASE 1		RETURN    "PM_PREMWHR_MAZE"	BREAK	//Paleto Forest Bunker
				CASE 2		RETURN    "PM_PREMWHR_MAZE"	BREAK  //Great Chaparral Biker Clubhouse
				CASE 3		RETURN    "PM_PREMWHR_BIKAP" BREAK  //Senora Desert Counterfeit Cash Factory
				CASE 4		RETURN    "PM_PREMWHR_REAL"	BREAK  //1561 San Vitas Street
				CASE 5		RETURN    "PM_PREMWHR_REAL"	BREAK  //1337 Exceptionalists Way 10 Car Garage
				CASE 6		RETURN    "PM_PREMWHR_WAR"	BREAK   // Dune FAV
				CASE 7		RETURN    "PM_PREMWHR_LEG"	BREAK   // Turismo R
				CASE 8		RETURN    "PM_PREMWHR_LEG"	BREAK   // Banshee
				CASE 9		RETURN    "PM_PREMWHR_LEG"	BREAK   // Enus windsor
				CASE 10		RETURN    "PM_PREMWHR_LEG"	BREAK   // Coquette Classic
				CASE 11		RETURN    "PM_PREMWHR_LEG"	BREAK   // Huntley S
				CASE 12		RETURN    "PM_PREMWHR_SSA"	BREAK   // Obey Omnis
				CASE 13		RETURN    "PM_PREMWHR_SSA"	BREAK	// Pegassi Vortex
				CASE 14		RETURN    "PM_PREMWHR_SSA"	BREAK   // Western Zomvie Chopper
				CASE 15		RETURN    "PM_PREMWHR_ELI"	BREAK   // Maibatsu Frogger
				CASE 16		RETURN    "PM_PREMWHR_WEP"	BREAK
				CASE 17		RETURN    "PM_PREMWHR_WEP"	BREAK
				CASE 18		RETURN    "PM_PREMWHR_WEP"	BREAK
				CASE 19		RETURN    "PM_PREMWHR_CLT"	BREAK
				CASE 20		RETURN    "PM_PREMWHR_CLT"	BREAK
				CASE 21		RETURN    "PM_PREMWHR_CLT"	BREAK
				CASE 22		RETURN    "PM_PREMWHR_CLT"	BREAK
				CASE 23		RETURN    "PM_PREMWHR_TAT"	BREAK
			ENDSWITCH
		BREAK
	
		CASE 1 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_PROPERTY
			SWITCH menuindex
				CASE 0		RETURN    "PM_PREMWHR_DYN8"	BREAK   //Maze Bank West Executive Office
				CASE 1		RETURN    "PM_PREMWHR_MAZE"	BREAK	//Paleto Forest Bunker
				CASE 2		RETURN    "PM_PREMWHR_MAZE"	BREAK  //Great Chaparral Biker Clubhouse
				CASE 3		RETURN    "PM_PREMWHR_BIKAP" BREAK  //Senora Desert Counterfeit Cash Factory
				CASE 4		RETURN    "PM_PREMWHR_REAL"	BREAK  //1561 San Vitas Street
				CASE 5		RETURN    "PM_PREMWHR_REAL"	BREAK  //1337 Exceptionalists Way 10 Car Garage
			ENDSWITCH
		BREAK
		CASE 2 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_VEHICLES
			SWITCH menuindex		
				CASE 0		RETURN    "PM_PREMWHR_WAR"	BREAK   // Dune FAV
				CASE 1		RETURN    "PM_PREMWHR_LEG"	BREAK   // Turismo R
				CASE 2		RETURN    "PM_PREMWHR_LEG"	BREAK   // Banshee
				CASE 3		RETURN    "PM_PREMWHR_LEG"	BREAK   // Enus windsor
				CASE 4		RETURN    "PM_PREMWHR_LEG"	BREAK   // Coquette Classic
				CASE 5		RETURN    "PM_PREMWHR_LEG"	BREAK   // Huntley S
				CASE 6		RETURN    "PM_PREMWHR_SSA"	BREAK   // Obey Omnis
				CASE 7		RETURN    "PM_PREMWHR_SSA"	BREAK	// Pegassi Vortex
				CASE 8		RETURN    "PM_PREMWHR_SSA"	BREAK   // Western Zomvie Chopper
				CASE 9		RETURN    "PM_PREMWHR_ELI"	BREAK   // Maibatsu Frogger
			ENDSWITCH
		BREAK
		CASE 3 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_WEAPONS
			RETURN "PM_PREMWHR_WEP"
		BREAK
		
		
		CASE 4 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_RACE
			RETURN "PM_PREMWHR_CLT"
		BREAK
		
		CASE 5 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_BIKER
			RETURN "PM_PREMWHR_CLT"
		BREAK
		
		CASE 6 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_STUNT
			RETURN "PM_PREMWHR_CLT"
		BREAK
		
		CASE 7 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_IMPORT_EXPORT
			RETURN "PM_PREMWHR_CLT"
		BREAK
		
		CASE 8 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_TATTOOS
			RETURN "PM_PREMWHR_TAT"
		BREAK

	ENDSWITCH
	
	RETURN "PM_DETAILS"
ENDFUNC

FUNC CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT GET_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_ENUM_FROM_COLUMNS(INT pauseMenuRoute1, INT menuindex)

	SWITCH pauseMenuRoute1
		CASE 0 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_ALL
			SWITCH menuindex
					CASE 0		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE			break		
					CASE 1		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_PALETO_FOREST_BUNKER						break	
					CASE 2		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE			break	
					CASE 3		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_SENORA_DESERT_CONTERFEIT_CASH_FACTORY		break
					CASE 4		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT			break	
					CASE 5		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE		break
					CASE 6		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BF_DUNE_FAV									break
					CASE 7		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_GROTTI_TURISMO_R							break
					CASE 8		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BRAVADO_BANSHEE								break
					CASE 9		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_WINDSOR								break	
					CASE 10		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_INVETERO_COQUETTE_CLASSIC					break
					CASE 11		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_HUNTLEY_S								break	
					CASE 12		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_OBEY_OMNIS									break	
					CASE 13		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_PEGASSI_VORTEX								break
					CASE 14		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_WESTERN_ZOMBIE_CHOPPER						break	
					CASE 15		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_MAIBATSU_FROGGER							break
					CASE 16		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_WEAPON_MARKSMAN_RIFLE								break	
					CASE 17		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_WEAPON_COMPACT_RIFLE								break	
					CASE 18		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_WEAPON_COMPACT_GRENADE_LAUNCHER						break
					CASE 19		RETURN    CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_STUNT_RACE_ALL    	BREAK
					CASE 20		RETURN    CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_STUNT_RACE_ALL    	BREAK
					CASE 21		RETURN    CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_IMPORT_EXPORT_ALL   	BREAK
					CASE 22		RETURN    CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_IMPORT_EXPORT_ALL   	BREAK
					CASE 23		RETURN    CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_ALL     	BREAK
			
			
			ENDSWITCH
		BREAK
	
		CASE 2 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_VEHICLES
			SWITCH menuindex
				CASE 0		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BF_DUNE_FAV									break
				CASE 1		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_GROTTI_TURISMO_R							break	
				CASE 2		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BRAVADO_BANSHEE								break
				CASE 3		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_WINDSOR								break	
				CASE 4		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_INVETERO_COQUETTE_CLASSIC					break	
				CASE 5		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_HUNTLEY_S								break
				CASE 6		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_OBEY_OMNIS									break
				CASE 7		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_PEGASSI_VORTEX								break
				CASE 8		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_WESTERN_ZOMBIE_CHOPPER						break
				CASE 9		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_MAIBATSU_FROGGER							break	
			
			ENDSWITCH
		BREAK    	
		CASE 1//PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_PROPERTY
			
			SWITCH menuindex
				CASE 0		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE			break
				CASE 1		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_PALETO_FOREST_BUNKER					break	
				CASE 2		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE		break
				CASE 3		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_SENORA_DESERT_CONTERFEIT_CASH_FACTORY	break
				CASE 4		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT		break
				CASE 5		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE	break
			ENDSWITCH
		BREAK
		
		
		
		CASE 3 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_WEAPONS break
			SWITCH menuindex
				CASE 0		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_WEAPON_MARKSMAN_RIFLE								break
				CASE 1		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_WEAPON_COMPACT_RIFLE								break
				CASE 2		RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_WEAPON_COMPACT_GRENADE_LAUNCHER						break
			ENDSWITCH
		BREAK
		
		CASE 4 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_RACE
//			IF menuindex < 21	
//				RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_IMPORT_EXPORT_ALL 
//			ELSE //IF menuindex >= 21		
//				RETURN     CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_STUNT_RACE_ALL	 
//			ENDIF
			SWITCH menuindex
				CASE 0   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_NAVY_RACING_SUIT		BREAK			
				CASE 1   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_GRAY_RACING_SUIT       BREAK     
				CASE 2   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_RED_RACING_SUIT        BREAK     
				CASE 3   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_BLACK_RACING_SUIT      BREAK     
				CASE 4   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_GREEN_RACING_SUIT      BREAK     
				CASE 5   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_WHITE_RACING_SUIT      BREAK       
			ENDSWITCH
		BREAK
		
		
	

		
		CASE 5 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_BIKER	
		
			SWITCH menuindex
					CASE 0   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_GREEN_BIKER_SUIT       BREAK     
					CASE 1   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_RED_BIKER_SUIT         BREAK     
					CASE 2   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_ITALIAN_BIKER_SUIT     BREAK     
					CASE 3   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_BLACK_BIKER_SUIT       BREAK     
					CASE 4  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_FRENCH_BIKER_SUIT      BREAK     
					CASE 5  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_YELLOW_BIKER_SUIT      BREAK     
					CASE 6  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_PINK_BIKER_SUIT        BREAK     
					CASE 7  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_BLUE_BIKER_SUIT        BREAK     
			ENDSWITCH			
		BREAK      
		
		CASE 6 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_STUNT	
		
			SWITCH menuindex
					CASE 0  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_WHITE_STUNTMAN_SUIT    BREAK     
					CASE 1  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_BLUE_STUNTMAN_SUIT     BREAK     
					CASE 2  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_RED_STUNTMAN_SUIT      BREAK     
					CASE 3  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_BLACK_STUNTMAN_SUIT    BREAK     
					CASE 4  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_PINK_STUNTMAN_SUIT     BREAK     
					CASE 5  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_GOLD_STUNTMAN_SUIT     BREAK     
					CASE 6  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_SILVER_STUNTMAN_SUIT   BREAK     
					
			ENDSWITCH			
		BREAK     
		
		CASE 7// PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_IMPORT_EXPORT	
			SWITCH menuindex
				CASE 0  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_ANORAK             BREAK     
				CASE 1  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_BIGNESS            BREAK     
				CASE 2  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_DEALERSHIP         BREAK     
				CASE 3  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_GUNSHOW            BREAK     
				CASE 4  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_HIP_HOP            BREAK     
				CASE 5  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_NEW_SKOOL          BREAK     
				CASE 6  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_PATCHWORK          BREAK     
				CASE 7  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_PLAIN_SIGHT        BREAK     
				CASE 8  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_PUFF               BREAK     
				CASE 9  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_THE_SMOLDER            BREAK     
			ENDSWITCH				
		BREAK     
		
		CASE 8 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_TATTOOS
				SWITCH menuindex
					CASE 0   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIDE_FOREVER            BREAK       
					CASE 1   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BROTHERS_FOR_LIFE       BREAK       
					CASE 2   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FLAMING_REAPER          BREAK       
					CASE 3   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_CLAWED_BEAST            BREAK       
					CASE 4   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIP_MY_BROTHERS         BREAK       
					CASE 5   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FREEMOM_WHEELS          BREAK       
					CASE 6   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_CHOPPER_FREEDOM         BREAK       
					CASE 7   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FAGGIO                  BREAK       
					CASE 8   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WE_ARE_THE_MODS         BREAK       
					CASE 9   RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_REAPER_VULTURE          BREAK       
					CASE 10  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_UNFORGIVEN              BREAK       
					CASE 11  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_NO_REGRETS              BREAK       
					CASE 12  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BROTHERHOOD_OF_BIKES    BREAK       
					CASE 13  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WESTERN_EAGLE           BREAK       
					CASE 14  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BONE_WRENCH             BREAK       
					CASE 15  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_AMERICAN_DREAM          BREAK       
					CASE 16  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WESTERN_MC              BREAK       
					CASE 17  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GRUESOME_TALONS         BREAK       
					CASE 18  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SKELETAL_CHOPPER        BREAK       
					CASE 19  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_DEMON_CROSSBONES        BREAK       
					CASE 20  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MADE_IN_AMERICA         BREAK       
					CASE 21  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BOTH_BARRELS            BREAK       
					CASE 22  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_DEMON_RIDER             BREAK       
					CASE 23  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BIKER_MOUNT             BREAK       
					CASE 24  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GAS_GUZZLER             BREAK       
					CASE 25  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GEAR_HEAD               BREAK       
					CASE 26  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SKULL_OF_TAURUS         BREAK       
					CASE 27  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WEB_RIDER               BREAK       
					CASE 28  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WESTERN_STYLIZED        BREAK       
					CASE 29  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FTW                     BREAK       
					CASE 30  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MORBID_ARACHNID         BREAK       
					CASE 31  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MUFFLER_HELMET          BREAK       
					CASE 32  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_POISON_SCORPION         BREAK       
					CASE 33  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIDE_HARD_DIE_FAST      BREAK       
					CASE 34  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_CHAIN_FIST              BREAK       
					CASE 35  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GOOD_LUCK               BREAK       
					CASE 36  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_LIVE_TO_RIDE            BREAK       
					CASE 37  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_CRANIAL_ROSE            BREAK       
					CASE 38  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MACABRE_TREE            BREAK       
					CASE 39  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_URBAN_STUNTER           BREAK       
					CASE 40  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_MUM                     BREAK       
					CASE 41  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_THESE_COLOURS_DONT_RUN  BREAK       
					CASE 42  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SNAKE_BIKE              BREAK       
					CASE 43  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SKULL_CHAIN             BREAK       
					CASE 44  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_GRIM_RIDER              BREAK       
					CASE 45  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_EAGLE_EMBLEM            BREAK       
					CASE 46  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_LADY_MORTALITY          BREAK       
					CASE 47  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SWOOPING_EAGLE          BREAK       
					CASE 48  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_LAUGHING_SKULL          BREAK       
					CASE 49  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BONE_CRUISER            BREAK       
					CASE 50  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIDE_FREE               BREAK       
					CASE 51  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_SCORCHED_SOUL           BREAK       
					CASE 52  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_ENGULFED_SKULL          BREAK       
					CASE 53  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_BAD_LUCK                BREAK       
					CASE 54  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_RIDE_OR_DIE             BREAK       
					CASE 55  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_ROSE_TRIBUTE            BREAK       
					CASE 56  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_STFU                    BREAK       
					CASE 57  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_AMERICAN_MADE           BREAK       
					CASE 58  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_DUSK_RIDER              BREAK       
					CASE 59  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_WESTERN_INSIGNIA        BREAK       
					CASE 60  RETURN  CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_DRAGONS_FURY            BREAK 
				ENDSWITCH
			BREAK
		ENDSWITCH
	RETURN CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_OUTFIT_STUNT_RACE_ALL 
ENDFUNC

FUNC STRING GET_CRIMINAL_ENTERPRISE_STARTER_PACK_HOW_TO_USE(INT pauseMenuRoute1, INT menuindex)
	SWITCH pauseMenuRoute1
	
	
		CASE 0 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_ALL
			SWITCH menuindex
					CASE 0		RETURN    "PROP_MAZ_D"		BREAK   //Maze Bank West Executive Office
					CASE 1		RETURN    "PROP_PAL_D"     	BREAK	//Paleto Forest Bunker
					CASE 2		RETURN    "PROP_CHAP_D"    	BREAK  //Great Chaparral Biker Clubhouse
					CASE 3		RETURN    "PROP_SEN_D"     	BREAK  //Senora Desert Counterfeit Cash Factory
					CASE 4		RETURN    "PROP_VITA_D"    	BREAK  //1561 San Vitas Street
					CASE 5		RETURN    "PROP_EXCEP_D"   	BREAK  //1337 Exceptionalists Way 10 Car Garage
					CASE 6		RETURN    "VEH_DUNE_D"     	BREAK
					CASE 7		RETURN    "VEH_TURI_D"     	BREAK
					CASE 8		RETURN    "VEH_BAN_D"      	BREAK
					CASE 9		RETURN    "VEH_WINS_D"     	BREAK
					CASE 10		RETURN    "VEH_COQ_D"      	BREAK
					CASE 11		RETURN    "VEH_HUNT_D"     	BREAK
					CASE 12		RETURN    "VEH_OMIS_D"     	BREAK
					CASE 13		RETURN    "VEH_VORT_D"     	BREAK
					CASE 14		RETURN    "VEH_ZOMB_D"     	BREAK
					CASE 15		RETURN    "VEH_FROG_D"     	BREAK
					CASE 16		RETURN    "WEP_MARK_D"     	BREAK
					CASE 17		RETURN    "WEP_COMP_D"     	BREAK
					CASE 18		RETURN    "WEP_GREN_D"     	BREAK
					CASE 19		RETURN    "SUIT_RACE_D"    	BREAK
					CASE 20		RETURN    "WEP_BIKE_D"    	BREAK
					CASE 21		RETURN    "WEP_STUNT_D"    	BREAK
					CASE 22		RETURN    "WEP_IMPEX_D"    	BREAK
					CASE 23		RETURN    "WEP_TAT_D"      	BREAK
				BREAK
			ENDSWITCH
		BREAK
	
		CASE 1 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_PROPERTY
			SWITCH menuindex
					CASE 0		RETURN    "PROP_MAZ_D"		BREAK   //Maze Bank West Executive Office
					CASE 1		RETURN    "PROP_PAL_D"     	BREAK	//Paleto Forest Bunker
					CASE 2		RETURN    "PROP_CHAP_D"    	BREAK  //Great Chaparral Biker Clubhouse
					CASE 3		RETURN    "PROP_SEN_D"     	BREAK  //Senora Desert Counterfeit Cash Factory
					CASE 4		RETURN    "PROP_VITA_D"    	BREAK  //1561 San Vitas Street
					CASE 5		RETURN    "PROP_EXCEP_D"   	BREAK  //1337 Exceptionalists Way 10 Car Garage
				BREAK
			ENDSWITCH

		BREAK
		
		CASE 2 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_VEHICLES
			SWITCH menuindex
				CASE 0		RETURN    "VEH_DUNE_D"     	BREAK
				CASE 1		RETURN    "VEH_TURI_D"     	BREAK
				CASE 2		RETURN    "VEH_BAN_D"      	BREAK
				CASE 3		RETURN    "VEH_WINS_D"     	BREAK
				CASE 4		RETURN    "VEH_COQ_D"      	BREAK
				CASE 5		RETURN    "VEH_HUNT_D"     	BREAK
				CASE 6		RETURN    "VEH_OMIS_D"     	BREAK
				CASE 7		RETURN    "VEH_VORT_D"     	BREAK
				CASE 8		RETURN    "VEH_ZOMB_D"     	BREAK
				CASE 9		RETURN    "VEH_FROG_D"     	BREAK
				BREAK
			ENDSWITCH			
		

		BREAK
		
	

		CASE 3 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_WEAPONS
			SWITCH menuindex
				CASE 0		RETURN    "WEP_MARK_D"     	BREAK
				CASE 1		RETURN    "WEP_COMP_D"     	BREAK
				CASE 2		RETURN    "WEP_GREN_D"     	BREAK
				BREAK
			ENDSWITCH

		BREAK
		
		CASE 4 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_RACE
			RETURN    "SUIT_RACE_D" 				
		BREAK        	
		
		CASE 5 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_BIKER	
		
			RETURN    "WEP_BIKE_D" 				
		BREAK      
		
		CASE 6 // PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_STUNT	
		
			RETURN    "WEP_STUNT_D" 				
		BREAK     
		
		CASE 7// PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_OUTFITS_IMPORT_EXPORT	
		
			RETURN    "WEP_IMPEX_D" 				
		BREAK   
		
		CASE 8 //PM_D2_CRIMINAL_ENTERPRISE_STARTER_PACK_TATTOOS
			RETURN "WEP_TAT_D"
		BREAK
	ENDSWITCH        	
	                 	
	RETURN "PM_DETAILS"
ENDFUNC


/// PURPOSE: Sets the scaleform data slots to display the mission details

PROC DISPLAY_CRIMINAL_ENTERPRISE_STARTER_PACK_ITEM_DETAILS(INT iColumn, INT pauseMenuRoute2, STRING tl31MissionName, INT iPrice, INT pauseMenuRoute1, INT menuindex  )
	//tl63Description = tl63Description	// To be set up






	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_CRIMINAL_ENTERPRISE_STARTER_PACK_ITEM_DETAILS - bInitLoadMissionPhoto = FALSE")
	SHOW_PM_COLUMN(2)

	TAKE_CONTROL_OF_FRONTEND()
	SET_FRONTEND_DETAILS_TITLE(iColumn, "", tl31MissionName, 0, "", "", FALSE)
		
	
	PM_SET_DATA_SLOT_FOR_STAT(iColumn, 			    0, 	pauseMenuRoute2, 		0 , 	FALSE,	"BS_WB_COST", 		"ITEM_FREE")
	PM_SET_DATA_SLOT_FOR_STAT_WITH_NUMBER(iColumn, 	1, 	pauseMenuRoute2, 		1, 		"PM_PREM_RRP", "IMPOUND_COST", 	iPrice)

//	IF HAS_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(GET_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_ENUM_FROM_COLUMNS(pauseMenuRoute1,menuindex))
//		PM_SET_DATA_SLOT_FOR_STAT(iColumn, 			2, 	pauseMenuRoute2, 		2, 	FALSE, 	"PM_PREM_PURCH", 	"PM_PREM_OWNED")
//	else
		PM_SET_DATA_SLOT_FOR_STAT(iColumn, 			2, 	pauseMenuRoute2, 		2, 	FALSE, 	"PM_PREM_PURCH", 	GET_CRIMINAL_ENTERPRISE_STARTER_PACK_WHERE_TO_PURCHASE(pauseMenuRoute1,menuindex))
//	endif
	//PM_SET_DATA_SLOT_FOR_STAT(iColumn, 			3, 	pauseMenuRoute2, 		3, 	FALSE, 	"PM_PREM_USE", 		GET_CRIMINAL_ENTERPRISE_STARTER_PACK_HOW_TO_USE(pauseMenuRoute1,menuindex))
	PM_SET_DATA_SLOT(iColumn, 						3, 	pauseMenuRoute2, 		3, 	FALSE, 	" ", FALSE, 4) // Type 4 to show white bar
	PM_SET_DATA_SLOT(iColumn, 						4, 	pauseMenuRoute2, 		4, 	FALSE, 	GET_CRIMINAL_ENTERPRISE_STARTER_PACK_HOW_TO_USE(pauseMenuRoute1,menuindex), FALSE, 5)

	
	PM_DISPLAY_DATA_SLOT(iColumn)
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF pauseMenuGlobalState != PAUSE_MENU_GLOBAL_STATE_EDIT_PLAYLIST	
		RELEASE_CONTROL_OF_FRONTEND()
	ENDIF

ENDPROC

FUNC INT AMRCTT_GET_PAR_TIME_PAUSE_MENU(AMRCTT_VARIATION eVariation)
	IF g_sMPTunables.iRC_TIME_TRIAL_OVERRIDE_TIME > 0
		RETURN g_sMPTunables.iRC_TIME_TRIAL_OVERRIDE_TIME
	ELSE
		SWITCH eVariation
			CASE AV_CONSTRUCTION_SITE_I			RETURN 110000
			CASE AV_CYPRESS_FLATS				RETURN 90000
			CASE AV_HILL_VALLEY_CHURCH_CEMETERY	RETURN 80000
			CASE AV_LA_FUENTE_BLANCA			RETURN 87000
			CASE AV_LITTLE_SEOUL_PARK			RETURN 70000
			CASE AV_DAVIS_QUARTZ				RETURN 92000
			CASE AV_VESPUCCI_BEACH				RETURN 125000
			CASE AV_CONSTRUCTION_SITE_II		RETURN 72000
			CASE AV_VESPUCCI_CANALS				RETURN 113000
			CASE AV_PALMER_TAYLOR_POWER_STATION	RETURN 105000
		ENDSWITCH
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE: Sets the scaleform data slots to display the mission details
PROC DISPLAY_MISSION_DETAILS(INT iColumn, INT iMenuID, VECTOR vLocation, INT iRating, STRING tl63Creator,
							 STRING tl31MissionName, STRING tl63Description,
							 INT iRank, INT iMinPlayers, INT iMaxPlayers, INT iMissionType, INT iMissionSubType, INT iMissionBitSet, INT iMissionBitSetTwo,
							 BOOL bVerified, TEXT_LABEL_23 sFileName, DOWNLOAD_PHOTO_DATA &DLPhotoData, BOOL bHasMissionGotPhoto, BOOL bRockstarCreated, INT iMaxTeams, BOOL bSCNickname=FALSE, BOOL bPushbikeOnly=FALSE, BOOL bLiteralDecString = TRUE, INT iRootContentIDHash = 0, INT iAdversaryModeType = 0)
	//tl63Description = tl63Description	// To be set up
	
	//Setup Loading of Photo
	DLPhotoData.iColumn 				= iColumn
	DLPhotoData.iMissionType 			= iMissionType
	DLPhotoData.tl31MissionName 		= tl31MissionName
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_MISSION_DETAILS - tl31MissionName = ", tl31MissionName)
	DLPhotoData.bVerified 				= bVerified
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_MISSION_DETAILS - bVerified = ", bVerified)
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_MISSION_DETAILS - bRockstarCreated = ", bRockstarCreated)
	DLPhotoData.sCreator				= tl63Creator
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_MISSION_DETAILS - tl63Creator = ", tl63Creator)
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_MISSION_DETAILS - tl63Description = ", tl63Description)
	IF bHasMissionGotPhoto = TRUE
		DLPhotoData.sFileName			= sFileName
	ELSE
		//DLPhotoData.sCreator			= "RSN_BobbyW_1"
		//DLPhotoData.sFileName			= "513f98028fc43216549b3416"
		CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_MISSION_DETAILS - HAS NO PHOTO - USE DEFAULT")
		IF GET_HASH_OF_MAP_AREA_AT_COORDS(vLocation) = MAP_AREA_CITY
            DLPhotoData.sFileName = "City"
        ELSE
            DLPhotoData.sFileName = "Country_side"
        ENDIF
	ENDIF
	DLPhotoData.bLoadMissionPhoto 		= TRUE
	DLPhotoData.bInitLoadMissionPhoto 	= FALSE
	CPRINTLN(DEBUG_PAUSE_MENU, "[JA@PAUSEMENU] DISPLAY_MISSION_DETAILS - bInitLoadMissionPhoto = FALSE")
	SHOW_PM_COLUMN(2)

	TAKE_CONTROL_OF_FRONTEND()
		
		INT iIconColour = ENUM_TO_INT(HUD_COLOUR_WHITE)
		INT iJobType = 0
		
		// Added as UGC has been pushed back.
		//bRockstarCreated = TRUE
		
		IF bRockstarCreated
			tl63Creator = GET_ROCKSTAR_CREATED_USER_STRING()
			iIconColour = ENUM_TO_INT(HUD_COLOUR_BLUE)
			//iJobType = 2
		ELIF bVerified
			//tl63Creator = GET_ROCKSTAR_VERIFIED_USER_STRING()
			iIconColour = ENUM_TO_INT(HUD_COLOUR_BLUE)
			//iJobType = 1
		ENDIF
		
		// get correct icon for mission
		INT iMissionTypeIcon
		GET_PAUSE_MENU_MISSION_ICON(iMissionType, iMissionSubType, iMissionTypeIcon, iAdversaryModeType)
			 
		
		//PM_RESET_ALL_DATA_SLOTS(iColumn)
		
		//PM_SET_COLUMN_TITLE(iColumn, GET_MISSION_DETAIL_STRING_FOR_PAUSE_MENU(iMissionType), tl31MissionName, bVerified) // Will need to pass screenshot textures
		SET_FRONTEND_DETAILS_TITLE(iColumn, "", tl31MissionName, iJobType, "", "", TRUE, 1, iMissionType, iMissionSubType, sFileName, bPushbikeOnly, iRootContentIDHash, iAdversaryModeType)
		
		IF iMissionType = FMMC_TYPE_TIME_TRIAL 
		
			iIconColour = ENUM_TO_INT(HUD_COLOUR_NET_PLAYER2)
			PM_SET_DATA_SLOT_FOR_PLAYERS(iColumn, 			0, 	iMenuID, 		0, 			"PM_PLAYERS", 	iMinPlayers, 		iMaxPlayers)
			PM_SET_DATA_SLOT_FOR_TYPE_STAT(iColumn, 		1, 	iMenuID, 		1, 			"PM_TYPE", 		GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(iMissionType, iMissionSubtype, iMissionBitSet, iMissionBitSetTwo, DEFAULT, iAdversaryModeType), iMissionTypeIcon, iIconColour, IS_BIT_SET(iMissionBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
			PM_SET_DATA_SLOT_FOR_STAT_WITH_TIME(iColumn,	2,	iMenuID,		2,			"PM_PTIME",		"STRTNM1", 			GET_BEST_TRIAL_TIME(GET_CURRENT_ACTIVE_FM_TIME_TRIAL()))
			PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				3, 	iMenuID, 		3, 	FALSE, 	"PM_AREA", 		GET_NAME_OF_ZONE(vLocation))
			PM_SET_DATA_SLOT(iColumn, 						4, 	iMenuID, 		4, 	FALSE, 	" ", FALSE, 4) // Type 4 to show white bar
			PM_SET_DATA_SLOT(iColumn, 						5, 	iMenuID, 		5, 	FALSE, 	tl63Description, bLiteralDecString, 5)
		ELIF iMissionType = FMMC_TYPE_RC_TIME_TRIAL
			iIconColour = ENUM_TO_INT(HUD_COLOUR_NET_PLAYER2)
			PM_SET_DATA_SLOT_FOR_PLAYERS(iColumn, 			0, 	iMenuID, 		0, 			"PM_PLAYERS", 	iMinPlayers, 		iMaxPlayers)
			PM_SET_DATA_SLOT_FOR_TYPE_STAT(iColumn, 		1, 	iMenuID, 		1, 			"PM_TYPE", 		GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(iMissionType, iMissionSubtype, iMissionBitSet, iMissionBitSetTwo, DEFAULT, iAdversaryModeType), iMissionTypeIcon, iIconColour, IS_BIT_SET(iMissionBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
			PM_SET_DATA_SLOT_FOR_STAT_WITH_TIME(iColumn,	2,	iMenuID,		2,			"PM_PTIME",		"STRTNM1", AMRCTT_GET_PAR_TIME_PAUSE_MENU(GET_CURRENT_ACTIVE_FM_RC_TIME_TRIAL() ))
			PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				3, 	iMenuID, 		3, 	FALSE, 	"PM_AREA", 		GET_NAME_OF_ZONE(AMRCTT_GET_START_LOCATION(GET_CURRENT_ACTIVE_FM_RC_TIME_TRIAL() )))
			PM_SET_DATA_SLOT(iColumn, 						4, 	iMenuID, 		4, 	FALSE, 	" ", FALSE, 4) // Type 4 to show white bar
			PM_SET_DATA_SLOT(iColumn, 						5, 	iMenuID, 		5, 	FALSE, 	tl63Description, bLiteralDecString, 5)
		#IF FEATURE_GEN9_EXCLUSIVE
		ELIF iMissionType = FMMC_TYPE_HSW_TIME_TRIAL
			iIconColour = ENUM_TO_INT(HUD_COLOUR_NET_PLAYER2)
			PM_SET_DATA_SLOT_FOR_PLAYERS(iColumn, 			0, 	iMenuID, 		0, 			"PM_PLAYERS", 	iMinPlayers, 		iMaxPlayers)
			PM_SET_DATA_SLOT_FOR_TYPE_STAT(iColumn, 		1, 	iMenuID, 		1, 			"PM_TYPE", 		"HSWTT_TITLE", iMissionTypeIcon, iIconColour, IS_BIT_SET(iMissionBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
			
			
			IF GET_HSW_TIME_TRIAL_TIER() != eHSWTTTIER_INVALID
				PM_SET_DATA_SLOT_FOR_STAT_WITH_TIME(iColumn,	2,	iMenuID,		2,			"PM_PTIME",		"STRTNM1", GET_HSW_TIME_TRIAL_TARGET_TIME_MS(GET_CURRENT_HSW_TIME_TRIAL(), GET_HSW_TIME_TRIAL_TIER()))
				PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				3, 	iMenuID, 		3, 	FALSE, 	"PM_AREA", 		GET_NAME_OF_ZONE(GET_HSW_TIME_TRIAL_START_LOCATION(GET_CURRENT_HSW_TIME_TRIAL() )))
				PM_SET_DATA_SLOT(iColumn, 						4, 	iMenuID, 		4, 	FALSE, 	" ", FALSE, 4) // Type 4 to show white bar
				PM_SET_DATA_SLOT(iColumn, 						5, 	iMenuID, 		5, 	FALSE, 	tl63Description, bLiteralDecString, 5)
			ELSE
				PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				2, 	iMenuID, 		2, 	FALSE, 	"PM_AREA", 		GET_NAME_OF_ZONE(GET_HSW_TIME_TRIAL_START_LOCATION(GET_CURRENT_HSW_TIME_TRIAL() )))
				PM_SET_DATA_SLOT(iColumn, 						3, 	iMenuID, 		3, 	FALSE, 	" ", FALSE, 4) // Type 4 to show white bar
				PM_SET_DATA_SLOT(iColumn, 						4, 	iMenuID, 		4, 	FALSE, 	tl63Description, bLiteralDecString, 5)
			
			ENDIF
		#ENDIF
		ELSE
		
			INT iIndex = 0
		
			CPRINTLN(DEBUG_PAUSE_MENU, "  --  DISPLAY_MISSION_DETAILS  -- BEFORE RATED DRAWN - iRating = ", iRating)
			IF iRating = -1
				PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				iIndex, 	iMenuID,		iIndex, 	TRUE, 	"PM_RATING", 	"PM_RATING_N")
				CPRINTLN(DEBUG_PAUSE_MENU, "  --  DISPLAY_MISSION_DETAILS  -- NOT RATED - iRating = ", iRating)
			ELSE
				PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				iIndex, 	iMenuID,		iIndex, 	TRUE, 	"PM_RATING", 	"PM_RATING_V", 		iRating)
				CPRINTLN(DEBUG_PAUSE_MENU, "  --  DISPLAY_MISSION_DETAILS  -- RATED - iRating = ", iRating)
			ENDIF
			iIndex++
			
			PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"PM_CREATED", 	tl63Creator, 		0, 			TRUE, TRUE, bSCNickname)
			iIndex++
			
			PM_SET_DATA_SLOT_FOR_STAT_WITH_NUMBER(iColumn, 	iIndex, 	iMenuID, 		iIndex, 			"PM_RANK", 		"NUMBER", 			iRank)
			iIndex++
			
			IF iMaxPlayers > NUM_NETWORK_REAL_PLAYERS()
				iMaxPlayers = NUM_NETWORK_REAL_PLAYERS()
			ENDIF
			PM_SET_DATA_SLOT_FOR_PLAYERS(iColumn, 			iIndex, 	iMenuID, 		iIndex, 			"PM_PLAYERS", 	iMinPlayers, 		iMaxPlayers)
			iIndex++
			
			IF iMaxTeams > 1
				PM_SET_DATA_SLOT_FOR_STAT_WITH_NUMBER(iColumn, 	iIndex, 	iMenuID, 		iIndex, 			"PM_TEAMS", 		"NUMBER", 			iMaxTeams)
				iIndex++
				
				PM_SET_DATA_SLOT_FOR_TYPE_STAT(iColumn, 		iIndex, 	iMenuID, 		iIndex, 			"PM_TYPE", 		GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(iMissionType, iMissionSubtype, iMissionBitSet, iMissionBitSetTwo, DEFAULT, iAdversaryModeType), iMissionTypeIcon, iIconColour, IS_BIT_SET(iMissionBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
				iIndex++
				PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"PM_AREA", 		GET_NAME_OF_ZONE(vLocation))
				iIndex++
				IF !IS_STRING_NULL_OR_EMPTY(tl63Description)
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	" ", FALSE, 4) // Type 4 to show white bar
					iIndex++
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	tl63Description, TRUE, 5)
					iIndex++
				ELSE
					// Clear data slots
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"", TRUE, 5)
					iIndex++
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"", TRUE, 5)
					iIndex++
				ENDIF
			ELSE
				PM_SET_DATA_SLOT_FOR_TYPE_STAT(iColumn, 		iIndex, 	iMenuID, 		iIndex, 			"PM_TYPE", 		GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(iMissionType, iMissionSubtype, iMissionBitSet, iMissionBitSetTwo, DEFAULT, iAdversaryModeType), iMissionTypeIcon, iIconColour, IS_BIT_SET(iMissionBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
				iIndex++
				PM_SET_DATA_SLOT_FOR_STAT(iColumn, 				iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"PM_AREA", 		GET_NAME_OF_ZONE(vLocation))
				iIndex++
				IF !IS_STRING_NULL_OR_EMPTY(tl63Description)
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	" ", FALSE, 4) // Type 4 to show white bar
					iIndex++
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	tl63Description, TRUE, 5)
					iIndex++
					//PM_RESET_DATA_SLOT(8)
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"", TRUE, 5)
					iIndex++
				ELSE
					// Clear data slots
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"", TRUE, 5)
					iIndex++
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"", TRUE, 5)
					iIndex++
					//PM_RESET_DATA_SLOT(8)
					PM_SET_DATA_SLOT(iColumn, 						iIndex, 	iMenuID, 		iIndex, 	FALSE, 	"", TRUE, 5)
					iIndex++
				ENDIF
			ENDIF
		
		ENDIF
		
		PM_DISPLAY_DATA_SLOT(iColumn)
		
		IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	IF pauseMenuGlobalState != PAUSE_MENU_GLOBAL_STATE_EDIT_PLAYLIST	
		RELEASE_CONTROL_OF_FRONTEND()
	ENDIF

ENDPROC


/// PURPOSE: Displays a loading spinner in position passed
/// PARAMS:
///    iPos - 0-4 (xPos = 0.5, 1, 1.5, 2, 2.5)
PROC SET_COLUMN_AS_BUSY(INT iPos, BOOL bBusy)
	PAUSE_MENU_SET_BUSY_SPINNER(bBusy, iPos)
ENDPROC

//Get an array position from a vector
FUNC INT GET_ARRAY_POS_FROM_COORD(FMMC_ROCKSTAR_CREATED_STRUCT &sStructPassedIn, VECTOR vBlipPos)
	INT iLoops
	FOR iLoops = 0 TO (COUNT_OF(sStructPassedIn.sMissionHeaderVars) - 1)
		IF sStructPassedIn.sMissionHeaderVars[iLoops].vStartPos.x = vBlipPos.x
		AND sStructPassedIn.sMissionHeaderVars[iLoops].vStartPos.y = vBlipPos.y
			RETURN iLoops
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC
//Get an array position from a vector
FUNC INT GET_ARRAY_POS_FROM_COORD_VERIFIED(FMMC_ROCKSTAR_VERIFIED_STRUCT &sStructPassedIn, VECTOR vBlipPos)
	INT iLoops
	FOR iLoops = 0 TO (COUNT_OF(sStructPassedIn.sMissionHeaderVars) - 1)
		IF sStructPassedIn.sMissionHeaderVars[iLoops].vStartPos.x = vBlipPos.x
		AND sStructPassedIn.sMissionHeaderVars[iLoops].vStartPos.y = vBlipPos.y
			RETURN iLoops
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

//Get an array position from a vector
FUNC INT GET_ARRAY_POS_FROM_COORD_USER_MISSION(FMMC_GLOBAL_HEADER_STRUCT &sStructPassedIn, VECTOR vBlipPos)
	INT iLoops
	FOR iLoops = 0 TO (COUNT_OF(sStructPassedIn.sMyMissionHeaderVars) - 1)
		IF sStructPassedIn.sMyMissionHeaderVars[iLoops].vStartPos.x = vBlipPos.x
		AND sStructPassedIn.sMyMissionHeaderVars[iLoops].vStartPos.y = vBlipPos.y
			RETURN iLoops
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

//Get an array position from a vector
FUNC INT GET_ARRAY_POS_FROM_COORD_REMOTE_USER_MISSION(VECTOR vBlipPos)
	INT iLoops
	FOR iLoops = 0 TO (COUNT_OF(GlobalServerBD_MissionsShared.cloudDetails) - 1)
		IF GlobalServerBD_MissionsShared.cloudDetails[iLoops].mscHeaderData.vStartPos.x = vBlipPos.x
		AND GlobalServerBD_MissionsShared.cloudDetails[iLoops].mscHeaderData.vStartPos.y = vBlipPos.y
			RETURN iLoops
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

//Get all the details about a mission
FUNC BOOL GET_MISSION_INFO_FROM_VECTOR(	BLIP_INDEX 		biPassed, 				//The blip
										VECTOR			&vLocation,				//The location
										TEXT_LABEL_63 	&tl63Creator,			//The creator
										TEXT_LABEL_63 	&tl31MissionName,		//The mission name
										TEXT_LABEL_63 	&tl63Description, 		//The mission description
										INT 			&iMinPlayers, 			//The min number of players
										INT 			&iMaxPlayers, 			//the max number of players
										INT 			&iMissionType, 			//the mission type
										INT 			&iMissionSubType,		//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
										INT				&iMissionBitSet,		//The bitset of the mission (Capture mode, HOLD, CONTEND etc)
										INT				&iMissionBitSetTwo,		//The bitset of the mission (Capture mode, HOLD, CONTEND etc)
										INT				&iRank,					//The rank player must be at
										INT 			&iRating, 				//the mission rating
										BOOL			&bVerified,				//Rockstar verified
										INT				&iCreatorID,			//The CreatorID
										INT				&iCachedDescID,			//The cached mision descriptionID
										BOOL			&bHasPhoto,
										BOOL			&bRockstarCreated,
										TEXT_LABEL_23 	&tl23FileName,
										INT				&iDecHash,
										INT				&iMaxTeams,
										BOOL			&bAllowStartJob,			// KEITH 20/3/14: To disable 'start job' option for remote player's UGC on map
										BOOL			&bPushbikeOnly,
										INT				&iPhotoVersion,
										INT				&iPhotoPath,
										INT 			&iRootContentIDHash,
										INT				&iAdversaryModeType
										#IF IS_DEBUG_BUILD ,
										BOOL bPrintData	= TRUE
										#ENDIF)				

	// KEITH 3/5/13: Set defaults for a couple of return values so they can be ignored by the calling function if not set below
	iCreatorID		= ILLEGAL_CREATOR_ID
	iCachedDescID	= ILLEGAL_ARRAY_POSITION
	bAllowStartJob	= FALSE
	iRootContentIDHash = 0
	iAdversaryModeType = 0
	
	
	IF DOES_BLIP_EXIST(biPassed)
		IF GET_BLIP_INFO_ID_DISPLAY(biPassed) != DISPLAY_NOTHING
						
			vLocation 	= GET_BLIP_COORDS(biPassed)	//The coords?
			INT 		iArrayPos	= -1						//the return array pos
			BLIP_SPRITE blipSprite = GET_BLIP_SPRITE(biPassed)
			INT 		blipColour = GET_BLIP_COLOUR(biPassed)
			
			IF blipSprite = RADAR_TRACE_COMMUNITY_SERIES
				iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[ciCV2_SERIES_COMMUNITY]
				CPRINTLN(DEBUG_PAUSE_MENU, "RADAR_TRACE_COMMUNITY_SERIES - iArrayPos = ", iArrayPos)
				//Fill out the data
				IF iArrayPos != -1
				
					tl63Creator 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlOwner 				//The creator
					tl31MissionName		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlMissionName			//The mission name
					tl63Description 	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlMissionDec			//The mission description
					iMinPlayers 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMinPlayers			//The min number of players
					iMaxPlayers 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMaxPlayers			//The max number of players
					iMissionType 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iType					//The mission type.
					iMissionSubType 	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iSubType				//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
					iMissionBitSet		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitset				//Bitset of the mission
					iMissionBitSetTwo	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitsetTwo				//Bitset of the mission
					iRank		 		= 1																				//The mission rank. 
					iRating				= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iRating				//The mission rating
					tl23FileName 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlName					//The file name
					iDecHash			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMissionDecHash		//The description hash
					iPhotoVersion		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iPhotoVersion
					iPhotoPath			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iPhotoPath
					iRootContentIDHash 	= g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[iArrayPos].iRootContentIdHash
					iAdversaryModeType 	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iAdversaryModeType
					bVerified			= FALSE
					iCreatorID			= FMMC_ROCKSTAR_CREATOR_ID
					iCachedDescID		= iArrayPos
					bHasPhoto			= TRUE //IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
					bRockstarCreated	= TRUE
					iMaxTeams			= g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[iArrayPos].iMaxNumberOfTeams
					bAllowStartJob		= TRUE
					bPushbikeOnly		= IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY)
					#IF IS_DEBUG_BUILD
					IF bPrintData
					CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - A")
					CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
					CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
					CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash		  = ", iDecHash)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType  	  = ", iMissionType)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
					CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
					CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
					ENDIF
					#ENDIF		
					RETURN TRUE
				ENDIF
				
				RETURN FALSE
			ENDIF
			
			IF blipSprite = RADAR_TRACE_STUNT_PREMIUM
				INT iLoop, iPlaylist
				REPEAT ciMAX_V2_PROFESSIONAL_CORONAS iLoop
					IF g_sMPTunables.iProfesionalCoronaType[iLoop] = FMMC_TYPE_RACE
						iPlaylist = iLoop
					ENDIF
				ENDREPEAT
				iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlaylist]
				CPRINTLN(DEBUG_PAUSE_MENU, "RADAR_TRACE_STUNT_PREMIUM - iArrayPos = ", iArrayPos)
				//Fill out the data
				IF iArrayPos != -1
					tl63Creator 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlOwner 			//The creator
					tl31MissionName		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlMissionName		//The mission name
					tl63Description 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlMissionDec		//The mission description
					iMinPlayers 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMinPlayers			//The min number of players
					iMaxPlayers 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMaxPlayers			//The max number of players
					iMissionType 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iType				//The mission type.
					iMissionSubType 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iSubType			//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
					iMissionBitSet		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitset				//Bitset of the mission
					iMissionBitSetTwo	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitsetTwo			//Bitset of the mission
					iRank		 		= 1																			//The mission rank. 
					iRating				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iRating				//The mission rating
					tl23FileName 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlName				//The file name
					iDecHash			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMissionDecHash		//The description hash
					iPhotoVersion		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iPhotoVersion
					iPhotoPath			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iPhotoPath
					iRootContentIDHash 	= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iArrayPos].iRootContentIdHash
					iAdversaryModeType 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iAdversaryModeType
					bVerified			= FALSE
					iCreatorID			= FMMC_ROCKSTAR_CREATOR_ID
					iCachedDescID		= iArrayPos
					bHasPhoto			= TRUE //IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
					bRockstarCreated	= TRUE
					iMaxTeams			= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iArrayPos].iMaxNumberOfTeams
					bAllowStartJob		= TRUE
					bPushbikeOnly		= IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY)
					#IF IS_DEBUG_BUILD
					IF bPrintData
					CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - A")
					CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
					CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
					CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash		  = ", iDecHash)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType  	  = ", iMissionType)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
					CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
					CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
					ENDIF
					#ENDIF		
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF blipSprite = RADAR_TRACE_STUNT
			OR blipSprite = RADAR_TRACE_FEATURED_SERIES
			OR CV2_IS_BLIP_ADVERSARY_SERIES_BLIP(blipSprite)
			OR blipSprite = RADAR_TRACE_STEERINGWHEEL
			OR blipSprite = RADAR_TRACE_ADVERSARY_BUNKER
			OR blipSprite = RADAR_TRACE_RACE_TF
			OR blipSprite = RADAR_TRACE_ACSR_RACE_TARGET
			OR blipSprite = RADAR_TRACE_ACSR_RACE_HOTRING
			OR blipSprite = RADAR_TRACE_ARENA_SERIES
			OR ((blipSprite = RADAR_TRACE_RACE_LAND) AND (blipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_PINKLIGHT)))
			OR ((blipSprite = RADAR_TRACE_HORDE) AND (blipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_PINKLIGHT)))
			OR blipSprite = RADAR_TRACE_RACE_OPEN_WHEEL
			OR blipSprite = RADAR_TRACE_STREET_RACE_SERIES
			OR blipSprite = RADAR_TRACE_PURSUIT_SERIES
			#IF FEATURE_GEN9_EXCLUSIVE
			OR blipSprite = RADAR_TRACE_HSW_RACE_SERIES
			#ENDIF
			OR blipSprite = RADAR_TRACE_CAYO_SERIES
				IF blipSprite = RADAR_TRACE_STUNT
					iArrayPos = g_sV2CoronaVars.iCurrentMissionArrayPos 
					CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_STUNT_PREMIUM iArrayPos = ", iArrayPos)
				ELIF blipSprite = RADAR_TRACE_STEERINGWHEEL
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_SPECIAL_VEHICLE_RACE_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_STEERINGWHEEL iArrayPos = ", iArrayPos)
					ENDIF
				ELIF blipSprite = RADAR_TRACE_ADVERSARY_BUNKER
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_BUNKER_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_ADVERSARY_BUNKER iArrayPos = ", iArrayPos)
					ENDIF
				ELIF blipSprite = RADAR_TRACE_RACE_TF
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_TRANSFORM_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_TRANSFORM iArrayPos = ", iArrayPos)
					ENDIF
				ELIF blipSprite = RADAR_TRACE_ACSR_RACE_TARGET
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_TARGET_ASSAULT_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_ACSR_RACE_TARGET iArrayPos = ", iArrayPos)
					ENDIF
				ELIF blipSprite = RADAR_TRACE_ACSR_RACE_HOTRING
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_HOTRING_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_ACSR_RACE_HOTRING iArrayPos = ", iArrayPos)
					ENDIF
				ELIF blipSprite = RADAR_TRACE_ARENA_SERIES
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_ARENA_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_ARENA_SERIES iArrayPos = ", iArrayPos)
					ENDIF
				ELIF ((blipSprite = RADAR_TRACE_RACE_LAND) AND (blipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_PINKLIGHT)))
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_RACE_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_RACE_LAND and HUD_COLOUR_PINKLIGHT iArrayPos = ", iArrayPos)
					ENDIF
				ELIF ((blipSprite = RADAR_TRACE_HORDE) AND (blipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_PINKLIGHT)))
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_SURVIVAL_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_HORDE and HUD_COLOUR_PINKLIGHT iArrayPos = ", iArrayPos)
					ENDIF
				ELIF (blipSprite = RADAR_TRACE_RACE_OPEN_WHEEL)
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_OPEN_WHEEL_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_RACE_OPEN_WHEEL iArrayPos = ", iArrayPos)
					ENDIF
				ELIF blipSprite = RADAR_TRACE_FEATURED_SERIES
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_MISSION_NEW_VS)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* CV2_GET_ADVERSARY_SERIES_ARRAY_POS iArrayPos = ", iArrayPos)
					ENDIF
				ELIF blipSprite = RADAR_TRACE_STREET_RACE_SERIES
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_STREET_RACE_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_STREET_RACE_SERIES iArrayPos = ", iArrayPos)
					ENDIF
				ELIF blipSprite = RADAR_TRACE_PURSUIT_SERIES
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_PURSUIT_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_PURSUIT_SERIES iArrayPos = ", iArrayPos)
					ENDIF
				#IF FEATURE_GEN9_EXCLUSIVE
				ELIF blipSprite = RADAR_TRACE_HSW_RACE_SERIES
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_HSW_RACE_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_HSW_RACE_SERIES iArrayPos = ", iArrayPos)
					ENDIF
				#ENDIF
					
				ELIF blipSprite = RADAR_TRACE_CAYO_SERIES
					iArrayPos = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_CAYO_PERICO_SERIES)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* RADAR_TRACE_CAYO_SERIES iArrayPos = ", iArrayPos)
					ENDIF
					
				ELSE
					iArrayPos = CV2_GET_ADVERSARY_SERIES_FROM_BLIP_SPRITE(blipSprite)
					IF iArrayPos != -1
						iArrayPos = g_sV2CoronaVars.iCurrentArrayPosProfessional[iArrayPos]
						CPRINTLN(DEBUG_PAUSE_MENU, "[CV2]* CV2_GET_ADVERSARY_SERIES_FROM_BLIP_SPRITE iArrayPos = ", iArrayPos)
					ENDIF
				ENDIF
				//Fill out the data
				IF iArrayPos != -1
					tl63Creator 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlOwner 			//The creator
					tl31MissionName		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlMissionName		//The mission name
					tl63Description 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlMissionDec		//The mission description
					
					INT iPlaylist = CV2_GET_ADVERSARY_SERIES_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iArrayPos].iRootContentIdHash, TRUE)
					IF iPlaylist != -1
					AND g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMin[iPlaylist] != -1
						iMinPlayers = g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMin[iPlaylist]
					ELSE
						iMinPlayers = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMinPlayers			//The min number of players
					ENDIF
					IF iPlaylist != -1
					AND g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMax[iPlaylist] != -1
						iMaxPlayers = g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMax[iPlaylist]
					ELSE
						iMaxPlayers = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMaxPlayers			//The max number of players
					ENDIF
					iMissionType 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iType				//The mission type.
					iMissionSubType 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iSubType			//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
					iMissionBitSet		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitset				//Bitset of the mission
					iMissionBitSetTwo	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitsetTwo			//Bitset of the mission
					iRank		 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iRank				//The mission rank. 
					iRating				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iRating				//The mission rating
					tl23FileName 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlName				//The file name
					iDecHash			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMissionDecHash		//The description hash
					iPhotoVersion		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iPhotoVersion
					iPhotoPath			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iPhotoPath
					iRootContentIDHash 	= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iArrayPos].iRootContentIdHash
					iAdversaryModeType 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iAdversaryModeType
					bVerified			= FALSE
					iCreatorID			= FMMC_ROCKSTAR_CREATOR_ID
					iCachedDescID		= iArrayPos
					bHasPhoto			= TRUE //IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
					bRockstarCreated	= TRUE
					iMaxTeams			= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iArrayPos].iMaxNumberOfTeams
					bAllowStartJob		= TRUE
					bPushbikeOnly		= IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY)
					#IF IS_DEBUG_BUILD
					IF bPrintData
					CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - A")
					CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
					CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
					CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash		  = ", iDecHash)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType  	  = ", iMissionType)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
					CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
					CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
					CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
					ENDIF
					#ENDIF		
					RETURN TRUE
				ENDIF
			ENDIF
			
			SWITCH blipColour
				//Rockstar created
				CASE BLIP_COLOUR_DEFAULT
				CASE BLIP_COLOUR_BLUE
				CASE BLIP_COLOUR_YELLOW
				CASE BLIP_COLOUR_WHITE
					//ROCKSTAR CREATED
					iArrayPos = GET_ARRAY_POS_FROM_COORD(g_FMMC_ROCKSTAR_CREATED, vLocation)
					CPRINTLN(DEBUG_PAUSE_MENU, "ROCKSTAR ATTEMPT - iArrayPos = ", iArrayPos)
					//Fill out the data
					IF iArrayPos != -1
						tl63Creator 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlOwner 			//The creator
						tl31MissionName		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlMissionName		//The mission name
						tl63Description 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlMissionDec		//The mission description
						iMinPlayers 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMinPlayers			//The min number of players
						iMaxPlayers 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMaxPlayers			//The max number of players
						iMissionType 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iType				//The mission type.
						iMissionSubType 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iSubType			//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
						iMissionBitSet		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitset				//Bitset of the mission
						iMissionBitSetTwo	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitsetTwo			//Bitset of the mission
						iRank		 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iRank				//The mission rank. 
						iRating				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iRating				//The mission rating
						tl23FileName 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlName				//The file name
						iDecHash			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMissionDecHash		//The description hash
						iPhotoVersion		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iPhotoVersion
						iPhotoPath			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iPhotoPath
						iRootContentIDHash 	= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iArrayPos].iRootContentIdHash
						iAdversaryModeType 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iAdversaryModeType
						bVerified			= FALSE
						iCreatorID			= FMMC_ROCKSTAR_CREATOR_ID
						iCachedDescID		= iArrayPos
						bHasPhoto			= TRUE //IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
						bRockstarCreated	= TRUE
						iMaxTeams			= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iArrayPos].iMaxNumberOfTeams
						bAllowStartJob		= TRUE
						bPushbikeOnly		= IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY)
						#IF IS_DEBUG_BUILD
						IF bPrintData
						CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - A")
						CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
						CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
						CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash		  = ", iDecHash)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType  	  = ", iMissionType)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
						CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
						CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
						ENDIF
						#ENDIF		
						RETURN TRUE
					ELSE
						//LOCAL USER
						iArrayPos = GET_ARRAY_POS_FROM_COORD_USER_MISSION(g_FMMC_HEADER_STRUCT, vLocation)
						CPRINTLN(DEBUG_PAUSE_MENU, " USER ATTEMPT - iArrayPos = ", iArrayPos)
						PRINTLN(" USER ATTEMPT - player name = ", GET_PLAYER_NAME(PLAYER_ID()))
						IF iArrayPos != -1
							IF iArrayPos >= MAX_NUMBER_FMMC_SAVES
								tl63Creator 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].tlOwner 		//The creator
							ELSE
								tl63Creator 		= GET_PLAYER_NAME(PLAYER_ID())  									//The creator
							ENDIF
							tl31MissionName		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].tlMissionName	// tlMissionName[iArrayPos]		//The mission name
							tl63Description 	= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].tlMissionDec		//The mission description
							iMinPlayers 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iMinPlayers		//The min number of players
							iMaxPlayers 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iMaxPlayers		//the max number of players
							iMissionType 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iType			//the mission type. 
							iMissionSubType 	= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iSubType			//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
							iMissionBitSet		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iBitset			//Bitset of the mission
							iMissionBitSetTwo	= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iBitsetTwo		//Bitset of the mission
							iRank		 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iRank			//the mission rank. 
							iRating				= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iRating			//the mission rating
							iDecHash			= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iMissionDecHash	//The description hash
							iPhotoVersion		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iPhotoVersion
							iPhotoPath			= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iPhotoPath
							bVerified			= FALSE	//TRUE										
							iCreatorID			= NATIVE_TO_INT(PLAYER_ID())
							iCachedDescID		= iArrayPos
							bHasPhoto			= TRUE //IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
							bRockstarCreated	= FALSE
							tl23FileName 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].tlName
							iMaxTeams			= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iMaxNumberOfTeams
							bAllowStartJob		= TRUE
							
							#IF IS_DEBUG_BUILD
							IF bPrintData
							CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - B")
							CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
							CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
							CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash		  = ", iDecHash)
							CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
							CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType     = ", iMissionType)
							CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
							CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
							CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
							CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
							ENDIF
							#ENDIF		
							RETURN TRUE
						ELSE
							//VERIFIED
							iArrayPos = GET_ARRAY_POS_FROM_COORD_VERIFIED(g_FMMC_ROCKSTAR_VERIFIED, vLocation)
							CPRINTLN(DEBUG_PAUSE_MENU, " VERIFIED ATTEMPT - iArrayPos = ", iArrayPos)
							IF iArrayPos != -1
								tl63Creator 		= g_FMMC_ROCKSTAR_VERIFIED.tlOwnerPretty[iArrayPos]						//The creator
								tl31MissionName		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlMissionName		//The mission name
								tl63Description 	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlMissionDec		//The mission description
								iMinPlayers 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMinPlayers		//The min number of players
								iMaxPlayers 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMaxPlayers		//the max number of players
								iMissionType 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iType				//the mission type. 
								iMissionSubType 	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iSubType			//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
								iMissionBitSet		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitset			//Bitset of the mission
								iMissionBitSetTwo	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitsetTwo			//Bitset of the mission
								iRank		 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iRank				//the mission rank. 
								iRating				= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iRating			//the mission rating
								iDecHash			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMissionDecHash	//The description hash
								iPhotoVersion		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iPhotoVersion
								iPhotoPath			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iPhotoPath
								bVerified			= TRUE
								iCreatorID			= FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
								iCachedDescID		= iArrayPos
								bHasPhoto			= TRUE //IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
								bPushbikeOnly		= IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY)
								bRockstarCreated	= FALSE
								tl23FileName		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlName
								iMaxTeams			= g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[iArrayPos].iMaxNumberOfTeams
								bAllowStartJob		= TRUE
								
								#IF IS_DEBUG_BUILD
								IF bPrintData
								CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - C")
								CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
								CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
								CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash  		  = ", iDecHash)
								CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
								CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType     = ", iMissionType)
								CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
								CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
								CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
								CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
								CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
								ENDIF
								#ENDIF		
								RETURN TRUE
							ELSE
								//REMOTE USER
								iArrayPos = GET_ARRAY_POS_FROM_COORD_REMOTE_USER_MISSION(vLocation)
								CPRINTLN(DEBUG_PAUSE_MENU, " REMOTE USER ATTEMPT - iArrayPos = ", iArrayPos)
								IF iArrayPos != -1
									tl63Creator 		= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.tlOwner			//The creator
									tl31MissionName		= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.tlMissionName		//The mission name
									tl63Description 	= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.tlMissionDec		//The mission description
									iMinPlayers 		= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iMinPlayers		//The min number of players
									iMaxPlayers 		= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iMaxPlayers		//the max number of players
									iMissionType 		= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iType				//the mission type. 
									iMissionSubType 	= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iSubType			//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
									iMissionBitSet		= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iBitset			//Bitset of the mission
									iRank		 		= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iRank				//the mission rank. 
									iRating				= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iRating			//the mission rating
									// KEITH 21/3/14 [1788785] - this is server data and the Description Hash is specific to the local player so the shared Hash is locally wrong
									//								and the actual hash needs the mission header data to be downloaded, which it isn't, and so isn't locally available
									//iDecHash			= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iMissionDecHash	//The description hash
									iDecHash			= 0
									bVerified			= FALSE
									iCreatorID			= FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
									iCachedDescID		= iArrayPos
									bHasPhoto			= TRUE //IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
									bRockstarCreated	= FALSE
									tl23FileName		= GlobalServerBD_MissionsShared.cloudDetails[iArrayPos].mscHeaderData.tlName
									iMaxTeams 			= 1
									bAllowStartJob		= FALSE		// Remote player's UGC on map shouldn't display or accept the 'start mission' option
									
									#IF IS_DEBUG_BUILD
									IF bPrintData
									CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - D")
									CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
									CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
									CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash		  = ", iDecHash)
									CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
									CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType     = ", iMissionType)
									CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
									CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
									CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
									CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
									CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
									ENDIF
									#ENDIF		
									RETURN TRUE
								ELSE
									//NONE
									CPRINTLN(DEBUG_PAUSE_MENU, "GET_MISSION_INFO_FROM_VECTOR = -1 (BLIP_COLOUR_DEFAULT & BLIP_COLOUR_WHITE)")
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				//Rocstar Verified
				CASE BLIP_COLOUR_FREEMODE
					//Get an array Pos
					iArrayPos = GET_ARRAY_POS_FROM_COORD_VERIFIED(g_FMMC_ROCKSTAR_VERIFIED, vLocation)
					CPRINTLN(DEBUG_PAUSE_MENU, "B - iArrayPos = ", iArrayPos)
					//Fill out the data
					IF iArrayPos != -1
						tl63Creator 		= g_FMMC_ROCKSTAR_VERIFIED.tlOwnerPretty[iArrayPos]							//The creator
						tl31MissionName		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlMissionName		//The mission name
						tl63Description 	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlMissionDec		//The mission description
						iMinPlayers 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMinPlayers		//The min number of players
						iMaxPlayers 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMaxPlayers		//the max number of players
						iMissionType 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iType				//the mission type. 
						iMissionSubType 	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iSubType			//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
						iMissionBitSet		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitset			//Bitset of the mission
						iMissionBitSetTwo	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitsetTwo			//Bitset of the mission
						iRank		 		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iRank				//the mission rank. 
						iRating				= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iRating			//the mission rating
						iDecHash			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iMissionDecHash	//The description hash
						iPhotoVersion		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iPhotoVersion
						iPhotoPath			= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iPhotoPath
						bVerified			= TRUE
						iCreatorID			= FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
						iCachedDescID		= iArrayPos
						bHasPhoto			= TRUE //IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
						bRockstarCreated	= FALSE
						tl23FileName		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iArrayPos].tlName
						iMaxTeams			= g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[iArrayPos].iMaxNumberOfTeams
						bAllowStartJob		= TRUE
						
						#IF IS_DEBUG_BUILD
						IF bPrintData
						CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - C")
						CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
						CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
						CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash		  = ", iDecHash)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType     = ", iMissionType)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
						CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
						CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
						CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
						ENDIF
						#ENDIF		
						RETURN TRUE
					ELSE
						iArrayPos = GET_ARRAY_POS_FROM_COORD_USER_MISSION(g_FMMC_HEADER_STRUCT, vLocation)
						CPRINTLN(DEBUG_PAUSE_MENU, "B - SECOND ATTEMPT - iArrayPos = ", iArrayPos)
						IF iArrayPos != -1
							tl63Creator 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].tlOwner 			//The creator
							tl31MissionName		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].tlMissionName	// tlMissionName[iArrayPos]		//The mission name
							tl63Description 	= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].tlMissionDec		//The mission description
							iMinPlayers 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iMinPlayers		//The min number of players
							iMaxPlayers 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iMaxPlayers		//the max number of players
							iMissionType 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iType			//the mission type. 
							iMissionSubType 	= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iSubType			//The Sub Type (Team Deathmatch, Vehicle Deathmatch, etc)
							iMissionBitSet		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iBitset			//Bitset of the mission
							iMissionBitSetTwo	= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iBitsetTwo		//Bitset of the mission
							iRank		 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iRank			//the mission rank. 
							iRating				= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iRating			//the mission rating
							iDecHash			= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iMissionDecHash	//The description hash
							iPhotoVersion		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iPhotoVersion
							iPhotoPath			= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iPhotoPath
							bVerified			= FALSE	//TRUE										
							iCreatorID			= NATIVE_TO_INT(PLAYER_ID())
							iCachedDescID		= iArrayPos
							bHasPhoto			= TRUE //IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_PHOTO)
							bRockstarCreated	= FALSE
							tl23FileName 		= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].tlName
							iMaxTeams			= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iArrayPos].iMaxNumberOfTeams
							bAllowStartJob		= TRUE
							
							#IF IS_DEBUG_BUILD
							IF bPrintData
							CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - D")
							CPRINTLN(DEBUG_PAUSE_MENU, "tl63Creator      = ", tl63Creator)
							CPRINTLN(DEBUG_PAUSE_MENU, "tl31MissionName  = ", tl31MissionName)
							CPRINTLN(DEBUG_PAUSE_MENU, "iDecHash  		  = ", iDecHash)
							CPRINTLN(DEBUG_PAUSE_MENU, "iMinPlayers      = ", iMinPlayers)
							CPRINTLN(DEBUG_PAUSE_MENU, "iMissionType     = ", iMissionType)
							CPRINTLN(DEBUG_PAUSE_MENU, "iMissionSubType  = ", iMissionSubType)
							CPRINTLN(DEBUG_PAUSE_MENU, "iMaxPlayers      = ", iMaxPlayers)
							CPRINTLN(DEBUG_PAUSE_MENU, "iRank            = ", iRank)
							CPRINTLN(DEBUG_PAUSE_MENU, "iRating          = ", iRating)
							ENDIF
							#ENDIF		
							RETURN TRUE
						ELSE
							CPRINTLN(DEBUG_PAUSE_MENU, "B - GET_MISSION_INFO_FROM_VECTOR = -1 (BLIP_COLOUR_FREEMODE)")
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				
				//Player created.
				DEFAULT
					CPRINTLN(DEBUG_PAUSE_MENU, "MAP - MISSION DATA - X")
					CPRINTLN(DEBUG_PAUSE_MENU, "GET_MISSION_INFO_FROM_VECTOR - NOT SURE WHAT BLIP IS")
				BREAK
			ENDSWITCH
		ELSE
			CPRINTLN(DEBUG_PAUSE_MENU, "GET_MISSION_INFO_FROM_VECTOR - GET_BLIP_INFO_ID_DISPLAY(biPassed) = DISPLAY_NOTHING")
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_ARENA_CREATOR_MAP()	
	IF IS_PAUSE_MENU_ACTIVE()
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
	ENDIF
ENDPROC

FUNC INT GET_SERIES_FROM_BLIP(BLIP_INDEX biPassed)

	BLIP_SPRITE blipSprite = GET_BLIP_SPRITE(biPassed)
	HUD_COLOURS hudColour = GET_HUD_COLOUR_FROM_BLIP_COLOUR(GET_BLIP_COLOUR(biPassed))

	IF hudColour != HUD_COLOUR_PINKLIGHT
		RETURN -1
	ENDIF

	SWITCH blipSprite
		// Stunt Series
		CASE RADAR_TRACE_STUNT
			RETURN ciCV2_SERIES_STUNT
			
		// Premiumn Race
		CASE RADAR_TRACE_STUNT_PREMIUM
			RETURN ciCV2_SERIES_PREMIUM_RACE
			
		// Featured Series
		CASE RADAR_TRACE_FEATURED_SERIES
			RETURN ciCV2_SERIES_FEATURED
			
		// Adversary Series (16 Player)
		CASE RADAR_TRACE_ADVERSARY_16
			RETURN ciCV2_SERIES_ADVERSARY_LARGE
			
		// Adversary Series (8 Player)
		CASE RADAR_TRACE_ADVERSARY_8
			RETURN ciCV2_SERIES_ADVERSARY_MEDIUM
			
		// Adversary Series (4 Player)
		CASE RADAR_TRACE_ADVERSARY_4
			RETURN ciCV2_SERIES_ADVERSARY_SMALL
			
		// Special Vehicle Series
		CASE RADAR_TRACE_STEERINGWHEEL
			RETURN ciCV2_SERIES_SPECIAL_VEHICLE
			
		// Bunker Series
		CASE RADAR_TRACE_ADVERSARY_BUNKER
			RETURN ciCV2_SERIES_BUNKER
			
		// Transform Series
		CASE RADAR_TRACE_RACE_TF
			RETURN ciCV2_SERIES_TRANSFORM
			
		// San Andreas Super Sport Series (Hotring)
		CASE RADAR_TRACE_ACSR_RACE_HOTRING
			RETURN ciCV2_SERIES_SAN_ANDREAS_SUPER_SPORT
			
		// Arena Series
		CASE RADAR_TRACE_ARENA_SERIES
			RETURN ciCV2_SERIES_ARENA_WAR
			
		// Race Series
		CASE RADAR_TRACE_RACE_LAND
			IF hudColour = HUD_COLOUR_PINKLIGHT
				RETURN ciCV2_SERIES_RACE
			ENDIF
		BREAK
		
		// Survival Series
		CASE RADAR_TRACE_HORDE
			IF hudColour = HUD_COLOUR_PINKLIGHT
				RETURN ciCV2_SERIES_SURVIVAL
			ENDIF
		BREAK
		
		// Open Wheel Series
		CASE RADAR_TRACE_RACE_OPEN_WHEEL
			RETURN ciCV2_SERIES_OPEN_WHEEL
			
		// Street Race Series
		CASE RADAR_TRACE_STREET_RACE_SERIES
			RETURN ciCV2_SERIES_STREET_RACE
			
		// Pursuit Series
		CASE RADAR_TRACE_PURSUIT_SERIES
			RETURN ciCV2_SERIES_PURSUIT
			
		// HSW Race Series
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE RADAR_TRACE_HSW_RACE_SERIES
			RETURN ciCV2_SERIES_HSW_RACE
		#ENDIF
			
		CASE RADAR_TRACE_COMMUNITY_SERIES
			RETURN ciCV2_SERIES_COMMUNITY
			
		CASE RADAR_TRACE_CAYO_SERIES
			RETURN ciCV2_SERIES_CAYO_PERICO
			
	ENDSWITCH

	RETURN -1
ENDFUNC

FUNC BOOL REQUEST_AND_LOAD_SERIES_BLIP_INFORMATION(INT iSeries)
	UNUSED_PARAMETER(iSeries)
	RETURN TRUE
ENDFUNC

PROC DISPLAY_SERIES_BLIP_INFORMATION(INT iSeries)
		
	TAKE_CONTROL_OF_FRONTEND()
	
	INT iColumn = ciDLC_CONTENT_PM_COLUMN
	INT iIndex = 0
	INT iMenuID = ENUM_TO_INT(MENU_UNIQUE_ID_MISSION_CREATOR_LISTITEM)
	
	SHOW_CONTACT_INSTRUCTIONAL_BUTTON(FALSE)
	SHOW_START_MISSION_INSTRUCTIONAL_BUTTON(TRUE)
	
	SHOW_PM_COLUMN(iColumn)
		
	// IMAGE (with the pack name in the overlay at the bottom)
	SET_FRONTEND_DETAILS_TITLE(iColumn, "", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_SERIES_NAME(iSeries)), DEFAULT, GET_DLC_CONTENT_TEXTURE_DICT(), GET_SERIES_TEXTURE_STRING(iSeries))
		
	// Show race class or vehicle model for Premium race
	IF iSeries = ciCV2_SERIES_PREMIUM_RACE
	AND g_iCurrentPremiumRacePos != -1
	
		// Vehicle Model
		IF g_sMPTunables.iFmCoronaPlaylistProffesionalRaceClass[g_iCurrentPremiumRacePos] > FMMC_VEHICLE_UPDATE_FUTURE_VEHICLES
		OR g_sMPTunables.iFmCoronaPlaylistProffesionalRaceClass[g_iCurrentPremiumRacePos] < 0
	
			PM_SET_DATA_SLOT_FOR_STAT(			iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE,				"FMMCCPD_CHG", 						GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iFmCoronaPlaylistProffesionalRaceClass[g_iCurrentPremiumRacePos])))
			iIndex++
	
		// Vehicle Class
		ELSE
		
			PM_SET_DATA_SLOT_FOR_STAT(			iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE,				"FMMC_VEH_CL", 						CV2_GET_ACTIVE_RACE_CLASS_STRING(g_sMPTunables.iFmCoronaPlaylistProffesionalRaceClass[g_iCurrentPremiumRacePos]))
			iIndex++
		
		ENDIF
		
	ENDIF
		
	// PLAYERS
	IF GET_SERIES_MIN_PLAYERS(iSeries) = GET_SERIES_MAX_PLAYERS(iSeries)
		PM_SET_DATA_SLOT_FOR_STAT_WITH_NUMBER(	iColumn, 	iIndex, 	iMenuID, 	iIndex, 	"PM_PLAYERS", 		"NUMBER",							GET_SERIES_MIN_PLAYERS(iSeries))
		iIndex++
	ELSE
		PM_SET_DATA_SLOT_FOR_PLAYERS(			iColumn, 	iIndex, 	iMenuID, 	iIndex, 	"PM_PLAYERS", 		GET_SERIES_MIN_PLAYERS(iSeries), 	GET_SERIES_MAX_PLAYERS(iSeries))
		iIndex++
	ENDIF
		
	// TYPE
	PM_SET_DATA_SLOT_FOR_STAT(					iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE,				"DLCC_TYPE", 						GET_SERIES_TYPE_STRING(iSeries))
	iIndex++
	
	// BREAK --------------------------------------------------------------------------------------------------------------------------------------------
	PM_SET_DATA_SLOT(							iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE, 				" ", 								FALSE, 							4) // Type 4 to show white bar
	iIndex++
	
	// ROLLOVER
	PM_SET_DATA_SLOT(							iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE, 	GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_SERIES_ROLLOVER_COPY(iSeries)), 	TRUE, 5)
	iIndex++
	
	PM_DISPLAY_DATA_SLOT(iColumn)
	
	RELEASE_CONTROL_OF_FRONTEND()
	
ENDPROC

FUNC DLC_CONTENT GET_DLC_CONTENT_FROM_BLIP(BLIP_INDEX biBlip)

//	INT 		blipColour 		= GET_BLIP_COLOUR(biBlip)
//	HUD_COLOURS blipHudColour 	= GET_HUD_COLOUR_FROM_BLIP_COLOUR(blipColour)
//	STRING 		blipName 		= GET_BLIP_NAME(biBlip)
	BLIP_SPRITE blipSprite 		= GET_BLIP_SPRITE(biBlip)
	VECTOR 		blipCoords 		= GET_BLIP_COORDS(biBlip)

	SWITCH blipSprite
		// Heist
		CASE RADAR_TRACE_MP_HEIST
			RETURN eDLCCONTENT_HEISTS
			
		CASE RADAR_TRACE_MP_LAMAR
			IF ARE_VECTORS_ALMOST_EQUAL(blipCoords, GET_DLC_CONTENT_BLIP_COORD(eDLCCONTENT_THE_CONTRACT), 5.0)
				RETURN eDLCCONTENT_THE_CONTRACT
			ENDIF
			RETURN eDLCCONTENT_LOWRIDER
			
		CASE RADAR_TRACE_SECUROSERV
			RETURN eDLCCONTENT_FAIFAF
			
		CASE RADAR_TRACE_MICHAEL_FAMILY
			RETURN eDLCCONTENT_BIKER
			
		CASE RADAR_TRACE_ARMENIAN_FAMILY
			RETURN eDLCCONTENT_GUNRUNNING
			
		// Smuggler
		CASE RADAR_TRACE_RON
			RETURN eDLCCONTENT_SMUGGLER
			
		CASE RADAR_TRACE_HEIST_DOOMSDAY
		CASE RADAR_TRACE_HEIST_DOOMSDAY_UNAVAILABLE
			RETURN eDLCCONTENT_DOOMSDAY_HEIST
			
		CASE RADAR_TRACE_TREVOR_FAMILY
			RETURN eDLCCONTENT_AFTER_HOURS
			
		CASE RADAR_TRACE_FBI_OFFICERS_STRAND
			RETURN eDLCCONTENT_ARENA_WAR
			
		CASE RADAR_TRACE_CASINO
			RETURN eDLCCONTENT_DIAMOND_CASINO_AND_RESORT
			
		CASE RADAR_TRACE_HEIST_DIAMOND
		CASE RADAR_TRACE_HEIST_DIAMOND_UNAVAILABLE
			RETURN eDLCCONTENT_DIAMOND_CASINO_HEIST
			
		CASE RADAR_TRACE_HEIST_ISLAND
		CASE RADAR_TRACE_HEIST_ISLAND_UNAVAILABLE
			RETURN eDLCCONTENT_CAYO_PERICO_HEIST
	
		CASE RADAR_TRACE_MARTIN_MADRAZZO
			RETURN eDLCCONTENT_MADRAZO_DISPATCH
			
		CASE RADAR_TRACE_SIMEON_FAMILY
			RETURN eDLCCONTENT_SIMEON_REPO
			
		CASE RADAR_TRACE_GERALD
			RETURN eDLCCONTENT_GERALDS_LAST_PLAY
			
		CASE RADAR_TRACE_CAR_MEET
			RETURN eDLCCONTENT_LOS_SANTOS_TUNERS
			
		#IF FEATURE_DLC_1_2022
		CASE RADAR_TRACE_AGENT_ULP
			RETURN eDLCCONTENT_ULP_MISSIONS
		
		CASE RADAR_TRACE_CAR_SHOWROOM_SIMEON
			RETURN eDLCCONTENT_PREMIUM_DELUXE_MOTORSPORT
		BREAK
		#ENDIF
			
	ENDSWITCH
	
	RETURN eDLCCONTENT_INVALID
ENDFUNC

FUNC BOOL IS_DLC_CONTENT_BLIP(BLIP_INDEX biBlip)
	RETURN GET_DLC_CONTENT_FROM_BLIP(biBlip) != eDLCCONTENT_INVALID
ENDFUNC

FUNC BOOL REQUEST_AND_LOAD_DLC_CONTENT_BLIP_INFORMATION(DLC_CONTENT eContent)
	UNUSED_PARAMETER(eContent)
	RETURN TRUE
ENDFUNC

PROC DISPLAY_DLC_CONTENT_BLIP_INFORMATION(DLC_CONTENT eContent, BOOL &bShowStartMission, DOWNLOAD_PHOTO_DATA &DLPhotoData)
	
	TAKE_CONTROL_OF_FRONTEND()
	
	// We have textures to show here, so reset any loading of previous job images
	DLPhotoData.bLoadMissionPhoto 		= FALSE
	DLPhotoData.bInitLoadMissionPhoto 	= FALSE
	
	INT iColumn = ciDLC_CONTENT_PM_COLUMN
	INT iIndex = 0
	INT iMenuID = ENUM_TO_INT(MENU_UNIQUE_ID_MISSION_CREATOR_LISTITEM)
	
	SHOW_START_MISSION_INSTRUCTIONAL_BUTTON(FALSE)
	
	IF g_sDLCContentContollerStruct.iTriggerBitset = 0
	AND DOES_DLC_CONTENT_HAVE_CORRESPONDANCE(eContent)
		SHOW_START_MISSION_INSTRUCTIONAL_BUTTON(FALSE)
		SHOW_CONTACT_INSTRUCTIONAL_BUTTON(TRUE)
		bShowStartMission = FALSE
	ENDIF
	
	SHOW_PM_COLUMN(iColumn)
		
	// IMAGE (with the pack name in the overlay at the bottom)
	SET_FRONTEND_DETAILS_TITLE(iColumn, "", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DLC_CONTENT_PACK_NAME(eContent)), DEFAULT, GET_DLC_CONTENT_TEXTURE_DICT(), GET_DLC_CONTENT_TEXTURE_STRING(eContent))
		
	// CONTACT
	PM_SET_DATA_SLOT_FOR_STAT(					iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE,		"DLCC_CONTACT", 			GET_DLC_CONTENT_CONTACT_STRING(eContent))
	iIndex++
	
	// PROPERTY
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_DLC_CONTENT_PROPERTY_STRING(eContent))
		PM_SET_DATA_SLOT_FOR_STAT(				iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE,		"DLCC_PROPERTY", 		GET_DLC_CONTENT_PROPERTY_STRING(eContent))
		iIndex++
	ENDIF
	
	// WEBSITE
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_DLC_CONTENT_WEBSITE_STRING(eContent))
		PM_SET_DATA_SLOT_FOR_STAT(				iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE,		"DLCC_WEBSITE", 			GET_DLC_CONTENT_WEBSITE_STRING(eContent))
		iIndex++
	ENDIF
	
	// TYPE
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_DLC_CONTENT_TYPE_STRING(eContent))
		PM_SET_DATA_SLOT_FOR_STAT(				iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE,		"DLCC_TYPE", 			GET_DLC_CONTENT_TYPE_STRING(eContent))
		iIndex++
	ENDIF
		
	// BREAK --------------------------------------------------------------------------------------------------------------------------------------------
	PM_SET_DATA_SLOT(							iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE, 	" ", 				FALSE, 							4) // Type 4 to show white bar
	iIndex++
	
	PM_SET_DATA_SLOT(							iColumn, 	iIndex, 	iMenuID, 	iIndex, 	FALSE, 	GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DLC_CONTENT_ROLLOVER_COPY(eContent)), 	TRUE, 5)
	iIndex++
	
	PM_DISPLAY_DATA_SLOT(iColumn)
	
	RELEASE_CONTROL_OF_FRONTEND()
	
ENDPROC


