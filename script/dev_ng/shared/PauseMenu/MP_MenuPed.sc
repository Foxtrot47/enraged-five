/***********************************
*	Name: MP_MenuPed.sc
*	Author: Brenda Carey
*	Date: 15/10/2012
***********************************/

USING "PauseMenu_public.sch"
USING "net_include.sch"
USING "Screens_Header.sch"
USING "fm_in_corona_header.sch"
USING "FMMC_Corona_Controller.sch"
USING "fmmc_request_to_play_mission.sch"

// ************************************************************
// ******************** MAIN SCRIPT LOOP **********************
// ************************************************************
/// PURPOSE:
///    Populates frontend menu with relevant list of players (gamer handles used)
PROC FILL_OUT_PAUSE_MENU_PLAYER_LIST(INT iMenuID)

	
	INT i
	INT iCount = 0
	GAMER_HANDLE joinedPlayersGH[NUM_NETWORK_PLAYERS]
	
	IF iMenuID = ENUM_TO_INT(MENU_UNIQUE_ID_CORONA_INVITE_LAST_JOB_PLAYERS)
		// Populate last job list

		TEXT_LABEL_63 joinedPlayerNames[NUM_NETWORK_PLAYERS]
	
		SWITCH GET_CORONA_STATUS()
			CASE CORONA_STATUS_LAST_HEIST_PLAYERS
				// Populate with last job players
				REPEAT NUM_NETWORK_PLAYERS i
					IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
						IF IS_GAMER_HANDLE_VALID(lastGangOpsHeistPlayers.Gamers[i])
						AND NOT IS_STRING_NULL_OR_EMPTY(TEXT_LABEL_TO_STRING(lastGangOpsHeistPlayers.GamersNames[i]))
							CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: FILL_OUT_PAUSE_MENU_PLAYER_LIST (MENU_UNIQUE_ID_CORONA_INVITE_LAST_HEIST_PLAYERS). - GANG OPS - Adding GAMER HANDLE: ", iCount, " name: ", lastGangOpsHeistPlayers.GamersNames[i])
							
							joinedPlayersGH[iCount] = lastGangOpsHeistPlayers.Gamers[i]
							joinedPlayerNames[iCount] = lastGangOpsHeistPlayers.GamersNames[i]
							iCount++
							
						ENDIF	
					ELSE
						IF IS_GAMER_HANDLE_VALID(lastHeistPlayers.Gamers[i])
						AND NOT IS_STRING_NULL_OR_EMPTY(TEXT_LABEL_TO_STRING(lastHeistPlayers.GamersNames[i]))
							CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: FILL_OUT_PAUSE_MENU_PLAYER_LIST (MENU_UNIQUE_ID_CORONA_INVITE_LAST_HEIST_PLAYERS). Adding GAMER HANDLE: ", iCount, " name: ", lastHeistPlayers.GamersNames[i])
							
							joinedPlayersGH[iCount] = lastHeistPlayers.Gamers[i]
							joinedPlayerNames[iCount] = lastHeistPlayers.GamersNames[i]
							iCount++
							
						ENDIF	
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE CORONA_STATUS_LAST_JOB_PLAYERS
			DEFAULT
				// Populate with last job players
				REPEAT NUM_NETWORK_PLAYERS i
					IF IS_GAMER_HANDLE_VALID(lastJobPlayers.Gamers[i])
						
						CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: FILL_OUT_PAUSE_MENU_PLAYER_LIST (MENU_UNIQUE_ID_CORONA_INVITE_LAST_JOB_PLAYERS). Adding GAMER HANDLE: ", iCount)
						
						joinedPlayersGH[iCount] = lastJobPlayers.Gamers[i]
						joinedPlayerNames[iCount] = lastJobPlayers.GamersNames[i]
						iCount++
						
					ENDIF	
				ENDREPEAT
			BREAK
	
		ENDSWITCH

		CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: args.operation = kFill FILLOUT_PM_PLAYER_LIST_WITH_NAMES. With Count: ", iCount)
				
		REFRESH_PLAYER_LIST_STATS(PLAYER_LIST_TYPE_LAST_JOB)
		FILLOUT_PM_PLAYER_LIST_WITH_NAMES(joinedPlayersGH, joinedPlayerNames, iCount, PLAYER_LIST_TYPE_LAST_JOB)
	ELIF iMenuID = ENUM_TO_INT(MENU_UNIQUE_ID_CORONA_INVITE_PLAYERS)
	
		// Populate players in session list
		
		PLAYER_INDEX playerID
		GAMER_HANDLE aGamerHandle
		REPEAT NUM_NETWORK_PLAYERS i
			
			playerID = INT_TO_PLAYERINDEX(i)
			
			IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
				
				IF HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(playerID)
				
					aGamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
				
					IF IS_GAMER_HANDLE_VALID(aGamerHandle)
						
						IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(aGamerHandle)
						
							CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: FILL_OUT_PAUSE_MENU_PLAYER_LIST (MENU_UNIQUE_ID_CORONA_INVITE_PLAYERS). Adding GAMER HANDLE: ", iCount, " for Player: ", GET_PLAYER_NAME(playerID))
							
							joinedPlayersGH[iCount] = aGamerHandle
							iCount++
						
						#IF IS_DEBUG_BUILD
						ELSE
							CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: args.operation = kFill MENU_UNIQUE_ID_CORONA_INVITE_PLAYERS. Player already in corona ", iCount, " for Player: ", GET_PLAYER_NAME(playerID))
						#ENDIF
							
						ENDIF						
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT

		CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: args.operation = kFill MENU_UNIQUE_ID_CORONA_INVITE_PLAYERS. With Count: ", iCount)
		
		REFRESH_PLAYER_LIST_STATS(PLAYER_LIST_TYPE_INVITABLE_SESSION_PLAYERS)
		FILLOUT_PM_PLAYER_LIST(joinedPlayersGH, iCount, PLAYER_LIST_TYPE_INVITABLE_SESSION_PLAYERS)
		
	ELIF iMenuID = ENUM_TO_INT(MENU_UNIQUE_ID_CORONA_INVITE_MATCHED_PLAYERS)
		
		// Populate matched players
		IF matchedPlayers.bFinished
			TEXT_LABEL_63 joinedPlayerNamesM[NUM_NETWORK_PLAYERS]
		
			REPEAT NUM_NETWORK_PLAYERS i
				
				IF IS_GAMER_HANDLE_VALID(matchedPlayers.Gamers[i])
	
					CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: FILL_OUT_PAUSE_MENU_PLAYER_LIST (MENU_UNIQUE_ID_CORONA_INVITE_MATCHED_PLAYERS). Adding GAMER HANDLE: ", iCount, " name: ", matchedPlayers.GamersNames[i])
						
					joinedPlayersGH[iCount] = matchedPlayers.Gamers[i]
					joinedPlayerNamesM[iCount] = matchedPlayers.GamersNames[i]
					iCount++

				ENDIF
			ENDREPEAT

			CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: args.operation = kFill MENU_UNIQUE_ID_CORONA_INVITE_MATCHED_PLAYERS. With Count: ", iCount)
			
			REFRESH_PLAYER_LIST_STATS(PLAYER_LIST_TYPE_MATCHED_PLAYERS)
			FILLOUT_PM_PLAYER_LIST_WITH_NAMES(joinedPlayersGH, joinedPlayerNamesM, iCount, PLAYER_LIST_TYPE_MATCHED_PLAYERS)
		#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: args.operation = kFill MENU_UNIQUE_ID_CORONA_INVITE_MATCHED_PLAYERS. List not ready")
		#ENDIF	
		
		ENDIF
		
	ELSE
		// Populate joined player list
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 strName
		#ENDIF
		
		REPEAT GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION() i
	
			// Is the players gamer handle valid
			IF IS_GAMER_HANDLE_VALID(GET_TRANSITION_PLAYER_GAMER_HANDLE(i))
			AND (IS_TRANSITION_PLAYER_READY_TO_LAUNCH_JOB(i)
				OR IS_THIS_TRANSITION_SESSION_PLAYER_ON_CALL(i))
			AND NOT IS_THIS_TRANSITION_SESSION_PLAYER_A_ROCKSTAR_DEV_SPECTATOR(i)
				
				#IF IS_DEBUG_BUILD
					strName = GET_TRANSITION_PLAYER_GAMERTAG(i)
					CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: FILL_OUT_PAUSE_MENU_PLAYER_LIST (MENU_UNIQUE_ID_CORONA_INVITE_JOINED_PLAYERS). Adding GAMER HANDLE: ", iCount, " name: ", strName)
				#ENDIF
				
				joinedPlayersGH[iCount] = GET_TRANSITION_PLAYER_GAMER_HANDLE(i)
				iCount++
			ENDIF

		ENDREPEAT
		
		// Default the player list type
		PLAYER_LIST_TYPE ePlayerListType = PLAYER_LIST_TYPE_JOINED
		
		// If we are in a launched transition session situation we should use the new player list type
		IF HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
		OR HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			ePlayerListType = PLAYER_LIST_TYPE_CORONA_PLAYLIST
			CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: Override player list type to PLAYER_LIST_TYPE_CORONA_PLAYLIST for a launched transition session")
		ENDIF		
		
		CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: args.operation = kFill FILL_OUT_PAUSE_MENU_PLAYER_LIST. With Count: ", iCount)
		
		IF ePlayerListType = PLAYER_LIST_TYPE_JOINED
			REFRESH_PLAYER_LIST_STATS(ePlayerListType)
		ENDIF
		FILLOUT_PM_PLAYER_LIST(joinedPlayersGH, iCount, ePlayerListType)

		GAMER_HANDLE onCall
		REPEAT GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION() i
	
			IF IS_THIS_TRANSITION_SESSION_PLAYER_ON_CALL(i)
				onCall = GET_TRANSITION_PLAYER_GAMER_HANDLE(i)
				IF IS_GAMER_HANDLE_VALID(onCall)
				
					#IF IS_DEBUG_BUILD
						strName = GET_TRANSITION_PLAYER_GAMERTAG(i)
						CPRINTLN(DEBUG_PAUSE_MENU, "[TS] MP_MENUPED = PLAYER NOW ON CALL, notify code. strName = ", strName)
					#ENDIF	
						
					NETWORK_SET_INVITE_ON_CALL_FOR_INVITE_MENU(onCall)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC DISPLAY_BUSYSPIN(INT MenuInt, BOOL isActive)

	FRONTEND_MENU_SCREEN MenuItem = INT_TO_ENUM(FRONTEND_MENU_SCREEN, MenuInt)

	SWITCH MenuItem
		
		CASE MENU_UNIQUE_ID_CORONA_INVITE_FRIENDS
		CASE MENU_UNIQUE_ID_FRIENDS_MP
		CASE MENU_UNIQUE_ID_CREW
			PAUSE_MENU_SLOT_BUSY_SPINNER(isActive, 1)
		BREAK 
		
		CASE MENU_UNIQUE_ID_CORONA_INVITE
			PAUSE_MENU_SLOT_BUSY_SPINNER(isActive, 2)
		BREAK
	
	ENDSWITCH

ENDPROC


SCRIPT( PAUSE_MENU_LAUNCH_DATA args ) 

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_PAUSE_MENU, "MP_MenuPed Select Menu Launched with args: ", args.operation, ", Menu: ", args.MenuScreenId, ", Prev:", args.PreviousId, ", Unique: ", args.UniqueIdentifier)
	#ENDIF
		
	INT iSlot = 0

	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_PAUSE_MENU_TERMINATED) 
          
		NET_NL()NET_PRINT("MP_MENUPED: HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_PAUSE_MENU_TERMINATED)  ")NET_NL() 
		  
		
		DELETE_ALL_PARENT_PAUSE_MENU_PED( iSlot)
		IF NOT IS_PED_INJURED(g_CharPedCreation[0].MenuPed)
			NET_NL()NET_PRINT("MP_MENUPED: DELETE_PED  ")NET_NL() 

			DELETE_PED(g_CharPedCreation[0].MenuPed)
		ENDIF
		RESET_ALL_CHAR_PED_CREATION()                  
        g_bMissionCreatorPauseMenuActive = FALSE 
        TERMINATE_THIS_THREAD() 
    ENDIF 
	
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	
	

	
	GAMER_HANDLE gamerToInvite
	PLAYER_INDEX playerToInvite
	GAMER_HANDLE aGamer
	INT crewRed, crewGreen, crewBlue
	#IF IS_DEBUG_BUILD
//	STRING friendName
	#ENDIF



	SWITCH args.operation
	
		CASE kPopulatePeds
	
			crewRed = -1
			crewGreen = -1 
			crewBlue = -1
			 
			DELETE_ALL_PARENT_PAUSE_MENU_PED( iSlot)
			DELETE_CHILD_PED(g_CharPedCreation[0].MenuPed)
			RESET_ALL_CHAR_PED_CREATION()
			
			DISPLAY_BUSYSPIN(args.MenuScreenId, TRUE)
			
			NET_NL()NET_PRINT("MP_MENUPED: kPopulatePeds Running ")NET_PRINT_INT(iSlot)NET_NL()
			
		
			IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MP_MenuPed FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT - PAUSE_MENU_ACTIVATE_CONTEXT(HASH(\"DisableSpectateScript\"))")
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DisableSpectateScript"))
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "[FM AME] - MP_MenuPed FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT - PAUSE_MENU_DEACTIVATE_CONTEXT(HASH(\"DisableSpectateScript\"))")
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DisableSpectateScript"))
			ENDIF
			
			
			WAIT(0)
			WAIT(0)
			
			WHILE NETWORK_GET_CURRENTLY_SELECTED_GAMER_HANDLE_FROM_INVITE_MENU(aGamer) = FALSE
				NET_NL()NET_PRINT("MP_MENUPED: NETWORK_GET_CURRENTLY_SELECTED_GAMER_HANDLE_FROM_INVITE_MENU ")NET_PRINT_INT(iSlot)NET_NL()
				WAIT(0)
			ENDWHILE

//			#IF IS_DEBUG_BUILD
//			IF IS_GAMER_HANDLE_VALID(aGamer)
//				friendName = NETWORK_GET_GAMERTAG_FROM_HANDLE(aGamer)
//				NET_NL()NET_PRINT("MP_MENUPED: friendName = ")NET_PRINT(friendName)
//			ENDIF
//			#ENDIF
			
//			IF IS_STRING_EMPTY_HUD(friendName)
//				CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch -IS_STRING_EMPTY(friendName) killing thread ")
//				TERMINATE_THIS_THREAD()
//			ENDIF
//			
//			WHILE NETWORK_CLAN_DOWNLOAD_MEMBERSHIP(aGamer) = FALSE
//				NET_NL()NET_PRINT("MP_MENUPED: NETWORK_GET_CURRENTLY_SELECTED_GAMER_HANDLE_FROM_INVITE_MENU ")NET_PRINT_INT(iSlot)NET_NL()
//				WAIT(0)
//			ENDWHILE
			
			WHILE GET_PM_PLAYER_CREW_COLOR(crewRed, crewGreen,crewBlue ) = FALSE
				NET_NL()NET_PRINT("MP_MENUPED: GET_PM_PLAYER_CREW_COLOR ")NET_PRINT_INT(iSlot)NET_NL()
				WAIT(0)
			ENDWHILE
			
//			GET_GAMER_CLAN_COLOUR(aGamer, crewRed, crewGreen, crewBlue)
			NET_NL()NET_PRINT("MP_MENUPED: GET_GAMER_CLAN_COLOUR Red =  ")NET_PRINT_INT(crewRed)NET_PRINT(" Green =  ")NET_PRINT_INT(crewGreen)NET_PRINT(" Blue =  ")NET_PRINT_INT(crewBlue)NET_NL()

			WHILE GENERATE_PAUSE_MENU_PED(aGamer, g_CharPedCreation[0].MenuPed, iSlot, crewRed, crewGreen, crewBlue) = FALSE
			
				NET_NL()NET_PRINT("MP_MENUPED: GENERATE_PAUSE_MENU_PED BEFORE WAIT ")NET_PRINT_INT(iSlot)NET_NL()
				WAIT(0)
				NET_NL()NET_PRINT("MP_MENUPED: GENERATE_PAUSE_MENU_PED AFTER WAIT ")NET_PRINT_INT(iSlot)NET_NL()
			ENDWHILE
			
			DELETE_ALL_PARENT_PAUSE_MENU_PED( iSlot)
			
			WAIT(0)
			WAIT(0)
			
			NET_NL()NET_PRINT("MP_MENUPED: CREATED PED for slot ")NET_PRINT_INT(iSlot)NET_NL()
			
			DISPLAY_BUSYSPIN(args.MenuScreenId, FALSE)
			
			FINALIZE_HEAD_BLEND(g_CharPedCreation[0].MenuPed)
			GIVE_PED_TO_PAUSE_MENU(g_CharPedCreation[0].MenuPed)
			
			RESET_ALL_CHAR_PED_CREATION()
				
				
	
//			ENDIF


			NET_NL()NET_PRINT("MP_MENUPED: WHILE args.operation = kPopulatePeds ")
				
		BREAK
	
		CASE kUpdateInput
			CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: args.operation = kUpdateInput (INVITE / KICK / CANCEL INVITE)")

			IF args.PreviousId = ENUM_TO_INT(INPUT_FRONTEND_X)
				
				CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: WHILE args.operation = kUpdateInput KICK PLAYER / CANCEL INVITE square pressed")
				
				IF NETWORK_GET_CURRENTLY_SELECTED_GAMER_HANDLE_FROM_INVITE_MENU(gamerToInvite)
				
					playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerToInvite)
					IF playerToInvite != player_id()
						IF IS_GAMER_HANDLE_VALID(gamerToInvite)
							IF IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(gamerToInvite)
							
								IF NOT IS_CORONA_BIT_SET(CORONA_CLIENT_HAS_ACCESSED_JOINED_PLAYERS)
								AND IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
								
									CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: KICK - Gamer is in our transition session")
									
									NETWORK_SEND_TRANSITION_GAMER_INSTRUCTION(gamerToInvite, "", TRANSITION_SESSION_INSTRUCTION_KICK, TRANSITION_SESSION_INSTRUCTION_DEFAULT_PARAM, FALSE)
									
									PLAY_DEFERRED_SOUND_FRONTEND("BACK","HUD_FREEMODE_SOUNDSET")
							
									REMOVE_INVITED_GAMER_TO_CORONA_LIST(gamerToInvite, "")
								
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
				
			ENDIF

			// Detect the button press of the player:
			IF args.PreviousId = ENUM_TO_INT(INPUT_FRONTEND_ACCEPT)

				CPRINTLN(DEBUG_PAUSE_MENU, "MP_MENUPED: WHILE args.operation = kUpdateInput SEND INVITE triangle pressed")
				
				// Update global so player is locked in.
				SET_PLAYER_HAS_SENT_AN_INVITE(TRUE)
				
				IF NETWORK_GET_CURRENTLY_SELECTED_GAMER_HANDLE_FROM_INVITE_MENU(gamerToInvite)
					playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerToInvite)
					IF playerToInvite != player_id()
						IF IS_GAMER_HANDLE_VALID(gamerToInvite)
							IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerToInvite)
								IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(gamerToInvite)
									//IF NETWORK_IS_FRIEND(gamerToInvite)
									//	IS_FRIEND_ONLINE
									//	CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch -NETWORK_INVITE_GAMER_TO_TRANSITION not is session attempting to send an invite")
									//ELSE
										CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch -NETWORK_INVITE_GAMER_TO_TRANSITION not is session attempting to send an invite")
										crossSessionInvite.gamerToInvite = gamerToInvite
										crossSessionInvite.iStage = 1
									//ENDIF
								ELSE
									CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch -NETWORK_INVITE_GAMER_TO_TRANSITION NOT Inviting gamer to transition session they are already in it one player")
								ENDIF						
							ELSE
							
								playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerToInvite)
								IF playerToInvite <> INVALID_PLAYER_INDEX()
									IF IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)
										IF NETWORK_CAN_SEND_LOCAL_INVITE(gamerToInvite)
											//iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
											
											BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
																		sendInviteDetailsStruct.iType,
																		sendInviteDetailsStruct.iCreatorID,
																		0,
																		sendInviteDetailsStruct.vCoronaLocation,
																		sendInviteDetailsStruct.sContentID)
											CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch - Inviting player to corona: ", GET_PLAYER_NAME(playerToInvite))
											
											// See if we have a valid slot to save this invited player
											ADD_INVITED_PLAYER_TO_CORONA_LIST(0, gamerToInvite, GET_PLAYER_NAME(playerToInvite))
										ELSE
											CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch - NOT Inviting player to corona blocked: ", GET_PLAYER_NAME(playerToInvite))
											
											INT bitfield
											SET_BIT(bitfield, ENUM_TO_INT(INVITESTATUS_BLOCKED))
											
											STRUCT_PRESENCE_INVITE_REPLY_EVENT sei
											sei.hInviter = gamerToInvite
											sei.nInviteStatus = INT_TO_ENUM(INVITE_STATUS, bitfield)
											sei.bDecisionMade = FALSE
											sei.bDecision = FALSE
											
											PROCESS_EVENT_INVITE_REPLY_COMMON(sei)
										ENDIF
									ELSE
										CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch - not inviting player to corona not in playing state yet")
									ENDIF	
								ELSE
									CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch - playerToInvite = INVALID_PLAYER_INDEX")
								ENDIF
							ENDIF
						ELSE
							CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch - gamer handle invalid for invite")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch - trying to invite yourself??")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_PAUSE_MENU, "failed to get currently selected gamer")
				ENDIF
			ENDIF
			
		BREAK
		
		CASE kFill
		
			FILL_OUT_PAUSE_MENU_PLAYER_LIST(args.MenuScreenId)

		BREAK
		
		DEFAULT 
			NET_NL()NET_PRINT("MP_MENUPED: TERMINATE THREAD - unknown type, args.operation") NET_NL()
		BREAK
	
	ENDSWITCH
	
	
	DISPLAY_BUSYSPIN(args.MenuScreenId, FALSE)

	CPRINTLN(DEBUG_PAUSE_MENU, "MP_menuped.sch -killing thread ")
	TERMINATE_THIS_THREAD()
	
	
	
	
	
	

ENDSCRIPT

