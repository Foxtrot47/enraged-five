

#IF IS_DEBUG_BUILD
USING "Shared_hud_displays.sch"
USING "Net_mission_details_hud.sch"
USING "Transition_Invites.sch"
USING "Cheat_Handler.sch"
USING "MPHud_CharacterController.sch"
USING "net_private_yacht_private.sch"
USING "rgeneral_include.sch"
#ENDIF


USING "net_process_events.sch"

USING "AM_Common_UI.sch"


USING "Transition_Common.sch"

USING "net_big_feed_player_info_screens.sch"

USING "net_heists_common.sch"

//USING "Commands_socialclub.sch"
//USING "Net_stat_Generator.sch"



#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

BOOL updateCrewMetaData = TRUE

BOOL bValidatePlayerLastGenStatus = FALSE
BOOL bValidationInProgress = FALSE

BOOL SetupCrewData 
SCRIPT_TIMER PedLeftTimer


SCRIPT_TIMER BikerAppCooldown
SCRIPT_TIMER CasinoSaveCooldown

INT RunRefreshStages
SCRIPT_TIMER aRefreshTimer
INT RefreshTotalRetryTime
NET_GAMESERVER_TRANSACTION_STATUS Refresh_network_shop_transaction_status

//SCALEFORM_LOADING_ICON SavesIconStruct
//BOOL bHasfreshedSavesIcon

RUN_SAVE_STAGES iStat_Save_System_Stages

SYSTEM_ACTIVITY_STAGES iSystem_Activity_Stages

#IF IS_DEBUG_BUILD
STRUCT_STAT_DATE BadSportTimeLeft
STRUCT_STAT_DATE BadSport_MPPLY_BECAME_BADSPORT_DT_Value
#ENDIF

INT BadSportTrackingNumDays
SCRIPT_TIMER BadSportTrackingResetTimer
FLOAT fBadSportIncreaseTickerAmount

#IF IS_DEBUG_BUILD
BOOL bDisplaySavingData 
BOOL bStartOfGameAfterLoadPrint = TRUE
BOOL bRunSplashScreenHud
BOOL bDrawArrows
//INT IntenseWidget

INT i_DeleteCharQuickIndex
INT i_DeleteCharQuickStage

BOOL bHaveHeistBonusWidgetsInit = FALSE

BOOL bShouldCTRLHDeleteBankToo = FALSE

BOOL bTurnOnBusySpinner

BOOL b_TurnOnTripleHead
BOOL b_TurnOffTripleHead

BOOL bTurnOnRenderFreeze
BOOL bTurnOffRenderFreeze
BOOL bTurnOffRenderFreezeAndEffect

BOOL bSwapModeltoBigfoot
BOOL bSwapModeltoPlayer
BOOL bchangeAfterlifetoBigfoot

BOOL bgivePlayerABeerHat

BOOL DisplayMultiplayerCashText
BOOL SetMultiplayerCashText

BOOL Set_UseFakeMPCash
BOOL UseFakeMpCash

BOOL set_changeFakeMpCash
INT iChangeFakeMpCash
//
//ENUM SORTTYPE
//	SORTTYPE_ASCENDING,
//	SORTTYPE_DESCENDING,
//	SORTTYPE_ALPHABETICAL
//ENDENUM
//
//CONST_INT MAX_NUM_SORTING_PLAYERS 10
//STRUCT SORTING_DATA
//	
//	TEXT_LABEL_63 PlayerName
//	TEXT_LABEL_63 Country
//	INT HighScore
//	INT AccountID
//	FLOAT HighestDistanceWheelie
//ENDSTRUCT
//
//SORTING_DATA AllPlayers[MAX_NUM_SORTING_PLAYERS]
//
//NATIVE PROC SORT_STRUCT_CODE(STRUCT& NotSortedStruct, INT sizeOfNotSortedStruct, STRUCT& Sortedstruct, INT sizeOfSortedStruct, INT PrimaryConditionINDEX, SORTTYPE PrimarysortType, INT SecondaryconditionINDEX, SORTTYPE asortType)  = "0x4ac86d014df5d30f"
//
//PROC SORT_LEADERBOARD(SORTING_DATA& astruct)
//		
//	SORTING_DATA SortedPlayers[MAX_NUM_SORTING_PLAYERS]
//	
//	//Sort By highScore, then Playername Alphabetically	
//	SORT_STRUCT_CODE(astruct, SIZE_OF(astruct), SortedPlayers, SIZE_OF(SortedPlayers), 2,SORTTYPE_ASCENDING ,0, SORTTYPE_ALPHABETICAL)
//
//ENDPROC


#ENDIF


//SCRIPT_TIMER st_PlayingWithFriendsTimer

FUNC BOOL IS_ANYTHING_DISPLAYING(BOOL& ArrayBools[])
	INT I
	FOR I = 0 TO NUMBER_OF_DIFFERENT_HUD_ELEMENTS-1
		IF ArrayBools[I] = TRUE
			RETURN TRUE
		ENDIF
	ENDFOR
//	SORT_LEADERBOARD(AllPlayers)
	RETURN FALSE

ENDFUNC






FUNC BOOL SHOULD_DISPLAY_OVER_PAUSE_MENU()

	IF IS_THIS_PLAYER_ACTIVE_IN_CORONA(PLAYER_ID()) 
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


PROC DISPLAY_OVER_PAUSE_MENU(BOOL isActive)

	IF SHOULD_DISPLAY_OVER_PAUSE_MENU()
		IF isActive
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		ELSE
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
		ENDIF
	ENDIF

ENDPROC




PROC RENDER_MPHUD_PEDS_ONLY(MPHUD_PLACEMENT_TOOLS& Placement)

	INT I
	
	VECTOR SlotOffset[5]
	VECTOR SlotHeading[5]


	SET_PED_SLOT_OFFSETS(SlotOffset, SlotHeading)
	
	
	VECTOR PlaceGuy, HeadGuy//, LightGuy
	VECTOR CamRot
	VECTOR CamCoord
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		CamRot = GET_CAM_ROT(GET_RENDERING_CAM())
		CamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
	ENDIF
	

	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
	
		BOOL bisSlotActive = IS_STAT_CHARACTER_ACTIVE(I)
		IF bisSlotActive = TRUE

			PlaceGuy = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord,CamRot.z, SlotOffset[I])
			HeadGuy = CamRot+SlotHeading[I] 
			
			
			
			RUN_PED_MENU(Placement.IdlePed[I], GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(GET_STAT_CHARACTER_TEAM(I), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, I)), PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(I), I, 50, FALSE, TRUE)	
			
		ENDIF
	ENDFOR

ENDPROC




PROC RESET_PLAYER_LEFT_OVERHEAD()


//	IF MPGlobalsEvents.bOverheadTagSetup = TRUE
//		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(PedLeftTimer, 4000)
//			REMOVE_PED_OVERHEAD_NAME(MPGlobalsEvents.iOverheadTagID, MPGlobalsEvents.bOverheadTagSetup, MPGlobalsEvents.bOverheadTagInitialise)		 
//		ENDIF
//	ENDIF
	
	BOOL bDoReset
	
	IF g_OverheadPed_LeftBehind_Details.iOneDisplaying
		INT I
		FOR I = 0 TO NUM_NETWORK_PLAYERS-1
			bDoReset = FALSE
			IF IS_STRING_EMPTY_HUD(g_OverheadPed_LeftBehind_Details.tl_reason[I]) = FALSE
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(PedLeftTimer, 4000)
					bDoReset = TRUE
				ENDIF
			ENDIF
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping
				bDoReset = TRUE
			ENDIF
			IF bDoReset
				g_OverheadPed_LeftBehind_Details.tl_reason[I] = ""
				REMOVE_PED_OVERHEAD_NAME(g_OverheadPed_LeftBehind_Details.iMpTag[I], g_OverheadPed_LeftBehind_Details.bSetup[I], g_OverheadPed_LeftBehind_Details.bInitialise[I])	
			ENDIF
		ENDFOR
	ENDIF

ENDPROC




FUNC BOOL SHOULD_OPEN_SAVES_ON_ACTIVITY(SAVE_TYPE aSaveType)
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF aSaveType = STAT_SAVETYPE_CRITICAL
		OR aSaveType = STAT_SAVETYPE_END_CREATE_NEWCHAR
		OR aSaveType = STAT_SAVETYPE_END_MATCH
		OR aSaveType = STAT_SAVETYPE_END_SESSION
		OR aSaveType = STAT_SAVETYPE_DELETE_CHAR
		OR aSaveType = STAT_SAVETYPE_END_SHOPPING
		OR aSaveType = STAT_SAVETYPE_END_GARAGE
			RETURN TRUE	
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC REMOVE_ALL_QUEUED_SAVES_FROM_END_MATCH_SAVE()

	IF g_bPrivate_ProcessingWhichSave != -1
	AND g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_END_MATCH
	AND  g_bPrivate_ProcessingWhichSave+1 <= MAX_NUM_SAVE_QUEUE-1
		INT I
		FOR I = g_bPrivate_ProcessingWhichSave+1 TO MAX_NUM_SAVE_QUEUE-1
			NET_NL()NET_PRINT("REMOVE_ALL_QUEUED_SAVES_FROM_END_MATCH_SAVE - CALLED")
			g_bPrivate_SaveRequested[I] = STAT_SAVETYPE_DEFAULT
			g_ePrivate_SaveReason[I] = SSR_REASON_DEFAULT
		ENDFOR
	ENDIF

ENDPROC

PROC REMOVE_ALL_QUEUED_SAVES_DUE_TO_SAVES_FAIL()
	NET_NL()NET_PRINT("[BCLOAD] REMOVE_ALL_QUEUED_SAVES_DUE_TO_SAVES_FAIL - CALLED")
	INT I
	FOR I = 0 TO MAX_NUM_SAVE_QUEUE-1
		g_bPrivate_SaveRequested[I] = STAT_SAVETYPE_DEFAULT
		g_ePrivate_SaveReason[I] = SSR_REASON_DEFAULT
	ENDFOR
	
	g_bPrivate_ProcessingWhichSave = -1
	
ENDPROC

PROC CLEAR_SAVE_SYSTEM_HELPER_FLAGS()
	//Biker app
	IF g_b_BikeAppStatChange
	OR g_b_BikeAppStatChangeMajor
		g_b_BikeAppStatChange = FALSE
		g_b_BikeAppStatChangeMajor = FALSE	
		REINIT_NET_TIMER(BikerAppCooldown)
		PRINTLN("[SAVECOOLDOWN][BIKER] Save done elsewhere now, clearing flags. ")
	ENDIF
	
	//Casino system
	IF g_b_CasinoPrioritySave
	OR g_b_CasinoSave
		g_b_CasinoPrioritySave 	= FALSE
		g_b_CasinoSave 			= FALSE	
		REINIT_NET_TIMER(CasinoSaveCooldown)
		PRINTLN("[SAVECOOLDOWN][CASINO] Save done elsewhere now, clearing flags. ")
	ENDIF
ENDPROC


PROC RUN_SYSTEM_SAVING(RUN_SAVE_STAGES& iStages)

	SWITCH iStages
		CASE RUN_SAVE_STAGES_NONE 
			g_bPrivate_IsSaveHappening = FALSE
			IF HAS_ANY_SAVE_BEEN_REQUESTED()
				g_bPrivate_IsSaveHappening = TRUE
				iStages = RUN_SAVE_STAGES_QUIT_CHECKS
			ENDIF
		BREAK
		CASE RUN_SAVE_STAGES_QUIT_CHECKS
			
				IF DOES_PLAYER_HAVE_PRIVILEGES() = FALSE
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: NETWORK_HAVE_ONLINE_PRIVILEGES = FALSE, cancelling Save. ")
					#ENDIF
					CLEAR_ALL_SAVE_REQUESTS()
					IF g_bPrivate_ProcessingWhichSave > -1
						g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_DEFAULT
						g_ePrivate_SaveReason[g_bPrivate_ProcessingWhichSave] = SSR_REASON_DEFAULT
						g_bPrivate_ProcessingWhichSave = -1
					ENDIF
					SET_SKIP_CORONA_SAVE_CHECK(FALSE)
					g_bPrivate_IsSaveHappening = FALSE
					iStages = RUN_SAVE_STAGES_NONE
				
				//url:bugstar:6080348 - Ignorable Assert - Error: Assertf(!GetBlockSaveRequests()) FAILED: FAIL SAVE REQUEST: Save requests are blocked, SAVE requested for file slot = < 0 > ...from Updating script ingamehud
				//Code now in one instance block saving so we need to check for this
				ELIF STAT_GET_BLOCK_SAVES()
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_GET_BLOCK_SAVES = TRUE, cancelling Save. ")
					#ENDIF
					CLEAR_ALL_SAVE_REQUESTS()
					IF g_bPrivate_ProcessingWhichSave > -1
						g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_DEFAULT
						g_ePrivate_SaveReason[g_bPrivate_ProcessingWhichSave] = SSR_REASON_DEFAULT
						g_bPrivate_ProcessingWhichSave = -1
					ENDIF
					SET_SKIP_CORONA_SAVE_CHECK(FALSE)
					g_bPrivate_IsSaveHappening = FALSE
					iStages = RUN_SAVE_STAGES_NONE
					
				ELIF IS_PLAYER_IN_CORONA() = TRUE
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: IS_PLAYER_IN_CORONA = TRUE, holding Save. ")
					#ENDIF
					iStages = RUN_SAVE_STAGES_HOLDINGSTATE
				
				
				ELIF IS_TRANSITION_SESSION_LAUNCHING()
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: IS_TRANSITION_SESSION_LAUNCHING = TRUE, cancelling Save. ")
					#ENDIF
					CLEAR_ALL_SAVE_REQUESTS()
					IF g_bPrivate_ProcessingWhichSave > -1
						g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_DEFAULT
						g_ePrivate_SaveReason[g_bPrivate_ProcessingWhichSave] = SSR_REASON_DEFAULT
						g_bPrivate_ProcessingWhichSave = -1
					ENDIF
					SET_SKIP_CORONA_SAVE_CHECK(FALSE)
					g_bPrivate_IsSaveHappening = FALSE
					iStages = RUN_SAVE_STAGES_NONE
				
				ELIF HAS_IMPORTANT_STATS_LOADED() = FALSE

					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: HAS_IMPORTANT_STATS_LOADED = FALSE, cancelling Save. ")
					#ENDIF
					CLEAR_ALL_SAVE_REQUESTS()
					IF g_bPrivate_ProcessingWhichSave > -1
						g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_DEFAULT
						g_ePrivate_SaveReason[g_bPrivate_ProcessingWhichSave] = SSR_REASON_DEFAULT
						g_bPrivate_ProcessingWhichSave = -1
					ENDIF
					SET_SKIP_CORONA_SAVE_CHECK(FALSE)
					g_bPrivate_IsSaveHappening = FALSE
					iStages = RUN_SAVE_STAGES_NONE
									
				ELSE
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: move to RUN_SAVE_STAGES_LOAD_IMPORTANT. ")
					#ENDIF
				
					iStages = RUN_SAVE_STAGES_LOAD_IMPORTANT
					
				ENDIF
	
			
		BREAK
		
		CASE RUN_SAVE_STAGES_HOLDINGSTATE //Hold up saving
			IF IS_PLAYER_IN_CORONA() = FALSE
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: IS_PLAYER_IN_CORONA() = FALSE so move from RUN_SAVE_STAGES_HOLDINGSTATE to RUN_SAVE_STAGES_LOAD_IMPORTANT. ")
				#ENDIF
				iStages = RUN_SAVE_STAGES_LOAD_IMPORTANT
			ENDIF
			IF SHOULD_SKIP_CORONA_SAVE_CHECK()
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: SHOULD_SKIP_CORONA_SAVE_CHECK = TRUE so move from RUN_SAVE_STAGES_HOLDINGSTATE to RUN_SAVE_STAGES_LOAD_IMPORTANT. ")
				#ENDIF
				SET_SKIP_CORONA_SAVE_CHECK(FALSE)
				iStages = RUN_SAVE_STAGES_LOAD_IMPORTANT
			ENDIF
		BREAK
		
		CASE RUN_SAVE_STAGES_LOAD_IMPORTANT
			SET_SKIP_CORONA_SAVE_CHECK(FALSE)
			IF HAS_IMPORTANT_STATS_LOADED() = FALSE
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: HAS_IMPORTANT_STATS_LOADED = FALSE, cancelling Save. ")
				#ENDIF
				CLEAR_ALL_SAVE_REQUESTS()
				g_bPrivate_IsSaveHappening = FALSE
				iStages = RUN_SAVE_STAGES_NONE
			ELSE
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: RUN_SAVE_STAGES_LOAD_IMPORTANT to  RUN_SAVE_STAGES_ERROR_CHECK. ")
				iStages = RUN_SAVE_STAGES_ERROR_CHECK
			ENDIF
			
		BREAK
		
		CASE RUN_SAVE_STAGES_ERROR_CHECK
//			g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_DEFAULT
			
			IF STAT_CLOUD_SLOT_LOAD_FAILED(0) //BLock saving if the main slot has not loaded
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(0) = TRUE, moving to safe case, cancelling Save. ")
				#ENDIF
				
				iStages = RUN_SAVE_STAGES_SKIP_SAVING
			ENDIF
			
			IF STAT_SLOT_IS_LOADED(0) = FALSE
			
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(0) = FALSE, moving to safe case, cancelling Save. ")
				#ENDIF
				
				iStages = RUN_SAVE_STAGES_SKIP_SAVING
			ENDIF
			
			IF STAT_SLOT_IS_LOADED(GET_ACTIVE_CHARACTER_SLOT()+1) = FALSE
			
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(GET_ACTIVE_CHARACTER_SLOT)+1 = FALSE, moving to safe case, cancelling Save. ")
				#ENDIF
				
				iStages = RUN_SAVE_STAGES_SKIP_SAVING
			ENDIF
			
			IF g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] != STAT_SAVETYPE_DELETE_CHAR
				IF STAT_CLOUD_SLOT_LOAD_FAILED(1) //BLock saving if the main slot has not loaded
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(1) = TRUE carrying on still. ")
					#ENDIF
					iStages = RUN_SAVE_STAGES_SKIP_SAVING
				ENDIF
				IF STAT_CLOUD_SLOT_LOAD_FAILED(2) //BLock saving if the main slot has not loaded
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(2) = TRUE carrying on still. ")
					#ENDIF
					iStages = RUN_SAVE_STAGES_SKIP_SAVING
				ENDIF
//				IF STAT_CLOUD_SLOT_LOAD_FAILED(3) //BLock saving if the main slot has not loaded
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(3) = TRUE carrying on still. ")
//					#ENDIF
//					iStages = RUN_SAVE_STAGES_SKIP_SAVING
//				ENDIF
//				IF STAT_CLOUD_SLOT_LOAD_FAILED(4) //BLock saving if the main slot has not loaded
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(4) = TRUE carrying on still. ")
//					#ENDIF
//					iStages = RUN_SAVE_STAGES_SKIP_SAVING
//				ENDIF
//				IF STAT_CLOUD_SLOT_LOAD_FAILED(5) //BLock saving if the main slot has not loaded
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(5) = TRUE carrying on still. ")
//					#ENDIF
//					iStages = RUN_SAVE_STAGES_SKIP_SAVING
//				ENDIF
			ENDIF
			
			IF iStages != RUN_SAVE_STAGES_SKIP_SAVING
			AND iStages != RUN_SAVE_STAGES_NONE
				iStages = RUN_SAVE_STAGES_IS_LOADING_DONE
			ENDIF
		BREAK
		
		CASE RUN_SAVE_STAGES_IS_LOADING_DONE
			#IF IS_DEBUG_BUILD
				IF g_bPrivate_ProcessingWhichSave > -1
					NET_NL()NET_PRINT("[stat_savemgr_script] RUN_SYSTEM_SAVING: Waiting for existing loads/saves to finish: Trying to save enum ")NET_PRINT(GET_STAT_SAVETYPE_STRING(g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave]))
					NET_PRINT(" index = ")NET_PRINT_INT(g_bPrivate_ProcessingWhichSave)
				ELSE
					NET_NL()NET_PRINT("[stat_savemgr_script] RUN_SYSTEM_SAVING: Waiting for existing loads/saves to finish: Trying to save enum ")
					NET_PRINT(" index = ")NET_PRINT_INT(g_bPrivate_ProcessingWhichSave)
				ENDIF
			#ENDIF
			IF NOT STAT_SAVE_PENDING()
			AND NOT STAT_LOAD_PENDING() 
			AND NOT STAT_SAVE_PENDING_OR_REQUESTED()
				iStages = RUN_SAVE_STAGES_DO_SAVE
			ENDIF
		BREAK
		
		CASE RUN_SAVE_STAGES_DO_SAVE
			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(0) = ")NET_PRINT_BOOL(STAT_SLOT_IS_LOADED(0))	
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(1) = ")NET_PRINT_BOOL(STAT_SLOT_IS_LOADED(1))	
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(2) = ")NET_PRINT_BOOL(STAT_SLOT_IS_LOADED(2))	
//				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(3) = ")NET_PRINT_BOOL(STAT_SLOT_IS_LOADED(3))	
//				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(4) = ")NET_PRINT_BOOL(STAT_SLOT_IS_LOADED(4))	
//				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(5) = ")NET_PRINT_BOOL(STAT_SLOT_IS_LOADED(5))	
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: GET_ACTIVE_CHARACTER_SLOT()+1 = ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT()+1)	


//				NET_NL()NET_PRINT("[stat_savemgr_script] RUN_SYSTEM_SAVING: STAT_SAVE() is called on enum ")NET_PRINT(GET_STAT_SAVETYPE_STRING(g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave]))
//				NET_PRINT(" index = ")NET_PRINT_INT(g_bPrivate_ProcessingWhichSave)
				
			#ENDIF
			
			
			IF STAT_SLOT_IS_LOADED(0) = FALSE
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(0) = FALSE RETURN TO WAIT ")
				iStages = RUN_SAVE_STAGES_ERROR_CHECK
				EXIT
			ENDIF
			
			IF STAT_SLOT_IS_LOADED(GET_ACTIVE_CHARACTER_SLOT()+1) = FALSE
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(GET_ACTIVE_CHARACTER_SLOT()+1) = FALSE  RETURN TO WAIT ")
				iStages = RUN_SAVE_STAGES_ERROR_CHECK
				EXIT
			ENDIF
			
			IF DOES_PLAYER_HAVE_PRIVILEGES() = FALSE
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: DOES_PLAYER_HAVE_PRIVILEGES() = FALSE  RETURN TO NONE, and try again ")
				iStages = RUN_SAVE_STAGES_NONE
				EXIT
			ENDIF
			
			//url:bugstar:6080348 - Ignorable Assert - Error: Assertf(!GetBlockSaveRequests()) FAILED: FAIL SAVE REQUEST: Save requests are blocked, SAVE requested for file slot = < 0 > ...from Updating script ingamehud
			//Code now in one instance block saving so we need to check for this
			IF STAT_GET_BLOCK_SAVES()
				NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_GET_BLOCK_SAVES() = TRUE  RETURN TO QUIT CHECKS, and try again ")
				iStages = RUN_SAVE_STAGES_QUIT_CHECKS
				EXIT
			ENDIF
			
			IF g_bPrivate_ProcessingWhichSave > -1
				IF SHOULD_OPEN_SAVES_ON_ACTIVITY(g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave])
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SET_OPEN_SAVETYPE_IN_JOB() - called on enum ")NET_PRINT(GET_STAT_SAVETYPE_STRING(g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave]))
					STAT_SET_OPEN_SAVETYPE_IN_JOB(g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave])
				ENDIF
			ENDIF
			
			REMOVE_ALL_QUEUED_SAVES_FROM_END_MATCH_SAVE()
			IF g_bPrivate_ProcessingWhichSave > -1
				NET_NL()NET_PRINT("[stat_savemgr_script] RUN_SYSTEM_SAVING: STAT_SAVE() is called on enum ")NET_PRINT(GET_STAT_SAVETYPE_STRING(g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave]))
				NET_PRINT(" index = ")NET_PRINT_INT(g_bPrivate_ProcessingWhichSave)
				STAT_SAVE(0, FALSE, g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave], ENUM_TO_INT(g_ePrivate_SaveReason[g_bPrivate_ProcessingWhichSave]))
			ENDIF
			IF g_bPrivate_ProcessingWhichSave > -1
				IF g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_END_SHOPPING
				OR g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_CRITICAL
					
				ENDIF
			ENDIF
			iStages = RUN_SAVE_STAGES_IS_SAVE_DONE
		BREAK
		
		
		CASE RUN_SAVE_STAGES_IS_SAVE_DONE
			IF (NOT STAT_SAVE_PENDING()
			AND NOT STAT_LOAD_PENDING()
			AND NOT STAT_SAVE_PENDING_OR_REQUESTED())
			OR STAT_CLOUD_SLOT_SAVE_FAILED(-1) 
			
				IF STAT_CLOUD_SLOT_SAVE_FAILED(-1) 
					REMOVE_ALL_QUEUED_SAVES_DUE_TO_SAVES_FAIL()
				ENDIF
			
				IF g_bPrivate_ProcessingWhichSave > -1
					g_bPrivate_SaveRequested[g_bPrivate_ProcessingWhichSave] = STAT_SAVETYPE_DEFAULT
					g_ePrivate_SaveReason[g_bPrivate_ProcessingWhichSave] = SSR_REASON_DEFAULT
					g_bPrivate_ProcessingWhichSave = -1
				ENDIF
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[stat_savemgr_script] RUN_SYSTEM_SAVING: STATS FINISHED SAVING ")
				#ENDIF
				
				IF g_bIslandHeistSaveRequested
					PRINTLN("[stat_savemgr_script] RUN_SYSTEM_SAVING: Save successful after island heist completion. Clearing anti cheat chances. ")
					CLEAR_ISLAND_HEIST_FINALE_ANTI_CHEAT_CHANCES()
					g_bIslandHeistSaveRequested = FALSE
				ENDIF
				
				IF HAS_ANY_SAVE_BEEN_REQUESTED()
					iStages = RUN_SAVE_STAGES_QUIT_CHECKS
				ELSE
					iStages = RUN_SAVE_STAGES_NONE
				ENDIF
			ENDIF
		BREAK
		
		
		
		CASE RUN_SAVE_STAGES_SKIP_SAVING
			IF STAT_CLOUD_SLOT_LOAD_FAILED(0) = FALSE
			AND STAT_CLOUD_SLOT_LOAD_FAILED(1) = FALSE
			AND STAT_CLOUD_SLOT_LOAD_FAILED(2) = FALSE
			AND STAT_CLOUD_SLOT_LOAD_FAILED(3) = FALSE
			AND STAT_CLOUD_SLOT_LOAD_FAILED(4) = FALSE
//			AND STAT_CLOUD_SLOT_LOAD_FAILED(5) = FALSE
			AND  STAT_SLOT_IS_LOADED(0) = TRUE
			AND  HAS_ACTIVE_SLOT_STATS_LOADED() = TRUE
				IF HAS_ANY_SAVE_BEEN_REQUESTED()
					
					iStages = RUN_SAVE_STAGES_QUIT_CHECKS
				ELSE
					iStages = RUN_SAVE_STAGES_NONE
				ENDIF
			ELSE
				iStages = RUN_SAVE_STAGES_NONE
			ENDIF
			
			
			#IF IS_DEBUG_BUILD
			//Main slot load failed. 
			IF STAT_CLOUD_SLOT_LOAD_FAILED(0) = TRUE
				IF g_b_IsInTransition
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(0) = TRUE HOLDING ")
				ENDIF
				
			ENDIF
			//Main slot load failed. 
			IF STAT_CLOUD_SLOT_LOAD_FAILED(1) = TRUE
				IF g_b_IsInTransition
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(1) = TRUE HOLDING ")
				ENDIF
			
			ENDIF
			//Main slot load failed. 
			IF STAT_CLOUD_SLOT_LOAD_FAILED(2) = TRUE
				IF g_b_IsInTransition
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(2) = TRUE HOLDING ")
				ENDIF
			ENDIF
//			//Main slot load failed. 
//			IF STAT_CLOUD_SLOT_LOAD_FAILED(3) = TRUE
//				IF g_b_IsInTransition
//					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(3) = TRUE HOLDING ")
//				ENDIF
//				
//			ENDIF
//			//Main slot load failed. 
//			IF STAT_CLOUD_SLOT_LOAD_FAILED(4) = TRUE
//				IF g_b_IsInTransition
//					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(4) = TRUE HOLDING ")
//				ENDIF
//			ENDIF	
			//Main slot load failed. 
//			IF STAT_CLOUD_SLOT_LOAD_FAILED(5) = TRUE
//				IF g_b_IsInTransition
//					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_CLOUD_SLOT_LOAD_FAILED(5) = TRUE HOLDING ")
//				ENDIF
//				
//			ENDIF
			
			
			IF STAT_SLOT_IS_LOADED(0) = FALSE
			
				IF g_b_IsInTransition
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: STAT_SLOT_IS_LOADED(0) = FALSE HOLDING ")
				ENDIF
				

			ENDIF
			
			IF HAS_ACTIVE_SLOT_STATS_LOADED() = FALSE
			
				
				IF g_b_IsInTransition
					NET_NL()NET_PRINT("RUN_SYSTEM_SAVING: HAS_ACTIVE_SLOT_STATS_LOADED () = FALSE HOLDING ")
				ENDIF
				
				
			ENDIF
			
			#ENDIF
			
				
		BREAK
	
	ENDSWITCH
	
	
	RUN_SAVE_FAIL_MESSAGE_CHECK()

ENDPROC




#IF IS_DEBUG_BUILD


	BOOL inGameOnce
	
	BOOL RunPauseTimer
	BOOL StartPauseTimer
	SCRIPT_TIMER PauseTimer
	SCRIPT_TIMER PauseTimerTemp
	INT PauseTimerInt
	INT PauseTimerDiff
	BOOL PauseTimerReset



	
	BOOL bDisableIdleKick
	
	BOOL bTurnonRaceBox
	INT RaceBoxType 
	BEFORERACESTRUCT RaceDetails
	
	BOOL bPrintOutAllRankTitles
	
	BOOL bChangeRankTitlesSource
	BOOL bRankTitlesUsePersonal
	
	BOOL bPrintAllXpTypeHashes
	
	BOOL bTurnOnDamageBar
	BOOL bTurnOnCheckpointBar
	BOOL bTurnOnEliminationBar
	BOOL bTurnOnScore
	BOOL bTurnOnDoubleNumber
	BOOL bTurnOnSingleNumber
	BOOL bTurnOnSingleNumberInfinity
	BOOL bTurnOnDoubleNumberPlace
	BOOL bTurnOnTimer
	BOOL bTurnOnTimerTriple
	BOOL bTurnOnBigRacePosition
	BOOL bTurnOnSpriteDamageBar
	BOOL bTurnOnFourIconBar
	BOOL bTurnOnFiveIconScoreBar
	BOOL bTurnOnDoubleText
	
	BOOL bFourIconBarFlashingIconOne
	BOOL bFourIconBarFlashingIconTwo
	BOOL bFourIconBarFlashingIconThree
	BOOL bFourIconBarFlashingIconFour
	
	BOOL bTurnOnBikerBusiness
	
	BOOL bdisplayPackage
	BOOL bdisplayPackageStrings1
	BOOL bdisplayPackageStrings2
	
	BOOL bTurnOnFarLeftJustified
	BOOL bTurnOnMiddleJustified
	
//	BOOL bTurnOnBottomRightOverlay
//	BOOL bTurnOnBottomRightRaceDisplay
//	BOOL bTurnOnBottomRightDMDisplayALL
//	BOOL bTurnOnBottomRightDMDisplayFirst
//	BOOL bTurnOnBottomRightDMDisplaySecond
//	BOOL bTurnOnBottomRightDMDisplayThird
//	BOOL bTurnOnBottomRightDMDisplayFourth

	BOOL bTurnonDMLeaderTimer 
	BOOL bFlipOrderingDMTimer
	
	BOOL bTurnOnTeamNames
	
	BOOL TurnonJoinFailedSCreen
	
	INT PercentageLine
	INT iDamageMeterValue
	INT iDamageMeterDENOM
	INT iPlacementValue
	INT iPlacementDENOM
	INT iTimer1Value = 4568695
	INT iTimer2Value = 56856
	
	INT WeaponTypeInt
	WEAPON_TYPE CurrentWeapon = WEAPONTYPE_ADVANCEDRIFLE

	INT ScoreFlashingType
	INT ScoreFlashingTime

	BOOL GivePlayerWeapons
	
	INT iFlashTimer
	INT iExtraTime
	
	BOOL bUseNonPlayerFont = FALSE

	INT iWidgetInventory
	
//	BOOL TurnOnBirdsAndBees
	
	BOOL TurnOnOverheadString
	
	BOOL bHideMyOverheads
	BOOL bHideEveryoneElsesOverheads
	BOOL bHideAllOverheads
	
	BOOL bHideMyOverheads_ThisFrame
	BOOL bHideEveryoneElsesOverheads_ThisFrame
	BOOL bHideAllOverheads_ThisFrame
	
	BOOL TagAllRemotePlayers
	BOOL ClearAllRemotePlayersTags
	
	BOOL bTurnonVectorOverhead
	
	
	BOOL turnonSlotChecker
	BOOL Draw_crewTag
	BOOL Draw_DebugRankbadge
	INT RankBadgeLevel = 1

	BOOL bDrawCrossesCheckpoints
	BOOL bDrawCrossesElimination
	
	BOOL Draw_crewTagPlayer
	FLOAT CrewTagScale
	
	
	INT iShirtColourPalette
	BOOL bRefreshShirtColour
	
	BOOL bClearFocus
	BOOL bSetVectorFocus
	BOOL bIsFocusOnPlayer
	BOOL bRestoreFocusOnPlayer
	BOOL bSetFocusOnPlayer
	VECTOR vVectorFocusPos
	BOOL bWarpPlayerUsingWrapper
	BOOL bCameraUsingWrapper
	
	VECTOR vVectorFocusPosConst1 = <<1306, 989, 106>>
	VECTOR vVectorFocusPosConst2 = <<128, -1307, 29>>
	VECTOR vVectorFocusPosConst3 = <<-841, 158, 67>>
	VECTOR vVectorFocusPosConst4 = <<-2219, 1602, 275>>
	BOOL bSwapFocusVectors
	INT bCurrentVectorLoaded = 1
	
	VECTOR vVectorFocusVel = <<5, 5, 5>>
	BOOL bMovePlayerToPos
	STREAMVOL_ID streamvolId
	BOOL bCreateStreamVolSphere
	BOOL bHasStreamVolLoaded
	BOOL bDeleteStreamVolSphere
	INT StreamVolAssetType = 1
	INT StreamVolAssetLOD = 33
	INT iCurrentNumberStreamingRequests
	INT iWarpAtRequest = -1
	BOOL bStartWarpAtRequestcheck
	CAMERA_INDEX FocusCamera
	INT FocusAnInt
	VECTOR vLoadingCameraRot = <<0, 0, -12>>
	BOOL bFocusDestroyCamera
	BOOL bCreateLoadScene
	FLOAT fLoadSceneFarclip
	INT iLoadSceneFlags
	BOOL bIsLoadSceneLoaded
	BOOL bIsLoadSceneActive
	BOOL bDestroyLoadScene
	BOOL bLoadSceneAndWarp
	BOOL bLoadSceneCreateCamAndWarp
	BOOL bLoadingABool
	
	INT iTurnOnCameraAtRequest = -1
	BOOL bTurnOnCameraAtRequestcheck
	
	BOOL b_BailWithBobbyStuff
	
	BOOL b_spitoutrewardstring
	
	INT iVoteBitset
	BOOL bDisplayVoteBar
	
	SCALEFORM_LOADING_ICON DeleteCharStruct

	BOOL bHasfreshedIcon
	BOOL bDisabledSelectorForDelete
	
	BOOL bUpdateSnackDropping
	
	BOOL JoinMyCrew
	
	BOOL b_Trigger_Script_Cloud_Down
	
	BOOL DoBlurIn, DoBlurOut

	BOOL TurnOnPauseMenuEMPTY
	BOOL TurnOnPauseMenSOMETHING
	BOOL TurnOffPauseMen
	BOOL IsPauseMenuActive
	INT SkyBlurStage
	
//	INT OverlayAlpha = 0
	SPRITE_PLACEMENT XPIconSprite_Widget
	SPRITE_PLACEMENT XPIconSprite99_Widget
	SPRITE_PLACEMENT XPIconSprite999_Widget
	
	FLOAT RankBadgeX_Widget = 0.5
	FLOAT RankBadgeY_Widget = 0.5
	
	BOOL ClearNoMoreTutorials
	
	BOOL bLaunchMPIntro = FALSE
	BOOL bHasLaunchedMpIntroCut = FALSE
	
	BOOL bRankXpLimitsPrint
	
	BOOL bKickSCTVForRockstar
	INT iPlayerKickSCTVForRockstar
	
	BOOL b_printwindmeter

	BOOL bPrintSomeObjectiveText	
	
	BOOL b_PrintAllXPAWARDSHashes
	
	BOOL bRunHeistPropertyCamera
	BOOL bRunHeistPropertyCameraMoveOn
	INT iHeistCameraStage
	BOOL bRunHeistCameraUp

	BOOL bActivatePS4ActivityFeedDebug
	INT iDisplayPS4ActivityFeed
	INT iDisplayPS4ActivityFeed_INT
	
	BOOL DisplaySessionFullScreen
	
	BOOL DisplayIfCharactersAreLastGen
	
	BOOL DisplaySaveSelectionScreen
	BOOL DisplaySaveSelectionScreenOneCall
	
	BOOL bGivePlayerLotsOfMPAwards
	
	SAVE_OVERWRITE_DATA OverwriteData
	
	BOOL bRunEvent_1STINTSC_2NDINTSC_3RDINTSC_CHECKPOINTS_LOCALINT_TIMER
	BOOL bRunEvent_TurnOnPlayer1 = TRUE
	BOOL bRunEvent_TurnOnPlayer2 = TRUE
	BOOL bRunEvent_TurnOnPlayer3 = TRUE
	BOOL bRunEvent_ForceRefresh = FALSE
	
	BOOL bRunEvent_1STINT_2NDINT_3RDINT_ATTEMPT_LOCALINT_TIMER
	BOOL bRunEvent_VEHTEAM_ENEMIES_TIMER
	
	BOOL bRunEvent_VEHTEAM_COMPETITIVE_ENEMIES_TIMER
	BOOL bRunEvent_GAMERTAG_COMPETITIVE_ENEMIES_TIMER
	
	

	
	BOOL bRunEvent_1STFLOAT_2NDFLOAT_3RDFLOAT_ATTEMPT_LOCALFLOAT_TIMER
	BOOL bRunEvent_1STTIME_2NDTIME_3RDTIME_ATTEMPT_LOCALTIME_TIMER
	BOOL bRunEvent_1STTIME_2NDTIME_3RDTIME_TIMEHELD_TIMER
	BOOL bRunEvent_CHECKPOINT_TIMER
	BOOL bRunEvent_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER
	BOOL bRunEvent_1STINT_2NDINT_3RDINT_LOCALINT_TIMER
	BOOL bRunEvent_1STINT_2NDINT_3RDINT_KILLED_TIMER
	BOOL bRunEvent_1TIME_2TIME_3TIME_4TIME_TIMER
	BOOL bRunEvent_PLAYER_STATE_TIMER
	
	BOOL bRunEvent_BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER
	
	BOOL bRunEvent_BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_CASH1_CASH2_TIMER
	
	BOOL bDrawHudOverPhone
	BOOL bSetHeistPrisonPlaneDownProfileSetting
	
	BOOL bTriggerBGKickNewGame

	BOOL bTurnOnChallengesHud
	BOOL bTurnOnCheckpointHud
	BOOL bTurnOnTimeTrialHud
	BOOL bTurnOnWarfareHud
	
	// Listens for widget usage
	PROC MAINTAIN_GTAO_DEBUG_WIDGETS()

		IF g_b_DebugRunCharacterCreator
			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("characterTest")) = 0
				IF NOT (NETWORK_IS_SCRIPT_ACTIVE("characterTest", -1, TRUE))
					REQUEST_SCRIPT("characterTest")
					IF HAS_SCRIPT_LOADED("characterTest")
						CPRINTLN(DEBUG_NET_CHARACTER, "Start the characterTest script")
						g_b_DebugRunCharacterCreator = FALSE
						
						START_NEW_SCRIPT("characterTest", MULTIPLAYER_MISSION_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("characterTest")
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDPROC
	
#ENDIF

PROC REMOVE_SPLASH()
	
	PRINTLN("[BIGFEED] - REMOVE_SPLASH has been called:")
	DEBUG_PRINTCALLSTACK()
	
	g_bSplashScreenBigMethodCalledJoining = FALSE
	
	g_IsNewsFeedDisplaying = FALSE
	
	IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash) 
		SC_TRANSITION_NEWS_END()	
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
	ENDIF
	
	g_bSplashScreenBigMethodCalledJoining = FALSE
	SET_JOINING_GAMETIPS_FEED_DISPLAYING(FALSE)
	
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.g_aBigSplashScreenTimer)
	
	THEFEED_FLUSH_QUEUE()
	 g_b_ReapplyStickySaveFailedFeed = FALSE
	THEFEED_REPORT_LOGO_OFF()
	
	RESET_STRUCT_DL_PHOTO_VARS_LITE(g_TransitionSessionNonResetVars.sDynamicBigFeedData.sBigFeedUnplayedJobPhotoData)
	IF g_TransitionSessionNonResetVars.sDynamicBigFeedData.iUnplayedJobArrayIndex != -1
		UGC_RELEASE_CACHED_DESCRIPTION(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[g_TransitionSessionNonResetVars.sDynamicBigFeedData.iUnplayedJobArrayIndex].iMissionDecHash)
	ENDIF
	g_TransitionSessionNonResetVars.sDynamicBigFeedData.iUnplayedJobArrayIndex = -1
	g_TransitionSessionNonResetVars.sDynamicBigFeedData.iRandomJobType = -1
	g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset = 0
	STRUCT_DL_PHOTO_VARS_LITE sBigFeedUnplayedJobPhotoDataTemp
	CACHED_MISSION_DESCRIPTION_LOAD_VARS sBigFeedMissionDescVarsTemp
	g_TransitionSessionNonResetVars.sDynamicBigFeedData.sBigFeedUnplayedJobPhotoData = sBigFeedUnplayedJobPhotoDataTemp
	g_TransitionSessionNonResetVars.sDynamicBigFeedData.sBigFeedMissionDescVars = sBigFeedMissionDescVarsTemp
	PRINTLN("[BIGFEED] - REMOVE_SPLASH - called RESET_STRUCT_DL_PHOTO_VARS_LITE")
	PRINTLN("[BIGFEED] - REMOVE_SPLASH - called UGC_RELEASE_CACHED_DESCRIPTION")
	PRINTLN("[BIGFEED] - REMOVE_SPLASH - set iUnplayedJobArrayIndex = -1")
	PRINTLN("[BIGFEED] - REMOVE_SPLASH - set g_iBigFeedGrabUnplayedJobBitset = 0")
	PRINTLN("[BIGFEED] - REMOVE_SPLASH - emptied sBigFeedUnplayedJobPhotoData")
	PRINTLN("[BIGFEED] - REMOVE_SPLASH - emptied sBigFeedMissionDescVars")
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iSplashProg = 0
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShowOneMessageInBigNews = FALSE
	
ENDPROC

FUNC BOOL CAN_DISPLAY_NEWSWIRE()
	
	INT iBigFeedScreenToShow
	
	IF IS_ACCOUNT_OVER_17() = FALSE
		RETURN FALSE
	ENDIF
	
//	IF NETWORK_HAS_PRIVILEDGE_FOR_SCS() = FALSE
//		RETURN FALSE
//	ENDIF
	
	IF HAS_IMPORTANT_STATS_LOADED() AND ALREADY_CREATED_GANG_MEMBER_IN_SLOT_TRANSITION() = FALSE
		RETURN FALSE
	ENDIF
	
	IF GET_IS_LOADING_SCREEN_ACTIVE()
		RETURN FALSE
	ENDIF
	
	// We don't want the unplayed jobs message to display if we cannot load data from the UGC.
	IF SC_TRANSITION_NEWS_HAS_EXTRA_DATA_TU()
		IF SC_TRANSITION_NEWS_GET_EXTRA_DATA_INT_TU("scriptDisplay", iBigFeedScreenToShow)
			IF (iBigFeedScreenToShow = ciBIG_FEED_UNPLAYED_JOB)
				IF NOT GET_FM_UGC_INITIAL_HAS_FINISHED()
					IF SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
						PRINTLN("[BIGFEED] - CAN_DISPLAY_NEWSWIRE - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was successful.")
					ELSE
						PRINTLN("[BIGFEED] - CAN_DISPLAY_NEWSWIRE - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was not successful.")
					ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC 

PROC HIDE_ONLINE_LOGO(SCALEFORM_INDEX& aMovie)
	IF HAS_SCALEFORM_MOVIE_LOADED(aMovie)
		BEGIN_SCALEFORM_MOVIE_METHOD(aMovie, "HIDE_ONLINE_LOGO")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

FUNC BOOL SHOW_NEWS()
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShowOneMessageInBigNews
		IF SC_TRANSITION_NEWS_SHOW_TIMED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, 0)
			RETURN TRUE
		ENDIF
	ELSE	
		IF SC_TRANSITION_NEWS_SHOW(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_POST_MISSION_CLEANUP_SPLASH_SCREEN()

	SWITCH g_TransitionSessionNonResetVars.sPostMissionCleanupData.iSplashProg
		
		CASE 0
			// Wait for global to be set to 1.
		BREAK
		
		CASE 1
			IF NOT HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
			
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash = REQUEST_SCALEFORM_MOVIE("GTAV_ONLINE")
				RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.g_aBigSplashScreenTimer)
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iSplashProg++
				
				PRINTLN("[Big Feed] - MAINTAIN_POST_MISSION_CLEANUP_SPLASH_SCREEN - GTAV_ONLINE movie loaded.")
				
			ENDIF
		BREAK
		
		CASE 2
			
			IF NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
				IF NOT g_IsNewsFeedDisplaying
					SET_JOINING_GAMETIPS_FEED_DISPLAYING(TRUE)
					PRINTLN("[Big Feed] - MAINTAIN_POST_MISSION_CLEANUP_SPLASH_SCREEN - set SET_JOINING_GAMETIPS_FEED_DISPLAYING TRUE.")
				ENDIF
			ENDIF
			
			IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
			AND IS_SCREEN_FADED_IN() //Added for Class C 1731429
				IF IS_TRANSITION_ACTIVE()
					IF GET_CURRENT_GAMEMODE() != GAMEMODE_EMPTY
					AND GET_CURRENT_GAMEMODE() != GAMEMODE_SP
						THEFEED_REPORT_LOGO_ON()
					ENDIF
					FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE)
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				ENDIF
	
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
				OR GET_CURRENT_GAMEMODE() = GAMEMODE_SP
					HIDE_ONLINE_LOGO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
				ENDIF
				
				IF g_IsNewsFeedDisplaying = TRUE
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, 255, 255, 255, 255)
				ENDIF
				
				IF CAN_DISPLAY_NEWSWIRE() 
					IF g_bSplashScreenBigMethodCalledJoining = FALSE
						IF SHOW_NEWS()
							SET_JOINING_GAMETIPS_FEED_DISPLAYING(FALSE)
							THEFEED_FLUSH_QUEUE()
							 g_b_ReapplyStickySaveFailedFeed = FALSE
//							SC_TRANSITION_NEWS_SHOW_NEXT_ITEM() //Was causing the first screen to flash past. 
							g_IsNewsFeedDisplaying = TRUE
							g_bSplashScreenBigMethodCalledJoining = TRUE
						ELSE
							IF NOT IS_TRANSITION_ACTIVE()
								HIDE_ONLINE_LOGO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_TRANSITION_ACTIVE()
							HIDE_ONLINE_LOGO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
						ENDIF
					ENDIF
					
					IF g_bSplashScreenBigMethodCalledJoining
						IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.g_aBigSplashScreenTimer)
							START_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.g_aBigSplashScreenTimer)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.g_aBigSplashScreenTimer, 20000)
								RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.g_aBigSplashScreenTimer)
								//SC_TRANSITION_NEWS_SHOW_NEXT_ITEM() Removed for 1779794
//								PRINTLN("[WJK] - MAINTAIN_POST_MISSION_CLEANUP_SPLASH_SCREEN - g_aBigSplashScreenTimer >= 20000, calling SC_TRANSITION_NEWS_SHOW_NEXT_ITEM for next big splash message.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			// Wait for glboal to be set to 2.
			
		BREAK
		
		CASE 3
			
			REMOVE_SPLASH()
			
		BREAK
		
	ENDSWITCH
	
ENDPROC



PROC PRINT_ALL_XP_LIMITS()
	
	INT I
	FOR I = 0 TO MAX_FM_RANK
		NET_NL()NET_PRINT("PRINT_ALL_XP_LIMITS: g_sMPTunableArrays.iTopRankValues[")NET_PRINT_INT(I)NET_PRINT("] = ")
		NET_PRINT_INT(g_sMPTunableArrays.iTopRankValues[I])
	ENDFOR

ENDPROC



PROC PROCESS_HUD_DISPLAYS_DEBUG()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_HUD_DISPLAYS_DEBUG")
	#ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD

		#IF IS_DEBUG_BUILD
			IF inGameOnce = FALSE
			
				START_WIDGET_GROUP("MP In SP Widgets")
												
					START_WIDGET_GROUP("SAVING")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)								
						ADD_WIDGET_BOOL("g_ShowSavingPrints", g_ShowSavingPrints)
						ADD_WIDGET_BOOL("g_ShowSavingMigrationPrints", g_ShowSavingMigrationPrints)
						ADD_WIDGET_BOOL("bTurnOnBusySpinner", bTurnOnBusySpinner)
						ADD_WIDGET_BOOL("Has Entered GTA Online this boot", g_b_Has_Entered_GTAO_This_Boot)
						ADD_WIDGET_BOOL("g_b_SaveHeistDebug", g_b_SaveHeistDebug)
						ADD_WIDGET_BOOL("Hold up third delete stage", g_b_HoldUpDeleteThirdStage)
					STOP_WIDGET_GROUP()
					
					
					START_WIDGET_GROUP("STREAMING")
						ADD_WIDGET_BOOL("TURN ON WIDGETS", G_bTurnOnInGameWidgets)	
						
						
						ADD_WIDGET_VECTOR_SLIDER("Vec Pos", vVectorFocusPos, -2000, 2000, 1)
						ADD_WIDGET_VECTOR_SLIDER("Vec Vel", vVectorFocusVel, -2000, 2000, 1)
						ADD_WIDGET_VECTOR_SLIDER("Cam Rot", vLoadingCameraRot, -100,100,1)
				
						ADD_WIDGET_BOOL("Swap Vectors", bSwapFocusVectors)
						
						ADD_WIDGET_BOOL("Move player there", bMovePlayerToPos)
						
						START_WIDGET_GROUP("FOCUS")
							ADD_WIDGET_BOOL("bClearFocus", bClearFocus)
							ADD_WIDGET_BOOL("Set Focus", bSetVectorFocus)
							ADD_WIDGET_BOOL("Focus and Warp", bWarpPlayerUsingWrapper)
							ADD_WIDGET_BOOL("Camera and Warp", bCameraUsingWrapper)

							ADD_WIDGET_BOOL("Is Focus OnPlayer", bIsFocusOnPlayer)
							ADD_WIDGET_BOOL("Restore Focus", bRestoreFocusOnPlayer)
							ADD_WIDGET_BOOL("Set Focus on Player", bSetFocusOnPlayer)
							ADD_WIDGET_INT_SLIDER("FocusAnInt ", FocusAnInt, -1, 500, 1)
							ADD_WIDGET_BOOL("Destroy camera", bFocusDestroyCamera)
							ADD_WIDGET_INT_SLIDER("Camera At request", iTurnOnCameraAtRequest, -1, 100, 1)
							ADD_WIDGET_INT_SLIDER("Warp At request", iWarpAtRequest, -1, 100, 1)
						
						STOP_WIDGET_GROUP()
						
						
						START_WIDGET_GROUP("LOADSCENE")
							ADD_WIDGET_BOOL("Load scene", bCreateLoadScene)
							ADD_WIDGET_FLOAT_SLIDER("Far clip", fLoadSceneFarclip, 0, 1000, 1)
							ADD_WIDGET_INT_SLIDER("flag:1,2,4", iLoadSceneFlags, 0, 5, 1)
							
							ADD_WIDGET_BOOL("Is Active", bIsLoadSceneActive)
							ADD_WIDGET_BOOL("Is Loaded", bIsLoadSceneLoaded)
							
							ADD_WIDGET_BOOL("Stop Load scene", bDestroyLoadScene)
							
							ADD_WIDGET_BOOL("Load Scene and warp", bLoadSceneAndWarp)
							ADD_WIDGET_BOOL("Load Scene and Cam", bLoadSceneCreateCamAndWarp)
							ADD_WIDGET_BOOL("Destroy camera", bFocusDestroyCamera)
							

						
						STOP_WIDGET_GROUP()
						
						START_WIDGET_GROUP("STREAMVOL")
							ADD_WIDGET_BOOL("Create StreamVol", bCreateStreamVolSphere)
							ADD_WIDGET_BOOL("Has StreamVol Loaded", bHasStreamVolLoaded)
							ADD_WIDGET_BOOL("Delete StreamVol", bDeleteStreamVolSphere)
							
							ADD_WIDGET_INT_SLIDER("Type: 1,2,12", StreamVolAssetType, 1,13,1)
							ADD_WIDGET_INT_SLIDER("LOD: 33,94,127", StreamVolAssetLOD, 1,128,1)
						STOP_WIDGET_GROUP()
						
						
						ADD_WIDGET_INT_READ_ONLY("Streaming Requests", iCurrentNumberStreamingRequests)
	
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Pause Timer")
						ADD_WIDGET_BOOL("TURN ON WIDGETS", G_bTurnOnInGameWidgets)	
						ADD_WIDGET_BOOL("Start Timer", StartPauseTimer)
						ADD_WIDGET_INT_READ_ONLY("int", PauseTimerInt)
						ADD_WIDGET_INT_READ_ONLY("Diff", PauseTimerDiff)
						ADD_WIDGET_BOOL("Pause", RunPauseTimer)
						ADD_WIDGET_BOOL("Reset", PauseTimerReset)
					
					STOP_WIDGET_GROUP()
					
				
					ADD_WIDGET_BOOL("Trigger Script Cloud Down", b_Trigger_Script_Cloud_Down)
					ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)				
					START_WIDGET_GROUP( "Freemode Helper") 
						ADD_WIDGET_BOOL(" ON", g_bUSE_NEW_UGC_SYSTEM )
						ADD_WIDGET_BOOL("Other", g_bSAVE_ONLY_TO_NEW_UGC)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("SHIFT CREATION")
						ADD_WIDGET_BOOL("TURN ON WIDGETS", G_bTurnOnInGameWidgets)	
						ADD_WIDGET_BOOL("Shift guy with lower rank", G_bShiftWithLowRank)
						ADD_WIDGET_INT_SLIDER("Shift guy with this rank", g_iCreateGuyAtThisRank, -1, 8000, 1)
						ADD_WIDGET_BOOL("g_ShouldShiftingTutorialsBeSkipped", g_ShouldShiftingTutorialsBeSkipped)
						ADD_WIDGET_BOOL("Shift F a Female Character", g_b_ShiftF_FemaleCharacter)
						ADD_WIDGET_BOOL("CTRL H Also deletes Bank cash", bShouldCTRLHDeleteBankToo)
						ADD_WIDGET_BOOL("Set Prison Plane Down Profile Setting", bSetHeistPrisonPlaneDownProfileSetting)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("TRIPLEHEAD")
						ADD_WIDGET_BOOL("TURN ON WIDGETS", G_bTurnOnInGameWidgets)	
						ADD_WIDGET_BOOL("Turn On Triplehead", b_TurnOnTripleHead)
						ADD_WIDGET_BOOL("Turn Off Triplehead", b_TurnOffTripleHead)
					STOP_WIDGET_GROUP()
					
					
					START_WIDGET_GROUP("FREEZE")
						ADD_WIDGET_BOOL("TURN ON WIDGETS", G_bTurnOnInGameWidgets)	
						ADD_WIDGET_BOOL("Turn On Freeze", bTurnOnRenderFreeze)
						ADD_WIDGET_BOOL("Turn Off Freeze", bTurnOffRenderFreeze)
						ADD_WIDGET_BOOL("Turn Off Freeze and POSTFX", bTurnOffRenderFreezeAndEffect)
					STOP_WIDGET_GROUP()
					

					START_WIDGET_GROUP("SC Admin Gift")
						ADD_WIDGET_BOOL("TURN ON the Gift set", g_RunDebugScAdminCorrectionWidget)	
						ADD_WIDGET_INT_SLIDER("RP Gift Set", g_iDebugRPGiftSet, -1000, 1000, 1)
						ADD_WIDGET_INT_SLIDER("RP Gift Delat", g_iDebugRPGiftDelta, -1000, 1000, 1)
						ADD_WIDGET_INT_SLIDER("Cash Gift Set", g_iDebugCashGiftSet, -1000, 1000, 1)
					STOP_WIDGET_GROUP()	
	
					START_WIDGET_GROUP("BIG FEED")
						ADD_WIDGET_BOOL("g_bDisplayBigFeedJobCounts", g_bDisplayBigFeedJobCounts)
						ADD_WIDGET_BOOL("g_bDebugSkipBigFeedScreen", g_bDebugSkipBigFeedScreen)
						ADD_WIDGET_INT_SLIDER("g_iDisplayBigFeedScriptScreens", g_iDisplayBigFeedScriptScreens, 0, 10, 1)
						ADD_WIDGET_INT_READ_ONLY("g_iLastBigFeedScreenShown", g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("SYSTEM ACTIVITY FEED")
						ADD_WIDGET_INT_SLIDER("Which Feed", iDisplayPS4ActivityFeed, 0, 99, 1)
						ADD_WIDGET_INT_SLIDER("Int Param", iDisplayPS4ActivityFeed_INT, 0, 999999, 1)
						ADD_WIDGET_BOOL("Turn On Debug", bActivatePS4ActivityFeedDebug)
						START_WIDGET_GROUP("MP FEED - GUIDE")
							ADD_WIDGET_STRING("1.  Published Race")
							ADD_WIDGET_STRING("2.  Published Deathmatch")
							ADD_WIDGET_STRING("3.  Published Capture")
							ADD_WIDGET_STRING("4.  Verified Race")
							ADD_WIDGET_STRING("5.  Verified Deathmatch")
							ADD_WIDGET_STRING("6.  Verified Capture")
							ADD_WIDGET_STRING("7.  Played with Friends Verified Race")
							ADD_WIDGET_STRING("8.  Played with Friends Verified DM")
							ADD_WIDGET_STRING("9.  Played with Friends Verified Capture")
							ADD_WIDGET_STRING("10.  Playing with Friends")
							ADD_WIDGET_STRING("11.  Got a Tattoo")
							ADD_WIDGET_STRING("12.  Purchased Second property")
							ADD_WIDGET_STRING("13.  Purchased First property")
							ADD_WIDGET_STRING("14.  Moved House")
							ADD_WIDGET_STRING("15.  Purchased Weapon")
							ADD_WIDGET_STRING("16.  Purchased Vehicle Mod")
							ADD_WIDGET_STRING("17.  Purchased Vehicle")
							ADD_WIDGET_STRING("18.  Bought Clothes")
							ADD_WIDGET_STRING("19.  Bought Haircut or Makeup")
							ADD_WIDGET_STRING("20.  Completed Job")
							ADD_WIDGET_STRING("21.  Playing Heist")
							ADD_WIDGET_STRING("22.  Play Golf")
							ADD_WIDGET_STRING("23.  Play Arm Wrestling")
							ADD_WIDGET_STRING("24.  Play Shooting Range")
							ADD_WIDGET_STRING("25.  Play Darts")
							ADD_WIDGET_STRING("26.  Play Tennis")
							ADD_WIDGET_STRING("27.  Play Pilot School")
							ADD_WIDGET_STRING("28.  Play Playlist")
							ADD_WIDGET_STRING("29.  Played Tournament")
							ADD_WIDGET_STRING("30.  Played Tournament Qualifier")
							ADD_WIDGET_STRING("31.  Won Tournament")
							ADD_WIDGET_STRING("32.  Play Event Playlist")
							ADD_WIDGET_STRING("33.  Played Challenge Playlist")
							ADD_WIDGET_STRING("34.  Setup Challenge Playlist")
							ADD_WIDGET_STRING("35.  Won Challenge Playlist $X")
							ADD_WIDGET_STRING("36.  Done Gang Attack")
							ADD_WIDGET_STRING("37.  Done Bounty $X")
							ADD_WIDGET_STRING("38.  Done Import Export")
							ADD_WIDGET_STRING("39.  Done Deliver")
							ADD_WIDGET_STRING("40.  Done Security Van")
							ADD_WIDGET_STRING("41.  Collected Crate Drop")
							ADD_WIDGET_STRING("42.  Collected Special Crate Drop")
							ADD_WIDGET_STRING("43.  Robbed Hold up Store")
							ADD_WIDGET_STRING("44.  Done Stunt Jump")
							ADD_WIDGET_STRING("45.  Done 1-On-1 DM")
							ADD_WIDGET_STRING("46.  Done Impromptu Race")
							ADD_WIDGET_STRING("47.  Uploaded Photo")
							ADD_WIDGET_STRING("48.  Made Money Betting")
							ADD_WIDGET_STRING("49.  Met Lester")
							ADD_WIDGET_STRING("50.  Met Trevor")
							ADD_WIDGET_STRING("51.  Met Martin")
						STOP_WIDGET_GROUP()	
						START_WIDGET_GROUP("SP FEED - GUIDE")
							ADD_WIDGET_STRING("52.  Completed Prologue")
							ADD_WIDGET_STRING("53.  Completed Lester1")
							ADD_WIDGET_STRING("54.  Completed Franklin2")
							ADD_WIDGET_STRING("55.  Completed All Random Chars")
							ADD_WIDGET_STRING("56.  Completed SP Mission as Michael")
							ADD_WIDGET_STRING("57.  Completed SP Mission as Franklin")
							ADD_WIDGET_STRING("58.  Completed SP Mission as Trevor")
							ADD_WIDGET_STRING("59.  Driven All Vehicles")
							ADD_WIDGET_STRING("60.  Collected Spaceship")
							ADD_WIDGET_STRING("61.  All Letter Scraps")
							ADD_WIDGET_STRING("62.  Collected Sonar")
							ADD_WIDGET_STRING("63.  Driven X Miles")
							ADD_WIDGET_STRING("64.  Flown X Miles")
							ADD_WIDGET_STRING("65.  Ran X Miles")
							ADD_WIDGET_STRING("66.  Busted 10 times")
							ADD_WIDGET_STRING("67.  Wasted 10 times")
							ADD_WIDGET_STRING("68.  Fired Half mil Bullets")
							ADD_WIDGET_STRING("69.  Evaded 5 star wanted level")
							ADD_WIDGET_STRING("70.  Purchased Car Online")
							ADD_WIDGET_STRING("71.  Purchased Rhino Online")
							ADD_WIDGET_STRING("72.  Purchased Buzzard Online")
							ADD_WIDGET_STRING("73.  Stockmarket Loss")
							ADD_WIDGET_STRING("74.  Stockmarket Invested")
							ADD_WIDGET_STRING("75.  Stunt jumps X%")
							ADD_WIDGET_STRING("76.  Under Bridge X%")
							ADD_WIDGET_STRING("77.  Found Highest Point")
							ADD_WIDGET_STRING("78.  Drunk X Times")
							ADD_WIDGET_STRING("79.  Purchased all Properties")
							ADD_WIDGET_STRING("80.  Exploration X%")
							ADD_WIDGET_STRING("81.  Found Lowest Point")
							ADD_WIDGET_STRING("82.  High Vehicle Spend $Xmill")
							ADD_WIDGET_STRING("83.  High Weapon Spend $X")
							ADD_WIDGET_STRING("84.  High Clothes Spend $X")
							ADD_WIDGET_STRING("85.  Hole in One")
							ADD_WIDGET_STRING("86.  Driven Xkm")
							ADD_WIDGET_STRING("87.  Flown Xkm")
							ADD_WIDGET_STRING("88.  Ran Xkm")
							ADD_WIDGET_STRING("89.  Taken Cat Photo")
							ADD_WIDGET_STRING("90.  Taken Border Collie Photo")
							ADD_WIDGET_STRING("91.  Taken Pig Photo")
							ADD_WIDGET_STRING("92.  Taken Pug Photo")
							ADD_WIDGET_STRING("93.  Taken Mountain Lion Photo")
							ADD_WIDGET_STRING("94.  ")
							ADD_WIDGET_STRING("95.  ")
							ADD_WIDGET_STRING("96.  ")
							ADD_WIDGET_STRING("97.  ")
							ADD_WIDGET_STRING("98.  ")
							ADD_WIDGET_STRING("99.  ")
						STOP_WIDGET_GROUP()	
						
						
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("NEW GAME")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)	
						ADD_WIDGET_BOOL("NEW GAME TO MP", bTriggerBGKickNewGame)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("KICKING")
						ADD_WIDGET_BOOL("TURN ON FORCE KICK PRINTS", g_BKickingRunWIDGETPRINTS)
						ADD_WIDGET_BOOL("GENERAL ERROR", g_Private_EndSessionHappenedSomehow)
						ADD_WIDGET_BOOL("KICK TO REBOOT", g_b_Private_Kick_player_to_switch_to_offline_Until_reboot)
						ADD_WIDGET_BOOL("FM DEAD KICK", g_b_Debug_MakeFMArrayOverrun)
						ADD_WIDGET_BOOL("ENABLE FM DEAD KICK", g_B_TurnOnIsFreemodeRunningChecks)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("MISC PRINTS")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)													
						ADD_WIDGET_BOOL("Turn on radar map prints", g_b_DisplayPrintsForRadarMap)
						ADD_WIDGET_BOOL("Print all XP hashes", bPrintAllXpTypeHashes)
						ADD_WIDGET_BOOL("Spit out Reward String", b_spitoutrewardstring)
					STOP_WIDGET_GROUP()
					
					
					
					START_WIDGET_GROUP("HEIST PROP CAM")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)			
						ADD_WIDGET_BOOL("Run Heist Camera", bRunHeistPropertyCamera)
						ADD_WIDGET_BOOL("Heist Camera Move On", bRunHeistPropertyCameraMoveOn)
						ADD_WIDGET_INT_READ_ONLY("Heist Camera Stage", iHeistCameraStage)
						ADD_WIDGET_BOOL("Heist Camera Up", bRunHeistCameraUp)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("AWARDS & XP")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
						ADD_WIDGET_BOOL("Give lots of MP Awards", bGivePlayerLotsOfMPAwards)
						ADD_WIDGET_BOOL("Print all XP Award Hashes", b_PrintAllXPAWARDSHashes)
						
						ADD_WIDGET_BOOL("Give player a beer hat", bgivePlayerABeerHat)
					STOP_WIDGET_GROUP()
					
					
					START_WIDGET_GROUP("SKYCAM")
						ADD_WIDGET_BOOL("Is Everyone Ready for Normal Skycam", g_b_Debug_HoldSkyCamBeforeDecent)
					STOP_WIDGET_GROUP()
					
					#IF FEATURE_SHORTEN_SKY_HANG
					START_WIDGET_GROUP("SHORTEN HANGING IN SKY")
						ADD_WIDGET_INT_SLIDER("g_iHangingInSkyState", g_iHangingInSkyState, -1, 99, 1)
						ADD_WIDGET_INT_SLIDER("g_sMPTunables.iHangingInSkyTimeout", g_sMPTunables.iHangingInSkyTimeout, -1, HIGHEST_INT, 1)
						ADD_WIDGET_BOOL("g_bLastSkySwoopDownWasTruncated", g_bLastSkySwoopDownWasTruncated)
						ADD_WIDGET_BOOL("g_bSkySwoopDownDidFadeOut", g_bSkySwoopDownDidFadeOut)
						ADD_WIDGET_BOOL("g_bPMCIsSwitchingToPlayer", g_bPMCIsSwitchingToPlayer)
					STOP_WIDGET_GROUP()
					#ENDIF
					
					START_WIDGET_GROUP("TRANSITION WARN")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)			
						
						ADD_WIDGET_BOOL("Display Session Full Screen", DisplaySessionFullScreen)
						ADD_WIDGET_BOOL("Display Save Transfers Possible", g_b_RunSaveTransferScreen)
						ADD_WIDGET_BOOL("Display Save Selection", DisplaySaveSelectionScreen)
						ADD_WIDGET_BOOL("Invite Replay Editor Flag", g_bRockstarEditorActive)
						
					STOP_WIDGET_GROUP()
					
					
					START_WIDGET_GROUP("HUD PROTO")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)		
						
						ADD_WIDGET_BOOL("SET MULTIPLAYER_CASH_TEXT", SetMultiplayerCashText)
						ADD_WIDGET_BOOL("MULTIPLAYER_CASH_TEXT", DisplayMultiplayerCashText)
						
						ADD_WIDGET_BOOL("SET USEFakeMpCash", Set_UseFakeMPCash)
						ADD_WIDGET_BOOL("USEFakeMpCash", UseFakeMpCash)
												
						ADD_WIDGET_BOOL("SET CHANGEFakeMpCash", set_changeFakeMpCash)
						ADD_WIDGET_INT_SLIDER("iCHANGEFakeMpCash", iChangeFakeMpCash, 0, 100, 1)

					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("SCTV")
						ADD_WIDGET_BOOL("Kick player for Rockstar", bKickSCTVForRockstar)
						ADD_WIDGET_INT_SLIDER("Which Player Index", iPlayerKickSCTVForRockstar, 0, NUM_NETWORK_PLAYERS, 1)
						ADD_WIDGET_BOOL("Make Invites SCTV invites", g_b_WidgetMakeAllInviteSCTVInvites)
						ADD_WIDGET_BOOL("g_DisableSCTVPlayerSlots",g_DisableSCTVPlayerSlots)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("CHARACTER CREATOR")
						ADD_WIDGET_BOOL("Debug new character creator", g_b_DebugRunCharacterCreator)
						ADD_WIDGET_BOOL("Reset new character creator screen", g_b_DebugResetCharacterCreator)
						
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("PC GIFT")
						START_WIDGET_GROUP("PC GIFT")
							ADD_WIDGET_INT_SLIDER("Scadmin congrat screen ", g_i_SCAdminCashGiftScreenTypeWidget, 0, 30, 1)
						STOP_WIDGET_GROUP()
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)		
						ADD_WIDGET_BOOL("Setup", g_b_SetupFakePCGiftsWidget)
						ADD_WIDGET_INT_SLIDER("Cash type", g_i_CashGiftWidgetAmount, -1, 1, 1)
						ADD_WIDGET_INT_SLIDER("Cash amount", g_i_DoCashGiftACTUALAmountWidget, 0, 50000, 100)
						ADD_WIDGET_BOOL("Has Item been given", g_b_HasItemBeenGivenOnPcWidget)
						ADD_WIDGET_INT_READ_ONLY("Is this congratulations", (g_i_SCAdminCashGiftScreenType))				
						ADD_WIDGET_BOOL("READ ONLY Block main gift", g_b_IsFakePCGiftsWidgetActive)
						
					STOP_WIDGET_GROUP()
					
					
					START_WIDGET_GROUP("BAD SPORT")
						ADD_WIDGET_INT_READ_ONLY("BadSport Number Of Days Before Forgiven", BadSportTrackingNumDays)
					
						ADD_WIDGET_DATE_READ_ONLY("Bad Sport Time Left", BadSportTimeLeft)

						START_WIDGET_GROUP("MPPLY_BECAME_BADSPORT_DT Change")
							ADD_WIDGET_DATE_SLIDER("Shave off ", g_Debug_DateToShaveOffBecomingBadSport, 0, 100000, 1)
						
							ADD_WIDGET_DATE_READ_ONLY("MPPLY_BECAME_BADSPORT_DT", BadSport_MPPLY_BECAME_BADSPORT_DT_Value)
						

						
							ADD_WIDGET_BOOL("Apply Time to Shave off MPPLY_BECAME_BADSPORT_DT ", g_Debug_ApplyDateToShaveOffBecomingBadSport)		
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					


					START_WIDGET_GROUP("FAKE TRANSITION TIMEOUTS")
						ADD_WIDGET_BOOL("Timed out loading player data", g_Debug_FakeTimeout_PrehudStats)
						
						ADD_WIDGET_BOOL("Timed out joining session", g_Debug_FakeTimeout_JoiningSession)
					
						ADD_WIDGET_BOOL("Timed out loading session", g_Debug_FakeTimeout_SettingupPlayer)
						
						ADD_WIDGET_BOOL("Timed out locating session", g_Debug_FakeTimeout_WaitingToJoin)
					
						ADD_WIDGET_BOOL("Timed out pre joining", g_Debug_FakeTimeout_PreJoiningSession)
					
					STOP_WIDGET_GROUP()
	
					START_WIDGET_GROUP("PRINT TOGGLES")
						ADD_WIDGET_BOOL("Turn on Idle Kick prints", g_B_IdleKickPrints)
						ADD_WIDGET_BOOL("SHOW ALL HUD BLOCKERS", G_bTurnOnAllHUDBlockers )
						ADD_WIDGET_BOOL("PRINT XP LIMITS", bRankXpLimitsPrint)
						ADD_WIDGET_BOOL("ROS CREDENTIAL PRINTS", g_b_displayROSCredentialsDroppedPrint)
						ADD_WIDGET_BOOL("Print some objective text", bPrintSomeObjectiveText)
						ADD_WIDGET_BOOL("Print result from IS_LAST_GEN_PLAYER", DisplayIfCharactersAreLastGen)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("MODEL SWAP")
						ADD_WIDGET_BOOL("Swap model to bigfoot", bSwapModeltoBigfoot)
						ADD_WIDGET_BOOL("Swap model to Player", bSwapModeltoPlayer)
						ADD_WIDGET_BOOL("Change Afterlife to Bigfoot", bchangeAfterlifetoBigfoot)
						ADD_WIDGET_BOOL("block player count check", g_bBlockFmEventPlayerCountCheck)
					STOP_WIDGET_GROUP()
					
					ADD_WIDGET_BOOL("b_BailWithBobbyStuff", b_BailWithBobbyStuff)
					
					ADD_WIDGET_BOOL("DELETE ALL CHARACTERS",g_bDeleteAllCharsQuick)
					
					ADD_WIDGET_BOOL("Use FORCE_ALL_HEADING_VALUES_TO_ALIGN", g_UseForceAllHeadingValuesToAlign)
					ADD_WIDGET_BOOL("Update Crew MetaData", updateCrewMetaData)
					
					ADD_WIDGET_BOOL("Run Change Character screen", g_bRunChangeCharacterScreen)
					
					ADD_WIDGET_BOOL("Ignore Combat Pack", g_bIgnoreCombatPackCheck)
					ADD_WIDGET_BOOL("4 Hour Wanted Check", g_Debug4HourWantedCheck)
					ADD_WIDGET_BOOL("Clear No More Tutorials", ClearNoMoreTutorials)
					ADD_WIDGET_BOOL("Do Idle Kick Now", g_Private_KickedForIdling)
					ADD_WIDGET_BOOL("Do Constrained Kick Now", g_Private_KickedForConstrained)
					
					
					ADD_WIDGET_FLOAT_SLIDER("Velocity Range ", g_fDebug_BadSportPersonalVehicle_VelocityRange, -5, 5, 0.001)
					
					ADD_WIDGET_BOOL("g_b_DoPropertySkyCamWidget", g_b_DoPropertySkyCamWidget)
					
					ADD_WIDGET_BOOL("AWARD WEEKLY INTEREST NOW",g_bDebugAwardWeeklyInterestNow)
					
					ADD_WIDGET_BOOL("BHangTheTransition", g_b_HangTheTransition)
					
					ADD_WIDGET_BOOL("Ignore solo checks for cheating", g_b_IgnoreSoloChecksForCheating)
					
					ADD_WIDGET_BOOL("Has ENtered OFFLINE SAVE MODE ", g_b_HasEntered_MPOfflineSAVEMode)
					
					START_WIDGET_GROUP("CASH GIFT")
						ADD_WIDGET_INT_SLIDER("CASH GIFT INT", g_i_DoCashGiftMessageAmount, -1, 1, 1)
						ADD_WIDGET_INT_SLIDER("CASH GIFT AMOUNT", g_i_DoCashGiftACTUALAmount, -100, 100, 1)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("RANK TITLES")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)			
						ADD_WIDGET_BOOL("bPrintOutAllRankTitles", bPrintOutAllRankTitles)
						ADD_WIDGET_BOOL("bChangeRankTitlesSource", bChangeRankTitlesSource)
						ADD_WIDGET_BOOL("bRankTitlesUsePersonal", bRankTitlesUsePersonal)
					STOP_WIDGET_GROUP()
					
					
					
					ADD_WIDGET_BOOL("Play MP Intro Splash", bRunSplashScreenHud)
					ADD_WIDGET_BOOL("Display Saving Data", bDisplaySavingData)
					ADD_WIDGET_BOOL("What Timer is Displaying" , g_DisplayTimersDisplaying)
					ADD_WIDGET_BOOL("HUD Order Prints", g_bHUDOrderPrints)
					ADD_WIDGET_STRING("")
					ADD_WIDGET_STRING("")
					ADD_WIDGET_STRING("Flow Mission Selector")
					//ADD_WIDGET_BOOL("Activate the mission selector", g_bMissionSelectorActive)
	//				ADD_WIDGET_BOOL("bTurnonRaceBox", bTurnonRaceBox)
					ADD_WIDGET_INT_SLIDER("RaceBoxType", RaceBoxType, 0, 3, 1)
					
	//				ADD_WIDGET_BOOL("Turn On Overhead Prints HealthBar Related", g_TurnOnOverheadDebugHealthBar)
					ADD_WIDGET_BOOL("Turn on Slot Checker", turnonSlotChecker)
	//				ADD_WIDGET_BOOL("GivePlayerWeapons", GivePlayerWeapons)
					ADD_WIDGET_BOOL("Draw_crewTag", Draw_crewTag)
					ADD_WIDGET_INT_SLIDER("Change Shirt Colour", iShirtColourPalette, 0, 16, 1)
					ADD_WIDGET_BOOL("Refresh Shirt Colour", bRefreshShirtColour)
					ADD_WIDGET_BOOL("Is a Rockstar Dev", g_bIsRockstarDev)
					ADD_WIDGET_BOOL("g_iFakeSLotFAil", g_iFakeSLotFAil)
					ADD_WIDGET_BOOL("bDrawArrows", bDrawArrows)
	//				ADD_WIDGET_INT_SLIDER("IntenseWidget", IntenseWidget, 0, 6, 1)
					ADD_WIDGET_BOOL("Force PlayerHas Played An Hour", g_bForcePlayerPlayedAnHour)
					ADD_WIDGET_BOOL("Kick Local Player", g_Widget_KickLocalPlayer)
					ADD_WIDGET_BOOL("TurnonJoinFailedSCreen", TurnonJoinFailedSCreen)
					
					ADD_WIDGET_BOOL("TurnonJoinFailedSCreen", TurnonJoinFailedSCreen)
					
					START_WIDGET_GROUP("IDLE KICK")
						ADD_WIDGET_BOOL("Disable Idle Kick", bDisableIdleKick)
						ADD_WIDGET_INT_SLIDER("How Long Idling ", MPGlobalsHud.iHowLongIdling, LOWEST_INT, HIGHEST_INT, 1000)
					STOP_WIDGET_GROUP()
					
					
					START_WIDGET_GROUP("CONNECTION WIDGETS")
						ADD_WIDGET_BOOL("Compat check fail main transition", g_b_MakeCompatPackCheckFail_Maintransition)
					STOP_WIDGET_GROUP()
				
					START_WIDGET_GROUP("SPINNER OFFSET")	
						ADD_WIDGET_BOOL("TURN ON THESE WIDGETS ", g_b_TurnOnStreetnameMOVEMENT)
						ADD_WIDGET_BOOL("Force on street names", g_B_TurnOffStreetNames)
						ADD_WIDGET_BOOL("street names PRINTS", g_b_TurnOnStreetnamePrints)
						ADD_WIDGET_BOOL("Set StreetName Widget Use", g_b_StreenameWidgetUse)
						
						ADD_WIDGET_FLOAT_SLIDER("iXPosLoadingIconAboveButton", iXPosLoadingIconAboveButton, -1, 1, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("iYPosLoadingIconAboveButton", iYPosLoadingIconAboveButton, -1, 1, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("iXPosSubtitlesAboveButton", iXPosSubtitlesAboveButton, -1, 1, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("iYPosSubtitlesAboveButton", iYPosSubtitlesAboveButton, -1, 1, 0.001)
						
						ADD_WIDGET_FLOAT_SLIDER("iXPosStreetNameAboveButton", iXPosStreetNameAboveButton, -1, 1, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("iYPosStreetNameAboveButton", iYPosStreetNameAboveButton, -1, 1, 0.001)

					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("RANK BADGE")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
						ADD_WIDGET_BOOL("Draw_DebugRankbadge", Draw_DebugRankbadge)
						ADD_WIDGET_INT_SLIDER("RankBadgeLevel", RankBadgeLevel, 1, 8000, 1)
						
						ADD_WIDGET_FLOAT_SLIDER("RankBadgeX_Widget", RankBadgeX_Widget,-1, 1, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("RankBadgeY_Widget", RankBadgeY_Widget,-1, 1, 0.001)
						
						CREATE_A_SPRITE_PLACEMENT_WIDGET(XPIconSprite_Widget, "XPIconSprite_Widget")
						CREATE_A_SPRITE_PLACEMENT_WIDGET(XPIconSprite99_Widget, "XPIconSprite99_Widget")
						CREATE_A_SPRITE_PLACEMENT_WIDGET(XPIconSprite999_Widget, "XPIconSprite999_Widget")
					STOP_WIDGET_GROUP()	

					START_WIDGET_GROUP("CREW TAG")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
						ADD_WIDGET_BOOL("Draw_crewTagPlayer", Draw_crewTagPlayer)
						ADD_WIDGET_FLOAT_SLIDER("CrewTagScale",CrewTagScale, -10, 10, 0.001)
					STOP_WIDGET_GROUP()	
					
					START_WIDGET_GROUP("PAUSE MENU")
						ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
						ADD_WIDGET_BOOL("Do Blur In", DoBlurIn)
						ADD_WIDGET_BOOL("Do Blur Out", DoBlurOut)
						ADD_WIDGET_BOOL("TurnOnPauseMenuEMPTY", TurnOnPauseMenuEMPTY)
						ADD_WIDGET_BOOL("TurnOnPauseMenSOMETHING", TurnOnPauseMenSOMETHING)
						ADD_WIDGET_BOOL("TurnOffPauseMen", TurnOffPauseMen)
						ADD_WIDGET_BOOL("IsPauseMenuActive", IsPauseMenuActive)
						ADD_WIDGET_INT_READ_ONLY("SKY BLUR STAGE", SkyBlurStage)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("JOINING NUMBERS")
						ADD_WIDGET_INT_SLIDER("Joining: Change number of players on your team",g_iNumberOfplayersIncludingMeWidget, -40, 40, 1 )
						ADD_WIDGET_INT_READ_ONLY("Joining: number of players in the game on your team",g_iNumberOfplayersIncludingMeDisplay)
						ADD_WIDGET_INT_READ_ONLY("Joining: number of actual players in the game on your team",g_iNumberOfplayersIncludingMeRawDisplay)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("HEALTH SNACK")
						ADD_WIDGET_INT_SLIDER("Amount Of Health To Give", g_iSnackHealthAmount, 0, 1000, 5)
						ADD_WIDGET_FLOAT_SLIDER("Probability Of Snack Being Dropped", g_fSnackHealthProbability, 0, 1, 0.01)
						ADD_WIDGET_BOOL("Update Snack Dropping ", bUpdateSnackDropping)
					
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("MY CREW DATA")				
						ADD_WIDGET_INT_SLIDER("ID", g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id, -1, 99999, 1)
						ADD_WIDGET_STRING(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanName)
						ADD_WIDGET_STRING(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanTag)
						ADD_WIDGET_INT_READ_ONLY("MEMBER COUNT", g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].MemberCount)
						ADD_WIDGET_INT_READ_ONLY("IS SYSTEM", g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].IsSystemClan)
						ADD_WIDGET_INT_READ_ONLY("IS OPEN", g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].IsOpenClan)
						ADD_WIDGET_STRING(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].RankName)
						ADD_WIDGET_INT_READ_ONLY("RANK ORDER", g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].RankOrder)
						ADD_WIDGET_BOOL("IS CREW COLOUR SET", g_Private_IsCrewColourSet)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("HUD")
					
						ADD_WIDGET_INT_SLIDER("BIRDS AND BEES RATE", g_i_Private_BirdsAndBeesAlphaRate, 0, 255, 1)
						ADD_WIDGET_INT_SLIDER("BIRDS AND BEES COLOUR DIFF", g_i_Private_BirdsAndBeesColourDiff, 0, 255, 1)
						
						
//						START_WIDGET_GROUP("Ped Head Shot")
//							ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
//							ADD_WIDGET_BOOL("TurnOnPedHeadshot", TurnOnPedHeadshot)
//							ADD_WIDGET_BOOL("SET_PEDHEADSHOT_CUSTOM_LIGHTING_Enable", HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHTING_Enable)
							
//							FOR Headshot_Index = 0 TO 3
//								HeadWidgetName = "Light Number "
//								HeadWidgetName += Headshot_Index
//								START_WIDGET_GROUP(HeadWidgetName)
//									ADD_WIDGET_VECTOR_SLIDER("SET_PEDHEADSHOT_CUSTOM_LIGHT_vPos", HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vPos[Headshot_Index], -100, 100, 1)
//									ADD_WIDGET_VECTOR_SLIDER("SET_PEDHEADSHOT_CUSTOM_LIGHT_vColor", HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vColor[Headshot_Index], -100, 100, 1)
//									ADD_WIDGET_FLOAT_SLIDER("SET_PEDHEADSHOT_CUSTOM_LIGHT_intensity", HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_intensity[Headshot_Index], -100, 100, 0.001)
//									ADD_WIDGET_FLOAT_SLIDER("SET_PEDHEADSHOT_CUSTOM_LIGHT_radius", HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_radius[Headshot_Index], -100,100, 0.001)
//								STOP_WIDGET_GROUP()
//							ENDFOR
//						STOP_WIDGET_GROUP()
					
					
						START_WIDGET_GROUP("EVENT TIMERS")
					
							
					
							START_WIDGET_GROUP("1st2nd3rdCheckpoint")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
								ADD_WIDGET_BOOL("Draw Hud Over Phone", bDrawHudOverPhone)
													
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1STINTSC_2NDINTSC_3RDINTSC_CHECKPOINTS_LOCALINT_TIMER)
								ADD_WIDGET_BOOL("Turn on Player1", bRunEvent_TurnOnPlayer1)
								ADD_WIDGET_BOOL("Turn on Player2", bRunEvent_TurnOnPlayer2)
								ADD_WIDGET_BOOL("Turn on Player3", bRunEvent_TurnOnPlayer3)
								ADD_WIDGET_BOOL("Force Refresh Names", bRunEvent_ForceRefresh)

							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("Coords")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
								ADD_WIDGET_BOOL("Wind meter", b_printwindmeter)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("1st2nd3rdAttemptINT")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1STINT_2NDINT_3RDINT_ATTEMPT_LOCALINT_TIMER)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("Vehicle Team")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_VEHTEAM_ENEMIES_TIMER)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("Vehicle Competitive Team")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_VEHTEAM_COMPETITIVE_ENEMIES_TIMER)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("Gamertag Competitive Team")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_GAMERTAG_COMPETITIVE_ENEMIES_TIMER)
							STOP_WIDGET_GROUP()
							
							
							START_WIDGET_GROUP("1st2nd3rdAttemptFLOAT")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1STFLOAT_2NDFLOAT_3RDFLOAT_ATTEMPT_LOCALFLOAT_TIMER)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("1st2nd3rdAttemptTIME")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1STTIME_2NDTIME_3RDTIME_ATTEMPT_LOCALTIME_TIMER)
							STOP_WIDGET_GROUP()
							
							
							START_WIDGET_GROUP("1st2nd3rdTimeHeld")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1STTIME_2NDTIME_3RDTIME_TIMEHELD_TIMER)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("1st2nd3rdKilled")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1STINT_2NDINT_3RDINT_KILLED_TIMER)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("PlayerstateTimer")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_PLAYER_STATE_TIMER)
							STOP_WIDGET_GROUP()
												
							START_WIDGET_GROUP("CheckpointTimer")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_CHECKPOINT_TIMER)
								ADD_WIDGET_BOOL("Draw Hud Over Phone", bDrawHudOverPhone)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("StartEventTimer")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("1st2nd3rdDamage")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER)
								ADD_WIDGET_BOOL("Draw Hud Over Phone", bDrawHudOverPhone)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("1st2nd3rdScore")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1STINT_2NDINT_3RDINT_LOCALINT_TIMER)
								ADD_WIDGET_BOOL("Draw Hud Over Phone", bDrawHudOverPhone)

							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("1st2nd3rdCash")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_CASH1_CASH2_TIMER)
								ADD_WIDGET_BOOL("Draw Hud Over Phone", bDrawHudOverPhone)

							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("1234Time")
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)														
								ADD_WIDGET_BOOL("Turn on Display", bRunEvent_1TIME_2TIME_3TIME_4TIME_TIMER)
								ADD_WIDGET_BOOL("Draw Hud Over Phone", bDrawHudOverPhone)

							STOP_WIDGET_GROUP()
							
							
							
							
							
							
					
					
						STOP_WIDGET_GROUP()
						
						
					
						START_WIDGET_GROUP("Overhead")
						
							ADD_WIDGET_BOOL("TurnOnOverheadString", TurnOnOverheadString)
										
							ADD_WIDGET_INT_SLIDER("gPRivate_LowerDistance", gPRivate_LowerDistance, 0, 1000, 1)
							ADD_WIDGET_INT_SLIDER("gPRivate_upperDistance", gPRivate_upperDistance, 0, 1000, 1)
							ADD_WIDGET_VECTOR_SLIDER("gPRivate_PositionOverhead", gPRivate_PositionOverhead, -10000, 10000, 1)

							ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
							ADD_WIDGET_INT_SLIDER("G_iPlayerRoleWidgets",G_iPlayerRoleWidgets, 0, 5, 1) 
							ADD_WIDGET_INT_SLIDER("iWidgetHeistRole", G_iHeistRoleWidgets, 0, 5, 1)
							ADD_WIDGET_INT_SLIDER("iWidgetInventory", iWidgetInventory, 0, 128, 1)
							ADD_WIDGET_BOOL("Hide My Overheads", bHideMyOverheads)
							ADD_WIDGET_BOOL("Hide Everyone Elses Overheads", bHideEveryoneElsesOverheads)
							ADD_WIDGET_BOOL("Hide All Overheads", bHideAllOverheads)
							ADD_WIDGET_BOOL("Hide My Overheads This Frame", bHideMyOverheads_ThisFrame)
							ADD_WIDGET_BOOL("Hide Everyone Elses Overheads This Frame", bHideEveryoneElsesOverheads_ThisFrame)
							ADD_WIDGET_BOOL("Hide All Overheads This Frame", bHideAllOverheads_ThisFrame)
							ADD_WIDGET_BOOL("Turn on Vector Icon", bTurnonVectorOverhead)
							ADD_WIDGET_BOOL("Turn on Overhead Sprite Text", g_bTurnOnSpriteText)
							ADD_WIDGET_BOOL("Tag all other players", TagAllRemotePlayers)
							ADD_WIDGET_BOOL("clear players tags", ClearAllRemotePlayersTags)
	//						START_WIDGET_GROUP("OHD EVENT")
	//							FOR WidgetIndex_I = 0 TO NUM_NETWORK_PLAYERS-1
	//								OHDTITLE = "OHD EVENT PLAYER "
	//								OHDTITLE += WidgetIndex_I	
	//								START_WIDGET_GROUP(OHDTITLE)
	//									FOR WidgetIndex_J = 0 TO OH_BITSET_EVENT_NUM-1
	//										OHDTITLE = "OHD EVENT BITSET "
	//										OHDTITLE += WidgetIndex_J	
	//										ADD_BIT_FIELD_WIDGET(OHDTITLE, MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset_LastFrame[WidgetIndex_J][WidgetIndex_I])
	//									ENDFOR
	//								STOP_WIDGET_GROUP()
	//							ENDFOR
	//						STOP_WIDGET_GROUP()
	//						
	//						OHDTITLE = "OHD DISPLAY PLAYER "
	//						START_WIDGET_GROUP("OHD DISPLAY")
	//							FOR WidgetIndex_I = 0 TO NUM_NETWORK_PLAYERS-1	
	//								OHDTITLE = "OHD DISPLAY PLAYER "
	//								OHDTITLE += WidgetIndex_I
	//								START_WIDGET_GROUP(OHDTITLE)
	//									FOR WidgetIndex_J = 0 TO OH_BITSET_NUM-1
	//										OHDTITLE = "OHD DISPLAY BITSET "
	//										OHDTITLE += WidgetIndex_J	
	//										ADD_BIT_FIELD_WIDGET(OHDTITLE, MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[WidgetIndex_J][WidgetIndex_I])
	//									ENDFOR
	//								STOP_WIDGET_GROUP()
	//							ENDFOR
	//						STOP_WIDGET_GROUP()
	//						
	//						
	//						START_WIDGET_GROUP("OHD LOGIC")
	//							FOR WidgetIndex_I = 0 TO NUM_NETWORK_PLAYERS-1
	//								OHDTITLE = "OHD LOGIC PLAYER "
	//								OHDTITLE += WidgetIndex_I
	//								START_WIDGET_GROUP(OHDTITLE)
	//									FOR WidgetIndex_J = 0 TO OH_BITSET_LOGIC_NUM-1
	//										OHDTITLE = "OHD LOGIC BITSET "
	//										OHDTITLE += WidgetIndex_J	
	//										ADD_BIT_FIELD_WIDGET(OHDTITLE, MPGlobalsHud.DisplayInfo.iOverheadLogicBitset_LastFrame[WidgetIndex_J][WidgetIndex_I])
	//									ENDFOR
	//								STOP_WIDGET_GROUP()
	//							ENDFOR
	//						STOP_WIDGET_GROUP()
							
						STOP_WIDGET_GROUP()
						START_WIDGET_GROUP("Bottom Right")
														
//							START_WIDGET_GROUP("New Layout")
//								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
//								ADD_WIDGET_BOOL("Turn on the Overlay", bTurnOnBottomRightOverlay)
//								ADD_WIDGET_BOOL("Turn on RACE Overlay", bTurnOnBottomRightRaceDisplay)
//								
//								ADD_WIDGET_BOOL("Turn on DM Overlay", bTurnOnBottomRightDMDisplayALL)
//								ADD_WIDGET_BOOL("Turn on DM Overlay FIRST ", bTurnOnBottomRightDMDisplayFirst)
//								ADD_WIDGET_BOOL("Turn on DM Overlay SECOND", bTurnOnBottomRightDMDisplaySecond)
//								ADD_WIDGET_BOOL("Turn on DM Overlay THIRD", bTurnOnBottomRightDMDisplayThird)
//								ADD_WIDGET_BOOL("Turn on DM Overlay FOURTH", bTurnOnBottomRightDMDisplayFourth)
//
//								ADD_WIDGET_BOOL("Turn on DM Chris ", bTurnonDMLeaderTimer)
//								ADD_WIDGET_BOOL("Flip DM Timer Chris", bFlipOrderingDMTimer)
//
//								
//		
//								ADD_WIDGET_INT_SLIDER("Meter Line Slider", PercentageLine, 0, 9, 1)
//								ADD_WIDGET_INT_SLIDER("OverlayAlpha", OverlayAlpha, 0, 255, 1)
//								ADD_WIDGET_INT_SLIDER("iDamageMeterValue", iDamageMeterValue, 0, 100000, 1)
//								ADD_WIDGET_INT_SLIDER("iDamageMeterDENOM", iDamageMeterDENOM, 0, 100, 1)
//								ADD_WIDGET_INT_SLIDER("iPlacementValue", iPlacementValue, 0, 1000, 1)
//								ADD_WIDGET_INT_SLIDER("iPlacementDENOM", iPlacementDENOM, 0, 1000, 1)
//								ADD_WIDGET_INT_SLIDER("Weapon Type", WeaponTypeInt, 0, 15, 1)
//								CREATE_A_SPRITE_PLACEMENT_WIDGET(XPIconSprite_Widget, "XPIconSprite_Widget")
//								
//							STOP_WIDGET_GROUP()
							START_WIDGET_GROUP("Individual Drawing")
							
								ADD_WIDGET_BOOL("G_bTurnOnInGameWidgets", G_bTurnOnInGameWidgets)
								ADD_WIDGET_BOOL("Turn on Team Names", bTurnOnTeamNames)
								ADD_WIDGET_BOOL("Turn on Far Left Justification", bTurnOnFarLeftJustified)
								ADD_WIDGET_BOOL("Turn on Middle Justification", bTurnOnMiddleJustified)

								ADD_WIDGET_BOOL("bUseNonPlayerFont", bUseNonPlayerFont)
								ADD_WIDGET_BOOL("Damage Bar", bTurnOnDamageBar)
								ADD_WIDGET_BOOL("Sprite Damage Bar", bTurnOnSpriteDamageBar)
								ADD_WIDGET_BOOL("Checkpoint Bar", bTurnOnCheckpointBar)
								ADD_WIDGET_BOOL("Elimination Bar", bTurnOnEliminationBar)
								ADD_WIDGET_BOOL("Package Strings 1", bdisplayPackageStrings1)
								ADD_WIDGET_BOOL("Package Strings 2", bdisplayPackageStrings2)
								ADD_WIDGET_BOOL("Package Bars", bdisplayPackage)
								ADD_WIDGET_BOOL("Score", bTurnOnScore)
								ADD_WIDGET_BOOL("Double Number", bTurnOnDoubleNumber)
								ADD_WIDGET_BOOL("Single Number", bTurnOnSingleNumber)
								ADD_WIDGET_BOOL("Double Number Place", bTurnOnDoubleNumberPlace)
								ADD_WIDGET_BOOL("Timer Double", bTurnOnTimer)
								ADD_WIDGET_BOOL("Timer Triple", bTurnOnTimerTriple)
								ADD_WIDGET_BOOL("Big Race Position", bTurnOnBigRacePosition)
								ADD_WIDGET_BOOL("Four Icon Bar", bTurnOnFourIconBar)
								ADD_WIDGET_BOOL("Five Icon Score Bar", bTurnOnFiveIconScoreBar)
								ADD_WIDGET_BOOL("Double Text", bTurnOnDoubleText)
								
								ADD_WIDGET_BOOL("Biker Business", bTurnOnBikerBusiness)
								
								ADD_WIDGET_BOOL("Timer checkpoint w. crosses", bDrawCrossesCheckpoints)
								ADD_WIDGET_BOOL("Timer Elimination w. crosses", bDrawCrossesElimination)
								
								ADD_WIDGET_INT_SLIDER("Meter Line Slider", PercentageLine, 0, 9, 1)
								ADD_WIDGET_INT_SLIDER("iDamageMeterValue", iDamageMeterValue, 0, 100000, 1)
								ADD_WIDGET_INT_SLIDER("iDamageMeterDENOM", iDamageMeterDENOM, 0, 100, 1)
								ADD_WIDGET_INT_SLIDER("iPlacementValue", iPlacementValue, 0, 1000, 1)
								ADD_WIDGET_INT_SLIDER("iPlacementDENOM", iPlacementDENOM, 0, 1000, 1)
								ADD_WIDGET_INT_SLIDER("iTimer1Value", iTimer1Value, 0, 999999999, 500)
								ADD_WIDGET_INT_SLIDER("iTimer2Value", iTimer2Value, 0, 100000000, 500)
								ADD_WIDGET_INT_SLIDER("iFlashTimer", iFlashTimer, 0, 10000, 500)
								ADD_WIDGET_INT_SLIDER("iExtraTime", iExtraTime, -20000, 20000, 1000)
								ADD_WIDGET_BOOL("Draw Infinity Single Number", bTurnOnSingleNumberInfinity)
								ADD_WIDGET_INT_SLIDER("Weapon Type", WeaponTypeInt, 0, 68, 1)
							
							
								ADD_WIDGET_INT_SLIDER("ScoreFlashingType", ScoreFlashingType, 0, 4, 1)
								ADD_WIDGET_INT_SLIDER("ScoreFlashingTime" , ScoreFlashingTime, 0, 100000, 1000)
								
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_CHECKPOINT", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_CHECKPOINT)])
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_DOUBLE_NUMBER", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_DOUBLE_NUMBER)])
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_DOUBLE_NUMBER_PLACE", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_DOUBLE_NUMBER_PLACE)])
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_ELIMINATION", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_ELIMINATION)])
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_METER", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_METER)])
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_SCORE", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_SCORE)])
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_SINGLE_NUMBER", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_SINGLE_NUMBER)])
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_SINGLE_NUMBER", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_SINGLE_NUMBER)])
//								ADD_BIT_FIELD_WIDGET("CURRENT PROGRESSHUD_TIMER", MPGlobalsScoreHud.ProgressHud_ActivationBitset[ENUM_TO_INT(PROGRESSHUD_TIMER)])
						
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_CHECKPOINT", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_CHECKPOINT)])
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_DOUBLE_NUMBER", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_DOUBLE_NUMBER)])
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_DOUBLE_NUMBER_PLACE", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_DOUBLE_NUMBER_PLACE)])
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_ELIMINATION", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_ELIMINATION)])
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_METER", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_METER)])
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_SCORE", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_SCORE)])
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_SINGLE_NUMBER", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_SINGLE_NUMBER)])
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_SINGLE_NUMBER", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_SINGLE_NUMBER)])
//								ADD_BIT_FIELD_WIDGET("LAST PROGRESSHUD_TIMER", MPGlobalsScoreHud.ProgressHud_LastFrameBitset[ENUM_TO_INT(PROGRESSHUD_TIMER)])
					
							STOP_WIDGET_GROUP()
							START_WIDGET_GROUP("VOTE BAR")
								ADD_WIDGET_INT_SLIDER("iVoteBitset", iVoteBitset, 0, 10000, 500)
								ADD_WIDGET_BOOL("bDisplayVoteBar", bDisplayVoteBar)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("Four Icon Bar Flashing")
								ADD_WIDGET_BOOL("Flash Icon One", bFourIconBarFlashingIconOne)
								ADD_WIDGET_BOOL("Flash Icon Two", bFourIconBarFlashingIconTwo)
								ADD_WIDGET_BOOL("Flash Icon Three", bFourIconBarFlashingIconThree)
								ADD_WIDGET_BOOL("Flash Icon Four", bFourIconBarFlashingIconFour)
							STOP_WIDGET_GROUP()
							
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					
					ADD_WIDGET_BOOL("PauseMenu: SP category", g_bDebugPMIncludeSPCategory)
					
					START_WIDGET_GROUP("MP Intro Cutscene")
						ADD_WIDGET_BOOL("Launch MP Intro Cutscene", bLaunchMPIntro)
						ADD_WIDGET_BOOL("MP Intro Cutscene Launched", bHasLaunchedMpIntroCut)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Overheads")
						ADD_BIT_FIELD_WIDGET("Block Health Bars", MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0])
						ADD_WIDGET_BOOL("Using Chat Window", bInChatWindow)
						ADD_WIDGET_BOOL("Block Local Player Name Tag", g_bBlockLocalPlayerOverhead[0])
						ADD_WIDGET_BOOL("Toggle on Respawning OHD Event", MPGlobalsHud.bInRaceRespawnDelay[0])
						ADD_WIDGET_BOOL("Turn on Overhead Prints", g_TurnOnOverheadDebug)
						ADD_WIDGET_BOOL("Turn on arrow debug prints", g_bDoArrowCheckDebug)
						ADD_WIDGET_BOOL("Turn on passive debug prints", g_bDoOverheadsPassivePrints)
						ADD_WIDGET_BOOL("Turn on logic and event active debug prints", g_bDoSetLogicAndEventActivePrints)
						ADD_WIDGET_BOOL("Turn On Overhead Prints Extra", g_TurnOnOverheadDebugFullOn)
						ADD_WIDGET_BOOL("Turn on Overhead Rally Driver Prints", g_TurnOnRallyDriverOverheadDebug)
						ADD_WIDGET_BOOL("Turn on Overhead Same Veh Prints", g_TurnOnSameVehCheckDebug)
						ADD_WIDGET_BOOL("Show Rally Icons p0", MPGlobalsHud.bShowRallyIcons[1][0])
						ADD_WIDGET_BOOL("Show Rally Icons p1", MPGlobalsHud.bShowRallyIcons[1][1])
						ADD_WIDGET_BOOL("Show Rally Icons p2", MPGlobalsHud.bShowRallyIcons[1][2])
						START_WIDGET_GROUP("Player Overhead Toggle")
							INT iCounter
							TEXT_LABEL_31 tl31Temp
							REPEAT NUM_NETWORK_PLAYERS iCounter
								tl31Temp = "Toggle Block OH On Player "
								tl31Temp += iCounter
								ADD_WIDGET_BOOL(tl31Temp, g_bTogglePlayerOverhead[0][iCounter])
							ENDREPEAT
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Heist Bonus Challenges")
						ADD_WIDGET_BOOL("Update Heist Bonus Challenge Progress", g_DEBUG_Heist_Enable_Flow_Challenge_Widgets)
						START_WIDGET_GROUP("First Time")
							ADD_WIDGET_BOOL("Fleeca", g_DEBUG_Heist_First_Time_Fleeca)
							ADD_WIDGET_BOOL("Prison", g_DEBUG_Heist_First_Time_Prison)
							ADD_WIDGET_BOOL("Humane", g_DEBUG_Heist_First_Time_Humane)
							ADD_WIDGET_BOOL("Series A", g_DEBUG_Heist_First_Time_Series)
							ADD_WIDGET_BOOL("Pacific", g_DEBUG_Heist_First_Time_Pacific)
						STOP_WIDGET_GROUP()
							ADD_BIT_FIELD_WIDGET("Flow Order", g_DEBUG_Heist_Flow_Order_BS)
							ADD_BIT_FIELD_WIDGET("Same Team", g_DEBUG_Heist_Same_Team_BS)
							ADD_BIT_FIELD_WIDGET("Ultimate Challenge", g_DEBUG_Heist_No_Deaths_BS)
							ADD_BIT_FIELD_WIDGET("Member", g_DEBUG_Heist_Member_BS)
							ADD_BIT_FIELD_WIDGET("First Person",g_DEBUG_Heist_First_Person_BS)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Lowrider Ambient Events")
						ADD_WIDGET_BOOL("bTurnOnChallengesHud", bTurnOnChallengesHud)
						ADD_WIDGET_BOOL("bTurnOnCheckpointHud", bTurnOnCheckpointHud)
						ADD_WIDGET_BOOL("bTurnOnTimeTrialHud", bTurnOnTimeTrialHud)
						ADD_WIDGET_BOOL("bTurnOnWarfareHud", bTurnOnWarfareHud)
					STOP_WIDGET_GROUP()
					
				STOP_WIDGET_GROUP()
			
			
	//			HeadShot_Details.HeadshotPed = PLAYER_PED_ID()
			
				inGameOnce = TRUE
			ENDIF
		#ENDIF		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("widget creation")
		#ENDIF
		#ENDIF		
		
		
		
		
		
		#IF IS_DEBUG_BUILD
			IF bSwapModeltoBigfoot
				IF CHANGE_PLAYER_PED_TO_BIGFOOT()
					bSwapModeltoBigfoot = FALSE
				ENDIF
	//			REQUEST_MODEL(IG_ORLEANS)
	//			IF HAS_MODEL_LOADED(IG_ORLEANS)
	//				SET_NET_PLAYER_MODEL(IG_ORLEANS )	
	//				
	//				REMOVE_PED_HELMET(PLAYER_PED_ID(), FALSE)
	//				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayerIsWeird, TRUE)
	//				SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 100.0)
	//				SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 2500)
	//				SET_ENTITY_HEALTH(PLAYER_PED_ID(), 2500)
	//				ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_DANGEROUS_ANIMAL, PLAYER_PED_ID(),0)
	//
	//				//Configure animal's variations.
	//				SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
	//				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0, 0)
	//				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 0, 0, 0)
	//				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0, 0)
	//
	//				//Tell code we're now an animal. Don’t think you need this.
	//				SET_PLAYER_IS_IN_ANIMAL_FORM(TRUE)
	//
	//				bSwapModeltoBigfoot = FALSE
	//				bAmIBigfoot = TRUE
	//				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	//			ENDIF
			ENDIF


			IF bSwapModeltoPlayer
				IF CHANGE_PLAYER_PED_TO_HUMAN()
					bSwapModeltoPlayer = FALSE
				ENDIF
				
	//			VECTOR Pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	//			VECTOR Rot = <<0, 0, GET_ENTITY_HEADING(PLAYER_PED_ID())>>
	//			IF RUN_PED_MENU(aBigFootPed, GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(GET_STAT_CHARACTER_TEAM(), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE)), Pos, Rot, FALSE,TRUE, GET_ACTIVE_CHARACTER_SLOT(), 50, FALSE, TRUE)	
	//			AND HAS_PED_HEAD_BLEND_FINISHED(aBigFootPed)
	//			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(aBigFootPed)
	//
	//				SET_NET_PLAYER_MODEL(GET_PLAYER_MODEL_FOR_TEAM( GET_STAT_CHARACTER_TEAM()))	
	//				CLONE_PED_TO_TARGET(aBigFootPed, PLAYER_PED_ID())
	//				bSwapModeltoPlayer = FALSE
	//				FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
	//				bAmIBigfoot = FALSE
	//				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	//				SETUP_PLAYER_FROM_STATS()
	//			ENDIF
			ENDIF
			
			IF bchangeAfterlifetoBigfoot
				SET_GAME_STATE_ON_DEATH(AFTERLIFE_SPAWN_IN_AS_BIGFOOT)
				bchangeAfterlifetoBigfoot = FALSE
			ENDIF
			
		IF bgivePlayerABeerHat
			TEXT_LABEL_31 SometextForHat
			GIVE_MP_REWARD_CLOTHING(COMP_TYPE_PROPS, GET_PED_COMP_ITEM_FROM_NAME_HASH(MP_M_FREEMODE_01, HASH("DLC_MP_IND_M_PHEAD_6_2"), COMP_TYPE_PROPS, 3), SometextForHat)
			DISPLAY_TSHIRT_AWARD_MESSGE_AFTER_TRANSITION(COMP_TYPE_PROPS)
			bgivePlayerABeerHat = FALSE
		ENDIF
			
		IF bActivatePS4ActivityFeedDebug
		
			TEXT_LABEL_63 txtAFLabel
			TEXT_LABEL_63 txtAFFriendLabel
			SWITCH iDisplayPS4ActivityFeed
				CASE 1 
					txtAFLabel = "My Published Race"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PUBLISHED_RACE(txtAFLabel)
				BREAK
				CASE 2
					txtAFLabel = "My Published DM"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PUBLISHED_DM(txtAFLabel)
				BREAK
				CASE 3
					txtAFLabel = "My Published Capture"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PUBLISHED_CAPTURE(txtAFLabel)
				BREAK
				CASE 4	
					txtAFLabel = "My Verified Race"
					REQUEST_SYSTEM_ACTIVITY_TYPE_VERIFIED_RACE(txtAFLabel)
				BREAK
				CASE 5
					txtAFLabel = "My Verified DM"
					REQUEST_SYSTEM_ACTIVITY_TYPE_VERIFIED_DM(txtAFLabel)
				BREAK
				CASE 6	
					txtAFLabel = "My Verified Capture"
					REQUEST_SYSTEM_ACTIVITY_TYPE_VERIFIED_CAPTURE(txtAFLabel)
				BREAK
				CASE 7
					txtAFFriendLabel = "My Friend"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_FRIENDS_PUBLISHED_RACE(txtAFFriendLabel)
				BREAK
				CASE 8
					
					txtAFFriendLabel = "My Friend"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_FRIENDS_PUBLISHED_DM(txtAFFriendLabel)
				BREAK
				CASE 9
					
					txtAFFriendLabel = "My Friend"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_FRIENDS_PUBLISHED_CAPTURE(txtAFFriendLabel)
				BREAK
				CASE 10
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYING_WITH_FRIENDS()
				BREAK
				CASE 11
					REQUEST_SYSTEM_ACTIVITY_TYPE_GOT_A_TATTOO()
				BREAK
				CASE 12
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_SECOND_PROPERTY()
				BREAK
				CASE 13
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_FIRST_PROPERTY()
				BREAK
				CASE 14
					REQUEST_SYSTEM_ACTIVITY_TYPE_MOVED_HOUSE()
				BREAK
				CASE 15
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_WEAPON()
				BREAK
				CASE 16
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_VEHICLE_MOD()
				BREAK
				CASE 17
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_VEHICLE()
				BREAK
				CASE 18
					REQUEST_SYSTEM_ACTIVITY_TYPE_BOUGHT_CLOTHES()
				BREAK
				CASE 19
					REQUEST_SYSTEM_ACTIVITY_TYPE_BOUGHT_HAIRCUT_OR_MAKEUP()
				BREAK
				CASE 20
					txtAFLabel = "The Job Name"
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_JOB(txtAFLabel)
				BREAK
				CASE 21
					txtAFLabel = "The Heist Name"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYING_HEIST(txtAFLabel)
				BREAK
				CASE 22
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_GOLF()
				BREAK
				CASE 23
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_ARM_WRESTLING()
				BREAK
				CASE 24
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_SHOOTING_RANGE()
				BREAK
				CASE 25
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_DARTS()
				BREAK
				CASE 26
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_TENNIS()
				BREAK
				CASE 27
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_PILOT_SCHOOL()
				BREAK
				CASE 28
					txtAFLabel = "a Playlist Name"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_PLAYLIST(txtAFLabel)
				BREAK
				CASE 29
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_TOURNAMENT()
				BREAK
				CASE 30
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_TOURNAMENT_QUALIFIER()
				BREAK
				CASE 31
					REQUEST_SYSTEM_ACTIVITY_TYPE_WON_TOURNAMENT()
				BREAK
				CASE 32
					txtAFLabel = "an Event Playlist Name"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_EVENT_PLAYLIST(txtAFLabel)
				BREAK
				CASE 33
					txtAFLabel = "a Challenge Playlist Name"
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_CHALLENGE_PLAYLIST(txtAFLabel)
				BREAK
				CASE 34
					REQUEST_SYSTEM_ACTIVITY_TYPE_SETUP_CHALLENGE_PLAYLIST()
				BREAK
				CASE 35
					txtAFLabel = "a Challenge Playlist Name"
					REQUEST_SYSTEM_ACTIVITY_TYPE_WON_CHALLENGE_PLAYLIST(txtAFLabel, iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 36
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_GANG_ATTACK()
				BREAK
				CASE 37
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_BOUNTY(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 38
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_IMPEXPORT()
				BREAK
				CASE 39
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_DELIVER()
				BREAK
				CASE 40
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_SECURITY_VAN()
				BREAK
				CASE 41
					REQUEST_SYSTEM_ACTIVITY_TYPE_COLLECTED_CRATE_DROP()
				BREAK
				CASE 42
					REQUEST_SYSTEM_ACTIVITY_TYPE_COLLECTED_SPECIAL_CRATE_DROP()
				BREAK
				CASE 43
					REQUEST_SYSTEM_ACTIVITY_TYPE_ROBBED_HOLD_UP_STORE()
				BREAK
				CASE 44
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_STUNT_JUMP(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 45
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_ONE_ON_ONE_DM()
				BREAK
				CASE 46
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_IMPROMPTU_RACE()
				BREAK
				CASE 47
					REQUEST_SYSTEM_ACTIVITY_TYPE_UPLOADED_PHOTO()
				BREAK
				CASE 48
					REQUEST_SYSTEM_ACTIVITY_TYPE_MADE_MONEY_BETTING(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 49
					REQUEST_SYSTEM_ACTIVITY_TYPE_MET_LESTER()
				BREAK
				CASE 50
					REQUEST_SYSTEM_ACTIVITY_TYPE_MET_TREVOR()
				BREAK
				CASE 51
					REQUEST_SYSTEM_ACTIVITY_TYPE_MET_MARTIN()
				BREAK

				
				
				CASE 52
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_PROLOGUE()
				BREAK
				CASE 53
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_LESTER1()
				BREAK
				CASE 54
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_FRANKLIN2()
				BREAK
				CASE 55
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_ALL_RANDOM()
				BREAK
				CASE 56
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_SP_MISSION(CHAR_MICHAEL)
				BREAK
				CASE 57
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_SP_MISSION(CHAR_FRANKLIN)
				BREAK
				CASE 58
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_SP_MISSION(CHAR_TREVOR)
				BREAK
				CASE 59
					REQUEST_SYSTEM_ACTIVITY_TYPE_DRIVEN_ALL_VEHICLES()
				BREAK
				CASE 60
					REQUEST_SYSTEM_ACTIVITY_TYPE_COLLECTED_SPACESHIP()
				BREAK
				CASE 61
					REQUEST_SYSTEM_ACTIVITY_TYPE_ALL_LETTER_SCRAPS()
				BREAK
				CASE 62
					REQUEST_SYSTEM_ACTIVITY_TYPE_COLLECTED_SONAR()
				BREAK
				CASE 63
					REQUEST_SYSTEM_ACTIVITY_TYPE_DRIVEN_MILES(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 64
					REQUEST_SYSTEM_ACTIVITY_TYPE_FLOWN_MILES(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 65
					REQUEST_SYSTEM_ACTIVITY_TYPE_RAN_MILES(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 66
					REQUEST_SYSTEM_ACTIVITY_TYPE_BUSTED_10_TIMES(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 67
					REQUEST_SYSTEM_ACTIVITY_TYPE_WASTED_10_TIMES(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 68
					REQUEST_SYSTEM_ACTIVITY_TYPE_FIRED_HALFMIL_BULLETS()
				BREAK
				CASE 69
					REQUEST_SYSTEM_ACTIVITY_TYPE_EVADED_5STAR_WANTED()
				BREAK
				CASE 70
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_CAR_ONLINE()
				BREAK
				CASE 71
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_RHINO_ONLINE()
				BREAK
				CASE 72
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_BUZZARD_ONLINE()
				BREAK
				CASE 73
					REQUEST_SYSTEM_ACTIVITY_TYPE_STOCKMARKET_LOSS()
				BREAK
				CASE 74
					REQUEST_SYSTEM_ACTIVITY_TYPE_STOCKMARKET_INVESTED()
				BREAK
				CASE 75
					REQUEST_SYSTEM_ACTIVITY_TYPE_SP_STUNT_JUMPS(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 76
					REQUEST_SYSTEM_ACTIVITY_TYPE_SP_UNDER_BRIDGE(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 77
					REQUEST_SYSTEM_ACTIVITY_TYPE_FOUND_HIGHEST_POINT()
				BREAK
				CASE 78
					REQUEST_SYSTEM_ACTIVITY_TYPE_DRUNK(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 79
					REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_ALL_PROPERTIES()
				BREAK
				CASE 80
					REQUEST_SYSTEM_ACTIVITY_TYPE_EXPLORATION(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 81
					REQUEST_SYSTEM_ACTIVITY_TYPE_FOUND_LOWEST_POINT()
				BREAK
				CASE 82
					REQUEST_SYSTEM_ACTIVITY_TYPE_HIGH_VEHICLE_SPEND(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 83
					REQUEST_SYSTEM_ACTIVITY_TYPE_HIGH_WEAPON_SPEND(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 84
					REQUEST_SYSTEM_ACTIVITY_TYPE_HIGH_CLOTHES_SPEND(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 85
					REQUEST_SYSTEM_ACTIVITY_TYPE_HOLE_IN_ONE()
				BREAK
				CASE 86
					REQUEST_SYSTEM_ACTIVITY_TYPE_DRIVEN_KILOMETERS(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 87
					REQUEST_SYSTEM_ACTIVITY_TYPE_FLOWN_KILOMETERS(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 88
					REQUEST_SYSTEM_ACTIVITY_TYPE_RAN_KILOMETERS(iDisplayPS4ActivityFeed_INT)
				BREAK
				CASE 89
					REQUEST_SYSTEM_ACTIVITY_TYPE_TAKEN_ANIMAL_PHOTOGRAPHS(AFA_CAT)
				BREAK
				CASE 90
					REQUEST_SYSTEM_ACTIVITY_TYPE_TAKEN_ANIMAL_PHOTOGRAPHS(AFA_BORDER_COLLIE)
				BREAK
				CASE 91
					REQUEST_SYSTEM_ACTIVITY_TYPE_TAKEN_ANIMAL_PHOTOGRAPHS(AFA_PIG)
				BREAK
				CASE 92
					REQUEST_SYSTEM_ACTIVITY_TYPE_TAKEN_ANIMAL_PHOTOGRAPHS(AFA_PUG)
				BREAK
				CASE 93
					REQUEST_SYSTEM_ACTIVITY_TYPE_TAKEN_ANIMAL_PHOTOGRAPHS(AFA_MOUNTAIN_LION)
				BREAK
				
			
			ENDSWITCH
		
			iDisplayPS4ActivityFeed_INT = 0
			iDisplayPS4ActivityFeed = 0
			bActivatePS4ActivityFeedDebug = FALSE
		
		ENDIF
		
		IF ClearNoMoreTutorials
			SET_MP_BOOL_PLAYER_STAT(MPPLY_NO_MORE_TUTORIALS, FALSE)
			ClearNoMoreTutorials = FALSE
		ENDIF
		
		IF bDrawArrows
//			DRAW_RALLY_ARROWS(IntenseWidget, IntenseWidget)
		ENDIF
		
		IF bUpdateSnackDropping
			SET_HEALTH_SNACKS_CARRIED_BY_ALL_NEW_PEDS(g_fSnackHealthProbability, g_iSnackHealthAmount)			
			bUpdateSnackDropping = FALSE
		ENDIF
		#ENDIF
		


		#IF IS_DEBUG_BUILD
			IF HAS_IMPORTANT_STATS_LOADED()
				BadSport_MPPLY_BECAME_BADSPORT_DT_Value = GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT)
			ENDIF
			
		


	
			IF G_bTurnOnInGameWidgets
			
				IF bTriggerBGKickNewGame
					LOBBY_SET_AUTO_MULTIPLAYER(TRUE)
					SHUTDOWN_AND_LOAD_MOST_RECENT_SAVE()
					bTriggerBGKickNewGame = FALSE
				ENDIF
			
				IF b_TurnOnTripleHead
					SET_MULTIHEAD_SAFE(true, false)
					b_TurnOnTripleHead = FALSE	
				ENDIF
				
				IF b_TurnOffTripleHead
					SET_MULTIHEAD_SAFE(false, false)
					b_TurnOffTripleHead = FALSE	
				ENDIF
				
				IF bTurnOnRenderFreeze
					SET_SKYFREEZE_FROZEN(TRUE)
					bTurnOnRenderFreeze = FALSE	
				ENDIF
				
				IF bTurnOffRenderFreeze
					SET_SKYFREEZE_CLEAR(TRUE)
					bTurnOffRenderFreeze = FALSE	
				ENDIF
				
				IF bTurnOffRenderFreezeAndEffect
					SET_SKYFREEZE_CLEAR(TRUE)
					ANIMPOSTFX_STOP_ALL()
					bTurnOffRenderFreezeAndEffect = FALSE
				ENDIF
				

				IF g_b_SetupFakePCGiftsWidget
					g_i_DoCashGiftMessageAmount = g_i_CashGiftWidgetAmount
					g_i_CashGiftWidgetAmount = 0
					
					g_i_DoCashGiftACTUALAmount = g_i_DoCashGiftACTUALAmountWidget
					g_i_DoCashGiftACTUALAmountWidget = 0
					
					g_b_HasItemBeenGivenOnPc = g_b_HasItemBeenGivenOnPcWidget
					g_b_HasItemBeenGivenOnPcWidget = FALSE
				
					g_i_SCAdminCashGiftScreenType = g_i_SCAdminCashGiftScreenTypeWidget
					g_i_SCAdminCashGiftScreenTypeWidget = 0
					
					g_b_IsFakePCGiftsWidgetActive = TRUE
					g_b_SetupFakePCGiftsWidget = FALSE
					
					
				ENDIF
			
				IF bRunHeistPropertyCamera
					HEIST_PROPERTY_CAM_STAGE aStage
					aStage = RUN_HEIST_PROPERTY_CAMERA()
					iHeistCameraStage = ENUM_TO_INT(aStage)
					IF aStage = HEIST_PROPERTY_FINISHED
						bRunHeistPropertyCamera = FALSE
					ENDIF
					IF bRunHeistPropertyCameraMoveOn
						SET_HEIST_PROPERTY_CAMERA_TO_MOVE_INTO_INTERIOR()
						bRunHeistPropertyCameraMoveOn = FALSE
						
					ENDIF
				ENDIF
				
				IF bRunHeistCameraUp
					IF SET_SKYSWOOP_UP()
						bRunHeistCameraUp = FALSE
					ENDIF
				ENDIF
				
				
				IF Draw_crewTagPlayer
					DRAW_CREW_TAG_PLAYER(PLAYER_ID(), 0.2, 0.5, CrewTagScale)
				ENDIF
				

				IF bClearFocus
					CLEAR_FOCUS()
					bClearFocus = FALSE
				ENDIF
				
				iCurrentNumberStreamingRequests = GET_NUMBER_OF_STREAMING_REQUESTS()
				
				
				IF bSwapFocusVectors
					IF bCurrentVectorLoaded = 1
						vVectorFocusPos = vVectorFocusPosConst1
						bCurrentVectorLoaded = 2
						
					ELIF bCurrentVectorLoaded = 2
						vVectorFocusPos = vVectorFocusPosConst2
						bCurrentVectorLoaded = 3
					ELIF bCurrentVectorLoaded = 3
						vVectorFocusPos = vVectorFocusPosConst3
						bCurrentVectorLoaded = 4
					ELIF bCurrentVectorLoaded = 4
						vVectorFocusPos = vVectorFocusPosConst4
						bCurrentVectorLoaded = 1
					ENDIF
					bSwapFocusVectors = FALSE
				ENDIF
				
				IF bFocusDestroyCamera
					IF DOES_CAM_EXIST(FocusCamera)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						DESTROY_CAM(FocusCamera)
						
					ENDIF
					bFocusDestroyCamera = FALSE
				ENDIF
				
				IF bMovePlayerToPos
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vVectorFocusPos, FALSE)
					bMovePlayerToPos = FALSE
				ENDIF
				
				bIsFocusOnPlayer = IS_ENTITY_FOCUS(PLAYER_PED_ID())
				
				IF bStartWarpAtRequestcheck
					IF iWarpAtRequest > -1 AND bIsFocusOnPlayer = FALSE
						IF iCurrentNumberStreamingRequests < iWarpAtRequest
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vVectorFocusPos, FALSE)
							iWarpAtRequest = -1
							bStartWarpAtRequestcheck = FALSE
							bTurnOnCameraAtRequestcheck = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF bTurnOnCameraAtRequestcheck
					IF iTurnOnCameraAtRequest > -1 AND bIsFocusOnPlayer = FALSE
						IF iCurrentNumberStreamingRequests < iTurnOnCameraAtRequest
							CREATE_EXTERIOR_CAMERA(FocusCamera, vVectorFocusPos, vVectorFocusVel, 50)
							iTurnOnCameraAtRequest = -1
							bStartWarpAtRequestcheck = FALSE
							bTurnOnCameraAtRequestcheck = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF bSetVectorFocus
					SET_FOCUS_POS_AND_VEL(vVectorFocusPos, vVectorFocusVel)
					bStartWarpAtRequestcheck = TRUE
					bTurnOnCameraAtRequestcheck = TRUE
					bSetVectorFocus = FALSE
				ENDIF
				
				IF bRestoreFocusOnPlayer
					SET_RESTORE_FOCUS_ENTITY(PLAYER_PED_ID())
					
					bRestoreFocusOnPlayer = FALSE
				ENDIF
				
				IF bSetFocusOnPlayer
					SET_FOCUS_ENTITY(PLAYER_PED_ID())
					bSetFocusOnPlayer = FALSE
				ENDIF
				

				IF bWarpPlayerUsingWrapper
					IF SCRIPT_LOAD_SCENE_AND_WARP(FocusAnInt, vVectorFocusPos, vVectorFocusVel)
					
						bWarpPlayerUsingWrapper = FALSE
					ENDIF
				ENDIF
				
				IF bCameraUsingWrapper
					IF SCRIPT_LOAD_SCENE_CREATE_AND_TURN_ON_CAMERA(FocusAnInt, FocusCamera, vVectorFocusVel, vVectorFocusPos, vLoadingCameraRot, 50)
						bCameraUsingWrapper = FALSE
					ENDIF
				ENDIF

				IF bCreateStreamVolSphere
					streamvolId = STREAMVOL_CREATE_SPHERE(vVectorFocusPos, 100, INT_TO_ENUM(STREAMVOL_ASSET_TYPES, StreamVolAssetType), INT_TO_ENUM(STREAMVOL_LOD_FLAGS, StreamVolAssetLOD))
					bCreateStreamVolSphere = FALSE
				ENDIF
				
				IF STREAMVOL_IS_VALID(streamvolId)
					bHasStreamVolLoaded = STREAMVOL_HAS_LOADED(streamvolId)
				ENDIF
				
				IF bDeleteStreamVolSphere
					IF STREAMVOL_IS_VALID(streamvolId)
						STREAMVOL_DELETE(streamvolId)
					ENDIF
					bDeleteStreamVolSphere = FALSE
				ENDIF
				
				IF bCreateLoadScene
				
					IF NEW_LOAD_SCENE_START(vVectorFocusPos, vVectorFocusVel, fLoadSceneFarclip, INT_TO_ENUM(NEWLOADSCENE_FLAGS, iLoadSceneFlags))
						bCreateLoadScene = FALSE
					ENDIF
				ENDIF
				
				bIsLoadSceneActive = IS_NEW_LOAD_SCENE_ACTIVE()
				bIsLoadSceneLoaded = IS_NEW_LOAD_SCENE_LOADED()
				
				IF bDestroyLoadScene
					NEW_LOAD_SCENE_STOP()
					bDestroyLoadScene = FALSE
				ENDIF
				
				IF bLoadSceneAndWarp
					IF SCRIPT_NEW_LOAD_SCENE_AND_WARP(bLoadingABool, vVectorFocusPos, vVectorFocusVel, fLoadSceneFarclip, INT_TO_ENUM(NEWLOADSCENE_FLAGS, iLoadSceneFlags))
						bLoadSceneAndWarp = FALSE
					ENDIF
				ENDIF
				
				IF bLoadSceneCreateCamAndWarp
					IF SCRIPT_NEW_LOAD_SCENE_CREATE_AND_TURN_ON_CAMERA(bLoadingABool, FocusCamera, vVectorFocusVel, vVectorFocusPos, vLoadingCameraRot, 45, fLoadSceneFarclip, INT_TO_ENUM(NEWLOADSCENE_FLAGS, iLoadSceneFlags))
						bLoadSceneCreateCamAndWarp = FALSE
					ENDIF
				ENDIF
				

				IF b_Trigger_Script_Cloud_Down
					SET_PLAYER_KICKED_TO_GO_OFFLINE(TRUE)
					b_Trigger_Script_Cloud_Down = FALSE
				ENDIF
								
				IF Draw_DebugRankbadge
					
					
//					REQUEST_STREAMED_TEXTURE_DICT("RankBadge")
//					IF HAS_STREAMED_TEXTURE_DICT_LOADED("RankBadge")
//					
//						SPRITE_PLACEMENT XPIconBarsSprite
//					
////						RankBadgeSpritePlacement.w = 0.031
////						RankBadgeSpritePlacement.h = 0.052
//					
//						XPIconBarsSprite.x = 0.5
//						XPIconBarsSprite.y = 0.5
//						XPIconBarsSprite.w = 1.0
//						XPIconBarsSprite.h = 1.0
//						XPIconBarsSprite.a = 255
//						
//						XPIconBarsSprite.x += XPIconSprite_Widget.x
//						XPIconBarsSprite.y += XPIconSprite_Widget.y
//						XPIconBarsSprite.w += XPIconSprite_Widget.w
//						XPIconBarsSprite.h += XPIconSprite_Widget.h
//						XPIconBarsSprite.r += XPIconSprite_Widget.r
//						XPIconBarsSprite.g += XPIconSprite_Widget.g
//						XPIconBarsSprite.b += XPIconSprite_Widget.b
//						XPIconBarsSprite.a += XPIconSprite_Widget.a
//						
//						DRAW_2D_SPRITE("RankBadge", "Leaderboard", XPIconBarsSprite)
//					
//					
//					
//						
//						
//					ENDIF	
					
					DRAW_RANK_BADGE_FONT_LEADERBOARD(RankBadgeLevel, RankBadgeX_Widget, RankBadgeY_Widget, RANKDISPLAYTYPE_FULL_WITH_BOX)

					
					
					
					
					
				ENDIF

			
				IF DoBlurIn
					IF SET_SKYBLUR_BLURRY()
						DoBlurIn = FALSE
					ENDIF
				ENDIF
				
				IF DoBlurOut
					IF SET_SKYBLUR_CLEAR()
						DoBlurOut = FALSE
					ENDIF
				ENDIF
				
				IF bRankXpLimitsPrint
					PRINT_ALL_XP_LIMITS()
					bRankXpLimitsPrint = FALSE
				ENDIF
				
				IF TurnOnPauseMenuEMPTY
					IF IS_PAUSE_MENU_ACTIVE()
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND)
					ELSE
						ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND, FALSE)
					ENDIF
					TurnOnPauseMenuEMPTY = FALSE
				ENDIF	
				
				IF TurnOnPauseMenSOMETHING
					IF IS_PAUSE_MENU_ACTIVE()
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_PRE_LOBBY)
					ELSE
						ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_PRE_LOBBY, FALSE)
					ENDIF
					TurnOnPauseMenSOMETHING = FALSE
				ENDIF
				
				IF TurnOffPauseMen
					SET_FRONTEND_ACTIVE(FALSE)
					TurnOffPauseMen = FALSE
				ENDIF
				
				IsPauseMenuActive = IS_PAUSE_MENU_ACTIVE()
				SkyBlurStage = ENUM_TO_INT(GET_SKYBLUR_STAGE())
					
				IF StartPauseTimer
					START_NET_TIMER(PauseTimer)	
					PauseTimerInt = NATIVE_TO_INT(PauseTimer.Timer)
					PauseTimerDiff = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(PauseTimer)
					IF RunPauseTimer
						NET_TIMER_PAUSE_THIS_FRAME(PauseTimer, PauseTimerTemp)
						
					ENDIF
					IF PauseTimerReset
						NET_TIMER_PAUSE_RESET(PauseTimerTemp)
						PauseTimerReset = FALSE
					ENDIF	
				ENDIF
					
					
				
					
				IF bRefreshShirtColour
				
					IF IS_CHARACTER_MALE(GET_ACTIVE_CHARACTER_SLOT())
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, TORSO_FMM_0_0, FALSE, iShirtColourPalette)
					ELSE
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, TORSO_FMF_0_0, FALSE, iShirtColourPalette)
					ENDIF
				
					bRefreshShirtColour = FALSE
				ENDIF
			
				IF bDisplayVoteBar
					//DRAW_VOTE_ELEMINATION(iVoteBitset, 8, 0)
				ENDIF
			
				GAMER_HANDLE MyGamerhandle= GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				IF Draw_crewTag
					DRAW_CREW_TAG_GAMER(MyGamerhandle, 0.5, 0.5 )
				ENDIF
			
				IF GivePlayerWeapons
					GIVE_PLAYER_WEAPONS_OF_EACH_TYPE()
					GivePlayerWeapons = FALSE
				ENDIF
				
				IF bSetHeistPrisonPlaneDownProfileSetting
					PROFILE_SETTING aSetting = GET_JOB_ACTIVITY_PROFILE_SETTING_FROM_SLOT(0)
					INT StoredInt = GET_PROFILE_SETTING(aSetting)
					NET_NL()NET_PRINT("[BCSYNC] WIDGET DEBUG! JOB_ACTIVITY - PSJA_HEIST_PRISON_PLANE_DONE - SET THIS for SLOT 0 ")			
					SET_BIT(StoredInt,  ENUM_TO_INT(PSJA_HEIST_PRISON_PLANE_DONE))
					SET_JOB_ACTIVITY_ID_STARTED(StoredInt, 0)
					bSetHeistPrisonPlaneDownProfileSetting = FALSE
				ENDIF
			
				IF bHideEveryoneElsesOverheads
					INT I
					PLAYER_INDEX aPlayer
					FOR I = 0 TO NUM_NETWORK_PLAYERS-1
						aPlayer = INT_TO_NATIVE(PLAYER_INDEX, I)
						IF aPlayer <> PLAYER_ID()
							IF aPlayer <> NULL
								IF IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(aPlayer)) = FALSE
									DISABLE_SCRIPT_HUD(HUDPART_THISPLAYER_OVERHEADS, true, NATIVE_TO_INT(aPlayer))
								ENDIF
							ENDIF
							
						ENDIF
					ENDFOR
				ELSE
					INT I
					PLAYER_INDEX aPlayer
					FOR I = 0 TO NUM_NETWORK_PLAYERS-1
						aPlayer = INT_TO_NATIVE(PLAYER_INDEX, I)
						IF aPlayer <> PLAYER_ID()
							IF aPlayer <> NULL
								IF IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(aPlayer))
									DISABLE_SCRIPT_HUD(HUDPART_THISPLAYER_OVERHEADS, FALSE, NATIVE_TO_INT(aPlayer))
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				
				ENDIF
			
			
				IF bHideMyOverheads
					IF IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(PLAYER_ID())) = FALSE
						DISABLE_SCRIPT_HUD(HUDPART_THISPLAYER_OVERHEADS, TRUE, NATIVE_TO_INT(PLAYER_ID()))
					ENDIF
				ELSE
					IF IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(PLAYER_ID())) 
						DISABLE_SCRIPT_HUD(HUDPART_THISPLAYER_OVERHEADS, FALSE, NATIVE_TO_INT(PLAYER_ID()))
					ENDIF
				ENDIF
				
				IF bHideAllOverheads
					IF IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS) = FALSE
						DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, TRUE)
					ENDIF
				
				ELSE
					IF IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS) = TRUE
						DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, FALSE)
					ENDIF
				
				ENDIF
				
				IF bPrintAllXpTypeHashes
					

					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_INVALID ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_INVALID))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_AWARDS ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_AWARDS))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_SOCIALCLUB ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_SOCIALCLUB))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_VEHICLE ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_VEHICLE))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_SKIP_INGAME ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_SKIP_INGAME))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_SKIP_DEBUG ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_SKIP_DEBUG))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_WANTED_LEVEL ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_WANTED_LEVEL))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_ACTION ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_ACTION))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_COMPLETE ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_COMPLETE))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_COLLECT ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_COLLECT))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_SKILL ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_SKILL))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_PLAYLIST ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_PLAYLIST))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_SCADMIN ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_SCADMIN))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_MENTAL_STATE ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_MENTAL_STATE))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_INITIALISE ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_INITIALISE))	
					NET_NL()NET_PRINT("[AWARDXP] XPTYPE_ERROR ")NET_PRINT_INT(ENUM_TO_INT(XPTYPE_ERROR))	
					
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_INVALID ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_INVALID))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_BRONZE_AWARD ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_BRONZE_AWARD))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SILVER_AWARD ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SILVER_AWARD))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_GOLD_AWARD ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_GOLD_AWARD))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_PLATINUM_AWARD ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_PLATINUM_AWARD))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SOCIALCLUB_JOINED_CREW ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SOCIALCLUB_JOINED_CREW))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SOCIALCLUB_MISSION_LIKES ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SOCIALCLUB_MISSION_LIKES))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SOCIALCLUB_PLAYED_A_FRIENDS_MISSION ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SOCIALCLUB_PLAYED_A_FRIENDS_MISSION))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SOCIALCLUB_PLAYED_OWN_CONTENT_AGAINST_OTHERS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SOCIALCLUB_PLAYED_OWN_CONTENT_AGAINST_OTHERS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SOCIALCLUB_ACCEPTED_MISSION_INVITATION ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SOCIALCLUB_ACCEPTED_MISSION_INVITATION))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SOCIALCLUB_MISSION_VERIFIED ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SOCIALCLUB_MISSION_VERIFIED))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SOCIALCLUB_XP_INBOX ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SOCIALCLUB_XP_INBOX))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SOCIALCLUB_XP_CHALLENGE_EXPIRED ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SOCIALCLUB_XP_CHALLENGE_EXPIRED))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_VEHICLE_CRUISE_WITH_BUDDY ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_VEHICLE_CRUISE_WITH_BUDDY))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_VEHICLE_DESTROYED_W_PLAYER_INSIDE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_VEHICLE_DESTROYED_W_PLAYER_INSIDE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKIP_INGAME_END_TUTORIAL ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKIP_INGAME_END_TUTORIAL))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKIP_DEBUG_ONDEMO ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKIP_DEBUG_ONDEMO))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_WANTED_LEVEL_LOST ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_WANTED_LEVEL_LOST))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_WANTED_LEVEL_KEPT_1MIN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_WANTED_LEVEL_KEPT_1MIN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_ACTION_HEADSHOTS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_ACTION_HEADSHOTS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_ACTION_KILLS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_ACTION_KILLS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_ACTION_ASSISTED_KILLS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_ACTION_ASSISTED_KILLS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_ACTION_SOLD_DRUGS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_ACTION_SOLD_DRUGS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_ACTION_STOPPED_MUGGER ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_ACTION_STOPPED_MUGGER))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_ACTION_HIDEOUT_KILLS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_ACTION_HIDEOUT_KILLS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_ACTION_SURVIVAL_KILLS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_ACTION_SURVIVAL_KILLS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_BOUNTY_HUNT_EXECUTED ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_BOUNTY_HUNT_EXECUTED))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_BOUNTY_HUNT_ESCAPED ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_BOUNTY_HUNT_ESCAPED))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_DARTS_WIN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_DARTS_WIN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_A_GOLF_HOLE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_A_GOLF_HOLE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_A_TENNIS_GAME ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_A_TENNIS_GAME))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_ROBBED_SECURITY_VAN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_ROBBED_SECURITY_VAN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_HOLD_UP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_HOLD_UP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_IMPORT_EXPORT_BOARD ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_IMPORT_EXPORT_BOARD))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_IMPORT_EXPORT_DELIVERY ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_IMPORT_EXPORT_DELIVERY))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_FIRST_RACE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_FIRST_RACE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_RACE_TAKING_PART ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_RACE_TAKING_PART))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_TEN_ROCKSTAR_JOBS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_TEN_ROCKSTAR_JOBS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_TEN_OTHER_USER_JOBS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_TEN_OTHER_USER_JOBS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_DM ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_DM))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_VOTE_REWARD ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_VOTE_REWARD))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_HIDEOUT_CLEARED ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_HIDEOUT_CLEARED))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_DM_IN_TOP_THREE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_DM_IN_TOP_THREE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_SURVIVAL_TAKEN_PART ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_SURVIVAL_TAKEN_PART))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_SURVIVAL_WAVE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_SURVIVAL_WAVE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_MISSION_DELIVERED_VEHICLE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_MISSION_DELIVERED_VEHICLE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_MISSION_DELIVERED_OBJ ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_MISSION_DELIVERED_OBJ))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_MISSION_DELIVERED_PED ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_MISSION_DELIVERED_PED))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_MISSION_TIMETRAIL_TEAM_FINISHED_FIRST ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_MISSION_TIMETRAIL_TEAM_FINISHED_FIRST))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_MISSION_TIMETRAIL_SINGLE_TEAM ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_MISSION_TIMETRAIL_SINGLE_TEAM))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_MISSION_ALLVS1_COMPLETE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_MISSION_ALLVS1_COMPLETE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_MISSION_PASS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_MISSION_PASS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_MISSION_FAIL ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_MISSION_FAIL))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_PLAYED_5_MISSIONS_TODAY ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_PLAYED_5_MISSIONS_TODAY))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_ARM_WRESTING ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_ARM_WRESTING))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_SHOOTING_RANGE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_SHOOTING_RANGE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_SHOOTING_RANGE_GUNRUN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_SHOOTING_RANGE_GUNRUN))
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_GOLF_WON ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_GOLF_WON))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_RACE_TO_POINT_WON ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_RACE_TO_POINT_WON))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_PLAYLIST_WON ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_PLAYLIST_WON))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_PLANE_TAKEDOWN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_PLANE_TAKEDOWN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_DISTRACT_COPS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_DISTRACT_COPS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_DESTROY_VEH ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_DESTROY_VEH))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COLLECT_RESPAWN_PICKUP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COLLECT_RESPAWN_PICKUP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COLLECT_CRATE_DROP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COLLECT_CRATE_DROP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DARTS_BULLSEYE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DARTS_BULLSEYE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_TENNIS_RALLY ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_TENNIS_RALLY))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DESTROYED_CRATE_DROP_PLANE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DESTROYED_CRATE_DROP_PLANE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_OVERTAKE_5 ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_OVERTAKE_5))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_OVERTAKE_10 ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_OVERTAKE_10))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_OVERTAKE_15 ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_OVERTAKE_15))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_FIRST_WHOLE_LAP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_FIRST_WHOLE_LAP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_GOT_IN_FIRST_PLACE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_GOT_IN_FIRST_PLACE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_SLIP_5SEC ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_SLIP_5SEC))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_SLIP_10SEC ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_SLIP_10SEC))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_SLIP_15SEC ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_SLIP_15SEC))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_CLEAN_LAP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_CLEAN_LAP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_FASTEST_LAP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_FASTEST_LAP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_WORLD ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_WORLD))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_PERSONAL_BEST ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_PERSONAL_BEST))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_UNDER_BRIDGE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_UNDER_BRIDGE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_FIRST_WON ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_FIRST_WON))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_POSITION_XP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_POSITION_XP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_RACE_DAILY_WIN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_RACE_DAILY_WIN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_DAILY_WIN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_DAILY_WIN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_VEH_KILL ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_VEH_KILL))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_TWO_TEN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_TWO_TEN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_30_SECS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_30_SECS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_LOW_HEALTH ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_LOW_HEALTH))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_HEADSHOTS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_HEADSHOTS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_MELEE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_MELEE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_HEALTH ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_HEALTH))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_RATIO ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_RATIO))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_PISTOL ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_PISTOL))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_DM_OBJ_PODIUM ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_DM_OBJ_PODIUM))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_STREAK ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_STREAK))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_TWO_TEN_SECS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_TWO_TEN_SECS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_FIRST_30_SECS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_FIRST_30_SECS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_ON_FIRE_KILL ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_ON_FIRE_KILL))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_DEATH_STRK_MRE_KILL ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_DEATH_STRK_MRE_KILL))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_POSTHUMOUS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_POSTHUMOUS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_MORE_KILLS_DEATHS ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_MORE_KILLS_DEATHS))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_GET_FIRST_KILL ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_GET_FIRST_KILL))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_POWER_PLAYER ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_POWER_PLAYER))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SKILL_VEH_OBJ_PODIUM ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SKILL_VEH_OBJ_PODIUM))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETED_SHOWER ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETED_SHOWER))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETED_UNIQUE_STUNT_JUMP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETED_UNIQUE_STUNT_JUMP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETED_ROLLERCOASTER ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETED_ROLLERCOASTER))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETED_DAILY_OBJECTIVES ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETED_DAILY_OBJECTIVES))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SCADMIN_RP_GIFT ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SCADMIN_RP_GIFT))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SCADMIN_SET_RP_GIFT_ADMIN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SCADMIN_SET_RP_GIFT_ADMIN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SCADMIN_TANK_GIVEN_ADMIN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SCADMIN_TANK_GIVEN_ADMIN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_SCADMIN_TANK_REMOVED_ADMIN ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_SCADMIN_TANK_REMOVED_ADMIN))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_MENTAL_STATE_KILLED_MENTALIST ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_MENTAL_STATE_KILLED_MENTALIST))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_INITIALISE_RP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_INITIALISE_RP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_LIVE_RP_RESET ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_LIVE_RP_RESET))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_MENU_DEBUG ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_MENU_DEBUG))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_KEYS_DEBUG ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_KEYS_DEBUG))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_INITIALISE_RP_RACE ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_INITIALISE_RP_RACE))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_INITIALISE_RP_MISSION ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_INITIALISE_RP_MISSION))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_INITIALISE_RP_HOLDUP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_INITIALISE_RP_HOLDUP))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_INITIALISE_RP_CARMOD ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_INITIALISE_RP_CARMOD))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_RP_CORRECTION ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_RP_CORRECTION))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COMPLETE_HOT_TARGET ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COMPLETE_HOT_TARGET))	
					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COLLECT_CHECKPOINT ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COLLECT_CHECKPOINT))	
////					NET_NL()NET_PRINT("[AWARDXP] XPCATEGORY_COLLECT_PLANE_DROP ")NET_PRINT_INT(ENUM_TO_INT(XPCATEGORY_COLLECT_PLANE_DROP))	
				
					
					bPrintAllXpTypeHashes = FALSE
				ENDIF
				
				
				IF bHideEveryoneElsesOverheads_ThisFrame
					INT I
					PLAYER_INDEX aPlayer
					FOR I = 0 TO NUM_NETWORK_PLAYERS-1
						aPlayer = INT_TO_NATIVE(PLAYER_INDEX, I)
						IF aPlayer <> PLAYER_ID()
							IF aPlayer <> NULL
								DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(aPlayer))
								g_heliGunCoPlayerId = aPlayer
							ENDIF
							
						ENDIF
					ENDFOR
			
				ENDIF
			
			
				IF bHideMyOverheads_ThisFrame
						DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(PLAYER_ID()))
			
				ENDIF
				
				IF bHideAllOverheads_ThisFrame
						DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
				
		
				
				ENDIF
				IF TagAllRemotePlayers
					INT I
					PLAYER_INDEX aPlayer
					g_iHeliGunTaggedPlayers = 0
					FOR I = 0 TO NUM_NETWORK_PLAYERS-1
						aPlayer = INT_TO_NATIVE(PLAYER_INDEX, I)
						IF aPlayer <> PLAYER_ID()
							IF aPlayer <> NULL
								SET_BIT(g_iHeliGunTaggedPlayers, NATIVE_TO_INT(aPlayer))
							ENDIF
						ENDIF
					ENDFOR
					NET_NL()NET_PRINT("TagAllRemotePlayers g_iHeliGunTaggedPlayers = ")NET_PRINT_INT(g_iHeliGunTaggedPlayers)
					TagAllRemotePlayers = FALSE
				ENDIF
				
				IF ClearAllRemotePlayersTags
					g_iHeliGunTaggedPlayers = 0
					ClearAllRemotePlayersTags = FALSE
				ENDIF
				
				IF bTurnonVectorOverhead
				
					VECTOR aPosition = GET_PLAYER_COORDS(PLAYER_ID())
					aPosition += gPRivate_PositionOverhead
					
					DRAW_SPRITE_ON_OBJECTIVE_COORD_THIS_FRAME(aPosition, MP_TAG_SCRIPT_ARM_WRESTLING)
				ENDIF
			ENDIF
		#ENDIF
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("widget processing")
		#ENDIF
		#ENDIF		

	
		IF b_BailWithBobbyStuff
			SET_TRANSITION_SESSIONS_INVITE_TIMED_OUT()
			NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_FROM_WIDGET))
			b_BailWithBobbyStuff = FALSE
		ENDIF
		
		
		IF bKickSCTVForRockstar
			IF NETWORK_IS_GAME_IN_PROGRESS()
				PLAYER_INDEX aPlayerToKick = INT_TO_NATIVE(PLAYER_INDEX, iPlayerKickSCTVForRockstar)
				IF aPlayerToKick != INVALID_PLAYER_INDEX()
					BROADCAST_SCRIPT_EVENT_BAIL_ME_FOR_SCTV(aPlayerToKick)
				ENDIF
			ENDIF
			bKickSCTVForRockstar = FALSE
		ENDIF


		IF b_spitoutrewardstring
			DEBUG_FILE_REWARD_OUTPUT()
			b_spitoutrewardstring = FALSE
		ENDIF	
			
		
		IF bPrintSomeObjectiveText
			IF NOT Has_This_MP_Objective_Text_Been_Received("PM_INF_CRSB") 
				Print_Objective_Text("PM_INF_CRSB")
				bPrintSomeObjectiveText = TRUE
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF HAVE_STATS_LOADED() = TRUE
			IF turnonSlotChecker
				IF ALREADY_CREATED_GANG_MEMBER_IN_SLOT_TRANSITION()
					NET_NL()NET_PRINT("ALREADY_CREATED_GANG_MEMBER_IN_SLOT_TRANSITION Found a FM Freemoder")
				ELSE
					NET_NL()NET_PRINT("ALREADY_CREATED_GANG_MEMBER_IN_SLOT_TRANSITION NOT Found a FM Freemoder")
				ENDIF	
				turnonSlotChecker = FALSE
			ENDIF
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("g_bHaveAllStatsLoaded")
		#ENDIF
		#ENDIF
		
		
		
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())			
//			IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())	
//				ENTITY_INDEX aPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				VECTOR PlayerVehCoord = GET_ENTITY_COORDS(aPlayerVeh, FALSE)
//				FLOAT BoatHeading
//				VECTOR vBoattilt
//				VECTOR vBoatCoords
//				GET_ENTITY_DOCKING_POSITIONS(aPlayerVeh, PlayerVehCoord, GET_ENTITY_HEADING(aPlayerVeh), vBoattilt, BoatHeading, vBoatCoords)
//				
//				SET_ENTITY_COORDS_NO_OFFSET(aPlayerVeh,vBoatCoords , FALSE, FALSE, FALSE)
//				SET_ENTITY_ROTATION(aPlayerVeh,vBoattilt)
////				SET_ENTITY_HEADING(aPlayerVeh, BoatHeading)
//				
//			ENDIF
//		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF G_bTurnOnInGameWidgets
			
				IF SetMultiplayerCashText
					ALLOW_DISPLAY_OF_MULTIPLAYER_CASH_TEXT(DisplayMultiplayerCashText)
					SetMultiplayerCashText = FALSE
				ENDIF
				
				IF Set_UseFakeMPCash
					USE_FAKE_MP_CASH(UseFakeMpCash)
					Set_UseFakeMPCash = FALSE
				ENDIF
				
				IF set_changeFakeMpCash
					CHANGE_FAKE_MP_CASH(iChangeFakeMpCash, 69)
					set_changeFakeMpCash = FALSE
				ENDIF
				
				
			
				IF bTurnOnBusySpinner
					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
					END_TEXT_COMMAND_BUSYSPINNER_ON(0)
					bTurnOnBusySpinner = FALSE
				ENDIF
				
				IF b_PrintAllXPAWARDSHashes
					
					PRINT_ALL_AWARDXP_HASHES()
					
					b_PrintAllXPAWARDSHashes = FALSE
				ENDIF
			
				IF bGivePlayerLotsOfMPAwards
				
					IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_PRE_FM_LAUNCH_SCRIPT 
						SET_MP_INT_CHARACTER_AWARD(MP_AWARD_PASSENGERTIME,5)//4 hours as a passenger gets a platinum  
						SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_FILL_TITAN, TRUE)
						SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_A  , TRUE, TRUE)
						SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_B  , TRUE, TRUE)
						SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_C  , TRUE, TRUE)
						INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FMRALLYWONNAV, 15)
						SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMTIME5STARWANTED, GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FMTIME5STARWANTED, AWARDPOSITIONS_GOLD, TEAM_FREEMODE))
						SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH, GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FMDRIVEWITHOUTCRASH, AWARDPOSITIONS_BRONZE, TEAM_FREEMODE))
						SET_MP_INT_CHARACTER_AWARD(MP_AWARD_5STAR_WANTED_AVOIDANCE, GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_5STAR_WANTED_AVOIDANCE, AWARDPOSITIONS_GOLD, TEAM_FREEMODE))
						SET_MP_INT_CHARACTER_AWARD(MP_AWARD_LAPDANCES, GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_LAPDANCES, AWARDPOSITIONS_SILVER, TEAM_FREEMODE))
						SET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACES_WON, GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_RACES_WON, AWARDPOSITIONS_SILVER, TEAM_FREEMODE))
						SET_MP_INT_CHARACTER_AWARD(MP_AWARD_50_KILLS_GRENADES, GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_50_KILLS_GRENADES, AWARDPOSITIONS_SILVER, TEAM_FREEMODE))
						
						
						
						
						bGivePlayerLotsOfMPAwards = FALSE
					ENDIF
				ENDIF
			
			
				IF DisplayIfCharactersAreLastGen
					NET_NL()NET_PRINT("IS_LAST_GEN_PLAYER() = ")NET_PRINT_BOOL(IS_LAST_GEN_PLAYER())
					DisplayIfCharactersAreLastGen = FALSE
				ENDIF
				
				
				
				IF DisplaySessionFullScreen
					SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED, TRUE, ENUM_TO_INT( RESPONSE_DENY_GROUP_FULL))
					DisplaySessionFullScreen = FALSE
				ENDIF
				
				IF DisplaySaveSelectionScreen
					SET_WARNING_MESSAGE_WITH_HEADER("HUD_CONNPROB", "HUD_ST_AVAIL", FE_WARNING_OKCANCEL)
					
					IF IS_WARNING_MESSAGE_READY_FOR_CONTROL() 
						IF DisplaySaveSelectionScreenOneCall = FALSE
							SET_WARNING_MESSAGE_OPTION_ITEMS (0, 
																		GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_SLOTONE"),
																		999,
																		g_struct_Save_transfer_data_PS3.m_xp[0],
																		GET_FM_RANK_FROM_XP_VALUE(g_struct_Save_transfer_data_PS3.m_xp[0]),
																		0)
																		
							SET_WARNING_MESSAGE_OPTION_HIGHLIGHT(0)
							DisplaySaveSelectionScreenOneCall = TRUE

						ENDIF
					ENDIF
				ELSE
					DisplaySaveSelectionScreenOneCall = FALSE
				ENDIF
				
			
//				IF TurnOnBirdsAndBees
//					DRAW_BACKGROUND_BIRDS()
//				ENDIF
				
				IF TurnOnOverheadString
					DISPLAY_PLAYER_OVERHEAD_STRING(PLAYER_ID(), "HUD_OFFRADAR")
				ELSE
					RESET_PLAYER_OVERHEAD_STRING(PLAYER_ID())
				ENDIF
				
			

//				IF TurnOnPedHeadshot
//
//					SET_PEDHEADSHOT_CUSTOM_LIGHTING(HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHTING_Enable)
//					SET_PEDHEADSHOT_CUSTOM_LIGHT(0, 
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vPos[0],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vColor[0],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_intensity[0],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_radius[0])
//					SET_PEDHEADSHOT_CUSTOM_LIGHT(1, 
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vPos[1],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vColor[1],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_intensity[1],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_radius[1])
//					SET_PEDHEADSHOT_CUSTOM_LIGHT(2, 
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vPos[2],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vColor[2],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_intensity[2],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_radius[2])
//					SET_PEDHEADSHOT_CUSTOM_LIGHT(3, 
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vPos[3],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_vColor[3],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_intensity[3],
//												HeadshotWidget_SET_PEDHEADSHOT_CUSTOM_LIGHT_radius[3])


	//				IF bRefreshFace = true
	//	
	//					IF REGISTER_PEDHEADSHOT_TAB(HeadShot_Details.HeadshotPed , HeadShot_Details.PlayerData.PlayerFaceTextureName)
	//					
	//						REFRESH_SCALEFORM_TABS_FACES(HeadShot_Details)
	//						bRefreshFace = FALSE
	//					ENDIF
	//					
	//				ENDIF
	//				HeadShotScaleform = REQUEST_SCALEFORM_MOVIE("pause_menu_header")
	//				IF HAS_SCALEFORM_MOVIE_LOADED(HeadShotScaleform)
	//					RUN_SCALEFORM_TABS_FACES(HeadShotScaleform, HeadShot_Details, SHOULD_REFRESH_SCALEFORM_TABS_FACES(DisplayStruct))
	//				ENDIF
//				ENDIF

			
	//			GlobalplayerBD_CNC[GBD_SLOT(PLAYER_ID())].iMyOverHeadInventory = iWidgetInventory
				
	//			SET_BIT(GlobalplayerBD_CNC[GBD_SLOT(PLAYER_ID())].iMyOverHeadInventory, iWidgetInventory)
	//			
	//			IF GlobalplayerBD_CNC[GBD_SLOT(PLAYER_ID())].iMyOverHeadInventory > 0
	//				GlobalplayerBD_CNC[GBD_SLOT(PLAYER_ID())].bRunOverHeadinventory = TRUE
	//			ELSE
	//				GlobalplayerBD_CNC[GBD_SLOT(PLAYER_ID())].bRunOverHeadinventory = FALSE
	//			ENDIF
			
				IF bTurnOnDamageBar
		
					//DRAW_GENERIC_METER(iDamageMeterValue, iDamageMeterDENOM, GET_PLAYER_NAME(PLAYER_ID()), HUD_COLOUR_GREEN, iFlashTimer, HUDORDER_DONTCARE, -1, -1, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, INT_TO_ENUM(PERCENTAGE_METER_LINE, PercentageLine))
					DRAW_GENERIC_METER(iDamageMeterValue, iDamageMeterDENOM, "HUD_DISTANCE", HUD_COLOUR_GREEN, iFlashTimer, DEFAULT,DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					//DRAW_GENERIC_METER(iDamageMeterValue, iDamageMeterDENOM, "HUD_DISTANCE", HUD_COLOUR_YELLOW, iFlashTimer, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, ScoreFlashingTime, TRUE)
				
					
				ENDIF
				
				IF bTurnOnBikerBusiness
					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, "HUD_CASH")
					BOOL MakeUrgent
					FLOAT Value = (TO_FLOAT(iDamageMeterValue)/(TO_FLOAT(iDamageMeterDENOM))*(TO_FLOAT(100)))
					PRINTLN("VALUE = ", Value)
					IF (Value < 20.0)
						MakeUrgent = TRUE
					ENDIF
					
					DRAW_GENERIC_METER(iDamageMeterValue, iDamageMeterDENOM, "HUD_DISTANCE", HUD_COLOUR_GREEN, iFlashTimer, HUDORDER_SECONDBOTTOM,DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PERCENTAGE_METER_LINE_ALL_20, DEFAULT, DEFAULT, DEFAULT, DEFAULT, MakeUrgent)
					DRAW_GENERIC_METER(iDamageMeterValue, iDamageMeterDENOM, "HUD_DISTANCE", HUD_COLOUR_GREEN, iFlashTimer, HUDORDER_BOTTOM,DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PERCENTAGE_METER_LINE_ALL_20)
				
				ENDIF
				
				IF bTurnOnSpriteDamageBar


		
		
		
					IF WeaponTypeInt <= 0
						CurrentWeapon = WEAPONTYPE_DLC_ASSAULTMG
					ELIF WeaponTypeInt = 1
						CurrentWeapon = WEAPONTYPE_BALL
					ELIF WeaponTypeInt = 2
						CurrentWeapon = WEAPONTYPE_BAT
					ELIF WeaponTypeInt = 3
						CurrentWeapon = WEAPONTYPE_DLC_BOTTLE
					ELIF WeaponTypeInt = 4
						CurrentWeapon = WEAPONTYPE_DLC_BULLPUPRIFLE
					ELIF WeaponTypeInt = 5
						CurrentWeapon = WEAPONTYPE_DLC_COMBATPDW
					ELIF WeaponTypeInt = 6
						CurrentWeapon = WEAPONTYPE_DLC_COMPACTRIFLE
					ELIF WeaponTypeInt = 7
						CurrentWeapon = WEAPONTYPE_CROWBAR
					ELIF WeaponTypeInt = 8
						CurrentWeapon = WEAPONTYPE_DLC_DAGGER
					ELIF WeaponTypeInt = 9
						CurrentWeapon = WEAPONTYPE_DLC_DBSHOTGUN
					ELIF WeaponTypeInt = 10
						CurrentWeapon = WEAPONTYPE_DLC_FIREWORK
					ELIF WeaponTypeInt = 11
						CurrentWeapon = WEAPONTYPE_FLARE
					ELIF WeaponTypeInt = 12
						CurrentWeapon = WEAPONTYPE_DLC_FLAREGUN
					ELIF WeaponTypeInt = 13
						CurrentWeapon = WEAPONTYPE_DLC_FLASHLIGHT
					ELIF WeaponTypeInt = 14
						CurrentWeapon = WEAPONTYPE_GOLFCLUB
					ELIF WeaponTypeInt = 15
						CurrentWeapon = WEAPONTYPE_DLC_REVOLVER
					ELIF WeaponTypeInt = 16
						CurrentWeapon = WEAPONTYPE_DLC_GUSENBERG
					ELIF WeaponTypeInt = 17
						CurrentWeapon = WEAPONTYPE_HAMMER
					ELIF WeaponTypeInt = 18
						CurrentWeapon = WEAPONTYPE_DLC_HATCHET
					ELIF WeaponTypeInt = 19
						CurrentWeapon = WEAPONTYPE_GRENADELAUNCHER
					ELIF WeaponTypeInt = 20
						CurrentWeapon = WEAPONTYPE_MINIGUN
					ELIF WeaponTypeInt = 21
						CurrentWeapon = WEAPONTYPE_DLC_HEAVYRIFLE
					ELIF WeaponTypeInt = 22
						CurrentWeapon = WEAPONTYPE_RPG
					ELIF WeaponTypeInt = 23
						CurrentWeapon = WEAPONTYPE_DLC_HEAVYPISTOL
					ELIF WeaponTypeInt = 24
						CurrentWeapon = WEAPONTYPE_DLC_HEAVYSHOTGUN
					ELIF WeaponTypeInt = 25
						CurrentWeapon = WEAPONTYPE_DLC_HOMINGLAUNCHER
					ELIF WeaponTypeInt = 26
						CurrentWeapon = WEAPONTYPE_PETROLCAN
					ELIF WeaponTypeInt = 27
						CurrentWeapon = WEAPONTYPE_KNIFE
					ELIF WeaponTypeInt = 28
						CurrentWeapon = WEAPONTYPE_DLC_KNUCKLE
					ELIF WeaponTypeInt = 29
						CurrentWeapon = WEAPONTYPE_MG
					ELIF WeaponTypeInt = 30
						CurrentWeapon = WEAPONTYPE_COMBATMG
					ELIF WeaponTypeInt = 31
						CurrentWeapon = WEAPONTYPE_DLC_MACHETE
					ELIF WeaponTypeInt = 32
						CurrentWeapon = WEAPONTYPE_DLC_MACHINEPISTOL
					ELIF WeaponTypeInt = 33
						CurrentWeapon = WEAPONTYPE_DLC_MARKSMANPISTOL
					ELIF WeaponTypeInt = 34
						CurrentWeapon = WEAPONTYPE_DLC_MARKSMANRIFLE
					ELIF WeaponTypeInt = 35
						CurrentWeapon = WEAPONTYPE_MOLOTOV
					ELIF WeaponTypeInt = 36
						CurrentWeapon = WEAPONTYPE_DLC_MUSKET
					ELIF WeaponTypeInt = 37
						CurrentWeapon = WEAPONTYPE_NIGHTSTICK
					ELIF WeaponTypeInt = 38
						CurrentWeapon = WEAPONTYPE_PISTOL
					ELIF WeaponTypeInt = 39
						CurrentWeapon = WEAPONTYPE_DLC_PISTOL50
					ELIF WeaponTypeInt = 40
						CurrentWeapon = WEAPONTYPE_APPISTOL
					ELIF WeaponTypeInt = 41
						CurrentWeapon = WEAPONTYPE_COMBATPISTOL
					ELIF WeaponTypeInt = 42
						CurrentWeapon = WEAPONTYPE_DLC_PROGRAMMABLEAR
					ELIF WeaponTypeInt = 43
						CurrentWeapon = WEAPONTYPE_DLC_PROXMINE
					ELIF WeaponTypeInt = 44
						CurrentWeapon = WEAPONTYPE_DLC_RAILGUN
					ELIF WeaponTypeInt = 45
						CurrentWeapon = WEAPONTYPE_ADVANCEDRIFLE
					ELIF WeaponTypeInt = 46
						CurrentWeapon = WEAPONTYPE_ASSAULTRIFLE
					ELIF WeaponTypeInt = 47
						CurrentWeapon = WEAPONTYPE_CARBINERIFLE
					ELIF WeaponTypeInt = 48
						CurrentWeapon = WEAPONTYPE_ASSAULTSHOTGUN
					ELIF WeaponTypeInt = 49
						CurrentWeapon = WEAPONTYPE_DLC_BULLPUPSHOTGUN
					ELIF WeaponTypeInt = 50
						CurrentWeapon = WEAPONTYPE_PUMPSHOTGUN
					ELIF WeaponTypeInt = 51
						CurrentWeapon = WEAPONTYPE_SAWNOFFSHOTGUN
					ELIF WeaponTypeInt = 52
						CurrentWeapon = WEAPONTYPE_SMG
					ELIF WeaponTypeInt = 53
						CurrentWeapon = WEAPONTYPE_DLC_ASSAULTSMG
					ELIF WeaponTypeInt = 54
						CurrentWeapon = WEAPONTYPE_MICROSMG
					ELIF WeaponTypeInt = 55
						CurrentWeapon = WEAPONTYPE_SNIPERRIFLE
					ELIF WeaponTypeInt = 56
						CurrentWeapon = WEAPONTYPE_DLC_ASSAULTSNIPER
					ELIF WeaponTypeInt = 57
						CurrentWeapon = WEAPONTYPE_HEAVYSNIPER
					ELIF WeaponTypeInt = 58
						CurrentWeapon = WEAPONTYPE_DLC_SNOWBALL
					ELIF WeaponTypeInt = 59
						CurrentWeapon = WEAPONTYPE_DLC_SNSPISTOL
					ELIF WeaponTypeInt = 60
						CurrentWeapon = WEAPONTYPE_DLC_SPECIALCARBINE
					ELIF WeaponTypeInt = 61
						CurrentWeapon = WEAPONTYPE_STUNGUN
					ELIF WeaponTypeInt = 62
						CurrentWeapon = WEAPONTYPE_DLC_SWITCHBLADE
					ELIF WeaponTypeInt = 63
						CurrentWeapon = WEAPONTYPE_BZGAS
					ELIF WeaponTypeInt = 64
						CurrentWeapon = WEAPONTYPE_GRENADE
					ELIF WeaponTypeInt = 65
						CurrentWeapon = WEAPONTYPE_STICKYBOMB
					ELIF WeaponTypeInt = 66
						CurrentWeapon = WEAPONTYPE_UNARMED
					ELIF WeaponTypeInt = 67
						CurrentWeapon = WEAPONTYPE_DLC_VINTAGEPISTOL
					ENDIF
		

					WEAPON_TYPE aWeapon = INT_TO_ENUM(WEAPON_TYPE, CurrentWeapon)
					
					TEXT_LABEL_31 astring = "GREEN TEAM "
					astring += 4
					astring += "/"
					astring += 10
					DRAW_GENERIC_WEAPON_SPRITE_METER(iDamageMeterValue, iDamageMeterDENOM, astring,aWeapon, HUD_COLOUR_GREEN, iFlashTimer, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE,DEFAULT, HUD_COLOUR_GREEN)
					
					
					
				ENDIF
				
				
				
				IF bTurnOnCheckpointBar
//					DRAW_GENERIC_CHECKPOINT(iDamageMeterValue, iDamageMeterDENOM, "HUD_DISTANCE", HUD_COLOUR_PURPLE, -1, iFlashTimer, HUDORDER_DONTCARE, -1, -1, FALSE, HUDFLASHING_NONE, 0, iPlacementValue)
//					DRAW_GENERIC_CHECKPOINT(iDamageMeterValue, iDamageMeterDENOM, "HUD_SPIKES", HUD_COLOUR_WHITE, -1, iFlashTimer, HUDORDER_DONTCARE, -1, -1, FALSE, HUDFLASHING_NONE, 0, iPlacementValue)
					DRAW_GENERIC_CHECKPOINT(iDamageMeterValue, iDamageMeterDENOM, GET_PLAYER_NAME(PLAYER_ID()), HUD_COLOUR_BLUE, -1, iFlashTimer, HUDORDER_DONTCARE, -1, -1, TRUE, HUDFLASHING_NONE, 0, iPlacementValue)
				
				
				ENDIF
				IF bTurnOnEliminationBar
				
					DRAW_GENERIC_ELIMINATION(iDamageMeterDENOM, GET_PLAYER_NAME(PLAYER_ID()), -1, TRUE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, 
											HUD_COLOUR_GREEN, HUD_COLOUR_RED, -1, HUDORDER_TOP, -1, -1, TRUE, HUD_COLOUR_PURE_WHITE,
											HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE,
											HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE,
											HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE,
											HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE, -1, HUDFLASHING_NONE, 0, 
											DEFAULT, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, bUseNonPlayerFont, HUD_COLOUR_RED)
					
					
					
					
					
					
//					DRAW_GENERIC_ELIMINATION(iDamageMeterDENOM, "CNC_CUT_SKIP0", -1, TRUE, FALSE, TRUE, FALSE, FALSE, FALSE,TRUE, FALSE, HUD_COLOUR_GREEN, HUD_COLOUR_RED, iFlashTimer, HUDORDER_DONTCARE, -1, -1, FALSE, 
//											HUD_COLOUR_YELLOW, HUD_COLOUR_YELLOW, HUD_COLOUR_YELLOW, HUD_COLOUR_YELLOW, HUD_COLOUR_YELLOW, HUD_COLOUR_YELLOW, HUD_COLOUR_YELLOW, HUD_COLOUR_YELLOW, 
//											HUD_COLOUR_BLUE, HUD_COLOUR_BLUE , HUD_COLOUR_BLUE, HUD_COLOUR_BLUE, HUD_COLOUR_BLUE, HUD_COLOUR_BLUE, HUD_COLOUR_BLUE, HUD_COLOUR_WHITE, 5000)
				ENDIF
				
				
				
				IF bdisplayPackage
				
					STRING AStringPackage1 = ""
					STRING AStringPackage2 = ""
					
					IF bdisplayPackageStrings1
						AStringPackage1 = "HUD_SPIKES"
					ENDIF
					IF bdisplayPackageStrings2
						AStringPackage2 = "HUD_DISTANCE"
					ENDIF
				
					DRAW_ONE_PACKAGES_EIGHT_HUD(5,AStringPackage1 , false, HUD_COLOUR_BLACK, HUD_COLOUR_BLACK, HUD_COLOUR_RED, HUD_COLOUR_GREEN, 
											HUD_COLOUR_BLACK, HUD_COLOUR_RED, HUD_COLOUR_RED, HUD_COLOUR_RED, -1, HUD_COLOUR_WHITE)
					
					DRAW_TWO_PACKAGES_EIGHT_HUD(6,AStringPackage1,8, AStringPackage2, HUD_COLOUR_RED, HUD_COLOUR_GREEN, HUD_COLOUR_BLUEDARK,
												HUD_COLOUR_BLACK, HUD_COLOUR_BLUEDARK, HUD_COLOUR_BLACK, HUD_COLOUR_BLUEDARK, HUD_COLOUR_GREEN,
												HUD_COLOUR_BLACK, HUD_COLOUR_BLUEDARK, HUD_COLOUR_BLACK, HUD_COLOUR_BLUEDARK, HUD_COLOUR_BLUEDARK, 
												HUD_COLOUR_BLUEDARK, HUD_COLOUR_GREEN, HUD_COLOUR_GREEN, -1, HUD_COLOUR_WHITE,
												HUD_COLOUR_WHITE)
				
				ENDIF
				
				
				
				IF bTurnOnScore
					MPGlobalsScoreHud.bTitleExtraLeftJustified = TRUE
					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_NINETHBOTTOM, FALSE, "HUD_CASH", FALSE, 0, HUDFLASHING_FLASHRED, ScoreFlashingTime)
					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_EIGHTHBOTTOM, FALSE, "HUD_CASH", FALSE, 0, HUDFLASHING_FLASHRED, ScoreFlashingTime)
					DRAW_GENERIC_TIMER(iTimer1Value,"MP_SPINLOADING", iExtraTime, TIMER_STYLE_USEMILLISECONDS , iFlashTimer, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM, FALSE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_BEAST, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_BULLET)
//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_RANDOM)
//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_SLOW_TIME)
//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_SWAP)
//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_TESTOSTERONE)
//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_THERMAL)
//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_WEED)
//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_SPIKES", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", FALSE, 0, DEFAULT, ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_HIDDEN)


//					DRAW_GENERIC_SCORE(iTimer1Value, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM, FALSE, "", FALSE, 0, INT_TO_ENUM(HUDFLASHING, ScoreFlashingType), ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE, ACTIVITY_POWERUP_XP)
//					DRAW_GENERIC_SCORE(iTimer1Value, GET_PLAYER_NAME(PLAYER_ID()), iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM, TRUE, "", FALSE, 0, INT_TO_ENUM(HUDFLASHING, ScoreFlashingType), ScoreFlashingTime, HUD_COLOUR_PURE_WHITE, FALSE, 0 , FALSE)
					
//					g_b_iNumberOfDecimalPlacesForScore = 2
//					MPGlobalsScoreHud.bTitleFarLeftJustified = TRUE
//					REQUEST_ADDITIONAL_TEXT("FMMC", MISSION_TEXT_SLOT)

					
//					DRAW_GENERIC_SCORE(0, "FMMC_LENGTH", iFlashTimer, HUD_COLOUR_WHITE, DEFAULT, FALSE, "FM_ISC_DIST2", TRUE, 0.31, default, default, default, true )
				ENDIF
				
				IF bDrawCrossesCheckpoints 
					DRAW_GENERIC_CHECKPOINT(14, 15, GET_PLAYER_NAME(PLAYER_ID()), HUD_COLOUR_BLUE, -1, iFlashTimer, HUDORDER_DONTCARE, -1, -1, TRUE, HUDFLASHING_NONE, 0, iPlacementValue, TRUE, FALSE, TRUE, TRUE)
				ENDIF 
								
				IF bDrawCrossesElimination
					DRAW_ONE_PACKAGES_EIGHT_HUD(iDamageMeterValue, "HUD_TIME", TRUE, HUD_COLOUR_BLUE, HUD_COLOUR_BLUE, HUD_COLOUR_BLUE, HUD_COLOUR_BLUE, HUD_COLOUR_GREEN, HUD_COLOUR_GREEN,HUD_COLOUR_GREEN, HUD_COLOUR_YELLOW, -1, HUD_COLOUR_WHITE #IF USE_TU_CHANGES, FALSE, TRUE, FALSE, TRUE, FALSE, TRUE, FALSE #ENDIF )
					DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlacementValue, iPlacementDENOM, GET_PLAYER_NAME(PLAYER_ID()), iFlashTimer, HUD_COLOUR_PURPLE, HUDORDER_DONTCARE, TRUE, HUDFLASHING_NONE, 0, FALSE, bUseNonPlayerFont)
					DRAW_GENERIC_TIMER(iTimer2Value, GET_PLAYER_NAME(PLAYER_ID()), iExtraTime, TIMER_STYLE_DONTUSEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_BOTTOM, TRUE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
//					SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
				ENDIF
				
				IF bTurnOnTeamNames
					g_b_ChangePlayerNameToTeamName = TRUE
				ENDIF
				IF bTurnOnDoubleNumber
				
	//				SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	//				SPRITE_PLACEMENT ainstructionalbuttonsprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	//				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR), "HUD_INPUT1B", abuttonStruct)
	//				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(abuttonMovie, ainstructionalbuttonsprite, abuttonStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(abuttonStruct))
					
//					DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlacementValue, iPlacementDENOM, GET_PLAYER_NAME(PLAYER_ID()), iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, TRUE, HUDFLASHING_NONE, 0, TRUE)
					DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlacementValue, iPlacementDENOM, GET_PLAYER_NAME(PLAYER_ID()), iFlashTimer, HUD_COLOUR_PURPLE, HUDORDER_DONTCARE, TRUE, HUDFLASHING_NONE, 0, FALSE, bUseNonPlayerFont)
					DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlacementValue, iPlacementDENOM, "MC_LEVEL4", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0, TRUE)
				
				ENDIF
				
				IF bTurnOnFarLeftJustified
					SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
				ENDIF

				IF bTurnOnMiddleJustified
					SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
				ENDIF
				
				IF bTurnOnBigRacePosition
					DRAW_GENERIC_BIG_RACE_POSITION(1, HUD_COLOUR_WHITE, HUDORDER_TOP)
				ENDIF
				
				IF bTurnOnFourIconBar
					DRAW_GENERIC_FOUR_ICON_BAR(HUD_COLOUR_GREEN, PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, HUDORDER_TOP)
					DRAW_GENERIC_FOUR_ICON_BAR(HUD_COLOUR_GREEN, PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, HUDORDER_TOP, bFourIconBarFlashingIconOne, bFourIconBarFlashingIconTwo, bFourIconBarFlashingIconThree, bFourIconBarFlashingIconFour, iFlashTimer)
				ENDIF
				
				IF bTurnOnFiveIconScoreBar
					DRAW_GENERIC_FIVE_ICON_SCORE_BAR(100, 0.0, "", FALSE, 0, FALSE, HUD_COLOUR_GREENLIGHT, PLAYER_ID(), INVALID_PLAYER_INDEX(), PLAYER_ID(), INVALID_PLAYER_INDEX(), PLAYER_ID(), ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT, ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT, ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT, ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT, ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT, HUDORDER_TOP)
					DRAW_GENERIC_FIVE_ICON_SCORE_BAR(100, 0.0, "", FALSE, 0, FALSE, HUD_COLOUR_BLUELIGHT, PLAYER_ID(), INVALID_PLAYER_INDEX(), PLAYER_ID(), INVALID_PLAYER_INDEX(), PLAYER_ID(), ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO, ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT, HUDORDER_TOP, PLAYER_ID(), TRUE)
					DRAW_GENERIC_FIVE_ICON_SCORE_BAR(100, 0.0, "", FALSE, 0, FALSE, HUD_COLOUR_REDLIGHT, PLAYER_ID(), INVALID_PLAYER_INDEX(), PLAYER_ID(), INVALID_PLAYER_INDEX(), PLAYER_ID(), ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO, ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, ACTIVITY_POWERUP_PED_HEADSHOT, HUDORDER_TOP, PLAYER_ID(), TRUE)
					DRAW_GENERIC_FIVE_ICON_SCORE_BAR(100, 0.0, "", FALSE, 0, FALSE, HUD_COLOUR_SILVER, PLAYER_ID(), INVALID_PLAYER_INDEX(), PLAYER_ID(), INVALID_PLAYER_INDEX(), PLAYER_ID(), ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO, ACTIVITY_POWERUP_PED_HEADSHOT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, ACTIVITY_POWERUP_PED_HEADSHOT, HUDORDER_TOP, PLAYER_ID(), TRUE)
				ENDIF
				
				IF bTurnOnDoubleText
					DRAW_GENERIC_DOUBLE_TEXT("rockbar", "TSA_FIRST", TRUE, FALSE, HUDORDER_FOURTHBOTTOM, HUD_COLOUR_GOLD)
					DRAW_GENERIC_DOUBLE_TEXT("This Gamer Name Is 32 Chars Long", "2nd", TRUE, TRUE, HUDORDER_THIRDBOTTOM, HUD_COLOUR_SILVER)
					
					DRAW_GENERIC_DOUBLE_TEXT("rockbar", "TSA_FIRST", TRUE, FALSE, HUDORDER_SECONDBOTTOM, HUD_COLOUR_GOLD, TRUE)
					DRAW_GENERIC_DOUBLE_TEXT("This Gamer Name Is 32 Chars Long", "2nd", TRUE, TRUE, HUDORDER_BOTTOM, HUD_COLOUR_SILVER, TRUE)
				ENDIF
				
				IF bTurnOnSingleNumber
					DRAW_GENERIC_BIG_NUMBER(88, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", HUD_COLOUR_WHITE, bTurnOnSingleNumberInfinity )
				ENDIF
				
				IF bTurnOnDoubleNumberPlace
					DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(iPlacementValue, iPlacementDENOM, "HUD_DISTANCE",HUD_COLOUR_WHITE, iFlashTimer)
				ENDIF
				
				IF bTurnOnTimerTriple
					SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
					STRING PlayerName="TOP"
					STRING LiteralName = "Tank"
					DRAW_GENERIC_TIMER(iTimer1Value,PlayerName, iExtraTime, TIMER_STYLE_USEMILLISECONDS , iFlashTimer, PODIUMPOS_NONE, HUDORDER_TOP, TRUE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
					DRAW_GENERIC_TIMER(iTimer1Value,"MP_SPINLOADING", iExtraTime, TIMER_STYLE_USEMILLISECONDS , iFlashTimer, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM, FALSE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
					DRAW_GENERIC_TIMER(iTimer1Value,LiteralName, iExtraTime, TIMER_STYLE_USEMILLISECONDS , iFlashTimer, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM, FALSE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0, DEFAULT, DEFAULT, TRUE )
				
				ENDIF
				
				IF bTurnOnTimer
					DRAW_GENERIC_TIMER(iTimer2Value, GET_PLAYER_NAME(PLAYER_ID()), iExtraTime, TIMER_STYLE_DONTUSEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_BOTTOM, TRUE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
				ENDIF
				
				
				IF bTurnonDMLeaderTimer 
					IF bFlipOrderingDMTimer
						DRAW_GENERIC_SCORE(1452, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_BOTTOM, FALSE, "", FALSE, 0, INT_TO_ENUM(HUDFLASHING, ScoreFlashingType), ScoreFlashingTime)
						DRAW_GENERIC_SCORE(1452, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_SECONDBOTTOM, FALSE, "", FALSE, 0, INT_TO_ENUM(HUDFLASHING, ScoreFlashingType), ScoreFlashingTime)
						DRAW_GENERIC_SCORE(1452, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM, FALSE, "", FALSE, 0, INT_TO_ENUM(HUDFLASHING, ScoreFlashingType), ScoreFlashingTime)

						DRAW_GENERIC_BIG_NUMBER(7, "First", -1, HUD_COLOUR_BLUE, HUDORDER_FOURTHBOTTOM, TRUE)
						DRAW_GENERIC_BIG_NUMBER(10, "Second", -1, HUD_COLOUR_BLUE, HUDORDER_FIFTHBOTTOM, TRUE)
					ELSE
					
						DRAW_GENERIC_SCORE(1452, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_BOTTOM, FALSE, "", FALSE, 0, INT_TO_ENUM(HUDFLASHING, ScoreFlashingType), ScoreFlashingTime)
						DRAW_GENERIC_SCORE(1452, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_SECONDBOTTOM, FALSE, "", FALSE, 0, INT_TO_ENUM(HUDFLASHING, ScoreFlashingType), ScoreFlashingTime)
						DRAW_GENERIC_SCORE(1452, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM, FALSE, "", FALSE, 0, INT_TO_ENUM(HUDFLASHING, ScoreFlashingType), ScoreFlashingTime)

					
						DRAW_GENERIC_BIG_NUMBER(7, "First", -1, HUD_COLOUR_BLUE, HUDORDER_FIFTHBOTTOM, TRUE)
						DRAW_GENERIC_BIG_NUMBER(10, "Second", -1, HUD_COLOUR_BLUE, HUDORDER_FOURTHBOTTOM, TRUE)
					ENDIF
				ENDIF
				
				
//				IF bTurnOnBottomRightOverlay
//				
//					
//	//				DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlacementValue, iPlacementDENOM, "TIM_LAP", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0, TRUE)
//
//
//	//				DRAW_KILLSTREAK_DISPLAY(4)
//	////				DRAW_GENERIC_BIG_NUMBER(iPlacementValue, "HUD_DISTANCE", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "", HUD_COLOUR_WHITE, TRUE)
//	//				DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(iPlacementValue, iPlacementDENOM, "HUD_DISTANCE",HUD_COLOUR_WHITE, iFlashTimer, HUDORDER_FIFTHBOTTOM)
//	//				DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlacementValue, iPlacementDENOM, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_FOURTHBOTTOM)
//	//				DRAW_GENERIC_CHECKPOINT(iDamageMeterValue, iDamageMeterDENOM, "HUD_DISTANCE", HUD_COLOUR_GREEN, -1, iFlashTimer, HUDORDER_THIRDBOTTOM)
//	//				DRAW_GENERIC_TIMER(iTimer1Value, "HUD_DISTANCE", iExtraTime, TIMER_STYLE_USEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM)
//	//				DRAW_GENERIC_TIMER(iTimer2Value, "HUD_DISTANCE", iExtraTime, TIMER_STYLE_DONTUSEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_BOTTOM)
//				
//
//					DRAW_GENERIC_METER(iDamageMeterValue, iDamageMeterDENOM, "HUD_DISTANCE", HUD_COLOUR_YELLOW, iFlashTimer, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, ScoreFlashingTime, TRUE)
//				
//					
//					REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
//					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
//					
//						SPRITE_PLACEMENT XPIconBarsSprite
//					
//						XPIconBarsSprite.x = 0.5
//						XPIconBarsSprite.y = 0.5
//						XPIconBarsSprite.w = 1.0
//						XPIconBarsSprite.h = 1.0
//						XPIconBarsSprite.a = 255
//						
//						XPIconBarsSprite.x += XPIconSprite_Widget.x
//						XPIconBarsSprite.y += XPIconSprite_Widget.y
//						XPIconBarsSprite.w += XPIconSprite_Widget.w
//						XPIconBarsSprite.h += XPIconSprite_Widget.h
//						XPIconBarsSprite.r += XPIconSprite_Widget.r
//						XPIconBarsSprite.g += XPIconSprite_Widget.g
//						XPIconBarsSprite.b += XPIconSprite_Widget.b
//						XPIconBarsSprite.a += XPIconSprite_Widget.a
//						
//						DRAW_2D_SPRITE("TimerBars", "Timers_PIXEL_PERFECT_20", XPIconBarsSprite)
//	//				
//					
//	//					SPRITE_PLACEMENT OverlaySprite
//	//					OverlaySprite.x = 0.5
//	//					OverlaySprite.y = 0.5
//	//					OverlaySprite.w = 1.0
//	//					OverlaySprite.h = 1.0
//	//					OverlaySprite.r = 255
//	//					OverlaySprite.g = 255
//	//					OverlaySprite.b = 255
//	//					OverlaySprite.a = OverlayAlpha
//	//				
//	//				
//	//					DRAW_2D_SPRITE("TimerBars", "RaceTimer_DDS_Overlay", OverlaySprite)
//					ENDIF	
//					
//					
//					
//					
//				ENDIF
				
				IF bRunEvent_1STINTSC_2NDINTSC_3RDINTSC_CHECKPOINTS_LOCALINT_TIMER
				
					IF bRunEvent_TurnOnPlayer1
					AND bRunEvent_TurnOnPlayer2
					AND bRunEvent_TurnOnPlayer3
						BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 
																					5, 6, 7, 0, 80, 15, 12000, bRunEvent_ForceRefresh )
						
						
					ELIF bRunEvent_TurnOnPlayer1 = FALSE
					AND bRunEvent_TurnOnPlayer2
					AND bRunEvent_TurnOnPlayer3
					
						BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(INVALID_PLAYER_INDEX(), PLAYER_ID(), PLAYER_ID(), 
																					5, 6, 7, 0, 80, 15, 12000, bRunEvent_ForceRefresh )

					
					ELIF bRunEvent_TurnOnPlayer1 
					AND bRunEvent_TurnOnPlayer2 = FALSE
					AND bRunEvent_TurnOnPlayer3
						BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(PLAYER_ID(),INVALID_PLAYER_INDEX(), PLAYER_ID(), 
																					5, 6, 7, 0, 80, 15, 12000, bRunEvent_ForceRefresh )

					ELIF bRunEvent_TurnOnPlayer1 
					AND bRunEvent_TurnOnPlayer2 
					AND bRunEvent_TurnOnPlayer3 = FALSE
						BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(PLAYER_ID(), PLAYER_ID(), INVALID_PLAYER_INDEX(), 
																					5, 6, 7, 0, 80, 15, 12000, bRunEvent_ForceRefresh )

					
					
					ELIF bRunEvent_TurnOnPlayer1 
					AND bRunEvent_TurnOnPlayer2 = FALSE
					AND bRunEvent_TurnOnPlayer3 = FALSE

						BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(PLAYER_ID(), INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), 
																					5, 6, 7, 0, 80, 15, 12000, bRunEvent_ForceRefresh )
					
					
					ELIF bRunEvent_TurnOnPlayer1 = FALSE
					AND bRunEvent_TurnOnPlayer2 
					AND bRunEvent_TurnOnPlayer3 = FALSE
					
						BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(INVALID_PLAYER_INDEX(), PLAYER_ID(), INVALID_PLAYER_INDEX(), 
																					5, 6, 7, 0, 80, 15, 12000, bRunEvent_ForceRefresh )
					
					
					ELIF bRunEvent_TurnOnPlayer1 = FALSE
					AND bRunEvent_TurnOnPlayer2 = FALSE
					AND bRunEvent_TurnOnPlayer3 
						BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(INVALID_PLAYER_INDEX(), INVALID_PLAYER_INDEX(), PLAYER_ID(), 
																					5, 6, 7, 0, 80, 15, 12000, bRunEvent_ForceRefresh )
					ENDIF
					
					
					
				
				ENDIF
				
				IF bRunEvent_1STINT_2NDINT_3RDINT_ATTEMPT_LOCALINT_TIMER
					BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_ATTEMPT_LOCALINT_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 48, 32, 10, 180, 9, 4555, "", bRunEvent_ForceRefresh)
				ENDIF
				
				IF b_printwindmeter
					DRAW_GENERIC_WIND_METER("HUD_DISTANCE", 700, 10, 255, 0,0,HUDORDER_DONTCARE)
				ENDIF
				
				IF bRunEvent_1STFLOAT_2NDFLOAT_3RDFLOAT_ATTEMPT_LOCALFLOAT_TIMER
					BOTTOM_RIGHT_UI_1STFLOAT_2NDFLOAT_3RDFLOAT_ATTEMPT_LOCALFLOAT_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 202.9, 201.9, 189.9, 0.0, 156.9, 12000, "AMCH_KMHN", bRunEvent_ForceRefresh)
				ENDIF
				
				IF bRunEvent_1STTIME_2NDTIME_3RDTIME_ATTEMPT_LOCALTIME_TIMER
					BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_ATTEMPT_LOCALTIME_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 4800, 3002, 1000, 18000, 900, 455500, bRunEvent_ForceRefresh, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FMEVENT_SCORETITLE_YOUR_BEST )
				
				ENDIF
				
				IF bRunEvent_1STTIME_2NDTIME_3RDTIME_TIMEHELD_TIMER
					BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_TIMEHELD_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 89, 75, 69, 800, 12000, bRunEvent_ForceRefresh)
				ENDIF
				
				IF bRunEvent_1STINT_2NDINT_3RDINT_KILLED_TIMER
					BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_KILLED_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 45, 78, 20, 8, 12000, bRunEvent_ForceRefresh)
				ENDIF
				
				IF bRunEvent_PLAYER_STATE_TIMER
					BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER(GET_PLAYER_NAME(PLAYER_ID()),15,
														GET_PLAYER_NAME(PLAYER_ID()), -2,
														GET_PLAYER_NAME(PLAYER_ID()),15, 
														GET_PLAYER_NAME(PLAYER_ID()),150,
														GET_PLAYER_NAME(PLAYER_ID()), -1, 
														GET_PLAYER_NAME(PLAYER_ID()),15,
														GET_PLAYER_NAME(PLAYER_ID()), 708,
														GET_PLAYER_NAME(PLAYER_ID()),15, 
														DEFAULT, DEFAULT, 
														120000)
				ENDIF
				
				IF bRunEvent_CHECKPOINT_TIMER
					BOTTOM_RIGHT_UI_CHECKPOINT_TIMER(45, 12000)
				ENDIF
				
				IF bRunEvent_BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER
					BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER(12000)
				ENDIF
				
				IF bRunEvent_VEHTEAM_ENEMIES_TIMER
					BOTTOM_RIGHT_UI_VEHTEAM_ENEMIES_TIMER(34, 57, 60, 658888, DEFAULT, TRUE)
				ENDIF
				
				IF bRunEvent_VEHTEAM_COMPETITIVE_ENEMIES_TIMER
					BOTTOM_RIGHT_UI_VEHTEAM_COMPETITIVE_ENEMIES_TIMER( 85,1, 5,2, 2,3, 1,4, 12000, HUD_COLOUR_RED)
				ENDIF
				
				IF bRunEvent_GAMERTAG_COMPETITIVE_ENEMIES_TIMER
					BOTTOM_RIGHT_UI_GAMERTAG_COMPETITIVE_ENEMIES_TIMER("RSN_BRENDA",0,  "RSN_WATSON",1,  "RSN_KEVIN",2, "RSN_NEIL",3, 80, 70, 60, 40, 12000)
				ENDIF
				
				IF bRunEvent_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER		
					BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 89, 75, 69, 800, 12000, bRunEvent_ForceRefresh)
				ENDIF
				
				IF bRunEvent_BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_CASH1_CASH2_TIMER
					BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_CASH1_CASH2_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 5589, iTimer1Value, 69, 800,900, 12000, bRunEvent_ForceRefresh)
				ENDIF
				
				IF bRunEvent_1STINT_2NDINT_3RDINT_LOCALINT_TIMER
					BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_LOCALINT_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(),12, 15, 23, 40,12000, bRunEvent_ForceRefresh )
				ENDIF
				
				IF bDrawHudOverPhone
					SET_PHONE_UNDER_HUD_NO_RISE_THIS_FRAME()
				ENDIF
				
				IF bRunEvent_1TIME_2TIME_3TIME_4TIME_TIMER
					BOTTOM_RIGHT_UI_1TIME_2TIME_3TIME_4TIME_TIMER(PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), 12000, 11000, 10000, 9000, FALSE, TRUE, FALSE, TRUE, 15000,bRunEvent_ForceRefresh )
				ENDIF
				
//				IF bTurnOnBottomRightRaceDisplay
//				
//					DRAW_GENERIC_BIG_DOUBLE_NUMBER_PLACE(iPlacementValue, iPlacementDENOM, "HUD_DISTANCE",HUD_COLOUR_WHITE, iFlashTimer, HUDORDER_FIFTHBOTTOM)
//					DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlacementValue, iPlacementDENOM, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_FOURTHBOTTOM)
//					DRAW_GENERIC_SCORE(iDamageMeterValue, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM)
//					DRAW_GENERIC_TIMER(iTimer1Value, "HUD_DISTANCE", iExtraTime, TIMER_STYLE_USEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM)
//					DRAW_GENERIC_TIMER(iTimer2Value, "HUD_DISTANCE", iExtraTime, TIMER_STYLE_USEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_BOTTOM)
//
//					REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
//					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
//					
//					
//						SPRITE_PLACEMENT OverlaySprite
//						OverlaySprite.x = 0.5
//						OverlaySprite.y = 0.5
//						OverlaySprite.w = 1.0
//						OverlaySprite.h = 1.0
//						OverlaySprite.r = 255
//						OverlaySprite.g = 255
//						OverlaySprite.b = 255
//						OverlaySprite.a = OverlayAlpha
//					
//					
//						DRAW_2D_SPRITE("TimerBars", "969029_TimersAndScoresOverlay", OverlaySprite)
//					ENDIF
//				
//				ENDIF
//				
//				IF bTurnOnBottomRightDMDisplayALL
//				
//					IF bTurnOnBottomRightDMDisplayFourth
//						DRAW_GENERIC_BIG_NUMBER(iPlacementValue, "HUD_DISTANCE",iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_FIFTHBOTTOM)
//					ENDIF
//					IF bTurnOnBottomRightDMDisplayThird
//						DRAW_GENERIC_BIG_NUMBER(iPlacementDENOM, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_FOURTHBOTTOM)
//					ENDIF
//					IF bTurnOnBottomRightDMDisplaySecond
//						DRAW_GENERIC_SCORE(iDamageMeterValue, "HUD_DISTANCE", iFlashTimer, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM)
//					ENDIF
//					IF bTurnOnBottomRightDMDisplayFirst
//						DRAW_GENERIC_TIMER(iTimer2Value, "HUD_DISTANCE", iExtraTime, TIMER_STYLE_DONTUSEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_BOTTOM)
//					ENDIF
//					REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
//					IF HAS_STREAMED_TEXTURE_DICT_LOADED("TimerBars")
//					
//					
//						SPRITE_PLACEMENT OverlaySprite
//						OverlaySprite.x = 0.5
//						OverlaySprite.y = 0.5
//						OverlaySprite.w = 1.0
//						OverlaySprite.h = 1.0
//						OverlaySprite.r = 255
//						OverlaySprite.g = 255
//						OverlaySprite.b = 255
//						OverlaySprite.a = OverlayAlpha
//					
//					
//						DRAW_2D_SPRITE("TimerBars", "1074284 _TimersAndScoresOverlay", OverlaySprite)
//					ENDIF
//				
//				ENDIF
				
				
							
				IF bChangeRankTitlesSource
					IF bRankTitlesUsePersonal
						IF GET_MP_CHARACTER_TITLE_SETTING() != MP_SETTING_TITLE_PERSONAL
							SET_MP_CHARACTER_TITLE_SETTING(MP_SETTING_TITLE_PERSONAL)
						ENDIF
					ELSE
						IF GET_MP_CHARACTER_TITLE_SETTING() != MP_SETTING_TITLE_CREW
							SET_MP_CHARACTER_TITLE_SETTING(MP_SETTING_TITLE_CREW)
						ENDIF
					ENDIF			
				ENDIF
				
				IF bPrintOutAllRankTitles
					TEXT_LABEL_31 tl31_rank 
					INT i
					
					

					FOR i = 0 TO MAX_FM_RANK
						tl31_rank = GET_RANK_TEXTLABEL(i, TEAM_FREEMODE, FALSE)
						NET_NL()NET_PRINT("GET_RANK_TEXTLABEL(")NET_PRINT_INT(i)NET_PRINT(") = ")NET_PRINT(tl31_rank)
					ENDFOR
					bPrintOutAllRankTitles = FALSE
				ENDIF
				
				IF bTurnonRaceBox
					DRAW_RACE_BEST_TIME_BOX(INT_TO_ENUM(BESTRACETIMEBOX ,RaceBoxType), 30000, "RSN GAMERTAG", RaceDetails)
				ENDIF

				IF bDisableIdleKick
					SET_IDLE_KICK_DISABLED_THIS_FRAME()
				ENDIF
				
								
				IF TurnonJoinFailedSCreen 
					SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED, TRUE)
					TurnonJoinFailedSCreen = FALSE
				ENDIF
				
				IF bTurnOnChallengesHud
					IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
					AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
					AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
					AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_TRANSITION_ACTIVE()
					AND NOT IS_PLAYER_IN_CORONA()
					AND NOT NETWORK_IS_ACTIVITY_SESSION()
					AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
					AND IS_SKYSWOOP_AT_GROUND()
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_HOLDUP)
					AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
					AND IS_NET_PLAYER_OK(PLAYER_ID())
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_GANGHIDEOUT)	
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM() 
						DRAW_GENERIC_SCORE(1,GET_PLAYER_NAME(PLAYER_ID()), 0, HUD_COLOUR_RED, HUDORDER_SIXTHBOTTOM, TRUE, DEFAULT,TRUE,1.0, DEFAULT, DEFAULT, HUD_COLOUR_RED)
						DRAW_GENERIC_SCORE(1,GET_PLAYER_NAME(PLAYER_ID()), 0, HUD_COLOUR_RED, HUDORDER_FIFTHBOTTOM, TRUE, DEFAULT,TRUE,1.0, DEFAULT, DEFAULT, HUD_COLOUR_RED)
						DRAW_GENERIC_SCORE(1,GET_PLAYER_NAME(PLAYER_ID()), 0, HUD_COLOUR_RED, HUDORDER_FOURTHBOTTOM, TRUE, DEFAULT,TRUE,1.0, DEFAULT, DEFAULT, HUD_COLOUR_RED)
						DRAW_GENERIC_SCORE(1,GET_PLAYER_NAME(PLAYER_ID()), 0, HUD_COLOUR_BLUE, HUDORDER_THIRDBOTTOM, TRUE, DEFAULT,TRUE,1.0, DEFAULT, DEFAULT, HUD_COLOUR_BLUE)
						DRAW_GENERIC_SCORE(1,"AMCH_CURRENT", 0, HUD_COLOUR_BLUE, HUDORDER_SECONDBOTTOM, FALSE, DEFAULT,TRUE,1.0, DEFAULT, DEFAULT, HUD_COLOUR_BLUE)
						DRAW_GENERIC_TIMER(iTimer2Value, "AMCH_10S", 0, TIMER_STYLE_DONTUSEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
					ENDIF
				ENDIF
				
				IF bTurnOnCheckpointHud
					IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
					AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
					AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
					AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_TRANSITION_ACTIVE()
					AND NOT IS_PLAYER_IN_CORONA()
					AND NOT NETWORK_IS_ACTIVITY_SESSION()
					AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
					AND IS_SKYSWOOP_AT_GROUND()
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_HOLDUP)
					AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
					AND IS_NET_PLAYER_OK(PLAYER_ID())
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_GANGHIDEOUT)	
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM() 
						DRAW_GENERIC_SCORE(1,"CPC_COLLECT", 0, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM, FALSE, DEFAULT,FALSE,0, DEFAULT, DEFAULT, HUD_COLOUR_WHITE)
						DRAW_GENERIC_SCORE(59,"CPC_REMAIN", 0, HUD_COLOUR_WHITE, HUDORDER_SECONDBOTTOM, FALSE, DEFAULT,FALSE,0, DEFAULT, DEFAULT, HUD_COLOUR_WHITE)
						DRAW_GENERIC_TIMER(iTimer2Value, "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
					ENDIF
				ENDIF
				
				IF bTurnOnTimeTrialHud
					IF IS_NET_PLAYER_OK(PLAYER_ID())
					AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						DRAW_GENERIC_TIMER(iTimer2Value, "TIMER_WORLDTIME", 0, TIMER_STYLE_USEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM, FALSE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
						DRAW_GENERIC_TIMER(iTimer2Value, "TIMER_TIME", 0, TIMER_STYLE_USEMILLISECONDS, iFlashTimer, PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, HUD_COLOUR_WHITE, HUDFLASHING_NONE, 0)
						SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
					ENDIF
				ENDIF
				
				IF bTurnOnWarfareHud
					IF IS_NET_PLAYER_OK(PLAYER_ID())
					AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						DRAW_GENERIC_BIG_DOUBLE_NUMBER(1, 9, "GHO_KILLB")
					ENDIF
				ENDIF
				
			ENDIF
			
			// Listens for widget usage
	 		MAINTAIN_GTAO_DEBUG_WIDGETS()
			
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("G_bTurnOnInGameWidgets")
		#ENDIF
		#ENDIF
		

		
		#IF IS_DEBUG_BUILD
			
			IF g_bDeleteAllCharsQuick = TRUE
				
				IF NETWORK_IS_SESSION_ACTIVE()
					g_bDeleteAllCharsQuick = FALSE
				ELSE
			
					IF bHasfreshedIcon = FALSE
						REFRESH_SCALEFORM_LOADING_ICON(DeleteCharStruct)
						bHasfreshedIcon = TRUE
					ENDIF
					DeleteCharStruct.sMainStringSlot = "HUD_DELETING"
					RUN_SCALEFORM_LOADING_ICON(DeleteCharStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(DeleteCharStruct))
				
				
					IF IS_SELECTOR_DISABLED() = FALSE
						bDisabledSelectorForDelete = TRUE
						DISABLE_SELECTOR()
					ENDIF
				ENDIF
			
			ELSE
	//			bHasfreshedIcon = FALSE
	//			
				IF bDisabledSelectorForDelete = TRUE
					SET_LOADING_ICON_INACTIVE()
					ENABLE_SELECTOR()
				ENDIF
				bDisabledSelectorForDelete = FALSE
			ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("g_bDeleteAllCharsQuick")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF NOT bHasLaunchedMpIntroCut
			IF bLaunchMPIntro
				REQUEST_SCRIPT("FM_INTRO_CUT_DEV")
		    	IF (HAS_SCRIPT_LOADED("FM_INTRO_CUT_DEV"))
					g_ShouldShiftingTutorialsBeSkipped = FALSE
				    START_NEW_SCRIPT("FM_INTRO_CUT_DEV", MULTIPLAYER_MISSION_STACK_SIZE)
				    SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_INTRO_CUT_DEV")
					bHasLaunchedMpIntroCut = TRUE
					bLaunchMPIntro = FALSE
				ELSE
		        	NET_PRINT("...Freemode waiting for script to launch: FM_INTRO_CUT_DEV") NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_INTRO_CUT_DEV")) = 0)
				bHasLaunchedMpIntroCut = FALSE
			ENDIF
		ENDIF
		#ENDIF
		
//		IF bHasfreshedIcon = FALSE
//			REFRESH_SCALEFORM_LOADING_ICON(DeleteCharStruct)
//			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(TestbuttonsStruct)
//			bHasfreshedIcon = TRUE
//		ENDIF
		
//
//		SPRITE_PLACEMENT ScaleformSpriteButtons = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
//		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR), "HUD_INPUT1B", TestbuttonsStruct)
//		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(anInstructionalIconMovie, ScaleformSpriteButtons, TestbuttonsStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(TestbuttonsStruct))
	
		
		#IF IS_DEBUG_BUILD
			KICK_LOCAL_PLAYER()
		#ENDIF
		
		
		
		
		
		IF HAS_IMPORTANT_STATS_LOADED()
		
		
			
			
			#IF IS_DEBUG_BUILD
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_H, KEYBOARD_MODIFIER_CTRL, " Debug DELETE characters ") 
				AND (GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY OR GET_CURRENT_GAMEMODE() = GAMEMODE_SP)
					IF g_bDeleteAllCharsQuick = FALSE
						IF G_DELETE_STAGES_QUICK = 0
							SET_HAS_ENTERED_GTAO_THIS_BOOT(TRUE)
							REFRESH_SCALEFORM_LOADING_ICON(DeleteCharStruct)
							PRINTSTRING("/n [PCDELETE] INGAME HUD - Deleting all Characters with Ctrl-H ")
							g_bDeleteAllCharsQuick = TRUE
							
						ENDIF
					ENDIF
				ENDIF
				
				IF g_bDeleteAllCharsQuick = TRUE
			
					DeleteCharStruct.sMainStringSlot = "HUD_DELETING"
					RUN_SCALEFORM_LOADING_ICON(DeleteCharStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(DeleteCharStruct))
				ENDIF
			
				IF NOT bHaveHeistBonusWidgetsInit
					g_DEBUG_Heist_First_Time_Fleeca = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_FLEECA_FIN_INDEX)
					g_DEBUG_Heist_First_Time_Prison = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_PRISON_FIN_INDEX)
					g_DEBUG_Heist_First_Time_Humane = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_HUMANE_FIN_INDEX)
					g_DEBUG_Heist_First_Time_Series = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_SERIESA_FIN_INDEX)
				    g_DEBUG_Heist_First_Time_Pacific = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_PACIFIC_FIN_INDEX)
                                                                           
					g_DEBUG_Heist_Flow_Order_BS	= GET_MP_INT_PLAYER_STAT(MPPLY_HEISTFLOWORDERPROGRESS)
					g_DEBUG_Heist_Same_Team_BS	= GET_MP_INT_PLAYER_STAT(MPPLY_HEISTTEAMPROGRESSBITSET)
					g_DEBUG_Heist_No_Deaths_BS	= GET_MP_INT_PLAYER_STAT(MPPLY_HEISTNODEATHPROGREITSET)
					g_DEBUG_Heist_Member_BS = GET_MP_INT_PLAYER_STAT(MPPLY_HEISTMEMBERPROGRESSBITSET)
					g_DEBUG_Heist_First_Person_BS	= GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_1STPERSON_PROG)
					PRINTLN("[MJM] HEIST CHALLENGE STATS INITIALSED")
					bHaveHeistBonusWidgetsInit = TRUE
				ENDIF
			
				IF CLEAR_ALL_CHARACTER_QUICK(g_bDeleteAllCharsQuick, i_DeleteCharQuickIndex, i_DeleteCharQuickStage, OverwriteData, bShouldCTRLHDeleteBankToo)
					SET_LOADING_ICON_INACTIVE()
					REFRESH_SCALEFORM_LOADING_ICON(DeleteCharStruct)
					ENABLE_SELECTOR()
					SET_MP_BOOL_PLAYER_STAT(MPPLY_CAN_SPECTATE, TRUE)
					g_bDeleteAllCharsQuick = FALSE
					
					NET_NL()NET_PRINT("[PCDELETE] DELETE ALL CHARACTERS QUICK WIDGET: FINISHED")NET_NL()					
				ENDIF
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_H, KEYBOARD_MODIFIER_CTRL, " Debug DELETE characters ") 
			AND (GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY OR GET_CURRENT_GAMEMODE() = GAMEMODE_SP)
				IF g_bDeleteAllCharsQuick = FALSE
					IF G_DELETE_STAGES_QUICK = 0
						SET_HAS_ENTERED_GTAO_THIS_BOOT(TRUE)
						REFRESH_SCALEFORM_LOADING_ICON(DeleteCharStruct)
						PRINTSTRING("/n [PCDELETE]  INGAME HUD - Deleting all Characters with Ctrl-H but load things first. ")
						g_bDeleteAllCharsQuick = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			
				
			IF g_bDeleteAllCharsQuick = TRUE
			
				IF STAT_CLOUD_SLOT_LOAD_FAILED(0)
				OR HAS_ACTIVE_SLOT_STATS_FAILED()
				
//					RUN_OVERWRITE()
					RUN_OVERWRITE_FUNCTION(OverwriteData, bShouldCTRLHDeleteBankToo, DELETE_REASON_CTRL_H_USER_DEBUG_ACTION)
					
					
				
//					SET_LOADING_ICON_INACTIVE()
					NET_NL()NET_PRINT("DELETE ALL CHARACTERS QUICK WIDGET: STAT_CLOUD_SLOT_LOAD_FAILED(0) ")NET_NL()
//					g_bDeleteAllCharsQuick = FALSE
				ENDIF
				
				
			
				DeleteCharStruct.sMainStringSlot = "HUD_DELETING"
				RUN_SCALEFORM_LOADING_ICON(DeleteCharStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(DeleteCharStruct))
				
				IF NOT NETWORK_IS_SIGNED_ONLINE()
					NET_NL()NET_PRINT("DELETE ALL CHARACTERS QUICK WIDGET: NETWORK_IS_SIGNED_ONLINE() = FALSE ")NET_NL()
					g_bDeleteAllCharsQuick = FALSE
					SET_HAS_ENTERED_GTAO_THIS_BOOT(FALSE)
					
					SET_LOADING_ICON_INACTIVE()
				ENDIF	
				
			ENDIF
			#ENDIF
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
			IF bDisplaySavingData
				NET_NL()NET_PRINT("IS_PLAYER_ONLINE = ")NET_PRINT_BOOL(IS_PLAYER_ONLINE())
				NET_NL()NET_PRINT("NETWORK_IS_CLOUD_AVAILABLE = ")NET_PRINT_BOOL(NETWORK_IS_CLOUD_AVAILABLE())
				NET_NL()NET_PRINT("SCRIPT_IS_CLOUD_AVAILABLE = ")NET_PRINT_BOOL(SCRIPT_IS_CLOUD_AVAILABLE())
				NET_NL()NET_PRINT("CAN_LOAD_STATS = ")NET_PRINT_BOOL(CAN_LOAD_STATS())
				NET_NL()NET_PRINT("HAS_IMPORTANT_STATS_LOADED = ")NET_PRINT_BOOL(HAS_IMPORTANT_STATS_LOADED())
				NET_NL()NET_PRINT("HAVE_CODE_STATS_LOADED(0) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(0))
				NET_NL()NET_PRINT("HAVE_CODE_STATS_LOADED(1) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(1))
				NET_NL()NET_PRINT("HAVE_CODE_STATS_LOADED(2) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(2))
				NET_NL()NET_PRINT("HAVE_CODE_STATS_LOADED(3) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(3))
				NET_NL()NET_PRINT("HAVE_CODE_STATS_LOADED(4) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(4))
				NET_NL()NET_PRINT("HAVE_CODE_STATS_LOADED(5) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(5))
				NET_NL()NET_PRINT("STAT_LOAD_PENDING(0) = ")NET_PRINT_BOOL(STAT_LOAD_PENDING(0))
				NET_NL()NET_PRINT("STAT_LOAD_PENDING(1) = ")NET_PRINT_BOOL(STAT_LOAD_PENDING(1))
				NET_NL()NET_PRINT("STAT_LOAD_PENDING(2) = ")NET_PRINT_BOOL(STAT_LOAD_PENDING(2))
				NET_NL()NET_PRINT("STAT_LOAD_PENDING(3) = ")NET_PRINT_BOOL(STAT_LOAD_PENDING(3))
				NET_NL()NET_PRINT("STAT_LOAD_PENDING(4) = ")NET_PRINT_BOOL(STAT_LOAD_PENDING(4))
				NET_NL()NET_PRINT("STAT_LOAD_PENDING(5) = ")NET_PRINT_BOOL(STAT_LOAD_PENDING(5))
				NET_NL()NET_PRINT("STAT_SAVE_PENDING() = ")NET_PRINT_BOOL(STAT_SAVE_PENDING())
//				NET_NL()NET_PRINT("GET_ACTIVE_CHARACTER_SLOT() = ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())
//				NET_NL()NET_PRINT("MPPLY_LAST_MP_CHAR = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR))
				NET_NL()
			ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("g_bHaveAllStatsLoaded")
		#ENDIF
		#ENDIF

		IF JoinMyCrew
			IF NETWORK_CLAN_JOIN(8310)
				JoinMyCrew = FALSE
			ENDIF
		ENDIF

		
		#IF IS_DEBUG_BUILD
			IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			AND NETWORK_IS_GAME_IN_PROGRESS()
				STAT_GET_TIME_AS_DATE(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), BadSportTrackingResetTimer.Timer), BadSportTimeLeft)
			ENDIF

		IF g_bDrawPlaceHolderCutRect
			IF g_bDrawPlaceHolderCutRectSmall
				DRAW_RECT(0.75, 0.75, 0.5, 0.5, 0, 0, 0, 255)
			ELSE
				DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
			ENDIF
		ENDIF
		#ENDIF

		#IF IS_DEBUG_BUILD
		IF bStartOfGameAfterLoadPrint = FALSE
			IF HAS_IMPORTANT_STATS_LOADED()
				NET_NL()NET_PRINT("		- SHOULD_GIVE_SHOP_DISCOUNT = ")NET_PRINT_BOOL(SHOULD_GIVE_SHOP_DISCOUNT())
				NET_NL()NET_PRINT("		- HAS_PLAYER_PLAYED_A_WEEK = ")NET_PRINT_BOOL(HAS_PLAYER_PLAYED_A_WEEK())	
				bStartOfGameAfterLoadPrint = TRUE
				
			ENDIF
		ENDIF
		#ENDIF

	#ENDIF
	
ENDPROC



PROC PROCESS_FINAL()


	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_FINAL")
	#ENDIF
	#ENDIF
	
	IF IS_PLAYER_ONLINE()
		IF SetupCrewData = FALSE
			
			SETUP_CREW_DATA()
			SetupCrewData = TRUE
		ENDIF
	
		IF updateCrewMetaData
			UPDATE_CREW_METADATA()
			updateCrewMetaData = FALSE
		ENDIF
	ENDIF
	
	

		
	IF g_HasLastSPSwitchCheck_Initialised = FALSE
		IF HAVE_CODE_STATS_LOADED(0)
		AND HAVE_CODE_STATS_LOADED(1) OR HAVE_CODE_STATS_LOADED(2)

			NET_NL()NET_PRINT("BC: INIT GAME INITIALISATION")
			
			INT I
			FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
				g_Private_CHARACTER_TEAM[I] = GET_PACKED_STAT_INT(PACKED_CHAR_TEAM, I)
			ENDFOR
			
			// stats are now available, check for landing page character selection
			// set as last MP char in stats if valid
			#IF FEATURE_GEN9_STANDALONE
			INT iLandingPageCharSelection = GET_LANDING_PAGE_SELECTED_CHARACTER_SLOT()
			IF iLandingPageCharSelection = 0
			OR iLandingPageCharSelection = 1
				SET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR, iLandingPageCharSelection)
				NET_NL()NET_PRINT("[PROCESS_FINAL] Set last MP character stat based on landing page selection: ")NET_PRINT_INT(iLandingPageCharSelection)
			ENDIF
			#ENDIF
			
			g_Private_ACTIVE_CHARACTER = GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR)
			
			NET_NL()NET_PRINT("[SET_ACTIVE_CHARACTER_SLOT] g_Private_ACTIVE_CHARACTER = ")NET_PRINT_INT(g_Private_ACTIVE_CHARACTER)

			
			IF DONE_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_RACE)
				NET_NL()NET_PRINT("DONE_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_RACE) = TRUE - calling CLEAR_TUTORIAL_INVITES_ARE_BLOCKED ")
				CLEAR_TUTORIAL_INVITES_ARE_BLOCKED()
			ENDIF
			
			SYNC_BADSPORT_LAST_FRAME_VARIABLES()
			CLEAR_SUPPRESS_INVITE_AT_BOOT()
		
			g_WasCheaterLastSPSwitchCheck = NETWORK_PLAYER_IS_CHEATER()
			g_WasBadSportLastSPSwitchCheck = NETWORK_PLAYER_IS_BADSPORT()
			g_HasLastSPSwitchCheck_Initialised = TRUE
		ENDIF
	ENDIF

	SETUP_CREW_METADATA()
	

	
	RUN_STAT_LOAD_AT_BOOT()
	
	RUN_SYSTEM_SAVING(iStat_Save_System_Stages)
	
	RUN_STAT_LOAD_CHARACTER_SLOT(GET_ACTIVE_CHARACTER_SLOT())
	
	RUN_LOADING_ICON_OFFSET_SHIFT() 
//	RUN_SUBTITLES_OFFSET_SHIFT()
	RUN_OBJECTIVETEXT_OFFSET_SHIFT()
	
	RUN_STREETNAMES_OFFSET_SHIFT()
	
	DISPLAY_EXTERNAL_WARNING_SCREENS()
	
	RUN_EXTERNAL_EMERGENCY_SKYCAM_UP()
	RUN_EXTERNAL_EMERGENCY_SKYCAM_DOWN()
	
	RUN_LOBBY_AND_LOADING_TRACKERS()
	
	RUN_INVITE_LISTENER_FOR_COMPATPACKS()
	
	RUN_SYSTEM_ACTIVITY_SYSTEM(iSystem_Activity_Stages)
	
	RUN_SHOP_DATA_REFRESH(RunRefreshStages, aRefreshTimer, RefreshTotalRetryTime, Refresh_network_shop_transaction_status)

	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		RUN_BIGFOOT_MAINTAIN_FUNCTIONS()
	ENDIF
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	AND NOT IS_TRANSITION_ACTIVE()
		RUN_BAD_SPORT_DEGREDATION(BadSportTrackingResetTimer, BadSportTrackingNumDays, fBadSportIncreaseTickerAmount)	
		RUN_BAD_SPORT_POOL_MESSAGES(BadSportTrackingResetTimer, BadSportTrackingNumDays)
	ELSE
		IF HAS_NET_TIMER_STARTED(BadSportTrackingResetTimer)
			RESET_NET_TIMER(BadSportTrackingResetTimer)
		ENDIF
	ENDIF
	
	// Process any Facebook updates required
	RUN_CHARACTER_FACEBOOK_POST_UPDATES()
	
	
	MAINTAIN_POST_MISSION_CLEANUP_SPLASH_SCREEN()
	
	// Maintain function for supplying player data to the big feed messages about Jobs played etc.
	MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS(	g_TransitionSessionNonResetVars.sDynamicBigFeedData.sBigFeedUnplayedJobPhotoData, 
											g_TransitionSessionNonResetVars.sDynamicBigFeedData.sBigFeedMissionDescVars, 
											g_TransitionSessionNonResetVars.sDynamicBigFeedData.iUnplayedJobArrayIndex			)
	
	// No rank bar on the blurry screen after the job script has finished please.
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
	OR NOT IS_SKYSWOOP_AT_GROUND()
	//OR gdisablerankupmessage = TRUE		// added by Kevin 14/8/2014 BUG 1975745
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
		IF MPGlobalsHud.b_ShowSubtitlesDuringSkyswoop
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
		ENDIF
	ENDIF
	


	
	
	IF MPGlobalsAmbience.bPlayerBlipHighlighted 
		IF NOT IS_TRANSITION_ACTIVE()
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				DISPLAY_ARROW_ON_PLAYER(PLAYER_ID(), HUD_COLOUR_FREEMODE)
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(scLBSignInWarn.signInDelay)
	OR scLBSignInWarn.loadingIcon.bInitialised
		IF scLBSignInWarn.iFrameLastCalled + 5 < GET_FRAME_COUNT()
		AND scLBSignInWarn.iFrameLastCalled > 0
			RESET_NET_TIMER(scLBSignInWarn.signInDelay)
			RESET_NET_TIMER(scLBSignInWarn.signedOutTimer)
			scLBSignInWarn.iFrameLastCalled = 0
			SET_LOADING_ICON_INACTIVE()
		ENDIF
	ENDIF
	
	// AMEC HEISTS

	IF NOT IS_PLAYER_SCTV(PLAYER_ID())

		IF GET_HEIST_JIP_TO_PLANNING_BOARD()
			IF GET_HEIST_PROPERTY_CAM_STAGE() = HEIST_PROPERTY_HOLD_STAGE
				IF GET_HEIST_NONRESET_AUTO_CONFIGURE()
					IF NOT IS_MP_HEIST_PRE_PLANNING_ACTIVE()
						PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - GET_HEIST_JIP_TO_PLANNING_BOARD = TRUE, waiting for heist pre-planning board to load before cleaning up property cam.")
					ELSE
						PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - GET_HEIST_JIP_TO_PLANNING_BOARD = TRUE, IS_MP_HEIST_PRE_PLANNING_GUEST_MODE_ACTIVE = TRUE, moving to cleanup heist property cam.")
						SET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP()
						SET_HEIST_JIP_TO_PLANNING_BOARD(FALSE)
						
						IF TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
							CLEAR_TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP()
		
	//		PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - GET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP = TRUE")
			
			HEIST_PROPERTY_CAM_STAGE aStage = GET_HEIST_PROPERTY_CAM_STAGE()
			
			PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - GET_HEIST_PROPERTY_CAM_STAGE() = ", GET_HEIST_PROPERTY_CAM_STATE_NAME(aStage))
				
			IF aStage = HEIST_PROPERTY_HOLD_STAGE		
			OR aStage = HEIST_PROPERTY_LOAD_INTERIOR
			OR aStage = HEIST_PROPERTY_WAIT_FOR_LOAD
			OR aStage = HEIST_PROPERTY_CLEANUP
			OR aStage = HEIST_PROPERTY_FINISHED
			
				IF aStage = HEIST_PROPERTY_HOLD_STAGE		
					IF NOT GET_HEIST_PROPERTY_CAMERA_TO_MOVE_INTO_INTERIOR()
						PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - GET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP = TRUE but GET_HEIST_PROPERTY_CAMERA_TO_MOVE_INTO_INTERIOR = FALSE, force bcheistcam cleanup.")
						SET_HEIST_PROPERTY_CAMERA_TO_MOVE_INTO_INTERIOR()
						
						IF NOT GET_HEIST_SUPPRESS_CULL_LEADERS_APARTMENT()
							SET_HEIST_SUPPRESS_CULL_LEADERS_APARTMENT(FALSE)
						ENDIF
						
					ENDIF
				ENDIF
				
				BOOL bDontClearCam 
				
				IF IS_TRANSITION_SESSION_LAUNCHING()
				OR (IS_PLAYER_IN_CORONA() AND NOT IS_THIS_PLAYER_LEAVING_THE_CORONA(PLAYER_ID()))
				OR IS_MP_HEIST_PRE_PLANNING_CURRENTLY_IN_USE(TRUE)
					bDontClearCam = TRUE
				ENDIF
			
				PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - Calling: RUN_HEIST_PROPERTY_CAMERA. bDontClearCam: ", bDontClearCam)
				aStage = RUN_HEIST_PROPERTY_CAMERA(bDontClearCam)
				
				IF aStage = HEIST_PROPERTY_FINISHED
					SET_HEIST_SUPPRESS_CULL_LEADERS_APARTMENT(FALSE)
				ELSE
					SET_HEIST_SUPPRESS_CULL_LEADERS_APARTMENT(TRUE)
				ENDIF
				
			ELIF aStage = HEIST_PROPERTY_START 
			OR aStage = HEIST_PROPERTY_FINISHED
			
				PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - CAmera cleanup flag is TRUE, but heistcam is inactive. Cleaning flags.")
				CLEAR_HEIST_PROPERTY_CAMERA_STAT_CLEANUP()
				
			ENDIF
			
		ELSE
			
			IF NOT IS_PLAYER_IN_CORONA()
			
				IF NOT g_HeistSharedClient.bSupressPropertyCamCleanup
			
					HEIST_PROPERTY_CAM_STAGE aStage = GET_HEIST_PROPERTY_CAM_STAGE()
				
					IF aStage = HEIST_PROPERTY_HOLD_STAGE		
						PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - Player is NOT in corona, and heist cam is holding. Forcing into interior...")
						SET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP()
					ENDIF
					
				ENDIF
			
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF g_HeistSharedClient.bAntiCheatResetMissionTracking
		PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - bAntiCheatResetMissionTracking = TRUE, resetting all mission tracking bits.")
		CLEAR_ALL_HEIST_ANTI_CHEAT_BITSETS()
		SET_HEIST_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
		g_HeistSharedClient.bAntiCheatResetMissionTracking = FALSE
	ENDIF
	
	IF g_HeistSharedClient.bAntiCheatResetChanceTracking
		PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - bAntiCheatResetChanceTracking = TRUE, resetting all chance tracking bits.")
		CLEAR_HEIST_FINALE_ANTI_CHEAT_CHANCES()
		SET_HEIST_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
		g_HeistSharedClient.bAntiCheatResetChanceTracking = FALSE
	ENDIF
	
	IF GET_HEIST_STRAND_UPDATE_CHECK_REQUIRED()
		PRINTLN("[AMEC][HEIST_MISC] - INGAME HUD - GET_HEIST_STRAND_UPDATE_CHECK_REQUIRED = TRUE, check if heist strand needs to be cleaned up.")
		FORCE_HEIST_END_OF_MISSION_UPDATE()
		SET_HEIST_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
	ENDIF
	
	
	IF g_HeistSharedClient.bGangopsAntiCheatResetChanceTracking
		PRINTLN("[AMEC][GANGOPS_MISC][GO_ANTI] - INGAME HUD - bGangopsAntiCheatResetChanceTracking = TRUE, resetting all chance tracking bits.")
		CLEAR_GANGOPS_FINALE_ANTI_CHEAT_CHANCES()
		SET_GANGOPS_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
		g_HeistSharedClient.bGangopsAntiCheatResetChanceTracking = FALSE
	ENDIF
	
	IF GET_GANGOPS_STRAND_UPDATE_CHECK_REQUIRED()
		PRINTLN("[AMEC][GANGOPS_MISC][GO_ANTI] - INGAME HUD - GET_GANGOPS_STRAND_UPDATE_CHECK_REQUIRED = TRUE, check if gangops strand needs to be cleaned up.")
		FORCE_GANGOPS_END_OF_MISSION_UPDATE()
		SET_GANGOPS_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
	ENDIF
	
	IF g_HeistSharedClient.bCasinoHeistAntiCheatResetChanceTracking
		PRINTLN("[AMEC][CASINO_HEIST_ANTICHEAT] - INGAME HUD - bCasinoHeistAntiCheatResetChanceTracking = TRUE, resetting all chance tracking bits.")
		CLEAR_CASINO_HEIST_FINALE_ANTI_CHEAT_CHANCES()
		SET_CASINO_HEIST_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
		g_HeistSharedClient.bCasinoHeistAntiCheatResetChanceTracking = FALSE
	ENDIF
	
	IF GET_CASINO_HEIST_STRAND_UPDATE_CHECK_REQUIRED()
		PRINTLN("[AMEC][CASINO_HEIST_ANTICHEAT] - INGAME HUD - GET_CASINO_HEIST_STRAND_UPDATE_CHECK_REQUIRED = TRUE, check if gangops strand needs to be cleaned up.")
		FORCE_CASINO_HEIST_END_OF_MISSION_UPDATE()
		SET_CASINO_HEIST_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
	ENDIF
	
	IF g_HeistSharedClient.bIslandHeistAntiCheatResetChanceTracking
		PRINTLN("[AMEC][ISLAND_HEIST_ANTICHEAT] - INGAME HUD - bIslandHeistAntiCheatResetChanceTracking = TRUE, resetting all chance tracking bits.")
		CLEAR_ISLAND_HEIST_FINALE_ANTI_CHEAT_CHANCES()
		SET_ISLAND_HEIST_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
		g_HeistSharedClient.bIslandHeistAntiCheatResetChanceTracking = FALSE
	ENDIF
	
	IF GET_ISLAND_HEIST_STRAND_UPDATE_CHECK_REQUIRED()
		PRINTLN("[AMEC][ISLAND_HEIST_ANTICHEAT] - INGAME HUD - GET_ISLAND_HEIST_STRAND_UPDATE_CHECK_REQUIRED = TRUE, check if gangops strand needs to be cleaned up.")
		FORCE_ISLAND_HEIST_END_OF_MISSION_UPDATE()
		SET_ISLAND_HEIST_STRAND_UPDATE_CHECK_REQUIRED(FALSE)
	ENDIF
	
	RUN_PED_LEFT_BEHIND_DISPLAY()
	
	RESET_PLAYER_LEFT_OVERHEAD()
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("g_bHaveAllStatsLoaded")
	#ENDIF
	#ENDIF	
	
	

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("g_bDrawPlaceHolderCutRect")
	#ENDIF
	#ENDIF	


#IF IS_DEBUG_BUILD
#IF SCRIPT_PROFILER_ACTIVE
ADD_SCRIPT_PROFILE_MARKER("EXTEND_ALL_HUD_ELEMENTS")
#ENDIF
#ENDIF

#IF IS_DEBUG_BUILD
#IF SCRIPT_PROFILER_ACTIVE
OPEN_SCRIPT_PROFILE_MARKER_GROUP("MPGlobalsScoreHud.isSomethingDisplaying")
#ENDIF
#ENDIF

IF MPGlobalsScoreHud.isSomethingDisplaying

//	IF CAN_INGAME_HUD_ELEMENTS_DISPLAY()


		START_TIMERSHUD_SCRIPT()

//	ENDIF

ENDIF



#IF IS_DEBUG_BUILD
#IF SCRIPT_PROFILER_ACTIVE
CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
ADD_SCRIPT_PROFILE_MARKER("MPGlobalsScoreHud.isSomethingDisplaying")
#ENDIF
#ENDIF


	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF

ENDPROC

PROC RUN_ARE_CHARACTERS_LAST_GEN_CHECK()

	// Don't run these checks if we are in the process of validating a last gen status.
	// See RUN_LAST_GEN_STATUS_VALIDATION() below.
	IF NOT bValidatePlayerLastGenStatus

		BOOL bSlowPrint = FALSE
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 5000 = 0
				bSlowPrint = TRUE
			ENDIF
		#ENDIF

		//Are we still waiting to run checks for either the character or the player last gen status?
		IF GET_CHARACTER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
		OR GET_PLAYER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
		
			#IF USE_FINAL_PRINTS
				//PRINTLN_FINAL("[LASTGEN] Running last gen checks with final logging...")
			#ENDIF

			IF NOT HAS_IMPORTANT_STATS_LOADED()
			AND NOT STAT_LOAD_PENDING(0)
			AND NOT STAT_LOAD_PENDING(1)
			AND NOT STAT_LOAD_PENDING(2)
			
				IF CAN_RUN_START_SAVE_TRANSFER()	

					// Query to get platform data for the 360 account attached to the player's Social Club profile.
					MIGRATION_STATUS migrate
					migrate = STAT_MIGRATE_CHECK_GET_PLATFORM_STATUS(PLATFORM_XBOX360, g_struct_Save_transfer_data_XBOX360)

					// If we failed to get data then we have no hope of 
					// granting player or character last gen status.
					IF migrate = MIGRATION_STATUS_FAILED
						NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK MIGRATION_STATUS_FAILED on Xbox, hold off deciding ")NET_NL()
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK MIGRATION_STATUS_FAILED on Xbox, hold off deciding  ")
						#ENDIF
						IF GET_PLAYER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
							SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_FAILED)
						ENDIF

						IF GET_CHARACTER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
							SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_FAILED)
						ENDIF
						
					//We retrieved data for the 360 account.
					ELSE

						// Has the player migrated an MP charater on last gen on 360? 
						// If so we should grant character last gen status.
						IF migrate = MIGRATION_STATUS_ERROR_ALREADY_DONE
							SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_IS_LAST_GEN)
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF migrate = MIGRATION_STATUS_ERROR_ALREADY_DONE
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = 360 MIGRATION_STATUS_ERROR_ALREADY_DONE  ")NET_NL()
							ENDIF
							IF migrate = MIGRATION_STATUS_AVAILABLE
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = 360 MIGRATION_STATUS_AVAILABLE  ")NET_NL()
							ENDIF
							IF migrate = MIGRATION_STATUS_ERROR_NOT_AVAILABLE
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = 360 MIGRATION_STATUS_ERROR_NOT_AVAILABLE  ")NET_NL()
							ENDIF
							IF migrate = MIGRATION_STATUS_RUNNING
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = 360 MIGRATION_STATUS_RUNNING  ")NET_NL()
							ENDIF
						#ENDIF
						
						#IF USE_FINAL_PRINTS
							IF migrate = MIGRATION_STATUS_ERROR_ALREADY_DONE
								PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = 360 MIGRATION_STATUS_ERROR_ALREADY_DONE")
							ENDIF
							IF migrate = MIGRATION_STATUS_AVAILABLE
								PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = 360 MIGRATION_STATUS_AVAILABLE")
							ENDIF
							IF migrate = MIGRATION_STATUS_ERROR_NOT_AVAILABLE
								PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = 360 MIGRATION_STATUS_ERROR_NOT_AVAILABLE")
							ENDIF
							IF migrate = MIGRATION_STATUS_RUNNING
								PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = 360 MIGRATION_STATUS_RUNNING")
							ENDIF
						#ENDIF

						// Has the player played singleplayer last gen or do they have a GTA:O char on 360? 
						// If so we should try and unlock 'last gen' status for their profile
						IF GET_PLAYER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
							IF g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp > 0
							OR g_struct_Save_transfer_data_XBOX360.m_numberOfChars > 0
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp =  ")NET_PRINT_FLOAT(g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp)NET_NL()
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Account has played an MP character on last gen 360, attempting to run validation.")NET_NL()
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp =  ", g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp)
									PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Account has played an MP character on last gen 360, attempting to run validation.")								
								#ENDIF
								bValidatePlayerLastGenStatus = TRUE
								EXIT
							ENDIF
						ENDIF
					ENDIF

					// Query to get platform data for the PS3 account attached to the player's Social Club profile.
					migrate = STAT_MIGRATE_CHECK_GET_PLATFORM_STATUS(PLATFORM_PS3, g_struct_Save_transfer_data_PS3)

					// If we failed to get data then we have no hope of 
					// granting player or character last gen status.
					IF migrate = MIGRATION_STATUS_FAILED
						NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK MIGRATION_STATUS_FAILED on PS3, hold off deciding ")NET_NL()
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK MIGRATION_STATUS_FAILED on PS3, hold off deciding  ")
						#ENDIF
						IF GET_PLAYER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
							SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_FAILED)
						ENDIF
						IF GET_CHARACTER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
							SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_FAILED)
						ENDIF
						
					//We retrieved data for the PS3 account.	
					ELSE

						// Has the player migrated an MP charater on last gen on PS3? 
						// If so we should grant character last gen status.
						IF migrate = MIGRATION_STATUS_ERROR_ALREADY_DONE
							SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_IS_LAST_GEN)
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF migrate = MIGRATION_STATUS_ERROR_ALREADY_DONE
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = PS3 MIGRATION_STATUS_ERROR_ALREADY_DONE  ")NET_NL()
							ENDIF
							IF migrate = MIGRATION_STATUS_AVAILABLE
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = PS3 MIGRATION_STATUS_AVAILABLE  ")NET_NL()
							ENDIF
							IF migrate = MIGRATION_STATUS_ERROR_NOT_AVAILABLE
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = PS3 MIGRATION_STATUS_ERROR_NOT_AVAILABLE  ")NET_NL()
							ENDIF
							IF migrate = MIGRATION_STATUS_RUNNING
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = PS3 MIGRATION_STATUS_RUNNING  ")NET_NL()
							ENDIF
						#ENDIF
						
						#IF USE_FINAL_PRINTS
							IF migrate = MIGRATION_STATUS_ERROR_ALREADY_DONE
								PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = PS3 MIGRATION_STATUS_ERROR_ALREADY_DONE")
							ENDIF
							IF migrate = MIGRATION_STATUS_AVAILABLE
								PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = PS3 MIGRATION_STATUS_AVAILABLE")
							ENDIF
							IF migrate = MIGRATION_STATUS_ERROR_NOT_AVAILABLE
								PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = PS3 MIGRATION_STATUS_ERROR_NOT_AVAILABLE")
							ENDIF
							IF migrate = MIGRATION_STATUS_RUNNING
								PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK migrate = PS3 MIGRATION_STATUS_RUNNING")
							ENDIF
						#ENDIF

						// Has the player played singleplayer last gen or do they have a GTA:O char on PS3? 
						// If so we should try and unlock 'last gen' status for their profile.
						IF GET_PLAYER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
							IF g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp > 0
							OR g_struct_Save_transfer_data_PS3.m_numberOfChars > 0
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp =  ")NET_PRINT_FLOAT(g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp)NET_NL()
								NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Account has played SP last gen PS3, attempting to run validation.")NET_NL()
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp =  ", g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp)
									PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Account has played SP last gen PS3, attempting to run validation.")								
								#ENDIF
								bValidatePlayerLastGenStatus = TRUE
								EXIT
							ENDIF
						ENDIF
					ENDIF

					//If we've passed through all these checks without setting a failed or "is last gen" status
					//then it must mean this player is not a last gen player. Set this state and exit.
					IF GET_PLAYER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
						SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_IS_NOT_LAST_GEN)
						NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK ** Setting Player hasn't played last gen. ")NET_NL()
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK ** Setting Player hasn't played last gen.  ")
						#ENDIF
					ENDIF
					IF GET_CHARACTER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE
						SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_IS_NOT_LAST_GEN)
						NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK ** Setting Characters as not from last gen. ")NET_NL()
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK ** Setting Characters as not from last gen.  ")
						#ENDIF
					ENDIF
					EXIT
				ENDIF
			ELSE
				IF bSlowPrint
					IF STAT_LOAD_PENDING(0)
						NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Trying to check, but the STAT_LOAD_PENDING(0) are pending. ")NET_NL()
					
					ELIF STAT_LOAD_PENDING(1)
						NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Trying to check, but the STAT_LOAD_PENDING(1) are pending. ")NET_NL()
					ELIF STAT_LOAD_PENDING(2)
						NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Trying to check, but the STAT_LOAD_PENDING(2) are pending. ")NET_NL()
					ELSE
						NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Trying to check, but the stats are loaded ")NET_NL()
					ENDIF
					#IF USE_FINAL_PRINTS
						IF STAT_LOAD_PENDING(0)
							PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Trying to check, but the STAT_LOAD_PENDING(0) are pending. ")
						ELIF STAT_LOAD_PENDING(1)
							PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Trying to check, but the STAT_LOAD_PENDING(1) are pending. ")
						ELIF STAT_LOAD_PENDING(2)
							PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Trying to check, but the STAT_LOAD_PENDING(2) are pending. ")
						ELSE
							PRINTLN_FINAL("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Trying to check, but the stats are loaded ")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF

		// Check for refreshing the attempt to get a character last gen status.
		// Regulate how often this is allowed to happen carefully.
		ELIF GET_CHARACTER_LAST_GEN_CHECK() = LAST_GEN_STATUS_FAILED

//			IF g_i_Private_LastGenCharacterAttempt < 0
//				STAT_MIGRATE_CHECK_START()
//				g_i_Private_LastGenCharacterAttempt++
//				NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Retry Attempt ")NET_PRINT_INT(g_i_Private_LastGenCharacterAttempt)
//
//				IF GET_CHARACTER_LAST_GEN_CHECK() = LAST_GEN_STATUS_FAILED
//					SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_NONE)
//				ENDIF
//
//			ELSE
//		  		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_st_Private_LastGenCharacterRetry, g_sMPTunables.ilastgencharacter)
//					
//					NET_NL()NET_PRINT("[LASTGEN] RUN_ARE_CHARACTERS_LAST_GEN_CHECK Retry getting status ")
//					g_i_Private_LastGenCharacterAttempt = 0
//
//					IF GET_CHARACTER_LAST_GEN_CHECK() = LAST_GEN_STATUS_FAILED
//						SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_NONE)
//					ENDIF
//
//				ENDIF
//			ENDIF

		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE: 	Attempt to verify that this console profile is allowed to use this last gen social club binding.
///    			If another profile from the same console type has already bound to this social club profile this
///    			will fail. If no profile or the same profile has bound to the social club account this will pass
///    			and last gen status will be awarded.
///    
///    			NB. This check should only concern itself with the PLAYER_LAST_GEN status. Not CHARACTER_LAST_GEN.
PROC RUN_LAST_GEN_STATUS_VALIDATION()

	//Listen for validation requests.
	IF bValidatePlayerLastGenStatus 
	
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[LASTGEN] Running last status validation with final logging...")
		#ENDIF

		//Check if this whole process needs to be skipped due to command line arg.
		#IF IS_DEBUG_BUILD 
			IF GET_COMMANDLINE_PARAM_EXISTS("SkipSocialClubLastGenAccountBinding")
				CWARNINGLN(DEBUG_LASTGEN, "<VALIDATE> Consume automatically passed due to command line \"-SkipSocialClubLastGenAccountBinding\"...")

				SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_IS_LAST_GEN)			
				bValidationInProgress = FALSE
				bValidatePlayerLastGenStatus = FALSE
			ELSE
		#ENDIF
		
		//We're online. We can try and validate with the servers.
		IF NETWORK_IS_SIGNED_ONLINE()
		AND NETWORK_IS_SIGNED_IN()
		AND ARE_PROFILE_SETTINGS_VALID()

			//Only run this check if we haven't already this session.
			IF GET_PLAYER_LAST_GEN_CHECK() = LAST_GEN_STATUS_NONE

				//Are we already considered last gen? If so there is no need to validate.
				IF NOT IS_LAST_GEN_PLAYER()

					//Validation needs starting.
					IF NOT bValidationInProgress

						//Do we have valid PS3 data to work with?
						IF (g_struct_Save_transfer_data_PS3.m_totalProgressMadeInSp > 0 OR g_struct_Save_transfer_data_PS3.m_numberOfChars > 0)
						AND NOT IS_STRING_NULL_OR_EMPTY(g_struct_Save_transfer_data_PS3.m_gamerHandle)
						
							//Attempt to start the last-gen consume from the PS3 data.
							CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Consume data... ContentID:", HASH("played_last_gen"), " PlatformID:\"ps3\" GamerHandle:\"", g_struct_Save_transfer_data_PS3.m_gamerHandle, "\"")
							
							#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[LASTGEN][VALIDATE] Consume data... ContentID:", HASH("played_last_gen"), " PlatformID:\"ps3\" GamerHandle:\"", g_struct_Save_transfer_data_PS3.m_gamerHandle, "\"")
							#ENDIF
							
							IF STAT_SAVE_MIGRATION_CONSUME_CONTENT( HASH("played_last_gen"), "ps3", g_struct_Save_transfer_data_PS3.m_gamerHandle )
								bValidationInProgress = TRUE
							ELSE
								CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> The consume command failed to run correctly. Flagging the last gen status update as failed.")
								
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][VALIDATE] The consume command failed to run correctly. Flagging the last gen status update as failed.")
								#ENDIF
								
								SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_FAILED)
							ENDIF
							
						//Do we have valid 360 data to work with?
						ELIF (g_struct_Save_transfer_data_XBOX360.m_totalProgressMadeInSp > 0 OR g_struct_Save_transfer_data_XBOX360.m_numberOfChars > 0)
						AND NOT IS_STRING_NULL_OR_EMPTY(g_struct_Save_transfer_data_XBOX360.m_gamerHandle)

							//Attempt to start the last-gen consume from the 360 data.
							CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Consume data... ContentID:", HASH("played_last_gen"), " PlatformID:\"xbox360\" GamerHandle:\"", g_struct_Save_transfer_data_XBOX360.m_gamerHandle, "\"")
							IF STAT_SAVE_MIGRATION_CONSUME_CONTENT( HASH("played_last_gen"), "xbox360", g_struct_Save_transfer_data_XBOX360.m_gamerHandle )
								bValidationInProgress = TRUE
							ELSE
								CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> The consume command failed to run correctly. Flagging the last gen status update as failed.")
								
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][VALIDATE] The consume command failed to run correctly. Flagging the last gen status update as failed.")
								#ENDIF
								
								SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_FAILED)
							ENDIF
						
						//Player has no social club data to query with.
						ELSE
							CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Last gen social club data not available. Validation can not be attempted. Falling back to cached last gen status.")
							
							#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[LASTGEN][VALIDATE] Last gen social club data not available. Validation can not be attempted. Falling back to cached last gen status.")
							#ENDIF
							
							bValidatePlayerLastGenStatus = FALSE
						ENDIF
						
					//Validation is in progress.
					ELSE 
					
						COMSUME_CONTENT_ERROR_CODE consumeError
						INT iCurrentLastGenContentProfileSettings
						
						SWITCH STAT_GET_SAVE_MIGRATION_CONSUME_CONTENT_STATUS(consumeError)
						
							CASE NET_STATUS_NONE
					        CASE NET_STATUS_PENDING
								CDEBUG3LN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Waiting for consume task to process.")
								
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][VALIDATE] Waiting for consume task to process.")
								#ENDIF
							BREAK

							CASE NET_STATUS_CANCELED
								CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> The consume command was cancelled! Flagging the last gen status update as failed.")
								
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][VALIDATE] The consume command was cancelled! Flagging the last gen status update as failed.")
								#ENDIF
								
								SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_FAILED)
								bValidationInProgress = FALSE
								bValidatePlayerLastGenStatus = FALSE
							BREAK

					        CASE NET_STATUS_FAILED
								CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Consume command failed!")
								
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][VALIDATE] Consume command failed!")
								#ENDIF

								SWITCH consumeError
									CASE ERROR_UNKNOWN				
										CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Fail reason: unknown. Flagging the last gen status update as failed.")
										#IF USE_FINAL_PRINTS
											PRINTLN_FINAL("[LASTGEN][VALIDATE] Fail reason: unknown. Flagging the last gen status update as failed.")
										#ENDIF
									BREAK

									CASE ERROR_NONE
										CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Fail reason: None. Flagging the last gen status update as failed.")
										#IF USE_FINAL_PRINTS
											PRINTLN_FINAL("[LASTGEN][VALIDATE] Fail reason: None. Flagging the last gen status update as failed.")
										#ENDIF
									BREAK

									CASE ERROR_UNLOCK_ALREADY_USED	
										CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Fail reason: Unlock already used! Flagging the last gen status update as failed.")
										#IF USE_FINAL_PRINTS
											PRINTLN_FINAL("[LASTGEN][VALIDATE] Fail reason: Unlock already used! Flagging the last gen status update as failed.")
										#ENDIF
									BREAK

									DEFAULT
										CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Fail reason: Default ERROR UNKNOWN! Flagging the last gen status update as failed.")
					              		#IF USE_FINAL_PRINTS
											PRINTLN_FINAL("[LASTGEN][VALIDATE] Fail reason: Default ERROR UNKNOWN! Flagging the last gen status update as failed.")
										#ENDIF
									BREAK
								ENDSWITCH

								SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_FAILED) //Must be set regardless of fail reason.
								bValidationInProgress  = FALSE
								bValidatePlayerLastGenStatus = FALSE
							BREAK

					        CASE NET_STATUS_SUCCEEDED
								CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Consume succeeded! Flagging the player as last gen.")
								
								#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[LASTGEN][VALIDATE] Consume succeeded! Flagging the player as last gen.")
								#ENDIF
								
								//Dont do the check again this session unless we reboot disregarding the result.							
								SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_IS_LAST_GEN)
				
								//Store this result into a profile setting so that it is locked in permanently,
								//even if the player loses social club connectivity. This is safe as the result
								//has been security checked. This safeguards against players losing CGtoNG content
								//whenever SC is unavailable which would be unsafe.
								IF ARE_PROFILE_SETTINGS_VALID()
									//Read the current last gen content settings we have.
									iCurrentLastGenContentProfileSettings = GET_PROFILE_SETTING(GAMER_HAS_SPECIALEDITION_CONTENT)

									//Update the "IS_LAST_GEN" bit only.
									SET_BIT(iCurrentLastGenContentProfileSettings, BIT_IS_LAST_GEN)
									
									//Push this back to the player's profile.
									SET_HAS_SPECIALEDITION_CONTENT(iCurrentLastGenContentProfileSettings)
									CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Last gen status sucessfully saved out to profile settings.")
								
									#IF USE_FINAL_PRINTS
										PRINTLN_FINAL("[LASTGEN][VALIDATE] Last gen status sucessfully saved out to profile settings.")
									#ENDIF
								ENDIF

								bValidationInProgress  = FALSE
								bValidatePlayerLastGenStatus = FALSE
							BREAK
						ENDSWITCH
					ENDIF

				//Player already has last gen granted.
				ELSE
					CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Player already has last gen granted.")
					
					#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[LASTGEN][VALIDATE] Player already has last gen granted.")
					#ENDIF
					
					SET_PLAYER_LAST_GEN_CHECK(LAST_GEN_STATUS_IS_LAST_GEN)
					bValidationInProgress = FALSE
					bValidatePlayerLastGenStatus = FALSE
				ENDIF

			//Player already has tried to consume last gen.
			ELSE
				CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Player already has tried to consume last gen.")
				
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[LASTGEN][VALIDATE] Player already has tried to consume last gen.")
				#ENDIF
				
				bValidationInProgress  = FALSE
				bValidatePlayerLastGenStatus = FALSE
			ENDIF

		//Player is offline. We'll have to run with cached results for now.
		ELSE
			CPRINTLN(DEBUG_LASTGEN, "[LASTGEN] <VALIDATE> Player is offline. Validation can not be attempted. Falling back to cached last gen status.")
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[LASTGEN][VALIDATE] Player is offline. Validation can not be attempted. Falling back to cached last gen status.")
			#ENDIF
			
			bValidationInProgress  = FALSE
			bValidatePlayerLastGenStatus = FALSE
		ENDIF

		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF

	ENDIF
ENDPROC


PROC RUN_BIKER_APP_SAVE_COOLDOWN()
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		IF DOES_PLAYER_OWN_A_BUSINESS(PLAYER_ID(), TRUE)
			IF g_b_BikeAppStatChange
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(BikerAppCooldown, g_sMPTunables.iBIKER_Minor_SAVE_Cooldown) //5mins
					PRINTLN("RUN_BIKER_APP_SAVE_COOLDOWN - Calling REQUEST_SAVE minor - cooldown = ", g_sMPTunables.iBIKER_Minor_SAVE_Cooldown)
					REQUEST_SAVE(SSR_REASON_BIKER_BUSINESS, STAT_SAVETYPE_AMBIENT)
					g_b_BikeAppStatChange = FALSE
					g_b_BikeAppStatChangeMajor = FALSE
					REINIT_NET_TIMER(BikerAppCooldown)
				ENDIF
			ENDIF
			
			IF g_b_BikeAppStatChangeMajor
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(BikerAppCooldown, g_sMPTunables.iBIKER_Major_SAVE_Cooldown) //50secs
					PRINTLN("[SAVECOOLDOWN] RUN_BIKER_APP_SAVE_COOLDOWN - Calling REQUEST_SAVE Major - cooldown = ", g_sMPTunables.iBIKER_Major_SAVE_Cooldown)
					IF NETWORK_IS_ACTIVITY_SESSION()
						STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
					ENDIF
					REQUEST_SAVE(SSR_REASON_BIKER_BUSINESS, STAT_SAVETYPE_END_SHOPPING)
					g_b_BikeAppStatChangeMajor = FALSE
					g_b_BikeAppStatChange = FALSE
					REINIT_NET_TIMER(BikerAppCooldown)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC RUN_CASINO_SAVE_COOLDOWN()
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	AND NOT g_b_DisableCasinoSaveManagement
		IF g_b_CasinoSave
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(CasinoSaveCooldown, g_sMPTunables.iCASINO_SAVE_Cooldown) //5 minutes
				PRINTLN("RUN_CASINO_SAVE_COOLDOWN - Calling REQUEST_SAVE Time played - cooldown = ", g_sMPTunables.iCASINO_SAVE_Cooldown)
				IF NETWORK_IS_ACTIVITY_SESSION()
					STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
				ENDIF
				REQUEST_SAVE(SSR_REASON_CASINO, STAT_SAVETYPE_END_SHOPPING)
				g_b_CasinoPrioritySave 	= FALSE
				g_b_CasinoSave 			= FALSE
				REINIT_NET_TIMER(CasinoSaveCooldown)
			ENDIF
		ENDIF
		
		IF g_b_CasinoPrioritySave
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(CasinoSaveCooldown, g_sMPTunables.iCASINO_PRIORITY_SAVE_Cooldown) //1 minute
				PRINTLN("[SAVECOOLDOWN] RUN_CASINO_SAVE_COOLDOWN - Calling REQUEST_SAVE Major - Chips = ", g_sMPTunables.iCASINO_PRIORITY_SAVE_Cooldown)
				IF NETWORK_IS_ACTIVITY_SESSION()
					STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
				ENDIF
				REQUEST_SAVE(SSR_REASON_CASINO, STAT_SAVETYPE_END_SHOPPING)
				g_b_CasinoPrioritySave 	= FALSE
				g_b_CasinoSave 			= FALSE
				REINIT_NET_TIMER(CasinoSaveCooldown)
			ENDIF
		ENDIF
	ENDIF

ENDPROC


SCRIPT


	
	// This script needs to cleanup only when the game runs the magdemo
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_MAGDEMO))
		PRINTSTRING("...event_controller.sc has been forced to cleanup (MAGDEMO)")
		PRINTNL()
		
		TERMINATE_THIS_THREAD()
	ENDIF
	
	
	
	#IF IS_DEBUG_BUILD
		IF NETWORK_IS_SIGNED_IN()
			INT I
			NET_NL()NET_PRINT("STARTING INGAMEHUD with ")
			NET_NL()NET_PRINT(" <<< GET_IS_LAUNCH_FROM_LIVE_AREA = ")NET_PRINT_BOOL(GET_IS_LAUNCH_FROM_LIVE_AREA())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT = ")NET_PRINT_BOOL(GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< LOBBY_AUTO_MULTIPLAYER_FREEMODE = ")NET_PRINT_BOOL(LOBBY_AUTO_MULTIPLAYER_FREEMODE())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< NETWORK_IS_CLOUD_AVAILABLE = ")NET_PRINT_BOOL(NETWORK_IS_CLOUD_AVAILABLE())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< NETWORK_HAVE_USER_CONTENT_PRIVILEGES(PC_EVERYONE) = ")NET_PRINT_BOOL(NETWORK_HAVE_USER_CONTENT_PRIVILEGES(PC_EVERYONE))NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< NETWORK_HAVE_USER_CONTENT_PRIVILEGES(PC_FRIENDS) = ")NET_PRINT_BOOL(NETWORK_HAVE_USER_CONTENT_PRIVILEGES(PC_FRIENDS))NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< NETWORK_HAVE_ONLINE_PRIVILEGES = ")NET_PRINT_BOOL(NETWORK_HAVE_ONLINE_PRIVILEGES())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< NETWORK_HAVE_COMMUNICATION_PRIVILEGES(PC_EVERYONE) = ")NET_PRINT_BOOL(NETWORK_HAVE_COMMUNICATION_PRIVILEGES(PC_EVERYONE))NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< NETWORK_HAVE_COMMUNICATION_PRIVILEGES(PC_FRIENDS) = ")NET_PRINT_BOOL(NETWORK_HAVE_COMMUNICATION_PRIVILEGES(PC_FRIENDS))NET_PRINT(" >>>")
			FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
				NET_NL()NET_PRINT(" <<< HAS_SLOT_BEEN_IGNORED_SCRIPT(")NET_PRINT_INT(I)NET_PRINT(") = ")NET_PRINT_BOOL(HAS_SLOT_BEEN_IGNORED_SCRIPT(I))NET_PRINT(" >>>")
			ENDFOR
			NET_NL()NET_PRINT(" <<< DOES_PLAYER_HAVE_PRIVILEGES() = ")NET_PRINT_BOOL(DOES_PLAYER_HAVE_PRIVILEGES())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< PRIVALEGES_OKAY_FOR_MP() = ")NET_PRINT_BOOL(PRIVALEGES_OKAY_FOR_MP())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< PRIVALEGES_OKAY_FOR_MP() = ")NET_PRINT_BOOL(PRIVALEGES_OKAY_FOR_MP())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< IS_ACCOUNT_BLOCKED = ")NET_PRINT_BOOL(IS_ACCOUNT_BLOCKED())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< IS_ACCOUNT_OVER_17 = ")NET_PRINT_BOOL(IS_ACCOUNT_OVER_17())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< IS_ACCOUNT_OVER_17_FOR_UGC = ")NET_PRINT_BOOL(IS_ACCOUNT_OVER_17_FOR_UGC())NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID()) = ")NET_PRINT_BOOL(IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID()))NET_PRINT(" >>>")
			NET_NL()NET_PRINT(" <<< PERMISSION LEVEL = ")NET_PRINT(GET_PAUSE_MENU_PERMISSION_ERROR_MESSAGE(GET_ACCOUNT_PERMISSION_SETUP()))
			IF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
				NET_NL()NET_PRINT(" <<< NETWORK_GET_NP_UNAVAILABLE_REASON = ")NET_PRINT(GET_UNAVAILABILITY_REASON_STRING(NETWORK_GET_NP_UNAVAILABLE_REASON()))
			ENDIF
		ENDIF

	#ENDIF

//	BREAK_ON_NATIVE_COMMAND("TOGGLE_PAUSED_RENDERPHASES", FALSE)	
//	TRACE_NATIVE_COMMAND("SHOW_HUD_COMPONENT_THIS_FRAME")	
		

	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
//	INIT_STATS()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	CREATE_SCRIPT_PROFILER_WIDGET()
	#ENDIF
	#ENDIF

	SET_PLAYER_HAVE_PRIVILEGES(HAS_ONLINE_PRIVILAGES())


	SET_IGNORE_CHEATER_RATING(SC_GAMERDATA_GET_BOOL("bIgnoreCheaterOverride"))
	SET_IGNORE_BAD_SPORT_RATING(SC_GAMERDATA_GET_BOOL("bIgnoreBadSportOverride"))
	
	EventHudPlayerIndexLastFrame[0] = INVALID_PLAYER_INDEX()
	EventHudPlayerIndexLastFrame[1] = INVALID_PLAYER_INDEX()
	EventHudPlayerIndexLastFrame[2] = INVALID_PLAYER_INDEX()
	EventHudPlayerIndexLastFrame[3] = INVALID_PLAYER_INDEX()
	EventHudPlayerIndexLastFrame[4] = INVALID_PLAYER_INDEX()
	
	
	
//	#IF IS_DEBUG_BUILD
//	CLEAR_DEBUG_FILE_REWARD_OUTPUT()
//	#ENDIF
	
	// The main mission loop 
	WHILE TRUE 	
	
//		
		
		IF g_b_IsRockstarIDValidLastFrame_Init = FALSE
			IF NETWORK_HAS_VALID_ROS_CREDENTIALS()
				g_b_IsRockstarIDValidLastFrame = NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
				g_b_IsRockstarIDValidLastFrame_Init = TRUE
				NET_NL()NET_PRINT("[LASTGEN] g_b_IsRockstarIDValidLastFrame init = ")NET_PRINT_BOOL(g_b_IsRockstarIDValidLastFrame)
			ENDIF
		ENDIF

		RUN_ARE_CHARACTERS_LAST_GEN_CHECK()
		RUN_LAST_GEN_STATUS_VALIDATION()
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
		//Need this running all the time for the singleplayer hud displays. Added by BC 22/08/11
		PROCESS_HUD_DISPLAYS_DEBUG()
		
		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_HUD_DISPLAYS_DEBUG")
		#ENDIF
		#ENDIF
		
		PROCESS_FINAL()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_FINAL")
		#ENDIF
		#ENDIF
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
	
		RUN_BIKER_APP_SAVE_COOLDOWN()
		RUN_CASINO_SAVE_COOLDOWN()
		
		IF g_sMPTunables.bBlockNewGameButBlockedSkyCam = FALSE
		
			IF g_b_bBlockNewGameButBlockedSkyCam = FALSE
		
				IF NETWORK_NEED_TO_START_NEW_GAME_BUT_BLOCKED()
					IF IS_SKYSWOOP_IN_SKY()
						PRINTLN("BC: NETWORK_NEED_TO_START_NEW_GAME_BUT_BLOCKED = TRUE, skycam is in the sky not moving. kill skycam ")
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("BC: NETWORK_NEED_TO_START_NEW_GAME_BUT_BLOCKED = TRUE, skycam is in the sky not moving. kill skycam ")
						#ENDIF
						
						KILL_SKYCAM()
						
					ELSE
						
						PRINTLN("BC: NETWORK_NEED_TO_START_NEW_GAME_BUT_BLOCKED = TRUE, skycam isn't in the sky, so start new game ")
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("BC: NETWORK_NEED_TO_START_NEW_GAME_BUT_BLOCKED = TRUE, skycam isn't in the sky, so start new game ")
						#ENDIF
					
						SHUTDOWN_AND_LAUNCH_SINGLE_PLAYER_GAME()
						
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	
		WAIT(0)
		
		#IF IS_DEBUG_BUILD
			IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_SHIFT, "SKIP FM TUTORIALS")
					g_ShouldShiftingTutorialsBeSkipped = TRUE
				ENDIF
			ENDIF
		#ENDIF
		
		
	ENDWHILE
	
	
ENDSCRIPT
