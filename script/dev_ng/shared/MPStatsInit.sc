
//Script used to initialise the stats. Starts in initial.sc - Brenda 07/08/2013
USING "Globals.sch"
USING "MP_Globals.sch"
USING "Net_stat_Generator.sch"



SCRIPT


	INIT_STATS()
	
	g_b_HaveStatsInitialised = TRUE
	PRINTSTRING(" MPStatsInit.sc : g_b_HaveStatsInitialised = TRUE ")
	
	TERMINATE_THIS_THREAD()

ENDSCRIPT


